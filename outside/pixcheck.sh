#!/bin/bash
# 
# script pixcheck for emulating INTAPE/FITS in Midas
# using keyword file "INTAPE.KEY"
# to check for correct no. of data values in FITS file
# 
# execute as $ pixcheck FITS-file [YES]
# if 2nd param. given, full listing is shown
# 
# K. Banse	DMD/DFS, ESO - Garching
# 041112	creation
# 
  
# fill keyword file

echo "p1/c/1/40 1                                           " > INTAPE.KEY
echo "p2/c/1/40 toto                                        " >> INTAPE.KEY
echo "p3/c/1/128 $1" >> INTAPE.KEY
if [ $# -lt 2 ]; then
##   echo "p4/c/1/3 NNN " >> INTAPE.KEY
   echo "p4/c/1/3 SON " >> INTAPE.KEY
else
   echo "p4/c/1/3 FOY " >> INTAPE.KEY
fi
echo "p5/c/1/8 9999    " >> INTAPE.KEY

# switch to outside mode + execute
export MIDAS_OUTSIDE=1
$MID_EXE/intape.exe
# 
# get rid of created Midas file
rm -f toto0001.*
