#!/bin/bash
# 
# set up the variables MID_MONIT, MID_EXE
# which are needed for Midas programs running
# in "outside mode"
# 
# K. Banse	ESO - Garching
# 040105	last modif
# 
## set -x
 
if [ $MIDVERS = "test" ] ; then
   export MIDASHOME=${HOME}/midas
else
   export MIDASHOME=home/esomidas
   export MIDVERS=04SEP
fi

export MID_MONIT=${MIDASHOME}/${MIDVERS}/monit
export MID_EXE=${MIDASHOME}/${MIDVERS}/prim/exec

export PATH=${PATH}:${MIDASHOME}/${MIDVERS}/outside


