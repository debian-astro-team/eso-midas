#!/bin/bash
# 
# script for emulating READ/DESCR in Midas
# using keyword file "DESCR.KEY"
# 
# K. Banse	DMD/DFS, ESO - Garching
# 031218	creation
# 040105	last modif
# 
  
# fill keyword file

echo "in_a/c/1/80 $1" > DESCR.KEY
echo action/c/1/2 rb >> DESCR.KEY

if [ $# -lt 2 ]; then
   echo p2/c/1/80 \*\   >> DESCR.KEY
else
   echo "p2/c/1/80 $2"  >> DESCR.KEY
fi

# switch to outside mode + execute
export MIDAS_OUTSIDE=1
$MID_EXE/descr.exe

