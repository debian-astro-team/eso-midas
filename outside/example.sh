#!/bin/bash
# 
# example script for using a Midas application
# with a keyword file "PROGR.KEY"
# as given in SCSPRO(PROGR) within application
# 
# K. Banse	DMD/DFS, ESO - Garching
# 031218	creation
# 041112	last modif
# 
  
# rm ./PROGR.KEY

# fill keyword file (according to Midas procedure intape.prg)

echo "in_a/c/1/80 $1" > PROGR.KEY
echo action/c/1/2 rb >> PROGR.KEY

if [ $# -lt 2 ]; then
   echo p2/c/1/80 \*\   >> PROGR.KEY
else
   echo "p2/c/1/80 $2"  >> PROGR.KEY
fi

# switch to outside mode + execute
export MIDAS_OUTSIDE=1
$MID_EXE/descr.exe

# cat PROGR.KEY
