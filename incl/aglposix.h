/* @(#)aglposix.h	19.1 (ESO-IPG) 02/25/03 13:49:24 */
/*
 * HEADER : aglposix.h     - Vers 3.6.000  - May 1993 -  C.Guirao, ESO
 *
 *
 * This file contains standard includes needed by AGL, suitable
 * for any Posix conforming system
 *
 * 15-Jul-92: strings.h strrchr() & strchr() automatically defined. CG.
 */

#define UNIX                 /* Define this system as Unix                  */

#include <stdio.h>           /*    Standard libraries                       */
#include <stdlib.h>          /*    Function libraries                       */
#include <fcntl.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <termios.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>

#ifndef MIDAS			/* Midas has his own mechanism to do that   */

#include <strings.h>
#define strchr(a,b)	index(a,b)
#define strrchr(a,b)	rindex(a,b)

#endif
