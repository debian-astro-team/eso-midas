/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		atex.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Character Classification for TeX
.ENVIRONMENT	ASCII characters
.COMMENTS	System-independent character classification.
		(only for systems using ASCII code!)
		Mnemonics are terminated with the underscore (_) for
		functions requiring a table.
.VERSION 1.0	19-Apr-1988: creation
.VERSION 1.1	27-Oct-1990: Added ATEX_C conditional, to avoid the external
		declar
 090831		last modif 
------------------------------------------------------------*/

#ifndef ATEX_DEF

#define ATEX_DEF	0

#include <compiler.h>
#include <osdefos.h>

#ifndef ATEX_C
MID_EXTERN 	unsigned char main_TeX[256]; /* ASCII attributes table	*/
#endif
 
/*===========================================================================
 *		TeX Character Attributes
 *===========================================================================*/

#define _TeX_UPPER_	0x01		/* UPPER-case letter A...Z	*/
#define _TeX_LOWER_	0x02		/* lower-case letter a...z	*/
#define _TeX_DIGIT_	0x04		/* digit             0...9	*/
#define _TeX_SPACE_	0x08		/* space: blank Tab CR LF FF	*/
#define _TeX_PUNCT_	0x10		/* punctuation, but not % etc..	*/
#define _TeX_MATH_	0x20		/* Symbols require Math env	*/
#define _TeX_NULL_	0x40		/* Null character		*/
#define _TeX_ESCAPE_	0x80		/* \ $ % { } ~ & # ^ _		*/

#define _TeX_ALPHA_	(_TeX_UPPER_|_TeX_LOWER_)	/* Alphabetic 	*/
#define _TeX_ALNUM_	(_TeX_ALPHA_|_TeX_DIGIT_)	/* Alphanumeric */
#define _TeX_GRAPH_ 	(_TeX_PUNCT_|_TeX_ALNUM_) 	/* Non-space	*/
#define _TeX_NORMAL_ 	(_TeX_GRAPH_|_TeX_SPACE_) 	/* Non-space	*/

/*======================================================================
 * 			Test of one char attributes
 *======================================================================*/

#define isTeX(c,attr)	(main_TeX[(unsigned char)(c)] & (attr))	 \
			/* test a character for an ASCII attribute	*/

/*======================================================================
 * 			Scan / Span Function
 *======================================================================*/

#define TeX_span(str,len,mask)	oscspan((unsigned char *)str, len, mask, main_TeX)
#define TeX_scan(str,len,mask)	oscscan((unsigned char *)str, len, mask, main_TeX)

#define TeX_bspan(str,len,mask)	oscbspan((unsigned char *)str, len, mask, main_TeX)
#define TeX_bscan(str,len,mask)	oscbscan((unsigned char *)str, len, mask, main_TeX)

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
int	ed_tex(char *dest, int dest_size, char *source);
#endif

#endif
