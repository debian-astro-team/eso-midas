/* @(#)tblfmt.h	19.1 (ESO-IPG) 02/25/03 13:49:43 */
/*+++++++++++++++++++++++ tblfmt.h +++++++++++++++++++++++++++++++++++++
.LANGUAGE        C

.IDENTIFICATION  
    header file TBLFMT.H

.AUTHOR         J.D.Ponz

.KEYWORDS       Table format control 

.ENVIRONMENT    Independent

.VERSION        3.0  1 Feb 1987 : Conversion into C

------------------------------------------------------------------------*/


extern long FMT_NCOL;  
extern long FMT_NROW;  
extern long FMT_LLEN;  

extern struct TBL_FMT FMT[FMT_NBUF]; 

