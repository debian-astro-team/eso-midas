C @(#)idimem.inc	19.1 (ESO-IPG) 02/25/03 13:49:34
C 
C --------------------------  IDIMEM.INC  ------------------------------
C      
      INTEGER            DZMEMI(20)		!not 17 anymore, 001201 
      INTEGER            LOADDR,SSPX,SSPY,NSX,NSY,SFPX,SFPY
      INTEGER            SCALX,SCALY,SCROLX,SCROLY,ZOOMX,ZOOMY
      INTEGER            SOURCE,ITTYES,DZDRAW,ZPLANE
C
      EQUIVALENCE      (DZMEMI(1),LOADDR)
      EQUIVALENCE      (DZMEMI(2),SSPX),(DZMEMI(3),SSPY)
      EQUIVALENCE      (DZMEMI(4),NSX),(DZMEMI(5),NSY)
      EQUIVALENCE      (DZMEMI(6),SFPX),(DZMEMI(7),SFPY)
      EQUIVALENCE      (DZMEMI(8),SCALX),(DZMEMI(9),SCALY)
      EQUIVALENCE      (DZMEMI(10),SCROLX),(DZMEMI(11),SCROLY)
      EQUIVALENCE      (DZMEMI(12),ZOOMX),(DZMEMI(13),ZOOMY)
      EQUIVALENCE      (DZMEMI(14),SOURCE),(DZMEMI(15),ITTYES)
      EQUIVALENCE      (DZMEMI(16),DZDRAW),(DZMEMI(17),ZPLANE)
C      
      COMMON      /IDMEMI/ DZMEMI
C      
C      
C ------------------------
C 
C      LOADDR = load direction flag (0 for bottom-up, 1 for top-down)
C      SSPX   = start screen pixel in X      (for zoom = 1)
C      SSPY   = start screen pixel in Y      (for zoom = 1)
C      NSX    = no. of image channel pixels in X
C      NSY    = no. of image channel pixels in Y
C      SFPX   = start frame pixel in X
C      SFPY   = start frame pixel in Y
C      SCALX  = scaling factor in X
C      SCALY  = scaling factor in Y
C      SCROLX = scroll value in X
C      SCROLY = scroll value in Y
C      ZOOMX  = zoom factor in X
C      ZOOMY  = zoom factor in Y
C      SOURCE = source command for filling the memory channel
C             0, internal write (e.g. DAZINT)
C             1, LOAD/IMAGE 
C             2, LOAD/IMAGE with OVERWRITE option
C      ITTYES = 1 or 0, if ITT enabled or not
C      DZDRAW = 1 if we have graphics/text drawn in an image channel
C	      = 0, if not
C      ZPLANE = no. of plane loaded for 3-dim frames (beginning with 1),
C 		= 0 for 2-dim frames
C
C 
C ------------------------
C 
C  The array DZMEMI is saved in keyword IDIMEMI (and character stuff in IDIMEMC)
C 
C      K. Banse      910109, 930121, 950209, 950623
C
C -----------------------------------------------------------------
