/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c) 1996-2008  European Southern Observatory
.LANGUAGE     C
.IDENT        fitsdef.h
.AUTHOR       P.Grosbol   ESO/IPG
.KEYWORDS     FITS, definitions
.PURPOSE      define FITS specific constants and structures
.VERSION      1.0   1988-Nov-14:  Creation,  PJG
.VERSION      1.1   1989-Oct-23:  Include kwflag in bfdef,  PJG
.VERSION      1.2   1990-Feb-02:  Modify keyword structure,  PJG
.VERSION      1.3   1990-Jul-14:  Add arrays to keyword values,  PJG
.VERSION      1.4   1990-Nov-08:  Change KWDEF and increase MXIDNT, PJG
.VERSION      1.5   1991-Mar-17:  Change BFDEF, PJG
.VERSION      1.6   1992-Aug-13:  Add IMAGE extension, PJG
.VERSION      1.8   1993-Jul-05:  Include comments in MDBUF structure, PJG
.VERSION      1.9   1993-Oct-27:  Include prototypes, PJG
.VERSION      2.0   1996-Oct-22:  Add MXMDN for MIDAS desc. names, PJG
.VERSION      2.1   1998-Aug-19:  Change MXMDN to 49, PJG

 080617		last modif
--------------------------------------------------------------------*/

#include      <fitsfmt.h>

#ifndef       FITSDEF
#define       FITSDEF

#define       MXS                17   /* max. length of char string */
#define       MXMDN              49   /* max. length of desc. name  */
#define       MXDIM              13   /* max. no. of dimensions     */
#define       MIDAS_MXDIM         6   /* max. no. of dims in Midas  */
#define       MXPAR              64   /* max. no. of parameters     */
#define       MXIDNT             82   /* max. identifier length     */
#define       MXHKW               8   /* max. no. of hierarch KW    */
#define       MXKVN              40   /* max. no. of values for KW  */

#define       NOFITS             -3   /* No FITS header             */
#define       EOFITS             -2   /* End Of FITS file           */
#define       FBFITS             -1   /* False basic FITS format    */
#define       BFITSE              0   /* Basic FITS format Empty    */
#define       BFITS               1   /* Basic FITS format          */
#define       RGROUP              2   /* Random groups format       */
#define       UKNOWN              3   /* Unknown extension          */
#define       ATABLE              4   /* ASCII table extension      */
#define       BTABLE              5   /* Binary table extension     */
#define       TEXT                6   /* Text file (ASCII)          */
#define       IMAGE               7   /* Image extension            */

typedef struct {                      /* FITS keyword structure     */
		 char       kw[MXS];  /* prime FITS keyword         */
		 char   *hkw[MXHKW];  /* pointers to hierarch KW    */
		 int            hkn;  /* no. of hierachical KW      */
		 int            kno;  /* no. field of keyword       */
		 int            kvn;  /* no. of values in KW        */
		 char           fmt;  /* format of FITS keyword     */
                                      /* Int, Real, String, Logical */
                                      /* Comment or ? unknown       */
		 union {
		  char        *pc;    /* pointer to string value    */
		  int           i;    /* integer value              */
		  double d[MXKVN];    /* double precision value     */
		       }        val;  /* keyword value              */
		 char         *pcom;  /* pointer to keyword comment */
		 char   buf[MXIDNT];  /* buffer to keep strings     */
		 char   hkb[MXIDNT];  /* buffer for hierarchical KW */
               } KWORD;

typedef struct {                      /* MIDAS descriptor buffer    */
		 char   desc[MXMDN];  /* name of MIDAS descriptor   */
		 char          type;  /* type of MIDAS descriptor   */
                                      /* Int, Real, String, Logical */
                                      /* Comment or ? unknown       */
		 int            idx;  /* index in descriptor        */
		 int            kvn;  /* no. of values              */
		 union {
		  int           i;    /* integer value              */
		  double d[2];	      /* double precision value     */
		       }        val;  /* keyword value              */
		 short int ioff;  /* pointer to keyword comment */
		 char   buf[MXIDNT];  /* buffer to keep strings     */
               } MDBUF;

typedef struct {                      /* keyword definition         */
		 char           *kw;  /* Keyword to be defined      */
		 char           fmt;  /* Keyword data format        */
		 int          group;  /* Group of keyword action    */
		 int         action;  /* action within group        */
		 char         *desc;  /* Associated descriptor      */
		 int            idx;  /* Index for value in desc.   */
		 char          type;  /* Variable type of desc.     */
		 double         fac;  /* Factor desc = fac*keyword  */
		 char         *unit;  /* Physical unit of keyword   */
               } KWDEF;

typedef struct {                      /* Special keyword groups     */
		 char           *kw;  /* Keyword to be defined      */
		 KWDEF        *list;  /* Definitions of low level   */
               } SKWLIST;

typedef struct {                    /* Definition of image scaling  */
                 double      bfac;  /* inverse scaling factor       */
                 double      boff;  /* zero offset of data          */
		 double      dmax;  /* max. data value in matrix    */
		 double      dmin;  /* min. data value in matrix    */
                 long       dsize;  /* size of data matrix in pixel */
               } SDEF;

typedef struct {                    /* Definition of data matrix    */
                 int        naxis;  /* no. of pixels for axis       */
                 double     crval;  /* Coordinate of ref. pixel     */
                 double     crpix;  /* Reference pixel              */
                 double     cdelt;  /* Coordinate increment         */
                 double     crota;  /* Rotation value for axis      */
                 char  ctype[MXS];  /* Type of coordinate axis      */
               } ADEF;

typedef struct {                    /* Definition of parm's block   */
                 double     pscal;  /* Scaling factor of parm.      */
                 double     pzero;  /* Zero offset for parm.        */
                 char  ptype[MXS];  /* Type of parameter            */
               } PDEF;

typedef struct {                      /* Basic data parameters      */
                 int         bitpix;  /* Bits per pixel             */
                 int          count;  /* no. of FITS keyword records
					 before data */
                 int          naxis;  /* no. of axes in data matrix */
                 int         pcount;  /* parameter count value      */
                 int         gcount;  /* group count value          */
                 int          mtype;  /* MIDAS File type            */
                 int         kwflag;  /* flags for given keywords   */
                 int          cflag;  /* frame create flag          */
                 int          tflag;  /* flag for Text-file         */
                 int          bflag;  /* flag for valid BLANK value */
                 int          blank;  /* BLANK value                */
                 int       extlevel;  /* Level no. of extension     */
                 int         extver;  /* Version no. of extension   */
                 int          xflag;  /* flag for FITS extension    */
                 int          mflag;  /* flag for DATAMIN/MAX       */
                 int          sflag;  /* flag for valid scaling     */
                 int          crflag; /* flag for CROTA found       */
                 double        dmin;  /* minimum data value         */
                 double        dmax;  /* maximum data value         */
                 double      bscale;  /* scaling factor on values   */
                 double       bzero;  /* zero offset for values     */
                 char    bunit[MXS];  /* units of values            */
                 char  extname[MXS];  /* Name of extension          */
                 char ident[MXIDNT];  /* Identifier                 */
                 ADEF         *data;  /* pointer to data def's      */
                 PDEF         *parm;  /* pointer to parm. def's     */
                 char         *extd;  /* pointer to ext-data def's  */
               } BFDEF;

typedef struct {                       /* Definitions of a field    */
                 int           tbcol;  /* Start column of field     */
                 int           twdth;  /* Width of field in columns */
                 int           tdfmt;  /* Data format in field      */
                 int           tdfdd;  /* Data fraction dec. digits */
                 int           tncpf;  /* No. of columns per field  */
                 int           trepn;  /* Repetiion no. in field    */
                 int           sflag;  /* Flag for valid scaling    */
                 double        tscal;  /* Scaling factor of field   */
                 double        tzero;  /* Zero offset of field      */
                 int           nflag;  /* Flag for valid NULL value */
                 int           tnnul;  /* Null value for field (BT) */
                 char     tnull[MXS];  /* Null value for field (AT) */
                 char     ttype[MXS];  /* Type of field 'Label'     */
                 char     tform[MXS];  /* Format of field           */
                 char     tunit[MXS];  /* Unit of data field        */
                 char     tdisp[MXS];  /* Display format for field  */
               } FDEF;

typedef struct {                       /* Definition of table ext.  */
                 int            nrow;  /* No. of table rows         */
                 int           mxrow;  /* Max. bytes in full row    */
                 int           mxcol;  /* Max. byte in column entry */
                 int           theap;  /* Heap offset in bytes      */
                 int         tfields;  /* No. of table fields       */
                 FDEF           *col;  /* def's of table fields     */
               } TXDEF;

#include      <proto_dio.h>

#endif
