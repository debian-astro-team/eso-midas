C @(#)prjdat.inc	19.1 (ESO-IPG) 02/25/03 13:49:39
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C       INCLUDE FILE PRJDAT.INC                      20-Jan-1989
C---------------------------------------------------------------------
C++

        DATA ROT0    / 1., 0., 0., 0., 1., 0., 0., 0., 1./
        DATA ROTD    / 1., 0., 0., 0., 1., 0., 0., 0., 1./
        DATA ROTG    / 1., 0., 0., 0., 1., 0., 0., 0., 1./
        DATA ROTL    / 1., 0., 0., 0., 1., 0., 0., 0., 1./
        DATA COSR    /-1./
        DATA ALMAG   /6.5/   
        DATA SCMAG   /3./
CC      DATA PINIT   /.FALSE./  

C  Points PARAMETERS
CC      DATA FULLP   /720./
        DATA FULLP   /90./

C
C**      Include more for : ASTPROJ3, ASTPROJ1,ASTPROJ2
C

       DATA     LX1,LX2,LY1,LY2  /0,1,0,1/
       DATA     X1,  X2, Y1, Y2  /0.,1.,0.,1./
       DATA     XL,  YL          /1.,1./

