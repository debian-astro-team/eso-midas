/* @(#)ftoc_logvms.h	19.1 (ESO-IPG) 02/25/03 13:49:32 */
/*++++++++++++++++++
IDENTIFICATION ftoc_logvms.h
LANGUAGE       C
AUTHOR         Francois Ochsenbein
ENVIRONMENT
KEYWORDS
VERSION  1.0   12-Dec-1990
COMMENTS       Here is the definition of LOGICAL
               values in fortran F77.
	       CG. For VMS.
--------------*/
#ifndef F77DEF_DEF
#define F77DEF_DEF
#define F77TRUE    -1
#define F77FALSE    0
#endif
