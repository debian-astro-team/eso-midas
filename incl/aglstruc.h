/* @(#)aglstruc.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:25   */
/*
 * HEADER : aglstruc.h     - Vers 3.6.001  - Jul 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA
 */




/*********************************           *********************************/
/*                              DATA STRUCTURES                              */


enum GRAPH_MODES { NORMAL=0, USER=1, SPECIAL=2 };

struct polyline	{		/* POLYLINE STRUCTURE                        */
	int maxvects;		/* Maximum number of elements allowed into   */
				/* polyline body vectors                     */
	int nvects;		/* Number of elements into polyline          */
	int marker;		/* Either -1  for polyline, or marker code   */
				/* for polymarker                            */
	float *XV, *YV;		/* polyline body                             */
	enum GRAPH_MODES grmode;/* Graphic mode attached to polyline.        */
				/* Either USER, NORMAL or SPECIAL.           */
	enum GRAPH_MODES clipmode;	/* Clipping mode attached to polyline*/
					/* Either USER or NORMAL.            */
	int join;		/* flag to specify joining of a series of    */
				/* polylines                                 */
	void (*flush)();	/* Flushing routine address                  */
};


struct AGL_cmod {		/* Character modification structure          */
				/* ------------------------------------------*/
				/* The following parameters are set up by    */
                                /* metacharacter interpretation              */
	int the_char;		/* Code of character  to draw                */
	int font;		/* Font                                      */
	int backspace;		/* number of backspaces prior of drawing     */
        int newline;            /* Number of newlines prior of drawing       */
	int lwdt;		/* Line width to be used in drawing          */
	int color;		/* Color to be used in drawing               */
	double mfact;		/* Multiplying factor                        */
	double vshift;		/* Vertical shift                            */
	double slant;		/* Slant factor                              */
};

struct AGL_cdes {		/* Character description structure           */
				/* ------------------------------------------*/
	double blk_gap;		/* Blank gap to next character (pixels)      */
	double heigth;		/* Character heigth (pixels) maximum as in   */
				/* characters l,d                            */
	double width;		/* Character width   (pixels)                */
	double down_ext;	/* Downward extension (pixels) as in p,q     */
	double scale;		/* Scaling factor initialized from font scale*/
	int ncodes;		/* number of codes in char description       */
	unsigned char *codes;	/* pointer to char description               */
	int nstrokes;		/* Number of strokes to draw the character   */
	struct polyline strokes[MAX_STROKES];	/* Stroke polilines          */
	float XB[CHARPOLYBUFLNG];		/* Coordinate buffers        */
	float YB[CHARPOLYBUFLNG];		/* Coordinate buffers        */
};

struct AGL_char {
	struct AGL_cmod modifier;
	struct AGL_cdes descriptor;
};

/* The following structure holds character definition for AGL character      */
/* fonts. Character fonts for basic ASCII characters and for marker symbols  */
/* are coded within AGL, More fonts may be provided (and actually are) as    */
/* files to be read at run time, if required.                                */

struct AGL_font {
	char fontnam [12];	/* Font name                                 */
	int fcode;		/* First code accepted (lower codes return   */
				/* the same character as the first one)      */
	unsigned nchars;	/* Number of characters                      */
	unsigned nbytes;	/* Number of bytes in character buffer       */
	float  c_gap;		/* Blank gap between characters (standard=1) */
	float  maxup;		/* Maximum character dimension upward (as in */
				/* characters d,l)                           */
	float  maxdown;		/* Maximum character dimension downward (as  */
				/* in characters p,q)                        */
	float  fscale;		/* Scaling factor to get same dimension as   */
				/* standard font                             */
				/* For the standard font this value is 1.0   */
	short *index;		/* Pointer to indexes array                  */
	unsigned char *chrwdt;	/* Character widths pointer                  */
	unsigned char *dsclen;	/* Character description lengths pointer     */
	unsigned char *cbuffr;	/* Character description buffer pointer      */
};

struct limits {
	double xlower,xupper;
	double ylower,yupper;
 };


/**********************  AGL DYNAMIC STATUS STRUCTURE  **********************/

/* AGL dynamic status is the set of variables associated with each viewport */

struct VWPDYN {				/**************************************/
					/* DRIVER INTERFACE SUPPORT           */
	int devidx;			/* Device internal index              */
	int curchan;			/* Device graphic channel             */

					/**************************************/
					/* GRAPHIC STATUS VARIABLES           */
	int color;			/* Current pen/color                  */
	int lstyle;			/* Current line style                 */
	int lwidth;			/* Current line width                 */
	int wrmode;			/* Current write mode (S,X,O,E)       */
	int curstyl;			/* Current cursor style               */
	enum GRAPH_MODES modeflag;	/* Current graphic mode               */
	int autoasp;			/* Auto aspect ratio control flag     */
	struct limits vwprt;		/* Current viewport bounds            */
	struct limits clpng;		/* Current clipping area bounds       */
	struct limits saved;		/* Clipping area limits save buffer   */
	
	int window;			/* Window defined flag                */
	int flogx,flogy;		/* Logarithm. transform flags         */
	struct limits usrwn;		/* User window limits                 */
 
	void (*UsrInit)();		/* User initialization routine        */
	int  (*UsrTransf)();		/* User transformation routine        */
	int  (*UsrRTransf)();		/* User inverse transformation routine*/

					/**************************************/
					/* TEXT RELATED VARIABLES             */
	int textlw;			/* Current line width for text        */
	double angfct;			/* Angular conversion factor          */
	double changle;			/* Current string orientation         */
	double chrhsize,chrvsize;	/* Current character sizes            */
	double symbsize;		/* Current symbol size                */
	double scale;			/* Global scaling factor for symbols  */
					/* and characters                     */
	int curfont;			/* Current font selector              */
	
					/**************************************/
					/* METAFILE RELATED VARIABLES         */
	int filact;			/* Metafile active flag               */
	int metamode;			/* Temporary storage for flag         */
	FILE *metafile;			/* Metafile file pointer              */

					/**************************************/
					/* USER DEFINABLE AREA                */
	int n_int_user;			/* Number of integer values defined   */
	int int_user[MAX_INT_US];	/* Integer user parameters area       */
	int n_float_user;		/* Number of float values defined     */
	float float_user[MAX_FLOAT_US];	/* Float user parameters area         */
};


/************************  DEVICE STATUS STRUCTURE  *************************/

struct DEVPAR {
	double xpixels,ypixels;		/* Dimensions in pixels               */
	double xleng,yleng;		/* Default axes length (cm.)          */
	double xmaxl,ymaxl;		/* Max axes lengths (zero if unsupp.) */
	double xcurl,ycurl;		/* Current axes lengths               */
	double rslx,rsly;		/* resolution (1/xpixel,1/ypixel)     */
	double ratyx;			/* Device aspect ratio Y/X            */
	double ratxy;			/* Device aspect ratio X/Y            */
	double dotlen;			/* Dot lenght (for dot/dashes)        */
	double charmult;		/* Character dimension multiplier     */
	unsigned flags;			/* Characteristics flags              */
	int (*(*drvaddr))();		/* Device driver table pointer        */
	int nplans;			/* Number of colors (pens)            */
	int  grchan;			/* Graphic I/O channel                */
	int  actreqst;			/* Active viewport requests           */
	int maxlwid;			/* Max line width identifier          */
	int colors[AGLCOLORS];		/* Standard colors                    */
	char curbackgr;			/* Current background color           */
	char defbackgr;			/* Default background color           */
	char defforegr;			/* Default foreground color           */
	char devnam[PHNAMLNG];		/* Device name                        */
	char filnam[PHNAMLNG];		/* Associated file name               */
	char devcmd[DEVCMDLNG];		/* Command to execute at device close */
};

struct AGL_STATUS {			/* This structure contains the full   */
					/* AGL status                         */

	struct DEVPAR actidx[MAXACTIVE];/* DEVICE CHARACTERISTICS        */


					/**************************************/
					/* STATIC STATUS VARIABLES            */


	struct VWPDYN *vwpts[MAXVWPTS];
	int curvwp;			/* Current viewport id                */
	int devid;			/* Current device index (=DY(devidx)) */
	struct VWPDYN *vwpoint;		/* Current viewport pointer           */

	int  (*CURS_POINTER)();		/* Current driver entry points        */
	int  (*ERAS_POINTER)(); 
	int  (*ESCP_POINTER)();
	int  (*INIT_POINTER)();
	int  (*SEND_POINTER)();
	int  (*TERM_POINTER)();
	int  (*POLY_POINTER)();

					/**************************************/
					/* GRAPHIC STATUS VARIABLES           */
	double aglfcx,aglfcy,		/* Auxiliary params for coord.convers.*/
               aglofx,aglofy;    

	int DefBkgr;			/* Try to set this as default backgr. */
					/* color at device init.              */
	int ersonst;			/* Erase on start flag                */
	int xlogon,ylogon;		/* Current transformation flags       */

	int UTransf;			/* User transformation active flag    */
	int (*CUsrTransf)();		/* Current user transformation entry  */

					/**************************************/
					/* DASH SUPPORT                       */
	int dashon;			/* TRUE if dashing is on              */
					/**************************************/

					/**************************************/
					/* ERROR MANAGEMENT VARIABLES         */

	int  errstat;			/* Most recent error status           */
	int  lasterr;			/* Last error code                    */
	char *lastname;			/* last module name                   */
	int  sevmsg;			/* Severity treshold for  messages    */
	int  sevterm;			/* Severity treshold for termination  */

					/*--------------------------------*/
					/* Device drivers support         */

	int ndevs;			/* Number of device drivers       */
	char 
   	drvname[NDRIVERS][DRVNAMLNG];	/* device driver names table      */
	int 
   	(*drvtab[NDRIVERS][NFUNCTS])();	/* driver entry points table      */

	struct bufcom cmbuffr;
};
