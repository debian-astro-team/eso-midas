/* @(#)proto_ctab.h	19.1 (ESO-IPG) 02/25/03 13:49:39 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   proto_ctab.h
.AUTHOR      C. Guirao
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for all prim/table/libsrc functions

.ENVIRONment none

.VERSION     1.0     24-Nov-1998   Creation 
------------------------------------------------------------*/

#ifndef  PROTO_CTAB              /* Avoid redefinitions */
#define  PROTO_CTAB      0

/*
 * module tbcomsel.c
 */

#ifdef  __cplusplus
extern "C" char*  tbl_select_ima(
const char *intable,
const char *criteria,
int myrefrow
);
#else

extern char*  tbl_select_ima(
#ifdef __STDC__
char *intable,
char *criteria,
int myrefrow
#endif
);
#endif

#endif
