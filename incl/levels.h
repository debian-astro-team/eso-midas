/* @(#)levels.h	19.1 (ESO-IPG) 02/25/03 13:49:35 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE 		Header
.NAME 		STDEF_LEVELS
.VERSION 1.0
.VERSION 1.1	01-Jul-1987
.LANGUAGE 	C
.AUTHOR		Francois Ochsenbein [ESO], Alan Richmond [ST-ECF].
.CATEGORY	General Definitions
.COMMENTS 	This module includes the level definitions:
		each module has his own level (from 0 thru 31)
		used in the program monitoring.
.ENVIRONMENT	
-----------------------*/

#ifndef LEVELS_DEF
#define LEVELS_DEF	0

/*===========================================================================
 *             Levels of Components
 *===========================================================================*/

#define LEVEL_STR	31	/* Level for string-handling functions	*/
#define LEVEL_OS	30	/* OS-dependant routines		*/
#define LEVEL_FI	29	/* File interfaces			*/
#define LEVEL_TU	28	/* TermCap interfaces			*/
#define LEVEL_TV	27	/* Terminal Handling with TermCap	*/
#define LEVEL_TW	26	/* TermWindows functions		*/
#define LEVEL_TX	25	/* TermDisplay				*/
#define LEVEL_TH	24	/* TermHelp				*/
#define LEVEL_TA	24	/* TermHelp				*/
#define LEVEL_TF	23	/* Forms				*/

#define LEVEL_FITS	 6	/* IHAP tranformations			*/
#define LEVEL_IHAP	 5	/* IHAP tranformations			*/
#define LEVEL_X25	 5	/* X25 communications			*/
#define LEVEL_SW	 6	/* Software parsing			*/

#define LEVEL_OO	30	/* Object-Oriented routines		*/

#endif
