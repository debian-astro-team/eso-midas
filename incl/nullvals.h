/* @(#)nullvals.h	19.1 (ESO-IPG) 02/25/03 13:49:36 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE        C
.IDENTIFICATION  header file NULLVALS.H
.AUTHOR         J.D.Ponz, K.Banse
.KEYWORDS       Null values
.ENVIRONMENT    Independent
.VERSION        1.0  871111: modified from TBLCNT.H
------------------------------------------------------------------------*/

#if vms

#define NUL_IVAL  2147483647			/* undefined integer */ 
#define NUL_RVAL  1.701411e38 			/* undefined real */ 
#define NUL_DVAL  1.70141183460469229e38  	/* undefined double */ 
#define NUL_CVAL  '\0' 				/* undefined char  */

#else

#define NUL_IVAL  2147483647			/* undefined integer */ 
#define NUL_RVAL  1.701411e38 			/* undefined real */ 
#define NUL_DVAL  1.70141183460469229e38  	/* undefined double */ 
#define NUL_CVAL  '\0' 				/* undefined char  */

#endif
