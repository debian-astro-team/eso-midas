/* @(#)osnull.h	19.1 (ESO-IPG) 02/25/03 13:49:37 */
/*++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      osnull.h
.AUTHOR              Francois Ochsenbein
.KEYWORDS            operating system, parameters, system structures
.ENVIRONMENT         VMS / UNIX
.COMMENTS            Here are defined how NULL devices are coded.
.VERSION 1.0	10-Jan-1990  Creation
.VERSION 1.1	09-Oct-1991  NULL_DEV for Ultrix. CG.
--------------------------------------------------------------------------*/

#ifndef  OSNULL_DEF 
#define  OSNULL_DEF      0    /* Indicates inclusion of this file */

#ifndef MID_OS_ENV
#include <osdefos.h>
#endif

/*===========================================================================
 *		NULL_Device definition
 *===========================================================================*/
#if	MID_OS_VMS
#define		NULL_DEV	"NL:"
#endif
#if	MID_OS_BSD|MID_OS_SYSV|MID_OS_SYSV_V2|MID_OS_ULTRIX
#define		NULL_DEV	"/dev/null"
#endif
#if	MID_OS_MSDOS
#define		NULL_DEV	"nul:"
#endif
#ifndef NULL_DEV
#define		NULL_DEV	"nul:"
#endif

#endif
