/* @(#)aglsys.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:25   */
/*
 * HEADER : aglsys.h       - Vers 3.6.002  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.001  - May 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *  This file includes system dependent definition files.
 *
 *  The symbol selecting the target system is defined at the cc command
 *  level
 */

#ifndef SYSDEPH_DEF
#define SYSDEPH_DEF

#ifndef ANSI

#ifdef ansi
#define ANSI
#endif

#ifdef __STDC__
#define ANSI
#endif

#endif

#ifdef MIDAS
#include <osdefos.h>
#else
#include <aglos.h>
#endif

#if MID_OS_MSDOS
#include <agldos.h>                 /* MS-DOS dependencies                 */
#endif

#if MID_OS_VMS
#include <aglvms.h>                 /* VAX/VMS dependencies                */
#endif

#if MID_OS_SYSV
#include <aglsysv.h>                /* Unix system V dependencies          */
#endif

#if MID_OS_SYSV_V2
#include <aglsysv.h>                /* Unix system V dependencies          */
#endif

#if MID_OS_ULTRIX
#include <aglsysv.h>                /* Unix ULTRIX dependencies            */
#endif

#if MID_OS_BSD
#include <aglbsd.h>                 /* Unix BSD dependencies               */
#endif

#if MID_OS_POSIX
#include <aglposix.h>               /* POSIX dependencies                  */
#endif

#ifdef UNIX
#include <aglunix.h>                /* Generic Unix dependencies           */
#endif

#ifdef MIDAS

#ifdef NO_STRING_H
#include <strings.h>
#else
#include <string.h>
#endif

#ifdef NO_STRCHR
#define strchr  index
#define strrchr rindex
#endif

#ifndef strchr
char *strchr();
#endif

#ifndef strrchr
char *strrchr();
#endif

#endif

#endif
