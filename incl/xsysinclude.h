/* @(#)xsysinclude.h	19.1 (ESO-IPG) 02/25/03 13:49:46 */
/* ----------------------------------------------------------------- */
/*            Include file xsysinclude.h                             */
/*                                                                   */
/* K.Banse   ESO - DMD						     */
/* 								     */
/* version 1.0	010515	creation				     */
/* 								     */
/* ----------------------------------------------------------------- */

#ifdef vms
# include <decw$include/Xlib.h>
# include <decw$include/Xutil.h>
# include <decw$include/cursorfont.h>
# include <decw$include/keysym.h>

#else
# include <X11/Xlib.h>
# include <X11/Xutil.h>
# include <X11/cursorfont.h>
# include <X11/keysym.h>
#endif



/*
      if you use OpenWindows on the Sun the include files may be somewhere
      else, e.g. at ESO one would have to change the 4 lines above to:

# include "/ns0a/xnews/include/X11/Xlib.h"
# include "/ns0a/xnews/include/X11/Xutil.h"
# include "/ns0a/xnews/include/X11/cursorfont.h"
# include "/ns0a/xnews/include/X11/keysym.h"

*/

