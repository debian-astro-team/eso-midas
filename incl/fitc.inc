C  @(#)fitc.inc	19.1 (ESO-DMD) 02/25/03 13:49:46 
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C.IDENTIFICATION
C       FITC.INC                     version 1.1 840328
C        J.D.Ponz              ESO - Garching
C
C.PURPOSE
C
C       Control Data Structure for Fitting
C
C       User Interface Control Variables : Common /FZCOMMON0/,
C                                            /FZCOMMON1/
C.KEYWORDS
C
C       FIT, DATA STRUCTURE
C
C.VERSIONS
C       2.0 -- Modify common FITCM1 to include first double precision
C 
C 021122	last modif
C 
C------------------------------------------------------------------------------
C
C       Common areas
C
       COMMON/FITCM0/  
     .       FZNAME,FZTYPE,FZMASK,
     .       FZRIMA,FZFNAM,FZSPEC,FZPTOKEN
 
       COMMON/FITCM1/
     .       FZSTART,FZSTEP,			!doubles first
     .       FZERROR,FZVALUE,FZGUESS,
     .       FZNFUN,FZNITER,FZNDAT,
     .       FZNPTOT,FZFLAG,FZDVAR,FZWEIGHT,FZNIND,FZIVAR,FZNPIX,
     .       FZFIXED,FZFCODE,FZACTPAR,FZPLEN,FZRELAX,FZCCHIS,
     .       FZCHISQ,FZCHI,
     .       FZUNCER,FZPFAC,FZMAPPED,FZFPAR,
     .       FZSELE
 
        COMMON/FITCM2/ 
     .       FZMETPAR,FZLOWB,FZUPPB,FZNRPFIX,
     .       FZPPRN,FZIWGT,FZIBND,FZIMET
 
        COMMON/FITCM3/ 
     .       FERPAR,FERFUN,FERTAB,FERDAT,FERPFU,
     .       FERDUP,FERCON,FZIDEN
 
       COMMON/FITCM4/ 
     .       FZPTRI,FZPTRM			!I*8 pointers 
 
       EQUIVALENCE       (FZNAXIS,FZNIND)
       EQUIVALENCE       (FZCHAR,FZNAME)
       EQUIVALENCE       (FZINTG,FZNFUN)
       EQUIVALENCE       (FZREAL,FZRELAX)
       EQUIVALENCE       (FZDUMM,FZGUESS)
