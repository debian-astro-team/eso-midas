/* @(#)filedef.h	17.1 (ESO-IPG) 01/25/02 17:31:09 */
/*+++++++++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      filedef.h
.AUTHOR              Francois Ochsenbein [ESO-IPG]
.KEYWORDS            Keywords for Operations on Files
.ENVIRONMENT         Any
.COMMENTS            Definitions related to File Management
.VERSION 1.0	24-Oct-1989  Extracted from osfile.h
.VERSION 1.1	31-Jan-1990  Added filename operations

 020502		last modif
--------------------------------------------------------------------------*/

#ifndef  FILEDEF_DEF 
#define  FILEDEF_DEF

#include <unistd.h>


/*===========================================================================
 *		Basic Definitions
 *===========================================================================*/

#define	 CHMOD        0
#define	 CHOWN        1

#define  READ         0
#define  WRITE        1
#define  READ_WRITE   2
#define  APPEND       3

#define  FILE_START   SEEK_SET		/* 0 before */
#define  FILE_CURRENT SEEK_CUR		/* 1 before */
#define  FILE_END     SEEK_END		/* 2 before */

#define  PROTECTION   0755     /* Default Protection to create Files	     */
#define  LARGE_BUFFER  0x8000	/* Large Blocking Option	*/
#define  NOATTR	      0x10000	/* VMS only (no record attr) 	*/


#ifndef EOF
#define EOF		(-1)		/* End of File		*/
#endif

/*===========================================================================
 *		Components of a file name
 *===========================================================================*/

#define __NODE_		6	/* To parse file names		*/
#define __PATH_		5	/* To parse file names		*/
#define __FILE_		3
#define __EXTENSION_	2
#define __VERSION_	1

#define _NODE_		(1<<__NODE_)
#define _PATH_		(1<<__PATH_)
#define _FILE_		(1<<__FILE_)
#define _EXTENSION_	(1<<__EXTENSION_)
#define _VERSION_	(1<<__VERSION_)

#define __TYPE_		__EXTENSION_
#define _TYPE_		_EXTENSION_

#ifndef _TEMPLATES_
#include <compiler.h>
#endif
#if _TEMPLATES_
/*===========================================================================
 *			Operations on FileNames
 *===========================================================================*/
char *osfparse	(char *phname, unsigned int components);
char *osfsupply	(char *phname, char *template);
char *osftr	(char *logname);
#else					/* No templates ... */
char *osfparse(), *osfsupply(), *osftr();
#endif

#endif
