/* @(#)osdev.h	19.1 (ESO-IPG) 02/25/03 13:49:36 */
/*++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      osdev.h
.AUTHOR              Francois Ochsenbin, Benoit Pirenne  [ESO-IPG]
.KEYWORDS            operating system, parameters, system structures
.ENVIRONMENT         VMS / UNIX
.COMMENTS            Here are defined structures / constants
			related to special devices (osu routines)
.VERSION 1.0	10-Mar-1988  Creation from old version of osparms.h.
.VERSION 1.1	24-May-1989  Added NULL_DEV
.VERSION 1.2	18-Jul-1989  Modified unistat - Added osublock
.VERSION 1.3	10-Jan-1990  Modified unistat.
.VERSION 1.4	18-May-1990  Parameter to osuclose (with Dismount)
.VERSION 1.5	09-Oct-1991  NULL_DEV for Ultrix. CG.
--------------------------------------------------------------------------*/

#ifndef  OSDEV_DEF 
#define  OSDEV_DEF      0    /* Indicates inclusion of this file */

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
/* int osuinfo 	(int devid, struct devstat *info); */
int osubsize	(int devid);				/* Get Blocksize */
int osuopen 	(char *device, int mode, int density);
int osuclose 	(int devid, int option);
int osuread 	(int devid, char *buffer, int buf_size);
int osuwrite 	(int devid, char *buffer, int length);
int osufclose 	(int devid);				/* Write Tape-Mark */
long osufseek 	(int devid, long offset, int mode);	/* File Seek (tape)*/
long osuftell 	(int devid);				/* File number	   */
long osufeet 	(int devid);				/* Thousandths	   */
long osubseek 	(int devid, long offset, int mode);	/* Block Seek	   */
long osubtell 	(int devid);				/* Block Number	   */
#else
long osufseek(), osuftell(), osubseek(), osubtell();
#endif

/*===========================================================================
 *		NULL_Device definition
 *===========================================================================*/

#include <osdefos.h>
#if	MID_OS_VMS
#define		NULL_DEV	"NL:"
#endif
#if	MID_OS_BSD|MID_OS_SYSV|MID_OS_SYSV_V2|MID_OS_ULTRIX|MID_OS_POSIX
#define		NULL_DEV	"/dev/null"
#endif
#if	MID_OS_MSDOS
#define		NULL_DEV	"nul:"
#endif

#endif
