/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2012 European Southern Observatory
.IDENTifer   cgn.h
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for standard interface modules,
.VERSION

 120420		last modif
------------------------------------------------------------*/

#ifndef  PROTO_ST               /* Avoid redefinitions */
#define  PROTO_ST       0

#include <fitsdef.h>

#ifdef  __cplusplus
extern "C" {
#endif

extern int CGN_CLEANF( 
#ifdef __STDC__
	char *infile,
	int  deftypno,
	char *outfile,
	int  lout,
	int  *namtype,
	int  *ext_flg
#endif
);

extern int CGN_APPNDTYP( 
#ifdef __STDC__
	char *s,
	int  typeno
#endif
);

extern int CGN_CNVT( 
#ifdef __STDC__
	char   *line, 
	int    type,
	int    maxval, 
	int    *ibuf,
	float  *rbuf, 
	double *dbuf
#endif
);
 
extern int CGN_xCNVT( 
#ifdef __STDC__
	char   *line, 
	int    type,
	int    maxval, 
	int    *ibuf,
	float  *rbuf, 
	double *dbuf,
	size_t *sbuf
#endif
);
 
extern int CGN_DATE(
#ifdef __STDC__
	int    iso, 
	int    n,
	char *s
#endif
);
 
extern int CGN_INDEXC(
#ifdef __STDC__
	char *s, 
	char t
#endif
);

extern int CGN_INDEXS(
#ifdef __STDC__
	char *s, 
	char *t
#endif
);

extern int CGN_FILINFO(
#ifdef __STDC__
	char *file, 
	long int *size, 
	long int *time,
	int *prot
#endif
);

extern char CGN_LOWER( 
#ifdef __STDC__
	int c
#endif
);

extern int CGN_NINT( 
#ifdef __STDC__
	float rval   
#endif
);

extern int CGN_DNINT( 
#ifdef __STDC__
	double dval   
#endif
);

extern int CGN_NUMBER( 
#ifdef __STDC__
	char *string 
#endif
);

extern int CGN_OPEN( 
#ifdef __STDC__
	char *file,
	int  option   
#endif
);

extern int CGN_EQUAL( 
#ifdef __STDC__
	char *namea,
	char *nameb  
#endif
);

extern void CGN_REPLA( 
#ifdef __STDC__
	char *string, 
	int lstring, 
	int chara, 
	int charb
#endif
);

extern int CGN_SKIP( 
#ifdef __STDC__
	char *string, 
	char skipchar,
	char flag,
	int  *indx   
#endif
);

extern char CGN_UPPER( 
#ifdef __STDC__
	int c 
#endif
);

extern int CGN_UPCOPY( 
#ifdef __STDC__
	char *stra, 
	char *strb, 
	int lim
#endif
);

extern void CGN_UPSTR( 
#ifdef __STDC__
	char *string
#endif
);

extern void CGN_LOWSTR( 
#ifdef __STDC__
	char *string
#endif
);

extern int CGN_LOWCOPY( 
#ifdef __STDC__
	char *stra, 
	char *strb, 
	int lim
#endif
);

extern void CGN_COPYALL( 
#ifdef __STDC__
	char *cpntra, 
	char *cpntrb, 
	int slen
#endif
);

extern void CGN_strcpy( 
#ifdef __STDC__
	char *dest, 
	char *src 
#endif
);

extern void CGN_strncpy( 
#ifdef __STDC__
	char *dest, 
	char *src,
	size_t n 
#endif
);

extern void CGN_FUNC( 
#ifdef __STDC__
	int    fno, 
	double *dval
#endif
);

extern void CGN_CUTOFF( 
#ifdef __STDC__
	char *instr,
	char *outstr
#endif
);

extern int CGN_DISPFIL( 
#ifdef __STDC__
	int  color,
	char *file, 
	char *prefix, 
	char *label
#endif
);

extern int CGN_GETLIN( 
#ifdef __STDC__
	char *file, 
	char *prefix, 
	char *label,
	char *line
#endif
);

extern int CGN_EXTRSS( 
#ifdef __STDC__
	char *string, 
	int lstring, 
	char delim, 
	int  *start, 
	char *ss, 
	int ssl
#endif
);

extern int CGN_COPY( 
#ifdef __STDC__
	char *instr,
	char *outstr
#endif
);

extern void CGN_GETLINE( 
#ifdef __STDC__
	char *string, 
	int lim
#endif
);

extern int CGN_INDEXK( 
#ifdef __STDC__
	char *s, 
	char t, 
	int count
#endif
);

extern int CGN_JNDEXC( 
#ifdef __STDC__
	char *s, 
	char t
#endif
);

extern int CGN_JNDEXS( 
#ifdef __STDC__
	char *s, 
	char *t
#endif
);

extern int CGN_EXIST( 
#ifdef __STDC__
        int  flag,
	char *direc, 
	char *patt,
        char *fname
#endif
);

extern int CGN_IBUILD( 
#ifdef __STDC__
        int  simno,
	char *name, 
        int  datform,
        int  size,
        int  *imno,
        int  *cloned
#endif
);

extern int clframes(
#ifdef __STDC__
	char    *inname,
	int     type ,
	char    *outname
#endif
);

extern int  CGN_FRAME(  
#ifdef __STDC__
	char *inname, 
	int  type, 
	char *outname,
	int  optio  
#endif
);

extern int CGN_DSCUPD(  
#ifdef __STDC__
	int  inno,
	int  outno,
	char *history  
#endif
);

extern void CGN_LOGNAM(  
#ifdef __STDC__
	char *infile, 
	char *outfile, 
	int  maxout  
#endif
);

/*
** module scca.c
*/

extern int SCCCRE(
#ifdef __STDC__
	char *catfile, 
	int type, 
	int flag
#endif
);

extern int SCCCRA(
#ifdef __STDC__
	char *catfile, 
	int type, 
	int flag,
	char  *catdsc
#endif
);

/*
** module scce.c
*/

extern int SCCADD(
#ifdef __STDC__
	char *catfile, 
	char *name, 
	char *ident
#endif
);

/*
** module scc.c
*/

extern int create_cat(
#ifdef __STDC__
	char *catfile,
	int type,
	int flag,
	int cno
#endif
);

extern int SCCSUB(
#ifdef __STDC__
	char *catfile,
	char *name
#endif
);

extern int SCCGET(
#ifdef __STDC__
	char *catfile, 
	int flag,
	char *name, 
	char *ident,
	int *no
#endif
);

extern int SCCLIS(
#ifdef __STDC__
	char *catfile, 
	int intval[2]
#endif
);

extern int SCCSHO(
#ifdef __STDC__
	char *catfile,
	int *noent, 
	int *last
#endif
);

extern int outcat(
#ifdef __STDC__
	char *catfile,
	int  option,
	int  *intval
#endif
);


/*
** module sccd.c
*/

extern int SCCFND(
#ifdef __STDC__
	char *catfile, 	
	int frmno,
	char *frame
#endif
);

/*
** module scd.c
*/

extern int SCDHRC(
#ifdef __STDC__
	int   imno,
	char  *descr,
	int noelm,
	int felem,
	int maxvals,
	int *actvals,
	char  *values,
	char  *htxt,
	int  hmax,
	int  *unit,
	int *null
#endif
);

extern int SCDHRD(
#ifdef __STDC__
        int   imno,
        char  *descr,
        int felem,
        int maxvals,
        int *actvals,
        double  *values,
        char  *htxt,
        int  hmax,
        int  *unit,
        int *null
#endif
);

extern int SCDHRR(
#ifdef __STDC__
        int   imno,
        char  *descr,
        int felem,
        int maxvals,
        int *actvals,
        float  *values,
        char  *htxt,
        int  hmax,
        int  *unit,
        int *null
#endif
);

extern int SCDHRI(
#ifdef __STDC__
        int   imno,
        char  *descr,
        int felem,
        int maxvals,
        int *actvals,
        int  *values,
        char  *htxt,
        int  hmax,
        int  *unit,
        int *null
#endif
);

extern int SCDHRL(
#ifdef __STDC__
        int   imno,
        char  *descr,
        int felem,
        int maxvals,
        int *actvals,
        int  *values,
        char  *htxt,
        int  hmax,
        int  *unit,
        int *null
#endif
);

extern int SCDRDX(
#ifdef __STDC__
        int imno,
        int flag,
        char *name,
        char *type,
        int *bytelem,
        int *noelem,
        int *hnc
#endif
);

extern int SCDRDC(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	int noelm, 
	int felm, 
	int maxvals,
	int *actvals, 
	char *values, 
	int *unit, 
	int  *null  
#endif
);

extern int SCDRDD(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	int felm, 
	int maxvals,
	int *actvals, 
	double *values, 
	int *unit, 
	int *null  
#endif
);

extern int SCDRDS(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	int felm, 
	int maxvals,
	int *actvals, 
	size_t *values, 
	int *unit, 
	int *null  
#endif
);

extern int SCDRDI(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	int felm, 
	int maxvals,
	int *actvals, 
	int *values, 
	int *unit, 
	int *null  
#endif
);

extern int SCDRDL(
#ifdef __STDC__
        int imno,
        char *desc,
        int felm,
        int maxvals,
        int *actvals,
        int *values,
        int *unit,
        int *null
#endif
);

extern int SCDRDR(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	int felm, 
	int maxvals,
	int *actvals, 
	float *values, 
	int *unit, 
	int *null  
#endif
);

extern int SCDRDH(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	int felm, 
	int maxvals,
	int *actvals, 
	char *values, 
	int *totvals
#endif
);

extern int SCDRDZ(
#ifdef __STDC__
	int imno,
	int *dirsize,
	int *direlem
#endif
);

/*
** module scda.c, scdb.c, scdc.c, scdx.c
*/


extern int SCDCOP(  
#ifdef __STDC__
	int from, 
	int to, 
	int mask, 
	char *dsc
#endif
);

extern int YSCDCOP(  
#ifdef __STDC__
	int from, 
	int to, 
	int mask, 
	char *dsc
#endif
);

extern int SCDFND(
#ifdef __STDC__
	int imno, 
	char *desc, 
	char *type, 
	int *noelm, 
	int *bytelm  
#endif
);

extern int XSCDINF(
#ifdef __STDC__
	int chanl,
	int imno, 
	int snpos, 
	int fno,
	char *buf, 
	int lbuf, 
	int *numbuf
#endif
);

extern int SCDINF(
#ifdef __STDC__
	int imno, 
	int npos, 
	int fno,
	char *buf, 
	int ibuf, 
	int *numbuf
#endif
);

extern int YSCDINF(
#ifdef __STDC__
	int  chanl,
        int imno,
        int npos,
        int fno,
        char *buf,
        int ibuf,
        int *numbuf
#endif
);

extern int SCDDEL(  
#ifdef __STDC__
	int imno, 
	char *dsc
#endif
);

extern int SCDGETC( 
#ifdef __STDC__
	int imno, 
	char *desc, 
	int felm, 
	int maxvals,
	int *actvals, 
	char *values 
#endif
);

extern int SCDWRC(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	int noelm, 
	char *values, 
	int felm, 
	int nvals, 
	int *unit  
#endif
);

extern int SCDWRD(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	double *values, 
	int felm, 
	int nval, 
	int *unit  
#endif
);

extern int SCDWRS(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	size_t *values, 
	int felm, 
	int nval, 
	int *unit  
#endif
);

extern int SCDWRI(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	int *values, 
	int felm, 
	int nval, 
	int *unit  
#endif
);

extern int SCDWRL(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	int *values, 
	int felm, 
	int nval, 
	int *unit  
#endif
);

extern int SCDWRR(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	float *values, 
	int felm, 
	int nval, 
	int *unit  
#endif
);

extern int SCDWRH(  
#ifdef __STDC__
	int imno, 
	char *desc, 
	char *values, 
	int felm, 
	int nval
#endif
);

extern int xwdscdir(  
#ifdef __STDC__
	int imno, 
	char *descr,
	int bytelem, 
	int nval
#endif
);

extern int xSCDWRC(
#ifdef __STDC__
        int imno,
        char *desc,
        int noelm,
        char *values,
        int felm,
        int nvals,
        int *unit
#endif
);

extern int xSCDWRD(
#ifdef __STDC__
        int imno,
        char *desc,
        double *values,
        int felm,
        int nval,
        int *unit
#endif
);

extern int xSCDWRS(
#ifdef __STDC__
        int imno,
        char *desc,
        size_t *values,
        int felm,
        int nval,
        int *unit
#endif
);

extern int xSCDWRI(
#ifdef __STDC__
        int imno,
        char *desc,
        int *values,
        int felm,
        int nval,
        int *unit
#endif
);

extern int xSCDWRL(
#ifdef __STDC__
        int imno,
        char *desc,
        int *values,
        int felm,
        int nval,
        int *unit
#endif
);

extern int xSCDWRR(
#ifdef __STDC__
        int imno,
        char *desc,
        float *values,
        int felm,
        int nval,
        int *unit
#endif
);

extern int xSCDWRH(
#ifdef __STDC__
        int imno,
        char *desc,
        char *values,
        int felm,
        int nval
#endif
);

extern int SCDHWC(
#ifdef __STDC__
        int imno,
        char *desc,
        int noelm,
        char *values,
        int felm,
        int nval,
        int *unit,
        char  *htext
#endif
);

extern int SCDHWD(
#ifdef __STDC__
        int imno,
        char *desc,
        double *values,
        int felm,
        int nval,
        int *unit,
        char  *htext
#endif
);

extern int SCDHWS(
#ifdef __STDC__
        int imno,
        char *desc,
        size_t *values,
        int felm,
        int nval,
        int *unit,
        char  *htext
#endif
);

extern int SCDHWI(
#ifdef __STDC__
        int imno,
        char *desc,
        int *values,
        int felm,
        int nval,
        int *unit,
        char  *htext
#endif
);

extern int SCDHWL(
#ifdef __STDC__
        int imno,
        char *desc,
        int *values,
        int felm,
        int nval,
        int *unit,
        char  *htext
#endif
);

extern int SCDHWR(
#ifdef __STDC__
        int imno,
        char *desc,
        float *values,
        int felm,
        int nval,
        int *unit,
        char  *htext
#endif
);

extern int xSCDHWC(
#ifdef __STDC__
        int imno,
        char *desc,
        int noelm,
        char *values,
        int felm,
        int nval,
        int *unit,
	char  *htext
#endif
);

extern int xSCDHWD(
#ifdef __STDC__
        int imno,
        char *desc,
        double *values,
        int felm,
        int nval,
        int *unit,
	char  *htext
#endif
);

extern int xSCDHWS(
#ifdef __STDC__
        int imno,
        char *desc,
        size_t *values,
        int felm,
        int nval,
        int *unit,
	char  *htext
#endif
);

extern int xSCDHWI(
#ifdef __STDC__
        int imno,
        char *desc,
        int *values,
        int felm,
        int nval,
        int *unit,
	char  *htext
#endif
);

extern int xSCDHWL(
#ifdef __STDC__
        int imno,
        char *desc,
        int *values,
        int felm,
        int nval,
        int *unit,
	char  *htext
#endif
);

extern int xSCDHWR(
#ifdef __STDC__
        int imno,
        char *desc,
        float *values,
        int felm,
        int nval,
        int *unit,
	char  *htext
#endif
);

/*
** module sce.c
*/

extern int  SCECNT(
#ifdef __STDC__
	char *action, 
	int *cont, 
	int *log, 
	int *disp
#endif
);

extern void SCETER(  
#ifdef __STDC__
	int errno, 
	char *text  
#endif
);

extern void SCESIG(  
#ifdef __STDC__
	char *sys, 
	char *text, 
	int stat
#endif
);

/*
** module scf.c, scfa.c, scfb.c, scfe.c
*/

extern int  SCFOPN(  
#ifdef __STDC__
	char *name, 
	int dattyp, 
	int newopn, 
	int fltyp, 
	int *imno  
#endif
);

extern int  SCFSAV(
#ifdef __STDC__
        int imno,
        int flag
#endif
);

extern int  SCFINF(  
#ifdef __STDC__
	char *name, 
	int fno, 
	int *ibuf  
#endif
);

extern int SCFXMP(  
#ifdef __STDC__
	int  nopix,
	int  datform,
	char **pntr
#endif
);
 
extern int SCFYMP(  
#ifdef __STDC__
	int  nopix,
	int  datform,
	int  *imno,
	char **pntr
#endif
);

extern int SCFNAME(
#ifdef __STDC__
	int   fno,
	char  *fname,
	int   fnamlen
#endif
);

extern int  SCFCLO(  
#ifdef __STDC__
	int imno  
#endif
);

extern int  SCFCRE(  
#ifdef __STDC__
	char *name, 
	int dattype, 
	int iomode, 
	int filtype, 
	int size,
	int  *imno
#endif
);

extern int  SCFXCR(  
#ifdef __STDC__
	char *name, 
	int dattype, 
	int iomode, 
	int filtype, 
	size_t sizze,
	int *optio,
	int  *imno
#endif
);

/*
** module scfa.c
*/

extern int  SCFMOD(
#ifdef __STDC__
        int imno,
        int dattype,
        int size
#endif
);

extern int  SCFREE(
#ifdef __STDC__
        char *type,
        char *nampat
#endif
);

extern void  maxi(
#ifdef __STDC__
	int imno,
	size_t datsize,
	float *cuts
#endif
);

/*
* * module scfaux.c
*/

extern int  Fextcor(  
#ifdef __STDC__
	int  fno,
	char *string,
	int  *sta,
	int  *end,
	int  *size
#endif
);

extern int Fcnvsng(  
#ifdef __STDC__
	char *stri,
	int  no
#endif
);

extern int Fextfr(  
#ifdef __STDC__
	int fno, 
	int *sta, 
	int *end, 
	int tno
#endif
);

extern int Finsfr(  
#ifdef __STDC__
	int fno, 
	int tno
#endif
);

extern int cp_squeeze(  
#ifdef __STDC__
	int inq,
	int outq
#endif
);

/*
** module scfd.c
*/

extern int  SCFMAP(
#ifdef __STDC__
	int imno, 
	int iomode, 
	int felm, 
	int size, 
	int *actsiz, 
	char **pntr  
#endif
);

extern int  SCFUNM(  
#ifdef __STDC__
	int imno  
#endif
);

/*
** module scf.c
*/

extern int  SCFREE(
#ifdef __STDC__
        char *type,
        char *nampat
#endif
);

extern int  SCFDEL(  
#ifdef __STDC__
	char *name  
#endif
);

extern int  SCFXDEL(  
#ifdef __STDC__
	int imno 
#endif
);

extern int  SCFRNM(  
#ifdef __STDC__
	char *oldnam, 
	char *newnam  
#endif
);

extern int  SCFGET(  
#ifdef __STDC__
	int imno, 
	int felm, 
	int size, 
	int *actsiz, 
	char *bufadr  
#endif
);

extern int  SCFPUT(  
#ifdef __STDC__
	int imno, 
	int felm, 
	int size, 
	char *bufadr  
#endif
);

/*
** module sci.c
*/
 
extern int SCIGET(  
#ifdef __STDC__
	char *name, 
	int dattype, 
	int iomode, 
	int filtype, 
	int maxnr,
	int *naxis, 
	int *npix, 
	double *start, 
	double *step, 
	char *ident, 
	char *cunit, 
	char **pntr, 
	int *imno  
#endif
);

/*
** module scia.c
*/

extern int SCIPUT(  
#ifdef __STDC__
	char *name, 
	int dattype, 
	int iomode, 
	int filtype,
	int naxis, 
	int *npix, 
	double *start, 
	double *step, 
	char *ident, 
	char *cunit, 
	char **pntr, 
	int *imno  
#endif
);

/*
** module scka.c
*/

extern int SCKFND(  
#ifdef __STDC__
	char *key, 
	char *type, 
	int *noelm, 
	int *byteelm  
#endif
);

extern int SCKINF(  
#ifdef __STDC__
	int npos, 
	int fno, 
	char *buf, 
	int ibuf, 
	int *numbuf
#endif
);

extern int SCKPRC(
#ifdef __STDC__
	char *prompt, 
	char *key, 
	int noelm, 
	int felem, 
	int mxvals, 
	int *iact, 
	char *values, 
	int *unit, 
	int *null
#endif
);

extern int SCKPRD(
#ifdef __STDC__
	char *prompt, 
	char *key, 
	int felem, 
	int mxvals, 
	int *iact, 
	double *values, 
	int *unit, 
	int *null
#endif
);

extern int SCKPRI(
#ifdef __STDC__
	char *prompt, 
	char *key, 
	int felem, 
	int mxvals, 
	int *iact, 
	int *values, 
	int *unit, 
	int *null
#endif
);

extern int SCKPRR(
#ifdef __STDC__
	char *prompt, 
	char *key, 
	int felem, 
	int mxvals, 
	int *iact, 
	float *values, 
	int *unit, 
	int *null
#endif
);

extern int SCKPRS(
#ifdef __STDC__
	char *prompt, 
	char *key, 
	int felem, 
	int mxvals, 
	int *iact, 
	size_t *values, 
	int *unit, 
	int *null
#endif
);

/*
** module sck.c
*/

extern int SCKGETC(  
#ifdef __STDC__
	char *key, 
	int felem, 
	int mxvals, 
	int *iact, 
	char *val  
#endif
);

extern int SCKRDC(  
#ifdef __STDC__
	char *key, 
	int noelm, 
	int foelem, 
	int maxvals, 
	int *actvals, 
	char *values, 
	int *unit, 
	int *null  
#endif
);

extern int SCKRDD(  
#ifdef __STDC__
	char *key, 
	int felem, 
	int maxvals, 
	int *actvals, 
	double *values, 
	int *unit, 
	int *null  
#endif
);

extern int SCKRDI(  
#ifdef __STDC__
	char *key, 
	int felem, 
	int maxvals, 
	int *actvals, 
	int *values, 
	int *unit, 
	int *null  
#endif
);

extern int SCKRDR(  
#ifdef __STDC__
	char *key, 
	int felem, 
	int maxvals, 
	int *actvals, 
	float *values, 
	int *unit, 
	int *null  
#endif
);

extern int SCKRDS(  
#ifdef __STDC__
	char *key, 
	int felem, 
	int maxvals, 
	int *actvals, 
	size_t *values, 
	int *unit, 
	int *null  
#endif
);

/*
** module sckb.c
*/

extern int SCKWRC(  
#ifdef __STDC__
	char *key, 
	int noelm, 
	char *values, 
	int felem, 
	int maxvals, 
	int *unit  
#endif
);

extern int SCKWRD(  
#ifdef __STDC__
	char *key, 
	double *values, 
	int felem, 
	int maxvals, 
	int *unit  
#endif
);

extern int SCKWRS(  
#ifdef __STDC__
	char *key, 
	size_t *values, 
	int felem, 
	int maxvals, 
	int *unit  
#endif
);

extern int SCKWRI(  
#ifdef __STDC__
	char *key, 
	int *values, 
	int felem, 
	int maxvals, 
	int *unit  
#endif
);

extern int SCKWRR(  
#ifdef __STDC__
	char *key, 
	float *values, 
	int felem, 
	int maxvals, 
	int *unit  
#endif
);

/*
** module scp.c
*/

extern int SCPGET(
#ifdef __STDC__
	int imno,
	int plandir,
	int planno,
	char *bufadr
#endif
);

extern int SCPPUT(
#ifdef __STDC__
	int imno,
	int plandir,
	int planno,
	char *bufadr
#endif
);

/*
** module scs.c
*/

extern void interc(  
#ifdef __STDC__
	int s
#endif
);

extern void interf(
#ifdef __STDC__
	void  
#endif
);

extern int myINIT(
#ifdef __STDC__
	int from
#endif
);

extern void tsubr(
#ifdef __STDC__
	void  
#endif
);

extern int  SCSPRO(  
#ifdef __STDC__
	char *prog  
#endif
);

extern int  SCSEPI(  
#ifdef __STDC__
	void  
#endif
);

extern int  SCPSET(  
#ifdef __STDC__
	int  para,
	int  *pval
#endif
);

/*
** module scsa.c
*/

extern int  SCSKIN(  
#ifdef __STDC__
	char  *fname
#endif
);

/*
** module sct.c
*/
 
extern int  SCTPUT(  
#ifdef __STDC__
	char *text  
#endif
);

extern int  SCTSYS(  
#ifdef __STDC__
	int color,
	char *text  
#endif
);

extern int  SCTDIS(  
#ifdef __STDC__
	char *text,
	int  bc
#endif
);

extern int  SCTMES(  
#ifdef __STDC__
	int color,
	char *text  
#endif
);
 
/*
** module midcat.c
*/

extern int MID_CKLO(
#ifdef __STDC__
	char *catnam
#endif
);

extern int MID_CCLO(
#ifdef __STDC__
	int   catno
#endif
);

extern int MID_COPN(
#ifdef __STDC__
	char    *catfile,
	int   *type,
	int   *cimno
#endif
);

/*
** module midcata.c
*/

extern int ascii_tst(
#ifdef __STDC__
	char  *inpntr, 
	char  *outpntr
#endif
);

/*
** module midcatb.c
*/

extern int readcat(
#ifdef __STDC__
	int  catid,
	int  kno, 
	char *buf, 
	int  *kk
#endif
);

extern int rewicat(
#ifdef __STDC__
	int  catid,
	int  kno
#endif
);


/*
** module midcatc.c
*/

extern int MID_CCRE(
#ifdef __STDC__
	char  *catfile,
	int   type,
	char  *descr,
	int   *cimno
#endif
);

/*
** module middsc.c 
*/

extern int DSCNAM_COPY(  
#ifdef __STDC__
	char *out,
	char *in
#endif
);

extern int MID_DSCDIR(
#ifdef __STDC__
	int   entrx,
	char  action,
	char  *descr,
	char  *type,
	int   *bytelem,
	int   *noelem,
	int   *unit,
	int   *block,
	int   *indx
#endif
);

extern void  MID_DRESET(
#ifdef __STDC__
        void
#endif
);

extern int MID_YDSCDIR(
#ifdef __STDC__
	int   entrx,
	char  action,
	char  *descr,
	char  *type,
	int   *bytelem,
	int   *noelem,
	int   *unit,
	int   *block,
	int   *indx,
	char  *help
#endif
);

/*
** module middsca.c
*/

extern int MID_CLONEDS(
#ifdef __STDC__
        struct FCB_STRUCT  *fcbp,
        int  iochan,
        struct FCT_STRUCT  *fctpntr
#endif
);

extern int MID_INITDS(
#ifdef __STDC__
	struct FCB_STRUCT  *fcbp,
	int  iochan
#endif
);

extern int MID_RDSCRC(
#ifdef __STDC__
	int  iochan,
	int  start_block,
	int  start_indx,
	int  first,
	int  total,
	char *cvals,
	int  *nullo
#endif
);


extern int MID_RDSCRI(
#ifdef __STDC__
	int   iochan,
	int   start_block,
	int   start_indx,
	int   first,
	int   total,
	int   *ivals,
	int   *nullo
#endif
);


extern int MID_RDSCRR(
#ifdef __STDC__
	int   iochan,
	int   start_block,
	int   start_indx,
	int   first,
	int   total,
	float *rvals,
	int   *nullo
#endif
);

extern int MID_WDSCRI(
#ifdef __STDC__
	int   iochan,
	int   start_block,
	int   start_indx,
	int   *ivals,
	int   conflg,
	int   first,
	int   nval
#endif
);

extern int MID_WDSCRR(
#ifdef __STDC__
	int   iochan,
	int   start_block,
	int   start_indx,
	float *rvals,
	int   conflg,
	int   first,
	int   nval
#endif
);

extern int MID_WDSCRC(
#ifdef __STDC__
	int   iochan,
	int   start_block,
	int   start_indx,
	char  *cvals,
	int   conflg,
	int   first,
	int   nval
#endif
);

/*
** module midfct.c, midfcta.c, midfctb.c
*/
 
extern int hdr_init_M(
#ifdef __STDC__
	BFDEF *bpntr,
	ADEF **apntr,
	PDEF **ppntr,
	int cf
#endif
);

extern int MID_ACCFITS(
#ifdef __STDC__
	char   *name,
	int   flag,
	int   extensio,
	int   *entrx
#endif
);

extern int MID_ACCFRM(
#ifdef __STDC__
	char   *name,
	int   flag,
	int   *entrx,
	int   *aux
#endif
);

extern int MID_FCTCTRL(
#ifdef __STDC__
	int   flag,
	int   id,
	int   *values
#endif
);

extern int MID_FCTIN(
#ifdef __STDC__
	int  fctnos
#endif
);

extern int MID_fitsin(
#ifdef __STDC__
	int flag,
	char *fname,
	int  extensio,
	char *bname,
	char *outbuf,
	int  *mfd,
	int  *info
#endif
);

extern int fitsmove(
#ifdef __STDC__
	BFDEF *bfdef
#endif
);

extern int MID_fitstest(
#ifdef __STDC__
	char *fname
#endif
);

extern int MyPut(
#ifdef __STDC__
	int kdfmt,
	int kfelem,
	int kdn,
	char *inbuf
#endif
);

extern int MID_CREFRE(
#ifdef __STDC__
	char  *name,
	int   wanted
#endif
);

extern int MID_FINDFR(
#ifdef __STDC__
	char  *name
#endif
);

extern int MID_RETNAM(
#ifdef __STDC__
	int   id,
	char  *name,
	int   lnam
#endif
);

extern int MID_INITFR(
#ifdef __STDC__
	char *name,
	int  dattype,
	int  filtype,
	size_t  sizze,
	int *optio,
	int  *entrx
#endif
);

extern int MID_TDCLON(
#ifdef __STDC__
	int  source_entry,
	int  dattype,
	int  sizze
#endif
);

extern void MID_GETLOCAL(
#ifdef __STDC__
	int *inoutno,
	char *keyid,
	int  *byti,
	int  *noi
#endif
);

extern int MID_FNDKEY(
#ifdef __STDC__
	char *name,
	char *type,
	int  *bytelm,
	int  *noelm,
	int  *unit
#endif
);

extern int MID_MOVKEY(
#ifdef __STDC__
	char *direct,
	char *option
#endif
);

extern void MID_TYPCHK(
#ifdef __STDC__
	char *intype,
	char *outtype,
	int *bytelm
#endif
); 

/*
** module midkeya.c
*/

extern void MID_SHOKEY(
#ifdef __STDC__
	int  flag,
	char *keywo
#endif
);


extern int MID_DSPKEY(
#ifdef __STDC__
	char    *name,
	char  dflag
#endif
);

/*
** module midkeyb.C 
*/ 

extern int MID_TSTKEY( 
#ifdef __STDC__
	char *name
#endif
);

extern int MID_DEFKEY(
#ifdef __STDC__
	char *name,
	char loco,
	char *type,
	int  actval,
	int *unit
#endif
);

extern int MID_DELKEY(
#ifdef __STDC__
	char *name
#endif
);

extern void MID_PACKY(
#ifdef __STDC__
	void
#endif
);

/*
** module midkeyc.c
*/

extern int MID_CPROMPT(
#ifdef __STDC__
	char   *p_string,
	int    *noval,
	char   *carray,
	int    *nullo
#endif
);

extern int MID_DPROMPT(
#ifdef __STDC__
	char   *p_string,
	int    *noval,
	double *array,
	int    *nullo
#endif
);

extern int MID_IPROMPT(
#ifdef __STDC__
	char  *p_string,
	int   *noval,
	int   *carray,
	int   *nullo
#endif
);

extern int MID_RPROMPT(
#ifdef __STDC__
	char  *p_string,
	int   *noval,
	float *carray,
	int   *nullo
#endif
);

extern int MID_SPROMPT(
#ifdef __STDC__
	char  *p_string,
	int   *noval,
	size_t *carray,
	int   *nullo
#endif
);

/*
** module miderr.c
*/

extern void MID_DSPERR(
#ifdef __STDC__
	void
#endif
);

extern void MID_ERRFIL(
#ifdef __STDC__
	int   ulevel,
	char   *label
#endif
);

extern void MID_ERROR(
#ifdef __STDC__
	char   *subsystem,
	char   *text,
	int   status,
	int   print
#endif
);

extern void MID_INITER(
#ifdef __STDC__
	void
#endif
);

extern int MID_PUTERR(
#ifdef __STDC__
	int  source
#endif
);

extern void  MID_E1(
#ifdef __STDC__
	int  funcno,
	char *text,
	int  status,
	int  print
#endif
);

extern void  MID_E2(
#ifdef __STDC__
	int  funcno,
	int imno,
	char *text,
	int  status,
	int  print
#endif
);

extern void  MID_E3(
#ifdef __STDC__
	char *callfunc
#endif
);

extern void  MID_ABORT(
#ifdef __STDC__
	int erro, 
	int source
#endif
);

extern void error_ret(
#ifdef __STDC__
	char *fnam, 
	char *addon,
	int errno, 
	int status
#endif
);

/*
** module midinfo.c
*/

extern void MID_TYPINFO(
#ifdef __STDC__
	char dtype, 
	int  dbytes,
	char *type,
	int  ltype
#endif
);

extern int MID_SHOWFCB(
#ifdef __STDC__
	char *name
#endif
);

extern void MID_SHOWFCT(
#ifdef __STDC__
	char *name
#endif
);

/*
** module midkeyc.c
*/

extern int MID_KEYFILE(
#ifdef __STDC__
	char *prog
#endif
);

/*
** module midldb.c
*/

extern void clearLDB(
#ifdef __STDC__
	int  chanl
#endif
);

extern int cacheLDB(
#ifdef __STDC__
	int flag,
	int  chanl,
	int  blockno,
	struct LDB_STRUCT   **ldbp
#endif
);

extern int MID_CRELDB(
#ifdef __STDC__
	int myentry,
	struct LDB_STRUCT   *ldbp
#endif
);

extern void MID_RDLDB(
#ifdef __STDC__
	int   chanl,
	struct LDB_STRUCT   *ldbp,
	int   indx,
	int   typ,
	int   *iatom,
	float *ratom,
	int   first,
	int   *alen,
	int   *extens
#endif
);

extern void MID_RDcLDB(
#ifdef __STDC__
	int   chanl,
	struct LDB_STRUCT   *ldbp,
	int   indx,
	char  *catom,
	int   first,
	int   *alen,
	int   *extens
#endif
);

extern void MID_WRLDB(
#ifdef __STDC__
	int   chanl,
	struct LDB_STRUCT   *ldbp,
	int   indx,
	int   typ,
	int   *iatom,
	float *ratom,
	char  *catom,
	int   conflg,
	int   first,
	int   *alen,
	int   *extens
#endif
);

/*
** module midlog.c
*/

extern int MID_LOG(
#ifdef __STDC__
	char  flag,
	char  *line,
	int   linlen
#endif
);

/*
** module midmessage.c
*/

extern int MID_message(
#ifdef __STDC__
	int  id,
	char  *nam,
	char  *buf,
	int  indnt
#endif
);

/*
** module midterm.c
*/

extern int MID_VMEM(
#ifdef __STDC__
	int flag,
	int parm2,
	int *fileid
#endif
);

extern int MID_VMIO(
#ifdef __STDC__
	int flag,
	int idx,
	char  *pbuf,
	unsigned int  nobyt,
	int  vbn
#endif
);

extern void MID_TINIT(
#ifdef __STDC__
	void
#endif
);

extern void MID_TPUT(
#ifdef __STDC__
	char *string
#endif
);

extern void MID_TDISP(
#ifdef __STDC__
	char	*string,
	int   b_count,
	int   nocols
#endif
);

extern void MID_TPRO(
#ifdef __STDC__
	char *prompt,
	char *line,
	int  len
#endif
);

extern void MID_TTINFO(
#ifdef __STDC__
	int *tcols,
	int *tlines
#endif
);

/*
** module osyunixc.c
*/

extern int OSY_ASCTIM(
#ifdef __STDC__
	char  *time
#endif
);

extern void OSY_MESSAGE(
#ifdef __STDC__
	int  mstat,
        char *tmp
#endif
);

extern int OSY_TIMER(
#ifdef __STDC__
        char  t_action,
        float  *time
#endif
);

extern int OSY_DASSGN(
#ifdef __STDC__
        int  entrx,
        int  iochan
#endif
);

extern int OSY_DELSEC(
#ifdef __STDC__
	int  entrx
#endif
);

extern int OSY_MAPSEC(
#ifdef __STDC__
	int  iochan,
	int  fvbn,
	int  nvb,
	int  **vmaddr
#endif
);

extern int OSY_SETACC(
#ifdef __STDC__
	int    **vmaddr,
	char   access 
#endif
);

extern int OSY_WVB(
#ifdef __STDC__
	int  iochan,
	char *pbuf,
	unsigned int  nobyt,
	int  nvb
#endif
);

extern int OSY_RVB(
#ifdef __STDC__
	int  iochan,
	char *pbuf,
	unsigned int  nobyt,
	int  nvb
#endif
);

extern int OSY_WLDB(
#ifdef __STDC__
	int  iochan,
	char *pbuf,
	int  nvb
#endif
);

extern int OSY_RLDB(
#ifdef __STDC__
	int  iochan,
	char *pbuf,
	int  nvb
#endif
);


extern int OSY_TRNLOG(
#ifdef __STDC__
	char   *name,
	char   *sysname,
	int   lsysin,
	int   *lsysout
#endif
);


extern int OSY_TRANSLA(
#ifdef __STDC__
        char  *name,
        char  *sysname,
        int   lsysout
#endif
);

extern void OSY_GETSYMB(
#ifdef __STDC__
	char   *symbol,
	char   *resbuf,
	int   lrb
#endif
);

extern int OSY_SLEEP(
#ifdef __STDC__
	unsigned int  ms,
	int flag
#endif
);

#ifdef vms
extern int OSY_SPAWN(
#ifdef __STDC__
	char *mailbox_name,
	char *process,
	char *outterm,
	int  *pid,
	int  (* astmodule)(),
	int  *astparm
#endif
);
#else
extern int OSY_SPAWN(
#ifdef __STDC__
	int  cmd,
	char *phname,
	char *procname,
	int   maxtime,
	int  *pid
#endif
);
#endif

/*
** module scaux.c
*/

extern int conv_dat(
#ifdef __STDC__
	int   flg,
	int   imno,
	int   felem,
	int   size,
	int   *actsize,
	char   **newptr
#endif
);

extern void conv_pix(
#ifdef __STDC__
	char *newpntr, 
	char *oldpntr,
	int  newfmt, 
	int  oldfmt, 
	int  size
#endif
);

extern int get_byte(
#ifdef __STDC__
	int  format 
#endif
);

extern int rddisk(
#ifdef __STDC__
	int   imno,
	int   felem,
	int   size,
	int   *actsize,
	char    *bufadr
#endif
);

extern int wrdisk(
#ifdef __STDC__
	int   imno,
	int   felem,
	int   size,
	char  *bufadr
#endif
);

/*
** module fsy.c
*/

extern void FSY_CREBDF(
#ifdef __STDC__
	char *name,
	int  len,
	int  inalq,
	int  *nvb,
	int  *status 
#endif
);

#if vms
extern void FSY_EXTBDF(
#ifdef __STDC__
	int   fileida,
	int   fileidb,
	char  *device,
	int   exalq,
	int   *nvb,
	int   *status
#endif
#else
extern int FSY_EXTBDF(
#ifdef __STDC__
	int   fileid,
	int   exalq,
	int   *nvb
#endif
#endif
);

extern void FSY_OPNBDF(
#ifdef __STDC__
	char  *name,
	int   len,
	int   *iochan,
	int   *fileida,
	int   *fileidb,
	char  *device,
	int   *status
#endif
);

extern void FSY_OPNFIL(
#ifdef __STDC__
	char  *name,
	int   len,
	int   *iochan,
	int   *status
#endif
);

extern int FSY_MODBDF(
#ifdef __STDC__
        int  fileid,
        long int  nobyt,
        int  *nvb
#endif
);

extern int CGN_singleframe(
#ifdef __STDC__
	char *inname,
	int type,
	char *outname
#endif
);

extern int issub(
#ifdef __STDC__
	char  *name
#endif
);



#ifdef  __cplusplus
}
#endif

#endif 
