/* @(#)idinumd.h	19.1 (ESO-IPG) 02/25/03 13:49:34 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1994 European Southern Observatory
.IDENTifer   idinumd.h
.AUTHOR      K. Banse, ESO - Garching
.PURPOSE     MIDAS definition table for display routines using IDI interfaces

.VERSION     1.0     07-Feb-1994   creation by  K. Banse

001201			last modif
------------------------------------------------------------*/

#define IDI_CLEN 80			/* from 60 (001201) */
#define IDI_ILEN 20			/* from 17 (001201) */
#define IDI_RLEN 8

extern char   dzmemc[];
extern int    dzdev[]; 
extern int    dzmemi[];
extern float  dzmemr[];

struct Point
   {
   int px;
   int py;
   };


#define   QDSPNO   dzdev[0]
#define   QDSZX    dzdev[1]
#define   QDSZY    dzdev[2]
#define   QDDEP    dzdev[3]
/*
dzdev[4], dzdev[5] currently unused
*/
#define   QRGBFL   dzdev[6]
#define   QJOYFL   dzdev[7]
#define   QGRSEG   dzdev[8]
#define   QALPNO   dzdev[9]
#define   QMSZX    dzdev[10]
#define   QMSZY    dzdev[11]
#define   QMDEP    dzdev[12]
#define   QOVCH    dzdev[13]
#define   QIMCH    dzdev[14]
#define   QLSTCH   dzdev[15]
#define   QLUTSZ   dzdev[16]
#define   IDINUM   dzdev[17]
#define   QXAUX    dzdev[18]
#define   QYAUX    dzdev[19]
#define   QNOLUT   dzdev[20]

#define   ZDSPNO   dzdev[22]
#define   QDZFND   dzdev[23]
#define   GDSPNO   dzdev[24]

/*

      QDSPNO    = display no. of currently active display window
      QDSZX     = size of display screen in x
      QDSZY     = size of display screen in y
      QDDEP     = depth of display
      QRGBFL    = RGB/pseudo_colour mode
      QJOYFL    = joystick/trackball flag
      QGRSEG    = no. of graph. segments (only X11)
      QALPNO    = flag for alphanumerics plane
      QMSZX     = size of memory channel in x
      QMSZY     = size of memory channel in y
      QMDEP     = depth of memory channel
      QOVCH     = current overlay channel
      QIMCH     = current active image channel
      QLSTCH    = last channel no.
      QLUTSZ    = size of LUT (it's not always 2**QMDEP ...)
      IDINUM  = number specifying which image display we currently use
                -1 - the NULL device
                 0 - Gould DeAnza IP8500
                 1 - Ramtek
                 2 - Sigma Args
                10 - X-Windows version 10.4
                11 - X-Windows version 11, from release 2 on
      QXAUX     = size of auxiliary window in x (only for XWindow)
      QYAUX     = size of auxiliary window in y 
      QNOLUT    = no. of possible LUTs on device (only for XWindow)
      dzdev[21] = init flag (1 = after initialize/display)
		   only used in procedures
      ZDSPNO    = display no. of connected zoom window
      QDZFND    = 1 if IIDOPN just opens, = 2 if IIDOPN creates the window
      GDSPNO    = display no. of currently active graphics window
  
*/


#define      LOADDR  dzmemi[0]
#define      SSPX    dzmemi[1]
#define      SSPY    dzmemi[2]
#define      NSX     dzmemi[3]
#define      NSY     dzmemi[4]
#define      SFPX    dzmemi[5]
#define      SFPY    dzmemi[6]
#define      SCALX   dzmemi[7]
#define      SCALY   dzmemi[8]
#define      SCROLX  dzmemi[9]
#define      SCROLY  dzmemi[10]
#define      ZOOMX   dzmemi[11]
#define      ZOOMY   dzmemi[12]
#define      SOURCE  dzmemi[13]
#define      ITTYES  dzmemi[14]
#define      DZDRAW  dzmemi[15]
#define      ZPLANE  dzmemi[16]
 
/*
      LOADDR = load direction flag (0 for bottom-up, 1 for top-down)
      SSPX   = start screen pixel in X      (for zoom = 1)
      SSPY   = start screen pixel in Y      (for zoom = 1)
      NSX    = no. of image channel pixels in X
      NSY    = no. of image channel pixels in Y
      SFPX   = start frame pixel in X
      SFPY   = start frame pixel in Y
      SCALX  = scaling factor in X
      SCALY  = scaling factor in Y
      SCROLX = scroll value in X
      SCROLY = scroll value in Y
      ZOOMX  = zoom factor in X
      ZOOMY  = zoom factor in Y
      SOURCE = source command for filling the memory channel
             0, internal write (e.g. DAZINT)
             1, LOAD/IMAGE 
             2, LOAD/IMAGE with OVERWRITE option
      ITTYES = 1 or 0, if an ITT enabled or not
      DZDRAW = 1 if we have graphics/text drawn in an image channel
	      = 0, if not
      ZPLANE = no. of plane loaded for 3-dim frames (beginning with 1),
	     = 0 for 2-dim frames)

*/
