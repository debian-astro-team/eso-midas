/* @(#)agldcaps.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:23   */
/*
 * HEADER : agldcaps.h     - Vers 3.6.000  - Dec 1991 -  L. Fini, OAA
 */


/*                                  DEVICE CHARACTERISTICS FLAGS MASKS      */

#define NCHFLAGS 7               /* Number of device characteristics flags  */

#define INTERACTIVE  1           /* Device interactive flag mask            */
#define RETPIXVAL    2           /* Can return locator pixel value (Unsupp.)*/
#define PARTERASE    4           /* Partial erase flag mask                 */
#define SEPALPHA     8           /* Separated alpha plane flag mask         */
#define ERASEIT     16           /* The device must be cleared on start     */
#define EXECOMMND   32           /* Execute command at close flag mask      */
#define SETBACKGR   64           /* Can set background color                */

