/* @(#)tblmac.h	19.1 (ESO-IPG) 02/25/03 13:49:43 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE        C
.IDENTIFICATION  
    header file TBLMAC.H
.AUTHOR         various
.KEYWORDS       Classical macros
.ENVIRONMENT    Independent
.VERSION        3.0  1 Feb 1987 
------------------------------------------------------------------------*/

#define MAX(x,y)   ((x) >= (y) ?  (x) : (y)) /* maximum */
#define MIN(x,y)   ((x) <= (y) ?  (x) : (y)) /* minimum */
#define ABSOLUTE(x)     ((x) <   0  ? (-x) : (x)) /* absolute */
#define NINT(x)    ((x) <   0  ? ((long) x-0.5) : ((long) x+0.5)) /*NINT*/
#define UNSPEC(add) *(unsigned long *)(add)  /* bit pattern */
