/* @(#)wcs.h	19.1 (ESO-IPG) 02/25/03 13:49:45 */

/*  include file for WCSLIB of Mark Calabretta (mcalabre@atnf.csiro.au)	*/


#ifndef WCS
#define WCS

#include "cel.h"
#include "lin.h"

struct wcsprm {
   int flag;
   char pcode[4];
   char lngtyp[5], lattyp[5];
   int lng, lat;
};

#if __STDC__
   int wcsset(int,
              char[][9],
              struct wcsprm *);

   int wcsfwd(char[][9],
              struct wcsprm *,
              double[],
              double[],
              struct celprm *, 
              double *,
              double *, 
              struct prjprm *, 
              double[], 
              struct linprm *,
              double[]);

   int wcsrev(char[][9],
              struct wcsprm *,
              double[], 
              struct linprm *,
              double[], 
              struct prjprm *, 
              double *,
              double *, 
              double[], 
              struct celprm *, 
              double[]);

   int wcsmix(char[][9],
              struct wcsprm *,
              int,
              int,
              double[],
              double,
              int,
              double[],
              double[],
              struct celprm *,
              double *,
              double *,
              struct prjprm *,
              double[], 
              struct linprm *,
              double[]);

#else /* not __STDC__ */
   int wcsset(), wcsfwd(), wcsrev(), wcsmix();
#endif /* __STDC__ */

#define WCSSET 137

#endif /* WCS */
