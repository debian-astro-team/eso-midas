/* @(#)fitscvb.h	19.1 (ESO-IPG) 02/25/03 13:49:30 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c)  1994   European Southern Observatory
.LANGUAGE     C
.IDENT        fitscvb.h
.AUTHOR       P.Grosbol   ESO/IPG
.KEYWORDS     data representation
.PURPOSE      define computers with different data formats
.VERSION      1.0   1991-Feb-07:  Creation (from datafmt.h),  PJG
.VERSION      1.1   1993-Oct-26:  Include 'fitsfmt.h',  PJG
.VERSION      1.2   1994-May-09:  Cast constants for ANSI-C,  PJG
--------------------------------------------------------------------*/
#include    <computer.h>           /* computer dependant constants  */

#ifndef  _FITSFMT
#include     <fitsfmt.h>
#endif

typedef  struct {
                  int            ifmt;   /* integer format          */
                  int           fpfmt;   /* floating point format   */
                  INT4            bos;   /* byte order for short    */
                  INT4            bol;   /* byte order for long     */
                  INT4            bof;   /* byte order for float    */
                  INT4            bod;   /* byte order for double   */
                } DFMT;

static struct {                     /* 32 and 64 bit IEEE exp. mask  */
	        INT4   dummy;       /* dummy variable for alignment  */
	        char   cd[4];
                char   cf[4];
              } FDMSK = {0,{0x7F,(char)0xF0,0x00,0x00},
	                   {0x7F,(char)0x80,0x00,0x00}};

#define FPXM   (*(INT4 *)FDMSK.cf)         /* 32 bit IEEE exp. mask  */
#define DPXM   (*(INT4 *)FDMSK.cd)         /* 64 bit IEEE exp. mask  */

static  DFMT  cpu = { INTFMT, FLOATFMT, 
		      SWAPSHORT, SWAPINT, SWAPFLOAT, SWAPDOUBLE
		    };

