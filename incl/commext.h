/* @(#)commext.h	19.1 (ESO-DMD) 02/25/03 13:49:27 */
/*
	
-------------------------------- COMMDEF -----------------------------

reference to the structures for MIDAS commands + qualifiers
for definition see file commdef.h

on file MID_INCLUDE:commext.h

K. Banse	890421, 920122, 921216, 930226

011023		last modif

----------------------------------------------------------------------

*/
	

struct	COMND_STRUCT
	{
	char		STR[6];
	char		QDEF[4];
	short int	INDX;
	};
	

struct	QUALIF_STRUCT
	{
	char		STR[4];
	short int	OFF;
	short int	LAST;
	short int	NEXT;
	short int	DEFSET;
	short int	CTXNO;
	};
	


struct	COMND_ALL
	{
	int		CMAX;
	int		QMAX;
	int		ENDLIN;
	int		FIRST;
	int		STRL;
	int		INUSEC;
	int		INUSEQ;
	int		LPRIMC;
	int		LPRIMQ;
	int		FDEL;
	struct	COMND_STRUCT	*CP;
	struct	QUALIF_STRUCT	*QP;
	char			*LINE;
	};

extern struct	COMND_ALL	COMN;

/* --------------------------------------------------------------*/
