/* $Id */
/*++++++++++++++
.IDENTIFICATION ftoc_solaris.h
.LANGUAGE       C
.AUTHOR         Carlos GuiraoSO - Garching
.ENVIRONMENT    with Solaris 8 and g77+gcc V3.3
.VERSION  	031107	creation
.COMMENTS	Using &noargs instead of Cargs. Cargs[-1]=&noargs, but
		for strange reasons Cargs is not initialized correctly
---------------*/

#ifndef  FTOC_DEF		/* Avoid redefinitions */
#define  FTOC_DEF	0

typedef int fint2c;             /* Fortran INT passed to C */
typedef int flong2c;            /* Fortran LONG passed to C */

/* #include <stdarg.h> */		/* Linux */
#include <varargs.h>			/* Solaris */

char *ftoc_get();

	/* In the following definitions:
	   p  is the positional argument number (starting from 1)
	   a  is the total number of FORTRAN arguments
	   s  counts how many CHARACTER arguments exist before p
	   t  is the data type

	   Example: if s' are CHARACTER arguments in the calling sequence
	   CALL SSP(a,b,s1,d,s2,s3)
	   ==> a  is defined by (p=1, a=6, s=0)
	   ==> b  is defined by (p=2, a=6, s=0)
	   ==> s1 is defined by (p=3, a=6, s=0)
	   ==> d  is defined by (p=4, a=6, s=1)
	   ==> s2 is defined by (p=5, a=6, s=1)
	   ==> s3 is defined by (p=6, a=6, s=2)
	 */


#define PARAM(p,a,s,t)     (((t *)(Cargs))[p+3])	/* Solaris */

/* #define PARAM(p,a,s,t)    (((t *)(&noargs))[p]) */	/* Linux */ 


#endif
