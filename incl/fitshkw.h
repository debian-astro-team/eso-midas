/* @(#)fitshkw.h	19.1 (ESO-IPG) 02/25/03 13:49:31 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c)  1994   European Southern Observatory
.LANGUAGE     C
.IDENT        fitshkw.h
.AUTHOR       P.Grosbol   ESO/IPG
.KEYWORDS     FITS, hierarchical keywords, definitions
.PURPOSE      define FITS hierarchical keywords
.VERSION      1.0   1990-Nov-21 :  Creation,  PJG
.VERSION      1.1   1993-Nov-11 :  Add 'OUT-#' level to detector,  PJG
.VERSION      1.2   1994-Feb-04 :  Add more level-2 names,  PJG
--------------------------------------------------------------------*/

typedef struct {                  /* Hierarchical KW level     */
                 char          *name;  /* Name of level keyword     */
                 char          *abrv;  /* Abbreviation for name     */
		 char          *next;  /* Pointer to next level     */
		 KWDEF           *kw;  /* Pointer to keywords level */
               } HKWL;

static KWDEF esogkw[] = {
             {"ID",      'S',WDESC,0,"ID",     1,'S',0.0,""},
             {"NAME",    'S',WDESC,0,"NAME",   1,'S',0.0,""},
             {"TYPE",    'S',WDESC,0,"TYPE",   1,'S',0.0,""},
             {"MODE",    'S',WDESC,0,"MODE",   1,'S',0.0,""},
             {"STATUS",  'S',WDESC,0,"STATUS", 1,'S',0.0,""},
             {"",        '\0',0,0,"",0,'\0', 0.0,""},
	     {(char *) 0,'\0',0,0,(char *) 0,0,'\0',0.0,(char *) 0}
			};

static KWDEF esotkw[] = {
             {"ID",      'S',WDESC,0,"ID",     1,'S',0.0,""},
             {"NAME",    'S',WDESC,0,"NAME",   1,'S',0.0,""},
             {"TYPE",    'S',WDESC,0,"TYPE",   1,'S',0.0,""},
             {"MODE",    'S',WDESC,0,"MODE",   1,'S',0.0,""},
             {"STATUS",  'S',WDESC,0,"STATUS", 1,'S',0.0,""},
             {"", '\0',0,0,"",0,'\0',0.0,""},
	     {(char *) 0,'\0',0,0,(char *) 0,0,'\0',0.0,(char *) 0}
			};

static KWDEF esoakw[] = {
             {"ID",      'S',WDESC,0,"ID",     1,'S',0.0,""},
             {"NAME",    'S',WDESC,0,"NAME",   1,'S',0.0,""},
             {"TYPE",    'S',WDESC,0,"TYPE",   1,'S',0.0,""},
             {"MODE",    'S',WDESC,0,"MODE",   1,'S',0.0,""},
             {"STATUS",  'S',WDESC,0,"STATUS", 1,'S',0.0,""},
             {"", '\0',0,0,"",0,'\0',0.0,""},
	     {(char *) 0,'\0',0,0,(char *) 0,0,'\0',0.0,(char *) 0}
			};

static KWDEF esoikw[] = {
             {"ID",      'S',WDESC,0,"ID",     1,'S',0.0,""},
             {"NAME",    'S',WDESC,0,"NAME",   1,'S',0.0,""},
             {"TYPE",    'S',WDESC,0,"TYPE",   1,'S',0.0,""},
             {"MODE",    'S',WDESC,0,"MODE",   1,'S',0.0,""},
             {"STATUS",  'S',WDESC,0,"STATUS", 1,'S',0.0,""},
             {"", '\0',0,0,"",0,'\0',0.0,""},
	     {(char *) 0,'\0',0,0,(char *) 0,0,'\0',0.0,(char *) 0}
			};

static KWDEF esodkw[] = {
             {"ID",      'S',WDESC,0,"ID",     1,'S',0.0,""},
             {"NAME",    'S',WDESC,0,"NAME",   1,'S',0.0,""},
             {"TYPE",    'S',WDESC,0,"TYPE",   1,'S',0.0,""},
             {"MODE",    'S',WDESC,0,"MODE",   1,'S',0.0,""},
             {"STATUS",  'S',WDESC,0,"STATUS", 1,'S',0.0,""},
             {"PARM#",   'S',WDESC,0,"PARM#",  1,'S',0.0,""},
             {"DATE",    'T',WDESC,0,"DATE", 1,'R',0.0,""},
             {"", '\0',0,0,"",0,'\0',0.0,""},
	     {(char *) 0,'\0',0,0,(char *) 0,0,'\0',0.0,(char *) 0}
			};

static KWDEF esockw[] = {
             {"ID",      'S',WDESC,0,"ID",   1,'S',0.0,""},
             {"NAME",    'S',WDESC,0,"NAME", 1,'S',0.0,""},
             {"TYPE",    'S',WDESC,0,"TYPE", 1,'S',0.0,""},
             {"MODE",    'S',WDESC,0,"MODE", 1,'S',0.0,""},
             {"", '\0',0,0,"",0,'\0',0.0,""},
	     {(char *) 0,'\0',0,0,(char *) 0,0,'\0',0.0,(char *) 0}
			};

static HKWL  esogen[] = {
                          {"PROJ","P", (char *) 0, esogkw},
                          {"TARG","T", (char *) 0, esogkw},
                          {"EXPO","E", (char *) 0, esogkw},
                          {"CAT","C",  (char *) 0, esogkw},
                          {"WIND","W", (char *) 0, esogkw},
			  {(char *) 0, "", (char *) 0, (KWDEF *) 0}
			};

static HKWL  esotel[] = {
                          {"FOCU","F",     (char *) 0, esotkw},
                          {"TRAK","T",     (char *) 0, esotkw},
                          {"DOME","D",     (char *) 0, esotkw},
                          {"PARANG","P",   (char *) 0, esotkw},
                          {"AIRM","A",     (char *) 0, esotkw},
			  {(char *) 0, "", (char *) 0, esotkw}
			};

static HKWL  esoada[] = {
                          {"LAMP-#","L#", (char *) 0, esoakw},
                          {"GUID-#","G#", (char *) 0, esoakw},
			  {(char *) 0, "", (char *) 0, (KWDEF *) 0}
			};

static HKWL  esoins[] = {
                          {"FOCU",   "F", (char *) 0, esoikw},
                          {"COMP",   "C", (char *) 0, esoikw},
                          {"MIRR-#","M#", (char *) 0, esoikw},
                          {"GRAT-#","G#", (char *) 0, esoikw},
                          {"SLIT-#","S#", (char *) 0, esoikw},
                          {"OPTI-#","O#", (char *) 0, esoikw},
			  {(char *) 0, "", (char *) 0, (KWDEF *) 0}
			};

static HKWL  esodet[] = {
                          {"FRAM","F",     (char *) 0, esodkw},
                          {"SHUT","S",     (char *) 0, esodkw},
                          {"COMP","C",     (char *) 0, esodkw},
                          {"OUT-#","O#",   (char *) 0, esodkw},
			  {(char *) 0, "", (char *) 0, esodkw}
			};

static HKWL  esohkw[] = {
                          {"GEN","G", (char *) esogen,     esogkw},
                          {"TEL","T", (char *) esotel,     esotkw},
                          {"ADA","A", (char *) esoada,     esoakw},
                          {"INS","I", (char *) esoins,     esoikw},
                          {"DET","D", (char *) esodet,     esodkw},
                          {"ARC","C", (char *)      0,     esockw},
			  {(char *) 0, "", (char *) 0, (KWDEF *) 0}
			};

static HKWL  hkwgrp[] = {
                          {"ESO",    "_E", (char *) esohkw, (KWDEF *) 0},
			  {(char *) 0, "", (char *) 0,      (KWDEF *) 0}
			};

