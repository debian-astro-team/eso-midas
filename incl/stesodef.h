/* @(#)stesodef.h	19.1 (ESO-IPG) 02/25/03 13:49:41 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		stesodef.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO], Alan Richmond [ST-ECF].
.CATEGORY	General Definitions
.COMMENTS	ESO / ST-ECF Standard Header File.
	To be included in all STARCAT modules.
	e.g. to know the definitions of OK or NOK, returned by almost
	all routines as completion code. Major STCDEFs are also included. 
	etc. etc.
.ENVIRONMENT	*** Minimal Implementation ***
.HISTORY     Original version imported from GSSS and  STScI (STCDEFS).
.VERSION 1.0   Francois Ochsenbein:
                     special typedef's,
                     constants depending on the computer,
                     memory management.

.VERSION 2.0 	20-Jan-1986 : Merged ESODEF and STARCAT headers.[ar]

.VERSION 3.0 	06-Jun-1986 : Modified ENTER Macro and added ENTER_ for 
         non-interruptible routines.

.VERSION 4.0	01-Aug-1986 : Split in several files:
.VERSION 5.0	13-Oct-1988 : Use os Interfaces, Separated into components.
		If the `Pascalisation' is to be taken out,
		just add the line 
		`#define PASCAL_DEF 0'
.VERSION 5.1	21-Jul-1989 : Added OpenProgramLog, main Log File
------------------------------------------------------------*/

#ifndef STESODEF_DEF
#define STESODEF_DEF	0

/*===========================================================================
 *			Computer-dependant constants
 *===========================================================================*/

#include <osdefos.h>	/* Operating System	*/
#include <compiler.h>	/* Constants depending on the compiler */

/*===========================================================================
 *			Special Values Definitions
 *===========================================================================*/

#include <ok.h>

/*===========================================================================
 *			Classical macros
 *===========================================================================*/

#include <macrogen.h>

/*===========================================================================
 *			Memory Management
 *===========================================================================*/

#if _TEMPLATES_
void	*mm_alloc	(unsigned int bytes);
void	*mm_expand	(void *address, unsigned int bytes);
void	*mm_save	(void *address, unsigned int bytes);
int	 mm_free	(void *address);
#else
char 	*mm_alloc(), *mm_expand();
#endif

#define MEM_FREE(ptr) 	mm_free((char *)ptr)
			/* free previously allocated memory	*/
#define MEM_EXP(type,ptr,n)	(type *)mm_expand(ptr,sizeof(type)*(n)) \
			/* reallocates n items of type		*/
#define MEM_GET(type,n)		(type *)mm_alloc(sizeof(type)*(n)) 	 \
			/* allocates n items of type		*/
#define MEM_SAVE(type,ptr,n)	(type *)mm_save(ptr,sizeof(type)*(n)) 	 \
			/* copies n items of type into new area	*/

/*===========================================================================
 *			Program Monitoring Subsystem
 *===========================================================================*/

#include <trace.h>
#include <levels.h>
#include <error.h>
char *osmsg();			/* OS Messages */

#if (SW_LEVEL == 0)
#	define SYSNAME      "Minimal"   /* Minimal Implementation	*/
#else
#	define SYSNAME      "Complete"	/* Minimal Implementation	*/
#endif

/*===========================================================================
 *			File Definitions / Usages
 *===========================================================================*/

#include <osfile.h>

#if _TEMPLATES_
int 	fi_create	(char *filename, int filesize, int protection);
int     fi_load 	(char *filename, long offset, char *buffer, 
					unsigned int buffer_size);
int 	fi_open		(char *filename, int open_mode);
char   *fi_name    	(int fid);
int 	fi_close    	(int fid);
int     fi_read 	(int fid, char *buffer, unsigned int buffer_size);
int 	fi_write	(int fid, char *buffer, unsigned int buffer_length);
long 	fi_seek     	(int fid, long offset, int mode);
long 	fi_tell     	(int fid);
int     fi_gets 	(int fid, char *buffer, unsigned int buffer_size);
int 	fi_put   	(int fid, char *text);
int 	fi_puts    	(int fid, char *record);
int 	fi_flush	(int fid);
int 	fi_chmod	(char *filename, int new_protection);
int 	fi_delete	(char *filename);
int 	fi_rename	(char *filename, char *newname);
long	fi_size		(char *filename);
long	fi_date		(char *filename);
#else
long 	fi_seek(), fi_tell(), fi_date(), fi_size();
char	*fi_name();
#endif
	
#define OPEN_IN(f)	fi_open(f,READ|RECORD_MODE)	/* Open INput file */
#define OPEN_OUT(f)	fi_open(f,WRITE)	/* Open OUTput file     */
#define OPEN_MOD(f)	fi_open(f,READ_WRITE)	/* Open file for MODifs */
#define OPEN_APPEND(f)	fi_open(f,APPEND)	/* Open file for Append */
#define REWIND(f) 	fi_seek(f,0,FILE_START)	/* Go to beginning of file */
#define ENDFILE(f)	fi_seek(f,0,FILE_END)	/* Go to end of file       */
#define fi_wopen(f)	osfop('V', 2048), fi_open(f,WRITE|RECORD_MODE)

#define ParseFile(name,op)	ParseFilename(name,op)
#define ParseFilename(name,op)	osfparse(name,op)		\
		/* Extract parts of a file name to buffer	*/
#define NameFile(name,temp)	osfsupply(name, temp)		\
		/* Name a file according to a template		*/

#define OpenProgramLog(title)	/* Open The Log File */		\
	OpenLog(NameFile(ParseFile(GetProgramFile(),_FILE_),".log"),title)

/*===========================================================================
 *		Various Host Services
 *===========================================================================*/

#if _TEMPLATES_
char *oshenv(char *logname, char *table_name);
		/* Translation of logical names (str, table) */
long oshtime(void);
long oshgmt (void);
int  oshcmd (char *command, char *in_file, char *out_file, char *err_file);
#else		/* No Templates */
char *oshenv();
long oshtime(), oshgmt();
#endif

/*===========================================================================
 *			C 'Pascalisation'
 *===========================================================================*/

#include <pascal.h>

#endif
