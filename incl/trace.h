/* @(#)trace.h	19.1 (ESO-IPG) 02/25/03 13:49:44 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		trace.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO], Alan Richmond [ST-ECF].
.CATEGORY 	General Definitions
.COMMENTS	Definition of tracing routines and macros.
		Note that TRACING equated to zero disables the tracing.
.ENVIRONMENT

.VERSION 1.0	01-Aug-1986: creation
		(Excerpt from original STESODEF file)
.VERSION 1.1	27-Jan-1987: Added confidentiality macros
		to avoid logging sensible data in the log file
.VERSION 1.2	25-Jun-1987: Minimal SW_level
.VERSION 1.3	01-Feb-1988: For Midas usage
.VERSION 1.4	08-May-1989: Added GetLevel() to retrieve max trace level.
			     M. Albrecht [ESIS] (see `#ifdef ESIS')
.VERSION 1.5	20-Jun-1989: Use NOTRACING option 
.VERSION 1.5	06-Oct-1989: pm_lfile / GetLog added.
------------------------------------------------------------*/

#ifndef TRACE_DEF
#define TRACE_DEF	0

#ifdef  NOTRACING
#define TRACING		0
#endif

#ifndef TRACING
#  ifdef PM_LEVEL
#    define TRACING	1
#  else
#    define TRACING	0
#  endif
#endif

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

#define MONITOR(module)		static  int  status= 0

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
int 	 pm_get_depth 	(void);		/* Current depth	*/
int 	 pm_sensible 	(int true_false);
int 	 pm_set_depth 	(int max_tracing_depth);	
int 	 pm_fetch 	(char *function, int max_tracing_depth);
int 	 pm_unfetch 	(char *function);
int 	 pm_open 	(char *filename, char *title);
int 	 pm_close 	(void);
int 	 pm_level	(int tracing_level);
int 	 pm_stop	(int tracing_level);
int 	 pm_enter 	(int level, char *function);
int 	 pm_iexit 	(int level, int status);
long  	 pm_lexit 	(int level,long status);
char   	*pm_pexit 	(int level,char *status);
int 	 pm_history 	(void);
int 	 pm_trace 	(int level, char *text);
int 	 pm_tr2 	(int level, char *text, int text_length);
int 	 pm_ed_trace  	(int level, char *text, char *string);
int 	 pm_ed_tr2 	(int level, char *text, char *string,int string_length);
int 	 pm_ed_i 	(int level, char *text, int value);
int 	 pm_log 	(char *string);
int 	 pm_comment 	(char *string);
int 	 pm_get_level	(void);
int 	 pm_lfile	(void);
int 	 pm_islev	(int level_to_check);
#else
char *pm_pexit();
long  pm_lexit();
#endif

/*===========================================================================
 *			Tracing Definitions
 *===========================================================================*/

#define OpenLog(filename,title)	pm_open(filename,title)
#define GetLog()		pm_lfile()
#define CloseLog()		pm_close()
#define Confidential(x)		pm_sensible(x)
#define ConfidentialOn()	pm_sensible(1)
#define ConfidentialOff()	pm_sensible(0)

#define TraceOn(level)		pm_level(level)
#define TraceOff(level)		pm_stop(level)
#define TraceUntil(depth)	pm_set_depth(depth)
#define LogLevel		TraceOn
#define LogHistory()		pm_history()

#define TraceRoutine(f)		pm_fetch(f,0)
#define TraceRoutineUntil(f,d)	pm_fetch(f,d)
#define UntraceRoutine(f)	pm_unfetch(f)

#define GetDepth()		pm_get_depth()
#define GetLevel()		pm_get_level()
#define CheckTracing(lev)	pm_islev(lev)

/*===========================================================================
 *		TRACING Macros always valid
 *===========================================================================*/

#define ENTER_			ENTER
#define EXIT			EXITi
#define _EXIT			EXIT_PTR
#define TRACE			TRACE_STRING

#if TRACING
/*===========================================================================
 *		Tracing Definitions if Tracing is ON
 *===========================================================================*/

#define ENTER(fct)		pm_enter(PM_LEVEL,fct)	 		\
		/* Pass the function name 		*/
#define EXITi(code)		return( pm_iexit (PM_LEVEL,code))	\
		/* Pass the completion code 		*/
#define EXITl(code)		return(pm_lexit(PM_LEVEL,code))		\
		/* Pass the completion code 		*/
#define EXITp(code)		return( pm_pexit (PM_LEVEL,code))	\
		/* Pass the completion code 		*/
#define EXIT_PTR(t,code)	return((t *)pm_pexit(PM_LEVEL,(char *)code))\
		/* Pass the completion code 		*/
#define EXIT_CAST(t,code)	return((t)pm_pexit(PM_LEVEL,(char *)code))\
		/* Pass the completion code 		*/
#define TRACE_STR2(text,l)	pm_tr2(PM_LEVEL,text,l)            	\
		/* Trace a string of length l		*/
#define TRACE_TEXT(text)	pm_tr2(PM_LEVEL,text,sizeof(text)-1)	\
		/* Trace a constant string text		*/
#define TRACE_STRING(text)	pm_trace(PM_LEVEL,text) 		\
		/* Trace an EOS-terminated string 	*/
#define TRACE_ED_TEXT(t,s)	pm_ed_str2(PM_LEVEL,t,s,sizeof(s)-1)	\
		/* Trace a text followed by a string  	*/
#define TRACE_ED_STRING(t,s)	pm_ed_trace(PM_LEVEL,t,s)		\
		/* Trace a text followed by a string  	*/
#define TRACE_ED_STR2(t,s,l)	pm_ed_tr2(PM_LEVEL,t,s,l)		\
		/* Trace a text followed by a string  	*/
#define TRACE_ED_I(t,i)		pm_ed_i   (PM_LEVEL,t,i)		\
		/* Trace a text followed by a number  	*/

#else		/* Suppress Tracing			*/
/*===========================================================================
 *		Tracing Definitions if Tracing is OFF
 *===========================================================================*/

#define ENTER(fct)
#define EXITi(code)		return(code)
#define EXITl(code)		return(code)
#define EXITp(code)		return(code)
#define EXIT_PTR(t,code)	return((t *)code)
#define EXIT_CAST(t,code)	return((t)code)
#define TRACE_STR2(text,l)
#define TRACE_TEXT(text)
#define TRACE_STRING(text)
#define TRACE_ED_TEXT(t,s)
#define TRACE_ED_STRING(t,s)
#define TRACE_ED_STR2(t,s,l)
#define TRACE_ED_I(t,i)
#endif

/*===========================================================================
 *			LOG Macros
 *===========================================================================*/

#define LOG(string)		pm_log (string) 			 \
		/* Log a string				*/
#define LOG_COMMENT(string)	pm_comment (string)			 \
		/* Log a comment string			*/
#define LOG_ED_TEXT(t,s)	pm_ed_str2(-1,t,s,sizeof(s)-1)	\
		/* Log a text followed by a string  	*/
#define LOG_ED_STRING(t,s)	pm_ed_trace(-1,t,s)		\
		/* Log a text followed by a string  	*/
#define LOG_ED_STR2(t,s,l)	pm_ed_tr2(-1,t,s,l)		\
		/* Log a text followed by a string  	*/
#define LOG_ED_I(t,i)		pm_ed_i   (-1,t,i)		\
		/* Log a text followed by a number  	*/

#endif
