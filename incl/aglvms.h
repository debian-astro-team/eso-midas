/* @(#)aglvms.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:25   */
/*
 * HEADER : aglvms.h       - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA
 *
 */

/*                             VAX/VMS definitions                           */

/* This file contains parameter definitions needed for VAX/VMS version       */

/* The following parameters are not strictly system dependent but may be     */
/* different for different system for optimization purposes                  */

#ifndef VMS
#define VMS
#endif

#define MAXVWPTS 10        /* Number of viewports concurently active         */
#define MAXACTIVE 5        /* Number of different devices concurrently active*/
#define NDRIVERS 25        /* Max number of device drivers supported         */

#define MAXTXTLEN 133      /* Max length of a text string                    */
#define PHNAMLNG 277       /* Max suitable file name length                  */
#define DRVNAMLNG 21       /* Max driver file name length + 1                */
#define DEVCMDLNG 513      /* Max device close command length + 1            */

#define DEVGLOBAL "AGL3DEV"       /* Override config. current device type    */

#define AGLSTDIR "AGL3CONFIG" /* Environment var with AGL standard directory */

/*                            Standard VAX C libraries                       */

#include <stdlib.h>
#include <stdio.h>
#include <file.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <descrip.h>
#include <time.h>
#include <unixio.h>

                          /* Define parameters for fopen()                   */
#define B_CREATE "w"                           /* Create binary file         */
#define B_APPEND "a"                           /* Append to binary file      */
#define B_READ   "r"                           /* Open for read binary file  */
#define T_CREATE "w"                           /* Create text file           */
#define T_APPEND "a"                           /* Append to text file        */
#define T_READ   "r"                           /* Open for read text file    */


#define SYSTEM AGL_spawn                     /* VAX-C doesn't support system()*/

                          /* Unix I/O routines emulation (for drivers)       */
                          /* They are all defined in vmsdep.c                */

                             /* These macros must work on any character case */

#define TOUPPER(x) (islower(x) ? toupper(x) : (x))
#define TOLOWER(x) (isupper(x) ? tolower(x) : (x))

#define REMOVE remove


#define RAWMODE 1
