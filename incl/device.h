/* @(#)device.h	19.1 (ESO-IPG) 02/25/03 13:49:28 */
/*++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      device.h
.AUTHOR              Francois Ochsenbein [ESO-IPG]
.KEYWORDS            Tape / Raw devices
.ENVIRONMENT         
.COMMENTS            Here are defined structures / constants
			related to special devices (dev_ routines)
.VERSION 1.0	10-Jan-1990: Creation 
----------------------------------------------*/

#ifndef  DEVICE_DEF 
#define  DEVICE_DEF      0    /* Indicates inclusion of this file */

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

/*===========================================================================
 *		Unit Info Structure
 *===========================================================================*/

#ifndef  DEVSTAT_DEF 
#include <devstat.h>
#endif

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
int  dev_stat 	(int devid, struct devstat *info);
int  dev_bsize	(int devid);				/* Get Blocksize */
int  dev_open 	(char *device, int mode, int density);
int  dev_close 	(int devid, int option);
int  dev_read 	(int devid, char *buffer, int buf_size);
int  dev_write 	(int devid, char *buffer, int length);
int  dev_fclose (int devid);				/* Write Tape-Mark */
long dev_fseek 	(int devid, long offset, int mode);	/* File Seek (tape)*/
long dev_ftell 	(int devid);				/* File number	   */
long dev_feet 	(int devid);				/* Tape length	   */
long dev_bseek 	(int devid, long offset, int mode);	/* Block Seek	   */
long dev_btell 	(int devid);				/* Block Number	   */
#else
long dev_fseek(), dev_ftell(), dev_bseek(), dev_btell();
#endif

#endif
