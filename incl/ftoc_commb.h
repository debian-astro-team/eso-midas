/* @(#)ftoc_commb.h	19.1 (ESO-IPG) 02/25/03 13:49:32 */
/*++++++++++++++
.IDENTIFICATION ftoc_commb.h
.LANGUAGE       C
.AUTHOR         Francois Ochsenbein
.ENVIRONMENT    
.KEYWORDS       
.VERSION  1.0   12-Dec-1990
.COMMENTS       Here there is the definition of the COMMON used
		for address passing between FORTRAN routines (replaces %LOC)
		Only Apollo is currently using this definition
---------------*/

#ifndef VMR_DEF
#define VMR_DEF		0	/* To avoid redefinitions */

		/* structure for COMMON in main Fortran programs  */
/*typedef struct { int addr; } vmmidas; */
/*vmmidas         vmr     #attribute[section(vmr)]; */

struct { int addr;}    vmr     #attribute[section(vmr)]; 

		/* translate address into FORTRAN index in VMR common */
#define COMMON_INDEX(a)		(int *)a - (int *)&((&(vmr.addr))[-1])
#endif
