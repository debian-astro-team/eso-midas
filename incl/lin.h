/* @(#)lin.h	19.1 (ESO-IPG) 02/25/03 13:49:35 */

/*  include file for WCSLIB of Mark Calabretta (mcalabre@atnf.csiro.au) */

#ifndef LIN
#define LIN

struct linprm {
   int flag;
   int naxis;
   double *crpix;
   double *pc;
   double *cdelt;

   /* Intermediates. */
   double *piximg;
   double *imgpix;
};

#if __STDC__
   int linset(struct linprm *);
   int linfwd(double[], struct linprm *, double[]);
   int linrev(double[], struct linprm *, double[]);
   int matinv(int, double [], double []);
#else /* not __STDC__ */
   int linset(), linfwd(), linrev(), matinv();
#endif /* __STDC__ */

#define LINSET 137

#endif /* LIN */
