/* @(#)fitsfmt.h	19.1 (ESO-IPG) 02/25/03 13:49:30 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c)  1993   European Southern Observatory
.LANGUAGE     C
.IDENT        fitsfmt.h
.AUTHOR       P.Grosbol   ESO/IPG
.KEYWORDS     type definitions
.PURPOSE      define of data types and constants for FITS
.VERSION      1.0   1991-Jan-25:  Creation (from datafmt.h),  PJG
.VERSION      1.1   1993-Sep-22:  change INT4 to 'int',  PJG
--------------------------------------------------------------------*/
#ifndef     _FITSFMT
#define     _FITSFMT     1

#define     FITS         1             /* FITS format file          */
#define     IHAP         2             /* IHAP format file          */

#define     FITSLR    2880         /* Logical record size of FITS   */

typedef     char            BYTE;
typedef     unsigned char   UINT1;
typedef     short           INT2;
typedef     unsigned short  UINT2;
typedef     int             INT4;
typedef     unsigned int    UINT4;
typedef     float           REAL4;
typedef     double          REAL8;

#endif
/* Using OSY_TRNLOG in VMS instead of osfphname() */

#ifdef vms
#define	osfphname(x,y)	OSY_TRNLOG(x,y,64,&no)
#endif
