/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE        C
.IDENTIFICATION  header file tbledt.h
.AUTHOR         J.D.Ponz
.KEYWORDS       Table editor control buffers
.ENVIRONMENT    Independent
.VERSION        3.0  1 Feb 1988
.PURPOSE
                Define table editor structures.
.VERSION        3.1  15 Sept 1989, CG 890915
.VERSION        3.2  24-Nov-1989, added NEXTFIELD (FO)

 090713		last modif
------------------------------------------------------------------------*/

#include <tw.h>      /* TermWindow definitions */
#include <twhelp.h>      /* TermWindow definitions */

#define EDT_MAXCOL     64 /* maximum number of columns to be edited */
#define EDT_MAXROW    100 /* maximum number of rows to be edited */
#define EDT_MAXCHA   1024 /* maximum number of characters per row edited */
#define EDT_NAMLEN     20 /* length of window name */
#define EDT_MAXMOD    128 
#define EDT_TXTLEN     80 /* length of file names */
/* TRUE & FALSE maybe defined in other include file.	CG 890915 */
#ifndef TRUE
#define TRUE		1 
#endif

#ifndef FALSE
#define FALSE		0
#endif

/* DATA WINDOW */

WINDOW *editor_window     /* pointer to editor window */;

WINDOW *data_subwindow, *sequence_subwindow, *header_window;

WINDOW *dialogue_window           /* pointer to the dialogue window */;

TWHELP *twh                       /* pointer to help */;


/* GENERAL EDT CONTROL VARIABLES */
short   cursor_pos[2];           /* cursor position*/
/* static short   cursor_pos[2]; */          /* cursor position*/
/*static int     thecol = 0;     */         /* current column number*/
int     thecol ;              /* current column number*/
char    edt_term[EDT_NAMLEN]            /* device name */;
char    edt_termfile[EDT_NAMLEN]          /* term file */;
int     edt_env                           /* control characters */;
int     edt_width                           /* actual line width */;
int     data_lines               /* number of lines in the data window */;
int     data_columns             /* number of columns in the data window */;
int     edt_column[EDT_MAXCOL]   /* column index */;
int     edt_nr                   /* actual number of rows displayed */;
int     edt_nc                   /* actual number of columns displayed */;
int     edt_row[EDT_MAXROW]      /* row index */;
int     edt_wlen                 /* length in word buffer */;
int     edt_llen                 /* length in line buffer */;
char    edt_linebuf[EDT_MAXCHA]  /* line buffer */;
char    edt_wordbuf[EDT_MAXCHA]  /* word buffer */;
char    edt_charbuf[1]           /* character buffer */;
int     edt_advance              /* advance flag (TRUE adv., FALSE rev.) */;
int     edt_mode                 /* editing mode (TABULAR/ROW) */;
int     edt_status               /* exit status (EXIT/QUIT) */;
int     edt_action               /* action == TERMINATE */;
double  edt_vsearch              /* value to search */;
double  edt_esearch              /* error in the search */;
char    edt_csearch[EDT_MAXCHA]  /* string to search */;
int     edt_cstart;
int     edt_cend;

/* TABLE CONTROL VARIABLES */
char    edt_table[EDT_TXTLEN]    /* table name */;
int     edt_tid;
int     edt_ncol                 /* number of columns defined in the table */;
int     edt_nrow                 /* number of rows in the table */;
int     edt_nacol                /* number of words per row */;
int     edt_narow                /* number of allocated rows */;

#define TABULAR        0
#define ROW            1
#define TERMINATE      99
#define NULLACTION     0
#define INTERRUPT      1
/* #define EOF            2 	not needed ... KB 090710 */
#define BAD_KEY        3
#define NEXTCOL        4
#define PREVCOL        5
#define RIGHTPAGE      6
#define LEFTPAGE       7
#define FIRSTPAGE      8
#define RESETPAGE      9
#define QUIT          10 
#define EXIT          11 
#define COMMAND       12
#define NEXTFIELD     13
