/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		str.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Operations on Strings.
.ENVIRONMENT	
.COMMENTS	System-independent string functions, using osc routines.
		Strings are assumed to be terminated with a null character.
		Mnemonics are str for case-sensitive function, stu for
		case insensitive functions. 
		The function name is terminated with the underscore (_) for
		functions requiring a table.
.VERSION 1.0	06-Jun-1986: creation
.VERSION 1.1	23-Jun-1987: Control chars definition
.VERSION 2.0	21-Jan-1988: Inserted as ctype
.VERSION 2.1	12-May-1989: Added strncopy
.VERSION 2.2	22-Jun-1989: Added strsave
.VERSION 2.3	31-Jan-1990: Added tokenisation functions, strloc1, strscan1
.VERSION 2.4	29-Mar-1990: Renamed string tokenization functions.
		Changed strred to a function.
.VERSION 2.5	11-Sep-1990: Added strnins strins
 070829		last modif
------------------------------------------------------------*/

#ifndef STR_DEF
#define STR_DEF		0

#include <atype.h>

/*===========================================================================
 *			Copy functions	(with overlay)
 *===========================================================================*/

int strcopy	(char *dest, char *source);
int strncopy	(char *dest, int dest_size, char *source);
int strfill 	(char *dest, int len, int filler);
int strnins	(char *dest, int dest_size, char *text);
int strins	(char *dest, char *text);
char *mm_ssave	(char *source);

/*======================================================================
 * 			Locate / Skip characters
 *======================================================================*/

int strloc	(char *str, int c);
int strloc1	(char *str, int c);	/* char not escaped */
int stuloc	(char *str, int c);
int strbloc	(char *str, int c);
int stubloc	(char *str, int c);

int strskip	(char *str, int c);
int stuskip	(char *str, int c);
int strbskip	(char *str, int c);
int stubskip	(char *str, int c);

int strline_	(char *source, int linesize, unsigned char mask, 
			unsigned char *table);

/*======================================================================
 * 			Scan / Span Functions
 *======================================================================*/

int strspan_	(char *source, unsigned char mask, unsigned char *table);
int strbspan_	(char *source, unsigned char mask, unsigned char *table);

int strspans	(char *source, char *matching_list);
int stuspans	(char *source, char *matching_list);
int strbspans	(char *source, char *matching_list);
int stubspans	(char *source, char *matching_list);

int strscan_	(char *source, unsigned char mask, unsigned char *table);
int strbscan_	(char *source, unsigned char mask, unsigned char *table);

int strscans	(char *source, char *matching_list);
int strscan1	(char *source, char *matching_list);
int stuscans	(char *source, char *matching_list);
int strbscans	(char *source, char *matching_list);
int stubscans	(char *source, char *matching_list);

int strset	(unsigned char *generated_table, char *list);
int stuset	(unsigned char *generated_table, char *list);


/*======================================================================
 * 			Comparison macros
 *======================================================================*/

int strcomp	(char *str1, char *str2);
int stucomp	(char *str1, char *str2);

int strmatch	(char *str1, char *str2);
int stumatch	(char *str1, char *str2);

int strindex	(char *source, char *substring);
int stuindex	(char *source, char *substring);

int stritem	(char *source, char *item, char *list_of_seperators);
int stuitem	(char *source, char *item, char *list_of_seperators);

/*======================================================================
 * 			Conversion Function
 *======================================================================*/

int strupper	(char *str);
int strlower	(char *str);
int strcase	(char *str);

int strsetr	(unsigned char *table, char *from,  char *to);

int strtr_	(char *dest, char *source, unsigned char *table);
int strtrs	(char *dest, char *source, char *to_convert, char *converted);

int strred_	(char *str, unsigned char mask, unsigned char *table);
int strred	(char *str);
int strred1	(char *str, char escape);

int strdel_	(char *str, unsigned char mask, unsigned char *table);

int ed_tab 	(char *dest, int desst_size, char *source, int source_len, 
		int tab_len);

/*======================================================================
 * 		Compute Length of Token (str2)
 *======================================================================*/
int stkid 	(char *str);		/* Identifier 	*/
int stkint 	(char *str);		/* Integer	*/
int stknum 	(char *str);		/* Numeric	*/
int stkstr 	(char *str);		/* Quoted str	*/
int stkcomment	(char *str);		/* / * ... * /	*/


/*======================================================================
 * 			Macros
 *======================================================================*/

#define strdiff(s1,s2)			strcomp(s1,s2)
#define studiff(s1,s2)			stucomp(s1,s2)
#define strblank(dest,len)		strfill(dest,len,' ')

#define strline(s,lmax)			strline_(s,lmax,_SPACE_,main_ascii)
#define strspan(s,mask)			strspan_(s,mask,main_ascii)
#define strbspan(s,mask)		strbspan_(s,mask,main_ascii)
#define strscan(s,mask)			strscan_(s,mask,main_ascii)
#define strbscan(s,mask)		strbscan_(s,mask,main_ascii)
#define strdel(s,mask)			strdel_(s, mask, main_ascii)

#define strfree(s)			mm_free(s)
#define strsave(s)			mm_ssave(s)

#define stripspaces(s)			s[1+strbspan(s,_SPACE_)] = EOS   \
					/* Remove Trailing Blanks	*/

#endif
