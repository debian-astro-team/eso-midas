/* @(#)wd.h	19.1 (ESO-IPG) 02/25/03 13:49:45 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE 		Header
.NAME 		STDEF_WD
.LANGUAGE 	C
.AUTHOR		Francois Ochsenbein [ESO]
.CATEGORY	Window Display
.COMMENTS 	Definition of structures for displaying 
		a ``TeX-like'' text on a window.
.ENVIRONMENT	
.VERSION 1.0	26-Sep-1986: Creation
.VERSION 1.1	30-Jan-1987: Removed bug in definition of DisplayText
.VERSION 1.2	01-Jul-1987: Directly included in WINDOW.H
------------------------------------------------------------*/

#ifndef WD_DEF

#define WD_DEF

/*===========================================================================
 *             Structure WD and related
 *===========================================================================*/

struct WD_struct {
	int 	allocated_buffer;	/* Buffer containing TeX-like text   */
	int 	used_buffer;
	char	*buffer;
	char	*text_to_insert;
	short int allocated_depth;	/* Depth conditions		*/
	short int depth;		/* Current depth		*/
	struct depth_struct *wd_depth;
	short int allocated_cols;	/* columns in Table Mode	*/
	short int used_cols;
	short int lineno;
	short int cols;			/* Current column number	*/
	struct cols_struct *wd_cols;
	unsigned char flags;
#define StartedMacro	0x01
#define ExecuteMacro	0x02
	unsigned char tab;		/* Tabular mode			*/
	unsigned char icol;		/* Increment for next column	*/
	unsigned char unused;
	int	offset;			/* Current position		*/
	};
typedef struct WD_struct WD;

struct depth_struct {
	short int start, len;
	unsigned char smart;		/* Smart (justify) attribute	*/
	unsigned char attr;		/* Attribute for this depth	*/
	unsigned short opening;		/* What to do at opening	*/
	unsigned char closing;		/* What to do at closing	*/
	unsigned char unused;
	};
typedef struct depth_struct s_depth;

struct cols_struct {
	short int start, len;		/* Position & length of column	*/
	unsigned char smart;		/* Smart (justify) attribute	*/
	unsigned char flags;		/* Not used			*/
	};
typedef struct cols_struct s_cols;

#define NULL_WD		(WD*)_NULL

/*===========================================================================
 *             Related functions and macros
 *===========================================================================*/

WD *wd_alloc();

#define AllocateWD(size)		wd_alloc(size)
#define LoadTex(file,wd,stopper)	wd_load(file,wd,stopper)
#define DisplayTex(window,wd)		wd_display(window,wd)
#define DisplayText(window,str)		wd_text(window,str)

#endif
