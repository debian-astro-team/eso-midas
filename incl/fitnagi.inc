C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C.IDENTIFICATION
C       FITNAGI.INC                     version 1.1 851205
C        J.D.Ponz              ESO - Garching
C       Ph. Defert               "
C
C.PURPOSE
C
C       Control Data Structure for Fitting
C
C       User Interface Control Variables : Common /FIT_COMMON0/,
C                                            /FIT_COMMON1/
C.KEYWORDS
C
C       FIT, DATA STRUCTURE
C
C 040209	last modif
C 
C------------------------------------------------------------------------------
C
C       parameters
C
       INTEGER       MAXFUN       ! max.no. of allowed functions
       INTEGER MAXIND       ! max.no. of independent variables
       INTEGER       MAXPAR       ! max.no. of used parameters
       INTEGER       MAXDEFF       ! no. of system defined functions
       INTEGER       NCHAR       ! no. of characters saved
       INTEGER       NINTG       ! no. of integers saved
       INTEGER       NREAL       ! no. of reals saved
C
       INTEGER       CINTG       ! optional number of copied integers
       INTEGER       CREAL       ! optional number of copied reals
       PARAMETER       (MAXFUN=16)
       PARAMETER       (MAXIND=3)
       PARAMETER       (MAXPAR=128)
       PARAMETER       (MAXDEFF=50) ! no. of system functions implemented
       PARAMETER       (NCHAR=28)       ! 12+4+12
       PARAMETER       (NINTG=14)       ! long load
       PARAMETER       (NREAL=109)       ! 3+3*PARMAX*FUNMAX+2*INDMAX+10
       PARAMETER       (CREAL=103)       ! 
       PARAMETER       (CINTG=5)       ! short load
C
C       Character       (FITCHAR)
C
       CHARACTER*12       FILNAM       ! name of i/o file
       CHARACTER*4       FILTYP       ! type of i/o file ('BDF ','TBL ')
       CHARACTER*12       MASK       ! optional i/o weight mask
       CHARACTER*12       RIMA       ! optional reference image
C
       CHARACTER*8       FNAM(MAXDEFF)       ! system defined function names
       CHARACTER*80       SPEC(MAXFUN)       ! function spec
       CHARACTER*8       PTOKEN(MAXPAR)       ! parameter names
C
C       Integers saved (FITINTG)
C
       INTEGER        NRFUN       ! no. of defined functions
       INTEGER              NRITER       ! iteration counter
       INTEGER         NRDATA       ! no. of data entering in the fit
       INTEGER              NRPARAM       ! total number of parameters
       INTEGER              FLAG       ! NOT USED FOR NAG
C
C       Integers optionally loaded
C
       INTEGER        DEPCOL       ! dependent variable (0 -BDF, col#-TBL)
       INTEGER        WGTCOL       ! optional i/o weight column
       INTEGER        NRIND       ! no. of independent variables
       INTEGER        INDCOL(MAXIND)       ! independent variables
       INTEGER              NRPIX(MAXIND)       ! number of pixels per axis
C
C       Integers used as work space
C
       INTEGER       FIXPAR(MAXPAR)       ! fixed param. flag
       INTEGER       FCTCOD(MAXFUN)       ! function code
       INTEGER       ACTPAR(MAXFUN)       ! actual n. of params
       INTEGER       PLEN(MAXPAR)       ! length of the param names
C
       INTEGER       NRAXIS       ! number of axis for BDF(=NIND)
       INTEGER       FZPPRN
       INTEGER       FZIWGT 
       INTEGER       FZIBND 
       INTEGER       FZIMET 
C
C       Reals saved (FITREAL)
C
       REAL              RELAX       ! relaxation factor
       REAL              FINCHI       ! Final chi sq. value
       REAL              PRECIS        ! defined chi sq. value
       REAL              CHI(100)       ! chisq conv
       DOUBLE PRECISION  START(MAXIND)       ! starting coords. for BDF
       DOUBLE PRECISION  STEP(MAXIND)       ! sampling step for BDF
C       (FITERROR)
       DOUBLE PRECISION       ERROR(MAXPAR)       ! parameter error 
C       (FITPARAM)
       DOUBLE PRECISION       PARAM(MAXPAR)       ! par. values
C
C       Reals used as work space
C
       DOUBLE PRECISION       PARINI(MAXPAR)       ! initial guess
       REAL              UNCER(MAXPAR)       ! uncertain.
       REAL              PRPFAC(MAXPAR)       ! linear terms in the const.
C
C       auxiliary data
C
       INTEGER*8       PTRI       ! pointer to bdf
       INTEGER*8       PTRM       ! pointer to weights

       INTEGER       MAPPED       ! mapped flag
       INTEGER       FPAR(MAXDEFF)       ! number of parameters per
                                          ! defined function
       INTEGER       SELE(20)       ! function selection flag
C
       CHARACTER*28       CHAR              ! char buffer
       INTEGER       INTG(NINTG)       ! intg buffer
C       REAL              REAL(NREAL)       ! real buffer
C
C       Specific to NAG and the new version
C
       REAL              METPAR(10)      ! method parameters
       DOUBLE PRECISION       PARLOW(MAXPAR)       ! par. lower bnd
       DOUBLE PRECISION       PARUPP(MAXPAR)       ! par. upper bnd
        INTEGER                 NRPFIX          ! number of fixed par.
C
C       Errors
C
       INTEGER       ERRFPAR       ! error in input parameters
       INTEGER       ERRFFUN       ! non defined function name
       INTEGER       ERRFTAB       ! inp/out on different tables
       INTEGER       ERRFDAT       ! data not available
       INTEGER       ERRFPFU       ! error in function params.
       INTEGER       ERRFDUP       ! duplicated param name
       INTEGER       ERRFCON       ! error in cons. definition
C
C  ... added in portable version
C
       INTEGER       FZIDEN
