/* @(#)trtime.h	19.1 (ESO-IPG) 02/25/03 13:49:44 */
/*++++++
.HEADER		trtime.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.KEYWORDS	Operation on time/date
.COMMENTS	This header contains declarations for time transformations.
\begin{TeX}
\begin{itemize}
\item	integer (mnemonic {\tt i}): the number of seconds elapsed
		since Jan 1, 1970, at 00:00:00.
		The available period is roughly 1902-2037.
\item	double (mnemonic {\tt jd}): the Julian Date
\item	structure (mnemonic {\tt tm}): a structure containing, as integers:
	\begin{itemize}
	\item	{\tt year} number
	\item 	{\tt mon} the month number in range [0-11]
	\item 	{\tt mday} the day number within month, in range [1-31]
	\item 	{\tt yday} the day number within year, in range [0-355]
	\item 	{\tt wday} the day number within week, in range [0-6]
				(0 is Sunday)
	\item 	{\tt hour} in range [0-23]
	\item 	{\tt min} in range [0-59]
	\item 	{\tt sec} in range [0-59]
	\end{itemize}
\end{itemize}
This structure is the system file <time.h>
\end{TeX}

.VERSION 1.0	04-Mar-1987: Creation.
.VERSION 1.1	13-Oct-1988: Cosmetic modifications
------------------*/

#ifndef  TRTIME_DEF
#define  TRTIME_DEF	0

#include <time.h>	/* System	*/

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

/*=========================================================================
		Time structure
 *=========================================================================*/

/* The time may be expressed as:
	- long integer = time elapsed(s) from Jan. 1, 1970, 0h TU (mnemonic t)
	- tm structure (mnemonic T) defined in system file <time.h>
	- double = JD julian day (mnemonic JD)
 */

#if 0 /*===========================TAKEN FROM SYSTEM ===================*/
struct tm {			/* Gregorian!		*/
	int	year,		/* Complete year	*/
		mon,		/* 0-11		*/
		mday,		/* 1-31		*/
		hour,		/* 0-23		*/
		min,		/* 0-59		*/
		sec,		/* 0-59		*/
		wday,		/* 0(Sunday)-6	*/
		yday,		/* 0-365	*/
		isdst;		/* unused	*/
	   };        	
#endif

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
long 	oshtime	(void);			/* Current time from 1970 in sec   */
int 	tr_itm	(long t, struct tm *T);
int 	tr_tmi	(struct tm *T, long *t);
int 	tr_tm	(struct tm *T);
int 	tr_atm	(char *str, int len, struct tm *T);
int 	tr_at	(char *str, int len, long *t);
int 	ed_t	(char *buffer, char *pic, long t);
int 	ed_t_F	(char *buffer, char *pic, long t);		/* French  */
int 	ed_t_G	(char *buffer, char *pic, long t);		/* German  */
int 	ed_tm	(char *buffer, char *pic, struct tm *T);
int 	ed_tm_F	(char *buffer, char *pic, struct tm *T);	/* French  */
int 	ed_tm_G	(char *buffer, char *pic, struct tm *T);	/* German  */
int 	TeX_tm_F(char *buffer, char *pic, struct tm *T);	/* French  */
int 	TeX_tm_G(char *buffer, char *pic, struct tm *T);	/* German  */
#else
long int oshtime();			/* Current time from 1970 in sec   */
#endif

/*=========================================================================
		Time functions
 *=========================================================================*/

#define time_1950	(-(((365*20)+5)*86400))	/* Time of  Jan. 1, 1950   */
#define time_2000	(10957*86400)	/* Time of 00:00:00 Jan. 1, 2000   */

#define tr_tT(t,T)	tr_itm(t,T)	/* Converts seconds to structure   */
#define tr_Tt(T,t)	tr_tmi(T,t)	/* Converts structure to seconds   */

#define tr_T(T)		tr_tm(T)	/* Modifies the time structure	   */


/*=========================================================================
		Time Acquisition / Editions
 *=========================================================================*/
 
#define tr_aT(str,l,T)	tr_atm(str,l,T)	/* Interprets a date / time	*/
#define ed_T(buf,pic,T)	ed_tm(buf,pic,T) /* Edit the date / time	*/

#endif
