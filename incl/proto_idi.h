/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2012 European Southern Observatory
.IDENTifer   proto_idi.h
.AUTHOR      C. Guirao IPG-ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for IDI interface.
.VERSION     1.0     23-Feb-1994   Creation by CG.

 120420         last modif
------------------------------------------------------------*/

#ifndef PROTO_IDI
#define PROTO_IDI 0

#ifdef  __cplusplus
extern "C" {
#endif


# include <xsysinclude.h>
#include <idistruct.h>


/* module idilocal1.c */

extern int Xwstinit(
int flg, char * screen, int no, int * fono, int minlut
#endif
);

extern void wr_lut(
#ifdef __STDC__
int dspno, struct lut_data *lut, int flag
#endif
);

extern void rd_lut(
#ifdef __STDC__
int dspno, struct lut_data *lut
#endif
);

extern int allo_mem(
#ifdef __STDC__
int dspno, struct mem_data *mem, int memid
#endif
);

extern int allo_zmem(
#ifdef __STDC__
int dspno, struct mem_data *mem, int memid
#endif
);

extern int get_hcopy(
#ifdef __STDC__
int dspno, struct mem_data *mem, int memid
#endif
);

extern void smv(
#ifdef __STDC__
int flag, int dspno, struct mem_data *mem, int memid,
               int srcx, int srcy, unsigned int xdim, unsigned int ydim,
               int dstx, int dsty
#endif
);

extern void int_enable(
#ifdef __STDC__
int dspno
#endif
);

extern void int_disable(
#ifdef __STDC__
int dspno
#endif
);

extern void exit_trg_enable(
#ifdef __STDC__
int dspno
#endif
);

extern void wait_int(
#ifdef __STDC__
int dspno, int *type, int *data, char *cdata, int *position
#endif
);

extern int trigsta(
#ifdef __STDC__
int dspno, int trgno, int event, int data, char *cbuf
#endif
);

extern int buildRGBLUT(
#ifdef __STDC__
int num, int lutlen, XColor xcolo[], unsigned long int *pxls
#endif
);

extern void LUTinstall(
#ifdef __STDC__
int dspno
#endif
);


/* module idilocal2.c */

extern int crea_win(
#ifdef __STDC__
int dspno, int xoff, int yoff, unsigned int xdim, unsigned int ydim, char dspchar
#endif
);

extern void cl_win(
#ifdef __STDC__
int dspno , int colval
#endif
);

extern void set_wcur(
#ifdef __STDC__
int dspno , int nz
#endif
);

extern void sendX(
#ifdef __STDC__
int dspno
#endif
);

extern int crealph(
#ifdef __STDC__
int dspno , int dysize
#endif
);

extern void clalph(
#ifdef __STDC__
int dspno , int flag , int x , int y , int dim
#endif
);

extern void alprfr(
#ifdef __STDC__
int dspno, struct alph_data *alph
#endif
);

extern void alptext(
#ifdef __STDC__
int flag, int dspno, struct alph_data *alph,
		    char *txt, int x0, int y0
#endif
);

extern int crelutbar(
#ifdef __STDC__
int dspno, struct lut_bar *bar
#endif
);

extern void vislutbar(
#ifdef __STDC__
int dspno, struct lut_bar *bar, int vis
#endif
);

extern void updalutbar(
#ifdef __STDC__
int dspno, struct lut_bar *bar
#endif
);

extern void get_string(
#ifdef __STDC__
int dspno, char *outbuf, int *lout
#endif
);

extern void get_char(
#ifdef __STDC__
int dspno, char *outbuf
#endif
);

extern void initgra(
#ifdef __STDC__
int dspno
#endif
);

extern void polyline(
#ifdef __STDC__
int dspno, int dysize, struct mem_data *mem, 
int col, int style, int *xs, int *ys, int np
#endif
);

extern void polyrefr(
#ifdef __STDC__
int dspno, struct mem_data *mem, int flag, int memid
#endif
);

extern void txtline(
#ifdef __STDC__
int dspno, int dysize, struct mem_data *mem, char txt[],
int x0, int y0, int path, int orient, int col, int size
#endif
);

extern void txtrefr(
#ifdef __STDC__
int dspno, struct mem_data *mem, int flag, int memid
#endif
);

extern void rd_pllut(
#ifdef __STDC__
int dspno, struct lut_data *plut
#endif
);

extern void do_misc(
#ifdef __STDC__
int dspno,int flag,int auxid, char *cbuf, int *ibuf, float *rbuf
#endif
);


/* module idilocal3.c */

extern void draw_curs(
#ifdef __STDC__
int dspno, int flag, int dysize, 
int curno, int xcur, int ycur, int cursh, int curcol
#endif
);

extern void draw_rroi(
#ifdef __STDC__
int dspno, int flag, int dysize, 
int x0, int y0, int  x1, int y1, int roicol
#endif
);

extern void draw_croi(
#ifdef __STDC__
int dspno, int flag, int dysize, 
int x0, int y0, int  r1, int r2, int r3, int roicol
#endif
);

extern int font_load(
#ifdef __STDC__
int flag,int no,int *fontno
#endif
);

extern void destroy(
#ifdef __STDC__
int dspno, char *stri
#endif
);

extern int exposed(
#ifdef __STDC__
int wstno, int dispno
#endif
);

extern int stopped(
#ifdef __STDC__
int dspno
#endif
);

extern void iconify(
#ifdef __STDC__
int dspno, int flag
#endif
);

extern void send_event(
#ifdef __STDC__
int dspno
#endif
);



/* module idilocal4.c */

extern int test_swap(
int value
#ifdef __STDC__
#endif
);

extern int get_plane_offset(
#ifdef __STDC__
unsigned int mask, unsigned int wordsize
#endif
);

extern unsigned char *idi_conv24_32(
#ifdef __STDC__
XImage *image, unsigned char *src
#endif
);
 
extern unsigned char * idi_conv32_24(
#ifdef __STDC__
XImage *image, unsigned char *src
#endif
);

extern unsigned char *
#ifdef __STDC__
idi_conv32_16(XImage *image, unsigned char *src, unsigned int wordsize
#endif
);

extern unsigned char * idi_conv16_32(
#ifdef __STDC__
XImage *image, unsigned char *src,
unsigned int wordsize
#endif
);

extern void idi_putimage(
#ifdef __STDC__
Display *display, Drawable draw, GC gc, XImage *image,
int sx, int sy, int dx, int dy, 
unsigned int width, unsigned int height
#endif
);

extern XImage *idi_getsubimage(
#ifdef __STDC__
Display *display, Drawable draw, int x, int y,
unsigned int width, unsigned int height,
unsigned long int plane_mask,int format,
XImage *dimage, int no
#endif
);




/* module idiutil1.c */

extern void wr_mem(
#ifdef __STDC__
int dspno, int RGBflag, struct mem_data *mem,
int x0, int y0, int ix, int iy, int const_flag,
unsigned char *data
#endif
);

extern void rd_mem(
#ifdef __STDC__
int dspno, int RGBflag, struct mem_data *mem, int hcopflag,
char *inpntr, int xoff, int yoff, int xsize, int ix, int iy, 
int ittf, unsigned char *data
#endif
);

extern void show_pix8(
#ifdef __STDC__
int dspno, int cursno, int x0, int y0, int x1, int y1,
unsigned char *data
#endif
);

extern void copy_mem(
#ifdef __STDC__
int dspno, struct mem_data *mema, int *offseta,
struct mem_data *memb, int *offsetb, int *xysize,
int zoomf
#endif
);

extern void copy_zmem(
#ifdef __STDC__
int dspno , struct mem_data *mema
#endif
);

extern void cp_zmem8(
#ifdef __STDC__
struct mem_data *mema, int offa, int xsiza, int offb, int *xysize
#endif
);

extern void cp_mem8(
#ifdef __STDC__
struct mem_data *mema, int offa, int xsiza,
struct mem_data *memb, int offb, int xsizb, int *xysize, int zoomf
#endif
);

extern void cp_mem16(
#ifdef __STDC__
struct mem_data *mema, int offa, int xsiz,
struct mem_data *memb, int offb, int xsizb, int *xysize, int zoomf
#endif
);

extern void cp_mem32(
#ifdef __STDC__
struct mem_data *mema, int offa, int xsiza,
                     struct mem_data *memb, int offb, int xsizb,
                     int *xysize, int zoomf
#endif
);

extern void zero_mem(
#ifdef __STDC__
int dspno, struct mem_data *mem, int zflag, int bck
#endif
);

extern void clmem(
#ifdef __STDC__
struct mem_data *mem
#endif
);


/* module idiutil2.c */

extern void clgraph(
#ifdef __STDC__
struct mem_data * mem
#endif
);

extern void loc_zero(
#ifdef __STDC__
int dspno
#endif
);

extern void loc_mod(
#ifdef __STDC__
int dspno, int locno, int lkey
#endif
);

extern void curmove(
#ifdef __STDC__
int dspno, int curno, int locno, int ldat, int *lpos
#endif
);

extern void roimove(
#ifdef __STDC__
int dspno, int locno, int *lpos
#endif
);

extern void roimodify(
#ifdef __STDC__
int dspno, int locno
#endif
);

extern void polyclear(
#ifdef __STDC__
int dspno, struct mem_data *mem, int *xs, int *ys, int np
#endif
);

extern void txtclear(
#ifdef __STDC__
int dspno, struct mem_data *mem, int xs, int ys, int np
#endif
);

extern void allrefr(
#ifdef __STDC__
int dspno, struct mem_data *mem, int memid, int flag
#endif
);

extern void rectrefr(
#ifdef __STDC__
int dspno, struct mem_data *mem, int memid
#endif
);

extern void copy_over(
#ifdef __STDC__
int dspno, int memid, int append
#endif
);


/* module iic.c */

extern int IICSCV_C(
#ifdef __STDC__
int display, int curn, int vis
#endif
);

extern int IICRCP_C(
#ifdef __STDC__
int display, int inmemid, int curn, int *xcur, int *ycur,int *outmemid
#endif
);

extern int IICWCP_C(
#ifdef __STDC__
int display, int memid, int curn, int xcur, int ycur
#endif
);

extern int IICINC_C(
#ifdef __STDC__
int display, int memid, int curn, int cursh, int curcol, int xcur, int ycur
#endif
);


/* module iid1.c */

extern void IIDINIT(
#ifdef __STDC__
void
#endif
);

extern int IIDOPN_C(
#ifdef __STDC__
char display[], int *displayid
#endif
);


/* module iid2.c */

extern int IIDCLO_C(
#ifdef __STDC__
int display
#endif
);

extern int IIDRST_C(
#ifdef __STDC__
int display
#endif
);

extern void waste_disp(
#ifdef __STDC__
int dspno
#endif
);

extern int IIDQDV_C(
#ifdef __STDC__
int display, int *nconf, int *xdev, int *ydev, 
int *depthdev, int *maxlutn, int *maxittn, int *maxcurn
#endif
);

extern int IIDQCI_C(
#ifdef __STDC__
int display, int devcap, int size, int capdata[], int *ncap
#endif
);

extern int IIDQCR_C(
#ifdef __STDC__
int display, int devcap, int size, float capdata[], int *ncap
#endif
);

extern int IIDQDC_C(
#ifdef __STDC__
int display, int confn, int memtyp, int maxmem, 
int *confmode, int mlist[], int mxsize[], int mysize[],
int mdepth[], int ittlen[], int *nmem
#endif
);

extern int IIDSDP_C(
#ifdef __STDC__
int display, int memlist[], int nmem, int lutflag[], int ittflag[]
#endif
);

extern int IIDSSS_C(
#ifdef __STDC__
int display, int memid[], int xoff[], int yoff[], int splitf, int splitx, int splity
#endif
);

extern int IIDSNP_C(
#ifdef __STDC__
int display, int colmode, int npixel, int xoff, int yoff,
int depth, int packf, unsigned char *cdata
#endif
);

extern int IIDICO_C (
#ifdef __STDC__
int display,int flag
#endif
);

extern int IIDDEL_C(
#ifdef __STDC__
char display[], int *nodels, int *imindx, int *grindx
#endif
);


/* module iie.c */

extern int IIEGDB_C(
#ifdef __STDC__
int display, int flag, int auxid, 
char *cbuf, int *ibuf, float*rbuf
#endif
);

extern int IIESDB_C(
#ifdef __STDC__
int display, int flag, int auxid, 
char *cbuf, int *ibuf, float*rbuf
#endif
);


/* module iig.c */

extern int IIGTXT_C(
#ifdef __STDC__
int display, int memid, char txt[], int x0, int y0,
int path, int orient, int color, int txtsize
#endif
);

extern int IIGPLY_C(
#ifdef __STDC__
int display, int memid, int *x, int *y, int np,
int color, int style
#endif
);

extern int IIGCPY_C (
#ifdef __STDC__
int display, int memid, int append
#endif
);



/* module iii.c */

extern int IIISTI_C(
#ifdef __STDC__
int display
#endif
);

extern int IIIEIW_C(
#ifdef __STDC__
int display , int trgstatus[10]
#endif
);

extern int IIIGLE_C(
#ifdef __STDC__
int display , int evalno , int * cbuf
#endif
);

extern int IIIGCE_C(
#ifdef __STDC__
int display , int evalno , char * cbuf
#endif
);

extern int IIIGSE_C(
#ifdef __STDC__
int display , int evalno , char * cbuf , int * lcbuf
#endif
);

extern int IIIGLD_C(
#ifdef __STDC__
int display , int locn , int * xdis , int * ydis
#endif
);

extern int IIIENI_C(
#ifdef __STDC__
int display , int intype , int intid , int objtype , int objid , int oper , int trigger
#endif
);


/* module iil.c */

extern int IILRIT_C(
#ifdef __STDC__
int display , int memid , int ittn , int ittstart , int ittlen , float ittdata[]
#endif
);

extern int IILWLT_C(
#ifdef __STDC__
int display , int lutn , int lutstart , int lutlen , float lutdata[]
#endif
);

extern int IILRLT_C(
#ifdef __STDC__
int display , int lutn , int lutstart , int lutlen , float lutdata[]
#endif
);

extern int IILSBV_C(
#ifdef __STDC__
int display , int memid , int vis
#endif
);

extern int IILWIT_C(
#ifdef __STDC__
int display , int memid , int ittn , int ittstart , int ittlen , float ittdata[]
#endif
);


/*
 * module iim.c
 */

extern int IIMWMY_C(
#ifdef __STDC__
int display , int memid , unsigned char * data , int npixel , int depth , 
int packf , int x0 , int y0
#endif
);

extern int IMRMY_C(
#ifdef __STDC__
int display , int memid , int npixel , int x0 , int y0 , int depth , 
int packf , int ittf , unsigned char * data
#endif
);

extern int IIMSMV_C(
#ifdef __STDC__
int display , int memlist[] , int nmem , int vis
#endif
);

extern int IIMCMY_C(
#ifdef __STDC__
int display , int memlist[] , int nmem , int bck
#endif
);

extern int IIMSLT_C(
#ifdef __STDC__
int display , int memid , int lutn , int ittn
#endif
);

extern int IIMBLM_C(
#ifdef __STDC__
int display , int memlst[] , int nmem , float period[]
#endif
);

extern int IIMCPY_C(
#ifdef __STDC__
int displaya , int memida , int * offseta , int displayb , int memidb , 
int * offsetb , int * xysize , int zoom
#endif
);

extern int IIMCPV_C(
#ifdef __STDC__
int displaya , int memida , int * offseta , int displayb , int memidb , 
int * offsetb , int * xysize , int zoom
#endif
);

extern int IIMSTW_C(
#ifdef __STDC__
int display , int memid , int loaddir , int xwdim , int ywdim , int depth , 
int xwoff , int ywoff
#endif
);


/*
 * module iir.c
 */

extern int IIRSRV_C(
#ifdef __STDC__
int display , int roiid , int vis
#endif
);

extern int IIRRRI_C(
#ifdef __STDC__
int display , int inmemid , int roiid , int * roixmin , int * roiymin , int * roixmax , 
int * roiymax , int * outmemid
#endif
);

extern int IIRWRI_C(
#ifdef __STDC__
int display , int memid , int roiid , int roixmin , int roiymin , 
int roixmax , int roiymax
#endif
);

extern int IICINR_C(
#ifdef __STDC__
int display , int memid , int roicol , int roixcen , int roiycen , 
int radiusi , int radiusm , int radiuso , int * roiid
#endif
);

extern int IICRRI_C(
#ifdef __STDC__
int display , int inmemid , int roiid , int * roixcen , int * roiycen , 
int * radiusi , int * radiusm , int * radiuso , int * outmemid
#endif
);

extern int IICWRI_C(
#ifdef __STDC__
int display , int memid , int roiid , int roixcen , int roiycen ,
 int radiusi , int radiusm , int radiuso
#endif
);

extern int IIRINR_C(
#ifdef __STDC__
int display , int memid , int roicol , int roixmin , int roiymin , int roixmax ,
 int roiymax , int * roiid
#endif
);


/* module iiz.c */

extern int IIZWZM_C(
#ifdef __STDC__
int display, int memlist[], int nmem, int zoom
#endif
);

extern int IIZRSZ_C(
#ifdef __STDC__
int display, int memid, int *xscr, int *yscr, int *zoom
#endif
);

extern int IIZWZP_C(
#ifdef __STDC__
int display, int xscr, int yscr, int zoom
#endif
);

extern int IIZRZP_C(
#ifdef __STDC__
int display, int *xscr, int *yscr, int *zoom
#endif
);

extern int IIZWSC_C(
#ifdef __STDC__
int display, int memlist[], int nmem, int xscr, int yscr
#endif
);

extern int IIZWSZ_C (
#ifdef __STDC__
int display, int memid, int xscr, int yscr, int zoom
#endif
);




/* module xwimg.c */

extern int XWIMG(
#ifdef __STDC__
int ldspno, int limch, char *frame, int *khelp, int loaddir,
int *npix, int *icen, float *cuts, int *scale
#endif
);


/* main module idiserv.c */

extern void SetAutoCursor(
#ifdef __STDC__
char *cc, char *midw
#endif
);

extern void iismrmy (
#ifdef __STDC__
   void
#endif
);

extern void iisdsnp (
#ifdef __STDC__
   void
#endif
);

#ifdef  __cplusplus
}
#endif


