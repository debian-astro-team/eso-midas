/* @(#)nrutil.h	19.1 (ESO-IPG) 02/25/03 14:19:15 */

/**********************************************************
* Error handling and memory allocation utilities
* Include file from numrec.c
*
* Edition History
*
* 06.05.93  initial test version             B.Schulz
* 04.02.98  wrong declarations!
*           change vector -> dvector, fvector -> vector
*           change matrix -> dmatrix, fmatrix -> matrix
*                                            S.Wolf (swolf@eso.org)
*
************************************************************/


/*==================================================
  Standard error handler for numerical recipes
==================================================*/

void nrerror();

/*==================================================
 Standard error handler
==================================================*/

void xerror();


/*==================================================
   Allocates a double vector with range [nl..nh]
==================================================*/

double *dvector(int, int);


/*==================================================
  Frees a double vector allocated by dvector()
==================================================*/

void free_dvector(double *, int, int);


/*==================================================
   Allocates a float vector with range [nl..nh]
==================================================*/

float *vector(int, int);
float *fvector(int, int);


/*==================================================
  Frees a float vector allocated by vector()
==================================================*/

void free_vector(float *, int, int);
void free_fvector(float *, int, int);


/*==================================================
   Allocates an int vector with range [nl..nh]
==================================================*/

int *ivector(int, int);

/*==================================================
  Frees an int vector allocated by ivector()
==================================================*/

void free_ivector(int *, int, int);


/*==================================================
   Allocates an short int vector with range [nl..nh]
==================================================*/

short int *sivector(int, int);


/*==================================================
  Frees an short int vector allocated by ivector()
==================================================*/

void free_sivector(short int *, int, int);


/*==================================================
   Allocates a double matrix
   with range [nrl..nrh][ncl..nch]
==================================================*/

double **dmatrix(int, int, int, int);


/*==================================================
   Frees a matrix allocated with dmatrix
==================================================*/

void free_dmatrix(double **, int, int, int, int);


/*==================================================
   Allocates a float matrix
   with range [nrl..nrh][ncl..nch]
==================================================*/

float **matrix(int, int, int, int);
float **fmatrix(int, int, int, int);

/*==================================================
   Frees a float matrix allocated with matrix
==================================================*/

void free_matrix(float **, int, int, int, int);
void free_fmatrix(float **, int, int, int, int);


/*==================================================
   Allocates an int matrix
   with range [nrl..nrh][ncl..nch]
==================================================*/
int **imatrix(int, int, int, int);

/*==================================================
   Frees a matrix allocated with imatrix
==================================================*/

void free_imatrix(int **, int, int, int, int);


/* EOF */
