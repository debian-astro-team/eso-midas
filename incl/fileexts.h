/* 
 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
      fileexts.h    contains the translations for the external refs
                    header files...
 
                    also the MIDAS definition file midas_def.h is included


                    K. Banse	880414, 010307, 030711, 090318
 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
*/


#include <filedef.h>

#include <catext.h>

#ifndef struct_DEF
#define struct_DEF
#include <fctext.h>
#include <fcbext.h>
#include <ldbext.h>
#endif

#include <keyext.h>
/*
#include <ndscext.h>
#include <zdscext.h>
*/
#include <ydscext.h>
#include <errext.h>

#include <midas_def.h>

#include <nullvals.h>


