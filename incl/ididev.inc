C @(#)ididev.inc	19.1 (ESO-IPG) 02/25/03 13:49:34
C 
C --------------------------  IDIDEV.INC  ------------------------------
C      
      INTEGER            DZDEV(26)
      INTEGER            QDSPNO,QDSZX,QDSZY,QDDEP
      INTEGER            QRGBFL,QJOYFL,QGRSEG,QALPNO
      INTEGER            QMSZX,QMSZY,QMDEP,QOVCH,QIMCH,QLSTCH
      INTEGER            QLUTSZ,IDINUM,QXAUX,QYAUX,QNOLUT
      INTEGER            ZDSPNO,QDZFND,GDSPNO
C      
      EQUIVALENCE      (DZDEV(1),QDSPNO),(DZDEV(2),QDSZX)
      EQUIVALENCE      (DZDEV(3),QDSZY),(DZDEV(4),QDDEP)
      EQUIVALENCE      (DZDEV(7),QRGBFL),(DZDEV(8),QJOYFL)
      EQUIVALENCE      (DZDEV(9),QGRSEG),(DZDEV(10),QALPNO)
      EQUIVALENCE      (DZDEV(11),QMSZX),(DZDEV(12),QMSZY)
      EQUIVALENCE      (DZDEV(13),QMDEP),(DZDEV(14),QOVCH)
      EQUIVALENCE      (DZDEV(15),QIMCH),(DZDEV(16),QLSTCH)
      EQUIVALENCE      (DZDEV(17),QLUTSZ),(DZDEV(18),IDINUM)
      EQUIVALENCE      (DZDEV(19),QXAUX),(DZDEV(20),QYAUX)
      EQUIVALENCE      (DZDEV(21),QNOLUT)
      EQUIVALENCE      (DZDEV(23),ZDSPNO),(DZDEV(24),QDZFND)
      EQUIVALENCE      (DZDEV(25),GDSPNO)
C
      COMMON      /IDDISP/ DZDEV
C 
C ------------------------
C 
C      QDSPNO    = display no. currently active display window
C      QDSZX     = size of display screen in x
C      QDSZY     = size of display screen in y
C      QDDEP     = depth of display
C      QRGBFL    = RGB/pseudo_colour mode
C      QJOYFL    = joystick/trackball flag
C      QGRSEG    = no. of graph. segments (only X11)
C      QALPNO    = flag for alphanumerics plane
C      QMSZX     = size of memory channel in x
C      QMSZY     = size of memory channel in y
C      QMDEP     = depth of memory channel
C      QOVCH     = current overlay channel
C      QIMCH     = current active image channel
C      QLSTCH    = last channel no.
C      QLUTSZ    = size of LUT (it's not always 2**QMDEP ...)
C      IDINUM  = number specifying which image display we currently use
C                -1 - the NULL device
C                 0 - Gould DeAnza IP8500
C                 1 - Ramtek
C                 2 - Sigma Args
C                10 - X-Windows version 10.4
C                11 - X-Windows version 11, from release 2 on
C      QXAUX     = size of zoom window in x (only for XWindow)
C      QYAUX     = size of zoom window in y 
C      QNOLUT    = no. of possible LUTs on device (only for XWindow)
C      DZDEV(22) = init flag (1 = after initialize/display)
C		   only used in procedures
C      ZDSPNO    = display no. of connected zoom window
C      QDZFND    = 1 if IIDOPN just opens, = 2 if IIDOPN creates the window
C      GDSPNO    = display no. of currently active graphics window
C  
C ------------------------
C 
C  The array DZDEV is saved in keyword IDIDEV,
C  IDINUM is set up in IIDOPN.
C
C      K. Banse      880708, 890615, 900601, 920117, 920207, 940111, 950126
C      ESO/IPG - Garching
C
C -----------------------------------------------------------------
