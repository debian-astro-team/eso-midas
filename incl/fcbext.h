/*

--------------------- FCB - Frame Control Block --------------------- 
	
on file MID_INCLUDE:fcbext.h
K. Banse	921113, 950711, 010307, 020312, 030109, 030812
	
120420		last modif
*/

	
#ifndef FCBEXT_DEF

#ifdef VERSION
#  undef VERSION
#endif


#define FCBEXT_DEF


#define YENTRY_SIZE 100		/* descr. entry length in descr.directory */


struct FCB_STRUCT
	{
	int		CLONY;
	char		BDTYPE[8];
	unsigned int	NDVAL;
	unsigned int	XNDVAL[2];	/* size_t for NDVAL */
	char		RESERV1[12];	/* spare */
	char 		SWPSHORT;
	char 		SWPINT;
	char 		FLOTFMT;
	char 		DSCFLAG;	/* flag for descr. updates */
	char		VERSION[8];	/* SCFINF has to be in sync! */
	int		PROT;
	unsigned int	DATAINFO[6];	/* basic data information as integer */
	int		INCARN;
	int		PTRLDB;
	int		LEXBDF;
	int		PEXBDF;
	int		ENDLDB;
	int		ENDLDB_OFF;
	int		NOLDB;		/* no. of LDBs */
	char 		RESERV2[18];	/* spare */
	short int	DIREXT;		/*  = 1500/2500/6000 bytes */
	int		DIRENTRY;	/*  = 100 bytes per entry */
	int	INLDB[2];	/* inital no. of LDBs  */
	char		RESERV3[8];	/* spare */
	unsigned int	XFITSINF1[2];	/* size_t for FITSINF1 */
	unsigned int	XFITSINF2[2];	/* size_t for FITSINF2 */
	int		DBEGIN;		/* initial size of dscdir */
	int		DFILLED;	/* increases in chunks of DIRENTRY */
	int		DSIZE;		/* increases in chunks of DIREXT */
        int             NOBYT;
        int             DFORMAT;
        int             PIXPBL;
        int             D1BLOCK;
        int             DLBLOCK;
	char		CREATE[28];
	int		CRETIM[2];	/* creation time in secs. */
	unsigned int	XDATINFO[12];	/* size_t for DATAINFO */
	char		RESERV4[224];	/* reserved for later use */
	int	FITSINF1;	/* Info for FITS files    */
	int	FITSINF2;
	int		NEXT;
	};
	
#endif			/* FCBEXT_DEF */


/*

CLONY   ...  = 1, if clones, = 0 if not				I*4	4
BDTYPE  ...  frame type: IMAGE, TABLE, FITFILE, ...		C*8     8
NDVAL   ...  Number of data values.				I*4     4
XNDVAL  ...  NDVAL as unsigned int array		     2* I*4     8
RESERV1 ...  FCB space reserved for future use 		        C*12   12
SWPSHORT     set according to <computer.h>                      C*1     1
		'=' for SWAPSHORT = 12, 's' for SWAPSHORT = 21
SWPINT       set according to <computer.h>                      C*1     1
		'=' for SWAPINT = 1234, 's' for SWAPINT = 4321
		'h' for SWAPINT = 2143, 'w' for SWAPINT = 3412
FLOTFMT      set according to <computer.h>                      C*1     1
		'=' for FLOATFMT = IEEEFLOAT
		'V' for FLOATFMT = VAXFLOAT
		'G' for FLOATFMT = VAXGFLOAT
		'H' for FLOATFMT = HPFLOAT
DSCFLAG ...  flag to indicate which version of the descriptor stuff 
	     is used 						C*1	1
		'Z' large format (old)
		'Y' new name+help format  
VERSION ...  Version in ASCII					C*8     8
		01FEB - VERS_010 (Z)
		01SEP - VERS_100 (Y)
		02FEB - VERS_100 (Y)
		02SEP - VERS_101 (Y)
		03FEB - VERS_105 (Y)
		06SEP - VERS_110 (Y)
		09SEP - VERS_120 (Y)
PROT    ...  protection flag                			I*4     4
DATAINFO ..  basic data info available outside Midas	     6* I*4    24
[0] 	     Naxis
[1] 	     Npix[0]
[2] 	     Npix[1]
[3] 	     Npix[2]
[4] 	     data format, D_R4_FORMAT, etc.
[5] 	     first byte where pixel values are stored
	     (counting begins at 0, like in C)
INCARN  ...  Number of incarnations in frame - initialized to 1	I*4     4
             currently not used
PTRLDB  ...  Pointer to 1. Local Descriptor Block.		I*4     4
LEXBDF  ...  Logical Extension of BDF (last block)		I*4     4
PEXBDF  ...  Physical Extension of BDF (last block)		I*4     4
ENDLDB  ...  no. of last used LDB                               I*4     4
ENDLDB_OFF   first free word in block                           I*4     4
NOLDB   ...  no. of LDBs (of 2048 bytes)			I*4     4
RESERV2 ...  spare  						C*18   18 
DIREXT  ...  length of each extension of descriptor directory 
             currently = 6000 (in chars.)	                I*2     2
DIRENTRY     length of single entry in descr. directory 
             currently 100 (in chars.)	                        I*4     4
INLDB   ...  Initial no. of LDBs for dscdir + descr data    (2) I*4     8
RESERV3 ...  FCB space reserved for future use.		        C*8     8
XFITSINF1 .  No. of pixels in original FITS file accessed as size_t 
							     2* I*4     8
XFITSINF2 .  pixel offset in original FITS file accessed as size_t 
							     2* I*4     8
DBEGIN  ...  inital size of dsc_directory 		 	I*4     4
DFILLED ...  length of descriptor directory currently in use 	I*4     4
	     increases in chunks of DIRENTRY bytes
DSIZE   ...  no. of allocated elements in descr. directory,  
	     initialized to min. no. of DIREXTs to hold space for 
	     (100 + 1) descrs + 3 LDB control ints (= 10112 bytes)
	     => initial value = 12000 (= 120 entries)		I*4     4
NOBYTE  ...  no. of bytes per data value			I*4	4
DFORMAT ...  format of data values				I*4	4
	     (see the different D_xx_FORMAT in midas_def.h)
PIXPBL  ...  no. of data values per page/block			I*4	4
D1BLOCK	     no. of first block of data                         I*4     4
DLBLOCK	     no. of last block of data                          I*4     4
CREATE  ...  Creation time in ASCII				C*28   28
CRETIM  ...  Creation time in seconds (as long int)	    (2) I*4     8
XDATINFO ..  DATAINFO as unsigned int array		     12 I*4    48
[0] 	     Naxis
[1] 	     unused
[2,3] 	     Npix[0]
[4,5] 	     Npix[1]
[6,7] 	     Npix[2]
[8] 	     data format, D_R4_FORMAT, etc.
[9] 	     unused
[10,11]	     first byte where pixel values are stored
	     (counting begins at 0, like in C)
RESERV4 ...  spare					        C*224 224
FITSINF1 ... REFVAL, no. of pixels in original FITS file	I*4     4
FITSINF2 ... OFFSET, offset of data in FITS file		I*4     4
NEXT    ...  Next Frame Control Block.				I*4     4
                                                                     -----
                                                                      512
*/
