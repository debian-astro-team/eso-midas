/* @(#)error.h	19.1 (ESO-IPG) 02/25/03 13:49:28 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		error.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO], Alan Richmond [ST-ECF].
.CATEGORY	Error Handling
.COMMENTS	Edition / Retrieval of errors.
.ENVIRONMENT
.VERSION 1.0 	16-Jun-1987: Francois Ochsenbein: extracted from STESODEF.
.VERSION 1.1 	08-Dec-1988: Added ERR_STR2
.VERSION 1.2 	14-Jul-1989: Changed ERR_ED_STRING to allow NULL pointers
--------------------------------------*/

#ifndef	ERROR_DEF
#define ERROR_DEF	0

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

#define _ERROR_      	0		/* Error in tracing   	*/
#define _WARNING_   	1 		/* Warning level   	*/

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
			/* Notes: subr, level and class unused */
int	eh_state	(void);
int 	eh_put 		(char *subr, char *message, int class);
int 	eh_put1 	(char *message);
int 	eh_put2 	(char *subr, char *message, int class, int lmsg);
char 	*eh_loc 	(int level, char *subr, int class);
int 	eh_clear 	(int level);
int 	eh_ed_str2	(char *text, char *message, int message_length);
int 	eh_ed_i 	(char *text, int value);
#else
char 	*eh_loc();
#endif

/*===========================================================================
 *			Passing error messages
 *===========================================================================*/

#define ERR_TEXT	ERROR	/* Pass an error message to error handler   */
#define ERR_STRING	ERROR   /* Pass an error message to error handler   */

#define ERR_STR2(m,l)	eh_put2((char *)0, m, _ERROR_, l)
#define ERROR(message)	eh_put1(message)	 \
			/* Pass an error message to error handler 	*/

#define WARNING(message)	eh_put((char *)0, message, _WARNING_)  \
			/* Pass a warning message to error handler 	*/

#define ERR_ED_TEXT(t,s) 	eh_ed_str2(t,s,sizeof(s)-1)		\
			/* Pass an error text followed by a string */
#define ERR_ED_STRING(t,s)	eh_ed_as(t,s)		\
			/* Pass an error text followed by a string */
#define ERR_ED_STR2(t,s,l)	eh_ed_str2(t,s,l)			\
			/* Pass an error text followed by a string */
#define ERR_ED_I(t,i)		eh_ed_i   (t,i)				\
			/* Pass an error text followed by a number */

/*===========================================================================
 *			Retrieving error messages
 *===========================================================================*/

#define ERR_GET()		ERR_LOC()
#define ERR_LOC()		eh_loc(0, (char *)0, 0)		\
					/* Retrieve last error	*/
#define ERR_CLEAR()		eh_clear(0)			\
					/* Clear Error Stack	*/
#define ERR_STATE()		eh_state()			\
					/* TRUE or FALSE	*/
#endif
