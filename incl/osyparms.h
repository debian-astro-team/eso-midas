/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE  C
.IDENTIFICATION  FSY_H
.AUTHOR  Benoit Pirenne  [ESO_IPG]
.KEYWORDS  fsy parameters
.ENVIRONMENT independant from any operating system or compiler.
.COMMENTS
  header file to contain the different parameters necessary to 
     run the OSY and FSY routines.
.VERSION  0.1  15-Dec-1986   B. Pirenne
 040119		last modif

-------------------------------------------------------------------*/

#ifndef  OSYPARMS_DEF 

#define  OSYPARMS_DEF      1    /* Indicates inclusion of OSYPARMS header */

#include   <stdio.h>
#define    DEVNAM       0
#define    OUR_BLOCK_SIZE   512



#endif
