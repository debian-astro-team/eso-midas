/* @(#)aglmacro.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:24   */
/*
 * HEADER : aglmacro.h     - Vers 3.6.000  - Dec 1991 -  L. Fini, OAA
 */




/*********************************           *********************************/
/*                            AGL MACROS DEFINITION                          */

#define MAX(x,y) ((x)>(y)?(x):(y))
#define MIN(x,y) ((x)<(y)?(x):(y))

                                       /* Error management definitions      */

#define DY(x) ((AGL_status.vwpoint)->x)

#define XVWLOW (AGL_status.vwpoint)->vwprt.xlower
#define XVWUP (AGL_status.vwpoint)->vwprt.xupper
#define YVWLOW (AGL_status.vwpoint)->vwprt.ylower
#define YVWUP (AGL_status.vwpoint)->vwprt.yupper
#define VWPLIMS &((AGL_status.vwpoint)->vwprt)

#define XCLOW (AGL_status.vwpoint)->clpng.xlower
#define XCLUP (AGL_status.vwpoint)->clpng.xupper
#define YCLOW (AGL_status.vwpoint)->clpng.ylower
#define YCLUP (AGL_status.vwpoint)->clpng.yupper
#define CLPLIMS &((AGL_status.vwpoint)->clpng)

#define YXRATIO RATYX(DY(devidx))*(YCLUP-YCLOW)/(XCLUP-XCLOW)

#define XSVLOW (AGL_status.vwpoint)->saved.xlower
#define XSVUP (AGL_status.vwpoint)->saved.xupper
#define YSVLOW (AGL_status.vwpoint)->saved.ylower
#define YSVUP (AGL_status.vwpoint)->saved.yupper

#define XUSLOW (AGL_status.vwpoint)->usrwn.xlower
#define XUSUP (AGL_status.vwpoint)->usrwn.xupper
#define YUSLOW (AGL_status.vwpoint)->usrwn.ylower
#define YUSUP (AGL_status.vwpoint)->usrwn.yupper

#define GRCHAN(z) ((AGL_status.actidx[z]).grchan)
#define DRVADDR(z) ((AGL_status.actidx[z]).drvaddr)
#define DEVNAM(z) ((AGL_status.actidx[z]).devnam)
#define FILNAM(z) ((AGL_status.actidx[z]).filnam)
#define DEVCOMMND(z) ((AGL_status.actidx[z]).devcmd)
#define ACTREQST(z) ((AGL_status.actidx[z]).actreqst)
#define FLAGS(z) ((AGL_status.actidx[z]).flags)
#define NPLANS(z) ((AGL_status.actidx[z]).nplans)
#define MAXLWIDTH(z) ((AGL_status.actidx[z]).maxlwid)

#define AGLBLACK(z) 	((AGL_status.actidx[z]).colors[1])
#define AGLRED(z) 	((AGL_status.actidx[z]).colors[2])
#define AGLGREEN(z) 	((AGL_status.actidx[z]).colors[3])
#define AGLBLUE(z) 	((AGL_status.actidx[z]).colors[4])
#define AGLYELLOW(z) 	((AGL_status.actidx[z]).colors[5])
#define AGLMAGENTA(z) 	((AGL_status.actidx[z]).colors[6])
#define AGLCYAN(z) 	((AGL_status.actidx[z]).colors[7])
#define AGLWHITE(z) 	((AGL_status.actidx[z]).colors[8])

#define AGLCOLOR(z,c) ((AGL_status.actidx[z]).colors[c])

#define DEFFOREGR(z) ((AGL_status.actidx[z]).defforegr)
#define DEFBACKGR(z) ((AGL_status.actidx[z]).defbackgr)
#define CURBACKGR(z) ((AGL_status.actidx[z]).curbackgr)
#define XLENG(z) ((AGL_status.actidx[z]).xleng)
#define XPIXELS(z) ((AGL_status.actidx[z]).xpixels)
#define YPIXELS(z) ((AGL_status.actidx[z]).ypixels)
#define YLENG(z) ((AGL_status.actidx[z]).yleng)
#define XMAXL(z) ((AGL_status.actidx[z]).xmaxl)
#define YMAXL(z) ((AGL_status.actidx[z]).ymaxl)
#define XCURL(z) ((AGL_status.actidx[z]).xcurl)
#define YCURL(z) ((AGL_status.actidx[z]).ycurl)
#define RSLX(z) ((AGL_status.actidx[z]).rslx)
#define RSLY(z) ((AGL_status.actidx[z]).rsly)
#define RATYX(z) ((AGL_status.actidx[z]).ratyx)
#define RATXY(z) ((AGL_status.actidx[z]).ratxy)
#define DOTLEN(z) ((AGL_status.actidx[z]).dotlen)
#define CHARMULT(z) ((AGL_status.actidx[z]).charmult)

#define AGLDVCOM   (&cmbuffr)
#define DEFINE_AGLDVCOM struct bufcom cmbuffr

#define CURSFUNCT  (*(AGL_status.CURS_POINTER))
#define ERASFUNCT  (*(AGL_status.ERAS_POINTER))
#define ESCPFUNCT  (*(AGL_status.ESCP_POINTER))
#define INITFUNCT  (*(AGL_status.INIT_POINTER))
#define SENDFUNCT  (*(AGL_status.SEND_POINTER))
#define TERMFUNCT  (*(AGL_status.TERM_POINTER))
#define POLYFUNCT  (*(AGL_status.POLY_POINTER))



#define STATU(x,y) ((x<XCLOW)?1:((x>XCLUP)?2:0))|((y<YCLOW)?4:((y>YCLUP)?8:0))

#define CNVRT_BEGIN for(;;) { 

#define CNVRT_LOGX(retval,aux_x,aux_y) \
if(AGL_status.xlogon){\
	if(aux_x<=0.0){\
		AGL_status.errstat=NOLOGERR;\
		retval=(-1);break;\
	}\
	aux_x=log(aux_x);\
} 

#define CNVRT_LOGY(retval,aux_x,aux_y) \
if(AGL_status.ylogon){\
	if(aux_y<=0.0){\
		AGL_status.errstat=NOLOGERR;\
		retval=(-1);\
		break;\
	}\
	aux_y=log(aux_y);\
}

#define CNVRT_USRT0(retval,aux_x,aux_y) \
if(AGL_status.UTransf){\
	retval=(*DY(UsrTransf))(&(aux_x),&(aux_y));\
	aux_x=(aux_x*AGL_status.aglfcx)+AGL_status.aglofx;\
	aux_y=(aux_y*AGL_status.aglfcy)+AGL_status.aglofy;

#define CNVRT_USRT1(retval,aux_x,aux_y) \
	if(retval==(-1))\
		AGL_status.errstat=USRTRSERR;\
} else {

#define CNVRT_USER0(retval,aux_x,aux_y) \
	aux_x=(aux_x*AGL_status.aglfcx)+AGL_status.aglofx;\
	aux_y=(aux_y*AGL_status.aglfcy)+AGL_status.aglofy;

#define CNVRT_USER1(retval,aux_x,aux_y) \
	retval=STATU(aux_x,aux_y);\
}\
break;\
}
