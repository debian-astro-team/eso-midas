/* @(#)aglenvr.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:23   */
/*
 * HEADER : aglenvr.h      - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA
 */


/*                 Includes all the environment definitions for AGL   */

#include <aglsys.h>


#include <aglparam.h>
#include <aglmacro.h>
#include <agldcaps.h>
#include <agldcom.h>
#include <aglstruc.h>
#include <aglerror.h>

#include <aglproto.h>
#include <agl.h>
