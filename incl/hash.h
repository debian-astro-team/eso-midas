/* @(#)hash.h	19.1 (ESO-IPG) 02/25/03 13:49:34 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE 		Header
.NAME 		hash.h
.VERSION 1.0	18-Apr-1988: Creation
.LANGUAGE 	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	General Hash-Table Definition
.COMMENTS 	A hash table is described by a header.
.ENVIRONMENT	Any
------------------------------------------------------------*/

#ifndef HASH_DEF
#define HASH_DEF	0

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

/*===========================================================================
 *             Structures
 *===========================================================================*/

typedef struct s_h_item{
	struct s_h_item *next;		/* Next definition in chain	*/
	int leq;			/* Length of Equivalence string */
	unsigned char ls;		/* Length of symbol		*/
	char strings[2];		/* Strings terminated with EOS	*/
	} H_ITEM;

typedef struct {
	int size;			/* Number of items in table	*/
	int symbols;			/* Number of symbols		*/
	int collisions;			/* Number of collisions		*/
	H_ITEM *start[1];		/* Starting points of chains	*/
	} H_TABLE;

#if _TEMPLATES_
/*===========================================================================
 *             Related functions and macros
 *===========================================================================*/
int 	 h_factor	(int n);	/* Hash factor (2 is default)	*/
H_TABLE *h_create	(int size);	/* Size is adjusted to prime	*/
int 	 h_clear	(H_TABLE *ht);	/* Make table empty		*/
int 	 h_log		(H_TABLE *ht);	/* Log the table contents	*/
H_ITEM 	*h_look		(H_TABLE *ht, char *symbol, int len);
H_ITEM 	*h_add		(H_TABLE *ht, char *symbol, int ls, char *eq, int leq);
int 	 h_remove	(H_TABLE *ht, char *symbol, int len);
char 	*h_get		(H_TABLE *ht, char *symbol, int len);

#else
H_TABLE *h_create();
H_ITEM  *h_add(), *h_look();	
char    *h_get();
#endif

#define H_Create(size)		h_create(size)
#define H_Clear(ht)		h_clear(ht)
#define H_Lookup(ht,s)		h_look(ht,s,strlen(s))
#define H_Gets(ht,s)		h_get(ht,s,strlen(s))
#define H_Add(ht,s,eq)		h_add(ht,s,strlen(s),eq,strlen(eq))
#define H_Remove(ht,s)		h_remove(ht,s,strlen(s))

#endif
