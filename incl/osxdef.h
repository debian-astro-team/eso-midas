/*++++++++++++++++++++++++++ midas/osparms.h ++++++++++++++++++++++++++++
.TYPE           Header
.LANGUAGE       C
.IDENTIFICATION osxdef.h
.AUTHOR         Carlog Guirao [ESO-SDAG]
.KEYWORDS       Interprocces communications.
.ENVIRONMENT    Unix
.VERSION 1.1 	6.6.94:	Implementation

 060406		last modif
--------------------------------------------------------------------------*/
#ifndef  OSX_DEF 
#define  OSX_DEF          /* Indicates inclusion of OSX_DEF header */

#include <oserror.h>

#define  MAX_IPCC     32        /* maximum number of IPC channels in use */

/* open modes : */

#define  LOCAL        00	/* The call to the osxopen interface */
#define  NETW         02	/* with a combination of two of these */
#define  IPC_READ     00	/* switches allows the choice of the */
#define  IPC_WRITE    01	/* right open mode. i.e. LOCAL | SOCK_READ */
#define  CLIENT       04	/* special flag for client open (no priv.) */

/* queue lengths : */

#define  LOCAL_QUEUE_LENGTH  5	/* queue length on local proccesses */
#define  NETW_QUEUE_LENGTH   5	/* queue length on network proccesses */

/* returned values from osxinfo */

#define NODATA  0
#define DATARDY 1
#define NOCONN  2

/* communication channels status : */

#define  WAIT_ON_WRITE       0
#define  WAIT_ON_READ        1
#define  OPENED_READ         2
#define  OPENED_WRITE        3
#define  CLOSED              4
#define  IDLE                5

/* interprocess communication structure */

struct  ipccstat {		/* structure socket-status */
    char  *chname;		/* channel name */
    char  *phname;		/* second name/host name */
    int   omode;		/* open mode (either IPC_READ or IPC_WRITE) */
    int   type;			/* LOCAL or NETW */
    int   status;               /* status of the channel */
    int   accept;		/* descriptor returned by accept */
};

#endif /* OSX_DEF */
