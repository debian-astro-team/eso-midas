/* @(#)tex.h	19.1 (ESO-IPG) 02/25/03 13:49:43 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE 		Header
.NAME 		tex.h
.LANGUAGE 	C
.AUTHOR		Francois Ochsenbein [ESO]
.CATEGORY	TeX-like scan
.COMMENTS 	Definition of structures for scanning
		a ``TeX-like'' text.
.ENVIRONMENT	
.VERSION 1.0	20-Apr-1988: Creation
.VERSION 1.1	20-Nov-1990: Added obeylines / obeyspaces
------------------------------------------------------------*/

#ifndef TEX_DEF

#define TEX_DEF		0

#include <hash.h>	/* Hash table is used	*/

#ifndef _TEMPLATES_
#include <compiler.h>
#endif


/*===========================================================================
 *             Structures
 *===========================================================================*/

#define TeX_BUF	40			/* Normally, only one word... 	*/

typedef struct {
	H_TABLE *macros;		/* Stored macros		*/
	int	(*output)();		/* Output function		*/
	int	(*action)();		/* Action function		*/
	char	*ap;			/* Parameter address		*/
	char	mode;			/* Verbatim mode flag		*/
#define _TeX_BEGIN_	'\01'		/* New Environment		*/
#define _TeX_END_	'\02'		/* End Environment		*/
#define _TeX_ACTION_	'\03'		/* Triggers action = next byte	*/
#define _TeX_ENDACTION_	'\04'		/* End-of-action byte		*/
#define _TeX_VERBATIM_	'\05'		/* Inhibate substitutions	*/
#define _TeX_DEF_	'\06'		/* Definition flag		*/
#define _TeX_INCLUDE_	'\07'		/* Include a file		*/
#define _TeX_IF_	'\021'		/* Basic tests			*/
#define _TeX_LEN_	'\022'		/* Length of a text		*/
#define _TeX_TIME_	'\023'		/* Date / Time editions		*/
#define _TeX_obeylines	'\101'		/* Keep original line breaks	*/
#define _TeX_obeyspaces	'\103'		/* Keep original tabs / spaces	*/
#define _TeX_OBEY_	0100
	unsigned char	flags;		/* Defined by user		*/
	short int nbuf;			/* Number of chars in out_buf	*/
	char	out_buf[TeX_BUF];
	} TeX;

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
int 	tex_exec	(TeX *htex, char *str, int len);
int 	tex_mexec	(TeX *htex, char **str, int nstrings);
int 	tex_tell	(void);
int 	tex_getvparm	(int param_no);		/* Verbatim parameter	*/
int 	tex_getparm	(int param_no);		/* Processed parameter	*/
int 	tex_unit	(char *str, int len);	/* Length of {....}	*/
int 	tex_input	(TeX *htex, char *filename, int opt);
int 	tex_load	(TeX *htex, int fid, int len, int opt);
int 	tex_list	(TeX *htex);		/* List Macros		*/
char	*tex_symbol	(char *str);		/* Conversion of a Symbol */
#else
char	*tex_symbol();
#endif

/*===========================================================================
 *             Related functions and macros
 *===========================================================================*/

#define TeX_Execute(hTeX,s,l)		tex_exec(hTeX,s,l)
#define TeX_Continue(hTeX)		tex_exec(hTeX,(char *)0, 0)

#define TeX_ExecuteArray(hTeX,ss,n)	tex_mexec(hTeX,ss,n)

#define TeX_Load(hTeX,fid,bytes)	tex_load(hTeX,fid, bytes,0)
#define TeX_Include(hTeX,fname)		tex_input(hTeX,fname,0)

#endif
