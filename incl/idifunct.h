/* @(#)idifunct.h	19.1 (ESO-IPG) 02/25/03 13:49:34 */

/******************************************************************************
  Include file 'idifunct.h' for the IDI clients and IDI server.
  Contains the function codes needed by the IDI routines.
  910606, 910909, 930112, 940201
******************************************************************************/



/*** Definition of IDI functions codes ***/

#define     DOPN_CODE    1
#define     DCLO_CODE    2
#define     DICO_CODE   81		/* added */
#define     DDEL_CODE   92		/* added */
#define     DRST_CODE    3
#define     DUPD_CODE    4
#define     DERR_CODE    5
#define     DQDV_CODE    6
#define     DQCI_CODE    7
#define     DQCR_CODE    8
#define     DQDC_CODE    9
#define     DSEL_CODE   10 
#define     MSMV_CODE   11 
#define     ZWSC_CODE   12 
#define     ZWZM_CODE   13 
#define     ZRSZ_CODE   14 
#define     MSLT_CODE   15 
#define     DSDP_CODE   16 
#define     MWMY_CODE   17 
#define     MCMY_CODE   18 
#define     MCPY_CODE   91 		/* added */
#define     MCPV_CODE   93 		/* added */
#define     MRMY_CODE   19 
#define     MSTW_CODE   20 
#define     GPLY_CODE   21 
#define     GTXT_CODE   22 
#define     GCPY_CODE   80		/* added */
#define     LWIT_CODE   23 
#define     LRIT_CODE   24 
#define     LWLT_CODE   25 
#define     LRLT_CODE   26 
#define     ZWSZ_CODE   27 		/* changed function */
#define     CINC_CODE   29 
#define     CSCV_CODE   30 
#define     CRCP_CODE   31 
#define     CWCP_CODE   32 
#define     RINR_CODE   33 
#define     RRRI_CODE   34 
#define     RWRI_CODE   35 
#define     RSRV_CODE   36 
#define     IENI_CODE   37 
#define     IEIW_CODE   38 
#define     ISTI_CODE   39 
#define     IQID_CODE   40 
#define     IGLD_CODE   41 
#define     IGIE_CODE   42 
#define     IGRE_CODE   43 
#define     IGSE_CODE   44 
#define     IGCE_CODE   90 
#define     IGLE_CODE   45
#define     DSNP_CODE   46 
#define     MBLM_CODE   47 
#define     DSSS_CODE   48 
#define     LSBV_CODE   49 
#define     DIAG_CODE   50 
#define     DENC_CODE   51 
#define     DAMY_CODE   52 
#define     DSTC_CODE   53
#define     DRLC_CODE   54 
#define     ESDB_CODE   55 		/* new Escape routines */
#define     EGDB_CODE   56
#define     MEBM_CODE   57
#define     CINR_CODE   58              /* added */
#define     CRRI_CODE   59              /* added */
#define     CWRI_CODE   60              /* added */

#define     XWIM_CODE   94 		/* added */
#define     XQMI_CODE   95 		/* added */
#define     SSIN_CODE   96 		/* added */
