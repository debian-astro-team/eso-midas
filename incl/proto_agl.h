/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 2009-2012 European Southern Observatory
.IDENTifer   proto_agl.h
.AUTHOR      K. Banse   ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for Low & High level AGL Interfaces

.ENVIRONment none

.VERSION     1.0     090407     creation

 120123		last modif
------------------------------------------------------------*/

#ifndef  PROTO_AGL               /* Avoid redefinitions */
#define  PROTO_AGL       0


#ifdef  __cplusplus
extern "C" {
#endif

extern void AGL_reset(
#ifdef __STDC__
	FILE  *fp
#endif
);

extern void AGL_setraw(
#ifdef __STDC__
	FILE  *fp
#endif
);

extern FILE *AGL_fopen(
#ifdef __STDC__
	char  *fname
#endif
);

extern void AG_CDEF(
#ifdef __STDC__
	double  x1,
	double  x2,
	double  y1,
	double  y2
#endif
);

extern void AG_DMSG(
#ifdef __STDC__
	char  *m1,
	char  *m2
#endif
);

extern void AG_CLS();

extern void AG_MCLS();

extern void AG_MOPN(
#ifdef __STDC__
	char  *fname
#endif
);

extern void AG_MRDW(
#ifdef __STDC__
	char  *fname
#endif
);

extern void AG_MRES();

extern void AG_MSUS();

extern void AG_ESC(
#ifdef __STDC__
	char  *cmd,
	int  cmdlen
#endif
);

extern void AG_VERS();

extern char *AG_GETS(
#ifdef __STDC__
	char  *buf,
	int  len,
	FILE  *fp
#endif
);
 
extern char *AG_GETN(
#ifdef __STDC__
	char  *buf,
	int  len,
	FILE  *fp,
	int  *nlin
#endif
);
 
extern char *AG_IDN();
 
extern char *AG_SCAN(
#ifdef __STDC__
	char  *chstrg,
	int   delim,
	int   tklen,
	char *token
#endif
);
 
extern FILE *AG_STDO(
#ifdef __STDC__
	char  *fname,
	char  *ext,
	int  flag
#endif
);
 
extern void AG_NEWN(
#ifdef __STDC__
	char  *templa
#endif
);
 
extern void AG_SSET(
#ifdef __STDC__
	char  *tbuf
#endif
);

extern int AG_VDEF (
#ifdef __STDC__
	char  *device,
	double xa,
	double xb,
	double ya,
	double yb,
	double xlim,
	double ylim
#endif
);

extern void AG_AXIS (
#ifdef __STDC__
	int  type,
	float *data,
	double lspace,
	char  *format,
	char  *label
#endif
);

extern void AG_NLIN (
#ifdef __STDC__
	double xa,
	double xb,
	double ya,
	double yb,
	char *select
#endif
);

extern void AG_AXES (
#ifdef __STDC__
	double xa,
	double xb,
	double ya,
	double yb,
	char *option
#endif
);

extern void AG_ORAX (
#ifdef __STDC__
	int  flags,
	float *ends,
	float *data,
	char *form,
	char *label
#endif
);

extern void AG_HIST (
#ifdef __STDC__
	float *xv,
	float *yv,
	int  np,
	int  mode,
	int join
#endif
);

extern void AG_VKIL();

extern void AG_VUPD();

extern void AG_DRIV();

extern void AG_VSEL(
#ifdef __STDC__
	int  id
#endif
);

extern void AG_WDEF(
#ifdef __STDC__
	double  x1,
	double  x2,
	double  y1,
	double  y2
#endif
);

extern void AG_GPLG(
#ifdef __STDC__
	float  *xv,
	float  *yv,
	int  np
#endif
);

extern void AG_GPLL(
#ifdef __STDC__
	float  *xv,
	float  *yv,
	int  np
#endif
);

extern void AG_GPLM(
#ifdef __STDC__
	float  *xv,
	float  *yv,
	int  np,
	int  mark
#endif
);

extern void AG_FILL(
#ifdef __STDC__
	float  *x,
	float  *y,
	int  n,
	double space,
	double angle,
	char  *ss
#endif
);

extern void AG_GINT(
#ifdef __STDC__
	float  *xv,
	float  *yv,
	int  np
#endif
);

extern void AG_GTXT(
#ifdef __STDC__
	double  xc,
	double  yc,
	char  *text,
	int  center
#endif
);

extern void AG_GERR(
#ifdef __STDC__
	int  code,
	char  *msg
#endif
);

extern void AG_ISET(
#ifdef __STDC__
	int  number,
	int  *values
#endif
);

extern void AG_RSET(
#ifdef __STDC__
	int  number,
	float  *values
#endif
);

extern int AG_RGET(
#ifdef __STDC__
	char  *item,
	float  *values
#endif
);

extern int AG_IGET(
#ifdef __STDC__
	char  *item,
	int  *values
#endif
);

extern int AG_IVAL(
#ifdef __STDC__
	char  *chstrg,
	int   nvals,
	int  *values
#endif
);

extern int AG_RVAL(
#ifdef __STDC__
	char  *chstrg,
	int   nvals,
	float  *values
#endif
);

extern int AG_SVAL(
#ifdef __STDC__
	char  *in,
	int   nvals,
	char  *out
#endif
);

extern void AG_TRNS(
#ifdef __STDC__
	void (*myinit)(),
	int  (*mytransf)(),
	int  (*myrtransf)()
#endif
);

extern void AG_TGET(
#ifdef __STDC__
	char  *text,
	float  *xd,
	float  *yd
#endif
);

extern void AG_VN2U(
#ifdef __STDC__
	double  xpn,
	double  ypn,
	float  *xu,
	float  *yu
#endif
);

extern void AG_VU2N(
#ifdef __STDC__
	double  xu,
	double  yu,
	float  *xpn,
	float  *ypn
#endif
);

extern void AG_VLOC(
#ifdef __STDC__
	float  *xv,
	float  *yv,
	int *key,
	int *pixval
#endif
);

extern void AG_VLOS(
#ifdef __STDC__
	float  *xv,
	float  *yv,
	int  maxlen,
	char  *chstr,
	int *pixval
#endif
);

extern void AG_MAGN(
#ifdef __STDC__
	double  xf,
	double  yf,
	float  *xv,
	float  *yv,
	int  np
#endif
);

extern void AG_TROT(
#ifdef __STDC__
	float  *xv,
	float  *yv,
	int  np
#endif
);

extern void AG_TSET(
#ifdef __STDC__
	float  xoff,
	float  yoff,
	float  angle,
	int  item
#endif
);




#ifdef  __cplusplus
}
#endif

#endif 


