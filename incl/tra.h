/* @(#)tra.h	19.1 (ESO-IPG) 02/25/03 13:49:44 */
/*+++++++++++++++
.Header		tra.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.KEYWORDS	Conversions ascii <-> character
.COMMENTS	This header contains declarations for translations
		between binary and ascii. 
.VERSION 1.0	04-Mar-1987: Creation.
----------------*/

#ifndef  TRA_DEF
#define  TRA_DEF	0

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

struct trerr_s{			/* Error `packet' */
	int errno;	/* Error number 		*/
	char *msg;	/* Explanation of errno		*/
	char *str;	/* Text where error occured	*/
	short len;	/* Length of str		*/
	short offset;	/* Position of bad char in str	*/
	};

/*===========================================================================
 *		Error codes
 *===========================================================================*/

#define TRERR_UNDEF	(-1)
#define TRERR_OK	0
#define TRERR_DIGIT	1
#define TRERR_OCTAL	2
#define TRERR_HEXA	3
#define TRERR_FLOAT	4
#define TRERR_TOOLARGE	5
#define TRERR_TOOSMALL	6
#define TRERR_TOOMANY	7
#define TRERR_TOOFEW	8
#define TRERR_VOID	9
#define TRERR_SEXA	10
#define TRERR_SPT	11
#define TRERR_SPL	12
#define TRERR_MT 	13

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
int 	ed_pic	(char *buffer, char *pic, long value);
int 	ed_fpic	(char *buffer, char *pic, double value);
int 	ed_pic4	(char *buffer, char *pic, long value, char fill_char);
int 	tr_al	(char *string, int string_length, long *value);
int 	tr_al1	(char *string, int string_length, long *value, int n_val);
int 	tr_aln	(char *string, int string_length, long *value, int n_val);
int 	tr_af	(char *string, int string_length, double *value);
int 	tr_af1	(char *string, int string_length, double *value, int n_val);
int 	tr_afn	(char *string, int string_length, double *value, int n_val);
int 	tr_pic	(char *string, int string_length, char *pic, long *value);
int 	tr_fpic	(char *string, int string_length, char *pic, double *value);
char   *ltoa	(long value);
char   *ftoa	(double value);
struct trerr_s *tr_errs();		/* Get translation error	*/
#else	
char *ltoa(), *ftoa();
struct trerr_s *tr_errs();
#endif

#define itoa(x)	ltoa(x)

#endif
