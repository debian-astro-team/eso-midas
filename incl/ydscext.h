/*

------------------------------ YDSCEXT ------------------------------ 
 
layout of last version of new large descriptor + help directory entry 

K. Banse	ESO - Garching		010307

030701		last modif
 
*/	

extern char    DSC_FLAG;                       /* = Y   if new layout */


struct	YDIRDSC				/* 100 bytes  */
	{
	char		NAMESTR[78];
	char		TYPE;
        char            dumdee;		/* just place holder */
	short int	NAMELEN;
	short int	HELPLEN;
	short int	UNIT;
	short int	BYTELEM;
	int		NOELEM;
	int		START;
	int		INDEX;
	};


extern struct YDIRDSC	YDSCDIR_ENTRY;
extern struct YDIRDSC	*YDSC_PNTR;	/* will point to YDSCDIR_ENTRY */
 

/*
	NAMESTR	name of descriptor terminated by '\0'	   
		followed by help string (with '\0')	      ch*78 ) 
        TYPE    type of descriptor                            ch*1  )
                C(haracter), I(nteger), R(eal),
                D(ouble precision) (implemented as I*4)
                L(ogical) (implemented as I*4),
                H(elp) (implemented as C*1)
	dumdee	free					      ch*1  ) 80 bytes

	NAMELEN	length of name 					I*2 )  
	HELPLEN	length of help text 				I*2 ) 
	UNIT	descr. units (not used yet) 			I*2 )
	BYTELEM	no. of bytes per element			I*2 ) 8 bytes

	NOELEM	no. of elements in descriptor			I*4 )
	START	start block of descr. data			I*4 ) 12 bytes
	INDEX	offset within starting block with descr. data 	I*4 )


-----------------------------------------------------------------------


*/
