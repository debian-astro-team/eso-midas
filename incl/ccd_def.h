/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2010 European Southern Observatory
.IDENTifer   ccd_def.h
.AUTHOR      R.H. Warmels IPG-ESO Garching (original from plot_def.h (RvanH)
.PURPOSE     MIDAS definition table for the ccd routine 
.VERSION     1.0  940306 RHW

 100224		last modif
------------------------------------------------------------*/
/*
 * Some general constants for the plot routines
 */
#undef   PI
#define  PI             3.14159265358979325e0
#define  CCD_SMLD       0x0000000000000001
#define  CCD_EPSD       0x3CB0000000000000

#undef   TRUE
#undef   FALSE
#define  TRUE            1
#define  FALSE           0

#define  MYMIN(a,b)   ((a) > (b) ? (b) : (a))
#define  MYMAX(a,b)   ((b) > (a) ? (b) : (a))
/*
 * dimension of data to be plotted
 */
#define  MAXFRM     80
#define  MAXFRM1    MAXFRM+1
#define  MAXCMP     ((MAXFRM+1)*MAXFRM)/2
#define  MAXDIM     2
#define  MAXPIX     512                   /* max frame dim. accessed at once */
#define  MAXSIZ     (MAXPIX * MAXPIX)
#define  BIGWT      1.0e4
#define  HALF       0.5               /* One half, in the required precision */
#define  INTVL      1.0/20.0
#define  MINPTS     7
#define  MAXSTK     100  

/*
 * define some macros
 */
#define  MYMIN(a,b)   ((a) > (b) ? (b) : (a))
#define  MYMAX(a,b)   ((b) > (a) ? (b) : (a))
#define  NINT(a)      ((a) < 0 ? (int)((a) - 0.5 ) : (int)((a) + 0.5) )
#define  MYDIST(a,b)  ( sqrt( (a) * (a) + (b) * (b) ) )
#define  MYLENGTH(a,b)( sqrt( (a) * (a) + (b) * (b) ) )
#define  NSTEP(str,until,step) (int) fabs( ((until) - (str))/(step) )+1
#define  SQR(a) ((a) * (a))
/* 
 Definition for macros for mocaicing
 */
#define  MO_ROW       "ROW"
#define  MO_COLUMN    "COLUMN"
#define  MO_LL        "LL"
#define  MO_LR        "LR"
#define  MO_UL        "UL"
#define  MO_UR        "UR"
#define  MO_INDEFR    -999
#define  MO_NMARGIN   4
#define  MO_NYOUT     16
#define  MO_DIM2      2

#define  MO_NINTERP   5
#define  MO_BINEAREST 1
#define  MO_BILINEAR  2
#define  MO_BIPOLY3   3
#define  MO_BIPOLY5   4
#define  MO_BISPLINE3 5

/* 
 Definition of global variables - here we had to fix strings too short ...
 */
int      MO_CORNER;
char     MO_ORDER[8];
char     MO_RASTER[4];
int      MO_NXOVERLAP, MO_NYOVERLAP;
int      MO_NXSUB, MO_NYSUB;
int      MO_NXRSUB, MO_NYRSUB;
int      MO_NCOLS;
int      MO_NROWS;
char     MO_DEFAULT[8];
int      MO_XREF, MO_YREF;
float    MO_BLANK;                                 /* blank value */
float    MO_NULL;                                  /* undefined = null value */

int      MO_IC1[MAXFRM];
int      MO_IC2[MAXFRM];
int      MO_IL1[MAXFRM];
int      MO_IL2[MAXFRM];
int      MO_OC1[MAXFRM];
int      MO_OC2[MAXFRM];
int      MO_OL1[MAXFRM];
int      MO_OL2[MAXFRM];
float    MO_DELTAX[MAXFRM];
float    MO_DELTAY[MAXFRM];
float    MO_DELTAI[MAXFRM];
float    MO_REOFF[MAXFRM*MAXFRM];
float    MO_COUNT[MAXFRM*MAXFRM];
int      MO_IREF[MAXFRM*MAXFRM];
int      MO_JREF[MAXFRM*MAXFRM];

float    MO_XRSHIFTS[MAXFRM][MAXFRM];
float    MO_YRSHIFTS[MAXFRM][MAXFRM];
float    MO_XCSHIFTS[MAXFRM][MAXFRM];
float    MO_YCSHIFTS[MAXFRM][MAXFRM];
int      MO_NRSHIFTS[MAXFRM][MAXFRM];
int      MO_NCSHIFTS[MAXFRM][MAXFRM];

/*
 Here for the variables related to the fitting stuff 
 */
int      MO_MSI_TYPE;
float   *MO_MSI_COEFF;
int 	 MO_MSI_NXCOEFF;
int 	 MO_MSI_NYCOEFF;
int 	 MO_MSI_FSTPNT;






