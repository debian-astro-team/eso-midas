C @(#)fitd.inc	19.1 (ESO-IPG) 02/25/03 13:49:30
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C.IDENTIFICATION
C       FITD.INC                     version 1.1 840328
C        J.D.Ponz              ESO - Garching
C          M.Peron                   version 1.2 900830
C.PURPOSE
C
C       Control Data Structure for Fitting
C
C       User Interface Control Variables : Common /FZCOMMON0/,
C                                            /FZCOMMON1/
C.KEYWORDS
C
C       FIT, DATA STRUCTURE
C
C------------------------------------------------------------------------------
C

       DATA FERPAR/-600/       ! error in input parameters
       DATA FERFUN/-601/       ! non defined function name
       DATA FERTAB/-602/       ! inp/out on different tables
       DATA FERDAT/-603/       ! data not available
       DATA FERPFU/-604/       ! error in function params.
       DATA FERDUP/-605/       ! duplicated param name

       DATA FZPPRN/21/
       DATA FZIWGT/13/
       DATA FZIBND/14/
       DATA FZIMET/15/
       DATA       FZNAME/' '/, FZTYPE/' '/
       DATA       FZFNAM/'POLY','LOG ','EXP ','SIN ','TAN ',
     . 'SINH','TANH'   ,'ASIN','ATAN','ASINH','ATANH','BOX ',
     . 'TRIANG','POISSON','GAUSS','CAUCHY','LORENTZ','VOIGT','LOGPROF',
     . 'EXPPROF','FEXPINT','ERF','SINC','SINCS','FRANZ','HUBBLE',
     . 'KING','RQUART','BRAND','USER00','USER01','USER02','USER03',
     . 'USER04','USER05','USER06','USER07','USER08','USER09',
     . 'LAPLACE','LOGISTIC','SEMILOG','LOGNORM','PARETO','GAMMA',
     . 'DIPOLE','IGAUSS','GAUSSA','IGAUSSA','MOFFAT'/
       DATA FERCON/-606/       ! error in cons. definition
       DATA    FZFPAR/0,3,3,3,3,
     .              3,3,3,3,3,3,3,
     .              3,2,0,3,4,3,3,
     .              3,3,2,3,3,0,3,
     .              3,3,3,0,0,0,0,
     .          0,0,0,0,0,0,3,
     .              3,3,4,4,4,3,3,
     .              0,3,7/
