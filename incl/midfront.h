/*
 
---------------------------- MIDFRONT ------------------------------- 

layout of the different structures which are used at the Terminal FrontEnd
 
K. Banse	930125, 940202, 020527, 031219, 051201

---------------------------------------------------------------------

*/

#define MAX_COMWIN 128
#define MAX_WINLEN 158


int  moncnt;



struct	OLDTOKEN_STRUCT
	{
	char		STR[64];		/* fixed to 60+ chars */
	int		LEN;
	};

struct	OLDTOKEN_STRUCT	OLDTOKEN[10];


struct	COMWIN_STRUCT
	{
	int		COMNO;
	int		LREF;
	int		FIX;
	char		STR[MAX_WINLEN+2];
	};

struct	COMWIN_STRUCT	*comwinp, *comwincur;


struct	TERM_STRUCT
	{
	int		FLAG;
	int		COLS;
	int		LINES;
	int		EDITMODE;
	int		INTENA;
	int		TIMEOUT;
	};

struct	TERM_STRUCT	TERM;




/*


structure OLDTOKEN[10]:
-----------------------

STR		holds token of previous command line
LEN		length of above



structure COMWIN:
-----------------

COMNO           holds command no.    ) of commands stored in buffer
LREF            last reference       )
FIX             lockflag (0 or 1000) )
STR             holds the command line



structure TERM:
---------------

FLAG		= 0/1 for TermWindow no/yes
COLS		no. of columns on terminal window
LINES		no. of lines on terminal window
EDITMODE	Insert_Mode_flag, if = 1 we use insert mode in command 
		editing, else we use change mode
INTENA		interrupt enable flag, if = 1 we are already waiting for input
TIMEOUT		timeout value

this structure is used for the tv routines (terminal handling of F.O.)


*/
