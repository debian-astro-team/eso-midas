C @(#)deanzah.inc	19.1 (ESO-IPG) 02/25/03 13:49:27
C 
C --------------------------   DEANZAH.INC ------------------------------
C	
	INTEGER*2	SYSREG(64),MEMREG(4,5),MEM7RG(5)
	INTEGER*2	VOCREG(4),FCR(8),CURREG(8),DZIOSB(4)
C	
	COMMON	/DZDEVA/ SYSREG,VOCREG,FCR,CURREG,DZIOSB,
     +			 MEMREG,MEM7RG
C 
C 
C ------------------------
C	
C	SYSREG(1,...32)	  = system registers 0,...,31
C	SYSREG(33,...64)  = CMRs 0,...,15  (system regs. 32 - 63)
C	
C	MEMREG(1 - 4,n)	  = memory registers 0 - 3 for channel n
C	MEM7RG		  = memory register 7 for channel n
C	
C	VOCREG(1 - 4)	  = VOC registers 0 - 3
C	FCREG(1 - 8)	  = FCR registers 0 - 7
C	
C	CURREG(1 - 7)	  = cursor registers 0 - 6
C	
C	DZIOSB		  = IOSB buffer for driver calls
C
C ------------------------
C 
C  the above values are read from the DeAnza if needed, except the memory
C  registers MEMREG, MEM7RG which are kept in the keyword DAZDEVR
C  because they are not readable from the DeAnza
C 
C	K. Banse	version 1.00	880122
C
C -----------------------------------------------------------------
