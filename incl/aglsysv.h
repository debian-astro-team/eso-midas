/* @(#)aglsysv.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:25   */
/*
 * HEADER : aglsysv.h      - Vers 3.6.001  - Jul 1992 -  L. Fini, OAA
 *                                                    -  C. Guirao. ESO-IPG
 *                         - Vers 3.6.000  - Dec 1991 -  L. Fini, OAA
 *
 * This file contains standard includes needed by AGL, suitable
 * for any Unix System V system
 *
 */

#define UNIX                 /* Define this system as Unix                  */

#include <stdio.h>           /*    Standard libraries                       */
#include <fcntl.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <termio.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/un.h>

#ifndef MIDAS			/* Midas has his own mechanism to do that   */

#include <string.h>
#include <malloc.h>

#endif
