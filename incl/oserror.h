/* @(#)oserror.h	19.1 (ESO-IPG) 02/25/03 13:49:37 */
/*++++++++++++++++++++++++++ midas/osparms.h ++++++++++++++++++++++++++++
.TYPE         	Header
.LANGUAGE      	C
.IDENTIFICATION oserror.h
.AUTHOR         CG  [ESO-IPG]
.KEYWORDS       operating system 
.ENVIRONMENT    Unix
.COMMENTS       Definitions of MIDAS oserror and osermsg variables.
.VERSION 1.1 	09-Jul-1992 CG. Implementation 
.VERSION 1.2	28-Jul-1992 CG. errno included, missed in errno.h of Alliant
--------------------------------------------------------------------------*/

#ifndef _oserror_h
#define _oserror_h

#include <osdefos.h>

#ifdef OSERROR_D        /* oserror is already defined (Silicon Graphics) */
#define oserror midaserror
#endif

MID_EXTERN int oserror;
MID_EXTERN char *oserrmsg;

#ifndef VMS
MID_EXTERN int errno;
#endif

#endif /*!_oserror_h*/
