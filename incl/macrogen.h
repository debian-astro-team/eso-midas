/* @(#)macrogen.h	19.1 (ESO-IPG) 02/25/03 13:49:35 */
/*++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      macrogen.h
.AUTHOR              Francois Ochsenbein, JD Ponz [ESO-IPG]
.KEYWORDS            Classical Macros
.ENVIRONMENT         Unix / VMS
.COMMENTS            These miscellaneous parameters are defined as keywords in
                     order to  provide auto-documentation in the programs.
.VERSION 1.0	     22-Oct-1987  Creation
.VERSION 2.0	     01-Jun-1990  Merged with tblmac
.VERSION 3.0         22-Sep-1993  Add macros for handling null values
-------------------*/

#ifndef  MACROS_DEF 
#define  MACROS_DEF      0

#define ITEMS(s)	(sizeof(s)/sizeof(s[0]))   /* How many in a table */

/* CG. 09.11.94. Undefine MAX & MIN to avoid conflict with macros in param.h */
#undef MAX
#undef MIN
#define MAX(x,y)	((x) >= (y) ? (x):(y) )    /* Maximum */
#define MIN(x,y)	((x) <= (y) ? (x):(y) )    /* Minimum */
#define ABSOLUTE(x)	((x) < 0 ? (-x) : (x))     /* Absolute*/
#define NINT(x)		(x) + ( (x) < 0 ? -0.5: 0.5)
						/* Nearest Integer */

		/* Special values for various NULL's	*/

#define EOS		'\0'			/* End Of String   */
#define NULL_PTR(type)	(type *)0
#define NULL_FCT(type)	(type (*)())0

#define	Control_(x)	CNTRL(x)	/* For back compatibility      */
#define	CNTRL(x)	((x) & 037)	/* Control char, eg CNTRL('A') */

#define if_not(code)	if(!(code))

        /* Define all NULL tests as macros */
#define isNULL1(x)      (*(char  *)(x)  == NULL1)
#define isNULL2(x)      (*(short *)(x)  == NULL2)
#define isNULL4(x)      (*(int *)(x)   == NULL4)
#if (FLOATFMT == IEEEFLOAT)
#define isNULLF(x)      ((*(int *)(x) & FMASKEXP)  == FMASKEXP)
#define isNULLD(x)      ((((int *)(x))[DINTEXP] & DMASKEXP)  == DMASKEXP)
#else
#define isNULLF(x)      (*(int *)(x)   == NULLF)
#define isNULLD(x)      ((((int *)(x))[DINTEXP]   == NULLD) && \
                         (((int *)(x))[DINTEXP^1] == 0   ))
#endif
        /* Define all NULL tests as macros */
#define toNULL1(x)      *(char  *)(x)  = NULL1
#define toNULL2(x)      *(short *)(x)  = NULL2
#define toNULL4(x)      *(int *)(x)   = NULL4
#define toNULLF(x)      *(int *)(x)   = NULLF
#define toNULLD(x)      ((int *)(x))[DINTEXP]   = NULLD,  \
                        ((int *)(x))[DINTEXP^1] = 0

          	/* String -> string,length    		*/

#define ARG_STRING(s)  s,strlen(s)         /* string address + length    */
#define ARG_TEXT(s)    s,(sizeof(s)-1)     /* constant string addr+length*/


		/* Bit operations 			*/
		
#define UNSPEC(a) 		*(unsigned long *)(a)  	

#define MASK(bit)		(1<<bit)

#define SetBit(v,mask)		((v) |= (mask))
#define ClearBit(v,mask)	((v) &= (~(mask)))
#define ChangeBit(v,mask)	((v) ^= (mask))

				/* Test bit(s) set (1), or clear (0)	*/
		
#define isSet(v,mask)		((v) &  (mask))	
#define isClear(v,mask)		(!isSet(v,mask))	
#define areSet(v,mask)		(((v)&(mask)) == (mask))
#define ONES			areSet		/* Back compatibility	*/

#endif
