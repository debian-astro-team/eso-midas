/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 2009 European Southern Observatory
.IDENTifer   agl.h
.AUTHOR      K. Banse   ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     defintion of constants for AGL

.ENVIRONment none

.VERSION     1.0     091221     creation

 091222		last modif
------------------------------------------------------------*/


/* Define marker names			*/

#define DOT			0
#define EXAGON			1
#define BOX			2
#define TRIANGLE		3
#define PLUS			4
#define CROSS			5
#define CROSS_ON_PLUS		6
#define STAR			7
#define PLUS_ON_BOX		8
#define CROSS_ON_BOX		9
#define LOZENGE			10
#define HBAR			11
#define VBAR			12
#define RIGHT_ARROW		13
#define UP_ARROW		14
#define LEFT_ARROW		15
#define DOWN_ARROW		16
#define FILLED_EXAGON		17
#define FILLED_BOX		18
#define FILLED_TRIANGLE		19
#define FILLED_LOZENGE		20


#include <proto_agl.h>


