C  @(#)fitnagc.inc	19.1 (ESO-DMD) 02/25/03 13:49:30 
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C.IDENTIFICATION
C       FITNAG.INC                     version 1.1 851205
C        J.D.Ponz              ESO - Garching
C       Ph. Defert               "
C
C.PURPOSE
C
C       Control Data Structure for Fitting
C
C       User Interface Control Variables : Common /FIT_COMMON0/,
C                                            /FIT_COMMON1/
C.KEYWORDS
C
C       FIT, DATA STRUCTURE
C
C 021122	last modif
C 
C------------------------------------------------------------------------------
C
C       Common areas
C
       COMMON/FITCM0/   
     .       FILNAM,FILTYP,MASK,
     .       RIMA,FNAM,SPEC,
     .       PTOKEN

       COMMON/FITCM1/ 
     .       START,STEP,
     .       ERROR,PARAM,PARINI,NRFUN,NRITER,NRDATA,
     .       NRPARAM,FLAG,DEPCOL,WGTCOL,NRIND,INDCOL,NRPIX,
     .       FIXPAR,FCTCOD,ACTPAR,PLEN,RELAX,FINCHI,PRECIS,
     .       CHI,UNCER,PRPFAC,MAPPED,FPAR,
     .       SELE

        COMMON/FITCM2/ 
     .       METPAR,PARLOW,PARUPP,NRPFIX,
     .       FZPPRN,FZIWGT,FZIBND,FZIMET

        COMMON/FITCM3/ 
     .       ERRFPAR,ERRFFUN,ERRFTAB,ERRFDAT,
     .       ERRFPFU,ERRFDUP,ERRFCON,FZIDEN

        COMMON/FITCM4/ 
     .       PTRI,PTRM				!I*8 pointers
C
       EQUIVALENCE       (NRAXIS,NRIND)
       EQUIVALENCE       (CHAR,FILNAM)
       EQUIVALENCE       (INTG,NRFUN)
C       EQUIVALENCE       (REAL,RELAX)
