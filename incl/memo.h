/* @(#)memo.h	19.1 (ESO-IPG) 02/25/03 13:49:35 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		memo.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO], Alan Richmond [ST-ECF].
.CATEGORY	Memory Allocations
.COMMENTS	
.ENVIRONMENT	
.VERSION 1.0   	15-Apr-1988: Extracted from stesodef 
---------------*/

#ifndef MEMO_DEF
#define MEMO_DEF	0

#ifndef _TEMPLATES_
#include <compiler.h>
#endif


#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
void	*mm_alloc	(unsigned int bytes);
void	*mm_expand	(void *address, unsigned int bytes);
int	 mm_free	(void *address);
#else
char 	*mm_alloc(), *mm_expand();
#endif


#define MEM_FREE(ptr) 	mm_free((char *)ptr)
			/* free previously allocated memory	*/
#define MEM_EXP(type,ptr,n)	(type *)mm_expand(ptr,sizeof(type)*(n)) \
			/* reallocates n items of type		*/
#define MEM_GET(type,n)		(type *)mm_alloc(sizeof(type)*(n)) 	 \
			/* allocates n items of type		*/

#endif
