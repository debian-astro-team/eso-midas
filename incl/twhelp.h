/* @(#)twhelp.h	19.1 (ESO-IPG) 02/25/03 13:49:45 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		twhelp.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Hierarchical Help
.COMMENTS	

.ENVIRONMENT	TermWindows

.VERSION 3.0	Created 23-May-1988. Requires TermWindows version 3.
.VERSION 3.1	22-Jun-1989. th_set returns the old context.
.VERSION 3.2	04-Dec-1990: added th_oset + th_merge

------------------------------------------------------------*/

#ifndef TWHELP_DEF

#define TWHELP_DEF	0

#include <tw.h>		/* Requires TermWindows Definitions */
#include <buffer.h>	/* Requires knowledge of buffers    */

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

/*=======================================================================
 * 		Basic Structures
 *=======================================================================*/

typedef struct {
	WINDOW 	*w[3];		/* Windows: Title, display, dialog	*/
	BUFFER 	*buf;		/* Buffer to store input		*/
	long	date;		/* Date of main file			*/
	char	*title;		/* Help title				*/
	char   	*context;	/* Choice of the Context (4th parameter)*/
	char   	*bad_index;	/* What to do if index too old		*/
	char	indexed;	/* Flag for Indexed Help		*/
	char	flags;		/* 3 if Title taken from 3rd parameter	*/
    }	TWHELP;

/*=======================================================================
 * 		Functions description
 *=======================================================================*/

#if _TEMPLATES_
TWHELP	*th_init	(char *filename, WINDOW *wt, WINDOW *w, WINDOW *wd);
char	*th_set		(TWHELP *helpin, char *context, int title_or_context);
int	 th_merge	(TWHELP *helpin, char *filename);
int 	 th_help	(TWHELP *helpin, char *topic, int exit_option);
int	 th_oset	(TWHELP *helpin, int  which_option,  int value);
int	 th_topsep	(char *separators_list);
#else
TWHELP 	*th_init();
char	*th_set();
#endif

#define InitHelp(file,w1,w2,w3)	th_init(file,w1,w2,w3)
#define AddHelp(h,file)		th_merge(h,file)
#define Help(h)			th_help(h, (char *)0, 0)
#define HelpOnTopic(h,s)	th_help(h, s, 0)

#define DisplayHelpTopic(h,s)	th_help(h, s, 1)

#define SetHelpSeparators(s)	th_topsep(s)
#define SetHelpContext(h,s)	th_set(h, s, 1)
#define SetHelpTitle(h,s)	th_set(h, s, 0)
#define SetHelpIndex(h,s)	th_set(h, s, 2)
#define SetHelpTitleOption(h,v)	th_oset(h,2, v)

#endif
