/* @(#)rtape.h	19.1 (ESO-IPG) 02/25/03 13:49:41 */
#define OPEN_DEVICE     0
#define CLOSE_DEVICE    1
#define READ_DATA       2
#define WRITE_DATA      3
#define WRITE_EOF       4
#define SKIP_FSF        5
#define SKIP_BSF        6
#define REWIND          7
#define SKIP_EOM        8
#define READ_INFO       9

#define MAX_HOST_NAME	64	/* Maximum length for hostnames */
#define MAX_DEVICE_NAME 80	/* Maximum length for devicenames */
#define MAX_USER_NAME	10	/* Maximum length for usernames */
#define MAX_MSG		160	/* Maximum length for error messages */
#define MAX_STRING	80	/* Maximum length for strings */

struct command {
        int cmd;
        int param[2];
        };

struct result {
        int ret;
        int oserror;
        int nobyt;
        };
