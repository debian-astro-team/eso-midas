/* @(#)oshost.h	19.1 (ESO-IPG) 02/25/03 13:49:37 */
/*+++++++++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      oshost.h
.AUTHOR              Francois Ochsenbein [ESO-IPG]
.KEYWORDS            Host interfaces
.ENVIRONMENT         Unix
.COMMENTS            This module includes function declarations
			for time services and error messages.
.VERSION 1.0	04-Mar-1988  Creation   
--------------------------------------------------------------------------*/

#ifndef  OSHOST_DEF 
#define  OSHOST_DEF      0

/*===========================================================================
 *		Clock Definitions
 *===========================================================================*/

	/* the date & time structure definition are in time1.h */

#define   INIT_CLOCK	SET_CLOCK
#define   SET_CLOCK   	0
#define   GET_CLOCK    	1

#include <compiler.h>

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/

char *osmsg(void);		/* System messages	*/

char *oshenv(char *logname, char *table_name);
		/* Translation of logical names (str, table) */
int  oshtr  (char *logname, char *translation, int translation_size);
int  oshset (char *logname, char *translation);

long oshtime(void);
long oshgmt(void);

long oshcpu (int set_or_get, float *cpu_float);	/* click */
long oshela (int set_or_get, long *ela_float);	/* s     */

int  oshcmd (char *command, char *in_file, char *out_file, char *err_file);

#else		/* No Templates */

char *osmsg(), *oshenv();
long oshtime(), oshgmt();

long oshcpu (), oshela ();
#endif

#endif
