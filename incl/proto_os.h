/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2012 European Southern Observatory
.IDENTifer   proto_os.h
.AUTHOR      C. Guirao IPG-ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for all OS Interfaces,

.ENVIRONment none

.VERSION     1.0     22-Oct-1993   Creation by CG.

 120420		last modif
------------------------------------------------------------*/

#ifndef PROTO_OS
#define PROTO_OS

/* to avoid generic complains for strlen() */
#include <string.h>
#include <sys/types.h>

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * module iodev.c
 */
extern struct iolist * iodev(
#ifdef __STDC__
	void
#endif
);

/*
 * module iodevr.c
 */
extern struct iolist * iodevr(
#ifdef __STDC__
	void
#endif
);

/*
 * module iodevd.c
 */
extern struct iolist * iodevd(
#ifdef __STDC__
	void
#endif
);

/*
 * module osa.c
 */
extern int osaopen(
#ifdef __STDC__
	char * phname , int mode
#endif
);
extern int osaclose(
#ifdef __STDC__
	int fid
#endif
);
extern int osaread(
#ifdef __STDC__
	int fid , char * pbuf , int nochar
#endif
);
extern int osawrite(
#ifdef __STDC__
	int fid , char * pbuf , int nochar
#endif
);
extern long osaseek(
#ifdef __STDC__
	int fid , long address , int mode
#endif
);
extern int osawait(
#ifdef __STDC__
	int fid
#endif
);
extern long osasize(
#ifdef __STDC__
	int fid
#endif
);
extern int osaunix(
#ifdef __STDC__
	void
#endif
);

/*
 * module osc.c
 */
extern int oscopy(
#ifdef __STDC__
	char * dest , char * source , int len
#endif
);
extern int oscopuc(
#ifdef __STDC__
	char * dest , char * source , int len , char escape
#endif
);
extern int oscfill(
#ifdef __STDC__
	char * dest , int len , char fill
#endif
);
extern int oscloc(
#ifdef __STDC__
	char * str , int len , char fill
#endif
);
extern int oscbloc(
#ifdef __STDC__
	char * str , int len , char fill
#endif
);
extern int oscskip(
#ifdef __STDC__
	char * str , int len , char fill
#endif
);
extern int oscbskip(
#ifdef __STDC__
	char * str , int len , char fill
#endif
);
extern int oscspan(
#ifdef __STDC__
	unsigned char * str , int len , unsigned char mask , unsigned char * table
#endif
);
extern int oscbspan(
#ifdef __STDC__
	unsigned char * str , int len , unsigned char mask , unsigned char * table
#endif
);
extern int oscscan(
#ifdef __STDC__
	unsigned char * str , int len , unsigned char mask , unsigned char * table
#endif
);
extern int oscbscan(
#ifdef __STDC__
	unsigned char * str , int len , unsigned char mask , unsigned char * table
#endif
);
extern int oscomp(
#ifdef __STDC__
	char * str1 , char * str2 , int len
#endif
);
extern int osccomp(
#ifdef __STDC__
	char * str1 , char * str2 , int len
#endif
);
extern int oscindex(
#ifdef __STDC__
	char * str1 , int len1 , char * str2 , int len2
#endif
);
extern int osctr(
#ifdef __STDC__
	unsigned char * dest , unsigned char * source , int len , unsigned char * table
#endif
);

/*
 * module osd.c
 */
extern int osdopen(
#ifdef __STDC__
	char * phname , int mode
#endif
);
extern int osdclose(
#ifdef __STDC__
	int fid
#endif
);
extern int osdread(
#ifdef __STDC__
	int fid , char * pbuf , size_t nobyt
#endif
);
extern int osdofread(
#ifdef __STDC__
	int fid , off_t address, char * pbuf , size_t nobyt
#endif
);
extern int osdofwrite(
#ifdef __STDC__
	int fid , off_t address, char * pbuf , size_t nobyt
#endif
);
extern int osdwrite(
#ifdef __STDC__
	int fid , char * pbuf , size_t nobyt
#endif
);
extern int osdwait(
#ifdef __STDC__
	void
#endif
);
extern off_t osdseek(
#ifdef __STDC__
	int fid , off_t address , int mode
#endif
);
extern long osdsize(
#ifdef __STDC__
	int fid
#endif
);
extern int osdunix(
#ifdef __STDC__
	void
#endif
);
extern int osdputs(
#ifdef __STDC__
	int fid , char * pbuf
#endif
);

/*
 * module oserror.c
 */

/*
 * module osf.c
 */

#ifdef __STDC__
struct  filestatus;
struct  lname;
#endif 

extern int osfcreate(
#ifdef __STDC__
	char * phname , size_t nobyt , int mode
#endif
);
extern int osfdelete(
#ifdef __STDC__
	char * phname
#endif
);
extern int osfcontrol(
#ifdef __STDC__
	char * phname , int function , int value1 , int value2
#endif
);
extern int osfinfo(
#ifdef __STDC__
	char * phname , struct filestatus * status
#endif
);
extern int osfmkdir(
#ifdef __STDC__
	char * phname
#endif
);
extern int osfrename(
#ifdef __STDC__
	char * oldname , char * newname
#endif
);
/* osfphname might be defined on VMS as OSY_TRNLOG */
#ifndef osfphname
extern int osfphname(
#ifdef __STDC__
	char * logname , char * phname
#endif
);
#endif
extern int osflgname(
#ifdef __STDC__
	char * phname , struct lname * logname
#endif
);

/*
 * module osfdate.c
 */
extern int osfop(
#ifdef __STDC__
	char fmt,
	int lenght
#endif
);
extern int osfunix(
#ifdef __STDC__
	void
#endif
);
extern long osfdate(
#ifdef __STDC__
	char * phname
#endif
);
extern long osfsize(
#ifdef __STDC__
	char * phname
#endif
);

/*
 * module osfparse.c
 */
extern char * osfsupply(
#ifdef __STDC__
	char * name , char * tmpl
#endif
);
extern char * osftr(
#ifdef __STDC__
	char * name
#endif
);
extern char * osfparse(
#ifdef __STDC__
	char * name , unsigned int option
#endif
);

/*
 * module osh.c
 */
#ifdef __STDC__
struct  tm;
#endif

extern int oshcpu(
#ifdef __STDC__
	int op , float * ctime
#endif
);
extern long oshtime(
#ifdef __STDC__
	void
#endif
);
extern struct tm * oshtm(
#ifdef __STDC__
	long clock
#endif
);
extern long oshtl(
#ifdef __STDC__
	struct tm * T
#endif
);
extern int oshdate(
#ifdef __STDC__
	char date_string[28] , struct tm * date_struct
#endif
);
extern int oshmsg(
#ifdef __STDC__
	int code , char * message
#endif
);
extern int oshcmd(
#ifdef __STDC__
	char * command , char * input , char * output , char * error
#endif
);
extern int oshchdir(
#ifdef __STDC__
	char * path
#endif
);

extern int oshgetcwd(
#ifdef __STDC__
	char ** path
#endif
);

/*
 * module oshenv.c
 */
extern char * oshenv(
#ifdef __STDC__
	char * logname, char * table
#endif
);
extern int oshpid(
#ifdef __STDC__
	void
#endif
);

/*
 * module osl.c
 */
extern int oslopen(
#ifdef __STDC__
	char * dirname , char * pattern
#endif
);
extern int amatch(
#ifdef __STDC__
	char * lin , char * arg
#endif
);
extern struct dirent * oslread(
#ifdef __STDC__
	void
#endif
);
extern int oslclose(
#ifdef __STDC__
	void
#endif
);
extern int omatch(
#ifdef __STDC__
	char * ch , char * s , int j
#endif
);

/*
 * module osm.c
 */
extern int osmopen(
#ifdef __STDC__
	char * phname , int mode
#endif
);
extern int osmclose(
#ifdef __STDC__
	int fid
#endif
);
extern int osmmap(
#ifdef __STDC__
	int fid , char * * pbuf , unsigned int nobyt , long address
#endif
);
extern int osmunmap(
#ifdef __STDC__
	int fid , char * address
#endif
);

/*
 * module osmemory.c
 */
extern char * osmmget(
#ifdef __STDC__
	size_t nbytes
#endif
);
extern void osmmfree(
#ifdef __STDC__
	void * address
#endif
);
extern char * osmmexp(
#ifdef __STDC__
	char * address , size_t nbytes
#endif
);

/*
 * module osmessage.c
 */
extern char * osmsg(
#ifdef __STDC__
	void
#endif
);

/*
 * module osp.c
 */
extern int ospcreate(
#ifdef __STDC__
	char * phname , char * procname , int backgrd , int stin , int stout, unsigned int time_out
#endif
);
extern void ospexit(
#ifdef __STDC__
	int stat
#endif
);
extern int ospwait(
#ifdef __STDC__
	unsigned int time
#endif
);
extern int ospuwait(
#ifdef __STDC__
	unsigned int time
#endif
);
extern int osppid(
#ifdef __STDC__
	void
#endif
);

/*
 * module oss.c
 */
typedef void (*CATCH_FUNCTION)();
extern int osssend(
#ifdef __STDC__
	int pid , int sig
#endif
);
extern int osscatch(
#ifdef __STDC__
	int sig , CATCH_FUNCTION f
#endif
);
extern int osswait(
#ifdef __STDC__
	int sig , unsigned int time_out
#endif
);

/*
 * module osstr.c
 */
extern int strsglen(
#ifdef __STDC__
	char * str , char cha
#endif
);
extern int strtolower(
#ifdef __STDC__
	char * s1 , char * s2
#endif
);
extern int strgetline(
#ifdef __STDC__
	char * buf
#endif
);
extern int truelen(
#ifdef __STDC__
	char * string
#endif
);
extern int strin(
#ifdef __STDC__
	char * s1 , char * s2
#endif
);
extern char * strstrs(
#ifdef __STDC__
	char * s1 , char * s2
#endif
);

/*
 * module ost.c
 */
#ifdef __STDC__
struct  termstatus;
#endif
extern void oststop(
#ifdef __STDC__
	int s
#endif
);
extern int ostclose(
#ifdef __STDC__
	void
#endif
);
extern int ostopen(
#ifdef __STDC__
	void
#endif
);
extern int ostset(
#ifdef __STDC__
	struct termstatus * tstat
#endif
);
extern int ostraw(
#ifdef __STDC__
	int op
#endif
);
extern CATCH_FUNCTION ostint(
#ifdef __STDC__
	CATCH_FUNCTION f
#endif
);
extern CATCH_FUNCTION ostwinch(
#ifdef __STDC__
	CATCH_FUNCTION f
#endif
);
extern int ostin(
#ifdef __STDC__
	void
#endif
);
extern int ostread(
#ifdef __STDC__
	char * string , int length , int timeout
#endif
);
extern int ostwrite(
#ifdef __STDC__
	char * string , int length
#endif
);
extern int ostinfo(
#ifdef __STDC__
	struct termstatus * tstat
#endif
);

/*
 * module osu.c
 */
#ifdef __STDC__
struct devstat;
#include <osudef.h>		/* For OPITEM definition */
#endif

extern long osufseek(
#ifdef __STDC__
	int f , long offset , int mode
#endif
);
extern int osugrep(
#ifdef __STDC__
	char * class_name , OPITEM *item
#endif
);
extern int osumode(
#ifdef __STDC__
	int f
#endif
);
extern int osubsize(
#ifdef __STDC__
	int f
#endif
);
extern int osustatus(
#ifdef __STDC__
	int f , struct devstat * stat
#endif
);
extern int osuopen(
#ifdef __STDC__
	char * device , int mode , int den
#endif
);
extern int osuclose(
#ifdef __STDC__
	int f , int option
#endif
);
extern int osuread(
#ifdef __STDC__
	int f , char * buffer , int length
#endif
);
extern int osuwrite(
#ifdef __STDC__
	int f , char * buffer , int length
#endif
);
extern int osufclose(
#ifdef __STDC__
	int f
#endif
);
extern long osuftell(
#ifdef __STDC__
	int f
#endif
);
extern char * osuname(
#ifdef __STDC__
	int f
#endif
);

/*
 * module osx.c
 */
extern int osxopen(
#ifdef __STDC__
	register char * channame[2] , register int mode
#endif
);
extern int osxclose(
#ifdef __STDC__
	register int cid
#endif
);
extern int osxread(
#ifdef __STDC__
	int cid , char * pbuf , int nobyt
#endif
);
extern int osxwrite(
#ifdef __STDC__
	int cid , char * pbuf , int nobyt
#endif
);
extern int osxinfo(
#ifdef __STDC__
	int cid , int sec , int usec
#endif
);
extern int osxgetservbyname(
#ifdef __STDC__
	char * service
#endif
);

/*
 * module winsize.c
 */
extern void winsize(
#ifdef __STDC__
	int fd , unsigned short * col , unsigned short * row
#endif
);

#ifdef  __cplusplus
}
#endif

#endif
