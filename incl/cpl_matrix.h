#include <stdlib.h>
typedef long cpl_size;
typedef struct _cpl_vector cpl_vector;
typedef struct _cpl_matrix cpl_matrix;
cpl_vector * cpl_vector_new(cpl_size n);
cpl_vector * cpl_vector_wrap(long n, double *d);
void cpl_vector_delete(cpl_vector * v);
void cpl_vector_unwrap(cpl_vector * v);
void cpl_vector_set(cpl_vector *, cpl_size, double);
double cpl_vector_get(cpl_vector * v, cpl_size i);
cpl_matrix * vander1d(
          const cpl_vector * sample,
          cpl_size           degree,
          void (*func)(double, double *, int),
          size_t offset);
cpl_matrix * polyvander1d(
        const cpl_vector * sample,
        cpl_size           degree);
cpl_matrix * vander2d(
          const cpl_vector * sample_x,
          const cpl_vector * sample_y,
          cpl_size           degree,
          void (*func)(double, double, double *, int),
          size_t offset);
cpl_vector * eval_poly(
      cpl_matrix * design,
      cpl_matrix * coef);
void lsqfit(
         const cpl_matrix * design,
         const cpl_vector * values,
         const cpl_vector * errors,
         cpl_matrix **coef);
double cpl_matrix_get(const cpl_matrix *, cpl_size, cpl_size);
void cpl_matrix_delete(cpl_matrix *);

/* similar interface to NR */
void lsqfit_nr(
    double x[], double y[], double sig[], int ndat, double a[],
    int ma,
    void (*funcs)(double, double [], int));
void lsqfit2d_nr(
    double x[], double y[], double z[], double sig[], int ndat, double a[],
    int ma,
    void (*funcs)(double, double, double [], int));
