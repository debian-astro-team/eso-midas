/* @(#)wcstrig.h	19.1 (ESO-IPG) 02/25/03 13:49:45 */

/*  include file for WCSLIB of Mark Calabretta (mcalabre@atnf.csiro.au) */

#ifndef WCSTRIG
#define WCSTRIG

#include <math.h>

#if __STDC__
   double wcs_cosd(double);
   double wcs_sind(double);
   double wcs_tand(double);
   double wcs_acosd(double);
   double wcs_asind(double);
   double wcs_atand(double);
   double wcs_atan2d(double, double);
#else
   double wcs_cosd();
   double wcs_sind();
   double wcs_tand();
   double wcs_acosd();
   double wcs_asind();
   double wcs_atand();
   double wcs_atan2d();
#endif

/* Domain tolerance for asin and acos functions. */
#define WCSTRIG_TOL 1e-10

#endif /* WCSTRIG */
