/* @(#)stackext.h	19.1 (ESO-IPG) 02/25/03 13:49:41 */
/*
 
---------------------------- STACKEXT ------------------------------- 

external references to STACKDEF.H
 

on file MID_DISK:[CMIDAS.NEW.MONIT]STACKEXT.H

K. Banse	version 1.10	870713

---------------------------------------------------------------------

*/

struct	STACK1
	{
	int		IA[40];
	int		PNTR;
	int		OVF;
	char		CA[40];
	};

struct	STACK1		STACK;
