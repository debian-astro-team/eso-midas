/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2009 European Southern Observatory
.IDENTifer   proto_fitnol.h
.AUTHOR      Otmar Stahl 
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for all NR-Routines routines used in MOS

.ENVIRONment none

.VERSION     1.0    6-APR-1994 Otmar Stahl 

090416		last modif
------------------------------------------------------------*/

extern void fgauss
(
#ifdef __STDC__
 double, double [], double *, double [], int
#endif
 );

extern int fit_gauss
( 
#ifdef __STDC__
 double *, double *, int, double *, int
#endif
 );


