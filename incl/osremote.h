/* @(#)osremote.h	19.1 (ESO-IPG) 02/25/03 13:49:37 */
/*+++++++++++++++++++
.TYPE           Header
.LANGUAGE       C
.IDENTIFICATION osremote.h
.AUTHOR        	Francois Ochsenbein [ESO-IPG]
.KEYWORDS     	Communication via X25
.ENVIRONMENT         
.COMMENTS    	Definition of routines related to waiting
.VERSION 1.0	01-Jul-1988  Creation 
-----------------------*/

#ifndef  OSREMOTE_DEF 
#define  OSREMOTE_DEF      0

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

/*===========================================================================
 *		Remote Status Structures
 *===========================================================================*/

 struct remstat{
 	char *dte;	/* Remote Address	*/
 	char *net;	/* Network name		*/
 	char *device;
 	int  unit;
 };

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/

int osrinfo	(int remid, struct remstat *info);
int osropen	(char *dte, char *subdte, char *user, char *net);
int osrclose	(int remid);
int osrend	(void);			/* Close all connections 	*/
int osrin	(int remid);		/* Number of incoming bytes	*/
int osrread	(int remid, char *buffer, int buf_size, int sec_timeout);
int osrwrite	(int remid, char *buffer, int lbuf);
int osrputs	(int remid, char *buffer);
#endif

#define osrput(rem,s)		osrwrite(rem,s,strlen(s))

#endif
