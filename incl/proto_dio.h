/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1996-2011 European Southern Observatory
.IDENTifer   proto_dio.h
.AUTHOR      C. Guirao IPG-ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for DIO interface.

.ENVIRONment none

.VERSION     1.0     27-Oct-1993   Creation by CG.
.VERSION     1.1     22-Oct-1996   Add parameter to 'fitshkw()', PJG

 110906		last modif
------------------------------------------------------------*/

#ifndef PROTO_DIO
#define PROTO_DIO

#ifdef  __cplusplus
extern "C" {
#endif

#include   <fitsfmt.h>         /* define types of binary data format */
/* #include   <fitscvb.h>          define data conversion formats     */
#include   <fitsdef.h> 

/*
 * module fitsrhd.c
 */
extern int fitsrhd(
#ifdef __STDC__
  int * pmfd , BFDEF * bfdef , int * psize , int * pmfdt , char fmt ,
  char hist , int popt, int Midas_flag
#endif
);

/*
 * module fitsthd.c
 */
extern int kwcomp(
#ifdef __STDC__
  char * pk , char * ps
#endif
);
extern int fitsthd(
#ifdef __STDC__
  int lno , KWORD * kw
#endif
);

/*
 * module fitsrkw.c
 */
extern int fitsrkw(
#ifdef __STDC__
  char *line , KWORD *kw, int  *uns32
#endif
);

/*
 * module fitsckw.c
 */
extern int fitsckw(
#ifdef __STDC__
  int mfd , BFDEF * bfdef , int htype , KWORD * kw , char fmt , 
  char hist, int *delt_flag, int flag
#endif
);
extern int kwcmp(
#ifdef __STDC__
  char * pk , char * ps
#endif
);

/*
 * module fitsgpm.c
 */
extern int fitsgpm(
#ifdef __STDC__
  char *fname, char *bname, int *mfd
#endif
);

/*
 * module fitsrdm.c
 */
extern int fitsrdm(
#ifdef __STDC__
  int mfd , BFDEF * bfdef , int size , int mfdt , char fmt, int Midas_flag
#endif
);

/*
 * module fitsrdmui2.c
 */
extern int fitsrdmUI2(
#ifdef __STDC__
  int mfd , BFDEF * bfdef , int size , int mfdt , char fmt, int Midas_flag
#endif
);

extern void fitsrdmbad(
#ifdef __STDC__
  char *name
#endif
);

/*
 * module fitsrat.c
 */
extern int fitsrat(
#ifdef __STDC__
  int mfd , BFDEF * bfdef , int size, int Midas_flag
#endif
);

/*
 * module fitsrbt.c
 */
extern int fitsrbt(
#ifdef __STDC__
  int mfd , BFDEF * bfdef , int size, int Midas_flag
#endif
);

/*
 * module fitssxd.c
 */
extern int fitssxd(
#ifdef __STDC__
  int size, int Midas_flag
#endif
);

/*
 * module fitsrmd.c
 */
extern int fitsrmd(
#ifdef __STDC__
  int mfd , KWORD * kw , int * pmdc
#endif
);

/*
 * module fitscdm.c
 */
extern int fitsidm(
#ifdef __STDC__
  BFDEF * bfdef
#endif
);
extern int fitscdm(
#ifdef __STDC__
  float * pbuf , int no
#endif
);

/*
 * module fitswkw.c
 */
extern int fitswkp(
#ifdef __STDC__
  int plevel
#endif
);
extern int fitswkl(
#ifdef __STDC__
  char * kw , char * hkw[] , int hkn , int no , int val , char * com
#endif
);
extern int kwput(
#ifdef __STDC__
  char * kw , char * hkw[] , int hkn , int no
#endif
);
extern int kwcom(
#ifdef __STDC__
  char * com
#endif
);
extern int fitswki(
#ifdef __STDC__
  char * kw , char * hkw[] , int hkn , int no , int val , char * com
#endif
);
extern int fitswkd(
#ifdef __STDC__
  char * kw , char * hkw[] , int hkn , int no , double val , char * fmt , char * com
#endif
);
extern int fitswks(
#ifdef __STDC__
  char * kw , char * hkw[] , int hkn , int no , char * val , char * com
#endif
);
extern int fitswkc(
#ifdef __STDC__
  char * kw , char * com
#endif
);

/*
 * module fitswmd.c
 */
extern int fitswmd(
#ifdef __STDC__
  int mfd , char * name
#endif
);

/*
 * module fitswhd.c
 */
extern int fitswhd(
#ifdef __STDC__
  int mfd , int mft , int mff , char * mfn , char ffmt , char cut , int fht
#endif
);

/*
 * module fitswdm.c
 */
extern int fitswdm(
#ifdef __STDC__
  int mfd , int mff , int ffmt
#endif
);

/*
 * module fitswat.c
 */
extern int fitswat(
#ifdef __STDC__
  int mfd
#endif
);

/*
 * module fitswbt.c
 */
extern int fitswbt(
#ifdef __STDC__
  int mfd
#endif
);

/*
 * module fitstkw.c
 */
extern int fitstkw(
#ifdef __STDC__
  KWORD * kw , char fmt
#endif
);

/*
 * module fitswdb.c
 */
extern int fitswdb(
#ifdef __STDC__
  int mfd,
  int ddflag,
  int *fits_info
#endif
);

/*
 * module fitshkw.c
 */
extern int fitshkw(
#ifdef __STDC__
  KWORD * kw , KWDEF * kwd, int opt
#endif
);

/*
 * module fitshdr.c
 */
extern TXDEF * hdr_tbl(
#ifdef __STDC__
  int  nocols
#endif
);
extern BFDEF * hdr_init(
#ifdef __STDC__
  void
#endif
);

/*
 * module fitsmdb.c
 */
extern MDBUF * mdb_init(
#ifdef __STDC__
  void
#endif
);
extern MDBUF * mdb_info(
#ifdef __STDC__
  int  *nsize
#endif
);
extern int  mdb_size(
#ifdef __STDC__
  void
#endif
);
extern int mdb_put(
#ifdef __STDC__
  KWORD * kw , KWDEF * kwd
#endif
);
extern int mdb_cont(
#ifdef __STDC__
  int mfd,
  int flag,
  char * dsc, 
  char * data
#endif
);
extern int mdb_get(
#ifdef __STDC__
  int mfd
#endif
);

/*
 * module fitsinf.c
 */
extern SDEF * fitsbdf(
#ifdef __STDC__
  int mfd , int mff , char * mfn, int *outflg
#endif
);
extern void fpeh(
#ifdef __STDC__
  int s
#endif
);
extern TXDEF * fitstbl(
#ifdef __STDC__
  int mfd , int ffmt , int cut
#endif
);

/*
 * module ihaprhd.c
 */
extern int ihaprhd(
#ifdef __STDC__
  int * pmfd , BFDEF * bfdef , int * psize , char fmt , int popt
#endif
);

/*
 * module ihaprdm.c
 */
extern int ihaprdm(
#ifdef __STDC__
  int mfd , BFDEF * bfdef , int size , char fmt
#endif
);

/*
 * module cvb.c
 */
extern int cvi2(
#ifdef __STDC__
  INT2 * pbuf , int no , int to
#endif
);
extern int cvi4(
#ifdef __STDC__
  INT4 * pbuf , int no , int to
#endif
);
extern int cvr4(
#ifdef __STDC__
  REAL4 * pbuf , int no , int to
#endif
);
extern int cvr8(
#ifdef __STDC__
  REAL8 * pbuf , int no , int to
#endif
);
extern int cvinit(
#ifdef __STDC__
#endif
);

/*
 * module dataio.c
 */
extern int dopen(
#ifdef __STDC__
  char * name , int iomode , char type , int den
#endif
);
extern int drinit(
#ifdef __STDC__
  void
#endif
);
extern int dread(
#ifdef __STDC__
  char * * ppbuf , int no
#endif
);
extern int dwinit(
#ifdef __STDC__
  int bf
#endif
);
extern int dwrite(
#ifdef __STDC__
  char * pbuf , int no
#endif
);
extern int dclose(
#ifdef __STDC__
  int fid
#endif
);
extern int dapos(
#ifdef __STDC__
  int no
#endif
);
extern int dskip(
#ifdef __STDC__
  int no
#endif
);
extern int dbfill(
#ifdef __STDC__
  char val
#endif
);
extern int dweof(
#ifdef __STDC__
  void
#endif
);

/*
 * module ofname.c
 */
extern char * newfn(
#ifdef __STDC__
  char type , char * ext
#endif
);
extern int outname(
#ifdef __STDC__
  char * name , int no, char start_opt
#endif
);

/*
 * module dclist.c
 */
extern int getlist(
#ifdef __STDC__
  int * pno
#endif
);
extern int deflist(
#ifdef __STDC__
  char * plist
#endif
);

/*
 * module fkwstr.c
 */
extern int fkwcat(
#ifdef __STDC__
  char * pkw , char * ptp , int no
#endif
);
extern int fkwcmp(
#ifdef __STDC__
  char * pkw , char * ptp , int * pn
#endif
);

/*
 * module getval.c
 */
extern int getval(
#ifdef __STDC__
  char * pc , int mc , int * pi , double * pdbl
#endif
);


/*
 * module getint.c
 */
extern int getint(
#ifdef __STDC__
  char * pc , int mc , int * pi , int * pint
#endif
);

/*
 * module f77fmt.c
 */
extern int fldis(
#ifdef __STDC__
  char * * pc , char * * ps
#endif
);
extern int fldiv(
#ifdef __STDC__
  char * * pc , double * pv
#endif
);
extern int dcffmt(
#ifdef __STDC__
  char * pfmt , int * rep , char * type , int * wdth , int * dig
#endif
);

/*
 * module datecvt.c
 */
extern double dateymd(
#ifdef __STDC__
  int y , int m , int d
#endif
);
extern char * ymddate(
#ifdef __STDC__
  double y , double m , double d
#endif
);
extern char * fitsdate(
#ifdef __STDC__
  long time
#endif
);

/*
 * module txtfile.c
 */
extern int text_open(
#ifdef __STDC__
  char * name , int mode
#endif
);
extern int text_put(
#ifdef __STDC__
  char * line
#endif
);
extern int text_get(
#ifdef __STDC__
  char * line
#endif
);
extern int text_close(
#ifdef __STDC__
  void
#endif
);

#ifdef  __cplusplus
}
#endif

#endif 
