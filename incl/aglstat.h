/* @(#)aglstat.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:24   */
/*
 * HEADER : aglstat.h      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 */


#ifndef AGLSTATH
#define AGLSTATH

extern int AGL_wasinit;
extern struct AGL_STATUS AGL_status;
extern FILE *AGL_err;
extern int AGL_DEBUG;
extern char *AGL_mfhead;

#endif
