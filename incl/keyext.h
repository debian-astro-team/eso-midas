/*

-------------------------- KEYEXT --------------------------
	
             definition of keyword data base

..VERSION

 890611		creation
 130419		last modif

*/


#ifndef KEYEXT_H
#define KEYEXT_H
 

/* KEY_MAXENT and LOC_MAXENT determine the size of the internal keyfile */

#define	KEY_NAMELEN 15
#define	KEY_MAXENT 1500			/* global keyword */
#define	LOC_MAXENT 3000			/* local keywords */
#define	KEY_FACT 48

 
struct	KEY_STRUCT
	{
	char		IDENT[20];
	short int	BYTELEM;
	short int	FRPAD;
	int	NOELEM;
	int	LEN;
	int	OFFSET;
	int	UNIT;
	};
 
  
struct	KEY_ALL
	{
	int		ORIGIN;
	int		SYSNO;
	int		GLOBENT;		
	int		GLOBDAT;		
	int		GLOBNO;
	int		GLOBEND;
	int		LOCENT;		
	int		LOCDAT;		
	int		LOCNO;
	int		LOCEND;
	struct KEY_STRUCT	*KEYNAMES;		
	double			*KEYWORDS;
	};
 
extern struct KEY_ALL		KEYALL; 

extern double      *KDWORDS;
extern int	   *KIWORDS;
extern float       *KRWORDS;
extern char        *KCWORDS;
extern size_t      *KSWORDS;




/*  offsets for reserved keywords */
 
#define OFF_MODE KEYALL.KEYNAMES[0].OFFSET
#define OFF_ERROR KEYALL.KEYNAMES[1].OFFSET
#define OFF_SYS KEYALL.KEYNAMES[2].OFFSET
#define OFF_LOG KEYALL.KEYNAMES[3].OFFSET
#define OFF_MONPAR KEYALL.KEYNAMES[4].OFFSET
#define OFF_AUX KEYALL.KEYNAMES[5].OFFSET
#define OFF_CATINF KEYALL.KEYNAMES[6].OFFSET
#define OFF_CATAL KEYALL.KEYNAMES[7].OFFSET
#define OFF_CMND KEYALL.KEYNAMES[8].OFFSET
#define OFF_PRSTAT KEYALL.KEYNAMES[9].OFFSET
#define OFF_PCOUNT KEYALL.KEYNAMES[10].OFFSET
#define OFF_PSTAT KEYALL.KEYNAMES[11].OFFSET
#define OFF_P1 KEYALL.KEYNAMES[12].OFFSET
#define OFF_P2 KEYALL.KEYNAMES[13].OFFSET
#define OFF_P3 KEYALL.KEYNAMES[14].OFFSET
#define OFF_P4 KEYALL.KEYNAMES[15].OFFSET
#define OFF_P5 KEYALL.KEYNAMES[16].OFFSET
#define OFF_P6 KEYALL.KEYNAMES[17].OFFSET
#define OFF_P7 KEYALL.KEYNAMES[18].OFFSET
#define OFF_P8 KEYALL.KEYNAMES[19].OFFSET
#define OFF_Q1 KEYALL.KEYNAMES[20].OFFSET
#define OFF_Q2 KEYALL.KEYNAMES[21].OFFSET
#define OFF_Q3 KEYALL.KEYNAMES[22].OFFSET
#define OFF_SESS KEYALL.KEYNAMES[23].OFFSET
#define OFF_APPLIC KEYALL.KEYNAMES[24].OFFSET
#define OFF_PRINT KEYALL.KEYNAMES[25].OFFSET
#define OFF_LINE KEYALL.KEYNAMES[26].OFFSET
#define OFF_OUTNAM KEYALL.KEYNAMES[27].OFFSET
#define OFF_OUTFLG KEYALL.KEYNAMES[28].OFFSET
#define OFF_DPATH KEYALL.KEYNAMES[29].OFFSET
#define OFF_DEBUG KEYALL.KEYNAMES[36].OFFSET		/* omit 6 keywords! */
#define OFF_INFO KEYALL.KEYNAMES[37].OFFSET
  
  
#endif


/*

Definitions:
------------

	KEY_NAMELEN 	max. size of keyword name 
			has to be synchronized with midas_def.h !!
	KEY_MAXENT	initial max. no of global keyword entries
	LOC_MAXENT	initial max. no of local keyword entries
	KEY_FACT	average no. of bytes per keyword


Structure KEY_ALL:
------------------

	ORIGIN		indicates, if ST interfaces are called from the Midas
                        monitor (= 1) or from an application running
			inside MIDAS (= 0) or completely outside (= -1)
	SYSNO		No. of system keyword entries
	GLOBENT		max. no of global keyword entries
	GLOBDAT		max. space for global keywords in bytes, calculated
			as GLOBENT * KEY_FACT
	GLOBNO		No. of last global keyword entry in use (0, 1, ... )
	GLOBEND		Last 4-byte word used for global keywords (0, 1, ... )

        global and local keywords are stored bottom up; local keywords begin
        at limit of global keywords

	LOCENT		max. no of local keyword entries
	LOCDAT		max. space for local keywords in bytes, calculated
			as LOCENT * KEY_FACT
	LOCNO		No. of last local keyword entry in use (GLOBENT, G+1, )
	LOCEND		Last 4-byte word used for local keywrds (GLOBDAT, G+1, )
	

	KEYNAMES	pointer to KEY_STRUCT array
	KEYWORDS	pointer to data buffer (as double)


Structure KEY_STRUCT:
---------------------

	IDENT		Name + type of keyword:
			IDENT[0]-[14] = keyword name 	
			IDENT[15] = keyword type	
			IDENT[16] = A,B,...,J to indicate level 1,...,10
			           for local keywords
	BYTELEM		No. of bytes per element of keyword
	NOELEM		No. of elements connected with keyword
	LEN		Length in bytes of elements connected with keyword
	OFFSET		Offset of keyword values within data block
	FRPAD		No. of bytes needed to pad in front to get to valid
			offset in data space for given data type 
	UNIT		units (not implemented yet)
	

	KEYALL.KEYWORDS is an unstructured data block (all data types are
	intermixed) and is pointed to by:

	IWORDS		for int values
	RWORDS		for float values
	DWORDS		for double values
	CWORDS		for char values
	SWORDS		for size_t values


K. Banse   921217, 021217, 090318

--------------------------------------------------------------
 
*/
