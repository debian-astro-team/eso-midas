/* @(#)tblerr.h	19.1 (ESO-IPG) 02/25/03 13:49:42 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.LANGUAGE        C

.IDENTIFICATION  
    header file TBLERR.H

.AUTHOR         J.D.Ponz

.KEYWORDS       Table error codes

.ENVIRONMENT    Independent

.VERSION        3.0  1 Feb 1987 : Conversion into C
                3.1  900724     : throw out things which are in midas_def.h

------------------------------------------------------------------------*/

/*
  error codes at level 2
*/

#include <midas_def.h>

#define  ERR_APPPAR  (-500) /* general error in parameters */
#define  ERR_APPCOD  (-501) /* error in operation code */
#define  ERR_APPFMT  (-502) /* error in i/o format */
#define  ERR_APPRED  (-503) 
#define  ERR_APPSCL  (-504)
#define  ERR_APPCOL  (-505)
#define  ERR_APPREF  (-506)
#define  ERR_APPALG  (-507)
#define  ERR_APRGNP  (-600) /* Regression, No of points */
#define  ERR_APRGAL  (-601) /* Regression, algorithm    */
  

/* 
  
  Warning + information messages are numbered < 0
  Error messages are numbered > 0 


*/
