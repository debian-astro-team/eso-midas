/* @(#)tblcnt.h	19.1 (ESO-IPG) 02/25/03 13:49:42 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE        C
.IDENTIFICATION  header file TBLCNT.H
.AUTHOR         J.D.Ponz
.KEYWORDS       Table control buffers
.ENVIRONMENT    Independent
.VERSION        3.0  1 Feb 1987 : Conversion into C
.VERSION        3.1  1 Apr 1988 : Generalize
------------------------------------------------------------------------*/

extern char     TBL_KEYN02[TBL_DESLEN+1]; 
extern char     TBL_KEYN07[TBL_DESLEN+1];
extern char     TBL_KEYN10[TBL_DESLEN+1]; 
extern char     TBL_KEYN11[TBL_DESLEN+1]; 
extern double   TBL_RENTRY;
extern double   TBL_DENTRY;
extern char     TBL_CENTRY[TBL_ELMLEN];


extern struct TBL_FCT TBL[TBL_NUMBER];
