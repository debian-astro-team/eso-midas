/* @(#)aglaxes.h	8.2  (OAA-ASTRONET) 10/26/94 10:25:09   */
/*
 * HEADER : aglaxes.h      - Vers 3.6.005  - Oct 1994 -  L. Fini, OAA
 *                         - Vers 3.6.004  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.003  - Jul 1993 -  L. Fini, OAA
 */


#include <aglsys.h>

#include <agl.h>

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE  1
#endif


#define MAXLABEL 80

#define MAXTITLE 80

#define MAXTOKEN 132

#define MAX_FMT	10

#define AG_PI	3.14159265358979324
#define AG_PI05	1.57079632679489678
#define AG_PI15	4.71238898038468986
#define AG_PI2	6.28318530717958648

#define LOG10 	2.30258509299404568
#define LOG60 	4.09434456222210068

enum AX_MODES {
		AX_UNDEF,		/* Undefined                        */
		LIN,			/* Linear axis (straight line)      */
		LOG,			/* Logarithmic (straight line)      */
		ANG			/* Angular axis (2nd axis of        */
					/* polar, or any of special transf. */
					/* Axis)                            */
};

enum ANGLES { DEGREES, RADIANS, HOURS };

enum CASES {
	AITOFF,
	ARC,
	BLAB,
	BOLD,
	EOS,
	DEG12, DEG1, DEG2,
	DIV1, DIV2,
	HOUR12, HOUR1, HOUR2,
	FACT1, FACT2,
	FIXIT,
	GEOM,
	GLS,
	GNOMONIC,
	GRID12, GRID1, GRID2,
	LAB1, LAB2,
	LLAB,
	LOG12, LOG1, LOG2,
	MAN1, MAN2,
	MARK1, MARK2,
	MERCATOR,
	NGRID12, NGRID1, NGRID2,
	NQUOT12, NQUOT1, NQUOT2,
	OFST1, OFST2,
	POLAR,
	QDEG12, QDEG1, QDEG2,
	QHOUR12, QHOUR1, QHOUR2,
	QRAD12, QRAD1, QRAD2,
	RAD12, RAD1, RAD2,
	RLAB,
	SIN,
	S2,
	STEREO,
	TITLE,
	TXDIM,
	WINDOW,
	XSPACE
};
		

enum TK_MODES { AUTO, MANUAL, NONE };

struct AXIS {

enum AX_MODES	mode;		/* Graph mode (LIN,LOG,ANG)               */
enum ANGLES	Angmode;	/* Angular values interpretation          */
enum TK_MODES	man;		/* Manual selection of divisions flag     */
int		grid;		/* Draw grid flag                         */
int		fmark;		/* Mark was set flag                      */
float		fact;		/* Multipl. factor for quotes             */
float		offst;		/* Offset for quotes                      */
float		ftik,ltik;	/* First,last tik coordinate (user)       */
float		tikmj,tikmn;	/* Tik distance (major,minor) (User)      */
float		wind[2];	/* Axis limits (min,max)                  */
float		divs[2];	/* Tiks limits (min,max) (may differ)     */
float		mark;		/* Trace a line at given coordinate       */
float		iexp;		/* Exponent to remove                     */
char		form[MAX_FMT];	/* format specifier for quotations        */
char		label[MAXLABEL+5];	/* Axis label                     */
};

struct AX_STATUS {

int		Geom;		/* Aspect ratio control flag              */
int		Fixit;		/* Fix clipping area dimensions flag      */
int		Timeflag;	/* Write plot time flag                   */
int		Bold;		/* =0 for normal, =10 for thick axes      */

struct AXIS	X;		/* X axis data                            */
struct AXIS	Y;		/* Y axis data                            */

float		Xspace;		/* Space to the left of Y axis (n. chars) */
float		Txtdim;		/* Global dimension multipl. for charact. */

float		Clpa[4];	/* Clipping area limits                   */
float		Boxa[4];	/* Box enclosing the full plot            */

char		Title[MAXTITLE+5];	/* Plot title                     */
};


#define AXIS_TYPE	3	/* Select axis type bits                    */

#define X_BOTTOM_AXIS	0
#define Y_RIGHT_AXIS	1
#define X_TOP_AXIS	2
#define Y_LEFT_AXIS	3
				/* Selection flag masks                     */
#define THICK_AXIS	4	/* Draw the axis with thicker line          */
#define NO_AXIS_LINE	8	/* Do not draw the axis line                */
#define GRID_SOLID	16	/* If this bit=1 use solid line to draw the */
#define QUOTE_DISPL	32	/* If this bit=1 quotes are not centered on */
				/* the tickmark, but sligtly displaced      */
#define ILLEG   (-1)


				/* function templates                       */

void AGLfmt(
#ifdef ANSI
		struct AXIS *
#endif
);

double AGLAngF(
#ifdef ANSI
		enum ANGLES angmode
#endif
);	

void AGLConvert(
#ifdef ANSI
		char *,
		double,
		char *
#endif
);	


double AGLnmnt(
#ifdef ANSI
		double,
		int,
		double,
		double
#endif
);

double AGLGuardU(
#ifdef ANSI
		double,
		int,
		double
#endif
);

double AGLGuardL(
#ifdef ANSI
		double,
		int,
		double
#endif
);

double AGLnmjt(
#ifdef ANSI
		double,
		int,
		double
#endif
);

void AGLsset(
#ifdef ANSI
		char *,
		int
#endif
);
