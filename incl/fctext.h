
/*
---------------------- FCT - Frame Control Table ------------
 	
   definition of structure FCT  in file MID_INCLUDE:fctext.h

   K. Banse	ESO - Garching
	
.VERSION
 120420		last modif
 	
*/
	
#ifndef FCTEXT_DEF
#define FCTEXT_DEF


#define FCT_MAXENT    8
#define FCT_NAME_LEN  160

				/* if this stuff is modified, do not forget
				   to sync routines MID_FCTIN, MID_ACCFRM ! */
 
struct FCT_STRUCT
	{
	struct FCB_STRUCT *FZP;
	char	*BDADDR[4];
	char	*PNTR;
	char	*FITSADDR[3];
	int 	NAMLEN;
	int 	IOCHAN;
#ifdef vms
	int	FILEIDA;
	int	FILEIDB;
#else
	int 	FILEID;
#endif
	unsigned int 	KAUX[4];
	size_t 	SIZE;
	int 	PROT;
	int 	COMPRESS;
	int	NOBYTE;
	int	FORMAT;
	int	PIXPBL;
	int	STBLOK;
	int 	DATTYP;
	int 	FILTYP;
	int 	FITSEXT;
	int 	LINK[2];
	int 	CR_FLAG;
	int 	O_NAMLEN;
#if vms
	char 	DEVICE[20];		/* only needed for RMS */
#endif
	char 	NAME[FCT_NAME_LEN];
	char	FITSOUT;
	char	CATALOG[2];
	char	ACCESS;
	};
	

struct	FCT_ALL
	{
	struct FCT_STRUCT	*ENTRIES;
	int		MAXENT;
	int 		PARM[4];
	};


extern struct FCT_ALL		FCT;
	
#endif			/* FCTEXT_DEF */


/*	

structure FCT_ALL:
------------------

     MAXENT  ...  Maximum no. of entries in FCT table
     PARM    ...  auxiliary parameters:
                  [0,1] for  F_XD_PARM (extended descriptor area at creation)
                  = initial no. of descriptors and total descr. space in bytes
                  [2] for F_DEL_PARM (delete flag for extracted frames)
		  = 1 for delete, = 0 for not delete
                  [3] FITS header processing
     ENTRIES ...  pointer to individual entries in FCT table


structure FCT_STRUCT:
---------------------

     FZP     ...  pointer to FCB of this frame
     NAME    ...  complete frame name with type (null terminated)
             ...  scratch files begin with "-"
             ...  if NAME[0] = ' ', the entry is free
     NAMLEN  ...  length of NAME
     IOCHAN  ...  I/O channel associated with entry.
     FILEID  ...  FILES-11 File Identification associated with entry.
     DEVICE  ...  Device Identification associated with entry.
     BDADDR[4] .. Address limits of virtual memory allocated to bulk data
                  [2] + [3] only used for internal data conversion
     KAUX[4] ...  first element,
		  no. of elements mapped in SCFMAP,
                  descriptor modified flag (0 = not modified, 1 = modified),
                  nobytes of FITS data section
     SIZE    ...  total no. of pixels
     PROT    ...  1 = write_only, 2 = read_only, 3 = read_write
     COMPRESS ... 0 = was uncompressed, 1 = was compressed before
     NOBYTE  ...  no. of bytes per pixel
     FORMAT  ...  original data type - see DATTYP
     PIXPBL  ...  no. of pixels per block/page
     STBLOK  ...  no. of 1. block of data
     DATTYP  ...  required data type:
                  1 = I*1, 2 = I*2, 4 = I*4 or 10 = R*4
     FILTYP  ...  file type:
                  0 = Midas file, >0 = FITS file
                  = 22, for a mapped table via TCTINI (dummyPUT)
     FITSEXT ...  if FITS file, this is the FITS extension
		  0 - no extension, < 0 - extension string
		  > 0 - extension no.
     LINK[2] ...  [0] = flag for links, 
		         0 = no link, 1 = father, 2 = son
                  [1] = no. of sons, if father frame, else imno of father
     CR_FLAG ...  = 1, if file is created, = 2, if opened
     O_NAMLEN...  the length of name without extension added
		  (for FITS files only)
     PNTR    ...  pointer to first data element 
		  always equal or greater than BDADDR[0]
     FITSADDR[3] ... pointers to the internal FITS structures
		     (BFDEF,ADEF,PDEF)
     FITSOUT ...  'X' if FITS data has been moved to memory
		  'Y' if FITS file data should be written from memory
		  back out to disk 
     CATALOG ...  [0] = I, T, F for image, table or fit file
		  [1] = Y(es) or N(o), if FCT entry should be also recorded in 
                  relevant catalog
     ACCESS  ...  I for read-only, O for write or X for scratch file 



*/
	
