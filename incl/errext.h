/* @(#)errext.h	19.1 (ESO-IPG) 02/25/03 13:49:28 */
/*
---------------------------    ERREXT    ----------------------------

definition of error structures 
 
*/
 
#define	ERRO_MAXENT 10
#define ERRO_MAXLEN (ERRO_MAXENT*40)
 
extern int	ERRO_CONT, ERRO_LOG, ERRO_DISP;

extern int	ERROS[ERRO_MAXENT][2];
extern int	ERRO_INDX, ERRO_OFF;

extern char	ERRO_MESAGS[ERRO_MAXLEN]; 
 
/*

  global control variables:
	ERRO_MAXENT	Max. no of entries in error stack
	ERRO_MAXLEN	Total length of message block
	ERRO_INDX	index into stack 
	ERRO_OFF	offset into message buffer
	ERRO_CONT	continuation flag: - 1, stop on warnings + errors
					     0, stop on errors only
					   + 1, stop on nothing
	ERRO_LOG	error log flag: 0, log nothing (in error stack !)
					1, log only errors
					2, log warnings + errors
	ERRO_DISP	error display flag: 0, display nothing 
					    1, display only errors
					    2, display warnings + errors
	
  fixed table with sources + offset in message block (terminated by \0)

	ERROS[n][0]	Source flag:	1 - from STDLIB
					2 - Operating/File system
					10 - interactive monitor
					100 - application program

	ERROS[n][1]	Offset of message within message block
	


  unstructured message block to hold character data only

	ERRO_MESAGS	Character string

  K.Banse   881025, 930226, 930907

--------------------------------------------------------------
*/
