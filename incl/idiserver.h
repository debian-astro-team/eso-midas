/* @(#)idiserver.h	19.1 (ESO-IPG) 02/25/03 13:49:34 */
/*****************************************************************************
 Include file for the IDI server and clients.
 Contains the data structures needed by the osx routines.    
 K. Banse	ESO - Garching
 900201, 910905, 940318, 940426
 *****************************************************************************/

#ifndef IDISERVER
#define IDISERVER

#include    <stdio.h>
#include    <osparms.h>

#define III_SIZE  sizeof(int)
#define RRR_SIZE  sizeof(float)
#define DDD_SIZE  sizeof(double)

#define BASE_SERVICE       "midxcon"   /* service-name must be defined */
#define BASE_PORT          "6000   "   /* in /etc/services, otherwise 6000 */
#define EXIT_CODE  	   -1


#define     MAXINTBUF       1000   /* max. array length in argument passing  */
#define     MAXSTRLEN       400    /* max. string length in argument passing */
#define     MAXIBUFL        1024 
#define     MAXRBUFL        1024 
#define     MAXDBUFL        512 
#define     BUFHEAD         (III_SIZE*4)           /* size of buffer head  */
#define     RET_SIZE        (III_SIZE*4)       /* size of return buffer head */


static struct                      /* Structure for passing data to server   */
    {
    int     nobyt;                 /* current length of buffer in bytes      */
    int     ident;
    int     code_id;               /* function code */
    int     tmpa;		   /* spare */
    union                          /* data section  */
        {
        int     in[MAXIBUFL];
        float   fl[MAXRBUFL];
        double  db[MAXDBUFL];
        } data;
    } serv_buf;



static struct                      /* structure to receive data from server  */
    {
    int     code;                  /* return code                            */
    int     nobyt;                 /* length of return buffer in bytes */
    int     tmpb;		   /* spare */
    int     tmpc;		   /* spare */
    union                          /* data section                           */
        {
        int     in[MAXIBUFL];
        float   fl[MAXRBUFL];
        double  db[MAXDBUFL];
        } data;
    } serv_ret;

#endif
