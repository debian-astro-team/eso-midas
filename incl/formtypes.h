/* @(#)formtypes.h	19.1 (ESO-IPG) 02/25/03 13:49:31 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ProGenerated.
.NAME           formtypes.h
.TYPE		Header
.LANGUAGE       C
.CATEGORY       Atomic data types
.ENVIRONMENT    Proteus_2.0 .
.AUTHOR		Alan Richmond	ST--ECF.
.VERSION  1.0   21-Dec-1988	Creation (F. Ochsenbein)
.VERSION  1.1   01-Jun-1989	Added _NL_
.VERSION  1.2   22-Sep-1989	Added _LIKE_
.VERSION  1.3   23-Mar-1990	Completed set of relational operators.
.VERSION  1.4   11-May-1990	Completed set of Strings

.COMMENTS
	Definition of datatypes 
------------------------------------------------------------------------*/

#ifndef FORMTYPES_DEF
#define FORMTYPES_DEF	0

#define _DTYPE_INT	0	/* Integer (I*4)	*/
#define _DTYPE_DOUBLE	1	/* R*8			*/
#define _DTYPE_ACTION	2	/* Routine		*/
#define _DTYPE_FLOAT	3	/* R*4			*/
#define _DTYPE_STR	4	/* String case sensitive	*/
#define _DTYPE_LOWER	5	/* String in uppercase		*/
#define _DTYPE_UPPER	6	/* String in uppercase		*/
#define _DTYPE_STRCASE	7	/* String case insensitive	*/
#define is_DTYPE_STR(x)		(x&4)	/* Is a string	*/

#define _DTYPE_TIME	0x10	/* Modifier for Date/Time '1980	*/
#define _DTYPE_TIME80	0x10	/* Modifier for Date/Time '1980	*/
#define _DTYPE_TIME70	0x20	/* Modifier for Date/Time '1970	*/
#define _DTYPE_PERIODIC	0x40	/* e.g. longitude, RA	*/

/*================================================================
 * 	Parsing a Qualification
 *================================================================*/

#define _X_		'\033'	/* Raw values (no pic)	*/
#define _AND_         	 1
#define _OR_          	 2

#define _EQ_          	 4
#define _LIKE_           4
#define _EQS_         	 5
#define _EQC_         	 6
#define _NE_          	 7

#define _LT_          	 8
#define _LE_          	 9
#define _GT_          	10
#define _GE_          	11
#define _LP_		12	/* Left paranthesis	*/
#define _RP_		13	/* Right Paranthesis	*/
#define _NL_          	0177 	/* Null value		*/

char *tr_qual();	/* Function returning a parsed Qualification */

#endif
