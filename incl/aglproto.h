/* @(#)aglproto.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:24   */
/*
 * HEADER : aglproto.h     - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA
 */

/****************************************************************************/
/*	Function prototypes for AGL internal routines
 *
 *
 */

char *AGL_mchar(
#ifdef ANSI
		char *, 
		struct AGL_char *
#endif
);	

void AGL_mfex(
#ifdef ANSI
		char *
#endif
);

char *AGL_scan(
#ifdef ANSI
		void
#endif
);

char *AGL_top(
#ifdef ANSI
		void
#endif
);


void AGL_ch2pl(
#ifdef ANSI
		struct AGL_char *,
		int
#endif
);

int AGL_cnvt(
#ifdef ANSI
		double *,
		double *
#endif
);

int AGL_gfloat(
#ifdef ANSI
		char *,
		float []
#endif
);	

int AGL_gint(
#ifdef ANSI
		char *,
		int []
#endif
);	

int AGL_n2u (
#ifdef ANSI
		double,
		double,
		double *,
		double *
#endif
);	

void AGL_puterr(
#ifdef ANSI
		int,
		char *
#endif
);

int AGL_rcvt(
#ifdef ANSI
		double *,
		double *
#endif
);

int AGL_scmp(
#ifdef ANSI
		char *,
		char *
#endif
);

int AGL_set(
#ifdef ANSI
		char *
#endif
);

void AGL_siger(
#ifdef ANSI
		char *
#endif
);

int AGL_sstyl(
#ifdef ANSI
		double
#endif
);

struct AGL_font *AGL_chinf(
#ifdef ANSI
		 int
#endif
);

struct polyline *AGL_padj(
#ifdef ANSI
		struct polyline *
#endif
);	

void AGL_cginit (
#ifdef ANSI
		void
#endif
);


void AGL_chrd(
#ifdef ANSI
		double,
		double,
		struct polyline *,
		double,
		double,
		double,
		double
#endif
);

void AGL_dpll (
#ifdef ANSI
		struct polyline *
#endif
);

void AGL_dshz(
#ifdef ANSI
		void
#endif
);

void AGL_dstop(
#ifdef ANSI
		int
#endif
);

void AGL_fvwp(
#ifdef ANSI
		int
#endif
);			

void AGL_gcdes(
#ifdef ANSI
		struct AGL_char *
#endif
);

void AGL_gdev(
#ifdef ANSI
		char *,
		double,
		double
#endif
);

void AGL_gpll (
#ifdef ANSI
		struct polyline *
#endif
);

void AGL_init(
#ifdef ANSI
		void
#endif
);


void AGL_loc (
#ifdef ANSI
		float *,
		float *,
		int,
		char *,
		int *
#endif
);

void AGL_mopn(
#ifdef ANSI
		char *
#endif
);

void AGL_mput(
#ifdef ANSI
		FILE *,
		struct polyline *,
		int
#endif
);

void AGL_pcoo(
#ifdef ANSI
		double,
		double,
		struct polyline *
#endif
);

void AGL_pflsh(
#ifdef ANSI
		struct polyline *
#endif
);		

void AGL_plmk(
#ifdef ANSI
		float *,
		float *,
		int,
		int,
		enum GRAPH_MODES,
		struct polyline *
#endif
);  

void AGL_pop(
#ifdef ANSI
		void
#endif
);


void AGL_push(
#ifdef ANSI
		char *
#endif
);			

void AGL_sclr(
#ifdef ANSI
		void
#endif
);

void AGL_sdrv(
#ifdef ANSI
		void
#endif
);

void AGL_sgbl(
#ifdef ANSI
		void
#endif
);


void AGL_sgeom(
#ifdef ANSI
		int
#endif
);

void AGL_spfls(
#ifdef ANSI
		void (*)(),
		struct polyline *
#endif
);

void AGL_sstat(
#ifdef ANSI
		void
#endif
);

void AGL_stop(
#ifdef ANSI
		void
#endif
);


void AGL_strd(
#ifdef ANSI
		char *,
		int,
		double *,
		double *
#endif
);

void AGL_swdt(
#ifdef ANSI
		int
#endif
);

void AGL_term(
#ifdef ANSI
		void
#endif
);

void AGL_trns(
#ifdef ANSI
		char *,
		int,
		char *
#endif
);

void AGL_vflsh(
#ifdef ANSI
		void
#endif
);

