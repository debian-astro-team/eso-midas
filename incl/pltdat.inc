C @(#)pltdat.inc	19.1 (ESO-IPG) 02/25/03 13:49:38
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION: PLTDAT.INC
C.PURPOSE:        To define the several variables in the plot system
C.AUTHOR:         Rein H. Warmels
C.NOTE:           More documentation can be found in PLTDEC.INC
C.VERSION:        890301  RHW   Header included
C-----------------------------------------------------------------------------
      DATA 		PI     /3.14159265/
      DATA              ANGLE  /0.0/
      DATA 		EEE    /2.7182818/
      DATA              XLEXP  /1.E-4/
      DATA              XHEXP  /1.E5/
      DATA              YLEXP  /1.E-4/
      DATA              YHEXP  /1.E5/
