/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2009 European Southern Observatory
.IDENTifer   proto_xcon.h
.AUTHOR      K. Banse	ESO - DMD, DPG
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for Midas communication interfacers

.ENVIRONment none

.VERSION  [1.00]     981118	Creation

 090407		last modif
------------------------------------------------------------*/

#ifndef  PROTO_XCON               /* Avoid redefinitions */
#define  PROTO_XCON       0

#ifdef  __cplusplus
extern "C" {
#endif

extern int XCCSND(
#ifdef __STDC__
	int   cid,
        char  *comline,
        int   secs,
        int   *retstat
#endif
);

extern int XCCWAI( 
#ifdef __STDC__
	int    cid,
	int    *retstat
#endif
);
 
extern int XCCRDX(
#ifdef __STDC__
        int    cid,
        int    *retstat
#endif
);
 
extern int XCKRDI(
#ifdef __STDC__
        int    cid,
        char   *key,
        int    felem,
        int    maxvals,
        int    *actvals,
        int    *values,
        int    *unit,
        int    *null,
        int    *kstat
#endif
);

extern int XCKRDR(
#ifdef __STDC__
        int    cid,
        char   *key,
        int    felem,
        int    maxvals,
        int    *actvals,
        float  *values,
        int    *unit,
        int    *null,
        int    *kstat
#endif
);

extern int XCKRDD(
#ifdef __STDC__
        int    cid,
        char   *key,
        int    felem,
        int    maxvals,
        int    *actvals,
        double *values,
        int    *unit,
        int    *null,
        int    *kstat
#endif
);

extern int XCKRDC(
#ifdef __STDC__
        int    cid,
        char   *key,
        int    noelm,
        int    felem,
        int    maxvals,
        int    *actvals,
        char   *values,
        int    *unit,
        int    *null,
        int    *kstat
#endif
);


extern int XCKWRI(
#ifdef __STDC__
        int    cid,
        char   *key,
        int    *values,
        int    felem,
        int    maxvals,
        int    *unit,
        int    *kstat
#endif
);


extern int XCKWRR(
#ifdef __STDC__
        int    cid,
        char   *key,
        float  *values,
        int    felem,
        int    maxvals,
        int    *unit,
        int    *kstat
#endif
);

extern int XCKWRD(
#ifdef __STDC__
        int    cid,
        char   *key,
        double *values,
        int    felem,
        int    maxvals,
        int    *unit,
        int    *kstat
#endif
);


extern int XCKWRC(
#ifdef __STDC__
        int    cid,
        char   *key,
        int    noelm,
        char   *values,
        int    felem,
        int    maxvals,
        int    *unit,
        int    *kstat
#endif
);


extern int XCZINI(
#ifdef __STDC__
        char   *midasunit,
        char   *myunit,
        char   *direc,
        char   *display,
        char   *midasvers,
        int    maxtim,
        int    *cid
#endif
);


extern int XCXINI(
#ifdef __STDC__
        char   *midasunit,
        char   *host,
        char   *display,
        char   *midasvers,
        int    maxtim,
        int    *cid
#endif
);


extern int XCXOPN(
#ifdef __STDC__
        char   *midasunit,
        char   *host,
        int    *cid
#endif
);


extern int XCZOPN(
#ifdef __STDC__
        char   *midasunit,
        char   *mysunit,
        char   *direc,
        int    *cid
#endif
);


extern int XCXCLO(
#ifdef __STDC__
        int    cid
#endif
);


extern int XCXSTP(
#ifdef __STDC__
        int    cid
#endif
);


extern int XCXSTA(
#ifdef __STDC__
        int    cid
#endif
);


extern int XCXGFD(
#ifdef __STDC__
        int    cid,
        int    *fd
#endif
);

extern void inxcon(
#ifdef __STDC__
        char  *munit,
        char  *direc
#endif
);

extern int Mrunning(
#ifdef __STDC__
        char  *munit,
        int  maxtime
#endif
);

extern int msetup(
#ifdef __STDC__
        int    flag,
        char  *token1,
        char  *token2,
        char  *token3,
        char  *token4
#endif
);

extern int ClientClose(
#ifdef __STDC__
        int   idclient,
	int   *retosx
#endif
);

extern int ClientInfo(
#ifdef __STDC__
        int   idclient,
	int   *fd
#endif
);

extern int ClientWait(
#ifdef __STDC__
        int   idclient,
	int   timo
#endif
);

extern int ClientWrite(
#ifdef __STDC__
        int   idclient,
	char  *comline,
	int   *retosx
#endif
);

extern int ClientRead(
#ifdef __STDC__
        int   idclient,
	char  *comline,
	int   *bgstat,
	int   *retosx
#endif
);

extern int ClientKRead(
#ifdef __STDC__
        int   idclient,
        int   typ,
	char  *comline,
	char  *retbuf,
	int   *karr,
	int   *retosx
#endif
);

extern int ClientKWrite(
#ifdef __STDC__
        int   idclient,
        int   typ,
	char  *comline,
	char  *sendbuf,
	int   *karr,
	int   *retosx
#endif
);

extern int ClientInit(
#ifdef __STDC__
        char  *shost,
	char  *sunit,
	int   *idclient,
	int   *retosx
#endif
);

extern int outmail(
#ifdef __STDC__
        int    flag,
	char  *comline,
	int  bgindx,
        int    *retstat
#endif
);


#ifdef  __cplusplus
}
#endif

#endif

