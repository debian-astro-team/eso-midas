/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2009 European Southern Observatory
.IDENTifer   proto_mutil.h
.AUTHOR      Otmar Stahl 
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for misc routines used in FEROS
.ENVIRONment none
.VERSION     1.0    6-APR-1994 Otmar Stahl 

090416		last modif
------------------------------------------------------------*/
extern void lfit
(
#ifdef __STDC__
 double [], double [], int, double [], int, 
 void (*) (double, double *, int)
#endif
 );

extern float eval_fpoly
(
#ifdef __STDC__
 float, float *, int
#endif
);

extern double eval_dpoly
(
#ifdef __STDC__
 double, double *, int
#endif
);

extern void dpoly
(
#ifdef __STDC__
 double, double [], int
#endif
);

void dleg(double x, double pl[], int nl);
void dcheb(double x, double pl[], int nl);

extern float median
(
#ifdef __STDC__
 float [], int
#endif
 );

extern void piksrt
(
#ifdef __STDC__
 int, float []
#endif
);

double select_pos (unsigned long k, unsigned long n, double arr[]);

extern float pik_median
(
#ifdef __STDC__
 int, float []
#endif
);

extern void heap_copy
(
#ifdef __STDC__
    int, float [], float[]
#endif
);

extern int heap_compare
(
#ifdef __STDC__
 int, float [], float[]
#endif
);

extern void heap_sort
(
#ifdef __STDC__
 int, float []
#endif
);

extern float heap_median
(
#ifdef __STDC__
 int, float []
#endif
);

extern void std_sorti(
#ifdef __STDC__
  int array_size, int numbers[]
#endif
);

extern void std_sortf(
#ifdef __STDC__
  int array_size, float numbers[]
#endif
);

extern void std_sortd(
#ifdef __STDC__
  int array_size, double numbers[]
#endif
);

extern void heapSortInt(
#ifdef __STDC__
  int array_size, int numbers[], int rank[]
#endif
);

extern void heapSortFloat(
#ifdef __STDC__
  int array_size, float numbers[], int rank[]
#endif
);

extern void heapSortDouble(
#ifdef __STDC__
  int array_size, double numbers[], int rank[]
#endif
);

void fit_poly (float inimage[], float outimage[], double start,
	  double step, int npix, int fit_deg);

extern void fit_poly_weight
(
#ifdef __STDC__
 float [], float [], float [], int, float *, float *, int, int, double *
#endif
);

void nat_spline(const float * x, const float *y, int n, double * p);
double nat_spline_int(const float * x, const float * y, double * p, int n, float xv, int * last);
void nat_spline2d(const float *y, float **z, int rows, int cols, double ** p2d);
void nat_spline2d_int(const float * x, const float *y, float **z,
                        double **p2d,
                        int rows, int cols,
                        int ninterpolate, float * xpos, float ypos, float * interpolated);
double hsplint( double xp, double *x, float *y, int n, int *istart );
