C @(#)prjdec.inc	19.1 (ESO-IPG) 02/25/03 13:49:39
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C       INCLUDE FILE PRJDEC.INC                         20-Jan-1989
C---------------------------------------------------------------------
C Note: This include file is for Projection plot
C
C++
C
C  Common block for Sperical Projections
C      The parameter to be difined:
C        KPROJ     Projection 1 (tan) to 8 (Mercator)
C                  (PROJ_TAN     =1)
C                  (PROJ_TAN2    =2)
C                  (PROJ_SIN     =3)
C                  (PROJ_SIN2    =4)
C                  (PROJ_ARC     =5)
C                  (PROJ_AITOFF  =6)
C                  (PROJ_GLS     =7)
C                  (PROJ_MERCATOR=8)
C        RADIR     Direction for Right Ascension
C        AL0,AB0   Center (degrees)
C        KF0       Frame in which center is given: Q/G/S/E
C                  (FRAME_Q = 1) ! Equatorial
C                  (FRAME_G = 2) ! Galactic
C                  (FRAME_S = 3) ! Supergalactic
C                  (FRAME_E = 4) ! Ecliptic
C        EP0       Epoch (for eq/ecl) of center
C        KFD       Frame of Data coordinates
C        EPD       Epoch of Data coordinates
C        KFG       Frame of Grid
C        EPG       Epoch of Grid
C        ROT0(3,3)        Rotation matrix from KF0 to B1950
C        ROTD(3,3)        Rotation matrix from KFD frame to B1950
C        ROTG(3,3)        Rotation matrix from KFG frame to B1950
C        ROTL(3,3)        Rotation matrix to centered (local) frame
C        DIMX,DIMY        Dimensions (or diameter) in degrees
C        COSR             Max. distance from center (cosine)
C        PINIT            True if PBOX done(MONGO), here this was removed   
C        ALMAG            Mean Magnitude
C        SCMAG            Magnitude range
C        FULLP            Curve plot steps parameter
C
C------------------------------------------------------------------------------

CC+
C       Common block for Sperical Projections

        INTEGER KPROJ
        INTEGER RADIR
        INTEGER KF0
        INTEGER KFD
        INTEGER KFG
        
        REAL    EP0
        REAL    EPD
        REAL    EPG
        REAL    AL0,AB0
        REAL    ROT0(3,3)
        REAL    ROTD(3,3)
        REAL    ROTG(3,3)
        REAL    ROTL(3,3)
        REAL    DIMX,DIMY
        REAL    COSR
        REAL    ALMAG
        REAL    SCMAG 
        REAL    FULLP

CC      LOGICAL PINIT   

C  Projection transformation functions
CC      INTEGER  TRPU, TRUP
        EXTERNAL TRPU, TRUP

C+
      COMMON /PROJ/KPROJ,RADIR,DIMX,DIMY,COSR,
     1             AL0,AB0,ROTL,KF0,EP0,ROT0,
     2             KFD,EPD,ROTD,KFG,EPG,ROTG,ALMAG,SCMAG,
     3             FULLP
C    4             ,PINIT

C
C**      Include more for : ASTPROJ3, ASTPROJ1,ASTPROJ2
C

       INTEGER  SYMBT                     ! plot symbol
       INTEGER  LX1, LX2, LY1, LY2        ! AGCDEF winder 
       
       REAL     X1, X2, Y1, Y2            ! plot device winder limitation
       REAL     XL, YL                ! used in GRID(BOX) and TABLE AGWDEF(WD1)
       REAL     XLTXT,YLTXT                          ! used in TEXT AGWDEF(WD2)
       REAL     XCBOX1, XCBOX2, YCBOX1, YCBOX2 ! for GRID(BOX) and TABLE AGCDEF
       REAL     WRATIO                               ! WD1 & WD2 ratio
 
C+            
       COMMON    /PRJPLOT/LX1,LX2,LY1,LY2,
     1                    X1,X2,Y1,Y2,
     2                    XL,YL,
     3                    XLTXT,YLTXT,
     4                    XCBOX1, XCBOX2, YCBOX1, YCBOX2,
     5                    WRATIO,
     6                    SYMBT
