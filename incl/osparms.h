/*++++++++++++++++++++++++++ midas/osparms.h ++++++++++++++++++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      osparms.h
.AUTHOR              BP  [ESO-IPG]
.KEYWORDS            operating system, parameters, system structures
.ENVIRONMENT         Unix
.COMMENTS            These miscellaneous parameters are defined as keywords in
                     order to  provide auto-documentation in the programs.
.VERSION 1.0	6-Nov-1986  Creation   BP
.VERSION 2.0 	10-Mar-1987  Also set defines for standard calls BP
.VERSION 2.1 	21-Jun-1989  Modified FNAME_LEN (F. Ochsenbein) 
.VERSION 2.2 	890720	     merge the VMS osx stuff  KB
.VERSION 2.3 	19-Feb-1992  Now filestatus structure in osfile.h CG.
.VERSION 2.4 	921022       Increase _NFILE to 64 files (KB) 

 060406		last modif
--------------------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE 1
#endif

#ifndef  OSPARMS_DEF 
#define  OSPARMS_DEF          /* Indicates inclusion of OSPARMS header */

#include <oserror.h>

#ifndef F_OK
#define F_OK 0
#endif


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   the following constant definitions do comply with the X/OPEN standard
   definition and allows to avoid the use of the local system include
   files (which are not always complete) */

#define    PROCLOCK          1    /* lock test and data into the memory */
				  /* (For plock(2)) */
#define    UNLOCK            0    /* unlock all segments */
#define    TXTLOCK           2    /* lock text segment only */
#define    DATLOCK           4    /* lock data segment only */

#define    FTW_F             0    /* walk on files (for ftw(3)) */
#define    FTW_D             1    /* walk on directories */
#define    FTW_DNR           2    /* walk on directories with no read perm. */
#define    FTW_NS            3    /* unknown type stat failed */

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    Data type length definition.
*/

#define   INT_SIZE      4
#define   FLOAT_SIZE    4
#define   DOUBLE_SIZE   8
#define   CHAR_SIZE     1 

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    System parameters
*/

#define   SYS_PAGE_SIZE 512

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
   Particular names for the os routines
*/
/* for          osa_routines             */

#define  _NFILE_      64   /* maximum number of files to be open simult. */

/* for          osd_routines             */

#ifndef FNAME_LEN
#define  FNAME_LEN 128
#endif

/* for          ose_routines             */

#define _NFLAGS     128
#define FLAG_KEY      1

/* for          osh_routines             */

#define   INIT_CPU_CLOCK   INIT_CLOCK 
#define   GET_CPU_CLOCK    GET_CLOCK
#define   INIT_CLOCK       0
#define   GET_CLOCK        1

/* for          osm_routines             */

#define  LOCK   0
#define  UN_LOCK 1

#define  READ         0
#define  WRITE        1
#define  READ_WRITE   2
#define  APPEND       3

#define  _NMAPPED   256

#define  PRIVATE      0

/* for          osp_routines             */

#define  LOWER_PRI    15
#define  HIGHER_PRI  -15
#define  _NPROC	      10
#define  _PROC_NM_LEN 20
#define  FOREGRD       0
#define  BACKGRD       1
#define  _EXERR      127	/* an impossible exit code */

/* for          oss_routines             */

#include <signal.h>

/* Signals allowed for the user (send) */

#define  USR1         0
#define  USR2         1
#define  TERM         2
#define  ALRM         3

/* for          ost_routines             */

/* --- The following was removed , Francois Ochsenbein
#define  ostopen   sm_xv_init
#define  ostclose  sm_xv_close
#define  ostread   sm_xv_in
#define  ostcread  sm_xv_cin
#define  ostwrite  sm_xv_out
--------- End of Removed		*/


/* for          osx_routines             */
#define  MAX_IPCC     32        /* maximum number of IPC channels in use */

/* open modes : */

#define  LOCAL        00	/* The call to the osxopen interface */
#define  NETW         02	/* with a combination of two of these */
#define  IPC_READ     00	/* switches allows the choice of the */
#define  IPC_WRITE    01	/* right open mode. i.e. LOCAL | SOCK_READ */
#define  CLIENT       04	/* special flag for client open (no priv.) */

/* queue lengths : */

#define  LOCAL_QUEUE_LENGTH  5	/* queue length on local proccesses */
#define  NETW_QUEUE_LENGTH   5	/* queue length on network proccesses */

/* returned values from osxinfo */

#define NODATA  0
#define DATARDY 1
#define NOCONN  2

/* communication channels status : */

#define  WAIT_ON_WRITE       0
#define  WAIT_ON_READ        1
#define  OPENED_READ         2
#define  OPENED_WRITE        3
#define  CLOSED              4
#define  IDLE                5

/* interprocess communication structure */

struct  ipccstat {		/* structure socket-status */
    char  *chname;		/* channel name */
    char  *phname;		/* second name/host name */
    int   omode;		/* open mode (either IPC_READ or IPC_WRITE) */
    int   type;			/* LOCAL or NETW */
    int   status;               /* status of the channel */
    int   accept;		/* descriptor returned by accept */
};


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/* miscellaneous structures : */

/* 1 : process status structure */

struct prstatus			/* process status structure tag */ 
	{
	int  pid;			/* process identification */
	int  bflg;			/* background flag (0-foreg, 1-backg) */
	char procname[_PROC_NM_LEN];	/* process name */
	};

/* 2 : file status structure. Now in osfile.h */

/* 3 : logical name structure */

#define _LPLEN 32
#define _LFLEN 16
#define _LTLEN 3
struct lname {			/* structure lname (tbd) */
	char lpath[_LPLEN];	/* logical path */
	char lfile[_LFLEN];	/* logical file */
	char ltype[_LTLEN];	/* file type */
	int  version;		/* version number */
	};
/* 6 : Date structure */

#ifndef vms
# include <sys/types.h>
# include <sys/stat.h>
#else
# include <stat.h>
# define S_ISCHR(m)      (((m)&S_IFMT) == S_IFCHR)
# define S_ISREG(m)      (((m)&S_IFMT) == S_IFREG)
#endif

#ifndef ENAMETOOLONG
#define ENAMETOOLONG ENOENT
#endif

#endif /* OSPARMS_DEF */
