/***********************************************************************
*                                                                      *
*   file idistruct.h                                                   *
*   IDI  Data Structure definition                                     *
*                                                                      *
************************************************************************
*   V 1.1   871201:  P. Santin  - Trieste Astronomical Observatory     *
*   V 2.0   881219:  K. Banse   - ESO Garching (from now on)           *
* 								       *
* 070420		last modif				       *
* 150827		Xworkst.name -> 120 chars  (KB)		       *
***********************************************************************/
  
#ifndef STRUCT_IDI
#define STRUCT_IDI

struct      lut_bar          /*  LUT bar structure              */
    {
    int     wp;		     /* window pointer                  */
    int     vis;             /* current visibility              */
    int     xoff;            /* offset in x                     */
    int     yoff;            /* offset in y                     */
    int     xsize;           /* size in x                       */
    int     ysize;           /* size in y                       */
    };
typedef struct lut_bar   INTBAR;


struct      glist        /* graphic display structure */
    {
    int     geln;        /* element sequential no.              */
    int     maxpnt;      /* max no. of points                   */
    int     *x;          /* coordinates                         */
    int     *y;  
    int     *color;      /* color for drawing                   */ 
    int     *lwidth;     /* linewidth                           */ 
    int     *off;        /* offsets for coords                  */ 
    int     *count;      /* no.of points                        */
    };
typedef struct glist GLIST;


struct      tlist        /* text display structure */
    {
    int     teln;              /* element sequential no.        */
    int     x[MAX_TXT];        /* start coordinates             */
    int     y[MAX_TXT];  
    int     off[MAX_TXT];      /* offsets for text strings      */ 
    int     count[MAX_TXT];    /* no.of chars                   */
    int     size[MAX_TXT];     /* text size used                */
    int     color[MAX_TXT];    /* text color used               */
    char    text[MAX_TXTLEN];  /* buffer for of chars           */
    };
typedef struct tlist TLIST;


struct      lut_data    /* LUT data structure                */
    {                                                           
    int     lutr[256];  /* LUT Red data                      */
    int     lutg[256];  /* LUT Green data                    */
    int     lutb[256];  /* LUT Blue data                     */
    int     vis;        /* visibility flag (1 or 0)          */
    };                                                          
typedef struct lut_data LUT_DATA;


struct      itt_data    /* ITT data structure                */
    {                                                           
    int     val[256];   /* ITT data values                   */
    int     vis;        /* visibility flag (1 or 0)          */
    };                                                          
typedef struct itt_data ITT_DATA;


struct      curs_data  /* cursors data structure               */
    {
    int     sh;    /* cursor shape                             */
    int     col;   /* cursor color                             */
    int     vis;       /* cursor visibility                    */
    int     xpos;     /* X cursor position in IDI space!       */
    int     ypos;     /* Y cursor position                     */
    };
typedef struct curs_data CURS_DATA;


struct      roi_data   /* cursors data structure               */
    {
    int     col;       /* roi color                            */
    int     sh;        /* roi shape                            
                          = 0 for rectangle, = 1 for circle    */
    int     vis;       /* roi visibility                       */
    int     xmin;     /* X min in IDI space!                   */
    int     ymin;     /* Y min                                 */
    int     xmax;     /* X max                                 */
    int     ymax;     /* Y max                                 */
    int     radiusi;  /* inner radius                          */
    int     radiusm;  /* middle radius (maybe = 0)             */
    int     radiuso;  /* outer radius (maybe = 0)              */
    int     radno;    /* flag for currently active radius      */
    };
typedef struct roi_data ROI_DATA;


struct      inter_data /* interactions data structure           */
    {
    int     inttype;     /* interactor type                     */
    int     intid;       /* interactor id                       */
    int     objtype;     /* object type                         */
    int     objid;       /* object id                           */
    int     oper;        /* interactive operation               */
    int     interactor;  /* connected interactor                */
    };
typedef struct inter_data INTER_DATA;


struct	    mem_data    /* memory data structure                */
	{
    char    *mmbm;      /* main memory bitmap                   */
    char    *zmbm;      /* zoom memory bitmap                   */
    int     pixmap;     /* = 1, if we have a Pixmap, else = 0   */
    int     visibility; /* memory visibility flag               */
    int     xsize;      /* memory x size                        */
    int     ysize;      /* memory y size                        */
    int     depth;      /* memory depth                         */
    int     type;       /* memory type                          */
    int     xwoff;      /* image data offset                    */
    int     ywoff;      /*        "                             */
    int     xwdim;      /* image data dimensions                */
    int     ywdim;      /*        "                             */
    int     load_dir;   /* transfer window load direction       */
                        /* 0 = bottom->top / 1 = top->bottom    */
    int     lut_id;     /* current lookup                       */
    struct glist  *gpntr;  
                        /* graphic display list pointer         */
    struct tlist  *tpntr;  
                        /* graphic text list pointer            */
    int     xscroll;    /* x scroll position                    */
    int     yscroll;    /* y scroll position                    */
    int     zoom;       /* zoom factor                          */
    int     xscale;     /* x scaling factor			*/
    int     yscale;     /* y scaling factor			*/
    int     sspx;       /* x start screen pixel                 */
    int     sspy;       /* y start screen pixel                 */
    int     nsx;        /* no. memory pixels actually used in x */
    int     nsy;        /* no. memory pixels actually used in y */
    int     sfpx;       /* x start frame pixel                  */
    int     sfpy;       /* y start frame pixel                  */
    int     source;     /* source for filling memory,
			   0 = clear/channel
			   1 = load/image
			   2 = load/image with overwrite option */
    int     plane_no;   /* no. of loaded plane (if 3-dim image) */
    char    frame[80];	/* name of loaded frame (source > 0)    */
    float   rbuf[8];	/* start, end, ... of loaded frame     */
    struct  itt_data *ittpntr;   /* pointer to ITT structure    */
    };
typedef struct mem_data MEM_DATA;


struct alph_data
   {
   int      savx[16];
   int      savy[16];
/*   char     savtxt[320]; */
   char     savtxt[480];
   };
typedef struct alph_data ALPH_DATA;


struct locator
   {
   int      lowsp;      /* low speed                           */
   int      spfact;     /* speed factor                        */
   int      speed;      /* actual speed                        */
   int      xpos;       /* locator x position in X11 space!    */
   int      ypos;       /* locator y position                  */
   int      xdif;       /* locator x displacement              */
   int      ydif;       /* locator y displacement              */
   int      interactor;	/* interactor connected to this locator*/
   };
typedef struct locator LOCATOR;


struct trigger
   {
   int     def;         /* trigger definition                  */
   int     interactor;	/* interactor connected to this trigger*/
   };
typedef struct trigger TRIGGER;


struct     conf_data    /* config. data structure               */
    {
    int     nmem;       /* number of memories                   */
    int     memid;      /* current memory id                    */
    int     overlay;    /* id of graphics/overlay memory        */
    int     RGBmode;    /* 1 for TrueColor, 0 for PseudoColor, 
			   2 for PseudoColor on DirectColor,
			   3 for PseudoColor on TrueColor       */
    struct  mem_data *memory[MAX_MEM];
                        /* image memory structure               */
    struct  alph_data *alpmem[MAX_MEM];
                        /* alpha memory structure               */
    };
typedef struct conf_data CONF_DATA;


struct      dev_data    	/* device data structure              */
    {
    char    devname[9]; 	/* display name                       */
    char    devtyp;     	/* display type    i - image display w.
						   g - graphics w.
						   s - image shadow w.
						   t - graphics shadow w.
						   z - zoom w.
						   c - cursor w.      */
    char    ref;		/* reference display, if we are a shadow */
    int     opened;     	/* display is opened                   */
    int     screen;    		/* screen (Xworkst) no.               */
    int     xsize;      	/* display x_size                     */
    int     ysize;      	/* display y_size                     */
    int     depth;      	/* display depth                      */
    int     ncurs;      	/* number of cursors                  */
    struct  curs_data *cursor[MAX_CURS];
    struct  roi_data  *roi;
    struct  lut_data *lookup;   /* lookup structure           */
    int     lutsect;    	/* LUT section: 0 -> MAX_LUT-1        */
    int     lutoff;     	/* LUT offsets (X11 feature)          */
    struct  conf_data *confptr; /* config. structure          */
    int     n_inter;    	/* no. of enabled interactions        */
    int     trigger;    	/* exit trigger                       */
    struct  inter_data *inter[MAX_INTER];
                        	/* interaction structure              */
    long int inter_mask; 	/* event mask for interactions	      */
    struct  lut_bar  *bar;
    int     alpno;      	/* if >= 90, we have an alpha memory  */
    int     alphx;		/* x-size of alpha window */
    int     alphy;		/* y-size of alpha window */
    int     alphxscal;		/* x-scale factor of alpha window */
    int     alphlinsz;		/* pixels per line of alpha window */
    char    *hcopy;     	/* pointer to memory allocated for hardcopy */
    int     shadow[MAX_DEV];	/* if >= 0, we have a shadow display  */
    int     curswin;    	/* if >= 0, we have a cursor window */
    unsigned long int backpix;	/* background pixel */
    short int link[2];	    	/* link[0] = 0/1 if not/yes has subwindows */
				/* link[1] = id of parent, if a subwindow */
    }
    ididev [MAX_DEV];


struct
   {
   int      visual;   	/* = 2 for PseudoColor, 
   		  	   = 3 for DirectColor/TrueColor;
   		  	   = 4 for emulated PseudoColor on Direct/TrueColor */
   int      RGBord;   	/* = 0 for 0x0000ff (red), 0x00ff00 (green) and
                                   0xff0000 (blue),
                           = 1 for 0xff0000 (red), 0x00ff00 (green) and
                                   0x0000ff (blue)             */
   int      lutflag;   	/* > 0 if we use private colormap,
			   = 0 if we use default colormap      
			   < 0 if we use no colormap           */
   int      ownlut;     /* own LUT flag 0, 1 or -1 (companion mode) 
			   if visual = 3 or 4, 
			   ownlut = 1/0 indicates Direct/TrueColor  */
   int      auxcol;   	/* no. of colors used for plot + from system  */
   int      width;   	/* width of Xworkstation in pixels     */
   int      height;   	/* height of Xworkstation in pixels    */
   int      depth;   	/* depth of Xworkstation in bits       */
   int      nobyt;      /* no. of bytes used per pixel         
                           no. of bits per pixel, if < 0	*/
   int      flag24[3];  /* 24bit flags (write,read,special)	*/
   int      fixpix[9];  /* pixel values of fixed colours       */
   int      mapin[256*MAX_LUT];	 /* for noncontiguous LUTs (in) */
   int      mapout[256*MAX_LUT]; /* for noncontiguous LUTs (out)*/
   int      nolut;      /* no. of  LUTs (of size 'lutsize')    */
   int      lutsize;    /* size of LUT                         */
   int      lutlen;     /* size of LUT actually used ...
			   = lutsize - auxcol                  */
   float    lutfct;	/* factor for LUT colour conversion    */
   float    lutinv;	/* inverse of lutfct		       */
   unsigned long int black;
   unsigned long int white;  /* black, white colour of station */
   unsigned char blmask; /* full byte of black pixels          */
   char     name[120];	/* X display name of workstation       */
   char     miduni[4];	/* MIDAS unit                          */
   }
   Xworkst[MAX_WST];


struct 
   {
   int   nloc;		/* no. of locators 		       */
   struct locator  *loc[MAX_LOC];
   int   ntrig;         /* no. of triggers                     */
   struct trigger  *trig[MAX_TRG];
   }
   intdevtable[MAX_DEV];


int auto_cursor_fid, record_cursor_fid;

#endif
