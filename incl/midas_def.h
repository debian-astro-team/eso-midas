/* --------------------- MIDAS definition Table ---------------------------
	
                      in file MID_INCLUDE:midas_def.h

..VERSION	880414:  creation  (KB)

 130610		last modif
--------------------- MIDAS definition Table --------------------------- */
	

/*  Error definitions:   
 
  Warning + information messages are numbered < 0 .
  Error messages are numbered > 0 .

  Updated error codes have to be reflected in errtable.h + ../errpar.dat
*/


#ifndef MIDAS_DEF	/* === Just to avoid multiple includes === */
#define MIDAS_DEF

#ifdef vms		/* VMS does not implement the GNU readline library */
#define NO_READLINE
#endif

#include <fcntl.h>		/* needed for defs of off_t, ... */
/* since glibc 2.17  we also need stddef.h at least for  size_t ... */
#include <stddef.h>

#ifndef struct_DEF
#define struct_DEF
#include <fctext.h>
#include <fcbext.h>
#include <ldbext.h>
#endif

#include <stdio.h> 
#include <unistd.h> 

#include <proto_st.h>   /* ANSI-C prototyping for STandard interfaces */
#include <proto_tbl.h>  /* ANSI-C prototyping for TaBLe interfaces */
#include <proto_os.h>   /* ANSI-C prototyping for OS interfaces */
#include <proto_plt.h>  /* ANSI-C prototyping for PLoT interfaces */
#include <proto_dsp.h>  /* ANSI-C prototyping for DiSPLay interfaces */
#include <proto_gen.h>  /* ANSI-C prototyping for GENeral interfaces */
#include <proto_xcon.h> /* ANSI-C prototyping for XConnection interfaces */
#include <proto_ctab.h> /* ANSI-C prototyping for CTAB interfaces */
#include <proto_agl.h> /* ANSI-C prototyping for CTAB interfaces */

#define	ERR_NORMAL   0  /* successful return status */
#define	ERR_DSCNPR   1  /* descriptor not present */
/*  error no. 2 is free ... */
#define	ERR_LOGFIL   3  /* problems with logfile */
#define	ERR_OPSSYS   4  /* Operating system error */
#define	ERR_DSKFUL   5  /* disk full */
#define	ERR_FRMNAC   6  /* frame not accessible */
#define	ERR_INPINV   7  /* input invalid */
#define	ERR_CATOVF   8  /* overflow in general catalog */
#define	ERR_DSCBAD   9  /* descriptor bad */
#define	ERR_KEYBAD  10  /* keyword bad */
#define	ERR_KEYOVL  11  /* overflow in keyword data area */
#define	ERR_KEYOVN  12  /* overflow in keyword name table */
#define	ERR_FILNAM  13  /* invalid syntax for file name  */
#define	ERR_FILBAD  14  /* file handling error */
#define	ERR_CATBAD  15  /* bad catalog */
#define	ERR_MEMOUT  16  /* out of memory */
#define	ERR_FILPRO  17  /* file protection */
#define	ERR_FMTBAD  18  /* bad (not matching) binary data format */
#define	ERR_VERNOR  19  /* unrecognized Midas file version */
#define	ERR_CBYTES   -1  /* bytes per element are not exact */
#define	ERR_DSCOVF   -2  /* descriptor data overflow ( > 65535) */
#define	ERR_NODATA   -3  /* no data available */
#define	ERR_KEYTYP   -4  /* wrong type of keyword */
#define	ERR_CATENT   -5  /* no entry in catalog */
#define	ERR_MESOVF   -6	 /* overflow in error message stack */
#define	ERR_LOSPRC   -8  /* loss of prec.: double data -> real descr */
#define	ERR_DSCTYP   -9  /* different descr.type than requested */

 
#define	ERR_TBLFUL 20  /* Table errors start at 20 */
#define	ERR_TBLMEM 21  /*  error allocating dynamic mem. */
#define	ERR_TBLMAP 22  /*  error mapping the table */
#define	ERR_TBLCOV 23  /*  column overflow */
#define	ERR_TBLENT 24  /*  table not found wrong init. */
#define	ERR_TBLCOL 25  /*  wrong column number */
#define	ERR_TBLROW 26  /*  wrong row number */
#define	ERR_TBLKEY 27  /*  identifier not found */
#define	ERR_TBLFMT 28  /*  error in column format */
#define	ERR_TBLIMP 29  /*  not implemented */
#define	ERR_TBLRFM 30  /*  error reformatting table */
#define	ERR_TBLDNM 31  /*  duplicate table name */
#define	ERR_TBLABL 32  /*  illegal label */
#define	ERR_TBLINI 33  /*  error in table init */
#define	ERR_TABVER 34  /*  old VMS Midas table format  */

/* here space for 2 more error codes, 
   for more ERRTAB (errtable.h) has to be extended */

#define	ERR_FITEXT  37  /* invalid FITS extension */
#define	ERR_INSBAD  38  /* bad Midas installation */
#define	ERR_OUTLIM  39  /* 1. elem or no.elem out of bounds */
#define	ERR_NOCLON  40  /* frame not clonable */


/*        Character buffer length definitions:               */

#define KEYNAME_LEN   15
#define DESCRNAME_LEN 40
#define MODE_LEN       6
#define TYPENAME_LEN  14
#define TEXT_LEN      84
#define COPY_LEN     200

#ifndef FNAME_LEN
#define FNAME_LEN    128
#endif

#define XFNAMC       '"'


/* 
             all following definitions have to be reflected in files
             st_def.inc + st_dat.inc
*/

/*        Mode definitions:                                  */

#ifndef F_MODES			/* CGS: Also defined in tbldef.h */
#define F_MODES
#define F_TRANS      0		/* table transposed format */
#define F_RECORD     1		/* table record format */

#define F_I_MODE     0		/* map file for input only    */
#define F_O_MODE     1		/* map file for output        */
#define F_IO_MODE    2		/* map file for updating      */
#define F_U_MODE     2		/* map file for updating      */
#define F_X_MODE     9		/* create/map virtual memory  */
#endif
#define F_H_MODE     7		/* create header file only */
#define F_FO_MODE    11		/* map FITS file for output   */

#ifndef F_MAP_FORCE
#define F_MAP_FORCE 0x10	/* Force the mapping (modifier) 	*/
#define F_EIO_FORCE 0x20	/* Force the elementary i/o (modifier)*/
#define F_ALL_FORCE 0x40        /* Force the allocated values   */
#endif 


/*        File parameter definitions:                             */


#define F_XD_PARM    0		/* create file with Xlarge dsc area */
#define F_DEL_PARM   1		/* set delete flag for extracted */
#define F_FITS_PARM  2		/* set flags for handling FITS headers */


/*        File type definitions:                             */


#define F_OLD_TYPE	0		/* use old type of files	*/
#define F_IMA_TYPE	1		/* type no. for image files	*/
#define F_ASC_TYPE	2		/* type no. for ASCII files	*/
#define F_TBL_TYPE	3		/* type no. for table files	*/
#define F_FIT_TYPE	4		/* type no. for fit files	*/
#define F_FIMA_TYPE	11		/* type no. for FITS images     */
#define F_FTBL_TYPE	13		/* type no. for FITS tables     */
#define F_FFIT_TYPE	14		/* type no. for FITS fit files	*/


/*         Data Format definitions:                    */


#define D_OLD_FORMAT    0
#define D_I1_FORMAT     1		/* I*1 = 1 byte  		*/
#define D_I2_FORMAT     2		/* I*2 = 16 bit integer 	*/
#define D_UI2_FORMAT  102		/* I*2 = 16 bit unsigned integer */
#define D_I4_FORMAT     4		/* I*4 = 32 bit integer		*/
#define D_R4_FORMAT    10		/* R*4 = 32 bit floating point	*/
#define D_R8_FORMAT    18		/* R*8 = 64 bit floating point	*/
#define D_L1_FORMAT    21		/* L*1 = 1 byte logical		*/
#define D_L2_FORMAT    22		/* L*2 = 16 bit logical 	*/
#define D_L4_FORMAT    24		/* L*4 = 32 bit logical		*/
#define D_C_FORMAT     30		/* 1 byte character 		*/
#define D_X_FORMAT     40		/* 1 byte flags 		*/
#define D_P_FORMAT     50		/* pointers     		*/


/*         Direction definitions:                    */


#define X_Y_PLANE    10
#define X_Z_PLANE    11
#define Z_Y_PLANE    12
#define LINE_X       13
#define LINE_Y       14
#define LINE_Z       15



/*         text color definitions:                    */


#define M_DEF_COLOR    0
#define M_BLACK_COLOR  1
#define M_RED_COLOR    2
#define M_GREEN_COLOR  3
#define M_BLUE_COLOR   4
#define M_YELLOW_COLOR 5
#define M_MAGNT_COLOR  6
#define M_CYAN_COLOR   7
#define M_WHITE_COLOR  8

#endif

/* use builtin functions for gcc */

#ifdef GCC
#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

#else
#define likely(x)       x
#define unlikely(x)     x
#endif



/*  ------------------------------------------------------------------  */
