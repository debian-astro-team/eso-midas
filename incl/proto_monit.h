/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2010 European Southern Observatory
.IDENTifer   proto_monit.h
.AUTHOR      K. banse	ESO - Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for Monitor modules,
               resp.: prepa1.c, prepa2.c, prepb.c, ..., prepx.c

.ENVIRONment none

.VERSION  [1.00]  23-April-2001   Creation by K. Banse

 100608		last modif
------------------------------------------------------------*/

#ifndef  PROTO_MONIT               /* Avoid redefinitions */
#define  PROTO_MONIT       0

#ifdef  __cplusplus
extern "C" {
#endif

 
extern void intermail( 
#ifdef __STDC__
#endif
);

extern void init_here( 
#ifdef __STDC__
	char  *cp,
	int  *ip
#endif
);

extern int inmail( 
#ifdef __STDC__
	int    flag,
	char   *cpntr, 
	int    *pntr
#endif
);
 
extern void busymail( 
#ifdef __STDC__
#endif
);
 
extern void trigback( 
#ifdef __STDC__
	int    idx
#endif
);

extern int showback( 
#ifdef __STDC__
	char   *munit, 
	int    disp
#endif
);

extern void waitback( 
#ifdef __STDC__
	char   *token, 
	int    *restat
#endif
);

extern int setback( 
#ifdef __STDC__
	char   *token
#endif
);
 
extern int break_line( 
#ifdef __STDC__
	int   *more_flag,
	char  *remaind
#endif
);
 
extern void free_cmd_list();

extern void update_cmd_list();
 
extern void close_xhelp();
 
extern int Parse3(
#ifdef __STDC__
        int start
#endif
);

extern void TTSET(
#ifdef __STDC__
        int   flg
#endif
);

extern void TTINIT(
#ifdef __STDC__
        int   hdr
#endif
);
 
extern int TTEDIT(
#ifdef __STDC__
	char *line,
        int len
#endif
);

extern void TTPRO(
#ifdef __STDC__
	char *prompt,
	char *line,
        int   len
#endif
);

extern void tinfo(
#ifdef __STDC__
        int   *tcols,
        int   *tlines
#endif
);
 
extern int NUMBIZ(
#ifdef __STDC__
        int *curcom
#endif
);
 
extern int REDOBIZ(
#ifdef __STDC__
        int nbra
#endif
);

extern int MYREAD(
#ifdef __STDC__
#endif
);

extern int COM_WINDOW(
#ifdef __STDC__
	char  *action,
	int *curcom
#endif
);

extern int cmw_read(
#ifdef __STDC__
	int *curcom
#endif
);

extern int cmw_write(
#ifdef __STDC__
	int parflag,
	int curcom
#endif
);

extern void cmw_clear(
#ifdef __STDC__
#endif
);

extern int sort_it(
#ifdef __STDC__
#endif
);

extern int ishostcom(
#ifdef __STDC__
        char   *string
#endif
);

extern void MYBATCH( 
#ifdef __STDC__
	char *cmd,
	char *procedu
#endif
);
 
extern void fixout( 
#ifdef __STDC__
	int  run,
	int  curlevl
#endif
);
 
extern void ECHO_line( 
#ifdef __STDC__
	char *cptr,
	int  lc,
        int lv
#endif
);
 
extern int PARSE( 
#ifdef __STDC__
	int   sw,
	int   parsecho,
	int level
#endif
);

extern int SAVE_PARM( 
#ifdef __STDC__
	int   flag,
	int   *pindx
#endif
);

extern void CROSS_PARM( 
#ifdef __STDC__
#endif
);
 
extern void CLEAR_LOCAL( 
#ifdef __STDC__
	int  level
#endif
);
 
extern void LOG_line( 
#ifdef __STDC__
	char *cptr,
	int lc
#endif
);

extern int Parse2( 
#ifdef __STDC__
	int   sww,
	int   start
#endif
);
 
extern void FRAMACC( 
#ifdef __STDC__
	char actio,
	char *filnam,
	int  ity,
	int  *entrx
#endif
);

extern int PIXEL_ACCESS( 
#ifdef __STDC__
	int   flag,
	char *string,
	float *value
#endif
);
 
extern int CODE_ALLOC(
#ifdef __STDC__
	int  size
#endif
);
 
extern void CODE_FREE( 
#ifdef __STDC__
	int  level,
	int  indx
#endif
);

extern int INTERNAL( 
#ifdef __STDC__
	char  *action,
	char  *name,
	int   *nobytes
#endif
);
 
extern int opti_code( 
#ifdef __STDC__
	int  *nb,
	int  option,
	int  dflag
#endif
);

extern int is_label(
#ifdef __STDC__
        char   *pntra,
        char   *pntrb
#endif
);

extern int noprocess(
#ifdef __STDC__
        char   *label,
        char   *kpntr
#endif
);

extern int DO_KEYS( 
#ifdef __STDC__
	char  key_task,
	char  *prompt
#endif
);

extern int IMMEDIATE( 
#ifdef __STDC__
#endif
);
 
extern int TABLE_ACCESS( 
#ifdef __STDC__
	int  flag,
	char  *string,
	int   *ival,
	float *rval,
	char  *cval,
	double *dval,
	char  *type,
	int   *size
#endif
);
 
extern void DESCR_ACCESS( 
#ifdef __STDC__
	int  flag,
	char  *string,
	int   *ival,
	float *rval,
	char  *cval,
	double *dval,
	char  *type,
	int   *elem,
	int   *size
#endif
);

extern int EVALU( 
#ifdef __STDC__
	int   nt
#endif
);

extern void KEYFUNC( 
#ifdef __STDC__
	char  *parm,
	int   lparm,
	int   *ires,
	float *rres,
	char  *cres,
	double *dres,
	int    *crlen,
	char  *restype
#endif
);
 
extern void Replace_it( 
#ifdef __STDC__
	char  *parm,
	int   *lparm,
	int   maxout,
	int   *ibuf,
	float  *rbuf,
	double *dbuf,
	char   *datatype
#endif
);

extern int REPLACE( 
#ifdef __STDC__
        char  *parm,
        int   *lparm,
        int   maxout
#endif
);
 
extern void REPFORM( 
#ifdef __STDC__
	char  type,
	int  *ival,
	float *rval,
	double *dval,
	int   lv,
	char  *string,
	int   *lstr
#endif
);

extern int KEY_ACCESS( 
#ifdef __STDC__
	char  *parm,
	int   *ibuf,
	float  *rbuf,
	char   *cbuf,
	double *dbuf,
	char   *type,
	int    csiz
#endif
);

extern int GETOP( 
#ifdef __STDC__
	char  *parm,
	int   lparm,
	int   *ibuf,
	float  *rbuf,
	char   *cbuf,
	double *dbuf,
	size_t *sbuf,
	char   *type,
	int    csiz
#endif
);

extern int KGN_INDEXS( 
#ifdef __STDC__
	char  *s,
	char  *t
#endif
);

extern int COMPILE( 
#ifdef __STDC__
	int   comflg,
	int   *offset
#endif
);

extern int PARDEFS( 
#ifdef __STDC__
	char  action,
	char  *comqual
#endif
);

extern int SETMID( 
#ifdef __STDC__
	int   *info
#endif
);
 
extern void DIR_Expand( 
#ifdef __STDC__
	char  *instr,
	char  *outstr
#endif
);
 
extern void disp_midvals( 
#ifdef __STDC__
	int  kidx,
	char  *optio
#endif
);

extern int INITCOM( 
#ifdef __STDC__
#endif
);
 
extern void EXTRACOM( 
#ifdef __STDC__
	char  *input,
	char  *comnd,
	char   *qualif
#endif
);

extern int FINDCOM( 
#ifdef __STDC__
        char  *comnd,
        char   *qualif,
	char   *defqual,
	int   *defset,
	int   *ctxno,
	char  **line,
	int   *linelen
#endif
);

extern int ADDCOM( 
#ifdef __STDC__
        char  *comnd,
        char   *qualif,
	int   type,
	int   lenoff,
	char  *line
#endif
);

extern void CLEANCOM( 
#ifdef __STDC__
	int  ctxno
#endif
);
 
extern void LENCOM( 
#ifdef __STDC__
        char  *comnd,
        char   *qualif,
	int  *clen,
	int   *qlen
#endif
);

extern int DELCOM( 
#ifdef __STDC__
        char  *comnd,
        char   *qualif
#endif
);

extern int SETDFF( 
#ifdef __STDC__
        char  *comnd,
        char   *qualif,
	int   flag

#endif
);

extern int COMUSED( 
#ifdef __STDC__
	int   koff
#endif
);
 
extern void cut_links( 
#ifdef __STDC__
#endif
);

extern int SHOWCOM( 
#ifdef __STDC__
	int   fp,
	char  *option,
	char  *qualif
#endif
);
 
extern int PACKCOM( 
#ifdef __STDC__
#endif
);
 
extern void MODOFF( 
#ifdef __STDC__
	int  off,
	int  m
#endif
);
 
extern void spitout( 
#ifdef __STDC__
	int  tp,
	char  *rbuf
#endif
);
 
extern void INPREPA( 
#ifdef __STDC__
#endif
);
 
extern void RUN_IT( 
#ifdef __STDC__
	char  *image
#endif
);

extern int DCLOP( 
#ifdef __STDC__
	char  *dcl
#endif
);

extern int KEYCOMP( 
#ifdef __STDC__
	int   optio,
	int   nt,
	char  *rettype
#endif
);

extern int TOKBLD( 
#ifdef __STDC__
	int   indx,
	char  *buff,
	int   lbuff,
	int   skip,
	int   lindx
#endif
);

extern int COMPU( 
#ifdef __STDC__
	int   noop,
	char  *rtype,
	int  *ii,
	float  *rr,
	char  *cop1,
	int   *cclen,
	double *dd,
	size_t *ss
#endif
);
 
extern void KEY_PARSE( 
#ifdef __STDC__
	char  *parm,
	char  *key,
	char  *type,
	int  *bytelm,
	int *elem,
	int  *first,
	int  *last
#endif
);

extern void PARSE_ELEM(
#ifdef __STDC__
        char  *string,
        int  fp,
        int  lp,
        char  *type,
        int  bytelm,
        int *elem,
        int  *first,
        int  *last
#endif
);

extern void DSCR_PARSE(
#ifdef __STDC__
	int  imno,
        char  *parm,
        char  *descr,
        char  *type,
        int  *bytelm,
        int *elem,
        int  *first,
        int  *last
#endif
);

extern int ascfiles( 
#ifdef __STDC__
	int   flag,
	int   *bad
#endif
);
 
extern void PREPERR( 
#ifdef __STDC__
	char  *source,
	char  *message,
	char  *err_str
#endif
);

extern void TABLE_PARSE(
#ifdef __STDC__
	int  flag,
        char  *string,
        int  *ival,
	float  *rval,
	char  *cval,
	double *dval,
        char  *type,
        int  *size
#endif
);

extern int build_prg( 
#ifdef __STDC__
	int   flag,
	int   spec_flg
#endif
);

extern int K_LOCK( 
#ifdef __STDC__
	int   flag
#endif
);

extern int SYNCHRO( 
#ifdef __STDC__
	char  *qualf
#endif
);

extern int CREA_COM( 
#ifdef __STDC__
	char  *qualf
#endif
);

extern int STORE_FR( 
#ifdef __STDC__
	int   *wk_flag
#endif
);

extern int WAIT_SECS( 
#ifdef __STDC__
	char  *string
#endif
);
 
extern void WRITE_QU( 
#ifdef __STDC__
	char  *qualif,
	int   *retval
#endif
);

extern int DEFIPAR( 
#ifdef __STDC__
#endif
);

extern int TYPE_CHECK( 
#ifdef __STDC__
	int   parno
#endif
);

extern int OSYCOMP( 
#ifdef __STDC__
	char  *comstring
#endif
);

extern int DEBUGGY( 
#ifdef __STDC__
	int   actio,
	char   *qualif
#endif
);

extern int Contexter( 
#ifdef __STDC__
	int   actio
#endif
);

extern int prepx(
#ifdef __STDC__
	int  kswitch,
        char  *cxpntr,
	int  *ixpntr
#endif
);

extern int KEXP_CLEAN(
#ifdef __STDC__
        char  *instring,
        char  *outstring,
        int   maxcnt,
	char  **atom,
	int  *latom
#endif
);

extern int KEXP_ATOM(
#ifdef __STDC__
        char  *input,
        char  *delim,
	char  *atom
#endif
);

extern int KEXP_REDUCE(
#ifdef __STDC__
        char  *input,
        char  *output,
        char  *operation
#endif
);

extern int KEXP_CLASSIFY(
#ifdef __STDC__
        char  input,
        int  *ipr,
	int  *spr
#endif
);

extern int KEXP_POLISH(
#ifdef __STDC__
        char  *instring,
        char  *outstring
#endif
);

extern void KEXP_STACK(
#ifdef __STDC__
#endif
);

extern int KEXP_PUSH(
#ifdef __STDC__
        int  ival,
	char  cval
#endif
);

extern int KEXP_POP(
#ifdef __STDC__
        int  *ival,
	char  *cval
#endif
);

extern int KEXP_PEEP(
#ifdef __STDC__
        int  *ival,
	char  *cval
#endif
);

extern int worldcnv(
#ifdef __STDC__
#endif
);

extern int ServInit(
#ifdef __STDC__
        char  *chost,
	int  *retosx
#endif
);

extern int ServRead(
#ifdef __STDC__
        char  *comline,
	int  maxcom,
	int  *fcode,
	int  *retosx
#endif
);

extern int ServKRead(
#ifdef __STDC__
	int  typ,
        char  *sendbuf,
	int  *karr,
	int  *retosx
#endif
);

extern int ServKWrite(
#ifdef __STDC__
	int  typ,
        char  *retbuf,
	int  *karr,
	int  *retosx
#endif
);

extern int ServWrite(
#ifdef __STDC__
	int  status,
	int  *retosx
#endif
);

extern int ServClose(
#ifdef __STDC__
	int  *retosx
#endif
);

extern void qinit_here(
#ifdef __STDC__
#endif
);



#ifdef  __cplusplus
}
#endif

#endif 
