/* @(#)catext.h	19.1 (ESO-IPG) 02/25/03 13:49:26 */
/*
 
---------------------------- CATEXT ------------------------------- 

layout of the catalog related structures 
 

K. Banse	900122, 930226

---------------------------------------------------------------------

*/


#define  CAT_MAXNO  5


struct	CAT_STRUCT
	{
	char	NAME[64];
	int	TYPE_SET;
	int	TYPE;
	int	FID;
	int	RECNO;
	char	DESCR[48];
	};

extern struct	CAT_STRUCT	CATAL[CAT_MAXNO];

#define  CATREC_LEN 160
#define  CATIDENT_LEN 40


/*


structure CATAL:
----------------

NAME		name of catalog file
FID		file id for catalog file
TYPE_SET	1, if first line:  =Image catalog, IDENT
		else = 0 (then defaulted to image catalog)
TYPE		the catalog type number:
		1 = image, 2 = spare, 3 = table, 4 = fitfile
RECNO           current active record no.	
DESCR		name of descr. used for Ident field (default = IDENT)
	
*/
