C @(#)pltdec.inc	19.1 (ESO-IPG) 02/25/03 13:49:38
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION: FILE PLTDEC.INC
C.PURPOSE:        To declare the several variables in the plot system
C.AUTHOR:         Rein H. Warmels
C.VERSION:        890301  RHW   Header included
C-----------------------------------------------------------------------------
C *** PLISET contain the parameters set by the user and stored in PLISTAT
      INTEGER       PMODE
      INTEGER       FONT
      INTEGER       OCOUNT
      INTEGER       LTYPE
      INTEGER       STYPE
      INTEGER       LWIDT
      INTEGER       TWIDT
      INTEGER       COLOUR
      INTEGER       BCOLOR
      COMMON        /PLISET/PMODE, FONT, OCOUNT, LTYPE, 
     2                      STYPE, LWIDT, TWIDT, COLOUR, 
     3                      BCOLOR

C *** PLRSET contains the parameters set by the user and stored in PLRSTAT
      REAL          SSIZE
      REAL          TSIZE
      REAL          XSCUSR
      REAL          YSCUSR
      REAL          XOFUSR
      REAL          YOFUSR
      COMMON        /PLRSET/SSIZE,  TSIZE,
     2                      XSCUSR, YSCUSR,
     3                      XOFUSR, YOFUSR
C
C *** PLCSET contains the parameters set by the user and stored in PLCSTAT
      CHARACTER*4   XAXIS           ! x axis setting: auto or manual
      CHARACTER*4   YAXIS           ! y axis setting: auto or manual
      CHARACTER*4   XSCALE          ! x scale: auto or manual
      CHARACTER*4   YSCALE          ! y scale: auto or manual
      CHARACTER*8   XFORM           ! x ticks format
      CHARACTER*8   YFORM           ! y ticks format
      CHARACTER*4   BNMODE          ! bin mode: on or off
      CHARACTER*4   CNTYPE          ! contour mode
      CHARACTER*4   CLMODE          ! colour mode: Xor or Sub
      CHARACTER*4   ERASE           ! erase before plot command
      CHARACTER*4   DBMODE          ! debug mode: on or off
      COMMON        /PLCSET/XAXIS,  YAXIS,
     2                      XSCALE, YSCALE,
     3                      XFORM,  YFORM,
     4                      BNMODE, CNTYPE,
     5                      CLMODE, ERASE,
     6                      DBMODE
C
C *** PLRARR contains various general parameters needed 
      REAL        XP,YP             ! current coursor position in norm. units
      REAL        GX1,GX2,GY1,GY2   ! lim. of act. plot area in PIXEL coord.
      REAL        XPIX,YPIX         ! num of pixels in y and y direction
      REAL        ASPECT            ! aspect ratio of the device (<1=.0)
      REAL        ANGLE             ! Rotation of characters (etc) (countercl.)
      REAL        EXPAND,FRMEXP     ! User and frame labels expansion factors
      REAL        PI,EEE            ! a well known number 
      REAL        XLEXP, XHEXP, YLEXP,YHEXP ! low and high exp. format limits 

      COMMON      /PLPARR/XP,YP,
     2                    GX1,GX2,GY1,GY2,
     3                    XPIX,YPIX,
     4                    ASPECT,ANGLE,EXPAND,FRMEXP,
     6                    PI,EEE,
     7                    XLEXP,XHEXP,YLEXP,YHEXP
