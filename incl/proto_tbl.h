/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2010 European Southern Observatory
.IDENTifer   tbl.h
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for all Table Interfaces,
               resp.: tca, tcc, tcd, tce, tcm, tcr, tct.

.ENVIRONment none

.VERSION     1.0     05-Oct-1993   Creation by R.M. van Hees
.VERSION     1.1     14-Oct-1993   tz* modules included. CG.

 100903		last modif (undo modif before...)
------------------------------------------------------------*/

#ifndef  PROTO_TBL              /* Avoid redefinitions */
#define  PROTO_TBL      0

#ifdef  __cplusplus
extern "C" {
#endif

#include <tblsys.h>		/* Table control definitions */
#include <string.h>
/*
 * module tca.c
 */

extern void *TCTTST(
#ifdef __STDC__
	int tid
#endif
);

extern int TCAMAP(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	char **addr
#endif
);

extern int TCAUNM(  
#ifdef __STDC__
       int tid,
       char *addr
#endif
);

extern int TCAEDC(
#ifdef __STDC__
	int tid, 
	char *abin, 
	int col, 
	int index, 
	int items, 
	char *buffer
#endif
);

extern int TCATRC(
#ifdef __STDC__
	int tid, 
	char *abin, 
	int col, 
	int index, 
	int items, 
	char *buffer
#endif
);

extern int TCARDC(
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	char *values  
#endif
);

extern int TCARDI(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	int *values  
#endif
);

extern int TCARDR(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	float *values  
#endif
);

extern int TCARDD(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	double *values  
#endif
);

extern int TCARDS(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	char *values  
#endif
);

extern int TCADEL(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items
#endif
);

extern int xTCAWRC(  
#ifdef __STDC__
	TABLE *tp,
	int tid, 
	int row, 
	int col, 
	int items, 
	char *values  
#endif
);

extern int TCAWRC(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	char *values  
#endif
);

extern int TCAWRI(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	int *values  
#endif
);

extern int xTCAWRI(  
#ifdef __STDC__
	TABLE *tp,
	int tid, 
	int row, 
	int col, 
	int items, 
	int *values  
#endif
);

extern int xTCAWRR(  
#ifdef __STDC__
	TABLE *tp,
	int tid, 
	int row, 
	int col, 
	int items, 
	float *values  
#endif
);

extern int TCAWRR(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	float *values  
#endif
);

extern int xTCAWRD(  
#ifdef __STDC__
	TABLE *tp,
	int tid, 
	int row, 
	int col, 
	int items, 
	double *values  
#endif
);

extern int TCAWRD(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	double *values  
#endif
);

extern int TCAWRS(
#ifdef __STDC__
        int tid,
        int row,
        int col,
        int index,
        char *value
#endif
);

extern int TCASRC(
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	char *values, 
	int *next
#endif
);

extern int TCASRD(
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	double *values, 
	int *next
#endif
);

extern int TCASRI(
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	int *values, 
	int *next
#endif
);

extern int TCASRR(
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int index, 
	int items, 
	float *values, 
	int *next
#endif
);

extern int TCAPUT(
#ifdef __STDC__
	int tid, 
	int col, 
	int alen 
#endif
);

/*
 * module tcc.c
 */
extern int TCCDEL(  
#ifdef __STDC__
	int tid, 
	int col, 
	int *ncol  
#endif
);

extern int TCCINI(  
#ifdef __STDC__
	int tid, 
	int dtype, 
	int alen, 
	char *form, 
	char *unit,
	char *label, 
	int *col  
#endif
);

extern int TCCMAP(  
#ifdef __STDC__
	int tid, 
	int col, 
	char **address  
#endif
);

extern int TCCSEL(  
#ifdef __STDC__
	int tid, 
	char *text, 
	int mxcol, 
	int *cols, 
	int *flags, 
	int *outcol  
#endif
);

extern int TCCSER(  
#ifdef __STDC__
	int tid, 
	char *text, 
	int *col  
#endif
);

extern int TCCSRT(  
#ifdef __STDC__
	int tid, 
	int nc, 
	int *col, 
	int *soltfl  
#endif
);

extern int TCCUNM(  
#ifdef __STDC__
	int tid, 
	char *address  
#endif
);

/*
 * module tcd.c
 */
extern int GetLabel(
#ifdef __STDC__
	TABLE *tp, 
	int col, 
	int pos, 
	int len, 
	char *buf
#endif
);

extern int PutLabel(
#ifdef __STDC__
	TABLE *tp, 
	int col, 
	char *buf, 
	int pos, 
	int len
#endif
);

extern int  TCBGET(  
#ifdef __STDC__
	int tid, 
	int col, 
	int *dtype, 
	int *items, 
	int *bytes  
#endif
);

extern int  TCDGET(  
#ifdef __STDC__
	int tid, 
	int *store  
#endif
);

extern int  TCFGET(  
#ifdef __STDC__
	int tid, 
	int col, 
	char *fmt, 
	int *len, 
	int *dtype  
#endif
);

extern int  TCFPUT(  
#ifdef __STDC__
	int tid, 
	int col, 
	char *fmt  
#endif
);

extern int  TCIGET(
#ifdef __STDC__
	int tid, 
	int *ncol, 
	int *nrow, 
	int *nsort, 
	int *alcol, 
	int *allrow  
#endif
);

extern int  TCIPUT(
#ifdef __STDC__
	int tid, 
	int column, 
	int row
#endif
);

extern int  TCKGET(  
#ifdef __STDC__
	int tid, 
	int *col  
#endif
);

extern int  TCKPUT(  
#ifdef __STDC__
	int tid, 
	int col  
#endif
);

extern int  TCLGET(  
#ifdef __STDC__
	int tid, 
	int col, 
	char *label  
#endif
);

extern int  TCLPUT(  
#ifdef __STDC__
	int tid, 
	int col, 
	char *label  
#endif
);

extern int  TCLSER(  
#ifdef __STDC__
	int tid, 
	char *label, 
	int *col
#endif
);

extern int  TCOERR(
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int status, 
	char *text
#endif
);

extern int  TCOGET(
#ifdef __STDC__
	char *op_name,
	int  *value
#endif
);

extern int  TCOSET(
#ifdef __STDC__
	char *op_name, 
	int value
#endif
);

extern int  TCSDSC(  
#ifdef __STDC__
	int tid
#endif
);

extern int  TCSGET(  
#ifdef __STDC__
	int tid, 
	int row, 
	int *isel  
#endif
);

extern int  TCSPUT(  
#ifdef __STDC__
	int tid, 
	int row, 
	int *value  
#endif
);

extern int  TCSCNT(
#ifdef __STDC__
	int tid,
	int *count
#endif
);

extern int  TCSINI(
#ifdef __STDC__
	int tid
#endif
);

extern int  TCSINF(  
#ifdef __STDC__
	int tid, 
	char *info
#endif
);

extern int  TCSSET(  
#ifdef __STDC__
	int tid, 
	char *info  
#endif
);

extern int  TCUGET(  
#ifdef __STDC__
	int tid, 
	int col, 
	char *unit  
#endif
);

extern int  TCUPUT(  
#ifdef __STDC__
	int tid, 
	int col, 
	char *unit  
#endif
);

extern int  TCVERS(
#ifdef __STDC__
#endif
);

/*
 * module tce.c
 */
extern int  TCEDEL(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col
#endif
);

extern int  TCEMAP(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	char **addr, 
	int *inull  
#endif
);

extern int  TCEEDC(  
#ifdef __STDC__
	int tid, 
	char *abin, 
	int col, 
	char *buff
#endif
);

extern int  TCERDC(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	char *value
	, int *inull  
#endif
);

extern int  TCERDI(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	int *value, 
	int *inull  
#endif
);

extern int  TCERDR(  
#ifdef __STDC__
	int tid, 
	int row, 
	int col, 
	float *value, 
	int *inull  
#endif
);

extern int  TCERDD(  
#ifdef __STDC__
	int tid,
	int row, 
	int col, 
	double *value, 
	int *inull  
#endif
);

extern int  TCETRC(
#ifdef __STDC__
	int tid,
	char *abin, 
	int col,
	char *buff
#endif
);

extern int  TCEWRC(
#ifdef __STDC__
	int tid,
	int row, 
	int col, 
	char *value
#endif
);

extern int  TCEWRI(  
#ifdef __STDC__
	int tid,
	int row, 
	int col, 
	int *value  
#endif
);

extern int  TCEWRR(  
#ifdef __STDC__
	int tid,
	int row, 
	int col, 
	float *value  
#endif
);

extern int  TCEWRD(  
#ifdef __STDC__
	int tid,
	int row, 
	int col, 
	double *value  
#endif
);

extern int  TCESRC(
#ifdef __STDC__
	int tid,
	int column, 
	char *val, 
	int start, 
	int len, 
	int first,
	int *next
#endif
);

extern int  TCESRI(
#ifdef __STDC__
	int tid,
	int col, 
	int value, 
	int deltv, 
	int first,
	int *next
#endif
);

extern int  TCESRR(
#ifdef __STDC__
	int tid,
	int col, 
	float value, 
	float deltv, 
	int first,
	int *next
#endif
);

extern int  TCESRD(
#ifdef __STDC__
	int tid,
	int col, 
	double value, 
	double deltv, 
	int first,
	int *next
#endif
);

extern int  TCEUNM(
#ifdef __STDC__
	int tid,
	char *addr
#endif
);

/*
 * module tcm.c
 */
extern int TCMALL(  
#ifdef __STDC__
	char *type,
	int ncol, 
	int nrow, 
	int *pointer  
#endif
);

extern int TCMFRE(  
#ifdef __STDC__
	int *pointer  
#endif
);

extern int TCMGET(  
#ifdef __STDC__
	int tid, 
	int *pointer, 
	int *nac, 
	int *nar  
#endif
);

extern int TCMPUT(  
#ifdef __STDC__
	int tid, 
	int *pointer, 
	int nac, 
	int nar  
#endif
);

extern int TCMCON(  
#ifdef __STDC__
	float *tbsel, 
	double *tdtrue, 
	double *tbfalse  
#endif
);

extern int TCMNUL(  
#ifdef __STDC__
	int *inull, 
	float *rnull, 
	double *dnull  
#endif
);

/*
 * module tcr.c
 */
extern int TCRDEL(  
#ifdef __STDC__
	int tid, 
	int row  
#endif
);

extern int TCRRDC(  
#ifdef __STDC__
	int tid, 
	int row, 
	int nc, 
	int *col, 
	char *value, 
	int *null  
#endif
);

extern int TCRRDI(  
#ifdef __STDC__
	int tid, 
	int row, 
	int nc, 
	int *col, 
	int *value, 
	int *null  
#endif
);

extern int TCRRDR(  
#ifdef __STDC__
	int tid, 
	int row, 
	int nc, 
	int *col, 
	float *value, 
	int *null  
#endif
);

extern int TCRRDD(  
#ifdef __STDC__
	int tid, 
	int row, 
	int nc, 
	int *col, 
	double *value, 
	int *null  
#endif
);

extern int TCRSEL(  
#ifdef __STDC__
	int tid, 
	char *text, 
	int max_ranges, 
	int *low_bounds, 
	int *upp_bounds, 
	int *found_ranges  
#endif
);

extern int TCRWRC(  
#ifdef __STDC__
	int tid, 
	int row, 
	int nc, 
	int *col, 
	char *value  
#endif
);

extern int TCRWRI(  
#ifdef __STDC__
	int tid, 
	int row, 
	int nc, 
	int *col, 
	int *value  
#endif
);

extern int TCRWRR(  
#ifdef __STDC__
	int tid, 
	int row, 
	int nc, 
	int *col, 
	float *value  
#endif
);

extern int TCRWRD(  
#ifdef __STDC__
	int tid, 
	int row, 
	int nc, 
	int *col, 
	double *value  
#endif
);

/*
 * module tct.c
*/

extern int  TCTCLO(  
#ifdef __STDC__
	int tid  
#endif
);

extern int  TCTCRV(  
#ifdef __STDC__
	char *name, 
	char *ref_name, 
	int mode
#endif
);

extern int  TCTUNM(  
#ifdef __STDC__
	int tid  
#endif
);

extern int  TCTUNMF(  
#ifdef __STDC__
	int tid,
	int flag  
#endif
);

extern int  TCTID(  
#ifdef __STDC__
	char *name
#endif
);

extern int  TCTINI(  
#ifdef __STDC__
	char *name, 
	int storage, 
	int mode, 
	int allcol, 
	int allrow,
	int *tid  
#endif
);

extern int  TCTMAP(  
#ifdef __STDC__
	int tid,
	char **addr
#endif
);

extern int  TCTOPN(  
#ifdef __STDC__
	char *name, 
	int mode, 
	int *tid  
#endif
);

extern int  TCTFIX(
#ifdef __STDC__
        int tid
#endif
);

extern int TCTVIS(
#ifdef __STDC__
	int tid,
	char *name
#endif
);
 
/*
 * module tz0.c
 */
extern int  TBL_isNULL(  
#ifdef __STDC__
	int    dtype,
	int    *x
#endif
);

extern int  TBL_toNULL(  
#ifdef __STDC__
	int    dtype,
	char   *x
#endif
);

extern int  TBL_cv2(  
#ifdef __STDC__
	char    *x1,
	int     dtyp1,
	char    *x2,
	int     dtyp2
#endif
);

extern int  TBL_toMAX(  
#ifdef __STDC__
	int    dtype,
	char   *x
#endif
);

/*
 * module tz1.c
 */
extern int  TBL_offset(  
#ifdef __STDC__
	TABLE   *tp,
	int     row,
	int     col
#endif
);

extern char *TBL_Dlab(  
#ifdef __STDC__
	int    col
#endif
);

extern char  *TBL_Cfmt(  
#ifdef __STDC__
	char    *form,
	int     ttype
#endif
);

extern int  TBL_FMTCHK(  
#ifdef __STDC__
	char    *form,
	int     ttype,
	char    *oform
#endif
);

extern int  TBL_UNICHK(  
#ifdef __STDC__
	char    *unit,
	char    *ounit
#endif
);

extern int  TBL_LABCHK(  
#ifdef __STDC__
	char    *label,
	char    *outlab
#endif
);

extern int  TBL_TYPCHK(  
#ifdef __STDC__
	int     dtype,
        int     alen,
        int     *icode
#endif
);

/* 
 * module tz2.c
 */
extern int  TBL_s0(  
#ifdef __STDC__
	char    *s,
	int     len
#endif
);

extern int  TBL_s1(  
#ifdef __STDC__
	char    *s,
	int     len
#endif
);

extern int  TBL_sdiff(  
#ifdef __STDC__
	char    *s1,
	char    *s2
#endif
);

extern int  TBL_labcase(  
#ifdef __STDC__
	int     value
#endif
);

extern char *TBL_ssave(  
#ifdef __STDC__
	char    *s
#endif
);

/*
 * module tz3.c
 */
extern int TBL_cv(  
#ifdef __STDC__
	char    *buf,
	char    *form,
	int     dtype,
	char    *addr
#endif
);

extern int TBL_ed(  
#ifdef __STDC__
	char    *buf,
	char    *form,
	int     dtype,
	char    *addr
#endif
);

/*
 * module tz4.c
 */
extern void TBL_ERROR(  
#ifdef __STDC__
	char    *name,
	int    tid,
	int    status
#endif
);
 
extern char *TBL_eget(  
#ifdef __STDC__
	void
#endif
);
extern int TBL_eput(  
#ifdef __STDC__
	int     status,
	char    *message
#endif
);

extern int TBL_eclear(  
#ifdef __STDC__
	void
#endif
);

extern int TBL_eset(  
#ifdef __STDC__
	int opt
#endif
);

extern int TBL_enter(  
#ifdef __STDC__
	char    *name
#endif
);

extern int TBL_exit(  
#ifdef __STDC__
	int     status
#endif
);

extern int TBL_errf(  
#ifdef __STDC__
	int     status,
	char *fmt,
	...
#endif
);

extern int TBL_errs(  
#ifdef __STDC__
	int     tid,
	int     status,
	int     value
#endif
);

/*
 * module tz5.c
 */
extern int TBL_new(  
#ifdef __STDC__
	int     fid
#endif
);

extern int TBL_kill(  
#ifdef __STDC__
	int     fid
#endif
);

extern TABLE *TBL_ptr(  
#ifdef __STDC__
	int     fid
#endif
);

extern int TBL_tid(  
#ifdef __STDC__
	char *name
#endif
);

/* 
 * module tz6.c
 */


extern int TBL_WR(  
#ifdef __STDC__
	TABLE *tp
#endif
);

extern int TBL_RDst(  
#ifdef __STDC__
	void
#endif
);

extern char *TBL_RD(  
#ifdef __STDC__
	TABLE *tp,
	int   offset,
	int   len
#endif
);

extern char *TBL_RDF(  
#ifdef __STDC__
	TABLE *tp,
	int   offset,
	int   len,
	int   flag
#endif
);

extern int TBL_UMAP(  
#ifdef __STDC__
	TABLE *tp,
	char  *addr
#endif
);

/* 
 * module tz8.c
 */
extern int TBL_ALLOCOL(  
#ifdef __STDC__
	int   tid,
	int   no
#endif
);

extern int TBL_ALLOROW(  
#ifdef __STDC__
	int   tid,
	int   nr
#endif
);

extern int TBL_ADDROW(  
#ifdef __STDC__
	int   tid,
	int   irow,
	int   nr
#endif
);

extern int TBL_DELROW(  
#ifdef __STDC__
	int   tid,
	int   irow,
	int   no
#endif
);

/*
 * module tz9.c
 */
extern int TBL_SSI1(  
#ifdef __STDC__
	char    *data,
	int     value,
	int     error,
	int     count,
	int     incr
#endif
);

extern int TBL_SSI2(  
#ifdef __STDC__
	short   *data,
	int     value,
	int     error,
	int     count,
	int     incr
#endif
);

extern int TBL_SSI4(  
#ifdef __STDC__
	int     *data,
	int     value,
	int     error,
	int     count,
	int     incr
#endif
);

extern int TBL_SSR(  
#ifdef __STDC__
	float   *data,
	double  value,
	double  error,
	int     count,
	int     incr
#endif
);

extern int TBL_SSD(  
#ifdef __STDC__
	double  *data,
	double  value,
	double  error,
	int     count,
	int     incr
#endif
);

extern int TBL_SSC(  
#ifdef __STDC__
	char    *data,
	char    *value,
	int     start,
	int     ncomp,
	int     count,
	int     incr
#endif
);

extern int TBL_BSI1(  
#ifdef __STDC__
	char    *data,
	int     value,
	int     error,
	int     count,
	int     tonext
#endif
);

extern int TBL_BSI2(  
#ifdef __STDC__
	short   *data,
	int     value,
	int     error,
	int     count,
	int     tonext
#endif
);

extern int TBL_BSI4(  
#ifdef __STDC__
	int     *data,
	int     value,
	int     error,
	int     count,
	int     tonext
#endif
);

extern int TBL_BSR(  
#ifdef __STDC__
	float   *data,
	double  value,
	double  error,
	int     count,
	int     tonext
#endif
);

extern int TBL_BSD(  
#ifdef __STDC__
	double  *data,
	double  value,
	double  error,
	int     count,
	int     tonext
#endif
);

extern int TBL_BSC(  
#ifdef __STDC__
	char    *data,
	char    *value,
	int     start,
	int     ncomp,
	int     count,
	int     tonext
#endif
);

extern int TBL_BSCTA(  
#ifdef __STDC__
	char    *data,
	char    *value,
	int     start,
	int     ncomp,
	int     count,
	int     nb
#endif
);

extern int TBL_BSCTD(  
#ifdef __STDC__
	char    *data,
	char    *value,
	int     start,
	int     ncomp,
	int     count,
	int     nb
#endif
);

#ifdef  __cplusplus
}
#endif

#endif
