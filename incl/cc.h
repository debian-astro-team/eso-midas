/* @(#)cc.h	19.1 (ESO-IPG) 02/25/03 13:49:26 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE 		Header
.NAME 		cc.h
.LANGUAGE 	C
.AUTHOR		Francois Ochsenbein [ESO]
.CATEGORY	C-like language compile
.COMMENTS 	
.ENVIRONMENT	
.VERSION 1.0	05-Dec-1988: Creation
.VERSION 1.1	15-Jun-1989: Added cc_Function
.VERSION 2.0	20-Nov-1989: Redefinitions of cc_Bind.
.VERSION 2.1	20-Mar-1990: Redefinitions of cc_BindString
------------------------------------------------------------*/

#ifndef CC_DEF

#define CC_DEF		0

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
int 	cc_compile	(char *source_code);
int 	cc_exec		(int pgmno);
int 	cc_free		(int pgmno);
int 	cc_ext		(char *dcl_string, int *addr);
int 	cc_glb		(int type, char *dcl_string, int *addr);
int 	cc_fct		(char *dcl_string, int np, int (*addr)() ); 
int 	cc_dcl		(char *dcl_string, int np, int (*addr)() ); 
#endif

/*===========================================================================
 *             Related functions and macros
 *===========================================================================*/

#define cc_Compile(s)		cc_compile(s)
#define cc_Free(thecode)	cc_free(thecode)
#define cc_BindVariable(dcl,a)	cc_ext(dcl,(int *)(&a))
#define cc_BindChar(dcl,a)	cc_glb(0x01, dcl,(int *)(&a))
#define cc_BindShort(dcl,a)	cc_glb(0x02, dcl,(int *)(&a))
#define cc_BindInteger(dcl,a)	cc_glb(0x00, dcl,(int *)(&a))
#define cc_BindDouble(dcl,a)	cc_glb(0x03, dcl,(int *)(&a))
#define cc_BindString(dcl,a)	cc_glb(0x11, dcl,(int *)(a))
#define cc_BindAddress(dcl,a)	cc_ext(dcl,(int *)a)
#define cc_Execute(thecode)	cc_exec(thecode)
#define cc_BindFunction(t,np,f)	cc_fct(t,np,(int(*)())f)
#define cc_Function(t,np,f)	cc_dcl(t,np,(int(*)())f)

#endif
