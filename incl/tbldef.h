/* @(#)tbldef.h	19.1 (ESO-IPG) 02/25/03 13:49:42 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE        C
.IDENTIFICATION  header file TBLDEF.H
.AUTHOR         J.D.Ponz
.KEYWORDS       Table control buffers
.ENVIRONMENT    Independent
.VERSION  3.0  	01 Feb 1987 : Conversion into C
.VERSION  3.1  	01 Apr 1988 : Include new parameters
.VERSION  3.2 	21 Jun 1988 : C. Guirao, Avoiding double definition
.VERSION  3.3 	13 Jan 1989 : include physical table format
.VERSION  3.4 	13-Jun-1990 : Include F_MAP_FORCE + F_EIO_FORCE
.PURPOSE
                Define TBL parameters and structures
------------------------------------------------------------------------*/

#ifndef TBL_NAMLEN   

#define TBL_MAXSIZ   ((1<<24)-1)	/* Maximum size of array   */

#define TBL_NAMLEN   60 /* maximum filename length */
#define TBL_LABLEN   16 /* length of label field */
#define TBL_UNILEN   16 /* length of unit field */
#define TBL_DESLEN    8 /* length of descriptor names */
#define TBL_DESLAB   41 /* length of the table descriptor TBLABLxxx */
#define TBL_FORLEN    6 /* length of the format descriptor */
#define TBL_MODLEN    6 /* length of MODE string */
#define TBL_TYPLEN    5 /* length of TYPE string */
#define TBL_ROWLEN 1024 /* max. length of row of characters */
#define TBL_ELMLEN  256 /* max. length of table element */
#define TBL_MAXCOL  256 /* maximum number of allocated columns */
#define TBL_DALCOL   15 /* default column allocation */
#define TBL_DALROW 4096 /* default row allocation */
#define TBL_TRUE      1 /* selection on ??        */
#define TBL_FALSE     0 /* selection off ??       */
#define TBL_EMPTFM    0 /* not defined */
#if 0	/* Old Definitions */
#define TBL_SINGLE    -4 /* code for entry in single precision */
#define TBL_DOUBLE    -8 /* code for entry in double precision */
#define TBL_INT1      -11 /* code for int*1 */
#define TBL_INT2      -12 /* code for int*2 */
#define TBL_INT4      -14 /* code for int*4 */
#define TBL_I1NULVAL  (1 << 7) /* null value for int*1 */
#define TBL_I2NULVAL  (1 << 15) /* null value for int*2 */
#endif


#ifndef F_MODES			/* Also defined in midas_def.h CG */
#define F_MODES
#define F_TRANS      0		/* table transposed format */
#define F_RECORD     1		/* table record format */

#define F_I_MODE     0		/* map file for input only    */
#define F_O_MODE     1		/* map file for output        */
#define F_IO_MODE    2		/* map file for updating      */
#define F_U_MODE     2		/* map file for updating      */
#define F_X_MODE     9		/* create/map virtual memory  */
#define F_D_MODE     2		/* map for descriptors only */
#endif 

#ifndef F_B_VIEW	  /* Table as a View	*/
#define F_B_VIEW   0x100  /* Bit view		*/
#endif

#ifndef F_MAP_FORCE
#define F_MAP_FORCE 0x10  /* Force the mapping (modifier) 	*/
#define F_EIO_FORCE 0x20  /* Force the elementary i/o (modifier)*/
#define F_ALL_FORCE 0x40  /* Force the allocated values 	*/
#endif

#endif

