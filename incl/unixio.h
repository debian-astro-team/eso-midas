/* @(#)unixio.h	19.1 (ESO-IPG) 02/25/03 13:49:45 */
/*++++++++++++++
.IDENTIFICATION ftoc_sun.h
.LANGUAGE       C
.AUTHOR         C.Guirao.
.ENVIRONMENT    VMS 
.KEYWORDS       
.VERSION  1.0   20-Aug-1992
.COMMENTS       This file is meant here to avoid redefinitions for the
                GNU C compiler (VAX/VMS). 
                We cannot use the original one: SYS$SYSROOT:[SYSLIB]unixio.h
                because it contains the statements:
			 #pragma nostandard and
                 	 #pragma standard 
                that are ignored (and therefore causing errors) when the
                file is included with the GNU C compiler.

		This include file is used by all AGL modules.
---------------*/

/*      UNIXIO - V3.0 - Prototypes for UNIX emulation functions */
/*
/*#pragma nostandard
/*#include stdio
/*#include stat
/*#pragma standard
/*
/*int     access  (char *file_spec, int mode);
/*      UNIXIO - V3.0 - Prototypes for UNIX emulation functions */
/*
/*int     access  (char *file_spec, int mode);
/*int     close   (int file_desc);
/*int     creat   (char *file_spec, unsigned int mode, ...);
/*int     delete  (const char *file_spec);
/*int     dup     (int file_desc);
/*int     dup2    (int file_desc1, int file_desc2);
/*int     fstat   (int file_desc, stat_t *buffer);
/*char   *getname (int file_desc, char *buffer, ...);
/*int     isapipe (int file_desc);
/*int     isatty  (int file_desc);
/*int     lseek   (int file_desc, int offset, int direction);
/*char   *mktemp  (char *template);
/*int     read    (int file_desc, void *buffer, int nbytes);
/*int     rename  (const char *old_file_spec, const char *new_file_spec);
/*int     open    (char *file_spec, int flags, unsigned int mode, ...);
/*int     stat    (char *file_spec, stat_t *buffer);
/*char   *ttyname (void);
/*int     write   (int file_desc, void *buffer, int nbytes);
*/
