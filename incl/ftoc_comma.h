/* @(#)ftoc_comma.h	19.1 (ESO-IPG) 02/25/03 13:49:32 */
/*++++++++++++++
.IDENTIFICATION ftoc_comma.h
.LANGUAGE       C
.AUTHOR         Francois Ochsenbein
.ENVIRONMENT    
.KEYWORDS       
.VERSION  1.0   12-Dec-1990
.COMMENTS       Here there is the definition of the COMMON used
		for address passing between FORTRAN routines (replaces %LOC)
		Almost all systems use this definition (except Apollo)
---------------*/

#ifndef VMR_DEF
#define VMR_DEF		0	/* To avoid redefinitions */

		/* structure for COMMON in main Fortran programs  */
extern struct { int addr; } vmr ;

		/* translate address into FORTRAN index in VMR common */
#define COMMON_INDEX(a)		(int *)a - (int *)&((&(vmr.addr))[-1])
#endif
