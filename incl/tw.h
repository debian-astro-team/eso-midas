/* @(#)tw.h	19.1 (ESO-IPG) 02/25/03 13:49:44 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		tw.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Windowing 
.COMMENTS	If TW_import is set to 0, only structures and functions
		are defined; this is used only in the code of
		tw routines.

.ENVIRONMENT	TermWindows

.VERSION	1.0	Created 14-Aug-1986 
.VERSION 	2.0	03-Dec-1986:	New terminal-independant graphic 
			characters with output buffering version.
.VERSION 	2.1	01-Jul-1987:	Version '2'
.VERSION 	3.0	02-May-1988:	Version '3', with
				TermDisplay extensions
.VERSION 	3.1	08-Dec-1988:	Added GetCcode.
.VERSION 	3.2	21-Feb-1989:	Added GetControl
.VERSION 	3.3	01-Mar-1989:	Ccode modified
.VERSION 	3.31	31-Mar-1989:	Added SaveWindowText, WriteAchars
.VERSION 	3.32	17-May-1989:	Added DestroySubwindow
.VERSION 	3.33	07-Jun-1989:	Defined TW_CHECK_FCT . 
					Added DisplayError.
.VERSION 	3.4	29-Jun-1989:	Added Rules for Windows
.VERSION 	3.41	22-Aug-1989:	Correct declaration of ostint
.VERSION 	3.42	16-Feb-1990: 	Added UngetChar
.VERSION 	3.5	09-Mar-1990: 	Added GetsPassword  GetsAnswer
					PutsString PutsText LowerWindow
.VERSION 	3.6	06-Nov-1990: 	Added ResetTerm 
.VERSION 	3.7	03-Dec-1990: 	Added Added TitleWindow + Doc routines

--------------------------*/

#ifndef TW_DEF

#define TW_DEF

#ifndef TW_import
#define TW_import	1	/* Complete definitions	*/
#endif

/*************************************************************************
 * 		Definition of Ingredients (at your choice...)
 *			(Here are defaults)
 *************************************************************************/


#ifndef TW_LEVEL
#define TW_LEVEL	1	/* There are 3 levels for Terminal Routines:
	 - 0 : Minimal Implementation, better suited for microcomputers;
	 - 1 : Required capabilities for TermWindows (default)
	 - 2 : Complete Terminal Capabilities
				 */
#endif

#ifndef TWFAC_Stacking
#define TWFAC_Stacking	1	/* Stacking Commands is allowed		*/
#endif

#ifndef TWFAC_Log
#define TWFAC_Log	0	/* No logging				*/
#endif

#ifndef TWFAC_TeX
#define TWFAC_TeX	1	/* TermDisplay allowed			*/
#endif

#ifndef TERMCAPFILE
#define TERMCAPFILE	oshenv("TERMCAPFILE", (char *)0)	
#endif

/* The Default Control Characters are defined in twset.h
   as VMS or UNIX, depending on the preferred system. 
   But the UNIX means that Delete key = Interrupt, which isn't quite
   good.... There the default is redefined here as VMS.
*/
#ifndef TW_cc_default 
#define TW_cc_default	 TW_cc_VMS
#endif 

/**************************************************************************/

#include <twdef.h>
#include <string.h>

#ifndef _TEMPLATES_
#include <compiler.h>
#endif


/*=======================================================================
 * 		Window Structure
 *=======================================================================*/

struct WINDOW_struct {		/*=====Only a subset here===============*/
	char	version;
	unsigned char flags;	
#define Echo	 0x01		/*	Indicates echo input text	*/
#define Clear	 0x02		/*	Window is clear			*/
#define Present	 0x04		/*	Window not Removed		*/
#define Imode	 0x08		/*	Insert Mode facility		*/
#define Stacking 0x10		/* 	Stacking Commands is Active	*/
	char 	wpos;		/* Flags for the window position	*/
	unsigned char hw;	/* Hardware capabilities		*/
	char	id[8];		/* Window Name, as given in Open	*/
	short int dim[2];	/* Window size				*/
	short int home[2];	/* Position of the upper left corner	*/
	int 	homa;		/* Home position in bytes		*/
	int 	pos;		/* Cursor position from upper left	*/
	int 	marker[2];	/* Range definition			*/
	ACHAR	attr_init;	/* Window default `blank' char		*/
	ACHAR	attr;		/* Current `blank' character		*/
	struct 	WINDOW_struct
		*help;		/* Associated Help Window		*/
  };
typedef struct WINDOW_struct WINDOW;	/* General Window Structure	*/

#define NULL_WINDOW	(WINDOW *)0

#define TheScreen	NULL_WINDOW


/*=======================================================================
 * 		Cursor + Attribute 
 *=======================================================================*/

typedef struct {		/* Saving Cursor + Attribute */
	short int pos[2];
	unsigned char attr;
  } TVSAVE;

typedef struct {		/* Saving Cursor + Attribute */
	int 	pos;
	unsigned char attr;
  } TWSAVE;

/*=======================================================================
 * 		TWFIELD structure
 *=======================================================================*/

typedef struct {		/* What's in a field on a window */
	char *name;		/* Field Name			*/
	char *type;		/* Field Type			*/
	char *pic;		/* Field Picture or Template	*/
	short int home[2];	/* Starting point of field 	*/
	short int dim[2];	/* Dimension of Field		*/
	} TWFIELD;

/*=======================================================================
 * 		Functions description
 *=======================================================================*/

#if _TEMPLATES_
/* typedef int 		(*TW_CHECK_FCT)(WINDOW* w, char * str, int str_size);*/
typedef int 		(*TW_CHECK_FCT)();

int 	 tv_open	(char *device, char *termcapfile, int env);
int 	 tv_close	(void);
int 	 tv_ac		(ACHAR *pa, int length, char *bytes);
int 	 tv_attr	(int attribute);
int 	 tv_bell	(void);
int 	 tv_box		(int top_line, int top_col, int lines, int cols);
int	 tv_cc		(int number);
int	 tv_cl		(void);
int	 tv_clear	(int direction);
int	 tv_cur		(TVSAVE *saved);
int	 tv_cus		(TVSAVE *saved);
int 	 tv_dim		(short int *sizes);
int	 tv_dc		(int number);
int	 tv_dl		(int number);
int	 tv_fw		(int (*func)(int));	/* WhileInput function 	*/
int	 tv_getc	(char *ch);
int	 tv_get1	(char *buffer, int buf_size, char *ret_list);
int	 tv_getp	(char *buffer, int buf_size);
int	 tv_getb	(char *buffer, int buf_size);
char 	*tv_gterm	(void);		/* Terminal name	*/
int	 tv_goto	(int line, int col);
int	 tv_agoto	(int position);
int	 tv_help	(void);
int	 tv_home	(void);
int	 tv_ic		(int number);
int	 tv_il		(int number);
int	 tv_imode	(int true_false);
int	 tv_inc		(void);		/* Incoming bytes 	*/
int	 tv_iwrite	(char *str, int bytes);
int	 tv_mods	(char *buffer, int buf_size, int template_len);
int	 tv_mvc		(int direction, int times);
int	 tv_nl		(void);
int 	 tv_pos		(void);
int 	 tv_puts	(char *text);
int 	 tv_range	(short *cursor, short *dim);
int 	 tv_rb		(short *home_pos, short *dim, short *limits);
int 	 tv_rule	(int direction, int length);
int 	 tv_setcc	(int cc, int action);
int 	 tv_setsc	(int action);
int 	 tv_getcc	(int action);
int 	 tv_gto		(int seconds);
int	 tv_scroll	(int direction, int lines);
int	 tv_sr		(int top_line, int bottom_line);	/* SetScroll */
int	 tv_stop	(int true_false);
int	 tv_stopin	(int set_type, char *set_list);
int	 tv_supply	(char *text, int len, int queue);
int	 tv_wa		(ACHAR *pa, int length);
int	 tv_wait	(int milliseconds);
int	 tv_wg		(char *graphics, int length);
int	 tv_where	(short *position);
int	 tv_write	(char *str, int bytes, int cc_option);
int	 tv_wsend	(char *str, int bytes);

int 	 tw_init	(char *device, char *termcapfile, int env);
int 	 tw_end		(void);
WINDOW	*tw_open	(WINDOW *parent, char *name, int home_line, int home_col,
			int lines, int cols, int attr, int options, int stacked);
WINDOW	*tw_hop		(char *name, int home_line, int home_col,
				char *cc, char **texts);
int 	 tw_copy	(WINDOW *w, int dest_pos, ACHAR *text, int text_len);
int 	 tw_close	(WINDOW *w, int close_or_destroy);
int 	 tw_attr	(WINDOW *w, int attribute);
int 	 tw_awhere	(WINDOW *w);
int 	 tw_box		(WINDOW *w, int top_line, int top_col, 
				int lines, int cols);
int	 tw_cc		(WINDOW *w, int number);
int	 tw_cl		(WINDOW *w);
int	 tw_clear	(WINDOW *w, int direction);
int	 tw_cline	(WINDOW *w, char *text, int text_length);
int	 tw_cur		(TWSAVE *saved);
int	 tw_cus		(TWSAVE *saved);
int	 tw_dc		(WINDOW *w, int number);
int	 tw_dl		(WINDOW *w, int number);
int	 tw_getc	(WINDOW *w, char *ch);
int	 tw_gc1		(WINDOW *w, char *ch, char *ret_list);
int	 tw_gc2		(WINDOW *w);
int	 tw_modf	(WINDOW *w, char *buf, int buf_size, int linput,
				int (*check_fct)(WINDOW*, char *, int));
int	 tw_ms2		(WINDOW *w, char *buf, int buf_size, int linput,
				int (*check_fct)(WINDOW*, char *, int));
int	 tw_geth	(WINDOW *w, short *home_pos);
int	 tw_get1	(WINDOW *w, char *buffer, int buf_size, char *ret_list);
WINDOW	*tw_getlw	(WINDOW *w, int link_direction);
WINDOW	*tw_gettw	(WINDOW *w);
WINDOW	*tw_getw	(char *name);
int	 tw_goto	(WINDOW *w, int line, int col);
int	 tw_agoto	(WINDOW *w, int position);
int	 tw_helps	(WINDOW *w, int switch_or_remove);
WINDOW	*tw_hset	(WINDOW *w, WINDOW *w_help);
int	 tw_home	(WINDOW *w);
int	 tw_ic		(WINDOW *w, int number);
int	 tw_il		(WINDOW *w, int number);
int	 tw_mark	(WINDOW *w, int line, int col, int length);
int 	 tw_mattr	(WINDOW *w, int length, int new_attribute);
int	 tw_mods	(WINDOW *w, char *buffer, int buf_size, int template_len);
int 	 tw_mrule	(WINDOW *w, int len);
int	 tw_mvc		(WINDOW *w, int direction, int times);
int	 tw_mw		(WINDOW *w, int now_home_line, int new_home_col);
int	 tw_nl		(WINDOW *w);
int 	 tw_occluded	(WINDOW *w, int window_or_range);
int 	 tw_puts	(WINDOW *w, char *text);
int 	 tw_r		(WINDOW *w, int refresh_option, WINDOW *prec);
int 	 tw_rule	(WINDOW *w, int direction, int length);
int	 tw_scroll	(WINDOW *w, int direction, int lines);
int	 tw_st		(WINDOW *w, int flag_no, int true_false);
int	 tw_stopin	(WINDOW *w, int set_type, char *set_list);
int	 tw_tr		(WINDOW *w, char *buffer, int length);
int 	 tw_tra		(WINDOW *w, ACHAR *text, int text_len);
int	 tw_uflag	(WINDOW *w, int true_false);
int 	 tw_wa		(WINDOW *w, ACHAR *text, int text_len);
int	 tw_wf		(WINDOW *w, int fill_char, int length);
int	 tw_where	(WINDOW *w, short *position);
int	 tw_write	(WINDOW *w, char *str, int bytes, int cc_option);
int	 tw_zadd	(WINDOW *w, char *command, int length);
int	 tw_zclear	(WINDOW *w);
int	 tw_zm		(WINDOW *w, int direction);
int	 tw_zn		(WINDOW *w, int command_number);
int	 tw_zo		(WINDOW *w_ed, WINDOW *w, int numbering_option);
int	 tw_zo1		(WINDOW *w_ed, WINDOW *w, int numbering_option);

WINDOW	*ta_dummy	(void);
WINDOW  *ta_open	(char *name, int lines, int cols, int attr, 
				int border, int pos_opt);
WINDOW  *ta_aopen	(char *name, char *text, int bytes, int attr, 
				int border, int pos_opt);
int	 ta_yes		(char *prompt, int attr, int pos_opt);
int	 ta_ret		(int attr, int pos_opt);
int	 ta_cmd		(char *cmd, char *input_file);
int      ta_gets	(char *prompt, char *buffer, int size, int pos_opt);
int      ta_passwd	(char *prompt, char *buffer, int size, int pos_opt);
int      ta_prp		(char *text, int len, int attr, int pos_opt);
int 	 ta_error	(char *msg);
WINDOW 	*tm_open	(char *title, int home_line, int home_col, 
			char **items_list, int items, int attribute);
int 	 tm_select	(WINDOW *w, int standout_attribute);

int	 tx_display	(WINDOW *w, char *TeX_text, int length, int clear_optn);
int	 tx_fdisplay	(WINDOW *w, int fid, int bytes, int clear_optn);
int	 tx_mdisplay	(WINDOW *w, char **text, int number, int clear_optn);
TWFIELD	*tx_fields	(WINDOW *w);
char	*tx_more	(char *env);
char	*tx_symbol	(char *symbol);
int	 tx_def		(char *symbol, char *equivalence);
int	 tx_file	(WINDOW *w, char *filename, int clear_optn);
int	 tx_math	(int (*fct)(char *, int len));
WINDOW  *tx_aopen	(char *name, char *text, int bytes, int attr, 
				int border, int pos_opt);
int	 tx_prp		(char *text, int text_len, int attribute, int border,
				int pos_optn);
int	 ty_display	(WINDOW *w, char *TeX_text, int length, int full_optn);
int	 ty_fdisplay	(WINDOW *w, int fid, int bytes, int full_optn);
int	 ty_mdisplay	(WINDOW *w, char **text, int number, int full_optn);
int	 ty_file	(WINDOW *w, char *filename, int full_optn);
int	 ty_close	(int doc_id);
int	 ty_pseek	(int doc_id, int pageno, int mode);
int	 ty_lseek	(int doc_id, int lineno, int mode);
int	 ty_show	(int doc_id);
int	 ty_more	(int doc_id, int key);
#else		/* Only functions that don't return int value	*/
typedef int (*TW_CHECK_FCT)();
char 	*tv_gterm();		/* Retrieve terminal name	*/
WINDOW 	*tw_hop();		/* Initialize Help Window	*/
WINDOW 	*tw_open();		/* Initialize function		*/
WINDOW 	*tw_getw();		/* Retrieve named window	*/
WINDOW 	*tw_getlw();		/* Retrieve linked window	*/
WINDOW 	*tw_gettw();		/* Retrieve Title window	*/
WINDOW	*tw_hset();		/* Attach Helkp Window		*/
WINDOW 	*tm_open();		/* Initialize Menu		*/
WINDOW 	*ta_dummy();		/* Dummy Window			*/
WINDOW 	*ta_open();		/* Adjusted Window		*/
WINDOW 	*ta_aopen();		/* Adjusted Window		*/
WINDOW 	*tx_aopen();		/* Adjusted Window with TeX	*/
TWFIELD *tx_fields();		/* Retrieve fields		*/
char	*tx_more();
char	*tx_symbol();
#endif

#define GetCommand		Gets

/*=======================================================================
 *		Functions common to TV / TW routines
 *=======================================================================*/


#define OpenTerm(dev,capfile,o)	tv_open(dev,capfile,o)
#define ResetTerm()		tv_reset()
#define CloseTerm()		tv_close()
#define GetTerm()		tv_gterm()
#define ScreenSize(d)		tv_dim(d)	/* Screen dimensions	*/
#define DefineControl(cc,act)	tv_setcc(cc,act)
#define GetControl(act)		tv_getcc(act)
#define SetControls(act)	tv_setsc(act)

#define CheckInput()		tv_inc()	/* Number of input chars */
#define UngetChar(c)		tv_supply(c,1,0)
#define SupplyInput(str)	tv_supply(str,(int)strlen(str),0)
#define SupplyInputs(str)	tv_supply(str,0,0)
#define QueueInput(str)		tv_supply(str,(int)strlen(str),1)
#define QueueInputs(str)	tv_supply(str,0,1)

#define OnInterrupt(f)		ostint(f)	/* Interrupt fct	*/
#define WhileInput(f)		tv_fw(f)

#define Bell()			tv_bell()
#define SendRaw(str,l)		tv_wsend(str,l)

#define GetChar(ch)		tv_getc(ch)
#define GetRaw(buf,lbuf)	tv_getb(buf,lbuf)
#define PutChar(ch)		ostwrite(ch,1)

#define ClearScreen()		tv_clear(_WHOLE_)

#define SetScroll(top,bot)	tv_sr(top,bot)
#define SetTimeout(sec)		tv_gto(sec)

#define ScreenCursor(pos2)	tv_where(pos2)
#define ScreenCursorTo(x,y)	tv_goto(x,y)

#define ScreenSave(tvsave)	tw_cus(tvsave)
#define ScreenRestore(tvsave)	tw_cur(tvsave)

#define	ScreenAttr(x)		tv_attr(x)
#define	ScreenWrite(str,l)	tv_write(str,l,1)
#define ScreenPut(str)		tv_write(str, (int)strlen(str),1)
#define ScreenChar(ch)		tv_getc(ch)
#define ScreenPuts(str)		tv_puts(str)

#define CheckCursor(c,lim)	tv_range(c,lim)
#define CheckBox(h,d,l)		tv_rb(h,d,l)
#define ConvertAchars(a,l,c)	tv_ac(a,l,c)

#define StopTerm()		tv_stop(1)
#define StartTerm()		tv_stop(0)
#define WaitTerm(ms)		tv_wait(ms)	/* Waits after each output */


#if TW_import

/*=======================================================================
 * 		TW macros in Window Context (tw / tx)
 *=======================================================================*/


#define OpenWindow(name,hl,hc,lines,cols,a,opt,com)	\
			tw_open(NULL_WINDOW,name,hl,hc,lines,cols,a,opt,com)
#define OpenSubWindow(w,name,hl,hc,lines,cols,a,opt,com)	\
			tw_open(w,name,hl,hc,lines,cols,a,opt,com)
#define OpenHelpWindow(name,hl,hc,cc,text)	\
			tw_hop(name,hl,hc,cc,text)
#define SetRange(w,i,j,len)	tw_mark(w,i,j,len)
#define RemoveRange(w)		tw_mark(w,0,0,0)

#define ScreenModified()	tw_uflag(NULL_WINDOW,1)
#define EchoOn			ActiveWindow
#define EchoOff			DeactiveWindow
#define ActiveWindow(w)		tw_st(w,Echo,1), TouchWindow(w)
#define DeactiveWindow(w)	tw_st(w,Echo,0)
#define IsOccluded(w)		tw_occluded(w,1)

#define	SetAttr(w,x)		tw_attr(w,x)
#define	GetAttr(w)		(w->attr)>>8
#define	ChangeAttr(w,len,x)	tw_mattr(w,len,x)
#define	ChangeRule(w,l)		tw_mrule(w,l)

#define Put(w,str)		tw_write(w,str,(int)strlen(str),1)
#define Puts(w,str)		tw_puts(w,str)
#define PutCentered(w,str)	tw_cline(w,str,(int)strlen(str))
#define	WriteAchars(w,str,l)	tw_wa(w,str,l)
#define	Write(w,str,l)		tw_write(w,str,l,1)
#define	WriteBinary(w,str,l)	tw_write(w,str,l,0)
#define DrawBox(w,tx,ty,dx,dy)	tw_box(w,tx,ty,dx,dy)
#define DrawHrule(w,n)		tw_rule(w,_RIGHT_,n)
#define DrawVrule(w,n)		tw_rule(w,_DOWN_,n)
#define Fill(w,ch,len)		tw_wf(w,ch,len)
#define FillWindow(w,ch)	tw_wf(w,ch,(w)->dim[0]*(w)->dim[1])

#define EnterImode(w)		(w)->flags |= Imode
#define ExitImode(w)		(w)->flags &= ~Imode
#define DeleteChars(w,n)	tw_dc(w,n)
#define InsertLines(w,n)	tw_il(w,n)

#define SaveCursor(twsave)	tw_cus(twsave)
#define RestoreCursor(twsave)	tw_cur(twsave)

#define GetHomePosition(w,h2)	tw_geth(w,h2)
#define GetPosition(w)		tw_awhere(w)
#define SetPosition(w,pos)	tw_agoto(w,pos)
#define GetCursor(w,s2)		tw_where(w,s2)
#define CursorLeft(w,n)		tw_mvc(w,_LEFT_,n)
#define CursorRight(w,n)	tw_mvc(w,_RIGHT_,n)
#define CursorUp(w,n)		tw_mvc(w,_UP_,n)
#define CursorDown(w,n)		tw_mvc(w,_DOWN_,n)
#define CursorHome(w)		tw_home(w)			
#define CursorTo(w,x,y)		tw_goto(w,x,y)
#define NewLine(w)		tw_nl(w)

#define ClearWindow(w)		tw_clear(w,_WHOLE_)	
#define ClearUp(w)		tw_clear(w,_UP_)	
#define ClearDown(w)		tw_clear(w,_DOWN_)	
#define ClearLeft(w)		tw_clear(w,_LEFT_)	
#define ClearRight(w)		tw_clear(w,_RIGHT_)	
#define ClearCursor(w)		tw_cc(w,1)
#define ClearChars(w,n)		tw_cc(w,n)
#define ClearLine(w)		tw_cl(w)

#define TouchWindow(w)		tw_r(w,0,NULL_WINDOW)			
#define RaiseWindow(w)		tw_r(w,1,NULL_WINDOW)
#define LowerWindow(w)		tw_r(w,8,NULL_WINDOW)
#define PlaceWindow(w,after)	tw_r(w,9,after)
#define RefreshScreen()		tw_r(NULL_WINDOW,1,NULL_WINDOW)
#define ShowWindow		RaiseWindow
#define Updatewindow		TouchWindow
#define RefreshWindow		RaiseWindow

#define ScrollUp(w,n)		tw_scroll(w,_UP_,n)
#define ScrollDown(w,n)		tw_scroll(w,_DOWN_,n)

#define MoveWindow(w,hl,hc)	tw_mw(w,hl,hc)
#define RemoveWindow(w)		tw_r(w,4,NULL_WINDOW)
#define RemoveWindows()		tw_r(NULL_WINDOW,4,NULL_WINDOW)
#define CloseWindow(w)		tw_close(w, 0)
#define CloseSubWindow(w)	tw_close(w, 2)
#define DestroyWindow(w)	tw_close(w, 1)

#define GetWindow(name)		tw_getw(name)
#define TitleWindow(w)		tw_gettw(w)
#define TopWindow()		PreviousWindow(NULL_WINDOW)
#define BottomWindow()		NextWindow(NULL_WINDOW)
#define ChildWindow(w)		tw_getlw(w,0)
#define NextWindow(w)		tw_getlw(w,1)
#define PreviousWindow(w)	tw_getlw(w,-1)
#define TranslateWindow(w,s,l)	tw_tr(w,s,l)
#define SaveWindowText(w,s,l)	tw_tra(w,s,l)
#define WindowName(w)		(w)->id

#define SetStopping(w,type,lis)	tw_stopin(w,type,lis)
#define EnableKeypad(w)		tw_stopin(w,_KEYPAD_,"*"),	\
				tw_stopin(w,_PF_,"*")
#define DisableKeypad(w)	tw_stopin(w,_KEYPAD_,""),	\
				tw_stopin(w,_PF_,"")
#define EnableArrows(w)		tw_stopin(w,_ARROW_,"^UDLR")
#define DisableArrows(w)	tw_stopin(w,_ARROW_,"")

#define Gets(w,str,len)		tw_mods(w,str,len, 0)
#define GetsWithCheck(w,s,l,f)	tw_modf(w,s,l,0,f)
#define Gets1(w,str,len,list)	tw_get1(w,str,len, list)
#define Mods(w,str,len)		tw_mods(w,str,len,(int)strlen(str))
#define ModsWithCheck(w,s,l,f)	tw_modf(w,s,l,(int)strlen(s),f)
#define GetKey(w, ch)		tw_getc(w, ch)
#define GetKey1(w, ch, list)	tw_gc1(w, ch, list)
#define GetKey2(w)		tw_gc2(w)
#define Mods2(w,s,l,f)		tw_ms2(w,s,l,(int)strlen(s),f)

#define AttachHelpWindow(w,wh)	tw_hset(w,wh)
#define RemoveHelpWindow(w)	tw_helps(w,0)
#define SwitchHelpWindow(w)	tw_helps(w,1)

#define ActiveStacking(w)	(w)->flags |= Stacking
#define DeactiveStacking(w)	(w)->flags &= ~Stacking
#define EnableStacking		ActiveStacking
#define DisableStacking		DeactiveStacking

#define ClearCommands(w)	tw_zclear(w)
#define SetCommand(w,n)		tw_zn(w, n)
#define AddCommand(w,str)	tw_zadd(w, str, (int)strlen(str))

#define EditCommand(w,wed)	tw_zo1(wed,w,0)
#define EditNumberedCommand(w,wed)	tw_zo1(wed,w,1)
#define ListCommands(w,wed)	tw_zo(wed,w,0)
#define ListNumberedCommands(w,wed)	tw_zo(wed,w,1)

#define PreviousCommand(w)	tw_zm(w, _UP_)
#define NextCommand(w)		tw_zm(w, _DOWN_)
#define LastCommand(w)		tw_zm(w, _HOME_)

#define InitWindows(dev,term_file,env)	tw_init(dev,term_file,env)
#define EndWindows()			tw_end()

/*=======================================================================
 * 		TX Functions (TermDisplay)
 *=======================================================================*/

#define ShowText(w,str,l)	tx_display(w,str,l,0)
#define ShowString(w,str)	tx_display(w,str,(int)strlen(str),0)

#define DisplayText(w,str,l)	tx_display(w,str,l,1)
#define DisplayString(w,str)	tx_display(w,str,(int)strlen(str),1)
#define DisplayArray(w,ss,n)	tx_mdisplay(w,ss,n,1)
#define DisplayContinuation(w)	tx_display(w,(char *)0,0,1)

#define DisplayFile(w,fname)	tx_file(w,fname,1)
#define DisplayFromFile(w,fid,bytes)	\
				tx_fdisplay(w,fid,bytes,1)

#define GetSymbol(symbol)	tx_symbol(symbol)	
#define SetSymbol(symbol,eq)	tx_def(symbol,eq)
#define GetCcode()		tx_more("Ccode")	
#define GetMarkedFields(w)	tx_fields(w)	
#define SetMathAction(f)	tx_math(f)

#define AdjustedTextWindow(n,s,l,a,b,o)	tx_aopen(n,s,l,a,b,o)
#define DisplayTextWindow(s,l,a,o)	tx_prp(s,l,a,0,o)
#define DisplayStringWindow(s,a,o)	tx_prp(s,(int)strlen(s),a,0,o)

/*=======================================================================
 * 		TY Functions (TermDoc)
 *=======================================================================*/

#define CreateDocText(w,str,l)	ty_display(w,str,l,1)	/* Full document */
#define CreateDocArray(w,ss,n)	ty_mdisplay(w,ss,n,1)	/* Full document */
#define CreateDocFile(w,fname)	ty_file(w,fname,1)	/* Full document */

#define OpenDocText(w,str,l)	ty_display(w,str,l,0)
#define OpenDocArray(w,ss,n)	ty_mdisplay(w,ss,n,0)
#define OpenDocFile(w,fname)	ty_file(w,fname,0)

#define CloseDoc(d)		ty_close(d)
#define ShowDoc(d)		ty_show(d)
#define SeekDocPage(d,n,mode)	ty_pseek(d,n,mode)
#define SeekDocLine(d,n,mode)	ty_lseek(d,n,mode)
#define MoreDoc(d,key)		ty_more(d,key)
#define atLastDocPage(d)	ty_end(d)

/*=======================================================================
 * 		Other Applications (ta and tm)
 *=======================================================================*/

#define OpenSmartWindow(n,lines,cols,a,bw,posop)			\
				ta_open(n,lines,cols,a,bw,posop)
#define AdjustedWindow(n,text,a,bw,posop)			\
				ta_aopen(n,text,(int)strlen(text),a,bw,posop)
#define DummyWindow()		ta_dummy()
#define GetYes(prompt)		ta_yes(prompt, _NORMAL_, _DOWNRIGHT_)
#define HitReturn(a,o)		ta_ret(a,o)
#define Execute(cmd,file)	ta_cmd(cmd,file)
#define DisplayError(msg)	ta_error(msg)
#define GetsAnswer(pr,s,l,o)	ta_gets(pr,s,l,o)
#define GetsPassword(s,l,o)	ta_passwd("Password",s,l,o)
#define WriteWindow(s,l,a,o)	ta_prp(s,l,a,o)
#define PutWindow(s,a,o)	ta_prp(s,(int)strlen(s),a,o)

#define OpenMenuWindow(title,hl,hc,list,items,a)	\
				tm_open(title,hl,hc,list,items,a)
#define GetMenuChoice(w,a)	tm_select(w,a)

/*=======================================================================
 * 		TV Functions (Screen level)
 *=======================================================================*/

#else

#define	SetAttr(x)		tv_attr(x)

#define SaveCursor(tvsave)	tv_cus(tvsave)
#define RestoreCursor(tvsave)	tv_cur(tvsave)

#define Put(str)		tv_write(str,(int)strlen(str),1)
#define Puts(str)		tv_puts(str)
#define	Write(str,l)		tv_write(str,l,1)
#define	WriteBinary(str,l)	tv_write(str,l,0)
#define	WriteGraphic(g,l)	tv_wg(g,l)
#define	WriteAchars(str,l)	tv_wa(str,l)

#define Hrule(n)		tv_rule(_RIGHT_,n)
#define Vrule(n)		tv_rule(_DOWN_,n)
#define DrawBox(tx,ty,dx,dy)	tv_box(tx,ty,dx,dy)
#define	ShowHelp()		tv_help()

#define SetStopping(type,lis)	tv_stopin(type,lis)
#define EnableKeypad()		tv_stopin(_KEYPAD_,"*"),	\
				tv_stopin(_PF_,"*")
#define DisableKeypad()		tv_stopin(_KEYPAD_,""),	\
				tv_stopin(_PF_,"")
#define Gets(str,len)		tv_mods(str,len, 0)
#define Gets1(str,len,list)	tv_get1(str,len, list)
#define Mods(str,len)		tv_mods(str,len,(int)strlen(str))
#define GetPassword(str,len)	tv_getp(str,len)

#define GetCursor(pos2)		tv_where(pos2)
#define CursorTo(x,y)		tv_goto(x,y)
#define MoveCursor(dir,n)	tv_mvc(dir,n)
#define CursorLeft(n)		tv_mvc(_LEFT_,n)
#define CursorRight(n)		tv_mvc(_RIGHT_,n)
#define CursorUp(n)		tv_mvc(_UP_,n)
#define CursorDown(n)		tv_mvc(_DOWN_,n)
#define CursorHome()		tv_home()
#define NewLine()		tv_nl()
#define GetPosition()		tv_pos()
#define SetPosition(pos)	tv_agoto(pos)

#define ClearUp()		tv_clear(_UP_)
#define ClearDown()		tv_clear(_DOWN_)
#define ClearLeft()		tv_clear(_LEFT_)
#define ClearRight()		tv_clear(_RIGHT_)
#define ClearCursor()		tv_cc(1)
#define ClearChars(n)		tv_cc(n)
#define ClearLine()		tv_cl()

#define ScrollUp(n)		tv_scroll(_UP_,n)
#define ScrollDown(n)		tv_scroll(_DOWN_,n)


#define DeleteLines(n)		tv_dl(n)
#define DeleteChars(n)		tv_dc(n)
#define InsertLines(n)		tv_il(n)
#define InsertBlanks(n)		tv_ic(n)
#define InsertString(str)	tv_iwrite(str,(int)strlen(str))
#define Iwrite(str,l)		tv_iwrite(str,l)
#define EnterImode()		tv_imode(1)
#define ExitImode()		tv_imode(0)

#endif


#endif		/* End of Definition File */
