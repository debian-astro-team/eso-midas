/* @(#)tform.h	19.1 (ESO-IPG) 02/25/03 13:49:44 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		TFORM.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Forms
.COMMENTS	

.ENVIRONMENT	TermWindows

.VERSION 1.0	07-Jun-1989: Created. 

------------------------------------------------------------*/

#ifndef TFORM_DEF

#define TFORM_DEF	0

#include <tw.h>		/* Requires TermWindows Definitions 	*/
#include <formtypes.h>	/* The atomic supported data types	*/

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

#ifndef TFORM_MACROS
#define TFORM_MACROS	1	/* Macro Definitions are provided */
#endif

/*=======================================================================
 * 		Basic Structures
 *=======================================================================*/

#if _TEMPLATES_
typedef int	(*TF_FCT)();	/* (*TF_FCT)(TFORM *form);	*/
typedef int	(*TF_CHECK)();	/* (*TF_CHECK)(TFORM *f, char *str, int size);*/
#else
typedef int	(*TF_FCT)();
typedef int	(*TF_CHECK)();
#endif

typedef	struct	field		{
	char	*name;		/* Name of this field 		*/
	short	pos[2];		/* Position on Window		*/
	unsigned char attr;	/* Video Attribute of Field 	*/
	unsigned char type;	/* Type in memory		*/
	short	string_size;	/* maximum size of string	*/
	char	*string;	/* String expression		*/
	union	{		/* storage for data 		*/
		long   integer;		/* type is Integer	*/
		long   intnull[2];	/* type is Integer+null	*/
		double real;		/* type is floating	*/
		WINDOW *window;		/* Type is String (subw)*/
		TF_FCT action;		/* Type is Action	*/
		}	value;
	char	*picture;	/* Conversion string <-> sval	*/
	int	factor;		/* sval=value.integer[0]/factor	*/
	unsigned short options;	/* Options (from 0x100 for user)*/
#define _FIELD_TOSCREEN		0x0001 	/* display on screen 	*/
#define _FIELD_DISPLAY		0x0002	/* Display-only		*/
#define _FIELD_NOECHO		0x0004	/* No echo		*/
#define _FIELD_MODIFIED		0x0008	/* Was modified		*/
#define _FIELD_COMPUTED		0x0010	/* Was modified		*/
#define _FIELD_TOFILE		0x0020 	/* display onto file 	*/
#define _FIELD_RAW		0x0040 	/* display onto file 	*/
	unsigned char bintype;	/* Type for Binary Output	*/
	unsigned char parm_size;/* Length of Parameters		*/
	char	*parm;		/* Parameters 			*/
	char	*note;		/* associated stuff (optional)	*/
	double  binfactor;	/* Binary = integer/binfactor	*/
	}	TFIELD;

typedef	struct	TFORM	{
	char	*name;		/* Form name				*/
	short	mfields;	/* Maximal number of available fields	*/
	short	nfields;	/* How many fields in this form		*/
	short	ifield;		/* Index of the current field		*/
	unsigned short options;
#define _FORM_TOSCREEN		_FIELD_TOSCREEN	/* Show on Screen	*/
#define _FORM_TOFILE		_FIELD_TOFILE	/* display onto file 	*/
	TFIELD	*fields;	/* ptr to beginning of array of FIELDSs	*/
	WINDOW	*window;	/* Window associated to Form		*/
	struct	TFORM *prev;	/* Chain of FORM structures		*/
	int	*avalue;	/* CUrrent Value (related to cc)	*/
	short	pgmno;		/* Parameter for compute_fct		*/
	short	pgmcheck;	/* Parameter for check_fct		*/
	TF_FCT 	compute_fct;	/* How to Compute Fields		*/
	TF_CHECK  check_fct;	/* Input Check Function			*/
	short	afileno;	/* File Number for ASCII file		*/
	short	bfileno;	/* File Number for BINARY file (unsued)	*/
	long	acount;		/* Records written onto ASCII file	*/
	long	bcount;		/* Records written onto BINARY file	*/
   }	TFORM;

#define NULL_FORM	(TFORM *)0

/*=======================================================================
 * 		TF Functions 
 *=======================================================================*/

#if _TEMPLATES_
TF_FCT 	 tf_fset	(TFORM *form, TF_FCT fct);
TF_FCT 	 tff_aset	(TFORM *form, char *field_name, TF_FCT action);
TF_CHECK tf_cfset	(TFORM *form, TF_CHECK fct);

TFORM	*tf_new	 	(char *name, int mfields, WINDOW *w);
TFORM 	*tf_find	(char *name);
TFORM 	*tf_next	(TFORM *form);
TFORM 	*tf_load	(char *formname, char *filename,   WINDOW *fw);
TFORM 	*tf_make	(char *formname, char *tex_string, WINDOW *fw);
int 	 tf_free	(TFORM *form);
int 	 tf_active	(TFORM *form, int yes_or_no);
int 	 tf_raise	(TFORM *form);
int 	 tf_remove	(TFORM *form);
int 	 tf_oclear	(TFORM *form, int mask);
int 	 tf_opwd	(TFORM *form, int mask);
int 	 tf_clear	(TFORM *form);
int 	 tf_oset	(TFORM *form, int mask);

int 	 tff_new	(TFORM *form, char *name, char *type, char *pic, 
		 	int line, int col, int lines, int cols, TF_FCT f) ;
int 	 tff_anew	(TFORM *form, char *name, char *type, char *pic, 
		 		char *text, int attr, TF_FCT action);
char 	*tff_parm	(TFORM *form, char *parm);
int 	 tff_find	(TFORM *form, char *name);
int 	 tff_goto	(TFORM *form, int fieldno);
int 	 tff_move	(TFORM *form, int direction);
int 	 tff_cur	(TFORM *form);
int 	 tff_clear	(TFORM *form);
TFIELD 	*tff_field	(TFORM *form, int fieldno);
TFIELD 	*tff_ptr	(TFORM *form, char *field_name);
char 	*tff_string	(TFORM *form);
long 	 tff_ival	(TFORM *form);
double 	 tff_fval	(TFORM *form);
int 	 tff_isnull	(TFORM *form);
char	*tff_pic	(TFORM *form);
char 	*tff_dtype	(TFORM *form);
int 	 tff_oclear	(TFORM *form, int mask);
int 	 tff_oset	(TFORM *form, int mask);
int 	 tff_oget	(TFORM *form, int mask);
int 	 tff_putnote	(TFORM *form, char *note);
char 	*tff_note	(TFORM *form);

int 	 tf_edit	(TFORM *form);

int 	 tff_show	(TFORM *form);
int 	 tff_nshow 	(TFORM *form);
int 	 tff_pshow 	(TFORM *form);
int 	 tff_dshow 	(TFORM *form);
int 	 tff_oshow 	(TFORM *form, int mask, int ifset);
int 	 tff_display	(TFORM *form, char *string);
int 	 tf_update	(TFORM *form);
int 	 tf_show	(TFORM *form);
int 	 tf_nshow 	(TFORM *form);
int 	 tf_pshow 	(TFORM *form);
int 	 tf_dshow 	(TFORM *form);
int 	 tf_oshow 	(TFORM *form, int mask, int ifset);

int 	 tff_convert 	(TFIELD  *field, char *instr);
int 	 tff_check 	(TFIELD  *field, char *instr);
int 	 tff_edit 	(TFIELD  *field);
int 	 tff_dset	(TFIELD  *field, char *specifications);
int 	 tf_cc		(TFORM *form, char *source, int exec_pgm);

int 	 tff_get 	(TFORM *form);
int 	 tff_mods	(TFORM *form, char *str, int size, TF_CHECK fct);
int 	 tff_nget	(TFORM *form, TF_CHECK fct);
int 	 tf_nget	(TFORM *form, TF_CHECK fct);
int 	 tf_get 	(TFORM *form);
int 	 tf_oget 	(TFORM *form, int mask, int ifset);

char	*tf_fsep	(char *text);
int	 tf_fopen	(TFORM *form, char *filename);
int	 tf_fclose	(TFORM *form);
int	 tf_fput	(TFORM *form);
int	 tf_ftitle	(TFORM *form);

#else
TF_FCT  tf_fset(), tff_aset();
TF_CHECK tf_cfset();
TFORM	*tf_new(), *tf_find(), 	*tf_next(), *tf_load(), *tf_make();
TFIELD 	*tff_field(), *tff_ptr();
char 	*tff_parm(), *tff_string(), *tff_pic(), *tff_dtype(), *tff_note();
char 	*tff_ftext();
long 	tff_ival();
double 	tff_fval();
#endif

#if TFORM_MACROS
/*=======================================================================
 * 		TF Macros
 *=======================================================================*/

#define ischarField(f)			(((f)->type)&4)
#define isintField(f)			((((f)->type)&0xf) == _DTYPE_INT)
#define isrealField(f)			((((f)->type)&0xf) == _DTYPE_DOUBLE)
#define isactionField(f)		((((f)->type)&0xf) == _DTYPE_ACTION)

#define NewForm(name,fields,w)		tf_new(name,fields,w)
#define LoadForm(name,file,w)		tf_load(name,file,w)
#define MakeForm(name,tex_text,w)	tf_load(name,tex_text,w)
#define SetComputeFunction(f,fct)	tf_fset(f,fct)
#define SetCheckFunction(f,fct)		tf_cfset(f,fct)
#define FreeForm(f)			tf_free(f)
#define FindForm(name)			tf_find(name)
#define NextForm(f)			tf_next(f)
#define RaiseForm(f)			tf_raise(f)
#define RemoveForm(f)			tf_remove(f)
#define ClearForm(f)			tf_clear(f)
#define ActiveForm(f)			tf_active(f, 1)
#define DeactiveForm(f)			tf_active(f, 0)

#define CompileCcode(f,source,pgm)	tf_cc(f,source, pgm)

#define ClearFormOption(f,m)		tf_oclear(f,m)
#define ClearPasswordFields(f,m)	tf_opwd(f,m)
#define SetFormOption(f,m)		tf_oset(f,m)

#define NewField(f,n,t,p,hl,hc,l,c,fc)	tff_new(f,n,t,p,hl,hc,l,c,fc)
#define InstallField(f,n,t,p,txt,a,fc)	tff_anew(f,n,t,p,txt,a,fc)
#define SetFieldAction(f,name,fct)	tff_aset(f,name,fct)
#define FindFieldParm(f,key)		tff_parm(f,key)
#define FindField(f,name)		tff_find(f, name)
#define FindFieldPointer(f,name)	tff_ptr(f, name)
#define GotoField(f,fieldno)		tff_goto(f, fieldno)
#define MoveField(f,direction)		tff_move(f, direction)
#define MovePermittedField(f,direction)	tff_move(f, direction|0x10)
#define GotoCurrentField(f)		tff_goto(f, f->ifield)
#define ClearField(f)			tff_clear(f)

#define CurrentField(f)			tff_cur(f)
#define FieldPointer(f,fieldno)		tff_field(f,fieldno)
#define FieldName(f)			tff_name(f)
#define FieldString(f)			tff_string(f)
#define FieldInteger(f)			tff_ival(f)
#define FieldDouble(f)			tff_fval(f)
#define isNullField(f)			tff_isnull(f)
#define FieldPicture(f)			tff_pic(f)
#define FieldDatatype(f)		tff_dtype(f)
#define FieldNote(f)			tff_note(f)

#define ClearFieldOption(f,mask)	tff_oclear(f, mask)
#define SetFieldOption(f,mask)		tff_oset(f, mask)
#define GetFieldOption(f,mask)		tff_oget(f, mask)
#define PutFieldNote(f,note)		tff_putnote(f, note)

#define EditForm(f)			tf_edit(f)
#define ShowField(f)			tff_show(f)
#define ShowNote(f)	 		tff_nshow(f)
#define ShowPicture(f)			tff_pshow(f)
#define ShowDatatype(f)	 		tff_dshow(f)
#define ShowOption(f,m,c)		tff_oshow(f, m, c)
#define DisplayInField(f,s)		tff_display(f,s)
#define TouchForm(f)			tf_update(f)
#define UpdateForm(f)			tf_update(f)
#define ShowForm(f)			tf_show(f)
#define ShowNotes(f)			tf_nshow(f)
#define ShowPictures(f)			tf_pshow(f)
#define ShowDatatypes(f)		tf_dshow(f)
#define ShowOptions(f,m,c)		tf_oshow(f, m, c)

#define ConvertField(field,s)		tff_convert(field,s)
#define CheckField(field,s)		tff_check(field,s)
#define EditField(field)		tff_edit(field)
#define SetFieldType(field,sp)		tff_dset(field,sp)

#define GetField(f)			tff_get(f)
#define GetForm(f)			tf_get(f) 
#define GetFormOption(f,m,c)		tf_oget(f,m,c)
#define ModsField(f,str,l,fct)		tff_mods(f,str,l,fct)
#define GetFieldNote(f,fct)		tff_nget(f,fct)
#define GetFormNotes(f,fct)		tf_nget(f,fct)

#define ColumnSeparator(text)		tf_fsep(text)
#define OpenFormFile(f,name)		tf_fopen(f, name)
#define CloseFormFile(f)		tf_fclose(f)
#define WriteFormFile(f)	 	tf_fput(f)
#define WriteFormTitle(f)		tf_ftitle(f)

#endif

#endif
