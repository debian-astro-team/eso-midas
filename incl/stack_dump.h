
/*++++++++++++++
.IDENTIFICATION stack_dump.h
.LANGUAGE       C
.AUTHOR         K. Banse - ESO Garching
.ENVIRONMENT    Any
.KEYWORDS       Fortran to C interface
.COMMENTS       This code is inserted into the different routines 
		of ftoc_stack.fc
.VERSION  1.0   07-Mar-2005
.ALGORITHM      show parameter stack in C of the Fortran caller

 060228         last modif

---------------*/


printf("\nnoargs = %x, *noargs = %d\n",noargs,*noargs);
if (nogo[0] == 1) 
   {
   adr = (char *)noargs;
   (void)printf("char *noargs = %c - and %s\n",*adr,adr);
   }
else
   {
   (void)printf("noargs+1 = %x, *(noargs+1) = %d\n",(noargs+1),*(noargs+1));
   (void)printf("noargs+2 = %x, *(noargs+2) = %d\n",(noargs+2),*(noargs+2));
   }
(void)printf("\n");

for (k=kstart; k<=kend; k++)
   {
   (void)printf("stack # %d: ",k);
   adr = PARAM(k,0,1,char *);
   (void)printf("adr = %p (%%p), %x (%%x), %d (%%d)\n",adr,adr,adr);

/*
   nsiz1 = PARAM(k,0,1,int );
   printf("as int = %d\n",nsiz1);
   sprintf(txt,"%p",adr);
   numb = 0;
   if (strcmp(txt,"(nil)") == 0)
      {
      numb = 100;
      goto move_on;
      }
   iadr = (int *) adr;
   printf("as *iadr = %d\n",*iadr);
   n = ((int *)(Cargs))[k];
   (void)printf("as ((int *)(Cargs))[%d] = %d\n",k,n);

*/

   if (nogo[k] == 0) goto move_on;

   numb = nogo[k];
   if (numb == 1) 
      {
      stparm = PARAM(k,0,1,char *);
      (void)printf("as PARAM(k,0,1,char *) = %s|\n",stparm);
      cpt = PARAM(k,0,1,char **);
      (void)printf("as PARAM(k,0,1,char **) = %s|\n",cpt);
      }
   else if (numb == 2) 
      {
      iadr = (int *) adr;		/* PARAM(k,0,1,int *); */
      (void)printf("iadr = %d, *iadr = %d\n",iadr,*iadr);
      (void)printf("   <> of iadr-1, iadr, iadr+1 = %d, %d, %d\n",
                *(iadr-1),*iadr,*(iadr+1));
      lk = PARAM(k,0,1,long int );
      (void)printf("and PARAM(k,0,1,long int) =  %ld \n",lk);
      }

  move_on:
   (void)printf("------------------------- %d\n",numb);
   }

/*
if (stack_off == 111)
   {
   adr = (char *) noargs;
   (void)printf("\n*noargs as string = %s\n",*adr);
   }

iadr =  va_arg(Cargs,int *);
(void)printf("\nva_arg(Cargs,int *) = %x\n\n",iadr);
(void)printf("\nva_arg(Cargs,int *) = %x, and points to %d\n",iadr,*iadr);
stparm = va_arg(Cargs,char *);
(void)printf("va_arg(Cargs,char *) = %s|\n\n",stparm);
stparm = va_arg(Cargs,char **);
(void)printf("stparm = %s|\n",stparm);
*/


