/* @(#)agldcom.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:23   */
/*
 * HEADER : agldcom.h      - Vers 3.6.000  - Oct 1991 -  L. Fini, Oss. Arcetri
 *
 *
 *                                 Communication buffer access macros
 */

#ifndef DRVCBUFMAX
#define DRVCBUFMAX 132   /* Do not change: affects standard interface        */

struct bufcom {		/* Driver communication structure                   */
	float	*XARRAY;		/* Pointer to X array               */
	float	*YARRAY;		/* Pointer to Y array               */
	float	FLTB[10];
	int	INTB[20];		/* Modified since version 3.53      */
	int	ERRCOD;
	int	CHANNL;
	char	CHRBUF[DRVCBUFMAX];
	int	(*cnvrt)();		/* Pointer to conversion routine    */
					/* Used in aglvloc()                */
};

#define NPOINTS		AGLDVCOM->INTB[0]
#define VECTX		AGLDVCOM->XARRAY
#define VECTY		AGLDVCOM->YARRAY
#define RBUFFR(x) 	AGLDVCOM->FLTB[x]
#define IBUFFR(x) 	AGLDVCOM->INTB[x]
#define CHANNEL   	AGLDVCOM->CHANNL
#define CHARBUF   	AGLDVCOM->CHRBUF
#define ERRCODE   	AGLDVCOM->ERRCOD
#define CNVRT		(*AGLDVCOM->cnvrt)	/* Use as: CNVRT(&x,&y)     */

#endif
