/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2009 European Southern Observatory
.IDENTifer   proto_gen.h
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for General interfaces.
.VERSION     1.0     20-May-1994   Creation by RvH
090416		last modif
------------------------------------------------------------*/

#ifndef PROTO_GEN
#define PROTO_GEN

#ifdef  __cplusplus
extern "C" {
#endif


#ifdef __STDC__
extern int  Cjmagn ( int jmeth, float *p_img, int *npix, int ni, int nb, 
                     float *fac,
                     float *xycen, float *mag, float *dmag, 
                     float *sky, float *dsky, float *nrpix, float *flux );
extern int  Cstacen( char *meth, float *p_img, int *npix, int *image, 
                     float *xypos, float *xyerr, float *xysig, float *xyval );
extern void Czima  ( float *p_in, int *npix, float *xindx, float *yindx, 
                     int ndim, float *p_out, float *fmin, float *fmax );
extern int  Cpixlin( float xa, float ya, float xb, float yb,
                     double step, int ndim, float *xindx, float *yindx );
extern int  Cstvals( char *action, float *data, int naxis, int *npix, 
                     int *sublo, int *subhi, float *cutvls, float *results,
                     int *respix, int *nopix );
extern void  Chistvals( float *, int, int *, int *, int *, 
                                     float *, float, int, int *);
extern int  Convcoo(int intval, int imno,char *string,int pixdim,
                    int *subdim,int *sublo,int *subhi);

/*
 * module LUTTBL
 */
extern void OPNTBL (char *table,int *tid,int *ncols,int *nrows);
extern void CRELUT (char *table,float *rlut,int lutlen,int format,int *range);
extern void CREITT (char *table,float *ritt);

/*
 * module CALC
 */
extern void Cdspnul( int ncc, float usrNaN );
extern void Cfun1c( char *cfunc, float usrNaN, float aval, 
                                 float *result, int *ncount );
extern void Cfun2c( char *cfunc, float usrNaN, float *aval, 
                                 float *result, int *ncount );
extern void Copcc ( char *opcode, float scalA, float scalB, float usrNaN, 
                                               float *scalR, int *ncount );
extern void Cfn2ffw( char *cfunc, float *pntrA, int *npixA, int (*dimA)[2],
                                  float *pntrB, int *npixB, int (*dimB)[2],
                                  int *npixR, int (*dimR)[2], float *pntrR );
extern void Copffw( char *opcode, int tstflg, float usrNaN,
                                  float *pntrA, int *npixA, int (*dimA)[2],
                                  float *pntrB, int *npixB, int (*dimB)[2],
                      int *npixR, int (*dimR)[2], float *pntrR, int *ncount );
/*
 * module CCOPYF
 */
extern void Ccopwnd( float *pntrA, int *npixA, float *pntrB, int *npixB, 
                      int *bgnA, int *bgnB, int *endA );
extern void Ccopyf1( float *pntrA, int *npixA, int *bgnA, int *dimwA, 
                    float *pntrB, int *npixB, int *bgnB );
extern void Ccopyf2( float value, 
                    float *pntrB, int *npixB, int *bgnB, int *dimwB );
extern void Ccopyf ( float *pntrA, float *pntrB, int dim );
extern void Ccopyi ( int *pntrA, int *pntrB, int dim );
extern void Cconfil( float value, float *pntrB, int dim );
extern void Ccopyfx(float *pntrA,int imni,int imno,int *npix,int istrip);
/*
 * module CUTIL
 */
extern void FRAMOU_C(char *frame);
extern void BLANKO_C(char *string);
extern void OSCHAR_C(char *cval);
extern int fp2wc(int flag,int imno,double *coordin,double *coordout);

/*
modules from libsrc/math
*/

extern float randm( int *pdum);

#endif

#ifdef  __cplusplus
}
#endif

#endif
