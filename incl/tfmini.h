/* @(#)tfmini.h	19.1 (ESO-IPG) 02/25/03 13:49:43 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE        C
.IDENTIFICATION  header file TFMINI.H
.AUTHOR         J.D.Ponz
.KEYWORDS       Table control buffers
.ENVIRONMENT    Independent
.VERSION        3.0  1 Feb 1987 : Conversion into C
------------------------------------------------------------------------*/

struct TBL_FMT FMT[FMT_NBUF];

long FMT_NCOL, FMT_NROW, FMT_LLEN;
