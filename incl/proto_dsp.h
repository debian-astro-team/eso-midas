/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2007 European Southern Observatory
.IDENTifer   proto_dsp.h
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for Display interfaces.
.VERSION     1.0     20-Apr-1994   Creation by RvH
 070508		last modif

------------------------------------------------------------*/

#ifndef PROTO_DSP               /* Avoid redefinitions */
#define PROTO_DSP        0

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef __STDC__
extern struct Point *pnt;

/*
 * High level Display interfaces
 */
extern void DCOPEN(int flag);
extern void DCCLOS(int dispno);
extern int  DCSINF(int dispno, int option, int* ibuf, float* rbuf, char* cbuf);
extern int  DCGICH(int chanl);
extern void DCPICH(int chanl);
extern void DCLOAD( int imno, char *name, int dsplfl, int *icent, 
                              float *cuts, int *scale, int chan_flag );
/*
 * low level display interfaces
 */
extern void Cangle  ( float alpha, float size, int *ic );
extern void CONCHA_C( int dsplay, int chan, int grflag, int value );
extern void CURINF_C( int dsplay, int *nocurs, int *roiflg );
extern void DAZWAI_C( int *mytime );
extern void DRAW_IMA( void );
extern void DRAW_ANY( int intens );
extern void FIXP_INP(char *shape, char *ref,char *coord,char *frame,
                     int ncurs, float *cpos);
extern int  KEYW_INP( char *keynm, int ncurs, float *cpos );
extern int  CURS_INP( char *shape, int *ncurs, float *cpos );
extern int  TABL_INP( char *table, char *frame, char *ref, char *shape,
                                   int *aux_flag, float *cpos, float *aux );
extern void DRIMG_C ( int dsplay, int chan, int over, int *iaux, float *data, 
                      int npix, int *icen, float *cuts, int *sc, float ang );
extern void Cgetstr ( char *outstr, int *dim );
extern void HSIRGB_C( int flag, float *hsi, float *rgb );
extern int  JOYSTK_C( int dsplay, int iact, int nocurs, int *jx, int *jy );
extern void MYCUTS_C( char *meth, int flg, int imno, int size, 
                      int mapsiz, float *cuts );
extern void LOADWN_C( int *flags, int imno, int *npix, 
                      int *stapix, int *kpix, int *wsta, float *cuts );
extern void I1PACK_C(char *pntr, int *aux, float *faux, 
                      unsigned char *cdata, int *outaux);
extern void K1PACK_C(char *pntr, int *aux, float *faux, 
                      unsigned char *cdata, int *outaux);
extern void I1UNPA_C(unsigned char *cdata, int ndim, int *ibuf);
extern void F1UNPA_C(unsigned char *cdata, int ndim, float *buf);
extern void RDMON_C(int dsplay, int ichan, int ochan, int isplit, 
                      int *datdaz );
extern void RIMG_C(int chan, int ittflg, int *bgnA, int *dimA, float *ar);
extern void SETCUR_C(int dsplay, int cursno, int forma, int colo, int *coor);
extern void SIZER_C(int memsz, int npix, int scale, int scale_flag, int *icen,
                      int *nima, int *nssta, int *nf, int *nfsta);
extern void SPLCNT_C(int (*splcx)[5], int (*splcy)[5]);
extern void SUBCUR_C(int jflag);
extern void SUBDZT_C(char *action, int *dazhld, int dsplay, char *table);
extern void SUBEXT_C(int ncurs, int loopfl, int splmod, char *frame);
extern void SUBMEM_C(char *action, int *dazhld);
extern void SUBRDI_C(char *action, int ittflg, int ncurs, 
                                                int *nrpx, char *frame );
extern void SWAP_PNT(int indx1, int indx2, struct Point *);
extern void CURS_SETUP(int ncurs, int cform, int color);
extern int  GET_CPOS(int zoomwn, char *actflg, int curfl, int ncurs, 
		     int *iter, int *enter, struct Point *);
extern void END_PNT ( double rm, struct Point *, int *disp );
extern void TRACE1D ( char *action, int maxsize, int imf, 
                                    double stepln, char *outfra );
extern int  TRACE2D ( int imf, double *stepln, char *outfra );
extern void VIEWIT_C( int plotfl, int tid, int *colref, 
                      int imno, char *frame, float *cuts );
extern void WIMCLO_C( int dsplay, int chan, int refsc, char *frame, 
                      double *start, double *step, float *cuts );
extern void WIMGA_C ( int ldspno, int limch, int imno, int *khelp, int loaddir,
                      int *npix, int *icen, float *cuts, int *scale );
extern void WIMGB_C ( int imno, int *khelp, int *npix, float *cuts, 
                      int *scale, int outno, int *aux );

extern void buildgra ( char *shape, int *coords, float *arcs, 
		       int *xfig, int *yfig, int figmax, int *nop );
extern void GetCursor( char *action, char *frame, float *xyinfoa,
                      int *stata, float *xyinfob, int *statb );
extern float Pixy    ( float *a, int npix, int kx, int ky, int avfla );
/*
 * module CAUXWND
 */
extern int SetupIDI(char *devname,char *xstatio,int *dinfo);
extern int  Cauxwnd(int flag,int *info,int *xya,int *xyb);
extern void auxhelp(int flg);
extern int  Pixconv(char *cflag,int imno, double *dbuf1,
                    double *dbuf2, double *dbuf3);

/*
 * module CCURSIN
 */
extern void Ccursin( int dsplay, int iact, int nocurs, int *xya, 
                                 int *isca, int *xyb, int *iscb );
/*
 * module PSLIBI
 */
extern int psopen ( char *name, char *ident, double xsize, double ysize,
                    double xoff, double yoff, double angle, double *res );
extern int psmode ( int mode, double rbgc, double gbgc,double bbgc );
extern int psclose( void );
extern int psitf  ( int mode, int size, int *itt, 
                    int *red, int *green, int *blue );
extern int psframe( int mode, double xorg, double yorg, double xsize, 
                    double ysize, char *font, int fsize, double xstr, 
                    double ystr, double xend, double yend );
extern int psimage( int nx, int ny, int fmt, int bpp );
extern int psline( unsigned char *pbi, unsigned char *pgi, unsigned char *pri);
extern int pscapt ( double xstr, double ystr, char *font, int fsize, 
                    double offset );
extern int pstext ( char *label, char *str );
extern int pslabel( double xstr, double ystr, char *font, int fsize, 
                    char *label);
extern int pswedge( int mode, double xorg, double yorg, double xsize, 
                    double ysize, double zlow, double zhigh );
/*
 * module UTIL
 */
extern int  Cdazvis( int dsplay, int chanl, int flag, int vis );
extern int  Cdazscr( int dsplay, int chanl, int *scrx, int *scry );
extern void Crefrovr( void );
extern void Getactdsp( char *strsxw, char *strwnd );
extern int  Cdazzsc( int dsplay, int chanl, int zoom, int *scrx, int *scry );
extern void Sc2ch  (int dirflag, int *x,int *y);
extern void MakeLUT( int icount, float *rlut, int ocount, float *olut );
extern void MakeITT( int icount, float *ritt, int ocount, float *oitt );

/*
 * module WALPHB
 */
extern void Alphamem( int chan );
extern void Alpb2x  ( float *value, char *abuf );
extern void Alpcurs ( int cno, int flag );
extern void Alptxec ( char *text, int xp, int yp, int colour );
extern void Alptext ( int memo, char *text, int xp, int yp, int colour );

#endif

#ifdef  __cplusplus
}
#endif


#endif
