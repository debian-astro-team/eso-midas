C @(#)deanzas.inc	19.1 (ESO-IPG) 02/25/03 13:49:27
C 
C --------------------------   DEANZAS.INC ------------------------------
C	
	INTEGER*4	DZTRIG(2,10)
C
	COMMON	/DZDEVB/ DZTRIG
C	
C ------------------------
C 
C	DZTRIG(1,n)	= subsystem flag: 
C  			  0 if slot unused
C  			  1 for cursor #0
C			  2 for cursor #1
C			  3 for ROI = cursors #0 + #1
C			  4 for locator movement
C			  5 for trigger #0 (EXIT button of Locator)
C			  6 for trigger #1 (ENTER button of Locator)
C 
C	DZTRIG(2,n)	= interactive operation enabled (as defined in IDI doc)
C  
C ------------------------
C 
C	K. Banse	version 1.20	900327
C
C -----------------------------------------------------------------
