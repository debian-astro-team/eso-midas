/* @(#)x11defs.h	19.1 (ESO-IPG) 02/25/03 13:49:45 */
/* ----------------------------------------------------------------- 
            Include file x11defs.h                                
                                                                 
 K.Banse   890316	creation 
  
 020211		last modif				
 
 ----------------------------------------------------------------- */

# include <xsysinclude.h>

#define   XLUTFACT   65535.0
#define   MAX_PTS	400			/* synchronized with AGL! */


Display            *mydisp[MAX_WST];
Visual             *myvis[MAX_WST];
Colormap           cmap[MAX_WST][MAX_LUT];
Cursor             mycurs[MAX_WST][4];
GC                 gcima[MAX_DEV], gcdraw[MAX_DEV];
GC                 gclut[MAX_DEV], gcalph[MAX_DEV];
XGCValues          xgcvals;
KeySym             mykey;
Status             x11stat;
Drawable           xdrawable;
Window             rw[MAX_WST], mwndw[MAX_DEV], lutwnd[MAX_DEV],
                   alphwnd[MAX_DEV];

XColor             mycolr[MAX_WST][256*MAX_LUT], fixcolr[MAX_WST][9], exact;
XEvent             myevent;
XFontStruct        *myfont[MAX_WST][4];
XImage             *myima, *mxima[MAX_DEV][MAX_MEM], *mzima[MAX_DEV][MAX_MEM];
XImage             *lutxima[MAX_DEV];
XImage             *hcopy[MAX_DEV];
Pixmap             mxpix[MAX_DEV][MAX_MEM];
XPoint		   mypoint[MAX_PTS+1];
/***
XSegment           myvect[MAX_PTS];
***/
XSegment           curso0[MAX_DEV][4], curso1[MAX_DEV][4], roio[MAX_DEV][4],
                   zoomcross[2];
XSetWindowAttributes   sattributes;
XWindowAttributes      attributes;
XSizeHints         myhint[MAX_DEV];
XVisualInfo        vinfo;
XWMHints           xwmh;
XComposeStatus     xcstat;

long int           event_mask;
int                myscreen;


