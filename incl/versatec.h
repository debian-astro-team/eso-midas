/* @(#)versatec.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:45   */
/*
 * HEADER : versatec.h     - Vers 3.5.003 - Aug 1990 - F. Tribioli, Oss. Arcetri
 *
 * Include file with common definition for Versatec programs
 * Modify it to adapt to your Versatec printer
 *
 */

#define MAXROWS 2000              /* maximum number of rows into a bitmap  */

#define DEFWDPOINTSMM 7.8740157   /* Width resolution points/mm            */
#define DEFLNPOINTSMM 7.8740157   /* Length resolution points/mm           */

#define DEFWDTOTPOINTS 2112       /* Number of pixels per row. It must be  */
				  /* changed to 1536 if you have a smaller */
                                  /* V-80 printer                          */

#define VERSATECBUFSIZE 132       /* Versatec device buffer size           */
                                  /* DEFWDTOTPOINTS/8 must be a multiple   */
                                  /* of this number                        */

#ifdef VMS
#define VERSATECDEV "_LVA0:"      /* Versatec device name                  */
#define IO$M_PLOT 0x40            /* IO modifier for plot on Versatec V-80 */
#define IO$M_REMOTE 0x80          /* IO modifer to send remote command     */
#else
#define VERSATECDEV "/dev/vpc0"
#include <aglvcmd.h>              /* Header file with command definition   */
                                  /* for Versatec printers                 */
#endif
