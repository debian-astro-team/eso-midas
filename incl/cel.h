/* @(#)cel.h	19.1 (ESO-IPG) 02/25/03 13:49:27 */

/*  include file for WCSLIB of Mark Calabretta (mcalabre@atnf.csiro.au) */

#ifndef CEL
#define CEL

#include "proj.h"

static int  npcode = 25;
static char pcodes[25][4] =
      {"AZP", "TAN", "SIN", "STG", "ARC", "ZPN", "ZEA", "AIR", "CYP", "CAR",
       "MER", "CEA", "COP", "COD", "COE", "COO", "BON", "PCO", "GLS", "PAR",
       "AIT", "MOL", "CSC", "QSC", "TSC"};

struct celprm {
   int flag;
   double ref[4];
   double euler[5];

#if __STDC__
   int (*prjfwd)(double, double, struct prjprm *, double *, double *);
   int (*prjrev)(double, double, struct prjprm *, double *, double *);
#else
   int (*prjfwd)();
   int (*prjrev)();
#endif
};

#if __STDC__
   int celset(char *, struct celprm *, struct prjprm *);
   int celfwd(char *, double, double,
              struct celprm *,
              double *, double *,
              struct prjprm *,
              double *, double *);
   int celrev(char *, double, double,
              struct prjprm *,
              double *, double *,
              struct celprm *,
              double *, double *);
#else /* not __STDC__ */
   int celset(), celfwd(), celrev();
#endif /* __STDC__ */

#define CELSET 137

#endif /* CEL */
