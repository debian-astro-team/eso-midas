/* @(#)tfmdef.h	19.1 (ESO-IPG) 02/25/03 13:49:43 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE        C
.IDENTIFICATION  header file TFMDEF.H
.AUTHOR         J.D.Ponz
.KEYWORDS       Table format buffers
.ENVIRONMENT    Independent
.VERSION        3.0  1 Feb 1987 : Conversion into C
------------------------------------------------------------------------*/

#define    FMT_NBUF   32     /* maximum number of columns to be formatted */

struct TBL_FMT
  {
  char LABEL[TBL_LABLEN];       /* column label */
  char UNIT[TBL_UNILEN];        /* column unit  */
  char TYPE[TBL_TYPLEN]; 	/* column type  */
  char FORMAT[TBL_FORLEN+1];    /* column format */
  long colno;          		/* Table Column number */
  long FIRST;      		/* first column in the field */
  long LAST;          		/* last  column in the field */
  };

