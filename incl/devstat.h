/* @(#)devstat.h	19.1 (ESO-IPG) 02/25/03 13:49:28 */
/*++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      devstat.h
.AUTHOR              Francois Ochsenbin
.KEYWORDS            Device status
.ENVIRONMENT         VMS / UNIX
.COMMENTS            Here are defined structures / constants
			related to special devices (osu routines)
.VERSION 1.0	10-Jan-1990  Creation.
--------------------------------------------------------------------------*/

#ifndef  DEVSTAT_DEF 
#define  DEVSTAT_DEF      0    /* Indicates inclusion of this file */

struct devstat {			/* Status of a device		*/
	char 	*name;			/* PHYSICAL Device Name		*/
	char 	*klass;			/* 	    Device Class Name	*/
	int 	density;		/* Density (Tapes), sector size	*/
	int 	sectorsize;		/* Size of one sector		*/
	long 	usize;			/* bytes of file / total blocks	*/
	int 	blocksize;		/* Size of one block		*/
	int 	file_no;		/* number of file (for MT)	*/
	long 	block_no;		/* block position within file	*/
	int 	block_max;		/* Maximum blocksize (tape)	*/
	int 	block_min;		/* Minimum blocksize (tape)	*/
	long 	feet;			/* used tape length in feet/1000*/
	int 	tape_marks;		/* Number of CONSECUTIVE TM 	*/
  };
  
#endif
