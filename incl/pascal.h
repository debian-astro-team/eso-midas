/* @(#)pascal.h	19.1 (ESO-IPG) 02/25/03 13:49:38 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		pascal.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO], Alan Richmond [ST-ECF].
.CATEGORY	C "Pascalisation"
.COMMENTS	Includes definitions of typedef's and
		symbols which allow to use C almost like Pascal.
		Not anybody does like this...
.VERSION 1.0   Extracted from stesodef.h
----------------*/

#ifndef	PASCAL_DEF
#define PASCAL_DEF	0

/*===========================================================================
 *                        Variable declarations
 *===========================================================================*/
                           
typedef double			DOUBLE,	*DOUBLE_PTR	;
typedef float			FLOAT, 	*FLOAT_PTR	; 
typedef long int 		LONG,	*LONG_PTR	; 
typedef int			INT,	*INT_PTR	;
typedef unsigned int 		UINT,	*UINT_PTR	; 
typedef short int		SHORT,	*SHORT_PTR	;
typedef unsigned short int 	USHORT,	*USHORT_PTR	;
typedef char			BYTE,	*BYTE_PTR	;
typedef unsigned char		UBYTE, 	*UBYTE_PTR	;
typedef int			BOOLEAN,*BOOLEAN_PTR	;
typedef char				*STRING		;

/*===========================================================================
 *			Error Handling
 *===========================================================================*/

#include <error.h>

/*===========================================================================
 *			C "Pascalisation"
 *===========================================================================*/

#define	IF		if(  
#define THEN		){ 
#define ELSE		} else { 
#define ELSEIF		} else if (
#define ENDIF 		}

#define WHILE		while ( 
#define ENDWHILE	}

#define FOR 		for ( 
#define FOREVER		for ( ;; ) {
#define DO		){ 
#define ENDFOR		}

#define REPEAT		do {
#define UNTIL		} while ( NOT ( 
#define ENDREP		));

#define CASE		switch (
#define OF		){ 
#define ENDCASE		}

#define NOT		!
#define MODULO 		%
#define AND		&& 
#define OR 		||  
#define EQ		== 
#define NEQ		!=
#define LT		< 
#define LE		<=
#define GT		> 
#define GE		>=

#endif
