/* @(#)stackdef.h	19.1 (ESO-IPG) 02/25/03 13:49:41 */
/*
 
---------------------------- STACKDEF ------------------------------- 

layout of the structure for the stack needed for converting 
algebraic expressions to polish postfix notation
 

on file MID_DISK:[CMIDAS.NEW.MONIT]STACKDEF.H

K. Banse	version 1.10	870713

---------------------------------------------------------------------

*/

struct	STACK1
	{
	int		IA[40];
	int		PNTR;
	int		OVF;
	char		CA[40];
	};

struct	STACK1		STACK;
