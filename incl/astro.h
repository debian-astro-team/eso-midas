/* @(#)astro.h	19.1 (ESO-IPG) 02/25/03 13:49:26 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		astro.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Astronomic definitions
.COMMENTS	This module include astronomical declarations.
.ENVIRONMENT
.VERSION 1.0	02-Mar-1987: Creation
.VERSION 1.1	24-Oct-1988: New definitions, including editions
.VERSION 1.2	26-May-1989: Includes trigo

------------------------------------------------------------*/

#ifndef ASTRO_DEF

#define ASTRO_DEF	0

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

/*===========================================================================
 *                        Naming Conventions
 *===========================================================================*/
                           
/*+++++++++++++++
	
	a[]:  ASCII string
	e[3]: Unit Vector in Ecliptic Frame
	f4[3]: Fundamental (FK4, B1950) Unit Vector
	f5[3]: Fundamental (FK5, J2000) Unit Vector
	g[3]: Galactic Unit Vector
	G[3]: Supergalactic Unit Vector
	o[2]: Angles in degrees
	p[2]: projections
	P[2]: projections in Gnomonic
	q[2]: eQuatorial Angles in degrees
	u[3]: Unit vector in Any Frame
	v[3]: Velocity vector, in arc.sec/yr
			(multiply by distance/km_s_pc to get km/s)
	s[8]: Structure spos (u, udot, epoch, equinox)
	R[3][3]: Rotation matrix
	
------------------*/

struct spos {
	double u[3];	/* Position	*/
	double ud[3];	/* Derivative	*/
	double epoch;	/* Julian Years	*/
	double equinox;	/* Julian Years	*/
	char   frame;	/* Coordinate system	*/
	char   precision;
	};

/*===========================================================================
 *             Coordinate Systems 
 *===========================================================================*/

#define _COO_FK4_	1	/* FK4 system (equatorial)	*/
#define _COO_FK5_	5	/* FK5 system (equatorial)	*/
#define _COO_GAL_	2	/* Galactic coordinates		*/
#define _COO_SUPERGAL_	3	/* SuperGalactic coordinates	*/
#define _COO_ECL_	4	/* Ecliptic coordinates		*/

/*===========================================================================
 *             Projections definitions
 *===========================================================================*/

#define _PROJ_TAN_	1	/* Gnomonic projection*/
#define _PROJ_TAN2_	2	/* Stereographic projection*/
#define _PROJ_STG_	2	
#define _PROJ_SIN_	3	
#define _PROJ_SIN2_	4	/* Equal- area */
#define _PROJ_ARC_	5	/* For Schmidt plates	*/
#define _PROJ_SCHMIDT_	5	/* For Schmidt plates	*/
#define _PROJ_AITOFF_	6	/* Aitoff Projection	*/
#define _PROJ_AIT_	6	/* Aitoff Projection	*/
#define _PROJ_GLS_	7	/* Global Sinusoidal	*/
#define _PROJ_MERCATOR_	8
#define _PROJ_MER_	8	

/*===========================================================================
 *            Time Transformations (see also trtime.h)
 *===========================================================================*/

#include <trtime.h>

#define JD_offset	2400000.5e0	/* Origin of Modified Julian Date  */
#define JDtoMJD(x)	(x-JD_offset)
#define MJDtoJD(x)	(x+JD_offset)

#define BYr		365.242198781e0		  /* Days in Besselian Year */
#define JYr		365.25e0		  /* Days in Julian    Year */

#define JD_J2000	2451545.e0		  /* Julian date of J2000   */
#define JD_B1900	2415020.31352e0		  /* Julian date of B1900   */
#define JD_B1950	BYtoJD(1950.e0)		  /* Julian date of B1950   */
#define JD1970		2440587.5e0	/* JD   of 00:00:00 Jan. 1, 1970    */

#define JYtoJD(jy)	(JD_J2000+(jy-2000.e0)*JYr)  /* Julian Year to JD   */
#define BYtoJD(by)	(JD_B1900+(by-1900.e0)*BYr)  /* Besselian Year to JD*/

#define JDtoJY(jd)	(2000.e0+(jd-JD_J2000)/JYr)  /* JD to Julian Year   */
#define JDtoBY(jd)	(1900.e0+(jd-JD_B1900)/BYr)  /* JD to Besselian Year*/

#define BYtoJY(y)	JDtoJY(BYtoJD(y))	  /* Besselian to Julian yr */
#define JYtoBY(y)	JDtoBY(JYtoJD(y))	  /* Julian to Besselian yr */

#define TtoJD(T)	tmtojd(T)	/* Converts structure to JD	   */
#define ttoJD(t)	itojd(t)	/* Converts seconds to JD	   */
#define tr_JDt(j,t)	tr_jdi(j,t)	/* Converts JD to seconds	   */
#define tr_JDT(j,T)	tr_jdtm(j,T)	/* Converts JD to time structure   */

#if _TEMPLATES_
double 	itojd	(long t);
double 	tmtojd	(struct tm *T);
int 	tr_jdi	(double JD, long *t);
int 	tr_jdtm	(double JD, struct tm *T);
#else
double itojd(), tmtojd();
#endif

/*===========================================================================
 *             Basic Coordinates Transformations
 *===========================================================================*/
/*	(All return OK / NOK)	*/

#include <trigo.h>	/* Basic tr_ou / tr_uo in trigo.h */

#if _TEMPLATES_
int 	tr_oo 	(double o_in[2] , double o_out[2], double R[3][3]);
int 	tr_oo1	(double o_in[2] , double o_out[2], double R[3][3]);
int 	tr_oR 	(double o[2] , double R[3][3] );
int 	tr_uR	(double u[3] , double R[3][3] );
int 	tr_uu	(double u_in[3] , double u_out[3] , double R[3][3] );
int 	tr_uu1	(double u_in[3] , double u_out[3] , double R[3][3] );
int 	tr_qvs	(double q[2] , double v[3] , struct spos *s);
int 	tr_sqv	(struct spos *s , double q[2] , double v[3]);
int 	tr_RR	(double r2[3][3] , double out[3][3], double r1[3][3] ); /* out=r1*r2*/
#endif

#define tr_qq	tr_oo
#define tr_qq1	tr_oo1
#define tr_qR	tr_oR
#define tr_qu	tr_ou
#define tr_uq	tr_uo

/*===========================================================================
 * 	            Projections
 *===========================================================================*/
/*	(All return OK / NOK)	*/

#if _TEMPLATES_
int 	tr_op 	(int proj_mode, double o[2], double p[2]);
int 	tr_po 	(int proj_mode, double p[2], double o[2]);
int 	tr_up 	(int proj_mode, double u[3], double p[2]);
int 	tr_pu 	(int proj_mode, double p[2], double u[3]);
#endif

	/* Standard projection P = Gnomonic */
	
#define tr_oP(o,P)	tr_op(_PROJ_TAN_, o, P)
#define tr_Po(P,o)	tr_po(_PROJ_TAN_, P, o)
#define tr_uP(u,P)	tr_up(_PROJ_TAN_, u, P)
#define tr_Pu(P,u)	tr_pu(_PROJ_TAN_, P, u)

#define tr_pq		tr_po
#define tr_qp		tr_op
#define tr_Pq		tr_Po
#define tr_qP		tr_oP

/*===========================================================================
 *             Proper Motions Transformations
 *===========================================================================*/

#define km_s_pc			0.210945902e0	/* km/s/pc to "/yr	*/

#define RVtoV0(Vr,plx)		((Vr)*(plx)*(km_s_pc))			\
				/* Radial Velocity (km/s) to velocity v[0] */
			
/*===========================================================================
 *             Coordinate Transformations
 *===========================================================================*/

#if 0
/************ Note: The Global symbols may be used:**********************/
MID_EXTERN	double gal_2000[3][3];		/* J2000 to Galactic	*/
MID_EXTERN	double gal_1950[3][3];		/* B1950 to Galactic	*/
MID_EXTERN	double ecl_2000[3][3];		/* J2000 to J2000 Ecliptic*/
MID_EXTERN	double supergal[3][3];		/* Galactic to Supergalactic*/
/************************************************************************/
#endif

#if _TEMPLATES_
		/* ========= Galactic ==============*/

int tr_gf5	(double g[3], double f[3]); /* Galactic to J2000          */
int tr_f5g	(double f[3], double g[3]); /* Galactic to J2000          */
int tr_gf4	(double g[3], double f[3]); /* Galactic to B1950	  */
int tr_f4g	(double f[3], double g[3]); /* Galactic to B1950          */
int tr_gsup	(double g[3], double G[3]); /* Galactic to SuperGalactic  */
int tr_supg	(double G[3], double g[3]); /* SuperGalactic to Galactic  */

		/* ========= Ecliptic ==============*/

int ecl_R	(double R[3][3], double yr);
					/* Julian Year to Ecliptic Matrix */
int tr_ef 	(double e[3] , double f[3] , double y)  ;
int tr_fe 	(double f[3] , double e[3] , double y)  ;

		/* ========= Precession, Old =======*/
		
int preb_R	(double R[3][3], double yr0, double yr1);
int preb_q	(double q0[2], double q1[2], double yr0, double yr1);
int preb_u	(double u0[3], double u1[3], double yr0, double yr1);

		/* ========= Precession, New =======*/
		
int prej_R	(double R[3][3], double yr0, double yr1);
int prej_q	(double q0[2], double q1[2], double yr0, double yr1);
int prej_u	(double u0[3], double u1[3], double yr0, double yr1);

		/* ========= FK4 / FK5 change ======*/
		
int fk4_5q 	(double qb[2], double vb[3], double qj[2], double vj[2]);
int fk4_qv0 	(double qb[2], double vb[3]);
int fk4_5s 	(struct spos *sb, struct spos *sj);
int fk4_s0 	(struct spos *sb);
int fk5_4q 	(double qj[2], double vj[3], double qb[2], double vb[2]);
int fk5_4s 	(struct spos *sj, struct spos *sb);

		/* ========= General Change ========*/
		
int tr_ss	(struct spos *s_in, struct spos *s_out);

#endif


#define	tr_gG(g,G)			tr_gsup(g,G)			\
					/* Galactic to SuperGalactic */
#define	tr_Gg(G,g)			tr_supg(G,g)			\
					/* SuperGalactic to Galactic */

/*=========================================================================
		Coordinates Editions
 *=========================================================================*/

#if _TEMPLATES_
int	ed_o1 (char *buf, double g[2], int precision, int coo);
int	ed_o  (char *buf, double g[2], int precision);
int	ed_q1 (char *buf, double q[2], int precision, int coo);
int	ed_q  (char *buf, double q[2], int precision);
int	tr_ao0(char *str, int len, double *x);
int	tr_ao (char *str, int len, double x[2]);
int	tr_aq (char *str, int len, double x[2]);
#endif

#endif
