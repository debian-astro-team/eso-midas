/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 2009 European Southern Observatory
.IDENTifer   proto_idi.h
.AUTHOR      K. Banse	ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for IDI interface.
.VERSION     
 090702		creation

 090706		last modif
------------------------------------------------------------*/

#ifndef PROTO_II
#define PROTO_II


#ifdef  __cplusplus
extern "C" {
#endif



/* module iic.c */

extern int IICSCV_C(int display, int curn, int vis);

extern int IICRCP_C(int display, int inmemid, int curn, int *xcur, int *ycur,
                    int *outmemid);

extern int IICWCP_C(int display, int memid, int curn, int xcur, int ycur);

extern int IICINC_C(int display, int memid, int curn, int cursh, 
                    int curcol, int xcur, int ycur);


/* module iid1.c */

extern void IIDINIT(void);

extern int IIDOPN_C(char display[], int *displayid);


/* module iid2.c */

extern int IIDCLO_C(int display);

extern int IIDRST_C(int display);

extern void waste_disp(int dspno);

extern int IIDQDV_C(int display, int *nconf, int *xdev, int *ydev, 
                    int *depthdev, int *maxlutn, int *maxittn, int *maxcurn);

extern int IIDQCI_C(int display, int devcap, int size, int capdata[],
                    int *ncap);

extern int IIDQCR_C(int display, int devcap, int size, float capdata[],
	            int *ncap);

extern int IIDQDC_C(int display, int confn, int memtyp, int maxmem, 
                    int *confmode, int mlist[], int mxsize[], int mysize[],
                    int mdepth[], int ittlen[], int *nmem);

extern int IIDSDP_C(int display, int memlist[], int nmem, int lutflag[],
                    int ittflag[]);

extern int IIDSSS_C(int display, int memid[], int xoff[], int yoff[], 
                    int splitf, int splitx, int splity);

extern int IIDSNP_C(int display, int colmode, int npixel, int xoff, int yoff,
                    int depth, int packf, unsigned char *cdata);

extern int IIDICO_C (int display,int flag);

extern int IIDDEL_C(char display[], int *nodels, int *imindx, int *grindx);


/* module iie.c */

extern int IIEGDB_C(int display, int flag, int auxid, 
		    char *cbuf, int *ibuf, float*rbuf);

extern int IIESDB_C(int display, int flag, int auxid, 
		    char *cbuf, int *ibuf, float*rbuf);


/* module iig.c */

extern int IIGTXT_C(int display, int memid, char txt[], int x0, int y0,
                    int path, int orient, int color, int txtsize);

extern int IIGPLY_C(int display, int memid, int *x, int *y, int np,
                    int color, int style);

extern int IIGCPY_C (int display, int memid, int append);



/* module iii.c */

extern int IIISTI_C(int display);

extern int IIIEIW_C(int display , int trgstatus[10]);

extern int IIIGLE_C(int display , int evalno , int * cbuf);

extern int IIIGCE_C(int display , int evalno , char * cbuf);

extern int IIIGSE_C(int display , int evalno , char * cbuf , int * lcbuf);

extern int IIIGLD_C(int display , int locn , int * xdis , int * ydis);

extern int IIIENI_C(int display , int intype , int intid , int objtype , int objid , int oper , int trigger);


/* module iil.c */

extern int IILRIT_C(int display , int memid , int ittn , int ittstart , int ittlen , float ittdata[]);

extern int IILWLT_C(int display , int lutn , int lutstart , int lutlen , float lutdata[]);

extern int IILRLT_C(int display , int lutn , int lutstart , int lutlen , float lutdata[]);

extern int IILSBV_C(int display , int memid , int vis);

extern int IILWIT_C(int display , int memid , int ittn , int ittstart , int ittlen , float ittdata[]);


/*
 * module iim.c
 */

extern int IIMWMY_C(int display , int memid , unsigned char * data , int npixel , int depth , int packf , int x0 , int y0);

extern int IIMRMY_C(int display , int memid , int npixel , int x0 , int y0 , int depth , int packf , int ittf , unsigned char * data);

extern int IIMSMV_C(int display , int memlist[] , int nmem , int vis);

extern int IIMCMY_C(int display , int memlist[] , int nmem , int bck);

extern int IIMSLT_C(int display , int memid , int lutn , int ittn);

extern int IIMBLM_C(int display , int memlst[] , int nmem , float period[]);

extern int IIMCPY_C(int displaya , int memida , int * offseta , int displayb , int memidb , int * offsetb , int * xysize , int zoom);

extern int IIMCPV_C(int displaya , int memida , int * offseta , int displayb , int memidb , int * offsetb , int * xysize , int zoom);

extern int IIMSTW_C(int display , int memid , int loaddir , int xwdim , int ywdim , int depth , int xwoff , int ywoff);


/*
 * module iir.c
 */

extern int IIRSRV_C(int display , int roiid , int vis);

extern int IIRRRI_C(int display , int inmemid , int roiid , int * roixmin , int * roiymin , int * roixmax , int * roiymax , int * outmemid);

extern int IIRWRI_C(int display , int memid , int roiid , int roixmin , int roiymin , int roixmax , int roiymax);

extern int IICINR_C(int display , int memid , int roicol , int roixcen , int roiycen , int radiusi , int radiusm , int radiuso , int * roiid);

extern int IICRRI_C(int display , int inmemid , int roiid , int * roixcen , int * roiycen , int * radiusi , int * radiusm , int * radiuso , int * outmemid);

extern int IICWRI_C(int display , int memid , int roiid , int roixcen , int roiycen , int radiusi , int radiusm , int radiuso);

extern int IIRINR_C(int display , int memid , int roicol , int roixmin , int roiymin , int roixmax , int roiymax , int * roiid);


/* module iiz.c */

extern int IIZWZM_C(int display, int memlist[], int nmem, int zoom);

extern int IIZRSZ_C(int display, int memid, int *xscr, int *yscr, int *zoom);

extern int IIZWZP_C(int display, int xscr, int yscr, int zoom);

extern int IIZRZP_C(int display, int *xscr, int *yscr, int *zoom);

extern int IIZWSC_C(int display, int memlist[], int nmem, int xscr, int yscr);

extern int IIZWSZ_C (int display, int memid, int xscr, int yscr, int zoom);




/* module xwimg.c */

extern int XWIMG(int ldspno, int limch, char *frame, int *khelp, int loaddir,
		 int *npix, int *icen, float *cuts, int *scale);



/* module idiclt.c */

extern int IIDSEL_C(int ldspno, int confn);

extern int IIXWIM_C(int ldspno, int limch, char *frame, int *khelp, 
		     int loaddir, int *npix, int *icen, float *cuts, 
		     int *scale);


#ifdef  __cplusplus
}
#endif


#endif

