/* @(#)fsydef.h	19.1 (ESO-IPG) 02/25/03 13:49:31 */
/*

------------------------------ FSYDEF ------------------------------ 
 
definitions of symbols used in the syntax of the file management system
also default file types are specified here

*/	

 

#if vms

#define FSY_DIREND ']'
#define FSY_DISKEND ':'
#define FSY_TESTA ';'

#else

#define FSY_DIREND '/'
#define FSY_DISKEND '/'
#define FSY_TESTA ' '

#endif



#define FSY_TYPMARK '.'

static char  *FSY_DEFPNTR[] = {".bdf", ".any", ".tbl",
                                ".fit", ".log", ".prg",
                                ".cat", ".tst", ".exe",
                                ".BDF", ".ANY", ".TBL",
                                ".FIT", ".LOG", ".PRG",
                                ".CAT", ".TST", ".EXE"};

 

/*

FSY_TYPMARK	char. used to separate file-name from file-type
 
FSY_DIREND	char. for end-marker of file-directory
 
FSY_DISKEND	char. for end-marker of disk device
 
FSY_TESTA 	char. needed in reading file directories
 

	K. Banse	version 1.55	910605
	
-----------------------------------------------------------------------


*/
