/* @(#)aglunix.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:25   */
/*
 * HEADER : aglunix.h      - Vers 3.6.000  - Dec 1991 -  L. Fini, OAA
 *
 *
 * This file contains standard includes needed by AGL, common
 * to any Unix system
 *
 */


                          /* Define parameters for fopen()                   */
#define B_CREATE "w"                           /* Create binary file         */
#define B_APPEND "a"                           /* Append to binary file      */
#define B_READ   "r"                           /* Open for read binary file  */
#define T_CREATE "w"                           /* Create text file           */
#define T_APPEND "a"                           /* Append to text file        */
#define T_READ   "r"                           /* Open for read text file    */


#define SYSTEM system

                          /* Unix I/O routines emulation (for drivers)       */
#define AGL_fread  fread
#define AGL_fwrite fwrite
#define AGL_fclose fclose
#define AGL_fflush fflush

                             /* These macros must work on any character case */

#define TOUPPER(x) (islower(x) ? toupper(x) : (x))
#define TOLOWER(x) (isupper(x) ? tolower(x) : (x))

#define REMOVE unlink


/*
 * Now define some AGL related parameters
 *
 *----------------------------------------------------------------------------
 * The following parameters are not strictly system dependent but they may be
 * different for different system for optimization purposes
 *
 */

#define MAXVWPTS 10        /* Number of viewports concurently active         */
#define MAXACTIVE 5        /* Number of different devices concurrently active*/
#define NDRIVERS 20        /* Max number of device drivers                   */
/*---------------------------------------------------------------------------*/


#define MAXTXTLEN 133      /* Max length of a text string                    */
#define PHNAMLNG 513       /* Max suitable file name length                  */
#define DRVNAMLNG 21       /* Max driver file name length + 1                */
#define DEVCMDLNG 513      /* Max device close command length + 1            */

#define DEVGLOBAL "AGL3DEV"


#define AGLSTDIR "AGL3CONFIG" /* Environment var with AGL standard directory */

