/*
 
---------------------------- MONITDEF ------------------------------- 

layout of the different structures which are used inside the MIDAS monitor
 
K. Banse	ESO - Garching, DMD

090320		last modif

---------------------------------------------------------------------

*/

#define MAX_LINE 400		/* sync with MID_MAX_LINE in xconndef.h !! */
#define MAX_TOKEN 240		/* sync with syskeys.datorg, Pi keywords */



struct	TOKEN_STRUCT
	{
	char		STR[MAX_TOKEN+2];
	int		LEN;
        };

struct	TOKEN_STRUCT	TOKEN[10];


struct  LINE_STRUCT
        {
        char            STR[MAX_LINE];
        int             LEN;
        };

struct  LINE_STRUCT     LINE;


struct  FRONT_STRUCT
        {
	int		PID;
	int		COMCNT;
        int             PEND;
        int             INTERM;
        int             PLAYBACK;
        int             PP;
        char            PROMPT[24];
        char            TITLE[16];
        char            DAZUNIT[2];
        char		ENV;
	char		SYSTEM[20];
	char		STARTUP[160];
        };

struct  FRONT_STRUCT    FRONT;


struct  server_STRUCT
        {
        int             SLEEP;
        int             MODE;
        int		FUNC;
        char		ECKO;
        };

struct  server_STRUCT    server;



struct  ERROR_STRUCT
        {
        int        SYS;
        int        STATUS;
        int        OFFSET;
        int        INDEX;
        };

struct  ERROR_STRUCT    ERRORS;



struct	KAUX_STRUCT
	{
	char		OUT[MAX_TOKEN];
	char		STR[MAX_LINE];
        int 		IVAL;
        float 		RVAL;
        double 		DVAL;
        size_t 		SVAL;
#if vms
	unsigned char	DCLINP;		/* only needed for VMS */
#endif
	};

struct	KAUX_STRUCT	KAUX;


#include <proto_monit.h>



/*


structure LINE:
---------------

STR		the MIDAS command line
LEN             corresponding length



structure TOKEN[10]:
--------------------

STR[122]        different tokens of a command line ( including \0 )
LEN             corresponding lengths of tokens



structure FRONT :
-----------------

PID		PID of Monitor
COMCNT          MIDAS command counter
PEND		size of MIDAS prompt string
INTERM		mail interrupt flag
PLAYBACK	flag to indicate playback session
PP		fid of playback file
PROMPT[24]      prompt string for MIDAS monitor
TITLE[16]	title line appearing when command buffer is displayed
DAZUNIT[2]      user working station A0,A1,...
ENV		env_prompt_char, > or $ to indicate where we are...
		set to '*' if Unix_command_line_input
SYSTEM[20]	computer system, MIDAS is installed on
STARTUP[160]	Midas startup directory



structure server:
-----------------

                this structure used when running as background MIDAS

SLEEP           no. of seconds to sleep
MODE            = 0/2 for fore/background mode
FUNC            the function code obtained in a ServRead call
ECKO            to echo or not the commands which are sent



structure ERRORS:
------------------

SYS		Monitor error
STATUS          low level return status (e.g. from SC or TC calls)
OFFSET          offset to add to `^^^^^^^^^' string in PREPERR
INDEX		index of TOKEN which causes the error



structure KAUX:
--------------

OUT[MAX_TOKEN]	just a place for output messages
STR		for copies of LINE.STR
IVAL		global int variable
RVAL		global float variable
DVAL		global double variable

DCLINP		= 1, if we have to reassign SYS$INPUT after a DCL command

	
*/
