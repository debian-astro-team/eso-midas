/* @(#)aglerror.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:23   */
/*
 * HEADER : aglerror.h     - Vers 3.6.001  - Jul 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Dec 1991 -  L. Fini, OAA
 */


/*****************************************************************************/
/*++                                                                         */

/*                          AGL ERROR CODES                                  */


/*        NOTE: codes in the range   1..99  are INFORMATION                  */
/*              codes in the range 101..199 are WARNINGS                     */
/*              codes in the range 201..299 are ERRORS                       */
/*              codes in the range 301..399 are SEVERE ERRORS                */

#define INFOTRSH     0   /* Information threshold                            */
#define WARNTRSH     100 /* Warning threshold                                */
#define ERRTRSH      200 /* Error threshold                                  */
#define SEVTRSH      300 /* Severe error threshold                           */
#define MAXERRCD     399 /* Max error code from AGL                          */

/*       SYMB.NAME   CODE   DESCRIPTION                                      */

#define AGLNOERR     (-1)/* No error                                         */

                         /*++                                                */
#define UNSFEATINF     2 /* Unsupported feature (information).               */
                         /* Requested operation cannot be performed because  */
                         /* the required optional feature is not supported   */
                         /* by the device driver.                            */
#define CLPOUTINF      3 /* Coordinate out of current clipping area.         */
                         /* The result of a coordinate conversion yields a   */
                         /* point which lies outside the clipping area.      */
#define NOPIXINF       4 /* Pixel value in locator not available.            */
                         /* Returning of pixel value corresponding to given  */
                         /* locator position is an optional feature and may  */
                         /* not be supported by some device drivers.         */
#define TOOMANYINF     5 /* Too many parameters.                             */
                         /* Too many parameters have been specified.         */
                         /* See the called routine documentation to know the */
                         /* maximum number of parameters allowed.            */

#define ILLBOUWNG    101 /* Illegal bounds requsted to routine.              */
                         /* Viewport bounds must be in the range [0.0 1.0],  */
                         /* upper bound on any axis must be greather than    */
                         /* the corresponding lower bound. Clipping area     */
                         /* bounds must be within the viewport bounds.       */
#define CMDBOVFWNG   102 /* Device close command too long for AGL buffer.    */
                         /* Command associated to the device description in  */
                         /* the configuration file agldevs.dat is too long.  */
#define ILLCLRWNG    103 /* Error in color/pen request.                      */
                         /* Required color or pen is not available for the   */
                         /* given device, or the color specification is not  */
                         /* legal. The color is not changed.                 */
#define DEVCMDWNG    104 /* Close command execute error.                     */
                         /* Execution of the device close command associated */
                         /* with the device type in the file agldevs.dat     */
                         /* results into an error condition.                 */
#define STRCLPWNG    105 /* Text string clipped.                             */
                         /* The text string passed to the module is too long,*/
                         /* It is truncated to the maximum allowed length.   */
#define LOCCLPWNG    106 /* Locating device outside viewport.                */
                         /* The position of the locator lies outside the     */
                         /* current viewport.                                */
#define ERRFILWNG    107 /* Error opening error log file.                    */
                         /* The requested redirection of AGL run time errors */
                         /* to the specified file is not performed.          */
#define ILLCODWNG    108 /* Illegal request code.                            */
                         /* Check AGL User's Guide for a list of currently   */
                         /* currently defined request codes for the called   */
                         /* module.                                          */
#define ILLMRKWNG    109 /* Illegal marker specifier.                        */
                         /* Marker symbol specification is outside the cur-  */
                         /* rently defined range. See AG_GPLM documentation  */
                         /* in the AGL User's Guide.                         */
#define SETSYNWNG    110 /* AG_SSET string syntax error.                     */
                         /* Check the string passed to AG_SSET for syntax    */
                         /* errors.                                          */
#define MFEOFWNG     111 /* Unexpected end of file on input metafile         */
                         /* The metafile was probably improperly closed.     */
#define VECLNGWNG    112 /* Illegal vector length in AG_GPLx.                */
                         /* Zero or negative vector length is illegal for    */
                         /* AG_GPLM, a length less than or equal to 1 is     */
                         /* illegal for AG_GPLL.                             */
#define MFNOTWNG     113 /* No Metafile is currently active.                 */
                         /* A metafile must be currently active to allow the */
                         /* requested operation.                             */
#define MFACTVWNG    114 /* Metafile is already active.                      */
                         /* An attempt to open or activate a metafile when   */
                         /* another metafile is already active has been made.*/
#define STRSTXWNG    115 /* Illegal string syntax.                           */
                         /* Usually due to misuse of metacharacter sequences */
                         /* or to unexpected end of string (i.e. untermina-  */
                         /* ted metacharacter sequence).                     */
#define ILLPOSWNG    116 /* Illegal TEXT position specifier.                 */
                         /* Check AGL User's Guide for legal values of the   */
                         /* text string relative position specifier.         */
#define ASPECTWNG    117 /* Illegal aspect ratio request.                    */
                         /* The requested aspect ratio is well beyond any    */
                         /* reasonable value.                                */
#define ILLFONTWNG   118 /* Illegal font name or number.                     */
                         /* The font specification is illegal or the font    */
                         /* file cannot be read. Default font is selected.   */
#define UNSFEATWNG   119 /* Unsupported feature (warning).                   */
                         /* Requested operation cannot be performed because  */
                         /* the required optional feature is not supported   */
                         /* by the device driver.                            */
#define VWPOUTWNG    120 /* Coordinate outside the current viewport.         */
                         /* Coordinate values resulting from the operation   */
                         /* are outside the currently defined viewport       */
#define ILLSETWNG    121 /* Illegal value for parameter                      */
                         /* An illegal value was specified for the parameter */
                         /* to be set.                                       */
#define TOOMANYWNG   122 /* Too many parameters.                             */
                         /* Too many parameters have been specified!         */
                         /* See the called routine documentation to know the */
                         /* maximum number of parameters allowed.            */

#define POSCNVERR    202 /* Coordinate conversion error.                     */
                         /* Coordinate conversion cannot be performed due to */
                         /* computation errors (e.g.: log(negative)).        */
#define NOWNDERR     203 /* Window is undefined (ERROR).                     */
                         /* The requested operation cannot be performed if   */
                         /* the user window has not been previously defined  */
                         /* (see AG_WDEF).                                   */
#define ILLWNDERR    204 /* Illegal window bounds.                           */
                         /* Requested user window bounds cannot be set (e.g: */
                         /* negative values with log scaling in effect).     */
#define NOVWPERR     205 /* No viewport is currently active.                 */
                         /* The requested operation cannot be performed if   */
                         /* a viewport has not been previously opened (see   */
                         /* AG_VDEF).                                        */
#define UNSFEATERR   206 /* Unsupported feature (error).                     */
                         /* Requested operation cannot be performed because  */
                         /* the required optional feature is not supported   */
                         /* by the device driver.                            */
#define ILLDEVERR    207 /* Illegal device specifier.                        */
                         /* The requested device specifier is not known to   */
                         /* AGL. Check for possible misspelling in device    */
                         /* name, wrong definition either into agldevs.dat   */
                         /* files or into environment variables. You must    */
                         /* understand AGL device naming conventions (refer  */
                         /* to AGL User's Guide and to AGL Installation and  */
                         /* Customization Guide).                            */
#define NOLOGERR     208 /* Log.transf. not applicable on current window.    */
                         /* Logarithmic transformation cannot be applied to  */
                         /* currently defined user window (e.g: because of   */
                         /* zero or negative bounds).                        */
#define NOARGERR     209 /* Required argument not specified in the call.     */
                         /* Some device drivers will return this error       */
#define VWPSELERR    210 /* Illegal viewport identifier.                     */
                         /* The requested viewport has not been previously   */
                         /* defined.                                         */
#define BUFOVFERR    211 /* Escape command buffer overflow.                  */
                         /* Device specific command is longer than related   */
                         /* buffer. See AG_ESC in the AGL User's Guide for   */
                         /* max allowed length.                              */
#define ILLCLPERR    212 /* Illegal clipping area definition.                */
                         /* Clipping area bounds must be contained within the*/
                         /* viewport, lower bound must be less than upper    */
                         /* bound.                                           */
#define MFILLCDERR   214 /* Illegal metafile code.                           */
                         /* Either the metafile is corrupted, or it may have */
                         /* been generated by older versions of AGL.         */
#define MFOPENERR    215 /* Error opening metafile.                          */
                         /* Check the file name, reading rights on the de-   */
                         /* vice, etc.                                       */
#define MFCLOSERR    216 /* Error closing metafile.                          */
                         /* Close error is usually due to O.S. problems.     */
#define MFWRITERR    217 /* Error writing onto metafile.                     */
                         /* Check for device full, device failure, etc.      */
#define MFREADERR    218 /* Error reading from input metafile.               */
                         /* Read errors are usually due to O.S. problems.    */
#define MFFMTERR     219 /* Specified file is not a standard AGL metafile.   */
                         /* Standard AGL metafile header has not been found  */
                         /* at the beginning of the file: this is not an AGL */
                         /* metafile.                                        */
#define NODEVICE     220 /* No device specified at viewport definition.      */
                         /* The device specification string in the AG_VDEF   */
                         /* call doesn't contain any recognizable device na- */
                         /* me.                                              */
#define FNTNUMERR    221 /* Given font number is not defined.                */
                         /* Check AGL User's Guide for legal values of the   */
                         /* font specifier (see AG_SSET or AG_GTXT).         */
#define FNTFILERR    222 /* Error opening font file.                         */
                         /* This may be due to incorrect installation of     */
                         /* AGL. Refer to AGL Installation and Customization */
                         /* Guide. Also check read permission to the font    */
                         /* files on the standard AGL directory.             */
#define MEMORYERR    223 /* Error allocating memory (not enough space).      */
                         /* Many dynamic structures are allocated when need- */
                         /* ed. Your program is too big to allow the opera-  */
                         /* tion. You may try to kill unused viewports or to */
                         /* avoid to use character fonts other than the de-  */
                         /* fault.                                           */
#define FNTRDERR     224 /* Font file read error.                            */
                         /* Read errors are usually due to O.S. problems.    */
#define USRTRSERR    225 /* Error in user defined transformation.            */
                         /* The user defined coordinate transformation       */
                         /* returns with an error condition. The origin of   */
                         /* the error depends on the transformation currently*/
                         /* active.                                          */
#define MODEERR      226 /* Set mode error.                                  */
                         /* The selected graphic mode cannot be set, probably*/
                         /* because the SPECIAL mode was selected without    */
                         /* a previous call to AG_TRNS() for setting the     */
                         /* special user transformation                      */

#define RANGEERR     227 /* Range error.                                     */
                         /* Some argument passed to the module is outside    */
                         /* required range                                   */

#define ILLDRVSEV    301 /* Illegal driver version.                          */
                         /* The accessed driver has not version greater than */
                         /* 34. You shouldn't get this error code when using */
                         /* officially supportde AGL drivers. You might get  */
                         /* it, anyway, if you're using a locally developed  */
                         /* device driver.                                   */
#define DEVIOSEV     302 /* Unspecific I/O error on graphic device.          */
                         /* I/O error when reading from or writing to a de-  */
                         /* vice are usually due to O.S. problems.           */
#define DEVOPNSEV    303 /* Device open error.                               */
                         /* The device cannot be opened. Check for access    */
                         /* rights, then ask the system manager.             */
#define VWPOVFSEV    304 /* Too many viewports active.                       */
                         /* There is a system limitation on the number of    */
                         /* viewports which can be opened concurrently. Try  */
                         /* to kill unused ones (see AG_VKIL in AGL User's   */
                         /* guide).                                          */
#define UNUSED005    305 /* Currently unused code.                           */
                         /* You should not get this error message !!         */
                         /* Be sure you're using the correct AGL version.    */
#define CNFOPNSEV    306 /* Error opening agldevs.dat file                   */
                         /* Either AGL was not properly installed or you have*/
                         /* not read access to the file.                     */
#define CAPOPNSEV    307 /* Error opening caps file.                         */
                         /* Either AGL was not properly installed or you have*/
                         /* not read access to the file.                     */
#define CAPRDSEV     308 /* Error reading caps file.                         */
                         /* Read errors are usually due to O.S. problems,    */
                         /* such as access conflicts and the like, but check */
                         /* also for possible syntax errors within the file, */
                         /* if it is not provided in the standard AGL kit.   */
#define DEVOVFSEV    309 /* Too many devices concurrently active.            */
                         /* You may try to kill all viewports connected to   */
                         /* unused devices.                                  */
#define UNDDRVSEV    310 /* Device driver name cannot be found.              */
                         /* The required device driver is not installed. You */
                         /* must check for misspellings in the agldevs.dat   */
                         /* files. Then you may get a list of installed dri- */
                         /* vers (see AG_DRIV in AGL User's Guide). If the   */
                         /* required driver is supported you may follow di-  */
                         /* rections in AGL Installation and Customization   */
                         /* Guide to include it into your configuration.     */
#define CHGBUFOVSEV  311 /* Character generator polyline buffer overflow.    */
                         /* This problem must be reported to the author for  */
                         /* a check of the consistency of the character ge-  */
                         /* nerator code.                                    */
#define NOTIMPLSEV   312 /* Module not implemented yet.                      */
                         /* The called module has not yet been implemented.  */
#define ILLCHGSEV    313 /* Inconsistent definition of character.            */
                         /* The character generator is not well formed. If   */
                         /* Any of the loadable character fonts is in use    */
                         /* this may depend on a bad definition in the font  */
                         /* file (named *.nfn). Check for possible corrup-   */
                         /* tion of font files on the AGL standard directory */
                         /* (files *.nfn).                                   */

#define MEMALLOCSEV  314 /* Memory allocation error                          */
                         /* Somewhere in AGL a memory allocation request has */
                         /* failed. Maybe your program is too big!           */

                                                                         /*--*/ 

