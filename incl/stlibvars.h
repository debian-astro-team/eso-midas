/*

---------------------------- stlibvars.h -------------------------------


non-external definitions of all variables

K. Banse        940322

090318		last modif

---------------------------------------------------------------------

*/


struct  CAT_STRUCT      CATAL[CAT_MAXNO];


struct FCT_ALL           FCT;


struct FCB_STRUCT        FCB;


struct LDB_STRUCT       LDB;
struct LDB_STRUCT       *LDB_PNTR;      /* will point to LDB  */


struct KEY_ALL          KEYALL;

double      *KDWORDS;
int         *KIWORDS;
float       *KRWORDS;
char        *KCWORDS;
size_t      *KSWORDS;


struct YDIRDSC   YDSCDIR_ENTRY;
struct YDIRDSC   *YDSC_PNTR;		/* will point to YDSCDIR_ENTRY */


int     ERRO_CONT, ERRO_LOG, ERRO_DISP;

int     ERROS[ERRO_MAXENT][2];
int     ERRO_INDX, ERRO_OFF;

char    ERRO_MESAGS[ERRO_MAXLEN];

char    DATA_PATH[328];			/* holds 4 data paths of 80 chars. */

