/*++++++++++++++
.IDENTIFICATION ftoc_commc.h
.LANGUAGE       C
.AUTHOR         Klaus Banse
.ENVIRONMENT    
.KEYWORDS       
.VERSION  1.0   060322
 060323		last modif

---------------*/

#ifndef VMR_DEF
#define VMR_DEF		0	/* To avoid redefinitions */

		/* structure for COMMON in main Fortran programs  */
extern struct { int addr; } vmr ;

		/* translate address into FORTRAN index in VMR common */
#define COMMON_INDEXA(a)	(int *)a - (int *)&((&(vmr.addr))[-1])
#define COMMON_INDEXB(a)	(int *)&((&(vmr.addr))[-1]) - (int *)a 
#define COMMON_INDEX(a) (\
        ((int *)a>(int *)(&(vmr.addr)))?COMMON_INDEXA(a):COMMON_INDEXB(a))


#define IS_HIGH(a)	((int *)a>(int *)(&(vmr.addr)))?1:0

#endif
