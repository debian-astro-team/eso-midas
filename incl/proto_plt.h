/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993-2007 European Southern Observatory
.IDENTifer   proto_plt.h
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for Low & High level Plot Interfaces

.ENVIRONment none

.VERSION     1.0     27-Oct-1993   Creation by R.M. van Hees
 070810		last modif
------------------------------------------------------------*/

#ifndef  PROTO_PLT               /* Avoid redefinitions */
#define  PROTO_PLT       0

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef __STDC__
struct Odata *Data( void );
struct Pdata *Proj( void );

extern int  PLINIT( char *plckey, int *plikey, float *plrkey );
extern void PLCON ( float *p_img, float *image, float *area, double *step, 
		    int nlevl, float  *clevl, int *ctype );
extern void PLCONI( int plmode, char *name, char *ident, 
                    float *clevl, int *ctype, int nlevl );
extern void PLGRA ( float *p_img, float *image, float *area, double *step, 
                    float *glevl, int nlevl, int *options, float  greynes );
extern void WEDGE ( double *step, float *glevl, int nlevl, int *options, 
                    float  greynes );
extern void PLGRAI( int plmode, char *name, char *ident, int nlevl, 
		    float *glevl, int *options );
extern void PLERR ( int tid, int row, int *col, int *log, int loc, char *bar );
extern void PLGRD ( char *cinput, char *xy_opt );
extern void PLMODF( char *cpntr, float *image, int *npix, double *start, \
                    double *step, int ncur, int ndeg, int bin, int *go_on, 
                    int *ibar );
extern void PLPER ( float *wcfram, float *p_img, float *image, double *start, 
		    double *step, float *angle, float *scales, int *xyflag );
extern void PLFRM3( float *xwcfrm, float *ywcfrm, float *zwcfrm, char *labelx,
                    char *labely, char *labelz, float *angle, float scale );
extern void PLPERI( int plmode, char *name, char *ident, float *image,
		    float *angle, float *wcfram );
extern void PLVEC ( float *p_imgA, float *p_imgB, float *image, float *area, 
                    double *step, float scar, float *ranp, int head );
extern void PLVECI( int plmode, char *nameA, char *identA, char *nameB,
		    char *identB, float scar, float *ranp );
extern void PLIDEN( int plmode, char *lstrng, char *rstrng );
extern void PLUSER( char *text );
extern void PLDATI( float *xnorm, float *ynorm );
extern void PLLOGI( float *xt, float *yt );
extern void PLDESI( int plmode, char *name, char *ident, char *desc, 
                    int  *image );
extern void PLKEYI( int plmode, char *name, int  *image );
extern void PLBDFI( int plmode, char *name, char *ident, float *image );
extern void PLHFRI( int plmode, char *name, char *ident, 
		    int nbins, float binsiz );
extern void PLHTBI( int plmode, char *name, char *column, char *sel, 
                    int nbins, float binsiz );
extern void PLTBLI( int plmode, int tid, char *name, char *col[], 
                                         char *sel, int tbldim );
extern void SETKEY( int nc, int ni, int nr, int cnum, int inum, int rnum,
                    char *value, char *plckey, int *plikey, float *plrkey );
extern void BOXWTP( float *rbound, int npix, double start, double step, 
		    float *ibound );
extern void BOXPTW( float *ibound, int npix, double start, double step, 
		    float *rbound );
extern void SKIPNULL( int *nline, int *inull[2], float *inoutval[2], 
                      int *nconn, int *nnull);
extern void SSKIPNULL( int *nline, int *inull[2], float *inoutval[2], 
                      int *nconn, int *nnull, int *sele);
extern void LABSTR( char *string );
extern void MINMAX( float *array, int ndim, float *min, float *max );
extern void SORLEV( int n, float *clevl );
extern void SETWND( float *wind );
extern int  USRINP( char type, char *str, int mxval, char *cbuff, int *nrval );
extern void TBL_USRINP( char *str, int *type, char *column, 
                        int  *rownr, int *index, int  *items );
extern void GETBDF( char *cpntr, float *image, int *npix, double *start, 
		    double *step, float *xval, float *yval );
extern void GETCOL( int tid, int *col, int *depth, int *ilog, int nrrow,
		    int *outrow, float *rval[2] );
extern void GETDAT( int imf, int mxsize, int *npix, float *image, 
                    int ism, float *p_img );
extern int  GETDEV( char *kname, char *lname );
extern void GETFRM( char *frame, float *wcfram );
extern void GETPROJ( float *wcfram, float *angle, float *scales, 
                     int nval, struct Odata *data, struct Pdata *proj );
extern int  GETSIN( char *string, int nrpix, double start, double step );
extern void GETTIC( float *wcfram, float *atkstr, float *atkend );
extern void PCTSET( void );
extern void PCKRDC( char *cpar, int maxvals, int *actvals, char *value );
extern void PCKRDI( char *ipar, int maxvals, int *actvals, int *value );
extern void PCKRDR( char *rpar, int maxvals, int *actvals, float *value );
extern void PCKDEF( void );
extern void PCKWRC( char *cpar, char *value );
extern void PCKWRI( char *ipar, int nrval, int *ival );
extern void PCKWRR( char *fpar, int nrval, float *fval );
extern void PCOPEN( char *devnam, char *plname, int access, int *plmode );
extern void PCCLOS( void );
extern void PCFRAM( float *xwcfrm, float *ywcfrm, char *labelx, char *labely );
extern void PCAXES( float *xmnmx, float *ymnmx, 
		    char *labelx, char *labely, char *AGLopt );
extern void PCDATA( int stype, int ltype, int binmod, 
                    float *x_data, float *y_data, float y_off, int nrdata );
extern void PCHIST( int   nbins, float *xval, float *rfr, float *fopt );
extern void PCTEXT( char  *text, float xw, float yw, 
		    float angle, float chsiz, int ipos );
extern int  PCGCUR( float *xval, float *yval, int *key );
#endif

#ifdef  __cplusplus
}
#endif

#endif
