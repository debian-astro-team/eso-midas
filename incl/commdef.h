/* @(#)commdef.h	19.1 (ESO-DMD) 02/25/03 13:49:27 */
/*
	
-------------------------------- COMMDEF -----------------------------

layout of the structures for MIDAS commands + qualifiers


on file MID_INCLUDE:commdef.h

K. Banse	910507, 920123, 921216, 930226, 940707

ESO/IPG - Garching

110427		last modif

----------------------------------------------------------------------

*/
	
#define MAXSTA_COM  250			/* initial no. of commands  */


struct	COMND_STRUCT
	{
	char		STR[6];
	char		QDEF[4];
	short int	INDX;
	};
	

struct	QUALIF_STRUCT
	{
	char		STR[4];
	short int	OFF;
	short int	LAST;
	short int	NEXT;
	short int	DEFSET;
	short int	CTXNO;
	};
	


struct	COMND_ALL
	{
	int		CMAX;
	int		QMAX;
	int		ENDLIN;
	int		FIRST;
	int		STRL;
	int		INUSEC;
	int		INUSEQ;
	int		LPRIMC;
	int		LPRIMQ;
	int		FDEL;
	struct	COMND_STRUCT	*CP;
	struct	QUALIF_STRUCT	*QP;
	char			*LINE;
	};

struct	COMND_ALL	COMN;


/*	
	
struct COMND_STRUCT
----------------------

STR[6]		command name, 
                STR[0] = '\' indicates, that this entry is unused
DEFQUAL[4]	default qualifier for command
INDX		index (in array Q) of first qualifier of this command
                -1, if none
	

struct QUALIF_STRUCT
------------------------

STR[4]		qualifier name
                STR[0] = '\' indicates, that this entry is unused
OFF		offset into COMLINE of corresponding command string
		< 0,    for primitive commands + sequence no. 
		        (used for later fixed branching)
		>= 0,   for added commands 
		> 1000, for dynamic commands (offset will be adjusted)
NEXT		index (in array Q) of next qualifier of this command
                -1, if none
LAST		index (in array Q) of last qualifier of this command
                < 0, for "first" qualifier of a command and then index 
		into array C for related command
DEFSET		= 0 or 1 if defaults have been defined for this
                command/qualifier combination by the user
CTXNO		context no. this comnd/qual belongs to,
		-1 indicates at start up, 0 for individually added commands
		1, 2, ..., MAX_CONTXT for contexts


struct COMN:
------------

CMAX		limit for commands
QMAX		limit for qualifiers
ENDLIN		limit for command strings in COMN.LINE

FIRST		offset of first free space, in [0,ENDLIN]
STRL		length of last (command string + '\0')

INUSEC		last used command entry
INUSEQ		last used qualifier entry

LPRIMC		last command entry of primitive commands
LPRIMQ		last qualifier entry of primitive commands

FDEL		first index where search for deleted command lines should start

*CP		pointer to command structures
*QP		pointer to qualifier structures
	
*LINE		space for actual command lines
	
*/
