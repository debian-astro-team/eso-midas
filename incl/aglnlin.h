/* @(#)aglnlin.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:24   */
/*
 * HEADER : aglnlin.h      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 * Definitions for the AGL support of non linear projections
 */


#include <aglenvr.h>
#include <aglaxes.h>



struct NLN_STATUS {

int		Metamode;	/* Save current metafile mode               */
int		Fixit;		/* Fix clipping area dimensions flag        */
int		Bold;		/* =0 for normal, =10 for thick axes        */
int 		Windfix;	/* Window specified by user                 */

int 		Proj;		/* Projection type                          */

struct AXIS	c1;		/* First coordinate axis data               */
struct AXIS	c2;		/* Second coordinate axis data              */

float		Txtdim;		/* Global dimension multipl. for charact.   */

float		Clpa[4];	/* Current viewport dimensions              */
float		Boxa[4];	/* Box enclosing the full plot              */
float		Wind[4];	/* User coordinate extreems (as specified)  */

char		Title[MAXTITLE+5];	/* Plot title                       */
char		Rlabel[MAXTITLE+5];	/* Plot right label                 */
};

#define MIN3(x,y,z) ((x)<(y))?(((x)<(z))?(x):(z)):(((y)<(z))?(y):(z))
#define MAX3(x,y,z) ((x)>(y))?(((x)>(z))?(x):(z)):(((y)>(z))?(y):(z))

#define SUCCESS 0
#define FAILURE (-1)
