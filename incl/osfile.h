/*+++++++++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      osfile.h
.AUTHOR              Francois Ochsenbein [ESO-IPG]
.KEYWORDS            File Definitions
.ENVIRONMENT         Unix
.COMMENTS            Definitions related to File Management
.VERSION 1.0	04-Mar-1988  Creation   
.VERSION 1.1	31-Jan-1990  Removed filename operations (in filedef)
.VERSION 2.0	17-Feb-1992  filestatus structure for osfinfo here. CG.
.VERSION 2.1	13-Jul-1992  blocksize remove. SCO/PC does not support it. CG 

 090528		last modif
--------------------------------------------------------------------------*/

#ifndef  OSFILE_DEF 
#define  OSFILE_DEF      0

/*===========================================================================
 *		Basic Definitions
 *===========================================================================*/

#include <filedef.h>

/*===========================================================================
 *		File structure
 *===========================================================================*/

struct filestatus 		/* structure status (tbd) */
 { 
   long filesize;		/* file size in BYTES 	*/
   long date;           	/* Revision date 	*/
   int  owner;			/* owner (UID) id 	*/
   int  protection;		/* protection 		*/
  };

/*===========================================================================
 *			I/O Functions 
 *===========================================================================*/

#ifndef _TEMPLATES_
#include <compiler.h>
#endif


/*===========================================================================
 *			I/O Macros
 *===========================================================================*/

#define osaflush	osawait		/* Just a more obvious name... */
#define osdflush	osdwait		/* Just a more obvious name... */

#define osatell(f)	osaseek(f,0L,FILE_CURRENT);
#define osdtell(f)	osdseek(f,0L,FILE_CURRENT);

#define  osaput(f,str)		osawrite(f,str,strlen(str))
#define  osdput(f,str)		osdwrite(f,str,strlen(str))
#define  osdputext(f,str)	osdwrite(f,str,sizeof(str)-1)

#define  osfchmod(f,mod)	osfcontrol(f,CHMOD,mod,0)
#define  osfchown(f,o,g)	osfcontrol(f,CHOWN,o,g)

#endif
