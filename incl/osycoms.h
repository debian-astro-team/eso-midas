/* @(#)osycoms.h	19.1 (ESO-IPG) 02/25/03 13:49:38 */
/*

------------------------------  OSYCOMS  ------------------------------ 
 
definitions of operating system commands
on file  mid_include:osycoms.h  

010713		last modif	KB

*/	


#define OSY_COMCOUNT  10


static char	*OSY_COMS[] = {
"DIR",
"COPY",
"@",
"DELCNF",
"DELETE",
"RENAME",
"TYPE",
"MORE",
"PURGE",
"PRINT"
};
       
                  
#if vms

static char	*OSY_DEFS[] = {
"$DIREC", 
"$COPY",
"$@",
"$DELETE/CONF",
"$DELETE/NOCONF",
"$RENAME",
"$TYPE",
"$TYPE/P",
"$PURGE",
"$PRINT"
};

#else

static char	*OSY_DEFS[] = {
"$ls", 
"$cp",
"$sh",
"$rm -i",
"$rm -f",
"$mv",
"$cat",
"$more", 		/* was: $page before */
"$ ",
"$lpr"
};

#endif

 

/* 
 
   operating system commands are indicated via -comnd
   where comnd defined above in OSY_COMS


   K. Banse	version 1.65	900926
	
-----------------------------------------------------------------------


*/
