/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c)  1993-2007   European Southern Observatory
.LANGUAGE   C
.IDENT      fitskwt.h
.AUTHOR     P.Grosbol   ESO/IPG
.KEYWORDS   FITS, table extension keywords, definitions
.PURPOSE    define FITS keywords for table extension
.VERSION    1.0   1988-Sep-24:  Creation,  PJG
.VERSION    1.1   1990-Nov-08:  Change KWDEF + display format,  PJG
.VERSION    1.2   1991-Mar-05:  Rename MXFS to MXF + add nrow etc, PJG
.VERSION    1.3   1991-Mar-22:  Make TNULL type undefined, PJG
.VERSION    1.4   1991-Sep-23:  Add 'tncpf' to FDEF structure, PJG
.VERSION    1.5   1992-Feb-20:  Add THEAP keyword and number sign, PJG
.VERSION    1.6   1993-Oct-27:  Move FDEF and TXDEF to fitsdef.h , PJG

 070423		last modif

--------------------------------------------------------------------*/

#define       MXF        999    /* max. no of table fields (was 768) 
				   according to FITS standard the max
				   for keyword TFIELDS is 999  */

#define       TXCTL        3    /* keyword group: table ext. cntr.  */

#define       TFIELDS      1    /* keyword action : table extension */
#define       TBCOL        2
#define       TFORM        3
#define       TTYPE        4
#define       TUNIT        5
#define       TSCAL        6
#define       TZERO        7
#define       TNULL        8
#define       TDISP        9
#define       THEAP       10

static  KWDEF  tkw[] = { 
	 {"TBCOL###",'I' ,TXCTL,TBCOL,"",  0,'\0',0.0,""},
	 {"TDISP###",'S' ,TXCTL,TDISP,"",  0,'\0',0.0,""},
	 {"TFIELDS ",'I' ,TXCTL,TFIELDS,"",0,'\0',0.0,""},
	 {"TFORM###",'S' ,TXCTL,TFORM,"",  0,'\0',0.0,""},
	 {"THEAP   ",'I' ,TXCTL,THEAP,"",  0,'\0',0.0,""},
	 {"TNULL###",'\0',TXCTL,TNULL,"",  0,'\0',0.0,""},
	 {"TSCAL###",'R' ,TXCTL,TSCAL,"",  0,'\0',0.0,""},
	 {"TTYPE###",'S' ,TXCTL,TTYPE,"",  0,'\0',0.0,""},
	 {"TUNIT###",'S' ,TXCTL,TUNIT,"",  0,'\0',0.0,""},
	 {"TZERO###",'R' ,TXCTL,TZERO,"",  0,'\0',0.0,""},
         {(char *) 0,'\0',0,0,"",0,'\0',0.0,""} };
