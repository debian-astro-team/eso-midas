/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c) 1996,2003  European Southern Observatory
.LANGUAGE     C
.IDENT        fitsvals.h
.AUTHOR       K. Banse   ESO/IPG
.KEYWORDS     FITS, definitions
.PURPOSE      define specific values needed in the FITS I/O library
.VERSION      1.0   030801	creation

 030801		last modif
--------------------------------------------------------------------*/


int           same_comp_i2;  /* same 2-byte integer format         */
int           same_comp_i4;  /* same 4-byte integer format         */

