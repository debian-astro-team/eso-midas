/*
 
---------------------------- MIDBACK ------------------------------- 

layout of the different structures which are used in the BackEnd MIDAS monitor
 
K. Banse	930210

.VERSION
 070813		last modif
---------------------------------------------------------------------

*/

#define CODE_BLOCKS 20				/* initial size of CODE.CODE */
#define CODE_DEFS 400
#define CODE_DEFZ CODE_DEFS-1
#define CODE_DEFLEN 450
#define CODE_PRNAME CODE_DEFS+CODE_DEFLEN+1
#define CODE_START CODE_PRNAME+89

#define MAX_BATCH 6
#define MAX_FRAME 6
#define MAX_CONTXT 15
#define MAX_LEVEL 25			/* !synchronize with midkeyb.c! */
#define MAX_LEV1 MAX_LEVEL+1



struct	FLAGS_STRUCT
	{
	int		LEVEL;
	int		PAUSLEVL;
	int		TOPLEVL;
	int		COUNT;
	int		CROSS_COUNT;
	int		INTERRUPT;
	int		PCODE[MAX_LEVEL];
	int		QCODE[MAX_LEVEL];
	int		PROCESS;
	int		POFF[MAX_LEV1];
	int		MAXTIME;
	int		MXT[MAX_LEV1];
        long int	ENDT[MAX_LEV1];
	long int	PID;
	unsigned char   ECKO[MAX_LEV1];
	unsigned char   PDEBUG[MAX_LEV1];
	unsigned char   QDEBUG[MAX_LEV1];
	unsigned char   MDEBUG[MAX_LEV1];
	unsigned char	COMPILED;
	unsigned char	CTRLC;
	char		FRAME_USED;
	unsigned char	CMD_LIST_UPDA;
	unsigned char	ENV;
	};

struct	FLAGS_STRUCT	MONIT;
 


struct	CROSS_STRUCT
	{
	char		LABEL[20];
	char		PARM[MAX_TOKEN+2];
	int		NO;
	};

struct	CROSS_STRUCT	CROSS[8];


struct	CODE_STRUCT
	{
	char		*CODE;
	int		LEN;
	char		WORK1[MAX_TOKEN+2];
	char		WORK2[2*MAX_TOKEN+2];
	char		WORK3[800];
	char		WORK4[4000];
	};

struct	CODE_STRUCT	CODE;


struct	FRAME_STRUCT
	{
	int      	ENTRY;
	int     	TYPE;
	int		LEVEL;
	};

struct	FRAME_STRUCT	FRAME[MAX_FRAME];	


struct	TRANSLATE_STRUCT
	{
	char 		*PNTR[2];
	int		LEN[2];
	};

struct	TRANSLATE_STRUCT	TRANSLATE[MAX_LEVEL];


struct  CONTEXT_STRUCT
        {
        char       NAME[MAX_CONTXT*8+2];
        char       *pdirec[MAX_CONTXT];
        int        QUEUE[MAX_CONTXT];
        int        BASE;
        int        STACK;
	char       P_INSTR[16];
        };

struct  CONTEXT_STRUCT        CONTXT;


struct	PROC_STRUCT
	{
	char		FNAME[168];
	char		QNAME[168];
	char		ENTRY[8];
	char		FORMAT[(MAX_LEV1)*15];
	};

struct	PROC_STRUCT	PROC;


struct	BATCH_STRUCT
	{
	char		CMND[8];
	};

struct	BATCH_STRUCT	BATCH[MAX_BATCH];


struct	PATH_STRUCT
	{
	char		STR[84];
	};

struct	PATH_STRUCT	CPATH[4], EPATH[4];


struct  REDIRECT_STRUCT
        {
        int	OUTFLAG[4];
        char	OUTFILE[64];
        };

struct  REDIRECT_STRUCT     REDIRECT[5];


struct	PIPE_STRUCT
	{
	int	HOME_LEN;
        char	HOME[160];
	char	CONT[24];
	char	EXE[24];
	char	PROC[24];
	char	INSTRUM[16];
	};

struct	PIPE_STRUCT	PIPE;


/*

structure MONIT :
-----------------

LEVEL		program level (0,..,MAX_LEVEL),
		i.e. nesting MAX_LEVEL levels deep is possible 
PAUSLEVL        save foreground level at PAUSE statement
TOPLEVL         deepest (highest) level reached within a procedure execution,
	        kept for option of simply recopying previously used
	        procedures
COUNT		no. of different tokens in a command line (max = 10)
CROSS_COUNT	no. of parameters passed with procedure 
		for cross reference business
INTERRUPT       interrupt flag for PAUSE command
                = 1, when PAUSE encountered in foreground procedure
                = 0, when CONTINUE command encountered
PCODE[0]...[m]	program counters (bytes) for levels 1 - MAX_LEVEL
QCODE[0]...[m]	save PCODE[0...m] in PAUSE/CONTINUE scenario
                `m' = MAX_LEVEL - 1
PROCESS		process status flag for permanent foreground subprocess (VMS)
		process type (Unix)
		= 2 (host command, e.g. $ ls -l) = 3 (Midas command)
		= 8 (exec command with debugger, e.g. $dbs statis.exe)
		= 9 (time the command) 
POFF		holds offsets within key structure of keys 
		P1,...,P8 + Q1,...,Q3
PID		PID of child process
MAXTIME		first (lowest level) max. time in secs. for commands/procedures
MXT		max. time in secs. at each proc. level
		if > 0 is used as flag that there is a timeout + first approx.
		end time is calculated with it
ENDT		end time in secs. at each proc. level
                if MXT > 0, exact end time is calculated at start of procedure,
		child process generation
ECKO[n]		Echo: 0 = off, 1 = on,
		2 = full (= show replacements of parameters + variables)
PDEBUG[n]	0 = execute procedures normally, (n = MONIT.LEVEL])
		1 = execute procedures in stepwise debug mode,
		2 = execute procedures in "go" debug mode
QDEBUG[n]	save PDEBUG[0...MAX_LEVEL] in PAUSE/CONTINUE scenario
MDEBUG[n]	0 = execute modules/programs normally, (n = MONIT.LEVEL])
		1 = do not execute modules
		2 = execute modules in debug mode
COMPILED	if set, we have as input already compiled code
CTRLC		CntrlC flag, if set a CntrlC interrupt has occurred
FRAME_USED	if set to 'x', the FRAME struct contains opened images/tables
CMD_LIST_UPDA	flag for updating the `cmd_list': 0 = off, 1 = on,
		used with READLINE stuff for command completion
ENV		'P' if called from PyMidas, otherwise 'N' (normal)




structure CROSS[8]:
-------------------

LABEL[20]	label of parameter (maybe "Pi")	 ( including '\0' )
PARM[82]	parameter itself ( including '\0' )
NO		no. of corresponding parameter	

	
structure CODE:
---------------

CODE		pointer to buffer which holds the procedure code
		in there each command line is delimited by '\r',
		end of procedure by '\r\r'.

CODE_DEFS	start of default values for parms P1 - P8 (currently = 200)

CODE[0]		no. of parameters passed
CODE[1 - (cd-1)]    actual parameter values delimited by '\r' (cd = CODE_DEFS)
CODE[cd - (cd+cdl)] default values for parameters delimited by '\r' 
                    (cdl = CODE_DEFLEN)
CODE[cdp - (cdp+88)] entry name (8 chars) + full name of procedure  (max. 80)
                    (cdp = CODE_PRNAME, cds = CODE_START)
CODE_START	start of procedure code (= cdp + 89)
	
LEN		current length of CODE
WORKi		working buffers, but handle with care...
	



structure FRAME[MAX_FRAME]:
-------------------

ENTRY		frame no. returned from SCFOPN, TCTOPN
TYPE		frame type, e.g. F_IMA_TYPE, F_TBL_TYPE, ...
LEVEL		level at which frames where mapped: 1 - MAX_LEVEL



structure TRANSLATE[MAX_LEVEL]:
-------------------------------

PNTR[2]		pointer to allocated memory for CODE (foregr, PAUSE)
LEN[2]		size of allocated memory (foregr, PAUSE)
	


structure CONTXT:
-----------------

NAME		holds enabled context names (8 chars. each), the position of
		the context name determines the context no. via
		offset/8 + 1 (context no. = 1,2,...,MAX_CONTXT)
pdirec		pointer to specific directory, if none of the "usual" direcs
QUEUE		keeps the context no. ordered according to activation
		if QUEUE[0] == 0, no context enabled
BASE		index in QUEUE of context which is the first SET/CONTEXT
		to help handling nested SET/CONTEXT commands
STACK		incremented with each SET/CONTEXT
P_INSTR		pipeline instrument, if relevant context, e.g. FORS2
	


structure PROC:
---------------

FNAME[168]	name of procedure file
QNAME[168]	save FNAME in PAUSE/CONTINUE scenario
ENTRY[8]	entry in procedure file
FORMAT[15]	integer, real + double format (5 chars) for each level
	


structure BATCH[MAX_BATCH]:
-------------------

CMND[6]         special commands to provide structures in procedures,
	        e.g IF ... THEN ...ELSE ... ENDIF or DO ...ENDDO
	


structure CPATH[4]:
-------------------

STR[84]         specification of path for accessing Midas procedures
	

structure EPATH[4]:
-------------------

STR[84]         specification of path for accessing Midas executables
	


structure REDIRECT[5]:
-------------------------------

OUTFLAG[4]	exactly as first 4 elements of keyword OUT$FLAG
OUTFILE		name of output file as keyword OUT$FILE


structure PIPE:
---------------

HOME_LEN	length of PIPE_HOME directory string
HOME[160]	the PIPE_HOME directory
CONT[24]	instrument + "/context/" string
EXE[24]		instrument + "/exec/" string
PROC[24]	instrument + "/proc/" string
INSTRUM[16]	the pipeline instrument, currently: 
		fors1, fors2, uves

*/
