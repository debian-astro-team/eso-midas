/* @(#)xconndef.h	19.1 (ESO-DMD) 02/25/03 13:49:45 */
/*
 
---------------------------- XCONNDEF ------------------------------- 

layout of the connection structure for communication with a Midas monitor
 
K. Banse	940421, 981211, 990830

---------------------------------------------------------------------

*/

#define MAX_BACK 10		/* sync with bkstat[] in prepb.c! */


/* the following constant MAX_MIDAS_LINE
   has to be synchronized with MAX_LINE in monitdef.h !!! */

#define MAX_MIDAS_LINE  400



struct BACK_STRUCT
        {
        int	        PID;
        int             WAIT;
        int             CHAN;
        char            UNIT[2];
        char            HOST[60];
        char            COMC;
        char            METHOD;
        char            FLAG;
        char            ACTIVE;
        };

extern struct BACK_STRUCT      BKMIDAS[];


struct XCONN_STRUCT
        {
        int             MYPID;
        int             OFFS;
        int             OFFR;
        char            MYUNIT[2];
        char            SBOX[80];
        char            RBOX[80];
        };

extern struct XCONN_STRUCT      XCONNECT;





/*


structure BKMIDAS[MAX_BACK]:
----------------

PID             process id of background Midas (only for FILES method)
WAIT            no. of secs. to wait, if FLAG = 'w' (wait)
CHAN            osx channel (only for OSX method)
UNIT[2]         unit of background Midas
HOST[60]        remote host of background Midas if not on same machine
COMC            character to precede the actual command, e.g.
                read/descr => COMC,read/descr
                this command then goes automatically to the background Midas
METHOD          indicates FILES or OSX method of connecting to background MIDAS
FLAG		wait flag
ACTIVE		= Y or N if active or not



structure XCONNECT
----------------

MYPID		my own process id 
MYUNIT[2]	my own 'pseudo' Midas unit




*/
