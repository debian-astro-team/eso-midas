/* @(#)te.h	19.1 (ESO-IPG) 02/25/03 13:49:43 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.IDENTIFICATION twset.h
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Window Management
.ENVIRONMENT	TermWindows
.COMMENTS	This file just includes definitions used for
		compilation of TermWindows

.VERSION  2.0	01-Jul-1987: Version '2'
.VERSION  3.0	20-Mar-1988: Version '3'
.VERSION  3.1	27-Jun-1989: Version '3.1'
.VERSION  3.2	09-Feb-1990: Added 2 patch bytes to overcome the Sun3/Sun4 pb.

----------------------------------------------------------------------------*/

#ifndef TWSET_DEF
#define TWSET_DEF	0

#define	TW_VERSION	'\032'

/******************* Definition of Structures via TW_STRUCT ****************/

#ifndef TW_STRUCT
#define TW_STRUCT	1
#endif

#include <tw.h>
#include <macrogen.h>	/* Classical Maacros	*/
#include <ok.h>		/* OK / NOK Definitions	*/
#include <osfile.h>	/* File i/o functions	*/
#include <trace.h>	/* Tracing Subsystem	*/
#include <error.h>	/* Error Tracing	*/
#include <osdefos.h>	/* Memory Classes	*/
#include <atype.h>
#if TW_STRUCT
#include <buffer.h>
#endif
#if TRACING
#include <levels.h>
#endif

char *osmmget(), *osmmexp();

#ifndef MEM_GET
#define MEM_GET(type,n)		(type *)osmmget(sizeof(type)*(n)) 	 \
			/* allocates n items of type		*/
#define MEM_FREE(ptr) 	osmmfree((char *)ptr)
			/* free previously allocated memory	*/
#define MEM_EXP(type,ptr,n)	(type *)osmmexp(ptr,sizeof(type)*(n)) \
			/* reallocates n items of type		*/
#endif

#ifdef 	EOF
#undef	EOF
#endif

#define EOF	_EOF_

#ifndef TW_MACROS
#define TW_MACROS	1	/* Include macros for tw_ functions */
#endif

/*=======================================================================
	The following definitions should be in twparam.h, but... 
 *=======================================================================*/
	
#ifndef TWFAC_Stacking
#define TWFAC_Stacking	1	/* Stacking Commands is allowed	*/
#endif
#ifndef TWFAC_Log
#define TWFAC_Log	0	/* No logging				*/
#endif
#ifndef TWFAC_TeX
#define TWFAC_TeX	1	/* TermDisplay allowed			*/
#endif
#ifndef TW_LEVEL
#define TW_LEVEL	1
#endif

#ifndef TERMCAPFILE
#define TERMCAPFILE	oshenv("TERMCAPFILE", (char *)0)	
#endif

#ifndef TW_cc_default
#if OS_VMS
#define TW_cc_default	 TW_cc_VMS
#else 
#define TW_cc_default	 TW_cc_UNIX
#endif
#endif

/*=======================================================================
	Definitions related to Terminal (tv)
 *=======================================================================*/

#define MEM_AMOUNT	512

#define	DEL		0177	/* Code for ASCII delete character	*/
#define	SI		'\017'	/* Code for ASCII shift in	(SI)	*/
#define	SO		'\016'	/* Code for ASCII shift out	(SO)	*/
#define FORMAT		0376	/* Format indicator			*/

#define TVBUF	 	132	/* Maximum I/O packet to terminal	*/

struct INDEX_struct {			/* Structure of the index	*/
	char cap[2];		/* The cap code	*/
	short int  icap;	/* Corresponding offset in CAPLIST	*/
	};
typedef struct INDEX_struct INDEX;

/*=======================================================================
 * 		Basic TERM structure, for use with TERMCAP
 *=======================================================================*/

struct TERM_struct {
	char term_type;		/* 0: normal / 1: File			*/
	char version;		/* The version number			*/
	unsigned short int msw;	/* ms to wait after each output		*/
	short int  size;	/* Size of CAPLIST			*/
	short int  captop;	/* Used bytes in CAPLIST		*/
	short int  ns;		/* Number of sequences			*/
	short int  index;	/* Starting byte in caplist with indexes
					by capcode			*/
	short int  nsr;		/* Number of readable sequences		*/
	short int  indexr;	/* Starting byte in caplist with indexes
					by sequences			*/
	short int  baud;	/* Baud rate of the terminal		*/
	unsigned char attr_init;
	unsigned char attr;	/* Present attributes			*/
	short int  dim_init[2];	/* Dimensions at init			*/
	short int  dim[2];
	short int  pos[2];	/* Present cursor pos.			*/
	short int  scregion[2];	/* Present scrolling region		*/
	unsigned long int specials;
				/* Bit string specifying input seq.	*/
	short int  buf_size;	/* Size of read/write/space buffers	*/
	short int  ir;		/* Position in bufr			*/
	short int  irtop;	/* Last char in bufr			*/
	short int  iw;		/* Position in bufw			*/
	unsigned char flags;	/* Flags defined as follows:		*/
#define TERM_buffered	0x01	/* If set output only to buffer		*/
#define TERM_KPAD	0x02	/* If set, in keypad mode		*/
#define TERM_RAW	0x04	/* Raw mode, i.e. do not interpret controls */
#define TERM_IMODE	0x08	/* Insert Mode		*/
#define TERM_am		0x10	/* Automatic margins in the terminal	*/
#define TERM_NOWRITE	0x20	/* If set, don't output text to screen	*/
#define TERM_hard	0x40	/* If set, it's harcopy terminal	*/
	char pad_char;		/* Used char for padding		*/
	char standout;		/* Standout: 
				 * 0 = attribute pointers unloaded
				 * 1 = attribute pointers loaded
				 * 2 = stand-out with overstrike
				 * 3 = stand-out with case change
				 * 	Other: don't use standout	*/
#define NO_STANDOUT	   4
#define ChangedLine	0x10	/* Reissue standout			*/
#define ChangedControl	0x20	/* Modify Help Window			*/
	char tc[33];		/* Control Characters	(only 33 used)	*/
	char *term_name;	/* Expanded name			*/
	char *save_file;	/* File where the terminal is saved	*/
	int (*fct)();		/* To execute while waiting for input	*/
	char *caplist;		/* Address of CAPLIST			*/
	char *bufr;		/* Read buffer				*/
	char *bufw;		/* Write buffer				*/
	char *bufs;		/* Space buffer				*/
	char *tt_ptr[6];	/* Cursor mouvements			*/
#define	tt_cm	tt_ptr[5]	/* Direct cursor addressing		*/
	char *tt_clear;		/* Clear whole screen			*/
	char *tt_attr[7];	/* Changing attribute sequences		*/
	char gchars[12];  	/* Terminal-dependant graphic chars	*/
	char last_char;		/* Last input				*/
	char last_type;		/* Last input				*/
	char spare_bytes[2];
	};
typedef struct TERM_struct TERM;

/*=======================================================================
 *	Basic functions, used also in the Windows context
 *=======================================================================*/

char *tu_scap();		/* Search a capability	*/
#define SearchCap(cap)		tu_scap(cap)
#define SendCap(cap)		tv_send(SearchCap(cap),1)
#define tv_gopen()		tv_open((char *)0, (char *)0, TW_cc_default)
char *osmsg(), *oshenv();


/*=======================================================================
 * 		Basic WINDOW Structure
 *=======================================================================*/

typedef ACHAR *LINE;		/* Array of line pointers		*/

struct COMMAND_struct {	/* --- 	Structure of stored commands		*/
	unsigned int exit_cc;	/* List of Control Chars that return	*/
	unsigned char exit_ar;	/* List of Arrows that return		*/
	unsigned char exit_pf;	/* Other keys that exit			*/
	char last_type;		/* Key that caused exit			*/
	char last_char;		/* Key that caused exit			*/
	short int size;		/* Size of buffer with stored commands	*/
	short int used;		/* Used bytes in buffer			*/
	short int pointed;	/* Position of current command		*/
	short int pointed_no;	/* Number of current command		*/
	short int first_no;	/* Number of oldest command		*/
	short int last_no;	/* Number of last issued command	*/
	unsigned char  *text;	/* Buffer with stored commands		*/
  };
typedef struct COMMAND_struct COMMAND;	/* The Command Stack		*/

#if TW_STRUCT
struct	WINDOW1_struct {	/* Simplest Windows			*/
	WINDOW win;
	WINDOW	*previous,	/* Link between windows			*/
		*next,
		*parent,	/* For subwindows, 0 for Windows	*/
		*child;		/* First child window			*/
	LINE	*line,		/* First line of window			*/
		*m0,		/* First modified char			*/
		*m1;		/* Last modified char			*/
	BUFFER	*field_buf;	/* Collects Fields definitions		*/
	COMMAND *command;	/* Address of Command Stacking Buffer	*/
  } ;	
typedef struct WINDOW1_struct WINDOW1;

#define CommandLength	(82)	

#define tw_rw		tw_r		/* Refresh	*/
#define Wu	 0x01		/* 	Window is at screen top		*/
#define Wd	 0x02		/* 	Window is at screen bottom	*/
#define Wl	 0x04		/* 	Window is at screen left margin	*/
#define Wr	 0x08		/* 	Window is at screen right 	*/
#define Scroll 	 _SCROLL_	/*	Scrolling facility		*/
#define FullW	 _FULL_		/*	Full Window			*/

#define Parent(w)	(((WINDOW1 *)w)->parent)
#define Child(w)	(((WINDOW1 *)w)->child)
#define Next(w)		(((WINDOW1 *)w)->next)
#define Previous(w)	(((WINDOW1 *)w)->previous)
#define Command(w)	(((WINDOW1 *)w)->command)
#define Fields(w)	(((WINDOW1 *)w)->field_buf)
#define ALine(w)	(((WINDOW1 *)w)->line)
#define Am0(w)		(((WINDOW1 *)w)->m0)
#define Am1(w)		(((WINDOW1 *)w)->m1)
#define Aij(w,i,j)	(ALine(w)[i] + j)
#define Apos(w,pos)	Aij(w,pos/w->dim[1],pos%w->dim[1])
#define M0(w,i)		(Am0(w)[i])
#define M1(w,i)		(Am1(w)[i])

#endif

/*=======================================================================
 * 	Macros just to improve readibility in compiling tw_ routines	
 *=======================================================================*/

#define FINISH		goto FIN
#define HIGH		0xFF

#if TW_MACROS		/* Not loaded for TV routines	*/

#define	Ni		dim[0]		/* Lines	*/
#define	Nj		dim[1]		/* Columns	*/
#define	i0		home[0]		/* Home line	*/
#define	j0		home[1]		/* Home column	*/
#define	active		flags&Echo	/* Echo on	*/
#define	clear		flags&Clear	/* Is clear	*/
#define	scroll		flags&Scroll	/* Is scrolling	*/
#define	imode		flags&Imode	/* Insert Mode	*/

	/* Redefinition en Enter / Exit macros, such that a window
	 * is only physically output once in case of consecutive
	 * calls to any window function.
	 */

#ifndef Active_Removed
#define Active_Removed 	tw_rw(w, 0, NULL_WINDOW)      /* Update Window	*/
#endif

#define Return		goto ReturN
#define WinTest(w)	
#define	ENTER_TW(x)	unsigned char state_buffer;			\
			ENTER(x);					\
			status = OK;					\
			state_buffer  =	tv_buffer(1);	
#define	ENTER_TWA(x)	unsigned char state_buffer,state_echo;		\
			ENTER(x);					\
			status = OK;					\
			CheckScreen(w);					\
			state_buffer  =	tv_buffer(1);			\
			state_echo    = w->flags&Echo;			\
			tw_st(w, Echo, 0);
#define	EXIT_TW(f)	FIN:	ReturN:					\
			tv_buffer(state_buffer);			\
			EXIT(f)
#define	EXIT_TWA(f)	FIN:						\
			if(state_echo) Active_Removed; 			\
			ReturN:						\
			tw_st(w, state_echo, 1);			\
			tv_buffer(state_buffer);			\
			EXIT(f)
#define CheckScreen(w)	if(w == NULL_WINDOW)	w = Screen
#endif

#endif
