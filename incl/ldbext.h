/* ----------------------- LDBEXT ------------------------- 
 
               definition of local descriptor block

.VERSION
 090318		last modif
-----------------------------------------------------------  */

/*        size in bytes of different data types */

#ifndef SIZE_II
#define SIZE_II

#define II_SIZE  (int) sizeof(int)
#define JJ_SIZE  (int) sizeof(short int)
#define RR_SIZE  (int) sizeof(float)
#define DD_SIZE  (int) sizeof(double)
#define SS_SIZE  (int) sizeof(size_t)

#endif    /* SIZE_II */


#define LDB_SIZE 2048			/* current size of a LDB in bytes */
#define LDB_DATA (LDB_SIZE-(2*II_SIZE))	/* data bytes per LDB */

#define LDB_NDSCRW (LDB_DATA/II_SIZE)
#define LDB_NDSCRW1 (LDB_NDSCRW-1)


struct	LDB_STRUCT
	{
	int	BLKNUM;
	union
		{
		int      IWORD[LDB_NDSCRW];
		float	 RWORD[LDB_NDSCRW];
		char	 CWORD[LDB_DATA];
		} 	LDBWORDS;

	int	NEXT;
	};


/*

     NDSCRW  ...  Number of descriptor 4-byte words in block.
     NDSCRW1 ...  Last index in LDB (remember that we start from 0 ...)
     NDSCRB  ...  Number of descriptor bytes (characters) in block.
     BLKNUM  ...  Block Number of this LDB.
     IWORD   ...  I-Descriptors. (addressed as 4-byte words)
     RWORD   ...  R-Descriptors. (addressed as 4-byte words)
     CWORD   ...  C-Descriptors. (addressed as characters)
     NEXT    ...  Next Local Descriptor Block.


  K. Banse   880323, 930907, 990304

*/
