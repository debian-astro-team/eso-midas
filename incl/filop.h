/* @(#)filop.h	19.1 (ESO-IPG) 02/25/03 13:49:29 */
/*+++++++++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      filop.h
.AUTHOR              Francois Ochsenbein [ESO-IPG]
.KEYWORDS            Operations on Files
.ENVIRONMENT         Any
.COMMENTS            
.VERSION 1.0	24-October-1989  Extracted from stesodef.h
--------------------------------------------------------------------------*/

#ifndef  FILOP_DEF 
#define  FILOP_DEF	1	/* Include the definitions	*/
#endif

#if 	FILOP_DEF
#undef 	FILOP_DEF
#define FILOP_DEF	0	/* Don't include a second time 	*/

#ifndef _TEMPLATES_	/* Are templates allowed ? 	*/
#include <compiler.h>
#endif

#if _TEMPLATES_		/*===> Complete Function Prototypes	*/
int 	fi_create	(char *filename, int filesize, int protection);
int     fi_load 	(char *filename, long offset, char *buffer, 
					unsigned int buffer_size);
int 	fi_open		(char *filename, int open_mode);
char   *fi_name    	(int fid);
int 	fi_close    	(int fid);
int     fi_read 	(int fid, char *buffer, unsigned int buffer_size);
int 	fi_write	(int fid, char *buffer, unsigned int buffer_length);
long 	fi_seek     	(int fid, long offset, int mode);
long 	fi_tell     	(int fid);
int     fi_gets 	(int fid, char *buffer, unsigned int buffer_size);
int 	fi_put   	(int fid, char *text);
int 	fi_puts    	(int fid, char *record);
int 	fi_flush	(int fid);
int 	fi_chmod	(char *filename, int new_protection);
int 	fi_delete	(char *filename);
int 	fi_rename	(char *filename, char *newname);
long	fi_size		(char *filename);
long	fi_date		(char *filename);
#else			/*===> Those functions that don't return integer... */
long 	fi_seek(), fi_tell(), fi_date(), fi_size();
char	*fi_name();
#endif
	

#endif
