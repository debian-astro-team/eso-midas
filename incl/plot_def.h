/* @(#)plot_def.h	19.1 (ESO-IPG) 02/25/03 13:49:38 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   plot_def.h
.AUTHOR      R.M. van Hees IPG-ESO Garching
.PURPOSE     MIDAS definition table for the plotting routines

.VERSION     1.1     27-Oct-1993   creation by  RvH
------------------------------------------------------------*/
#include <computer.h>

/*
 * Some general constants for the plot routines
 * Note that PI and PLT_EPS can be defined as doubles!
 */
#ifndef  PI
# ifndef vms 
#   ifdef M_PI
#     define  PI		M_PI
#   else
#     define  PI    	3.14159265358979323846
#   endif
# else
#  define  PI    	3.14159265358979323846
# endif
#endif

#define  PLT_EPS        MINDOUBLE

#ifndef TRUE
#define TRUE            1
#define FALSE           0
#endif

/*
 * Difference in address space between X, Y and Z axis 
 * in the parameters: xAXIS & xWNDL 
 */
#define  FOR_X		0
#define  FOR_Y		4
#define  FOR_Z		8

/*
 * dimension of data to be plotted
 */
#undef  DIMMAX
#define PLDIM2          2
#define PLDIM3          3
#define DIMMAX          3

/*
 * define some macros
 */
#define MYMIN(a,b)   ((a) > (b) ? (b) : (a))
#define MYMAX(a,b)   ((b) > (a) ? (b) : (a))
#define NINT(a)      ((a) < 0 ? (int)((a) - 0.5 ) : (int)((a) + 0.5) )
#define MYDIST(a,b)  ( sqrt( (a) * (a) + (b) * (b) ) )
#define MYLENGTH(a,b)( sqrt( (a) * (a) + (b) * (b) ) )
#define NSTEP(str,until,step) (int) fabs( ((until) - (str))/(step) )+1

/*
 * adresses to values in MID$CMND -> *cmnd 
 */
#define COMM       cmnd
#define QUAL       cmnd+10

/*
 * Definition of two structs used to store 3-D data and 2-D data,
 * Odata is the original data (x,y) and pixel value
 * Pdata is the projected data, see GETPROJ, PLFRM3 and PLPER
 */
struct Odata { double xx;
               double yy;
               double zz;
             };

struct Pdata { double px;
               double py;
             };
