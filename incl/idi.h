/* @(#)idi.h	19.1 (ESO-IPG) 02/25/03 13:49:34 */
/***********************************************************************
*                          
*   file idi.h            
*                        
*   IDI  errors & constants definition 
*                                     
*                                   
*  V 1.1  871201: P. Santin  - Trieste Astronomical Observatory
*  V 2.0  881221: K. Banse - ESO  Garching                    
*  V 2.1  890310: KB                                         
*  V 2.2  890612: KB                                        
*  V 2.5  900126: now support up to 8 windows
*  V 2.6  901220: increase size of graphics structure
*  V 2.7  911129: throw out MAX_PNTS 
*  V 2.8  911218: add MAX_WST
*  V 2.9  920117: add ENTRYFND
*  V 2.9  920922: add NOFONTS
*  V 3.0  930218: MAX_DEV -> 16, add error ILLSHADOW
***********************************************************************/

# include <string.h>

# define MAX_WST          3        /* max no. of X workstations (screens) */
# define MAX_DEV         12        /* max no. of open displays on WS 
				      this has to be synchronized with
				      idisave.prg (which build idisaveXY.dat),
				      wndcrea.c (which uses idisaveXY.dat) */
# define MAX_MEM         13        /* max no. of memories for configuration 
				      no. of image chanls + 1 overlay chanl,
				      this has to be synchronized with
				      syskeys.datorg,
				      keyword DAZDEVR(20) = MAX_MEM - 1 */
# define MAX_LUT          4        /* max LUTs for configuration */
# define MAX_CURS         2        /* max cursors for display  */
# define MAX_INTER       10        /* max current interactions        */   
# define MAX_ZOOM       100        /* max zoom factor                 */

# define MAX_INT_DEV      2        /* max interactors on device       */
# define MAX_LOC          2        /* max locators for each interactor   */

/* MAX_TRG = max triggers for each interactor - has to be synchronized!!  */
# define MAX_TRG         10     

# define MAX_TXT        200        /* max text strings                 */
# define MAX_TXTLEN    1600        /* max size of text strings         */
# define PLOT_COLOURS     9        /* no. of plot colours supported    */

# define II_NULL             0

# define II_USER             0        /* interactive operations      */
# define II_MOVE             1
# define II_ROTATE           2
# define II_ZOOM             3
# define II_UNZOOM           4
# define II_CLZOOM           5
# define II_BLINK            6
# define II_MODIFY           7
# define II_SLICE            8

# define II_CURSOR           1        /* interactive objects         */
# define II_ITT              2
# define II_LUT              3
# define II_ROI              4
# define II_MEMORY           5        
# define II_DISPLAY          6
# define II_COLOR            7

# define II_MOUSE            1        /* interactive devices         */
# define II_KEYB             2

# define II_LOC              0        /* Interactor types            */
# define II_EVLR             1
# define II_EVLI             2
# define II_EVLT             3
# define II_EVLC             4
# define II_TRG              5

# define II_KEY              1        /* trigger description         */
# define II_BUTT             2

# define II_CROSSHAIR        1        /* cursor shape                */
# define II_CROSS            2   
# define II_OPNCROSS         3   
# define II_SQUARE           4   
# define II_DIAMOND          5   
# define II_1CIRCLE          6   
# define II_ARROW            7   
# define II_SMALCROSS        8   

# define II_SOLID            1        /* line style                  */
# define II_DASHED           2
# define II_DOTTED           3
# define II_DASH_DOTTED      4

# define II_IMAGE            4        /* memory type                 */
# define II_GRAPHIC          2        
# define II_TEXT             1
# define II_RGB              8

# define II_DEFAULT          0        /* Write Mode                  */
# define II_XOR              1
# define II_OR               2

# define II_BLACK            1        /* Graphic colors              */
# define II_WHITE            2
# define II_RED              3
# define II_GREEN            4
# define II_BLUE             5
# define II_YELLOW           6
# define II_MAGENTA          7
# define II_CYAN             8

/***********************************************************************

           error codes returned from IDI routines

***********************************************************************/

# define II_SUCCESS      0

# define FNCNOTIMPL    100		/* function not implemented */
# define DISWNDERR     101
# define NOAVAILDEV    102
# define DEVNOTOP      103
# define DEVNAMERR     104
# define WSTNAMERR     105
# define ILLSHADOW     106
# define MAXCONFN      107

# define MEMALLERR     111
# define DCTFILERR     112
# define DCTREADERR    113

# define ILLCONFID     121

# define MEMNOTFREE    131
# define ILLMEMID      132
# define MEMNOTALL     133
# define MAXMEM        134

# define TWTOOBIG      141
# define IMGTOOBIG     142

# define LUTIDERR      151
# define LUTLENERR     152
# define LUTNOTDEF     153

# define ITTIDERR      161
# define ITTLENERR     162

# define CURNOTDEF     171

# define ROINOTDEF     181
# define MAXROI        182

# define ILLCURID      191
# define ILLTRIGGER    192 
# define ILLINTTYPE    193
# define ILLINTOBJ     194
# define ILLINTOPER    195

# define NOALPHA       201

# define WINOTOPN      231
# define BADVISUAL     232
# define NOCOLALLO     233
# define NOFONTS       234
# define XCONNERR      235
# define BADXWTYPE     236

# define BADINPUT      301

/* -------------------- Info + Warnings -----------------------------*/

# define ENTRYFND     -99
# define DEPTHERR     -111
# define MEMTYPERR    -112
# define MAXMEMLST    -113

# define LUTNOTFREE   -121

# define CURNOTFREE   -131
# define CURNOTVIS    -132

# define ROINOTVIS    -141

# define INTNOTALL    -151 
# define MAXNOINT     -152
# define INTNOEN      -153

/*********************************************************************/
/* #include <proto_idi.h>		    ANSI prototyping */
