/* @(#)osudef.h	19.1 (ESO-IPG) 02/25/03 13:49:37 */
/*++++++++++++
.TYPE  		Header
.LANGUAGE	C
.IDENTIFICATION	osudef.h
.AUTHOR		Francois Ochsenbin, Benoit Pirenne  [ESO-IPG]
.KEYWORDS	Local definitions for device access
.ENVIRONMENT	VMS / UNIX
.COMMENTS   	Here are defined structures / constants
			common to various iodev routines
.VERSION 1.0	12-Oct-1989
.VERSION 1.1	20-Dec-1989: Added U_EOM (Go to End-Of-Media)
.VERSION 1.11	12-Jan-1990: Added TapeMode
.VERSION 1.2	18-May-1990: Close has an option to dismount.
--------------------------------------------------------------------------*/

#ifndef  OSUDEF_DEF 
#define  OSUDEF_DEF	0    /* Indicates inclusion of this file */

/*===========================================================================
 *		Unit Status Structures
 *===========================================================================*/

struct osustat {
	long usize;		/* Size in blocks, non meaningful for tapes */
 	int blocksize;		/* Block or Sector size	*/
 	int density;		/* Density (tapes)	*/
	int isda;		/* 1 if Direct Access	*/
	int istm;		/* 1 if Tape Mode 	*/
  };

/*===========================================================================
 *		Class of iodev's definition
 *===========================================================================*/
struct opitem {
        int     opid;           /* Operator Identifier  */
        int     (*opf)();       /* Operator Function    */
  };
typedef struct opitem OPITEM;

struct iolist {
	struct iolist *(*next)();	
	char 	*klass;		/* Name of this class 	*/
	int	nop;		/* Number of Operators 	*/
	OPITEM	*oplist;	/* List of Functions	*/
};
typedef struct iolist	*(*IODEV)();
			/* Pointer to function returning ptr to struct 	*/


/*===========================================================================
 *		Available Operations
 *===========================================================================*/

#define U_MAX	13	/* Highest function in the following list: */

#define U_INFO	 	0
#define U_OPEN	 	1
#define U_CLOSE  	2
#define U_READ	 	3
#define U_WRITE	 	4
#define U_REWIND 	5
#define U_SREAD	 	6	/* Sector Read		*/
#define U_SWRITE 	7	/* Sector Write		*/
#define U_WEOF		8	/* Write TM    		*/
#define U_FMF		9	/* File Move Forward  	*/
#define U_FMB		10	/* File Move Backward 	*/
#define U_BMF		11	/* Block Move Forward  	*/
#define U_BMB		12	/* Block Move Backward 	*/
#define U_EOM		13	/* Move to End-Of-Media	*/

	/* Available Options */

#define OPU_DISMOUNT	0x100	/* Dismount on Close	*/

#endif
