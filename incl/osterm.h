/* @(#)osterm.h	19.1 (ESO-IPG) 02/25/03 13:49:37 */
/*+++++++++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      osterm.h
.AUTHOR              Francois Ochsenbein [ESO-IPG]
.KEYWORDS            Terminal Definitions
.ENVIRONMENT        
.COMMENTS            Terminal structures are here.
.VERSION 1.0	04-Mar-1988  Creation   
.VERSION 1.1	21-Jan-1991  Redefinition ostint
.VERSION 1.2	06-Jul-1994  Templates are defined in proto_os.h  CG.
-----------------------*/

#ifndef  OSTERM_DEF 
#define  OSTERM_DEF      0

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

/*===========================================================================
 *		Terminal Status
 *===========================================================================*/

struct termstatus 		/* structure status (tbd) */
 { 
   char *termname;              /* TERMCAP name		*/
   int  baud_rate;		/* bits/s		*/
   int  lines;           	/* Number of lines	*/
   int  columns;		/* Number of columns	*/
   char cc_INT;			/* Interrupt Char	*/
   char cc_QUIT;		/* QUIT control char	*/
  };

#define  ostput(str)	ostwrite(str,strlen(str))

#endif
