C @(#)stack.inc	19.1 (ESO-IPG) 02/25/03 13:49:41
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C 
C	Stack Control Data Structure
C
C	Parameters
C
C	STACK_MAXNUM		Maximum number of stacks allowed
C	STACK_DEPDEF		Default stack deep
C	STACK_RECDEF		Default record length
C	STACK_MAXSIZ		Number of longwords of stack elements
C				to decide the stack allocation
C
C	Control variables
C
C	STACK_CONTRL		Control variable. 0 - stack in memory
C				                  n>0 stack on disk.
C	STACK_ACDEEP		Actual deep of the stack
C	STACK_TOPPTR		Pointer to the top of each stack
C	STACK_ELMLEN		Actual number of longwords of elements in stack
C	STACK_LSTLEN		Number of longwords in the last record of the
C				elements in stack
C	STACK_RECNUM		Actual number of records used by each element
C	STACK_ADDRES		Address of stack in memory
C
	INTEGER*4		STACK_MAXNUM
	INTEGER*4		STACK_DEPDEF
	INTEGER*4		STACK_RECDEF
	INTEGER*4		STACK_MAXSIZ
	PARAMETER		(STACK_MAXNUM=  16)
	PARAMETER		(STACK_DEPDEF=   8)
	PARAMETER		(STACK_RECDEF=1024)
	PARAMETER		(STACK_MAXSIZ=  32)
C
	INTEGER*4		STACK_CONTRL(STACK_MAXNUM)
	INTEGER*4		STACK_ACDEEP(STACK_MAXNUM)
	INTEGER*4		STACK_TOPPTR(STACK_MAXNUM)
	INTEGER*4		STACK_ELMLEN(STACK_MAXNUM)
	INTEGER*4		STACK_LSTLEN(STACK_MAXNUM)
	INTEGER*4		STACK_RECNUM(STACK_MAXNUM)
	INTEGER*4		STACK_ADDRES(STACK_MAXNUM)
	COMMON/STACK_COMMON/	STACK_CONTRL,
     .				STACK_ACDEEP,
     .				STACK_TOPPTR,
     .				STACK_ELMLEN,
     .				STACK_LSTLEN,
     .				STACK_RECNUM,
     .				STACK_ADDRES
C
C       temporary error variables
C
	INTEGER*4		ERR_STKILL/-100/
	INTEGER*4		ERR_STKCRE/-101/
	INTEGER*4		ERR_STKDEL/-102/
	INTEGER*4		ERR_STKWRT/-103/
	INTEGER*4		ERR_STKEMP/-104/
	INTEGER*4		ERR_STKREA/-105/
	INTEGER*4		ERR_STKMEM/-106/
	INTEGER*4		ERR_STKOVF/-107/
	INTEGER*4		ERR_STKOPC/-108/
	INTEGER*4		ERR_NORMAL/  0/
	REAL*4			TBL_NULVAL/'FFFF7FFF'X/
	REAL*4			TBL_TRUE  /1.0/
	REAL*4			TBL_FALSE /0.0/
C
C------------------------------------------------------------------------------
C
