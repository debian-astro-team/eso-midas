/* @(#)pgm.h	19.1 (ESO-IPG) 02/25/03 13:49:38 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		pgm.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO]
.CATEGORY	Macros for Main routines (main parameters)
.ENVIRONMENT	

.COMMENTS	There are several ways to get the parameters of a main 
		procedure:
		1) char *NextParm() move in the vector of parameters
		2) char *GetParmString() find a named parameter,
				e.g. GetParmString("x=")

.VERSION 1.0	20-Feb-1986  Creation by extraction from STESODEF
.VERSION 1.1 	06-Nov-1986  Inclusion of parameter functions (mp_...)
.VERSION 2.0 	01-Jun-1988  Added GetsOption(letter) (eg -Ofile)
.VERSION 2.1 	22-Jun-1989  Added SetParm
.VERSION 2.2 	08-Dec-1989  Added GetParmString, GetParmInt, etc...

------------------------------------------------------------*/

#ifndef PGM_DEF
#define PGM_DEF		0

#ifndef _TEMPLATES_
#include <compiler.h>
#endif

#define PGM(name)	int main(argc,argv,envp)    /* name Main Program */ \
			int argc; char **argv,**envp; /* Main routine */

#if _TEMPLATES_
/*===========================================================================
 *		Function Templates
 *===========================================================================*/
int 	 mp_save	(int argc, char **argv, char **envp);
int 	 mp_reset	(void);
char 	*mp_get		(int arg_number);
char 	*mp_next	(void);
char 	*mp_anext	(void);
char	*mp_flagged	(char flag_char);	/* Example: '<'		*/
int	 mp_option	(char option_char);	/* Example: -t80	*/
char 	*mp_poption	(char option_flag);	/* Example: -iInput	*/
char 	*mp_gs		(char *start_string);	/* Example: name=value	*/
long	 mp_gl		(char *start_string);	/* Example: name=number	*/
/* double mp_gf		(char *start_string);	/* Example: name=number	*/

#else
char 	*mp_get(), *mp_next(), *mp_anext(), *mp_poption(), *mp_gs(), 
	*mp_flagged();
long	mp_gl();
/* double	mp_gf(); */
#endif

#define SaveParm(n,p1,p2)	mp_save(n,p1,p2)	/* Save main params */
#define SaveParms()		SaveParm(argc,argv,envp)/* Save main params */
#define ResetParms()		mp_reset()
#define GetParm(n)		mp_get(n)
#define GetProgramFile()	mp_get(0)
#define NextParm()		mp_anext()
#define GetNextParm()		mp_next()
#define GetFlaggedParm(x)	mp_flagged(x)
#define GetInputFile()		mp_flagged('<')
#define GetOutputFile()		mp_flagged('>')
#define GetTerminalName()	mp_flagged('^')
#define GetOption(x)		mp_option(x)
#define GetsOption(x)		mp_poption(x)

#define GetParmString(s)	mp_gs(s)
#define GetParmInteger(s)	mp_gl(s)
#define GetParmFloat(s)		mp_gf(s)

#endif
