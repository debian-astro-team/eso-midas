/* @(#)twdef.h	19.1 (ESO-IPG) 02/25/03 13:49:44 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		twdef.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein, Guido Russo
.CATEGORY	Terminal Definitions
.COMMENTS	Only definitions about cursor mouvements and screen
		attributes are here.
.ENVIRONMENT	TermWindows

.VERSION 1.0	Extracted from TERMINAL header file on 17-Nov-1986
.VERSION 1.1	03-Dec-1986: Added terminal-independant graphic chars 
.VERSION 2.0	01-Jul-1987: Redefined symbols for Version 2.
.VERSION 2.1	07-Jun-1989: Added Symbols _ARROW  _PF  _FK  _KEYPAD
.VERSION 2.2	03-Dec-1990: Modified TW_cc_USER + Cosmetic
------------------------------------------------------------*/

#ifndef TWDEF_DEF
#define TWDEF_DEF	0

/*=======================================================================
		Definitions of Keyboard
 *=======================================================================*/

#define _STANDARD_	1		/* standard ASCII	*/
#define _ARROW_		2		/* Arrow keys		*/
#define _KEYPAD_	3		/* Keypad symbols	*/
#define _PF_		4		/* PF keys		*/
#define _FK_		5		/* Other Function keys	*/
#define _EOF_		(-1)
#define _INTERRUPT_	(-2)

#define _ARROW(k)	(_ARROW_<<8)|k	/* Arrow keys		*/
#define _KEYPAD(k)	(_KEYPAD_<<8)|k	/* Keypad symbols	*/
#define _PF(k)		(_PF_<<8)|k	/* PF keys		*/
#define _FK(k)		(_FK_<<8)|k	/* Other Function keys	*/

#define is_CONTROL(k)	(k&(~0xff))== 0	/* Control keys		*/
#define is_ARROW(k)	(k>>8)==_ARROW_	/* Arrow keys		*/
#define is_KEYPAD(k)	(k>>8)==_KEYPAD_/* Keypad symbols	*/
#define is_PF(k)	(k>>8)==_PF_	/* PF keys		*/
#define is_FK(k)	(k>>8)==_FK_	/* Other Function keys	*/

/*=======================================================================
		Definitions of Screen Rendition Attributes
 *=======================================================================*/

#define	_NORMAL_	0x00	
#define _BOLD_		0x01	
#define _UNDERSCORE_	0x02	
#define	_BLINK_		0x04	
#define	_REVERSE_	0x08	
#define	_GRAPHICS_	0x10	

#define _ATTRIBUTE_	0x80	/* Attribute of home character for subwindows */
	
/*=======================================================================
		Options at Window Opening
 *=======================================================================*/

#define _BORDER_	_GRAPHICS_
#define	_SCROLL_	0x20	/* Scrolling Mode			*/
#define	_FULL_		0x40	/* Full Mode				*/
#define	_DISPLAY_	0x80	/* Display-only Window			*/
#define	_TITLE_		0x100	/* Add title on Window			*/
#define _BORDER2_	0x200	/* Blank left & right column		*/


/*=======================================================================
		Cursor moving or clearing definitions			
 *=======================================================================*/

#define _UP_		0		/* Up    arrow		*/
#define _DOWN_		1		/* Down  arrow		*/
#define _VERTICAL_	0		/* Up/Down >> 1		*/

#define _LEFT_		2		/* Left  arrow		*/
#define _RIGHT_		3		/* Right arrow		*/
#define _HORIZONTAL_	1		/* Left/Right >> 1	*/

#define _UPLEFT_	0
#define _DOWNLEFT_	1
#define _UPRIGHT_	3
#define _DOWNRIGHT_	2

#define _NEWLINE_	4		/* Go to next line	*/
#define _WHOLE_		4		/* Whole screen		*/
#define _HOME_		4		/* Home position	*/
#define _CENTER_	4		/* Centering option	*/


/*=======================================================================
		Definitions of Control Actions
 *=======================================================================*/

#define TW_cc_VMS	 1	/* VMS-like control characters 	*/
#define TW_cc_UNIX	 2	/* UNIX-like control characters	*/
#define TW_cc_USER	 3	
#define TW_cc_SOCKET	 4
#define TW_cc_SET	 0x80	/* Take previous set of Ctrl ch */

#define TW_cc_NONE	0
#define TW_cc_INT	1	/* Interrupt 	*/
#define TW_cc_QUIT	2	/* Quit or Stop	*/
#define TW_cc_EOF	3	/* End of File	*/
#define TW_cc_HELP	4	/* Help Window	*/
#define TW_cc_REFR	5	/* Refresh	*/
#define TW_cc_MODE	6	/* Switch Insert/Replace */
#define TW_cc_RAW	7	/* Escape (next char wihout interpretation) */
#define TW_cc_RET	8	/* Return key	*/
#define TW_cc_UP	(9+_UP_)	/* Move Up	*/
#define TW_cc_DOWN	(9+_DOWN_)	/* Move Down	*/
#define TW_cc_LEFT	(9+_LEFT_)	/* Move Left	*/
#define TW_cc_RIGHT	(9+_RIGHT_)	/* Move Right	*/
#define TW_cc_HOME	(9+_HOME_)	/* Move Left Margin	*/
#define TW_cc_EOL	14		/* Move to End of Line	*/
#define TW_cc_NW	15		/* Move Next Word	*/
#define TW_cc_DELC	16		/* Delete Previous Char	*/
#define TW_cc_DELW	17		/* Delete Previous Word	*/
#define TW_cc_DELE	18		/* Delete to End of Line*/
#define TW_cc_DELL	19		/* Delete Line		*/
#define TW_cc_max	19		/* Maximal		*/

/*=======================================================================
		Definitions of ACHAR's 
 *=======================================================================*/

#define ACHAR		unsigned short /* attribute + char	*/
#define NULL_ACHAR	(ACHAR *)0

/*=======================================================================
		Definitions of Graphic Characters
 *=======================================================================*/

#define _CORNER_	2		/* Pos. of first corner */	
#define _CROSS_		4		/* Intersection of 2 lines */

#define _TEE_		7		/* 7=up, 8=down, etc	*/
#define _RUBBISH_	11		/* Normally filled squa	*/

#define GraphicChar(x)		('a' + (x))
#define CornerChar(x)		('a' + _CORNER_ + (x))
#define RuleChar(x)		('a' + (x))
#define TeeChar(x)		('a' + _TEE_ + (x))
#define RubbishChar()		('a' + _RUBBISH_)

#define Agraphic		(_GRAPHICS_<<8)
#define isGraphic(a)		((a)&Agraphic)
#define GraphicAchar(x)		(GraphicChar(x)|Agraphic)
#define RubbishAchar()		(GraphicAchar(RubbishChar()))
#define ACHARtoc(a)		((a)&0177)

/*=======================================================================
			Some Synonyms
 *=======================================================================*/

#if 0
#define	NORMAL		_NORMAL_	
#define BOLD		_BOLD_	
#define BRIGHT		_BOLD_	
#define UNDER		_UNDERSCORE_	
#define	BLINK		_BLINK_	
#define	INVERSE		_REVERSE_	
#define	REVERSE		_REVERSE_
#define	GRAPHICS	_GRAPHICS_

#define	v_bold		_BOLD_
#define	v_underscore	_UNDERSCORE_
#define	v_blink		_BLINK_
#define	v_inverse	_REVERSE_
#define	v_graphics	_GRAPHICS_
#endif

#endif
