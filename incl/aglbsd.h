/* @(#)aglbsd.h	19.1 (OAA-ASTRONET) 02/25/03 13:49:23 */
/*
 * HEADER : aglbsd.h       - Vers 3.6.001  - Jul 1992 -  L. Fini, OAA
 *                                                    -  C. Guirao. ESO-IPG
 *
 *
 * This file contains standard includes needed by AGL, suitable
 * for any Unix BSD system
 *
 * C.Guirao: Some changes in the names of include files to make it more
 *           pure BSD.
 *
 */

#define UNIX                 /* Define this system as Unix                  */
                             /*    Standard libraries                       */

#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sgtty.h> 
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/un.h>

#ifndef MIDAS			/* Midas ha his own mechanism to do that    */

#include <strings.h>
#define strchr(a,b)	index(a,b)
#define strrchr(a,b)	rindex(a,b)

#endif
