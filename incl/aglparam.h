/* @(#)aglparam.h	19.1  (OAA-ASTRONET) 02/25/03 13:49:24   */
/*
 * HEADER : aglparam.h     - Vers 3.6.001  - Aug 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 */




/*********************************           *********************************/
/*                       CONFIGURATION PARAMETERS                            */

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define UPNIB  0xf0
#define LOWNIB 0x0f

#define BLANK ' '                      /* Blank character                    */

#define ON 1                           /* Generic status ON                  */
#define OFF 0                          /* Generic status OFF                 */

#define X_AXIS 100
#define Y_AXIS 101

#define CRSUNDEF 99                    /* Cursor undefined flag              */

#define VWPEMPTY (-1)                  /* Viewport slot free flag            */

#define NFUNCTS 8                      /*  Number of driver entry points     */

                                       /*  Error severity tresholds          */
#define AG_INFO    0
#define AG_WARNING 1
#define AG_ERROR   2
#define AG_SEVERE  3


#define NFONTS      11           /* 0 to 9 are character fonts.             */
#define SYMBFONT    10           /* 10 is reserved for symbol generator     */

				/* Metafile function codes                 */

enum METACODE { MFILLC=0,  MFCDEF=2, MFWDEF, MFSSET, MFGTXT, 
		MFVERS,    MFGPLL,   MFGPLM, MFESC,  MFGINT, 
		MFEXTN=128 
};

				/* Metafile status codes                     */
#define  NOMETAFILE   0
#define  SOFTMETAFILE 1
#define  HARDMETAFILE 2

#define ERRSTACKLNG 10     /* Error stack length                            */
#define ERRNAMLNG 8        /* Max AGL module name length                    */
#define MAXKEYLNG 31       /* Max command keyword length in AGLSET          */


				/********************************************/
				/* Character generator related poarameters  */
#define MAX_STROKES    20	/* max number of strokes to draw characters */
#define CHARPOLYBUFLNG 100	/* Max total number of coordinates in the   */
				/*     description of any character         */


#define AUXPOLYBUFLNG  400	/* Auxiliary polyline max lenght            */
				/* DO NOT MAKE IT BIGGER THAN 400 !!!       */

#define MFHDRLEN 21                             /* DO NOT MODIFY !!!!!!!    */
#define MFHEAD   "AGL standard metafile"        /* DO NOT MODIFY !!!!!!!    */

#define MAXLSTYL 	5

#define AGLCOLORS	9

#define FOREGROUND 1
#define BACKGROUND 0

#define MAX_INT_US    10	/* Max number of user defined integer params */
#define MAX_FLOAT_US  10	/* Max number of user defined float params   */

#define CURRDRV_VERS	36	/* Current driver version                    */

#define NCHLEVELS 	10	/* Max number of nested metachar. sequences  */


#define AGL_INIT	0x01	/* Wasinit flag mask: AGL_init called        */
#define AGL_BKGPRESET	0x02	/* Wasinit flag mask: background color prese*/

