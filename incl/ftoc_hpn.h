/* @(#)ftoc_hpn.h	19.1 (ESO-IPG) 02/25/03 13:49:32 */
/*++++++++++++++
.IDENTIFICATION ftoc_hpn.h
.LANGUAGE       C
.AUTHOR         Francois Ochsenbein
.ENVIRONMENT    HP9000s800 . Risc CPU. (Tested on a HP720)
.KEYWORDS       
.VERSION  1.0   10-Sep-1990
.VERSION  2.0   17-Mar-1992 CG. 
.COMMENTS       The length of fortran strings come 
		FOLLOWING the other arguments.
---------------*/

#ifndef  FTOC_DEF		/* Avoid redefinitions */
#define  FTOC_DEF	0

typedef int fint2c;             /* Fortran INT passed to C */
typedef int flong2c;            /* Fortran LONG passed to C */

#include <stdarg.h>
char *ftoc_get();

	/* In the following definitions:
	   p  is the positional argument number (starting from 1)
	   a  is the total number of FORTRAN arguments
	   s  counts how many CHARACTER arguments exist before p
	   t  is the data type

	   Example: if s' are CHARACTER arguments in the calling sequence
	   CALL SSP(a,b,s1,d,s2,s3)
	   ==> a  is defined by (p=1, a=6, s=0)
	   ==> b  is defined by (p=2, a=6, s=0)
	   ==> s1 is defined by (p=3, a=6, s=0)
	   ==> d  is defined by (p=4, a=6, s=1)
	   ==> s2 is defined by (p=5, a=6, s=1)
	   ==> s3 is defined by (p=6, a=6, s=2)
	 */

#define PARAM(p,a,s,t) 		(((t *)(Cargs))[-(p+s)])
#define CHAR_LOC(p,a,s)    	PARAM(p,a,s,char *)
#define CHAR_LEN(p,a,s)    	PARAM(p,a,s+1,int)

#define C_STRING(p,a,s)		ftoc_get(CHAR_LOC(p,a,s),CHAR_LEN(p,a,s),0)
#define STRIPPED_STRING(p,a,s) 	ftoc_get(CHAR_LOC(p,a,s),CHAR_LEN(p,a,s),1)
#define STRFCOPY(p,a,s,cs) 	ftoc_cpy(CHAR_LOC(p,a,s),cs,CHAR_LEN(p,a,s))

#endif
