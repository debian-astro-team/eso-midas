/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE       C
.IDENTIFICATION tblsys.h
.AUTHOR         J.D.Ponz, F. Ochsenbein [ESO-IPG]
.KEYWORDS       Table control definitions
.ENVIRONMENT    Independent
.VERSION   1.0  01-Jun-1990:
.VERSION   1.1  28-Oct-1990: Added NULL for floating-point (~0)
.VERSION   1.2  18-Dec-1990: Added number of selected rows in TBL_FCT structure
.VERSION   1.3  24-Feb-1991: Use standard lowercase for variables,
				uppercase for constants.
.VERSION   1.4  29-Apr-1991: Other variables used for EIO mode for optimisation
.VERSION   1.5  10-Jun-1991: ColumnWidth macro modified
.VERSION   1.6  22-Sep-1992: remove defintion of NULL1 NULL2 NULL4
.VERSION   1.7  15-Oct-1993: long -> int. CG.

 040726		last modif  KB

.COMMENTS	
		This file should not be included in applications programs;
		it is required for compilation of TBL / TC routines only.
------------------------------------------------------------------------*/

#ifndef	TBL_VERSION
#define	TBL_VERSION	1		/* Version with Arrays =========*/

#include <tbldef.h>			/* Symbols used for Tables	*/

		/* Configuration Parameters: */
		/* The TBL_LABCASE switch is 1 for case sensitivity	*/

#define TBL_NUMBER   	16   /* maximum number of tables currently open */
#define COLS_MAX	128  /* Maximum number of columns 		*/
#define SORT_MAX	8			/* For Sorting (TCC)	*/
#define TBL_LABCASE	0			/* No case sensitivity	*/

		/* The TBL_EIO_LIMIT means: use EIO if size > TBL_EIO_LIMIT.
		   It's possible to overrule defaults with the mode
		   parameter in TCTOPN / TCTINI				*/

/*	the following was used until 010612
#define TBL_EIO_LIMIT	(4<<20)			 4Mb 
#define TBL_TBUFS	10			 How many buffers 
*/

#define TBL_EIO_LIMIT	(16<<20)			/* 16Mb	*/
#define TBL_TBUFS	20			/* How many buffers 	*/
		   
		/* Null values for Integers (Floating NULLS in tz0.c)	*/

		/* The Datatypes for Tables are built as
			(element << TBL_D_BITS) + items.
		   Note that the datatype is made of 5 bits abcde where
     			=> a=1 for Floating-Point
			=> b=1 for Signed Numbers
			=> (cde) gives the length as log2(length)
		*/
		   

#define TBL_D_BITS	24
#define TBL_D_MASK	((1<<TBL_D_BITS)-1)

#define TBL_D_I1	010
#define TBL_D_I2	011
#define TBL_D_I4	012

#define TBL_D_A1	000	/* Unsigned	*/
#define TBL_D_A2	001
#define TBL_D_A4	002

#define TBL_D_R4	032
#define TBL_D_R8	033

#define TBL_ElementType(t)	(t>>TBL_D_BITS)
#define TBL_ElementSize(t)	(1<< (TBL_ElementType(t) & 3))
#define TBL_Items(t)		(t & TBL_D_MASK)
#define TBL_Bytes(t) 		(TBL_Items(t) << (TBL_ElementType(t)&7))

		/* The size of buffers is defined here. Unit is 8k	*/

#define TBL_LOG2BUF	13			/* 8k = 2**13 ...	*/
#define TBL_BUFSIZE	(1<<TBL_LOG2BUF)	/* Size in Bytes	*/
#define TBL_BUFITEMS	(1<<(TBL_LOG2BUF-2))	/* Size in Float	*/

		/* Descriptor names		*/

#define	TBL_Dmain	"TBLCONTR"		/* Main definitions	*/
#define	TBL_Dmain_SIZE	 10		
#define	TBL_Ddtypes	"TBLENGTH"		/* Datatypes descriptor	*/
#define	TBL_Doffset	"TBLOFFST"		/* Label descriptors	*/
#define	TBL_Dlabel	"TLABL"			/* Label descriptors	*/
#define	TBL_Dselect	"TSELTABL"		/* Selection descriptor	*/
#define	TBL_Dselect_SIZE 64		
#define	TBL_Dview	"TVIEWTBL"		/* View name		*/
#define	TBL_Dview_SIZE	 64

#define TBL_tmpname	"MIDDUMA.tbl"		/* Temporary Name	*/

		/* In EIO mode, data use TBL_BUF's	*/
struct TBL_BUF {
	int	offset;		/* Starting point of file loaded (word)	*/
	int	size;		/* How many words allocated in buffer	*/
	int	used;		/* How many words used in buffer	*/
	int	flags;		/* If modified (see tflags below)	*/
	unsigned int	iocount;
	int	*buf;		/* Actual data				*/
   };
typedef struct TBL_BUF TABLE_BUF;

		/* Structure that describes one Table.
		   The # comment tells that the variable is
		   stored in the TBL_Dmain descriptor.
		*/

struct TBL_FCT {
	char *phname;	/* physical filename, as given in Open 	*/
	char *usname;	/* users filename (alias name, unused) 	*/
	int acols; 	/* number of words allocated for columns -- #0 */
	int arows; 	/* number of allocated rows 		 -- #1 */
	int cols;	/* actual number of columns in the table -- #2 */
	int rows;	/* actual number of rows in the table    -- #3 */
	int kcol; 	/* Reference Column			 -- #4 */
	int scol;	/* sorted column number  		 -- #5 */
	int swise;	/* physical table format on disk  	 -- #6 */
	int version;	/* Table Version			 -- #7 */
	int colitems;	/* Items reserved for column descriptors -- #8 */
	int selected;	/* How many rows are selected            -- #9 */
	int *dtypes;	/* Array of datatypes for each column #TBLENGTH	*/ 
	int *offset;	/* Array of byte offsets each column  #TBLOFFST	*/
	int *bytes;	/* Array of byte lengths for each column	*/
	int *abytes;	/* Array of ascii lengths (1 edited element)	*/
	char *label;	/* Array of labels,  each of (1+TBL_LABLEN)	*/
	char *format;	/* Array of formats, each of (1+TBL_FORLEN)	*/
	char *addres;	/* table address (row #1, col #0)		*/
	int wsize; 	/* Size in words = (acols+1)*arows		*/
	short tflags;	/* control flags				*/
#define TBL__MODIFIED	0x01			/* Table was modified	*/
#define TBL__MAPPED	0x02			/* Mapped Table		*/
#define TBL__READONLY	0x04			/* Don't modify		*/
#define TBL__EIO	0x08			/* Elementary i/o mode	*/
#define TBL__VIRTUAL	0x10			/* Virtual (no data)	*/
#define TBL__PROTOTYPE	0x20			/* Never Remove		*/
#define TBL__VREADONLY	0x40			/* View Read-Only	*/
#define TBL__KEEPVERS	0x4000			/* To force version num	*/
#define TBL__BADNULLS	0x8000			/* To force change 0->1	*/
	char select;	/* Selection flag:
			   '\1' for All, 
			   zero if unknown	*/
	char intlFITS;	/* = 'F' if internal FITS table, else zero */
	int reclen;	/* Record length (bytes)		*/
	int imno;	/* File number for SC Interfaces	*/
	unsigned char *loaded_map;	/* 8k / bit, 64K/byte	*/
	unsigned char *modified_map;	/* 8k / bit, 64K/byte	*/
	short	nbuf, cbuf;		/* === In Eio mode=====	*/
	int	sbuf;			/*  	Total buf size	*/
	int	iocount;
	TABLE_BUF *tbuf;
	int	 vno;			/* === View Info ====== */
	int	 vsize;
	unsigned char *vsel;
  };
typedef struct TBL_FCT	TABLE;

		/* Special Functions (those that don't return status)	*/

/* CG. 14.07.94. These functions are already defined on proto_tbl.h */
/*char *TBL_Dlab();*/	/* Edit the TBLLBLxxx descriptor	*/
/*char *TBL_Cfmt();*/	/* Convert F77 format to C		*/
/*char *TBL_ssave();*/	/* Save a string to a new memory	*/
/*char *TBL_RD();*/		/* Read at offset			*/
/*char *TBL_RDF();*/	/* Read and flag modified at offset	*/
/*TABLE *TBL_ptr();*/	/* Converts tid into pointer to TABLE	*/

/*int TBL_offset();*/	/* Compute offset position (bytes)	*/
/*int TBL_SSC(), TBL_SSI1(), TBL_SSI2(), TBL_SSI4(), TBL_SSR(), TBL_SSD(); */
/*int TBL_BSC(), TBL_BSI1(), TBL_BSI2(), TBL_BSI4(), TBL_BSR(), TBL_BSD(); */

		/* Frequently used op's	*/

#define CheckTable(tp)		(tp ? 	ERR_NORMAL : ERR_TBLENT )

#define CheckColumn(tp,c)	((c < 0) || (c > tp->cols) ? 	\
					ERR_TBLCOL : ERR_NORMAL)

#define CheckTrueColumn(tp,c)	((c < 1) || (c > tp->cols) ? 	\
					ERR_TBLCOL : ERR_NORMAL)

#define CheckRow(tp,row)	((row < 1) || (row > tp->arows) ?	\
					ERR_TBLROW : ERR_NORMAL)

#define CheckTrueRow(tp,row)	((row < 1) || (row > tp->rows) ?	\
					ERR_TBLROW : ERR_NORMAL)

#define ColumnType(tp,c)	(tp)->dtypes[(c)-1]
#define ColumnWidth(tp,c)	(c == 0 ? sizeof(int) : (tp)->bytes[(c)-1])
#define ColumnOffset(tp,c)	(tp)->offset[(c)-1]
#define ColumnFormat(tp,c)	((tp)->format + (c-1)*(1+TBL_FORLEN))
#define ColumnLabel(tp,c)	((tp)->label  + (c-1)*(1+TBL_LABLEN))
#define ColumnEditedWidth(tp,c)	(tp)->abytes[(c)-1] 

#endif
