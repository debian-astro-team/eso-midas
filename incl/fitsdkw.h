/* @(#)fitsdkw.h	19.2 (ESO-IPG) 05/05/03 08:56:09 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c)  1992   European Southern Observatory
.LANGUAGE     C
.IDENT        fitsdkw.h
.AUTHOR       P.Grosbol   ESO/IPG
.KEYWORDS     FITS, keywords, MIDAS descriptors
.PURPOSE      define mapping between MIDAS descriptors and
              FITS keywords when writting a FITS header
.VERSION      1.0   1988-Nov-23 :  Creation,  PJG
.VERSION      1.1   1989-Jun-13 :  Add ROTA keyword,  PJG
.VERSION      1.2   1990-Feb-02 :  Modify for hierachical keyword, PJG
.VERSION      1.3   1990-Nov-07 :  Include more basic keywords, PJG
.VERSION      1.4   1991-Feb-18 :  Add FILENAME and DATANULL, PJG
.VERSION      1.5   1991-Mar-24 :  Add TBLOFFST + ESO_LOG, PJG
.VERSION      1.6   1992-Jun-05 :  Map also O_POS+O_TIME etc., PJG
.VERSION      1.7   1992-Sep-10 :  Map also O_AIRM+AIRMASS, PJG

 030502		last modif

--------------------------------------------------------------------*/

#define     NCTL         0         /* no mapping action             */
#define     LCTL         1         /* line feed control             */
#define     CCTL         2         /* Comment/history control       */
#define     SCTL         3         /* skip trailing space control   */
#define     HCTL         4         /* hierarchical keyword control  */
#define     DCTL         5         /* Date keyword control          */
#define     ZCTL         6         /* Zero control                  */
#define     TCTL         7         /* Time control - hour->sec      */

typedef struct {                   /* keyword name for descriptor   */
                 int           no; /* element no. in descriptor     */
                 char         *kw; /* prime level keyword name      */
                 int          ctl; /* store control type            */
               } DTOKW;

typedef struct {                   /* descriptor to keyword map     */
                 char       *desc; /* MIDAS descriotor name         */
                 DTOKW       *dtk; /* pointer to keyword name       */
               } DKMAP;

static  DTOKW   idtkw[] = { {-1,"OBJECT" , SCTL}, { 0,"",        NCTL} };
static  DTOKW   hiskw[] = { {-1,"HISTORY", CCTL}, { 0,"",        NCTL} };
static  DTOKW   comkw[] = { {-1,"COMMENT", CCTL}, { 0,"",        NCTL} };
static  DTOKW   esokw[] = { {-1,"ESO-LOG", CCTL}, { 0,"",        NCTL} };
static  DTOKW   hiekw[] = { {-1,"HIERARCH",CCTL}, { 0,"",        NCTL} };
static  DTOKW   inskw[] = { {-1,"INSTRUME",SCTL}, { 0,"",        NCTL} };
static  DTOKW   telkw[] = { {-1,"TELESCOP",SCTL}, { 0,"",        NCTL} };
static  DTOKW   autkw[] = { {-1,"AUTHOR",  SCTL}, { 0,"",        NCTL} };
static  DTOKW   refkw[] = { {-1,"REFERENC",SCTL}, { 0,"",        NCTL} };
static  DTOKW   radkw[] = { {-1,"RADECSYS",SCTL}, { 0,"",        NCTL} };
static  DTOKW   obskw[] = { {-1,"OBSERVER",SCTL}, { 0,"",        NCTL} };
static  DTOKW   airkw[] = { { 1,"AIRMASS", ZCTL}, { 0,"",        NCTL} };
static  DTOKW   expkw[] = { { 1,"EXPTIME", NCTL}, { 0,"",        NCTL} };
static  DTOKW   rakw[]  = { { 1,"RA",      NCTL}, { 0,"",        NCTL} };
static  DTOKW   deckw[] = { { 1,"DEC",     NCTL}, { 0,"",        NCTL} };
static  DTOKW   equkw[] = { { 1,"EQUINOX", ZCTL}, { 0,"",        NCTL} };
static  DTOKW   poskw[] = { { 1,"RA",      NCTL}, { 2,"DEC",     NCTL},
                            { 3,"EQUINOX", ZCTL}, { 0,"",        NCTL} };
static  DTOKW   timkw[] = { { 1,"DATE-OBS",DCTL}, { 4,"MJD-OBS", ZCTL},
                            { 5,"TM-START",TCTL}, { 7,"EXPTIME", NCTL}, 
                            { 0,"",        NCTL} };
static  DTOKW   oamkw[] = { { 1,"AIRMASS", ZCTL}, { 0,"",        NCTL} };

static  DKMAP  dkm[] = {
         {"IDENT"   , idtkw},
	 {"HISTORY" , hiskw},
	 {"COMMENT" , comkw},
	 {"ESO_LOG" , esokw},
	 {"EXPTIME" , expkw},
	 {"INSTRUME", inskw},
	 {"TELESCOP", telkw},
	 {"AIRMASS" , airkw},
	 {"OBSERVER", obskw},
	 {"HIERARCH", hiekw},
	 {"AUTHOR"  , autkw},
	 {"REFERENC", refkw},
	 {"RADECSYS", radkw},
	 {"RA"      ,  rakw},
	 {"DEC"     , deckw},
	 {"EQUINOX" , equkw},
	 {"O_POS"   , poskw},
	 {"O_TIME"  , timkw},
	 {"O_AIRM"  , oamkw},
         {"NPIX"    , (DTOKW *) 0},
         {"NAXIS"   , (DTOKW *) 0},
         {"CUNIT"   , (DTOKW *) 0},
         {"CRVAL"   , (DTOKW *) 0},
         {"REFPIX"  , (DTOKW *) 0},
         {"ROTA"    , (DTOKW *) 0},
         {"STEP"    , (DTOKW *) 0},
         {"START"   , (DTOKW *) 0},
         {"FILENAME", (DTOKW *) 0},
         {"DATE"    , (DTOKW *) 0},
         {"ORIGIN"  , (DTOKW *) 0},
         {"DATANULL", (DTOKW *) 0},
         {"TBLENGTH", (DTOKW *) 0},
         {"TBLCONTR", (DTOKW *) 0},
         {"TSELTABL", (DTOKW *) 0},
         {"TBLOFFST", (DTOKW *) 0},
         {(char *) 0,(DTOKW *) 0} };
