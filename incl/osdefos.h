/* @(#)osdefos.h	19.1 (ESO-IPG) 02/25/03 13:49:36 */
/*+++++++++++++++++++++
.TYPE                Header
.LANGUAGE            C
.IDENTIFICATION      osdefos.h
.AUTHOR              BP  [ESO-IPG], Francois Ochsenbein
.KEYWORDS            operating system, parameters, fortran compilers
.ENVIRONMENT    VMS  ULTRIX  MSDOS  SYSTEM_FIVE  SYSV_V2  BSD
.COMMENTS            This modules defines the OS #def's, and an option
			SW_LEVEL set to zero for minimizing
			the size of the generated programs.
		This version includes #def's keywords
		MID_OS_VMS  MID_OS_ULTRIX  MID_OS_MSDOS MID_OS_SYSV  
		MID_OS_SYSV_V2  MID_OS_BSD
		and the memory classes MID_EXTERN, etc...
.VERSION 1.0	     10-Mar-1987  Also set defines for standard calls BP
.VERSION 1.1	     11-Apr-1990  MID_EXTERN is defined as extern for UNIX. CG
--------------------------------------------------------------------------*/

#ifndef OSDEFOS_DEF
#define OSDEFOS_DEF	0

/*===========================================================================
 *             List of supported Operating Systems
 *===========================================================================*/

#define _VMS_		1
#define _ULTRIX_	2
#define _MSDOS_		3
#define _BSD_		4
#define _SYSV_		5
#define _SYSV_V2_	6
#define __POSIX__	7

#define MID_OS_VMS	(MID_OS_ENV == _VMS_)
#define MID_OS_ULTRIX	(MID_OS_ENV == _ULTRIX_)
#define MID_OS_MSDOS	(MID_OS_ENV == _MSDOS_)
#define MID_OS_BSD	(MID_OS_ENV == _BSD_)
#define MID_OS_SYSV	(MID_OS_ENV == _SYSV_)
#define MID_OS_SYSV_V2	(MID_OS_ENV == _SYSV_V2_)
#define MID_OS_POSIX	(MID_OS_ENV == __POSIX__)


/*===========================================================================
 *             Define Here Your Specific Implementation
 *===========================================================================*/

#if 0	/* Example of minimal implementation for MicroComputer running MSDOS */
#define SW_LEVEL	0	
#define MID_OS_ENV		_MSDOS_
#endif	/* End of Example */


#ifndef MID_OS_ENV
/*===========================================================================
 *             Definition of Default Operating System
 *===========================================================================*/

#ifdef VMS
#define MID_OS_ENV		_VMS_
#endif

#ifndef MID_OS_ENV
#ifdef vms
#define MID_OS_ENV		_VMS_
#endif
#endif

#ifdef ULTRIX
#define MID_OS_ENV		_ULTRIX_
#endif

#ifdef MSDOS
#define MID_OS_ENV		_MSDOS_
#endif

#ifdef __MSDOS_
#define MID_OS_ENV		_MSDOS_
#endif

#ifdef SYSV_V2
#define MID_OS_ENV		_SYSV_V2_
#endif

#ifdef SYSV
#define MID_OS_ENV		_SYSV_	
#endif

#ifdef BSD
#define MID_OS_ENV		_BSD_	
#endif

#ifdef POSIX
#define MID_OS_ENV		__POSIX__	
#endif

#endif

	/* If no OS variable defined, choose among ULTRIX / SYSV / BSD */
	
#ifndef MID_OS_ENV
#  define MID_OS_ENV	__POSIX__
#endif 


/*===========================================================================
 *             Definition of Memory Classes (related to OS)
 *===========================================================================*/

#ifndef SW_LEVEL
#define SW_LEVEL	9
#endif

#define Rstatic		MID_RSTATIC

#ifdef 	vax11c
#define MID_REGISTER	register
#define MID_STATIC	static noshare		/* standard 		*/
#define MID_RSTATIC	static readonly		/* Added    13-Feb-1986	*/
#define	MID_EXTERN	globalref		/* Mod.  13-Feb-1986	*/
#define MID_GLOBAL	globaldef noshare 	/* Added 13-Feb-1986	*/

#else  /* gcc */
#define MID_REGISTER 	register 
#define MID_STATIC 	static 
#define MID_RSTATIC	static 
#define	MID_EXTERN	extern
#define MID_GLOBAL
#endif

#endif
