/* @(#)atype.h	19.1 (ESO-IPG) 02/25/03 13:49:26 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Header
.NAME		atype.h
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Character Classification
.ENVIRONMENT	ASCII characters
.COMMENTS	System-independent character classification.
		(only for systems using ASCII code!)
		Mnemonics are terminated with the underscore (_) for
		functions requiring a table.
.VERSION 1.0	06-Jun-1986: creation
.VERSION 1.1	23-Jun-1987: Control chars definition
.VERSION 2.0	21-Jan-1988: Inserted as atype
.VERSION 2.1	07-Dec-1989: Added tocntrl
.VERSION 2.2	30-Jan-1990: Added isquote
 
------------------------------------------------------------*/

#ifndef ATYPE_DEF

#define ATYPE_DEF	0

#include <osdefos.h>
#include <compiler.h>

#define   main_tab	8
MID_EXTERN unsigned char main_ascii[256]; /* ASCII attributes table	*/
 
/*===========================================================================
 *		Character Attributes
 *===========================================================================*/

#define _UPPER_		0x01		/* UPPER-case letter A...Z	*/
#define _LOWER_		0x02		/* lower-case letter a...z	*/
#define _DIGIT_		0x04		/* digit             0...9	*/
#define _SPACE_		0x08		/* space: blank Tab CR LF FF	*/
#define _PUNCT_		0x10		/* punctuation			*/
#define _CNTRL_		0x20		/* control character		*/
#define _XDIGIT_	0x40		/* hexa digit			*/
#define _BLANK_		0x80		/* blank			*/

#define _ALPHA_		(_UPPER_|_LOWER_)	/* Alphabetic letter 	*/
#define _ALNUM_		(_ALPHA_|_DIGIT_)	/* Alphanumeric 	*/

#define _GRAPH_ 	(_PUNCT_|_ALNUM_) 	/* Non-blank char 	*/
#define _PRINT_		(_GRAPH_|_BLANK_)	/* Any non-control char	*/

#define _ASCII_ 	(_PRINT_|_CNTRL_)	/* Any valid ASCII char	*/

/*======================================================================
 * 			Test of one char attributes
 *======================================================================*/

#ifdef isupper
#undef isupper
#undef islower
#undef isalpha
#undef isdigit
#ifdef isxdigit
#undef isxdigit
#endif
#undef isalnum
#undef isxdigit
#undef isspace
#undef ispunct
#undef isgraph
#undef isprint
#undef iscntrl
#undef isascii
#endif

#ifdef toupper
#undef toupper
#undef tolower
#endif

#ifdef tocase
#undef tocase
#endif
#ifdef toascii
#undef toascii
#endif



#define ischar_(c,attr,table)	(table[(unsigned char)(c)] & (attr))	 \
				/* test a character for an attribute	*/

#define ischar(c,attr)	(main_ascii[(unsigned char)(c)] & (attr))	 \
			/* test a character for an ASCII attribute	*/

#define isupper(c)	ischar(c,_UPPER_)			 \
			/* test a character is upper-case	*/
#define islower(c)	ischar(c,_LOWER_)			 \
			/* test a character is lower-case	*/
#define isalpha(c)	ischar(c,_ALPHA_)			 \
			/* test a character is alphabetic	*/
#define isdigit(c)	ischar(c,_DIGIT_)			 \
			/* test a character is a digit  	*/
#define isalnum(c)	ischar(c,_ALNUM_)			 \
			/* test a character is alphanemeric	*/
#define isxdigit(c)	ischar(c,_XDIGIT_)			 \
			/* test a character is hexa digit	*/
#define isspace(c)	ischar(c,_SPACE_)			 \
			/* test a character is sp,Tab, CR,LF,FF	*/
#define ispunct(c)	ischar(c,_PUNCT_)			 \
			/* test a character is punctuation	*/
#define isgraph(c)	ischar(c,_GRAPH_)			 \
			/* test a character is valid not blank	*/
#define isprint(c)	ischar(c,_PRINT_)			 \
			/* test a character is not control	*/
#define iscntrl(c)	ischar(c,_CNTRL_)			 \
			/* test a character is a control	*/
#define isascii(c)	ischar(c,_ASCII_)			 \
			/* test a character is valid ASCII	*/

/*======================================================================
 * 			Conversion macros
 *======================================================================*/

#define toupper(c)	(islower(c) ? (c) & 0x5F: (c))		 \
			/* translate a char to uppercase	*/
#define tolower(c)	(isupper(c) ? (c) | 0x20: (c))		 \
			/* translate a char to lowercase	*/
#define tocase(c)	(isalpha(c) ? (c) ^ 0x20: (c))		 \
			/* change case of a character		*/
#define toascii(c)	((c) & 0x7F)				 \
			/* clears parity bit			*/
#define tocntrl(c)	((c == '?') ? 0x7F : (c)&0x1F)		 \
			/* ascii to control			*/
#define toprint(c)	((c == 0x7F) ? '?' : (c)|'@')		 \
			/* control to printable			*/

/* Already defined in proto_os.h */
/*===========================================================================
 *			osc functions
 *===========================================================================*/
/*
#if _TEMPLATES_ 
int oscopy	(char *dest, char *source, int bytes);
int oscopuc	(char *dest, char *source, int len, char stopping);
int oscfill	(char *dest, int len, char fill);
int oscloc	(char *source, int len, char char_to_find);
int oscbloc	(char *source, int len, char char_to_find);
int oscskip	(char *source, int len, char char_to_skip);
int oscbskip	(char *source, int len, char char_to_skip);
int oscscan	(char *source, int len, unsigned char mask, unsigned char *table);
int oscbscan	(char *source, int len, unsigned char mask, unsigned char *table);
int oscspan	(char *source, int len, unsigned char mask, unsigned char *table);
int oscbspan	(char *source, int len, unsigned char mask, unsigned char *table);
int oscomp	(char *str1, char *str2, int len);
int osccomp	(char *str1, char *str2, int len);
int oscindex	(char *source, int source_len, char *substring, int sub_len);
int osctr	(char *dest, char *source, int len, unsigned char *tr_table);
#endif
*/

/* Test a char is valid for an identifier: in body, or first */

#define isid(c)	  	(isalnum(c) || (c == '_') || (c == '$'))
#define isid1(c)  	(isalpha(c) || (c == '_') || (c == '$'))
#define isquote(c)	((c == '\'')|| (c == '"') || (c == '`'))

#endif
