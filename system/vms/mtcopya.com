$ ! @(#)mtcopya.com	19.1 (ESO-IPG) 02/25/03 14:32:20 
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " ******************************************"
$ WRITE SYS$OUTPUT "    Procedure to duplicate a BACKUP tape   "
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT "    Ch. Ounnas   ESO - Garching   840713   "
$ WRITE SYS$OUTPUT "    K. Banse                      860513   "
$ WRITE SYS$OUTPUT " ******************************************"
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " "
$ INQUIRE/NOPUNCT TAPEIN  "Enter the input tape unit  (MTA0/MTA1) : "
$ INQUIRE/NOPUNCT DENSIN  "Enter the density of the input tape    : "
$ INQUIRE/NOPUNCT TAPEOUT "Enter the output tape unit (MTA0/MTA1) : "
$ INQUIRE/NOPUNCT DENSOUT "Enter the density of the output tape   : "
$ MOUN/FOR/BLOCK=8464/DENSITY='DENSOUT' 'TAPEOUT':
$ !
$ BEGIN:
$ N = 2
$ MOUN/FOR/BLOCK=8464/DENSITY='DENSIN 'TAPEIN':
$ !
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " Start of copy input tape on output"
$ !
$ LOOP:
$ !
$ COPY 'TAPEIN': 'TAPEOUT':
$ COPY 'TAPEIN': 'TAPEOUT':
$ COPY 'TAPEIN': 'TAPEOUT':
$ !
$ N = N - 1
$ IF (N .GT. 0) THEN GOTO LOOP
$ !
$ DISM/NOUNLOAD 'TAPEIN':
$ !DISM 'TAPEOUT':
$ !
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " End of copy procedure"
$ WRITE SYS$OUTPUT " "
$ !
$ INQUIRE/NOPUNCT MORE "more tapes (Y/N)"
$ IF MORE .NE. "Y" THEN EXIT
$ !
$ GOTO BEGIN
