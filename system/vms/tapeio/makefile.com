$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.SYSTEM.VMS.TAPEIO]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:25:02 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   dclist.for
$ FORTRAN   iodev.for
$ LINK/NOMAP/EXE=[---.exec] rtape.obj, dclist.obj, iodev.obj
$ LINK/NOMAP/EXE=[---.exec] wtape.obj, dclist.obj, iodev.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
