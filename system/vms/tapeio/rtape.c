/* @(#)rtape.c	19.1 (ES0-DMD) 02/25/03 14:32:27 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c) 1988  European Southern Observatory
.LANGUAGE     C
.IDENT        rtape.c
.AUTHOR       P.Grosbol   ESO/IPG
.KEYWORDS     read tape, copy tape to disk
.ENVIRON      VAX/VMS
.PURPOSE      copy tape to disk files in fix block format
.COMMENT      Fix block file format is used only because it is more
              efficient on a VAX/VMS system!!
              Size could be increased from 512 to 1024 or larger.
.VERSION      1.0  1988-Dec-16 : Creation,   PJG
.VERSION      1.1  1989-Mar-11 : Increase to 1024 byte blocks,   PJG
--------------------------------------------------------------------*/

#define   MBUF              32000   /* max. no. of bytes in record */
#define   BZ                 1024   /* block size on disk          */

static    char         buf[MBUF];       /* I/O buffer              */

main()
{
  int     no,nof,n,rec,nol;
  int     mb,idx,nb,i,nbyte;
  int     fdo,fdi;
  float   byte;
  char    *output,*newfn();
  char    input[80],prefix[80],list[80];

  printf("Name of input tape unit   : "); scanf("%s",input);
  printf("Prefix on output filename : "); scanf("%s",prefix);
  printf("List of file no. to copy  : "); scanf("%s",list);
  if (deflist(list)) {                           /* check if list valid */
     printf("Error: Invalid list of file no's\n");
     ospexit(1);
  }
  fdi = ioopen(input,0,1600);                    /* Open Mag. tape unit */
  if (fdi<=0) ospexit(1);
  iorew(fdi);                                    /* rewind to file no.1 */
  nol = 1;
  while (getlist(&nof)) {                       /* read file one by one */
     if (nol<nof) {                             /* position tape        */
        for (i=nol; i<nof; i++) {
            n = ioread(fdi,buf,BZ);
            if (n<=0) {                        /* error of EOT detected */
               printf("Warning: End of Information detected on tape!\n");
               iorew(fdi,0); ospexit(1);
            }
            iofsf(fdi,1);
        }
      }
      else if (nof<nol) {                       /* wrong order in list  */
         printf("Error: List NOT ascending\n");
         break;
      }

      outname(prefix,nof);                      /* get new file name    */
      output = newfn('I',".mt");

      rec = 0; idx = 0;
      mb = MBUF - BZ;
      for (;;) {                              /* copy block by block    */
          if ((no=ioread(fdi,&buf[idx],mb))<=0) break;
          if (!rec) {
             if ((fdo=creat(output,0
#ifdef vms
                  ,"rfm=fix","mrs=1024"
#endif
                                    ))<0) ospexit(1);
             nbyte = 0;
          }
          if (no<=0) break;
          rec++; nbyte += no; idx += no;
          nb = (idx/BZ) * BZ;
          nb = write(fdo,buf,nb);
          no = idx; idx = 0;
          for (i=nb; i<no; i++) buf[idx++] = buf[i];
      }
      if (no<=0 && rec==0) {                     /* End of Tape mark    */
         printf("Warning: End of Information detected on tape!\n");
         break;
      }
      if (idx) {                        /* write remaining data to disk */
         nb = ((idx-1)/BZ + 1) * BZ; 
         i = write(fdo,buf,nb);
      }
      nol = nof + 1;
      close(fdo);
      byte = nbyte/(1024.0*1024.0);
      printf(" %7.2f Mbyte in %5d records copied to file: %s\n",
               byte,rec,output);
  }
  ioclose(fdi);
  ospexit(0);
}
