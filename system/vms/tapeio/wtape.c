/* @(#)wtape.c	19.1 (ES0-DMD) 02/25/03 14:32:28 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c) 1988  European Southern Observatory
.LANGUAGE     C
.IDENT        wtape.c
.AUTHOR       P.Grosbol   ESO/IPG
.KEYWORDS     copy file to tape, write tape
.ENVIRON      VAX/VMS
.PURPOSE      copy FITS file in byte stream format to mag. tape
.VERSION      1.0  1988-Nov-28 : Creation,   PJG
.VERSION      1.1  1989-Mar-12 : Correct error in block count,   PJG
--------------------------------------------------------------------*/

#define   MBUF              31680   /* max. no. of bytes in record */
#define   FLR                2880   /* FITS logical record length  */

static    char         buf[MBUF];       /* I/O buffer              */

main()
{
  int     no,nof,n,rec,nr,den;
  int     fdi,mb,idx,nb,i,bf;
  int     fdo;
  char    *input,*newfn();
  char    list[80],app[80];
  char    output[80],prefix[80];

  printf("Name of output tape unit   : "); scanf("%s",output);
  printf("Prefix on FITS filename    : "); scanf("%s",prefix);
  printf("List of file no. to copy   : "); scanf("%s",list);
  if (deflist(list)) {                          /* check if valid list  */
     printf("Error: Invalid list specification\n");
     ospexit(1);
  }
  printf("Append file to tape (Y/N)  : "); scanf("%s",app);
  printf("Tape density in bpi        : "); scanf("%d",&den);
  printf("FITS blocking factor (1-10): "); scanf("%d",&bf);

  if (bf<1) bf = 1; else if (10<bf) bf = 10;   /* check blocking factor */
  bf *= FLR;

  fdo = ioopen(output,1,den);                    /* Open Mag. tape unit */
  if (fdi<=0) ospexit(1);

  if (app[0]!='N' && app[0]!='n') {              /* move to end of tape */
     printf("Files will be appended to tape!\n");
     n = ioread(fdo,buf,bf);
     if (n<=0) {                                 /* empty tape          */
        printf("Warning: Tape was empty!\n");
        iobsf(fdo,1);
     }
     else {                                  /* not empty - skip to end */
        do { iofsf(fdo,1);
        } while (0<ioread(fdo,buf,bf));
        iobsf(fdo,1);                         /* position between EOF's */
     }
  }

  while (getlist(&nof)) {                       /* read file one by one */
      outname(prefix,nof);                      /* get new file name    */
      input = newfn('I',".mt");
      if ((fdi=open(input,0))<0) {              /* open FITS input file */
         printf("Cannot open file >%s<\n",input);
         continue;
      }
      rec = 0; idx = 0; nr = 0;
      mb = MBUF - FLR;
      do {                                    /* copy block by block    */
         no = read(fdi,&buf[idx],mb);
         if (!nr++ && no) {                   /* check if FITS file     */
            for (i=0; i<9 && buf[i]=="SIMPLE  ="[i]; i++);
            if (i!=9) {                       /* no FITS file - skip    */
               printf("Error: file >%s< not FITS format - skipped!\n",input);
               break;
            }
         }
         if (0<no) idx += no;
         if (bf<=idx || no<=0) {              /* write block tp tape    */
            nb = 0;
            n = (idx/FLR) * FLR;
            n = (bf<n) ? bf : n;
            while (n && n<=idx-nb) {
               i = iowrite(fdo,&buf[nb],n);
               if (i<0) {                     /* End of tape reached    */
                  iobsf(fdo,1);
                  ioweof(fdo);
                  close(fdi);
                  printf("Warning: End of Tape Mark detected !!\n");
                  ioclose(fdo); ospexit(1);
               }
               nb += i; rec++;
            }
            n = idx; idx = 0;
            for (i=nb; i<n; i++) buf[idx++] = buf[i];
         }
      } while (0<no);
      close(fdi);
      if (rec) {
         i = ioweof(fdo);
         printf(" File >%s< written to tape with %5d records\n",input,rec);
      }
  }
  ioweof(fdo); iobsf(fdo,1);
  ospexit(0);
}
