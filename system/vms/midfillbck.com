$ @(#)midfillbck.com	19.1 (ESO-IPG) 02/25/03 14:32:20
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command procedure MIDFILLBCK.COM
$ !
$ ! IPG			ESO - Garching          870706, 910312  K. Banse
$ ! IPG			ESO - Garching          881215 C.Guirao
$ ! IPG			ESO - Garching          931007 C.Guirao
$ !
$ ! fill the MIDAS directories from the export backup file
$ ! which has been written with MIDEXPORT.COM
$ !
$ !----------------------------------------------------------
$ !
$ ! get logical assignments for MIDAS stuff
$ IF P1 .NES. "" THEN GOTO P1_OK
$ IF MIDVERS .NES. "" THEN GOTO CHECK_P2
$ WRITE SYS$OUTPUT "We need the MIDAS version (e.g. 93NOV) as 1. Parameter ..."
$ WRITE SYS$OUTPUT "or MIDVERS must be defined previously"
$ EXIT
$ !
$ P1_OK:
$ MIDVERS :== 'P1'
$ !
$ CHECK_P2:
$ !
$ IF P2 .NES. "" THEN GOTO P2_OK
$ IF MIDASHOME .NES. "" THEN GOTO START
$ WRITE SYS$OUTPUT "We need the MIDAS home (e.g. MIDAS) as 2. Parameter ..."
$ WRITE SYS$OUTPUT "or MIDASHOME must be defined previously"
$ EXIT
$ !
$ P2_OK:
$ MIDASHOME :== 'P2'
$ !
$ START:
$ !
$ INQUIRE/NOPUNCT BCKFILE "enter backup file, (e.g. 93N08.BCK) "
$ !
$ SET VERIFY
$ SET NOON
$ !
$ SET PROTECTION=(G:RWE,W:RWE)/DEFAULT
$ !
$ MIDAS:
$ !
$ ! fill the directories MID_DISK:['MIDASHOME'.'MIDVERS'...]
$ !
$ SET DEF MID_DISK:['MIDASHOME'.'MIDVERS']
$ !
$ BACKUP/NEW_VERSION/SELECT=([PMIDAS.'MIDVERS'...]*.*) 'BCKFILE'/SAV -
  MID_DISK:['MIDASHOME'.'MIDVERS'...]*.*
$ !
$ !  get the logical assignments
$ SET DEF MID_DISK:['MIDASHOME'.'MIDVERS']
$ @ MID_DISK:['MIDASHOME'.'MIDVERS'.MONIT]MIDLOGS
$ !
$ !
$ PURGE/LOG MID_DISK:['MIDASHOME'.'MIDVERS'...]
$ !
$ SET NOVERI
$ EXIT
