$ ! @(#)compile.com	19.1 (ESO-IPG) 02/25/03 14:32:19 
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command file COMPILE     	version 1.1	820602
$ ! K. Banse   
$ !
$ ! compile all FORTRAN files in a directory
$ ! execute as @ COMPILE par1, where par1 specifies the directory
$ ! e.g., @ COMPILE [APPLIC.DAZ]
$ !
$ !-----------------------------------------------------
$ !
$ ! save directory
$ SAVE_DIR := 'F$LOGICAL("SYS$DISK")''F$DIRECTORY()'
$ !
$ ! set default to directory given in P1
$ IF P1 .EQS. "" THEN P1 := 'SAVE_DIR'
$ SET DEF 'P1'
$ PURGE
$ !
$ ! store directory (in nice format) into file DIRFILE.DAT
$ DEL DIRFILE.DAT.*
$ DIR/SIZE/OUTPUT=DIRFILE.DAT *.FOR
$ !
$ ! and read that file now
$ OPEN/READ INPUT DIRFILE.DAT
$ SET NOON
$ BEGIN:
$ READ/END_OF_FILE=EOF INPUT RECORD
$ NN = 'F$LOCATE(".FOR",RECORD)'
$ IF NN .EQ. 'F$LENGTH(RECORD)' THEN GOTO BEGIN
! 
! now compile each FORTRAN file
$ SOURCE := 'F$EXTRACT(0,NN,RECORD)'
$ SET VERI
$ FORTRAN 'SOURCE'
$ SET NOVERI
$ GOTO BEGIN
$ !
$ ! That's it folks...
$ EOF:
$ SET ON
$ CLOSE INPUT
$ SET DEF 'SAVE_DIR'
$ EXIT
