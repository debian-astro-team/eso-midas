$ @(#)tar_bck.com	19.1 (ESO-IPG) 02/25/03 14:32:20
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command procedure TAR_BCK.COM
$ ! IPG		ESO-Garching		940117 	C.Guirao
$ ! save the MIDAS tar files and executables on a BACKUP/SAVE file
$ !
$ !----------------------------------------------------------
$ !
$ SET DEF MID_DISK:['MIDASHOME']
$ BACKUP/INTERCHANGE/IGNO=INTER -
   *.EXE,MIDASDOC.TAR_Z,93NOVPL*.TAR_Z MIDAS.BCK/SAVE
$ EXIT
