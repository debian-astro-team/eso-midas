$ ! @(#)midexport.com	19.1 (ESO-IPG) 02/25/03 14:32:20 
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command procedure MIDEXPORT.COM
$ ! IPG		ESO-Garching		880301
$ ! IPG		ESO-Garching		881215 	C.Guirao
$ ! IPG		ESO-Garching		890426 	C.Guirao
$ !         	New parameter: MIDASHOME
$ !                                     900417  M.Peron
$ ! save the directory ['MIDASHOME'...] on magtape or disk
$ !
$ ! the release version is reflected in the label of the backup tape
$ ! right now it is 	MIDVERS
$ !
$ !----------------------------------------------------------
$ !
$ ! get necessary input
$ !
$ INQUIRE MIDAS_DEVICE  "From what disk do you take the version of MIDAS ''version'($1$DUA7) ? "
$ INQUIRE MIDASHOME "Which is the Midas home directory (e.g. PMIDAS) ? "
$ INQUIRE MIDVERS "Which version do you want to export (e.g. 93NOV) ? "
$ !
$ BLKSIZE :==
$ TAPE :==
$ BCKFILE :== MIDAS.BCK
$ SAVE :== 
$ !
$ INQUIRE BCK_DEV "Backup on tape or disk (default: disk)? " 
$ IF BCK_DEV .eqs. "TAPE" THEN GOTO BCK_TAPE
$ !
$ SAVE :== /SAVE_SET
$ INQUIRE BCKFILE "Enter backup file, (e.g. 93NOV.BCK) "
$ INQUIRE BLOCK_SIZE "enter block size (e.g. 2048-ftp, CR-default) "
$ IF BLOCK_SIZE .nes. "" THEN BLKSIZE :== /BLOCK_SIZE='BLOCK_SIZE'
$ GOTO CONT1
$ !
$ BCK_TAPE:
$ INQUIRE TAPE "enter magtape unit, (e.g. MTA0/MTA1) "
$ NN = 'F$LOCATE(":",TAPE)'
$ IF NN .EQ. F$LENGTH(TAPE) THEN TAPE = TAPE+":"
$ INQUIRE DENS "density of the tape, (800/1600/6250) "
$ !
$ INIT/DENSITY='DENS' 'TAPE' 'MIDVERS'
$ MOUNT/DENSITY='DENS'/FOR 'TAPE'
$ !
$ CONT1:
$ INQUIRE CLEANY "do you want to clean up first ? (Y/N)"
$ !
$ !
$ SET NOON		!make sure BACKUP goes all the way...
$ !
$ ASSIGN 'MIDAS_DEVICE': MID_DISK
$ !
$ IF BCK_DEV .eqs. "TAPE" THEN GOTO BCK_MIDFILL
$ GOTO CONT2
$ !
$ BCK_MIDFILL:
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT "Put MIDFILL.COM at the beginning of the tape in the"
$ WRITE SYS$OUTPUT "               save set MIDFILL.BCK"
$ WRITE SYS$OUTPUT " "
$ !
$ SET DEF MID_DISK:['MIDASHOME'.'MIDVERS'.SYSTEM.VMS]
$ BACKUP/IGNORE=INTER/LABEL='MIDVERS' MIDFILL.COM 'TAPE'MIDFILL.BCK
$ !
$ !
$ CONT2:
$ ! Here for ['MIDASHOME'...] 
$ IF CLEANY .NES. "Y" THEN GOTO B_MIDAS
$ !
$ ! 	cleanup ['MIDASHOME']
$ !
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT "               Cleaning of MID_DISK:['MIDASHOME'.''MIDVERS'...]"
$ WRITE SYS$OUTPUT " "
$ !
$ SET DEF MID_DISK:['MIDASHOME'.'MIDVERS']
$ @ MID_DISK:['MIDASHOME'.'MIDVERS'.SYSTEM.VMS]CLEANUP
$ !
$ B_MIDAS:
$ !
$ ! Here for ['MIDASHOME'.'MIDVERS']
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT "               Backup of MID_DISK:['MIDASHOME'.''MIDVERS'...]"
$ WRITE SYS$OUTPUT " "
$ !
$ !
$ SET DEF MID_DISK:['MIDASHOME']
$ BACKUP'BLKSIZE'/INTERCHANGE/IGNO=INTER/EXCL=(-
   ['MIDASHOME'.'MIDVERS'.PRIM.PROC]INTERNAL.COD,-
   ['MIDASHOME'.'MIDVERS'.LIBSRC.OS.UNIX]*.*,-
   ['MIDASHOME'.'MIDVERS'.STDRED.ASTRO...]*.*,-
   ['MIDASHOME'.'MIDVERS'.STDRED.CCD...]*.*,-
   ['MIDASHOME'.'MIDVERS'.STDRED.HSP...]*.*,-
   ['MIDASHOME'.'MIDVERS'.STDRED.MMF...]*.*,-
   ['MIDASHOME'.'MIDVERS'.STDRED.PROC...]*.*,-
   ['MIDASHOME'.'MIDVERS'.CONTRIB.XSTUFF...]*.*,-
   ['MIDASHOME'.'MIDVERS'.CONTRIB.ESOLV...]*.*,-
   ['MIDASHOME'.'MIDVERS'.CONTRIB.STELLA...]*.*,-
   ['MIDASHOME'.'MIDVERS'.CONTRIB.PROC...]*.*,-
   ['MIDASHOME'.'MIDVERS'.INSTALL.UNIX...]*.*,-
   ['MIDASHOME'.'MIDVERS'.GUI...]*.*,-
   ['MIDASHOME'.'MIDVERS'.DOC...]*.*,-
   ['MIDASHOME'.'MIDVERS'...]*.EXE,-
   ['MIDASHOME'.'MIDVERS'...]*.LOG,-
   ['MIDASHOME'.'MIDVERS'...]*.OBJ,-
   ['MIDASHOME'.'MIDVERS'.SYSTAB.BIN]*.LUT,-
   ['MIDASHOME'.'MIDVERS'.SYSTAB.BIN]*.ITT,-
   ['MIDASHOME'.'MIDVERS'.SYSTAB.BIN]*.AUX,-
   ['MIDASHOME'.'MIDVERS'...]*.OLB)-
   MID_DISK:['MIDASHOME'.'MIDVERS'...]*.* 'TAPE''BCKFILE''SAVE'
$ !
$ BYBY:
$ IF BCK_DEV .eqs. "TAPE" THEN DISMOUNT 'TAPE'
$ SET DEF MID_DISK:['MIDASHOME'.'MIDVERS'.SYSTEM.VMS]
$ SET ON
$ EXIT
