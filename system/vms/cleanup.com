$ ! @(#)cleanup.com	19.1 (ESO-IPG) 02/25/03 14:32:19 
$ ! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ ! 
$ ! command procedure CLEANUP
$ ! K. Banse	850819, 910312
$ ! purge directory + delete DIRFILE.DAT, SEARCH.DAT, *.KEY, *.LOG, CODE.SAV
$ ! delete all files of type .LIS and .OBJ and .MAP and .TST
$ ! 
$ ! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ PURGE/LOG [...]
$ DELETE/LOG [...]DIRFILE.DAT.*,SEARCH.DAT.*
$ DELETE/LOG [...]CODE.SAV.*,*.LOG.*,*.KEY.*
$ DELETE/LOG [...]*.LIS.*
$ DELETE/LOG [...]*.O.*
$ DELETE/LOG [...]*.F.*
$ DELETE/LOG [...]*.OBJ.*
$ DELETE/LOG [...]*.MAP.*
$ DELETE/LOG [...]*.TST.*
$ EXIT
