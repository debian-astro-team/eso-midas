$ ! @(#)midsave.com	19.1 (ESO-IPG) 02/25/03 14:32:20 
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command procedure MIDSAVE.COM
$ ! K. Banse       861208  ESO-Garching
$ ! save the directories [MIDAS...], [SYSMIDAS], [DEVICES...],
$ !			 [APPLIC...], [MIDEXE], [APPEXE] on magtape 
$ ! C.Guirao	   890425  MIDVERS and MIDASHOME must be defined
$ !
$ !----------------------------------------------------------
$ !
$ ! get logical assignments for MIDAS stuff
$ IF P1 .NES. "" THEN GOTO P1_OK
$ IF MIDVERS .NES. "" THEN GOTO CHECK_P2
$ WRITE SYS$OUTPUT "We need the MIDAS version (e.g. 91MAY) as 1. Parameter ..."
$ WRITE SYS$OUTPUT "or MIDVERS must have been defined previously"
$ EXIT
$ !
$ P1_OK:
$ MIDVERS :== 'P1'
$ !
$ CHECK_P2:
$ !
$ IF P2 .NES. "" THEN GOTO P2_OK
$ IF MIDASHOME .NES. "" THEN GOTO START
$ WRITE SYS$OUTPUT "We need the MIDAS home (e.g. MIDAS) as 2. Parameter ..."
$ WRITE SYS$OUTPUT "or MIDASHOME must have been defined previously"
$ EXIT
$ !
$ P2_OK:
$ MIDASHOME :== 'P2'
$ !
$ START:
$ !
$ ! get necessary input
$ !
$ INQUIRE/NOPUNCT TAPE "enter magtape unit, (e.g. MTA0/MTA1) "
$ INQUIRE/NOPUNCT DENS "density of the tape, (800/1600/6250) "
$ INQUIRE/NOPUNCT LIST "Do you want the directory-listing of the tape ?(Y/N)"
$ !
$ INIT/DENSITY='DENS' 'TAPE': MIDAS
$ MOUNT/DENSITY='DENS'/FOR 'TAPE':
$ SET NOON				!make sure BACKUP/IGNORE=INTERLOCK goes all the way...
$ SET VERI
$ !
$ BACKUP/IGNORE=INTERLOCK  MID_DISK:['MIDASHOME'.'MIDVERS'...]*.* 'TAPE':MIDAS
$ !
$ IF LIST .NES. "Y" THEN GOTO BYBY
$ !
$ !  List the contents of the save sets
$ BACKUP/REWIND/LIST=MID_DISK:['MIDASHOME'.'MIDVERS']MIDAS.BKP 'TAPE':MIDAS
$ SET DEF MID_DISK:['MIDASHOME'.'MIDVERS']
$ SHO DEF
$ WRITE SYS$OUTPUT "Backup listing stored in MIDAS.BKP in directory above"
$ !
$ BYBY:
$ DISMOUNT 'TAPE':
$ SET NOVERI
$ SET NOON
$ SET DEF MID_DISK:['MIDASHOME'.'MIDVERS']
$ EXIT
