$ ! @(#)midcop.com	19.1 (ESO-IPG) 02/25/03 14:32:19 
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " ******************************************"
$ WRITE SYS$OUTPUT "    Procedure to duplicate the MIDAS tape  "
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT "    Ch. Ounnas   ESO - Garching   850716   "
$ WRITE SYS$OUTPUT " ******************************************"
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " INPUT  TAPE ON MTA0: IN 1600 BPI "
$ WRITE SYS$OUTPUT " OUTPUT TAPE ON MTA1: IN 1600 BPI "
$ WRITE SYS$OUTPUT " "
$! INQUIRE/NOPUNCT TAPEIN  "Enter the input tape unit  (MTA0/MTA1) : "
$! INQUIRE/NOPUNCT DENSIN  "Enter the density of the input tape    : "
$! INQUIRE/NOPUNCT TAPEOUT "Enter the output tape unit (MTA0/MTA1) : "
$! INQUIRE/NOPUNCT DENSOUT "Enter the density of the output tape   : "
$! WRITE SYS$OUTPUT " "
$! WRITE SYS$OUTPUT " "
$! INQUIRE/NOPUNCT N "Number of Save_sets to copy on the output tape : "
$! WRITE SYS$OUTPUT " "
$! WRITE SYS$OUTPUT " "
$ !
$ N = 2
$ MOUN/FOR/BLOCK=8464/DENSITY=1600 MTA0:
$ MOUN/FOR/BLOCK=8464/DENSITY=1600 MTA1:
$ !
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " Start of copy input tape on output"
$ !
$ LOOP:
$ !
$ COPY MTA0: MTA1:
$ COPY MTA0: MTA1:
$ COPY MTA0: MTA1:
$ !
$ N = N - 1
$ IF (N .GT. 0) THEN GOTO LOOP
$ !
$ DISM/NOUNLOAD MTA0:
$ DISM MTA1:
$ !
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " "
$ WRITE SYS$OUTPUT " End of copy procedure"
$ WRITE SYS$OUTPUT " "
$ !
$ EXIT
