
 1)	ftoc_names.for + ftoc_namesc.c => ftoc_names.exe

	find out how Fortran subroutine names like CSUB() are passed on
	to C
	the executable prints out the string (3/4 chars):
	-fl	for csub()		-fu	for CSUB()
	-fl_	for csub_()		-fu_	for CSUB_()
	-_fl_	for _csub_()		-f_u_	for _CSUB_()
	-_fl	for _csub()		-f_u	for _CSUB()

	e.g. on AMD 64 Athlon +3500 with SUSE 9.3, gcc 3.3 we get
	$ ../exec/ftoc_names.exe
	-fl_

 2)	forif.c + ftoc.c => forif.exe

	convert a .fc file to a regular .c file
	using output from ftoc_names.exe as input


 3)	ftoc_log.for + ftoc_logc.fc => ftoc_log.exe

	to determine the integer values given to .TRUE. and .FALSE. in Fortran
	as the type .fc indicates, ftoc_log.exe already uses the interface
	F77-C determined before...


 4)	for_try.for + ftoc_stack.fc + stack_dump.h => ftoc_stack.exe

	to show the parameter stack for different subroutine calls from 
	Fortran, inside the called C routine

	the program is controlled via the ASCII file stack-test (1 line)
	which contains a control-flag, the 1st and last stack entry to be
	displayed

	within the C routines of ftoc_stack.fc, the flags nogo[i] are set 
	 = 0 (just show bare stack), = 1 (interpret entry as holding a 
	character parameter), = 2 (look for integer parameters). 


 5)	for_test.for + ftoc_test.fc => ftoc_test.exe

	test calling C routines from Fortran, i.e.
	SSP1(s1,n1,s2,n2), SSP2(n1,n2,n3,n4,n5,n6), and SSP3(s1,s2,s3)

	compare numbers, strings and lengths of strings with original values
	from the Fortran main program



 060228

