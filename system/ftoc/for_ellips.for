C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++
C.IDENTIFICATION for_ellips.for
C.LANGUAGE       F77
C.AUTHOR         K. Banse
C.ENVIRONMENT    Any
C.KEYWORDS       FORTRAN, C
C.PURPOSE        Check Fortran to C interface
C.COMMENTS       
C.VERSION  1.0   060315
C 
C 060316	last modif
C 
C 
C this is a Fortran program for the most basic test:
C 1) using COMMON in Fortran and access it from C
C 2) calling an ellips C routine 
C 
C----------------
C 
C 
      INTEGER    NUMB, LS2, MADRID(3)
C 
      CHARACTER*161 S2
C 
      COMMON /VMR/ MADRID
C 
      WRITE(*,995)
      WRITE(*,998)
      WRITE(*,996)
C 
      NUMB = -33
      MADRID(1) = 123456
      MADRID(2) = NUMB
      MADRID(3) = 99
      WRITE(*,2200) MADRID(1),MADRID(2),MADRID(3)
      CALL CSUB()
      WRITE(*,1100) 'CSUB'
C 
      WRITE(*,2100) 99123
      CALL SSN(99123)
      WRITE(*,1100) 'SSN'
      S2 = 'ss<abcdefghijklmnopqrstuvwxyz01234567890>XYZ'
      LS2 = 161
      WRITE(*,1000) S2,LS2
      CALL SSS(s2)
      WRITE(*,1100) 'SSS'
C 
      STOP 0
C 
995   FORMAT
     +(/'*********************************************************')
996   FORMAT
     +('*********************************************************'/)
998   FORMAT(/' Test of calling a C function with ellips',
     +       ' parameter definition from Fortran'/)
999   FORMAT('1 ')
1000  FORMAT('F: calling SSS(str1), str1 = ',A40/
     +'             length = ',I4)
1100  FORMAT('F: returning from ',A,/)
2100  FORMAT('F: calling SSN(n1), n1 = ',I8)
2200  FORMAT('F: calling CSUB(), COMMON = ',I6,',',I6,',',I6)
C 
      END
