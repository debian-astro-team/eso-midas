/*===========================================================================
  Copyright (C) 1995-2006 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++
.IDENTIFICATION ftoc_ellips.fc
.LANGUAGE       C
.AUTHOR         K. Banse - ESO Garching
.ENVIRONMENT    Any
.KEYWORDS       Fortran to C interface
.COMMENTS       this subroutine is called from for_ellips.for
.VERSION  1.0   060315
.ALGORITHM	show if an ellips function can be called from Fortran

 060316		last modif
 
---------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <ftoc_test.h>

#ifndef VOIDSIG
#define VOIDSIG   void
#endif


#define vmr	vmr_		/* parameter for Name Translation is l_ */
#define CSUB	csub_
#define SSN	ssn_
#define SSS	sss_


extern struct { int addr[3]; } vmr ;

/*

*/

/*==========================================================================
 *     Check Fortran to C Interface 
 *     FORTRAN CSUB, SSA, SSX, ... calls in for_try.for
 *==========================================================================*/

int CSUB()

{
int  mm[3];


printf("C: entering CSUB...\n");

mm[0] = vmr.addr[0];
mm[1] = vmr.addr[1];
mm[2] = vmr.addr[2];
printf("C: FORTRAN COMMON VMR = %d, %d, %d\n\n",mm[0],mm[1],mm[2]);

}


/*==========================================================================*/

SSN(int *noargs, ...) 

{
va_list Cargs;


va_start(Cargs,noargs);       /* <forif> */


printf("C: entering SSN...\n");

}

/*==========================================================================*/

SSS(int *noargs, ...) 

{
va_list Cargs;
int FORmark;          


va_start(Cargs,noargs);
FORmark = ftoc_mark(); 

 
printf("C: entering SSS...\n");

ftoc_free(FORmark); 

}

