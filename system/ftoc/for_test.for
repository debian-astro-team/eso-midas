C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++
C.IDENTIFICATION for_test.for
C.LANGUAGE       F77
C.AUTHOR         Francois Ochsenbein
C.ENVIRONMENT    Any
C.KEYWORDS       FOR to C
C.PURPOSE        Check Fortran to C interface
C.COMMENTS       
C.VERSION  1.0   16-Oct-1990
C 
C 060228	last modif
C----------------
C 
      INTEGER    MADRID(3)
C 
      CHARACTER*79  s1
      CHARACTER*161 s2
      CHARACTER*4   s3
C 
      CHARACTER*5   s4
      CHARACTER*40  st1, st2, st3
C 
      COMMON/VMR/MADRID
C 
      s1 = 'sY<01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ>'
      s2 = 's2<abcdefghijklmnopqrstuvwxyz01234567890>XYZ'
      s3 = 's3.X'
      st1 = 'where does this string go? '
      st2 = 'filling up space... '
      st3 = '&now, occupying 40 characters over here&& '
      s4 = 'Klaus'
C
      MADRID(2) = 99
      CALL CSUB()
      IF (MADRID(1) .NE. -1) THEN
         WRITE(*,1123) MADRID(1)
         STOP 1
      ENDIF
C 
CC      write(*,1234) 'call SSP1(s1,-71,s2,-2)'
      CALL SSP1(s1,-71,s2,-2)
C 
CC      write(*,1234) 'call SSP2(3,-2,400,-71,555,666)'
      CALL SSP2(3,-2,400,-71,555,666)
C 
CC      write(*,1234) 'call SSP3(s4,s3,s1)'
      CALL SSP3(s4,s3,s1)
C 
C 
      STOP
C 
1234  FORMAT(A)
1123  FORMAT('COMMON MADRID(1) is ',I5,' instead of -1 ...')
C 
      END
