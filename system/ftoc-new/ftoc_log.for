C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++
C.IDENTIFICATION ftoc_log.for
C.LANGUAGE       F77
C.AUTHOR         Francois Ochsenbein
C.ENVIRONMENT    
C.KEYWORDS       
C.PURPOSE        Test of TRUE/FALSE logical values
C.COMMENTS       
C.VERSION  1.0   19-Dec-1990
C 060203	last modif
C 
C----------------
C
      LOGICAL T,F
      INTEGER I,J
C
      T = .TRUE.
      F = .FALSE.

      WRITE (6,1)
  1   FORMAT('/*++++++++++++++++++')
      WRITE (6,2)
  2   FORMAT('.IDENTIFICATION ftoc_log.h')
      WRITE (6,3)
  3   FORMAT('.LANGUAGE       C')
      WRITE (6,4)
  4   FORMAT('.AUTHOR         Francois Ochsenbein')
      WRITE (6,5)
  5   FORMAT('.ENVIRONMENT')
      WRITE (6,6)
  6   FORMAT('.KEYWORDS')
      WRITE (6,7)
  7   FORMAT('.VERSION  1.0   12-Dec-1990')
      WRITE (6,8)
  8   FORMAT('.COMMENTS       Here is the definition of LOGICAL')
      WRITE (6,9)
  9   FORMAT('                values in Fortran.')
      WRITE (6,10)
  10  FORMAT('---------------*/')
      WRITE (6,11)
  11  FORMAT('#ifndef F77DEF_DEF'/'#define F77DEF_DEF')

C get the numerical values for .TRUE. and .FASLE.
      CALL CSUB(T,F,I,J)		!call C routine

      WRITE (6,12) I,J
  12  FORMAT('#define F77TRUE ',I10/'#define F77FALSE',I10)
      WRITE(6,13)
  13  FORMAT('#endif')
C
      STOP
      END
