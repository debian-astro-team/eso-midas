
/*

function map in file ftoc_commap.c

*/


#include <stdlib.h>
#include <stdio.h>


int map(size,pntr)
int   size;      /* IN : number of data values (pixels) to be mapped */
char  **pntr;    /* OUT: pointer to data in memory */

{
char  *mypntr;
int   *iptr;



mypntr = malloc((size_t) size);   
if (mypntr == (char *) 0) return -1;
 
/*
printf("map: size = %d, mypntr = %x\n",size,mypntr);
*/


iptr = (int *) mypntr;
*iptr = -54321;

*pntr = mypntr;
return 0;
}
