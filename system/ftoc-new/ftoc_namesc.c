/*===========================================================================
  Copyright (C) 1995-2006 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE	     Test module
.IDENT       ftoc_names.c
.LANGUAGE    C
.AUTHOR      Carlos Guirao [ESO/IPG]
.ENVIRONMENT UNIX
.COMMENT     Print the FortranName_to_CName translation on current host.
.REMARKS
.VERSION     1.1   901212:  Creation,     CG
.VERSION     1.2   910913:  For CONVEX, the common data has an extra underscore
.VERSION     1.3   920715:  CONVEX deleted, now is detected in ftoc_vmr.exe
 060203		last modif

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#include <stdio.h>




#ifndef vms
void csub () { printf("-fl\n"); }
void csub_ () { printf("-fl_\n"); }
void _csub_ () { printf("-f_l_\n"); }
void _csub () { printf("-f_l\n"); }
#endif /* vms */

void CSUB () { printf("-fu\n"); }
void CSUB_ () { printf("-fu_\n"); }
void _CSUB_ () { printf("-f_u_\n"); }
void _CSUB () { printf("-f_u\n"); }
