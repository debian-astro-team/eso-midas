C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C     This FORTRAN code is in the file named "ftoc_commff.for"
C
C.VERSION
C 060322		last modif
C 
C
      SUBROUTINE RTST(A,SIZE,STAT)
      REAL   A(*)
      INTEGER  SIZE,STAT
C
      STAT = 0
C 
C  1st element was set to 77.7 in C routine fmap_
      IF ((A(1).LT.76.699) .OR. (A(1).GT.77.701)) THEN
         write(*,123) a(1)
123      format('a(1) = ',f9.4)
         STAT = -888
         RETURN
      ENDIF
C 
      A(2) = 66.6
      A(SIZE) = 99.9
C 
      IF ((A(2).LT.66.5999) .OR. (A(2).GT.66.6001)) THEN
         STAT = -999
      ENDIF
C 
      RETURN
      END
