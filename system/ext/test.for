C @(#)test.for	19.1 (ES0-DMD) 02/25/03 14:31:15
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      PROGRAM TEST
C+++
C.IDENTIFICATION
C     PROGRAM TEST
C
C.PURPOSE
C     TEST THE EXTRACTION OF FORTRAN EXTENSIONS FROM A SOURCE FILE
C
C---
      PARAMETER    (MAX_ARRAY=100)
C
      IMPLICIT NONE
      INTEGER  I,N
      REAL     ARRAY(MAX_ARRAY,MAX_ARRAY)
      REAL     THIS_IS_REAL,RESULT
C+
C     TEST ENDDO
C-
      DO I = 1,MAX_ARRAY
	 DO N = 1,MAX_ARRAY
            IF (I.EQ.N) GOTO 200
	    ARRAY(I,N) = I*K
  200    ENDDO
      ENDDO
C+
C     INITIATE VARIABLES
C-
      RESULT = 0.0E0                    ! RESET RESULT
C+
C     NEXT TEST
C-
      DO I = 1,MAX_ARRAY
	 DO 100, N = 1,MAX_ARRAY
            THIS_IS_REAL = RESULT + ARRAY(I,N)
  100    CONTINUE
      ENDDO
C+
C     CALL SUBROUTINE
C-
      CALL DUMMTEST(RESULT)
C+
C     END TEST
C-
      STOP
      END
C+
C
C-
      SUBROUTINE DUMMTEST(X)
      IMPLICIT NONE
      REAL X
C
      X = X * X
C
      RETURN
      END
C+
C
C-
      CHARACTER*(*)  FUNCTION  NTOCHR (N)
C
C Converts an integer in the range 1-99 to two characters representing
C the number.
C
      IMPLICIT NONE
      INTEGER N, ITENS
      IF ((N .GT. 0) .AND. (N .LT. 100)) GO TO 1010
      NTOCHR='00'
      RETURN
C
 1010 ITENS=N/10
      NTOCHR=CHAR(48+ITENS)//CHAR(48+N-10*ITENS)
      RETURN
C
      END
