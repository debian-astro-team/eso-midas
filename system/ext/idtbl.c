/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++  Remove ESO extensions to FORTRAN 77    ++++++++++++++++++++
.COPYRIGHT   (c) 1994-2005 European Southern Observatory
.LANGUAGE    C
.IDENT       idtbl.c
.AUTHOR      Preben J. Grosbol [ESO/IPG]
.KEYWORDS    identifier table, long names
.ENVIRONMENT UNIX
.COMMENT
.VERSION     1.0   12-Nov-1987: Creation,     PJG
.VERSION     1.1   14-Jan-1988: Add check for all id's,   PJG
.VERSION     1.2   08-Sep-1988: Standard error lists,   PJG
.VERSION     1.3   27-Apr-1994: Correct unique ID check,   PJG
 051012		last modif

------------------------------------------------------------------------*/

#include   <stdlib.h>
#include   <stdio.h>                        /* standard I/O functions   */
#include   <ctype.h>                        /* character types          */
#include   <string.h>                       /* string functions         */
#include   <f77ext.h>                       /* definition of constants  */

extern    int                   no_id;      /* no. of identifiers       */
extern    ID                  idtbl[];      /* table of identifiers     */

ID *add_id(id,size,type,group,err)        /* add new identifier to list */
char       *id;
int       size;
char      type;
char     group;
int       *err;
{
  int     n,i;
  char    c,*pid,*tid;
  ID      *pidtbl;

  if (size<=0) return (ID *) 0;          /* if zero length - return    */
  pidtbl = idtbl; *err = 0;
  for (n=0; n<no_id; n++) {              /* go through identifiers     */
    pid = id; tid = pidtbl->lname; i = size;
    while ((c = *tid++) == *pid++ && c != '\0') i--;
    if (!(c || i)) {                  /* identifier found, check it */
      return pidtbl;
    }
    pidtbl++;
  }
  if (MXID<=no_id) {
    fprintf(stderr,"Error: Too many identifiers\n");
    exit(1);
  }

  pidtbl->size = size;                  /* add new identifier to table */
  pidtbl->type = type;
  pidtbl->group = group;
  pidtbl->sname[0] = '\0';
  tid = pidtbl->lname;
  while (size--) *tid++ = *id++;        /* copy identifier over        */
  *tid = '\0';
  no_id++;

  return pidtbl;
}

void new_id(id,size,new)                     /* make short identifier      */
char       *id;
int       size;
char      *new;
{
  int     n,nomod;
  char    c,*pc;

  nomod = size < MXIDENT;               /* no modification for short  */

  n = 0; pc = new;
  while (size--) {                      /* construct short ident.     */
    c = *id++;
    if (c=='_') { nomod = 0; continue; }
    n++;
    if (n<MXIDENT-1 || !size) *new++ = c;
  }
  if (nomod) *pc = '\0'; else *new = '\0';

  return;
}

int chk_id()                         /* check if short id's are uniq */
{
  int      i,n,err;
  char     *sid,*ssd;
  ID       *pid;

  pid = idtbl; err = 0;
  for (n=0; n<no_id; n++, pid++) {
     if (*(sid = pid->sname)) {         /* check only new ident's     */
	for (i=0; i<no_id; i++)          /* compare with all short id  */
	  if (i!=n && *(ssd=idtbl[i].sname))
	    if (!strcmp(ssd,sid)) {
	       err = 1;
	       fprintf(stderr,"Error: Identifier >%s< - >%s< : >%s<\n",
		       pid->lname,idtbl[i].lname,sid);
	       break;
	     }
	for (i=0; i<no_id; i++)          /* compare with all long  id  */
	  if (!strcmp(idtbl[i].lname,sid)) {
	     err = 1;
	     fprintf(stderr,"Error: Identifier error >%s< : >%s<\n",
		     idtbl[i].lname,sid);
	     break;
	   }
      }
   }

  return err;
}
