/*===========================================================================
  Copyright (C) 1987-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++  Remove ESO extensions to FORTRAN 77    ++++++++++++++++++++
.COPYRIGHT   (c) 1994 European Southern Observatory
.LANGUAGE    C
.IDENT       getline.c
.AUTHOR      Preben J. Grosbol [ESO/IPG]
.KEYWORDS    ESO fortran, line input
.ENVIRONMENT UNIX
.COMMENT
.VERSION     1.0   10-Nov-1987: Creation,     PJG
.VERSION     1.1   14-Jan-1988: Replace tabs with space, PJG
.VERSION     1.2   15-Feb-1988: Replace form-feed with space, PJG
.VERSION     1.3   23-Mar-1988: Redefine 'c' as int, PJG
.VERSION     1.4   20-Dec-1988: Check for max. line ident., PJG
.VERSION     1.5   24-Jun-1992: Correct check for '' in format, PJG
.VERSION     1.6   27-Apr-1994: Warning for character after col.72, PJG
.VERSION     1.7   06-May-1994: Check only non-space after col72, PJG

 090903		last modif
------------------------------------------------------------------------*/
#include   <stdio.h>                        /* standard I/O functions   */
#include   <ctype.h>                        /* character types          */
#include   <f77ext.h>                       /* definition of constants  */

extern    int                  x_flag;      /* extension option flag    */
extern    int                   equal;      /* level zero equal sign    */
extern    int                   comma;      /* level zero comma         */
extern    int                     nlb;      /* present index in 'lbuf'  */
extern    int                 id_size;      /* length of identifier     */
extern    int                  no_lid;      /* no. of line identifiers  */
extern    int                   nstat;      /* char. index in 'stat'    */
extern    char                 stmt[];      /* present statement        */
extern    char   lbuf[MXLBUF][MXLINE];      /* buffer for input lines   */
extern    LID                   lid[];      /* list of line identifiers */

static    int                 q_level;      /* quote level in statement */
static    int                 p_level;      /* parenthesis level        */
static    char                 *pstat;      /* pointer to statement     */

char get_line(fp)                           /* add character to buffer  */
FILE         *fp;
{
  int          c, nlbuf, e_mark, ncol, ncx;
  char         *plbuf;

  ncol = 0; ncx = 0;
  if (!nstat) {                        /* reset at start of statement   */
    q_level = 0; p_level = 0;
    pstat = stmt; id_size = 0;
  }

  nlbuf = 0; e_mark = 0;
  plbuf = &lbuf[nlb][0];

  while ((c=getc(fp)) != '\n' && c != EOF) {  /* read rest of line      */
    if (c=='\t' || c=='\f') c = ' ';
    if (MXLINE<=nlbuf) continue;
    if (e_mark) {
      if (!(x_flag & EXC_FLAG)) { *plbuf++ = c; nlbuf++; }
      continue;
    }
    if (c=='\'') q_level = !q_level;
    if (c=='!' && !q_level) {
      if (!(x_flag & EXC_FLAG)) { *plbuf++ = c; nlbuf++; }
      e_mark = 1;
      continue;
    }
    if (65<ncol++ && c!=' ') ncx = 1;
    if (c==',' && !p_level && !q_level) comma = 1;
    if (c=='=' && !p_level && !q_level) equal = 1;
    if (!q_level && (x_flag & UPC_FLAG)) c = (islower(c)) ? toupper(c) : c;

    if (id_size && c!=' ') {                  /* check for identifiers */
      if (isalpha(c) || isdigit(c) || c=='_') /* continue identifier   */
        id_size++;
      else {                                  /* end of identifier     */
        lid[no_lid].size = id_size;
        lid[no_lid].level = p_level;
        lid[no_lid].id = (ID *) 0;
        id_size = 0;
        if (MXLID<=++no_lid)
           fprintf(stderr,"Error: Too many identifiers in statement\n");
      }
    }
    else if (!q_level && isalpha(c)) {        /* start of identifier   */
      lid[no_lid].sid = pstat;
      lid[no_lid].lid = plbuf;
      lid[no_lid].lno = nlb;
      id_size++;
    }
    if (c=='(') p_level++;
    if (c==')') p_level--;
    *plbuf++ = c; nlbuf++;
    if (c!=' ' || q_level) { *pstat++ = c; nstat++; }
  }
  if (id_size)  {                             /* end of identifier     */
        lid[no_lid].size = id_size;
        lid[no_lid].level = p_level;
        lid[no_lid].id = (ID *) 0;
        id_size = 0;
        if (MXLID<=++no_lid)
           fprintf(stderr,"Error: Too many identifiers in statement\n");
  }
  *plbuf = '\0';
  if (ncx) {                             /* characters after column 72 */
     fprintf(stderr,"esoext-warning: Non-space characters after column 72\n");
     fprintf(stderr," >%-75.75s\n",&lbuf[nlb][0]);
   }
  return c;
}
