#! /bin/sh
# .COPYRIGHT: Copyright (c) 1988-2005 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           ftoc_vmr.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS  	  Test for Fortran to C interface. Executes ftoc_vmr.exe
#		  if exists and it is executable, otherwise nothing.
# .REMARKS
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    06-Jul-1992:	Implementation
# 050422	last modif


# <dirname> emulated with <sed>
#   DIR=`dirname $0`
DIR=`echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`

if [ -f $DIR/ftoc_vmr.exe -a -s $DIR/ftoc_vmr.exe ] ; then
        $DIR/ftoc_vmr.exe 
fi
exit 0
