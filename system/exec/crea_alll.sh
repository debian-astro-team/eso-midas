#! /bin/sh
# @(#)crea_alll.sh	19.1 (ESO-IPG) 02/25/03 14:31:07

# CG. To avoid SCCS keywords to be expanded in the echo command.
A=%
echo "% ${A}W% (ESO-IPG) ${A}G% ${A}U%"
echo ' '

ls *.hlc | awk 'BEGIN{FS="."}{print $1}' | tr a-z A-Z |     \
awk '{ORS = " "} {for (i=1;i<=NF;i++) {l = length($i); \
ORS = substr("             ",1,12-l);                  \
print substr($i,1,12)}}' |                             \
awk '{l = length ($0)} {n = l/72 + 1} {for (i=1;i<=n;i++) \
{s = 72*(i-1) + 1; print substr($0,s,72)}}'
