#! /bin/sh
# @(#)hlqtohlc.sh	19.1 (ESO-IPG) 02/25/03 14:31:08
# 
#  Bourne shell script "hlqtohlc.sh"
#  to build the .hlc files from the .hlq files
#  910529  KB
#  910712  CG. removing temporary files.
# 
if [ -f alll.hlc ]; then
	mv alll.hlc alll.hlk
fi
rm -f *.hlc
# 
for i in `ls *.hlq | \
	sed  -e 's/_.*//'  -e 's/\.hlq//' | \
	awk '{print substr($1,1,6)}' | \
	sort -u`
do
   for file in `ls $i*.hlq`
   do
      sed -n '/SECTION./,/\\us/p' $file | sed '/^\\/d' | sed '2d' >> $i.hlc
   done
done
if [ -f alll.hlk ]; then
	mv alll.hlk alll.hlc
fi
