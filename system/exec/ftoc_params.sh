#! /bin/sh
# .COPYRIGHT: Copyright (c) 1988-2006 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           ftoc_params.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS  	  Test for Fortran to C interface     
# .REMARKS
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    02-Jan-1991:	Implementation
# .VERSION 1.2    04-Oct-1991:	eval has been deleted because apollo complains.
#				but could also be used within quotes.
# 060301	last modif


show_result()
{
echo  ftoc_${FTOC_PARAM}.h          #return the result include file

echo "" >&2
echo "#################################################################" >&2
echo  ftoc_${FTOC_PARAM}.h matches local compiler \(copied to ftoc.h\) >&2
echo "#################################################################" >&2
echo "" >&2

exit
}

#
# <dirname> emulated with <sed>
#   DIR=`dirname $0`
# 
DIR=`echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`

echo .................... trying ftoc_sun.h ............................. >&2
if [ -f $DIR/ftoc_sun.exe -a -s $DIR/ftoc_sun.exe ] ; then
        $DIR/ftoc_sun.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="sun"
           show_result
        fi
fi

echo .................... trying ftoc_hp.h .............................. >&2
if [ -f $DIR/ftoc_hp.exe -a -s $DIR/ftoc_hp.exe ] ; then
        $DIR/ftoc_hp.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="hp"
           show_result
        fi
fi

echo .................... trying ftoc_hpr.h .............................. >&2
if [ -f $DIR/ftoc_hpr.exe -a -s $DIR/ftoc_hpr.exe ] ; then
        $DIR/ftoc_hpr.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="hpr"
           show_result
        fi
fi

echo .................... trying ftoc_hpn.h .............................. >&2
if [ -f $DIR/ftoc_hpn.exe -a -s $DIR/ftoc_hpn.exe ] ; then
        $DIR/ftoc_hpn.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="hpn"
           show_result
        fi
fi

echo .................... trying ftoc_pc.h ............................... >&2
if [ -f $DIR/ftoc_pc.exe -a -s $DIR/ftoc_pc.exe ] ; then
        $DIR/ftoc_pc.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="pc"
           show_result
        fi
fi

echo .................... trying ftoc_osf.h .............................. >&2
if [ -f $DIR/ftoc_osf.exe -a -s $DIR/ftoc_osf.exe ] ; then
        $DIR/ftoc_osf.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="osf"
           show_result
        fi
fi

echo .................... trying ftoc_osf2.h ............................. >&2
if [ -f $DIR/ftoc_osf2.exe -a -s $DIR/ftoc_osf2.exe ] ; then
        $DIR/ftoc_osf2.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="osf2"
           show_result
        fi
fi

echo .................... trying ftoc_axp.h .............................. >&2
if [ -f $DIR/ftoc_axp.exe -a -s $DIR/ftoc_axp.exe ] ; then
        $DIR/ftoc_axp.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="axp"
           show_result
        fi
fi

echo .................... trying ftoc_pc3.h .............................. >&2
if [ -f $DIR/ftoc_pc3.exe -a -s $DIR/ftoc_pc3.exe ] ; then
        $DIR/ftoc_pc3.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="pc3"
           show_result
        fi
fi

echo .................... trying ftoc_solaris.h .......................... >&2
if [ -f $DIR/ftoc_solaris.exe -a -s $DIR/ftoc_solaris.exe ] ; then
        $DIR/ftoc_solaris.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="solaris"
           show_result
        fi
fi

echo .................... trying ftoc_alliant.h .......................... >&2
if [ -f $DIR/ftoc_alliant.exe -a -s $DIR/ftoc_alliant.exe ] ; then
        $DIR/ftoc_alliant.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="alliant"
           show_result
        fi
fi

echo .................... trying ftoc_fx.h ............................... >&2
if [ -f $DIR/ftoc_fx.exe -a -s $DIR/ftoc_fx.exe ] ; then
        $DIR/ftoc_fx.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="fx"
           show_result
        fi
fi

echo .................... trying ftoc_amd64.h ............................ >&2
if [ -f $DIR/ftoc_amd64.exe -a -s $DIR/ftoc_amd64.exe ] ; then
        $DIR/ftoc_amd64.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="amd64"
           show_result
        fi
fi

echo .................... trying ftoc_amd64gcc4.h ......................... >&2
if [ -f $DIR/ftoc_amd64gcc4.exe -a -s $DIR/ftoc_amd64gcc4.exe ] ; then
        $DIR/ftoc_amd64gcc4.exe >/dev/null 2>&1
        if [ $? -eq "0" ] ; then
           FTOC_PARAM="amd64gcc4"
           show_result
        fi
fi



echo "" >&2
echo "#################################################################" >&2
echo "# ftoc_params: ERROR. Unexpected Fortran to C interface      	#" >&2
echo "# Unable to set your <ftoc.h> file                           	#" >&2
echo "# MIDAS installation will fail if you do not set your own    	#" >&2
echo "# <ftoc.h> in \$MIDASHOME/\$MIDVERS/incl                     	# ">&2
echo "#                                                            	#" >&2
echo "# Please report this to ESO-IPG.                             	#" >&2
echo "#         INTERNET:midas@eso.org                             	#" >&2
echo "#################################################################" >&2
echo "" >&2
exit 1
 
