#! /bin/sh
# .COPYRIGHT: Copyright (c) 1988-2006 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           ftoc_params.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS  	  Test for Fortran to C interface     
# .REMARKS
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    02-Jan-1991:	Implementation
# .VERSION 1.2    04-Oct-1991:	eval has been deleted because apollo complains.
#				but could also be used within quotes.
# 060320	last modif


echo  ftoc_pc3.h          #return the result include file

 
