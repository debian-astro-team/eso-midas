#! /bin/sh
#  @(#)ftoc_nam.sh	19.1 (ESO-IPG) 02/25/03 14:31:08
# .COPYRIGHT: Copyright (c) 1988 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           ftoc_nam.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Test for Fortran to C interface. The output is used in
#		  the compilation of the file "ftoc_vmrc.fc".
# .REMARKS
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    02-Jul-1992:  Implementation

# <dirname> emulated with <sed>
#   DIR=`dirname $0`
DIR=`echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`

value=`$DIR/ftoc_names.exe`
case $value in
-fl) echo "-DNAMES=0" ;;
-fl_) echo "-DNAMES=1" ;;
-f_l_) echo "-DNAMES=2" ;;
-f_l) echo "-DNAMES=3" ;;
-fu) echo "-DNAMES=4" ;;
-fu_) echo "-DNAMES=5" ;;
-f_u_) echo "-DNAMES=6" ;;
-f_u) echo "-DNAMES=7" ;;
*)
esac
