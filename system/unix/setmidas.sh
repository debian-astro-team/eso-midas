#!/bin/sh
#
# @(#)setmidas.sh	19.1 (ESO-IPG) 02/25/03 14:32:00
# .COPYRIGHT: Copyright (c) 1988 European Southern Observatory,
#
# .TYPE		command
# .NAME		setmidas.csh
# .LANGUAGE	Bourne-shell script
# .ENVIRONMENT	Unix Systems. Executable ONLY under "sh" or "bash" with the
#               command "."
# .USAGE	. <path_name>/setmidas.sh
# 		It can be simplified with an environment definition:
#		setmidas=". <path_name>/setmidas.sh"
#		and executed as $setmidas.
# .COMMENTS	Interface to the MIDAS environment. It sets the variables
#		MIDASHOME, MIDVERS, MIDOPTION & MIDWORK for the next script 
#		"inmidas".
#
# .REMARKS 	This file helps users to chose between several releases of 
#		MIDAS. It is not necessary if you have only one release of
#		MIDAS installed.
#
# .REMARKS 	MIDASHOME0 & MIDVERS0 are set to defaults values by option 
#		"8- MIDAS setup" in the MIDAS "config" script.
#
# .VERSION 1.1	910221:	CG. Implementation
# .VERSION 2.1	930706:	CG. Adding options
#-------------------------------------------------------------------------

#**************************************************************************
# Configure Here: 
# ---------------
# Modify the values of MIDVERS? and MIDASHOME? accordingly to your site. 
# MIDVERS0 and MIDASHOME0 represents the default release of MIDAS.
#
# Other releases of MIDAS can be invoked from here by setting MIDVERS[123]
#
# MIDVERS0: Release of MIDAS by default. Cumpulsory.
#
MIDVERS0=test
MIDASHOME0=/auto/home/ns2b/midas
#
# Other MIDAS releases here. Set them in comments if you do not use them
#
#MIDVERS1=
#MIDASHOME1=/midas
#
#MIDVERS2=
#MIDASHOME2=/midas
#
#MIDVERS3=
#MIDASHOME3=/midas
#
# END of Configure Here
#**************************************************************************
export MIDASHOME MIDVERS MIDOPTION MID_WORK

#
# Check echo command
#
necho=echo
if [ "`echo -n `" = "-n" ]; then
  SV_NONL="\c"
else
  necho="echo -n"
fi

#
# Check MIDVERS0 variable is defined.
#
if [ -z "$MIDVERS0" ]; then
	echo "MIDVERS0: Undefined variable."
	echo "File <setmidas.sh> must contain this definition."
	echo "Call your MIDAS operator. EXIT."
	shift >/dev/null 2>&1   # Equivalent to exit
fi

#
# Check MIDASHOME0 variable is defined.
#
if [ -z "$MIDASHOME0" ]; then
	echo "MIDASHOME0: Undefined variable."
	echo "File <setmidas.sh> must contain this definition."
	echo "Call your MIDAS operator. EXIT."
	shift >/dev/null 2>&1   # Equivalent to exit
fi

#
# Here comes output-text
#
VERS_AVAIL="Versions of MIDAS available: $MIDVERS0 $MIDVERS1 $MIDVERS2 $MIDVERS3"
ENTER_VERS="Enter MIDAS version (default="
OPT_AVAIL="MIDAS option: PARALLEL, NOPARALLEL"
ENTER_OPT="Enter MIDAS option (default="
DEF_WORK="Default MIDAS startup directory= "
ENTER_WORK="Enter MIDAS startup directory: "

#
# If MIDVERS variable not defined then set to default.
#
if [ -z "$MIDVERS" ]; then
   MIDVERS=$MIDVERS0
fi

#
# Check MIDAS versions:
# If none of them then set to default.
#
case  "$MIDVERS" in
"$MIDVERS0")
   MIDVERS=$MIDVERS0
   MIDASHOME=$MIDASHOME0
   ;; 
"$MIDVERS1")
   MIDVERS=$MIDVERS1
   MIDASHOME=$MIDASHOME1
   ;; 
"$MIDVERS2")
   MIDVERS=$MIDVERS2
   MIDASHOME=$MIDASHOME2
   ;; 
"$MIDVERS3")
   MIDVERS=$MIDVERS3
   MIDASHOME=$MIDASHOME3
   ;;
*)
   MIDVERS=$MIDVERS0
   MIDASHOME=$MIDASHOME0
   ;;
esac

#
# Check for another MIDVERS
#
while :
do
  echo ""
  echo $VERS_AVAIL
  $necho ${ENTER_VERS}${MIDVERS}"): " $SV_NONL
  read NEWMIDVERS

  case "$NEWMIDVERS" in
  '') 	
     ;;
  "$MIDVERS0")
     MIDVERS=$MIDVERS0
     MIDASHOME=$MIDASHOME0
     ;;
  "$MIDVERS1")
     MIDVERS=$MIDVERS1
     MIDASHOME=$MIDASHOME1
     ;;
  "$MIDVERS2")
     MIDVERS=$MIDVERS2
     MIDASHOME=$MIDASHOME2
     ;;
  "$MIDVERS3")
     MIDVERS=$MIDVERS3
     MIDASHOME=$MIDASHOME3
     ;;
  *)
     continue
     ;;
  esac
  break
done

#
# MIDOPTION set to default
#
if [ "$MIDOPTION" != "PARALLEL" ]; then
   MIDOPTION=NOPARALLEL
fi

while :
do
  echo ""
  echo $OPT_AVAIL
  $necho ${ENTER_OPT}${MIDOPTION}"): " $SV_NONL
  read NEWMIDOPT

  case "$NEWMIDOPT" in
  P*|p*)
    MIDOPTION=PARALLEL
    ;;
  N*|n*)
    MIDOPTION=NOPARALLEL
    ;;
  '')
    ;;
  *)
    continue
    ;;
  esac
  break
done

#
# MID_WORK set to default
#
if [ -z "$MID_WORK" ]; then
  MID_WORK=$HOME/midwork
fi

echo ""
echo ${DEF_WORK}${MID_WORK}
$necho ${ENTER_WORK} $SV_NONL
read NEWMIDOPT
case "$NEWMIDOPT" in
'')
  ;;
*)
  MID_WORK=$NEWMIDOPT
  ;;
esac
