#! /bin/sh
# @(#)add_sccs.sh	19.1 (ESO-IPG) 02/25/03 14:31:58 

cmd=`basename $0`
A=%
header=" ${A}W% (ESO-IPG) ${A}G% ${A}U%"

if [ -z "$1" ]; then
  echo "Usage: $cmd files"
  exit 1
fi

for file in $*
do
    if [ ! -f "$file" ]; then
	echo "${cmd}: $file not found"
   	continue
    fi
    case $file in

    *.c|*.h|*.fc)
       echo '/*'$header' */' > .$file
       ;;

    *.for|*.incl)
       echo 'C'$header > .$file
       ;;

    makefile|*.sh|*.csh)
       echo '#'$header > .$file
       ;;

    *.prg|*.ctx)
       echo '!'$header > .$file
       ;;

    *.com)
       echo '$ !'$header > .$file
       ;;

    *.hlq|*.doc|*.alc|*.alq|*.twh|*.tex)
       echo '%'$header > .$file
       ;;

    *.n)
       echo '.\"'$header > .$file
       ;;

    *.mar)
       echo ';'$header > .$file
       ;;

    *)
       echo 'File '$file' requires no SCCS header'
       touch .$file
       ;;

    esac

   cat $file >> .$file
   mv .$file $file

done


