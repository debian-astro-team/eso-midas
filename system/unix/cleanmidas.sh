#! /bin/sh
# @(#)cleanmidas.sh	19.1 (ESO-IPG) 02/25/03 14:31:58
#
# cleanrelease procedure
# remove all dependent files
# .VERSION 1.1		04 Nov 88 - C. Guirao [ESO-IPG]

# Determine how to set prompt
if [ "`echo -n a`" = a ]; then
        # BSD echo
        NNN=-n
        CCC=
else
        # USG echo
        NNN=
        CCC='\c'
fi

necho()
{
        #
        #  A safe way to perform echo without sending a new-line
        #

        echo $NNN "$*" $CCC > /dev/tty
}

ask_yn()
{
        #
        #  Get a yes/no answer from the user; $1 is the prompt, $2 the default
        #

        prompt="$1"
        def="${2:-y}"           # yes is the default if not supplied
        while true; do
                necho "$prompt [$def]: "
                read answer
                answer="${answer:-$def}"
                case "$answer" in
                        [yY])
                                answer=y
                                break
                                ;;
                        [nN])
                                answer=n
                                break
                                ;;
                          *)
                                error Please enter "'y'" or "'n'".
                                ;;
                esac
        done
        [ $answer = y ]
}

MID_HERE=`pwd`
if [ -z "$MIDVERS" ] ; then
  MIDVERS=`echo $MID_HERE | sed 's/^.*midas\/\([^\/]*\).*$/\1/'`
fi
if [ -z "$MIDASHOME" ] ; then
  MIDASHOME=`echo $MID_HERE | sed 's/\/'$MIDVERS'.*$//'`
fi
MID_HOME=$MIDASHOME/$MIDVERS

export MIDASHOME MIDVERS

if ask_yn "REMOVING DEPENDENT FILES UNDER $MIDASHOME/$MIDVERS"; then
  echo ""
else
  exit 0
fi

cd $MIDASHOME/$MIDVERS

echo "rm -rf install/vms install/unix/test install/unix/systems"
rm -rf install/vms install/unix/test install/unix/systems
echo "rm -rf local/shlib local/preinstall local/setup local/README"
rm -rf local/util local/shlib local/preinstall local/setup local/README
echo "rm -rf test tmp contrib/lib"
rm -rf test tmp contrib/lib
echo "rm -rf util/bdf2ps util/help util/libsrc util/ltape util/tapeserv"
rm -rf util/bdf2ps util/help util/libsrc util/ltape util/tapeserv
echo "rm -rf pipeline"
echo "mkdir pipeline"
echo "chmod 775 pipeline"
echo "touch pipeline/EMPTY"
rm -rf pipeline
mkdir pipeline
chmod 775 pipeline
touch pipeline/EMPTY

echo "rm -f *.o *.for *.tex *.mar *.fc *.doc *.csh *.com *.f *.c"
echo "rm -f SELECTED DEPENDENCIES"

find . \( \
	-name "*.o" -o \
        -name "*.for" -o  \
        -name "*.tex" -o  \
        -name "*.mar" -o  \
        -name "*.fc" -o  \
        -name "*.doc" -o  \
        -name "*.csh" -o  \
        -name "*.com" -o  \
        -name "*.lasc" -o  \
        -name "*.iasc" -o  \
        -name "*.f" -o  \
        -name "SELECTED" -o \
        -name "DEPENDENCIES" -o \
        -name "*.c" \) -exec rm -f {} \;

cd $MIDASHOME/$MIDVERS/system/exec
echo "rm -f computer.exe ftoc_fx.exe ftoc_hp*  ftoc_osf.exe"
rm -f computer.exe ftoc_fx.exe ftoc_hp*  ftoc_osf.exe
echo "rm -f ftoc_pc.exe ftoc_sun.exe  ftoc_titan.exe  fontsup.exe"
rm -f ftoc_pc.exe ftoc_sun.exe  ftoc_titan.exe  fontsup.exe

cd $MIDASHOME/$MIDVERS
echo "rm -rf ./gui/GraphLib ./gui/incl ./gui/lib/*.a"
rm -rf ./gui/GraphLib ./gui/incl ./gui/lib/*.a

cd $MIDASHOME/$MIDVERS/lib

# Libraries included in libmidas.a and libgmidas.a
# For 95NOV also: rm -f libdio.a libdsp.a
rm -f libos.a libftoc.a libst.a libtbl.a libidicl.a libagl3.a libplot.a 
#
rm -f libreadline.a libmath.a libtw.a libtw3.a ../monit/libprep.a
#
# Libraries from applic
#rm -f libfit.a libfituser.a libagen.a libaplot.a libstat.a
