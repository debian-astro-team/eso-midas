#!/bin/sh
# @(#)ld77.sh	19.1 (ESO-IPG) 02/25/03 14:31:59

# 
# Script to link Fortran with C in PC/Alpha using f2c 
# The script just rewrites the link command putting the arguments -lf2c -lm at
# the end. Otherwise the linker will report not resolve definitions 

exec cc `echo $* | sed 's/-o/-lf2c -lm -o/'`
