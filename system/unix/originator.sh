#!/bin/sh
# @(#)originator.sh	19.1 (ESO-IPG) 02/25/03 14:32:00
# Copyright (C) 1993 Free Software Foundation, Inc.

[ -z "$TMPDIR" ] && TMPDIR=/tmp
TEMP=$TMPDIR/p$$

# Find out the name of the originator of this PR.
if [ -n "$NAME" ]; then
  ORIGINATOR="$NAME"
elif [ -f $HOME/.fullname ]; then
  ORIGINATOR="`sed -e '1q' $HOME/.fullname`"
else 
  grep "^$LOGNAME:" /etc/passwd | cut -f5 -d':' | sed -e 's/,.*//' > $TEMP
  ORIGINATOR="`cat $TEMP`"
  rm -f $TEMP
fi

if [ "$ORIGINATOR" = "" -a -f /bin/domainname ]; then
  if [ "`/bin/domainname`" != "" -a -f /usr/bin/ypcat ]; then
    # Must use temp file due to incompatibilities in quoting behavior
    # and to protect shell metacharacters in the expansion of $LOGNAME
    /usr/bin/ypcat passwd 2>/dev/null | cat - /etc/passwd | grep "^$LOGNAME:" |
      cut -f5 -d':' | sed -e 's/,.*//' > $TEMP
    ORIGINATOR="`cat $TEMP`"
    rm -f $TEMP
  fi
fi

echo $ORIGINATOR
exit 0
