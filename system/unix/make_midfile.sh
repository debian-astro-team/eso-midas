#! /bin/sh
# @(#)make_midfile.sh	19.1 (ESO-IPG) 02/25/03 14:32:00
# .TYPE           command
# .NAME           make_midfile.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Generates the midfile.map for the current directory
#                 midfile.map are used on HP systems with the command
#                 mapnames to recreate the original Unix File System using
#                 links, from a CD-ROM without RockRidge extensions.
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    22-Jun-1995:          Implementation

# Call this script with absolute pathname
#echo $0 | egrep -s "^/" 
#if [ $? != 0 ]; then
#  echo "Use the absolute pathname to execute this script"
#  exit 1
#fi

dir=${1:-.}

cd $dir

# Remove previous midfile.map if exists
rm -f MIDFILE.MAP

echo "Creating MIDFILE.MAP in " `pwd`  

ls -l | gawk '
{
  FILENAME=""
  EXT=""

  if ( NF < 8 || $8 == "." || $8 == ".." ) continue

  original=$8
  file=$8
  flags=$1
  n=split(file,tmp,".")
  if ( n > 1 ) {
    EXT=toupper(substr(tmp[n],0,3))
    file=(substr(file,0,length(file)-length(tmp[n])-1))
    }

  FILENAME=toupper(substr(file,0,8))
  gsub(/\./,"_",FILENAME)

  type=substr(flags,0,1)
  if ( type == "-" ) {
     TOTAL=FILENAME "." EXT ";1"
     printf "F %-14s                           %s\n",TOTAL,original
     }
  if ( type == "d" ) 
     printf "D %-14s                           %s\n",FILENAME,original
}' > MIDFILE.MAP

exit 0
