#! /bin/sh
# @(#)gomidas.sh	19.1 (ESO-IPG) 02/25/03 14:31:59
# .COPYRIGHT: Copyright (c) 1988 European Southern Observatory,
#
# .TYPE         command
# .NAME         gomidas
# .LANGUAGE     Bourne-shell script
# .ENVIRONMENT  Unix Systems. Executable under any SHELL.
# .USAGE        gomidas [-d display] [-m mid_work] 
#                 display:   X server for display and graphical windows.
#                 mid_work:  MIDAS startup directory (default: $HOME/mid_work)
#
# .COMMENTS     Resume a stopped session of MIDAS. It uses the following 
#		environment MID_WORK and DISPLAY
#               variables if they exist, otherwise takes defaults.
#               It checks arguments if any and exec to
#               $MIDASHOME/$MIDVERS/monit/prepa.exe
#
# .VERSION 1.1  900308: KB. Implementation
# .VERSION 2.1  920221: CG. Adapted to Unix scripts.
# .VERSION 3.1  930706: CG. Simplified and passing arguments.
#**************************************************************************

#
# Check for options:
#
while [ -n "$1" ]
do
  case $1 in
  -d)  DISPLAY=$2; export DISPLAY; shift; shift ;;
  -m)  MID_WORK=$2; export MID_WORK; shift; shift ;;
  *)  echo "Usage: gomidas [-d DISPLAY] [-m MID_WORK]"; exit 1 ;;
  esac
done

if [ -z "$MID_WORK" ]; then
  MID_WORK=$HOME/midwork/ ; export MID_WORK
fi

#
# if last char. of MID_WORK != '/'  append a '/'
#
if [ `expr $MID_WORK : '.*\(.\)'` != '/' ]; then
  MID_WORK=$MID_WORK/ ; export MID_WORK
fi

if [ ! -d $MID_WORK ]; then
  echo "Directory $MID_WORK does not exist"
  echo "Run inmidas first. EXIT."
  exit 1
fi

#
# Strip "/dev/", strip "pty/" (HP), "pts/" (IBM), and "tty" (all)
#
if ( tty -s )
then
  tt=`tty | sed -e 's/^.*dev\///' -e 's/pty\///' -e 's/pts\///' -e 's/tty//'`
else
  tt=none
fi

#
# Get's environment from previous inmidas
#
if [ ! -f $MID_WORK/.inmidas.$tt ]; then
  if [ ! -f $MID_WORK/.inmidas ]; then
    echo "No MIDAS session finished yet."
    echo "Run inmidas first. EXIT."
    exit 1
  else
    . $MID_WORK/.inmidas
    cp $MID_WORK/.inmidas $MID_WORK/.inmidas.$tt
  fi
else
  . $MID_WORK/.inmidas.$tt
fi

#
# Check if there is a running session already for DAZUNIT.
#
if [ -f "$MID_WORK/RUNNING$DAZUNIT" ]; then
  echo "Unit $DAZUNIT is locked by another MIDAS session. EXIT."
  exit 1
fi

#
# Check access to directories and files
#
if [ ! -d $MIDASHOME ]; then
  echo "${MIDASHOME}: not such directory."
  echo "Call your MIDAS operator. EXIT."
  exit 1
fi
if [ ! -d $MIDASHOME/$MIDVERS ]; then
  echo "$MIDASHOME/${MIDVERS}: not such directory."
  echo "Call your MIDAS operator. EXIT."
  exit 1
fi

#
# get all the variables (logical names)
#
if [ -f $MIDASHOME/$MIDVERS/monit/midlogs.sh ]; then
  . $MIDASHOME/$MIDVERS/monit/midlogs.sh
else
  echo "$MIDASHOME/$MIDVERS/monit/midlogs.sh not such file."
  echo "Call your MIDAS operator. EXIT."
  exit 1
fi

#
# logical assignements to devices. 
#
if [ -f $MID_WORK/devices.sh ] ; then
  . $MID_WORK/devices.sh
else
  . $MIDASHOME/$MIDVERS/monit/devices.sh
fi

#
# Catch any possible interrupt and remove the lock file
#
trap "rm -f $MID_WORK/RUNNING$DAZUNIT; exit 1" 1 2 3 15
touch $MID_WORK/RUNNING$DAZUNIT

#
# set TERMWIN to "yes" in any other previous release to 94NOV 
#
#if [ -z "$TERMWIN" -a "$MIDVERS" != "94NOV" ]; then
#  TERMWIN=yes; export TERMWIN
#fi


#
# Ignore CTRL-C before runnning prepa.exe
#
trap "" 2

#
# Set PATH for shared libraries
#
os=`uname`
case "$os" in
    "SunOS"|"Linux")
	if [ -z "$LD_LIBRARY_PATH" ] ; then
	  LD_LIBRARY_PATH=$MIDASHOME/$MIDVERS/lib
	else
	  LD_LIBRARY_PATH=$MIDASHOME/$MIDVERS/lib:$LD_LIBRARY_PATH
	fi
	export LD_LIBRARY_PATH
	;;
    "HP-UX")
	if [ -z "$SHLIB_PATH" ] ; then
	  SHLIB_PATH=$MIDASHOME/$MIDVERS/lib
	else
	  SHLIB_PATH=$MIDASHOME/$MIDVERS/lib:$SHLIB_PATH
	fi
	export SHLIB_PATH
	;;
     *)
	;;
esac

#
# run PREPA
#
$MID_MONIT/prepa.exe
if  [ $? = 1 ] ; then
  echo "...Midas session aborted..."
  rm -f $MID_WORK/RUNNING$DAZUNIT
  exit 1
fi

cp $MID_WORK/.inmidas.$tt $MID_WORK/.inmidas

rm -f $MID_WORK/RUNNING$DAZUNIT

exit 0
