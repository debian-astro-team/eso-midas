#!/bin/sh
# @(#)nmgrep.sh	19.1 (ESO-IPG) 02/25/03 14:32:00
pattern=$1
shift
for i in $*
do
  	# -h option not always recongnized (Sony/News)
	if (test -h $i 2>/dev/null); then
	   ls -l $i
	   continue
        fi
	echo "FILE: $i"
	nm $i | grep -i $pattern
done
