.\" @(#)inmidas.l	19.1 (ESO-IPG) 02/25/03 14:32:14
.\" Copyright (c) 1989 European Southern Observatory.
.\" All rights reserved.  
.\"
.TH inmidas 1L "11 November 1994" MIDAS
.SH NAME
inmidas \- MIDAS start-up procedure for users.
.SH SYNOPSIS
.B inmidas
[
.I unit
] [
.B \-h
.I midashome
] [
.B \-r
.I midvers
] 
.br 
[
.B \-d
.I display
] [
.B \-m
.I mid_work
] [
.B \-p/-P/-nop
] [
.B \-noh
] 
.br
[
.B \-j
.I midas-command-line
] [ 
.B \-help
]
.SH DESCRIPTION
.I inmidas 
starts a new MIDAS session. 
.PP
Without arguments,
.I inmidas 
initiates a MIDAS session with default definitions.
Some of these definitions can be modified with arguments in the command line of
.I inmidas
or by environment variables. Arguments in the command
line are taken with preference to environment variables.
.SH OPTIONS
.I inmidas 
has been configured by the operator at installation time to start a release
of MIDAS located in 
.IR $MIDASHOME0/$MIDVERS0 
(defined internally in the script).
However alternative releases can be specified using the:
.IP "\fB-h\fp \fImidashome\fp"
Home directory for MIDAS. Absolute pathname containing, at least, one release 
of MIDAS. It may also contain subdirectories for demo and calibration data.
.IP "\fB-r\fp \fImidvers\fp"
Release of MIDAS to be executed. It must be a subdirectory under 
.IR midashome.
.IP "\fB-d\fp \fIdisplay\fp"
Specifies another X server for the display and graphical MIDAS windows (
.B NOTE:
be aware of allowed access to a remote X server using the
.IR xhost (1)
command.)
.IP "\fB-p/-P/-nop\fp"
Options 
.I -p
and 
.I -P
set the MIDAS environment variable MIDOPTION to PARALLEL while option
.I -nop
sets it to NOPARALLEL (default: NOPARALLEL).
In NOPARALLEL mode all intermediate MIDAS working files in the MIDAS startup 
directory are deleted when starting MIDAS via 
.I inmidas.
In PARALLEL mode no intermediate files are deleted, and this is necessary
to run several MIDAS sessions with the same startup directory.
With 
.I -P 
option and if not 
.I unit 
is given the system will select
automatically one free unit for you. With 
.I -p 
option and no 
.I unit
, the user will be requested to enter one.
.IP "\fIunit\fp"
Unit to be associated to the MIDAS session (default: 00 only if MIDAS is
working in NOPARALLEL mode). Valid values for
this option are in the range (00, 01, ..., 99, xa, ..., zz) where numerical
values indicates that the user is working on a X11 environment (DISPLAY 
environment or argument
.B -d
should be given),
and the rest indicates an ASCII or graphical terminals others than X11.
.IP "\fB-m\fp \fImid_work\fp"
Specifies the MIDAS startup directory (default: $HOME/midwork). MIDAS will
create this directory if it does not exist and writes in there temporary
files used by MIDAS (e.g.: FORGRnn.LOG that logs the MIDAS session). It is
also the directory for automatic start procedures like the "login.prg" file
and for the "devices.sh" file containing particular MIDAS device-names 
defined by users.
At the end of the MIDAS session
.I inmidas
will saved the MIDAS session in a hidden file located in this directory.
This file will be then used later by the
.I gomidas
command to resume this MIDAS session where it exited.
.IP "\fB-noh\fp"
Starts MIDAS without clearing the terminal and without any welcome message.
.IP "\fB-j\fp \fImidas command line\fp"
.I midas-command-line
will be executed in MIDAS as if it were the first
command line typed in the MIDAS monitor. This option sets also 
.B \-noh 
option.
.B NOTE:
.I midas-command-line 
should be typed between single quotes to be interpreted by
.I inmidas 
as a single argument and to be passed to the MIDAS monitor as it is.
.IP "\fB-help\fp"
Display a brief description of all options and exits quietly.
.SH ENVIRONMENT
.I inmidas
can also use the following definitions in 
the environment with the same performance as previous described arguments.
.br
.B NOTE:
Arguments in the 
.I inmidas
command line are taken with preference to environment variables.
.IP "\fBMIDASHOME\fp"
Home directory for MIDAS.
.IP "\fBMIDVERS\fp"
Release of MIDAS.
.IP "\fBDISPLAY\fp"
X server for the display and graphical MIDAS windows.
.IP "\fBMIDOPTION\fp"
Set to PARALLEL or NOPARALLEL.
.IP "\fBMID_WORK\fp"
Startup directory for MIDAS.
.SH SEE ALSO
.IR gomidas(1L)
