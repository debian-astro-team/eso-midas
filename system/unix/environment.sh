#!/bin/sh
# @(#)environment.sh	19.1 (ESO-IPG) 02/25/03 14:31:59
# Copyright (C) 1993 Free Software Foundation, Inc.

SYSTEM=`( [ -f /bin/uname ] && /bin/uname -a ) || \
        ( [ -f /usr/bin/uname ] && /usr/bin/uname -a ) || echo ""`
ARCH=`[ -f /bin/arch ] && /bin/arch`
MACHINE=`[ -f /bin/machine ] && /bin/machine`

[ -n "$SYSTEM" ] && echo System: $SYSTEM
[ -n "$ARCH" ] && echo Architecture: $ARCH
[ -n "$MACHINE" ] && echo Machine: $MACHINE

exit 0
