#! /bin/sh 
#
# Assign the necessary logical names for Midas
# needed for the Unix command `drs'
# Klaus Banse ESO - DMD                000503
# 
# 071108	last modif
# 
#**************************************************************************
# Configure Here:
# ---------------
# Modify the default values of MIDASHOME0 and MIDVERS0
# accordingly to your site.
#
MIDASHOME0=/midas
MIDVERS0=07SEP
#
# Environment variables MIDASHOME and MIDVERS overwrite the defaults above
#
#**************************************************************************

## set -x 

#
# If MIDASHOME variable not defined then set to default.
#
if [ -z "$MIDASHOME" ]; then
   MIDASHOME="$MIDASHOME0"; export MIDASHOME
fi

#
# If MIDVERS variable not defined then set to default.
#
if [ -z "$MIDVERS" ]; then
   MIDVERS="$MIDVERS0"; export MIDVERS
fi

#
# Check access to directories and files
#
if [ ! -d $MIDASHOME/$MIDVERS ]; then
  echo "$MIDASHOME/${MIDVERS}: not such directory."
  echo "Call your MIDAS operator. EXIT."
  exit 1
fi

#
# if necessary create MIDAS work directory
#
if [ -z "$MID_WORK" ]; then
   MID_WORK=$HOME/midwork/ ; export MID_WORK
fi

#
# get all the variables (logical names)
#
if [ -f $MIDASHOME/$MIDVERS/monit/midlogs.sh ]; then
  . $MIDASHOME/$MIDVERS/monit/midlogs.sh
else
  echo "$MIDASHOME/$MIDVERS/monit/midlogs.sh not such file."
  echo "Call your MIDAS operator. EXIT."
  exit 1
fi

#
# Catch any possible interrupt and remove the lock file
#
trap "exit 1" 1 3 15

#
# Set PATH for shared libraries
#
os=`uname`
case "$os" in
    "SunOS"|"Linux")
        if [ -z "$LD_LIBRARY_PATH" ] ; then
          LD_LIBRARY_PATH=$MIDASHOME/$MIDVERS/lib
        else
          LD_LIBRARY_PATH=$MIDASHOME/$MIDVERS/lib:$LD_LIBRARY_PATH
        fi
        export LD_LIBRARY_PATH
        ;;
    "HP-UX")
        if [ -z "$SHLIB_PATH" ] ; then
          SHLIB_PATH=$MIDASHOME/$MIDVERS/lib
        else
          SHLIB_PATH=$MIDASHOME/$MIDVERS/lib:$SHLIB_PATH
        fi
        export SHLIB_PATH
        ;;
     *)
        ;;
esac

# run QREQA
#
DAZUNIT=01; export DAZUNIT			# must be 01 ...
MIDAS_OUTSIDE=YES; export MIDAS_OUTSIDE
$MID_MONIT/qreqa.exe $*

exit 0
