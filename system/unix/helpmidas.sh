#! /bin/sh
# @(#)helpmidas.sh	19.1 (ESO-IPG) 02/25/03 14:31:59
# .COPYRIGHT: Copyright (c) 1988 European Southern Observatory,
#
# .TYPE         command
# .NAME         helpmidas
# .LANGUAGE     Bourne-shell script
# .ENVIRONMENT  Unix Systems. Executable under any SHELL.
# .USAGE        helpmidas [-h midashome] [-r midvers] [-d display] [-m mid_work]
#		  display:   X server for display and graphical windows.
#		  midashome: Home directory for MIDAS. (default: MIDASHOME0)
#		  midvers:   Version of MIDAS. (default: MIDVERS0)
#		  mid_work:  MIDAS startup directory (default: $HOME/mid_work)
#
# .COMMENTS     Script to start MIDAS Help. It uses the following environment:
#		MIDASHOME and MIDVERS variables if they exist, otherwise 
#	        takes defaults.
#		It checks arguments if any and exec to $GUI_EXE/XHelp.exe
#
# .REMARKS      MIDASHOME0 & MIDVERS0 are set to defaults values by option
#               "8- MIDAS setup" in the MIDAS "config" script.
#
# .VERSION 1.1	930803:	CG. Implementation
#**************************************************************************

#**************************************************************************
# Configure Here: 
# ---------------
# Modify the default values of MIDASHOME0 and MIDVERS0 
# accordingly to your site. 
#
# Environment variables MIDASHOME and MIDVERS overwrite these defaults.
#
# Done automaticaly by option "8- setup MIDAS" in the "config" script.
#
MIDASHOME0=/midas
MIDVERS0=95NOV
#
# END of Configure Here
#**************************************************************************

# Check for options:
#
while [ -n "$1" ]
do
  case $1 in
  -h)  MIDASHOME=$2; export MIDASHOME; shift; shift ;;
  -r)  MIDVERS=$2; export MIDVERS; shift; shift ;;
  -d)  DISPLAY=$2; export DISPLAY; shift; shift ;;
  -m)  MID_WORK=$2; export MID_WORK; shift; shift ;;
  *)  echo "Usage: helpmidas [-h midashome] [-r midvers] "
       echo "[-d display]  [-m mid_work]"
       exit 1 ;;
  esac
done

#
# If MIDASHOME variable not defined then set to default.
#
if [ -z "$MIDASHOME" ]; then
  if [ -z "$MIDASHOME0" ]; then
    echo "Environment variable MIDASHOME not defined" 
    echo "and no default MIDASHOME0 found in this script. EXIT." 
    exit 1
  else
    MIDASHOME="$MIDASHOME0"; export MIDASHOME
  fi
fi

#
# If MIDVERS variable not defined then set to default.
#
if [ -z "$MIDVERS" ]; then
  if [ -z "$MIDVERS0" ]; then
    echo "Environment variable MIDVERS not defined"
    echo "and no default MIDVERS0 found in this script. EXIT."
    exit 1
  else
    MIDVERS="$MIDVERS0"; export MIDVERS
  fi
fi

#
# Check access to directories and files
#
if [ ! -d $MIDASHOME ]; then
  echo "${MIDASHOME}: not such directory."
  echo "Call your MIDAS operator. EXIT."
  exit 1
fi
if [ ! -d $MIDASHOME/$MIDVERS ]; then
  echo "$MIDASHOME/${MIDVERS}: not such directory."
  echo "Call your MIDAS operator. EXIT."
  exit 1
fi

#
# get all the variables (logical names)
#
if [ -f $MIDASHOME/$MIDVERS/monit/midlogs.sh ]; then
  . $MIDASHOME/$MIDVERS/monit/midlogs.sh
else
  echo "$MIDASHOME/$MIDVERS/monit/midlogs.sh not such file."
  echo "Call your MIDAS operator. EXIT."
  exit 1
fi

#
# if necessary create MIDAS work directory
#
if [ -z "$MID_WORK" ]; then
  MID_WORK=$HOME/midwork/ ; export MID_WORK
fi

#
# if last char. of MID_WORK != '/'  append a '/'
#
if [ `expr $MID_WORK : '.*\(.\)'` != '/' ]; then
  MID_WORK=$MID_WORK/ ; export MID_WORK
fi

if [ ! -d $MID_WORK ]; then
  mkdir `expr $MID_WORK : '\(.*\).'`         #use MID_WORK except last char.
  if [ $? != 0 ]; then
    echo Could not create directory `expr $MID_WORK : '\(.*\).'`
    exit 1
  fi
fi

if [ -f $MID_MONIT/syskeys.unix ]; then
  SYSKEYS=$MID_MONIT/syskeys.unix
elif [ -f $MID_MONIT/syskeys.dat ]; then
  SYSKEYS=$MID_MONIT/syskeys.dat
else
  echo "No such file: $MID_MONIT/syskeys.unix or $MID_MONIT/syskeys.dat"
  exit 1
fi

mail=`awk -F/ '{ if (found == 1) {print $0; exit } if ($1 == "MID$MAIL") found=1 }' $SYSKEYS`

print=`awk -F/ '{ if (found == 1) {print $0; exit } if ($1 == "SYSCOMS") found=1 }' $SYSKEYS | sed -e 's/....................\(....................\).*/\1/'`

#echo MIDVERS=$MIDVERS
#echo mail=$mail
#echo print=$print
#echo MID_CONTEXT=$MID_CONTEXT
#echo MID_WORK=$MID_WORK
#echo GUI_EXE=$GUI_EXE

#
# Set PATH for shared libraries
#
os=`uname`
case "$os" in
    "SunOS"|"Linux")
	if [ -z "$LD_LIBRARY_PATH" ] ; then
	  LD_LIBRARY_PATH=$MIDASHOME/$MIDVERS/lib
	else
	  LD_LIBRARY_PATH=$MIDASHOME/$MIDVERS/lib:$LD_LIBRARY_PATH
	fi
	export LD_LIBRARY_PATH
	;;
    "HP-UX")
	if [ -z "$SHLIB_PATH" ] ; then
	  SHLIB_PATH=$MIDASHOME/$MIDVERS/lib
	else
	  SHLIB_PATH=$MIDASHOME/$MIDVERS/lib:$SHLIB_PATH
	fi
	export SHLIB_PATH
	;;
     *)
	;;
esac

# 
# run help.exe (94NOV) or XHelp.exe
#
if [ -f $GUI_EXE/help.exe ]; then
  echo "Preparing the HELP graphic user interface..."
  $GUI_EXE/help.exe $MIDVERS $mail "$print" $MID_CONTEXT $MID_WORK " "  &
elif  [ -f $GUI_EXE/XHelp.exe ]; then
  echo "Preparing the XHelp user interface..."
  $GUI_EXE/XHelp.exe $MIDVERS $mail "$print" $MID_CONTEXT $MID_WORK " "  &
else
  echo "No such file: $GUI_EXE/XHelp.exe"
  exit 1
fi

exit 0
