/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSIONS

 090709		last modif
===========================================================================*/

/* 
	fontsup.c  K.Banse	ESO-Graching

	execute via  fontsup.exe

*/

# include <stdio.h>
# include <idi.h>
# include <x11defs.h>
# include <proto_os.h>

Window w, fw;
char *gets();

 
int main(argc,argv)
int argc;
char *argv[];

{
char   *display = "";         /* XWindows unit searched in environment */
char   strf[160];
char **fontpntr, *pp, pattern[4];

int fcount, i;
static int fp;




/* Connect to X server */

#ifdef vms
mydisp[0] = XOpenDisplay("DECW$DISPLAY");
#else
mydisp[0] = XOpenDisplay(display);
#endif

if (!mydisp[0])
   {
   (void) printf("we got an error in XOpenDisplay!\n");
   return (-1);
   }
 

/* get a list of available fonts */

(void) strcpy(pattern,"*");
fontpntr = XListFonts(mydisp[0],pattern,3000,&fcount);
(void) printf("total no. of available fonts for X11 = %d\n",fcount);

fp = osaopen("x11fonts.dat",1);
if (fp == -1)
   (void) printf("Could not open fontfile x11fonts.dat...\n");


/*  loop through Font list  */

for (i=0; i<fcount; i++)
   {
   pp = *fontpntr++;
   sprintf(strf,"(%d): %s",i+1,pp);
   if (fp != -1)
      osawrite(fp,strf,(int)strlen(strf));
   }

if (fp != -1) (void) osaclose(fp);

return 0;
}
