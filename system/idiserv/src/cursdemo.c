/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


# include <stdio.h>
# include <math.h>
#include <idi.h>
#include <x11defs.h>


int show_cur(w,cu,curnam)
Window w;
unsigned int      cu;
char     *curnam;

{
Cursor xcu;
int n;
double varia, res;

printf("cursor: XC_%s\n",curnam);
xcu = XCreateFontCursor(mydisp[0],cu);
XDefineCursor(mydisp[0],w,xcu);
XFlush(mydisp[0]);

for (n=0; n<99999; n++)			/* just a waiting loop  */
   {
   varia = n;
   res = sqrt(varia);
   }
XFreeCursor(mydisp[0],xcu);
XFlush(mydisp[0]);
XSync(mydisp[0],1);		/* that's just to get the X errors  */

return 0;
}



int curs_demo(w)
Window w;

{
unsigned int cu;
char curnam[20];

/* the cursors 'num_glyphs', 'hand', 'handl_mask' and 'dot_box_mask'
   do not exist...                                                  */

strcpy(curnam,"X_cursor");
cu = XC_X_cursor;
show_cur(w,cu,curnam);
strcpy(curnam,"arrow");
cu = XC_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"based_arrow_down");
cu = XC_based_arrow_down;
show_cur(w,cu,curnam);
strcpy(curnam,"based_arrow_up");
cu = XC_based_arrow_up;
show_cur(w,cu,curnam);
strcpy(curnam,"boat");
cu = XC_boat;
show_cur(w,cu,curnam);
strcpy(curnam,"bogosity");
cu = XC_bogosity;
show_cur(w,cu,curnam);
strcpy(curnam,"bottom_left_corner");
cu = XC_bottom_left_corner;
show_cur(w,cu,curnam);
strcpy(curnam,"bottom_right_corner");
cu = XC_bottom_right_corner;
show_cur(w,cu,curnam);
strcpy(curnam,"bottom_side");
cu = XC_bottom_side;
show_cur(w,cu,curnam);
strcpy(curnam,"bottom_tee");
cu = XC_bottom_tee;
show_cur(w,cu,curnam);
strcpy(curnam,"box_spiral");
cu = XC_box_spiral;
show_cur(w,cu,curnam);
strcpy(curnam,"center_ptr");
cu = XC_center_ptr;
show_cur(w,cu,curnam);
strcpy(curnam,"circle");
cu = XC_circle;
show_cur(w,cu,curnam);
strcpy(curnam,"clock");
cu = XC_clock;
show_cur(w,cu,curnam);
strcpy(curnam,"coffee_mug");
cu = XC_coffee_mug;
show_cur(w,cu,curnam);
strcpy(curnam,"cross");
cu = XC_cross;
show_cur(w,cu,curnam);
strcpy(curnam,"cross_reverse");
cu = XC_cross_reverse;
show_cur(w,cu,curnam);
strcpy(curnam,"crosshair");
cu = XC_crosshair;
show_cur(w,cu,curnam);
strcpy(curnam,"diamond_cross");
cu = XC_diamond_cross;
show_cur(w,cu,curnam);
strcpy(curnam,"dot");
cu = XC_dot;
show_cur(w,cu,curnam);
strcpy(curnam,"double_arrow");
cu = XC_double_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"draft_large");
cu = XC_draft_large;
show_cur(w,cu,curnam);
strcpy(curnam,"draft_small");
cu = XC_draft_small;
show_cur(w,cu,curnam);
strcpy(curnam,"draped_box");
cu = XC_draped_box;
show_cur(w,cu,curnam);
strcpy(curnam,"exchange");
cu = XC_exchange;
show_cur(w,cu,curnam);
strcpy(curnam,"fleur");
cu = XC_fleur;
show_cur(w,cu,curnam);
strcpy(curnam,"gobbler");
cu = XC_gobbler;
show_cur(w,cu,curnam);
strcpy(curnam,"gumby");
cu = XC_gumby;
show_cur(w,cu,curnam);
strcpy(curnam,"heart");
cu = XC_heart;
show_cur(w,cu,curnam);
strcpy(curnam,"icon");
cu = XC_icon;
show_cur(w,cu,curnam);
strcpy(curnam,"iron_cross");
cu = XC_iron_cross;
show_cur(w,cu,curnam);
strcpy(curnam,"left_ptr");
cu = XC_left_ptr;
show_cur(w,cu,curnam);
strcpy(curnam,"left_side");
cu = XC_left_side;
show_cur(w,cu,curnam);
strcpy(curnam,"left_tee");
cu = XC_left_tee;
show_cur(w,cu,curnam);
strcpy(curnam,"leftbutton");
cu = XC_leftbutton;
show_cur(w,cu,curnam);
strcpy(curnam,"ll_angle");
cu = XC_ll_angle;
show_cur(w,cu,curnam);
strcpy(curnam,"lr_angle");
cu = XC_lr_angle;
show_cur(w,cu,curnam);
strcpy(curnam,"man");
cu = XC_man;
show_cur(w,cu,curnam);
strcpy(curnam,"middlebutton");
cu = XC_middlebutton;
show_cur(w,cu,curnam);
strcpy(curnam,"mouse");
cu = XC_mouse;
show_cur(w,cu,curnam);
strcpy(curnam,"pencil");
cu = XC_pencil;
show_cur(w,cu,curnam);
strcpy(curnam,"pirate");
cu = XC_pirate;
show_cur(w,cu,curnam);
strcpy(curnam,"plus");
cu = XC_plus;
show_cur(w,cu,curnam);
strcpy(curnam,"question_arrow");
cu = XC_question_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"right_ptr");
cu = XC_right_ptr;
show_cur(w,cu,curnam);
strcpy(curnam,"right_side");
cu = XC_right_side;
show_cur(w,cu,curnam);
strcpy(curnam,"right_tee");
cu = XC_right_tee;
show_cur(w,cu,curnam);
strcpy(curnam,"rightbutton");
cu = XC_rightbutton;
show_cur(w,cu,curnam);
strcpy(curnam,"rtl_logo");
cu = XC_rtl_logo;
show_cur(w,cu,curnam);
strcpy(curnam,"sailboat");
cu = XC_sailboat;
show_cur(w,cu,curnam);
strcpy(curnam,"sb_down_arrow");
cu = XC_sb_down_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"sb_h_double_arrow");
cu = XC_sb_h_double_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"sb_left_arrow");
cu = XC_sb_left_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"sb_right_arrow");
cu = XC_sb_right_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"sb_up_arrow");
cu = XC_sb_up_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"sb_v_double_arrow");
cu = XC_sb_v_double_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"shuttle");
cu = XC_shuttle;
show_cur(w,cu,curnam);
strcpy(curnam,"sizing");
cu = XC_sizing;
show_cur(w,cu,curnam);
strcpy(curnam,"spider");
cu = XC_spider;
show_cur(w,cu,curnam);
strcpy(curnam,"spraycan");
cu = XC_spraycan;
show_cur(w,cu,curnam);
strcpy(curnam,"star");
cu = XC_star;
show_cur(w,cu,curnam);
strcpy(curnam,"target");
cu = XC_target;
show_cur(w,cu,curnam);
strcpy(curnam,"tcross");
cu = XC_tcross;
show_cur(w,cu,curnam);
strcpy(curnam,"top_left_arrow");
cu = XC_top_left_arrow;
show_cur(w,cu,curnam);
strcpy(curnam,"top_left_corner");
cu = XC_top_left_corner;
show_cur(w,cu,curnam);
strcpy(curnam,"top_right_corner");
cu = XC_top_right_corner;
show_cur(w,cu,curnam);
strcpy(curnam,"top_side");
cu = XC_top_side;
show_cur(w,cu,curnam);
strcpy(curnam,"top_tee");
cu = XC_top_tee;
show_cur(w,cu,curnam);
strcpy(curnam,"trek");
cu = XC_trek;
show_cur(w,cu,curnam);
strcpy(curnam,"ul_angle");
cu = XC_ul_angle;
show_cur(w,cu,curnam);
strcpy(curnam,"umbrella");
cu = XC_umbrella;
show_cur(w,cu,curnam);
strcpy(curnam,"ur_angle");
cu = XC_ur_angle;
show_cur(w,cu,curnam);
strcpy(curnam,"watch");
cu = XC_watch;
show_cur(w,cu,curnam);
strcpy(curnam,"xterm");
cu = XC_xterm;
show_cur(w,cu,curnam);

return 0;
}
