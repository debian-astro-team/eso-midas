$ ! idigo.com
$ ! command procedure to start idi server in test mode (needs Midas unit AX)
$ ! 900131  KB
$ !
$ DAZUNIT := AX
$ WORK_STATION := DAZAX
$ ASSIGN ['MIDASHOME'.demo.data] MID_WORK 
$ !
$ RUN MID_DISK:['MIDASHOME'.'MIDVERS'.EXEC]kdiserv.exe
$ !
$ EXIT
