#! /bin/sh

# this is the IDI server in debug mode
# 1st start a Midas session with unit AX
# 2nd start this script
#
DAZUNIT=AX
export DAZUNIT
WORK_STATION=DAZAX
export WORK_STATION
I_TYPE="SXW  "
export I_TYPE
MID_WORK=$HOME/midwork/
export MID_WORK
MID_SYSTAB=$MIDASHOME/$MIDVERS/systab/bin
export MID_SYSTAB
# 
gdb $MIDASHOME/$MIDVERS/system/exec/idiserv.exe 
exit
