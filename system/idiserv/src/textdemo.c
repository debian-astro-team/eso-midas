/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


# include <stdio.h>
# include <math.h>
#include <idi.h>
#include <x11defs.h>

static int virgin;

extern int OSY_SLEEP(), XSWAP_INT(), XSWAP_SHORT();
/*

*/

void own_test(flag)
int flag;

{

if (flag == 1)
   {
   unsigned int  iorig, iout;
   unsigned short int  jorig, jout;
 
   iorig = 0xa1b2c3d4;
   iout = XSWAP_INT(iorig);
   (void) printf("input: %x   output: %x\n",iorig,iout);
 
   jorig = 0xabcd;
   jout = XSWAP_SHORT(jorig);
   (void) printf("input: %x       output: %x\n",jorig,jout);
   }

}

/*

*/

void text_demo(w,kgc)
Window w;
GC  kgc;

{
char cbuf[20];
char **fontpntr, *pp, pattern[4];

int fcount, limi, i;

void show_text();



/* get a list of available fonts */

/*  strcpy(pattern,"-*-*-*-*-*-*-*-*-*-*-*-*-*-*");  */
strcpy(pattern,"*");
fontpntr = XListFonts(mydisp[0],pattern,3000,&fcount);
(void) printf("total no. of available fonts = %d\n",fcount);

limi = 800;			/* default for medium delay */
(void) printf("Enter delay between different fonts, none/small/medium/large: ");
if (fgets(cbuf,8,stdin) != (char *) 0)
   {
   if (cbuf[0] == 'n')
      limi = 0;
   else if (cbuf[0] == 's')
      limi = 300;
   else if (cbuf[0] == 'l')
      limi = 2000;
   }

virgin = 0;

for (i=0; i<fcount; i++)
   {
   pp = *fontpntr++;
   show_text(w,kgc,pp,i,limi);
   virgin = 1;
   }

}

void show_text(w,gc,fontnam,no,waitlim)
Window w;
GC  gc;
char     *fontnam;
int  no, waitlim;

{
char   text[64];
int  x, y, ltxt, mm, kk;

mm = 0;
if (virgin == 0) 
   {
   x = 0;
   y = 50;
   XClearArea(mydisp[0],w,x,y,0,0,True);
   goto do_it;
   }


/*  waiting loop  */

OSY_SLEEP(waitlim,1);

x = 0;
mm = no % 4;
y = 50 + mm*100;
if (mm == 3)
   kk = 0;
else
   kk = 100;

XClearArea(mydisp[0],w,x,y,0,kk,True);

do_it:
(void) printf("(%d): %s\n",no,fontnam);
(void) sprintf(text,"(%d):  Viva Portable Midas...",no);

myfont[0][0] = XLoadQueryFont(mydisp[0],fontnam);
if (myfont[0][0] == (XFontStruct *) 0)
   (void) printf("Could not load the specified Font - so we skip ...\n");
else
   {
   XSetFont(mydisp[0],gc,myfont[0][0]->fid);
   x = 10;
   y = 100 + mm*100;
   ltxt = (int) strlen(text);
   XDrawString(mydisp[0],w,gc,x,y,text,ltxt);
   XFlush(mydisp[0]);
   XFreeFont(mydisp[0],myfont[0][0]);
   }
}
 
