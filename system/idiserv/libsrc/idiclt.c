/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        idiclt
.LANGUAGE    C
.AUTHOR      K. Banse, adapted from an original X10 version of M. Pucillo,
             Trieste Astronomical Observatory
.CATEGORY    Image Display Interfaces. Special library for use with server
.COMMENTS
 The IDI workstation server (IDIWS) allows application programs to
 access image display (virtual) devices without being concerned
 with their creation and maintenance. Many applications can
 access the same device in sequence, or different devices at
 the same time. This library MUST be used when IDI are accessed
 through the server IDIWS.
 Errors are managed the same way as IDI errors.
.HISTORY [0.0] Nov-1987   definition,  MP (TAO)
.HISTORY [1.0] 890102     fix IIGPLY for large buffers   KB
.HISTORY [3.5] 950118	  add IIZWSZ, remove IIZWZP and IIZRZP

.VERSION

 090120		last modif

------------------------------------------------------------*/ 

#include <stdlib.h>
#include <midas_def.h>
#include <idi.h>
#include <idifunct.h>
#include <idiserver.h>
#include <oserror.h>


static int    open_device = 0;     /* no. of requested connections  */
static int    fid, nbytes, osxchan, osx_cod, outsize;

static char   *idiservername[2];
static char   *pipedir, unitnam[4];
static char   errtxt[80], dfilnam[132];
static char   *charbuf, *memadr;


/*

*/

/* **************************************************************** 

              Open osx connection to IDI server

   **************************************************************** */

int IDI_SINI()

{
int    i, mode;

char   *osmmget();



if (!(pipedir = getenv("MID_WORK"))) {
   (void) printf ("!! MID_WORK not defined !!\n");
   ospexit (1);
   }

OSY_GETSYMB("DAZUNIT",unitnam,4);
unitnam[2] = '\0';

#if vms
 i = 16;
 idiservername[0] =  osmmget((unsigned int) i);
 (void) sprintf(idiservername[0],"%sXW",unitnam);
 mode = LOCAL | CLIENT | IPC_WRITE;

#else
 i = strlen(pipedir) + 20;
 idiservername[0] = osmmget((unsigned int) i);
 (void) sprintf(idiservername[0],"%smidas_xw%s",pipedir,unitnam);
 mode = LOCAL | IPC_WRITE;
#endif /* vms */


/* open/attach the osx channel in WRITE mode (try at most 10 times) */

for (i=0; i<10; i++)
   {
   if ((osxchan = osxopen(idiservername, mode)) == -1)
      {
      OSY_SLEEP(500.0,1);		/* wait 500 msec */
      }
   else
      {					/* we made it - init structures */
      memset((char *)&serv_buf,0,sizeof(serv_buf));
      memset((char *)&serv_ret,0,sizeof(serv_ret));
      return 0;
      }
   }


(void) sprintf(errtxt,
   /* "IDI_SINI: We could not connect to IDIserver (error = %d)",osxerror); */
       "IDI_SINI: We could not connect to IDIserver (error = %d)",oserror);
SCTMES(M_RED_COLOR,errtxt);          /* use red color*/
return (-1);
} 

/*****************************************************************************/
      
void IDI_EXIT ()		/* kill server */
            
{
#define     EXIT_SIZE   BUFHEAD

serv_buf.nobyt   = EXIT_SIZE;
serv_buf.code_id = EXIT_CODE;

osx_cod = osxwrite(osxchan,(char *)&serv_buf,serv_buf.nobyt);
#if vms
if (osx_cod != 0)
#else
if (osx_cod <= 0)
#endif
   {
   (void) sprintf(errtxt,"OSX: Writing error in IDI_EXIT, status = %d",
                  osx_cod);
   SCTMES(M_RED_COLOR,errtxt); 
   ospexit(EXIT_CODE);
   }

osxclose (osxchan);
}

/*****************************************************************************/
      
void IDI_SCLS ()		/* close connection to IDI server  */
            
{
osxclose (osxchan);
osmmfree(idiservername[0]);		/* free allocated memory */
}

/*****************************************************************************/
      
void round_trip(size)
int size;

{

osx_cod = osxwrite(osxchan,(char *)&serv_buf,serv_buf.nobyt);

#if vms
if (osx_cod != 0)
#else
if (osx_cod <= 0)
#endif
   {
   (void) sprintf(errtxt,"### round_trip: osxwrite error = %d",osx_cod);
   SCTMES(M_RED_COLOR,errtxt);		/* use red color*/
   (void) sprintf(errtxt,"code = %d, want to write %d bytes, read %d bytes",
          serv_buf.code_id,serv_buf.nobyt,size);
   SCTMES(M_RED_COLOR,errtxt);
   return;
   }

osx_cod = osxread(osxchan,(char *)&serv_ret,size);

if (osx_cod != size)
   {
   (void) sprintf(errtxt,
          "### round_trip: osxread, asked for %d, got %d bytes...",
          size,osx_cod);
   SCTMES(M_RED_COLOR,errtxt);		/* use red color*/
   (void) sprintf(errtxt,"code = %d, want to write %d bytes, read %d bytes",
          serv_buf.code_id,serv_buf.nobyt,size);
   SCTMES(M_RED_COLOR,errtxt);
   }
return;
}

/* **************************************************************** */

int IIDOPN_C ( display , displayid )
int  *displayid;
char display[];
  
{
#define     DOPN_RET   RET_SIZE+4

int    txtlen, np;

if (open_device <= 0)		 /* if first time: connect to IDI server  */
   {
   if ((np = IDI_SINI()) != 0) 
      {
      (void) strcpy(errtxt,"Called from IIDOPN_C ...");
      SCTMES(M_RED_COLOR,errtxt);
      return (DCTFILERR);
      }
   open_device = 0;
   }
open_device ++;

txtlen = strlen(display) + 1;
np = ((txtlen % 4) == 0) ? (txtlen/4) : (txtlen/4 + 1);

serv_buf.nobyt   = np*4 + BUFHEAD;
serv_buf.code_id = DOPN_CODE;
  
charbuf = (char *)(&serv_buf.data.in[0]);
(void) strcpy(charbuf,display);

round_trip(DOPN_RET);

*displayid = serv_ret.data.in[0];
return (serv_ret.code);
}

/* **************************************************************** */

int IIDDEL_C (display,nodels,imindx,grindx)
char  display[];
int *nodels, *imindx, *grindx;

{
#define     DDEL_RET   RET_SIZE+12

int    txtlen, np;

if (open_device <= 0)
   {
   np = IDI_SINI();
   if (np != 0) 
      {
      (void) strcpy(errtxt,"Called from IIDDEL_C ...");
      SCTMES(M_RED_COLOR,errtxt);
      return (DCTFILERR);
      }
   }

txtlen = strlen(display) + 1;
np = ((txtlen % 4) == 0) ? (txtlen/4) : (txtlen/4 + 1);

serv_buf.nobyt   = np*4 + BUFHEAD;
serv_buf.code_id = DDEL_CODE;

charbuf = (char *)(&serv_buf.data.in[0]);
(void) strcpy(charbuf,display);

round_trip(DDEL_RET);

*nodels  = serv_ret.data.in[0];
*imindx  = serv_ret.data.in[1];
*grindx  = serv_ret.data.in[2];

open_device -= *nodels;              /* update open_device_counter */
if (open_device <= 0) IDI_SCLS(); /* close connection, if last */

return (serv_ret.code);
}

/* **************************************************************** */

int IIDCLO_C( display )
int display;
  
{
open_device --;
if (open_device < 0) return(DEVNOTOP);

serv_buf.nobyt   = BUFHEAD+4;
serv_buf.code_id = DCLO_CODE;
serv_buf.data.in[0] = display;

round_trip(RET_SIZE);

if (open_device <= 0) IDI_SCLS(); /* close connection, if last */
return (serv_ret.code);
}
/* **************************************************************** */

int IIDRST_C (display)
int display;

{
serv_buf.nobyt = BUFHEAD+4;
serv_buf.code_id = DRST_CODE;
serv_buf.data.in[0] = display;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIDICO_C (display,flag)
int display, flag;

{
serv_buf.nobyt = BUFHEAD+8;
serv_buf.code_id = DICO_CODE;
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = flag;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIEGDB_C (display,flag,auxid,cbuf,ibuf,rbuf)
int display, flag, auxid, *ibuf;
char  *cbuf;
float *rbuf;

{
int   i;

serv_buf.nobyt = 12 + BUFHEAD;
serv_buf.code_id = EGDB_CODE;

serv_buf.data.in[0] = display;
serv_buf.data.in[1] = flag;
serv_buf.data.in[2] = auxid;

i = 80 + (20 + 8)*4 + RET_SIZE;		/* in bytes */
round_trip(i);

charbuf = (char *)(&serv_ret.data.in[0]);
(void) strcpy(cbuf,charbuf);

for (i=20; i<40; i++)
   *ibuf++ = serv_ret.data.in[i];
for (i=40; i<48; i++)
   *rbuf++ = serv_ret.data.fl[i];

return (serv_ret.code);
}
/* **************************************************************** */

int IIESDB_C (display,flag,auxid,cbuf,ibuf,rbuf)
int display, flag, auxid, *ibuf;
char  *cbuf;
float *rbuf;

{
int   i;

serv_buf.nobyt   = 12 + 80 + (17+8)*4 + BUFHEAD;	/* in bytes */
serv_buf.code_id = ESDB_CODE;

serv_buf.data.in[0] = display;
serv_buf.data.in[1] = flag;
serv_buf.data.in[2] = auxid;
charbuf = (char *)(&serv_buf.data.in[3]);
(void) strcpy(charbuf,cbuf);

for (i=23; i<40; i++)
   serv_buf.data.in[i] = *ibuf++;
for (i=40; i<48; i++)
   serv_buf.data.fl[i] = *rbuf++;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIDQDV_C (display, nconf, xdev, ydev, depthdev, maxlutn, maxittn,
              maxcurn)
int  display, *nconf, *xdev, *ydev, *depthdev;
int  *maxlutn, *maxittn, *maxcurn;

{
                                    
#define     DQDV_RET   RET_SIZE+28

serv_buf.nobyt   = 4 + BUFHEAD;
serv_buf.code_id = DQDV_CODE;
serv_buf.data.in[0] = display;

round_trip(DQDV_RET);

*nconf     = serv_ret.data.in[0];
*xdev      = serv_ret.data.in[1];
*ydev      = serv_ret.data.in[2];
*depthdev  = serv_ret.data.in[3];
*maxlutn   = serv_ret.data.in[4];
*maxittn   = serv_ret.data.in[5];
*maxcurn   = serv_ret.data.in[6];
return (serv_ret.code);
}
/* **************************************************************** */

int IIDQCI_C (display, devcap, size, capdata, ncap)
int  display , devcap, size, capdata[], *ncap;

{             
#define     DQCI_RET   RET_SIZE+4+4*size

register int i;

serv_buf.nobyt   = 12 + BUFHEAD;
serv_buf.code_id = DQCI_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = devcap;
serv_buf.data.in[2] = size;

round_trip(DQCI_RET);

*ncap = serv_ret.data.in[0];           
for (i=0; i<*ncap; i++)
   capdata[i] = serv_ret.data.in[i+1];

return (serv_ret.code);
}
/* **************************************************************** */

int IIDQCR_C (display, devcap, size, capdata, ncap)
int   display, devcap, size, *ncap;
float capdata[];

{
#define     DQCR_RET   RET_SIZE+4+4*size

register int i;

serv_buf.nobyt   = 12 + BUFHEAD;
serv_buf.code_id = DQCR_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = devcap;
serv_buf.data.in[2] = size;

round_trip(DQCR_RET);

*ncap = serv_ret.data.in[0];           
for (i=0; i<*ncap; i++)
   capdata[i] = serv_ret.data.fl[i+1];

return (serv_ret.code);
}
/* **************************************************************** */

int IIDQDC_C (display, confn, memtyp, maxmem, confmode,
              mlist, mxsize, mysize, mdepth, ittlen, nmem) 
int display, confn, memtyp, maxmem, *confmode, *nmem;
int mlist[], mxsize[], mysize[], mdepth[], ittlen[];

{
#define     DQDC_RET   RET_SIZE+8+4*maxmem*5

register int i;

serv_buf.nobyt   = 16 + BUFHEAD;
serv_buf.code_id = DQDC_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = confn;
serv_buf.data.in[2] = memtyp;
serv_buf.data.in[3] = maxmem;

round_trip(DQDC_RET);

*confmode = serv_ret.data.in[0];           
*nmem = serv_ret.data.in[1];           
for (i=0; i<*nmem; i++)
   {
   mlist[i]   = serv_ret.data.in[2+i];
   mxsize[i]  = serv_ret.data.in[2+i+maxmem];
   mysize[i]  = serv_ret.data.in[2+i+maxmem*2];
   mdepth[i]  = serv_ret.data.in[2+i+maxmem*3];
   ittlen[i]  = serv_ret.data.in[2+i+maxmem*4];
   }
return (serv_ret.code);
}
/* **************************************************************** */

int IIMSMV_C (display, memlist, nmem, vis)
int display, nmem, vis;
int memlist[];

{                   
register int i;

serv_buf.nobyt   = BUFHEAD+12+4*nmem;
serv_buf.code_id = MSMV_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = nmem;
serv_buf.data.in[2] = vis;
for (i=0; i<nmem; i++)             
   serv_buf.data.in[3+i] = memlist[i];

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */
                                                                    
int IIZWSC_C (display , memlist , nmem , xscr , yscr)
int display , nmem , xscr , yscr;
int memlist[];

{
register int i;

serv_buf.nobyt   = BUFHEAD+16+4*nmem;
serv_buf.code_id = ZWSC_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = nmem;
serv_buf.data.in[2] = xscr;
serv_buf.data.in[3] = yscr;
for (i=0; i<nmem; i++)
   serv_buf.data.in[4+i] = memlist[i];

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIZWZM_C (display , memlist , nmem , zoom)
int display , nmem , zoom;
int memlist[];

{
register int i;

serv_buf.nobyt   = BUFHEAD+12+4*nmem;
serv_buf.code_id = ZWZM_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = nmem;
serv_buf.data.in[2] = zoom;
for (i=0; i<nmem; i++)
   serv_buf.data.in[3+i] = memlist[i];

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIZRSZ_C (display , memid , xscr , yscr , zoom )
int display , memid , *xscr , *yscr , *zoom;

{
#define     ZRSZ_RET   RET_SIZE+12

serv_buf.nobyt   = 8 + BUFHEAD;
serv_buf.code_id = ZRSZ_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;

round_trip(ZRSZ_RET);

*xscr = serv_ret.data.in[0];
*yscr = serv_ret.data.in[1];
*zoom = serv_ret.data.in[2];
return (serv_ret.code);
}
/* **************************************************************** */

int IIZWSZ_C (display , memid , xscr , yscr , zoom )
int display , memid , xscr , yscr , zoom;

{
serv_buf.nobyt   = 20 + BUFHEAD;
serv_buf.code_id = ZWSZ_CODE;
 
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = xscr;
serv_buf.data.in[3] = yscr;
serv_buf.data.in[4] = zoom;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIDSDP_C (display , memlist , nmem , lutflag , ittflag)
int display , nmem;
int memlist[] , lutflag[] , ittflag[];

{
register int i;

serv_buf.nobyt   = BUFHEAD+8+nmem*3*4;
serv_buf.code_id = DSDP_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = nmem;
for (i=0; i<nmem; i++)
   {
   serv_buf.data.in[2+i]         = memlist[i];
   serv_buf.data.in[2+nmem+i]    = lutflag[i];
   serv_buf.data.in[2+nmem*2+i]  = ittflag[i];
   }

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIXWIM_C (display,chanl,frame,khelp,loaddir,npix,icen,cuts,scale)
int   display, chanl;
char  *frame;
int   *khelp, loaddir, *npix, *icen, *scale;
float *cuts;

{
int kk;
register int nr;

char  *charbuf;

serv_buf.nobyt   = BUFHEAD+(51*4);
serv_buf.code_id = XWIM_CODE;

serv_buf.data.in[0] = display;
serv_buf.data.in[1] = chanl;

kk = strlen(frame) + 1;
if (kk > 80) kk = 79;
charbuf = (char *)(&serv_buf.data.in[2]);
(void) strncpy(charbuf,frame,kk);
*(charbuf+kk) = '\0';

for (nr=0; nr<14; nr++)
    serv_buf.data.in[25+nr] = khelp[nr];

serv_buf.data.in[39] = loaddir;
serv_buf.data.in[40] = npix[0];
serv_buf.data.in[41] = npix[1];
serv_buf.data.in[42] = icen[0];
serv_buf.data.in[43] = icen[1];
serv_buf.data.in[44] = icen[2];
serv_buf.data.in[45] = icen[3];
serv_buf.data.fl[46] = cuts[0];
serv_buf.data.fl[47] = cuts[1];
serv_buf.data.in[48] = scale[0];
serv_buf.data.in[49] = scale[1];
serv_buf.data.in[50] = scale[2];

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIMWMY_C (display , memid , data , npixel , depth , 
               packf , x0 , y0)
int display, memid, npixel, depth, packf, x0, y0;
int *data;
            
{
int   i, l;

outsize = ((npixel % packf) == 0) ? (npixel / packf) : ((npixel / packf) + 1);

serv_buf.nobyt   = BUFHEAD+28;
serv_buf.code_id = MWMY_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = npixel;
serv_buf.data.in[3] = depth;
serv_buf.data.in[4] = packf;
serv_buf.data.in[5] = x0;
serv_buf.data.in[6] = y0;

if (outsize > MAXINTBUF)
   {
   (void) sprintf(dfilnam,"%sx11%s.xmy",pipedir,unitnam);
   fid = osdopen(dfilnam,WRITE);
   if (fid < 0)
      {
      (void) printf ("Could not create internal data file %s !\n",dfilnam);
      return (-99);
      }
   nbytes = outsize * 4;
   memadr = (char *) data;
   i = osdwrite(fid,memadr,nbytes);
   if (i < nbytes)
      {
      (void) printf("Error writing from file %s\n", dfilnam );
      return (-98);
      }
   osdclose(fid);
   }
else
   {
   for (i=0, l=7; i<outsize; i++, l++)
      {
      serv_buf.data.in[l] = data[i];
      }
   serv_buf.nobyt += (outsize*4);
   }

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIMCMY_C (display , memlist , nmem , bck)
int display , nmem , bck;
int memlist[];
 
{   
register int i;

serv_buf.nobyt   = BUFHEAD+12+nmem*4;
serv_buf.code_id = MCMY_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = nmem;
serv_buf.data.in[2] = bck;
for (i=0; i<nmem; i++)
   serv_buf.data.in[3+i] = memlist[i];

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIMCPY_C (displaya,mema,offseta,displayb,memb,offsetb,size,zoom)
int displaya, mema, *offseta, displayb, memb, *offsetb, *size, zoom;
 
{   
serv_buf.nobyt   = BUFHEAD+44;
serv_buf.code_id = MCPY_CODE;
  
serv_buf.data.in[0] = displaya;
serv_buf.data.in[1] = mema;
serv_buf.data.in[2] = offseta[0];
serv_buf.data.in[3] = offseta[1];
serv_buf.data.in[4] = displayb;
serv_buf.data.in[5] = memb;
serv_buf.data.in[6] = offsetb[0];
serv_buf.data.in[7] = offsetb[1];
serv_buf.data.in[8] = size[0];
serv_buf.data.in[9] = size[1];
serv_buf.data.in[10] = zoom;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIMCPV_C (displaya,mema,offseta,displayb,memb,offsetb,size,zoom)
int displaya, mema, *offseta, displayb, memb, *offsetb, *size, zoom;

{
serv_buf.nobyt   = BUFHEAD+44;
serv_buf.code_id = MCPV_CODE;

serv_buf.data.in[0] = displaya;
serv_buf.data.in[1] = mema;
serv_buf.data.in[2] = offseta[0];
serv_buf.data.in[3] = offseta[1];
serv_buf.data.in[4] = displayb;
serv_buf.data.in[5] = memb;
serv_buf.data.in[6] = offsetb[0];
serv_buf.data.in[7] = offsetb[1];
serv_buf.data.in[8] = size[0];
serv_buf.data.in[9] = size[1];
serv_buf.data.in[10] = zoom;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIMRMY_C (display , memid ,  npixel , x0 , y0 , depth , packf ,  ittf , 
          data)
int display, memid, npixel, depth, packf, x0, y0, ittf;
int *data;
 
{                 
register int i;

if (npixel == 0) return (0);

outsize = ((npixel % packf) == 0) ? (npixel / packf) : ((npixel / packf) + 1);

serv_buf.nobyt   = BUFHEAD+32;
serv_buf.code_id = MRMY_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = npixel;
serv_buf.data.in[3] = x0;
serv_buf.data.in[4] = y0;
serv_buf.data.in[5] = depth;
serv_buf.data.in[6] = packf;
serv_buf.data.in[7] = ittf;

if (outsize > MAXINTBUF)
   nbytes = 0;
else
   nbytes = outsize*4;

round_trip(RET_SIZE+nbytes);

if (outsize > MAXINTBUF)
   {
   (void) sprintf(dfilnam,"%sx11%s.xmy",pipedir,unitnam);
   fid = osdopen(dfilnam,READ);
   if (fid < 0)
      {
      (void) printf ("No internal data file %s !\n",dfilnam);
      return (-99);
      }
   nbytes = outsize*4;
   memadr = (char *) data;
   i = osdread(fid,memadr,nbytes);
   if (i < nbytes)
      {
      (void) printf("Error reading from file %s\n", dfilnam );
      return (-98);
      }

   osdclose(fid);
   osfdelete(dfilnam);
   }

else
   {
   for (i=0; i<outsize; i++)
      data[i] = serv_ret.data.in[i];
   }

return (serv_ret.code);
}
/* **************************************************************** */

int IIMSTW_C (display , memid , loaddir , xwdim , ywdim , depth ,
               xwoff , ywoff )
int display , memid , loaddir , xwdim , ywdim , depth , xwoff , ywoff;

{
serv_buf.nobyt   = BUFHEAD+32;
serv_buf.code_id = MSTW_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = loaddir;
serv_buf.data.in[3] = xwdim;
serv_buf.data.in[4] = ywdim;
serv_buf.data.in[5] = depth;
serv_buf.data.in[6] = xwoff;
serv_buf.data.in[7] = ywoff;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIGPLY_C (display , memid , x , y , np , color , style )
int  display , memid , np , color , style ;
int  x[] , y[];

{
int   m, mm, chunk, offset, nopnts;
register int i;

outsize = 2 * np;
offset = 0;

graph_loop:

if (outsize > MAXINTBUF)
   chunk = MAXINTBUF;
else
   chunk = outsize;

nopnts = chunk/2;

serv_buf.nobyt   = BUFHEAD+20;
serv_buf.code_id = GPLY_CODE;
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = nopnts;
serv_buf.data.in[3] = color;
serv_buf.data.in[4] = style;
 
for(i=0; i<nopnts; i++)
   {
   m = 5 + i; mm = offset + i;
   serv_buf.data.in[m] = x[mm];
   serv_buf.data.in[m+nopnts] = y[mm];
   }
serv_buf.nobyt += (chunk*4);
 
round_trip(RET_SIZE);
outsize -= chunk;			/* decrease graph size  */

if (outsize > 0)
   {
   offset += (nopnts-1);		/* we have to use last point as start */
   goto graph_loop;
   }

return (serv_ret.code);
}
/* **************************************************************** */

int IIGCPY_C (display,memid,append)
int  display, memid, append;

{
serv_buf.nobyt   = BUFHEAD+12;
serv_buf.code_id = GCPY_CODE;;
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = append;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIGTXT_C (display , memid , txt , x0 , y0 , path , orient , color , 
              txtsize)
int  display , memid , x0 , y0 , path , orient , color , txtsize;
char txt[];

{
int    txtlen, np;

txtlen = strlen(txt) + 1;
if (txtlen > MAXSTRLEN) return (-999);

np = ((txtlen % 4) == 0) ? (txtlen/4) : (txtlen/4 + 1);

serv_buf.nobyt   = np*4 + BUFHEAD + 32;
serv_buf.code_id = GTXT_CODE;

serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = x0;
serv_buf.data.in[3] = y0;
serv_buf.data.in[4] = path;
serv_buf.data.in[5] = orient;
serv_buf.data.in[6] = color;
serv_buf.data.in[7] = txtsize;
  
charbuf = (char *)(&serv_buf.data.in[8]);
(void) strcpy(charbuf,txt);

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IILWIT_C (display , memid , ittn , ittstart , ittlen , ittdata)
int display , memid , ittn , ittstart , ittlen ;
float ittdata [];

{
register int i;

serv_buf.nobyt   = BUFHEAD+ittlen*4+20;
serv_buf.code_id = LWIT_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = ittn;
serv_buf.data.in[3] = ittstart;
serv_buf.data.in[4] = ittlen;
for (i=0; i<ittlen; i++)
   serv_buf.data.fl[5+i] = ittdata[i];

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IILRIT_C (display , memid , ittn , ittstart , ittlen , ittdata )
int display , memid , ittn , ittstart , ittlen ;
float ittdata [];

{
#define        LRIT_RET     RET_SIZE+ittlen*4

register int i;

serv_buf.nobyt   = BUFHEAD+20;
serv_buf.code_id = LRIT_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = ittn;
serv_buf.data.in[3] = ittstart;
serv_buf.data.in[4] = ittlen;

round_trip(LRIT_RET);

for (i=0; i<ittlen; i++)
   ittdata[i] = serv_ret.data.fl[i];

return (serv_ret.code);
}
/* **************************************************************** */

int IILWLT_C (display , lutn , lutstart , lutlen , lutdata)
int display , lutn , lutstart , lutlen ;
float lutdata [];

{
register int i;

serv_buf.nobyt   = BUFHEAD+16+4*lutlen*3;
serv_buf.code_id = LWLT_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = lutn;
serv_buf.data.in[2] = lutstart;
serv_buf.data.in[3] = lutlen;
for (i=0; i<lutlen*3; i++)
   serv_buf.data.fl[4+i] = lutdata[i];

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IILRLT_C (display , lutn , lutstart , lutlen , lutdata )
int display , lutn , lutstart , lutlen ;
float lutdata [];

{
#define       LRLT_RET     RET_SIZE+4*lutlen*3
                                    
register int i;

serv_buf.nobyt   = BUFHEAD+16;
serv_buf.code_id = LRLT_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = lutn;
serv_buf.data.in[2] = lutstart;
serv_buf.data.in[3] = lutlen;

round_trip(LRLT_RET);

for (i=0; i<lutlen*3; i++)
   lutdata[i] = serv_ret.data.fl[i];

return (serv_ret.code);
}
/* **************************************************************** */

int IICINC_C (display , memid , curn , cursh , curcol , xcur , ycur )
int display , memid , curn , cursh , curcol , xcur , ycur;

{
serv_buf.nobyt   = BUFHEAD+28;
serv_buf.code_id = CINC_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = curn;
serv_buf.data.in[3] = cursh;
serv_buf.data.in[4] = curcol;
serv_buf.data.in[5] = xcur;
serv_buf.data.in[6] = ycur;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IICSCV_C (display , curn , vis )
int display , curn , vis;

{
serv_buf.nobyt   = BUFHEAD+12;
serv_buf.code_id = CSCV_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = curn;
serv_buf.data.in[2] = vis;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IICRCP_C (display , inmemid , curn , xcur , ycur , outmemid )
int display , inmemid , curn , *xcur , *ycur , *outmemid;

{
#define        CRCP_RET        RET_SIZE+12
                                    
serv_buf.nobyt   = BUFHEAD+12;
serv_buf.code_id = CRCP_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = inmemid;
serv_buf.data.in[2] = curn;

round_trip(CRCP_RET);

*xcur      = serv_ret.data.in[0];
*ycur      = serv_ret.data.in[1];
*outmemid  = serv_ret.data.in[2];

return (serv_ret.code);
}
/* **************************************************************** */

int IICWCP_C (display , memid , curn , xcur , ycur )
int display , memid , curn , xcur , ycur;

{
serv_buf.nobyt   = BUFHEAD+20;
serv_buf.code_id = CWCP_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = curn;
serv_buf.data.in[3] = xcur;
serv_buf.data.in[4] = ycur;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIRINR_C (display, memid, roicol, roixmin, roiymin,
              roixmax, roiymax, roiid )

int display, *roiid, memid, roicol;
int roixmin, roiymin, roixmax, roiymax;

{
#define        RINR_RET     RET_SIZE+4
                                    
serv_buf.nobyt   = BUFHEAD+28;
serv_buf.code_id = RINR_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = roicol;
serv_buf.data.in[3] = roixmin;
serv_buf.data.in[4] = roiymin;
serv_buf.data.in[5] = roixmax;
serv_buf.data.in[6] = roiymax;

round_trip(RINR_RET);

*roiid = serv_ret.data.in[0];

return (serv_ret.code);
}
/* **************************************************************** */

int IICINR_C (display , memid , roicol , roixcen , roiycen ,
              roirad1, roirad2, roirad3, roiid )

int display, *roiid, memid, roicol;
int roixcen, roiycen, roirad1, roirad2, roirad3;

{
#define        CINR_RET     RET_SIZE+4

serv_buf.nobyt   = BUFHEAD+32;
serv_buf.code_id = CINR_CODE;

serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = roicol;
serv_buf.data.in[3] = roixcen;
serv_buf.data.in[4] = roiycen;
serv_buf.data.in[5] = roirad1;
serv_buf.data.in[6] = roirad2;
serv_buf.data.in[7] = roirad3;

round_trip(CINR_RET);

*roiid = serv_ret.data.in[0];

return (serv_ret.code);
}
/* **************************************************************** */

int IIRRRI_C (display , inmemid , roiid , roixmin , roiymin , roixmax , 
              roiymax ,  outmemid )
int display , roiid , inmemid , *outmemid;
int *roixmin , *roiymin , *roixmax , *roiymax;

{
#define        RRRI_RET     RET_SIZE+20
                                    
serv_buf.nobyt   = BUFHEAD+12;
serv_buf.code_id = RRRI_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = inmemid;
serv_buf.data.in[2] = roiid;

round_trip(RRRI_RET);

*roixmin   = serv_ret.data.in[0];
*roiymin   = serv_ret.data.in[1];
*roixmax   = serv_ret.data.in[2];
*roiymax   = serv_ret.data.in[3];
*outmemid  = serv_ret.data.in[4];

return (serv_ret.code);
}
/* **************************************************************** */

int IICRRI_C (display , inmemid , roiid , roixcen , roiycen,
              roirad1, roirad2, roirad3, outmemid)
int display, roiid, inmemid, *outmemid;
int *roixcen, *roiycen, *roirad1, *roirad2, *roirad3;

{
#define        CRRI_RET     RET_SIZE+24

serv_buf.nobyt   = BUFHEAD+12;
serv_buf.code_id = CRRI_CODE;

serv_buf.data.in[0] = display;
serv_buf.data.in[1] = inmemid;
serv_buf.data.in[2] = roiid;

round_trip(CRRI_RET);

*roixcen   = serv_ret.data.in[0];
*roiycen   = serv_ret.data.in[1];
*roirad1   = serv_ret.data.in[2];
*roirad2   = serv_ret.data.in[3];
*roirad3   = serv_ret.data.in[4];
*outmemid  = serv_ret.data.in[5];

return (serv_ret.code);
}
/* **************************************************************** */

int IIRWRI_C (display, memid, roiid, roixmin, roiymin, roixmax, roiymax) 

int display , memid , roiid;
int roixmin , roiymin , roixmax , roiymax;

{
serv_buf.nobyt   = BUFHEAD+28;
serv_buf.code_id = RWRI_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = roiid;
serv_buf.data.in[3] = roixmin;
serv_buf.data.in[4] = roiymin;
serv_buf.data.in[5] = roixmax;
serv_buf.data.in[6] = roiymax;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IICWRI_C (display, memid, roiid, roixcen, roiycen,
              roirad1, roirad2, roirad3)

int display, memid, roiid;
int roixcen, roiycen, roirad1, roirad2, roirad3;

{
serv_buf.nobyt   = BUFHEAD+32;
serv_buf.code_id = CWRI_CODE;

serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = roiid;
serv_buf.data.in[3] = roixcen;
serv_buf.data.in[4] = roiycen;
serv_buf.data.in[5] = roirad1;
serv_buf.data.in[6] = roirad2;
serv_buf.data.in[7] = roirad3;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIRSRV_C (display , roiid , vis )
int    display , roiid , vis;

{
serv_buf.nobyt   = BUFHEAD+12;
serv_buf.code_id = RSRV_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = roiid;
serv_buf.data.in[2] = vis;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIIENI_C (display , intype , intid , objtype , objid , oper , trigger) 
int  intype , objtype , oper;
int  display , intid , objid , trigger;

{
serv_buf.nobyt   = BUFHEAD+28;
serv_buf.code_id = IENI_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = intype;
serv_buf.data.in[2] = intid;
serv_buf.data.in[3] = objtype;
serv_buf.data.in[4] = objid;
serv_buf.data.in[5] = oper;
serv_buf.data.in[6] = trigger;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIIEIW_C ( display , trgstatus )
int display , trgstatus[MAX_TRG] ;

{ 
#define        IEIW_RET     RET_SIZE+MAX_TRG*4
                                    
register int i;

serv_buf.nobyt   = BUFHEAD+4;
serv_buf.code_id = IEIW_CODE;
  
serv_buf.data.in[0] = display;

round_trip(IEIW_RET);

for (i=0; i<MAX_TRG; i++)
    trgstatus[i]  = serv_ret.data.in[i];

return (serv_ret.code);
}
/* **************************************************************** */

int IIISTI_C (display )
int  display ;

{
serv_buf.nobyt   = BUFHEAD+4;
serv_buf.code_id = ISTI_CODE;
  
serv_buf.data.in[0] = display;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIIQID_C ( display , intype , intn , intdscr , dscrlen )
int   display , intype , intn , *dscrlen ;
char  intdscr[];

{
#define        IQID_RET     RET_SIZE+4+MAXSTRLEN
                                    
serv_buf.nobyt   = BUFHEAD+12;
serv_buf.code_id = IQID_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = intype;
serv_buf.data.in[2] = intn;

round_trip(IQID_RET);

*dscrlen   = serv_ret.data.in[0];                   
  
charbuf = (char *)(&serv_ret.data.in[1]);
(void) strncpy(intdscr,charbuf,*dscrlen);

return (serv_ret.code);
}
/* **************************************************************** */

int IIIGLD_C ( display , locn , xdis , ydis )
int   display , locn , *xdis , *ydis ;

{
#define      IGLD_RET    RET_SIZE+8

serv_buf.nobyt   = BUFHEAD+8;
serv_buf.code_id = IGLD_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = locn;

round_trip(IGLD_RET);

*xdis = serv_ret.data.in[0];
*ydis = serv_ret.data.in[1];

return (serv_ret.code);
}
/* **************************************************************** */

int IIIGIE_C ( display , evlid , evlival )
int display , evlid , *evlival ;

{
#define        IGIE_RET     RET_SIZE+4
                                    
serv_buf.nobyt   = BUFHEAD+8;
serv_buf.code_id = IGIE_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = evlid;

round_trip(IGIE_RET);

*evlival   = serv_ret.data.in[0];
return (serv_ret.code);
}
/* **************************************************************** */

int IIIGRE_C ( display , evlid , evlrval )
int    display , evlid ;
float  *evlrval;

{
#define        IGRE_RET     RET_SIZE+4
                                    
serv_buf.nobyt   = BUFHEAD+8;
serv_buf.code_id = IGRE_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = evlid;

round_trip(IGRE_RET);

*evlrval   = serv_ret.data.fl[0];

return (serv_ret.code);
}
/* **************************************************************** */

int IIIGSE_C ( display , evlid , evlsval , evlslen )
int   display , evlid , *evlslen ;
char  evlsval[];

{
#define        IGSE_RET     RET_SIZE+4+80
                                    
serv_buf.nobyt   = BUFHEAD+8;
serv_buf.code_id = IGSE_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = evlid;

round_trip(IGSE_RET);

*evlslen = serv_ret.data.in[0];
  
charbuf = (char *)(&serv_ret.data.in[1]);
(void) strcpy(evlsval,charbuf);

return (serv_ret.code);
}
/* **************************************************************** */

int IIIGCE_C ( display , evlid , evlsval )
int   display , evlid;
char  *evlsval;

{

#define        IGCE_RET     RET_SIZE+4
                                    
serv_buf.nobyt   = BUFHEAD+8;
serv_buf.code_id = IGCE_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = evlid;

round_trip(IGCE_RET);

charbuf = (char *)(&serv_ret.data.in[0]);
*evlsval = charbuf[0];

return (serv_ret.code);
}
/* **************************************************************** */

int IIIGLE_C (display,evlid,evllval)
int display , evlid , *evllval ;

{
#define        IGLE_RET     RET_SIZE+4
                                    
serv_buf.nobyt   = BUFHEAD+8;
serv_buf.code_id = IGLE_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = evlid;

round_trip(IGLE_RET);

*evllval   = serv_ret.data.in[0];
return (serv_ret.code);
}
/* **************************************************************** */

int IIDSNP_C(display,colmode,npixel,xoff,yoff,depth,packf,data)
int display, colmode, npixel, xoff, yoff, depth, packf;
int *data;

{
int i;

if (npixel <= 0) return (0);

outsize = ((npixel % packf) == 0) ? (npixel / packf) : ((npixel / packf) + 1);

serv_buf.nobyt = BUFHEAD+28;
serv_buf.code_id = DSNP_CODE;
 
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = colmode;
serv_buf.data.in[2] = npixel;
serv_buf.data.in[3] = xoff;
serv_buf.data.in[4] = yoff;
serv_buf.data.in[5] = depth;
serv_buf.data.in[6] = packf;

if (outsize > MAXINTBUF)
   nbytes = 0;
else
   nbytes = outsize*4;

round_trip(RET_SIZE+nbytes);

if (outsize > MAXINTBUF)
   {
   (void) sprintf(dfilnam,"%sx11%s.xmy",pipedir,unitnam);
   fid = osdopen(dfilnam,READ);
   if (fid < 0)
      {
      (void) printf ("No internal data file %s !\n",dfilnam);
      return(0);
      }
   nbytes = outsize*4;
   memadr = (char *) data;
   i = osdread(fid,memadr,nbytes);
   if (i < nbytes)
      {
      (void) printf("Error reading from file %s\n", dfilnam );
      return(0);
      }

   osdclose(fid);
   osfdelete(dfilnam);
   }

else
   {
   for (i=0; i<outsize; i++)
      data[i] = serv_ret.data.in[i];
   }

return (serv_ret.code);
}
/* **************************************************************** */

int IIMBLM_C (display , memlst ,nmem , period )
int   display , memlst[] ,nmem ;
float period[]; 

{
register int i;



/* currently (020606) max. 12 channels */

serv_buf.nobyt   = BUFHEAD+80+nmem*4; 	/* 80 -> fl[20], and then nmem int's */
serv_buf.code_id = MBLM_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = nmem;
for (i=0; i<nmem; i++)
   {
   serv_buf.data.in[2+i] = memlst[i];
   serv_buf.data.fl[20+i] = period[i];	/* we begin at [20] to leave space */
   }

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IILSBV_C ( display , memid , vis )
int display , memid , vis;

{
serv_buf.nobyt   = BUFHEAD+12;
serv_buf.code_id = LSBV_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = memid;
serv_buf.data.in[2] = vis;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

/* ARGSUSED */

int IIDSSS_C (display , memid , xoff , yoff , splitf , splitx , splity) 
int display , splitf , splitx , splity ;
int memid[] , xoff[] , yoff[];

{
return (II_SUCCESS);
}
/* **************************************************************** */

/* ARGSUSED */

int IIMSLT_C (display , memid , lutn , ittn )
int display , memid , lutn , ittn ;

{
return (II_SUCCESS);
}
/* **************************************************************** */

/* ARGSUSED */

int IIDIAG_C (display , outf )
int display , outf ;


{
return (II_SUCCESS);
}
/* **************************************************************** */

/* ARGSUSED */

int IIDSEL_C (display, confn)
int display, confn;

{
return (II_SUCCESS);
}
/* **************************************************************** */

/* ARGSUSED */

void IIDERR_C (errn, errtext, txtlen)
int   errn, *txtlen;
char  errtext[];

{
return;
}
/* **************************************************************** */

/* ARGSUSED */

int IIDUPD_C (display)
int display;
 
{
return (II_SUCCESS);
}
/* **************************************************************** */

int IISSIN_C (display, flag, cbuf)
int  display, flag;
char *cbuf;

{
int    txtlen, np;

txtlen = strlen(cbuf) + 1;
if (txtlen > MAXSTRLEN) return (-999);

np = ((txtlen % 4) == 0) ? (txtlen/4) : (txtlen/4 + 1);

serv_buf.nobyt   = (np*4) + 8 + BUFHEAD;
serv_buf.code_id = SSIN_CODE;

serv_buf.data.in[0] = display;
serv_buf.data.in[1] = flag;

charbuf = (char *)(&serv_buf.data.in[2]);
(void) strcpy(charbuf,cbuf);

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIDENC_C (display )
int display;

{
serv_buf.nobyt   = BUFHEAD+4;
serv_buf.code_id = DENC_CODE;
serv_buf.data.in[0] = display;

round_trip(RET_SIZE);
return (serv_ret.code);
}
/* **************************************************************** */

int IIDSTC_C (display , confid )
int display , *confid ;

{
#define        DSTC_RET     RET_SIZE+4
                                    
serv_buf.nobyt   = BUFHEAD+4;
serv_buf.code_id = DSTC_CODE;
serv_buf.data.in[0] = display;

round_trip(DSTC_RET);

*confid    = serv_ret.data.in[0];

return (serv_ret.code);
}
/* **************************************************************** */

int IIDRLC_C (display , confid )
int display ,confid ;

{
serv_buf.nobyt   = BUFHEAD+8;
serv_buf.code_id = DRLC_CODE;
  
serv_buf.data.in[0] = display;
serv_buf.data.in[1] = confid;

round_trip(RET_SIZE);
return (serv_ret.code);
}
