% @(#)reducepisc.hlq	19.1 (ESO-IPG) 02/25/03 14:28:05
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      reducepisc.hlq
%.AUTHOR     MP, IPG/ESO
%.KEYWORDS   MIDAS, help files, REDUCE/PISCO  
%.PURPOSE    On-line help file for the command: REDUCE/PISCO 
%.VERSION    1.0  24-SEP-1991 : Creation, MP
%.VERSION    1.1  29-SEP-1992 : Update, KB
%----------------------------------------------------------------
\se
SECTION./PISC
\es\co
REDUCE/PISCO                                               24-SEP-1991 OS,MS
\oc\su
REDUCE/PISCO  catalog table sky calibration [mode]
	perform complete reduction of polarimetric data
\us\pu
Purpose:      
          Perform complete reduction of polarimetric data obtained at La Silla
          with PISCO 
\up\sy
Syntax:       
          REDUCE/PISCO catalog table sky calibration [mode] 
\ys\pa
          catalog = input catalog of data files to be reduced   
\ap\pa
          table = output table 
\ap\pa
          sky = input sky (or dark) measurement file
\ap\pa 
          calibration = input calibration file 
\ap\pa
          mode = 1, 2 or 3:   \\
                 1: X and Y channel are reduced separately \\
                 2: X and Y channel are reduced together \\
                 3: X and Y channel are reduced separately as well as together
\ap\sa
See also:
          CREATE/TABLE, EXTRACT/LINE, COMPUTE/IMAGE, FFT/...\\
          MIDAS User Manual - Volume B
\as\no
Note:        
          The data format must conform to the one described in the 
          1989 March version of ESO Operating Manual No. 13 (PISCO). 
          The format of old PISCO data may have to be adapted. 
\\
          A description of the command and the format of measurement 
          files and output table is given in Appendix F of Volume B of the
          MIDAS User Manual.
\\
          All data frames, including sky measurements, must have the double
          precision descriptor O_TIME set with element 7 (exposure time) not  
          being zero.  
\on\exs
Examples:     
\ex
          REDUCE/PISCO incat outtab sky calib 3 
          This would process all files withe entries in the image catalog
          `incat.cat' taking the file `sky.bdf' as sky measurement and
          calibrate with frame `calib.bdf'. The X and Y channels will be
          reduced separately as well as together.
          Results are stored in table `outtab.tbl'.
\xe \sxe
