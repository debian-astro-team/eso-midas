! @(#)ccdtestd.prg	19.1 (ESO-DMD) 02/25/03 14:17:11
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestd.prg
!.PURPOSE:        Procedure to the ccd dark characteristics
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestd p1 p2 p3
!.                where p1 = input catalogue containing the ccd bias frames
!.                      p2 = output identifier [bias]
!.                      p3 = decimation factor for frame reduction
!.USE:            @@ ccdtestd p1 p2 p3
!.Algorithm:      The following tests are done:
!.VERSION:        940608 RHW creation
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?             C  "Enter input catalogue:"
DEFINE/PARAMETER P2  DARK          C  "Enter combined output frame:"
DEFINE/PARAMETER P3  16            N  "Box size for averaging:"
!
define/local ifrm/i/1/1   0
define/local catal/i/1/1  0
define/local frame/c/1/60 " " all
define/local myouta/c/1/60 " " all
define/local rebin/c/1/3  "med"
define/local nbox/i/1/1   {p3}
define/local dksta/r/1/1  0.0
define/local dkend/r/1/1  0.0
define/local dkste/r/1/1  0.0
define/local scale/i/1/1/ 0
!
! *** check if the catalogue exists
IF M$EXIST("{P1}") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTD>: Catalogue {P1} not existing"
   RETURN/EXIT
ELSE
   read/icat {p1}
   write/out " "
ENDIF
!
!
write/out "Test D: Rebin dark frames with a median filter on a {p3}x{p3} pixel"
write/out "        area. The rebinned images are averaged into a combined"
write/out "        total dark frame, normalized to one hour exposure time"
write/out "----------------------------------------------------------------"
write/out " "
!
create/display
create/graph; set/graph frame=square pmode=2 xformat=i3 yformat=i3 
set/format f6.3
!
write/out "        Statistics of rebinned frames (bin size {p3}x{p3} pixels)"
LOOP: 
   store/frame in_a {p1} 1 FINISH
   frame = "{in_a}"
   ifrm  = ifrm + 1

   if M$EXIST("{SC_BSFRM}.bdf") .EQ. 1 then
      compute/imag -
          middummd = {ADUCF} * ({in_a} - {SC_BSFRM})/{{frame},{O_DESC}}*3600.
   else
      compute/imag -
          middummd = {ADUCF} * ({in_a} - {BIASMEAN})/{{frame},{O_DESC}}*3600.
   endif

   in_a  = "middummd"
   out_a = "middummo"
   myouta = out_a(1:60)
   run STD_EXE:ccdrebin
   statist/imag middummo ? ? ? NN
   write/out -
       "        {frame}: mean = {outputr(3)}; sigma = {outputr(4)} e-/h/pixel"

   if ifrm .eq. 1 then
      compute/image {p2} = {myouta}
   else 
      compute/image {p2} = {p2} + {myouta}
   endif
   load/image {P2} scale=4
   goto LOOP
FINISH:
!
compute/image {p2} = {p2}/{ifrm}
write/descr {p2} IDENT/C/1/40 "Dark current in e-/h/pixel"
statist/imag {p2} ? ? ? NN
write/out " "
write/out -
      "        {p2}: mean = {outputr(3)}; sigma = {outputr(4)} e-/h/pixel"
!
! *** now get the mean and sigma of the average dark frame and plot
scale = 512/{{p2},NPIX(1)}
load/itt expo
load/imag {p2} scale={scale}
copy/display p5=noprint
-copy screen{mid$sess(11:12)}.ps {p2}.ps
dkste = outputr(3)/2
dksta = outputr(3) - 2*{dkste}
dkend = outputr(3) + 2*{dkste}
plot/cont {p2} ? ? {dksta}:{dkend}:{dkste} ? 3
copy/graph postscript
-copy postscript.ps {p2}_cont.ps
write/out "End of test D"
!
delete/imag screen*.ps NO
delete/imag screen*.ima NO
delete/imag postscrip*.ps NO
delete/imag middummd NO
delete/imag middummo NO






