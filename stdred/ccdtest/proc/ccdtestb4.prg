!} @(#)ccdtestb4.prg	19.1 (ESO-DMD) 02/25/03 14:17:11
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestb4.prg
!.PURPOSE:        Procedure to check bias level single file
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestb4
!.                      p1 = file to be tested
!.                      p2 = output identifier [bias]
!.                      p3 = area
!.                      p4 = radius of smoothing box for median filtering,
!.                           decimation factor for image size reduction
!.USE:            @s ccdtestb4 p1 p2 p3 p4
!.Algorithm:      A histogram of the pixel values is made, ignoring the hot 
!.                pixels stored in the hot pisel table. The graph can be
!.                found in {p2}_hist.ps. Hereafter the frame is  rebinned
!.VERSION:        940620 RHW creation
!.VERSION:        950115 RHW Inclusion of the subtest 
!.VERSION:        950808 RHW Slit into single procedures
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/parameter p1  ?         I  "Input bias frame:"
define/parameter p2  BIAS      C  "Enter output identifier:"
define/parameter p3  [<,<:>,>] C  "Enter area:"
define/parameter p4  5,10      N  "Radius med. filtering, and dec. factor:"
!
write/keyw   inputi/i/1/2     {p4}
write/keyw   radius/i/1/1     {inputi(1)}
define/local nbox/i/1/1       {inputi(2)}
define/local rebin/c/1/3      "MED"
define/local scale/i/1/1      0.0
define/local xsta/r/1/1       0.0
define/local xend/r/1/1       0.0
!
write/out "Test B4: Inspection of single bias frames:"
write/out "------------------------------------------------------------------"
write/out "     A histogram of the pixel intensities of frame {p1}"
write/out "     over the area {p3} is computed, ignoring the hot pixels 
write/out "     stored in the table {p2}_hotpix.tbl, and cosmics."
write/out "     Hereafter the clean frame is rebinned using a median filter "
write/out "     over {RADIUS}x{RADIUS} pixels; output in {p2}_rebin.ps"!
!
! *** check if file  exists
if m$exist("{p1}.bdf") .eq. 0 then
   write/out "*** FATAL <TESTB4>: Input bias frame not existing"
   return
endif
!
! *** check the existence oif the input frame
if m$exist("{p2}_hotpix.tbl") .eq. 0 then
   write/out "*** INFO: No hot pixel tabel found: no corrections"
   copy/ii {p1} &a
else
   modify/colu {p1},{p2}_hotpix &a V                   ! modify the hot pixels
endif
!
! *** Plot a histogram distribution of a bias frame 
statist/image &a {p3} ? ? FN
statist/image &a {p3} 1 {xsta},{xend} FN
xsta = outputr(3) - 40.
xend = outputr(3) + 40.
set/graph xaxis={xsta},{xend} yaxis= frame=rect pmode=1
plot/histo &a
copy/graph postscript 
-copy postscript.ps {p2}_hist.ps
write/out " "
write/out "     Plot of the histogram is in file {p2}_hist.ps"
!
! *** Here rebin the input frame
in_a  = "middumma"
out_a = "{p2}_rebin"
run STD_EXE:ccdrebin
statistic/image {p2}_rebin
xsta = outputr(3) - outputr(4)
xend = outputr(3) + outputr(4)
scale = 512/{{p2}_rebin,NPIX(2)}
load/image {p2}_rebin scale={scale} cuts={xsta},{xend}
set/midas output=logonly
copy/display p5=noprint
set/midas output=yes
-copy screen{mid$sess(11:12)}.ps {p2}_rebin.ps
write/out "     Cleaned and rebinned bias frame is in file {p2}_rebin.ps"
write/out " "
!
delete/imag screen{mid$sess(11:12)}.ps no
delete/imag screen{mid$sess(11:12)}.ima no
delete/imag &a no
delete/imag &b no

