! @(#)ccdtestb2.prg	19.1 (ES0-DMD) 02/25/03 14:17:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestb2.prg
!.PURPOSE:        Procedure to compute the average row and column intensities
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestb2
!.                      p1 = input combined bias file
!.                      p2 = output identifier [bias]
!.                      p3 = row range to be averaged
!.                      p4 = column range to be averaged
!.USE:            @s ccdtestb p1 p2 p3 p4 
!.Algorithm:      From the combined bias frame row and columns are averaged 
!.                and plotted. The result plot are stored in the files 
!.                {p2}_aver.ps.
!.VERSION:        940620 RHW creation
!.VERSION:        950115 RHW Inclusion of the subtests 
!.VERSION:        950115 RHW Split subtest into seperate procedures 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?         C  "Enter combined output frame:"
DEFINE/PARAMETER P2  BIAS      C  "Enter combined output frame:"
DEFINE/PARAMETER P3  <,>       C  "Enter row numbers for averaging:" 
DEFINE/PARAMETER P4  <,>       C  "Enter column numbers for averaging:" 
!
DEFINE/LOCAL YSTA/R/1/1       0.0
DEFINE/LOCAL YEND/R/1/1       0.0
!
! *** check if the catalogue exists
IF M$EXIST("{P1}.bdf") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <TESTB2>: Input combined bias file not existing"
   RETURN
ENDIF
!
crea/graph; set/graph
!
write/out "Test B2: Inspection of combined bias frame:"
write/out "----------------------------------------------------------------"
!
write/out "     Averaging rows {p3} of the stacked bias frame;"
write/out "     Output frame will be {p2}_avrow.bdf;"
average/row {p2}_avrow = {p2} {p3}                    ! average rows
statist/imag {p2}_avrow ? ? ? FN                      ! calculate statistics
ysta = OUTPUTR(3) - 4                                 ! get relevant y range
yend = OUTPUTR(3) + 4
!ysta = OUTPUTR(3) - 5*OUTPUTR(4)                     ! get relevant y range
!yend = OUTPUTR(3) + 5*OUTPUTR(4)
set/graph yaxis={ysta},{yend} pmode=1                 ! set the y axis
plot/row {p2}_avrow @1 ?  -140,-40,25,75              ! plot the row
set/grap clear=off
!
! *** Plot the bias level averaged over columns
write/out " "
write/out "     Averaging columns {p4} of the stacked bias frame;"
write/out "     Output frame will be {p2}_avcol.bdf"
average/column {p2}_avcol = {p1} {p4}                  ! average columns
plot/row {p2}_avcol @1 ? -140,-40,25,15                ! plot column
set/graph
copy/graph postscript
-copy postscript.ps {p2}_aver.ps
write/out " "
write/out "     Plot file with row and column average is in {p2}_aver.ps"
write/out " "
!
RETURN

