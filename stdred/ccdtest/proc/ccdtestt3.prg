! @(#)ccdtestt3.prg	19.1 (ES0-DMD) 02/25/03 14:17:13
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestt3.prg
!.PURPOSE:        Procedure to the test ccd linearity and transfer
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestt3 p1 p2 p3 p4 p5
!.                where p1 = test to be execcted
!.                      p2 = input catalogue containing the ccd bias frames
!.                      p3 = output identifier [TRANS]
!.                      p4 = image section to be used for the median and
!.                           variance determination
!.                      p5 = time interval to fit the shutter offset
!.                      p6 = selection criteria table 
!.USE:            @@ ccdtestt p1 p2 p3 p4
!.Algorithm:      The following tests are done:
!.                Test T1: Creation of the transfer and linearity table"
!.                Test T2: Determination of linearity curve and shutter error
!.                Test T3: Determination of the transfer curve
!.VERSION:        940524 RHW creation
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?             T  "Enter input table:"
DEFINE/PARAMETER P2  TRANS         C  "Enter output identifier:"
DEFINE/PARAMETER P3  all           C  "Selection criterium for table:"
! 
define/local xlab/r/1/1 0.0
define/local ylab/r/1/1 0.0
define/local aducf1/r/1/1 0.0
define/local aducf2/r/1/1 0.0
define/local enoise1/r/1/1 0.0
define/local enoise2/r/1/1 0.0
!
! *** check if the catalogue exists
IF M$EXIST("{P1}.tbl") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTT3>: Table {P1} not existing"
   RETURN
ELSE
   read/table {p1}
ENDIF
!
crea/graph; set/graph 
set/graph pmode=1
!
define/local col1/c/1/20  :UT_tim1 
define/local col2/c/1/20  :UT_tim2 
define/local col3/c/1/20  :Exp_tim1 
define/local col4/c/1/20  :Exp_tim2
define/local col5/c/1/20  :Med_cnt1
define/local col6/c/1/20  :Med_cnt2
define/local col7/c/1/20  :Variance
define/local col8/c/1/20  :Frc_cnt1
define/local col9/c/1/20  :Frc_cnt2
define/local col10/c/1/20 :scratch
!
write/out "Test T3: Determination of the transfer curve"
write/out "----------------------------------------------------------------"
write/out "     From the selected entries the table a linear regression"
write/out "     analysis is done to determine the readout noise. The readout"
write/out "     noise is determined by the inverse of the slope between"
write/out "     the median and the variance" multiplied by the sigma of the"
write/out "     bias (determined by TESTB/CCD and stored in keyword BIASSIGM."
write/out " "
!
set/graph stype=5
set/midas output=logonly
select/table {p1} {p3}
set/graph xformat=e6.0 yformat=e6.2
plot/table {p1} {col5} {col7}  -80,-40,40,75
xlab = plrgrap(1) + (plrgrap(2)-plrgrap(1))/10
ylab = plrgrap(6) - (plrgrap(6)-plrgrap(5))/10
label/gra "Transfer curve (1), edited" {xlab},{ylab} ? ? 1
set/graph clear=off
regression/linear {p1} {col7} {col5}
save/regress {p1} regress
compute/regress {p1} {col10} = regress
ADUCF1  = 1/OUTPUTD(2) 
ENOISE1 = aducf1*BIASSIGM
set/midas output=yes
write/out "     Transfer curve 1 : slope: {outputd(2)}, offset in y: {outputd(1)}"
write/out "     Conv. factor: {aducf1} e-/ADU; Elec. RON: {enoise1} e-/pixel"
set/graph stype=0
overplot/table {p1} {col5} {col10}
!
set/midas output=logonly
select/table {p1} {p3}
set/graph stype=5
plot/table {p1} {col6} {col7} -80,-40,40,15
xlab = plrgrap(1) + (plrgrap(2)-plrgrap(1))/10
ylab = plrgrap(6) - (plrgrap(6)-plrgrap(5))/10
label/graph "Transfer curve (2), edited" {xlab},{ylab} ? ? 1
regression/linear {p1} {col7} {col6}
save/regress {p1} regress
compute/regress {p1} {col10} = regress
aducf2 = 1/OUTPUTD(2) 
enoise2 = aducf2*BIASSIGM
set/midas output=yes
write/out "     Transfer curve 2 : slope: {outputd(2)}, offset in y: {outputd(1)}"
write/out "     Conv. factor: {aducf2} e-/ADU; Elec. RON: {enoise2} e-/pixel"
ADUCF  = (aducf1 + aducf2) / 2
ENOISE = (enoise1 + enoise2) / 2
write/out " "
write/out "     Mean conv. factor: {ADUCF} e-/ADU"
WRITE/OUT "     Mean Elec. RON:   {ENOISE} e-/pixel"
write/out "     Values are store in the CCD keywords ADUCF and ENOISE"
set/graph stype=0
overplot/table {p1} {col6} {col10}
copy/graph postscript 
-copy postscript.ps {p2}_trans.ps
write/out " "
write/out "     Plot with fitted transfer curves in file {p2}_trans.ps"
write/out " "
!
set/grap clear=on
delete/imag postscript*.ps NO
delete/imag graph*.plt NO
RETURN








