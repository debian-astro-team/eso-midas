! @(#)ccdtestf2.prg	19.1 (ES0-DMD) 02/25/03 14:17:12
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestf2.prg
!.PURPOSE:        Procedure to test the ccd flat for cold pixels
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestf1
!.                where p1 = input frame
!.                      p2 = output identifier [flat]
!.                      p3 = area
!.                      p4 = range in exposure time
!.                      p5 = overwrite flag
!.USE:            @@ ccdtestf1 p1 p2 p3 p4 p5 p6
!.Algorithm:      Creating a cold pixel table.
!                 First the combined flat is corrected for the bias 
!                 offset. Therafter all pixels in the stacked master 
!                 flat frame that show values less than {p6} times the
!                 median counts in the frame are listed. Only pixels 
!                 within the area {p5} are considered and repetitions
!                 of cold pixels in the increasing y coordinate are 
!                 not listed.
!.VERSION:        940620 RHW creation
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?             C  "Enter input frame:"
DEFINE/PARAMETER P2  FLAT          C  "Enter output identifier:"
DEFINE/PARANETER P3  [<,<:>,>]     C  "Enter area for median filtering:"
DEFINE/PARAMETER P4  5.0           N  "Enter sigma multiplier for cold pixels:"
DEFINE/PARAMETER P5  N             C  "Enter overwrite option:"
!
DEFINE/LOCAL CDSTA/R/1/1      0.0
DEFINE/LOCAL CDEND/R/1/1      0.0
!
! *** check if the input frame  exists
IF M$EXIST("{P1}.bdf") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTF2>: Input flat frame {p1} not existing."
   RETURN
ENDIF
!
write/out "Test F2: Creating a cold pixel table"
write/out "------------------------------------------------------------------"
write/out "     First the bias corrected combined flat will be corrected for"
write/out "     its large scale structure by dividing it by its median one"
write/out "     All pixels in the resulting frame showing values less than"
write/out "     {p4} sigma below it mean value counts areare considered cold"
write/out "     and listed in the output table. Only pixels within the area "
write/out "     {p5} are considered and repetitions of cold pixels in the "
write/out "     increasing y coordinate are not listed."
write/out " "
!
IF P5(:1) .EQ. "Y" .OR. M$EXIST("{P2}_corr.bdf") .EQ. 0 THEN
   write/out "     WARNING: Median filtering will take a while; don't panic."
   set/midas output=no
   set/format I1
   filter/median {p1} &f 5,5 ? {p3}
   set/midas output=yes
   compute/image {p2}_corr = {p1}/&f
else
  WRITE/OUT "     Flat was already corrected for large scale structure"
endif
stat/imag {p2}_corr {p3} ? ? 
cdsta  = OUTPUTR(3) - {P4}*{OUTPUTR(4)}
!cdend  = {OUTPUTR(2)}
cdend  = 99999.
find/pixel {p2}_corr{p3} {cdsta},{cdend} out A dummtab 20000

if M$EXIST("dummtab.tbl") .EQ. 0 then
   write/out " "
   write/out "     *** INFO: No cold pixels below intensity {cdsta}"
else
   name/colu dummtab :x_pix :x
   name/colu dummtab :y_pix :y
   sort/table dummtab :x,:y
   write/keyw in_a/c/1/60  dummtab
   write/keyw out_a/c/1/60 {p2}_coldpix
   run STD_EXE:ccdhotpix
!
   if M$EXIST("{out_a}.tbl") .EQ. 0 then
      write/out "     No cold pixels in output cold pixel table."
   else   
      read/table {p2}_coldpix
      write/out "     Cold pixels inside the area {p3} are stored in MIDAS "
      write/out "     table {p2}_coldpix.tbl"
      delete/table dummtab NO
   endif
endif
RETURN














