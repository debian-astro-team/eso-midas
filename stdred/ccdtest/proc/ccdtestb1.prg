! @(#)ccdtestb1.prg	19.1 (ES0-DMD) 02/25/03 14:17:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestb.prg
!.PURPOSE:        Procedure to test bias characteristics: combine bias frames
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestb1 p1 p2 p3 p4
!.                      p1 = input catalogue containing the ccd bias frames
!.                      p2 = output identifier [bias]
!.                      p3 = method for computing the combined bias
!.                      P4 = overwrite option for combined bias frame [N]
!.USE:            @s ccdtestb1 p1 p2 p3 p4
!.Algorithm:      Creation of the combine bias frame. The combining method is 
!                 taken from P3. The result is load onto the display and a 
!                 hardcopy is generated in file {p2}_aver.ps
!.VERSION:        940620 RHW creation
!.VERSION:        950115 RHW Inclusion of the subtest 
!.VERSION:        950808 RHW Seperate procedure for B1 test
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?         C  "Enter input catalogue:"
DEFINE/PARAMETER P2  BIAS      C  "Enter combined output frame:"
DEFINE/PARAMETER P3  median    C  "Combining method:"
DEFINE/PARAMETER P4  N         C  "Enter overwrite option:"
!
DEFINE/LOCAL BSSTA/R/1/1      0.0
DEFINE/LOCAL BSEND/R/1/1      0.0
DEFINE/LOCAL BSSTE/R/1/1      0.0
DEFINE/LOCAL XSTA/R/1/1       0.0
DEFINE/LOCAL XEND/R/1/1       0.0
DEFINE/LOCAL YSTA/R/1/1       0.0
DEFINE/LOCAL YEND/R/1/1       0.0
DEFINE/LOCAL SCALE/I/1/1      0.0
!
! *** check if the catalogue exists
IF M$EXIST(P1) .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTB1>: Catalogue {P1} not existing"
   RETURN
ENDIF
!
!
WRITE/OUT "Test B1: Creating combined master bias frame"
write/out "------------------------------------------------------------------"
write/out "     All frames in the input catalogue are combined using the"
write/out "     <{P3}> method. The output frame will be {P2}.bdf.
write/out " "
!
! *** first set all the keywords for the combining
IF P4(:1) .EQ. "Y" .OR. M$EXIST("{P2}.bdf") .EQ. 0 THEN
   define/local sav_met/c/1/20   "{bs_met}"       ! save the combining method
   write/keyw   bs_met/c/1/20    "{p3}"           ! store new method
   define/local sav_del/c/1/3    "{bs_del}"       ! save the combining method
   write/keyw   bs_del/c/1/3     "NO"             ! store new method
   define/local sav_typ/c/1/60   "{bs_typ}"       ! save the exposure type
   write/keyw   bs_typ/c/1/60    "?"              ! store new method
   delete/ima {p2} no
   @s ccdcombine bs {p1} {p2}
   write/keyw   bs_met/c/1/20    "{sav_met}"      ! restore method
   write/keyw   bs_del/c/1/3     "{sav_del}"      ! restore deletion option
   write/keyw   bs_typ/c/1/60    "{sav_typ}"      ! restore exposure type

ELSE
   WRITE/OUT "*** INFO: Combined BIAS already present and not overwritten"
ENDIF
!
statist/imag {p2} ? ? ? FN              ! calculate statistics
bssta = OUTPUTR(3) - 1.0*OUTPUTR(4)
bsend = OUTPUTR(3) + 1.0*OUTPUTR(4)
!bssta = OUTPUTR(3) - 2.5
!bsend = OUTPUTR(3) + 2.5
!bssta = OUTPUTR(3) - 1.5*OUTPUTR(4)
!bsend = OUTPUTR(3) + 1.5*OUTPUTR(4)
cuts/image {p2} {bssta},{bsend}
scale = {{p2},NPIX(1)}/512
create/display
load/itt expo
load/image {p2} scale=-{scale}
set/midas output=no
copy/display p5=noprint
-copy screen{mid$sess(11:12)}.ps {p2}.ps
set/midas output=yes
write/out "     Image of average bias frame is in file {p2}.ps"
write/out " "
delete/image screen{mid$sess(11:12)}.ima NO
!
RETURN




