! @(#)ccdtests.prg	19.1 (ES0-DMD) 02/25/03 14:17:12
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtests.prg
!.PURPOSE:        Procedure to the ccd shutter characteristics
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtests p1 p2 p3 p4 p5
!.                where p1 = frame 1 with interruped shutter openings
!.                      p2 = frame 2 with continous shutter opening
!.                      p3 = output frame with the shutter error
!.                      p4 = Number of short exposures in the first frame
!.                      p5 = decimation frame for frame size reduction
!.USE:            @@ ccdtests p1 p2 p3 p4
!.Algorithm:      The following tests are done:
!.VERSION:        940621 RHW creation
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?            ima  "Enter input frame 1:"
DEFINE/PARAMETER P2  ?            ima  "Enter input frame 2:"
DEFINE/PARAMETER P3  SHERR        ima  "Enter output shutter error frame:"
DEFINE/PARAMETER P4  ?            N    "Enter number of exposures:"
DEFINE/PARAMETER P5  16           N    "Box size for averaging:"
!
define/local xlab/r/1/1 0.0
define/local ylab/r/1/1 0.0
define/local xend/i/1/1 0
define/local yend/i/1/1 0
define/local nexp/I/1/1 {p4}
define/local texp1/I/1/1 0.0
define/local texp2/I/1/1 0.0
define/local nbox/I/1/1 {p5}
define/local rebin/c/1/3 "mea"
define/local cnste/r/1/1 0.0
define/local cnsta/r/1/1 0.0
define/local cnend/r/1/1 0.0
define/local scale/r/1/1 0.0
define/local tdef/r/1/1 0.0
!
create/display
crea/graph; set/graph pmode=1 frame=square
!
texp1 = {{p1},{O_DESC}} 
texp2 = {{p2},{O_DESC}}
!
write/out "Test S: Determine the shutter error distribution defined by:"
write/out "        {p3} = ({p2}*{texp1} - {p1}*{texp2})/({p1} - {p4} * {p2})." 
write/out "        The first frame is assumed to be taken by openening and 
write/out "        closing the shutter many times ({p4}; the second frame is"
write/out "        one long exposure. The output error frame is rebinning by "
write/out "        determining the median within boxes of {p5}x{p5} pixels"
write/out "----------------------------------------------------------------"
write/out " "
!
compute/image &a = ({p2}*{texp1} - {p1}*{texp2})/({p1} - {nexp}*{p2})
in_a = "middumma"
out_a = "{p3}"
out_b = "middummb"
run STD_EXE:ccdrebin
write/desc {p3} IDENT/C/1/40 "Shutter error distribution"
delete/imag middumma NO
delete/imag middummb NO
xend = {{p3},NPIX(1)} - 2
yend = {{p3},NPIX(2)} - 2
statis/imag {p3} [@3,@3:@{xend},@{yend}] ? ? FS
cnsta = 0.0
cnend = {outputr(2)} 
cnste = {outputr(4)}
!
scale = 512/{{p3},NPIX(1)} 
load/imag {p3} scale={scale}
load/lut rainbow
set/midas output=logonly
copy/display p5=noprint
-copy screen{mid$sess(11:12)}.ps {p3}.ps
set/midas output=on
!
set/grap; set/grap frame=square pmode=2
plot/cont {p3} [@3,@3:@{xend},@{yend}] ? {cnsta}:{cnend}:{cnste}
copy/graph postscript
-copy postscript.ps {p3}_cont.ps
write/out "     Image of the shutter error distribution in {p3}.ps"
write/out "     Plot of the shutter error distribution in {p3}_cont.ps"
!
delete/imag screen*.ps NO
delete/imag screen*.ima NO
delete/imag postscript*.ps NO
delete/imag graph*.plt NO
RETURN





