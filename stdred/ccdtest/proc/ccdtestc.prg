! @(#)ccdtestc.prg	19.1 (ES0-DMD) 02/25/03 14:17:11
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestc.prg
!.PURPOSE:        Procedure to the ccd charge transfer efficiency
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestc p1 p2 p3 p4
!.                where p1 = input frame
!                       p2 = image section in the x direction in pixels
!.                      p3 = row range to be averged the horizontal CTE
!                       p4 = image section in the y direction in pixels
!.                      p5 = column range to be averged the horizontal CTE
!.VERSION:        940621 RHW creation
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?     ima  "Enter input frame:"
DEFINE/PARAMETER P2  <,>   C    "Enter row range to be averaged:"
DEFINE/PARAMETER P4  <,>   C    "Enter column range to be averaged:"
!
write/out "Test C: Determine the charge transfer efficiency using the "
write/out "        Extened Pixel Edge Response test (EPER test, Janesick"
write/out "        et al, 1987."
write/out "----------------------------------------------------------------"
write/out " "
!
define/local rpx/i/1/3 0,0
define/local npx/i/1/1 0
define/local lpx/i/1/1 0
define/local bpx/i/1/1 0
define/local hcte/r/1/1 0.0
define/local vcte/r/1/1 0.0
!
! *** horizontal charge transfer efficiency
if p2 .NE. "0,0" THEN
   write/keyw rpx/i/1/3 {p3}
   npx = rpx(2)-rpx(1) + 1
   average/row HCTE = {p1} {p2}
   lpx = {rpx(2)}
   bpx = {rpx(3)}
   hcte = -
   1 - ({HCTE[@{bpx}}-{BIASMEAN}) / (({HCTE[@{lpx}]}-{BIASMEAN})*{npx})
   write/out "     The Horizontal CTE is: {hcte}"
else
   write/out "     The Horizontal CTE is not computed"
endif
!
! *** vertical charge transfer efficiency
if p4 .NE. "0,0" THEN
   write/keyw rpx/i/1/3 {p5}
   npx = rpx(2)-rpx(1) + 1
   rotate/counter {p1} &a 1
   average/row VCTE = &a {p4}
   flip/image &a
   lpx = {rpx(2)}
   bpx = {rpx(3)}
   vcte = -
   1 - ({VCTE[@{bpx}]}-{BIASMEAN}) / (({VCTE[@{lpx}]}-{BIASMEAN})*{npx})
   write/out "     The Vertical CTE is: {vcte}"
   delete/imag &a no
else
   write/out "     The Vertical CTE is not computed"
endif
!
write/out "End of test C" 
RETURN





