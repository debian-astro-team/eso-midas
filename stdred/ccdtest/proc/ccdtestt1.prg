! @(#)ccdtestt1.prg	19.1 (ES0-DMD) 02/25/03 14:17:12
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestt1.prg
!.PURPOSE:        Procedure to the test ccd linearity and transfer
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestt p1  p2 p3 p4 p5
!.                where p1 = input catalogue containing the ccd bias frames
!.                      p2 = output identifier [TRANS]
!.                      p3 = image section to be used for the median and
!.                           variance determination
!.USE:            @@ ccdtestt1 p1 p2 p3 p4
!.Algorithm:      The following tests are done:
!.                Creation of the transfer and linearity table"
!.VERSION:        940524 RHW creation
!.VERSION:        950809 RHW Split into single procedures
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?             C  "Enter input catalogue:"
DEFINE/PARAMETER P2  TRANS         C  "Enter combined output frame:"
DEFINE/PARAMETER P3  [<,<:>,>]     C  "Enter the window for analysis:"
DEFINE/PARAMETER P4  N             C  "Enter overwrite option for table:"
! 
define/local xlab/r/1/1 0.0
define/local ylab/r/1/1 0.0
define/local aducf1/r/1/1 0.0
define/local aducf2/r/1/1 0.0
define/local enoise1/r/1/1 0.0
define/local enoise2/r/1/1 0.0
define/local shval/r/1/3 0.0,0.0,0.0
define/local sherr/r/1/1 0.0
define/local shut/r/1/1 0.0
define/local shoff1/r/1/1 0.0
define/local shoff2/r/1/1 0.0
define/local nstep/i/1/1 0
define/local iter/i/1/1 0
define/local n/i/1/1 0
!
! *** check if the catalogue exists
IF M$EXIST(P1) .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTT1>: Catalogue {P1} not existing"
   RETURN
ENDIF
!
set/midas output=logonly
read/icat {p1}
set/midas output=no
crea/graph; set/graph 
set/graph pmode=1
set/midas output=yes
!
define/local col1/c/1/20  :UT_tim1 
define/local col2/c/1/20  :UT_tim2 
define/local col3/c/1/20  :Exp_tim1 
define/local col4/c/1/20  :Exp_tim2
define/local col5/c/1/20  :Med_cnt1
define/local col6/c/1/20  :Med_cnt2
define/local col7/c/1/20  :Variance
define/local col8/c/1/20  :Frc_cnt1
define/local col9/c/1/20  :Frc_cnt2
define/local col10/c/1/20 :scratch
!
write/out "Test T1: Creation of the transfer and linearity table"
write/out "-------------------------------------------------------------------"
write/out "     The table will contain 7 columns: column 1 for the UT of the"
write/out "     of the exposure of the first image; column 2 for the UT of "
write/out "     the exposure of the second image; column 3 for the exposure"
write/out "     time of the first image; column 4 the exposure time of the "
write/out "     second image; column 5 the median pixel intensity over the"
write/out "     selected image section in frame a; column 6 the median pixel"
write/out "     intensity over the selected frame area in frame b; column 7"
write/out "     the variance of the difference of the frame a and b. 
write/out " "
write/out "     The median and variance values have been corrected for an"
write/out "     assumed bias offset of {BIASMEAN}"
write/out " "
!
IF P4(:1) .EQ. "Y" .OR. M$EXIST("{P2}.tbl") .EQ. 0 THEN
   create/table  {p2} 9 20 null
   create/table  {p2} 9 20 null
   create/colum {p2} {col1}      "hours"   F9.3 R*4
   create/colum {p2} {col2}      "hours"   F9.3 R*4
   create/colum {p2} {col3}      "sec"     F9.3 R*4
   create/colum {p2} {col4}      "sec"     F9.3 R*4   
   create/colum {p2} {col5}      "ADU"     F9.4 R*4
   create/colum {p2} {col6}      "ADU"     F9.4 R*4
   create/colum {p2} {col7}      "ADU"     F9.4 R*4
   create/colum {p2} {col8}      "ADU/sec" F9.4 R*4
   create/colum {p2} {col9}      "ADU/sec" F9.4 R*4
   create/colum {p2} {col10}      "   "    F9.4 R*4
!
   define/local ut_time/c/1/9 "O_TIME(5)"
   define/local catal/i/1/1 0
   define/local ifrm/i/1/1 0
   define/local frm1/c/1/60 " "
   define/local frm2/c/1/60 " "
   define/local var/r/1/1 0.0
   define/local tdef/r/1/1 0.0

   IF M$TSTNO(BIASMEAN) .EQ. 0 THEN
      write/OUT "     *** INFO: KEYWORD BIASMEAN is not filled with a number"
      write/OUT "               No bias offset subtracted"
   ENDIF
!
   LOOP1:
      store/frame in_a {p1} 1 CONT1
      frm1 = "{in_a}"
      store/frame in_b {p1} 1 CONT1
      frm2 = "{in_b}"
      tdef = {{frm1},{O_DESC}} - {{frm2},{O_DESC}}
      tdef = M$ABS({tdef})
      if tdef .GT. 0.02 then
         write/out "     *** WARNING: exposure times {frm1} amd {frm2} differ "
         write/out "                  more than .02 sec: image pair skipped"

      else
         ifrm = ifrm + 1
         {p2},{col1},@{ifrm} = {{frm1},{ut_time}}
         {p2},{col2},@{ifrm} = {{frm2},{ut_time}}
         {p2},{col3},@{ifrm} = {{frm1},{O_DESC}}
         {p2},{col4},@{ifrm} = {{frm1},{O_DESC}}
         stat/ima {frm1} {p3} ? ? FN
         {p2},{col5},@{ifrm} = {outputr(8)} - {BIASMEAN}
         stat/imag {frm2} {p3} ? ? FN
         {p2},{col6},@{ifrm} = {outputr(8)} - {BIASMEAN}
         comp/imag &a = {frm1} - {frm2}
         stat/imag &a {p3} ? ? FN
         var = outputr(4) * outputr(4)
         {p2},{col7},@{ifrm} = {var}
      endif
   goto LOOP1
   CONT1:
!
   delete/imag &a NO
   write/out
   write/out "     Content of unedited table {p2}:"
   read/table {p2} {col1},{col2},{col3},{col4},{col5},{col6},{col7}

else
   WRITE/OUT "     *** INFO: Linearity table {p2} exists; no new one created"
ENDIF
!
!
! *** table ready do the printed and plotted
SELECT/TABLE {P2} ALL
sort/table {p2} {col3}
read/table {p2} {col1},{col2},{col3},{col4},{col5},{col6},{col7}
write/out " "
write/out "     Table with exposure sequence is in file {p2}_table.txt"
!
plot/table {p2} {col1} {col3} -60,-40,25,75
xlab = plrgrap(1) + (plrgrap(2)-plrgrap(1))/10
ylab = plrgrap(6) - (plrgrap(6)-plrgrap(5))/10
label/graph "Exposure sequence" {xlab},{ylab} ? ? 1
!
set/graph clear=off yform=e6.2
plot/tab {p2} {col3} {col5} -60,-40,25,15
xlab = plrgrap(1) + (plrgrap(2)-plrgrap(1))/10
ylab = plrgrap(6) - (plrgrap(6)-plrgrap(5))/10
label/graph "Linearity Curve (1)" {xlab},{ylab} ? ? 1
!
plot/tab {p2} {col4} {col6} -60,-40,115,15
xlab = plrgrap(1) + (plrgrap(2)-plrgrap(1))/10
ylab = plrgrap(6) - (plrgrap(6)-plrgrap(5))/10
label/graph "Linearity Curve (2)" {xlab},{ylab} ? ? 1
!
set/grap xform=e6.0
plot/tab {p2} {col5} {col7} -60,-40,115,75
xlab = plrgrap(1) + (plrgrap(2)-plrgrap(1))/10
ylab = plrgrap(6) - (plrgrap(6)-plrgrap(5))/10
label/graph "Transfer Curve" {xlab},{ylab} ? ? 1
!
copy/graph postscript 
-copy postscript.ps {p2}_lin1.ps
write/out "     Plot with exposure sequence v. median counts, linearity"
write/out "     curve (1), linearity curve (2), and transfer curve "
write/out "     stored in file {p2}_lin1.ps"
set/graph clear=on
write/out " "




