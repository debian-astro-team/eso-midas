! @(#)ccdtestb5.prg	19.1 (ESO-DMD) 02/25/03 14:17:11
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestb5.prg
!.PURPOSE:        Procedure to find the means and standard deviations
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestb5
!.                where p1 = input catalogue containing the ccd bias frames
!.                      p2 = output identifier [bias]
!.                      p3 = area
!.                      p4 = radius of smoothing box for median filtering,
!.                           decimation factor for image size reduction
!.USE:            @s ccdtestb5 p1 p2 p3 p4 
!.Algorithm:      Determination of the bias level and standard deviation. For 
!.                each input frame in the catalogue determine the mean bias 
!.                and standard deviation after hot pixel correction, box 
!.                averaging and median filtering. The overall mean bias level 
!.                and  mean sigma is computed."
!.VERSION:        940620 RHW creation
!.VERSION:        950115 RHW Inclusion of the subtest 
!.VERSION:        950808 RHW Split into single procedures
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/parameter p1  ?         C  "Enter input catalogue:"
define/parameter p2  BIAS      C  "Enter output identifier:"
define/parameter p3  [<,<:>,>] C  "Enter area:"
define/parameter p4  5,10      N  "Radius med. filtering, and dec. factor:"
!
write/keyw   inputi/i/1/2     {P4}
write/keyw   radius/i/1/1     {inputi(1)}
define/local nbox/i/1/1       {inputi(2)}
define/local mean/r/1/1 0.0
define/local msum/r/1/1 0.0
define/local sigm/r/1/1 0.0
define/local ssum/r/1/1 0.0
define/local frame/c/1/60 " "
define/local myouta/c/1/60 " "
write/keyw   catal/i/1/1 0
write/keyw   rebin/c/1/3 "mea"
define/local ifrm/i/1/1 0
!
! *** check if the catalogue exists
IF M$EXIST(P1) .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTB5>: Catalogue {P2} not existing"
   RETURN
ENDIF
!
write/out "Test B5: Determination of the bias level and standard deviation"
write/out "------------------------------------------------------------------"
write/out "     For each input frame in the catalogue determine the mean"
write/out "     BIAS and standard deviation AFTER hot pixel correction,"
write/out "     Box averaging and median filtering." 
write/out "     Finally, the overall mean bias level and sigma are computed."
write/out
create/table {p2}_mean 3 10 null
create/colum {p2}_mean :NAME  "   " A20  C*20
create/colum {p2}_mean :MEAN  "ADU" F9.4 R*4
create/colum {p2}_mean :SIGMA "ADU" F9.4 R*4
LOOP2:
   store/frame in_a {p1} 1 FINISH
   ifrm  = ifrm + 1
   frame = in_a
   set/midas output=no
   if M$EXIST("{p2}_hotpix.tbl") .EQ. 1 THEN
      modify/colu {frame},{p2}_hotpix middummb V        ! modify the hot pixels
   else
      copy/ii {frame} middummb
   endif
   set/midas output=logonly
!
   extract/imag middumma = middummb{p3}
   in_a  = "middumma"
   out_a = "bs_mean"
   out_b = "bs_sigm"
   myouta = out_a(1:60)
   run STD_EXE:ccdrebin
   filter/median {myouta} &a {RADIUS},{RADIUS},0.0 
   filter/median {OUT_B} &b {RADIUS},{RADIUS},0.0 
   statist/imag &a ? ? ? NN
   mean  = outputr(3)
   statist/imag &b ? ? ? NN
   sigm = outputr(3)
   {p2}_mean,:NAME,@{ifrm}  = "{frame}"
   {p2}_mean,:MEAN,@{ifrm}  =  {mean}
   {p2}_mean,:SIGMA,@{ifrm} =  {sigm}
   set/midas output=yes
   write/out "     Statistics of {frame}: mean = {mean}; sigma = {sigm}"
   msum = msum + mean
   ssum = ssum + sigm
   biasmean = msum/ifrm
   biassigm = ssum/ifrm
   goto LOOP2
FINISH:
!
ifrm = ifrm + 1
{p2}_mean,:NAME,@{ifrm}  = "all_average"
{p2}_mean,:MEAN,@{ifrm}  =  {biasmean}
{p2}_mean,:SIGMA,@{ifrm} =  {biassigm}
delete/imag {myouta} no
delete/imag {OUT_B} no
delete/imag &a no
write/out " "
write/out -
       "     Statistics all bias frames: mean = {biasmean}; sigma = {biassigm}"

write/out "     The MIDAS table with results is in file {p2}_mean.tbl"
!
! cleanup 
delete/imag bs_sigm.bdf NO
delete/imag bs_mean.bdf NO
delete/imag postscript.ps NO
delete/imag screen*.ps NO
delete/imag screen*.ima NO
delete/imag graph*.plt NO
assign/print



