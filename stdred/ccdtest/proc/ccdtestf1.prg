! @(#)ccdtestf1.prg	19.1 (ESO-DMD) 02/25/03 14:17:11
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestf1.prg
!.PURPOSE:        Procedure to test the ccd flat field characteristics
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestf1
!.                where p1 = input catalogue containing the ccd bias frames
!.                      p2 = output identifier [flat]
!.                      p3 = method for computing the combined flat
!.                      p4 = area
!.                      p5 = range in exposure time
!.                      p6 = overwrite flag
!.USE:            @@ ccdtestf1 p1 p2 p3 p4 p5 p6
!.Algorithm:      Creating of the combined master flat frame
!                 All frames in the input flat catalogue are combined
!                 using the {P3} method. A printout of the combined
!                 frame is produced."
!.VERSION:        940620 RHW creation
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
DEFINE/PARAMETER P1  ?             C  "Enter input catalogue:"
DEFINE/PARAMETER P2  FLAT          C  "Enter combined output frame:"
DEFINE/PARAMETER P3  median        C  "Enter combining method:"
DEFINE/PARANETER P4  [<,<:>,>]     C  "Enter area for median filtering:"
DEFINE/PARAMETER P5  0,5           N  "Enter exposure time range:"
DEFINE/PARAMETER P6  N             C  "Enter overwrite option:"
!
DEFINE/LOCAL LDSTA/R/1/1      0.0
DEFINE/LOCAL LDEND/R/1/1      0.0
DEFINE/LOCAL SCALE/R/1/1      1.0
WRITE/KEYW   INPUTR/R/1/2     {P5}
!
! *** check if the catalogue exists
IF M$EXIST(P1) .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTF1>: Catalogue {P1} not existing"
   RETURN
ENDIF
!
WRITE/OUT "Test F1: Creating combined master flat frame"
write/out "------------------------------------------------------------------"
write/out "     Frames in the input flat catalogue are combined using the"
write/out "     <{P3}> method. The exposure times of the flats is in the range
write/out "     <{p5}>. The output frame is corrected for the bias offset "
write/out "     obtained from the keyword BIASMEAN." 
write/out " "

IF P4(:1) .EQ. "Y" .OR. M$EXIST("{P2}.bdf") .EQ. 0 THEN
   define/local catal/i/1/1 0
   define/local nfrm/i/1/1 0

!  set/midas output=logonly
   CREATE/ICAT flatdumm.cat NULL
   LOOP1:
      store/frame in_a {p1} 1 CONT1
      if {{in_a},{O_DESC}} .GE. inputr(1) .and. -
         {{in_a},{O_DESC}} .LE. inputr(2)  then
         nfrm = nfrm + 1
         set/midas output=logonly
         add/icat flatdumm.cat {in_a}
         set/midas output=yes
      endif
      goto LOOP1
   CONT1:
   set/midas output=yes

   define/local sav_met/c/1/20   "{ff_met}"       ! save the combining method
   write/keyw   ff_met/c/1/20    "{p3}"           ! store new method
   define/local sav_sca/c/1/3    "{ff_sca}"       ! save the scalng method
   write/keyw   ff_sca/c/1/3     "no"             ! store no scaling
   define/local sav_del/c/1/3    "{ff_del}"       ! save the deletion
   write/keyw   ff_del/c/1/3     "NO"             ! store new method
   define/local sav_typ/c/1/60   "{ff_typ}"       ! save the exposure type
   write/keyw   ff_typ/c/1/60    "?"              ! store no type check
!
   @s ccdcombine ff flatdumm.cat {p2}
   write/keyw   ff_met/c/1/20    "{sav_met}"      ! restore method
   write/keyw   ff_sca/c/1/3     "{sav_sca}"      ! restore scaling
   write/keyw   ff_del/c/1/3     "{sav_del}"      ! restore deletion option
   write/keyw   ff_typ/c/1/60    "{sav_typ}"      ! restore exposure type
   -delete flatdumm.cat
   compute/image {p2} = {p2} - {BIASMEAN}

else
   WRITE/OUT -
  "     Combined flat frame {p2}.bdf exists; no new one created"
endif
!
statistic/image {p2} {p4} ? ? FN
ldsta  = OUTPUTR(3) - 1.5 * OUTPUTR(4)         ! used for display
ldend  = OUTPUTR(3) + 1.5 * OUTPUTR(4)
scale = {{p2},NPIX(1)}/512
load/itt expo
load/image {p2} scale=-{scale} cuts={ldsta},{ldend}
set/midas output=no
copy/display p5=noprint
-copy screen{mid$sess(11:12)}.ps {p2}.ps
set/midas output=yes
write/out "     Combined output flat is stored in file {p2}.bdf"
write/out "     Image of average flat frame is in file {p2}.ps"
write/out " "
delete/imag screen{mid$sess(11:12)}.ima NO
delete/imag screen{mid$sess(11:12)}.ps NO














