! @(#)ccdtestt2.prg	19.1 (ES0-DMD) 02/25/03 14:17:13
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestt2.prg
!.PURPOSE:        Procedure to the test ccd linearity and transfer
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestt2 p1  p2
!.                where p1 = input table
!.                      p2 = selection criteria table 
!.USE:            @@ ccdtestt2 p1 p2
!.Algorithm:      Determination of linearity curve, count rates, shutter error
!.VERSION:        940524 RHW creation
!.VERSION:        950809 RHW Split in single procedures
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?             T  "Enter input table:"
DEFINE/PARAMETER P2  TRANS         C  "Enter output identifier:"
DEFINE/PARAMETER P3  all           C  "Selection criterium for table:"
DEFINE/PARAMETER P4  -2.0,2.0      C  "Time interval to fit the shutter offset"
! 
define/local xlab/r/1/1 0.0
define/local ylab/r/1/1 0.0
define/local shval/r/1/3 0.0,0.0,0.0
define/local sherr/r/1/1 0.0
define/local shut/r/1/1 0.0
define/local shoff1/r/1/1 0.0
define/local shoff2/r/1/1 0.0
define/local nstep/i/1/1 0
define/local iter/i/1/1 0
define/local n/i/1/1 0
define/local noff/i/1/1 0
write/keyw   shval/r/1/2 {p4}
write/keyw   shval/r/3/1 0.1
nstep = (shval(2)-shval(1))/shval(3)
!
! *** check if the catalogue exists
IF M$EXIST("{P1}.tbl") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTT2>: Table {P1} not existing"
   RETURN
ELSE
   read/table {p1}
ENDIF
!
crea/graph; set/graph 
set/graph pmode=1
!
define/local col1/c/1/20  :UT_tim1 
define/local col2/c/1/20  :UT_tim2 
define/local col3/c/1/20  :Exp_tim1 
define/local col4/c/1/20  :Exp_tim2
define/local col5/c/1/20  :Med_cnt1
define/local col6/c/1/20  :Med_cnt2
define/local col7/c/1/20  :Variance
define/local col8/c/1/20  :Frc_cnt1
define/local col9/c/1/20  :Frc_cnt2
define/local col10/c/1/20 :scratch
!
write/out "Test T2: Determination of linearity curve and the shutter error"
write/out "----------------------------------------------------------------"
write/out "     Entries in the table {p1} not fullfilling the selection"
write/out "     criteria <{p3}> will now be selected out."
write/out "     From the remaining entries in the table a linear fit is"
write/out "     done to determine the linearity curve for frames 1 and 2."
write/out " "
!
! *** linearity curve of frames 1
set/format F6.3
set/midas output=logonly
select/table {p1} {p3}
set/graph stype=5 xform= yform=e6.2
plot/table {p1} {col3} {col5} -60,-40,25,75
set/graph clear=off
xlab = plrgrap(1) + (plrgrap(2)-plrgrap(1))/10
ylab = plrgrap(6) - (plrgrap(6)-plrgrap(5))/10
label/grap "Linearity Curve (1)" {xlab},{ylab} ? ? 1
regression/linear {p1} {col5} {col3}
save/regress {p1} regress
compute/regress {p1} {col10} = regress
set/midas output=yes
write/out "     Linearity results data set 1:"
write/out "     Mean count rate: {outputd(2)} ADU/sec"
sherr = {outputd(1)}/{outputd(2)}
write/out "     Exposure offset: {sherr} seconds (+ = slow; - = fast)" 
set/graph ltyp=2 stype=0
overplot/tab {p1} {col3} {col10}
!
! *** linearity curve of frames 2
set/grap stype=5
set/midas output=logonly
select/tab {p1} {p3}
set/grap stype=5 xform= yform=e6.2
plot/tab {p1} {col4} {col6} -60,-40,115,75
xlab = {plrgrap(1)} + ({plrgrap(2)}-{plrgrap(1)})/10
ylab = {plrgrap(6)} - ({plrgrap(6)}-{plrgrap(5)})/10
label/grap "Linearity Curve (2)" {xlab},{ylab} ? ? 1
regression/lin {p1} {col6} {col4}
save/regress {p1} regress
compute/regress {p1} {col10} = regress
set/midas output=yes
write/out " "
write/out "     Linearity results data set 1:"
write/out "     Mean count rate: {outputd(2)} ADU/sec"
sherr = outputd(1)/outputd(2)
write/out "     Exposure offset: {sherr} seconds (+ = slow; - = fast)" 
set/graph stype=0
overplot/table {p1} {col4} {col10}
!

! *** First set: fractional count rate versus median count
write/descr {p1}.tbl HISTORY_UPDA/I/1/1 0
write/out " "
write/out "     Determining shutter offset from fractional count rates (1)"
set/midas output=no
!
ITER1:
shoff1 = 999.
do N = 1 {NSTEP}
   shut = shval(1) + (n-1)*shval(3)
   compute/table {p1} {col8} = {col5}/({col3}+{shut})
   select/table {p1} {p3}
   regression/linear {p1} {col8} {col3}
   shoff2 = {outputr(4)}
   if shoff2 .GT. {shoff1} .and. ITER .eq. 0 then
      shval(1) = shval(1) + (n-3)*shval(3)
      shval(3) = shval(3)/10
      nstep = 20
      iter  = 1
      goto ITER1
   else if shoff2 .GT. {shoff1} .and. ITER .eq. 1 then
      shut = shval(1) + (n-2)*shval(3)
      goto DONE1
   else
      shoff1 = {shoff2}
   endif
end do
!
! plot the fractional count rate and the fit
DONE1:
write/descr {p1}.tbl HISTORY_UPDA/I/1/1 1
set/midas output=yes
write/out "     Shutter offset: {shut} seconds"

compute/table {p1} {col8} = {col5}/({col3}+{shut})
set/midas output=no
select/table {p1} {p3}
stat/table {p1} {col8}
compute/table {p1} {col8} = {col8}/{outputr(3)}
select/table {p1} {p3}
set/midas output=yes
set/graph stype=5 xform=e6.0 yform=f6.4 clear=off
plot/table {p1} {col5} {col8} -60,-40,25,15
xlab = plrgrap(1) + (plrgrap(2)-plrgrap(1))/10
ylab = plrgrap(6) - (plrgrap(6)-plrgrap(5))/10
label/graph "Fract. Count Rate (1)" {xlab},{ylab} ? ? 1
!
set/midas output=no
regression/linear {p1} {col8} {col3}
set/midas output=yes
save/regress {p1} regress
compute/regress {p1} {col10} = regress
set/graph ltype=2 stype=0
overplot/table {p1} {col5} {col10}
!
! *** Second set: fractional count rate versus median count
write/descr {p1}.tbl HISTORY_UPDA/I/1/1 0
write/out " "
write/out "     Determining shutter offset from fractional count rates (2)"
set/midas output=no
write/keyw   shval/r/1/2 {p4}
write/keyw   shval/r/3/1 0.1
nstep = ({shval(2)}-{shval(1)})/{shval(3)}
iter  = 0
!
ITER2:
shoff1 = 999.
do N = 1 {nstep}
   shut = shval(1) + (n-1)*shval(3)
   compute/table {p1} {col9} = {col6}/({col4}+{shut})
   select/table {p1} {p3}
   regression/linear {p1} {col9} {col4}
   shoff2 = {outputr(4)}
   if shoff2 .GT. {shoff1} .and. ITER .eq. 0 then
      shval(1) = shval(1) + (n-3)*shval(3)
      shval(3) = shval(3)/10
      nstep = 20
      iter  = 1
      goto ITER2
   else if shoff2 .GT. {shoff1} .and. ITER .eq. 1 then
      shut = shval(1) + (n-2)*shval(3)
      goto DONE2
   else
      shoff1 = {shoff2}
   endif
end do
!
! plot the fractional count rate and the fit
DONE2:
write/descr {p1}.tbl HISTORY_UPDA/I/1/1 1
set/midas output=yes
write/out "     Shutter offset: {shut} seconds"
compute/table {p1} {col9} = {col6}/({col4}+{shut})
set/midas output=no
select/table {p1} {p3}
stat/table {p1} {col9}
compute/table {p1} {col9} = {col9}/{outputr(3)}
select/table {p1} {p3}
set/midas output=yes
set/graph stype=5 xform=e6.0 yform=f6.4 clear=off
plot/table {p1} {col6} {col9} -60,-40,115,15
xlab = plrgrap(1) + (plrgrap(2)-plrgrap(1))/10
ylab = plrgrap(6) - (plrgrap(6)-plrgrap(5))/10
label/graph "Fract. Count Rate (2)" {xlab},{ylab} ? ? 1
!
set/midas output=no
regression/lin {p1} {col9} {col4}
set/midas output=yes
save/regress {p1} regress
compute/regress {p1} {col10} = regress
set/graph ltype=2 stype=0
overplot/table {p1} {col6} {col10}
set/graph clear=on
!
!*** all done; get the postscript file
copy/graph postscript 
-copy postscript.ps {p2}_lin2.ps
write/out " "
write/out "     Plot with fitted linearity curves in file {p2}_lin2.ps"
set/graph clear=on
write/out " "













