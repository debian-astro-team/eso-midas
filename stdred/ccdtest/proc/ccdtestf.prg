! @(#)ccdtestf.prg	19.1 (ES0-DMD) 02/25/03 14:17:11
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestf.prg
!.PURPOSE:        Procedure to test the ccd flat field characteristics
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestf
!.                where p1 = input catalogue containing the ccd bias frames
!.                      p2 = output identifier [flat]
!.                      p3 = method for computing the combined flat
!.                      p4 = area
!.                      p5 = range of exposure times to be combined:"
!.                      p6 = threshold for determining the cold pixels.
!.USE:            @@ ccdtestf p1 p2 p3 p5 p5 p6
!.Algorithm:      The following tests are done:
!                 Test F1: Creating of the combined master flat frame"
!                          All frames in the input flat catalogue are combined
!                          using the {P4} method. A printout of the combined
!                          frame is produced."
!                 Test F2: Creating a cold pixel table.
!                          First the combined flat is corrected for the bias 
!                          offset. Therafter all pixels in the stacked master 
!                          flat frame that show values less than {p6} times the
!                          median counts in the frame are listed. Only pixels 
!                          within the area {p5} are considered and repetitions
!                          of cold pixels in the increasing y coordinate are 
!                          not listed.
!.VERSION:        940620 RHW creation
!.VERSION:        950813 RHW Split is single procedures
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?             C  "Enter input catalogue:"
DEFINE/PARAMETER P2  FLAT          C  "Enter combined output frame:"
DEFINE/PARAMETER P3  median        C  "Enter combining method:"
DEFINE/PARANETER P4  [<,<:>,>]     C  "Enter area for median filtering:"
DEFINE/PARAMETER P5  0,5           N  "Enter exposure time range:"
DEFINE/PARAMETER P6  5.0           N  "Enter sigma multiplier for cold pixels:"
!
! *** check if the catalogue exists
IF M$EXIST(P1) .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTF>: Catalogue {P1} not existing"
   RETURN/EXIT
ELSE
   read/icat {p1}
   write/out " "
ENDIF
!
create/display
@s ccdtestf1 {p1} {p2} {p3} {p4} {p5} ?
@s ccdtestf2 {p2} {p2} {p4} {p6} ?
RETURN














