! @(#)ccdtestt.prg	19.1 (ES0-DMD) 02/25/03 14:17:12
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestt.prg
!.PURPOSE:        Procedure to the test ccd linearity and transfer
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestt p1  p2 p3 p4 p5
!.                where p1 = test to be execcted
!.                      p2 = input catalogue containing the ccd bias frames
!.                      p3 = output identifier [TRANS]
!.                      p4 = image section to be used for the median and
!.                           variance determination
!.                      p5 = time interval to fit the shutter offset
!.                      p6 = selection criteria table 
!.USE:            @@ ccdtestt p1 p2 p3 p4
!.Algorithm:      The following tests are done:
!.                Test T1: Creation of the transfer and linearity table"
!.                Test T2: Determination of linearity curve and shutter error
!.                Test T3: Determination of the transfer curve
!.VERSION:        940524 RHW creation
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?             C  "Enter input catalogue:"
DEFINE/PARAMETER P2  TRANS         C  "Enter combined output frame:"
DEFINE/PARAMETER P3  [<,<:>,>]     C  "Enter the frame window for analysis:"
DEFINE/PARAMETER P4  -2.0,2.0      C  "Time interval to fit the shutter offset"
DEFINE/PARAMETER P5  all           C  "Selection criterium for table:"
! 
! *** check if the catalogue exists
IF M$EXIST(P1) .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTT>: Catalogue {P1} not existing"
   RETURN/EXIT
ELSE
   read/icat {p1}
   write/out " "
ENDIF
!
crea/graph; set/graph 
set/graph pmode=1
!
@s ccdtestt1 {p1} {p2} {p3} ?
@s ccdtestt2 {p2} {p2} {p5} {p4}
@s ccdtestt3 {p2} {p2} {p5}

