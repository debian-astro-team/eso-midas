! @(#)ccdtestb3.prg	19.1 (ES0-DMD) 02/25/03 14:17:11
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestb3.prg
!.PURPOSE:        Procedure to find the hot pixels in bias frame
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestb3
!.                      p1 = input bias frame
!.                      p2 = output identifier [bias]
!.                      p3 = area 
!.                      p4 = radius median filtering
!.                      p5 = overwrite option for filtered bias frame
!.USE:            @s ccdtestb3 p1 p2 p3 p4 p5
!.Algorithm:      Find the hot pixels. The combined bias frame is median 
!.                filtered and subtracted from the original. A plot is 
!.                generated showing the position of the hot pixels and the 
!.                affected columns (file {p2}_hotpix.ps). A MIDAS table 
!.                ({p2}_hotpix.tbl) will contain the pixels found.
!.VERSION:        940620 RHW creation
!.VERSION:        950115 RHW Inclusion of the subtest 
!.VERSION:        950808 RHE Split in seperate single procedures.
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?         I  "Enter input bias frame:"
DEFINE/PARAMETER P2  BIAS      C  "Enter combined output frame:"
DEFINE/PARAMETER P3  [<,<:>,>] C  "Enter area:"
DEFINE/PARAMETER P4  5         N  "Radius med. filtering:"
DEFINE/PARAMETER P5  N         C  "Enter overwrite option:"
!
DEFINE/LOCAL BSSTA/R/1/1      0.0
DEFINE/LOCAL BSEND/R/1/1      0.0
DEFINE/LOCAL BSSTE/R/1/1      0.0
!
! *** check if file  exists
IF M$EXIST("{P1}.bdf") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <TESTB3>: Input bias frame not existing"
   RETURN/EXIT
ENDIF
!
write/out "Test B3: Finding the hot pixels"
write/out "------------------------------------------------------------------"
write/out "     Generate a table with hot pixels inside the area {p7} by"
write/out "     subtracting the median smoothed stacked bias from the "
write/out "     unsmoothed one and doing statistics of the result frame."
write/out "     A plot and a MIDAS table with the hot pixels area is produced"
write/out " "
!
IF P5(:1) .EQ. "Y" .OR. M$EXIST("{P2}_hotpix.bdf") .EQ. 0 THEN
   write/out "     WARNING: Median filtering will take a while; don't panic."
   set/midas output=no
   set/format I1
   filter/median {p1} &s {p4},{p4},0.0 ? {p3}
   set/midas output=yes
   compute/image {p2}_hotpix = {p2} - &s
else
   WRITE/OUT "     Hot pixel frame {p2}_hotpix.bdf exists; no new one created"
endif
!
write/desc {p2}_hotpix IDENT/C/1/40 "{DETEC} - Hot pixels"
stat/image {p2}_hotpix {p3} ? ? FN
bssta = OUTPUTR(3) + 0.25 * OUTPUTR(4) + 5
set/graph frame=square xaxis= yaxis= pmode=1
plot/cont {p2}_hotpix {p3} ? {bssta} NEG 
bssta = -999999
bsend = OUTPUTR(3) + 0.25 * OUTPUTR(4) + 5
set/midas output=no
find/pixel {p2}_hotpix{p3} {bssta},{bsend} out A dummtab 50000
set/midas output=yes
!
IF M$EXIST("dummtab.tbl") .EQ. 0 .OR. {dummtab.tbl,TBLCONTR(4)} .EQ. 0 THEN
   WRITE/OUT "*** INFO: No pixels found outside range {bssta}, {bsend}"
   WRITE/OUT "          No hot pixel image and table produced"
   write/out " "
   RETURN

ELSE
   name/colu dummtab :x_pix :x
   name/colu dummtab :y_pix :y
   sort/table dummtab :x,:y
   write/keyw in_a/c/1/60  dummtab
   write/keyw out_a/c/1/60 {p2}_hotpix
   run STD_EXE:ccdhotpix
!
   IF M$EXIST("{p2}_hotpix.tbl") .EQ. 0 THEN
      WRITE/OUT "*** INFO: No pixels found outside range {bssta}, {bsend}"
      WRITE/OUT "          No hot pixel image and table produced"
      RETURN

   ELSE
      select/table {p2}_hotpix :VALUE.GT.0.0
      read/table {p2}_hotpix
      overpl/tabl {p2}_hotpix :x :y 5
      copy/graph postscript
      -copy postscript.ps {p2}_hotpix.ps
      write/out " "
      write/out "     The contour plot with hot pixels inside area {p7} is "
      write/out "     stored in {p2}_hotpix.ps. The MIDAS table containing the"
      write/out "     the hot pixels is in {p2}_hotpix.tbl"
      write/out " "
      delete/imag &s NO
   ENDIF
ENDIF
delete/tabl dummtab NO
!
RETURN




