! @(#)ccdtestb.prg	19.1 (ES0-DMD) 02/25/03 14:17:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdtestba.prg
!.PURPOSE:        Procedure to the ccd bias characteristics
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdtestba
!.                      p1 = input catalogue containing the ccd bias frames
!.                      p2 = output identifier [bias]
!.                      p3 = method for computing the combined bias
!.                      p4 = row range to be averaged
!.                      p5 = column range to be averaged
!.                      p6 = area
!.                      p7 = radius of smoothing box for median filtering,
!.                           decimation factor for image size reduction
!.USE:            @s ccdtestba p1 p2 p3 p4 p5 p6 p7 
!.Algorithm:      The following tests are done:
!                 TESTB1: Creation of the combine bias frame. The combining
!                         method is taken from P3.
!                         The result is load onto the display and a hardcopy
!                         is generated in file {p3}_aver.ps
!                 TESTB2: From the combined bias frame row and columns are 
!                         averaged and plotted. The result plot are stored
!                         in the files {p3}_avcol.ps and {p2}_avrow.ps.
!                 TESTB3: Find the hot pixels.
!                         The combined bias frame is median filtered and 
!                         subtracted from the original. A plot is generated 
!                         showing the position of the hot pixels and the 
!                         affected columns (file {p3}_hotpix.ps). A MIDAS
!                         table ({p3}_hotpix.tbl).
!                 TESTB4: Inspection of single frame: the first and the last 
!                         one in the catalogue. For the first one a histogram
!                         of the pixel values is made, ignoring the hot pixels
!                         The plot file can be found in {p3}_hist.ps. For the
!                         last frame in the catalogue is first corrected for 
!                         hot pixels and then rebin 
!                 TESTB5: Determination of the bias level and standard 
!                         deviation. For each input frame in the catalogue 
!                         determine the  mean bias and standard deviation 
!                         after hot pixel correction, box averaging and 
!                         median filtering. The overall mean bias level and 
!                         mean sigma is computed."
!.VERSION:        940620 RHW creation
!.VERSION:        950115 RHW Inclusion of the subtest 
!.VERSION:        950808 RHW Split into single procedures 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAMETER P1  ?         C  "Enter input catalogue:"
DEFINE/PARAMETER P2  BIAS      C  "Enter combined output frame:"
DEFINE/PARAMETER P3  median    C  "Combining method:"
DEFINE/PARAMETER P4  <,>       C  "Enter row numbers for averaging:" 
DEFINE/PARAMETER P5  <,>       C  "Enter column numbers for averaging:" 
DEFINE/PARAMETER P6  [<,<:>,>] C  "Enter area for filtering :"
DEFINE/PARAMETER P7  5,10      N  "Radius median filtering, and dec. factor:"
!
WRITE/KEYW   INPUTI/I/1/2     {P7}
WRITE/KEYW   RADIUS/I/1/1     {INPUTI(1)}
DEFINE/LOCAL NBOX/I/1/1       {INPUTI(2)}
DEFINE/LOCAL IDOT/I/1/1        0
DEFINE/LOCAL catal/i/1/1       0
DEFINE/LOCAL nfrm/i/1/1        0
!
! *** check if the catalogue exists
IF M$EXIST(P1) .EQ. 0 THEN
   WRITE/OUT "*** FATAL <CCDTESTB>: Catalogue {P1} not existing"
   RETURN
ELSE
   read/icat {p1}
   write/out " "
ENDIF
!
! *** run through the catalogue
LOOP1:
   store/frame in_a {p1} 1 CONT1
   nfrm = nfrm + 1
   if nfrm .eq. 1 then
       define/local frm1/c/1/60 {in_a}
       idot = M$INDEX(frm1,".")-1
   endif
   goto LOOP1
CONT1:
!
@s ccdtestb1 {p1} {p2} {p3} ?            ! combine bias frames into master bias
@s ccdtestb2 {p2} {p2} {p4} {p5}         ! plot row and column intensities
@s ccdtestb3 {p2} {p2} {p6} {radius} ?   ! find hot pixels
@s ccdtestb4 {frm1(1:{IDOT})} {p2} {p6} {p7} ! inspection of single frame
@s ccdtestb5 {p1} {p2} {p6} {p7}         ! get mean and st. dev values 




