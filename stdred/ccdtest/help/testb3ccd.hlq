% @(#)testb3ccd.hlq	19.1 (ESO-SDAG) 02/25/03 14:16:56
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1994 European Southern Observatory
%.IDENT      testb3ccd.hlq
%.AUTHOR     RHW, SDAG/ESO
%.KEYWORDS   MIDAS, ccd package, bias, testing
%.PURPOSE    On-line help file for the command: TESTB3/CCD
%.VERSION    940620 RHW created
%.VERSION    950210 RHW txt output eleiminated; printed info reduced
%---------------------------------------------------------------------
\se
SECTION./CCD
\es\co
TESTB3/CCD                                                      20-Jun-1994 RHW
\oc\su
TESTB3/CCD in_frm [out_id] [area] [size] [option]
	Find the hot pixels in a (combined) bias frame
\us\pu
Purpose:    
	Find the hot pixels in a (combined) bias frame
\up\sy
Syntax:     
	TESTB3/CCD in_frm [out_id] [area] [size] [option]
\ys\pa
        in_frm =  input frame; no default
\ap\pa 
        out_id =  output identifier for the output of the test prodedure. 
                  All output (i.e. frames, listings and plots) will start 
                  with this identifier. Default is BIAS.
\ap\pa
        area =    area which the hot pixels will be determined. Reducing the
                  area will increase the speed of the execution. Default is 
                  whole image area.
\ap\pa
        size =    size of smoothing box used for median filtering used for
                  obtaining the hot pixel list. Default is 5.
\ap\no
Note:
        This command is the third test in a series of tests on bias frames:
        B1 to B5. See the command TESTBA/CCD.
\\
        The command will try to find the hot pixels. The combined bias frame 
        is median filtered (using the parameter `size') and subtracted 
        from the original. A plot is generated showing the position of the hot 
        pixels and the affected columns. Hot pixels will only be searched for 
        within the requested area and above the intensity level of 
        (mean + 0.25*sigma + 5.),  where mean is the mean intensity level, 
        sigma is the standard deviation. 
\\
        To avoid unnecessary computations the command checks for the presence 
        of the median filtered hot pixel frame and does not recompute this 
        frames if is already present.
\\
        For more details about the various combining methods, please refer 
        to the CCD Chapter in Volume B of the ESO-MIDAS Users' Guide.
\on\op
Output:
        The command will stored the outputs in a number of ascii and 
        postscript files (preceding by the identification string out_id):\\
           `out_id'_hotpix.tbl: MIDAS table containing the hot pixels;\\
           `out_id'_hotpix.ps:  postscript file with a plot of the hot pixels\\
\po\see
See also:   
        TESTBA/CCD, SET/CCD, SHOW/CCD, COMBINE/CCD, AVERAGE/ROW, 
        AVERAGE/COLUMN, FILTER/MEDIAN, Ch. 3 in Vol. 2 of the Users Guide.
\ees\exs
Examples:
\ex
        TESTB3/CCD BIAS BS [@900,@900:@1100,@1100]
        Run the test procedure on the frame BIAS. All output will start with 
        the identifier BS. Use the default for median filter box.
\xe \sxe



