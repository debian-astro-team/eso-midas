#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>

#define is_close_abs(a, expected, abs) \
    assert(fabs((a) - (expected)) <= (abs))

#define is_close_rel(a, expected, rel) \
    do { \
        assert(isfinite(a)); \
        if (((fabs((a) - (expected))) / fabs(expected)) > (rel)) { \
            printf("%g - %g = (%g %g%%)\n", a, expected, fabs(a - expected), \
                   100*fabs((a - expected) / expected)); \
            assert(((fabs((a) - (expected))) / fabs(expected)) <= (rel)); \
        } \
    } \
    while (0)

void gauss_fit(float *y, float*sig, int ndata,
               float *param, int npar, int *istat);


void test_fit_gauss(void)
{
    int i;
    float y[] = {0.,
        4.51912784,  4.4803483 ,  4.49815891,  4.50391175,  4.44209424,
        4.50642107,  4.50610642,  4.39039021,  4.60703791,  4.67056789,
        4.557359  ,  4.64554183,  4.69778552,  4.78062677,  5.00735675,
        5.17941118,  5.36604897,  5.64010167,  5.8546386 ,  6.13909468,
        6.40385729,  6.766126  ,  6.96161185,  7.26617407,  7.35062334,
        7.40077334,  7.49810857,  7.36797348,  7.16667408,  7.06196476,
        6.89422399,  6.5337182 ,  6.31071451,  5.96784071,  5.61276602,
        5.39849545,  5.22423652,  5.06970012,  4.91781292,  4.79335798,
        4.75865657,  4.58709514,  4.61116167,  4.41828574,  4.56031361,
        4.41063057,  4.60790814,  4.43245423,  4.47966526,  4.52575265};

    int ndata = sizeof(y) / sizeof(y[0]) - 1;
    float sig[ndata];
    float a[] = {0., 3., 26.,  7.0710, 4.5};

    double rel = 0.001;
    double exp[] = {0.,
        2.97971035,  26.66978935,   8.778967,   4.480772};
    int istat;

    for (i = 0; i < ndata + 1; i++) {
        sig[i] = 0.05;
    }
    y[36] = 5.;
    sig[36]= 1.;

    gauss_fit(y, sig, ndata, a, 4, &istat);

    is_close_rel(a[1], exp[1], rel);
    is_close_rel(a[2], exp[2], rel);
    is_close_rel(a[3], exp[3], rel);
    is_close_rel(a[4], exp[4], rel);
}

int main(int argc, const char *argv[])
{
    test_fit_gauss();
    return 0;
}
