! @(#)necupdate.prg	19.1 (ES0-DMD) 02/25/03 14:19:44
! @(#)necupdate.prg	19.1  (ESO)  02/25/03  14:19:44
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       necupdate.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command UPDATE/ECHELLE
!.VERSION     1.0    Creation    24-JAN-1994  PB
!
!-------------------------------------------------------
!
define/param  P1   ?       I     "Input file:"

define/local start/D/1/2 0.,0.
define/local end/D/1/2 0.,0.
define/local step/D/1/2  0.,0.
define/local npix/I/1/2  0,0
define/local tmp/D/1/1   0.

copy/dk {P1} start/D/1/2 start
copy/dk {p1} step/D/1/2  step
copy/dk {p1} npix/I/1/2  npix


end(1) = start(1) + (npix(1)-1)*step(1)
if step(1) .lt. 0. then
   tmp = start(1)
   start(1) = end(1)
   end(1) = tmp
endif
end(2) = start(2) + (npix(2)-1)*step(2)
if step(2) .lt. 0. then
   tmp = start(2)
   start(2) = end(2)
   end(2) = tmp
endif

imsize(1) = npix(1)
imsize(2) = npix(2)
if scan(1) .lt. 1       scan(1) = 1
if scan(2) .gt. npix(2) scan(2) = npix(2)

define/local EX/I/1/1 0

EX =  M$EXIST(ORDTAB) 

IF EX .EQ. 1 THEN
  select/table {ORDTAB} :X.GE.{start(1)}.AND.:X.LE.{end(1)}  {SESSOUTV}
  select/table {ordtab} SELECT.AND.:YFIT.GE.{start(2)}.AND.:YFIT.LE.{end(2)} {SESSOUTV}

  stat/table {ordtab} :ORDER {SESSOUTV}
  echord(4) = outputr(1)
  echord(5) = outputr(2)
  echord(1) = echord(5) - echord(4) + 1
  sel/tab {ordtab} all
ENDIF

return


