! @(#)necinit.prg	19.1 (ES0-DMD) 02/25/03 14:19:38
! @(#)necinit.prg	19.1  (ESO)  02/25/03  14:19:38
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       necinit.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Create and set keywords of context echelle.
!.VERSION     1.0    Creation    1991-JUL-24
!-------------------------------------------------------
!
register/session echel  STD_PROC necinit.prg nectab.tbl  ! Initialisation 
!
!Begin Session List
!
!
! Common Keywords: The type definition of the following keywords must be
! consistent with the other spectroscopy packages Mos and Echelle:
!
WRITE/KEYW AVDISP/R/1/1  0.     !Average dispersion (wavel. units per pixel)
WRITE/KEYW WCENTER/D/1/1 0.     !Central wavelength
WRITE/KEYW BETA/R/1/1    0.     !Non-Linearity to the dispersion relation
WRITE/KEYW   ALPHA/R/1/1   0.85 !Broadening factor [0,1]
WRITE/KEYW FILTMED/I/1/3 10,0,0 !Response median filtering (see FILTER/MED)
WRITE/KEYW FILTSMO/I/1/3 10,0,0 !Response Smoothing filtering (see FILTER/SMO)
WRITE/KEYW   FLAT/C/1/60  "+++"    ! Flat field (Order Definition: see ORDREF)
WRITE/KEYW   FLUXTAB/C/1/60  "unity"    ! Flux table associated to STD
WRITE/KEYW   GUESS/C/1/60    "+++"  ! Name of guess session (if WLCMTD=GUESS)
WRITE/KEYW   OBJ/C/1/60     "+++"   ! Object spectrum
WRITE/KEYW   RON/R/1/1       40.    ! Read-out noise of the CCD (e-)
WRITE/KEYW   GAIN/R/1/1      10.    ! Inverse gain factor of the CCD (e-/ADU
WRITE/KEYW   STD/C/1/60      "+++"  ! Name of standard star spectrum
WRITE/KEYW   TOL/R/1/1        0.2   ! Tolerance for single order calibration
WRITE/KEYW  WLC/C/1/80     "+++"    ! Wavelength calibration spectrum
WRITE/KEYW  LINCAT/C/1/80  "MID_ARC:thar.tbl" ! Line catalog for WLC
WRITE/KEYW  LINTAB/C/1/60  line.tbl    ! Table of line identifications
WRITE/KEYW   WLCMTD/C/1/10   "PAIR" ! Wavelength calibration method: 
!$ PAIR,ANGLE,GUESS,RESTART,ORDER
!$ (Associated command IDENT/ECHELLE)
WRITE/KEYW   WLCNITER/I/1/2   3,20  ! Minimum, Maximum number of iterations
!
! End of Common Keywords
!
WRITE/KEYW  ORDTAB/C/1/12  order.tbl   ! Table of order positions
WRITE/KEYW  BAKTAB/C/1/12  back.tbl    ! Table of background positions
!
WRITE/KEYW  ORDREF/C/1/60  "+++"    ! Reference image for order  definition
WRITE/KEYW  CORRECT/C/1/60 "ffcorr" ! Flat field corrected for background
WRITE/KEYW  BLAZE/C/1/60   "blaze"  ! Blaze function estimated from flat-field
WRITE/KEYW  INSTRUM/C/1/60   "+++"  ! Instrument name
WRITE/KEYW  GRATING/C/1/60 "+++"    ! Disperser name
WRITE/KEYW  CROSS/C/1/60   "+++"    ! Cross disperser name
WRITE/KEYW  CCD/C/1/60     "+++"    ! CCD Name
WRITE/KEYW  CCDBIN/R/1/2   1.,1.    ! X,Y binning factor (updated by ROTATE/ECH)
WRITE/KEYW  OBSTAB/C/1/60  "speclist.tbl" ! List of observations
!
!
WRITE/KEYW   FFOPT/C/1/3          "NO "       ! Optional flat-fielding
WRITE/KEYW   MGOPT/C/1/3          "YES"       ! Optional merge
!
! Numerical keywords. General section.
!
WRITE/KEYW   SCAN/I/1/2    0,0	! Scan limits in pixels (set by SCAN/ECHELLE)
WRITE/KEYW   ECHORD/I/1/6  0,0,0,0,0,0	! Nb of orders (1), 
!$ absolute number of 1st and last order (2-3),
!$ relative number of 1st and last order (4-5),
!$ absolute to relative conversion constant (6).
WRITE/KEYW   IMSIZE/I/1/2  0,0   ! Image size, in pixel.
WRITE/KEYW   WLRANGE/R/1/2 0,0   ! Wavelength range
!
! Rotation
! 
WRITE/KEYW  ROTMTD/C/1/12 "DEF"  ! Rotation method (DEF, CASPEC, EMMI, EFOSC, ECHELEC)
WRITE/KEYW  FLIPAXIS/C/1/4  "X"  ! Flip Axis for rotate/echelle (None, X, Y, XY)
WRITE/KEYW  ROTANGLE/R/1/1   0.  ! Rotation angle for rotate/eche (0, 90, 180, 270)
WRITE/KEYW  DEFTIME/R/1/1   1.   ! Default exposure time (sec) 
!
! Orders definition.
!
WRITE/KEYW  DEFMTD/C/1/12   "HOUGH"   ! Order definition methods: COM,STD,HOUGH
!$ (COM,STD call DEFINE/ECH; HOUGH calls DEFINE/HOUGH)
! 
WRITE/KEYW   DEFPOL/I/1/2    4,5   ! Degrees of bivariate plynomial
! 
! Methods COM or STD. Command DEFINE/TRACE
! 
WRITE/KEYW   THRES1/R/1/1    1000. ! Threshold for order following
WRITE/KEYW   WIDTH1/I/1/1    8     ! Order width
WRITE/KEYW   SLOPE/R/1/1     0.05  ! Mean slope of orders
! 
! Method AUTO. Command DEFINE/ECHELLE
WRITE/KEYW   NBORDI/I/1/1  0  ! Number of orders  (automatic if null)
WRITE/KEYW   WIDTHI/I/1/1  0  ! Order width       (automatic if null)
WRITE/KEYW   THRESI/R/1/1  0. ! Thresh. for order following (automatic if null)
! 
! Method ZEBRA. 
WRITE/KEYW   ZEBCOL/R/1/1   0.5  ! Position of starting column (fraction of the number of columns)
WRITE/KEYW   ZEBWIND/R/1/1  0.1 ! Width of the analysis window (fraction of the number of columns)
WRITE/KEYW   ZEBSLOPE/R/1/1 0.  ! Average slope of orders (updated by zebpar)
WRITE/KEYW   ZEBWIDTH/R/1/1 0.  ! Average width of orders (updated by zebpar)
WRITE/KEYW   ZEBSTEP/I/1/1  20  ! Step (pixels) between cross-sections
WRITE/KEYW   ZEBVISU/C/1/3  NO  ! Visualization of intermediate results
!
! Filtering
!
WRITE/KEYW CRFILT/I/1/3   15,11,3  !Cosmic rays filtering: widx, widy, no_iter
WRITE/KEYW MEDFILT/R/1/3  1,1,10   !Median filtering     : radx, rady, mthresh
WRITE/KEYW CCDFILT/R/1/3  40.,10.,4. !CCD parameters       : ron, g, ethresh
!
! Background.
!
WRITE/KEYW  BKGMTD/C/1/12   "SPLINE" ! Background method: POLY,SPLINE,SMOOTH 
!$ (POLY,SPLINE for BACKGR/ECH; SMOOTH for BACKGR/SMOOTH)
WRITE/KEYW  BKGVISU/C/1/3   NO    !Backgr. visu (YES/NO) for LOAD/ECH,SUB/BACK
WRITE/KEYW  BKGSTEP/I/1/1   10    !Step along X.     (Meth. SPLINE and POLY)
WRITE/KEYW  BKGRAD/I/1/2    6,2   !Radius in X and Y (Meth. SPLINE and SMOOTH)
!$ Method SPLINE. Command BACK/ECHELLE
WRITE/KEYW  BKGDEG/I/1/1    3      !Degree of spline polynomials (Meth. SPLINE)
WRITE/KEYW  BKGSMO/R/1/1    1000.  !Smoothing factor             (Meth. SPLINE)
!$ Method POLY. Command BACK/ECHELLE
WRITE/KEYW  BKGPOL/I/1/2    4,5    !Degree in X and Y of polynomial (Meth. POLY)
!$ Method SMOOTH. Command BACK/SMOOTH
WRITE/KEYW   BKGNIT/I/1/1    10     !Number of iterations        (Meth. SMOOTH)
!
! Extraction
!
WRITE/KEYW EXTMTD/C/1/12 "AVERAGE" !Extraction methods: AVERAGE, LINEAR, OPTIMAL
!$ (AVERAGE,LINEAR for EXTRACT/ECH; OPTIMAL for EXTRACT/OPTIMAL)
WRITE/KEYW   SLIT/R/1/1      0.     ! Slit height (pixel)
WRITE/KEYW   OFFSET/R/1/1    0.     ! Offset to the order reference (pixel)
!$ Positive if the spectrum is above 
!$ the trace displayed by LOAD/ECHELLE
WRITE/KEYW   NSKY/I/1/1     2           ! Number of sky windows  (1 or 2) 
WRITE/KEYW   SKYWIND/R/1/4  0.,0.,0.,0. ! Offset limits of sky windows
WRITE/KEYW   POSSKY/C/1/12  AUTO        ! Sky feature positioning mode.
!$ AUTO:   Plots the central order at the central column
!$ CURSOR: Position defined with the cursor in display window
!$ order,column: Relative order number and column pixel number
WRITE/KEYW   EXTSIGMA/R/1/1  3.     ! Threshold  (in units of the 
!$ theoretical noise sigma of each pixel).
! 
!
! Search
!
WRITE/KEYW   SEAMTD/C/1/8  GAUSSIAN  ! Line search method.
!$ GAUSSIAN  (recommended) or GRAVITY 
!$ (Associated command SEARCH/ECHELLE).
WRITE/KEYW   WIDTH2/R/1/1    10 ! Width of lines along X axis
WRITE/KEYW   THRES2/R/1/1    0. ! Relative threshold above background.
!$ The threshold is determined automatically if 
!$ its value is less or equal zero
!
! Wavelength calibration.
!
WRITE/KEYW   WLCOPT/C/1/60   "1D"    ! Dispersion relation type (1D, 2D)
WRITE/KEYW   WLCREG/C/1/12 "STANDARD" ! Regression mode (Standard, Robust)
WRITE/KEYW   DC/I/1/1          4     ! Degree for global identification [1,5]
!$ If TOL=0., the tolerance is auto-adaptive and  set 
!$ to 3 times the standard deviation of the residuals.
WRITE/KEYW   WLCITER/R/1/5  1,0.3,20,5,4. ! Initial tolerance (pixel), Next neighbour, Error maxi (pixel),
!$ Initial error (pixel), Iterative error (nb of std deviations)
!$ Next neighbour parameter must be strictly positive. More
!$ secure identifications are obtained with values below 1.0 
WRITE/KEYW   WLCVISU/C/1/3    YES    ! Visualization of identifications (YES/NO)
!
! On-Line Wavelength Calibration
!
WRITE/KEYW RELORD/I/1/1  5 !Relative order number used to center disp. relation
WRITE/KEYW ABSORD/I/1/1 0      !Absolute order number corresponding to RELORD
!
! Rebinning.
!
WRITE/KEYW REBMTD/C/1/2 NONLINEAR !Rebin method: NONLINEAR (for REBIN/ECHELLE).
!$ LINEAR mode non supported
WRITE/KEYW   SAMPLE/C/1/60   +++      ! Step in wavelength
!
! Instrumental response correction
!
WRITE/KEYW   RESPOPT/C/1/3        "YES"       ! Optional Response Correction
WRITE/KEYW   RESPMTD/C/1/12  "STD"   !Instrum. response corr. methods: STD, IUE
!$ STD for RESPONSE/ECH, IUE for RIPPGE/ECH
!
! Method: STD    Correction by a standard star
!
WRITE/KEYW RESPONSE/C/1/60 "response" ! Name of output response function
WRITE/KEYW PIXNUL/I/1/2    10,10  !No. of pixels zeroed on left and right edges
!
! Method: IUE    IUE-like correction of instrumental response
!
WRITE/KEYW   RIPMTD/C/1/12  "SINC"   ! Ripple method (if RESPMTD=IUE): SINC, OVER, FIT
WRITE/KEYW   RIPFRZ/C/1/3      NO      ! Freeze values K and Alpha [YES/NO]
WRITE/KEYW   RIPK/R/1/1         0.     ! Grating constant
WRITE/KEYW   LAMBDA1/R/1/1     1.00    ! Delta   in  wavelength unit
WRITE/KEYW   LAMBDA2/R/1/1     20.0    ! Overlap in  wavelength unit
!
! Method: MEDIAN   Median estimate of response function
!
WRITE/KEYW   RIPRAD/I/1/2     1,3 ! X radius for median and smoothing.
WRITE/KEYW   RIPORD/I/1/1      1  ! Order currently processed
WRITE/KEYW   RIPLIM/I/1/2     0,0 ! Order limits in pixels (0,0: all)
WRITE/KEYW   RIPBIN/R/1/1     0.03  ! Bin of histogram for normalization
!
! Orders merging.
!
WRITE/KEYW MRGMTD/C/1/12   AVERAGE ! Merging methods: NOAPPEND,AVERAGE
!$ (Associated command MERGE/ECHELLE)
WRITE/KEYW DELTA/R/1/1     3.0     !Wav. interval to be skipped (MRGMTD=AVERAGE)
WRITE/KEYW MRGORD/I/1/2    0,0     ! Ord1,Ord2 (MRGMTD=NOAPP). 0,0:all orders
!
!Batch Reduction (Specific keywords)
!
WRITE/KEYW ROTOPT/C/1/3  "NO "  ! Rotation/Flip option (Yes/No)
WRITE/KEYW ROTAXIS/C/1/3 "FLIP" ! Mode of rotation or flip     
WRITE/KEYW CROPT/C/1/3   "NO "  ! Cosmic rays filtering option (Yes/No)
WRITE/KEYW BKGOPT/C/1/3  "YES"  ! Interorder Background subtraction option
WRITE/KEYW SKYOPT/C/1/3  "NO "  ! Sky extraction option (Yes/No)
WRITE/KEYW EXTOPT/C/1/3  "YES"  ! Spectrum extraction option (Yes/No)
WRITE/KEYW REBOPT/C/1/3  "YES"  ! Rebin option (Yes/No)
!
!End Session List
!
IF P1(1:1) .EQ.  "?"    RETURN
!
! Copying tables
VERIFY/ECHELLE {P1}ORDE.tbl  TABLE
!
DEFINE/LOCAL OLDTAB/I/1/1   0
!
IF M$EXISTD("{P1}ORDE.tbl","ECHC") .EQ. 1  THEN    !old descr. ECHC is present
IF M$EXISTD("{P1}ORDE.tbl","ECHORD") .EQ. 0  THEN  !new descr. ECHORD is not there
!
OLDTAB = 1
!
WRITE/OUT " *** Warning  ****"
WRITE/OUT "Table {P1}ORDE.tbl contains old format descriptors"
WRITE/OUT "Consider to update it by the command SAVE/ECHELLE {P1}"
WRITE/OUT "Reading old descriptors..."
!
DEFINE/LOCAL ECHR/R/1/20  0.   ALL
DEFINE/LOCAL ECHI/I/1/20  0    ALL
DEFINE/LOCAL ECHC/C/1/110 " "  ALL
!
COPY/DK  {P1}ORDE.tbl  ECHR/R/1/20   ECHR
COPY/DK  {P1}ORDE.tbl  ECHI/I/1/20   ECHI
COPY/DK  {P1}ORDE.tbl  ECHC/C/1/110  ECHC
!
width1 = echr(1)
thres1 = echr(2)
thres2 = echr(3)
SLOPE  = echr(4)
WIDTH2 = echr(5) 
!TOL    = echr(6)   ! Must be at the default value
! RESPOL = echr(8)    RESPOL is Not used anymore
SLIT   = echr(9)
OFFSET = echr(10)
LAMBDA1 = echr(12)
LAMBDA2 = echr(13)
RIPK   = echr(14)
ALPHA  = echr(15)
DELTA  = echr(16)
BKGSMO = echr(19)
!
DEFPOL(1) = echi(1)
DEFPOL(2) = echi(2)
DC        = echi(3)
ECHORD(1) = echi(4)
ECHORD(2) = echi(6)
ECHORD(3) = echi(5)
IMSIZE(1) = echi(7)
IMSIZE(2) = echi(8)
SCAN(1)   = 1
SCAN(2)   = echi(8)
BKGPOL(1) = echi(9)
BKGPOL(2) = echi(10)
!
SET/ECH ORDREF={ECHC(1:8)} FLAT={ECHC(1:8)} WLC={ECHC(9:16)} 
SET/ECH DEFMTD={ECHC(17:20)} BLAZE={ECHC(21:28)}  GRATING={ECHC(37:40)}
SET/ECH STD={ECHC(41:48)} RESPONSE={ECHC(49:56)} CORRECT={ECHC(57:64)}
SET/ECH FLUXTAB={ECHC(65:72)} SAMPLE={ECHC(73:80)} LINCAT={ECHC(81:88)}
SET/ECH RESPMTD={ECHC(97:98)} RIPMTD={ECHC(99:100)} EXTMTD={ECHC(101:102)}
SET/ECH INSTR={ECHC(103:110)}
!
ENDIF
ENDIF
!
IF OLDTAB .EQ. 0 THEN 
   SAVINIT/ECHELLE {P1}ORDE.tbl  READ
ENDIF
!
! Copying tables
!
-COPY    {P1}ORDE.tbl   {ORDTAB}
! 
IF M$EXIST("{P1}LINE.tbl") .NE. 0 THEN
-COPY   {P1}LINE.tbl  {LINTAB}
IF OLDTAB .EQ. 1  COMPUTE/TABLE {LINTAB}  :SELECT = 1.
ENDIF
! 
IF M$EXIST("{P1}back.tbl") .NE. 0 THEN
-COPY  {P1}back.tbl  {BAKTAB}
ELSE
   WRITE/OUT "Background table {BAKTAB} not stored. Created..."
   PREPARE/BACKGROUND  ?  INI
ENDIF
