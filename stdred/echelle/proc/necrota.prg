! @(#)necrota.prg	19.1 (ES0-DMD) 02/25/03 14:19:42
! -------------------------------------------------------------------
!
! necrota.prg: rotates and flips ECHELLE format spectra
!
! ROTATE/ECHELLE inputcatal.cat name-root [mode]
! inputcatal.cat       input catalogue
! name-root            root for file names (<5)
! mode                 operation mode (ROTATE - CASPEC old CCD, before 1985)
!                                     (FLIP   - CASPEC new CCD, default)
!
! Author : Pascal Ballester
! Date   : 10-11-1994
! -------------------------------------------------------------------
!
DEFINE/PARAM P1 ?    CHAR  "Enter input catalogue:"
DEFINE/PARAM P2 rot  CHAR  "Root"
DEFINE/PARAM P3 {ROTMTD}    CHAR  "Method (DEF, ROT, FLIP, CASPEC, EMMI, EFOSC, ECHELEC):"
DEFINE/PARAM P4 {FLIPAXIS}  CHAR  "Flip axis (NONE,X,Y,XY)"
DEFINE/PARAM P5 {ROTANGLE}  NUMB  "Rotation angle (degrees):"
DEFINE/PARAM P6 {DEFTIME}   NUMB  "Default exposure time:"
!
DEFINE/LOCAL CATAL/I/1/1  0
DEFINE/LOCAL INPUT/C/1/60 INDUMMY
DEFINE/LOCAL OUTPUT/C/1/60 "            "
DEFINE/LOCAL INCAT/C/1/60 "                    "
DEFINE/LOCAL NUMB/I/1/1   1
CLEAR/ICAT
!
define/local rtmtd/C/1/12 {P3}
define/local axis/C/1/4   {P4}
define/local angle/R/1/1  {P5}

if rtmtd(1:1) .EQ. "D" THEN 
      GOTO ROTATION
endif

if rtmtd(1:1) .eq. "R" then  ! ROTATE
   write/key axis  NONE
   angle = {P5}
   GOTO ROTATION
endif

if rtmtd(1:1) .eq. "F" then  ! FLIP
   write/key axis  {P4}
   angle = {P5}
   GOTO ROTATION
endif

if rtmtd(1:2) .eq. "EM" then
   write/key axis  X
   angle = 0
   GOTO ROTATION
endif

if rtmtd(1:2) .eq. "CA" then
   write/key axis  X
   angle = 0
   GOTO ROTATION
endif

if rtmtd(1:2) .eq. "EF" then
   write/key axis  X
   angle = 0
   GOTO ROTATION
endif

if rtmtd(1:2) .eq. "EC" then
   write/key axis  X
   angle = 0
   GOTO ROTATION
endif

write/out "Sorry. Unknown rotation mode: {RTMTD}"
write/out "Valid modes are: DEF, ROTATE, FLIP, EMMI, CASPEC, EFOSC, ECHELLEC"
return

ROTATION:
!
!  test, if really catalog name entered...
DEFINE/LOCAL II/I/1/2 0,0
II(1) = M$INDEX(P1,".")
IF II(1) .LE. 0 write/key P1 {p1}.cat
!
II(1) = M$INDEX(P1,".cat")
IF II(1) .LE. 0 THEN
!   WRITE/KEYW INCAT 'P1'.cat
    Write/Out "Rotate/Flip image: {P1}"
    WRITE/KEYW INPUT   {P1}
    WRITE/KEYW OUTPUT  {P2}
    CATAL = -1
    GOTO SINGLE
ELSE
   Write/Out "Rotate/Flip catalog: {P1}"
   WRITE/KEYW INCAT 'P1'
ENDIF
!
! Start the loop over the input frames in the catalogue
!
LOOP:
STORE/FRAME INPUT 'INCAT' 
!READ/KEYW INPUT
WRITE/KEYW  OUTPUT  {P2}{NUMB}
SINGLE:
NUMB = NUMB + 1
WRITE/OUT ***************************************************
WRITE/OUT Input frame: 'INPUT', Output frame: 'OUTPUT'
WRITE/OUT "IDENT: {{INPUT},IDENT}"
!
VERIFY/ECHELLE  {INPUT}  OTIME {P6}
!
! Rebin + Flip
!
!
IF M$ABS({angle}) .EQ. 0.  THEN
  COPY/II  'INPUT' 'OUTPUT'
ELSE
  REBIN/ROTATE 'INPUT' 'OUTPUT'  {angle}
ENDIF
!
IF AXIS(1:1) .NE. "N" THEN
  FLIP/IMAGE 'OUTPUT' {axis}
ENDIF

CCDBIN(1) = M$ABS({{OUTPUT},STEP(1)})
CCDBIN(2) = M$ABS({{OUTPUT},STEP(2)})

IF CATAL .GT. 0 GOTO LOOP
!
WRITE/OUT *** FINISHED ***








