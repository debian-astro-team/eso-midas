! @(#)necoffset.prg	19.1 (ES0-DMD) 02/25/03 14:19:40
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       necoffset.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, Offset
!.PURPOSE     Command OFFSET/ECHELLE
!.VERSION     1.0    Creation    16-MAR-1995  PB
!
!-------------------------------------------------------
!
DEFINE/PARAM  P1    ?        IMA    "Input image:"
DEFINE/PARAM  P2   20        NUMB   "Range of search (pixels):"
DEFINE/PARAM  P3   30        NUMB   "Coverage"
DEFINE/PARAM  P4   {ORDTAB}  TABL   "Order table name:"
DEFINE/PARAM  P8   VERB      CHAR   "Mode (Verbose/Silent)"

verify/echelle {P1}
inputi(3) = echord(4) ! Lower relative order number
inputi(4) = echord(5) ! Upper relative order number

INPUTI(9) = M$SECS()

write/key in_a {P1}
write/key in_b {P4}
write/key inputc/C/11/10 "COEFF     "
write/key inputi/I/1/2   {P2},{P3}

RUN STD_EXE:necoffset

IF P8(1:1) .EQ. "V" THEN
   OFFSET = OUTPUTR(1)
   write/out "Set session keyword: OFFSET = {OFFSET}"
ENDIF

RETURN



