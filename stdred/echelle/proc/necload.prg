! @(#)necload.prg	19.1 (ES0-DMD) 02/25/03 14:19:39
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       necload.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command LOAD/ECHELLE
!.VERSION     1.0    Creation    
!
!-------------------------------------------------------
!
DEFINE/PARAM P1  ECHELLE  C   "Mode (ECHELLE/IDENT)        :"
DEFINE/PARAM P2  FIT      C   "Load/Echelle mode (RAW/FIT) :"
DEFINE/PARAM P8  *  IMA   "Name of displayed image:"

DEFINE/LOCAL DISPIMA/C/1/60  {P8}
IF DISPIMA(1:1) .EQ. " " THEN
   WRITE/OUT "LOAD/ECHELLE : Requires a display and a loaded image"
   RETURN/EXIT
ENDIF

CLEAR/CHAN OVER

IF P1(1:1) .EQ. "E" THEN   ! Mode ECHELLE
   SELECT/TABLE {ORDTAB} ALL
   IF P2(1:1) .EQ. "F" THEN  ! Display fitted positions and background
      LOAD/TABLE   {ORDTAB} :X :YFIT :ORDER 0 0 4 >null
      IF BKGVISU(1:1) .EQ. "Y" THEN
         LOAD/TABLE {BAKTAB} :X :YBKG   >null
      ENDIF
   ELSE                      ! Display raw order positions
      LOAD/TABLE   {ORDTAB} :X :Y :ORDER 0 0 4  >null
!      LOAD/TABLE   {ORDTAB} :X :Y            >null
   ENDIF
ENDIF

IF P1(1:1) .EQ. "I" THEN   ! Mode IDENT
   IF P2(1:1) .NE. "R" THEN
      SELECT/TABLE {LINTAB}  (:SELECT.GE.1.0)       {SESSOUTV}
      LOAD/TABLE   {LINTAB}  :X :YNEW {SESSOUTV}
      READ/TABLE   {LINTAB}  :X :YNEW :ORDER :IDENT {SESSOUTN}
   ELSE
      SELECT/TABLE {LINTAB} ALL
      LOAD/TABLE   {LINTAB} :X  :YNEW {SESSOUTV}
   ENDIF
   SELECT/TABLE {LINTAB}  ALL
ENDIF

RETURN
