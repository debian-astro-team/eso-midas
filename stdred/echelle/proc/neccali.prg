! @(#)neccali.prg	19.1 (ES0-DMD) 02/25/03 14:19:35
! @(#)neccali.prg	19.1  (ESO)  02/25/03  14:19:35
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echcali.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command CALIBRATE/ECHELLE
!.VERSION     1.0    Creation    23-AUG-1991 PB
!
!-------------------------------------------------------
!
DEFINE/PARAM  P1   {DEFMTD}   C    "Order definition method:"
DEFINE/PARAM  P2   {WLCMTD}   C    "Wavelength calibration method:"
DEFINE/PARAM  P3   BOTH       C    "Mode (ORDER, WLC, BOTH):"

DEFINE/LOCAL  NBLINE/I/1/1  0
DEFINE/LOCAL  NBEXP/I/1/1   0
DEFINE/LOCAL  ANSW/C/1/3    HAY

! Order definition

IF P1(1:1) .EQ. "D" .OR. P3(1:1) .EQ. "W" THEN
   WRITE/OUT "--- Info:  Skipped order definition"
   GOTO WAVE
ENDIF

WRITE/OUT "--------------------------------------------------------"
WRITE/OUT "  ORDER REFERENCE FRAME :   ORDREF = {ORDREF}"
VERIFY/ECHELLE {ORDREF}

DEFINE/LOCAL   ORDEF/I/1/1   0
SET/ECHELLE DEFMTD =  {P1}

IF DEFMTD(1:1) .EQ. "H" THEN      !  Method HOUGH
  ORDEF = 1
  DEFINE/HOUGH
ENDIF
IF DEFMTD(1:1) .EQ. "S" THEN      ! Method STD
  ORDEF = 1
  DEFINE/ECHELLE
ENDIF
IF DEFMTD(1:1) .EQ. "C" THEN      ! Method COM
  ORDEF = 1
  DEFINE/ECHELLE
ENDIF

IF ORDEF .EQ. 0 THEN
      WRITE/OUT "CALIBRATE/ECHELLE: unknown method DEFMTD = {DEFMTD}"
      HELP/ECH  DEFMTD
      RETURN/EXIT
ENDIF

IF P3(1:1) .EQ. "O" GOTO END

! Lines extraction and searching
WAVE:

WRITE/OUT "--------------------------------------------------------"
WRITE/OUT "  WAVELENGTH CALIBRATION FRAME :  WLC = {WLC}"
VERIFY/ECHELLE {WLC}
@ creifnot 2 heat
LOAD {WLC}

SET/ECHELLE WLCMTD =  {P2}

IF WLCMTD(1:1) .NE. "O"    THEN  ! Not performed for WLCMTD=ORDER
  EXTRACT/ECHELLE  {WLC}  &a
SEARCH:
  SEARCH/ECHELLE   &a
  NBLINE = {{LINTAB},TBLCONTR(4)}  ! Detected number of lines
  NBEXP  = 10*ECHORD(1)            ! Expected number of lines
  IF NBLINE .LT. NBEXP  THEN

     WRITE/OUT "There may be not enough lines to perform the wavelength"
     WRITE/OUT "calibration. Current threshold value is {THRES2} and"
     WRITE/OUT "width is {WIDTH2}"
     INQUIRE/KEYW ANSW/C/1/3 "Do you want to use other values (y/n,def=y) ? "
     IF AUX_MODE(7) .NE. 0 THEN
        IF ANSW(1:1) .EQ. "N" GOTO CALIB
     ENDIF
     INQUIRE/KEYW  WIDTH2/R/1/1  "Width (pixels) : "
     INQUIRE/KEYW  THRES2/R/1/1  "Threshold above the background: "
     GOTO SEARCH

  ENDIF

ENDIF

! Wavelength calibration
CALIB:
IDENT/ECHELLE

END:
RETURN


