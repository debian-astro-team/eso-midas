! @(#)necexor.prg	19.1 (ES0-DMD) 02/25/03 14:19:37
! @(#)necexor.prg	19.1  (ESO)  02/25/03  14:19:37
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echexor.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command EXTR/ORD in out slit,angle,offset meth 
!                                             table coeffs [ord1,ord2]
!             meth is B for bilinear interpolation
!                     N for nearest pixel
!.VERSION     1.0    Creation   J.D. Ponz  131083 
!-------------------------------------------------------
!
DEFINE/PARAM P1 ? IMAGE    "Enter input image:"
DEFINE/PARAM P2 ? IMAGE    "Enter output extracted orders:"
DEFINE/PARAM P3 ? NUMBER   "Enter extraction pars (SLIT,ANGLE,OFFSET):"
DEFINE/PARAM P4 B ?        "Enter method:"
DEFINE/PARAM P5 ? TABLE    "Enter auxiliary table:"
DEFINE/PARAM P6 ? ?        "Enter coeffs with order position:"
DEFINE/PARAM P7 0,0 NUMBER
!
! Case unsensitivity and check method
DEFINE/LOCAL   METHOD/C/1/7   "Y"
IF P4(1:1) .EQ. "A"     WRITE/KEYW   METHOD/C/1/7    "AVERAGE"
IF P4(1:1) .EQ. "L"     WRITE/KEYW   METHOD/C/1/7    "LINEAR "
IF P4(1:1) .EQ. "M"     WRITE/KEYW   METHOD/C/1/7    "MEDIAN "
IF P4(1:1) .EQ. "W"     WRITE/KEYW   METHOD/C/1/7    "WEIGHT "
IF METHOD(1:1) .EQ. "Y" THEN
   WRITE/OUT "EXTRACT/ORDER: unknown method: {P4}"
   WRITE/OUT "Possible methods are AVE, LIN, MED, WEI"
   RETURN/EXIT
ENDIF
!
WRITE/KEYW IN_A/C/1/60    {P1}
WRITE/KEYW OUT_A/C/1/60   {P2}
WRITE/KEYW INPUTR/R/1/3   {P3}
WRITE/KEYW IN_C/C/1/7     {METHOD}
WRITE/KEYW IN_B/C/1/60    {P5}
WRITE/KEYW INPUTC/C/1/60  {P6}
WRITE/KEYW INPUTI/I/1/2   {P7}
!
RUN STD_EXE:NECEXOR
