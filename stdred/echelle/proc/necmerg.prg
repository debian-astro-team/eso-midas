! @(#)necmerg.prg	19.1 (ES0-DMD) 02/25/03 14:19:39
! +++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  ECHELLE PROCEDURE : ECHMERG.PRG
!  J.D.Ponz			version 1.0 131083
!  S.Wolf	optimal merging implemented 131099
!
! .PURPOSE
!
!  execute the command :
!  MERGE/ECHELLE input output [delta]     AVERAGE
!  OR
!  MERGE/ECHELLE input output [delta]     OPTIMAL weights [variance]
!  OR
!  MERGE/ECHELLE input output [ord1,ord2] NOAPPEND
!
! ----------------------------------------
!
DEFINE/PARAM P1 ?         IMAGE         "Enter input image:"
DEFINE/PARAM P2 ?         IMAGE         "Enter output image:"
DEFINE/PARAM P4 {MRGMTD}    C           "Method"
IF P4(1:1) .EQ. "N"  DEFINE/PARAM P3 {MRGORD(1)},{MRGORD(2)} NUMBER
IF P4(1:1) .EQ. "A"  DEFINE/PARAM P3 {DELTA}   NUMBER   ! Overlapping param.
IF P4(1:1) .EQ. "O"  THEN
  DEFINE/PARAM P3 {DELTA}   NUMBER   ! Overlapping param.
  DEFINE/PARAM P5 ?   IMAGE "Enter weight image:"
  DEFINE/PARAM P6 middummv.bdf   IMAGE "Enter variance image (output):"
ENDIF
!
VERIFY/ECHELLE {P1}  REBIN
!
DEFINE/LOCAL   METHOD/C/1/8  "YYYYYYYY"
IF P4(1:1) .EQ. "N"  WRITE/KEYW  METHOD/C/1/8 "NOAPPEND"
IF P4(1:1) .EQ. "A"  WRITE/KEYW  METHOD/C/1/8 "AVERAGE"
IF P4(1:1) .EQ. "O"  WRITE/KEYW  METHOD/C/1/8 "OPTIMAL"
IF P4(1:1) .EQ. "C"  WRITE/KEYW  METHOD/C/1/8 "CONCATEN"
IF P4(1:1) .EQ. "S"  WRITE/KEYW  METHOD/C/1/8 "SINC"
IF METHOD(1:1) .EQ. "Y" THEN
   WRITE/OUT "MERGE/ECHELLE: unknown method {P4}"
   RETURN/EXIT
ENDIF
WRITE/KEYW  MRGMTD/C/1/12  {METHOD}
IF MRGMTD(1:1) .EQ. "A"   DELTA = {P3}
IF MRGMTD(1:1) .EQ. "N"   WRITE/KEYW   MRGORD/I/1/2  {P3}
!
IF P3(1:1) .NE. "?" THEN
   WRITE/KEYW INPUTR/R/1/2   {P3}
ELSE
   WRITE/KEYW INPUTR/R/1/2   0.  ALL
ENDIF
!
RUN STD_EXE:NECMERGE
!
RETURN
