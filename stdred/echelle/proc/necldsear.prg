! @(#)necldsear.prg	19.1 (ES0-DMD) 02/25/03 14:19:39
! @(#)necldsear.prg	19.1  (ESO)  02/25/03  14:19:39
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       echldsear.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command LOAD/SEARCH
!.VERSION     1.0    Creation    P. Ballester 19-APR-1994
!
!-------------------------------------------------------
!
DEFINE/PARAM P1 {WLC}    IMA "WLC Image:"
DEFINE/PARAM P2 {LINTAB} TAB "Line table:"
DEFINE/PARAM P8  *       IMA "Displayed image:"
!
VERIFY/ECHELLE {WLC}    IMA
VERIFY/ECHELLE {LINTAB} TAB
!
@ creifnot 2 heat
CLEAR/CHAN OVER
!
IF P8(1:1) .EQ. " " LOAD/IMAGE {WLC}
LOAD/TABLE {LINTAB} :X :YNEW 
!
RETURN
