! @(#)neczebdef.prg	19.1 (ES0-DMD) 02/25/03 14:19:44
! @(#)neczebdef.prg	19.1  (ESO)  02/25/03  14:19:44
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       zebdefi.prg
!.AUTHOR      A.Yu.Kniazev SAO USSR
!.KEYWORDS    Spectroscopy, Echelle, Order detection, Zebra method
!.PURPOSE     - Automatic order detection 
!             - Order Following and creation of table order.tbl
!    execute the command :
!    DEFINE/ZEBRA  FRAME COLUMN HALF WIDTH INCL NSTEP PLOT DEGREE
!    where:
!    FRAME   - input image name
!    COLUMN  - the number of center column in input region
!    HALF    - the half of width in input region
!    WIDTH   - the average width of orders in input region
!    INCL    - the average slope of orders in input region
!    NSTEP   - the number of step
!    PLOT    - plot or not the results of work
!    DEGREE  - degrees of polynomes
!
!.VERSION     1.0    Creation    20.05.1992
!-------------------------------------------------------
!
DEFINE/PARAM   P1  {ORDREF}  CHAR   
DEFINE/PARAM   P4  YES       CHAR

SET/ECHE       ORDREF={P1}
VERIFY/ECHELLE {P1}
!
IF P1 .NE. "?"  SET/ECH ORDREF={P1}
IF P2 .NE. "?"  ZEBCOL = {P2}
IF P3 .NE. "?"  ZEBWIND = {P3}
!
DISPLAY/ECHELLE  {ORDREF} {ZEBVISU}
IF P4(1:1) .EQ. "Y" THEN
  ANALYZE/ZEBRA
ENDIF
!PARAMETER/ECHELLE
!
WRITE/KEYW  IN_A      {ORDREF}
WRITE/KEYW  IN_B      middummo.tbl
INPUTI(1) = IMSIZE(1)*ZEBCOL
INPUTI(2) = IMSIZE(1)*ZEBWIND
INPUTI(3) = IMSIZE(1)/ZEBSTEP
INPUTR(1) = ZEBSLOPE
INPUTR(2) = ZEBWIDTH
WRITE/KEYW   INPUTC/C/1/3  {ZEBVISU}

RUN STD_EXE:NECZEBORD


SEL/TABLE &o :Y.GE.{SCAN(1)}.AND.:Y.LE.{SCAN(2)}
COPY/TABLE  &o {ORDTAB}

SET/ECH     ZEBVISU=NO
!
REGR/ECHELLE
PREP/BACK  ?   INI
LOAD/ECHELLE






