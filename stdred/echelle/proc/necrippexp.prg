! @(#)necrippexp.prg	19.1 (ES0-DMD) 02/25/03 14:19:42
! @(#)necrippexp.prg	19.1  (ESO)  02/25/03  14:19:42
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       rippexp.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command RIPPLE/MEDIAN
!.VERSION     1.0    Creation    22-AUG-1991  PB
!
!-------------------------------------------------------
!
define/param  P1  ?              IMA   "Input Image"
define/param  P2  ?              IMA   "Output normalized"
define/param  P3  {RESPONSE}     IMA   "Output Response"
define/param  P4  {RIPORD}                  N   "Order number               :"
define/param  P5  {RIPLIM}                  ?   "Start,End                  :"
define/param  P6  {RIPRAD(1)},{RIPRAD(2)}   N   "Median, Smooth x-rad       :"
define/param  P7  {RIPBIN}                  N   "Bin:"
define/param  P8  5                         N   "Degree:"

define/local ROW/I/1/1 0

IF {P4} .LT. 0  GOTO FIT

COPY/II {P1} &p
COPY/II {P1} {P3}
COMPUTE/IMAGE {P3} = 0.

@ creifnot 1

SET/GRAPH PMODE=1 

WRITE/KEYW INPUTI/I/1/2  {P6}
IF INPUTI(1) .NE. 0 THEN

  @s necrippexp,histo  middummp  {P4}  {P7}
  PLOT/ROW &p @{P4} 
  DO ROW = 1 {{P1},NPIX(2)}
     OVERPLOT/ROW &p @{ROW}
  ENDDO

  FILTER/MEDIAN  &p &f 0,{INPUTI(1)}

ELSE

  COPY/II &p &f

ENDIF

IF INPUTI(2) .NE. 0 THEN
  FILTER/MEDIAN  &f &g {INPUTI(2)},0
ELSE
  COPY/II &f &g
ENDIF

FIT:

WRITE/KEYW INPUTI/I/1/2 {P5}
IF INPUTI(1) .EQ. 0 .AND. INPUTI(2) .EQ. 0 THEN
   INPUTI(1) = 1
   INPUTI(2) = {{P1},NPIX(1)}
ENDIF

DO ROW = 1 {{P1},NPIX(2)}

EXTRACT/IMAGE   &c  = &g[<,@{ROW}:>,@{ROW}]

COPY/IT &c  &t 
COMPUTE/TABLE   &t  :SEQU = sequence
SELECT/TABLE    &t  (:SEQU.GT.{INPUTI(1)}.AND.:SEQU.LT.{INPUTI(2)})

COMPUTE/TABLE &t :WC2 = :SEQU*:SEQU
COMPUTE/TABLE &t :WC3 = :WC2*:SEQU
COMPUTE/TABLE &t :WC4 = :WC2*:WC2 
COMPUTE/TABLE &t :WC5 = :WC3*:WC2 
COMPUTE/TABLE &t :WC6 = :WC3*:WC3

REGRES/ROBUST &t  #1  :SEQU,:WC2,:WC3,:WC4,:WC5,:WC6  ? #1
SELECT/TABLE  &t  ALL

DELETE/COLU     &t  #7,#6,#5,#4,#3,#2
COPY/TI   &t  &z

COPY/DD   &c  *,1   &z

INSERT/IMAGE     &z   {P3}  @1,@{ROW}

PLOT/ROW {P3}  @{ROW}
OVERPLOT/ROW &p  @{ROW}

ENDDO

COMPUTE/IMAGE {P2} = &p/{P3}

!********************************************************** 

ENTRY histo

DEFINE/PARAM P1  ?  IMA  "Enter input/output image"
DEFINE/PARAM P2  0   N   "Reference order"
DEFINE/PARAM P3  ?   N   "Bin size"

DEFINE/LOCAL NORD/I/1/1   {{P1},NPIX(2)}
DEFINE/LOCAL ORD/I/1/1    0
DEFINE/LOCAL ORD1/I/1/1   0
DEFINE/LOCAL FACTOR/D/1/1 0.
DEFINE/LOCAL LOOP/I/1/1   0
DEFINE/LOCAL BIN/R/1/1    {P3}

DEFINE/LOCAl POSMAX/I/1/1 0
DEFINE/LOCAl POS0/I/1/1 0
DEFINE/LOCAl POS1/I/1/1 0
DEFINE/LOCAL COEF0/R/1/1  0
DEFINE/LOCAL COEFM/R/1/1  0
DEFINE/LOCAL COEF1/R/1/1  0
DEFINE/LOCAL SCOEF/R/1/1  0
DEFINE/LOCAL POSGRAV/R/1/1 0
DEFINE/LOCAL CORD/I/1/1   {P2}

IF NORD .EQ. 1   RETURN 

IF CORD .EQ. 0    CORD = NORD/2
COPY/IT   {P1}   &t


DO ORD =  1 {NORD}

   IF ORD .NE. CORD  THEN

goto suit
extract/image &r1 = {P1}[<,@{ORD}:>,@{ORD}]
extract/image &r2 = {P1}[<,@{CORD}:>,@{CORD}]
write/descr &r1 start 1
write/descr &r2 start 1
write/descr &r1 step 1
write/descr &r2 step 1

xcorr/image &r1 &r2 &rc 20.
find/minmax &rc {SESSOUTV}
outputi(1) = outputi(3) - 21
write/descr &r2 start {outputi(1)}

compute/image &r3 = &r1/&r2
copy/it &r3 &r4
compute/histo &rh = middummr4.tbl   #1  {RIPBIN}  0.5  1.5


   STAT/IMAGE   &rh {SESSOUTV}
   POSMAX = OUTPUTI(3)
   POS0   = POSMAX - 1
   IF POS0 .LT. 1  POS0 = 1
   POS1   = POSMAX + 1
   COEF0  = {&rh[@{POS0}]}
   COEFM  = {&rh[@{POSMAX}]}
   COEF1  = {&rh[@{POS1}]}
   SCOEF  = COEF0 + COEFM + COEF1
   POSGRAV = (POS0*COEF0+POSMAX*COEFM+POS1*COEF1)/SCOEF
   FACTOR = (POSGRAV-1)*{middummrh,STEP(1)}+{middummrh,START(1)}
   WRITE/OUT "Order {ORD}. Normalization {FACTOR}"

   COMPUTE/IMAGE &r2 = &r2*{FACTOR}
   INSERT/IMAGE  &r2 {P1} @{&r2,START},@{ORD}
suit:

   COMPUTE/TABLE  &t  :AUX = #{ORD}/#{CORD}

   COMPUTE/HISTO &a = middummt.tbl  :AUX  {BIN}  0.1  10.
   STAT/IMAGE   &a {SESSOUTV}
   POSMAX = OUTPUTI(3)
   POS0   = POSMAX - 1
   IF POS0 .LT. 1  POS0 = 1
   POS1   = POSMAX + 1
   COEF0  = {&a[@{POS0}]}
   COEFM  = {&a[@{POSMAX}]}
   COEF1  = {&a[@{POS1}]}
   SCOEF  = COEF0 + COEFM + COEF1
   POSGRAV = (POS0*COEF0+POSMAX*COEFM+POS1*COEF1)/SCOEF
   FACTOR = (POSGRAV-1)*{middumma,STEP(1)}+{middumma,START(1)}
   WRITE/OUT "Order {ORD}. Normalization {FACTOR}"
   COMPUTE/TABLE   &t  #{ORD} = #{ORD} / {FACTOR}
   
suit:
   ENDIF



ENDDO

DELETE/COLUMN &t  :AUX
COPY/TI  &t   &u
COPY/DD  {P1}  *,1  &u
COPY/II  &u    {P1}
COPY/DD  &u    *,1  {P1}

RETURN

!********************************************************8

ENTRY PIXELS

IF M$TSTNO(P4) .EQ. 1  THEN 
       write/keyw  PIX/I/1/2   {P4}
       IF PIX(1) .EQ. 0   PIX(1) = 1
       IF PIX(2) .EQ. 0   PIX(2) = NBCOL
ELSE
       AVE/ROW       &b = {NAME}  @{ORD},@{ORD}
       STAT/IMAGE      &b       {SESSOUTV}
       COMPUTE/IMAGE   &c = &b/{OUTPUTR(3)}
       SET/GRAPH      YAXIS=0.,5.
       PLOT/ROW          &c
       GET/GCURS     PIX,DESCR
       COPY/DK       &c  PIX/R/3/1   PIX/I/1/1
       COPY/DK       &c  PIX/R/10/1  PIX/I/2/1
       IF PIX(1) .GT. PIX(2) THEN
          ROW = PIX(1)
          PIX(1) = PIX(2)
          PIX(2) = ROW
       ENDIF
       IF PIX(1) .LT. 1  PIX(1) = 1
       IF PIX(2) .GT. NBCOL  PIX(2) = NBCOL
       WRITE/OUT     "Start, End positions: {PIX(1)},{PIX(2)}"
       SET/GRAPH
ENDIF

RETURN

!**************************************************************

ENTRY ONEROW

DEFINE/PARAM P1 ?  IMA   "Image 1 :"
DEFINE/PARAM P2 ?  IMA   "Image 2 :"
define/param P3 1  NUM   "Row Number:"

DEFINE/LOCAl POSMAX/I/1/1 0
DEFINE/LOCAl POS0/I/1/1 0
DEFINE/LOCAl POS1/I/1/1 0
DEFINE/LOCAL COEF0/R/1/1  0
DEFINE/LOCAL COEFM/R/1/1  0
DEFINE/LOCAL COEF1/R/1/1  0
DEFINE/LOCAL SCOEF/R/1/1  0
DEFINE/LOCAL POSGRAV/R/1/1 0
DEFINE/LOCAL FACTOR/D/1/1 0.

extract/image &r1 = {P1}[<,@{P3}:>,@{P3}]
extract/image &r2 = {P2}[<,@{P3}:>,@{P3}]

write/descr &r1 start 1
write/descr &r2 start 1
write/descr &r1 step 1
write/descr &r2 step 1

xcorr/image &r1 &r2 &rc 20.
find/minmax &rc {SESSOUTV}
outputi(1) = outputi(3) - 21
write/descr &r2 start {outputi(1)}

compute/image &r3 = &r1/&r2
copy/it &r3 &r4

compute/histo &rh = middummr4.tbl   #1  {RIPBIN}  0.5  1.5


   STAT/IMAGE   &rh {SESSOUTV}
   POSMAX = OUTPUTI(3)
   POS0   = POSMAX - 1
   IF POS0 .LT. 1  POS0 = 1
   POS1   = POSMAX + 1
   COEF0  = {&rh[@{POS0}]}
   COEFM  = {&rh[@{POSMAX}]}
   COEF1  = {&rh[@{POS1}]}
   SCOEF  = COEF0 + COEFM + COEF1
   POSGRAV = (POS0*COEF0+POSMAX*COEFM+POS1*COEF1)/SCOEF
   FACTOR = (POSGRAV-1)*{middummrh,STEP(1)}+{middummrh,START(1)}
   WRITE/OUT "Normalization {FACTOR}"

   COMPUTE/IMAGE &r2 = &r2*{FACTOR}
   INSERT/IMAGE  &r2 {P2} @{&r2,START},@{P3}

RETURN


