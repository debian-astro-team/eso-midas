! @(#)necresp.prg	19.1 (ES0-DMD) 02/25/03 14:19:42
! @(#)necresp.prg	19.1  (ESO)  02/25/03  14:19:42
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echresp.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command RESPONSE/ECHELLE [std] [fluxtab] [response]
!.VERSION     1.0    Creation    
!.VERSION     1.1    change `\' to ` ',  KB
!
!-------------------------------------------------------
!
DEFINE/PARAM  P1  {STD}      I  "Raw standard star:"
DEFINE/PARAM  P2  {FLUXTAB}  I  "Flux table:"
DEFINE/PARAM  P3  {RESPONSE} I  "Output response:"

VERIFY/ECHELLE {P1}
SET/ECH STD={P1}  FLUXTAB={P2}   RESPONSE={P3}
VERIFY/SPEC    {FLUXTAB}  MID_STANDARD FLUXTAB


DEFINE/LOCAL    LOOP/I/1/1    0
DEFINE/LOCAL    PIXPOS/I/1/1  0   
DEFINE/LOCAL    NROW/I/1/1    0

REDUCE/SIMPLY   {STD}  &m  &j  !  Simplified reduction
FILTER/MEDIAN   &m   &p  {FILTMED(1)},{FILTMED(2)},{FILTMED(3)}
FILTER/SMOOTH   &p   &n  {FILTSMO(1)},{FILTSMO(2)},{FILTSMO(3)}

WRITE/OUT       "Compute flux calibration: Read columns :WAVE, :FLUX_W -
from {FLUXTAB}"
CONVERT/TABLE   &o = {FLUXTAB} :WAVE :FLUX_W  &n SPECTR
VERIFY/ECHELLE  &o   REBIN
VERIFY/ECHELLE  &o AGAINST &n
COMPUTE/IMAGE   &p = &o/&n

WRITE/IMAGE     &p  [<,<:@{PIXNUL(1)},>] 0.  ALL  {SESSOUTV}

NROW = {middummp,NPIX(2)}
DO LOOP = 1  {NROW}
   PIXPOS = {middummm.bdf,NPTOT({LOOP})} - PIXNUL(2)
   WRITE/IMAGE &p [@{PIXPOS},@{LOOP}:>,@{LOOP}]  0.  ALL  {SESSOUTV}
ENDDO

COPY/II          middummp  {RESPONSE}
COPY/DD          {STD}  *,3  {RESPONSE}

!@ creifnot 2 heat
!DISP/CHAN       0
!LOAD/IMAGE      {RESPONSE}  0 8,8

WRITE/OUT       OUTPUT RESPONSE
WRITE/OUT       "---------------"
READ/DESC       {RESPONSE}
WRITE/OUT       "------------------------------------------------------"
WRITE/OUT       "Check if RESPONSE looks reasonable..."
WRITE/OUT       "To correct it use REPEAT/ECHELLE"
WRITE/OUT       "------------------------------------------------------"
WRITE/OUT       "Apply RESPONSE to Standard Star"

VERIFY/ECHELLE  &m AGAINST {RESPONSE}
COMPUTE/IMAGE   &a = &m*{RESPONSE}

COPY/DD         {STD}  *,3  &a

WRITE/OUT       "Use MERGE/ECHELLE &a name 3.0 AVERAGE"
WRITE/OUT       "to inspect the calibrated standard star"

RETURN





