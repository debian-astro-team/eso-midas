!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991-2011 European Southern Observatory
!.IDENT       neciden.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Wavelength calibration of echelle spectra
!             Implement command IDENT/ECHELLE
!    IDENT/ECH  WLC  Lincat  Degree  Tol  Initol,Dev,Stop  Starter Session
!.VERSION     1.0    Creation    18.01.91
! 110525	last modif
! 
!-------------------------------------------------------
!
IF P1(1:1) .NE. "?"   WRITE/KEY    WLC/C/1/60      {P1}
IF P2(1:1) .NE. "?"   WRITE/KEY    LINCAT/C/1/60   {P2}
IF P3(1:1) .NE. "?"   DC = {P3}
IF P4(1:1) .NE. "?"   TOL = {P4}
IF P5(1:1) .NE. "?"   WRITE/KEY    WLCITER/R/1/5   {P5}
IF P6(1:1) .NE. "?"   WRITE/KEY    WLCMTD/C/1/10   {P6}
IF P7(1:1) .NE. "?"   WRITE/KEY    GUESS/C/1/60    {P7}
IF P8(1:1) .NE. "?"   WRITE/KEY    CCDBIN/R/1/2    {P8}

DEFINE/LOCAL    METHOD/C/1/7  "YYYYYYY" ?  +lower
IF WLCMTD(1:1) .EQ.  "P"   WRITE/KEY  METHOD/C/1/7  "PAIR   "
IF WLCMTD(1:1) .EQ.  "A"   WRITE/KEY  METHOD/C/1/7  "ANGLE  "
IF WLCMTD(1:1) .EQ.  "T"   WRITE/KEY  METHOD/C/1/7  "TWO-D  "
IF WLCMTD(1:1) .EQ.  "G"   WRITE/KEY  METHOD/C/1/7  "GUESS  "
IF WLCMTD(1:1) .EQ.  "R"   WRITE/KEY  METHOD/C/1/7  "RESTART"
IF WLCMTD(1:1) .EQ.  "O"   WRITE/KEY  METHOD/C/1/7  "ORDER  "
IF WLCMTD(1:1) .EQ.  "L"   WRITE/KEY  METHOD/C/1/7  "LINEAR "

IF METHOD(1:1) .EQ. "Y" THEN
   WRITE/OUT "IDENT/ECHELLE: unknown method {WLCMTD}"
   WRITE/OUT "Method can be PAIR, ANGLE, TWO-D, GUESS, RESTART, ORDER, LINEAR"
   RETURN/EXIT
ENDIF

VERIFY/ECHELLE {WLC}
VERIFY/ECHELLE {LINTAB} TAB
VERIFY/SPEC    {LINCAT} MID_ARC LINCAT TAB

IF WLCVISU(1:1) .EQ. "Y"   THEN
  DISPLAY/ECHELLE {WLC}
  CLEAR/CHAN OVER
ENDIF

WRITE/KEY  IN_A {LINTAB}
WRITE/KEY  IN_B {LINCAT}
INPUTR(1) = -10 ! Verification mode
RUN STD_EXE:spematch 
IF INPUTR(1) .EQ. -1    THEN
    WRITE/OUT   "Now sorting table {LINCAT}."
    SORT/TABLE  {LINCAT}  :WAVE
    WRITE/KEY  IN_A {LINTAB}
    WRITE/KEY  IN_B {LINCAT}
    INPUTR(1) = -10 ! Verification mode
    RUN STD_EXE:spematch 
ENDIF
IF INPUTR(1) .LT. 0  RETURN/EXIT

! Initial check : Number of elements of LINE > 2 * NBORD * (DEGREE + 1)
IF DC .GT.  8   ERROR/ECHELLE  IDENT/ECHELLE   DC
IF WLCOPT(1:1) .NE. "1" .AND. WLCOPT(1:1) .NE. "2" THEN
    ERROR/ECHELLE  IDENT/ECHELLE   WLCOPT
ENDIF
IF WLCREG(1:1) .NE. "S" .AND. WLCREG(1:1) .NE. "R" then
   write/out "Error: Keyword WLCREG = {WLCREG}. Accepted values: Standard, Robust"
   error/echelle  IDENT/ECHELLE WLCREG
ENDIF

DEFINE/LOCAL   WLCMTD/C/1/12 {WLCMTD} ? +lower 
DEFINE/LOCAL   DEGREE/I/1/1  {DC}     ? +lower  
! Degree of poly. for orders fit.
DEFINE/LOCAL   NBORD/I/1/1   {ECHORD(1)} ? +lower   ! Number of orders

DEFINE/LOCAL ROTNAM/C/1/6  ROTATE     ? +lower 
DEFINE/LOCAL   nbr/r/1/3     0,0,0    ? +lower 
DEFINE/LOCAL   last/i/1/1    0        ? +lower 
DEFINE/LOCAL   line/i/1/1    0        ? +lower 
DEFINE/LOCAL   xymax/r/1/2   0,0      ? +lower 
DEFINE/LOCAL   nbsel/i/1/1   0        ? +lower 
DEFINE/LOCAL   alpha/d/1/3   0,0,0    ? +lower 
DEFINE/LOCAL   coupl/i/1/1   0        ? +lower 
DEFINE/LOCAL   ordiff/i/1/1  0        ? +lower  
DEFINE/LOCAL   ordpos/i/1/2  0,0      ? +lower 
DEFINE/LOCAL   rms/r/1/2     0,0      ? +lower 
DEFINE/LOCAL   pixel/r/1/3   0,0,0    ? +lower 
DEFINE/LOCAL   select/r/1/3  0,0,0    ? +lower 
DEFINE/LOCAL   alpmax/r/1/1  0        ? +lower 
DEFINE/LOCAL   RATIO/R/1/1   0.       ? +lower 
DEFINE/LOCAL   NBRMIN/I/1/1  0        ? +lower 
DEFINE/LOCAL   NBLMIN/I/1/1  0        ? +lower 
DEFINE/LOCAL   LWCL/R/1/5    0.,0.,0.,0.,0. ? +lower
define/local   wlcstep/D/1/1 0.       ? +lower

WRITE/KEYWORD  xypos/r/1/2   0,0
WRITE/KEYWORD  wave/d/1/1    0
WRITE/KEYWORD  sequ/I/1/1    0
WRITE/KEYWORD  ordval/r/1/1  0
WRITE/KEYWORD  answ/c/1/3    HAI
COPY/KEYWORD   wlciter/R/1/5 LWCL/R/1/5
copy/dk        {wlc} step/D/1/1  wlcstep

SET/FORMAT  I1  F12.5

WRITE/OUT      "*** Wavelength calibration ***"            
WRITE/OUT      "   "                                        

IF OUTMODE(1:1) .NE. "S" THEN ! Mode Silent
WRITE/OUT      " Method                    : {WLCMTD}"      
WRITE/OUT      " Dispersion relation type  : {WLCOPT}"
WRITE/OUT      " Guess session (if any)    : {GUESS}"       
WRITE/OUT      " WLC frame                 : {WLC}"         
WRITE/OUT      " Line catalog              : {LINCAT}"      
WRITE/OUT      " Number of orders          : {NBORD}"       
WRITE/OUT      " Polynomial degree         : {DEGREE}"      
WRITE/OUT      " Binning                   : {CCDBIN(1)},{CCDBIN(2)}"    
WRITE/OUT      "** Identification loop **"    
WRITE/OUT      " Initial accuracy          : {LWCL(1)} pixel"    
WRITE/OUT      " Next neighbour parameter  : {LWCL(2)}"          
WRITE/OUT      " Maximal error             : {LWCL(3)} pixel"
WRITE/OUT      " Initial error             : {LWCL(4)} pixel"
WRITE/OUT      " Iteration error           : {LWCL(5)} std dev."
WRITE/OUT      "** Rejection loop **"                               
WRITE/OUT      " Tolerance                 : {TOL}"                 
ENDIF

IF LWCL(2) .LE. 0.  ERROR/ECHELLE  IDENTIFY/ECHELLE  LWCL(2)

WRITE/KEY      LAST/I/1/1   {{LINTAB},TBLCONTR(4)} ! Number of elements in LINE
LINE   = (2. * NBORD * (DEGREE + 2.))  ! Minimum number of elements in table line
NBRMIN = 2*(DEGREE+1)*(DEGREE+1)       ! Minimum number of identifications to avoid echelle relat.

IF LAST .LT. LINE  THEN
         WRITE/OUT "WARNING : The table LINE, produced by SEARCH/ECHELLE"
         WRITE/OUT "perhaps does not contain enough lines for this calibration."
         WRITE/OUT "Corrective actions can be to repeat:"
         WRITE/OUT "       - SEARCH/ECH with a lower threshold"
         WRITE/OUT "       - IDENT/ECH with a lower degree"
ENDIF

IF WLCMTD(1:1) .EQ. "O"  THEN
   COPY/DK  {LINTAB} PIXEL  PIXEL 
   GOTO  END
ENDIF

WRITE/DESCR     {LINTAB}      HISTORY_UPDA/I/1/1   0    ! No update of history

CREATE/COLUMN   {LINTAB}      :ORDER       "Abs. order"            I6   {SESSOUTV}
CREATE/COLUMN   {LINTAB}      :IDENT       "Wavelength"    F10.4   R*8  {SESSOUTV}
CREATE/COLUMN   {LINTAB}      :WAVEC       "Wavelength"    F10.4   R*8  {SESSOUTV}
CREATE/COLUMN   {LINTAB}      :AUX         "ORD.lambda"    F10.4   R*8  {SESSOUTV}
CREATE/COLUMN   {LINTAB}      :XROT        "Rotat. pos"    F7.2         {SESSOUTV}

CREATE/COLUMN   {LINTAB}      :ERROR       "Wav. un.  "    F10.6   R*8  {SESSOUTV}
CREATE/COLUMN   {LINTAB}      :DISTANCE    "Wav. un.  "    F10.6   R*8  {SESSOUTV}
CREATE/COLUMN   {LINTAB}      :RESIDUAL    "Wavel. un."    F10.6   R*8  {SESSOUTV}
CREATE/COLUMN   {LINTAB}      :SELECT      "Logical   "    I6      I    {SESSOUTV}

CREATE/TABLE    &t    1    20
CREATE/COLUMN   &t    :COL

COMPUTE/TABLE   {LINTAB}      :ERROR = -1.
COMPUTE/TABLE   {LINTAB}      :IDENT = 0.
COMPUTE/TABLE   {LINTAB}      :RESIDUAL = 0.
COMPUTE/TABLE   {LINTAB}      :SEQU  = sequence
COMPUTE/TABLE   {LINTAB}      :SELECT = 0.

IF  WLCMTD(1:1) .EQ. "R"  THEN
    COPY/DK  middummr.tbl  WLCMTD   WLCMTD
ELSE
    CREATE/TABLE    &r    4    10
    CREATE/COLUMN   &r    :X
    CREATE/COLUMN   &r    :Y
    CREATE/COLUMN   &r    :DX
    CREATE/COLUMN   &r    :DY
ENDIF
!
! Use different starters depending on the configuration
! Interactive PAIR method is supported by the routine PAIR
! Other interactive methods (ANGLE, TWO-D) are supported by ANGLE

IF WLCMTD(1:1) .EQ. "P" @s neciden,pair

IF WLCMTD(1:1) .EQ. "A" @s neciden,angle
IF WLCMTD(1:1) .EQ. "T" @s neciden,angle

IF WLCMTD(1:1) .EQ. "G" @s neciden,guess
IF WLCMTD(1:1) .EQ. "L" @s neciden,hough

COPY/KD  PIXEL  {LINTAB}     PIXEL
COPY/KD  WLCMTD middummr.tbl WLCMTD

! Branch to the main iteration loop
ITERATION:
IF WLCREG(1:1) .EQ. "S"  @s neciden,iter
IF WLCREG(1:1) .EQ. "R"  @s neciden,htiter

END:
pixel(1) = M$ABS(pixel(1))
!
! Now, to be able to find estimated polynomials where there is not 
! enough lines, one fits a 2D polynomial as m.lambda=f(x,y). The reason 
! why this is not used from the beginning is that one needs 
! (DEGREE+2)*(DEGREE+2) observation values (at least).
!
COMPUTE/TABLE   {LINTAB}   :AUX = 0.
COMPUTE/TABLE   {LINTAB}   :AUX = :ORDER * :IDENT
SELECT/TABLE    {LINTAB}   (:IDENT.GT.0.0)  {SESSOUTV}
REGRES/POLY     {LINTAB}   :AUX   :X,:ORDER  {DEGREE},{DEGREE}  KEYLONG {SESSOUTV}
SAVE/REGR       {LINTAB}   REGR   KEYLONG 

IF TOL .EQ. 0. THEN
   PIXEL(2) = 3.*KEYLONGR(5)*2./(echord(2)+echord(3))
ELSE
 IF TOL .GT. 0. THEN
     PIXEL(2)        =          TOL*PIXEL(1)
 ELSE
     PIXEL(2)        =          M$ABS(TOL)
 ENDIF
ENDIF

COMPUTE/REGRE {LINTAB} :WAVEC = REGR
COMPUTE/TABLE {LINTAB} :WAVEC = :WAVEC/:ORDER
COMPUTE/TABLE {LINTAB} :RESIDUAL = :WAVEC - :IDENT
SELECT/TABLE  {LINTAB} ABS(:RESIDUAL) .LT. {PIXEL(2)} {SESSOUTV}

Write/Out     "Final Selection: Threshold = {pixel(2)} wav. units, Nb = {outputi(1)}"
IF WLCVISU(1:1) .EQ. "Y" THEN
   CLEAR/CHAN OVER
   LOAD/TABLE {LINTAB} :X :YNEW
ENDIF

! Save the final dispersion relation and order numbers for later
! use in GUESS mode.
REGRES/POLY  {LINTAB} :AUX :X,:ORDER  {DEGREE},{DEGREE}  KEYLONG {SESSOUTV}
SAVE/REGR    {LINTAB} REGR KEYLONG 
REGRES/POLY  {LINTAB} :ORDER :X,:YNEW  {DEGREE},{DEGREE}  KEYLONG {SESSOUTV}
SAVE/REGR    {LINTAB} RORD KEYLONG 

SELECT/TABLE    {LINTAB}   (:IDENT.GT.0.0) {SESSOUTV}
IF WLCOPT(1:1) .EQ. "2" SELECT/TABLE {LINTAB} SELECT.AND.ABS(:RESIDUAL).LT.{PIXEL(2)}

INPUTI(1)   = DC
INPUTI(2)   = echord(1)
INPUTI(3)   = imsize(1)
INPUTI(4)   = {WLCOPT(1:1)}

INPUTR(1)   = PIXEL(2)  ! Tolerance in wavelength units

INPUTD(1)   = {{WLC},START(1)}
INPUTD(2)   = {{WLC},STEP(1)}

RUN  STD_EXE:neciden

WLRANGE(1) = OUTPUTR(1)
WLRANGE(2) = OUTPUTR(2)

@s neciden,verif

! Estimate value of K for optional IUE-like RIPPLE correction
set/format F20.10
STAT/TAB  {LINTAB}   :AUX  {SESSOUTV}
write/key  SAMPLE  {{LINTAB},PIXEL(1)}
RIPK = OUTPUTR(3)
echord(6) = echord(3) + 1
write/out "Set parameters SAMPLE={SAMPLE} and RIPK={RIPK}"

EXIT:

if wlcvisu(1:1) .eq. "Y" PLOT/RESIDUAL

RETURN

!*****************************************************************************

ENTRY PAIR

INTERACT:

IF METHOD(1:1) .NE. "R" THEN

WRITE/OUT    "Info: You must identify lines which are repeated in"
WRITE/OUT    "      overlapped regions of two adjacent orders. You"
WRITE/OUT    "      will do it two times."
WRITE/OUT    "    "
WRITE/OUT    "With the cursor, select two lines that appear in two "
WRITE/OUT    "different orders each. Click them in the following order :"
WRITE/OUT    "   - First line: left-hand side, then right-side"
WRITE/OUT    "   - Second line: left-hand-side, then right-side"

ENDIF

@s neciden,cursor {METHOD}
@s neciden,overlay

if outputi(1) .ne. 3 then
   write/out "you haven't selected four lines. please restart."
   write/descr  middummr.tbl  tblcontr/i/4/1   0
   GOTO  INTERACT
endif

! *** Find selections in table LINE


@s neciden,preploop

DO line = 1  'last'

     @s neciden,assoc

     IF line .ne. 2 THEN
        IF line .ne. 4 THEN
           COUPL(1)   =    {{LINTAB},:ORDER,@{SEQU}}
           INQUIRE/KEY     WAVE/D/1/1  "Sequence no. {LINE}, Order no. {COUPL}. Enter wavelength : "
        ENDIF
     ENDIF
     COPY/KT               WAVE/D/1/1  {LINTAB}   :IDENT   @{SEQU}

ENDDO

! computes rotation angle and peforms rotation
ANGLE:

alpha(1) = ({middummr,:y,@1}-{middummr,:y,@2})/({middummr,:x,@1}-{middummr,:x,@2})
alpha(2) = ({middummr,:y,@3}-{middummr,:y,@4})/({middummr,:x,@3}-{middummr,:x,@4})

ALPHA(1) = M$ATAN (ALPHA(1))
ALPHA(2) = M$ATAN (ALPHA(2))

inputr(1) = 0.
! @s echidenew,analys
@s neciden,analys
alpmax = outputr(1)

! Average angle as a result of geometrical analysis
ALPHA(3) =  (0.5)*(ALPHA(1) + ALPHA(2))
WRITE/OUT  "      >>>   Geometrical orientation analysis   >>>  Angle: 'ALPHA(3)' deg."
WRITE/OUT  "      >>>   Analytical  orientation analysis   >>>  Angle: 'ALPMAX' deg."

define/local angdif/R/1/1 0.
angdif = M$ABS(ALPHA(3)-ALPMAX)
IF angdif .gt. 8. then
    Write/Out "Warning: An inconsistency has been detected between geometrical"
    Write/Out "         and analytical determination of the rotation angle."
    Write/Out "         This could result from misidentifications or wrong "
    Write/Out "         order number. "
    INQUIRE/KEYWORD ANSW/C/1/3   "Continue anyway (y/n,def=n) ? "
    IF ANSW(1:1) .EQ. "N" .OR. AUX_MODE(7) .EQ. 0  RETURN/EXIT
ENDIF

!Prepares table
COMPUTE/TABLE  {LINTAB}  :XROT = :X*{CCDBIN(1)}*COS('alpha(3)')+:YNEW*{CCDBIN(2)}*SIN('alpha(3)')


COMPUTE/TABLE  {LINTAB}  :AUX = :ORDER * :IDENT
SELECT/TABLE   {LINTAB}  (:IDENT.GT.0.0) {SESSOUTV}
NBR(1) = OUTPUTI(1)
REGRES/POLY    {LINTAB}  :AUX   :XROT  2 {SESSOUTV}
SAVE/REGRES    {LINTAB}  REGR

RMS(1)   =  2.  *  OUTPUTR(5) / (ORDPOS(1) + ORDPOS(2))
PIXEL(1) =  2.  *  OUTPUTD(2) / (ORDPOS(1) + ORDPOS(2))
PIXEL(1) = M$ABS(PIXEL(1)*wlcstep)

IF {LWCL(1)} .GT. 0. THEN
        PIXEL(2) =  LWCL(1)*PIXEL(1)
ELSE
        PIXEL(2) = M$ABS(LWCL(1))
ENDIF

WRITE/OUT  "*** Initial, coarse estimate ***"
WRITE/OUT  "Mean pixel width      : {PIXEL(1)} wavel. units"
WRITE/OUT  "RMS error of relation : {RMS(1)} wavel. units"
WRITE/OUT  "Maximum allowed error : {PIXEL(2)} wavel. units"

IF rms(1) .gt. pixel(2) THEN
   WRITE/OUT "This accuracy appears insufficient to attempt automatic line identification."
   WRITE/OUT "Please check your identifications."

   INPUTR(1) = ALPHA(3)
   INPUTR(2) = 1.
   @s neciden,analys
   RATIO = M$ABS(RMS(1)/PIXEL(2))
   IF RATIO .LT. LWCL(3)  THEN
   WRITE/OUT "However the accuracy may be still acceptable."
   INQUIRE/KEY   ANSW/C/1/3  "Start anyway (y/n, default no) ?"
   IF ANSW(1:1) .EQ. "Y" THEN
           GOTO END
   ELSE
           RETURN/EXIT
           WRITE/DESCR  middummc.tbl  TBLCONTR/I/4/1   0
           IF METHOD(1:1) .NE. "R"  WRITE/DESCR  middummr.tbl TBLCONTR/I/4/1 0
           GOTO  INTERACT
   ENDIF
   ELSE
   WRITE/OUT "Inaccuracy is too large. Aborted..."
   RETURN/EXIT
   ENDIF
ENDIF

END:
 !Prepares new table with analytical rotation angle
 COMPUTE/TABLE  {LINTAB}  :XROT = :X*{CCDBIN(1)}*COS('alpmax')+:YNEW*{CCDBIN(2)}*SIN('alpmax')
 
 COMPUTE/TABLE  {LINTAB}  :AUX = :ORDER * :IDENT
 SELECT/TABLE   {LINTAB}  (:IDENT.GT.0.0)
 NBR(1) = OUTPUTI(1)
 REGRES/POLY    {LINTAB}  :AUX   :XROT  2
 SAVE/REGRES    {LINTAB}  REGR
 
 RMS(1)   =  2.  *  OUTPUTR(5) / (ORDPOS(1) + ORDPOS(2))
 PIXEL(1) =  2.  *  OUTPUTD(2) / (ORDPOS(1) + ORDPOS(2))
 pixel(1) =  M$ABS(pixel(1))
 
 IF {LWCL(1)} .GT. 0. THEN
        PIXEL(2) =  {LWCL(1)} *  PIXEL(1)
 ELSE
        PIXEL(2) = M$ABS({LWCL(1)})
 ENDIF

RETURN

! ***************** Start with 4 lines and play with rotation ************

ENTRY ANGLE

DEFINE/LOCAL  DEGSTART/I/1/1  2  ? +lower 

METH2:

IF WLCMTD(1:1) .EQ. "A"  NBLMIN = 4
IF WLCMTD(1:1) .EQ. "C"  NBLMIN = DEGSTART + 2
IF WLCMTD(1:1) .EQ. "T"  NBLMIN = (DEGSTART+1)*(DEGSTART+1)+1

SET/FORMAT  I1

IF METHOD(1:1) .NE. "R" THEN
WRITE/OUT    "Point at least {NBLMIN} lines of which you know the wavelength"
ENDIF

@s neciden,cursor {METHOD}
@s neciden,overlay

outputi(1) = outputi(1) + 1

IF outputi(1) .lt. NBLMIN THEN
   WRITE/OUT "You have selected less than {NBLMIN} lines. Please start again."
   WRITE/DESCR middummr.tbl  TBLCONTR/I/4/1  0
   GOTO METH2
ENDIF
SET/FORMAT

@s neciden,preploop

DO line = 1  'last'

     @s neciden,assoc

     COUPL(1)   =    {{LINTAB},:ORDER,@{SEQU}}
     INQUIRE/KEY     WAVE/D/1/1  "Sequence no. {LINE}, Order no. 'COUPL'. Enter wavelength : "
     COPY/KT         WAVE/D/1/1  {LINTAB}   :IDENT   @{SEQU}

ENDDO

!KB SELECT/TABLE  {LINTAB}  (:IDENT.NE.0.0) {SESSOUTV}
SELECT/TAB       {LINTAB} (ABS(:IDENT).GT.0.00001)  {SESSOUTV}

IF WLCMTD(1:1) .EQ. "A" THEN ! Method ANGLE
  inputr(1) = 0.
  @s neciden,analys
  alpha(3) = outputr(1)
  COMPUTE/TABLE  {LINTAB}  :XROT=:X*COS('ALPHA(3)')+:YNEW*SIN('ALPHA(3)')
  COMPUTE/TABLE  {LINTAB}  :AUX = :ORDER * :IDENT
  SELECT/TABLE   {LINTAB}  (:IDENT.GT.0.0)
  NBR(1) = OUTPUTI(1)
  REGRES/POLY    {LINTAB}  :AUX   :XROT  {DEGSTART}
  PIXEL(1) = M$ABS(OUTPUTD(2)*wlcstep)
ENDIF

IF WLCMTD(1:1) .EQ. "T" THEN   ! Method TWO-D
  COMPUTE/TABLE  {LINTAB}  :AUX = :ORDER * :IDENT
  SELECT/TABLE   {LINTAB}  (:IDENT.GT.0.0)
  NBR(1) = OUTPUTI(1)
  REGRES/POLY    {LINTAB}  :AUX  :X,:ORDER  {DEGSTART},{DEGSTART}
  PIXEL(1) = M$ABS(OUTPUTD(2)*wlcstep)
ENDIF

SAVE/REGRES    {LINTAB}  REGR
RMS(1)   = OUTPUTR(5)
IF WLCMTD(1:1) .NE. "C"  THEN
   RMS(1)   =  2.*RMS(1)/(ORDPOS(1) + ORDPOS(2))
   PIXEL(1) =  2.*PIXEL(1)/ (ORDPOS(1) + ORDPOS(2))
ENDIF

IF {LWCL(1)} .GT. 0. THEN
        PIXEL(2) =  LWCL(1)*  PIXEL(1)
ELSE
        PIXEL(2) = M$ABS(LWCL(1))
ENDIF

WRITE/OUT  "*** Initial, coarse estimate ***"
WRITE/OUT  "Mean pixel width      : {PIXEL(1)} wavel. units"
WRITE/OUT  "RMS error of relation : {RMS(1)} wavel. units"
WRITE/OUT  "Maximum allowed error : {PIXEL(2)} wavel. units"
IF rms(1) .gt. pixel(2) THEN
   WRITE/OUT "This accuracy appears insufficient to attempt automatic line identification."
   WRITE/OUT "Please check your identifications."
   IF WLCMTD(1:1) .EQ. "A" THEN
      inputr(1) = alpha(3)
      @s neciden,analys
      alpha(3) = outputr(1)
   ENDIF
   INQUIRE/KEY   ANSW/C/1/3  "Start anyway (y/n, default no) ?"
   IF ANSW(1:1) .EQ. "Y" THEN
           GOTO END
   ELSE
           WRITE/DESCR  middummc.tbl  TBLCONTR/I/4/1   0
           WRITE/DESCR  middummr.tbl  TBLCONTR/I/4/1   0
           GOTO  METH2
   ENDIF
ENDIF

END:
RETURN

! ************************ Start with a guess *************************
!
! The dispersion relation m*lambda = f(x,m) and order numbers
! m = f(x,y) have been stored (REGR, RORD) as polynomial regression
! coefficients. The different steps necessary to generate the
! solution for a new observation set include:
!
! - Determine the absolute order number of the lines in the table
!   using the solution RORD.
! - Determine the shift (due to flexures, etc...) by cross-correlation
!   of the X positions in the two tables using common orders.
! - Apply the X shift to the positions and generate an initial dispersion
!   relation.
!
ENTRY GUESS
!
DEFINE/LOCAL   ORDMAX/I/1/1   0    ? +lower 
DEFINE/LOCAL   SHIFLOG/I/1/1  0    ? +lower 
! Indicates if a shift value is provided in GUESS
DEFINE/LOCAL   SHIFT/R/1/1    0.   ? +lower 
DEFINE/LOCAL   GUETAB/C/1/60  XXX  ? +lower  ! Guess LINE table
DEFINE/LOCAL   MIDORD/I/1/2 0,0    ? +lower 
!
SHIFLOG = M$INDEX(GUESS,",")
IF SHIFLOG .GT. 0  THEN
   SHIFLOG = SHIFLOG - 1
   WRITE/KEY GUETAB  {GUESS(1:{SHIFLOG})}LINE.tbl
   SHIFLOG = SHIFLOG + 2
   SHIFT   = {GUESS({SHIFLOG}:)}
ELSE
   WRITE/KEY GUETAB  {GUESS}LINE.tbl
ENDIF
!
! Check existence of the guess table and copies the 
! average pixel value.
VERIFY/ECHELLE {GUETAB}  TAB
COPY/DK     {GUETAB} PIXEL/R/1/1    PIXEL/R/1/1  
IF {LWCL(1)} .GT. 0. THEN
        PIXEL(2) = LWCL(1) * PIXEL(1)
ELSE
        PIXEL(2) = M$ABS(LWCL(1))
ENDIF
!
! Determines absolute order numbers in the line.tbl
! and updates the ORDPOS keyword.
!
define/local doris/I/1/1 0
doris = M$EXISTD(GUETAB,"RORDI")

if doris .le. 0 then
   write/out "Warning:"
   write/out "Guess table {GUETAB} is of old format"
   write/out "Updating descriptors..."
   regress/poly {GUETAB} :ORDER :X,:YNEW {DEGREE},{DEGREE} KEYLONG {SESSOUTV}
   save/regr    {GUETAB} RORD  KEYLONG
endif

COPY/DD     {GUETAB} RORDI  {LINTAB}
COPY/DD     {GUETAB} RORDC  {LINTAB}
COPY/DD     {GUETAB} RORDR  {LINTAB}
COPY/DD     {GUETAB} RORDD  {LINTAB}
COMPUTE/REGR  {LINTAB} :AUX = RORD
COMPUTE/TABLE {LINTAB} :ORDER = INT(:AUX)
STATIST/TABLE {LINTAB} :ORDER {SESSOUTV}
ORDPOS(1) = outputr(2) ! Max
ordpos(2) = outputr(1) ! Min
echord(2) = ordpos(2)
echord(3) = ordpos(1)
!
! Determines the range of common absolute orders.
! Take the min of the two maximum absolute order numbers
! Take the max of the two minimum absolute order numbers
!
define/local guesord/I/1/2 0,0
define/local orange/I/1/2  0,0
COPY/DK  {GUETAB} ORDER/I/1/2  guesord
orange(1) = ordpos(1)
if guesord(1) .lt. orange(1)  orange(1) = guesord(1)
orange(2) = ordpos(2)
if guesord(2) .gt. orange(2)  orange(2) = guesord(2)
if orange(1) .lt. orange(2) then
   write/out "Guess Wavelength Calibration:"
   write/out "   No common absolute order number with session {GUESS}"
   write/out "   Cross-correlation is not peformed"
   shift = 0.
   goto gfit
endif
!
! Estimates the shift by cross-correlation or read value provided in 
! keyword GUESS. The estimate is performed on five orders selected in the 
! middle of the spectrum.
!
IF SHIFLOG .LE. 0 THEN
   MIDORD(1) = orange(2)
   MIDORD(2) = orange(1)
    WRITE/OUT "Estimate shift by cross-correlation"
    CORREL/LINE  {GUETAB}  {LINTAB}  ?  0.,0.25,100.,0.5 X,ORDER,+ {MIDORD(1)},{MIDORD(2)}  {SESSOUTV}
    SHIFT = OUTPUTR(1)
ENDIF
WRITE/OUT    "Cross-correlation shift value : 'SHIFT'"
!
! Applies the shift to the guess table and computes new coefficients
!
gfit:
COPY/TABLE      {GUETAB}  &g
COMPUTE/TABLE   &g :X = :X + 'shift'
COMPUTE/TABLE   &g :AUX = NULL
COMPUTE/TABLE   &g :AUX = :ORDER * :IDENT
SELECT/TABLE &g  (:SELECT.GT.0.5)  {SESSOUTV}
REGRES/POLY  &g  :AUX  :X,:ORDER  {DEGREE},{DEGREE}  KEYLONG {SESSOUTV}
SAVE/REGR    {LINTAB} REGR  KEYLONG
!
OUTPUTR(5) = {{LINTAB},REGRR(5)}
NBR(1)     = {{LINTAB},REGRI(1)}
RMS(1)     = 2.*OUTPUTR(5)/(ORDPOS(1)+ORDPOS(2))
!
WRITE/DESCR    {LINTAB}  ORDER/I/1/2   {ORDPOS(1)},{ORDPOS(2)}
WRITE/DESCR    {LINTAB}  PIXEL/R/1/2   {PIXEL(1)},{PIXEL(2)}
!
RETURN
! ************************  Load table in overlay plane *****************

ENTRY OVERLAY

DEFINE/LOCAL   NBROW/I/1/1   0    ? +lower 

IF WLCVISU(1:1) .NE. "Y"  RETURN

SELECT/TABLE   &r (sequence.ne.0)  {SESSOUTV}
NBROW = OUTPUTI(1)
COMPUTE/TABLE  &r  :SEQU = sequence
WRITE/DESCR    middummr.tbl  TBLCONTR/I/4/1  {NBROW}
CLEAR/CHAN    over
SELECT/TABLE  &r  (sequence.eq.1) {SESSOUTV}
LOAD/TABLE    &r  :X  :Y  :SEQU 0   10  {SESSOUTV}
SELECT/TABLE  &r  (sequence.ne.1) {SESSOUTV}
LOAD/TABLE    &r  :X  :Y  :SEQU 1   10 {SESSOUTV}

RETURN

! ************************  Cursor interaction **************************

ENTRY CURSOR

DEFINE/PARAMETER   P1    HAY   C    "Method" 

IF P1(1:1) .EQ. "R"  THEN
        IF OUTMODE(1:1) .EQ. "V" WRITE/OUT "Selection read from table middummr.tbl"
        WRITE/OUT "Previous method : {WLCMTD(1:8)}"
        RETURN
ENDIF

IF WLCVISU(1:1) .NE. "Y"  THEN
   @ creifnot 2 heat
   clear/chan over
   LOAD {WLC}
   WLCVISU = "YES"
ENDIF

! If the command CENTER/MOMENT is modified here, check consistency with
! command used near label RECLICK

WRITE/OUT    "For each line (for help, see CENTER/MOMENT with option CURSOR):"
WRITE/OUT    "       - select approximate position with cursor"
WRITE/OUT    "       - if necessary, optimize size of cursor rectangle"
WRITE/OUT    "       - enter position by pressing Enter on keyboard"
WRITE/OUT    "Exit by pressing Exit button"

CENTER/MOMENT  CURSOR  &c  

COMPUTE/TABLE  middummc   :XERR = ABS(:XEND - :XSTART)/2.
COMPUTE/TABLE  middummc   :YERR = ABS(:YEND - :YSTART)/2.

SELECT/TABLE   middummc   (ABS(:STATUS).LT.0.01)  {SESSOUTV}

COPY/TT        middummc   :XCEN     middummr   :X  {SESSOUTV}
COPY/TT        middummc   :YCEN     middummr   :Y  {SESSOUTV}
COPY/TT        middummc   :XERR     middummr   :DX {SESSOUTV}
COPY/TT        middummc   :YERR     middummr   :DY {SESSOUTV}

WRITE/DESCR    middummr.tbl    WLCMTD/C/1/8   {METHOD(1:5)}

RETURN

! ********************* Prepares association loop values *******************

ENTRY PREPLOOP

COMPUTE/TABLE  {LINTAB}   :IDENT = 0.

LAST(1) = {middummr.tbl,TBLCONTR(4)}

RETURN

! ******************** Associates positions ****************************

ENTRY ASSOC

STASS:

     xypos(1) = {middummr.tbl,:X,@{line}}
     xypos(2) = {middummr.tbl,:Y,@{line}}
     xymax(1) = {middummr.tbl,:DX,@{line}}
     xymax(2) = {middummr.tbl,:DY,@{line}}

     select/table  {LINTAB} ABS(:YNEW-'xypos(2)').LT.'xymax(2)'.and.ABS(:X-'xypos(1)').LT.'xymax(1)' {SESSOUTV}
     nbsel = outputi(1)

     IF nbsel .ne. 1 THEN
       WRITE/OUT "*** Sorry. Cannot identify feature {LINE}        ***"
       WRITE/OUT "*** Line either not found or blended             ***"
       WRITE/OUT "*** Please start over again with new line        ***"
       WRITE/OUT "*** All detected lines are now marked on display ***"
       SELECT/TABLE  {LINTAB}  ALL
       LOAD/TABLE    {LINTAB}  :X  :YNEW  {SESSOUTV}
       @s  neciden,reenter
       GOTO STASS
     ENDIF

     COPY/TT  {LINTAB}   :SEQU   middummt   #1 {SESSOUTV}
     SEQU(1)   =  {middummt.tbl,#1,@1}
     COPY/TT  {LINTAB}   :Y      middummt   #1 {SESSOUTV}
     ORDVAL(1)   =  {middummt.tbl,#1,@1}
     WRITE/TAB  {LINTAB} :SELECT @{SEQU}  2

     IF line .eq. 1  THEN
       INQUIRE/KEYW COUPL/I/1/1  "Enter absolute order number of first pointed line (square mark) : "
       ORDIFF(1)  =  M$NINT(ORDVAL(1)+COUPL(1))
       COMPUTE/TABLE  {LINTAB}   :ORDER = INT('ORDIFF'-:Y) !Nearest integer
       ORDPOS(1)  =   ORDIFF  -  NBORD
       ORDPOS(2)  =   ORDIFF  -  1
       WRITE/DESCR    {LINTAB}    ORDER/I/1/2    'ORDPOS(2)','ORDPOS(1)'
       ECHORD(3)  =   ORDPOS(2)   ! Non desirable duplication of info.
       ECHORD(2)  =   ORDPOS(1)
     ENDIF

RETURN

! ******************************************************************

ENTRY ITER

DEFINE/LOCAL ITER/I/1/1   0              ? +lower 
DEFINE/LOCAL NITER/I/1/1  {WLCNITER(2)}  ? +lower 
DEFINE/LOCAL NINDEP/I/1/1 0              ? +lower 
DEFINE/LOCAL PASS/I/1/1   0              ? +lower 
define/local rmsratio/R/1/1  0.

SET/FORMAT F12.5  I1

WRITE/OUT "Iterative Identification:"

ITERAT:

COMPUTE/REGRES  {LINTAB}  :WAVEC =  REGR
IF WLCMTD(1:1) .NE. "C"  THEN
   COMPUTE/TABLE   {LINTAB}  :WAVEC = :WAVEC/:ORDER
ENDIF
COMPUTE/TABLE   {LINTAB}  :IDENT = 0.

WRITE/KEY   IN_A/C/1/60     {LINTAB}
WRITE/KEY   IN_B/C/1/60     {LINCAT}
WRITE/KEY   INPUTR/R/1/2    {LWCL(2)}
WRITE/KEY   OUT_A/C/1/60    middummk.bdf
WRITE/KEY   INPUTI/I/1/1    400
WRITE/KEY   INPUTD/D/1/2    -5.,0.05
inputr(2) = 1.

SET/MIDAS OUTPUT=NO
WRITE/KEY   PROGSTAT/I/1/1   0
RUN STD_EXE:spematch  
IF PROGSTAT(1) .NE. 0  GOTO EXIT ! Problems in matching loop
SET/MIDAS OUTPUT=YES

!KB SELECT/TAB       {LINTAB} (:IDENT.NE.0.0)  {SESSOUTV}
SELECT/TAB       {LINTAB} (ABS(:IDENT).GT.0.00001)  {SESSOUTV}

NBR(2) = OUTPUTI(1)
WRITE/OUT  "*** Number of identifications: {OUTPUTI(1)}"

IF WLCVISU(1:1) .EQ. "Y" THEN
   CLEAR/CHAN       OVER
   LOAD/TAB         {LINTAB}  :X  :YNEW  {SESSOUTV}
ENDIF

STAT/TABLE       {LINTAB}  :RESIDUAL  {SESSOUTV}
RMS(2)   = OUTPUTR(4)
PIXEL(3) = M$ABS(RMS(2)/PIXEL(1))

! Tests whether one stops the iteration
IF ITER .EQ. NITER  GOTO ENDIT
ITER = ITER + 1
IF ITER .GT. 1 THEN
  rmsratio = M$ABS(1. - rms(2)/rms(1))
  IF NBR(2) .LT. NBR(1) .OR. RMSRATIO .LT. 0.01 THEN
     IF ITER .GT. WLCNITER(1) GOTO  ENDIT
  ENDIF
ENDIF
NBR(1) = NBR(2)
RMS(1) = RMS(2)

WRITE/OUT  "*** STD. DEV. {RMS(2)} wavelength units, i.e. {PIXEL(3)} pixels  ***"

IF PIXEL(3) .GT. LWCL(3)  THEN
   WRITE/OUT  "Sorry, wrong identifications..."
   RETURN/EXIT
ENDIF

COMPUTE/TABLE   {LINTAB}   :AUX = 0.
COMPUTE/TABLE   {LINTAB}   :AUX = :ORDER * :IDENT
!KB SELECT/TABLE    {LINTAB}   (:IDENT.NE.0.0) {SESSOUTV}
SELECT/TAB       {LINTAB} (ABS(:IDENT).GT.0.00001)  {SESSOUTV}


IF NBR(2) .GT. NBRMIN THEN
     WRITE/KEY  WLCMTD  TWO-D
ENDIF

IF WLCMTD(1:1) .EQ. "P"  REGRES/POLY  {LINTAB} :AUX :XROT      {DEGREE} KEYLONG {SESSOUTV}
IF WLCMTD(1:1) .EQ. "A"  REGRES/POLY  {LINTAB} :AUX :XROT      {DEGREE} KEYLONG {SESSOUTV}

IF WLCMTD(1:1) .EQ. "L"  REGRES/POLY  {LINTAB} :AUX :X,:ORDER  {DEGREE},{DEGREE} KEYLONG {SESSOUTV}
IF WLCMTD(1:1) .EQ. "G"  REGRES/POLY  {LINTAB} :AUX :X,:ORDER  {DEGREE},{DEGREE} KEYLONG {SESSOUTV}
IF WLCMTD(1:1) .EQ. "T"  REGRES/POLY  {LINTAB} :AUX :X,:ORDER  {DEGREE},{DEGREE} KEYLONG {SESSOUTV}

IF WLCMTD(1:1) .EQ. "C"  REGRES/POLY  {LINTAB} :IDENT  :YNEW   {DEGREE} KEYLONG {SESSOUTV}

SAVE/REGRES       {LINTAB}   REGR  KEYLONG

GOTO ITERAT

ENDIT:

SET/FORMAT
RETURN

!*****************************************************************************

ENTRY ANALYS

   SELECT/TABLE  {LINTAB}  (:SELECT.GT.1) {SESSOUTV}
   WRITE/KEY  IN_A  {LINTAB}
   WRITE/KEY  IN_B  {WLCMTD}
   inputr(2) = 1.
   RUN STD_EXE:necwlcopt
   IF OUTPUTI(1) .NE. 0  RETURN/EXIT

RETURN

!************************************************************************

ENTRY REENTER

IF WLCMTD(1:1) .EQ. "P" THEN
  WRITE/OUT "Please identify the pair again, in the same order"
  WRITE/OUT "as you did it the first time."
ELSE
  WRITE/OUT "Please identify line {line} again."
ENDIF

CENTER/MOMENT CURSOR &c

COMPUTE/TABLE  middummc   :XERR = ABS(:XEND - :XSTART)/2.
COMPUTE/TABLE  middummc   :YERR = ABS(:YEND - :YSTART)/2.

SELECT/TABLE   middummc   (ABS(:STATUS).LT.0.01)  {SESSOUTV}

COPY/TAB       &c   &d

DEFINE/LOCAL   LINPAIR/I/1/1  0   ? +lower 
IF line .le. 2 THEN
          linpair = 1
ELSE
          linpair = 3
ENDIF

IF WLCMTD(1:1) .EQ. "P" THEN
   WRITE/TABLE &r :X  @{linpair}  {&d,:XCEN,@1}   
   WRITE/TABLE &r :Y  @{linpair}  {&d,:YCEN,@1}   
   WRITE/TABLE &r :DX @{linpair}  {&d,:XERR,@1}   
   WRITE/TABLE &r :DY @{linpair}  {&d,:YERR,@1}   
   linpair = linpair + 1
   WRITE/TABLE &r :X  @{linpair}  {&d,:XCEN,@2}   
   WRITE/TABLE &r :Y  @{linpair}  {&d,:YCEN,@2}   
   WRITE/TABLE &r :DX @{linpair}  {&d,:XERR,@2}   
   WRITE/TABLE &r :DY @{linpair}  {&d,:YERR,@2}   
ELSE
   WRITE/TABLE &r :X  @{line}  {&d,:XCEN,@1}   
   WRITE/TABLE &r :Y  @{line}  {&d,:YCEN,@1}   
   WRITE/TABLE &r :DX @{line}  {&d,:XERR,@1}   
   WRITE/TABLE &r :DY @{line}  {&d,:YERR,@1}   
ENDIF

RETURN


!***********************************************************************

ENTRY HOUGH

define/local    factor/D/1/1  0.
define/local    convert/I/1/1 0
define/local    ymean/R/1/1   0.
define/local    rms/R/1/1     0.
define/local    slop/D/1/1    0.

convert = relord + absord

echord(6) = convert
echord(2) = convert - echord(1) 
echord(3) = convert - 1

ordpos(1) = echord(3)
ordpos(2) = echord(2)

copy/kd ordpos/I/1/2 {lintab} ORDER/I/1/2

compute/table {LINTAB}  :ORDER = {CONVERT} - :Y

SELECT/TABLE {LINTAB} :Y.EQ.{relord} {SESSOUTV}

stat/table   {LINTAB} :YNEW  {SESSOUTV}
ymean = outputr(3)

dispersion/hough ? ? {IMSIZE(1)},{{WLC},START(1)},{{WLC},STEP(1)}  {LINTAB}  {SESSOUTV}

COMPUTE/TABLE   {LINTAB}  :AUX  = :WAVEC*:ORDER
SELECT/TABLE    {LINTAB}  :Y.EQ.{relord}  {SESSOUTV}
REGRESS/POLY    {LINTAB}  :AUX  :X  1     {SESSOUTV}

slop = outputd(2)
rms = lwcl(4)*slop
rms = rms*rms

! First estimate on the error on the angle = 1. degree
factor = slop*M$TAN(1)

SAVE/REGR       {LINTAB}  REGR

COMPUTE/TABLE   {LINTAB}  :ERROR = (:YNEW-{YMEAN})*{FACTOR}
COMPUTE/TABLE   {LINTAB}  :ERROR = SQRT(:ERROR*:ERROR + {RMS})
COMPUTE/TABLE   {LINTAB}  :ERROR = :ERROR/:ORDER

stat/tab {lintab} :ERROR  {SESSOUTV}
write/out  "Error range: {outputr(1)} to {outputr(2)} wav. units"

SELECT/TABLE {LINTAB} ALL
pixel(1) = avdisp

RETURN

! ******************************************************************

ENTRY HTITER

DEFINE/LOCAL ITER/I/1/1   0              ? +lower 
DEFINE/LOCAL NITER/I/1/1  {WLCNITER(2)}  ? +lower 
DEFINE/LOCAL NINDEP/I/1/1 0              ? +lower 
DEFINE/LOCAL PASS/I/1/1   0              ? +lower 
define/local rmsratio/R/1/1  0.

WRITE/OUT ".. Iterative Identification:"

IF WLCVISU(1:1) .EQ. "Y" THEN
   graph/spec
   set/graph
   set/graph pmode=1
ENDIF

HTITER:

COMPUTE/REGRES  {LINTAB}  :WAVEC =  REGR
COMPUTE/TABLE   {LINTAB}  :WAVEC = :WAVEC/:ORDER

SET/FORMAT F12.5  I1

COMPUTE/TABLE   {LINTAB}  :IDENT  = 0.
COMPUTE/TABLE   {LINTAB}  :SELECT = 0.

WRITE/KEY   IN_A/C/1/60     {LINTAB}
WRITE/KEY   IN_B/C/1/60     {LINCAT}
WRITE/KEY   INPUTR/R/1/2    {LWCL(2)}
WRITE/KEY   OUT_A/C/1/60    middummk.bdf
WRITE/KEY   INPUTI/I/1/1    400
WRITE/KEY   INPUTD/D/1/2    -5.,0.05
inputr(2) = 1.

SET/MIDAS OUTPUT=NO
WRITE/KEY   PROGSTAT/I/1/1   0
RUN STD_EXE:spematch  
IF PROGSTAT(1) .NE. 0  GOTO EXIT ! Problems in matching loop
SET/MIDAS OUTPUT=YES

STAT/TABLE {LINTAB} :RESIDUAL
RMS(2)   = OUTPUTR(4)*2./(echord(2)+echord(3))
PIXEL(3) = RMS(2)/PIXEL(1)
WRITE/OUT  "*** Matching: STD. DEV. {RMS(2)} wavelength units, i.e. {PIXEL(3)} pixels  ***"

IF WLCVISU(1:1) .EQ. "Y" THEN
  PLOT &k
  PLOT/HISTO {LINTAB} :RESIDUAL 
  select/table  {LINTAB} :IDENT.GT.0.0
  PLOT/TABLE {LINTAB} :IDENT :RESIDUAL
ENDIF

!KB SELECT/TAB       {LINTAB} (:IDENT.NE.0.0)  {SESSOUTV}
SELECT/TAB       {LINTAB} (ABS(:IDENT).GT.0.00001)  {SESSOUTV}

NBR(2) = OUTPUTI(1)
WRITE/OUT  "*** Iteration: {ITER}. Number of identifications: {OUTPUTI(1)}"

IF WLCVISU(1:1) .EQ. "Y" THEN
   !KB SELECT/TAB       {LINTAB} (:IDENT.NE.0.0)  {SESSOUTV}
   SELECT/TAB       {LINTAB} (ABS(:IDENT).GT.0.00001)  {SESSOUTV}

   CLEAR/CHAN       OVER
   LOAD/TAB         {LINTAB}  :X  :YNEW  {SESSOUTV}
ENDIF

COMPUTE/TABLE   {LINTAB}   :AUX = :ORDER * :IDENT
SELECT/TAB      {LINTAB} (:IDENT.GT.0.0)  {SESSOUTV}

IF NBR(2) .GT. NBRMIN THEN
     WRITE/KEY  WLCMTD  TWO-D
ENDIF


! Robust Regression PROGRESS
compute/table   {LINTAB} :X2  = :X*:X
compute/table   {LINTAB} :X3  = :X*:X*:X
compute/table   {LINTAB} :O2  = :ORDER*:ORDER
compute/table   {LINTAB} :O3  = :O2*:ORDER
compute/table   {LINTAB} :O4  = :O2*:O2
compute/table   {LINTAB} :O3X = :O3*:X
compute/table   {LINTAB} :O3X2= :O3*:X2
compute/table   {LINTAB} :OX  = :ORDER*:X
compute/table   {LINTAB} :O2X  = :O2*:X
compute/table   {LINTAB} :OX2  = :ORDER*:X2
compute/table   {LINTAB} :O2X2 = :O2*:X2
!compute/table   {LINTAB} :PIXFRAC = :X - INT(:X)

select/table    {LINTAB} :IDENT.GT.0.0 {SESSOUTV}
regress/robust  {LINTAB} :AUX :ORDER,:X,:X2,:OX,:O2,:O2X,:O3,:O3X,:O4,:X3 ? :AUX  {SESSOUTV}

!Computes :WAVEC for all cases
!set/format F15.5
!COMPUTE/TABLE {LINTAB} :WAVEC = {OUTPUTR(1)}*:ORDER+ {OUTPUTR(2)}*:X+ {OUTPUTR(3)}*:X2+ {OUTPUTR(4)}*:X3
!COMPUTE/TABLE {LINTAB} :WAVEC = :WAVEC + {OUTPUTR(5)}*:OX + {OUTPUTR(6)}*:O2 + {OUTPUTR(7)}*:O2X
!COMPUTE/TABLE {LINTAB} :WAVEC = :WAVEC + {OUTPUTR(8)}*:OX2 + {OUTPUTR(9)}*:O2X2 + {OUTPUTR(10)}

! Determines median of residuals

select/table  {lintab} :IDENT.GT.0.0

IF WLCVISU(1:1) .EQ. "Y" THEN
  set/graph colour=1 pmode=1
  PLOT/TABLE {LINTAB}  :IDENT :RESIDUAL
ENDIF

copy/table    {LINTAB} &l
compute/table &l :R2 = ABS(:RESIDUAL)
sort/table    &l :R2
inputi(1)  = {middumml.tbl,TBLCONTR(4)}/2
outputr(4) = {middumml.tbl,:R2,@{inputi(1)}}
RMS(2)     = OUTPUTR(4)*2.5 

SELECT/TABLE {LINTAB} :IDENT.GT.0.0.AND.ABS(:RESIDUAL).LT.{RMS(2)} {SESSOUTV}

IF WLCVISU(1:1) .EQ. "Y" THEN
  select/table {lintab} .NOT.SELECT
  set/graph colour=4
  OVER/TABLE {lintab} :IDENT :RESIDUAL
  select/table {lintab} .NOT.SELECT
  set/graph colour=0
ENDIF


! Measures filtered dispersion 
STAT/TABLE   {LINTAB} :RESIDUAL {SESSOUTV}
RMS(2)   = OUTPUTR(4)*2./(echord(2)+echord(3))
COMPUTE/TABLE   {LINTAB}    :ERROR = {OUTPUTR(4)}
COMPUTE/TABLE   {LINTAB}    :ERROR = :ERROR/:ORDER
PIXEL(3) = RMS(2)/PIXEL(1)
WRITE/OUT  "*** Rejection: STD. DEV. {RMS(2)} wav. units, i.e. {PIXEL(3)} pixels  ***"

!Computes wavelength by polynomial interpolation
!IF WLCMTD(1:1) .EQ. "P"  REGRES/POLY  {LINTAB} :AUX :XROT      {DEGREE} KEYLONG {SESSOUTV}
!IF WLCMTD(1:1) .EQ. "A"  REGRES/POLY  {LINTAB} :AUX :XROT      {DEGREE} KEYLONG {SESSOUTV}
!IF WLCMTD(1:1) .EQ. "L"  REGRES/POLY  {LINTAB} :AUX :X,:ORDER  {DEGREE},{DEGREE} KEYLONG {SESSOUTV}
!IF WLCMTD(1:1) .EQ. "G"  REGRES/POLY  {LINTAB} :AUX :X,:ORDER  {DEGREE},{DEGREE} KEYLONG {SESSOUTV}
!IF WLCMTD(1:1) .EQ. "T"  REGRES/POLY  {LINTAB} :AUX :X,:ORDER  {DEGREE},{DEGREE} KEYLONG {SESSOUTV}

REGRES/POLY  {LINTAB} :AUX :X,:ORDER  {DEGREE},{DEGREE} KEYLONG {SESSOUTV}
SAVE/REGR    {LINTAB} REGR  KEYLONG {SESSOUTV} 

! Tests whether one stops the iteration
IF ITER .GT. NITER  THEN
   WRITE/OUT "Reached maximal number of iterations WLCNITER(2)={WLCNITER(2)}"
   GOTO HTENDIT
ENDIF

ITER = ITER + 1
IF ITER .GT. 1 THEN
  rmsratio = M$ABS(1. - rms(2)/rms(1))
  IF NBR(2) .LT. NBR(1) .OR. RMSRATIO .LT. 0.01 THEN
       IF ITER .GT. WLCNITER(1) GOTO  HTENDIT
  ENDIF
ENDIF
NBR(1) = NBR(2)
RMS(1) = RMS(2)


IF PIXEL(3) .GT. LWCL(3)  THEN
   WRITE/OUT  "Sorry, wrong identifications..."
   RETURN/EXIT
ENDIF

GOTO HTITER

HTENDIT:

SET/FORMAT

RETURN


!***************************************************************************

ENTRY VERIF

DEFINE/LOCAL RATIO/R/1/1  0. ? +lower 
DEFINE/LOCAL BRIGHT/I/1/1 0  ? +lower 
DEFINE/LOCAL SELECT/I/1/1 0  ? +lower 
DEFINE/LOCAL MINSEL/I/1/1 0  ? +lower 

SELECT/TABLE {LINTAB}  ALL
COPY/TI      {LINTAB}  &h
STATIST/IMA  &h  [<,@3:>,@3] OPTION=HN

SELECT/TABLE {LINTAB} :PEAK.GT.{OUTPUTR(8)} {SESSOUTV}  ! OUTPUTR(8) = Median
BRIGHT = OUTPUTI(1)

SELECT/TABLE {LINTAB} SELECT.AND.:SELECT.GE.1 {SESSOUTV}
SELECT = OUTPUTI(1)

SELECT/TABLE {LINTAB}  ALL
MINSEL = DEGREE + 2
RATIO  = 100.*SELECT/BRIGHT

SET/FORMAT  I1  F3.0

WRITE/OUT "**************************  Verification  ***********************************"
WRITE/OUT "     "
WRITE/OUT "1) Minimum number of selections per order : {MINSEL}"
WRITE/OUT "   If the number of selections in any order (column NO.LINES above)"
WRITE/OUT "   is less or equal than the minimum, this order should be checked."
WRITE/OUT "        "
WRITE/OUT "2) Percentage of identifications among the half brighter lines : {RATIO} %"
WRITE/OUT "   This percentage must be as high as possible (above 50%). Low values"
WRITE/OUT "   indicate an uncertain calibration."
WRITE/OUT "        "

SET/FORMAT
RETURN


!**********************************************************************

ENTRY MAPRES

compute/table {LINTAB} :RES=:RESIDUAL * :RESIDUAL
select/table {LINTAB} :IDENT.GT.0.0
convert/table &l = line.tbl :X,:YNEW :RES {WLC} POLY 1,1

select/table {LINTAB}  all

COMPUTE/TABLE  {LINTAB}  :XSTA = :X - 1
COMPUTE/TABLE  {LINTAB}  :XEND = :X + 1
COMPUTE/TABLE  {LINTAB}  :YSTA = :YNEW - 1
COMPUTE/TABLE  {LINTAB}  :YEND = :YNEW + 1
COMPUTE/TABLE  {LINTAB}  :YBKG = :YNEW

WRITE/KEY IN_A/C/1/60  {LINTAB}
WRITE/KEY IN_B/C/1/60  middumml.bdf
WRITE/KEY OUT_A/C/1/60 middummi.bdf

WRITE/KEY INPUTI/I/1/2 1,-10

RUN STD_EXE:NECBACK

compute/table {LINTAB} :ERROR = 2.*SQRT(:BKG)

dele/col {lintab} :XSTA
dele/col {lintab} :YSTA
dele/col {lintab} :XEND
dele/col {lintab} :YEND
dele/col {lintab} :BKG
dele/col {lintab} :RES

RETURN

