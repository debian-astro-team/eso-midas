! @(#)necrebi.prg	19.1 (ES0-DMD) 02/25/03 14:19:41
! +++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  ECHELLE PROCEDURE : ECHREBI.PRG
!  J.D.Ponz			version 2.0 131083
!
! .PURPOSE
!
!  execute the command :
!  REBIN/ECHELLE INPUT OUTPUT REFERENCE_IMAGE [METHOD] [TABLE]
!  OR
!  REBIN/ECHELLE INPUT OUTPUT [DW] [METHOD] [TABLE]
! ---------------------------------------
!
DEFINE/PARAM P1     ?     IMAGE   "Enter input image"
DEFINE/PARAM P2     ?     IMAGE   "Enter output image"
DEFINE/PARAM P3  {SAMPLE}   ?     "Sample or reference image:"
DEFINE/PARAM P4  {REBMTD}   C     "Method: LINEAR, NONLINEAR"
DEFINE/PARAM P5  {LINTAB}  TABLE  "Auxiliary table"
!
! Case unsensitivity and check method
!
DEFINE/LOCAL METHOD/C/1/12  "YYYYYYYYYYYY"
IF P4(1:1) .EQ. "L"   THEN
   WRITE/KEYW METHOD/C/1/12 "LINEAR      "
   WRITE/OUT "REBIN/ECHELLE: Sorry, method LINEAR is not supported yet"
   RETURN/EXIT
ENDIF
IF P4(1:1) .EQ. "N"   WRITE/KEYW METHOD/C/1/12 "NONLINEAR   "
IF METHOD(1:1) .EQ. "Y" THEN
   WRITE/OUT "REBIN/ECHELLE: unknown method {P4}"
   RETURN/EXIT
ENDIF
!
IF P3(1:1) .EQ. "+"  ERROR/ECHELLE  REBIN/ECHELLE  SAMPLE
!
SET/ECH  REBMTD={METHOD}  SAMPLE={P3}
!
IF REBMTD(1:1) .EQ. "N"  THEN
    VERIFY/ECHELLE {P1}  EXTR
ELSE
    VERIFY/ECHELLE {P1}  REBIN
ENDIF
!
WRITE/KEYW   IN_A           {P1}
WRITE/KEYW   IN_B           {P5}  ! Auxiliary table
WRITE/KEYW   OUT_A          {P2}
WRITE/KEYW   P4/C/1/12      {METHOD}
!
RUN STD_EXE:NECREBI
!
! Descriptors transfer is performed in the program. New descriptors 
! WSTART and NPTOT are created.
!
RETURN


