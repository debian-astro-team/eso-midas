! @(#)necsord.prg	19.1 (ES0-DMD) 02/25/03 14:19:43
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echsord.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command SEARCH/ORDER [image] [w,t,s] [method] [table]
!.VERSION     1.0    Creation    29-JUL-1991  PB
!
!-------------------------------------------------------
!
DEFINE/PARAM P1 {ORDREF}                  IMAGE  "Enter input image:"
DEFINE/PARAM P2 {WIDTH1},{THRES1},{SLOPE}  N     "Enter window,threshold,slope:"
DEFINE/PARAM P3 {DEFMTD}                   C     "Method(STD,COM):"
DEFINE/PARAM P4 {ORDTAB}                  TABLE  "Output table"
!
SET/ECH ORDREF={P1}  DEFMTD={P3}
DEFINE/LOCAL K/R/1/3 'P2'
WIDTH1 = K(1)
THRES1 = K(2)
SLOPE  = K(3)
!
DEFINE/LOCAL    ERRFLAG/I/1/1  1
IF DEFMTD(1:1) .EQ.  "S"   ERRFLAG = 0   ! Method STD
IF DEFMTD(1:1) .EQ.  "C"   ERRFLAG = 0   ! Method COM
IF ERRFLAG .NE. 0  ERROR/ECHELLE SEARCH/ORDER  DEFMTD
IF WIDTH1  .LE. 0  ERROR/ECHELLE SEARCH/ORDER  WIDTH1
IF SLOPE   .LT. 0  ERROR/ECHELLE SEARCH/ORDER  SLOPE

WRITE/KEYW   IN_A/C/1/60  {ORDREF}
WRITE/KEYW   IN_B/C/1/60  {ORDTAB}
COPY/KEYW   K/R/1/3      INPUTR/R/1/3 
RUN STD_EXE:NECNORD

ECHORD(1)  =  OUTPUTI(1)  ! OUTPUTI/I/2/2 contains size of frame
ECHORD(2)  =  1
ECHORD(3)  =  OUTPUTI(1)  ! Number of first and last order.

RETURN

