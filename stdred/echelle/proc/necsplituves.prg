! @(#)necsplituves.prg	19.1 (ESO-IPG) 02/25/03 14:19:43
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       necsplituves.prg
!.AUTHOR      Andrea Modigliani
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Split and rotation of UVES spectra
!.DESCRIPTION
!  MIDAS procedure to reformat UVES Echelle Spectra Images:!
!  This procedure:
!
!  loads a FITS image
!  cuts it in two components, one corresponding to each UVES detector side
!
!  sets uves context
!  for each component:
!
!   rotates each component clockwise 90 degrees
!   saves each component on two different assigned names
!   loads each image
!   inits echelle package
!   applies to each image the hough transformation
!   loads echelle
!   saves it on an assigned name
!   save the defined order on an assigned filename
!   applies the wavelength calibration
!
!
!.VERSION     1.0    Creation    05.10.98
!-------------------------------------------------------
!
DEFINE/PARAMETER  p1 ?  c "Set the FITS image name whithout fits extension (ima.fits=> ima): "

DEFINE/PARAMETER  p2 {P1} c  "Set the BDF  image root name " 
DEFINE/PARAM      p3 NO      c  "Display option"

DEFINE/LOCAL half_x_size/D/1/1 0.

INTAPE/FITS 1 {p2} {p1}.fits
if p3(1:1) .eq. "Y"  @ creifnot 2 heat

if p3(1:1) .eq. "y" LOAD/IMAGE    {p2}0001.bdf scale=-10,-10

half_x_size = {{p1}0001.bdf,NPIX(1)} / 2

!first image part
EXTRACT/IMAGE {p2}_low.bdf = {p2}0001.bdf [{half_x_size}+1,<:>,>]
if p3(1:1) .eq. "y" LOAD/IMAGE    {p2}_low.bdf scale=-10,-10
ROTATE/CLOCK {p2}_low.bdf {p2}_low_r.bdf
FLIP/IMAGE {p2}_low_r.bdf X
WRITE/DESCRIPTOR {p2}_low_r.bdf start/D/1/2, 1,1
WRITE/DESCRIPTOR {p2}_low_r.bdf step/D/1/2, 1,1

!second image part
EXTRACT/IMAGE {p2}_upp.bdf = {p2}0001.bdf [<,<:{half_x_size},>]
if p3(1:1) .eq. "y" LOAD/IMAGE    {p2}_upp.bdf scale=-10,-10
ROTATE/CLOCK {p2}_upp.bdf {p2}_upp_r.bdf
FLIP/IMAGE {p2}_upp_r.bdf X
WRITE/DESCRIPTOR {p2}_upp_r.bdf start/D/1/2, 1,1
WRITE/DESCRIPTOR {p2}_upp_r.bdf step/D/1/2, 1,1

!comment-out the rest 
if p3(1:1) .eq. "y" LOAD/IMAGE    {p2}_low_r.bdf scale=-10,-10
if p3(1:1) .eq. "y" LOAD/IMAGE    {p2}_upp_r.bdf scale=-10,-10

$rm {p2}_low.bdf {p2}_upp.bdf {p2}0001.bdf











