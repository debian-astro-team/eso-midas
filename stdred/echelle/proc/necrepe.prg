! @(#)necrepe.prg	19.1 (ES0-DMD) 02/25/03 14:19:42
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echrepe.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command REPEAT/ECHELLE
!.VERSION     1.0    Creation    
!.VERSION     1.1    `\` ==> ` ',  KB
!
!-------------------------------------------------------
!
DEFINE/PARAM  P1   -2,20         N   "Scaling factors:"
DEFINE/PARAM  P2   {RESPONSE}   IMA  "Response function:"

DEFINE/LOCAL  SCALE/I/1/2  {P1}
DEFINE/LOCAL  CUTS/C/1/30  0.,1000.

VERIFY/ECHELLE  {P2}   REBIN

IF SCALE(2) .LT. 0   THEN
   WRITE/OUT  "REPEAT/ECHELLE: Y-scaling factor cannot be negative."
   RETURN/EXIT
ENDIF


@a scaler {P2}  middummp  {SCALE(1)},{SCALE(2)}
WRITE/DESCR &p  START   1.,1.
WRITE/DESCR &p  STEP    1.,1.
LOAD  &p  scale=1,1

CUTS:
INQUIRE/KEYW ANSW/C/1/3  "Loaded scaled image. Cuts are O.K  (y/n,def=y) ? "

IF AUX_MODE(7) .NE. 0  THEN
IF ANSW(1:1) .EQ. "N" THEN
   WRITE/KEYW    CUTS/C/1/30  " "  ALL
   INQUIRE/KEYW  CUTS/C/1/30  "Enter new cuts (e.g. 0.,5000.) : "
   LOAD &p cuts={CUTS}
   GOTO CUTS
ENDIF
ENDIF

WRITE/OUT    "Use command MODIFY/AREA to modify image middummp"
MODIFY/AREA  ?   middummq

INQUIRE/KEYW   ANSW/C/1/3  "Do you accept the modifications (y/n,def=y):"

IF AUX_MODE(7) .EQ. 0  ANSW(1:3) = "YES"

IF ANSW(1:1) .NE. "N" THEN

   SCALE(1) = -SCALE(1)
   SCALE(2) = -SCALE(2)

   @a scaler middummq  middummr   {SCALE(1)},{SCALE(2)}
   COPY/DD {P2} START/D/1/2  &r  START/D/1/2
   COPY/DD {P2} STEP/D/1/2   &r  STEP/D/1/2
   COPY/II &r {P2}

ENDIF

WRITE/OUT       OUTPUT RESPONSE
WRITE/OUT       "---------------"
READ/DESC       {P2}
WRITE/OUT       "------------------------------------------------------"
WRITE/OUT       "Check if RESPONSE looks reasonable..."
WRITE/OUT       "To correct it use again REPEAT/ECHELLE"
WRITE/OUT       "------------------------------------------------------"
WRITE/OUT       "Apply RESPONSE to Standard Star"
WRITE/OUT       "using command REDUCE/ECHE {STD}  test"



