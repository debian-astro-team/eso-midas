! @(#)neccalmap.prg	19.1 (ESO-IPG) 02/25/03 14:19:35
!
! This procedures makes order definition and wavelength calibration
! from the wavelength projection map.
!
define/param P1 {WLC}  IMA
define/param P2 mapss  CHAR "Name of the resulting session"

verify/echelle {P1}

create/table {ORDTAB} 3 {npline.tbl,TBLCONTR(4)}
copy/tt npline :ORDER {ORDTAB} :ORDER
copy/tt npline :XMOD  {ORDTAB} :X
copy/tt npline :YMOD  {ORDTAB} :Y
define/local  relord/I/1/1  {{ORDTAB},:ORDER,@1}
compute/table {ORDTAB} :ORDER = {relord} + 1 - :ORDER
copy/dd    {P1} START/D/1/2  {ORDTAB} defstart/D/1/2
copy/dd    {P1} START/D/1/2  {ORDTAB} defstep/D/1/2

sort/table {ORDTAB}  :ORDER,:X 
regress/echelle
load/echelle
prepare/background ? INI

create/table {LINTAB} 3 {npline.tbl,TBLCONTR(4)}
copy/tt npline :XMOD  {LINTAB} :X
copy/tt npline :ORDER {LINTAB} :Y
copy/tt npline :ORDER {LINTAB} :PEAK
copy/tt npline :IDENT {LINTAB} :IDENT
copy/tt npline :YMOD  {LINTAB} :YNEW
copy/tt npline :ORDER {LINTAB} :ORDER
copy/tt npline :IDENT {LINTAB} :WAVEC

compute/table {LINTAB} :AUX = :ORDER*:IDENT

define/local disprel/D/1/3 0,0,0
regress/POLY {LINTAB} :AUX :X  3 
copy/kk OUTPUTD/D/1/3  disprel/D/1/3
stat/table {LINTAB} :ORDER
write/descr  {LINTAB} ORDER/I/1/2  {outputr(2)},{outputr(1)}

set/format E25.16

compute/table {LINTAB} :PIXEL = ({disprel(2)} + 2*{disprel(3)}*:X)/:ORDER
stat/table    {LINTAB} :PIXEL
set/echelle   avdisp={outputr(3)}

write/descr   {LINTAB} PIXEL/R/1/1 {avdisp}

set/format
set/format F12.4
write/out "Average pixel size: {avdisp} wav. units"

REGRESS/POLY {LINTAB} :ORDER :X,:YNEW 0004,0004 KEYLONG
SAVE/REGR    {LINTAB} RORD KEYLONG

save/echelle  {P2}
set/echelle   wlcmtd=guess guess={P2} 

write/out "You can now extract a Th-Ar frame (EXTRACT/ECHELLE), search it for lines"
write/out "(SEARCH/ECHELLE), calibrate in guess mode (IDENT/ECHELLE) using session {P2}"


