! @(#)necclean.prg	19.1 (ES0-DMD) 02/25/03 14:19:35
! @(#)necclean.prg	19.1  (ESO)  02/25/03  14:19:35
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       echclean.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command CLEAN/ECHELLE
!.VERSION     1.0    Creation    25-MAR-1994 PB
!
!-------------------------------------------------------
!
DELETE/TABLE {LINTAB} NO
DELETE/TABLE {ORDTAB} NO
DELETE/TABLE {BAKTAB} NO

CLEAR/CONTEXT
CLEAR/CONTEXT

RETURN



