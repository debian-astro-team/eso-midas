! @(#)necdefsky.prg	19.1 (ES0-DMD) 02/25/03 14:19:36
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       necdefsky.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, Sky definition
!.PURPOSE     Defines sky offset by cursor pointing
!             DEFINE/SKY input [nsky] [order,column] [width]
!.VERSION     1.0    Creation    23.08.94 PB-ESO/DMD
!-------------------------------------------------------
!
SET/FORMAT I1 F20.12

DEFINE/PARAM P1 ?  IMA      "Image name:"
DEFINE/PARAM P2 {NSKY} NUMB "Number of sky windows:"
DEFINE/PARAM P3 {POSSKY} ?  "Position definition:"
DEFINE/PARAM P4 1.00   NUMB "Half-width plotted:"

define/local meanpos/R/1/2 0.,0.
define/local order/I/1/1   0       ? +lower
define/local column/I/1/1  0       ? +lower
define/local wpos/D/1/2    0.,0.   ? +lower
define/local staep/D/1/2   0.,0.   ? +lower
define/local grlims/R/1/2  0.,0.
define/local nwind/I/1/2   {P2},0  ? +lower
define/local cnt/I/1/1     0

@s necdefsky,range nwind
verify/echelle {P1} IMA

nwind(2) = nwind(1)*2
copy/DK {P1} START/D/1/1 staep/D/1/1
copy/DK {P1} STEP/D/1/1  staep/D/2/1 

if P3(1:1) .EQ. "A" then
   order  = echord(1)/2
   column = {{P1},NPIX(1)}/2
else
   IF P3(1:1) .EQ. "C" THEN
     @s necdefsky,cursor {P1}
   ELSE
     cnt = M$TSTNO("P3")
     if cnt .eq. 1 then
       WRITE/KEYW INPUTI/I/1/2 {P3}
       order  = INPUTI(1)
       column = INPUTI(2)
     else
       write/out "Error: Invalid parameter POSSKY={P3}"
       write/out "       Accepted values: cursor, <order,column>, auto"
       return/exit
     endif
   ENDIF
endif

write/key  POSSKY {P3}  
write/key  nsky   {nwind}

write/out "Plotting {P1} at column {column} and relative order {order}..."

   wpos   = staep(1) + (column-1)*staep(2)
   select/table {ordtab} seq.le.3  {SESSOUTV}
   copy/table   {ordtab} &o 
   select/table {ordtab} all
   &o,:X,@1     = wpos(1)
   &o,:X,@2     = wpos(1)
   &o,:X,@3     = wpos(1)
   &o,:ORDER,@1 = order
   &o,:ORDER,@2 = order - {P4}
   &o,:ORDER,@3 = order + {P4}
   compute/regress &o :YFIT = COEFF
   wpos(2)      = {&o,:YFIT,@1}
   grlims(1)    = {&o,:YFIT,@2}
   grlims(2)    = {&o,:YFIT,@3}
   extract/image &e = {P1}[{wpos(1)},{grlims(1)}:{wpos(1)},{grlims(2)}]

   graph/spec
   set/graph
   set/graph pmode=1
   plot &e


write/out "   "
write/out "Graphic cursor definition of the sky limits:"
write/out "  Click on {nwind(2)} positions in the graphic window, in any order"
write/out "  These positions will be sorted by ascending row position"
write/out "  and will be used to determine the offsets of the sky limits"
write/out "   "

select:
GET/GCURSOR middummc.tbl,TABLE  P3={nwind(2)}

if {middummc.tbl,TBLCONTR(4)} .ne. nwind(2) then
   write/out "A number of {nwind(2)} positions need to be pointed."
   inquire/keyw answ/C/1/3 "Do you want to restart the selection (yes/no, def=yes)"
   if answ(1:1) .eq. "n" return/exit
   goto select
endif

sort/table &c :X_COORD

!set/ech skywind=0,0,0,0
write/key skywind 0,0,0,0

DO CNT = 1 {nwind(2)}
   skywind({cnt}) = {&c,:X_AXIS,@{cnt}} - wpos(2)
ENDDO

set/format F6.3
write/out "   "
write/out "Number of sky windows:            NSKY = {nsky}"
write/out "Sky offsets for window 1: SKYWIND(1-2) = {SKYWIND(1)},{SKYWIND(2)}"
if nsky .gt. 1 then
 write/out "Sky offsets for window 2: SKYWIND(3-4) = {SKYWIND(3)},{SKYWIND(4)}"
endif

RETURN

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ENTRY RANGE

define/param P1 ? CHAR "Keyword name:"

if {P1} .gt. 2 .or. {P1} .le. 0 then
   write/out "Warning: Valid range for parameter NSKY is [1,2]"
   write/out "         Use command DEFINE/SKY in [nsky] [order] [column] 
   write/out "         to set the parameter and sky windows interactively"
   write/out "         or command SET/ECHELLE NSKY=... SKYWIND=..."
   return/exit
endif

RETURN

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ENTRY CURSOR

load {P1}

write/out "Sky feature cursor definition: 
write/out "      Click in the display window on the central position"
write/out "      of an order."
GET/CURSOR middummc.tbl P4=1,1

write/out "Identifying order..."

select/table {ordtab} seq.le.{echord(1)} {SESSOUTV}
copy/table   {ordtab} &o 
select/table {ordtab} all

wpos(1) = {&c,:X_COORD,@1}
wpos(2) = {&c,:Y_COORD,@1}

column = (wpos(1)-staep(1))/staep(2) + 1

compute/table &o :X = {wpos(1)}
compute/table &o :ORDER = seq
compute/regr  &o :YFIT  = COEFF
compute/table &o :Y = ABS(:YFIT - {wpos(2)})
sort/table    &o :Y

order = {&o,:ORDER,@1}

Write/out "Found position: Relative order {order}, column {column}"

RETURN







