! @(#)necback.prg	19.1 (ESO-DMD) 02/25/03 14:19:35
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echback.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, Background
!.PURPOSE     Execute command BACKGROUND/ECHELLE
!.VERSION     1.0    Creation    27.06.90   PB
!             99.06.22-SW Parameter P7 added. Allows to take the MINIMUM
!			  instead of the MEDIAN within the box centered
!			  on the background position (back.tbl).
! 021213	last modif
!
!-------------------------------------------------------
!
DEFINE/PARAM P1 ?  IMAGE "Enter input image:"
DEFINE/PARAM P2 ?  IMAGE "Enter output background:"
DEFINE/PARAM P4 {BKGDEG}  N ! Degree of spline (method SPLINE only) 1,10
DEFINE/PARAM P5 {BKGSMO}  N ! Smoothing factor (method SPLINE only) 0.,1.0E+30
DEFINE/PARAM P6 {BKGMTD}  C ! Background computation method
DEFINE/PARAM P7 MEDIAN    C ! Measure value in box centered on backgr. position (MINIMUM, or MEDIAN)
!
! Test consistency of inputs. Follows a tricky way to write correct
! default values and to store parameter P3 depending on the method.
!
SET/ECH BKGDEG={P4}  BKGSMO={P5}  BKGMTD={P6}
DEFINE/LOCAL PAR3/I/1/3     0,0,0
DEFINE/LOCAL ERFLG/I/1/1   1
DEFINE/LOCAL MMODE/C/1/10

IF BKGMTD(1:2) .EQ. "SP"  THEN
   ERFLG = 0
   DEFINE/PARAM  P3  {BKGRAD(1)},{BKGRAD(2)},{BKGSTEP}   NUMBER 
   WRITE/KEYW         PAR3/I/1/3  {P3}
   SET/ECH BKGRAD={PAR3(1)},{PAR3(2)}
ENDIF

IF BKGMTD(1:1) .EQ. "P"  THEN
   ERFLG = 0 
   DEFINE/PARAM  P3  {BKGPOL(1)},{BKGPOL(2)},{BKGSTEP}   NUMBER
   WRITE/KEYW     PAR3/I/1/3   {P3}
   SET/ECH BKGPOL={PAR3(1)},{PAR3(2)}
ENDIF

IF ERFLG .NE. 0   ERROR/ECHELLE  BACKGROUND/ECHELLE  BKGMTD

BKGSTEP =  PAR3(3)
!
ERFLG = 0
IF PAR3(1) .LT. 0  ERFLG = 1
IF PAR3(2) .LT. 0  ERFLG = 1
IF BKGSTEP .LE. 0  ERFLG = 1
IF ERFLG .NE. 0  THEN
   WRITE/OUT  "BACKGR/ECHELLE: wrong input in parameter P3: ({PAR3(1)},{PAR3(2)},{PAR3(3)})"
   WRITE/OUT  "Please check documentation"
   RETURN/EXIT
ENDIF
VERIFY/ECHELLE {P1}
!
! Test method: POLY or SPLINE
!
PREPARE/BACK  {PAR3(3)}	            ! Prepares background table
COPY/TABLE {BAKTAB}   middummb.tbl  ! Copy back table into &b (for selection)
!
IF BKGMTD(1:1) .EQ. "P"   GOTO POLY
!
! Write keywords as input of ECHBACK
WRITE/KEYW IN_A/C/1/60  middummb.tbl
WRITE/KEYW IN_B/C/1/60  {P1}
WRITE/KEYW OUT_A/C/1/60 {P2}
!
INPUTI(1) = PAR3(2)		!instead of: WRITE/KEYW INPUTI/I/1/1 {PAR3(2)}
WRITE/KEYW INPUTR/R/1/1 {P5}
WRITE/KEYW INPUTI/I/2/1 {P4}
! Check the measure mode at background position:
MMODE = M$UPPER(P7)
IF MMODE(1:2) .EQ. "MI" THEN
  INPUTI(3) = 1
ELSE
  INPUTI(3) = 0
ENDIF
!
RUN STD_EXE:NECBACK
GOTO HISTO
!
! Polynomial method
!

POLY:
!
WRITE/OUT    "Bkg method POLY. Degree {BKGPOL(1)},{BKGPOL(2)} Step: {BKGSTEP}"
!
AVERAGE/TABLE {P1} middummb.tbl :X,:YBKG  :BKG  0
CONVERT/TABLE {P2} = middummb.tbl :X,:YBKG  :BKG  {P1} POLY {P3}
!
HISTO:
COPY/DD {P1}  *,3  {P2}
WRITE/DESCR {P2} HISTORY/C/-1/80  "BACKGR/ECHELLE   P(1-3) = {P1} {P2} {P3}"
WRITE/DESCR {P2} HISTORY/C/-1/80  "BACK/ECH (contd) P(4-6) = {P4} {P5} {P6}"

