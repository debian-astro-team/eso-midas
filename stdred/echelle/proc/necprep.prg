! @(#)necprep.prg	19.1 (ES0-DMD) 02/25/03 14:19:41
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echprep.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Prepares a set of file for AVERAGE/WINDOW
!             Syntax: PREPARE/WIND inputcatal.cat flat_bkg lhcuts
!.VERSION     1.0    Creation    24-OCT-1985 Eli Brinks
!.VERSION     920413  KB
!
!-------------------------------------------------------
!
DEFINE/PARAM P1 ?         ?    "Enter input catalogue:"
DEFINE/PARAM P2 0.     NUMBER  "Flat background"
DEFINE/PARAM P3 0.,0   NUMBER  "LHCUTS"
!
DEFINE/LOCAL TIME/D/1/1/    1.
DEFINE/LOCAL CATAL/I/1/2    0,0
DEFINE/LOCAL INPUT/C/1/40   INDUMMY
CLEAR/ICAT
WRITE/KEYW INPUT/C/1/40   " " ALL
!
! Start the loop over the input frames in the catalogue
!
LOOP:
STORE/FRAME   INPUT {P1}.cat  1  FINITO
WRITE/OUT input frame {INPUT}
!
! Fill the descriptors needed by AVERAGE/WINDOW
!
WRITE/DESCRIPTOR {INPUT}  FLAT_BKG/R/1/1 {P2}
WRITE/DESCRIPTOR {INPUT}  LHCUTS/R/5/2   {P3}
!
! Check exposure time
!
VERIFY/ECHELLE {INPUT} OTIME
GOTO LOOP
FINITO:
