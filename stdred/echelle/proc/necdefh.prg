! @(#)necdefh.prg	12.1 (ES0-DMD) 09/16/97 11:17:44
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echdef.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, Order detection, Hough transform
!.PURPOSE     - Automatic order detection by Hough Transform
!             - Cluster detection
!             - Order Following and creation of table ORDER
!.VERSION     1.0    Creation    29.04.91
!-------------------------------------------------------
!
CROSSREF ORDREF NBORDI  WIDTHI  HGMTD  THRESI  DEFPOL  HGPAR  HGSETUP
!
define/param   P1  {ORDREF} IMA     "Order Reference frame"
define/param   P2  {NBORDI}   N     "Number of orders"     
define/param   P3  {WIDTHI}   N     "Half-width:"         
define/param   P4  DENSE      C     "Step, nb of traces or ALL,CENTER,DENSE,NO,FOLLOW"
define/param   P5  {THRESI}   C     "Low Threshold or keyword names (integer + real)"
define/param   P6  {DEFPOL(1)},{DEFPOL(2)} N  "Degrees of bivariate polynomial"
define/param   P7  100000,10  N   "Hot thres, step"
!
! Parameter P8 is passed to HOUGH/ECHELLE without verification
!
define/local   NBHW/I/1/2   0,0
WRITE/KEYW      NBHW/I/1/2   {P2},{P3}
define/local   VALI/I/1/2   {P7}
define/local   FLAG/C/1/3   HMN
define/local   THRES/R/1/1  0
define/local   P4INT/C/1/20 {P4}
define/local   FILTER/C/1/1 Y        ! Flag for filtering (Yes/No)
define/local   start/D/1/2   0.,0.
define/local   step/D/1/2    0.,0.

WRITE/KEYW  ORDREF/C/1/60  {P1}
WRITE/KEYW  DEFPOL/I/1/2   {P6}
NBORDI  = {P2}
WIDTHI = {P3}
IF M$TSTNO(P5) .EQ. 1  THRESI = {P5}

IF NBORDI .LT. 0   NBHW(1) = M$ABS(NBORDI)

VERIFY/ECHELLE  {ORDREF}
DISPLAY/ECHELLE {ORDREF}

IF P4INT(1:1) .EQ. "-" THEN
   FILTER = "N"
   WRITE/KEYW P4INT/C/1/20 {P4INT(2:20)}
ENDIF

IF P4INT(1:1) .EQ. "+" THEN
   COPY/II {P4INT(2:)} &h
   COPY/DD {P4INT(2:)} *,3 &h
   WRITE/KEYW P4INT/C/1/2  "NO"
ENDIF

WRITE/OUT  "****    Order Definition    ****"
IF OUTMODE(1:1) .NE. "S" THEN
IF P4INT(1:1) .EQ. "F"   THEN
       WRITE/OUT  "No detection. Order detection read from middummr.tbl."
       WRITE/OUT  "Orders  are followed on the filtered frame middummi.bdf"
       GOTO FOLLOW
ELSE
       WRITE/OUT  "Order reference frame: ORDREF={ORDREF}"
       WRITE/OUT  "Preprocessed frame:    middummi.bdf"
       WRITE/OUT  "Hough transform:       midddumh.bdf"
       WRITE/OUT  "Orders detection:      middummr.tbl"
       WRITE/OUT  "Output tables:         ORDTAB={ORDTAB} and BAKTAB={BAKTAB}"
ENDIF
ENDIF

IF NBHW(2) .NE. 0     WRITE/KEYW  FLAG/C/1/3  HLN
IF P4INT(1:1) .EQ. "N"   THEN
     WRITE/KEYW  FLAG/C/1/1  N
ELSE
     IF FILTER(1:1) .EQ. "Y"   THEN
          WRITE/OUT "Preprocessing..."
          FILTER/MEDIAN  {ORDREF} &i  2,1 
     ELSE
          COPY/II {ORDREF} &i
     ENDIF
ENDIF

define/local   STNB/I/1/2   0,50    ! Number of columns processed

IF P4INT(1:1) .EQ. "D" THEN           ! Option DENSE
  STNB(1) = {{P1},NPIX(1)}/STNB(2)    ! Distance between columns
  WRITE/KEYW P4INT/C/1/20  {STNB(1)},{STNB(2)}
ENDIF

IF P4INT(1:1) .EQ. "C" THEN             ! Option CENTER
  STNB(1) = {{P1},NPIX(1)}/STNB(2)/4    ! Distance between columns
  WRITE/KEYW P4INT/C/1/20  {STNB(1)},{STNB(2)}
ENDIF

START:

HOUGH/ECH  &i  {SCAN(1)},{SCAN(2)} {P4INT} {NBHW(1)} {FLAG(1:3)}-
 {NBHW(2)} 3,0.5,{VALI(1)},1  {P8}

FOLLOW:

STAT/TABLE   &r  :FWHM  {SESSOUTV}
slit = outputr(3)*2.
write/out "Parameter slit set to: {slit} pixels"

CHECK:
COMPUTE/TABLE  &r  :SEQ = seq
!COMPUTE/TABLE  &r  :AUX = :ORIG + :SLOPE*{IMSIZE(1)}/2.
COMPUTE/TABLE  &r  :AUX = :ORIG 
REGRES/POLY    &r  :ORDER  :AUX   {DEFPOL(2)}  {SESSOUTV}
SAVE/REGR      &r  NBORD
COMPUTE/REGR   &r  :SEQFIT = NBORD
COMPUTE/TABLE  &r  :SEQDIF = ABS(:SEQFIT - :ORDER)
SELECT/TABLE   &r  (:SEQDIF.LE.0.3)            {SESSOUTV}
REGRES/POLY    &r  :ORDER :AUX    {DEFPOL(2)}  {SESSOUTV}
SAVE/REGR      &r  NBORD
COMPUTE/REGR   &r  :AUX = NBORD
COMPUTE/TABLE  &r  :AUX = INT(:AUX)
COMPUTE/TABLE  &r  :SEQDIF = :AUX - :ORDER
SELECT/TABLE   &r  (:SEQDIF.NE.0)              {SESSOUTV}

IF  OUTPUTI(1) .NE. 0  THEN
    READ/TABLE    &r  :ORIG  :ORDER  :AUX  
    @ creifnot 1
    SET/GRAPH
    SELECT/TABLE  &r  ALL
    PLOT/TABLE    &r  :ORDER  :ORIG
    WRITE/OUT "**************************************************"
    WRITE/OUT "****  Warning: Defaults in the order detection ***"
    WRITE/OUT "****   Table middummr.tbl has been plotted     ***"
    WRITE/OUT "**************************************************"
    WRITE/OUT "*       Read the help of DEFINE/HOUGH,           *"
    WRITE/OUT "*       Section: In Case of Emergency            *"
    WRITE/OUT "**************************************************"
    WRITE/OUT "* If you modify interactively middummr.tbl,      *"
    WRITE/OUT "* restart later with DEFINE/HOUGH P4=FOLLOW      *"
    WRITE/OUT "**************************************************"
    WRITE/OUT "* Now you can:                                   *"
    WRITE/OUT "*  1 - Go on                                     *"
    WRITE/OUT "*  2 - Stop here                                 *"
    WRITE/OUT "**************************************************"
    DEFINE/LOCAL   ANSW/I/1/1  0
    INQUIRE/KEYW   ANSW/I/1/1  "* Enter your choice : "
    IF ANSW .EQ. 2  RETURN/EXIT
ENDIF


DELETE/COLUMN  &r :SEQDIF
DELETE/COLUMN  &r :SEQFIT
DELETE/COLUMN  &r :SEQ
DELETE/COLUMN  &r :AUX

CHECKEND:

IF OUTMODE(1:1) .NE. "S" THEN
 SELECT/TABLE     &r ALL
 WRITE/OUT "************** Order Detection Table ***********"
 WRITE/OUT "     "
 READ/TABLE  &r
 WRITE/OUT "     "
ENDIF

ECHORD(1)  = {middummr.tbl,TBLCONTR(4)}
ECHORD(2)  = 1
ECHORD(3)  = echord(1)

IF M$TSTNO(P5) .EQ. 1 THEN
   COMPUTE/TABLE middummr :THRES = {P5}
ENDIF

write/keyw  in_a/C/1/60    middummi.bdf
write/keyw  in_b/C/1/60    middummr.tbl
write/keyw  out_a/C/1/60   {ORDTAB}

write/keyw  inputi/I/1/1  {VALI(2)}
WRITE/KEYW  inputi/I/2/2  {SCAN(1)},{SCAN(2)}
write/keyw  inputr/R/1/1  {VALI(1)}

IF OUTMODE(1:1) .EQ. "S" SET/MIDAS OUTPUT=NO
RUN STD_EXE:necdef
SET/MIDAS OUTPUT=YES

! Transform pixel coordinates to world coordinates
copy/dk {ORDREF} START/D/1/2 start
copy/dk {ORDREF} STEP/D/1/2  step
compute/table {ORDTAB} :X = {START(1)} + (:X-1)*{STEP(1)}
compute/table {ORDTAB} :Y = {START(2)} + (:Y-1)*{STEP(2)}
copy/kd   START/D/1/2 {ORDTAB} DEFSTART/D/1/2 
copy/kd   STEP/D/1/2  {ORDTAB} DEFSTEP/D/1/2  

IF NBORDI .LT.  0   THEN

LOAD/ECHELLE ECHELLE RAW 

WRITE/OUT "You have now the possibility to introduce a differential number"
WRITE/OUT "of orders, i.e. entering a number like +n if you want n more "
WRITE/OUT "orders, or -n for less orders. You can also enter 0 or type on "
WRITE/OUT "RETURN to go on."

write/keyw inputc  " " ALL
INQUIRE/KEYWORD INPUTC/C/1/5  "Differential : "  

IF M$TSTNO(INPUTC) .EQ. 1  THEN
   WRITE/KEYW INPUTI {INPUTC}
   IF INPUTI(1) .NE. 0 THEN
     NBHW(1) = ECHORD(1) + INPUTI(1)
     WRITE/KEYW  FLAG/C/1/1  N
     GOTO START
   ENDIF
ENDIF

ENDIF

REGRESS/ECHELLE  {P6}

DEFINE/LOCAL  BKGV/C/1/3 {BKGVISU}
SET/ECH  BKGVISU=NO
LOAD/ECHELLE
SET/ECH BKGVISU={BKGV}

PREP/BACK  ?   INI

RETURN
