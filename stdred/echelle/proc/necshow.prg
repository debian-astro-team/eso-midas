! @(#)necshow.prg	19.1 (ES0-DMD) 02/25/03 14:19:43
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echshow.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Display echelle session parameters
!.VERSION     1.0    Creation    21-AUG-1991  PB
!
!-------------------------------------------------------
!
SET/FORMAT I1  F12.5  
ECHO/OFF
SESSLINE = 0 ! Initialize the line counter
IF P8(1:1) .EQ. "H" GOTO HELP
!
IF P1(1:1) .EQ. "G" GOTO GENERAL
IF P1(1:1) .EQ. "O" GOTO ORDER
IF P1(1:1) .EQ. "B" GOTO BKG
IF P1(1:1) .EQ. "C" GOTO FILT
IF P1(1:1) .EQ. "E" GOTO EXTR
IF P1(1:1) .EQ. "W" GOTO WLC
IF P1(1:1) .EQ. "R" GOTO REBIN
IF P1(1:1) .EQ. "F" THEN
   IF P1(2:2) .EQ. "F" THEN 
      GOTO FLAT
   ELSE
      GOTO FLUX
   ENDIF
ENDIF
IF P1(1:1) .EQ. "M" GOTO MERGE
!
GENERAL:
!
SHOW/SESS  "****************  Echelle Parameters  *******************"
SHOW/SESS  "   "

SHOW/SESS  "--- (G) General Information ----"
SHOW/SESS  "    Instrument     :  INSTR   = {INSTRUM}"
SHOW/SESS  "    Dispersers     :  GRATING = {GRATING}  CROSS = {CROSS}"
SHOW/SESS  "    Detector       :  CCD     = {CCD}"
SHOW/SESS  "    Binning factor :  CCDBIN  = {CCDBIN(1)},{CCDBIN(2)}"
SHOW/SESS  "    Image size     :  IMSIZE  = {IMSIZE(1)},{IMSIZE(2)}"
SHOW/SESS  "    Scan limits    :  SCAN    = {SCAN(1)},{SCAN(2)}"
SHOW/SESS  "    Orders         :  {ECHORD(1)} orders, from {ECHORD(2)} to {ECHORD(3)}"
SHOW/SESS  "    Wavel. range   :  {WLRANGE(1)} - {WLRANGE(2)}  wavel. units"
SHOW/SESS  "   "
!
IF P1(1:1) .NE. "?" GOTO RET
!
ORDER:
!
SHOW/SESS  "--- (O) Order definition ----"
SHOW/SESS  "    Method (STD/COM/HOUGH)       : DEFMTD = {DEFMTD}"
SHOW/SESS  "    Polynomial Degree(X,Y)       : DEFPOL = {DEFPOL(1)},{DEFPOL(2)}"
SHOW/SESS  "    Order Reference Frame        : ORDREF = {ORDREF}"
IF DEFMTD(1:1) .EQ. "H" THEN
SHOW/SESS  "    Parameters for the method Hough:"
SHOW/SESS  "    Number of orders             : NBORDI = {NBORDI}"
SHOW/SESS  "    Width of the orders (pixels) : WIDTHI = {WIDTHI}"
SHOW/SESS  "    Threshold                    : THRESI = {THRESI}"
SHOW/SESS  "    Parameter SCAN is also taken into account during Hough order definition"
SHOW/SESS  "    Values of NBORDI, WIDTHI and THRESI will be estimated automatically"
SHOW/SESS  "    if these parameters are set to the null value."
ELSE
SHOW/SESS  "    Parameters for the methods STD and COM:"
SHOW/SESS  "    Width of the orders (pixels) : WIDTH1 = {WIDTH1}"
SHOW/SESS  "    Threshold                    : THRES1 = {THRES1}"
SHOW/SESS  "    Approximate slope (unitless) : SLOPE  = {SLOPE}"
ENDIF
!
SHOW/SESS  "     "
IF P1(1:1) .NE. "?" GOTO RET
!
BKG:
SHOW/SESS  "--- (B) Background ---"
SHOW/SESS  "    Method (POLY,SPLINE,SMOOTH)  : BKGMTD  = {BKGMTD}"
SHOW/SESS  "    Visualisation flag (YES,NO)  : BKGVISU = {BKGVISU}"
IF BKGMTD(1:1) .EQ. "P"    THEN
   SHOW/SESS  "    Parameters for method POLY"
   SHOW/SESS  "    Step (in pixels)             : BKGSTEP = {BKGSTEP}"
   SHOW/SESS  "    Polynomial degress (X,Y)     : BKGPOL  = {BKGPOL(1)},{BKGPOL(2)}"
ELSE
   SHOW/SESS  "    Radius (X,Y in pixels)       : BKGRAD  = {BKGRAD(1)},{BKGRAD(2)} " 
   IF BKGMTD(1:2) .EQ. "SP"   THEN 
      SHOW/SESS  "    Step (in pixels)             : BKGSTEP = {BKGSTEP}"
      SHOW/SESS  "    Spline degreee               : BKGDEG  = {BKGDEG}"
      SHOW/SESS  "    Smoothing factor             : BKGSMO  = {BKGSMO}"
   ENDIF
   IF BKGMTD(1:2) .EQ. "SM"   THEN 
      SHOW/SESS  "    Number of iterations         : BKGNIT  = {BKGNIT}"
   ENDIF
ENDIF
!
SHOW/SESS  "     "
IF P1(1:1) .NE. "?" GOTO RET
!
FILT:
SHOW/SESS  "--- (C) Cosmic Rays Filtering ---"
SHOW/SESS  "   Median filter (X,Y,THRES): MEDFILT= {MEDFILT(1)},{MEDFILT(2)},{MEDFILT(3)}"
SHOW/SESS  "   Cosmics filter (X,Y,Nit) : CRFILT = {CRFILT(1)},{CRFILT(2)},{CRFILT(3)}"
SHOW/SESS  "   CCD Params (ron,g,thres) : CCDFILT= {CCDFILT(1)},{CCDFILT(2)},{CCDFILT(3)}"
!
SHOW/SESS  "     "
IF P1(1:1) .NE. "?" GOTO RET
!
EXTR:
SHOW/SESS  "--- (E) Spectrum and Sky Extraction ---"
SHOW/SESS  "    "
SHOW/SESS  " -- Spectrum Extraction"

SHOW/SESS  "    Extraction method (LINEAR, AVERAGE, OPTIMAL)  : EXTMTD = {EXTMTD}   
SHOW/SESS  "    Slit Length (in pixels)                       : SLIT   = {SLIT}"
IF EXTMTD(1:1) .NE. "O"  THEN
  SHOW/SESS  "    Offset (in pixels)                            : OFFSET = {OFFSET}"
ELSE
  SHOW/SESS  "    Read-Out Noise           :  RON      = {RON}"
  SHOW/SESS  "    Gain                     :  GAIN     = {GAIN}"
  SHOW/SESS  "    Threshold (in std. dev.) :  EXTSIGMA = {EXTSIGMA}"
ENDIF
SHOW/SESS  "     "
SHOW/SESS  " -- Sky Definition and Extraction:"
SHOW/SESS  "    Number of sky windows                         : NSKY   = {NSKY}"
SHOW/SESS  "    Sky feature position (<col,order>,auto,cursor): POSSKY = {POSSKY}"
SHOW/SESS  "    Offset limits of 1st window  : SKYWIND(1-2) = {SKYWIND(1)},{SKYWIND(2)}"
IF NSKY .EQ. 2 THEN
SHOW/SESS  "    Offset limits of 2nd window  : SKYWIND(3-4) = {SKYWIND(3)},{SKYWIND(4)}"
ENDIF
!
SHOW/SESS  "     "
IF P1(1:1) .NE. "?" GOTO RET
!
WLC:
SHOW/SESS  "--- (W) Wavelength Calibration ---"
SHOW/SESS  "    Search Method (GRAVITY, GAUSSIAN) : SEAMTD  = {SEAMTD}"
SHOW/SESS  "    Analysis window width             : WIDTH2  = {WIDTH2}"
SHOW/SESS  "    Threshold above the background    : THRES2  = {THRES2}"
SHOW/SESS  "    Wavelength calibr. meth.          : WLCMTD  = {WLCMTD}"
SHOW/SESS  "    (Possible methods: PAIR, ANGLE, GUESS, LINEAR, ORDER, RESTART)
SHOW/SESS  "    Wavelength calibr. option (1D/2D) : WLCOPT  = {WLCOPT}"
SHOW/SESS  "    Regress. method (Standard,Robust) : WLCREG  = {WLCREG}"
SHOW/SESS  "    Visualisation flag (Yes,No)       : WLCVISU = {WLCVISU}"
IF WLCMTD(1:1) .EQ. "G" THEN
SHOW/SESS  "    Method Guess. Guess Session       : GUESS = {GUESS}"
ENDIF
SHOW/SESS  "    Arc frame                         : WLC     = {WLC}"
SHOW/SESS  "    Line Catalog                      : LINCAT  = {LINCAT}"
SHOW/SESS  "    Polynomial Degree                 : DC      = {DC}"
SHOW/SESS  "    Tolerance                         : TOL     = {TOL}"
SHOW/SESS  "NB: Parameter CCDBIN is also involved"
SHOW/SESS  "    Mini, Maxi number of iterations   : WLCNITER = {WLCNITER(1)},{WLCNITER(2)}"
SHOW/SESS  "    Iteration Loop parameters:"
SHOW/SESS  "    WLCITER(1-3) = {WLCITER(1)}, {WLCITER(2)}, {WLCITER(3)}
SHOW/SESS  "    WLCITER(4-5) = {WLCITER(4)}, {WLCITER(5)}"
!
SHOW/SESS  "     "
IF P1(1:1) .NE. "?" GOTO RET
!
REBIN:
SHOW/SESS  "--- (R) Spectrum Resampling---"
SHOW/SESS  "    Resampling Option (Yes/No)        : REBOPT   = {REBOPT}"
SHOW/SESS  "    Resampling Method                 : REBMTD   = {REBMTD}"
SHOW/SESS  "    Resampling step or reference frame: SAMPLE   = {SAMPLE}"
!
SHOW/SESS  "     "
IF P1(1:1) .NE. "?" GOTO RET
!
FLAT:
SHOW/SESS  "--- (F) Flat Field correction ---"
SHOW/SESS  "    Flat-Field option (Yes,No)        : FFOPT    = {FFOPT}"
IF FFOPT(1:1) .EQ. "Y" THEN
SHOW/SESS  "      Flat-Field frame                : FLAT     = {FLAT}"
SHOW/SESS  "      Background corrected flat-field : CORRECT  = {CORRECT}"
SHOW/SESS  "      Blaze function                  : BLAZE    = {BLAZE}"
ENDIF
SHOW/SESS  "     "
!
IF P1(1:1) .NE. "?" GOTO RET
!
FLUX:
SHOW/SESS  "---  (F) Flux Calibration ---"
SHOW/SESS  " Response Correction Option (Yes, No) : RESPOPT = {RESPOPT}"
IF RESPOPT(1:1) .EQ. "Y" THEN
 SHOW/SESS  "      Response Corection Method : RESPMTD = {RESPMTD}"
 SHOW/SESS  "      (Possible methods: STD, IUE, FLAT)"

 IF RESPMTD(1:1) .EQ. "S"  THEN
  SHOW/SESS  "        Standard Star                   : STD      = {STD}"
  SHOW/SESS  "        Response Table                  : RESPONSE = {RESPONSE}"
  SHOW/SESS  "        Flux Table                      : FLUXTAB  = {FLUXTAB}"
  SHOW/SESS  "        Median Filter param. (X,Y,thres): FILTMED  = {FILTMED(1)},"
  SHOW/SESS  "        Smooth filter param. (X,Y,THRES): FILTSMO  = {FILTSMO(1)}"
  SHOW/SESS  "        Valid pixel range               : PIXNUL   = {PIXNUL(1)}, {PIXNUL(2)}"
 ENDIF

 IF RESPMTD(1:1) .EQ. "I"  THEN
  SHOW/SESS  "        RIPMTD = {RIPMTD}    RIPFRZ = {RIPFRZ}"
   IF RIPMTD(1:1) .NE. "S" THEN
    SHOW/SESS  "          LAMBDA1 = {LAMBDA1}  LAMBDA2 = {LAMBDA2}"
   ENDIF
  SHOW/SESS    "          RIPK   = {RIPK}      ALPHA = {ALPHA}"
 ENDIF

 IF RESPMTD(1:1) .EQ. "M" THEN
  SHOW/SESS  "        OBSTAB  = {OBSTAB}   RESPONSE = {RESPONSE}"
  SHOW/SESS  "        RIPORD = {RIPORD}    RIPRAD = {RIPRAD(1)},{RIPRAD(2)}"
  SHOW/SESS  "        RIPPIX = {RIPPIX}    RIPREF = {RIPREF}"
  SHOW/SESS  "        RIPBIN = {RIPBIN}"
 ENDIF
ENDIF
!
SHOW/SESS  "     "
IF P1(1:1) .NE. "?" GOTO RET
!
MERGE:
SHOW/SESS  "--- (M) Merging ---"
SHOW/SESS  "    Orders merging option (Yes, No)     : MGOPT  = {MGOPT}"
show/sess  "    Order merging method  (Aver, Noapp) : MRGMTD = {MRGMTD}"
IF MGOPT(1:1) .EQ. "Y" THEN 
IF MRGMTD(1:1) .EQ. "N"  THEN
   SHOW/SESS  "    First, last order number            : MRGORD = {MRGORD(1)},{MRGORD(2)}"
ELSE
   SHOW/SESS  "    Zeroed interval (wavel. units)      : DELTA  = {DELTA}"
ENDIF
ENDIF
!
SHOW/SESS  "     "
IF P1(1:1) .NE. "?" GOTO RET
!
RET:
SET/FORMAT
RETURN

HELP:

SHOW/SESS  "            ****  Echelle   Main   Help    Message  ****"
SHOW/SESS  "  "
SHOW/SESS  " This is the list of high level commands useful for an echelle"
SHOW/SESS  " reduction in the sequential order you will need them. Optional"
SHOW/SESS  " steps are indicated by (opt.). Entries in brackets [] indicate"
SHOW/SESS  " the entries of SHOW/ECHELLE which must be checked before running"
SHOW/SESS  " the command. "
SHOW/SESS  " More help is available for each command with HELP <command>"
SHOW/SESS  "   "
SHOW/SESS  "  INTAPE/FITS"
SHOW/SESS  "  ROTATE/ECHELLE"
SHOW/SESS  "  PREPARE/ECHELLE   (opt.)"
SHOW/SESS  "  SHOW/ECHELLE"
SHOW/SESS  "  HELP/ECHELLE"
SHOW/SESS  "  SET/ECHELLE"
SHOW/SESS  "  CALIBRATE/ECHELLE  [Ord. Def.]  [Extr.]  [Search.]  [Wav. cal.]"
SHOW/SESS  "  SAVE/ECHELLE"
SHOW/SESS  "  INIT/ECHELLE"
SHOW/SESS  "  SELECT/BACKGROUND (opt.)"
SHOW/SESS  "  FILTER/ECHELLE    (opt.)  [Background] [Filtering]"
SHOW/SESS  "  FLAT/ECHELLE      (opt.)  [Flat-field] [Backgr.] [Extr.] [Rebin]"
SHOW/SESS  "  RESPONSE/ECHELLE  (opt.)  [Backgr.] [Extr.] [Rebin] [Response]"
SHOW/SESS  "  REPEAT/ECHELLE    (opt.)"
SHOW/SESS  "  EDIT/ECHELLE      (opt.)"
SHOW/SESS  "  REDUCE/ECHELLE    [Backgr.] [Extr.] [Rebin] [Response]  [Merging]"
SHOW/SESS  "    "
SHOW/SESS  "TUTORIAL/ECH  demonstrates the use of the different commands"
SHOW/SESS  "SHOW/ECHELLE  display a list of the parameters to be set (SET/ECH"
SHOW/SESS  "        or direct assignment. The list depends on the chosen method"
SHOW/SESS  "HELP/ECH method    provide a list of all methods available"
SHOW/SESS  "           for the different steps of the calibration and reduction"
SHOW/SESS  "HELP/ECH option    provide a list of all available options"

RETURN


