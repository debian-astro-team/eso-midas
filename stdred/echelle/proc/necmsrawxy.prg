! @(#)necmsrawxy.prg	19.1 (ESO-IPG) 02/25/03 14:19:40
! Purpose: This procedure measures the raw XY position and width
! of Th-Ar lines on an arc echelle exposure. It determines the
! associated spectral resolution and displays statistics. 
!
define/param P1 {WLC}     IMA   "Measurement Frame:" 
define/param P2 resol.tbl TAB   "Result resolution table:"
define/param P3 GAUSS CHAR     "Measurement method (GAUSS/MOMENT):"
define/param P4  YES  CHAR      "Visualisation option:" 
define/param P5 0,0   NUMB      "Position offsets (Xobser-Xmodel): " 
define/param P6 0,0   NUMB      "Position offsets (Xobser-Xmodel): " 
define/param P7 40,40   NUMB      "Measurement box (X,Y): " 

verify/echelle {P1}

define/local XYOFF/R/1/2   {P5}
define/local XYSCALE/R/1/2 {P6}
define/local MBOX/R/1/2    {P7} 

select/table pline :IDENT.GT.0. 
copy/table pline &l 

compute/table &l :XSTART = :XMOD*(1.+{XYSCALE(1)}) + {XYOFF(1)} - {MBOX(1)}/2.
compute/table &l :XEND   = :XMOD*(1.+{XYSCALE(1)}) + {XYOFF(1)} + {MBOX(1)}/2.
compute/table &l :YSTART = :YMOD*(1.+{XYSCALE(2)}) + {XYOFF(2)} - {MBOX(2)}/2.
compute/table &l :YEND   = :YMOD*(1.+{XYSCALE(2)}) + {XYOFF(2)} + {MBOX(2)}/2.

define/local limits/D/1/2 0,0
limits(1) = imsize(1) - mbox(1)
limits(2) = imsize(2) - mbox(2)

select/table &l :XSTART.GT.{MBOX(1)}..AND.:XSTART.LT.{limits(1)}
select/table &l SELECT.AND.:YSTART.GT.{MBOX(2)}.AND.:YSTART.LT.{limits(2)}
copy/table   &l &m
copy/table   &l rline

if p3(1:1) .eq. "G" then
  center/gauss {P1},middummm.tbl middummm.tbl 
else
  center/moment {P1},middummm.tbl middummm.tbl 
endif

copy/tt &m :XCEN    rline.tbl :XMES
copy/tt &m :YCEN    rline.tbl :YMES
copy/tt &m :STATUS  rline.tbl :STATUS

select/table rline :STATUS.EQ.0.
copy/table   rline  &p
copy/table   &p     rline 

if p4(1:1) .eq. "Y" then
 @ creifnot 2 heat
 clear/chan over
 load/table rline  :XMES :YMES  ? 0 2
 load/table rline  :XMOD :YMOD  ? 0 1
endif

compute/table rline :XDIF = :XMES - :XMOD
compute/table rline :YDIF = :YMES - :YMOD
regress/poly  rline :XDIF :XMOD,:YMOD 2,2
save/regress  pline XDIF
regress/poly  rline :YDIF :XMOD,:YMOD 2,2
save/regress  pline YDIF

set/graph
plot/table rline :XMOD :XDIF
plot/table rline :XMOD :YDIF

sel/tab pline all 
copy/table pline npline
compute/regress npline :XDIF = XDIF
compute/regress npline :YDIF = YDIF
compute/table   npline :XMOD = :XMOD + :XDIF
compute/table   npline :YMOD = :YMOD + :YDIF

clear/chan over
load/table npline :XMOD :YMOD ?  0 8

