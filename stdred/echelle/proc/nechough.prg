! @(#)nechough.prg	19.1 (ES0-DMD) 02/25/03 14:19:38
!  @(#)nechough.prg	19.1 (ESO) 02/25/03 14:19:38
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! ECHELLE PROCEDURE : HOUGH.PRG
! P. Ballester - ESO Garching                    version 1.0  15.03.91
!
! .PURPOSE
!
! Compute Hough Transform of an image  and perform cluster analysis.
! 
! --------------------------------------------------------------------
!
IF P1 .NE. "?" WRITE/KEYW ECHC/C/1/8   'P1'

define/param   P1    ?        I      "Input frame:"
define/param   P2 1,{{P1},NPIX(2)}    N      "Scan parameters"
define/param   P3   30,5      ?      "Step, number of traces or ALL"
define/param   P4     0       N      "Minimum, maximum number of orders"
define/param   P5    HMN      C      "Flags:"

IF P5(2:2) .EQ. "L" THEN
    define/param   P6    ?            N    "Half-width:"
ENDIF
define/param   P7   3,0.5,16000,1     N    "DFWHM, DSLOPE, HOT PIXEL THRESH.:"


! ***  Local keywords definition

define/local    time/C/1/30     HAY            ! Time string
define/local    THRES/R/1/4    {P7}            ! Thresholds
define/local    center/R/1/1    0   ! Centering threshold
define/local    HTLOC/C/1/20    middummt.bdf   ! Name of local HT


! ***  Parameter   P3 :  If this parameter is set to ALL

define/local   orgcol/i/1/1    {{P1},NPIX(1)}

define/local   htpar/C/1/20    {P3}
define/local   htcol/i/1/2     0,0

IF P5(1:1) .NE. "H"  THEN
   COPY/DK  middummh   HTPAR/C/1/20    HTPAR/C/1/20
   WRITE/OUT  "No Hough transform. Input read from middummh.bdf"
ELSE
   IF  P3(1:1) .EQ. "A"  WRITE/KEYW   htpar/C/1/20   1,{orgcol}
ENDIF

WRITE/KEYW   htcol/i/1/2     {htpar}

! *** Centering constant.

IF thres(4) .eq. 1 THEN
      center = 1. - 0.5/htcol(2)
      IF center .gt. 0.99  center = 0.99
ELSE
      center = thres(4)
ENDIF


! *** Parameter P4 : Minimum and maximum number of orders

define/local         MINMAX/I/1/2  {P4},{P4} ! Mini and maxi number of orders
IF MINMAX(1) .EQ. 0  WRITE/KEYW MINMAX/I/1/2  1,1000
!
!***  Parameter   P8 : This parameter should never be set by the user, unless
!***  he has very good reasons, and a clear understanding of the method.
! P8 : cluster, range_mini, range_maxi, step_x, step_y
! Parameters:
!  cluster :  Cluster detection constant. Possible range [0,1]. Default 0.15
!  range_mini, range_maxi : range of slope to be detected. Default: -0.1,0.4
!  step_x,step_y:  Step of the Hough transform. Default: 0.005, 1.
!

!
! Set default values and  read P8 to take into account user-defined values
!
define/local   cluster/R/1/1   0.15
define/local   slrang/D/1/2    -0.1,0.4   ! Range of slope (min,max)
define/local   step/d/1/2       0.005,1   ! Step of HT (x,y)

SET/FORMAT     F10.5
define/param   P8  {cluster},{slrang(1)},{slrang(2)},{step(1)},{step(2)}   N
SET/FORMAT

define/local   P8INT/D/1/7    {P8}
cluster        = P8INT(1)
slrang(1)      = P8INT(2)
slrang(2)      = P8INT(3)
write/keyw      step/D/1/2     {P8INT(4)},{P8INT(5)}

!
! Computes derived parameters: pix(1),pix(2), step(1),step(2)
! Parameters pix(1), pix(2) are computed from range_.. and step_..
!
define/local   pix/I/1/2       0,0        ! Number of pixels along X
define/local   start/d/1/2     0,0        ! Start of HT

pix(1)   = (slrang(2)-slrang(1))/step(1)
pix(2)   = ({{P1},NPIX(2)} + orgcol*(slrang(2)-slrang(1)))/step(2)
start(1) = slrang(1)
start(2) = (-1.)*orgcol*slrang(2)

! ***  Compute the Hough Transform, or access an already existing image.

IF P5(1:1) .EQ. "H"  THEN

   WRITE/OUT       "Performing Hough transform..."

   WRITE/KEYW IN_A/C/1/60   {P1}
   WRITE/KEYW IN_B/C/1/60   middummh.bdf
   WRITE/KEYW INPUTI/I/1/6  {HTPAR},{PIX(1)},{PIX(2)},{P2}
   WRITE/KEYW INPUTR/R/1/1  {THRES(3)}
   WRITE/KEYW INPUTD/D/1/4  {START(1)},{START(2)},{STEP(1)},{STEP(2)}

   IF OUTMODE(1:1) .EQ. "S" SET/MIDAS OUTPUT=NO
      RUN STD_EXE:nechough
   SET/MIDAS OUTPUT=YES

   COPY/KD      HTPAR/C/1/20    middummh   HTPAR/C/1/20

ENDIF
! ***  Perform cluster detection
CLUSTER:

IF P5(2:2) .EQ. "N"  RETURN         ! No cluster detection

COPY/II         middummh.bdf   {HTLOC}

COMPUTE/KEYW        TIME = M$TIME()
WRITE/OUT       "Performing cluster detection..."

define/local    RESTAB/C/1/20   middummr.tbl   ! Table of results
define/local    HWINI/I/1/1   0     ! First half_width of orders

IF P5(2:2) .EQ. "L"   THEN
           HWINI = {P6}         ! Low contrast : enforce HW
ELSE
           HWINI = 0            ! Otherwise ; automatic computation
ENDIF

WRITE/KEYW   INPUTC/C/1/20 {P5(2:2)}  ! Method for the order detection
WRITE/KEYW   IN_A/C/1/60   {HTLOC}
WRITE/KEYW   IN_B/C/1/60   {RESTAB}
WRITE/KEYW   INPUTR/R/1/2  {CENTER},{CLUSTER}
WRITE/KEYW   INPUTI/I/1/6  {HWINI},{ORGCOL},{HTPAR},{MINMAX(1)},{MINMAX(2)}

IF OUTMODE(1:1) .EQ. "S" SET/MIDAS OUTPUT=NO
RUN STD_EXE:necfindmax
SET/MIDAS OUTPUT=YES

CHECK:

SORT/TABLE      {RESTAB}   :ORIG
COMPUTE/TABLE   {RESTAB}   :ORDER = seq

! *** End of detection

END:
RETURN


