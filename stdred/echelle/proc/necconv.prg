! @(#)necconv.prg	19.1 (ES0-DMD) 02/25/03 14:19:36
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! ECHELLE PROCEDURE : ECHCONV2.PRG
! J.D.Ponz			version 1.0 130785
!
! .PURPOSE
!
! execute the command :
! CONV/ECH INPUT OUTPUT DOMAIN function PAR option
! DOMAIN - IS EITHER START,STEP OR REFERENCE IMAGE
! FUNCTION - REBINNING FUNCTION
! PAR    - FUNCTION COEFFS
! OPTION - INTERP. OPTION
! ---------------------------------------------------
DEFINE/PARAM P1 ?     IMAGE  "Enter input image:"
DEFINE/PARAM P2 ?     IMAGE  "Enter output image:"
DEFINE/PARAM P3 ?     ?      "Enter output domain:"
DEFINE/PARAM P4 LIN   ?      "Rebinning function:"
DEFINE/PARAM P5 0.,1. NUMBER "Function params:"
DEFINE/PARAM P6 PIX   ?      "Inter. option:"
WRITE/KEYW INPUTD/D/1/12    0. ALL
WRITE/KEYW INPUTD/D/1/12    'P5'
RUN STD_EXE:NECCONV
