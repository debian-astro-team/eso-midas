! @(#)necextr.prg	19.1 (ESO-DMD) 02/25/03 14:19:37
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echextr.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command EXTRACT/ECHELLE INPUT OUTPUT W,OFF,A[,WS,OFF1,OFF2]
!.VERSION     1.0    Creation   31-JUL-1991  PB
! 020322	last modif
! 
!-------------------------------------------------------
!
define/param p1 ?       IMAGE  "Enter input image:"
define/param p2 ?       IMAGE  "Enter output extracted orders:"
define/param p3 {slit}  N "Extraction params (slit,offset or slit,ron,g,sigma):" 
define/param p4 {extmtd}  C      "Extraction method"
!
! Check extraction method
!
define/local errflag/i/1/1  1

if p4(1:1) .eq. "L"  ERRFLAG = 0
if p4(1:1) .eq. "A"  ERRFLAG = 0
if p4(1:1) .eq. "O"  ERRFLAG = 0
if errflag .eq. 0  then
   set/echelle EXTMTD={p4}
else
   error/echelle EXTRACT/ECHELLE  EXTMTD
endif
!
! Update extraction parameters
!
define/local w/r/1/4   0. all
!
if extmtd(1:1) .eq. "O" then
  write/keyw w/r/1/4  {slit},{ron},{gain},{extsigma}
  write/keyw w/r/1/4  {p3}
  slit     = w(1)
  ron      = w(2)
  gain     = w(3)
  extsigma = w(4)
else
  write/keyw w/r/1/2  {slit},{offset}
  write/keyw w/r/1/2  {p3}
  slit     = w(1)
  offset   = w(2)
endif
!
verify/echelle {p1}
if slit      .le. 0.     error/echelle EXTRACT/ECHELLE  SLIT
if echord(1) .le. 0      error/echelle EXTRACT/ECHELLE  ECHORD(1)
if ron       .lt. 0.     error/echelle EXTRACT/ECHELLE  RON
if gain      .lt. 0.     error/echelle EXTRACT/ECHELLE  GAIN
if extsigma  .lt. 0.     error/echelle EXTRACT/ECHELLE  EXTSIGMA
!
if extmtd(1:1) .eq. "O" then
   extract/optimal {p1} {p2} {slit},{echord(4)},{echord(5)} -
                   {ron},{gain},{extsigma} {ordtab} COEFF
else
   extr/order {p1} {p2} {w(1)},0.,{w(2)} {p4} {ordtab} -
              COEFF {echord(4)},{echord(5)}
endif

