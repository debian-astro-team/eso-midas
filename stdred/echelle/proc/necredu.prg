! @(#)necredu.prg	19.1 (ES0-DMD) 02/25/03 14:19:41
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       necbatch.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Batch Reduction of echelle spectra.
!.VERSION     1.0    Creation    15-MAY-1995
!-------------------------------------------------------
!
DEFINE/LOCAL TIME/D/1/1     1.
DEFINE/LOCAL REDMTD/C/1/6   FULL
DEFINE/LOCAL BACKGR/C/1/40  HAY
DEFINE/LOCAL BAKFLG/I/1/1   0
DEFINE/LOCAL VALUE/C/1/3    HAY

DEFINE/PARAM  P1  ?       I   "Input image:"
DEFINE/PARAM  P2  ?       I   "Output spectrum:"
IF P3(1:1) .NE. "?"  REDMTD = "SIMPLE"

VERIFY/ECHELLE {P1}

! 1) Check observation time, std rotation and cosmic rays filtering
VERIFY/ECHELLE {P1} OTIME
TIME = {{P1},O_TIME(7)}

IF ROTOPT(1:1) .EQ. "Y" THEN
  ROTATE/ECHELLE {P1} &r
ELSE 
  COPY/II {P1} &r0001
ENDIF

IF REDMTD(1:1) .NE. "S" THEN
   IF RESPMTD(1:1) .EQ. "I"   GOTO IUE   ! IUE-like method
ENDIF

! 2) Subtract background

IF CROPT(1:1) .EQ. "Y" THEN
   FILTER/ECHELLE &r0001 &f
ELSE
   SUBTRACT/BACKGR  &r0001   &r  &s
ENDIF 


! 3) Perform flat-field correction

IF FFOPT(1:1) .EQ. "Y"  THEN
   WRITE/OUT "Perform flat-field correction: CORRECT = {CORRECT}, BLAZE = {BLAZE}"
   VERIFY/ECHELLE {CORRECT}
   VERIFY/ECHELLE {BLAZE}   REBIN
   VERIFY/ECHELLE &s AGAINST {CORRECT}
   COMPUTE/IMAGE &t = &s/{CORRECT}
ELSE
-COPY  middumms.bdf middummt.bdf
ENDIF

! 4) Extraction and time normalization

IF EXTOPT(1:1) .EQ. "Y" THEN
  EXTRACT/ECHELLE &t &u
  COMPUTE/IMAGE &u = &u/{TIME}
ELSE
  RETURN ! All following steps require extraction
ENDIF

! 4.5) Sky subtraction

IF SKYOPT(1:1) .EQ. "Y" THEN
   EXTRACT/SKY  &t &sky
   COMPUTE/IMAGE &ts = &u - &sky/{TIME}
ELSE
   COPY/II &u &ts
ENDIF

! 5) Rebin and end of optional flat-field correction

IF REBOPT(1:1) .EQ. "N" THEN 
  DEFINE/LOCAL REBF/I/1/1  0
  IF FFOPT(1:1) .EQ. "Y"   REBF = 1
  IF RESPOPT(1:1) .EQ. "Y" REBF = 1
  IF MGOPT(1:1) .EQ. "Y"   REBF = 1
  IF REBF .EQ. 1 THEN
    WRITE/OUT "Rebin required by Flat-Field, Response correction or Merge"
    WRITE/OUT "Set REBOPT to YES"
    SET/ECHELLE REBOPT=YES
  ENDIF
ENDIF

IF REBOPT(1:1) .EQ. "Y" THEN
   REBIN/ECHELLE &ts  &v 
ELSE
   COPY/II &ts  &v
ENDIF

! 5.2) Flat-field rough flattening
IF RESPOPT(1:1) .EQ. "Y" .AND. RESPMTD(1:1) .EQ. "F" THEN
-RENAME middummv.bdf middummr.bdf
GOTO MERGE
ENDIF

IF FFOPT(1:1) .EQ. "Y"  THEN
   VERIFY/ECHELLE &v AGAINST {BLAZE}
   COMPUTE/IMAGE &w = &v * {BLAZE}
ELSE
-RENAME  middummv.bdf middummw.bdf
ENDIF

IF REDMTD(1:1) .EQ. "S" THEN   ! Reduction method = SIMPLE
   COPY/II &s {P3}
   COPY/II &w {P2}
   COPY/DD {P1} *,3 {P2}
   RETURN
ENDIF

! 6)  Response correction (method STD)

IF RESPOPT(1:1) .EQ. "Y" THEN
   VERIFY/ECHELLE  {RESPONSE}  REBIN
   VERIFY/ECHELLE  &w AGAINST {RESPONSE}
   COMPUTE/IMAGE &r = &w*{RESPONSE}
ELSE
-RENAME middummw.bdf middummr.bdf
ENDIF

GOTO MERGE

RETURN

IUE:

WRITE/OUT  "OBJECT REDUCTION : IUE-like method"

WRITE/OUT   Extract Gross spectrum 
EXTRACT/ECHELLE  {P1}  &r
REBIN/ECHELLE     &r   &y
!
IF M$EXISTD(P1,"BACKGROUND") .EQ. 1  THEN
WRITE/KEYW VALUE/C/1/3  {{P1},BACKGROUND}
IF VALUE(1:3) .EQ. "SUB" THEN
WRITE/OUT "Warning :"
WRITE/OUT "The background has been already subtracted from {P1}"
WRITE/OUT "The intermediate background frame is required"
WRITE/OUT "If you cannot provide the name of the background frame,"
WRITE/OUT "type <RETURN> and the background will be recomputed on {P1}"
INQUIRE/KEYW  BACKGR/C/1/40  "Background frame: "
IF AUX_MODE(7) .EQ.  0  THEN
   DELETE/DESCR  {P1}   BACKGROUND
ELSE
   COPY/II      {BACKGR}  &s
   COPY/II      {P1}      &w
   BAKFLG = 1
ENDIF
ENDIF
ENDIF

IF BAKFLG .EQ. 0 THEN
WRITE/OUT  Subtract background level
SUBTRACT/BACKGROUND {P1} &s  &w
ENDIF

EXTRACT/ECHELLE          &s  &u
REBIN/ECHELLE            &u  &z

WRITE/OUT  Extract Net spectrum
IF FFOPT(1:1) .EQ. "Y"  THEN
    VERIFY/ECHELLE  &w AGAINST {CORRECT}
    COMPUTE/IMAGE   &v = &w/{CORRECT}
    EXTRACT/ECHELLE &v &w
    REBIN/ECHELLE   &w &x
    VERIFY/ECHELLE  &x AGAINST {BLAZE}
    COMPUTE/IMAGE   &q = &x*{BLAZE}
ELSE
    VERIFY/ECHELLE  &y AGAINST &z
    COMPUTE/IMAGE   &q = &y - &z
ENDIF

WRITE/OUT  Blaze correction
RIPPLE/ECHELLE  &q  &r


MERGE:

IF MGOPT(1:1) .EQ. "Y"  THEN
    COPY/DD {P1} *,3  middummr.bdf
    MERGE/ECHELLE   &r  {P2}
ELSE
    COPY/II  &r       {P2}
    COPY/DD {P1} *,3  {P2}
ENDIF


RETURN
