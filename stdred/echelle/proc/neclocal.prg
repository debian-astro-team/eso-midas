! @(#)neclocal.prg	19.1 (ES0-DMD) 02/25/03 14:19:39
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echlide.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command LOAD/IDENT
!.VERSION     1.0    Creation   22-AUG-1991  PB 
!
!-------------------------------------------------------
!
DEFINE/PARAM P1 {WLC} IMA 
DEFINE/PARAM P2 &r    IMA

SET/FORMAT F10.6

write/out "Extracting image: {P1}"
EXTRACT/ECHELLE {p1} &w 

write/out "Resampling."
IF SAMPLE(1:1) .EQ. "+" SAMPLE = "{{LINTAB},PIXEL(1)}"
REBIN/ECHELLE   &w   {P2} 

COPY/DD {P1} LHCUTS  {P2}
DISPLAY/ECHELLE {P2}

