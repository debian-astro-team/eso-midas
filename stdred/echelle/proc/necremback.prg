! @(#)necremback.prg	19.1 (ES0-DMD) 02/25/03 14:19:41
! Procedure : remback.prg
! Author    : P. Ballester    -  ESO Garching
! Date      : 910221
!             990622-swolf P6 added
!             990721-swolf colours to the plot
!
! Purpose   : Subtract background and update descriptor BACKGROUND
!
! Syntax    : SUBTRACT/BACK  raw_frame out_frame  [bkgmtd]
!
DEFINE/PARAM  P1   ?    I     "Enter name of raw frame:"
DEFINE/PARAM  P2   ?    I     "Enter name of bkg frame:"
DEFINE/PARAM  P3   ?    I     "Enter name of output frame:"
DEFINE/PARAM  P4  {BKGMTD}  C  "Background method:"
DEFINE/PARAM  P5  {BKGVISU} C  "Optional visualization:"
DEFINE/PARAM  P6  MEDIAN    C  "Measure value in box centered on backgr. position (MINIMUM, or MEDIAN):"

DEFINE/LOCAL CORD/I/1/1 0

VERIFY/ECHELLE {P1}

IF M$EXISTD(P1,"BACKGROUND") .EQ. 1  THEN
   WRITE/OUT "Background already subtracted from {P1}. Skipped..."
   COPY/II {P1}  {P3}
   RETURN
ENDIF

IF P4(1:1) .EQ. "D" THEN
   WRITE/OUT "No background computed. Subtracted {P2} from {P1}. "
   VERIFY/ECHELLE {P2}
   GOTO MARK
ENDIF

DEFINE/LOCAL   ERFLG/I/1/1  0

IF P4(1:1) .EQ. "P" THEN
   ERFLG = 1
   BACKGROUND/ECHELLE {P1} {P2}  P6={P4}
ENDIF

IF P4(1:2) .EQ. "SP" THEN
   ERFLG = 1
   BACKGROUND/ECHELLE {P1}  {P2}  P6={P4} P7={P6}
ENDIF

IF P4(1:2) .EQ. "SM" THEN
   ERFLG = 1
   BACKGROUND/SMOOTH {P1}   {P2}  
ENDIF

IF ERFLG .EQ. 0  THEN
   WRITE/OUT "SUBTRACT/BACKGROUND: unknown method {BKGMTD}"
   HELP/ECHE BKGMTD
   RETURN/EXIT
ENDIF

WRITE/KEYW  BKGMTD/C/1/12  {P4}

MARK:

VERIFY/ECHELLE {P1} AGAINST {P2}
COMPUTE/IMAGE  {P3} = {P1} - {P2}

COPY/DD        {P1}   *,3    {P3}
WRITE/DESCR    {P3}   BACKGROUND/C/1/11   subtracted

VISU:

WRITE/KEYW  BKGVISU/C/1/3   {P5}
IF BKGVISU(1:1) .NE. "Y"   THEN
   WRITE/KEYW BKGVISU/C/1/3  "NO "
   RETURN
ELSE
   WRITE/KEYW BKGVISU/C/1/3  "YES"
ENDIF

@ creifnot 1

CORD = IMSIZE(1)/2
SET/GRAP COLOUR=1
PLOT/COLUMN {P1}  @{CORD}
SET/GRAP COLOUR=2
OVERPLOT/COLUMN {P2}  @{CORD}
SET/GRAP COLOUR=1

RETURN





