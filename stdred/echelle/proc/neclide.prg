! @(#)neclide.prg	19.1 (ES0-DMD) 02/25/03 14:19:39
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echlide.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command LOAD/IDENT
!.VERSION     1.0    Creation   22-AUG-1991  PB 
!
!-------------------------------------------------------
!
LOAD/ECHELLE IDENT
RETURN
