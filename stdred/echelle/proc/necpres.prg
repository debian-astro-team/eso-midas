! @(#)necpres.prg	19.1 (ES0-DMD) 02/25/03 14:19:41
! @(#)necpres.prg	19.1  (ESO)  02/25/03  14:19:41
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echpres.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command PLOT/RESIDUAL  [N1,N2]
!.VERSION     1.0    Creation    J.D. Ponz  131083
!
!-------------------------------------------------------
!
@ creifnot 1
SET/GRAPH
IF "'P1'" .EQ. "?" THEN
  SELECT/TABLE   {LINTAB} (:SELECT.NE.0)  {SESSOUTV}
  PLOT/TABLE     {LINTAB} :ORDER :RESIDUAL
ELSE
  DEFINE/LOCAL I/I/1/1 0
  DEFINE/LOCAL I1/I/1/3 0,0,0
  WRITE/KEYW    I1/I/1/2 'P1'
  I1(3) = I1(1) + 1
  SELECT/TABLE   {LINTAB} (:Y.EQ.{I1(1)}.AND.:SELECT.NE.0) {SESSOUTV}
  PLOT/TABLE     {LINTAB} :WAVE :RESIDUAL
  DO I = 'I1(3)' 'I1(2)'
    SELECT/TABLE       {LINTAB} (:Y.EQ.{I}.AND.:SELECT.NE.0) {SESSOUTV}
    OVERPLOT/TABLE     {LINTAB} :WAVE :RESIDUAL
  ENDDO
  SELECT/TABLE   {LINTAB} ALL 
ENDIF

RETURN
