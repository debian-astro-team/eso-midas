! @(#)necregr.prg	12.1 (ES0-DMD) 09/16/97 11:17:44
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echregr.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Fit mesured position of orders by a bivariate polynomial
!.VERSION     1.0    Creation    13-OCT-1983
!
!-------------------------------------------------------
!
IF P1 .NE. "?" WRITE/KEYW    DEFPOL/I/1/2  {P1}
DEFINE/PARAM   P2    3      N    "Number of outliers rejection:"
DEFINE/PARAM   P3    2.0    N    "Absolute tolerance (pixels):"
DEFINE/PARAM   P4    4.5    N    "Kappa-sigma factor :"
!
DEFINE/LOCAL  CNT/I/1/1      0
DEFINE/LOCAL  THRES/R/1/1    0.
DEFINE/LOCAL  ORDER/I/1/1    0
DEFINE/LOCAL  RMSMAX/R/1/1   0.
DEFINE/LOCAL  RMS/R/1/1      0.
DEFINE/LOCAL  REJECT/I/1/300 0  ALL
DEFINE/LOCAL  NBREJ/I/1/1    0
!
WRITE/OUT  "Display orders positions..."
SET/FORMAT I1  E14.7 

LOAD/ECHELLE ECHELLE RAW 
!

inputr(1) = m$value({ordtab},defstart(1))
inputr(2) = m$value({ordtab},defstart(1))+(imsize(1)-1)*m$value({ordtab},defstep(1))
if inputr(2) .lt. inputr(1) then
   inputr(3) = inputr(1)
   inputr(1) = inputr(2)
   inputr(2) = inputr(3)
endif
SELECT/TABLE  {ORDTAB} (:X.GE.{inputr(1)}.AND.:X.LE.{inputr(2)}) {SESSOUTV}


inputr(1) = m$value({ordtab},defstart(2))+(scan(1)-1)*m$value({ordtab},defstep(2))
inputr(2) = m$value({ordtab},defstart(2))+(scan(2)-1)*m$value({ordtab},defstep(2))
if inputr(2) .lt. inputr(1) then
   inputr(3) = inputr(1)
   inputr(1) = inputr(2)
   inputr(2) = inputr(3)
endif
SELECT/TABLE  {ORDTAB} SELECT.AND.(:Y.GE.{inputr(1)}.AND.:Y.LE.{inputr(2)}) {SESSOUTV}

WRITE/KEYW IN_A    {ORDTAB}
WRITE/KEYW OUT_A   middummw.tbl
INPUTI(1) = ECHORD(1)

RUN STD_EXE:necregr

COPY/TABLE  &w  &s    >null
SORT/TABLE  &s  :RMS
ORDER = ECHORD(1)/2
if echord(1) .eq. 1 order = 1
RMSMAX = 3.5*{middumms.tbl,:RMS,@{ORDER}}
IF RMSMAX .LT. 0.05  RMSMAX = 0.05

!WRITE/OUT "*** Order by order linear rms ***"
!READ/TABLE &w
IF OUTMODE(1:1) .NE. "S" WRITE/OUT "Maximum admissible rms : {RMSMAX}"

DO ORDER = 1  {ECHORD(1)}

 RMS = {middummw.tbl,:RMS,@{ORDER}}
 IF RMS .GT. RMSMAX THEN
   IF OUTMODE(1:1) .NE. "S" WRITE/OUT   "Warning :   Rejected order number {ORDER}. Rms = {RMS} pixels"
   NBREJ = NBREJ + 1
   REJECT({NBREJ}) = ORDER
 ENDIF

ENDDO


DO ORDER = 1 {NBREJ}
    SELECT/TABLE {ORDTAB} SELECT.AND.:ORDER.NE.{REJECT({ORDER})} {SESSOUTV}
ENDDO

INPUTI(1) = DEFPOL(2) + 1      ! Degree Y + 1
INPUTI(2) = ECHORD(1) - NBREJ  ! Number of orders minus rejected orders
IF INPUTI(1) .GT. INPUTI(2) THEN
   WRITE/OUT "*****************************************************"
   WRITE/OUT "**** Warning : Number of selected orders {INPUTI(2)}
   WRITE/OUT "**** is too small for the current value of echelle
   WRITE/OUT "**** parameter DEFPOL(2)={DEFPOL(2)}
   WRITE/OUT "*****************************************************"
   HELP/ECHELLE DEFPOL
ENDIF

REGRE/POLY  {ORDTAB} :Y :X,:ORDER {DEFPOL(1)},{DEFPOL(2)}  KEYLONG  {SESSOUTV}
SAVE/REGR   {ORDTAB} COEFF  KEYLONG
COMPUTE/REGR   {ORDTAB} :YFIT     = COEFF
COMPUTE/TABLE    {ORDTAB} :RESIDUAL = :Y - :YFIT
THRES = {P4} * KEYLONGR(5)
IF THRES(1) .GT. {P3}   THRES = {P3}
SELE/TABLE    {ORDTAB}  ABS(:RESIDUAL).LT.{THRES}  {SESSOUTV}

IF {P2} .LT. 1  GOTO END

WRITE/OUT "Kappa-Sigma clipping..."
DO CNT = 1   {P2}
   REGRE/POLY  {ORDTAB} :Y :X,:ORDER {DEFPOL(1)},{DEFPOL(2)}  KEYLONG  {SESSOUTV}
   
   IF OUTMODE(1:1) .NE. "S" WRITE/OUT "Ndata = {KEYLONGI(1)} - Rms = {KEYLONGR(5)} pixels"
   SAVE/REGR   {ORDTAB} COEFF   KEYLONG
   COMPUTE/REGR   {ORDTAB} :YFIT = COEFF 
   COMPUTE/TABLE    {ORDTAB} :RESIDUAL = :Y - :YFIT
   THRES = {P4} * KEYLONGR(5)
   IF THRES(1) .GT. {P3}   THRES = {P3}
   SELE/TABLE    {ORDTAB}  ABS(:RESIDUAL).LT.{THRES} {SESSOUTV}
ENDDO
!
END:
-PURGE      {ORDTAB}
!
RETURN

