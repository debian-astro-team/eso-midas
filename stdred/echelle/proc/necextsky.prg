! @(#)necextsky.prg	19.1 (ES0-DMD) 02/25/03 14:19:37
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       necextsky.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, Sky definition
!.PURPOSE     Extracts sky spectrum. Windows are defined 
!             by DEFINE/SKY.
!             As soon as line table(s) are specified by P4 a separate
!             rebinning of the extracted sky orders will be performed
!             before averaging the extracted orders of the two sky
!             windows. Set P4 to + in order to switch the rebinning off
!             (default: P4=+)
!             EXTRACT/SKY input output 
!.VERSION     1.0    Creation    26.08.94 PB-ESO/DMD
!             2.0    Do a rebinning of the extracted sky orders before
!			averaging of the two windows.
!-------------------------------------------------------
!
SET/FORMAT I1 F20.12
DEFINE/PARAM P1 ?  IMA      "Input Image name:"
DEFINE/PARAM P2 ?  IMA      "Output Image name:"
DEFINE/PARAM P3 AVE CHAR    "Mode (Average/Filter):"
DEFINE/PARAM P4 +   CHAR    "Line table(s) to be used for rebinning:"
!
define/local ooff/R/1/1   {offset}
define/local nwind/I/1/1  {nsky} ? +lower
define/local oslit/R/1/1  {slit}
define/local omtd/C/1/12  {extmtd}
!
@s necdefsky,range nwind
verify/echelle {P1} IMA
!
if omtd(1:1) .eq. "O" then
   write/out "Warning: Extraction method EXTMTD=OPTIMAL is not valid"
   write/out "         for sky extraction. Using method AVERAGE"
   set/echelle extmtd=average
else 
   if extmtd(1:1) .ne. "A" .and. extmtd(1:1) .ne. "L" then
      write/out "Error: Invalid extraction method EXTMTD={EXTMTD}"
      return/exit
   endif
endif
!
IF P3(1:1) .EQ. "F" .AND. extmtd(1:1) .ne. "A" THEN
      write/out "Warning: Extraction method EXTMTD={extmtd} is not valid"
      write/out "         for particle hits cleaning. Using AVERAGE"
      set/echelle extmtd=average
endif
!
write/out "Temporarily modifying SLIT and OFFSET parameters"
!
IF P3(1:1) .NE. "F" THEN   ! Test on Filter option 

write/out "Option: Average"
write/out "Extracting first window..."
slit   = skywind(2) - skywind(1)
if slit .le. 0. then
   write/out "Error: Invalid parameters skywind(1)={skywind(1)} and"
   write/out "       skywind(2)={skywind(2)} yielding to slit={slit}"
   write/out "       Use command DEFINE/SKY to set sky offset limits."
   return/exit
endif
offset = (skywind(2) + skywind(1))/2.
extract/echelle {P1} &a 
!
if nwind .eq. 2 then
   write/out "Extracting second window..."
   slit   = skywind(4) - skywind(3)
if slit .le. 0. then
   write/out "Error: Invalid parameters skywind(3)={skywind(3)} and"
   write/out "       skywind(4)={skywind(4)} yielding to slit={slit}"
   write/out "       Use command DEFINE/SKY to set sky offset limits."
   return/exit
endif
   offset = (skywind(4) + skywind(3))/2.
   extract/echelle {P1} &b
endif

if P4(1:1) .ne. "+" then		!do the rebinning
  set/format I2
  outputi(1) = m$parse(P4,"lintab")
  if outputi(1) .gt. 1 then
    rebin/echelle &a &ra {SAMPLE} NONL {lintab01}
    if nwind .eq. 2 rebin/echelle &b &rb {SAMPLE} NONL {lintab02}
  else if outputi(1) .eq. 1 then
    rebin/echelle &a &ra {SAMPLE} NONL {lintab01}
    if nwind .eq. 2 rebin/echelle &b &rb {SAMPLE} NONL {lintab01}
  endif
  -rename middummra.bdf middumma.bdf
  if nwind .eq. 2 -rename middummrb.bdf middummb.bdf
endif


if nwind .eq. 1 then
   copy/ii &a  {P2}
else ! nwind .eq. 2
   compute/image {P2} = (&a + &b)/2.
endif

ELSE  ! Test on Filter option 

write/out "Option: Filter"
define/local cnt/I/1/1 0
set/format I1

write/out "Extracting first window..."
slit   = (skywind(2) - skywind(1))/3.
do cnt = 1 3
   offset = skywind(1) + slit/2. + (cnt-1.)*slit
   extract/echelle {P1} &s{cnt}
enddo

if nwind .eq. 2 then
   write/out "Extracting second window..."
   slit   = (skywind(4) - skywind(3))/3.
   do cnt = 1 3
     offset = skywind(3) + slit/2. + (cnt-1.)*slit
     extract/echelle {P1} &t{cnt}
   enddo
endif

if nwind .eq. 1 then 
   average/image {P2} = &s1,&s2,&s3 ? ? median,1,0,index
endif
if nwind .eq. 2 then
   average/image {P2} = &s1,&s2,&s3,&t1,&t2,&t3 ? ? median,2,1,index
endif

ENDIF    ! Test on Filter option 

copy/dd {P1} *,3 {P2}

set/echelle extmtd={omtd} slit={oslit} offset={ooff}

RETURN
