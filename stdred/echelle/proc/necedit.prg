! @(#)necedit.prg	19.1 (ES0-DMD) 02/25/03 14:19:37
! @(#)necedit.prg	19.1  (ESO)  02/25/03  14:19:37
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echedit.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command EDIT/ECHELLE
!.VERSION     1.0    Creation    22-AUG-1991  PB
!
!-------------------------------------------------------
!
define/param  P1   {OBSTAB}    C    "Table name:"

IF M$INDEX(P1,".") .LT. 1  P1 = "{P1}.tbl"

IF M$EXIST(P1) .NE. 1  THEN

    WRITE/OUT   "Table {P1} does not exist. Created..."
    create/table  {P1}   2    1000
    create/column  {P1}   :INPUT     C*30
    create/column  {P1}   :OUTPUT    C*30

ENDIF

EDIT/TABLE   {P1}
COPY/TABLE   {P1}  &s
SELECT/TABLE &s    (:INPUT.NE.NULL)
COPY/TABLE   &s    {P1}

RETURN


