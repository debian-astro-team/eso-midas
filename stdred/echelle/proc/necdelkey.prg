! @(#)necdelkey.prg	19.1 (ES0-DMD) 02/25/03 14:19:36
! @(#)necdelkey.prg	19.1  (ESO)  02/25/03  14:19:36
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echdelkey.prg
!.AUTHOR      Klaus Banse,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Delete keywords of context echelle.
!.VERSION     1.0    Creation    940712
!-------------------------------------------------------
!
!  has to be synchronized with `echinit.prg'  !!!
! 
delete/keyw  ORDTAB,baktab,lintab,ordref,flat,correct,blaze
delete/keyw  WLC,lincat,obj,instr,grating,cross,ccd,ccdbin
delete/keyw  OBSTAB,ffopt,mgopt,scan,echord,imsize
delete/keyw  WLRANGE,defmtd,defpol,thres1,width1
delete/keyw  SLOPE,nbordi,widthi,thresi,zebcol
delete/keyw  ZEBWIND,zebslope,zebwidth,zebstep,zebvisu
delete/keyw  CRFILT,medfilt,ccdfilt,bkgmtd,bkgvisu,bkgstep
delete/keyw  BKGRAD,bkgdeg,bkgsmo,bkgpol,bkgnit
delete/keyw  EXTMTD,slit,offset,ron,gain,extsigma
delete/keyw  SEAMTD,width2,thres2,wlcmtd,guess,dc
delete/keyw  TOL,wlciter,wlcvisu,wlcniter
delete/keyw  TWODOPT,avdisp,wcenter,beta,relord
delete/keyw  ABSORD,rebmtd,sample,respopt
delete/keyw  RESPMTD,std,response,fluxtab,filtmed,filtsmo
delete/keyw  PIXNUL,ripmtd,ripfrz,ripk,alpha
delete/keyw  LAMBDA1,lambda2,riprad,ripord,ripref
delete/keyw  RIPBIN,mrgmtd,delta,mrgord
delete/keyw  OBJECT,uppsky,lowsky
