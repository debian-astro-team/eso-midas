! @(#)necplot.prg	19.1 (ES0-DMD) 02/25/03 14:19:40
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echplot.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command PLOT/ECHELLE
!.VERSION     1.0    Creation    22-AUG-1991  PB
!
!-------------------------------------------------------
!
DEFINE/PARAM  P1    ?        IMA    "Input image:"
DEFINE/PARAM  P2   1,1        N     "Start and end order number:"
DEFINE/PARAM  P3   +          C     "Printer:"
DEFINE/PARAM  P4   ECHELLE    C     "Mode (ECHELLE/IDENT):"

DEFINE/LOCAL    I/I/1/2        {P2},{P2}
DEFINE/LOCAL    LOOP/I/1/1     0
DEFINE/LOCAL    ST/D/1/1       0.
DEFINE/LOCAL    IEX/I/1/1      0
DEFINE/LOCAL    ERRFLAG/I/1/1  0
DEFINE/LOCAL    NAME/C/1/40    {P1}

! Initial checks

graph/spec

IF M$INDEX(NAME,".") .LT. 1  NAME = "{NAME}.bdf"

IEX = M$EXISTD(NAME,"WSTART")

IF IEX .EQ. 0 THEN
   VERIFY/ECHELLE  {NAME}   EXTR
ELSE
   VERIFY/ECHELLE  {NAME}   REBI
ENDIF

IF I(1) .LT. 1         ERRFLAG = 1
IF I(2) .GT. ECHORD(1) ERRFLAG = 1

IF ERRFLAG .NE. 0  THEN
   WRITE/OUT  "PLOT/{P4}: Incorrect range for order number {I(1)},{I(2)}"
   RETURN/EXIT
ENDIF

! Plot the orders

ST  = {{NAME},START(1)}
set/format I1

DO LOOP = {I(1)} {I(2)}

   IF IEX .EQ. 1  {NAME},START(1) = {{NAME},WSTART({LOOP})}
   PLOT/ROW  {NAME}  @{LOOP}
   IF P4(1:1) .EQ. "I" THEN   ! Mode = IDENT
      SELECT/TABLE {LINTAB}  (:Y.EQ.{LOOP}.AND.:SELECT.GE.1)
      IF IEX .EQ. 1 THEN  
             OVERPLOT/IDENT {LINTAB} :WAVEC  :IDENT
      ELSE
             OVERPLOT/IDENT {LINTAB} :X      :IDENT
      ENDIF
   ENDIF


   IF LOOP .NE. I(2) THEN
      INQUIRE/KEYW INPUTC/C/1/20 -
      "Plotted order number {LOOP}. (Type <Return> to continue)"
   ELSE
      write/out "Plotted order number {LOOP}."
   ENDIF

   IF P3(1:1) .NE. "+"  THEN  ! Printer defined
      COPY/GRAPH {P3}
   ENDIF

ENDDO

IF IEX .EQ. 1  {NAME},START(1) = {ST}

RETURN


