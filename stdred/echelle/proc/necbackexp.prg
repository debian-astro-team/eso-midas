! @(#)necbackexp.prg	19.1  (ESO)  02/25/03  14:19:35
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       backexp.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Estimates interorder background using interorder mask 
!             definition by strong smoothing.
!             (from an algorithm of J. Wampler   ESO - Garching)
!             BACK/SMOOTH  in_frame  out_frame  [radx,rady] [niter] [Visu]
!.VERSION     1.0    Creation    910510
!
!-------------------------------------------------------
!
define/param  P1   ?                        I  "Input image:"
define/param  P2   ?                        I  "Output background image:"
define/param  P3  {BKGRAD(1)},{BKGRAD(2)}   N  "X and Y smoothing radius"    0,100
define/param  P4  {BKGNIT}         N     "Number of replacement iterations"  1,100
define/param  P5  {BKGVISU(1:3)}   C     "Visualisation flag (Yes/No):"
!
write/keyw     BKGRAD/I/1/2   {P3}
write/keyw     BKGNIT/I/1/1   {P4}
write/keyw     BKGVISU/C/1/3  {P5}
define/local  erflg/I/1/1    1      ! Error flag
if BKGVISU(1:1) .EQ. "Y"  erflg =  0
if BKGVISU(1:1) .EQ. "N"  erflg =  0
if erflg .ne. 0  then
   write/out  "BACK/SMOOTH: Parameter BKGVISU incorrect. Possible values: Yes,No"
   return/exit
endif
!
VERIFY/ECHELLE  {P1}  ! Check existence and size of image P1
!
define/local  n/i/1/1      0
define/local  ends/i/1/4   0,0,0,0
define/local  value/R/1/1  -1.0E+03    ! Replacement value
define/local  par4/R/1/2   {P3}
define/local  mid/I/1/1    0
define/local  last/i/1/1   0
define/local  name/C/1/10  SCAN

MID = {{P1},NPIX(1)}/2
IF P5(1:1) .EQ. "Y"   @ creifnot  3   heat

! Extract scanned part of input image
EXTRACT/IMAGE &a = {P1}[<,@{SCAN(1)}:>,@{SCAN(2)}]
COPY/DD       {P1}   LHCUTS   &a   LHCUTS
COPY/II       &a     &x

IF P5(1:1) .EQ. "Y" THEN
  SET/GRAPH      XAXIS   YAXIS
  PLOT/COLUMN      &a      @{MID}
ENDIF

! Create output image
COPY/II       {P1}      {P2}
!COPY/DD       {P1}    LHCUTS   {P2}   LHCUTS
COPY/DD       {P1}       *,3    {P2}
!
! Perform iterative filtering
DO N = 1  {P4}

FILTER/SMOOTH   &x    &m    {P3},0
COMPUTE/IMAGE     &c    =  &x / &m
! All pixels above 1.0  in &c were bright pixels 
! (particle hits, orders). A mask &d is prepared.
REPLACE/IMAGE     &c    &d   1.,>=1,0
! &e =  &m (smoothed frame) if the mask is set.
!    =  &x (original frame) if the mask is unset.
REPLACE/IMAGE     &x    &e   &d/0.9,>=&m

copy/dd       {P1}   LHCUTS         &e
IF M$EXISTD(P1,"DISPLAY_DATA") .EQ. 1  THEN
   COPY/DD       {P1}   DISPLAY_DATA   &e   DISPLAY_DATA
ENDIF

IF P5(1:1) .EQ. "Y"  THEN
    LOAD          &e
    OVERPLOT/COLUMN      &m     @{MID}
ENDIF

! The replaced frame &e becomes the original frame &x.
-COPY       middumme.bdf      middummx.bdf
! The smoothed frame in inserted in the output frame.
INSERT/IMAGE    &m     {P2}

ENDDO

RETURN




