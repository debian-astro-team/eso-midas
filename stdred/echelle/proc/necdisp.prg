! @(#)necdisp.prg	19.1 (ES0-DMD) 02/25/03 14:19:37
! @(#)necdisp.prg	19.1  (ESO)  02/25/03  14:19:37
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       echdisp.prg
!.AUTHOR      P. Ballester
!.KEYWORDS    Spectroscopy, Echelle, Display
!.PURPOSE     Check if an image is displayed and optionally
!             creates the display window and graphic window
!.VERSION     1.0 Creation 20.05.1992 (from A.Yu.Kniazev SAO USSR)
!             2.0          29.06.1992 (Use creifnot)
!-----------------------------------------------------------------
!
DEFINE/PARAM  P1  ?  CHAR   "Image Name:"
DEFINE/PARAM  P2 NO  CHAR   "Create a graphic window:"
DEFINE/PARAM  P8  *  CHAR   "Image in display:"

@ creifnot 2 heat
IF P2(1:1) .EQ.  "Y"     graph/spec
clear/chan over

define/local size/R/1/2    0.,0.
define/local scale/R/1/2   0.,0.
define/local iscale/I/1/2  0,0

size(1) = ididev(2)
size(2) = ididev(3)

scale(1) = 0.95*{{P1},NPIX(1)}/size(1)
scale(2) = 0.95*{{P1},NPIX(2)}/size(2)

if scale(1) .lt. 1. then 
   scale(1) = 1./scale(1)
else
   scale(1) = 0. - scale(1)
endif

if scale(2) .lt. 1. then
   scale(2) = 1./scale(2)
else
   scale(2) = 0. - scale(2)
endif

iscale(1) = scale(1) - 1.
iscale(2) = scale(2) - 1.

load/image {P1} scale={iscale(1)},{iscale(2)} center=c,c

RETURN







