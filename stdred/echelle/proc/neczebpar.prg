! @(#)neczebpar.prg	19.1 (ES0-DMD) 02/25/03 14:19:44
!! @(#)neczebpar.prg	19.1  (ESO)  02/25/03  14:19:44
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       zebpar.prg
!.AUTHOR      A.Yu.Kniazev SAO USSR
!.KEYWORDS    Spectroscopy, Echelle, Order detection
!.PURPOSE     - Pre analysis of the order reference frame
!       execute the command :
!       ANALYZE/ZEBRA FRAME COLUMN HALF PLOT
!       where:
!       FRAME   - input image name
!       COLUMN  - the number of center column in input region
!       HALF    - the half of width in input region
!       PLOT    - plot or no the results of work
!.VERSION     1.0    Creation    20.05.1992
!-------------------------------------------------------
!
DEFINE/PARAM   P1  {ORDREF} CHAR  "Order Reference frame:"
SET/ECHELLE  ORDREF={P1}
VERIFY/ECHELLE {P1}
!
SET/FORMAT I1
!           check for plot
IF P4(1:1) .EQ. "Y" THEN
   @ creifnot 1 
ENDIF
!

INPUTI(1) = IMSIZE(1)*ZEBCOL
INPUTI(2) = IMSIZE(1)*ZEBWIND
WRITE/KEYW  IN_A  {ORDREF}
WRITE/KEYW  INPUTC/C/1/1  {ZEBVISU}

RUN STD_EXE:NECZEBPAR

ZEBSLOPE = OUTPUTR(1)
ZEBWIDTH = OUTPUTR(2)

RETURN


