! @(#)necflat.prg	19.1 (ES0-DMD) 02/25/03 14:19:38
! @(#)necflat.prg	19.1  (ESO)  02/25/03  14:19:38
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echflat.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command FLAT/ECHELLE [flat] [flatcor] [blaze]
!.VERSION     1.0    Creation    02-SEP-1991 PB
!
!-------------------------------------------------------
!
IF P1 .NE. "?"  SET/ECH  FLAT={P1}
IF P2 .NE. "?"  SET/ECH  CORRECT={P2}
IF P3 .NE. "?"  SET/ECH  BLAZE={P3}

IF FFOPT(1:1) .NE. "Y" THEN
   WRITE/OUT  "Parameter FFOPT set to YES"
ENDIF

FFOPT = "NO "

REDUCE/SIMPLY  {FLAT}  &g  {CORRECT} 

FILTER/SMOOTH   &g {BLAZE}  10,0

FFOPT = "YES"

RETURN



