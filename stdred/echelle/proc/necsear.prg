! @(#)necsear.prg	19.1 (ESO-DMD) 02/25/03 14:19:43
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! ECHELLE PROCEDURE : ECHSEAR.PRG
! J.D.Ponz			version 1.0 131084
!   M. Peron                                200390
! .PURPOSE
!
! execute the command :
! SEARCH/ECHELLE image params
!
! 021213	last modif
! -------------------------------------------------
!
define/param  p1  ?                   ima    "Input frame:"
define/param  p2  {width2},{thres2}   numb   "Width, Threshold"
define/param  p3  2.0                 numb   "Kappa:"
!
define/local  k/r/1/2  {p2}
width2 = k(1)
thres2 = k(2)
!
if width2 .le. 0. error/echelle SEARCH/ECHELLE WIDTH2
!
verify/echelle {p1}  EXTR
!
if thres2 .le. 0 error/echelle  SEARCH/ECHELLE  THRES2
! 
! Search for emission lines
search/line {p1} {width2},{k(2)} {lintab} {seamtd} EMISSION 
! 
! Compute line position in pixels
copy/dd {ordtab} COEFFC,COEFFI,COEFFR,COEFFD {lintab} 
!
write/desc {lintab}  coeffi/i/4/2   1,2   
! 
! Written position of reference columns :X and :ORDER
compute/regr {lintab} :YNEW = COEFF
!
delete/descr {lintab}  PIXEL  ! This descriptor is cerated by SEARCH/LINE

