! @(#)necplspec.prg	19.1 (ES0-DMD) 02/25/03 14:19:40
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       echplspec.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, Sky definition
!
!.PURPOSE     Plots an echelle spectrum in wavelength
!             range
!
!.VERSION     1.0    Creation    26.08.94 PB-ESO/DMD
!-------------------------------------------------------
!
define/param P1 ? IMA "Rebinned image:"
!define/param P2 ? NUMB "Start,End wavelength"

verify/echelle {P1} REBIN

define/local over/I/1/1 0
define/local PL/I/1/1   0
define/local WST/D/1/4  0.,0.,0.,0.
define/local NP/I/1/3   0,0,0
define/local STEP/D/1/1 0.

set/graph pmode=1 font=1

copy/dk {P1} npix/I/1/2 NP/I/2/2
copy/dk {P1} step/D/1/1 step
copy/dk {P1} WSTART  STARTW

if P2(1:1) .NE. "?" then
    write/key WST/D/1/2 {P2}
    set/graph xaxis={wst(1)},{wst(2)}
else
    wst(1) = plrstat(1)
    wst(2) = plrstat(2)
endif


DO NP = 1 {NP(3)}
   OVER = 0
   WST(3) = STARTW({NP})
   WST(4) = WST(3) + (NP(2)-1.)*STEP


   IF WST(1) .LE. WST(4) .AND. WST(2) .GE. WST(3) over = 1

   if over .eq. 1 then
      average/row &a = {P1} @{NP},@{NP}
      copy/kd     wst/D/3/1 &a start
      if pl .eq. 0 then
          pl  = 1
          set/graph color={pl}
          plot &a
      else
          if pl .eq. 1 then 
             pl = 4
          else
             pl = 1
          endif
          set/graph color={pl}
          over/row &a
      endif
    endif
    if wst(3) .gt. wst(2) return
    enddo
    return







