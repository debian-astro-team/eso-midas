! @(#)necdemo.prg	19.1 (ESO-DMD) 02/25/03 14:19:36
! Prodedure: necdemo.prg
! Author   : P. Ballester  -  ESO Garching
! Date     : 910221		990805 KB
!
! Purpose  : Demonstration procedure of new echelle package
!
! Syntax   : tutorial/echelle
!
!
DEFINE/PARAM P1 INTER CHAR "mode (Auto/Interactive):"
!
! ECHO/OFF
!
DEFINE/LOCAL PRINT/C/1/6   LASERD
DEFINE/LOCAL LOOP/I/1/1    0
DEFINE/LOCAL LINE/R/1/3    0,0,0       ?   +lower
DEFINE/LOCAL EXIST/I/1/1   0
DEFINE/LOCAL NAME/C/1/30   HAY
DEFINE/LOCAl ANSW/C/1/3     "HAY"
DEFINE/LOCAL WLCMODE/I/1/1    0
define/local MODEMO/C/1/5     {P1}       ?    +lower

@ creifnot 3  heat   ! Creates graphic and display window
CLEAR/CHANNEL OVER

! 
!  get the necessary data files from $MID_HOME/demo/raw
! 
WRITE/OUT "Transfering demo images from MID_TEST..."

@s necdemo,cpy MID_TEST:casth.fits  casth.bdf
@s necdemo,cpy MID_TEST:casff.fits  casff.bdf
@s necdemo,cpy MID_TEST:casobj.fits casobj.bdf
@s necdemo,cpy MID_TEST:casstd.fits casstd.bdf

@s necdemo,cpy MID_TEST:lincat06.tfits thar.tbl
@s necdemo,cpy MID_TEST:ltt1020.tfits  ltt1020.tbl
!
SELECT/TABLE  thar     ALL
SELECT/TABLE  ltt1020  ALL

ECHO/ON

INIT/ECHELLE
SESSDISP = "NO "
ECHO/SESSION SILENT

ECHO/OFF
WRITE/OUT "Echelle commands are executed in Silent mode during this"
WRITE/OUT "tutorial and will remain so if the execution is interrupted"
WRITE/OUT "Normal output mode is initialized  by the command INIT/ECHELLE."

@s necdemo,stop

SELECT/TABLE ltt1020 ALL
WRITE/OUT  "   "
WRITE/OUT  " Session keywords can be set with the command SET/ECHELLE"
WRITE/OUT  " Keywords description is provided by HELP/ECHELLE <key>"
WRITE/OUT  "   "
ECHO/ON

SET/ECHELLE  ORDREF=casff 
HELP/ECHE    ORDREF

ECHO/OFF

WRITE/OUT "  "
WRITE/OUT "Order positions are determined with the commands "
WRITE/OUT "DEFINE/ECHELLE or DEFINE/HOUGH. The command SCAN/ECHELLE"
WRITE/OUT "allows to define the limits of the detector scanned area."
@s necdemo,stop

ECHO/ON
SCAN/ECHELLE casff 3,321

DEFINE/HOUGH

ECHO/OFF

CREATE/TABLE    middummr    3   4
CREATE/COLUMN    middummr    :X
CREATE/COLUMN    middummr    :Y
CREATE/COLUMN    middummr    :IDENT
WRITE/KEYW     LINE/R/1/3  32,236,88
COPY/KT       LINE/R/1/3  middummr    :X :Y :IDENT  @1
WRITE/KEYW     LINE/R/1/3  397,236,6416.315
COPY/KT       LINE/R/1/3  middummr    :X :Y :IDENT  @2
WRITE/KEYW     LINE/R/1/3  26,39,98
COPY/KT       LINE/R/1/3  middummr    :X :Y :IDENT  @3
WRITE/KEYW     LINE/R/1/3  353,38.9,5760.55
COPY/KT       LINE/R/1/3  middummr    :X :Y :IDENT  @4
COMPUTE/TABLE    &r  :DX = 6.
COMPUTE/TABLE      &r  :DY = 7.5
COMPUTE/TABLE      &r  :SEQU = sequ
WRITE/DESCR   middummr.tbl  WLCMTD/C/1/12  PAIR

SET/ECHELLE  WLC=casth  LINCAT=thar  FLUXTAB=ltt1020
SET/ECHELLE  CORRECT=ffcor  BLAZE=blaze    RESPONSE=response
SET/ECHELLE  STD=casstd  SLIT=5.
SET/ECHELLE  WIDTH1=6 THRES1=2000 THRES2=60 

LOAD casth
CLEAR/CHAN OVER

IF MODEMO(1:1) .NE. "A" THEN
 LOAD/TABLE middummr :X :Y :IDENT 1 10

 WRITE/OUT  "Identifications to be used are currently displayed."
 WRITE/OUT  "You will be prompted to identify the occurences of two"
 WRITE/OUT  "lines in adjacent orders:"
 WRITE/OUT  " Orders 88/89  Wavelength: 6416.315"
 WRITE/OUT  " Orders 98/99  Wavelength: 5760.550"
 WRITE/OUT  "Order numbers are displayed on the left side and wavelengths"
 WRITE/OUT  "on the right side"
 WRITE/OUT  "Type <ENTER> when ready or AUTO to get a non interactive"
 WRITE/OUT  "wavelength calibration"

 INQUIRE/KEYW  ANSW/C/1/3   "Type key <ENTER>, or AUTO : "
 IF AUX_MODE(7) .NE. 0 THEN
   IF ANSW(1:1) .EQ. "A"  SET/ECH WLCMTD=RESTART
 ENDIF
ELSE
 ECHO/ON
 SET/ECH WLCMTD=LINEAR RELORD=9 ABSORD=93 WCENTER=6110 AVDISP=0.17
ENDIF

 ECHO/ON
 SET/ECH  THRES2=60 DC=3
 CALIBRATE/ECHELLE DONE

CLEAR/CHAN OVER    ! To be included in calibration

SET/ECHELLE  FLAT=casff SAMPLE=0.15 SLIT=5.0 BKGMTD=SPLINE BKGSMO=500.
SET/GRAPH     YAXIS=0.,2000.
FLAT/ECHELLE

SET/ECHELLE  FLUXTAB=ltt1020 STD=casstd
SET/ECHELLE  BKGRAD=6,2 BKGDEG=3    BKGSMO=500.
SET/GRAPH     YAXIS=0.,500.
RESPONSE/ECHELLE

load response  cuts=0.,30000.  scale=-2,16

SET/ECHELLE   SAMPLE=response  BKGSMO=500.
REDUCE/ECHELLE casobj spectrum

SESSDISP = "YES"


SET/GRAPH  BIN=OFF  YAXIS=0.,50.
PLOT/ROW  spectrum
SET/GRAPH  BIN=ON

!COPY/GRAPH {PRINT}
ECHO/OFF

RETURN

ENTRY CPY

DEFINE/PARAM   P1   ?   C  "Source:"
DEFINE/PARAM   P2   ?   C  "Object:"


IF M$EXIST(P2) .EQ. 0  THEN
        indisk/fits {p1} {p2} >Null
        WRITE/OUT "Copied/converted {P1} to {P2}"
ENDIF

RETURN

!*********************************************************

ENTRY STOP

IF MODEMO(1:1) .ne. "A" THEN
   INQUIRE/KEYW  ANSW/C/1/3   "Press return to Continue:"
ENDIF

RETURN
