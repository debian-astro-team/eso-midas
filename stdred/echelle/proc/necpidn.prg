! @(#)necpidn.prg	19.1 (ES0-DMD) 02/25/03 14:19:40
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echpidn.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command PLOT/IDENT 
!.VERSION     1.0    Creation    22-AUG-1991  PB
!
!-------------------------------------------------------
!
PLOT/ECHELLE  {P1}  {P2}  {P3}  IDENT
RETURN
 
