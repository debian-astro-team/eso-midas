! @(#)necselback.prg	19.1 (ES0-DMD) 02/25/03 14:19:43
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       selback.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Interactive selection of background locations
!.VERSION     1.0    Creation    20-FEB-1990
!
!-------------------------------------------------------
!
DEFINE/PARAM P1  NULL   C   "P1=ALL to select all locations:"
!
DEFINE/LOCAL ORDER/C/1/30 {BAKTAB}
DEFINE/LOCAL LOOP/I/1/1   0
DEFINE/LOCAL LAST/I/1/1   0
DEFINE/LOCAL XPOS/R/1/1   0
DEFINE/LOCAL YPOS/R/1/1   0

IF P1(1:3) .EQ. "ALL"  SELECT/TABLE  {ORDER} ALL
@ creifnot 2 heat
BEGIN:
CLEAR/CHANNEL OVER
LOAD/TABLE    {ORDER} :X   :YBKG

WRITE/OUT     "Point non desirable background locations, then Exit"

SET/MIDAS CLOVERLAY=NO
GET/CURSOR    &c
SET/MIDAS CLOVERLAY=YES

LAST = {middummc.tbl,TBLCONTR(4)}

IF LAST .EQ. 0   RETURN

DO LOOP = 1 {LAST}

   XPOS = {middummc,:X_COORD,@{LOOP}}
   YPOS = {middummc,:Y_COORD,@{LOOP}}
   SELECT/TABLE {ORDER} (select.and.(ABS(:X-{XPOS}).GE.2.5.OR.ABS(:YBKG-{YPOS}).GE.2.5))

ENDDO

CLEAR/CHANNEL OVER
LOAD/TABLE    {ORDER} :X   :YBKG
INQUIRE/KEYW   ANSW/C/1/3   "Is everything O.K. (y/n. Default:y) ?"

IF AUX_MODE(7) .EQ. 0 GOTO END

IF ANSW(1:1) .eq. "N" THEN
   WRITE/OUT "Add more selections"
   GOTO  BEGIN
ENDIF

END:
RETURN


