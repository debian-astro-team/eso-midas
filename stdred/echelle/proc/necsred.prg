! @(#)necsred.prg	19.1 (ES0-DMD) 02/25/03 14:19:43
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echredu.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command REDUCE/ECHELLE [frame] [spectrum] [correct]
!.VERSION     1.0    Creation    02-SEP-1991  PB
!
!----------------------------------------------------------------
!
DEFINE/LOCAL TIME/D/1/1     1.
DEFINE/LOCAL REDMTD/C/1/6   FULL
DEFINE/LOCAL BACKGR/C/1/40  HAY
DEFINE/LOCAL BAKFLG/I/1/1   0
DEFINE/LOCAL VALUE/C/1/3    HAY

DEFINE/PARAM  P1  ?       I   "Input image:"
DEFINE/PARAM  P2  ?       I   "Output spectrum:"
IF P3(1:1) .NE. "?"  REDMTD = "SIMPLE"

VERIFY/ECHELLE {P1}

! WRITE/DESCR {P1} START/D/1/2  1.,1.
! WRITE/DESCR {P1} STEP/D/1/2   1.,1.

! 1) Check observation time
VERIFY/ECHELLE {P1} OTIME
TIME = {{P1},O_TIME(7)}

! 2) Subtract background

SUBTRACT/BACKGR  {P1} &r  &s

! 3) Perform flat-field correction

IF FFOPT(1:1) .EQ. "Y"  THEN
   WRITE/OUT "Perform flat-field correction: CORRECT = {CORRECT}, BLAZE = {BLAZE}"
   VERIFY/ECHELLE {CORRECT}
   VERIFY/ECHELLE {BLAZE}   REBIN
   VERIFY/ECHELLE &s AGAINST {CORRECT}
   COMPUTE/IMAGE &t = &s/{CORRECT}
ELSE
-COPY  middumms.bdf middummt.bdf
ENDIF

! 4) Extraction and time normalization

EXTRACT/ECHELLE &t &u

COMPUTE/IMAGE &u = &u/{TIME}

! 5) Rebin and end of optional flat-field correction

REBIN/ECHELLE &u  &v 

IF FFOPT(1:1) .EQ. "Y"  THEN
   VERIFY/ECHELLE &v AGAINST {BLAZE}
   COMPUTE/IMAGE &w = &v * {BLAZE}
ELSE
-RENAME  middummv.bdf middummw.bdf
ENDIF

COPY/II &s {P3}
COPY/II &w {P2}
COPY/DD {P1} *,3 {P2}
RETURN

