! @(#)necorder.prg	19.1 (ES0-DMD) 02/25/03 14:19:40
! @(#)necorder.prg	19.1  (ESO)  02/25/03  14:19:40
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       necupdate.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command UPDATE/ECHELLE
!.VERSION     1.0    Creation    24-JAN-1994  PB
!
!-------------------------------------------------------
!
define/param  P1   ?    IMAG   "Reference image:"
define/param  P2   AUTO CHAR   "Offset: "

define/local test/I/1/1 0
define/local off/R/1/1  0.

test = M$TSTNO(P2)

if test .eq. 1 then
   off = {P2}
else
   offset/echelle {P1} P8=SILENT
   off = outputr(1)
endif

write/out "Table {ORDTAB} corrected for offset: {OFF}"
compute/table {ORDTAB} :Y = :Y + ({OFF})
regress/echelle

