! @(#)necavtab.prg	19.1 (ES0-DMD) 02/25/03 14:19:35
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       necavtab.prg (formerly t2avertbl.prg)
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, Background
!.PURPOSE     Command AVERAGE/TABLE
!.VERSION     1.0    Creation    5-OCT-1989  JDP
!
!-------------------------------------------------------
!
DEFINE/PAR P1 ? IMAGE  "Enter image:"
DEFINE/PAR P2 ? TABLE  "Enter table:"
DEFINE/PAR P3 ? ?      "Enter columns with positions:"
DEFINE/PAR P4 ? ?      "Enter output column:"
DEFINE/PAR P5 0 NUMBER
!
WRITE/KEY INPUTI/I/1/1 'P5'
!
RUN MID_EXE:topertbl
!
RETURN


