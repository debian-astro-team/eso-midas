! @(#)necdefi.prg	12.1 (ES0-DMD) 09/16/97 11:17:43
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echdefi.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, Order detection
!
!.PURPOSE     Execute the command:
!             define/echelle [frame] [w,t,s] [method] [degree]
!             METHOD is the search method as
!                         STD  -- If order width < interorder width
!                         COM  -- Otherwise
!             DEGREE is the degree of the 2D polynomial
!
!.VERSION     1.0    Creation    29.04.91
!-------------------------------------------------------
!
SET/FORMAT I1
DEFINE/PARAM P1 {ORDREF}                   IMA  "Order reference frame:"
DEFINE/PARAM P2 {WIDTH1},{THRES1},{SLOPE}  N    "W,T,S"
DEFINE/PARAM P3        STD                 C    "Method"
DEFINE/PARAM P4 {DEFPOL(1)},{DEFPOL(2)}    N    "degx,degy"

WRITE/KEYW    ORDREF/C/1/60  {P1}
DEFINE/LOCAL R/R/1/3        {P2}
WIDTH1 = R(1)
THRES1 = R(2)
SLOPE  = R(3)
WRITE/KEYW    DEFMTD/C/1/12  {P3}
WRITE/KEYW    DEFPOL/I/1/2   {P4}
!
VERIFY/ECHELLE {P1}
IF WIDTH1 .LE. 0  ERROR/ECHELLE DEFINE/TRACE WIDTH1
IF SLOPE  .LE. 0  ERROR/ECHELLE DEFINE/TRACE SLOPE
!
DISPLAY/ECHELLE {ORDREF}
!
SEARCH/ORDER {P1} {P2} {SESSOUTV} ! find order positions
!
! Transform pixel coordinates to world coordinates
define/local   start/D/1/2   0.,0.
define/local   step/D/1/2    0.,0.
copy/dk {ORDREF} START/D/1/2 start
copy/dk {ORDREF} STEP/D/1/2  step
compute/table {ORDTAB} :X = {START(1)} + (:X-1)*{STEP(1)}
compute/table {ORDTAB} :Y = {START(2)} + (:Y-1)*{STEP(2)}

copy/kd   START/D/1/2 {ORDTAB} DEFSTART/D/1/2 
copy/kd   STEP/D/1/2  {ORDTAB} DEFSTEP/D/1/2  
!
WRITE/OUT "   "
WRITE/OUT "Number of orders: {ECHORD(1)}"
WRITE/OUT "Fitting bivariate polynomial..."
REGRESS/ECHELLE

DEFINE/LOCAL  BKGV/C/1/3 {BKGVISU}
SET/ECH      BKGVISU=NO
LOAD/ECHELLE
SET/ECH      BKGVISU={BKGV}

PREP/BACK   ?   INI

RETURN



