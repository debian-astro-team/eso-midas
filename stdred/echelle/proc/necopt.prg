! @(#)necopt.prg	19.1 (ESO-DMD) 02/25/03 14:19:40
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! ECHELLE PROCEDURE : ECHOPT.PRG
!  Michele Peron	version 1.0   sep91
!  Sebastian Wolf	version 2.0   apr99
!
! .PURPOSE
!
! execute the command : level 1
! EXTR/OPT in out slit,ord1,ord2 ron,g,sigma table coeffs calcw
!
! .RETURN	-1 on error, 0 on successful return.
!
! .HISTORY
! 1999.04.08-SW	Compute the initial variance frame from
!		input frame, RON and GAIN. Here GAIN means the
!		inverse gain-factor, i.e. CONAD
! 1999.05.17-SW	Do an optimal extraction using the weights of an previous
!		call to EXTRACT/OPTIMAL. This may be done by setting P7=NO
!		This is necessary for flatfielding optimal extracted science
!		spectra.
! 1999.06.11-SW	Average the weights for each order and store in wbin.bdf
! 1999.09.08-SW	The variance of the input frame and the variance of the
!		extracted data will be determined in any case of P7
!		P7 may accept the name of the weight map (e.g. Y,name).
! 
! 020322	last modif
!
! --------------------------------------------------
!
define/param p1 ? I "Enter input image:"
define/param p2 ? I "Enter output extracted orders:"
define/param p3 ? N "Enter extraction pars(SLIT,ORD1,ORD2):"
define/param p4 {ron},{gain},{extsigma} N "Enter extraction pars (RON,G,SIGMA):"
define/param p5 {ordtab} TABLE "Enter auxiliary table:"
define/param p6 COEFF CHAR "Enter coeffs with order position:"
define/param p7 YES CHAR "Enter YES determine weights, NO apply weights:"
!
write/keyw in_a/c/1/60   {p1}
write/keyw out_a/c/1/60  {p2}
write/keyw out_b/c/1/60  " " all
write/keyw outputc/c/1/60  variance.bdf
write/keyw inputi/i/2/3  0,0
write/keyw inputi/i/1/3  {p3}
write/keyw inputr/r/1/3  {p4}
write/keyw in_b/c/1/60   {p5}
write/keyw inputc/c/1/8  {p6}

define/local ron/r/1/1 0.0
!
! compute the variance frame. The input frame is assumed to be only background
! subtracted (de-biased)
! The variance frame is in units of [ADU^2]:
!	V_ij = (RON / CONAD)^2 + |IN_ij|/CONAD ;
!	[RON] = e-, [CONAD] = e-/ADU, [GAIN] = ADU/e-
!
if inputr(2) .gt. 0.0 then
  ron = inputr(1) / INPUTR(2)	!RON in ADU
  ron = ron * ron			!RON^2 in ADU
  compute/image {outputc} = {ron} + abs({p1}) / {inputr(2)}
else
  write/out "Error bad CONAD to determine variance frame!"
  return -1
endif

inputi(4) = m$index(p7,",") + 1	!Check for the name of the weight map
if inputi(4) .gt. 1 then
  out_b = p7({inputi(4)}:)
else
  out_b = "weight.bdf"
endif

inputi(4) = 0			!Perform an optimal extraction with the
				!weights of a previous call to EXTRACT/OPTIMAL
				!(used for flatfields) if INPUTI(4) == 0

if p7(1:1) .eq. "Y" .or. p7(1:1) .eq. "y" -
   inputi(4) = 1		!otherwise determine the weights.

write/keyw out_a/c/1/60  {p2}

run STD_EXE:NECOPT

write/out "Optimal extraction done."

if inputi(4) .eq. 1 then
  define/local istp/i/1/1 1
  istp = m$value({out_b},npix(2)) / m$value({p2},npix(2))
  ! copy/dd {p2} *,3 {out_b}  (now done in necopt.exe)
  rebin/linear {out_b} wbin 1,{istp} 0,0
  copy/dd {p2} start,step wbin 
endif
!
return 0

