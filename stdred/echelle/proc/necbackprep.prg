!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991,2004 European Southern Observatory
!.IDENT       backprep.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Prepares a table of background reference positions
!.VERSION     1.0    Creation    26-JUL-1991
! 041015	last modif
!
!-------------------------------------------------------
!
DEFINE/PARAM  P1  {BKGSTEP}   N     "Step:"
DEFINE/PARAM  P2   NULL       C     "P2=INI to create a new table: " 
DEFINE/PARAM  P3  {BAKTAB}   TAB    "Output table:"
DEFINE/PARAM  P4  {ORDTAB}   TAB    "Input table:"
DEFINE/PARAM  P5   COEFF      C      "Descriptor name:"
DEFINE/PARAM  P6   0          N     "Extra orders"

! Input table (P4) is of type order.tbl and contains results of regres/echelle
! as stored in descriptors P5.

DEFINE/LOCAL   STEP/R/1/1  {P1}
DEFINE/LOCAL   NPOS/I/1/1   0
DEFINE/LOCAL   NROW/I/1/1   0
DEFINE/LOCAL   NORD/I/1/1  {ECHORD(1)}  ! Number of orders
DEFINE/LOCAL   NORD1/I/1/1  0
DEFINE/LOCAL   RADX/I/1/1   {BKGRAD(1)} ! Radius to measure the slope
DEFINE/LOCAL   NEWTAB/I/1/1 0  ! Logical flag: Creation of a new table

IF BKGSTEP .LT. 1  ERROR/ECHELLE PREPARE/BACKGROUND  BKGSTEP
SET/ECH  BKGSTEP={P1}

IF  M$INDEX(P3,".tbl") .LE. 1   P3 = "{P3}.tbl"

! Initial checks: Create a new table if:
!    P2 = INI
!    BAKTAB does not exist
!    SCAN or STEP  have been modified
! Otherwise: Return
!

IF  P2(1:3) .EQ. "INI"  GOTO CREATE

IF  M$EXIST(P3) .EQ. 0    THEN
     NEWTAB = 1
ELSE

  IF M$EXISTD(P3,"SCAN") .EQ. 1 THEN
     IF  {{P3},SCAN(1)} .NE. SCAN(1)   NEWTAB = 1
     IF  {{P3},SCAN(2)} .NE. SCAN(2)   NEWTAB = 1
  ENDIF

  IF M$EXISTD(P3,"STEP") .EQ. 1 THEN
     IF  {{P3},STEP(1)} .NE. STEP(1)   NEWTAB = 1
  ENDIF

  IF M$EXISTD(P3,"BKSTEP") .EQ. 1 THEN
     IF  {{P3},BKSTEP(1)} .NE. STEP(1)   NEWTAB = 1
  ENDIF

ENDIF

IF NEWTAB .EQ. 0    RETURN

! CREATION OF A NEW TABLE

CREATE:

WRITE/OUT "Creating table {BAKTAB}..."

NORD1 = NORD + 1 + {P6}*2
NPOS = IMSIZE(1)/STEP
NROW = NORD1*NPOS

define/local wordes/I/1/1 0
wordes = M$EXISTD("{ORDTAB}","DEFSTART")
if wordes .ne. 1 then
   write/out "World coordinate keywords not found. Writing descriptors"
   write/out "DEFSTART, DEFSTEP to table {ORDTAB} with default values 1,1"
   write/descr {ORDTAB} DEFSTART/D/1/2 1.,1.
   write/descr {ORDTAB} DEFSTEP/D/1/2  1.,1.
endif

define/local ABSTEP/D/1/2  0,0
ABSTEP(1) = M$ABS({{ORDTAB},DEFSTEP(1)})
ABSTEP(2) = M$ABS({{ORDTAB},DEFSTEP(2)})

CREATE/TABLE  middummt.tbl 5 {NROW}

! some echelle programs expect 25 chars for COEFFC...
write/descr middummt.tbl {p5}C/C/1/25 " " all

COPY/KD     SCAN/I/1/2    middummt.tbl    SCAN/I/1/2
COPY/KD     STEP/R/1/1    middummt.tbl    BKSTEP/R/1/1
compute/table middummt.tbl :ORDER = INT((seq-1)/{NPOS}+0.5) - 0.5 - {P6}
compute/table      middummt.tbl  :X    = (seq - {NPOS}*(:order+{P6}-0.5))*{STEP}
compute/table   &t :X = {{ORDTAB},DEFSTART(1)} + (:X-1)*({{ORDTAB},DEFSTEP(1)})
COMPUTE/TABLE   middummt.tbl  :XSTA = :X - {RADX}*{ABSTEP(1)}
COMPUTE/TABLE   middummt.tbl  :XEND = :X + {RADX}*{ABSTEP(1)}

COPY/DD {P4}  {P5}C  middummt.tbl  {P5}C
COPY/DD {P4}  {P5}I  middummt.tbl  {P5}I
COPY/DD {P4}  {P5}R  middummt.tbl  {P5}R
COPY/DD {P4}  {P5}D  middummt.tbl  {P5}D

! Compute central position
COMPUTE/REGR  middummt.tbl  :YBKG = {P5}
WRITE/DESCR   middummt.tbl  {P5}I/I/4/1    3   !  #3 = :XSTA
COMPUTE/REGR  middummt.tbl  :YSTA = {P5}
WRITE/DESCR   middummt.tbl  {P5}I/I/4/1    4   !  #4 = :XEND
COMPUTE/REGR  middummt.tbl  :YEND = {P5}

define/local limit/D/1/4 0.,0.,0.,0.,0.
define/local lim/D/1/2   0.,0.

lim(1) = {{ORDTAB},DEFSTART(1)}
LIM(2) = LIM(1) + ({{ORDTAB},DEFSTEP(1)})*(IMSIZE(1)-1)
if lim(1) .lt. lim(2) then
   limit(1) = lim(1)
   limit(2) = lim(2)
else
   limit(1) = lim(2)
   limit(2) = lim(1)
endif

LIM(1) = {{ORDTAB},DEFSTART(2)} + ({{ORDTAB},DEFSTEP(2)})*(SCAN(1)-1)
LIM(2) = {{ORDTAB},DEFSTART(2)} + ({{ORDTAB},DEFSTEP(2)})*(SCAN(2)-1)
if lim(1) .lt. lim(2) then
   limit(3) = lim(1)
   limit(4) = lim(2)
else
   limit(3) = lim(2)
   limit(4) = lim(1)
endif

SEL/TABLE    &t MIN(:XSTA,:XEND).GE.{limit(1)} {SESSOUTV}
select/table &t select.AND.MAX(:XSTA,:XEND).LE.{limit(2)}      {SESSOUTV}
SEL/TABLE    &t SELECT.AND.MIN(:YSTA,:YEND).GE.{limit(3)}      {SESSOUTV}
sel/table    &t select.AND.MAX(:YSTA,:YEND).LE.{limit(4)}      {SESSOUTV}

COPY/TABLE  middummt.tbl  {P3}  {SESSOUTV}

RETURN





