! @(#)necscan.prg	19.1 (ES0-DMD) 02/25/03 14:19:43
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle
!.PURPOSE     Writes down values of scanned part of images in keyword SCAN
!             Execute the command :
!             SCAN/ECHELLE    image    scan-param
!             scan-param can be:
!              NOSCAN    Set scan keyword to 1 and nrow
!              beg,end   Set scan keyword to beg and end
!              CURSOR    Interactive setting
!.VERSION     1.0    Creation    15-MAR-1991  PB
!-------------------------------------------------------
!
DEFINE/PARAM   P1   ?       I    "Input image"
DEFINE/PARAM   P2  NOSCAN   ?    "Scan parameters:"


DEFINE/LOCAL  VALUE/I/1/1   0
DEFINE/LOCAL  FLAG/I/1/1    0   ! Test if any action has been done

IMSIZE(1) = {{P1},NPIX(1)}
IMSIZE(2) = {{P1},NPIX(2)}

IF P2(1:1) .EQ. "N"  THEN
      WRITE/KEYW    SCAN/I/1/2   1,{{P1},NPIX(2)}
      FLAG = 1
ENDIF

IF P2(1:1) .EQ. "C" THEN
   AVERAGE/COLUMN  &t = {P1}  <,>
   @ creifnot  1  
   STAT/IMAGE  &t
   COMPUTE/IMAGE   &u = &t/{OUTPUTR(3)}
   SET/GRAPH    xaxis  yaxis=-1.,5.
   PLOT/ROW     &u
   WRITE/OUT   "In Graphic Window, point first lower, then upper edge"
   GET/GCURS   middummc,TAB  ? 2
   WRITE/KEYW     SCAN/I/1/2    0,0
   COPY/TK    middummc  :X_COORD  @1  SCAN/I/1/1
   COPY/TK    middummc  :X_COORD  @2  SCAN/I/2/1
   FLAG = 1
ENDIF

IF M$TSTNO(P2) .EQ. 1 THEN
    WRITE/KEYW  SCAN/I/1/2  {P2}
    FLAG = 1
ENDIF

IF FLAG .EQ. 0  THEN

   WRITE/OUT "*** SCAN/ECHELLE:  unknown option {P2}"
   WRITE/OUT "*** Possible options are N,C or <begin>,<end>"
   RETURN/EXIT

ENDIF

IF SCAN(1) .LT. 1  SCAN(1) = 1
IF SCAN(2) .GT. {{P1},NPIX(2)}  SCAN(2) = {{P1},NPIX(2)}

IF SCAN(1) .GT. SCAN(2)  THEN
   VALUE = SCAN(1)
   SCAN(1) = SCAN(2)
   SCAN(2) = VALUE
ENDIF

WRITE/OUT  "Scan parameters : {SCAN(1)},{SCAN(2)}"

RETURN





