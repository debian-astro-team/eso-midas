! @(#)necfilt.prg	19.1 (ESO-DMD) 02/25/03 14:19:38
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echfilt.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, Cosmic rays
!.PURPOSE     Execute the command :
! FILTER/ECHELLE IN  OUT 
! 
!.VERSION     1.0    Creation    02-09-1991   PB
! 021213	last modif
! 
!-------------------------------------------------------
!
DEFINE/PARAM P1 ?          I  "input image:"
DEFINE/PARAM P2 ?          I  "output  image:"
!
VERIFY/ECHELLE {P1}
!
WRITE/OUT "median filter"
!
FILTER/MEDIAN {P1} {P2} {MEDFILT(1)},{MEDFILT(2)},{MEDFILT(3)}  NA
!
WRITE/OUT "background estimate"
SUBTRACT/BACKGR  {P1}  &a  &b  
!
WRITE/OUT "echelle filter"
!
WRITE/KEYW IN_A            middummb 
WRITE/KEYW IN_B            {ORDTAB}
WRITE/KEYW INPUTC          COEFF
WRITE/KEYW OUT_A           'P2'
WRITE/KEYW INPUTI/I/1/3    {CRFILT(1)},{CRFILT(2)},{CRFILT(3)}
WRITE/KEYW INPUTR/R/1/3    {CCDFILT(1)},{CCDFILT(2)},{CCDFILT(3)} 
!
RUN STD_EXE:NECFILT

