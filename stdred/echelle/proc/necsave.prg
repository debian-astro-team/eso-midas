! @(#)necsave.prg	19.1 (ES0-DMD) 02/25/03 14:19:42
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echsave.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command SAVE/ECHELLE
!.VERSION     1.0    Creation    23-AUG-1991 PB
! 030626	last modif
!-------------------------------------------------------
!
define/param   p1    ?     C    "Session name :"
!
write/out Process tables are saved
!
!
-copy    {ordtab}      {p1}ORDE.tbl
-copy    {lintab}      {p1}LINE.tbl
if baktab(1:3) .ne. "+++" -copy {baktab} {p1}back.tbl
!
if m$existd("{p1}ORDE.tbl","ECHORD") .eq. 1 delete/descr {ordtab}  ECHORD  
! For compatibility with new type
SAVINI/ECHELLE {p1}ORDE.tbl  WRITE

