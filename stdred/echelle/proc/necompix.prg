! @(#)necompix.prg	19.1 (ES0-DMD) 02/25/03 14:19:40
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       
!.AUTHOR      Adriano Fontana OAR Roma
!.KEYWORDS    Spectroscopy, Echelle
!.PURPOSE     Compute pixel size of echelle orders
!             Execute the command :
!             PIXEL/SIZE    ord1,ord2
!
!.VERSION     1.0    Creation    12-DEC-1994  AF
!	      1.1    P2 - plot flag added 05-OCT-99 SW
!-------------------------------------------------------
!
DEFINE/PARAM   P1  {echord(4)},{echord(5)} N  "Input first,last order"
DEFINE/PARAM   P2  Y C "Plot Yes or No:"


DEFINE/LOCAL  VALUE/I/1/1   0
DEFINE/LOCAL  FLAG/I/1/20    0   ! Test if any action has been done

inputi(3) = 0
if P2(1:1) .eq. "Y" then
  graph/spec
  set/graph
  inputi(3) = 1
endif

write/key flag {inputi}
write/key inputi {p1}
   RUN STD_EXE:necompix
write/key inputi {flag}
