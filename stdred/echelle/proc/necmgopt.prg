! @(#)necmgopt.prg	19.1 (ES0-DMD) 02/25/03 14:19:39
define/param P1 ? IMA "Flux Calibrated Spectrum:"
define/param P2 ? IMA "Weighting image:"
define/param P3 ? IMA "Output image:"
define/param P4 {DELTA} NUMB "Zeroed interval (in Angstroems):"

define/local nbord/I/1/3 {{P1},NPIX(2)},0,0
define/local cnt/I/1/1   0
nbord(2) = nbord(1)-1
define/local zeroed/D/1/2 {P4},{{P1},STEP(1)}
define/local NSKIP/I/1/1  0

nskip = zeroed(1)/zeroed(2)
set/ech delta={P4}

merge/echelle {P1}  abcd ? NOAPPEND
merge/echelle {P2}  efgh ? NOAPPEND

copy/ii abcd0001 &res

do cnt = 1 {nbord(2)}

   nbord(3) = cnt+1
   write/out "Merging orders {cnt} and {nbord(3)}"
   merge/spectrum &res abcd{nbord(3)} &dfg {nskip} OPT efgh{cnt} efgh{nbord(3)}
   copy/ii &dfg &res

enddo

copy/ii &res {P3}
copy/dd {P1} *,3 {P3}

-DELETE abcd*.bdf
-DELETE efgh*.bdf

return
