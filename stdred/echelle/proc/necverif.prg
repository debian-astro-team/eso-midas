! @(#)necverif.prg	19.2 (ESO-DMD) 03/10/03 16:39:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       echcheck.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command CHECK/ECHELLE 
!.VERSION     1.0    Creation    30-JUL-1991  PB
! 030227	last modif
! 
!-------------------------------------------------------
!
define/param  P1   ?       I     "Input file:"
define/param  P2   FRAME   C     "File type" !FRAM,EXTR,REB,TAB,OTIME,AGAINST

if p2(1:1) .eq. "T" then		 ! Check if P1 exist as a table
   if m$index(p1,".tbl") .lt. 1  p1 = "{p1}.tbl"
   if m$exist(p1) .ne. 1 then
      write/out "Table {p1} not present or not accessible."
      return/exit
   endif
   return				 ! End of test for a table
endif


if m$index(p1,".bdf") .lt. 1  p1 = "{p1}.bdf"

if m$exist(p1) .ne. 1 then
   write/out "File {p1} not present or not accessible."
   return/exit
endif
! 
define/local  sum/i/1/1      0
define/local  errflag/i/1/1  0
define/local  answ/c/1/3     HAY
! 
if p2(1:1) .eq. "A" then		! Check image P1 against P3
   !
   if {{p1},naxis} .ne. {{p3},naxis}   then
      errflag = 1
   else
      if {{p1},start(1)} .ne. {{p3},start(1)}  errflag = 1
      if {{p1},step(1)}  .ne. {{p3},step(1)}   errflag = 1

      if {{p1},naxis} .eq. 2 then
         if {{p1},start(2)} .ne. {{p3},start(2)}  errflag = 1
         if {{p1},step(2)}  .ne. {{p3},step(2)}   errflag = 1
      endif
   endif
   !
   if errflag .eq. 1 then
      write/out "***************************************************"
      write/out "*** Error : Frames {P1} and {P3} do not match "
      write/out "***         Now displaying standard descriptors "
      write/out "***************************************************"
      read/descr {p1}
      write/out "***************************************************"
      read/descr {p3}
      !
      inquire/keyword answ/c/1/3   "Continue anyway (y/n,def=n) ? "
      if answ(1:1) .eq. "Y" then
         return
      else
         return/exit
      endif
   endif

else if p2(1:1) .eq. "F"  then          ! Check for a normal frame
   sum = imsize(1)+imsize(2)
   if sum .eq. 0 then              ! Initialize keyword IMSIZE
      imsize(1)  =  {{p1},npix(1)}
      imsize(2)  =  {{p1},npix(2)}
      write/out  "************************************"
      write/out  "Warning    : Update keyword IMSIZE  "
      write/out  "Dimensions : {IMSIZE(1)},{IMSIZE(2)}"
      write/out  "************************************"
   endif
   !
   sum = scan(1)+scan(2)
   if sum .eq. 0 then
      scan(1) = 1
      scan(2) = imsize(2)
      write/out  "******************************"
      write/out  "Warning: Update keyword SCAN  "
      write/out  "Limits : {SCAN(1)},{SCAN(2)}  "
      write/out  "******************************"
   endif
   !
   update/echelle {p1}

else if p2(1:1) .eq. "O" then		! Check for O_TIME descriptor
   ! An optional default observation time value can be passed through P3
   define/local time/d/1/1  1.
   define/local itime/i/1/1  1.
   !
   ! Check exposure time
   !
   itime = m$existd(p1,"o_time")
   if itime .eq. 1 then
      write/keyw time/d/1/1 {{p1},o_time(7)}
      if time .gt. 0.00001 THEN
         write/out "Exposure time : {time} secs."
         itime = 0
      endif
   else
      itime = 1
   endif
   !
   ! After the previous check, if itime = 1, the time has to be entered
   !
   if itime .eq. 1 then
      if m$existd(p1,"o_time") .ne. 1 write/descr {p1} o_time/d/1/8 0. all
      if p3(1:1) .eq. "?" then
         inquire/keyw time " Enter exposure time (in secs.):"
      else
        time = {p3}
        write/out "Enforced exposure time : {time} secs."
      endif
      write/descr {p1} o_time/d/7/1 {time}
   endif
   return
endif

