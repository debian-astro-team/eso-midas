! @(#)necsegment.prg	19.1 (ES0-DMD) 02/25/03 14:19:43
! 
define/param P1 {ORDREF} IMA "Input image:"
define/param P2 &i       IMA "Output image:"
define/param P3 3.5      NUM "Kappa factor:"
!
define/local  segment/R/1/1 0.
define/local  thres/R/1/1   0.
!
goto meth2
! 
! Method 1 : Defines a threshold by histogram analysis
meth1:
!
center/histo {P1}
segment = outputr(1) + {P3}*outputr(2)
replace/image {P1} {P2} <,{segment}=0.
copy/dd {P1} *,3 {P2}
return
! 
! Method 2 : Determines the background by wavelet transform
meth2:
!
scale/twodim {P1}  &w7  2  &w1  1 
compute/image &s = {P1} - &w7

center/histogram &w1
thres = outputr(1) + 2.*outputr(2)

replace/image &s {P2} <,{thres}=0.
copy/dd {P1} *,3 {P2}
return


