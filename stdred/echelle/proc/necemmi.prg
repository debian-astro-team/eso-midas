! @(#)necemmi.prg	19.1 (ES0-DMD) 02/25/03 14:19:37
! @(#)necorder.prg	1.1  (ESO)  3/2/95  11:31:45
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       necemmi.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command INIT/EMMI 
!.VERSION     1.0    Creation    24-JAN-1994  PB
!
!-------------------------------------------------------
!
define/param  P1  +  ?  
define/param  P8  *    IMA    "Displayed image:"
define/local  image/C/1/80 " " all
define/local  numb/I/1/1 1

if P1(1:1) .eq. "+" then
   write/keyw image {P8}
   verify/echelle {image} IMA
   numb = 0
else
   numb = M$TSTNO(P1)
   if numb .eq. 1 then
     define/param  P1   ?    NUMB  "Grating number (9,10):"   9,10
     define/param  P2   ?    NUMB  "Grism number (3,4,5,6):"  3,6
   else
     define/param  P1   ?  CHAR  "Image name:"
     write/keyw image {P1}
     verify/echelle {image} IMA
   endif
endif

define/local grating/I/1/1 0
define/local grism/I/1/1   0
define/local STRING1/C/1/10 "          " 
define/local STRING2/C/1/10 "          "
DEFINE/LOCAL index/I/1/2   0,0

if numb .ne. 1 then
  write/key string1 {{image},_EIG1_ID}
  index = M$INDEX(string1,"#") + 1
  index(2) = index(1) + 1
  grating = {string1({index(1)}:{index(2)})}
  write/key string2 {{image},_EIO9_ID}
  index = M$INDEX(string2,"#") + 1
  grism = {string2({index}:{index})}
else
  grating = {P1}
  grism   = {P2}
  write/key string1 #{grating}
  write/key string2 #{grism}
endif

set/format I1
write/out "Looking for Grating {grating}/Grism {grism} pre-calibrated solution"
init/echelle MID_EMMI:ech{grating}g{grism}

set/echelle instr=EMMI GRATING={string1} CROSS={string2}
write/descr {BAKTAB} BKSTEP/R/1/1 {BKGSTEP}

if numb .ne. 1 set/ech CCD="{{image},_ED_ID}"

