! @(#)necpov.prg	19.1 (ES0-DMD) 02/25/03 14:19:41
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       echpdis.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Command PLOT/DISPERSION 
!.VERSION     1.0    Creation    P.Ballester 940405
!
!-------------------------------------------------------
!
DEFINE/PARAM P1  ?  IMA  "Rebinned WLC spectrum:"
DEFINE/PARAM P2  1  NUM  "Order number:"
DEFINE/PARAM P3  0. NUM  "Offset: "
DEFINE/PARAM P4  +  NUM  "Wavelength range: "
DEFINE/PARAM P5  +  NUM  "Intensity range: "


VERIFY/ECH {P1} REB

@ creifnot 1
set/graph default
set/grap pmode=1 font=1

show/disp >Null

define/local xrat/r/1/1 1.0
xrat = 1.0*outputi(13)/625.0
define/local yrat/r/1/1 1.0
yrat = 1.0*outputi(14)/425.0

define/local XSZ1/r/1/1 -150.0
XSZ1 = XSZ1 * xrat
define/local XOF1/r/1/1 20.0
XOF1 = XOF1 * xrat
define/local YSZ1/r/1/1 -80.0
YSZ1 = YSZ1 * yrat
define/local YOF1/r/1/1 35.0
YOF1 = YOF1 * yrat
define/local XSZ2/r/1/1 {XSZ1}
define/local XOF2/r/1/1 {XOF1}
define/local YSZ2/r/1/1 -20.0
define/local YOF2/r/1/1 10.0
YSZ2 = YSZ2 * yrat

define/local x1/r/1/1 0
define/local y1/r/1/1 0
define/local xmin/r/1/1 0.0
define/local xmax/r/1/1 0.0
define/local ymin/r/1/1 -1.0
define/local ymax/r/1/1 6.0
define/local order/i/1/1 0
define/local ord1/i/1/1 0
define/local ord2/i/1/1 0


order = {P2} + 1
ord1  = {{p1},norder({p2})}
ord2  = {{p1},norder({order})}

average/row &a = {P1} @{P2},@{P2}
average/row &b = {P1} @{order},@{order}
compute/image &c = &b + {P3} 

write/descr &a START {{P1},WSTART({P2})}
write/descr &b START {{P1},WSTART({order})}
write/descr &c START {{P1},WSTART({order})}

xmin = {&a,START}
xmax = {&a,START} + {&a,STEP} * {&a,NPIX(1)}
if p4(1:1) .ne. "+" then
  write/keyw outputr/r/1/2 {p4}
  xmin = outputr(1)
  xmax = outputr(2)
endif

if P5(1:1) .eq. "+" then
  statist/imag &a ? ? ? SN
  ymin = outputr(3) - 2*outputr(4)
  ymax = outputr(3) + 2*outputr(4)
else
  outputi = m$parse(P5,"yr")
  ymin = {yr01}
  ymax = {yr02}
endif
if {p3} .lt. 0 ymin = ymin + {p3}
if {p3} .gt. 0 ymax = ymax + {p3}

set/format
set/graph color=1 xformat=NONE xaxis={xmin},{xmax} yaxis={ymin},{ymax}
plot/axes ? ? {XSZ1},{YSZ1},{XOF1},{YOF1}
outputr(1) = -XSZ1 / 2.0
outputr(2) = -YSZ1 + 5
label/grap "Overlapping Echelle Orders of: {p1}" {outputr(1)},{outputr(2)},mm 0 1.2
outputr(2) = -YSZ1 / 2.0
label/grap "Flux" -10,{outputr(2)},mm 90


set/format F7.2 I1
overplot/row &a
x1 = plrgrap(1) + 0.5*plrgrap(4)
y1 = ymax - plrgrap(8)
label/graph "Order: {ord1}" {x1},{y1} 0 1 1
set/gra clear=OFF

set/graph color=4 ltype=3
overplot/row &c
y1 = y1 - plrgrap(8)
label/graph "Order: {ord2}" {x1},{y1} 0 1 1

compute/imag &d = (&c - {p3} - &a) / &a >Null

xmin = {&d,START}
xmax = {&d,START} + {&d,STEP} * {&d,NPIX(1)}
!statistic/ima &d [{xmin}:{xmax}] ? ? SN
statistic/ima &d ? ? ? W
write/out "Mean deviation: {outputr(3)} +/- {outputr(4)}"
write/out "Median deviation: {outputr(8)} +/- {outputr(14)}"
set/graph cleargra=OFF yaxis=-0.5,0.5 colour=1 xformat=AUTO ltype=1
plot/axes ? ? {XSZ2},{YSZ2},{XOF2},{YOF2}
outputr(2) = -YSZ2 / 2.0
label/grap "Rel. deviation" -10,{outputr(2)},mm 90 1.5
outputr(1) = -XSZ2 / 2.0
label/grap "Wavelength [A]" {outputr(1)},-5,mm 0 1.5

overplot/row &d
x1 = plrgrap(1) + 0.5*plrgrap(4)
y1 = plrgrap(6) - 2*plrgrap(8)
label/grap "Median deviation: {outputr(8)} +/- {outputr(14)}" {x1},{y1} 0 2 1
OVERPL/LINE 2 {plrgrap(1)},0.1 {plrgrap(2)},0.1
OVERPL/LINE 2 {plrgrap(1)},-0.1 {plrgrap(2)},-0.1

SET/GRAPH


return


PLOT/ROW  &a
SET/GRAPH COLOR=4
OVERPLOT/ROW  &c
SET/GRAPH

RETURN

