% @(#)savinieche.hlq	19.1 (ESO-IPG) 02/25/03 14:18:40
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      savinieche.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: SAVINIT/ECHELLE
%.VERSION    1.0  09-MAR-95 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./ECHELLE
\es\co
SAVINIT/ECHELLE					      09-MAR-95  PB
\oc\su
SAVINIT/ECHELLE  ima,tab  mode
	saves/reads echelle session keywords as descriptor of an image/table
\us\pu
Purpose:    
            Low-level command allowing to save or retrieve the echelle
            session keyword from an image or table. The keywords are
            saved as descriptors of identical name in the Midas structure.
            This command is used in SAVE/ECHELLE and INIT/ECHELLE to
            save and initialize echelle sessions. The command can also be used
            to save the session parameters as descriptors of 
            a reduced observation.
\up\sy
Syntax:     
            SAVINIT/ECHELLE  ima,tab mode
 \\
\ys\pa
            ima,tab = image or table name, including extension (.tbl or .bdf)
 \\
\ap\pa
            mode    = READ or WRITE to read (write) the descriptors from (to)\\
                      the data structure.
 \\
\ap\sa
See also: 
            REGISTER/SESSION, SAVE/ECHELLE, INIT/ECHELLE
\as\no
Note:       
            None
 \\
\on\exs
Examples:
\ex
            SAVINIT/ECHELLE  order.tbl  WRITE
\xe \sxe


