% @(#)identieche.hlq	19.1 (ESO-IPG) 02/25/03 14:18:35
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      identieche.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, IDENT/ECHELLE
%.PURPOSE    On-line help file for the command: IDENT/ECHELLE
%.VERSION    1.0  08-FEB-1991 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./ECHE
\es\co
IDENT/ECHELLE                                      10-NOV-1994  PB
\oc\su
IDENT/ECHEL  [wlc] [lincat] [dc] [tol] [wlcloop] [wlcmtd] 
                                                  [guess,[shift]] [ccdbin]
	perform wavelength calibration of echelle spectra
\us\pu
Purpose:   Implement an instrument independent wavelength calibration for 
           echelle spectra. Line positions are assumed to be in the auxiliary 
           table line.tbl, as created by, e.g., SEARCH/ECHELLE.
\\ 
           The command works interactively in the display, assumed to be 
           existent. The command computes dispersion coefficients if the
           line identification has been performed successfully.
\up\sy
Syntax: 
           IDENT/ECH [wlc] [lincat] [dc] [tol] [wlcloop] [wlcmtd] 
                     [guess,[shift]] [ccdbin]
\ys\pa
           wlc       =  Name of wavelength calibration frame
\ap\pa
           lincat    =  Name of line catalog table, which must include at
                        least a column called :WAVE, providing laboratory
                        wavelengths of the calibration lamp used to
                        expose Wlc. The column :ORDER, required by command
                        IDENT/ECH is not used anymore.
\ap\pa
           dc        =  Degree of polynomial defining dispersion relation.
                        Maximum value is 3 and also the recommended value
                        for CASPEC.
\ap\pa
           tol       =  Tolerance on rms error of global relation,
                        Default value is 0.2.
\\
                        Tol is interpreted as follows:\\
                        If Tol > 0, Tol is considered in pixel units.\\
                        If Tol < 0, abs(Tol) is in wavelength units.
\\
                        Tol is involved at different steps of the loop:\\
                        - Standard deviation of the very first echelle
                          relation must be smaller than Tol. Otherwise,
                          the user is prompted whether he wants to 
                          "Start anyway".\\
                        - Tol controls the smallest wavelength window
                          when identifying lines using a global dispersion
                          relation.\\
                        - Before computing single-order dispersion relations,
                          lines with residuals larger than Tol will be
                          rejected.

                        Relaxing Tol value means accepting less accurate
                        global dispersion relation in the automatic line
                        identification and increases the risk of misiden-
                        tifications. Values larger than 1. pixel should
                        not be used.
\ap\pa
           wlcloop   =  initial tolerance, next neighbour, maxi error.
                        These three parameters control the loop of
                        lines identification at the global step.
\\
                        Initial tolerance (pixels) is used to check
                        the very first dispersion relation obtained from 
                        initial identifications. If the accuracy of this
                        starting relation is not better than the specified
                        tolerance, the user is prompted whether it is
                        possible to start anyway.
\\
                        The second element of wlcloop controls the
                        identification of lines by comparison of 
                        the residual of a line (computed wavelength
                        minus position of the next catalog line) and
                        the distance of the next neighbours in the 
                        line catalog and in the line table. If residual
                        is less than wlcloop(2) times the distance of
                        the next neighbour, the identification is confirmed.
\\                     
                        Maximal error (pixels) controls the ungraceful exit
                        of the identification loop. If the rms error of
                        the global relation becomes larger than the specified
                        limit (due to initial misidentifications or
                        distortions of the wavelength calibration frame),
                        the loop will be interrupted
\ap\pa
           wlcmtd    =  Starting method. Can be PAIR, ANGLE, TWO-D, GUESS, 
                        RESTART or ORDER. Default is PAIR. See note 1.
\ap\pa
            guess    =  Name of a previously saved session (cf. SAVE/ECH). 
                        The  global solution of this session will be used
                        to perform the initial line identification. Used
                        only with method Guess.
\\
                        An additional parameter shift can be added to the
                        name of the guess session, separated by a coma.
                        Its value indicates the shift in pixels between the
                        two sets of calibration lines. If no value is
                        provided, the shift is estimated by cross-correlation.
\ap\pa
            ccdbin   =  Bin factor of the CCD. This keyword is 
                        updated by the command ROTATE/ECHELLE.
\ap\sa
See also: 
            SEARCH/ECHELLE, PLOT/RESIDUAL, PLOT/IDENT, LOAD/IDENT,\\
            CORRELATE/LINE, PLOT/CALIBRATE, LOAD/CALIBRATE
\as\no
Note:       
            1. Presentation of the methods.

            The methods PAIR, ANGLE and TWO-D are interactive and 
            use the command CENTER/MOMENT with option CURSOR to point the
            lines. It is recommended to beginners to read the
            related help and use this command before identify/echelle.
            The three methods can be used if the disperser is a grating.
            Only the method TWO-D can apply if the disperser is a grism.
            In case of initial misidentifications, methods PAIR and
            ANGLE invoke a wavelength calibration diagnosis (see Note 3).

            In interactive modes, one must first point all the positions 
            to identify, then provide order number and wavelengths.

            In method PAIR, one must recognize two pairs of
            lines in overlapped regions of the spectrum and
            provide the absolute order number of the first
            pointed line and wavelengths for each of the two
            pairs. The geometry of the pairs provide the angle of
            orientation of the spectrum.

            In method ANGLE, one identify at least four
            lines anywhere and provide absolute order number of the
            first pointed line and the wavelengths of all lines. The
            angle of orientation of the spectrum is computed analytically.

            At the difference of PAIR and ANGLE which involve
            the echelle relation (product order by wavelength as
            a function of the x position), the method TWO-D
            fits from the beginning a bivariate polynomial to
            the identifications. Therefore, it requires more initial
            identifications than the above methods. The minimum number
            is given by (dc + 1)**2 + 1, where dc is the degree of
            the polynomial used to fit the dispersion relation.

            Method RESTART must be used only IMMEDIATELY AFTER the
            methods PAIR, ANGLE or TWO-D. It avoids the interactive
            pointing of the lines and enters the program at the step 
            of the identifications.

            Method GUESS is not interactive and can be used after
            any calibration by an interactive method. The method GUESS
            requires the name of a previously saved echelle session, which 
            process tables will be used to start automatically the
            lines identification. The shift between the two sets of 
            calibration lines is estimated by cross-correlation, unless 
            its value is provided (see parameter guess).

            Method ORDER allows to restart only the single-order
            solutions with a different parameter wlctol.

            2. Input parameters

            Echelle keywords are shown in section 6. Wavelength
            calibration of the SHOW/ECHELLE).If not provided, parameters 
            are read from echelle keywords. One additional parameter is 
            only read from the echelle session:\\
              -  number of orders (fixed by DEFINE/ECH).

            3. Initial identifications, Error diagnosis

            The command first try to fit an accurate global dispersion 
            relation from the minimum four identifications provided by 
            the user (either two times two lines in overlapped regions 
            of successive orders (PAIR), or four lines anywhere (ANGLE).

            If it appears that the initial relation is not accurate
            enough, the user will be prompted for corrective actions,
            which can consist of:\\
                  - starting anyway\\
                  - perform initial identifications again.
\on\no
Note:
            In methods PAIR and ANGLE, a wavelength calibration diagnosis
            is invoked. The diagnosis makes use of the redundancy in the
            input data to check whether one of the values is possibly
            wrong. Therefore, no more than one error can be recovered.
            By solving equations, all sets of (n-1) parameters are used
            to compute the optimal n-th parameter and the resulting
            rms obtained by replacement of the actual value by the 
            optimal value is computed. The interpretation of the diagnosis
            is normally done as follows: A linear fit of the echelle
            relation with the original values is computed. Read the
            rms of this fit and take it as reference value. The optimum
            for each parameter is computed. A diagnosis is possible if
            one of the values improves dramatically the rms. Then use the
            mathematical optimum as an indication of the correct value.
            A very common mistake in the initial identifications is the
            order number wrong by one.

            4. Identification loop

            The initial solution being accepted, an iterative loop
            improves the global solution by identifying more
            lines. The convergence is achieved when no new lines can
            be identified. Finally, order by order solutions
            are either fitted, or estimated from the global solution,
            depending on the number of lines available for each order.
            Residuals are computed for every order (of course unless
            there is no identified line in that order).

            5. Final solution

            All fits are performed using polynomials. Calibration
            results are written in the table line.tbl and will be used
            in next reduction steps or as a guess for another calibration.

            The command IDENTIFY/ECHELLE accepts an additional option 
            controlled by the session keyword WLCOPT, which
            value can be 1D or 2D. This keyword controls the method used to
            compute the coefficients of the single order coefficients: in 1D
            mode the coefficients are computed independently for each order,
            in 2D mode the coefficients are derived from a global 2D solution.
            The default value of WLCOPT is 1D. 

\on\exs
Examples:
\ex
            IDENT/ECHELLE mywlc  mycat  2  0.25  3.0  ANGLE
\xe\ex
            IDENT/ECHELLE P6=GUESS  P7=oldses,-1.
\xe\ex
            IDENT/ECH
\xe\sxe
            
           




            
