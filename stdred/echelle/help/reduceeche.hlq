% @(#)reduceeche.hlq	19.1 (ESO-IPG) 02/25/03 14:18:39 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      reduceeche.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, REDUCE/ECHELLE
%.PURPOSE    On-line help file for the command: REDUCE/ECHELLE
%.VERSION    1.0  05-MAR-1991 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./ECHELLE
\es\co
REDUCE/ECHELLE						25-SEP-1991 PB
\oc\su
REDUCE/ECHELLE input output [bkcor]
	reduction of echelle spectra.
\us\pu
Purpose:    
            Object reduction of echelle data.
            There are two available methods (parameter REDUCTION):
            STANDARD consists of the following steps
\\
                     . Background correction
\\
                     . Flat field correction (optional, depending on FFOPT)
\\
                     . Order extraction
\\
                     . Calibration in wavelengths
\\
                     . Response correction (optional, depending on RESPOPT)
\\
                     . Order merging
\\
            The parameters involved in this command are displayed by
            SHOW/ECHELLE. Before using the command, the following sections
            must be checked:

                  2. Background
                  4. Extraction
                  7. Rebin
                  8. Flat field correction
                  9. Response correction
                 10. Merging

\up\sy
Syntax:     REDUCE/ECHELLE input output [bkcor]
\ys\pa
            input    = raw image
\ap\pa
            output   = 1D reduced spectrum
\ap\pa
            bkcor    = if an image name is provided, a simplified reduction
                       is performed, including background correction,
                       extraction, rebin, and theoptional flat-field 
                       correction.
\ap\no
Note:       
\on\exs
Examples:
\ex
            REDUCE/ECHELLE raw spectrum  
\xe\sxe


