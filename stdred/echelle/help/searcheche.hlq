% @(#)searcheche.hlq	19.1 (ESO-IPG) 02/25/03 14:18:40 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      searcheche.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, SEARCH/ECHELLE
%.PURPOSE    On-line help file for the command: SEARCH/ECHELLE
%.VERSION    1.0  18-DEC-1986 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./ECHE
\es\co
SEARCH/ECHELLE						18-DEC-1986 JDP
\oc\su
SEARCH/ECHELLE frame [width2,thres2]
	search for emission lines
\us\pu
Purpose:    
            search for calibration lines in a wavelength calibration echelle
            frame in the space pixel-order. The positions the detected lines
            are stored in the auxiliary table LINE. The command uses
            internally SEARCH/LINE with the method GAUSSIAN (default) 
            or GRAVITY on EMISSION lines (See the parameter SEAMTD of the
            command SET/ECHELLE).
\up\sy
Syntax:     SEARCH/ECHELLE frame [width2,thres2]
\ys\pa
            frame  = input image corresponding to a extracted wavelength
                     calibration frame in the space pixel-order, as
                     produced by the command EXTRACT/ECHELLE
\ap\pa
            widht2 = is the approx. width of the lines along the dispersion
                     direction in pixels.
                     Echelle keyword is WIDTH2. Default is 5.
\ap\pa
            thres2 = is the detecting threshold relative to the local
                     background.
                     Echelle keyword is THRES2. Default is 50.
\ap\no
Note:       
            The command EXTRACT/ECHELLE has to be used previously
\on\exs
Examples:
\ex
            SEARCH/ECHELLE extwlc 6,100
\xe \sxe


