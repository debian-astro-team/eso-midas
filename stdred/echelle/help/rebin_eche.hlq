% @(#)rebin_eche.hlq	19.1 (ESO-IPG) 02/25/03 14:18:38 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      rebin_eche.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, REBIN/ECHELLE
%.PURPOSE    On-line help file for the command: REBIN/ECHELLE
%.VERSION    1.0  17-DEC-1986 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./ECHE
\es\co
REBIN/ECHELLE						20-OCT-1992  PB
\oc\su
REBIN/ECHELLE input output  sample
	rebin echelle orders into wavelength
\us\pu
Purpose:    
            Rebin extracted orders into linear wavelength steps. The
            frame is converted from the space pixel-order into the space
            wavelength-order
\up\sy
Syntax:     
            REBIN/ECHELLE input output  sample
\ys\pa
            input    = input image, sampled in the space pixel-order
                       Corresponds to the extracted orders
\ap\pa
            output   = output image, sampled in the space wavelength-order
\ap\pa
            sample   = output sampling domain, given as a real number
                       defining the sampling step in Angstroms or as a
                       filename of the reference image used to define start
                       and step for each order

                       Echelle keyword is SAMPLE.
\ap\no
Note:       
            The rebinned spectrum is a two-dimensional image in the
            space wavelength-order. Therefore, the descriptor START of
            this image cannot indicate the starting wavelength, 
            which is different for each order. To produce a final spectrum 
            including correct descriptors START and STEP, use the command 
            MERGE/ECHELLE.
\on\exs
Examples:
\ex
            REBIN/ECHELLE extord1 extsp1 0.2
\xe \sxe

