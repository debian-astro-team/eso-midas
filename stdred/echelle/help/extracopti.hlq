% @(#)extracopti.hlq	19.1 (ESO-IPG) 02/25/03 14:18:33 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      extracopti.hlq
%.AUTHOR     MP, IPG/ESO; SW, DMD/ESO
%.KEYWORDS   MIDAS, help files, EXTRACT/OPTIMAL
%.PURPOSE    On-line help file for the command: EXTRACT/OPTIMAL
%.VERSION    1.0  28-NOV-1991 : Creation, MP
%            2.0  09-SEP-1999 : cosmic ray rejection, variance calculation
%                               and calc parameter added.
%----------------------------------------------------------------
\se
SECTION./OPTI
\es\co
EXTRACT/OPTIMAL						09-SEP-1999 MP/SW
\oc\su
EXTR/OPT input output  slit,ord1,ord2  [ron,g,sigma] [table] [coeffs] [calc]
	 weigthed extraction of echelle orders
\us\pu
Purpose:    
            Extract echelle orders. Position of the orders are defined
            in the auxiliary table order.tbl, generated with the command
            DEFINE/ECHELLE or DEFINE/HOUGH. Orders are extracted by 
            calculating a weighted sum of the pixels values across the
            profile of the object. The algorithm is based on a paper from
            Koji Mukai(1990, Optimal Extraction of Cross-Dispersed Spectra,
            Pub. of Astr. Soc of the pacific,102:183-189).
\up\sy
Syntax:     EXTR/OPT in out slit,ord1,ord2 [ron,g,sigma] [table] [coeff] [calc]
\ys\pa
            in          = input image sampled in the usual pixel-pixel space
\ap\pa
            out         = extracted image sampled in the space pixel-order
\ap\pa
            slit        = numerical slit length in pixels
\ap\pa
            ord1,ord2   = number of the first and last order to extract.
                          Relative order numbers are displayed by the
                          command LOAD/ECHELLE.
\ap\pa
            ron,g,sigma = read-out-noise(e-), inverse gain factor (e-/ADU),
                          threshold for the removal of cosmic ray hits 
                          (in units of the standard deviation calculated
                          for each pixel from the number of electrons and
                          the ron.
                          If not provided on the command line, these values
                          are read from echelle keywords RON, GAIN, EXTSIGMA.
\ap\pa
            table   =     name of the input table providing the order 
                          definition. Default: order.tbl
\ap\pa
            coeff   =     name of the descriptor providing the coefficients
                          of the order definition. Default: COEFF.
\ap\pa
            calc    =     determine/apply weights for optimal extraction.
                          Y,name: do an optimal extraction and store the
                               weights in name.
                          N,name:  do an optimal extraction using the weights
                               name of a previously performed extraction.
                          Default: Y,weight.bdf
\ap\no
Note:       
            The parameter slit defines the integer length of the numerical
            extraction slit centered on the  current order center at every
            position along the dispersion direction. (We suppose that the
            command DEFINE/ECHELLE or DEFINE/HOUGH has been used previously 
            to find the position of the orders )
            The weigths are proportional  to the mean  order profile 
            perpendicular to the dispersion direction.

            This version of this command is now able to remove cosmic rays.
            Furthermore, a variance frame of the extracted orders will be
            produced. The variance frame has the same name as the extracted
            data with the prefix 'var_'.

            EXTRACT/OPTIMAL produces averaged spectra in the pixel-order
            space. In order to get the integral extracted orders you only have
            to multiply your result frame by the slit parameter and the
            variance frame by the squared slit parameter.

\on\exs
Examples:
\ex
            EXTRACT/OPT raw extract  6,1,15  40,10.,3. 
\xe\ex
            Do an optimal extraction of the object

            EXTRACT/OPT obj extrobj  15,1,20 ? ? ? Y,objwght.bdf

            Do an optimal extraction of the flatfield using the same weights
            as for the optimal extraction of the object.

            EXTRACT/OPT flat extrflat  15,1,20 ? ? ? N,objwght.bdf
\xe\sxe






