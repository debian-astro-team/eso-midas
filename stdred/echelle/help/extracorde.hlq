% @(#)extracorde.hlq	19.1 (ESO-IPG) 02/25/03 14:18:34 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1992 European Southern Observatory
%.IDENT      extracorde.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, EXTRACT/ORDER
%.PURPOSE    On-line help file for the command: EXTRACT/ORDER
%.VERSION    1.0  17-AUG-1992 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./ORDE
\es\co
EXTRACT/ORDER						17-AUG-1992 PB
\oc\su
EXTRACT/ORDER  inp out sl,ang,off meth table coeff [ord1,ord2]
	Extract echelle orders and produces a frame in space pixel-order
\us\pu
Purpose:    
            Extract echelle orders from spectra. Order positions are
            defined by coefficients of a regression, slit width, angle
            and offset. The orders are extracted by passing a numerical
            slit of given and angle in positions defined by the regression
            coefficients. The slit will sample at increments of one step
            in X.
\up\sy
Syntax:     EXTRACT/ORDER  inp out sl,ang,off meth table coeff [ord1,ord2]
\ys\pa
            inp        =  input image in space pixel-pixel.
\ap\pa
            out        =  output image in space pixel-order. The orders
                          extracted are controlled by [ord1,ord2].
\ap\pa
            sl,ang,off =  slit, angle, offset. These parameters control
                          the width of the slit in pixels, the angle
                          of the slit with respect to the columns of the
                          spectrum, the offset of the slit relatively to
                          the positions defined by the regression
                          coefficients.
\ap\pa
            meth       =  extraction method. Three methods are supported:
                          LINEAR, AVERAGE, WEIGHTED (See notes).
\ap\pa
            table      =  table in which the regression coefficients are
                          stored as descriptors \\
                          (command SAVE/REGRESSION).
\ap\pa
            coeff      =  Name of the regression coefficients as used
                          in the command SAVE/REGRESSION.
\ap\pa
            ord1,ord2  =  Relative order number of the orders to be
                          extracted. The default value 0,0 means that
                          all orders are extracted.
\ap\sa
See also:   
            EXTRACT/OPTIMAL, EXTRACT/ECHELLE, SAVE/REGRESSION.
\as\no
Note:       The three supported methods are the following:\\
            LINEAR    : Pixel values are added. A linear interpolation
                        is performed.\\
            AVERAGE   : pixel values are averaged. A linear interpolation
                        is performed.\\
            MEDIAN    : Pixel values are averaged using the median instead
                        of mean.\\
            WEIGHTED  : Pixel values are weighted proportionally to the
                        profile perpendicular to the dispersion direction.
\on\exs
Examples:
\ex
            EXTRACT/ECHELLE  ccd0001  ext1 8,0.,0. AVERAGE order COEFF
\xe \sxe



