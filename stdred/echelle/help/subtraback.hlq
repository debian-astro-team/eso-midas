% @(#)subtraback.hlq	19.1 (ESO-IPG) 02/25/03 14:18:41 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      subtraback.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, SUBTRACT/BACKGROUND
%.PURPOSE    On-line help file for the command: SUBTRACT/BACKGROUND
%.VERSION    1.0  03-MAR-1991 : Creation, PB
%                 22-JUN-1999 : bkgmea option added, SW
%----------------------------------------------------------------
\se
SECTION./BACK
\es\co
SUBTRACT/BACKGROUND					    22-JUN-1999  PB
\oc\su
SUBTRACT/BACKGROUND  input  bkg  output  [bkgmtd]  [bkgvisu] [bkgmea]
	compute and subtract background from input frame.
\us\pu
Purpose: 
            Compute and subtract background from input frame and update 
            descriptor BACKGROUND. The background estimate step will be skipped
            when running high level commands (FLAT/ECH, FILT/ECH, CALIB/ECH,
            RESP/ECH, REDU/ECH).
\up\sy
Syntax:     
            SUBTRACT/BACKGROUND  input  bkg  output  [bkgmtd]  [bkgvisu]
\ys\pa
            input   = name of raw input frame
\ap\pa
            bkg     = name of background frame
\ap\pa
            output  = name of output frame (input - bkg)
\ap\pa
            bkgmtd  = Background computation method (POLY,SPLINE,SMOOTH)
                      Echelle keyword is BKGMTD and default is POLY.
\ap\pa
            bkgvisu = Optional visualization of the background (YES/NO)
                      (plots central column of input and bkg)
                      Echelle keyword is BKGVISU and default is YES.
                      The required graphic window is created if not
                      already existent. The limits of the plot must be
                      previously set using SET/PLOT command.
\ap\pa
            bkgmea  = Specify the measuring method at background position
                      in case of bkgmtd=SPLINE. Usually the median within
                      the box defined by BKGRAD centered on the background
                      position read from table BAKTAB is used to determine
                      the background. In some cases it is useful to take
                      the MINIMUM instead.
                      Default: bkgmea=MEDIAN.
\ap\no
Note:       
            The options and parameters of this high level command
            are read from section 2. (Background) of the SHOW/ECHELLE.
            The current method is defined by keyword BKGMTD (possible
            values POLY, SPLINE, SMOOTH) and parameters to be
            set for each method are displayed by the SHOW/ECHELLE. 

            See also BACKGROUND/ECHELLE, BACKGROUND/SMOOTH, SELECT/BACKGR
\on\exs
Examples:
\ex
            none
\xe \sxe

