% @(#)tutorieche.hlq	19.1 (ESO-IPG) 02/25/03 14:18:41 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      tutorieche.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, TUTORIAL/ECHELLE
%.PURPOSE    On-line help file for the command: TUTORIAL/ECHELLE
%.VERSION    1.0  25-APR-1991 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./ECHELLE
\es\co
TUTORIAL/ECHELLE						25-APR-1991 PB
\oc\su
TUTORIAL/ECHELLE
	demonstrates main commands of echelle package
\us\pu
Purpose:
        Demonstrates main commands of echelle package    
\up\sy

Syntax:     TUTORIAL/ECHELLE

\ys\no
Note:       
            a) This procedure requires a display and a graphic window.
               They are automatically created if necessary.

            b) Frames casff.bdf, casth.bdf, casobj.bdf, casstd.bdf are
               copied from MID_TEST if not already present.
               Tables lincat06.tbl (renamed thar.tbl) and ltt1020.tbl are
               copied from MID_CASPEC if not already present.

            c) The only interactive part of the tutorial consists of
               identifying lines for the wavelength calibration.
               The user will be  prompted to identify the occurences of two
               lines in adjacent orders:\\
                         Orders 88/89  Wavelength: 6416.315\
                         Orders 98/99  Wavelength: 5760.550\\
               Order numbers are displayed on the left side and wavelengths
               on the right side of the display window.

               The execution of the tutorial is stopped, in order to
               enable a command like:\\
                         COPY/DISPLAY   ps_printer

               The tutorial is reactivated by the command:\\
                         CONTINUE

            d) The four main steps of echelle package are performed:\\
                         CALIBRATE/ECHELLE
                         FLAT/ECHELLE
                         RESPONSE/ECHELLE
                         REDUCE/ECHELLE

               During the wavelength calibration, if the sequence of
               identification is correct, an angle of rotation is
               computed and must be around -0.09 degrees. 
               Then the user must provide as input an order number (88)
               and two wavelengths (6416.315 and 5760.550).

               It is recommended to consult Vol. B, Chapter Echelle Package,
               before running the tutorial.
\on\exs
Examples:
\ex           
               TUTORIAL/ECHELLE
	       run the tutorial
\xe \sxe
