% @(#)preparwind.hlq	19.1 (ESO-IPG) 02/25/03 14:18:38 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      preparwind.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, PREPARE/WINDOW
%.PURPOSE    On-line help file for the command: PREPARE/WINDOW
%.VERSION    1.0  17-DEC-1986 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./WIND
\es\co
PREPARE/WINDOW						30-JUL-1991 PB
\oc\su
PREPARE/WINDOW catalogue flat-bkg lhcuts
	prepare echelle images for the command AVERAGE/WINDOW
\us\pu
Purpose:    
            Write the descriptors FLAT_BKG and LHCUTS onto the
            images in the catalogue. These descriptors are used
            by the command AVERAGE/WINDOW in order to remove
            bad pixels from the raw data. Specially recommended
            for dark exposures to produce the BIAS and pre-flashed
            BIAS frames.
            Additionally, the command check the presence of the
            descriptor with the exposure time and will prompt for
            it if not present.
            The command works on catalogues containing the input images.
            The catalogue is created with
            CREATE/CATALOG catalogue dirspec  (See HELP)
\up\sy
Syntax:    
            PREPARE/WINDOW catalogue flatbkg lhcuts
\ys\pa
            catalogue  = name of the catalogue with the input images
\ap\pa
            flatbkg  = value to be written into descriptor FLAT_BKG
\ap\pa
            lhcuts   = values to be written into descriptor LHCUTS

            See AVERAGE/WINDOW for more information  about these two
            parameters.
\ap\no
Note:       
            none
\on\exs
Examples:
\ex
            AVERAGE/WINDOW    mycat  3.   20,50.
\xe \sxe



