# @(#)makefile	19.1 (ESO-IPG) 02/25/03 14:20:21
# .COPYRIGHT:	Copyright (c) 1988 European Southern Observatory,
#						all rights reserved
# .TYPE		make file
# .NAME		$MIDASHOME/$MIDVERS/stdred/echelle/src/makefile 
# .LANGUAGE	makefile syntax
# .ENVIRONMENT	Unix Systems. 
# .COMMENT	Compiles source files and generates "echelle" commands
#
# .REMARKS	
# .AUTHOR	
# .VERSION 1.1	880131:		Implementation
# .VERSION 1.2	890104:		Generating makefile.com for VMS systems
# .VERSION 2.1  901212:		New directory structure . CG
# .VERSION 2.2  920521:         Removing MLIB CG
# .VERSION 3.0  930308:		Using default.mk file

include ../../../local/default.mk

M = ../../exec

#TABLEDIR = ../../../prim/table/libsrc
TABLEDIR = ../../../libsrc/math

CFLAGS += $(C_OPT) $(DEBUG) $(SYS) -I$(LINC) -I$(INC)

LLIB = -L$(LIBDIR) -lmidas
LLIB1 = -L$(LLIBDIR) -lspec -lmidmath -L$(LIBDIR) -lftab -lgen -lmidas
LLIBG = -L$(LLIBDIR) -lspec -lmidmath -L$(LIBDIR) -lftab -lgen -lgmidas -lmidas
LLIBN = -L$(LLIBDIR) -lech_nr -lmidmath 

LIBS =	$(LIBDIR)/libmidas.a

LIBS1 =	$(LLIBDIR)/libspec.a \
	$(LIBDIR)/libgen.a \
	$(LIBDIR)/libftab.a \
	$(LIBDIR)/libmidas.a 

LIBSG =	$(LLIBDIR)/libspec.a \
	$(LIBDIR)/libgen.a \
	$(LIBDIR)/libftab.a \
	$(LIBDIR)/libgmidas.a \
	$(LIBDIR)/libmidas.a 

LIBSN =	$(LLIBDIR)/libech_nr.a

#LIBU =  $(MLIBDIR)/tabuser.a

OUT =	$(M)/necwlcopt.exe $(M)/necompix.exe \
	$(M)/necregr.exe   $(M)/necconv.exe $(M)/necexor.exe  \
	$(M)/necmerge.exe  $(M)/necfilt.exe \
        $(M)/necavmed.exe  $(M)/necripcor.exe                   \
	$(M)/necrebi.exe   $(M)/necripp.exe \
	$(M)/necnord.exe   $(M)/necford.exe \
	$(M)/necripp1.exe  $(M)/necback.exe \
	$(M)/neciden.exe   $(M)/necopt.exe     $(M)/necoffset.exe \
        $(M)/nechough.exe  $(M)/necfindmax.exe $(M)/necdef.exe


# DEPENDENCIES:
all: $(MAKEFILE_VMS) $(OUT)

$(MAKEFILE_VMS): makefile
	$(MAKE_VMS)

$(M)/necompix.exe: necompix.o $(LIBSG)
	$(LD77) necompix.o   $(LLIBG) $(MLIB) $(SLIB) -o $@
$(M)/necoffset.exe: necoffset.o $(LIBS1)
	$(LDCC) necoffset.o  $(LLIB1) $(MLIB) $(SLIB) -o $@
$(M)/necwlcopt.exe: necwlcopt.o $(LIBS)
	$(LD77) necwlcopt.o $(LLIB) $(SLIB) -o $@
$(M)/necregr.exe: necregr.o $(LIBS)
	$(LDCC) necregr.o  $(LLIB) $(MLIB) $(SLIB) -o $@
$(M)/necconv.exe: necconv.o $(LIBS) 
	$(LD77) necconv.o $(LLIB1) $(MATHLIB) $(SLIB) -o $@
$(M)/necexor.exe: necexor.o $(LIBS1)
	$(LD77) necexor.o $(LLIB1) $(SLIB) -o $@
$(M)/necopt.exe: necopt.o $(LIBS1) $(LIBSN)
	$(LD77) necopt.o $(LLIB1) $(SLIB) $(LLIBN) -o $@
$(M)/necfilt.exe: necfilt.o $(LIBS1)
	$(LD77) necfilt.o $(LLIB1) $(SLIB) -o $@
$(M)/necmerge.exe: necmerge.o $(LIBS1)
	$(LD77) necmerge.o $(LLIB1) $(SLIB) -o $@
$(M)/necrebi.exe: necrebi.o $(LIBS1)
	$(LD77) necrebi.o $(LLIB1) $(SLIB) -o $@
$(M)/necripp.exe: necripp.o $(LIBS1)
	$(LD77) necripp.o $(LLIB1) $(SLIB) -o $@
$(M)/necnord.exe: necnord.o $(LIBS1)
	$(LD77) necnord.o $(LLIB1) $(SLIB) -o $@
$(M)/necford.exe: necford.o $(LIBS)
	$(LD77) necford.o $(LLIB) $(SLIB) -o $@
$(M)/necripp1.exe: necripp1.o $(LIBS1)
	$(LD77) necripp1.o $(LLIB1) $(NAGLIB) $(SLIB) -o $@
$(M)/necback.exe: necback.o $(LIBS1)
	$(LD77) necback.o $(LLIB1) $(SLIB) -o $@
$(M)/neciden.exe: neciden.o $(LIBS1)
	$(LD77) neciden.o $(LLIB1) $(SLIB) -o $@
$(M)/nechough.exe: nechough.o $(LIBS1)
	$(LDCC) nechough.o $(LLIB1) $(MLIB) $(SLIB) -o $@
$(M)/necfindmax.exe: necfindmax.o $(LIBS)
	$(LDCC) necfindmax.o $(LLIB) $(MLIB) $(SLIB) -o $@
$(M)/necdef.exe: necdef.o $(LIBS)
	$(LDCC) necdef.o $(LLIB) $(MLIB) $(SLIB) -o $@
$(M)/necavmed.exe: necavmed.o $(LIBS)
	$(LD77) necavmed.o $(LLIB) $(SLIB) -o $@
$(M)/necripcor.exe: necripcor.o $(LIBS)
	$(LD77) necripcor.o $(LLIB) $(SLIB) -o $@
$(M)/neczebpar.exe: neczebpar.o $(LIBS)
	$(LD77) neczebpar.o $(LLIB) $(SLIB) -o $@
$(M)/neczebord.exe: neczebord.o $(LIBS1)
	$(LD77) neczebord.o $(LLIB1) $(SLIB) -o $@

clean_exec:
	rm -f $(OUT)

clean:
	rm -f *.o
	rm -f *.f

rebu01.for: $(TABLEDIR)/rebu01.for
	cp $(TABLEDIR)/rebu01.for .
