% @(#)mask_irac.hlq	19.1 (ESO-VLT) 02/25/03 14:22:38
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      mask_irac2.hlq
%.AUTHOR     GFI, VLTI/ESO
%.KEYWORDS   MIDAS, help files, MASK/IRAC2
%.PURPOSE    On-line help file for the command MASK/IRAC2
%.VERSION    1.0  09-JUL-1992 : Creation, GFI
%----------------------------------------------------------------
\se
SECTION./CONT
\es\co
MASK/IRAC2                                                    09-JUL-1992 GFI
\oc\su
MASK/IRAC2 inframe outframe
	replaces bad pixels by closest good pixel
\us\pu
Purpose:     
	     Replace each bad pixel by closest good pixel.
\up\sy
Syntax:      
             MASK/IRAC2 inframe outframe
\ys\pa
             inframe     = name of image file which is to be cosmetically
                           treated by replacing bad pixels by neighbouring
                           good pixels.
\ap\pa
             outframe    = name of resulting file which contains the 
                           clean image having all bad pixels replaced.
\ap\no
Note:        
             The pixelmask is stored in file GOODPX.bdf which has to be
             created by the MIDAS command CMASK/IRAC2 in the current
             directory. Good pixels are marked by 1 and bad pixels are
             marked by 0 in file GOODPX.bdf.
\on\exs
Examples:
\ex
             MASK/IRAC2 spiral spiralclean 
             The bad pixels of the frame spiral.bdf are replaced by
             neighbouring good pixels using the pixel mask GOODPX.bdf
             which has been created by the midas command CMASK/IRAC2.
             The clean image is stored in the frame spiralclean.bdf and
             displayed using the cut levels of frame spiral.bdf.
\xe \sxe
