% @(#)obslisirac.hlq	19.1 (ESO-USG) 02/25/03 14:22:38   
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c) 1995 European Southern Observatory
%.IDENT      obslisirac.hlq
%.AUTHOR     Chris Lidman, ESO
%.KEYWORDS   MIDAS, help files, IRAC2 context
%.PURPOSE    On-line help file for the command: OBSLIST/IRAC2
%.VERSION    951029 CEL Created
%----------------------------------------------------------------
\se
SECTION./IRAC2
\es\co
OBSLIST/IRAC2                                                    1-Nov-1995 CEL
\oc\su
OBSLIST/IRAC2 [start] [end]
        Lists a subsection of the IRAC2B OST (Observation Summary Table)
\us\pu
Purpose:    
        Lists a subsection of the IRAC2B OST (Observation Summary Table)
\up\sy
Syntax:     
        OBSLIST/IRAC2 [start] [end]
\ys\pa
        start = starting OST column number
\ap\pa
        end   = ending OST column number
\ap\no
Note:   
        This command uses the OST column number and not the number in the
        file name. These numbers usually agree, but not always. If both
        parameters, start and end, are left blank, the last ten entries are 
        displayed. If the parameter end is left blank the ten entries from 
        start onwards are displayed. 
\\
        The format of the output is dictated by the file obslst.fmt. The 
        name of OST is irac2b_ost.tbl. 
\on\see
See also:   
        OBSREP/IRAC2, CREATE/OST
\ees\exs
Examples:
\ex
      OBSLIST/IRAC2 4 7
      List column 4 to 7 of the observation summary table irac2b_ost.tbl
\xe\sxe



