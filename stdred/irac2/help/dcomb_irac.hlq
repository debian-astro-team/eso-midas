% @(#)dcomb_irac.hlq	19.1 (ESO-USG) 02/25/03 14:22:37   
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c) 1995 European Southern Observatory
%.IDENT      dcomb_irac.hlq
%.AUTHOR     Chris Lidman, ESO
%.KEYWORDS   MIDAS, help files, IRAC2 context
%.PURPOSE    On-line help file for the command: DCOMB/IRAC2
%.VERSION    951029 CEL Created
%----------------------------------------------------------------
\se
SECTION./IRAC2
\es\co
DCOMB/IRAC2                                                      1-Nov-1995 CEL
\oc\su
DCOMB/IRAC2 [select] [seqname] [accsky] [align] output [trim] [tag]
        Sky subtract and combine dithered images.
\us\pu
Purpose:    
        Sky subtract and combine dithered images.
\up\sy
Syntax: 
        DCOMB/IRAC2 [select] [seqname] [accsky] [align] output [trim] [tag]
\ys\pa
        select =  selection string which selects files in the OST table. For 
                  example @21..24,29 will select files in row numbers 21 to 
                  24 inclusive  and file in row number 29. Default = null.
\ap\pa
        seqname = files with this sequence number will be selected.
                  Default is null.
\ap\pa
        accsky =  Flag for accurate sky subtraction. Default is N.
\ap\pa
        align =   Flag for aligning object frames. Options are:\\
                   Y for interactive alignment;\\ 
                   A for automatic alignment;\\
                   B for blind alignment and \\
                   N for no alignment.\\
                  Default is Y.
\ap\pa
        output =  name of output image.
\ap\pa
        trim =    flag for trimming image to largest common area. Default is Y.
\ap\pa
        tag =     flag for tagging all frames marked as skies or tagging only
                  those with the word ``sky'' in the identifier. Default is Y.
\ap\no
Note: 
        Out of the input parameters select and seqname only one should be set.
\\
        If the flag for accurate sky subtraction is set to Y, the user is 
        prompted to enter a number which determines which pixels are replaced
        before image smoothing. Use a low value if the sky is stable and use 
        a high value if the sky is varying rapidly. However, the higher the 
        value, the more inaccurate photometry becomes.
\\
        The first few characters of the output name must be different than the
        prefix of the raw data files. 
\\
        If the flag for tagging frames as skies is set to N, the identifier
        of the frames to be used as skies must be marked as sky.
\\
        The name of the output image is recorded in the MIDAS table reductions.
\\
	For IRAC2 data, option B is not recommended.
	
\on\see
See also:   
        RCOMB/IRAC2
\ees\exs
Examples:
\ex
        DCOMB/IRAC2 ? 342 N Y hd1234 N Y
        Using the images from sequence 342, combine all the images to make a 
        sky, subtract this sky image from all images, and combine all these 
        images into the final image with the user selecting objects for 
        image registration. The final image is called hd1234 and is trimmed 
        to the largest common area of the input images. 
\xe\sxe
