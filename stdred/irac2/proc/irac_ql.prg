! @(#)irac_ql.prg	19.1 (ES0-DMD) 02/25/03 14:22:56
! @(#)irac_ql.prg	19.1 (ESO-Chile) 02/25/03 14:22:56
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       irac_ql.prg
!.AUTHOR      C. Lidman,  ESO-Chile
!.KEYWORDS    IRAC package
!.PURPOSE     Programme that takes the difference between two images
!             and divides by the dit.
!.NOTE        It requires the operations staff to update the local variable
!             "head". It requires the programme acuts.prg.
!.VERSION     ?????? anonymous prehistoric
!             951010 CEL 
!-------------------------------------------------------
crossref image1 image2 outimage

define/para p1  ?   C   "Enter first file number"
define/para p2  ?   C   "Enter second file number"
define/para p3  out ima "Enter image name"
!
define/loca head/c/1/4 "cd"   
define/loca dit/d/1/2 1.,1.
define/loca ditdif/d/1/1 0.0
define/loca nnn/I/1/2 0,0
define/loca in_1/C/1/60 " "
define/loca in_2/C/1/60 " "
set/format  I4 f8.4
!
IF M$TSTNO("{p1}") .EQ. 1 .AND. M$TSTNO("{p2}") .EQ. 1 THEN
   nnn(1) = {p1}
   nnn(2) = {p2}
   in_1 = "{head}{nnn(1)}"
   in_2 = "{head}{nnn(2)}"
   info/imag {in_1}
   info/imag {in_2}

ELSE
   in_1 = "{p1}"
   in_2 = "{p2}"
ENDIF
!
copy/dk {in_1} _ed_dit dit/d/1/1
copy/dk {in_2} _ed_dit dit/d/2/1
ditdif = M$ABS({dit(1)} - {dit(2)})
if {ditdif} .GT. 0.01 then
   write/out " INFO: Image {in_1} and {in_2} have unequal DITs "
endif
!
write/out "*** Computing {p3} = {in_1}/{dit(1)} - {in_2}/{dit(2)}"
comp/ima {p3} = {in_1}/{dit(1)} - {in_2}/{dit(2)}
!
@s irac_acuts {p3} 
!load {p3} scale=2



