! @(#)irac_cmask.prg	19.1 (ES0-DMD) 02/25/03 14:22:54
! @(#)irac_cmask.prg	19.1 (ESO-VLT) 02/25/03 14:22:54
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: mask.prg 
!.PURPOSE:        Implements the command cmask/irac2 
!.AUTHOR:         Gert Finger (ESO-VLT)
!.VERSION:        920701 GF  creation
!.VERSION:        920713 RHW Header included
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAM P1 ? IMA "ENTER INPUT FRAME :"
DEFINE/PARAM P2 ? IMA "ENTER OUTPUT FRAME :"
DEFINE/PARAM P3 ? N   "ENTER LOWMASK,HIGHMASK: "
DEFINE/PARAM P4 R C   "ENTER FLAG , R(epalcae) OR H(ighmask): "
WRITE/KEYW OUT_A 'P2'
WRITE/KEYW IN_A 'P1'
WRITE/KEYW INPUTR/R/1/2 'P3'
WRITE/KEYW DEFAULT 'P4'
RUN STD_EXE:CRPIXMASK
WRITE/KEYW CUTS/D/1/4 0.,0.,0.,0.
COPY/DK 'P1' LHCUTS CUTS 
WRITE/OUT   {CUTS(1)}{CUTS(2)}
LOAD 'P2' CUTS='CUTS(1)','CUTS(2)'




