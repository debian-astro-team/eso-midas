! @(#)irac_rcomb.prg	19.1 (ES0-DMD) 02/25/03 14:22:56
! @(#)irac_rcomb.prg	19.1 (ESO-Chile) 02/25/03 14:22:56
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       irac_rcomb.prg
!.AUTHOR      C. Lidman,  ESO-Chile
!.KEYWORDS    IRAC package
!.PURPOSE     Procedure to combine IR images
!.VERSION     950803 C.E. Lidman Creation
!             951010 CEL 
!-------------------------------------------------------

crossref select align output

define/par p1 ? C "Enter selection string:"
define/par p2 Y C "Align object frames (N/Y/A):"
define/par p3 ? C "Name of output image:"

if p2 .ne. "Y" then
  write/out For aligning frames, option {p4} has not been implemented
  return
endif  

write/out {p1},{p2},{p3}

def/local  last/I/1/1 100
def/local  xc/r/1/1 1.0
def/local  yc/r/1/1 1.0
def/local  n/I/1/1 1
def/local  xshift/R/1/1 0.0
def/local  yshift/R/1/1 0.0
def/local  xlow/R/1/1 1.0
def/local  xhigh/R/1/1 256.0
def/local  ylow/R/1/1 1.0
def/local  yhigh/R/1/1 256.0
def/local  yshmin/R/1/1 0.0
def/local  yshmax/R/1/1 1.0
def/local  xshmin/R/1/1 0.0
def/local  xshmax/R/1/1 1.0
def/local  medran/I/1/1 1
def/local  totwei/I/1/1 0
def/local  xsize/i/1/1 1
def/local  ysize/i/1/1 1
def/local  xmax/i/1/1 1
def/local  ymax/i/1/1 1
def/local  lens/c/1/2 LC


copy/table reductions work
create/col work :ADD I*1
write/tab work :ADD {p1} 1
select/table work :ADD .eq. 1

!If alignment is not required

if p2 .eq. "N" then 
! Weighting will need to be introduced
  create/icat obj.cat work,:FILENAME
  average/images obj = obj.cat
else

copy/tab work work2
create/col work2 :SHIFILENAME ? ? C*15

set/midas output=no
show/tab work2
last = {outputi(2)}
set/midas output=yes

write/out {last}

copy/dk {work2,FILENAME,@1} _EIO3_NAME lens

!Calculate total weights and find the size of the largest image

do n = 1 {last} 1
   totwei = totwei + {work2,COMBINED,@{n}}
   copy/dk {work2,FILENAME,@{n}} npix/i/1/1 xsize
   copy/dk {work2,FILENAME,@{n}} npix/i/2/1 ysize

   if {xsize} .gt. {xmax} then
      xmax = {xsize}
   endif
   if {ysize} .gt. {ymax} then
      ymax = {ysize}
   endif
enddo

!write/out {xmax} {ymax}


do n = 1 {last} 1

create/image &a 2,{xmax},{ymax} 1,1,1,1 ? -10000
insert/image {work2,FILENAME,@{n}} &a @1,@1

load/ima &a cuts=-100,100

write/out "Select the same object in each frame"
center/gauss CURSOR ? ? ? 20,20  !box size is 20 x 20

!Shift images - except the first 

if {n} .gt. 1 then
 xshift = {xc}-{OUTPUTR(5)}
 yshift = {yc}-{OUTPUTR(6)}
 write/out "{n} xshift = {xshift}  yshift = {yshift}"

 if {xshift} .lt. {xshmin} then
  xshmin = {xshift}
 endif
 if {xshift} .gt. {xshmax} then
  xshmax = {xshift}
 endif
 if {yshift} .lt. {yshmin} then
  yshmin = {yshift}
 endif
 if {yshift} .gt. {yshmax} then
  yshmax = {yshift}
 endif

!There is strange feature in the MIDAS shift command. Zero shifts do not
!produce a resultant image.

 if M$ABS({xshift}) .lt. 0.5 .and. M$ABS({yshift}) .lt. 0.5 then
   copy/ii &a imsh{n}
 else
   shift/ima  &a imsh{n} {xshift},{yshift}
 endif

 del/image &a NO

 write/tab work2 :SHIFILENAME @{n} imsh{n}.bdf
 del/ima &a NO

else

copy/ii &a imsh{n}

xc = {OUTPUTR(5)}
yc = {OUTPUTR(6)}
write/tab work2 :SHIFILENAME @1 imsh{n}.bdf

endif

enddo

!Add images with appropriate weights

comp/ima {p3} = {reductions,COMBINED,@1}/{totwei}*imsh0001

write/out {reductions,COMBINED,@1} {totwei}

do n 2 {last} 1

write/out {reductions,COMBINED,@{n}} {totwei}

comp/ima {p3} = {p3} + {reductions,COMBINED,@{n}}/{totwei}*imsh{n}

enddo

endif

!The high number could be more accurately determined
!It will depend on the size of the largest image.
!We should trim with the minimum
 write/out {xshmax} {yshmax} {xshmin} {yshmin} {xmax} {ymax}
 xshmin = {xmax} + {xshmin}
 yshmin = {ymax} + {yshmin} 
 xshmax = {xshmax}
 yshmax = {yshmax}
 write/out {xshmax} {yshmax} {xshmin} {yshmin}
 extract/image {p3} = {p3}[{xshmax},{yshmax}:{xshmin},{yshmin}]

@s irac_acuts {p3}

!Write output file to a results table that should not be deleted

set/midas output=no
show/tab reductions
last = {outputi(2)}
set/midas output=yes

last = {last} + 1

write/table reductions :FILENAME @{last} {p3}
write/table reductions :COMBINED @{last} {totwei}
write/descr {p3} _EIO3_NAME/C/1/2 {lens}

!CLEAN UP ALL YOUR LOOSE FILES

!del/icat obj NOCONF
!del/icat sky NOCONF
!del/tab work NO
!del/tab work2 NO
