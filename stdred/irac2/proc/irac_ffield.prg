! @(#)irac_ffield.prg	19.1 (ES0-DMD) 02/25/03 14:22:55
! @(#)irac_ffield.prg	19.1 (ESO-DMD) 02/25/03 14:22:55
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       irac_ffield.prg
!.AUTHOR      Dave Clements,  ESO-Garching
!.KEYWORDS    IR imaging, IRAC2b package
!.PURPOSE     Subtract sky frame from object frame 
!             ?do we match medians beforehand?
!.VERSION     ???
!.NOTE        
!-------------------------------------------------------
DEFINE/PARAM P1 ?          IMA "Enter object frame: "
DEFINE/PARAM P2 ?          IMA "Enter flat field frame: "
DEFINE/PARAM P3 ?          IMA "Enter flat fielded output frame : "
!
!
   COMPUTE/IMAGE {P3} = {P1} / {P2}

RETURN





