! @(#)irac_obslist.prg	19.1 (ES0-DMD) 02/25/03 14:22:55
! @(#)irac_obslist.prg	19.1 (ESO-Chile) 02/25/03 14:22:55
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       irac_obslist.prg
!.AUTHOR      C. Lidman,  ESO-Chile
!.KEYWORDS    IRAC package
!.PURPOSE     procedure to print out the OST in a nice format
!.NOTE        Requires the files irac2b_ost.tbl, irac2b_descr.tbl and
!             obslist.fmt
!.VERSION     950915 C. Lidman Creation
!             951010 CEL Added cross referencing
!             951106 CEL Added Option for displaying the OST with one number 
!-------------------------------------------------------
crossref start end

define/par p1 0   N "Enter the first file number" 0,10000
define/par p2 0 N "Enter the last file number" 0,10000

define/loc upper/I/1/1/ 1
define/loc lower/I/1/1 1
define/loc fmtname/C/1/20 "obslist"
IF M$EXIST("{fmtname}.fmt") .EQ. 0 THEN  
   IF AUX_MODE(1) .LE. 1 THEN                                    ! VMS
      define/local fmtdir/c/1/60 -
         "MID_DISK:[&MIDASHOME.&MIDVERS.STDRED.IRAC2.LIB]"
   ELSE                                                          ! UNIX
      define/local fmtdir/c/1/60 -
         "$MIDASHOME/$MIDVERS/stdred/irac2/lib/"
   ENDIF
   -COPY {fmtdir}{fmtname}.fmt {fmtname}.fmt                       ! copy
ENDIF

set/midas output=no

show/tab irac2b_ost

lower = {p1}

if {outputi(2)} .lt. {p2} then
    upper = {outputi(2)}
else
    upper = {p2}
endif

!If only start is set then the 10 entries (If there are 10) from start
!onwards are displayed.

if {p2} .eq. 0 then
   upper = {lower} + 9
   if {outputi(2)} .lt. {upper} then
      upper = {outputi(2)}
   endif
endif

!If both parameters are not set the last 10 exposures are displayed.

if {p2} .eq. 0 .and. {p1} .eq. 0 then
   upper = {outputi(2)}
   lower = {outputi(2)}-9
   if {lower} .le. 0 then
      lower = 1
   endif
endif


set/midas output=yes

read/table irac2b_ost :FILENAME,IDENT,SEQID,RA,DEC,FILT,EXP,NDIT,LENS,FP,WAV,METH @{lower} @{upper} {fmtname}.fmt

!end program
