! @(#)irac_ssub.prg	19.1 (ES0-DMD) 02/25/03 14:22:56
! @(#)irac_ssub.prg	19.1 (ESO-DMD) 02/25/03 14:22:56
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       irac_ssub.prg
!.AUTHOR      Dave Clements,  ESO-Garching
!.KEYWORDS    IR imaging, IRAC2b package
!.PURPOSE     Subtract sky frame from object frame 
!             ?do we match medians beforehand?
!.VERSION     ???
!.NOTE        
!-------------------------------------------------------
DEFINE/PARAM P1 ?          IMA "Enter object frame: "
DEFINE/PARAM P2 ?          IMA "Enter sky frame: "
DEFINE/PARAM P3 ?          IMA "Enter sky subtracted output frame : "
!
!
   COMPUTE/IMAGE {P3} = {P1} - {P2}

RETURN





