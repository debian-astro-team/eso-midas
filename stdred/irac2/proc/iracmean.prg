! @(#)iracmean.prg	19.1 (ES0-DMD) 02/25/03 14:22:56
! @(#)iracmean.prg	19.1 (ESO-DMD) 02/25/03 14:22:56
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdmean.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    IRAC package
!.PURPOSE     Compute the mean of an image and write in the descriptor
!.USE         @s iracmean p1
!.               p1 = input image; 
!.METHOD      Use statistics/image
!.VERSION     930417  RHW  Creation
!-------------------------------------------------------
DEFINE/PARAM P1 ?  I       "Enter input image [no default]:"
!
! *** Check if trimming has already been applied
!IF M$EXISTD("{P1}","CCDMEAN") .EQ. 1 THEN
!   RETURN
!ENDIF
!
! *** return if only listing is wanted
!IF SC_PROC .EQ. "yes" THEN
!   WRITE/OUT "<CCDMEAN> {P1}: ***TO BE DONE*** Find mean value of the image"
!   RETURN
!ENDIF
!
STATISTIC/IMAGE {P1} + ? ? SN                      ! get the mean
COPY/KD OUTPUTR/R/3/1 {P1} IRACMEAN/R/1/1                  ! store in descriptor
RETURN


