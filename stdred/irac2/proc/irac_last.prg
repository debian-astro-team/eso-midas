! @(#)irac_last.prg	19.1 (ES0-DMD) 02/25/03 14:22:55
! @(#)irac_last.prg	19.1 (ESO-Chile) 02/25/03 14:22:55
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       irac_last.prg
!.AUTHOR      C. Lidman,  ESO-Chile
!.KEYWORDS    IRAC package
!.PURPOSE     Lists the last n entries of the catalogue
!.NOTE        Requires the catalog data.cat
!.VERSION     950729 C. Lidman Creation
!             951010 CEL Added cross referencing
!-------------------------------------------------------
crossref num

define/par p1 10 N "Enter the number of files to be listed" 0,10000

define/loc sss/i/1/2 1,1
!
log(4) = 2
show/icat data
log(4) = 0
!
sss(2) = outputi(2)
sss(1) = {sss(2)}-{p1}+1
if 'sss(1)' .le. 0 then 
   sss(1) = 1
endif
read/icat data 'sss(1)',{sss(2)}
