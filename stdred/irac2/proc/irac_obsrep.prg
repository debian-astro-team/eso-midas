! @(#)irac_obsrep.prg	19.1 (ES0-DMD) 02/25/03 14:22:56
! @(#)irac_obsrep.prg	19.1 (ESO-Chile) 02/25/03 14:22:56
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       irac_obsrep.prg
!.AUTHOR      C. Lidman,  ESO-Chile
!.KEYWORDS    IRAC package
!.PURPOSE     Procedure to print out the OST in a nice format.  
!.NOTE        The programme requires the files irac2b_ost.tbl, 
!             irac2b_descr.tbl and obsrep.fmt
!.VERSION     ?????? anonymous prehistoric
!             951010 CEL 
!-------------------------------------------------------
crossref start end

define/par p1 ?   N "Enter the first file number" 1,10000
define/par p2 ? N "Enter the last file number" 1,10000

!There are no defaults

define/loc upper/I/1/1/ 1
define/loc lower/I/1/1 1
define/loc fmtname/C/1/20 "obsrep"
IF M$EXIST("{fmtname}.fmt") .EQ. 0 THEN  
   IF AUX_MODE(1) .LE. 1 THEN                                    ! VMS
      define/local fmtdir/c/1/60 -
         "MID_DISK:[&MIDASHOME.&MIDVERS.STDRED.IRAC2.LIB]"
   ELSE                                                          ! UNIX
      define/local fmtdir/c/1/60 -
         "$MIDASHOME/$MIDVERS/stdred/irac2/lib/"
   ENDIF
   -COPY {fmtdir}{fmtname}.fmt {fmtname}.fmt                       ! copy
ENDIF

set/midas output=no

show/tab irac2b_ost

lower = {p1}
if {outputi(2)} .lt. {p2} then
    upper = {outputi(2)}
else
    upper = {p2}
endif

set/midas output=yes

assign/print laser

print/table irac2b_ost :FILENAME,IDENT,RA,DEC,FILT,EXP,NDIT,LENS,FP,WAV,METH @{lower} @{upper} {fmtname}.fmt

assign/print terminal

!end program


