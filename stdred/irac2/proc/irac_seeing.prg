! @(#)irac_seeing.prg	19.1 (ES0-DMD) 02/25/03 14:22:56
! @(#)irac_seeing.prg	19.1 (ESO-Chile) 02/25/03 14:22:56
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       irac_seeing.prg
!.AUTHOR      C. Lidman,  ESO-Chile
!.KEYWORDS    IRAC package
!.PURPOSE     compute the seeing with IRAC2 on the 2.2m
!.NOTE        This routine will give an over-estimate with undersampled 
!             images or if the psf is non-gaussian
!.VERSION     950729 C.E. Lidman Creation
!             951010
!             951105 CEL Pixel scales adjusted
!-------------------------------------------------------
def/loc fwx/r/1/1 0.0
def/loc fwy/r/1/1 0.0
def/loc scale/r/1/1 0.0
def/loc lens/c/1/2 nu

copy/dk {IDIMEMC} _EIO3_NAME lens

!This will need to be cahnged with the new header keywords

write/out The lens is {lens}.

If lens .eq. "LA" then
  scale = 0.151
elseif lens .eq. "LB" then
  scale = 0.278
elseif lens .eq. "LC" then
  scale = 0.507
elseif lens .eq. "LD" then
  scale = 0.708
elseif lens .eq. "LE" then
  scale = 1.061
elseif lens .eq. "nu" then
  write/out I'm sorry Dave, but I can't determine the lens
  return
endif

crea/tab seeing 15 0 null
clear/chan over

center/gauss cursor seeing
log(4) = 2
select/tab seeing :icen.le.60000

stat/tab seeing :xsig 
fwx = 'outputr(3)' * 2.35482 * 'scale'

stat/tab seeing :ysig
fwy = 'outputr(3)' * 2.35482 * 'scale'

log(4) = 0
write/out
write/out
set/format F6.2 F6.2
write/out "FWHMx = " 'fwx' "  FWHMy = " 'fwy'


