!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!                                        
!  MIDAS procedure to print out the OST  
!  in a nice format.                     
!
!  The programme requires the files irac2b_ost.tbl, irac2b_descr.tbl and
!  obslist.fmt
!
!  C. Lidman 15/9/95                     
!            10/10/95  Added Cross Ref.  
!            06/11/95  Added Option for displaying the OST with one number.
!            
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

crossref start end

define/par p1 0   N "Enter the first file number" 0,10000
define/par p2 0 N "Enter the last file number" 0,10000

define/loc upper/I/1/1/ 1
define/loc lower/I/1/1 1

set/midas output=no

show/tab irac2b_ost

lower = {p1}

if {outputi(2)} .lt. {p2} then
    upper = {outputi(2)}
else
    upper = {p2}
endif

!If only start is set then the 10 entries (If there are 10) from start
!onwards are displayed.

if {p2} .eq. 0 then
   upper = {lower} + 9
   if {outputi(2)} .lt. {upper} then
      upper = {outputi(2)}
   endif
endif

!If both parameters are not set the last 10 exposures are displayed.

if {p2} .eq. 0 .and. {p1} .eq. 0 then
   upper = {outputi(2)}
   lower = {outputi(2)}-9
   if {lower} .le. 0 then
      lower = 1
   endif
endif


set/midas output=yes

read/table irac2b_ost :FILENAME,IDENT,SEQID,RA,DEC,FILT,EXP,NDIT,LENS,FP,WAV,METH @{lower} @{upper} obslist.fmt

!end program
