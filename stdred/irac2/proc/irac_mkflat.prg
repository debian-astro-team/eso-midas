! @(#)irac_mkflat.prg	19.1 (ES0-DMD) 02/25/03 14:22:55
! @(#)irac_mkflat.prg	19.1 (ESO-DMD) 02/25/03 14:22:55
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       make_iracflat.prg
!.AUTHOR      Dave Clements,  ESO-Garching
!.KEYWORDS    IR imaging, IRAC2b package
!.PURPOSE     Make a flat field by subtracting lamp off from lamp on image 
!             and then normalising the result
!.VERSION     ???
!.NOTE        
!-------------------------------------------------------
DEFINE/PARAM P1 ?          IMA "Enter lamp on frame: "
DEFINE/PARAM P2 ?          IMA "Enter lamp off frame: "
DEFINE/PARAM P3 ?          IMA "Enter flat field output frame : "
!
DEFINE/LOCAL SCALE/R/1/1 1.0
!
   COMPUTE/IMAGE {P3} = {P1}-{P2}

   @s iracmean {P3}

   SCALE = {{P3},IRACMEAN}

   COMPUTE/IMAGE {P3} = {P3} / {SCALE}


RETURN





