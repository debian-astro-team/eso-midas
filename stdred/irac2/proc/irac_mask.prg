! @(#)irac_mask.prg	19.1 (ES0-DMD) 02/25/03 14:22:55
! @(#)irac_mask.prg	19.1 (ESO-VLT) 02/25/03 14:22:55
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: mask.prg 
!.PURPOSE:        Implements the command mask/irac2 
!.AUTHOR:         Gert Finger (ESO-VLT)
!.VERSION:        920701 GF  creation
!.VERSION:        920713 RHW Header included
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAM P1 ? IMA "ENTER INPUT FRAME :"
DEFINE/PARAM P2 ? IMA "ENTER OUTPUT FRAME :"
WRITE/KEYW OUT_A 'P2'
WRITE/KEYW IN_A 'P1'
RUN STD_EXE:MASK 
WRITE/KEYW CUTS/D/1/4 0.,0.,0.,0. 
COPY/DK 'P1' LHCUTS CUTS
LOAD 'P2' CUTS='CUTS(1)','CUTS(2)' SCALE=2,2





