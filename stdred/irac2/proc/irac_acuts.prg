! @(#)irac_acuts.prg	19.1 (ES0-DMD) 02/25/03 14:22:54
! @(#)irac_acuts.prg	19.1 (ESO-Chile) 02/25/03 14:22:54
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       irac_acuts.prg
!.AUTHOR      C. Lidman,  ESO-Chile
!.KEYWORDS    IRAC package
!.PURPOSE     procedure to set cuts to mean-3sig,mean+nsig
!.USE         @s acuts [image] [load] [plot] [upper]
!             with
!               image = name of image to work on
!               load_flag = Yes or No to load the image also
!                           defaulted to Yes
!               plot_flag = P to plot histogram, def. to p
!.VERSION     920521 KB
!             920606 A. Monetti
!             950729 C. Lidman  Added the option to display the image in the
!                                variable {IDIMEMC} as the default image.
!-------------------------------------------------------
crossref image load plot upper

define/para p1 {IDIMEMC} ima "Enter image name: "
define/para p2 Y C "Enter Y(es) or N(o) if the image should be loaded: "
define/para p3 p c "Enter P(lot) or N(oplot) for histogram plotting: "
define/para p4 8 n "Enter upper sigma cutoff:"
!
define/local mycuts/r/1/2 0.,0.
set/format I4 F8.4
! 
!p1 = {IDIMEMC}
set/midas output=no
error(3) = -1
statist/image {p1} ? ? ? fn 
error(3) = 0
set/midas output=yes
! 
mycuts(1) = outputr(3)-4*outputr(4)
mycuts(2) = outputr(3)+4*outputr(4)
if mycuts(2) .ge. 22000   mycuts(2) = 22000
statist/image {p1} ? #128 {mycuts(1)},{mycuts(2)} fn ? {p3}
! 
mycuts(1) = outputr(3)-3*outputr(4)
mycuts(2) = outputr(3)+{p4}*outputr(4)
!write/out "Cuts for {p1} are: {mycuts(1)}, {mycuts(2)} "
write/out " >>>Image {p1}:   median = {outputr(3)}   RMS = {outputr(4)} "
if p2(1:1) .eq. "Y" then
   load/image {p1}  cuts={mycuts(1)},{mycuts(2)}
else
   cuts {p1} {mycuts(1)},{mycuts(2)}
endif
