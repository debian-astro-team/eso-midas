C @(#)colmean.for	19.1 (ESO-IPG) 02/25/03 14:22:15
      PROGRAM COLMEA
C+
C
C.IDENTIFICATION
C
C  program colmea                       version 1.0             900703
C
C  Otmar Stahl                           Landessternwarte Heidelberg
C
C  rowmean          inframe, list of columns
C
C  OUT_A                normalized output frame
C  INPUTI               list of columns
C
      IMPLICIT NONE
      INTEGER         NPIX(2),IAV,ISTAT,NAXIS,NUM(10)
      INTEGER         IMNO1,I
      INTEGER*8       IPNTR1
      INTEGER         MADRID,KUN,KNUL
C 
      CHARACTER*40    INFRAME
      CHARACTER       CUNIT*64,IDENT*72
C 
      DOUBLE PRECISION  START(2),STEP(2)
C                                       
      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      COMMON /VMR/MADRID(1)
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'
C
      CALL STSPRO('COLMEA')
C
C get input and map images
C
      CALL STKRDC('IN_A',1,1,40,IAV,INFRAME,KUN,KNUL,ISTAT)
      CALL STKRDI('INPUTI',1,10,IAV,NUM,KUN,KNUL,ISTAT)
      CALL STIGET(INFRAME,D_R4_FORMAT,F_IO_MODE,F_IMA_TYPE,
     &2,NAXIS,NPIX,START,STEP,IDENT,CUNIT,
     &IPNTR1,IMNO1,ISTAT)
C
      DO I = 1,10
         IF (NUM(I).GT.0) IAV = I
      ENDDO
C
C   compute mean
C
      CALL COLMEAN(MADRID(IPNTR1),NPIX,IAV,NUM,NPIX(1),NPIX(2))
C
      CALL STSEPI
      STOP
      END
C
C*************************************************************************
      SUBROUTINE COLMEAN(IN,NPIX,IAV,NUM,NPIX1,NPIX2)
C
C compute mean of adjacent columns
C
      IMPLICIT  NONE
      INTEGER         NPIX(2),IAV,NUM(10),I,J
      INTEGER         NUM1(10),NUM2(10),NPIX1,NPIX2
      REAL            IN(NPIX1,NPIX2)
C
C      TYPE *,NPIX(1),NPIX(2)
      DO I = 1,IAV
C        type *,num(i)
         NUM1(I) = NUM(I)-1
         NUM2(I) = NUM(I)+1
      ENDDO
C
      DO J = 1,NPIX(2)
         DO I = 1,IAV
C                 TYPE *,J,I,num(i),num1(i),num2(i)
            IN(NUM(I),J) = (IN(NUM1(I),J)+IN(NUM2(I),J))/2.
         ENDDO
      ENDDO
C
      RETURN
      END
