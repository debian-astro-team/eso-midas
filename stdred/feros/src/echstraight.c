/* @(#)echstraight.c	19.1 (ESO-IPG) 02/25/03 14:22:16 */
/* 

   Otmar Stahl

   echstraight.c

   extract echelle orders

   First step:

   First step of extraction of echelle spectra.
   Align spectrum along rows for easier access.
   Rebinning is not done.  

   Information about position is kept in two auxiliary files.
   This is needed for 2-D rebinning or optimum extraction.

*/

/* system includes */

#include <ctype.h>
#include <string.h>
#include <stdio.h>

/* general Midas includes */

#include <tbldef.h>
#include <midas_def.h>
#include <ok.h>

/* FEROS specific includes */

#include <nrutil.h>
#include <mutil.h>
#include <echelle.h>


int main ()

{
  char in_image[60], out_image[60], in_table[60];

  char line[80];

  int npix[2], width, off1, off2;
  int  nfirst, nlast, nact, fit_deg;

  int actvals, kun, knul, dunit, dnul, tid, iparam[4];
  int imno, mode;
  int i, j, naxis;

  float  **centers;

  double start[2], step[2], *ad, xposs, yposs, start0, step0;

  /* get into Midas and get parameters */

  SCSPRO ("echstraight");                        /* connect to Midas */

  SCKGETC ("IN_A",   1, 60, &actvals, in_image); /* read Midas keywords */
  SCKGETC ("IN_B",   1, 60, &actvals, in_table); /* into variables */
  SCKGETC ("OUT_A",  1, 60, &actvals, out_image);

  SCKRDI  ("INPUTI", 1,  4, &actvals, iparam, &kun, &knul);

  SCFOPN (in_image, D_R4_FORMAT, F_I_MODE, F_IMA_TYPE, &imno); /* open frame */
  SCDRDI (imno, "NAXIS", 1, 1, &actvals, &naxis, &kun, &knul); /* read integer
                                                 descriptor into variable */
  if (naxis != 2)                       /* check number of axes */
    {
      SCTPUT("Frame not 2-D, exiting");
      SCSEPI();
    }

  SCDRDI (imno, "NPIX", 1, 2, &actvals, npix, &kun, &knul);          /* read */
  SCDRDD (imno, "START", 1, 2, &actvals, start, &kun, &knul); /* descriptors */
  SCDRDD (imno, "STEP", 1, 2, &actvals, step, &kun, &knul); /* into variables*/
  SCTPUT ("Straighten orders");                                /* write info */
  SCTPUT ("-----------------\n");
  sprintf (line, "Input image:         %s", in_image);
  SCTPUT (line);
  sprintf (line, "Output image:        %s", out_image);
  SCTPUT (line);
  sprintf (line, "Input table:         %s\n", in_table);
  SCTPUT (line);

  /* get polynomial coefficients and compute centers */

  TCTOPN (in_table, F_D_MODE, &tid);              /* read table */
  SCDRDI(tid, "FIRSTORD", 1, 1, &actvals, &nfirst, &dunit, &dnul);  
  SCDRDI(tid, "ECHORD", 1, 1, &actvals, &nlast, &dunit, &dnul);   /* read into */
  SCDRDI(tid, "FITORD", 1, 1, &actvals, &fit_deg, &dunit, &dnul); /* variables */
  nact = nlast - nfirst + 1;

  ad = dvector (1, fit_deg);                      /* allocate mem for var ad */
  centers = matrix(1, nact, 1, npix[1]);     /* allocate mem for var centers */

  for (i = 1; i <= nact; i++)                       /* columns */   
    {
      sprintf(line, "FIT%04i", nfirst - 1 + i);
      SCDRDD(tid, line, 1, fit_deg, &actvals, &ad[1], &dunit, &dnul); /* read
					      descriptor into variable ad[1] */
      for (j = 0; j < npix[1]; j++)                   /* rows */  
	{
	  xposs = start[1] + step[1] * j;
	  yposs =  eval_dpoly (xposs, ad, fit_deg);
	  centers[i][j + 1] = (float) ((yposs - start[0]) / step[0]); 
          /* coordinates of order centers (in pixel numbers) */               
	}
    }

  TCTCLO(tid);

  width = 2 * (iparam[0] / 2) + 1;
  off1 = iparam[1];
  off2 = iparam[2];
  mode = iparam[3];
  start0 = start[1];
  step0 = step[1];

  rectify
    (centers, imno, npix, nact, start0, step0, width, off1, off2, 
     mode, out_image);

  SCFCLO(imno);

  SCSEPI(); 
  return(0);
}
