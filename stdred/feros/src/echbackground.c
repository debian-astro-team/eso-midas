/* @(#)echbackground.c	19.1 (ESO-IPG) 02/25/03 14:22:15 */
/* 

   Otmar Stahl, Anton Malina

   echbackground.c

   compute and subtract background

*/

/* system includes */

#include <stdio.h>
#include <ctype.h>

/* general Midas includes */

#include <tbldef.h>
#include <midas_def.h>
#include <ok.h>

/* FEROS specific includes */

#include <nrutil.h>
#include <mutil.h>
#include <echelle.h>

int main ()
{
  char in_image[60], out_image[60], in_table[60];
  char line[80], qualif[2];
  int npix[2], npixo[2], xysize[2], fibmode;
  int fit_ord, nfirst, nlast, nact;
  int actvals, kun, knul, dunit, dnul, tid, iparam[5], inull;
  int i, j, imno, imnor, naxis, size;
  float lhcuts[4], rnull, **centers;
  double start[2], step[2], stepo[2], starto[2], dnull, *ad, xposs, yposs;

  /*         get into Midas and get parameters             */

  SCSPRO ("echbackground");
  TCMNUL(&inull,&rnull,&dnull);

  SCKGETC ("IN_A",   1, 60, &actvals, in_image);
  SCKGETC ("IN_B",   1, 60, &actvals, in_table);
  SCKGETC ("OUT_A",  1, 60, &actvals, out_image);
  SCKGETC ("INPUTC", 1, 1, &actvals, qualif);
  SCKRDI  ("INPUTI", 1, 5,  &actvals, iparam, &kun, &knul);
  SCKRDI  ("XYSIZE", 1, 2,  &actvals, xysize, &kun, &knul);
  SCKRDI  ("FIBMODE", 1, 1,  &actvals, &fibmode, &kun, &knul);

  SCFOPN (in_image, D_R4_FORMAT, F_I_MODE, F_IMA_TYPE, &imno);
  
  SCDRDI (imno, "NAXIS", 1, 1, &actvals, &naxis, &kun, &knul);
  if (naxis != 2) 
    {
      SCTPUT("Frame not 2-D, exiting");
      SCSEPI();
    }
  SCDRDI (imno, "NPIX", 1, 2, &actvals, npix, &kun, &knul);
  SCDRDD (imno, "START", 1, 2, &actvals, start, &kun, &knul);
  SCDRDD (imno, "STEP", 1, 2, &actvals, step, &kun, &knul);

  switch (toupper(qualif[0]))
    case ('B'): 
      {
	SCTPUT ("Compute and subtract background\n");
	break;
      case ('N'):   
	SCTPUT ("Compute background\n");
	break;
      default:
	sprintf(line, "Error: Unknown qualifier %s\n", qualif);
	SCTPUT(line);
	SCETER(9, "Exiting...");
      }

  SCTPUT ("--------------\n");
  sprintf (line, "Input image:         %s", in_image);
  SCTPUT (line);
  sprintf (line, "Output image:        %s", out_image);
  SCTPUT (line);
  sprintf (line, "Input table:         %s\n", in_table);
  SCTPUT (line);

  /* now get polynomial coefficients  */

  TCTOPN(in_table, F_D_MODE, &tid);
  SCDRDI(tid, "FIRSTORD", 1, 1, &actvals, &nfirst, &dunit, &dnul);
  SCDRDI(tid, "ECHORD", 1, 1, &actvals, &nlast, &dunit, &dnul);   
  SCDRDI(tid, "FITORD", 1, 1, &actvals, &fit_ord, &dunit, &dnul);
  nact = nlast - (nfirst - 1);
  centers = matrix(1, nact, 1, npix[1]); 

  ad = dvector (1, fit_ord);
  for(i = 1; i <= nact; i++) 
    {
      sprintf(line, "FIT%04i", nfirst - 1 + i);
      SCDRDD(tid, line, 1, fit_ord, &actvals, &ad[1], &dunit, &dnul);
      for(j = 0; j < npix[1]; j++)
	{
	  xposs = start[1] + step[1] * j;
	  yposs =  eval_dpoly (xposs, ad, fit_ord);
	  centers[i][j + 1] = (float) ((yposs - start[0]) / step[0]);	  
	}
    }

  free_dvector(ad, 1, fit_ord);
 
  starto[0] = start[0]; starto[1] = start[1];
  stepo[0] = step[0]; stepo[1] = step[1];
  npixo[0] = npix[0]; npixo[1] = npix[1];
  size = npixo[0] * npixo[1];
  lhcuts[0] = lhcuts[1] = lhcuts[2] = lhcuts[3] = 0.0;
  
  SCFCRE (out_image, D_R4_FORMAT, F_O_MODE, F_IMA_TYPE, size, &imnor);
  SCDWRI (imnor, "NAXIS", &naxis, 1, 1, &kun);
  SCDWRI (imnor, "NPIX",  npix, 1, 2, &kun);
  SCDWRD (imnor, "START", start, 1, 2, &kun);
  SCDWRD (imnor, "STEP", step, 1, 2, &kun);
  SCDWRR (imnor, "LHCUTS", lhcuts, 1, 4, &kun);
  SCDWRC (imnor, "IDENT", 1, "norm frame", 1, 72, &kun);
  SCDWRC (imnor, "CUNIT", 1, "ADU", 1, 72, &kun);

  switch(toupper(qualif[0]))
    {

/* subtract background */

    case ('B'): 
      comp_back(npix, imno, imnor, iparam, nact, centers, 0, xysize, fibmode);
      break;
      
/* compute background */

    case ('N'):   
      comp_back(npix, imno, imnor, iparam, nact, centers, 1, xysize, fibmode);
      break;
      
    default:
      sprintf(line, "Error: Unknown qualifier %s\n", qualif);
      SCTPUT(line);
      SCETER(9, "Exiting...");
    }

/* disconnect from Midas */  

  SCSEPI();   
  return(0);
}
