/* @(#)misc.h	19.1 (ESO-IPG) 02/25/03 14:21:11 */
/* miscellaneous routines */
/* written by Anton Malina */

#define LIGHTSPEED 299792.5
#define SQUARE(x) ( (x) * (x) ) 

#define FALSE 0
#define TRUE 1

#define data_per_pixel 3

#define WM_DONTWRITE 0 /* for center_all_orders */
#define WM_DOWRITE 1   /* " */

typedef int BOOLEAN;

void fit_wavelength
(
#ifdef __STDC__
 double inimage[], double ximage[], double yimage[], 
 int npixi, double outimage[], double ximageo[], 
 double yimageo[], int npixo, int fit_deg, double a[], 
 int printout
#endif
 );

void deriv_OS
(
#ifdef __STDC__
 double x1, double x2, double a[], double *y, double dyda[],  int n
#endif
 );

double eval_OS
(
#ifdef __STDC__
 double x1, double x2, double a[], int n
#endif
 );

double deg_to_rad 
( 
#ifdef __STDC__
 double argument
#endif
 );

int global_to_poly
(
#ifdef __STDC__
 double dcoef[], double polcoeff[], int order1, int order2, int poldeg
#endif
 );
