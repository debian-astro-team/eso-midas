/* @(#)echelle.h	19.1 (ESO-IPG) 02/25/03 14:21:10 */
/* Include file for echelle routines */

#define MAXORDERS 200
#define MAXLINES 1000
#define GRAVITY 0 
#define GAUSSIAN 1

#define myabs(A) ((A)  >  0 ? (A) : (-A))

int fit_line
(
#ifdef __STDC__
 float [], int, float [], double [], double [], int,  
 int, int, float
#endif
 );

int fit_lines
(
#ifdef __STDC__
 float[], int[], float[], int
#endif
 );

int center_lines
(
#ifdef __STDC__
 float[], int [], float[], float [], float, float, 
 int, int, int *
#endif
 );

int search_lines
(
#ifdef __STDC__
 float[], int [], int[], int, int, float
#endif
 );

int extract_cuts
(
#ifdef __STDC__
 int, int , int , int , float *, int []
#endif
 );

int fold_image
(
#ifdef __STDC__
 float [], float [], float [], int, int[], int
#endif
 );

int fit_orders
(
#ifdef __STDC__
 int, int, int, float [], int [], float
#endif
 );

int follow_orders
(
#ifdef __STDC__
 int, float [], float [], float [], int [], int [], 
 double [], double [], int [], float [], int [], int [], 
 int, int, int, int, float
#endif
 );

int center_order
(
#ifdef __STDC__
 float [], float [], float [], int, int [], 
 double [], double [], int, int, float *, float *, 
 float *, int *
#endif
 );

int center_one_order
(
#ifdef __STDC__
 float imageo[], float imaget[], float corr_arr[], 
 int center, int npixt[], 
 int width2, int order, int lrspan,
 int lnfit_method, float *shift,  float *maxi, int *max_index
#endif
 );

int center_orders
(
#ifdef __STDC__
 float [], float [], float [], int [], int [], int [], 
 float [], int , int [], float [], double [], double [], 
 int [], int , int, int, int *, int [], int, int
#endif
 );

int center_all_orders
(
#ifdef __STDC__
 float imageo[], float imaget[], float corr_arr[], 
 int xcenter[], int xcenter_0[], int offset[], 
 float center_0[], int line_no, int npixt [], float tab[], 
 double start[], double step[], int npix[],
 int win, int width2, int nact, int tid, int first_entry,
 int icol[], int left_edge, int right_edge, int lnfit_method, int writemode
#endif
 );

int fit_back
(
#ifdef __STDC__
 float[], float [], float **, double **,
 int [], int, int, int, int, float **, int[], int, int 
#endif
 );

int extract_profiles
(
#ifdef __STDC__
 int inimagefile, int xposfile, int dyfile,
 int nact, int nlines, int npts, int *npoints,
 float v0, float gain, float thres,
 int tid, int icol[], int fit_deg
#endif
 );

int spatial_profile 
(
#ifdef __STDC__
 float frame[], float sky[], float outframe[], 
 float varframe[], 
 float mask[], float profile[], float variance[], 
 float x[], float y[],
 float z[], int slitlen, int ntotal, int npix, 
 int niter, int fit_deg, 
 float v0, float gain, float thres, double ad[]
#endif
 );

int interpolate
(
#ifdef __STDC__
 float in[], int howmany_in, float by_howmuch, float out[], int howmany_out
#endif
 );

float opt_ext
(
#ifdef __STDC__
 float profile[], float image[], float variance[], int number,  int niter, 
 float v0,  float gain, float thres,  float ea,
 int DoOptext, float *mask_img, int masked_counter[], int total_masked
#endif
 );

int comp_back
(
#ifdef __STDC__
 int npix[], int imno, int imnor, int iparam[], 
 int nact, float **centers, int mode, int xysize[], int fibmode
#endif
 );


int extract_spec
(
#ifdef __STDC__
 float imageo1[],
 int fit_deg, char mode,
 float v0, float gain, float thres, int niter, int nact, int nlines, int npts,
 int *npoints, int straight, int xposfile, int dxfile, char coeffile[],
 int get_profile, int write_imgs, char fit_image[], char mask_image[]
#endif
 );

int rectify
(
#ifdef __STDC__
 float **centers, int imno, int npix[], int nact, 
 double start0, double step0,
 int width, int off1, int off2, int mode, char* out_image
#endif
 );

void fit_select 
(
#ifdef __STDC__
 double p1[], double p2[], double p3[], int *p4, double p5[],
 double p6[], double p7[], double p8[], int p9, int p10, double p11[],
 double p12, int printout
#endif
 );

double closest_to
(
#ifdef __STDC__
 double *reference_wavelengths, double what, double howclose, double dnull
#endif
 );
