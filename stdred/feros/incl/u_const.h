/* @(#)u_const.h	19.1 (ESO-IPG) 02/25/03 14:21:12 */
/*.pn 407 */
/*.hlAnhang: C - Programme*/
/*.hrKonstanten- und Macro-Definitionen*/
/*.fe Die Include-Datei u_const.h ist in das Verzeichnis zu stellen,  */
/*.fe wo der Compiler nach Include-Dateien sucht.                     */


/*-----------------------  FILE u_const.h  ---------------------------*/

#define IEEE

/* IEEE - Norm fuer die Darstellung von Gleitkommazahlen:

      8 Byte lange Gleitkommazahlen, mit

     53 Bit Mantisse  ==> Mantissenbereich:    2 hoch 52 versch. Zahlen
                                               mit 0.1 <= Zahl < 1.0,
                                               1 Vorzeichen-Bit
     11 Bit Exponent  ==> Exponentenbereich:  -1024...+1023

   Die 1. Zeile ( #define IEEE ) ist zu loeschen, falls die Maschine
   bzw. der Compiler keine Gleitpunktzahlen gemaess der IEEE-Norm
   benutzt. Zusaetzlich muessen die Zahlen  MAXEXPON, MINEXPON
   (s.u.) angepasst werden.
   */

#ifdef IEEE         /*----------- Falls IEEE Norm --------------------*/

#define MACH_EPS  2.220446049250313e-016   /* Maschinengenauigkeit    */
                                           /* IBM-AT:  = 2 hoch -52   */
/* MACH_EPS ist die kleinste positive, auf der Maschine darstellbare
   Zahl x, die der Bedingung genuegt: 1.0 + x > 1.0                   */

#define EPSQUAD   4.930380657631324e-032
#define EPSROOT   1.490116119384766e-008

#define POSMAX    8.98846567431158e+307    /* groesste positive Zahl  */
#define POSMIN    5.56268464626800e-309    /* kleinste positive Zahl  */
#define MAXROOT   9.48075190810918e+153

#define BASIS     2                        /* Basis der Zahlendarst.  */
#define PI        3.141592653589793e+000
#define EXP_1     2.718281828459045e+000

#else               /*------------------ sonst -----------------------*/

double exp  ();
double atan ();
double pow  ();
double sqrt ();

double masch()            /* MACH_EPS maschinenunabhaengig bestimmen  */
{
  double eps = 1.0, x = 2.0, y = 1.0;
  while ( y < x )
    { eps *= 0.5;
      x = 1.0 + eps;
    }
  eps *= 2.0; return (eps);
}

int basis()               /* BASIS maschinenunabhaengig bestimmen     */
{
  double x = 1.0, one = 1.0, b = 1.0;

  while ( (x + one) - x == one ) x *= 2.0;
  while ( (x + b) == x ) b *= 2.0;

  return ( (int) ((x + b) - x) );
}

#define BASIS     basis()                  /* Basis der Zahlendarst.  */

/* Falls die Maschine (der Compiler) keine IEEE-Darstellung fuer
   Gleitkommazahlen nutzt, muessen die folgenden 2 Konstanten an-
   gepasst werden.
   */

#define MAXEXPON  1023.0                   /* groesster Exponent      */
#define MINEXPON -1024.0                   /* kleinster Exponent      */

/* nach einmaliger Bestimmung sollten die folgenden #define's
   durch Konstanten ersetzt werden
   */

#define MACH_EPS  masch()
#define EPSQUAD   MACH_EPS * MACH_EPS
#define EPSROOT   sqrt(MACH_EPS)

#define POSMAX    pow ((double) BASIS, MAXEXPON)
#define POSMIN    pow ((double) BASIS, MINEXPON)
#define MAXROOT   sqrt(POSMAX)

#define PI        4.0 * atan (1.0)
#define EXP_1     exp(1.0)

#endif              /*-------------- ENDE ifdef ----------------------*/


#define NEGMAX   -POSMIN                   /* groesste negative Zahl  */
#define NEGMIN   -POSMAX                   /* kleinste negative Zahl  */

#define TRUE      1
#define FALSE     0


/* Definition von Funktionsmakros:
   */

#define min(X, Y) (((X) < (Y)) ? (X) : (Y))    /* Minimum von X,Y     */
#define max(X, Y) (((X) > (Y)) ? (X) : (Y))    /* Maximum von X,Y     */
#define sign(X, Y) (((Y) < 0) ? -abs(X) : abs(X))  /* Vorzeichen von  */
                                               /* Y mal abs(X)        */
#define sqr(X) ((X) * (X))                     /* Quadrat von X       */

