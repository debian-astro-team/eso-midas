/* @(#)gl_defs.h	19.1 (ESO-IPG) 02/25/03 14:21:11 */
/* @(#)gl_defs.h	1.3 (ESO-IPG) 2/11/94 09:29:44 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        gl_defs.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      global definitions for Midas related applications.
.KEYWORDS     global definitions
.COMMENTS     
.VERSION 1.0  1-Mar-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

#define MAXLINE		128
#define END_FILE	-1
#define OSAERR		-1	/* error status returned by osa routines */
#define SUCCESS		0	/* exit status returned by osa routines */

#define MAXDATA		16384	/* max number of elements in a column */
#define MAXLABEL	32	/* max length of a column label */
#define MAXCOLS		512 	/* max. no of cols in a table */
#define MAXNAME		128	/* max. length of a filename */
#define DEFAULT_CHAR	'?'	/* character used for default parameter */

#define MAX_IDENT	72  	/* as defined in Appendix C of env. Manual */

#define OUTSIDE_MIDAS	"-1"
#define MAXVALS		60      /* max. length of default parameters */
#define MAXUNIT		16	/* max. length of a unit string */

#define ALL_DESC	1       /* used in SCDCOP() */

#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

#define FALSE		0
#define TRUE		(! FALSE)

#define POSVALUE	25 	/* position of value in write/key command */

/* Definitions for text choice widgets */
#define NUMTEXTWIDGETS	40

/* definitions for help */
#define MAXSIZE_HELP	4096
#define COMMAND_HELP	0
#define PULLDOWN_HELP	1

/* functions of the OSMEMORY module of the Midas system library */
char *osmmget();
void osmmfree();

/* Macros */

#define SWAP(a, b)    {int swap_tmp; \
                         swap_tmp = a; a = b; b = swap_tmp;}

#define NINT(x)       ((x) + ( ((x) < 0.0) ? -0.5: 0.5))

