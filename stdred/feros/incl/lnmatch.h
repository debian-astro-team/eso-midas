/* @(#)lnmatch.h	19.1 (ESO-IPG) 02/25/03 14:21:11 */
/* Definition of rejection codes for lnmatch */

#define  NOT_PROCESSED     0
#define  SUCCESSFULL_MATCH 1
#define  MATCH_FAILED     -1
#define  REACHED_LIMIT    -3
#define  RESIDUAL_GT_TOL  -5

int match
(
#ifdef __STDC__
 int verif, double linid[], double linwav[], 
 double liny[], double lindif[], int nbrow, 
 double lcat[], int nbrowcat, double alpha, 
 double *stdres, double dnull, int reject[]
#endif  
);








