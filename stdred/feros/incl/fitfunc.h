/* @(#)fitfunc.h	19.1 (ESO-IPG) 02/25/03 14:21:11 */
/*

various fitting functions

*/

extern void fit_gh 
(
#ifdef __STDC__
 double *, double *, int, double *, int, double *
#endif
 );
