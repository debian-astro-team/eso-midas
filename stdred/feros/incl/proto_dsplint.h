/* @(#)proto_dsplint.h	19.1 (ESO-IPG) 02/25/03 14:21:11 */
/* @(#)proto_nrutil.h	1.1 (ESO-IPG) 2/11/94 10:14:52 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   proto_nrutil.h
.AUTHOR      Otmar Stahl 
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for all NR-Routines routines used in MOS

.ENVIRONment none

.VERSION     1.0    6-APR-1994 Otmar Stahl 
------------------------------------------------------------*/
extern double dsplint
(
#ifdef __STDC__
 double, double *, float *, int, int *
#endif
 );

