/* @(#)rebin.h	19.1 (ESO-IPG) 02/25/03 14:21:12 */
/*
  rebin.h              Thomas Szeifert  -   LSW Heidelberg 080797



------------------------------------------------------------*/

#define MAXINTENS   99999.0
#define MININTENS  -99999.0 

#define LINEAR    0
#define QUADRATIC 1
#define SPLINE    2

#define NINT(x)       ((x) + ( ((x) < 0.0) ? -0.5: 0.5))

extern void rebin
( 
#ifdef __STDC__
 double *, double *, float *, float *, 
 int , int , double, int, char
#endif
 );

extern void closest_index
( 
#ifdef __STDC__
 double, double *, double *, int, 
 int, double *, int * ,double, char
#endif
 );





