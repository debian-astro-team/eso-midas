/* @(#)glsp.h	19.1 (ESO-IPG) 02/25/03 14:21:11 */
/* smoothing splines */

int glspnp 
(
#ifdef __STDC__
 int n, double *xn, double *fn, double *w, 
 int marg_cond, double marg_0, double marg_n, 
 double *a, double *b, double *c, double *d
#endif
 );

int glsp2a 
(
#ifdef __STDC__
 int n, double *xn, double *fn, double *w, 
 double marg_0, double marg_n, int rep, 
 double *a, double *b, double *c, double *d,
 double *h, double *h1, double *h2, double *md, 
 double *ud1, double *ud2, double *rs
#endif
 );

int fdiasy 
(
#ifdef __STDC__
 int n, double *md, double *ud1, double *ud2, 
 double *rs, double *x
#endif
 );

int fdiasz 
(
#ifdef __STDC__
 int n, double *md, double *ud1, double *ud2
#endif
 );


void fdiasl 
(
#ifdef __STDC__
 int n, double *md,  double *ud1, double *ud2, 
 double *rs, double *x
#endif
	     );

double spval 
(
#ifdef __STDC__
 int n, double x0, double *a, double *b, double *c, 
 double *d, double *x, double ausg[]
#endif
 );
