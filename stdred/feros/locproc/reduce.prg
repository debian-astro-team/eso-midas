! @(#)reduce.prg	19.1 (ESO-IPG) 02/25/03 14:21:46

!
!      reduce.prg
!

define/local infile/c/1/60 {p1}
define/local num/i/1/1 {p2}

define/local ra/d/1/1 0.0

!define/local ah/r/1/1 0.0
!define/local am/r/1/1 0.0
!define/local as/r/1/1 0.0
!define/local dd/r/1/1 0.0
!define/local dm/r/1/1 0.0
!define/local ds/r/1/1 0.0

define/local rebstep/r/1/2 0.0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!	compute barycentric velocity
!

!
! 	RA in Hours Minutes Seconds
!
ra = {{infile},o_pos(1)}
ra = ra/15

!
! 	precess coordinates
!
!comp/prec {ra} {{infile},o_pos(2)} {{infile},cepoch}  {{infile},o_time(1)} 

!ah = {outputr(1)}
!am = {outputr(2)}
!as = {outputr(3)}
!dd = {outputr(4)}
!dm = {outputr(5)}
!ds = {outputr(6)}
!

comp/bary {infile}.bdf {ra} {{infile},o_pos(2)}
write/desc {infile} bary_corr/r/1/1 {outputr(1)}
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!return


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!	reduction starts here
!

back/feros {p1} back
straighten/feros back back_str
extract/feros back_str b{num}ext GO

FLAT/FERO b{num}ext  {FLAT_IMG} f{num}ext

rebstep(1) = {rebin_step}
rebstep(2) = {{infile},bary_corr}

REBIN/FERO f{num}ext {LINE_POS_TBL} rebinned I {rebstep(1)},{rebstep(2)}
!MERGE/FERO rebinned f{num} 1,39 NOAPP
MERGE/FERO rebinned f{num} 1 S

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!	copy descriptors
!
cop/dd {infile} *,3 b{num}ext1
cop/dd {infile} *,3 b{num}ext2

cop/dd {infile} *,3 f{num}ext1
cop/dd {infile} *,3 f{num}ext2

cop/dd {infile} *,3 f{num}1
cop/dd {infile} *,3 f{num}2

