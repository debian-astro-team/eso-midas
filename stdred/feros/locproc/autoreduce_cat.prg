! @@ autoreduce_cat.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  autoreduce_cat.prg	  
! O.Stahl, A.Kaufer, S.Tubbesing	990125
!
! PURPOSE
! reduce catalogue of object-frames
!
!				
! 	exposuretype=SCIENCE 	:
!                                          pre-reduction 
!					   background subtraction
!					   order straightening
!					   order extraction		
!					   flatfielding/blaze correction
!					   rebinning: wavelength calibration
!						      barycentric correction
!					   order merging
!					
! USAGE
!
! @@ autoreduce_cat catalogue-name.cat
!
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local infile/c/1/60 " " all
define/local num/c/1/4 " " all
define/local i/i/1/1 0
define/local order/i/1/1 0
define/local ra/d/1/1 0.0
define/local lstart/r/1/1 0.0
define/local lend/r/1/1 0.0
define/local rebstep/r/1/2 0.0
define/local etime/D/1/1 0
DEFINE/LOCAL CATAL/I/1/1 0


write/key DARK/C/1/1 D

!
! for SCIENCE frames do standard reduction:
!

next:
store/frame infile {p1} 1 done

@@ check {infile}

write/key num/c/1/4 {infile(5:8)}

! pre-reduction
!
 @@ prered {num} raw_image

! convert RA in Hours Minutes Seconds
!
 ra = {raw_image,o_pos(1)}
 ra = ra/15.

! compute barycentric velocity
!
comp/bary raw_image.bdf {ra} {raw_image,o_pos(2)}
write/desc raw_image bary_corr/r/1/1 {outputr(1)}

! reduction starts here
!

if "{dark}" .EQ. "D" then    ! dark-subtraction

  etime = {{infile},o_time(7)}
  comp sub_image = raw_image-DARK*{etime}/3600
  BACKGR/FEROS sub_image back

else

  BACKGR/FEROS raw_image back

endif

RECTIFY/FEROS back back_str

if EXT_MODE(1:1) .EQ. "O" then
   @@ standard {num}
   @@ optimum {num}

else  	
  @@ standard {num}

endif
	

goto next

done:

exit:







