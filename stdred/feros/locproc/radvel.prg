! @(#)radvel.prg	19.1 (ESO-IPG) 02/25/03 14:21:46
! @@ radvel.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure radvel
! A. Kaufer	981130
! 
! frontend for xcorall.prg to use input from catalogues
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ? C "Enter catalog : "
define/par p2 ? N "Enter number of ref. frame : "
define/par p3 ? C "Enter ThAr catalog : "
define/par p4 ? N "Enter number of ref. ThAr frame : "
define/par p5 0.15 N "Enter histogram binsize : "
!
define/local tmpname/c/1/40 " " all
define/local infile/c/1/40 " " all
define/local tharname/c/1/40 " " all
define/local tharfile/c/1/40 " " all
define/local catal/i/1/1 0 
define/local ind/i/1/2 0,0 
!
@@ xcorall {p1} 0 0 0 0 {p5} create
!
next:
store/frame tmpname {p1}.cat 1 finish
ind(1) = m$index(tmpname,"ext1.bdf")-1
ind(2) = ind(1)-1
write/key infile/c/1/40 " " all
infile = tmpname(2:{ind(1)})

catal = catal-1
store/frame tharname {p3}.cat 1 finish
ind(1) = m$index(tharname,"ext1.bdf")-1
ind(2) = ind(1)-1
write/key tharfile/c/1/40 " " all
tharfile = tharname(2:{ind(1)})
!
write/out "==================================="
write/out "Processing {infile}, {tharfile} ..."
write/out "==================================="
!
@@ xcorall {p1} {infile} {p2} {tharfile} {p4} {p5} enter
!
goto next
finish:
