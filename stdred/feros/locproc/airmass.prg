! %W% (ESO-IPG) %G% %U%
! @@ airmass.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  airmass.prg  
! A. Kaufer	950214
! 
! compute airmass in dependency of UT for actual date on La Silla
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ? C "RA hh,mm,ss : "
define/par p2 ? C "DE dd,mm,ss : "
define/par p3 ? C "date yyyy,mm,dd : "
!
define/local ut/i/1/1 0 
define/local n/i/1/1 0 
define/local ra/r/1/1 0.0 
define/local h/i/1/1 0
define/local m/i/1/1 0
define/local s/i/1/1 0
!
crea/tab AM 3,25 
crea/col AM :UT  R F5.2
crea/col AM :LST R F5.2
crea/col AM :AM  R F5.2
crea/col AM :HA  R F5.2
!
do ut = 0 23 1  ! dark time at La Silla in Summer
 n = n+1
 COMPUTE/AIRMASS {p1} {p2} ? ? ? ? {p3} {ut},0,0
 AM,:UT,@{n}  = {ut}
 AM,:LST,@{n} = {outputd(19)}
 AM,:AM,@{n}  = {outputr(1)}
enddo

h = {p1(1:2)}
m = {p1(4:5)}
s = {p1(7:8)}
ra = {h}+{m}/60.+{s}/3600.
compute/tab AM :HA = :LST-{ra}

sele/tab AM :AM .gt. 0
read/tab AM
plot/axes -0.5,10.5 3,0.8 ?  "UT" "AM"
label/gra "RA {p1}   DE {p2}   DATE {p3}"  5,0.7
set/gra color=2
over/line 1 -0.5,2.5 10.5,2.5
set/gra color=3
over/line 1 -0.5,1 10.5,1
set/gra stype=2 color=4
over/tab AM :UT :AM
set/gra stype=0
over/tab AM :UT :AM
!
define/local nrow/i/1/1 0
define/local x/r/1/1 0
define/local y/r/1/1 0
define/local ha/r/1/1 0
!
set/format F5.1
copy/dk AM.tbl tblcontr/i/4/4 nrow/i/1/1
do n = 1 {nrow}
 x = {AM,:UT,@{n}}+0.0
 y = {AM,:AM,@{n}}-0.2
 ha = {AM,:HA,@{n}}
 if {ha} .lt. -12 then
  ha = ha+24
 endif
 label/gra "{ha}" {x},{y} 0 0.75 0
enddo
!
set/format F5.2
!
define/local de/r/1/1 0.0
define/local phi/r/1/1 -29.25	! Breite La Silla
define/local am/r/1/1 2.5	! airmass
define/local z/r/1/1 0.0	! zenith distance
define/local sign/c/1/1 " " 
define/local lst/r/1/2 0,0

sign = "{p2(1:1)}"
h = {p2(2:3)}
m = {p2(5:6)}
s = {p2(8:9)}
de = {h}+{m}/60.+{s}/3600
if sign .eq. "-" then
 de = -1.*de
endif
z = m$acos(1./am)

ha = m$acos((m$sin(90.-z)-m$sin(de)*m$sin(phi))/(m$cos(de)*m$cos(phi)))
ha = ha/15.
lst(1) = ra-ha
lst(2) = ra+ha
if {lst(1)} .lt.  0. lst(1) = lst(1)+24.
if {lst(2)} .lt.  0. lst(2) = lst(2)+24.
if {lst(1)} .ge. 24. lst(1) = lst(1)-24.
if {lst(2)} .ge. 24. lst(2) = lst(2)-24.

define/local h1/i/1/1 0
define/local m1/i/1/1 0
define/local s1/i/1/1 0
define/local h2/i/1/1 0
define/local m2/i/1/1 0
define/local s2/i/1/1 0
define/local H0/r/1/1 0
define/local K0/r/1/1 0

h1 = lst(1)
H0 = lst(1)-h1
m1 = 60.*H0
K0 = (H0*60.)-m1
s1 = K0*60.
h2 = lst(2)
H0 = lst(2)-h2
m2 = 60.*H0
K0 = (H0*60.)-m2
s2 = K0*60.


write/out "Critical hour angles (AM={am}) for RA={p1}, DE={p2} : HA=+/-{ha} h"
write/out "LST range: {lst(1)} - {lst(2)}"
set/format I2
write/out "LST range: {h1},{m1},{s1}  -  {h2},{m2},{s2}"
write/out "LST range: {h1}h{m1}m  -  {h2}h{m2}m"

finish:
set/gra color=1
set/format