! %W% (ESO-IPG) %G% %U%
! @@ listferos.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  listferos.prg  
! A. Kaufer	981008  
!
! show selected descriptors of a catalog
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ? C "Enter name for file list   : "
define/par p2 ? N "Enter filenumber start,end : "
!
define/local range/i/1/2 {p2}
define/local infile/c/1/40 " " all
define/local i/i/1/1 0 
define/local ind/i/1/1 0 
define/local num/i/1/1 0 
define/local ftype/i/1/1 0 
define/local ra/r/1/1 0.0

crea/tab {p1} 10 10000
crea/col {p1}.tbl :filename ""     I*2  I4
crea/col {p1}.tbl :cident   ""     C*20 A10  
crea/col {p1}.tbl :ident ""        C*20 A10  
crea/col {p1}.tbl :texp  "sec"     R*4 F6.1
crea/col {p1}.tbl :mode  ""        C*2 A2  
crea/col {p1}.tbl :exptype ""      C*2 A2  
crea/col {p1}.tbl :date  "Date/UT" T19.10 R*8
crea/col {p1}.tbl :RA   "hh:mm:ss" R10.5 R*8
crea/col {p1}.tbl :DE   "dd:mm:ss" S11.5 R*8
crea/col {p1}.tbl :AM   ""         R*4 F4.2
crea/col {p1}.tbl :BC   "km/s"     R*4 F7.3

do num = {range(1)} {range(2)} 1
!
 i = i+1
 ftype = 0
!
 ind = M$EXIST("fero{num}.mt")
 if  ind .eq. 1 then
  infile = "fero{num}.mt"
  crea/ima &header 1,1 1,1 NODATA
  cop/dd {infile} *,3 &header
  cop/dd {infile} IDENT &header
  comp/st &header
  comp/air &header >null
  ra = {raw_image,o_pos(1)}
  ra = ra/15.
  comp/bary middummheader.bdf {ra} {middummheader.bdf,o_pos(2)} >null
  write/desc middummheader.bdf bary_corr/r/1/1 {outputr(1)}
  infile = "middummheader.bdf"
  ftype = 3
 endif
! ind = M$EXIST("f{num}ext1.bdf")
! if  ind .eq. 1 then
!  infile = "f{num}ext1.bdf"
!  ftype = 2
! endif
 ind = M$EXIST("f{num}1.bdf")
 if ind .eq. 1 then 
  infile = "f{num}1.bdf"
  ftype = 1
 endif
!
 if ftype .eq. 0 goto next
!
 {p1},:filename,{i} = {num}
 {p1},:cident,{i}   = {{infile},CIDENT}
 {p1},:ident,{i}    = {{infile},IDENT}
 {p1},:mode,{i}    = "{{infile},F_MODE}"
 {p1},:exptype,{i} = "{{infile},EXPTYPE}"
!
  {p1},:texp,{i}     = {{infile},O_TIME(7)}
!
  {p1},:RA,{i}   = {{infile},O_POS(1)}
  {p1},:DE,{i}   = {{infile},O_POS(2)}
!
  copy/dk {infile} O_TIME/D/4/1 jd/d/1/1
  jd = jd+2400000.5
  {p1},:date,{i} =  m$value(jd)
!
  {p1},:AM,{i}   = {{infile},O_AIRM}
  if "{{infile},EXPTYPE}" .eq. "SCIENCE" then
   {p1},:BC,{i}   = {{infile},BARY_CORR}
  endif
!
 read/tab {p1} @{i}
 next:
enddo

assign/print FILE {p1}.list
print/tab {p1} ? ? ? ? 132
write/out "ASCII logfile stored in {p1}.list"
inquire/key ans/c/1/1 "Do you want to print {p1}.list [y/n]" 
if "{ans}" .eq. "y" then
 $lpr -olandscape {p1}.list 
endif

end: