! @(#)create_cats.prg	19.1 (ESO-IPG) 02/25/03 14:21:44
! @@ create_cats.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  create_cats.prg	  
! O.Stahl	981022
!
! extract FF, ThAr and object files from catalog
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ?     C "Enter catalog.cat : "
!
define/local tmpname/c/1/40 " " all
define/local infile/c/1/40 " " all
define/local catal/i/1/1 0 
define/local ind/i/1/1 0 
define/local i/i/1/1 0 
define/local mm/i/1/1 0

crea/icat ThAr null
crea/icat FF null
crea/icat Objects null

next:
store/frame infile {p1} 1 finish
!
! now the operation on the catalog
!
i = i+1

write/out "{infile} {{infile},EXPTYPE} "

if "{{infile},EXPTYPE}" .eq. "CALIBRATION" then
  add/icat ThAr {infile}
!endif
elseif "{{infile},EXPTYPE}" .eq. "FLATFIELD" then
  add/icat FF {infile}
else
add/icat Objects {infile}
endif
!
goto next
finish:
