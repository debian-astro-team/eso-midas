! @(#)temperature.prg	19.1 (ESO-IPG) 02/25/03 14:21:47
! @@ temperature.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  temperature.prg  
! A. Kaufer	981124  
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ? C "Enter name for file list   : "
define/par p2 ? N "Enter filenumber start,end : "
!
define/local range/i/1/2 {p2}
define/local infile/c/1/40 " " all
define/local i/i/1/1 0 
define/local ind/i/1/1 0 
define/local num/i/1/1 0 
define/local ftype/i/1/1 0 

crea/tab {p1} 3 1000
crea/col {p1} :num   R*4 F5.2
crea/col {p1} :JD24  R*8 F13.5
crea/col {p1} :tspec R*4 F5.2
crea/col {p1} :troom R*4 F5.2
crea/col {p1} :rhum  R*4 F7.2

do num = {range(1)} {range(2)} 1
!
 i = i+1
 ftype = 0
!
 ind = M$EXIST("fero{num}.mt")
 if  ind .eq. 1 then
  infile = "fero{num}.mt"
  crea/ima &header 1,1 1,1 NODATA
  cop/dd {infile} *,3 &header
  comp/st &header
  infile = "middummheader.bdf"
  ftype = 3
 endif
! ind = M$EXIST("f{num}ext1.bdf")
! if  ind .eq. 1 then
!  infile = "f{num}ext1.bdf"
!  ftype = 2
! endif
 ind = M$EXIST("f{num}1.bdf")
 if ind .eq. 1 then 
  infile = "f{num}1.bdf"
  ftype = 1
 endif
!
 if ftype .eq. 0 goto next
!
 {p1},:num,{i}   = {num}
 copy/dk {infile} O_TIME/D/4/1 jd24/d/1/1
 {p1},:jd24,{i}  = m$value(jd24)
 {p1},:tspec,{i} = {{infile},TSPEC}
 {p1},:troom,{i} = {{infile},TROOM}
 {p1},:rhum,{i}  = {{infile},RHROOM}
!
 read/tab {p1} @{i}
 next:
enddo
!
set/gra stype=2 xa ya=15,16.5 color=1
sele/tab {p1} :tspec.gt.0
plo/tab {p1}  :jd24  :tspec 
set/gra color=4
over/tab {p1} :jd24  :troom 3
set/gra ya color=1


