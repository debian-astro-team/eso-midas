! @(#)loadccd.prg	19.1 (ESO-IPG) 02/25/03 14:21:45
! @@ loadccd.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  loadccd.prg	  
! O.Stahl, A.Kaufer	981105
!
! PURPOSE
! display incoming new frame and start automatic reduction
!
! USAGE
!
! @@ loadccd fileprefix filenumber
!
! with : fileprefix	:  fixed filename prefix set by BIAS
!        filenumber 	:  running 4-digit filenumber
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ?  C "Enter fileprefix : "
define/par p2 ?  N "Enter filenumber : "
!
define/local filename/c/1/40 " " all
define/local tape/c/1/40 "/dev/rmt/1mn"

write/key filename {p1}{p2}.mt

write/out "File {filename} received, loading"
crea/dis 0 500,1000,760,0
!stat/ima {filename}

load/ima {filename} sc=-4
read/desc {filename}
add/icat Feros.cat {filename}

! write data to tape $TAPE
write/out "Writing {filename} to {tape} in background ..."
$ dd if={filename} of={tape} obs=2880 &

! start automatic reduction
write/out
write/out "Starting on-line DRS on {filename} "
write/out
@@ autoreduce {p1} {p2}

