! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  standard.prg	  
! O.Stahl, A.Kaufer, S.Tubbesing	990201
!
! PURPOSE
! reduce  object-frame
!
!				
! 	exposuretype=SCIENCE 	:	
!					   order extraction		
!					   flatfielding/blaze correction
!					   rebinning: wavelength calibration
!						      barycentric correction
!					   order merging
!					
! USAGE
!
! @@ standard filenumber
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local infile/c/1/60 " " all
define/local num/c/1/4 {p1}
define/local i/i/1/1 0
define/local order/i/1/1 0
define/local ra/d/1/1 0.0
define/local lstart/r/1/1 0.0
define/local lend/r/1/1 0.0
define/local rebstep/r/1/2 0.0
define/local etime/D/1/1 0
DEFINE/LOCAL CATAL/I/1/1 0


SET/FERO EXT_MODE=S
EXTRACT/FEROS back_str b{num}ext {SPECTR_TYPE}{EXT_MODE}{PROFILE_GET}{IMG_WRITE}
FLAT/FERO b{num}ext {FLAT_IMG} f{num}ext

rebstep(1) = {REBIN_STEP}
rebstep(2) = {raw_image,bary_corr}
REBIN/FERO f{num}ext {LINE_POS_TBL} rebinned I {rebstep(1)},{rebstep(2)}

MERGE/FERO rebinned f{num} {MERGE_DELTA} {MERGE_MTD}
!MERGE/FERO rebinned f{num} 
write/out
!
! copy all but standard descriptors
!
cop/dd raw_image *,3 b{num}ext1
cop/dd raw_image *,3 b{num}ext2
cop/dd raw_image IDENT b{num}ext1
cop/dd raw_image IDENT b{num}ext2 
!
cop/dd raw_image *,3 f{num}ext1
cop/dd raw_image *,3 f{num}ext2
cop/dd raw_image IDENT f{num}ext1 
cop/dd raw_image IDENT f{num}ext2 
!
! 
!
if MERGE_MTD(1:1) .EQ. "N" then  
 order = 0
 do i = {ORDER_FIRST} {ORDER_LAST} 1
  order = order+1
  cop/dd raw_image *,3 f{num}1{order}
  cop/dd raw_image *,3 f{num}2{order}
  cop/dd raw_image IDENT f{num}1{order} 
  cop/dd raw_image IDENT f{num}2{order}   
  lstart = {f{num}1{order},START(1)}
  lend   = {f{num}1{order},START(1)}+{f{num}1{order},NPIX(1)}*{f{num}1{order},STEP(1)}
  set/format F4.0 I4
  write/out "Reduced order {order}, {lstart}-{lend}, saved in f{num}1{order} (object fiber)"
  write/out "                                        f{num}2{order} (sky fiber)"
  set/format
 enddo
else
 cop/dd raw_image *,3 f{num}1
 cop/dd raw_image *,3 f{num}2
 cop/dd raw_image IDENT f{num}1 
 cop/dd raw_image IDENT f{num}2 
 lstart = {f{num}1,START(1)}
 lend   = {f{num}1,START(1)}+{f{num}1,NPIX(1)}*{f{num}1,STEP(1)}
 set/format F4.0 I4
 write/out "Reduced 1D spectrum, {lstart}-{lend}, saved in f{num}1 (object fiber)"
 write/out "                                         f{num}2 (sky    fiber)"
 set/format
endif






