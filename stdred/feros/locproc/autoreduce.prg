! @(#)autoreduce.prg	19.1 (ESO-IPG) 02/25/03 14:21:44
! @(#)autoreduce.prg	14.1.1.1 (ESO-IPG) 09/16/99 09:47:18
! @@ autoreduce.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  autoreduce.prg	  
! O.Stahl, A.Kaufer	981105
!
! PURPOSE
! reduce incoming new frame depending on exposure type
!
! 	exposuretype=FLATFIELD 	:	pre-reduction 
!					adding to catalog FF.cat
!					
! 	exposuretype=CALIBRATION:	pre-reduction 
!					adding to catalog ThAr.cat
!					
! 	exposuretype=SCIENCE 	:	if SHSTAT = CLOSED (-> DARK)
!                                          adding to catalog Dark.cat
!                                       if SHSTAT = CLOSED (-> SCIENCE)
!                                          adding to catalog Object.cat
!                                          pre-reduction 
!					   adding to catalog
!					   background subtraction
!					   order straightening
!					   order extraction		
!					   flatfielding/blaze correction
!					   rebinning: wavelength calibration
!						      barycentric correction
!					   order merging
!					
! USAGE
!
! @@ autoreduce fileprefix filenumber
!
! with : fileprefix	:  fixed filename prefix set by BIAS
!        filenumber 	:  running 4-digit filenumber
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ?  C "Enter fileprefix : "
define/par p2 ?  N "Enter filenumber : "
!
define/local infile/c/1/60 {p1}{p2}.mt
define/local num/i/1/1 {p2}
define/local i/i/1/1 {p2}
define/local order/i/1/1 {p2}
define/local ra/d/1/1 0.0
define/local lstart/r/1/1 0.0
define/local lend/r/1/1 0.0
define/local rebstep/r/1/2 0.0
define/local f_mode/c/1/2 OS

!
! sort incoming file into
! Flatfield, ThAr, Dark, and Objects catalogues according
! to image descriptor EXPTYPE and SHSTAT

if "{{infile},EXPTYPE}" .eq. "CALIBRATION" then
  add/icat ThAr {infile}
elseif "{{infile},EXPTYPE}" .eq. "FLATFIELD" then
  add/icat FF {infile}
elseif "{{infile},EXPTYPE}" .eq. "SCIENCE" then 
  if "{{infile},SHSTAT}" .eq. "CLOSED"  then
    write/descr {infile} EXPTYPE/C/1/7 "DARK   "
    add/icat Dark.cat {infile}
  elseif "{{infile},SHSTAT}" .eq. "OPEN"  then
    add/icat Objects {infile}
  endif
endif
write/out "--------------------------------------------"
write/out "{infile}: Exposure type  {{infile},EXPTYPE} "
write/out "--------------------------------------------"
!
! for SCIENCE frames do standard reduction:
!
if "{{infile},EXPTYPE}" .eq. "SCIENCE" then 
!
! pre-reduction
!
 @@ prered {num} raw_image

! convert RA in Hours Minutes Seconds
!
 ra = {raw_image,o_pos(1)}
 ra = ra/15.

! compute barycentric velocity
!
comp/bary raw_image.bdf {ra} {raw_image,o_pos(2)}
write/desc raw_image bary_corr/r/1/1 {outputr(1)}

! reduction starts here
!
BACKGR/FEROS raw_image back
RECTIFY/FEROS back back_str
EXTRACT/FEROS back_str b{num}ext {SPECTR_TYPE}{EXT_MODE}{PROFILE_GET}{IMG_WRITE}
FLAT/FERO b{num}ext {FLAT_IMG} f{num}ext

rebstep(1) = {REBIN_STEP}
rebstep(2) = {raw_image,bary_corr}
f_mode = "{raw_image,f_mode}"
REBIN/FERO f{num}ext {LINE_POS_TBL} rebinned I {rebstep(1)},{rebstep(2)} ? {f_mode}

!MERGE/FERO rebinned f{num} {MERGE_DELTA} {MERGE_MTD}
MERGE/FERO rebinned f{num} 
write/out
!
! copy all but standard descriptors
!
cop/dd raw_image *,3 b{num}ext1
cop/dd raw_image *,3 b{num}ext2
cop/dd raw_image IDENT b{num}ext1
cop/dd raw_image IDENT b{num}ext2 
!
cop/dd raw_image *,3 f{num}ext1
cop/dd raw_image *,3 f{num}ext2
cop/dd raw_image IDENT f{num}ext1 
cop/dd raw_image IDENT f{num}ext2 
!
! 
!
if MERGE_MTD(1:1) .EQ. "N" then
 order = 0
 do i = {ORDER_FIRST} {ORDER_LAST} 1
  order = order+1
  cop/dd raw_image *,3 f{num}1{order}
  cop/dd raw_image *,3 f{num}2{order}
  cop/dd raw_image IDENT f{num}1{order} 
  cop/dd raw_image IDENT f{num}2{order}   
  lstart = {f{num}1{order},START(1)}
  lend   = {f{num}1{order},START(1)}+{f{num}1{order},NPIX(1)}*{f{num}1{order},STEP(1)}
  set/format F4.0 I4
  write/out "Reduced order {order}, {lstart}-{lend}, saved in f{num}1{order} (object fiber)"
  write/out "                                        f{num}2{order} (sky fiber)"
  set/format
 enddo
else
 cop/dd raw_image *,3 f{num}1
 cop/dd raw_image *,3 f{num}2
 cop/dd raw_image IDENT f{num}1 
 cop/dd raw_image IDENT f{num}2 
 lstart = {f{num}1,START(1)}
 lend   = {f{num}1,START(1)}+{f{num}1,NPIX(1)}*{f{num}1,STEP(1)}
 set/format F4.0 I4
 write/out "Reduced 1D spectrum, {lstart}-{lend}, saved in f{num}1 (object fiber)"
 write/out "                                         f{num}2 (sky    fiber)"
 set/format
endif
!
endif
exit:
