! @@ prepare.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure prepare.prg   
! O.Stahl, A.Kaufer, S.Tubbesing	990125
!
! PURPOSE
! initialize catalogues for FF, ThAr, Dark and Objects
!
!
!
! USAGE
!
! @@ prepare
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local tmpname/c/1/40 " " all
define/local infile/c/1/40 " " all
define/local catal/i/1/1 0 
define/local ind/i/1/1 0 
define/local i/i/1/1 0 
define/local mm/i/1/1 0

$ln -s  ../Objects.tbl
$ln -s  ../ThAr50000.tbl
$ln -s ../BLAZE.tbl
$ln -s ../darks/DARK.bdf

write/out "Resetting catalogues for Flats, ThArs, Darks, and Objects" 
crea/icat Feros null
crea/icat ThAr null
crea/icat FF null
crea/icat Dark null
crea/icat Objects null

set/context feros

crea/icat Feros fero????.mt

next:
store/frame infile Feros.cat 1 done

if "{{infile},EXPTYPE}" .eq. "CALIBRATION" then
  add/icat ThAr {infile}
elseif "{{infile},EXPTYPE}" .eq. "FLATFIELD" then
  add/icat FF {infile}
elseif "{{infile},EXPTYPE}" .eq. "SCIENCE" then 

  if "{{infile},SHSTAT}" .eq. "CLOSED"  then
    write/descr {infile} EXPTYPE/C/1/7 "DARK   "
    add/icat Dark.cat {infile}
  elseif "{{infile},SHSTAT}" .eq. "OPEN"  then

    add/icat Objects {infile}
  endif

endif

write/out "--------------------------------------------"
write/out "{infile}: Exposure type  {{infile},EXPTYPE} "
write/out "--------------------------------------------"

goto next
done:






