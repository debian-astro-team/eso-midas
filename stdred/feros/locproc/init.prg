! @(#)init.prg	19.1 (ESO-IPG) 02/25/03 14:21:45
! @@ init.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  init.prg	  
! O.Stahl, A.Kaufer	981105
!
! PURPOSE
! initialize catalogues for FF, ThAr, and Objects
! average all available FFs and ThArs
! reduce averaged FF, ThAr as guess for auto reduction
!
! USAGE
!
! @@ init guess action
!
! with : guess   :  Name of guess session to be used
!        action  :  Action to be taken:
!		   	start   :  full procedure from start
!			flat    :  start with reduction of flat
!			calib	:  start with reduction of calibration	
!			reset	:  reset catalogues 
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 #           C "Enter name of guess session : "
define/par p2 start       C "Enter action: [start/flat/calib/reset] : "
!
!
define/local tmpname/c/1/40 " " all
define/local infile/c/1/40 " " all
define/local catal/i/1/1 0 
define/local ind/i/1/1 0 
define/local i/i/1/1 0 
define/local mm/i/1/1 0
define/local action/c/1/40 {p2}
!
define/local lgain/r/1/1 2.60
define/local hgain/r/1/1 0.66
!
! jump according to required action
!
goto {action}

reset:
write/out "Resetting catalogues for Flats, ThArs, Darks, and Objects" 
!crea/icat Feros null
crea/icat ThAr null
crea/icat FF null
crea/icat Dark null
crea/icat Objects null

write/out
write/out "Make sure that an empty DAT is inserted in the drive"
write/out "for the on-line backup. "
write/out "!! The DAT will be written from its current position !! "
goto exit

start:
!
! set FEROS context 
! initialize session according to a saved guess session,
! 	i.e., typically the session from the last night
!
set/context feros
if "{p1}" .eq. "#" then
 INIT/FEROS
 write/out "Default session parameters loaded"
else
 INIT/FEROS  {p1}
 write/out "Session parameters loaded from session {p1}"
endif

!
! average flatfield expsoures, 
! averaged flat is used for further reductions
!
@@ average_cat FF.cat

! average calibration exposures, 
! averaged thar is used for wavelength calibration
!
@@ average_cat ThAr.cat

flat:
! reduce FF: 	order definition
!		background subtraction
!		order straightening
!		order extraction		
!
load/ima {RAW_IMG} sc=-4
! set gain acc. to descriptor 
write/out 
if "{{RAW_IMG},GAINM}" .eq. "LOW" then
 SET/FEROS CCD_GAIN={lgain}
else
 SET/FEROS CCD_GAIN={hgain}
endif
write/out "CCD_GAIN set to {CCD_GAIN}"
!
DEFINE/FEROS 
load/tab {CENTER_TBL} :Fit_pos :Ypos ? 0 1  
BACKGR/FEROS {RAW_IMG} back
RECTIFY/FEROS back back_str
! prepare cross-order profiles for optimum extraction
EXTRACT/FEROS back_str {RAW_IMG}ext GOYN
! extract flat with standard extraction
EXTRACT/FEROS back_str {RAW_IMG}ext GSNN
SET/FEROS FLAT_IMG={RAW_IMG}ext

clear/channel over


calib:
!reduce ThAr	background subtraction
!		order straightening
!		order extraction		
!		line search
!		wavelength calibration
!
load/ima {WLC_IMG} sc=-4
BACKGR/FEROS {WLC_IMG} back
RECTIFY/FEROS back back_str
EXTRACT/FEROS back_str {WLC_IMG}ext GS

SET/FEROS LINE_POS_TBL={WLC_IMG}lines
SEARCH/FERO {WLC_IMG}ext {LINE_W},{LINE_THRES},1 {LINE_POS_TBL}
CALIBRATE/FEROS {LINE_POS_TBL} ? ? ? ? {INIT_WLC}
set/gra xa ya
sele/tab {LINE_POS_TBL}1 abs(:residual).lt.0.05
plot/tab {LINE_POS_TBL}1 :mabs :residual
stat/tab {LINE_POS_TBL}1 :residual
label/graphic "rms = {outputr(4)} \AA " 25,0.02 0 1 1

SAVE/FEROS {WLC_IMG}
write/out "Session parameters saved as session {WLC_IMG}"

exit:
