! %W% (ESO-IPG) %G% %U%
! @@ average_cat.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  average_cat.prg	  
! O.Stahl	981022
!
! average catalog
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ?     C "Enter catalog.cat : "
!
define/local tmpname/c/1/40 " " all
define/local num/c/1/4 " " all
define/local infile/c/1/40 " " all
define/local catal/i/1/1 0 
define/local ind/i/1/1 0 
define/local i/i/1/1 0 

i = m$index(p1,".")-1

write/key tmpname/c/1/{i} {p1}/c/1/{i}

crea/icat tempframes null

i = 0
next:
store/frame infile {p1} 1 finish
!
! now the operation on the catalog
!
i = i+1

write/key num/c/1/4 {infile(5:8)}
if i .eq. 1 then
 write/key outname/c/1/10 {tmpname}{num}
endif 
write/out {infile} {num} {tmpname}xxx{i}
@@ prered {num} xxx{tmpname}{i}
add/icat tempframes xxx{tmpname}{i}
!
goto next
finish:

aver/ima {outname} = tempframes.cat
write/out "average {tmpname} stored in {outname}"

if "{tmpname}" .eq. "FF" then
 set/feros RAW_IMG={outname}
endif
if "{tmpname}" .eq. "ThAr" then
 set/feros WLC_IMG={outname}
endif

dele/icat tempframes NO