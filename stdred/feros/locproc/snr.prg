! @(#)snr.prg	19.1 (ESO-IPG) 02/25/03 14:21:46
!
! SNR for a region of a 1-dim frame 
! 
! use via
!
!  SNR filename start,end
!    with     
!           filenumber  :  4-digit filenumber of input file
!           start,end   :  wavelength intervall to measure S/N
!
define/para  p1 ? I "Enter filenumber: " 
define/para  p2 ? N "Enter start,end wavelength: " 
define/para  p3 1 N "Enter fiber [1/2]: " 
!
define/local int/d/1/2 {p2}
define/local infile/c/1/40 f{p1}{p3}
define/local snr/r/1/1 0.
define/local npix/i/1/1 0
define/local win/i/1/1 0
!
! extract region 
!
extract/ima &x = {infile}[{int(1)}:{int(2)}]
npix = {&x,NPIX(1)} 
win  = m$nint(npix/20.)
if win .gt. 50 then
 win = 50
endif
!filt/gauss &x &y {win}
filt/median &x &y {win}
!
set/gra xa ya
!plo &x
!over &y
!
! mean = 0
!
comp &SNR = &x/&y-1.
plo &SNR
!
! compute sdev and SNR
!
stat/ima &SNR
snr = 1/{outputr(4)}
!
write/out "-------------------------------------------"
set/format F6.0
write/out "{infile} [{int(1)}:{int(2)}]:  S/N = {snr}"
write/out "-------------------------------------------"
!
dele/temp no

