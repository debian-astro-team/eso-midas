! %W% (ESO-IPG) %G% %U%
! @@ focus.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  focus.prg	  
! O.Stahl, A.Kaufer	981105
!
! PURPOSE
! measure calibration lines with gauss-hermite polynomials
! to check FEROS image-quality/focus
!
! USAGE
!
! @@ focus filenumber
!
! with :    filenumber 	:  running 4-digit filenumber
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/para p1 ? C "Enter Thorium-Argon frame number: "
!
define/local fn/i/1/1 0
define/local fib/c/1/20 " " all
!
!goto start 
@@ prered {p1} raw_image
BACK/FEROS raw_image back
STRAIGHTEN/FEROS back back_str
EXTRACT/FEROS back_str b{p1}ext GSNN
!
FIND/FERO b{p1}ext 5,10000,1 b{p1}lines
CALIBRATE/FEROS b{p1}lines ? ? ? ? c
crea/gra 0
set/gra ya=-0.05,0.05
plot/tab b{p1}lines1 :mabs :residual
set/gra ya

!start: 
crea/gra 9 600,600
set/gra color=1

set/format I1
do fn = 1 2 
 set/gra color=1
 if {fn} .eq. 1 then
  plot/ax 0,4096 0,8 -155,-75,15,15 "x-axis [pixels]" "FWHM_x [pixels]" 
  fib = "object"
 else
  over/ax 0,4096 0,8 -155,-75,15,95 " "              "FWHM_x [pixels]" 
  fib = "sky"
  label/gra "FEROS focus test (input file: fero{p1}.mt)" 0,8.5 0 1.5 1
 endif
 label/gra "Fiber {fn} ({fib})" 100,7 0 1.5 1
 over/line 1 0,2.2 4096,2.2 
 over/line 2 0,2.53 4096,2.53 
 over/line 2 0,1.7 4096,1.7

 sele/tab b{p1}lines{fn} :wave.NE.NULL.and.abs(:residual).lt.0.02
!sele/tab b{p1}lines{fn} :peak.gt.20000
 copy/tab b{p1}lines{fn} &lines{fn}
 comp/tab &lines{fn} :x = (:x+30.72)/0.015

 copy/ii b{p1}ext{fn} &ext
 write/descr &ext START/D/1/2 1,1
 write/descr &ext STEP/D/1/2 1,1

 IMAQUA/FERO &ext &lines{fn}.tbl 8 4

 comp/tab &lines{fn} :GH1m = :GH1-1.
 sele/tab &lines{fn} abs(:GH2).lt.0.05.and.abs(:GH1m).lt.0.05

 copy/tab &lines{fn} &liness{fn}

 sele/tab &liness{fn} :y.ge.22 
 ! .and.:peak.lt.70000 
 set/gra color=2
 stat/tab &liness{fn} :GFWHM
 over/tab &liness{fn} :x :GFWHM 4

 sele/tab &liness{fn} :y.gt.8.and.:y.lt.22 
 ! .and.:peak.lt.70000 
 set/gra color=3
 stat/tab &liness{fn} :GFWHM
 over/tab &liness{fn} :x :GFWHM 3

 sele/tab &liness{fn} :y.le.8 
 ! .and.:peak.lt.70000
 set/gra color=4
 stat/tab &liness{fn} :GFWHM
 over/tab &liness{fn} :x :GFWHM 2

 set/gra color=1
 sele/tab &liness{fn} all

enddo
set/gra xa ya color 
!
end: