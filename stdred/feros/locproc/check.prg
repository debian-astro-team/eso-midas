!! @@ check.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  check.prg	  
! O.Stahl, S.Tubbesing		  990125
!
! PURPOSE
! Check table for dark and extmode
!
! USAGE
! @@ check filename
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local infile/c/1/40 {p1}
define/local object/c/1/40 " " all
define/local len/i/1/1 0
define/local sel/i/1/1 0

define/local extmode/c/1/1 " " 

write/key object {{infile},CIDENT}

len = m$len(object)
sele/tab Objects :name .eq. "{object(1:{len})}*"
cop/tab Objects &a

sel = {&a.tbl,tblcontr(4)}

if {sel} .GT. 0 THEN

  dark = "{&a.tbl,:dark,@1}" 
  extmode = "{&a.tbl,:extmode,@1}" 

else

  write/out "Warning: Object not found, using defaults ***"
  dark = "N"
  extmode = "S"

endif

write/out "Object:" {object} ", Dark:" {dark} ", ExtMode:" {extmode}

SET/FEROS EXT_MODE={extmode}

