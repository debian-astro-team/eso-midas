! @(#)fitimaqual.prg	19.1 (ESO-IPG) 02/25/03 14:21:45
!
! MIDAS procedure fitimaqual.prg 
! A. Kaufer	980724
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!  
define/par p1 ?         C  "Enter intable: "
define/par p2 ?         C  "Enter dependent column: "
define/par p3 :x        C  "Enter x column: "
define/par p4 :yabs     C  "Enter y column: "
define/par p5 ref2D     C  "Enter reference image: "
!
WRITE/KEYW in_a {p1}.tbl
WRITE/KEYW in_b centers.tbl 
run STD_EXE:ypos.exe

fit/tab 100,0.001 {p1} {p2} {p3},{p4} plane
!
crea/ima  {p5} 2,2048,1024 1,1,2,2 NODATA
!
write/out "create fit image {p1}fit"
compute/fit {p1}fit = plane({p5})
load/ima {p1}fit sc=-4
!load/ima {p1}fit sc=-2,10
end:



