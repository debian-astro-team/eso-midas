! %W% (ESO-IPG) %G% %U%
!==========================================================================
!
!    fiberratio.prg
!    A. Kaufer, ESO, 23-Jan-1999
!
!    Compute relative fiber efficiency
!
!    Syntax:  @@ fiberratio [filenum]
!
!    with 
!     filenum : the 4-digit number of the spectrum of the sky spectrum, 

!###########################################################################

def/par P1 ? I "Enter number of sky spectrum frame: "
!
define/local num/i/1/1 {p1}
define/local i/i/1/1 0
define/local win/i/1/3 0 all
define/local rebstep/r/1/2 0,0
define/local mergemtd/c/1/20 " " all
!
!
! pre-reduction
!
@@ prered {num} raw_image
!
! reduction starts here
!
BACKGR/FEROS raw_image back
RECTIFY/FEROS back back_str
EXTRACT/FEROS back_str b{num}ext {SPECTR_TYPE}S{PROFILE_GET}{IMG_WRITE}
rebstep(1) = {REBIN_STEP}
rebstep(2) = 0
REBIN/FERO b{num}ext {LINE_POS_TBL} rebinned I {rebstep(1)},{rebstep(2)}

mergemtd = "{MERGE_MTD}"
MERGE/FERO rebinned fib 1,39 NOAPP
SET/FEROS MERGE_MTD={mergemtd}

crea/tab fiberratio{num} 3 50 
crea/col fiberratio{num} :order I*2
crea/col fiberratio{num} :lamc  R*4
crea/col fiberratio{num} :ratio R*4

do i = 1 39
 comp/ima ratio{i} = fib1{i}/fib2{i}
 win(1) = {ratio{i},NPIX(1)}/2
 win(2) = win(1)-500
 win(3) = win(1)+500
 stat/ima ratio{i}[@{win(2)}:@{win(3)}]
 fiberratio{num},:order,{i} = i
 fiberratio{num},:lamc,{i} = win(1)*{ratio{i},STEP(1)}+{ratio{i},START(1)} 
 fiberratio{num},:ratio,{i} = outputr(8)
enddo

write/out "-------------------------------------------------------------------"
write/out "Computed ratio of fiber efficiency (fiber 1/fiber 2) stored in     "
write/out "table efficiency{p1}"
write/out "Use columns :lamc :ratio"
write/out "-------------------------------------------------------------------"
!
plo/ax 3500,9300 0.9,2.0  ? "Wavelength [\AA ]" "Fiber Efficiency Ratio"
over/tab fiberratio{num} :lamc :ratio 2
label/gra "FEROS fiber efficiency test (input files: fero{p1}.mt)" 3500,2.03 0 1 1
set/gra xa ya