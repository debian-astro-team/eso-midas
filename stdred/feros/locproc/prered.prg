! @(#)prered.prg	19.1 (ESO-IPG) 02/25/03 14:21:46
! @@ prered.prg	  
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  prered.prg	  
! O.Stahl, A.Kaufer,S.Tubbesing	981105
!
! PURPOSE
!  Pre-reduction of FEROS echelle data :
!				convert FITS -> BDF
!				bias subtraction
!				extraction of useful area		
!				bad column correction
!				compute Julian Date, Siderial Time
!				compute Airmass
!
!! WARNING: Adapted to CCD #?? EEV 2kx4k 15mu
!					
! USAGE
!
! @@ prered filenumber outfile
!
! with :  filenumber 	:  running 4-digit filenumber
!	  outfile	:  optional name of outfile, 
!			   if not given, oufile will be b{filenumber}
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ?   N "Enter 4 digit filenumber:"
define/par p2 TMP ? "Output filename:"
!
define/local infile/i/1/1 {p1}
define/local tmpfile/c/1/20 {p2}
define/local bias/r/1/1 0
define/local ra/d/1/1 0.0
define/local start/d/1/2 0.000,0.000
define/local step/d/1/2  0.015,0.015
define/local size/i/1/2  2148,4102

! convert FITS -> BDF
!
indisk/fits fero{infile}.mt fero{infile}

! check for right detector name
!
if "{fero{infile},DETNAME}" .ne. "EEV 2k x 4k" then
 write/out "WARNING: Detector Name has changed !"
 write/out "         verify software settings in prered.prg first !!!"
 return
endif

! compute bias from overscan region
!
stat/ima fero{infile} [@3,<:@47,>] #101 150,250
!plot/histo fero{infile} 
bias = outputr(8)
write/out "bias = {bias}"

! extract useful area 
!
extr/ima b{infile} = fero{infile}[@51,<:@2098,>]
dele/ima fero{infile} NO

! replace bad columns by mean of neighboring columns
!
!@@ colmean b{infile}  336,344,647,901,1063,1065,1300,1702
!Halpha !
@@ colmean b{infile}  321,327,336,344,647,901,1063,1065,1300,1702

! subtract bias 
!
comp/ima b{infile} = b{infile}-{bias}

! set world coordinates to mm, chip center=0mm,0mm
!
start(1) = ({b{infile},start(1)}-1.-size(1)/2.)*step(1)
start(2) = ({b{infile},start(2)}-1.-size(2)/2.)*step(2)
write/desc b{infile} start/d/1/2 {start(1)},{start(2)}
write/desc b{infile} step/d/1/2  {step(1)},{step(2)}

!write/desc b{infile} start/d/1/2 -15.36,-30.76
!write/desc b{infile} step/d/1/2 0.015,0.015
read/descr  b{infile}

!!! convert ra in degree's, only necessary for early COMM1 data!!!
!!!
!ra = {b{infile},o_pos(1)}
!comp/key ra = ra*15
!write/desc b{infile} o_pos/d/1/1 {ra}
!!!
!!!

! compute Julian Date
! 
compute/st b{infile}

! compute Airmass
!
compute/air b{infile}

! rename frame 
!
if "{tmpfile}" .ne. "TMP" then
  rename/ima b{infile} {p2}
  write/out "Pre-reduced frame renamed to {p2}"
else
  write/out "Pre-reduced frame is named b{infile}"
endif
