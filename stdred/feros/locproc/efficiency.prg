! %W% (ESO-IPG) %G% %U%
!==========================================================================
!
!    efficiency.prg
!    O. Stahl, ESO, 8-Oct-1998
!
!    Compute FEROS efficiency + telescope + fibres
!
!    Syntax:  @@ efficiency [filenum] [standard] 
!
!    with 
!     filenum : the 4-digit number of the spectrum of the object, 
!    standard : the name of the MIDAS tabe containin the standard star.
!###########################################################################

def/par P1 ? I "Enter object frame: "
def/par P2 ? I "Standard star name: "

def/local N/i/1/1 0
def/local i/i/1/1 0
def/local j/i/1/1 0
def/local start/d/1/1 0.0
def/local effi/d/1/1 0.0
def/local step/d/1/1 0.0
def/local end/d/1/1 0.0
def/local phot/d/1/1 0.0
def/local npoints/d/1/1 0.0
def/local nstand/d/1/1 0.0
def/local diff/d/1/1 0.0
def/local num/i/1/1 0
def/local texp/r/1/1 0.0
def/local mergemtd/c/1/20 " " all

def/local numcoun/d/1/1 0.0
def/local wave/d/1/3 0.0 all
def/local eff1/d/1/1 0.0
def/local eff2/d/1/1 0.0

crea/tab efficiency 2 1000
crea/col efficiency :wave
crea/col efficiency :eff
crea/col efficiency :order

REBIN/FERO b{p1}ext {LINE_POS_TBL} rebinned I {REBIN_STEP},0
mergemtd = "{MERGE_MTD}"
MERGE/FERO rebinned eff 1,39 NOAPP
SET/FEROS MERGE_MTD={mergemtd}

write/out "Extinction correction for airmass = {b{p1}ext1,O_AIRM}"
do i = 1 39
 write/out "Order {i}: "
 cop/dd b{p1}ext1 *,3 eff1{i}
 EXTINC/SPEC eff1{i} ext1{i} ? MID_EXTINCTION:atmoexan 
enddo

copy/tab /midas/calib/data/spec/{P2} work
!
texp = {b{p1}ext1,O_TIME(7)}
write/out "---------------------------------------------------------------"
write/out "The following numbers have been ectracted from the descriptors:"
write/out "Exposure time = {texp} seconds"
write/out "CCD gain      = {CCD_GAIN} e-/ADU"
write/out "---------------------------------------------------------------"
!
!Compute the photons /A at the 1.5m telescope
comp/table work :phan = {texp}*((1.5*5.5*10**10)/:WAVE)*10**(-0.4*#2)
!copy/table work work :WAVE :phan

do N = 1 39
 start = {ext1{N},START}
 step = {ext1{N},STep} 
 npoints = {ext1{N},Npix(1)}
 nstand = {work.tbl,tblcontr(4)}
 do j = 1 {nstand}
  wave(1) = {work,:wave,@{j}}
  wave(2) = wave(1)-1.
  wave(3) = wave(1)+1.
  end = {START}+{NPOINTS}*{STEP}
  if {wave(2)} .gt. {START} then
   if wave(3) .lt. {end} then
    phot = {work,:phan,@{j}}
    stat/ima ext1{N}[{wave(2)}:{wave(3)}] >Null
    numcoun = {outputr(3)}
    if {numcoun} .gt. 1 then
     effi = ({CCD_GAIN}*{numcoun}) / ({phot} * {wave} * 1.e-5 )
     write/out {effi} {wave(1)} {numcoun} {phot} {step}
     num = num+1
     efficiency,:wave,@{num} = {wave(1)}
     efficiency,:eff,@{num} = {effi}
     efficiency,:order,@{num} = {n}
    endif
   endif
  endif
 enddo
enddo
!
sort/table efficiency :wave
crea/column efficiency :wave1
crea/column efficiency :eff1
nstand = {efficiency.tbl,tblcontr(4)}
do j = 1 {nstand}
 n = j+1
 wave(1) = {efficiency.tbl,:wave,@{j}}
 wave(2) = {efficiency.tbl,:wave,@{n}}
 eff1 = {efficiency.tbl,:eff,@{j}}
 eff2 = {efficiency.tbl,:eff,@{n}}
 diff = wave(1)-wave(2)
 if M$abs({diff}) .lt. 0.5  then
  effi = {eff1} + {eff2}
  efficiency,:wave1,@{j} = {wave(1)}
  efficiency,:eff1,@{j} = {effi}
 else
  efficiency,:wave1,@{j} = {wave(1)}
  efficiency,:eff1,@{j} = {eff1} 
 endif
enddo
!
sele/tab efficiency all
copy/tab efficiency efficiency{p1}
write/out "-------------------------------------------------------------------"
write/out "Computed efficiencies for fiber 1 stored in table efficiency{p1}"
write/out "Use columns :wave1 :eff1"
write/out "-------------------------------------------------------------------"
!
plo/ax 3500,9300 -0.01,0.20  ? "Wavelength [\AA ]" "Efficiency of Tel+Fib+Inst+Det"
over/tab efficiency{p1} :wave1 :eff1 2
label/gra "FEROS efficiency test (input files: fero{p1}.mt, {p2})" 3500,0.205 0 1 1
set/gra xa
set/format