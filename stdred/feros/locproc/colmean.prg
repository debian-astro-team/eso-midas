! @(#)colmean.prg	19.1 (ESO-IPG) 02/25/03 14:21:44
!
! Execute the command :
! 
! @ colmean file list_of_columns
! ------------------------------------------------------------------------------
DEFINE/PAR P1 ?          I  "output image:"
DEFINE/PAR P2 ?          N  "list of columns:"
WRITE/KEY INPUTI/I/1/20 -1 ALL
WRITE/KEY IN_A            {P1}
WRITE/KEY INPUTI/I/1/20   {P2}     
!
RUN STD_EXE:COLMEAN
!
