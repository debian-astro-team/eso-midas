/* @(#)center_all_orders.c	19.1 (ESO-IPG) 02/25/03 14:21:23 */
/* 

   Otmar Stahl

   center_all_orders.c   

   center orders 

*/

/* system includes */

#include <stdio.h>
#include <math.h>

/* general Midas includes */

#include <midas_def.h>

/* FEROS specific includes */

#include <echelle.h>
#include <nrutil.h>
#include <mutil.h>
#include <misc.h>

#ifdef __STDC__
int center_all_orders
(
 float imageo[], float imaget[], float corr_arr[], 
 int xcenter[], int xcenter_0[], int offset[], 
 float center_0[], int line_no, int npixt [], float tab[], 
 double start[], double step[], int npix[],
 int win, int width2, int nact, int tid, int first_entry,
 int icol[], int left_edge, int right_edge, int lnfit_method, int writemode
 )
#else
int center_all_orders
(
 imageo, imaget, corr_arr, xcenter, xcenter_0, 
 offset, center_0, line_no, npixt, tab, 
 start, step, npix, win, width2, nact, tid, first_entry, 
 icol, left_edge, right_edge, lnfit_method, writemode
 )
     
     float imageo[], imaget[], corr_arr[], center_0[], tab[];
     int xcenter[], xcenter_0[], offset[];
     int line_no, npixt [], npix[];
     double start[], step[]; 
     int win, width2, nact, tid, first_entry, icol[];
     int left_edge, right_edge, lnfit_method, writemode;
#endif
     
{
#define XCORR_THRES 0.7
#define X_SHIFT_THRES 3.0
    
  int first_pix, center, index, i;
  float maxi, shift, diff, xdiff;
  int tablerow;
  char line[40];
    
  first_pix = line_no * npix[0];
  tablerow = first_entry;

  /* loop over orders */

  for(i = 0; i < nact; i++)
    {

      /* xcenter == 0 means order not centered (lost) */

      if (xcenter[i] > 0)
	{
	  center = first_pix + xcenter[i];

	  /* here call center routine, get index, factor, shift   */

	  center_one_order
	    (
	     imageo, imaget, corr_arr, center, npixt,
	     width2, i, win, lnfit_method, &shift, &maxi, &index
	     );
	}
      
      if(writemode == WM_DOWRITE)
	{
	  if (xcenter[i] > 0)
	    {
	      if (fabs(shift) > X_SHIFT_THRES) 
		{
		  sprintf(line, "big shift detected %f, order %d lost", 
			  fabs(shift), i+1);
		  SCTPUT(line);
		  xcenter[i] = 0;
		}
	    }
      
	  if (xcenter[i] > 0)
	    {
	      if (maxi < XCORR_THRES)
		{
		  sprintf(line, "bad match %f, order %d lost", 
			  maxi,i+1);
		  SCTPUT(line);
		  xcenter[i] = 0;
		}
	    }

	  if (xcenter[i] > 0)
	    {
	      if ((xcenter[i] < left_edge) || (xcenter[i] > right_edge))
		{
		  sprintf(line, "hit edge, order %d lost", i + 1);
		  SCTPUT(line);
		  xcenter[i] = 0;
		}
	    }
	}
      
      if (xcenter[i] > 0)
	{
	  diff =  center_0[i] + step[0] * (index + shift + offset[i]);

	  /* write result to output table */	      

	  xdiff = diff - center_0[i];
	  xcenter[i] = xcenter_0[i] + xdiff / step[0]; /* int -> float ??*/
	  offset[i] = xcenter[i] - xcenter_0[i];
	      
	  tab[0] = diff;
	  tab[1] = start[1] + line_no * step[1];

	  tab[2] = (float) (i + 1);
	  tab[3] = maxi;
	  
	  if(writemode == WM_DOWRITE)
          {
            tablerow = first_entry + i + 1;
            TCRWRR(tid, tablerow, 4, icol, tab);
          }
	}
    } /* end for loop over orders */
  return 0;
}
