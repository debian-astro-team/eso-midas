$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.STDRED.FEROS.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:25:00 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libech extract_cuts.obj,fit_orders.obj,search_lines.obj,fit_line.obj,fold_image.obj,follow_orders.obj,center_one_order.obj,center_all_orders.obj,fit_back.obj,glsp.obj
$ LIB/REPLACE libech extract_spec.obj,rectify.obj,spatial_profile.obj,interpolate.obj,opt_ext.obj,extract_profiles.obj,wavecal.obj,fitfunc.obj,comp_back.obj,lnmatch.obj,rebin.obj,dsplint.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
