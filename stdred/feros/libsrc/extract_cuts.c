/* @(#)extract_cuts.c	19.1 (ESO-IPG) 02/25/03 14:21:23 */
/*

  Otmar Stahl

  extract_cuts.c

  extract lines from echelle spectrum
  
  IN:    
  imno:            input frame
  first_cut:       first cut in y
  cut_step :       step in y
  cut_nr:          number of cuts
  npix:            size of input frame
  
  OUT:
  imageo:          array holding output, space must be reserved

*/

/* general Midas includes */

#include <midas_def.h>

int extract_cuts 
#ifdef __STDC__
(
 int imno, int first_cut, int cut_step, int cut_nr, 
 float imageo[], int npix[]
 )
#else
     (
      imno, first_cut, cut_step, cut_nr, imageo, npix
      )
     int  imno, first_cut, cut_step, cut_nr, npix[];
     float imageo[];
#endif


{
  float *buffer;
  int i, j, jj, actvals, pix_step, k, kk, l;
  
  buffer =  (float *)osmmget(npix[0] * sizeof(float));
  
  pix_step = cut_step * npix[0];
  j = -npix[0];

  jj = (first_cut + 1)* npix[0] + 1; 

  for(i = 0; i < cut_nr; i++)
    {

      /* -1 0 + 1 */

      /*      kk = jj - npix[0];   */

      kk = jj;  

      j = j + npix[0];

      /*  average three lines */
      
      for(k = 0; k<npix[0]; k++)
	{
	  imageo[j+k] = 0.0;
	}
      for(k = 0 ; k<1; k++)
	{
	  SCFGET(imno, kk, npix[0], &actvals, (char *)buffer);
	  for(l = 0; l<npix[0]; l++)
	    {
	      imageo[j+l] = imageo[j+l]+buffer[l];
	    }
	  kk = kk + npix[0];
	}

      /*      SCFGET(imno, jj, npix[0], &actvals, (char *) &imageo[j]); */

      jj = jj + pix_step;
    }
  return 0;
}

