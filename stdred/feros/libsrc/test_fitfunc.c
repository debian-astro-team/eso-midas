#include <fitfunc.h>
#include <misc.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>
void fit_wavelength2
(
 double inimage[], double ximage[], double yimage[], int npixi,
 double outimage[], double ximageo[], double yimageo[], int npixo,
 int fit_deg, double a[], int printout
 );

#define is_close_abs(a, expected, abs) \
    assert(fabs((a) - (expected)) <= (abs))

#define is_close_rel(a, expected, rel) \
    do { \
        assert(isfinite(a)); \
        if (((fabs((a) - (expected))) / fabs(expected)) > (rel)) { \
            printf("%g - %g = (%g %g%%)\n", a, expected, fabs(a - expected), \
                   100*fabs((a - expected) / expected)); \
            assert(((fabs((a) - (expected))) / fabs(expected)) <= (rel)); \
        } \
    } \
    while (0)


void test_fit_gauss(void)
{
    /*
    x = np.linspace(-5, 5)
    y = np.polynomial.hermite.hermval(x, [1., 0., 0., 0.04, 0.0004])
    y += gaus(amp=500, mean=0, sigma=2)
    */
    double y[] = {
        0., /* NR padding */
        -1.110673318829629252e+01,
        -8.430347160013802466e-01,
        1.050458431735285458e+01,
        2.314440794370152332e+01,
        3.728139490927527078e+01,
        5.310722821523716419e+01,
        7.078866242142346721e+01,
        9.045455922511084168e+01,
        1.121821992572330799e+02,
        1.359836472136937005e+02,
        1.617931071966135335e+02,
        1.894563130545624858e+02,
        2.187230334038349042e+02,
        2.492437158058824878e+02,
        2.805711391408725035e+02,
        3.121676868142657781e+02,
        3.434185063151546160e+02,
        3.736504044641893643e+02,
        4.021558744706997572e+02,
        4.282212005499583825e+02,
        4.511571822496235313e+02,
        4.703307069807533480e+02,
        4.851952128542591254e+02,
        4.953180514006514272e+02,
        5.004028931809090750e+02,
        5.003056139860157714e+02,
        4.950425335466960064e+02,
        4.847904155334148868e+02,
        4.698782288466425712e+02,
        4.507712614173173051e+02,
        4.280487145959765485e+02,
        4.023763404330113644e+02,
        3.744759788422121005e+02,
        3.450939850697551492e+02,
        3.149705053677596425e+02,
        2.848113723770240995e+02,
        2.552640780699051390e+02,
        2.268988785023907724e+02,
        2.001956342557625703e+02,
        1.755365372300179274e+02,
        1.532044582703112781e+02,
        1.333863029895215107e+02,
        1.161805067469767323e+02,
        1.016076443082223761e+02,
        8.962307450377336693e+01,
        8.013057509780212229e+01,
        7.299603099192120226e+01,
        6.806039864641644499e+01,
        6.515135877650600094e+01,
        6.409326681170369966e+01,
    };

    int i;
    double A[4];
    double coef[4];
    int nval = sizeof(y) / sizeof(y[0]) - 1;
    int ncoef = sizeof(coef) / sizeof(coef[0]) - 1;
    double x[nval + 1];
    x[0] = 0;
    x[1] = -5.;
    for (i = 2; i < nval + 1; i++) {
        x[i] = x[i - 1] + 10. / (nval - 1);
    }
    x[nval] = 5.;

    fit_gh(x, y, nval, A, ncoef, coef);
    for (i = 1; i < 4; i++) {
        printf("%g\n", A[i]);
    }
    assert(fabs(A[1] - 500) <  0.1);
    assert(fabs(A[2] - 0.) <  0.06);
    assert(fabs(A[3] - 2.) <  0.05);
    for (i = 1; i < 4; i++) {
        printf("%g\n", coef[i]);
    }
    assert(fabs(coef[1] - 1.) <  0.1);
    assert(fabs(coef[2] - 0.04) <  0.001);
    assert(fabs(coef[3] - 0.004) <  0.0001);
}


void test_fit_wavelength(void)
{
    int i, n = 817, no = 1773;
    
    FILE * f = fopen("x.dat", "rb");
    double x[n + 1], y[n + 1], z[n + 1];
    double out_x[no + 1], out_y[no + 1], out_z[no + 1], out_z_e[no + 1];
    fread(&x[1], 8, n, f);
    fclose(f);
    f = fopen("y.dat", "rb");
    fread(&y[1], 8, n, f);
    fclose(f);
    f = fopen("z.dat", "rb");
    fread(&z[1], 8, n, f);
    fclose(f);
    f = fopen("xout.dat", "rb");
    fread(&out_x[1], 8, no, f);
    fclose(f);
    f = fopen("yout.dat", "rb");
    fread(&out_y[1], 8, no, f);
    fclose(f);
    f = fopen("zout.dat", "rb");
    fread(&out_z_e[1], 8, no, f);
    fclose(f);
    double a[26] = {NAN,
        226435.57402505854,
        1.4014365991638869,
        0.047336152694743491,
        -0.00057519863757688155,
        1.0100493150807506e-05,
        135.51662586608529,
        0.15102300347803294,
        -0.0044339477064652627,
        8.4438406022600178e-05,
        -6.2501613399611617e-07,
        -0.33597331914522238,
        0.0025299520519875872,
        0,
        0,
        0,
        -0.00049272339644965504,
        -6.4136593719532506e-05,
        0,
        0,
        0,
        -1.9178384104682761e-06,
        0,
        0,
        0,
        0};

    
    fit_wavelength(z, x, y, n, out_z, out_x, out_y, no, 25, a, 0);
    double rel = 0.015;
    is_close_rel(a[1], 226434.975, rel);
    is_close_rel(a[2], -7.758069, rel);
    is_close_rel(a[3], 0.41562, rel);
    is_close_rel(a[4], -0.006867, rel);
    is_close_rel(a[5], 4.909096e-5, rel);
    is_close_rel(a[6], 135.862, rel);
    is_close_rel(a[7], 0.14016, rel);
    is_close_rel(a[8], -0.00511, rel);
    is_close_rel(a[9], 0.0001154, rel);
    is_close_rel(a[10], -9.40352e-7, rel);
    is_close_rel(a[11], -0.30065, rel);
    is_close_rel(a[12], -0.000497, rel);
    is_close_abs(a[13], 0., 0.);
    is_close_abs(a[14], 0., 0.);
    is_close_abs(a[15], 0., 0.);
    is_close_rel(a[16], -0.0010958, rel);
    is_close_rel(a[17], -1.135340e-6, rel);
    is_close_abs(a[18], 0, 0);
    is_close_abs(a[19], 0, 0);
    is_close_abs(a[20], 0, 0);
    is_close_rel(a[21], 7.95606e-7, rel);
    is_close_abs(a[22], 0, 0);
    is_close_abs(a[23], 0, 0);
    is_close_abs(a[24], 0, 0);
    is_close_abs(a[25], 0, 0);

    for (i = 1; i < no + 1; i++) {
        is_close_rel(out_z[i], out_z_e[i], 0.01);
    }
}

int main(int argc, const char *argv[])
{
    test_fit_gauss();
    test_fit_wavelength();
    return 0;
}
