/*
  
  Otmar Stahl
  
  search_lines.c 

  search peaks in spectrum

.VERSION

 090728		last modif
*/


int search_lines 
#ifdef __STDC__
(
 float rval[], int linelist[], int npix[], int win, 
 int width, float thres
 )
#else
     (
      rval, linelist, npix, win, width, thres
      )
     float rval[], thres;
     int linelist[], npix[], win, width;
#endif

{
  int number = 0, j, i, imax;
  float diff, max_intens;

  for (j = win; j < npix[0] - win; j++)
    {
      diff = rval[j];
      if (diff > thres)
	{
	  imax = j;
	  max_intens = rval[j];
	  for (i = j - win; i <= j + win; i++)
	    if (rval[i] > max_intens)
	      {
		max_intens = rval[i];
		imax = i;
	      }
	  linelist[number] = imax;
	  number++;
	}
    }
  /*       remove doublets                                  */
  i = 0;
  while (i < number - 1)
    {
      if ((linelist[i + 1] - linelist[i]) < width)
	{
	  number--;
	  for (j = i + 1; j < number; j++)
	    {
	      linelist[j] = linelist[j + 1];
	    }
	  i--;	/* check again (more than two detections!) */
	}
      i++;
    }
  return number;
}


