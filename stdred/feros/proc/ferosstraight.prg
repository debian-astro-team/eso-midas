! @(#)ferosstraight.prg	19.1 (ESO-IPG) 02/25/03 14:22:03
! @(#)ferosstraight.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferosstraight.prg	1.0 (ESO-IPG) 14-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferosstraight.prg
!.AUTHOR      Anton Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Convert echelle spectrum to an easy to handle format
!.VERSION     1.0    Creation    14-Nov-1997
!
!-------------------------------------------------------
!
define/param p1 {FLAT_IMG} I "Input image: "
define/param p2 {STRAIGHT_IMG} I "Output image: "
define/param p3 {CENTER_TBL} T "Input table: "
define/param p4 {PROFILE_W},{FIBER_OFF1},{FIBER_OFF2},{FIBER_MODE} C "Width of profile, offset 1, offset 2, mode (1|2 for 1|2 fiber): "

write/key in_a {p1}
write/key out_a {p2}
write/key in_b {p3}
write/key inputi {p4}

run STD_EXE:echstraight.exe
