! @(#)feroscalibrate.prg	19.1 (ESO-IPG) 02/25/03 14:22:01
! @(#)feroscalibrate.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)feroscalibrate.prg	1.0 (ESO-IPG) 14-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       feroscalibrate.prg
!.AUTHOR      Anton Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Do wavelength calbration on spectrum
!.VERSION     1.0    Creation    14-Nov-1997
!
!-------------------------------------------------------
!
set/format I4 F5.2,F5.2
define/param P1 {LINE_POS_TBL} T "line table: "
define/param P2 {LINE_REF_TBL} T "line reference catalog: "
define/param P3 {ID_ALPHA},{ID_TOL},{ID_THRES},{CCD_ROT},{CCD_SHIFT},{SPECTR_G},{SPECTR_F},{SPECTR_THB} N "alpha, tol, thres, ccd rotation, ccd shift, g, f, thetaB"
define/param p4 {COEF_WLC} T "dispersion relation coefficient table: "
define/param p5 {CENTER_TBL} T "center table: "
define/param p6 {INIT_WLC} C "get coefficients (G|C|W): "

! spectral line identification and wavelength calibration by the method
! of Andreas Kaufer
! More than 15 lines must be identified, as we have
! 15 independent coefficients to be found by non-linear fitting
! with the Levenberg Marquardt method

WRITE/KEYW IN_B/C/1/80  {P5}
WRITE/KEYW IN_C/C/1/80  {P2}
WRITE/KEYW INPUTR/R/1/8 {P3}
WRITE/KEYW INPUTC/C/1/1 {P6}

IF {FIBER_MODE} .EQ. 1 THEN                     ! one fiber
  WRITE/KEYW IN_A/C/1/80 {P1}
  WRITE/KEYW IN_D/C/1/80 {P4}

  COMPUTE/TABLE {P1} :xabs = :X
  COMPUTE/TABLE {P1} :mabs = {ORDER_LAST}-:Y+1

  IF "{P6}" .eq. "W" THEN
    @@ STD_PROC:ferosiden {in_a}
  ENDIF

  RUN STD_EXE:ypos.exe
  RUN STD_EXE:echcalibrate.exe
ELSE                                            ! two fibers
  ! first fiber
  WRITE/KEYW IN_A/C/1/80 {P1}1
  WRITE/KEYW IN_D/C/1/80 {P4}1

  COMPUTE/TABLE {P1}1 :xabs = :X
  COMPUTE/TABLE {P1}1 :mabs = {ORDER_LAST}-:Y+1

  IF "{P6}" .eq. "W" THEN
    @@ STD_PROC:ferosiden {in_a}
  ENDIF

  RUN STD_EXE:ypos.exe
  RUN STD_EXE:echcalibrate.exe

  ! second fiber
  WRITE/KEYW IN_A/C/1/80 {P1}2
  WRITE/KEYW IN_D/C/1/80 {P4}2

  COMPUTE/TABLE {P1}2 :xabs = :X
  COMPUTE/TABLE {P1}2 :mabs = {ORDER_LAST}-:Y+1

  IF "{P6}" .eq. "W" THEN
    @@ STD_PROC:ferosiden {in_a}
  ENDIF

  RUN STD_EXE:ypos.exe
  RUN STD_EXE:echcalibrate.exe
ENDIF
