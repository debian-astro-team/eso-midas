! @(#)ferosmerge1.prg	19.1 (ESO-IPG) 02/25/03 14:22:02
! @(#)ferosmerge1.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)necmerg.prg	12.1.1.1 (ES0-DMD) 09/16/97 11:17:56
! +++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  ECHELLE PROCEDURE : ECHMERG.PRG
!  J.D.Ponz			version 1.0 131083
!
! .PURPOSE
!
!  execute the command :
!  MERGE/ECHELLE input output [delta]     AVERAGE
!  OR
!  MERGE/ECHELLE input output [ord1,ord2] NOAPPEND
!
! ----------------------------------------
!
DEFINE/PARAM P1 ?         IMAGE         "Enter input image:"
DEFINE/PARAM P2 ?         IMAGE         "Enter output image:"
DEFINE/PARAM P4 {MERGE_MTD}    C        "Method"
IF P4(1:1) .EQ. "A"  DEFINE/PARAM P3 {MERGE_DELTA}   NUMBER   ! Overlapping param.
IF P4(1:1) .EQ. "N"  DEFINE/PARAM P3 {MERGE_ORD(1)},{MERGE_ORD(2)} NUMBER
!
!VERIFY/ECHELLE {P1}  REBIN
!
DEFINE/LOCAL   METHOD/C/1/8  "YYYYYYYY"
IF P4(1:1) .EQ. "N"  WRITE/KEYW  METHOD/C/1/8 "NOAPPEND"
IF P4(1:1) .EQ. "A"  WRITE/KEYW  METHOD/C/1/8 "AVERAGE"
!IF P4(1:1) .EQ. "C"  WRITE/KEYW  METHOD/C/1/8 "CONCATEN"
IF P4(1:1) .EQ. "S"  WRITE/KEYW  METHOD/C/1/8 "SINC"
IF METHOD(1:1) .EQ. "Y" THEN
   WRITE/OUT "MERGE1/FEROS: unknown method {P4}"
   RETURN/EXIT
ENDIF
WRITE/KEYW  MERGE_MTD/C/1/12  {METHOD}
IF MERGE_MTD(1:1) .EQ. "A"   MERGE_DELTA = {P3}
IF MERGE_MTD(1:1) .EQ. "N"   WRITE/KEYW   MERGE_ORD/I/1/2  {P3}
!
IF P3(1:1) .NE. "?" THEN
   WRITE/KEYW INPUTR/R/1/2   {P3}
ELSE
   WRITE/KEYW INPUTR/R/1/2   0.  ALL
ENDIF
!
RUN STD_EXE:ECHMERG1
!
RETURN
