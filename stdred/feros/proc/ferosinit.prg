! @(#)ferosinit.prg	19.1 (ESO-IPG) 02/25/03 14:22:02
! @(#)ferosinit.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferosinit.prg	1.0 (ESO-IPG) 13-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferosinit.prg
!.AUTHOR      Anton Malina, LSW Heidelberg, IPG/ESO
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Command INIT/FEROS [NAME]
!.VERSION     1.0    Creation	13-Nov-1997   
!
!-------------------------------------------------------
!
IF "{P1(1:1)}" .EQ. "?" THEN                        ! write default keywords
   WRITE/OUT " "
   WRITE/OUT "Initializing keywords"
   WRITE/OUT " "
   REGISTER/SESSION FEROS STD_PROC ferosinit.prg ferostab.tbl
!Begin Session List
   WRITE/KEYWORD  RAW_IMG/C/1/80       flat2d		! name of raw image
   WRITE/KEYWORD  WLC_IMG/C/1/80       wlc2d		! name of wlc image
   WRITE/KEYWORD  GUESS_TBL/C/1/80     echpos		! name of guess table
   WRITE/KEYWORD  CENTER_TBL/C/1/80    centers	        ! name of order position table
   WRITE/KEYWORD  LOC_CUTSTEP/I/1/1    10		! cut step for order localization
   WRITE/KEYWORD  LOC_WINDOW/I/1/1     17		! window size for order localization
   WRITE/KEYWORD  FIT_DEG/I/1/1        5		! order polynomial fit degree
   WRITE/KEYWORD  LOC_THRES/R/1/1      0.9		! background level in order localization
   WRITE/KEYWORD  LOC_MODE/C/1/1       G		! localization mode; valid values: S|G
   WRITE/KEYWORD  LOC_METHOD/C/1/1     R                ! localization method; valid values: R|A
   WRITE/KEYWORD  CUTS_IMG/C/1/80      cuts		! cuts image
   WRITE/KEYWORD  FOLD_IMG/C/1/80      fold		! fold image
   WRITE/KEYWORD  TEMPL_IMG/C/1/80     template	        ! input template image
   WRITE/KEYWORD  TEMPLT_IMG/C/1/80    templatet	! output template image
   WRITE/KEYWORD  FIT_IMG/C/1/80       fitted_img       ! root name if fitted profiles image
   WRITE/KEYWORD  MASK_IMG/C/1/80      mask_img         ! root name of masked pixels image
   WRITE/KEYWORD  INIT_TBL/C/1/80      init		! init table
   WRITE/KEYWORD  FLAT_IMG/C/1/80      nobg2d		! name of flatfielded image
   WRITE/KEYWORD  FLATEXT_IMG/C/1/80   flatxtrctd       ! name of extracted flatfield image
   WRITE/KEYWORD  UNBLAZED_IMG/C/1/80  unblazed	        ! name of unblazed image
   WRITE/KEYWORD  BG_MODE/C/1/1        B	 	! background determination mode; valid values: B|N|S
   WRITE/KEYWORD  BG_STEPX/I/1/1       51		! step in x for background determination
   WRITE/KEYWORD  BG_STEPY/I/1/1       51		! step in y for background determination
   WRITE/KEYWORD  BG_WIDTHX/I/1/1      21		! width in x for background determination
   WRITE/KEYWORD  BG_WIDTHY/I/1/1      45		! width in y for background determination
   WRITE/KEYWORD  BG_MEDIANX/I/1/1     4		! median filter size in x
   WRITE/KEYWORD  BG_MEDIANY/I/1/1     11		! median filter size in y
   WRITE/KEYWORD  BG_DIST/I/1/1        50		! minimum distance between orders
   WRITE/KEYWORD  STRAIGHT_IMG/C/1/80  straightened	! root name of straightened image
   WRITE/KEYWORD  PROFILE_W/I/1/1      15		! width of spatial profile
   WRITE/KEYWORD  FIBER_OFF1/I/1/1     -9		! shift of order center against value in CENTER_TBL
   WRITE/KEYWORD  FIBER_OFF2/I/1/1     9		! shift of order center against value in CENTER_TBL (second fiber)
   WRITE/KEYWORD  FIBER_MODE/I/1/1     2		! number of fibers
   WRITE/KEYWORD  IMG_WRITE/C/1/1      N                ! write additional images
   WRITE/KEYWORD  EXT_IMG/C/1/80       extracted 	! root name of extracted image
   WRITE/KEYWORD  SPECTR_TYPE/C/1/1    G		! spectrograph type; valid values: F|G
   WRITE/KEYWORD  EXT_MODE/C/1/1       S		! extraction mode; valid values: S|O|M
   WRITE/KEYWORD  PROFILE_GET/C/1/1    N		! get spatial profile from spectrum; valis values: Y|N
   WRITE/KEYWORD  EXT_ITER/I/1/1       3		! maximum iterations in optimum extraction
   WRITE/KEYWORD  CCD_RON/R/1/1        3.5		! readout noise of CCD (optimum extraction)
   WRITE/KEYWORD  CCD_GAIN/R/1/1       0.66		! gain of CCD (optimum extraction)
   WRITE/KEYWORD  CCD_THRES/R/1/1      4.0		! clipping threshold (optimum extraction)
   WRITE/KEYWORD  CCD_ROT/R/1/1        2.4		! rotation of CCD
   WRITE/KEYWORD  CCD_SHIFT/R/1/1      0.0		! shift of CCD in x direction
   WRITE/KEYWORD  COEF_COP/C/1/80      cop_coeffs	! root name of cross order profile coefficient table
   WRITE/KEYWORD  COEF_WLC/C/1/80      wlc_coeffs	! root name of wavelength calibration coefficient table
   WRITE/KEYWORD  INIT_WLC/C/1/1       C		! get dispersion coefficients from spectrum; valid values: G|C|W
   WRITE/KEYWORD  ORDER_FIRST/I/1/1    25		! first order on CCD
   WRITE/KEYWORD  ORDER_LAST/I/1/1     63		! last order on CCD
   WRITE/KEYWORD  LINE_W/I/1/1         5		! width of spectral line
   WRITE/KEYWORD  LINE_THRES/R/1/1     10000		! background level
   WRITE/KEYWORD  LINE_POS_TBL/C/1/80  found_lines	! root name of found line table
   WRITE/KEYWORD  LINE_MTD/C/1/80      gauss		! line search method; valid values: GRAVITY|MAXIMUM|MINIMUM|GAUSSIAN
   WRITE/KEYWORD  LINE_TYPE/C/1/80     emission	        ! line type; valid values: EMISSION|ABSORPTION
   WRITE/KEYWORD  LINE_REF_TBL/C/1/80  ThAr50000	! name of laboratory wavelength table
   WRITE/KEYWORD  ID_ALPHA/R/1/1       0.2		! alpha value for line identification
   WRITE/KEYWORD  ID_TOL/R/1/1         0.2		! tolerance value for line identification
   WRITE/KEYWORD  ID_THRES/R/1/1       0.0		! intensity threshold for first step of line identification
   WRITE/KEYWORD  SPECTR_G/R/1/1       79.0		! grating constant
   WRITE/KEYWORD  SPECTR_F/R/1/1       410.0		! focal length of camera
   WRITE/KEYWORD  SPECTR_THB/R/1/1     63.4		! blaze angle of spectrograph
   WRITE/KEYWORD  REBIN_IMG/C/1/80     rebinned   	! root name of rebinned spectrum
   WRITE/KEYWORD  REBIN_SCL/C/1/1      I		! scaling of rebinned spectrum (lInear or lOgarithmic); valid values: I|O
   WRITE/KEYWORD  REBIN_STEP/R/1/1     0.03		! wavelength step of rebinned spectrum
   WRITE/KEYWORD  REBIN_MTD/C/1/1      S		! rebinning method; valid values: L|Q|S
   WRITE/KEYWORD  MERGE_IMG/C/1/80     merged		! root name of merged spectrum
! merge_opt no longer used!
   WRITE/KEYWORD  MERGE_OPT/C/1/3      YES		! Optional merge
   WRITE/KEYWORD  MERGE_MTD/C/1/12     SINC	        ! Merging methods: NOAPPEND,AVERAGE,SINC (Associated command MERGE/FEROS) 
   WRITE/KEYWORD  MERGE_DELTA/R/1/1    1.0		! Wav. interval to be skipped (MERGE_MTD=AVERAGE) 
   WRITE/KEYWORD  MERGE_ORD/I/1/2      0,0              ! Ord1,Ord2 (MERGE_MTD=NOAPP). 0,0:all orders 
!End Session List
ELSE                                                    ! restore saved session
   SAVINIT/FEROS  {P1}_INIT.tbl READ
   COPY/TABLE {P1}_ORDER.tbl {CENTER_TBL} 
   IF "{LOC_MODE}" .EQ. "G" THEN
     COPY/TABLE {P1}_GORDER.tbl {GUESS_TBL}
   ENDIF
!   COPY/TABLE {P1}_{LINE_REF_TBL}.tbl {LINE_REF_TBL}
   COPY/II    {P1}_TEMPLATE.bdf {TEMPL_IMG}
!   COPY/II    {P1}_{TEMPLT_IMG}.bdf {TEMPLT_IMG}

   IF "{EXT_MODE}" .NE. "S" THEN
     IF FIBER_MODE .EQ. 1 THEN
       COPY/TABLE {P1}_COP.tbl {COEF_COP}
     ELSE
       COPY/TABLE {P1}_COP1.tbl {COEF_COP}1
       COPY/TABLE {P1}_COP2.tbl {COEF_COP}2
     ENDIF
   ENDIF

   IF FIBER_MODE .EQ. 1 THEN
     COPY/TABLE {P1}_WLC.tbl {COEF_WLC}
     COPY/TABLE {P1}_LINE.tbl {LINE_POS_TBL}
   ELSE
     COPY/TABLE {P1}_WLC1.tbl {COEF_WLC}1
     COPY/TABLE {P1}_WLC2.tbl {COEF_WLC}2
     COPY/TABLE {P1}_LINE1.tbl {LINE_POS_TBL}1
     COPY/TABLE {P1}_LINE2.tbl {LINE_POS_TBL}2
   ENDIF

ENDIF
