! @(#)ferosfind.prg	19.1 (ESO-IPG) 02/25/03 14:22:02
! @(#)ferosfind.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferosfind.prg	1.0 (ESO-IPG) 14-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferosfind.prg
!.AUTHOR      Anton Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Find lines in extracted spectrum
!.VERSION     1.0    Creation    14-Nov-1997
!
!-------------------------------------------------------
!
define/param p1 {UNBLAZED_IMG}
define/param p2 {LINE_W},{LINE_THRES},1
define/param p3 {LINE_POS_TBL}
define/param p4 {LINE_MTD}
define/param p5 {LINE_TYPE}
! uses SEARCH/LINE from context spec
IF {FIBER_MODE} .EQ. 1 THEN         ! one fiber
  SEARCH/LINE {P1} {p2} {p3} {p4} {p5}
ELSE                                ! two fibers
  SEARCH/LINE {p1}1 {p2} {p3}1 {p4} {p5}
  SEARCH/LINE {p1}2 {p2} {p3}2 {p4} {p5}
ENDIF
