! @(#)ferosrebin1d.prg	19.1 (ESO-IPG) 02/25/03 14:22:02
! @(#)ferosrebin1d.prg	14.1.1.1 (ESO-IPG) 09/16/99 09:47:07
! @(#)ferosrebin1d.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferosrebin1d.prg	1.0 (ESO-IPG) 14-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferosrebin1d.prg
!.AUTHOR      Anton Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     rebin extracted spectrum to constant steps in lambda or log lambda
!.VERSION     1.0    Creation    14-Nov-1997
!
!-------------------------------------------------------
!
define/param P1 {UNBLAZED_IMG} I "input frame: "
define/param P2 {LINE_POS_TBL} T "input table: "
define/param P3 {REBIN_IMG} I "output frame: "
define/param P4 {REBIN_SCL} C "scaling (lInear / lOgarithmic): "
define/param P5 {REBIN_STEP} N "Step size in lambda or log lambda: "
define/param P6 {REBIN_MTD} C "Method L(inear) Q(uadratic) or S(pline): "
define/param P7   OS        C  "Fiber mode: OS or OC: "

WRITE/KEYWORD INPUTC/C/1/1 {P4}
WRITE/KEYWORD INPUTD/D/1/2 0,0
WRITE/KEYWORD INPUTD/D/1/2 {P5}

IF "{P6(1:1)}" .EQ. "L" THEN
  WRITE/KEY INPUTI/I/1/1 0
ElSE IF "{P6(1:1)}" .EQ. "Q" THEN
  WRITE/KEY INPUTI/I/1/1 1
ELSE IF "{P6(1:1)}" .EQ. "S" THEN
  WRITE/KEY INPUTI/I/1/1 2
ELSE
  WRITE/OUT "YOU HAVE NOT SELECTED A CORRECT METHOD!"
ENDIF

IF {FIBER_MODE} .EQ. 1 THEN       ! one fiber
  WRITE/KEYWORD IN_A/C/1/80 {P1}
  WRITE/KEYWORD IN_B/C/1/80 {P2}
  WRITE/KEYWORD OUT_A/C/1/80 {P3}

  RUN STD_EXE:echrebin1d

ELSE                              ! two fibers
  ! first fiber
  WRITE/KEYWORD IN_A/C/1/80 {P1}1
  WRITE/KEYWORD IN_B/C/1/80 {P2}1
  WRITE/KEYWORD OUT_A/C/1/80 {P3}1

  RUN STD_EXE:echrebin1d

! second fiber

  IF "{p7}" .EQ. "OC" THEN
    WRITE/OUT " "
    WRITE/OUT "Setting barycentric correction to zero for calibration"
    WRITE/OUT " "
    INPUTD(2) = 0.0
  ENDIF

  WRITE/KEYWORD IN_A/C/1/80 {P1}2
  WRITE/KEYWORD IN_B/C/1/80 {P2}2
  WRITE/KEYWORD OUT_A/C/1/80 {P3}2

  RUN STD_EXE:echrebin1d

ENDIF
