! @(#)ferosmerge.prg	19.1 (ESO-IPG) 02/25/03 14:22:02
! @(#)ferosmerge.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferosmerge.prg	1.0 (ESO-IPG) 14-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferosmerge.prg
!.AUTHOR      Anton Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Merge rebinned order by order spectrum to 1D-spectrum
!.VERSION     1.0    Creation    14-Nov-1997
!
!-------------------------------------------------------
!
!uses MERGE/ECHELLE from context echelle
!SET/ECHELLE MGOPT={MERGE_OPT}

define/param p1 {REBIN_IMG} I
define/param p2 {MERGE_IMG} I
define/param p4 {MERGE_MTD}

IF "{p4(1:1)}" .EQ. "A" THEN
  define/param p3 {MERGE_DELTA} ?
ELSEIF "{p4(1:1)}" .EQ. "S" THEN
  define/param p3 {MERGE_DELTA} ?
ELSEIF "{p4(1:1)}" .EQ. "N" THEN
  define/param p3 {MERGE_ORD(1)},{MERGE_ORD(2)} ?
ENDIF
IF {FIBER_MODE} .EQ. 1 THEN              ! one fiber
  @s ferosmerge1 {p1} {p2} {p3} {p4}
ELSE                                     ! two fibers
  @s ferosmerge1 {p1}1 {p2}1 {p3} {p4}
  @s ferosmerge1 {p1}2 {p2}2 {p3} {p4}
ENDIF

