! @(#)feroslocate.prg	19.1 (ESO-IPG) 02/25/03 14:22:02
! @(#)feroslocate.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)feroslocate.prg	1.0 (ESO-IPG) 14-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       feroslocate.prg
!.AUTHOR      Anton Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Locate orders in echelle spectrum
!.VERSION     1.0    Creation    14-Nov-1997
!
!-------------------------------------------------------
!
define/param p1 {RAW_IMG} I "Input image: "
define/param p2 {GUESS_TBL} T "Input table: "
define/param p3 {CENTER_TBL} I "Output table: "
define/param p4 {LOC_CUTSTEP},{LOC_WINDOW},{FIT_DEG} N "Cut step, window, fit degree: "
define/param p5 {LOC_THRES} N "Treshold: "
define/param p6 {LOC_MODE}{LOC_METHOD} C "Order localization mode and method: "

write/key in_a {p1}
write/key in_b {p2}
write/key out_b {p3}
write/key inputi {p4}
write/key inputr {p5}
write/key inputc {p6}

run STD_EXE:echloc.exe
