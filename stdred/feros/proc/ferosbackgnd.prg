! @(#)ferosbackgnd.prg	19.1 (ESO-IPG) 02/25/03 14:22:01
! @(#)ferosbackgnd.prg	19.1 (ESO-IPG) 02/25/03 14:22:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferosbackgnd.prg
!.AUTHOR      Anton Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Detremine and subtract background from echelle spectrum
!.VERSION     1.0    Creation    14-Nov-1997
!
!-------------------------------------------------------
!
define/param p1 {RAW_IMG} I "Input image: "
define/param p2 {FLAT_IMG} I "Output image: "
define/param p3 {CENTER_TBL} T "Input table: "
define/param p4 {BG_MODE} C "Mode: "
define/param p5 {BG_STEPX},{BG_STEPY},{BG_WIDTHX},{BG_WIDTHY},{BG_DIST} N "step1, step2, width1, width2, minimum distance: "
define/param p6 {BG_MEDIANX},{BG_MEDIANY} N "x and y size of median filter: "
define/param p7 {FIBER_MODE} I "Fiber mode: "

write/key in_a {p1}
!write/keyword RAW_IMG/C/1/80 {p1}
write/key out_a {p2}
write/key in_b {p3}
write/key inputc {p4}
write/key inputi {p5}
define/local xysize/i/1/2 {p6}
define/local fibmode/i/1/2 {p7}

if xysize(2) .LT. inputi(2) then
  run STD_EXE:echbackground.exe
else
  WRITE/OUT "*** ERROR ***  Median window must be smaller then step size"
endif
