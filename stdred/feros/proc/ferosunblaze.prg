! @(#)ferosunblaze.prg	19.1 (ESO-IPG) 02/25/03 14:22:03
! @(#)ferosunblaze.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferosunblaze.prg	1.0 (ESO-IPG) 14-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferosunblaze.prg
!.AUTHOR      Anton Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Remove blaze function
!.VERSION     1.0    Creation    14-Nov-1997
!
!-------------------------------------------------------
!
define/param p1 {EXT_IMG}
define/param p2 {FLATEXT_IMG}
define/param p3 {UNBLAZED_IMG}


! uses SEARCH/LINE from context spec
IF {FIBER_MODE} .EQ. 1 THEN         ! one fiber
  COMPUTE/IMA {p3} = {p1} / {p2}
ELSE                                ! two fibers
  COMPUTE/IMA {p3}1 = {p1}1 / {p2}1
  COMPUTE/IMA {p3}2 = {p1}2 / {p2}2	
ENDIF
