! @(#)ferosextract.prg	19.1 (ESO-IPG) 02/25/03 14:22:01
! @(#)ferosextract.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferosextract.prg	1.0 (ESO-IPG) 14-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferosextract.prg
!.AUTHOR      Anton Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Extract spectrum
!.VERSION     1.0    Creation    14-Nov-1997
!
!-------------------------------------------------------
!
define/param p1 {STRAIGHT_IMG} I "straightened input spectrum: "
define/param p2 {EXT_IMG} I "Output image: "
define/param p3 {SPECTR_TYPE}{EXT_MODE}{PROFILE_GET}{IMG_WRITE} C "Spectrograph type, extraction mode, determine profile, write images (F|G,S|O,Y|N,Y|N): "
define/param p4 {FIT_DEG},{EXT_ITER} ? "fit degree, number of iterations: "
define/param p5 {CCD_RON},{CCD_GAIN},{CCD_THRES} N "readout noise, gain, threshold: "
define/param p6 {COEF_COP} T "coefficient output file: "

write/key in_c/C/1/80 {p1}
write/key out_a {p2}
write/key inputc {p3}
write/key inputi {p4}
write/key ccd_par/R/1/80 {p5}
write/key out_c/C/1/80 {p6}
write/key out_D/C/1/80 {FIT_IMG}
write/key out_E/C/1/80 {MASK_IMG}

run STD_EXE:echext.exe
