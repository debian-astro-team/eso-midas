! @(#)ferosxcorr.prg	19.1 (ESO-IPG) 02/25/03 14:22:03
! @(#)ferosxcorr.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
!
! This is part of the FEROS data reduction software
! convert IDTAB by cross-correlating with reference frame and 
! converting :x-axis of reference IDTAB
! 
! Otmar Stahl, Nov. 9, 1998
!
!
define/param P1 ? I "line by line spectrum: "
define/param P2 20 N "order to plot: "

define/local i/i/1/1 0

!goto next

cop/tab IDTAB_REF IDTAB

crea/tab xcorr 2 39
crea/col xcorr :row
crea/col xcorr :xcorr

! loop over orders

do i = 1 39 

  EXTRAC/IMAG xx1 = THAR_REF[<,@{i}:>,@{i}]
  EXTRAC/IMAG xx2 = {p1}[<,@{i}:>,@{i}]

  XCORRE/IMAG xx2 xx1 xcorr 30
!plo xcorr

  cente/gauss xcorr

  xcorr,:row,@{i} = {i}
  xcorr,:xcorr,@{i} = {outputr(5)}

  enddo

!next:

REGRES/POLY xcorr :xcorr :row 1
COMPUT/TABL IDTAB :x = :x+({outputd(1)}+{outputd(2)}*:y)

plo {p1} @{p2}
sele/tab  IDTAB :y.eq.{p2}

!OVERPL/IDEN IDTAB :x :ident
set/gra colo=2
OVERPL/IDEN  IDTAB :x :ident
set/gra colo=1
!
