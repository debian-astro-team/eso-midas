! @(#)ferossave.prg	19.1 (ESO-IPG) 02/25/03 14:22:03
! @(#)ferossave.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferossave.prg	1.0 (ESO-IPG) 13-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferossave.prg
!.AUTHOR      Anton Malina, LSW Heidelberg, IPG/ESO
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Command SAVE/FEROS
!.VERSION     1.0    Creation	13-Nov-1997   
!
!-------------------------------------------------------

DEFINE/PARAM P1 ? C "Session name :"

WRITE/OUT Process tables are saved

SET/MIDAS OUTPUT=NO
SELECT/TABLE  {CENTER_TBL}    ALL
COPY/TABLE    {CENTER_TBL}    {P1}_ORDER.tbl
!SELECT/TABLE  {LINE_REF_TBL}  ALL
!COPY/TABLE    {LINE_REF_TBL}  {P1}_{LINE_REF_TBL}.tbl
COPY/II	      {TEMPL_IMG}     {P1}_TEMPLATE.bdf
!COPY/II	      {TEMPLT_IMG}    {P1}_{TEMPLT_IMG}.bdf

IF "{EXT_MODE}" .NE. "S" THEN
  IF FIBER_MODE .EQ. 1 THEN
    SELECT/TABLE  {COEF_COP}    ALL
    COPY/TABLE    {COEF_COP}    {P1}_COP.tbl
  ELSE
    SELECT/TABLE  {COEF_COP}1   ALL
    COPY/TABLE    {COEF_COP}1   {P1}_COP1.tbl
    SELECT/TABLE  {COEF_COP}2   ALL
    COPY/TABLE    {COEF_COP}2   {P1}_COP2.tbl
  ENDIF
ENDIF

IF FIBER_MODE .EQ. 1 THEN
  SELECT/TABLE  {COEF_WLC}    ALL
  COPY/TABLE    {COEF_WLC}    {P1}_WLC.tbl

  SELECT/TABLE  {LINE_POS_TBL}    ALL
  COPY/TABLE    {LINE_POS_TBL}    {P1}_LINE.tbl  
ELSE
  SELECT/TABLE  {COEF_WLC}1   ALL
  COPY/TABLE    {COEF_WLC}1   {P1}_WLC1.tbl 
  SELECT/TABLE  {COEF_WLC}2   ALL
  COPY/TABLE    {COEF_WLC}2   {P1}_WLC2.tbl

  SELECT/TABLE  {LINE_POS_TBL}1   ALL
  COPY/TABLE    {LINE_POS_TBL}1   {P1}_LINE1.tbl 
  SELECT/TABLE  {LINE_POS_TBL}2   ALL
  COPY/TABLE    {LINE_POS_TBL}2   {P1}_LINE2.tbl
ENDIF

SELECT/TABLE    ferostab.tbl      ALL
COPY/TABLE      ferostab.tbl      {P1}_INIT.tbl

IF LOC_MODE .EQ. "G" THEN
  SELECT/TABLE  {GUESS_TBL}     ALL
  COPY/TABLE    {GUESS_TBL}     {P1}_GORDER.tbl
ENDIF
SET/MIDAS OUTPUT=YES

WRITE/KEYWORD   INIT_TBL      {P1}_INIT
SAVINIT/FEROS   {P1}_INIT.tbl  WRITE

RETURN
