! @(#)ferosshow.prg	19.1 (ESO-IPG) 02/25/03 14:22:03
! @(#)ferosshow.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferosshow.prg	1.0 (ESO-IPG)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1997 European Southern Observatory
!.IDENT       ferosshow.prg
!.AUTHOR      Anton Malina, LSW Heidelberg, IPG/ESO
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Display echelle session parameters
!.VERSION     1.0    Creation    13-Nov-1997
!
!-------------------------------------------------------
!
DEFINE/PARAM P1 ALL CHAR "Selection (GENE, LOCA, BACK, STRA, EXTR, IDENT, REBI, ALL): "
SESSLINE = 0 ! Initialize the line counter
!
!ECHO/OFF
SET/FORMAT I1 F12.3,F12.3
!
SHOW/SESS "-----------=========*******#### FEROS Parameters ####*******=========----------"
IF P1(1:1) .EQ. "G"  GOTO GENERAL
IF P1(1:1) .EQ. "L"  GOTO DEFINE
IF P1(1:1) .EQ. "B"  GOTO BACKGROUND
IF P1(1:1) .EQ. "S"  GOTO RECTIFY
IF P1(1:1) .EQ. "E"  GOTO EXTRACT
IF P1(1:1) .EQ. "I"  GOTO IDENTIFY
IF P1(1:1) .EQ. "R"  GOTO REBIN

GENERAL:
SHOW/SESS "General parameters:"
SHOW/SESS " "
SHOW/SESS "name of input image (RAW_IMG):                                     {RAW_IMG}"
SHOW/SESS "name of input image (WLC_IMG):                                     {WLC_IMG}"
SHOW/SESS "name of guess table for profile centers (GUESS_TBL):               {GUESS_TBL}"
SHOW/SESS "name of table of profile centers (CENTER_TBL):                     {CENTER_TBL}"
SHOW/SESS "root name of extracted flatfield image (FLATEXT_IMG):              {FLATEXT_IMG}"
SHOW/SESS "order localization mode (LOC_MODE):                                {LOC_MODE}"
SHOW/SESS "name of flatfielded image (FLAT_IMG):                              {FLAT_IMG}"
SHOW/SESS "background determination mode (BG_MODE):                           {BG_MODE}"
SHOW/SESS "root name of straightened image (STRAIGHT_IMG):                    {STRAIGHT_IMG}"
SHOW/SESS "fiber mode (FIBER_MODE):                                           {FIBER_MODE}"
SHOW/SESS "root name of extracted image (EXT_IMG):                            {EXT_IMG}"
SHOW/SESS "spectrograph type (SPECTR_TYPE):                                   {SPECTR_TYPE}"
SHOW/SESS "extraction mode (EXT_MODE):                                        {EXT_MODE}"
SHOW/SESS "number of iterations in optimum extraction (EXT_ITER):             {EXT_ITER}"
SHOW/SESS "spatial profile determination mode (PROFILE_GET):                  {PROFILE_GET}"
SHOW/SESS "root name of cross order profile table (COEF_COP):                 {COEF_COP}" 
SHOW/SESS "CCD readout noise (CCD_RON):                                       {CCD_RON}"
SHOW/SESS "CCD gain (CCD_GAIN):                                               {CCD_GAIN}"
SHOW/SESS "CCD threshold (CCD_THRES)                                          {CCD_THRES}"
SHOW/SESS "polynomial fit degree (FIT_DEG):                                   {FIT_DEG}"
SHOW/SESS "first and last order (ORDER_FIRST, ORDER_LAST):                    {ORDER_FIRST},{ORDER_LAST}"
SHOW/SESS " "
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS "-------------------------------------------------------------------------------"

DEFINE:
SHOW/SESS "Order localization related: DEFINE/FEROS"
SHOW/SESS "   "
SHOW/SESS "name of input image (RAW_IMG):                                     {RAW_IMG}"
SHOW/SESS "name of guess table for profile centers (GUESS_TBL):               {GUESS_TBL}"
SHOW/SESS "name of table of profile centers (CENTER_TBL):                     {CENTER_TBL}"
SHOW/SESS "cut step (LOC_CUTSTEP):                                            {LOC_CUTSTEP}"
SHOW/SESS "search window width (LOC_WINDOW):                                  {LOC_WINDOW}"
SHOW/SESS "polynomial fit degree (FIT_DEG):                                   {FIT_DEG}"
SHOW/SESS "threshold (LOC_THRES):                                             {LOC_THRES}"
SHOW/SESS "order localization mode (LOC_MODE):                                {LOC_MODE}"
SHOW/SESS "order localization method (LOC_METHOD):                            {LOC_METHOD}"
SHOW/SESS " "
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS "-------------------------------------------------------------------------------"

BACKGROUND:
SHOW/SESS "Background determination related: BACKGR/FEROS" 
SHOW/SESS "   "
SHOW/SESS "name of input image (RAW_IMG):                                     {RAW_IMG}"
SHOW/SESS "name of flatfielded image (FLAT_IMG):                              {FLAT_IMG}"
SHOW/SESS "name of table of profile centers (CENTER_TBL):                     {CENTER_TBL}"
SHOW/SESS "background determination mode (BG_MODE):                           {BG_MODE}"
SHOW/SESS "minimum distance between orders (BG_DIST)                          {BG_DIST}"
SHOW/SESS "step size in x and y (BG_STEPX, BG_STEPY):                         {BG_STEPX},{BG_STEPY}"
SHOW/SESS "width in x and y (BG_WIDTHX, BG_WIDTHY):                           {BG_WIDTHX},{BG_WIDTHY}"
SHOW/SESS "median filter size in x and y (BG_MEDIANX, BG_MEDIANY):            {BG_MEDIANX},{BG_MEDIANY}"
SHOW/SESS "polynomial fit degree (FIT_DEG):                                   {FIT_DEG}"
SHOW/SESS " "
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS "-------------------------------------------------------------------------------"

RECTIFY:
SHOW/SESS "Order straightening related: RECTIFY/FEROS"
SHOW/SESS "   "
SHOW/SESS "name of flatfielded image (FLAT_IMG):                              {FLAT_IMG}"
SHOW/SESS "name of table of profile centers (CENTER_TBL):                     {CENTER_TBL}"
SHOW/SESS "root name of straightened image (STRAIGHT_IMG):                    {STRAIGHT_IMG}"
SHOW/SESS "spatial profile width (PROFILE_W):                                 {PROFILE_W}"
SHOW/SESS "displacement of the fibers from center (FIBER_OFF1, FIBER_OFF2):   {FIBER_OFF1},{FIBER_OFF2}"
SHOW/SESS "fiber mode (FIBER_MODE):                                           {FIBER_MODE}"
SHOW/SESS " "
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS "-------------------------------------------------------------------------------"

EXTRACT:
SHOW/SESS "Extraction related: EXTRACT/FEROS" 
SHOW/SESS "   "
SHOW/SESS "name of flatfielded image (FLAT_IMG):                              {FLAT_IMG}"
SHOW/SESS "root name of extracted image (EXT_IMG) :                           {EXT_IMG}"
SHOW/SESS "name of table of profile centers (CENTER_TBL):                     {CENTER_TBL}"
SHOW/SESS "spectrograph type (SPECTR_TYPE):                                   {SPECTR_TYPE}"
SHOW/SESS "extraction mode (EXT_MODE):                                        {EXT_MODE}"
SHOW/SESS "spatial profile determination mode (PROFILE_GET):                  {PROFILE_GET}"
SHOW/SESS "write additional images (IMG_WRITE):                               {IMG_WRITE}"
SHOW/SESS "polynomial fit degree (FIT_DEG):                                   {FIT_DEG}"
SHOW/SESS "number of iterations in optimum extraction (EXT_ITER):             {EXT_ITER}"
SHOW/SESS "CCD readout noise (CCD_RON):                                       {CCD_RON}"
SHOW/SESS "CCD gain (CCD_GAIN):                                               {CCD_GAIN}"
SHOW/SESS "CCD threshold (CCD_THRES)                                          {CCD_THRES}"
SHOW/SESS "root name of straightened image (STRAIGHT_IMG):                    {STRAIGHT_IMG}"
SHOW/SESS "root name of fitted profiles image (FIT_IMG):                      {FIT_IMG}"
SHOW/SESS "root name of masked pixels image (MASK_IMG):                       {MASK_IMG}"
SHOW/SESS "root name of profile table (COEF_COP):                             {COEF_COP}"
SHOW/SESS "root name of extracted flatfield image (FLATEXT_IMG):              {FLATEXT_IMG}"
SHOW/SESS "root name of flat-fielded image (UNBLAZED_IMG):                        {UNBLAZED_IMG}"
SHOW/SESS "fiber mode (FIBER_MODE):                                           {FIBER_MODE}"
SHOW/SESS " "
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS "-------------------------------------------------------------------------------"

IDENTIFY:
SHOW/SESS "Line identification related: SEARCH/FEROS, CALIBRATE/FEROS"
SHOW/SESS "   "
SHOW/SESS "line width, background level (LINE_W, LINE_THRES):                 {LINE_W},{LINE_THRES}"
SHOW/SESS "root name of found lines table (LINE_POS_TBL):                     {LINE_POS_TBL}"
SHOW/SESS "method for line position determination (LINE_MTD):                 {LINE_MTD}"
SHOW/SESS "line type (LINE_TYPE):                                             {LINE_TYPE}"
SHOW/SESS "reference line table (LINE_REF_TBL):                               {LINE_REF_TBL}"
SHOW/SESS "root name of dispersion relation coefficient table (COEF_WLC):     {COEF_WLC}"
SHOW/SESS "dispersion relation coefficient determination mode (INIT_WLC):     {INIT_WLC}"
SHOW/SESS "alpha, tol for initial identification (ID_ALPHA, ID_TOL):          {ID_ALPHA},{ID_TOL}"
SHOW/SESS "minimum intensity for initial identification (ID_THRES):           {ID_THRES}"
SHOW/SESS "CCD rotation and shift (CCD_ROT, CCD_SHIFT):                       {CCD_ROT},{CCD_SHIFT}"
SHOW/SESS "grating constant (SPECTR_G):                                       {SPECTR_G}" 
SHOW/SESS "camera focal length {SPECTR_F):                                    {SPECTR_F}"
SHOW/SESS "blaze angle (SPECTR_THB):                                          {SPECTR_THB}"
SHOW/SESS "first and last order (ORDER_FIRST, ORDER_LAST):                    {ORDER_FIRST},{ORDER_LAST}"
SHOW/SESS "fiber mode (FIBER_MODE):                                           {FIBER_MODE}"
SHOW/SESS " "
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS "-------------------------------------------------------------------------------"

REBIN:
SHOW/SESS "Rebin related: REBIN/FEROS, MERGE/FEROS"
SHOW/SESS "   "
SHOW/SESS "root name of rebinned image (REBIN_IMG):                           {REBIN_IMG}"
SHOW/SESS "scaling (lIn,lOg) (REBIN_SCL):                                     {REBIN_SCL}"
SHOW/SESS "step size in rebinned image (REBIN_STEP):                          {REBIN_STEP}"
SHOW/SESS "method for rebinned line construction (REBIN_MTD):                 {REBIN_MTD}"
SHOW/SESS "root name of merged spectrum (MERGE_IMG):                          {MERGE_IMG}"
!SHOW/SESS "merge option (MERGE_OPT):                                          {MERGE_OPT}"
SHOW/SESS "merge method (AVE/NOA/SIN) (MERGE_MTD):                            {MERGE_MTD}"
SHOW/SESS "merge delta (MERGE_DELTA):                                         {MERGE_DELTA}"
SHOW/SESS "merge orders (MERGE_ORD):                                          {MERGE_ORD(1)},{MERGE_ORD(2)}"
SHOW/SESS "fiber mode (FIBER_MODE):                                           {FIBER_MODE}"
SHOW/SESS " "
IF P1(1:1) .NE. "A"  RETURN

SET/FORMAT
RETURN
