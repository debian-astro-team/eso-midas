! @(#)ferosiden.prg	19.1 (ESO-IPG) 02/25/03 14:22:02
! @(#)ferosiden.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
! @(#)ferosiden.prg	1.0 (ESO-IPG) 14-11-97
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1998 European Southern Observatory
!.IDENT       ferosiden.prg
!.AUTHOR      Otmar Stahl Malina, LSW Heidelberg
!.KEYWORDS    Spectroscopy, Echelle, FEROS
!.PURPOSE     Do wavelength calbration on spectrum
!.VERSION     1.0    Creation    09-Nov-1998
!
!-------------------------------------------------------
!
define/param P1 {LINE_POS_TBL} T "line table: "

run STD_EXE:echidena
