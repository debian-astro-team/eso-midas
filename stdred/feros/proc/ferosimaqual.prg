! @(#)ferosimaqual.prg	19.1 (ESO-IPG) 02/25/03 14:22:02
! @(#)ferosimaqual.prg	1.1 (ESO-IPG) 02/25/99 16:55:20
define/par p1 ? I     "Enter pixel-order frame : "
define/par p2 ? I     "Enter table with line positions : "
define/par p3 0.075 N "Enter window half width in world coordinates : "
define/par p4 4 N     "Enter number of fit parameters (<=4)  : "
!
!
write/key in_a {p1}
write/key in_b {p2}
write/key inputr {p3}
write/key inputi {p4}

run STD_EXE:imaqual 

end:




