% @(#)rebin_fero.hlq	19.1 (ESO-IPG) 02/25/03 14:20:56
% @(#)rebin_fero.hlq	14.1.1.1 (ESO-IPG) 09/16/99 09:46:46
% @(#)rebin_fero.hlq  1.0 (ESO-IPG) 14-11-1997
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1997 European Southern Observatory
%.IDENT      rebin_fero.hlq
%.AUTHOR     AM, LSW Heidelberg, IPG/ESO
%.KEYWORDS   MIDAS, help files, REBIN/FEROS
%.PURPOSE    On-line help file for the command: REBIN/FEROS
%.VERSION    1.0  14-NOV-97 : Creation, AM
%----------------------------------------------------------------
\se
SECTION./FEROS
\es\co
REBIN/FEROS                                           23-FEB-99 AM/OS
\oc\su
REBIN/FEROS  [extimage] [linetable] [rebimage] [scaling] [step,corr] 
                [method] [mode]
        Rebin extracted spectrum
\us\pu
Purpose:    
            Rebin extracted spectrum to constant steps in lambda or
            log lambda.
\up\sy
Syntax:     
            REBIN/FEROS  [extimage] [linetable] [rebimage] [scaling]
              [step,corr] [method] [mode]

\ys\pa
            extimage = root name of input file

\ap\pa
            linetable = root name of input table

\ap\pa
            rebimage = root name of output file

\ap\pa
            scaling = scaling of rebinned spectrum
            valid values are
            I for linear (default) and
            O for logarithmic

\ap\pa
            step = stepsize of rebinned spectrum, 
            corr = barycentric correction in km/sec
            

\ap\pa
            method = rebin method
            valid values are
            L for linear,
            Q for quadratic and
            S for spline (default)

\ap\pa
            mode = fiber mode
	    valid values are 
            OC for object-calibration
            OS for object-sky (default)
            in mode OC, no barycentric correction is applied for fiber 2

\ap\sa
See also: 
            EXTRACT/FEROS, FLAT/FEROS
\as\no

Note:       
            1) extimage is the root name of the input image
               (extracted, unblazed spectrum). For single fiber
               spectrographs this is also the complete name of the
               input image. For double fiber spectra two input images
               are read. Their names are generated from the root name
               and the appendices 1 and 2. extimage can be created by
               executing the command EXTRACT/FEROS.

            2) linetable is the root name of the input line table
               (identified lines). For single fiber spectrographs
               this is also the complete name of the input line table.
               For double fiber spectra two input tables are read.
               Their names are generated from the root name and the
               appendices 1 and 2. linetable can be created by executing
               the command SEARCH/FEROS.

            3) rebimage is the root name of the output image
               (rebinned spectrum). For single fiber spectrographs
               this is also the complete name of the output image.
               For double fiber spectra two output images are created.
               Their names are generated from the root name and the
               appendices 1 and 2.

            4) The distinction between single fiber and double fiber is
               made via the keyword FIBER_MODE.

\on\exs
Examples:
\ex
            REBIN/FEROS extracted found_lines rebinned
            Rebin spectrum named extracted using the table named
            found_lines to the spectrum named rebinned. Use default
            values for scaling, step and method.

\xe\ex
            REBIN/FEROS extracted found_lines rebinned O 0.1 L
            Rebin spectrum named extracted using the table named
            found_lines to the spectrum named rebinned. Use
            logarithmic scaling, a step size of log lambda = 0.1
            and linear rebinning.
\xe \sxe
