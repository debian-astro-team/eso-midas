% %W% (ESO-IPG) %G% %U%
% @(#)help_fero.hlq 1.0 (ESO-IPG) 12-11-1997
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1997 European Southern Observatory
%.IDENT      help_fero.hlq
%.AUTHOR     AM, LSW Heidelberg, IPG/ESO
%.KEYWORDS   MIDAS, help files, HELP/FEROS 
%.PURPOSE    On-line help file for the command: HELP/FEROS
%.VERSION    1.0  12-NOV-97 : Creation, AM
%----------------------------------------------------------------
\se
SECTION./FEROS
\es\co
HELP/FEROS                                            23-FEB-99 AM/OS
\oc\su
HELP/FEROS  [param]
        show feros session 
\us\pu
Purpose:    
            Show the parameters of the current session
\up\sy
Syntax:     
            HELP/FEROS [param]

\ys\pa
            param  =  usually, the complete name of an FEROS keyword
            If no parameter is entered, a general help message is
            displayed. You may also try *name*, if you cannot remember
            the full name.
\ap\no
Note:  
            Present value of the keyword is displayed.  
\on\exs
Examples:
\ex
            HELP/FEROS  *MODE
            display: Keyword:        LOC_MODE 
                     Default Value:   G  ; first element:  G 
                     Description: localization mode; valid values: S|G 
                     Type:         C/1/1 
                     All elements: 
                     G
     
                     Keyword:        BG_MODE 
                     Default Value:   B  ; first element:  B 
                     Description: background mode; valid values: B|N|S 
                     Type:         C/1/1 
                     All elements: 
                     B
     
                     Keyword:        FIBER_MODE 
                     Default Value:   2  ; first element:  2 
                     Description: number of fibers
                     Type:         I/1/1 
                     All elements: 
                     2
     
                     Keyword:        EXT_MODE 
                     Default Value:   O  ; first element:  O 
                     Description: extraction mode; valid values: S|O 
                     Type:         C/1/1 
                     All elements: 
                     O

\xe \sxe