% @(#)searchfero.hlq	19.1 (ESO-IPG) 02/25/03 14:20:57
% @(#)searchfero.hlq  1.0 (ESO-IPG) 13-11-1997
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1997 European Southern Observatory
%.IDENT      searchfero.hlq
%.AUTHOR     AM, LSW Heidelberg, IPG/ESO
%.KEYWORDS   MIDAS, help files, SEARCH/FEROS 
%.PURPOSE    On-line help file for the command: SEARCH/FEROS
%.VERSION    1.0  13-NOV-97 : Creation, AM
%----------------------------------------------------------------
\se
SECTION./FEROS
\es\co
SEARCH/FEROS                                            23-FEB-99 AM/OS
\oc\su
SEARCH/FEROS [spectrum] [lineparams] [linepos] [linemethod] [linetype]
        Search for emission/absorption lines in spectra.

\us\pu
Purpose:    
            Search for emission/absorption lines in spectra.

\up\sy
Syntax:     
            SEARCH/FEROS [spectrum] [lineparams] [linepos] [linemethod]
              [linetype]

\ys\pa
            spectrum = root name of input spectrum

\ap\pa      
            lineparams = linewidth,linethres,linescan
            Parameters for line search
                linewidth = (window) is the approx. zero intensity line
                             width in pixels.
                linethres = (threshold) is the absolute value of the
                            background.
                linescan  = number of scan lines of the input spectrum
                            to be averaged before searching for the
                            lines. Default is one.

\ap\pa
            linepos  = output table
            The output table contains the columns:
            :X    - center of the line, in world coordinates
            :Y    - scan number for 2D spectra
            :PEAK - value at the maximum/minimum

\ap\pa
            linemethod = centering method
            valid values are
            GRAVITY  - center of gravity of the 2 highest pixels
                       with respect to the third one.
            MAXIMUM  - pixel with the maximum value
            MINIMUM  - pixel with the minimum value (default for
                       absorption lines)
            GAUSSIAN - center of the gaussian fitted to the line
                       (Default method).

\ap\pa
            linetype = EMISSION (Default) or ABSORPTION

\ap\sa
See also: 
            SEARCH/LINE

\as\no
Note:       
            1) spectrum and linepos are root names. For double
               fiber mode the names of the files to be accessed
               are generated from the root name and the appendices
               1 and 2. For all other modes the filenames are
               identical with the root names.

            2) The distinction between single fiber and double fiber is
               made via the keyword FIBER_MODE.

\on\exs
Examples:
\ex
            SEARCH/FEROS  extracted 5,0,1 found_lines GRAVITY EMISSION
            Search for emission lines in the spectrum with root name
            extracted. Signals that have five or more pixels and peak
            value at or above 0 are detected. No averaging of scan
            lines is performed. The line position is determined with
            the center of gravity method. The found lines are stored
            in a table named found_lines.

\xe \sxe
