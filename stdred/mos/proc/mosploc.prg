! @(#)mosploc.prg	8.3 (ESO-IPG) 11/23/94 14:19:57
! @(#)mosploc.prg	1.1 (ESO-IPG) 2/14/94 13:57:57
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSFIND.PRG
!
! .PURPOSE
!
! execute the command :
!  LOCATE/MOS ff para
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 {MOS}      TABLE  "Slit Table:"
!
! SET/MOS MOS={P1}
!
@ creifnot 1
!
set/graph
set/graph pmode=1 font=1 
!
set/graph  stype=3
plot/table {P1} ? :ystart
set/graph  stype=4
overplot/table {P1} ? :yend
!
RETURN


