! @(#)moscalib.prg	19.1 (ESO-DMD) 02/25/03 14:26:43
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       moscalib.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!             Sabine Moehler   (LSW)
!.KEYWORDS    Spectroscopy, Long-Slit, MOS
!.PURPOSE     fit dispersion relation
!.VERSION     1.0  Package Creation  17-MAR-1993  
!             1.1  adapted for MOS   20-AUG-1993   
!
! CALIBWLC/MOS tolerance degree method central wavelength, av. dispersion 
!               (A/pix), plot option
!-------------------------------------------------------
!
CROSSREF tol deg mtd cen,disp plo disp
!
DEFINE/PARAM  P1  {TOL}        NUMB   "Tolerance "
DEFINE/PARAM  P2  {WLCORD(1)}  NUMB   "Degree     "
DEFINE/PARAM  P3  {WLCMET}     CHAR  -
 "Mode (I[dent]/L[inear]/R[ecall])(C[onst.]/V[ar.]/T[wo-dim.]) (Default: RT) :"
DEFINE/PARAM  P4  {WCENTER},{AVDISP},0.  NUMB   "Center wav., dispersion:"
DEFINE/PARAM  P5  {PLOTC}      CHAR   "Plot of residuals (Y/N):"
DEFINE/PARAM  P6  {DISP}       NUMB  -
 "Display (100 = detailed, 50 = partial. Default = 0)"
!
DEFINE/LOCAL I/I/1/1 0
DEFINE/LOCAL KEYSEL/C/1/64 - 
DEFINE/LOCAL COUNT/I/1/1 0.
!-------------------------------------------------------
! check if linpos and linecat are tables
!-------------------------------------------------------
VERIFY/MOS {LINPOS} TAB
VERIFY/MOS {LINECAT} TAB
!
!-------------------------------------------------------
! get parameters
!-------------------------------------------------------
WRITE/KEYW DISP   {P6}
!
WRITE/KEYW IN_A   {LINPOS}
WRITE/KEYW IN_B   {LINECAT}
WRITE/KEYW INPUTC {P3}
WRITE/KEYW OUT_A  {LINFIT}
WRITE/KEYW IN_C/C/1/60   {MOS}
!
INPUTR(1) = ALPHA
INPUTR(2) = MAXDEV
INPUTR(3) = {P1}
!
INPUTI(10) = DISP
!
COPY/DK {wlc} START/D/1/2 START/D/1/2
COPY/DK {wlc} STEP/D/1/2 STEP/D/1/2
COPY/KEYW  START/D/1/2   INPUTD/D/1/2  
COPY/KEYW  STEP/D/1/2    INPUTD/D/3/4  
!-------------------------------------------------------
! set integer parameters
!-------------------------------------------------------
WRITE/KEY INPUTI/I/1/1 {P2}
INPUTI(2) = NPIX(1)+START(1) ! total number of pixels along dispersion axis
INPUTI(3) = WLCNITER(1)
INPUTI(4) = WLCNITER(2)
INPUTI(5) = YSTART
!-------------------------------------------------------
! Linear Fit or Recall Method
!-------------------------------------------------------
IF INPUTC(1:1) .NE. "I"  THEN
!
  IF NPIX(1) .EQ. 0 THEN
    COPY/DK {WLC} NPIX NPIX/I/1/2 
  ENDIF
  IF NPIX(1) .EQ. 0 THEN
    WRITE/OUT "No wavelength calibration frame defined - frame size unknown!"
    GOTO END
  ENDIF
  
  DEFINE/LOCAL WDEF/D/1/3  {P4}
  INPUTD(5) = WDEF(1) - (NPIX(1)+START(1)-1)/2.*WDEF(2) ! Coeffs in world 
  INPUTD(6) = WDEF(2)                                   ! coordinates system
  INPUTD(7) = WDEF(3)					
ENDIF
!
!-------------------------------------------------------
! FORS Fit 
!-------------------------------------------------------
IF INPUTC(1:1) .EQ. "F" THEN
!SET/FORMAT I2   
  write/out "Warning: FORS option has been replaced"
  goto end
!  IF GRISM .EQ. 0 THEN
!    WRITE/OUT "No grism defined!"
!    GOTO END
!  ENDIF
  IF NPIX(1) .EQ. 0 THEN
    COPY/DK {WLC} NPIX NPIX/I/1/2 
  ENDIF
  IF NPIX(1) .EQ. 0 THEN
    WRITE/OUT "No wavelength calibration frame defined - frame size unknown!"
    GOTO END
  ENDIF
  DEFINE/LOCAL WDEF/D/1/3  {P4}	
!  INPUTD(5) = FORS{GRISM}(1) - (NPIX(1)+START(1)-1)/2.*FORS{GRISM}(2) ! Coeffs
!  INPUTD(6) = FORS{GRISM}(2)                      ! in world coordinates
!  INPUTD(7) = 0.
  INPUTD(5) = WDEF(1) - (NPIX(1)+START(1)-1)/2.*WDEF(2) ! Coeffs in world 
  INPUTD(6) = WDEF(2)                                   ! coordinates system
  INPUTD(7) = WDEF(3)
ENDIF
!
RUN STD_EXE:moscalib
!
!-------------------------------------------------------
! Plot Option
!-------------------------------------------------------
COPY/DK {linpos}.tbl TSELTABL KEYSEL
IF P5(1:1) .eq. "Y" THEN
 DO I = 1 {NSLIT} 
   IF CAL({I}:{I}) .gt. 0 THEN
     RESPLOT/MOS {LINPOS} {I}  
   ELSEIF CAL({I}:{I}) .lt. 0 THEN
     WRITE/OUT "No dispersion relation for slitlet no. {I}"
   ENDIF
 ENDDO
ELSE 
 GOTO END
ENDIF
END:
IF KEYSEL(1:1) .eq. "-" THEN
  SELECT/TABLE {linpos} all
ELSE
  COPY/KD KEYSEL/c/1/64 {linpos}.tbl TSELTABL/c/1/64
ENDIF

