! @(#)mosverif.prg	19.1 (ESO-DMD) 02/25/03 14:26:46 
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnverif.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!.VERSION     1.0  Package Creation  17-MAR-1993  
! 020913	last modif
!-------------------------------------------------------
!
DEFINE/PARAM  P1  ?  CHAR  "Input file:"
DEFINE/PARAM  P2  ?  CHAR  "Type (IMA/TAB/OTIME/AIRMASS) :"

! Before any further test, checks that the file exists.

DEFINE/LOCAL EXIST/I/1/1 0
DEFINE/LOCAL INDEX/I/1/1 0
DEFINE/LOCAL INPFIL/C/1/60 " " ALL
DEFINE/LOCAL ERRORS/I/1/1   0

INDEX = M$INDEX(P1,".")
WRITE/KEYW INPFIL {P1}

IF INDEX .EQ. 0 THEN
   IF P2(1:1) .EQ. "T"  THEN
          WRITE/KEYW INPFIL {P1}.tbl
   ELSE
          WRITE/KEYW INPFIL {P1}.bdf
   ENDIF
ENDIF

!EXIST = M$EXIST(INPFIL)

IF P2(1:1) .EQ. "T" THEN  ! Table file
   IF M$EXISTD(INPFIL,"TBLENGTH") .EQ. 1  exist = 1
ELSE                      ! Image
   IF M$EXISTD(INPFIL,"NAXIS") .EQ. 1     exist = 1
ENDIF 

IF EXIST .EQ. 0 THEN
   IF P2(1:1) .EQ. "O"  WRITE/OUT "Error ** Could not find file : {P1}"
   IF P2(1:1) .EQ. "I"  WRITE/OUT "Error ** Could not find image : {P1}"
   IF P2(1:1) .EQ. "T"  WRITE/OUT "Error ** Could not find table : {P1}"
   IF P2(1:1) .EQ. "A"  WRITE/OUT "Error ** Could not find file : {P1}"
   RETURN/EXIT
ENDIF

IF P2(1:1) .EQ. "I" THEN

   IF {{P1},STEP(1)} .LE. 0 THEN
      WRITE/OUT "Error: Negative descriptor step in file {P1}"
      RETURN/EXIT
   ENDIF

ENDIF

IF P2(1:1) .EQ. "O" THEN

   IF M$EXISTD(P1,"O_TIME") .LE. 0 THEN
      WRITE/OUT "Error: Descriptor not present : O_TIME in file {P1}"
      ERRORS = 1
   ELSE
      IF {{P1},O_TIME(7)} .LE. 0.  THEN
         WRITE/OUT "Error: Incorrect time {{P1,O_TIME(7)} in file {P1}"
         ERRORS = 1
      ENDIF
   ENDIF

   IF ERRORS .EQ. 1 THEN
      WRITE/KEYW   INPUTD/D/1/20  0. ALL
      INQUIRE/KEYW INPUTD/D/7/1 "Enter Observation time (in seconds) : "
      COPY/KD     INPUTD/D/1/7 {P1}   O_TIME/D/1/7
   ENDIF

ENDIF

IF P2(1:1) .EQ. "A" THEN

   IF M$EXISTD(P1,"O_AIRM") .LE. 0 THEN
      WRITE/OUT "Error: Descriptor not present : O_AIRM in file {P1}"
      ERRORS = 1
   ELSE
      IF {{P1},O_AIRM} .LE. 0.  THEN
         WRITE/OUT "Error: Incorrect descriptor O_AIRM : {{P1},O_AIRM} in file {P1}"
         ERRORS = 1
      ENDIF
   ENDIF

   IF ERRORS .EQ. 1 THEN
      WRITE/KEYW   INPUTR/R/1/20  0. ALL
      INQUIRE/KEYW INPUTR/R/1/1 "Enter Airmass : "
      COPY/KD     INPUTR/R/1/1 {P1}   O_AIRM/R/1/1
   ENDIF

ENDIF



