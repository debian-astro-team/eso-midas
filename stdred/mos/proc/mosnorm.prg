! @(#)mosnorm.prg	8.3 (ESO-IPG) 05/07/96 10:48:37
! @(#)mosnorm.prg	1.3 (ESO-IPG) 2/11/94 13:19:59
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSNORM.PRG
!
! .PURPOSE
!
! execute the command :
! NORM/MOS image params
!
! -------------------------------------------------
!
CROSSREF FF TABLE NORMFF ORDER METHOD
!
DEFINE/PARAM P1 {FLAT}     IMAGE   "Enter input image:"
DEFINE/PARAM P2 {MOS}      TABLE   "Enter MOS table:"
DEFINE/PARAM P3 {NORMFLAT} IMAGE   "Enter output mage:"
DEFINE/PARAM P4 {FFORD}    NUMBER  "Enter order of fit:"
DEFINE/PARAM P5 {NORMMET}  ?       "Method for smoothing:"
!
!SET/MOS flat={P1} normflat={P2} fford={P3} mos={P4} NORMMET={P5}
!
WRITE/KEYW IN_A {P1}
WRITE/KEYW IN_B {P2}
WRITE/KEYW OUT_A {P3}
WRITE/KEYW INPUTI {P4}
WRITE/KEYW INPUTC {P5}
!
RUN STD_EXE:MOSNORM
