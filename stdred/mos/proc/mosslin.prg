! @(#)mosslin.prg	8.4 (ESO-IPG) 05/07/96 10:49:04
! @(#)mosslin.prg	1.7 (ESO-IPG) 2/17/94 17:28:46
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSSLIN.PRG
!
! .PURPOSE
!
! execute the command :
! SEARCH/MOS image params
!
! -------------------------------------------------
!
CROSSREF WLC SEAPAR YBIN METHOD TABLE LINPOS 
!
DEFINE/PARAM P1 {WLC}      IMAGE   "Enter input image:"
DEFINE/PARAM P2 {SEAPAR}   NUMBER  "Threshold, window:"
DEFINE/PARAM P3 {YBIN}     NUMBER  "Step, binning in Y:"
DEFINE/PARAM P4 {CENTMET}  ?       "Centering method"
DEFINE/PARAM P5 {MOS}      TABLE   "Enter MOS table:"
DEFINE/PARAM P6 {LINPOS}   TABLE   "Enter output table:"
!
DEFINE/MAXPAR 6
!
! SET/MOS wlc={P1} seapar={P2} ybin={P3} centmet={P4} mos={P5} linpos={P6} 
!
NPIX(1) = {{P1},NPIX(1)}
IF {{P1},NAXIS} .GT. 1 NPIX(2) = {{P1},NPIX(2)}
!
WRITE/KEYW IN_A   {P1}
WRITE/KEYW IN_B   {P5}
WRITE/KEYW OUT_A  {P6}
WRITE/KEYW INPUTI {P2},{P3}
WRITE/KEYW INPUTC {P4}
!
RUN STD_EXE:MOSSLIN
!

