! @(#)mosplresid.prg	8.2 (ESO-IPG) 11/18/94 09:21:27
! @(#)mosplresid.prg	1.3 (ESO-IPG) 2/15/94 12:08:50
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       mosplresid.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!             S. Moehler       (LSW)
!.KEYWORDS    MOS 
!.PURPOSE     plot residuals
!.VERSION     1.0  Package Creation  17-MAR-1993  
!             1.1  adapted for MOS   20-AUG-1993 
!-------------------------------------------------------
!
crossref linpos islit
DEFINE/PARAM P1 {LINPOS} TAB "Line table : "
DEFINE/PARAM P2 {ISLIT} NUM "Number of slitlet "
DEFINE/LOCAL KEYSEL/C/1/64 x all
!
VERIFY/MOS  {P1} TABLE
!
GRAPH/SPEC
SET/GRAPH
SET/GRAPH pmode=1 font=1
!-------------------------------------------------------
! Redirect MIDAS comments
!-------------------------------------------------------
SET/MIDAS OUTPUT=LOGONLY
!-------------------------------------------------------
! Copy selection flag
!-------------------------------------------------------
COPY/DK {P1}.tbl TSELTABL KEYSEL
IF KEYSEL(1:1) .eq. "-" THEN
  SELECT/TABLE {P1}.tbl :SLIT.EQ.{P2}
ELSE
  SELECT/TABLE {P1}  SELECT.AND.:SLIT.EQ.{P2}
ENDIF
!-------------------------------------------------------
! Plot fitted lines
!-------------------------------------------------------
IF OUTPUTI(1) .GT. 0 THEN
SET/MIDAS OUTPUT=YES
 WRITE/OUT "Slit Nr. {P2}"
 PLOT/TABLE {P1} :WAVE  :RESIDUAL
SET/MIDAS OUTPUT=LOGONLY
!-------------------------------------------------------
! Plot rejected lines
!-------------------------------------------------------
 SET/GRAPH  STYPE=18  COLOR=3
 WRITE/OUT "Rejected lines"
 SELECT/TABLE {P1} SELECT.AND.:REJECT.EQ.-5
 OVERPLOT/TABLE {P1} :WAVEC  :RESIDUAL
ENDIF
!-------------------------------------------------------
! Reset system
!-------------------------------------------------------
SET/GRAPH
IF KEYSEL(1:1) .eq. "-" THEN
  SELECT/TABLE {P1} all
ELSE
  SELECT/TABLE {P1}  ({KEYSEL})
ENDIF
SET/MIDAS OUTPUT=YES
RETURN
