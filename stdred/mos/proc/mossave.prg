! @(#)mossave.prg	1.4 (ESO-IPG) 2/15/94 11:30:54
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       mossave.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, MOS
!.PURPOSE     Command SAVE/MOS
!.VERSION     1.0    Creation    23-AUG-1991 PB
!
!-------------------------------------------------------
!
DEFINE/PARAM   P1    ?     C    "Session name :"
!
WRITE/OUT Process tables are saved
!
!
SET/MIDAS OUTPUT=NO
SELECT/TABLE  {MOS}     ALL
COPY/TABLE    {MOS}     {P1}SLIT.tbl
SELECT/TABLE  {LINPOS}  ALL
COPY/TABLE    {LINPOS}  {P1}LIN.tbl
SELECT/TABLE  {LINFIT}  ALL
COPY/TABLE    {LINFIT}  {P1}FIT.tbl
SELECT/TABLE  {WINDOWS} ALL
COPY/TABLE    {WINDOWS} {P1}WIN.tbl
SET/MIDAS OUTPUT=YES
!
SAVINIT/MOS  {P1}SLIT.tbl  WRITE
!
RETURN

