! @(#)mosflat.prg	1.3 (ESO-IPG) 2/11/94 13:19:59
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSFLAT.PRG
!
! .PURPOSE
!
! execute the command :
! FLAT/MOS image params
!
! -------------------------------------------------
!
CROSSREF OBJ OBJF FF TABLE NORMFF ORDER METHOD
!
DEFINE/PARAM P1 {OBJ}   IMAGE    "Enter input image:"
DEFINE/PARAM P2 XXX        IMAGE   "Enter output image:"
DEFINE/PARAM P3 {FLAT}     IMAGE   "Enter input flat:"
DEFINE/PARAM P4 {MOS}      TABLE   "Enter MOS table:"
DEFINE/PARAM P5 {NORMFLAT} IMAGE   "Enter output flat:"
DEFINE/PARAM P6 {FFORD}    NUMBER  "Enter order of fit:"
DEFINE/PARAM P7 {NORMMET}  ?       "Method for smoothing:"
!
!SET/MOS flat={P3} normflat={P4} fford={P5} mos={P6} NORMMET={P7}
!
WRITE/KEYW IN_A {P3}
WRITE/KEYW IN_B {P4}
WRITE/KEYW OUT_A {P5}
WRITE/KEYW INPUTI {P6}
WRITE/KEYW INPUTC {P7}
!
RUN STD_EXE:MOSNORM
IF P2(1:3) .eq. "XXX" then
  WRITE/KEY OUT/C/1/60 F{P1}
ELSE
  WRITE/KEY OUT/C/1/60 {P2}
ENDIF
COMP/IMA {OUT} = {P1}/{P5}
