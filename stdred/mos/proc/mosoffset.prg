! @(#)mosoffset.prg	1.3 (ESO-IPG) 2/17/94 17:42:34
! @(#)mosoffset.prg	version 5/30/96 -- T.Szeifert@lsw.uni-heidelberg.de 
! 	

define/param P1 {MOS}     TAB  "Slit Table:"
define/param P2 {WLC}     IMA  "Arc frame:"
define/param P3 {SEAPAR}  TAB  "Search parameters:"

define/local slit/I/1/1 0
define/local offset/R/1/1 0. all
define/local pixel/I/1/2  0,0
define/local seap/R/1/2 {P3}
set/format I1  F7.2

pixel(1) = {{P1},:ystart,@1}
pixel(2) = {{P1},:yend,@1}

COPY/DK {P2} npix npix/i/1/2						!<<
set/midas output=LOGONLY
AVERAGE/ROW &i = {P2} {pixel(1)},{pixel(2)}
SEARCH/LINE &i {seap(2)},{seap(1)}  &a  gauss emiss
comp/tabl &a :PEAK = sqrt(:PEAK)					!<<
WRITE/TABLE {P1} :XOFFSET @1 0.

SET/MIDAS OUTPUT=YES

write/out "Slit Nr. 1. Offset = 0."


DO slit = 2 {NSLIT}
   SET/MIDAS output=LOGONLY
   pixel(1) = {{P1},:ystart,@{slit}}
   pixel(2) = {{P1},:yend,@{slit}}
   AVERAGE/ROW &i = {P2} {pixel(1)},{pixel(2)}
   SEARCH/LINE &i {seap(2)},{seap(1)}  &b  gauss emiss
   comp/tabl &b :PEAK = sqrt(:PEAK)
   CORRELATE/LINE &a &b 1 0.,3,{NPIX(1)},2  X,+,:PEAK  		!<<
   FIND/MINMAX &x
   offset = (outputi(3)-1)*{middummx.bdf,STEP(1)}+{middummx.bdf,START(1)}
   {P1},:XOFFSET,@{SLIT} = OFFSET 
   SET/MIDAS OUTPUT=YES
   WRITE/OUT "Slit Nr. {slit}. Offset = {offset}"
ENDDO
name/colum {p1} #4 ? f9.3



