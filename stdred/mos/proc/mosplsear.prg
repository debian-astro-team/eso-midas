!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993-2010 European Southern Observatory
!.IDENT       mosident.prg
!.AUTHORS     Sabine Moehler   (LSW)
!             
!.KEYWORDS    Spectroscopy, Long-Slit, MOS
!.PURPOSE     line identification 
!.VERSION     1.0  Package Creation  17-MAR-1993  
!             1.1  adapted for MOS   20-AUG-1993   
!-------------------------------------------------------
!
define/param P1  2D       CHAR  "Mode (1D/2D):"
define/param P2  1        NUMB  "Slit number (in 1D):"
define/param P3  {LINPOS} CHAR  "Table:"
!
define/local table/C/1/60 {P3}
define/local row/I/1/1    0 
!
!
IF P1(1:1) .EQ. "1" THEN
   set/midas output=LOGONLY
   SELECT/TABLE {P3} :SLIT.EQ.{P2}
   copy/table   {P3} &t 
   SELECT/TABLE {P3} ALL
   write/keyw    table  middummt.tbl
   row = {middummt.tbl,:Y,@1}
   select/table &t :Y.EQ.{row}
   SET/MIDAS output=YES
ENDIF
!
@s splsear {P1} {table} ? {row}
!
RETURN



