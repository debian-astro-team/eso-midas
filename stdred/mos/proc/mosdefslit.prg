! @(#)mosdefslit.prg	8.5 (ESO-IPG) 11/18/94 09:21:19
! @(#)mosdefslit.prg	8.5 (ESO-IPG) 11/18/94 09:21:19
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       mosdefslit.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!.KEYWORDS    Spectroscopy, Multi-Object
!.PURPOSE     Interactive definition of slitlet positions
!.VERSION     1.0  Creation  15-NOV-1994
!-------------------------------------------------------
!
!
DEFINE/PARAM P1  READ    CHAR   "Mode (INIT/ADD/DELETE/LOAD/READ): "
DEFINE/PARAM P2   +       ?     "Slit Number:"
DEFINE/PARAM P3   +       ?     "Slit limits"
DEFINE/PARAM P4   0.     NUMB   "Slit Offset:"
DEFINE/PARAM P5  mos.tbl TAB    "MOS Slits Table:"
DEFINE/PARAM P8   *      IMA    "Displayed image:"
!
define/local index/I/1/1 0
index  =  M$INDEX(P5,".tbl")
if index .lt. 1 write/key P5 {P5}.tbl
!
IF P1(1:1) .EQ. "I" THEN 
  if P2(1:1) .NE. "+"   then 
      @s mosdefslit,init  {P5}
  else
      @s mosdefslit,init {P5} {P2}
  endif  
  Write/Out "Created table {P5}"
ENDIF
!
IF P1(1:1) .EQ. "R" THEN
   READ/TABLE {P5}
ENDIF
!
IF P1(1:1) .EQ. "S" THEN
   SORT/TABLE {P5} :slit
ENDIF
!
IF P1(1:1) .EQ. "A" THEN
   define/local pos/I/1/1    {{P5},TBLCONTR(4)}
   pos = pos + 1

   IF P3(1:1) .EQ. "C" THEN 
      define/local ncurs/I/1/2 0,0
      if P2(1:1) .EQ. "+"  then 
         ncurs = 1
      else
         ncurs = {P2}
      endif
      WRITE/OUT "Now click in the display on the lower, then upper"
      WRITE/OUT "positions of the slit ({ncurs} slits)"
      ncurs(2) = ncurs*2
      CURS:
      GET/CURSOR &c ? ? {ncurs(2)},1
      WRITE/OUT "Now updating the table..."
      IF {middummc.tbl,TBLCONTR(4)} .ne. ncurs(2) then
       write/out "Error: You did not enter {ncurs} positions for the"
       write/out "expected {ncurs} slits. Please restart."
       goto CURS
      ENDIF
      define/local ydescr/D/1/2 0.,0.
      copy/dk {P8} START/D/2/1  ydescr/D/1/1
      copy/dk {P8} STEP/D/2/1   ydescr/D/2/1
      COMPUTE/TABLE &c :Y_COORD = (:Y_COORD - {ydescr(1)})/{ydescr(2)} + 1.
      define/local nnslit/I/1/4 0,0,0,0
      define/local slit/I/1/1  0
      DO SLIT = 1 {NCURS}
       nnslit(1) = 1  + (slit-1)*2
       nnslit(2) = nnslit(1) + 1
       nnslit(3) = pos + slit - 1
       WRITE/TAB {P5} @{nnslit(3)} :ystart {middummc.tbl,:Y_COORD,@{nnslit(1)}}
       WRITE/TAB {P5} @{nnslit(3)} :yend   {middummc.tbl,:Y_COORD,@{nnslit(2)}}
       WRITE/TAB {P5} @{nnslit(3)} :xoffset {P4}
      ENDDO
   ELSE
     define/local limits/I/1/2 {P3}
     WRITE/TABLE {P5} @{pos} :ystart  {limits(1)}
     WRITE/TABLE {P5} @{pos} :yend    {limits(2)}
     WRITE/TABLE {P5} @{pos} :xoffset {P4}
   ENDIF

   SORT/TABLE  {P5} :ystart
   compute/table {P5} :slit = sequence
   NSLIT = {{P5},TBLCONTR(4)}
ENDIF
!
IF P1(1:1) .EQ. "M" THEN
   define/local limits/I/1/2 {P3}
   define/local pos/I/1/1    {P2}
   WRITE/TABLE {P5} @{pos} :ystart  {limits(1)}
   WRITE/TABLE {P5} @{pos} :yend    {limits(2)}
   WRITE/TABLE {P5} @{pos} :xoffset {P4}
   SORT/TABLE  {P5} :ystart
   compute/table {P5} :slit = sequence
   NSLIT = {{P5},TBLCONTR(4)}
ENDIF
!
IF P1(1:1) .EQ. "D" THEN
   select/table {P5} sequence.ne.{P2}
   copy/table   {P5} &w
   COMPUTE/TABLE &w :slit = sequence
   COPY/TABLE   &w   {P5}
   READ/TABLE   {P5}
   NSLIT = {{P5},TBLCONTR(4)}
ENDIF
!
IF P1(1:1) .EQ. "L" THEN
!
copy/table {P5} &a
copy/table {P5} &b
comp/table &a :x_pos = {{idimemc},start(1)}
comp/table &b :x_pos = {{idimemc},start(1)}+({{idimemc},step(1)}*{{idimemc},npix(1)})
merge/table &a &b &c
sort/table &c :ystart,:x_pos
clear/chan over
if P2(1:1) .EQ. "L" then 
   write/out "Lower slit limits represented in green..."
  load/table &c :x_pos :ystart :slit -1 ?  4
endif
if P2(1:1) .EQ. "U" then
   write/out "Upper slit limits represented in blue..."
   load/table &c :x_pos :yend   :slit -1 ?  5
endif
if P2(1:1) .NE. "L" .AND. P2(1:1) .NE. "U" then
   write/out "Lower slit limits represented in green..."
   write/out "Upper slit limits represented in blue..."
   load/table &c :x_pos :ystart :slit -1 ?  4
   load/table &c :x_pos :yend   :slit -1 ?  5
endif
!
ENDIF
!
ENTRY INIT
!
DEFINE/PARAM P1 ? TAB "Name:"
DEFINE/PARAM P2 10 NUMB "Number of slits:"
!
CREATE/TABLE  {P1}  {P2}  100
!
CREATE/COLUMN {P1} :slit    F6.0   R*4 "Unitless"
CREATE/COLUMN {P1} :ystart  F7.1   R*4 "Pixel"
CREATE/COLUMN {P1} :yend    F7.1   R*4 "Pixel"
CREATE/COLUMN {P1} :xoffset F7.1   R*4 "Pixel"
!
NSLIT = 0
!
RETURN



