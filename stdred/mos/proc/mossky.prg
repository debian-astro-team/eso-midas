! @(#)mossky.prg	8.6 (ESO-IPG) 12/08/94 10:12:30
! @(#)mossky.prg	1.3 (ESO-IPG) 2/11/94 13:20:01
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSSKY.PRG
!
! .PURPOSE
!
! execute the command :
!  FITSKY/MOS image mos-table windows-table out par mode
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
CROSSREF OBJ TABLE SKYFRAME SKYPAR
!
DEFINE/PARAM P1 {OBJ}   IMAGE  "Enter input image:"
DEFINE/PARAM P2 {MOS}      TABLE  "Enter input mos table:"
DEFINE/PARAM P3 {WINDOWS}  TABLE  "Enter input windows table:"
DEFINE/PARAM P4 {SKYFRAME} IMAGE  "Enter output frame:"
DEFINE/PARAM P5 {SKYPAR}   NUMBER "Enter fit order:"
DEFINE/PARAM P6 {SKYMET}   ?      "Mode:"
IF "{P6(1:3)}" .eq. "pol"  .or. "{P6(1:3)}" .eq. "POL" THEN
   DEFINE/PARAM P7 {CCDPAR(1:60)},{REJTHRES} ? "filter parameter"
   WRITE/KEYW INPUTR/R/1/3 {P7}	
ENDIF
!
!SET/MOS obj={P1} MOS={P2} windows={p3} skyframe={P4} skypar={P5}
!
!
if "{p6(1:3)}" .eq. "NOW" then
   write/out "No windows"
   cop/table {mos} middummz
   name/column middummz :SLIT :SKY_SLIT
   name/column middummz :YSTART :SKY_STRT
   name/column middummz :YEND   :SKY_END
   write/keyw IN_C/C/1/60 middummz
   write/keyw INPUTC/C/1/3 MED
else	
   WRITE/KEYW IN_C/C/1/60 {P3}
endif
!
WRITE/KEYW IN_A {P1}
WRITE/KEYW IN_B {P2}
WRITE/KEYW OUT_A {P4}
WRITE/KEYW INPUTI {P5}
WRITE/KEYW INPUTC {P6}
!
RUN STD_EXE:MOSSKY

