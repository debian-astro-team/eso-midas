! @(#)mosinit.prg	1.6 (ESO-IPG) 2/15/94 11:30:54
! +
!
!  PROCEDURE mosinit
!  EXECUTE THE COMMAND
!  INIT/MOS [NAME]
!
! -
IF "'P1(1:1)'" .EQ. "?" THEN
   WRITE/OUT " "
   WRITE/OUT "Initializing key words"
   WRITE/OUT " "
   REGISTER/SESSION MOS STD_PROC mosinit.prg mostab.tbl
!Begin Session List
   WRITE/KEYW ALPHA/R/1/1  0.2   ! Rejection parameter for lines matching [0,.5]
   WRITE/KEYW AVDISP/R/1/1    0.  ! Average dispersion per pixel
   WRITE/KEYW CAL/I/1/100  0  ALL ! Results of moscalib (+1 dispersion relation 
                                  ! fitted, -1 no dispersion relation fitted)
   WRITE/KEYW CALOBJEC/C/1/40 "?" ! root name for table derived from extracted 
                                  ! object frame (APPLY/MOS)
   WRITE/KEYW CCDPAR/C/1/60 " "   ! read-out-noise and conversion factor of CCD 
   WRITE/KEYW CENTMET/C/1/16 "GRAVITY" ! centering method for calibration lines 
   WRITE/KEYW DISP/I/1/1 0.       ! amount of displayed information for
                                  ! CALIB/MOS
   WRITE/KEYW EXTOBJEC/C/1/60 "?" ! extracted object frame 
   WRITE/KEYW EXTPAR/C/1/60 " "   ! extraction parameters (order, iter)
   WRITE/KEYW FFORD/I/1/1     3   ! order of fit for the FF normalization
   WRITE/KEYW FLAT/C/1/60 "flat"  ! flat-field 
   WRITE/KEYW FLATLIM/R/1/3 0 all ! parameters for slitlet location  
   WRITE/KEYW FORS01/D/1/2 0,0    ! Central wavelength and average 
                                  ! dispersion (A/pix) of grism fors01
   WRITE/KEYW GRISM/I/1/1 1       ! No. of grism used 
   WRITE/KEYW INT_LIM/D/1/1 0.001 ! fraction of central intensity where object
                                  ! limits shall be defined
   WRITE/KEYW ISLIT/I/1/1   0     ! counter for slitlets
   WRITE/KEYW LID/D/1/50   0. ALL ! Positions of identified lines (Angstroems)
   WRITE/KEYW LINECAT/C/1/60 "hear"  ! table with wavelength positions 
   WRITE/KEYW LINFIT/C/1/60 "coerbr" ! table with dispersion coefficients  
   WRITE/KEYW LINPOS/C/1/60 "linpos" ! table with wavelength positions 
   WRITE/KEYW MAXDEV/R/1/1    10. ! Maximum deviation (pixels)
   WRITE/KEYW MIN_DIST/I/1/1  3   ! Minimum distance between object/sky 
   WRITE/KEYW MIN_SKY/I/1/1   5   ! minimum number of rows for sky
   WRITE/KEYW MOS/C/1/60 "mos"    ! table with slitlets' positions 
   WRITE/KEYW NOBJ/I/1/1 0        ! number of objects found by define/mos
   WRITE/KEYW NORMFLAT/C/1/60 "normflat" ! normalized flat-field 
   WRITE/KEYW NORMMET/C/1/20 POLY ! method for FF normalization
   WRITE/KEYW NPIX/I/1/2 0,0      ! size of raw frame 
   WRITE/KEYW NSKY/I/1/1 0        ! number of sky-regions found by define/mos
   WRITE/KEYW NSLIT/I/1/1   0     ! total number of slitlets
   WRITE/KEYW OBJ/C/1/60 "?"      ! object frame 
   WRITE/KEYW PLOTC/C/1/1  N      ! Plot residuals of wavelength calibration
   WRITE/KEYW POLTYP/C/1/10 CHEBYSHEV ! type of polynomials used for fitting
                                  ! (Legendre or Chebyshev)
   WRITE/KEYW REBEND/D/1/1   0.   ! Final wavelength for rebinning
   WRITE/KEYW REBMET/C/1/12 LINEAR ! Rebinning method (LINEAR, QUADRATIC, SPLINE)
   WRITE/KEYW REBSTP/D/1/1   0.   ! Wavelength step for rebinning
   WRITE/KEYW REBSTRT/D/1/1  0.   ! Starting wavelength for rebinning
   WRITE/KEYW REJTHRES/R/1/1 3.   ! rejection threshold
   WRITE/KEYW SCAN_POS/D/1/1 0. ! center and width for scan (locate/mos,define/mos) 
				  !in world coordinates
   WRITE/KEYW SEAPAR/C/1/20 200,5 ! detection threshold and width 
   WRITE/KEYW SHIFTTOL/I/1/1 10   ! tolerance for line identification from one
                                  ! slitlet to next (mode ID)
   WRITE/KEYW SKYFRAME/C/1/60 "sky" ! sky  frame 
   WRITE/KEYW SKYMET/C/1/16 " " ALL ! method used to fit sky  
   WRITE/KEYW SKYPAR/I/1/6 0 ALL  ! parameters for sky-subtraction 
   WRITE/KEYW START/D/1/2 0.,0.   ! start values of raw frame 
   WRITE/KEYW STEP/D/1/2 0.,0.    ! step-size of raw frame 
   WRITE/KEYW THRESH/R/1/1 -0.04  ! detection threshold for object search
   WRITE/KEYW TOL/R/1/1     2     ! tolerance for autom. wavel. ident.
   WRITE/KEYW TOLWIND/I/1/1 4     ! tolerance (pix) for interact. wavel. ident.
   WRITE/KEYW TUTO/C/1/1 N        !  TUTORIAL (Y/N)
   WRITE/KEYW WCENTER/D/1/1   0.  ! Central wavelength 
   WRITE/KEYW WIND/I/1/1        5 ! detection window for object search 
   WRITE/KEYW WINDOWS/C/1/60 "windows" ! table with sky and objects positions 
   WRITE/KEYW WLC/C/1/80 "wlc "    ! calibration frame  
   WRITE/KEYW WLCMET/C/1/2  FC    ! Wavelength calibration method (Ident/Linear/
                                  ! Fors) and (Constant/Variable/Fit)
   WRITE/KEYW WLCNITER/I/1/2 3,20 ! Minimum, Maximum number of iterations
   WRITE/KEYW WLCORD/I/1/2 2,1    ! order of fit used to compute the dispersion 
   WRITE/KEYW XBIN/I/1/1     20   ! Binning in X for object search
   WRITE/KEYW XPOS/D/1/50  0. ALL ! Positions of identified lines (pixels)
   WRITE/KEYW YBIN/C/1/20    3,3  ! Step and Binning in Y
   WRITE/KEYW YSTART/I/1/1  0     ! Starting row for calibration (pixel value)
!End Session List
   WRITE/KEYW INPUTI/I/1/10 0 ALL  ! keyword for integer input
   WRITE/KEYW INPUTR/R/1/10 0. ALL ! keyword for real input
   WRITE/KEYW INPUTD/D/1/10 0. ALL ! keyword for double input
ELSE
   SAVINIT/MOS  {P1}SLIT.tbl READ
   COPY/TABLE   {P1}SLIT.tbl {MOS}
   COPY/TABLE   {P1}LIN.tbl  {LINPOS}
   COPY/TABLE   {P1}FIT.tbl  {LINFIT}
   COPY/TABLE   {P1}WIN.tbl  {WINDOWS}
ENDIF
