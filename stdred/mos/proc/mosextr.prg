! @(#)mosextr.prg	8.3 (ESO-IPG) 11/23/94 12:21:05
! @(#)mosextr.prg	1.3 (ESO-IPG) 2/11/94 13:19:57
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSEXTR.PRG
!
! .PURPOSE
!
! execute the command :
! NORM/MOS image params
!
! -------------------------------------------------
!
CROSSREF OBJ SKY TABLE EXTOBJ EXTPAR CCDPAR REJECT
!
DEFINE/PARAM P1 {OBJ}      IMAGE   "Enter input image:"
DEFINE/PARAM P2 {SKYFRAME} IMAGE   "Enter sky   image:"
DEFINE/PARAM P3 {WINDOWS}  TABLE   "Enter input table:"
DEFINE/PARAM P4 {EXTOBJEC} IMAGE   "Enter output image:"
DEFINE/PARAM P5 {EXTPAR}   ?       "Order of fit, Number of iterations:"
DEFINE/PARAM P6 {CCDPAR}   ?       "Ron, Gain:"
DEFINE/PARAM P7 {REJTHRES} ?       "Rejection Threshold:"
!
!SET/MOS obj={P1} mos={P2} extobjec={P3} 
!
WRITE/KEYW IN_A {P1}
WRITE/KEYW IN_B {P2}
WRITE/KEYW INPUTC {P3}
WRITE/KEYW OUT_A {P4}
WRITE/KEYW INPUTI {P5}
WRITE/KEYW INPUTR {P6},{P7}
!
RUN STD_EXE:MOSEXT
!
COPY/DD {P1} *,3 {P4}
