! @(#)mosskyex.prg	1.3 (ESO-IPG) 2/11/94 13:20:01
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSSKYEX.PRG
!
! .PURPOSE
!
! execute the command :
!  SKYEX/MOS image mos-table windows-table out par mode linfit-table reb_obj
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
CROSSREF OBJECT TABLE SKYFRAME SKYPAR
!
DEFINE/PARAM P1 {OBJECT}   IMAGE  "Enter input image:"
DEFINE/PARAM P2 {MOS}      TABLE  "Enter input mos table:"
DEFINE/PARAM P3 {WINDOWS}  TABLE  "Enter input windows table:"
DEFINE/PARAM P4 {LINFIT}   TABLE  "Enter table with dispersion coefficients:"
DEFINE/PARAM P5 {SKYPAR}   NUMBER "Enter Sky parameters:"
DEFINE/PARAM P6 1          NUMBER "Mode:"
DEFINE/PARAM P7 {EXTPAR}   NUMBER "Enter extraction parameters (order, iter):"
!
!------------------------------------------------------------------------------
! 
! Local variables
!
!------------------------------------------------------------------------------
DEFINE/LOCAL FITD/I/1/1 0
!
!------------------------------------------------------------------------------
! 
! Sky Fit
!
!------------------------------------------------------------------------------
!
WRITE/KEYW IN_A {P1}
WRITE/KEYW IN_B {P2}
!
if {p6} .eq. 0 then
   cop/table {mos} middummz
   name/column middummz :SLIT :SKY_SLIT
   name/column middummz :YSTART :SKY_STRT
   name/column middummz :YEND   :SKY_END
   write/keyw IN_C/C/1/60 middummz
else	
   WRITE/KEYW IN_C/C/1/60 {P3}
endif
!
WRITE/KEYW OUT_A SKY{P1}
WRITE/KEY  SKYFRAME {OUT_A}
WRITE/KEYW INPUTI {P5}
comp S{P1} = {P1}-{SKYFRAME}
!
RUN STD_EXE:MOSSKY
!
!------------------------------------------------------------------------------
! 
! Extraction of objects
!
!------------------------------------------------------------------------------
WRITE/KEYW IN_A S{P1}
WRITE/KEYW IN_B {SKYFRAME}
WRITE/KEYW INPUTC {P3}
WRITE/KEYW OUT_A EXT{P1}
WRITE/KEYW INPUTI {EXTPAR}
WRITE/KEYW INPUTR {CCDPAR},{REJTHRES}
!
RUN STD_EXE:MOSEXT
!
COPY/DD {P1} *,3 EXT{P1}
!
!------------------------------------------------------------------------------
! 
! Rebinning of 1-dimensional extracted spectra
!
!------------------------------------------------------------------------------
WRITE/KEYW   INPUTR {REBSTRT},{REBEND},{REBSTP}
VERIFY/MOS  {LINFIT} TAB
VERIFY/MOS  EXT{P1} IMA
!
!------------------------------------------------------------------------------
!
! get numbers for rebinning method
!
!------------------------------------------------------------------------------
INPUTI(1) = -1
IF REBMET(1:1) .EQ. "L"  WRITE/KEYW INPUTI/I/1/1   0    ! Method LINEAR
IF REBMET(1:1) .EQ. "Q"  WRITE/KEYW INPUTI/I/1/1   1    ! Method QUADRATIC
IF REBMET(1:1) .EQ. "S"  WRITE/KEYW INPUTI/I/1/1   2    ! Method SPLINE
!
IF INPUTI(1) .LT. 0  THEN
   WRITE/OUT "Rebin: Unknown method {REBMET}"
   RETURN/EXIT
ENDIF
!
COPY/KD INPUTR/R/1/3 {LINFIT}.tbl REBPAR/R/1/3
!------------------------------------------------------------------------------
!
! do polynomial regression for all coefficients
!
!------------------------------------------------------------------------------
! (to be done, Otmar Stahl)
!
COPY/DK    {LINFIT}.tbl LNCOE FITD
!
WRITE/KEYW  IN_A         EXT{P1}
WRITE/KEYW  IN_B         {P4} 
WRITE/KEYW  OUT_A        REB{P1}
RUN STD_EXE:mosrebin1d
!------------------------------------------------------------------------------
WRITE/OUT  "Calibration Performed."
!
