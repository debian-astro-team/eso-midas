! @(#)mosident.prg	10.1 (ESO-IPG) 8/7/95 17:33:31
! @(#)mosid.prg	5.1.1.3  (LSW)  02/07/94 15:56:13
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 Landessternwarte Heidelberg  
!.IDENT       mosident.prg
!.AUTHORS     Sabine Moehler   (LSW)
!             
!.KEYWORDS    Spectroscopy, Long-Slit, MOS
!.PURPOSE     line identification 
!.VERSION     1.0  Creation          20-AUG-1993   
!-------------------------------------------------------
!
CROSSREF wlc ystart linpos tolwindow 
!
DEFINE/PARAM  P1  {WLC}               IMA    "WLC image "
DEFINE/PARAM  P2  {YSTART}            NUMB   "Y Coordinate "
DEFINE/PARAM  P3  {LINPOS}            TAB    "table with line positions "
DEFINE/PARAM  P4  {TOLWIND}           NUMB   "tolerance window "
DEFINE/LOCAL  VALROW/I/1/1 -1
DEFINE/LOCAL  SIGNAL/I/1/1 0
DEFINE/LOCAL  OLDPOS/R/1/2 -1,-1 
DEFINE/LOCAL  I/I/1/1 0
DEFINE/LOCAL  NUMROW/I/1/1 0
!
VERIFY/MOS {LINPOS} TAB
VERIFY/MOS {WLC} IMA
!-------------------------------------------------------
! Initialize keywords
!-------------------------------------------------------
WRITE/KEYW XPOS 0 ALL
WRITE/KEYW LID 0 ALL

I = {P2}
!-------------------------------------------------------
! find CCD line where emission lines shall be identified
!-------------------------------------------------------
TEST_TBL:
  SELECT/TABLE {P3} :Y.EQ.{I}
  IF OUTPUTI(1) .EQ. 0 THEN
    WRITE/OUT "line" {I} " has not been scanned. We try the next line"
    I = I+1
    GOTO TEST_TBL 
  ENDIF
!-------------------------------------------------------
! craete temporary table
!-------------------------------------------------------
  COPY/TABLE {P3} MIDTMP
  PLOT/ROW {P1} {I}
  INPUTI(1) = {I} 
  WRITE/KEYW YSTART {INPUTI(1)}
  INPUTR(1) = {P4} 
!-------------------------------------------------------
! start identification
!-------------------------------------------------------
identify/gcurs MIDTMP :IDENT :X ? {P4}
!-------------------------------------------------------
! finish and delete temporary tables
!-------------------------------------------------------
WRITE/KEYW IN_A MIDTMP
RUN STD_EXE:mosget_id
sele/table {LINPOS} all
delete/table MIDTMP no
