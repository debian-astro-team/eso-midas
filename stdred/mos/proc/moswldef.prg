! @(#)moswldef.prg	1.7 (ESO-IPG) 2/17/94 17:28:46
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSWLDEF.PRG
!
! .PURPOSE
!
! execute the command :
! WLDEF/MOS image params
!
! -------------------------------------------------
!
CROSSREF WLC SEAPAR YBIN METHOD TOL DEG MTD PLO 
!
!
DEFINE/PARAM P1 {WLC}      IMAGE   "Enter input image:"
DEFINE/PARAM P2 {SEAPAR}   NUMBER  "Threshold, window:"
DEFINE/PARAM P3 {YBIN}     NUMBER  "Step, binning in Y:"
DEFINE/PARAM P4 {CENTMET}  ?       "Centering method"
!
DEFINE/PARAM P5 {TOL}                  NUMB   "Tolerance "
DEFINE/PARAM P6 {WLCORD(1)}            NUMB   "Degree     "
DEFINE/PARAM P7 {WLCMET}               CHAR   "Mode (I[dent]/L[inear]/F[ors])(C[onst.]/V[ar.]/F[it]) (Default: FC) :"
DEFINE/PARAM P8 {PLOTC}                CHAR   "Plot of residuals (Y/N):"
!-------------------------------------------------------
! Local parameters
!-------------------------------------------------------
DEFINE/LOCAL I/I/1/1 0
DEFINE/LOCAL KEYSEL/C/1/64 - 
DEFINE/LOCAL COUNT/I/1/1 0.
DEFINE/LOCAL DISP/I/1/1 0.
DEFINE/MAXPAR 8
!
!-------------------------------------------------------
! Line search
!-------------------------------------------------------
! SET/MOS wlc={P1} seapar={P2} ybin={P3} centmet={P4}  
!
NPIX(1) = {{P1},NPIX(1)}
IF {{P1},NAXIS} .GT. 1 NPIX(2) = {{P1},NPIX(2)}
!
WRITE/KEYW IN_A   {P1}
WRITE/KEYW IN_B   {mos}
WRITE/KEYW OUT_A  {linpos}
WRITE/KEYW INPUTI {P2},{P3}
WRITE/KEYW INPUTC {P4}
!
RUN STD_EXE:MOSSLIN
!
!-------------------------------------------------------
! Line calibration
!-------------------------------------------------------
!-------------------------------------------------------
! check if linpos and linecat are tables
!-------------------------------------------------------
VERIFY/MOS {LINPOS} TAB
VERIFY/MOS {LINECAT} TAB
!
!-------------------------------------------------------
! get parameters
!-------------------------------------------------------
!WRITE/KEYW TOL    {P5}
!WRITE/KEYW WLCMET {P7}
!WRITE/KEYW WLCORD {P6} 
!WRITE/KEYW PLOTC  {P8}
WRITE/KEYW DISP   50
!
WRITE/KEYW IN_A   {LINPOS}
WRITE/KEYW IN_B   {LINECAT}
WRITE/KEYW INPUTC {P7}
!WRITE/KEYW INPUTC {WLCMET}
WRITE/KEYW OUT_A  {LINFIT}
!
INPUTR(1) = ALPHA
INPUTR(2) = MAXDEV
!INPUTR(3) = TOL
WRITE/KEY INPUTR/R/3/3 {P5}
!
INPUTI(10) = DISP
!
COPY/DK {wlc} START/D/1/2 START/D/1/2
COPY/DK {wlc} STEP/D/1/2 STEP/D/1/2
COPY/KEYW  START/D/1/2   INPUTD/D/1/2  
COPY/KEYW  STEP/D/1/2    INPUTD/D/3/4  
!-------------------------------------------------------
! set integer parameters
!-------------------------------------------------------
WRITE/KEY INPUTI/I/1/1 {P6}
!INPUTI(1) = WLCORD(1)
!INPUTI(2) = WLCORD(2)
INPUTI(2) = NPIX(1)+START(1) ! total number of pixels along dispersion axis
INPUTI(3) = WLCNITER(1)
INPUTI(4) = WLCNITER(2)
INPUTI(5) = YSTART
!-------------------------------------------------------
! Linear Fit
!-------------------------------------------------------
IF INPUTC(1:1) .EQ. "L" THEN

  IF NPIX(1) .EQ. 0 THEN
    COPY/DK {WLC} NPIX NPIX/I/1/2 
  ENDIF
  IF NPIX(1) .EQ. 0 THEN
    WRITE/OUT "No wavelength calibration frame defined - frame size unknown!"
    GOTO END
  ENDIF
  
  DEFINE/LOCAL WDEF/D/1/3  {WCENTER},{AVDISP},0.
  INPUTD(5) = WDEF(1) - (NPIX(1)+START(1)-1)/2.*WDEF(2) ! Coeffs in world 
  INPUTD(6) = WDEF(2)                                   ! coordinates system
  INPUTD(7) = WDEF(3)
ENDIF
!
!-------------------------------------------------------
! FORS Fit 
!-------------------------------------------------------
IF INPUTC(1:1) .EQ. "F" THEN
SET/FORMAT I2   

  IF GRISM .EQ. 0 THEN
    WRITE/OUT "No grism defined!"
    GOTO END
  ENDIF
  IF NPIX(1) .EQ. 0 THEN
    COPY/DK {WLC} NPIX NPIX/I/1/2 
  ENDIF
  IF NPIX(1) .EQ. 0 THEN
    WRITE/OUT "No wavelength calibration frame defined - frame size unknown!"
    GOTO END
  ENDIF

  INPUTD(5) = FORS{GRISM}(1) - (NPIX(1)+START(1)-1)/2.*FORS{GRISM}(2) ! Coeffs
  INPUTD(6) = FORS{GRISM}(2)                      ! in world coordinates system
  INPUTD(7) = 0.
ENDIF
!
RUN STD_EXE:moscalib
!
!-------------------------------------------------------
! Plot Option
!-------------------------------------------------------
COPY/DK {linpos}.tbl TSELTABL KEYSEL
!IF PLOTC(1:1) .eq. "Y" THEN
IF P8(1:1) .eq. "Y" THEN
 DO I = 1 {NSLIT} 
   IF CAL({I}:{I}) .gt. 0 THEN
     RESPLOT/MOS {LINPOS} {I}  
   ELSEIF CAL({I}:{I}) .lt. 0 THEN
     WRITE/OUT "No dispersion relation for slitlet no. {I}"
   ENDIF
 ENDDO
ELSE 
 GOTO END
ENDIF
END:
IF KEYSEL(1:1) .eq. "-" THEN
  SELECT/TABLE {linpos} all
ELSE
  COPY/KD KEYSEL/c/1/64 {linpos}.tbl TSELTABL/c/1/64
ENDIF
