! @(#)mosdefwin.prg	8.2 (ESO-IPG) 11/18/94 09:21:20
! @(#)mosdefwin.prg	8.2 (ESO-IPG) 11/18/94 09:21:20
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSSLIN.PRG
!
! .PURPOSE
!
! execute the command :
! SEARCH/MOS image params
!
! -------------------------------------------------
!
DEFINE/PARAM P1  READ    CHAR   "Mode (INIT/ADD/DELETE/LOAD/READ): "
DEFINE/PARAM P2   +      NUMB   "Sequence Number:"
DEFINE/PARAM P3   +      NUMB   "Object Limits:"
DEFINE/PARAM P4   +      NUMB   "Sky Limits:"
DEFINE/PARAM P5  mos     TAB    "MOS Slits Table:"
DEFINE/PARAM P6  window  TAB    "MOS Windows Table:"
DEFINE/PARAM P8   *      IMA    "Displayed image:"
!
IF P1(1:1) .EQ. "I" THEN 
  @s mosdefwin,init    {P6}
  copy/tt {P5} :slit   {P6} :Obj_Slit
  copy/tt {P5} :slit   {P6} :Sky_Slit
  copy/tt {P5} :ystart {P6} :Sky_Strt
  copy/tt {P5} :yend   {P6} :Sky_End
  read/table {P6}
ENDIF
!
IF P1(1:1) .EQ. "R" THEN
   READ/TABLE {P6}
ENDIF
!
IF P1(1:1) .EQ. "A" THEN
   define/local object/I/1/2 {P3}
   WRITE/TABLE {P6} @{P2} :Obj_Strt {object(1)}
   WRITE/TABLE {P6} @{P2} :Obj_End  {object(2)}
   WRITE/TABLE {P6} @{P2} :Obj_Slit   {P2}        
   WRITE/TABLE {P6} @{P2} :Sky_Slit   {P2}         
   IF P4(1:1) .NE. "+"  THEN
       define/local sky/I/1/2   {P4}
       WRITE/TABLE {P6} @{P2} :Sky_Strt {sky(1)}
       WRITE/TABLE {P6} @{P2} :Sky_End  {sky(2)}
   ELSE 
       select/table {P6} :Sky_Strt.EQ.NULL.OR.:Sky_End.EQ.NULL >>middummn.tbl
       if outputi(1) .ne. 0 then
          write/out "   "
          write/out "****** Error: Sky Limits must be defined. *****"
          write/out "   "
          read/table {P6}
       else
          select/table {P6} all
       endif
   ENDIF
ENDIF
!
IF P1(1:1) .EQ. "D" THEN
   select/table {P6} sequence.ne.{P2}
   copy/table   {P6} &w
   COPY/TABLE   &w   {P6}
   READ/TABLE   {P6}
ENDIF
!
IF P1(1:1) .EQ. "L" THEN

write/out "Sky limits represented in green..."
write/out "Object limits represented in blue..."
copy/table {p6} &a
copy/table {p6} &b
comp/table &a :x_pos = {{idimemc},start(1)}
comp/table &b :x_pos = {{idimemc},start(1)}+({{idimemc},step(1)}*{{idimemc},npix(1)})
merge/table &a &b &c
sort/table &c :sky_strt
clear/chan over
load/table &c :x_pos :sky_strt :sky_slit -1 ? 4
load/table &c :x_pos :sky_end :sky_slit -1 ? 4
sort/table &c :obj_strt
load/table &c :x_pos :obj_strt :obj_slit -1 ? 5
load/table &c :x_pos :obj_end :obj_slit -1 ? 5

ENDIF
!
ENTRY INIT
!
DEFINE/PARAM P1 ? TAB "Name:"
!
CREATE/TABLE  {P1}  10  100
!
CREATE/COLUMN {P1} :Obj_Slit  F6.0   R*4
CREATE/COLUMN {P1} :Obj_Strt  F10.2  R*4
CREATE/COLUMN {P1} :Obj_End   F10.2  R*4
CREATE/COLUMN {P1} :Sky_Strt  F10.2  R*4
CREATE/COLUMN {P1} :Sky_End   F10.2  R*4 
CREATE/COLUMN {P1} :Sky_Slit  F6.0   R*4
!
RETURN



