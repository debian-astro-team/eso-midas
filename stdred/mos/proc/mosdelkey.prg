! @(#)mosdelkey.prg	8.5 (ESO-IPG) 03/24/95 11:10:29
! @(#)mosdelkey.prg	8.5 (ESO-IPG) 03/24/95 11:10:29
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  mosdelkey.prg
! .PURPOSE
! delete keywords from context mos
! .AUTHOR  Klaus Banse   ESO - Garching
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
delete/keyw FLATLIM,fford,normmet,seapar,ybin,thresh,wind,xbin
delete/keyw YSTART,wlcmtd,poltyp,tol,tolwind,wlcord
delete/keyw ALPHA,wlcniter,maxdev,wcenter
delete/keyw AVDISP,xpos,lid,plotc,cal
delete/KEYW REBMET,rebstrt,rebend,rebstp,size,skymet
delete/KEYW SKYPAR,step,start,npix,scan_pos
delete/KEYW INT_LIM,nobj,nsky,centmet,linmet,defmod
delete/KEYW EXMODE,rebmet,object,extobjec,calobjec,extpar
delete/KEYW REJTHRES,skyframe,wlc,flat,normflat,cattab
delete/KEYW MOS,linfit,linpos,linecat,windows,standard,ccdpar
delete/KEYW GRISM,fors01
delete/KEYW ISLIT,nslit
