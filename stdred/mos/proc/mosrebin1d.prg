! @(#)mosrebin1d.prg	8.4 (ESO-IPG) 02/17/95 14:41:35
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       mosrebin.prg
!.AUTHORS     Sabine Moehler   (LSW)
!.KEYWORDS    MOS
!.PURPOSE     
!.VERSION     1.0  Package Creation  17-MAR-1993  
!             1.1  adapted for MOS   20-AUG-1993   
!-------------------------------------------------------
!
! CROSSREF tol deg mtd guess
!
DEFINE/PARAM  P1  {EXTOBJEC}     IMA "Enter image to be rebinned " 
DEFINE/PARAM  P2  ?              IMA "Enter result image (root name) " 
DEFINE/PARAM  P3 {REBSTRT},{REBEND},{REBSTP} N "Start, End, Step in wavelength:"
!
DEFINE/PARAM  P4  {REBMET}        CHA "Rebin method (SPLINE/QUADRATIC,LINEAR) :"
DEFINE/PARAM  P5  {LINFIT}        CHA "Table with dispersion coefficients: "
!------------------------------------------------------------------------------
! 
! Local variables
!
!------------------------------------------------------------------------------
DEFINE/LOCAL FITD/I/1/1 0
!
WRITE/KEYW   INPUTR {REBSTRT},{REBEND},{REBSTP}
VERIFY/MOS  {LINFIT} TAB
VERIFY/MOS  {P1} IMA
!
WRITE/KEYW INPUTR/R/1/3 {P3}
!
!SET/FORS REBMET={P4} LINFIT={P5} REBSTRT={INPUTR(1)}
!SET/FORS REBEND={INPUTR(2)} REBSTP={INPUTR(3)} MOS={P6}
!
!------------------------------------------------------------------------------
!
! get numbers for rebinning method
!
!------------------------------------------------------------------------------
INPUTI(1) = -1
IF P4(1:1) .EQ. "L"  WRITE/KEYW INPUTI/I/1/1   0    ! Method LINEAR
IF P4(1:1) .EQ. "Q"  WRITE/KEYW INPUTI/I/1/1   1    ! Method QUADRATIC
IF P4(1:1) .EQ. "S"  WRITE/KEYW INPUTI/I/1/1   2    ! Method SPLINE
!
IF INPUTI(1) .LT. 0  THEN
   WRITE/OUT "Rebin: Unknown method {P4}"
   RETURN/EXIT
ENDIF
!
COPY/KD INPUTR/R/1/3 {LINFIT}.tbl REBPAR/R/1/3
!------------------------------------------------------------------------------
!
! do polynomial regression for all coefficients
!
!------------------------------------------------------------------------------
! (to be done, Otmar Stahl)
!
COPY/DK    {LINFIT}.tbl LNCOE FITD
!
WRITE/KEYW  IN_A         {P1}
WRITE/KEYW  IN_B         {P5} 
WRITE/KEYW  OUT_A        {P2}
RUN STD_EXE:mosrebin1d
!------------------------------------------------------------------------------
WRITE/OUT  "Calibration Performed."
!
