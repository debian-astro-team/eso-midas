! @(#)mosrebin.prg	8.6 (ESO-IPG) 05/07/96 10:50:15
! @(#)mosrebin.prg	1.4 (ESO-IPG) 2/11/94 13:20:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       mosrebin.prg
!.AUTHORS     Sabine Moehler   (LSW)
!.KEYWORDS    MOS
!.PURPOSE     
!.VERSION     1.0  Package Creation  17-MAR-1993  
!             1.1  adapted for MOS   20-AUG-1993   
!-------------------------------------------------------
!
! CROSSREF tol deg mtd 
!
DEFINE/PARAM  P1 {OBJ}            IMA "Enter image to be rebinned" 
DEFINE/PARAM  P2  ?               IMA "Enter result image" 
DEFINE/PARAM  P3 {REBSTRT},{REBEND},{REBSTP} N "Start, End, Step in wavelength:"
DEFINE/PARAM  P4  {REBMET}        CHA "Rebin method (SPLINE/QUADRATIC,LINEAR) :"
DEFINE/PARAM  P5  {LINFIT}        CHA "Table with dispersion coefficients: "
DEFINE/PARAM  P6  {MOS}           CHA "Table with slitlet positions: "
!DEFINE/PARAM  P7  {WLCORD(2)}     NUM "order of interpolation between rows: "
!------------------------------------------------------------------------------
! 
! Local variables
!
!------------------------------------------------------------------------------
DEFINE/LOCAL FITD/I/1/1 0
!
VERIFY/MOS {P1} IMA
VERIFY/MOS {P5} TAB
VERIFY/MOS {P6} TAB
!
WRITE/KEYW  IN_A          {P1}
WRITE/KEYW  OUT_A         {P2}
WRITE/KEYW  INPUTR/R/1/3  {P3}
WRITE/KEYW  INPUTC        {P4}
WRITE/KEYW  IN_B          {P5} 
WRITE/KEYW  IN_C/C/1/60   {P6} 
!------------------------------------------------------------------------------
!
! get numbers for rebinning method
!
!------------------------------------------------------------------------------
INPUTI(1) = -1
IF P4(1:1) .EQ. "L"  WRITE/KEYW INPUTI/I/1/1   0    ! Method LINEAR
IF P4(1:1) .EQ. "Q"  WRITE/KEYW INPUTI/I/1/1   1    ! Method QUADRATIC
IF P4(1:1) .EQ. "S"  WRITE/KEYW INPUTI/I/1/1   2    ! Method SPLINE
IF INPUTI(1) .LT. 0  THEN
   WRITE/OUT "Rebin: Unknown method {P4}"
   RETURN/EXIT
ENDIF
!
COPY/KD INPUTR/R/1/3 {IN_B}.tbl REBPAR/R/1/3
!------------------------------------------------------------------------------
!
! do polynomial regression for all coefficients
!
!------------------------------------------------------------------------------
!
! (to be done, Otmar Stahl)
!
!
COPY/DK    {IN_B}.tbl LNCOE FITD
!
!RUN STD_EXE:sprebin
RUN STD_EXE:mosrebin
!------------------------------------------------------------------------------
WRITE/OUT  "Calibration Performed."
!



