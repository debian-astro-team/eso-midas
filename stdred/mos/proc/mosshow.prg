! @(#)mosshow.prg	1.2 (ESO-IPG) 2/11/94 13:20:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       mosshow.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle, 
!.PURPOSE     Display echelle session parameters
!.VERSION     1.0    Creation    21-AUG-1991  PB
!
!-------------------------------------------------------
!
DEFINE/PARAM P1  ALL  CHAR  "Selection (GEN, WLC, FLAT, EXTR, INST, REDU) : "
SESSLINE = 0 ! Initialize the line counter
!
!ECHO/OFF
SET/FORMAT I1 F12.3,F12.3
!
SHOW/SESS  "****************  MOS Parameters  *******************"
IF P1(1:1) .EQ. "C"  GOTO CALIB
IF P1(1:1) .EQ. "D"  GOTO WIND
IF P1(1:1) .EQ. "E"  GOTO EXTR
IF P1(1:1) .EQ. "F"  GOTO FF
IF P1(1:1) .EQ. "G"  GOTO GEN
IF P1(1:1) .EQ. "R"  GOTO REBI
IF P1(1:1) .EQ. "S"  GOTO SKY
IF P1(1:1) .EQ. "W"  GOTO WLC
GEN:
SHOW/SESS  "   "
SHOW/SESS  "General parameters   "
SHOW/SESS  "   "
SHOW/SESS  "Object frame (OBJ):                                {OBJ}"
SHOW/SESS  "Number of pixels (NPIX):                           {NPIX}   "
SHOW/SESS  "Step size (STEP):                                  {STEP} "
SHOW/SESS  "Start values (START):                              {START} "
SHOW/SESS  "CCD read-out-noise and conversion factor (CCDPAR): {CCDPAR} "
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS  "-------------------------------------------------------------------"
FF:
SHOW/SESS "Flat field related: LOCATE/MOS - NORMAL/MOS - FLAT/MOS - PLOT/LOCATE"
SHOW/SESS  "   "
SHOW/SESS  "FF frame (FLAT):                   {FLAT}" 
SHOW/SESS  "Detec. thres. (FLATLIM):           {FLATLIM(1)},{FLATLIM(2)},{FLATLIM(3)} "
SHOW/SESS  "Table MOS (MOS):                   {MOS}"
SHOW/SESS  "Number of slitlets (NSLIT):        {NSLIT}"
SHOW/SESS  "Method for smoothing (NORMMET):    {NORMMET}"
SHOW/SESS  "Order of fit or med-wind. (FFORD): {FFORD}"
SHOW/SESS  "Norm. FF (NORMFLAT):               {NORMFLAT}" 
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS  "-------------------------------------------------------------------"
WLC:
SHOW/SESS  "Line-search related: SEARCH/MOS - LINPLOT/MOS" 
SHOW/SESS  "                            "
SHOW/SESS  "WLC image (WLC):                  {WLC}" 
SHOW/SESS  "Center method (CENTMET):          {CENTMET}" 
SHOW/SESS  "Thres. and window (SEAPAR):       {SEAPAR}" 
SHOW/SESS  "Step and Binning in Y (YBIN):     {YBIN}" 
SHOW/SESS  "Table with lines (LINPOS):        {LINPOS}" 
SHOW/SESS  "                            "
SHOW/SESS  "Line-identification related: IDENTIFY/MOS"
SHOW/SESS  "                            "
SHOW/SESS  "Line identifications (LID):                            {LID}"
SHOW/SESS  "Pixel positions of ident. lines (XPOS):                {XPOS}"
SHOW/SESS  "CCD row in which the lines are identified (YSTART):    {YSTART}"
SHOW/SESS  "Tolerance for interact. line identification (TOLWIND): {TOLWIND}"
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS  "-------------------------------------------------------------------"
CALIB:
SHOW/SESS  "Line calibration related: CALIBRATE/MOS - RESPLOT/MOS" 
SHOW/SESS  "                            "
SHOW/SESS  "Wavelength calibration method (WLCMET):                  {WLCMET}"
SHOW/SESS  "Polynomials for dispersion relation (POLTYP):            {POLTYP}"
SHOW/SESS  "Order of fit for dispersion relation (WLCORD(1)):        {WLCORD(1)}"
SHOW/SESS "Minimum/maximum number of iterations (WLCNITER):  {WLCNITER(1)},{WLCNITER(2)}"
SHOW/SESS  "Central wavelength (WCENTER):                            {WCENTER}"
SHOW/SESS  "Average dispersion (A/pix) (AVDISP):                     {AVDISP}"
SHOW/SESS  "Table with fit coefficients (LINFIT):                    {LINFIT}"
SHOW/SESS  "Table with wavelength catalog (LINECAT):                 {LINECAT}"
SHOW/SESS  "Grism number (GRISM):                                    {GRISM}"
SHOW/SESS  "Tolerance for automatic line identification (TOL):       {TOL}"
SHOW/SESS  "Plot flag for line residuals (PLOTC):                    {PLOTC}"
SHOW/SESS  "Flag for dispersion relations for slitlets (CAL):        {CAL} "
SHOW/SESS  "Matching parameter (0 to 0.5) (ALPHA):                   {ALPHA}"
SHOW/SESS  "Maximal Deviation (MAXDEV):                              {MAXDEV}"
SHOW/SESS  "Slitlet counter (ISLIT):                                 {ISLIT}"
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS  "-------------------------------------------------------------------"
REBI:
SHOW/SESS  "Rebin related: REBIN/MOS - REB1D/MOS              "
SHOW/SESS  "                            "
SHOW/SESS  "Method of interpolation along dispersion axis (REBMET):  {REBMET}"
SHOW/SESS  "Order of interpolation along y-axis (WLCORD(2)):         {WLCORD(2)}"
!SHOW/SESS  "Start, Step, End (REBSTRT, REBSTP, REBEND):             {REBSTRT},{REBSTP},{REBEND}"
SHOW/SESS  "Start (REBSTRT):                                         {REBSTRT}"
SHOW/SESS  "Step (REBSTP):                                           {REBSTP}"
SHOW/SESS  "End (REBEND):                                            {REBEND}"
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS  "-------------------------------------------------------------------"
WIND:
SHOW/SESS  "Object and sky search related: DEFINE/MOS - DEFWIN/MOS"
SHOW/SESS  "                            "
SHOW/SESS  "Center for scan in world coord. (SCAN_POS):               {SCAN_POS}"
SHOW/SESS  "Table with sky and objects limits (WINDOWS):              {WINDOWS}"
SHOW/SESS  "Detection threshold for objects (<0: relative) (THRESH):  {THRESH}"
SHOW/SESS  "Detection window for objects (WIND):                      {WIND}"
SHOW/SESS  "Binning in X for object search  (XBIN):                   {XBIN}"
SHOW/SESS  "Fraction of central intensity for object limit (INT_LIM): {INT_LIM}"
SHOW/SESS  "Minimum distance between sky and object limits (MIN_DIST): {MIN_DIST}"
SHOW/SESS  "Minimum number of rows for sky (MIN_SKY):                 {MIN_SKY}"
SHOW/SESS  "Number of object regions (NOBJ):                          {NOBJ}"
SHOW/SESS  "Number of sky regions (NSKY):                             {NSKY}"
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS  "-------------------------------------------------------------------"
SKY:
SHOW/SESS  "Sky-fit related: SKYFIT/MOS "
SHOW/SESS  "                            "
SHOW/SESS  "Sky frame (SKYFRAME):          {SKYFRAME}"
SHOW/SESS  "Parameters (SKYPAR):           {SKYPAR}"
SHOW/SESS  "Method for sky fit (SKYMET):   {SKYMET}"
IF P1(1:1) .NE. "A"  RETURN
SHOW/SESS  "-------------------------------------------------------------------"
EXTR:
SHOW/SESS  "Extraction related: EXTRACT/MOS - APPLY/MOS "
SHOW/SESS  "                            "
SHOW/SESS  "Extrac. Object (EXTOBJEC):                           {EXTOBJEC}"
SHOW/SESS  "Root name for table created by APPLY/MOS (CALOBJEC): {CALOBJEC}" 
SHOW/SESS  "Order of fit, No. Iterations (EXTPAR) :              {EXTPAR}"
SHOW/SESS  "Rejection threshold (REJTHRES):                      {REJTHRES} sigma"
SHOW/SESS  "Read-out-noise, conv. factor (CCDPAR):               {CCDPAR}"
SHOW/SESS  "                            "
SET/FORMAT
RETURN
