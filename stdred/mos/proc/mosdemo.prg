! 
! this is the tutorial for MOS
!
define/param  P1  AUTO char  "Mode (INTER/AUTO/STEP):"
defi/loca dummy/c/1/1 " "
! 
if p1(1:1) .eq. "A" then
   write/keyword alltutos/i/1/1 1
endif
!
IF P2(1:1) .NE. "?" GOTO {P2}
!-------------------------------------------------------------------------------
! define local keys
!-------------------------------------------------------------------------------
define/local delete/c/1/1 n
!-------------------------------------------------------------------------------
! initialize displays
!-------------------------------------------------------------------------------
IF P1(1:1) .EQ. "D" .or. P1(1:1) .EQ. "d" THEN
  @s mosdemo,finito
ENDIF
RESET/DISPLAY
!init/mos
create/display 0 620,800
GRAPH/SPEC ? 520,30
load/lut idl4
!-------------------------------------------------------------------------------
! initialize data
!-------------------------------------------------------------------------------
-DELETE demo_mos*.bdf
-DELETE demos*.bdf
intape/fits 1 toto MID_TEST:mos0121.fits NOC
-rename toto0001.bdf  demo_mos121.bdf
intape/fits 1 toto MID_TEST:mos0122.fits NOC
-rename toto0001.bdf  demo_mos122.bdf
intape/fits 1 toto MID_TEST:mos0132.fits NOC
-rename toto0001.bdf  demo_mos132.bdf
intape/fits 1 toto MID_TEST:mos0140.fits NOC
-rename toto0001.bdf  demo_mos140.bdf
intape/fits 1 toto MID_TEST:ha300.tfits NOC
-rename toto0001.tbl  demos_hear.tbl >Null
!-------------------------------------------------------------------------------
! rotate test data
!-------------------------------------------------------------------------------
!rotation:
write/out "Spectra are preliminary rotated to the standard orientation"
!write/out "CREATE/ICAT moscat demo_mos*.bdf"
SET/MIDAS output=logonly
CREATE/ICAT moscat demo_mos*.bdf
!write/out "ROTATE/SPEC moscat demos mode=DELETE"
ROTATE/SPEC moscat demos mode=DELETE
SET/MIDAS output=yes
!-------------------------------------------------------------------------------
! initialize parameters
!-------------------------------------------------------------------------------
init:
init/mos
write/out "SET/MOS LINECAT=demos_hear"
SET/MOS LINECAT=demos_hear
write/out "SET/MOS WLC=demos0003 OBJ=demos0002 FLAT=demos0004"
SET/MOS WLC=demos0003 OBJ=demos0002 FLAT=demos0004
!-------------------------------------------------------------------------------
! display test data
!-------------------------------------------------------------------------------
write/out "*** object frame..."
write/out "LOAD {obj}"
load  {obj} scale=1,2
!
write/out "*** wavelength calibration frame..."
write/out "LOAD {wlc}"
load  {wlc} scale=1,2
!
write/out "*** flat frame..."
write/out "LOAD {flat}" 
load {flat} scale=1,2
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! locate slitlets
!-------------------------------------------------------------------------------
locate:
write/out "*** search slitlets..."
write/out "LOAD {flat}"
load {flat} scale=1,2
set/mos FLATLIM=0.1,3,20 SCAN_POS=300 XBIN=20
write/out "locate/MOS ? {flatlim(1)},{flatlim(2)},{flatlim(3)} {mos} {scan_pos} {xbin}"
locate/MOS {flat} {flatlim(1)},{flatlim(2)},{flatlim(3)} {mos} {scan_pos} {xbin}
define/slit load
write/out "PLOT/LOCATE "
plot/locate 
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! normalize flats
!-------------------------------------------------------------------------------
norm:
write/out "*** normalize flat..."
write/out "NORMA/MOS {flat} mos normflat 2 poly"
norma/MOS {flat} mos normflat 2 poly
write/out "LOAD normflat cuts=0.9,1.2"
load normflat cuts=0.9,1.2 scale=1,2
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! search arc lines
!-------------------------------------------------------------------------------
search:
write/out "*** search lines..."
write/out "LOAD {wlc}"
load {wlc} scale=1,2
write/out "SET/MOS seapar=30,10 "
SET/MOS seapar=30,10 
write/out "SEAR/MOS {wlc} ? 3,2 gauss"
sear/MOS {wlc} ? 3,3 gravity
load/tabl {LINPOS} :X :Y p7=5
write/out "LINPLOT/MOS "
linplot/mos 
@s mosdemo,pause {P1}
write/out "LINPLOT/MOS 1D 2"
linplot/mos 1D 1
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! determine offsets between slitlets 
!-------------------------------------------------------------------------------
offset:
write/out "*** determine offsets between slitlets ..."
write/out "OFFSET/MOS"
offset/mos ? ? 200,15
!
calibr:
IF P1(1:1) .NE. "I" THEN
!-------------------------------------------------------------------------------
! calibrate lines automatically
!-------------------------------------------------------------------------------
  write/out "*** calibrate lines automatically ..."
  write/out "WRITE/KEYW grism 1"
  write/keyw grism 1
  write/out "set/mos alpha=0.3 maxdev=5	"
  set/mos alpha=0.3 maxdev=5	
  write/out "CALIBRATE/MOS 3 4 RV  5536.,6.4 n 50"
  calibrate/mos 3 4 RV  5536.,6.4 n 50
ELSE
!-------------------------------------------------------------------------------
! identify lines
!-------------------------------------------------------------------------------
  write/out "SET/MOS ystart=321"
  set/mos ystart=321
  write/out "*** Identify lines..."
  write/out "Identify the following three lines:"
  write/out "X     =   166.97     264.19   351.02   483.33"
  write/out "WAVE  =  3888.64    4471.48  5015.68  5875.62"
  write/out "IDENT/MOS "
  ident/MOS 
!-------------------------------------------------------------------------------
! calibrate lines
!-------------------------------------------------------------------------------
  write/out "*** calibrate wavelengths..."
  write/out "CALIBRATE/MOS  2  2 IV ? y 50"
  calibrate/MOS 2 2 IV ? y 50
ENDIF
!
write/out "RESPLOT/MOS {linpos} 2 "
RESPLOT/MOS {linpos} 2
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! rebin wavelength calibration frame
!-------------------------------------------------------------------------------
rebin:
write/out "*** Rebin wavelength calibration frame..."
write/out "REBIN/MOS {wlc} test3 3500,8000,3. line coerbr mos 3"
rebin/MOS {wlc} test3 3500,8000,3. line coerbr mos 3
write/out "LOAD test3 "
load test3 cuts=100,300 scale=-3,2 center=cc
clear/chan over
load/tabl linpos :WAVE :Y p7=5
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! save and initialize a session
!-------------------------------------------------------------------------------
!write/out "*** save and initialize a session ***"
!-------------------------------------------------------------------------------
! Flat-field object frame
!-------------------------------------------------------------------------------
write/out "*** Flat-field object frame..."
write/out "COMP/IMA object3 = ({obj}/normflat)"
comp/ima object3 = ({obj}/normflat)
!-------------------------------------------------------------------------------
! Rebin object frame
!-------------------------------------------------------------------------------
write/out "*** Rebin object frame..."
write/out "REBIN/MOS object3 rebobj 3000,8000,3"
rebin/mos object3 rebobj 3500,8000,3
clear/disp
write/out "LOAD rebobj scale=-3,2 center=c,c "
load rebobj scale=-3,2 center=c,c cuts=0,1000
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! Defining limits of objects and optionally sky
!-------------------------------------------------------------------------------
!write/out "Defining limits of objects and optionally sky"
!
!write/out "DEFINE/WINDOW init"
!define/window init
!write/out "DEFINE/WINDOW add 1 156,162"
!define/window add 1 156,162
!write/out "DEFINE/WINDOW add 3 203,207"
!define/window add 3 203,207
!write/out "DEFINE/WINDOW add 4 236,241"
!define/window add 4 236,241
!write/out "DEFINE/WINDOW add 5 256,262"
!define/window add 5 256,262
!write/out "DEFINE/WINDOW add 6 282,291"
!define/window add 6 282,291
!write/out "DEFINE/WINDOW add 7 332,336 316,331"
!define/window add 7 332,336 316,331
!write/out "DEFINE/WINDOW add 8 341,347 350,361"
!define/window add 8 341,347 350,361
!write/out "DEFINE/WINDOW add 9 367,372 373,387"
!define/window add 9 367,372 373,387
!write/out "DEFINE/WINDOW add 10 406,410"
!define/window add 10 406,410
!write/out "DEFINE/WINDOW add 12 444,449"
!define/window add 12 444,449
!write/out "DEFINE/WINDOW read"
!
!define/window read
!write/out "DEFINE/WINDOW load"
!define/window load
!@s mosdemo,pause {P1}
!
!-------------------------------------------------------------------------------
! Search objects and sky
!-------------------------------------------------------------------------------
define:
clear/chan overlay
write/out "*** Search objects and sky..."
write/out "set/mos int_lim=0.05 min_dist=1 min_sky=2"
set/mos int_lim=0.05 min_dist=1 min_sky=2
write/out "DEFINE/MOS rebobj mos windows -0.10 4 100 5720 1"
define/mos rebobj mos  windows -0.10 4 100 5720 1
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! Fit and subtract rebinned sky 
!-------------------------------------------------------------------------------
sky_reb:
write/out "*** Fit sky ( rebinned object!)..."

write/out "SKYFIT/MOS rebobj mos windows sky 5 MEDIAN"
skyfit/MOS rebobj mos windows sky 5 MEDIAN
write/out "LOAD sky scale=-3,2"
load sky scale=-3,2 cuts=0,1000
@s mosdemo,pause {P1}
write/out "*** Subtract sky..."
write/out "COMP/IMA object4 = rebobj - sky"
comp/ima object4 = rebobj - sky
write/out "LOAD object4 scale=-3,2 cuts=0,1000"
load object4 scale=-3,2 cuts=-50,200
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! Extract rebinned objects
!-------------------------------------------------------------------------------
ex_reb:
write/out "*** Extract rebinned objects..."
!write/out "LOAD rebobj scale=-3,2"
!LOAD rebobj scale=-3,2 cuts=0,1000
write/out "EXTRA/MOS rebobj sky windows extreb 3,3 40,5 3"
extra/MOS rebobj sky windows extreb 3,3 40,5 3
write/out "PLO/ROW extreb @2"
plo/row extreb @2
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! Fit and subtract sky 
!-------------------------------------------------------------------------------
sky:
write/out "*** Fit sky ( non-rebinned object!)..."

write/out "LOAD object3 scale=1,2"
load object3 scale=1,2 cuts=0,1000
@s mosdemo,pause {P1}
write/out "SKYFIT/MOS object3 mos windows sky 5 MEDIAN"
skyfit/MOS object3 mos windows sky 5 MEDIAN
write/out "LOAD sky scale=1,2"
@s mosdemo,pause {P1}
load sky scale=1,2 cuts=0,1000
write/out "*** Subtract sky..."
write/out "COMP/IMA object4 = object3 - sky"
comp/ima object4 = object3 - sky
write/out "LOAD object4 scale=1,2"
load object4 scale=1,2 cuts=-50,200
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! Extract objects
!-------------------------------------------------------------------------------
extract:
write/out "*** Extract non-rebinned objects..."
!write/out "LOAD object3 scale=1,2 "
!LOAD object3 scale=1,2 cuts=0,1000
write/out "EXTRA/MOS object3 sky windows extobj 3,3 40,5 3"
extra/MOS object3 sky windows extobj 3,3 40,5 3
write/out "PLO/ROW extobj @2"
plo/row extobj @2
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! Extract rows to tables 
!-------------------------------------------------------------------------------
apply:
write/out "*** Extract rows to tables ..."
write/out "APPLY/MOS extobj coerbr ext "
apply/mos extobj coerbr ext 
set/grap ya stype=0
write/out "PLO/TABLE ext0002 :lambda :flux"
plo/table ext0002 :lambda :flux
@s mosdemo,pause {P1}
!-------------------------------------------------------------------------------
! Extract rows to images 
!-------------------------------------------------------------------------------
rebin1d:
write/out "*** Extract rows to images ..."
write/out "REB1D/MOS extobj ext 3500,8000,3 lin coerbr"
reb1d/mos extobj ext 3500,8000,3 lin coerbr
write/out "PLO/ROW ext0002 "
plo/row ext0002
@s mosdemo,finito
!
ENTRY PAUSE

define/param P1 ? CHAR "Mode:"
define/local nix/i/1/1 1

if P1(1:1) .NE. "A" THEN
	inquire/keyw nix "Press return to continue; -1 to stop "
	if nix .lt. 0 @s mosdemo,finito
ENDIF
! 
ENTRY FINITO
define/local del/c/1/1 n
set/gra
write/out "*** End of Tutorial ***"
write/keyw del y
if m$existk("alltutos") .eq. 1 then
  if alltutos .eq. 1 goto del_check
else
  inquire/key del "delete all frames/tables created/used by Tutorial?"
endif

del_check:
if del(1:1) .eq. "y" then
    -DELETE moscat.cat
    dele test3 no
    dele object3 no
    dele object4 no
    dele rebobj no
    dele extobj no
    dele extreb no
    dele normflat no
    dele sky no
endif





