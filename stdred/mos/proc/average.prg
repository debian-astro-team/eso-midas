! @(#)average.prg	3.1 (ESO-IPG) 3/23/92 11:10:29 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  average.prg  for AVERAGE/IMAGE, /WEIGHTS, /WINDOW
! K. Banse	880919, 910318, 920326, 930123, 931103
! 
! use via
! AVER/IMA out = inframes [mergeflg] [null] [option] [valid_interval]
! with inframes: frame1,frame2,...,framen  or  catalog.cat
! 
! or      AVER/WEI out = inframes [mergeflg]  [null]
! and     AVER/ROW out = input start,end [NO]
! and     AVER/COL out = input start,end [NO]
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ? ima "Enter result frame: "
if p2(1:2) .ne. "= " then
      write/out 2. parameter:
      goto errors
   endif
!
if mid$cmnd(11:13) .eq. "ROW" then		!AVERAGE/ROW
   define/par p3 ? ima "Enter input frame: "
   define/par p4 <,> c "Enter start,endpixel in row: "
   define/par p5 Y C
   define/maxpar 5
   write/key history "AVERAGE/ROW"
   run STD_EXE:AVEROW
!
elseif mid$cmnd(11:13) .eq. "COL" then		!AVERAGE/COL
   define/par p3 ? ima "Enter input frame:"
   define/par p4 <,> c "Enter start,endpixel in column: "
   define/par p5 Y C
   define/maxpar 5
   write/key history "AVERAGE/COLUMN"
   run STD_EXE:AVEROW
!
else  					!AVERAGE/IMA + /WEIGHT
   define/par p3 ? c "Enter frame_list or catalog.cat: "
   define/par p4 N C "Enter merging option M(erge) or N(oMerge): "
   if p4(2:2) .ne. " " then
      write/out 4. parameter:
      goto errors
   endif
   define/par p5 {null(2)} c "Enter Null value for undefined pixels: "
   define/par p6 AV C "Enter option for averaging: "
   define/par p7 + n "Enter valid interval xlo,xhi: "
   define/maxpar 7
! 
   write/key history "AVERAGE/{MID$CMND(11:14)}"
   write/key action/c/1/2 {p4(1:1)}{mid$cmnd(11:11)}
!   write/key out_a {p1}
   define/local out_a/c/1/60 "{p1}"
   run MID_EXE:AVERAG
!
endif
return
! 
errors:
write/err 30
