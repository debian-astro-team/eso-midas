! @(#)mosapdisp.prg	8.2 (ESO-IPG) 11/18/94 09:21:17
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSSLIN.PRG
!
! .PURPOSE
!
! execute the command :
! SEARCH/MOS image params
!
! -------------------------------------------------
!
CROSSREF WLC TABLE LINPOS SEAPAR YBIN METHOD
!
DEFINE/PARAM P1 {EXTOBJEC}      IMAGE   "Enter input image (extracted objects):"
DEFINE/PARAM P2 {LINFIT}      TABLE   "Enter LINFIT table:"
DEFINE/PARAM P3 {CALOBJEC}      IMAGE   "Enter output table (root name):"
!
!SET/FORS flat={P1} linpos={P2} wlclim={P3} centmet={P4}
WRITE/KEYW IN_A  {P1}
WRITE/KEYW IN_B  {P2}
WRITE/KEYW OUT_A {P3}
!
RUN STD_EXE:MOSAPDISP
