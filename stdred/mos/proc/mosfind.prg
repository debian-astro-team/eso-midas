! @(#)mosfind.prg	8.7 (ESO-IPG) 03/23/95 10:21:08
! @(#)mosfind.prg	1.4 (ESO-IPG) 2/14/94 15:44:57
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! MOS PROCEDURE : MOSFIND.PRG
!
! .PURPOSE
!
! execute the command :
!  LOCATE/MOS ff para
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
CROSSREF FF TABLE THRES XPOS
!
DEFINE/PARAM P1 {FLAT} 	IMAGE 	"Enter input image:"
DEFINE/PARAM P2 {FLATLIM(1)},{FLATLIM(2)},{FLATLIM(3)}  NUMBER "Enter threshold, width, low-limit:"
DEFINE/PARAM P3 {MOS} 	TABLE  	"Enter output table:"
DEFINE/PARAM P4 {SCAN_POS(1)} NUMBER  "x-position:"
DEFINE/PARAM P5 {XBIN} 	NUMBER 	"x-width:"
DEFINE/PARAM P6 2	NUMBER 	"no mans land at slit edge"
!
DEFINE/LOCAL ORI/C/1/60 " " all
DEFINE/LOCAL QUALIF/C/1/4 " " all
DEFINE/LOCAL XLIMITS/I/1/2 0 all
DEFINE/LOCAL INDEX/I/1/1 0
!
WRITE/KEYW QUALIF {MID$CMND(11:14)}
index = M$INDEX(MOS,".tbl")
if index .le. 0 write/key MOS {MOS}.tbl
!
IF M$EXISTD(P1,"XPOS") .NE. 1 THEN
   WRITE/DESCR  {P1}  XPOS/R/1/100  0.   all
ENDIF
!
WRITE/KEYW IN_A    {P1}
WRITE/KEYW INPUTR  {P2}
WRITE/KEYW INPUTD  {P4},{P5}
WRITE/KEYW INPUTI  {P6}
!
IF INPUTD(1) .eq. 0 THEN
  INPUTD(1) = {{in_a},npix(1)}/2*{{in_a},step(1)}+{{in_a},start(1)}
ENDIF
xlimits(1) = inputd(1)-{{in_a},start(1)}-inputd(2)/{{in_a},step(1)}+1
xlimits(2) = inputd(1)-{{in_a},start(1)}+inputd(2)/{{in_a},step(1)}+1
!
WRITE/KEYW ORI {IN_A}
aver/col &z = {in_a} @{xlimits(1)},@{xlimits(2)}
WRITE/DESCR  &z  XPOS/R/1/100  0.   all
!
WRITE/KEYW OUT_A   {P3}
WRITE/KEYW IN_A    middummz
RUN STD_EXE:MOSFIND
!
!NSLIT = {{MOS},TBLCONTR(4)}
!SET/MOS FLATLIM={P2}
write/out "Found {nslit} slitlets"




