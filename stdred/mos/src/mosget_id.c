/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .COPYRIGHT   (C) 1993-2009 European Southern Observatory     */
/* .IDENT       mosget_id.c                                */
/* .AUTHORS     Sabine Moehler   (LSW)                     */
/* .KEYWORDS    Spectroscopy, Long-Slit, MOS               */
/* .PURPOSE     gets identification for wavelength         */
/*              calibration line                           */
/* .VERSION     1.0  Creation  20-AUG-1993                 */
/* 
    090804	last modif

   .INPUT/OUTPUT:
       IN_A         :  temporary line table (selected)  
       INPUTR/R/1/3 :  tolwind, xpos, line identification
       INPUTI/I/1/1 :  ystart
   -------------------------------------------------------
*/

#include <tbldef.h>
#include <midas_def.h>
#include <ok.h>

#include <moscalib.h>
#include <proto_spec.h>
#include <proto_longmos.h>

int    inull;   /* Representation of NULL values */
float  rnull;
double dnull;

int main()
{

   char   tmp[60]; 
   int    tmpx, tidtmp, nbcol, nbrow, nsort, allcol, allrow; /* Table numbers */
   int    tmpid, row, actvals, kunit;                    
   double *colx, x_id, *colid, line_id;  

   SCSPRO("mosget_id");

/* Read name of table tmp in keyword IN_A,  */
   SCKGETC("IN_A",  1, 60, &actvals, tmp);

/* Read parameters: 
             INPUTI(1)   Ystart 
             INPUTI(2)   table row for which identification is entered
             INPUTR(1)   Maximal standard deviation of residuals (pixels)
             INPUTR(2)   Line position (in pixels)
             INPUTR(3)   Line position (in Angstroem)
*/
/* Open table tmp and search for column :X. All 
   table are mapped to double precision arrays */

   if (TCTOPN(tmp, F_I_MODE, &tidtmp))
      SCTPUT("**** Error while opening temporary table");
   TCIGET(tidtmp, &nbcol, &nbrow, &nsort, &allcol, &allrow);

   /* Finds column :X in temporary table */

   TCCSER(tidtmp, ":X", &tmpx);
   if (tmpx == (-1)) SCTPUT("**** Column :X not found");

   /* Finds column :IDENT in temporary table */

   TCCSER(tidtmp, ":IDENT", &tmpid);
   if (tmpx == (-1)) SCTPUT("**** Column :IDENT not found");

/* Initialisations */

/* Get representation of NULL values and allocate memory for auxiliary 
   arrays */

   TCMNUL(&inull, &rnull, &dnull);

/* Load line table and line catalog in memory. Table columns are stored in 
   double although they can be of any type (I, R*4 or R*8) */
   
   colx = (double *) osmmget((nbrow+1)*sizeof(double));
   read_col(tidtmp, nbrow, tmpx, colx, dnull);  
   colid = (double *) osmmget((nbrow+1)*sizeof(double));
   read_col(tidtmp, nbrow, tmpid, colid, dnull);  
   for (row = 1; row <= nbrow+1; row++)
     {
      x_id = colx[row-1];
      line_id = colid[row-1];
      if (line_id == dnull) line_id = 0.;
      SCKWRD("LID", &line_id, row, 1, &kunit);
      SCKWRD("XPOS", &x_id, row, 1, &kunit);
     }
   osmmfree((char *) colx);
   osmmfree((char *) colid);
   TCTCLO(tidtmp);
   SCSEPI();
return 0;
}
 
