/* @(#)proto_mos.h	10.1.1.1 (ESO-IPG) 8/7/95 17:22:41 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   proto_spec.h
.AUTHOR      C. Guirao IPG-ESO Garching
.AUTHOR      T. Szeifert LSW Heidelberg
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for all SPEC routines,
             2D-tool included by TS-1996 

.ENVIRONment none

.VERSION     1.0     22-Oct-1993   Creation by CG.
------------------------------------------------------------*/

#ifndef PROTO_MOS
#define PROTO_MOS

extern void eval_disp(
#ifdef __STDC__
	double x[], double l[], int n
#endif
);

extern void finishdisp(
#ifdef __STDC__
	 void 
#endif
);

extern void initdisp(
#ifdef __STDC__
	char name[], char mode[], int start
#endif
);

extern double mos_fit_disp(
#ifdef __STDC__
	int *ndata, int *deg, double x[], double l[]
#endif
);

extern void mos_initdisp(
#ifdef __STDC__
	char name[], char mode[], int  start
#endif
);

extern void mos_savedisp(
#ifdef __STDC__
	double save[]
#endif
);

extern void mos_writedisp(
#ifdef __STDC__
	int line, int slit, int ypix,double y,int numrow, double rms
#endif
);

extern void printdisp(
#ifdef __STDC__
	 void 
#endif
);

extern double readdisp(
#ifdef __STDC__
	int y
#endif
);

extern void setdisp(
#ifdef __STDC__
	int deg, double coefs[]
#endif
);

extern void setrefdeg(
#ifdef __STDC__
	int deg
#endif
);

void writedisp(
#ifdef __STDC__
	int line, int ypix, double y, double pixel, double rms
#endif
);

/*  These are the 2D-tools: */

#ifdef _STDC__
extern   void setrefdeg_2D(int deg);
#else
extern   void setrefdeg_2D(); 
#endif

#ifdef _STDC__
extern   void setdisp_2D(int deg, double coefs[]);
#else
extern   void setdisp_2D();
#endif

#ifdef _STDC__
extern   double mos_fit_disp_2D(int *ndata, int *deg, double x[], double y[],
		       double l[]);
#else
extern   double mos_fit_disp_2D();
#endif

#ifdef __STDC__
extern   void mos_eval_disp_2D (double x[], double y[], double l[], int n);
#else
extern   void mos_eval_disp_2D ();
#endif

#ifdef __STDC__
void mos_writedisp_2D(int line, int slit, int ypix, 
		      double y, int numrow, double rms);
#else
void mos_writedisp_2D();  
#endif


#ifdef __STDC__
  void fleg_2D( double x1, double x2, double pl[], int nl);
#else
  void fleg_2D();
#endif 
 
#ifdef __STDC__
  void fpoly_2D( double x1, double x2, double pl[], int nl);
#else
  void fpoly_2D();
#endif  

#ifdef __STDC__
  void fcheb_2D( double x1, double x2, double pl[], int nl);
#else
  void fcheb_2D();
#endif  

#ifdef _STDC__
  void lfit2_2D(double x1[], double x2[], double y[], double sig[], int ndata, 
	   double a[], int ma, int lista[], int mfit, double **covar, 
	   double *chisq, void (*funcs)(double, double, double *, int));
#else
  void lfit2_2D();
#endif



#ifdef __STDC__
int match(int verif, double linid[], double linwav[], 
	  double liny[], double lindif[], int nbrow, 
	  double lcat[], int nbrowcat, double alpha, 
	  double *stdres, double dnull, int reject[]);
#else
  int match( );
#endif  







#endif
