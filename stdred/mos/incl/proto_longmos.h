/* @(#)proto_longmos.h	1.1 (ESO-IPG) 2/11/94 10:14:52 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   proto_longmos.h
.AUTHOR      Otmar Stahl 
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for all NR-Routines routines used in MOS
.ENVIRONment none
.VERSION     1.0    6-APR-1994 Otmar Stahl 
------------------------------------------------------------*/
extern int read_col(
#ifdef __STDC__
     int, int, int, double [], double
#endif
);

extern int write_dcol(
#ifdef __STDC__
     int, int, int [], int,  double []
#endif
);
