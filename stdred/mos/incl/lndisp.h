/* @(#)lndisp.h	19.1 (ESO-DMD) 02/25/03 14:26:19 */

/* Include file for dispersion relation module lndisp.h */


void   setdisp();
void   printdisp();

void   eval_disp();
void   initdisp();
void   finishdisp();
void   writedisp();
double fit_disp();
double readdisp();

