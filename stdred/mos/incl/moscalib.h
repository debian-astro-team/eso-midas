/* @(#)moscalib.h	1.1 (ESO-IPG) 2/11/94 10:24:55 */
/* @(#)moscalib.h	1.1 (ESO-IPG) 12/20/93 11:20:33 */
/* @(#)lncalib.h	5.1.1.1 (ESO-IPG) 4/7/93 09:38:33 */
/* Definition of rejection codes */

#define  NOT_PROCESSED     0
#define  SUCCESSFULL_MATCH 1
#define  MATCH_FAILED     -1
#define  REACHED_LIMIT    -3
#define  RESIDUAL_GT_TOL  -5

