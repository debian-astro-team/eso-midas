% @(#)avmos_row.hlq	19.1 (ESO-IPG) 02/25/03 14:25:57
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      avmos_row.hlq
%.AUTHOR     SC, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: AVMOS/ROW
%.VERSION    1.0  12-DEC-94 : Creation, SC
%----------------------------------------------------------------
\se
SECTION./ROW
\es\co
AVMOS/ROW					      12-DEC-94  SM
\oc\su
AVMOS/ROW  out = in [start,end] 
            average rows in two-dim. image
\us\pu
Purpose:    
            Average rows in a selected range of the input image. 
\up\sy
Syntax:     
            AVMOS/ROW  out = in [start,end] 
 \\
\ys\pa
            out = resulting 1 dimensional frame
\ap\pa
            in = image to be averaged
\ap\pa
            start = first row, defaulted to the first row of the frame
\ap\pa
            end = last row, defaulted to the last row of the frame
 \\
\ap\sa
See also: 
            AVMOS/COL, AVERAGE/ROW, AVERAGE/COL
\as\no
Note:       
            First and last row can be specified in pixels or world coords.
 \\
\on\exs
Examples:
\ex
            AVMOS/ROW out = in 1.,10.
            Average the rows of frame in.bdf, store result in out.bdf
\xe \sxe
