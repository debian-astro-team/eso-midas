% @(#)show_mos.hlq	1.2 (ESO-DMD) 2/14/94 14:21:20
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1994 European Southern Observatory
%.IDENT      show_mos.hlq
%.AUTHOR     PB, DMD/ESO
%.KEYWORDS   MIDAS, help files, SHOW/MOS
%.PURPOSE    On-line help file for the command: SHOW/MOS
%.VERSION    1.0  14-FEB-1994 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./MOS
\es\co
SHOW/MOS						25-SEP-1991 PB
\oc\su
SHOW/MOS 
	show parameters of MOS session
\us\pu
Purpose:
	  Show the parameters of the current MOS session.
\up\sy
Syntax:
	  SHOW/MOS [param]
          param = A (all parameters)
                  C (wavelength calibration)
                  D (define object/sky regions)
                  E (extraction)
                  F (flat-field)
                  G (general and CCD)
                  R (rebin) 
                  S (sky fit)
                  W (wavelength search/identification)
                  
\ys\no
Note:
	  1) The parameters displayed by SHOW/MOS depend on the
	  values of method and option keywords. The list of these
	  keywords and a short description is provided by commands
	  "HELP/MOS method" and "HELP/MOS options"

	  The displayed parameters correspond to the only parameters
	  to be set depending on the options and methods.

	  2) When the screen is full, the scrolling is interrupted and can be
	  restarted by typing <Return> or interrupted by q+<Return>.
	  Uninterrupted scrolling is made available by SET/MOS SESSDISP=NO
\on\exs
Examples:
\ex
	  SHOW/MOS c
          will give the following display
          ****************  MOS Parameters  ******************* 
          Line calibration related: CALIBRATE/MOS - RESPLOT/MOS 
                             
          Wavelength calibration method (WLCMTD):                  FC 
          Polynomials for dispersion relation (POLTYP):            CHEBYSHEV 
          Order of fit for dispersion relation (WLCORD(1)):        2 
          Minimum/maximum number of iterations for fit (WLCNITER): 3 
          Central wavelength (WCENTER):                            0.0 
          Average dispersion (A/pix) (AVDISP):                     0.0 
          Table with fit coefficients (LINFIT):                    coerbr 
          Table with wavelength catalog (LINECAT):                 hear 
          Grism number (GRISM):                                    1 
          Tolerance for automatic line identification (TOL):       2.0 
          Plot flag for line residuals (PLOTC):                    N 
          Flag for dispersion relations for slitlets (CAL):        0  
          Matching parameter (0 to 0.5) (ALPHA):                   0.2 
          Maximal Deviation (MAXDEV):                              10.0 
          Slitlet counter (ISLIT):                                 0 
\xe \sxe
