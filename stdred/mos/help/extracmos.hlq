% @(#)extracmos.hlq	1.2 (ESO-DMD) 2/14/94 14:21:12
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1994 European Southern Observatory
%.IDENT      extracmos.hlq
%.AUTHOR     PB, DMD/ESO
%.KEYWORDS   MIDAS, help files, EXTRACT/MOS
%.PURPOSE    On-line help file for the command: EXTRACT/MOS
%.VERSION    1.0  14-FEB-1994 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./MOS
\es\co
EXTRACT/MOS                                             19-AUG-1993  OS
\oc\su
EXTRACT/MOS [object] [sky] [table] [extobj] [extpar] [ccdpar] [reject] 
	extract spectra from MOS image with optimum weights
\us\pu
Purpose:
	  Extracts spectra from MOS image using Horne algorithm
\up\sy
Syntax:		
	  EXTRACT/MOS [object] [sky] [windows] [extobj] [extpar] [ccdpar] [rej]
\ys\pa
	  object = object frame
              Session keyword: OBJECT
              Default: none
	  sky = sky frame
              Session keyword: SKYFRAME
              Default: sky
	  windows = table with object positions
              Session keyword: WINDOWS
              Default: window
	  extobj = Output frame
              Session keyword: EXTOBJEC
              Default: none
	  extpar = extraction parameters (order, n_iter)
              Session keyword: EXTPAR
              Default: none
	  ccdpar = CCD parameters (RON, gain)
              Session keyword: CCDPAR
              Default: none
	  rej = threshold for rejection of pixels
              Session keyword: REJTHRESH
              Default: 3
\ap/sa
See also:
	  SKYEX/MOS	
\as\no
Note:       
	  This command subtracts the sky stored in sky.bdf 
          and extracts 1D-spectra from a MOS frame. The limits
	  of the extraction slitlets have to be defined before with the
	  command DEFINE/MOS or DEFINE/WINDOW. A weighted extraction following
	  the method of Horne (1986, PASP 98, 609) is used to average the 
	  rows for each object. With order set to -1, a simple average is used.
          Pixels are rejected as cosmic events if their deviation from mean 
          the spatial profile exceeds rej*sigma. sigma is computed from the
          ADU number of the pixel using the CCDPAR values for the statistics. 
	  The ouput is a 2-D image containing one extracted spectrum per row. 
	  Use REB1D/MOS to create 1-dimensional frames rebinned to constant 
          wavelength steps for each object or APPLY/MOS to create a table with
          flux, wavelength, etc, for each object separately 
          (without rebinning).
	 
\on\exs
Examples:
\ex
	  EXTRA/MOS object sky windows extobj 3,3 40,5 3
          Extracts the objects as defined by windows.tbl (columns
          :Obj_Strt and :Obj_End). The extracted frames are stored in 
          extobj.bdf. The CCD used has a read-out-noise of 40 e- and a gain
          5 e-/ADU. The sky frame is used for estimation of the errors.
\xe \sxe
