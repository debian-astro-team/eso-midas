% @(#)skyfitmos.hlq	1.2 (ESO-DMD) 2/14/94 14:21:20
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1994 European Southern Observatory
%.IDENT      skyfitmos.hlq
%.AUTHOR     PB, DMD/ESO
%.KEYWORDS   MIDAS, help files, SKYFIT/MOS
%.PURPOSE    On-line help file for the command: SKYFIT/MOS
%.VERSION    1.0  14-FEB-1994 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./MOS
\es\co
SKYFIT/MOS					19-AUG-1993 OS
\oc\su
SKYFIT/MOS [object] [tabmos] [tabwin] [skyfra] [skypar] [skymet] [ccdpar]
	fit sky to MOS frame
\us\pu
Purpose:   
	  Fit sky in a MOS image
\up\sy
Syntax:	
	  SKYFIT/MOS [object] [tabmos] [tabwin] [skyfra] [skypar] [skymet] [ccdpar]
\ys\pa
	  object = Object frame
              Session keyword: OBJECT
              Default value: none
	  tabmos = Table with slitlet positions (MOS)
              Session keyword: MOS
              Default value: mos
	  tabwin = Table with windows positions (WINDOWS)
              Session keyword: WINDOWS
              Default value: window
	  skyfra = Output frame
              Session keyword: SKYFRAME
              Default value: sky
	  skypar = order of fit for polynomial or width of window for median
              Session keyword: SKYPAR
              Default value: none
	  skymet = method for fitting sky background
              Session keyword: SKYMET
              Default value: none
              MEDIAN : median over windows 
              POLYNOMIAL : fit polynomial over windows 
              NOWINDOWS : ignore table tabwin, use median over full slitlet
	  ccdpar = ccd parameter for cosmic elimination
	      Session keywords: CCDPAR(1),CCDPAR(2),REJTHRES only used for
	      method POLYNOMIAL. CCDPAR(1) and CCDPAR(2) are RON (in e-) and
	      the gain (e-/adu). REJTHRES is the rejection threshold in units
	      of sigma. For negative values of REJTHRES no cosmics are 
	      detected.
	      
\ap\no
Note:       
	  This command fits the sky in a MOS frame. In each individual
	  slitlet, the sky is approximated by taking the median value
          or fitting a polynomial along columns within the regions defined 
          to contain sky (see DEFINE/MOS). Before using the polynomial fit, 
          the object frame should be filtered from cosmic ray hits.
	  If parameter CCDPAR(3) > 0 is given, the program selects the  
	  Pixels with values within CCDPAR(3) * sigma around the median,
	  before fitting polynoms. If the number of selections is less
	  or equal the order of the fit the no selection is done in the
	  row.

          In slitlets where no sky regions have been defined, the program 
          copies the flux within the slitlet to the sky frame. After sky 
          subtraction this results in slitlets with zero flux.

          For skymet=NOWINDOWS the median is taken along the whole slitlet, 
          ignoring tabwin.

\on\exs
Examples:
\ex
	  SKYFIT/MOS object mos windows sky 
          Determine the sky in the frame object. The limits of the slitlets
          are taken from the table mos and the windows for sky are defined
          in the table windows. The sky is approximated as the median value
          of all values marked as sky in table windows. The output is stored 
          in frame sky.
\xe \sxe



