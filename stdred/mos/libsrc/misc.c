/* @(#)misc.c	8.3 (ESO-IPG) 11/18/94 09:23:21 */
/* @(#)misc.c	8.3 (ESO-IPG) 11/18/94 09:23:21 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        misc.c
.MODULE       subroutines
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Miscelaneous functions:
		- Checking of the existence of a Unix file.
		- Checking of the Midas graphics window.
.KEYWORDS     file checking, graphics window
.VERSION 1.0  1-Apr-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

#include <proto_os.h>
#include <stdio.h>
#include <string.h>
#ifdef vms
#include <types.h>
#include <stat.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#endif
#include <gl_defs.h>

/* check for the existence of 'file' with the extension 'ext' (incl. dot) */
#ifdef __STDC__
   int file_exists( char *file, char *ext )
#else
   int file_exists( file, ext )
   char *file, *ext;
#endif
{
    char fileext[MAXLINE];
    int i;
    struct stat statbuf;

    for ( i = 0; file[i] != '\0'; i++ )
	if ( file[i] == ' ' ) {
	    file[i] = '\0';
	    break;
	}

    if ( strstrs(file, ext) == NULL )
    	sprintf( fileext, "%s%s", file, ext );
    else
	strcpy( fileext, file );

    if ( stat(fileext, &statbuf) == -1 )
	return(FALSE);

    return(TRUE);
}

#ifdef __STDC__
   int graphwin_exists( void )
#else
   int graphwin_exists( )
#endif
{
    char unit[10];
    char file_old[MAXLINE], file[MAXLINE];
    char midwork[MAXLINE];

    osfphname("MID_WORK", midwork);
    osfphname("DAZUNIT", unit);
    unit[3] = '\0';

    sprintf(file_old, "%s%sXW", midwork, unit);
    sprintf(file, "%smidas_xw%s", midwork, unit);
    return(file_exists(file_old, ".soc") || file_exists(file_old, ".soc=") ||
           file_exists(file, "") || file_exists(file, "=") );
}
#ifdef __STDC__
   void DropTrailingBlanks( char *str)
#else
   void DropTrailingBlanks( str)
   char *str;
#endif
{
    int i;

    for ( i = 0; str[i] != '\0'; i++ )
        if ( str[i] == ' ' ) {
            str[i] = '\0';
            break;
        }
}
