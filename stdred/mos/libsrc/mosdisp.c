/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       mosdisp.c                                  */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    Spectroscopy, Long-Slit, MOS               */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Modification 21-SEP-1993      */
 
/*  090507		last modif			   */
/* ------------------------------------------------------- */

/* Purpose:
   This module is related to the dispersion relation and its representation.
   The upper modules (lncalib.c, lnrebin.c) do not know which representation is
   used (Chebyshev polynomials, or any other), nor how it is stored (which
   descriptor, meaning of coefficients). This design enables to replace 
   this module lndisp.c by any other based on a different representation.
 */

/* Note:

   the general rule is that arrays start at index 1. However
   the module can accomodate any starting index, which is specified at 
   initialisation time (function initdisp).
 */

/* Algorithms:

   The module provides ten top-level routines related to the evaluation of the
   dispersion relation. Only those six routines can be used in applications 
   related to the dispersion relation (wavelength calibration, rebinning, ...).
   The lower level routines must be considered as private.

   --- void mos_savedisp (save) ---

   Stores dispersion coefficients in array <save>.


   --- double mos_fit_disp (ndata, degree, x, l) ---

   This routine evaluates the dispersion relation on the basis of ndata 
   couples of d values (x,l). Array indexes start at 0. The degree is 
   indicative. If ndata is too small, a smaller degree will be fitted 
   to the couples. Minimum value for ndata is 2. The procedure returns 
   a rough estimate of the average pixel size and the error of the 
   dispersion relation.

   --- void mos_eval_disp (x, l, ndata) ---

   Applies the last previously estimated (or loaded) dispersion relation 
   to ndata values of x and estimates the corresponding wavelength l. 
   Arrays start at index 0.

   --- void mos_initdisp (name, mode, start) ---

   Open (mode = OLD) or Create (mode = NEW) a storage table (named <name>).
   In addition to initdisp also the column :SLIT is created.


   --- void mos_writedisp(line, slit, pix_y, world_y, nyrow) ---

   Store in the storage table the dispersion relation corresponding to the 
   slitlet <slit>, row number <line>, and world coordinate <y>.

   --- double mos_readdisp(line) ---

   Retrieves from the storage table the dispersion relation corresponding 
   to the row number <line>.  The procedure returns a rough estimate of 
   the average pixel size.


 */


#include "mutil.h"
#include "dispersion.h"
#include <stdio.h>
#include <ctype.h>
#include <nrutil.h>


/* -------------------------------------------------------------------------- */

#ifdef __STDC__
void 
mos_savedisp (double save[])
#else
void 
mos_savedisp (save)
     double save[];
#endif

{

  int i;

  /* printf("Dispersion Relation. Degree: %d. Refdeg: %d. MaxCoef:%d\n",
     fdeg,refdeg,maxcoef);
     printf("Coefficients: ");  */

  for (i = 0; i <= ncoef-1; i++)
    {
      save[i] = coef[i+1];
      /*     printf("save[%d] =  %f ",i, save[i]);  */
    }

}

/* -------------------------------------------------------------------------- */

#ifdef __STDC__
double 
mos_fit_disp (int *ndata, int *deg, double x[],
	      double l[])
#else
double 
mos_fit_disp (ndata, deg, x, l)
     int *ndata, *deg;
     double x[], l[];
#endif

{

  double *sig, **covar;
  int    *lista;
  int    actvals, maxdeg, i; 
  char   poltyp[12];

/* printf("NData : %d   ---   Deg value : %d\n",*ndata,*deg); */

  refdeg = *deg; 
  maxdeg = *ndata - 1;
  if (refdeg > maxdeg)
    fdeg = maxdeg;
  else
    fdeg = refdeg;
  ncoef = fdeg + 1;
  maxcoef = refdeg + 1;

  if (*ndata < 2)
    {
      printf ("Not enough lines (minimum is 2). \nNo dispersion relation computed\n");
      return (-2.);
    }

  else if (fdeg < 1)
    {
      printf ("Degree : %d. No dispersion relation fitted\n", *deg);
      return (-1.);
    }

  else
    {
      covar = (double **) dmatrix (1, *ndata, 1, *ndata);
      sig = (double *) dvector (1, *ndata);

      lista = (int *) ivector (1, ncoef);
      for (i = 1; i <= ncoef; i++)
	lista[i] = i;
      for (i = 1; i <= *ndata; i++)
        sig[i] = 1.;

      SCKGETC("POLTYP", 1, 8, &actvals, poltyp);
      if (toupper(poltyp[0]) == 'L')
        lfit2(x,l,sig,*ndata,coef,ncoef,dleg);
      else
        lfit2(x,l,sig,*ndata,coef,ncoef,dcheb);

      free_dmatrix (covar, 1, *ndata, 1, *ndata);
      free_dvector (sig, 1, *ndata);
      free_ivector (lista, 1, ncoef);

      pixbin = coef[2];
      return (pixbin);
    }
}
/* -------------------------------------------------------------------------- */
#ifdef __STDC__
void mos_eval_disp (double x[], double l[], int n)
#else
void mos_eval_disp (x, l, n)
     double x[], l[];
     int n;
#endif

{

  int actvals, i, icoef;
  double xp[MAXNCOE];
  char   poltyp[12];

  SCKGETC("POLTYP", 1, 8, &actvals, poltyp);
  for (i = start_index; i < (n + start_index); i++)
    {
      l[i] = 0.;
      if (toupper(poltyp[0]) == 'L')
        dleg (x[i], xp, ncoef);
      else 
        dcheb (x[i], xp, ncoef);
      for (icoef = 1; icoef <= ncoef; icoef++)
	l[i] += coef[icoef] * xp[icoef];
    }
}

/* -------------------------------------------------------------------------- */

#ifdef __STDC__
void mos_initdisp (char name[], char mode[], int start)
#else
void mos_initdisp (name, mode, start)
     char name[], mode[];
     int start;
#endif

{

  int icoef;
  char colnam[30], numb[10];
  int actvals, null;
  int kunit;
  int ncol, nrow, nsort, allcol, allrow;


  start_index = start;

  if (toupper (mode[0]) == 'N')
    {				/*  NEW table */
      if (TCTINI (name, F_TRANS, F_IO_MODE, 5, 10, &tide))
	SCTPUT ("**** Error while creating output table");
      nbline = 0;
    }
  else
    {
      if (TCTOPN (name, F_IO_MODE, &tide))
	SCTPUT ("**** Error while opening output table");
      SCDRDD (tide, "LNPIX", 1, 1, &actvals, &pixbin, &kunit, &null);
      SCDRDI (tide, "LNDEG", 1, 1, &actvals, &refdeg, &kunit, &null);
      SCDRDI (tide, "LNCOE", 1, 1, &actvals, &maxcoef, &kunit, &null);
      /* fdeg=refdeg, ncoef=maxcoef; */
      TCIGET (tide, &ncol, &nrow, &nsort, &allcol, &allrow);
      nbline = nrow;
    }

  TCCSER (tide, ":SLIT", &colslit);
  if (colslit == (-1))
    TCCINI (tide, D_I4_FORMAT, 1, "I6", "Slit Number",
	    "SLIT", &colslit);

  TCCSER (tide, ":ROW", &colline);
  if (colline == (-1))
    TCCINI (tide, D_I4_FORMAT, 1, "I6", "Row Number",
	    "ROW", &colline);

  TCCSER (tide, ":Y", &coly);
  if (coly == (-1))
    TCCINI (tide, D_R8_FORMAT, 1, "F8.2", "Y Value",
	    "Y", &coly);

  TCCSER (tide, ":RMS", &colrms);
  if (colrms == (-1))
    TCCINI (tide, D_R8_FORMAT, 1, "F8.4", "Angstrom",
	    "RMS", &colrms);

  for (icoef = 1; icoef <= maxcoef; icoef++)
    {
      strcpy (colnam, ":COEF_");
      sprintf (numb, "%d", icoef);
      strcat (colnam, numb);
      TCCSER (tide, colnam, &colcoef[icoef]);
      if (colcoef[icoef] == (-1))
	TCCINI (tide, D_R8_FORMAT, 1, "F16.10", "Coefficients",
		colnam, &colcoef[icoef]);
    }
}

/*----------------------------------------------------------------------------*/
#ifdef __STDC__
int mos_readdisp (int y, int slit)
#else
int mos_readdisp (y, slit)
     int y, slit;
#endif

/* long int  y;     Pixel number of the row */
{

  int line, yval, linext, ydif, null, icoef;
  int mindif, actslit;

  mindif = -1;

  for (line = 1; line <= nbline; line++)
    {
      TCERDI (tide, line, colline, &yval, &null);
      TCERDI (tide, line, colslit, &actslit, &null);
      if (!null && actslit == slit)
	{
	  ydif = y - yval;
	  if (ydif < 0)
	    ydif = (-1) * ydif;
	  if (mindif < 0)
	    mindif = ydif;
	  if (ydif <= mindif)
	    mindif = ydif, linext = line;
	}
    }

  if (mindif == -1)
    return (-1);

  fdeg = refdeg, ncoef = maxcoef;

  for (icoef = 1; icoef <= ncoef; icoef++)
    TCERDD (tide, linext, colcoef[icoef], &coef[icoef], &null);

  return (0);

}

/* -------------------------------------------------------------------------- */

#ifdef __STDC__
void mos_writedisp (int line, int slit, int ypix,
	       double y, int numrow, double rms)
#else
void mos_writedisp (line, slit, ypix, y, numrow, rms)
     int line, slit, ypix, numrow;
     double y, rms;
#endif

{

  int icoef;

  TCEWRI (tide, line, colslit, &slit);
  TCEWRI (tide, line, colline, &ypix);
  TCEWRD (tide, line, coly, &y);
  TCEWRD (tide, line, colrms, &rms);

/*      if (nbline<line) nbline=line;   */
  if (nbline < line)
    nbline = numrow;

  for (icoef = 1; icoef <= maxcoef; icoef++)
    TCEWRD (tide, line, colcoef[icoef], &coef[icoef]);
}

