/* @(#)longmos.c	8.2 (ESO-IPG) 11/18/94 09:23:20 */
/* @(#)longmos.c	1.1 (ESO-IPG) 2/9/94 18:16:33 */

/**********************************************/
/* Subroutine read_col: Read a table column   */
/*    tid: table identifier                   */
/*     nb: number of rows                     */
/*  colnb: column number                      */
/*    col: output array containing the values */  
/**********************************************/

#include <nrutil.h>

#ifdef __STDC__
int read_col(int tid, int nb, int colnb, double col[], double dnull)
#else
int read_col( tid, nb, colnb, col, dnull)
  double col[], dnull;
  int    tid,nb,colnb;
#endif

{
  int i, k=0, null, sel;

  for (i=1; i<=nb; i++) 
    {
     TCSGET(tid, i, &sel);
     if (sel) 
       {
        TCERDD(tid, i, colnb, &col[++k], &null);
        if (null) col[k] = dnull;
       }
    }
  return(k);
}

/*---------------------------------------------------------------------------*/
/* Subroutine write_dcol: Write a table column   */
#ifdef __STDC__
int write_dcol( int tid, int nb, int select[], int colnb, double col[])
#else
int write_dcol ( tid, nb, select, colnb, col)
  double col[];
  int tid,nb,colnb,select[];  
#endif

{
  int i;

  for (i=1; i<=nb; i++)
    TCEWRD(tid, select[i], colnb, &col[i]);
}
