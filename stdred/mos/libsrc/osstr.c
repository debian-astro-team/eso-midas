/* @(#)osstr.c	8.2 (ESO-IPG) 11/18/94 09:23:23 */
/* @(#)osstr.c	1.2 (ESO-IPG) 2/9/94 18:16:12 */
/* @(#)osstr.c	5.1.1.3 (ESO-IPG) 5/19/93 14:20:26 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        str
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Independant string handling interfaces.
.COMMENTS    
 Handling of character strings. These routines normally return
             a non-negative integer number on successful return.
             Otherwise, a value of -1 is set to indicate an error
             condition and the variable ``oserror'' contains the 
             symbolic error code.

.HISTORY     [0.0] 25-Aug-1986   Definition     J. D. Ponz
             [1.0] 10-Apr-1987   Programmation  B. Pirenne
             [1.1] 04-Aug-1987   VMS Includes
             [1.2] 910911	 bzero removed CG.(memset can be used instead)
------------------------------------------------------------*/
#include <errno.h>

#if VMS
#include   "MID_INCLUDE:osparms.h"
#else
#include  <osparms.h>
#endif

char *strstrs( char *s1, char *s2)
/*+++++++++++++++++++++++++++++ strstrs +++++++++++++++++++++++++++++++++++
.PURPOSE  Pattern matching
.RETURNS  returns a pointer to the first occurrence of the pattern string s2
          in s1. Otherwise returns NULL
-------------------------------------------------------------------------*/
   
{
   register char  *t1,*t2;
   char            *ptr;
   
   ptr = (char *)0;
   t2 = s2;
   t1 = s1;
   for (;*t1 != '\0' && *t2 != '\0' ;t1++) {
     if (*t1 == *t2) t2++;
     else {
       if (*t2 != *s2) {
         t1 -= t2 - s2;
	 t2 =  s2; 
       }
     }
   }
   if (*t2 == '\0')  ptr = t1 - strlen(s2);
   return(ptr);
}
