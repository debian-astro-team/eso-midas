% @(#)bias_qc.hlq	11.1.1.1 (ESO-IPG) 05/06/97 13:04:18
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      bias_qc.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: BIAS/QC
%.VERSION    1.0  02-MAY-97 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./QC
\es\co
BIAS/QC					      02-MAY-97  PB
\oc\su
BIAS/QC  result  sof_bias  sof_mbias
	checks a bias frame for quality control
\us\pu
Purpose:    
            The command compares the bias to the master,
            computes average and variances, and adds the 
            result to the quality control table.
\up\sy
Syntax:     
            BIAS/QC result  sof_bias  sof_mbias
 \\
\ys\pa
            result = Result frame (unused parameter)
 \\
\ap\pa
            sof_bias = Name of the ASCII file containing the \\
                    full path of the bias frame to be analyzed. 
 \\
\ap\pa
            sof_mbias = Name of the ASCII file containing the \\
                    full path of the master bias frame used for \\
                    comparison.
 \\
\ap\sa
See also: 
            DARK/QC, FLAT/QC
\as\no
Note:       
            The syntax of the command is adapted to a usage
            within the pipeline environment.
 \\
\on\exs
Examples:
\ex
            BIAS/QC  hh  1997-06-12T19:53:35_sof 1997-06-12T19:53:35_sof_sof
\xe \sxe
