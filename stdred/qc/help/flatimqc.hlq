% @(#)flatimqc.hlq	11.1.1.1 (ESO-IPG) 05/06/97 13:04:18
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      flatimqc.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: FLATIMA/QC
%.VERSION    1.0  02-MAY-97 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./QC
\es\co
FLATIMA/QC					      02-MAY-97  PB
\oc\su
FLATIMA/QC  result sof_flat  sof_mflat
	    checks a flat field exposure for quality
\us\pu
Purpose:    
            The flat is corrected for bias and dark, and
            normalised by its average value. It is then compared
            to the master flat. Average and variances are added
            to the quality control table.
\up\sy
Syntax:     
            FLATIMA/QC result sof_flat  sof_mflat
 \\
\ys\pa
            result = Result frame (unused parameter)
 \\
\ap\pa
            sof_flat = Name of the ASCII file containing the \\
                    full path of the flat frame to be analyzed. 
 \\
\ap\pa
            sof_mflat = Name of the ASCII file containing the \\
                    full path of the master flat, master dark and 
                    master bias frames used for comparison, in that order. 
 \\
\ap\sa
See also: 
            BIAS/QC, DARK/QC 
\as\no
Note:       
            The syntax of the command is adapted to a usage
            within the pipeline environment.
 \\
\on\exs
Examples:
\ex
            FLATIMA/QC  hh  1997-06-12T19:53:35_sof 1997-06-12T19:53:35_sof_sof
\xe \sxe
