! @(#)qcinit.prg	1.2 (ESO-IPG) 05/05/97 14:26:34
!
write/out Set parameters to default value
register/session qc STD_PROC qcinit.prg qckey.tbl ! Initialisation
!
!Begin Session List 
!
write/keyword SUSICAL/C/1/255 "."     ! SUSI directory
write/keyword EMMICAL/C/1/255  "."    ! EMMI directory
write/keyword TABQUAL/C/1/255   "qc.tbl"     !  Current quality table
write/keyword INSQUAL/C/1/25    "SUSI"       !  Current instrument
write/keyword MEDFILT/I/1/2     1,1          !  Size of median filter
write/keyword REJTOL/R/1/2      0.98,1.02    !  Tolerance for rejection
!
!End Session List
!
$$ rm -f answ.dat
$$echo 'if [ -d /tmpSusi/qc ]\;  then echo 'yes'\; fi' | /bin/sh -f > answ.dat

open/file answ.dat READ INPUTI
read/file {inputi(1)} INPUTC 20
close/file {inputi(1)}

if inputc(1:1) .eq. "y" then
    set/qc SUSICAL="/tmpSusi/qc"
else
    write/out "Could not find /tmpSusi/qc. Using local directory"
endif

$$ rm -f answ.dat
$$echo 'if [ -d /tmpEmmi/qc ]\;  then echo 'yes'\; fi' | /bin/sh -f > answ.dat

open/file answ.dat READ INPUTI
read/file {inputi(1)} INPUTC 20
close/file {inputi(1)} 

if inputc(1:1) .eq. "y" then
    set/qc EMMICAL="/tmpEmmi/qc"
else
    write/out "Could not find /tmpEmmi/qc. Using local directory"
endif








