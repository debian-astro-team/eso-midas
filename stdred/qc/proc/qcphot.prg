! @(#)qcflatima.prg	11.1.1.1 (ESO-IPG) 05/06/97 13:15:16
! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure sqc03
! P.Ballester  26.03.97
! 
! this procedure verifies a flat frame for quality control
! 
! param1 = name of result image
! param2 = name of the flat frame
! param3 = name of the corresponding master flat
! param4 = name of the corresponding master dark
! param5 = name of the corresponding master bias
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ima  ! Result frame
define/param p2 ? ima  ! Input photometry standard
define/param p3 ? ima  ! Set of frames containing master flat, 
! master dark, master bias, bad pixel mask, exactly in that order
define/param p4 25 numb ! Half-size of the measurement window
define/param p5 150 numb ! Half-size of the initial search window
!
log/recipe sqcphot in			!log time of procedure start
echo/full
!
define/local count/i/1/1 0
define/local mean/R/1/2  0.,0.
define/local ron/R/1/2   0.,0.
define/local status/I/1/1 0
define/local catal/i/1/2 0,0
!
@ set_of_frames IN {p2} sqc01a		!convert FITS input frames
count = outputi(1)		!save no. of frames 
if count .lt. 1 return/exit
!
@ set_of_frames IN {p3} sqc01b		!convert FITS input frames
count = outputi(1)		!save no. of frames 
if count .lt. 3 return/exit
!
define/local work2/c/1/80 " "
define/local work3/c/1/80 " "
define/local work4/c/1/80 " "
define/local work5/c/1/80 " "
! 
store/frame work2 sqc01a.cat 1 finito
finito:
store/frame work3 sqc01b.cat 2 finita   ! Master flat
store/frame work4 sqc01b.cat 2 finita   ! Master dark
store/frame work5 sqc01b.cat 2 finita   ! Master bias
finita:
!
define/local gain/R/1/1 0.
define/local exd/I/1/1 0
gain = 0.
exd = m$existd(work2, ESO.DET.OUT1.CF)
if exd .eq. 1 gain = m$value({work2},ESO.DET.OUT1.CONAD)
exd = m$existd(work2, ESO.DET.OUT4.CF)
if exd .eq. 1 gain = m$value({work2},ESO.DET.OUT1.CONAD)
exd = m$existd(work2, ESO.DET.OUT1.CONAD)
if exd .eq. 1 gain = m$value({work2},ESO.DET.OUT1.CONAD)
exd = m$existd(work2, ESO.DET.OUT4.CONAD)
if exd .eq. 1 gain = m$value({work2},ESO.DET.OUT4.CONAD)
if gain .le. 0. then
   write/out "Error: No valid gain value. Taking 2.5 e-/ADU"
   gain = 2.5
endif
!
define/local exptime/R/1/1 0.
exptime = m$value({work2},O_TIME(7))
!
! bias and dark current correction
compute/image &v =  "{work2}" - "{work4}"*{exptime} - "{work5}"
! FF correction and conversion to electrons
compute/image {P1} = {gain}*&v/"{work3}" 
! All values in the ETC are multiplied by the exposure time so
! we don't need normalization (/{exptime})  
!
define/local mesbox/I/1/4 0,0,0,0 ! Measurement box
define/local maxdata/I/1/3 0,0,0
!
mesbox(1) = {{P1},NPIX(1)}/2 - {P5} ! X minimum
mesbox(2) = {{P1},NPIX(2)}/2 - {P5} ! Y minimum
mesbox(3) = {{P1},NPIX(1)}/2 + {P5} ! X maximum
mesbox(4) = {{P1},NPIX(2)}/2 + {P5} ! Y maximum
!
stat/image {P1} [@{mesbox(1)},@{mesbox(2)}:@{mesbox(3)},@{mesbox(4)}]
!
maxdata(1) = outputr(2) ! Value of the maximum
maxdata(2) = outputi(4) ! X position of the maximum
maxdata(3) = outputi(5) ! Y position of the maximum
!
mesbox(1) = maxdata(2) - {P4} ! X minimum
mesbox(2) = maxdata(3) - {P4} ! Y minimum
mesbox(3) = maxdata(2) + {P4} ! X maximum
mesbox(4) = maxdata(3) + {P4} ! Y maximum
!
create/table &mst 4 4 
create/column &mst :XSTART
create/column &mst :XEND
create/column &mst :YSTART
create/column &mst :YEND
write/table &mst :XSTART @1 {mesbox(1)}
write/table &mst :XEND   @1 {mesbox(3)}
write/table &mst :YSTART @1 {mesbox(2)}
write/table &mst :YEND   @1 {mesbox(4)}
!
center/gauss {P1},&mst &gcnt
define/local gfit/R/1/4 0,0,0,0
gfit(1) = m$value(&gcnt,:XFWHM,@1)
gfit(2) = m$value(&gcnt,:YFWHM,@1)
gfit(3) = m$value(&gcnt,:ICENT,@1)
!
stat/image {P1} [@{mesbox(1)},@{mesbox(2)}:@{mesbox(3)},@{mesbox(4)}]
gfit(4) = outputr(7)
!
mesbox(1) = maxdata(2) - {P5} ! X minimum
mesbox(2) = maxdata(3) - {P4} - {P5} ! Y minimum
mesbox(3) = maxdata(2) + {P5} ! X maximum
mesbox(4) = maxdata(3) - {P4} ! Y maximum
!
define/local gsky/R/1/2 0,0
!extract/image &e = {P1} [@{mesbox(1)},@{mesbox(2)}:@{mesbox(3)},@{mesbox(4)}]
copy/ii {P1} &e ! Only for the time being
sort/col  &e &f
!stat/image &f [<,<:>,@{P4}]
stat/image &f [@{mesbox(1)},@100:@{mesbox(3)},@200]
gsky(1) = outputr(3)
gsky(2) = outputr(4)
!
write/out "Average sky level: {gsky(1)} +/- {gsky(2)} e-/s"
write/out "FWHM in x and y:   {gfit(1)},{gfit(2)} pixels" 
write/out "Peak pixel value:  {maxdata(1)} e-/s/pixel"
write/out "Integrated intensity: {gfit(4)} e-/s"
!
!status/qc {mean(2)} {ron(2)}
status = 0
!
ADD/QCTAB PHOTOM {work2} {status} {gfit(1)} {gfit(2)} {gfit(4)} {maxdata(1)} {gsky(1)},{gsky(2)}
!
! until quality control takes over we put the FITS keywords
! ESO.PRO.CATG and ESO.PRO.QC.STATUS into the result file
! this section has to be removed once quality control is implemented...
!
!write/descr ssr04 ESO.PRO.CATG/c/1/12      "MEAN_FF     "
!write/descr ssr04 ESO.PRO.QC.STATUS/c/1/12 "CHECKED     "
!
!@ set_of_frames OUT ssr04 {resframe}
! 
log/recipe sqcphot out 	!log time of procedure end + set return status





