! @(#)qcbias.prg	11.1.1.1 (ESO-IPG) 05/06/97 13:15:15
! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure sqc01
! P.Ballester  26.03.97
! KB  970520
! PB  970611
! 
! this procedure verifies a bias frame for quality control
! 
! param1 = name of result image
! param2 = name of the bias frame
! param3 = name of the corresponding master bias
! 
! Assumed: quality control result table (qc.tbl)
! +++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ima  ! result image 
define/param p2 ? char ! input bias image
define/param p3 ? char ! corresponding master bias
!
echo/full 1,2
log/recipe sqc01 in			!log time of procedure start
!
define/local QCTAB/C/1/60  "qc.tbl" 
!
define/local mean/R/1/2  0.,0.
define/local ron/R/1/2   0.,0.
define/local status/I/1/1 -1
define/local catal/i/1/2 0,0
!
@ set_of_frames IN {p2} sqc01a		!convert FITS input frames
if outputi(1) .ne. 1 return/exit
!
@ set_of_frames IN {p3} sqc01b		!convert FITS input frames
if outputi(1) .ne. 1 return/exit
! 
define/local work2/c/1/80 " "
define/local work3/c/1/80 " "
! 
store/frame work2 sqc01a.cat 1 finito
finito:
store/frame work3 sqc01b.cat 2 finita
finita:
!
compute/image &r =  "{work2}"/"{work3}"
!
define/local imsize/I/1/2 0,0
copy/dk {work2} NPIX/I/1/2 imsize/I/1/2

if imsize(1) .gt. 200 .and. imsize(2) .gt. 200 then
  imsize(1) = imsize(1) - 100
  imsize(2) = imsize(2) - 100
!
  filter/median  {work2} &f  {medfilt(1)},{medfilt(2)}
  statist/image  &f [@100,@100:@{imsize(1)},@{imsize(2)}]
  mean(1) = outputr(3)
  ron(1)  = outputr(4)
!
  filter/median &r &f {medfilt(1)},{medfilt(2)}
  statist/image &f [@100,@100:@{imsize(1)},@{imsize(2)}]
  mean(2) = outputr(3)
  ron(2)  = outputr(4)
!
  status/qc {mean(2)} {ron(2)}
  status = inputi(1)
endif
!
ADD/QCTAB  BIAS {work2} {status}  {mean(1)}  {ron(1)} {mean(2)}  {ron(2)} 
!
! until quality control takes over we put the FITS keywords
! ESO.PRO.CATG and ESO.PRO.QC.STATUS into the result file
! this section has to be removed once quality control is implemented...
!
!write/descr ssr04 ESO.PRO.CATG/c/1/12      "MEAN_FF     "
!write/descr ssr04 ESO.PRO.QC.STATUS/c/1/12 "CHECKED     "
!
!@ set_of_frames OUT ssr04 {resframe}
! 
log/recipe sqc01 out 	!log time of procedure end + set return status




