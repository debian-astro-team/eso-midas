! @(#)qcaddtab.prg	11.1.1.1 (ESO-IPG) 05/06/97 13:15:15
! Updated by P.Ballester on June 11,1997 
! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure addQC
! P.Ballester  26.03.97
! KB 970520
! 
! this procedure updates the quality control table
! 
! param1 = mode (BIAS, DARK, FLAT)
! param2 = name of the processed frame
! param3 = status result (0/1)
! param4 to param8: QC results (depending on mode)
! 
! Assumed: quality control result table (qc.tbl)
! +++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param P1 ? CHA  "Type:  BIAS, DARK, FLAT"
define/param P2 ? IMA  "Reference Image:"
!
define/local nrows/I/1/1 0
define/local qctab/C/1/60  {TABQUAL} 
define/local inst/C/1/20   {{P2},INSTRUME}

create/qctab qc.tbl  {inst} 
write/key qctab/C/1/60 {tabqual}

show/table {qctab}
read/descr {p2} O_TIME
copy/dk {qctab} TBLCONTR/I/4/1  NROWS/I/1/1

nrows = nrows + 1

define/local date/C/1/30  ""
define/local time/D/1/1   0.
date = M$TIME()
time = M$SECS()

write/table {qctab} :DATE @{nrows} "{date}"
write/table {qctab} :TIME @{nrows}  {time}
! To preserve full accuracy the following redundant line
{qctab},:TIME,@{nrows} = time

define/local dex/I/1/1  0

write/table {qctab} @{nrows} :EXPTIME   {{P2},O_TIME(7)}
write/table {qctab} @{nrows} :DETECTOR  {{P2},ESO.DET.ID}

dex = M$EXISTD(P2,"ESO.DET.MODE")
if dex .eq. 1  write/table {qctab} @{nrows} :DET_MODE  {{P2},ESO.DET.MODE}

write/table {qctab} @{nrows} :FILTER    {{P2},ESO.INS.FILT1.NAME}
write/table {qctab} @{nrows} :RB_ID     "000"
write/table {qctab} @{nrows} :ARCNAME   {P2}
write/table {qctab} @{nrows} :STATUS    {P3}

define/local detqc/I/1/1 0

if p1(1:1) .EQ. "B" THEN
   write/table {qctab} @{nrows} :QC_ACTION "BIAS"
   write/table {qctab} @{nrows} :BIAS      {P4}
   detqc = 1  
endif

if p1(1:1) .EQ. "D" THEN
   write/table {qctab} @{nrows} :QC_ACTION "DARK"
   write/table {qctab} @{nrows} :DARK      {P4}
   detqc = 1       
endif

if p1(1:1) .EQ. "F" THEN
   write/table {qctab} @{nrows} :QC_ACTION "FLAT"
   write/table {qctab} @{nrows} :FLAT      {P4}  
   detqc = 1  
endif

if detqc .eq. 1 then
   write/table {qctab} @{nrows} :RMS       {P5}  
   write/table {qctab} @{nrows} :RATIO_MST {P6}  
   write/table {qctab} @{nrows} :RMS_MST   {P7}  
endif

if p1(1:1) .eq. "P" THEN
   define/local skyvalue/R/1/2 {P8}
   write/table {qctab} @{nrows} :QC_ACTION "PHOT"
   write/table {qctab} @{nrows} :XFWHM     {P4}  
   write/table {qctab} @{nrows} :YFWHM     {P5}  
   write/table {qctab} @{nrows} :COUNTS    {P6}
   write/table {qctab} @{nrows} :PEAK      {P7}   
   write/table {qctab} @{nrows} :SKY       {skyvalue(1)}
   write/table {qctab} @{nrows} :RMS       {skyvalue(2)}
endif

define/local updtim/D/1/1 0.
updtim = m$secs()

write/descr {qctab} LASTUPD/D/1/1 0.
copy/kd updtim {qctab} LASTUPD

