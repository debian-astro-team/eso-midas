! @(#)qcflatima.prg	11.1.1.1 (ESO-IPG) 05/06/97 13:15:16
! Updated by P.Ballester on June 11,1997 
! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure sqc03
! P.Ballester  26.03.97
! 
! this procedure verifies a flat frame for quality control
! 
! param1 = name of result image
! param2 = name of the flat frame
! param3 = name of the corresponding master flat
! param4 = name of the corresponding master dark
! param5 = name of the corresponding master bias
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ima  ! Result frame
define/param p2 ? ima  ! Set of flat frame to be tested
define/param p3 ? ima  ! Set of frames containing master flat, 
! master dark, master bias, bad pixel mask, exactly in that order
!
log/recipe sqc03 in			!log time of procedure start
!
define/local count/i/1/1 0
define/local mean/R/1/2  0.,0.
define/local ron/R/1/2   0.,0.
define/local status/I/1/1 0
define/local catal/i/1/2 0,0
!
@ set_of_frames IN {p2} sqc01a		!convert FITS input frames
count = outputi(1)		!save no. of frames 
if count .lt. 1 return/exit
!
@ set_of_frames IN {p3} sqc01b		!convert FITS input frames
count = outputi(1)		!save no. of frames 
if count .lt. 3 return/exit
!
define/local work2/c/1/80 " "
define/local work3/c/1/80 " "
define/local work4/c/1/80 " "
define/local work5/c/1/80 " "
! 
store/frame work2 sqc01a.cat 1 finito
finito:
store/frame work3 sqc01b.cat 2 finita   ! Master flat
store/frame work4 sqc01b.cat 2 finita   ! Master dark
store/frame work5 sqc01b.cat 2 finita   ! Master bias
finita:
!
define/local exptime/R/1/1 0.
exptime = m$value({work2},O_TIME(7))
!
compute/image &v =  "{work2}" - "{work4}"*{exptime} - "{work5}"
statist/image &v
compute/image &r = &v/{outputr(3)}/"{work3}"
! 
filter/median &v &f {medfilt(1)},{medfilt(2)}
statist/image  &f
mean(1) = outputr(3)
ron(1)  = outputr(4)
filter/median &r &f {medfilt(1)},{medfilt(2)}
statist/image &f
mean(2) = outputr(3)
ron(2)  = outputr(4)
!
status/qc {mean(2)} {ron(2)}
status = inputi(1)
!
ADD/QCTAB  FLAT {work2} {status}  {mean(1)}  {ron(1)}  {mean(2)}  {ron(2)} 
!
! until quality control takes over we put the FITS keywords
! ESO.PRO.CATG and ESO.PRO.QC.STATUS into the result file
! this section has to be removed once quality control is implemented...
!
!write/descr ssr04 ESO.PRO.CATG/c/1/12      "MEAN_FF     "
!write/descr ssr04 ESO.PRO.QC.STATUS/c/1/12 "CHECKED     "
!
!@ set_of_frames OUT ssr04 {resframe}
! 
log/recipe sqc03 out 	!log time of procedure end + set return status


