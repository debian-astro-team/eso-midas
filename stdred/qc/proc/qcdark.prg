! @(#)qcdark.prg	11.1.1.1 (ESO-IPG) 05/06/97 13:15:15
! Updated by P.Ballester on June 11,1997 
! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure sqc02
! P.Ballester  26.03.97
! KB  970520
! PB  970611
! 
! this procedure verifies a dark frame for quality control
! 
! param1 = name of result image
! param2 = name of the dark frame
! param3 = name of the corresponding master dark
! param4 = name of the corresponding master bias
! 
! Assumed: quality control result table (qc.tbl)
! +++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ima  ! Result image
define/param p2 ? cha  ! Dark frame to be evaluated
define/param p3 ? char ! set of frames containing dark, bias in that order  
!
log/recipe sqc02 in			!log time of procedure start
!
define/local count/i/1/1 0
define/local mean/R/1/2  0.,0.
define/local ron/R/1/2   0.,0.
define/local status/I/1/1 0
define/local catal/I/1/2  0,0
!
@ set_of_frames IN {p2} sqc01a		!convert FITS input frames
count = outputi(1)		!save no. of frames 
if count .lt. 1 return/exit
!
@ set_of_frames IN {p3} sqc01b		!convert FITS input frames
count = outputi(1)		!save no. of frames 
if count .lt. 2 return/exit
!
define/local work2/c/1/80 " "
define/local work3/c/1/80 " "
define/local work4/c/1/80 " "
! 
store/frame work2 sqc01a.cat 1 finito
finito:
store/frame work3 sqc01b.cat 2 finita
store/frame work4 sqc01b.cat 2 finita
finita:
!
define/local exptime/R/1/2 0.,0.
exptime(1) = m$value({work2},O_TIME(7))
exptime(2) = m$value({work3},O_TIME(7))
!
compute/image &v =  ("{work2}" - "{work4}")/{exptime(1)}
compute/image &r = &v*{exptime(2)}/"{work3}"
!
define/local gain/R/1/1 0.
define/local exd/I/1/1 0

gain = 0.
exd = m$existd(work2, ESO.DET.OUT1.CONAD)
if exd .eq. 1 gain = m$value({work2},ESO.DET.OUT1.CONAD)
exd = m$existd(work2, ESO.DET.OUT4.CONAD)
if exd .eq. 1 gain = m$value({work2},ESO.DET.OUT4.CONAD)
if gain .le. 0. then
   write/out "Error: No valid gain value. Taking 2.5 e-/ADU"
   gain = 2.5
endif
!
compute/image &w = &v*{gain}*3600.
filter/median &w &f {medfilt(1)},{medfilt(2)}
statist/image  &f
mean(1) = outputr(3)
ron(1)  = outputr(4)
filter/median &r &f {medfilt(1)},{medfilt(2)}
statist/image &f
mean(2) = outputr(3)
ron(2)  = outputr(4)
!
status/qc {mean(2)} {ron(2)}
status = inputi(1)
!
ADD/QCTAB  DARK  {work2} {status}  {mean(1)}  {ron(1)}  {mean(2)}  {ron(2)} 
!
! until quality control takes over we put the FITS keywords
! ESO.PRO.CATG and ESO.PRO.QC.STATUS into the result file
! this section has to be removed once quality control is implemented...
!
!write/descr ssr04 ESO.PRO.CATG/c/1/12      "MEAN_FF     "
!write/descr ssr04 ESO.PRO.QC.STATUS/c/1/12 "CHECKED     "
!
!@ set_of_frames OUT ssr04 {resframe}
! 
log/recipe sqc02 out 	!log time of procedure end + set return status

