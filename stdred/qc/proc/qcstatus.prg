define/param P1 ? NUMB "Normalized average"
define/param P2 ? NUMB "Normalized variance"

define/local bounds/R/1/2 0.,0.

bounds(1) = {P1} - {P2}
bounds(2) = {P1} + {p2} 

inputi(1) = 0
if bounds(1) .gt. rejtol(2) .or. bounds(2) .lt. rejtol(1) inputi(1) = 1

return
