! @(#)qcdeltab.prg	11.1.1.1 (ESO-IPG) 05/06/97 13:15:15
! Updated by P.Ballester on June 11,1997 
! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure tcreaQC
! P.Ballester  26.03.97
! 
! this procedure creates the quality control table
! 
! param1 = name of the table
! 
! Assumed: quality control result table (qc.tbl)
! +++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
delete/table {tabqual}
