! @(#)qcreatab.prg	11.1.1.1 (ESO-IPG) 05/06/97 13:15:16
! Updated by P.Ballester on June 11,1997 
! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure qcreatab.prg
! P.Ballester  26.03.97
! 
! this procedure creates the quality control table
! 
! param1 = name of the table
! 
! Assumed: quality control result table (qc.tbl)
! +++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param P1 qc.tbl TABL "SUSI QC Table name:"
define/param P2 SUSI   CHAR "Instrument (SUSI/EMMI):"
define/param P3 NO     CHAR "Force creation (Yes/No/Update):"

define/local name/C/1/255
define/local exist/I/1/1 0

if P2(1:1) .eq. "S" write/key name {SUSICAL}/{P1}
if P2(1:1) .eq. "E" write/key name {EMMICAL}/{P1}

exist = m$exist(name)
set/qc TABQUAL={name}

if exist .eq. 0 .or. P3(1:1) .eq. "y"  then

  create/table {name} 12 12 
  exist = 0

endif

if exist .eq. 0  .or. P3(1:1) .eq. "u"  then

  create/column  {name}  :DATE        C*30 "QC Date"
  create/column  {name}  :TIME        R*8  "Seconds since 1/1/70"  
  create/column  {name}  :RB_ID       C*20 "Reduction Block ID" A4
  create/column  {name}  :STATUS      I*3  "QC Status"     I2
  create/column  {name}  :QC_ACTION   C*12 "QC Action"     A6
  create/column  {name}  :BIAS        R*8  "level, ADU"    F6.2
  create/column  {name}  :DARK        R*8  "level, e-/hr"  F6.2
  create/column  {name}  :FLAT        R*8  "Aver. level"   F10.2
  create/column  {name}  :RMS         R*8  "Std Dev."      F6.2
  create/column  {name}  :RATIO_MST   R*8  "Unitless"      F6.2
  create/column  {name}  :RMS_MST     R*8  "Unitless"      F6.2
  create/column  {name}  :XFWHM       R*4  "Pixel"      F6.2
  create/column  {name}  :YFWHM       R*4  "Pixel"      F6.2
  create/column  {name}  :COUNTS      R*4  "Integ. int"  F10.2 "e-/s"
  create/column  {name}  :PEAK        R*4  "Peak count" F10.2 "e-/s/pixel"
  create/column  {name}  :SKY         R*4  "sky level"  F10.2 "e-/s/pixel"
  create/column  {name}  :FILTER      C*20 "Filter"    
  create/column  {name}  :DETECTOR    C*20 "Detector"
  create/column  {name}  :DET_MODE    C*20 "Detector Mode"  
  create/column  {name}  :EXPTIME     R*8  "Exposure Time"  F6.1
  create/column  {name}  :ARCNAME     C*80 "Archive Name"  A30

endif

define/local updtim/D/1/1 0.
updtim = m$secs()

write/descr {tabqual} LASTUPD/D/1/1 0.
copy/kd updtim {tabqual} LASTUPD

