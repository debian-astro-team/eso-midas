define/param P1 SUSI      CHAR "Instrument"
define/param P2 ALL       CHAR "Parameter (BIAS/DARK/FLAT/ALL)"
define/param P3 -72.,0.   NUMB "Maximum number of values displayed"
define/param P4 0.8,1.2   NUMB "Limits of the graph"
define/param P5 {rejtol(1)},{rejtol(2)} NUMB "Tolerance limits"

!**********************************************************
!**** Initialisations
!**********************************************************

define/local INSTRUM/C/1/20 "SUSI" ? +lower
define/local MODE/C/1/4  "BIAS"
set/format F20.10

if P1(1:1) .eq. "S" then
      write/key INSTRUM/C/1/20  "SUSI    "
endif
if P1(1:1) .eq. "E" then
      write/key INSTRUM/C/1/20  "EMMI    "
endif

create/qc ? {INSTRUM}
copy/table {TABQUAL} &q

if P2(1:1) .eq. "B" write/key MODE/C/1/4 "BIAS"
if P2(1:1) .eq. "D" write/key MODE/C/1/4 "DARK"
if P2(1:1) .eq. "F" write/key MODE/C/1/4 "FLAT"


!**********************************************************
!**** Preparing the axes
!**********************************************************

define/local xaxis/D/1/5  0.,0.,0.,0.,0. ? +lower
define/local yaxis/R/1/2  {P4} ? +lower
define/local ytol/R/1/2   {P5} ? +lower

if P2(1:1) .ne. "A" then 
  select/table &q  :QC_ACTION.EQ."{MODE}" >Null
else
  select/table &q ALL >Null
  outputi(1) = {middummq.tbl,TBLCONTR(4)}
ENDIF

define/local nbsel/I/1/1 {outputi(1)}

if nbsel .le. 0 then
   write/out "No Quality Control Values available"
   return
else
   copy/table &q &qc
endif

define/local line/I/1/2 0,0
define/local timlim/D/1/2  {p3},{p3}

if timlim(1) .gt. 0. then
 if nbsel .gt. {P3} then
   line(2) = {middummqc.tbl,TBLCONTR(4)}
   line(1) = line(2) - {P3} 
   select/table &qc SEQ.GT.{line(1)} >Null
 endif
 copy/table &qc &q
endif

sort/table &q :TIME
copy/tk middummq.tbl :TIME @1 xaxis/D/3/1
copy/tk middummq.tbl :TIME @{middummq.tbl,TBLCONTR(4)} xaxis/D/4/1

compute/table &q :TIMEC = (:TIME - {xaxis(4)})/3600.

if timlim(1) .le. 0. then
   select/table &q :TIMEC.GE.{TIMLIM(1)}.AND.:TIMEC.LE.{TIMLIM(2)}
   copy/table &q &qc
   copy/table &qc &q
endif

sort/table &q :TIMEC
copy/tk middummq.tbl :TIMEC @1 xaxis/D/3/1
copy/tk middummq.tbl :TIMEC @{middummq.tbl,TBLCONTR(4)} xaxis/D/4/1

xaxis(5) = xaxis(4) - xaxis(3)
xaxis(1) = xaxis(3) - xaxis(5)*0.05
xaxis(2) = xaxis(4) + xaxis(5)*0.05

set/graph
set/graph pmode=1 font=1 xformat=F10.3 yformat=F10.3
set/graph xaxis={xaxis(1)},{xaxis(2)} 
set/graph yaxis={yaxis(1)},{yaxis(2)}
@ creifnot 1
plot/axes ? ? ? "Time (elapsed hours)"  "Normalized value"
overplot/line 1 {xaxis(1)},{ytol(1)} {xaxis(2)},{ytol(1)}
overplot/line 1 {xaxis(1)},{ytol(2)} {xaxis(2)},{ytol(2)}
overplot/line 2 {xaxis(1)},1.0       {xaxis(2)},1.0

define/local xylabel/R/1/2 0.,0. ? +lower
xylabel(1) = (xaxis(1) + xaxis(2))/2.
xylabel(2) = yaxis(1) + 1.025*(yaxis(2)-yaxis(1))
define/local chlabel/C/1/30 "Quality Control on {INSTRUM}"
label/graph "{chlabel}" {xylabel(1)},{xylabel(2)} 

!**********************************************************
!**** Now preparing the plot
!**********************************************************

if P2(1:1) .ne. "A" then 
  @s qcplot,parplt {MODE} 
else
  @s qcplot,parplt BIAS  1  0.95
  @s qcplot,parplt DARK  4  0.90
  @s qcplot,parplt FLAT  2  0.85
ENDIF

RETURN

!**********************************************************
!**** Subroutine plotting a single parameter
!**********************************************************

ENTRY PARPLT

define/param P1 ? CHAR "Mode:"
define/param P2 4 NUMB "Graph Color Index:"   0,7
define/param P3 0.95  NUMB "Relative label position:" 0.,1.

define/local cnt/I/1/1 0
set/graph color={P2}

xylabel(1) = (xaxis(1) + xaxis(2))/2.
xylabel(2) = yaxis(1) + {P3}*(yaxis(2)-yaxis(1))
define/local chlabel/C/1/30 "{P1}"
label/graph "{chlabel}" {xylabel(1)},{xylabel(2)} 

select/table &q :QC_ACTION.EQ."{P1}" >Null
if outputi(1) .le. 0  return
overplot/error &q :TIMEC  :RATIO_MST :RMS_MST 
select/table &q SELECT.AND.:RATIO_MST.LT.{yaxis(1)}.OR.:RATIO_MST.GT.{yaxis(2)} >Null

if outputi(1) .gt. 0 then
   copy/table &q &qc
   do cnt = 1 {middummqc.tbl,TBLCONTR(4)}
      xaxis(3) = {middummqc.tbl,:TIMEC,@{cnt}}
      overplot/line 1 {xaxis(3)},{yaxis(1)} {xaxis(3)},{yaxis(2)}
   enddo
endif

return

!**********************************************************
!**** Subroutine plotting a single parameter
!**********************************************************
ENTRY TRACK

define/param P1 10 NUMB "Update period (secs)"
define/param P2 SUSI CHAR "Inst"

define/local lastupd/D/1/2 0.,0.

create/qc ? {P2}

loop:
copy/dk {tabqual} LASTUPD LASTUPD/D/2/1
if lastupd(2) .gt. lastupd(1) then
    @s qcplot {P2} {P3} {P4} {P5} {P6} {P7} {P8}
    lastupd(1) = m$secs()
endif
wait/secs {P1}
goto loop

RETURN




