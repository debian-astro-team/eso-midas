$ ! @(#)preinstall.com	19.1 (ESO-IPG) 02/25/03 14:23:49 
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1987 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       PREINSTALL.COM
$ ! .COMMENTS    Preinstallation procedure ['MIDASHOME'.'MIDVERS'.STDRED.LIB]
$ !              Create Midas libraries.
$ ! .REMARKS     Genereted by hand.
$ ! .DATE        Thu Dec  1 12:24:17 MET 1988
$ ! .DATE        900316  KB
$ ! .DATE        910110  CG
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ SET VERIFY
$ !
$ LIB/CREATE LIBASTRO
$ LIB/CREATE LIBSPEC
$ LIB/CREATE LIBDO
$ LIB/CREATE LIBCCD
$ LIB/CREATE LIBOPTO
$ LIB/CREATE LIBMOS
$ LIB/CREATE LIBECH
$ LIB/CREATE LIBECH_NR
$ !
$ SET NOVER
$ PURGE
