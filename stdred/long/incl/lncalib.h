/* @(#)lncalib.h	19.1 (ESO-IPG) 02/25/03 14:24:34 */
/* Definition of rejection codes */

#define  NOT_PROCESSED     0
#define  SUCCESSFULL_MATCH 1
#define  MATCH_FAILED     -1
#define  REACHED_LIMIT    -3
#define  RESIDUAL_GT_TOL  -5

