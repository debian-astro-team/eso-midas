/* @(#)main_defs.h	19.1 (ESO-IPG) 02/25/03 14:24:34 */
/* @(#)gm_defs.h    1.0.0.0 (ESO-La Silla) 10/08/91 12:00:00 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        gm_defs.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Contain extern declarations of variables and 
              functions, #includes, #defines and typedefs 
              related to the graphical interface of Spectra.
.KEYWORDS     global variables, X11
.COMMENTS     Athena widgets implementation.
.VERSION 1.0  1-Mar-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

/* types of plots for verification of calibration */
#define PLOT_NONE	0
#define PLOT_IDENT	1
#define PLOT_WAVE	2
#define PLOT_RESIDUAL	3
#define PLOT_SPLABEL	4

/* types of plots for response curve generation */
#define PLOT_RATIO_WAVE	0
#define PLOT_MAGN_WAVE	1
#define PLOT_MAGN_FREQ	2

/* label or no label the spectrum representing the starting line */
#define LABEL_SPEC	0
#define NO_LABEL	1

/* options for rebinning */
#define LINEAR		0
#define QUADRATIC	1
#define SPLINE		2

/* parameters for identification */
#define IDBEGIN		0
#define IDCONT		1

/* parameters of update_line_table() */
#define ADDMARK		0
#define DELMARK		1

/* for values in the :ERASED column in the line table */
#define VAL_ERASED	'-'
#define VAL_INVALID	'x'
#define VAL_OK		' '

extern float 	Rnull;		/* null values for Midas */
extern int 	Inull;
extern double 	Dnull;

/*************************************************************
  Definitions related to the addition of spectral lines
 *************************************************************/

extern int	NToAdd; 	/* current no. of spectral lines to add */
extern float	LinesToAdd[]; 	/* x-coords of lines to add */
