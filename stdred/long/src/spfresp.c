/*===========================================================================
  Copyright (C) 1991-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        spresp.c
.MODULE       main program -- spfresp.exe
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      This programs generates the response curve.
.KEYWORDS     response curve, flux calibration
.VERSION 1.0  1-Apr-1991   Implementation

 090728		last modif
------------------------------------------------------------*/


#include <stdio.h>
#include <midas_def.h>

#define RESP		"resp"
#define WAVE_COLOUR	1
#define FREQ_COLOUR	2

extern void free_dvector(), free_fvector();

float *fvector();
double *dvector();
double eval_dpoly();

int main()
{
    int  unit;		/* useless */
    int nulval, actval;	/* useless */
    float xout, *yout, *a, *yin;
    double *d_a;
    int i, npix, idf, idt;
    float start, step;
    int param[2], fit_deg, plot_type;

    SCSPRO( "SPFRES" );

    SCFOPN( RESP, D_R4_FORMAT, 0, F_IMA_TYPE, &idf );

    SCKRDI( "INPUTI", 1, 2, &actval, param, &unit, &nulval );
    plot_type = param[0];
    fit_deg  = param[1]+1;
    SCDRDI( idf, "NPIX", 1, 2, &actval, &npix, &unit, &nulval );

    TCTOPN( RESP, F_I_MODE, &idt );
    SCDRDR( idt, "WSTART", 1, 1, &actval, &start, &unit, &nulval );
    SCDRDR( idt, "WSTEP", 1, 1, &actval, &step, &unit, &nulval );
    TCTCLO(idt);

    a = fvector(0, fit_deg - 1);
    SCDRDR( idf, "COEFF", 1, fit_deg, &actval, a, &unit, &nulval );

    yin   = fvector(0, npix - 1);
    yout  = fvector(0, npix - 1);

    SCFGET( idf, 1, npix, &nulval, (char *)yin );

    d_a = dvector(0, fit_deg - 1);
    for ( i = 0; i < fit_deg; i++ ) /* because of a bug in SCDRDD */
	d_a[i] = a[i];

    for ( i = 0; i < npix; i++ )
	if ( plot_type == FREQ_COLOUR ) {
	    xout = 1.0 / (start + step * i);
	    yout[i] = eval_dpoly( xout, d_a - 1, fit_deg );
	}

    SCDWRR( idf, "START", &start, 1, 1, &unit );
    SCDWRR( idf, "STEP", &step, 1, 1, &unit );

    SCFPUT( idf, 1, npix, (char *)yout );

    free_dvector(d_a, 0, fit_deg - 1);
    free_fvector(a, 0, fit_deg - 1);
    free_fvector(yin, 0, npix - 1);
    free_fvector(yout, 0, npix - 1);

    SCSEPI();
return 0;
}

