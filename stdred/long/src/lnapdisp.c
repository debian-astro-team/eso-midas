/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       lnapdisp.c				   */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    Spectroscopy, Long-Slit                    */
/* .PURPOSE     Wavelength Calibration for 1D, Long and MOS*/
/* .VERSION     1.0  Package Creation  15-JAN-1993         */
 
/*  090728	last modif				   */
/* ------------------------------------------------------- */

#include <tbldef.h>
#include <midas_def.h>
#include <ok.h>
#include <lndisp.h>

#define   ipos(col,row,siz)              row * siz + col

void rebin(), write_dcol();




int main()

{

   char      inframe[60], coerbr[60], outtab[60]; /* Table names */
   int       status, actvals, null; /* Std Interfaces variables */
   int       naxis, npix[2], imno, ypix, kunit;
   double    start[2], step[2], y_pos[2];
   double    *x_world, *lambda, *bin;
   char      ident[80], cunit[64];
   double    *image;
   int       colw, colb, colf, tid;

   int       debug = FALSE;

if (!debug) {

   SCSPRO("lnapdisp");

   status = SCKGETC("IN_A", 1, 60, &actvals, inframe);
   if (status)
      SCTPUT("Error while reading IN_A");

   status = SCKGETC("IN_B", 1, 60, &actvals, coerbr);
   if (status)
      SCTPUT("Error while reading IN_B");

   status = SCKGETC("OUT_A", 1, 60, &actvals, outtab);
   if (status)
      SCTPUT("Error while reading OUT_A");


 }

else {
  SCSPRO("-1");
  strcpy(inframe,"arc0001");
  strcpy(outtab,"reb1");
  strcpy(coerbr,"coerbr");
}

  SCKRDD("INPUTD", 1,  2, &actvals, y_pos, &kunit, &null);

  strcpy(ident," ");
  strcpy(cunit, " ");
  SCIGET(inframe, D_R8_FORMAT, F_I_MODE, F_IMA_TYPE, 2, &naxis, npix, start,
         step, ident, cunit, (char**)&image, &imno);

  if (TCTINI(outtab, F_TRANS, F_O_MODE, 5, npix[0], &tid))
            SCTPUT("**** Error while creating output table");

  TCCINI(tid, D_R8_FORMAT, 1, "F8.2", "Lambda",  "LAMBDA", &colw);
  TCCINI(tid, D_R8_FORMAT, 1, "F8.2", "Bin size","BIN",    &colb);
  TCCINI(tid, D_R8_FORMAT, 1, "F8.2", "Flux",    "FLUX",   &colf);

  if (naxis == 1)  {npix[1] = 1; start[1] = 1.; step[1] = 1.;}

  x_world = (double *)   osmmget(npix[0]*sizeof(double));
  lambda  = (double *)   osmmget(npix[0]*sizeof(double));
  bin     = (double *)   osmmget(npix[0]*sizeof(double));

  /* Do the job */

  if (y_pos[0] < 0)  ypix = y_pos[1];
  else               ypix = (int) ((y_pos[1] - start[1])/step[1] + 0.5);

  rebin(coerbr, x_world, lambda, bin, start, step, npix, ypix);

  /* For 1D images, the value ypix is considered as the position where
     dispersion coefficients are read. y values are decremented by 1 because
     arrays start at 0 */

  if (naxis == 1)  ypix = 1;
  ypix -= 1;

  /* All results are written to the output table */

  write_dcol (tid, npix[0], colw, lambda, 0); 
  write_dcol (tid, npix[0], colb, bin, 0);
  write_dcol (tid, npix[0], colf, image, ipos(0,ypix,npix[0])); 

  /* Close everything and good bye */
  osmmfree((char *)x_world), osmmfree((char *)lambda), osmmfree((char *)bin);
  TCTCLO(tid), SCFCLO(imno), SCSEPI();

return 0;
 }

void write_dcol(tid, nb, colnb, col, start)

int      tid, nb, colnb, start;
double   col[];

{

int      i;

for (i=0; i<nb; i++)
    TCEWRD(tid, (i+1), colnb, &col[start+i]);

}


void rebin(coerbr, x_world, lambda, bin, start, step, npix, y)

char        coerbr[];
double      x_world[], bin[], lambda[];
double      start[], step[];
int         npix[], y;

{

  int pix;
  double pixel, readdisp();

  for (pix=0; pix<npix[0]; pix++)
       x_world[pix] = pix*step[0] + start[0];

  /* The following 4 routines are called (module lndisp.c) to 
     apply the dispersion relation to the world coordinates */

  initdisp(coerbr,"OLD",0);
  pixel = readdisp(y);
  eval_disp(x_world, lambda, npix[0]);
  finishdisp();


  /* Compute bin column (differences of lambda) */

  for (pix=1; pix<npix[0]; pix++) 
       bin[pix] = lambda[pix] - lambda[pix-1];
  bin[0] = bin[1];

}




