/*===========================================================================
  Copyright (C) 1991-2010 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        libsp.c
.MODULE       subroutines -- spcallin.exe, spcalib.exe
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Functions for reading the line catalog table.
.KEYWORDS     calibration
.VERSION 1.0  1-Mar-1991   Implementation

 100917		last modif
------------------------------------------------------------*/

#include <stdio.h>
#include <midas_def.h>

#include <gl_defs.h>
#include <spec_defs.h>

extern void free_ivector(), free_fvector(), free_cmatrix();

float *fvector();
int *ivector();
char **cmatrix();

/*
 * read_catalog_table(): reads catalog table named by 'lincat' and
 * stores the values in the 'lc' structure. Values should have the
 * following conditions:
 *	wrang[0] <= wave <= wrang[1]
 *	intens >= imin
 */
int read_catalog_table( lc, lincat, wrang, imin )
LCTAB *lc;
char *lincat;
int wrang[2];
float imin;
{
    int inull;
    float rnull;
    double dnull;
    int  nulval, sortcol, aw, ar, ncols; 
    int  nrows = 0;
    float intens;
    float wave;
    int  col_wave, col_intens, col_ion;
    int i, id, sel;
    char str[MAXLINE];

    strcpy( (char *)lc->name, lincat );

    TCMNUL( &inull, &rnull, &dnull ); /* obtain NULL values */
    if ( TCTOPN( (char *)lc->name, F_I_MODE, &id ) != 0 ) {
	sprintf( str, "Table %s couldn't be opened.",(char *) lc->name );
	SCTPUT( str );
	return FALSE;
    }
    TCIGET( id, &ncols, (int *)&lc->nrows, &sortcol, &aw, &ar );

    TCCSER( id, COL_WAVE, &col_wave );
    if ( col_wave == -1 ) {
	sprintf( str, "*** column %s missing ***", COL_WAVE );
	SCTPUT( str );
	return FALSE;
    }
    TCCSER( id, COL_INTENSITY, &col_intens );
    if ( col_intens == -1 )
    	TCCSER( id, COL_STRENGTH, &col_intens );
    
    TCCSER( id, COL_ION, &col_ion );
    /* 
    if ( col_ion == -1 ) {
	sprintf( str, "*** Warning: column %s missing ***", COL_ION );
	SCTPUT( str );
    }
    */

    /* allocate space for data */
    lc->sel    = ivector( 0, lc->nrows - 1 );
    lc->row    = ivector( 0, lc->nrows - 1 );
    lc->wave   = fvector( 0, lc->nrows - 1 );
    lc->intens = fvector( 0, lc->nrows - 1 );
/* 1 char. less than actually used!
   only gave a problem when freeing the last lc->ion ...
    lc->ion    = cmatrix( 0, lc->nrows - 1, 0, MAXION - 1 );
*/
    lc->ion = cmatrix(0,lc->nrows-1,0,MAXION);

    for ( i = 0; i < lc->nrows; i++ ) {
	lc->intens[nrows] = 0;
	lc->ion[nrows][0] = '\0';
        TCSGET(id, i+1, &sel);
	TCERDR( id, i+1, col_wave, &wave, &nulval );
	if ( wave > wrang[1] || wave < wrang[0] ) /* Wavelength range */
	    continue;
  	if ( col_intens != -1 ) {
	    TCERDR( id, i+1, col_intens, &intens, &nulval );
	    if ( intens != rnull && intens < imin )	/* Strength limit */
	        continue;
	    if ( intens != rnull )
	        lc->intens[nrows] = intens;
	}
  	if ( col_ion != -1 )
	    TCERDC( id, i+1, col_ion, lc->ion[nrows], &nulval );

        lc->sel[nrows] = sel;
        lc->row[nrows] = i+1;
	lc->wave[nrows] = wave;
	nrows++;
    }
    lc->nrows = nrows; /* no. of effective rows */
    TCTCLO( id );

    return TRUE;
}

void free_catalog_table( lc )
LCTAB *lc;
{
    free_ivector(lc->sel,    0, lc->nrows - 1);
    free_ivector(lc->row,    0, lc->nrows - 1);
    free_fvector(lc->wave,   0, lc->nrows - 1);
    free_fvector(lc->intens, 0, lc->nrows - 1);
    free_cmatrix(lc->ion,    0, lc->nrows - 1, 0, MAXION );

    osmmfree((char *)lc);
}

