/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .COPYRIGHT   (C) 1993 European Southern Observatory     */
/* .IDENT       lnerase.c                                  */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    Spectroscopy, Long-Slit                    */
/* .PURPOSE                                                */
/*           Unselect lines from Line Catalog table        */
/*           depending on the erased lines of the          */
/*           calibration process.                          */
/* .VERSION     1.0  Package Creation  17-MAR-1993         */

/*  090728	last modif				   */
/* ------------------------------------------------------- */

#include <stdio.h>
#include <math.h>

#include <midas_def.h>

#include <gl_defs.h>
#include <main_defs.h>
#include <spec_defs.h>
#include <agl_defs.h>

#define EPS		0.001
#define MAXDEL		1000

extern void free_catalog_table();
extern int read_catalog_table(); 

static int  read_line_table();

LCTAB *Lc;



int main()
{
    int  unit;		/* useless */
    int nulval, actval;	/* useless */
    int wrang[2], i, j, id;
    float imin;
    double invalid[MAXDEL];
    int num_invalid;
    int  unselect = FALSE;
    char lintab[MAXLINE], lincat[MAXLINE];
    
    SCSPRO("LNERAS");

    Lc = (LCTAB *)osmmget( sizeof(LCTAB) );

    SCKGETC( "LINTAB", 1, 20, &actval, lintab );
    SCKGETC( "LINCAT", 1, 20, &actval, lincat );
    SCKRDR( "IMIN", 1, 1, &actval, &imin, &unit, &nulval );
    SCKRDI( "WRANG", 1, 2, &actval, wrang, &unit, &nulval );

    read_catalog_table(Lc, lincat, wrang, imin);
    num_invalid = read_line_table(lintab, invalid);

    TCTOPN( lincat, F_IO_MODE, &id );
    /* delete invalid values from the catalog array */
    for ( i = 0; i < Lc->nrows; i++ )
    	for ( j = 0; j < num_invalid; j++ )
	    if ( fabs(Lc->wave[i] - invalid[j]) < EPS )
	    	TCSPUT(id, Lc->row[i], &unselect);
    TCTCLO(id);

    free_catalog_table(Lc);
    SCSEPI();
return 0;
}

int read_line_table( lintab, invalid)
char lintab[];
double invalid[];
{
    char erased;
    int  nulval, sortcol, aw, ar, ncols, nrows;
    int col_erased, col_wave;
    int i, id, n = 0; 

    TCTOPN( lintab, F_IO_MODE, &id );
    TCIGET( id, &ncols, &nrows, &sortcol, &aw, &ar );
    TCCSER( id, ":ERASED", &col_erased );
    TCCSER( id, ":WAVE", &col_wave );
    
    for ( i = 1; i <= nrows; i++ ) {
	erased = VAL_OK;
	TCERDC( id, i, col_erased, &erased, &nulval );
	if ( erased == VAL_ERASED ) {
	    /* col_wave is overwritten !! for this reason, it must be */
	    /* read again everytime (the bug started with 94NOV) */
	    TCCSER( id, ":WAVE", &col_wave );
	    TCERDD( id, i, col_wave, &invalid[n], &nulval );
	    n++;
	}
    }
    TCTCLO( id );

    return(n);
}

