/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       lncalib.c                                  */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    Spectroscopy, Long-Slit                    */
/* .PURPOSE     Wavelength Calibration for 1D, Long and MOS*/

/* .VERSION     1.0  Package Creation  17-MAR-1993         */

/*  090728	last modif  		                   */
/* ------------------------------------------------------- */

#include <stdio.h>

#include <tbldef.h>
#include <midas_def.h>
#include <ok.h>


int       inull;   /* Representation of NULL values */
float     rnull;
double    dnull;

int read_col();



int main()

{


   char      line[60], colwav[20], text[80];
   int       tid, nbcol, nbrow, nsort, allcol, allrow;
   int       linwav, status, actvals, row, elt;
   int       nwave, nallow, found, maxoccur, log;
   double    *linwave, *wave, *allow;
   int       *noccur, *position;

   SCSPRO("lnlinsel");

   status = SCKGETC("IN_A",  1, 60, &actvals, line);
   status = SCKGETC("IN_B",  1, 20, &actvals, colwav);

   if (TCTOPN(line, F_IO_MODE, &tid))
      SCTPUT("**** Error while opening table line.tbl");

   TCIGET(tid, &nbcol, &nbrow, &nsort, &allcol, &allrow);

   TCCSER(tid, ":WAVE", &linwav);
      if (linwav == (-1)) SCTPUT("**** Column :WAVE not found");

   noccur   = (int      *) osmmget((nbrow+1)*sizeof(int     ));
   position = (int      *) osmmget((nbrow+1)*sizeof(int     ));

   allow    = (double *)   osmmget((nbrow+1)*sizeof(double));
   wave     = (double *)   osmmget((nbrow+1)*sizeof(double));
   linwave  = (double *)   osmmget((nbrow+1)*sizeof(double));

   actvals = read_col (tid, nbrow, linwav, linwave, position);

   nwave = 0;

   for  (row=1; row<=actvals; row++) {
         found = 0;
         for (elt=1; elt<=nwave; elt++) {
              if (wave[elt] == linwave[row]) ++noccur[elt], found=1; }
         if (found == 0) {
              noccur[++nwave] = 1; 
              wave[nwave] = linwave[row]; }
       }

   maxoccur = 0, nallow = 0;

   for (elt=1; elt<=nwave; elt++)
        if (noccur[elt] > maxoccur) maxoccur = noccur[elt];

   for (elt=1; elt<=nwave; elt++)
        if (noccur[elt] == maxoccur)  allow[++nallow] = wave[elt];

   sprintf (text,"Number of lines selected : %d\n",nallow);
   SCTPUT(text);

   log = TRUE;
   for (row=1; row<=actvals; row++) {
         found = 0;
         for (elt=1; elt<=nallow; elt++)
              if (allow[elt] == linwave[row]) found=1;
         if (found == 1)   TCSPUT(tid, position[row], &log);
    }        

   TCTCLO(tid);
   SCSEPI();
return 0;
}


int read_col(tid, nb, colnb, col, pos)

  int      tid, colnb, nb, pos[];
  double   col[];

{
int      i, k=0, null, sel, log=FALSE;
double   value;

for (i=1; i<=nb; i++) {
    TCSGET(tid, i, &sel);
    if (sel) { 
         TCERDD(tid, i, colnb, &value, &null);
         if (!(null)) col[++k] = value, pos[k] = i;
       }
    TCSPUT(tid, i, &log);
  }
return(k+1);
}

