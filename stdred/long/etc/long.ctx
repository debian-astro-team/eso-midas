! @(#)long.ctx	19.1 (ES0-DMD) 02/25/03 14:23:53
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       long.ctx
!.AUTHORS     P. Ballester, C. Levin
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Context file for package Long
!.VERSION     1.0     Creation    17-MAR-1993     PB
!	      1.1     add entry CLEAR, 940712 KB
!------------------------------------------------------------- 
!
! define origin of context
!HELP = stdred
!
SET/CONTEXT spec
!
DEFINE/LOCAL   LERR/I/1/1      {ERROR(2)}  !  Saves user-level
SET/MIDAS USER=EXPERT
!
! Basic commands
!
CREATE/COMMAND INITIALIZE/LONG      	@s lninit
CREATE/COMMAND SAVE/LONG      	        @s lnsave
CREATE/COMMAND SHOW/LONG                @s lnshow
CREATE/COMMAND CLEAN/LONG      	        @s lnclean
CREATE/COMMAND MAKE/DISPLAY     	@s lndispl
CREATE/COMMAND TUTORIAL/LONG            @s lndemo
CREATE/COMMAND VERIFY/LONG              @s lnverif
!
CREATE/COMMAND SET/LONG         	@a keyset
CREATE/COMMAND HELP/LONG        	@a keyhelp
CREATE/COMMAND ERROR/LONG       	@a keyerror
CREATE/COMMAND SAVINIT/LONG     	@a keyrdwr
CREATE/COMMAND KEYDEL/LONG              @a keydel
!
! Lines Searching and Identify
!
CREATE/COMMAND SEARCH/LONG  		@s lnsearch
CREATE/COMMAND PLOT/SEARCH              @s lnplsear
CREATE/COMMAND IDENTIFY/LONG            @s lnident
CREATE/COMMAND XIDENT/LONG           	@s lnident 
CREATE/COMMAND PLOT/IDENT               @s lnplident
CREATE/COMMAND LINADD/LONG           	@s lnlinadd
!
! Wavelength Calibration and Verification
!
CREATE/COMMAND ESTIMATE/DISPERSION      @s lnhough
CREATE/COMMAND CALIBRATE/LONG           @s lncalib
CREATE/COMMAND CALIBRATE/TWICE          @s lntwice
CREATE/COMMAND ERASE/LONG	        @s lnerase
CREATE/COMMAND SELECT/LINE              @s lnlinsel
CREATE/COMMAND PLOT/RESIDUAL            @s lnplresid
CREATE/COMMAND PLOT/CALIBRATE           @s lnplcalib
CREATE/COMMAND PLOT/DELTA               @s lnpldelta
CREATE/COMMAND PLOT/DISTORTION          @s lnpldist
!
! Apply Dispersion Relation
!
CREATE/COMMAND APPLY/DISPERSION       	@s lndisp   ! 1D -> table
CREATE/COMMAND PLOT/SPECTRUM            @s lnplspec ! Plots a table
CREATE/COMMAND REBIN/LONG               @s lnrebin  ! 2D -> 2D image (RBR)
CREATE/COMMAND RECTIFY/LONG             @s lnrect   ! 2D -> 2D (bivar. polyn.)
!
! A bunch of obsolete commands
!
!CREATE/COMMAND REBIN/RBR                @s rebirbr  ! 2D -> 2D image (RBR)
!CREATE/COMMAND FIT/RESPONSE 	        @s lnffit   ! Previously FFIT/SPEC
!CREATE/COMMAND RESPONSE/LONG       	@s lnfresp  ! Previously FRESP/SPECTRA
!
! Dark, Bias Correction and Flat-Fielding
!
CREATE/COMMAND COMBINE/LONG  	        @s lncomb
CREATE/COMMAND NORMALIZE/FLAT           @s ffnorm
!
! Sky subtraction and Extraction 
!
CREATE/COMMAND EXTRACT/LONG             @s lnextr
CREATE/COMMAND EXTRACT/AVERAGE         	@s lnaver ! Previously XSIMPLE/LONG
!CREATE/COMMAND FIT/SKY             	@s skyfit
CREATE/COMMAND SKYFIT/LONG         	@s lnskyfit
!
! Instrumental response
!
CREATE/COMMAND EXTINCTION/LONG 	        @s lnfext   ! Previously FEXT/SPEC
CREATE/COMMAND INTEGRATE/LONG       	@s lnintegr ! Prev. INTEGR/SPEC
CREATE/COMMAND RESPONSE/LONG       	@s lnffit   ! Previously FFIT/SPEC
CREATE/COMMAND RESPONSE/FILTER          @s lnresp 
CREATE/COMMAND CALIBRA/FLUX             @s lnflux
CREATE/COMMAND PLOT/RESPONSE            @s lnplresp ! Prev. FPLOT/SPEC
CREATE/COMMAND PLOT/FLUX        	@s lnplflux 
CREATE/COMMAND EDIT/FLUX                @s lnedflux
!
! Batch reduction
!
CREATE/COMMAND PREPARE/LONG             @s lnprepa
CREATE/COMMAND REDUCE/LONG              @s lnreduce  ! Prev. batch/reduce
CREATE/COMMAND REDUCE/INIT              @s lnredinit
CREATE/COMMAND REDUCE/SAVE              @s lnredsave
CREATE/COMMAND BATCH/LONG               @s lnredbatc

!
! Utilities for graphic and plotting
!
CREATE/COMMAND LOAD/LONG            	@s lnloadima
CREATE/COMMAND GCOORD/LONG  		@s lngcoord
CREATE/COMMAND GRAPH/LONG               @s lngraph
CREATE/COMMAND RESET/GRAPH              @s lngreset
!
!
ERROR(2) = LERR
INIT/LONG
WRITE/OUT  "**************** Context Long enabled *****************"
WRITE/OUT  "       "
! 
! 
! 			delete all keywords created via INIT/LONG
entry clear
! 
@a keydel ? long
delete/keyw coefyi,coefyr,coefyd,coefyc,aglims
