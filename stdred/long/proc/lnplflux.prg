! @(#)lnplflux.prg	19.1 (ES0-DMD) 02/25/03 14:24:48
! @(#)lnplflux.prg	19.1  (ESO)  02/25/03  14:24:48
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnplflux.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!               Plotting of flux calibration table.
!               Command:
!                          PLOT/FLUX
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
define/param P1 {FLUXTAB} TAB "Flux Table : "
!
set/long FLUXTAB={P1}
verify/spec {p1} MID_STANDARD FLUXTAB
graph/spec
reset/graph
reset/graph pmode=1 xaxis=auto yaxis=auto font=1
plot/table {fluxtab} :wave :flux_w
reset/graph


