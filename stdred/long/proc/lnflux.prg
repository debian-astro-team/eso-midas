! @(#)lnflux.prg	19.1 (ES0-DMD) 02/25/03 14:24:46
! @(#)lnflux.prg	19.1  (ESO)  02/25/03  14:24:46
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnfluxlnflux.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Performs flux calibration of 1D and 2D spectra
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
DEFINE/PARAM P1   ?        IMA  "Input spectrum: "
DEFINE/PARAM P2   ?        IMA  "Corrected spectrum:"
DEFINE/PARAM P3 {RESPONSE} IMA  "Response image :"

VERIFY/LONG {P1} IMA
VERIFY/LONG {P1} OTIME
VERIFY/LONG {P3} IMA

WRITE/OUT "Exposure time : {{P1},O_TIME(7)} s"

IF {{P1},NAXIS} .NE. 1 THEN
    def/loc fstart/d/1/1 '{P1},START(2)'
    def/loc fstep/d/1/1  '{P1},STEP(2)'
    def/loc fnpix/d/1/1  '{P1},NPIX(2)'
    GROW/IMAGE &r = {P3} 'fstart','fstep','fnpix'
    COMP/IMAGE {P2} = {P1} / &r
ELSE
    COMP/IMAGE {P2} = {P1} / {P3}
ENDIF

COMPUTE/IMAGE {P2} = {P2}/{{P1},O_TIME(7)}

COPY/DD {P1}  *,3  {P2}






