! @(#)lntwice.prg	19.1 (ES0-DMD) 02/25/03 14:24:51
! @(#)lntwice.prg	19.1  (ESO)  02/25/03  14:24:51
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lntwice.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Wavelength Calibration in two steps
!.VERSION     1.0  Package Creation  11-JUN-1993
!-------------------------------------------------------
!
CALIBRATE/LONG

define/local method/C/1/20 {wlcmtd}

IF WLCMTD(1:1) .EQ. "I" .OR. WLCMTD(1:1) .EQ. "L" THEN
   WRITE/OUT "Warning: second calibration is performed"
   WRITE/OUT "in mode WLCMTD=GUESS"
   SAVE/LONG tmp
   SET/LONG  WLCMTD = GUESS   GUESS = tmp
ENDIF

SELECT/LINE 

CALIBRATE/LONG

SELECT/TABLE {LINTAB} ALL
SET/LONG WLCMTD = {METHOD}

RETURN

