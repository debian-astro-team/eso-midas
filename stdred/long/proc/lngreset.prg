! @(#)lngreset.prg	19.1 (ES0-DMD) 02/25/03 14:24:46
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       lngrinit.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Optional graph initialisation
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
define/local line/C/1/132  " "
!
line(1:132)  = MID$LINE(13:)
!
IF GRINIT(1:1) .EQ. "Y"  THEN
  SET/GRAPH {line}
ENDIF
RETURN
