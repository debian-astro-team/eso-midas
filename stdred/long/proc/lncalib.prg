! @(#)lncalib.prg	19.1 (ES0-DMD) 02/25/03 14:24:44
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lncalib.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
CROSSREF tol deg mtd guess
!
DEFINE/PARAM  P1  {TOL}                  NUMB   "Tolerance, Degree: "
DEFINE/PARAM  P2  {DCX(1)},{DCX(2)}      NUMB   "Line table:"
DEFINE/PARAM  P3  {WLCMTD}               CHAR   "Mode (IDENT/GUESS/LINEAR) :"
DEFINE/PARAM  P4  {GUESS}                CHAR   "Guess session"
DEFINE/PARAM  P5  {WCENTER},{AVDISP},{BETA}  NUMB   "Center wav., dispersion:"
!
DEFINE/MAXPAR 5
!
VERIFY/LONG {LINTAB} TAB
VERIFY/SPEC {LINCAT} MID_ARC LINCAT 
!
SET/LONG TOL={P1}  WLCMTD={P3} GUESS={P4}
WRITE/KEYW DCX      {P2},{P2}
CREATE/COLUMN {LINTAB} :IDENT R*8 {SESSOUTV}
!
IF WLCMTD(1:1) .EQ. "L" THEN
   COPY/KK DISPCOE/D/1/3  INPUTD/D/5/3
ENDIF
!
IF WLCMTD(1:1) .EQ. "G" THEN
   IF COROPT(1:1) .EQ. "Y" THEN
      define/local  ybound/D/1/2  0.,0.
      ybound(1) = START(2) + (ystart-1)*STEP(2) - YSTEP*STEP(2)*0.45
      ybound(2) = ybound(1) + YSTEP*STEP(2)*0.90
      WRITE/OUT "Performs cross-correlation at line :Y = 'YSTART'"
      CORREL/LINE    {GUESS}  {LINTAB}  {STEP(1)}  P6='YBOUND(1)','YBOUND(2)'
      IF CORVISU(1:1) .EQ. "Y"  THEN
         GRAPH/SPEC
         RESET/GRAPH
         RESET/GRAPH PMODE=1
         PLOT &x
         RESET/GRAPH
      ENDIF
      SHIFT = OUTPUTR(1)
   ENDIF
ENDIF
!
WRITE/KEYW  IN_A   {LINTAB}
WRITE/KEYW  IN_B   {LINCAT}
WRITE/KEYW  INPUTC {WLCMTD}
WRITE/KEYW  OUT_A  {COERBR}
WRITE/KEYW  OUT_B  {GUESS}COE.tbl
!
INPUTI(1) = DCX(1)
INPUTI(2) = DCX(2)
INPUTI(3) = WLCNITER(1)
INPUTI(4) = WLCNITER(2)
INPUTI(5) = YSTART
INPUTI(6) = WRANG(1)
INPUTI(7) = WRANG(2)
!
INPUTR(1) = ALPHA
INPUTR(2) = MAXDEV
INPUTR(3) = TOL
INPUTR(4) = SHIFT
INPUTR(5) = IMIN
!
INPUTI(10) = 75
!
INPUTD(1)  = START(2)
INPUTD(2)  = STEP(2)
!
COPY/KEYW  START/D/1/2   INPUTD/D/1/2  
COPY/KEYW  STEP/D/1/2    INPUTD/D/3/2  
!
RUN STD_EXE:lncalib
!
IF PROGSTAT(1) .NE. 0  RETURN/EXIT
!
IF TWODOPT(1:1) .EQ. "Y" THEN
select/table       {LINTAB}    :REJECT.GT.0
regression/poly    {LINTAB}    :WAVE :X,:Y 'dcx(1)','dcx(2)'  KEYLONG
save/regr          {LINTAB}    COEFXY  KEYLONG
create/column      {LINTAB}    :WAV2D  R*8  F10.5
compute/regres     {LINTAB}    :WAV2D = COEFXY
ENDIF
!
log(4) = 2
regres/poly      {LINTAB}    :WAVE  :X   1	  ! to get rebstp
REBSTP  = outputd(2)*step(1)
regres/poly      {LINTAB}    :WAVE  :X   'dcx(1)' ! to get rebstrt, rebend
log(4) = 0

define/local deg/I/1/2  0,0
define/local xval/D/1/4 0.,0.,0.,0.,0.

rebstrt = outputd(1)
rebend  = outputd(1)
xval(1) = start(1)
xval(2) = start(1)+(npix(1)-1.)*step(1)
xval(3) = xval(1)
xval(4) = xval(2)

DO DEG = 1  'DCX(1)'
  deg(2)  = deg + 1
  REBSTRT = rebstrt + outputd('deg(2)')*xval(3)
  xval(3) = xval(3)*xval(1)
  REBEND  = REBEND  + outputd('deg(2)')*xval(4)
  xval(4) = xval(4)*xval(2)
ENDDO

IF REBEND .LT. REBSTRT THEN
    inputd(1) = rebstrt
    rebstrt   = rebend
    rebend    = inputd(1)
ENDIF
IF rebstp .lt. 0. rebstp = rebstp*(-1.)

SET/FORMAT F12.3
WRITE/OUT  " "
WRITE/OUT  " Initial Wav. : {REBSTRT}"
WRITE/OUT  " Final Wav.   : {REBEND}"
WRITE/OUT  " Step         : {REBSTP}"
!
! Write rebin descriptors in 'COERBR' table
!
write/desc 'coerbr' rebstrt/d/1/1 'rebstrt'
write/desc 'coerbr' rebstp/d/1/1  'rebstp'
write/desc 'coerbr' rebend/d/1/1  'rebend'
!
