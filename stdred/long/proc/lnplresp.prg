! @(#)lnplresp.prg	19.1 (ES0-DMD) 02/25/03 14:24:49
! @(#)lnplresp.prg	19.1  (ESO)  02/25/03  14:24:49
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnplresp.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!               Plotting of flux calibration table.
!               Command:
!                          PLOT/RESPONSE
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
define/param P1 {RESPONSE} IMA  "Fitted response image :"
!
GRAPH/SPEC
reset/graph
reset/graph pmode=1  xaxis=auto yaxis=auto font=1
!
PLOT/ROW  {P1}
reset/graph

