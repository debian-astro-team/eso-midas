! @(#)lndisp.prg	19.1 (ES0-DMD) 02/25/03 14:24:45
! @(#)lndisp.prg	19.1  (ESO)  02/25/03  14:24:45
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lndisp.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
DEFINE/PARAM  P1   ?       IMA    "Input  Image:"
DEFINE/PARAM  P2   ?       TAB    "Output Table:"
DEFINE/PARAM  P3 @{YSTART} CHAR   "Line number :"
DEFINE/PARAM  P4 {COERBR} TAB     "Coefficients table :"

VERIFY/LONG {P1} IMA
VERIFY/LONG {P4} TAB

WRITE/KEYW     IN_A  {P1}
WRITE/KEYW     IN_B  {P4}
WRITE/KEYW     OUT_A {P2}

IF M$INDEX(OUT_A,".tbl") .EQ. 0  WRITE/KEYW OUT_A {OUT_A}.tbl

! Position Y can be given in pixels or world coordinates
IF P3(1:1) .EQ. "@" THEN
   WRITE/KEYW INPUTD/D/1/2  -1.,{P3(2:)}
ELSE
   WRITE/KEYW INPUTD/D/1/2  1.,{P3}
ENDIF

RUN  STD_EXE:lnapdisp

COPY/DD {IN_A} *,3 {OUT_A}


