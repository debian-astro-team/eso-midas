! @(#)lnplident.prg	19.1 (ES0-DMD) 02/25/03 14:24:48
! @(#)lnplident.prg	19.1  (ESO)  02/25/03  14:24:48
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnplident.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
DEFINE/PARAM  P1  {WLC}     CHAR  "Wavelength calibration frame : "
DEFINE/PARAM  P2 {LINTAB}  CHAR  "Line table : "
DEFINE/PARAM  P3   :X       CHAR  "X pos:"
DEFINE/PARAM  P4   :IDENT   CHAR  "Ident:"
DEFINE/PARAM  P5   :WAVE    CHAR  "WAVE"

DEFINE/LOCAL CMND/C/1/20  {MID$CMND(11:19)}

GRAPH/SPEC
RESET/GRAPH
RESET/GRAPH PMODE=1 FONT=1
PLOT/ROW  {P1} @{YSTART}

SORT/TABLE {P2} :X
reset/graph  color=4

IF CMND(1:1) .EQ.  "I"  THEN
 SELECT/TABLE   {P2}   :IDENT.NE.NULL
 overplot/ident {P2} {P3} {P4} TOP
ENDIF

IF CMND(1:1) .EQ.  "C"  THEN
 @s lnident,seline {P2}
 SELECT/TABLE {P2} SELECT .AND. :WAVE .NE. NULL
 overplot/ident {P2} {P3} {P5} TOP
ENDIF

RESET/GRAPH
SELECT/TABLE {P2} ALL
SORt/TABLE {P2} :Y :X
RETURN
