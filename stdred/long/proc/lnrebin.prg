! @(#)lnrebin.prg	19.1 (ES0-DMD) 02/25/03 14:24:50
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnrebin.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!               Row by row rebinning of a 2D spectrum.
!               Command:
!                          REBIN/SPECTRA
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
define/param P1 ?                  I "Enter image to be rebinned      : "
define/param P2 ?                  I "Enter result image              : "
define/param P3 {REBSTRT},{REBEND},{REBSTP} N "Start, Step in wavelength : "
define/param P4 {REBMTD}           C "Enter method (SPLINE/QUADRATIC) : "
define/param P5 {COERBR}           C "Enter parameters table          : "
!
DEFINE/MAXPAR P5
!
VERIFY/LONG {P1} IMA
VERIFY/LONG {P5} TAB
!
WRITE/KEYW INPUTR/R/1/3 {P3}
SET/LONG REBMTD={P4} COERBR={P5} REBSTRT={INPUTR(1)}
SET/LONG REBEND={INPUTR(2)} REBSTP={INPUTR(3)}
VERIFY/SPEC {COERBR} ? COERBR
!
def/loc indx/i/1/1 0
def/loc partab/c/1/80 " " all
!
DEFINE/LOCAL  FITD/I/1/1           0
DEFINE/LOCAL  CFTAB/C/1/80         {COERBR}
COPY/DK       {COERBR}  LNDEG      FITD

def/loc extab/i/1/1 0
extab = m$exist(cftab)
if extab .eq. 0 then
   write/out *** Table 'cftab' does not exist. Perform the calibration\before\***
   return
endif

if rebstrt .eq. 0. error/long REBIN/LONG REBSTRT
if rebend  .eq. 0. error/long REBIN/LONG REBEND 
if rebstp  .eq. 0. error/long REBIN/LONG REBSTP

write/keyw inputr/r/1/3 {REBSTRT},{REBEND},{REBSTP}
copy/kd   inputr/R/1/3 'cftab' REBPAR/R/1/3

INPUTI(1) = -1
IF P4(1:1) .EQ. "L"  WRITE/KEYW INPUTI/I/1/1   0    ! Method LINEAR
IF P4(1:1) .EQ. "Q"  WRITE/KEYW INPUTI/I/1/1   1    ! Method QUADRATIC
IF P4(1:1) .EQ. "S"  WRITE/KEYW INPUTI/I/1/1   2    ! Method SPLINE

IF INPUTI(1) .LT. 0  THEN
   WRITE/OUT "Rebin: Unknown method {REBMTD}"
   RETURN/EXIT
ENDIF

write/keyw in_a/c/1/20 'P1'
write/keyw out_a/c/1/20 'P2'
write/keyw in_b/c/1/60 'cftab'

run STD_EXE:sprebin 

COPY/DD {P1} *,3 {P2}



