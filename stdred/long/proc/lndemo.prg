!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993-2008 European Southern Observatory
!.IDENT       lndemo.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Tutorial for context Long . PB 12.02.1993
!.VERSION     1.0  Package Creation  17-MAR-1993  
! 080321	last modif
!-------------------------------------------------------
!                                           local variables
echo/on
init/long
make/display
graph/spec
set/grapH
echo/off
!
define/local imtyp/c/1/8 {mid$types(1:8)}
define/local tbtyp/c/1/8 {mid$types(9:16)}

write/out Copy test images 
-DELETE lndemo_*.*

if aux_mode(13) .eq. 1 then                     !use FITS files
   aux_mode(13) = 0

   ! these old EMMI files have an ominous descr. ESO-LOG in their
   ! FITS heaqder which screws up Midas internal FITS file writing...

  intape/fits 1 toto MID_TEST:emhear.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits  toto0001.bdf lndemo_wlch.{imtyp}
   intape/fits 1 toto MID_TEST:emth.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf lndemo_wlcth.{imtyp}
   intape/fits 1 toto MID_TEST:emstd.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf    lndemo_wstd.{imtyp}
   intape/fits 1 toto MID_TEST:emmi0042.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf  lndemo_bias1.{imtyp}
   intape/fits 1 toto MID_TEST:emmi0043.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf  lndemo_bias2.{imtyp}
   intape/fits 1 toto MID_TEST:emmi0044.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf  lndemo_bias3.{imtyp}
   intape/fits 1 toto MID_TEST:emmi0045.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf  lndemo_bias4.{imtyp}
   intape/fits 1 toto MID_TEST:emmi0046.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf  lndemo_flat1.{imtyp}
   intape/fits 1 toto MID_TEST:emmi0047.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf  lndemo_flat2.{imtyp}
   intape/fits 1 toto MID_TEST:emmi0048.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf  lndemo_flat3.{imtyp}
   intape/fits 1 toto MID_TEST:emmi0049.fits NOC >Null
   @ dsc_cleaner toto0001.bdf eso_log
   outdisk/fits toto0001.bdf  lndemo_flat4.{imtyp}
  aux_mode(13) = 1
 
   -copy MID_TEST:thorium.tfits lndemo_thorium.{tbtyp}
   -copy MID_TEST:hear.tfits    lndemo_hear.{tbtyp}
   -copy MID_TEST:l745.tfits    lndemo_l745.{tbtyp}
   -copy MID_TEST:atmoexan.tfits   lndemo_atmo.{tbtyp}
else
   intape/fits 1 toto MID_TEST:emhear.fits NOC
   -rename toto0001.bdf  lndemo_wlch.bdf 
   intape/fits 1 toto MID_TEST:emth.fits NOC
   -rename toto0001.bdf lndemo_wlcth.bdf >Null
   intape/fits 1 toto MID_TEST:emstd.fits NOC
   -rename toto0001.bdf    lndemo_wstd.bdf >Null
   intape/fits 1 toto MID_TEST:emmi0042.fits NOC
   -rename toto0001.bdf  lndemo_bias1.bdf >Null
   intape/fits 1 toto MID_TEST:emmi0043.fits NOC
   -rename toto0001.bdf  lndemo_bias2.bdf >Null
   intape/fits 1 toto MID_TEST:emmi0044.fits NOC
   -rename toto0001.bdf  lndemo_bias3.bdf >Null
   intape/fits 1 toto MID_TEST:emmi0045.fits NOC
   -rename toto0001.bdf  lndemo_bias4.bdf >Null
   intape/fits 1 toto MID_TEST:emmi0046.fits NOC
   -rename toto0001.bdf  lndemo_flat1.bdf >Null
   intape/fits 1 toto MID_TEST:emmi0047.fits NOC
   -rename toto0001.bdf  lndemo_flat2.bdf >Null
   intape/fits 1 toto MID_TEST:emmi0048.fits NOC
   -rename toto0001.bdf  lndemo_flat3.bdf >Null
   intape/fits 1 toto MID_TEST:emmi0049.fits NOC
   -rename toto0001.bdf  lndemo_flat4.bdf >Null

   indisk/fits MID_TEST:thorium.tfits lndemo_thorium.tbl >Null
   indisk/fits MID_TEST:hear.tfits    lndemo_hear.tbl >Null
   indisk/fits MID_TEST:l745.tfits    lndemo_l745.tbl >Null
   indisk/fits MID_TEST:atmoexan.tfits lndemo_atmo.tbl >Null
endif
!
echo/on
if p2(1:1) .eq. "1" goto onedim
!
echo/off
write/out "This tutorial shows how to calibrate long slit spectra"
write/out "The package assumes wavelengths increasing from"
write/out "left to rigth."
write/out "It is assumed that the images have been already"
write/out "rotated, corrected for pixel to pixel variation"
write/out "and the dark current has been subtracted."
write/out "Input data are:"
write/out "wlc.{imtyp}       - wavelength calibration image"
write/out "obj.{imtyp}       - object image"
write/out "lincat.{tbtyp}    - line catalogue"
!
init/long 
wait/secs  5
!
echo/off
write/out  "Combining flat and dark images"
ECHO/ON
!
load/image   lndemo_flat1
write/out CREATE/ICAT bias lndemo_bias*.{imtyp}
CREATE/ICAT bias lndemo_bias*.{imtyp}
COMBINE/LONG   bias  lnbias  MEDIAN
STAT/IMAGE       lnbias
!
CREATE/ICAT flat lndemo_flat*.{imtyp}
SET/LONG TRIM=20,60,520,457
PREPARE/LONG   flat.cat lndemo_ft 
CREATE/ICAT flat lndemo_ft*.{imtyp}
COMBINE/LONG   flat  lnff    AVERAGE
NORMALIZE/FLAT lnff  lnflat  190.
!
ECHO/ON
CREATE/ICAT  lndemocat  lndemo_w*.{imtyp}
READ/ICAT    lndemocat
LOAD/image         lndemo_wlch
ECHO/OFF 
WRITE/OUT "Extracting useful part of spectra with command PREPARE/LONG"
ECHO/ON
SET/LONG  TRIM = 0,60,0,457
PREPARE/LONG  lndemocat.cat lndemo
!
WLC:

SET/GRAPH PMODE=1 XAXIS=AUTO YAXIS=AUTO
SET/LONG WLC=lndemo1 LINCAT=lndemo_hear YWIDTH=10 THRES=30.
SET/LONG YSTEP=10 WIDTH=8 TWODOPT=YES DCX=2,2
!
SESSDISP = "NO "
SHOW/LONG wlc
!
ECHO/OFF
WRITE/OUT Search lines:
WRITE/DESCR {WLC}  STEP/D/2/1  -2.
ECHO/ON
write/out 1111111 =======================
SEARCH/LONG                ! search calibration lines
PLOT/SEARCH
ECHO/OFF
!
IF P1(1:1) .EQ. "A" THEN
   WRITE/OUT  "Use an approximate definition of the dispersion relation"
   WRITE/OUT  "to start the calibration process. Don't forget to adapt"
   WRITE/OUT  "the units of the values stored in descriptors and catalogs"
   ECHO/ON
!
   Write/out  {{wlc},_EIG1_WLEN}
   Write/out  {{wlc},_ED_PIXSIZE}
!
   define/local val/D/1/2 0.,0.
!
   val(1) =  {{wlc},_EIG1_WLEN}} * 1.0E+10
   val(2) =  {{wlc},_ED_PIXSIZE}
! The grating used has a dispersion of 56 A/mm.
! 1.0E+10 and 1000. are unit coefficients
   val(2) = val(2)*56*{{wlc},step(1)}*1000.
!
   set/long  wcent={val(1)}  avdisp={val(2)} 
!
   SET/LONG  WLCMTD=LINEAR TOL=0.5
   ESTIMATE/DISPERSION
   CALIBRATE/LONG
ELSE
  WRITE/OUT "Identify some of the brightest lines:"
  WRITE/OUT
  WRITE/OUT " X   = 379.30     922.50 " 
  WRITE/OUT " WAV = 5015.680   5606.733"
  WAIT/SECS 2
  ECHO/ON
  IDENTIFY/LONG              ! interactive line identification
  SET/LONG  WLCMTD=IDENT  TOL=0.3
  CALIBRATE/TWICE            ! wavelength calibration
  PLOT/IDENT                 ! display initial identifications
  ECHO/OFF
ENDIF
ECHO/ON
!
WRITE/OUT Compute the dispersion coefficients by fitting a 2-D polynomial
WRITE/OUT to the whole array
ECHO/ON
PLOT/CALIBRATE             ! display all identifications
PLOT/RESIDUAL
PLOT/DISTORTION 5015.680
!
SAVE/LONG ses1
ECHO/OFF
WRITE/OUT "Now calibrating another arc spectrum in GUESS mode"
ECHO/ON
SET/LONG  WLCMTD=GUESS GUESS=ses1 WLC=lndemo2 LINCAT=lndemo_thorium
SET/LONG  WIDTH=4  THRES=3.  TOL=0.1   ALPHA=0.2
LOAD/image {wlc}
write/out 22222222  =======================
SEARCH/LONG
CALIBRATE/LONG
!
ECHO/OFF
WRITE/OUT "Now demonstrating the three possible ways to apply the"
WRITE/OUT "dispersion relation : "
WRITE/OUT " - APPLY/DISPERSION involves no rebinning and outputs a table."
WRITE/OUT "     Input must be a 1D spectrum or a row of a long-slit spectrum"
WRITE/OUT " - REBIN/LONG       rebins row by row, taking coefficients from coerbr.tbl"
WRITE/OUT " - RECTIFY/LONG     applies the 2D polynomial dispersion relation"
WRITE/OUT "Note: Rebin can be applied before or after extraction"
ECHO/ON
!
!INIT/LONG ses1
!
APPLY/DISPERSION {wlc}  wlct  @100
PLOT/SPECTRUM    wlct
!
REBIN/LONG {wlc} wlcrb
LOAD/image       wlcrb
PLOT/row       wlcrb  @100
!
RECTIFY/LONG {wlc}  wlc2
LOAD/image         wlc2
PLOT/row         wlc2  @100
!
ECHO/OFF
WRITE/OUT "Session is now saved, initialized, and loaded from session tables"
ECHO/ON
SAVE/LONG mysess
INIT/LONG 
SESSDISP = "NO "
SHOW/LONG  
INIT/LONG mysess
SESSDISP = "NO "
SHOW/LONG
!
ECHO/OFF
WRITE/OUT "Now extracting a spectrum with two possible methods:"
WRITE/OUT "  - Simple rows average with EXTRACT/AVERAGE"
WRITE/OUT "  - Optimal extraction  with EXTRACT/LONG"
ECHO/ON
LOAD/IMAGE         lndemo3
SET/LONG         REBSTR=4600. REBEND=5800. REBSTP=2.00
REBIN/LONG       lndemo3   ext8
SET/LONG         LOWSKY = 189,198  UPPSKY = 204,215
SET/LONG         GAIN=2. RON=5. THRES=3. RADIUS=2
SKYFIT/LONG      ext8    stdsky
LOAD/image             stdsky
COMPUTE/IMAGE    ext7 = ext8 - stdsky
SET/LONG         OBJECT = 199,203
EXTRACT/AVERAGE  ext7   stda
PLOT/row             stda
EXTRACT/LONG     ext8   stde  stdsky
PLOT/row             stde
!
ECHO/OFF
WRITE/OUT  "Now computing instrumental response"
ECHO/ON
!
SET/LONG FLUXTAB=lndemo_l745  EXTAB=lndemo_atmo
PLOT/FLUX
EXTINCTION/LONG  stde stdext
INTEGRATE/LONG   stdext
RESPONSE/LONG fit=SPLINE
PLOT/RESPONSE
CALIBRATE/FLUX   stdext stdcor
CUTS             stdcor   100.,500.
PLOT/row             stdcor
!
ECHO/OFF
!WRITE/OUT  "Now perform a batch reduction on a catalog of images"
!ECHO/ON
!
!SET/LONG FLATOPT=YES BIASOPT=YES RESPOPT=YES EXTOPT=YES REBOPT=YES 
!SET/LONG OUTPUTF=proc BIAS=lnbias FLAT=lnflat 
!SHO/LONG B
!
!REDUCE/LONG  lndemocat.cat
!PLOT proc0002
!ECHO/OFF
!
RETURN
!
! Reduction of one dimensional spectra 
!
ONEDIM:
!
set/long lows=249,258 upps=264,275 obj=259,263
extract/average lndemo_wstd    lndemo_std
set/long lows=20,50 upps=470,500
extract/average lndemo_wlch    lndemo_whear
extract/average lndemo_wlcth   lndemo_wthor
!
set/long wlc=lndemo_whear lincat=lndemo_hear dcx=2  
set/long thres=1000. width=8 twodopt=yes alpha=1.0
!
ECHO/OFF
WRITE/OUT Search lines:
ECHO/ON
SEARCH/LONG                ! search calibration lines
PLOT/SEARCH
ECHO/OFF
!
IF P1(1:1) .EQ. "A" THEN
   WRITE/OUT  "Use an approximate definition of the dispersion relation"
   WRITE/OUT  "to start the calibration process. Don't forget to adapt"
   WRITE/OUT  "the units of the values stored in descriptors and catalogs"
   ECHO/ON
!
   Write/out  {{wlc},_EIG1_WLEN}
   Write/out  {{wlc},_ED_PIXSIZE}
!
   define/local val/D/1/2 0.,0.
!
   val(1) =  {{wlc},_EIG1_WLEN}} * 1.0E+10
   val(2) =  {{wlc},_ED_PIXSIZE}
! The grating used has a dispersion of 56 A/mm.
! 1.0E+10 and 1000. are unit coefficients
   val(2) = val(2)*56*{{wlc},step(1)}*1000.
!
   estimate/disp  wcent={val(1)}  wdisp={val(2)}
!
   SET/LONG  WLCMTD=LINEAR TOL=1.0
   CALIBRATE/LONG
ELSE
  WRITE/OUT "Identify some of the brightest lines:"
  WRITE/OUT
  WRITE/OUT " X   = 379.30     922.50 " 
  WRITE/OUT " WAV = 5015.680   5606.733"
  WAIT/SECS 2
  ECHO/ON
  IDENTIFY/LONG              ! interactive line identification
  SET/LONG  WLCMTD=IDENT  TOL=1.0
  CALIBRATE/LONG             ! wavelength calibration
  PLOT/IDENT                 ! display initial identifications
  ECHO/OFF
ENDIF
ECHO/ON
!
WRITE/OUT Compute the dispersion coefficients by fitting a 2-D polynomial
WRITE/OUT to the whole array
ECHO/ON
PLOT/CALIBRATE             ! display all identifications
PLOT/DELTA
PLOT/RESIDUAL
!
SAVE/LONG ses1
ECHO/OFF
WRITE/OUT "Now calibrating another arc spectrum in GUESS mode"
ECHO/ON
SET/LONG  WLCMTD=GUESS GUESS=ses1 WLC=lndemo_wthor LINCAT=lndemo_thorium
SET/LONG  WIDTH=4  THRES=3.  TOL=0.5  alpha=0.5
PLOT/row {wlc}
write/out ======================================================
echo/on 1,2
SEARCH/LONG
CALIBRATE/LONG
!
ECHO/OFF
WRITE/OUT "Now demonstrating the three possible ways to apply the"
WRITE/OUT "dispersion relation : "
WRITE/OUT " - APPLY/DISPERSION involves no rebinning and outputs a table."
WRITE/OUT "     Input must be a 1D spectrum or a row of a long-slit spectrum"
WRITE/OUT " - REBIN/LONG       rebins row by row, taking coefficients from coerbr.tbl"
WRITE/OUT " - RECTIFY/LONG     applies the 2D polynomial dispersion relation"
WRITE/OUT "Note: Rebin can be applied before or after extraction"
ECHO/ON
!
!INIT/LONG ses1
!
APPLY/DISPERSION {wlc}  wlct
PLOT/SPECTRUM    wlct
!
REBIN/LONG {wlc} wlcrb
PLOT/row       wlcrb
!
REBIN/LONG lndemo_std  rstd
PLOT/row       rstd 
!
ECHO/OFF
WRITE/OUT  "Now computing instrumental response"
ECHO/ON
!
SET/LONG FLUXTAB=lndemo_l745  EXTAB=lndemo_atmo
PLOT/FLUX
EXTINCTION/LONG  rstd   stdext
RESPONSE/FILTER  stdext
INTEGRATE/LONG   stdext
RESPONSE/LONG fit=SPLINE
PLOT/RESPONSE
CALIBRATE/FLUX   stdext stdcor
CUTS             stdcor   100.,500.
PLOT/row             stdcor
!
ECHO/OFF
RETURN





