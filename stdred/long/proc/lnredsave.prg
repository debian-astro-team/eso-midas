! @(#)lnredsave.prg	19.1 (ES0-DMD) 02/25/03 14:24:50
! @(#)lnredsave.prg	19.1  (ESO)  02/25/03  14:24:50
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnredsave.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!                init parameters for Spectra reduction
!                Command: SAVE/REDUCE
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
def/param p1 ? t "Enter batch reduction file : "

def/loc indx/i/1/1 0
def/loc partab/C/1/60  " "  ALL

compute/keyw indx = m$index(p1, ".brf")

if indx .eq. 0 then
    write/keyw partab {p1}.brf
else
    write/keyw partab {p1}
endif

write/out Saving batch reduction parameters in file 'partab' ...

crea/table 'partab' 1 1 null	
savinit/long 'partab' write
