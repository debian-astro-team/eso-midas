! @(#)lnffit.prg	19.1 (ES0-DMD) 02/25/03 14:24:46
! @(#)lnffit.prg	19.1  (ESO)  02/25/03  14:24:46
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnffit.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE
!               Fitting for generating the response curve.
!               Command:
!                          RESPONSE/LONG
!.VERSION     1.0  Package Creation  17-MAR-1993
!-------------------------------------------------------
!
CROSSREF PLOT FIT  DEG  SMO  TABLE  IMAGE  VISU
!
define/param P1 {PLOTYP}   C "Enter type of plot : "
define/param P2 {FITYP}    C "Enter type of fit  : "
define/param P3 {FITD}     N "Degree of fit      : "
define/param P4 {SMOOTH}   N "Smoothing factor   : "
define/param P5 {RESPTAB}  C "Response table     : "
define/param P6 {RESPONSE} C "Response image     : "
define/param P7 {RESPLOT}  C "Plot intermediate results (YES/NO):"
!
DEFINE/MAXPAR 7
!
VERIFY/LONG {P5}        TAB
SET/LONG    PLOTYP={P1} FITYP={P2}   FITD={P3}
SET/LONG    SMOOTH={P4} RESPTAB={P5} RESPLOT={P7} RESPONSE={P6}
!
define/local index/I/1/1 0
index = M$INDEX(P5,".tbl") 
if INDEX .LE. 0 set/long resptab={P5}.tbl
!
define/local nwaves/i/1/1 0
define/local wstart/r/1/1 0.0
define/local wstep/r/1/1  0.0
!
write/keyw nwaves/i/1/1 '{RESPTAB},nwaves'
write/keyw wstart/r/1/1 '{RESPTAB},wstart'
write/keyw wstep/r/1/1 '{RESPTAB},wstep'
!
DEFINE/LOCAL PARAMS/C/1/30 " " ALL
DEFINE/LOCAL COLUMN/C/1/10  " " ALL

IF FITYP(1:1) .EQ. "S"  WRITE/KEYW PARAMS "SPLINE {SMOOTH},{FITD}"
IF FITYP(1:1) .EQ. "P"  WRITE/KEYW PARAMS "POLY   {FITD}"

IF PLOTYP(1:1) .EQ. "R" WRITE/KEYW COLUMN  :RATIO
IF PLOTYP(1:1) .EQ. "M" WRITE/KEYW COLUMN  :COLOUR

CREATE/IMAGE &f 1,'nwaves' 'wstart','wstep'
CONVERT/TABLE &r = {RESPTAB}  :WAVE {COLUMN}  &f {PARAMS}

IF RESPLOT(1:1) .NE. "Y" GOTO OUT

PLOT:
GRAPH/SPEC
RESET/GRAPH
RESET/GRAPH PMODE=1 FONT=1
PLOT/TABLE {RESPTAB}  :WAVE {COLUMN}
RESET/GRAPH COLOR=4
OVERPLOT/ROW   &r 
RESET/GRAPH

OUT:
IF PLOTYP(1:1)  .EQ. "R"  COPY/II     &r  {RESPONSE}
IF PLOTYP(1:1)  .EQ. "M"  COMPUTE/IMAGE {RESPONSE} = 10.**(-0.4*&r)

RETURN



