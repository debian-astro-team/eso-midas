! @(#)lnhough.prg	19.1 (ESO-DMD) 02/25/03 14:24:46
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnhough.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Tutorial for context Long . PB 12.02.1993
!.VERSION     1.0  Command Creation  09-APR-1993
! 020906	last modif
!-------------------------------------------------------
!
CROSSREF wdisp wcent ystart line cat  mode  range
!
define/param p1  {avdisp}  ?    "Dispersion, tolerance, accuracy:"
define/param p2  {wcenter} ?    "Central wav., tolerance, accuracy:"
define/param p3  {ystart}  NUM    "Reference row:"
define/param p4  {lintab}  TAB    "Line table:"
define/param p5  {lincat}  TAB    "Line catalog:"
define/param p6   LINEAR   CHAR   "Mode:"
define/param p7   2.5      NUM    "Range:"
define/param p8   K        CHAR   "Keep/Visualise result (K/N/V)"

define/local  wcent/d/1/3  0.,0.,0.
define/local  wdisp/d/1/3  0.,0.,0.
define/local  werror/i/1/1  1
define/local  dim/i/1/1   0

write/keyw     wcent {P2}
write/keyw     wdisp {P1}

if wdisp(2) .eq. 0.  wdisp(2) = 15  ! Tolerance in percent
if wdisp(3) .eq. 0   wdisp(3) = 1   ! Accuracy in percent
if wcent(2) .eq. 0   wcent(2) = 50  ! Tolerance 100 pixels
if wcent(3) .eq. 0   wcent(3) = 0.2 ! Accuracy in pixels

set/format  f8.6
write/out "Initial values:"
write/out " Central Wav.  : {wcent(1)} +/- {wcent(2)} pix, {wcent(3)} pix"
write/out " Average disp. : {wdisp(1)} +/- {wdisp(2)} %, {wdisp(3)} %"

wdisp(2) = wdisp(2)/100 
wdisp(3) = wdisp(3)/100
!
define/local nblines/I/1/1 0
define/local conf/R/1/1 0.
!
@s lnident,seline
nblines = outputi(1)
!
copy/table {lintab} &l
select/table {lintab} ALL
create/column &l :WAVEC R*8 F10.3


define/local xc/d/1/1 0.
define/local xmean/r/1/1 0.
define/local range/r/1/1 0.
define/local factor/r/1/1 0.
define/local offset/r/1/1 0.

range  = {p7}*step(1)

! Dispersion relation is relative to central positions of the group
! of lines in pixel space.
statistics/table {lintab} :X

xmean = (outputr(3) - start(1))/step(1) + 1.
xc = npix(1)/2. + 0.5

factor = xmean
offset = (xmean - xc)*wdisp(1)

write/out "XCENTER = {xc}  XMEAN = {xmean} OFFSET={offset}"

wcent(1) = wcent(1) + offset

set/format f20.10
compute/table  &l  :XMEAN  =  (:X - ({start(1)}))/{step(1)}+1.-{factor}
compute/table  &l  :XNORM  =  :XMEAN/{factor}

define/local wlim/d/1/2  0.,0.
if lincat(1:1) .ne. "@" copy/table {lincat} &c

define/local hg_start/D/1/3 0.,0.,0.  ? +lower
define/local hg_step/D/1/3  0.,0.,0.  ? +lower
define/local hg_npix/I/1/3  0,0,0     ? +lower

if p6(1:1) .eq. "L"  then
  hg_start(1) = wdisp(1) * (1.-wdisp(2))
  hg_step(1)  = wdisp(1) * wdisp(3)
  hg_npix(1)  = 2.*wdisp(2)/wdisp(3)

  hg_start(2) = wcent(1) - wcent(2)*wdisp(1)
  hg_step(2)  = wcent(3)*wdisp(1)
  hg_npix(2)  = 2.*wcent(2)/wcent(3)

  dim = 2
  write/keyw inputc/c/1/10  :XMEAN
endif

if p6(1:1) .eq. "1" then
      dim = 1
      hg_start(1) = wcent(1) - wcent(2)*wdisp(1)
      hg_step(1)  = wcent(3)*wdisp(1)
      hg_npix(1)  = 2.*wcent(2)/wcent(3)
      inputr(2) = wdisp(1)
      write/keyw inputc/c/1/10 :XMEAN
endif

if p6(1:1) .eq. "N" then
      dim = 2
      inputr(2) = wdisp(1)*factor
      write/keyw inputc/c/1/10  :XNORM

      hg_start(1) = -0.30
      hg_step(1)  = m$abs(hg_start(1))/100.
      hg_npix(1)  = 200

      hg_start(2) = wcent(1) - wcent(2)*wdisp(1)
      hg_step(2)  = wcent(3)*wdisp(1)
      hg_npix(2)  = 2.*wcent(2)/wcent(3)
endif

if p6(1:1) .eq. "3" then
      dim = 3
      inputr(2) = wdisp(1)*factor
      range     = range/factor
      write/keyw inputc/c/1/10  :XNORM

      hg_start(1) = inputr(2) * (1.-wdisp(2))
      hg_step(1)  = inputr(2) * wdisp(3)
      hg_npix(1)  = 2.*wdisp(2)/wdisp(3)

      hg_start(2) = wcent(1) - wcent(2)*wdisp(1)
      hg_step(2)  = wcent(3)*wdisp(1)
      hg_npix(2)  = 2.*wcent(2)/wcent(3)

      hg_start(3) = -0.2
      hg_step(3)  = 0.01
      hg_npix(3)  = 40
endif

if dim .eq. 0  then
   write/out "Wrong Method : {p6}"
   return/exit
endif

write/keyw inputc/c/10/10  :WAVE
write/keyw in_a middumml.tbl

if lincat(1:1) .ne. "@" then
   write/keyw in_b   middummc.tbl
else
   write/keyw in_b   {LINCAT}
endif
if p8(1:1) .ne. "N" then
    write/keyw out_a  middummh.bdf
else
    write/keyw out_a @@@
endif
write/key out_b  {P6}

@s lnhough,bounds

inputi(4) = dim
inputr(4) = range
copy/keyw hg_npix/I/1/3  inputi/I/1/3
copy/keyw hg_start/D/1/3 inputd/D/1/3 
copy/keyw hg_step/D/1/3  INPUTD/D/4/3

run STD_EXE:lnhough.exe

define/local panmax/i/1/1 {outputi(5)}

conf = (outputr(1)/nblines)*100.
if conf .gt. 100. conf = 100.

if p8(1:1) .eq. "V"  then
   extract/image &y = &h[<,<,@{panmax}:>,>,@{panmax}]
   load/image &y scale=5,1
endif

define/local staep/D/1/3  0.,0.,0.

if p6(1:1) .eq. "1" then
   staep(1) = hg_start(1) + (outputi(3)-1)*hg_step(1)
   wcenter = staep(1)  - OFFSET
endif

if p6(1:1) .eq. "L" .or. p6(1:1) .eq. "N" then
   staep(1) = hg_start(1) + (outputi(3)-1)*hg_step(1)
   staep(2) = hg_start(2) + (outputi(4)-1)*hg_step(2)

   if p6(1:1) .eq. "L" then
       compute/table &l :WAVEC = {staep(2)}+{staep(1)}*:XMEAN
       avdisp   = staep(1)
       beta     = 0.
   endif

   if p6(1:1) .eq. "N" then
       compute/table &l :WAVEC = -
         {staep(2)}+{wdisp(1)}*{factor}*:XNORM+{staep(1)}*:XNORM*:XNORM
       beta     = staep(1)
   endif

   wcenter  = staep(2) - offset
endif

if p6(1:1) .eq. "3" then
   staep(1) = hg_start(1) + (outputi(3)-1)*hg_step(1)
   staep(2) = hg_start(2) + (outputi(4)-1)*hg_step(2)
   staep(3) = hg_start(3) + (outputi(5)-1)*hg_step(3)

   compute/table &L :WAVEC = -
     {staep(2)}+{staep(1)}*:XNORM+{staep(3)}*:XNORM*:XNORM

   avdisp   = staep(1)/factor
   wcenter  = staep(2) - offset
   beta     = staep(3)
endif


set/format f12.5
write/out "Confidence Level: {CONF} %"
write/out "Set values WCENTER = {WCENTER} and AVDISP = {AVDISP} and BETA={BETA}"

! Now Updates column :WAVEC in line.tbl

regres/poly  &l  :WAVEC  :X  2
copy/kk outputd/d/1/3  dispcoe/d/1/3
save/regress {lintab}  HREG

outputr(1) = conf



ENTRY BOUNDS

define/local dim/I/1/1 0
define/local hgs/D/1/1 0.

do dim = 1 3
   hgs = hg_step({dim})
   if hgs .lt. 0. then
      hg_start({dim}) = hg_start({dim}) + (hg_npix({dim})-1.)*hgs
      hg_step({dim}) = hgs*(-1.)
   endif
enddo

