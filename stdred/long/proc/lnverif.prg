! @(#)lnverif.prg	19.1 (ESO-DMD) 02/25/03 14:24:51
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnverif.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!.VERSION     1.0  Package Creation  17-MAR-1993  
! 020307	last modif
!-------------------------------------------------------
!
define/param  p1  ?  char  "Input file:"
define/param  p2  ?  char  "Type (IMA/TAB/OTIME/AIRMASS) :"

! Before any further test, checks that the file exists.

define/local exist/i/1/1 0
define/local index/i/1/1 0
define/local inpfil/c/1/80 " " all
define/local errors/i/1/1   0

index = m$index(p1,".")
write/keyw inpfil {p1}

if index .eq. 0 then
   if p2(1:1) .eq. "T"  then
          write/keyw inpfil {p1}.{mid$types(9:16)}
   else
          write/keyw inpfil {p1}.{mid$types(1:8)}
   endif
endif

exist = m$exist(inpfil)

if exist .eq. 0 then
   if p2(1:1) .eq. "O"  write/out "Error ** Could not find file : {P1}"
   if p2(1:1) .eq. "I"  write/out "Error ** Could not find image : {P1}"
   if p2(1:1) .eq. "T"  write/out "Error ** Could not find table : {P1}"
   if p2(1:1) .eq. "A"  write/out "Error ** Could not find file : {P1}"
   return/exit
endif

if p2(1:1) .eq. "O" theN

   if m$existd(p1,"O_TIME") .le. 0 then
      write/out "Error: Descriptor not present : O_TIME in file {P1}"
      errors = 1
   else
      if {{p1},O_TIME(7)} .le. 0.  then
         write/out "Error: Incorrect time {{P1,O_TIME(7)} in file {P1}"
         errors = 1
      endif
   endif

   if errors .eq. 1 then
      write/keyw   inputd/d/1/20  0. all
      inquire/keyw inputd/d/7/1 "Enter Observation time (in seconds) : "
      copy/kd     inputd/d/1/7 {P1}   O_TIME/D/1/7
   endif

endif

if p2(1:1) .eq. "A" then

   if m$existd(p1,"O_AIRM") .le. 0 then
      write/out "Error: Descriptor not present : O_AIRM in file {P1}"
      errors = 1
   else
      if {{p1},O_AIRM} .le. 0.  then
         write/out "Error: Incorrect descriptor O_AIRM : {{P1},O_AIRM} in file {P1}"
         errors = 1
      endif
   endif

   if errors .eq. 1 then
      write/keyw   inputr/r/1/20  0. all
      inquire/keyw inputr/r/1/1 "Enter Airmass : "
      copy/kd     inputr/r/1/1 {p1}   O_AIRM/R/1/1
   endif

endif



