! @(#)lnplot.prg	19.1 (ES0-DMD) 02/25/03 14:24:48
! @(#)lnplot.prg	19.1  (ESO)  02/25/03  14:24:48
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnplot.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE 
!               Plotting for verification of calibration results 
!               Command:
!                          @s lnplot mode type
!         
!               mode = 
!			0 : plot
!			1 : delete
!			2 : get cursor
!               type = 
!			2 : plot delta(wave) / wave
!			4 : plot spectrum labelled with wavelengths
!
!.VERSION     1.0  Package Creation  17-MAR-1993
!----------------------------------------------------------------------
!
if "'p2(1:1)'" .eq. "?" then
   write/keyw spplt/i/1/1 'P1'
else
   write/keyw spplt/i/1/2 'P1','P2'
endif
write/keyw inputc/c/1/20 "long.plt"
write/keyw PLCMETA       "long.plt"

@s lnident,seline
graph/spec

if '{wlc},naxis' .eq. 1 then
    if 'ystart' .ne. 1 then
       write/out "*** Starting line out of bound ***"
       return
    endif
else
    if "'ystart'" .gt. "'{wlc},npix(2)'" then
       write/out "*** Starting line out of bound ***"
       return
    endif
endif

run STD_EXE:lnplot 

PLISTAT(4) = 1
select/table {lintab} all
