! @(#)lnpldist.prg	19.1 (ES0-DMD) 02/25/03 14:24:48
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnpldist.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE
!                Command: PLOT/DISTORTION
!.VERSION     1.0  Package Creation  17-MAR-1993
!-------------------------------------------------------
!
CROSSREF  wave  delta  mode
!
DEFINE/PARAM  P1  ?    NUMB  "Wavelength:"
DEFINE/PARAM  P2  0.5  NUMB  "Interval of wavelength in pixels:"
DEFINE/PARAM  P3  RBR  CHAR  "Mode (RBR or 2D)"
!
GRAPH/SPEC
RESET/GRAPH

define/local wave/R/1/2   {P1},{P2}
define/local bounds/d/1/2 0.,0.

bounds(1) = wave(1) - rebstp*{P2}
bounds(2) = wave(1) + rebstp*{P2} 

if bounds(2) .lt. bounds(1) then
   inputd(1) = bounds(1)
   bounds(1) = bounds(2)
   bounds(2) = inputd(1)
endif

RESET/GRAPH YAXIS=AUTO XAXIS={BOUNDS(1)},{BOUNDS(2)} PMODE=1 FONT=1 STYPE=1 SSIZE=1

define/local COLUMN/C/1/10  :WAVEC

IF P3(1:1) .EQ. "2" THEN 
  IF TWODOPT(1:1) .EQ. "Y"  THEN
       WRITE/KEYW COLUMN  :WAV2D
  ELSE
       WRITE/OUT "Error: Bivariate solution not computed. SET TWODOPT to YES"
       goto fin
  ENDIF
ENDIF


SELECT/TABLE {LINTAB} {COLUMN}.GT.{BOUNDS(1)}.AND.{COLUMN}.LT.{BOUNDS(2)} {SESSOUTV}

if outputi(1) .eq. 0 then
    write/out "Error: wavelength value not found."
    goto fin
endif

! the "else if" doesn't work in this case (can you believe it?) C.Levin
if outputi(1) .eq. 1 then
    log(4) = 0
    write/out "Error: calibration should be done in the whole image."
    goto fin
endif


PLOT/TABLE {LINTAB} {COLUMN}  :Y

STAT/TABLE {LINTAB} :Y {SESSOUTV}
RESET/GRAPH  COLOR=4
OVERPLOT/LINE    1   {WAVE(1)},{OUTPUTR(1)} {WAVE(1)},{OUTPUTR(2)}
!LABEL/GRAPH      "WAVELENGTH : {WAVE(1)}"  40,10,mm

FIN:
RESET/GRAPH
SELECT/TABLE {LINTAB} ALL

RETURN
