! @(#)lnintegr.prg	19.1 (ES0-DMD) 02/25/03 14:24:47
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnintegrlnintegr.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE       . Implements command INTEGRATE/LONG
!               Do the integration of a image, necessary for  
!               generating the response curve.
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
CROSSREF std  flux  resp
!
define/param P1 ?          I "Name of extracted, rebinned standard star : "
define/param P2 {FLUXTAB}  T "Enter table    : "
define/param P3 {RESPTAB}  T "Output response table : "
!
DEFINE/MAXPAR 3
!
VERIFY/LONG {P1} IMA
set/long fluxtab={P2} resptab={P3}
VERIFY/SPEC {P2} MID_STANDARD FLUXTAB
!
IF {{P1},STEP(1)} .LT. 0. THEN
   WRITE/OUT "Sorry... Image {P1} must be rebinned in positive wavelength step."
   RETURN/EXIT
ENDIF
!
crea/table {P3}  6 {{p1},npix(1)}
write/out "Image: {P1}. Exposure time: {{P1},O_TIME(7)}"
COMPUTE/IMAGE    &a = {P1}/{{P1},O_TIME(7)}
!
write/keyw in_a   middumma.bdf
write/keyw in_b   {FLUXTAB}
write/keyw out_a  {RESPTAB}
!
run STD_EXE:spintegr 
!
comp/table  {RESPTAB} :colour = -2.5 * log10(:ratio)
!

