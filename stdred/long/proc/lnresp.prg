! @(#)lnresp.prg	19.1 (ES0-DMD) 02/25/03 14:24:50
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnresplnresp.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Computes Instrumental Response
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
DEFINE/PARAM  P1  ?           IMA  "Observed Standard Star:"
DEFINE/PARAM  P2  {fluxtab}   TAB  "Flux table:"
DEFINE/PARAM  P3  {response}  IMA  "Output response :"


VERIFY/LONG {P1} IMA
VERIFY/LONG {P1} OTIME
set/long fluxtab={P2} response={P3}
VERIFY/SPEC {P2} MID_STANDARD FLUXTAB

WRITE/OUT "Warning: This command will be suppressed after the 95NOV version"
WRITE/OUT "         Use preferably INTEGRATE/LONG, RESPONSE/LONG sequence"

WRITE/OUT     "Exposure time : {{P1},O_TIME(7)}

COMPUTE/IMAGE &a = {P1}/{{P1},O_TIME(7)}

CONVERT/TABLE &o = {FLUXTAB} :WAVE :FLUX_W  &a SP

COMPUTE/IMAGE   &p = &a/&o

FILTER/MEDIAN  &p &q  {FILTMED},0,0
FILTER/SMOOTH  &q &p  {FILTSMO},0,0

COPY/II          middummp  {RESPONSE}
COPY/DD          {P1}  *,3  {RESPONSE}

PLOT/RESPONSE


