! @(#)lnplresid.prg	19.1 (ES0-DMD) 02/25/03 14:24:48
! @(#)lnplresid.prg	19.1  (ESO)  02/25/03  14:24:48
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnplresid.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
DEFINE/PARAM P1 {YSTART}  ?  "Position:"
DEFINE/PARAM P2 {LINTAB} TAB "Line table : "
!
DEFINE/LOCAL CMND/C/1/20  {MID$CMND(11:19)}
DEFINE/LOCAL WPAR/R/1/3   0.,0.,0.  ?  +lower
!
VERIFY/LONG  {P2} TABLE
!
GRAPH/SPEC 
!
LOG(4) = 2
!
IF P1(1:1) .NE. "A" THEN
@s lnident,seline {P2} {P1}
SELECT/TABLE {P2}  SELECT.AND.:REJECT.GT.0.AND.:WAVE.NE.NULL
ELSE
SELECT/TABLE {P2} ALL
ENDIF

IF OUTPUTI(1) .GT. 0 THEN

STAT/TABLE {P2}  :WAVE
COPY/KEYW OUTPUTR/R/1/2 WPAR/R/1/2
RESET/GRAPH
RESET/GRAPH PMODE=1 FONT=1

 IF CMND(1:1) .EQ.  "D"  THEN
        COPY/TABLE   {P2}  &l
        PLOT/TABLE         &l  :WAVE  :DELTA
        WPAR(3) = (WPAR(2)-WPAR(1))/512.
        CREATE/IMAGE         &r   1,512  {WPAR(1)},{WPAR(3)}
        CONVERT/TABLE      &m  = &l  :WAVE  :DELTA  &r  POLY  {DCX(1)}
        RESET/GRAPH COLOR=4
        OVERPLOT/ROW           &m
        OVERPLOT/LINE      4   {WPAR(1)},0. {WPAR(2)},0.
 ELSE
        PLOT/TABLE       {P2}  :WAVE  :RESIDUAL
        RESET/GRAPH        COLOR=4
        OVERPLOT/LINE    4   {WPAR(1)},0. {WPAR(2)},0.
        STAT/TABLE       {P2}  :RESIDUAL
        LABEL/GRAPH      "RMS = {OUTPUTR(4)}"  30,10,mm  
 ENDIF

ELSE

WRITE/OUT "Error: Selected table is empty. No lines plotted"

ENDIF

SELECT/TABLE {P2}  ALL
RESET/GRAPH

!
LOG(4) = 0
!
RETURN


ENTRY PLOT

DEFINE/PARAM P1 {LINTAB}    TAB
DEFINE/PARAM P2 :RESIDUAL   CHAR
DEFINE/PARAM P3 PLOT        CHAR  "Mode (PLOT/OVER)"

{P3}/TABLE {P1}  :WAVE  {P2}
OVERPLOT/LINE    4   {WPAR(1)},0. {WPAR(2)},0.
STAT/TABLE {P1}  {P2}
LABEL/GRAPH      "RMS = {OUTPUTR(4)}"  30,10,mm  

!
LOG(4) = 0
!
RETURN
