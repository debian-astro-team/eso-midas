! @(#)lnlinsel.prg	19.1 (ES0-DMD) 02/25/03 14:24:47
! @(#)lnlinsel.prg	19.1  (ESO)  02/25/03  14:24:47
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnlinsel.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!.VERSION     1.0  Package Creation  10-JUN-1993 
!-------------------------------------------------------
!
DEFINE/PARAM P1 {LINTAB}   TAB "Input table :"
DEFINE/PARAM P2 linsel.tbl TAB "Output table :"

WRITE/KEYW IN_A {P1}
WRITE/KEYW IN_B :WAVE

RUN STD_EXE:lnlinsel.exe

RETURN
