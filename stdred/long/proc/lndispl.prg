! @(#)lndispl.prg	19.1 (ES0-DMD) 02/25/03 14:24:45
! @(#)lndispl.prg	19.1  (ESO)  02/25/03  14:24:45
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lndispl.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!      Creates a display window using the next free id.
!      Command:
!         DISPLAY/SPEC 
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
@ creifnot 2 heat

