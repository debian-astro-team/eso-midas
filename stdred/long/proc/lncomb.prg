! @(#)lncomb.prg	19.1 (ES0-DMD) 02/25/03 14:24:44
! @(#)lncomb.prg	19.1  (ESO)  02/25/03  14:24:44
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lncomb.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Apply command AVERAGE/IMAGE on a catalog
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
define/param  P1  ?        CHAR  "Input Catalogue         : "
define/param  P2  ?        IMA   "Output Image            : "
define/param  P3  {comet}  CHAR  "Method (AVERAGE/MEDIAN) : "

def/loc catalog/C/1/60 " " ALL
def/loc expos/R/1/1    0.

SET/LONG COMET={P3}

IF M$INDEX(P1,".") .EQ. 0 THEN
   WRITE/KEYW CATALOG {P1}.cat
ELSE
   WRITE/KEYW CATALOG {P1}
ENDIF

IF M$EXIST(CATALOG) .EQ. 0 THEN
   WRITE/OUT "*** Error : Could not find catalog {CATALOG}"
   RETURN/EXIT
ENDIF

IF P3(1:1) .eq. "A" THEN
    write/out "average over files..."
    average/image  'p2' = 'CATALOG' P6=Average
ELSE
    write/out "median over files..."
    average/image 'p2' = 'CATALOG' P6=Median
ENDIF

