! @(#)lnlinadd.prg	19.1 (ES0-DMD) 02/25/03 14:24:47
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnlinadd.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!      Search of additional lines.
!      Command:
!         LINADD/SPECTRA image ystart window,ybin 
!                        [method] [lines_added_table] [line_table]
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
CROSSREF in  wbin  y  mtd  line  out
!
def/param p1 ?        i "Enter input image  : "
def/param p2 ?        n "Enter window,ybin  : "
def/param p3 {YSTART} n "Enter starting line: "
def/param p4 {SEAMTD} c
def/param p5 {LINTAB} t
def/param p6 lineadd  t
!
DEFINE/MAXPAR  6
!
VERIFY/LONG {P1}  IMA
VERIFY/SPEC {P5}  ?  LINTAB  TABLE
!
SET/LONG YSTART={P3} SEAMTD={P4} LINTAB={P5} 
!
!write/desc 'p1' START/D/1/2 1.,1.
!write/desc 'p1' STEP/D/1/2 1.,1.
copy/dk    'p1' NPIX/i/1/2 NPIX/I/1/2
!
write/keyw inputi/i/1/1 'p3'
write/keyw inputr/r/1/2 0.,0.
write/keyw inputr/r/1/2 'p2'
!
run STD_EXE:splinadd
!
write/out "Updating table {LINTAB}..."
sel/table  {LINTAB} all
sort/table {LINTAB} :y,:x
write/desc {LINTAB} rebpar/r/1/2 0.0,1.0
write/desc {LINTAB} coeys/d/1/20 0.0 all
copy/kd fitd/i/1/1 {LINTAB} fitd/i/1/1

