! @(#)lnprepa.prg	19.1 (ES0-DMD) 02/25/03 14:24:49
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnprepa.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Implements command PREPARE/LONG
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
CROSSREF  in  out  trim
!
DEFINE/PARAM P1  ?         CHAR  "Frame or catalog to process:"
DEFINE/PARAM P2  ext       CHAR  "Output root name:"
DEFINE/PARAM P3  {TRIM(1)},{TRIM(2)},{TRIM(3)},{TRIM(4)}  NUMB  "Limits:"
!
DEFINE/MAXPAR  3
!
DEFINE/LOCAL LIMIT/I/1/4 {P3}
SET/LONG     TRIM={LIMIT(1)},{LIMIT(2)},{LIMIT(3)},{LIMIT(4)}
!
DEFINE/LOCAL CATAL/I/1/1 0
DEFINE/LOCAL II/I/1/1    1
DEFINE/LOCAL LIMC/C/1/40 "[" 

SET/FORMAT   I1

IF LIMIT(1) .EQ. 0  THEN
   WRITE/KEYW LIMC {LIMC}<,
ELSE
   WRITE/KEYW LIMC {LIMC}@{LIMIT(1)},
ENDIF

IF LIMIT(2) .EQ. 0  THEN
   WRITE/KEYW LIMC {LIMC}<:
ELSE
   WRITE/KEYW LIMC {LIMC}@{LIMIT(2)}:
ENDIF

IF LIMIT(3) .EQ. 0  THEN
   WRITE/KEYW LIMC {LIMC}>,
ELSE
   WRITE/KEYW LIMC {LIMC}@{LIMIT(3)},
ENDIF

IF LIMIT(4) .EQ. 0  THEN
   WRITE/KEYW LIMC {LIMC}>]
ELSE
   WRITE/KEYW LIMC {LIMC}@{LIMIT(4)}]
ENDIF

DEFINE/LOCAL INDEX/I/1/1 0

IF M$INDEX(P1,".bdf") .NE. 0 THEN
   VERIFY/LONG {P1} IMA
   EXTRACT/IMAGE  {P2}{II} = {P1}{LIMC}
ELSE
  LOOP:
  STORE/FRAME IN_A  {P1}
  VERIFY/LONG    {IN_A}  IMA
  WRITE/OUT "Processing image {IN_A}"
  EXTRACT/IMAGE  {P2}{II} = {IN_A}{LIMC}
  II = II + 1
  GOTO LOOP
ENDIF







