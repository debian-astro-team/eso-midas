! @(#)lngcoord.prg	19.1 (ES0-DMD) 02/25/03 14:24:46
! @(#)lngcoord.prg	19.1  (ESO)  02/25/03  14:24:46
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lngccoord.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!      Get coordinates from the display window.
!      Command:
!         GCOORD/LONG  number_of_coords
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
define/param p1 1 N  "Number of coordinates : "
define/param p2 {coorfil} C  "Output table          : "
define/maxpar   2
!
if 'dazdevr(10)' .eq. -1 then
    write/out "*** Please create the display window and load an image ***"
    return/exit
endif
!
get/curs 'p2' ? ? 'p1',1
comp/table 'p2' :y_wcoord = 1.0 + (:y_coord - '{idimemc},start(2)') / '{idimemc},step(2)'
