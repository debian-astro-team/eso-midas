! @(#)lnskyfit.prg	19.1 (ES0-DMD) 02/25/03 14:24:51
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnskyfit.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!     Fit polynomial of degree DEGREE to selected rows in every 
!     column of a frame. May be used to fit the sky background of 
!     a long-slit spectrum.
!     It uses the algorithm of the command FIT_SKY/SPEC, adding the
!     the following feature:
!      filter the cosmics defined as pixels whose values are
!      THRESHOLD times the standard deviation above the median
!      average on pixels within RADIUS (taking into account
!      the RON and the conversion factor)
!
!      Command:
!            SKYFIT/SPECTRA  OUT IN  A1,A2,B1,B2  DEGREE MODE ADUPARS RADIUS
!              defaults:      ?  ?       ?          2     ?     ?       ?
!            where ADUPARS = conversion factor(e-/ADU), RON(ADU), threshold
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
!                  Original version:  ESO-IPG       SEP91
!                  Modified version:  A.Smette      MAY92
!-------------------------------------------------------
!
DEFINE/PARA P1 ? I    "input frame?"
DEFINE/PARA P2 ? I    "output frame?"
DEFINE/PARA P3 {LOWSKY(1)},{LOWSKY(2)},{UPPSKY(1)},{UPPSKY(2)}  N    "limits (a1,a2,b1,b2) in pixels?"
DEFINE/PARA P4 {SKYORD}             N    "order of the fit?"
DEFINE/PARA P5 {SKYMOD}             N    "mode 0 or 1?"
DEFINE/PARA P6 {GAIN},{RON},{SIGMA} N    "conversion factor(e-/ADU), RON(ADU), threshold?"
DEFINE/PARA P7 {RADIUS}             N    "radius?"

WRITE/KEYW OUT_A        'P2'
WRITE/KEYW IN_A         'P1'
WRITE/KEYW INPUTI/I/1/4 'P3'
WRITE/KEYW INPUTI/I/5/1 'P4'
WRITE/KEYW INPUTI/I/6/1 'P5'
WRITE/KEYW INPUTI/I/7/1 'P7'
WRITE/KEYW INPUTR/R/1/3 'P6'

write/out "Processing..."
run STD_EXE:spskyfit
write/out "Done."



