!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993-2005 European Southern Observatory
!.IDENT       lnident.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!                line identification in long slit reduction
!                command:
!                          IDENTIFY/LONG
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
! 050531	last modif
! 
!-------------------------------------------------------
!
CROSSREF wlc ystart lintab  tol
!
define/param  p1  {wlc}     CHAR  "Wavelength calibration frame: "
define/param  p2  {ystart}  NUMB  "Ystart: "
define/param  p3  {lintab}  CHAR  "Line table: "
define/param  p4  +   N   "Error:"
!
define/local cmd/c/1/20 "{mid$cmnd} "		!copy Midas command string
define/local toler/d/1/1 0.
if p4(1:1) .eq. "+"  then
   toler = 4. * step(1)
else
   toler = {p4}
endif
!
verify/long {p1} IMA
set/long  WLC={p1}  YSTART={p2}   LINTAB={p3} 
!
graph/spec
reset/graph
!
if ystart .eq. 0  ystart = npix(2)/2
compute/table   {lintab}  :IDENT=NULL
@s lnident,seline
!
if outputi(1) .eq. 0  then
   write/out "Error: Wrong parameter YSTART = {YSTART}"
   return/exit
endif
!
if cmd(1:1) .eq. "X" then
   set/gcursor ? c_hair
   $ $GUI_EXE/ident.exe {aux_mode(3)} &
   return
endif
!
plot/row          {WLC}       @{YSTART} 
identify/gcursor  {LINTAB}    :IDENT :X   {TOLER}
select/table      {LINTAB}     ALL


ENTRY SELINE

define/param p1  {LINTAB}  TAB
define/param p2  {YSTART}  NUMB
!
define/local  midscan/D/1/1  0.	
define/local  wcbin/D/1/1    0.

if {p2} .lt. 0 .or. {p2} .gt. npix(2) then
   write/out "Error: Wrong parameter YSTART = {p2}"
   return/exit
endif
!
wcbin = M$ABS(YSTEP*STEP(2)/2.)
!
if {p2} .eq. 0 .and. npix(2) .eq. 1 then
   midscan = 1.0
   wcbin = 0.2
elseif {p2} .eq. 1  .or. {p2} .eq. npix(2) then
   midscan  = start(2) + ({P2}-1.)*step(2)
else
   midscan  = start(2) + ({P2}-0.9)*step(2)
endif
!
select/table {p1} ABS(:Y-({midscan})).le.{wcbin}
!
if outputi(1) .eq. 0  then
   write/out "Error: Wrong parameter YSTART = {p2}"
   return/exit
endif
