! @(#)lnpldelta.prg	19.1 (ES0-DMD) 02/25/03 14:24:48
! @(#)lnpldelta.prg	19.1  (ESO)  02/25/03  14:24:48
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnpldelta.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE
!.VERSION     1.0  Package Creation  17-MAR-1993
!-------------------------------------------------------
!
define/param P1  NONE  CHAR   "Mode (NONE/EDIT)"
!
IF P1(1:1) .NE. "E" THEN
   @s lnplot 0 2
ELSE
   @s lnplot 1 2
ENDIF
