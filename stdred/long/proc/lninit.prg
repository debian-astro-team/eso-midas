!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993-2009 European Southern Observatory
!.IDENT       lninit.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!                init parameters for Spectra reduction
!                Command: INIT/SPEC
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
! 091222	last modif
! 
!-------------------------------------------------------
!
if p1(1:1) .eq. "?" then
   write/out Set parameters to default value
   register/session long STD_PROC lninit.prg lntab.tbl	!Initialisation
   !
   !Begin Session List 
   !
   !
   ! Common Keywords: The type definition of the following keywords must be
   ! consistent with the other spectroscopy packages Mos and Echelle:
   !
   WRITE/KEYW ALPHA/R/1/1  0.4 ! Rejection parameter for lines matching [0,0.5]
   WRITE/KEYW AVDISP/R/1/1    0.   ! Average dispersion per pixel
   WRITE/KEYW BETA/R/1/1      0.   ! Non-linearity in mode LINEAR
   WRITE/KEYW COERBR/C/1/60    coerbr.tbl  ! Table of coefficients for RBR
   WRITE/KEYW FILTMED/I/1/3    10        ! Radius of median filter
   WRITE/KEYW FILTSMO/I/1/3    10        ! Radius of smoothing filter
   WRITE/KEYW FLAT/C/1/60      " " ALL      ! Flat-Field Image
   WRITE/KEYW FLUXTAB/C/1/120   +++       ! Flux Table of the standard star
   WRITE/KEYW GAIN/R/1/1     2.     ! Gain  (e-/ADU)       
   WRITE/KEYW GUESS/C/1/60    +++  ! Guess session name
   WRITE/KEYW LINCAT/C/1/80    MID_ARC:hear.tbl    ! line catalogue
   WRITE/KEYW LINTAB/C/1/60    line.tbl    ! Table of line identifications
   WRITE/KEYW NPIX/I/1/2    0,0    ! size of the raw images in pixels
   WRITE/KEYW REBMTD/C/1/12 LINEAR !Rebinning method (LINEAR,QUADRATIC,SPLINE)
   WRITE/KEYW REBSTRT/D/1/1  0.     ! Starting wavelength for rebinning
   WRITE/KEYW REBEND/D/1/1   0.     ! Final wavelength for rebinning
   WRITE/KEYW REBSTP/D/1/1   0.     ! Wavelength step for rebinning
   WRITE/KEYW RON/R/1/1      7.     ! Read-Out-Noise (ADU)
   WRITE/KEYW START/D/1/2   0.,0.  ! start points of the raw image. 
   WRITE/KEYW STD/C/1/60       +++       ! Standard Star Spectrum
   WRITE/KEYW TOL/R/1/1     0.2    ! tolerance in Angstroms for wavelength ident.
   WRITE/KEYW WCENTER/D/1/1   0.   ! Central wavelength 
   WRITE/KEYW WLC/C/1/80      " " ALL ! wavelength calibration image
   WRITE/KEYW WLCMTD/C/1/10 IDENT  ! Wavelength calibration method (IDENT,GUESS)
   WRITE/KEYW WLCNITER/I/1/2  2,20 ! Minimum, Maximum number of iterations
   WRITE/KEYW YSTART/I/1/1  0  ! Starting row for the calibration (pixel value)
   !
   ! End of Common Keywords
   !
   WRITE/KEYW INSTRUME/C/1/20 "EFOSC" ! instrument  
   WRITE/KEYW GRINIT/C/1/3    "YES"   ! Graph initialisation flag
   !
   WRITE/KEYW WIDTH/I/1/1   8   ! Window size in X for line detection (pixels)
   WRITE/KEYW THRES/R/1/1 30  !Threshold for line detection (above local median)
   WRITE/KEYW SEAMTD/C/1/12 GAUSS  ! Search centering method (GAUSS, GRAV, MAXI)
   WRITE/KEYW YWIDTH/I/1/1  0      !Window size in Y for line detection (pixels)
   WRITE/KEYW YSTEP/I/1/1   1      ! Step in Y for line searching (pixels)
   !
   WRITE/KEYW TWODOPT/C/1/3 NO     ! Computes bivariate polynomial option.
   WRITE/KEYW FITD/I/1/1    3      ! fit degree of the dispersion coeff.
   WRITE/KEYW DCX/I/1/2     3,2    ! fit degree of the dispersion coeff.
   ! The 2 following keywords must be initialized with extension .tbl
   WRITE/KEYW SESSION/C/1/60   default.tbl ! Session name 
   WRITE/KEYW WRANG/I/1/2   0,12000 ! wavelength range to take from LINCAT
   WRITE/KEYW IMIN/R/1/1    0       ! lower limit from LINCAT
   
   WRITE/KEYW MAXDEV/R/1/1    10.  ! Maximum deviation (pixels)
   WRITE/KEYW COROPT/C/1/3    NO   ! Computes correlation
   WRITE/KEYW CORVISU/C/1/3   YES  ! Plots correlation peak
   WRITE/KEYW SHIFT/R/1/1     0.   ! Shift in pixels
   WRITE/KEYW DISPCOE/D/1/3  0.,0.,0. ! Dispersion coefficients
   !
   !
   WRITE/KEYW EXTMTD/C/1/12  LINEAR  ! Extraction method (AVERAGE, LINEAR)
   WRITE/KEYW OBJECT/I/1/2   0,0     !Lower, upper row number of object spectrum
   WRITE/KEYW LOWSKY/I/1/2   0,0     ! Lower, upper row number of lower sky
   WRITE/KEYW UPPSKY/I/1/2   0,0     ! Lower, upper row number of upper sky
   !
   WRITE/KEYW SIGMA/R/1/1    3.   !Threshold for rejection of cosmics (std dev.)
   WRITE/KEYW RADIUS/I/1/1   2      ! Radius for cosmics rejection
   WRITE/KEYW SKYORD/I/1/1   1      ! Orderfor sky fit
   WRITE/KEYW SKYMOD/I/1/1   0      ! Mode of fitting
   WRITE/KEYW ORDER/I/1/1    3      ! Order for optimal extraction
   WRITE/KEYW NITER/I/1/1    3      ! Number of iterations
   WRITE/KEYW COORFIL/C/1/20 middummgc.tbl ! Name of coords table of GCOORD/LONG
   !
   WRITE/KEYW DARK/C/1/60      " " ALL      ! Dark image or constant
   WRITE/KEYW BIAS/C/1/60      0.           ! Bias image or constant 
   WRITE/KEYW COMET/C/1/12     MEDIAN       !Combination method (AVERAGE/MEDIAN)
   WRITE/KEYW FFIT/C/1/60      middummf.bdf ! Flat fitted function
   WRITE/KEYW FDEG/I/1/1       2            ! Flat fitting degree
   WRITE/KEYW FVISU/C/1/3      YES          ! Visualisation flag (YES/NO)
   !
   WRITE/KEYW OUTPUTF/C/1/60   " " ALL  ! Output generic name
   WRITE/KEYW OUTNUMB/I/1/1    1        ! Output starting number
   WRITE/KEYW INPUTF/C/1/60    " " ALL  ! Input generic name 
   WRITE/KEYW INPNUMB/C/1/60   " " ALL  ! Input generic name 
   WRITE/KEYW ROTOPT/C/1/3     NO       ! Rotation Option (YES/NO)
   WRITE/KEYW ROTSTART/D/1/1   1.       ! Y-start after rotation
   WRITE/KEYW ROTSTEP/D/1/1    1.       ! Y-step after rotation
   WRITE/KEYW TRIMOPT/C/1/3    NO       ! Trim Option  (YES/NO)
   WRITE/KEYW TRIM/I/1/4       0,0,0,0  ! Trim window (x1,y1,x2,y2) in pixels
   WRITE/KEYW REBOPT/C/1/3     NO       ! Rebin Option (YES/NO)
   WRITE/KEYW EXTOPT/C/1/3     NO       ! Extinction Correction Option (YES/NO)
   WRITE/KEYW FLATOPT/C/1/3    NO       ! Flat Correction (YES/NO)
   WRITE/KEYW RESPOPT/C/1/3    NO       ! Response Correction Option  (YES/NO)
   WRITE/KEYW BIASOPT/C/1/3    NO       ! Bias Correction  (YES/NO)
   WRITE/KEYW DARKOPT/C/1/3    NO       ! Dark Correction (YES/NO)
   !
   WRITE/KEYW RESPONSE/C/1/60  response  ! Response Image
   WRITE/KEYW RESPTAB/C/1/60   resp.tbl  ! Intermediate Response Table
   WRITE/KEYW EXTAB/C/1/60     MID_EXTINCTION:atmoexan.tbl  ! Extinction Table
   WRITE/KEYW PLOTYP/C/1/6     RATIO     ! Type of plot (RATIO, MAGNITUDE)
   WRITE/KEYW FITYP/C/1/12     POLY      ! Type of fit (POLY, SPLINE)
   WRITE/KEYW RESPLOT/C/1/3    YES       ! Plot flag for response computation
   WRITE/KEYW SMOOTH/R/1/1     0.        ! Smoothing factor for spline fitting
   !
   WRITE/KEYW STEP/D/1/2    0.,0.  ! start points of the raw image. 
   !End Session List
   !
   ! Coefficients for slit distortion correction.
   !
   WRITE/KEYW COEFYI/I/1/20 0   ALL  
   WRITE/KEYW COEFYR/R/1/10 0.  ALL
   WRITE/KEYW COEFYD/D/1/20 0.  ALL
   WRITE/KEYW COEFYC/C/1/20 " " ALL
   !
   SET/LONG SESSION = default.tbl
   !
else
   write/out Parameters are initialized from table {p1}

   verify/long {p1} TAB

   define/local indx/i/1/1 0
   define/local partab/C/1/60  " "  ALL

   compute/keyw indx = m$index(p1,".tbl")

   if indx .eq. 0 then
      copy/keyw p1 partab
   else
      indx = indx - 1
      copy/keyw p1/C/1/{indx} partab
   endif

   set/long session = {partab}.tbl
   savinit/long  {partab}.tbl    READ
   -copy {partab}.tbl    {LINTAB}
   -copy {partab}COE.tbl {COERBR}
   !
endif
!
! initialize keyword for AGL applications 
!
write/keyw AGLIMS/R/1/4 0.0,0.0,0.0,0.0

