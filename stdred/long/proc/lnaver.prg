! @(#)lnaver.prg	19.1  (ESO)  02/25/03  14:24:44
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnaver.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!      Extract spectrum from a CCD frame using  row averages
!      Command:
!         EXTRACT/AVERAGE in out  [obj]  [sky]  [mtd]
!
!.VERSION     1.0  Package Creation            17-MAR-1993  
! 020906	last modif
!----------------------------------------------------------------------
!
CROSSREF in  out  obj  sky  mtd 
!
define/param p1 ? I "Enter input frame        : "
define/param p2 ? I "Enter output frame       : "
define/param p3 {OBJECT(1)},{OBJECT(2)}  N "Enter object coordinates : "
define/param p4 {LOWSKY(1)},{LOWSKY(2)},{UPPSKY(1)},{UPPSKY(2)} N "Sky: "
define/param p5 {EXTMTD} C "Extraction method:"
!
define/maxpar  5
!
define/local cobj/i/1/2 {p3}
define/local csky/i/1/4 {p4}
define/local errors/I/1/1  0

verify/long  {p1} ima
set/format   i1

copy/keyw  cobj/i/1/2  object/i/1/2
copy/keyw  csky/i/1/2  lowsky/i/1/2
copy/keyw  csky/i/3/2  uppsky/i/1/2
set/long  EXTMTD={p5}

if EXTMTD(1:1) .NE. "L" .AND. EXTMTD(1:1) .NE. "A" THEN
   write/out "Error: Wrong parameter EXTMTD = {EXTMTD}"
   write/out "       Possible values are LINEAR and AVERAGE"
   return/exit
endif

define/local np/i/1/1    {{p1},npix(2)}

if cobj(1) .lt. 1 .or.  cobj(1) .gt. np  errors = 1
if cobj(2) .lt. 1 .or.  cobj(2) .gt. np  errors = 1
if csky(1) .lt. 1 .or.  csky(1) .gt. np  errors = 1
if csky(2) .lt. 1 .or.  csky(2) .gt. np  errors = 1
if csky(3) .lt. 1 .or.  csky(3) .gt. np  errors = 1
if csky(4) .lt. 1 .or.  csky(4) .gt. np  errors = 1

if errors .ne. 0 then
   write/out "Error :  Invalid object or sky limits "
   write/out "         (session keywords OBJECT, LOWSKY, UPPSKY)"
   write/out "  Valid range is     : 1,{np}"
   write/out "  Present values are : OBJECT = {OBJECT(1)},{OBJECT(2)}"
   write/out "                       LOWSKY = {LOWSKY(1)},{LOWSKY(2)}"
   write/out "                       UPPSKY = {UPPSKY(1)},{UPPSKY(2)}"
   return/exit
endif

average/row &a = {p1} @{csky(1)},@{csky(2)} 
average/row &b = {p1} @{csky(3)},@{csky(4)} 
compute/image &s = (&a + &b) / 2.0

average/row &o   = {p1} @{cobj(1)},@{cobj(2)} 

comp/image {p2} = &o - &s

if extmtd(1:1) .eq. "L" then
  compute/image {P2} = {P2}*({cobj(2)}-{cobj(1)}+1)
endif

copy/dd  {p1}  *,3  {p2}

