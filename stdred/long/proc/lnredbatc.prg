! @(#)lnredbatc.prg	19.1 (ES0-DMD) 02/25/03 14:24:50
! @(#)lnredbatc.prg	19.1  (ESO)  02/25/03  14:24:50
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnredbatc.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!                Opens the batch reduction interface
!                command:
!                          BATCH/LONG
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
WRITE/OUT "Preparing the Batch Reduction user interface..."
! trick for updating keywords
sync/midas
$ $GUI_EXE/batchred.exe {AUX_MODE(3)} {MID$SESS(11:12)}  &
