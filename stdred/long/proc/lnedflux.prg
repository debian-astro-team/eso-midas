! @(#)lnedflux.prg	19.1 (ES0-DMD) 02/25/03 14:24:45
! @(#)lnedflux.prg	19.1  (ESO)  02/25/03  14:24:45
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnedflux.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE 
!             Edition of response table (flux calibration process)
!               Command:
!                          EDFLUX/LONG [response table]
!         
!.VERSION     1.0  Package Creation  17-MAR-1993
!----------------------------------------------------------------------
!
define/param p1 {RESPTAB}  C "Response table : "
verify/long {p1} TAB
set/long RESPTAB={p1}

graph/spec

run STD_EXE:lnedflux 
