! @(#)lnloadima.prg	19.1 (ES0-DMD) 02/25/03 14:24:47
! @(#)lnloadima.prg	19.1  (ESO)  02/25/03  14:24:47
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnloadima.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE
!          load a frame in the display window calculating
!          automatically the cuts. It is based in the IHAP 
!          algorithm.
!               Command:
!                          LOAD/LONG frame [scale-x[,scale-y]]
!.VERSION     1.0  Package Creation  17-MAR-1993
!-------------------------------------------------------
!
def/param p1 ? i "Enter image : "
def/param p2 1

def/loc exist/i/1/1 0
def/loc index/i/1/1 0
def/loc inpfil/c/1/60 'p1'

index = m$index(p1,".")
if index .eq. 0 then
   write/keyw inpfil 'p1'.bdf
endif
exist = m$exist(inpfil)
if exist .eq. 0 then
   write/out "Error ** Could not find image : {inpfil} **"
   return
endif

log(4) = 2

def/loc xpix/i/1/1 0
def/loc ypix/i/1/1 0
def/loc xstart/i/1/1 0
def/loc xend/i/1/1 0
def/loc ystart/i/1/1 0
def/loc yend/i/1/1 0
def/loc mini/r/1/1 0
def/loc maxi/r/1/1 0
def/loc cutl/r/1/1 0
def/loc cuth/r/1/1 0
def/loc xzero/r/1/1 0
def/loc yzero/r/1/1 0
 
xpix = '{p1},npix(1)'
ypix = '{p1},npix(2)'

xzero = '{p1},start(1)'
yzero = '{p1},start(2)'

xstart = 'xpix' * 0.20 
xend   = 'xpix' * 0.80
ystart = 'ypix' * 0.10
yend   = 'ypix' * 0.12
stat/image 'p1' [@'xstart',@'ystart':@'xend',@'yend']

mini = 'outputr(3)'
maxi = 'outputr(3)'

ystart = 'ypix' * 0.50 
yend   = 'ypix' * 0.52
stat/image 'p1' [@'xstart',@'ystart':@'xend',@'yend']

if 'mini' .gt. 'outputr(3)' mini = 'outputr(3)'
if 'maxi' .lt. 'outputr(3)' maxi = 'outputr(3)'

ystart = 'ypix' * 0.88
yend   = 'ypix' * 0.90
stat/image 'p1' [@'xstart',@'ystart':@'xend',@'yend']

if 'mini' .gt. 'outputr(3)' mini = 'outputr(3)'
if 'maxi' .lt. 'outputr(3)' maxi = 'outputr(3)'

log(4) = 0

cutl = 0.9 * 'mini'
cuth = 1.2 * 'maxi'
load/image 'p1' cuts='cutl','cuth' scale='p2'
