! @(#)lnplsear.prg	19.1 (ES0-DMD) 02/25/03 14:24:49
! @(#)lnplsear.prg	19.1  (ESO)  02/25/03  14:24:49
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnplsear.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE
!.VERSION     1.0  Package Creation  10-JUN-1993
!-------------------------------------------------------
!
define/param P1  2D       CHAR "Mode (1D/2D)"
define/param P2 {LINTAB}  TAB  "Input table:"
define/param P3 :X        CHAR "Info column:"

graph/spec
RESET/GRAPH
reset/graph pmode=1 font=1

IF NPIX(2) .EQ. 1 .OR. P1(1:1) .EQ. "1" THEN
   PLOT/ROW {WLC} @{YSTART}
   @s lnident,seline
   reset/graph  color=4
   OVERPLOT/IDENT {P2} :X {P3} TOP
   SELECT/TABLE {P2} ALL
ELSE
   reset/graph ltype=0  stype=1 
   SELECT/TABLE {P2} ALL
   plot/table {P2}  :X :Y
ENDIF

reset/graph
RETURN
