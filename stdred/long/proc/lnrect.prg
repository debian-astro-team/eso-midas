! @(#)lnrect.prg	19.1 (ES0-DMD) 02/25/03 14:24:50
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnrect.prg
!.PURPOSE:        MIDAS command procedure RECTIFY/SPECTRUM
!.AUTHOR:         J.D.Ponz
!.VERSION:        850813  D. Baade, ST-ECF: creation
!.VERSION:        881220  J.D.Ponz
!.USE:            RECTIFY/SPECTRUM in out ref subsampling subfactor
! ------------------------------------------------------------------
!
DEFINE/PARAM P1 ? IMA         "Enter input  image:"
DEFINE/PARAM P2 ? IMA         "Enter output image:"
DEFINE/PARAM P4 1 NUM         "Nrep:"
DEFINE/PARAM P5 N CHAR        "Deconvolution flag:"
DEFINE/PARAM P6 {LINTAB} TAB  "Reference table:"
!
DEFINE/LOCAL MYACT/C/1/2 'MID$CMND(11:12)'       !save qualifier...
DEFINE/LOCAL ERRSAV/I/1/1 'ERROR(3)'  ! disable errors in regression...
!

IF M$EXISTD(P6,"COEFXYD") .EQ. 0 THEN
   WRITE/OUT "Error: "
   WRITE/OUT "Computation of the bivariate solution has not"
   WRITE/OUT "been performed. Check session keyword TWODOPT"
   RETURN/EXIT
ENDIF
!
ERROR(3) = -1			
WRITE/KEYW INPUTI/I/1/1 'P4'
IF P5(1:1) .EQ. "Y" THEN
   INPUTR(1) = 3                 ! SUBFACTOR in RECTIMAG/RECTSPEC
ELSE
   INPUTR(1) = 1.
ENDIF

INPUTD(1) = REBSTRT
INPUTD(2) = REBSTP
INPUTI(3) = (REBEND-REBSTRT)/REBSTP + 0.5
CREATE/IMAGE &h 2,{INPUTI(3)},{NPIX(2)} {INPUTD(1)},{START(2)},-
{INPUTD(2)},{STEP(2)}
WRITE/KEYW    IN_B  middummh.bdf

COPY/KEYW  START/D/1/2   INPUTD/D/1/2
COPY/KEYW  STEP/D/1/2    INPUTD/D/3/2

COPY/DK {P6}  COEFXYC   KEYLONGC
COPY/DK {P6}  COEFXYI   KEYLONGI
COPY/DK {P6}  COEFXYD   KEYLONGD

RUN STD_EXE:RECTSPEC  ! RECTIFY/SPECTRUM

COPY/DD {P1} *,3 {P2}



