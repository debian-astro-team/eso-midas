!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993-2008 European Southern Observatory
!.IDENT       ffnorm.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Execute the command:
!                NORMALIZE/FLAT  input  norm  bias  [degree] [function]
!             Fit a polynomial of degree DEGREE to the mean of the 
!             flat-field of a long-slit spectrum. 
!             Needed by EXTRACT/SPECTRUM.
!.VERSION     1.0  Package Creation  17-MAR-1993  
! 080321	last modif
!------------------------------------------------------------------------
!
CROSSREF in out bias deg fit visu
!
DEFINE/PARA P1 ?              IMA    "Input image    :"
DEFINE/PARA P2 ?              IMA    "Normalized flat:"
DEFINE/PARA P3 {BIAS}         CHAR   "Bias value:"
DEFINE/PARA P4 {FDEG}      N      "Fit order:"
DEFINE/PARA P5 {FFIT}      CHAR   "Fitted function:"
DEFINE/PARA P6 {FVISU}     CHAR   "Visualisation flag: "
!
DEFINE/MAXPAR  6 
!
VERIFY/LONG  {P1}  IMA
IF M$TSTNO(P3) .EQ. 0  VERIFY/LONG {P3} IMA
!
SET/LONG     BIAS={P3}  FDEG={P4}  FFIT={P5}  FVISU={P6}
!
COMPUTE/IMAGE  &a = {P1} - {P3}
!
WRITE/KEYW IN_A          middumma.{mid$types(1:8)}
WRITE/KEYW OUT_A         'P2'
WRITE/KEYW OUT_B         'P5'
!
WRITE/KEYW INPUTR/R/1/1  0.
WRITE/KEYW INPUTI/I/1/1  'P4'
!
RUN STD_EXE:FFNORM
!
IF FVISU(1:1) .EQ. "Y" THEN
   define/local cent/I/1/1  0
   cent = {{P1},NPIX(2)} / 2
   @ creifnot 1
   PLOT/ROW {P1}  @{cent}
   OVERPLOT/ROW {FFIT} @{cent}
ENDIF




