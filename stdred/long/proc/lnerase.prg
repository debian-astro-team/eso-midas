! @(#)lnerase.prg	19.1 (ES0-DMD) 02/25/03 14:24:45
! @(#)lnerase.prg	19.1  (ESO)  02/25/03  14:24:45
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnerase.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE
!.VERSION     1.0  Package Creation  17-MAR-1993
!-------------------------------------------------------
!
RUN STD_EXE:lnerase
