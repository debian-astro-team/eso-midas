! @(#)lnclean.prg	19.1 (ES0-DMD) 02/25/03 14:24:44
! @(#)lnclean.prg	19.1  (ESO)  02/25/03  14:24:44
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnclean.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!               clean the temporary files created by Long and
!		ends the context.
!               Command:
!                          CLEAN/LONG
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
del/table {LINTAB}    no
del/table {COERBR}    no
del/table {RESPTAB}   no
!
! it's done many times to remove spec and long
!
clear/context
clear/context

