! @(#)lnsearch.prg	19.1 (ES0-DMD) 02/25/03 14:24:51
! @(#)lnsearch.prg	19.1  (ESO)  02/25/03  14:24:51
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnsearch.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!            1) Detects spectral features, like lines of an arc spectrum
!            2) Creates all necessary columns of the table LINTAB.
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
CROSSREF in thres width yaver step mtd mode out
!
define/param P1 {WLC}     I "Enter input image : "
define/param P2 {THRES}   N "Threshold:"
define/param P3 {WIDTH}   N "Width:"
define/param P4 {YWIDTH}  N "Row average:"
define/param P5 {YSTEP}   N "Step:"
define/param P6 {SEAMTD}  C "Method:"
define/param P7 EMISSION  C "Mode:"
define/param P8 HISTO     C "Detection algorithm: HISTO or LOCALMAX"
!
DEFINE/MAXPAR 8
!
VERIFY/LONG {P1} IMA
!
COPY/DK {P1}  START  START
COPY/DK {P1}  STEP   STEP
COPY/DK {P1}  NPIX   NPIX
!
SET/LONG  WLC={P1}  THRES={P2}  WIDTH={P3}  YWIDTH={P4}-
    YSTEP={P5}  SEAMTD={P6}
!
DEFINE/LOCAL LTHRES/R/1/1 {THRES}
!
IF THRES .LT. 0. THEN
   LTHRES = (-1.)*THRES
   ESTIMATE/NOISE {P1} {LTHRES}
   LTHRES = OUTPUTR(3)
ELSE
   LTHRES = THRES
ENDIF
!
IF {{P1},NAXIS} .EQ. 1 .OR. {{P1},NPIX(2)} .EQ. 1 THEN 	  ! 1D-spectra
   YSTEP    = 1
   YWIDTH   = 0
   YSTART   = 1
   DCX(2)   = 0
  IF {{P1},NAXIS} .EQ. 1 THEN
    NPIX(2)  = 1
    START(2) = 1.
    STEP(2)  = 1.
  ENDIF
ELSE
  YSTART = NPIX(2)/2
ENDIF
!
WRITE/KEYW  IN_A         {WLC}
WRITE/KEYW  OUT_A        {LINTAB}
INPUTI(1) = 0
IF P8(1:1) .EQ. "L" INPUTI(1) = 1
INPUTR(1) = WIDTH
INPUTR(2) = LTHRES
INPUTR(3) = YWIDTH
INPUTR(4) = YSTEP
WRITE/KEYW  IN_B         {SEAMTD}
WRITE/KEYW  OUT_B        {P7}
COPY/KEYW   START/D/1/2  INPUTD/D/1/2
COPY/KEYW   STEP/D/1/2   INPUTD/D/3/2
!
RUN STD_EXE:lnsearch
!
! Create aux. column for plotting and calibration processes
!
verify/spec {lintab} ? lintab
!
write/desc {LINTAB} rebpar/r/1/2 0.0,1.0
write/desc {LINTAB} coeys/d/1/20 0.0 all
copy/kd fitd/i/1/1 {LINTAB} fitd/i/1/1
!
CREATE/COLUMN {LINTAB}  :ERASED    C*4  A1     " "
CREATE/COLUMN {LINTAB}  :IDENT     R*8  F15.7
CREATE/COLUMN {LINTAB}  :WAVE      R*8  F15.7
CREATE/COLUMN {LINTAB}  :WAVEC     R*8  F15.7
CREATE/COLUMN {LINTAB}  :RESIDUAL  R*8  F15.7
!
RETURN







