! @(#)lnshow.prg	19.1 (ES0-DMD) 02/25/03 14:24:51
! @(#)lnshow.prg	19.1  (ESO)  02/25/03  14:24:51
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       longshowlnshow.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, long
!.PURPOSE     Execute command SHOW/LONG
!.VERSION     1.0    Creation    27.06.92   PB
!
!-------------------------------------------------------
!
DEFINE/PARAM P1  ALL  CHAR  "Selection (GEN, WLC, FLAT, EXTR, INST, REDU) : "
SESSLINE = 0
IF P8(1:1) .EQ. "H" GOTO HELP
!
!ECHO/OFF
SET/FORMAT I1 F12.3
!
IF P1(1:1) .EQ. "G"  GOTO GEN
IF P1(1:1) .EQ. "W"  GOTO WLC
IF P1(1:1) .EQ. "D"  GOTO COMB
IF P1(1:1) .EQ. "E"  GOTO EXTR
IF P1(1:1) .EQ. "R"  GOTO REBI
IF P1(1:1) .EQ. "F"  GOTO FLUX
IF P1(1:1) .EQ. "B"  GOTO REDU
!
GEN:
show/sess "--- 1. (G) General Session Parameters ---"
show/sess "Instrument       :  INSTRUME = 'INSTRUME'"
show/sess "Image size       :  NPIX   = 'NPIX(1)','NPIX(2)'"
show/sess "Start descriptor :  START  = 'start(1)','start(2)'"
show/sess "Step descriptor  :  STEP   = 'step(1)','step(2)'"
show/sess "Graphic Initial. :  GRINIT = 'GRINIT'"

IF P1(1:1) .NE. "A"  RETURN
show/sess  "    "

WLC:
show/sess "--- 2. (W) Wavelength Calibration Parameters ---"
show/sess " ... SEARCH/LONG"
show/sess "Wavelength Calibration Frame  : WLC     = 'WLC'"
show/sess "Line table                    : LINTAB  = {LINTAB}"
show/sess "Search method                 : SEAMTD  = 'SEAMTD'"
show/sess "Half-size of averaging window : YWIDTH  = 'YWIDTH'"
show/sess "Step                          : YSTEP   = 'YSTEP'"
show/sess "Threshold above background    : THRES   = 'THRES'"
show/sess "Width of local histogram      : WIDTH   = 'WIDTH'"
show/sess " ... IDENTIFY/LONG"
show/sess "Calibration Starting Row      : YSTART  = 'YSTART'"
show/sess "Line Catalog                  : LINCAT  = 'LINCAT'"
show/sess "Wavelength Range              : WRANG   = 'WRANG(1)','WRANG(2)'"
show/sess "Minimal Intensity in Catalog  : IMIN    = 'IMIN'"
show/sess " ... CALIBRATE/LONG"
show/sess "Coefficients table            : COERBR   = {COERBR}"
show/sess "Calibration method            : WLCMTD   = 'WLCMTD'"
show/sess "2D disp. relation (Yes/No)    : TWODOPT  = 'TWODOPT'"
IF WLCMTD(1:1) .EQ. "G" THEN   
   show/sess "Guess Session                 : GUESS    = 'GUESS'"
   show/sess "Compute Cross-Correlation     : COROPT   = 'COROPT'"
   show/sess "Shift                         : SHIFT    = 'SHIFT'"
   show/sess "Plot correlation peak         : CORVISU  = 'CORVISU'"
ENDIF
IF WLCMTD(1:1) .EQ. "L" THEN   
   show/sess "Central Wavelength            : WCENTER  = 'WCENTER'"
   show/sess "Average dispersion            : AVDISP   = 'AVDISP'"
ENDIF
show/sess "Tolerance                     : TOL      = 'TOL'"
show/sess "Degree of polynomial          : DCX      = 'DCX(1)','DCX(2)'"
show/sess "Min. and Max. number of iter. : WLCNITER = 'WLCNITER(1)','WLCNITER(2)'"
show/sess "Matching parameter (0 to 0.5) : ALPHA    = 'ALPHA'"
show/sess "Maximal Deviation             : MAXDEV   = 'MAXDEV'"

IF P1(1:1) .NE. "A"  RETURN
show/sess  "    "

COMB:
show/sess "--- 3. (D) Dark, Bias And Flat-Field ---"
show/sess " ... NORMALIZE/FLAT"
show/sess "Flat-Field fitting function : FFIT  = {FFIT} "
show/sess "Fitting degree              : FDEG  = {FDEG}"
show/sess "Visualisation flag          : FVISU = {FVISU}"
show/sess " ... COMBINE/LONG"
show/sess "Combination method          : COMET = {COMET}"
show/sess "Flat-Field Image            : FLAT  = {FLAT}"
show/sess "Bias Image or Constant      : BIAS  = {BIAS}"
show/sess "Dark Image or constant      : DARK  = {DARK}"

IF P1(1:1) .NE. "A"  RETURN
show/sess  "    "

EXTR:
show/sess "--- 4. (E) Extraction Parameters ---"
show/sess "Extraction method           : EXTMTD  = {EXTMTD}"
show/sess "Object limits (pixels)      : OBJECT  = {OBJECT(1)},{OBJECT(2)}"
show/sess "Lower sky limits (pixels)   : LOWSKY  = {LOWSKY(1)},{LOWSKY(2)}"
show/sess "Upper sky limits (pixels)   : UPPSKY  = {UPPSKY(1)},{UPPSKY(2)}"

show/sess "Read-Out Noise (ADU)        : RON     = {RON}"
show/sess "Gain (e-/ADU)               : GAIN    = {GAIN}"
show/sess "Cosmics rejection threshold : SIGMA   = {SIGMA}"
show/sess "Radius                      : RADIUS  = {RADIUS}"
show/sess "Optimal extraction order    : ORDER   = {ORDER}"
show/sess "Sky Fitting Order           : SKYORD  = {SKYORD}"
show/sess "Sky Fitting Mode            : SKYMOD  = {SKYMOD}"
show/sess "Number of Iterations        : NITER   = {NITER}"

IF P1(1:1) .NE. "A"  RETURN
show/sess  "    "

REBI:
show/sess "--- 5. (R) Rebin Parameters ---"
show/sess "Rebinning method            : REBMTD  = {REBMTD}"
show/sess "Wavelength Start            : REBSTRT = {REBSTRT}"
show/sess "Wavelength End              : REBEND  = {REBEND}"
show/sess "Wavelength Step             : REBSTP  = {REBSTP}"

IF P1(1:1) .NE. "A"  RETURN
show/sess  "    "

FLUX:

show/sess --- 6. (F) Flux Calibration  Parameters --- 
show/sess " ... EXTINCTION/LONG"
show/sess "Airmass Extinction Table    : EXTAB    = {EXTAB}"
show/sess " ... INTEGRATE/LONG"
show/sess "Standard Star               : STD      = {STD}"
show/sess "Flux Table                  : FLUXTAB  = {FLUXTAB}"
show/sess "Intermediate Response Table : RESPTAB  = {RESPTAB}"
show/sess " ... RESPONSE/LONG"
show/sess "Plot type                   : PLOTYP   = {PLOTYP}"
show/sess "Fit type                    : FITYP    = {FITYP}"
show/sess "Response Fitting Degree     : FITD     = {FITD}"
show/sess "Smoothing factor            : SMOOTH   = {SMOOTH}"
show/sess "Final Response Image        : RESPONSE = {RESPONSE}"
show/sess " ... RESPONSE/FILTER"
show/sess "Median filter half-size     : FILTMED  = {FILTMED}"
show/sess "Smooth filter half-size     : FILTSMO  = {FILTSMO}"


IF P1(1:1) .NE. "A"  RETURN
show/sess  "    "


REDU:

show/sess "--- 7. (B) Batch Reduction Parameters ---"
show/sess "Input images generic name      : INPUTF  = {INPUTF}"
show/sess "Output Images generic name     : OUTPUTF = {OUTPUTF}"
show/sess "Output images starting number  : OUTNUMB = {OUTNUMB}"
show/sess "Dark Correction Option         : DARKOPT = {DARKOPT}"
show/sess "Flat-Field Correction Option   : FLATOPT = {FLATOPT}"
show/sess "Response Correction Option     : RESPOPT = {RESPOPT}"
show/sess "Extinction Correction Option   : EXTOPT  = {EXTOPT}"
show/sess "Rebin Option                   : REBOPT  = {REBOPT}"
show/sess "Sub-Window Processing Option   : TRIMOPT = {TRIMOPT}"
show/sess "Rotation Option                : ROTOPT  = {ROTOPT}"

RETURN

HELP:

SHOW/SESS "Long-slit package: RTM"

RETURN



