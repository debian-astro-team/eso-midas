! @(#)lnfext.prg	19.1 (ES0-DMD) 02/25/03 14:24:46
! @(#)lnfext.prg	19.1  (ESO)  02/25/03  14:24:46
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnfext.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE 
!               Generates the extinction spectrum from the extinction
!               table and the airmass.
!               Calls: extin/spec (from context SPEC)
!               Command:
!                          EXTINCTION/LONG
!
! .INPUT/OUTPUT:
! inimage   - the image to be correct for extinction
! outimage  - the image to hold the corrected data
! scale     - the scaling factor: a) E(B-V) if interstellar reddening
!				  b) AIRMASS if atmospheric extinction
! table     - the table with the wavelength dependence of the reddening or
!             the extinction
! col       - the column of the table to be used
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
CROSSREF IN OUT SCALE TABLE
!
define/param P1  ?              I   "Enter input  image : "
define/param P2  ?              C   "Enter output image : "
define/param P3  1              N   "Scaling factor"
define/param P4 {EXTAB}         TAB "Extinction table : "
DEFINE/PARAM P5  2             NUMBER
!
DEFINE/MAXPAR 5
!
VERIFY/LONG {P1}  IMA
set/long extab={P4}
VERIFY/SPEC {P4} MID_EXTINCTION EXTAB
!
! The command EXTINCT/SPEC recognizes two keywords:
!  o_airm  (as in EFOSC)
!  airmass (as in EMMI)
!

! By default if P3=0, the command EXTINCT/SPEC looks for the airmass value
! in the two above descriptors. If the descriptors are not present, user
! is prompted to provide the value. If P3 is different from 0, this value is
! taken as airmass value.
!
DEFINE/LOCAL SCALE/R/1/1 'P3'
!
IF 'P3' .EQ. 1 THEN  ! get airmass
  IF M$EXISTD(P1,"O_AIRM")  .EQ. 1  THEN
              COPY/DK 'P1' O_AIRM   SCALE
  ELSE
       IF M$EXISTD(P1,"AIRMASS") .EQ. 1  THEN 
            COPY/DK 'P1' AIRMASS  SCALE
       ELSE
            WRITE/OUT "Could not find descriptor O_AIRM nor AIRMASS in image {P1}"
            WRITE/OUT "Taking default value: 1."
            SCALE = 1.
       ENDIF
  ENDIF
ENDIF
!
WRITE/OUT "Airmass/Extinction value : 'SCALE'"
!
CREATE/IMAGE &r  1,{{P1},NPIX(1)} {{P1},START(1)},{{P1},STEP(1)}
CONVERT/TABLE middumex = {EXTAB} #1 #'P5' &r  SPLINE   ! convert to image of same st
IF {{P1},NAXIS} .GT. 1 THEN
   GROW/IMAGE &t = middumex {{P1},START(2)},{{P1},STEP(2)},{{P1},NPIX(2)}
   -RENAME middummt.bdf middumex.bdf
ENDIF
COMP/IMAGE 'P2' = 'P1'*EXP10(0.4*middumex*'SCALE')   
COPY/DD {P1} *,3 {P2}
!
DELETE/IMAGE middumex NO			   ! discard of intermediate file







