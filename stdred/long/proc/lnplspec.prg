! @(#)lnplspec.prg	19.1 (ES0-DMD) 02/25/03 14:24:49
! @(#)lnplspec.prg	19.1  (ESO)  02/25/03  14:24:49
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnplspec.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
define/param  P1  ?  TAB  "Spectral table:"
!
VERIFY/LONG {P1} TAB
GRAPH/SPEC
reset/graph
reset/graph stype=0  ltype=1 pmode=1 font=1
select/table {P1} ALL
plot/table {P1} :LAMBDA :FLUX
reset/graph
return
