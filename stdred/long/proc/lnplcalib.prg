! @(#)lnplcalib.prg	19.1 (ES0-DMD) 02/25/03 14:24:47
! @(#)lnplcalib.prg	19.1  (ESO)  02/25/03  14:24:47
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnplcalib.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE
!.VERSION     1.0  Package Creation  17-MAR-1993
!-------------------------------------------------------
!
define/param P1 NONE CHAR "Mode (NONE/EDIT/CURSOR):"
!
IF P1(1:1) .EQ. "N"  THEN
    @s lnplot 0 4
ELSE IF P1(1:1) .EQ. "E" THEN
    @s lnplot 1 4
ELSE IF P1(1:1) .EQ. "C" THEN
    @s lnplot 2
ELSE
    WRITE/OUT "PLOT/CALIB: Unknown mode {P1}"
ENDIF
