! @(#)lnsave.prg	19.1 (ES0-DMD) 02/25/03 14:24:51
! @(#)lnsave.prg	19.1  (ESO)  02/25/03  14:24:51
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnsave.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Implements command SAVE/LONG
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
DEFINE/PARAM P1  ?  CHAR  "session name : "
!
verify/spec {lintab} ? LINTAB
!
def/loc exlin/i/1/1 0
exlin = m$exist("{LINTAB}")
if exlin .eq. 0 then
    write/out "*** Could not find table {LINTAB}  ***"
    write/out "(May be the extension .tbl is missing)"
    write/out "*** Lines have not been searched.  ***"
    write/out "*** Session cannot be saved        ***"
    return/exit
endif

write/out Session parameters are saved in table 'P1'
define/local indx/i/1/1 0
define/local partab/c/1/60 " " all

compute/keyw indx = m$index(p1, ".tbl")
if indx .eq. 0 then
    write/keyw partab/c/1/60 'p1'
else
    indx = 'indx' - 1
    write/keyw partab/c/1/60 'p1(1:{indx})'
endif


SELECT/TABLE {LINTAB} ALL
COPY/TABLE {LINTAB}  'partab'     

SAVINIT/LONG    'partab'.tbl     WRITE

IF M$EXIST(COERBR) .EQ. 1 THEN
SELECT/TABLE {COERBR} ALL	
COPY/TABLE {COERBR}  'partab'COE
ENDIF

RETURN



