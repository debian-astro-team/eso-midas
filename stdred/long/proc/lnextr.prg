! @(#)lnextr.prg	19.1 (ES0-DMD) 02/25/03 14:24:45
! @(#)lnextr.prg	19.1  (ESO)  02/25/03  14:24:45
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnextr.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!   Execute the command :
!      EXTRACT/LONG out in sky limits order,iter ron,g,thresh 
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
CROSSREF  in  out  sky  obj  
!
DEFINE/PARA P1 ?   I  "input frame:"
DEFINE/PARA P2 ?   I  "output frame:"
DEFINE/PARA P3 0.  ?  "sky [frame or constant]:"
DEFINE/PARA P4 {OBJECT(1)},{OBJECT(2)}   N  "object limits c1,c2 in pixels:"
DEFINE/PARA P5 {ORDER},{NITER}           N  "order, iter:"     
DEFINE/PARA P6 {RON},{GAIN},{SIGMA}      N  "ron, g, thresh:"  
!
DEFINE/MAXPAR  6
!
VERIFY/LONG {P1} IMA
WRITE/KEYW   PUTI/I/1/4  {P4},{P5}
WRITE/KEYW   PUTR/R/1/3  {P6}

DEFINE/LOCAL  ERRORS/I/1/1  0
IF PUTI(1) .LT. 1 .OR. PUTI(1) .GT. {{P1},NPIX(2)}  ERRORS = 1
IF PUTI(2) .LT. 1 .OR. PUTI(2) .GT. {{P1},NPIX(2)}  ERRORS = 1
IF ERRORS .NE. 0 THEN
   SET/FORMAT I1
   WRITE/OUT "Error: Parameter OBJECT={PUTI(1)},{PUTI(2)} out of range"
   WRITE/OUT "       Valid range is 1 to {{P1},NPIX(2)}"
   RETURN/EXIT
ENDIF
SET/LONG    OBJECT={PUTI(1)},{PUTI(2)} ORDER={PUTI(3)} NITER={PUTI(4)}
SET/LONG    RON={PUTR(1)}  GAIN={PUTR(2)}  SIGMA={PUTR(3)}
!
DEFINE/LOCAL SKY/C/1/80 " " ALL
!
! check, if sky is a number or a frame
!
PUTI(1) = M$TSTNO(P3)
IF PUTI(1) .EQ. 1 THEN                 ! sky is a number
  CREA/IMAGE &s = 'P1' POLY 'P3'
  WRITE/KEYW SKY middumms
ELSE                                     ! sky is a frame
  VERIFY/LONG   {P3}  IMA
  WRITE/KEYW SKY 'P3'                    
ENDIF
!
WRITE/OUT "extract useful part"
!
WRITE/KEYW PUTI/I/1/2  'P4'
!
EXTR/IMAGE   &a =  'P1'[<,@'PUTI(1)':>,@'PUTI(2)']
EXTR/IMAGE   &b =  'SKY'[<,@'PUTI(1)':>,@'PUTI(2)']
!
WRITE/OUT "For comparison: frame middummd.bdf contains"
WRITE/OUT "result with equal weights and no mask for cosmics"
!
COMP/IMAGE   &c = &a - &b
AVERA/ROW  &d = &c <,> SUM                        
!
WRITE/OUT "extract weighted mean"
! 
WRITE/KEYW IN_A       middumma
WRITE/KEYW IN_B       middummb
WRITE/KEYW OUT_A        'P2'
WRITE/KEYW PUTI       'P5'
WRITE/KEYW PUTR       'P6'
!
RUN STD_EXE:SPOEXT

COPY/DD  {P1}  *,3  {P2}
!
! purge files
!
-PURGE cosmics.bdf,middumm*.bdf
!

