% @(#)plot_cali.hlq	19.1 (ESO-IPG) 02/25/03 14:24:05
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      plot_cali.hlq
%.AUTHOR     23-MAR-93, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: PLOT/CALIBRATE
%.VERSION    1.0  PB : Creation, 23-MAR-93
%----------------------------------------------------------------
\se
SECTION./CALIBR
\es\co
PLOT/CALIBRATE					   PB  23-MAR-93
\oc\su
PLOT/CALIBRATE  [mode]
	Plot wavelength calibration identifications. 
\us\pu
Purpose:    
            This command allows to plot the arc lines identified
            by CALIBRATE/LONG. The lines can also interactively be 
            rejected from calibration or their position read with
            the graphic cursor.
\up\sy
Syntax:     
            PLOT/CALIBRATE [mode]
\ys\pa
            mode   =  interaction mode. Possible values: NONE, EDIT, CURSOR.
                      Default : NONE
\ap\sa
See also: 
            PLOT/IDENT, PLOT/RESIDUAL, PLOT/DISTORTION, PLOT/DELTA
\as\no
Note:       
            The arc spectrum is plotted in the graphic window. The plotted
            row is controlled by the session keyword YSTART.
\on\exs
Examples:
\ex
            PLOT/CALIBRATE
\xe \sxe

