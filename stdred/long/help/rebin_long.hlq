% @(#)rebin_long.hlq	19.1 (ESO-IPG) 02/25/03 14:24:06
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      rebin_long.hlq
%.AUTHOR     01-APR-93, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: REBIN/LONG
%.VERSION    1.0  PB : Creation, 01-APR-93
%----------------------------------------------------------------
\se
SECTION./LONG
\es\co
REBIN/LONG					   PB   30-OCT-95
\oc\su
REBIN/LONG  in  out   [start,end,step]  [mtd]  [table]
	Rebin a long-slit spectrum using the row-by-row method
\us\pu
Purpose:    
            Non linear rebinning of 2D long-slit spectra using the
            row-by-row method.
\up\sy
Syntax:     
            REBIN/LONG  in  out  [start,end,step]  [mtd]  [table]
\ys\pa
            in             = Input image.
\ap\pa
            out            = Output spectrum.
\ap\pa
            start,end,step = Starting, final and bin wavelength, in 
                             wavelength units. \\
                             Session keywords: REBSTRT, REBEND, REBSTP \\
                             Values initialised by CALIBRATE/LONG.
\ap\pa      
            mtd            = Rebinning method. (LINEAR, QUADRATIC, SPLINE). \\
                             Session keyword: REBMTD; default: LINEAR
\ap\pa      
            table          = coefficients table. \\
                             Session keyword: COERBR; default: coerbr.tbl
\ap\sa
See also: 
            CALIBRATE/LONG, RECTIFY/LONG
\as\no
Note:       
            1) Very strong variations of flux, like in bright emission lines
            or cosmics can yield to negative interpolated values with 
            methods QUADRATIC or SPLINE.

            2) The command CALIBRATE/LONG initializes the values of
            start, end and step wavelength. The step wavelength is set
            by default to the average dispersion per pixel. It must be
            noted that in order to respect the Nyquist criterion, step value
            should be half of the average dispersion per pixel, the rebinned
            spectra becoming about twice as large as the original
            spectrum.

\on\exs
Examples:
\ex
            REBIN/LONG  ccd0023  reb23
\xe \sxe


