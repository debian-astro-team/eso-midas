% @(#)responfilt.hlq	19.1 (ESO-IPG) 02/25/03 14:24:07
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      responfilt.hlq
%.AUTHOR     01-APR-93, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: RESPONSE/FILTER
%.VERSION    1.0  PB : Creation, 01-APR-93
%----------------------------------------------------------------
\se
SECTION./FILTER
\es\co
RESPONSE/FILTER					     01-APR-93  PB
\oc\su
RESPONSE/FILTER  std  [flux]  [resp]
	Generate a response image by filtering based method.
\us\pu
Purpose:    
            Compares an observed standard star spectrum to a reference 
            flux table and smooths the resulting response by filtering
\up\sy
Syntax:     
            RESPONSE/FILTER   std  [flux]  [resp]
\ys\pa
            std    = one-dimensional reduced spectrum of a standard star.
\ap\pa
            flux   = Flux table of the standard star. A collection of
                     flux tables is available in MID_FLUX. \\
                     Session keyword: FLUXTAB; no default value
\ap\pa
            resp   = Output response image.
                     Session keyword: RESPONSE; default: response.bdf
\ap\sa
See also: 
            INTEGRATE/LONG, RESPONSE/LONG, PLOT/FLUX, PLOT/RESPONSE, \\
            CALIBRATE/FLUX, EXTINCTION/LONG
\as\no
Note:       
            1) The command applies in sequence a median and a smooth filter
            to the ratio image. The parameters of the filters are 
            respectively controlled by the session keywords FILTMED and
            FILTSMO.

            2) Broad absorption lines in the spectrum of the standard
            star tend to be propagated to the response curve. In this case
            the commands INTEGRATE/LINE and RESPONSE/LINE should
            preferably be used.

\on\exs
Examples:
\ex
            RESPONSE/FILTER  sp23  l745x4
\xe \sxe
