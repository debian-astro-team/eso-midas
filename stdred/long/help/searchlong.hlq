% @(#)searchlong.hlq	19.1 (ESO-IPG) 02/25/03 14:24:08
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      searchlong.hlq
%.AUTHOR     23-MAR-93, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: SEARCH/LONG
%.VERSION    1.0  PB : Creation, 23-MAR-93
%----------------------------------------------------------------
\se
SECTION./LONG
\es\co
SEARCH/LONG					   PB  20-APR-94
\oc\su
SEARCH/LONG  [in]  [thres]  [width]  [yaver]  [step]  [mtd]  [mode]
	search for spectral features in a long-slit spectrum
\us\pu
Purpose:    
            search for spectral features in a long-slit spectrum
\up\sy
Syntax:     
            SEARCH/LONG [in] [thres] [width] [yaver] [step] [mtd] [mode]
\ys\pa
            in     = Input image 
                     Session keyword: WLC; no default value
\ap\pa
            thres  = Threshold in ADU units. The threshold must be 
                     counted above the local background.
                     Session keyword: THRES; default: 30.
\ap\pa
            width  = Width of the local histogram window (in pixels)
                     Session keyword: WIDTH; default: 8
\ap\pa
            yaver  = Row average parameter. The actual number of rows
                     averaged before detection is 2*yaver+1. 
                     Session keyword: YWIDTH; default: 0
\ap\pa
            step   = Step in pixels along the y-axis. 
                     Session keyword: YSTEP; default: 1
\ap\pa
            mtd    = Centering method. Possible values are GAUSSIAN, 
                     GRAVITY, MAXIMUM.
                     Session keyword: SEAMTD; default: GAUSSIAN
\ap\pa
            mode   = Type of spectral features (EMISSION or ABSORPTION)
                     (This parameter is not controlled by a session keyword)
                     Default: EMISSION
\ap\sa
See also: 
            IDENTIFY/LONG, CALIBRATE/LONG
\as\no
Note:       
            1: The input image is assumed to be rotated in such a way
               that the dispersion direction is oriented along the rows.
               The wavelength must be increasing from the left to the
               right and the STEP descriptors of the image must be positive.

            2: Session keywords START, STEP, NPIX are initialised by this
               command.

            3: Spectral features are detected along sucessive rows 
               separated by <step>. Before detection, 2*yaver+1 rows are 
               averaged for noise reduction.

            4: Algorithm: A line detection occurs if the number of counts
               in a pixel is greater than the local threshold estimated
               as the sum of the local background and <thres>. The local
               background value is estimated as the local median in a window
               of size <width>. Multiple detections are checked and line
               centers are estimated by gaussian, gravity or maximum method.

            5: The output table is used to store the position and peak 
               value of the detected lines as follow:
               X     : center of the line in world coordinates 
                       (wavelength direction)
               Y     : scan row number (in world coordinates)
               PEAK  : peak value of the line (ADU)
\on\exs
Examples:
\ex
            SEARCH/LONG  arc1
\xe\ex
\\{\tt
            SET/LONG     WLC = arc1 \\
            SEARCH/LONG  thres=300
\\}
\xe \sxe





