% @(#)normalflat.hlq	19.1 (ESO-IPG) 02/25/03 14:24:05
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      normalflat.hlq
%.AUTHOR     23-MAR-93, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: NORMALIZE/FLAT
%.VERSION    1.0  PB : Creation, 23-MAR-93
%----------------------------------------------------------------
\se
SECTION./FLAT
\es\co
NORMALIZE/FLAT					   PB  1-AUG-95
\oc\su
NORMALIZE/FLAT  in  out  [bias]  [deg]  [fit]  [visu]
	Normalisation of flat-fields
\us\pu
Purpose:    
            The flat-field is divided by its average shape, obtained by 
            fitting a polynomial along the dispersion direction of an 
            averaged flat-field.
\up\sy
Syntax:     
            NORMALIZE/FLAT in  out [bias] [deg] [fit] [visu]
\ys\pa
            in       = input image, consisting of a flat-field or a 
                       combination of flat-fields (resulting from
                       COMBINE/LONG).
\ap\pa
            out      = Name of the output normalised flat-field.
\ap\pa
            bias     = bias image or constant to be subtracted prior
                       to normalisation. \\
                       Session keyword: BIAS; default: 0
\ap\pa
            deg     =  Order of the polynomial. \\
                       Session keyword: FDEG; default: 2
\ap\pa
            fit     =  fitted 1D spectrum used for normalisation. \\
                       Session keyword: FFIT; default: middummf.bdf 
\ap\pa
            visu    =  Visualisation flag (YES/NO). If visu=YES, the central
                       row of the flat field is plotted as well as the
                       fitted function. \\
                       Session keyword: FVISU; default: YES 
\ap\sa
See also: 
            COMBINE/LONG, CONTINUUM/SPEC
\as\no
Note:       
            1) Session keywords relative to this command are displayed by 
            SHOW/LONG D

            2) Smoothing spline fitting can be performed with the
            command CONTINUUM/SPEC.

\on\exs
Examples:
\ex
            NORMALIZE/FLAT  flat  ffnorm   deg=3  visu=no
\xe \sxe




