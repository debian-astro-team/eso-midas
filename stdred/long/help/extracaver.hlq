% @(#)extracaver.hlq	19.1 (ESO-IPG) 02/25/03 14:24:03
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      extracaver.hlq
%.AUTHOR     23-MAR-93, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: EXTRACT/AVERAGE
%.VERSION    1.0  PB : Creation, 23-MAR-93
%----------------------------------------------------------------
\se
SECTION./AVERAGE
\es\co
EXTRACT/AVERAGE					   PB  23-MAR-93
\oc\su
EXTRACT/AVERAGE  in  out  [obj]  [sky]  [mtd]
	extract a long-slit spectrum by averaging rows
\us\pu
Purpose:    
            Invokes command AVERAGE/ROW to estimate the sky and the object \\
            spectrum within defined limits and subtracts the sky from  \\
            the object.
\up\sy
Syntax:     
            EXTRACT/AVERAGE in out  [obj]  [sky]  [mtd]
\ys\pa
            in   = Input long-slit spectrum
\ap\pa
            out  = Output one-dimensional spectrum
\ap\pa
            obj  = low,upp. Lower and upper limits of the object spectrum, in
                   pixels. \\
                   Session keyword: OBJECT; default: 0,0 \\
\ap\pa
            sky  = low1,upp1,low2,upp2. Lower and upper limits of the sky. \\
                   Session keywords: LOWSKY, UPPSKY; default: 0,0,0,0 
\ap\pa
            mtd  = Extraction method (LINEAR or AVERAGE). \\
                   Session keyword: EXTMTD; default:  LINEAR \\
\ap\sa
See also: 
            AVERAGE/ROW, EXTRACT/LONG, GET/CURSOR
\as\no
Note:       
            It is assumed that : \\
            LOWSKY(1)<=LOWSKY(2) < OBJECT(1)<=OBJECT(2) < UPPSKY(1)<=UPPSKY(2)

            If method LINEAR is used, the averaged flux of the sky 
            subtracted object is multiplied by the width of the object.

            Parameters relative to this command are displayed by \\
            SHO/LONG E

            The command creates temporary images in the following files: \\
                Lower sky      : middumma.bdf \\
                Upper sky      : middummb.bdf \\
                Average sky    : middumms.bdf \\
                Average object : middummo.bdf \\
\on\exs
Examples:
\ex
            EXTRACT/AVERAGE  ccd0001  spec1   190,192  180,188,194,202
\xe \sxe

