% @(#)set_long.hlq	19.1 (ESO-IPG) 02/25/03 14:24:08
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      set_long.hlq
%.AUTHOR     23-MAR-93, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: SET/LONG
%.VERSION    1.0  PB : Creation, 23-MAR-93
%----------------------------------------------------------------
\se
SECTION./LONG
\es\co
SET/LONG					   PB  23-MAR-93
\oc\su
SET/LONG  key=value [...]
	Assigns a value to long-slit session keywords
\us\pu
Purpose:    
            Assigns a value to long-slit session keywords
\up\sy
Syntax:     
            SET/LONG key=value [...]
\ys\pa
            key   = keyword name
\ap\pa
            value = parameter value
\ap\sa
See also: 
            SHOW/LONG, HELP/LONG, SAVE/LONG, INIT/LONG
\as\no
Note:       
            a)  Space characters can be inserted on the right or left
                part of the sign "=". The name of the echelle keyword
                can be truncated to a non ambiguous root. 

            b)  String values can be specified between double quotes.
                The double quote (") is a reserved character and cannot be
                used in a value.

            c)  Short description of long-slit keywords is provided by
                HELP/LONG command, values are displayed by SHOW/LONG.

            d)  It is possible to assign a value to long-slit keywords using
                monitor assignment. In this case, there must be a space
                on both parts of the sign =. The syntax depends on the
                definition of the keyword (provided by HELP/LONG).
                Examples:\\
                   FLAT = "ff456.bdf    "    (keyw. FLAT/C/1/60)\\
                   WIDTH = 12                (keyw. WIDTH/I/1/1)\\
                   OBJECT(2) = 252           (keyw. OBJECT/I/1/2)\\
                To assign values to character keywords, additional spaces
                must be inserted to overwrite a longer previous value.

            e)  To assign the value of an element of a multiple element
                keyword (e.g. OBJECT/I/1/2) it is possible to use
                the direct assignment (OBJECT(2) = 252) or the SET/LONG by
                inserting comas in place of the values that must not be
                changed (SET/LONG OBJ = ,252).

            f)  The total length of the line is limited to 132 characters.
\on\exs
Examples:
\ex
            SET/LONG FLAT =ff456.bdf WIDTH=12 OBJECT = ,252  INSTR = "EFOSC 2"
\xe \sxe
