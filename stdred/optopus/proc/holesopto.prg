! @(#)holesopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:38
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: OPTOP.PRG
!.PURPOSE: Start OPTOP program in MIDAS environment
!.USE: @@ optop [inp_tab] [out_tab] [HH,MM,SS.sss] [+/-DD,AM,AS.sss] [AC_Flag] 
!                 [P_Flag] [Old_eq,New_eq]
!.AUTHOR: Alessandra Gemmo                Padova Department of Astronomy
!.VERSION: 910228 AG Creation
!          910525 AG Modified for OPTOPUS context
!          920721 Modified by Roland den Hartog, Sterrewacht Leiden,
!                 to allow automatic input of the plate-center
!------------------------------------------------------------------------------
DEFINE/PARAM P1 opto1.tbl T "Enter input table [Default=opto1.tbl]: "
DEFINE/PARAM P2 opto2.tbl T "Enter output table [Default=opto2.tbl]: "
DEFINE/PARAM P3 {PLATECEN(1)},{PLATECEN(2)},{PLATECEN(3)} N "RA (hrs,min,sec):"
DEFINE/PARAM P4 {PLATECEN(4)},{PLATECEN(5)},{PLATECEN(6)} N "DEC (deg,amin,asec):"
DEFINE/PARAM P5 {ACFLAG} C "AC_Flag: "
DEFINE/PARAM P6 {PFLAG} C "P_Flag: "
DEFINE/PARAM P7 {EQUINOX(1)},{EQUINOX(2)} N "Old and new equinox for center coordinates: "
DEFINE/LOCAL INPUTF1/C/1/80 "temp.tbl"
DEFINE/LOCAL TABEQUI/D/1/1 0.
DEFINE/LOCAL INDX/I/1/1 0
!
COMPUTE/KEYW INDX = M$INDEX(P1,".tbl")
IF INDX .EQ. 0 THEN
  WRITE/KEYW INPUTFI/C/1/50 'P1'.tbl
ELSE 
  WRITE/KEYW INPUTFI/C/1/50 'P1'
ENDIF
!
WRITE/KEYW OUTPUTF       {P2}
WRITE/KEYW INPUTD/D/1/3  {P3}
WRITE/KEYW INPUTD/D/4/3  {P4}
WRITE/KEYW INPUTC/C/1/80 {P4}
COPY/DK 'INPUTFI' TABEQUI/D/1/1 TABEQUI/D/1/1
!
! do some checks
!
IF {INPUTD(1)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Wrong RA-hours value !!"
   RETURN
ENDIF
IF {INPUTD(1)} .GT. 24. THEN
   WRITE/OUT "*** FATAL: Wrong RA-hours value !!"
   RETURN
ENDIF
!
IF {INPUTD(2)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Wrong RA-min value !!"
   RETURN
ENDIF
IF {INPUTD(2)} .GT. 60. THEN
   WRITE/OUT "*** FATAL: Wrong RA-min value !!"
   RETURN
ENDIF
!
IF {INPUTD(3)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Wrong RA-sec value !!"
   RETURN
ENDIF
IF {INPUTD(3)} .GT. 60. THEN
   WRITE/OUT "*** FATAL: Wrong RA-sec value !!"
   RETURN
ENDIF 
!
IF "{P5}" .EQ. "Y" THEN
   WRITE/KEYW ACFLAG/C/1/1 {P5}
ELSEIF "{P5}" .EQ. "N" THEN
   WRITE/KEYW ACFLAG/C/1/1 {P5}
ELSEIF "{P5}" .EQ. "A" THEN
   WRITE/KEYW ACFLAG/C/1/1 {P5}
ELSE
  WRITE/OUT "*** FATAL: AC flag can only be either Y, N or A!!"
  RETURN
ENDIF
!
IF "{P6}" .EQ. "Y" THEN
   WRITE/KEYW PFLAG/C/1/1 {P6}
ELSEIF "{P6}" .EQ. "N" THEN
   WRITE/KEYW PFLAG/C/1/1 {P6}
ELSE
  WRITE/OUT "*** FATAL: P flag can only be either Y or N !!"
  RETURN
ENDIF
!
WRITE/KEYW INPUTD/D/7/2 {P7}
IF {INPUTD(7)} .LT. 1800. THEN
   WRITE/OUT "*** FATAL: Wrong value of OLD_EQUINOX !!"
   RETURN
ENDIF
IF {INPUTD(7)} .GT. 2100. THEN
   WRITE/OUT "*** FATAL: Wrong value of OLD_EQUINOX !!"
   RETURN
ENDIF
IF {INPUTD(8)} .LT. 1800. THEN
   WRITE/OUT "*** FATAL: Wrong value of NEW_EQUINOX !!"
   RETURN
ENDIF
IF {INPUTD(8)} .GT. 2100. THEN
   WRITE/OUT "*** FATAL: Wrong value of NEW_EQUINOX !!"
   RETURN
ENDIF
WRITE/KEYW EQUINOX/D/1/2 {INPUTD(7)},{INPUTD(8)}
!
! get plate center, if any, from input table
!
IF "{ACFLAG(1:1)}" .EQ. "A" THEN
   SET/OPT CDEC=
   SET/FORMAT F12.9
   SELECT/TABLE {P1} :TYPE.EQ."C".OR.:TYPE.EQ."c"
   STATISTICS/TABLE 'P1' :RA
   WRITE/KEYW PLATECEN/D/1/3 'OUTPUTR(3)',0.,0.
   STATISTICS/TABLE {P1} :DEC
   WRITE/KEYW PLATECEN/D/4/3 'OUTPUTR(3)',0.,0.
   WRITE/KEYW PFLAG/C/1/1 N
ENDIF
!
! select rows that are not deleted and do not contain the plate-center
!
SELECT/TABLE {P1} ALL
SELECT/TABLE {P1} :CHECK.NE."d".AND.:CHECK.NE."D"
SELECT/TABLE {P1} SELECT.AND.:TYPE.NE."c".AND.:TYPE.NE."C"
!
! determination of the center if not the automatic mode was chosen
!
SET/FORMAT F12.9
!
IF "{ACFLAG(1:1)}" .EQ. "Y" THEN
   IF {INPUTD(1)} .EQ. {PLATECEN(1)} THEN
      STATISTICS/TABLE {P1} :RA
      WRITE/KEYW PLATECEN/D/1/3 {OUTPUTR(3)},0.,0.
      IF {TABEQUI(1)} .NE. {EQUINOX(2)} THEN
         WRITE/KEYW PFLAG/C/1/1 N
      ENDIF
   ELSE
      WRITE/KEYW PLATECEN/D/1/3 {INPUTD(1)},{INPUTD(2)},{INPUTD(3)}
   ENDIF

   IF {INPUTD(4)} .EQ. {PLATECEN(4)} THEN
      STATISTICS/TABLE {P1} :DEC
      WRITE/KEYW PLATECEN/D/4/3 {OUTPUTR(3)},0.,0.
      IF {TABEQUI(1)} .NE. {EQUINOX(2)} THEN
         WRITE/KEYW PFLAG/C/1/1 N
      ENDIF
   ELSE
      WRITE/KEYW PLATECHA/C/1/80 {INPUTC(1:80)}
   ENDIF

ELSEIF "{ACFLAG(1:1)}" .EQ. "N" THEN
   IF {INPUTD(1)} .EQ. {PLATECEN(1)} THEN
      WRITE/OUT "I'm using default or previously calculated center RA..."
   ELSE
      WRITE/KEYW PLATECEN/D/1/3 {INPUTD(1)},{INPUTD(2)},{INPUTD(3)}
   ENDIF

   IF {INPUTD(4)} .EQ. {PLATECEN(4)} THEN
      WRITE/OUT "I'm using default or previously calculated center DEC..."
   ELSE
      WRITE/KEYW PLATECHA/C/1/80 {INPUTC(1:80)}
   ENDIF

ELSE
   WRITE/KEYW ACFLAG/C/1/1 Y
ENDIF
!
RUN STD_EXE:OPTOP
!
! disable automatic determination of the center 
!
WRITE/KEYW ACFLAG/C/1/1 N
COPY/TT 'P2' :CHECK 'P1' :CHECK
COPY/TABLE 'P1' 'INPUTF1'
DELETE/TABLE 'P1' NO
RENAME/TABLE 'INPUTF1' 'P1'


