! @(#)holesid.prg	19.1 (ES0-DMD) 02/25/03 14:27:38
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: HOLESID.PRG
!.PURPOSE:        Plot identification numbers close to holes on OPTOPUS plate.
!.USE:            @s holesid [table] [xpos] [ypos] [ident]
!.AUTHOR:         Alessandra Gemmo, Department of Astronomy, Padova
!.VERSION: 910607 AG  Creation
!-------------------------------------------------------------------------------
DEFINE/PARAM P1 ? T "Enter table: "
DEFINE/PARAM P2 ? C "Enter x_column: "
DEFINE/PARAM P3 ? C "Enter y_column: "
DEFINE/PARAM P4 ? C "Enter id_column: "
!
WRITE/KEYW IN_A 'P1'
WRITE/KEYW INPUTC/C/1/16 'P2'
WRITE/KEYW INPUTC/C/17/16 'P3'
WRITE/KEYW INPUTC/C/33/16 'P4'
!
RUN STD_EXE:HOLESID
