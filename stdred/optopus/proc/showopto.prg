! @(#)showopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:40
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: showopto.prg
!.PURPOSE:        Start SHOW_OPTO command in OPTOPUS context
!.USE:            show/optopus
!.AUTHOR:         Alessandra Gemmo, Department of Astronomy, Padova
!.VERSION: 910521 AG Creation
!-------------------------------------------------------------------------------
RUN STD_EXE:SHOWOPTO
