! @(#)drillopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:38
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT  (c) 1991  European Southern Observatory
!.TYPE       Procedure
!.IDENT      drillopto.prg
!.AUTHOR     P.Grosbol,   ESO/IPG
!.PURPOSE    Read MIDAS table with object positions for OPTOPUS 
!            and create ASCII command for drilling machine.
!.USAGE      OPTOPUS/DRILL  in_table [file]
!.VERSION    1.0  1991-Sep-03 : Creation,  PJG
! ------------------------------------------------------------------
CROSSREF TABLE NAME
DEFINE/PARAM P1 ?          T "Enter name of OPTOPUS table :"
DEFINE/PARAM P2 {P1}.dat   C "Enter name of drill-file: "
!
WRITE/KEYW  IN_A/C/1/60  {P1}
WRITE/KEYW  OUT_A/C/1/60 {P2}
!
RUN STD_EXE:OPTPLATE

