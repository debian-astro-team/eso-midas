! @(#)startopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:40
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: startopto.prg
!.PURPOSE:        Create the input table for HOLES/POSITION command in the 
!                 requested format and do the precession of the coordinates.
!.USE:            @s optab inp_file [out_tab] [fmt_file] [Old_equinox]
!.AUTHOR:         Alessandra Gemmo
!.VERSION: 910615 AG Creation
!.VERSION: 910618 AG Modified to include computation of precession
!.VERSION: 910826 AG Modified to separate creation of the table from
!.                   computation of precession. Routine TBLSER also
!.                   included.
!.VERSION: 920722 Roland den Hartog, Sterrewacht Leiden:
!.                combined with precessopto, for convenience
!------------------------------------------------------------------------------
DEFINE/PARAM P1 ? C "Enter input file: "
DEFINE/PARAM P2 opto1.tbl T "Enter output table [Default=opto1.tbl]: "
DEFINE/PARAM P3 copy C "Enter format file [Default=opto.fmt]: "
DEFINE/PARAM P4 {EQUINOX(1)}  N "Enter equinox of coordinates: "
DEFINE/PARAM P5 {EQUINOX(2)}  N "Enter equinox of observing date: "
DEFINE/LOCAL  INDX/I/1/1 0
DEFINE/LOCAL  CIRTAB/C/1/80 "circle.tbl"
DEFINE/LOCAL  TABEQUI/D/1/1 1950
!
COMPUTE/KEYW INDX = M$INDEX(P2,".tbl")
IF INDX .EQ. 0 THEN
  WRITE/KEYW OUTPUTF/C/1/50 {P2}.tbl
ELSE
  WRITE/KEYW OUTPUTF/C/1/50 {P2}
ENDIF
!
WRITE/KEYW INPUTFI {P1}
!
! do some checks
!
IF {P4} .LT. 1800. THEN
   WRITE/OUT "*** FATAL: Value of EQUINOX not allowed !!"
   RETURN
ELSEIF {P4} .GT. 2100. THEN
   WRITE/OUT "*** FATAL: Value of EQUINOX not allowed !!"
   RETURN
ENDIF
!
! copy the format file from the optopus include directory
!
IF P3 .EQ. "copy" THEN
   IF AUX_MODE(1) .LE. 1 THEN              ! VMS
      define/local optodir/c/1/60 -
        "MID_DISK:[&MIDASHOME.&MIDVERS.STDRED.OPTOPUS.INCL]"
      IF M$EXIST("opto.fmt") .EQ. 0 THEN
         $ COPY {optodir}opto.fmt []
         $ SET PROT=(O:RWED) opto.fmt
      ELSE
         WRITE/OUT "*** INFO: Format file opto.fmt already available"
         WRITE/OUT "          Existing opto.fmt format file will be used"
      ENDIF
   ELSE                                    ! UNIX
      define/local optodir/c/1/60 "$MIDASHOME/$MIDVERS/stdred/optopus/incl/"
      IF M$EXIST("opto.fmt") .EQ. 0 THEN
         $ cp {optodir}opto.fmt `pwd`
         $ chmod 644 opto.fmt
      ELSE
         WRITE/OUT "*** INFO: Format file opto.fmt already available"
         WRITE/OUT "          Existing opto.fmt file format will be used"
      ENDIF
   ENDIF
   WRITE/KEYW P3/C/1/8 opto.fmt
ENDIF
!
! create MIDAS table
!
CREATE/TABLE {OUTPUTF} 9 100 {P1} {P3}
!
SET/FORMAT F10.5
IF {P4} .EQ. {EQUINOX(1)} THEN
   WRITE/DESCR {OUTPUTF} TABEQUI/D/1/1 {EQUINOX(1)}
ELSE 
   WRITE/DESCR {OUTPUTF} TABEQUI/D/1/1 {P4}
   WRITE/KEYW   EQUINOX/D/1/1 {P4}
ENDIF
!
RUN STD_EXE:OPTAB
!
DELETE/COLU {P2} :AHR
DELETE/COLU {P2} :AMIN
DELETE/COLU {P2} :ASEC
DELETE/COLU {P2} :SIGN
DELETE/COLU {P2} :DDEG
DELETE/COLU {P2} :DMIN
DELETE/COLU {P2} :DSEC
!
! create table with circle coordinates
!
CREATE/TABLE  {CIRTAB} 2 360 NULL
COMPUTE/TABLE {CIRTAB} :XX = 137000.*COS(SEQ)
COMPUTE/TABLE {CIRTAB} :YY = 137000.*SIN(SEQ)
COMPUTE/TABLE {CIRTAB} :RAX = :XX*0.
COMPUTE/TABLE {CIRTAB} :DECY = :YY*0.
!
!
! *** do the precession
WRITE/KEYW INPUTFI/C/1/50 {OUTPUTF}
!
! *** do some checks
COPY/DK {INPUTFI} TABEQUI/D/1/1 TABEQUI/D/1/1
IF {TABEQUI(1)} .EQ. 'EQUINOX(2)' THEN
   WRITE/OUT "*** FATAL: Table has already been precessed !!"
   RETURN
ENDIF
!
SET/FORMAT F10.5
IF {P5} .EQ. {EQUINOX(2)} THEN
   WRITE/DESCR {INPUTFI} TABEQUI/D/1/1 'EPOCH(1)'
ELSE 
   WRITE/DESCR {INPUTFI} TABEQUI/D/1/1 'P5'
   WRITE/KEYW EQUINOX/D/2/1 'P5'
ENDIF
!
RUN STD_EXE:PRECESS
!
NAME/COLU {INPUTFI} :RA :OLDRA
NAME/COLU {INPUTFI} :DEC :OLDDEC
NAME/COLU {INPUTFI} :PRA :RA
NAME/COLU {INPUTFI} :PDEC :DEC


