! @(#)plotopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:39
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLOCT.PRG
!.PURPOSE: Plot a drawing of the holes to be drilled on a plate to be used
!          at the Cassegrain focus of the 3.6 mt telescope.
!.USE: @@ ploct [table] [label] [EW_flip_flag]
!.AUTHOR: Alessandra Gemmo      Padova Department of Astronomy
!.VERSION: 840921 S. Cristiani  Last update on HP machine
!          910514 A. Gemmo      MIDAS procedure
!          910905 A. Gemmo      Modified to introduce check on columns
!.VERSION: 920721 RHW           Simplifications 
!------------------------------------------------------------------------------
DEFINE/PARAM P1 opto3.tbl T "Enter table [Default=opto3.tbl]: "
DEFINE/PARAM P2 "Optopus Plate" C "Plate label: "
DEFINE/PARAM P3 {EWFLAG} C "EW_flip_flag:"
DEFINE/LOCAL  INDX/I/1/1 0 
DEFINE/LOCAL  CHARKW/C/1/80 " "
DEFINE/LOCAL  INPUTF1/C/1/80 "temp.tbl"
DEFINE/LOCAL  CIRTAB/C/1/80 "circle.tbl"
!
WRITE/KEYW INPUTFI 'P1'
COPY/TABLE 'INPUTFI' 'INPUTF1'
!
! do some checks
!
WRITE/KEYW CHARKW/C/1/80 "OPT_SID_TIME"
COMPUTE/KEYW INDX = M$EXISTD(INPUTF1,CHARKW)
IF INDX .EQ. 0 THEN
 WRITE/OUT "*** WARNING: Input table does not contain refraction information"
 WRITE/OUT "             Did you run REFRACTION/OPTOPUS before?"
!RETURN
ENDIF
!
IF P2 .NE. "Optopus_plate" -
 WRITE/KEYW LABEL/C/1/40 'P2'
!
IF P3 .EQ. "Y" THEN
  WRITE/KEYW EWFLAG/C/1/1 Y
  WRITE/KEYW EWFLAG/C/1/1 'P3'
ELSEIF P3 .EQ. "N" THEN
  WRITE/KEYW EWFLAG/C/1/1 N
  WRITE/KEYW EWFLAG/C/1/1 'P3'
ELSE
  WRITE/OUT "*** FATAL: EW flag can only be either Y or N !!"
  RETURN
ENDIF
!
IF EWFLAG(1:1) .EQ. "Y" COMPUTE/TABLE 'INPUTF1' :X = :X*(-1.)
!
SET/GRAP PMODE=1 FRAME=SQUARE
PLOT/AXES -139000,139000 -139000,139000 ? "X_microns" -
          "Y_microns" 30,30
SELECT/TABLE 'INPUTF1' ALL
!
SET/GRAP PMODE=0 SSIZE=0.5 STYPE=7
SELECT/TABLE 'INPUTF1' :TYPE.EQ."O"
OVERPLOT/TABLE 'INPUTF1' :X :Y
SELECT/TABLE 'INPUTF1' ALL
!
SET/GRAP STYPE=2 SSIZE=1
SELECT/TABLE 'INPUTF1' :TYPE.EQ."S"
OVERPLOT/TABLE 'INPUTF1' :X :Y
SELECT/TABLE 'INPUTF1' ALL
!
SET/GRAP SSIZE=2 STYPE=11
SELECT/TABLE 'INPUTF1' :TYPE.EQ."B"
OVERPLOT/TABLE 'INPUTF1' :X :Y
SELECT/TABLE 'INPUTF1' ALL
!
! draw limits of forbidden area
!
set/graph colour=2
OVERPLOT/LINE 1 -20010.,137000. -20010.,118100.
OVERPLOT/LINE 1 -20010.,118100. +20010.,118100.
OVERPLOT/LINE 1 +20010.,118100. +20010.,137000.
!
! put label
!
SET/GRAPH COLOUR=1
LABEL/GRAPHIC "'LABEL(1:40)'" 0.,147500. 0 1 0
!
! put explanation of symbols
!
OVERPLOT/SYMBOL 7 150000.,130000. 0.75
OVERPLOT/SYMBOL 2 150000.,110000. 0.75
OVERPLOT/SYMBOL 11 150000.,90000. 0.75
!
LABEL/GRAPHIC "objects" 160000.,130000. 0 1 1
LABEL/GRAPHIC "small guidestars" 160000.,110000. 0 1 1
LABEL/GRAPHIC "big guidestars" 160000.,90000. 0 1 1
IF EWFLAG(1:1) .EQ. "Y" THEN
   LABEL/GRAPHIC "** EW FLAG = Y **" 150000.,50000. 0 1 1
ELSE
   LABEL/GRAPHIC "** EW FLAG = N **" 150000.,50000. 0 1 1
ENDIF
!
! draw circle to show plate limits
!
SET/GRAP SSIZE=1 STYPE=1
OVERPLOT/TABLE 'CIRTAB' :XX :YY
!
SET/GRAPH COLOUR=4
@s holesid 'INPUTF1' :X :Y :NUMBER
DELETE/TABLE 'INPUTF1' NO
SET/GRAPH

