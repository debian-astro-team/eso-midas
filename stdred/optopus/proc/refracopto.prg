! @(#)refracopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:40
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: refracopto.prg
!.PURPOSE:        Start ATREF program in MIDAS environment
!.USE:            @s atref [inp_tab] [out_tab] [year,month,day] [exp] 
!                 [lambda1,lambda2] [start_st_slot,end_st_slot] [opt_st] 
!                 [AST_flag]
!.AUTHOR:         Alessandra Gemmo, Department of Astronomy, Padova
!.VERSION: 910410 AG Creation
!          910603 AG Modified for Optopus context
!          920714 Debugged and extended to capabilities of old ATREF,
!                 by Roland den Hartog, Sterrewacht Leiden
!------------------------------------------------------------------------------
DEFINE/PARAM P1 opto2.tbl TBL "Enter input table [Default=opto2.tbl]: "
DEFINE/PARAM P2 opto3.tbl TBL "Enter output table [Default=opto3.tbl]: "
DEFINE/PARAM P3 {DATE(1)},{DATE(2)},{DATE(3)} -
               N "Year, month and day of observation: "
DEFINE/PARAM P4 {EXPTIME} N "Exposure time: "
DEFINE/PARAM P5 {LAMBDA(1)},{LAMBDA(2)}  N "Spectral range: "  
DEFINE/PARAM P6 {STSLOT(1)},{STSLOT(2)} N "Observational slot in ST: "
DEFINE/PARAM P7 {SIDTIME} N "ST of your choice: "
DEFINE/PARAM P8 {ASTFLAG} C "AST_Flag: "
!
WRITE/KEYW INPUTFI 'P1'
WRITE/KEYW OUTPUTF 'P2'
!
DEFINE/LOCAL INPUTF1/C/1/80 "temp.tbl"
!
! do some checks
!
WRITE/KEYW INPUTD/D/1/3 {P3}
IF {INPUTD(1)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Year can't be negative!!"
   RETURN
ENDIF
IF {INPUTD(2)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Wrong MONTH value!!"
   RETURN
ENDIF
IF {INPUTD(2)} .GT. 12. THEN
   WRITE/OUT "*** FATAL: Wrong MONTH value!!"
   RETURN
ENDIF
IF {INPUTD(3)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Wrong DAY value!!"
   RETURN
ENDIF
IF {INPUTD(3)} .GT. 31. THEN
   WRITE/OUT "*** FATAL: Wrong DAY value!!"
   RETURN
ENDIF
WRITE/KEYW DATE/D/1/3 {P3}
!
WRITE/KEYW INPUTD/D/1/1 {P4}
IF {INPUTD(1)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Exposure time can't be negative!!"
   RETURN
ENDIF
WRITE/KEYW EXPTIME/D/1/1 {P4}
!
WRITE/KEYW INPUTD/D/1/2 {P5}
IF {INPUTD(1)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Wavelength can't be negative!!"
   RETURN
ENDIF
IF {INPUTD(2)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Wavelength can't be negative!!"
   RETURN
ENDIF
WRITE/KEYW LAMBDA/D/1/2 {P5}
!
! The limits on the time-slot were relaxed, because around
! September 21th it is likely that you make an observation that
! begins before 24:00 ST and ends after 24:00 ST.
! This was also possible in the version of ATREF running on the
! HP 1000
!
WRITE/KEYW INPUTD/D/1/2 {P6}
IF {INPUTD(1)} .LT. -24. THEN
   WRITE/OUT "*** FATAL: Wrong start_st_slot value!!"
   RETURN
ENDIF
IF {INPUTD(1)} .GT. 48. THEN
   WRITE/OUT "*** FATAL: Wrong start_st_slot value!!"
   RETURN
ENDIF
IF {INPUTD(2)} .LT. -24. THEN
   WRITE/OUT "*** FATAL: Wrong end_st_slot value!!"
   RETURN
ENDIF
IF {INPUTD(2)} .GT. 48. THEN
   WRITE/OUT "*** FATAL: Wrong end_st_slot value!!"
   RETURN
ENDIF
WRITE/KEYW STSLOT/D/1/2 {P6}
!
WRITE/KEYW INPUTD/D/1/1 {P7}
IF {INPUTD(1)} .LT. 0. THEN
   WRITE/OUT "*** FATAL: Wrong opt_st value!!"
   RETURN
ELSEIF {INPUTD(1)} .GT. 24. THEN
   WRITE/OUT "*** FATAL: Wrong opt_st value!!"
   RETURN
ENDIF
WRITE/KEYW SIDTIME/D/1/1 {P7}
!
IF "{P8}" .EQ. "Y" THEN
   WRITE/KEYW ASTFLAG/C/1/1 {P8}
ELSEIF P8 .EQ. "N" THEN
   WRITE/KEYW ASTFLAG/C/1/1 {P8}
ELSE
  WRITE/OUT "*** FATAL: AST flag can only be either Y or N !!"
  RETURN
ENDIF
!
! consider only 'good' rows
!
SELECT/TABLE 'P1' :CHECK.NE."d".AND.:CHECK.NE."D"
SELECT/TABLE 'P1' SELECT.AND.:TYPE.NE."c".AND.:TYPE.NE."C
COPY/TABLE 'P1' 'INPUTF1'
DELETE/TABLE 'P1' NO
RENAME/TABLE 'INPUTF1' 'P1'
! 
RUN STD_EXE:atref
