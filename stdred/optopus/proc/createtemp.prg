! @(#)createtemp.prg	19.1 (ES0-DMD) 02/25/03 14:27:37
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: createtemp.PRG
!.PURPOSE:        Create input table to TEMPL program, still running on the HP.
!.USE:            @s temptab [inp_tab] [out_file] [fmt_file]
!.AUTHOR:         A. Gemmo, Padova Department of Astronomy
!.VERSION: 910723 AG Creation
!------------------------------------------------------------------------------
DEFINE/PARAM P1 opto3.tbl T "Enter input table [Default=opto3.tbl]: "
DEFINE/PARAM P2 ? C "Enter output file: "
DEFINE/PARAM P3 templ.fmt C "Enter format file [Default=templ.fmt]: "
!
WRITE/KEYW INPUTFI/C/1/50 'P1'
WRITE/KEYW OUTPUTF/C/1/50 'P2'
WRITE/KEYW INPUTC/C/1/80 'P3'
!
COPY/TABLE 'P1' templ.tbl
!
! re-arranging table columns before printing
!
CREATE/COLUMN templ.tbl :X1 " " I8 I
CREATE/COLUMN templ.tbl :Y1 " " I8 I
CREATE/COLUMN templ.tbl :DUMMY " " I8 I
CREATE/COLUMN templ.tbl :IDENT1 " " A8 C
CREATE/COLUMN templ.tbl :Z1 " " I8 I
CREATE/COLUMN templ.tbl :NUMBER1 " " I8 I
!
COMPUTE/TABLE templ.tbl :X1 = :X*1
COMPUTE/TABLE templ.tbl :Y1 = :Y*1
COMPUTE/TABLE templ.tbl :DUMMY = SEQ*0
COPY/TT templ.tbl :IDENT templ.tbl :IDENT1
COMPUTE/TABLE templ.tbl :Z1 = :Z*1
COMPUTE/TABLE templ.tbl :NUMBER1 = :NUMBER*1
!
! deleting 'old' columns
!
DELETE/COLUMN templ.tbl :IDENT
DELETE/COLUMN templ.tbl :TYPE
DELETE/COLUMN templ.tbl :X
DELETE/COLUMN templ.tbl :Y
DELETE/COLUMN templ.tbl :Z
DELETE/COLUMN templ.tbl :NUMBER
!
! printing ascii file
!
ASSIGN/PRINT FILE 'P2'
PRINT/TABLE templ.tbl 'P3'
DELETE/TABLE templ.tbl NO
