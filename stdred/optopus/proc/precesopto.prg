! @(#)precesopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:39
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: precesopto.prg
!.PURPOSE:        Precess coordinates in table created by CREATE/OPTABLE 
!                 command.
!.USE:            @s precess [inp_tab] [equinox1]
!.AUTHOR:         Alessandra Gemmo, department of Astronomy, Padova
!.VERSION: 910826 AG Creation
!------------------------------------------------------------------------------
DEFINE/PARAM P1 opto1.tbl T "Enter input table [Default=opto1.tbl]: "
DEFINE/PARAM P2 {EQUINOX(2)} N "Enter new equinox of coordinates: "
DEFINE/LOCAL  INDX/I/1/1 0
DEFINE/LOCAL  TABEQUI/D/1/1 1950.
!
COMPUTE/KEYW INDX = M$INDEX(P1,".tbl")
IF INDX .EQ. 0 THEN
  WRITE/KEYW INPUTFI/C/1/50 'P1'.tbl
ELSE
  WRITE/KEYW INPUTFI/C/1/50 'P1'
ENDIF
!
! do some checks
!
COPY/DK 'INPUTFI' TABEQUI/D/1/1 TABEQUI/D/1/1
IF 'TABEQUI(1)' .EQ. 'EQUINOX(2)' THEN
   WRITE/OUT "*** FATAL: Table has already been precessed !!"
   RETURN
ENDIF
!
IF 'P2' .LT. 1800. THEN
   WRITE/OUT "*** FATAL: Value of EQUINOX not allowed !!"
   RETURN
ELSEIF 'P2' .GT. 2100. THEN
   WRITE/OUT "*** FATAL: Value of EQUINOX not allowed !!"
   RETURN
ENDIF
!
SET/FORMAT F10.5
IF 'P2' .EQ. {EQUINOX(2)} THEN
   WRITE/DESCR 'INPUTFI' TABEQUI/D/1/1 'EPOCH(1)'
ELSE 
   WRITE/DESCR 'INPUTFI' TABEQUI/D/1/1 'P2'
   WRITE/KEYW EQUINOX/D/2/1 'P2'
ENDIF
!
RUN STD_EXE:PRECESS
!
NAME/COLU 'INPUTFI' :RA :OLDRA
NAME/COLU 'INPUTFI' :DEC :OLDDEC
NAME/COLU 'INPUTFI' :PRA :RA
NAME/COLU 'INPUTFI' :PDEC :DEC
