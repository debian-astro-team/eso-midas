! @(#)createopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:37
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: createopto.prg
!.PURPOSE:        Create the input table for HOLES/POSITION command in the 
!                 requested format.
!.USE:            @s optab inp_file [out_tab] [fmt_file] [Old_equinox]
!.AUTHOR:         Alessandra Gemmo
!.VERSION: 910615 AG Creation
!          910618 AG Modified to include computation of precession
!          910826 AG Modified to separate creation of the table from
!                    computation of precession. Routine TBLSER also
!                    included.
!------------------------------------------------------------------------------
DEFINE/PARAM P1 ? C "Enter input file: "
DEFINE/PARAM P2 opto1.tbl T "Enter output table [Default=opto1.tbl]: "
DEFINE/PARAM P3 copy C "Enter format file [Default=opto.fmt]: "
DEFINE/PARAM P4 {EQUINOX(1)}  N "Enter equinox of coordinates: "
DEFINE/LOCAL  INDX/I/1/1 0
DEFINE/LOCAL  CIRTAB/C/1/80 "circle.tbl"
!
COMPUTE/KEYW INDX = M$INDEX(P2,".tbl")
IF INDX .EQ. 0 THEN
  WRITE/KEYW OUTPUTF/C/1/50 {P2}.tbl
ELSE
  WRITE/KEYW OUTPUTF/C/1/50 {P2}
ENDIF
!
WRITE/KEYW INPUTFI {P1}
!
! do some checks
!
IF {P4} .LT. 1800. THEN
   WRITE/OUT "*** FATAL: Value of EQUINOX not allowed !!"
   RETURN
ELSEIF {P4} .GT. 2100. THEN
   WRITE/OUT "*** FATAL: Value of EQUINOX not allowed !!"
   RETURN
ENDIF
!
! copy the format file from the optopus include directory
!
IF P3 .EQ. "copy" THEN
   IF AUX_MODE(1) .LE. 1 THEN              ! VMS
      define/local optodir/c/1/60 -
        "MID_DISK:[&MIDASHOME.&MIDVERS.STDRED.OPTOPUS.INCL]"
      IF M$EXIST("opto.fmt") .EQ. 0 THEN
         $ COPY 'optodir'opto.fmt []
         $ SET PROT=(O:RWED) opto.fmt
      ELSE
         WRITE/OUT "*** INFO: Format file opto.fmt already available"
         WRITE/OUT "          Existing opto.fmt format file will be used"
      ENDIF
   ELSE                                    ! UNIX
      define/local optodir/c/1/60 "$MIDASHOME/$MIDVERS/stdred/optopus/incl/"
      IF M$EXIST("opto.fmt") .EQ. 0 THEN
         $ cp 'optodir'opto.fmt `pwd`
         $ chmod 644 opto.fmt
      ELSE
         WRITE/OUT "*** INFO: Format file opto.fmt already available"
         WRITE/OUT "          Existing opto.fmt file format will be used"
      ENDIF
   ENDIF
   WRITE/KEYW P3/C/1/8 opto.fmt
ENDIF
!
! create MIDAS table
!
CREATE/TABLE {OUTPUTF} 9 100 {P1} {P3}
!
SET/FORMAT F10.5
IF {P4} .EQ. {EQUINOX(1)} THEN
   WRITE/DESCR {OUTPUTF} TABEQUI/D/1/1 {EQUINOX(1)}
ELSE 
   WRITE/DESCR {OUTPUTF} TABEQUI/D/1/1 {P4}
   WRITE/KEYW EQUINOX/D/1/1 {P4}
ENDIF
!
RUN STD_EXE:OPTAB
!
DELETE/COLU {P2} :AHR
DELETE/COLU {P2} :AMIN
DELETE/COLU {P2} :ASEC
DELETE/COLU {P2} :SIGN
DELETE/COLU {P2} :DDEG
DELETE/COLU {P2} :DMIN
DELETE/COLU {P2} :DSEC
!
! create table with circle coordinates
!
CREATE/TABLE  {CIRTAB} 2 360 NULL
COMPUTE/TABLE {CIRTAB} :XX = 137000.*COS(SEQ)
COMPUTE/TABLE {CIRTAB} :YY = 137000.*SIN(SEQ)
COMPUTE/TABLE {CIRTAB} :RAX = :XX*0.
COMPUTE/TABLE {CIRTAB} :DECY = :YY*0.
