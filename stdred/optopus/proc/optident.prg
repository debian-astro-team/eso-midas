! @(#)optident.prg	19.1 (ES0-DMD) 02/25/03 14:27:39
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! Purpose: MIDAS procedure  optident.prg
! Use:     IDENTIFY/CURSOR  table :check :ident :x [:y] [error]
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
DEFINE/PARAM P1 ? TABLE "Enter input table:"
RUN STD_EXE:OPTIDENT
