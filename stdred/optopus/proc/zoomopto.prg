! @(#)zoomopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:41
!W% (ESO-IPG) 02/25/03 14:27:41
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: zoomopto.prg
!.PURPOSE:        Blow up the area of the plate where objects overlap for an 
!                 easier identification.
!.USE:            @s zoomopto [table] [zoom_fact]
!.AUTHOR : Alessandra Gemmo, Department of Astronomy, Padova
!.VERSION: 910531 A. Gemmo Creation
!          910906 A. Gemmo Modified to avoid bug in GET/GCURSOR
!          920720 Roland den Hartog, Sterrewacht Leiden,
!                 Modifications on advise of Rein Warmels, ESO-IPG
!------------------------------------------------------------------------------
DEFINE/PARAM P1 opto1.tbl T "Enter table [Default=opto1.tbl]: "
DEFINE/PARAM P2 5 N "Enter zooming factor: "
!
DEFINE/LOCAL IND/I/1/1 0
DEFINE/LOCAL IND1/I/1/1 0
DEFINE/LOCAL CHARKW/C/1/80 " "
DEFINE/LOCAL INPUTF1/C/1/80 "short.tbl"
DEFINE/LOCAL INPUTF2/C/1/80 "temp.tbl"
DEFINE/LOCAL CIRTAB/C/1/80 "circle.tbl"
WRITE/KEYW    COORD/R/1/10 0. ALL
WRITE/KEYW    PLIM/R/1/4 0.,0.,0.,0.
WRITE/KEYW    PSCALE/R/1/1 0.
!
COMPUTE/KEYW IND = M$INDEX(P1,".tbl")
IF IND .EQ. 0 THEN
 WRITE/KEYW INPUTFI 'P1'.tbl
ELSE 
 WRITE/KEYW INPUTFI 'P1'
ENDIF
!
WRITE/KEYW CHARKW/C/1/7 "TABEQUI"
COMPUTE/KEYW IND1 = M$EXISTD(INPUTFI,CHARKW)
IF IND1 .EQ. 0 THEN
 WRITE/OUT "*** FATAL: Wrong input table! You must use the one with RA & DEC!"
 RETURN
ENDIF
!
IF 'CURSFLAG(1)' .EQ. 1 GOTO NEXT
!
COPY/TABLE 'INPUTFI' 'INPUTF1'
WRITE/DESCR 'INPUTF1' COORD/R/1/10 0. ALL
!
! get limits of the area to be plotted
!
WRITE/KEYW PLCDATA/C/1/60 {INPUTF1}
WRITE/KEYW PLCDATA/C/61/20 TABLE
!
GET/GCURSOR COORD,DESCR 
RENAME/TABLE 'INPUTF1' 'INPUTFI'
WRITE/KEYW CURSFLAG/I/1/1 1
!
NEXT:
COPY/DK 'INPUTFI' COORD/R/1/10 COORD
!
COMPUTE/KEYW PSCALE(1) = ('PIPPO(3)'+'P10(1)')/'P2'
COMPUTE/KEYW PLIM(1) = 'COORD(1)' + ('PSCALE(1)'/15.)
COMPUTE/KEYW PLIM(2) = 'COORD(1)' - ('PSCALE(1)'/15.)
COMPUTE/KEYW PLIM(3) = 'COORD(2)' - 'PSCALE(1)'
COMPUTE/KEYW PLIM(4) = 'COORD(2)' + 'PSCALE(1)'
!
! plot the enlarged table
!
SET/GRAP PMODE=1 FRAME=SQUARE
SET/GRAP XAXIS='PLIM(1)','PLIM(2)',0.0166666
SET/GRAP YAXIS='PLIM(3)','PLIM(4)',0.166666
!
COMPUTE/TABLE 'INPUTFI' :RA = :RA-INT((:RA-{PLATEC1(1)})/24.0)*24.0
SELECT/TABLE 'INPUTFI' :CHECK.NE."d".AND.:CHECK.NE."D"
SELECT/TABLE 'INPUTFI' SELECT.AND.:TYPE.NE."C".AND.:TYPE.NE."c"
COPY/TABLE 'INPUTFI' 'INPUTF2'
DELETE/TABLE 'INPUTFI' NO
RENAME/TABLE 'INPUTF2' 'INPUTFI'
!
SET/GRAP SSIZE=1 TSIZE=1 STYPE=7
SELECT/TABLE 'INPUTFI' :TYPE.EQ."O".OR.:TYPE.EQ."o"
PLOT/TABLE 'INPUTFI' :RA :DEC
SELECT/TABLE 'INPUTFI' ALL
!
SET/GRAP PMODE=0 STYPE=2
SELECT/TABLE 'INPUTFI' :TYPE.EQ."S".OR.:TYPE.EQ."s"
OVERPLOT/TABLE 'INPUTFI' :RA :DEC
SELECT/TABLE 'INPUTFI' ALL
!
SET/GRAP SSIZE=2 STYPE=11
SELECT/TABLE 'INPUTFI' :TYPE.EQ."B".OR.:TYPE.EQ."b"
OVERPLOT/TABLE 'INPUTFI' :RA :DEC
SELECT/TABLE 'INPUTFI' ALL
!
SET/GRAP STYPE=3
SELECT/TABLE 'INPUTFI' :CHECK.EQ."N".OR.:CHECK.EQ."n"
OVERPLOT/TABLE 'INPUTFI' :RA :DEC
SELECT/TABLE 'INPUTFI' ALL
!
! draw limits of forbidden area
!
SET/GRAPH COLOUR=2
OVERPLOT/LINE 1 'XF(1)','YF(1)' 'XF(2)','YF(2)'
OVERPLOT/LINE 1 'XF(2)','YF(2)' 'XF(3)','YF(3)'
OVERPLOT/LINE 1 'XF(3)','YF(3)' 'XF(4)','YF(4)'
!
! draw circle
!
SET/GRAP SSIZE=1 STYPE=1
OVERPLOT/TABLE 'CIRTAB' :RAX :DECY
!
SET/GRAPH COLOUR=4
DEFINE/LOCAL RASEL/C/1/60   ":RA.LE.{PLIM(1)}.AND.:RA.GE.{PLIM(2)}"
DEFINE/LOCAL DECSEL/C/1/60  ":DEC.GE.{PLIM(3)}.AND.:DEC.LE.{PLIM(4)}"
SELECT/TABLE 'INPUTFI' {RASEL}
SELECT/TABLE 'INPUTFI' SELECT.AND.{DECSEL}
@s holesid 'INPUTFI' :RA :DEC :IDENT
SET/GRAPH COLOUR=6
@s optident 'INPUTFI' :CHECK :IDENT :RA :DEC
SELECT/TABLE 'INPUTFI' :RA.LT.0.0
COMPUTE/TABLE 'INPUTFI' :RA = :RA+SELECT*24.0
SELECT/TABLE 'INPUTFI' :RA.GE.0.0
COMPUTE/TABLE 'INPUTFI' :RA = :RA-SELECT*24.0
SET/GRA








