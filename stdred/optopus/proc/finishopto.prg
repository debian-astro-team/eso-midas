! @(#)finishopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:38
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT  (c) 1991  European Southern Observatory
!.TYPE       Procedure
!.IDENT      finishopto.prg
!.AUTHOR     Roland den Hartog, Sterrewacht Leiden
!.PURPOSE    Read MIDAS table with object positions for OPTOPUS 
!            and create ASCII command for drilling machine,
!            make necessary prints and plots
!.USAGE      FINISH/OPTOPUS  in_table [file] [plot_label] [flip]
!.VERSION    1.0  920722 RH  Creation
!.VERSION    1.1  920810 RHW UNIX and VMS 
! ------------------------------------------------------------------
CROSSREF TABLE NAME
DEFINE/PARAM P1 opto3.tbl T "Enter name of OPTOPUS table [opto3.tbl]: "
DEFINE/PARAM P2 opto      C "Enter name of output files (without extension) : "
DEFINE/PARAM P3 Optopus   C "Enter label of plots : "
DEFINE/PARAM P4 {EWFLAG}  C "EW flag: "
!
DEFINE/LOCAL INDX/I/1/1 0
DEFINE/LOCAL OUTPUTFI/C/1/80 " "
!
WRITE/KEYW  IN_A/C/1/60  'P1'
WRITE/KEYW  OUT_A/C/1/60 'P2'.dat
WRITE/KEYW  PRINT/C/1/20  " " ALL
PRINT = M$SYMBOL("LASER")
!
! *** here for the printout of the Optopus table
WRITE/KEYW OUTPUTFI 'P2'.lis
ASSIGN/PRINT FILE 'OUTPUTFI'
PRINT/TABLE {P1}
IF AUX_MODE(1) .LT. 2 THEN
   $PRINT/QUEUE={PRINT} {OUTPUTFI}
ELSE
!   $lpr -h -P{PRINT} {OUTPUTFI}
    $ {syscoms(1:16)}{PRINT} {OUTPUTFI}
ENDIF
!
! *** here for the descriptor listing of the Optopus table
WRITE/KEYW OUTPUTFI 'P2'.dat
ASSIGN/PRINT FILE 'OUTPUTFI'
PRINT/DESC {P1}.tbl
IF AUX_MODE(1) .LT. 2 THEN
   $PRINT/QUEUE={PRINT} {OUTPUTFI}
ELSE
!   $lpr -h -P{PRINT} {OUTPUTFI}
    $ {syscoms(1:16)}{PRINT} {OUTPUTFI}
ENDIF
!
! *** here for the printout of the drill 
RUN STD_EXE:OPTPLATE
!IF AUX_MODE(1) .LT. 2 THEN
!   $PRINT/QUEUE={PRINT} {OUT_A}
!ELSE
!   $lpr -h -P{PRINT} {OUT_A}
!ENDIF
!
! *** here for the drill plot 
ASSIGN/PLOT {PRINT} NOSPOOL
PLOT/OPTO {P1} {P3} {P4}
WRITE/KEYW OUTPUTFI {P2}.plt
IF AUX_MODE(1) .LT. 2 THEN
   $RENAME axes.plt {OUTPUTFI}
ELSE
   $mv axes.plt  {OUTPUTFI}
ENDIF
COPY/GRAPH {PRINT} {OUTPUTFI}
!
WRITE/OUT "Please pick up your results at the printer {PRINT}"
WRITE/OUT "Your will find - the Optopus table with XY positions;"
WRITE/OUT "               - the descriptor listing of the table;"
WRITE/OUT "               - and a plot of the XY positions."
WRITE/OUT "Your Optopus drill command file is: {OUT_A}"
!
SAVE/OPTO {P1}                                ! save the session
ASSIGN/GRAPH GRAPH

