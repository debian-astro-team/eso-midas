! @(#)tutoopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:40
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: TUTOOPTO.PRG
!.PURPOSE:        Demo. procedure for the OPTOPUS package
!.USE:            Execute as TUTORIAL/OPTOPUS
!                 The following files are copied into your directory :
!                 optopus.dat optopus.fmt: test data and format file
! --------------------------------------------------------------------
!
write/out "      ***** Tutorial for the OPTOPUS package *****"
write/out " "
write/out "This tutorial creates the following files:"
write/out "  - One test table and format file: optopus.dat and optopus.fmt"
write/out "  - One output table containing the hole positions on the OPTOPUS"
write/out "    late: prova1.tbl"
write/out "  - Two output tables containing the instructions for the milling"
write/out "    machine to drill the holes (one in ASCII, one in MIDAS table "
write/out "    format"
write/out "First we copy the ascci table and the format file."
!
-COPY MID_TEST:optopus.dat optopus.dat
-COPY MID_TEST:optopus.fmt optopus.fmt
create/graphic 
!
define/local flag/i/1/1 0
echo/on
set/opt label=f034 date=1992,09,20 extim=60.
set/opt sitslt=22.0,0.0 ost=23.0
set/opt cra=23,57,27.737 cdec=-40,00,00.0
show/opt
echo/off
!
echo/on
write/out "*** First, create the object table and calculate the"
write/out "     precessed positions."
create/opt optopus prova optopus.fmt     ! *** create the tables
precess/opt prova                        ! *** do the precession
holes/opt prova prova1 ? ? N             ! *** calculate the x and y positions 
echo/off
!
indietro:
write/out "*** Now, plot the positions of the objects"
echo/on
modi/opt prova
echo/off
write/out "            "
inquire/keyw flag " Enter 1 to zoom the plot, enter 0 to continue:"
if flag .EQ. 0 goto avanti
write/out " To executing the zooming: when the cursor is on, click the "
write/out " leftmost button of the mouse on the position to be zoomed; "
write/out " thereafter click the rightmost button"
echo/on
zoom/opt prova
echo/off
write/out "            "
inquire/keyw flag " For re-display enter 1, to continue enter 0:"
if flag .EQ. 1 goto indietro
!
avanti:
write/out " "
write/out "*** Now, redraw the positions of the holes"
echo/on
holes/opt prova prova1 ? ? N                   
refrac/opt prova1 prova2
plot/opt prova2                          ! plot the hole positions
copy/graph postscript                    ! copy the graph to potscript
$mv postscript.ps prova2.ps
echo/off
!
write/out " "
write/out "*** Now, we make the instruction file for the milling machine"
echo/on
drill/opt prova2 f034
save/opt prova2
assign/print file f034tab.txt
$mv prova2.tbl f034.tbl
print/table f034.tbl
assign/print file f034des.txt
print/desc f034.tbl *
echo/off
write/out " ***                        ALL DONE                          ****"
write/out "     The plot is stored in the postscript file prova2.ps.         "
write/out "     Table content and and table descriptor info can be found in  "
write/out "     in the files f034tabl.txt and f034des.txt.                   "
write/out " ***                     HAVE A GOOD RUN                      ****"
! $mv f034.tbl prova2.tbl
! rm prova*.tbl











