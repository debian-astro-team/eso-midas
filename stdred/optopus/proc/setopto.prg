! @(#)setopto.prg	19.1 (ES0-DMD) 02/25/03 14:27:40
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: setopto.prg
!.PURPOSE:        Enable user to modify parameters initialized in OPTOPUS 
!                 context
!.USE:            @s set_opto PAR1=value1 [PAR2=value2] [PAR3=value3] ...
!.AUTHOR:         Alessandra Gemmo, Department of Astronomy, Padova
!.VERSION: 910522 AG Creation
!-------------------------------------------------------------------------------
DEFINE/PARAM P1 DEFAULT
!
IF P1(1:3) .EQ. "DEF" THEN
     WRITE/KEYW INPUTFI None
     WRITE/KEYW OUTPUTF None
     SET/OPTO CRA= CDEC= OLDEQ= NEWEQ= LABEL= DATE= EXTIM= PFLAG=
     SET/OPTO WRANGE= SITSLT= OST= ACFLAG= ASTFLAG= EWFLAG=
ELSE
   RUN STD_EXE:SETOPTO
ENDIF
