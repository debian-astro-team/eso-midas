/*===========================================================================
  Copyright (C) 1991-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE   C
.IDENT      optplate
.AUTHOR     Preben Grosbol  (ESO-IPG)
.KEYWORDS   OPTOPUS, Star plates, drill
.ENVIRON    ESO-MIDAS
.COMMENTS   Uses 'osa' for writing ASCII command file
.PURPOSE    Convert a MIDAS table of OPTOPUS object positions
            into a ASCII command file for drilling the star
            plate.
.VERSION    1.0  1991-Sep-03 :  Creation,     PJG
.VERSION    1.1  1991-Nov-29 :  Exchange '.' with ',' in numbers,     PJG
.VERSION    1.2  1994-Feb-11 :  Update to new SC-routines,     PJG

 090903		last modif
------------------------------------------------------------------------*/

#include   <osparms.h>               /* MIDAS OS definitions            */
#include   <midas_def.h>             /* MIDAS definitions               */
#include   <optdef.h>                /* Definition of templete commands */

#include <stdio.h>




#define    MXNAME              60    /* Max. char. in file names        */
#define    MXOBJ              512    /* Max. objects in table file      */
#define    DFEXT           ".dat"    /* Default extension of ASCII file */

#define    X_LABEL            "X"
#define    Y_LABEL            "Y"
#define    Z_LABEL            "Z"
#define    T_LABEL         "TYPE"

int main()
{
  char     dname[MXNAME], tname[MXNAME], line[80];
  char     *pc, obj_type[MXOBJ];
  char     *strrchr();
  int      err, no_obj, i, bguide;
  int      tid, fid, iva, inull, sflag;
  int      col, row, nsort, acol, arow;
  int      col_no_xpos, col_no_ypos, col_no_zval, col_no_type;
  double   xpos[MXOBJ], ypos[MXOBJ], zval[MXOBJ];
  CMDLINE  *pcmd, **ptmpl;

  SCSPRO("OPTPLATE");                  /* initiate MIDAS environment   */

  /* read name of TABLE, open it and find columns  */

  err = SCKGETC("IN_A", 1, MXNAME, &iva, tname);
  pc = tname;
  while (*pc && *pc!=' ') pc++;
  *pc = '\0';
  err = TCTOPN(tname, F_I_MODE, &tid);
  err = TCIGET(tid, &col, &row, &nsort, &acol, &arow);
  
  err = TCLSER(tid, X_LABEL, &col_no_xpos);
  err = TCLSER(tid, Y_LABEL, &col_no_ypos);
  err = TCLSER(tid, Z_LABEL, &col_no_zval);
  err = TCLSER(tid, T_LABEL, &col_no_type);

  no_obj = 0;
  for (i=1; i<=row; i++) {                  /* read table into arrays */
     err = TCSGET(tid, i, &sflag);
     if (!sflag) continue;
     iva = 0;
     err = TCERDD(tid, i, col_no_xpos, &xpos[no_obj], &inull);
     iva |= inull;
     err = TCERDD(tid, i, col_no_ypos, &ypos[no_obj], &inull);
     iva |= inull;
     err = TCERDD(tid, i, col_no_zval, &zval[no_obj], &inull);
     iva |= inull;
     err = TCERDC(tid, i, col_no_type, line, &inull);
     iva |= inull;
     if (iva) continue;
     if (MXOBJ <= no_obj) {                /* too many table rows     */
	SCTPUT("Warning: too many rows in table - truncation done!");
	no_obj++; break;
      }
     xpos[no_obj] /= 1000.0;
     ypos[no_obj] /= 1000.0;
     zval[no_obj] /= 1000.0;
     obj_type[no_obj++] = *line;
   }
  sprintf(line,"%5d objects read from table >%s<",no_obj,tname);
  SCTPUT(line);
  TCTCLO(tid);                                  /* close table file   */

  /* read name of ASCII file and open it  */

  err = SCKGETC("OUT_A", 1, MXNAME, &iva, dname);
  pc = dname;
  while (*pc && *pc!=' ') pc++;
  *pc = '\0';

  if (*dname == '*') {                    /* create default file name */
     strcpy(dname,tname);
     if ((pc=strrchr(dname,'.'))) strcpy(pc,DFEXT);
   }
  if (!strrchr(dname,'.')) strcat(dname,DFEXT);

  if ((fid=osaopen(dname,WRITE)) == -1) {  /* open ASCII command file */
     SCETER(1, "Error: cannot open ASCII output file");
   }

  /* write ASCII command file             */

  ptmpl = tmpl;
  do {
     pcmd = *ptmpl;
     if (pcmd->type) 
       for (i=0; i<no_obj; i++) {
	  bguide = (obj_type[i]=='B' || obj_type[i]=='b');
	  while ((pc=pcmd->line)) {
	     if ((!bguide && (pcmd->type & 0x10)) || 
		 (bguide && (pcmd->type & 0x20))) {
		if (pcmd->type & 0x3) {             /* check format */
		   if (pcmd->type & 0x01) 
		     sprintf(line,pc,xpos[i],ypos[i]);
		   else if (pcmd->type & 0x02) sprintf(line,pc,zval[i]);
		   pc = line;
		   while (*pc) {                 /* change . with , */
		      if (*pc=='.') *pc = ',';
		      pc++;
		    }
		   osawrite(fid,line,strlen(line));
		 }
		else osawrite(fid,pc,strlen(pc));
	      }
	     pcmd++;
	   }
	  pcmd = *ptmpl;
	}
     else while ((pc=(pcmd++)->line)) osawrite(fid,pc,strlen(pc));
     ptmpl++;
   } while (*ptmpl);

  /* close files and exit                 */

  osaclose(fid);
  sprintf(line,"ASCII drill command file >%s< written",dname);
  SCTPUT(line);

  SCSEPI();

return 0;
}
