/* @(#)optdef.h	19.1 (ESO-IPG) 02/25/03 14:27:31 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c)  1991  European Southern Observatory
.LANGUAGE   C
.IDENT      optdef
.AUTHOR     Preben Grosbol  (ESO-IPG)
.KEYWORDS   OPTOPUS, Star plates, drill, header
.ENVIRON    ESO-MIDAS
.PURPOSE    Define format and syntax of ASCII command file for
            OPTOPUS star plate drilling program
.VERSION    1.0  1991-Sep-03 :  Creation,     PJG
.VERSION    1.1  1991-Nov-21 :  Correct type of lines,     PJG
------------------------------------------------------------------------*/

typedef  struct {                       /* structure for command line  */
                  int          type;    /* type of command line        */
                  char        *line;    /* command line                */
                } CMDLINE;
/*
 *  The 'type' of the command line is defined as an integer with 
 *  Two 4-bit fields written as a hex-code:
 *     First (MSB) 4-bits give class>   0x0_ : none object dependent
 *                                      0x1_ : objects only
 *                                      0x2_ : guidestars only
 *                                      0x3_ : all objects
 *
 *     Last (LSB) 4-bits give editing>  0x_0 : no editing
 *                                      0x_1 : x-y editing
 *                                      0x_2 : z editing
 */

CMDLINE  cmd1[] = { { 0x00, "TOOL DEF 1 L0R0 \n"},
                    { 0x00, "TOOL CALL 1 Z S2000 \n"},
                    { 0x00, "    Z+50 R0 F9999 M03 \n"},
                    { 0x00, "L X0 Y166 R0 F9999 M03\n"},
                    { 0x00, "    Z+10,5 R0 F9999 M03 \n"},
                    { 0x00, "    Z+8 R0 F25 M03\n"},
                    { 0x00, "    Z+11 R0 F9999 M03 \n"},
                    { 0x00, "L X+150 Y0 R0 F9999 M03\n"}, 
                    { 0x00, "    Z+0,5 R0 F9999 M03\n"},
                    { 0x00, "LBL 1 \n"},
                    { 0x00, "    Z-2 R0 F25 M03\n"},
                    { 0x00, "    Z+0,5 R0 F9999 M03\n"},
                    { 0x00, "LBL 0 \n"},
                    { 0x00, (char *) 0} };

CMDLINE  var1[] = { { 0x31, "L X%8.3f Y%8.3f R0 F9999 M03 \n"},
                    { 0x30, "CALL LBL 1 REP\n"},
                    { 0x00, (char *) 0} };

CMDLINE  cmd2[] = { { 0x00, "    Z+150 R0 F9999 M00\n"},
                    { 0x00, "TOOL DEF 2 L-20,125R0 \n"},
                    { 0x00, "TOOL CALL 2 Z S1000 \n"},
                    { 0x00, "L X0 Y166 R0 F9999 M03\n"},
                    { 0x00, "    Z+18 R0 F9999 M03 \n"},
                    { 0x00, "    Z-1,0 R0 F20 M03\n"},
                    { 0x00, "    Z+18 R0 F200 M03\n"},
                    { 0x00, "LBL 2 \n"},
                    { 0x00, "L X+150 Y0 R0 F9999 M03 \n"},
                    { 0x00, "    Z+7,5 R0 F9999 M03\n"},
                    { 0x00, "    Z-2 R0 F15 M03\n"},
                    { 0x00, "    Z+14 R0 F9999 M03 \n"},
                    { 0x00, (char *) 0} };

CMDLINE  var2[] = { { 0x11, "L X%8.3f Y%8.3f  R0 F9999 M03 \n"},
                    { 0x12, "    Z%8.3f  R0 F15 M03 \n"},
                    { 0x10, "CYCL DEF 9.0\n"},
                    { 0x10, "CYCL DEF 9.1 TEMP. 2,0\n"},
                    { 0x10, "    Z+28 R0 F200 M03\n"},
                    { 0x10, "    Z+7,5 R0 F400 M03 \n"},
                    { 0x00, (char *) 0} };

CMDLINE  cmd3[] = { { 0x00, "LBL 0 \n"},
                    { 0x00, "    Z+150 R0 F9999 M00\n"},
                    { 0x00, "TOOL DEF 3 L-21,980R0 \n"},
                    { 0x00, "TOOL CALL 3 Z S250\n"},
                    { 0x00, "CALL LBL 2 REP\n"},
                    { 0x00, "    Z+150 R0 F9999 M00\n"},
                    { 0x00, "TOOL DEF 4 L-23,590R0 \n"},
                    { 0x00, "TOOL CALL 4 Z S600\n"},
                    { 0x00, "    Z+12 R0 F9999 M03 \n"},
                    { 0x00, (char *) 0} };

CMDLINE  var3[] = { { 0x21, "L X%8.3f Y%8.3f  R0 F9999 M03 \n"},
                    { 0x22, "    Z%8.3f  R0 F15 M03 \n"},
                    { 0x20, "CYCL DEF 9.0\n"},
                    { 0x20, "CYCL DEF 9.1 TEMP. 2,0\n"},
                    { 0x20, "    Z+18,0 R0 F100 M03\n"},
                    { 0x20, "    Z+11,5 R0 F200 M03\n"},
                    { 0x00, (char *) 0} };

CMDLINE  cmd4[] = { { 0x00, "    Z+150 R0 F9999 M00\n"},
                    { 0x00, "TOOL DEF 5 L-11,580R0 \n"},
                    { 0x00, "TOOL CALL 5 Z S200\n"},
                    { 0x00, "    Z+1 R0 F9999 M03\n"},
                    { 0x00, (char *) 0} };

CMDLINE  var4[] = { { 0x21, "L X%8.3f Y%8.3f  R0 F9999 M03 \n"},
                    { 0x22, "    Z-11 R0 F15 M03 \n"},
                    { 0x20, "    Z-2 R0 F15 M03\n"},
                    { 0x20, "    Z+1 R0 F9999 M03\n"},
                    { 0x00, (char *) 0} };

CMDLINE  cmd5[] = { { 0x00, "    Z+150 R0 F9999 M00\n"},
                    { 0x00, "TOOL DEF 6 L17,445R0\n"},
                    { 0x00, "TOOL CALL 6 Z S200\n"},
                    { 0x00, "L X0 Y+166,0 R0 F9999 M03 \n"},
                    { 0x00, "    Z9,5 R0 F9999 M03 \n"},
                    { 0x00, "    Z9,3 R0 F25 M03 \n"},
                    { 0x00, "    Z12 R0 F9999 M03\n"},
                    { 0x00, "L X+150 Y0 R0 F9999 M03 \n"},
                    { 0x00, "LBL 3 \n"},
                    { 0x00, "    Z-0,50 R0 F9999 M03 \n"},
                    { 0x00, "    Z-0,78 R0 F25 M03 \n"},
                    { 0x00, "    Z+0,50 R0 F9999 M03 \n"},
                    { 0x00, "LBL 0 \n"},
                    { 0x00, (char *) 0} };

CMDLINE  var5[] = { { 0x11, "L X%8.3f Y%8.3f  R0 F9999 M03 \n"},
                    { 0x10, "CALL LBL 3 REP\n"},
                    { 0x21, "L X%8.3f Y%8.3f  R0 F9999 M03 \n"},
                    { 0x20, "    Z-5,00 R0 F9999 M03 \n"},
                    { 0x20, "    Z-5,20 R0 F25 M03 \n"},
                    { 0x20, "    Z+0,50 R0 F9999 M03 \n"},
                    { 0, (char *) 0} };

CMDLINE  cmd6[] = { { 0x00, "    Z+150 R0 F9999 M00\n"},
                    { 0x00, "TOOL DEF 7 L-12,530R1,510 \n"},
                    { 0x00, "TOOL CALL 7 Z S1600 \n"},
                    { 0x00, "L X+150 Y0 R0 F9999 M03 \n"},
                    { 0x00, "    Z+1 R0 F9999 M03\n"},
                    { 0x00, "    Z-2 R0 F400 M03 \n"},
                    { 0x00, "LBL 4 \n"},
                    { 0x00, "CYCL DEF 5.0\n"},
                    { 0x00, "CYCL DEF 5.1 DIST. -0,25\n"},
                    { 0x00, "CYCL DEF 5.2 PROF. -0,005 \n"},
                    { 0x00, "CYCL DEF 5.3 PASSE -0,005 F400\n"},
                    { 0x00, "CYCL DEF 5.4 RAYON +1,710 \n"},
                    { 0x00, "CYCL DEF 5.5 F20   DR-\n"},
                    { 0x00, "CYCL CALL M03 \n"},
                    { 0x00, "    Z0 R0 F400 M03\n"},
                    { 0x00, "LBL 0 \n"},
                    { 0x00, (char *) 0} };

CMDLINE  var6[] = { { 0x11, "L X%8.3f Y%8.3f  R0 F9999 M03 \n"},
                    { 0x12, "    Z%8.3f  R0 F400 M03\n"},
                    { 0x10, "CALL LBL 4 REP\n"},
                    { 0x00, (char *) 0} };

CMDLINE  cmd7[] = { { 0x00, "   Z+110 R0 F9999 M03\n"},
                    { 0x00, "STOP M02\n"},
                    { 0x00, (char *) 0} };

CMDLINE  *tmpl[] = { cmd1, var1, cmd2, var2, cmd3, var3, cmd4, var4,
                     cmd5, var5, cmd6, var6, cmd7,
                     (CMDLINE *) 0};
