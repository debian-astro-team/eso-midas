C @(#)alpha.for	19.1 (ES0-DMD) 02/25/03 14:27:33
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE ALPHA(A,LENGTH,NCHAR,NREST)
C+++                                        
C.PURPOSE:  Alphanumeric handling routine
C.AUTHOR:   Rein H. Warmels
C.COMMENTS: none
C.VERSION:  87???? RHW  creation
C.VERSION:  910115 RHW  IMPLICIT NONE added
C---
      IMPLICIT     NONE
      CHARACTER*(*) A                  ! IN: input array of dimension n        
      INTEGER      LENGTH              ! IN: max length of the character string
      INTEGER      NCHAR               ! OUT: number of actual characters
      INTEGER      NREST               ! OUT: number of remaining characters
C
      CHARACTER    X
      INTEGER      I
C
C***
      NCHAR  = 0 
      DO 10 I = 1,LENGTH 
         X      = A(LENGTH-I+1:LENGTH-I+1)
         IF (X.NE.' ') THEN 
            NCHAR  = LENGTH + 1 - I 
            NREST  = LENGTH - NCHAR
            RETURN
         END IF
   10 CONTINUE 
      RETURN 
      END



      SUBROUTINE LENBUF(BUF,L) 
C+++
C.PURPOSE:  counts the number of effective characters in a buffer           
C.AUTHOR:   Rein H. Warmels
C.COMMENTS: none                                                            
C.VERSION:  880205 RHW Creation                                             
C.VERSION:  910115 RHW  IMPLICIT NONE added
C---
      IMPLICIT      NONE
      CHARACTER*(*) BUF                   ! IN : character string with text 
      INTEGER       L                     ! OUT: number of characters in string
C
      INTEGER       LB
C
C ***
      LB = LEN(BUF)
  100 CONTINUE
         IF (BUF(LB:LB).NE.' ' .OR. LB.EQ.0) GO TO 200
         LB = LB - 1
      GO TO 100
  200 CONTINUE
      L = LB 
      RETURN 
      END 


      INTEGER FUNCTION LENC(C)
C+++ 
C.PURPOSE:  counts the number of effective characters in a buffer
C.AUTHOR:   Rein H. warmels
C.COMMENTS: none
C.VERSION:  880205 RHW Creation
C.VERSION:  910115 RHW  IMPLICIT NONE added
C---
      IMPLICIT      NONE
      CHARACTER*(*) C                          ! IN: string containing the text
      INTEGER       I 
C
C ***
      DO 10 I = LEN(C),1,-1
         LENC = I
         IF(C(I:I).NE.' '.AND.ICHAR(C(I:I)).NE.0) GOTO 11
10    CONTINUE
 
11    CONTINUE

      RETURN
      END

