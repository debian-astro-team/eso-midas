% @(#)normalspec.hlq	19.1 (ESO-IPG) 02/25/03 14:28:42 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      normalspec.hlq
%.AUTHOR     DB, IPG/ESO
%.KEYWORDS   MIDAS, help files, NORMALIZE/SPECTRUM
%.PURPOSE    On-line help file for the command: NORMALIZE/SPECTRUM
%.VERSION    1.0  01-NOV-1986 : Creation, DB
%----------------------------------------------------------------
\se
SECTION./SPEC
\es\co
NORMALIZE/SPECTRUM					01-NOV-1986  DB
\oc\su
NORMALIZE/SPECTRUM inframe outframe [mode] [table] [batch_flag]
	approximate continuum of 1-dim. spectra for later normalization
\us\pu
Purpose:    
            Approximate continuum of 1-D spectra for later normalization.
\up\sub
Subject:    
            Spectroscopy, flux calibration, spectral analysis.
\bus\sy
Syntax:     
            NORMALIZE/SPECTRUM inframe outframe [mode] [table] [batch_flag]
\ys\pa
            inframe   = name of input frame
\ap\pa
            outframe  = name of frame to hold the fit
\ap\pa
            mode      = GCURSOR, to start from scratch, input from graphics
                        cursor
\\
                      = ADD, to add new points to existing fit, input with 
                        cursor
\\
                      = DELETE, to delete points from existing point (with
                        cursor)
\\
                      = TABLE, to take positions and bin widths from "table" 
                        and integrate in "inframe" over corresponding bins.
                        Default: GCURSOR
\ap\pa
           table      = intermediate working table with cursor positions
                        defining wavelengths and bin widths.
                        In mode TABLE the flux in "inframe" is integrated
                        over bins provided with "table". "table" must contain
                        one column labeled :X_AXIS (with the wavelengths) and
                        another one labeled :BIN_WIDTH (with the bin widths).
                        Default: TABLE
\ap\pa
           batch_flag = if equal to Y all plots will be suppressed.
                        Default: N
\ap\out
Output:    
           Intermediate table FIT1D is used, with columns :X_AXIS and :Y_AXIS.
\tuo\no
Note:      
           All data is internally written to table FIT1D on which the actual
           spline is to be made.
           Mode GCURSOR is self-evident. Enter data with cursor and ENTER key.
           Exit: space bar.
           The wavelengths are taken as the center of the bins. For the
           integration, a rectangular "transmission curve" is assumed.
           The points determined are plotted as is the fit derived from them.
           Modes ADD and DELETE can be used to interactively edit table FIT1D
	   until a satisfactory fit is achieved. To delete a point, reply
           "*" to the question asked on your session terminal. NOTE that
           only the latest fit made with this command can be edited!

	   Note that the actual normalization needs, then, to be done like
           COMPUTE/IMAGE normalized = inframe/outframe.
\on\exs
Examples:
\ex
           NORMALIZE/SPECTRUM RAW FIT
           start a fit of image RAW, input is expected from graphics terminal,
           result to be written to new image FIT
\xe\ex	
           NORMALIZE/SPECTRUM RAW FIT D
  	   delete some ill fitting points (use graphics cursor), a new fit
           (new image FIT) will be made
\xe\ex
           NORMALIZE/SPECTRUM SPECTRUM CONTINUUM T LAMBIN
           use wavelengths and bin width in table LAMBIN to accordingly
           integrate the flux in image SPECTRUM
\xe \sxe
