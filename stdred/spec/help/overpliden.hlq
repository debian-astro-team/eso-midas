% @(#)overpliden.hlq	19.1 (ESO-IPG) 02/25/03 14:28:42 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      overpliden.hlq
%.AUTHOR     RHW, IPG/ESO
%.KEYWORDS   MIDAS, help files, OVERPLOT/IDENTIFICATION
%.PURPOSE    On-line help file for the command: OVERPLOT/IDENTIFICATION
%.VERSION    1.0  20-DEC-1986 : Creation, RHW
%----------------------------------------------------------------
\se
SECTION./IDEN
\es\co
OVERPLOT/IDENTIFICATION                                        20-DEC-1986  RHW
\oc\su
OVERPLOT/IDENTIFICATION [table] [xpos] [ident] [ypos]
	overplot line identifications
\us\pu
Purpose:    Overplot line identifications on the already displayed spectrum.
            The position on the x-axis and the corresponding identified lines
            are defined in two columns of the table.
\up\su
Subject:    Spectral analysis, wave calibration, pattern recognition, graphics
\us\sy
Syntax:     OVERPLOT/IDENTIFICATION [table] [xpos] [ident] [ypos]
\ys\pa
            table     = name of the table with the line identifications;
                        default: LINE
\ap\pa
            xpos      = reference to column with positions. The column must
                        be of REAL*4 type; default :X
\ap\pa
            ident     = reference to column with identifications.
                        The column can be of any type. The display
                        format of the column is used in the plotting;
                        default :IDENT
\ap\pa
            ypos      = code to define the vertical positions of labels
                        as TOP (default) or BOTTOM

\ap\sa
See also:   OVERPLOT/IDENT, SET/PLOT, PLOT
\as\exs
Examples:
\ex
            OVERPLOT/IMAGE MYFRAME
\xe\ex
            OVERPLOT/IDENTIFICATION MYTABLE :X :LABEL
\xe \sxe
\res
Restrictions: \\ A frame is assumed to have been plotted on the graphics display.
            Columns with positions are R*4 type.
\ser
