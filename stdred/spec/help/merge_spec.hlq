% @(#)merge_spec.hlq	19.1 (ESO-IPG) 02/25/03 14:28:42 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1992 European Southern Observatory
%.IDENT      merge_eche.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, MERGE/SPECTRUM
%.PURPOSE    On-line help file for the command: MERGE/SPECTRUM
%.VERSION    1.0  17-AUG-1992 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./SPEC
\es\co
MERGE/SPECTRUM 						17-AUG-1992 PB
\oc\su
MERGE/SPECTRUM spec1 spec2  out [interval] [mode] [var1] [var2]
	merge two 1D spectra
\us\pu
Purpose:    
            Merges two 1D spectra. The algorithm computes a weighted 
            average in the overlapping region of adjacent orders. The 
            normalized weight is a linear ramp between 0 and 1 in 
            the overlapping region.

\up\sy
Syntax:     MERGE/SPECTRUM spec1 spec2  out [interval]  [mode] [var1] [var2]
\ys\pa
            spec1      = input image, sampled in the wavelength space.
                         Corresponds to a extracted and rebinned spectrum.
\ap\pa
            spec2      = similar to spec1. This frame can precede
                         or follow spec1 in the wavelength space.
\ap\pa
            out        = name of the resulting output spectrum.
\ap\pa
            interval   = wavelength interval to be skipped at both edges 
                         of the overlapping region (in pixels).
                         Default is 5.
\ap\pa
            mode       = Mode of computing the weights in the overlaps.
                         Possible modes are: CONSTANT(default), RAMP or
                         OPTIMAL. The mode OPTIMAL requires the two additional
                         variance images var1 and var2.
\ap\pa      
            var1, var2 = Variance images for the computation of optimal 
                         weights.
\ap\no
Note:       Out of the overlaps, the spectra are copied to the output
            spectrum. Within the overlap, the weights formula are the 
            following:\\
            Method CONSTANT: \\
                   out  = (spec1 + spec2)/2.\\
            Method RAMP:\\
                   out  = spec1*(1-c) + spec2*c,\\ 
                   where c is a ramp varying from 0 to 1 in the overlap.
            Method OPTIMAL  :\\    
                   out  = (spec1*var1**2 + spec2*var2**2)/(var1**2+var2**2)
\on\exs
Examples:
\ex
            MERGE/SPECTRUM  spe0016  spe0017   spe1617   2
\xe \sxe


