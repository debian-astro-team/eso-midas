% @(#)searchline.hlq	19.1 (ESO-IPG) 02/25/03 14:28:42 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      searchline.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, SEARCH/LINE
%.PURPOSE    On-line help file for the command: SEARCH/LINE
%.VERSION    1.0  22-JUNE-1984 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./LINE
\es\co
SEARCH/LINE						18-DEC-1992 PB
\oc\su
SEARCH/LINE frame w,t[,nscan] [table] [meth] [type]
	search for spectral lines
\us\pu
Purpose:    Search for emission/absorption lines in spectra.
\up\sub
Subject:    Spectral analysis, wave calibration.
\bus\sy
Syntax:     SEARCH/LINE frame w,t[,nscan] [table] [meth] [type]
\ys\pa
            frame  = input spectrum
\ap\pa
            w,t,nscan  = input parameters as:
\\
                     w      (window) is the approx. zero intensity line
                            width in pixels.
\\
                     t      (threshold) is the absolute value of the
                            background.
\\
                     nscan  number of scan lines of the input spectrum
                            to be averaged before searching for the
                            lines. Default is one.
\ap\pa
            table  = output table. Default name is line.tbl
                     The output table contains the columns:
\\
            :X    - center of the line, in world coordinates
\\
            :Y    - scan number for 2D spectra
\\
            :PEAK - value at the maximum/minimum
\ap\pa
            method = centering method as
\\
                     GRAVITY - center of gravity of the 2 highest pixels
                               with respect to the third one.
\\
                     MAXIMUM - pixel with the maximum value
\\
                     MINIMUM - pixel with the minimum value (default for
                               absorption lines)
\\
                     GAUSSIAN- center of the gaussian fitted to the line
                               (Default method).
\ap\pa
            type   = EMISSION (Default) or ABSORPTION
\ap\no
Note:
            1. Detection algorithm: Data in a running window is sorted
               to form a local histogram. An emission (absorption) feature
               is found when the central value in the window is greater
               than the median value plus (minus) the threshold. The width 
               of the window corresponds to parameter w, the threshold to t.\\
\\
            2. Centering algorithm: Lines are centered according to 
               the selected method:\\
\\
                - Gaussian: Centers a gaussian to the line, using the
                            true pixel values (no local background
                            involved).\\
\\
                - Gravity:  Finds the center of gravity of two highest 
                            points of 3 points (the maximum and the two
                            flanking pixels).\\
\\
                - Maximum:  Finds position of the maximum.\\
\\
            3. The line positions in column :X are expressed in world
               coordinates. All spectra involved in the reduction session
               must in principle present the same start and step values.
               However, the package allows modifications of the world 
               coordinates references (at your own risk).\\
\on\see
See also:   IDENTIFY/LINE, CALIBRATE/LINE, REBIN/WAVE
\ees\exs
Examples:
\ex
            SEARCH/LINE SPECTRUM 5,100 line  GRAVITY EMISSION
            This command will search for emission lines in spectrum having
            more than five pixels and peak value over 100 in the local
            background.
\xe\sxe














