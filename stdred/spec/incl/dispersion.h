/* @(#)dispersion.h	19.1 (ESO-DMD) 02/25/03 14:28:53 */
/*===========================================================================
 021030		last modif
===========================================================================*/


#ifndef DISPERS_SPEC
#define DISPERS_SPEC

#include "mutil.h"

#include <math.h>
#include <midas_def.h>
#include <tbldef.h>
#include <lndisp.h>

#include <proto_spec.h>

#ifndef PI  
#define PI 3.141592653589793
#endif

#define TOL 1.0e-5
#define MAXNCOE 100

#define  FUNCTION        dpoly   

double pixbin;			/* Pixel bin in wavelength units */
double coef[MAXNCOE];		/* Array of coefficients  */
int fdeg, ncoef, refdeg, maxcoef;	/* Number of coefficients, Degree */

int tide, colslit, colline, coly, colrms, colcoef[MAXNCOE];
int linpix, linrms;
int start_index, nbline;

#endif


