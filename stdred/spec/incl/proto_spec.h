/* @(#)proto_spec.h	19.1 (ESO-IPG) 02/25/03 14:28:54 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   proto_spec.h
.AUTHOR      C. Guirao IPG-ESO Garching
.KEYWORDS    prototypes
.LANGUAGE    C & ANSI-C
.PURPOSE     prototypes for all SPEC routines,

.ENVIRONment none

.VERSION     1.0     22-Oct-1993   Creation by CG.
------------------------------------------------------------*/

#ifndef PROTO_SPEC
#define PROTO_SPEC

extern void eval_disp(
#ifdef __STDC__
	double x[], double l[], int n
#endif
);

extern void finishdisp(
#ifdef __STDC__
	 void 
#endif
);

extern void initdisp(
#ifdef __STDC__
	char name[], char mode[], int start
#endif
);

extern void printdisp(
#ifdef __STDC__
	 void 
#endif
);

extern double readdisp(
#ifdef __STDC__
	int y
#endif
);

extern void setdisp(
#ifdef __STDC__
	int deg, double coefs[]
#endif
);

extern void setrefdeg(
#ifdef __STDC__
	int deg
#endif
);

void writedisp(
#ifdef __STDC__
	int line, int ypix, double y, double pixel, double rms
#endif
);

void lfit2(
#ifdef __STDC__
           double x[], double y[], double sig[], int ndata, double a[],
           int ma,
           void (*funcs)(double, double *, int)
#endif
);

#endif
