C @(#)guess.for	19.1 (ES0-DMD) 02/25/03 14:29:01
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C

      SUBROUTINE GUESS (MODE,COEFF,SIZE,IDEG)

C --- Reads descriptors of table line and load initial
C --- coefficients of the dispersion relation X = f(lambda).
C
C INPUT:
C KEYNAM  CHAR*8  Generic name of descriptors storing the coefficients
C SIZE    INTG    Size of double precision array COEFF
C IDEG    INTG    Degree of the fit. If guess option, this value is 
C                 checked against the stored value.
C KEYWORDS INPUT:
C IN_A/C/1/60     Mode of operation IN_A  = GUESS for guess mode
C IN_B/C/1/60     Name of guess table.
C INPUTD/D/1/1    Shift in pixels
C
C DESCRIPTORS INPUT:
C GUESSI/I/5/1    Degree of the regression
C GUESSD/D/1/20   Coefficients of the regression.
C
C OUTPUT:
C MODE    INTG    Mode=0 (no Guess) or Mode=1 (Guess)
C COEFF   REAL*8  Coefficients
C IDEG    INTG    Stored value if guess option.
C

      IMPLICIT NONE
      INTEGER  MODE, SIZE, IDEG, GDEG
      INTEGER  KUN, KNUL, STATUS, IACT, TID
      CHARACTER  GUETAB*60, WLCMTD*8
      DOUBLE PRECISION COEFF(SIZE), SHIFT

C --- Read keyword WLCMTD and check if Guess mode. Optionally read
C --- keyword GUETAB.

      CALL STKRDC('IN_A',1,1,8,IACT,WLCMTD,KUN,KNUL,STATUS)

      MODE = 0
      IF (WLCMTD(1:1).EQ.'G' .OR. WLCMTD(1:1).EQ.'g') THEN
          MODE = 1
          CALL STKRDC('IN_B',1,1,60,IACT,GUETAB,KUN,KNUL,STATUS)
          CALL TBTOPN(GUETAB, 10, TID, STATUS)
          CALL STDRDI(TID,'GUESSI',6,1,IACT,GDEG,KUN,KNUL,STATUS)
          IF (GDEG.NE.IDEG) THEN
             CALL STTPUT ('Changed degree to the value
     . stored in guess table',STATUS)
             IDEG = GDEG
          ENDIF
          CALL STDRDD(TID,'GUESSD',1,20,IACT,COEFF,KUN,KNUL,STATUS)
          CALL STKRDD('INPUTD',1,1,IACT,SHIFT,KUN,KNUL,STATUS)
          COEFF(1) = COEFF(1) + SHIFT
          CALL TBTCLO(TID,STATUS)
      ENDIF

      RETURN
      END







