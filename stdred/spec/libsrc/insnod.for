C @(#)insnod.for	19.1 (ESO-DMD) 02/25/03 14:29:01
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE INSNOD(NODE,FA,LINK,NAUX)                                      
C 
      IMPLICIT NONE                                                            
C 
      INTEGER CURR,NEXT,NODE,NAUX,LINK(NAUX)
C 
      REAL FA(NAUX)                                                             
C                                                                               
C  curr gets loc of listhead (first loc in FA, LINK)                            
C                                                                               
      CURR   = 1                                                                
   10 NEXT   = LINK(CURR)                                                       
C                                                                               
C  while next <> null LINK and SLOT not found do                                
C                                                                               
      IF (NEXT.EQ.0) GO TO 20                                                   
      IF (FA(NEXT).GE.FA(NODE)) GO TO 20                                        
      CURR = NEXT                                                             
C             advance ptrs                                                      
      GO TO 10                                                                  
                                                                                
   20 LINK(NODE) = NEXT                                                         
      LINK(CURR) = NODE                                                         
C             set pointers and return                                           
      RETURN                                                                    
C
      END                                                                       
