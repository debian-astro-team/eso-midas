C @(#)newnod.for	19.1 (ESO-DMD) 02/25/03 14:29:02
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      INTEGER FUNCTION NEWNOD(POINT,AVAIL,MIN,MAX,FA,LINK,NAUX)
C 
      IMPLICIT NONE
C 
      INTEGER NAUX,AVAIL,LINK(NAUX),MIN,MAX
      REAL FA(NAUX),POINT
C
C ADVANCE AVAIL (TAKE A NODE OFF FREE LIST)
C
      NEWNOD = AVAIL
      FA(AVAIL) = POINT
      LINK(AVAIL) = 0
      AVAIL  = AVAIL + 1
      IF (AVAIL.GT.MAX) AVAIL  = MIN
      RETURN

      END
