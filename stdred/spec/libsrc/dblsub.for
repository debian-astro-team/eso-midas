C @(#)dblsub.for	19.1 (ES0-DMD) 02/25/03 14:29:00
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
        subroutine read1d(IN,NPIX,DATA)

        implicit none

        INTEGER  NPIX
        REAL     IN(NPIX)
        REAL     DATA(NPIX)

        INTEGER  II

        do  II = 1,NPIX
          DATA(II) = IN(II)
        end do

        return
        end


        subroutine read2d(IN,NPIX,DATA,NDATA)

        implicit none

        INTEGER  NPIX(2)
        REAL     IN(1)
        INTEGER  NDATA
        REAL     DATA(NDATA)
        character*80  STRING
        INTEGER  STAT

        INTEGER  II,JJ,KK

C        STRING = ' NPIX = '
C        write (STRING(9:80),101) (NPIX(II),II=1,2)
C        call sttput(STRING,STAT)
 101        format(2I4)

C        STRING = ' IN = '
C        write (STRING(7:80),102) (IN(1,II),II=1,10)
C        call sttput(STRING,STAT)
 102        format(10F5.0)

        KK = 0
        do  JJ = 1,NPIX(2)
          do  II = 1,NPIX(1)
            KK = KK+1
            DATA(KK) = IN(KK)
            if (DATA(KK).eq.0) then
              STRING = ' zero data value read in at pixel '
              write (STRING(35:80),100) KK
              call sttput(STRING,STAT)
            end if
          end do
        end do

 100        format(I2)

        return
        end


