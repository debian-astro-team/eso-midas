C @(#)delnod.for	19.1 (ES0-DMD) 02/25/03 14:29:00
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE DELNOD(NODE,FA,LINK,NAUX)                                      
      IMPLICIT NONE                                                            
      INTEGER NAUX,LINK(NAUX),NODE,CURR,NEXT
      REAL FA(NAUX)                                                             
C                                                                               
C  CURR GETS LOC OF LISTHEAD                                                    
C                                                                               
      CURR   = 1                                                                
C                                                                               
C   WHILE NEXT NOT THE NODE TO DELETE DO GET NEXT NODE                          
C                                                                               
   10 NEXT   = LINK(CURR)                                                       
      IF (NEXT.EQ.0) GO TO 30                                                   
      IF (NEXT.EQ.NODE) GO TO 20                                                
      CURR   = NEXT                                                             
      GO TO 10                                                                  
C             DELETE NODE FROM LIST                                             
   20 LINK(CURR) = LINK(NEXT)                                                   
      RETURN                                                                    
C                                                                               
C   ERROR EXIT, SHOULDN'T GET HERE                                              
C   ELEMENT NOT FOUND                                                           
C                                                                               
   30 CONTINUE                                                                  
      RETURN                                                                    
                                                                                
      END                                                                       
