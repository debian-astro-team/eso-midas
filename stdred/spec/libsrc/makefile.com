$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.STDRED.SPEC.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:59 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   delnod.for
$ FORTRAN   insnod.for
$ FORTRAN   newnod.for
$ FORTRAN   identf.for
$ FORTRAN   guess.for
$ FORTRAN   spec.for
$ FORTRAN   dblsub.for
$ FORTRAN   prc.for
$ FORTRAN   prd.for
$ FORTRAN   pre.for
$ FORTRAN   prf.for
$ FORTRAN   prm.for
$ FORTRAN   world.for
$ LIB/REPLACE libspec delnod.obj,insnod.obj,newnod.obj,identf.obj,guess.obj,spec.obj,misc.obj,dispersion.obj,libhough.obj,dblsub.obj,prc.obj,prd.obj,pre.obj,prf.obj,prm.obj,world.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
