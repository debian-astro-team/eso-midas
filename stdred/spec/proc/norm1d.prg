!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! Midas command procedure  norm1d.prg
!
! D. Baade, ST-ECF, Garching           version 1.0               281085
!
! execute the command :
! normalize/spectrum inimage fitimage [mode] [table] [batch_flag]
!
! inimage:   input 1-dim frame
! fitimage:  fit to the continuum
! mode:      GCURSOR - create new table FIT1D, input from graphics cursor
!            ADD     - add new points to table FIT1D
!            DELETE  - delete points from table FIT1D
!            TABLE   - get positions and bin widths from "table", integrate
!		       in "inimage" over corresponding bins
! batch_flag  if equal to Y suppress all plots
!
! 040114	last modif
!-------------------------------------------------------------------------------
!
define/param p1 ? IMAGE "Enter input image:"
define/param p2 ? IMAGE "Enter output image:"
define/param p3 G C
define/param p4 TABLE C
define/param p5 N ?
!
branch p3(1:1) T,A,D  TABLE,ADD,DELETE
! 
GCURSOR: 		!start from scratch is the default
plot/row {p1}
write/out "                       >>> Use graphics cursor to enter data <<<"
get/gcurs FIT1D
goto FIT
!
ADD: 			!add points to previoulsy created table FIT1D
plot/row {p1}
overplot/table FIT1D :X_AXIS :Y_AXIS
get/gcurs FIT1D ADD
goto FIT
!
DELETE:			!delete points from previously created table FIT1D
plot/row {p1}
overplot/table FIT1D :X_AXIS :Y_AXIS
ident/gcurs FIT1D :X_AXIS :X_AXIS :Y_AXIS 3
goto FIT
!
TABLE: 			!integrate over bins in inmage
if p5(1:1) .ne. "Y" plot/row {p1}
write/keyw nfimage/c/1/60 {p1}
write/keyw nftable/c/1/60 "FIT1D"
-copy {p4}.tbl FIT1D.tbl
create/column FIT1D :Y_AXIS
run STD_EXE:INTEGBIN
if p5(1:1) .ne. "Y" overplot/table FIT1D :X_AXIS :Y_AXIS
!
! common section
! 
FIT: 				! do the spline fit
sort/table FIT1D :X_AXIS
convert/table {p2} = FIT1D :X_AXIS :Y_AXIS {P1} SPLINE
if p5(1:1) .ne. "Y" overplot/row {p2}

