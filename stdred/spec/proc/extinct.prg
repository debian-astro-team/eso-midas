! @(#)extinct.prg	19.1 (ES0-DMD) 02/25/03 14:29:18
! ++++++++++++++++++++++++++++++++++++++++++++++++
!
! Midas procedure : extinct.prg
!
! D. Baade, ESO - Garching                   870525
! 900703    KB
!
! .PURPOSE : correct spectra for interstellar or atmospheric extinction
!
! execute via:
! EXTINCT/SPEC inimage outimage [scale] [table] [col]
!
! inimage   - the image to be correct for extinction
! outimage  - the image to hold the corrected data
! scale     - the scaling factor: a) E(B-V) if interstellar reddening
!				  b) AIRMASS if atmospheric extinction
! table     - the table with the wavelength dependence of the reddening or
!             the extinction
! col       - the column of the table to be used
!
! --------------------------------------------------
!
define/param p1 ?        IMAGE  "Enter input image:"
define/param p2 ?        IMAGE  "Enter output image:"
define/param p3 0        NUMBER
define/param p4 atmoexan TABLE
define/param p5 2        NUMBER
define/maxpar 5
!
define/local scale/r/1/1 {p3}
!
if {p3} .eq. 0 then				!get airmass
   if m$existd(p1,"O_AIRM") .eq. 1 then
      copy/dkey {p1} O_AIRM SCALE
   else
      if m$existd(p1,"AIRMASS") .eq. 1 then 
         copy/dkey {p1} AIRMASS SCALE
      else
         write/out "Could not find descriptor O_AIRM nor AIRMASS in image {P1}"
         inquire/keyword scale/r/1/1 "Enter airmass value : "
      endif
   endif
endif
!
write/out "Airmass value : {scale}"
!
create/image &r 1,{{p1},npix(1)} {{p1},start(1)},{{p1},step(1)}
convert/table middumex = {p4} #1 #{p5} &r  SPLINE   !convert to image 
if {{p1},naxis} .gt. 1 then
   grow/image &t = middumex {{p1},start(2)},{{p1},step(2)},{{p1},npix(2)}
   -rename middummt.bdf middumex.bdf
endif
compute/image {p2} = {p1}*exp10(0.4*middumex*{scale})	!do the computation
!
delete/image middumex NO			!discard intermediate file
