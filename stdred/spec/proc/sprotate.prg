! @(#)sprotate.prg	19.1 (ES0-DMD) 02/25/03 14:29:22
! @(#)sprotate.prg	19.1  (ESO)  02/25/03  14:29:22
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       sprotate.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!.KEYWORDS    Spectroscopy
!.PURPOSE     Rotation/Flip of a catalog of images
!.VERSION     1.0  Package Creation  14-JAN-1994
!-------------------------------------------------------
!
CROSSREF  cat root meth axis angle mode
!
DEFINE/PARAM P1 ?    CHAR  "Enter input catalogue:"
DEFINE/PARAM P2 rot  CHAR  "Root Name for Output:"
DEFINE/PARAM P3 FLIP CHAR  "Method (FLIP/ROTATE)"
DEFINE/PARAM P4 X    CHAR  "Flip axis (X,Y,XY)"
DEFINE/PARAM P5 90.0 NUMB  "Rotation angle (degrees):"
DEFINE/PARAM P6 KEEP CHAR  "Mode (KEEP/DEL) for input image:"
!
DEFINE/MAXPAR  6
!
DEFINE/LOCAL CATAL/I/1/1  0
DEFINE/LOCAL INPUT/C/1/60 INDUMMY
DEFINE/LOCAL OUTPUT/C/1/60 "            "
DEFINE/LOCAL INCAT/C/1/60 "                    "
DEFINE/LOCAL NUMB/I/1/1   1
CLEAR/ICAT
!
!  test, if really catalog name entered...
!
DEFINE/LOCAL II/I/1/2 0,0
II(1) = M$INDEX(P1,".cat")
IF II(1) .LE. 0 THEN
   WRITE/KEYW INCAT 'P1'.cat
ELSE
   WRITE/KEYW INCAT 'P1'
ENDIF
!
! Start the loop over the input frames in the catalogue
!
LOOP:
STORE/FRAME INPUT 'INCAT' 
!READ/KEYW INPUT
WRITE/KEYW  OUTPUT  {P2}{NUMB}
NUMB = NUMB + 1
!
! Rebin + Flip
!
IF M$ABS({P5}) .LT. 0.0001  THEN
  COPY/II      'INPUT' 'OUTPUT'
ELSE
  REBIN/ROTATE 'INPUT' 'OUTPUT'  {P5}
ENDIF
!
IF "'P3(1:1)'" .EQ. "F" THEN
  FLIP/IMAGE 'OUTPUT' {P4}
ENDIF
!
WRITE/DESCR 'OUTPUT' START 1.,1.
WRITE/DESCR 'OUTPUT' STEP  1.,1.
!
WRITE/OUT ***************************************************
WRITE/OUT Rotated Input frame: 'INPUT'. Output: 'OUTPUT'
IF P6(1:1) .EQ. "D" THEN
   DELETE/IMAGE 'INPUT' NO
   WRITE/OUT "Deleted image: 'INPUT'"
ENDIF
!
IF CATAL .GT. 0 GOTO LOOP
!
WRITE/OUT *** FINISHED ***

RETURN
