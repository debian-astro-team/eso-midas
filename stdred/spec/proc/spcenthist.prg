! @(#)spcenthist.prg	19.1 (ES0-DMD) 02/25/03 14:29:20
!
define/param P1 ?    IMA  "Input Image:"

define/local median/R/1/2 0.,0.

stat/image {P1} >Null
median(1) = outputr(8) ! Median
compute/image &t = {P1} - ({outputr(8)})  ! Subtracts median
compute/image &tt = &t * &t
stat/image &tt >Null
median(2) =  M$SQRT(outputr(8)) ! Median of squared residuals

write/out "Median = {median(1)} - S = {median(2)}"
copy/key   median/R/1/2 outputr/R/1/2

return

