! @(#)overiden.prg	19.1 (ES0-DMD) 02/25/03 14:29:19
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  ECHELLE PROCEDURE : OVERIDEN.PRG
!  J.D.Ponz                	version 1.0 131083
!
! .PURPOSE
!
!    implements the overplot on the HP terminal as :
!
!    OVERPLOT/IDENTIFICATION table col-ref1 col-ref2 ypos
!
! --------------------------------------------------------
!
DEFINE/PARAM P1 line TABLE
DEFINE/PARAM P2 :X       ?
DEFINE/PARAM P3 :IDENT   ?
DEFINE/PARAM P4 TOP      C
RUN STD_EXE:OVERIDEN
!
WRITE/KEYW PLCDATA/C/1/60  {P1}                    ! name of data structure
WRITE/KEYW PLCDATA/C/61/20 "TABLE"                 ! type of data structure
@ sendplot


