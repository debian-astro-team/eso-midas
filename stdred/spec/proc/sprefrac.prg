! @(#)sprefrac.prg	19.1 (ES0-DMD) 02/25/03 14:29:22
DEFINE/PAR P1 ? IMA "Enter input frame:  "
DEFINE/PAR P2 ? IMA "Enter output frame:  "
DEFINE/PAR P3 INTER CHAR "Mode (Inter/Auto)"

WRITE/KEY IN_A 'P1'
WRITE/KEY IN_B 'P3'
WRITE/KEY OUT_A 'P2'

set/graph xaxis=auto
set/graph color=1
plot 'p1'

RUN STD_EXE:REFRAC

set/graph color=2
over 'p2'
