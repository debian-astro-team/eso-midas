! @(#)sphough.prg	19.1 (ES0-DMD) 02/25/03 14:29:21
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lnhough.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     Tutorial for context Long . PB 12.02.1993
!.VERSION     1.0  Command Creation  09-APR-1993
! 
! 020913	last modif
!-------------------------------------------------------
!
CROSSREF wdisp wcent ystart line cat  mode  range
!
define/param P1  {AVDISP}  ?    "Dispersion, tolerance, accuracy:"
define/param P2  {WCENTER} ?    "Central wav., tolerance, accuracy:"
define/param P3  ?   NUM        "Npix(1), Start(1), Step(1):"
define/param P4  {LINTAB}  TAB    "Line table:"
define/param P5  {LINCAT}  TAB    "Line catalog:"
define/param P6   LINEAR   CHAR   "Mode:"
define/param P7   2.5      NUM    "Range:"
define/param P8   K        CHAR   "Keep/Visualise result (K/N/V)"

define/local  wcent/D/1/3  0.,0.,0.
define/local  wdisp/D/1/3  0.,0.,0.
define/local   DIM/I/1/1   0

write/keyw     wcent {P2}
write/keyw     wdisp {P1}

if wdisp(2) .eq. 0.  wdisp(2) = 15   ! Tolerance in percent
if wdisp(3) .eq. 0   wdisp(3) = 1    ! Accuracy in percent
if wcent(2) .eq. 0   wcent(2) = 50   ! Tolerance 100 pixels
if wcent(3) .eq. 0   wcent(3) = 0.2  ! Accuracy in pixels

SET/FORMAT  F8.3
WRITE/OUT "Initial values:"
WRITE/OUT " Central Wav.  : {wcent(1)} +/- {wcent(2)} pix, {wcent(3)} pix"
WRITE/OUT " Average disp. : {wdisp(1)} +/- {wdisp(2)} %, {wdisp(3)} %"

!wdisp(1) = wdisp(1)
wdisp(2) = wdisp(2)/100 
wdisp(3) = wdisp(3)/100
!
define/local nblines/I/1/1 0
define/local conf/R/1/1    0.
!
!


DEFINE/LOCAL  XC/D/1/1     0.
DEFINE/LOCAL  XMEAN/R/1/1  0.
define/local RANGE/R/1/1  0.
define/local FACTOR/R/1/1 0.
define/local OFFSET/R/1/1 0.

define/local wdes/D/1/3 {P3}
DEFINE/LOCAL STEP/D/1/1  {wdes(3)}
DEFINE/LOCAL START/D/1/1 {wdes(2)}
DEFINE/LOCAL NPIX/I/1/1  {wdes(1)}

RANGE  = {P7}*STEP(1)

! Dispersion relation is relative to central positions of the group
! of lines in pixel space.
STAT/TABLE {LINTAB} :X  {SESSOUTV}

XMEAN = (OUTPUTR(3) - START(1))/STEP(1) + 1.
XC    = NPIX(1)/2. + 0.5

FACTOR = XMEAN
OFFSET = (XMEAN - XC)*WDISP(1)

WRITE/OUT "XCENTER = {XC}  XMEAN = {XMEAN} OFFSET={OFFSET}"

WCENT(1) = WCENT(1) + OFFSET

COMPUTE/TABLE  {P4} :SECT   = select
COMPUTE/TABLE  {P4} :XMEAN  =  (:X - ({START(1)}))/{STEP(1)}+1.-{FACTOR}
COMPUTE/TABLE  {P4} :XNORM  =  :XMEAN/{FACTOR}
select/table {P4} :SECT.GT.0 {SESSOUTV}
nblines = outputi(1)
delete/column   {P4} :SECT

define/local wlim/D/1/2  0.,0.

define/local hg_start/D/1/3 0.,0.,0.
define/local hg_step/D/1/3  0.,0.,0.
define/local hg_npix/I/1/3  0,0,0

IF P6(1:1) .EQ. "L"  THEN
  hg_start(1) = wdisp(1) * (1.-wdisp(2))
  hg_step(1)  = wdisp(1) * wdisp(3)
  hg_npix(1)  = 2.*wdisp(2)/wdisp(3)

  hg_start(2) = wcent(1) - wcent(2)*wdisp(1)
  hg_step(2)  = wcent(3)*wdisp(1)
  hg_npix(2)  = 2.*wcent(2)/wcent(3)

  DIM = 2
  WRITE/KEYW INPUTC/C/1/10  :XMEAN
ENDIF

IF P6(1:1) .EQ. "1" THEN
      DIM = 1
      hg_start(1) = wcent(1) - wcent(2)*wdisp(1)
      hg_step(1)  = wcent(3)*wdisp(1)
      hg_npix(1)  = 2.*wcent(2)/wcent(3)
      INPUTR(2) = WDISP(1)
      WRITE/KEYW INPUTC/C/1/10 :XMEAN
ENDIF

IF P6(1:1) .EQ. "N" THEN

      DIM = 2
      INPUTR(2) = WDISP(1)*FACTOR
      WRITE/KEYW INPUTC/C/1/10  :XNORM

      hg_start(1) = -0.30
      hg_step(1)  = M$ABS(hg_start(1))/100.
      hg_npix(1)  = 200

      hg_start(2) = wcent(1) - wcent(2)*wdisp(1)
      hg_step(2)  = wcent(3)*wdisp(1)
      hg_npix(2)  = 2.*wcent(2)/wcent(3)

ENDIF

IF P6(1:1) .EQ. "3" THEN

      DIM = 3
      INPUTR(2) = WDISP(1)*FACTOR
      RANGE     = RANGE/FACTOR
      WRITE/KEYW INPUTC/C/1/10  :XNORM

      hg_start(1) = inputr(2) * (1.-wdisp(2))
      hg_step(1)  = inputr(2) * wdisp(3)
      hg_npix(1)  = 2.*wdisp(2)/wdisp(3)

      hg_start(2) = wcent(1) - wcent(2)*wdisp(1)
      hg_step(2)  = wcent(3)*wdisp(1)
      hg_npix(2)  = 2.*wcent(2)/wcent(3)

      hg_start(3) = -0.20
      hg_step(3)  = 0.01
      hg_npix(3)  = 20

ENDIF

IF DIM .EQ. 0  THEN
   WRITE/OUT "Wrong Method : {P6}"
   RETURN/EXIT
ENDIF

WRITE/KEYW INPUTC/C/10/10  :WAVE

write/keyw in_a   {P4}

IF P5(1:1) .NE. "@" THEN
   write/keyw in_b   {P5}
ELSE
   write/keyw in_b   {P4}
ENDIF

IF P8(1:1) .NE. "N" THEN
    write/keyw out_a  middummh.bdf
ELSE
    write/keyw out_a @@@
ENDIF
write/keyw out_b  {P6}

write/keyw out_b/C/10/7 "NOIDENT"

INPUTI(4) = DIM
INPUTR(4) = RANGE

copy/keyw hg_npix/I/1/3  inputi/I/1/3
copy/keyw hg_start/D/1/3 inputd/D/1/3 
copy/keyw hg_step/D/1/3  INPUTD/D/4/3

RUN STD_EXE:sphough.exe

define/local panmax/I/1/1 {OUTPUTI(5)}

conf = (outputr(1)/nblines)*100.
if conf .gt. 100. conf = 100.

IF P8(1:1) .EQ. "V"  THEN
   EXTRACT/IMAGE &y = &h[<,<,@{PANMAX}:>,>,@{PANMAX}]
   LOAD &y SCALE=5,1
ENDIF

define/local staep/D/1/3  0.,0.,0.

IF P6(1:1) .EQ. "1" THEN
   staep(1) = HG_START(1) + (OUTPUTI(3)-1)*HG_STEP(1)
   WCENTER = staep(1)  - OFFSET
ENDIF

IF P6(1:1) .EQ. "L" .OR. P6(1:1) .EQ. "N" THEN

staep(1) = HG_START(1) + (OUTPUTI(3)-1)*HG_STEP(1)
staep(2) = HG_START(2) + (OUTPUTI(4)-1)*HG_STEP(2)

IF P6(1:1) .EQ. "L" THEN
    AVDISP   = staep(1)
    BETA     = 0.
ENDIF

IF P6(1:1) .EQ. "N" THEN
    BETA     = staep(1)
ENDIF

WCENTER  = staep(2) - OFFSET

ENDIF

IF P6(1:1) .EQ. "3" THEN

staep(1) = HG_START(1) + (OUTPUTI(3)-1)*HG_STEP(1)
staep(2) = HG_START(2) + (OUTPUTI(4)-1)*HG_STEP(2)
staep(3) = HG_START(3) + (OUTPUTI(5)-1)*HG_STEP(3)

AVDISP   = staep(1)/FACTOR
WCENTER  = staep(2) - OFFSET
BETA     = staep(3)

ENDIF


SET/FORMAT F12.5
WRITE/OUT "Confidence Level: {CONF} %"
WRITE/OUT "Set values WCENTER = {WCENTER} and AVDISP = {AVDISP} and BETA={BETA}"

! Now Updates column :WAVEC in line.tbl

OUTPUTR(1) = CONF

RETURN








