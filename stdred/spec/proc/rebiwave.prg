! @(#)rebiwave.prg	19.1 (ES0-DMD) 02/25/03 14:29:20
! +++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  SPECTRAL PROCEDURE : REBIWAVE.PRG
!  J.D.Ponz			version 1.0 131083
!
! .PURPOSE
!
!  execute the command :
!  REBIN/WAVE INPUT OUTPUT [start,step,npixels] [table]
!  REBIN/WAVE INPUT OUTPUT REFERENCE            [table]
! ---------------------------------------
DEFINE/PARAM P1 ? IMAGE "Enter input image:"
DEFINE/PARAM P2 ? IMAGE "Enter output image:"
DEFINE/PARAM P3 ?   ?   "Enter reference frame or start,step,npix: "
DEFINE/PARAM P4 line.tbl TABLE "Coefficients table:"

SET/FORMAT E24.12

DEFINE/LOCAL  COEDISP/D/1/10   0.  ALL  +lower

DEFINE/LOCAL  LINTAB/C/1/60   {P4}
IF M$INDEX(LINTAB,".") .LT. 1   WRITE/KEYW LINTAB {LINTAB}.tbl
COPY/DK       {LINTAB}  GUESSD  COEDISP

IF M$TSTNO(P3) .EQ. 1 THEN
   WRITE/KEYW INPUTI/I/1/3  {P3}
   WRITE/KEYW INPUTD/D/1/3  {P3}
   CREATE/IMAGE {P2}  1,{INPUTI(3)} {INPUTD(1)},{INPUTD(2)}
   REBIN/II 'P2' 'P1' 'P2'  POL COEDISP,KEY  SPG

ELSE
   REBIN/II 'P2' 'P1' 'P3' POL  COEDISP,KEY SPG
ENDIF

COPY/DD   {P1}  *,3  {P2}

SET/FORMAT





