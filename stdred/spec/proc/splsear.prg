! @(#)splsear.prg	19.1 (ES0-DMD) 02/25/03 14:29:22
! @(#)splsear.prg	19.1  (ESO)  02/25/03  14:29:22
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       splsear.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!.KEYWORDS    Spectroscopy
!.PURPOSE     General module for PLOT/SEARCH commands
!.VERSION     1.0  Package Creation  15-FEB-1994
!-------------------------------------------------------
!
define/param P1  2D    CHAR "Mode (1D/2D)"
define/param P2  ?     TAB  "Input table:"
define/param P3 {WLC}  IMA  "Image:"
define/param P4  0     NUMB "Y Position:"
!
graph/spec
SET/GRAPH
set/graph pmode=1 font=1

define/local nrow/I/1/1  0
define/local nplot/I/1/1 {P4}

IF {{P3},NAXIS} .EQ. 1 THEN 
     nrow  = 1
ELSE
     nrow  = {{P3},NPIX(2)}
ENDIF

IF nplot .eq. 0  nplot = nrow/2 + 1

IF nrow .EQ. 1 .OR. P1(1:1) .EQ. "1" THEN
   PLOT/ROW {WLC} @{nplot}
   set/graph  color=4
   OVERPLOT/IDENT {P2} :X :X TOP
   SELECT/TABLE {P2} ALL
ELSE
   set/graph ltype=0  stype=1 
   SELECT/TABLE {P2} ALL
   plot/table {P2}  :X :Y
ENDIF

set/graph
RETURN
