!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995-2008 European Southern Observatory
!.IDENT       spcontin.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!.KEYWORDS    Spectroscopy
!.PURPOSE     Fit spectra continuum with smoothing splines
!.VERSION     1.0  Command Creation  09-MAR-1995
! 081016	last modif
!-------------------------------------------------------
!
CROSSREF  in out rad type s d

define/param P1 ? IMA "Image name:"
define/param P2 ? IMA "Out image:"
define/param P3 3      CHAR "Filter radius:"
define/param P4 ABS    CHAR "Type (ABSORPTION, EMISSION, ALL):"
define/param P5 10000. NUM  "Smoothing factor:"
define/param P6 3      NUM  "Degree:" 
define/param P7 CONT   CHAR "Output Type (CONT, NORM):"

define/local answ/C/1/3 "   " 
define/local bound1/C/1/20 " "
define/local bound2/C/1/20 " "
define/local mode/C/1/3    "MAX"
define/local back/I/1/1    0
define/local flag/I/1/1    0
define/local limit/D/1/2   0.,0.
define/local tmptab/C/1/30 spct    ! No extension .tbl here
!
! 0. Preparation of files
! 
if {P6} .GT. 5 THEN
   write/out "Degree is out of range: {P6}. Valid range is [1,5]"
   return/exit
endif

write/out "Selecting continuum points..."

if {{P1},NAXIS} .GT. 1  then
   average/row &av = {P1} <,>
else
   copy/ii {P1} &av
endif

graph/spec
set/graph
set/graph pmode=1 
plot &av

IF P4(1:2) .EQ. "AL" THEN
   set/graph  stype=1 ltype=0  ssize=1.
ENDIF

if P4(1:1) .EQ. "E" write/key mode "MIN"


IF P3(1:1) .EQ. "S"   GOTO INTER

IF P3(1:1) .EQ. "F" THEN 
   set/graph color=4
   over/table {tmptab} :X :VALUE
   GOTO FIT
ENDIF

inputi(1) = M$TSTNO(P3)
IF inputi(1) .NE. 1 THEN
   WRITE/OUT "Wrong parameter: P3 = {P3}"
   RETURN
ENDIF

!
! 1. Preliminary selection of the continuum
!
filter/median &av  &med {P3} 
filter/smooth &med &smo {P3} 

IF P4(1:2) .NE. "AL" THEN
  write/key in_a middummsmo.bdf
  write/key in_b {mode}
  write/key out_a {tmptab}.tbl
  run STD_EXE:localmax
ELSE
  COPY/IT &smo {tmptab} :X
  name/column {tmptab} #2 :VALUE
ENDIF
!
! 2. Optional interactive correction
!
INTER:
set/graph color=4
over/table {tmptab} :X :VALUE
copy/ii &smo &sm
SELECT:

write/key answ/C/1/3 " " ALL
write/key bound1 " " ALL
write/key bound2 " " ALL

inquire/keyw answ/C/1/3 "Interactive Correction (h/y/n/z/u/a/v/q, h for help): " 

back = 0
flag = 0

if answ(1:1) .eq. "H" then
   write/out "***** Help on Correction Options *****"
   write/out "   "
   write/out "h) Help:   Displays this help"
   write/out "y) Yes:    Interactive points removal wanted. 
   write/out "           Click the lower and upper bound of the region to delete"
   write/out "n) No:     No more point removal wanted. Now fit the spline.
   write/out "z) Zoom:   Interactive definition of a zoom region.
   write/out "           Click the lower and upper bound of the region to zoom"
   write/out "u) Unzoom: Unzoom spectrum."
   write/out "a) Add:    Interactively add points."
   write/out "           Click on as many points as you wish, exit with middle button"
   write/out "v) Values: Enter lower and upper bounds of the region to delete by their"
   write/out "           world coordinates. This option supports the syntax ">" (end of"
   write/out "           spectrum) and "<" (beginning of spectrum)"
   write/out "q) Quit:   Aborts the procedure at this point"
   write/out "   "
   write/out " ** Apart from No and Quit, all options go back to this menu **"
   write/out "   "
   back = 1
endif

if answ(1:1) .eq. "Z" then  ! Zoom Option
     write/out   "Enter the lower and upper position of the region to zoom:"
     CLICK2:
     get/gcursor middummc.tbl,TABLE ? 2
     if {middummc.tbl,TBLCONTR(4)} .NE. 2 then
      write/out "Please select identify 2 positions (lower, upper):"
      goto click2
     endif
     sort/table &c :X_AXIS

     set/graph
     set/graph xaxis={&c,:X_AXIS,@1},{&c,:X_AXIS,@2} yaxis={&c,:Y_AXIS,@1},{&c,:Y_AXIS,@2}  pmode=1
     PLOT &av
     set/graph color=4
     over/table {tmptab} :X :VALUE
     back = 1
     flag = 1
endif

if answ(1:1) .eq. "U" then
     set/graph
     set/graph pmode=1
     PLOT &av
     set/graph color=4
     over/table {tmptab} :X :VALUE
     back = 1
     flag = 1
endif

if answ(1:1) .eq. "A" then
     write/out "Adding points: click on a number of positions, "
     write/out "exit with the middle button"
     get/gcursor middummc.tbl,TABLE
     name/column &c :X_AXIS :X
     name/column &c :Y_AXIS :VALUE
     merge/table {tmptab} &c &hh
     sort/table  &hh :X
     -RENAME middummhh.tbl {tmptab}.tbl
     back = 1
endif

if back .eq. 1 goto select

if answ(1:1) .ne. "N" then
   if answ(1:1) .eq. "Q" return

   if answ(1:1) .eq. "V" then
     inquire/keyw bound1/C/1/20 "Lower bound:"
     inquire/keyw bound2/C/1/20 "Upper bound:"     
     flag = 1
   endif

   if answ(1:1) .eq. "Y" then
     write/out   "Enter the lower and upper position of one rejected region:"
     CLICK:
     get/gcursor middummc.tbl,TABLE ? 2
     if {middummc.tbl,TBLCONTR(4)} .NE. 2 then
      write/out "Please select identify 2 positions (lower, upper):"
      goto click
     endif
     sort/table &c :X_AXIS
     write/key bound1/C/1/20  {&c,:X_AXIS,@1} 
     write/key bound2/C/1/20  {&c,:X_AXIS,@2} 
     flag = 1
   endif

   if flag .ne. 1 goto inter
   CANCEL:  

   if bound1(1:1) .eq. "<"  then 
       limit(1) = {middummsm.bdf,START(1)} - 1.
   else
       limit(1) = {bound1}
   endif

   if bound2(1:1) .eq. ">"  then
       limit(2) = {middummsm,START(1)} + ({middummsm,NPIX(1)}+2)*{middummsm,STEP(1)}
   else
       limit(2) = {bound2}
   endif

   select/table {tmptab} :X.lt.{limit(1)}.or.:X.gt.{limit(2)}
   copy/table   {tmptab} {tmptab}s
   copy/table   {tmptab}s {tmptab}

   set/graph color=1
   plot &av
   set/graph color=4
   over/table {tmptab} :X :VALUE
   goto select
endif
!
! 3. Interpolation by a smoothing spline
!
FIT:
copy/it &av  &tcont :X
name/column   &tcont #2 :FLUX
compute/table {tmptab} :V=0.

interpolate/tt &tcont :X,:FLUX {tmptab} :X,:VALUE {P5} {P6}
delete/column  &tcont :X
copy/ti &tcont &aw
copy/dd &av * &aw

set/graph color=3
over  &aw
!
! 4. Final preparation of results.
!
if {{P1},NAXIS} .GT. 1  then
   grow/image {P2} = &aw {{P1},START(2)},{{P1},STEP(2)},{{P1},NPIX(2)}
else
   copy/ii &aw {P2}
endif
copy/dd {P1} * {P2}

IF P7(1:1) .EQ. "N" THEN  ! Output is normalized spectrum
   compute/image {P2} = {P1}/{P2}
ENDIF

return















