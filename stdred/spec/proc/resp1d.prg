! @(#)resp1d.prg	19.1 (ES0-DMD) 02/25/03 14:29:20
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! . IDENTIFICATION
!
! MIDAS COMMAND PROCEDURE : RESP1D.PRG
!
! D. Baade, ST-ECF, Garching           version 1.0               281085
! M. Peron  910502 
! K. BAnse  920413
! .PURPOSE
!
! execute the command :
!
! RESPONSE/1D response observed [fluxtable] [mode] [batch_flag] [method] [deg]
! or
! RESPONSE/1D response observed [fluxtable] [mode] [batch_flag] [method] [s,deg]
!
!
! response:    image to hold response curve
! observed:    image with standard star observations
! fluxtable:   table with wavelengths, fluxes, and binwidths of standard data
! mode:        START - get positions and bin widths from "fluxtable",
!		       integrate in "observed" over corresponding bins
!              DELETE  - delete points from table RESP1D (with cursor)
!              ADD     - add new points to table RESP1D (with cursor)
! batch_flag:  if equal to Y suppress all plots
! method    :  SPLINE   use spline approximation
!              POLY     use polynomial approximation
! deg       :  degree of the polynomial
!
!-------------------------------------------------------------------------------
!
DEFINE/PARAM P1 ?       IMAGE "Enter input image"
DEFINE/PARAM P2 ?       IMAGE "Enter output image"
DEFINE/PARAM P3 RESP1D  TABLE
DEFINE/PARAM P4 S       C
DEFINE/PARAM P5 N       C
DEFINE/PARAM P6 SPLINE  C
DEFINE/PARAM P7 1,3     N
!
IF P4(1:1) .EQ. "A"  GOTO ADD
IF P4(1:1) .EQ. "D"  GOTO DELETE
IF P3(1:6) .EQ. "RESP1D" THEN
   WRITE/OUT "In this mode you can't default the flux table !"
   RETURN
ENDIF
!
START:			! 1st case: start from scratch
!
IF P5(1:1) .NE. "Y" PLOT/ROW {P2}
WRITE/OUT "This is the observed spectrum of the standard star ..."
WRITE/KEYW NFIMAGE/C/1/60 {P2}
WRITE/KEYW NFTABLE/C/1/60 "RESP1D "
COPY/TABLE  {P3} RESP1D
CREATE/COLUMN RESP1D :Y_AXIS
RUN STD_EXE:INTEGBIN
NAME/COLUMN RESP1D :Y_AXIS :OBSERVED
IF P5(1:1) .NE. "Y" OVERPLOT/TABLE RESP1D :X_AXIS :OBSERVED
WRITE/OUT -
  "... and the points derived for the determination of the response curve"
COMP/TABLE RESP1D :Y_AXIS = :OBSERVED/:FLUX		! calculate response
IF P5(1:1) .NE. "Y" PLOT/TABLE RESP1D :X_AXIS :Y_AXIS
WRITE/OUT "Here are the individual response data - next comes the fit"
GOTO FIT
!
DELETE: 	! 2nd case: delete points from previously created table RESP1D
!
PLOT/TABLE RESP1D :X_AXIS :Y_AXIS
IDENT/GCURS RESP1D :X_AXIS :X_AXIS :Y_AXIS 5
GOTO FIT
!
ADD: 		! 3rd case: add points to previoulsy created table RESP1D
!
PLOT/TABLE RESP1D :X_AXIS :Y_AXIS
GET/GCURS RESPDUMM
WRITE/KEYW LNROWOL/I/1/1 0
WRITE/KEYW LNROWNE/I/1/1 0
WRITE/KEYW X_P/R/1/1 0.
WRITE/KEYW Y_P/R/1/1 0.
COPY/DK RESP1D.tbl TBLCONTR/I/4/1 LNROWOL/I/1/1	    ! number of rows in RESP1D
COPY/DK RESPDUMM.tbl TBLCONTR/I/4/1 LNROWNE/I/1/1	    ! number of rows in RESP
DEFINE/LOCAL I/I/1/1 0
DEFINE/LOCAL LNROW/I/1/1 0
DO I = 1 {LNROWNE}
   COPY/TK RESPDUMM :X_AXIS @{I} X_P/R/1/1
   COPY/TK RESPDUMM :Y_AXIS @{I} Y_P/R/1/1
   COMPUTE/KEYW LNROW = LNROWOL+I
   WRITE/TABLE RESP1D :X_AXIS @{LNROW} {X_P}
   WRITE/TABLE RESP1D :Y_AXIS @{LNROW} {Y_P}
ENDDO
DELETE/TABLE RESPDUMM NO
!
FIT: 				! do the spline fit
!
SORT/TABLE RESP1D :X_AXIS
IF P6(1:1) .EQ. "S" THEN
  IF PCOUNT .EQ. 7 THEN
    CONVERT/TABLE {P1} = RESP1D :X_AXIS :Y_AXIS {P2} SPLINE {P7}
  ELSE
    CONVERT/TABLE {P1} = RESP1D :X_AXIS :Y_AXIS {P2} SPLINE
  ENDIF
ELSE
  CONVERT/TABLE {P1} = RESP1D :X_AXIS :Y_AXIS {P2} POLY {P7}
ENDIF
IF P5(1:1) .NE. "Y" OVER {P1}
