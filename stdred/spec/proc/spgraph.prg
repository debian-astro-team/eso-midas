! @(#)spgraph.prg	19.1 (ES0-DMD) 02/25/03 14:29:21
! @(#)spgraph.prg	19.1  (ESO)  02/25/03  14:29:21
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       lngraph.prg
!.AUTHORS     Pascal Ballester (ESO/Garching) 
!             Cristian Levin   (ESO/La Silla)
!.KEYWORDS    Spectroscopy, Long-Slit
!.PURPOSE     
!               internal procedure of Spectra for creating 
!               the graphic window.
!               Command:
!                          GRAPH/SPECTRA
!
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
DEFINE/PARAM P1  800,350  NUMB  "Graph size     : "
DEFINE/PARAM P2  1,1      NUMB  "Graph position : "
DEFINE/PARAM P3  0        NUMB  "Graph id       : " 
!
log(4) = 2
crea/graph {P3} {P1},{P2}
set/gcur ? c_h
log(4) = 0
!
