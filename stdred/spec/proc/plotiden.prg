! @(#)plotiden.prg	19.1 (ES0-DMD) 02/25/03 14:29:19
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! ECHELLE PROCEDURE : PLOTIDEN.PRG
! J.D.Ponz			version 1.0 131083
!                                       2.0 240686
!
! .PURPOSE
!
! execute the command :
! PLOT/IDENT arc_ref [line]
! -------------------------------------------------
DEFINE/PARAM P1 ? IMAGE "Enter input image:"
DEFINE/PARAM P2 line TABLE
DEFINE/PARAM P3 :X    ?
DEFINE/PARAM P4 :WAVE ?
DEFINE/PARAM P5 TOP   C "TOP/BOTTOM"
PLOT/ROW 'P1'
OVERPLOT/IDENT   'P2' 'P3' 'P4' 'P5'
