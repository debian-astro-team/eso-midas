! @(#)sphst.prg	19.1 (ESO-IPG) 02/25/03 14:29:22
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1996 European Southern Observatory
!.IDENT       sphst.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!.KEYWORDS    Spectroscopy
!.PURPOSE     Read HST spectra (FITS format to Midas table)
!.VERSION     1.0  Creation  14-JAN-1996
!-------------------------------------------------------
!
DEFINE/PARAM P1 ? CHAR "Name of wavelength FITS file"
DEFINE/PARAM P2 ? CHAR "Name of flux FITS file"
define/param P3 ? TAB  "Name of result table:"

write/out "Converting wavelength FITS file"
indisk/fits {P1}  &w
write/out "Converting flux FITS file"
indisk/fits {P2}  &f

copy/it &w &tw 
copy/it &f &tf

define/local last/I/1/1 0
define/local ax/I/1/1 {middummf.bdf,NAXIS}

if ax .eq. 1  then
    last = 1
else
    last = {middummf.bdf,NPIX(2)}
endif

name/column &tf #{last} :FLUX
copy/tt     &tw #{last} &tf :WAVE

define/local name/C/1/80 {P3}
define/local ind/I/1/1   0
ind = M$INDEX(name,".tbl")
if ind .le. 0 write/key name {name}.tbl

copy/table &tf  {name}
copy/dd  &f *,3 {name}

write/out "Created table: {name}"

return





