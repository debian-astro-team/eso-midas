! @(#)spcumhist.prg	19.1 (ES0-DMD) 02/25/03 14:29:21
define/param P1 ? IMA "Input histogram:"
define/param P2 ? IMA  "Output cumulated histogram :"
define/param P3 ABSORPTION CHAR "Mode (ABS/EMISS)"

write/key in_a {P1}
write/key out_a {P2}
write/key in_b  {P3}

RUN STD_EXE:spcumhist

return
