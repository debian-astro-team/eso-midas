! @(#)spemerge.prg	19.1 (ES0-DMD) 02/25/03 14:29:21
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       spemerge.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle,
!.PURPOSE     Command MERGE/SPECTRUM
!.VERSION     1.0    Creation  12-AUG-1992
!
!-------------------------------------------------------
!
DEFINE/PARAM  P1  ?         IMA   "Spectrum 1:"
DEFINE/PARAM  P2  ?         IMA   "Spectrum 2:"
DEFINE/PARAM  P3  ?         IMA   "Output name:"
DEFINE/PARAM  P4  5         NUM   "Interval (in pixels):"
DEFINE/PARAM  P5  CONSTANT  CHAR  "Merging mode (RAMP, CONSTANT, OPTIMAL):"
DEFINE/PARAM  P8  CCODE     CHAR  "Option for Optimal: CCODE or PROC"
!
IF P5(1:1) .EQ. "O" .AND. P8(1:1) .EQ. "C" THEN
   WRITE/KEY IN_A {P1}
   WRITE/KEY IN_B {P2}
   WRITE/KEY OUT_A {P6}
   WRITE/KEY OUT_B {P7}
   WRITE/KEY INPUTC {P3}
   INPUTI(1) = {P4}
   RUN STD_EXE:spmerge
   RETURN
ENDIF
!
DEFINE/LOCAL  STEP/D/1/2    0.,0.
DEFINE/LOCAL  START/D/1/2   0.,0.
DEFINE/LOCAL  END/D/1/2     0.,0.
DEFINE/LOCAL  NPIX/I/1/3    0,0,0
DEFINE/LOCAL  TEMPD/D/1/1   0.
DEFINE/LOCAL  TEMPI/I/1/1   0
DEFINE/LOCAL  TEMPC/C/1/60  XXX
DEFINE/LOCAL  SPEC1/C/1/60  {P1}
DEFINE/LOCAL  SPEC2/C/1/60  {P2}
DEFINE/LOCAL  INTERV/I/1/1  {P4}
!
COPY/DK {P1}  STEP/D/1/1    STEP/D/1/1
COPY/DK {P2}  STEP/D/1/1    STEP/D/2/1
COPY/DK {P1}  START/D/1/1   START/D/1/1
COPY/DK {P2}  START/D/1/1   START/D/2/1
COPY/DK {P1}  NPIX/I/1/1    NPIX/I/1/1
COPY/DK {P2}  NPIX/I/1/1    NPIX/I/2/1

IF STEP(1) .NE. STEP(2) THEN
   WRITE/OUT "Error: Spectra {P1} and {P2} must have the same step"
   RETURN/EXIT
ENDIF

IF START(2) .LT. START(1) THEN
   TEMPD    = START(2)
   START(2) = START(1)
   START(1) = TEMPD
   TEMPI    = NPIX(2)
   NPIX(2)  = NPIX(1)
   NPIX(1)  = TEMPI
   WRITE/KEYW  TEMPC  {SPEC2}
   WRITE/KEYW  SPEC2  {SPEC1}
   WRITE/KEYW  SPEC1  {TEMPC}
ENDIF

END(1)   = START(1) + (NPIX(1)-1)*STEP(1)
END(2)   = START(2) + (NPIX(2)-1)*STEP(2)
NPIX(3) = (END(2) - START(1))/STEP(1) + 1
CREATE/IMAGE {P3}  1,{NPIX(3)}  {START(1)},{STEP(1)}

DEFINE/LOCAL  LIMIT/I/1/2  0.,0.
LIMIT(1) = INTERV
LIMIT(2) = {{SPEC1},NPIX(1)}   - INTERV
EXTRACT/IMAGE  &a = {SPEC1}[@{LIMIT(1)}:@{LIMIT(2)}]
INSERT/IMAGE &a {P3}

LIMIT(2) = {{SPEC2},NPIX(1)}  - INTERV
EXTRACT/IMAGE  &b = {SPEC2}[@{LIMIT(1)}:@{LIMIT(2)}]
INSERT/IMAGE &b {P3}

IF START(2) .GE. END(1) THEN
   WRITE/OUT "Warning: No overlap between the spectra"
ELSE
   IF P5(1:1) .EQ. "R" THEN   ! Method RAMP
      COMPUTE/IMAGE  &o = &a+&b
      CREATE/IMAGE   &r   1,{&o,NPIX(1)}  {&o,START(1)},{&o,STEP(1)} POLY  0.,1.
      COMPUTE/IMAGE  &r   = &r - {&r[@1]}
      COMPUTE/IMAGE  &r   = &r / {&r[@{&r,NPIX(1)}]}
      COMPUTE/IMAGE  &o = &a*(1.-&r) + &b*&r
   ENDIF
   IF P5(1:1) .EQ. "O" THEN   ! Method OPTIMAL
      COMPUTE/IMAGE &v = {P6}*{P6}
      COMPUTE/IMAGE &w = {P7}*{P7}
      COMPUTE/IMAGE &o = (&a*&v + &b*&w)/(&v+&w)
   ENDIF
   IF P5(1:1) .EQ. "C" THEN   ! Method CONSTANT
      COMPUTE/IMAGE &o = (&a+&b)/2.
   ENDIF
   INSERT/IMAGE   &o   {P3}
ENDIF

RETURN

!********************************************************** 

ENTRY histo

DEFINE/PARAM P1  ?  IMA  "Enter input/output image"
DEFINE/PARAM P2  0   N   "Reference order"
DEFINE/PARAM P3  ?   N   "Bin size"

DEFINE/LOCAL NORD/I/1/1   {{P1},NPIX(2)}
DEFINE/LOCAL ORD/I/1/1    0
DEFINE/LOCAL ORD1/I/1/1   0
DEFINE/LOCAL FACTOR/D/1/1 0.
DEFINE/LOCAL LOOP/I/1/1   0
DEFINE/LOCAL BIN/R/1/1    {P3}

DEFINE/LOCAl POSMAX/I/1/1 0
DEFINE/LOCAl POS0/I/1/1 0
DEFINE/LOCAl POS1/I/1/1 0
DEFINE/LOCAL COEF0/R/1/1  0
DEFINE/LOCAL COEFM/R/1/1  0
DEFINE/LOCAL COEF1/R/1/1  0
DEFINE/LOCAL SCOEF/R/1/1  0
DEFINE/LOCAL POSGRAV/R/1/1 0
DEFINE/LOCAL CORD/I/1/1   {P2}

IF NORD .EQ. 1   RETURN 


IF CORD .EQ. 0    CORD = NORD/2
COPY/IT   {P1}   &t

! SET/GRAPH  yaxis  xaxis=0.,2.

DO ORD =  1 {NORD}

   IF ORD .NE. CORD  THEN

   COMP/TABLE  &t  :AUX = #{ORD}/#{CORD}
   COMP/HISTO &a = middummt.tbl  :AUX  {BIN}  0.1  10.
   STAT/IMAGE   &a
   POSMAX = OUTPUTI(3)
   POS0   = POSMAX - 1
   IF POS0 .LT. 1  POS0 = 1
   POS1   = POSMAX + 1
   COEF0  = {&a[@{POS0}]}
   COEFM  = {&a[@{POSMAX}]}
   COEF1  = {&a[@{POS1}]}
   SCOEF  = COEF0 + COEFM + COEF1
   POSGRAV = (POS0*COEF0+POSMAX*COEFM+POS1*COEF1)/SCOEF
   FACTOR = (POSGRAV-1)*{middumma,STEP(1)}+{middumma,START(1)}
   WRITE/OUT "Order {ORD}. Normalization {FACTOR}"
   COMP/TABLE   &t  #{ORD} = #{ORD} / {FACTOR}

   
   ENDIF

ENDDO

DELETE/COLUMN &t  :AUX
COPY/TI  &t   &u
COPY/DD  {P1}  *,1  &u
COPY/II  &u    {P1}
COPY/DD  &u    *,1  {P1}

RETURN

