! @(#)flux1d.prg	19.1 (ES0-DMD) 02/25/03 14:29:18
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! . IDENTIFICATION
!
! MIDAS procedure : flux1d.prg
!
! D. Baade, ST-ECF, Garching           version 1.0              851028
! KB								901108
!
! .PURPOSE
!
! execute the command :
! FLUX/1D calibrated observed response [flag] [airmass]
!
! calibrated:  file to hold calibrated spectrum
! observed:    image with observed spectrum
! response:    image with instrumental response curve
! flag:        if first position equal to N, extinction will not be corrected fo
!              if second position equal to N, wavelengths in nanometer,
!                                                        else Angstrom units
! airmass:     airmass (to correct for atmospheric extinction)
!
!-------------------------------------------------------------------------------
!
define/param p1 ? IMA "Enter name for calibrated spectrum:"
define/param p2 ? IMA "Enter name of observed spectrum:"
define/param p3 ? IMA "Enter name instrumental response curve:"
define/param p4 YA
define/param p5 1. ? "Enter airmass:"
define/maxpar 5
!
if p4(1:1) .ne. "N" then
   if p4(2:2) .eq. "N" then
      extinction {p2} exdummy {p5} atmoexnm
   else
      extinction {p2} exdummy {p5} atmoexan
   endif
   compute/image {p1} = exdummy/{p3}
   delete/image exdummy NO
   return
endif
!
compute/image {p1} = {p2}/{p3}
