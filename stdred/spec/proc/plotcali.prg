! @(#)plotcali.prg	19.1 (ES0-DMD) 02/25/03 14:29:19
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! ECHELLE PROCEDURE : PLOTCALI.PRG
! J.D.Ponz			version 1.0 131083
!
! .PURPOSE
!
! execute the commands :
! PLOT/CALI  [line]
! PLOT/RESI  [line]
! -------------------------------------------------
DEFINE/PARAM P1 line TABLE
!
DEFINE/LOCAL QUALIF/C/1/8  {MID$CMND(11:18)}
DEFINE/LOCAL SAVE/I/1/3 0,0,0
!
SAVE(1) = PLISTAT(2)
SAVE(2) = PLISTAT(3)
SAVE(3) = PLISTAT(4)
SET/GRAPH STYPE=5 SSIZE=2
IF QUALIF(1:1) .EQ. "C" THEN
   PLOT/TABLE   'P1' :WAVEC :DELTAC
   SET/GRAPH STYPE=0 LTYPE=1
   OVERPLOT/TABLE 'P1' :WAVEC :DELTA  -1
ELSE
   PLOT/TABLE   'P1' :WAVEC :RESIDUAL
ENDIF
PLISTAT(2) = SAVE(1)
PLISTAT(3) = SAVE(2)
PLISTAT(4) = SAVE(3)
