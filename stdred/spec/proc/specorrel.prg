! @(#)specorrel.prg	19.1 (ES0-DMD) 02/25/03 14:29:21
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       specorrel.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle,
!.PURPOSE     Command CORRELATE/SPECTRUM
!.VERSION     1.0    Creation  12-AUG-1992
!
!-------------------------------------------------------
!
DEFINE/PARAM  P1  ?            TABLE  "Table 1:"
DEFINE/PARAM  P2  ?            TABLE  "Table 2:"
DEFINE/PARAM  P3  1.           NUMB   "Pixel size:"
DEFINE/PARAM  P4  0,0.05,5,0.1 NUMB   "Center, Tol, Range, Step"
DEFINE/PARAM  P5  X,Y,+        CHAR   "Positions, Reference, Weights"
DEFINE/PARAM  P6  +            CHAR   "Reference  value"
DEFINE/PARAM  P7  middummx.bdf IMA    "Name of output image:"

WRITE/KEYW   IN_A   {P1}
WRITE/KEYW   IN_B   {P2}
WRITE/KEYW   OUT_A  {P7}

DEFINE/LOCAL INDX/I/1/1   0  ! Position of the comas in P5
DEFINE/LOCAL PAR5/C/1/80  {P5}
INDX = M$INDEX(PAR5,",") - 1
WRITE/KEYW  INPUTC  {PAR5(1:{INDX})}
INDX = INDX + 2
WRITE/KEYW  PAR5   {PAR5({INDX}:)}
INDX = M$INDEX(PAR5,",") - 1
WRITE/KEYW   INPUTC/C/21/20  {PAR5(1:{INDX})}
INDX = INDX + 2
WRITE/KEYW   INPUTC/C/41/20  {PAR5({INDX}:)}
!
WRITE/KEYW   INPUTD/D/1/1   {P3}
WRITE/KEYW   INPUTR/R/1/4   {P4}
!
INPUTR(2) = INPUTR(2)*INPUTD(1)
INPUTR(3) = INPUTR(3)*INPUTD(1)
INPUTR(4) = INPUTR(4)*INPUTD(1)
!
IF M$TSTNO(P6) .EQ. 1 THEN
   WRITE/KEYW INPUTC/C/61/1  "+"     ! Provided a reference value
   WRITE/KEYW INPUTD/D/1/2   {P6},{P6}
ELSE
   WRITE/KEYW INPUTC/C/61/1  "-"
ENDIF
!
RUN STD_EXE:specorrel.exe
! Position of the correlation peak is found is OUTPUTR(1)
!
RETURN





