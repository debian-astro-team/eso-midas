!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994-2008 European Southern Observatory
!.IDENT       spverif.prg
!.AUTHORS     Pascal Ballester (ESO/Garching)
!.KEYWORDS    Spectroscopy,
!.PURPOSE     Locates calibration table in standard directories
!.VERSION     1.0  Command Creation  09-JAN-1995
! 080322	last modif
!-------------------------------------------------------
!
define/param P1  ?      CHAR  "File name:"
define/param P2  +++    CHAR  "Calibration directory:"
define/param P3  ?      CHAR  "Session keyword:"
define/param P4 TAB     CHAR  "File type"

define/local name/C/1/80 {P1}

define/local modif/I/1/1 0
define/local index/I/1/1 0

if p4(1:1) .eq. "T" then
   if m$ftset(p1) .eq. 0 then
      write/keyw name {p1}.{mid$types(9:16)}
      modif = 1
   endif
endif

index = m$exist(name) 
if index .le. 0 .and. P2(1:1) .ne. "+" then 
   write/key name {P2}:{name}
   modif = 1
endif

index = m$exist(name) 
if index .le. 0 then
   write/out "Error: wrong session keyword {P3} = {P1}"
   write/out "Could not find file: {P1} in local directory,"
   if P2(1:1) .ne. "+" write/out  "nor in calibration directory: {P2}"
   return/exit
else
   if modif .ne. 0 write/out "Updated keyword {P3} = {name}"
   write/key {P3} {name}
endif

