! @(#)fripple.prg	19.1 (ES0-DMD) 02/25/03 14:29:18
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! . IDENTIFICATION
!
! MIDAS COMMAND PROCEDURE : FRIPPLE.PRG
!
! D. Baade, ST-ECF, Garching           version 1.0                 291085
!                                      version 1.1  P. Ballester   220291
!  KB  920401
!
! .PURPOSE
!
! execute the command :
!
! FILTER/RIPPLE inimage outimage period [start,end]
!
! inimage:   input frame
! outimage:  output frame
! period:    ripple period
! start,end: first and last pixel of range over which analysis is to be performe
!
!-------------------------------------------------------------------------------
!
DEFINE/PARAM P1 ? IMA    "Enter input image:"
DEFINE/PARAM P2 ? IMA    "Enter output image:"
DEFINE/PARAM P3 ? NUMBER "Enter period:"
DEFINE/LOCAL  DELTA/R/1/1  0.0
!
WRITE/KEYW HISTORY "FILTER/RIPPLE "
WRITE/KEYW IN_A {P1}
WRITE/KEYW OUT_A {P2}
WRITE/KEYW INPUTR/R/1/1 {P3}
!
IF P4(1:1) .EQ. "?" THEN
   WRITE/KEYW INPUTI 1,0
   COPY/DK {P1} NPIX/I/1/1 INPUTI/I/2/1
ELSE
   WRITE/KEYW INPUTI/I/1/2 {P4}
ENDIF
! 
! Check consistency of input data    910222
! 
IF INPUTI(2) .LE. INPUTI(1) THEN
   WRITE/OUT  "Condition End > Start not fulfilled"
   RETURN/EXIT
ELSEIF INPUTR(1) .LT. 1. THEN
   WRITE/OUT  "Period must be larger than 1. pixel"
   RETURN/EXIT
ENDIF
! 
DELTA = INPUTI(2) - INPUTI(1)
! 
IF DELTA .LE. INPUTR(1) THEN
   WRITE/OUT "Condition (End-Start)>Period not fulfilled"
   RETURN/EXIT
ENDIF
! 
! Calls program
RUN  STD_EXE:FRIPPLE
