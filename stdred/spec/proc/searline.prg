! @(#)searline.prg	19.1 (ES0-DMD) 02/25/03 14:29:20
!  @(#)searline.prg	19.1 (ESO) 02/25/03 14:29:20
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       searline.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Spectroscopy, Echelle,
!.PURPOSE     Execute the command :
!             SEARCH/LINE  input W,TH,aver table method type
!             method = MAXIMUM
!                      GRAVITY
!                      GAUSSIAN (default)
!             type   = EMISSION (DEFAULT)
!                      ABSORPTION
!
!.VERSION     1.0    Creation  20-MAR-1984, JDP
!
!-------------------------------------------------------
!
DEFINE/PARAM P1 ?       IMAGE  "Enter input image:"
DEFINE/PARAM P2 ?       NUMBER "Enter window,threshold,naver:"
DEFINE/PARAM P3 line    TABLE
DEFINE/PARAM P4 GAUSSIAN  C
DEFINE/PARAM P5 EMISSION C
WRITE/KEYW INPUTR/R/1/3 0.,0.,0.
WRITE/KEYW INPUTR/R/1/3 'P2'
RUN STD_EXE:SPESEARCH
CREATE/COLUMN  'P3'  :IDENT  R*8   F15.7


