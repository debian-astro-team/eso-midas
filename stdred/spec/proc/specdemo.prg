! @(#)specdemo.prg	19.1 (ESO-DMD) 02/25/03 14:29:21
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!      Demo. procedure for CES reduction. Name : CESDEMO.PRG
!      Execute as TUTORIAL/SPEC
!      The following files are copied into your directory :
!           CESFF.BDF     -- Flat Field
!           CESWLC.BDF    -- Comparison
!           CESWLCA.BDF   -- Reference
!           CESRON.BDF    -- Reticon Readout noise
!           CESOBJ.BDF    -- Object
!           THORCES.TBL   -- Comparison LINEs
!    -  The reduction requires cursor interaction to identify
!       four LINEs.
!       Use Wavelengths: 6537.6138 (51.58)
!                        6584.3721 (1583.55)
!                        6584.6128 (1591.61)
!
!    -  Files will be deleted after demo.
!
!      J.D.Ponz 		ESO - Munich     Feb. 12 , 1986
!      P. Ballester             ESO - Munich     Apr. 25 , 1991
!
! ---------------------------------------------------------------
!
@ creifnot  1       ! Creates a graphic window
!
WRITE/OUT Images are copied into your directory
intape/fits 1 toto MID_TEST:cesff.fits NOC
-rename toto0001.bdf  cesff.bdf
intape/fits 1 toto MID_TEST:ceswlca.fits NOC
-rename toto0001.bdf  ceswlca.bdf
intape/fits 1 toto MID_TEST:ceswlc.fits NOC
-rename toto0001.bdf  ceswlc.bdf
intape/fits 1 toto MID_TEST:cesron.fits NOC
-rename toto0001.bdf  cesron.bdf
intape/fits 1 toto MID_TEST:cesobj.fits NOC
-rename toto0001.bdf  cesobj.bdf
indisk/fits MID_TEST:thorces.tfits  catalog.tbl >Null
select/table catalog all
!
ECHO/ON
SET/GRAPH BIN=ON XAXIS=AUTO YAXIS=AUTO PMODE=1
ASSIGN/GRAPH T NOFILE
!
ECHO/OFF
WRITE/OUT Compute 1D scans:
ECHO/ON
AVERAGE/ROW ceswlc1 = ceswlc
AVERAGE/ROW cesron1 = cesron
AVERAGE/ROW cesff1  = cesff
!
ECHO/OFF
WRITE/OUT Subtract readout noise from comparison:
ECHO/ON
COMPUTE/IMAGE ceswlc2 = ceswlc1-cesron1
CUTS          ceswlc2 0,2000
!
ECHO/OFF
WRITE/OUT Search lines:
ECHO/ON
SEARCH/LINE ceswlc2 3,8 line GAUSSIAN
!
ECHO/OFF
WRITE/OUT "Identify the five brightest lines:"
WRITE/OUT "X   =  588.061   1343.83    1568.40    1723.88    1823.21"
WRITE/OUT "WAV.=  6554.160  6577.214   6583.906   6588.539   6591.484"
WRITE/OUT 
ECHO/ON
PLOT/ROW      ceswlc2
IDENTIFY/LINE      line
!
ECHO/OFF
WRITE/OUT Compute dispersion coefficients:
ECHO/ON
CALIBRATE/LINE 1,2  line  catalog
! COPY/GRAPH LASER
!
NAME/COLUMN line :IDENT F10.4
NAME/COLUMN line :WAVE  F10.4
NAME/COLUMN line :WAVEC F10.4
NAME/COLUMN line :RESIDUAL F10.4
CLEAR/GRAPHIC
!
ECHO/OFF
WRITE/OUT Display table LINE
ECHO/OFF
READ/TABLE       line :X :IDENT :WAVE :WAVEC :RESIDUAL
STATISTICS/TABLE line  :RESIDUAL
PLOT/TABLE       line :WAVE :RESIDUAL
! COPY/GRAPH  LASER
!
ECHO/OFF
WRITE/OUT Display identifications:
ECHO/ON
SELECT/TABLE line  :WAVE.GT.0
PLOT/IDENT ceswlc2  line
! COPY/GRAPH  LASER
SELECT/TABLE line  ALL
!
ECHO/OFF
WRITE/OUT Sample in wavelengths:
ECHO/ON
REBIN/WAVE ceswlc2 ceswlc3 ceswlca
WRITE/DESC ceswlc3 IDENT/C/1/72 "CES-RETICON WAV.COMP."
!
ECHO/OFF
WRITE/OUT Reduce object:
ECHO/ON
COMPUTE/IMAGE cesobj1  = (cesobj-cesron1)/(cesff1-cesron1)
REBIN/WAVE    cesobj1 cesobj2 ceswlca
WRITE/DESC    cesobj2 IDENT/C/1/72 "CES-RETICON DEL CEN"
!
ECHO/OFF
WRITE/OUT Display results:
ECHO/ON
CUTS cesobj2 0,2
SET/GRAPH XAXIS=6535,6595,10,2 YAXIS=0,1.5,0.5,0.1
PLOT/ROW cesobj2
! COPY/GRAPH LASER
SET/GRAPH XAXIS=AUTO YAXIS=AUTO
!
ECHO/OFF
