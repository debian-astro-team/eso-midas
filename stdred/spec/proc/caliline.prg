! @(#)caliline.prg	19.1 (ES0-DMD) 02/25/03 14:29:18
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! SPECTRAL PROCEDURE : CALILINE.PRG
! J.D.Ponz			version 2.0 130386
!   M.Peron                                 051190
!   P.Ballester  (Guess option)             221091
! .PURPOSE
!
! execute the command :
! CALIBRATE/LINE ERROR,DEGREE TABLINE CATALOGUE [MODE]
!
! 020913	last modif
! ------------------------------------------------------------
DEFINE/PARAM P1 ?           NUMBER   "Enter error,degree:"
DEFINE/PARAM P2 line        TABLE    "Line table"
DEFINE/PARAM P3 CATALOG     TABLE    "Line catalogue"
DEFINE/PARAM P4 IDENT         ?      "Mode (IDENT, MODI, GUESS)"
DEFINE/PARAM P5 line.tbl    TABLE    "Name of guess table:"
!DEFINE/PARAM P6  ?         NUMB     "Shift in pixels:"

DEFINE/LOCAL I/I/1/2          {P1}
DEFINE/LOCAL XERROR/D/1/2      {P1}
DEFINE/LOCAL TOL/D/1/1        0.
DEFINE/LOCAL LINTAB/C/1/60    {P2}
DEFINE/LOCAL TMODE/C/1/20      {P4}
!
WRITE/KEYW    DISPCOE/D/1/20   0.    ALL

IF M$INDEX(LINTAB,".") .LE. 0  WRITE/KEYW LINTAB  {LINTAB}.tbl
IF XERROR(1) .LT. 0. THEN
   TOL = M$ABS(XERROR(1))
ELSE
   IF M$EXISTD(LINTAB,"PIXEL") .NE. 1 THEN
      WRITE/OUT   "Created descriptor PIXEL in table {LINTAB}. Set to 1."
      WRITE/DESCR {LINTAB}  PIXEL/D/1/1 1.
   ENDIF
   TOL = {{LINTAB},PIXEL(1)}*XERROR(1)
ENDIF

IF P4(1:1) .EQ. "I" THEN ! Mode IDENT
  WRITE/KEYW    IN_A    {TMODE}
  INPUTR(1) = TOL
  INPUTR(2) = I(2)
  RUN STD_EXE:CALILINE
  COPY/KK      OUTPUTD/D/1/20 DISPCOE/D/1/20
ENDIF

IF P4(1:1) .EQ. "G" THEN   !  Mode GUESS
   IF P6(1:1) .EQ. "?" THEN
      CORREL/LINE  {P5}  {LINTAB}
      INPUTD(1) = OUTPUTR(1)
      WRITE/OUT "Estimate shift by cross-correlation : {OUTPUTR(1)}"
   ELSE
      INPUTD(1) = {P6}
   ENDIF
   WRITE/KEYW    IN_A {TMODE}
   WRITE/KEYW  IN_B   {P5}
   IF M$INDEX(IN_B,".") .LT. 1  IN_B = "{IN_B}.tbl"
   INPUTR(1) = TOL
   INPUTR(2) = I(2)
   RUN STD_EXE:CALILINE
   COPY/KK      OUTPUTD/D/1/20   DISPCOE/D/1/20
ENDIF


IF P4(1:1) .EQ. "M" THEN   !  Mode MODIFICATION
  WRITE/KEYW    IN_A        {TMODE}
  WRITE/KEYW INPUTR/R/1/2   0.,0.
  WRITE/KEYW INPUTR/R/1/2   'P1'
  COPY/KK   INPUTR/R/1/2   I/I/1/2
  IDENTIFY/GCURSOR 'LINTAB' :WAVE :WAVEC 30
  REGR/POLY  'LINTAB'  :X :WAVE 'I(2)'
  COPY/KK    OUTPUTD/D/1/20 DISPCOE/D/1/20
  REGR/POLY  'LINTAB'  :WAVE :X 'I(2)'
  SAVE/REGR  'LINTAB'  COEF
  WRITE/DESC 'LINTAB'  COEFS/I/1/2 'I(2)',1
  INPUTI(5) = I(2) + 1
  WRITE/DESC 'LINTAB'     COEFI/I/1/1 'INPUTI(5)'
  COMP/REGR  'LINTAB'    :WAVEC = COEF
  COMP/TABLE   'LINTAB'    :RESIDUAL = :WAVE-:WAVEC
  REGR/POL   'LINTAB'    :WAVEC :X 1
  SAVE/REGR  'LINTAB'    LINE
  COMP/REGR  'LINTAB'    :LINE = LINE
  COMP/TABLE   'LINTAB'    :DELTA = :LINE-:WAVEC
  COMP/TABLE   'LINTAB'    :DELTAC = :LINE-:WAVE
ENDIF

INPUTI(6)  = I(2)
COPY/KD   INPUTI/I/1/6  'LINTAB'  GUESSI
COPY/KD   DISPCOE       'LINTAB'  GUESSD
SET/GRAPH XAXIS=AUTO YAXIS=AUTO
PLOT/CALIBRATION 'LINTAB'


RETURN
