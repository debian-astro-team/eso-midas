! @(#)progress.prg	19.1 (ES0-DMD) 02/25/03 14:29:20
!
! Procedure: progress.prg
! Author:    P. Ballester - ESO-Garching
! Date:      21/06/94
! 
! Purpose:   Robust regression based on Leroy's book:
!            "Robust regression and outliers rejection"
!            Wiley and Sons, New-York, 1987
!
define/param P1 ?            TAB  "Input table:"
define/param P2 ?            CHAR "Dependent variable:"
define/param P3 ?            CHAR "Independent variables:"
define/param P4 progress.out CHAR "ASCII Output File:"
define/param P5 :WCALC       CHAR "Output column:"
define/param P6 :RESIDUAL    CHAR "Residual column:"
define/param P7 Y            CHAR "Constant term? Y/N:"

write/key in_a   {P1}
write/key inputc {P2}
write/key in_b   {P3}
write/key out_a  {P4}
write/key outputc/C/1/17 {P5}
write/key outputc/C/18/17 {P6}
write/key action   {P7}

define/local nind/I/1/1  1
define/local index/I/1/1 0
define/local pos/I/1/1   1
define/local string/C/1/80 " " 
define/local pind/I/1/1  0
inputi(1) = 0

loop:
string = in_b({pos}:)
index  = m$index(string,",")

if index .gt. 0 then
   pind = nind
   nind = nind + 1
   inputi({nind}) = index + inputi({pind})
   pos = inputi({nind}) + 1
   goto loop
endif

inputi(1) = nind
nind = nind + 1
inputi({nind}) = m$len(in_b) 

! inputi(1) : Number of indep. variables
! inputi(2) to inputi(2+n-1): Position of coma separators
! inputi(2+n) : length of the character string

-DELETE {P4}

run STD_EXE:progress.exe

if P7(1:1) .eq. "N" then
   outputr(2) = 0
endif

RETURN







