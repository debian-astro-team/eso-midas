! @(#)idenline.prg	19.1 (ES0-DMD) 02/25/03 14:29:19
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! SPECTRAL PROCEDURE : IDENLINE.PRG
! J.D.Ponz			version 2.0 130386
!
! .PURPOSE
!
! execute the command :
! IDENTIFY/LINE [TABLE] [ERROR]
!
! ------------------------------------------------------------
DEFINE/PARAM P1 line TABLE
DEFINE/PARAM P2  4.  NUMB
!
IDENTI/GCURSOR 'P1' :IDENT :X  'P2'
! COPY/TT   'P1' :IDENT :WAVE
