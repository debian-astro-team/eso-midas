/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* Program : localmax.c                                           */
/* Author  : P. Ballester   -     ESO Garching                    */
/* Date    : 20.03.95      Version 1.0                            */
/*                                                                */
/* Purpose :                                                      */
/*           Determines local extrema in an image. Generates a    */
/*           table of positions                                   */

/* Input   : IN_A  - Input image name (1D)                        */
/*           IN_B  - Mode (MAXI, MINI for local maxima or minima) */
/*           OUT_A - Output table name                            */
/*.VERSION

   090723							  */


#include <math.h>
#include <ctype.h>
#include <tbldef.h>
#include <midas_def.h>

#define   ipos(col,row,siz)              row * siz + col

int main()

{
      char      outtab[TEXT_LEN];
      char      inframe[61], mode[11];
      char      ident[80], cunit[64];

      int  col, pixel, rank=0;
      int  pcol, xcol, vcol;
      int  naxis, npix[2], imno; 

      int       actvals, tid;
      double    *image;
      double    start[2], step[2], value, x;
      int       cond1, cond2, mod_min, mod_max;

      SCSPRO("localmax");

      SCKGETC ("IN_A", 1, 60,  &actvals, inframe);
      SCKGETC ("IN_B", 1, 10,  &actvals, mode);
      SCKGETC ("OUT_A", 1, 60, &actvals, outtab);

      SCIGET(inframe, D_R8_FORMAT, F_I_MODE, F_IMA_TYPE, 1, &naxis, 
             npix, start, step, ident, cunit, (char**)&image, &imno);

      TCTINI (outtab, F_TRANS, F_IO_MODE, 3, 100, &tid);

      TCCINI (tid, D_R4_FORMAT, 1, "I6",   "  ", "PIXEL", &pcol);
      TCCINI (tid, D_R4_FORMAT, 1, "F12.4", "  ","X",     &xcol);
      TCCINI (tid, D_R4_FORMAT, 1, "F12.4", "  ","VALUE", &vcol);

      mod_min = (toupper(mode[1]) == 'I');
      mod_max = (toupper(mode[1]) == 'A');

     for (col=1; col<npix[0]-1; col++) {

      cond1 = (mod_max && image[col-1]<image[col] && image[col+1]<image[col]);
      cond2 = (mod_min && image[col-1]>image[col] && image[col+1]>image[col]);

          if (cond1 || cond2) {
              value  = image[col];
              pixel  = col;
              x = (col-1)*step[0] + start[0];
              rank++;
              TCEWRI(tid, rank , pcol  , &pixel);
              TCEWRD(tid, rank , xcol  , &x);
              TCEWRD(tid, rank , vcol  , &value);
      }}

      /* Close everything and good bye. */
      TCTCLO(tid), SCSEPI();

    SCSEPI();
    return 0;
    }
