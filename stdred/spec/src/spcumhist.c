/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* Program : spcumhist.c					  */
/* Author  : P. Ballester   -     ESO Garching                    */
/* Date    : 31.08.94      Version 1.0                            */
/*                                                                */
/* Purpose :                                                      */
/*                                                                */
/* Transforms a histogram into a cumulative histogram (image)     */
/*                                                                */
/* Input:                                                         */  
/*             - name of input image            : IN_A            */
/*             - name of output image           : OUT_A           */
/*.VERSION

   090723	last modif					  */

	


#include <math.h>
#include <ctype.h>
#include <midas_def.h>

#define  ipos2D(col,row,npix) row*npix[0]+col

int main ()

{

    char   inframe[100], outframe[100], cunit[64], ident[72], mode[12];
    int    actvals,x,naxis,npix[2],imnin,imnout,row;
    float  *pntrin, *pntrout;
    double start[2], step[2];

    SCSPRO ("spcumhist");

    SCKGETC ("IN_A",  1, 60, &actvals, inframe);
    SCKGETC ("IN_B",  1, 10, &actvals, mode);
    SCKGETC ("OUT_A", 1, 60, &actvals, outframe);

    SCIGET(inframe, D_R4_FORMAT, F_I_MODE, F_IMA_TYPE, 2, 
           &naxis, npix, start, step, ident, cunit, (char **)&pntrin, &imnin);

    strcpy(ident,"Cumulated histogram ");
    strcat(ident,inframe);
    if (naxis == 1) npix[1]=1;

    SCIPUT(outframe, D_R4_FORMAT, F_IO_MODE, F_IMA_TYPE, naxis, 
           npix, start, step, ident, cunit, (char **)&pntrout, &imnout);
 
    

    if (toupper(mode[0]) == 'A') {
         for (row=0; row<npix[1]; row++) {
            pntrout[ipos2D(0,row,npix)] = pntrin[ipos2D(0,row,npix)];
            for (x=1; x<npix[0]; x++)
              pntrout[ipos2D(x,row,npix)] = 
              pntrout[ipos2D(x-1,row,npix)] + pntrin[ipos2D(x,row,npix)];
       }}


    if (toupper(mode[0]) == 'E') {
         for (row=0; row<npix[1]; row++) {
            pntrout[ipos2D(npix[0],row,npix)] = 
                       pntrin[ipos2D(npix[0],row,npix)];
         for (x=npix[0]-1; x>=0; x--)
              pntrout[ipos2D(x,row,npix)] = 
              pntrout[ipos2D(x+1,row,npix)] + pntrin[ipos2D(x,row,npix)];
       }}


    SCSEPI();
  return 0;
}

