% @(#)subtrairsp.hlq	19.1 (ESO-IPG) 02/25/03 14:23:17 
% @(#)
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1992 European Southern Observatory
%.IDENT      rectifirsp.hlq
%.AUTHOR     E.Oliva, Arcetri (Florence)
%.KEYWORDS   MIDAS, help files, SUBTRACT/IRSPEC
%.PURPOSE    On-line help file for the command: SUBTRACT/IRSPEC
%.VERSION    1.0  22-SEP-1992 : Creation, E.Oliva
%----------------------------------------------------------------
\se
SECTION./IRSPEC
\es\co
SUBTRACT/IRSPEC                            22-SEP-1992 EO
\oc\su
SUBTRACT/IRSPEC in_ima out_ima degree [exclude=area_to_exclude]
             [cont=continuum_image] [load=load_option]
         Fit and subtract, row by row, polynomial to a given image.
\us\pu
Purpose:    To subtract, row by row, a polynomial fit of the continuum
            from an image. The user can exclude one or more areas from
            the fit. An image containing the fitted continua can be
            also produced, if requested.
\up\sy
Syntax:     SUBTRACT/IRSPEC in_ima out_ima degree [exclude=area_to_exclude]
                            [cont=continuum_image] [load=load_option]
\ys\pa
            in_ima   = Name of the input image. Can be either 2D or 1D,
                       untouched on exit.
\ap\pa
            out_ima  = Name of the output, continuum subtracted image.
                       It will have the same size and limits as in_ima.
\ap\pa
            degree   = Degree of the interpolating polynomials.\\
\\
            Additional parameters and options.
\ap\pa
            area_to_exclude=\\
                       Defines the region(s) to exclude from the fit
                       (e.g. regions containing strong lines). You have
                       three possibilities:\\
                     - CURSOR (the default) will ask you to define with
                       the cursor the area(s) to exclude;\\
                     - [xs,ys:xe,ye] (or [xs:xe] for 1D images) defines 
                       a given area of the image (see HELP EXTR/IMA if
                       you are not familiar with this syntax);\\
                     - table_name,tbl (e.g. mytable,tbl) in case the
                       regions to exclude are stored in a table; you
                       may want to create it with the cursor using
                         GET/CURSOR table_name ? ? 999,2\\
                       for 2D images and\\
                         GET/GCURS  table_name\\
                       for 1D images.
\ap\pa
            continuum_image=\\
                       Optional name of the image containing the fitted
                       continua, defaulted to - none - (no image created).
\ap\pa
            load_option = 0,1 default=1.
                       If =0 the command works silently.
                       if =1 (default) it will display the image before 
                       and after the continuum subtraction.
\ap\no
Note:       Working with 2D images be careful not to exclude too many 
            pixels, you may get funny results. The program may get crazy
            if you exclude one or more entire rows.
\on\exs
Examples:
\ex         
            SUBTR/IRS cc0012 cc12sub 3 
\xe\ex
            SUBTR/IRS a0118 a118sub 2 e=[@12,@28:@27,@33] c=a118cont
\xe\ex
            SUBTR/IRS iras12 iras12nc 3 l=0 c=iras12c e=line2,tab
\xe\sxe













