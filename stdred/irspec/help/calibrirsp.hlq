% @(#)calibrirsp.hlq	19.1 (ESO-IPG) 02/25/03 14:23:15 
% @(#)
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1992 European Southern Observatory
%.IDENT      rectifirsp.hlq
%.AUTHOR     E.Oliva, Arcetri (Florence)
%.KEYWORDS   MIDAS, help files, CALIBRATE/IRSPEC
%.PURPOSE    On-line help file for the command: CALIBRATE/IRSPEC
%.VERSION    1.0  22-SEP-1992 : Creation, E.Oliva
%----------------------------------------------------------------
\se
SECTION./IRSPEC
\es\co
CALIBRATE/IRSPEC                                     22-SEP-1992 EO
\oc\su
CALIBRATE/IRSPEC ima
        Apply on-line (mechanical) wavelength calibration.
CALIBRATE/IRSPEC ima_ref mode=define
        Define and store in ima_ref precise calibration from sky/lamp lines
CALIBRATE/IRSPEC ima ref=ima_ref
        Apply precise wavelength calibration to frame ima from parameters
        stored in ima_ref
\us\pu
Purpose:    The wavelength dispersion on the IRSPEC array is linear within
            a small - and totally negligible - fraction of the pixel size.
            Hence, wavelength calibrating simply means modifying the x-start
            and x-step values (descriptor) of the image. Another advantage
            of IRSPEC is that one can very precisely compute (analytically)
            the pixel size - in wavelength - once the central wavelength of
            the frame is known. A quite precise - usually within 1 pixel - 
            estimate of this quantity is available on-line at the instrument 
            ("mechanical" calibration) and is stored in the original files 
            in the form of descriptors. You can directly use this information
            and determine the "mechanical" wavelength calibration as in the
            first example.
            This command allows you also to determine more precisely the
            central wavelength of your frame - and hence to obtain a very 
            accurate wavelength calibration - if you have a frame containing
            lines with known wavelengths; up to 2.3 microns the OH lines in
            the sky frames are a very convenient calibrator (Oliva \& Origlia,
            1992, A\&A 254, 466). See the 2-nd example and/or use 
            TUTORIAL/CALIBRATE.
            Finally, this command can be used to apply a precise calibration
            to a given image (see third example).
\up\sy
Syntax:     CALIBRATE/IRSPEC ima [ref=reference_ima] [mode=calibration_mode] 
                             [units=wavelength_units]
\ys\pa
            ima     =  Frame to wavelength calibrate, it may have any
                       size in Y (could e.g. be 1D) but must have the 
                       original size in X. "Untouched" on exit (see last
                       note below)
                       If mode=d (definition of precise calibration), 
                       "ima" is the sky/lamp frame containing lines with 
                       known wavelengths and must have the original X,Y 
                       sizes. Untouched on exit.
\ap\pa
            ref      = Optional reference image which contains precise 
                       calibration parameters, determined using CALIB/IRSP
                       with mode=d
\ap\pa 
            mode     = If set to d (define) it interactively defines the
                       best wavelength calibration parameters using the
                       lines contained in the frame "ima" (see also
                       TUTORIAL/CALIB). Use   mode=d   (or just m=d) for
                       calibration using sky lines (you need a flat and
                       a dark for this). For lamp lines frames it may be 
                       more convenient to work on the frame as it is, in
                       which case use      m=d1    (sorry for the dirty
                       trick...).
\ap\pa
            units    = Wavelengths are given in microns. You can use
                       units=a (or just u=a) if you prefer to work in
                       Angstroms.
\ap\no
Note:       It is most convenient (and clearer) to apply the wavelength
            calibration only to "rectified frames" - i.e. with straight 
            spectral lines (see RECTIFY/IRSPEC). 
\\
            The wavelength calibrated frame is identical to the original
            but has different start and step in X. Therefore, if you want
            to go back to the uncalibrated image it's enough using
            WRITE/DESC image_name start 1
            WRITE/DESC image_name step 1
\on\exs
Examples:
\ex
            CALIB/IRSP test4
              apply mechanical calibration to frame test4 
\xe\ex
            CALIB/IRSP sky3 mode=d
              define and store in "sky3" precise calibration parameters
              from the sky/lamp lines contained in the same frame (see
              also TUTORIAL/CALIB)
\xe\ex
            CALIB/IRSP test4 ref=sky3
               apply precise parameters (defined above) to frame "test4"
\xe\sxe






