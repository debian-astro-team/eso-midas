! @(#)irsflat.prg	19.1 (ES0-DMD) 02/25/03 14:23:29
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irsflat.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Execute command  FLAT/IRSPEC
!
! used to normalize an halogen frame
! Just divides by the average of the central ny/10 rows
! Takes care of the vignetted scan-lines.
! The operation is performed by irsflat.for
!
!.VERSION     1.0    Creation     02.09.1992   E. Oliva
!
!-------------------------------------------------------
!
! tested O.K. september 3 1992 , Tino
!
crossref halo dark out load thres vignet
def/param p1 ?  ima "Input halogen image ? : "
def/param p2 ?  cha "Input dark image (0 for none) ? : "
def/param p3 ?  ima "Output image ? : "
def/param p4 1  num "Load option (>0 = yes) "
def/param p5 .50 num "Threshold (relative) below it force scanline=p6" 0.1,0.7
def/param p6 100 num "Value to force vignetted scan-lines" 1,1e4
!
def/loc incmd/c/1/132 "{MID$LINE}"
!
! global variables used:
!   Iscale
!
def/loc ny/i/1/1 0 ? +lower
def/loc i/i/1/1 0
def/loc j/i/1/2 0,0
def/loc lev/r/1/3 0,0,0 ? +lower
def/loc framo/c/1/60
def/loc thres/r/1/1 0.
def/loc vignet/r/1/1 0.
def/loc ide/c/1/20 " " all +lower
thres = 'p5'
vignet = 'p6'
!
VERIFY/IRSPEC 'p1' 
@ creifnot 2 heat
!
copy/dk 'p1' npix/i/2/1 ny/i/1/1
j(1) = ny/2-5
j(2) = ny/2+5
if "{p2}" .ne. "0" then
  comp/image &a = 'p1'-'p2'
else
  comp/image &a = 'p1' 
endif
copy/dd 'p1' * &a
BADPIX/IRSPEC &a &b c=b l='p4'
!
set/format i2 f6.2
write/out " "
write/out "Normalizing the frame by AVER, the average value of the central 10 rows"
write/out "All rows with mean < {thres}*AVER "
write/out "are considered vignetted and set = {vignet}"
write/out " "
set/format
!
stat/image &b [@1,@'j(1)':>,@'j(2)'] option=rn
copy/dk &b statistic/r/1/3 lev/r/1/3
run STD_EXE:irsflat.exe 
comp 'p3' = &c
cut 'p3' 0.7,1.3
if 'p4' .gt. 0 load/image 'p3' scale='Iscale'
copy/dk 'p1' ident/c/1/19 ide/c/1/19
write/desc 'p3' ident/c/1/19 "{ide}"
write/desc 'p3' ident/c/20/40 "level={lev(3)}"
write/desc 'p3' levels/r/1/3 'lev(1)','lev(2)','lev(3)'
copy/kd incmd/c/1/132 'p3' history/c/-1/132
set/format i4 e8.2
!
write/out "Normalized flat {p3} is now created."
write/out "If you want to use it in SKYSUB/IRSPEC, CALIB/IRSPEC... type:"
write/out "      ----   set/irspec flat={p3}  ----  "
!
