! @(#)irstilt.prg	19.1 (ES0-DMD) 02/25/03 14:23:30
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irstilt.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Execute command RECTIFY/IRSPEC
!              used to rectify slits
!.VERSION     1.0    Creation     02.09.1992   E. Oliva
!
!-------------------------------------------------------
!
! tested O.K. sep 4 1992, Tino
!
crossref in out load tilt ref dummy1 verbose
!
!
def/param p1 ?  ima "Input irspec image ? : "
def/param p2 ?  ima "Output image ? : "
def/param p3 0  num "Load option (>0=yes)"
def/param p4 0  num "tan(tilt_angle), 0=automatic comp. "
def/param p5 0  num "Reference row ? : " 
def/param p6 0  cha Dummy
def/param p7 1  num "verbose option.."
!
def/loc incmd/c/1/132 "{MID$LINE}"
!
def/loc framei/c/1/60 
def/loc frameo/c/1/60
def/loc angle/r/1/1 0
def/loc rowref/i/1/1 0
def/loc wlcen/d/1/1 0. ? +lower
def/loc ngrat/i/1/1 0 ? +lower
def/loc order/i/1/1 0 ? +lower
def/loc ny/i/1/1 0 ? +lower
def/loc i/i/1/1 0 ? +lower
def/loc j/i/1/1 0
!
@ creifnot 2 heat
!
if p1 .eq. p2 then
  write/out "Output and input frames cannot have the same names"
  return
endif
write/keyw framei 'p1'
write/keyw frameo 'p2'
copy/dk 'p1' naxis/i/1/1 i/i/1/1
if i .eq. 1 goto fram1d
copy/dk 'p1' npix/i/2/1 i/i/1/1
if i .eq. 1 goto fram1d
VERIFY/IRSPEC 'framei'
j = m$index(Itilt,"/")-1
i = m$existd(p1,Itilt(1:{j}))
if i .gt. 0 then
  if 'p7' .eq. 1 then
    write/out "Input frame is already rectified "
  endif
  return
endif
if 'p7' .ge. 2 then
  write/out "Input frame is not rectified. Rectifying....."
endif
if p6 .eq. "0" then
  copy/dk 'p1' 'Icw' wlcen/d/1/1
  copy/dk 'p1' 'Igrat' ngrat/i/1/1
  copy/dk 'p1' 'Iorder' order/i/1/1
else
  verify/irspec 'p6'
  copy/dk 'p1' 'Icw' wlcen/d/1/1
  copy/dk 'p1' 'Igrat' ngrat/i/1/1
  copy/dk 'p1' 'Iorder' order/i/1/1
endif
copy/dk 'p1' npix/i/2/1 ny/i/1/1

rowref = 'p5'
if rowref .le. 0 rowref = ny/2
if rowref .gt. ny then
  write/out  Reference row out of range.
  return/exit
endif
angle = 'p4'
if 'p3' .gt. 0 load 'framei' scale='Iscale'
run STD_EXE:irstilt.exe
copy/dd 'framei' * 'p2'
copy/kd incmd/c/1/132 'p2' history/c/-1/132
write/desc 'p2' 'Itilt' 1
if 'p3' .gt. 0 load 'p2' scale='Iscale'
return
fram1d:
if 'p7' .eq. 2 return
write/out "Input frame is 1D"
return
