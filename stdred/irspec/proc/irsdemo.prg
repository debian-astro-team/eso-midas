! @(#)irsdemo.prg	19.1 (ES0-DMD) 02/25/03 14:23:29
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irsdemo.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Quick demonstration and test of all IRSPEC commands.
!.VERSION     1.0    Creation     28.09.1992   E. Oliva
!
!-------------------------------------------------------
!
DEFINE/PARAM P1 NOAUTO ? "AUTO/NOAUTO"
def/loc i/i/1/1 0
IF P1(1:4) .NE. "AUTO" THEN
  write/out " "
  write/out "This is a quick demonstration of all the IRSPEC commands which"
  write/out "is mainly used as a test for the installation of MIDAS."
  write/out "You are kindly invited to look at the help of the single commands"
  write/out "(typing HELP you will get a list of them)."
  write/out "Note that after the demonstration all the files named : "
  write/out "--  irstut*.* -- "
  write/out "will be destroyed. So, if you have files called this way you are"
  write/out "still in time to stop the procedure."
  write/out "Be also aware that the name of the on-line flat and dark will be"
  write/out "overwritten."
  inqu/keyw i "Enter 1 to stop here, return to continue : "
  if i .ne. 0 return
  write/out " "
ENDIF
!
! Images: irstut{i}
!
! 0001 = flat
! 0002 = dark
! 0003 = original halo to make the flat
! 0004 = OBJ-DEMO for SKYSUB
! 0005 = SKY-DEMO for SKYSUB
! 0006 = SUBTR-STAR-DEMO for RESPONSE
! 0007 = LINE-DEMO for SUBTRACT
! 0008-0012 = DEMO for CONNECT
!
! Tables: irstut{i}
!
! 0001 = table for skysub_demo
! 0002 = table (originally bs779) demo for RESPONSE
!
! ASCI files
!
! irstut0001.dat
!
WRITE/OUT "Copying demo files......'
do i = 1 12
   intape/fits 1 toto MID_TEST:irstut{i}.fits NOC
   -rename toto0001.bdf  irstut{i}.bdf
enddo
do i = 1 2
   intape/fits 1 toto MID_TEST:irstut{i}.tfits NOC
   -rename toto0001.tbl  irstut{i}.tbl
enddo
-copy MID_TEST:irstut0001.dat irstut.flux
!
@ creifnot 3 heat
!
write/out " "
write/out "DEFINE/IRSPEC"
DEFINE/IRSPEC irstut0002 irstuttab1 l=1 
write/out " "
write/out "BADPIX/IRSPEC"
CLEA/CHA OVER
BADPIX/IRSPEC irstut0005 irstutima0 l=1 
write/out " "
write/out "FLAT/IRSPEC"
FLAT/IRSPEC irstut0003 0 irstutimas l=1 
write/out " "
write/out "SKYSUB/IRSPEC"
SET/IRSPEC flat=irstut0001 dark=irstut0002
SKYS/IRSP irstut0004 irstut0005 irstutima1 0 s=irstut0001 c=-30,30 f=1
write/out " "
write/out "RECTIFY/IRSPEC"
RECTIFY/IRSPEC irstutima0 irstutimas l=1
RECTIFY/IRSPEC irstutima1 irstutima2 l=0
write/out " "
write/out "CALIBRATE/IRSPEC"
CALIB/IRSP irstutima2
write/out " "
write/out "STANDARD/IRSPEC with black-body"
STANDARD/IRSPEC irstut.flux irstuttab2 bb
write/out "STANDARD/IRSPEC with polynomial fit in log-log"
STANDARD/IRSPEC irstut.flux irstuttab2 poly 3
write/out " "
CALIB/IRSP irstut0006
write/out "RESPONSE/IRSPEC"
RESPONSE/IRSPEC irstut0006 irstuttab2 irstutima3 y=25,31,38,44
CLEAR/CHA over
write/out " "
write/out "FLUX/IRSPEC"
FLUX/IRSPEC irstutima2 irstutima3 irstutima4
write/out " "
write/out "SUBTRACT/IRSPEC"
SUBTRACT/IRSPEC irstut0007 irstutima5 2 excl=[2.121,22:2.131,44]
write/out " "
write/out "MERGE/IRSPEC"
MERGE/IRSPEC irstut 8,12 irstuttab3 f=i4 e=4 
SET/GRAPH xaxis=auto yaxis=auto
write/out " "
write/out " "
write/out "Removing irstut* files...."
if aux_mode .lt. 2 then
   -delete irstut*.*.*
else
   -delete irstut*.*
endif
write/out " "
write/out "End of test/demonstration"
