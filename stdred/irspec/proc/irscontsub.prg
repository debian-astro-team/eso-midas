! @(#)irscontsub.prg	19.1 (ES0-DMD) 02/25/03 14:23:29
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irscontsub.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Execute command SUBTRACT/IRSPEC
!      used to subtract (row by row) the continuum
!      from a given 2D frame. The continuum is fitted by
!      1D polynomia of a chosen degree.
!      The region of the image  which must not be included
!      in polynomial fitting is defined by means of a "write/ima" command,
!      the user can use CURSOR,GCURSOR,tables,[...]....
!      A continuum 2D image can also be created if requested
!      The fitting and subtraction is performed by irscontsub.for
!
!.VERSION     1.0    Creation     02.09.1992   E. Oliva
!
!-------------------------------------------------------
!
! Tested O.K. Sept. 3 1992, Tino
!
crossref in out degree exclude cont load
def/param p1 ?  ima "Input image ? : "
def/param p2 ?  ima "Ouput image ? : "
def/param p3 ?  num "Interpolation degree ? : " 0,10
def/param p4 CURSOR  cha "Region to exclude from fit = ? "
def/param p5 none  ima "Continuum image ? : "
def/param p6 1  num "Load option (>0 = yes) "
!
def/loc incmd/c/1/132 "{MID$LINE}"
!
def/loc i/i/1/1 0
def/loc ny/i/1/1 0 ? +lower
def/loc deg/i/1/1 0
def/loc cont/i/1/1 0
def/loc pp4/c/1/60 
!
verify/irspec 'p1'
@ creifnot 3 heat
!
deg = 'p3'
cont = 1
pp4 = m$upper(p4)
if p5(1:4) .eq. "none" cont = 0
if p5(1:4) .eq. "NONE" cont = 0
!
copy/dk 'p1' naxis/i/1/1 ny/i/1/1
if ny .gt. 1 then
  copy/dk 'p1' npix/i/2/2 ny/i/1/1
endif
comp &csa = 'p1'
copy/dd 'p1' *,2 &csa
if 'p6' .gt. 0 then
  cuts &csa 0,0
  if ny .gt. 1 then
    load/image &csa scale='Iscale'
  else
    plot &csa
  endif
endif
if pp4(1:6) .eq. "CURSOR" then
  if ny .gt. 1 then
    if 'p6' .le. 0 load/image &csa scale='Iscale'
    write/out "Define with the cursor the region(s) containing line(s)"
    log(4) = 2
    write/image cursor 0 all
    log(4) = 0
    clear/cha over
  else
    write/out "Take away the line(s) with the cursor (as in modify/gcursor)"
    mod/gc &csa
  endif
else
  if pp4(1:4) .ne. "NONE" then
    log(4) = 2
    write/image &csa 'p4' 0 all
    log(4) = 0
  endif
endif
comp &csw = &csa/'p1'
write/keyw IN_A  'p1'
write/keyw OUT_A  'p2'
write/keyw OUT_B  'p5'
run STD_EXE:irscontsub.exe
copy/dd 'p1' ident 'p2'
write/desc 'p2' ident/c/21/30 "cont. subtract"
copy/kd incmd/c/1/132 'p2' history/c/-1/132
if 'p6' .gt. 0 then
  if ny .gt. 1 then
    load/image 'p2' scale='Iscale'
  else
    plot 'p2'
  endif
endif
return
