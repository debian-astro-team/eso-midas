! @(#)irsfluxcal.prg	19.1 (ES0-DMD) 02/25/03 14:23:29
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irsfluxcal.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Execute command FLUX/IRSPEC
!
! Used to flux-calibrate an object 2D (or 1D) reduced image 
! using a response frame obtained from RESP/IRSPEC
! Does not use external (.for) files.
!
!.VERSION     1.0    Creation     24.09.1992   E. Oliva
!
!-------------------------------------------------------
!
!
crossref in ires out smooth shift norm rect
def/param p1 ? ima "Input  object image ? : "
def/param p2 ? ima "Input 1D response image ? : "
def/param p3 ? tab "Name of output frame ? : "
def/param p4 0,0 num "Smooth to apply to obj,response (in pixels)"
def/param p5 0 num "Shift to apply to response star (in pixels)"
def/param p6 1 num "Normalize option (>0 check and do)"
def/param p7 1 num "Rectify option (>0 check and do)"
!
def/loc incmd/c/1/132 "{MID$LINE}"
!
! global keys used
!   Iscale
!   Idit
!   Iresp
!
def/loc pp1/c/1/80 'p1'
def/loc wl/d/1/2 0.,0. ? +lower
def/loc dwl/d/1/2 0.,0. ? +lower
def/loc nx/i/1/2 0,0 ? +lower
def/loc ny/i/1/1 0 ? +lower
def/loc nax/i/1/1 0 ? +lower
def/loc i/i/1/1 0
def/loc jresp/i/1/1 0 ? +lower
def/loc n/i/1/1 0
def/loc smooth/r/1/2 0.,0.
def/loc shift/r/1/1 0.
!
verify/irspec 'pp1' 'p2' 
@ creifnot 3 heat
!
!
copy/dk 'pp1' npix/i/1/1 nx/i/1/1
copy/dk 'p2' npix/i/1/1 nx/i/2/1
if nx(1) .ne. nx(2) then
  write/out "Object and star frames must have the same # of pixels in X "
  return/exit
endif
copy/dk 'pp1' step/d/1/1 dwl/d/1/1
if dwl .eq. 1 then
  write/out "Input frame is not wavelength calibrated."
  return/exit
endif
copy/dk 'p2' step/d/1/1 dwl/d/2/1
if dwl(1) .ne. dwl(2) then
  write/out "Wavelength steps of obj and star frames are different"
  return/exit
endif
copy/dk 'pp1' start/d/1/1 wl/d/1/1
copy/dk 'p2' start/d/1/1 wl/d/2/1
if wl(1) .ne. wl(2) then
  write/out "Starting wavelengths of obj and star frames are different "
  return/exit
endif
copy/dk 'p2' naxis/i/1/1 nax/i/1/1
if nax .gt. 1 then
  write/out "Input response image is 2D, not creatinly a response frame."
  return/exit
endif
i = m$index(Iresp,"/")-1
i = m$existd(p2,Iresp(1:{i}))
if i .le. 0 then
  write/out "I have some doubts that the second image in the command list"
  write/out  - {p2} -
  write/out "is a response frame."
  inqu/keyw i "Strike return to continue, enter 1 to abort : "
  if i .gt. 1 return/exit
else
  copy/dk 'p2' 'Iresp' jresp/i/1/1
  if jresp .eq. 2 then
    write/out WARNING. The response image contains only the relative response
    write/out Hence, the final file will not be properly flux calibrated.
  endif
endif
comp &fo = 'pp1'
if 'p7' .ge. 1 then
  RECTIFY/IRSPEC 'pp1' &fo verbose=2
endif
if 'p6' .ge. 1 then
  @s irsrespons,normalize ima=&fo 
endif
comp &fr = 'p2'
write/desc &fo start/d/1/2 1,1
write/desc &fo step/d/1/1 1,1
write/desc &fr start 1
write/desc &fr step 1
write/keyw smooth/r/1/2 'p4'
write/keyw shift/r/1/1 'p5'
if smooth(1) .gt. 0 then
  comp &fs = &fo
  log(4) = 2
  filter/smoo &fs &fo 'smooth(1)'
  log(4) = 0
endif
if smooth(2) .gt. 0 then
  comp &fs = &fr
  log(4) = 2
  filter/smoo &fs &fr 'smooth(2)'
  log(4) = 0
endif
if shift .ne. 0 then
  comp &fs = &fr
  run STD_EXE:irsshift.exe
endif
copy/dk &fo naxis/i/1/1 nax/i/1/1
if nax .eq. 1 goto fram1d
copy/dk &fo npix/i/2/1 nax/i/1/1
if nax .eq. 1 goto fram1d
copy/dk &fo npix/i/2/1 ny/i/1/1
grow/image &fs = &fr 1,1,'ny'
comp/image 'p3' = &fo/&fs
copy/dd 'pp1' start/d/1/2 'p3' start/d/1/2
copy/dd 'pp1' step/d/1/2  'p3' step/d/1/2
load 'p3' scale='Iscale'
goto goon
fram1d:
comp/image 'p3' = &fo/&fr
copy/dd 'pp1' start/d/1/1 'p3' start/d/1/1
copy/dd 'pp1' step/d/1/1  'p3' step/d/1/1
plot 'p3'
goon:
copy/dd 'pp1' *,3 'p3'
copy/kd incmd/c/1/132 'p3' history/c/-1/132
if jresp .eq. 0 write/desc 'p3' cunit "Unkwown                        "
if jresp .eq. 1 write/desc 'p3' cunit "Flux Units                     "
if jresp .eq. 2 write/desc 'p3' cunit "Relative units                 "
return

