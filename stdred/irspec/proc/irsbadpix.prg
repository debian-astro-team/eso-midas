! @(#)irsbadpix.prg	19.1 (ES0-DMD) 02/25/03 14:23:28
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irsbadpix.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Execute command BADPIX/IRSPEC which modify badpixels
!             with coordinates stored in a ref. table
!             The operation (modification) is performed by
!             irsbadpix.for
!.VERSION     1.0    Creation     02.09.1992   E. Oliva
!
!-------------------------------------------------------
!
! Tested september 3 1992 O.K. Tino.
!
crossref in out load clean debug ref 
def/param p1 ?  cha "Input irspec image ? : "
def/param p2 ?  ima "Output image ? : "
def/param p3 0  num "Load option (>0=yes)"
def/param p4 a  cha "Direction option (x, y, b or a) : "
def/param p5 0  num "Debug option "
def/param p6 'badpix' cha "Reference table" 
!
def/loc incmd/c/1/132 "{MID$LINE}"
!
! global keys used : Iscale, badpix
!
def/loc direct/c/1/1
def/loc debug/i/1/1 0
!
@ creifnot 2 heat
!
if p1 .eq. p2 then
  write/out "Output and input frames cannot have the same names"
  return
endif
comp &bpx = 'p1'
if 'p3' .gt. 0 load &bpx scale='Iscale'
direct = p4
debug = 'p5'
write/keyw IN_A &bpx
write/keyw OUT_A 'p2'
write/keyw IN_B 'p6'
run STD_EXE:irsbadpix.exe
copy/dd  &bpx * 'p2'
if 'p3' .gt. 0 load 'p2' scale='Iscale'
copy/kd incmd/c/1/132 'p2' history/c/-1/132
!
return
