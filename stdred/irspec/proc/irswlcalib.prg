! @(#)irswlcalib.prg	19.1 (ES0-DMD) 02/25/03 14:23:31
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       wlcalib.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Execute command  CALIBRATE/IRSPEC
!             Does wl-calibration "mechanical" (on line)
!                                 "accurate"   (from user definition)
!                                 "define"     (to apply in accurate)
!.VERSION     1.0    Creation     02.09.1992   E. Oliva
!
!-------------------------------------------------------
!
! A dirty merge of wlraw, wloh, wlcal
!  may God forgive us.....
!
!
crossref ima ref mode cw units
def/param p1 ?  ima  "image to wl-cal ? : "
def/param p2 0  cha  "image containing wl-cal parameters ? : "
def/param p3 a  cha  "calibration mode (a=auto_apply, d,d1=define) "
def/param p4 0. num  "optional cw (only for wlraw)"
def/param p5 m  cha  "m=micron a=Angstrom"
!
def/loc pp3/c/1/2 "00"
def/loc wlpar/d/1/2 0.,0. ? +lower
def/loc j/i/1/1 0 ? +lower
def/loc i/i/1/1 0
!
VERIFY/IRSPEC 'p1'
@ creifnot 1

!
pp3(1:2) = m$lower(p3(1:2))
if pp3(1:1) .eq. "a"  then
  if p2 .ne. "0" then
    VERIFY/IRSPEC 'p2'
    j = m$index(Iwlcal,"/")-1
    i = m$existd(p2,Iwlcal(1:{j}))
    if i .eq. 1 then
      copy/dk 'p2' 'Iwlcal' wlpar/d/1/2
      if wlpar(1) .gt. 0. then
        j = m$index(Icw,"/")-1
        i = m$existd(p1,Icw(1:{j}))
        if i .eq. 1 then
          def/loc wlc/d/1/2 0.,0. ? +lower
          def/loc diff/d/1/1 0.
          copy/dk 'p1' 'Icw' wlc/d/1/1
          copy/dk 'p1' npix/i/1/1 j/i/1/1 
          wlc(2) = wlpar(1)+(('j'+1)/2)*wlpar(2)
          diff = (wlc(1)-wlc(2))/wlpar(2)
          if diff .lt. 0. diff = -diff
          if diff .gt. 4. then
            write/out "the on-line (mechanical) central wavelength of your frameis "
            write/out " - {wlc(1)} - "
            write/out "and is far from that imposed by the reference frame:"
            write/out " - {wlc(2)} - "
            write/out "Check if you did not make any mistake and...."
            j = 0
            inqu/keyw j ".. enter 1 to continue, hit return to abort. "
            if j .ne. 1 return/exit
          endif
        endif
        copy/kd wlpar/d/1/1 'p1' start/d/1/1
        copy/kd wlpar/d/2/1 'p1' step/d/1/1
        return
      endif
    endif
    write/out "WARNING The reference frame -- {p2} -- "
    write/out "does not contain accurate wavelength calibration parameters"
    i = 0
    inqu/keyw i "Hit return to apply the mechanical calibration, 1 to abort : "
    if i .ne. 0 return/end
  endif
  @s irswlcalib,wlraw ima='p1' ref='p2' cw='p4' units='p5'
  return
endif
if pp3(1:1) .eq. "d" then
  @s irswlcalib,wloh ima='p1' mode='pp3(2:2)' units='p5'
  return
endif
write/out "Invalid mode"
write/out "Only  A (apply) and D (define) are available"
write/out "...sorry...."
return/exit

!
!
entry wlraw
!
! originally was wlraw procedure.
!
crossref ima ref mode cw units
def/param p1 ?  ima  "image to wl-cal ? : "
def/param p2 0  cha  "image containing wl-cal parameters ? : "
def/param p3 a  cha "mode (unused here)."
def/param p4 0. num  "optional cw"
def/param p5 m  cha  "units (m=micron, a=armostroeng)"
!
def/loc ord/i/1/1 0 ? +lower
def/loc grat/i/1/1 0 ? +lower
def/loc cw/r/1/1 0. ? +lower
def/loc ix/i/1/1 0 ? +lower
def/loc begin/d/1/1 0.
def/loc stp/d/1/1 0.
def/loc scr/d/1/1 0.
def/loc dgamma/r/1/1 0.
!
copy/dk 'p1' npix/i/1/1 ix/i/1/1
if ix .ne. Inpix(1) then
  write/out "WARNING! the image is not {Inpix(1)} pixels in X !!"
endif
if p2(1:1) .ne. "0" then 
  copy/dk 'p2' 'Iorder' ord
  copy/dk 'p2' 'Igrat' grat
  copy/dk 'p2' 'Icw' cw
else
  copy/dk 'p1' 'Iorder' ord
  copy/dk 'p1' 'Igrat' grat
  copy/dk 'p1' 'Icw' cw
endif
if 'p4' .gt. 0. cw = 'p4' 
!
dgamma = 57.29577951*Igamma0
scr = ord*cw/2/m$cos(dgamma)/1000*Igrarul('grat')
scr = 1-scr*scr
stp = m$cos(dgamma)*1000./Igrarul('grat')*Ialpix/ord*m$sqrt(scr)
begin = cw-(ix-1.)/2.0*stp
p5 = m$lower(p5)
if p5(1:1) .eq. "a" then
  begin = begin*1e4
  stp = stp*1e4
endif
copy/kd begin/d/1/1 'p1' start/d/1/1
copy/kd stp/d/1/1 'p1' step/d/1/1
outputd(1) = begin
outputd(2) = stp 
return

!
entry wloh
!
! Originally was wloh procedure....
!
! Used to help determining start_wl of frame
! using sky OH lines or lamp lines.
!
crossref ima dummy1 mode dummy2 units
def/param p1 ?  ima  "image contianing reference lines ? : "
def/param p2 0  cha  "dummy"
def/param p3 0  cha  "mode..."
def/param p4 0  cha  "dummy"
def/param p5 m  cha  "m=micron a=Angstrom"
!
def/loc i/i/1/1 0
def/loc j/i/1/2 0
def/loc ix/i/1/1 0 ? +lower
def/loc r/d/1/1 0.
def/loc wlpar/d/1/2 0.
def/loc sh/d/1/1 0.
def/loc cw/d/1/1 0.
def/loc units/c/1/1 

units = m$lower(p5(1:1))
if p3(1:1) .ne. "1" then
  VERIFY/IRSPEC flatdark=1
  comp &wla = ('p1'-'dark')/'flat'
else
  comp &wla = 'p1'
endif
BADPIX/IRSPEC &wla &wlb 
RECTIFY/IRSPEC &wlb &wlc l=1
j(1) = Inpix(2)/2-3
j(2) = Inpix(2)/2+3
aver/row &wlb = &wlc @'j(1)',@'j(2)'
write/out "We must now subtract the continuum under the lines..."
set/grap xaxis=auto yaxis=auto
SUBTRACT/IRSPEC &wlb &wld 2 l=0 cont=&wlh
copy/dk &wld npix/i/1/1 ix/i/1/1
copy/dd 'p1' *,3 &wld
@s irswlcalib,wlraw ima=&wld units=a
wri/out " "
wri/out "Define center of line(s) as in CENTER/GAUSSIAN "
do i = 1 100
  copy/kk outputd/d/1/2 wlpar/d/1/2
  plot &wld
  center/gauss gcursor
  inqu/keyw sh "Shift to apply (in Angstrom), 0=end : "
  if sh .eq. 0. goto ok
  wlpar(1) = wlpar(1)+sh
  cw = wlpar(1)+(ix-1)/2.0*wlpar(2)
  cw = cw/1e4
  @s irswlcalib,wlraw ima=&wld  cw='cw' units=a
enddo
ok:
if units .ne. "a" then
  wlpar(1) = wlpar(1)/1e4
  wlpar(2) = wlpar(2)/1e4
endif
write/out "In case you made some mistakes.... "
sh = 0
inqu/keyw sh "Hit return to continue, enter 1 to abort... : "
if sh .ne. 0 return
copy/kd wlpar/d/1/2 'p1' 'Iwlcal'
write/out " "
write/out "precise wl-calibration parameters are stored in frame "
write/out "  -- {p1} --"
write/out " "
write/out "Use CALIB/IRS ima ref={p1}"
write/out "to wavelength calibrate your image(s)"
write/out "Keep in mind that this is accurate only if you are damn sure that "
write/out "the grating was exactly at the same position as in reference frame "
write/out " "
return

