! @(#)irsdefbad.prg	19.1 (ES0-DMD) 02/25/03 14:23:29
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irsdefbad.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Execute command DEFINE/IRSPEC
!
! Used to copy into a table the coordinates of the "bad pixels"
! of a given image (usally a dark).
! The criterium for defining the bad pixels can be according to a
! threshold in absolute values or in sigma (measured in a 10x10 area
! centered on a given pixel).
! The selection is performed by irsdefbad.for
!
!.VERSION     1.0    Creation     02.09.1992   E. Oliva
!
!-------------------------------------------------------
!
! Tested O.K. september 3 1992, Tino
!
crossref in out mode thresh nsub load
def/param p1 ?  ima "Input irspec image ? : "
def/param p2 ?  tab "Output table containing list of bad pixels ? : "
def/param p3 r  cha "Selection mode (r=relative a=absolute) "
def/param p4 15 num "Threshold (n*sigma for r, min,max for a) " 
def/param p5 6  num "Number (Nx=Ny) of subframes on which compute std " 2,20
def/param p6 1  num "Load option (if >0 make a badpix and show it) "
!
def/loc incmd/c/1/132 "{MID$LINE}"
!
def/loc framei/c/1/60 
def/loc tableo/c/1/60
def/loc smode/c/1/1
def/loc thres/r/1/2 0,-1e6
def/loc nsub/i/1/1 0

verify/irspec 'p1'
@ creifnot 2 heat
!
framei = p1
tableo = p2
smode = p3
write/keyw thres/r/1/2 'p4'
nsub = 'p5'
if "{p3}" .eq. "r" then
  write/out Select pixels with abs(value-average) > threshold*sigma
  write/out threshold = 'thres(1)'
  write/out number of sub-frames used = 'p5'x'p5'
endif
if "{p3}" .eq. "a" then
  if thres(2) .le. thres(1) then
    wri/out Wrong values of thresholds
    return
  endif
  write/out Select pixels with values 
  write/out  < 'thres(1)' or  > 'thres(2)'
endif
crea/table 'tableo' 4 10 null
crea/column 'tableo' :x_coord "PIXEL COORD" F7.0 R*4
crea/column 'tableo' :y_coord "PIXEL COORD" F7.0 R*4
crea/column 'tableo' :avalue "ABS. VALUE" G12.6 R*4
crea/column 'tableo' :rvalue "SIGMA-REL. VALUE" G12.6 R*4
run STD_EXE:irsdefbad.exe
if 'p6' .gt. 0 then
  clea/cha over
  write/out " "
  write/out  "This is the image before...."
  BADPIX/IRSPEC 'p1' &b ref='p2' l=1
  write/out "...and after the bad-pixels correction."
  comp/table 'p2' :x_disp = :x_coord+0.5
  comp/table 'p2' :y_disp = :y_coord+0.5
  write/out "These are the bad-pixels found."
  log(4) = 2
  load/table 'p2' :x_disp :y_disp ? 0 2
  log(4) = 0
endif
copy/kd incmd/c/1/132 'p2'.tbl history/c/-1/132
write/out " "
write/out "If you want to use the newly created table type"
write/out "---     set/irspec badpix='p2'    ---"
!
return
