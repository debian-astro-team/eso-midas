! @(#)irstutsky.prg	19.1 (ES0-DMD) 02/25/03 14:23:30
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irstutsky.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Demonstration of SKYSUB/IRSPEC (command TUTORIAL/SKYSUB)
!.VERSION     1.0    Creation     22.09.1992   E. Oliva
!
!-------------------------------------------------------
!
def/loc i/i/1/1 0
write/out " "
write/out "Before starting the demonstration be aware that all files named"
write/out " --  irstut*.* -- "
write/out "will be destroyed. So, if you have files called this way you are"
write/out "still in time to stop the procedure."
write/out "Be also aware that the name of the on-line flat and dark will be"
write/out "overwritten."
inqu/keyw i "Enter 1 to stop here, return to continue : "
if i .ne. 0 return
do i = 1 5
   intape/fits 1 toto MID_TEST:irstut{i}.fits NOC
   -rename toto0001.bdf  irstut{i}.bdf
enddo
write/out " "
write/out " "
write/out "Before starting using SKYSUB you must store a dark and a flat using"
write/out "SET/IRSPEC flat=flat_image dark=dark_image " 
write/out "To create the flat_image you can use FLAT/IRSPEC while as dark"
write/out "you can directly use the raw dark_frame; be however careful that"
write/out "the integration time of the dark_frame is the same as in the "
write/out "object and sky frames."
SET/IRSPEC flat=irstut0001 dark=irstut0002
write/out " "
inqu/keyw i "Hit Return to continue"
write/out " "
write/out " "
write/out " "
write/out "Here is a typical example of a pair of object and sky frames "
write/out "(called irstut0004 and irstut0005 respectively)."
write/out "where the OH sky-lines do not cancel by straight subtraction."
write/out "Let us first use the automatic mode...."
write/out ".... please be careful to define the sky region(s) as fairly large"
write/out "areas out from the object and containing sky lines"
write/out "(the actual position of the cursor is fine),"
write/out "use the right and up arrows to expand the size of the cursor. "
write/out " "
write/out "(note that - 0 - means automatic and cuts=.. is the same as in LOAD/IMA) "
@ creifnot 2 heat
set/cursor ? rect 46,130,460,180
clear/cha over
write/out " "
write/out "SKYSUB/IRSPEC irstut0004 irstut0005 irstutima1 0 cuts=-40,40"
SKYSUB/IRSPEC irstut0004 irstut0005 irstutima1 0 cuts=-40,40
write/out " "
inqu/keyw i "Hit Return to continue"
write/out " "
write/out " "
write/out " "
write/out "Now, if you want you can store the above defined sky region(s)"
write/out "(which presently are in a table called -- cursor -- ) "
write/out "into a table which you can also use in other SKYSUB commands"
write/out "This can be done using : "
write/out " "
write/out "RENAME/TABLE cursor irstutskyt"
RENAME/TABLE cursor irstutskyt
write/out " "
inqu/keyw i "Hit Return to continue"
write/out " "
write/out " "
write/out " "
write/out "This sky-table ( -- irstutskyt -- ) can be accessed by SKYSUB using"
write/out "the   sky=...   option, as in the following example"
write/out "(please note that for all options only 1 letter is enough, e.g.  cuts=.."
write/out "can be shortened into   c=.... ) "
write/out " "
write/out "SKYS/IRS irstut0004 irstut0005 irstutima2 0 s=irstutskyt c=-40,40"
SKYS/IRS irstut0004 irstut0005 irstutima2 0 s=irstutskyt c=-40,40
write/out " "
write/out "which gives the same result as with the original command"
write/out "with the manual sky-definition"
write/out " "
inqu/keyw i "Hit Return to continue "
write/out " "
write/out " "
write/out " "
write/out "You can easily see that the level of the background (sky) in the final"
write/out "image is not zero. This is a not unusual occurrence deriving from the"
write/out "fact that the detector `dark` is not exactly the same when the instrument" 
write/out "is looking at the sky."
write/out "You can try to correct this effect using the `force` (force_sky_to_zero)"
write/out "option which, when set to 1, subtract from the final frame the average"
write/out " value of the sky-regions. Example: "
write/out " "
write/out "SKYS/IRSP irstut0004 irstut0005 irstutima3 0 s=irstutskyt f=1 c=-40,40"
SKYS/IRSP irstut0004 irstut0005 irstutima3 0 s=irstutskyt f=1 c=-40,40
write/out " "
write/out "which does the same as before but forces the average value of the `sky` "
write/out "(i.e. the frames areas defined in the sky-table) in the final"
write/out "frame to zero. This certainly looks nicer and, indeed, force=1"
write/out "could be happily used in all cases when one does not need to obtain precise"
write/out "measurements of equivalent widths in objects with relatively faint continua."
write/out "If this is your case you should be extremely careful to check"
write/out "that the force=1 option does not introduce unwanted offsets"
write/out "(in case e.g. you chose your sky in regions where ther is still "
write/out " some continuum emission). "
write/out " "
inqu/keyw i "Hit Return to continue"
write/out " "
write/out " "
write/out " "
write/out " "
write/out "SKYSUB can also work interactively with the graphic cursor by setting"
write/out "the 4-th parameter to negative values. Examples : "
write/out " ...  using the force_sky_to_zero option.... "
write/out " "
write/out "SKYS/IRS irstut0004 irstut0005 irstutima5 -1,0 s=irstutskyt f=1 c=-40,40"
SKYS/IRS irstut0004 irstut0005 irstutima5 -1,0 s=irstutskyt f=1 c=-40,40
write/out " "
write/out " .... or not using it (in which case the sky table is not needed)  .... "
write/out " "
write/out "SKYS/IRS irstut0004 irstut0005 irstutima6 -1,0 c=-40,40"
SKYS/IRS irstut0004 irstut0005 irstutima6 -1,0 c=-40,40
write/out " "
write/out " "
write/out "In case you need to select factor,shift values outside the graphic range"
write/out "you can redefine the position of the graphic center "
write/out "playing with the 4-th parameter (factor,shift[,deltax,deltay]) "
write/out "Examples:"
write/out " "
write/out "SKYS/IRS irstut0004 irstut0005 irstutima7 -.92,.02 c=-40,40"
SKYS/IRS irstut0004 irstut0005 irstutima7 -.92,.02 c=-40,40
write/out "SKYS/IRS irstut0004 irstut0005 irstutima8 -.9,0,.05,.05 c=-40,40'
SKYS/IRS irstut0004 irstut0005 irstutima8 -.9,0,.05,.05 c=-40,40
write/out " "
inqu/keyw i "Hit Return to continue"
write/out " "
write/out " "
write/out " "
write/out "Finally, you can force SKYSUB to use a given combination of factor and shift"
write/out "just by entering the two values you want into the 4-th parameter.Example:"
write/out " "
write/out "SKYS/IRS irstut0004 irstut0005 irstutima9 .94,.02 c=-40,40"
SKYS/IRS irstut0004 irstut0005 irstutima9 .94,.02 c=-40,40
write/out " "
inqu/keyw i "Hit Return to continue"
write/out " "
write/out "The following, last example shows how SKYSUB can also be used"
write/out "to operate a straight object-sky subtraction without any further"
write/out "modification of the data."
write/out " "
write/out "SKYS/IRS irstut0004 irstut0005 irstutima10 1,0 c=-40,40"
SKYS/IRS irstut0004 irstut0005 irstutima10 1,0 c=-40,40
write/out " "
set/cursor
write/out "Deleting irstut* files"
-delete irstut*.*
