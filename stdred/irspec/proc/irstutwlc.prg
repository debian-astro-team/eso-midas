! @(#)irstutwlc.prg	19.1 (ESO-DMD) 02/25/03 14:23:30
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irstutsky.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Demonstration of CALIBRATE/IRSPEC (command TUTORIAL/CALIBRATE)
!.VERSION     1.0    Creation     22.09.1992   E. Oliva
!
!-------------------------------------------------------
!
def/loc i/i/1/1 0
write/out " "
write/out "---     Demonstration of CALIB/IRSPEC image mode=d     -----"
write/out "    i.e. how to make a precise wavelength calibration "
write/out "    from a frame containing OH sky line(s) ot Ne-Kr lamp lines "
write/out " "
write/out "Before starting the demonstration be aware that all files named"
write/out " --  irstut*.* -- "
write/out "will be destroyed. So, if you have files called this way you are"
write/out "still in time to stop the procedure."
write/out "Be also aware that the name of the on-line flat and dark will be"
write/out "overwritten"
inqu/keyw i "Enter 1 to stop here, return to continue : "
if i .ne. 0 return
write/out " "
write/out " "
write/out "To use CALIB with mode=d you must first store a dark and a flat using"
write/out "SET/IRSPEC flat=flat_image dark=dark_image " 
write/out "To create the flat_image you can use FLAT/IRSPEC while as dark"
write/out "you can directly use the raw dark_frame; be however careful that"
write/out "the integration time of the dark_frame is the same as in the "
write/out "frame containing the reference line(s)"
do i = 1 2
   intape/fits 1 toto MID_TEST:irstut{i}.fits NOC
   -rename toto0001.bdf  irstut{i}.bdf
enddo
i = 5
intape/fits 1 toto MID_TEST:irstut{i}.fits NOC
-rename toto0001.bdf  irstut{i}.bdf
SET/IRSPEC flat=irstut0001 dark=irstut0002
write/out " "
inqu/keyw i "Hit return to continue "
write/out " "
write/out " "
write/out "Here is a typical example of a sky frame containing several OH "
write/out "lines with precisely known wavelength."
write/out "The raw frame is called  -- irstut0005 --"
!
@ creifnot 2 heat
LOAD/IMAGE irstut0005 scale='Iscale' cuts=100,700
!
write/out " "
write/out "The lines you see in the image are, from left to right:"
write/out "  (3,1)R_2(1) 1.49278 (faint, close to the left edge of the array)"
write/out "  (3,1)Q-branch (very bright with 2 faint satellites on the right) "
write/out "  (3,1)P_2(2) 1.51830 "
write/out "  (3,1)P_1(2) 1.52368 "
write/out "and those with wavelengths can be used as calibrators "
write/out "(see Oliva & Origlia, 1992, A&A 254, 466)"
write/out "N.B. You can precisely wavelength calibrate even with just a single "
write/out "line in your reference frame"
write/out " "
inqu/keyw i "Hit return to continue "
write/out " "
write/out " "
write/out "To define a precise wavelength calibration from the sky lines, "
write/out "which is indeed equivalent to determine the shift from the on-line"
write/out "(mechanical) wavelength calibration, use: "
write/out " "
write/out "CALIB/IRSPEC irstut0005 mode=define (m=d will also do)"
write/out "and define interactively the shift with the help of the graphic cursor"
write/out " "
write/out "whenever you get : "
write/out "     start         end        center     pixel_value    FWHM "
write/out " "
write/out "click (enter) with the graphic cursor two positions on the side of the"
write/out "line(s) you want to use as calibrators. You can easily compute the shift"
write/out "manually (the wavelengths are given in Angstroms for convenience),"
write/out "exit from the graphic (click middle mouse) and enter the shift."
write/out "The procedure goes on iteratively until you are satisfied."
write/out " "
inqu/keyw i "Hit return to continue "
write/out " "
write/out "Here is how it works:"
write/out "(remember that the pixel size is 5 Angstroms, errors <=1 A are OK)"
write/out "CALIB/IRSPEC irstut0005 m=d"
write/out " "
@ creifnot 1 
CALIB/IRSPEC irstut0005 m=d
write/out " "
inqu/keyw i "Hit return to continue "
write/out " "
write/out "Note that the input image (here -- irstut0005 -- ) has not been modified"
write/out "(i.e. it is not wavelength calibrated) but contains, as descriptors,"
write/out "the parameters which are needed to calibrate other image(s). This can"
write/out "be performed using CALIB/IRSP ima ref=...."
write/out " "
write/out "Be always aware that all wavelength calibrations refer to the central"
write/out "row of the array and are valid for rectified images provided that you"
write/out "use the default value of reference_row in RECTIFY/IRSPEC (see the help"
write/out "of this command)"
write/out " "
inqu/keyw i "Hit return to continue "
write/out " "
write/out "Finally, remember that you can also work without a flat and a dark"
write/out "(i.e. define the lines centers on the original frame cleaned"
write/out "and rectified) by simply using   m=d1  instead of   m=d  "
write/out " "
-delete irstut*.*
