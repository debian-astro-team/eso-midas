! @(#)irsdelkey.prg	19.1 (ES0-DMD) 02/25/03 14:23:29
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irsdelkey.prg
!.AUTHOR      Klaus Banse, ESO Garching
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     delete keywords of context IRSPEC
!.VERSION     1.0    Creation     940712  KB
!-------------------------------------------------------
!
! has to be synchronized with `irsinit.prg'  !!!
! 
delete/keyw Igamma0,icsi0,ialpix,igrarul,inpix,iscale,flat,dark
delete/keyw badpix,icw,igrat,iorder,idit,itilt
delete/keyw Iresp,ifactor,ishift,iwlcal
