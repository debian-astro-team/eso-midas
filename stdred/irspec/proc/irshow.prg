! @(#)irshow.prg	19.1 (ES0-DMD) 02/25/03 14:23:29
! @(#)irshow.prg	19.1  (ESO)  02/25/03  14:23:29
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       irshow.prg
!.AUTHOR      E. Oliva, Arcetri (Firenze)
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Execute command SHOW/IRSPEC
!.VERSION     1.0    Creation    11.04.94  OE
!
!-------------------------------------------------------
!
write/out " "
write/out "Parameters used by IRSPEC reduction"
write/out " "
write/out "-- flat -- Name of 2D image containing flat field (see FLAT/IRSP) : "
write/out "                {flat}"
write/out " "
write/out "-- dark -- Name of 2D image containing dark : "
write/out "                {dark}"
write/out " "
write/out "-- badpix -- Name of table with positions of bad pixels (see DEF/IRSP) : "
write/out "                {badpix}"
write/out " "
write/out "The above paramters can be modified using SET/IRSPEC "
write/out " "
def/loc i/i/1/1 0
inqu/keyw i "Hit Return to end, enter 1 to continue...."
if i .ne. 1 return/end
write/out " "
write/out "Display/graphic parameters : "
write/out " "
write/out "-- Iscale/i/1/1  -- Scale for image loading : {Iscale}"
write/out " "
write/out "Instrumental parameters (to be modified only when using"
write/out "this package with instruments other than IRSPEC...."
write/out "... all angles are in radians)" 
write/out " "
write/out "-- Igamma0/r/1/1 -- Offaxis angle of the grating :"
write/out "                {Igamma0}"
write/out "-- Icsi0/r/1/1   -- Tilt angle of the slit :"
write/out "                {Icsi0}"
write/out "-- Ialpix/r/1/1  -- Projected size (angle) of a pixel on grating :"
write/out "                {Ialpix}"
write/out "-- Igrarul/r/1/2 -- Ruling (lines/mm) of gratings 1,2"
write/out "                {Igrarul(1)},{Igrarul(2)}"
write/out "-- Inpix/r/1/2   -- Size of the array (nX,nY)"
write/out "                {Inpix(1)},{Inpix(2)}"
write/out " "
inqu/keyw i "Hit Return to continue...."
write/out " "
write/out "Names and types of descriptors read from raw images"
write/out " "
write/out "-- Icw/c/1/30    -- name of descriptor with central wavelength :"
write/out "                {Icw}"
write/out "-- Igrat/c/1/30  -- name of descriptor with grating number :"
write/out "                {Igrat}"
write/out "-- Iorder/c/1/30 -- name of descriptor with order number :"
write/out "                {Iorder}"
write/out "-- Idit/c/1/30   -- name of descriptor with DIT value :"
write/out "                {Idit}"
write/out " "
write/out "Names and types of descriptors written in reduced images"
write/out " "
write/out "-- Itilt/c/1/30   -- descriptor with already-tilted flag :"
write/out "                {Itilt}"
write/out "-- Iresp/c/1/30   -- descriptor with 1D-response_frame flag :"
write/out "                {Iresp}"
write/out "-- Ifactor/c/1/30 -- descriptor with sky-factor applied by SKYSUB :"
write/out "                {Ifactor}"
write/out "-- Ishift/c/1/30  -- descriptor with obj-shift applied by SKYSUB :"
write/out "                {Ishift}"
write/out "-- Iwlcal/c/1/30  -- descriptor with wl-calibration parameters :"
write/out "                {Iwlcal}"
