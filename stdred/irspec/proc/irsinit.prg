! @(#)irsinit.prg	19.1 (ES0-DMD) 02/25/03 14:23:30
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       irsinit.prg
!.AUTHOR      E. Oliva,  Firenze, Arcetri
!.KEYWORDS    Spectroscopy, IRSPEC
!.PURPOSE     Execute command INIT/IRSPEC
!             Define (initialize) all the keywords necessary for irspec.
!             Also create default table for badpixels (irsbadpix.tbl)
!
!.VERSION     1.0    Creation     02.09.1992   E. Oliva
!
!-------------------------------------------------------
!
! has to be synchronized with `irsdelkey.prg' !!!
! 
register/session irspec  STD_PROC irsinit.prg irstab.tbl ! Initialisation
write/out Set parameters to default value
!Begin Session List
write/keyw  Igamma0/R/1/1  0.19626      ! Off-axis angle of the grating (rad)
write/keyw  Icsi0/R/1/1    0.2901       ! Angle by which the slit is tilted (rad) 
write/keyw  Ialpix/R/1/1   3.772e-4     ! Projected size of 1 pixel
write/keyw  Igrarul/R/1/2  298.5,596.96 ! Ruling (lines/mm) of gratings 1,2
write/keyw  Inpix/I/1/2    62,58        ! Size of the array (pixels x pixels)
write/keyw  Iscale/I/1/1   8            ! SCale for loading of frames.

write/keyw  flat/C/1/60    +++           ! Default name of flat (none...)
write/keyw  dark/C/1/60    +++           ! Default name of dark (none...)
write/keyw  badpix/C/1/60  irsbadpix     ! Default name of badpix table

!
! descriptors used...
!
write/keyw  Icw/C/1/30     _EI_CENWAVL/d/1/1    ! name of CW.
write/keyw  Igrat/C/1/30   _EI_GRATING/i/1/1    ! # of the grating
write/keyw  Iorder/C/1/30  _EI_ORDER/i/1/1      ! grating order used
write/keyw  Idit/C/1/30    _ED_DIT/d/1/1        ! DIT used
write/keyw  Itilt/C/1/30   IRS_RECTIFIED/i/1/1  ! Already_tilted flag
write/keyw  Iresp/C/1/30   IRS_RESPONSE/i/1/1   ! 1D-response_frame flag
write/keyw  Ifactor/C/1/30 IRS_FACTOR/r/1/1     ! sky-factor applied by SKYSUB
write/keyw  Ishift/C/1/30  IRS_SHIFT/r/1/1      ! obj-shift applied by SKYSUB
write/keyw  Iwlcal/C/1/30  IRS_WLCAL/d/1/2      ! wl-calibration parameters
!End Session List
! create default badpix table in local directory
!
crea/table 'badpix' 2 20 null
crea/column 'badpix' :X_COORD R*4
crea/column 'badpix' :Y_COORD R*4
'badpix',#1,@1 = 36
'badpix',#1,@2 = 26
'badpix',#1,@3 = 12
'badpix',#1,@4 = 16
'badpix',#1,@5 = 35
'badpix',#1,@6 = 43
'badpix',#1,@7 = 52
'badpix',#1,@8 = 57
'badpix',#1,@9 = 57
'badpix',#1,@10 = 56
'badpix',#1,@11 = 62
'badpix',#1,@12 = 56
'badpix',#1,@13 = 61
'badpix',#1,@14 = 57
'badpix',#1,@15 = 60
'badpix',#1,@16 = 61
'badpix',#1,@17 = 60

'badpix',#2,@1 = 2.00000e+00
'badpix',#2,@2 = 1.10000e+01
'badpix',#2,@3 = 1.40000e+01
'badpix',#2,@4 = 2.60000e+01
'badpix',#2,@5 = 3.90000e+01
'badpix',#2,@6 = 3.60000e+01
'badpix',#2,@7 = 5.00000e+01
'badpix',#2,@8 = 4.70000e+01
'badpix',#2,@9 = 4.20000e+01
'badpix',#2,@10 = 3.80000e+01
'badpix',#2,@11 = 3.80000e+01
'badpix',#2,@12 = 3.50000e+01
'badpix',#2,@13 = 3.20000e+01
'badpix',#2,@14 = 3.00000e+01
'badpix',#2,@15 = 2.40000e+01
'badpix',#2,@16 = 2.00000e+01
'badpix',#2,@17 = 1.50000e+01

