! @(#)irsverify.prg	19.1 (ES0-DMD) 02/25/03 14:23:31
crossref flatdark
def/param p1 ? cha "??"

def/loc i/i/1/1 0
def/loc j/i/1/1 0
def/loc n/i/1/1 0
def/loc k/i/1/1 0

if p1(1:1) .eq. "1" then
  if flat(1:1) .eq. "+" then
    write/out "Must first define flat frame using"
    write/out "SET/IRSP FLAT=image_name "
    return/exit
  endif
  if dark(1:1) .eq. "+" then
    write/out "Must first define dark frame using"
    write/out "SET/IRSP DARK=image_name "
    return/exit
  endif
  k = m$index(flat,".")
  if k .le. 0 then
    j = m$exist("{flat}.bdf")
    if j .le. 0 then
      write/out "Flat frame -- {flat} --"
      write/out "does not exist. Use SET/IRSP flat=... to define it"
      return/exit
    endif
  else
    j = m$exist(flat)
    if j .le. 0 then
      write/out "Flat frame -- {flat} --"
      write/out "does not exist. Use SET/IRSP flat=... to define it"
      return/exit
    endif
  endif
  k = m$index(dark,".")
  if k .le. 0 then
    j = m$exist("{dark}.bdf")
    if j .le. 0 then
      write/out "Dark frame-- {dark} --"
      write/out "does not exist. Use SET/IRSP dark=... to define it"
      return/exit
    endif
  else
    j = m$exist(dark)
    if j .le. 0 then
      write/out "Dark frame -- {dark} --"
      write/out "does not exist. Use SET/IRSP dark=... to define it"
      return/exit
    endif
  endif
  write/out "Currently used flat is - {flat} - "
  write/out "Currently used dark is - {dark} - "
  return
endif

set/format i1
n = pcount(1)
do i = 1 'n'
  k = m$index(p{i},".") 
  if k .le. 0 then
    j = m$exist("{p{i}}.bdf")
    if j .le. 0 then
      write/out "Image -- {p{i}} -- does not exist"
      return/exit
    endif
  else
    j = m$exist(p{i})
    if j .le. 0 then
      write/out "File -- {p{i}} -- does not exist"
      return/exit
    endif
  endif
enddo
return
