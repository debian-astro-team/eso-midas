/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
..TYPE        Module
..NAME        tbrules.c
..LANGUAGE    C
..AUTHOR      IPG-ESO Garching
..CATEGORY    Data Organizer utilities 
..COMMENTS       This module implements the following Midas commands:
\begin{TeX}
\begin{enumerate}
\item {\tt CREATE/RULE table rule
\end{enumerate}
\end{TeX}

..VERSION  1.0   15-Mar-1993  Definition     M. Peron

 130703		last modif
-----------------------------------------------------------------*/

#include <atype.h>
#include <tbldef.h>
#include <tblsys.h>
#include <tblerr.h>
#include <midas_def.h>
#include <macrogen.h>
#include <proto_tbl.h>
#define issign(c)               ((c == '+') || (c == '-'))
 


int main()
{
int tid,tid0,nicol,nirow,norow;
int null, dummy,exist;
int colc,colb,colf,cold,colname,col[256];
int i,j,coltype,mycol,nmiss ;
char action[2],*descrname;
char intable[60],inkey[60],label[1+TBL_LABLEN],exptype[60],filename[60];
char text[256];
int kuni;
double dvalue;


SCSPRO("tbcheck");
SCKGETC("IN_A",1L,60L,&dummy,intable);
SCKGETC("IN_B",1L,60L,&dummy,inkey);
SCKGETC("ACTION",1L,1L,&dummy,action);
TCTOPN(intable,F_IO_MODE,&tid0);
TCIGET(tid0,&nicol,&nirow,&dummy,&dummy,&dummy);
TCLSER(tid0,"EXPTYPE",&coltype);
TCLSER(tid0,"FILENAME",&colname);
strcpy(action,"K");
if (action[0] == 'C') {
      TCTINI("TEMP_TAB",F_TRANS,F_IO_MODE,4,nicol,&tid);
      TCCINI(tid,D_C_FORMAT,17L,"A17"," ","COLUMN",&colc);
      for (i=0; i<nicol; i++) {
         TCLGET(tid0,i+1,label);
         TCEWRC(tid,i+1,1L,label);
      }
      TCCINI(tid,D_I4_FORMAT,1,"I1"," ","BIAS",&colb);
      TCCINI(tid,D_I4_FORMAT,1,"I1"," ","DARK",&cold);
      TCCINI(tid,D_I4_FORMAT,1,"I1"," ","FF",&colf);
      }
else {
     TCTOPN(inkey,F_I_MODE,&tid);
     TCIGET(tid,&dummy,&norow,&dummy,&dummy,&dummy);
     descrname = osmmget(norow*(1+TBL_LABLEN));
     for (j=0; j<norow; j++) {
         TCERDC(tid,j+1,1,descrname+j*(1+TBL_LABLEN),&null);
         TCLSER(tid0,descrname+j*(1+TBL_LABLEN),&col[j]);
     }
     for (i=1; i<=nirow; i++) {
         TCERDC(tid0,i,colname,filename,&null);
         TCERDC(tid0,i,coltype,exptype,&null); 
         TCCSER(tid,exptype,&mycol);
         nmiss = 0;
         strcpy(text,"Missing Keywords: ");
         for (j=0; j<norow; j++) {
             TCERDI(tid,j+1,mycol,&exist,&null);
             if (exist) {
                TCERDD(tid0,i,col[j],&dvalue,&null);
                if (null) {
                   strcat(text,descrname+j*(1+TBL_LABLEN));
                   strcat(text," ");
                   nmiss++;
                   }
                }
             }
         if (nmiss) SCDWRC(tid0,filename,1,text,1,256,&kuni);
         }
}
SCSEPI();
return 0;
}

