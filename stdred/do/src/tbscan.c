/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*---------------------------------------------------------------------
.TYPE        Module
.NAME        tbscan.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Data Organizer utilities
.COMMENTS       This module implements the following Midas commands:
\begin{TeX}
\begin{enumerate}
\item
\end{enumerate}
\end{TeX}

.VERSION  1.0   15-Mar-1993   Definition     M. Peron

 090831		last modif
-------------------------------------------------------------------*/

#include <midas_def.h>


extern int ostcrea();


int main()
{
char flist[128],intable[60],outable[60],flag[4],pfix[5];
int dummy,status;

SCSPRO("TBSCAN");
SCKGETC("FLIST",1,128,&dummy,flist);
SCKGETC("PFIX",1,4,&dummy,pfix);
SCKGETC("IN_A",1,60,&dummy,intable);
SCKGETC("OUT_A",1,60,&dummy,outable);
SCKGETC("INPUTC",1,3,&dummy,flag);
status = ostcrea(flist,pfix,intable,outable,flag);
SCSEPI();
return 0;
}
