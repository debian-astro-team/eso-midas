/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
..TYPE        Module
..NAME        tbrules.c
..LANGUAGE    C
..AUTHOR      IPG-ESO Garching
..CATEGORY    Data Organizer utilities 
..COMMENTS       This module implements the following Midas commands:
\begin{TeX}
\begin{enumerate}
\item {\tt CREATE/RULE table rule
\end{enumerate}
\end{TeX}

..VERSION  1.0   15-Mar-1993  Definition     M. Peron

 130703		last modif
-----------------------------------------------------------------*/

#include <atype.h>
#include <tblsys.h>
#include <tblerr.h>
#include <midas_def.h>
#include <str.h>
#include <macrogen.h>

#include <string.h>
#include <stdio.h>

extern int decrypt1(), tbl_hist();


char *mystand[] = { 
                  "TBLENGTH","TBLOFFST","TBLCONTR","TSELTABL",
                   "TLABL"
                  }; 
int main()
{
int status,tid,tid0,nrow,nicol;
int null, len, lennew, dummy;
int colc,colr;
int i,n,unit ;
char line[40],column[1+TBL_LABLEN];
char selecrit[256],outdescr[8],action[2];
char intable[60],label[1+TBL_LABLEN];
char temptable[60],*temp,text[60];

lennew = 0;
SCSPRO("tbrules");
strcpy(temptable,"TEMP_TAB");
SCKGETC("ACTION",1L,1L,&dummy,action);
SCKGETC("INPUTC",1L,8L,&dummy,outdescr);
for (i=0;i<5;i++) 
if ((n = stuindex(outdescr,mystand[i])) == 0 ){
   sprintf(text,"Error writing in standard descriptor: %s",outdescr);
   SCETER(1,text);
   }
SCKGETC("IN_A",1L,60L,&dummy,intable);
TCTOPN(intable,F_IO_MODE,&tid0);
if (action[0] == 'C') {
    TCIGET(tid0,&nicol,&dummy,&dummy,&dummy,&dummy);
    TCTINI("TEMP_TAB",F_TRANS,F_IO_MODE,2,nicol,&tid);
    TCCINI(tid,D_C_FORMAT,13L,"A13"," ","COLUMN",&colc);
    for (i=0; i<nicol; i++) {
        TCLGET(tid0,i+1,label);
        TCEWRC(tid,i+1,1L,label);
    }
    TCCINI(tid,D_C_FORMAT,40L,"A40"," ","RULE",&colr);
    }
else {
    TCTOPN(temptable,F_I_MODE,&tid);
    TCIGET(tid,&dummy,&nrow,&dummy,&dummy,&dummy);
    SCKGETC("INPUTC",1L,8L,&dummy,outdescr);
    temp = osmmget(256);
    oscfill(selecrit,256,'\0');
    for (i=0; i<nrow; i++) {
       status = TCERDC(tid,i+1,2L,line,&null);
       if (null) continue;
       *column = ':';
       TCERDC(tid,i+1,1L,column+1,&null);
       if (selecrit[0] != '\0') strcat(selecrit,".AND.");
       decrypt1(column,line,selecrit,temp);
       lennew = strlen(selecrit);
       len += lennew;
       }
    SCDWRC(tid0,outdescr,1L,selecrit,1L,lennew,&unit);
     }
tbl_hist(tid0);
SCSEPI();
return 0;
}



      
