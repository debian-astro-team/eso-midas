/* @(#)tbfhand.c	19.1 (ESO-DMD) 02/25/03 14:18:06 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

 010802		last modif

===========================================================================*/

#include <atype.h>
#include <tbldef.h>
#include <tblsys.h>
#include <tblerr.h>
#include <midas_def.h>
#include <macrogen.h>
#include <math.h>
#include <proto_tbl.h>

#define MAXBIN 1024
#define  NM 32
#define  NT 5
char *osmmget() ;
main()
{
int dummy,i,j,k,l ;
int status,tid,nrow,kcol,nocat,otid,no,ormf ;
int *create, imno, noelem[256], noelembis,ocol[256],colf;
int *keyival[256], *ipos,ontype;
int null, nl, unit;
int naxis,npix[3];
float *keyrval[256],ms[2];
double *keydval[256],start[3],step[3];
char incata[60],intable[60],outable[60],filename[60];
char *cdummy, type,*keycval[256];
char form[1+TBL_FORLEN],*temp, *otype;
char *keyname,*label,*area,defaul[5];
char *keyword = "descr_iname";
status = SCSPRO("TDATABASE");
status = SCKGETC("IN_A",1,60,&dummy,incata);
status = SCKGETC("IN_B",1,60,&dummy,intable);
status = SCKGETC("OUT_A",1,60,&dummy,outable);

		       /*open table containing keywords names */
status = TCTOPN(intable,F_I_MODE,&tid)  ;
status = TCIGET(tid,&dummy,&nrow,&dummy,&dummy,&dummy);
status = SCCSHO(incata,&nocat,&dummy);

status = TCTINI(outable,F_TRANS,F_IO_MODE,nrow,nocat,&otid);
status = TCCINI(otid,D_C_FORMAT,60L,"A16"," ","FILENAME",&colf);
status = TCCSER(tid,keyword,&kcol);
keyname = osmmget(NM*nrow);
otype = osmmget(NT*nrow);
nl = TBL_LABLEN+1;
label = osmmget(nl*nocat);
ipos = (int *)osmmget(nrow*sizeof(int));
create = (int *)osmmget((nrow+2)*sizeof(int));
for (i=0; i<nrow; i++) {
    TCERDC(tid,i+1,kcol,keyname+i*NM,&null);
    if (null) *(keyname+i*NM) = '\0';
    TCERDC(tid,i+1,4L,otype+i*NT,&null);
    if (null) *(otype+i*NT) = '\0';
    else {
	 temp = otype+i*NT + strskip(otype+i*NT,' ');
	 noelem[i] = 1;
         if ((temp[0] == 'C') || (temp[0] == 'c')) 
	     if (temp[1] == '*') noelem[i] = atoi(temp+2); 
         }
    TCERDI(tid,i+1,2L,ipos+i,&null);
    if (null) *(ipos+i) = 1;
    TCERDC(tid,i+1,3L,label+i*nl,&null);
    if (null) *(label+i*nl) = '\0';
    *(create+i) = 0;  
    if (*(otype+i*NT)) {
       switch (temp[0]) {
	  case 'C': case 'c': 
		  ontype = D_C_FORMAT; 
                  sprintf(form,"A%d",noelem[i]);
                  keycval[i] = osmmget(noelem[i]);
		  break;
	  case 'I': case 'i': 
		  ontype = D_I4_FORMAT; 
		  strcpy(form,"I8");
/*	          keyival[i] = (int *)osmmget(sizeof(int)); */
		  break;
	  case 'R': case 'r': 
		  ontype = D_R4_FORMAT; 
		  strcpy(form,"E12.6");
/*                  keyrval[i] = (float *)osmmget(sizeof(float)); */
		  break;
	  case 'D': case 'd': 
		  ontype = D_R8_FORMAT; 
		  strcpy(form,"D24.17");
/*                  keydval[i] = (double *)osmmget(sizeof(double)); */
		  break;
          }
	 if (!*(label+i*nl)) strncpy(label+i*nl,keyname+i*NM,nl);
         TCCINI(otid,ontype,noelem[i],form," ",label+i*nl,&ocol[i]);
         *(create+i) = 1;
       }
}
filename[0] = '\0';
*(create+nrow) = 0;
ormf = 0;
l =0; no = 0;
for (j=0; j<nocat; j++) {
 status = SCCGET(incata,0,filename,cdummy,&no);
 if (filename[0] != ' ') {
   ormf = MAX(ormf,strlen(filename));
   SCFOPN(filename,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,&imno);
   l++;
   TCEWRC(otid,l,colf,filename);   
   for (i=0; i<nrow; i++) {
     SCDFND(imno,keyname+i*NM,&type,&noelembis,&dummy);
     if (!*(label+i*nl)) strncmp(label+i*nl,keyname+i*NM,nl);
     switch(type) {
      case 'C':
       if (*(create+i) == 0) {
         sprintf(form,"A%d",noelembis);
         TCCINI(otid,D_C_FORMAT,noelembis,form," ",label+i*nl,&ocol[i]);
         *(create+i) = 1;
         keycval[i] = osmmget(noelembis);
         }
         SCDRDC(imno,keyname+i*NM,1L,1L,noelem[i],&dummy,
		     keycval[i],&unit,&null);
         TCEWRC(otid,l,ocol[i],keycval[i]);
         break;
      case 'I':
       if (*(create+i) == 0) {
          TCCINI(otid,D_I4_FORMAT,1,"I8"," ",label+i*nl,&ocol[i]);
          *(create+i) = 1;
	  }
	  keyival[i] = (int *)osmmget(sizeof(int));
          SCDRDI(imno,keyname+i*NM,*(ipos+i),1L,&dummy,keyival[i],&unit,&null);
          TCEWRI(otid,l,ocol[i],keyival[i]);
          break;
       case 'R':
       if (*(create+i) == 0) {
          TCCINI(otid,D_R4_FORMAT,1,"E12.6"," ",label+i*nl,&ocol[i]);
          *(create+i) = 1;
	   }
          keyrval[i] = (float *)osmmget(sizeof(float));
          SCDRDR(imno,keyname+i*NM,*(ipos+i),1L,&dummy,keyrval[i],&unit,&null);
          TCEWRR(otid,l,ocol[i],keyrval[i]);
          break;
        case 'D':
        if (*(create+i) == 0) {
          TCCINI(otid,D_R8_FORMAT,1,"D24.17"," ",label+i*nl,&ocol[i]);
          *(create+i) = 1;
	  }
          keydval[i] = (double *)osmmget(sizeof(double));
          SCDRDD(imno,keyname+i*NM,*(ipos+i),1L,&dummy,keydval[i],&unit,&null);
          TCEWRD(otid,l,ocol[i],keydval[i]);
	  break;
         case ' ':
        if (*(create+i) != 0) TCEDEL(otid,l,ocol[i]); 
      }
    }
   }
   if (*(create+nrow) == 0) {
       TCCINI(otid,D_R4_FORMAT,1,"E15.6"," ","MEAN",&ocol[nrow]);
       TCCINI(otid,D_R4_FORMAT,1,"E15.6"," ","SIGMA",&ocol[nrow+1]);
       *(create+nrow) = 1;
       }
   strcpy(defaul,"YNSNY");
   SCDRDI(imno,"NAXIS",1L,1L,&dummy,&naxis,&unit,&null);
   SCDRDI(imno,"NPIX",1L,naxis,&dummy,npix,&unit,&null);
/*   measig(imno,area,naxis,npix,start,step,defaul,&ms[0],&ms[1]); */
   TCEWRR(otid,l,ocol[nrow],&ms[0]);
   TCEWRR(otid,l,ocol[nrow+1],&ms[1]);
   SCFCLO(imno);
}
sprintf(form,"A%d",ormf);
status = TCFPUT(otid,colf,form);
status = SCSEPI();
}

