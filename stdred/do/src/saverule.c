/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include <midas_def.h>



int main()
{
int tid1,tid2,null,unit,dummy;
char selecrit[257],intable[60],rulename[17];


SCSPRO("saverule");
TCTOPN("TMP_RULE",F_I_MODE,&tid1);
SCKGETC("IN_A",1,59,&dummy,intable);
TCTOPN(intable,F_IO_MODE,&tid2);
SCKGETC("INPUTC",1,16,&dummy,rulename);
TCERDC(tid1,1,1,selecrit,&null);
SCDWRC(tid2,rulename,1,selecrit,1,256,&unit);
TCTCLO(tid1);
TCTCLO(tid2);
SCSEPI();
return 0;

}

