/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbapplyrul.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Data Organizer utilities
.COMMENTS       This module implements the following Midas commands:
\begin{TeX}
\begin{enumerate}
\item {\tt CLASSIFY/IMAGE} table rule column outchar
\end{enumerate}
\end{TeX}

.VERSION  1.0   15-Mar-1993   Definition     M. Peron

 090831		last modif
-----------------------------------------------------------------------*/

#include <tbldef.h>
#include <midas_def.h>


extern void classify();



#define MAXLEN 80


int main()
{
int status,dummy;
char mycval[81],intable[60],assod[60],column[1+TBL_LABLEN];
status = SCSPRO("classify");
SCKGETC("IN_A",1L,60L,&dummy,intable);
SCKGETC("INPUTC",1L,60L,&dummy,assod);
SCKGETC("OUTCOL",1L,TBL_LABLEN+1,&dummy,column);
SCKGETC("VAL",1L,MAXLEN,&dummy,mycval);
classify(intable,assod,column,mycval);
SCSEPI();
return 0;

}
