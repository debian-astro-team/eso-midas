/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbassoap.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Data Organizer utilities
.COMMENTS       This module implements the following Midas commands:
\begin{TeX}
\begin{enumerate}
\item {\tt ASSOCIATE/IMA} table columns exptype table_rule outtab
\end{enumerate}
\end{TeX}

.VERSION  1.0   15-Mar-1993   Definition     M. Peron

 090831		last modif
-----------------------------------------------------------*/

#include <midas_def.h>


extern void associate_ima();



int main()
{
int dummy,icount,unit;
char intabdat[60],intabrul[60],exptype[10],outtab[60],incol[80];
char ocolname[TBL_LABLEN+1],fc[2],imaname[60];


SCSPRO("tbssoap");
SCKGETC("IN_A",1,60,&dummy,intabdat);
SCKGETC("INPUTC",1,10,&dummy,exptype);
SCKGETC("INCOL",1,80,&dummy,incol);
SCKGETC("IN_B",1,60,&dummy,intabrul);
SCKGETC("OUT_A",1,60,&dummy,outtab);
SCKGETC("FC",1L,1,&dummy,fc);
SCKGETC("IMANAME",1,60,&dummy,imaname);
SCKGETC("OUTCOLA",1,TBL_LABLEN,&dummy,ocolname);
SCKRDI("ICOUNT",1,1,&dummy,&icount,&unit,&dummy);
associate_ima(intabdat,exptype,incol,intabrul,outtab,fc,imaname,ocolname,icount);
SCSEPI();
return 0;

}
