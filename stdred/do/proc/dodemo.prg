! 
! Prodedure: dodemo.prg
! Author   : M. Peron  -  ESO Garching
! Date     : 931201
!
! Purpose  : Demonstration procedure of new do package
!
! Syntax   : tutorial/do
!
! 040831	last modif
!
echo/off
@s dodemo,cpy MID_TEST:susi0001.mt susi0001.mt
@s dodemo,cpy MID_TEST:susi0002.mt susi0002.mt
@s dodemo,cpy MID_TEST:susi0003.mt susi0003.mt
@s dodemo,cpy MID_TEST:susi0004.mt susi0004.mt
@s dodemo,cpy MID_TEST:susi0005.mt susi0005.mt
@s dodemo,cpy MID_TEST:susi0006.mt susi0006.mt
@s dodemo,cpy MID_TEST:susi0007.mt susi0007.mt
@s dodemo,cpy MID_TEST:susi0008.mt susi0008.mt
@s dodemo,cpy MID_TEST:susi0009.mt susi0009.mt
@s dodemo,cpy MID_TEST:susi0010.mt susi0010.mt
@s dodemo,cpy MID_TEST:susi0011.mt susi0011.mt
@s dodemo,cpy MID_TEST:susi0012.mt susi0012.mt
@s dodemo,cpy MID_TEST:susi0013.mt susi0013.mt
@s dodemo,cpy MID_TEST:susi0014.mt susi0014.mt
indisk/fits MID_TEST:susi_descr.tfits susi_descr.tbl >Null
indisk/fits MID_TEST:bias_rule.tfits bias_rule.tbl >Null
indisk/fits MID_TEST:ff_rule.tfits ff_rule.tbl >Null
indisk/fits MID_TEST:template.fits template.bdf >Null
WRITE/OUT "You have in your directory a set of 14 FITS files:" 
WRITE/OUT 
-dir susi0*.mt
WRITE/OUT
WRITE/OUT "These FITS files are NTT exposures obtained with the SUSI instrument"
WRITE/OUT "The goal of this demo is to associate to each  scientific"
WRITE/OUT "exposure two BIAS and two FLAT-FIELD exposures, taking in account"
WRITE/OUT "the instrument and detector setups and other observations parameters"
WRITE/OUT "which seem relevant for the reduction process."

WRITE/OUT "The command CREATE/OST stores the needed  information"
WRITE/OUT "in a table called Observation Summary Table"
WRITE/OUT "The command uses as input a list of MIDAS or FITS files"
WRITE/OUT "as well as a table containing a set of MIDAS descriptors"
WRITE/OUT "describing the observation."
WRITE/OUT "Each of these descriptors will be mapped into one column "
WRITE/OUT "of the OST and the corresponding information for a given input "
WRITE/OUT "file is stored into one of its rows."
WRITE/OUT
WRITE/OUT "One has created a table susi_descr containing  a list of"
WRITE/OUT " descriptors:"
WAIT 8
echo/on
READ/TAB susi_descr
echo/off
WAIT 3
WRITE/OUT "For instance the descriptor ESO.INS.OPTI-2.ID that contains the 
WRITE/OUT "filter number that what used will be mapped into a column of"
WRITE/OUT "the OST labeled :SFILTER_ID with the type C*8"
WAIT 2
CREATE/OST susi0*.mt ? susi_descr  susi_ost FC
show/tab susi_ost
read/tab susi_ost :filename :ident :sfilter_no
WAIT 2
ECHO/OFF
COPY/dd template FF susi_ost.tbl FF
COPY/dd template BIAS susi_ost.tbl BIAS
COPY/dd template SCI susi_ost.tbl SCI
COPY/dd template SIMAGING susi_ost.tbl SIMAGING
WRITE/OUT "In order to get an overview on the data one has, one wish to "
WRITE/OUT "classify the data into a set of groups.
WRITE/OUT "One wants first to classify the images according to their exposure"
WRITE/OUT "type and one needs for that purpose to define a set of" 
WRITE/OUT "classification rules"
!WRITE/OUT "The command CREATE/CRULE will create a temporary table and enters"
!WRITE/OUT "the table editor". The first column contains the labels of the "
!WRITE/OUT "columns of the OST while the constraints to be applied on the values"
!WRITE/OUT "of these columns may be entered in the second one"
!WRITE/OUT "For instance, the rule for classifying the BIAS may in natural "
!WRITE/OUT "language be: select all the frames for which the descriptor IDENT"  
!WRITE/OUT "contains the substring BIAS"
!WRITE/OUT "This constraint may be entered in the table editor by typing:"
!WRITE/OUT =BIAS*  at the row 7 in the second column of the table.
WRITE/OUT "A set of default rules for SUSI has been written. For instance, are"
WRITE/OUT "classified as BIAS all exposures having a descriptor IDENT"
WRITE/OUT "that contains the substring BIAS"
WAIT 8
WRITE/OUT "The constraint has been translated into a MIDAS selection criteria"
WRITE/OUT "and stored into the descriptor BIAS of the table susi_ost."
ECHO/ON
read/descr susi_ost.tbl BIAS
echo/off
WAIT 1
WRITE/OUT "The same kind of rules may be defined to classify FF and SCI "
WRITE/OUT "exposures. For instance,exposures may be classified as FF if their"
WRITE/OUT descriptor IDENT contains the substring FF* 
WAIT 2
ECHO/OFF
WRITE/OUT "The command CLASSIFY/IMAGE susi_ost BIAS EXPTYPE BIAS will now"
WRITE/OUT "select all the frames which satisfy the classification criteria "
WRITE/OUT "stored into the descriptor BIAS of the table susi_ost and will flag"
WRITE/OUT "them with the character string BIAS that will be stored in the "
WRITE/OUT "column EXPTYPE of the OST"
WAIT 2
ECHO/ON
CLASSIFY/IMAGE susi_ost BIAS EXPTYPE BIAS
READ/TAB susi_ost :EXPTYPE
ECHO/OFF
WRITE/OUT "The same procedure is applied to flag the FF exposures with the"
WRITE/OUT character string FF and the SCI exposures with the string SCI 
WAIT 1
ECHO/ON
CLASSIFY/IMAGE susi_ost FF EXPTYPE FF
READ/TAB susi_ost :EXPTYPE
ECHO/OFF
WAIT 4
ECHO/ON
CLASSIFY/IMAGE susi_ost SCI EXPTYPE SCI
READ/TAB susi_ost :EXPTYPE
ECHO/OFF
WAIT 4
WRITE/OUT One should now define a rule for classify the exposures according 
WRITE/OUT to the optical path that was used. In our case, this consists simply
WRITE/OUT in classify the frames according to the filter number that was used.
WRITE/OUT This parameter is stored into the column SFILTER_ID of the ost while
WRITE/OUT the column SFILTER_TYPE contains either the substring "FILTER" or
WRITE/OUT the substring FREE
READ/DESCR susi_ost.tbl SIMAGING
WRITE/OUT One wants in some way to store the filter number into the character
WRITE/OUT string flagging the files satisfying the criteria. This may be done
WRITE/OUT by using the & replacement character in the command CLASSIFY
WRITE/OUT The command CLASSIFY/IMAGE susi_ost SIMAGING OPATH SFILT_&2 will look
WRITE/OUT for the 2d  occurence of the character * into the rule  
WRITE/OUT (:SFILTER_TYPE.EQ."FILTER*".AND.:SFILTER_ID.EQ."#*"), look for the 
WRITE/OUT corresponding column (:SFILTER_ID) and append to the substring SFILT_
WRITE/OUT the contents of this column.
WAIT 8
ECHO/ON
CLASSIFY/IMAGE susi_ost SIMAGING OPATH SFILT_&2
READ/TAB susi_ost :OPATH
ECHO/OFF
WAIT 4
COMPUTE/TABLE susi_ost :MJD_LOC = :MJD+0.375-49009.0
select/tab susi_ost all
WRITE/OUT The next step consists in associating to each scientific frame
WRITE/OUT two bias and two FF exposures.
WRITE/OUT The general philosophy of the command ASSOCIATE is as follows:
WRITE/OUT One defines two sets of association criteria, a "first choice" 
WRITE/OUT set of criteria and a second choice set of criteria.
WRITE/OUT The process looks for the frames that satisfy the first choice
WRITE/OUT set of criteria and expands its search using the second choice
WRITE/OUT set of criteria if not enough calibration frames are found.
WRITE/OUT For instance, one could in natural language say: 
WRITE/OUT Look for 2 bias observed within the same night and if not found
WRITE/OUT look for the exposures done within the same observing run.
WAIT 8
WRITE/OUT The following plot illustrates these two set of criteria
WRITE/OUT The scientific frames is being represented by a circle why the
WRITE/OUT bias are represented by crosses.
CREATE/GRAPH
@s plotb1
WRITE/OUT It is clear that the process will be able to associate two bias
WRITE/OUT to the frame susi0006 satisfying the first choice set of
WRITE/OUT association criteria
WRITE/OUT Let's have now a look at the image susi0009
@s plotb2 
WRITE/OUT In this case, only bias satisfying the second choice set of 
WRITE/OUT association criteria will be found.
WRITE/OUT One needs now to choose the best ones, i.e to rank the selected
WRITE/OUT ones. This is done by assigning weights to the individual criteria.
WRITE/OUT The files susi0002 and susi0002 will be choosen if one gives more
WRITE/OUT importance to the temperature while the files susi0010 and
WRITE/OUT susi0011 will be choosen if one gives more importance to the
WRITE/OUT date/time of the observations.
select/table susi_ost all
associate/image susi_ost bias bias_rule susi_asso C 2
WRITE/OUT "The association process generates an output table:"
read/tab susi_asso
WRITE/OUT Let's try now to find two suitable Flat-Field exposures for susi0006
WRITE/OUT and susi0007. One has to make sure that one associates only frames 
WRITE/OUT obtained in the same  instrument configuration. 
WRITE/OUT Let's first have a look at susi0006
WRITE/OUT The following plot illustrates the two set of criteria mentioned
WRITE/OUT above. The scientific frames is being represented by a circle 
WRITE/OUT while the flat-fields are represented by crosses of different colors.
WAIT 5
@s plotff1
WRITE/OUT It is clear that the process will be able to associate only one
WRITE/OUT flat-field satisfying the second set of association criteria
WRITE/OUT Let's  have now a look at susi0007
WAIT 2
@s plotff2
WRITE/OUT Two suitable flat-field satisfying the first set of selection criteria
WRITE/OUT will be found
select/table susi_ost all
associate/image susi_ost ff ff_rule susi_asso A 2
WRITE/OUT "The table containing the associations has been updated:
read/table susi_asso
write/out 
write/out ...end of tutorial...
write/out 
! 
ENTRY CPY
DEFINE/PARAM   P1   ?   C  "Source:"
DEFINE/PARAM   P2   ?   C  "Object:"


IF M$EXIST(P2) .EQ. 0  THEN
        -COPY  {P1}   {P2}
        WRITE/OUT "Copied {P2} from {P1}"
ENDIF

RETURN

