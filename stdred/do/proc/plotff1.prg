! @(#)plotff1.prg	19.1 (ES0-DMD) 02/25/03 14:17:52
set/graph
write/tab susi_ost @3 :DET_TEMP 172.11
write/tab susi_ost @4 :DET_TEMP 172.12
write/tab susi_ost @5 :DET_TEMP 172.13
write/tab susi_ost @14 :DET_TEMP 172.12
set/graph xformat=f5.1
plot/axes 171.5,172.7 -0.7,2.3 ? "DET_TEMP" "DATE"
set/graph stype=2 color=4
select/tab susi_ost seq.eq.6
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph  stype=5
select/tab susi_ost :EXPTYPE.eq."FF*".and.:OPATH.eq."SFILT_642*"
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph color=6
select/tab susi_ost :EXPTYPE.eq."FF*".and.:OPATH.eq."SFILT_639*"
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph color=3
select/tab susi_ost :EXPTYPE.eq."FF*".and.:OPATH.eq."SFILT_640*"
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph color=1
overplot/line 4 171.95,-0.042 172.25,-0.042
overplot/line 4 171.95,-0.042 171.95,0.958
overplot/line 4 171.95,0.958 172.25,0.958
overplot/line 4 172.25,-0.042 172.25,0.958
overplot/line 4 171.75,2 171.85,2
label/graph "first choice selection criteria" 172.2,2
overplot/line 5 171.6,-0.542 172.6,-0.542
overplot/line 5 171.6,-0.542 171.6,1.548
overplot/line 5 171.6,1.548 172.6,1.548
overplot/line 5 172.6,-0.542 172.6,1.548
overplot/line 5 171.75,1.8 171.85,1.8
label/graph "second choice selection criteria" 172.2,1.8
label/graph "6" 172.1,0.6
label/graph "3" 172.09,0.25
label/graph "4" 172.12,0.25
label/graph "5" 172.15,0.25
label/graph "13" 172.09,1.25
label/graph "14" 172.15,1.25
