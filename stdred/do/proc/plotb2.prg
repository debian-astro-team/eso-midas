! @(#)plotb2.prg	19.1 (ES0-DMD) 02/25/03 14:17:52
set/graph
set/graph xformat=f5.1 font=1
plot/axes 171.6,173.0 -0.7,2.3 ? "DET_TEMP" "DATE"
set/graph stype=2
select/tab susi_ost seq.eq.9
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph  stype=5
select/tab susi_ost :EXPTYPE.eq."BIAS*"
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph color=1
overplot/line 4 172.15,0.023 172.45,0.023
overplot/line 4 172.15,0.023 172.15,1.023
overplot/line 4 172.15,1.023 172.45,1.023
overplot/line 4 172.45,0.023 172.45,1.023
overplot/line 4 171.75,2 171.85,2
set/graph  font=0
label/graph "first choice selection criteria" 172.4,2
overplot/line 5 171.8,-0.477 172.8,-0.477
overplot/line 5 171.8,-0.477 171.8,1.548
overplot/line 5 171.8,1.548 172.8,1.548
overplot/line 5 172.8,-0.477 172.8,1.548
overplot/line 5 171.75,1.8 171.85,1.8
label/graph "second choice selection criteria" 172.4,1.8
label/graph "9" 172.3,0.4
label/graph "1" 172.1,0.23
label/graph "2" 172.0,0.25
label/graph "10" 172.37,1.17
label/graph "11" 172.37,1.3
label/graph "12" 172.37,1.43
set/graph tsize=1
!overplot/symbol 2 3.6,7 
!label/graph "scientific exposure" 5.7,7
!overplot/symbol 5 3.6,6.3
!label/graph "dark exposure" 5.2,6.3
