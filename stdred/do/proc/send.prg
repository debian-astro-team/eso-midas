! @(#)send.prg	19.1 (ES0-DMD) 02/25/03 14:17:53
DEFINE/PAR P1 ? CHAR "Name of  FITS file"
WRITE/KEY MYINPUT/C/1/60 $HOME/{P1}.mt
WRITE/KEY ASSOIN/C/1/60 {P1}.mt
!WRITE/KEY MYINPUT $HOME/{P1}.mt
IF M$EXIST("TAB_COMM.tbl") .GT. 0 THEN
COPY/TK TAB_COMM #2 @1 INPUTC/C/1/60
IF INPUTC(1:1) .EQ. "?" THEN
   IF M$EXIST("ost.tbl") .GT. 0 THEN
      IF M$EXISTD("ost.tbl","table_descr") .EQ. 0 THEN
          WRITE/DESCR ost.tbl TABLE_DESCR/C/1/60 "emmi_template"
      ENDIF
      CREATE/OST {MYINPUT} ? {ost.tbl,table_descr} ost.tbl FAF
      WRITE/TABLE TAB_COMM #3 @1 "{ost.tbl,table_descr}"
      CLASSIFY/IMAGE ost OPATH_CLAS.tbl
      CLASSIFY/IMAGE ost EXPTYPE_CLAS.tbl
   ELSE
      CREATE/OST {MYINPUT} ? emmi_template ost FCF
      COPY/DD template *,3 ost.tbl
      WRITE/TABLE TAB_COMM #3 @1 "emmi_template"
      CLASSIFY/IMAGE ost OPATH_CLAS.tbl
      CLASSIFY/IMAGE ost EXPTYPE_CLAS.tbl
   ENDIF
WRITE/TABLE TAB_COMM #4 @1 0
WRITE/TABLE TAB_COMM #2 @1 "ost.tbl"
ELSE
   IF M$EXIST("{TAB_COMM,#2,@1}") .GT. 0 THEN
      WRITE/TABLE TAB_COMM #4 @1 1
      CREATE/OST  {MYINPUT} ? {TAB_COMM,#3,@1}  {TAB_COMM,#2,@1} FAF
      CLASSIFY/IMAGE {TAB_COMM,#2,@1} OPATH_CLAS.tbl
      CLASSIFY/IMAGE {TAB_COMM,#2,@1} EXPTYPE_CLAS.tbl
   ELSE
      CREATE/OST {MYINPUT} ? emmi_template ost FCF
      COPY/DD template *,3 ost.tbl
      WRITE/TABLE TAB_COMM #3 @1 "emmi_template"
      CLASSIFY/IMAGE ost OPATH_CLAS.tbl
      CLASSIFY/IMAGE ost EXPTYPE_CLAS.tbl
   ENDIF
ENDIF
RUN STD_EXE:SEND
ELSE
IF M$EXIST("ost.tbl") .GT. 0 THEN
      IF M$EXISTD("ost.tbl","table_descr") .EQ. 0 THEN
          WRITE/DESCR ost.tbl TABLE_DESCR/C/1/60 "emmi_template"
      ENDIF
      CREATE/OST {MYINPUT} ? {ost.tbl,table_descr} ost.tbl FAF
      CLASSIFY/IMAGE ost OPATH_CLAS.tbl
      CLASSIFY/IMAGE ost EXPTYPE_CLAS.tbl
ELSE
      CREATE/OST {MYINPUT} ? emmi_template ost FCF
      COPY/DD template *,3 ost.tbl
      CLASSIFY/IMAGE ost OPATH_CLAS.tbl
      CLASSIFY/IMAGE ost EXPTYPE_CLAS.tbl
ENDIF
ENDIF
IF M$EXIST("ost_asso.tbl") .GT. 0 THEN
   ASSOCIATE/IMAGE ost,{ASSOIN} BIAS bias_rule ost_asso A 2
   ASSOCIATE/IMAGE ost,{ASSOIN} FFSKY ff_rule ost_asso A 2
   ASSOCIATE/IMAGE ost,{ASSOIN} FFDOME ff_rule ost_asso A 2
   ASSOCIATE/IMAGE ost,{ASSOIN} FFLAMP ff_rule ost_asso A 2
ELSE
   ASSOCIATE/IMAGE ost,{ASSOIN} BIAS bias_rule ost_asso C 2
   ASSOCIATE/IMAGE ost,{ASSOIN} FFSKY ff_rule ost_asso A 2
   ASSOCIATE/IMAGE ost,{ASSOIN} FFDOME ff_rule ost_asso A 2
   ASSOCIATE/IMAGE ost,{ASSOIN} FFLAMP ff_rule ost_asso A 2
ENDIF
!
! *** the following copies the association table to the data directory
COPY/TAB ost_asso.tbl ../ccd_asso.tbl
