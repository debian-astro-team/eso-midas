! @(#)plotb1.prg	19.1 (ES0-DMD) 02/25/03 14:17:52
set/graph
set/graph xformat=f5.1 font=1
plot/axes 171.5,172.8 -0.7,2.3 ? "DET_TEMP" "DATE"
set/graph stype=2 
select/tab susi_ost seq.eq.6
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph  stype=5
select/tab susi_ost :EXPTYPE.eq."BIAS*"
overplot/tab susi_ost :DET_TEMP :MJD_LOC
overplot/line 4 171.95,-0.042 172.25,-0.042
overplot/line 4 171.95,-0.042 171.95,0.958
overplot/line 4 171.95,0.958 172.25,0.958
overplot/line 4 172.25,-0.042 172.25,0.958
overplot/line 4 171.6,2 171.65,2
label/graph "first choice selection criteria" 172.2,2
overplot/line 5 171.6,-0.542 172.6,-0.542
overplot/line 5 171.6,-0.542 171.6,1.548
overplot/line 5 171.6,1.548 172.6,1.548
overplot/line 5 172.6,-0.542 172.6,1.548
overplot/line 5 171.6,1.8 171.65,1.8
label/graph "second choice selection criteria" 172.2,1.8
set/graph tsize=0.7 font=0
label/graph "6" 172.1,0.6
label/graph "1" 172.1,0.23
label/graph "2" 172.0,0.25
label/graph "10" 172.37,1.17
label/graph "11" 172.37,1.3
label/graph "12" 172.37,1.43
