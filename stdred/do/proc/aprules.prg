! @(#)aprules.prg	19.1 (ES0-DMD) 02/25/03 14:17:50
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS procedure  aprules.prg to implement APPLY/RULE
!
!  M.Peron                ESO - Garching    15 MAR 93
!
!  use via CLASSIFY/IMAGE table indescr outcol outchar
!
! ---------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Input table"
DEFINE/PARAM P2 ? CHAR "Descriptor or table name"
DEFINE/PARAM P3 + CHAR "Output Column"
DEFINE/PARAM P4 + CHAR "Value for true"
DEFINE/LOCAL NAME/C/1/60 " " ALL
WRITE/KEYW IN_A 'P1'
WRITE/KEYW INPUTC 'P2'
WRITE/KEYW OUTCOL/C/1/16 'P3'
WRITE/KEYW VAL/C/1/80 "{P4}"
WRITE/KEYW STRING/C/1/256 " " all
WRITE/KEYW VAL/C/1/80 "{P4}"
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
RUN STD_EXE:TBAPPLYRUL
