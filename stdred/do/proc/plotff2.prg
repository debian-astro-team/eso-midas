! @(#)plotff2.prg	19.1 (ES0-DMD) 02/25/03 14:17:52
set/graph
set/graph xformat=f5.1
plot/axes 171.4,172.6 -0.7,2.3 ? "DET_TEMP" "DATE"
set/graph stype=2 color=4
select/tab susi_ost seq.eq.7
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph  stype=5
select/tab susi_ost :EXPTYPE.eq."FF*".and.:OPATH.eq."SFILT_639*"
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph color=6
select/tab susi_ost :EXPTYPE.eq."FF*".and.:OPATH.eq."SFILT_642*"
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph color=3
select/tab susi_ost :EXPTYPE.eq."FF*".and.:OPATH.eq."SFILT_640*"
overplot/tab susi_ost :DET_TEMP :MJD_LOC
set/graph color=1
overplot/line 4 171.85,-0.033 172.15,-0.033
overplot/line 4 171.85,-0.033 171.85,0.967
overplot/line 4 171.85,0.967 172.15,0.967
overplot/line 4 172.15,-0.033 172.15,0.967
overplot/line 4 171.75,2 171.85,2
label/graph "first choice selection criteria" 172.2,2
overplot/line 5 171.5,-0.542 172.5,-0.542
overplot/line 5 171.5,-0.542 171.5,1.548
overplot/line 5 171.5,1.548 172.5,1.548
overplot/line 5 172.5,-0.542 172.5,1.548
overplot/line 5 171.75,1.8 171.85,1.8
label/graph "second choice selection criteria" 172.2,1.8
label/graph "7" 172.0,0.3
label/graph "3" 172.09,0.25
label/graph "4" 172.12,0.25
label/graph "5" 172.15,0.25
label/graph "13" 172.09,1.25
label/graph "14" 172.15,1.25


