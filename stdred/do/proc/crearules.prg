! @(#)crearules.prg	19.1 (ES0-DMD) 02/25/03 14:17:51
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS procedure  crearules.prg to implement CREATE/RULE
!
!  M.Peron                ESO - Garching    15 MAR 93
!
!  use via CREATE/CRULE table outdescr
!
! ----------------------------------------------------------------------
!

DEFINE/PARAM P1 ? TABLE "input table"
DEFINE/PARAM P2 ? CHAR "Output descriptor"
DEFINE/LOCAL EXIST/I/1/1 0
DEFINE/LOCAL NAME/C/1/60 " " ALL
WRITE/KEYW IN_A 'P1'
WRITE/KEYW INPUTC 'P2'
IF M$INDEX(P1,".tbl") .GT. 0 THEN
   WRITE/KEYW NAME 'P1'
ELSE
   WRITE/KEYW NAME 'P1'.tbl
ENDIF
WRITE/KEYW ACTION/C/1/1 "C"
RUN STD_EXE:TBRULES
EDIT/TABLE TEMP_TAB
WRITE/KEYW 'INPUTC'/C/1/256 " " ALL
WRITE/KEYW ACTION/C/1/1 "I"
EXIST = M$EXISTD(NAME,"{P2}")
IF EXIST .EQ. 1 DELETE/DESCR {NAME}  {P2}
WRITE/KEYW HISTORY "CREATE/CRULE "
RUN STD_EXE:TBRULES
READ/DESCR {P1}.tbl  {P2}
