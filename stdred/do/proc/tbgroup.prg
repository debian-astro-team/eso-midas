! @(#)tbgroup.prg	19.1 (ES0-DMD) 02/25/03 14:17:53
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS procedure  tbgroup.prg to implement GROUP/ROW
!
!  M.Peron                ESO - Garching    15 MAR 93
!
!  use via GROUP/ROW table incol outcol flag
!
! ----------------------------------------------------------------------
!

DEFINE/PARAM P1 ? TABLE "Input data table"
DEFINE/PARAM P2 ? CHAR "attribute"
DEFINE/PARAM P3 ? CHAR "output column"
DEFINE/PARAM P4 N CHAR 
DEFINE/LOCAL NAME/C/1/60 " " ALL
WRITE/KEYW IN_A 'P1'
WRITE/KEYW INPUTC 'P2'
WRITE/KEYW OUTPUTC 'P3'
WRITE/KEYW ACTION/C/1/1 'P4'
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
RUN STD_EXE:TBGROUP
