! @(#)check.prg	19.1 (ES0-DMD) 02/25/03 14:17:51
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS procedure  crearules.prg to implement CREATE/RULE
!
!  M.Peron                ESO - Garching    15 MAR 93
!
!  use via CREATE/CRULE table outdescr
!
! ----------------------------------------------------------------------
!

DEFINE/PAR P1 ? TABLE "input table"
DEFINE/LOCAL EXIST/I/1/1 0
DEFINE/LOCAL NAME/C/1/60 " " ALL
WRITE/KEY IN_A 'P1'
IF M$INDEX(P1,".tbl") .GT. 0 THEN
   WRITE/KEY NAME 'P1'
ELSE
   WRITE/KEY NAME 'P1'.tbl
ENDIF
RUN STD_EXE:TBCHECK
!EDIT/TAB TEMP_TAB
