! @(#)assoima.prg	19.1 (ES0-DMD) 02/25/03 14:17:50
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS procedure  assoima.prg to implement ASSOCIATE/IMA
!
!  M.Peron                ESO - Garching    15 MAR 93
!
!  use via ASSOCIATE/IMAGE table incols exptype table_rule outtab
!
! ----------------------------------------------------------------------
!

DEFINE/PARAM P1 ? TABLE "Input data table"
!DEFINE/PARAM P2 + CHAR "columns"
DEFINE/PARAM P2 ? CHAR "exp type"
DEFINE/PARAM P3 ? TABLE "Intable rule table"
DEFINE/PARAM P4 ? CHAR "Output Table"
DEFINE/PARAM P5 C CHAR
DEFINE/PARAM P6 1 NUM 
DEFINE/LOCAL NAME/C/1/60 " " ALL
DEFINE/LOCAL L/I/1/1 0
WRITE/KEY IMANAME/C/1/60 " " all
WRITE/KEY OUTCOLA/C/1/16 " " all
L = M$INDEX(P1,",")
IF L .GT. 0 THEN
   L = L-1
   WRITE/KEYW IN_A {P1(1:{L})}
   L = L+2
   WRITE/KEYW IMANAME {P1({L}:>)}
ELSE
   WRITE/KEYW IN_A 'P1'
ENDIF   
WRITE/KEYW INCOL/C/1/80 "+"
WRITE/KEYW INPUTC 'P2'
WRITE/KEYW IN_B 'P3'
L = M$INDEX(P4,",")
IF L .GT. 0 THEN
   L = L-1
   WRITE/KEYW OUT_A {P4(1:{L})}
   L = L+2
   IF P4({L}:{L}) .EQ. ":"  THEN
    L = L+1
   ENDIF
WRITE/KEYW OUTCOLA {P4({L}:>)}
ELSE
   WRITE/KEYW OUT_A 'P4'
ENDIF   
WRITE/KEYW ROWPLUS/I/1/1  0
WRITE/KEYW FC/C/1/1 'P5'
WRITE/KEYW ICOUNT/I/1/1 'P6'
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
RUN STD_EXE:TBASSOAP
