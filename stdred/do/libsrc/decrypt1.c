/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*

.VERSION
 090831		last modif

*/


#include <midas_def.h>
#include <str.h>
#include <string.h>

extern int sstrfind(), ssdelim(), sstsnum();


int decrypt1(column,line,selecrit,temp)
char *line,*selecrit,*column,*temp;
{
int i,k,str;
str = 0;
if (!*line) return(0);
oscfill(temp,256,'\0');
strcat(selecrit,column);
if (!*line) return(0);
else  {
      if (sstrfind(line,"~*[=")) { 
    /*       strcat(selecrit,".EQ."); */
	   if (*line == '=') line++;
           }
      else if ((*line == '!') && (*(line+1) == '=')) {
              line += 2;
              str = 1;
              strcat(selecrit,".NE.");
              }
      else if (*line == '<') {
              str = 1;
	      line++;
	      if (*line == '=') {
		  strcat(selecrit,".LE.");
		  line++;
		  }
	      else strcat(selecrit,".LT.");
	      }
      else if (*line == '>') {
              str = 1;
	      line++;
	      if (*line == '=') {
                  strcat(selecrit,".GE.");
                  line++;
                  }
	      else strcat(selecrit,".GT.");
	      }
/*      else strcat(selecrit,".EQ."); */
      i = 0;
      while(!ssdelim(line) && (*line)) {
          *(temp+i) = *line++;
	  i++;
          }
      k = strindex(temp,"..");
      if (temp[k]) {
	   strcat(selecrit,".GE.");
	   strncat(selecrit,temp,k);
           strcat(selecrit,".AND.");
           strcat(selecrit,column);
	   temp = temp+k+2;
	   strcat(selecrit,".LE.");
	   strcat(selecrit,temp);
	   }
      else {
           if (!str) strcat(selecrit,".EQ.");
           if (sstsnum(temp) <= 0) {
               strcat(selecrit,"\"");
               strcat(selecrit,temp);
               strcat(selecrit,"\"");
               }
           else strcat(selecrit,temp);
           }
      if (*line == '|')  strcat(selecrit,".OR.");
      else if (*line == '&') strcat(selecrit,".AND.");
      if (*line == '\0') return(0);
      line++;
      decrypt1(column,line,selecrit,temp);
      }
return 0;
}
