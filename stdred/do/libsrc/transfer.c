/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090831		last modif

===========================================================================*/

#include <fitsfmt.h>
#include <fitsdef.h>


void transfer(bfdef,buffer,image,n,incr)
BFDEF *bfdef;
char *buffer;
float *image;
int n,*incr;
{
int dfmt,i ;
float fac;
float zero;

dfmt = bfdef->bitpix;
fac = bfdef->bscale;
zero = bfdef->bzero;
i=0;
while (n > 0) {
  switch (dfmt) {

     case 16: *(image + *incr) = *(short *)(buffer + i*2) * fac + zero;
              n-=2;
              (*incr)++;
              i++;
              break;
     case 32: *(image + *incr) = *(int *)(buffer + i*4) * fac + zero;
              n-=4;
              (*incr)++;
              i++;
              break;
     case -32: *(image + *incr) = *(float *)(buffer + i*4) ;
              n-=4;
              (*incr)++;
              i++;
              break;
     case -64: *(image + *incr) = *(double *)(buffer + i*8);
              n-=8;
              (*incr)++;
              i++;
              break;
     }
 }
}
