/* @(#)doutil.c	19.1 (ES0-DMD) 02/25/03 14:17:41 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include <ctype.h>		/* Def for isdigit */
#define issign(c)               ((c == '+') || (c == '-'))
int sstrfind(ch,s)
char *ch,*s;
{
    while (*s) if (*s++==*ch) return 1;
    return 0;
}
int ssdelim(s)
char *s;
{
if (sstrfind(s,"|&") || *s==9 || *s=='\r' || *s==0)
    return 1;
   return 0;
}

int sstsnum(s)
char    *s;     /* IN: String to scan */
{
char    *p, x,y;
p = s;
if    (issign(*p))    p++;
while (isdigit(*p))   p++;
if (*p == '.') {
   x = toupper(*(p+1));
   y = toupper(*(p+2));
   if (( x == 'G') || (x == 'L') || (x == 'N') || (x == 'A') ||
   (x == 'O'))
       return (p-s);
   else if ((x == 'E') && (y == 'Q')) return (p-s);
   else {
        for (++p; isdigit(*p); p++) ;
        x = toupper(*p);
        if ( (x == 'E') || (x == 'D'))        /* Look for Exponent */
        {     p++;
              if    (issign(*p))      p++;
              while (isdigit(*p))     p++;
        }
        }
 }
else {
   x=toupper(*p);
   if ((x == 'E') || (x == 'D')) {
         if (issign(*(p+1)) || isdigit(*(p+1))) {
           p++;
           if    (issign(*p))      p++;
           while (isdigit(*p))     p++;
           }
  }
}

return (p-s);
}

