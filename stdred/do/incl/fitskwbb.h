/* @(#)fitskwbb.h	19.1 (ESO-IPG) 02/25/03 14:17:37 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c)  1994  European Southern Observatory
.LANGUAGE     C
.IDENT        fitskwb.h
.AUTHOR       P.Grosbol   ESO/IPG
.KEYWORDS     FITS, basic keywords, definitions
.PURPOSE      define basic FITS keywords
.VERSION      1.0   1988-Nov-20 :  Creation,  PJG
.VERSION      1.1   1990-Feb-15 :  Include exposure time,  PJG
.VERSION      1.2   1990-Mar-19 :  Change desc. type C*n to 'S',  PJG
.VERSION      1.3   1990-Oct-23 :  Include HIERARCH keyword,  PJG
.VERSION      1.4   1990-Nov-08 :  Change due to KWDEF,  PJG
.VERSION      1.5   1991-Mar-03 :  Add EXTEND, FILENAME etc.,  PJG
.VERSION      1.6   1991-Mar-24 :  Add MIDAFTP + ESO-LOG,  PJG
.VERSION      1.7   1992-Feb-20 :  Add number sign for keywords,  PJG
.VERSION      1.8   1992-Mar-30 :  Add RA, DEC etc. keywords,  PJG
.VERSION      1.9   1992-Sep-02 :  Change map of AIRMASS keywords,  PJG
.VERSION      2.0   1994-Jun-28 :  Get UT from MDJ-OBS,  PJG
.VERSION      2.1   1994-Nov-04 :  Change names of PTYPE,PSCAL,PZERO,  PJG
--------------------------------------------------------------------*/

#define      NOACT          0  /* Keyword group: NO action          */
#define      WDESC          1  /* Keyword group: Write descriptor   */
#define      BFCTL          2  /* Keyword group: Basic FITS control */

#define      BITPIX         1  /* Keyword action for Basic FITS     */
#define      NAXIS          2
#define      CRVAL          3
#define      CRPIX          4
#define      CDELT          5
#define      CTYPE          6
#define      CROTA          7
#define      BSCALE         8
#define      BZERO          9
#define      BLANK         10
#define      BUNIT         11
#define      PCOUNT        12
#define      GCOUNT        13
#define      EXTNAME       14
#define      EXTVER        15
#define      EXTLEVEL      16
#define      RGPTYPE       17
#define      RGPSCAL       18
#define      RGPZERO       19
#define      END           20
#define      OBJECT        21
#define      EXTEND        22
#define      MIDASFTP      23
#define      DATAMIN       24
#define      DATAMAX       25

#define      TMSTART        1  /* Keyword action for descriptors    */
#define      TMEND          2
#define      HIERARCH       3
#define      TEXTFILE       4
#define      MJDOBS         5

static  KWDEF  bkw[] = { 
	 {"HISTORY ",'C',WDESC,0,"HISTORY",    -1,'S', 0.0,""},
	 {"COMMENT ",'C',WDESC,0,"COMMENT",    -1,'S', 0.0,""},
	 {"        ",'C',WDESC,0,"COMMENT",    -1,'S', 0.0,""},
	 {"BITPIX  ",'I',BFCTL,BITPIX,"",       0,'\0',0.0,""},
	 {"NAXIS   ",'I',BFCTL,NAXIS,"",        0,'\0',0.0,""},
	 {"NAXIS###",'I',BFCTL,NAXIS,"",        0,'\0',0.0,""},
	 {"CRVAL###",'R',BFCTL,CRVAL,"",        0,'\0',0.0,""},
	 {"CRPIX###",'R',BFCTL,CRPIX,"",        0,'\0',0.0,""},
	 {"CDELT###",'R',BFCTL,CDELT,"",        0,'\0',0.0,""},
	 {"CTYPE###",'S',BFCTL,CTYPE,"",        0,'\0',0.0,""},
	 {"CROTA###",'R',BFCTL,CROTA,"",        0,'\0',0.0,""},
	 {"BSCALE  ",'R',BFCTL,BSCALE,"",       0,'\0',0.0,""},
	 {"BZERO   ",'R',BFCTL,BZERO,"",        0,'\0',0.0,""},
	 {"BLANK   ",'I',BFCTL,BLANK,"",        0,'\0',0.0,""},
	 {"BUNIT   ",'S',BFCTL,BUNIT,"",        0,'\0',0.0,""},
	 {"PCOUNT  ",'I',BFCTL,PCOUNT,"",       0,'\0',0.0,""},
	 {"GCOUNT  ",'I',BFCTL,GCOUNT,"",       0,'\0',0.0,""},
	 {"EXTNAME ",'S',BFCTL,EXTNAME,"",      0,'\0',0.0,""},
	 {"EXTVER  ",'I',BFCTL,EXTVER,"",       0,'\0',0.0,""},
	 {"EXTLEVEL",'I',BFCTL,EXTLEVEL,"",     0,'\0',0.0,""},
	 {"PTYPE###",'S',BFCTL,RGPTYPE,"",      0,'\0',0.0,""},
	 {"PSCAL###",'R',BFCTL,RGPSCAL,"",      0,'\0',0.0,""},
	 {"PZERO###",'R',BFCTL,RGPZERO,"",      0,'\0',0.0,""},
	 {"END     ",'C',BFCTL,END,"",          0,'\0',0.0,""},
	 {"END END ",'C',BFCTL,END,"",          0,'\0',0.0,""},
	 {"OBJECT  ",'S',BFCTL,OBJECT,"",       0,'\0',0.0,""},
	 {"EXTEND  ",'L',BFCTL,EXTEND,"",       0,'\0',0.0,""},
	 {"MIDASFTP",'S',BFCTL,MIDASFTP,"",     0,'\0',0.0,""},
	 {"DATAMIN ",'R',BFCTL,DATAMIN,"",      0,'\0',0.0,""},	
	 {"DATAMAX ",'R',BFCTL,DATAMAX,"",      0,'\0',0.0,""},
	 {"BLOCKED ",'L',NOACT,0,"",            0,'\0',0.0,""},
	 {"GROUPS  ",'L',NOACT,0,"",            0,'\0',0.0,""},
	 {"SIMPLE  ",'L',NOACT,0,"",            0,'\0',0.0,""},
	 {"XTENSION",'S',NOACT,0,"",            0,'\0',0.0,""},
	 {"ESO-LOG ",'C',WDESC,0,"ESO_LOG",    -1,'S', 0.0,""},
	 {"DATE    ",'T',WDESC,0,"DATE",        1,'R', 0.0,""},
	 {"ORIGIN  ",'S',WDESC,0,"ORIGIN",      1,'S', 0.0,""},
	 {"TELESCOP",'S',WDESC,0,"TELESCOP",    1,'S', 0.0,""},
	 {"OBSERVER",'S',WDESC,0,"OBSERVER",    1,'S', 0.0,""},
	 {"INSTRUME",'S',WDESC,0,"INSTRUME",    1,'S', 0.0,""},
	 {"FILENAME",'S',WDESC,0,"FILENAME",    1,'S', 0.0,""},
         {"RA      ",'R',WDESC,0,"RA",          1,'D', 0.0,""},
         {"DEC     ",'R',WDESC,0,"DEC",         1,'D', 0.0,""},
         {"EXPTIME ",'R',WDESC,0,"EXPTIME",     1,'D', 0.0,""},
         {"LST     ",'R',WDESC,0,"LST",         1,'D', 0.0,""},
         {"UTC     ",'R',WDESC,0,"UTC",         1,'D', 0.0,""},
	 {"DATE-OBS",'T',WDESC,0,"O_TIME",      1,'D', 0.0,""},
	 {"MJD-OBS ",'R',WDESC,MJDOBS,"O_TIME", 4,'D', 0.0,""},
	 {"TM-START",'R',WDESC,TMSTART,"O_TIME",5,'D', 0.0,""},
	 {"TM-END  ",'R',WDESC,TMEND,"O_TIME",  7,'D', 0.0,""},
	 {"POSTN-RA",'R',WDESC,0,"O_POS",       1,'D', 0.0,""},
	 {"POSTN-DE",'R',WDESC,0,"O_POS",       2,'D', 0.0,""},
	 {"EPOCH   ",'R',WDESC,0,"O_POS",       3,'D', 0.0,""},
	 {"EQUINOX ",'R',WDESC,0,"O_POS",       3,'D', 0.0,""},
	 {"AIRMASS ",'R',WDESC,0,"AIRMASS",      1,'R', 0.0,""},
	 {"AUTHOR  ",'S',WDESC,0,"AUTHOR",      1,'S', 0.0,""},
	 {"REFERENC",'S',WDESC,0,"REFERENC",    1,'S', 0.0,""},
	 {"RADECSYS",'S',WDESC,0,"RADECSYS",    1,'S', 0.0,""},
	 {"TEXTFILE",'S',WDESC,TEXTFILE,"TEXTFILE",1,'S',0.0,""},
	 {"HIERARCH",'H',WDESC,HIERARCH,"",     0,'\0',0.0,""},
         {(char *) 0,'\0',0,0,"",0,'\0',0.0,""} };
