/* @(#)mmfsrch.h	19.1 (ESO-IPG) 02/25/03 14:25:37 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT     (c)  1988  European Southern Observatory
.LANGUAGE      C
.IDENT         mmfsrch.h
.AUTHOR        Preben Grosbol  (ESO-IPG)
.KEYWORDS      Object search, definition, structures
.ENVIRONMENT   MIDAS
.COMMENTS      Routines used for Measuring Machine facility
.PURPOSE       Define parameters and structures for the fast
               object search.
.VERSION       1.0    1988-Sep-30 :   Creation,     PJG
------------------------------------------------------------------------*/

typedef struct {                      /* object parameters              */
	       float             x;   /* X position                     */
	       float             y;   /* Y position                     */
	       float            xs;   /* X second moment                */
	       float            ys;   /* Y second moment                */
	       float            xy;   /* X-Y correlation                */
	       float           den;   /* total intensity                */
	       float           max;   /* max. intensity                 */
	       float            bg;   /* mean background intensity      */
	       float          grad;   /* max. gradient                  */
	       int              ix;   /* first X pixel in object        */
	       int              iy;   /* first Y pixel in object        */
	       int            npix;   /* total no. of pixels in object  */
	       int           nline;   /* no. of lines in object         */
             } OBJ;

#define        KZN           20       /* treshold for gradient change   */
#define        MPIX           3       /* min. pixels in object          */
#define        MAX_OBJ      500       /* max. no. of active objects     */
