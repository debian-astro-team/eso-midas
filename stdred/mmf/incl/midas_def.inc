C @(#)midas_def.inc	19.1 (ESO-IPG) 02/25/03 14:25:37
C 
C 
C --------------------- MIDAS definition Table ---------------------------
C 	
C                       on file MIDAS_DEF.INC
C 
C version [2.00]  880414:  creation  (KB)
C version [2.10]  881030:  update  (KB)
C version [2.20]  890920:  update  (KB)
C 
C 
C 	
C 
C   Error definitions:   
C  
C   Warning + information messages are numbered < 0 .
C   Error messages are numbered > 0 .
C 
C 
C 
C 
      INTEGER ERR_NORMAL     ! successful return status 
      INTEGER ERR_DSCNPR     ! descriptor not present 
      INTEGER ERR_FCTOVF     ! overflow in frame control table 
      INTEGER ERR_LOGFIL     ! problems with logfile 
      INTEGER ERR_OPSSYS     ! Operating system error 
      INTEGER ERR_DSKFUL     ! disk full 
      INTEGER ERR_FRMNAC     ! frame not accessible 
      INTEGER ERR_INPINV     ! input invalid 
      INTEGER ERR_CATOVF     ! overflow in general catalog 
      INTEGER ERR_DSCBAD     ! descriptor bad 
      INTEGER ERR_KEYBAD     ! keyword bad 
      INTEGER ERR_KEYOVL     ! overflow in keyword data area 
      INTEGER ERR_KEYOVN     ! overflow in keyword name table 
      INTEGER ERR_FILNAM     ! invalid file name
      INTEGER ERR_FILBAD     ! file handling error 
      INTEGER ERR_CATBAD     ! bad catalog 
      INTEGER ERR_DSCOVF     ! descriptor data overflow ( > 32767) 
      INTEGER ERR_NODATA     ! no data available 
      INTEGER ERR_KEYTYP     ! wrong type of keyword 
      INTEGER ERR_CATENT     ! no entry in catalog 
      INTEGER ERR_MESOVF     ! overflow in error message stack 
      INTEGER ERR_KEYNPR     ! keyword not present 
C
C 
C      INTEGER ERR_FULTBL    ! Table errors start at 20 
C      INTEGER ERR_MEMTBL    !  error allocating dynamic mem. 
C      INTEGER ERR_MAPTBL    !  error mapping the table 
C      INTEGER ERR_COVTBL    !  column overflow 
C      INTEGER ERR_ENTTBL    !  table not found wrong init. 
C      INTEGER ERR_COLTBL    !  wrong column number 
C     INTEGER ERR_ROWTBL    !  wrong row number 
C     INTEGER ERR_KEYTBL    !  identifier not found 
C     INTEGER ERR_FMTTBL    !  error in column format 
C     INTEGER ERR_IMPTBL    !  not implemented 
C     INTEGER ERR_RFMTBL    !  error reformatting table 
C     INTEGER ERR_DNMTBL    !  duplicate table name 
C      INTEGER ERR_LABTBL    !  illegal label 
C
C
C   Mode definitions:   
C 
C
C
      INTEGER F_I_MODE                     ! map file for input only    
      INTEGER F_O_MODE                     ! map file for output        
      INTEGER F_IO_MODE                    ! map file for updating      
      INTEGER F_X_MODE                     ! create/map virtual memory  
C
C
C 
C   Data Format definitions:   
C 
C
C
      INTEGER D_OLD_FORMAT     
      INTEGER D_I1_FORMAT     
      INTEGER D_I2_FORMAT     
      INTEGER D_I4_FORMAT     
      INTEGER D_R4_FORMAT    
C
C
C
      DATA    ERR_NORMAL /0/,  ERR_DSCNPR   /1/
      DATA    ERR_FCTOVF /2/,  ERR_LOGFIL   /3/
      DATA    ERR_OPSSYS /4/,  ERR_DSKFUL   /5/
      DATA    ERR_FRMNAC /6/,  ERR_INPINV   /7/
      DATA    ERR_CATOVF /8/,  ERR_DSCBAD   /9/
      DATA    ERR_KEYBAD /10/, ERR_KEYOVL  /11/
      DATA    ERR_KEYOVN /12/, ERR_FILNAM  /13/
      DATA    ERR_FILBAD /14/, ERR_CATBAD  /15/
      DATA    ERR_DSCOVF /16/, ERR_NODATA  /-4/
      DATA    ERR_KEYTYP /-5/, ERR_CATENT  /-6/
      DATA    ERR_MESOVF /-7/, ERR_KEYNPR  /-9/ 

 
C      DATA ERR_TBLFUL /20/, ERR_TBLMEM /21/
C      DATA ERR_TBLMAP /22/, ERR_TBLCOV /23/
C      DATA ERR_TBLENT /24/, ERR_TBLCOL /25/
C      DATA ERR_TBLROW /26/, ERR_TBLKEY /27/
C      DATA ERR_TBLFMT /28/, ERR_TBLIMP /29/
C      DATA ERR_TBLRFM /30/, ERR_TBLDNM /31/
C      DATA ERR_TBLABL /32/
C 
C 
      DATA F_I_MODE   /0/
      DATA F_O_MODE   /1/
      DATA F_IO_MODE  /2/
      DATA F_X_MODE   /9/
C
C 
      DATA D_OLD_FORMAT /0/
      DATA D_I1_FORMAT  /1/
      DATA D_I2_FORMAT  /2/
      DATA D_I4_FORMAT  /4/
      DATA D_R4_FORMAT  /10/
C 
C  ------------------------------------------------------------------  
