! @(#)ccdmean.prg	19.1 (ESO-DMD) 02/25/03 14:16:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdmean.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Compute the mean of an image and write in the descriptor
!.USE         @s ccdmean p1
!.               p1 = input image; 
!.METHOD      Use statistics/image
!.VERSION     930417  RHW  Creation
!             980519  SW   MEAN has to be stored in CCDMEAN , too (swolf@)
!-------------------------------------------------------
DEFINE/PARAM P1 ?  I       "Enter input image [no default]:"
!
! *** Check if trimming has already been applied
IF M$EXISTD(P1,"CCDMEAN") .EQ. 1 THEN
   CCDMEAN = {{P1},CCDMEAN}			! store in global keyword, too
   RETURN
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "<CCDMEAN> {P1}: ***TO BE DONE*** Find mean value of the image"
   RETURN
ENDIF
!
STATISTIC/IMAGE {P1} {IM_SEC} ? ? SN            ! get the mean
COPY/KD OUTPUTR/R/3/1 {P1} CCDMEAN/R/1/1        ! store in descriptor
CCDMEAN = {{P1},CCDMEAN}			! store in global keyword, too
RETURN


