! @(#)make_cal.prg	19.1 (ES0-DMD) 02/25/03 14:16:03
! @(#)make_cal.prg	19.1 (ESO)  02/25/03 14:16:03
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       make_cal.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, combining calibration frames
!.PURPOSE     Do the frame combining
!.USE         @@ mkcalfrm [p1] [p2] [p3] 
!.            with p1 = combine option (BS/DK/FF/SK/OT)
!.                 p2 = input catalogue with exposures 
!.                 p3 = output combine frame
!.                 p4 = merge option 
!.VERSION     920629  RHW  Creation
!.VERSION     920716  RHW  parameter p2
!.VERSION     930725  RHW  chnage of syntax
!-------------------------------------------------------
DEFINE/PARAMETER P1 ?          C   "Enter combining option (BS/DK/FF/SK/OT):"
!
DEFINE/LOCAL EXP/C/1/2    " " ALL
DEFINE/LOCAL ACTION/C/1/2 " " ALL
IF P1 .EQ. "BS" THEN
   WRITE/KEYW EXP/C/1/2 "BS"
ELSEIF P1 .EQ. "DK" THEN
   WRITE/KEYW EXP/C/1/2 "DK"
ELSEIF P1 .EQ. "FF" THEN
   WRITE/KEYW EXP/C/1/2 "FF"
ELSEIF P1 .EQ. "SK" THEN 
   WRITE/KEYW EXP/C/1/2 "SK"
ELSEIF P1 .EQ. "OT" THEN
   WRITE/KEYW EXP/C/1/2 "OT"
ELSE                                                   ! comb option incorrect
   WRITE/OUT -
   "*** FATAL <MKCAL>: Unknown combining option {P1}, try again ..."
   RETURN
ENDIF
!
DEFINE/PARAMETER P2  ?  C  "Enter input catalogue:"
DEFINE/PARAMETER P3  ?  C  "Enter combined output frame:"
DEFINE/PARAMETER P4 "M" C  "Enter merge option [M]:"
!
! *** check the consistency of the input parameters 
IF M$EXIST("{P2}") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <MKCAL>: Catalogue {P2} not existing"
   RETURN/EXIT
ENDIF
!
! *** All done; now copy remaining keywords to local ones
DEFINE/LOCAL INCAT/C/1/60     {P2}            ! input catalog
DEFINE/LOCAL OUTFRM/C/1/60    {P3}            ! output frame
DEFINE/LOCAL SIGMA/C/1/5      {{EXP}_SIG}     ! generate an output sigma frame
DEFINE/LOCAL METHOD/C/1/20    {{EXP}_MET}     ! averaging method
DEFINE/LOCAL EXPTYP/C/1/20    {{EXP}_TYP}     ! which exposure type allowed
DEFINE/LOCAL DELETE/C/1/3     {{EXP}_DEL}     ! delete input cat + frames
DEFINE/LOCAL EXPOSU/C/1/3     {{EXP}_EXP}     ! scale by exposure time
DEFINE/LOCAL SCALE/C/1/3      {{EXP}_SCA}     ! scale by the mode
DEFINE/LOCAL OFFSET/C/1/3     {{EXP}_OFF}     ! add offset from mode
DEFINE/LOCAL WEIGHT/C/1/3     {{EXP}_WEI}     ! use the weighted average
DEFINE/LOCAL SECTION/C/1/60   {{EXP}_SEC}     ! frame section for finding mode
DEFINE/LOCAL RANGE/R/1/2      {{EXP}_RAN(1)},{{EXP}_RAN(2)} ! pixel range
DEFINE/LOCAL CLIP/R/1/2       {{EXP}_CLP(1)},{{EXP}_CLP(2)} ! sigma clip factor
DEFINE/LOCAL BLANK/C/1/20     {{EXP}_NUL}     ! null value
!
WRITE/KEYW    ACTION/C/1/2 {P4}
WRITE/KEYW    INPUTR/R/1/1 256
!
IF EXP .EQ. "BS" THEN
   WRITE/OUT "Combining BIAS frames: Input={INCAT}; output={OUTFRM}"
ELSEIF EXP .EQ. "DK" THEN
   WRITE/OUT "Combining DARK frames: Input={INCAT}; output={OUTFRM}"
ELSEIF EXP .EQ. "FF" THEN
   WRITE/OUT "Combining FLAT frames: Input={INCAT}; output={OUTFRM}"
ELSEIF EXP .EQ. "SK" THEN
   WRITE/OUT "Combining SKY frames: Input={INCAT}; output={OUTFRM}"
ELSEIF EXP .EQ. "OT" THEN
   WRITE/OUT "Combining OTHER frames: Input={INCAT}; output={OUTFRM}"
ENDIF
RUN STD_EXE:CCDCOMB
!
! *** check on existence of the output frame
IF M$EXIST("{OUTFRM}") .EQ. 0 THEN                               ! is it there?
   RETURN
ENDIF
!
! *** write the descriptors
WRITE/DESCR {OUTFRM} LHCUTS/R/1/1 {OUTPUTR(1)},{OUTPUTR(2)}
WRITE/DESCR {OUTFRM} MEAN/R/1/1   {OUTPUTR(3)}
WRITE/DESCR {OUTFRM} MODE/R/1/1   {OUTPUTR(7)}
WRITE/DESCR {OUTFRM} MEDIAN/R/1/1 {OUTPUTR(8)}
!
! *** delete files if requested
IF {EXP}_DEL .EQ. "yes" THEN
   WRITE/OUT "{P1}: Deleting frames  entries in input catalogue"
!  DELETE/ICAT {CCD_IN} CONF ?
ENDIF
RETURN








