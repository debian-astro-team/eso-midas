! @(#)ccdload.prg	19.1 (ES0-DMD) 02/25/03 14:16:01
! @(#)ccdload.prg	19.1 (ESO)  02/25/03 14:16:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       ccdload.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Write  telescope/detector/instrument specification into keywords
!.USE         LOAD/CCD instr
!.            where instr is the instrument/detector combination for which
!             descriptor names and default values will be load into the system
!.VERSION     921216  RHW  Creation
!.VERSION     930416  RHW  Inclusion of bad pixel file
!-------------------------------------------------------
DEFINE/PARAMETER P1 ?  C  "Enter intrumental setup procedure: "
!
DEFINE/LOCAL CONFIG/C/1/20 "eso_specs"
DEFINE/LOCAL IR/I/1/1      0
DEFINE/LOCAL NR/I/1/1      0
DEFINE/LOCAL NAME/C/1/20   " " ALL
!
! *** check if ccd configuration file exists
IF M$EXIST("{config}.tbl") .EQ. 0 THEN                           ! no         
   IF AUX_MODE(1) .LE. 1 THEN                                    ! VMS
      define/local ccddir/c/1/60 -
            "MID_DISK:[&MIDASHOME.&MIDVERS.STDRED.CCDRED.INCL]"
      define/local protec/c/1/60 "SET PROT=(O:RWED)"
   ELSE                                                          ! UNIX
      define/local ccddir/c/1/60 -
              "$MIDASHOME/$MIDVERS/stdred/ccdred/incl/"
      define/local protec/c/1/60 "chmod 644"
   ENDIF
   -COPY {ccddir}{CONFIG}.fmt {CONFIG}.fmt                       ! format file
   -COPY {ccddir}{CONFIG}.dat {CONFIG}.dat                       ! data file
   $ {protec} {CONFIG}.dat 
   $ {protec} {CONFIG}.fmt
   CREATE/TABLE {CONFIG} * * {CONFIG} {CONFIG}                   ! create table
ENDIF
!
! *** loop through science frame column 
NR = {{CONFIG}.tbl,TBLCONTR(4)}
DO IR = 1 {NR}                                     ! loop thu rows
   NAME = "{{CONFIG},:IDENT,@{IR}}"                ! science frame
   IF P1 .EQ. "{NAME}" THEN                        ! ident found
      TELES    = "{{CONFIG},:TELES,@{IR}}"         ! telescope ID 
      INSTR    = "{{CONFIG},:INSTR,@{IR}}"         ! instrument ID
      DETEC    = "{{CONFIG},:DETEC,@{IR}}"         ! detector ID
      NPIXX    = {{CONFIG},:X_NUMB,@{IR}}          ! number of pixels in x
      NPIXY    = {{CONFIG},:Y_NUMB,@{IR}}          ! number of pixels in y
      SPIXX    = {{CONFIG},:X_SIZE,@{IR}}          ! pixel size in x in micron
      SPIXY    = {{CONFIG},:Y_SIZE,@{IR}}          ! pixel size in y in micron 
      IF READMOD(1:1) .EQ. "F" THEN
         READON   = {{CONFIG},:RON_F,@{IR}}        ! readout noise per AD unit
         AD_VAL   = {{CONFIG},:CON_F,@{IR}}        ! analog to digital factor

      ELSEIF READMOD(1:1) .EQ. "S" THEN
         READON   = {{CONFIG},:RON_S,@{IR}}        ! readout noise per AD unit
         AD_VAL   = {{CONFIG},:CON_S,@{IR}}        ! analog to digital factor

      ELSE
         WRITE/OUT "*** INFO: Readout mode undefined; <SLOW> assumed"
         READMOD  = "SLOW"
         READON   = {{CONFIG},:RON_S,@{IR}}        ! readout noise per AD unit
         AD_VAL   = {{CONFIG},:CON_S,@{IR}}        ! analog to digital factor
      ENDIF
      WRITE/OUT "*** INFO: Instrumental setup for <{P1}> loaded"
      RETURN
   ENDIF 
ENDDO
!
WRITE/OUT "*** WARNING: Instrumental setup for <{P1}> not found"






