! @(#)ccdscan.prg	19.1 (ESO-DMD) 02/25/03 14:16:02
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdscan.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     High level procedure for overscan correction
!.USE         @s ccdscan p1 p2 p3 p4 p5
!.               p1 = input frame; 
!.               p2 = output frame; 
!.               p3 = overscan area
!.               p4 = readout direction of the CCD
!.               p5 = mode of operation
!.METHOD      The procedure use the prep_scan lower level procedure to make
!.            the overscan frame 
!.NOTE        The following descriptors are set in case a correction has 
!.            been done.
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for bad pixel correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930503  RHW  Creation
!.VERSION     980128  SW   table output of fit implemented
!-------------------------------------------------------
DEFINE/PARAM P1 ?           I "Enter input frame:"
DEFINE/PARAM P2 ?           I "Enter output frame:" 
DEFINE/PARAM P3 {OV_SEC}    C "Enter overscan area:"
DEFINE/PARAM P4 {DIRECT}    C "Readout direction of the CCD:"
DEFINE/PARAM P5 {OV_IMODE}  C "Enter mode of operation:"
!
! *** set the processing keywords
DEFINE/LOCAL OVSEC/C/1/40 {OV_SEC}
OV_SEC = P3
DEFINE/LOCAL OVDIR/C/1/3  {DIRECT}
DIRECT = P4
DEFINE/LOCAL OVMOD/C/1/10 {OV_IMODE}
OV_IMODE = P5
DEFINE/LOCAL OVFUN/C/1/20 {OV_FUNCT}                         ! fitting function
DEFINE/LOCAL OVORD/I/1/1  {OV_ORDER}               ! number of polynomial terms
DEFINE/LOCAL OVAVE/I/1/1  {OV_AVER}               ! number of points to combine
DEFINE/LOCAL OVITE/I/1/1  {OV_ITER}                      ! number of iterations
DEFINE/LOCAL OVREJ/R/1/1  {OV_REJEC}                   ! sigma rejection factor
DEFINE/LOCAL IDXT/I/1/1 0			       !index in P2 of ',table'
!
! *** check if overscan has been corrected for
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/OUT "{P1}: *** WARNING: No check on CCD reduction status possible"
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ELSE
   IF {{P1},CCDSTAT(1)} .EQ. 1 THEN
   WRITE/OUT 
         "{P1}: *** WARNING: Overscan already subtracted; nothing done ..."
      RETURN
   ENDIF
ENDIF
!
! *** check if output (P2) is a table
IDXT = M$INDEX(P2,",t")             !test for name,table
IF {IDXT} .LT. 1 IDXT = M$INDEX(P2,",T")
IDXT = {IDXT} - 1
!
! *** column or row readout direction
IF P4(1:1) .EQ. "C"  .OR. P4(1:1) .EQ. "Y" THEN
   DIRECT = "Y"
ELSE IF P4(1:1) .EQ. "R" .OR. P4(1:1) .EQ. "X" THEN
   DIRECT = "X"
ELSE
   WRITE/OUT "*** FATAL: Unknown readout direction; "
   WRITE/OUT "           Use C or Y (for column) and R or X (for row)"
   RETURN/EXIT
ENDIF
!
! *** interactive of batch mode                   
IF P5(1:1) .EQ. "I" .OR. P5(1:1) .EQ. "y" THEN
   OV_IMODE = "yes"
ELSE
   OV_IMODE = "no"
ENDIF
!
! *** determine the overscan and correct
DEFINE/LOCAL SCSAV/C/1/3 {SC_SCAN}
SC_SCAN = "yes"
@s prep_scan {P1}                                      ! determine the overscan
SC_SCAN = SCSAV
IF {IDXT} .GE. 1 THEN					 ! save only scan-table
  RENAME/TABLE {Q2} {P2(1:{IDXT})}		! rename to specified tablename
ELSE
  COMPUTE/IMAGE {P2} = {P1} - {CCDSCAN}                    ! do the subtraction
!
  WRITE/DESCR {P2} CCDSTAT/I/1/1 1                   ! overscan correction done
  WRITE/DESCR {P2} HISTORY/C/-1/80 "OVERSCAN/CCD {P1} {P2} {P3}"
  IF VERBOSE(1:1) .EQ. "Y" THEN
     STATISTIC/IMAGE {P2} {IM_SEC} ? ? FF                     ! full statistics
  ELSE
     STATISTIC/IMAGE {P2} {IM_SEC} ? ? FX                    ! short statistics
  ENDIF
ENDIF
!
! *** restore the settings
OV_SEC   = OVSEC
DIRECT   = OVDIR
OV_FUNCT = OVFUN
OV_ORDER = OVORD
OV_AVER  = OVAVE
OV_ITER  = OVITE
OV_REJEC = OVREJ
!
RETURN
