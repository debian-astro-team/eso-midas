! @(#)ccdflatcor.prg	19.1 (ESO-DMD) 02/25/03 14:16:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdflatcor.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, SKYCOR/CCD
!.PURPOSE     Make illumination illumination correction frame(s)
!.USE         ILLCOR/CCD p1 p2 p3 p4 p5 p6
!.	      ILLFLAT/CCD p1 p2 p3 p4 p5 p6
!.            where p1 = reduction table or single flat field
!.                  p2 = output frame (only in case of single frame input)
!.                  p3 = xboxmin, xboxmax smoothing box
!.                  p4 = yboxmin, yboxmax smoothing box
!.                  p5 = clipping option
!.                  p6 = low and high clipping factor 
!.ALGORITHM:  In case the input is the association table the procedure first 
!.            checks if the flat frame is available. If not available the
!.            frame will be created and reduced. For single frame input all
!.            frames have to be available and stored in the CCD keywords.
!.            Using the reduced flat the illumation frame are created by a box 
!.            smoothing algorithm.
!.            The illumination correction frame will be indicated as a FF-
!.            correction frame -- in contrast to an sky-correction frame --
!.            by the descriptor ILLCOR. It later helps to identify the illumi-
!.            nation correction frame during processing the science frame by
!.            means of the REDUCE/CCD command.
!.            In the case the association table is used as input, the output 
!.            illumination correction (command ILLCOR/CCD) is stored in the
!.            table.
!.NOTE        The individual procedures check if one operation was already done
!.            by checking the descriptor CCDSTAT. The CCDSTAT descriptor values
!.            represent the following status:
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION:    920731  RHW  Creation
!.VERSION:    930728  RHW  Improve layout and structure
!.VERSION:    971201  SW   Add descr. ILLCOR to identify dome-ff-correc. frame
!-------------------------------------------------------
DEFINE/PARAM P1 {CCD_IN} C "Enter input specification [no default]:"
!
! *** keyword saving
DEFINE/LOCAL CCDIN/C/1/60   " "
DEFINE/LOCAL CCDOUT/C/1/60  " "
DEFINE/LOCAL CCDSAV/C/1/60  "{CCD_IN}"
DEFINE/LOCAL BSFRM/C/1/60   "{SC_BSFRM}"
DEFINE/LOCAL DKFRM/C/1/60   "{SC_DKFRM}"
DEFINE/LOCAL SELECT/C/1/80  " "
!
DEFINE/LOCAL NCA/I/1/1      0
DEFINE/LOCAL NRA/I/1/1      0
DEFINE/LOCAL IRA/I/1/1      0
DEFINE/LOCAL IDOT/I/1/1     0
DEFINE/LOCAL TBL/I/1/1      0
DEFINE/LOCAL MCMND/C/1/20   "{MID$CMND}"

DEFINE/PARAM P3 {IL_XBOX(1)},{IL_XBOX(2)} N
DEFINE/PARAM P4 {IL_YBOX(1)},{IL_YBOX(2)} N
DEFINE/PARAM P5 {IL_CLIP} C
!
IF P5(1:1) .EQ. "Y" THEN
   DEFINE/PARAM P6 {IL_SIGMA(1)},{IL_SIGMA(2)}
ENDIF
!
! *** first check for single frame of input table
TBL = M$INDEX(P1,".tbl")                                      ! search for .tbl
IF TBL .EQ. 0 THEN                                                ! frame input
   IF M$EXIST("{P1}.bdf") .EQ. 0 THEN                         ! check existence
      WRITE/OUT "*** FATAL: Missing input flat frame {P1}"
      RETURN
   ELSE
      CCDIN = "{P1}"
   ENDIF
!
ELSE                                                    ! reduction table input
   CCD_IN = "{P1}"
   SET/MIDAS OUTPUT=LOGONLY
   SELECT = "{{CCD_IN},TSELTABL(1:60)}"                 ! read the selection
   SET/MIDAS OUTPUT=YE
   IF M$EXIST(CCD_IN) .EQ. 0 THEN                       ! check exsistence
      WRITE/OUT "*** FATAL: Missing CCD input table {CCD_IN}
      RETURN
   ENDIF
   IF SC_BSCOR(1:1) .EQ. "y" .AND. M$EXISTC(CCD_IN,BS_COL) .EQ. -1 THEN
      WRITE/OUT "*** FATAL: No BIAS column defined; *** REDUCTION STOPPED"
      RETURN
   ENDIF
   IF SC_DKCOR(1:1) .EQ. "y" .AND. M$EXISTC(CCD_IN,DK_COL) .EQ. -1 THEN
      WRITE/OUT "*** FATAL: No DARK column defined; *** REDUCTION STOPPED"
      RETURN
   ENDIF
   IF MCMND(1:6) .EQ. "ILLCOR" .AND. -
      M$EXISTC(CCD_IN,IL_COL) .EQ. -1 THEN
      CREATE/COLUMN {CCD_IN} {IL_COL} C*60 ? A20       ! create illum column
   ENDIF

   SET/FORMAT I3
   NCA = {{CCD_IN},TBLCONTR(3)}
   NRA = {{CCD_IN},TBLCONTR(4)}                        ! check number of rows
   IF NRA .EQ. 0 THEN
      WRITE/OUT -
            "*** INFO: No (selected) entries in input table; REDUCTION STOPPED" 
      GOTO FINISH
   ENDIF
!
! *** collect the input flat frame and calibration frames
   WRITE/OUT "Start processing flat frames frames in table {CCD_IN}
   IRA   = 0
   NXTFRM:
   IRA   = IRA + 1
   SELECT/TABL {CCD_IN} {SELECT}
   IF {{CCD_IN},SEL,@{IRA}} .EQ. 0 THEN
      GOTO DONE
   ENDIF
!
   CCDIN = "{{CCD_IN},:{FF_COL},@{IRA}}"                 ! get input image name
   IDOT = M$INDEX(CCDIN,".bdf") - 1                  ! remove bdf extension
   IF IDOT .GT. 0 THEN 
      CCDIN = "{CCDIN(1:{IDOT})}"
   ELSE
      CCDIN = "{CCDIN}"
   ENDIF
   SET/MIDAS OUTPUT=LOGONLY
   SELECT/TABL {CCD_IN} :{FF_COL}.EQ."{CCDIN}.bdf" .OR. :{FF_COL}.EQ."{CCDIN}"
   SET/MIDAS OUTPUT=YES
   WRITE/OUT " "
!
   IF M$EXIST("{CCDIN}.bdf") .EQ. 0 THEN                  ! checking flat frame
      WRITE/OUT "*** INFO Combined flat frame {CCDIN} to be created"
      COMBINE/CCD FF {CCD_IN}                                         ! combine
   ENDIF
   IF M$EXIST("{CCDIN}.bdf") .EQ. 0 THEN                  ! checking flat frame
       WRITE/OUT "*** ERROR: Failed to make combined flat frame {CCDIN}"
      GOTO FINISH
   ENDIF   
!
   IF SC_BSCOR(1:1) .EQ. "y" THEN                        ! bias correc. wanted?
      SET/MIDAS OUTPUT=LOGONLY
      SC_BSFRM = "{{CCD_IN},:{BS_COL},@{IRA}}"
      SET/MIDAS OUTPUT=YES
      IF SC_BSFRM(1:1) .EQ. " "  THEN
         WRITE/OUT "{CCDIN}: No master BIAS defined; ** NO REDUCTION ***"
         GOTO DONE
      ENDIF
   ENDIF
!           
   IF SC_DKCOR(1:1) .EQ. "y" THEN                        ! dark correc. wanted?
      SET/MIDAS OUTPUT=LOGONLY
      SC_DKFRM = "{{CCD_IN},:{DK_COL},@{IRA}}"
      SET/MIDAS OUTPUT=YES
      IF SC_DKFRM(1:1) .EQ. " "  THEN
         WRITE/OUT "{CCDIN}: No master DARK defined; *** NO REDUCTION ***"
         GOTO DONE
      ENDIF
   ENDIF
ENDIF
!
IF M$EXIST("{CCDIN}.bdf") .EQ. 0 THEN                       ! check exsistence
   WRITE/OUT "*** WARNING: Missing input frame {CCDIN}; *** NO REDUCTION"
   GOTO DONE
ENDIF
!
IF MCMND(1:6) .EQ. "ILLCOR" THEN         ! illumination correction (ILLCOR/CCD)
   IF TBL .EQ. 0 THEN                              ! determine the output frame
      DEFINE/PARAM P2 {CCDIN}_ill IMA "Enter the output frame:"
      CCDOUT = "{P2}"
   ELSE
      CCDOUT  = "{CCDIN}_ill"                        ! reduce output image name
   ENDIF 
   @s ccdredfrm {CCDIN} {CCDIN}                        ! reduction of the frame
   @s make_illum {CCDIN} {CCDOUT} {P3} {P4} {P5} {P6}      ! create illum frame
!
   IF M$EXIST("{CCDOUT}.bdf") .EQ. 0 THEN                        ! is it there?
      WRITE/OUT "*** ERROR: Failed to make illum. correction from {CCDIN}"
      GOTO FINISH
   ELSE
      WRITE/OUT "{CCDIN}: Illumination correction frame {CCDOUT} created"
      WRITE/DESCR {CCDOUT} ILLCOR/C/1/10 screen        ! ill corr.descriptor
      WRITE/DESCR {CCDOUT} HISTORY/C/-1/80 "ILLCOR/CCD {CCDIN} {CCDOUT}"
      IF TBL .GT. 0 THEN                               ! stored output in table
         {CCD_IN},:{IL_COL},@{IRA} = "{CCDOUT}"
      ENDIF
   ENDIF

ELSE                                     ! here illumination flat (ILLFLAT/CCD)
   IF TBL .EQ. 0 THEN                              ! determine the output frame
      DEFINE/PARAM P2 {CCDIN} IMA "Enter the output illumination flat frame:"
      CCDOUT = "{P2}"
   ELSE
      CCDOUT  = "{CCDIN}"                            ! reduce output image name
   ENDIF 
   @s ccdredfrm {CCDIN} {CCDIN}                        ! reduction of the frame
   @s make_illum {CCDIN} &d {P3} {P4} {P5} {P6}       ! create illum frame
!
   IF M$EXIST("&d") .EQ. 0 THEN                        ! is it there?
      WRITE/OUT "*** ERROR: Failed to make illum. correction from {CCDIN}"
      GOTO FINISH
   ELSE
      @s make_illflat {CCDIN} &d {CCDOUT}
      WRITE/OUT "{CCDIN}: Illumination corrected flat field {CCDOUT} created"
      WRITE/DESCR {CCDOUT} HISTORY/C/-1/80 "ILLFLAT/CCD {CCDIN} {CCDOUT}"
   ENDIF
ENDIF
!
! *** finish up
DONE:
IF TBL .GE. 1 THEN                                         ! single frame input
   IF IRA .LT. {NRA} THEN
      GOTO NXTFRM
   ELSE 
      WRITE/OUT " "
      IF NRA .EQ. 0 THEN
         WRITE/OUT "{P1}: *** WARNING: Input table {P1} selection is empty"
      ELSE
         IF MCMND(1:6) .EQ. "ILLCOR" THEN             ! illumination correction
            WRITE/OUT "{P1}: *** ALL DONE: Illumination correction frames ready"
         ELSE
            WRITE/OUT "{P1}: *** ALL DONE: Illumination flat frames ready"
         ENDIF
      ENDIF
      SET/MIDAS OUTPUT=LOGONLY
      SELECT/TABL {P1} {SELECT}
      SET/MIDAS OUTPUT=YES
      DELETE/IMAG &d NO
   ENDIF
ENDIF
!
! *** restore keywords settings
FINISH:
CCD_IN   = "{CCDSAV}"
SC_BSFRM = "{BSFRM}"                                  ! restore keyword setting
SC_DKFRM = "{DKFRM}"
RETURN
