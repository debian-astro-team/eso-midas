! @(#)prep_trim.prg	19.1 (ES0-DMD) 02/25/03 14:16:06
! @(#)prep_trim.prg	19.1 (ESO)  02/25/03 14:16:06
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       prep_trim.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Trimmed image from overscan area.
!.USE         @s prep_trim p1
!.               p1 = input frame
!.METHOD      A subimage of the desired size is extracted by EXTRACT/IMAGE 
!.            command. All coordinates are in (integer pixels)
!.PARAMETERS  P1: input frame
!             P2: correction label string which will be checked by
!                 ccdcorrect.prg to do the desired actions.
!.NOTE        The procedure checks if the operation was already done
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     921005  RHW  Creation
!.VERSION     971117  SW   correction labels have to be local -> new param.
!-------------------------------------------------------
DEFINE/PARAM P1 ?        I  "Input frame for trimming [no default]:"
DEFINE/PARAM P2 {CCDCORS(1:20)}
!The correction label string CCDCORS which will be checked by CCDCORRECT.PRG
!has to be local, in order to always get the right link to the actual frame.
!In a global definition the correction labels may overwritten by other pre-
!pared frames which leads to unwished actions to previous frames (SW,97.11.17).
!
! *** Check if trimming has already been applied
IF SC_TRIM .NE. "yes" .OR. {{P1},CCDSTAT(2)} .EQ. 1 THEN
   WRITE/OUT "{P1}: Trimming not requested or was applied"
   RETURN {P2}
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Trim section is {IM_SEC}"
   RETURN {P2}
ENDIF
!
WRITE/OUT "{P1}: Trim section is {IM_SEC}"
P2(2:2) = "T"
P2(11:11) = "Y"
!
RETURN {P2}
