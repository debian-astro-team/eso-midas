! @(#)prep_illum.prg	19.1 (ESO-DMD) 02/25/03 14:16:06
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       prep_illium.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Prepare the illumination correction image for the reduction
!.METHOD      - Return if illumination correction is not requested or has been 
!               corrected previously
!.            - Get the illumination correction image. If not found return an 
!.              error
!.            - If illumination image has not been processed execute ccdredfrm
!.            - Set the processing flags.
!.PARAMETERS  P1: input frame
!             P2: correction label string which will be checked by
!                 ccdcorrect.prg to do the desired actions.
!.NOTE        The procedure checks if the operation was already done
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930416  RHW  Creation
!.VERSION     930507  RHW  Check input frame for illumination correction
!.VERSION     931015  RHW  Inclusion of constant value 
!.VERSION     940113  RHW  Change for DO assocuation table
!.VERSION     971117  SW   correction labels have to be local -> new param.
!-------------------------------------------------------
DEFINE/PARAM P1 ? I "Input frame for illumination correction:"
DEFINE/PARAM P2 {CCDCORS(1:20)}
!The correction label string CCDCORS which will be checked by CCDCORRECT.PRG
!has to be local, in order to always get the right link to the actual frame.
!In a global definition the correction labels may overwritten by other pre-
!pared frames which leads to unwished actions to previous frames (SW,97.11.17).
!
DEFINE/LOCAL CONST/I/1/1 0
!
! *** do we want the illumination correction or has it been done
IF SC_ILCOR .NE. "yes" .OR. {{P1},CCDSTAT(7)} .EQ. 1 THEN
   WRITE/OUT "{P1}: Illumination correction not requested or was applied"
   RETURN {P2}
ENDIF
!
! *** check the calibration table for the column
IF SC_ILFRM(1:1) .EQ. " " THEN                             ! ill. image unknown
   WRITE/OUT "{P1}: *** WARNING: Illumination correction unknown or undefined"
   RETURN {P2}
ENDIF
!
IF M$TSTNO("{SC_ILFRM}") .EQ. 1 THEN    
   CONST = 1
ELSE
   IF M$EXIST("{SC_ILFRM}.bdf") .EQ. 0 THEN                      ! is it there?
      WRITE/OUT "{P1}: *** ERROR: Illumination frame not available"
      WRITE/OUT "*** INFO: Run ILLCOR/CCD or SKYCOR/CCD command first" 
      RETURN {P2}
   ENDIF
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Illimination correction is {SC_ILFRM}"
   RETURN {P2}
 ENDIF
!
! *** here for the real thing
IF CONST .EQ. 0 THEN
   IF M$EXISTD("{SC_ILFRM}","MKILLUM") .EQ. 0 THEN
      WRITE/OUT "{SC_ILFRM}: MKILLUM Illumination flag missing in descriptor"
      RETURN {P2}
   ENDIF
   IF M$EXISTD("{SC_ILFRM}","ILLUMSCALE") .EQ. 1 THEN
      CCDILLUM = {{SC_ILFRM},{ILLUMSCALE}}
   ELSE
      @s ccdmean {SC_ILFRM}
      IF M$EXISTD("{SC_ILFRM}","CCDMEAN") .EQ. 1 THEN
         CCDILLUM = {{SC_ILFRM},CCDMEAN}
      ELSE
         CCDILLUM = 1.0
      ENDIF
   ENDIF
ELSE
   CCDILLUM = 1.0
ENDIF
!
WRITE/OUT "{P1}: Illum. correction {SC_ILFRM}; scale = {CCDILLUM}"
P2(7:7) = "I"
P2(11:11) = "Y"
RETURN {P2}
