! @(#)ccdillum.prg	19.1 (ESO-DMD) 02/25/03 14:16:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       ccdillum.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Do the illumination correction for flat field
!.VERSION     920731  RHW  Creation
!.NOTE        The procedure checks if the operation was already done
!.            by checking the descriptor CCDSTAT(7)
!.            Other CCDSTAT descriptor values
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!-------------------------------------------------------
DEFINE/PARAM P1 ?                 IMA "Enter input frame: "
DEFINE/PARAM P2 ?                IMA "Enter output frame: "
DEFINE/PARAM P3 {SC_ILFRM}        IMA "Enter illumination frame: "
!
! *** check if frame has already be flatfielded
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/OUT "{P1}: *** WARNING: No check on CCD reduction status possible"
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ELSE
   IF {{P1},CCDSTAT(7)} .EQ. 1 THEN 
      WRITE/OUT -
         "{P1}: *** WARNING: Illum. correction was applied; nothing done ..."
      RETURN
   ENDIF
ENDIF
!
! *** check illumination frame for its descriptors; return on errors
IF M$EXIST("{P3}.bdf"} .EQ. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: Illum. correction {P3} doesn't exist ..."
   RETURN
ENDIF
IF IL_TYP .NE. "*" .AND. IL_TYP .NE. "?" THEN 
   IF M$EXISTD(P3,EXP_DESC) .EQ. 1 .AND. -
      "{{P3},{EXP_DESC}}" .NE. "{IL_TYP}" THEN
      WRITE/OUT -
      "{P3}: *** ERROR: Exposure descriptor {EXP_DESC} has wrong type"
   RETURN
   ENDIF
ENDIF
!
IF M$EXISTD(P3,"MKILLUM") .EQ. 0 THEN
   WRITE/OUT "{P3}: MKILLUM Illumination flag missing in descriptor"
   RETURN
ENDIF
IF M$EXISTD(P3,"ILLUMSCALE") .EQ. 1 THEN
   CCDILLUM = {{P3},{ILLUMSCALE}}
ELSE
   @s ccdmean {P3}
   IF M$EXISTD(P3,"CCDMEAN") .EQ. 1 THEN
      CCDILLUM = {{P3},CCDMEAN}
   ELSE
      CCDILLUM = 1.0
   ENDIF
ENDIF
!
! *** do the actual work
COMPUTE/IMAGE {P2} = {P1}*{CCDILLUM}/{P3}
WRITE/DESCR {P2} CCDSTAT/I/7/1 1                       ! illum. correction done
WRITE/DESCR {P2} HISTORY/C/-1/80 "ILLUM/CCD {P1} {P2} {P3}
IF VERBOSE(1:1) .EQ. "Y" THEN
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FF                       ! full statistics
ELSE 
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FX                      ! short statistics
ENDIF
!
RETURN



