! @(#)mosaicmatch.prg	19.1 (ES0-DMD) 02/25/03 14:16:05
! @(#)mosaicmatch.prg	19.1 (ESO-SDAG) 02/25/03 14:16:05
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       mosaicmatch.prg
!.PURPOSE     Do alignment of mosaic of subframes
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, mosacing
!.USE         @@ mosaicmatch
!.            with  P1 input mosaic frame
!.                  P2 database table"
!.                  P3 combined output frame"
!.                  P4 column and row of reference subframe"
!.                  P5 matching subrasters
!.                  P6 column and row of reference subraster
!.                  P7 x and y offset of reference subframe"
!.                  P8 size of the output frame"
!.VERSION     950608 RHW  Creation
!-------------------------------------------------------
DEFINE/PARAMETER P1  ?    C  "Enter input mosaic frame:"
DEFINE/PARAMETER P2  ?    C  "Enter database table:"
DEFINE/PARAMETER P3  ?    C  "Enter combined output frame:"
DEFINE/PARAMETER P4  ?    C  "Enter alignment method and data:"
DEFINE/PARAMETER P5  "*"  C  "Subrasters to be matched:"
DEFINE/PARAMETER P6  0,0  N  "Enter column and row of reference subframe:"
DEFINE/PARAMETER P7  0,0  N  "Enter x and y offset of reference subframe:"
DEFINE/PARAMETER P8  0,0  N  "Enter size of the output frame:"
!
DEFINE/LOCAL CCDIN/C/1/60    " "
DEFINE/LOCAL CCDOUT/C/1/60   " "
DEFINE/LOCAL TBLDB/C/1/60    " "
DEFINE/LOCAL ICOMMA/I/1/1    0
DEFINE/LOCAL ALIGN/C/1/60    " "
DEFINE/LOCAL TBLCO/C/1/60    " "
DEFINE/LOCAL SHIFT/R/1/2     0,0
DEFINE/LOCAL IDOT/I/1/1      0
!
! *** All done; now copy remaining keywords to local ones
IDOT = M$INDEX("{P1}",".bdf")                                 ! get input frame
IF IDOT .GT. 0 THEN 
   CCDIN = "{P1(1:{IDOT})}"
ELSE
   CCDIN = "{P1}"                                          
ENDIF
IF M$EXIST("{CCDIN}.bdf") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <MATCH>: Input frame {CCDIN} not existing"
   RETURN/EXIT
ENDIF
!
IDOT = M$INDEX("{P2}",".tbl")                              ! get database table
IF IDOT .GT. 0 THEN  
   TBLDB = "{P2(1:{IDOT})}"
ELSE
   TBLDB = "{P2}" 
ENDIF
!
CCDOUT = "{P3}"
!
! *** get method and data
ICOMMA = M$INDEX("{P4}",",") -1
ALIGN =  "{P4(1:{ICOMMA})}"
ICOMMA = ICOMMA + 2
IF ALIGN(1:1) .EQ. "S" THEN                              ! here for fixed shift
   WRITE/KEY SHIFT/R/1/2  {P4({ICOMMA}:>)}               
ELSEIF ALIGN(1:1) .EQ. "C" THEN   
   TBLCO = "{P4({ICOMMA}:>)}"                            ! here for coordinates
ELSEIF ALIGN(1:1) .EQ. "R" THEN                      
   TBLCO = "{P4({ICOMMA}:>)}"                        ! here for reference shift
ELSE
   WRITE/OUT "*** FATAL <MATCH>: None existing matching method"
   RETURN/EXIT
ENDIF
!
! *** write keywords
WRITE/KEYW   MATCH_IN/C/1/60 {P5}                 ! list of matching subrasters
DEFINE/LOCAL NRSUB/I/1/2     {P6}
DEFINE/LOCAL XYREF/I/1/2     {P7}
DEFINE/LOCAL OSIZE/I/1/2     {P8}
DEFINE/LOCAL INTERPOL/C/1/20 {MO_INTER}
DEFINE/LOCAL MINPIX/I/1/1    {MO_MNPX}
DEFINE/LOCAL TR_SEC/I/1/4   {MO_TRIM(1)},{MO_TRIM(2)},{MO_TRIM(3)},{MO_TRIM(4)}
DEFINE/LOCAL BLANK/C/1/20    {MO_NUL}                             ! null value
!
RUN STD_EXE:CCDMATCH
!
! *** check on existence of the output frame
IF M$EXIST("{CCDOUT}.bdf") .EQ. 0 THEN                           ! is it there?
   WRITE/OUT "*** ERROR: Failed to make the aligned output frame"
   RETURN
ENDIF
!
! *** write the descriptors
WRITE/DES {CCDOUT} LHCUTS/R/1/1 {OUTPUTR(1)},{OUTPUTR(2)}
WRITE/DES {CCDOUT} MEAN/R/1/1   {OUTPUTR(3)}
WRITE/DES {CCDOUT} MODE/R/1/1   {OUTPUTR(7)}
WRITE/DES {CCDOUT} MEDIAN/R/1/1 {OUTPUTR(8)}
!
RETURN








