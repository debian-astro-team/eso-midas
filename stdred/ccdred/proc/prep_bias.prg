! @(#)prep_bias.prg	19.1 (ES0-DMD) 02/25/03 14:16:05
! @(#)prep_bias.prg	19.1 (ESO-SDAG) 02/25/03 14:16:05
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       prep_bias.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Prepare bias correction image for the reduction
!.METHOD      - Return if bias correction is not requested or has been 
!.              corrected previously
!.            - Get the bias correction image. If not found return an error
!.            - If bias image has not been processed execute ccdredfrm
!.            - Set the processing flags.
!.PARAMETERS  P1: input frame
!             P2: correction label string which will be checked by
!                 ccdcorrect.prg to do the desired actions.
!.NOTE        The procedure checks if the operation was already done
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for the bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for bad pixel correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930416  RHW  Creation
!.VERSION     930507  RHW  Bug fix: Input file checked for being processed
!.VERSION     931015  RHW  Include constant bias 
!.VERSION     940113  RHW  Change for DO association table
!.VERSION     971117  SW   correction labels have to be local -> new param.
!-------------------------------------------------------
DEFINE/PARAM P1 ? I "Input frame for bias correction:"
DEFINE/PARAM P2 {CCDCORS(1:20)}
!The correction label string CCDCORS which will be checked by CCDCORRECT.PRG
!has to be local, in order to always get the right link to the actual frame.
!In a global definition the correction labels may overwritten by other pre-
!pared frames which leads to unwished actions to previous frames (SW,97.11.17).
!
DEFINE/LOCAL CONST/I/1/1  0   
!
! *** do we want the bias correction
IF SC_BSCOR .NE. "yes" .OR. {{P1},CCDSTAT(4)} .EQ. 1 THEN
   WRITE/OUT "{P1}: Bias correction not requested or was applied"
   RETURN {P2}
ENDIF
!
! *** check the bias image name
IF SC_BSFRM(1:1) .EQ. " " THEN                             ! bias frame unknown
   WRITE/OUT "{P1}: *** WARNING: Bias correction unknown or undefined"
   RETURN {P2}
ENDIF
!
! *** check if bias correction is a constant
IF M$TSTNO("{SC_BSFRM}") .EQ. 1 THEN                 ! check for constant value
   CONST = 1
ELSE                                                        ! real frame wanted
   IF M$EXIST("{SC_BSFRM}.bdf") .EQ. 0 THEN         ! check if bias fram exists
      WRITE/OUT "{P1}: Bias correction {SC_BSFRM} to be created"
      COMBINE/CCD BS                                                  ! combine
      IF M$EXIST("{SC_BSFRM}.bdf") .EQ. 0 THEN                   ! is it there?
         WRITE/OUT "{P1}: *** ERROR: Failed to make bias correction"
         RETURN {P2}
      ENDIF
   ENDIF
ENDIF
!
! *** no processing requested
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Bias correcting is {SC_BSFRM}"
   RETURN {P2}
ENDIF
!
! *** here for the real thing
IF CONST .EQ. 0 THEN                                    ! here for a real frame
   @s ccdcheck {SC_BSFRM} BS                               ! check if processed
   IF Q1 .EQ. "1" THEN                                     ! no;  not processed
      @s ccdredfrm {SC_BSFRM} {SC_BSFRM}                        ! do processing
   ENDIF
ENDIF
!
WRITE/OUT "{P1}: Bias correction is {SC_BSFRM}"
P2(4:4) = "B"
P2(11:11) = "Y"
RETURN {P2}
