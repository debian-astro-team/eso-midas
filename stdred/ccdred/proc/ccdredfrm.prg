!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993,2005 European Southern Observatory
!.IDENT       ccdredfrm.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Do the reduction of a single image
!.USE:        @s ccdredfrm p1 p2
!.            where p1 is input frame
!.                  p2 in output frame
!.NOTE        After each reduction step the CCD descriptor CCDRED is updated
!             (write 1) to indicate that the reduction step is done.
!             The following elements are allocated:  
!             CCDSTAT(1) for overscan correction
!             CCDSTAT(2) for trimming the frame
!             CCDSTAT(3) for bad pixel correction
!             CCDSTAT(4) for bias subtraction
!             CCDSTAT(5) for dark current subtraction
!             CCDSTAT(6) for flat fielding
!             CCDSTAT(7) for the illumination correction
!             CCDSTAT(8) for fringing correction
!.VERSION     921009  RHW  Creation
!.VERSION     930330  RHW  Additional checks
!.VERSION     971117  SW   correct. labels have to be local (CCDCORS->CCD_CRS)
! 050309	last modif
!------------------------------------------------------------------------------
! 
DEFINE/PARAM  P1  ?    IMA     "Enter the input frame to reduce:"
DEFINE/PARAM  P2  ?    IMA     "Enter output result frame:"
DEFINE/PARAM  P3  N    C       "Overwrite the output frame [N]:"
!
DEFINE/LOCAL  IND/I/1/1  0
DEFINE/LOCAL  IDOT/I/1/1  0
DEFINE/LOCAL  CCD_CRS/C/1/30  " " ALL
! 
! The correction label string CCDCORS which will be checked by CCDCORRECT.PRG
! has to be local, in order to always get the right link to the actual frame.
! In a global definition the correction labels may overwritten by other pre-
! pared frames which leads to unwished actions to previous frames.
! That is the reason for defining a new local keyword CCD_CRS which will now
! do the work of CCDCORS and CCDOR together (SW,97.11.17).
!
! *** find out the exposure type
IF M$EXISTD(P1,EXP_DESC) .EQ. 0 THEN
   WRITE/OUT "{P1}: *** WARNING: Frame descriptor {EXP_DESC}" 
   WRITE/OUT "      exposure type descriptor not found ---> NOT REDUCED <---"
   RETURN -1
ELSE 
   DEFINE/LOCAL EXP_TYP/C/1/10 "{{P1},{EXP_DESC}}"       ! exposure type
ENDIF
!
! *** see if there is a status descriptor
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ENDIF
!
! *** remove the .bdf extension of the calibration frames
IDOT = M$INDEX(SC_BSFRM,".bdf") - 1
IF IDOT .GT. 0 THEN 
   SC_BSFRM = "{SC_BSFRM(1:{IDOT})}"
ENDIF
IDOT = M$INDEX(SC_DKFRM,".bdf") - 1
IF IDOT .GT. 0 THEN 
   SC_DKFRM = "{SC_DKFRM(1:{IDOT})}"
ENDIF
IDOT = M$INDEX(SC_FFFRM,".bdf") - 1
IF IDOT .GT. 0 THEN 
   SC_FFFRM = "{SC_FFFRM(1:{IDOT})}"
ENDIF
IDOT = M$INDEX(SC_ILFRM,".bdf") - 1
IF IDOT .GT. 0 THEN 
   SC_ILFRM = "{SC_ILFRM(1:{IDOT})}"
ENDIF
IDOT = M$INDEX(SC_FRFRM,".bdf") - 1
IF IDOT .GT. 0 THEN 
   SC_FRFRM = "{SC_FRFRM(1:{IDOT})}"
ENDIF
!
! *** define the reduction keywords
CCDCOR = "no "                                            ! calibration
CCDCORS = "XXXXXXXXXXXXXXXXXXXX"                          ! init flags
CCD_CRS = CCDCORS
!
WRITE/OUT "{P1}: Start reduction; output frame {P2}"
! *** here for the overscan, trimming, and bad pixel correction
@s prep_scan {P1} {CCD_CRS}                              ! get the overscan
CCD_CRS = "{Q1}"
@s prep_trim {P1} {CCD_CRS}                              ! get trim section
CCD_CRS = "{Q1}"
@s prep_fix  {P1} {CCD_CRS}                              ! get pixel correct.
CCD_CRS = "{Q1}"
!
! *** now do the exposure type dependent correction
!
BRANCH EXP_TYP(1:2) BS,DK,FF,SK,IL,SC-
                    BIAS,DARK,FLAT,FLAT,ILLUM,SCIENCE

BRANCH EXP_TYP(1:3) BIA,DAR,FFD,FFS,SKY,ILL,SCI-
                    BIAS,DARK,FLAT,FLAT,FLAT,ILLUM,SCIENCE

BRANCH EXP_TYP(1:4)
{BS_TYP(1:4)},{DK_TYP(1:4)},{FF_TYP(1:4)},{SK_TYP(1:4)},{IL_TYP(1:4)},{SC_TYP(1:4)} -
                    BIAS,DARK,FLAT,FLAT,ILLUM,SCIENCE
! this branch has been added by swolf@eso.org (06.05.98)
!in order to be more flexible in identifying the process to be done!

WRITE/OUT "{P1}: *** WARNING: Frame descriptor {EXP_DESC} = {{P1},{EXP_DESC}}" 
WRITE/OUT "      exposure type unknown ---> NOT REDUCED <---"
WRITE/OUT "      . default exposure types: BS, DK, FF, SK, IL, SC"
WRITE/OUT "      . or select exposure types by ??_TYP = {{P1},{EXP_DESC}}"
RETURN  -2
!
! *** bias correction frame
BIAS:
GOTO DORED
!                                                        ! do nothing
! *** here for dark current frame
DARK:                                          
@s prep_bias {P1}  {CCD_CRS}                             ! get bias frame
CCD_CRS = "{Q1}"
GOTO DORED
! 
! *** here for flat field frame
FLAT:
@s prep_bias {P1} {CCD_CRS}                              ! get bias frame
CCD_CRS = "{Q1}"
@s prep_dark {P1} {CCD_CRS}                              ! get dark frame
CCD_CRS = "{Q1}"
CCD_CRS(11:11) = "Y"
!CCDCORS(11:11) = "Y"
GOTO DORED
!
! *** here for the illumination frame
ILLUM:
@s prep_bias {P1} {CCD_CRS}                              ! get bias frame
CCD_CRS = "{Q1}"
@s prep_dark {P1} {CCD_CRS}                              ! get dark frame
CCD_CRS = "{Q1}"
@s prep_flat {P1} {CCD_CRS}                              ! get flat frame
CCD_CRS = "{Q1}"
GOTO DORED
!
! *** here for the object
SCIENCE:
@s prep_bias {P1} {CCD_CRS}                              ! get bias frame
CCD_CRS = "{Q1}"
@s prep_dark {P1} {CCD_CRS}                              ! get dark frame
CCD_CRS = "{Q1}"
@s prep_flat {P1} {CCD_CRS}                              ! get flat frame
CCD_CRS = "{Q1}"
@s prep_illum {P1} {CCD_CRS}                             ! get illum. frame
CCD_CRS = "{Q1}"
@s prep_fring {P1} {CCD_CRS}                             ! get fringe frame
CCD_CRS = "{Q1}"
GOTO DORED
!
! *** all done: frames are prepared to be used in final step
DORED:
IF CCD_CRS(11:11) .EQ. "Y" THEN

    @s ccdcorrect {P1} {P2} {CCD_CRS}
    WRITE/DESCR {P2} -
       HISTORY/C/-1/80 "Processed by @s ccdredfrm {P1} {P2}"
    IF VERBOSE(1:1) .EQ. "Y" THEN
       STATISTIC/IMAGE {P2} {IM_SEC} ? ? FF              ! full statistics
    ELSE
       STATISTIC/IMAGE {P2} {IM_SEC} ? ? FN              ! short statistics
    ENDIF
    WRITE/OUT "{P1}: Reduction completed, output is in {P2}"

ELSEIF SC_PROC .EQ. "yes" THEN
    WRITE/OUT "{P1}: *** NO REDUCTION *** SC_PROC option was set"
    RETURN 0

ELSE
    WRITE/OUT -
    "{P1}: *** INFO: Calibration already done, OR problems ..."
    RETURN -3
ENDIF
!
! *** here calculated the mean of the flat field
IF EXP_TYP(1:2) .EQ. "FF" THEN
   @s ccdmean {P1}
ENDIF
IF EXP_TYP(1:2) .EQ. "IL" THEN
   @s ccdmean {P1}
ENDIF
IF EXP_TYP(1:2) .EQ. "FR" THEN
   @s ccdmean {P1}
ENDIF

RETURN 0
