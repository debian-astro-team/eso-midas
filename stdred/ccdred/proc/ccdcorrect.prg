! @(#)ccdcorrect.prg	19.1 (ES0-DMD) 02/25/03 14:15:59
!@(#)ccdcorrect.prg	19.1 (ESO)  02/25/03 14:15:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdcorrect.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Do the requested calbration correction on the input image
!.USE         @s ccdcorrect p1 p2 p3
!.               p1 = input frame
!.               p2 = output frame
!.               p3 = processing  option
!.VERSION     930423  RHW  Creation
!.VERSION     971113  SW   input frame has to be internaly renamed (see swolf)
!.VERSION     971201  SW   illum.correc. has to be split for SKY and SCREEN-FF,
!                          CHECK if FF has already be ill.corr.!
!-------------------------------------------------------
DEFINE/PARAM P1 ?                 I "Enter input frame [no default]:"
DEFINE/PARAM P2 ?                 I "Enter output frame [no default]:"
DEFINE/PARAM P3 {CCDCORS(1:10)}   C "Enter calibration to be done:"

!
! here for trimming and pixel correction
IF P3(2:2) .EQ. "T" THEN                          ! check if trimming is needed
   IF P1 .EQ. "{P2}" THEN
      EXTRACT/IMAGE &d = {P1}{IM_SEC}
      COPY/II &d {P2}
      WRITE/DESCR {P2} CCDSTAT/I/2/1 1                ! write descriptor status
      DELETE/IMAGE &d NO
   ELSE
      EXTRACT/IMAGE {P2} = {P1}{IM_SEC}
   ENDIF
ENDIF
!
IF P3(3:3) .EQ. "P" THEN                  ! check if pixel correction is needed
   IF FX_METH(1:1) .EQ. "A" THEN                      ! use modify/area command
      MODIFY/AREA   {P1},{FX_TABLE} {P2} {FX_FPAR(1)} {FX_NOISE}

   ELSEIF FX_METH(1:1) .EQ. "C" THEN                ! use modify/column command
      MODIFY/COLUMN {P1},{FX_TABLE} {P2} V 

   ELSEIF FX_METH(1:1) .EQ. "R" THEN                   ! use modify/row command
      MODIFY/ROW {P1},{FX_TABLE} {P2} V 

   ELSE                                             ! use modify/pixels command
      MODIFY/PIXELS {P1},{FX_TABLE} {P2} {FX_FACT} {FX_FPAR} ? {FX_NOISE}
   ENDIF
   WRITE/DESCR {P2} CCDSTAT/I/3/1 1                   ! write descriptor status
   P3(3:3) = "X" !<--- swolf@eso.org (13.11.97) otherwise next IF's are
		 !always FALSE although they should be TRUE (e.g. XXPBXX)

   P1 = "{P2}"	 !<--- swolf@eso.org (13.11.97) change name of parameter
		 !in order to get the right computations in the following
		 !IF statements!!! One has to use the CORRECTED version
		 !for the computations!
ENDIF
!
! *** here the part for the basic correction 
IF P3(1:6) .EQ. "OXXXXX" THEN                                        ! overscan
   COMPUTE/IMAGE -
   {P2} = {P1} - {CCDSCAN}               
   WRITE/DESCR {P2} CCDSTAT/I/1/1 1                  ! overscan correction done

ELSEIF P3(1:6) .EQ. "XXXBXX" THEN                                        ! bias
   COMPUTE/IMAGE -
   {P2} = {P1} - {SC_BSFRM} 
   WRITE/DESCR {P2} CCDSTAT/I/4/1 1                      ! bias correction done
!
ELSEIF P3(1:6) .EQ. "OXXBXX" THEN                             ! overscan + bias
   COMPUTE/IMAGE -
   {P2} = {P1} - {CCDSCAN} - {SC_BSFRM} 
   WRITE/DESCR {P2} CCDSTAT/I/1/1 1                  ! overscan correction done
   WRITE/DESCR {P2} CCDSTAT/I/4/1 1                     ! bias  correction done
!
! *** here the part including the dark corection
ELSEIF P3(1:6) .EQ. "XXXXDX" THEN                                        ! dark
   COMPUTE/IMAGE -
   {P2} = {P1} - {CCDDARK}*{SC_DKFRM}
   WRITE/DESCR {P2} CCDSTAT/I/5/1 1                      ! dark correction done

ELSEIF P3(1:6) .EQ. "OXXXDX" THEN                         ! overscan + dark
   COMPUTE/IMAGE -
   {P2} = {P1} - {CCDSCAN} - {CCDDARK}*{SC_DKFRM} 
   WRITE/DESCR {P2} CCDSTAT/I/1/1 1                  ! overscan correction done
   WRITE/DESCR {P2} CCDSTAT/I/5/1 1                      ! dark correction done

ELSEIF P3(1:6) .EQ. "XXXBDX" THEN                                 ! bias + dark
   COMPUTE/IMAGE -
   {P2} = {P1} - {SC_BSFRM} - {CCDDARK}*{SC_DKFRM} 
   WRITE/DESCR {P2} CCDSTAT/I/4/1 1                      ! bias correction done
   WRITE/DESCR {P2} CCDSTAT/I/5/1 1                      ! dark correction done

ELSEIF P3(1:6) .EQ. "OXXBDX" THEN                      ! overscan + bias + dark
   COMPUTE/IMAGE -
   {P2} = {P1} - {CCDSCAN} - {SC_BSFRM} - {CCDDARK}*{SC_DKFRM}
   WRITE/DESCR {P2} CCDSTAT/I/1/1 1                  ! overscan correction done
   WRITE/DESCR {P2} CCDSTAT/I/4/2 1,1              ! bias, dark correction done
!
! *** here the part with flat correction
! if flatfield corrections has to be done (illumination correction) one has to
! guarantee that the result frame will be used for these corrections. That's
! the reason why the name of the 'input' frame has to be changed to the name
! of the result frame in this part of the procedure (swolf@eso.org, 01.12.97)
ELSEIF P3(1:6) .EQ. "XXXXXF" THEN                                        ! flat
   COMPUTE/IMAGE -
   {P2} = {P1}*{CCDFLAT}/{SC_FFFRM}
   WRITE/DESCR {P2} CCDSTAT/I/6/1 1                      ! flat correction done
   P1 = "{P2}"		!if ill.corr or fringe.corr has to be done: change name

ELSEIF P3(1:6) .EQ. "OXXXXF" THEN                             ! overscan + flat
   COMPUTE/IMAGE -
   {P2} = ({P1} - {CCDSCAN})*{CCDFLAT}/{SC_FFFRM}
   WRITE/DESCR {P2} CCDSTAT/I/1/1 1                  ! overscan correction done
   WRITE/DESCR {P2} CCDSTAT/I/6/1 1                      ! flat correction done
   P1 = "{P2}"		!if ill.corr or fringe.corr has to be done: change name

ELSEIF P3(1:6) .EQ. "XXXBXF" THEN                                 ! bias + flat
   COMPUTE/IMAGE -
   {P2} = ({P1} - {SC_BSFRM})*{CCDFLAT}/{SC_FFFRM}
   WRITE/DESCR {P2} CCDSTAT/I/4/1 1                      ! bias correction done
   WRITE/DESCR {P2} CCDSTAT/I/6/1 1                      ! flat correction done
   P1 = "{P2}"		!if ill.corr or fringe.corr has to be done: change name

ELSEIF P3(1:6) .EQ. "XXXXDF" THEN                                 ! dark + flat
   COMPUTE/IMAGE -
   {P2} = ({P1} - {CCDDARK}*{SC_DKFRM}) * {CCDFLAT}/{SC_FFFRM}
   WRITE/DESCR {P2} CCDSTAT/I/5/2 1,1              ! dark, flat correction done
   P1 = "{P2}"		!if ill.corr or fringe.corr has to be done: change name

ELSEIF P3(1:6) .EQ. "OXXBXF" THEN                    ! overscan + bias + flat
   COMPUTE/IMAGE -
   {P2} = ({P1} - {CCDSCAN} - {SC_BSFRM}) * {CCDFLAT}/{SC_FFFRM}
   WRITE/DESCR {P2} CCDSTAT/I/1/1 1                  ! overscan correction done
   WRITE/DESCR {P2} CCDSTAT/I/4/1 1                      ! bias correction done
   WRITE/DESCR {P2} CCDSTAT/I/6/1 1                      ! flat correction done
   P1 = "{P2}"		!if ill.corr or fringe.corr has to be done: change name

ELSEIF P3(1:6) .EQ. "OXXXDF" THEN                      ! overscan + dark + flat
   COMPUTE/IMAGE -
   {P2} = ({P1} - {CCDSCAN} - {CCDDARK}*{SC_DKFRM}) * {CCDFLAT}/{SC_FFFRM}
   WRITE/DESCR {P2} CCDSTAT/I/1/1 1                  ! overscan correction done
   WRITE/DESCR {P2} CCDSTAT/I/5/2 1,1              ! dark, flat correction done
   P1 = "{P2}"		!if ill.corr or fringe.corr has to be done: change name

ELSEIF P3(1:6) .EQ. "XXXBDF" THEN                          ! bias + dark + flat
   COMPUTE/IMAGE -
   {P2} = ({P1} - {SC_BSFRM} - {CCDDARK}*{SC_DKFRM}) * {CCDFLAT}/{SC_FFFRM}
   WRITE/DESCR {P2} CCDSTAT/I/4/3 1,1,1       ! bias dark, flat correction done
   P1 = "{P2}"		!if ill.corr or fringe.corr has to be done: change name

ELSEIF P3(1:6) .EQ. "OXXBDF" THEN               ! overscan + bias + dark + flat
   COMPUTE/IMAGE {P2} = - 
   ({P1}-{CCDSCAN} - {SC_BSFRM} - {CCDDARK}*{SC_DKFRM}) * {CCDFLAT}/{SC_FFFRM}
   WRITE/DESCR {P2} CCDSTAT/I/1/1 1                  ! overscan correction done
   WRITE/DESCR {P2} CCDSTAT/I/4/3 1,1,1       ! bias dark, flat correction done
   P1 = "{P2}"		!if ill.corr or fringe.corr has to be done: change name
ENDIF
!
! *** here the part for the less frequently used corrections
IF P3(7:8) .EQ. "IX" THEN
   IF M$EXISTD(SC_FFFRM,"ILLFLAT") .EQ. 1 .OR. -
      M$EXISTD(SC_FFFRM,"SKYFLAT") .EQ. 1 THEN	!illum.corr. already done
     WRITE/OUT "*** WARNING: flatfield has already been illumination corrected"
     WRITE/OUT "***          '{SC_ILFRM}' will not be applied!"
   ELSE
     IF M$EXISTD(SC_ILFRM,"ILLCOR") .EQ. 1 THEN	!illum.corr. by dome-flat
       COMPUTE/IMAGE -
       {P2} = {P1}/{CCDILLUM}*{SC_ILFRM}
       WRITE/DESCR {P2} CCDSTAT/I/7/1 1           ! illum correction done
     ELSE IF M$EXISTD(SC_ILFRM,"SKYCOR") .EQ. 1 THEN !illum.corr. by dome-flat
       COMPUTE/IMAGE -
       {P2} = {P1}*{CCDILLUM}/{SC_ILFRM} 
       WRITE/DESCR {P2} CCDSTAT/I/7/1 1           ! illum correction done
     ELSE
       WRITE/OUT "*** ERROR: No proper illumination correction frames specified!"
       WRITE/OUT "***        Use ILLCOR/CCD or SKYCOR/CCD to create correction frame."
     ENDIF
   ENDIF

ELSEIF P3(7:8) .EQ. "XQ" THEN
   COMPUTE/IMAGE -
   {P2} = {P1} - {CCDFRING}*{SC_FRFRM}
   WRITE/DESCR {P2} CCDSTAT/I/8/1 1                    ! fringe correction done

ELSEIF P3(7:8) .EQ. "IQ" THEN
   IF M$EXISTD(SC_ILFRM,"ILLCOR") .EQ. 1 THEN	!illum.corr. by dome-flat
     COMPUTE/IMAGE -
     {P2} = {P1}*{CCDILLUM}*{SC_ILFRM} - {CCDFRING}*{SC_FRFRM}
   ELSE IF M$EXISTD(SC_ILFRM,"SKYCOR") .EQ. 1 THEN !illum.corr. by dome-flat
     COMPUTE/IMAGE -
     {P2} = {P1}*{CCDILLUM}/{SC_ILFRM} - {CCDFRING}*{SC_FRFRM}
   ELSE
     WRITE/OUT "*** ERROR: No proper illumination correction frames specified!"
     WRITE/OUT "***        Use ILLCOR/CCD or SKYCOR/CCD to create correction frame."
   ENDIF
   WRITE/DESCR {P2} CCDSTAT/I/7/2 1,1           ! illum, fringe correction done
ENDIF
!
RETURN
