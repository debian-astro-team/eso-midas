! @(#)ccdfrcor.prg	19.1 (ESO-DMD) 02/25/03 14:16:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdfrcor.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, SKYCOR/CCD
!.PURPOSE     Make fringe correction frame
!.USE         FRCOR/CCD p1 p2 p3 p4 p5 p6
!.            where p1 = reduction table or single flat field
!.                  p2 = output frame (only in case of single frame input)
!.                  p3 = xboxmin, xboxmax smoothing box
!.                  p4 = yboxmin, yboxmax smoothing box
!.                  p5 = clipping option
!.                  p6 = low and high clipping factor 
!.ALGORYTHM:  The procedure check if the flat field(s) are available and 
!.            continues with the pipeline reduction of each flat if that is 
!.            required. Using the reduced field a illumination frame is created
!.            by a box smoothing algorithm.
!.NOTE        The individual procedures check if one operation was already done
!.            by checking the descriptor CCDSTAT. The CCDSTAT descriptor values
!.            represent the following status:
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION:    920731  RHW  Creation
!.VERSION:    930728  RHW  Improve layout and structure
!-------------------------------------------------------
DEFINE/PARAM P1 {CCD_IN}  C "Enter input specification [no default]:"
!
! *** keyword saving
DEFINE/LOCAL CCDIN/C/1/60   " "
DEFINE/LOCAL CCDOUT/C/1/60  " "
DEFINE/LOCAL SCFRM/C/1/60   " "
DEFINE/LOCAL BSFRM/C/1/60   "{SC_BSFRM}"
DEFINE/LOCAL DKFRM/C/1/60   "{SC_DKFRM}"
DEFINE/LOCAL FFFRM/C/1/60   "{SC_FFFRM}"
DEFINE/LOCAL ILFRM/C/1/60   "{SC_ILFRM}"
!
DEFINE/LOCAL NAME/C/1/60    " "
DEFINE/LOCAL CNAME/C/1/10   " "
DEFINE/LOCAL IC/I/1/1       0
DEFINE/LOCAL IR/I/1/1       0
DEFINE/LOCAL LL/I/1/1       0
DEFINE/LOCAL NRR/I/1/1      0
DEFINE/LOCAL NCR/I/1/1      0
DEFINE/LOCAL NCA/I/1/1      0
DEFINE/LOCAL NRA/I/1/1      0
DEFINE/LOCAL IRA/I/1/1      0
DEFINE/LOCAL IDOT/I/1/1     0
DEFINE/LOCAL TBL/I/1/1      0
!
DEFINE/PARAM P3 {IL_XBOX(1)},{IL_XBOX(2)} N
DEFINE/PARAM P4 {IL_YBOX(1)},{IL_YBOX(2)} N
DEFINE/PARAM P5 {IL_CLIP} C
!
IF P5(1:1) .EQ. "Y" THEN
   DEFINE/PARAM P6 {IL_SIGMA(1)},{IL_SIGMA(2)}
ENDIF
!
! *** first check for single frame of input table
TBL = M$INDEX(P1,".tbl")                                  ! search for .bdf
IF TBL .EQ. 0 THEN                                                ! frame input
   IDOT = M$INDEX(P1,".bdf")
   IF IDOT .EQ. 0 THEN 
      CCDIN = "{P1}.bdf"
   ELSE
      CCDIN = "{P1}"
   ENDIF
   IF M$EXIST(CCDIN) .EQ. 0 THEN
      WRITE/OUT "*** ERROR: Input frame {CCDIN} doesn't exist"
      RETURN
   ENDIF
!
   DEFINE/PARAM P2 {CCD_OUT} IMA "Enter the output fringe frame:"
   IDOT = M$INDEX(P2,".bdf")
   IF IDOT .EQ. 0 THEN 
      CCDOUT = "{P2}.bdf"
   ELSE
      CCDOUT = "{P2}"
   ENDIF
!
! *** the following part in for reduction table input  
ELSE                                                    ! reduction table input
   IF M$EXIST(P1) .EQ. 0 THEN                           ! check exsistence
      WRITE/OUT "*** FATAL: Missing CCD reduction table {P1}"
      RETURN
   ENDIF
!
   COPY/TABLE {P1} &r                                 ! copy only selected part
   WRITE/OUT "{P1}: Start creating fringe correction frames"
   SET/FORMAT I3
   NCA = {&r.tbl,TBLCONTR(3)}
   NRA = {&r.tbl,TBLCONTR(4)}                            ! check number of rows
!
! *** collect the input/output frame and calibration frames
   IRA = 0
   NXTFRM:
   IRA = IRA + 1
   IF IRA .LE. {NRA} THEN
      CCDIN  = "{&r,:{SK_COL},@{IRA}}"                   ! get input image name
      CCDOUT = "{&r,:{FR_COL},@{IRA}}"                           ! output frame
!
! *** this part for checking the input frame
      IF M$EXIST(CCDIN) .EQ. 0 THEN
         WRITE/OUT "*** INFO Flat field {CCDIN} to be created"
         LL = M$INDEX(P1,"_sk")-1                           ! science image
         SCFRM  = "{P1(1:{LL})}"                               ! master science
         COMBINE/CCD SK {SCFRM} {CCDIN}                               ! combine
      ENDIF
      IF M$EXIST(CCDIN) .EQ. 0 THEN                          ! is it there?
         WRITE/OUT "*** ERROR: Failed to make combined flat frame {CCDIN}"
         RETURN
      ENDIF
!
      IF SC_BSCOR(1:1) .EQ. "y" THEN                     ! bias correc. wanted?
         BSFRM    = "{SC_BSFRM}"
         SC_BSFRM = "{&r,:{BS_COL},@{IRA}}"
      ENDIF
!            
      IF SC_DKCOR(1:1) .EQ. "y" THEN                     ! dark correc. wanted?
         DKFRM    = "{SC_DKFRM}"
         SC_DKFRM = "{&r,:{DK_COL},@{IRA}}"
      ENDIF
!            
      IF SC_FFCOR(1:1) .EQ. "y" THEN                     ! dark correc. wanted?
         FFFRM    = "{SC_FFFRM}"
         SC_FFFRM = "{&r,:{FF_COL},@{IRA}}"
      ENDIF
!            
      IF SC_ILCOR(1:1) .EQ. "y" THEN                     ! dark correc. wanted?
         ILFRM    = "{SC_ILFRM}"
         SC_ILFRM = "{&r,:{IL_COL},@{IRA}}"
      ENDIF
   ENDIF
ENDIF
!
! *** see if there is a status descriptor
IF M$EXISTD(CCDIN,"CCDSTAT") .EQ. 0 THEN
   WRITE/DESCR {CCDIN} CCDSTAT/I/1/10 0 ALL
ENDIF
!
! *** set the processing keywords
CCDCOR  = "no"                                                    ! calibration
CCDCORS = "XXXXXXXXXXXXXXXXXXXX"                                   ! init flags
!
! *** set the correction flags
@s prep_scan {CCDIN}                                         ! get the overscan
@s prep_trim {CCDIN}                                         ! get trim section
@s prep_fix  {CCDIN}                                       ! get pixel correct.
@s prep_bias {CCDIN}                                           ! get bias frame
@s prep_dark {CCDIN}                                           ! get dark frame
@s prep_flat {CCDIN}                                           ! get flat frame
@s prep_illum {CCDIN}                                         ! get illum frame
!
! *** all done: frames are prepared to be used in final step
IF CCDCOR .EQ. "yes" THEN
    @s ccdcorrect {CCDIN} {CCDIN}
    IF VERBOSE(1:1) .EQ. "Y" THEN
       STATISTIC/IMAGE {CCDIN} {IM_SEC} ? ? FF                ! full statistics
    ELSE  
       STATISTIC/IMAGE {CCDIN} {IM_SEC} ? ? FN               ! short statistics
    ENDIF
ENDIF
!
! *** now do the smoothing
@s make_fringe {CCDIN} {CCDOUT} {P3} {P4} {P5} {P6}                    ! smooth
WRITE/DESCR {CCDOUT} HISTORY/C/-1/80 "ILLCOR/CCD {P1} {P2}
!
! *** finish up
FINISH:
IF TBL .EQ. 0 THEN                                         ! single frame input
   WRITE/OUT "{CCDOUT}: Fringe frame created from {CCDIN} ready"
ELSE                                                              ! table input
   IF IRA .EQ. NRA THEN                                              ! all done
      SC_BSFRM = "{BSFRM}"                            ! restore keyword setting
      SC_DKFRM = "{DKFRM}"
      SC_FFFRM = "{FFFRM}"  
      SC_ILFRM = "{ILFRM}"
      DELETE/TABLE &r NO
      WRITE/OUT "{P1}: Fringe correction frame(s) created"
   ELSE
      GOTO NXTFRM                                         ! next frame in table
   ENDIF
ENDIF
!
RETURN
