! @(#)make_pix.prg	19.1 (ES0-DMD) 02/25/03 14:16:04
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       make_pix.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Create a ccd instrument specific bad pixel file
!.VERSION     930316  RHW  Creation
!.VERSION     930316  RHW  
!-------------------------------------------------------
DEFINE/PARAMETER P1 {INSTR} T "Enter instrument (see LOAD/CCD): "
!
! *** VMS or UNIX
IF AUX_MODE(1) .LE. 1 THEN                                                ! VMS
   define/local ccddir/c/1/60 -
          "MID_DISK:[&MIDASHOME.&MIDVERS.STDRED.CCDRED.INCL]"
   define/local protec/c/1/60 "SET PROT=(O:RWED)"
ELSE                                                                     ! UNIX
   define/local ccddir/c/1/60 -
           "$MIDASHOME/$MIDVERS/stdred/ccdred/incl/"
   define/local protec/c/1/60 "chmod 644"
ENDIF
!
! *** check in current direcotry
IF M$EXIST("{P1}.tbl") .EQ. 0 THEN                       
   IF P1(1:4) .EQ. "eso_" THEN                                  ! eso standard?
      WRITE/KEYW INPUTC/C/1/60 "{P1}"
      IF M$EXIST("{ccddir}{INPUTC}.tbl") .EQ. 1 THEN             ! table exists
         - COPY {ccddir}{INPUTC}.tbl {INPUTC}.tbl                  ! table file
      
      ELSEIF M$EXIST("{ccddir}{INPUTC}.fmt") .EQ. 1 .AND. -
             M$EXIST("{ccddir}{INPUTC}.dat") .EQ. 1 THEN                ! found
         - COPY {ccddir}{INPUTC}.fmt {INPUTC}.fmt                 ! format file
         - COPY {ccddir}{INPUTC}.dat {INPUTC}.dat                   ! data file
         $ {protec} {INPUTC}.dat 
         $ {protec} {INPUTC}.fmt
         CREATE/TABLE {INPUTC} * * {INPUTC}                        ! create table
         WRITE/KEYW FX_TABLE/C/1/60 {INPUTC}.tbl

      ELSE
         WRITE/OUT -
         "*** WARNING {P1}: ESO bad pixel file not found; nothing done"
         RETURN
      ENDIF

   ELSE
      WRITE/KEYW INPUTC/C/1/60 "{P1}"
      IF M$EXIST("{INPUTC}.fmt") .EQ. 1 .AND. -
         M$EXIST("{INPUTC}.dat") .EQ. 1 THEN                            ! found
         CREATE/TABLE {INPUTC}_pixel * * {INPUTC}                  ! create table
         WRITE/KEYW FX_TABLE/C/1/60 {INPUTC}.tbl
      ELSE
         WRITE/OUT -
         "*** WARNING {P1}: No bad pixel file found: nothing done"
         RETURN
      ENDIF
   ENDIF

ELSE
   WRITE/OUT "{P1}: CCD bad pixel table already present"
   RETURN
ENDIF
!



