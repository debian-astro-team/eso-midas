! @(#)ccdreduce.prg	19.1 (ESO-DMD) 02/25/03 14:16:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdreduce.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, REDUCE/CCD
!.PURPOSE     Do the reduction of an single science image or a series of 
!             science frames stored in a MIDAS table.
!.USE         REDUCE/CCD p1 p2
!.            where p1 = input table of science frames or single frame
!.                  p2 = output frame (only in case of single frame input)
!.ALGORYTHM:  The procedure checks for the valid input and continues with 
!.            the pipeline reduction. In case a single frame input is given
!.            calibration frames are taken for the CCD keywords exp_FRM;
!.            for the reduction of the science frames stored in a MIDAS
!.            table the calibration frames are assumed to be available in this
!             table.
!.            Each single frame is reduced in the procedure ccdredfrm.
!.NOTE        The individual procedures check if one operation was already done
!.            by checking the descriptor CCDSTAT. The CCDSTAT descriptor values
!.            represent the following status:
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for bad pixel correction
!.            CCDSTAT(8) for fringing correction
!.VERSION:    920731  RHW  Creation
!.VERSION:    930728  RHW  Improve layout and structure
!.VERSION:    940113  RHW  Change for the DO association table
!-------------------------------------------------------
DEFINE/PARAM P1 {CCD_IN} C "Enter CCD input table or frame:"
DEFINE/PARAM P3 N        C "Overwrite existing reduced output frame"
!
! *** keyword saving
DEFINE/LOCAL CCDIN/C/1/60    " " ALL
DEFINE/LOCAL CCDOUT/C/1/60   " " ALL
DEFINE/LOCAL CCDTBL/C/1/60   "{CCD_IN}"
DEFINE/LOCAL BSFRM/C/1/60    "{SC_BSFRM}"
DEFINE/LOCAL DKFRM/C/1/60    "{SC_DKFRM}"
DEFINE/LOCAL FFFRM/C/1/60    "{SC_FFFRM}"
DEFINE/LOCAL ILFRM/C/1/60    "{SC_ILFRM}" 
DEFINE/LOCAL FRFRM/C/1/60    "{SC_FRFRM}" 
!
DEFINE/LOCAL TBL/I/1/1      0
DEFINE/LOCAL TABL/C/1/60    " " ALL
DEFINE/LOCAL SELECT/C/1/60  " " ALL
DEFINE/LOCAL NCA/I/1/1      0
DEFINE/LOCAL NRA/I/1/1      0
DEFINE/LOCAL IRA/I/1/1      0
DEFINE/LOCAL IDOT/I/1/1     0
!
CCDCMD = "REDUCE/CCD"                          ! store the command and keywords
!
! *** first check for single frame of input table
TBL = M$INDEX(P1,".tbl")                                      ! search for .bdf
IF TBL .EQ. 0 THEN                                                ! frame input
   IF M$EXIST("{P1}.bdf") .EQ. 0 THEN                         ! check existence
      WRITE/OUT "*** FATAL: Missing CCD input frame {P1}"
      RETURN
   ELSE
      CCDIN = "{P1}"
   ENDIF

ELSE                                                    ! reduction table input
   IF M$EXIST(P1) .EQ. 0 THEN                            ! check exsistence
      WRITE/OUT "*** FATAL: Missing CCD input table {P1}"
      RETURN
   ELSE
      CCD_IN = "{P1}"
      CCDIN  = "{CCD_IN}"
      SELECT = "{{CCDIN},TSELTABL(1:60)}" 
      IF SELECT(1:1) .NE. " " .AND. SELECT .NE. "-" THEN
         TABL = "middummtbl.tbl"
         COPY/TABLE {P1} {TABL}                       ! copy only selected part
      ELSE
         TABL = "{CCDIN}"
      ENDIF

      SET/FORMAT I3
      NCA = {{TABL},TBLCONTR(3)}
      NRA = {{TABL},TBLCONTR(4)}                         ! check number of rows
      IF NRA .EQ. 0 THEN
         GOTO DONE
      ENDIF
   ENDIF
!
! *** collect the input/output science frame and calibration frames
   WRITE/OUT "Start processing science frames in table {P1}"
   IRA = 0
   NXTFRM:
   IRA = IRA + 1
   CCDIN   = "{{TABL},:{SC_COL},@{IRA}}"                 ! get input image name
   WRITE/OUT " "
!
   IF SC_BSCOR(1:1) .EQ. "y" THEN                        ! bias correc. wanted?
      IF M$EXISTC(TABL,BS_COL) .EQ. -1 THEN
         WRITE/OUT "*** FATAL: No BIAS column defined; *** REDUCTION STOPPED"
         GOTO FINISH
      ELSE
         SET/MIDAS OUTPUT=LOGONLY
         SC_BSFRM = "{{TABL},:{BS_COL},@{IRA}}"
         SET/MIDAS OUTPUT=YES
         IF SC_BSFRM(1:1) .EQ. " "  THEN
            WRITE/OUT "{CCDIN}: No master BIAS defined; ** NO REDUCTION ***"
            GOTO DONE
         ENDIF
      ENDIF
   ENDIF
!           
   IF SC_DKCOR(1:1) .EQ. "y" THEN                        ! dark correc. wanted?
      IF M$EXISTC(TABL,DK_COL) .EQ. -1 THEN
         WRITE/OUT "*** FATAL: No DARK column defined; *** REDUCTION STOPPED"
         GOTO FINISH
      ELSE
         SET/MIDAS OUTPUT=LOGONLY
         SC_DKFRM = "{{TABL},:{DK_COL},@{IRA}}"
         SET/MIDAS OUTPUT=YES
         IF SC_DKFRM(1:1) .EQ. " "  THEN
            WRITE/OUT "{CCDIN}: No master DARK defined; *** NO REDUCTION ***"
            GOTO DONE
         ENDIF
      ENDIF
   ENDIF
!
   IF SC_FFCOR(1:1) .EQ. "y" THEN                        ! flat correc. wanted?
      IF M$EXISTC(TABL,FF_COL) .EQ. -1 THEN
         WRITE/OUT -
           "*** FATAL: No FLAT column defined; *** REDUCTION STOPPED ***"
         GOTO FINISH
      ELSE
         SET/MIDAS OUTPUT=LOGONLY
         SC_FFFRM = "{{TABL},:{FF_COL},@{IRA}}"
         SET/MIDAS OUTPUT=YES
         IF SC_FFFRM(1:1) .EQ. " "  THEN
            WRITE/OUT "{CCDIN}: No master FLAT defined; NO REDUCTION ***"
            GOTO DONE
         ENDIF
      ENDIF
   ENDIF
!
   IF SC_ILCOR(1:1) .EQ. "y" THEN                         ! ill correc. wanted?
      IF M$EXISTC(TABL,IL_COL) .EQ. -1 THEN
         WRITE/OUT -
            "*** FATAL: No ILLUMINATION column defined; *** REDUCTION STOPPED"
         GOTO FINISH
      ELSE
         SET/MIDAS OUTPUT=LOGONLY
         SC_ILFRM = "{{TABL},:{IL_COL},@{IRA}}"
         SET/MIDAS OUTPUT=YES
         IF SC_ILFRM(1:1) .EQ. " "  THEN
            WRITE/OUT -
                "{CCDIN}: No master ILLUMINATION defined; NO REDUCTION ***"
            GOTO DONE
         ENDIF
      ENDIF
   ENDIF

   IF SC_FRCOR(1:1) .EQ. "y" THEN                      ! fringe correc. wanted?
      IF M$EXISTC(TABL,FR_COL) .EQ. -1 THEN
         WRITE/OUT "*** FATAL: No FRINGE column defined; *** REDUCTION STOPPED"
         GOTO FINISH
      ELSE
         SET/MIDAS OUTPUT=LOGONLY
         SC_FRFRM = "{{TABL},:{FR_COL},@{IRA}}"
         SET/MIDAS OUTPUT=YES
         IF SC_FRFRM(1:1) .EQ. " "  THEN
            WRITE/OUT "{CCDIN}: No master FRINGE defined; NO REDUCTION ***"
            GOTO DONE
         ENDIF
      ENDIF
   ENDIF
ENDIF
!
! *** get the input data either a single frame input or from the table
IDOT = M$INDEX(CCDIN,".") - 1
IF IDOT .GT. 0 THEN 
   CCDIN = "{CCDIN(1:{IDOT})}"
ELSE
   CCDIN = "{CCDIN}"
ENDIF

IF TBL .GT. 0 THEN
   SET/MIDAS OUTPUT=LOGONLY
   SELECT/TABL {TABL} :{SC_COL}.EQ."{CCDIN}.mt".OR. :{SC_COL}.EQ."{CCDIN}.bdf"
   SET/MIDAS OUTPUT=YES
ENDIF

IF M$EXIST("{CCDIN}.bdf") .EQ. 0 THEN                       ! check exsistence
   WRITE/OUT "*** WARNING: Missing input frame {CCDIN}; *** NO REDUCTION"
   GOTO DONE
ENDIF
!
! *** determine the output frame
IF TBL .EQ. 0 THEN
   DEFINE/PARAM P2 "{CCDIN}_red" IMA "Enter the calibrated output frame:"
   CCDOUT = "{P2}"
ELSE
   CCDOUT  = "{CCDIN}_reduc"                         ! reduce output image name
ENDIF 
IDOT = M$INDEX(CCDOUT,".") - 1
IF IDOT .GT. 0 THEN 
   CCDOUT = "{CCDOUT(1:{IDOT})}"
ELSE
   CCDOUT = "{CCDOUT}"
ENDIF
!
IF M$EXIST("{CCDOUT}.bdf") .EQ. 1 .AND. P3 .EQ. "N" THEN   ! find if output exists
   WRITE/OUT "{CCDIN}: Reduced frame {CCDOUT} already exist *** NO REDUCTION"
ELSE
   @s ccdredfrm {CCDIN} {CCDOUT}
   WRITE/OUT " "
   if {Q1} .lt. 0 then
     write/out "{CCDIN}: ERROR - Reduction failed!"
     goto FINISH
   endif
ENDIF
!
! *** done for this frame
DONE:
IF TBL .EQ. 0 THEN
   WRITE/OUT "*** ALL DONE *** Reduction frame {P1} *** ALL DONE ***"
ELSE
   IF IRA .LT. {NRA} THEN
      GOTO NXTFRM
   ELSE 
      WRITE/OUT " "
      IF NRA .EQ. 0 THEN
         WRITE/OUT "*** WARNING:  Selection in input table is empty *** ALL DONE ***"
      ELSE
         WRITE/OUT "*** ALL DONE *** Input table {P1} *** ALL DONE ***"
      ENDIF
      SET/MIDAS OUTPUT=LOGONLY
      IF SELECT(1:1) .NE. " " .AND. SELECT .NE. "-" THEN
         SELECT/TABL {P1} {SELECT}                             ! reset selection
      ELSE
         SELECT/TABL {P1} ALL                                    ! ALL selection
      ENDIF
      SET/MIDAS OUTPUT=YES
      DELETE/TABLE middummtbl.tbl NO
   ENDIF
ENDIF
!
! *** done for all
FINISH:
! *** restore keyword setting
CCD_IN   = "{CCDTBL}"
SC_BSFRM = "{BSFRM}"
SC_DKFRM = "{DKFRM}"
SC_FFFRM = "{FFFRM}"  
SC_ILFRM = "{ILFRM}"
SC_FRFRM = "{FRFRM}"
RETURN




