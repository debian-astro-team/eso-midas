! @(#)prep_fring.prg	19.1 (ESO-DMD) 02/25/03 14:16:06
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       prep_fring.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Prepare fringe correction image for the reduction
!.METHOD      - Return if fringecorrection is not requested or has been 
!               corrected previously
!.            - Get the fringe correction image. If not found return an error
!.            - If fringe image has not been processed execute ccdredfrm
!.            - Set the processing flags.
!.PARAMETERS  P1: input frame
!             P2: correction label string which will be checked by
!                 ccdcorrect.prg to do the desired actions.
!.NOTE        The procedure checks if the operation was already done
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930416  RHW  Creation
!.VERSION     930507  RHW  Check input frame for fringe correction
!.VERSION     931015  RHW  Inclusion of constant value
!.VERSION     940113  RHW  Changes for the DO association table
!.VERSION     971117  SW   correction labels have to be local -> new param.
!-------------------------------------------------------
DEFINE/PARAM P1 ? I "Enter frame for fringe correction:"
DEFINE/PARAM P2 {CCDCORS(1:20)}
!The correction label string CCDCORS which will be checked by CCDCORRECT.PRG
!has to be local, in order to always get the right link to the actual frame.
!In a global definition the correction labels may overwritten by other pre-
!pared frames which leads to unwished actions to previous frames (SW,97.11.17).
!
DEFINE/LOCAL CONST/I/1/1  0
!
! *** do we want the fringe correction or has it been done
IF SC_FRCOR .NE. "yes" .OR. {{SC_FRFRM},CCDSTAT(8)} .EQ. 1 THEN
   WRITE/OUT "{P1}: Fringe correction frame not requested or was applied"
   RETURN {P2}
ENDIF
!
! *** check the calibration table for the column
IF SC_FRFRM(1:1) .EQ. " " THEN                           ! fringe image unknown
   WRITE/OUT "{P1}: *** WARNING: Fringe correction unknown or undefined"
   RETURN {P2}
ENDIF
!
IF M$TSTNO("{SC_FRFRM}") .EQ. 1 THEN
   CONST = 1
ELSE
   IF M$EXIST("{SC_FRFRM}.bdf") .EQ. 0 THEN                      ! is it there?
      WRITE/OUT "{P1}: *** ERROR: Fringe frame {SC_FRFRM} not available"
      RETURN {P2}
   ENDIF
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Fringe correction is {SC_FRFRM}"
   RETURN {P2}
ENDIF
!
! *** here for the real thing
IF CONST .EQ. 0 THEN                                    ! here for a real frame
   IF M$EXISTD("{SC_FRFRM}","MKFRINGE") .EQ. 0 THEN
      WRITE/OUT "{SC_FRFRM}: MKFRINGE Fringe flag missing in descriptor"
      RETURN {P2}
   ENDIF
   DEFINE/LOCAL OBSTIM/R/1/2 0.0,0.0
   DEFINE/LOCAL FRSCALE/R/1/1/ 0.0
   OBSTIM(1) = {{P1},{O_DESC}}  
   OBSTIM(2) = {{SC_FRFRM},{O_DESC}}
   IF OBSTIM(1) .LE. 0 THEN
      WRITE/OUT "{P1}: *** ERROR: invalid exposure time "
      RETURN {P2}
   ENDIF
   IF OBSTIM(2) .LE. 0 THEN
      WRITE/OUT -
          "{P1}: *** ERROR: {SC_FRFRM} fringe frame has invalid exposure time"
      RETURN {P2}
   ENDIF
!
   IF M$EXISTD("{SC_FRFRM}","FRINGESCALE") .EQ. 1 THEN
      FRSCALE = {{SC_FRFRM},{FRINGESCALE}}
   ELSE
      FRSCALE = 1.0
   ENDIF
   CCDFRING = FRSCALE * OBSTIM(1)/OBSTIM(2)
!
ELSE                                                      ! here for a constant
   CCDFRING = 1.0
ENDIF
!
WRITE/OUT "{P1}: Fringe  correction {SC_FRFRM}; scale = {CCDFRING}"
P2(8:8) = "Q"
P2(11:11) = "Y"
RETURN {P2}







