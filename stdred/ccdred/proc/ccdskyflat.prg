! @(#)ccdskyflat.prg	19.1 (ESO-DMD) 02/25/03 14:16:02
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdskyflat.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, SKYCOR/CCD
!.PURPOSE     Make sky illumination corrected flat frame(s)
!.USE         SKYFLAT/CCD p1 p2
!.            where p1 = input table of science frames or single frame
!.                  p2 = output frame (only in case of single frame input)
!.                  p3 = xboxmin, xboxmax smoothing box
!.                  p4 = yboxmin, yboxmax smoothing box
!.                  p5 = clipping option
!.                  p6 = low and high clipping factor 
!.ALGORYTHM:  The procedure check if the sky frame are available and continues
!.            with the pipeline reduction of each sky frame if that is 
!.            required. Using the reduced sky the illumation frame(s) are 
!.            created by a box smoothing algorithm which multiplied with the 
!.            flat field (sky_flat.prg).
!.NOTE        The individual procedures check if one operation was already done
!.            by checking the descriptor CCDSTAT. The CCDSTAT descriptor values
!.            represent the following status:
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION:    920731  RHW  Creation
!.VERSION:    930728  RHW  Improve layout and structure
!-------------------------------------------------------
DEFINE/PARAM P1 {CCD_IN} C "Enter input specification [no default]:"
!
! *** keyword saving
DEFINE/LOCAL CCDIN/C/1/60   " "
DEFINE/LOCAL CCDOUT/C/1/60  " "
DEFINE/LOCAL SCFRM/C/1/60   " "
DEFINE/LOCAL BSFRM/C/1/60   "{SC_BSFRM}"
DEFINE/LOCAL DKFRM/C/1/60   "{SC_DKFRM}"
DEFINE/LOCAL FFFRM/C/1/60   "{SC_FFFRM}"
DEFINE/LOCAL FLATCOR/I/1/1  0
!
DEFINE/LOCAL NAME/C/1/60    " "
DEFINE/LOCAL CNAME/C/1/10   " "
DEFINE/LOCAL IC/I/1/1       0
DEFINE/LOCAL IR/I/1/1       0
DEFINE/LOCAL LL/I/1/1       0
DEFINE/LOCAL NRR/I/1/1      0
DEFINE/LOCAL NCR/I/1/1      0
DEFINE/LOCAL NCA/I/1/1      0
DEFINE/LOCAL NRA/I/1/1      0
DEFINE/LOCAL IRA/I/1/1      0
DEFINE/LOCAL IDOT/I/1/1     0
DEFINE/LOCAL TBL/I/1/1      0
!
DEFINE/PARAM P3 {IL_XBOX(1)},{IL_XBOX(2)} N
DEFINE/PARAM P4 {IL_YBOX(1)},{IL_YBOX(2)} N
DEFINE/PARAM P5 {IL_CLIP} C
!
IF P5(1:1) .EQ. "Y" THEN
   DEFINE/PARAM P6 {IL_SIGMA(1)},{IL_SIGMA(2)}
ENDIF
!
! *** first check for single frame of input table
TBL = M$INDEX(P1,".tbl")                                      ! search for .tbl
IF TBL .EQ. 0 THEN
   CCDIN = "{P1}"
   DEFINE/PARAM P2 {CCD_OUT} IMA "Enter the output illumination frame:"
   CCDOUT = "{P2}"
!
! *** the following part in for reduction table input  
ELSE
   IF M$EXIST(P1) .EQ. 0 THEN                            ! check exsistence
      WRITE/OUT "*** FATAL: Missing CCD reduction table {P1}"
      RETURN
   ELSE
      COPY/TABLE {P1} &r                              ! copy only selected part
      SET/FORMAT I3
      NCA = {&r.tbl,TBLCONTR(3)}
      NRA = {&r.tbl,TBLCONTR(4)}                         ! check number of rows
      IF NRA .EQ. 0 THEN
         GOTO FINISH
      ENDIF
   ENDIF
!
! *** collect the input/output frame and calibration frames
   IRA = 0
   WRITE/OUT "Creating flat illum. correction frames in table {P1}"
   NXTFRM:
   IRA = IRA + 1
   CCDIN  = "{&r,:{SK_COL},@{IRA}}"                      ! get input image name
   LL     = M$INDEX(CCDIN,"_sk")-1                          ! science image
   CCDOUT = "{CCDIN(1:{LL})}_sf"
!
   IF M$EXIST(CCDIN) .EQ. 0 THEN                   ! checking the sky frame
      WRITE/OUT "*** INFO Sky frame {CCDIN} to be created"
      SCFRM  = "{CCDIN(1:{LL})}"                               ! master science
      COMBINE/CCD SK {SCFRM} {CCDIN}                                  ! combine
   ENDIF
   IF M$EXIST(CCDIN) .EQ. 0 THEN                  ! checking the flat frame
      WRITE/OUT "*** ERROR: Failed to make combined sky frame {CCDIN}"
      GOTO FINISH
   ENDIF
!
   IF SC_BSCOR(1:1) .EQ. "y" THEN                        ! bias correc. wanted?
      SC_BSFRM = "{&r,:{BS_COL},@{IRA}}"
   ENDIF
!
   IF SC_DKCOR(1:1) .EQ. "y" THEN                        ! dark correc. wanted?
      SC_DKFRM = "{&r,:{DK_COL},@{IRA}}"
   ENDIF

   IF SC_FFCOR(1:1) .EQ. "y" THEN                        ! dark correc. wanted?
      SC_FFFRM = "{&r,:{FF_COL},@{IRA}}"
   ENDIF
ENDIF
!
! *** remove the .bdf extension
IDOT = M$INDEX(CCDIN,".bdf") - 1
IF IDOT .GT. 0 THEN 
   CCDIN = "{CCDIN(1:{IDOT})}"
ELSE
   CCDIN = "{CCDIN}"
ENDIF
IF M$EXIST("{CCDIN}.bdf") .EQ. 0 THEN                        ! check exsistence
   WRITE/OUT "*** FATAL: Missing input frame {P1}"
   GOTO FINISH
ENDIF
!
IDOT = M$INDEX(CCDOUT,".bdf") - 1
IF IDOT .GT. 0 THEN 
   CCDOUT = "{CCDOUT(1:{IDOT})}"
ELSE
   CCDOUT = "{CCDOUT}"
ENDIF
!
{CCDIN},{EXP_DESC} = "{IL_TYP} "
@s ccdredfrm {CCDIN} {CCDIN}                                 ! reduce the frame
FLATCOR = {{CCDIN},CCDSTAT(6)}
IF FLATCOR .NE. 1 THEN
   WRITE/OUT "{CCDIN}: *** WARNING: Frame should be flat fielded first"
   GOTO FINISH
ELSE
   @s make_illum {CCDIN} dummy_il {P3} {P4} {P5} {P6}      ! create illum frame
ENDIF
!
! *** check the presence of the illumination frame
IF M$EXIST("dummy_il.bdf") .EQ. 0 THEN                           ! is it there?
   WRITE/OUT "*** ERROR: Failed to make sky illumination frame from {CCDIN}"
   GOTO FINISH
ELSE
   IF M$EXIST("{SC_FFFRM}.bdf") .EQ. 0 THEN
      LL     = M$INDEX(SC_FFFRM,"_ff") - 1                  ! science image
      SCFRM  = "{SC_FFFRM(1:{LL})}"                            ! master science
      COMBINE/CCD FF {SCFRM} {SC_FFFRM}                               ! combine
      IF M$EXIST("{SC_FFFRM}.bdf") .EQ. 0 THEN                   ! is it there?
         WRITE/OUT "{CCDIN}: *** ERROR: Failed to make flat field correction"
         GOTO FINISH
      ENDIF
   ENDIF
   @s ccdcheck {SC_FFFRM} FF                               ! check if processed
   IF {Q1} .EQ. 1 THEN                                      ! no; not processed
      @s ccdredfrm {SC_FFFRM} {SC_FFFRM}                        ! do processing
   ENDIF
   @s make_skyflat {SC_FFFRM} dummy_il {CCDOUT}
   WRITE/DESCR {CCDOUT} HISTORY/C/-1/80 "SKYFLAT/CCD {P1} {P2}
ENDIF
!
! *** finish up
FINISH:
IF TBL .EQ. 0 THEN                                         ! single frame input
   WRITE/OUT "{CCDOUT}: Sky flat field created from {CCDIN} ready"
ELSE                                                              ! table input
   IF IRA .LT. {NRA} THEN
      GOTO NXTFRM
   ELSE 
      WRITE/OUT " "
      IF NRA .EQ. 0 THEN
         WRITE/OUT "{P1}: *** WARNING: Selection in reduction table is empty "
      ELSE
         WRITE/OUT "{P1}: Sky flat field frames ready"
      ENDIF
      DELETE/TABLE &r NO
   ENDIF
ENDIF
!
DELETE/IMAGE dummy_il NO
SC_BSFRM = "{BSFRM}"                                  ! restore keyword setting
SC_DKFRM = "{DKFRM}"
SC_FFFRM = "{FFFRM}"
RETURN
