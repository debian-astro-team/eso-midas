! @(#)prep_flat.prg	19.1 (ESO-DMD) 02/25/03 14:16:06
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       prep_flat.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Prepare flat correction image for the reduction
!.METHOD      - Return if flat correction is not requested or has been 
!               corrected previously
!.            - Get the flat correction image. If not found return an error
!.            - If flat image has not been processed execute ccdredfrm
!.            - Set the processing flags.
!.PARAMETERS  P1: input frame
!             P2: correction label string which will be checked by
!                 ccdcorrect.prg to do the desired actions.
!.NOTE        The procedure checks if the operation was already done
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930417  RHW  Creation
!.VERSION     930507  RHW  Check input frame for flat field correction
!.VERSION     940113  RHW  Change for DO association table
!.VERSION     971117  SW   correction labels have to be local -> new param.
!-------------------------------------------------------
DEFINE/PARAM P1 ? I "Input frame for flat field correction:"
DEFINE/PARAM P2 {CCDCORS(1:20)}
!The correction label string CCDCORS which will be checked by CCDCORRECT.PRG
!has to be local, in order to always get the right link to the actual frame.
!In a global definition the correction labels may overwritten by other pre-
!pared frames which leads to unwished actions to previous frames (SW,97.11.17).
!
DEFINE/LOCAL CONST/I/1/1  0
!
! *** do we want the flat correction
IF SC_FFCOR .NE. "yes" .OR. {{P1},CCDSTAT(6)} .EQ. 1 THEN
   WRITE/OUT "{P1}: Flat correction not requested or has been applied"
   RETURN {P2}
ENDIF
!
! *** check the bias image name
IF SC_FFFRM(1:1) .EQ. " " THEN                             ! flat image unknown
   WRITE/OUT "{P1}: *** WARNING: Flat correction unknown or undefined"
   RETURN {P2}
ENDIF
!
! *** check if flat frame is a constant
IF M$TSTNO("{SC_FFFRM}") .EQ. 1 THEN    
   CONST = 1
ELSE                                                        ! real frame wanted
   IF M$EXIST("{SC_FFFRM}.bdf") .EQ. 0 THEN                   ! make flat frame
      WRITE/OUT "{P1}: Flat field correction {SC_FFFRM} to be created"
      COMBINE/CCD FF
      IF M$EXIST("{SC_FFFRM}.bdf") .EQ. 0 THEN                   ! is it there?
         WRITE/OUT "{P1}: *** ERROR: Failed to make flat field correction"
         RETURN {P2}
      ENDIF
   ENDIF
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Flat correction is {SC_FFFRM}"
   RETURN {P2}
ENDIF
!
! *** here for the real thing
IF CONST .EQ. 0 THEN                                           ! frame required
   @s ccdcheck {SC_FFFRM} FF                               ! check if processed
   IF Q1 .EQ. "1" THEN                                      ! no; not processed
      @s ccdredfrm {SC_FFFRM} {SC_FFFRM}                        ! do processing
      IF P2(11:11) .NE. "Y" THEN
         RETURN {P2}
      ENDIF
   ENDIF
   IF M$EXISTD("{SC_FFFRM}","CCDMEAN") .EQ. 1 THEN
      CCDFLAT = {{SC_FFFRM},CCDMEAN}
   ELSE
      @s ccdmean {SC_FFFRM}
      CCDFLAT = {CCDMEAN}
   ENDIF
ELSE
   CCDFLAT = 1.0
ENDIF
!
WRITE/OUT "{P1}: Flat correction is {SC_FFFRM}; scale = {CCDFLAT}"
P2(6:6) = "F"
P2(11:11) = "Y"
RETURN {P2}
