! @(#)prep_dark.prg	19.1 (ES0-DMD) 02/25/03 14:16:05
! @(#)prep_dark.prg	19.1 (ESO-SDAG) 02/25/03 14:16:05
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       prep_dark.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Prepare dark correction image for the reduction
!.METHOD      - Return if dark correction is not requested or has been 
!               corrected previously
!.            - Get the dark correction image. If not found return an error
!.            - If dark image has not been processed execute ccdredfrm
!.            - Set the processing flags.
!.PARAMETERS  P1: input frame
!             P2: correction label string which will be checked by
!                 ccdcorrect.prg to do the desired actions.
!.NOTE        The procedure checks if the operation was already done
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930416  RHW  Creation
!.VERSION     930507  RHW  Check input frame for dark correction
!.VERSION     931015  RHW  Include constant dark
!.VERSION     940113  RHW  Change for DO association table
!.VERSION     971117  SW   correction labels have to be local -> new param.
!-------------------------------------------------------
DEFINE/PARAM P1 ? I "Input frame for dark correction:"
DEFINE/PARAM P2 {CCDCORS(1:20)}
!The correction label string CCDCORS which will be checked by CCDCORRECT.PRG
!has to be local, in order to always get the right link to the actual frame.
!In a global definition the correction labels may overwritten by other pre-
!pared frames which leads to unwished actions to previous frames (SW,97.11.17).
!
DEFINE/LOCAL CONST/I/1/1  0 
!
! *** do we want the dark correction
IF SC_DKCOR .NE. "yes" .OR. {{P1},CCDSTAT(5)} .EQ. 1 THEN
   WRITE/OUT "{P1}: Dark correction not requested or has been applied"
   RETURN {P2}
ELSE
   P2(11:11) = "Y"  !<--- swolf@eso.org (13.11.97)
ENDIF
!
! *** check the DARK image name
IF SC_DKFRM(1:1) .EQ. " " THEN                             ! dark image unknown
   WRITE/OUT "{P1}: *** WARNING: Dark correction unknown or undefined"
   RETURN {P2}
ENDIF
!
! *** check if dark current correction is a constant
IF M$TSTNO("{SC_DKFRM}") .EQ. 1 THEN                 ! check for constant value
   CONST = 1
ELSE                                                        ! real frame wanted
   IF M$EXIST("{SC_DKFRM}.bdf") .EQ. 0 THEN                   ! make dark frame
      WRITE/OUT "{P1}: Dark correction {SC_DKFRM} to be created"
      COMBINE/CCD DK  
      IF M$EXIST("{SC_DKFRM}.bdf") .EQ. 0 THEN                   ! is it there?
         WRITE/OUT "{P1}: *** ERROR: Failed to make dark correction"
         RETURN {P2}
      ENDIF
   ENDIF
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Dark correction is {SC_DKFRM}"
   RETURN {P2}
ENDIF
!
! *** here for the real thing
IF CONST .EQ. 0 THEN                                           ! frame required
   @s ccdcheck {SC_DKFRM} DK                               ! check if processed
   IF Q1 .EQ. "1" THEN                                      ! no; not processed
      @s ccdredfrm {SC_DKFRM} {SC_DKFRM}                        ! do processing
      IF P2(11:11) .NE. "Y" THEN
         RETURN {P2}
      ENDIF
   ENDIF
   DEFINE/LOCAL OBSTIM/R/1/2 0.0,0.0
   OBSTIM(1) = {{P1},{O_DESC}}  
   OBSTIM(2) = {{SC_DKFRM},{O_DESC}}
   IF OBSTIM(1) .LE. 0 THEN
      WRITE/OUT "{P1}: *** ERROR: invalid exposure time "
      RETURN {P2}
   ENDIF
   IF OBSTIM(2) .LE. 0 THEN
     WRITE/OUT "{P1}: *** ERROR: {SC_DKFRM} dark has invalid exposure time"
     RETURN {P2}
   ENDIF
   CCDDARK = OBSTIM(1)/OBSTIM(2)
ELSE
   CCDDARK = 1.0
ENDIF
!
WRITE/OUT "{P1}: Dark correction is {SC_DKFRM}; scale = {CCDDARK}"
P2(5:5) = "D"
P2(11:11) = "Y"
RETURN {P2}
