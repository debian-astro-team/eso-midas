! @(#)ccdcopy.prg	19.1 (ES0-DMD) 02/25/03 14:15:59
! @(#)ccdcopy.prg	19.1 (ESO-DAG) 02/25/03 14:15:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       ccdcopy.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Copy telescope/detector/instrument specification table and 
!             ccd descriptor procedure.
!.USE         @s ccdcopy
!.VERSION     940113  RHW  Creation
!-------------------------------------------------------
DEFINE/LOCAL SPECS/C/1/20 "eso_specs"
DEFINE/LOCAL DESCR/C/1/20 "eso_descr"
define/local ccddir/c/1/60 " " all
define/local protec/c/1/60 " " ALL
!
! *** copy the linking procedure
IF M$EXIST("{DESCR}.prg") .EQ. 0 THEN                            ! no         
   IF AUX_MODE(1) .LE. 1 THEN                                    ! VMS
      write/keyw ccddir/c/1/60 -
            "MID_DISK:[&MIDASHOME.&MIDVERS.STDRED.CCDRED.PROC]"
      write/keyw protec/c/1/60 "SET PROT=(O:RWED)"
   ELSE                                                          ! UNIX
      write/keyw ccddir/c/1/60 -
              "$MIDASHOME/$MIDVERS/stdred/ccdred/proc/"
      write/keyw protec/c/1/60 "chmod 644"
   ENDIF
   -COPY {ccddir}{DESCR}.prg {DESCR}.prg                         ! procedure
ENDIF

! *** check if ccd specification file exists
IF M$EXIST("{SPECS}.tbl") .EQ. 0 THEN                            ! no         
   IF AUX_MODE(1) .LE. 1 THEN                                    ! VMS
      write/keyw ccddir/c/1/60 -
            "MID_DISK:[&MIDASHOME.&MIDVERS.STDRED.CCDRED.INCL]"
      write/keyw protec/c/1/60 "SET PROT=(O:RWED)"
   ELSE                                                          ! UNIX
      write/keyw ccddir/c/1/60 -
              "$MIDASHOME/$MIDVERS/stdred/ccdred/incl/"
      write/keyw protec/c/1/60 "chmod 644"
   ENDIF
   -COPY {ccddir}{SPECS}.fmt {SPECS}.fmt                         ! format file
   -COPY {ccddir}{SPECS}.dat {SPECS}.dat                         ! data file
    $ {protec} {SPECS}.dat 
   $ {protec} {SPECS}.fmt
   CREATE/TABLE {SPECS} * * {SPECS} {SPECS}                        ! create table
ENDIF





