! @(#)ccdflat.prg	19.1 (ESO-DMD) 02/25/03 14:15:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       ccdflat.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Do the correction for flat field
!.VERSION     920731  RHW  Creation
!.NOTE        The procedure checks if the operation was already done
!.            by checking the descriptor CCDSTAT(6)
!.            Other CCDSTAT descriptor values
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!-------------------------------------------------------
DEFINE/PARAM P1 ?                 IMA "Enter input frame: "
DEFINE/PARAM P2 ?                 IMA "Enter output frame: "
DEFINE/PARAM P3 {SC_FFFRM}        IMA "Enter flat frame: "
!
! *** check if frame has already be flatfielded
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/OUT "{P1}: *** WARNING: No check on CCD reduction status possible"
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ELSE
   IF {{P1},CCDSTAT(6)} .EQ. 1 THEN 
      WRITE/OUT "{P1}: *** WARNING: Flat field was applied; nothing done ..." 
      RETURN
   ENDIF
ENDIF
!
! *** check existence of flat field and descriptors; return on errors
IF M$EXIST("{P3}.bdf"} .EQ. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: Flat field {P3}.bdf doesn't exist ..."
   RETURN
ENDIF
IF FF_TYP .NE. "*" .AND. FF_TYP .NE. "?" THEN 
   IF M$EXISTD(P3,EXP_DESC) .EQ. 1 .AND. -
      "{{P3},{EXP_DESC}}" .NE. "{FF_TYP}" THEN
      WRITE/OUT -
      "{P3}: *** ERROR: Exposure descriptor {EXP_DESC} has wrong type"
   RETURN
   ENDIF
ENDIF
!
DEFINE/LOCAL CCDFLAT/R/1/1 0
IF M$EXISTD(P3,"CCDMEAN") .EQ. 0 THEN
   STATISTIC/IMAGE {P3} {IM_SEC} ? ? SN                   ! get the mean
   COPY/KD OUTPUTR/R/3/1 {P3} CCDMEAN/R/1/1               ! store in descriptor
ENDIF
CCDFLAT = {{P3},CCDMEAN}
IF CCDFLAT .LE. 0 THEN
   CCDFLAT = 1.0
ENDIF
!
! *** do the work
COMPUTE/IMAGE {P2} = {P1}*{CCDFLAT}/{P3}
WRITE/DESCR {P2} CCDSTAT/I/6/1 1                          ! correction done
WRITE/DESCR {P2} HISTORY/C/-1/80 "FLAT/CCD {P1} {P2} {P3}
IF VERBOSE(1:1) .EQ. "Y" THEN
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FF                   ! full statistics
ELSE 
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FX                   ! short statistics
ENDIF
!
RETURN






