! @(#)ccdtrim.prg	19.1 (ESO-DMD) 02/25/03 14:16:03
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       ccdtrim.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Trimmed image from overscan area.
!.USE         @s ccdtrim p1 p2 p3 p4
!.               p1 = input frame; 
!.               p2 = output frame; 
!.               p3 = useful data area;
!.               p4 = delete flag.
!.METHOD      A subimage of the desired size is extracted by EXTRACT/IMAGE 
!.            command. All coordinates are in (integer pixels)
!.NOTE        The procedure checks if the operation was already done
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     921005  RHW  Creation
!-------------------------------------------------------
DEFINE/PARAM P1 ?         I  "Enter input frame:"
DEFINE/PARAM P2 ?         I  "Enter output frame:"
DEFINE/PARAM P3 {IM_SEC}  C  "Enter frame area to be extracted:"
DEFINE/PARAM P4 NO        C  "Delete input frame after extraction:"
!
! *** check if frame was trimmed
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/OUT "{P1}: *** WARNING: No check on CCD reduction status possible"
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ELSE
   IF {{P1},CCDSTAT(2)} .EQ. 1 THEN           
      WRITE/OUT "{P1}: *** WARNING: Frame was trimmed; nothing done ..."
      RETURN
   ENDIF
ENDIF
!
! *** do the extraction administration
EXTRACT/IMAGE {P2} = {P1}{P3}
WRITE/DESCR {P2} HISTORY/C/-1/80 "TRIM/CCD {P1} {P2} {P3} {P4}
WRITE/DESCR {P2} CCDSTAT/I/2/1 1                      ! image trimming done
IF VERBOSE(1:1) .EQ. "Y" THEN
   STATISTIC/IMAGE {P2} [<,<:>,>] ? ? FF                   ! full statistics
ELSE
   STATISTIC/IMAGE {P2} [<,<:>,>] ? ? FX                   ! short statistics
ENDIF
!
! *** delete frame if requested
IF P4(1:1) .EQ. "Y" THEN
  DELETE/IMAG {P1} NOCONF
ENDIF
!
RETURN

