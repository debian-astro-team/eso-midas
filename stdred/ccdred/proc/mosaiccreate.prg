! @(#)mosaiccreate.prg	19.1 (ES0-DMD) 02/25/03 14:16:04
! @(#)mosaiccreate.prg	19.1 (ESO-SDAG) 02/25/03 14:16:04  
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       mosaiccreate.prg
!.PURPOSE     Do mosaicing of a number of input frames
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, mosacing
!.USE         @@ mosaiccreate
!.            with p1 = input catalogue with exposures 
!.                 p2 = output combine frame
!.VERSION     920629  RHW  Creation
!.VERSION     920716  RHW  parameter p2
!.VERSION     930725  RHW  chnage of syntax
!-------------------------------------------------------
DEFINE/PARAMETER P1  ?   C  "Enter input catalogue:"
DEFINE/PARAMETER P2  ?   C  "Enter combined output frame:"
DEFINE/PARAMETER P3  ?   C  "Enter operations table:"
DEFINE/PARAMETER P4  ?   N  "Enter number of subframes in x and y:"
DEFINE/PARAMETER P5  " " C  "Enter list of unobserved rasters:"
DEFINE/PARAMETER P6  0,0 N  "Enter size of the output frame:"
!
DEFINE/LOCAL CCDIN/C/1/60    " "
DEFINE/LOCAL CCDOUT/C/1/60   " "
DEFINE/LOCAL CCDTBL/C/1/60   " "
DEFINE/LOCAL IDOT/I/1/1 0
WRITE/KEYW   INPUTI/I/1/2 {P4}
WRITE/KEYW   INPUTI/I/3/2 {P6}  
! *** check the consistency of the input parameters 
!
! *** All done; now copy remaining keywords to local ones
IDOT = M$INDEX("{P1}",".cat")
IF IDOT .EQ. 0 THEN 
   CCDIN = "{P1}.cat"                         ! input catalog
ELSE
   CCDIN = "{P1}"                             ! input catalog
ENDIF
IF M$EXIST("{CCDIN}") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <MOSAIC>: Catalogue {CCDIN} not existing"
   RETURN/EXIT
ENDIF
!
IDOT = M$INDEX("{P2}",".bdf")
IF IDOT .GT. 0 THEN 
   CCDOUT = "{P2(1:{IDOT})}"                      ! reference frame  
ELSE
   CCDOUT = "{P2}"                                ! reference frame
ENDIF
!
IDOT = M$INDEX("{P3}",".tbl")
IF IDOT .GT. 0 THEN 
   CCDTBL = "{P3(1:{IDOT})}"                   ! reference frame  
ELSE
   CCDTBL = "{P3}"                             ! reference frame
ENDIF
!
WRITE/KEYW   NSUB/I/1/2      {INPUTI(1)},{INPUTI(2)}  ! # frames in x and y
WRITE/KEYW   OSIZE/I/1/2     {INPUTI(3)},{INPUTI(4)}  ! size of output frame
WRITE/KEYW   NULL_IN/C/1/60  {P5}            ! list of unobserved rasters
!
DEFINE/LOCAL SECTION/C/1/40  {MO_SEC}        ! frame section for finding mode
DEFINE/LOCAL SUBTR/C/1/10    {MO_SUBT}       ! subtract median sky 
DEFINE/LOCAL CORNER/C/1/2    {MO_CORN}       ! starting position in output
DEFINE/LOCAL DIRECT/C/1/2    {MO_DIREC}      ! add subraster row/column wise
DEFINE/LOCAL RASTER/C/1/2    {MO_RAST}       ! add in raster pattern
DEFINE/LOCAL NOVER/I/1/2     {MO_OVER(1)},{MO_OVER(2)} ! #c/r betw. adj. frs
DEFINE/LOCAL BLANK/C/1/20    {MO_NUL}        ! null value
!
RUN STD_EXE:CCDMOSAIC
!
! *** check on existence of the output frame
IF M$EXIST("{CCDOUT}.bdf") .EQ. 0 THEN                           ! is it there?
   WRITE/OUT "*** ERROR: Failed to make the mosaic output frame"
   RETURN
ENDIF
!
! *** write the descriptors
WRITE/DES {CCDOUT} LHCUTS/R/1/1 {OUTPUTR(1)},{OUTPUTR(2)}
WRITE/DES {CCDOUT} MEAN/R/1/1   {OUTPUTR(3)}
WRITE/DES {CCDOUT} MODE/R/1/1   {OUTPUTR(7)}
WRITE/DES {CCDOUT} MEDIAN/R/1/1 {OUTPUTR(8)}
!
RETURN








