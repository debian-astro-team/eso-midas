! @(#)ccdcheck.prg	19.1 (ES0-DMD) 02/25/03 14:15:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       ccdcheck.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Check the processing status of an image
!.USE         @s ccdcheck p1 p2
!.               p1 = input frame; 
!.               p2 = exposure type
!.METHOD      - Check for the CCD keyword setting and the image descriptor
!.VERSION     930416  RHW  Creation
!.NOTE        The procedure checks if the operation was already done
!.            CCDSTAT(1)  for overscan correction
!.            CCDSTAT(2)  for trimming the frame
!.            CCDSTAT(3)  for the bad pixel correction
!.            CCDSTAT(4)  for bias subtraction
!.            CCDSTAT(5)  for dark current subtraction
!.            CCDSTAT(6)  for flat fielding
!.            CCDSTAT(7)  for bad pixel correction
!.            CCDSTAT(8)  for fringing correction
!-------------------------------------------------------
DEFINE/PARAM P1 ?  I       "Enter input frame [no default]:"
DEFINE/PARAM P2 ?  C       "Enter the exposure type [no default]: "
!
! *** see if there is any status descriptor; if not create one
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ENDIF
!
! *** Check if overscan correction has been dome
IF SC_SCAN .EQ. "yes" .AND. {{P1},CCDSTAT(2)} .NE. 1 THEN
   RETURN 1
ENDIF
!
! *** Check if trimming has already been applied
IF SC_TRIM .EQ. "yes" .AND. {{P1},CCDSTAT(1)} .NE. 1 THEN
   RETURN 1
ENDIF
!
! *** Check if bad pixel correction has been applied
IF SC_FXPIX .EQ. "yes" .AND. {{P1},CCDSTAT(1)} .NE. 1 THEN
   RETURN 1
ENDIF
!
! *** branch depending on exposure type
BRANCH P2(1:2) BS,DK,FF,SK,IL BIAS,DARK,FLAT,FLAT,ILLUM
!
! *** here for the default case
DEFAULT:
IF SC_BSCOR .EQ. "yes" .AND. {{P1},CCDSTAT(4)} .NE. 1 THEN          ! bias
   RETURN 1
ENDIF
IF SC_DKCOR .EQ. "yes" .AND. {{P1},CCDSTAT(5)} .NE. 1 THEN          ! dark
   RETURN 1
ENDIF
IF SC_FFCOR .EQ. "yes" .AND. {{P1},CCDSTAT(6)} .NE. 1 THEN          ! flat
   RETURN 1
ENDIF
IF SC_ILCOR .EQ. "yes" .AND. {{P1},CCDSTAT(7)} .NE. 1 THEN          ! illum
   RETURN 1
ENDIF
IF SC_FRCOR .EQ. "yes" .AND. {{P1},CCDSTAT(8)} .NE. 1 THEN          ! fringe
   RETURN 1
ENDIF
RETURN 0
!
BIAS:
RETURN 0
!
! *** here for the dark
DARK:                                                                
IF SC_BSCOR .EQ. "yes" .AND. {{P1},CCDSTAT(4)} .NE. 1 THEN          ! bias
   RETURN 1
ENDIF
RETURN 0
!
! *** here for the flat
FLAT:
IF SC_BSCOR .EQ. "yes" .AND. {{P1},CCDSTAT(4)} .NE. 1 THEN          ! bias
   RETURN 1
ENDIF
IF SC_DKCOR .EQ. "yes" .AND. {{P1},CCDSTAT(5)} .NE. 1 THEN          ! dark
   RETURN 1
ENDIF
IF M$EXISTD(P1,"CCDMEAN") .EQ. 0 THEN                           ! mean
   RETURN 1
ELSE
   CCDMEAN = {{P1},CCDMEAN}
ENDIF
RETURN 0
!
! *** here for illumination
ILLUM:
IF SC_BSCOR .EQ. "yes" .AND. {{P1},CCDSTAT(4)} .NE. 1 THEN          ! bias
   RETURN 1
ENDIF
IF SC_DKCOR .EQ. "yes" .AND. {{P1},CCDSTAT(5)} .NE. 1 THEN          ! dark
   RETURN 1
ENDIF
IF SC_FFCOR .EQ. "yes" .AND. {{P1},CCDSTAT(6)} .NE. 1 THEN          ! flat
   RETURN 1
ENDIF
IF M$EXISTD(P1,"CCDMEAN") .EQ. 0 THEN                           ! mean
   RETURN 1
ELSE
   CCDMEAN = {{P1},CCDMEAN}
ENDIF
RETURN 0


