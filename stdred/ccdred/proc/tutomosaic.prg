! @(#)tutomosaic.prg	19.1 (ES0-DMD) 02/25/03 14:16:06
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: tutomosaic.prg
!.PURPOSE:        Demonstration procedure for the ccd mosaic package. 
!.AUTHOR:         Rein Warmels  ESO - Garching May, 1999
!.USE:            Execute as TUTORIAL/MOSAIC
!.NOTE:           The following files should exist in MID_TEST:
!                 dor[0-5].fits, dor.cat, falign.tfits, calign.tfits 
!.VERSION:        990504 RHW creation
! -----------------------------------------------------------------------
! Procedure for demostrating the mosaicing capabilities
!
-copy MID_TEST:dor.cat dor.cat
indisk/fits MID_TEST:dor0.fits dor0.bdf >Null
indisk/fits MID_TEST:dor1.fits dor1.bdf >Null
indisk/fits MID_TEST:dor2.fits dor2.bdf >Null
indisk/fits MID_TEST:dor3.fits dor3.bdf >Null
indisk/fits MID_TEST:dor4.fits dor4.bdf >Null
indisk/fits MID_TEST:dor5.fits dor5.bdf >Null
indisk/fits MID_TEST:falign.tfits falign.tbl >Null
indisk/fits MID_TEST:calign.tfits calign.tbl >Null

! *** first we create the mosaic
write/out "This demonstration shows the mosaicing package in ESO-MIDAS"
write/out "consisting of mosaic creation, subraster alignment and matching"
crea/display
load/lut rainbow
wait 5
write/out "First we create a 3x3 mosaic with three input frames. Mosaic"
write/out "subraster numbers 3, 5, and 7 will be empty. The MIDAS table"
write/out "dormdb contains a database of the input data frames" 
crea/mosaic dor.cat dormos dormdb 3,3 3,5,7
read/tab dormdb
load dormos
wait 5
!
write/out " "
write/out "Next we create a 3x2 mosaic will all elements filled. The data"
write/out "base again contains the detailed input for this mosaic."
crea/mosaic dor.cat dormos dormdb 3,2
read/tab dormdb
load dormos
wait 5
!
! here the part for the alignment
write/out " "
write/out "Now we start the alignment using three different methods"
write/out "Method 1: Alignment based on constant shift"
write/out "  The method assumes a constant shift in x and/or y. The input"
write/out "  frame are shifted with respect to each other and combined."
write/out "  In this example the shifts are 30 pixels in x and y. The"
write/out "  reference subraster is subraster #2"
align/mosaic dormos dormdb doraligns s,30,30    ! shift align
load doraligns         
wait 5
!
write/out " "
write/out "Method 2: Alignment based on a reference table"
write/out "  The method uses a MIDAS table containing the relative shifts"
write/out "  of the subrasters with respect to a reference subraster (here"
write/out "  frame #2. First the reference table will be shown"
write/out " "
read/tab falign                                 ! read table
cop/tt falign :X_offset dormdb :X_offpix
cop/tt falign :Y_offset dormdb :Y_offpix
align/mosaic dormos dormdb doralignf r,dormdb   ! table, align
load doralignf
wait 5
!
write/out " "
write/out "Method 3: Alignment based on MIDAS table containing common objects"
write/out "  The method uses a MIDAS table with common objects. This table"
write/out "  can be create by the command SHIFT/MOSAIC". First we show the "
write/out "  input coordinate table; hereafter we do the alignment."
read/tab calign                                 ! read table
align/mosaic dormos dormdb doralignc c,calign   ! coord. align
load doralignf
wait 5
!
! *** here the part for the mosaicing
write/out " "
write/out " "
write/out "After the pixel alignment we now show both pixel alignment and"
write/out "intensity matching. Again we have three methods: constant shift,"
write/out "a MIDAS shift table, and a MIDAS table with common objects."
write/out " "
match/mosaic dormos dormdb dormatchc c,calign
write/out "Here comes the mosaiced image using the constant shift method"
load dormatchc
match/mosaic dormos dormdb dormatchs s,30,30
write/out "Here comes the mosaiced image using the shift table"
load dormatchs
match/mosaic dormos dormdb dormatchf r,dormdb
write/out "Here comes the mosaiced image using the table with common objects"
load dormatchf
wait 5
!
! *** determine the offset in the overlap regions
write/out " "
write/out "Finally, we show the command FIT/MOSAIC that aligns and matches the"
write/out "subrasters in the mosaic by taking into account all information"
write/out "to determine the best possible estimate of the offsets between the"
write/out "individual subrasters. Rather then having a boostrap approach like"
write/out "in the command MATCH/MOSAIC, FIT/MOSAIC uses all offsets between"
write/out "all frames."
write/out "For further information read the on-line help of FIT/MOSAIC" 
fit/mosa  dormos ? dormdb doroff
write/out " "
write/out "Here comes the mosaiced image using the overlap regions"
load doroff
write/out Tutorial finished...








