! @(#)mosaicshift.prg	19.1 (ES0-DMD) 02/25/03 14:16:05
! @(#)mosaicshift.prg	19.1 (ESO-SDAG) 02/25/03 14:16:05
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       mosaicshift.prg
!.PURPOSE     Get offsets/shifts from mosaiced frame
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, mosacing
!.USE         @@ mosaicshift
!.            with p1 = input ccd mosaic
!.                 p2 = output table containing the coordinates
!.                 p3 = centering option
!.                 p4 = subwindow size
!.VERSION     950802  RHW  Creation
!-------------------------------------------------------
DEFINE/PARAMETER P1  ?     C  "Enter output coordinates table:"
DEFINE/PARAMETER P2  GC    C  "Enter centering option:"
DEFINE/PARAMETER P3  50,50 N  "Enter subwindow size in x and y:"
DEFINE/PARAMETER P4  Y     C  "Enter option for clearing overlay:"

IF P4(1:1) .EQ. "Y" THEN
   CLEAR/CHANNEL OVERLAY 
ENDIF

IF P2(1:2) .EQ. "GC" THEN
   GET/CURSOR {P1}

ELSE IF P2(1:1) .EQ. "C" THEN
   DEFINE/LOCAL STARTX/D/1/1 0.d0
   DEFINE/LOCAL STARTY/D/1/1 0.d0
   DEFINE/LOCAL STEPX/D/1/1 0.d0
   DEFINE/LOCAL STEPY/D/1/1 0.d0

   IF P2(1:2) .EQ. "CG" THEN
      CENTER/GAUSS CURSOR {P1} ? 2,1,9999 {P3}
   ELSEIF P2(1:2) .EQ. "CM" THEN
      CENTER/MOMENT CURSOR {P1} ? 2,1,9999 {P3}
   ELSEIF P2(1:2) .EQ. "CI" THEN
      CENTER/IQE CURSOR {P1} ? 2,1,9999 {P3}
   ENDIF

   STARTX = {{IDIMEMC},START(1)}         
   STARTY = {{IDIMEMC},START(2)}
   STEPX  = {{IDIMEMC},STEP(1)}         
   STEPY  = {{IDIMEMC},STEP(2)}
   COMPUTE/TABLE {P1} :X_coordpix = INT((:XCEN-{STARTX})/{STEPX})         
   COMPUTE/TABLE {P1} :Y_coordpix = INT((:YCEN-{STARTY})/{STEPY})
   NAME/COLU {P1} :ICENT :VALUE

ELSE
   WRITE/OUT "FATAL: Unknown centering option"
ENDIF








