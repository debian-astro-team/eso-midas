! @(#)make_fringe.prg	19.1 (ESO-DMD) 02/25/03 14:16:03
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       make_fringe.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Make fringe frame
!.USE:        @s make_fringe p1 p2
!.            where p1 is the reduced input sky frame
!.                  p2 in output fringe correction frame
!.VERSION     930806  RHW  Creation
!------------------------------------------------------------------------------
DEFINE/PARAM P1 ?      IMA "Enter the input frame:"
DEFINE/PARAM P2 ?      IMA "Enter the output fringe frame:"
!
! *** illumination operation already done
IF M$EXISTD(P1,"MKFRING") .EQ. 1 THEN
   RETURN
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Fringe correction is {P2}"
   RETURN
ENDIF
!
! *** smoothing box in x and y
DEFINE/PARAMETER P3 {IL_XBOX(1)},{IL_XBOX(2)} N "Size of smoothing box in x"
DEFINE/PARAMETER P4 {IL_YBOX(1)},{IL_YBOX(2)} N "Size of smoothing box in y"
DEFINE/LOCAL XBOX/R/1/2 {P3}                                   ! x smooting box
DEFINE/LOCAL YBOX/R/1/2 {P4}                                  ! y smoothing box
!
! ** to clip or not to clip
DEFINE/PARAM P5 {IL_CLIP} C  "Enter clip option [{IL_CLIP}]:"
DEFINE/LOCAL CLIP/C/1/3 {P5}                                        ! clip flag
IF P5(1:1) .EQ. "y" THEN
   DEFINE/PARAM P6 {IL_SIGMA(1)},{IL_SIGMA(2)} N "Enter low and high sigma:"
   DEFINE/LOCAL SIGMA/R/1/2 {P6}                                 ! clip numbers
ENDIF
!
! *** here we finally do it !!!
WRITE/KEYW  IN_A {P1}                                              ! input frame
IF P1 .EQ. "{P2}" THEN                                           ! output frame
   OUT_A = "dummy_il"
   RUN STD_EXE:CCDILLUM
   COMPUTE/IMAGE {P2} = {P1}-{OUT_A}
   DELETE/IMAGE {OUT_A}
ELSE
   OUT_A = "{P2}"
   RUN STD_EXE:CCDILLUM
   COMPUTE/IMAGE {P2} = {P1}-{OUT_A}
ENDIF
!
IF M$EXIST("{P2}") .EQ. 1 THEN                            ! check fringe frame
   WRITE/DESCR {P2} MKFRINGE/C/1/10 MKFRINGE
   WRITE/DESCR {P2} {EXP_DESC}/C/1/20 {FR_TYP}                ! fringe exposure
ENDIF
RETURN 







