! @(#)make_illum.prg	19.1 (ESO-DMD) 02/25/03 14:16:03
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       make_illum.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Make illumination frame
!.USE:        @s mkillumination p1 p2
!.            where p1 is the reduced input sky frame
!.                  p2 in output illumination correction frame
!.VERSION     930806  RHW  Creation
!             980520  SW   IM_SEC will be used to get valid region for
!                          illumination correction frame creation!
!------------------------------------------------------------------------------
DEFINE/PARAM P1 ?      IMA "Enter the input frame:"
DEFINE/PARAM P2 ?      IMA "Enter the output illumination correction frame:"
!
! *** illumination operation already done
IF M$EXISTD(P1,"MKILLUM") .EQ. 1 THEN
   WRITE/OUT "{P1}: *** INFO: Illumination smoothing already done"
   RETURN
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Illumination correction is {P2}"
RETURN
ENDIF
!
! *** smoothing box in x and y
DEFINE/PARAMETER P3 {IL_XBOX(1)},{IL_XBOX(2)} N "Size of smoothing box in x"
DEFINE/PARAMETER P4 {IL_YBOX(1)},{IL_YBOX(2)} N "Size of smoothing box in y"
DEFINE/LOCAL XBOX/R/1/2 {P3}                                  ! x smooting box
DEFINE/LOCAL YBOX/R/1/2 {P4}                                  ! y smoothing box
!
! ** to clip or not to clip
DEFINE/PARAM P5 {IL_CLIP} C  "Enter clip option [{IL_CLIP}]:"
DEFINE/LOCAL CLIP/C/1/3 {P5}                                        ! clip flag
IF P5(1:1) .EQ. "y" THEN
   DEFINE/PARAM P6 {IL_SIGMA(1)},{IL_SIGMA(2)} N "Enter low and high sigma:"
   DEFINE/LOCAL SIGMA/R/1/2 {P6}                                 ! clip numbers
ENDIF
!
! *** here we finally do it !!!
!extract added by swolf@eso.org (20.5.98):
EXTRACT &e = {P1} {IM_SEC}			!extract image section
						!overscan areas would disturb!
						!(swolf@eso.org) (20.5.98)

IF P1 .NE. "{P2}" THEN	! name of output frame not the same as input frame
   COPY/II {P1} {P2}	! but shoul have same structure (swolf@eso.org)
ENDIF

IN_A  = "middumme.bdf"                          ! input frame
OUT_A = "middummi.bdf"                          ! output frame
RUN STD_EXE:CCDILLUM
!
IF M$EXIST("middummi.bdf") .EQ. 1 THEN		! check ill frame: output ok?
   INSERT/IMA &i {P2} 
   WRITE/DESCR {P2} MKILLUM/C/1/10 MKILLUM
   WRITE/DESCR {P2} {EXP_DESC}/C/1/20 {IL_TYP}                 ! illum exposure
ENDIF
RETURN
