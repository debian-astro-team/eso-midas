! @(#)ccdset.prg	19.1 (ES0-DMD) 02/25/03 14:16:02
! @(#)ccdset.prg	19.1 (ESO)02/25/03 14:16:02
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdset.prg
!.AUTHOR      Rein H. Warmels,  ESO - Garching
!.KEYWORDS    Session, Manager, Set
!.PURPOSE     Command SET/CCD  PAR=VALUE ... 
!.VERSION     1.0 02-SEP-1991 PB  Creation
!.VERSION     1.1 16-02-1993  RHW Addiotion for SET/CCD  
!-------------------------------------------------------
DEFINE/LOCAL NPAR/I/1/1    {PCOUNT(1)}
DEFINE/LOCAL LINE/C/1/132  " " ALL
COPY/KEYW     MID$LINE   LINE
!
INQUIRE/SESS {MID$CMND(11:14)} IN_A OUTPUTI
!
IF NPAR .LE. 0  THEN
   WRITE/OUT "*** FATAL: Command SET/CCD requires at least 1 argument"
   RETURN/EXIT
ENDIF
!
INPUTI(1) = NPAR
RUN APP_EXE:keyset
!
SET/FORMAT I1
DEFINE/LOCAL N/I/1/1 0
DO N = 1 {PCOUNT}
   IF P{N} .EQ. "IL_TYP" THEN
      IL_TYP = "{SK_TYP}"
   ENDIF
ENDDO
RETURN









