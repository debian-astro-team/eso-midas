! @(#)prep_fix.prg	19.1 (ES0-DMD) 02/25/03 14:16:05
! @(#)prep_fix.prg	19.1 (ESO)  02/25/03 14:16:05
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       prep_fix.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, bad pixel correction
!.PURPOSE     Do the bad pixel correction of the input frame
!.USE         The input file is corrected for bad pixel. The bad pixel
!.            are contained in a bad fixel file, which obviously depend
!.            on tescope and instrument combination. All ESO bad pixel
!.            file are assumed to be in the working directory or in the
!.            the subdirectory stdred/ccd/incl.
!.PARAMETERS  P1: input frame
!             P2: correction label string which will be checked by
!                 ccdcorrect.prg to do the desired actions.
!.NOTE        The procedure checks if the operation was already done
!.            by checking the descriptor CCDSTAT(6)
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for the bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for bad pixel correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930326  RHW  Creation
!.VERSION     971117  SW   correction labels have to be local -> new param.
!-------------------------------------------------------
DEFINE/PARAM P1 ? I     "Input frame for pixel correction [no default]:"
DEFINE/PARAM P2 {CCDCORS(1:20)}
!The correction label string CCDCORS which will be checked by CCDCORRECT.PRG
!has to be local, in order to always get the right link to the actual frame.
!In a global definition the correction labels may overwritten by other pre-
!pared frames which leads to unwished actions to previous frames (SW,97.11.17).
!
! *** Check if pixel fixing has already been applied
IF SC_FXPIX .NE. "yes" .OR. {{P1},CCDSTAT(3)} .EQ. 1 THEN
   WRITE/OUT "{P1}: Pixel correction not requested or was applied"
   RETURN {P2}
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Bad pixel table is {FX_TABLE}"
   RETURN {P2}
ENDIF
!
! *** check for the existence of the bad pixel table
IF M$EXIST("{FX_TABLE}") .EQ. 0 THEN                  ! look for bad pixel file
   WRITE/OUT "{P1} *** ERROR: Bad pixel table {FX_TABLE} not accessible"
   RETURN {P2}
ENDIF
!
! *** write the descriptor status and user info
WRITE/OUT "{P1}: Bad pixel correction; bad pixel table is {FX_TABLE}"
P2(3:3) = "P"
P2(11:11) = "Y"
!
RETURN {P2}
