! @(#)ccdfixpix.prg	19.1 (ESO-DMD) 02/25/03 14:15:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdpxcor.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, bad pixel correction
!.PURPOSE     Do the bad pixel correction of the input frame
!.USE         The input file is corrected for bad pixel. The bad pixel
!.            are contained in a bad fixel file, which obviously depend
!.            on telescope and instrument combination. All ESO bad pixel
!.            file are assumed to be in the working directory or in the
!.            the subdirectory stdred/ccd/incl.
!.NOTE        The procedure checks if the operation was already done
!.            by checking the descriptor CCDSTAT(6)
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for the bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for bad pixel correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930326  RHW  Creation
!-------------------------------------------------------
DEFINE/PARAM P1 ?          IMA   "Enter input frame: "
DEFINE/PARAM P2 ?          IMA   "Enter output frame: "
DEFINE/PARAM P3 {FX_TABLE} TBL   "Enter the bad pixel table:"
DEFINE/PARAM P4 {FX_METH}  C     "Option for pixel correction:"
!
! *** check if processing was already done
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/OUT "{P1}: *** WARNING: No check on CCD reduction status possible"
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ELSE
   IF {{P1},CCDSTAT(3)} .EQ. 1 THEN  
      WRITE/OUT "{P1}: *** INFO: Pixel correction was already applied"
      RETURN
   ENDIF
ENDIF
!
IF M$EXIST(FX_TABLE) .EQ. 0 THEN                 ! look for bad pixel file
   WRITE/OUT "{P1} *** ERROR: Bad pixel table {FX_TABLE} not accessible"
   RETURN
ENDIF
!
! *** go to pixel modifications
IF P4(1:1) .EQ. "A" THEN                       ! use modify/area command
   MODIFY/AREA   {P1},{P3} {P2} {FX_FPAR(1)} {FX_NOISE}

ELSEIF P4(1:1) .EQ. "C" THEN                 ! use modify/column command
   MODIFY/COLUMN {P1},{P3} {P2} V 

ELSEIF P4(1:1) .EQ. "R" THEN                    ! use modify/row command
   MODIFY/ROW {P1},{P3} {P2} V 

ELSE                                             ! use modify/pixels command
   MODIFY/PIXELS {P1},{P3} {P2} {FX_FACT} {FX_FPAR} ? {FX_NOISE}
ENDIF
!
! *** write the history and do statistics
WRITE/DESCR {P2} HISTORY/C/-1/80 "FIXPIX/CCD {P1} {P2} {P3}
WRITE/DESCR {P2} CCDSTAT/I/3/1 1                      ! image fixing done
IF VERBOSE(1:1) .EQ. "Y" THEN 
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FF                 ! full statistics
ELSE
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FX                ! short statistics
ENDIF
!
RETURN






