! @(#)ccdfringe.prg	19.1 (ESO-DMD) 02/25/03 %U
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       ccdfringe.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Do the fringe correction 
!.VERSION     920731  RHW  Creation
!.NOTE        The procedure checks if the operation was already done
!.            by checking the descriptor CCDSTAT(8)
!.            Other CCDSTAT descriptor values
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!-------------------------------------------------------
DEFINE/PARAM P1 ?                 IMA "Enter input frame: "
DEFINE/PARAM P2 ?                 IMA "Enter output frame: "
DEFINE/PARAM P3 {SC_FRFRM}        IMA "Enter fringe frame: "
DEFINE/PARAM P4 -999               N   "Enter fringe scaling factor:"
!
! *** check if already corrected
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/OUT "{P1}: *** WARNING: No check on CCD reduction status possible"
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ELSE
   IF {{P1},CCDSTAT(8)} .EQ. 1 THEN
      WRITE/OUT -
         "{P1}: *** WARNING: Fringe correction was applied; nothing done ..."
      RETURN
   ENDIF
ENDIF
!
! *** check to frame and its desciptors; exit on errors
IF M$EXIST("{P3}.bdf"} .EQ. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: Fringe correction {P3} doesn't exist ..."
   RETURN
ENDIF
IF FR_TYP .NE. "*" .AND. FR_TYP .NE. "?" THEN 
   IF M$EXISTD(P3,EXP_DESC) .EQ. 1 .AND. -
      "{{P3},{EXP_DESC}}" .NE. "{FR_TYP}" THEN
      WRITE/OUT -
      "{P3}: *** ERROR: Exposure descriptor {EXP_DESC} has wrong type"
      RETURN
   ENDIF
ENDIF
!
IF M$EXISTD(SC_FRFRM,"MKFRINGE") .EQ. 0 THEN
   WRITE/OUT "{SC_FRFRM}: MKFRINGE Fringe flag missing in descriptor"
   RETURN
ENDIF
IF M$EXISTD(P1,O_DESC) .EQ. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: Exposure descriptor {O_DESC} is missing"
   RETURN
ENDIF
IF M$EXISTD(P3,O_DESC) .EQ. 0 THEN
   WRITE/OUT "{P3}: *** ERROR: Exposure descriptor {O_DESC} is missing"
   RETURN
ENDIF
!
! *** get the desciptors contents
DEFINE/LOCAL OBSTIM/R/1/2 0.0,0.0
DEFINE/LOCAL FRSCALE/R/1/1/ 0.0
OBSTIM(1) = {{P1},{O_DESC}}  
OBSTIM(2) = {{P3},{O_DESC}}
IF OBSTIM(1) .LE. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: invalid exposure time "
   RETURN
ENDIF
IF OBSTIM(2) .LE. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: {P3} fringe frame has invalid exposure time"
   RETURN
ENDIF
!
IF M$EXISTD(P3,"FRINGESCALE") .EQ. 1 THEN
   FRSCALE = {{P3},{FRINGESCALE}}
ELSE
   FRSCALE = 1.0
ENDIF
IF P4 .NE. -999 THEN
   FRSCALE = {P4}
ENDIF
CCDFRING = FRSCALE * OBSTIM(1)/OBSTIM(2)
!
COMPUTE/IMAGE {P2} = {P1} - CCDFRING*{P3}
WRITE/DESCR {P2} CCDSTAT/I/8/1 1                      ! dark correction done
WRITE/DESCR {P2} HISTORY/C/-1/80 "Processed by @s ccdfrcor {P1} {P2} {P3}
IF VERBOSE(1:1) .EQ. "Y" THEN
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FF                    ! full statistics
ELSE 
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FX                    ! short statistics
ENDIF
!
RETURN



