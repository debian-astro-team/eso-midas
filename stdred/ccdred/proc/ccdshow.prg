!@(#)ccdshow.prg	19.1 (ESO-DMD) 02/25/03 14:16:02
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       ccdshow.prg
!.AUTHOR      Rein H. Warmels,  ESO - Garching
!.KEYWORDS    Direct imaging, CCD
!.PURPOSE     Display CCD session parameters
!.VERSION     920416  RHW Creation 
!
!-------------------------------------------------------
DEFINE/PARAM  P1 GENERAL C
IF P8(1:1) .EQ. "H" GOTO HELP
!
BRANCH P1(1:2) GE,BS,DK,FF,SK,OT,MO -
                           GENERAL,BIAS,DARK,FLAT,SKY,OTHER,MOSAIC
BRANCH P1(1:2) OV,IL,FR,FX OVERSC,ILLUM,FRINGE,FIX
BRANCH P1(1:2) HE,SC       HELP,SCI
WRITE/OUT "*** Sorry, unknown option (consult the help for legal ones)"
RETURN
!
SET/FORMAT I1  F12.5  
ECHO/OFF
!
SHOW/SESS  "****************  CCD Parameters  *******************"
SHOW/SESS  "   "
!
!
GENERAL:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- General Control Setup ----"
SHOW/SESS  "Object         : IDENT  ={IDENT}; [descr={.IDENT}]"
SHOW/SESS  "Observer       : OBSER  ={OBSER}; [descr={.OBSER}]"
SHOW/SESS  "Telescope      : TELES  ={TELES}; [descr={.TELES}]"
SHOW/SESS  "Instrument     : INSTR  ={INSTR}; [descr={.INSTR}]"
SHOW/SESS  "Detector       : DETEC  ={DETEC}; [descr={.DETEC}]" 
SHOW/SESS  "                 DIRECT ={DIRECT}; [descr={.DIRECT}]"
SHOW/SESS  "                 BINNING={BINNING(1)}, {BINNING(2)}; [descr={.BINNING}]"
SHOW/SESS  "                 READMOD={READMOD}; [descr={.READMOD}]"
SHOW/SESS  "                 READON ={READON}; [descr={.READON}]"
SHOW/SESS  "                 AD_VAL ={AD_VAL}; [descr={.AD_VAL}]"
SHOW/SESS  "Descriptors    : Exp. type={EXP_DESC}"
SHOW/SESS  "                 Exp. time={O_DESC}"
SHOW/SESS  "Image section  : IM_SEC={IM_SEC}"
SHOW/SESS  "Bad pixel file : FX_TABLE={FX_TABLE}"
SHOW/SESS  "Reduction mode : REDUCT={REDUCT}"
SHOW/SESS  "Input specs    : CCD_IN={CCD_IN}"
SHOW/SESS  "Exp. columns:    SC_COL=:{SC_COL}"
SHOW/SESS  "                 BS_COL=:{BS_COL}   DK_COL=:{DK_COL}"
SHOW/SESS  "                 FF_COL=:{FF_COL}   SK_COL=:{SK_COL}"
RETURN
!
!
BIAS:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- Bias Combining Setup ----"
SHOW/SESS  "Input specific.: CCD_IN={CCD_IN}"
SHOW/SESS  "Exposure type  : BS_TYP={BS_TYP}"
SHOW/SESS  "Sigma frame    : BS_SIG={BS_SIG}"
SHOW/SESS  "Method         : BS_MET={BS_MET}"
SHOW/SESS  "Delete         : BS_DEL={BS_DEL}"
SHOW/SESS  "Statistics     : BS_STA={BS_STA}"
SHOW/SESS  "M/M/M scaling  : BS_SCA={BS_SCA}"
SHOW/SESS  "M/M/M offset   : BS_OFF={BS_OFF}"
SHOW/SESS  "M/M/M section  : BS_SEC={BS_SEC}"
SHOW/SESS  "Exp. scaling   : BS_EXP={BS_EXP}"
SHOW/SESS  "Weighting      : BS_WEI={BS_WEI}"
SHOW/SESS  "Valid pix range: BS_RAN={BS_RAN(1)},{BS_RAN(2)}"
SHOW/SESS  "Low/High reject: BS_CLP={BS_CLP(1)},{BS_CLP(2)}"
SHOW/SESS  "Blank value    : BS_NUL={BS_NUL}"
RETURN
!
!
DARK:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- Dark Combining Setup ----"
SHOW/SESS  "Input specific.: CCD_IN={CCD_IN}"
SHOW/SESS  "Exposure type  : DK_TYP={DK_TYP}"
SHOW/SESS  "Sigma frame    : DK_SIG={DK_SIG}"
SHOW/SESS  "Method         : DK_MET={DK_MET}"
SHOW/SESS  "Delete         : DK_DEL={DK_DEL}"
SHOW/SESS  "Statistics     : DK_STA={DK_STA}"
SHOW/SESS  "M/M/M scaling  : DK_SCA={DK_SCA}"
SHOW/SESS  "M/M/M offset   : DK_OFF={DK_OFF}"
SHOW/SESS  "M/M/M section  : DK_SEC={DK_SEC}"
SHOW/SESS  "Exp. scaling   : DK_EXP={DK_EXP}"
SHOW/SESS  "Weighting      : DK_WEI={DK_WEI}"
SHOW/SESS  "Valid pix range: DK_RAN={DK_RAN(1)},{DK_RAN(2)}"
SHOW/SESS  "Low/High reject: DK_CLP={DK_CLP(1)},{DK_CLP(2)}"
SHOW/SESS  "Blank value    : DK_NUL={DK_NUL}"
RETURN
!
!
FLAT:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- Flat Field Combining Setup ----"
SHOW/SESS  "Input specific.: CCD_IN={CCD_IN}"
SHOW/SESS  "Exposure type  : FF_TYP={FF_TYP}"
SHOW/SESS  "Sigma frame    : FF_SIG={FF_SIG}"
SHOW/SESS  "Method         : FF_MET={FF_MET}"
SHOW/SESS  "Delete         : FF_DEL={FF_DEL}"
SHOW/SESS  "Statistics     : FF_STA={FF_STA}"
SHOW/SESS  "M/M/M scaling  : FF_SCA={FF_SCA}"
SHOW/SESS  "M/M/M offset   : FF_OFF={FF_OFF}"
SHOW/SESS  "M/M/M section  : FF_SEC={FF_SEC}"
SHOW/SESS  "Exp. scaling   : FF_EXP={FF_EXP}"
SHOW/SESS  "Weighting      : FF_WEI={FF_WEI}"
SHOW/SESS  "Valid pix range: FF_RAN={FF_RAN(1)},{FF_RAN(2)}"
SHOW/SESS  "Low/High reject: FF_CLP={FF_CLP(1)},{FF_CLP(2)}"
SHOW/SESS  "Blank value    : FF_NUL={FF_NUL}"
RETURN
!
!
SKY:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- Sky Flat Combining Setup ----"
SHOW/SESS  "Input specific.: CCD_IN={CCD_IN}"
SHOW/SESS  "Exposure type  : SK_TYP={SK_TYP}"
SHOW/SESS  "Sigma frame    : SK_SIG={SK_SIG}"
SHOW/SESS  "Method         : SK_MET={SK_MET}"
SHOW/SESS  "Delete         : SK_DEL={SK_DEL}"
SHOW/SESS  "Statistics     : SK_STA={SK_STA}"
SHOW/SESS  "M/M/M scaling  : SK_SCA={SK_SCA}"
SHOW/SESS  "M/M/M offset   : SK_OFF={SK_OFF}"
SHOW/SESS  "M/M/M section  : SK_SEC={SK_SEC}"
SHOW/SESS  "Exp. scaling   : SK_EXP={SK_EXP}"
SHOW/SESS  "Weighting      : SK_WEI={SK_WEI}"
SHOW/SESS  "Valid pix range: SK_RAN={SK_RAN(1)},{SK_RAN(2)}"
SHOW/SESS  "Low/High reject: SK_CLP={SK_CLP(1)},{SK_CLP(2)}"
SHOW/SESS  "Blank value    : SK_NUL={SK_NUL}"
RETURN
!
!
OTHER:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- Other Frame Combining Setup ----"
SHOW/SESS  "Input catalogue: CCD_IN={CCD_IN}"
SHOW/SESS  "Exposure type  : OT_TYP={OT_TYP}"
SHOW/SESS  "Sigma frame    : OT_SIG={OT_SIG}"
SHOW/SESS  "Method         : OT_MET={OT_MET}"
SHOW/SESS  "Delete         : OT_DEL={OT_DEL}"
SHOW/SESS  "Statistics     : OT_STA={OT_STA}"
SHOW/SESS  "M/M/M scaling  : OT_SCA={OT_SCA}"
SHOW/SESS  "M/M/M offset   : OT_OFF={OT_OFF}"
SHOW/SESS  "M/M/M section  : OT_SEC={OT_SEC}"
SHOW/SESS  "Exp. scaling   : OT_EXP={OT_EXP}"
SHOW/SESS  "Weighting      : OT_WEI={OT_WEI}"
SHOW/SESS  "Valid pix range: OT_RAN={OT_RAN(1)},{OT_RAN(2)}"
SHOW/SESS  "Low/High reject: OT_CLP={OT_CLP(1)},{OT_CLP(2)}"
SHOW/SESS  "Blank value    : OT_NUL={OT_NUL}"
RETURN
!
!
MOSAIC:
SESSLINE = 0
SHOW/SESS  "Mean section   : MO_SEC={MO_SEC}"
SHOW/SESS  "Subtract median: MO_SUBT={MO_SUBT}"
SHOW/SESS  "Mosaic origin  : MO_CORN={MO_CORN}"
SHOW/SESS  "Direction      : MO_DIREC={MO_DIREC}"
SHOW/SESS  "Raster pattern : MO_RAST={MO_RAST}"
SHOW/SESS  "Overlap        : MO_OVER={MO_OVER(1)},{MO_OVER(2)}"
SHOW/SESS  "Interpolant    : MO_INTER={MO_INTER}"
SHOW/SESS  "Min. # pixels  : MO_MNPX={MO_MNPX}"
SHOW/SESS  "Trim col/rows  : MO_TRIM={MO_TRIM(1)},{MO_TRIM(2)},{MO_TRIM(3)},{MO_TRIM(4)}"
SHOW/SESS  "Blank value    : MO_NUL={MO_NUL}"
RETURN
!
!
OVERSC:
SESSLINE = 0                                    ! Inilialize the line counter
SHOW/SESS  "Overscan sect. : OV_SEC={OV_SEC}"
SHOW/SESS  "Interactive fit: OV_IMODE={OV_IMODE}"
SHOW/SESS  "Fitting funct. : OV_FUNCT={OV_FUNCT}"
SHOW/SESS  "Order of fit   : OV_ORDER={OV_ORDER}"
SHOW/SESS  "# comb. points : OV_AVER={OV_AVER}"
SHOW/SESS  "Number of iter : OV_ITER={OV_ITER}"
SHOW/SESS  "Low/high rej.  : OV_REJEC={OV_REJEC}"
RETURN
!
!
FIX:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- Pixel Correction Parameters ----"
SHOW/SESS  "   "
SHOW/SESS "Bad pixel file  : FX_TABLE={FX_TABLE}"
SHOW/SESS "Correct. method : FX_METH={FX_METH}"
SHOW/SESS "Expansion facts : FX_FACT={FX_FACT}"
SHOW/SESS "Fitting params. : FX_FPAR={FX_FPAR}"
SHOW/SESS "Background noise: FX_NOISE={FX_NOISE}"
RETURN
!
!
ILLUM:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- Illumination Setup ----"
SHOW/SESS  "Input frame    : CCD_IN={CCD_IN}"
SHOW/SESS  "Exposure type  : IL_TYP={IL_TYP}"
SHOW/SESS  "Smooth box in x: IL_XBOX={IL_XBOX(1)},{IL_XBOX(2)}"
SHOW/SESS  "Smooth box in y: IL_YBOX={IL_YBOX(1)},{IL_YBOX(2)}"
SHOW/SESS  "Clip pixels:   : IL_CLIP={IL_CLIP}"
SHOW/SESS  "Low/High clip  : IL_SIGMA={IL_SIGMA(1)},{IL_SIGMA(2)}"
RETURN
!
!
FRINGE:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- Fringing Correction Setup ----"
SHOW/SESS  "Input frame    : CCD_IN={CCD_IN}"
SHOW/SESS  "Exposure type  : FR_TYP={FR_TYP}"
SHOW/SESS  "Smooth box in x: IL_XBOX={IL_XBOX(1)},{IL_XBOX(2)}"
SHOW/SESS  "Smooth box in y: IL_YBOX={IL_YBOX(1)},{IL_YBOX(2)}"
SHOW/SESS  "Clip pixels:   : IL_CLIP={IL_CLIP}"
SHOW/SESS  "Low/High clip  : IL_SIGMA={IL_SIGMA(1)},{IL_SIGMA(2)}"
RETURN
!
!
SCI:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS  "--- Science Frame Reduction Setup ----"
SHOW/SESS  "Input frm/tbl  : CCD_IN={CCD_IN}"
SHOW/SESS  "Exposure type  : SC_TYP={SC_TYP}"
SHOW/SESS  "List steps only: SC_PROC={SC_PROC}"
SHOW/SESS  "   "
SHOW/SESS  "Oversc. corr.  : SC_SCAN={SC_SCAN}"
SHOW/SESS  "Trim the frame : SC_TRIM={SC_TRIM}"
SHOW/SESS  "Fix bad pixel  : SC_FXPIX={SC_FXPIX}"
SHOW/SESS  "Bias correct.  : SC_BSCOR={SC_BSCOR}"
SHOW/SESS  "Dark correct.  : SC_DKCOR={SC_DKCOR}"
SHOW/SESS  "Flat correct.  : SC_FFCOR={SC_FFCOR}"
SHOW/SESS  "Illum correct. : SC_ILCOR={SC_ILCOR}"
SHOW/SESS  "Fringe correct.: SC_FRCOR={SC_FRCOR}"
SHOW/SESS  "   "
SHOW/SESS  "Bias frame     : SC_BSFRM={SC_BSFRM}"
SHOW/SESS  "Dark frame     : SC_DKFRM={SC_DKFRM}"
SHOW/SESS  "Flat frame     : SC_FFFRM={SC_FFFRM}"
SHOW/SESS  "Illum frame    : SC_ILFRM={SC_ILFRM}"
SHOW/SESS  "Fringe frame   : SC_FRFRM={SC_FRFRM}"
SHOW/SESS  " "
RETURN
!
!
HELP:
SESSLINE = 0                                     ! Initialize the line counter
SHOW/SESS "     ***** CCD standard reduction package - Main Help *****"
SHOW/SESS " *** Please report errors, bugs or problems to Rein Warmels ***"
SHOW/SESS " "
SHOW/SESS " This is the list of high level commands in the CCD reduction"
SHOW/SESS " package. They are listed in a number groups covering CCD keyword"
SHOW/SESS " handling, automatic reduction, manual reduction, special "
SHOW/SESS " calibration frames, and miscellaneous"
SHOW/SESS " Most commands read the input parameters from the keyword settings."
SHOW/SESS " In some cases, a keyword setting can be overruled by specifying "
SHOW/SESS " the parameter on the command line."
SHOW/SESS " Default parameters are indicated by square brackets (`[]')."
SHOW/SESS " More help is available for each command by using HELP <command>."
SHOW/SESS " "
SHOW/SESS " The following commands are available:"
SHOW/SESS " INITIAL/CCD [name]"
SHOW/SESS "    Initialize the ccd package"
SHOW/SESS " LOAD/CCD [instr]"
SHOW/SESS "    Load telescope/instrument specific default into CCD context"
SHOW/SESS " SAVE/CCD name"
SHOW/SESS "    Save the keyword settings "
SHOW/SESS " SHOW/CCD [subject]"
SHOW/SESS "    Show the complete setup of the CCD package"
SHOW/SESS " SET/CCD keyw=value [...]"
SHOW/SESS "    Set the CCD keywords"
SHOW/SESS " HELP/CCD [keyword]"
SHOW/SESS "   provide help information for a CCD keyword"
SHOW/SESS " "
!
SHOW/SESS " MKREDTBL/CCD ass_tab red_tab"
SHOW/SESS "    Create a CCD input table for pipeline CCD reduction"
SHOW/SESS " REDUCE/CCD [in_spec] [out_frm] [proc_opt]"
SHOW/SESS "    DO (partical) calibration of one or more science frames"
SHOW/SESS " COMBINE/CCD exp [in_spec] [out_frm]
SHOW/SESS "    Combine CCD frames using table, catalogue, file list input"
SHOW/SESS " "
!
SHOW/SESS " OVERSCAN/CCD [in_fram] [out_fram] CCD_area"
SHOW/SESS "    Fit the bias offset in the overscan region and correct"
SHOW/SESS " FIXPIX/CCD [in_fram] [out_fram] [fix_table] [fix_meth]"
SHOW/SESS "    Correct bad pixels using a pixel file"
SHOW/SESS " TRIM/CCD [in_fram] [out_fram] [im_sec] [del_flg]"
SHOW/SESS "    Extract the useful data fro the ccd frame"
SHOW/SESS " BIAS/CCD [in_fram] [out_fram] [bs_fram]"
SHOW/SESS "    Correct the input frame for the additive bias offset"
SHOW/SESS " DARK/CCD [in_fram] [out_fram] [dk_fram]"
SHOW/SESS "    Correct the input frame for the additive dark offset"
SHOW/SESS " FLAT/CCD [in_fram] [out_fram] [ff_fram]"
SHOW/SESS "    Do a flat fielding of the input frame"
SHOW/SESS " ILLUMINATE/CCD [in_spec] [out_frm] [il_fram]"
SHOW/SESS "    Do the illumination correction of the input frame"
SHOW/SESS " FRINGE/CCD [in_spec] [out_frm] [fr_fram]"
SHOW/SESS "    Do a fringe correction of the input frame"
SHOW/SESS " "
!
SHOW/SESS " ILLCOR/CCD [in_spec] [out_frm] [xboxmn,xboxmx] [yboxmn,yboxmx]"
SHOW/SESS "            [clip] [lowsig,higsig]"
SHOW/SESS "    Create the flat field calibration frames"
SHOW/SESS " ILLFLAT/CCD [in_spec] [out_frm] [xboxmn,xboxmx] [yboxmn,yboxmx]"
SHOW/SESS "             [clip] [lowsig,higsig]"
SHOW/SESS "    Create illumination corrected flat field frame(s)"
SHOW/SESS " SKYCOR/CCD [in_spec] [out_frm] [xboxmn,xboxmx] [yboxmn,yboxmx]"
SHOW/SESS "            [clip] [lowsig,higsig]"
SHOW/SESS "    Create sky illumination correction flat field(s)"
SHOW/SESS " SKYFLAT/CCD [in_spec] [out_frm] [xboxmn,xboxmx] [yboxmn,yboxmx]"
SHOW/SESS "             [clip] [lowsig,higsig]"
SHOW/SESS "    Create illumination correction sky flat(s)"
SHOW/SESS " FRCOR/CCD [in_spec] [out_frm] [xboxmn,xboxmx] [yboxmn,yboxmx]"
SHOW/SESS "            [clip] [lowsig,higsig]"
SHOW/SESS "    Create fringe correction frame(s)"
SHOW/SESS " "
!
SHOW/SESS " CREATE/MOSAIC in_cat out_frm out_tab nx_sub,ny_sub [not1,not2,...]"
SHOW/SESS "              [nocol,norow]"
SHOW/SESS "    Mosaic a set of (infrared) ccd frames"
SHOW/SESS " ALIGN/MOSAIC in_frm in_tab out_frm method,data [xref,yref] "
SHOW/SESS "              [xoff,yoff] [x_size,y_size]"
SHOW/SESS "    Align the elements of the mosaiced frame"
SHOW/SESS " MATCH/MOSAIC in_frm in_tab out_frm method,data [match] [xref,yref]"
SHOW/SESS "              [xoff,yoff] [x_size,y_size]"
SHOW/SESS "    Align and match the elements of the mosaiced frame"
SHOW/SESS " SHIFT/MOSAIC out_tab [curs_opt] [csx,csy] [clear_opt]"
SHOW/SESS "    Get x and y shifts of the subraster in the mosaic frame"
SHOW/SESS " "
