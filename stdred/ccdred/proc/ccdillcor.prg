! @(#)ccdillcor.prg	19.1 (ESO-DMD)  02/25/03 14:16:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdillcor.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, SKYCOR/CCD
!.PURPOSE     Make illumination illumination correction frame(s)
!.USE         ILLCOR/CCD p1 p2
!.            where p1 = reduction table or single flat field
!.                  p2 = output frame (only in case of single frame input)
!.                  p3 = xboxmin, xboxmax smoothing box
!.                  p4 = yboxmin, yboxmax smoothing box
!.                  p5 = clipping option
!.                  p6 = low and high clipping factor 
!.ALGORYTHM:  The procedure check if the flat field(s) are available and 
!.            continues with the pipeline reduction of each flat if that is 
!.            required. Using the reduced field a illumination frame is created
!.            by a box smoothing algorithm.
!.NOTE        The individual procedures check if one operation was already done
!.            by checking the descriptor CCDSTAT. The CCDSTAT descriptor values
!.            represent the following status:
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION:    920731  RHW  Creation
!.VERSION:    930728  RHW  Improve layout and structure
!-------------------------------------------------------
DEFINE/PARAM P1 {CCD_IN}  C "Enter input specification [no default]:"
DEFINE/PARAM P5 {IL_CLIP} C "Clean frame form object first [IL_CLIP]:"
!
! *** keyword saving
DEFINE/LOCAL CCDIN/C/1/60   " "
DEFINE/LOCAL CCDOUT/C/1/60  " "
DEFINE/LOCAL SCFRM/C/1/60   " "
DEFINE/LOCAL BSFRM/C/1/60   "{SC_BSFRM}"
DEFINE/LOCAL DKFRM/C/1/60   "{SC_DKFRM}"
!
DEFINE/LOCAL NAME/C/1/60    " "
DEFINE/LOCAL CNAME/C/1/10   " "
DEFINE/LOCAL IC/I/1/1       0
DEFINE/LOCAL IR/I/1/1       0
DEFINE/LOCAL LL/I/1/1       0
DEFINE/LOCAL NRR/I/1/1      0
DEFINE/LOCAL NCR/I/1/1      0
DEFINE/LOCAL NCA/I/1/1      0
DEFINE/LOCAL NRA/I/1/1      0
DEFINE/LOCAL IRA/I/1/1      0
DEFINE/LOCAL IDOT/I/1/1     0
DEFINE/LOCAL TBL/I/1/1      0
!
DEFINE/PARAM P3 {IL_XBOX(1)},{IL_XBOX(2)} N
DEFINE/PARAM P4 {IL_YBOX(1)},{IL_YBOX(2)} N
DEFINE/PARAM P5 {IL_CLIP} C
!
IF P5(1:1) .EQ. "Y" THEN
   DEFINE/PARAM P6 {IL_SIGMA(1)},{IL_SIGMA(2)}
ENDIF
!
! *** first check for single frame of input table
TBL = M$INDEX(P1,".tbl")                                      ! search for .tbl
IF TBL .EQ. 0 THEN
   CCDIN = "{P1}"
   DEFINE/PARAM P2 {CCD_OUT} IMA "Enter the output illumination frame:"
   CCDOUT = "{P2}"
!
! *** the following part in for reduction table input  
ELSE
   IF M$EXIST(P1) .EQ. 0 THEN                            ! check exsistence
      WRITE/OUT "*** FATAL: Missing CCD reduction table {P1}"
      RETURN
   ELSE
      COPY/TABLE {P1} &r                              ! copy only selected part
      SET/FORMAT I3
      NCA = {&r.tbl,TBLCONTR(3)}
      NRA = {&r.tbl,TBLCONTR(4)}                         ! check number of rows
      IF NRA .EQ. 0 THEN
         GOTO FINISH
      ENDIF
   ENDIF
!
! *** collect the input/output frame and calibration frames
   IRA = 0
   WRITE/OUT "Creating flat illum. correction frames in table {P1}"
   NXTFRM:
   IRA = IRA + 1
   CCDIN  = "{&r,:{FF_COL},@{IRA}}"                      ! get input image name
   LL     = M$INDEX(CCDIN,"_ff")-1                          ! science image
   CCDOUT = "{CCDIN(1:{LL})}_il"
!
   IF M$EXIST(CCDIN) .EQ. 0 THEN                  ! checking the flat frame
      WRITE/OUT "*** INFO Flat field {CCDIN} to be created"
      SCFRM  = "{CCDIN(1:{LL})}"                               ! master science
      COMBINE/CCD FF {SCFRM} {CCDIN}                                  ! combine
   ENDIF
!
   IF SC_BSCOR(1:1) .EQ. "y" THEN                        ! bias correc. wanted?
      SC_BSFRM = "{&r,:{BS_COL},@{IRA}}"
   ENDIF
!
   IF SC_DKCOR(1:1) .EQ. "y" THEN                        ! dark correc. wanted?
      SC_DKFRM = "{&r,:{DK_COL},@{IRA}}"
   ENDIF
ENDIF
!
! *** remove the .bdf extension
IDOT = M$INDEX(CCDIN,".bdf") - 1
IF IDOT .GT. 0 THEN 
   CCDIN = "{CCDIN(1:{IDOT})}"
ELSE
   CCDIN = "{CCDIN}"
ENDIF
IF M$EXIST("{CCDIN}.bdf") .EQ. 0 THEN                        ! check exsistence
   WRITE/OUT "*** FATAL: Missing input frame {CCDIN}"
   GOTO FINISH
ENDIF
!
IDOT = M$INDEX(CCDOUT,".bdf") - 1
IF IDOT .GT. 0 THEN 
   CCDOUT = "{CCDOUT(1:{IDOT})}"
ELSE
   CCDOUT = "{CCDOUT}"
ENDIF
!
@s ccdredfrm {CCDIN} {CCDIN}                                ! reduce flat field
@s make_illum {CCDIN} {CCDOUT} {P3} {P4} {P5} {P6}        ! create illum. frame
IF M$EXIST("{CCDOUT}.bdf") .EQ. 0 THEN                           ! is it there?
   WRITE/OUT "*** ERROR: Failed to make illumination correction {CCDOUT}"
   GOTO FINISH
ELSE
   WRITE/DESCR {CCDOUT} HISTORY/C/-1/80 "ILLCOR/CCD {P1} {P2}
ENDIF
!
! *** finish up
FINISH:
IF TBL .EQ. 0 THEN                                         ! single frame input
   WRITE/OUT "{CCDOUT}: Illumination frame from {CCDIN} ready"
ELSE                                                              ! table input
   IF IRA .LT. {NRA} THEN
      GOTO NXTFRM
   ELSE 
      WRITE/OUT " "
      IF NRA .EQ. 0 THEN
         WRITE/OUT "{P1}: *** WARNING: Selection in reduction table is empty "
      ELSE
         WRITE/OUT "{P1}: Illumination frames ready"
      ENDIF
      DELETE/TABLE &r NO
   ENDIF
ENDIF
!
SC_BSFRM = "{BSFRM}"                                  ! restore keyword setting
SC_DKFRM = "{DKFRM}"
RETURN






