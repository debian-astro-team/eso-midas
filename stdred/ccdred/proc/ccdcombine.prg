! @(#)ccdcombine.prg	19.1 (ESO-DMD) 02/25/03 14:15:59
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ccdcombine.prg
!.PURPOSE:        create master calibration frame(s)
!.AUTHOR:         Rein H. Warmels
!.USE:            @s ccdcombine
!.                where p1 = combine options (all available)
!.                      p2 = association table or catalogue
!.                      p3 = output frame (only if input is catalogue)
!.                      p4 = merge option
!.USE:            For each selected science frame in the table the command 
!                 collects all calibration frames and combined them
!.                In case the input is a catalogue all frame in the catalogue
!.                will be combined
!.VERSION:        930406 RHW creation
!.VERSION:        930725 RHW Now on column names
!.VERSION:        950110 RHW Revision; based on new DO ass. table; faster
!.                           Single file input not possible anymore
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAM P1 ?          C  "Enter combine option (BS, DK, FF, SK, or OT):"
DEFINE/PARAM P2 {CCD_IN}   C  "Enter input specification [CCD_IN]:"
DEFINE/PARAM P4 "M"        C  "Enter merge option [M]:"
!
DEFINE/LOCAL ASSCOL/C/1/60  " "
DEFINE/LOCAL ACTION/C/1/2 " " 
!
DEFINE/LOCAL TBL/I/1/1    0
DEFINE/LOCAL NCA/I/1/1    0
DEFINE/LOCAL ICA/I/1/1    0
!
! *** check the input
IF M$INDEX(P2,".cat") .GT. 1  THEN                      ! catalogue input
   DEFINE/PARAMETER P3  ?  C  "Enter combined output frame:"
   TBL = 0
   IF M$EXIST(P2) .EQ. 0  THEN             ! check if input is present
      WRITE/OUT -
         "*** FATAL: Input catalogue is not present; check your input"
      RETURN
   ENDIF
!
ELSEIF  M$INDEX(P2,".tbl") .GT. 1 THEN
   IF M$EXIST(P2) .EQ. 0  THEN             ! check if input is present
      WRITE/OUT -
         "*** FATAL: Input association table is not present; check your input"
      RETURN
   ENDIF

ELSE
   DEFINE/PARAMETER P3  ?  C  "Enter combined output frame:"
   TBL = 0
ENDIF
!
! *** CHECK THE COMBINING OPTION ***
!
IF P1 .EQ. "BS" THEN                                 ! check combining option
   WRITE/KEYW EXP/C/1/2 "BS"
   ASSCOL   = BS_COL
ELSE IF P1 .EQ. "DK" THEN
   WRITE/KEYW EXP/C/1/2 "DK"
   ASSCOL   = DK_COL
ELSE IF P1 .EQ. "FF" THEN
   WRITE/KEYW EXP/C/1/2 "FF"
   ASSCOL   = FF_COL
ELSE IF P1 .EQ. "SK" THEN
   WRITE/KEYW EXP/C/1/2 "SK"
   ASSCOL   = SK_COL

ELSE 
   IF TBL .EQ. 0 THEN                          ! for catalogue input allow OT
      IF P1 .EQ. "OT" THEN
         WRITE/KEYW EXP/C/1/2 "OT"
      ELSE                                            ! comb option incorrect
         WRITE/OUT -
            "*** FATAL <MKCAL>: Unknown combining option {P1}, try again ..."
         RETURN
      ENDIF
   ELSE                                                         ! table input
      WRITE/OUT -
         "{P1}: *** FATAL: Unknown combine option (use BS, DK, FF, or SK)"
      RETURN
   ENDIF
ENDIF
!
!*** All done; now copy combining keywords to local ones
DEFINE/LOCAL INCAT/C/1/60     {P2}             ! input specification
DEFINE/LOCAL OUTFRM/C/1/60    {P3}             ! output frame
DEFINE/LOCAL SIGMA/C/1/5      {{EXP}_SIG}      ! generate an sigma frame
DEFINE/LOCAL METHOD/C/1/20    {{EXP}_MET}      ! averaging method
DEFINE/LOCAL EXPTYP/C/1/20    {{EXP}_TYP}      ! which exposure type allowed
DEFINE/LOCAL DELETE/C/1/3     {{EXP}_DEL}      ! delete input cat + frames
DEFINE/LOCAL EXPOSU/C/1/3     {{EXP}_EXP}      ! scale by exposure time
DEFINE/LOCAL STATIST/C/1/6    {{EXP}_STA}      ! statistics mean/median/mode
DEFINE/LOCAL SCALE/C/1/3      {{EXP}_SCA}      ! scale by mmm
DEFINE/LOCAL OFFSET/C/1/3     {{EXP}_OFF}      ! add offset from mmm
DEFINE/LOCAL WEIGHT/C/1/3     {{EXP}_WEI}      ! use the weighted average
DEFINE/LOCAL SECTION/C/1/60   {{EXP}_SEC}      ! frame section for mode
DEFINE/LOCAL RANGE/R/1/2      {{EXP}_RAN(1)},{{EXP}_RAN(2)} ! pixel range
DEFINE/LOCAL CLIP/R/1/2       {{EXP}_CLP(1)},{{EXP}_CLP(2)} ! sig clip factor
DEFINE/LOCAL BLANK/C/1/20     {{EXP}_NUL}      ! null value
WRITE/KEYW   ACTION/C/1/2     {P4}
WRITE/KEYW   INPUTR/R/1/1     256
!
! *** NOW DO THE COMBINING ***
IF TBL .EQ. 1 THEN                                   ! input is an ass. table
   SET/FORMAT I3
   IF M$EXISTC(P2,ASSCOL) .EQ. -1 THEN
      WRITE/OUT "*** WARNING: Cannot find column {ASSCOL} in association table"
      WRITE/OUT "             Check column names in association table"
   ELSE
      RUN STD_EXE:CCDCOMB
   ENDIF

ELSE                                                   ! input is a catalogue
   RUN STD_EXE:CCDCOMB
   IF {EXP}_DEL .EQ. "yes" THEN
      WRITE/OUT "*** INFO: Deleting frames  entries in input catalogue"
!     DELETE/ICAT {CCD_IN} CONF ?
   ENDIF
ENDIF
!
RETURN


