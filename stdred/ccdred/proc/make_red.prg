! @(#)make_red.prg	19.1 (ES0-DMD) 02/25/03 14:16:04
! @(#)make_red.prg	19.1 (ESO-SDAG) 02/25/03 14:16:04
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENTIFICATION: make_red.prg
!.PURPOSE:        Create the reduction table containing all science frames
!.                to be reduced and the name of the master calibration frames
!.AUTHOR:         Rein H. Warmels
!.USE:            @s make_red
!.                where p1 = output table containing all columns for the 
!.                           science and master calibration frames as defined 
!.                           by the keyword setting SC_COL, "exp"_COL.
!.VERSION:        920410 RHW creation
!.VERSION:        930516 RHW Minor cosmetic changes
!.VERSION:        930516 RHW Improve speed and structure
!.VERSION:        950114 RHW New version based on the new DO output table
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAM P1 ?    TBL    "Name of output CCD Table:"
!
crea/tab {P1} NULL
!
if sc_col(1:1) .ne. "?" .and. sc_col(1:1) .ne. " " then
   crea/col {P1} :{SC_COL} A20 C*60
else
   write/out " *** ERROR: Science column ill defined; no column created"
endif

if bs_col(1:1) .ne. "?" .and. bs_col(1:1) .ne. " " then
   crea/col {P1} :{BS_COL} A20 C*60
else
   write/out " *** ERROR: Bias column ill defined; no column created"
endif

if dk_col(1:1) .ne. "?" .and. dk_col(1:1) .ne. " " then
   crea/col {P1} :{DK_COL} A20 C*60
else
   write/out " *** ERROR: Dark column ill defined; no column created"
endif

if ff_col(1:1) .ne. "?" .and. ff_col(1:1) .ne. " " then 
   crea/col {P1} :{FF_COL} A20 C*60
else
   write/out " *** ERROR: Flat column ill defined; no column created"
endif

if sk_col(1:1) .ne. "?" .and. sk_col(1:1) .ne. " " then
   crea/col {P1} :{SK_COL} A20 C*60
else
   write/out " *** ERROR: Sky column ill defined; no column created"
endif
!
RETURN























