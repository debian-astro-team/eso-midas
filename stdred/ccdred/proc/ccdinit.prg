! @(#)ccdinit.prg	19.1 (ES0-DMD) 02/25/03 14:16:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       ccdinit.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Create and set keywords of the context ccd
!.VERSION     920916  RHW  Creation
!-------------------------------------------------------
! 
IF P1(1:1) .NE. "?" GOTO INIT
!
!Begin Session List
!GEneral
! *** Create keywords for telescope and detector
WRITE/KEYW   IDENT/C/1/20     "?"              ! object identification
WRITE/KEYW   OBSER/C/1/20     "?"              ! observer

WRITE/KEYW   TELES/C/1/20     "?"              ! Telescope ID descriptor
WRITE/KEYW   INSTR/C/1/20     "?"              ! Instrument ID descriptor
WRITE/KEYW   DETEC/C/1/20     "?"              ! Detector ID descriptor
WRITE/KEYW   NPIXX/I/1/2      -99999           ! number of pixels in x
WRITE/KEYW   NPIXY/I/1/1      -99999           ! number of pixels in y
WRITE/KEYW   SPIXX/I/1/1      -99999           ! pixel size in x in micron
WRITE/KEYW   SPIXY/I/1/1      -99999           ! pixel size in y in micron
WRITE/KEYW   DIRECT/C/1/3     "?"              ! readout direction (x or y)
WRITE/KEYW   BINNING/I/1/2    -99999,-99999    ! binning factors (x and y)
WRITE/KEYW   READMOD/C/1/4    "?"              ! readout mode (slow or fast)
WRITE/KEYW   READON/R/1/1     -99999           ! readout noise per ADU
WRITE/KEYW   AD_VAL/R/1/2     -99999           ! analog to digital (e-/ADU)
!
! *** keywords for information about the data
WRITE/KEYW   CCD_IN/C/1/60    "?"              ! input frame or table
WRITE/KEYW   OV_SEC/C/1/40    "[<,<:>,>]"      ! overscan section
WRITE/KEYW   IM_SEC/C/1/40    "[<,<:>,>]"      ! useful data frame
!
!BiaS
! *** combining parameters for bias frames
WRITE/KEYW   BS_TYP/C/1/60    "*"              ! bias exposure type allowed
WRITE/KEYW   BS_SIG/C/1/3     "no"             ! generate output sigma frame
WRITE/KEYW   BS_MET/C/1/20    "maxrej"         ! method of combining 
WRITE/KEYW   BS_DEL/C/1/3     "no "            ! delete input cat + frames
WRITE/KEYW   BS_EXP/C/1/3     "no "            ! scale by exposure time
WRITE/KEYW   BS_STA/C/1/6     "mode"           ! correction by Mode/Median/Mean
WRITE/KEYW   BS_SCA/C/1/3     "no "            ! scale by the MMM
WRITE/KEYW   BS_OFF/C/1/3     "no "            ! add offset from MMM
WRITE/KEYW   BS_WEI/C/1/3     "yes"            ! use the weighted average
WRITE/KEYW   BS_SEC/C/1/40    "[<,<:>,>]"      ! frame section for finding MMM
WRITE/KEYW   BS_RAN/R/1/2     -99999,99999     ! pixel range used for combining
WRITE/KEYW   BS_CLP/R/1/2     3.0,3.0          ! low/high sigma clipping factor
WRITE/KEYW   BS_NUL/C/1/20    {NULL(2)}        ! blank value for rejections
!
!Dark
! *** combining parameters for dark frames
WRITE/KEYW   DK_TYP/C/1/60    "*"              ! dark exposure type allowed
WRITE/KEYW   DK_SIG/C/1/3     "no"             ! generate an output sigma frame
WRITE/KEYW   DK_MET/C/1/20    "median"         ! method of combining 
WRITE/KEYW   DK_DEL/C/1/3     "no "            ! delete input cat + frames
WRITE/KEYW   DK_EXP/C/1/3     "yes"            ! scale by exposure time
WRITE/KEYW   DK_STA/C/1/6     "mode"           ! correction by Mode/Median/Mean
WRITE/KEYW   DK_SCA/C/1/3     "no "            ! scale by the MMM
WRITE/KEYW   DK_OFF/C/1/3     "no "            ! add offset from MMM
WRITE/KEYW   DK_SEC/C/1/40    "[<,<:>,>]"      ! frame section for finding MMM
WRITE/KEYW   DK_WEI/C/1/3     "yes"            ! use the weighted average
WRITE/KEYW   DK_RAN/R/1/2     -99999,99999     ! pixel range used for combining
WRITE/KEYW   DK_CLP/R/1/2     3.0,3.0          ! low/high sigma clipping factor
WRITE/KEYW   DK_NUL/C/1/20    {NULL(2)}        ! blank value for rejections
!
!Flat Field
! *** combining parameters for dome flats
WRITE/KEYW   FF_TYP/C/1/60    "*"              ! flat exposure type allowed
WRITE/KEYW   FF_SIG/C/1/3     "no"             ! generate an output sigma frame
WRITE/KEYW   FF_MET/C/1/20    "median"         ! method of combining 
WRITE/KEYW   FF_DEL/C/1/3     "no "            ! delete input cat + frames
WRITE/KEYW   FF_EXP/C/1/3     "yes"            ! scale by exposure time
WRITE/KEYW   FF_STA/C/1/6     "mode"           ! correction by Mode/Median/Mean
WRITE/KEYW   FF_SCA/C/1/3     "yes"            ! scale by the MMM
WRITE/KEYW   FF_OFF/C/1/3     "no "            ! add offset from MMM
WRITE/KEYW   FF_SEC/C/1/40    "[<,<:>,>]"      ! frame section for finding MMM
WRITE/KEYW   FF_WEI/C/1/3     "no"             ! use the weighted average
WRITE/KEYW   FF_RAN/R/1/2     -99999,99999     ! pixel range used for combining
WRITE/KEYW   FF_CLP/R/1/2     3.0,3.0          ! low/high sigma clipping factor
WRITE/KEYW   FF_NUL/C/1/20    {NULL(2)}        ! blank value for rejections
!
!SKy
! *** combining parameters for sky flats
WRITE/KEYW   SK_TYP/C/1/60    "*"              ! sky exposure type allowed
WRITE/KEYW   SK_SIG/C/1/3     "no"             ! generate an output sigma frame
WRITE/KEYW   SK_MET/C/1/20    "avsigclip"      ! method of combining 
WRITE/KEYW   SK_DEL/C/1/3     "no "            ! delete input cat + frames
WRITE/KEYW   SK_EXP/C/1/3     "yes"            ! scale by exposure time
WRITE/KEYW   SK_STA/C/1/6     "mode"           ! correction by Mode/Median/Mean
WRITE/KEYW   SK_SCA/C/1/3     "yes"            ! scale by the MMM
WRITE/KEYW   SK_OFF/C/1/3     "no "            ! add offset from MMM
WRITE/KEYW   SK_SEC/C/1/40    "[<,<:>,>]"      ! frame section for finding MMM
WRITE/KEYW   SK_WEI/C/1/3     "no"             ! use the weighted average
WRITE/KEYW   SK_RAN/R/1/2     -99999,99999     ! pixel range used for combining
WRITE/KEYW   SK_CLP/R/1/2     3.0,3.0          ! low/high sigma clipping factor
WRITE/KEYW   SK_NUL/C/1/20    {NULL(2)}        ! blank value for rejections
!
!OTher
! *** combining parameters for other frames
WRITE/KEYW   OT_TYP/C/1/60    "*"              ! other exposure type allowed
WRITE/KEYW   OT_SIG/C/1/3     "no"             ! generate an output sigma frame
WRITE/KEYW   OT_MET/C/1/20    "average"        ! method of combining 
WRITE/KEYW   OT_DEL/C/1/3     "no "            ! delete input cat + frames
WRITE/KEYW   OT_EXP/C/1/3     "yes"            ! scale by exposure time
WRITE/KEYW   OT_STA/C/1/6     "mode"           ! correction by Mode/Median/Mean
WRITE/KEYW   OT_SCA/C/1/3     "yes"            ! scale by the MMM
WRITE/KEYW   OT_OFF/C/1/3     "no "            ! add offset from MMM
WRITE/KEYW   OT_SEC/C/1/40    "[<,<:>,>]"      ! frame section for finding MMM
WRITE/KEYW   OT_WEI/C/1/3     "yes"            ! use the weighted average
WRITE/KEYW   OT_RAN/R/1/2     -99999,99999     ! pixel range used for combining
WRITE/KEYW   OT_CLP/R/1/2     3.0,3.0          ! low/high sigma clipping factor
WRITE/KEYW   OT_NUL/C/1/20    {NULL(2)}        ! blank value for rejections
!
!MOsaic
! *** combining parameters for mosaicing
WRITE/KEYW   MO_SEC/C/1/40    "[<,<:>,>]"      ! frame section for finding mean
WRITE/KEYW   MO_SUBT/C/1/10   "no"             ! subtraction option
WRITE/KEYW   MO_CORN/C/1/2    "ll"             ! starting position in output
WRITE/KEYW   MO_DIREC/C/1/3   "row"            ! add subraster row/column wise
WRITE/KEYW   MO_RAST/C/1/3    "no "            ! add in raster pattern
WRITE/KEYW   MO_OVER/I/1/2    -1,-1            ! # col/row between adj. frames
WRITE/KEYW   MO_INTER/C/1/20  "LINEAR"         ! interpolation method
WRITE/KEYW   MO_TRIM/I/1/4    1,1,1,1          ! trim columns and rows
WRITE/KEYW   MO_MNPX/I/1/1    100              ! minimal number of pixels
WRITE/KEYW   MO_NUL/C/1/20    {NULL(2)}        ! blank value for rejections
!
!OVerscan
! *** parameters for the overscan dtermination
WRITE/KEYW   OV_IMODE/C/1/10  "yes"            ! fit overscan interactively
WRITE/KEYW   OV_FUNCT/C/1/20  "linear"         ! fitting function
WRITE/KEYW   OV_ORDER/I/1/1   3                ! number of polynomial terms
WRITE/KEYW   OV_AVER/I/1/1    1                ! number of points to combine
WRITE/KEYW   OV_ITER/I/1/1    1                ! number of iterations
WRITE/KEYW   OV_REJEC/R/1/1   3.               ! sigma rejection factor
!  
!FiX bad pixels
! *** parameters for the bad pixel correction!
WRITE/KEYW   FX_TABLE/C/1/60  "none"           ! table with bad pixels area
WRITE/KEYW   FX_METH/C/1/3    "PIX"            ! correction option to use
WRITE/KEYW   FX_FACT/I/1/2    3,3              ! expansion factor 
WRITE/KEYW   FX_FPAR/I/1/3    2,2,5            ! fitting parameters
WRITE/KEYW   FX_NOISE/C/1/3   "yes"            ! gaussian noise option
!
!ILlumination
! *** parameters for illumination frame
WRITE/KEYW   IL_TYP/C/1/60   "?"               ! exposure type for illum. frame 
WRITE/KEYW   IL_XBOX/R/1/2    5.,0.25          ! max box at edges, elsewhere
WRITE/KEYW   IL_YBOX/R/1/2    5.,0.25          ! max box at edges, elsewhere
WRITE/KEYW   IL_CLIP/C/1/3    "yes"            ! clipping the input pixels
WRITE/KEYW   IL_SIGMA/R/1/2   2.5,2.5          ! low and high clipping sigma
!
!FRinge
! *** parameters for fringe frame
WRITE/KEYW   FR_TYP/C/1/60   "?"               ! exposure type for fringe frame
!
!SCience
! *** parameters for the science correction
WRITE/KEYW   SC_TYP/C/1/60    "*"              ! science exposure type allowed
WRITE/KEYW   SC_PROC/C/1/3    "no"             ! list processing steps only
WRITE/KEYW   SC_SCAN/C/1/3    "no"             ! do the overcan correction
WRITE/KEYW   SC_TRIM/C/1/3    "no"             ! trim the frame(s)
WRITE/KEYW   SC_FXPIX/C/1/3   "no"             ! fix bad pixel or columns
WRITE/KEYW   SC_BSCOR/C/1/3   "yes"            ! do the bias correction
WRITE/KEYW   SC_BSFRM/C/1/60  " "              ! bias calibration frame
WRITE/KEYW   SC_DKCOR/C/1/3   "yes"            ! do the dark correction
WRITE/KEYW   SC_DKFRM/C/1/60  " "              ! dark calibration frame
WRITE/KEYW   SC_FFCOR/C/1/3   "yes"            ! do the flat fielding
WRITE/KEYW   SC_FFFRM/C/1/60  " "              ! flat calibration frame
WRITE/KEYW   SC_ILCOR/C/1/3   "no"             ! do illumination correction
WRITE/KEYW   SC_ILFRM/C/1/60  " "              ! illumination frame
WRITE/KEYW   SC_SKFRM/C/1/60  " "              ! empty sky frame
WRITE/KEYW   SC_FRCOR/C/1/3   "no"             ! do fringing  correction
WRITE/KEYW   SC_FRFRM/C/1/60  " "              ! fringing calibration frame
!
! *** file names and error keywords
WRITE/KEYW   VERBOSE/C/1/3    "no"             ! print standard output
WRITE/KEYW   REDUCT/C/1/8     "offline"        ! off/on line data reduction
WRITE/KEYW   CCDCORS/C/1/20   "XXXXXXXXXXXXXXXXXXXX" ! reduction options
WRITE/KEYW   CCDCOR/C/1/3     "NO "            ! reduction flag
WRITE/KEYW   CCDCMD/C/1/40    "?"              ! last command executed

WRITE/KEYW   CCDSCAN/C/1/60   " "              ! overscan image
WRITE/KEYW   SCANSEL/C/1/80   "ALL"            ! valid pixel range for overscan
WRITE/KEYW   CCDMEAN/R/1/1    -99999           ! mean value
WRITE/KEYW   CCDDARK/R/1/1    -99999           ! dark scaling factor 
WRITE/KEYW   CCDFLAT/R/1/1    -99999           ! flat scaling factor
WRITE/KEYW   CCDILLUM/R/1/1   -99999           ! illumination scaling factor
WRITE/KEYW   CCDFRING/R/1/1   -99999           ! fringe scaling factor
!
! *** here for the standard column names
WRITE/KEYW   SC_COL/C/1/60    "?"              ! standard science column name
WRITE/KEYW   BS_COL/C/1/60    "?"              ! standard bias column name
WRITE/KEYW   DK_COL/C/1/60    "?"              ! standard dark column name
WRITE/KEYW   FF_COL/C/1/60    "?"              ! standard flat column name
WRITE/KEYW   SK_COL/C/1/60    "?"              ! standard sky column name
WRITE/KEYW   IL_COL/C/1/60    "?"              ! standard illum. column name
WRITE/KEYW   FR_COL/C/1/60    "?"              ! standard fringe column name
! *** end Session List
!
! *** Initialize CCD context
REGISTER/SESSION CCD STD_PROC ccdinit.prg ccdtab.tbl  
!
@s ccdcopy                                     ! copy the procedure and table
@s eso_descr                                   ! copy ESO standard descriptors
RETURN                                         ! all done
!
! *** here for restoring the keyword from a session control table
INIT:
SAVINI/CCD {P1}_CCDSAVE.tbl READ
WRITE/OUT "*** INFO: CCD context setting restored for session <{P1}>"





