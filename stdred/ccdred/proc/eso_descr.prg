! @(#)eso_descr.prg	19.1 (ES0-DMD) 02/25/03 14:16:03
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT (C) 1993 European Southern Observatory
!.IDENT     eso_descr.prg
!.AUTHOR    Rein H. Warmels,  ESO-Garching
!.KEYWORDS  Direct imaging, CCD package
!.PURPOSE   Setup procedure for internal ccd keywords structure
!.VERSION   920416  RHW  Creation
!.VERSION   930121  RHW  Inclusion of exposure type and time
!.VERSION   950103  RHW  Simplification; deletion of keywords in ccdinit.prg
!
!---------------------------|---------------------| ---------------------------
! Do NOT write/change here  | ESO standard descr. ! Comments
!---------------------------| --------------------| ---------------------------
!
! *** here store the ESO standard descriptor names in the CCD keywords
WRITE/KEYW   .IDENT/C/1/20    "IDENT"             ! object ident. descriptor
WRITE/KEYW   .OBSER/C/1/20    "OBSERVER"          ! observer descriptor
WRITE/KEYW   .TELES/C/1/20    "ESO.TEL.ID"        ! telescope ID descriptor
WRITE/KEYW   .INSTR/C/1/20    "INSTRUME"          ! instrument ID descriptor
WRITE/KEYW   .DETEC/C/1/20    "ESO.DET.ID"        ! detector ID descriptor
WRITE/KEYW   .BINNING/C/1/20  "?"                 ! binning in x and y
WRITE/KEYW   .DIRECT/C/1/20   "?"                 ! readout direction
WRITE/KEYW   .READMOD/C/1/20  "ESO.DET.MODE"      ! readout mode
WRITE/KEYW   .READON/C/1/20   "ESO.DET.READON"    ! readout noise per AD unit
WRITE/KEYW   .AD_VAL/C/1/20   "ESO.DET.AD_VALUE"  ! analog to digital descr.
WRITE/KEYW   EXP_DESC/C/1/20  "ESO.GEN.EXPO.TYPE" ! exposure type descriptor
WRITE/KEYW   O_DESC/C/1/20    "O_TIME(7)"         ! exposure time descriptor
!
!
! *** here for the default column names of the association table
WRITE/KEYW   SC_COL/C/1/10    "SCI"               ! standard sci column name
WRITE/KEYW   BS_COL/C/1/10    "BIAS_MAST"         ! standard bias column name
WRITE/KEYW   DK_COL/C/1/10    "DK_MAST"           ! standard dark column name
WRITE/KEYW   FF_COL/C/1/10    "FF_MAST"           ! standard flat column name
WRITE/KEYW   SK_COL/C/1/10    "FFSKY_MAST"        ! standard sky column name
WRITE/KEYW   IL_COL/C/1/10    "IL_MAST"           ! standard illum. column name
WRITE/KEYW   FR_COL/C/1/10    "FR_MAST"           ! standard fringe column name
!
!---------------------------|---------------------!----------------------------
! Do NOT write/change here  | ESO standard descr. ! Comments
!---------------------------| --------------------|----------------------------

