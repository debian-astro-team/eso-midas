! @(#)mosaicfit.prg	19.1 (ES0-DMD) 02/25/03 14:16:05
! @(#)mosaicfit.prg	19.1 (ESO-SDAG) 02/25/03 14:16:05
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1995 European Southern Observatory
!.IDENT       mosaicfit.prg
!.PURPOSE     Do alignment fitting of mosaic of subframes
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package, mosacing
!.USE         @@ mosaicmatch
!.            with  P1 input mosaic frame
!.                  P2 database table"
!.                  P3 combined output frame"
!.                  P4 column and row of reference subframe"
!.                  P5 matching subrasters
!.                  P6 column and row of reference subraster
!.                  P7 x and y offset of reference subframe"
!.                  P8 size of the output frame"
!.VERSION     950919 RHW  Creation
!-------------------------------------------------------
DEFINE/PARAMETER P1  ?    C  "Enter input mosaic frame:"
DEFINE/PARAMETER P2  none C  "Enter input mask:"
DEFINE/PARAMETER P3  ?    C  "Enter database table:"
DEFINE/PARAMETER P4  ?    C  "Enter combined output frame:"
DEFINE/PARAMETER P5  none C  "Subrasters to be excluded:"
DEFINE/PARAMETER P6  0,0  N  "Enter column and row of reference subframe:"
DEFINE/PARAMETER P7  0,0  N  "Enter x and y offset of reference subframe:"
DEFINE/PARAMETER P8  0,0  N  "Enter size of the output frame:"
!
DEFINE/LOCAL CCDIN/C/1/60    " "
DEFINE/LOCAL CCDMSK/C/1/60   " "
DEFINE/LOCAL CCDOUT/C/1/60   " "
DEFINE/LOCAL TBLDB/C/1/60    " "
DEFINE/LOCAL IDOT/I/1/1      0
!
! *** All done; now copy remaining keywords to local ones
IDOT = M$INDEX("{P1}",".bdf")                                 ! get input frame
IF IDOT .GT. 0 THEN 
   CCDIN = "{P1(1:{IDOT})}"
ELSE
   CCDIN = "{P1}"                                          
ENDIF
IF M$EXIST("{CCDIN}.bdf") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <FIT>: Input frame {CCDIN} not existing"
   RETURN/EXIT
ENDIF
!
IDOT = M$INDEX("{P2}",".bdf")                                 ! get input frame
IF IDOT .GT. 0 THEN 
   CCDMSK = "{P2(1:{IDOT})}"
ELSE
   CCDMSK = "{P2}"                                          
ENDIF
IF CCDMSK .NE. "none" .AND. M$EXIST("{CCDMSK}.bdf") .EQ. 0 THEN
   WRITE/OUT "*** FATAL <FIT>: Input mask {CCDMSK} not existing"
   RETURN/EXIT
ENDIF
!
! *** check the database
IDOT = M$INDEX("{P3}",".tbl")                              ! get database table
IF IDOT .GT. 0 THEN  
   TBLDB = "{P3(1:{IDOT})}"
ELSE
   TBLDB = "{P3}" 
ENDIF
!
CCDOUT = "{P4}"
!
! *** fill thr required keywords 
WRITE/KEYW   EXCLUDE/C/1/60  {P5}                 ! list of matching subrasters
DEFINE/LOCAL NRSUB/I/1/2     {P6}
DEFINE/LOCAL XYREF/I/1/2     {P7}
DEFINE/LOCAL OSIZE/I/1/2     {P8}
DEFINE/LOCAL INTERPOL/C/1/20 {MO_INTER}
DEFINE/LOCAL MINPIX/I/1/1    {MO_MNPX}
DEFINE/LOCAL TR_SEC/I/1/4   {MO_TRIM(1)},{MO_TRIM(2)},{MO_TRIM(3)},{MO_TRIM(4)}
DEFINE/LOCAL BLANK/C/1/20    {MO_NUL}                              ! null value
!
RUN STD_EXE:CCDMOSFIT
!
! *** check on existence of the output frame
IF M$EXIST("{CCDOUT}.bdf") .EQ. 0 THEN                           ! is it there?
   WRITE/OUT "*** ERROR: Failed to make the intensity matched output frame"
   RETURN
ENDIF
!
! *** write the descriptors
WRITE/DES {CCDOUT} LHCUTS/R/1/1 {OUTPUTR(1)},{OUTPUTR(2)}
WRITE/DES {CCDOUT} MEAN/R/1/1   {OUTPUTR(3)}
WRITE/DES {CCDOUT} MODE/R/1/1   {OUTPUTR(7)}
WRITE/DES {CCDOUT} MEDIAN/R/1/1 {OUTPUTR(8)}
!
RETURN








