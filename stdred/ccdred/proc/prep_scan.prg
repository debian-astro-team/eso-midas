! @(#)prep_scan.prg	19.1 (ES0-DMD) 02/25/03 14:16:06
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       prep_scan.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     BIAS overscan correction of input frame
!.USE         @s prepscan p1 p2
!.              p1 = input frame
!.              p2 = correction lable string
!.METHOD      - Return if overscan is not requested or has been corrected
!.              previously
!.            - Determine scan direction and area.
!.            - Average overscan area in columns or rows
!.            - Convert the overscan row/column into table and fit a function 
!.              with the MIDAS fit commands.
!.            - Do the overscan correction of the input frame
!.            - Set the processing flags.
!.            The mean and s.d. of a defined area of overclocked columns is 
!.            calculated by STATISTICS/IMAGE and the resulting values are put 
!.            into the descriptor CCDBIAS. The mean is subtracted from the 
!.            image. All coordinates are in (integer) pixels.
!.PARAMETERS  P1: input frame
!             P2: correction label string which will be checked by
!                 ccdcorrect.prg to do the desired actions.
!.NOTE        The procedure checks if the operation was already done
!.            by checking the descriptor CCDSTAT
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for bad pixel correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930105  RHW  Creation
!.VERSION     971117  SW   correction labels have to be local -> new param.
!.VERSION     980128  SW   additional return param = name of scan-table
!                          bug: START, STEP have to be adjusted to INPUT-frame
!                          bug: DIRECT==y: use the right STA, STE, IPIX
!-------------------------------------------------------
DEFINE/PARAM P1 ?        I "Input frame for overscan correction:"
DEFINE/PARAM P2 {CCDCORS(1:20)}
!The correction label string CCDCORS which will be checked by CCDCORRECT.PRG
!has to be local, in order to always get the right link to the actual frame.
!In a global definition the correction labels may overwritten by other pre-
!pared frames which leads to unwished actions to previous frames (SW,97.11.17).
!
! *** Check if overscan has already been applied
IF SC_SCAN .NE. "yes" .OR. {{P1},CCDSTAT(1)} .EQ. 1 THEN
   WRITE/OUT "{P1}: Overscan correction not requested or was applied"
   RETURN {P2}
ENDIF
!
! *** return if only listing is wanted
IF SC_PROC .EQ. "yes" THEN
   WRITE/OUT "{P1}: *** TO BE DONE: Overscan section is {OV_SEC}"
   RETURN {P2}
ENDIF
!
! *** more locals
DEFINE/LOCAL KAPPA/R/1/1    {OV_REJEC}
DEFINE/LOCAL PRERMS/R/1/1   0.
DEFINE/LOCAL RMS/R/1/1      0.
DEFINE/LOCAL RATIO/R/1/1    0.
DEFINE/LOCAL ITER/I/1/1     0
DEFINE/LOCAL IDOT/I/1/1     0
DEFINE/LOCAL IPIX/I/1/2     0 ALL
DEFINE/LOCAL STA/D/1/2      0 ALL
DEFINE/LOCAL STE/D/1/2      0 ALL
DEFINE/LOCAL CCDIN/C/1/60   " " ALL
DEFINE/LOCAL CCDOUT/C/1/60  " " ALL
DEFINE/LOCAL SCANTBL/C/1/60  " " ALL
DEFINE/LOCAL SCANMEAN/R/1/1 0
!
! *** write the keywords
WRITE/KEYW INPUTC/C/1/1 {OV_IMODE}                     ! manual of auto fitting
COPY/DK {P1} NPIX/I/1/2 IPIX
COPY/DK {P1} START/D/1/2 STA
COPY/DK {P1} STEP/D/1/2  STE
!
STATIS/IMAGE {P1} {OV_SEC} ? ? SN               ! do statistics of the overscan
SCANMEAN = {{P1},STATISTIC(3)}
!
! *** setting for interactive use
IF INPUTC(1:1) .EQ. "y" THEN
   CREATE/GRAPH 0
   WRITE/OUT "{P1}: Overscan area interactively fitted"
   SET/GRAP pmode=1 ltype=1 stype=0
ENDIF
!
! ***  label for interactive use 
INTER:
IDOT = M$INDEX(P1,".bdf")-1
IF IDOT .GT. 0 THEN 
   CCDIN = "{P1(1:{IDOT})}"
ELSE
   CCDIN = "{P1}"
ENDIF
CCDOUT = "{CCDIN}_scan"
WRITE/KEYW INPUTI/I/1/1 {OV_AVER}                         ! smoothing parameter
RUN STD_EXE:ccdscan.exe                          ! create smooth overscan table
SELECT/TABLE {CCDOUT} {SCANSEL}
!
! here do the fitting
DO ITER = 1 {OV_ITER}
   WRITE/OUT "{CCDOUT}: Interation {ITER} out of {OV_ITER} started"
   IF OV_FUNCT(1:1) .EQ. "l" THEN                           ! do the linear fit
      REGRESSION/LINEAR {CCDOUT} :FLUX :X
      RMS = OUTPUTR(3)*KAPPA
   ELSEIF OV_FUNCT(1:1) .EQ. "p" THEN                       ! do polynomial fit
      REGRESSION/POLYNOMIAL {CCDOUT} :FLUX :X {OV_ORDER}
      RMS = OUTPUTR(5)*KAPPA
   ENDIF
! 
   IF ITER .EQ. 1 THEN                                         ! iteration step
      PRERMS = RMS
   ELSE
      RATIO  = RMS/PRERMS-1.
      PRERMS = RMS
      IF M$ABS(RATIO) .LT. 0.01 THEN
         IF OV_IMODE(1:1) .EQ. "y" THEN
            GOTO INQUIR
         ENDIF
      ENDIF
   ENDIF
   SAVE/REGRESSION {CCDOUT} COEF
   COMPUTE/REGRESSION {CCDOUT} :FIT = COEF
   COMPUTE/TABLE {CCDOUT} :RESIDUAL = :FIT - :FLUX      ! select for next iter
   SELECT/TABLE {CCDOUT} {SCANSEL}.AND.(ABS(:RESIDUAL).LT.{RMS})
ENDDO
!
INQUIR:
IF OV_IMODE(1:1) .EQ. "y" THEN                   ! overplot in interactive mode
   SELECT/TABLE {CCDOUT} ALL
   SET/GRAP COLOUR=2
   OVERPLOT/TABLE {CCDOUT} :X :FIT
   SET/GRAP COLOUR=1
   INQUIRE/KEYW INPUTC/C/1/1 "***> Do you want to try another fit [no]?: "
   IF AUX_MODE(7) .EQ. 0 INPUTC(1:1) = "N"  
   IF INPUTC(1:1) .EQ. "Y" THEN
      INQUIRE/KEYW OV_FUNCT/C/1/20 "***> Enter type of function [{OV_FUNCT}]: "
      IF OV_FUNCT(1:3) .EQ. "pol" THEN                             ! do the fit
         OV_FUNCT = "polynomial"
         INQUIRE/KEYW OV_ORDER/I/1/1 -
                      "***> Enter degree of the polynomial [3]: "
         IF AUX_MODE(7) .EQ. 0 OV_ORDER = 3
      ELSE    
         OV_FUNCT = "linear"
      ENDIF
      INQUIRE/KEYW OV_AVER/I/1/1 "***> Enter number of points to combine [1]: "
      IF AUX_MODE(7) .EQ. 0 OV_AVER = 1
      INQUIRE/KEYW OV_ITER/I/1/1 "***> Enter number of iterations [1]: "
      IF AUX_MODE(7) .EQ. 0 OV_ITER = 1    
      INQUIRE/KEYW OV_REJEC/R/1/1 "***> Enter sigma rejection factor [3.]: "
      IF AUX_MODE(7) .EQ. 0 OV_REJEC = 3.    
      GOTO INTER   
   ELSE
      IF CCDCMD(1:3) .EQ. "RED" THEN                     ! only for reduce/ccd
         INQUIRE/KEYW INPUTC/C/1/1 -
                      "***> Continue the interactive fitting [yes]: "
         IF AUX_MODE(7) .EQ. 0 OV_IMODE = "yes"  
         IF INPUTC(1:1) .EQ. "y" THEN
            OV_IMODE = "yes"  
         ELSE
            OV_IMODE = "no"
         ENDIF
      ENDIF 
   ENDIF
ENDIF
!
! *** write the output overscan frame
COMPUTE/KEYW SCANTBL = "{CCDOUT}"		      ! save name of scan-table
IF OV_ORDER .GE. 1 THEN                                                ! vector
   SELECT/TABLE {CCDOUT} all
   COPY/TABLE {CCDOUT} &t
   DELETE/COLUMN &t :X :FLUX :RESIDUAL
   COPY/TI &t &o
   IF DIRECT .EQ. "X" THEN
      GROW/IMAGE {CCDOUT} = &o {STA(1)},{STE(1)},{IPIX(1)} <,line C
!adjust world coordinates to the ones of the input frame as CCDOUT will later
!be used for bias correction [the x are already ok (SW)]
      {CCDOUT},START(2) = {STA(2)}
      {CCDOUT},STEP(2) = {STE(2)}
   ELSE
!      GROW/IMAGE {CCDOUT} = &o {STA(1)},{STE(1)},{IPIX(1)} <,line L   
      GROW/IMAGE {CCDOUT} = &o {STA(2)},{STE(2)},{IPIX(2)} <,line L   
!adjust world coordinates to the ones of the input frame as CCDOUT will later
!be used for bias correction [the y are already ok (SW)]
      {CCDOUT},START(1) = {STA(1)}
      {CCDOUT},STEP(1) = {STE(1)}
   ENDIF
   DELETE/TABLE &t NO
   DELETE/IMAGE &o NO
   CCDSCAN = "{CCDOUT}"                                  ! store overscan image

ELSE                                                                   ! scalar
   STATIS/TABLE {CCDOUT} :FLUX
   SCANMEAN = {OUTPUTR(3)}
   CCDSCAN  = "{SCANMEAN}"
ENDIF
!
WRITE/OUT "{CCDIN}.bdf: Overscan area is {OV_SEC}; mean={SCANMEAN}"
P2(1:1) = "O"
P2(11:11) = "Y"
RETURN {P2} {SCANTBL}
