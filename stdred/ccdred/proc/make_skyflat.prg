! @(#)make_skyflat.prg	19.1 (ESO-DMD) 02/25/03 14:16:04
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       make_skyflat.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Make a sky flat field by multiplying the input illumination 
!             frame with the flat field
!.VERSION     930730  RHW  Creation
!.NOTE        The procedure checks if the operation was already done
!.            by checking the descriptor CCDSTAT(6)
!.            Other CCDSTAT descriptor values
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!-------------------------------------------------------
DEFINE/PARAM P1 ?          IMA "Enter input flat field frame: "
DEFINE/PARAM P2 ?          IMA "Enter sky illumination frame: "
DEFINE/PARAM P3 ?          IMA "Enter sky flat output frame : "
!
DEFINE/LOCAL SCALE/R/1/1 1.0
!
! First, check if already operation has already been done
IF M$EXISTD(P1,"SKYFLAT") .EQ. 1 THEN
   RETURN
ENDIF
!
IF M$EXIST("{P2}.bdf"} .EQ. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: Illumination correction {P2} doesn't exist ..."
   RETURN
ENDIF
!
IF M$EXISTD(P2,"CCDMEAN") .EQ. 1 THEN
   SCALE = {{P2},CCDMEAN}
ELSE
   @s ccdmean {P2}
   IF M$EXISTD(P2,"CCDMEAN") .EQ. 1 THEN
      SCALE = {{P2},CCDMEAN}
   ELSE
      SCALE = 1.0
   ENDIF
ENDIF
!
IF SCALE .LE. 0 THEN
   COMPUTE/IMAGE {P3} = {P1}*{P2}
ELSE
   COMPUTE/IMAGE {P3} = {P1}*{P2}/{SCALE}
ENDIF
WRITE/DESCR {P3} CCDSTAT/I/7/1 1                              ! correction done
WRITE/DESCR {P3} SKYFLAT/C/1/10 SKYFLAT                   ! sky flat descriptor
WRITE/DESCR {P3} {EXP_DESC}/C/1/20 {FF_TYP}                     ! flat exposure
IF VERBOSE(1:1) .EQ. "Y" THEN 
   STATISTIC/IMAGE {P3} {IM_SEC} ? ? FF                       ! full statistics
ELSE 
   STATISTIC/IMAGE {P3} {IM_SEC} ? ? FX                      ! short statistics
ENDIF
!
RETURN





