! @(#)ccdbias.prg	19.1 (ESO-DMD) 02/25/03 14:15:58
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccdbias.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Do the correction of bias frame
!.USE         @s ccdbscor P1 P2 P3 
!.               P1 = input frame
!.               P2 = output frame
!.               P3 = bias frame
!.NOTE        The procedure checks if the operation was already done
!.            by checking the descriptor CCDSTAT(4)
!.            Other CCDSTAT descriptor values
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930503  RHW  Creation
!-------------------------------------------------------
DEFINE/PARAM P1 ?                 I "Enter input frame:"
DEFINE/PARAM P2 ?                 I "Enter output frame:"
DEFINE/PARAM P3 {SC_BSFRM}        I "Enter the bias frame:"
!
! *** check if bias hasn't already been subtracted or if present
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/OUT "{P1}: *** WARNING: No check on CCD reduction status possible"
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ELSE
   IF {{P1},CCDSTAT(4)} .EQ. 1 THEN                                ! bias
      WRITE/OUT "{P1}: *** WARNING: Bias already subtracted; nothing done ..."
      RETURN
   ENDIF
ENDIF
!
! *** check the frame and exposure descriptors; return on errors
IF M$EXIST("{P3}.bdf"} .EQ. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: Bias frame {P3}.bdf doesn't exist ..."
   RETURN
ENDIF
IF BS_TYP .NE. "*" .AND. BS_TYP .NE. "?" THEN 
   IF M$EXISTD(P3,EXP_DESC) .EQ. 1 .AND. -
      "{{P3},{EXP_DESC}}" .NE. "{BS_TYP}" THEN
      WRITE/OUT -
      "{P3}: *** ERROR: Exposure descriptor {EXP_DESC} has wrong type"
   RETURN
   ENDIF
ENDIF
!
! *** do the work
COMPUTE/IMAGE {P2} = {P1} - {P3} 
WRITE/DESCR {P2} CCDSTAT/I/4/1 1                      ! bias correction done
WRITE/DESCR {P2} HISTORY/C/-1/80 "BIAS/CCD {P1} {P2} {P3}
IF VERBOSE(1:1) .EQ. "Y" THEN
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FF               ! full statistics
ELSE
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FX               ! short statistics
ENDIF
!
RETURN







