! @(#)ccddark.prg	19.1 (ESO-DMD) 02/25/03 14:15:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       ccddark.prg
!.AUTHOR      Rein H. Warmels,  ESO-Garching
!.KEYWORDS    Direct imaging, CCD package
!.PURPOSE     Do the correction of dark current frame
!.ALGORYTHM   Check for the existence of the input frame and the correct
!.            exposure time descriptors. Exit if failing.
!.            Check the processing flag; if corrected already exit 
!.            Compute the dark current scaling factor. Set the processing
!.            flag if dark current correcte is finished
!.VERSION     920731  RHW  Creation
!.NOTE        The procedure checks if the operation was already done by
!.            checking the descriptor CCDSTAT(5). 
!             Other CCDSTAT descriptor values
!.            CCDSTAT(1) for overscan correction
!.            CCDSTAT(2) for trimming the frame
!.            CCDSTAT(3) for bad pixel correction
!.            CCDSTAT(4) for bias subtraction
!.            CCDSTAT(5) for dark current subtraction
!.            CCDSTAT(6) for flat fielding
!.            CCDSTAT(7) for illumination correction
!.            CCDSTAT(8) for fringing correction
!.VERSION     930503  RHW  Creation
!-------------------------------------------------------
DEFINE/PARAM P1 ?                IMA "Enter input frame: "
DEFINE/PARAM P2 ?                IMA "Enter output frame: "
DEFINE/PARAM P3 {SC_DKFRM}       IMA "Enter dark current frame: "
!
! *** check if dark hasn't already been subtracted or if present
IF M$EXISTD(P1,"CCDSTAT") .EQ. 0 THEN
   WRITE/OUT "{P1}: *** WARNING: No check on CCD reduction status possible"
   WRITE/DESCR {P1} CCDSTAT/I/1/10 0 ALL
ELSE
   IF {{P1},CCDSTAT(5)} .EQ. 1 THEN 
      WRITE/OUT "{P1}: *** WARNING: Dark already subtracted; nothing done ..."
      RETURN
   ENDIF
ENDIF
!
! *** check existence of dark frameand its descriptors; return on error
IF M$EXIST("{P3}.bdf"} .EQ. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: Dark frame {P3}.bdf doesn't exist ..."
   RETURN
ENDIF
IF DK_TYP .NE. "*" .AND. DK_TYP .NE. "?" THEN 
   IF M$EXISTD(P3,EXP_DESC) .EQ. 1 .AND. -
      "{{P3},{EXP_DESC}}" .NE. "{DK_TYP}" THEN
      WRITE/OUT -
      "{P3}: *** ERROR: Exposure descriptor {EXP_DESC} has wrong type"
   RETURN
   ENDIF
ENDIF
!
IF M$EXISTD(P1,O_DESC) .NE. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: Exposure descriptor {O_DESC} is missing"
   RETURN
ENDIF
!
! *** set the processing keywords
DEFINE/LOCAL OBSTIM/R/1/2 0.0,0.0
OBSTIM(1) = {{P1},{O_DESC}}
OBSTIM(2) = {{P3},{O_DESC}}
IF OBSTIM(1) .LE. 0 THEN
   WRITE/OUT "{P1}: *** ERROR: Input frame has invalid exposure time"
   RETURN
ELSEIF OBSTIM(2) .LE. 0 THEN
   WRITE/OUT "{P3}: *** ERROR: Dark frame has invalid exposure time"
   RETURN
ELSE
   CCDDARK = OBSTIM(1)/OBSTIM(2)
ENDIF
!
! *** do the correction and administration
COMPUTE/IMAGE {P2} = {P1} - {CCDDARK}*{P3}
WRITE/DESCR {P2} CCDSTAT/I/5/1 1                      ! dark correction done
WRITE/DESCR {P2} HISTORY/C/-1/80 "DARK/CCD {P1} {P2} {P3}
IF VERBOSE(1:1) .EQ. "Y" THEN
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FF               ! full statistics
ELSE
   STATISTIC/IMAGE {P2} {IM_SEC} ? ? FX               ! short statistics
ENDIF
!
RETURN




