$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.STDRED.CCDRED.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:25:00 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libccd sort.obj,getdat.obj,nrmed.obj,exmed.obj,mo_align.obj,mo_links.obj,mo_setup.obj,mo_shift.obj,mo_tblwpar.obj,mo_tblwinp.obj,mo_tblrpar.obj,mo_msi.obj
$ LIB/REPLACE libccd mo_bifit.obj,mo_zero.obj,mo_match.obj,mo_offset.obj,mo_fitoff.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
