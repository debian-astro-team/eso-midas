/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  ccd_setup.c
.AUTHOR      R.H. Warmels IPG-ESO Garching
.KEYWORDS    mosaicing software
.LANGUAGE    C
.PURPOSE     Setup the data base parameters for the images.
.ENVIRONment MIDAS
             #include <ccd_def.h>      Symbols used by the ccd package
.VERSION     1.0     16-May-1995   creation

 090903		last modif
------------------------------------------------------------*/

/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <ccd_def.h>

extern int  Zstats(), SCKRDR();



/*
 MO_INDICES -- Given the number in the list for a missing subraster and
 information about how the subrasters were written return the i and j
 indices of the specified subrasters.
*/

void MO_INDICES(num, i, j, nxsub, nysub, corner, raster, order)
int    num;                                /* number of the subraster */
int    *i, *j;                             /* indices of the subraster */
int    nxsub, nysub;                       /* number of subrasters in x and y */
int    corner;                             /* starting corner */
char   *raster;                            /* raster order */
char   *order;                             /* column or row order */

{	
switch( (corner) ) 
   {
   case 1:
      if (strcmp(order,MO_ROW) == 0)
         {
	 if (num % nxsub == 0) 
            {
	    *j = num / nxsub;
	    if (strcmp(raster,"YES") == 0 && (*j % 2) == 0)
	       *i = 1;
	    else
	       *i = nxsub;
	    } 
         else 
            {
	    *j = num / nxsub + 1;
	    if (strcmp(raster,"YES") == 0 && (*j % 2) == 0)
	       *i = nxsub - (num % nxsub) + 1;
	    else
	       *i = (num % nxsub);
	    }
	 } 

      else if (strcmp(order,MO_COLUMN) == 0)
         {
	 if (num % nysub == 0) 
            {
	    *i = num / nysub;
	    if (strcmp(raster,"YES") == 0 && (*i & 2) == 0)
	       *j = 1;
	    else
	       *j = nysub;
	    } 
         else 
            {
	    *i = num / nysub + 1;
	    if (strcmp(raster,"YES") == 0 && (*i % 2) == 0)
	       *j = nysub - (num % nysub) + 1;
	    else
	       *j = (num % nysub);
	    }
         }
         break;

   case 2:
      if (strcmp(order,MO_ROW) == 0) 
         {
	 if (num % nxsub == 0) 
            {
	    *j = num / nxsub;
	    if (strcmp(raster,"YES") == 0 && (*j % 2) == 0)
	       *i = nxsub;
	    else
	       *i = 1;
	    } 
         else 
            {
	    *j = num / nxsub + 1;
	    if (strcmp(raster,"YES") == 0 && (*j % 2) == 0)
	       *i = (num % nxsub);
	    else
	       *i = nxsub - (num % nxsub) + 1;
	    }
	 } 

      else if (strcmp(order,MO_COLUMN) == 0)
         {
	 if (num % nysub == 0) 
            {
	    *i = nxsub - num / nysub + 1;
	    if (strcmp(raster,"YES") == 0 && (*i % 2) != 0)
	       *j = 1;
	    else
	       *j = nysub;
	    } 
         else 
            {
	    *i = nxsub - num / nysub;
	    if (strcmp(raster,"YES") == 0 && (*i % 2) != 0)
	       *j = nysub - (num % nysub) + 1;
	    else
	       *j = (num % nysub);
	    }
	 }
         break;

   case 3:
      if (strcmp(order,MO_ROW) == 0) 
         {
	 if (num % nxsub == 0) 
            {
	    *j = nysub - num / nxsub + 1;
	    if (strcmp(raster,"YES") == 0 && (*j % 2) != 0)
	       *i = 1;
	    else
	       *i = nxsub;
	    } 
         else 
            {
	    *j = nysub - num / nxsub;
	    if (strcmp(raster,"YES") == 0 && (*j % 2) != 0)
	       *i = nxsub - (num % nxsub) + 1;
	    else
	       *i = (num % nxsub);
	    }
	 } 

      else if (strcmp(order,MO_COLUMN) == 0)
         {
	 if (num % nysub == 0) 
            {
	    *i = num / nysub;
	    if (strcmp(raster,"YES") == 0 && (*i % 2) == 0)
	       *j = nysub;
	    else
	       *j = 1;
	    } 
         else 
            {
	    *i = num / nysub + 1;
	    if (strcmp(raster,"YES") == 0 && (*i % 2) == 0)
	       *j = (num % nysub);
	    else
	       *j = nysub - (num % nysub) + 1;
	    }
	 }
         break;

   case 4:
      if (strcmp(order,MO_ROW) == 0) 
         {
	 if (num % nxsub == 0) 
            {
	    *j = nysub - num / nxsub + 1;
	    if (strcmp(raster,"YES") == 0 && (*j % 2) != 0)
	       *i = nxsub;
	    else
	       *i = 1;
	    } 
         else 
            {
	    *j = nysub - num / nxsub;
	    if (strcmp(raster,"YES") == 0 && (*j % 2) != 0)
	       *i = (num % nxsub);
	    else
	       *i = nxsub - (num % nxsub) + 1;
	    }
	 } 

      else if (strcmp(order,MO_COLUMN) == 0)
         {
	 if (num % nysub == 0) 
            {
	    *i = nxsub - num / nysub + 1;
	    if (strcmp(raster,"YES") == 0 && (*i % 2) != 0)
	       *j = nysub;
	    else
	       *j = 1;
	    } 
         else 
            {
	    *i = nxsub - num / nysub;
	    if (strcmp(raster,"YES") == 0 && (*i % 2) != 0)
	       *j = (num % nysub);
	    else
	       *j = nysub - (num % nysub) + 1;
	    }
	 }
   }

}  


void MO_SETUP(imnol, naxis, npix, start, step, nnull,
              null_input, im_area, med_area, npixc,
              index, c1, c2, r1, r2, isnull, median)
int     *imnol;
int     *naxis;
int     *npix;
double  *step, *start;
int     nnull;
int     *null_input;
char    *im_area, *med_area;
int     *npixc;
int     *index;
int     *c1, *c2;
int     *r1, *r2;
int     *isnull;
float   *median;

{
int     snpix[4];
float   zbins[3];
double  sstep[3], sstart[3];
int     uni, iav, nulo;
int     nimcols, nimrows;
int     i,j,k,nr;
int     next_null;
int     inull, stat;
int     imc;

static char  formstr[]   = "\0";

nimcols   = npixc[0];
nimrows   = npixc[1];
zbins[0]  = 256.0; zbins[1] = zbins[2] = 0.0;
sstep[0]  = sstep[1] = sstep[2] = 0.0;
sstart[0] = sstart[1] = sstart[2] = 0.0;

imc       = 0;
next_null = 0;
inull     = 1;

if (nnull == 0)
   next_null = MO_NXSUB * MO_NYSUB + 1;
else
   next_null = null_input[inull-1];

/* 
 Now loop over all subframes
 */
for ( i = 1; i <= MO_NXSUB * MO_NYSUB; i++ )
   {
   MO_INDICES(i, &j, &k, MO_NXSUB, MO_NYSUB, MO_CORNER, MO_RASTER, MO_ORDER);
   index[i-1] = i-1;
   c1[i-1]    = MYMAX (1, MYMIN (1 + (j - 1) * (MO_NCOLS - MO_NXOVERLAP), 
                     nimcols));
   c2[i-1]    = MYMIN (nimcols, MYMAX (1, c1[i-1] + MO_NCOLS - 1));
   r1[i-1]    = MYMAX (1, MYMIN (1 + (k - 1) * (MO_NROWS - MO_NYOVERLAP), 
                     nimrows));
   r2[i-1]    = MYMIN (nimrows, MYMAX (1, r1[i-1] + MO_NROWS - 1));

/*
 Set the index of each image in the image template
 and compute the median of the subraster.
 */
   if (i < next_null)
      {
      isnull[i-1] = imc;
      if (strlen(med_area) != (size_t) 0)
         {
	 snpix[0]  = *naxis++;
         for (nr = 0; nr < snpix[0]; nr++)
	     {
             snpix[1+nr]  = npix[3*imc + nr];
             sstart[nr]   = start[3*imc + nr];
             sstep[nr]    = step[3*imc + nr];
	     }
         Zstats(imnol[imc], med_area, snpix, zbins, formstr,
                    MO_DEFAULT);
         stat = SCKRDR("OUTPUTR",4,1,&iav,&median[i-1],&uni,&nulo); 
         }
      else
         median[i-1] = MO_INDEFR;
      imc++;
      }

   else
      {
      isnull[i-1] = -1;
      if (strlen(med_area) == (size_t) 0)
         median[i-1] = MO_INDEFR;
      else
         median[i-1] = MO_BLANK;
      if (nnull == 0 || ++inull > nnull)
         next_null = MO_NXSUB * MO_NYSUB + 1;
      else
         next_null = null_input[inull-1];
      }

   }
}

