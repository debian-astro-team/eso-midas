/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFICATION: subroutine sort
.KEYWORDS:       sorting heap sort
.PURPOSE:        to sort an array 
.ALGORITHM:      Heapsort algorithm 
.VERSION:        940221 RHW Modification for mosacing

 090713		last modif
--------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <midas_def.h>
#include <mutil.h> 

/*
 * here start the code of the function
 */

void sorti( n, ra )
int   n;
int   *ra;

{
  std_sorti(n,ra); 
}

/*

*/
void sortr( n, ra )
int   n;
float   *ra;

{
  std_sortf(n,ra); 
}

/*

*/
void sortd( n, da )
int     n;
double  *da;

{
  std_sortd(n,da);
}

/*

*/
void sortii( n, xval, yval )
int   n;
int   *xval, *yval;

{
  heapSortInt(n, xval, yval); 
}

/*

*/
void sortri( n, xval, yval )
int   n;
float *xval;
int   *yval;
{
  heapSortFloat(n,xval,yval); 
}







