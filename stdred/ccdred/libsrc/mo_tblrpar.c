/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  mo_tblrpar.c
.AUTHOR      R.H. Warmels IPG-ESO Garching
.KEYWORDS    mosaicing software
.LANGUAGE    C
.PURPOSE     Read the data base parameters from the table descriptor
.ENVIRONment MIDAS
             #include <ccd_def.h>      Symbols used by the ccd package
.VERSION     1.0     16-May-1995   creation

 090529		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <ccd_def.h>

#include <midas_def.h>


void MO_TBLRPAR(tbl, im_sec, med_sec)
int    tbl;                                /* table file number */
char   *im_sec;
char   *med_sec;

{
int    iac;
int    uni, nulo;
int    status;
int    ncols;
int    nrows;
int    nxsub;
int    nysub;
int    nxover;
int    nyover;

float  usrnul;

char   corner[2];
char   output[4];

status       = SCDRDC(tbl, "IM_SEC",1,1,40,&iac,im_sec,&uni,&nulo);
status       = SCDRDC(tbl, "MED_SEC",1,1,40,&iac,med_sec,&uni,&nulo);

status       = SCDRDI(tbl, "MO_NCOLS",1,1,&iac,&ncols,&uni,&nulo);
status       = SCDRDI(tbl, "MO_NROWS",1,1,&iac,&nrows,&uni,&nulo);
status       = SCDRDI(tbl, "MO_NXSUB",1,1,&iac,&nxsub,&uni,&nulo);
status       = SCDRDI(tbl, "MO_NYSUB",1,1,&iac,&nysub,&uni,&nulo);
status       = SCDRDI(tbl, "MO_NXOVERLAP",1,1,&iac,&nxover,&uni,&nulo);
status       = SCDRDI(tbl, "MO_NYOVERLAP",1,1,&iac,&nyover,&uni,&nulo);
MO_NCOLS     = ncols;
MO_NROWS     = nrows;
MO_NXSUB     = nxsub;
MO_NYSUB     = nysub;
MO_NXOVERLAP = nxover;
MO_NYOVERLAP = nyover;

status = SCDRDC(tbl, "MO_CORNER",1,1,2,&iac,corner,&uni,&nulo);
if  (strncmp(corner,MO_LL,2) == 0 )
    MO_CORNER = 1;
else if (strncmp(corner,MO_LR,2) == 0 )
    MO_CORNER = 2;
else if (strncmp(corner,MO_UL,2) == 0 )
    MO_CORNER = 3;
else if (strncmp(corner,MO_UR,2) == 0 )
    MO_CORNER = 4;
else
   SCETER(4,"*** FATAL: Unknown corner identification");  

status = SCDRDC(tbl, "MO_ORDER", 1, 1, 3, &iac, output, &uni, &nulo);
output[3] = '\0';		/* SCDRDC does not add '\0'  */
CGN_UPSTR(output);                   /* convert to upper case */
if (output[0] == 'C')
   strcpy(MO_ORDER,"COL");
else 
   strcpy(MO_ORDER,"ROW");

status = SCDRDC(tbl, "MO_RASTER", 1, 1, 3, &iac, output, &uni, &nulo);
output[3] = '\0';		/* SCDRDC does not add '\0'  */
CGN_UPSTR(output);                   /* convert to upper case */
if (output[0] == 'Y')
   strcpy(MO_RASTER,"YES");
else 
   strcpy(MO_RASTER,"NO");

status = SCDRDR(tbl, "MO_BLANK", 1, 1, &iac, &usrnul, &uni, &nulo);
MO_BLANK = usrnul;
}

