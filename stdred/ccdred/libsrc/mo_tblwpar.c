/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  mo_tblwpar.c
.AUTHOR      R.H. Warmels IPG-ESO Garching
.KEYWORDS    mosaicing software
.LANGUAGE    C
.PURPOSE     Write the data base parameters in the table descriptor
.ENVIRONment MIDAS
             #include <ccd_def.h>      Symbols used by the ccd package
.VERSION     1.0     16-May-1995   creation

 090526		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */

#include <midas_def.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <ccd_def.h>


void MO_TBLWPAR(tbl, im_sec, med_sec)
int    tbl;                                /* table file number */
char   *im_sec;
char   *med_sec;
{
int    uni;
int    status;
int    ncols;
int    nrows;
int    nxsub;
int    nysub;
int    nxover;
int    nyover;

float  usrnul;

char   corner[4];
char   raster[4];
char   order[4];

ncols  = MO_NCOLS;
nrows  = MO_NROWS;
nxsub  = MO_NXSUB;
nysub  = MO_NYSUB;
nxover = MO_NXOVERLAP;
nyover = MO_NYOVERLAP;

status = SCDWRC(tbl, "IM_SEC",1,im_sec,1,40,&uni);
status = SCDWRC(tbl, "MED_SEC",1,med_sec,1,40,&uni);
status = SCDWRI(tbl, "MO_NCOLS",&ncols,1,1,&uni);
status = SCDWRI(tbl, "MO_NROWS",&nrows,1,1,&uni);
status = SCDWRI(tbl, "MO_NXSUB",&nxsub,1,1,&uni);
status = SCDWRI(tbl, "MO_NYSUB",&nysub,1,1,&uni);
status = SCDWRI(tbl, "MO_NXOVERLAP",&nxover,1,1,&uni);
status = SCDWRI(tbl, "MO_NYOVERLAP",&nyover,1,1,&uni);

switch( MO_CORNER ) 
   {
   case 1:
      (void) strcpy(corner,MO_LL);
      status = SCDWRC(tbl, "MO_CORNER",1,corner,1,2,&uni);
      break;
   case 2:
      (void) strcpy(corner,MO_LR);
      status = SCDWRC(tbl, "MO_CORNER",1,corner,1,2,&uni);
      break;
   case 3:
      (void) strcpy(corner,MO_UL);
      status = SCDWRC(tbl, "MO_CORNER",1,corner,1,2,&uni);
      break;
   case 4:
      (void) strcpy(corner,MO_UR);
      status = SCDWRC(tbl, "MO_CORNER",1,corner,1,2,&uni);
      break;
    }

(void) strcpy(order,MO_ORDER);
status = SCDWRC(tbl, "MO_ORDER", 1, order, 1, 3, &uni);

(void) strcpy(raster,MO_RASTER);
status = SCDWRC(tbl, "MO_RASTER", 1, raster, 1, 3, &uni);

usrnul = MO_BLANK;
status = SCDWRR(tbl, "MO_BLANK", &usrnul, 1, 1, &uni);

}

