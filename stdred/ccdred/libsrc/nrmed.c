/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFICATION: subroutine nrmed.c
.KEYWORDS:       mosaicing, ccd package
.PURPOSE         Normalise corrections to a median value of zero.
       input:    int   n      number of corrections
                 float *a     corrections to be normalized, updated on output
       output    float amed   the median value used for normalisation
                 float *w     workspace
.ALGORITHM:      The routine normalises a set of corrections so that 
                 their median is zero by replacing each value a[i] by 
                 (a[i]-amed) where AMED is the median value. The value of 
                 AMED is also returned.
.VERSION:        940221 RHW Creation (original STARLINK)

 090903		last modif
--------------------------------------------------------------------------- */
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <math.h>
#include <midas_def.h>

/*
 * here start the code of the function
 */

extern void sortd(
#ifdef __STDC__
int     n,
double  *da 
#endif
);

void CCD_NRMED(n, a, amed)

int        n;
double     *a;
double     *amed;

{

int        i, imed;
float      denom;
double     *w;
char       *osmmget();

if (n == 1) 
   {
   *amed = *a;
   return;
   }

w = (double *) osmmget(n * sizeof( double ));

for (i=0; i<n; i++)
   w[i] = a[i];

if (n > 1) (void) sortd(n,w);

imed  = n/2 + 1;
denom = 2.0;
if (fmod((float) n, denom) == 0) 
   *amed = .5 * (w[imed] + w[imed-1]);
else
   *amed = w[imed];

for (i=0; i<n; i++)
   a[i] = a[i] - *amed;

osmmfree((char *) w);

}

void MO_MEDR(a,aindex,n,amed)

float      *a;
int        *aindex;
int        n;
float      *amed;

{

int        i, imed, m;
float      denom;
double     *w;
char       *osmmget();


w = (double *) osmmget(n * sizeof( double ));
m = 0;
for (i=0; i<n; i++)
   {
   if (aindex[i] > 0)
      {
      w[i] = a[i];
      m++;
      }
   }

if (m == 0)
   *amed = 0.0;
else if (m == 1) 
   *amed = *a;
else
   {
   (void) sortd(m,w);
   imed  = m/2 + 1;
   denom = 2.0;
   if (fmod((float) m, denom) == 0) 
      *amed = .5 * (w[imed] + w[imed-1]);
   else
   *amed = w[imed];
   }

osmmfree((char *) w);
}




