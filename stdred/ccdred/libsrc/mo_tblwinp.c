/*===========================================================================
  Copyright (C) 1995-2011 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  mo_tblwinp.c
.AUTHOR      R.H. Warmels IPG-ESO Garching
.KEYWORDS    mosaicing software
.LANGUAGE    C
.PURPOSE     Write the data related to the mosaic component into the 
             table
.ENVIRONment MIDAS
             #include <ccd_def.h>      Symbols used by the ccd package
.VERSION     1.0     16-May-1995   creation

 110930		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <ccd_def.h>
#include <midas_def.h>

void MO_TBLWINP(tid, catfil, frmcnt, framec, im_area, index, c1, c2, r1, r2,
                isnull, median, isub, verbose)
int    tid;
char   *catfil;
int    frmcnt;
char   *framec;
char   *im_area;
int    *index;
int    *c1, *c2;
int    *r1, *r2;
int    *isnull;
float  *median;
int    isub;
int    verbose;

{
char   frame[MAXFRM][61];
char   cframe[61];
char   output[61];
float  medsub;
char   line[84];
int    i;
 
int    status;
int    iframe;
int    irow;
int    iseq;

iseq = 0;
if (verbose == 1)
   {
    sprintf(line, 
            "Input subraster  Input area           Output area           Median      Correct");
    SCTPUT(line);
    }

for (i = 0; i < MO_NXSUB*MO_NYSUB; i++)
   {
   iframe = 0;
   irow   = i+1;
   if (isnull[i] < 0 ) 
      {
      status = TCEWRC(tid,irow,1,"nullimage");
      strcpy(cframe, "nullimage");
      }
   else
      {
      status = SCCGET(catfil,0,frame[iframe],output,&iseq);
      if (frame[iframe][0] != ' ')
         {
         status = TCEWRC(tid,irow,1,frame[iframe]);
         status = TCEWRC(tid,irow,2,im_area);
         strcpy(cframe, frame[iframe]);
         iframe++;
         }
      }   

   status = TCEWRI(tid, irow, 3, &c1[i]);
   status = TCEWRI(tid, irow, 4, &c2[i]);
   status = TCEWRI(tid, irow, 5, &r1[i]);
   status = TCEWRI(tid, irow, 6, &r2[i]);
   status = TCEWRR(tid, irow, 7, &median[i]);
   if (isub == 1)
      medsub = -median[i];
   else
      medsub = 0.0;
   status = TCEWRR(tid, irow, 8, &medsub);
   
   if (verbose == 1)
      {
      sprintf(line, "%-15s  %-15s [%-4d,%-4d:%-4d,%-4d]  %10.6g  %10.6g",
                   cframe,im_area,c1[i],r1[i],c2[i],r2[i],median[i],
                   medsub);
      SCTPUT(line);
      }
   }
}



