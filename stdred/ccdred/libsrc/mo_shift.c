/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  mo_shift.c
.AUTHOR      R.H. Warmels IPG-ESO Garching
.KEYWORDS    alignment software
.LANGUAGE    C
.PURPOSE     Routine to compute the shift for each subframe
.ENVIRONment MIDAS
             #include <ccd_def.h>      Symbols used by the ccd package
.VERSION     1.0     16-May-1995   creation

 090721		last  modif
------------------------------------------------------------*/
 
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <ccd_def.h>

extern int  SCDRDI();
extern void MO_INDICES();
void MO_MKSHIFT();



/*
 MO_SHIFTS -- Compute the input and output frame column limits and the 
              x and y shifts 
 */

void MO_SHIFTS(inim, outim, xrshifts, yrshifts, xcshifts, ycshifts, 
                       ic1, ic2, il1, il2, oc1, oc2, ol1, ol2, deltax, deltay)

int     inim;
int     outim;
float   *xrshifts;
float   *yrshifts;
float   *xcshifts;
float   *ycshifts;
int     *ic1;
int     *ic2;
int     *il1;
int     *il2;
int     *oc1;
int     *oc2;
int     *ol1;
int     *ol2;
float   *deltax;
float   *deltay;

{	
int     i, iav;
int     ideltax;
int     ideltay;
int     j;
int     k;
int     nimages;
int     nxsize, nysize;
int     nimcols;
int     nimlines;
int     npix[3];
int     c1ref, c2ref;
int     l1ref, l2ref;
int     nulo;
int     stat;
int     uni;

nxsize  = MO_NCOLS - MO_NXOVERLAP;
nysize  = MO_NROWS - MO_NYOVERLAP;
c1ref   = (MO_NXRSUB - 1) * nxsize + 1 + MO_XREF;
c2ref   = c1ref + MO_NCOLS - 1;
l1ref   = (MO_NYRSUB - 1) * nysize + 1 + MO_YREF;
l2ref   = l1ref + MO_NROWS - 1;
nimages = MO_NXSUB * MO_NYSUB;

for ( i = 1; i <= nimages; i++)
   {
/*
 Compute the indices of each subraster.
 */
   MO_INDICES(i, &j, &k, MO_NXSUB, MO_NYSUB, MO_CORNER, MO_RASTER, MO_ORDER);
/*
 Compute the indices of the input subraster.
 */
    stat       = SCDRDI(inim,"NPIX",1,3,&iav,npix,&uni,&nulo);
    nimcols    = npix[0];
    nimlines   = npix[1];
    ic1[i-1]   = MYMAX(1, MYMIN (1 + (j - 1) * nxsize, nimcols)); 
    ic2[i-1]   = MYMIN(nimcols, MYMAX(1, ic1[i-1] + MO_NCOLS - 1));
    il1[i-1]   = MYMAX(1, MYMIN(1 + (k - 1) * nysize, nimlines));
    il2[i-1]   = MYMIN(nimlines, MYMAX(1, il1[i-1] + MO_NROWS - 1));

/*
 Compute the shift relative to the input subraster.
 */
    MO_MKSHIFT(xrshifts, yrshifts, xcshifts, ycshifts,
               MO_NXSUB, MO_NYSUB, j, k, MO_NXRSUB,
               MO_NYRSUB, MO_ORDER, &deltax[i-1], &deltay[i-1]);
    ideltax = NINT (deltax[i-1]);
    ideltay = NINT (deltay[i-1]);

/*
 Get the output buffer.
 */
    oc1[i-1] = c1ref + (j - MO_NXRSUB) * MO_NCOLS + ideltax;
    oc2[i-1] = c2ref + (j - MO_NXRSUB) * MO_NCOLS + ideltax;
    ol1[i-1] = l1ref + (k - MO_NYRSUB) * MO_NROWS + ideltay;
    ol2[i-1] = l2ref + (k - MO_NYRSUB) * MO_NROWS + ideltay;
  }  
}

/*
 MO_FSHIFTS -- Compute the input and output columns limits
 */

void MO_FSHIFTS(inim, outim, deltax, deltay, 
                        ic1, ic2, il1, il2, oc1, oc2, ol1, ol2)

int     inim;
int     outim;
float   *deltax;
float   *deltay;
int     *ic1;
int     *ic2;
int     *il1;
int     *il2;
int     *oc1;
int     *oc2;
int     *ol1;
int     *ol2;

{	
int     i, iav;
int     j;
int     k;
int     nimages;
int     nimcols;
int     nimlines;
int     npix[3];
int     nxsize, nysize;
int     c1ref, c2ref;
int     l1ref, l2ref;
int     ideltax;
int     ideltay;
int     stat;
int     uni;
int     nulo;

nxsize  = MO_NCOLS - MO_NXOVERLAP;
nysize  = MO_NROWS - MO_NYOVERLAP;
c1ref   = (MO_NXRSUB - 1) * nxsize + 1 + MO_XREF;
c2ref   = c1ref + MO_NCOLS - 1;
l1ref   = (MO_NYRSUB - 1) * nysize + 1 + MO_YREF;
l2ref   = l1ref + MO_NROWS - 1;
nimages = MO_NXSUB * MO_NYSUB;

for ( i = 1; i <= nimages; i++)
   {
/*
 Compute the indices of each subraster.
 */
   MO_INDICES(i, &j, &k, MO_NXSUB, MO_NYSUB, MO_CORNER, MO_RASTER, MO_ORDER);
/*
 Compute the indices of the input subraster.
 */
    stat       = SCDRDI(inim,"NPIX",1,3,&iav,npix,&uni,&nulo);
    nimcols    = npix[0];
    nimlines   = npix[1];
    ic1[i-1]   = MYMAX (1, MYMIN (1 + (j - 1) * nxsize, nimcols)); 
    ic2[i-1]   = MYMIN (nimcols, MYMAX (1, ic1[i-1] + MO_NCOLS - 1));
    il1[i-1]   = MYMAX (1, MYMIN (1 + (k - 1) * nysize, nimlines));
    il2[i-1]   = MYMIN (nimlines, MYMAX (1, il1[i-1] + MO_NROWS - 1));

/*
 Compute the shift relative to the input subraster.
 */
    ideltax = NINT (deltax[i-1]);
    ideltay = NINT (deltay[i-1]);

/*
 Get the output buffer.
 */
    oc1[i-1] = c1ref + (j - MO_NXRSUB) * MO_NCOLS + ideltax;
    oc2[i-1] = c2ref + (j - MO_NXRSUB) * MO_NCOLS + ideltax;
    ol1[i-1] = l1ref + (k - MO_NYRSUB) * MO_NROWS + ideltay;
    ol2[i-1] = l2ref + (k - MO_NYRSUB) * MO_NROWS + ideltay;
  }  
}  



/*
 MO_MKSHIFT -- Routine to compute the total shift for each subframe. 
*/

void MO_MKSHIFT(xrshift, yrshift, xcshift, ycshift, nxsub, nysub,
           xsubindex, ysubindex, nxrsub, nyrsub, order, deltax, deltay)

float    (*xrshift)[MAXFRM];    /* x row shifts */
float    (*yrshift)[MAXFRM];    /* y row shifts */
float    (*xcshift)[MAXFRM];    /* x column shifts */
float    (*ycshift)[MAXFRM];    /* y column shifts */
int      nxsub;                 /* number of subraster in the x direction */
int      nysub;                 /* number of subrasters in the y direction */
int      xsubindex;             /* x index subraster */
int      ysubindex;             /* y index subraster */
int      nxrsub;                /* x index of reference subraster */
int      nyrsub;                /* y index of reference subraster */
char     *order;                /* row or column order */
float    *deltax;               /* total x shift */ 
float    *deltay;               /* total y shift */

{
int      j;

*deltax = 0.0;
*deltay = 0.0;

if (strcmp(order,MO_COLUMN) == 0)
   {
   if (ysubindex < nyrsub)
      for (j = ysubindex; j <= nyrsub - 1; j++)
          {
          *deltax = *deltax + xcshift[xsubindex-1][j-1];
          *deltay = *deltay + ycshift[xsubindex-1][j-1];
          }

   else if (ysubindex > nyrsub)
      for (j = nyrsub + 1; j <= ysubindex; j++)
          {
          *deltax = *deltax + xcshift[xsubindex-1][j-1];
          *deltay = *deltay + ycshift[xsubindex-1][j-1];
          }
   
   if (xsubindex < nxrsub)
      for (j = xsubindex; j <= nxrsub - 1; j++)
          {
          *deltax = *deltax + xrshift[j-1][nyrsub-1];
          *deltay = *deltay + yrshift[j-1][nyrsub-1];
          }

   else if (xsubindex > nxrsub)
      for (j = nxrsub + 1; j <= xsubindex; j++)
          {
          *deltax = *deltax + xrshift[j-1][nyrsub-1];
          *deltay = *deltay + yrshift[j-1][nyrsub-1];
          }
   }

else
   {
   if (xsubindex < nxrsub)
      for (j = xsubindex; j <= nxrsub - 1; j++)
          {
          *deltax = *deltax + xrshift[j-1][ysubindex-1];
          *deltay = *deltay + yrshift[j-1][ysubindex-1];
          }

   else if (xsubindex > nxrsub)
      for (j = nxrsub + 1; j <= xsubindex; j++)
          {
          *deltax = *deltax + xrshift[j-1][ysubindex-1];
          *deltay = *deltay + yrshift[j-1][ysubindex-1];
          }
   
   if (ysubindex < nyrsub)
      for (j = ysubindex; j <= nyrsub - 1; j++)
          {
          *deltax = *deltax + xcshift[nxrsub-1][j-1];
          *deltay = *deltay + ycshift[nxrsub-1][j-1];
          }

   else if (ysubindex > nyrsub)
      for (j = nyrsub + 1; j <= ysubindex; j++)
          {
          *deltax = *deltax + xcshift[nxrsub-1][j-1];
          *deltay = *deltay + ycshift[nxrsub-1][j-1];
          }
   }
}


