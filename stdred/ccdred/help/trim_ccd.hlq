% @(#)trim_ccd.hlq	19.1 (ESO_SDAG) 02/25/03 14:15:05
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      trim_ccd.hlq
%.AUTHOR     RHW, IPG/ESO
%.KEYWORDS   MIDAS, ccd package, TRIM/CCD
%.PURPOSE    On-line help file for the command: TRIM/CCD
%.VERSION    930305 RHW Created  
%---------------------------------------------------------------------
\se
SECTION./CCD
\es\co
TRIM/CCD                                                        25-Mar-1993 RHW
\oc\su
TRIM/CCD [in_fram] [out_fram] [im_sec] [del_flg]
	Extract the useful data from the ccd frame. 
\us\pu
Purpose:    
	Extract the useful data from the ccd frame. 
\up\sy
Syntax:     
        TRIM/CCD [in_fram] [out_fram] [im_sec] [del_flg]
\ys\pa 
        in_fram =   input frame to be trimmed; default is the name stored in
                    the keyword CCD_IN.
\ap\pa
        out_frame = resulting output frame; no default.

        im_sec =    image section to be extracted. Default is the section
                    stored in the keyword IM_SEC. 
\ap\pa 
        del_flg =   delete flag. Default is `NO'.
\ap\no
Note: 
        The command will not try to trim the input frame if that has already 
        been done. The descriptor CCDSTAT in the output frame is updated to 
        indicate that the frame has been trimmed.
\on\see
See also:   
        HELP/CCD, REDUCE/CCD, EXTRACT/IMAGE
\ees\exs
Examples:
\ex
        TRIM/CCD fieldin fieldext ? y
        The input frame fiedlin will be trimmed. The output frame fieldextr
        will contain the output area, stored in the keyword IM_SEC. After
        extraction the input frame will be deleted.
\xe \sxe


