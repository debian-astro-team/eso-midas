% @(#)flat_ccd.hlq	19.1 (ESO-SDAG) 02/25/03 14:15:00
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      flat_ccd.hlq
%.AUTHOR     RHW, IPG/ESO
%.KEYWORDS   MIDAS, ccd package, FLAT/CCD
%.PURPOSE    On-line help file for the command: FLAT/CCD
%.VERSION    930329 RHW created
%---------------------------------------------------------------------
\se
SECTION./CCD
\es\co
FLAT/CCD                                                        29-Mar-1993 RHW
\oc\su
FLAT/CCD [in_fram] [out_fram] [ff_fram]
	Do a flat field correction of the input frame
\us\pu
Purpose:    
	Do a flat field correction of the input frame
\up\sy
Syntax:     
	FLAT/CCD [in_fram] [out_fram] [ff_fram]
\ys\pa
	in_fram =  input frame to be corrected; default is the name stored in
                   the keyword CCD_IN.
\ap\pa
	out_fram = output frame resulting from the devision of the input frame
                   by a scaled flat field; no default.
\ap\pa
	ff_fram  = flat frame to be used; default is the frame stored in the 
                   keyword SC_FFFRM.
\ap\no
Note:   
        The command devides the input frame by the flat field frame according
        to the formula:\\
                       out_fram = in_fram/(ff_fram/mean)\\
        where mean is the value of the descriptor `MEAN' in the flat field 
        frame. This descriptor is created by the command COMBINE/CCD. Its 
        value is computed over the useful image defined in the keyword 
        'IM_SEC'. In case the descriptor does not exist, the mean intensity
        over the flat field using the area in the key `IM_SEC' will be used
        as the scaling factor. In case the descriptor contains a number smaller
        or equal to zero the value 1.0 will be used.\\

        In case the flat frame does not exit, a fatal error will be issued. In
        such case you may generate the flat frame using COMBINE/CCD.\\

        The command exists if the flat fielding has already been done.
	After the command has finished the descriptor CCDSTAT in the output 
        frame is updated to indicated that the flat field correction  offset
        has been applied.
\on\see
See also:   
	SET/CCD, SHOW/CCD, REDUCE/CCD, COMPUTE/IMAGE, COMBINE/CCD
\ees\exs
Examples:
\ex
	FLAT/CCD susi0097dkcor susi97ffcor susiflat
	Devide susu0097dkcor by the flat field `susiflat'. The output frame
        is susi97ffcor.
\xe \sxe



