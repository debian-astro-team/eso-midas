% @(#)skycorccd.hlq	19.1 (ESO-IPG) 02/25/03 14:15:04
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      skycorccd.hlq
%.AUTHOR     RHW, IPG/ESO
%.KEYWORDS   MIDAS, ccd package, SKYCOR/CCD
%.PURPOSE    On-line help file for the command: SKYCOR/CCD
%.VERSION    930827 RHW Documentation created
%---------------------------------------------------------------------
\se
SECTION./CCD
\es\co
SKYCOR/CCD                                                     25-Mar-1993 RHW
\oc\su
SKYCOR/CCD [in_spec] [out_frm] [xboxmn,xboxmx] [yboxmn,yboxmx] [clip] 
           [lowsig,higsig]
	Make sky illumination correction frame(s)
\us\pu
Purpose: 
	Make sky illumination correction frame(s)
\up\sy
Syntax:     
        SKYCOR/CCD [in_spec] [out_frm] [xboxmn,xboxmx] [yboxmn,yboxmx]
                   [clip] [lowsig,higsig]
\ys\pa
        in_spec = single flat frame or MIDAS CCD association table (with the 
                  extension .tbl) containing the names of the flat frame(s)
                  in the column :SKY. Default input is taken from the keyword 
                  CCD_IN.
\ap\pa
        out_frm = name of the output illumination frame. Default output is the
                  name of the input table with extension "_ill". Only used in
                  case of single frame input for the first parameter. In 
                  case the input is a MIDAS table the name(s) of the 
                  output frame(s) will have the same name as the sky frame
                  field except for the extension '_ill' (see below).
\ap\pa  
        xboxmn,xboxmx = minimum and maximum smoothing box size along the x 
                  axes.  The minimum box size is used at the edges and grows 
                  to the maximum size in the middle of the image.  This 
                  allows the smoothed image to better represent gradients 
                  at the edge of the image. If a size is less then 1 it is 
                  interpreted as a fraction of the image size.  If a size is
                  greater than or equal to 1 then it is the box size in 
                  pixels.  A size greater than the size of image selects a 
                  box equal to the size of the image. Default values are taken
                  from the keyword IL_XBOX.
\ap\pa
        yboxmn,yboxmx = see above. Default values are taken from IL_YBOX.
\ap\pa
        clip =    Clean the input frame(s) of objects? If yes then a clipping 
                  algorithm is used to detect and exclude objects from the 
                  smoothing. Default taken from IL_CLIP.
\ap\pa
        lowsig,higgsig = sigma clipping thresholds above and below the 
                  smoothed illumination. Default values are taken from
                  the keyword IL_SIGMA. The keyword is only read in case 
                  clip=yes.
\ap\no
Note:   
        In case the input is an association table the command will check for
        the existence of the sky frames. Sky frames that do not exit will be 
        created first, provided the association table contain the sky column. 
        Other relevant calibration frames for processing the sky frames, 
        including flat fields will be taken from the corresponding calibration
        columns belonging to the same science frame. Output illumination 
        frame(s) will have the same name as the original sky frame expanded
        with `_ill'. 

        In the case of single frame input and all calibration frames input 
        will be taken from the SC_ keywords and must exist.\\

        The command operates very similar to the command REDUCE/CCD, which
        the exception that the command not check for the exposure type of 
        the input frame. Master frames to be used to calibrate the input 
        frame first are taken for the SC_ keywords (in case of single frame 
        input), or from the reduction table in case of table input. For 
        details see the command REDUCE/CCD. \\

        The input frame are automatically processed, including flat field,
        before computing the illumination. The input frame are generally 
        blank sky calibration frames which have the same illumination and 
        instrumental effects as the object observations. Science frames 
        may be used but removal of the objects may not be very good; 
        particularly large, bright objects.\\

        The illumination frame is produced by heavily smoothing the 
        calibrated input frames using a moving "boxcar" average. The effects
        of objects in the frames may be minimized by using a sigma clipping 
        algorithm to detect and exclude the objects from the average. The 
        output illumination frame can used by REDUCE/CCD to remove the 
        illumination pattern in the science frames.\\

        The IM_SEC keyword may be used to assign a region of interest for which
        the correction has to be done.\\

        For more information see the CCD chapter in Volume B of the User's 
        manual.
\on\see
See also:   
        SET/CCD, SHOW/CCD, HELP/CCD, REDUCE/CCD, SKYFLAT/CCD
\ees\exs
Examples:
\ex
        SKYCOR/CCD ccd_asstbl.tbl
        Reduce all sky frames (including flat fielding) in the the table 
        ccd_asstbl.tbl in the column :SKY. All master calibration frames 
        will be obtained from the same table. Do the calibration according 
        to the keyword settings. After the calibrations are done create the 
        illumination frames, using the `IL_' keyword settings.
\xe\ex
        SKYCOR/CCD m100rraw_sk m100rraw_il
        Start the ccd reduction sequence for the frame m100rraw_sk. The output
        illumination frame will be m100rraw_il. In case only dark and flat 
        field correction is to be applied the names of these two calibration 
        frames  will be obtained from the keyword SC_DKFRM and SC_FFFRM, 
        respectively.
\xe\sxe

