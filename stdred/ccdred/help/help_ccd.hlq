% @(#)help_ccd.hlq	19.1 (ESO-IPG) 02/25/03 14:15:01
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c) 1993 European Southern Observatory
%.IDENT      help_ccd.hlq
%.AUTHOR     Rein H. Warmels, IPG/ESO
%.KEYWORDS   MIDAS, help files, HELP/CCD
%.PURPOSE    On-line help file for the command: HELP/CCD
%.VERSION    920603 RHW Created
%.VERSION    931101 RHW Released
%----------------------------------------------------------------
\se
SECTION./CCD
\es\co
HELP/CCD						       1-Nov-1993  RHW
\oc\su
HELP/CCD [keyword]
	show the parameter setting of the current CCD session
\us\pu
Purpose:    
        Show the parameter setting of the current CCD session
\up\sy
Syntax:     
        HELP/CCD  [key_string]
\ys\pa
        key_string = usually, the complete name of a CCD keyword. However also
                     parts of keyword names can be given as input.\\
                     If no parameter is entered, a general help message is
                     displayed.
\ap\no
Note:       
        Both the current value(s) of the keyword and its original default 
        content are displayed. 

\on\see
See also:   
        SHOW/CCD, SET/CCD, READ/KEY
\ees\exs
Examples:
\ex
        HELP/CCD OV_REJEC
        Display the contents of the keyword OV_REJEC
\xe\ex
        HELP/CCD BS
        Display information about all keywords that contain the string BS.
\xe\ex
        HELP/CCD
        Display the general command information of the CCD package
\xe \sxe
