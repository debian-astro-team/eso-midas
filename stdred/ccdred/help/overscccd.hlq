% @(#)overscccd.hlq	19.1 (ESO-SDAG) 02/25/03 14:15:03
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      overscccd.hlq
%.AUTHOR     RHW, IPG/ESO
%.KEYWORDS   MIDAS, ccd package, OVERSCAN/CCD
%.PURPOSE    On-line help file for the command: OVERSCAN/CCD
%.VERSION    920726 RHW created
%.VERSION    920726 RHW created
%---------------------------------------------------------------------
\se
SECTION./CCD
\es\co
OVERSCAN/CCD                                                    25-Mar-1993 RHW
\oc\su
OVERSCAN/CCD [in_fram] [out_fram] [sc_area] [mode]
	Correct the input frame for the bias offset in the overscan region
\us\pu
Purpose:    
	Correct the input frame for the bias offset determined from the overscan 
        region region
\up\sy
Syntax:     
	OVERSCAN/CCD [in_fram] [out_fram] [sc_area] [direct] [mode]
\ys\pa
        in_fram =  input frame to be corrected. Input frame can either be a 
                   sky frame or a science frame. To determine the exposure 
                   type of the input frame it should have a valid descriptor
                   containing a valid exposure type. Default is the name stored
                   in the keyword CCD_IN; see below for details.
\ap\pa
	out_fram = (a) output frame resulting from subtracting the bias frame 
                       from the input frame. No default.
                   (b) `tbname,TABLE' will only produce the fit table used to
                       create the correction frame from the overscan region.
                       No subtraction from the input frame will be done!
\ap\pa
	sc_area  = overscan area. Default is the area defined in the keyword
                   OV_SEC. 
\ap\pa
        direct   = readout direction of the CCD: `column' or `row'; default 
                   is the setting stored in the CCD keyword DIRECT.
\ap\pa
        mode     = mode of operation, interactive (I) or automatic (A). Default
                   is the current setting (keyword OV_IMODE)
\ap\no
Note:       
        Depending on the keyword DIRECT, the command averages the rows in the
        overscan region (DIRECT="col") or averages the columns (DIRECT="row").
 
        The area over which the average is calculated is obtained from the CCD 
        keyword OV_SEC. In case the keyword DIRECT is not filled with `row' or 
        `col', the average is taken row-wise if the area is large in y than in 
        x; else the average is taken column-wise.\\

        The command can correct all types of frames, including a bias itself.
        The default mode or operation is taken from the keyword OV_IMODE.
        Except for the overscan area itself, all fit parameters are taken from 
        the keyword setting. These keywords and their meaning are:\\
           OV_FUNCT\ \ lin/pol - linear or polynomial function;\\
           OV_ORDER\ \ numb    - the order of the polynomial function;\\
           OV_AVER \ \ numb    - box smooth with a width of `number' pixels;\\ 
           OV_ITER \ \ numb    - maximum number of iterations;\\
           OV_REJEC\ \ numb    - sigma rejection factor.\\

        After the overscan average is made the result is fitted, either 
        automatically, or interactively using the graphics cursor. In case 
        of interactive overscan fitting the command will first use the current
        keyword setting. The result will be displayed on the graphics window. 
        The cursor will appear to select the range to be fitted. The fit will 
        be overplotted and the user will be asked if this is satisfactory. 
        In case of `N' all fitted parameters can be modified and a new fit 
        loop will start.\\

        After the overscan vector is determined an overscan frame is created. 
        This frame will be used to correct the input frame for the overscan
        bias. In case the overscan correction is a constant (zero order 
        approximation: the mean of the offset in the overscan region) this 
        constant will be used.\\
 
        The command will not try to correct for the overscan offset if that 
        has already been done. The descriptor CCDSTAT in the output frame 
        is updated to indicate that the frame has been trimmed.
\on\see
See also:   
	SET/CCD, SHOW/CCD, REDUCE/CCD, REGRESSION/LINEAR, \\
        REGRESSION/POLYNOMIAL
\ees\exs
Examples:
\ex
	OVERSCAN/CCD susi0097 susi0097sccor ? col
	Average the overscan columns using the keyword setting OV_xxx, and the
        overscan area defined in the keyword OV_SEC. The readout direction is
        column wise. Subtract the overscan fit from the susi0097 to obtain the
        bias correction output frame susi0097sccor.
\xe \sxe









