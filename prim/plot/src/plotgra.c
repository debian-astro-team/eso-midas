/*===========================================================================
  Copyright (C) 1993-2011 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  PLOTGRA
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, bulk data frame, one-dimensional plotting
.LANGUAGE    C
.PURPOSE     Plot or overplot a gray scale map
  in/output: IN_A/C/1/60   = input frame
             IN_B/C/1/60   = coord_string
             P3/R/1/4      = scales in X and Y and offset X and Y
                             (default is auto scaling to device filling)
             INPUTC/C/1/40  = gray scales
             INPUTC/C/41/40 = options: option1[,option2[,option3]]
                  LOG  = logarythmic greyness     (default: LINear)
                  NEG  = inverse grey scale       (default: original values)
                  ABS  = abs. values are taken    (default: not absolute)
                  STAP = greyness increases stepw.(default: CONtinuous)
             INPUTI/I/1/1   = size of the smoothing box
             INPUTR/R/3/1   = greyness of the plot (0.0 --> 1.0)

.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      General symbols for Plot routines

.VERSION     1.0     20-Aug-1993   FORTRAN --> ANSI-C    RvH

 110930		last modif

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

/*
 * define some macros and constants
 */
#include <midas_def.h>
#include <plot_def.h>

#define MAXLEV		50
#define MAXPIX          512    /* max frame dimension (X,Y) accessed at once */
#define MAXSIZ          (MAXPIX * MAXPIX)

/*
 * here starts the code of the function
 */
int main()
{
register int ii;
int     actvals, chunks, imf, knul, last_row, naxis, size, stat, unit, 
        ndum[PLDIM2], npix[PLDIM2], sublo[PLDIM2], subhi[PLDIM2];

float   amin, amax, greynes, *p_img, nrpix[2], area[4], image[4], wcfram[12],
        glevl[MAXLEV];

double  start[PLDIM2], step[PLDIM2];

char    cmnd[21], ident[33], cunit[49], name[61], input[82], buff[81], 
        *label[4], *cbuff;
/*
 * initialised variables
 */
char  *err_usrin = "*** FATAL: error detected in USRINP",
      *err_1dim = "*** FATAL: frame has only one dimension",
      *err_1lev = "*** WARNING: only one level given, will use min. and max. in frame",
      *err_flat = "*** WARNING: zero dynamic range in data at %13.8g",
      *err_coord = "*** FATAL: invalid coordinate input ...",
      *err_xcuts = "*** FATAL: range in x has no overlap with current graph abscissa - NO PLOT",
      *err_ycuts = "*** FATAL: range in y has no overlap with current graph abscissa - NO PLOT";

int   access =  0,                        /* parameter for PCOPEN: plot mode */
      plmode = -1,                     /* plot mode taken from keyword PMODE */
      nlevl  =  0,                     /* Default number of contours is zero */
      ismoot =  1;                        /* Default no smooting of the data */
static int  options[] = { FALSE, FALSE, FALSE, FALSE, FALSE };
                                       /* The defaults for gray_options are: */
                                       /*   0) lineair increasing grayness   */
                                       /*   1) cont. increasing gray scales  */
                                       /*   2) leave signs unchanged         */
                                       /*   3) do NOT take absolute values   */
                                       /*   4) intensities above highest     */
                                       /*      contour, get the grayness     */
                                       /*      of the highest level          */

static char *axis[PLDIM2] = { "MANU", "MANU" };

/*
 * allocate memory for different character pointers and initialise a few
 */
for ( ii = 0; ii < 4; ii++ ) label[ii] = osmmget(81);

(void) strcpy( label[0], "Position (" );
(void) strcpy( label[1], "Position (" );
(void) strcpy( label[2], "Image: " );
(void) strcpy( label[3], "Ident: " );

/*
 * start of executable code
 */
(void) SCSPRO( "PLTGRA" );                   /*contact with the MIDAS monitor*/

/*
 * plot or overplot mode
 */
(void) SCKGETC( "MID$CMND", 1, 20, &actvals, cmnd );
if ( *cmnd == 'O' ) access = 1;

/*
 * find file name and read header information
 */
(void) SCKGETC( "IN_A", 1, 60, &actvals, name );
(void) SCFOPN( name, D_R4_FORMAT, 0, F_IMA_TYPE, &imf );
(void) SCDRDI( imf, "NAXIS", 1, 1, &actvals, &naxis, &unit, &knul );
(void) SCDRDI( imf, "NPIX" , 1, PLDIM2, &actvals, npix , &unit, &knul );

/*
 * check frame parameters
 */
if ( naxis < 2 || (npix[0] == 1 || npix[1] == 1) ) SCETER( 1, err_1dim );

/*
 * read rest descriptor
 */
(void) SCDRDD( imf, "START", 1, PLDIM2, &actvals, start, &unit, &knul );
(void) SCDRDD( imf, "STEP" , 1, PLDIM2, &actvals, step , &unit, &knul );
(void) SCDGETC( imf, "IDENT", 1, 32, &actvals, ident );
(void) SCDGETC( imf, "CUNIT", 1, 48, &actvals, cunit );

/*
 * Get the manual setting for the axes
 */
PCKRDR( "XAXIS", 4, &actvals, wcfram );
PCKRDR( "YAXIS", 4, &actvals, wcfram+FOR_Y );

/*
 * read window coordinates and take action
 */
(void) SCKGETC( "IN_B", 1, 60, &actvals, input );

if ( *input == 'm' || *input == 'M' )                      /* manual scaling */
   { BOXWTP( wcfram,       npix[0], start[0], step[0], image );
     BOXWTP( wcfram+FOR_Y, npix[1], start[1], step[1], image+2 );
   }
else
   { if ( *input == 'c' || *input == 'C' )                  /* display input */
        { (void) SCKRDR( "OUTPUTR", 10, 1, &actvals, image, &unit, &knul);
          (void) SCKRDR( "OUTPUTR", 11, 1, &actvals, image+2, &unit, &knul); 
          (void) SCKRDR( "OUTPUTR", 15, 1, &actvals, image+1, &unit, &knul);
          (void) SCKRDR( "OUTPUTR", 16, 1, &actvals, image+3, &unit, &knul);
        }
     else                                               /* automatic scaling */
        { stat = Convcoo(1,imf,input,PLDIM2,ndum,sublo,subhi);
          if ( stat != ERR_NORMAL ) SCETER( 2, err_coord );
          image[0] = sublo[0] + 1;
          image[1] = subhi[0] + 1;
          image[2] = sublo[1] + 1;
          image[3] = subhi[1] + 1;
        }
   }
BOXPTW( image,   npix[0], start[0], step[0], area );
BOXPTW( image+2, npix[1], start[1], step[1], area+2 );

PCKWRR( "PIXEL", 4, image );

if ( access == 0 )
   { 
/*
 * get size of frame along X-axis
 */
     if ( fabs( *wcfram ) < PLT_EPS && fabs( *(wcfram+1) ) < PLT_EPS )
        { axis[0] = "AUTO";
          wcfram[0] = area[0];
          wcfram[1] = area[1]; 
          wcfram[2] = wcfram[3] = 0.0;
        }
/*
 * get size of frame along Y-axis
 */
     if ( fabs( *(wcfram+FOR_Y) ) < PLT_EPS 
          && fabs( *(wcfram+FOR_Y+1) ) < PLT_EPS )
        { axis[1] = "AUTO";
          wcfram[FOR_Y]   = area[2];
          wcfram[FOR_Y+1] = area[3]; 
        }
     GETFRM( axis[0], wcfram );
     GETFRM( axis[1], wcfram + FOR_Y );
     PCKWRR( "XWNDL", 4, wcfram );
     PCKWRR( "YWNDL", 4, wcfram+FOR_Y );
   }
else                                                         /* overplot mode*/
   { PCKRDR( "XWNDL", 4, &actvals, wcfram );
     PCKRDR( "YWNDL", 4, &actvals, wcfram+FOR_Y );
/*
 * does overplot data fall within plotted frame? 
 */
     amin = MYMIN( *wcfram, *(wcfram + 1) );
     amax = MYMAX( *wcfram, *(wcfram + 1) );
     if ( ( MYMAX( area[0], area[1] ) < amin ) ||
          ( MYMIN( area[0], area[1] ) > amax ) )
        SCETER( 3, err_xcuts );

     amin = MYMIN( *(wcfram + FOR_Y), *(wcfram + FOR_Y+1) );
     amax = MYMAX( *(wcfram + FOR_Y), *(wcfram + FOR_Y+1) );
     if ( ( MYMAX( area[2], area[3] ) < amin ) ||
          ( MYMIN( area[2], area[3] ) > amax ) )
        SCETER( 4, err_ycuts );
   }
/*
 * store min and max of frame
 */
(void) SCDRDR( imf, "LHCUTS", 1, 4, &actvals, wcfram+FOR_Z, &unit, &knul);
if ( wcfram[FOR_Z] >= wcfram[FOR_Z+1] )
   { if ( wcfram[FOR_Z+2] >= wcfram[FOR_Z+3] )
        { (void) sprintf( buff, err_flat, wcfram[FOR_Z] );
          SCTPUT( buff );
        }
     else
        { *(wcfram+FOR_Z)   = *(wcfram+FOR_Z+2);
          *(wcfram+FOR_Z+1) = *(wcfram+FOR_Z+3);
        } 
   }
PCKWRR( "ZWNDL", 2, wcfram+FOR_Z );

/*
 * get the levels and smoothing parameter
 */
(void) SCKGETC( "INPUTC", 1, 40, &actvals, input );
CGN_LOWSTR( input );
if ( strncmp( input, "def", 3 ) != 0 )
   { cbuff = (char *) glevl;
     if ( USRINP( 'r', input, MAXLEV, cbuff, &nlevl ) != ERR_NORMAL )
        SCETER( 5, err_usrin );
/*
 * sort them
 */
     SORLEV( nlevl-1, glevl );
     if ( nlevl > 2 ) options[1] = TRUE;
   }
else
   { glevl[0] = wcfram[FOR_Z];
     glevl[1] = wcfram[FOR_Z+1];
     nlevl    = 2;
   }
/*
 * check for more than one input level
 */
if ( nlevl < 1 )
   { SCTPUT( err_1lev );
     glevl[0] = wcfram[FOR_Z];
     glevl[1] = wcfram[FOR_Z+1];
     nlevl    = 2;
   }
/*
 * get the smooting parameter
 */
(void) SCKRDI( "INPUTI", 1, 1, &actvals, &ismoot, &unit, &knul);

/*
 * get the greyness
 */
(void) SCKRDR( "INPUTR", 3, 1, &actvals, &greynes, &unit, &knul);
if ( greynes < 0.0 || greynes > 1.0 ) greynes = 1.0;

/*
 * get options
 */
(void) SCKGETC( "INPUTC", 41, 80, &actvals, input );
CGN_LOWSTR( input );

if ( strin( input, "log" ) )  options[0] = TRUE;
if ( strin( input, "step" ) ) options[1] = TRUE;
if ( strin( input, "neg" ) )  options[2] = TRUE;
if ( strin( input, "abs" ) )  options[3] = TRUE;
if ( strin( input, "cut" ) )  options[4] = TRUE;

/*
 * setup graphic device according to MIDAS settings
 */
PCOPEN( " ", " ", access, &plmode );

/*
 * determine number of pixels and number of chunks
 */
nrpix[0] = (int) fabs( image[1] - image[0] ) + 1;
nrpix[1] = (int) fabs( image[3] - image[2] ) + 1;
last_row = MYMAX( image[2], image[3]);
chunks   = (int) ceil( (double) nrpix[0] * nrpix[1] / MAXSIZ );
nrpix[1] = (int) ceil( (double) nrpix[1] / chunks );

/*
 * allocate virtual memory and scratch space
 */
size  = nrpix[0] * nrpix[1];
p_img = (float *) osmmget( size * sizeof( float ));

for ( ii = 0; ii < chunks; ii++ )
    { if ( image[3] > image[2] )                        /* size of chunk p.c.*/
         { if ( ii > 0 ) image[2] += nrpix[1] - 1.0;
           image[3] = MYMIN( last_row, image[2] + nrpix[1] - 1 );
         }
      else
         { if ( ii > 0 ) image[3] += nrpix[1] - 1.0;
           image[2] = MYMIN( last_row, image[3] + nrpix[1] - 1 );
         }
/*
 * get size of chunk in w.c.
 */
      BOXPTW( image+2, npix[1], start[1], step[1], area+2 );      
/*
 * extract chunck from original frame
 */
      GETDAT( imf, MAXSIZ, npix, image, ismoot, p_img );
/*
 * do it!!
 */
      PLGRA( p_img, image, area, step, glevl, nlevl, options, greynes );
/*
 * updating for the next loop
 */
      nrpix[1] = MYMIN( last_row - ii * nrpix[1], nrpix[1] );
      size  = nrpix[0] * nrpix[1];
    }
(void) SCFCLO( imf );

/*
 * draw the axes and the label
 */
if ( plmode >= 0 && access == 0 )
   { if ( strlen( cunit ) > (size_t) 32 )
        { (void) strcat( label[1], cunit+32 );
          *(cunit+32) = '\0';
        }
     if ( strlen( cunit ) > (size_t) 16 ) (void) strcat( label[0], cunit+16 );

     for ( ii = 0; ii < PLDIM2; ii++ )
         { (void) strcat( label[ii], ")" );
           LABSTR( label[ii] );
         }
/*
 * plot axes and labels
 */
     PCFRAM( wcfram, wcfram+FOR_Y, label[0], label[1] );

     if ( plmode == 1 )
        { (void) strcat( label[2], name );
          (void) strcat( label[3], ident );
          WEDGE( step, glevl, nlevl, options, greynes );
          PLIDEN( plmode, label[2], label[3] );
        }
     else if ( plmode == 2 )
        PLGRAI( plmode, name, ident, nlevl, glevl, options );
   }
/*
 * close plot file and terminate graphic operations
 */
PCCLOS();

return SCSEPI();
}
