/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  SENDPLOT
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, printing
.LANGUAGE    C
.PURPOSE     Redraw a plotfile and sends it to a device
      input:   P1/C/1/20: plotting device
               P2/C/1/60: plot file

.COMMENTS    none
.ENVIRONment MIDAS and AGL
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      General symbols for Plot routines
             #include <osparms.h>       Symbols used by the OS interfaces

.VERSION     1.1     13-Sep-1993   FORTRAN --> ANSI-C    RvH

 090422		last modif
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program

 */
#define  _POSIX_SOURCE 1


/* definition of the used functions in this module */

#include <stdio.h>
#include <string.h>

#include <midas_def.h>


#include <osparms.h>
#include <plot_def.h>

/*

*/

int main()
{ 
int   actvals, fid;
char  debug[6], device[22], dname[22], plnam[62];

char *err_plnm = "*** FATAL: No plot file present; sorry ...",
     *err_plex = "*** FATAL: Plot file not present; does it really exist?";




(void) SCSPRO("COPYGRAP");                   /*contact with the MIDAS monitor*/

(void) SCKGETC("P1",1,20,&actvals,device);
CGN_LOWSTR(device);
(void) GETDEV(device,dname);		/* always get something back... */



/* get the plot file to be sent */

(void) SCKGETC("P2",1,60,&actvals,plnam);
if ( strncmp(plnam,"undef",5) == 0)
   {
   PCKRDC("PLNAM",60,&actvals,plnam);
   if (strncmp(plnam,"none",4) == 0) SCETER(3,err_plnm);

   if ((fid = osdopen(plnam,READ)) == -1) SCETER(4,err_plex);
   }
else
   if ((fid = osdopen(plnam,READ)) == -1) SCETER(5,err_plex);


/* set the error recording */

PCKRDC("DEBUG",4,&actvals,debug);
if (strncmp(debug,"ON",2) == 0)
   AG_SSET("msgw;debu=1;errf=aglerr.log");
else
   AG_SSET("msgn;debu=0");


/* open viewport */

CGN_LOWSTR( dname );
(void) AG_VDEF( dname, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0 );
AG_MRDW( plnam );

(void) osdclose(fid);

AG_CLS();

return SCSEPI();
}
