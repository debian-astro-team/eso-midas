/*===========================================================================
  Copyright (C) 1995, 2003 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   PLOTAXES
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, box, axes
.LANGUAGE    C
.PURPOSE     Draws a frame with axis annotation on a graphics device
     input:     P1/R/4        = coordinate string for the xaxis
                P2/R/4        = coordinate string for the y-axis
                P3/R/4        = scale in X and Y, and offset in X and Y
                INPUTC/C/1/80 = X and Y label

.COMMENTS    the static function GETFRAME is also defined in PCOPEN
.ENVIRONment MIDAS
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      General symbols for Plot routines

.VERSION     1.0     15-Sep-1993   FORTRAN --> C    RvH
 030722		last modif

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdlib.h>
#include <string.h>
/*
 * define some macros and constants
 */
#include <midas_def.h>
#include <plot_def.h>

/*++++++++++++++++++++++++++++
.IDENTifer   GETFRAME
.PURPOSE     Get name of frame displayed on current active display channel
    output:    char *frame
.RETURNS     0 = o.k., else nothing loaded into channel
------------------------------*/
#ifdef __STDC__
      static int GETFRAME( char *frame )
#else
      static int GETFRAME( frame )
      char *frame;
#endif

{
int iav;


/* get the name of the frame loaded in the current image display */

(void) SCKGETC("IDIMEMC",1,80,&iav,frame);

if (truelen(frame) == 0)
   return 1;
else
   return ERR_NORMAL;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++
 * here start the code of the function
 */
int main()
{
int     access, actvals, ii, imf, knul, len, unit, 
        ndum[PLDIM2], npix[PLDIM2], sublo[PLDIM2], subhi[PLDIM2];

float   area[4], image[4], rbuff[4], wcfram[8], disfram[4];

double  start[PLDIM2], step[PLDIM2];

char    *action, cmnd[21], device[21], dname[21], frame[84],
        xcoord[73], ycoord[73], buff[91], *label[PLDIM2], *cbuff,
        ra[2], dec[3], xform[8];
/*
 * initialised variables
 */
char *err_coord = "*** FATAL: specify at least start and end values for the axes",
     *err_fram  = "*** FATAL: no frame loaded in the display channel",
     *err_extrc = "*** FATAL: Convcoo: invalid coordinate input ...",
     *err_usrin = "*** FATAL: error detected in USRINP",
     *err_gdev  = "*** FATAL: error detected in GETDEV";
/*
 * plot mode taken from keyword PMODE
 */
int  plmode = -1;

action = calloc( 9, sizeof(char));
for ( ii = 0; ii < PLDIM2; ii++ ) label[ii] = osmmget(81);

/*
 * start of executable code
 */
(void) SCSPRO( "PLTAXS" );		/*contact with the MIDAS monitor*/

ii = 1;
(void) SCPSET(F_FITS_PARM,&ii);          /* don't read in ESO.xyz keywords */


(void) SCKGETC( "MID$CMND", 1, 20, &actvals, cmnd );
if ( *cmnd == 'P' )                                               /*plot mode*/
   access = 0;
else                                                          /*overplot mode*/
   access = 1;
/*
 * get device
 */
(void) SCKGETC( "MID$PLOT", 1, 20, &actvals, device );
if ( GETDEV( device, dname ) != ERR_NORMAL ) SCETER( 1, err_gdev );

/*
 * get coordinate string for the X-axis
 */
(void) SCKGETC( "P1", 1, 72, &actvals, xcoord );

/*
 * if the current viewport is a image display
 */
if ( strncmp( dname, "image", 5 ) == 0 && strncmp( xcoord, "[", 1 ) == 0 )
   { (void) SCKGETC( "ACTION", 1, 8, &actvals, action );
     (void) SCKWRC( "ACTION", 1, "DISPLAY", 1, 7, &unit );
/*
 * get the name of the loaded frame and it's characteristics
 */
     if ( GETFRAME( frame ) != ERR_NORMAL ) (void) SCETER( 1, err_fram );
     (void) SCFOPN( frame, D_R4_FORMAT, 0, F_IMA_TYPE, &imf );
     (void) SCDRDI( imf, "NPIX" , 1, PLDIM2, &actvals, npix , &unit, &knul );
     (void) SCDRDD( imf, "START", 1, PLDIM2, &actvals, start, &unit, &knul );
     (void) SCDRDD( imf, "STEP" , 1, PLDIM2, &actvals, step , &unit, &knul );
     (void) SCDRDC( imf, "CUNIT", 1, 17, 2, &actvals, ra, &unit, &knul );
     (void) SCDRDC( imf, "CUNIT", 1, 33, 3, &actvals, dec, &unit, &knul );
     if ( Convcoo(1,imf,xcoord,PLDIM2,ndum,sublo,subhi) != 0 ) 
        (void) SCETER( 2, err_extrc );
     (void) SCFCLO( imf );
     image[0] = sublo[0] + 1.0;
     image[1] = subhi[0] + 1.0;
     image[2] = sublo[1] + 1.0;
     image[3] = subhi[1] + 1.0;
     BOXPTW( image,   npix[0], start[0], step[0], area );
     BOXPTW( image+2, npix[1], start[1], step[1], area+2 );
/*
 * check the area with the displayed area and stores the data
 */
     (void) SCKRDR( "IDIMEMR", 1, 4, &actvals, disfram, &unit, &knul);
     if (step[0] < 0 ){
       wcfram[0] = MYMIN( area[0], disfram[0] );     
       wcfram[1] = MYMAX( area[1], disfram[2] );
     }
     else{
       wcfram[0] = MYMAX( area[0], disfram[0] );     
       wcfram[1] = MYMIN( area[1], disfram[2] );
     }
     wcfram[2] = wcfram[3] = 0.0;
     GETFRM( "MANU", wcfram );

     if (step[1] < 0 ){
        wcfram[FOR_Y]   = MYMIN( area[2], disfram[1] );     
        wcfram[FOR_Y+1] = MYMAX( area[3], disfram[3] );
     }
     else{
        wcfram[FOR_Y]   = MYMAX( area[2], disfram[1] );     
        wcfram[FOR_Y+1] = MYMIN( area[3], disfram[3] );
     }
     wcfram[FOR_Y+2] = wcfram[FOR_Y+3] = 0.0;
     GETFRM( "MANU", wcfram + FOR_Y );

     PCKWRR( "XWNDL", 4, wcfram );
     PCKWRR( "YWNDL", 4, wcfram+FOR_Y );
   }
/*
 * for all the other devices
 */
else
   { (void) SCKGETC( "P2", 1, 72, &actvals, ycoord );
/*
 * Get the manual setting for the axes
 */
     PCKRDR( "XAXIS", 4, &actvals, wcfram );
     PCKRDR( "YAXIS", 4, &actvals, wcfram+FOR_Y );
     if ( *xcoord != 'M' || *xcoord != 'm' )
        { cbuff = (char *) rbuff;
          if ( USRINP( 'r', xcoord, 4, cbuff, &actvals ) != 0 )
             (void) SCETER( 2, err_usrin );
          switch ( actvals )
             { case 2:
                    wcfram[2] = wcfram[3] = 0.0;
                    break;
               case 3:
                    wcfram[2] = wcfram[3] = rbuff[2];
                    break;
               case 4:
                    wcfram[2] = rbuff[2];
                    wcfram[3] = rbuff[3];
                    break;
               default:
                    (void) SCETER( 3, err_coord );
             }
          wcfram[0] = rbuff[0];
          wcfram[1] = rbuff[1];
        }
     if ( *ycoord != 'M' || *ycoord != 'm' )
        { cbuff = (char *) rbuff;
          if ( USRINP( 'r', ycoord, 4, cbuff, &actvals ) != 0 )
             (void) SCETER( 2, err_usrin );
          switch ( actvals )
             { case 2:
                    wcfram[FOR_Y+2] = wcfram[FOR_Y+3] = 0.0;
                    break;
               case 3:
                    wcfram[FOR_Y+2] = wcfram[FOR_Y+3] = rbuff[2];
                    break;
               case 4:
                    wcfram[FOR_Y+2] = rbuff[2];
                    wcfram[FOR_Y+3] = rbuff[3];
                    break;
               default:
                    (void) SCETER( 3, err_coord );
             }
          wcfram[FOR_Y]   = rbuff[0];
          wcfram[FOR_Y+1] = rbuff[1];
        }
     GETFRM( "MANU", wcfram );
     GETFRM( "MANU", wcfram + FOR_Y );
     PCKWRR( "XWNDL", 4, wcfram );
     PCKWRR( "YWNDL", 4, wcfram+FOR_Y );
   }
/*
 *  get the labels
 */
(void) SCKGETC( "LABELX", 1, 80, &actvals, buff );
len = strlen( buff );
if ( *buff == '"' && *(buff+len) == '"' )
   (void) strncpy( label[0], buff+1, len-2 );
else
   (void) strcpy( label[0], buff );
LABSTR( label[0] );

(void) SCKGETC( "LABELY", 1, 80, &actvals, buff );
len = strlen( buff );
if ( *buff == '"' && *(buff+len) == '"' )
   (void) strncpy( label[1], buff+1, len-2 );
else
   (void) strcpy( label[1], buff );
LABSTR( label[1] );

/*
 * setup graphic device according to MIDAS settings
 */
PCOPEN( " ", " ", access, &plmode );
/*
 * Redefine the world coordinate window in case of 
 * RA and DEC descriptors
 */
(void) PCKRDC("XFORMAT", 1, &actvals, xform);
if ( (strncmp( ra, "RA", 2 ) == 0 ) && (strncmp( xform, "H", 1 ) == 0) ){
   wcfram[0] = wcfram[0]/15.0;
   wcfram[1] = wcfram[1]/15.0;
   wcfram[2] = wcfram[2]/15.0;
   wcfram[3] = wcfram[3]/15.0;
   PCKWRR( "XWNDL", 4, wcfram );
   AG_WDEF(wcfram[0],wcfram[1],wcfram[4],wcfram[5]);
}
/*
 * plot axes and labels
 */
if ( plmode >= 0 ) PCFRAM( wcfram, wcfram+FOR_Y, label[0], label[1] );

/*
 * close plot file and terminate graphic operations
 */
PCCLOS();

(void) SCKWRC( "ACTION", 1, action, 1, 7, &unit );
return SCSEPI();
}
