/* @(#)plotcol.c	19.1 (ESO-DMD) 02/25/03 14:08:03 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   PLOTCOL
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, bulk data frame, one-dimensional plotting
.LANGUAGE    C
.PURPOSE     plot or overplot a column of a FRAME
  in/output: IN_A/C/1/60  = input frame
             P2/C/72      = column number (default 1)
             P3/C/72      = first and last pixel of the line 
                            (default the whole line). 
                            This range will be taken, if manual scaling in x 
                            has been specified else the plot will be made in 
                            auto scaling mode.
             P4/R/2       = New plot:  scales in x and y 
                                        (default is auto scaling)
             P4/I/I       = Over plot: line type

.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      General symbols for Plot routines


.VERSION     1.1     10-Sep-1993   FORTRAN --> C    RvH

 010423		last modif

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


/*
 * define some macros and constants
 */
#include <midas_def.h>
#include <plot_def.h>

/*
 * here starts the code of the function
 */
int main()
{
int     access, actvals, binmod, ii, imf, knul, naxis, nrpcol, stat,
        stype, ltype, unit, npix[PLDIM2], ndum[PLDIM2], sublo[PLDIM2], 
        subhi[PLDIM2];

float   y_off, xmin, xmax, *ydata, *xdata, area[4], image[4], wcfram[8],
        cuts[4];

double  start[PLDIM2], step[PLDIM2];

char    bin[5], cmnd[21], name[61], cunit[33], ident[33], buff[81], *input,
        *label[4], *cpntr, *pntr, *xycoord;
/*
 * initialised variables
 */
int   plmode = -1;                     /* plot mode taken from keyword PMODE */

char  *err_1dim  = "*** WARNING: Image column contains only one point!",
      *err_flat  = "*** WARNING: zero dynamic range in data at %13.8g",
      *err_range = "*** FATAL: range in x has no overlap with current graph abscissa - no plot";
static char  *axis[PLDIM2] = { "MANU", "MANU" };

/*
 * allocate memory for different character pointers and initialise a few
 */

for ( ii = 0; ii < 4; ii++ ) label[ii] = osmmget(81);
(void) strcpy( label[0], "Position (" );
(void) strcpy( label[1], "Pixel value (" );
(void) strcpy( label[2], "Image: " );
(void) strcpy( label[3], "Column: #" );

xycoord = osmmget(81);
input   = osmmget(81);
/*
 * start of executable code
 */
(void) SCSPRO( "PLTCOL" );                   /*contact with the MIDAS monitor*/
(void) SCKGETC( "MID$CMND", 1, 20, &actvals, cmnd );
if ( *cmnd == 'P' )                                               /*plot mode*/
   access = 0;
else                                                          /*overplot mode*/
   access = 1;

/* 
 * find file name and read header information
 */
(void) SCKGETC( "IN_A", 1, 60, &actvals, name );
(void) SCFOPN( name, D_R4_FORMAT, 0, F_IMA_TYPE, &imf );
(void) SCDRDI( imf, "NPIX" , 1, PLDIM2, &actvals, npix , &unit, &knul );

/*
 * check frame parameters
 */
if ( npix[1] == 1 ) (void) SCETER( 1, err_1dim );

/*
 * read the descriptor
 */
(void) SCDRDI( imf, "NAXIS", 1, 1, &actvals, &naxis, &unit, &knul );
(void) SCDRDD( imf, "START", 1, PLDIM2, &actvals, start, &unit, &knul );
(void) SCDRDD( imf, "STEP" , 1, PLDIM2, &actvals, step , &unit, &knul );
(void) SCDRDR( imf, "LHCUTS" , 1, 4, &actvals, cuts , &unit, &knul );
(void) SCDGETC( imf, "IDENT", 1, 32, &actvals, ident );
(void) SCDGETC( imf, "CUNIT", 1, 48, &actvals, cunit );

/*
 * Get the manual setting for the axes
 */
PCKRDR( "XAXIS", 4, &actvals, wcfram );
PCKRDR( "YAXIS", 4, &actvals, wcfram+FOR_Y );

/*
 * column number (X-coordinate); default column = 1
 */
image[0] = image[1] = 1;
if ( naxis > 1 )
   { (void) SCKGETC( "P2", 1, 72, &actvals, input );
     (void) strcpy(xycoord,input);
     (void) strcat(xycoord,",<");
     stat = Convcoo(0,imf,xycoord,PLDIM2,ndum,sublo,subhi); 
     image[0] = image[1] = sublo[0] + 1;
     BOXPTW( image, npix[0], start[0], step[0], area );
   }

/*
 * find first and last pixel along the column (Y-coordinate)
 */
(void) SCKGETC( "P3", 1, 72, &actvals, input );
if ( *input == 'M' || *input == 'm' )
   { BOXWTP( wcfram, npix[1], start[1], step[1], image+2 );
     BOXPTW( image+2, npix[1], start[1], step[1], area+2 );
   }
else
   { pntr = strstr(input, ",");
     (void) strcpy(xycoord, "@1,");
     (void) strcat(xycoord, strtok( input, "," ) );
     stat = Convcoo(0,imf,xycoord,PLDIM2,ndum,sublo,subhi); 
     image[2] = sublo[1] + 1;

     (void) strcpy(xycoord,"@1,");
     (void) strcat(xycoord,pntr+1);
     stat = Convcoo(0,imf,xycoord,PLDIM2,ndum,sublo,subhi); 
     image[3] = sublo[1] + 1;
     BOXPTW( image+2, npix[0], start[1], step[1], area+2 );
   }

PCKWRR( "PIXEL", 4, image );

/*
 * allocate virtual memory for a single column of the frame
 */
nrpcol = (int) fabs(image[3] - image[2])+1;  /*number of points in the column*/
xdata  = (float *) osmmget( nrpcol * sizeof(float));
ydata  = (float *) osmmget( nrpcol * sizeof(float));

/*
 * copy the data of the column
 */
(void) SCFMAP( imf, F_I_MODE, 1, npix[0]* npix[1], &actvals, &cpntr );
GETBDF( cpntr, image, npix, start, step, xdata, ydata );
(void) SCFUNM( imf );

if ( access == 0 )
   {  
/*
 * calculate frame along x-axis
 */
     if ( fabs( *wcfram ) < PLT_EPS && fabs( *(wcfram+1) ) < PLT_EPS )
        { axis[0] = "AUTO";
          wcfram[0] = area[2];
          wcfram[1] = area[3]; 
          wcfram[2] = wcfram[3] = 0.0;
        }
/*
 * calculate frame along y-axis
 */
     if ( fabs( *(wcfram+FOR_Y)) < PLT_EPS 
          && fabs( *(wcfram+FOR_Y+1)) < PLT_EPS )
        {
        axis[1] = "AUTO";
        wcfram[FOR_Y]   = cuts[0];
        wcfram[FOR_Y+1] = cuts[1];
        if ( wcfram[FOR_Y] == wcfram[FOR_Y+1] )
           MINMAX( ydata, nrpcol, wcfram + FOR_Y, wcfram + FOR_Y + 1 );
        if ( wcfram[FOR_Y] == wcfram[FOR_Y+1] )
           {  
           (void) sprintf( buff, err_flat,  wcfram[FOR_Y]);
           SCTPUT( buff );
           }
        wcfram[FOR_Y+2] = wcfram[FOR_Y+3] = 0.0;
        }
     GETFRM( axis[0], wcfram );
     GETFRM( axis[1], wcfram+FOR_Y );
     PCKWRR( "XWNDL", 4, wcfram );
     PCKWRR( "YWNDL", 4, wcfram+FOR_Y );
     y_off = 0.0;
   }
else                                                         /* overplot mode*/
   { PCKRDR( "XWNDL", 4, &actvals, wcfram );
     PCKRDR( "YWNDL", 4, &actvals, wcfram+FOR_Y );
/*
 * does overplot data fall within plotted frame? 
 */
     xmin = MYMIN( wcfram[0], wcfram[1] );
     xmax = MYMAX( wcfram[0], wcfram[1] );
     if ( ( MYMAX( area[2], area[3] ) < xmin ) ||
          ( MYMIN( area[2], area[3] ) > xmax ) )
        (void) SCETER( 2, err_range );
     (void) SCKRDR( "INPUTR", 1, 1, &actvals, &y_off, &unit, &knul);
   }
/*
 * setup graphic device according to MIDAS settings
 */
PCOPEN( " ", " ", access, &plmode );

/*
 * get the symbol type, line type and binmode
 */
PCKRDI( "STYPE", 1, &actvals, &stype );
PCKRDI( "LTYPE", 1, &actvals, &ltype );
PCKRDC( "BINMOD", 4, &actvals, bin );
binmod = (strncmp( bin, "ON", 2 ) == 0) ? 1 : 0;

/* 
 * plot the data
 */
PCDATA( stype, ltype, binmod, xdata, ydata, y_off, nrpcol );

/*
 * draw the axes and the label
 */
if ( plmode >= 0 && access == 0 )
   { if ( strlen( cunit ) > (size_t) 0 )
        { (void) strncat( label[0], cunit+32, 16);
          (void) strncat( label[1], cunit, 16);
        }
     for ( ii = 0; ii < PLDIM2; ii++ )
         { (void) strcat( label[ii], ")" );
           LABSTR( label[ii] );
         }

/*
 * plot axes and labels
 */
     PCFRAM( wcfram, wcfram+FOR_Y, label[0], label[1] );

     if ( plmode == 1 )
        { (void) strcat ( label[2], name );
          (void) sprintf( buff, "%-.0d", (int) image[0] );
          (void) strcat ( label[3], buff );
          PLIDEN( plmode, label[2], label[3] );
        }
     else if ( plmode == 2 )
        PLBDFI( plmode, name, ident, image );

   }
/*
 * close plot file and terminate graphic operations
 */
PCCLOS();

return SCSEPI();
}
