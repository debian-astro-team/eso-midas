/* @(#)overgrd.c	19.1 (ES0-DMD) 02/25/03 14:08:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTIFIER  OVRGRD
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, table, one-dimensional plotting
.LANGUAGE    C
.PURPOSE     Plot a grid over an existing plot
      input: P1/C/1/1 = grid type
                           l(arge): connecting large tick marks
                           s(mall): also connecting small tick marks
             P2/C/1/2 = XY grid lines: grid in X, or Y or X & Y

.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>     Prototypes for MIDAS interfaces

.VERSION     1.1     13-Sep-1993   FORTRAN --> ANSI-C    RvH
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <string.h>
#include <midas_def.h>

/*
 * here starts the code of the function
 */
int main()
{
int     actvals;
char    cinput[2], option[3];

char *err_tick = "*** FATAL: you have to specify Large or Small tickmark grid",
     *err_xy   = "*** FATAL: you have to specify X and/or Y direction of grid";
/*
 * initialised variables
 */
int  access =  1,                    /* parameter for PCOPEN: overplot mode  */
     plmode = -1;                      /* plot mode taken from keyword PMODE */

/*
 * start of executable code
 */
(void) SCSPRO( "OVRGRD" );                   /*contact with the MIDAS monitor*/

/*
 * get User parameters
 */
(void) SCKGETC( "P1", 1, 1, &actvals, cinput );
if ( *cinput != 'L' && *cinput != 'l' && *cinput != 'S' && *cinput != 's' )
   SCETER( 1, err_tick );

(void) SCKGETC( "P2", 1, 2, &actvals, option );
CGN_LOWSTR( option );
if ( strchr( option, 'x' ) == NULL && strchr( option, 'y' ) == NULL )
   SCETER( 2, err_xy );

/*
 * setup graphic device according to MIDAS settings
 */
PCOPEN( " ", " ", access, &plmode );

/*
 * do the work
 */
PLGRD( cinput, option );

/*
 * good bye and finish
 */
PCCLOS();

return SCSEPI();
}

