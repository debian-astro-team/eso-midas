/*===========================================================================
  Copyright (C) 1993-2011 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   PLOTDES
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    bulk data frame , descriptor, graphics
.LANGUAGE    C
.PURPOSE     Plot or overplot the contents of a descriptor
        input: IN_A/C/1/60   = input frame
               INPUTC/C/1/20 = name of the descriptor (default HISTOGRAM)
               INPUTI/I/1/2  = first and last data point
               P4/R/1/4      = scales in X and Y and offset X and Y
                             (default is auto scaling to device filling)

.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      General symbols for Plot routines

.VERSION     1.1     10-Sep-1993   FORTRAN --> ANSI-C    RvH

 110930		last mdoif

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

/*
 * define some macros and constants
 */
#include <midas_def.h>
#include <plot_def.h>

#define MAXDSC		1000

/*
 * here start the code of the function
 */
int main()
{
int    actvals, access, dscelm, ibgn, iend, ii, imf, knul, ltype, nrdsc, 
       ndum, stype, xmin, xmax, unit, image[2], ibuff[5], *idata;

float  *x_data, y_off, *y_data, wcfram[8];

char   dtype[2], cmnd[21], desc[56], ident[34], name[61], buff[81], *label[4];

/*
 * initialised variables
 */
int    plmode = -1,               /* the value of PMODE is taken, see PCOPEN */
       binmod =  1;                  /* the data is plotted with BIN mode ON */
       
char  *err_mxnr = "*** WARNING: max. number of data points allowed: %6d",
      *err_flat = "*** WARNING: zero dynamic range in data at %13.8g",
      *err_name = "*** FATAL: Descriptor does not exist",
      *err_cdsc = "*** FATAL: Descriptor is of character type ",
      *err_2small = "*** WARNING: first data point has negative serial number; the value 1 is taken",
      *err_2large = "*** WARNING: last data point is beyond descriptor limits; took last item",
      *err_range = "*** FATAL: range in x has no overlap with current graph abscissa - no plot";

static char  *axis[PLDIM2] = { "MANU", "MANU" };

for ( ii = 0; ii < 4; ii++ ) label[ii] = osmmget(81);
(void) strcpy( label[0], "Sequence number" );
(void) strcpy( label[1], "Descriptor value" );
(void) strcpy( label[2], "Frame: " );
(void) strcpy( label[3], "Descriptor: " );

/*
 * start of executable code
 */
(void) SCSPRO( "PLTDES" );                   /*contact with the MIDAS monitor*/
(void) SCKGETC( "MID$CMND", 1, 20, &actvals, cmnd );
if ( *cmnd == 'P' )                                               /*plot mode*/
   access = 0;
else                                                          /*overplot mode*/
   access = 1;

/*
 * find file name and read header information
 */
(void) SCKGETC( "IN_A", 1, 60, &actvals, name );

/*
 * get descriptor
 */
(void) SCFOPN( name, D_OLD_FORMAT, 0, F_OLD_TYPE, &imf );
(void) SCFINF( name, 1, ibuff );
if ( ibuff[1] == F_IMA_TYPE )
   (void) SCDRDC( imf, "IDENT", 1, 1, 32, &actvals, ident, &unit, &knul );
if ( truelen( ident ) == 0 ) *ident = '\0';

(void) SCKGETC( "INPUTC", 1, 48, &actvals, desc );
(void) SCDFND( imf, desc, dtype, &dscelm, &ndum );
if ( *dtype == ' ' ) (void) SCETER( 1, err_name );
if ( *dtype == 'c' || *dtype == 'C' ) (void) SCETER( 2, err_cdsc );

/*
 * which values of the descriptor have to be read
 */
(void) SCKRDI( "INPUTI", 1, 2, &actvals, image, &unit, &knul );

if ( image[0] == image[1] )
   { if ( (ibgn = iend = image[0]) == 0 )
        { ibgn = 1;
          iend = dscelm;
        }
   }
else
   { ibgn = MYMIN( image[0], image[1] );
     iend = MYMAX( image[0], image[1] );

     if ( ibgn < 1 )
        { SCTPUT( err_2small );
          ibgn = 1;
        }

     if ( iend > dscelm )
        { (void) strcpy( buff, err_2large );
          SCTPUT( buff );
          iend = dscelm;
        }
   }
/*
 * check the number of descriptors
 */
if ( iend - ibgn + 1 > MAXDSC )
   { (void) sprintf( buff, err_mxnr, MAXDSC );
     SCTPUT( buff );
     iend = MAXDSC;
   }

if ( image[0] <= image[1] )
   { image[0] = ibgn;
     image[1] = iend;
   }
else       
   { image[0] = iend;
     image[1] = ibgn;
   }
nrdsc = iend - ibgn + 1;

/*
 * get the data values
 */
x_data = (float *) osmmget( nrdsc * sizeof( float ));
y_data = (float *) osmmget( nrdsc * sizeof( float ));
if ( *dtype == 'I' || *dtype == 'i' )
   { idata = (int *) osmmget( nrdsc * sizeof( int ));
     (void) SCDRDI( imf, desc, ibgn, nrdsc, &actvals, idata, &unit, &knul );
     for ( ii = 0; ii < actvals; ii++ )
         y_data[ii] = idata[ii];
     (void) osmmfree( (char *) idata );
   }
else
   (void) SCDRDR( imf, desc, ibgn, nrdsc, &actvals, y_data, &unit, &knul );
nrdsc = actvals;

x_data[0] = MYMIN( image[0], image[1] );
for ( ii = 1; ii < nrdsc; ii++ ) x_data[ii] = x_data[ii-1] + 1;

/*
 * Get the manual setting for the axes
 */
PCKRDR( "XAXIS", 4, &actvals, wcfram );
PCKRDR( "YAXIS", 4, &actvals, wcfram+FOR_Y );

/*
 * only in plot mode
 */
if ( access == 0 )
   { 
/*
 * calculate frame along X-axis
 */
     if ( fabs( *wcfram ) < PLT_EPS && fabs( *(wcfram+1) ) < PLT_EPS )
        { axis[0] = "AUTO";
          wcfram[0] = image[0];
          wcfram[1] = image[1];
          wcfram[2] = wcfram[3] = 0.0;
        }
/*
 * calculate frame along Y-axis
 */
     if ( fabs( *(wcfram+FOR_Y) ) < PLT_EPS 
          && fabs( *(wcfram+FOR_Y+1) ) < PLT_EPS )
        { axis[1] = "AUTO";
          MINMAX( y_data, nrdsc, wcfram + FOR_Y, wcfram + FOR_Y + 1 );
          if ( *(wcfram + FOR_Y) >= *(wcfram + FOR_Y + 1) )
             { (void) sprintf( buff, err_flat, *(wcfram + FOR_Y) );
               SCTPUT( buff );
             }
          wcfram[FOR_Y+2] = wcfram[FOR_Y+3] = 0.0;
        }
     GETFRM( axis[0], wcfram );
     GETFRM( axis[1], wcfram + FOR_Y );
     PCKWRR( "XWNDL", 4, wcfram );
     PCKWRR( "YWNDL", 4, wcfram+FOR_Y );
     y_off = 0.0;
   }
else                                                         /* overplot mode*/
   { PCKRDR( "XWNDL", 4, &actvals, wcfram );
     PCKRDR( "YWNDL", 4, &actvals, wcfram+FOR_Y );
/*
 * does overplot data  fall within plotted frame? 
 */
     xmin = MYMIN( wcfram[0], wcfram[1] );
     xmax = MYMAX( wcfram[0], wcfram[1] );
     if ( ( MYMAX( image[0], image[1] ) < xmin ) ||
          ( MYMIN( image[0], image[1] ) > xmax ) )
        (void) SCETER( 4, err_range );

     (void) SCKRDR( "INPUTR", 1, 1, &actvals, &y_off, &unit, &knul);
   }
/*
 * setup graphic device according to MIDAS settings
 */
PCOPEN( " ", " ", access, &plmode );

/*
 * get the symbol type and line type
 */
PCKRDI( "STYPE", 1, &actvals, &stype );
PCKRDI( "LTYPE", 1, &actvals, &ltype );

/* 
 * plot the data
 */
PCDATA( stype, ltype, binmod, x_data, y_data, y_off, nrdsc );

/*
 * draw the axes and the label
 */
if ( plmode >= 0 && access == 0 )
   { PCFRAM( wcfram, wcfram+FOR_Y, label[0], label[1] );

     if ( plmode == 1 )
        { (void) strcat ( label[2], name );
          (void) strcat( label[3], desc );
          PLIDEN( plmode, label[2], label[3] );
        }
     else if ( plmode == 2 )
        PLDESI( plmode, name, ident, desc, image );
   }
/*
 * close plot file and terminate graphic operations
 */
PCCLOS();

return SCSEPI();
}
                       
