/* @(#)showgrap.c	19.1 (ESO)-DMD) 02/25/03 14:08:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTIFIER  SHOGRA
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, plot mode, setup
.LANGUAGE    C
.PURPOSE     Show the setup of the MIDAS plotting software

.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      Symbols used by the PLT interfaces

.VERSION     1.1     13-Sep-1993   FORTRAN --> ANSI-C    RvH

 010423		last modif

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <string.h>
#include <math.h>

/*
 * define some macros and constants
 */
#include <midas_def.h>
#include <plot_def.h>

/*dimension of resp. *plckey, *plikey and *plrkey */
#define NRCKEYS    63
#define NRIKEYS    10
#define NRRKEYS    26
#define NRGKEYS    21

/* adresses to keyword values in PLCSTAT -> *plckey */
#define BNMODE     plckey          /* bin mode ON or OFF */
#define CLMODE     plckey+4        /* colour mode: SUB or XOR */
#define ERASE      plckey+8        /* erase for plot commands */
#define FRAME      plckey+16       /* AUTO scaling or SQUAre frame */
#define XFORM      plckey+20       /* format of values along the X-axis */
#define YFORM      plckey+28       /* format of values along the Y-axis */
#define ZFORM      plckey+36       /* format of values along the Z-axis */
#define CXSC       plckey+44       /* x scale coordinates */
#define CYSC       plckey+48       /* y scale coordinates */
#define CXOF       plckey+52       /* x offset coordinates */
#define CYOF       plckey+56       /* y offset coordinates */

/* adresses to keyword values in PLISTAT -> *plikey */
#define PMODE      plikey          /* pmode mode (ranging from 0 to 2) */
#define FONT       plikey+1        /* font used (ranging from 0 to 6) */
#define LTYPE      plikey+4        /* line line (ranging from 0 to 5) */
#define STYPE      plikey+5        /* symbol type (ranging from 0 to 20) */
#define LWIDTH     plikey+6        /* line width (ranging from 1 to 4) */
#define TWIDTH     plikey+7        /* text width (ranging from 1 to 4) */
#define COLOR      plikey+8        /* colour (ranging from 0 to 8) */
#define BCOLOR     plikey+9        /* background colour (0 to 8) */

/* adresses to keyword values in PLRSTAT -> *plrkey */
#define XAXIS      plrkey          /* x axis definition in world coord */
#define YAXIS      plrkey+4        /* y axis definition in world coord */
#define ZAXIS      plrkey+8        /* z axis definition in world coord */
#define XSCAL      plrkey+12       /* scale in x */
#define YSCAL      plrkey+13       /* scale in y */
#define ZSCAL      plrkey+14       /* scale in z */
#define XOFF       plrkey+20       /* offset in x */
#define YOFF       plrkey+21       /* offset in y */
#define SSIZE      plrkey+24       /* symbol multiplier */
#define TSIZE      plrkey+25       /* text size multiplier */

/*
 * here start the code of the function
 */
int main()
{
int    plikey[NRIKEYS];
float  plrkey[NRRKEYS];
char   *plckey;

int    actvals, iac, knul, stat, unit;
char   *orig, *fmt, *dname, *kname, *stru, *device, *plfile, *data, *text;

char *inf_dev = "******* Graphic device and plot file           *******",
     *inf_axs = "******* Axes, scales and tickmarks             *******",
     *inf_etc = "******* Plot mode, fonts, symbols, lines, etc. *******",
     *inf_des  = "Data:     name = %s; type = descriptor; source = %s",
     *inf_key  = "Data:     name = %s; type = keyword",
     *inf_fra  = "Data:     name = %s; type = frame",
     *inf_tab  = "Data:     name = %s; type = table",
     *err_pli  = "*** FATAL: can not find all keywords in PLISTAT",
     *err_plr  = "*** FATAL: can not find all keywords in PLRSTAT",
     *errdev   = "*** FATAL: unknown or unavailable device",
     *errhlp   = "           use HELP [printers]";
/*
 * allocate memory for different character pointers
 */
fmt    = osmmget(9);
stru   = osmmget(21);
dname  = osmmget(21);
kname  = osmmget(21);
device = osmmget(31);
plfile = osmmget(61);
data   = osmmget(61);
text   = osmmget(81);
plckey = osmmget(NRCKEYS + 1);

/*
 * start of executable code
 */
(void) SCSPRO( "SHOGRA" );                   /*contact with the MIDAS monitor*/

/*
 * initialise the keyword pointers
 */
(void) SCKGETC( "PLCSTAT", 1, NRCKEYS, &iac, plckey );

(void)  SCKRDI( "PLISTAT", 1, NRIKEYS, &iac, plikey, &unit, &knul );
if ( iac != NRIKEYS ) (void) SCETER( 1, err_pli );

(void)  SCKRDR( "PLRSTAT", 1, NRRKEYS, &iac, plrkey, &unit, &knul );
if ( iac != NRRKEYS ) (void) SCETER( 2, err_plr );

/*
 * selected device
 */
SCTPUT( inf_dev );
(void)  SCKGETC( "MID$PLOT", 1, 30, &actvals, device );
CGN_LOWSTR( device );
(void) strncpy( kname, device, 20 );
stat = GETDEV( kname, dname );
if ( stat != ERR_NORMAL || strncmp( dname, "unknown", 7 ) == 0 )
   { SCTPUT( errdev );
     (void) SCETER( 2, errhlp );
   }
(void) sprintf( text, "Device:   %s", dname );
SCTPUT( text );

/*
 * data source
 */
PCKRDC( "DNAM", 60, &actvals, data );
(void) strtok( data, " " );

PCKRDC( "DTYP", 20, &actvals, stru );
(void) strtok( stru, " " );
CGN_LOWSTR( stru );
if ( strncmp( stru, "des", 3 ) == 0 )
   { if ( (orig = strchr( data, ',' )) != NULL )
        (void) sprintf( text, inf_des, data, orig );
     else
        (void) sprintf( text, inf_des, data, "unknown" );
     SCTPUT( text );
   }
else if ( strncmp( stru, "key", 3 ) == 0 )
   { (void) sprintf( text, inf_key, data );
     SCTPUT( text );
   }
else if ( strncmp( stru, "fra", 3 ) == 0 )
   { (void) sprintf( text, inf_fra, data );
     SCTPUT( text );
   }
else if ( strncmp( stru, "tab", 3 ) == 0 )
   { (void) sprintf( text, inf_tab, data );
     SCTPUT( text );
   }

PCKRDC( "PLNAM", 60, &actvals, plfile );
/* The part below was eliminated after taking out the FILE/NOFILE option

if ( strncmp( device+20, "file", 4 ) == 0 )
   { if ( strncmp( device, "graph", 5 ) == 0 
          || strncmp( device, "image", 5 ) == 0 )
        (void) sprintf( text, "Plotfile: %s", plfile );
     else if ( strncmp( device+25, "spool", 5 ) == 0 )
        (void) sprintf( text, "Plotfile: %s   SPOOL   nospool", plfile );
     else
        (void) sprintf( text, "Plotfile: %s   spool   NOSPOOL", plfile );
   }
else
   (void) sprintf( text, inf_plfl, plfile );
SCTPUT( text );
*/

if ( strncmp( device, "graph", 5 ) == 0 
   || strncmp( device, "image", 5 ) == 0 )
   (void) sprintf( text, "Plotfile: %s", plfile );
else if ( strncmp( device+20, "spool", 5 ) == 0 )
   (void) sprintf( text, "Plotfile: %s   SPOOL   nospool", plfile );
else
   (void) sprintf( text, "Plotfile: %s   spool   NOSPOOL", plfile );
SCTPUT( text );

/*
 * AXIS: min, max and tickmarks
 */
SCTPUT( inf_axs );
if ( fabs( *(XAXIS) ) < PLT_EPS && fabs( *(XAXIS+1) ) < PLT_EPS )
   (void) strcpy( text, 
                  "XAXIS:    AUTO   x_start, x_end, x_big_step, x_small_step");
else
   (void) sprintf( text, "XAXIS:    auto   %-.5g %-.5g %-.5g %-.5g",
            *(XAXIS), *(XAXIS+1), *(XAXIS+2), *(XAXIS+3) );
SCTPUT( text );
if ( fabs( *(YAXIS) ) < PLT_EPS && fabs( *(YAXIS+1) ) < PLT_EPS )
   (void) strcpy( text, 
                  "YAXIS:    AUTO   y_start, y_end, y_big_step, y_small_step");
else
   (void) sprintf( text, "YAXIS:    auto   %-.5g %-.5g %-.5g %-.5g",
            *(YAXIS), *(YAXIS+1), *(YAXIS+2), *(YAXIS+3) );
SCTPUT( text );
if ( fabs( *(ZAXIS) ) < PLT_EPS && fabs( *(ZAXIS+1) ) < PLT_EPS )
   (void) strcpy( text, 
                  "ZAXIS:    AUTO   y_start, y_end, y_big_step, y_small_step");
else
   (void) sprintf( text, "ZAXIS:    auto   %-.5g %-.5g %-.5g %-.5g",
            *(ZAXIS), *(ZAXIS+1), *(ZAXIS+2), *(ZAXIS+3) );
SCTPUT( text );

if ( strncmp( FRAME, "SQ", 2 ) == 0  )
   (void) strcpy( text, "FRAME:    rect   SQUARE" );
else
   (void) strcpy( text, "FRAME:    RECT   square" );
SCTPUT( text );

/*
 * X, Y and Z scaling
 */
if ( strncmp( FRAME, "SQ", 2 ) == 0  )
   (void) strcpy( text, "XSCALE:   auto   xscale" );
else if ( fabs( *(XSCAL) ) < PLT_EPS )
   (void) strcpy( text, "XSCALE:   AUTO   xscale" );
else
  {
    if (strncmp( CXSC, "WO", 2 ) == 0 )
        (void) sprintf( text, "XSCALE:   auto   %-.5g (units/mm)", fabs(*(XSCAL)) );

     else if (strncmp( CXSC, "MM", 2 ) == 0 )
        (void) sprintf( text, "XSCALE:   auto   %-.5g (mm)", fabs(*(XSCAL)) );

     else if (strncmp( CXSC, "SC", 2 ) == 0 )
        (void) sprintf( text, "XSCALE:   auto   %-.5g (screen coord.)", *(XSCAL) );

     else if (strncmp( CXSC, "NO", 2 ) == 0 )
        (void) sprintf( text, "XSCALE:   auto   %-.5g (norm. coord.)", *(XSCAL) );

     else
        (void) sprintf( text, "XSCALE:   auto   %-.5g (units/mm)", *(XSCAL) );
   }
SCTPUT( text );
 
if ( strncmp( FRAME, "SQ", 2 ) == 0  )
   (void) strcpy( text, "YSCALE:   auto   yscale" );
else if ( fabs( *(YSCAL) ) < PLT_EPS )
   (void) strcpy( text, "YSCALE:   AUTO   yscale" );
else
   { if (strncmp( CYSC, "WO", 2 ) == 0 )
        (void) sprintf( text, "YSCALE:   auto   %-.5g (units/mm)", fabs(*(YSCAL)) );

     else if (strncmp( CYSC, "MM", 2 ) == 0 )
        (void) sprintf( text, "YSCALE:   auto   %-.5g (mm)", fabs(*(YSCAL)) );

     else if (strncmp( CYSC, "SC", 2 ) == 0 )
        (void) sprintf( text, "YSCALE:   auto   %-.5g (screen coord.)", *(YSCAL) );

     else if (strncmp( CYSC, "NO", 2 ) == 0 )
        (void) sprintf( text, "YSCALE:   auto   %-.5g (norm. coord.)", *(YSCAL));

     else
        (void) sprintf( text, "YSCALE:   auto   %-.5g (units/mm)", *(YSCAL) );
   }
SCTPUT( text );

/*
if ( fabs( *(ZSCAL) ) < PLT_EPS )
   (void) strcpy( text, "ZSCALE:   AUTO   zscale" );
else
   { if ( *(ZSCAL) > 0.0 )
        (void) sprintf( text, "ZSCALE:   auto   %-.5g (units/mm)", *(ZSCAL) );
     else
        (void) sprintf( text, "ZSCALE:   auto   %-.5g (mm)", - *(ZSCAL) );
   }
SCTPUT( text );
*/

/*
 * offset in X  an d Y
 */
if ( *(XOFF) == -999.0 )
   (void) strcpy( text, "XOFFSET:  DEF    xoffset" );
else
   { if ( (strncmp( CXOF, "WO", 2 ) == 0 ))
        (void) sprintf( text, "XOFFSET:  auto   %-.5g (world coord.)", *(XOFF) );

     else if ( (strncmp( CXOF, "MM", 2 ) == 0 ))
        (void) sprintf( text, "XOFFSET:  auto   %-.5g (mm)", fabs(*(XOFF)) );

     else if ((strncmp( CXOF, "SC", 2 ) == 0 ))
        (void) sprintf( text, "XOFFSET:  auto   %-.5g (screen coord.)", *(XOFF) );

     else if ((strncmp( CXOF, "NO", 2 ) == 0 ))
        (void) sprintf( text, "XOFFSET:  auto   %-.5g (norm. coord.)", *(XOFF) );

     else
        (void) sprintf( text, "XOFFSET:  auto   %-.5g (mm)", *(XOFF) );
   }
SCTPUT( text );

if ( *(YOFF) == -999.0 )
   (void) strcpy( text, "YOFFSET:  DEF    yoffset" );
else
   { if ( (strncmp( CYOF, "WO", 2 ) == 0 ))
        (void) sprintf( text, "YOFFSET:  auto   %-.5g (world coord.)", *(YOFF) );

     else if ( (strncmp( CYOF, "MM", 2 ) == 0 ))
        (void) sprintf( text, "YOFFSET:  auto   %-.5g (mm)", fabs(*(YOFF)) );

     else if ( (strncmp( CYOF  , "SC", 2 ) == 0 ))
        (void) sprintf( text, "YOFFSET:  auto   %-.5g (screen coord.)", *(YOFF) );

     else if ( (strncmp( CYOF  , "NO", 2 ) == 0 ))
        (void) sprintf( text, "YOFFSET:  auto   %-.5g (norm. coord.)", *(YOFF) );

     else
        (void) sprintf( text, "YOFFSET:  auto   %-.5g (mm)", *(YOFF) );
   }
SCTPUT( text );

/*
 * format along the axis
 */
if ( strncmp( XFORM, "NONE", 4 ) == 0 )
   (void) strcpy( text, "XFORMAT:  NONE   auto  x_format" );
else if ( strncmp( XFORM, "G   ", 4 ) == 0 )
   (void) strcpy( text, "XFORMAT:  none   AUTO  x_format" );
else
   { (void) strncpy( fmt, XFORM, 8 );
     (void) sprintf( text, "XFORMAT:  none   auto  %s", fmt );
   }
SCTPUT( text );

if ( strncmp( YFORM, "NONE", 4 ) == 0 )
   (void) strcpy( text, "YFORMAT:  NONE   auto  y_format" );
else if ( strncmp( YFORM, "G   ", 4 ) == 0 )
   (void) strcpy( text, "YFORMAT:  none   AUTO  y_format" );
else
   { (void) strncpy( fmt, YFORM, 8 );
     (void) sprintf( text, "YFORMAT:  none   auto  %s", fmt );
   }
SCTPUT( text );

if ( strncmp( ZFORM, "NONE", 4 ) == 0 )
   (void) strcpy( text, "ZFORMAT:  NONE   auto  z_format" );
else if ( strncmp( ZFORM, "G   ", 4 ) == 0 )
   (void) strcpy( text, "ZFORMAT:  none   AUTO  z_format" );
else
   { (void) strncpy( fmt, ZFORM, 8 );
     (void) sprintf( text, "ZFORMAT:  none   auto  %s", fmt );
   }
SCTPUT( text );

SCTPUT( inf_etc );
/*
 * plot mode
 */
(void) sprintf( text, "PMODE:    %-6d (modes -1, 0, 1 and 2 available)", *(PMODE) );
SCTPUT( text );

/*
 * font type
 */
(void) sprintf( text, "FONT:     %-6d (6 fonts available)", *(FONT) );
SCTPUT( text );

/*
 * line type
 */
(void) sprintf( text, "LTYPE:    %-6d (6 line types available)", *(LTYPE) );
SCTPUT( text );

/*
 * line width
 */
(void) sprintf( text, "LWIDTH:   %-6d (4 line widths available)", *(LWIDTH) );
SCTPUT( text );

/*
 * symbol size
 */
(void) sprintf( text, "SSIZE:    %-.2g", *(SSIZE) );
SCTPUT( text );

/*
 * symbol type
 */
(void) sprintf( text, "STYPE:    %-6d (21 symbol types available)", *(STYPE) );
SCTPUT( text );

/*
 * character size
 */
(void) sprintf( text, "TSIZE:    %-.2g", *(TSIZE) );
SCTPUT( text );

/*
 * character width
 */
(void) sprintf( text, "TWIDTH:   %-6d (4 character widths available)", 
                      *(TWIDTH) );
SCTPUT( text );

/*
 * colour
 */
(void) sprintf( text, "COLOUR:   %-6d (8 colours available)", *(COLOR) );
SCTPUT( text );

/*
 * background colour
 */
(void) sprintf( text, "BCOLOUR:  %-6d (8 colours available)", *(BCOLOR) );
SCTPUT( text );

/*
 * to bin or not to bin
 */
if ( strncmp( BNMODE, "OFF", 3 ) == 0 )
   (void) strcpy( text, "BINMODE:  OFF    on" );
else
   (void) strcpy( text, "BINMODE:  off    ON" );
SCTPUT( text );

/*
 * colour type and mode
 */
if ( strncmp( CLMODE, "X", 1 ) == 0 )
   (void) strcpy( text, "COLMODE:  XOR    sub" );
else
   (void) strcpy( text, "COLMODE:  xor    SUB" );
SCTPUT( text );

if ( strncmp( ERASE, "ON", 2 ) == 0 )
   (void) strcpy( text, "CLEARGRA: ON     off" );
else
   (void) strcpy( text, "CLEARGRA: on     OFF" );
SCTPUT( text );

return SCSEPI();
}
