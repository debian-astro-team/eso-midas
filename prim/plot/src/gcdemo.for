C @(#)gcdemo.for	19.1 (ES0-DMD) 02/25/03 14:08:02
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                    all rights reserved
C.IDENTIFICATION: Program GCURSR
C.PURPOSE:   Extract the coordinates and pixel values: the coordinates can be
C            included/appended in the descriptor, table or keyword.
C.AUTHOR:    Ch. Ounnas   ESO - Garching
C.LANGUAGE:  F77+ESOext
C.KEYWORDS:  Graphics, cursor, table descriptor
C.ALGORITHM: The outines uses the standard interface and AGL plotting routines.
C.VERSION:   850513 KB  ???
C.VERSION:   850925 ChO ???
C.VERSION:   851125 JDP ???
C.VERSION:   8602?? LF  implementation of the AGL plotting routines
C.VERSION:   86???? RHW Completely restructured
C.VERSION:   870211 RHW corr. default for the maximum number of input
C.VERSION:   870629 RHW include of facilitity to read contour plots
C.VERSION:   871113 RHW ESO-FORTRAN Conversion
C.VERSION:   910115 RHW IMPLICIT NONE added
C.VERSION:   910091 RHW Additions to handle plot/row and plot/column plots
C -----------------------------------------------------------------
      PROGRAM GCURSR
      IMPLICIT  NONE

      INTEGER   NCR
      PARAMETER (NCR=1)
C
      INTEGER   ACCESS, PLMODE
      INTEGER   ISTAT, KUN, MADRID(1)
      REAL      RDUM, X, Y
C
      CHARACTER STRING*1
C
      INCLUDE   'MID_INCLUDE:PLTDEC.INC/NOLIST'
      INCLUDE   'MID_INCLUDE:ST_DEF.INC/NOLIST'
      COMMON    /VMR/MADRID
      INCLUDE   'MID_INCLUDE:ST_DAT.INC/NOLIST'
C
C *** start of executable code
      CALL STSPRO('GCURSR')
      STRING = ' '
      ACCESS = -1 
      PLMODE = -1
      CALL PLOPN(' ',ACCESS,PLMODE)
      CALL AGVLOS(X,Y,1,STRING,RDUM)
      IF ((STRING.EQ.'1') .OR.
     2    (STRING.EQ.'2') .OR.
     3   (STRING.EQ.'3') .OR.
     4   (STRING.EQ.'4') .OR.
     5   (STRING.EQ.'5') .OR.
     6   (STRING.EQ.'6') .OR.
     7   (STRING.EQ.'7') .OR.
     8   (STRING.EQ.'8') .OR.
     9   (STRING.EQ.'9') .OR.
     0   (STRING.EQ.'0')) THEN
         CALL STKWRC('WAHL',1,STRING,1,1,KUN,ISTAT)
      ELSE 
         CALL STKWRC('START',1,STRING,1,1,KUN,ISTAT)
      ENDIF
      CALL PLCLS
      CALL STSEPI
      END
