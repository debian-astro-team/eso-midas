/*===========================================================================
  Copyright (C) 1993-2008 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  PCKWRC
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, plot setting
.LANGUAGE    C
.PURPOSE     Set the global plot parameters according to the command:
             SET/GRAP parameter=value ...
                      set automatic or manual mode
                      set the formats of axis
                      set the bin option "on" or "off"
                      set the lines of the contour plotting
                      set the plot mode (simple to advanced)
                      set the line type at 0 to 7
                      set the line width 
                      set the symbol type 0 to 16
                      set the colour type
                      set the plot frame in case of manual plotting

             IF P1 == "" or "DEF"ault ==> all PLOT keywords are set to default
             (see PCKDEF, PCKWRC, PCKWRI or PCKWRR)

.COMMENTS    Correspondence between parameter values and keyword:
             -- Note that the parameter names do not always have the same
                names as those in PCK.C!!! This is due to the fact that we
                can only change a subset of all the parameters of PCK.C

            PARAM.NAME  POSITION DEFAULT   DESCRIPTION
            ----------  -------- -------   -----------
   KEYWORD  PLCSTAT
            BINMODE       1 -  4    OFF    bin mode "ON  " or "OFF "
            COLMODE       5 -  8    SUB    colour mode: "Sub" or "Xor"
            CLEARGRA      9 - 12    ON     erase for plot commands
            DEBUG        13 - 16    OFF    debug mode: "ON" or "OFF"
            FRAME        17 - 20    AUTO   AUTO scaling or SQUAre frame
            XFORMAT      21 - 28    AUTO   format of values along the X-axis
            YFORMAT      29 - 36    AUTO   format of values along the Y-axis
            ZFORMAT      37 - 44    AUTO   format of values along the Z-axis
            CXSC         45 - 48    WO     Coordinates for the X scale
            CYSC         49 - 52    WO     Coordinates for the Y scale
            CXOF         53 - 56    MM     Coordinates for the X offset
            CYOF         57 - 62    MM     Coordinates for the Y offset

   KEYWORD  PLISTAT
            PMODE         1 -  1    2      pmode mode (ranging from -1 to 2)
            FONT          2 -  2    0      font used (ranging from 0 to 6)
            free          3 -  4
            LTYPE         5 -  5    1      line line (ranging from 0 to 6)
            STYPE         6 -  6    5      symbol type (ranging from 0 to 21)
            LWIDT         7 -  7    1      line width (ranging from 1 to 4)
            TWIDT         8 -  8    1      text width (ranging from 1 to 4)
            COLOUR        9 -  9    1      colour (ranging from 0 to 8)
            BCOLOR       10 - 10    0      background colour (0 to 8)

   KEYWORD  PLRSTAT
            XAXIS         1 -  4  0,0,0,0  x axis frame limits and ticks
            YAXIS         5 -  8  0,0,0,0  y axis frame limits and ticks
            ZAXIS         9 - 12  0,0,0,0  z axis frame limits and ticks
            XSCALE       13 - 13    0.0    scale set by user in x
            YSCALE       14 - 14    0.0    scale set by user in y
            ZSCALE       15 - 15    0.0    scale set by user in z
            XOFFSET      21 - 21 -999.0    offset in mm set by user in x
            YOFFSET      22 - 22 -999.0    offset in mm set by user in y
            free         23 - 24
            SSIZE        25 - 25    1.0    symbol multiplier
            TSIZE        26 - 26    1.0    text size multiplier

.ENVIRONment MIDAS
             #include <midas_def.h>     Prototypes for MIDAS interfaces

.VERSION     1.1     06-Oct-1993   Created by R.M. van Hees

 081203		last modif

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <string.h>

#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <plot_def.h>
#define MAXARR		4                 /* max. num. of values per parameter */
#define CNUM	       20                 /* number of parameters in PLCSTAT */
#define INUM		8                 /* number of parameters in PLISTAT */
#define RNUM		2                 /* number of parameters in PLRSTAT */

/*
 * here start the code of the function
 */
int main()
{
int    actvals, found, knul, ltype, nrpar, nrval, stype, unit;
int    ival[MAXARR];
float  fval[MAXARR];
char   *cbuff, *eqpntr, *pnam, *input, *name, *value, *cstring;

register int  ipar, nc, ni, nr;

/*
 * initialised variables
 */
int  l2zero = FALSE,                       /* flags used to detect if both  */
     s2zero = FALSE;                       /* ltype & stype are set to zero */
char *err_inp   = "*** FATAL: wrong syntax or not existing keyword",
     *war_sltyp = "*** WARNING: Both LTYPE and STYPE are set to 0 !!";
/*
 * initialised variables, see also PCK.C
 */                                                 /* names of the keywords */
static char *ckeynam[CNUM] = { "BINMODE", "COLMOD", "CLEAR", "DEBUG", 
                               "FRAME",   "XFORM",  "YFORM", "ZFORM", 
                               "CXSC",    "CYSC",   "CXOF",  "CYOF",
                               "XAXIS",   "YAXIS",  "ZAXIS", "XSCALE", 
                               "YSCALE", "ZSCALE", "XOFF", "YOFF"}, 
            *ikeynam[INUM] = { "PMODE", "FONT", "LTYPE", "STYPE", "LWIDTH", 
                               "TWIDTH", "COLOUR", "BCOLOUR" },
            *rkeynam[RNUM] = { "SSIZE", "TSIZE" };

                /* minimum number op characters needed to identify a keyword */
static int ckeyiden[CNUM] = { 2, 4, 2, 2, 
                              2, 2, 2, 2, 
                              3, 3, 3, 3,
                              2, 2, 2, 2, 
                              2, 2, 2, 2},
           ikeyiden[INUM] = { 2, 2, 2, 2, 2, 
                              2, 4, 2 },
           rkeyiden[RNUM] = { 2, 2 };

/*
 * allocate memory for different character pointers
 */
pnam    = osmmget(5);
input   = osmmget(81);
name    = osmmget(81);
value   = osmmget(81);
cstring = osmmget(81);
/*
 * start of executable code
 */
(void) SCSPRO( "SETPLT" );                   /*contact with the MIDAS monitor*/

/*
 * do the work
 */
(void) SCKRDI( "PCOUNT", 1, 1, &actvals, &nrpar, &unit, &knul );

/*
 * set to defaut if no parameter is given
 */
if ( nrpar == 0 ) PCKDEF();

/*
 * set the given parameters
 */
for ( ipar = 0; ipar < nrpar; ipar++ )
    { found = FALSE;
/*
 * read the input parameters one by one
 */ 
      (void) sprintf( pnam, "P%-d", ipar+1 );
      (void) SCKGETC( pnam, 1, 80, &actvals, input );
      CGN_UPSTR( input );
      if ( (eqpntr = strchr( input, '=' )) == NULL )
         { (void) strcpy( name, input );
           (void) strcpy( value, "DEFAULT" );
         }
      else
         { (void) strncpy( name, input, (eqpntr - input) );
           (void) strcpy( value, eqpntr+1 );
         }

/*
 * do we have to reset the keyword values to default ??
 */
      if ( strncmp( name, "DEF", 3 ) == 0 )
         { found = TRUE;
           PCKDEF();
         }
/*
 * do we have to change a character keyword ??
 */
      if ( ! found )
         { nc = 0;
           while ( nc < CNUM 
                   && strncmp( name, ckeynam[nc], ckeyiden[nc] ) != 0 )
                 { nc++; }
           if ( nc < CNUM ) 
              { found = TRUE;

/* 
 * Exception for XSC, YSC, XOF, and YOF with "value,char" input
 */
                if ( ( strncmp( name, "XS", 2 ) == 0 ) ||
                     ( strncmp( name, "YS", 2 ) == 0 ) ||
                     ( strncmp( name, "XO", 2 ) == 0 ) ||
                     ( strncmp( name, "YO", 2 ) == 0 ) )
                   {
  
                   if ( (eqpntr = strchr( value, ',' )) == NULL )
                      {
                      if (strncmp( value, "M", 1) == 0)
                         {
                         (void) strcpy(value, strtok( value, "M" ) );
                         (void) strcat (value,",MM");
                         }

                       else if (strncmp( value, "-", 1 ) == 0)
                         {
                         (void) strcpy(value, (strtok( value, "-" )-1) );
                         (void) strcat (value,",MM"); 
                         }

                       else if (strncmp( value, "S", 1 ) == 0)
                         {
                         (void) strcpy(value, strtok( value, "S" ) );
                         (void) strcat (value,",SC"); 
                         }

                       else if (strncmp( value, "@", 1 ) == 0)
                         {
                         (void) strcpy(value, strtok( value, "@" ) );
                         (void) strcat (value,",SC"); 
                         }

                       else if (strncmp( value, "N", 1 ) == 0)
                         {
                         (void) strcpy(value, strtok( value, "N" ) );
                         (void) strcat (value,",NO"); 
                         }

                       else if (strncmp( value, "W", 1 ) == 0)
                         {
                         (void) strcpy(value, strtok( value, "W" ) );
                         (void) strcat (value,",WO"); 
                         }

                       else
                         if ( ( strncmp( name, "XS", 2 ) == 0 ) ||
                              ( strncmp( name, "YS", 2 ) == 0 ) )
                            (void) strcat (value,",WO"); 
                         else
                           (void) strcat (value,",MM");
                
     	              eqpntr = strchr( value, ',' );
		      }


                   if ( strncmp( name, "XS", 2 ) == 0 ) 
                      { (void) strncpy( cstring, value, (eqpntr - value));
                        cstring[eqpntr - value] = '\0';
                        PCKWRC( name, cstring);
	                strcpy(name,"CXSC");
                        *cstring = '\0';
                        (void) strcpy( cstring, eqpntr+1 );
                        PCKWRC( name, cstring);
      	              }
		   		   
                   else if ( strncmp( name, "YS", 2 ) == 0 )
                      { (void) strncpy( cstring, value, (eqpntr - value));
                        cstring[eqpntr - value] = '\0';
                        PCKWRC( name, cstring);
		        strcpy(name,"CYSC");
                        *cstring = '\0';
                        (void) strcpy( cstring, eqpntr+1 );
                        PCKWRC( name, cstring);
		      }

                   else if ( strncmp( name, "XO", 2 ) == 0 )
                      { (void) strncpy( cstring, value, (eqpntr - value));
                       cstring[eqpntr - value] = '\0';
                       PCKWRC( name, cstring);
		       strcpy(name,"CXOF");
                       *cstring = '\0';
                       (void) strcpy( cstring, eqpntr+1 );
                       PCKWRC( name, cstring);
                      }
 
                   else if ( strncmp( name, "YO", 2 ) == 0 )
                      { (void) strncpy( cstring, value, (eqpntr - value));
                        cstring[eqpntr - value] = '\0';
                        PCKWRC( name, cstring);
	  	        strcpy(name,"CYOF");
                        *cstring = '\0';
                        (void) strcpy( cstring, eqpntr+1 );
                        PCKWRC( name, cstring);
                      }
		   }

                 else
                    PCKWRC( name, value );
              }

	 }
      if ( ! found )

/*
 * do we have to change an integer keyword ??
 */
      if ( ! found )
         { ni = 0;
           while ( ni < INUM 
                   && strncmp( name, ikeynam[ni], ikeyiden[ni] ) != 0 )
                 { ni++; }
           if ( ni < INUM ) 
              { found = TRUE;
 
                if (((strncmp(name,"COLO",4) == 0) || 	/* for COLOUR       */
                     (strncmp(name,"BCOL",4) == 0))  	/* and BCOLOUR      */
                    &&
                    (CGN_NUMBER(value) == 0))		/* look for STRING  */
                   {
                   *ival = 0;
                   if (*value == 'R') 
                      *ival = 2;
                   else if (*value == 'G') 
                      *ival = 3;
                   else if (*value == 'Y') 
                      *ival = 5;
                   else if (*value == 'M') 
                      *ival = 6;
                   else if (*value == 'C') 
                      *ival = 7;
                   else if (*value == 'W') 
                      *ival = 8;
                   else if (*value == 'B') 
                      {
                      if (*(value+2) == 'A')		/* black */
                         *ival = 1;
                      else if (*(value+2) == 'U')	/* blue */
                         *ival = 4;
                      }
                   if (*ival > 0) 			/* any colour match? */
                      {
                      nrval = 1;
                      PCKWRI(name,nrval,ival);
		      goto check_float;			/* skip numeric test */
                      }
                   }

                nrval = 0;
                if (( strncmp( value, "DEF", 3 ) != 0 ) &&
                    (value[0] != '\0' ))
 		   { cbuff = (char *) ival;
                     (void) USRINP( 'i', value, MAXARR, cbuff, &nrval );

                     PCKWRI( name, nrval, ival );
                     if ( *ival == 0 )
                        { if ( ni == 2 ) l2zero = TRUE;
                          if ( ni == 3 ) s2zero = TRUE;
		        }
		   }
                else
		  { nrval = 0;
                    ival[0] = 0;
                    PCKWRI( name, nrval, ival );
		  }
              }
         }

/*
 * do we have to change a floating point keyword ??
 */

check_float:
      if ( ! found )
         { nr = 0;
           while ( nr < RNUM 
                   && strncmp( name, rkeynam[nr], rkeyiden[nr] ) != 0 )
                 { nr++; }
           if ( nr < RNUM ) 
              { found = TRUE;
                nrval = 0;
                cbuff = (char *) fval;
                if ( strncmp( value, "DEF", 3 ) != 0 )
                   (void) USRINP( 'f', value, MAXARR, cbuff, &nrval );
                PCKWRR( name, nrval, fval );
              }
         }
/*
 * exit if the given keyword does not exists
 */
      if ( ! found ) (void) SCETER( 1, err_inp );
    }

if ( l2zero && s2zero )                     /* ltype & stype are set to zero */
   SCTPUT( war_sltyp );
else if ( l2zero )                                            /* check stype */
   { PCKRDI( "STYPE", 1, &actvals, &stype );
     if ( stype == 0 ) SCTPUT( war_sltyp );
   }
else if ( s2zero )                                            /* check ltype */
   { PCKRDI( "LTYPE", 1, &actvals, &ltype );
     if ( ltype == 0 ) SCTPUT( war_sltyp );
   }

return SCSEPI();
}








