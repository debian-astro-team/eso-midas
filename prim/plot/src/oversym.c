/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  OVRSYM
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, table, one-dimensional plotting
.LANGUAGE    C
.PURPOSE     Plot symbol in an existing plot
      input: INPUTC/C/1/80 = position of symbol in world coordinates 
                               or "C" for interactive curcur
             INPUTI/I/1/1  = s_type (default: cross + )
             INPUTR/R/1/1  = symbol size

.COMMENTS    none
.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      General symbols for Plot routines

.VERSION     1.1     13-Sep-1993   FORTRAN --> C         RvH

 090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <string.h>
#include <stdio.h>

#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <plot_def.h>

#undef  REAL
#define REAL		2                  /* type conversion in CGN-routine */
#define NRXY		2      /*maximal no. of output values for CGN-routine*/

/*
 * here starts the code of the function
 */
int main()
{
int     actvals, color, knul, key, len, stype, move, pixval, unit,
        idum[2], xysize[2], ixmin, ixmax, iymin, iymax, stat;
float   dval[2], scale, size, xw, yw, xy[2], xant, yant, 
        xmin, xmax, ymin, ymax, clpl[4], wcfram[8];
double  ddum[2];
char    *pntr, copt[3], draw[13], nodraw[13], cinput[81], cstring[81], 
        text[81], cmnd[256];

char    *device, *dname, *kname;

char *fmt      = "OVERPLOT/SYMBOL %2d %12.6f, %12.6f %5.1f",
     *err_coor = "*** FATAL: Illegal coordinate input, try again",
     *err_noln = "*** FATAL: Symbol type is ZERO (0); invisible!",
     *err_styp = "*** FATAL: Undefined symbol type: try again",
     *err_xmm = "*** FATAL: x coordinate should fall between: %6.1f and %6.1.f mm",
     *err_ymm = "*** FATAL: y coordinate should fall between: %6.1f and %6.1f mm",
     *err_sc   = "*** FATAL: x/y screen coordinate illegal; display has %4d x %4d screen pixels",
     *errdev   = "*** FATAL: screen coordinates only supported for display devices";

/*
 * initialised variables
 */
int     access =  1,                 /* parameter for PCOPEN: overplot mode  */
        plmode = -1,                   /* plot mode taken from keyword PMODE */
        nocolo = 8,
        pos    = FALSE;

static float  nclip[] = { 0.0, 1.0, 0.0, 1.0 },
              nfram[] = { 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0 };
        
/*
 * start of executable code
 */
(void) SCSPRO( "OVRSYM" );                   /*contact with the MIDAS monitor*/

/*
 * get User parameters
 */
(void) SCKRDI( "INPUTI", 1, 1, &actvals, &stype, &unit, &knul );
if ( stype == 0 ) SCETER( 1, err_noln );
if ( stype < 0 || (stype > 21 && stype < 32) || stype > 126 ) 
   SCETER( 2, err_styp );
if ( stype < 22 ) stype -= 1;

/*
 * get coordinates
 */
(void) SCKGETC( "INPUTC", 1, 80, &actvals, cinput );
CGN_LOWSTR( cinput );
if ( *cinput != 'c' )
   { pos = TRUE;

     if ( (pntr = strstrs( cinput, ",wo" )) != NULL )
        { (void) strcpy( copt, "wo" );
          (void) strncpy( cstring, cinput, (pntr - cinput));
          cstring[pntr - cinput] = '\0';
	}
     else if ( (pntr = strstrs( cinput, ",mm" )) != NULL )
        { (void) strcpy( copt, "mm" );
          (void) strncpy( cstring, cinput, (pntr - cinput));
          cstring[pntr - cinput] = '\0';
        }
     else if ( (pntr = strstrs( cinput, ",sc" )) != NULL )
        { (void) strcpy( copt, "sc" );
          (void) strncpy( cstring, cinput, (pntr - cinput));
          cstring[pntr - cinput] = '\0';
        }
     else if ( (pntr = strstrs( cinput, ",no" )) != NULL )
        { (void) strcpy( copt, "no" );
          (void) strncpy( cstring, cinput, (pntr - cinput));
          cstring[pntr - cinput] = '\0';
        }
     else
        { (void) strcpy( copt, "wo" );
          (void) strcpy( cstring, cinput );
        }

     actvals = CGN_CNVT( cstring, REAL, NRXY, idum, xy, ddum );
     if ( actvals < 2 )
        { SCTPUT( err_coor );
          (void) SCSEPI();
        }
     else
        { xw = xy[0];
          yw = xy[1];        }
   }

/*
 * setup graphic device according to MIDAS settings
 */
PCOPEN( " ", " ", access, &plmode );

/*
 * get symbol size 
 */ 
(void) AG_RGET( "SCALE", &scale );
(void) SCKRDR( "INPUTR", 1, 1, &actvals, &size, &unit, &knul );
(void) sprintf( text, "sydi= %.3f", scale * size );
AG_SSET( text );

/*
 * get viewport settings
 */
PCKRDR( "CLPL",  4, &actvals, clpl );
PCKRDR( "XWNDL", 4, &actvals, wcfram );
PCKRDR( "YWNDL", 4, &actvals, wcfram+FOR_Y );

/*
 * symbol by coordinate input of cursor
 */
if ( pos )
   { if ( strncmp( copt, "wo", 2 ) == 0 )
        { nfram[0] = wcfram[0] -
                     clpl[0] * (wcfram[1] - wcfram[0])/(clpl[1] - clpl[0]);
          nfram[1] = wcfram[1] + (1.0 - clpl[1]) * (wcfram[1] - wcfram[0])/
                                                   (clpl[1] - clpl[0]);
          nfram[2] = wcfram[2];
          nfram[3] = wcfram[3];
          nfram[4] = wcfram[4] -
                     clpl[2] * (wcfram[5]-wcfram[4]) / (clpl[3] - clpl[2]);
          nfram[5] = wcfram[5] + (1.0 - clpl[3]) * (wcfram[5] - wcfram[4])/
                                                   (clpl[3] - clpl[2]);
          nfram[6] = wcfram[6];
          nfram[7] = wcfram[7];
        }

     else if ( strncmp( copt, "mm", 2 ) == 0 )
        { (void) AG_RGET( "devd", dval );

          /* This part is special for the ESO-DFO group
          and redefines the coordinate reference point for mm */
          SCKGETC("PLCSTAT",74, 7, &actvals, cinput );
          if ( (pntr = strstrs( cinput, "ESO-DFO" )) != NULL )
             { xmin = - 10 * clpl[0] * dval[0];
               xmax = 10 * (1 - clpl[0]) * dval[0];
               ymin = - 10 * clpl[2] * dval[1];
               ymax = 10 * (1 - clpl[2]) * dval[1];}
 
          else
	    /* This is the default none ESO-DFO part */
             { xmin = 0.0;
               xmax = 10 * dval[0];
               ymin = 0.0;
               ymax = 10 * dval[1];}

          if ( xw < xmin || xw > xmax )
             { (void) sprintf( text, err_xmm, xmin, xmax );
               SCTPUT( text );
               SETWND( wcfram );
               (void) SCSEPI();
             }
          if ( yw < ymin || yw > ymax )
             { (void) sprintf( text, err_ymm, ymin, ymax );
               SCTPUT( text );
               SETWND( wcfram );
               (void) SCSEPI();
             }
	  /* This part is special for the ESO-DFO group
             and redefines the coordinate reference point */
          if ( (pntr = strstrs( cinput, "ESO-DFO" )) != NULL )
             { xw = clpl[0] + xw / (10 * dval[0]);
               yw = clpl[2] + yw / (10 * dval[1]);}

          else
             /* This is the default none ESO-DFO part */
             { xw = xw / (10 * dval[0]);
               yw = yw / (10 * dval[1]);}
          }


     else if ( strncmp( copt, "sc", 2 ) == 0 )
        { 
          device = osmmget(31);
          kname  = osmmget(21);
          dname  = osmmget(21);
          (void) SCKGETC( "MID$PLOT", 1, 30, &actvals, device );
          CGN_LOWSTR( device );
          (void) strncpy( kname, device, 20 );
          stat = GETDEV( kname, dname );
          if (strncmp( dname, "graph", 5 ) == 0 )
             (void) SCKRDI("IDIDEV", 32, 2, &actvals, xysize, &unit, &knul);
          else if (strncmp( dname, "image", 5 ) == 0 )
             (void) SCKRDI("IDIDEV", 2, 2, &actvals, xysize, &unit, &knul);
          else 
             (void) SCETER( 2, errdev );

          ixmin = 0;
          ixmax = xysize[0]-1;
          iymin = 0;
          iymax = xysize[1]-1;

          if ( xw < ixmin || xw > ixmax )
             { (void) sprintf( text, err_sc, ixmax, iymax );
               SCTPUT( text );
               SETWND( wcfram );
               (void) SCSEPI();
             }
          if ( yw < iymin || yw > iymax )
             { (void) sprintf( text, err_sc, ixmax, iymax );
               SCTPUT( text );
               SETWND( wcfram );
               (void) SCSEPI();
             }
          xw = xw/xysize[0];
          yw = yw/xysize[1];
        }

     else if ( strncmp( copt, "no", 2 ) == 0 )
        {
          xmin = 0.0;
          xmax = 1.0;
          ymin = 0.0;
          ymax = 1.0;

          if ( xw < xmin || xw > xmax )
             { (void) sprintf( text, err_xmm, xmin, xmax );
               SCTPUT( text );
               SETWND( wcfram );
               (void) SCSEPI();
             }
          if ( yw < ymin || yw > ymax )
             { (void) sprintf( text, err_ymm, ymin, ymax );
               SCTPUT( text );
               SETWND( wcfram );
               (void) SCSEPI();
             }
 	}

     (void) AG_CDEF( nclip[0], nclip[1], nclip[2], nclip[3] );
     SETWND( nfram );
     AG_GPLM( &xw, &yw, 1, stype );
     xant = xw;
     yant = yw;
     move = 1;
   }

else
   { nfram[0] = wcfram[0] -
                clpl[0] * (wcfram[1] - wcfram[0]) / (clpl[1] - clpl[0]);
     nfram[1] = wcfram[1] + (1.0 - clpl[1]) * (wcfram[1] - wcfram[0])/
                                              (clpl[1] - clpl[0]);
     nfram[2] = wcfram[2];
     nfram[3] = wcfram[3];
     nfram[4] = wcfram[4] -
                clpl[2] * (wcfram[5]-wcfram[4]) / (clpl[3]-clpl[2]);
     nfram[5] = wcfram[5] + (1.0 - clpl[3]) * (wcfram[5] - wcfram[4])/
                                              (clpl[3] - clpl[2]);
     nfram[6] = wcfram[6];
     nfram[7] = wcfram[7];
     (void) AG_CDEF( nclip[0], nclip[1], nclip[2], nclip[3] );
     SETWND( nfram );

/*
 * get color
 */ 
     (void) AG_IGET( "colo", &color );
     (void) sprintf( draw, "color= %1d", color );
     (void) sprintf( nodraw, "color= %1d", nocolo );

     AG_MSUS();
     key  = 1; 
     move = 0;
     while ( key == 1 )
           { AG_VLOC( &xw, &yw, &key, &pixval );
             if ( move == 1 )
                { AG_SSET( nodraw );
                  AG_GPLM( &xant, &yant, 1, stype );
                  AG_VUPD();
                }
             AG_SSET( draw );
             if ( key == 1 )
                { AG_GPLM( &xw, &yw, 1, stype );
                  AG_VUPD();
                  xant = xw;
                  yant = yw;
                  move = 1;
                }   
           }
     if ( move == 1 )
        { AG_MRES();
          AG_GPLM( &xant, &yant, 1, stype );
        }
   }

if (move == 1)
   {
   (void) sprintf( cmnd, fmt, stype, xant, yant, size );
   len = strlen( cmnd );
   (void) SCKWRC("MID$LINE", 1, cmnd, 1, len, &unit );
   }
/*
 * goodbye and finish
 */
(void) AG_CDEF( clpl[0], clpl[1], clpl[2], clpl[3] );
SETWND( wcfram );

PCCLOS();

return SCSEPI();
}

