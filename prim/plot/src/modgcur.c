/*===========================================================================
  Copyright (C) 1993-2011 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   MODGCUR
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, cusror interaction, bulk data frame
                       modification line profile
.LANGUAGE    C
.PURPOSE     Modify a (portion of a) line interactively using cursor control
  in/output: IN_A/C/1/60   = input frame
             P2/C/1/72     = line number (default 1)
             INPUTC/C/1/72 = first and last pixel of the line (default the
                             whole line) in case manual plotting in x has
                             been specified this range will be taken; else
                             the plot will be made in auto scaling mode.
             INPUTI/I/1/2  = no. of cursor positions
                             degree of polynomial for the interpolation

.COMMENTS    none
.ENVIRONment MIDAS and AGL
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      General symbols for Plot routines

.VERSION     1.1     13-Sep-1993   FORTRAN --> ANSI-C    RvH

 110930		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <plot_def.h>

/*
 * here starts the code of the function
 */
int main()
{
int     actvals, ii, ibar, imf, knul, go_on, naxis, ncur, ndeg, nrprow, 
        binmod, stype, ltype, unit, npix[DIMMAX], ival[2], isize;

float   *xdata, *ydata, area[4], image[4], wcfram[8];

double  start[DIMMAX], step[DIMMAX];

char    bin[5], name[62], ident[75], cunit[66], input[73], buff[81], *label[4];
char    *cpntr, *pntr;

/*
 * initialised variables & semi-constants; they ought not to be changed
 */
int   access = 0;                           /* make a new plot, no over plot */
int   plmode = 1;                     /* large plot with frame, minumal info */
float y_off  = 0.0;                            /* no offset along the y-axes */
char  *err_1dim  = "*** WARNING: Image column contains only one point!",
      *err_flat  = "*** WARNING: zero dynamic range in data at %13.8g";

static float scales[] = { 0.0, 0.0 },   /* set scales and offset of the plot */
             offset[] = { -999.0, -999.0 };    /* to default values, defined */
                                                                /* in PCOPEN */
static char  *axis[PLDIM2] = { "MANU", "MANU" };

/*
 * allocate memory for different character pointers and initialise a few
*/
for ( ii = 0; ii < 4; ii++ ) label[ii] = osmmget(81);

(void) strcpy( label[0], "Position (" );
(void) strcpy( label[1], "Pixel value (" );
(void) strcpy( label[2], "Image: " );
(void) strcpy( label[3], "Row: #" );

/* 
 * necessary initialisation for SCIGET
*/
memset((void *)ident,32,(size_t)74);
ident[74] = '\0';
memset((void *)cunit,32,(size_t)64);
cunit[64] = '\0';

/*
 * start of executable code
 */
(void) SCSPRO( "MODIFY" );                   /*contact with the MIDAS monitor*/

/*
 * find file name and read header information
 */
(void) SCKGETC( "IN_A", 1, 60, &actvals, name );
(void) SCIGET( name, D_R4_FORMAT, F_IO_MODE,F_IMA_TYPE, PLDIM2, 
               &naxis, npix, start, step, ident, cunit, &cpntr, &imf );

if ( npix[0] == 1 ) SCTPUT( err_1dim );

/*
 * Get the manual setting for the axes
 */
PCKRDR( "XAXIS", 4, &actvals, wcfram );
PCKRDR( "YAXIS", 4, &actvals, wcfram+FOR_Y );

/*
 * line number (Y-coordinate); default line number = 1
 */
image[2] = 1;
if ( naxis > 1 )
   { (void) SCKGETC( "IN_B", 1, 60, &actvals, input );
     image[2] = image[3] = (float) GETSIN( input, npix[1], start[1], step[1] );
     BOXPTW( image+2, npix[1], start[1], step[1], area+2 );
   }
image[3] = image[2];
/*
 * find first and last pixel along the row (X-coordinate)
 */
(void) SCKGETC( "INPUTC", 1, 72, &actvals, input );

if ( *input == 'M' )
   { BOXWTP( wcfram, npix[0], start[0], step[0], image );
     BOXPTW( image, npix[1], start[0], step[0], area );
   }
else
   { (void) strcpy( buff, input );
     image[0] = (float) GETSIN( strtok(buff,","), npix[0], start[0], step[0] );
     pntr = strchr( input, ',' );
     image[1] = (float) GETSIN( pntr+1, npix[0], start[0], step[0] );
     BOXPTW( image, npix[0], start[0], step[0], area );
   }
PCKWRR( "PIXEL", 4, image );

/*
 * allocate virtual memory for a single line of the frame
 */
isize  = npix[0] * npix[1];
nrprow = (int) fabs(image[1] - image[0]) + 1;   /*number of points in the row*/
xdata  = (float *) osmmget( nrprow * sizeof(float));
ydata  = (float *) osmmget( nrprow * sizeof(float));
GETBDF( cpntr, image, npix, start, step, xdata, ydata );
/*
 * calculate frame along X-axis
 */
if ( fabs( *wcfram ) < PLT_EPS && fabs( *(wcfram+1) ) < PLT_EPS )
   { axis[0] = "AUTO";
     wcfram[0] = area[0];
     wcfram[1] = area[1]; 
     wcfram[2] = wcfram[3] = 0.0;
   }
/*
 * calculate frame along Y-axis
 */
if ( fabs( *(wcfram+FOR_Y)) < PLT_EPS 
      && fabs( *(wcfram+FOR_Y+1)) < PLT_EPS )
   { axis[1] = "AUTO";
     MINMAX( ydata, nrprow, wcfram + FOR_Y, wcfram + FOR_Y + 1 );
     if ( wcfram[FOR_Y] == wcfram[FOR_Y+1] )
        { (void) sprintf( buff, err_flat, wcfram[FOR_Y] );
          SCTPUT( buff );
        }
     wcfram[FOR_Y+2] = wcfram[FOR_Y+3] = 0.0;
   }
GETFRM( axis[0], wcfram );
GETFRM( axis[1], wcfram + FOR_Y );

/*
 * set PLGRAP
 */
PCKWRR( "XWNDL", 4, wcfram );
PCKWRR( "YWNDL", 4, wcfram+FOR_Y );
PCKWRR( "SCALES", 2, scales );
PCKWRR( "OFFSET", 2, offset );

/*
 * setup graphic device according to MIDAS settings
 */
PCOPEN( " ", " ", access, &plmode );

/*
 * get the number of cursor positions and the interpolation
 */
(void) SCKRDI( "INPUTI", 1, 2, &actvals, ival, &unit, &knul );
ncur = ival[0];
ndeg = ival[1];

/*
 * get the symbol type, line type and binmode
 */
PCKRDI( "STYPE", 1, &actvals, &stype );
PCKRDI( "LTYPE", 1, &actvals, &ltype );
PCKRDC( "BINMOD", 4, &actvals, bin );
binmod = (strncmp( bin, "ON", 2 ) == 0) ? 1 : 0;

/*
 * set the labels
 */
/*
  if ( strlen( cunit ) > 0 )

*/
  if ( strlen( cunit ) > (size_t) 0 )
   { (void) strncat( label[0], cunit+17, 16 );
     (void) strncat( label[1], cunit, 16 );
   } 
for ( ii = 0; ii < PLDIM2; ii++ )
    { (void) strcat( label[ii], ")" );
      LABSTR( label[ii] );
    }
(void) strcat( label[2], name );
(void) sprintf( buff, "%-.0f", image[2] );
(void) strcat( label[3], buff );
     
/*
 * do the work
 */
ibar = 0;                  /* number of times the BAR is pressed by the user */
go_on = TRUE;                                     /* we just start, so go on */
while ( go_on )
      { AG_VERS();                                 /* erase current viewport */
/*
 * plot data in current viewport
 */
        PCDATA( stype, ltype, binmod, xdata, ydata, y_off, nrprow );
/*
 * draw axes and labels
 */
        PCFRAM( wcfram, wcfram+FOR_Y, label[0], label[1] );
        PLIDEN( plmode, label[2], label[3] );
/*
 * let the user interactively modify his/her data
 */
        PLMODF( cpntr, image, npix, start, step, ncur, ndeg, binmod,
                &go_on, &ibar );
/*
 * copy data of row to be plotted
 */
        GETBDF( cpntr, image, npix, start, step, xdata, ydata );
      }
/*
 * write modified data to frame
*/

(void) SCFPUT( imf, 1, isize, cpntr );

/*
 * close plot file and terminate graphic operations
 */
PCCLOS();

return SCSEPI();
}
