/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   CLEGRA
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    graphics, erase
.LANGUAGE    C
.PURPOSE     Clear graphic screen and to return to alpha-num mode

.COMMENTS    none
.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces

.VERSION     1.0     26-Jun-1993   FORTRAN --> ANSI-C    RvH

 090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <string.h>

#include <midas_def.h>

/*
 * here start the code of the function
 */
int main()
{ 
int   actvals;
char  device[21], dname[21];

char *err_gdev = "*** FATAL: error detected in GETDEV";

/*
 * start of executable code
 */
(void) SCSPRO( "CREGRA" );                   /*contact with the MIDAS monitor*/

(void) SCKGETC( "MID$PLOT", 1, 20, &actvals, device );
CGN_LOWSTR(device);  

if ( GETDEV( device, dname ) != ERR_NORMAL ) (void) SCETER( 1, err_gdev );

if ( strncmp( dname, "graph", 5 ) != 0
     && strncmp( dname, "image", 5 ) != 0 )
   SCTPUT( "*** WARNING: No graphics display unit: NO ACTION" );
else
   { (void) strcat( dname, ":" );
     (void) AG_VDEF( dname, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0 );
     AG_VERS();
     AG_CLS();
   }
return SCSEPI();
}
