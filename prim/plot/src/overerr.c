/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  OVERERR
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    Graphics, table, one-dimensional plotting
.LANGUAGE    C
.PURPOSE     Plots errorbars from one or two columns of a table
      input: P1/C/1/61    = input table
             P2/C/1/40    = reference column one (X-axis)
             P3/C/1/40    = reference column two (Y-axis)
             P4/C/1/40    = error column of the table
             INPUTI/I/1/1 = orientation of the error bar
             INPUTC/C/1/1 = cross bar at the end

.COMMENTS    none
.ENVIRONment MIDAS
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      General symbols for Plot routines

.VERSION     1.1     13-Sep-1993   FORTRAN --> ANSI-C    RvH

 090710		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

/*
 * define some macros and constants
 */
#include <midas_def.h>
#include <plot_def.h>

/*
 * here starts the code of the function
 */
int main()
{
int     actvals, allcol, allrow, ii, colnr, knul, loc, nrcol, 
        nrrow, nsort, tid, unit, ilog[3], col[3], dtype[3], len[3];

char    bar[2], table[61], sel[81], *fmt[3], *column[3];
char    *pntr;

/*
 * initialised variables
 */
int  access =  1,                    /* parameter for PCOPEN: overplot mode  */
     plmode = -1;                      /* plot mode taken from keyword PMODE */

char  *err_clmn  = "*** FATAL: you should give at least one column",
      *err_row   = "*** FATAL: no points in this table....",
      *err_ercl  = "*** FATAL: no default for the error column",
      *err_nmcol = "*** FATAL: can not find column with this name",
      *err_fcol  = "*** FATAL: column has character format";
/*
 * allocate memory for different character pointers and initialise a few
 */

for ( ii = 0; ii < 3; ii++ )
    { col[ii]    = 1;
      fmt[ii]    = osmmget(17);
      column[ii] = osmmget(41);
    } 
/*
 * start of executable code
 */
(void) SCSPRO( "OVRERR" );                   /*contact with the MIDAS monitor*/

/*
 * find file name and read header information
 */
(void) SCKGETC( "P1", 1, 60, &actvals, table );

/*
 * Try to open table, this routine EXITS the program if the table can not 
 * be found !!
 */
(void) TCTOPN( table, F_I_MODE, &tid );

/*
 * read column identifiers
 */
(void) SCKGETC( "P2", 1, 40, &actvals, column[0] );
(void) SCKGETC( "P3", 1, 40, &actvals, column[1] );
(void) SCKGETC( "P4", 1, 40, &actvals, column[2] );
(void) SCKRDI( "INPUTI", 1, 1, &actvals, &loc, &unit, &knul );
(void) SCKGETC( "INPUTC", 1, 1, &actvals, bar );
CGN_LOWSTR( bar );

if ( strchr( column[0], '?' ) != NULL  || strchr( column[1], '?' ) !=  NULL )
   { if ( strchr( column[0], '?' ) != NULL )         /* first column is seq. */
        { col[0]   = 0;		
          dtype[0] = D_R4_FORMAT;
        }
     else if ( strchr( column[1], '?' ) != NULL )   /* second column is seq. */
        { col[1]   = 0;
          dtype[1] = D_R4_FORMAT;
        }
     else
        SCETER( 1, err_clmn );
   }

/*
 * find the column for the error and the orientation
 */
if ( strchr( column[2], '?' ) != NULL ) SCETER( 2, err_ercl );

/*
 * read table size information
 */
(void) TCIGET( tid, &nrcol, &nrrow, &nsort, &allcol, &allrow );
if ( nrrow == 0 ) SCETER( 3, err_row );

/*
 * get selection information
 */
(void) TCSINF( tid, sel );

/*
 * get column adresses
 */
for ( colnr = 0; colnr < 2; colnr++ )
    { if (col[colnr] != 0)
      { if ( strncmp( column[colnr], "LOG", 3 ) == 0
           || strncmp( column[colnr], "log", 3 ) == 0 )
         { ilog[colnr] = 1;
           pntr = strchr( column[colnr], '(' ) + 1;
           (void) strcpy( column[colnr], strtok( pntr, ")" ) );
         }
      else if ( strncmp( column[colnr], "LN", 2 ) == 0
              || strncmp( column[colnr], "ln", 2 ) == 0 )
         { ilog[colnr] = 2;
           pntr = strchr( column[colnr], '(' ) + 1;
           (void) strcpy( column[colnr], strtok( pntr, ")" ) );
         }
      else
         ilog[colnr] = 0;
/*
 * search column by its name
 */
      (void) TCCSER( tid, column[colnr], col+colnr );
      if ( col[colnr] == -1 ) SCETER( 4, err_nmcol );
/*
 * read column format
 */
      (void) TCFGET( tid, col[colnr], fmt[colnr], len+colnr, dtype+colnr );
      if ( dtype[colnr] == D_C_FORMAT ) SCETER( 5, err_fcol );
    }
    }
/*
 * get column adresses for the error column
 */
if ( strncmp( column[2], "LOG", 3 ) == 0
     || strncmp( column[2], "log", 3 ) == 0 )
   { ilog[2] = 1;
     pntr = strchr( column[2], '(' ) + 1;
     (void) strcpy( column[2], strtok( pntr, ")" ) );
   }
else if ( strncmp( column[2], "LN", 2 ) == 0
          || strncmp( column[2], "ln", 2 ) == 0 )
   { ilog[2] = 2;
     pntr = strchr( column[2], '(' ) + 1;
     (void) strcpy( column[2], strtok( pntr, ")" ) );
   }
else
   ilog[2] = 0;

/*
 * search error column by its name
 */
(void) TCCSER( tid, column[2], col+2 );
if ( col[2] == -1 ) SCETER( 6, err_nmcol );

/*
 * read column format
 */
(void) TCFGET( tid, col[2], fmt[2], len+2, dtype+2 );
if ( dtype[2] == D_C_FORMAT ) SCETER( 7, err_fcol );

/*
 * setup graphic device according to MIDAS settings
 */
PCOPEN( " ", " ", access, &plmode );

/*
 * do the work
 */
PLERR( tid, nrrow, col, ilog, loc, bar );

/*
 * good bye and finish
 */
PCCLOS();

return SCSEPI();
}

