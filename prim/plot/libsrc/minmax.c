/*===========================================================================
  Copyright (C) 1995,2003 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993,2003 European Southern Observatory
.IDENTifer   MINMAX
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine
.LANGUAGE    C
.PURPOSE     Determine minnimum and maximum in an array
     input:  float *array  input array
             int   ndim    lenght of the array
    output:  float *min    minimum value found
             float *max    maximum value found

.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>      Prototypes for MIDAS interfaces

.VERSION     1.0     06-Jun-1993   FORTRAN --> ANSI-C   RvH
 030808		last modif
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */
#define  _POSIX_SOURCE 1


void MINMAX(array,ndim,min,max)
int ndim;
float *array, *min, *max;

{
register float rmin, rmax, value;

register int  nr;




value = *array++;
rmin = rmax = value;

for (nr=0; nr<(ndim-1); nr++)
   {
   value = *array++;
   if (value < rmin)
      rmin = value;
   else if (value > rmax)
      rmax = value;
   }

*min = rmin;
*max = rmax;

}
