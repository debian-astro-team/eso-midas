/* @(#)labstr.c	19.1 (ES0-DMD) 02/25/03 14:07:39 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   LABSTR
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine
.LANGUAGE    C
.PURPOSE     replace special characters by AGL characters
 in/output:  char *string

.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>      Prototypes for MIDAS interfaces

.VERSION     1.0     15-Jun-1993   FORTRAN --> ANSI-C    RvH
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

/*
 * definition of the used functions
*/
#include <stdio.h>
#include <string.h>

#include <midas_def.h>

/*
 * here start the code of the function
 */
void LABSTR( string )
char * string;

{
char *pntr, *pntc;
char *source = (char*)malloc( (strlen(string)+1)*sizeof(char)); 

char *bcksld = "\\!d",
     *bcksl1 = "\\_",
     *bckslu = "\\!u",
     *bcksl2 = "\\^",
     *strnul = "  ";

/*
 * if LABEL is empty EXIT
 */
if ( strlen( string ) == 0 ) return;

/*
 * substitude \d with \_
 */
while ( ( pntc = pntr = strstr( string, bcksld )) != NULL ) 
      { (void) strcpy( pntr, bcksl1 );
        (void) strcpy( source, pntc+3); 
        (void) strcat( string, source );
        // printf("string is now [%s]\n",string); 
     }
/*
 * substitude \u with \^
 */
while ( ( pntc = pntr = strstr( string, bckslu )) != NULL )
      { (void) strcpy( pntr, bcksl2 );
        (void) strcpy( source, pntc+3); 
        (void) strcat( string, source );
        // printf("string is now [%s]\n",string); 
     }
/*
 * remove double spaces
 */
while ( ( pntc = pntr = strstr( string, strnul )) != NULL )
      { (void) strcpy( pntr, "" );
        (void) strcpy( source, pntc+2); 
        (void) strcat( string, source );
        // printf("string is now [%s]\n",string); 
     }

/*
 * remove occurences of "( <no text> )"
 */
// printf("LABSTR: Remove No Text\n"); 

while ( ( pntc = pntr = strstr( string, "(  " )) != NULL )
      { (void) strcpy( pntr, "" );
        (void) strcpy( source, pntc+2); 
        (void) strcat( string, source );
        // printf("string is now [%s]\n",string); 
     }

while ( ( pntc = pntr = strstr( string, "  )" )) != NULL )
      { (void) strcpy( pntr, "" );
        (void) strcpy( source, pntc+2); 
        (void) strcat( string, source );
        // printf("string is now [%s]\n",string); 
     }

while ( ( pntc = pntr = strstr( string, "()" )) != NULL )
      { (void) strcpy( pntr, "" );
        (void) strcpy( source, pntc+2); 
        (void) strcat( string, source );
        // printf("string is now [%s]\n",string); 
     }

/*
 * Release allocated pointers
 */ 
free(source); 

/*
 * remove leading " " 
 */
//printf("LABSTR: Remove leading space\n"); 
 if (( pntr = strstr( string, " ")) == &string[0]){
   (void) strcpy( pntr, pntr+1);
   // printf("string is now [%s]\n",string); 
 }

/*
 * remove trailing " " 
 */
//printf("LABSTR: Remove trailing space\n"); 
 if (( pntr = strstr( string, " ")) == &string[strlen(string)-1]){
   (void) strcpy( pntr, pntr+1);
   // printf("string is now [%s]\n",string); 
 }
}
