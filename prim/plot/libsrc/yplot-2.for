C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  yplot.for +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module yplot.for
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C 
C 060328	last modif
C -----------------------------------------------------------------------------
C
      SUBROUTINE GETINP(CTYPE,CSTRING,MAXVAL,CBUFF,ACTVAL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CTYPE,CSTRING,CBUFF
      INTEGER     MAXVAL, ACTVAL,STATUS
C 
      CALL STSTR(1,CTYPE)                           !STRIPPED_STRING
      CALL STSTR(2,CSTRING)                         !STRIPPED_STRING
      CALL STLOC(1,CBUFF)                           !CHAR_LOC
C 
      CALL PLSUB1(MAXVAL,ACTVAL,STATUS)
C
      RETURN
      END

