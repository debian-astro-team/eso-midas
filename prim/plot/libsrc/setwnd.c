/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   SETWND
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine
.LANGUAGE    C
.PURPOSE     Set the plot window in world coordinates
   in/output:  float *wcfram  the plot window in world coordinated

.COMMENTS    none
.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces

.VERSION     1.1     13-Sep-1993   FORTRAN --> ANSI-C    RvH

090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <stdio.h>
#include <math.h>

#include <midas_def.h>

/*
 * here start the code of the function
 */
void SETWND( wcfram )
float *wcfram;

{
double  wndl[4];

/*
 * save the window setting
 */
PCKWRR( "XWNDL", 4, wcfram );
PCKWRR( "YWNDL", 4, wcfram+4 );

/*
 * copy input array because we are going to change it
 */
wndl[0] = (double) wcfram[0];
wndl[1] = (double) wcfram[1];
wndl[2] = (double) wcfram[4];
wndl[3] = (double) wcfram[5];

/*
 * set the window
 */
if ( wcfram[3] <= -2.0 )                                /* logarithmic x-axis*/
   { wndl[0] = (double) exp( wcfram[0] );
     wndl[1] = (double) exp( wcfram[1] );
   }
else if ( wcfram[3] < 0.0 )
   { wndl[0] = (double) pow( 10.0, wcfram[0] );
     wndl[1] = (double) pow( 10.0, wcfram[1] );
   }

if ( wcfram[7] <= -2.0 )                                /* logarithmic y-axis*/
   { wndl[2] = (double) exp( wcfram[4] );
     wndl[3] = (double) exp( wcfram[5] );
   }
else if ( wcfram[7] < 0.0 )
   { wndl[2] = (double) pow( 10.0, wcfram[4] );
     wndl[3] = (double) pow( 10.0, wcfram[5] );
   }
/* 
 *define window
 */
AG_WDEF( wndl[0], wndl[1], wndl[2], wndl[3] );
AG_SSET( "linx" );
AG_SSET( "liny" );
if ( wcfram[3] < 0 ) AG_SSET( "logx" );
if ( wcfram[7] < 0 ) AG_SSET( "logy" );

return;
}

