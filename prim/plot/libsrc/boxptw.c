/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   boxptw.c
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine, pixel convertion
.LANGUAGE    C
.PURPOSE     compute world coordinates and check for legal boundaries
     input:  float  *pxax      image coordinatex in pixel coordinates
                               Note: pxax[0] = -1 replaced by pxax[0] = 1
                                     pxax[1] =  0 replaced by pxax[1] = npix
                                  
             int    npix       # of pixels along the axis
             double start      start of image in world unit
             double step       distance between pixels in world unit
    output:  float  *wcax      image pixel in world coordinates

.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>      Prototypes for MIDAS interfaces

.VERSION     1.0     04-Jun-1993   FORTRAN --> ANSI-C    RvH
             1.1     18-Apr-1994   removed not used error messages, RvH

090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <string.h>
#include <midas_def.h>

/*
 * here start the code of the function
 */
void BOXPTW( pxax, npix, start, step, wcax )
int    npix; 
float  *pxax, *wcax;
double start, step;
{
register int  xy;

for ( xy = 0; xy < 2 ; xy++ )
    { if ( *(pxax+xy) == -1.0 ) *(pxax+xy) = 1;

      if ( *(pxax+xy) == 0.0 )  *(pxax+xy) = npix;

      *(wcax+xy) = (float) (start + step * ( *(pxax+xy) - 1 ));
    }
}  

	
