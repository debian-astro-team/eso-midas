/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   PLLOGI
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot interface
.LANGUAGE    C
.PURPOSE     Plot MIDAS logo
.COMMENTS    none
.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      Symbols used by the PLT interfaces

.VERSION     1.1     09-Sep-1993   FORTRAN --> ANSI-C    RvH

090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <plot_def.h>

/*++++++++++++++++++++++++++++++
.IDENTifer   PLBOXC
.PURPOSE     plot a rectangular box in clip coordinates
     input:   float xbgn    x position in the lower left corner
              float ybgn    y position in the lower left corner
              float xend    x position in the upper right corner
              float yend    y position in the upper right corner

.COMMENTS    static function
------------------------------*/
#ifdef __STDC__
      static void PLBOXC( float xbgn, float xend, float ybgn, float yend )
#else
      static void PLBOXC( xbgn, xend, ybgn, yend )
      float xbgn, xend, ybgn, yend;
#endif
{
float xdata[PLDIM2];
float ydata[PLDIM2];

AG_SSET("norm");

xdata[0]= xbgn;
xdata[1]= xend;
ydata[0]= ybgn;
ydata[1]= ybgn;
AG_GPLL( xdata, ydata, PLDIM2 );                    /*draw line at Y-small*/

xdata[0]= xend;
xdata[1]= xend;
ydata[0]= ybgn;
ydata[1]= yend;
AG_GPLL( xdata, ydata, PLDIM2 );                    /*draw line at X-large*/

xdata[0]= xend;
xdata[1]= xbgn;
ydata[0]= yend;
ydata[1]= yend;
AG_GPLL( xdata, ydata, PLDIM2 );                    /*draw line at Y-large*/

xdata[0]= xbgn;
xdata[1]= xbgn;
ydata[0]= yend;
ydata[1]= ybgn;
AG_GPLL( xdata, ydata, PLDIM2 );                    /*draw line at X-small*/

return;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   PLLOGI
.PURPOSE     produces MIDAS logo in the right corner of graphic display
    output:    float *xstr   X-position for further information
               float *ystr   Y-position for further information
----------------------------------------------------*/
void PLLOGI( xstr, ystr )
float *xstr, *ystr;

{
int    actvals, ltype;
float  xl[3], yl[3], large, scale, small;
float  xbgn, ybgn, xend, yend, xlarge, xc, yc;

static char buff[81], text[81];

/*
 * constants
 */
static float xypos[2]  = { 0.01, 1.00 },            /*(x,y) position to start*/
             xsize[2]  = { 0.05, 0.136 }, /*size in X-direction: small, large*/
             yscale[3] = { 4.5, 1.4, 3.5 };   /*blowup factors in Y-direction*/
char *fmt_sydi = "sydi=%-.3f;chdi=%-.3f,%-.3f";

/*
 * set line style to solid line
 */
(void) AG_IGET( "lstyl", &ltype );
AG_SSET( "lstyl=0" );

/*
 * set variables for position and size of the logo
 */
AG_VU2N( xypos[0], xypos[1], &xbgn, &ybgn );   /*convert to normalised coord.*/
AG_SSET( "lfrg" );                                    /*string drawing L -> R*/
AG_SSET( "scale=1.0" );                     /*set CHAR. & SYMB. dim. to basic*/
AG_TGET( "~0M", xl, yl );                               /*get text dimensions*/

/*
 * get the right (arbitrary) sizes of characters and symbols
 */
large = 0.02 / xl[1];
small = large / 1.5;

/*
 * give plot information at the right hand side
 */
(void) sprintf( buff, fmt_sydi, small, small, small );
AG_SSET( buff );
AG_TGET( "~0M", xl, yl );                               /*get text dimensions*/
xend   = xbgn + *xsize;
xlarge = xend + *(xsize+1);
yend   = ybgn - *yscale * yl[1];

/*
 * draw the two boxes
 */
PLBOXC( xbgn, xend, yend, ybgn );
PLBOXC( xend, xlarge, yend, ybgn );

(void) sprintf( buff, fmt_sydi, large, large, large );
AG_SSET( buff );
xc = xend + *(xsize+1) / 2;
yc = ybgn - *(yscale+1) * yl[1];
AG_GTXT( xc, yc, "~1MIDAS", 0 );

(void) sprintf( buff, fmt_sydi, small, small, small );
AG_SSET( buff );
(void) SCKGETC( "MID$SESS", 16, 5, &actvals, text );
yc = ybgn - *(yscale+2) * yl[1];
(void) sprintf( buff, "~1" );
AG_GTXT( xc, yc, strcat( buff, text ), 0 );

/*
 * plot ESO and the stars
 */
small *= 0.80;
(void) sprintf( buff, fmt_sydi, small, small, small );
AG_SSET( buff );
xc = xbgn + *xsize * 0.375;
yc = ybgn - *yscale * 0.373 * yl[1];
AG_GTXT( xc, yc, "~1E", 0 );

xc = xbgn + *xsize * 0.625;
yc = ybgn - *yscale * 0.340 * yl[1];
AG_GTXT( xc, yc, "~1S", 0 );

xc = xbgn + *xsize * 0.5;
yc = ybgn - *yscale * 0.6 * yl[1];
AG_GTXT( xc, yc, "~1O", 0 );

scale = 0.20;                                                /*plot the stars*/
(void) sprintf( buff, fmt_sydi, scale, scale, scale );
AG_SSET( buff );
xc = xbgn + *xsize * 0.5;
yc = ybgn - *yscale * 0.15 * yl[1];
AG_GPLM( &xc, &yc, 1, 4 );

scale = 0.30;
(void) sprintf( buff, fmt_sydi, scale, scale, scale );
AG_SSET( buff );
xc = xbgn + *xsize * 0.85;
yc = ybgn - *yscale * 0.35 * yl[1];
AG_GPLM( &xc, &yc, 1, 4 );  

scale = 0.50;
(void) sprintf( buff, fmt_sydi, scale, scale, scale );
AG_SSET( buff );
xc = xbgn + *xsize * 0.17;
yc = ybgn - *yscale * 0.43 * yl[1];
AG_GPLM( &xc, &yc, 1, 4 );

scale = 0.20;
(void) sprintf( buff, fmt_sydi, scale, scale, scale );
AG_SSET( buff );
AG_GPLM( &xc, &yc, 1, 5 ); 

scale = 0.75;
(void) sprintf( buff, fmt_sydi, scale, scale, scale );
AG_SSET( buff );
xc = xbgn + *xsize * 0.5;
yc = ybgn - *yscale * 0.85 * yl[1];
AG_GPLM( &xc, &yc, 1, 4 );  

scale = 0.25;
(void) sprintf( buff, fmt_sydi, scale, scale, scale );
AG_SSET( buff );
AG_GPLM( &xc, &yc, 1, 5 );

/*
 * give the right position to give further information
 */
*xstr = xbgn;
*ystr = ybgn - 7 * yl[1];

/*
 * reset line type
 */
if ( ltype > 0 )
   { (void) sprintf( text, "lstyl= %1d", ltype );
     AG_SSET( text );
   }
return;
}

