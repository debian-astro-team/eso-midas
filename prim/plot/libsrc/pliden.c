/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   PLIDEN
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    plot software, additional information
.PURPOSE     plot minimal information around a graph, outside the axis
     input:   int   plmode    plot mode, see PMODE in PLISTAT
              char  *lstrng   string plotted above graph in left corner
              char  *rstrng   string plotted above graph in right corner

.COMMENTS    this routine only works when PMODE == 1
.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces

.VERSION     1.0     26-Sep-1993   FORTRAN --> ANSI-C    RvH

090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <midas_def.h>

/*
 * define some macros and constants
 */
#define  NCTIME		5		/* we write hh:mm in variable TIM */

/*
 * here start the code
 */
void PLIDEN( plmode, lstrng, rstrng )
int  plmode;
char *lstrng, *rstrng;

{
int   actvals;
float scale, tsize, xl[3], yl[3], clpl[4];
char  tim[6], dattim[41], buff[81], text[81];

/* 
 * check if PMODE is one, else retrun
 */
if ( plmode != 1 ) return;

/*
 * get character dimension, from the MIDAS keywords
 * and scaling from AGL
 */
(void) AG_RGET( "scale", &scale );
PCKRDR( "TSIZE", 1, &actvals, &tsize );
AG_TGET( "M", xl, yl );

AG_SSET( "lfrg" );
AG_SSET( "norm" );
AG_SSET( "linx");
AG_SSET( "liny");
(void) AG_RGET( "clpl", clpl );

/*
 * write both strings
 */
(void) strcpy( text, "~^" );
(void) strcat( text, lstrng );
LABSTR( text );
AG_GTXT( clpl[0], clpl[3] + yl[2], text, 1 );

(void) strcpy( text, "~^" );
(void) strcat( text, rstrng );
LABSTR( text );
AG_GTXT( clpl[1], clpl[3] + yl[2], text, 3 );

/*
 * set character height and write from up to down (instead of left to right)
 */
AG_SSET( "scale=1.0;chdi=1.0,1.0" );
AG_SSET( "updo" );

/*
 * which MIDAS version is used ?
 */
(void) SCKGETC( "MID$SESS", 16, 5, &actvals, buff );
(void) strcpy( text, "~-~-eso-midas version: " );
(void) strcat( text, buff );

/*
 * get the date
 */
(void) SCKGETC( "DATTIM", 1, 40, &actvals, dattim );
(void) strcat( text, "    date: " );

*tim = '\0';
if ( strchr( dattim, ',' ) == 0 )
   { *(dattim+20) = '\0';
     (void) strncpy( tim, dattim+12, NCTIME );
     *(dattim+11)='\0';
     (void) strcat( text, dattim );
   }                
else
   { *(dattim+26) = '\0';
     (void) strncpy( tim, dattim+18, NCTIME );
     *(dattim+15)='\0';
     (void) strcat( text, dattim );
   }
(void) strcat( text, " " );
(void) strncat( text, tim, NCTIME );
AG_GTXT( 1.0, 1.0, text, 23 );

/*
 * get the username, and write it
 */
PLUSER( buff );
(void) strcat( strcpy( text, "~-~-" ), buff );
AG_GTXT( 1.0, 0.0, text, 17 );

/*
 * reset if necessary the symbol and text size
 */
AG_SSET( "lfrg" );
(void) sprintf( text, "scale=%-.3f;chdi=%-.3f,%-.3f", scale, tsize, tsize );
AG_SSET( text );
}


