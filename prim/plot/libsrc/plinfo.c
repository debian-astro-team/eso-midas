/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   module PLINFO
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot interface
.LANGUAGE    C
.PURPOSE     Retrieve general information such as date, time and user name
.COMMENTS    holds the routines PLUSER and PLDATI
.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces

.VERSION     1.1     09-Sep-1993   FORTRAN --> ANSI-C    RvH
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <midas_def.h>

/*++++++++++++++++++++++++++++++++++++++++++++++++++
 * here start the code of the function PLUSER
 *
.IDENTifer   PLUSER
.PURPOSE     Get the username of the user
    output:  string with the username
--------------------------------------------------*/
void PLUSER( text )
char *text;

{
int  actvals;
char it1[11], user[21];

(void) SCKGETC( "USER", 1, 20, &actvals, user );

if ( strlen( user ) != 1 )
   (void) sprintf( text, "User: %s", user );
else
   { (void) SCKGETC( "MID$WORK", 11, 10, &actvals, it1 );
     (void) sprintf( text, "user: %s", it1 );
   }
return;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * here start the code of the function PLDATI
 *
.IDENTifer   PLDATI
.PURPOSE     Plot date and time at creation of the plot
     input:  float xnorm  x-position in normalized coordinates
             float ynorm  y-position in normalized coordinates
----------------------------------------------------*/
void PLDATI( xnorm, ynorm )
float *xnorm, *ynorm;

{
int  actvals;

float xl[3],yl[3];

char  *dattim, dat[18], tim[18];

dattim = osmmget(41);

(void) strcpy( dat, "Date: " );
(void) strcpy( tim, "Time: " );

(void) SCKGETC( "DATTIM", 1, 40, &actvals, dattim );

if ( strchr( dattim, ',' ) == 0 )
   { *(dattim+20) = '\0';
     dattim += 12;
     (void) strcat( tim, dattim );
     dattim -= 12;
     *(dattim+11)='\0';
     (void) strcat( dat, dattim );
   }                
else
   { *(dattim+26) = '\0';
     dattim += 18;
     (void) strcat( tim, dattim );
     dattim -= 14;
     *(dattim+11)='\0';
     (void) strcat( dat, dattim );
     dattim -= 4;
   }

/*
 * get character height
 */
AG_TGET("M", xl, yl );

AG_GTXT( *xnorm, *ynorm, dat, 1 );
*ynorm -= 2 * yl[1];
AG_GTXT( *xnorm, *ynorm, tim, 1 );
*ynorm -= 2 * yl[1];

/*
 * free allocated memory
 */
(void) osmmfree( dattim );

return;
}
