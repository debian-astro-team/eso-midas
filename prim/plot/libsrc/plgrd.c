/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   PLGRD
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine, grid plotting
.LANGUAGE    C
.PURPOSE     Draws a grid over existing frame
     input:   char  *cinput   grid type
                                l(arge): connecting large tick marks
                                s(mall): also connecting small tick marks
              char  *xy_opt   XY grid lines: grid in X, or Y or X & Y

.COMMENTS    Grid will be drawn in the current line mode
.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      Symbols used by the PLT interfaces

.VERSION     1.1     10-Sep-1993   FORTRAN --> ANSI-C    RvH

090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <plot_def.h>

/*
 * here start the code of the function
 */
void PLGRD( cinput, xy_opt )
char *cinput, *xy_opt;

{
int   actvals, xy, x_or_y;
float factor, grid, lgrd, step, tkstr, tkend, xt[PLDIM2], yt[PLDIM2], 
      wcfram[8], adat[PLDIM2][6];
/*
 * initialised variables, should be kept constant
 */
static char  *scale[PLDIM2][2] = {{ "logx", "linx" }, 
                                  { "logy", "liny" }};
/*
 * pick up the frame settings
 */
PCKRDR( "XWNDL", 4, &actvals, wcfram );
PCKRDR( "YWNDL", 4, &actvals, wcfram+FOR_Y );

/*
 * get the order right (on purpuse, the same syntax is used as in PCFRAM)
 */
for ( xy = 0; xy < PLDIM2; xy++ ) 
    { x_or_y = ( xy == 0 ) ? FOR_X : FOR_Y;
      GETTIC( wcfram + x_or_y, &tkstr, &tkend );
      adat[xy][0] = MYMIN( wcfram[x_or_y], wcfram[x_or_y+1] );
      adat[xy][1] = MYMAX( wcfram[x_or_y], wcfram[x_or_y+1] );
      adat[xy][2] = MYMIN( tkstr, tkend );
      adat[xy][3] = MYMAX( tkstr, tkend );
      adat[xy][4] = wcfram[x_or_y + 2];
      adat[xy][5] = MYMIN( wcfram[x_or_y + 2], wcfram[x_or_y + 3] );
      AG_SSET( scale[xy][1] );
 
      if ( wcfram[x_or_y + 3] <= -2.0 )           /*natural logarithmic scale*/
         { adat[xy][0] = (float) exp( adat[xy][0] );
           adat[xy][1] = (float) exp( adat[xy][1] );
           adat[xy][2] = (float) exp( adat[xy][2] );
           adat[xy][3] = (float) exp( adat[xy][3] );
           adat[xy][4] = (float) exp( adat[xy][4] );
           adat[xy][5] = adat[xy][4];
           AG_SSET( scale[xy][0] );
         } 
      else if ( wcfram[x_or_y + 3] < 0.0 )                /*logarithmic scale*/
         { adat[xy][0] = (float) pow( 10.0, adat[xy][0] );
           adat[xy][1] = (float) pow( 10.0, adat[xy][1] );
           adat[xy][2] = (float) pow( 10.0, adat[xy][2] );
           adat[xy][3] = (float) pow( 10.0, adat[xy][3] );
           adat[xy][4] = (float) pow( 10.0, adat[xy][4] );
           adat[xy][5] = MYMAX( 1.0, adat[xy][4] / 10.0  );
           AG_SSET( scale[xy][0] );
         }
    }
/*
 * all frame parameters are known, do the plotting
 */
if ( strchr( xy_opt, 'x' ) != NULL || strchr( xy_opt, 'X' ) != NULL )
   { yt[0] = adat[1][0];
     yt[1] = adat[1][1];
     
     if ( wcfram[3] > 0 )
        { step = adat[0][4];
          if ( *cinput == 's' || *cinput == 'S' ) step = adat[0][5];

          grid = adat[0][2] + step;
          while ( grid - step > adat[0][0] ) grid -= step;
          while ( grid < adat[0][1] )
                { xt[0] = xt[1] = grid;
                  AG_GPLL( xt, yt, 2 );
                  grid += step;
                }
        }
     else 
        { if ( wcfram[3] <= -2.0 )
             factor = exp( 1.0 );
          else
             factor = 10.0;

          grid = lgrd = adat[0][2];
          if ( fabs(adat[0][0] - adat[0][2]) <= PLT_EPS ) grid *= factor;

          if ( wcfram[3] > -2.0 && (*cinput == 's' || *cinput == 'S') )
             { grid /= factor;
               while ( grid < adat[0][0] ) grid += lgrd/factor;
               if ( grid < adat[0][2] ) lgrd /= factor;
             }

          while ( grid < adat[0][1] )
                { if ( wcfram[3] > -2.0 && (*cinput == 's' || *cinput == 'S') )
                     { lgrd *= factor;
                       while ( grid < lgrd )
                             { xt[0] = xt[1] = grid;
                               AG_GPLL( xt, yt, 2 );
                               grid += lgrd/factor;
                             }
                     }
                  if ( grid < adat[0][1] )
                     { xt[0] = xt[1] = grid;
                       AG_GPLL( xt, yt, 2 );
                       if ( wcfram[3] > -2.0 && 
                            (*cinput == 's' || *cinput == 'S') )
                          grid *= 2;
                       else
                          grid *= factor;
                     }
                }
        }
    } 

if ( strchr( xy_opt, 'y' ) != NULL || strchr( xy_opt, 'Y' ) != NULL)
   { xt[0] = adat[0][0];
     xt[1] = adat[0][1];
     
     if ( wcfram[7] > 0 )
        { step = adat[1][4];
          if ( *cinput == 's' || *cinput == 'S' ) step = adat[1][5];

          grid = adat[1][2] + step;
          while ( grid - step > adat[1][0] ) grid -= step;
          while ( grid < adat[1][1] )
                { yt[0] = yt[1] = grid;
                  AG_GPLL( xt, yt, 2 );
                  grid += step;
                }
        }
     else 
        { if ( wcfram[7] <= -2.0 )
             factor = exp( 1.0 );
          else
             factor = 10.0;

          grid = lgrd = adat[1][2];
          if ( fabs(adat[1][0] - adat[1][2]) <= PLT_EPS ) grid *= factor;

          if ( wcfram[3] > -2.0 && (*cinput == 's' || *cinput == 'S') )
             { grid /= factor;
               while ( grid < adat[1][0] ) grid += lgrd/factor;
               if ( grid < adat[1][2] ) lgrd /= factor;
             }

          while ( grid < adat[1][1] )
                { if ( wcfram[3] > -2.0 && (*cinput == 's' || *cinput == 'S') )
                     { lgrd *= factor;
                       while ( grid < lgrd )
                             { yt[0] = yt[1] = grid;
                               AG_GPLL( xt, yt, 2 );
                               grid += lgrd/10;
                             }
                     }
                  if ( grid < adat[1][1] )
                     { yt[0] = yt[1] = grid;
                       AG_GPLL( xt, yt, 2 );
                       if ( wcfram[3] > -2.0 && 
                            (*cinput == 's' || *cinput == 'S') )
                          grid *= 2;
                       else
                          grid *= factor;
                     }
                }
        }
   } 
return;
}
