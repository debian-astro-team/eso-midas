/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   PLDESI
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    plot software, graphics
.LANGUAGE    C
.PURPOSE     Produce full information around a PLOT/DESCRIPTOR plot 
     input:  int   plmode    plot mode, see PMODE in PLISTAT
             char  *name     data frame name
             char  *ident    ascii identifier of image
             char  *desc     name of the descriptor
             int   *image    size in PIXEL coordinates
             
.COMMENTS    none
.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces

.VERSION     1.1     10-Sep-1993   FORTRAN --> ANSI-C    RvH

090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <midas_def.h>

/*
 * start of the code
 */
void PLDESI( plmode, name, ident, desc, image )
char  *name, *ident, *desc;
int   plmode, *image; 

{
int    actvals;
float  one, ssize, tsize, x1, x2, y1, y2, xt, yt, yh;
float  scale[2], xl[3], yl[3], clpl[4], wndl[4];
char   buff[81];

/*
 * Only for plot mode is 2
 */
if ( plmode != 2 ) return;

/*
 * get the symbol and character dimensions, from the MIDAS keywords
 */
PCKRDR( "SSIZE", 1, &actvals, &ssize );
PCKRDR( "TSIZE", 1, &actvals, &tsize );

/*
 * if the symbol size or the text size is not equal to 1, set it to 1
 * and call PCTSET to get the proper sizes for the MIDAS layout
 */
if ( ssize != 1.0 || tsize != 1.0 )
   { one = 1.0;
     PCKWRR( "SSIZE", 1, &one );
     PCKWRR( "TSIZE", 1, &one );
   }
PCTSET();

AG_SSET( "norm");
AG_SSET( "linx");
AG_SSET( "liny");
(void) AG_RGET( "clpl", clpl );
(void) AG_RGET( "wndl", wndl );
x1 = clpl[1] + 0.01;
x2 = 1.0;
y1 = 0.0;
y2 = clpl[3];
AG_CDEF( x1, x2, y1, y2);
AG_WDEF( 0.0, 1.0, 0.0, 1.0 );

/*
 * plot MIDAS logo
 */
PLLOGI( &xt, &yt );

/* 
 * set character height
 */
AG_SSET( "sydi=0.75;chdi=0.75,0.75" );
AG_TGET( "M", xl, yl );
yh = 2.0 * yl[1];

/*
 * plot user name
 */
PLUSER( buff );
AG_GTXT( xt, yt, buff, 1 );

/*
 * name of frame
 */
yt -= 2*yh;
if ( strlen( name ) > (size_t) 12 )
   { AG_GTXT( xt, yt, "Frame:", 1 );
     yt -= yh;
     AG_GTXT( xt, yt, name, 1 ); 
   }
else
   { (void) sprintf( buff, "Frame: %s", name );
     AG_GTXT( xt, yt, buff, 1 );
   }
/*
 * identification
 */
if ( strlen( ident ) > (size_t) 0 )
   { yt -= 2*yh;
     AG_GTXT( xt, yt, "Identification:", 1 );
     yt -= yh;
     AG_GTXT( xt, yt, ident, 1 );
   }
/*
 * descriptor name
 */
yt -= 2*yh;
AG_GTXT( xt, yt, "Descriptor:", 1 );
yt -= yh;
AG_GTXT( xt, yt, desc, 1 );

/*
 * descriptor elements
 */
yt -= 2*yh;
AG_GTXT( xt, yt, "Desc. elements:", 1 );
yt -= yh;
(void) sprintf( buff, "X: %d to %d", image[0], image[1] );
AG_GTXT( xt, yt, buff, 1 );

/*
 * scales
 */
PCKRDR( "SCALES", 2, &actvals, scale );
yt -= 2*yh;
AG_GTXT( xt, yt, "Scales:", 1 );
yt -= yh;
(void) sprintf( buff, "X: %-.6g", scale[0] );
AG_GTXT( xt, yt, buff, 1 );
yt -= yh;
(void) sprintf( buff, "Y: %-.6g", scale[1] );
AG_GTXT( xt, yt, buff, 1 );

/*
 * minimum and maximum
 */
yt -= 2*yh;
(void) sprintf( buff, "Min: %-.3g ", wndl[2] );
AG_GTXT( xt, yt, buff, 1 );
yt -= yh;
(void) sprintf( buff, "Max: %-.3g ", wndl[3] );
AG_GTXT( xt, yt, buff, 1 );

yt -= 2.*yh;
PLDATI( &xt, &yt );

/*
 * back to standard sizes
 */
AG_CDEF( clpl[0], clpl[1], clpl[2], clpl[3] );
AG_WDEF( wndl[0], wndl[1], wndl[2], wndl[3] );

/*
 * reset if necessary the symbol and text size
 */
if ( ssize != 1.0 || tsize != 1.0 )
   { PCKWRR( "SSIZE", 1, &ssize );
     PCKWRR( "TSIZE", 1, &tsize );
     PCTSET();
   }
return;
}          

