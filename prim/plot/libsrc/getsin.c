/* @(#)getsin.c	19.1 (ES0-DMD) 02/25/03 14:07:38 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   GETSIN
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine
.LANGUAGE    C
.PURPOSE     Convert a user coordinate input string into pixel values
     input:  char    *string      coordinate string
             int     nrpix        number pixels in frame
             double  start        start value of frame
             double  step         step between pixel

.RETURNS     actual number of the pixel
.COMMENTS    if a coordinate is outside the valid interval 
                GETSIN returns: 1      if the input is too small
                                nrpix  if the input is too large
.ENVIRONment MIDAS
             #include <midas_def.h>      Prototypes for MIDAS interfaces

.VERSION     1.0     14-Jun-1993   FORTRAN --> ANSI-C     RvH

 010423		last modif

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdlib.h>
#include <string.h>

#include <midas_def.h>

/*
 * here start the code of the function
 */
int GETSIN( string, nrpix, start, step )
char   *string; 
int    nrpix; 
double start, step;

{
int     pixel;
double  coord;
char    test, *str_mess;

char *str_err = "*** WARNING: One of the coordinates ",
     *str_small = "falls below frame boundaries",
     *str_large = "falls beyond frame boundaries";

/*
 * check one character of input string
 */
test = *string;
switch ( test )
   { case '<':                                            /*handle "<" format*/
         pixel = 1;
         break;
     case '>':                                            /*handle ">" format*/
         pixel = nrpix;
         break;
     case '@':                                            /*handle "@" format*/
         string++;
         pixel = atoi(string);
         break;
     default:                                /*here we have world coordinates*/
         coord = atof(string);
         pixel = (int) ((coord-start)/step) +1;
         break;
   }

if ( pixel < 1 || pixel > nrpix )
   { str_mess = osmmget(70);
     (void) strcpy( str_mess, str_err );
     if ( pixel < 1 )
        { pixel = 1;
          SCTPUT(strcat(str_mess,str_small));
        }
     else
        { pixel = nrpix;
          SCTPUT(strcat(str_mess,str_large));
        }
   }

return pixel;
}

