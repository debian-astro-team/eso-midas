/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   GETTIC
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine
.LANGUAGE    C
.PURPOSE     determine start and end values of the tickmarks
     input:   float *axchar  start, end value of the axis,
                             and big and small tickmarks
    output:   float *str_ax  starting large tickmark
              float *end_ax  last large tickmark

.COMMENTS    IMPORTANT!!!
               This routine is declared as a static function within PCFRAM,
               every change here must also be implemented in PCFRAM

.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      Symbols used by the PLT interfaces

.VERSION     1.1     11-Jun-1993   FORTRAN --> ANSI-C    RvH

090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <plot_def.h>

/*
 * here start the code of the function
 */
void GETTIC( axchar, str_ax, end_ax )
float *axchar, *str_ax, *end_ax;

{
float   clpl[4];
double  amin, amax, abig, asmall, alen, atkstr, atkend, off, tick, value, xlen;

amin   = *axchar;
amax   = *(axchar + 1);
abig   = *(axchar + 2);
asmall = *(axchar + 3);

/*
 * Special case 1: lenght of the axis is zero
 */
if ( amin == amax ) 
   { *str_ax = *axchar;
     *end_ax = *(axchar + 1);
     return;
   }
/*
 * Special case 2: the big tickmark is larger than the lenght of the axis
 */
if ( asmall > 0 && abig >= fabs( amin - amax ) )
   { *str_ax = *axchar;
     *end_ax = *(axchar + 1);
     return;
   }
/*
 * Special case 3: log scaling and the axis is longer than 30 tickmarks
 */
if ( asmall < 0.0 && fabs( amin - amax ) > 30.0 )
   { *str_ax = *axchar;
     *end_ax = *(axchar + 1);
     return;
   }

if ( amax < amin ) abig *= -1.0;
off = floor( amin / abig );
if ( amin / abig < 0.0 ) off -= 1.0;
tick = abig * off;

/* 
 * get the length of the plot diagonal in pixel coordinates
 */
(void) AG_RGET( "CLPL", clpl );
alen = MYDIST( (clpl[1] - clpl[0]), (clpl[3] - clpl[2]) );

/*
 * find the smallest value on the axis
 */
do { if ( asmall < 0 )
        { xlen = alen*( tick - amin )/( amax - amin );
          value = pow( 10.0, tick );
        }
     else
        { value = tick;
          if ( fabs( value ) < fabs( 1e-12 * abig ) ) 
             value = 0.0;
          xlen = alen*( value - amin )/( amax - amin );
        }
     if ( xlen < -PLT_EPS ) tick += abig;
   }
while ( xlen < -PLT_EPS );
atkstr = value;

/*
 * find the largest value on the axis
 */
atkend = atkstr;
do { if ( asmall < 0 )
        { xlen = alen*( tick - amin )/( amax - amin );
          value = pow( 10.0, tick );
        }
     else
        { value = tick;
          if ( fabs( value ) < fabs( 1e-12 * abig ) ) 
             value = 0.0;
          xlen = alen * ( value - amin )/( amax - amin );
        }
     if ( xlen  <= alen * ( 1 + PLT_EPS )) 
        { atkend = value;
          tick += abig;
        }
   }
while ( xlen  <= alen * ( 1 + PLT_EPS ));

if ( asmall < 0.0 )
   { *str_ax = (float) log10( atkstr );
     *end_ax = (float) log10( atkend );
   }
else
   { *str_ax = (float) atkstr;
     *end_ax = (float) atkend;
   }
return;
}

