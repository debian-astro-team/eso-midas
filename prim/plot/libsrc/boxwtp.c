/* @(#)boxwtp.c	19.1 (ES0-DMD) 02/25/03 14:07:37 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   boxwtp.c
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine, pixel convertion
.LANGUAGE    C
.PURPOSE     compute pixel coordinates and check for legal boundaries
     input:  float  *wcax      image pixel in world coordinates
             int    npix       # of pixels in the x direction
             double start      start of image in x in world unit
             double step       distance between x pixels in world unit
    output:  int    *pxax      image coordinatex in pixel coordinates
.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>      Prototypes for MIDAS interfaces

.VERSION     1.0     14-Jun-1993   FORTRAN --> ANSI-C   RvH

 010423		last modif

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <midas_def.h>

/*
 * here start the code of the function
 */
void BOXWTP( wcax, npix, start, step, pxax )
int    npix; 
float  *pxax, *wcax;
double start, step;
{
register int  xy;

char *string;

float wcax_xy;
int  err2small = 0, 
     err2large = 0;

static char *str_err   = "*** WARNING: Both coordinates ",
            *str_small = "fall below frame boundaries",
            *str_large = "fall beyond frame boundaries";


for ( xy = 0; xy < 2 ; xy++ )
    {
    if ( *(wcax+3) <= -2.0 )                  /*natural logarithmic scale*/
       wcax_xy = (float) exp( *(wcax+xy) );
    else if ( *(wcax+3) < 0.0 )                       /*logarithmic scale*/
       wcax_xy = (float) pow( 10.0, *(wcax+xy) );
    else
       wcax_xy = *(wcax+xy);

    *(pxax+xy) = (float) ceil((wcax_xy - start)/ step )+ 1;

      if ( *(pxax+xy) <= 0 )
         { *(pxax+xy) = 1;
           err2small += 1;
         }

      if ( *(pxax+xy) > npix )
         { *(pxax+xy) = npix;
           err2large += 1;
         }
    }

if ( err2small > 1 || err2large > 1 )
   { string = osmmget(70);
     (void) strcpy( string, str_err );
     if ( err2small > 1 ) 
        SCTPUT( strcat( string, str_small ) );
     else 
        SCTPUT( strcat( string, str_large ) );
     (void) osmmfree( string );
   }
}
