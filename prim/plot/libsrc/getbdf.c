/* @(#)getbdf.c	19.1 (ES0-DMD) 02/25/03 14:07:38 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   GETBDF
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    lowlevel plot routine
.LANGUAGE    C
.PURPOSE     Extract data along a ROW, or along a COLUMN, from a 2-D frame
        input: char   *cpntr  character pointer to the data of 2-D frame 
               float  *image  contains in pixel units
               int    *npix   standart descriptor of the frame
               double *start  minimum value of points along X-axis
               double *step   size of the steps along X-axis
       output: float  *xval   values of data along the X-axis
               float  *yval   values of data along the Y-axis

.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>      Prototypes for MIDAS interfaces

.VERSION     1.0     16-Jun-1993   FORTRAN --> ANSI-C    RvH
	     1.1     15-Jul-1994   improved the code     RvH
             1.2     940831	   for column extraction: nrprow -> nrpcol, KB
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdlib.h>
#include <math.h>

#include <midas_def.h>

/*
 * here start the code of the function
 */
void GETBDF( cpntr, image, npix, start, step, xval, yval )
char   *cpntr; 
int    *npix;
float  *image, *xval, *yval;
double *start, *step; 

{
int     ii, indx, nrpcol, nrprow;
float   *p_img;
double  xbgn, xstep;

nrprow = (int) fabs( *(image+1) - *image )     + 1;
nrpcol = (int) fabs( *(image+3) - *(image+2) ) + 1;
indx  = (int) ((*image - 1) + (*(image+2) - 1) * *npix);
p_img = (float *) cpntr + indx;

/*
 * get the data of a column
 */
if ((int)image[0] == (int)image[1])
   {
   xbgn  = start[1] + (image[2] - 1 ) * step[1];
   xstep = step[1];
   if ( image[2] > image[3] ) xstep = -xstep;
   for ( ii = 0; ii < nrpcol; ii++ )
      *xval++ = (float) (xbgn + ii * xstep);

   if ( image[3] > image[2] )
      { for ( ii = 0; ii < nrpcol; ii++, p_img += *npix ) 
              *yval++ = *p_img;
      }
   else
      { for ( ii = 0; ii < nrpcol; ii++, p_img -= *npix ) 
              *yval++ = *p_img;
      }
   }
/*
 * get the data of a row
 */
else 
   { 
   xbgn  = *start + (*image - 1 ) * *step;
   xstep = *step;
   if ( *image > image[1] ) xstep = -xstep;
   for ( ii = 0; ii < nrprow; ii++ )
      *xval++ = (float) (xbgn + ii * xstep);

   if ( image[1] > *image )
      for ( ii = 0; ii < nrprow; ii ++ ) *yval++ = *p_img++;
   else
      for ( ii = 0; ii < nrprow; ii ++ ) *yval++ = *p_img--;
   }
    
}
