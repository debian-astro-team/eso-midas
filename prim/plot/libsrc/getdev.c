/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   GETDEV.C
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    
.LANGUAGE    C
.PURPOSE     get logical device name for current graphics window
             on both workstations and terminals
     input:  char *kname    type of device
    output:  char *lname    logical name of device
.RETURNS     
.COMMENTS    The lenght of the file AGL3CONFIG:agldevs.dat, also 
             defines the number of supported graphics devices. 
             This number should consist with the number of devices 
             declared in the file pltdev.inc
.ENVIRONment MIDAS
             #include <osparms.h>       Symbols used by the OS interfaces
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      Symbols used by the PLT interfaces


.VERSION     2.0     11-Jun-1993   FORTRAN --> ANSI-C    RvH
051021		last modif

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate that this is a POSIX program
*/
#define  _POSIX_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <midas_def.h>

/*
 * define some macros and constants
*/

#include <osparms.h>
#include <plot_def.h>

#undef	MAXLEN
#define MAXLEN		80		    /* maximum length of device name */

#undef  FATAL
#define FATAL           1                              /* error status FATAL */

/*

*/

int GETDEV( kname, lname )
char *kname, *lname;

{
int  fid, found;

char *devnam, *strtest;   
char *pldev = "AGL3CONFIG:agldevs.dat",
    *err_flopn = "*** FATAL: GETDEV, Cannot open file with device definitions";



/* initialisation */

devnam = malloc((size_t)84);

(void) strtok(kname," ");                    /* get rid of trailing blanks */
CGN_LOWSTR(kname);

/*read from AGLDEVS.dat*/

if ((fid = CGN_OPEN(pldev,READ)) == -1) SCETER(1,err_flopn);


/* keyword conversion or user input conversion*/

if (*(kname+1) != '_')
   strtest = kname;				/*normal device name*/
else
   strtest = kname+2;				/*full device name*/

found = FALSE;
while ( !found && (osaread(fid,devnam,MAXLEN)) >= 0)
   { 
   if (*devnam != '#') 
      {
      (void) strtok(devnam,":");
      if ( strcmp(strtest,devnam) == 0)
         {
         (void) strcpy(lname,strtest);
         found = TRUE;
         }
      }
   }
(void) free((char *) devnam);
(void) osaclose(fid);

if (found == FALSE)		/* just use original name, if not found */
   (void) strcpy(lname,kname);

return ERR_NORMAL;
}

