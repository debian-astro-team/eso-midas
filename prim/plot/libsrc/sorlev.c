/* @(#)sorlev.c	19.1 (ES0-DMD) 02/25/03 14:07:41 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   SORLEV
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine
.LANGUAGE    C
.PURPOSE     Sort string of values into ascending sequence of values
     input:  int    n       dimension of arrays xval and yval
 in/output:  float  *clevl  array to be sorted

.COMMENTS    none
.ENVIRONment none

.VERSION     1.0     27-Jun-1993   Creation    R.M. van Hees
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * start of the code
 */
#ifdef __STDC__
      void SORLEV( int n, float *xval )
#else
      void SORLEV( n, xval )
      int   n;
      float *xval;
#endif
{
register int indx;

if ( n < 2 ) return;

indx = n - 1;
do { register int jj, ii = -1;

     for ( jj = 0; jj < indx; jj++ )
         { if ( xval[jj] > xval[jj+1] )
              { float xtemp = xval[jj];

                xval[jj]   = xval[jj+1];
                xval[jj+1] = xtemp;
                ii = jj;
              }
         }
     indx = ii;
   } 
while ( indx != -1 );

return;
}
