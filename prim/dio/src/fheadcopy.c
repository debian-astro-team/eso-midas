/*===========================================================================
  Copyright (C) 2003-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT    (c)  2003  European Southern Observatory
.IDENT        fheadcopy.c
.LANGUAGE     C
.AUTHOR       K. Banse		ESO - Garching
.KEYWORDS     FITS header
.PURPOSE      Copy FITS header from 1st FITS extension to other
	      extensions of same file, but do not overwrite existing data
.VERSION      1.0  031028	KB

 090710		last modification
---------------------------------------------------------------------*/

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 1
#endif

#include   <stdio.h>
#include   <midas_def.h>

/*

*/

int main()
{
char  root[64], extlist[104], exttypes[96], source[80];
char  name[120];

int   uni, nulo, nval, n, mm, mask;
int   extno[80], imnoa, imnob;
int   extcount, totext;

float  rdum;

double ddum;





(void) SCSPRO("FHEADCOPY");			/* get into MIDAS env. */
(void) SCKGETC("INPUTC",1,20,&nval,root);	/* read root for file names */


/* thanks to INDISK/MFITS, the total no. of extensions (files created) 
   is stored in keyword MID$INFO(4), and the types in OUTPUTC(1:)     */

nulo = -1;
(void) SCKRDI("MID$INFO",4,1,&n,&totext,&uni,&nulo);
n = totext + 1;
(void) SCKGETC("OUTPUTC",1,n,&nval,exttypes);


/* check, which extensions should be processed */

(void) SCKGETC("P6",1,100,&nval,extlist);
mm = CGN_INDEXC(extlist,',') + 1;
if (mm < 2) 
   SCETER(3,"Invalid copy_FITS_header_flag - we quit ...");

if (extlist[mm] != '*')				/* copy,e1,e2,... */
   {
   (void) strcpy(extlist,&extlist[mm]);		/* move to begin of string */
   extcount = CGN_CNVT(extlist,1,80,extno,&rdum,&ddum);
   if (extcount < 1) 
      SCETER(4,"invalid syntax for extension no.s ...");
   }
else						/* copy,*   */
   {
   extcount = totext - 1;		/* subtract 1. header (extension) */
   for (n=0; n<extcount; n++) extno[n] = n + 1;
   }


/* open source_frame (1. extension of FITS file) - it's always type IMAGE! */

(void) sprintf(source,"%s0000",root);
(void) SCFOPN(source,D_OLD_FORMAT,0,F_IMA_TYPE,&imnoa);

/* copy all but extended, standard descr.s, except the ones specified in
   dsclist param. - no overwrite */

mask = 210;			/* flag = 10, no_overwrite = 200 */

for (n=0; n<extcount; n++)
   {
   mm = extno[n];				/* no. of dest_frame */
   (void) sprintf(name,"%s%4.4d",root,mm);
   if ((exttypes[mm] == 'I') || (exttypes[mm] == 'E'))
      {
      (void) strcat(name,".bdf");		/* type IMAG */
      (void) SCFOPN(name,D_OLD_FORMAT,0,F_IMA_TYPE,&imnob);
      (void) SCDCOP(imnoa,imnob,mask,"O_POS");	/* don't copy O_POS */
      (void) SCFCLO(imnob);
      }
   else
      {
      (void) strcat(name,".tbl");		/* type TABL */
      (void) TCTOPN(name, F_IO_MODE, &imnob);
      (void) SCDCOP(imnoa,imnob,mask,"O_POS");	/* don't copy O_POS */
      (void) TCTCLO(imnob);
      }
   }


return SCSEPI();
}

