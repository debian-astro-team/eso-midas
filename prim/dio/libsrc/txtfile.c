/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c)  1993  European Southern Observatory
.IDENT      txtfile.c
.LAUGUAGE   C
.AUTHOR     P.Grosbol   ESO/IPG
.KEYWORDS   ASCII text, files
.COMMENT    Create and write to ASCII text files
.VERSION    1.0  1991-Mar-15 : Creation,   PJG
.VERSION    1.1  1993-Oct-26 : Update to new SC/TC/OS + prototypes,   PJG
.VERSION    1.2  1997-Jan-08 : Check/count non-ASCII characters,   PJG
 050802		last modif

---------------------------------------------------------------------*/

#include    <fitsdef.h>
#include    <midas_def.h>

#define    MXBUF         512      /* max char's in line buffer       */

static     char         *buf;     /* pointer to character buffer     */
static     int      tfd = -1;     /* text-file descriptor no.        */
static     int      n   =  0;     /* no. char. in buffer             */
static     int      sc  =  0;     /* special character               */


int text_open(name,mode)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    initiate ASCII text file
.RETURN     status, 0:created OK, 1:error
---------------------------------------------------------------------*/
char   *name;                 /* IN: name of text-file to create     */
int    mode;                  /* IN: open mode of file               */
{

  if (0<=tfd) return 1;

  buf = (char *)osmmget(MXBUF); 	/* allocate buffer   */
  if (!buf) return 1;     
  *buf = '\0';

  tfd = osaopen(name,mode);                     /* open text file    */
  if (tfd<0) return 1;

  return 0;
}

int text_put(line)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    put text into ASCII text file
.RETURN     status, 0:OK, 1:error
---------------------------------------------------------------------*/
char   *line;                 /* IN: text-line to be added           */
{
  int   nc, err;
  char  *pc, c;

  err = 0;
  if (tfd<0) return 1;

  pc = &buf[n];
  while ((c = *line++) && n<MXBUF) {
    if (sc) {
      switch (c) {
         case 't'  : c = '\t'; break; 
         case 'b'  : c = '\b'; break;
         case 'r'  : c = '\r'; break;
         case 'f'  : c = '\f'; break;
         case '\\' : c = '\\'; break;
	 case '0'  :
         case 'n'  : nc = osawrite(tfd,buf,n);
                     err = (n==nc) ? 0 : 1;
	             n = 0; pc = buf; sc = 0;
	             if (c=='0') return err;
                     continue;
	 }
      sc = 0;
    }
    else if (c == '\\') { sc = 1; continue; }
    *pc++ = c; n++;
  }

  if (MXBUF<=n) {                       /* buffer overflow - error   */
    nc = osawrite(tfd,buf,n);
    err = 1; n = 0;
  }

  return err;
}

int text_get(line)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    get text from ASCII text file
.RETURN     status: 0:EOF, 1:OK, -n:no. of non-ASCII char's
---------------------------------------------------------------------*/
char   *line;                 /* IN: text-line to be added           */
{
  char c;
  int  nc, i, nn;

  i = nn = 0;
  do {
    if (sc) { 
      *line++ = sc;
      if (sc=='0') break;
      sc = '\0';
      continue;
    }
    c = buf[n++];
    if (!c) {                             /* read next line          */
      nc = osaread(tfd,buf,MXBUF);
      n = 0;
      if (nc<0) {                         /* return - End-Of-File    */
	*line++ = '\\'; sc = '0';
	continue;
      }
      buf[nc] = '\n'; buf[nc+1] = '\0';
      c = buf[n++];
    }
    if (c<' ' || '~'<c || c=='\\')
      switch (c) {
         case '\n' : sc = 'n'; c = '\\'; break;
         case '\t' : sc = 't'; c = '\\'; break;
         case '\r' : sc = 'r'; c = '\\'; break;
         case '\f' : sc = 'f'; c = '\\'; break;
         case '\b' : sc = 'b'; c = '\\'; break;
         case '\\' : sc = '\\'; c = '\\'; break;
         default   : c = ' '; nn++; 
	 }
    *line++ = c;
  } while (i++<71);
  *line = '\0';

  if (sc=='0') return 0;
  return (nn) ? -nn : 1;
}

int text_close()
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    close  ASCII text file
.RETURN     status, 0:closed OK, 1:error
---------------------------------------------------------------------*/
{
  if (tfd<0) return 1;

  if (n) osawrite(tfd,buf,n);           /* flush last buffer to file */
  n = 0; sc = '\0';

  osaclose(tfd);
  osmmfree(buf);
  tfd = -1;

  return 0;
}
