/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT     (c)  1996  European Southern Observatory
.IDENT         fitshkw.c
.LAUGUAGE      C
.AUTHOR        P.Grosbol   ESO/IPG
.KEYWORDS      FITS keywords, hierarchical keywords
.VERSION       1.0  1990-Oct-24 : Creation - dummy,   PJG 
.VERSION       1.1  1990-Nov-11 : Actual implementation,   PJG 
.VERSION       1.2  1990-Nov-21 : Include no's at levels,   PJG 
.VERSION       1.3  1992-Jun-04 : Define default null keyword,   PJG 
.VERSION       1.4  1993-Aug-18 : Set idx=1 in 'kwnull',   PJG 
.VERSION       1.5  1996-Oct-22 : Add opt parameter for crypt,   PJG 
 050802		last modif

---------------------------------------------------------------------*/

#include       <stdio.h>      /* Define FITS keyword structures    */

#include       <fitsdef.h>      /* Define FITS keyword structures    */
#include       <fitskwb.h>      /* Basic keyword definitions         */
#include       <fitshkw.h>      /* Hierarchical keyword definitions  */

static  char   desc[MXMDN];     /* Internal storage for desc-name    */
static  KWDEF  kwnull[] = {     /* Null Keyword definition           */
   { "", '\0', 0, 0, "", 1, '\0', 0.0, ""},
   {(char *) 0, '\0', 0, 0, (char *) 0, 0, '\0', 0.0, (char *) 0}}; 

int fitshkw(kw,kwd,opt)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       check hierarchical keyword and creat MIDAS descriptor
               name etc.
.RETURN        status  0:OK, -1: error - incorrect hierarchical keyword
---------------------------------------------------------------------*/
KWORD      *kw;                 /* pointer to FITS keyword structure */
KWDEF      *kwd;                /* pointer to keyword descriptor def.*/
int        opt;                 /* Option: 1 -> old naming format    */
{
  char     *pc, *pd;
  int      n, nhk, no;
  HKWL     *phk;
  KWDEF    *kdef;

/*  reset variables and check no. of hierarchical levels             */

  for (n=0; n<MXMDN; n++) desc[n] = '\0'; 
  kwd->desc = "";
  kdef = (KWDEF *) 0;
  nhk = kw->hkn - 1;
  if (nhk<2) 
     {
     printf("%s\n",kw->kw);
     return -1;        /* Error - must be at least two level */
     }

  if (opt) {                   /* Old encrypting of names            */
    phk = hkwgrp;
    for (n=0; n<nhk && phk; n++) {      /* Search through know levels  */
      pc = kw->hkw[n];
      while (phk->name) {
	if (!fkwcmp(pc,phk->name,&no)) {      /* Level keyword found   */
	  fkwcat(desc,phk->abrv,no);
	  kdef = phk->kw;                     /* Set next level        */
	  phk = (HKWL *) phk->next;
	  break;
	}
	phk++;
      }
    }
    if (n!=nhk) return -2;            /* Error - not all levels found  */
    pd = desc;
    while (*pd) pd++;
    *pd++ = '_';
    pc = kw->hkw[nhk];
    while (kdef && kdef->kw) {        /* Search through lowest level   */
      if (!(*kdef->kw) || !fkwcmp(pc,kdef->kw,&no)) break;
      kdef++;
    }
    if (0<no) kw->kno = no;
    fkwcat(desc,pc,no);
  }
  else {                            /* Simple name mapping             */
    pd = desc;
    for (n=0; n<nhk; n++) {         /* Concatinate all keywords levels */
      pc = kw->hkw[n];
      while (*pc) *pd++ = *pc++;
      *pd++ = '.';                  /* Separate levels with '.'        */
    }
    pc = kw->hkw[nhk];
    while (*pc) *pd++ = *pc++;
    *pd = '\0';
  }

  kwd->desc = desc;
  if (!kdef || !(*kdef->kw)) kdef = kwnull;  /* No definition - use Null  */
  if (!(kdef->kw)) return -3;                /* Error - keyword not found */
  if (*kdef->kw) pc = kdef->kw;
  if (kdef->fmt) kwd->fmt = kdef->fmt;
  if (kdef->group) kwd->group = kdef->group;
  if (kdef->action) kwd->action = kdef->action;
  if (kdef->idx) kwd->idx = kdef->idx;
  if (kdef->type) kwd->type = kdef->type;
  if (kdef->fac) kwd->fac = kdef->fac;
  if (kdef->unit) kwd->unit = kdef->unit;

  return 0;
}
