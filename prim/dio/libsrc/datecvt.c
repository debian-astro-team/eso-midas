/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT       (c)   1994   European Southern Observatory
.IDENT           datecvt.c
.LANGUAGE        C
.AUTHOR          P.Grosbol,  ESO/IPG
.KEYWORDS        conversion, year+month+day to decimal year
.COMMENT         Convert three integers (year,month,day) to decimal year.
.VERSION         1.0   1988-Oct-12 : Creation,  PJG
.VERSION         1.1   1989-Oct-26 : Return zero on invalid input,  PJG
.VERSION         1.2   1990-Jun-20 : Allow day-no. upto 365,  PJG
.VERSION         1.3   1991-Mar-07 : Add fitsdate routine, PJG
.VERSION         1.4   1992-Sep-11 : Add ymddate function, PJG
.VERSION         1.5   1993-Jul-05 : Correct leap year, PJG
.VERSION         1.6   1993-Oct-26 : Update to prototypes, PJG
.VERSION         1.7   1994-May-10 : Correct size of date[], PJG
.VERSION         1.8   1997-Jul-29 : Update to Y2000 convention, PJG

 051012		last modif

-------------------------------------------------------------------------*/
#include <time.h>               /* define time structure 'tm'          */
#include <stdio.h>


static  char    date[81];         /* internal buffer for FITS date       */

double  dateymd(y,m,d)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    convert integer year+month+day to decimal year
.RETURN     return decimal year
-------------------------------------------------------------------------*/
int               y;                                 /* IN: year         */
int               m;                                 /* IN: month        */
int               d;                                 /* IN: day of month */
{
  int             i;
  double         dy;

  if (y<1 || m<1 || 12<m || d<1 || 365<d) return 0.0;
  i = m-1; d--;
  while (0 < i) d += "DADCDCDDCDCD"[--i] - '%';      /* days in month    */
  if (((y&3) == 0 && y%100 != 0) || y%400 == 0) 
     {						/* leap year (y&3 = y%4)*/
     if (2<m) d++; dy = 366.0;
  }
  else dy = 365.0;

  return (y+d/dy);
}

char *ymddate(y,m,d)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    convert year+month+day to date string
.RETURN     return pointer to date string
-------------------------------------------------------------------------*/
double      y;                                       /* IN: year         */
double      m;                                       /* IN: month        */
double      d;                                       /* IN: day of month */
{
  int       iy, im, id, i, leap;

  iy = y;
  leap = ((iy&3) == 0 && iy%100 != 0) || iy%400 == 0;  /* leap year        */
  if (m<1.0 && d<1.0) {
     id = (leap) ? 366.0*(y-iy)+0.5 : 365.0*(y-iy)+0.5;
     for (im=0; im<12; im++) {                       /* substract month  */
	i = "DADCDCDDCDCD"[im] - '%';                /* days in month    */
	if (leap && im==1) i++;
	if (id < i) break;
	id -=i;
      }
     im++; id++;
   }
  else { im = m+0.5; id = d+0.5; }
  if (00<=iy && iy<=99) iy += 1900;

  if (iy<1999) {
    iy -= 1900;
    sprintf(date,"%02d/%02d/%02d",id,im,iy);
  }
  else sprintf(date,"%04d-%02d-%02d",iy,im,id);

  return date;
}

char *fitsdate(uxt)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    convert time stamp to FITS date string
.RETURN     return pointer to FITS-date string
-------------------------------------------------------------------------*/
long           uxt;                         /* IN: UNIX time stamp       */
{
  struct tm   *tt;
  time_t      timer;

  *date = '\0';
  timer = (uxt==0) ? time((time_t *) 0) : (time_t) uxt;
  tt = gmtime(&timer);

  if (tt->tm_year<99)
    sprintf(date,"%02d/%02d/%02d",
	    tt->tm_mday, tt->tm_mon+1, tt->tm_year);
  else sprintf(date,"%04d-%02d-%02dT%02d:%02d:%02d",
	       tt->tm_year+1900, tt->tm_mon+1, tt->tm_mday,
	       tt->tm_hour, tt->tm_min, tt->tm_sec);

  return date;
}
