/* @(#)fkwstr.c	19.1 (ES0-DMD) 02/25/03 13:59:31 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c)  1996  European Southern Observatory
.IDENT      fkwstr.c
.LAUGUAGE   C
.AUTHOR     P.Grosbol   ESO/IPG
.KEYWORDS   FITS keywords, string routines
.VERSION    1.0  1990-Nov-21 : Creation,   PJG 
.VERSION    1.1  1996-Oct-22 : Allow also '-',   PJG 
---------------------------------------------------------------------*/

int fkwcmp(pkw,ptp,pn)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       compare FITS keyword with template including a '#' index
               field indicating a numeric subfield
.RETURN        status  0:OK-equal, 1: NOT-equal
---------------------------------------------------------------------*/
char      *pkw;                 /* pointer to FITS keyword string    */
char      *ptp;                 /* pointer to templete string        */
int       *pn;                  /* pointer to keyword index, 0=none  */
{
  char    c;

  *pn = 0;
  while ((c = *ptp++) && c!='#' && c==*pkw) pkw++;

  if (!c) {
    while ((c = *pkw++) && c==' ');
    if (!c) return 0;
  }
  else if (c=='#') {
    while ((c = *pkw) && '0'<=c && c<='9') {
      *pn = 10 * (*pn) + (c-'0');
      pkw++;
    }
    while ((c = *pkw++) && c==' ');
    if (!c) return 0;
    *pn = 0;
  }

  return 1;
}

int fkwcat(pkw,ptp,no)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       concatinate FITS keyword with template including a '#' 
               index field indicating a numeric subfield
.RETURN        status  0:OK
---------------------------------------------------------------------*/
char      *pkw;                 /* pointer to FITS keyword string    */
char      *ptp;                 /* pointer to templete string        */
int       no;                   /* keyword index, 0=none             */
{
  char    c;
  int     m, i;

  while (*pkw) pkw++;                       /* Find end of string    */
  while ((c = *ptp++) && c!='#')
    *pkw++ = (('A'<=c && c<='Z') || ('a'<=c && c<='z') ||
	      ('0'<=c && c<='9') || c=='_' || c=='-') ? c : '_';

  if (c=='#' && 0<no) {                     /* Add no. if #-index    */
    m = 1;
    while (no/m) m *= 10;
    while (m /= 10) { i = no/m; *pkw++ = i + '0'; no -= m*i; }
  }
  *pkw = '\0';

  return 0;
}
