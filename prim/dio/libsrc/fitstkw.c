/*===========================================================================
  Copyright (C) 1995,2003 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c)  1996,2003  European Southren Observatory
.IDENT      fitstkw.c
.LAUGUAGE   C
.AUTHOR     P.Grosbol   ESO/IPG
.KEYWORDS   FITS keywords, data type ckeck, conversion
.VERSION    1.0  1990-Feb-15 : Creation,   PJG 
.VERSION    1.1  1990-Mar-19 : Error on unknown type,   PJG 
.VERSION    1.2  1990-Oct-23 : Include HIERARCH type,   PJG 
.VERSION    1.3  1993-Oct-12 : Force comment for comment card,   PJG 
.VERSION    1.4  1993-Oct-26 : Update to new SC + prototypes,   PJG 
.VERSION    1.5  1996-Nov-12 : New date string, decode strings, PJG
.VERSION    1.6  1996-Nov-22 : Correct string decoding, PJG
.VERSION    1.7  1996-Dec-09 : Correct initiation of 'pc', PJG
.VERSION    1.8  2002-Jan-11 : Add code for logical keywords, PJG

 030919		last modif
---------------------------------------------------------------------*/

#include    <math.h>
#include    <string.h>
#include    <stdio.h>
#include    <fitsdef.h>         /* Define FITS keyword structures    */
#include    <midas_def.h>




#ifdef __STDC__
int fitstkw(KWORD *kw, char fmt)
#else
int fitstkw(kw,fmt)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       perform data type/format check of FITS keyword and
               convert data format if possible.
.RETURN        status  0:OK, -1: error - incompatible data types
---------------------------------------------------------------------*/
KWORD      *kw;                 /* pointer to FITS keyword structure */
char       fmt;                 /* expected data format of keyword   */
#endif

{
double   d, th, tm, ts, dateymd();

int      nd, iy, im, id;

char     *pc, *ps, text[80];
register char  rfmt;




rfmt = kw->fmt;
if (rfmt == fmt) return 0; 
if (rfmt == '?') return -1;   



switch (fmt)
   {
    case 'H'  :                       /* Hierarch keyword card      */
    case 'N'  :                       /* Not defined data type      */
    case '\0' : return  0;            /* data type not checked - OK */

    case 'L'  : if (rfmt == 'I') 
                   {			/* convert int -> logical  */
                   kw->fmt = 'L';
                   return 0;
		   }
                break;

    case 'I'  : if (rfmt == 'R')
                   {			/* convert real -> int     */
                   d = kw->val.d[0];
                   kw->val.i = d; kw->fmt = 'I';
                   sprintf(text,
                     "Warning: Keyword >%s< truncated to int!",kw->kw);
                   SCTPUT(text);
                   return 0;
		   }
                else if (rfmt == 'S') 
                   {			/* convert string -> int */
		   pc = kw->val.pc;
		   nd = sscanf(pc,"%d",&im);
		   if (nd<1) break;
		   kw->val.i = im; kw->fmt = 'I';
		   sprintf(text,"Warning: Keyword >%s< string to int!",
			   kw->kw);
		   SCTPUT(text);
		   return 0;
		   }
                break;

    case 'R'  :
    case 'D'  : if (rfmt == 'I') 
                   {			/* convert int -> real     */
                   d = kw->val.i; 
                   kw->val.d[0] = d; kw->fmt = 'R';
                   return 0;
		   }
                else if (rfmt == 'S')
                   {			/* convert string -> real */
                   pc = kw->val.pc;
                   if (strchr(pc,':')) 
                      {
		      if ((ps=strchr(pc,'-')) || (ps=strchr(pc,'+')))
		         pc = ps + 1; 
		      nd = sscanf(pc,"%lf:%lf:%lf",&th,&tm,&ts);
		      id = ((ps && *ps=='-') || th<0.0);
		      d  = fabs(th) + fabs(tm)/60.0 + fabs(ts)/3600.0;
		      if (id) d = -d;
		      }
		   else nd = sscanf(pc,"%lf",&d);

	           if (nd<1) break;
                   kw->val.d[0] = d; kw->fmt = 'R';
		   sprintf(text,"Warning: Keyword >%s< string to real!",
			   kw->kw);
		   SCTPUT(text);
                   return 0;
		   }
                break;

    case 'T'  : if (rfmt == 'S')
                   {			/* convert string -> time  */
                   pc = kw->val.pc;
		   nd = 0;
		   iy = im = id = 0;
		   th = tm = ts = 0.0;
		   if (pc[2]=='/' && pc[5]=='/') /* old date format */
		      nd = sscanf(pc, "%d/%d/%d", &id,&im,&iy);
		   else if (pc[4]=='-' && pc[7]=='-') /* new format */
		      nd = sscanf(pc, "%d-%d-%dT%lf:%lf:%lf",
				  &iy,&im,&id,&th,&tm,&ts);
		   if (nd<3 || iy<0 || im<1 || id<1)
                      {
		      sprintf(text,"Error: Keyword >%s< wrong date format!",
			      kw->kw);
		      SCTPUT(text);
                      break;
		      }
                   if (iy<100) iy += 1900;
                   kw->fmt = 'R';
		   kw->val.d[0] = dateymd(iy,im,id);
		   kw->val.d[1] = th + tm/60.0 + ts/3600.0;
                   return 0;
                   }
                else if (rfmt == 'R') return 0;
                break;

    case 'C'  :                       /* Comment keyword card       */
                kw->fmt = 'C';
                kw->val.pc = &kw->buf[0];
                return 0;

    default   : return -1;
   }

return -1;
}
