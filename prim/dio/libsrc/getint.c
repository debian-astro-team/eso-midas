/* @(#)getint.c	19.1 (ES0-DMD) 02/25/03 13:59:31 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c) 1993   European Southern Observatory
.IDENT      getint.c
.LANGUAGE   C
.AUTHOR     P.Grosbol,  ESO/IPG
.KEYWORDS   conversion, integer
.COMMENT    Conversion of an ASCII string to a binary integer.
            The routine checks if string could be read as a integer
            and flags it.
.VERSION    1.0   1988-Dec-08 : Creation,  PJG
.VERSION    1.1   1989-Jun-25 : Correct return count,  PJG
.VERSION    1.2   1993-Oct-26 : Change type of value to 'int',  PJG
-------------------------------------------------------------------------*/

int  getint(pc,mc,pi,pint)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    convert string to 'int'
.RETURN     no. of characters read from string
-------------------------------------------------------------------------*/
char              *pc;                      /* string to be converted    */
int                mc;                      /* max. no. of char to scan  */
int               *pi;                      /* flag if value was integer */
int             *pint;                      /* decoded value             */
{
  char    *s;
  int     n;
  int     sign,val;
 
  s = pc;
  val = 0; sign = 1; n = mc;
  *pint = 0; *pi = 1;
  if (!s || n<=0) return 0;

  while ( *s==' ' || *s=='\t' ) {                 /* skip spaces        */
     if (!(--n)) { *pint = 0; return mc; }
     s++;
  }

  if ( *s=='+' || *s=='-' ) {                     /* get sign           */
     sign = ( *s++ == '+' ) ? 1 : -1;
     if (!(--n)) { *pint = 0; return mc; }
  }

  while ( (*s>='0' && *s<='9') || *s==' ' ) {     /* decode number      */
     if (*s!=' ') val = 10 * val + ( *s - '0' );
     if (!(--n)) { *pint = sign*val; return mc; }
     s++;
  }

  *pint = sign*val; *pi = 0;
  return mc-n;
}
