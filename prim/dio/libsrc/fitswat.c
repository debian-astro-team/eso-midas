/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c)  1991   European Southern Observatory
.IDENT      fitswat.c
.LAUGUAGE   C
.AUTHOR     P.Grosbol   ESO/IPG
.KEYWORDS   FITS ASCII table data
.COMMENT    write FITS ASCII table data
.VERSION    1.0  1988-Nov-16 : Creation,   PJG 
.VERSION    1.1  1989-May-24 : Correct error for null-values,   PJG 
.VERSION    1.2  1989-Jul-05 : Include I*4 data type,   PJG 
.VERSION    2.0  1991-Mar-17 : Change call-seq. and structures,   PJG 
.VERSION    2.1  1991-Oct-25 : Correct error for ASCII columns,   PJG 
 050802		last modif

---------------------------------------------------------------------*/

#include   <stdio.h>

#include   <fitsfmt.h>
#include   <fitsdef.h>
#include   <fitskwt.h>
#include   <midas_def.h>

int fitswat(mfd)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       write FITS ASCII table data
.RETURN        return status  0:OK, -1:error
---------------------------------------------------------------------*/
int           mfd;             /* IN: MIDAS file number              */
{
  register char  *pb;
  register int   i, nb;
  char           *buf;
  int            nrw, ncol, nrow;
  int            nr, nc, null, n;
  float          f;
  double         d;
  FDEF           *fd;
  TXDEF          *tdef, *fitstbl();

  tdef = fitstbl(-1,' ',' ');
  nrw  = tdef->mxrow;
  nrow = tdef->nrow;
  ncol = tdef->tfields;

  buf=(char *)osmmget(nrw+1);			     /* get internal buffer */
  if (!buf) {
     SCTPUT("Error: cannot allocate internal buffer");
     return -1;
  }

  for (nr=1; nr<=nrow; nr++) {               /* write row by row    */
      pb = buf; fd = tdef->col;
      for (nc=1; nc<=ncol; nc++, fd++) {     /* encode each column  */
          nb = fd->twdth;
          switch (fd->tdfmt) {
             case 'A' :                               /* string     */
                  TCERDC(mfd,nr,nc,pb,&null);
		  i = 0;
                  if (null) while (nb--) *pb++ = ' ';
                   else while (nb--) {
		      if (i || (i = !(*pb))) *pb = ' ';
		      pb++;
		    }
                  break;
             case 'I' :                               /* integer    */
                  TCERDI(mfd,nr,nc,&n,&null);
                  if (null) while (nb--) *pb++ = ' ';
                   else { sprintf(pb,fd->tform,n); pb += nb; }
                  break;
             case 'E' :                               /* float      */
                  TCERDR(mfd,nr,nc,&f,&null);
                  if (null) while (nb--) *pb++ = ' ';
                   else { sprintf(pb,fd->tform,f); pb += nb; }
                  break;
             case 'D' :                               /* double     */
                  TCERDD(mfd,nr,nc,&d,&null);
                  if (null) while (nb--) *pb++ = ' ';
                   else { sprintf(pb,fd->tform,d); pb += nb; }
                  break;
             case 'S' :                               /* short int. */
                  TCERDI(mfd,nr,nc,&n,&null);
                  if (null) while (nb--) *pb++ = ' ';
                   else { sprintf(pb,fd->tform,n); pb += nb; }
                  break;
             case 'B' :                           /* unsigned char. */
                  TCERDI(mfd,nr,nc,&n,&null);
                  if (null) while (nb--) *pb++ = ' ';
                   else { sprintf(pb,fd->tform,n); pb += nb; }
                  break;
             case 'L' :                               /* logical    */
                  TCERDI(mfd,nr,nc,&n,&null);
                  *pb++ = (n) ? 'T' : 'F';
                  break;
             case 'X' :                               /* hex flags  */
                  TCERDI(mfd,nr,nc,&n,&null);
                  sprintf(pb,fd->tform,n);
		  pb += nb;
                  break;
          }
      }
      dwrite(buf,nrw);
  }

  dbfill(' ');
  osmmfree(buf);
  return 0;
}
