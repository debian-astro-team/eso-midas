/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c)  1993-2005   European Southern Observatory
.IDENT      fitsthd.c
.LAUGUAGE   C
.AUTHOR     P.Grosbol   ESO/IPG
.KEYWORDS   FITS, header type
.COMMENT    determine the type of a FITS header
.VERSION    1.0  1988-Nov-18 : Creation,   PJG 
.VERSION    1.1  1990-Jan-02 : Update to new keyword structure, PJG 
.VERSION    1.2  1991-Mar-16 : Change include file, add BINTABLE, PJG 
.VERSION    1.3  1992-Feb-20 : Include IMAGE extension, PJG 
.VERSION    1.4  1992-Aug-13 : Correct definition of IMAGE extension, PJG 
.VERSION    1.5  1993-Apr-16 : Skip tailing spaces in string comp,, PJG 

 050715		last modif

---------------------------------------------------------------------*/

#include   <fitsfmt.h>
#include   <fitsdef.h>

static int  htype = NOFITS;	/* present header type            */
static int  exthd;		/* flag for extension header      */
static int  nax;		/* no. of axes in data matrix     */

typedef struct {                   /* names and type for extensions  */
                 char      *name;  /* name of extension              */
                 int        type;  /* extension type no.             */
               } XTYPE;

/*
static  XTYPE  xtype[] = { {"TABLE   ",ATABLE},   /. ASCII table     ./
                           {"BINTABLE",BTABLE},   /. Binary table    ./
                           {"A3DTABLE",BTABLE},   /. Binary table    ./
                           {"IMAGE   ",IMAGE},    /. Image ext.      ./
                           {(char *) 0, 0},
                         };
*/

static  XTYPE  xtype[] = { {"TABLE",ATABLE},   /* ASCII table     */
                           {"BINTABLE",BTABLE},   /* Binary table    */
                           {"A3DTABLE",BTABLE},   /* Binary table    */
                           {"IMAGE",IMAGE},    /* Image ext.      */
                           {(char *) 0, 0},
                         };

int fitsthd(lno,kw)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       Check format and type of FITS header
.RETURN        type of FITS header (see fitsdef.h), -2: error
---------------------------------------------------------------------*/
int    lno;                 /* line no. of keyword in header  */
KWORD  *kw;                 /* pointer to keyword structure   */

{
int            ok,n;

switch (lno) 
   {			/* check position dependent keywords */
  case  1 :			/* first header line    */
   htype = NOFITS;
   if (kwcomp(kw->kw,"SIMPLE  ")) 
      {					/* main header  */
      exthd = 0; htype = FBFITS;
      if (kw->fmt=='L' && kw->val.i) htype = BFITS;
      }
   else 
      {
      if (kwcomp(kw->kw,"XTENSION"))   /* extension    */
         {
         if (kw->fmt=='S') 
            {
            exthd = 1; htype = UKNOWN;
            for (n=0; xtype[n].name; n++)
               {
               if (kwcomp(kw->val.pc,xtype[n].name)) 
                  {
                  htype = xtype[n].type; 
                  break;
                  }
               }
            }
         }
      }
   break;

  case  2 :			/* second header line   */
   ok = 0;
   if (kwcomp(kw->kw,"BITPIX  "))
      {
      if (kw->fmt=='I')
         {
         switch (htype) 
            {
	   case IMAGE  :
           case BFITS  : if ((n=kw->val.i)==32 ||
                             n==16 || n==-32 ||
                             n==8 || n==-64) ok = 1;
                         break;
           case UKNOWN : ok = 1;
                         break;
           case ATABLE :
           case BTABLE : if (kw->val.i == 8) ok = 1;
                         break;
            }
         }
      }
   if (!ok) htype = FBFITS;
   break;

  case  3 :			/* third header line    */
   if (!kwcomp(kw->kw,"NAXIS   ") ||
       kw->fmt!='I' || kw->val.i<0) htype = NOFITS;
   nax = kw->val.i;
   break;

  case  4 :			/* fourth header line   */
   if (0<nax)
      {
      if (kwcomp(kw->kw,"NAXIS1  ") && kw->fmt=='I') 
         {
         if (!(kw->val.i || exthd)) htype = RGROUP;
         }
      else 
         htype = FBFITS;
      }
   break;
   }

  return htype;
}

int kwcomp(pk,ps)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE      compare two strings
.RETURN       0: not equal, 1: equal
---------------------------------------------------------------------*/
char  *pk;               /* keyword string                   */
char  *ps;               /* string to compare keyword with   */

{
while (*ps && (*pk == *ps)) pk++, ps++;
while (*pk == ' ') pk++;

return !(*ps || *pk);
}
