/* @(#)getval.c	19.1 (ES0-DMD) 02/25/03 13:59:31 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c) 1989   European Southern Observatory
.IDENT      getval.c
.LANGUAGE   C
.AUTHOR     P.Grosbol,  ESO/IPG
.KEYWORDS   conversion, double
.COMMENT    Conversion of an ASCII string to a binary double real.
            The routine checks if string could be read as a integer
            and flags it.
.VERSION    1.0   1988-Sep-23 : Creation,  PJG
.VERSION    1.1   1989-Jun-26 : Correct return char. count,  PJG
-------------------------------------------------------------------------*/

int  getval(pc,mc,pi,pdbl)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    convert string to 'double'
.RETURN     no. of characters read from string
-------------------------------------------------------------------------*/
char              *pc;                      /* string to be converted    */
int                mc;                      /* max. no. of char to scan  */
int               *pi;                      /* flag if value was integer */
double          *pdbl;                      /* decoded value             */
{
  register  char    *s;
  register  int     n;
  int               psign,exp,sign;
  double            fac,val,power;
 
  s = pc; n = mc;
  val = 0.0; power = 1.0; fac = 1.0;
  exp = 0; sign = 1; *pi = 1;
  if (!s || n<=0) return 0;

  while ( *s==' ' || *s=='\t' ) {                 /* skip spaces        */
     if (!(--n)) { *pdbl = 0.0; return mc; }
     s++;
  }

  if ( *s=='+' || *s=='-' ) {                     /* get sign           */
     sign = ( *s++ == '+' ) ? 1 : -1;
     if (!(--n)) { *pdbl = 0.0; return mc; }
  }

  while ( (*s>='0' && *s<='9') || *s==' ' ) {     /* decode number      */
     if (*s!=' ') val = 10.0 * val + ( *s - '0' );
     if (!(--n)) { *pdbl = sign*val; return mc; }
     s++;
  }

  if ( *s=='.' ) {                                /* fraction part      */
     s++; *pi = 0;
     if (!(--n)) { *pdbl = sign*val*power; return mc; }
     while ( (*s>='0' && *s<='9') || *s==' ' ) {
       if (*s!=' ') { 
          val = 10.0 * val + (*s - '0'); power /= 10.0;
       }
       if (!(--n)) { *pdbl = sign*val*power; return mc; }
       s++;
     }
  }

  if ( *s=='e' || *s=='E' || *s=='d' || *s=='D' ) {   /* exponent part  */
     s++; psign = 1; *pi = 0;
     if (!(--n)) { *pdbl = sign*val*power; return mc; }
     if ( *s=='-' || *s=='+' ) {
        psign = (*s++ == '+') ? 1 : 0;
	if (!(--n)) { *pdbl = sign*val*power; return mc; }
     }
     while ( (*s>='0' && *s<='9') || *s==' ' ) {
        if (*s!=' ') exp = 10 * exp + ( *s - '0' );
	if (!(--n)) break;
	s++;
     }
     if (psign)
        while (exp--) power *= 10.0;
     else
        while (exp--) power /= 10.0;
  }

  *pdbl = sign*val*power;
  return mc-n;
}
