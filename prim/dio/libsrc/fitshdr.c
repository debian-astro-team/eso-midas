/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT      fitshdr.c
.LAUGUAGE   C
.AUTHOR     P.Grosbol   ESO/IPG
.KEYWORDS   FITS header, structure, initiation
.COMMENT    Define FITS header structure
.VERSION    1.0  1991-Mar-17 : Creation,   PJG 
.VERSION    1.1  1991-Sep-23 : Update table FDEF initiation,   PJG 
.VERSION    1.2  1992-Feb-20 : Add theap to tables,   PJG 

 051021		last modif
---------------------------------------------------------------------*/

#include    <stdio.h>
#include   <stdlib.h>

#include   <fitsfmt.h>
#include   <fitsdef.h>
#include   <fitskwt.h>

static     BFDEF       bfdef;    /* FITS header structure            */
static     ADEF  adef[MXDIM];    /* definition of data matrix axes   */
static     PDEF  pdef[MXPAR];    /* definition of groups parameters  */

static     TXDEF       txdef;    /* Table extension definitions      */
static     FDEF    fdef[MXF];    /* definition of table fields       */
/*

*/

int hdr_init_M(bpntr,apntr,ppntr,cf)
BFDEF *bpntr;
ADEF **apntr;
PDEF **ppntr;
int cf;			/* = 0, also init bpntr->count
			   = 1, leave bpntr->count as it is  */

{
int  i;
char *wpntr;

ADEF  *ad;
PDEF  *pd;

if (cf == 0) bpntr->count = 0;
bpntr->cflag = -1;
bpntr->tflag = 0;
bpntr->bflag = 0;
bpntr->sflag = 0;
bpntr->xflag = 0;
bpntr->mflag = 0;
bpntr->mtype = 0;
bpntr->kwflag = 0;
bpntr->naxis = 0;
bpntr->pcount = 0;
bpntr->gcount = 1;
bpntr->bscale = 1.0;
bpntr->bzero = 0.0;
bpntr->bunit[0] = '\0';
bpntr->extname[0] = '\0';

wpntr = bpntr->ident;
for (i=0; i<MXIDNT-1; i++) *wpntr++ = ' ';
*wpntr = '\0';

bpntr->data = *apntr;
ad = bpntr->data;
for (i=0; i<MIDAS_MXDIM; i++)
   {
   ad[i].naxis = 0;
   ad[i].crval = 1.0;
   ad[i].crpix = 1.0;
   ad[i].cdelt = 1.0;
   ad[i].crota = 0.0;
   ad[i].ctype[0] = '\0';
   }

bpntr->parm = *ppntr;
pd = bpntr->parm;
for (i=0; i<MXPAR; i++) 
   {
   pd[i].pscal = 1.0;
   pd[i].pzero = 0.0;
   pd[i].ptype[0] = '\0';
   }

bpntr->extd = (char *) 0;
return 0;
}
/*

*/

BFDEF *hdr_init()
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    initiate the FITS header structure
.RETURN     pointer to FITS header structure
---------------------------------------------------------------------*/
{
int  i;
char  *wpntr;


bfdef.count = 0;
bfdef.cflag = -1;
bfdef.tflag = 0;
bfdef.bflag = 0;
bfdef.sflag = 0;
bfdef.xflag = 0;
bfdef.mflag = 0;
bfdef.mtype = 0;
bfdef.kwflag = 0;
bfdef.naxis = 0;
bfdef.pcount = 0;
bfdef.gcount = 1;
bfdef.bscale = 1.0;
bfdef.bzero = 0.0;
bfdef.bunit[0] = '\0';
bfdef.extname[0] = '\0';

wpntr = bfdef.ident;
for (i=0; i<MXIDNT-1; i++) *wpntr++ = ' ';
*wpntr = '\0';
 
/*
for (i=0; i<MXIDNT; i++) bfdef.ident[i] = ' ';
bfdef.ident[MXIDNT-1] = '\0';
*/

bfdef.data = adef;
  for (i=0; i<MIDAS_MXDIM; i++) 
   {
   adef[i].naxis = 0;
   adef[i].crval = 1.0;
   adef[i].crpix = 1.0;
   adef[i].cdelt = 1.0;
   adef[i].crota = 0.0;
   adef[i].ctype[0] = '\0';
   }

bfdef.parm = pdef;
for (i=0; i<MXPAR; i++) 
   {
   pdef[i].pscal = 1.0;
   pdef[i].pzero = 0.0;
   pdef[i].ptype[0] = '\0';
   }

bfdef.extd = (char *) 0;

return &bfdef;
}
/*

*/

int hdr_tbl_M(bpntr,nocols)
BFDEF *bpntr;
int  nocols;		/* IN: no. of columns */

{
int  i;
unsigned int  sizze;

char  *mypntr;

TXDEF *tpntr;
FDEF  *ff;




/* see, if pointers already there */

if (bpntr->extd != (char *) 0) return (0);


mypntr = (char *) malloc((size_t) (sizeof(txdef)));
if (mypntr == (char *) 0) return (-5);

tpntr = (TXDEF *) mypntr;
bpntr->extd = mypntr;				/* save pointer here */

sizze = (unsigned int) (nocols*sizeof(fdef[0]));   /* only the size needed */
mypntr = (char *) malloc((size_t)sizze);
if (mypntr == (char *) 0) return (-5);
ff = (FDEF *) mypntr;

tpntr->nrow = 0;
tpntr->mxrow = 0;
tpntr->mxcol = 0;
tpntr->theap = 0;
tpntr->tfields = nocols;
tpntr->col = ff;				/* save pointer here */

for (i=0; i<nocols; i++) 		/* only actual no. of columns ... */
   {
   ff[i].twdth = 0;
   ff[i].tbcol = -1;
   ff[i].tdfmt = '\0';
   ff[i].tdfdd = 0;
   ff[i].tncpf = 1;
   ff[i].trepn = 1;
   ff[i].sflag = 0;
   ff[i].tscal = 1.0;
   ff[i].tzero = 0.0;
   ff[i].nflag = 0;
   ff[i].tnnul = 0;
   ff[i].tnull[0] = '\0';
   ff[i].ttype[0] = '\0';
   ff[i].tunit[0] = '\0';
   ff[i].tform[0] = '\0';
   ff[i].tdisp[0] = '\0';
   }

return 0;
}
/*

*/

TXDEF *hdr_tbl(nocols)
int  nocols;		/* IN: no. of columns */

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE    initiate the FITS header structure
.RETURN     pointer to FITS table header structure
---------------------------------------------------------------------*/
{
int  i;




bfdef.extd = (char *) &txdef;
txdef.nrow = 0;
txdef.mxrow = 0;
txdef.mxcol = 0;
txdef.theap = 0;
txdef.tfields = nocols;
txdef.col = fdef;
for (i=0; i<nocols; i++) 	/* only actual no. of columns */
   {
   fdef[i].twdth = 0;
   fdef[i].tbcol = -1;
   fdef[i].tdfmt = '\0';
   fdef[i].tdfdd = 0;
   fdef[i].tncpf = 1;
   fdef[i].trepn = 1;
   fdef[i].sflag = 0;
   fdef[i].tscal = 1.0;
   fdef[i].tzero = 0.0;
   fdef[i].nflag = 0;
   fdef[i].tnnul = 0;
   fdef[i].tnull[0] = '\0';
   fdef[i].ttype[0] = '\0';
   fdef[i].tunit[0] = '\0';
   fdef[i].tform[0] = '\0';
   fdef[i].tdisp[0] = '\0';
   }

return &txdef;
}
