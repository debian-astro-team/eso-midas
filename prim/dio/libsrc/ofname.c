/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT      ofname.c
.LAUGUAGE   C
.AUTHOR     P.Grosbol   ESO/IPG
.KEYWORDS   file names, output frames
.COMMENT    Generates output file names for 'intape' program
.VERSION    1.0  1988-Aug-20 : Creation,   PJG 
.VERSION    1.1  1990-Jul-02 : Upgrade and use index,   PJG 
.VERSION    1.2  1990-Sep-28 : Lower case ext. letters,   PJG 
.VERSION    1.3  1991-Mar-19 : Include FIT files,   PJG 
.VERSION    1.4  1994-Oct-19 : Just prefix name if no<0,   PJG 

 090119		last modif KB
---------------------------------------------------------------------*/

#include   <stdlib.h>
#include   <stdio.h>
#include   <string.h>

#include   <midas_def.h>

#define        MXNAME       128		/* max. length of new file name */

static  int    idx,ino,tno,fno;		/* image and table extension no. */
static  int    xflag, xno;
static  char   fname[MXNAME];		/* internal buffer for file name */

int outname(name,no,start_opt)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       initiate name of output file in intape program
.RETURN        status  0:OK, -1: error
---------------------------------------------------------------------*/
char        *name;		/* pointer to prefix label        */
int         no;			/* relative no. of file           */
char        start_opt;		/* root flag:
				   'x', 'y' used in MID_fitsin,
				   'o' the original usage         */

{
int        ww, n;

char       c;



xno = ino = tno = fno = 0;		/* init counters */

for (n=0; n<MXNAME; n++) fname[n] = '\0';
for (n=0; (c = *name++) && c!=' ' && n<MXNAME-9; n++) fname[n] = c;
idx = n;

xflag = 0;				/* init root-flag */
if (start_opt != 'o')
   {					/* here for 'x' and 'y' option */
   if (start_opt == 'x') xflag = -1;	/* for 'x' update root flag */

   return 0;				/* for 'y' just avoid the 0000 basis */
   }

if (MXNAME<n+9) return -1;

n = no; ww = 1;                    /* add. no. to file name        */
while (n /= 10) ww++;

if (ww<4) ww = 4;
while (ww-- && idx<MXNAME-1) fname[idx++] = '0';

n = idx;
do { fname[--n] += no % 10; } while ( no /= 10 );

return 0;
}

#ifdef __STDC__
char * newfn(char type , char * ext)
#else
char *newfn(type,ext)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       generate final output file name
.COMMENT       file no. is NOT incremented if 'ext' is a NULL pointer.
.RETURN        pointer to new file name
---------------------------------------------------------------------*/
char         type;		/* file type: I image, T table,
				   F fitfile, A all */
char         *ext;		/* string with file extension     */
#endif

{
int        i, n, ww;

char       c, number[8];



if (!ext) return fname;         /* NULL pointer - return file name */


/*
if (type == 'A') 
   {		
   if (xflag == -1)
       xno ++;

   return fname;
   }
*/



if (xflag == 1)		/* build name_DDDD.yyy */
   {
   switch (type)
      {
      case 'I' :
      case 'T' :
      case 'F' :
       (void) sprintf(number,"%4.4d",xno); xno ++;
       (void) strcpy(&fname[idx],number);               /* nameDDDD */
       i = idx + 5;
       break;

      default  :
       i = idx;
      }                                 /* `i' is now length of `fname' */


   while ((c = *ext++) && i<MXNAME-1) fname[i++] = c;
   fname[i] = '\0';                                     /* nameDDDD.yyy */
   }

else if (xflag == 0)
   {				/* build nameDDDDa.yyy */
   switch (type) {
       case 'I' : n = ino++; break;
       case 'T' : n = tno++; break;
       case 'F' : n = fno++; break;
          default  : n = 0;
       }

   ww = 0;
   if (n) 
      {				/* append a, b, c, ... */
      i = --n; ww = 1;
      while (i /= 26) ww++;
      i = idx + ww - 1;
      if (i<MXNAME-1)
        do { fname[i--] = 'a' + n%26; } while (n /= 26);
      }

   i = idx + ww;
   while ((c = *ext++) && i<MXNAME-1) fname[i++] = c;
   fname[i] = '\0';
   }

/* for xflag = -1 just keep the orig name, which was: FITZname.yyyNNN */

return fname;
}

/*

*/

int xoutname(name)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       init name of output file in indisk program
.RETURN        status  0:OK, -1: error
---------------------------------------------------------------------*/
char        *name;                 /* pointer to prefix label        */

{
register int   n;

register char  c;



xno = ino = tno = fno = 0;		/* init counter */
xflag = 1;                              /* set root-flag */

idx = (int) strlen(name);
if (MXNAME < (idx+10)) return -1;		/* name_0001.yyy */

xno = 0; 					/* re-init counter */

for (n=0; n<idx; n++)
   {
   c = name[n];
   if (c != ' ')
      fname[n] = c;
   else
      {
      idx = n;
      break;
      }
   }

fname[idx] = '\0';				/* length of name */

return 0;
}

/*

*/

void fillHISTORY(imno,text,tlen)
int  imno;      /* IN: frame no. */
char *text;     /* IN: file name if tlen = 0
		       else any text */
int  tlen;      /* IN: if > 0, length of text */


{
int  hsize, n, m, iav, klen, prelen;

char  *mypntr, *newpntr, work[32];



/* ensure, that HISTORY is multiple of 80 chars */

prelen = hsize = 0;
(void) SCDFND(imno,"HISTORY",work,&n,&m);
if (work[0] == 'C')
   {
   hsize = n*m;				/* curent size */
   m = hsize / 80;
   iav = m * 80;
   if (iav < hsize)                         /* force multiple of 80 chars. */
      {
      iav += 80;
      prelen = iav - hsize;
      }
   }


if (tlen > 0)
   n = tlen;
else
   {
   n = (int) strlen(text);
   n += 20;			/* for string 'Converted...' */
   }
m = n / 80;
iav = m * 80;
if (iav < n)
   {
   iav += 80;
   klen = iav - n;
   }
else
   klen = 0;

iav += prelen;			/* for blanks in front */
mypntr = (char *) malloc((size_t)(iav+2));
if (mypntr == (char *) 0)
   {
   SCETER(33,"Could not allocate memory...!");
   }

if (prelen > 0)
   memset((void *)mypntr,32,(size_t)prelen);	/* clear in front */

newpntr = mypntr + prelen;
if (tlen > 0)
   {
   (void) strcpy(newpntr,text);
   n = tlen;
   }
else
   n = sprintf(newpntr,"Converted from: %s",text);

if (klen > 0)					/* clear at end */
   memset((void *)(newpntr+n),32,(size_t)klen);

*(mypntr+iav) = '\0';
(void) SCDWRC(imno,"HISTORY",1,mypntr,hsize+1,iav,&m);


/* for debugging:
(void) SCDFND(imno,"HISTORY",work,&n,&m);
hsize = n*m;		
n = hsize/80;
m = n*80;
if (m != hsize) 
   {
   sprintf(mypntr,"prelen, iav, klen, tlen = %d, %d, %d, %d\n",prelen, iav, klen, tlen);
   SCTPUT(mypntr);
   sprintf(mypntr,"!OJO! new size = %d\n",hsize);
   SCTPUT(mypntr);
   }
*/

free((void *) mypntr);
}




