/* @(#)fitssxd.c	19.1 (ES0-DMD) 02/25/03 13:59:29 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT  (c)  1993   European Southern Observatory
.IDENT      fitssxd.c
.LAUGUAGE   C
.AUTHOR     P.Grosbol   ESO/IPG
.KEYWORDS   FITS extension, skip block
.COMMENT    skip unknown FITS extension
.VERSION    1.0  1988-Sep-11 : Creation,   PJG 
.VERSION    1.1  1991-Jan-25 : Change include file,   PJG 
.VERSION    1.2  1993-Oct-26 : Update to new SC + prototypes,   PJG 
---------------------------------------------------------------------*/
#include   <fitsfmt.h>
#include   <fitsdef.h>
#include   <midas_def.h>

int fitssxd(size,Midas_flag)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE       skip over FITS extension
.RETURN        error code - 0: OK, -1: error
---------------------------------------------------------------------*/
int        size;		/* IN: size of data matrix (bytes)    */
int        Midas_flag;		/* IN: = 0, `usual' mode
                                       = 1, internal FITS access in Midas */

{
char   *pb;
int    nb, kcount;


kcount = 0;
while (0<size) 
   {                /* skip through all extension data */
   nb=dread(&pb,FITSLR);
   if (nb!=FITSLR) 
      {
      SCTPUT("Error: wrong block size");
      return NOFITS;
      }
   if (Midas_flag == 1) kcount++;   
   size -= nb;
   }

return kcount;		/* inside Midas return the no. of records skipped */
}
