/*===========================================================================
  Copyright (C) 1989-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tdatatbl.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table aplication (Design 2.0)
.COMMENTS    This module contains the code executed with the commands
             
             create/table, show/table, read/table, print/table,
             delete/column, create/column, merge/table, name/column,
             sort/table, project/table, union/table
    
.PURPOSE     Main program activating the different routines.

.VERSION     1.0 25-Mar-1989 JDP  Implementation      
.VERSION     1.1 16-Jan-1990 JDP  Include WRITE/TABLE 
.VERSION     1.2 16-Jan-1990 JDP  Include COPY/KT, COPY/TK     
.VERSION     1.3 07-Dec-1990 FO   Added tbl_retro (Conversion)
.VERSION     1.4 08-JUl-1992 MP   add tbl_join, tbl_comp, tbl_select

 090710		last modif
------------------------------------------------------------*/

#include <midas_def.h>


int main()

{
int status;

int  tbl_create(),  tbl_read(),   tbl_show(),  tbl_sort(), tbl_delcol();
int  tbl_creacol(), tbl_copy(),   tbl_merge(), tbl_namecol();
int  tbl_project(), tbl_setref(), tbl_write();
int  tbl_copykt(),  tbl_copytk(), tbl_crview();
int  tbl_comp() ,  tbl_select(), tbl_join();
int  tbl_addnewrow(), tbl_deleterow(), tbl_copyti(), tbl_copyit();

int  tbl_getpar();
int  tk_add(), tk_cexec();

char command[8];



                                          /* get into MIDAS */
SCSPRO("TDATATBL");
  
                                          /* get command name */
status = tbl_getpar("MID$CMND",4,command);

                                          /* define actions in the program */

tk_add(0,0,tbl_copy,    "copy");           
tk_add(1,0,tbl_creacol, "ccre");           
tk_add(2,0,tbl_create,  "crea");          
tk_add(3,0,tbl_crview,  "crev");          
tk_add(4,0,tbl_merge,   "merg");           
tk_add(5,0,tbl_namecol, "name");           
tk_add(6,0,tbl_project, "proj");           
tk_add(7,0,tbl_read,    "read");           
tk_add(8,0,tbl_setref,  "set ");           
tk_add(9,0,tbl_show,    "show");           
tk_add(10,0,tbl_sort,   "sort");           
tk_add(11,0,tbl_read,   "prin");           
tk_add(12,0,tbl_write,  "writ");           
tk_add(13,0,tbl_delcol, "dele");           
tk_add(14,0,tbl_copytk, "tkcp");           
tk_add(15,0,tbl_copykt, "ktcp");           
tk_add(18,0,tbl_comp,   "comp");
tk_add(19,0,tbl_select, "sele"); 
tk_add(20,0,tbl_join,   "join");
tk_add(21,0,tbl_addnewrow,"add ");
tk_add(22,0,tbl_deleterow,"delr");
tk_add(23,0,tbl_copyit, "coit");
tk_add(24,0,tbl_copyti, "coti");

                                          /* execute command */

status = tk_cexec(command);  

if (status < 0) SCTPUT("Warning on status return");
  
SCSEPI();    
return 0;			/* just to keep gcc -Wall quiet... */
}
