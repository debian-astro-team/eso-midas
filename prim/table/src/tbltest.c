/* @(#)tbltest.c	19.1 (ES0-DMD) 02/25/03 14:11:56 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbltest.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching, F. Ochsenbein
.CATEGORY    Test of basic Table Routines
.COMMENTS    
             
             create/table, show/table, read/table, print/table,
             delete/column, create/column, merge/table, name/column,
             sort/table, project/table, union/table
    
.PURPOSE     Main program activating the different routines.

.VERSION     1.0 25-Mar-1989 JDP  Implementation      
.VERSION     1.1 16-Jan-1990 JDP  Include WRITE/TABLE 
.VERSION     1.2 16-Jan-1990 JDP  Include COPY/KT, COPY/TK     
------------------------------------------------------------*/

#include <tbldef.h>
#include <midas_def.h>

static long one  = 1;
static long zero = 0;
static char *dunit;		/* Not yet used	*/
static long dnull = 0;

	/* Mnemonics for some operations */
	
#define OnError_Continue    {	long flags[3];	\
				SCECNT("GET", flags, flags+1, flags+2);\
				SCECNT("PUT", &one,   &zero , &zero);
#define OnError_Reset 				\
				SCECNT("PUT", flags, flags+1, flags+2);\
			    }

typedef int	(*FUNCTION)();

/*=======================================================================*/
main()
{
	static  char	aChar[80];
	static  long	aLong[10];
	static  float	aFloat[10];
	static  double	aDouble[10];
	int 	storage, i, j, k;
	long 	ic, tid, null, nullc, eval;
	FUNCTION f;
	char	*table_name;
	
	int	TCEWRC(), TCEWRI(), TCEWRR(), TCEWRD();
	int	TCERDC(), TCERDI(), TCERDR(), TCERDD();
	
	static  FUNCTION  wf[] = {TCEWRC, TCEWRI, TCEWRR, TCEWRD};
	static  FUNCTION  rf[] = {TCERDC, TCERDI, TCERDR, TCERDD};
	
	static  char	*av[] = {aChar, (char *)aLong, 
		(char *)aFloat, (char *)aDouble};
	
                                          /* get into MIDAS */
  SCSPRO("TBLtest");
  
  SCTPUT ("==== Basic Test of TC interfaces ===");
  
  OnError_Continue;

  SCTPUT ("==== Record-organized Table ====");

  for (storage = 1; storage >= 0; storage--) {
	table_name = (storage ? "record" : "transp");
	if (TCTINI (table_name, storage, F_O_MODE, 3,3, &tid))
		SCTPUT ("**** Error TCTINI");
	TCTCLO(tid);
	
	if (TCTOPN (table_name, F_IO_MODE, &tid))
		SCTPUT ("**** Error TCTOPN");
	
	if (TCCINI (tid, D_I1_FORMAT, 1, "I", "int1", "i1", &ic))
		SCTPUT ("**** Error TCCINI (i1)");

	if (TCCINI (tid, D_I2_FORMAT, 1, "I", "int2(2)", "i2", &ic))
		SCTPUT ("**** Error TCCINI (i2)");

	if (TCCINI (tid, D_I4_FORMAT, 1, "t", "int4(2)", "time", &ic))
		SCTPUT ("**** Error TCCINI (i4)");
	
	if (TCCINI (tid, D_L4_FORMAT, 1, "X", "Hexa", "Hexa", &ic))
		SCTPUT ("**** Error TCCINI (a4)");
	
	if (TCCINI (tid, D_R8_FORMAT, 1, "f", "Double", "double", &ic))
		SCTPUT ("**** Error TCCINI (r8)");
	
	if (TCCINI (tid, D_C_FORMAT, 13, "A", "Char", "comment", &ic))
		SCTPUT ("**** Error TCCINI (a*13)");
	
	if (TCCINI (tid, D_R4_FORMAT, 1, "z", "Float", "Float", &ic))
		SCTPUT ("**** Error TCCINI (R*4)");
	
	if (TCCINI (tid, D_R8_FORMAT, 1, "t", "JD", "JD", &ic))
		SCTPUT ("**** Error TCCINI (r8)");
	
	TCEWRC (tid, 1, 3, "");		/* Time (now) */

					/* Write some data */
	for (j=1; j <= 8; j++) {
		printf("====Col #%d\n", j);
		for (i = 1; i <= 100; i++) {
			k = i&3;
			eval = 1000*i + j;
			if (j == 1)	eval = i;
			if (j == 8)	eval += 2440000;
			aLong[0]   = eval;
			aDouble[0] = aLong[0];
			aFloat[0]  = aDouble[0];
			sprintf(aChar, "%d", aLong[0]);
			f = wf[k];
			if ((*f)(tid, i, j, av[k]))
				printf("**** Error i=%d, j=%d\n", i, j);
		}
	}
	TCTCLO(tid);
	puts("========================================================");

	
					/* Re-Read some data */
	if (TCTOPN (table_name, F_I_MODE, &tid))
		SCTPUT ("**** Error TCTOPN");

	for (i = 1; i <= 20; i++) {
		for (j=1; j <= 8; j++) {
			printf("Row %d, col %d: ", i, j);
			eval = 1000*i + j;
			if (j == 1)	eval = i;
			if (j == 8)	eval += 2440000;
			if (TCERDC (tid, i, j, aChar, &nullc))
				printf("**** Error TCERDC");
			if (nullc)	printf(" =NULLC= ");
			else		printf(" `%s'", aChar);

			if (TCERDI (tid, i, j, aLong, &null))
				printf("**** Error TCERDI");
			if (null)	printf(" =NULLI= ");
			else	if (aLong[0] != eval)	printf("**** Bad I");

			if (TCERDR (tid, i, j, aFloat, &null))
				printf("**** Error TCERDR");
			if (null)	printf(" =NULLR= ");
			else	if (aFloat[0] != eval)	printf("**** Bad R");

			if (TCERDD (tid, i, j, aDouble, &null))
				printf("**** Error TCERDD");
			if (null)	printf(" =NULLD= ");
			else	if (aDouble[0] != eval)	printf("**** Bad D");
		
			puts("");

		}
	}

	SCTPUT ("==== Transposed-organized Table ====");
  }
                                          /* get command name */
  OnError_Reset;

  
  SCSEPI();    
  ospexit(0);
}
