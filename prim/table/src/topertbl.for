C @(#)topertbl.for	19.1 (ES0-DMD) 02/25/03 14:11:57
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.2  ESO-FORTRAN Conversion, AA  14:17 - 19 NOV 1989
C  1.3   900404 KB
C 
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C
C.IDENTIFICATION:
C         PROGRAM TOPERTBL
C
C.PURPOSE
C
C Statistics and mathematical operations on tables. 
C Implements the commands:
C AVERAGE/TABLE			removed 900404  KB
C COMPUTE/REGRESSION
C COMPUTE/HISTOGRAM
C CONVERT/TABLE
C READ/HISTOGRAM
C REGRESS/LINE
C REGRESS/POLY
C REGRESS/TABLE
C SAVE/REGRESSION
C STATISTICS/TABLE, PLOT/HIST, OVER/HIST
C
C.KEYWORDS
C
C ARITHMETIC OPERATS, TABLES.
C
C.ALGORITHM
C
C USE TABLE INTERFACE ROUTINES
C
C-----------------------------------------------------------
C
      PROGRAM TOPER
      IMPLICIT NONE
      INTEGER     NCOM
      PARAMETER   (NCOM=14)
      INTEGER     I, KUN, KNUL, STAT
      CHARACTER*4 COMMAND,WS
      CHARACTER*4 COMTAB(NCOM)
C
      DATA COMTAB/'AVER','COMP','HIST','CONV',
     .            'CSPL','CMAP','LINE','POLY',
     .            'REGR','SAVE','STAT','READ',
     .            'PLOT','OVER'/
C
C ... GET INTO MIDAS
C
      CALL STSPRO('TOPERTBL')
C
C ... GET COMMAND FORM ENVIRONMENT
C
      CALL STKRDC('MID$CMND',1,1,4,I,COMMAND,KUN,KNUL,STAT)
      CALL FORUPC(COMMAND,WS)      
      IF (WS.EQ.'PRIN') WS = 'READ'
C
C ... SELECT THE ACTION
C
      DO 10 I = 1, NCOM
         IF (WS.EQ.COMTAB(I)) GOTO 20
   10 CONTINUE
      CALL STTPUT('Warning: command not found',STAT)
      GOTO 3000
C
C ... EXECUTE ACTION
C
   20 CONTINUE
      GOTO (100,200,300,400,500,600,700,800,900,1000,1100,1200,
     .      1100,1100),I
      CALL STTPUT('Warning: command not found',STAT)
      GOTO 3000
  100 CALL TDAVER
      GOTO 3000
  200 CALL TDCMPR      ! COMPUTE/REGRESSION
      GOTO 3000 
  300 CALL TDCMPH      ! COMPUTE/HISTOGRAM
      GOTO 3000
  400 CALL TDCONV      ! CONVERT/TABLE (spline 1)
      GOTO 3000
  500 CALL TDCSPL      ! CONVERT/TABLE (spline 2)
      GOTO 3000
  600 CALL TDCMAP      ! CONVERT/TABLE (map)
      GOTO 3000
  700 CALL TDREGL      ! REGRESSION/LINEAR
      GOTO 3000
  800 CALL TDREGP      ! REGRESSION/POLYNOMIAL
      GOTO 3000
  900 CALL TDREGT      ! REGRESSION/TABLE
      GOTO 3000
 1000 CALL TDSAVR      ! SAVE/REGRESSION
      GOTO 3000
 1100 CALL TDSTAT      ! STATISTICS/TABLE
      GOTO 3000
 1200 CALL TDSTAT      ! FIRST COMPUTE STATISTICS, THEN
      CALL TDRHIS      ! READ/HISTOGRAM
      GOTO 3000
C
C ... EXIT FROM MIDAS
C
 3000 CALL STSEPI
      END
C
