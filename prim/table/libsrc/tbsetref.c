/* @(#)tbsetref.c	19.1 (ESO-DMD) 02/25/03 14:11:15 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbsetref.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities
.RETURNS
 status
.PURPOSE    Set reference column.

Input keywords:
[p1] table name.
[p2] column to be used as reference.


.VERSION     1.0 25-Mar-1989   Definition     J.D. Ponz

 020807		last modif

------------------------------------------------------------*/
#include <midas_def.h>
#include <tbldef.h>

#define PARLEN 80

tbl_setref()

{ 
char table[PARLEN], column[PARLEN];
int  tid, status, ic;
                                                /* read parameters */

  status = tbl_getpar("P1",PARLEN,table);
  status = tbl_getpar("P2",PARLEN,column);

  tid = -1;
  status =  TCTOPN(table,F_U_MODE,&tid);
  if (status != ERR_NORMAL) {SCTPUT("Error opening the table");
                             return (status);
                            }

  TCCSER(tid,column,&ic);
  if (ic == -1) {status = ERR_TBLCOL; 
                 SCTPUT("Column not found");
                 goto error;}

  TCKPUT(tid,ic);

error:
 TCTCLO(tid);

 return (status);
}       
