/* @(#)tbgetpar.c	19.1 (ES0-DMD) 02/25/03 14:11:13 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbgetpar.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities
.COMMENTS
	Interfaces to parmeter access.

.VERSION 1.0 	25-Mar-1989   Definition     J.D. Ponz
.VERSION 1.1 	04-Jul-1990   Added tbl_getarg, tbl_argc
.VERSION 1.1 	09-Oct-1990:  arg#2 of tbl_getpar is LENGTH, not SIZE.
------------------------------------------------------------*/

#include <midas_def.h>

/*=================================================================*/
tbl_getpar(par,len,value)
/*++++++++++++++++++
.PURPOSE Read named parameter
.RETURNS Status
------------------*/
    char *par;		/* IN: Name of parameter to read */
    int  len;  		/* IN: maximum length of the parameter */
    char value[]; 	/* OUT: parameter value */
{ 
  char *kunit;
  int  status, iav, knul;

						/* read parameter */
  status = SCKRDC(par,1,1,len,&iav,value,&kunit,&knul);

  if (status != ERR_NORMAL) {
	SCTPUT(" Error reading parameter ");
	return (status);
  }
  value[iav] = '\0';
  while ((iav > 0) && (value[--iav] == ' '))	value[iav] = '\0';

  return (status);
}

tbl_getarg(argno, len, value)
/*++++++++++++++++++
.PURPOSE Read specified argument, i.e. parameter Pn
.RETURNS Status
------------------*/
    int	argno;		/* IN: Number of Argument to read */
    int  len;  		/* IN: maximum size of the parameter */
    char value[]; 	/* OUT: parameter value */
{ 
  
  char field[16];

  sprintf(field, "P%d", argno);
  return (tbl_getpar(field, len-1, value));
}

int tbl_argc()
/*++++++++++++++++++
.PURPOSE Read how many arguments exist.
.RETURNS Number of arguments
------------------*/
{ 
  	int 	nar,npar,kunit,knul;
  
  SCKRDI("PCOUNT",1,1,&nar,&npar,&kunit,&knul);
  return (npar);
}
