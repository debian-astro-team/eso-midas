C @(#)tdlist.for	19.1 (ES0-DMD) 02/25/03 14:11:18
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:40 - 11 DEC 1987
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C  900219  KB, throw out SX calls
C
C
C.IDENTIFICATION        TDLIST.FOR
C.KEYWORDS           TABLE, APPLICATIONS
C.ENVIRONMENT  MIDAS
C.PURPOSE
C  LIST TABLE WITH EXTERNAL FORMAT FILE
C
C------------------------------------------------------------------
      SUBROUTINE TDLIST(TABLE,FILE,NCHAN,FFORMT,NCOL,ICOL,IRANGE,
     +                  STATUS)
      IMPLICIT NONE
      INTEGER TABLE         !   IN : table ident
      CHARACTER*(*) FILE    !   IN : output file
      INTEGER NCHAN         !   IN : output channel
      CHARACTER*(*) FFORMT  !   IN : format file
      INTEGER NCOL          !   IN : number of columns
      INTEGER ICOL(NCOL)    !   IN : column indices
      INTEGER IRANGE(2)     !   IN : range of rows
      INTEGER STATUS        !  OUT : STATUS
C
      INTEGER LIMIT
      PARAMETER (LIMIT=256)
      INTEGER        ISTAT,ERNORM,I,J,J1
      LOGICAL        ITERM,SELECT,NULL(LIMIT)
      CHARACTER*4096 LINE
      CHARACTER*80   ERRMSG
C
       INCLUDE 'MID_INCLUDE:TABLES.INC/NOLIST'
       INCLUDE 'MID_INCLUDE:TABLEF.INC/NOLIST'
       INCLUDE 'MID_INCLUDE:TABLED.INC/NOLIST'
C
      DATA ERRMSG/' *** format conversion error ***'/
C
C ... READ FORMAT
C
      ERNORM = 0
      CALL TDFFRM(FFORMT,STATUS)
      IF (STATUS.NE.ERNORM) THEN
          CALL STTPUT('Error in format definition file ...',ISTAT)
          RETURN
      END IF
C
C ... MODIFY FORMAT IF NEEDED
C
      CALL TDFMOD(TABLE,NCOL,ICOL,STATUS)
C
C ... LIST TABLE
C
      ITERM  = FILE(1:4) .EQ. 'TTY:'
      DO 10 I = IRANGE(1),IRANGE(2)
          CALL TBSGET(TABLE,I,SELECT,STATUS)
          IF (SELECT) THEN
              CALL TBRRDC(TABLE,I,NCOL,ICOL,LINE,NULL,STATUS)
              IF (STATUS.NE.ERNORM) CALL STTPUT(ERRMSG,STATUS)
              DO 15 J = 1, NCOL
                 J1 = FMFIRS(J)
                 IF (NULL(J)) LINE(J1:J1) = '*'
15            CONTINUE
              IF (ITERM) THEN
                  CALL STTPUT(LINE(1:FMLLEN),STATUS)
              ELSE
                  WRITE (NCHAN,9000) LINE(1:FMLLEN)
              END IF
          END IF
   10 CONTINUE
      RETURN
 9000 FORMAT (A)
      END
