/* @(#)tbappcol.c	19.1 (ES0-DMD) 02/25/03 14:11:12 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbappcol.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities
.RETURNS
	status
.PURPOSE    

Append Column from one table to another.
The current version is coded using the standard table routines.
The code has to be optimized to a turbo version avoiding some of the checking.


.VERSION     1.0 25-Mar-1989   Definition     J.D. Ponz
------------------------------------------------------------*/
#include <midas_def.h>
#include <tbldef.h>
#include <proto_tbl.h>

#define PARLEN 80

tbl_appcol(in,icol,out,ocol,type, nrow, nout)

int in		/* IN : input table id */;
int icol	/* IN : input column */;
int out		/* IN : output table id */;
int ocol	/* IN : output column */;
int type	/* IN : column type */;
int nrow	/* IN : total number of rows in input table */;
int *nout	/* OUT: actual numberof rows copied */;

{ int i, j, null, ival, status,sel;
  float rval;
  double dval;
  char   cval[256];

  j = *nout; status = 0;
  switch (type) {
  case D_I1_FORMAT:
  case D_I2_FORMAT:
  case D_I4_FORMAT: for (i=1; i<=nrow; i++)
                       {TCERDI(in,i,icol,&ival,&null);
                        TCSGET(in,i,&sel);
                        if (sel) {j++;
                                  if (null) TCEDEL(out,j,ocol);
                                  else      TCEWRI(out,j,ocol,&ival);
                                 }
                       }
                    break;
  case D_R4_FORMAT: for (i=1; i<=nrow; i++)
                       {TCERDR(in,i,icol,&rval,&null);
                        TCSGET(in,i,&sel);
                        if (sel) {j++;
                                  if (null) TCEDEL(out,j,ocol);
                                  else      TCEWRR(out,j,ocol,&rval);
                                 }
                       }
                    break;
  case D_R8_FORMAT: for (i=1; i<=nrow; i++)
                       {TCERDD(in,i,icol,&dval,&null);
                        TCSGET(in,i,&sel);
                        if (sel) {j++;
                                  if (null) TCEDEL(out,j,ocol);
                                  else      TCEWRD(out,j,ocol,&dval);
                                 }
                       }
                    break;
  case D_C_FORMAT:  for (i=1; i<=nrow; i++)
                       {TCERDC(in,i,icol,cval,&null);
                        TCSGET(in,i,&sel);
                        if (sel) {j++;
                                  if (null) TCEDEL(out,j,ocol);
                                  else      TCEWRC(out,j,ocol,cval);
                                 }
                       }
                    break;
 }                                    
 *nout = j;                                  
  
 return (status);
}       
