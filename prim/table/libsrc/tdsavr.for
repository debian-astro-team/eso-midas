C @(#)tdsavr.for	19.1 (ESO-DMD) 02/25/03 14:11:20
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  16:04 - 19 NOV 1987
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C
C.IDENTIFICATION:
C
C  program TDSAVR
C
C.KEYWORDS:
C  REGRESSION, FIT
C
C.PURPOSE:
C
C  SAVE DISPERSION COEFFICIENTS AS TABLE DESCRIPTORS
C
C  SAVE/REGRESSION TABLE NAME
C
C.ALGORITHM:
C
C  OUT PARAMETERS OF THE REGRESSION ARE STORED IN THE
C  DESCRIPTORS :
C  XXXXC/C/1/20 - CHARACTER INFOR
C  XXXXI/I/1/20 - REFERENCE TO VARIABLES IN THE REGRESSION
C  XXXXD/D/1/20 - COEFFICIENTS
C  XXXXR/R/1/20 - ASSOCIATED REAL*4 PARAMETERS
C  WHERE XXXX IS THE NAME GIVEN TO THE REGRESSION COEFFS IN THE
C  SAVE/REGRESSION COMMAND
C
C.VERSIOn
C 020807	last modif
C 
C-------------------------------------------------------------------
C
      SUBROUTINE TDSAVR
      IMPLICIT   NONE
C
      INTEGER   NPAR,STAT,INDX,TID,NAV,STATUS
      INTEGER   NOELM,BYTELM
      INTEGER   KUNIT
      INTEGER   OUTI(20)
      INTEGER   INDEX, INDKEY, KUN, KNUL
C
      REAL             OUTR(20)
C
      DOUBLE PRECISION OUTD(200)
C
      CHARACTER*80     TABLE*80
      CHARACTER*20     NAME, DESNAM, KEYNAM, INPNAM
      CHARACTER*20     OUTC
      CHARACTER*16     MSG,TYPE
C
C
      INCLUDE   'MID_INCLUDE:TABLES.INC'
      INCLUDE   'MID_INCLUDE:TABLED.INC'
      DATA MSG/'ERR:TREGRSAV'/
C
C ... get parameters
C
      CALL TDPGET(3,NPAR,STAT)
      IF (STAT.NE.0) GO TO 10
      TABLE  = TPARBF(1)
      NAME   = ' '
      NAME   = TPARBF(2)
      INDX   = INDEX(NAME,' ') - 1
      KEYNAM = ' '
      KEYNAM = TPARBF(3)
      INDKEY = INDEX(KEYNAM,' ') - 1
C
C ... access to the table
C
      TID = -1
      CALL TBTOPN(TABLE,F_D_MODE,TID,STAT)
      IF (STAT.NE.0) GO TO 10
C
C ... write descriptors
C
      INPNAM = KEYNAM(1:INDKEY)//'C'
      CALL STKRDC(INPNAM,1,1,20,NAV,OUTC,KUN,KNUL,STATUS)
      DESNAM = NAME(1:INDX)//'C '
      CALL STDWRC(TID,DESNAM,1,OUTC,1,20,KUNIT,STATUS)

      INPNAM = KEYNAM(1:INDKEY)//'I'
      CALL STKRDI(INPNAM,1,20,NAV,OUTI,KUN,KNUL,STATUS)
      DESNAM = NAME(1:INDX)//'I '
      CALL STDWRI(TID,DESNAM,OUTI,1,20,KUNIT,STATUS)

      INPNAM = KEYNAM(1:INDKEY)//'D'
      CALL STKFND(INPNAM,TYPE,NOELM,BYTELM,STAT)
      CALL STKRDD(INPNAM,1,NOELM,NAV,OUTD,KUN,KNUL,STATUS)
      DESNAM = NAME(1:INDX)//'D '
      CALL STDWRD(TID,DESNAM,OUTD,1,NOELM,KUNIT,STATUS)


      INPNAM = KEYNAM(1:INDKEY)//'R'
      CALL STKRDR(INPNAM,1,20,NAV,OUTR,KUN,KNUL,STATUS)
      DESNAM = NAME(1:INDX)//'R '
      CALL STDWRR(TID,DESNAM,OUTR,1,20,KUNIT,STATUS)
C
C ... end
C
      CALL DSCUPT(TID,TID,' ',STAT)
      CALL TBTCLO(TID,STAT)
   10 IF (STAT.NE.0) THEN
          WRITE (MSG(13:16),9000) STAT
          CALL TDERRR(STAT,MSG,STATUS)
      END IF
      RETURN
 9000 FORMAT (I4)
      END
