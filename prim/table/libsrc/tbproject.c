/*===========================================================================
  Copyright (C) 1989-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbproject.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities

.COMMENTS       This module implements the following Midas commands:
 PROJECT/TABLE intable outable column_specs

.VERSION 1.0 	25-Mar-1989   Definition     J.D. Ponz
.VERSION 3.0 	05-Jul-1990   New version with column arrays   F.O.
.VERSION 3.1    21-JAn-1991    MP

 090708		last modif
------------------------------------------------------------*/

#include <tbldef.h>
#include <midas_def.h>
#include <proto_tbl.h>

#include <stdlib.h>
#include <stdio.h>


#define PARLEN 80

extern int tbl_getarg(), tbl_copycol();




int tbl_project()
/*++++++++++++++++++
.PURPOSE PROJECT/TABLE source_table dest_table :col_selection 
.RETURNS Status
------------------*/
{
char param[PARLEN], outable[PARLEN], label[1+TBL_LABLEN];
char form[10], unit[1+TBL_UNILEN];

int  status, i, ic;
int  tid, tout, ncol, nrow, nout, dummy;
int  phform, nr, nbtot, bytes, items, nc, type, oc;
int  *icol, *flag;

void *memi;


					

tbl_getarg(1, PARLEN, param);			/* get input table */

tid = -1;
(void) TCTOPN(param, F_I_MODE, &tid);
(void) TCIGET(tid, &ncol, &nrow, &dummy, &dummy, &dummy);

tbl_getarg(2, PARLEN, outable);			/* get output table */
tbl_getarg(3, PARLEN, param);			/* Get columns	*/


/* should not project more columns than in input table - but who knows, 
   so give some additional (50 %) leeway for allocation of space ...
 */

oc = (ncol + (ncol/2)) * sizeof(int);
memi = (void *) malloc((size_t) oc);
if (memi == (void *) 0)
   {
   status = ERR_MEMOUT;
   goto error;
   }
icol = (int *) memi;			/* columns */

memi = (void *) malloc((size_t) oc);
if (memi == (void *) 0)
   {
   status = ERR_MEMOUT;
   goto error;
   }
flag = (int *) memi;			/* flags */

TCCSEL(tid, param, oc, icol, flag, &nc);	/* get the columns involved */
if (nc < 0) 
   {
   status = ERR_TBLCOL; 
   SCTPUT("****Column(s) not found");
   goto error;
   }


TCDGET(tid, &phform);
TCSCNT(tid, &nr);

						/* count required space */
nbtot = 0; 
for (i=0; i<nc; i++) 
   {
   TCBGET (tid, icol[i], &type, &items, &bytes);
   nbtot += bytes;
   }

nbtot = (nbtot+3)/4;				/* Size for TCTINI 	*/
if (nbtot & 1)	nbtot++;	
nr = 8*((nr+7)/8);

status = TCTINI(outable, phform, (nc<<16)|(F_O_MODE|F_ALL_FORCE),
         	nbtot, nr, &tout);
if (status != ERR_NORMAL) 
   {
   SCTPUT("Error creating output table");
   return (status);
   }

					/* create the columns and copy them*/
for (i = 0; (status == ERR_NORMAL) && (i< nc); i++) {
	ic = icol[i];
	(void) TCFGET (tid, ic, form,  &dummy, &type);
	(void) TCLGET (tid, ic, label);
	(void) TCUGET (tid, ic, unit);
	(void) TCBGET (tid, ic, &type, &items, &bytes);
        if (type == D_C_FORMAT)
             status = TCCINI(tout, type, bytes, form, unit, label, &oc);
	else status = TCCINI(tout, type, items, form, unit, label, &oc);
        if (type == D_C_FORMAT && items !=1 )
            (void) TCAPUT(tout,oc,items);
	status = tbl_copycol(tid,ic,tout,oc,type,nrow,&nout);
  }     


if (status == ERR_NORMAL) 
   {
   CGN_DSCUPD(tout,tout," ");
   (void) sprintf(param,"%d columns copied ...",nc);
   SCTPUT(param);
   }

(void) TCTCLO(tout);

error:
(void) TCTCLO(tid);

return (status);
}       
