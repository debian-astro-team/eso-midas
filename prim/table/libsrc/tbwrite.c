/*===========================================================================
  Copyright (C) 1990-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbwrite.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities

.COMMENTS       This module implements the following Midas commands:
\begin{TeX}
\begin{enumerate}
\item {\tt WRITE/TABLE} table column row\_selection value

	Write element(s) into a table. 
	This command is equivalent to the MCL statement
	table,column,row = value

\end{enumerate}
\end{TeX}


.VERSION  1.0 	25-Mar-1990   Definition     J.D. Ponz
.VERSION  3.0 	05-Jul-1990   New version with column arrays   F.O.

 090708		last modif
------------------------------------------------------------*/

#include <tbldef.h>
#include <midas_def.h>
#include <str.h>
#include <proto_tbl.h>

#define PARLEN 	80
#define COLS	256		/* Max number of Columns */
#define RANGES	256		/* Max number of Ranges	 */
#define MAXCHAR 4096

extern int tbl_getarg(), tbl_getrows();



int  tbl_write()
/*++++++++++++++++++
.PURPOSE WRITE/TABLE table_name column_choice element_choice value
.RETURNS Status
------------------*/
{
  char  table[PARLEN], p2[PARLEN], p3[PARLEN], value[MAXCHAR], *column, *row;
  char  *p;
  int  icol[COLS], fcol[COLS], lrange[RANGES], urange[RANGES], ncol, nr;
  int  tid, isnull;
  int   i, j, ir, ic, status;
  double atof();


						/* read parameter */
  tbl_getarg(1, PARLEN, table);
						/* open tablefile */
  tid = -1;
  status = TCTOPN(table, F_IO_MODE, &tid);
  if (status != ERR_NORMAL) {SCTPUT("Error opening the table");
                             return (status);
                            }

  tbl_getarg(2, PARLEN, p2);
  tbl_getarg(3, PARLEN, p3);
  if (p2[0] == '@')	column = p3, row = p2;
  else			column = p2, row = p3;

							/* Extract Cols	*/
  status = TCCSEL(tid, column, COLS, icol, fcol, &ncol);
  if ((status != ERR_NORMAL) || (ncol <= 0)) {
	SCTPUT("Column(s) not found");
	return(status);
  }

							/* Extract Rows	*/
  status = tbl_getrows(row, tid, RANGES, lrange, urange, &nr);
  if (status) return(status);

							/* read value */
  tbl_getarg(4, MAXCHAR, value);
  isnull = (stumatch(value,"null") == 4);

  p = value;
  if (value[0] == '"') {			/*       Remove Quotes 	*/
	i = strbloc(value, '"');			/* Last Quote	*/
	value[i] = '\0';
	p++;
  }

				/* a written element is selected...	*/
  for (ic = 0; (status == ERR_NORMAL) && (ic < ncol); ic++) {
  	j = icol[ic];
	for (ir = 0; (status == ERR_NORMAL) && (ir < nr); ir++) {
		for (i=lrange[ir]; (status == ERR_NORMAL) && (i<=urange[ir]); 
		     i++) {
  			if (isnull)	status = TCEDEL(tid, i, j);
  			else		status = TCEWRC(tid, i, j, p);
		}
        }
  }

  TCTCLO(tid);   
   
  return (status);
}       
