C @(#)tdrdpar.for	19.1 (ES0-DMD) 02/25/03 14:11:19
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:02 - 11 DEC 1987
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C
C.IDENTIFICATION        TDRDPAR
C.KEYWORDS           TABLE, APPLICATIONS
C.ENVIRONMENT  MIDAS
C.PURPOSE
C  READ STANDARD SYSTEM PARAMETERS IN KEYWORDS
C
C
C------------------------------------------------------------------
      SUBROUTINE TDPGET(NEPAR,NPAR,STATUS)
      IMPLICIT NONE
      INTEGER NEPAR  !  IN : no. of expected parameters
      INTEGER NPAR   !  OUT: actual no. of parameters
      INTEGER STATUS !  OUT: status
C
      INTEGER IAV,NARG,I,KNUL,KUN
      CHARACTER*8 TBARGN(8)
       INCLUDE 'MID_INCLUDE:TABLES.INC/NOLIST'
       INCLUDE 'MID_INCLUDE:TABLED.INC/NOLIST'
      DATA TBARGN/'P1 ','P2 ','P3 ','P4 ','P5 ','P6 ','P7 ','P8 '/
C
      CALL STKRDI('PCOUNT',1,1,IAV,NARG,KUN,KNUL,STATUS)
      NPAR   = NARG
      DO 10 I = 1,MIN(NEPAR,NARG)
          CALL STKRDC(TBARGN(I),1,1,64,IAV,TPARBF(I),KUN,KNUL,STATUS)
   10 CONTINUE
      IF (NARG.GT.NEPAR) STATUS = ERRPAR
      RETURN
      END
