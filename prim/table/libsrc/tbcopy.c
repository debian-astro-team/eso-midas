/*===========================================================================
  Copyright (C) 1989-2011 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbcopy.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities
.COMMENTS    Implementation of the Midas commands:

.ALGORITHM
 Copy column from input table into the output table.
 If the reference column is defined, the copy is done according to
 values in this column. 
 By default the sequence number is used as a reference.

.VERSION 1.0 	25-Mar-1989   Definition     J.D. Ponz
.VERSION 3.0 	05-Jul-1990   New version with column arrays   F.O.
.VERSION 3.1 	27-Nov-1990: COPY/TT uses ALWAYS same row if same table

 110630		last modif
------------------------------------------------------------*/

#include <tbldef.h>
#include <tblerr.h>
#include <midas_def.h>
#include <proto_tbl.h>
#include <str.h>

#define PARLEN 80

extern int tbl_argc(), tbl_getarg(), tbl_copycol(), tbl_copyref();



int tbl_copy()
/*++++++++++++++++++
.PURPOSE COPY/TT Midas Command
.RETURNS Status
------------------*/
{
  char *intable, *outable, *incol, *outcol;
  char parm[4][PARLEN];
  char form[10], unit[1+TBL_UNILEN];
  int  tidi, npar, iref, oref, bytes;
  int  nrow, status,i, ii, nout, io, type, alen, tido, dummy;

						/* get parameters */
  tidi = tido = -1;
  npar = tbl_argc();
  intable = outable = incol = outcol = (char *)0;

  if (npar > 4) 	SCTPUT("++++ Ignored argument(s):");
  while (npar > 4) {
	tbl_getarg (npar, PARLEN, parm[0]);
	SCTPUT (parm[0]);
	npar--;
  }

  for (i = 0; i < npar; i++) {
	tbl_getarg (i+1, PARLEN, parm[i]);
        if (parm[i][0] == '?') continue;
	if ((parm[i][0] == ':') || (parm[i][0] == '#')) {
		if (!incol)	incol  = parm[i];
		else		outcol = parm[i];
	}
	else	{
		if (!intable)	intable = parm[i];
		else		outable = parm[i];
	}
  }
  if (!outable)	outable = intable;
  if (!outcol)	outcol  = incol;
  
						/* open i/o table(s) */
  if (strcomp(intable, outable) == 0) {
	(void) TCTOPN(intable, F_IO_MODE, &tidi);
	tido   = tidi;
  }
  else {
	(void) TCTOPN(intable,F_I_MODE,&tidi);
      	(void) TCTOPN(outable,F_IO_MODE,&tido);
  }
  						/* search for input column */
  (void) TCCSER(tidi,incol,&ii);
  if (ii <= 0) {
  	SCTPUT("Input column not found "); 
  	status = ERR_TBLCOL; 
  	goto error;
  }
  (void) TCIGET(tidi, &dummy, &nrow, &dummy, &dummy, &dummy);
  (void) TCUGET(tidi,ii, unit);
  (void) TCFGET(tidi,ii, form,&alen,&type);
  (void) TCBGET(tidi,ii, &type, &alen, &bytes);

  						/* search for output column */
  (void) TCCSER(tido,outcol,&io);
  if (io <= 0) {
        if (type == D_C_FORMAT)
            (void) TCCINI(tido, type, bytes, form, unit, outcol, &io);
  	else 
            (void) TCCINI(tido, type, alen, form, unit, outcol, &io);

        if (type == D_C_FORMAT && alen !=1 ) (void) TCAPUT(tido,io,alen);
  }

  (void) TCKGET(tidi, &iref);
  (void) TCKGET(tido, &oref);
					/* copy by sequence or by reference */

  if (iref == 0 && oref == 0) 
	status = tbl_copycol(tidi,ii,tido,io,type,nrow,&nout);
  else 	status = tbl_copyref(tidi,ii,iref,tido,io,oref,type,nrow,&nout);

error:
 if (tido != tidi)
       {
        CGN_DSCUPD(tido,tido," ");
        (void) TCTCLO(tido);
        (void) TCTCLO(tidi);
       }
 else
       {
        CGN_DSCUPD(tidi,tidi," ");
       (void) TCTCLO(tidi);
       }

 return (status);
}       
