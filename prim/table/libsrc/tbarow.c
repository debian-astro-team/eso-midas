/*===========================================================================
  Copyright (C) 1989-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbarow.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table applications: Row operations
.COMMENTS
	The soubroutines of this module are tools manipulating rows.

.VERSION 1.0 	25-Mar-1989   Definition     J.D. Ponz
.VERSION 3.0 	05-Jul-1990   New version with column arrays   F.O.
.VERSION 4.0    15-jan-1993   Add tbl_addrow, tbl_delrow M.P

 090707		last modif
------------------------------------------------------------*/

#include <tbldef.h>
#include <midas_def.h>
#include <macrogen.h>		/* General-purpose macros   */
#include <proto_tbl.h>

#include <stdio.h>
#include <stdlib.h>


#define PARLEN 80
#define RANGES 256

int  tbl_getarg(), strbloc();
char *osmmget();



int tbl_getrows(text, tid, maxr, lrange, urange, found)
/*++++++++++++++++++
.PURPOSE Interpret the line text as REFERENCE or ROW number(s) or range(s)
.RETURNS Status
------------------*/
	char	*text;	 	/* IN: Text to interpret */
        int 	tid;		/* IN : table id	*/
        int	maxr;		/* IN: Size of lower& upper bound arrays */
        int	*lrange;	/* OUT: lower bounds of found ranges	*/
	int	*urange;	/* OUT: upper bounds of found ranges	*/
	int	*found;		/* OUT: How many ranges were found */
{
	int	status;
	int	dummy, rcol, scol;
	char	*p;
	
  status = ERR_NORMAL;
  *found = 0;
  p = text;
  
  if (*p == '@')	rcol = 0;
  else			TCKGET(tid, &rcol); 	/* reference column */

  if (rcol == 0)	{			/* sequence number  */
  	status = TCRSEL(tid, p, maxr,  lrange, urange, found);
  	if ((status != ERR_NORMAL) || (*found <= 0)) {
		SCTPUT("Row(s) not found");
		return(ERR_TBLROW);
	}
  }
  else	{
	TCIGET(tid, &dummy, &dummy, &scol, &dummy, &dummy);
	if (scol != rcol) {
		SCTPUT("Reference column is not sorted");
		return(ERR_TBLKEY);
	} 
	if (*p == '"')		/* Remove Quotes */
		p++, p[strbloc(p, '"')] = '\0';
	status = TCESRC(tid, rcol, p, 1,strlen(p), 1, lrange);
	if (*lrange <= 0) {
		SCTPUT("Entry not found");
		return(ERR_TBLKEY);
	}
	*urange = *lrange;
	*found  = 1;
  }
  return (status);
}



int tbl_addnewrow()
/*++++++++++++++++++
.PURPOSE add new row(s)
.RETURNS Status
------------------*/
{
  char table[PARLEN], msg[100] ;
  int tid, irow, nrow, ntrow, dummy;	
  int status, phform;
  char *p2,*p3;


  tid = -1;
  tbl_getarg(1, PARLEN, table);
  status = TCTOPN(table,F_IO_MODE,&tid);
  if (status != ERR_NORMAL) {SCTPUT("Error opening the table");
                           return (status);
                           }
  status = TCDGET(tid,&phform);
  if (phform == F_RECORD) {SCTPUT("Function not available for RECORD Tables ");
                           return(-1);
                           } 
  status = TCSINI(tid);
  status = TCIGET(tid,&dummy,&ntrow,&dummy,&dummy,&dummy);
  p2 = osmmget(PARLEN);
  p3 = osmmget(PARLEN);
  tbl_getarg(2, PARLEN, p2);
  tbl_getarg(3, PARLEN, p3); 
  if (*p2 == '@') {
       p2++;
       irow = atoi(p2);
       nrow = atoi(p3);
       }
  else if (*p3 == '@') {
       p3++;
       irow = atoi(p3); 
       nrow = atoi(p2);
       }
  else {
       SCTPUT("Bad row specification");
       return(ERR_TBLROW);
       }
  status = TBL_ADDROW(tid,irow,nrow);
  if (status) 
       {
       sprintf(msg,"Row position outside range [1..%d]",ntrow);
       SCTPUT(msg); 
       return(status);
       }
  status = TCTCLO(tid);
  return(status);
  }



int tbl_deleterow()
/*++++++++++++++++++
.PURPOSE Delete row(s) from a table
.RETURNS Status
------------------*/
{

  char table[PARLEN],msg[100] ;
  char p2[PARLEN];
  int tid, ntrow, dummy;	
  int lrange[RANGES],urange[RANGES],nr;
  int j,status,phform;
  tbl_getarg(1, PARLEN, table);


  tid = -1;
  status = TCTOPN(table,F_IO_MODE,&tid);
  if (status != ERR_NORMAL) 
      {
      SCTPUT("Error opening the table");
      return (status);
      }

  status = TCDGET(tid,&phform);
  if (phform == F_RECORD) 
      {
      SCTPUT("Function not available for RECORD Tables ");
      return(-1);
      } 

  status = TCSINI(tid);
  status = TCIGET(tid,&dummy,&ntrow,&dummy,&dummy,&dummy);
  tbl_getarg(2,PARLEN,p2);
  status = tbl_getrows(p2,tid,RANGES,lrange,urange,&nr);
  if (status != 0) 
      return(status);
  for (j= nr; j>0; j--)
     {
     status = TBL_DELROW(tid,lrange[j-1],urange[j-1]-lrange[j-1]+1);
     
     if (status != 0) 
         {
         sprintf(msg,"Row position outside range [1..%d]",ntrow);
         SCTPUT(msg); 
         return(status);
         }
     } 
return 0;
}
