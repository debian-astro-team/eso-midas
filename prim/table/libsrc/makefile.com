$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.PRIM.TABLE.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:55 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   tdforlib.for
$ FORTRAN   tdorder.for
$ FORTRAN   tdrdcode.for
$ FORTRAN   tddspfit.for
$ FORTRAN   tdhilist.for
$ FORTRAN   tdsel.for
$ FORTRAN   tdaver.for
$ FORTRAN   tdlist2.for
$ FORTRAN   tdcmpr.for
$ FORTRAN   tdcmph.for
$ FORTRAN   tdconv.for
$ FORTRAN   tdsearch1.for
$ FORTRAN   tdmemory.for
$ FORTRAN   tdrdpar.for
$ FORTRAN   tdcmap.for
$ FORTRAN   tddummy.for
$ FORTRAN   tdhistory.for
$ FORTRAN   tdminmax.for
$ FORTRAN   tdrebin.for
$ FORTRAN   tderror.for
$ FORTRAN   tdregr.for
$ FORTRAN   tdrhist.for
$ FORTRAN   tdcspl.for
$ FORTRAN   tdregl.for
$ FORTRAN   tdspline.for
$ FORTRAN   tdregp.for
$ FORTRAN   tdmregr.for
$ FORTRAN   tdscale.for
$ FORTRAN   tdinter.for
$ FORTRAN   tdname.for
$ FORTRAN   tdregt.for
$ FORTRAN   tdsavr.for
$ FORTRAN   tdnlrb.for
$ FORTRAN   tdstat.for
$ FORTRAN   tdfreq.for
$ FORTRAN   tdtstat.for
$ FORTRAN   tdcopy.for
$ LIB/REPLACE libftab tdforlib.obj,tdorder.obj,tdrdcode.obj,tddspfit.obj,tdhilist.obj,tdsel.obj,tdaver.obj,tdlist2.obj,tdcmpr.obj,tdcmph.obj,tdconv.obj,tdsearch1.obj
$ LIB/REPLACE libftab tdmemory.obj,tdrdpar.obj,tdcmap.obj,tddummy.obj,tdhistory.obj,tdminmax.obj,tdrebin.obj,tderror.obj,tdregr.obj,tdrhist.obj,tdcspl.obj,tdregl.obj
$ LIB/REPLACE libftab tdspline.obj,tdregp.obj,tdmregr.obj,tdscale.obj,tdinter.obj,tdname.obj,tdregt.obj,tdsavr.obj,tdnlrb.obj,tdstat.obj,tdfreq.obj,tdtstat.obj,tdcopy.obj
$ LIB/REPLACE libctab tbaload.obj,tbadec.obj,tbapar.obj,tbacol.obj,tbarow.obj,tbhist.obj,selechar.obj,tbjoin.obj
$ LIB/REPLACE libctab tbcrea.obj,tbcolumn.obj,tbcomsel.obj,tbread.obj,tbshow.obj,tbsort.obj,tbmerge.obj,tbrowname.obj,tbproject.obj,tbcopy.obj,tbwrite.obj,tbcopyk.obj,tbcopima.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
