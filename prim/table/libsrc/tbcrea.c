/*===========================================================================
  Copyright (C) 1989-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbcrea.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities

.COMMENTS
Create table file either as a NULL table or containing the information
in an ASCII file used to populate the table.

Columns in the table are created according to the description in a FORMAT file.
By default all the columns are floating point numbers in single precision.

This module implements the following Midas commands:
CREATE/TABLE with parameters
	p1 table name.
	p2 Number of Columns
	p3 Number of Lines
	p4 ASCII data file or NULL.
	p5 optional FORMAT file or NULL.
	p6 storage mode defined as TRANSPOSED (default) or RECORD.
CREATE/VIEW



.VERSION 1.0 	25-Mar-1989   Definition     J.D. Ponz
.VERSION 3.0 	05-Jul-1990   New version with column arrays   F.O.
		19-Feb-1991   default name of FMT file to datafile M.P 
 090708		last modif
 
------------------------------------------------------------*/

#include <tbldef.h>
#include <midas_def.h>

#include <str.h>			/* String utilities	*/
#include <proto_tbl.h>
#include <stdlib.h>

#define PARLEN 80


int  tbl_getarg(), tbl_load(), tbl_loadl(), tbl_iload();
char *osfsupply();




int tbl_crview()
/*++++++++++++++++++
.PURPOSE CREATE/VIEW table_name reference_table
.RETURNS Status
------------------*/

{
char viewfile[PARLEN], table[PARLEN];

int status;

                                                /* read parameters */
  
  tbl_getarg (1, PARLEN, viewfile);
  tbl_getarg (2, PARLEN, table);
						/* create table file */
  status = TCTCRV(viewfile, table, 0);
  return (status);
}
/*

*/

int tbl_create()
/*++++++++++++++++++
.PURPOSE CREATE/TABLE table_name columns rows data_file fmt_file organisation
.RETURNS Status
------------------*/
{
  char datafile[PARLEN], formatfile[PARLEN], table[PARLEN];
  char parm[PARLEN];
  int status, phform, tid, i;
  int lines, cols;

                                                /* read parameters */
  phform = F_RECORD;
  
  tbl_getarg (1, PARLEN, table);
  tbl_getarg (2, PARLEN, parm);		cols = atoi (parm);
  tbl_getarg (3, PARLEN, parm);		lines= atoi (parm);
  tbl_getarg (4, PARLEN, datafile);
  tbl_getarg (5, PARLEN, formatfile);
  tbl_getarg (6, PARLEN, parm);
  if ((parm[0] == 'T') || (parm[0] == 't')) phform = F_TRANS;
  
			/* Get Number of Lines / Cols from formatfile */
  if ( ((!cols) || (!lines)) && formatfile[0] && (studiff(formatfile, "null"))) 
  	tbl_iload (formatfile, &lines, &cols);

						/* create table file */
  if ((cols <0) || (lines < 0))
     SCETER(47,"invalid no. of rows/columns....");

  (void) TCTINI(table, phform, F_O_MODE, cols, lines, &tid);

						/* NULL table */
  if (stumatch(datafile,"null") == 4) {
        CGN_DSCUPD(tid,tid," ");
  	(void) TCTCLO(tid); 
  	return (ERR_NORMAL);
  }

  if (stumatch(formatfile,"null") == 4)  
      {
       i = strloc(datafile,'.');
       if (datafile[i]) 
          (void) strncpy(formatfile,datafile,i) ; 
       else
          (void) strcpy(formatfile,datafile);
       if (osfsize(osfsupply(formatfile,".fmt")) >=0 )
           status = tbl_load(tid,datafile,formatfile);
       else
           status = tbl_loadl(tid,datafile, cols);  /* load table no format */
       }
  else
      status = tbl_load(tid,datafile,formatfile);   /* load table with format */

     
  if (status == ERR_NORMAL) CGN_DSCUPD(tid,tid," ");

  (void) TCTCLO(tid);

  return (status);
}       
 
