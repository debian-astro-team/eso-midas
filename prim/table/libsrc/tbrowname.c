
/*
.VERSION
051012		last modif
*/

#include <tbldef.h>
#include <midas_def.h>
#include <macrogen.h>
#include <proto_tbl.h>

#include <stdio.h>


int getRowbyFrame(table,frame)
char table[60],frame[60];
{
int tid,row,icol;

tid = -1;
TCTOPN(table,F_I_MODE,&tid);
TCLSER(tid,"FILENAME",&icol);
(void) printf("frame:%s\n",frame);
TCESRC(tid,icol,frame,1,strlen(frame),1,&row);
(void) printf("row: %d\n",row);
TCTCLO(tid);
return(row);
}
