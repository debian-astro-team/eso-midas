/*



OJO:	this file is not used anymore!!

	tbl_decfmt() is now in tbadec.c


KB 100817




*/

/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbdecfmt.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities

.COMMENTS
\begin{TeX}

Decodes line in format file.
The line has the following information:

DEFINE/FIELD pos1 pos2 type [format] label [unit]

where type defines the data type as :

R[eal], R[eal]*4   - floating point single precision.
D[ouble], R[eal]*8 - floating point double precision.
I[nteger], I[nteger]*4 - long integer (4 bytes).
S[hort],   I[nteger]*2 - short integer (2 bytes).
B[yte],    I[nteger]*1 - byte integer (1 byte).
C[haracter*n]          - character string.

{\bf Note}: if the pos1 / pos2 are not defined, it's assumed that
	a {\em tab} separates the columns.
\end{TeX}


.VERSION 1.0 	25-Mar-1989   Definition     J.D. Ponz
.VERSION 1.1 	18-May-1990   default=length of field for character  M Peron
.VERSION 3.0 	05-Jul-1990   New version with column arrays   F.O.
------------------------------------------------------------*/

#include <tbldef.h>
#include <midas_def.h>
#include <str.h>		/* String Utilities	*/


tbl_decfmt(line,i1,i2,type,noelem,form,unit,label)
/*++++++++++++++++++
.PURPOSE Decode one line
.RETURNS Status
.REMARKS If type==0 on return, we've an emtpy line (comment).
	The input line IS MODIFIED !!!
------------------*/
 char *line;	/* IN: Line to scan	*/
 int  *i1;	/* OUT: position (1) 	*/
 int  *i2;	/* OUT: position (2)	*/
 int  *type;	/* OUT: Element type in midas_def format	*/
 int  *noelem;	/* OUT: Size of array	*/
 char *form;	/* OUT: Format		*/
 char *unit;	/* OUT: Unit		*/
 char *label;	/* OUT: Label (column name)	*/
{
 int status, l1, l2;
 char *pf, *pl, *pu, *dtype;
 char *p;

  *i1   = 0;
  *i2   = 0;
  *type = 0;
  *noelem = 1;
  *form = *unit = '\0';
  
  l1 = l2 = 0;

  line[strloc(line, '!')] = '\0';		/* Strip comments	*/

						/* DEFINE/KEY		*/
  p = line + strskip (line, ' ');
  if (*p != 'd' && *p != 'D') return(ERR_NORMAL);
  p += strloc (p, ' ');

						/* Get pos2  pos2	*/
  p += strskip(p, ' '); 
  if (isdigit (*p)) {
  	l1 = atoi(p); 	p += strloc(p, ' ');	p += strskip(p, ' '); 
  	l2 = atoi(p); 	p += strloc(p, ' ');
  	*i1 = l1; *i2 = l2; 
  	l2 = l2 - l1 + 1;
  }

  p += strskip(p, ' '); dtype = p; p += strloc(p, ' '); if (*p) *(p++) = '\0';
  p += strskip(p, ' '); pf = p;    p += strloc(p, ' '); if (*p) *(p++) = '\0';
  p += strskip(p, ' '); pl = p;    p += strloc(p, ' '); if (*p) *(p++) = '\0';
  p += strskip(p, ' '); pu = p;    p += strloc(p, ' '); if (*p) *(p++) = '\0';
  if (! *pl)		pl = pf, pf = pu;	/* Only 1 string */
  if (*pf == ':')	p = pl, pl = pf+1, pf = p;
  if (*pu == ':')	p = pl, pl = pu+1, pu = p;
  if (*pf == '"')	p = pu, pu = pf,   pf = p;

						/* decode type */
  if (status = tbl_dectyp(dtype, type, noelem, form))
  	return(status);

  if (*pf)	strcpy (form, pf);
  if (*pu)	strcpy (unit, pu);
  strcpy (label, pl);

  return(status);
}
