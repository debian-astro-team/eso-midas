C @(#)tdsel.for	19.1 (ES0-DMD) 02/25/03 14:11:20
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  19:57 - 11 DEC 1987
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C
C.IDENTIFICATION        TDSEL.FOR
C.KEYWORDS           TABLE, APPLICATIONS
C.ENVIRONMENT  MIDAS
C.PURPOSE
C
C     Handle descriptor with selection criterion
C     The routine TDRSEL has to be upgraded to avoid a 
C     terrible crash when the descriptor is not present
C
C------------------------------------------------------------------
      SUBROUTINE TDRSEL(TID,LINE,ISTAT)
C
C    READ SELECT FLAG
C
      IMPLICIT NONE
      INTEGER       TID          !  IN : table ident
      CHARACTER*(*) LINE         !  OUT: selection criterion
      INTEGER       ISTAT        !  OUT: output status
C
      INTEGER ACTVAL
      CHARACTER*10   SELDES
      INTEGER       START, LENGTH,DNULL,DUNIT(16)
      DATA          START/1/
      DATA          LENGTH/64/        
      DATA          SELDES/'TSELTABL '/
C
      LINE   = '-'
C
C ... MODIFY THE ERROR CONTROL
C
C
C ... READ DESCRIPTOR
C
      CALL STDRDC(TID,SELDES,1,START,LENGTH,ACTVAL,LINE,
     +            DUNIT,DNULL,ISTAT)
      RETURN

      END

      SUBROUTINE TDWSEL(TID,LINE,ISTAT)
C
C    WRITE SELECT FLAG
C
      IMPLICIT NONE
      INTEGER       TID      ! IN : table ident
      CHARACTER*(*) LINE     ! IN : selection criterion
      INTEGER       ISTAT    ! OUT: output status
C
      CHARACTER*10  SELDES
      INTEGER       START, LENGTH, DUNIT(16)
      DATA          START/1/
      DATA          LENGTH/64/        
      DATA          SELDES/'TSELTABL  '/
C
C ... WRITE DESCRIPTOR
C
      CALL STDWRC(TID,SELDES,1,LINE,START,LENGTH,DUNIT,ISTAT)
      RETURN
      END
