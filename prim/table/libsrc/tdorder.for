C @(#)tdorder.for	19.1 (ES0-DMD) 02/25/03 14:11:19
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:59 - 11 DEC 1987
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C.IDENTIFICATION        TDORDER.FOR
C.KEYWORDS           TABLE, APPLICATIONS
C.ENVIRONMENT  MIDAS
C.PURPOSE
C
C RETURNS THE VALUES -1,0,+1 IF THE ALPHANUMERIC CHARACTER
C STRING IN
C IX IS < , = , > TO THAT IN IY, IN THE SENSE THAT
C 0<1<2,...<9<SPACE<A<B<...<Z   .
C
C
C------------------------------------------------------------------
    
      INTEGER FUNCTION TDORDE(IST,IEN,IX,IY)
      IMPLICIT NONE
      INTEGER IST      ! IN : starting byte
      INTEGER IEN      ! IN : last byte
      CHARACTER*(*) IX ! IN : string1
      CHARACTER*(*) IY ! IN : string2
C
      INTEGER K
      DO 10 K = IST,IEN
          IF (IX(K:K).GT.IY(K:K)) GO TO 20
          IF (IX(K:K).LT.IY(K:K)) GO TO 30
   10 CONTINUE
      TDORDE = 0
      RETURN
   20 TDORDE = 1
      RETURN
   30 TDORDE = -1
      RETURN
      END
