/*===========================================================================
  Copyright (C) 1989-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbsort.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities

.COMMENTS       This module implements the following Midas commands:
\begin{TeX}
\begin{enumerate}
\item {\tt SORT/TABLE} table {\em key1[(-)][,key2[(-),...]]}

	Sort table by column(s).

\end{enumerate}
\end{TeX}

.VERSION  1.0 	25-Mar-1989   Definition     J.D. Ponz
.VERSION  3.0 	05-Jul-1990   New version with column arrays   F.O.
				New syntax for sorting column designations
 090708		last modif
------------------------------------------------------------*/

#include <tbldef.h>
#include <midas_def.h>
#include <str.h>
#include <proto_tbl.h>
#include <stdio.h>

#define PARLEN 		80
#define MAX_COLS	8


extern int tbl_getarg();



int tbl_sort()
/*++++++++++++++++++
.PURPOSE SORT/TABLE table keys
.RETURNS Status
------------------*/
{
  char table[PARLEN], column[PARLEN], flag[PARLEN];
  int  tid, icol[MAX_COLS], srtflg[MAX_COLS], status, ic;
  int count;
  int irow;
  int colu;
  int row;
  int nsort;
  int allcol;
  int allrow;
  int value;


                                                /* read parameters */
  status = tbl_getarg(1, PARLEN, table);
  status = tbl_getarg(2, PARLEN, column);
  status = tbl_getarg(3, PARLEN, flag);

  tid = -1;
  status = TCTOPN(table,F_IO_MODE,&tid);
  if (status != ERR_NORMAL) {
  	SCTPUT("Error opening the table");
	return (status);
  }

  /* reset the table selection to all rows selected*/
  status = TCIGET(tid, &colu, &row, &nsort, &allcol, &allrow);
  
  status = TCSCNT(tid, &count);
  if (row != count)
  {
       /*if not all rows selected, select them all*/
       value = 1;
       for (irow=1; irow<= row ; irow++ )
         {
         status = TCSPUT(tid, irow, &value);
	 }
       TCSSET(tid," ");
  }
  
  icol[0]   =  1;	/*   Defaults	*/
  srtflg[0] =  1;
  						/* Backwards compatibility */
  if (flag[0] == 'd' || flag[0] == 'D') srtflg[0] = -1;

						/* Read Sorting Keys */
  status = TCCSEL(tid, column, MAX_COLS, icol, srtflg, &ic);
  if (status) {
	SCTPUT ("**** Bad column(s) name");
  	goto error;
  }

  						/* sort */
  status = TCCSRT(tid, ic, icol, srtflg);

error:
 CGN_DSCUPD(tid,tid," ");
 TCTCLO(tid);

 return (status);
}       
