/*===========================================================================
  Copyright (C) 1989-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbshow.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities

.COMMENTS       This module implements the following Midas commands:
\begin{TeX}
\begin{enumerate}
\item {\tt SHOW/TABLE} table 

	Display table characteristics. 
	The routine get the table name from a keyword, opens the file,
	lists on the terminal the table control information
	and updates the {\bf outputi}(1..8) keyword with values
	cols, rows, sort\_col, ref\_col,
  	allocated\_cols, allocated\_rows, store, selected\_rows.

\end{enumerate}
\end{TeX}

.VERSION  1.0 	25-Mar-1989   Definition     J.D. Ponz
.VERSION  3.0 	05-Jul-1990   New version with column arrays   F.O.
.VERSION  3.1 	07-Dec-1990: List the "Obsolete Version"

 090708		last modif
------------------------------------------------------------*/

#include <tbldef.h>
#include <midas_def.h>
#include <proto_tbl.h>

#include <stdio.h>


#define PARLEN 80

static int kunit = 0;

extern int tbl_getarg();



int tbl_show()
/*++++++++++++++++++
.PURPOSE SHOW/TABLE table_name 
.RETURNS Status
------------------*/
{
char table[PARLEN], line[PARLEN+16];
char rlabel[1+TBL_LABLEN], slabel[1+TBL_LABLEN];
char label[1+TBL_LABLEN], unit[1+TBL_UNILEN], form[9], *type, ws[PARLEN];
char *pstore, *pnote;
 
int  tid, cols, rows, scol, acols, arows, rcol, store;
int  info[8], len, dtype, i, status;
int  items, bytes, selected;

                                                /* read parameter */
  status = tbl_getarg(1, PARLEN, table);
                                                /* open tablefile */
  tid = -1;
  status = TCTOPN(table, F_I_MODE|F_EIO_FORCE, &tid);
  if (status != ERR_NORMAL) {SCTPUT("Error opening the table");
                             return (status);
                            }
         					/* read global chars. */
  TCIGET(tid, &cols, &rows, &scol, &acols, &arows);
  scol = (scol > 0) ? scol : -scol;
  TCKGET(tid, &rcol);
  TCSCNT(tid, &selected);
  TCDGET(tid, &store);
  TCLGET(tid, rcol, rlabel);
  TCLGET(tid, scol, slabel);
  pstore = (store == F_RECORD ? "Record": "Transposed");
  pnote  = (TCVERS(tid) < 0   ? "***OLD*** "  : "");

  len = sprintf(line," Table : %s",table);
  for (i=len; i<=40; i++) line[i] = ' ';

  (void)sprintf(&line[i], "[%s%s format]", pnote, pstore);
  SCTPUT(line);  
  (void)sprintf(line," No.Columns : %7d   No.Rows : %7d ",cols,rows);
  SCTPUT(line);  
  (void)sprintf
  (line," All.Columns: %7d   All.Rows: %7d         Sel.Rows: %7d",
   acols,arows,selected);
  SCTPUT(line);  
  (void)sprintf(line," Sorted  by :%-10sReference:%-12s ", slabel, rlabel);
  SCTPUT(line);  

   						/* write output information */
  info[0] = cols;
  info[1] = rows;
  info[2] = scol;
  info[3] = rcol;
  info[4] = acols;
  info[5] = arows;
  info[6] = store;
  info[7] = selected;

  SCKWRI("OUTPUTI", info, 1, 8, &kunit);

status = tbl_getarg(2, 10, line);

if ((line[0] == 'f') || (line[0] == 'F'))
  { 					/* iteration on columns */
  for (i=1; i<=cols; i++)
      {TCLGET(tid, i, label); label[16] = '\0';
       TCUGET(tid, i, unit);  unit[16]  = '\0'; 
       TCBGET(tid, i, &dtype, &items, &bytes);
       TCFGET(tid, i, form, &len, &dtype);

       switch(dtype) {
       case D_I1_FORMAT: type = "I*1"; break;       
       case D_I2_FORMAT: type = "I*2"; break;       
       case D_I4_FORMAT: type = "I*4"; break;       
       case D_L1_FORMAT: type = "L*1"; break;       
       case D_L2_FORMAT: type = "L*2"; break;       
       case D_L4_FORMAT: type = "L*4"; break;       
       case D_R4_FORMAT: type = "R*4"; break;       
       case D_R8_FORMAT: type = "R*8"; break;       
       default         : type = "???"; break;
       case D_C_FORMAT:  if (bytes != items) bytes = bytes/items; 
                         sprintf(ws,"C*%d", bytes); 
       			 type = ws; break;
       }
       if ((dtype != D_C_FORMAT) &&  items > 1)	
              sprintf (ws, "%s(%d)", type, items), type = ws;
       else if ((dtype == D_C_FORMAT) && items != 1)
              sprintf (ws, "%s(%d)", type, items), type = ws;
       else
              sprintf(ws,"C*%d", bytes);
       
       (void)sprintf(line," Col.# %3d:%-16s Unit:%-16s Format:%-6s %s",
             i, label, unit, form, type);
   
      SCTPUT(line);
      }
  }
 						/* display selection flag */
  TCSINF(tid,ws);
  if (ws[0] == '-') 
     (void)sprintf(line," Selection: ALL"); 
  else              
     (void)sprintf(line," Selection: %s",ws); 
  SCTPUT(line);
  return (TCTCLO(tid));
}       
