/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION

 090903		last modif
===========================================================================*/

#include <midas_def.h>

#include <stdio.h>

 
void   descw(tid,j,values,klen,khisto)
int tid,j;
int values[9],*klen;
char khisto[81];

{
int ioff,iav,kuni,ll;
int jj;

char cbuf[81],parm[2];



  ll = values[j];
  sprintf(parm,"P%d",j);
  SCKGETC(parm,1,ll,&iav,cbuf); 
  if (khisto[0]) {
        khisto[*klen] = ' ';
        ioff = *klen+1;
        khisto[ioff] = '\0';
        }
  else ioff=0;
  iav = *klen+1+ll;
  if (iav>80)
           {
           SCDWRC(tid,"HISTORY",1,khisto,-1,80,&kuni);
           if (ll == 80)
              {
              SCDWRC(tid,"HISTORY",1,cbuf,-1,80,&kuni);
              *klen = -1;
               }
            else
               {
               *klen = -1;
               descw(tid,j,values,klen,khisto);
                }
            }
   else
            {
            *klen = *klen+1+ll;
            jj = 0;
            while (cbuf[jj]){
                  khisto[ioff+jj] = cbuf[jj]; 
                  jj++;
                  }
            khisto[ioff+jj] = ' ';
            }
}



int tbl_hist(tid)
int tid;

{
static int one = 1;
static int zero = 0;
int  ec,el,ed,knul,flag,iav,values[9];
int  ibuf[5];
int  kuni,i,j,status,noelem,bytelm,*klen;	/* `kuni' is of type int !! */

char type,khisto[81];
char *fin, name[60] ;


SCECNT("GET",&ec,&el,&ed);
SCECNT("PUT",&one,&zero,&zero);
status = SCDRDI(tid,"HISTORY_UPDA",1,1,&iav,&flag,&kuni,&knul);
SCECNT("PUT",&ec,&el,&ed);
if ((status==0)&&(flag==0)) return(0);
SCDFND(tid,"HISTORY",&type,&noelem,&bytelm);
if (type != ' ') {
    SCFNAME(tid,name,60);
    SCFINF(name,1,ibuf);
    if (ibuf[0] <= 5) {
       noelem = noelem/72;
       fin = osmmget(8*noelem);
       oscfill(fin,noelem*8,' ');
       SCDWRC(tid,"HISTORY",1,fin,-1,8*noelem,&kuni);
       }
    }
SCKGETC("HISTORY",1,80,&iav,khisto);
i = strlen(khisto);
klen = &i;
if (khisto[0])
    {
     if (*klen==80)
       {
       SCDWRC(tid,"HISTORY",1,khisto,-1,80,&kuni);
       *klen = 0;
       khisto[0] = '\0';
       }
    }
else *klen = -1;
SCKRDI("PCOUNT",1,9,&iav,values,&kuni,&knul);
if (values[0]>=1)
   {
    for (j=1;j<=values[0];j++)  descw(tid,j,values,klen,khisto);
    }  
khisto[*klen] = '\0';
if (*klen>0) 
 SCDWRC(tid,"HISTORY",1,khisto,-1,80,&kuni);
return(0);
} 

