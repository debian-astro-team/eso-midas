/*===========================================================================
  Copyright (C) 1995-2008 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbcopima.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities
.COMMENTS    This module implements the following Midas commands:
\begin{TeX}
\begin{enumerate}
\item {\tt COPY/TI} table image 
\item {\tt COPY/IT} image table
\end{enumerate}
\end{TeX}
.VERSION 1.0    18-dec-1992   Definition     M. Peron

 080407		last modif

---------------------------------------------------------------*/

#include <atype.h>
#include <tbldef.h>
#include <tblsys.h>
#include <tblerr.h>
#include <midas_def.h>
#include <computer.h>
#include <macrogen.h>
#include <proto_tbl.h>
#include <proto_os.h>

#include <stdio.h>



int tbl_copyit()
/*++++++++++++++++++
.PURPOSE COPY/IT image table
.RETURNS Status
------------------*/
{
char inima[62],outtab[62],column[TBL_LABLEN];
char ident[72],cunit[33];
int dummy, icol, status, i, j, k;
int imno, naxis, npix[3], items;
int nrow, ncol, tid, ic;
double  start[3], step[3], val;
float *pntr;


status = SCKGETC("IN_A",1,60,&dummy,inima);
status = SCKGETC("OUT_A",1,60,&dummy,outtab);
status = SCKGETC("INPUTC",1,TBL_LABLEN,&dummy,column);
/*status = SCKRDR("NULL",1,1,&actvals,&rnull,&kunit,&null); */
if (column[0] == '+') {
         icol = 0; 
         strcpy(column,"LAB001");
         }
else icol=1;

ident[0] = cunit[0] = '\0';

status = SCIGET(inima,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,3,&naxis,
       npix,start,step,ident,cunit,(char **)&pntr,&imno);
if (naxis == 1) npix[1] = 1;
ncol = npix[1] + icol;
nrow = npix[0];
items = npix[2];
status = TCTINI(outtab,F_TRANS,F_O_MODE,ncol,nrow,&tid);
status = TCCINI(tid,D_R4_FORMAT,items,"E12.6"," ",column,&ic);
for (i = 2; i<= ncol; i++) {
    sprintf(column,"LAB00%d",i);
    status = TCCINI(tid,D_R4_FORMAT,items,"E12.6"," ",column,&ic);
    }
if (icol == 1)  for (i=1; i<= nrow; i++) {
                val = (i-1) * step[0] + start[0];
                status = TCEWRD(tid,i,1,&val);
                }
for (k= 0; k<items; k++) 
  for (j=icol; j<ncol; j++) 
       for (i=0; i<nrow; i++)  {
           TCAWRR(tid,i+1,j+1,k+1,1,pntr);
           pntr++;
           }
status = TCTCLO(tid);
return(status);
}

int tbl_copyti()
/*++++++++++++++++++
.PURPOSE COPY/TI table image
.RETURNS Status
------------------*/
{
char ident[80], cunit[68], column[TBL_LABLEN+2];
char intab[62], outima[62];

int kunit;
int tid,ncol,nrow,nacol,narow,nrall;
int imno, naxis, npix[3], actvals;
int dummy,nbytes,type,items;
int iColumnNum, iRowNum, iItemNum;
int null, status, startcol, endcol;
int count;		/* number of selected rows               -> MIDEnv */
int value;		/* row selection flag, can be TRUE/FALSE -> MIDEnv */

double start[3], step[3];

float *pntr, rnull, *rvalue;



status = SCKGETC("IN_A",1,60,&dummy,intab);
(void) strcpy(ident,"from table ");
(void) strcat(ident,intab);
ident[72] = '\0';				/* we take no risk */
status = SCKGETC("OUT_A",1,60,&dummy,outima);
status = SCKGETC("INPUTC",1,TBL_LABLEN,&dummy,column);	/* single column? */
status = SCKRDR("NULL",1,1,&actvals,&rnull,&kunit,&null); 

tid = -1;
status = TCTOPN(intab,F_I_MODE,&tid);
status = TCIGET(tid,&ncol,&nrow,&nacol,&narow,&nrall);

start[0] = start[1] = start[2] =  0.;
step[0] = step[1] = step[2] = 1.;

/* get info about 1st column */

status = TCBGET(tid,1,&type,&items,&nbytes);
startcol = 0;
endcol = ncol;
npix[1] = ncol;    /* number of columns */

if (items != 1)
   { 				/* 3-dim table */
   naxis = 3;
   }
else 
   {
   naxis = 1;
   if (ncol > 1) 		/* more than 1 column */
      {
      if (column[0] == '+') 
         {
         naxis=2;  
         }
      else
         {
         status = TCCSER(tid,column,&startcol);
         startcol --;
         endcol = startcol + 1;
         npix[1] = 1;
         }
      }
   }
   

status = TCSCNT(tid,&count);
npix[0] = count;			/* no. of rows currently selected */   
npix[2] = items;
rvalue = (float *) osmmget((long) (items*sizeof(float)));
   
/* 1. allocate space for the image: */   
(void) strcpy(cunit,"       ");
status = SCIPUT(outima,D_R4_FORMAT,F_O_MODE,F_IMA_TYPE,naxis,npix,
               start,step,ident,cunit,(char **)&pntr,&imno); 

/* 2. fill the image with values: */
for (iColumnNum=startcol; iColumnNum<endcol; iColumnNum++)
   {					/* step through all columns */
   status = TCBGET(tid,iColumnNum+1,&type,&items,&nbytes);
	  				/* step through all rows  */
   for (iRowNum=0; iRowNum<nrow; iRowNum++) 
      {				/* take into account only selected rows */
      status = TCSGET(tid,iRowNum+1,&value);
      if (value != 0)			/* test if row selected */
	 {	  	          				
         status = TCARDR(tid,iRowNum+1,iColumnNum+1,1,items,rvalue);
         for (iItemNum=0; iItemNum<items; iItemNum++) 	/* loop for array  */
	    {
            null = isNULLF(rvalue+iItemNum);
            if (null == 1)
	       *pntr++ = rnull;    /* null elements will be set to null value 
				      defined by keyword NULL */
            else 
	       *pntr++ = *(rvalue+iItemNum); /* values */
            }
	 }
      }
   }

status = SCFCLO(imno);

osmmfree(rvalue);
return(status);
}


