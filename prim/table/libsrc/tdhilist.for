C @(#)tdhilist.for	19.1 (ES0-DMD) 02/25/03 14:11:17
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  12:12 - 15 DEC 1987
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C    900219   KB, replace SX calls by ST calls
C
C
C.IDENTIFICATION        TDHILIST.FOR
C.KEYWORDS           TABLE, APPLICATIONS
C.ENVIRONMENT  MIDAS
C.PURPOSE
C  LIST HISTOGRAM
C
C
C------------------------------------------------------------------
      SUBROUTINE TDHLIS(N,X,IFR,TSTAT,LAB1,LAB2,LAB3,UNIT,STATUS)
      IMPLICIT NONE
      INTEGER N           !   IN : number of points
      REAL X(N)           !   IN : classes
      INTEGER IFR(N)      !   IN : counts
      REAL TSTAT(8)       !   IN : statistics
      CHARACTER*(*) LAB1  !   IN : label 1
      CHARACTER*(*) LAB2  !   IN : label 2
      CHARACTER*(*) LAB3  !   IN : label 3
      CHARACTER*(*) UNIT  !   OUT: units
      INTEGER STATUS      !   OUT: status
C
      INTEGER I1,I2,I
      CHARACTER*80 LINE0
      CHARACTER*80 LINE1,LINE2,LINE3,LINE4,LINE5
      CHARACTER*80 LINE6,LINE7,LINE8,LINE9,LINEA
C
      DATA LINE1/
     +     ' NO. OF DATA    :                                     '/
      DATA LINE2/
     +     ' MEAN VALUE     :                                     '/
      DATA LINE3/
     +     ' STD. DEVIATION :                                     '/
      DATA LINE4/
     +     ' MINIMUM VALUE  :                                     '/
      DATA LINE5/
     +     ' MAXIMUM VALUE  :                                     '/
      DATA LINE6/
     +     ' INTERVAL       :                                     '/
      DATA LINE7/
     +     '  UP  TO                            :                 '/
      DATA LINE8/
     +     '                 TO                 :                 '/
      DATA LINE9/
     +     '                      AND ON        :                 '/
C
      I1     = 1
      I2     = N - 1
C
C ... LIST DATA
C
      LINEA = LAB3
      CALL STTPUT(LINEA,STATUS)
      WRITE (LINE1(21:35),9000) TSTAT(2)
      CALL STTPUT(LINE1,STATUS)
      WRITE (LINE2(21:35),9000) TSTAT(3)
      CALL STTPUT(LINE2,STATUS)
      WRITE (LINE3(21:35),9000) TSTAT(4)
      CALL STTPUT(LINE3,STATUS)
      WRITE (LINE4(21:35),9000) TSTAT(5)
      CALL STTPUT(LINE4,STATUS)
      WRITE (LINE5(21:35),9000) TSTAT(6)
      CALL STTPUT(LINE5,STATUS)
      WRITE (LINE6(21:35),9000) X(2) - X(1)
      LINE0  = LINE6(1:37)//UNIT
      CALL STTPUT(LINE0,STATUS)
      LINE0  = LAB2//UNIT//LAB1
      CALL STTPUT(LINE0,STATUS)
      WRITE (LINE7(21:35),9000) X(I1)
      WRITE (LINE7(39:53),9010) IFR(I1)
      CALL STTPUT(LINE7,STATUS)
      DO 10 I = I1 + 1,I2
          WRITE (LINE8(2:16),9000) X(I-1)
          WRITE (LINE8(21:35),9000) X(I)
          WRITE (LINE8(39:53),9010) IFR(I)
          CALL STTPUT(LINE8,STATUS)
   10 CONTINUE
      WRITE (LINE9(2:16),9000) X(I2)
      WRITE (LINE9(39:53),9010) IFR(I2+1)
      CALL STTPUT(LINE9,STATUS)
      RETURN

 9000 FORMAT (G15.7)
 9010 FORMAT (I7)
      END
