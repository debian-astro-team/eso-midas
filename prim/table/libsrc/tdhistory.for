C @(#)tdhistory.for	19.1 (ES0-DMD) 02/25/03 14:11:17
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  19:51 - 11 DEC 1987
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C
C
C.IDENTIFICATION        TDHISTORY.FOR
C.KEYWORDS           TABLE, APPLICATIONS
C.ENVIRONMENT  MIDAS
C.PURPOSE
C
C
C------------------------------------------------------------------
      SUBROUTINE TDHSTR(TID,ISTAT)
C
C    HANDLING OF HISTORY
C
      IMPLICIT NONE
      INTEGER TID,ISTAT
C
      INTEGER IAC,UNO,LEN,KUN,KNUL
      CHARACTER*8 NAME
      CHARACTER*80 HISTO
C
      NAME   = 'HISTORY '
      UNO    = 1
      LEN    = 80
C
      CALL STKRDC(NAME,UNO,UNO,LEN,IAC,HISTO,KUN,KNUL,ISTAT)
C
      UNO    = -UNO
C      CALL STDWRC(TID,NAME,UNO,HISTO,UNO,LEN,KUN,ISTAT)
      RETURN

      END
