/* @(#)tbdectyp.c	19.1 (ES0-DMD) 02/25/03 14:11:13 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbdectyp.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities

.COMMENTS
\begin{TeX}

Decodes column type. Input type is defined as:

R[eal], R[eal]*4   - floating point single precision.
D[ouble], R[eal]*8 - floating point double precision.
I[nteger], I[nteger]*4 - long integer (4 bytes).
S[hort],   I[nteger]*2 - short integer (2 bytes).
B[yte],    I[nteger]*1 - byte integer (1 byte).
C[haracter]*n          - character string.
\end{TeX}


.VERSION 1.0 	25-Mar-1989   Definition     J.D. Ponz
.VERSION 3.0 	05-Jul-1990   New version with column arrays   F.O.
------------------------------------------------------------*/

#include <tbldef.h>
#include <tblerr.h>
#include <midas_def.h>


tbl_dectyp(etype,type,noelem,form)
/*++++++++++++++++++
.PURPOSE Interpret the Type (e.g. R*4) declaration
.RETURNS Status
------------------*/
 char *etype;		/* IN: Text to scan	*/
 int  *type;		/* OUT: Midas data type	*/
 int  *noelem;		/* OUT: Array size	*/
 char *form;		/* OUT: Default Format	*/
{
	int 	i, l1, alen;
	char	msg[256];

  *noelem = 1;
  i = strloc (etype, '(');
  if (etype[i])	{
  	l1 = atoi(etype+1+i);
  	if ((l1 < 1) || (l1 > TBL_MAXSIZ)) {
		sprintf(msg, "****Bad array size in `%s'", etype);
		SCTPUT (msg);
		return (ERR_TBLCOL);
  	}
  	*noelem = l1;
  }

  l1 = 0;				/* decode type */
  i = strloc (etype, '*');
  if (etype[i])	l1 = atoi(etype + i + 1);
  switch (etype[0]) {
    case 'd': case 'D':
    case_double:
	if (!alen)	alen = 24;
    	sprintf (form, "D%d.%d", alen, alen-7);
    	*type = D_R8_FORMAT;
    	break;
    case 'R': case 'r': 
	if (l1 == 8)	goto case_double;
	if (!alen)	alen = 12;
    	sprintf (form, "E%d.%d", alen, alen-6);
	*type = D_R4_FORMAT;
	break;
    case 'I': case 'i': 
    case_int:
	if (l1 == 1)	{ *type = D_I1_FORMAT; if (!alen)  alen = 4; }
	else if (l1==2)	{ *type = D_I2_FORMAT; if (!alen)  alen = 6; }
	else		{ *type = D_I4_FORMAT; if (!alen)  alen = 11;}
    	sprintf (form, "I%d", alen);
	break;
    case 'l': case 'L': 
    case 'u': case 'U': 
    	if (l1 == 1)	{
		l1 = *noelem; 	*noelem = 1;
		goto case_ascii;
    	}
	if (l1==2)	{ *type = D_L1_FORMAT; if (!alen)  alen = 4; }
	else		{ *type = D_L4_FORMAT; if (!alen)  alen = 8;}
    	sprintf (form, "X%d", alen);
	break;
    case 'a': case 'A': 
    case 'c': case 'C': 
    case_ascii:
	if (*noelem > 1) {
		sprintf(msg, "**** C*n columns can't be arrays `%s'", etype);
		SCTPUT (msg);
		return (ERR_TBLCOL);
	}
	if (l1 == 0)	l1 = alen;
  	if ((l1 < 1) || (l1 > TBL_MAXSIZ)) {
		sprintf(msg, "**** Bad character size in `%s'", etype);
		SCTPUT (msg);
		return (ERR_TBLCOL);
  	}
	sprintf(form,"A%d",l1);
	*noelem = l1;
	*type = D_C_FORMAT;
	break;  
    case 'B': case 'b': 
    	l1 = 1;    	goto case_int;
    case 'S': 
    	l1 = 2;    	goto case_int;
    default:
	sprintf (msg, "**** Bad datatype %s", etype);
	SCTPUT  (msg);
	return (ERR_TBLCOL);
  }

  return (ERR_NORMAL);
}
