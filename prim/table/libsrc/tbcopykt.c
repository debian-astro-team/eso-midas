/* @(#)tbcopykt.c	19.1 (ES0-DMD) 02/25/03 14:11:13 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tbcopykt.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities
.RETURNS
	status
.PURPOSE    

\begin{TeX}

Copy keyword into table entry(s).
If columns are not defined in the command, the full table row
is copied into the keyword. 
This command is equivalent to the MCL statement
keyword = table,column,row 

Parameters:
\begin{description}
\item[p1]    input keyword.
\item[p2]    table name.
\item[p3-pj] column reference(s).
\item[pj+1]  entry (row) number.
\end{description}

\end{TeX}


.VERSION     1.0 25-Mar-1990   Definition     J.D. Ponz
.VERSION     1.1 07-Jun-1990                  M. Peron
------------------------------------------------------------*/
#include <stdio.h>
#include <midas_def.h>
#include <tbldef.h>
#include <tblcnt.h>

#define STRINGLEN 256
#define PARLEN    80
#define NCOL      10

tbl_copykt()

{ char table[PARLEN], column[PARLEN], line[STRINGLEN];
  char form[9], ws[10], keyw[PARLEN], kname[10], cval[STRINGLEN], *kun;
  int  tid, ncol, nrow, nsort, ncall, nrall, niden, knul;
  int  len, i, status, ival, ierr, ic, irow, type, k, iav, sel;
  int  ktype, istart, nval, j, npar, inull[NCOL], ibuf[NCOL], icol[NCOL];
  float rval, rerr, rbuf[NCOL];
  double dval, derr, dbuf[NCOL];
  double atof();

  ierr = 0; rerr = 0; derr = 0;

  status = SCKRDI("PCOUNT",1,1,&iav,&npar,&kun,&knul);
  if (nval < 3) {SCTPUT("Wrong number of parameters");
                 return (status);
                }
                                                /* read parameter */
  status = tbl_getpar("P2",PARLEN,table);
                                                /* open tablefile */
  status = TCTOPN(table, F_IO_MODE, &tid);
  if (status != ERR_NORMAL) {SCTPUT("Error opening the table");
                             return (status);
                            }
  TCIGET(tid, &ncol, &nrow, &nsort, &ncall, &nrall);
                                                /* read keyword */
  status = tbl_getpar("P1",PARLEN,keyw);
  for (i=0; i<PARLEN && keyw[i] != '/' && 
                        keyw[i] != '\0' &&
                        keyw[i] != ' '; i++) kname[i] = keyw[i];
  kname[i] = '\0';
  ktype    = D_R4_FORMAT;
  istart   = 1;
  nval     = 1;  
  if (keyw[i] == '/')                       /* decode keyword type...*/
     { switch (keyw[i+1]) {
       case 'c':
       case 'C': ktype = D_C_FORMAT; break;
       case 'i':
       case 'I': ktype = D_I4_FORMAT; break;
       case 'd':
       case 'D': ktype = D_R8_FORMAT; break;
       default : ktype = D_R4_FORMAT; break;
       }
       for (j=i+3, i=0; j<PARLEN && keyw[j] != '/' && 
            keyw[j] != '\0' && keyw[j] != ' '; j++, i++) 
            cval[i] = keyw[j];
       cval[i] = '\0';
       istart  = atoi(cval);
       i = j; 
       for (j=i+1, k=0; j<PARLEN && keyw[j] != '/' && 
            keyw[j] != '\0' && keyw[j] != ' '; j++, k++) 
            cval[k] = keyw[j];
       cval[i] = '\0';
       nval    = atoi(cval);
     }
    
                                                /* read row */
  sprintf(ws,"P%d",npar);
  status = tbl_getpar(ws,PARLEN,line);
  if (line[0] == '@')                           /* sequence number */
      irow = atoi(line+1);
  else
      {TCKGET(tid, &niden);                     /* value in reference column */
       if (niden==0) irow = atoi(line);
       else {
             if (nsort != niden) {SCTPUT("Reference column is not sorted");
                                  return(status);
                                 } 
             TCFGET(tid, niden, form, &len, &type);
             switch (type) {
             case D_I1_FORMAT:
             case D_I2_FORMAT:
             case D_I4_FORMAT: ival = atoi(line);
                               TCESRI(tid,niden,ival,ierr,1,&irow);
                               break;
             case D_R4_FORMAT: rval = atof(line);
                               TCESRR(tid,niden,rval,rerr,1,&irow);
                               break;
             case D_R8_FORMAT: dval = atof(line);
                               TCESRD(tid,niden,dval,derr,1,&irow);
                               break;
             case D_C_FORMAT:  if (line[0] != '"') 
                                 {SCTPUT("Wrong Data Type");
                                  SCTPUT("Reference column is character ");
                                  return(status);
                                 }
                               for (i=1; i<STRINGLEN && line[i] != '"'; i++);
                               line[i] = '\0';
                               TCESRC(tid,niden,line,2,i,1,&irow);
                               break;
             }
            if (irow <= 0) {SCTPUT("Entry not found");
                            return(status);
                           }
            }
      }
                                                /* read column */
  ncol = 0;
  for (i=3; i<npar; i++)
      {sprintf(ws,"P%d",i);
       status = tbl_getpar(ws,PARLEN,line);
       TCCSER(tid,line,&ic);
       if (ic < 0) 
          {SCTPUT("Column not found");
          return(status);}
       else        
          {icol[ncol] = ic;
           ncol++;}
      }
  if (ncol == 0) 
     {switch(ktype) {
      case D_C_FORMAT: ic = 1; ncol = 1; break;
      default:         ncol = nval; ic = 1;
                       for (i=0; i< ncol; i++) icol[i] = i+1;
                       break;
      }
     }
                                    /* copy from key into table */
  switch(ktype) {
  case D_I4_FORMAT: SCKRDI(kname, istart, nval, &iav, ibuf, &kun, &knul);
                    if (ncol==1) {ival = ibuf[0];
                                  TCEWRI(tid,irow,ic,&ival);}
                    else          TCRWRI(tid,irow,ncol,icol,ibuf);
                    break;   
  case D_R4_FORMAT: SCKRDR(kname, istart, nval, &iav, rbuf, &kun, &knul);
                    if (ncol==1) {rval = rbuf[0];
                                  TCEWRR(tid,irow,ic,&rval);}
                    else          TCRWRR(tid,irow,ncol,icol,rbuf);
                    break;   
  case D_R8_FORMAT: SCKRDD(kname, istart, nval, &iav, dbuf, &kun, &knul);
                    if (ncol==1) {dval = dbuf[0];
                                  TCEWRD(tid,irow,ic,&dval);}
                    else          TCRWRD(tid,irow,ncol,icol,dbuf);
                    break;   
  case D_C_FORMAT:  SCKRDC(kname, 1, istart, nval, &iav, cval, &kun, &knul);
                    TCEWRC(tid,irow,ic,cval);
                    break;   
  }

  TCTCLO(tid);   
   
  return (status);
}       
