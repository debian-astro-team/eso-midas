/* @(#)mhsplit.c	19.1 (ES0-DMD) 02/25/03 14:03:29 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1990 European Southern Observatory
.LANGUAGE    C
.IDENT       mhsplit.c
.AUTHOR      Preben J. Grosbol [ESO/IPG]
.KEYWORDS    MIDAS, help files, split
.ENVIRON     UNIX or VMS
.PURPOSE     Split old total help file into one help file for each
             command and qualifier.
.COMMENT     Usage: mhsplit [-i file_list] [-t] [-v]   
             where 
               file_list is file with list of help-file names,
                         default is stdin.
               t option  is for test - no output
               v option  is verbose
             example:   ls -1 *.hlp | mhsplit -v
.VERSION     1.0   1990-Dec-18 : Creation,     PJG
.VERSION     1.1   1990-Dec-21 : Also the last command,     PJG
.VERSION     1.2   1991-Jan-08 : Do not trigger on \see,     PJG
------------------------------------------------------------------------*/
#include    <stdio.h>                       /* standard I/O library     */

#define     MXHELP        1024              /* Max. lines in help       */
#define     MXLINE         128              /* Max. char. in input line */
#define     MXSTR          128              /* Max. char. in string     */

char                      ut1[] =           /* user text first part     */
   "usage: mhsplit [-i file_list] [-t] [-v]\n";

char     text[MXHELP][MXLINE];
char     name[MXSTR];
char     cdate[MXSTR];
char     cid[MXSTR];
char     cmd[MXSTR];
char     qual[MXSTR];
int      ver;
int      wmode;

main(argc, argv)
int        argc;
char    *argv[];
{
  char    *pl, *pc;
  char    *seb, *cob, *seeb;
  char    line[MXLINE];
  int     nh, com;
  int     nse, nsee, nco, chg, cchg;
  FILE    *fpi, *fph;

  fpi = stdin;                         /* define default values        */
  seb = "\\se"; nse = 3;
  seeb = "\\see"; nsee = 4;
  cob = "\\co"; nco = 3;
  ver = 0; wmode = 1;

  argv++;
  if (--argc != 0)                         /* decode parameters         */
    while (argc--) {
       pc = *argv++;
       if (*pc++ == '-')
	  switch (*pc) {
             case 'i' :              /* name of input directory file   */
                        if (!argc--) { printf("%s",ut1); exit(1); }
                        if ((fpi=fopen(*argv++,"r"))==NULL) {
			   printf("Can't open file %s\n",*(--argv));
			   exit(1);
                        }
                        break;
             case 'v' :               /* verbose option                */
             case 'V' : 
                        ver = 1; break;
             case 't' :               /* test DON'T write file         */
             case 'T' : 
                        wmode = 0; break;
             default  :
                        printf("%s",ut1); exit(1);
         }
    }

  while (fgets(name,MXLINE,fpi)) {              /* go through file    */
    if (name[0]=='%' || name[0]=='#' ||         /* skip comment lines */
        name[0]=='!') continue;
    pl = name; while (' ' < *pl) pl++; *pl = '\0';
    if ((fph=fopen(name,"r"))==NULL) {
	printf("Can't open help file: >%s<\n",name);
	continue;
    }

    chg = 0; com = 0; cchg = 0; nh = 0;
    while (fgets(line,MXLINE,fph)) {
      pl = line;
      while (*pl) {
        if (*pl == '\\') {              /* check TeX control sequence */
           chg  = chg || (!strncmp(pl,seb,nse) && strncmp(pl,seeb,nsee));
           cchg = cchg || !strncmp(pl,cob,nco);
        }
        pl++;
      }

      if (chg) {
	 helpfile(nh);
	 cmd[0] = '\0'; qual[0] = '\0'; name[0] = '\0';
         chg = 0; nh = 0;
       }

      pl = line; pc = &text[nh++][0];
      while (*pc++ = *pl++);

      if (cchg) { cchg = 0; com = 1; }
      if (chg || cchg || line[0]=='\\') continue;

      pl = line;
      if (com) {                                /* set command name     */
         pc = cmd;
         while (*pl && ' '<*pl && *pl!='/') *pc++ = *pl++;
         *pc = '\0';
	 if (*pl=='/') pl++;
	 pc = qual; 
	 while (*pl && ' '<*pl) *pc++ = *pl++; *pc = '\0';
         pc = cdate; while (*pl=='\t' || *pl==' ') pl++;
         while (*pl && ' '<*pl) *pc++ = *pl++; *pc = '\0'; 
         pc = cid; while (*pl=='\t' || *pl==' ') pl++;
         while (*pl && ' '<*pl) *pc++ = *pl++; *pc = '\0'; 
         if (ver) printf("Command >%s/%s< %s %s\n",
                         cmd,qual,cdate,cid);
         com = 0;
         continue;
       }
      if (MXHELP <= nh) break;
    }
    if (fph) fclose(fph);
    helpfile(nh);
    cmd[0] = '\0'; qual[0] = '\0'; name[0] = '\0';
  }
  if (fpi) fclose(fpi);
  exit(0);
}

helpfile(nh)
int    nh;
{
  char  *pc;
  char  name[MXSTR];
  int   n, i;
  FILE  *fpo;

  n = 0; pc = cmd;         /* make new name of help-file       */
  if (*pc=='@') pc = "aaaaaa";
  if (*pc=='$') pc = "vaxx";
  while (*pc && n<6)
    name[n++] = ('A' <= *pc && *pc <='Z') ? (*pc++)-'A'+'a' : *pc++;
  pc = qual; i = 0;
  if (n<6 && *pc) name[n++] = '_';
  while (*pc && i++<4)
    name[n++] = ('A' <= *pc && *pc <='Z') ? (*pc++)-'A'+'a' : *pc++;
  name[n] = '\0';
  if (n) strcat(name,".hlq");
  if (ver) printf("Write file >%s< with %4d lines\n",name,nh);
  
  if (wmode && n && (fpo=fopen(name,"w"))) {
     fprintf(fpo,"%%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
     fprintf(fpo,"%%.COPYRIGHT  (c)  1990 European Southern Observatory\n");
     fprintf(fpo,"%%.IDENT      %s\n",name);
     fprintf(fpo,"%%.AUTHOR     %s, IPG/ESO\n",cid);
     fprintf(fpo,"%%.KEYWORDS   MIDAS, help files, %s/%s\n",cmd,qual);
     fprintf(fpo,"%%.PURPOSE    On-line help file for the command: %s/%s\n",
	     cmd,qual);
     fprintf(fpo,"%%.VERSION    1.0  %s : Creation, %s\n",cdate,cid);
     fprintf(fpo,"%%----------------------------------------------------------------\n");

     for (n=0; n<nh; n++) fputs(&text[n][0],fpo);
     fclose(fpo);
     return 0;
   } 
  else {
    if (wmode && n) fprintf(stderr,"Can't open output file: %s\n",name);
    return 1;
  }
}
