% @(#)read_fcat.hlq	19.1 (ESO-IPG) 02/25/03 14:03:37 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      read_fcat.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, READ/FCAT
%.PURPOSE    On-line help file for the command: READ/FCAT
%.VERSION    1.0  10-OCT-1988 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./FCAT
\es\co
READ/FCAT	 	                            10-OCT-1988  KB
\oc\su
READ/FCAT [cat_name] [lowno,hino]
	read fit file catalog entries
\us\pu
Purpose: 
          Read entries of given FitFile Catalog.
\up\sy
Syntax:
          READ/FCAT [cat_name] [lowno,hino]
\ys\pa
          cat_name = name of FitFile Catalog;
\\
              defaulted to the currently active FitFile Catalog
\ap\pa
          lowno,hino = first entry no. and last entry no. to be listed;
\\
              defaulted to 1,99999
\ap\sa
See also:
          READ/ICAT, READ/TCAT
\\
          SET/FCAT, CREATE/FCAT, SHOW/FCAT, ADD/FCAT, SUBTRACT/FCAT
\as\no
Note:
          FitFiles in MIDAS commands may be accessed either by their name or by
          their entry number in a FitFile Catalog.
\\
          For VMS, note, that different versions of a file keep the same entry
          number, so you will always access the highest version of that file.
\on\exs
Examples:
\ex
          READ/FCAT LOLLY 10,22
          Display all entries with entry number in [10,22] of FitFile Catalog
          lolly.cat .
\xe\ex
          READ/FCAT
          Display all entries in the currently active FitFile Catalog.
\xe \sxe
