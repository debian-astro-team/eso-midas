%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2010 European Southern Observatory
%.IDENT      overpltabl.hlq
%.AUTHOR     RHW, IPG/ESO
%.KEYWORDS   MIDAS, help files, OVERPLOT/TABLE
%.PURPOSE    On-line help file for the command: OVERPLOT/TABLE
%.VERSION    1.0 09-JUN-1986 RHW Creation
%.VERSION    2.0 93-Oct-1993 RvH OVERPLOT/TABLE can handle 3D tables
%----------------------------------------------------------------
\se
SECTION./TABL
\es\co
OVERPLOT/TABLE						01-OCT-1993  RvH
\oc\su
OVERPLOT/TABLE table [plane1] [plane2] [x_sc,y_sc[,x_off,y_off]] 
        [symbols] [lines] [flag_dir]
	plot table data on selected plotting device
\us\pu
Purpose:    
            Overplot table data on selected plotting device
\up\sy
Syntax:     
            OVERPLOT/TABLE table [plane1] [plane2] [x_sc,y_sc[,x_off,y_off]]
                           [symbols] [lines] [flag_dir]
\ys\pa
            table  = name of table file
\ap\pa
            plane1 = vector or plane in abscissa (x-axis); default is 
                     sequence number, however see the note below.
\ap\pa
            plane2 = vector or plane in ordinate (y-axis); default is 
                     sequence number, however see the note below.
\ap\pa
            symbols = symbol types to be used in the plot; input can be given
                     as s_start:s_end:s_incr or s_nr1,s_nr2,s_nr3, ...
                     or any combination of these two possibilities separated 
                     by a comma. Default is the symbol type set by SET/GRAP. 
                     Data points on different lines in the extracted planes 
                     will be presented with different symbols if more than one
                     symbol type is given. The program will cycle though the 
                     given symbols if the number of lines exceed the number of
                     given symbols.
\ap\pa
            lines  = line types to be used in the plot; input can be given
                     as l_start:l_end:l_incr or l_nr1,l_nr2,l_nr3, ...
                     or any combination of these two possibilities separated
                     by a comma. PLOT/TABLE will draw lines if STYPE is equal 
                     to zero or if you specify here one or more line type 
                     to be used in the plot. Data points on the first axis 
                     of a plane will be connected with a line of given type, 
                     and if the number of lines exceed the number of given 
                     line types the program will cycle through the line types.
\ap\pa
            flag_dir = flag_dir specifies the way both planes are read.
                     Two values can be given: "D"efault and "O"pposite.\\
                     - a plane along a column in the depth direction is by
                       default stored column by column (thus the first axis
                       is along the columns, and each column can be 
                       represented by a symbol and/or line type).\\
                     - a plane along a row in the depth direction is by 
                       default stored array by array.\\
                     - a plane at a certain depth is stored column by column \\
                     Note that this flag also affects the way a vector
                     is treated!
\ap\no
Note:
               The new OVERPLOT/TABLE command is able to display data 
	    from a 3D table.
            A 3D table has 3 axes: column, row and depth (or array). The
            preliminary syntax allows you to define the following planes:
               #n[i..j]       : a plane along column "n", 
				from depth "i" to "j", 
               @r#n..m[i..j] : a plane along row "r", from column "n" 
				to "m" and from depth "i" to "j", 
               [i]#n..m      : a plane along depth "i", from column "n" 
				to "m". 
            It is also possible to refer to a column by its name ":name".
            The default for the range in depth or columns are all selected 
            elements along the depth or column axis. 
	    To specify one element one may type [i] or #n. A range of rows 
	    can only be selected by using the command SELECT/TABLE. 
               When the user gives a "?" for one of the two planes the 
	    values in the other plane will plotted against its sequence number.
            Only one of the plane input parameters can be defaulted to 
            sequence number. So, `table plane1 ?' and `table ? plane2' 
            are both valid input parameter strings, `table ? ?' is not. 
               The program allows to plot data extracted from planes with 
            different orientations against each other. But the number of
            elements along the first axis of the planes have to be equal.
            Remember that you can define the first axis by using "flag_dir". 
               It is possible to plot a vector against a plane or the 
	    other way around:
            plane1 = @3[4] (a vector along row 3 at array element 4) and 
            plane2 = @6    (a plane with all the data values at row 6). 
            The program will apply the same restrictions to the first plane
            as put on the second plane, ONLY if both planes have the same
            orientation AND if the first plane is defaulted to all elements
            in a certain direction, while the second is not. 
            For example: if plane1 = #2 and plane2 = #3[2..5] then the 
            program will only extract array elements 2 to 5 from both planes. 
            If you do not want this you will have to define first plane2 
            and plane1 as the next parameter or explicitly define the first 
            plane from the first to the last element. 
               The 2-D version of OVERPLOT/TABLE is rewritten in such a 
	    way that the old parameter list is still valid if your input 
	    table is 2-D. The program will treat a column as a plane with 
	    one array element at depth = 1. But you can also define a row 
            and plot it against an other row, column or sequence number.
               The functions LOG and LN, decimal and natural logs
            respectively, can be applied to the data to be plotted.
               Be aware that in case you plan to connect the data points with 
            a line (see SET/GRAPHICS) the data points will be connected in the
            same order as they appear in the table. In order to sort the data
            first (either in decreasing or increasing order) use the sorting
            command SORT/TABLE.
               After the overplot the last (over)plotted data set is the 
	    active one and subsequent commands will work on these data.

	    Note: This command is currently not working for images with
	    non-linear world coordinates. Use LOAD/TABLE in that case.
\on\see
See also:   
            PLOT/TABLE, PLOT/ERROR, TUTORIAL/GRAPH,SET/GRAPHICS, SHOW/GRAPHICS
\ees\exs
Examples:
\ex
            OVERPLOT/TAB 2D-table :MAGNITUDE LOG(:TEMPERATURE)
            Overplot column :MAGNITUDE versus the logarithmic value of column 
            :TEMPERATURE (base 10) of table `2D-table'.
\xe\ex
            OVERPLOT/TAB 3D-table LN(#3[1]) ?  3:6:1,10  ? Def,Op 
            Will do the same as the previous example. But it will plot the 
            first array element of a plane along column 3 against sequence
            number.
\xe\ex
            OVERPLOT/TAB 3D-table #3 #5 3:6:1,10 1,4
            Plot the values found in a plane along column 3 against column 5.
            The data will be presented according to the following scheme: 
            array element:  1  2  3  4  5  6  7  8  etc. 
            line type    :  1  4  1  4  1  4  1  4  ...  
            symbol type  :  3  4  5  6 10  3  4  5  ...  
\xe\ex
            OVERPLOT/TAB 3D-table @3:name @5 3:6:1,10 1,4
            Take a vector at row 3, column "name" as abscissa and 
            the plane along row 5 in the depth direction as ordinate. 
            Plotted with the same line and symbol types as in the previous 
            example. The vector and plane are both read (by default) array
            by array, thus there are no dimensional problems.
\xe\ex
            OVERPLOT/TAB 3D-table [3]#1..4 #2[1..4] 3:6:1,10 1,4
            Take plane along depth 3, from column 1 to 4 as abscissa and 
            the plane along column 2, from depth 1 to 4 as ordinate.
            Both planes are read column by column so four lines will be
            plotted with resp. line type 1, 4, 1, 4.
\xe \sxe
