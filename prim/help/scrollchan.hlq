% @(#)scrollchan.hlq	19.1 (ESO-IPG) 02/25/03 14:03:42 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      scrollchan.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, SCROLL/CHANNEL
%.PURPOSE    On-line help file for the command: SCROLL/CHANNEL
%.VERSION    1.0  30-APR-1990 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./CHAN
\es\co
SCROLL/CHANNEL						25-MAY-1998  KB
\oc\su
SCROLL/CHANNEL [chanl] [scrolx,scroly]
	scroll given ImageDisplay channel
\us\pu
Purpose:  
          Scroll image on given ImageDisplay channel
\up\sy
Syntax: 
          SCROLL/CHANNEL [chanl] [scrolx,scroly]
\ys\pa
          chanl = channel no.; defaulted to currently displayed channel
\ap\pa
          scrolx,scroly = fixed scroll values; if omitted, the scroll values
               are modified interactively
\ap\sa
See also:
          CLEAR/SCROLL, DISPLAY/CHANNEL, SHOW/CHANNEL, ZOOM/CHANNEL, LOAD/IMAGE
\as\no
Note:  
          The channel (image memory) x,y-size can be larger than the relevant
          display size (see CREATE/DISPLAY). In that case you can scroll (also
          called pan) the image within the display window.
\\
          The scroll values specify the column of the image memory which
          is displayed as first (left) column in the display and the row of
          the channel which is displayed as the last (top) row in the display.
          That means, the upper left pixel of the display is used as reference,
          not pixel (0,0) which is in the lower left corner of the display
          window. Columns and rows are counted from 0 on.
\\
          Scrolx must be in [-(chan_xsize-1),chan_xsize-1], negative values
          push the image to the right. Scroly must be in [0,2*chan_ysize-1],
          values above chan_ysize push the image down.
\\
         For interactive scrolling, use the arrow keys of the keyboard 
         (with the mouse cursor in the display window!). The number keys
         (on top of the keyword only) can be used to adjust the speed.
         To exit press EXIT (right) button on the mouse.
\on\exs
Examples:
\ex
          SCROLL/CHAN 0 200,200
            Set scroll values of channel 0  to 200,200 .
\xe\ex
          SCROLL/CHAN 
            Change scroll values of displayed channel interactively.
\xe \sxe
