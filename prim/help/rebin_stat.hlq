% @(#)rebin_stat.hlq	19.1 (ESO-IPG) 02/25/03 14:03:39 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      rtebin_stat.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, REBIN/STATISTICS
%.PURPOSE    On-line help file for the command: REBIN/STATISTICS
%.VERSION    1.0  22-SEP-1998 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./STAT
\es\co
REBIN/STATISTICS				   22-SEP-1998  KB
\oc\su
REBIN/STATISTICS in out [xpix,ypix] [stat_val]
	rebin image using statistical values as new pixel values
\us\pu
Purpose:  
          Reduce no. of pixels by replacing frame intervals by single
          statistical value.
\up\sy
Syntax: 
          REBIN/STATISTICS in out [xpix,ypix] [stat_val]
\ys\pa
          in = input frame
\ap\pa
          out = output frame 
\ap\pa
          xpix,ypix = no. of frame pixels of interval (subframe) over which
               statistics are calculated;
\\
               the interval of xpix*ypix pixels will be a single pixel in the
               result frame with value equal to one of the possible statistical
               quantities specified below;
\\
               defaulted to 8,8
\ap\pa
          stat_val = may be any of: Minimum, Maximum, Mean, Stddev, Mom3,
               Mom4, Total, Median, Mode1 or Mode (see STATISTICS/IMAGE for
               how these values are computed);
\\
               defaulted to Mean
\ap\sa
See also:
          STATISTICS/IMAGE, REBIN/LINEAR
\as\no
Note:
          This command is only applicable to 1- or 2-dim images.
\\
          The START values of result frame are the same as for the input frame.
          The STEP values of result frame are calculated as `xpix'*STEP(1)
          and `ypix'*STEP(2).
\\
          NPIX(i) of result frame is computed as: (NPIX(i)-1)/pix + 1 for
          i=1,2 and pix = `xpix' or `ypix'.
\on\exs
Examples:
\ex
          REBIN/STATIST conejo tortuga 16,16 stddev
            Assuming that `conejo.bdf' is a 1024x1024 image with START=0.,0.
            and STEP=1.,1., we create a 64x64 image `tortuga.bdf' with
            START=0.,0. and STEP=16.,16. Each pixel of the result frame is the
            standard deviation over a 16x16 pixel interval of `conejo.bdf'.
            The intervals used are [<,<:@16,@16], [@17,<:@32,@16], ...,
            [@1009,<:@1024,@16], [<,@17:@16,@32], [@17,@16:@32,@32], ...
\xe \sxe
