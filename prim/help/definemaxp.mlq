% @(#)definemaxp.mlq	19.1 (ESO-IPG) 02/25/03 14:03:13
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      definemaxp.mlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, DEFINE/MAXPAR
%.PURPOSE    On-line help file for the Midas language command: DEFINE/MAXPAR
%.VERSION    1.0  22-MAR-1993 : Creation, KB
%----------------------------------------------------------------
\rightpage
\se
SECTION./MAXP
\es\co
DEFINE/MAXPAR						22-MAR-1993  KB
\oc\su
DEFINE/MAXPAR maxno
	define the maximum no. of parameters for a procedure
\us\pu
Purpose:   
	  Define the maximum no. of parameters for a procedure.
\up\sy
Syntax:
          DEFINE/MAXPAR maxno
\ys\pa
          maxno = maximum no. of parameters, maxno in [1,8]
\ap\no
Note:  
          When the procedure is executed with more than `maxno' parameters
          a warning message is displayed and the procedure continues.
\on\sa
See also:
          DEFINE/PARAMETER
\as\exs
Examples:
\ex
          DEFINE/MAXPAR 3
           Tell MIDAS that the procedure should be executed with 3 parameters
           at most.
\xe \sxe
