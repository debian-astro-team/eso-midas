% @(#)computplan.hlq	19.1 (ESO-IPG) 02/25/03 14:03:03 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      computplan.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, COMPUTE/PLANE
%.PURPOSE    On-line help file for the command: COMPUTE/PLANE
%.VERSION    1.0  970701: Creation, KB
%----------------------------------------------------------------
\se
SECTION./PLAN
\es\co
COMPUTE/PLANE						 01-JUL-1997  KB
\oc\su
COMPUTE/PLANE res_frame.plane = arithmetic_expression
	do arithmetics on planes of images
\us\pu
Purpose:
          Perform arithmetic operations on planes of 3-dim images and store 
          result in a plane of the result image.
\up\sy
Syntax:  
          COMPUTE/PLANE res_frame.plane = expression
\ys\pa
          res_frame.plane = image and plane to get result of expression,
              the plane is specified by Pi (or pi), with i in [1,NPIX(3)];
\ap\pa
          expression = expression with up to 10 operands, which may be
              functions and planes or/and constants in the usual algebraic
              notation, planes of the res_frame are indicated by "Pi" (or "pi")
              with i = plane_no. (beginning at 1);
\\
              planes of other frames are indicated via `frame.Pi'
\ap\no
Note: 
          Inside the expression may be spaces or not. However, if your
          expression is longer than 100 chars. (the current max. token length) 
          without spaces, you should include spaces between some operands,
          then the limit is 256 chars. for the complete command line.
\\
          The operations +, -, *, / and ** are supported with the same
          precedence as in FORTRAN. Parentheses may be used to change that
          order as well as to nest operations.
\\
          The functions SQRT(a), EXP(a), EXP10(a), LN(a), LOG10(a), SIN(a)
          ASIN(a), COS(a), ACOS(a), TAN(a), ATAN(a), INT(a), ATAN2(a1,a2),
          MAX(a1,a2), MIN(a1,a2), MOD(a1,a2) and ABS(a), are implemented as in
          FORTRAN. Except that INT(a) will return the nearest integer of "a"
          converted to real.
\\
          The trigonometric functions expect arguments in degrees!
\\
          Results of illegal operations (e.g. division by zero) are set to
          the "null value" defined by the keyword NULL.
\\
          If planes from other frames are involved in the expression, they
          must have the same no. of pixels per plane, i.e. NPIX(1),NPIX(2).
          You can mix planes of 3-dim frames and 2-dim frames.
\\
          This command uses single precision (floating point) calculations,
          if you need higher precision, use COMPUTE/DPIXEL instead.
\on\see
See also:
          COMPUTE/XYPLANE, COMPUTE/DPLANE, COMPUTE/ROW, COMPUTE/IMAGE
\\
          TRANSPOSE/CUBE
\ees\exs
Examples:
\ex
          COMPUTE/PLANE tiburon.p2 = P1+P4
            Use image frame `tiburon.bdf' and add the 1st plane and 4th plane,
            store the result into the 2nd plane of frame `tiburon.bdf'.
\xe\ex
          COMPUTE/PLANE ballena.P7 = 99.9 * sin(delfin.p5)
            Set the 7th plane of frame `ballena.bdf' to the product of 99.9
            and the sine of the contents of plane no. 5 of frame `delfin.bdf'.
            The no. of pixels per plane of `ballena.bdf' and `delfin.bdf' must
            be equal.
\xe\ex
          COMPUTE/PLANE ballena.p2 = (1.1+ima2) / p7
            Compute 1.1+ima2, with 2-dim image `ima2.bdf', and divide the
            result by the 7th plane of `ballena.bdf' and store that into 
            2nd plane of frame `ballena.bdf'.
\xe \sxe
