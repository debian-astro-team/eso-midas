% @(#)info_fram.hlq	19.1 (ESO-IPG) 02/25/03 14:03:25 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      info_imag.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, INFO/FRAME
%.PURPOSE    On-line help file for the command: INFO/FRAME
%.VERSION    1.0  11-SEP-2000 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./FRAM
\es\co
INFO/FRAME					11-SEP-2000  KB
\oc\su
INFO/FRAME frame [option]
	get internal info of a frame
\us\pu
Purpose: 
          Get internal info of image/table frame (Midas or FITS format).
\up\sy
Syntax:  
          INFO/FRAME frame [option]
\ys\pa
          frame = name of image/table, maybe in FITS or Midas format;
                  file-type is defaulted to `.bdf'
\ap\pa
          option = DEBUG, show internal Midas file header (FCB) and the
                   corresponding FCT entry;
\\
                 = EXTENS, show all extensions of a FITS file, the primary
                           header does also count as extension;
\\
                   by default `option' is omitted
\ap\sa
See also:
          READ/DESCR, INFO/DESCR, SHOW/DESCR, INDISK/MFITS, INTAPE/FITS
\as\no
Note:  
          With the DEBUG option the command displays in a user readable 
          format the values of the internal File Control Block (FCB) of a
          binary Midas image or table, as well as the FCB created for
          a FITS file if used directly in Midas.
          The FCB (512 bytes) is the very first block of every Midas file.
          Also the FCT (File Control Table) entry of that frame is shown.
\\
          As you might guess, the EXTENS option is only valid for a FITS file;
          the total no. of extensions is also stored in keyword OUTPUTI(19).
          A list of the FITS keywords in the headers is not displayed.
          Note, that FITS tables are implemented via an empty primary FITS
          header followed by a table extension.
\\
          Without any option (the default) and a Midas file, the command shows 
          just the most important values of the file header.
\\
          And the integer keyword MID$INFO is filled as follows:
\\
          If the frame could not be opened MID$INFO(1) = -1, else
\\
          MID$INFO(1) contains the type of the image data, D_R4_FORMAT (10), 
          D_I1_FORMAT(1), ...
\\
          MID$INFO(2) holds the no. of pixels of the image and MID$INFO(3) 
          has the no. of bytes per pixel.
\\
          MID$INFO(4) is set to 1,3 or 4 if the frame is an image, a table
          or a FIT file (from the fitting package, nothing to do with FITS...).
\\
          Without any option and a FITS file, the command shows 
          the primary header and the header of all extensions; then 
          the keyword MID$INFO is NOT filled.
\on\exs
Examples:
\ex
          INFO/FRAME durazno
            Display all relevant FCB values of Midas image `durazno.bdf' and
            fill keyword MID$INFO.
\xe\ex
          INFO/FRAME tst0012.mt extens
            Display all extensions (if any) of FITS file `tst0012.mt'.
            This no. is also stored in OUTPUTI(19).
\xe\ex
          INFO/FRAME fors22.fits
            Display all FITS keywords of FITS file `fors22.fits'.
\xe \sxe
