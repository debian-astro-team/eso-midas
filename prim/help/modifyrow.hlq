% @(#)modifyrow.hlq	19.1 (ESO-IPG) 02/25/03 14:03:31 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      modifyrow.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, MODIFY/ROW
%.PURPOSE    On-line help file for the command: MODIFY/ROW
%.VERSION    1.0  17-OCT-1983 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./ROW
\es\co
MODIFY/ROW						14-APR-1998  KB
\oc\su
MODIFY/ROW  source_def res_frame [row_type] [row_coords]
	approximate values in a row
\us\pu
Purpose:    
          Modify the pixels of one or two adjacent rows of an image by
          using the two rows above + the two rows below as "good" pixels.
\up\sy
Syntax:     
          MODIFY/ROW  source_def res_frame [row_type] [row_coords]
\ys\pa
          source_def = defines how the rows we want to modify will be defined:
\\
              (a) = inframe - use image inframe and param. `row_coords' to
                    define the rows which are to be modified;
\\
              (b) = inframe,intab,collab - use image inframe and the column 
                    labeled collab of table intab to define the row_coords
\\
                    if only inframe,intab is given a column labeled :Y is used;
\\ 
              defaulted to currently displayed image
\ap\pa
          res_frame = output file; defaulted to: result.bdf
\ap\pa
          row_type = type (1 char.) of row replacement;
\\
              = C (constant) if row values just have additional constant offset
                and otherwise contain correct data, the adjacent rows are used
                to determine the correct offset;
\\
              = V (variable) if row values are corrupted, e.g. saturated rows,
                the new row values are obtained by approximation with a 
                least-squares, second order polynomial using the adjacent rows;
\\
              defaulted to C
\ap\pa
          row_coords = y-coordinates of rows, if two adjacent rows are to be
              worked on, the coords of both rows have to be entered;
\\
              (only used when no table involved)
\ap\sa
See also:
          MODIFY/COLUMN, MODIFY/PIXEL, MODIFY/AREA, REPLACE/IMAGE
\as\no
Note:       
          The row_coords (y-coordinates) on the command line may either be
          given as world-coordinates or as pixel nos. preceded by the 
          character "@".
\\
          The row_coords (y-coordinates) taken from a table must be stored
          as world-coordinates.
\\
          For parameter `row_type' any character other than 'C' is interpreted
          as 'V'.
\on\exs
Examples:
\ex
          MODIFY/ROW hector achilles V 53.,@100
            Approximate the row with y=53.0 and row no. 100 of frame
            `hector.bdf' and store results into frame `achilles.bdf'.
            The given rows hold no useful data.
\\
           If yworld-coord 53.0 indicates the 99th row, both adjacent
           rows are replaced in one go, otherwise each of the two rows
           is corrected individually.
\xe\ex
          MODIFY/ROW aguila,nopal vibora C
            Approximate the rows of frame `aguila.bdf' with y-coordinates of
            the rows to be modified stored in the column labeled :Y in table
            `nopal.tbl'. Store result in frame `vibora.bdf'. The given rows 
            hold useful data with an unknown constant offset.
\xe\ex
          MODIFY/ROW aguila,nopal,:y_coord vibora C
            As above but use column labeled :Y_COORD of table `nopal.tbl'.
\xe \sxe
