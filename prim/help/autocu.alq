% @(#)autocu.alq	19.1 (ESO-IPG) 02/25/03 14:02:57 
 
It is possible to execute an interactive Midas application (i.e. an application
waiting for cursor input via a MouseButton click) in "automatic_cursor"
mode:

The INITIALIZE/DISPLAY command has a fifth parameter which can be set to
AUTO_CURSOR. The default for that parameter is NO.

If you do: 
INITIALIZE/DISP p5=auto   

then the cursor coordinates are taken from the ASCII file `cursor.automatic'
located in the directory MID_WORK.


An example of such a file (called also `cursor.automatic') is stored in 
$MIDASHOME/$MIDVERS/test/prim and holds an explanation of the syntax in that
file:

! 
! automatic_cursor_file   `cursor.set'		K. Banse, 970224
! 
! this file holds ENTER + EXIT events for all Midas commands expecting
! cursor input
! 
! records have the form:  event,event_dep_data
! withe event = 0  for EXIT		0,delay
!             = 1  for ENTER		1,xpos,ypos,delay
!             = 2  for key		2,key_value,delay
!             = -1 for switching back to interactive mode
! 
!       xpos,ypos = screen pixel coordinates of cursor
!       key_value = 0, 1, ..., 9 for number keys;
!	 	  = -1, -2, -3, -4 for Arrow Up, Down, Right, Left
!		  = -5 for RETURN
!       delay is optional + given in msecs, defaulted to 1000, i.e. 1 sec
! 
! if the EOF is reached, the file is rewound and read again
! 
! the following data records could be used for the two commands:
! get/cursor
! extract/trace
! 
1,100,100,500
1,200,200
1,300,300
1,400,400,1000
0
! 
1,100,222,0
2,5
2,-3
2,-3
2,-5
2,-2
2,-2
2,-5
1,100,322
0 

Please, note, that 
RESET/DISPLAY clears the auto-cursor option from INIT/DISPLAY,
DELETE/DISPLAY does not.


                           - - -


Besides editing the file `cursor.automatic' by hand, you can also generate such 
an automatic cursor file by recording your cursor input interactively.

For that purpose you switch the display into record_cursor_mode, again via
INITIALIZE/DISPLAY by setting its fifth parameter to RECORD_CURSOR.

If you do:
INITIALIZE/DISP p5=record

then all the cursor related interaction (mouse button press, keyboard input)
are recorded in the ASCII file `cursor.recorded' stored in the directory 
MID_WORK.

To get this file correctly in the end you have to stop your display related
activities via DELETE/DISPLAY or "DELETE/DISPLAY all" NOT by entering 
RESET/DISPLAY !
For RESET/DISPLAY will leave `cursor.recorded' with size of 0 bytes...

After DELETE/DISPLAY change the name of the cursor record file via:
$ cd $MID_WORK
$ mv cursor.recorded cursor.automatic

after that you can use `cursor.automatic' as input file for automatic cursor 
mode.


We add a complete example for replaying a recorded GET/CURSOR command:

$ cd /tmp
$ inmidas
Midas > initialize/display p5=record
Midas > create/display 0
Midas > load/image lola
Midas > get/cursor

...  usual interactive cursor input terminated by EXIT ...

Midas > delete/display
Midas > bye

$ cd $MID_WORK
$ mv cursor.recorded cursor.automatic
$ cd /tmp
$ gomidas
Midas > initialize/display p5=auto
Midas > create/display 0
Midas > load/image lola
Midas > get/cursor

...  no interactive input,
     the previously recorded cursor actions are repeated until EXIT ...

Midas > reset/display
Midas > initialize/display

... continue in the usual interactive way ...



