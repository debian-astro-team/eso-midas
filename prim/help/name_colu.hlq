%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2010 European Southern Observatory
%.IDENT      name_colu.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, NAME/COLUMN
%.PURPOSE    On-line help file for the command: NAME/COLUMN
%.VERSION    1.0  12-OCT-1983 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./COLUMN
\es\co
NAME/COLUMN						18-AUG-2010  JDP + FO
\oc\su
NAME/COLUMN table column [new-column] [unit] [format]
	redefines label/unit/format of a column
\us\pu
Purpose:    
            Redefines the label, unit or format of table columns.
\up\sy
Syntax:     NAME/COLUMN table column [new-column] [unit] [format]
\ys\pa
            table = the table name
\ap\pa
            column = the column reference
\ap\pa
            new-column = the optional new label for the column,
		max. 16 chars including the ':'
\ap\pa
            unit = optional units included in double quotation marks,
		max. 16 chars; blanks are used by default
\ap\pa
            format = format associated to the column according to the
                FORTRAN-77 rules with some extensions. The format 
                is used by default when the table is displayed 
                (commands READ/TABLE and PRINT/TABLE). 
                Possible formats are : 
                for characters: Aww   
                for integers:   Iww    iww     
                                Xww    Oww      for hexa / octal
                                Tww.dd tww.dd   for Date+Time
                                                (seconds since 1970)

                for floating:   Fww.dd fww.dd   
                                Eww.dd eww.dd  
                                Gww.dd 
                                Rww.dd rww.dd   for Right Ascensions
                                Sww.dd sww.dd   for Sexagesimal (decl.)
                                Tww.dd tww.dd   for Date+Time (JD)
                                Zww.dd zww.dd   zero-filled
\ap\see
See also:
            CREATE/COLUMN
\ees\no
Note:      
            It is not possible to CHANGE the TYPE of a column.
            However the command COPY/TT will allow you to transform a column
            of a certain type into a column of an another type.
\on\exs
Examples:
\ex
            NAME/COLUMN mytable #2 :RADVEL "km.s-1" E12.3
              Use label RADVEL for 2nd column of the table `mytable.tbl',
              units will be km.s-1 and format E12.3 .
\xe\ex
            NAME/COLUMN mytable :RADVEL :VELOCITY
              Change label of column labeled RADVEL of table `mytable.tbl'
            to VELOCITY.
\xe \sxe
