% @(#)restorname.hlq	19.1 (ESO-IPG) 02/25/03 14:03:41 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      restorname.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, RESTORE/NAME
%.PURPOSE    On-line help file for the command: RESTORE/NAME
%.VERSION    1.0  24-SEP-1992 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./NAME
\es\co
RESTORE/NAME						04-NOV-1994  KB
\oc\su
RESTORE/NAME [file_spec] [verbose] [history] [overwrite] [descr]
	change file name according to descr. FILENAME
\us\pu
Purpose: 
	  Change file name to the name stored in descr. FILENAME
\up\su
Subject:  
          Images, tables, fit files, catalogs
\us\sy
Syntax:   
          RESTORE/NAME [file_spec] [verbose] [history] [overwrite] [descr]
\ys\pa
          file_spec = name of single file or catalog name (with type `.cat');
\\
               defaulted to currently active image catalog
\ap\pa
          verbose = YES or NO, to display new name for each file as well as
               error messages or comments;
\\
               defaulted to YES
\ap\pa
          history = flag for update of HISTORY descriptor;
\\
              if NO, the history descriptor of the new frame is not updated;
\\
              defaulted to NO
\ap\pa
          overwrite = flag for overwrite check;
\\
              if CONFIRM, we check, if a file with the new name already exists
              and if so, ask for confirmation before overwriting it;
\\
              defaulted to NO_CONFIRM
\ap\pa
          descr = name of character descriptor which should be used, if the
              descr. FILENAME does not exist for an input file;
\\
              defaulted to IDENT
\ap\sa
See also: 
          INTAPE/FITS, INDISK/FITS, CREATE/ICAT, READ/DESCR, RENAME/IMAGE
\as\no
Note: 
          The parameters may also be referenced via:
\\
          FILE_SPEC=  VERBOSE=  HISTORY=  OVERWRITE=  DESCR= 
\\
          Images, tables and fit files are supported as well as catalogs
          of these.
\\
          This command is especially useful after the INTAPE/FITS command
          in order to get the original filenames back.
\on\exs
Examples:
\ex
          RESTORE/NAME 
           Use the currently active image catalog. For each image frame in that
           catalog look for the char. descriptor FILENAME. If found, rename the
           frame to the content of FILENAME.
           Display new name for each image.
\xe\ex
          RESTORE/NAME dosequis.bdf
           If the image `dosequis.bdf' has a char. descriptor FILENAME
           containing e.g. the string `cartablanca.bdf' it will be renamed
           to cartablanca.bdf .
\xe\ex
          RESTORE/NAME tecate.cerveza no ? conf
           Rename file `tecate.cerveza' to the content of descriptor FILENAME,
           e.g. `paulaner.bier'.
           If a file named `paulaner.bier' already exists the user is asked
           for confirmation before actually renaming the input file.
           The file type, e.g. table, will be determined and any active table
           catalog is updated.
           The new name of `tecate.cerveza' is not displayed.
\xe\ex
          RESTORE/NAME bohemia.cat descr=fitname
           Rename all files in catalog bohemia. The actual type of the catalog,
           e.g. fit file catalog, will be determined and all fit files with
           entries in `bohemia.cat' will be renamed according to the content
           of descriptor FILENAME. If the descr. FILENAME does not exist for
           one of the input fit files, we try to use descr. FITNAME instead.
\xe\sxe
