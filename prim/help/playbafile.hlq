% @(#)playbafile.hlq	19.1 (ESO-IPG) 02/25/03 14:03:34 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      playbafile.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, PLAYBACK/FILE
%.PURPOSE    On-line help file for the command: PLAYBACK/FILE
%.VERSION    1.0  02-AUG-1985 : Creation, KB
%.VERSION    1.1  02-OCT-1991 : Update, KB
%.VERSION    1.2  23-DEC-1993 : Add qualifier FILE, KB
%----------------------------------------------------------------
\noqualifier
\se
SECTION./FILE
\es\co
PLAYBACK/FILE						23-DEC-1993  KB
\oc\su
PLAYBACK/FILE name
	playback MIDAS commands from an ASCII file
\us\pu
Purpose:
          Playback MIDAS commands from an ASCII file.
\up\sy
Syntax: 
          PLAYBACK/FILE name
\ys\pa
          name = name of ASCII file containing MIDAS commands;
\\
               file type ".log" is appended, if no file type explicitely given
\ap\sa
See also:
          PLAYBACK/LOGFILE
\as\no
Note:  
          PLAYBACK/FILE can be only entered interactively!
\\
          Nesting of playback files is not possible (i.e. you cannot playback
          a file which contains itself the command PLAYBACK).
\\
          Do not mix up the command PLAYBACK/FILE with @, @@:
\\
          PLAYBACK/FILE reads a file line by line and feeds them to MIDAS just
          like terminal input. That's why a playback file can only contain
          interactive MIDAS commands, no commands like IF or GOTO are possible.
\on\exs
Examples:
\ex
          PLAYBACK/FILE MID_WORK:myfile.dat
            Feed the lines contained in the ASCII file `myfile.dat' located
            in MID_WORK as input to MIDAS.
\xe \sxe
