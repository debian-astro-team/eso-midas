%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  2010-2011 European Southern Observatory
%.IDENT      copy_t3im.hlq
%.AUTHOR     KB
%.KEYWORDS   MIDAS, help files, 3D-table, image, conversion
%.PURPOSE    On-line help file for the command: COPY/IMT3
%.VERSION    1.0  100830
%----------------------------------------------------------------
\se
SECTION./IT3
\es\co
COPY/IMT3						05-DEC-2011  KB
\oc\su
COPY/IMT3 image table [column,row] [1st_elem,noelem] [option]
	Convert a MIDAS image into an array column of a 3D-table.
\us\pu
Purpose:
	   Convert a MIDAS image into an array column of a 3D-table.
\up\sy
Syntax: 
	   COPY/IMT3 image table [column,row] [1st_elem,noelem] [option]
\ys\pa
           image = name of image file to be copied/converted
		for 1-dim images, one row will be filled;
		for 2-dim images, y-pix rows will be filled
\ap\pa
           table = name of new (or existing) 3D-table
\ap\pa
           column,row = column ref. and starting row no.;
		defaulted to :Values,@1
\ap\pa
           1st_elem,noelem = 1st element and no. of elements
		to write in array column;
		default is 1st element and no. of x-pixels of image,
		i.e. = 1,npix[0]
\ap\pa
           option = update_flag (a) or no. of rows to allocate (b);
		(a) = UPDATE for updating an existing table;
		    = NO for creating a new table;
		(b) = no. of rows to allocate for new table; 
		defaulted to NO
\ap\pa
\ap\sa
See also:
           COPY/T3IMA, COPT/T3TAB, COPY/TEIMA, COPY/TI
\as\no
Note:
              Always complete lines of the input image, starting with the
	   1st pixel are taken as input buffer. The parameter 'noelem'
	   then limits the no. of pixels used.
	   Therefore, if you want to copy only a part of an image,
	   this subimage has to be extracted, first, and then used as
	   input for COPY/IMT3.
              The type of the image will be D_I4_FORMAT for all columns
           with data of type I1, I2, or I4. 
	      The copy command will be logged and appended to descriptor 
	   HISTORY of the table.
\on\exs
Examples:
\ex
           COPY/IMT3 conejo coyote 
             Create table `coyote.tbl' and fill array column :Values
	     with data from image `conejo.bdf'. The elements of this column 
	     will have the size of the image line (npix[0]). If it's a 1-dim 
	     image, only row no. 1 of the table will filled, for a 2-dim image
	     y-pix (npix[1]) rows will be created and filled.
\xe\ex
           COPY/IMT3 conejo coyote ? ? 200
	     As above, but allocate at least 200 rows, even if npix[1]
	     of `conejo.bdf' is < 200.
\xe\ex
           COPY/IMT3 conejo coyote ? 2, update
	      Update column :Values of `coyote.tbl' and fill the array
	      starting with the 2nd element of the column in row 1.
	      For 2-dim input image npix[1] rows will be filled, unless
	      this would exceed the no. of allocated rows of `coyote.tbl',
	      in that case the no. of rows to fill is truncated.
\xe\ex
           COPY/IMT3 conejo coyote :sierra,@2 
             As above, but create a column labeled :sierra and start with
	     the 2nd row. 
             OJO: The 1st row of the new table will be also selected but 
	     filled with Null value!.
\xe\ex
           COPY/IMT3 conejo puma :sierra,@3 ? UPDA
	      Update column :sierra of `puma.tbl' and add a 3rd row, filled
	      from `conejo.bdf' (if at least 3 rows are allocated).
\xe \sxe
