% @(#)add_tcat.hlq	19.1 (ESO-IPG) 02/25/03 14:02:55 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      add_tcat.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, ADD/TCAT
%.PURPOSE    On-line help file for the command: ADD/TCAT
%.VERSION    1.0  21-DEC-1989 : Creation, KB
%.VERSION    1.1  20-JUN-1991 : Update, KB
%----------------------------------------------------------------
\se
SECTION./TCAT
\es\co
ADD/TCAT					 25-SEP-1992  KB
\oc\su
ADD/TCAT [cat_name] table_list [lowstr,histr]
	add entries to a table catalog
\us\pu
Purpose: 
          Add entries to a table catalog
\up\su
Subject:  
          Tables, catalogs
\us\sy
Syntax:   
          ADD/TCAT [cat_name] table_list [lowstr,histr]
\ys\pa
          cat_name = name of table catalog; defaulted to currently active
               table catalog
\ap\pa
          table_list = list of tables to be added,
\\
               maybe file specifications, separated by a comma (no spaces!);
\\
               or a single catalog name;
\\
               or wildcard specifications (e.g. a3*,n*.tbl)
\ap\pa
          lowstr,histr = optional low and high strings;
\\
          if given, only tables with names >= `low' and <= `high' will be added
\ap\no
Note:
          The different options for the frame_list may not be mixed!
\\
          Entries are added automatically to the currently "active" table
          catalog (SET/TCAT command makes a catalog active).
\on\see
See also: 
          SET/TCAT, CREATE/TCAT, CLEAR/TCAT, READ/TCAT, SUBTRACT/TCAT
\\
          ADD/ICAT, ADD/FCAT
\ees\exs
Examples:
\ex
          ADD/TCAT dec89 coords
            Add an entry for table file `coords.tbl' to  table catalog 
            `dec89.cat'.
\xe\ex
          ADD/TCAT dec88 gal*
            Add entries for all tables where the names begin with the
            string "gal" to catalog `dec88.cat'.
\xe\ex
          ADD/TCAT dec88 d* demo0025.tbl,demo0036.tbl
            Add entries for tables with names `demo0025.tbl' -> `demo0036.tbl'
            to catalog `dec88.cat'.
\xe\ex
          ADD/TCAT dec88 nov89.cat
            Add entries for all tables which are in the table catalog 
            `nov89.cat' to table catalog `dec88.cat'.
\xe \sxe
