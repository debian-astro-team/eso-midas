% @(#)modifydisp.hlq	19.1 (ESO-IPG) 02/25/03 14:03:30 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      modifydisp.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, MODIFY/DISPLAY
%.PURPOSE    On-line help file for the command: MODIFY/DISPLAY
%.VERSION    1.0  27-JUN-1996 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./DISP
\es\co
MODIFY/DISPLAY					19-OCT-2000  KB
\oc\su
MODIFY/DISPLAY [option]
	modify the state of a display window (e.g. mapped Window <-> Icon)
\us\pu
Purpose:
          Modify the state of a display window, e.g. from mapped Window to
          Icon and vice versa.
\up\sy
Syntax:
          MODIFY/DISPLAY [option]
\ys\pa
          option = ICON or WINDOW (a)
                   PARENT or PARENT=ROOT (b)
              (a) for ICON the active display window is put into an Icon;
                  for WINDOW the iconized display window is displayed on 
		  the screen again;
              (b) the active display window will serve as parent window 
		  for all subsequently created windows (display + graphics) 
                  until the root window of the screen will become the global
		  parent window again - via PARENT=ROOT
              defaulted to ICON
\ap\sa
See also:
          INITIALIZE/DISPLAY, CREATE/DISPLAY, SHOW/DISPLAY, MODIFY/GRAPHICS
\as\no
Note: 
             The command works on the currently active display window.
          E. g., putting a window into an Icon state does not automatically
          assign another display window (if present on the screen) to be the 
          active one. That would have to be done explicitely by the user.
          Except interactive commands which need the mouse all other display
          related commands (like LOAD/IMAGE) can still be be applied to an
          iconized display window.
          OJO: Some window managers (e.g. Fvwm) have problems to display an
          iconized window again on the screen, if that window was not placed
          completely inside the screen before (including title bar).
 
             By default the screen of your workstation (PC) serves as parent
          window for all windows, i.e. all offsets are relative to that
          display area. Remember, that in Midas the origin (0,0) is the lower
          left corner and y increasing upwards, whereas in X11 (0,0) is the
          upper left corner and y increases downwards.
          With the option PARENT you set up the currently active display 
          window as parent window (PW) for all subsequent display/graphics
          windows. Thus their offsets are relative to that parent window and
	  also their size will be clipped relative to the size of the PW.
          Moving the PW also moves all its subwindows.
          Use the option PARENT=ROOT to reset the root window, i.e. the full
          screen, as PW. AFter that you could again create a new display and
          set that one up as the next parent window.
\on\exs
Examples:
\ex
          MODIFY/DISPLAY Icon
            Put the current display window into the Icon window of the Window
            manager.
\xe \ex
          MODIFY/DISPLAY Window
            Display an iconized window again on the screen.
\xe \ex
          MODIFY/DISPLAY Parent
            All following display/graphics windows will be subwindows of the
            currently active display window.
\xe \ex
          MODIFY/DISPLAY Parent=root
            All display/graphics windows can be placed again anywhere on 
            the screen.
\xe \se
