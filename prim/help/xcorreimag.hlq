% @(#)xcorreimag.hlq	19.1 (ESO-IPG) 02/25/03 14:03:51 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      xcorreimag.hlq
%.AUTHOR     MR,KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, XCORRELATE/IMAGE
%.PURPOSE    On-line help file for the command: XCORRELATE/IMAGE
%.VERSION    1.0  08-MAY-1989 : Creation, MR,KB
%.VERSION    1.1  08-JAN-2002 : Update by H.C.Thomas (MPA, Garching)
%----------------------------------------------------------------
\se
SECTION./IMAG
\es\co
XCORRELATE/IMAGE					08-JAN-2002  MR,KB,CT
\oc\su
XCORRELATE/IMAGE temp spec result shift norm_flag
	correlate 2 similar 1-dim frames over 2*(shift) bandwith
\us\pu
Purpose:  
           Correlate 2 similar 1-dim frames over 2*(shift)+1 bandwith.
\up\sy
Syntax:     
           XCORRELATE/IMAGE temp spec result [shift] [norm_flag]
\ys\pa
           temp = template frame
\ap\pa
           spec = frame to be analyzed
\ap\pa
           result = name of output frame 
\ap\pa
           shift = 1/2 bandwith in integer pixels; defaulted to 10
\ap\pa
           norm_flag = YES or NO for normalizing each pixel of the
		output frame; defaulted to NO (to be backwards compatible)
\ap\sa
See also:
           CONVOLVE/IMAGE, FFT/IMAGE, FFT/FREQUENCY
\as\no
Note: 
           The template frame should have at least (2*shift)+5 pixels.
           Identical STEP size (in the first dimension) of the template
           and analyzed frame is required.
           If "norm_flag" is set, the 1-dim result frame will contain the
	   correlation coefficient between template and analyzed frame as
           a function of the shift applied to the latter. 
           It will have (2*shift)+1 pixels and the
           start will be -shift * stepsize of the analyzed frame.
           If any input frame is a 2-dim image the first row of that frame
           is used for the correlation.
\on\exs
Examples:
\ex
           XCORRELATE/IMAGE anaconda cobra notoca 15
             Correlate image `cobra.bdf'  with template `anaconda.bdf' over a
             total of 31 pixels, results are in image `notoca.bdf'.
\xe \ex
           XCORRELATE/IMAGE anaconda cobra notoca 15 yes
             As above, but the pixels of `notoca.bdf' are normalized.
\xe \sxe
