%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      show_comm.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, SHOW/COMMAND
%.PURPOSE    On-line help file for the command: SHOW/COMMAND
%.VERSION    1.0  31-JAN-1984 : Creation, KB
%.VERSION    1.2  12-MAY-2004 : Update, KB
%----------------------------------------------------------------
\se
SECTION./COMM
\es\co
SHOW/COMMAND					12-MAY-2004  KB
\oc\su
SHOW/COMMAND [comnd/qualif]
	display MIDAS\ commands
\us\pu
Purpose: 
          Show the actual procedure which implements a MIDAS command.
\up\sy
Syntax:  
          SHOW/COMMAND [comnd/qualif]
\ys\pa
          comnd/qualif = command/qualifier combination of a MIDAS command;
                       if omitted, all user defined commands are displayed
\ap\sa
See also:
          SHOW/CODE, HELP command/qualif, HELP/QUALIF
\as\no
Note: 
          Also the default qualifier for the given command is shown as well
          as the default flag, which is 1 or 0 depending upon if the command
          CREATE/DEFAULTS had been used for that command to enforce user
          specific default values.
             In the case of an explicit `comnd/qualif' the command sets the
          integer keyword OUTPUTI(9) to 0 or -1 depending upon if 
          `comnd/qualif' exists or not.
             If instead of a command/qualifier you enter -D (for diagnostics)
          the internal structures of the command database and their size
          are displayed.
             If instead of a command/qualifier you enter -X only the size of
	  the internal structures of the command database are displayed.
\on\exs
Examples:
\ex
          SHOW/COMM LOAD/IMA
          Display relevant info for MIDAS command LOAD/IMAGE.
\xe\ex
          SHOW/COMM
          Display all user defined commands.
\xe\ex
          SHOW/COMM -x
          Display size and usage of internal command database.
\xe \sxe
