#! /bin/sh
# @(#)onemlc.sh	19.1 (ESO-IPG) 02/25/03 14:03:31
# 
#  Bourne shell script "onemlc.sh"
#  to build single .mlc file from a group of .mlq files
#  901219  KB
#  to use it e.g. for the copy command, do  sh onemlc.sh entry
# 
touch $1.mlc
rm $1.mlc
#
ls $1*.mlq |  sed  's/_.*/ /' | sort -u | sed '/_/d' > temp0
# 
cat temp0 | sed 's/\.mlq//' | awk '{print substr($1,1,6)}' | sort -u > temp1
#
for i in `cat temp1`
do
   for file in `ls $i*.mlq`
   do
      sed  -n '/SECTION./,/\\us/p' $file | sed '/^\\/d' | sed '2d' >> $i.mlc
   done
done
