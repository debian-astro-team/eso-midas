%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      get_curs.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, GET/CURSOR
%.PURPOSE    On-line help file for the command: GET/CURSOR
%.VERSION    1.0  12-JUL-1990 : Creation, KB
% 041129	last modif
%----------------------------------------------------------------
\se
SECTION./CURS
\es\co
GET/CURSOR					12-FEB-2003  KB
\oc\su
GET/CURSOR [output] [option] [marker] [curs_specs] [zw_option] 
	read cursor coords from display
\us\pu
Purpose:  
           Read cursor coordinates from image display system.
\up\sy
Syntax: 
           GET/CURSOR [output] [option] [marker] [curs_specs] [zw_option]
\ys\pa
           output = specification for storage of world coordinates and pixel
              values, may be:
              table name (a),
              descriptor name (b), specified as descr_name,DESCR
              omitted (i.e. = ?) (c), data is only displayed on the terminal;
              defaulted to ? 
\ap\pa
           option = append_flag for descriptors;
               A => data values will be appended to the end of the descriptor,
                    else data is written to beginning of the descriptor - this
                    is the default;
                    append_flag+id_flag or no_flag for tables;
               A => data values will be appended to the end of the table, 
                    else a new table is created - this is the default;
               ID => for each cursor input the user is prompted for an
                     identification (limited to 8 chars.);
               NO or NO,start_no => enable automatic numbering of coordinate
                     reading, starting at start_no;
                     this number is stored in a column labeled :No ;
              defaulted to nothing of the above (see also the Notes)
\ap\pa
           marker = two-character flag for marking position in the overlay;
              if first character = Y, a cross (rectangle) will be drawn in the
              overlay channel at the current cursor position(s);
              if second character = Y, the identifier or sequence number will
              also be written to the overlay channel, only applicable for
              option ID and NO described above;
              defaulted to YN
\ap\pa
           curs_specs = max number of coordinate readings and no. of cursors;
              if set to 0,0 (or just 0) the cursor coordinates will be read 
              out continuously and world coords. and intensity displayed;
              defaulted to 9999,1
\ap\pa
           zw_option = zoom_window_flag,zoom;
              zoom_window_flag = W for zoom_window, N for none,
              zoom = initial zoom factor;
              defaulted to N,4;
\ap\sa
See also:
           CREATE/CURSOR, SET/CURSOR, VIEW/IMAGE, EXTRACT/CURSOR, GET/GCURSOR
\as\no
Note: 
              If the output goes to a table the columns :X_coord,:Y_coord,
           :Value,:X_coordpix and :Y_coordpix will be created, if only one 
           cursor involved.
              If both cursors are involved, then for a rectangle the columns 
           :Xstart,:Xend, :Ystart,:Yend,:Value1, :Value2, :Xstartpix,
           :Ystartpix, :Xendpix and :Yendpix will be created;
           and for a circle the columns :X_coord,:Y_coord,:Value,:Radius1,
           :X_coordpix and :Y_coordpix will be created.
              If option = NO or NO,number a column labeled :No is also created,
           else a column :Ident is created which will contain an automatic
           identification (IDxyz) or if option = ID the user is prompted each
           time for an identification.
              The append_flag and either id_ or no_flag can be combined via 
           the `+' char., e.g. A+NO,2100 to append to the input table and 
	   fill the column :No with sequence numbers beginning at 2100.
              If the output goes to a descriptor the world coords of x,y and
           the intensity are stored for a single cursor. For two cursors,
           the world coords of x,y and intensity of both cursors are stored,
           if a rectangle is used; and the world coords of x,y and intensity
           of the center pixel as weel as the radius are stored, if a circle
           is used.
              Use the Mouse to move the cursor; the leftmost Mouse button or 
	   the RETURN key serve as ENTER button; the right Mouse button serves
	   as EXIT button.
              This command also stores last obtained cursor pixels in keyword
           CURSOR(1,..,4); the last obtained coordinates in keyword
           OUTPUTR(10...) and total no. of coords. read in OUTPUTI(1).
              If `curs_specs' is set to `xxx,1' (which is the default) a single
           cursor is used, irrespective of any previous SET/CURSOR command
           which might have initialized a cursor rectangle!
              If `curs_specs' is set to 0, i.e. continuous cursor read-out is
           done, always a single cursor is used.
              Normally, the overlay channel is not cleared at startup of the
           command; if you want to clear first, set keyword AUX_MODE(9) = 1.
              If we use the zoom_window mode, use first the cursor to choose a
           region in the main window. Press ENTER to copy the indicated
           rectangle to the zoom window. Move the cursor into this window and
           get the cursor values. To choose another region, press EXIT in the
           zoom window, move the cursor back to the main window and choose
           another region. To exit, press EXIT in the main window.
	   A separate terminal window is used for the additional output
	   provided with that option.
              Options exist for changing the zoom factor, using color LUTs,
           different ITTs and scrolling (like in VIEW/IMAGE), which can be
           activated by typing a single key (with the cursor in the main
           display window), type 'h' for help on that.
              If a cursor window exists (cf. CREATE/CURSOR) an area of
           9x9 pixels around the cursor #0 (i.e. the mouse) is shown in
	   the cursor window zoomed up by a factor of 20.
\on\exs
Examples:
\ex
           GET/CURSOR
             Display cursor data on terminal only.
\xe\ex
           GET/CURSOR coords,descr ? N
             Also  store world coordinates and intensity into real descriptor
             COORDS of displayed frame. 
             Inhibit drawing of cross at cursor position.
\xe\ex
           GET/CURSOR coords
             Create new table `coords.tbl', use single cursor and store data
             into that table.
\xe\ex
           GET/CURSOR coords ? ? 100,2
             Create new table `coords.tbl', use cursor rectangle and store 
             max. 100 data into that table.
\xe\ex
           GET/CURSOR coords A
             Append data to existing table `coords.tbl'.
\xe\ex
           GET/CURSOR coords ID
             Create new table, do not use automatic, sequential identifiers, 
             but get them from the user.
\xe\ex
           GET/CURSOR coords A+ID
             As above, but append to existing tabel `coords.tbl' .
\xe\ex
           GET/CURSOR coords A+ID YY
             Also draw the identifier into the overlay channel.
\xe\ex
           GET/CURSOR p4=0
             When the cursor is inside the image display window read out the
             cursor position continuously and display data on terminal.
\xe\ex
           GET/CURSOR p5=w
             Use auxiliary zoom window to get the cursor coordinates.
             If the zoom window does not exist yet, it will be created with
             half the size in x and y of main display window.
\xe\ex
           GET/CURSOR p4=0 p5=w 
             With the cursor choose a region inside the main image display
             window. Then inside the zoom window read out the cursor position
             continuously and display data on terminal.
\xe\sxe
