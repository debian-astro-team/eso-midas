% @(#)sort_tabl.hlq	19.1 (ESO-IPG) 02/25/03 14:03:46 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      sort_tabl.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, SORT/TABLE
%.PURPOSE    On-line help file for the command: SORT/TABLE
%.VERSION    1.0  12-Oct-1983 : Creation, JDP
%.VERSION    1.1  04-Mar-1991 : New Table Version, FO
%----------------------------------------------------------------
\se
SECTION./TABL
\es\co
SORT/TABLE						12-Oct-1983  JDP + FO
\oc\su
SORT/TABLE table keys
	sort table according to (ascending) values 
\us\pu
Purpose:    
            Sort table according to a combination of columns
\up\sy
Syntax:     
            SORT/TABLE table sort_keys
\ys\pa
            table      = name of table file
\ap\pa
            sort_keys  = sequence of columns to be used as sorting parameters,
                    separated by commas; each column name may be followed by
                    by (-) (a minus sign within brackets) for a descending 
                    sorting order.
\ap\no
Note:       
            If the sort_keys consist of columns which have different types,
	    the command may not work, e.g. in certain cases the combination 
	    of character and double columns might cause problems.
\on\exs
Examples:
\ex
            SORT/TABLE mytable :HD_NUMBER
            This would sort all the table columns in ascending of
            values stored in the column labelled :HD_NUMBER.
\xe\ex
            SORT/TABLE mytable :ra,:dec(-)
            This would sort the table in ascending order of values 
            stored in the column labelled :ra, and in case of identical 
            :ra values, the order is the decreasing values of the 
            :dec column.
\xe\sxe
