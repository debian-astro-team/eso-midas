% @(#)shift_imag.hlq	19.1 (ESO-IPG) 02/25/03 14:03:44 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      shift_imag.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, SHIFT/IMAGE
%.PURPOSE    On-line help file for the command: SHIFT/IMAGE
%.VERSION    1.0  02-OCT-1986 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./IMAG
\es\co
SHIFT/IMAGE						16-JAN-1991  KB
\oc\su
SHIFT/IMAGE inframe outframe [x,yshift]
	shift the pixels in an image
\us\pu
Purpose:    
            Shift the pixels in an image in x and y-direction.
\up\sy
Syntax:       
            SHIFT/IMAGE inframe outframe [x,yshift]
\ys\pa
            inframe = name of input image
\ap\pa
            outframe = name of output (shifted) image
\ap\pa
            x,yshift = no. of pixels to shift in x and in y-direction;
\\
                 defaulted to 1,0 (one pixel in x, no pixel in y)
\ap\sa
See also:
            FLIP/IMAGE
\as\no
Note:       
            Pixels (lines) are shifted right (towards the high end) and wrapped
            around. To shift left use negative shifts.
\on\exs
Examples:
\ex
            SHIFT/IMAGE mata hari 0,12
              Shift the lines of frame `mata.bdf' 12 lines up, the 12 top lines
              will be shifted to the bottom. The result is stored in the frame
              `hari.bdf' .
\xe \sxe
