%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2009 European Southern Observatory
%.IDENT      filtersmoo.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, FILTER/SMOOTH
%.PURPOSE    On-line help file for the command: FILTER/SMOOTH
%.VERSION    1.0  09-FEB-1990 : Creation, KB
%.VERSION    1.1  31-MAY-1991 : Remove edge comments, KB
%----------------------------------------------------------------
\se
SECTION./SMOO
\es\co
FILTER/SMOOTH					20-AUG-2009  KB
\oc\su
FILTER/SMOOTH frame outfram [filter_specs] [flag] [subima] [options]
	smooth an image by averaging
\us\pu
Purpose: 
            Apply a filter using the running average.
\up\sub
Subject: 
            Box average, running mean.
\bus\sy
Syntax: 
            FILTER/SMOOTH frame outfram [filter_specs] [flag] [subima] [options]
\ys\pa
            frame = input frame
\ap\pa
            outfram = result  frame
\ap\pa
            filter_specs = radx,rady,threshold
                 or
                 radx,rady,intens_low,intens_hi;
                 radx, rady is the radius in x and y, i.e. the number of pixels
                 "around" the central pixel which define the neighbourhood
                 (NBH) of that pixel,
                 total no. of pixels in NBH is then: (2*radx+1)*(2*rady+1);
                 threshold is in [0.,1.] (if relative) or >= 0. (if absolute);
                 intens_low, hi is the low intensity, high intensity for the
                 fixed interval option (flag(2) = 'F'),
                 < and > may be used to indicate lowest and highest value.
                 Default: 1,1,0.0
\ap\pa
            flag = two character flag,
                 (1) Y or N, to indicate inclusion of central pixel in the
                     smoothing over the neighbourhood,
                 (2) A for absolute threshold,
                     R for relative threshold using pixel value,
                     Q for relative threshold using smoothed value,
                     F for fixed intensity interval, Default: YR
\ap\pa
           subima = optional specification of a subimage on which the filter
                 will be applied, may be either [xstart,ystart:xend,yend]
                 (see page 3.4 in the MIDAS User manual, chapter 3) to indicate
                 the limits of the subimage (with coordinates given according
                 to the MIDAS standard), or
                 CURSOR if subimage will be chosen interactively, or
                 table_name if the table "table_name" holds the endpoints of
                 the subimage(s) in columns labeled :XSTART, :YSTART, :XEND
                 and :YEND,
                 defaulted to the complete input frame.
\ap\pa
            options = flag of 3 characters for options;
                 (1) L or N for loop or no loop,
                 (2) D or N for display of smoothed area or no display;
                 (3) E or N for expanding input frame first, so that result
                     frame has same dimensions as input frame or no expansion
                     with result frame smaller than input
                 defaulted to NNE
\ap\sa:
See also:
            FILTER/MAX, FILTER/MIN, FILTER/GAUSS, FILTER/DIGITAL,
            FILTER/MEDIAN, TUTORIAL/FILTER,
            FFT/IMAGE, CONVOLVE/IMAGE, DECONVOLVE/IMAGE
\as\no
Note:
            Smooth the image by
            1) calculating A(x,y), the average of all points in the
               neighbourhood of (x,y), which contains all points (x',y') with
               abs(x-x') .LE. radx   and   abs(y-y') .LE. rady
               (radx, rady as given in the 3rd parameter)
               So radx = 1 and rady = 1 yields the usual 3*3 neighbourhood.
            2) replacing the original value I(x,y) by A(x,y), if we have for
               ABS = abs(A(x,y)-I(x,y)):
               ABS > thresh (flag(2) = A),
               ABS > thresh*I(x,y) (flag(2) = R),
               ABS > thresh*A(x,y) (flag(2) = Q),
               or if I(x,y) in [intens_low,intens_hi] (flag(2) = F),
               (thresh and intens_low, intens_hi as given in the 3rd parameter)
            The values radx and rady do not have to be equal, by setting
            radx or rady to 0, also 1-dimensional filters may be used.
	    But note, that radx, rady should be integers. No fraction of
	    pixels are supported, the nearest integer of radx, rady
	    would be chosen, instead.
            If the subimage option is chosen, it must be at least as big as the
            filter kernel which has  (2*radx+1) * (2*rady+1) pixels.
            If `option(3:3) = 'E' the input image is expanded by 2*radx columns
            and 2*rady lines, so that also the "edge" pixels can be calculated.
            Note, that the flux is not conserved.
 
	    For 1-dim files one still has to enter rady, even though it
	    is ignored later on.
	    For 3-dim files the 2-dim filter is executed over all planes.
\on\exs
Examples:
\ex
            FILTER/SMOOTH gordo flaco 1,1,0.5
             Smooth image `gordo.bdf', calculate average over a 3*3 window
             around each pixel and replace the original value by the average
             value only if average value differs more than 0.5*original value
             from the original value.
\xe\ex
            FILTER/SMOOTH gordo flaco 1,1,0.5 YA CURSOR
             Smooth image `gordo.bdf', average over a 3*3 neighbourhood and
             replace the original value by the average value only, if average
             value differs more than 0.5*original value from the original value
             but apply the averaging process only to subimages of `gordo.bdf',
             which will be chosen via the cursor.
             It is assumed, that image `gordo.bdf' is loaded into the image 
             display! 
\xe\ex
            FILTER/SMOOTH gordo flaco 1,1,0.5 YA coords
             As above, but the subimage specifications are taken from
             the table `coords.tbl'. This table could e.g. be created by using
             the command GET/CURSOR with the table option (and 2 cursors!)
\xe\ex
            FILTER/SMO gordo flaco 3,0,0. NA [@20,<:@50,>]
             Smooth image `gordo.bdf' by averaging over a 7*1 neighbourhood;
             do not include the central value in the averaging and always
             replace the original value by the average value.
             Apply the filter only to the subimage of frame `gordo.bdf',
             which is defined by the coordinates given as  parameter 5.
\xe\ex
            FILTER/SMOOTH gordo flaco 3,2,22.3,24.6 NF ? nnn
             Apply boxcar filter only to pixels which have an intensity in the
             interval [22.3,24.6]. Do not include the central pixel in the
             calculation of the average value.
             Frame `flaco.bdf' will have xdim = NPX-6 and ydim = NPY-4, with
             NPX, NPY the no. of pixels in x,y of the input frame `gordo.bdf'.
\xe\ex
            FILTER/SMOOTH salvaje docil 103,1,0.
             Assuming that `salvaje.bdf' is a 1-dim spectrum, apply filter by 
	     averaging over 103 x-pixels, always replace original intensity 
	     by the running avaerage. Rady = 1 must be given, but is ignored 
	     in the processing.
\xe \sxe
