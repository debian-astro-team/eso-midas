% @(#)load_lut.hlq	19.1 (ESO-IPG) 02/25/03 14:03:28 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      load_lut.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, LOAD/LUT
%.PURPOSE    On-line help file for the command: LOAD/LUT
%.VERSION    1.0  04-MAR-1988 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./LUT
\es\co
LOAD/LUT						10-JAN-1995  KB
\oc\su
LOAD/LUT in_specs [load_specs] [disp_flag] [format]
	load colour lookup table into Image Display
\us\pu
Purpose:   
           Load colour lookup table into Image Display.
\up\sy
Syntax:   
           LOAD/LUT in_specs [load_specs] [disp_flag] [format]
\ys\pa
           in_specs = table  or  image,descr
\\
               (a) table - table name or table,:redlabl,:greenlabl,:bluelabl
               if the default columns :RED, :GREEN, :BLUE are not used
\\
               (b) image,descr - name of frame and descriptor where the LUT
               (as R1,G1,B1,R2,G2,B2,...) is stored
\\
               (c) ASCII file name - each line contains red, green and blue
               component separated by blanks or comma
\ap\pa
           load_specs = section, no. of values - defaulted to 0,256
\\
               this parameter was only applicable to DeAnza displays 
               (in case you still remember those prehistoric beasts),
               it is currently ignored, but should NOT be omitted (!)
\ap\pa
           disp_flag = D(isplay) or N(oDisplay) of LUT bar;
\\
               if omitted no change in current setup
\ap\pa
           format = TABLE or ASCII, for Midas binary table format or ASCII;
\\
               defaulted to TABLE
\ap\sa
See also:
           GET/LUT, MODIFY/LUT, DISPLAY/LUT, LOAD/ITT, TUTORIAL/LUT
\\
           chapter 6 of MIDAS User's Guide
\as\no
Note: 
           The LOAD/LUT command first looks in the current directory for the
           specified LUT table, if not found it searches for a system LUT table,
           which are stored in the directory MID_SYSTAB.
\\
           LUT columns may only be specified via labels (e.g. :MYLABL) not 
           via numbers (e.g. \#3).
\\
           If the LUT is an ASCII file, that data values may either be in the
           intervals [0,255] or normalized to [0.0,1.0].
\\
           For a demonstration of the standard LUTs use "TUTORIAL/LUT" .
\on\exs
Examples:
\ex
           LOAD/LUT staircase
             Load contents of columns :RED, :GREEN and :BLUE of system table
             `staircase.lut' as LUT.
\xe\ex
           LOAD/LUT mine,:special,:green,:myblue
             Load contents of column :SPECIAL, :GREEN and ;MYBLUE of private
             table `mine.lut' as LUT.
\xe\ex
           LOAD/LUT flaco,MIDAS_LUT
             Load contents of descriptor MIDAS_LUT stored in frame `flaco.bdf'
             as LUT.
\xe\ex
           LOAD/LUT gordo ? NO
             Load contents of columns :RED, :GREEN and :BLUE of LUT table
             `gordo.lut' into the display.
             Do not display the colour bar at the bottom of the display.
\xe\ex
           LOAD/LUT ridiculo.dat ? ? ASCII
             Load contents of ASCII file `ridiculo.dat' into the display.
             The LUT components (red, green, blue) per line are either in the
             interval [0,255] or [0.0,1.0].
\xe \sxe
