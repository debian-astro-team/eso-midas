%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2005 European Southern Observatory
%.IDENT      interpti.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, INTERPOLATE/TI
%.PURPOSE    On-line help file for the command: INTERPOLATE/TI
%.VERSION    1.0  29-MAY-1987 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./TI
\es\co
INTERPOLATE/TI						9-NOV-2005 JDP
\oc\su
INTERPOLATE/TI outima intab i,d refima [s] [degree]
	interpolate Image to Image
\us\pu
Purpose:    
           Spline interpolation of 1D data; table to image transformation.
\up\sy
Syntax:     
           INTERPOLATE/TI outima intab i,d refima [s] [degree]
\ys\pa
           outima = name of 1-dim output image, will be created by
		    this command
\ap\pa
           intab  = name of input table
\ap\pa
           refima = reference image;
		    this image must have the desired start, step value
		    in descr. START and STEP
\ap\pa
           i = column reference for independent variable
\ap\pa
           d = column reference for dependent variable
\ap\pa
           s = smoothness parameter (default 1.)
\ap\pa
           degree = degree of spline, max. 5
	   (default 3, cubic splines)
\ap\sa
See also:  
           INTERPOLATE/IT, REBIN/TI, CONVERT/TABLE
\as\no
Note:       
           This command should be used, if the new stepsize is NOT larger
	   than the current stepsize in column `i' (i.e. the difference
           between adjacent rows).
           If the stepsize is larger, the command REBIN/TI should be used
           instead, since that is doing an integration.

           Values in the column used as independent variable must be
           monotonically increasing or decreasing. Best results are obtained,
           if they are increasing.
           The parameter s controls the degree of smoothing. This parameter
           has to be chosen carefully: too small s-values will result in
           an overfitting, too large s-values will produce an underfitting
           of the data. For s very large it returns the least-squares
           polynomial fit.
           The number of spline knots and their positions  are determined
           automatically, taking into account the specific behaviour of
           the function underlying the data.
           Ref.: P. Dierckxx, 1982, Computer Graphics and Image Processing,
           vol. 20, 171-184.
\on\sxe
Examples:
\ex
           CREATE/IMAGE refima 1,2000 0.123,0.002 NODATA
             We don't need any data in the ref. frame.
           INTERPOLATE/TI resima intab :X,:Y refima 1. 3
             Create image `resima.bdf' from column :Y of table `intab.tbl'
             corresponding to :X resampled with stepsize 0.002.
             Smoothness = 1 and we use cubic splines.
	     The delta in :X between adjacent rows is assumed to be
             larger than (or equal to) 0.002 (otherwise we should have
             used REBIN/TI instead).
\xe \sxe
