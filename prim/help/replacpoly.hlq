% @(#)replacpoly.hlq	19.1 (ESO-IPG) 02/25/03 14:03:40 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      replacpoly.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, REPLACE/POLY
%.PURPOSE    On-line help file for the command: REPLACE/POLY
%.VERSION    1.0  07-JULY-1986 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./POLY
\es\co
REPLACE/POLY						03-NOV-1997  KB
\oc\su
REPLACE/POLYGON in,intab out test/low,hi=value
	replace pixels inside polygon
\us\pu
Purpose:      
           For all pixels in test frame inside a polygon the corresponding 
           pixels of input frame are replaced with the value.
\up\sy
Syntax:       
           REPLACE/POLYGON in,intab out test/low,hi=value
\ys\pa
           in = primary input frame
\ap\pa
           intab = optional table with coordinates of the windows surrounding
               the polygons stored in columns labeled :Xstart, :Ystart, :Xend
               and :Yend
\ap\pa
           out = output frame
\ap\pa
           test = optional different test frame;
\\
               if omitted (also no '/') the primary input frame will be used as
               test frame
\ap\pa
           low,hi = real low + high threshold for polygon detection in the test
               frame, pixels inside and on the polygon are replaced
\ap\pa
           value = replacement value, may be real constant (a) or auxiliary
               input frame (b)
\ap\sa
See also:
           REPLACE/IMAGE, FIND/PIXEL
\as\no
Note:         
           All images involved must have same dimensions!
\\
           Outside the polygon the output frame will be set equal to the input
           frame.
\on\exs
Examples:
\ex
           REPLACE/POLYGON  gustavo,adolfo apumanque 2.001,2.02=vitacura
            Use table `adolfo.tbl' to define the subframes of image 
            `gustavo.bdf' we work on. Inside these windows look for polygons
            with boundaries of intensity in the interval [2.001,2.02].
\\
            Replace all pixels of `gustavo.bdf' inside these polygons with the
            corresponding pixel values of the image `vitacura.bdf' and store 
            results in image `apumanque.bdf'.
\xe \sxe
