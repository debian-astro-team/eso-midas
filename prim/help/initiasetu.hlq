% @(#)initiasetu.hlq	19.1 (ESO-IPG) 02/25/03 14:03:26
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      initiasetu.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files
%.PURPOSE    On-line help file for the command: INIT/SETUP
%.VERSION    1.0  10-OCT-1988 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./SETU
\es\co
INITIALIZE/SETUP                                          27-JAN-1994  KB
\oc\su
INITIALIZE/SETUP [setup]
	initialize the variables of a Setup
\us\pu
Purpose:
          Initialize the variables of a Setup.
\up\sy
Syntax:
          INITIALIZE/SETUP setup
\ys\pa
          setup = name of a Setup
\ap\sa
See also:
          INFO/SETUP, READ/SETUP, WRITE/SETUP
\as\no
Note:
          Setups are a collection of variables (keywords) which have to be 
          set up before certain, more complex commands can be executed.
\\
          To get a list of all currently implemented Setups use the
          command INFO/SETUP.
\\
          Use "READ/SETUP setup" to see the default values for the variables
          of a specific Setup `setup' after having executed the command
          "INIT/SETUP setup".
\on\exs
Examples:
\ex
          INIT/SETUP catalog 
            Set the variables of the Setup `catalog' to their default values.
            With "READ/SETUP catalog" these variables may be displayed.
\xe \sxe


