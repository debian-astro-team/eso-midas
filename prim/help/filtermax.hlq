% @(#)filtermax.hlq	19.1 (ESO-IPG) 02/25/03 14:03:21 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      filtermax.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, FILTER/MAX
%.PURPOSE    On-line help file for the command: FILTER/MAX
%.VERSION    1.0  09-DEC-1992 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./MAX 
\es\co
FILTER/MAX					09-DEC-1992  KB
\oc\su
FILTER/MAX frame outfram [xyradius] [subima] [options]
	apply maximum filter to an image
\us\pu
Purpose: 
            Apply a maximum filter to a frame.
\up\sub
Subject: 
            Image smoothing, nonlinear filtering.
\bus\sy
Syntax: 
            FILTER/MAX frame outfram [xyradius] [subima] [options]
\ys\pa
            frame = input frame
\ap\pa
            outfram = result  frame
\ap\pa
            xyradius = radx,rady where `radx' and `rady' are the radius in x
                 and y, i.e. the number of pixels "around" the central pixel
                 which define the neighbourhood (NBH) of that pixel,
\\
                 total no. of pixels in NBH is then: (2*radx+1)*(2*rady+1);
\\
                 defaulted to 1,1 (a 3x3 neighbourhood, i.e. 9 pixel)
\ap\pa
            subima = optional specification of a subimage on which the filter
                 will be applied, may be either [xstart,ystart:xend,yend]
                 (see page 3.4 in the MIDAS User manual, chapter 3) to indicate
                 the limits of the subimage (with coordinates given according
                 to the MIDAS standard), or
\\
                 CURSOR if subimage will be chosen interactively, or
\\
                 table_name if the table "table_name" holds the endpoints of
                 the subimage(s) in columns labeled :XSTART, :YSTART, :XEND
                 and :YEND,
\\
                 defaulted to the complete input frame.
\ap\pa
            options = flag of 3 characters for options;
\\
                 (1) L or N for loop or no loop,
\\
                 (2) D or N for display of smoothed area or no display;
\\
                 (3) E or N for expanding input frame first, so that result
                     frame has same dimensions as input frame or no expansion
                     with result frame smaller than input
\\
                 defaulted to NNE
\ap\sa:
See also:
            FILTER/MIN, FILTER/GAUSS, FILTER/DIGITAL, FILTER/MEDIAN,
\\
            FILTER/SMOOTH, TUTORIAL/FILTER,
\\
            FFT/IMAGE, CONVOLVE/IMAGE, DECONVOLVE/IMAGE
\as\no
Note:
            Smooth the image by replacing each pixel value I(x,y) by Max(x,y),
            the maximum of all points in the neighbourhood of (x,y), which
            contains all pixels (x',y') with abs(x-x') .LE. radx  and
            abs(y-y') .LE. rady (radx, rady as given in the 3rd parameter).
\\
            The values radx and rady do not have to be equal, by setting
            radx or rady to 0, also 1-dimensional filters may be used.
\\
            If the subimage option is chosen, it must be at least as big as the
            filter kernel which has  (2*radx+1) * (2*rady+1) pixels.
\\
            If `option(3:3) = 'E' the input image is expanded by 2*radx columns
            and 2*rady lines, so that also the "edge" pixels can be calculated.
\\
            Note, that the flux is not conserved.
\on\exs
Examples:
\ex
            FILTER/MAX gordo flaco 
             Smooth image `gordo.bdf' by getting the maximum over a 3*3
             window around each pixel and set the corresponding pixels in
             frame `flaco.bdf' to that maximum.
\\
             The input frame is first expanded, so that the dimensions of the
             frame `flaco.bdf' are the same as the ones of `gordo.bdf'.
\xe\ex
            FILTER/MAX gordo flaco 1,1 CURSOR
             As above, but do the filtering process only to subimages of frame
             `gordo.bdf' which are chosen via the cursor.
\\
             It is assumed, that image `gordo.bdf' is loaded into the image
             display!
\xe\ex
            FILTER/MAX gordo flaco 3,0 coords
             As above, but the subimage specifications are taken from
             the table `coords.tbl'. This table could e.g. be created by using
             the command GET/CURSOR with the table option (and 2 cursors!)
\\
             Furthermore, a 7x1 window around each pixel will be used to find
             the maximum.
\xe\ex
            FILTER/MAX gordo flaco 2,1 [@20,<:@50,>]
             As above, but on only the subimage specified in the command line.
\xe\ex
            FILTER/MAX gordo flaco ? ? nnn
             As the first example, but the input frame is not expanded. The
             frame `flaco.bdf' will have xdim = NPX-2 and ydim = NPY-2, with
             NPX, NPY the no. of pixels in x,y of the input frame `gordo.bdf'.
\xe \sxe
