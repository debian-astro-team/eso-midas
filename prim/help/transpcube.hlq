% @(#)transpcube.hlq	19.1 (ESO-IPG) 02/25/03 14:03:48 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      flip_imag.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, FLIP/IMAGE
%.PURPOSE    On-line help file for the command: FLIP/IMAGE
%.VERSION    1.0  02-OCT-1986 : Creation, KB
%.VERSION    1.1  13-JAN-1992 : Update, KB
%----------------------------------------------------------------
\se
SECTION./CUBE
\es\co
TRANSPOSE/CUBE						 21-SEP-1995  KB
\oc\su
TRANSPOSE/CUBE inframe [outframe] [plane_spec]
	rearrange the planes of a cube
\us\pu
Purpose:
           Rearrange the planes of a cube.
\up\sy
Syntax:
           TRANSPOSE/CUBE inframe [outframe] [plane_spec]
\ys\pa
           inframe = input image (of 3 dimensions)
\ap\pa
           outframe = output image;
\\
                if omitted, the input image will be rearranged in place
\ap\pa
           plane_spec = plane specification (2 chars.), controls which planes
                of inframe will become the planes in xy-direction of outframe;
\\
                if set to ZY the zy_planes of inframe become the new xy_planes;
\\
                if set to XZ the xz_planes become the new xy_planes;
\\
                defaulted to ZY
\ap\sa
See also:
           COMPUTE/XYPLANE, COMPUTE/XZPLANE, COMPUTE/ZYPLANE
\\
           TRANSPOSE/IMAGE, FLIP/IMAGE
\as\no
Note:       
           The xy_plane of a cube is formed by the x- and y-axis and has
           NPIX(1) pixels per row and NPIX(2) pixels per column.
\\
           The zy_plane of a cube is formed by the z- and y-axis and has
           NPIX(3) pixels per row and NPIX(2) pixels per column.
\\
           The xz_plane of a cube is formed by the x- and z-axis and has
           NPIX(1) pixels per row and NPIX(3) pixels per column.
\\
           The planes of the input cube are rearranged in such a way as to
           keep their respective start and step values unchanged. That means,
           that the TRANSPOSE command does not just provide different views 
           at the cube as if one turned around a brick.
\on\exs
Examples:
\ex
            TRANSPOSE/CUBE chico ? xz
              Rearrange the planes of `chico.bdf' in such a way, that the
              xz-coords. become the new xy-coords. The first new xy-plane is
              the xz-plane which was located at the start y-coordinate of
              `chico.bdf' and the last new xy-plane is the xz-plane located
              at the end y-coord. of the frame. The contents of `chico.bdf' 
              are overwritten.
\xe \ex
            TRANSPOSE/CUBE largo otro
              Rearrange the planes of `largo.bdf' in such a way, that the
              zy-coords. become the xy-coords. of `otro.bdf'. The first 
              xy-plane of `otro.bdf' is the zy-plane of `largo.bdf' which was
              located at the start x-coordinate of `largo.bdf' and the last 
              xy-plane of `otro.bdf' is the zy-plane located at the last
              x-coord. of `largo.bdf'. The x-coords. of `largo.bdf' become the
              z-coords of `otro.bdf'.
\xe \sxe
