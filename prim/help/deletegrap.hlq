% @(#)deletegrap.hlq	19.1 (ESO-IPG) 02/25/03 14:03:14 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      deletegrap.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, DELETE/GRAPHICS
%.PURPOSE    On-line help file for the command: DELETE/GRAPHICS
%.VERSION    1.0  26-OCT-1990 : Creation, KB
%.VERSION    1.1  11-JUN-1991 : add parameter, KB
%----------------------------------------------------------------
\se
SECTION./GRAP
\es\co
DELETE/GRAPHICS					      11-JUN-1991  KB
\oc\su
DELETE/GRAPHICS [grap]
	delete graphic window(s) on XWindow displays
\us\pu
Purpose:    
          Delete graphic window(s) on XWindow displays.
\up\sy
Syntax:
          DELETE/GRAPHICS [grap]
\ys\pa
          grap = graphics window specification,
\\
               can be a number, the display_id used in "create/graphics";
\\
               or * to indicate all graphics windows;
\\
               or ALL to delete all windows, i.e. also the display windows;
\\
               defaulted to *
\ap\sa
See also:
          DELETE/DISPLAY, CREATE/DISPLAY, CREATE/GRAPHICS
\\
          Chapter 06 of MIDAS Users Manual, Volume A
\as\no
Note:  
          With the parameter set to ALL, DELETE/GRAPHICS is equivalent to
          DELETE/DISPLAY.
\\
          This command is only valid for XWindow Image Displays.
\on\exs
Examples:
\ex
          DELETE/GRAPHICS 
          Delete all graphics windows, leave any display window untouched.
\xe\ex
          DELETE/GRAPHICS 2
          Delete graphics window with id = 2.
\xe\ex
          DELETE/GRAPHICS all
          Remove all graphics and display windows, which have been previously
          created by Midas, from the screen.
\xe \sxe
