%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2009 European Southern Observatory
%.IDENT      deletecolu.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, DELETE/COLUMN
%.PURPOSE    On-line help file for the command: DELETE/COLUMN
%.VERSION    1.0  02-NOV-1985 : Creation, JDP
%----------------------------------------------------------------
\rightpage
\se
SECTION./COLUMN
\es\co
DELETE/COLUMN						11-MAR-2009  JDP, KB
\oc\su
DELETE/COLUMN table column_sel 
	delete table column(s)
\us\pu
Purpose:      
              Delete table column(s)
\up\sy
Syntax:       
              DELETE/COLUMN table column_sel
\ys\pa
              table  =     table name
\ap\pa
              column_sel = references, separated by blanks, to the column(s) 
                           to be deleted,
			   or a column interval
\ap\no
Note:         
              Column numbers or labels may be used.
	      When using a column interval, max. 100 columns can be deleted.
              Use SHOW/TABLE to see the resulting table layout.
\on\see
See also:
              CREATE/COLUMN
\ees\exs
Examples:
\ex
              DELETE/COLUMN tortilla #2 :X
                This command deletes the second column and the column :X
                from `tortilla.tbl'.
\xe\ex
              DELETE/COLUMN fajita :ra :dec 
                Deletes the columns :ra and :dec from `fajita.tbl'.
\xe\ex
              DELETE/COLUMN enchilada #3..30
                Deletes the 28 columns #3,#4,...,#30 from `enchilada.tbl',
                assuming that `enchildada.tbl' has at least 30 columns.
\xe\sxe
