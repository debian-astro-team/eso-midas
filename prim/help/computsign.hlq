% @(#)computsign.hlq	19.1 (ESO-IPG) 02/25/03 14:03:04 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      computsign.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, COMPUTE/SIGNATURE
%.PURPOSE    On-line help file for the command: COMPUTE/SIGNATURE
%.VERSION    1.0  06-JUN-2001 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./SIGN
\es\co
COMPUTE/SIGNATURE				03-AUG-2001  KB
\oc\su
COMPUTE/SIGNATURE datafile [option] [FITSfile]
	compute the MD5 signature for the data of image or table
\us\pu
Purpose: 
	  Compute the MD5 signature for the data of an image or table.
\up\sy
Syntax:
          COMPUTE/SIGNATURE datafile [option] [FITSfile]
\ys\pa
          datafile = name of image/table in Midas or FITS format
\ap\pa
          option = Update  or  Calculate_only; 
	     defaulted to Update
	     for the `Update' option a Midas descr. (FITS keyword) named 
	     DATAMD5 is written, containing the 128 bit MD5 signature,
	     represented in hexdecimal format as a 32 char. string;
	     for `Calculate' this signature is only calculated and
             can be compared to an exisiting DATAMD5  - 
	     no descriptor (FITS keyword) is written
\ap\pa
          FITSfile = optional name of FITS file;
	     only applicable if `datafile' is in Midas format,
	     DATAMD5 is calculated and written to the primary header
	     of the FITSfile (also for tables), see the Note for details;
             `option' is always taken as Update in that case
\ap\sa
See also:
          INDISK/MFITS, WRITE/DESCR, OUTDSK/SFITS
\as\no
Note:  
             The data of the file in their FITS representation are used to 
	  compute the MD5 signature, a 128 bit value, which uniquely
	  identifies this data set.
          Therefore, files in binary Midas format are first converted to
	  FITS and then the MD5 signature is calculated.
             If `datafile' is in Midas format, the MD5 signature is
          stored in the DATAMD5 descriptor of `datafile'. 
	  Thus, if that Midas file is a table and converted to FITS 
	  later on, DATAMD5 is written into the table extension header, 
	  NOT into the empty primary header of the FITS file, 
	  since DATAMD5 is just another descriptor of the Midas table.
             Since other applications always look for the DATAMD5 keyword
	  in the primary header, you should use the `FITSfile' parameter
	  to have DATAMD5 in the primary header also for Midas tables.
          Another possibility to force the DATAMD5 descr. to go to the
          primary header is to use the command OUTDSK/SFITS.
 
	  The command stores the MD5 signature also in the keyword OUTPUTC.
\on\exs
Examples:
\ex
          COMPUTE/SIGNATURE uves.fits
            Calculate the MD5 signature for the data of FITS file `uves.fits'
            and write it into the FITS header as keyword DATAMD5, as well as
	    into char. keyword OUTPUTC.
\xe\ex
          COMPUTE/SIGNATURE fors2.tbl calc
            Get the MD5 signature for the data of Midas table `fors2.tbl',
\xe\ex
          COMPUTE/SIGNATURE forspix.tbl ? forspix.tfits
            Convert `forspix.tbl' to FITS file `forspix.tfits' with keyword 
	    DATAMD5 in the primary header containing the MD5 signature 
	    calculated for the data of `forspix.tfits'.
\xe \sxe
