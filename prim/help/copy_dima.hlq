% @(#)copy_dima.hlq	19.1 (ESO-IPG) 02/25/03 14:03:06 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      copy_dima.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, COPY/DIMA
%.PURPOSE    On-line help file for the command: COPY/DIMA
%.VERSION    1.0  14-MAR-1987 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./DIMA
\es\co
COPY/DIMA 					09-APR-1992  KB
\oc\su
COPY/DIMA source_frame source_desc dest_frame
	copy descriptor of source frame to new image
\us\pu
Purpose:   
	  Copy numerical descriptor of source frame to new image.
\up\sy
Syntax:    
          COPY/DIMA source_frame source_desc dest_frame
\ys\pa
          source_frame = name of source frame
\ap\pa
          source_desc = source descriptor, it must be of non_character type
\ap\pa
          dest_frame = name of output image
\ap\sa
See also:
          COPY/ID, COPY/DK, COPY/KD
\as\no
Note:      
          The result image will always be created with real data type.
\on\exs
Examples:
\ex
          COPY/DIMA tiburon histogram ballena
          Copy the histogram of frame `tiburon.bdf' to new image `ballena.bdf'.
\xe \ex
          COPY/DIMA tiburon zdata/i/21/100 ballena
          Copy 100 elements of descriptor zdata of frame `tiburon.bdf' to new
          image `ballena.bdf'  (starting at the 21st element).
\xe \sxe
