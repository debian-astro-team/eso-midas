% @(#)grow_imag.hlq	4.2 (ESO-IPG) 3/16/93 10:56:21 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      grow_cube.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, GROW/CUBE
%.PURPOSE    On-line help file for the command: GROW/CUBE
%.VERSION    1.0  24-NOV-1995 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./CUBE
\es\co
GROW/CUBE					24-NOV-1995  KB
\oc\su
GROW/CUBE frame no_planes frame_list
	expand 2-dim/3-dim frame
\us\pu
Purpose:    
          Add planes to a 2-dim or 3-dim image.
\up\sy
Syntax:     
          GROW/CUBE frame no_planes frame_list
\ys\pa
          frame = name of frame to be expanded
\ap\pa
          no_planes = no. of planes to add
\ap\pa
          frame_list = name of a list_file or the actual list of 2-dim frames 
               separated by a comma; the list_file is either a Midas image
               catalog, indicated by the type .cat (e.g. luna.cat) or an ASCII
               file holding the frame names, one per line (e.g. sol.dat)
\ap\sa
See also:
          GROW/IMAGE, CREATE/IMAGE
\as\no
Note: 
          If `no_planes' is greater than the no. of 2-dim frames obtained from
          `frame_list' the remaining planes are filled with the contents of 
          real keyword NULL(2) (the user defined Null value).
\on\exs
Examples:
\ex
          GROW/CUBE mucho 1 mas
           Add one plane (in xy-direction) to 2-dim or 3-dim frame `mucho.bdf'.
           This plane is set equal to the 2-dim image `mas.bdf'.
\xe \ex
          GROW/CUBE rio 12 agua.cat
           Add 12 planes to 2-dim or 3-dim frame `rio.bdf'.
           If the image catalog `agua.cat' has less than 12 entries, the
           remaining planes are filled with the contents of NULL(2). If catalog
           `agua.cat' holds more than 12 entries only the first 12 entries
           are used.
\xe \sxe
