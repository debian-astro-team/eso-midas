%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      outDiskfits.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, OUTDISK/FITS
%.PURPOSE    On-line help file for the command: OUTDISK/FITS
%.VERSION    1.0  05-NOV-1993 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./FITS
\es\co
OUTDISK/FITS						26-AUG-2004  KB
\oc\su
OUTDISK/FITS in_files [out_spec] [option] [out_type]
	write Midas frames to FITS files on disk
\us\pu
Purpose:
	  Convert Midas images, tables or fitfiles to FITS files on disk.
\up\sy
Syntax: 
          OUTDISK/FITS in_files [out_spec] [option] [out_type]
\ys\pa
          in_files = filename(s) of Midas frames separated by a comma;
              or name of ASCII file (indicated by filetype `.cat') with a 
              filename on each line or name of Midas catalog (also indicated 
              by filetype `.cat') with input names;
              also wildcard characters are supported.
\ap\pa
          out_spec = corresponding FITS filenames separated by a comma;
              or name of ASCII file (indicated by filetype `.cat') with a
              filename on each line or name of Midas catalog (also indicated
              by filetype `.cat') with names;
              or string "NAME=INPUT" to use input file names with the filetype
              specified via parameter `out_type';
              or string "ROOT=abcd" to build output file names as 
              abcd0001.TYPE, abcd0002.TYPE, ... (default root is "toto")
              with TYPE set to parameter `out_type';
              if the param `out_spec' is omitted, output names are built as
              toto0001.TYPE, toto0002.TYPE, ... 
\ap\pa
          option = Image, Table or Fitfile specifying the type of the input
              Midas frames to be converted; defaulted to Image
\ap\pa
          out_type = file type used for output files; defaulted to `.mt'
\ap\sa
See also:
          OUTDISK/SFITS, OUTDISK/ASCII, OUTTAPE/FITS, INDISK/FITS, 
          RESTORE/NAME, @a inoutFITS
\as\no
Note:  
            If not as many output file names as input names are given, the 
          names toto0001.TYPE, toto0002.TYPE, ... are used.
            If your MIDAS frames are "essentially" 16bit integer frames
          use the command COPY/II first to convert them to 16bit data
          frames and then do the OUTDISK/FITS to obtain 16bit FITS files.
	    For Midas tables up to 768 columns are supported.
\on\exs
Examples:
\ex
          OUTDISK/FITS xyz
            Convert Midas image `xyz.bdf' to FITS file `toto0001.mt'.
\xe\ex
          OUTDISK/FITS xyz ? ? .fits
            Convert Midas image `xyz.bdf' to FITS file `toto0001.fits'.
\xe\ex
          OUTDISK/FITS paradis.cat david.cat
            Read Midas catalog (or ASCII file) `paradis.cat' and convert all
            Midas images with names in that catalog (file) to FITS files with
            the corresponding names in catalog (file) `david.cat'.
\xe\ex
          OUTDISK/FITS paradis.cat NAME=INPUT ? .data
            As above but output names are the same as input names with new
            file type `.data'. E.g. Midas image `lola.bdf' would be converted
            to FITS file `lola.data'.
\xe\ex
          OUTDISK/FITS paradis.cat ROOT=vieux ? .fits
            As above but names of output FITS files will be built as 
            `vieux0001.fits', `vieux0002.fits', ...
\xe\ex
          OUTDISK/FITS luminy*,canebiere ROOT=port table
            Convert all Midas tables matching the pattern `luminy*' as well as
            the table `canebiere.tbl' to FITS files with names `port0001.mt',...
\xe \sxe
