%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2007 European Southern Observatory
%.IDENT      label_grap.hlq
%.AUTHOR     RHW, IPG/ESO
%.KEYWORDS   MIDAS, help files, LABEL/GRAPHIC
%.PURPOSE    On-line help file for the command: LABEL/GRAPHIC
%.VERSION    1.0  15-JUL-1988 RHW: Created
%.Update     1.0  29-Oct-1999 RHW: Inclusion of screen coordinates
%----------------------------------------------------------------
\se
SECTION./GRAP
\es\co
LABEL/GRAPHIC					        28-SEP-2007 RHW,KB
\oc\su
LABEL/GRAPHIC label [x_pos,y_pos[,wo|,mm|,sc|,no]] [angle] [size] [pos_ind]
	     write a label_string on the graphics device
\us\pu
Purpose:     
             Write a label on graphics device; the position of the label can 
             be given with the cursor or directly on the command line
\up\sy
Syntax:      
             LABEL/GRAPHIC label [x_pos,y_pos[,wo|,mm|,sc|,no]] [angle] [size] 
                       [pos_ind]
\ys\pa
             label =   label_string to write up to 40 characters. If the label
                       contains any blanks, it has to be enclosed in double 
                       quotes (")
\ap\pa
             x_pos,y_pos[,wo|,mm|,sc|,no] = Position of the text in the plot.
                       Positions can be given via the cursor `C' or in world 
                       coordinates (,wo), in mm (,mm), in  screen coordinates 
                       (,sc) as well as in normalized coordinates (,no) running
                       from 0,0 to 1.0. 
                       World coordinates refer to the coordinate box of the 
                       axes, defined and plotted by either a plot command 
                       or plot/overplot axes command. The reference point 
                       (origin) for both screen coordinates and mm 
                       coordinates is the ower left corner (0,0). Screen 
                       coordinates are supported for graphics and display 
                       windows only.
                       By default, i.e. if no units are given, coordinates 
                       are interpreted as a world coordinates.

                       In case NO coordinate pair is entered the graphics 
                       cursor will appear, allowing you to enter the 
                       position with the left mouse button. 
                       Exit with the right mouse button.
\ap\pa
             angle   = angle for the text direction; defaulted to 0
\ap\pa
             size    = character multiplication factor; defaulted to 1
\ap\pa
             pos_ind = adjustment indicator: 0=text centered on position;
                       1=text starting at position; 2= text ending at
                       position; default is 0
\ap\no
Note:       
             Double quotes ("), e.g. used for arcsec in the string must
             be indicated as \aglquotes to avoid a hickup with the Midas
             parser which uses double quotes as separators.

             The command allows to overplot text outside the graphics frame.
             In such a case, if one wants to input coordinates an extrapolation
             of the x- and/or y-axis should be applied.

             The command allows you to re-iterate: as long as the exit button 
             isn't pressed you can choose another position of the text with 
             the left button.
\on\see
See also:    
             SET/GRAPHICS, SHOW/GRAPHICS, ASSIGN/GRAPHICS, OVERPLOT/SYMBOL,
             OVERPLOT/LINE           
\ees\exs
Examples:
\ex
             LABEL/GRAPH "a: Sa - Sab" 1.0,0.7 ? 1.5 1
\xe\ex
             LABEL/GRAPH "b: Sb - Sbc" 1.0,0.6 ? 1.5 1
\xe\ex
             LABEL/GRAPH "xyz 3\aglquotes abc"     1.0,0.5 ? 1.5 1
               Put in the indicated position the string: xyz 3" abc
\xe\ex
             LABEL/GRAPH "c: > Sc"     1.0,0.5 ? 1.5 1
\xe\ex
             LABEL/GRAPH "All Types"   30.2,20.7,mm ? 1.5 1
               Write four lines legenda in the plot starting. The first three
               start on world coordinate 1.0,0.7 to 1.0,0.5; 
               the last label will be put 30.2 mm in x and 20.7 mm. in y 
               from the lower left corner.
\xe \sxe
