%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2012 European Southern Observatory
%.IDENT      convertabl.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, CONVERT/TABLE
%.PURPOSE    On-line help file for the command: CONVERT/TABLE
%.VERSION    1.0  12-JULY-1985 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./TABL
\es\co
CONVERT/TABLE						10-FEB-2012 PB
\oc\su
CONVERT/TABLE image = table x[,y] z refima [method] [par]
CONVERT/TABLE image = table x[,y] refima FREQ
	table to image conversion
\us\pu
Purpose:    
            Table to image conversion. The values in the input table
            are used to generate an image in the domain defined by the
            reference image. Four methods are currently available for
            this conversion:
            - POLYNOMIAL: Polynomial fit to the table data;
            - SPLINE: 1D Spline interpolation of the table data. Using the
              algorithm :Publ. of the Dominion Astrophys. Obs. 204, 1982;
            - PLOT: Transform table entries into image pixels.
              Pixel position is defined by the columns x and y,
              pixel value is defined by the column z;
            - FREQUENCY: The output image is a 2D histogram counting
              the number of 'events' in the columns x,y of the table
\up\sy
Syntax:     
            CONVERT/TABLE image = table x[,y] z refima [method] [par]   (a)
            or
            CONVERT/TABLE image = table x[,y] refima FREQUENCY          (b)
\ys\pa
            image = output image
\ap\pa
            table = input table
\ap\pa
            x,[y] = column(s) with the independent variable(s)
\ap\pa
            z = column with the dependent variable 
\ap\pa
            refima = reference image, to define NPIX, START and STEP
                      corresponding to the output image
\ap\pa
            method (a) = POLYNOMIAL - polynomial fit,
                         SPLINE - 1D spline interpolation,
                         PLOT - straight table to image transformation,
		         defaulted to SPLINE 
                   (b) = FREQUENCY - 2D histogram distribution, 
			 only possible method for (b)
\ap\pa
            par = degree of the polynomial fit, for method POLYNOMIAL 
		  (defaulted to 0) or smoothing parameter and degree of 
		  the spline for method SPLINE; only applicable to (a)
\ap\sa
See also:   REBIN/TI, INTERPOLATE/TI
\as\no
Note:       The method PLOT can be used to display the table on the
            image display.

            The method SPLINE requires that the values in the column
            used as independent variable are monotonically increasing
            or decreasing. Only 1D transformations are supported.

            Two spline methods are available:
            -- The first one is based on Hermite polynomials and will be
               used when the smoothing parameter and the degree of the spline
               are not provided. (Ref: Publ. of the Dominion Astrophys. 
               Obs. 204, 1982). This method doesn't extrapolate , i.e doesn't
               handle undefined input at the extremes. 
            -- The second one is used when the smoothing parameter and the
               degree of the spline are provided. The smoothing parameter
               controls the approximation of the interpolated data to the
               input frame. This parameter has to be choosen carefully :
               too small s-values will result in an overfitting, too large 
               s-values will produce an underfitting of the data. A typical
               value to be used would be 1. The degree of the spline is 
               limited to 5.
\on\exs
Examples:
\ex
            CONVERT/TABLE outima = intable :X :Y refima SPLINE
            This command will create the image `outima.bdf' with the same
            standard descriptors as `refima.bdf' (the same definition domain),
            by using a spline interpolation scheme to the points defined
            in columns :X, :Y in `intable.tbl'.
\xe \sxe
