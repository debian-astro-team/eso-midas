% @(#)subtratcat.hlq	19.1 (ESO-IPG) 02/25/03 14:03:47 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      subtratcat.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, SUBTRACT/TCAT
%.PURPOSE    On-line help file for the command: SUBTRACT/TCAT
%.VERSION    1.0  21-DEC-1989 : Creation, KB
%.VERSION    1.1  20-JUN-1991 : Update, KB
%----------------------------------------------------------------
\se
SECTION./TCAT
\es\co
SUBTRACT/TCAT                                   20-JUN-1991  KB
\oc\su
SUBTRACT/TCAT [cat_name] frame_list
	remove entries from an table catalog
\us\pu
Purpose:
          Remove entries from an table catalog.
\up\sy
Syntax:
          SUBTRACT/TCAT [cat_name] [frame_list]
\ys\pa
          cat_name = name of table catalog;
\\
              defaulted to currently active table catalog
\ap\pa
          frame_list = list of table files to be subtracted,
\\
               maybe file specifications, separated by a comma (no spaces!);
\\
               or a single catalog name;
\\
               or wildcard specifications (e.g. a3*,n*.tbl);
\\
               or numbers referring to the names (e.g. \#7,\#1)
\ap\no
Note:
          The different options for the frame_list may not be mixed!
\on\sa
See also:
          SET/TCAT, CREATE/TCAT, CLEAR/TCAT, READ/TCAT, ADD/TCAT
\as\exs
Examples:
\ex
          SUBTRACT/TCAT dec89 coords
          Remove the entry for table file coords.tbl from catalog dec89.cat.
\xe\ex
          SUBT/TCAT dec88 coo*
          Remove entries for all tables where the names begin with the
          string "coo" from catalog dec88.cat.
\xe\ex
          SUBT/TCAT dec88 nov89.cat
          Remove entries for all tables which are in the table catalog
          nov89.cat from catalog dec88.cat.
\xe\ex
          SUBT/TCAT dec88 \#1,\#6
          Remove entries no. 1 and no. 6 from catalog dec88.cat.
\xe \sxe
