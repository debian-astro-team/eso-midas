% @(#)draw_slit.hlq	19.1 (ESO-IPG) 02/25/03 14:03:17 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      draw_slit.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, DRAW/SLIT
%.PURPOSE    On-line help file for the command: DRAW/SLIT
%.VERSION    1.0  03-MAY-1984 : Creation, KB
%.VERSION    1.1  23-JUL-1991 : Update, KB
%----------------------------------------------------------------
\se
SECTION./SLIT
\es\co
DRAW/SLIT						23-JUL-1991  KB
\oc\su
DRAW/SLIT [in_spec] [coord_ref] [draw_opt] [intens] [nocurs] [key_flag]
	draw IUE\ slits in the overlay channel
\us\pu
Purpose: 
           Draw slits in the overlay channel.
\up\sy
Syntax:  
           DRAW/SLIT [in_spec] [coord_ref] [draw_opt] [intens] [nocurs]
                     [key_flag]
\ys\pa
           in_spec = input specification for lower left and upper right corner
                of the rectangle inside the slit;
\\
                (a) CURSOR if coordinates are chosen via the cursor rectangle;
\\
                (b) name of table containing coordinates in columns labeled
                    :XSTART, :YSTART, :XEND and :YEND;
\\
                (c) x1,y1,x2,y2 defining the coords. of lower left and upper
                    right corner;
\\
                (d) name of integer keyword holding the coords, max. 40 values,
                    i.e. 10 slits, see parameter 'key_flag'
\\
                defaulted to CURSOR
\ap\pa
           coord_ref = F or S, to indicate that coordinates should be
                interpreted as screen pixels or frame coordinates in the usual
                MIDAS syntax, only applicable for option (b) and (c);
                for option (a) and (d) always screen pixels are used;
\\
                defaulted to S, also any "strange" input is interpreted as "S" !
\ap\pa
           draw_opt = F(ill) or N(ofill) the slit;
\\
                defaulted to N, also any "strange" input is interpreted as "N" !
\ap\pa
           intens = color,rotation angle (in degrees) of drawing,
\\
                the arrow-graph is first built up from the specs and in the
                very end rotated counter-clockwise with the rotation angle,
\\
                defaulted to WHITE,0.
\\
                the slit is first built up from the specs and in the very end
                rotated counter-clockwise by the rotation angle around its
                center
\ap\pa
           nocurs = no. of cursors to use, if you specify 1, only one cursor is
                used and you have to press ENTER twice for the corners of each 
                inner slit rectangle;
\\
                defaulted to 2

\ap\pa
           key_flag = K(ey) or N(okey) to indicate that 'in_spec' holds the
                name of an integer keyword which contains the inner corners for
                up to 10 slits, the last coord. must be followed by a -1;
\\
                defaulted to N
\ap\sa
See also: 
           DRAW/LINE, DRAW/CIRCLE, DRAW/ELLIPS, DRAW/RECTANGLE, DRAW/ARROW
\\
           DRAW/CROSS, DRAW/ANY
\as\no
Note: 
           The parameters may also be cross referenced via
\\
           INSPEC=, COOREF=, DROPT=, INTENS=, NOCURS= and KEY=
\\
           The following colors are supported (via name or number) in X11:
\\
           Red(3), Green(4), Blue(5), White(2), Black(1), Yellow (6),
           Magenta(7), Cyan(8). All values in [9,255] are interpreted as White.
\\
           If coord_ref = F, a frame must be loaded in the displayed channel.
\\
           The cursor rectangle is moved with the mouse and adjusted in size
           with the arrow keys. Use the ENTER and EXIT buttons on the mouse to
           draw or to exit.
\\
           Due to the redrawing of the cursor rectangle some parts of the
           slit will not have immediately the desired color, but that
           will be corrected, once you exit from the command.
\on\exs
Examples:
\ex
           DRAW/SLIT
             Use cursor rectangle to define corners and draw a white IUE slit.
\xe\ex
           DRAW/SLIT coords F N red
             Use values from columns :xstart, :ystart, :xend, :yend of table 
             `coords.tbl', interpret them as real world coordinates of the
             currently displayed frame and draw IUE slits in red color,
             do not fill them.
\xe\ex
           DRAW/SLIT 200,200,300,300 S F 6,32.5
             Draw a slit which has been rotated by 32.5 degrees (before the
             rotation the slit was defined by the screen pixels (200,200) and 
             (300,300)), fill it with yellow.
\xe \sxe
