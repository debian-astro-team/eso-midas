% @(#)extracslit.hlq	19.1 (ESO-IPG) 02/25/03 14:03:19 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      extracslit.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, EXTRACT/SLIT
%.PURPOSE    On-line help file for the command: EXTRACT/SLIT
%.VERSION    1.0  05-JUN-1986 : Creation, KB
%.VERSION    1.1  09-APR-1992 : Update, KB
%----------------------------------------------------------------
\se
SECTION./SLIT
\es\co
EXTRACT/SLIT						25-MAY-1998  KB
\oc\su
EXTRACT/SLIT [in_option] [resframe] [slit_specs]
	extract a subimage defined by a fixed slit from image
\us\pu
Purpose:    
           Extract a subimage defined by a fixed slit from image.
\up\sy
Syntax:     
           EXTRACT/SLIT [in_option] [resframe] [slit_specs]
\ys\pa
           in_option = CURSOR, if subimage will be extracted from displayed
              image via cursor slit (option 1);
\\
              inframe if subimage will be extracted from inframe (option 2);
\\
              defaulted to CURSOR
\ap\pa
           resframe = name for result frame, defaulted to slit.bdf
\ap\pa
           slit_specs = definition of slit size:
\\
            (a) slitw,slitl,refcoord stepx,stepy
\\
                slitw,slitl,refcoord = width, length of slit in real pixels
                or world coords; refcoord = X or Y, defaulted to X
\\
                stepx,stepy = stepsize along the base line and between
                different lines, default is stepsize in x and y of displayed
                image
\\
            (b) refslit
\\
                refslit = reference frame for slit:
                slitw,slitl,stepx,stepy will be taken from descriptors NPIX
                and STEP of frame refslit; and if option 2 is used, also the
                start values for the extracted slit are taken from descr START
\\
            (c) refslit slit_angle
\\
                slit_angle = angle (in degrees) of baseline of slit (which is
                the x-axis) with horizontal axis (counterclockwise);
\\
                defaulted to 45.0
\\
                (a), (b) are possible only for option 1, (c) only for option 2
\ap\sa
See also:
           EXTRACT/CURSOR, EXTRACT/TRACE, EXTRACT/ROTA, EXTRACT/IMAGE
\as\no
Note:       
           If you use the CURSOR option:
\\
           Press ENTER (left) button on the mouse to draw the slit which would
           be extracted. You can move one cursor via the mouse and the other
           via the arrow keys. Thus you can modify the position of the slit but
           not it's size.
           To really extract the subimage under the slit, press the EXIT (right)
           button on the mouse.
\on\exs
Examples:
\ex
           EXTRA/SLIT ? sub @22,@100
             Use a slit which is 22 real (frame) pixels wide and 100 pixels
             long. The size of the slit on the screen depends on the scaling
             and zooming values of the reference coordinate (= X as default)
             when the image was loaded.
\xe\ex
           EXTRA/SLIT ? sub 22.,100.4,Y
             Use a slit which is 'yw' pixels wide and 'yl' pixels long,
             yw = 22./y-step, yl = 100.4/y-step of displayed image.
\xe\ex
           EXTRA/SLIT ? sub refslit
             Use a slit the size of which is determined from the size of the
             frame `refslit.bdf'.
\xe\ex
           EXTRA/SLIT in sub slit 33.
             Use image `in.bdf' as input, `sub.bdf' as output and the image 
             `slit.bdf' as reference slit with a slit angle of 33. degrees.
\xe \sxe
