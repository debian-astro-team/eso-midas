%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990,2004 European Southern Observatory
%.IDENT      read_imag.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, READ/IMAGE
%.PURPOSE    On-line help file for the command: READ/IMAGE
%.VERSION    1.0  06-OCT-1989 : Creation, KB
%.VERSION    1.1  31-MAY-1991 : Update, KB
%.VERSION    1.2  25-FEB-1992 : Modif for Command Verif, KB
%----------------------------------------------------------------
\se
SECTION./IMAG
\es\co
READ/IMAGE					23-JUN-2004  KB
\oc\su
READ/IMAGE frame_specs [pixel_specs] [hide_header_flag] [novals-per-line]
	display image data values
\us\pu
Purpose:  
          Display image data values.
\up\sy
Syntax:
          READ/IMAGE frame_specs [pixel_specs] [hide_header_flag]
		     [novals-per-line]
\ys\pa
          frame_specs = name of data frame
               or CURSOR,option if cursor(s) are used to define the region
               of the displayed frame which will be read;
               option = curs_spec,enter_flag,nolines
               curs_spec = 1 - if cursor is used for reading a single
               pixel of displayed frame;
               curs_spec = 2 - if cursor defined window is used for reading 
               pixels;
               curs_spec = C - if a window (5 pixels wide) centered at single
               cursor is used for reading pixels;
               enter_flag = E for getting cursor coord. when pressing ENTER,
               enter_flag = C for getting continuously cursor coords. back 
               if curs_spec = C, the no. of lines of that window can be
               specified via `nolines', defaulted to 5
               if `frame_specs' set to CURSOR, `option' is defaulted to: 2,E
               and the following parameters are ignored
\ap\pa
          pixel_specs = xcoord,noval or xcoord,ycoord,noval or
               xcoord,ycoord,zcoord,noval,
               depending on the number of dimensions of the frame;
               with coords according to the MIDAS standard for coordinates:
                 a) coord = real no.    => world coordinate
                 b) coord = @integer    => pixel coordinate
                 c) coord = "<"         => first pixel coord. (same as @1)
                 d) coord = ">"         => last pixel coordinate;
               or = [xstart,ystart:xend,yend]
               or = Rn-m
		 to display row no. n till row no. m (inclusive)
               or = Cn-m
		 to display column no. n till column no. m (inclusive)
               or = tbname,T
                 to use a table `tbname' which must contain the columns
                 :XSTART, :YSTART, :XEND and :YEND defining the windows in the
                 frame which are to be read, only world coordinates are
                 possible for the window specifications;
               defaulted to read the first 20 values of specified  frame;
               i.e. like  "<,<,20"  for a 2-dim frame
\ap\pa
          hide_header_flag = if set to H, the frame name as well as the lines
               indicating line and pixel no. are suppressed, else not;
               defaulted to N
\ap\pa
          novals-per-line = optional no. of values to display on a single
	       line, default = 3 for double data, 5 for all other data (max. 20)
\ap\sa
See also:
          PRINT/IMAGE, GET/CURSOR, STATISTICS/IMAGE, WRITE/IMAGE 
\as\no
Note:
          To use the cursor option you have to enter the complete string
          `CURSOR', no abbreviation is possible. In that mode the lines are 
          displayed in descending order to match the displayed image, else
          lines are displayed in ascending order.
          The `hide_header_flag' is especially useful for producing plain
          ASCII files, containing only the data values, as output.
\on\exs
Examples:
\ex
          READ/IMAGE chango @200,<,20
            Read (=display on terminal) 20 pixels of frame `chango.bdf',
            starting at pixel no. 200 in x and first pixel in y.
\xe\ex
          READ/IMAGE chango [@200,<:@219,<]
            Same as above (assuming, that a line contains at least 219 pixels).
\xe\ex
          READ/IMAGE elefante 10.0,120.0,24 h
            Read 24 pixel values of image `elefante.bdf' starting at x-world
            coord 10.0 and y-world coord 120.0; do not display frame name and
            line, pixel numbers.
\xe\ex
          READ/IMAGE armadillo.fits R12-25
            Read all pixel values of FITS image `armadillo.fits' from
            row no. 12 until row no. 25, inclusive. Assuming, e.g. it's an
	    512x512 image, this would display 512x14 pixel.
\xe\ex
          READ/IMAGE CURSOR
            Move cursor window to define region on displayed frame and press
            ENTER button of the mouse to display all pixel values inside cursor
            defined window.
            To terminate, press EXIT (= right) button of the mouse.
\xe\ex
          READ/IMAGE cursor,c,e,9
            Use a 5x9 window centered at cursor to define region on displayed
            frame and press ENTER (= left) button of the mouse to read the 
            corresponding data.
\xe\ex
          READ/IMAGE leon tigre,t
            Read pixels of frame `leon.bdf', the regions are defined in the
            columns :XSTART, :YSTART, :XEND, :YEND of table `tigre.tbl'.
\xe \sxe
