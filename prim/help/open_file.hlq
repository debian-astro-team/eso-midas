% @(#)open_file.hlq	19.1 (ESO-IPG) 02/25/03 14:03:31 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      open_file.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, OPEN/FILE
%.PURPOSE    On-line help file for the command: OPEN/FILE
%.VERSION    1.0  02-JUN-1992 : Creation, KB
%.VERSION    1.1  14-JUN-1993 : Update, KB
%----------------------------------------------------------------
\se
SECTION./FILE
\es\co
OPEN/FILE					     23-SEP-1992  KB
\oc\su
OPEN/FILE filename flag file_control_key
	open an ASCII file for reading or writing
\us\pu
Purpose:
          Open an ASCII file for reading or writing.
\up\sy
Syntax:  
          OPEN/FILE filename flag file_control_key
\ys\pa
          filename = name of ASCII file (including file type, if any)
\ap\pa
          flag = READ, WRITE or APPEND, if opening the file for reading,
              writing (i.e. creating a new file) or appending;
\\
              defaulted to READ
\ap\pa
          file_control_key = \\ name of existing integer keyword of at least
              2 elements to receive in its 1st element the file-id of the
              opened file;
              subsequent READ/FILE and WRITE/FILE commands will store the no.
              of chars. read or written into the 2nd element of that keyword;
\\
              the file-id is then used in the READ, WRITE and CLOSE/FILE
              commands
\ap\sa
See also:
          CLOSE/FILE, READ/FILE, WRITE/FILE
\as\no
Note:   
          The file-id is a number greater than 0. If the opening of the file
          failed a value of -1 is stored in the keyword. No error message
          is displayed, so it is the responsibility of the user to check that
          value.
\\
          Opening a file for writing means creating a new file.
\on\exs
Examples:
\ex
          OPEN/FILE apumanque.dat write fctrl
            Create an ASCII file `apumanque.dat' and store the file-id in the
            first element of the integer keyword `fctrl' (which must have been
            created before with at least two elements).
\xe\ex
          OPEN/FILE macul.ascii ? OUTPUTI
            Open ASCII file `macul.ascii' for reading and store the file-id in
            integer keyword `outputi' (as the first element).
\xe\ex
          OPEN/FILE alfonso.asc A inputi
            Open existing ASCII file `alfonso.asc' for appending records in the
            end of the file and store the file-id in keyword inputi(1).
\xe\sxe

