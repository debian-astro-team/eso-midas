% @(#)averagcolu.hlq	19.1 (ESO-IPG) 02/25/03 14:02:57 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      averagcolu.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, AVERAGE/COLUMN
%.PURPOSE    On-line help file for the command: AVERAGE/COLUMN
%.VERSION    1.0  09-MAR-1984 : Creation, JDP
%.VERSION    1.1  17-APR-1991 : Modification, KB
%----------------------------------------------------------------
\se
SECTION./COLU
\es\co
AVERAGE/COLUMN						17-APR-1991 JDP
\oc\su
AVERAGE/COLUMN  out = in [start,end] [SUM]
	average image columns
\us\pu
Purpose: 
          Average columns in a selected range of the input image. The output is
          optionally the sum of the columns.
\up\sy
Syntax:
          AVERAGE/COLUMN  out = in [start,end] [SUM]
\ys\pa
          out = resulting 1 dimensional frame
\ap\pa
          in = image to be averaged
\ap\pa
          start = first column, defaulted to the first column of the frame
\ap\pa
          end = last column, defaulted to the last column of the frame
\ap\pa
          SUM = optional parameter to define the output as sum of the columns.
\\
                By default the output is the average of them.
\ap\sa
See also: 
          AVERAGE/ROW, TUTORIAL/AVERAGE
\as\no
Note:  
          First and last columns can be specified in pixels or world coords.
\on\exs
Examples:
\ex
          AVERAGE/COLUMN out = in @1,@10
          Average the columns of frame in.bdf, store result in out.bdf
\xe \sxe
