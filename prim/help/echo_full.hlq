% @(#)echo_full.hlq	19.1 (ESO-IPG) 02/25/03 14:03:18 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      echo_full.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, ECHO/FULL
%.PURPOSE    On-line help file for the command: ECHO/FULL
%.VERSION    1.0  04-JAN-1988 : Creation, KB
%.VERSION    1.1  02-MAY-1991 : Update, KB
%----------------------------------------------------------------
\se
SECTION./FULL
\es\co
ECHO/FULL						04-JAN-1988  KB
\oc\su
ECHO/FULL [levla,levlb]
	show substitutions in MIDAS procedure files
\us\pu
Purpose:  
           Show substitutions in MIDAS procedure files.
\up\sy
Syntax:
           ECHO/FULL [levla,levlb]
\ys\pa
           levla,levlb = procedure levels interval for full echo;
\\
                defaulted to level 1, if entered from terminal;
\\
                defaulted to current level, if executed within a MIDAS
                procedure;
\\
                may also be set to ALL to indicate all levels
\ap\sa
See also:
           ECHO/OFF, ECHO/ON, DEBUG/PROCEDURE, DEBUG/MODULE
\\
           Chapter 3 in MIDAS User Manual, Volume A
\as\no
Note:
           As ECHO/ON, but after any parameter or variable substitution has
           taken place, the command line is displayed again.
\on\exs
Examples:
\ex
           ECHO/FULL 2,4
            Turn full echo on for levels 2, 3 and 4 .
\xe \sxe
