% @(#)write_setu.hlq	19.1 (ESO-IPG) 02/25/03 14:03:51
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      write_setu.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files
%.PURPOSE    On-line help file for the command: WRITE/SETUP
%.VERSION    1.0  10-OCT-1988 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./SETU
\es\co
WRITE/SETUP                                          27-JAN-1994  KB
\oc\su
WRITE/SETUP [setup]
	modify the variables of a Setup
\us\pu
Purpose:
          Modify the variables of a Setup.
\up\sy
Syntax:
          WRITE/SETUP [setup] [par1] [par2] [par3] ... [par_i]
\ys\pa
          setup = name of a Setup
\ap\pa
          par1, par2, ... = parameters which depend upon the `setup' chosen
\ap\sa
See also:
          INITIALIZE/SETUP, READ/SETUP, INFO/SETUP
\as\no
Note:
          Setups are a collection of variables (keywords) which have to be 
          set up before certain, more complex commands can be executed.
\\
          You must use "INFO/SETUP abcdef" first to obtain the exact syntax of
          the WRITE/SETUP command for a specific Setup `abcdef'.
\on\exs
Examples:
\ex
          WRITE/SETUP catalog 2,4 2 NO 22,106 
            Fill the variables of the Setup `catalog' which are required for
            the command EXECUTE/CATALOG. See "INFO/SETUP catalog" for an
            explanation of the syntax.
\xe \sxe

