% @(#)equalihist.hlq	19.1 (ESO-IPG) 02/25/03 14:03:18 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      equalihist.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, EQUALIZE/HISTOGRAM
%.PURPOSE    On-line help file for the command: EQUALIZE/HISTOGRAM
%.VERSION    1.0  05-DEC-1989 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./HIST
\es\co
EQUALIZE/HISTOGRAM					09-NOV-1994  KB
\oc\su
EQUALIZE/HISTOGRAM frame descr itt_name
	perform histogram equalization
\us\pu
Purpose:     
           Perform histogram equalization and load ITT to apply it.
\up\sy
Syntax:      
           EQUALIZE/HISTOGRAM frame [descr] [itt_name] [no_load]
\ys\pa
           frame = name of data frame, defaulted to currently displayed frame
\ap\pa
           descr = name of descriptor where the equalized histogram will be
                 stored; default is HIST_EQ
\ap\pa
           itt_name = name for optional ITT where to save the resulting ITT;
                 defaulted to no ITT
\ap\pa
           no_load = NO if you don't want to load the ITT into the display;
\\
                 defaulted to YES
\ap\sa
See also:
           LOAD/ITT, GET/ITT, MODIFY/ITT, STATISTICS/IMAGE
\as\no
Note:        
           The algorithm uses a histogram of 256 bins stored in the relevant
           descriptors of the frame, as written e.g. by the command 
\\
           STATISTICS/IMAGE frame   .
\\
           If the histogram is "ill formed", e.g. the median is located 
           already in the lowest bin, meaningful equalization cannot be done
           and the `ramp' ITT is produced. ALso, a message is displayed and 
           keyword PROGSTAT(5) set to -1, else (i. e. in case of successful 
           equalization) keyword PROGSTAT(5) = 0.
\\
           If parm. `no_load' is not set to NO, the equalized histogram is 
           sent directly as an ITT to the currently displayed channel. 
\on\exs
Examples:
\ex
           EQUALIZE/HISTO chango 
            Use frame `chango.bdf' to create an equalized histogram, build an
            ITT from it and apply it to the image display.
\xe \ex
           EQUALIZE/HISTO chango ? equal no
            Create an equalized histogram as above, build an ITT from it and
            save that ITT in table `equal.itt'.
\xe \sxe
