% @(#)createlut.hlq	19.1 (ESO-IPG) 02/25/03 14:03:09 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      createlut.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, CREATE/LUT
%.PURPOSE    On-line help file for the command: CREATE/LUT
%.VERSION    1.0  08-SEP-1989 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./LUT
\es\co
CREATE/LUT					25-MAY-1998  KB
\oc\su
CREATE/LUT LUT_table H_specs S_specs I_specs cyclic_flag
	create a colour lookup table
\us\pu
Purpose:      
              Create a colour lookup table using hue, saturation and
              intensity instead of RGB values.
\up\sy
Syntax:       
              CREATE/LUT LUT_table H_specs S_specs I_specs cyclic_flag
\ys\pa
              LUT_table = name of new LUT table
\ap\pa
              H_specs = start,end,increment for hue, 
                   hue in [0.,360.]; defaulted to 0.0,360.0,-1.0,
\ap\pa
              S_specs = start,end,increment for saturation,
                   saturation in [0.,1.]; defaulted to 0.0,1.0,-1.0
\ap\pa
              I_specs = start,end,increment for intensity, 
                   intensity in [0.,1.]; defaulted to 0.0,1.0,-1.0
\ap\pa
              cyclic_flag = CY or NO, 
                   for stopping increments at limit (end of interval) or not;
                   defaulted to CY
\ap\no
Note:         
              A LUT of 256 levels will be created (as a Midas binary table).
\\
              If increments < 0., increments will be set to (end-start)/255
              to obtain 256 values.
\on\exs
Examples:
\ex
              CREATE/LUT mycolours 0.,360.,10. 0.8,1.0,-1. 
                Use default values for intensity, go from 0. to 360. in hue
                and 0.8 to 1.0 in saturation to create LUT `mycolours.lut'.
\xe \sxe
