% @(#)rebin_tt.hlq	19.1 (ESO-IPG) 02/25/03 14:03:39 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      rebin_tt.hlq
%.AUTHOR     MR, IPG/ESO
%.KEYWORDS   MIDAS, help files, REBIN/TT
%.PURPOSE    On-line help file for the command: REBIN/TT
%.VERSION    1.0  29-OCT-1985 : Creation, MR
%----------------------------------------------------------------
\se
SECTION./TT
\es\co
REBIN/TT						29-OCT-1985 MR
\oc\su
REBIN/TT outtab i,d[,b] intab i,d[,b] [func] [param] [intop]
	nonlinear rebin Table to Table
\us\pu
Purpose:    Nonlinear rebinning of 1D data. Table to Table transformation.
\up\sy
Syntax:     REBIN/TT outtab i,d[,b] intab i,d[,b] [func] [parm] [intop]
\ys\pa
            outtab = name of output table
\ap\pa
            intab  = name of input table
\ap\pa
            i      = column reference for independent variable
\ap\pa
            d      = column reference for dependent variable
\ap\pa
            b      = [optional] column reference for binwidth
\ap\pa
            func   = function code (see below) for the mapping of
                     OUTPUT indep.variable to INPUT indep.variable
                     defaulted to LIN
\ap\pa
            param  = parameters for the function used, defaulted to 0.,1.
\ap\pa
            intop  = interpolation option (se below), defaulted to PIX
\ap\no
Note:      
            FUNCTIONS    (parameters A, B, C, ..... )
\\
             LIN     x(old) = A + Bx
\\
             POL     x      = A + Bx +Cxx +  .... (up to 16 coeff)
\\
             INV     x      = A + B/x
\\
	     EXP     x      = A + B*EXP(Cx)
\\
             DEX     x      = A + B*10.**(Cx)
\\
	     LOG     x      = A + B*LOG(Cx)
\\
             DLG     x      = A + B*LOG10(x)
\\
	     IPO     x      = A + B/(x-D) + C/(x-D)**2 + ...
\\
	     U01     x      = user defined function of x.

	     INTOP  (interpolation options)

	     PIX     take fractions of pixel areas
	     LIN     interpolate linearly and add polygons
	     SPG     interpolate with hermite polynomials
                     and integrate with Gaussian formula

	             Recommended option is SPG. LIN is faster
                     and may provide sufficient accuracy whenever
                     well behaved data are rebinned to larger
                     stepsizes. PIX (same speed as LIN) will give
                     adequate results only when rebinning from
                     an oversampled array (very smal bins)
		     into large bins

            Defaults:   REBIN/TT OUT \#1,\#2,\#3 IN \#1,\#2,\#3 LIN 0.,1. PIX
\on\exs
Examples:
\ex
            none
\xe \sxe
