% @(#)outtapfits.hlq	19.1 (ESO-IPG) 02/25/03 14:03:32 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1996 European Southern Observatory
%.IDENT      outtapfits.hlq
%.AUTHOR     PJG, IPG/ESO
%.KEYWORDS   MIDAS, help files, OUTTAPE/FITS
%.PURPOSE    On-line help file for the command: OUTTAPE/FITS
%.VERSION    1.0  17-OCT-1990 : Creation, PJG
%.VERSION    1.1  16-MAY-1991 : catalog can be defaulted, PJG
%.VERSION    1.2  19-NOV-1991 : Improve help + add explicit names, PJG
%.VERSION    1.4  14-DEC-1994 : Physical tape devices, PJG
%.VERSION    1.5  08-JUL-1996 : Add comment on input file, PJG
%.VERSION    1.6  17-JUL-1996 : Note on no-rewinding devices, PJG
%----------------------------------------------------------------
\se
SECTION./FITS
\es\co
OUTTAPE/FITS				       		17-JUL-2002  PJG
\oc\su
OUTTAPE/FITS [catalog[,list]] device [flags] [density,block] 
	write to device in FITS format
\us\pu
Purpose:    
            Write frames from MID_WORK to magtape or disk in FITS format.
\up\sy
Syntax:     
            OUTTAPE/FITS [catalog[,list]] device [flags] [density,block]
\ys\pa
            catalog[,list] = name of catalog containing the names of the
                  frames that will be written to magtape (disk). A list of
                  file numbers in the catalog can be given. Default is 
                  all files in all catalog which are set with the SET/xCAT
                  command. The default for list is file 1-9999.\\
                  It is also possible to specify a single file name
                  but then its extension must be given.\\
                  Catalog or file names which do not start with a letter
                  must be enclosed in "" to be interpreted as names.
\ap\pa
           device = logical tape unit (e.g. TAPE1), physical tape unit (e.g.
                  /dev/nrst8 or host:/dev/nrst1) or prefix of file name 
                  on disk. The default extension for disk files is `.mt'.
                  It is possible to specify a full file name with extension
                  if only one is written.
\ap\pa
           flags = 3-character flag: append, display and cut.
                  append_flag: A(ppend) file to tape,
                               R(ewind) i.e. write from start,
                               N(o append) i.e. write from current location.
                  display flag: F(ull), S(hort) or N(one) 
                  cut flag: C(ut) using LHCUTS(3-4) for frames or display
                            format for tables. 
                  Defaulted to NSN (NOTE: that is NO APPEND mode).
\ap\pa
            density,block = density_of_magtape,blocking_factor, 
                  defaulted to 6250,10. The tape density is used only for 
                  1/2 inch tapes.
\ap\sa
See also:
            OUTDISK/FITS, OUTDISK/SFITS, COPY/II, INDISK/FITS, INTAPE/FITS, 
            CREATE/ICAT, SET/ICAT
\as\no
Note:       
              The default is NO rewind mode which will write new files
            from the current position of tape. This may erase previous
            information on the tape - be careful and check!
              The different parameters may be referenced in any order via
            CATALOG=  DEVICE=  FLAGS=  DENS,BLCK=  
            If writing to magtape, a tape (with write-ring) must be loaded
            on a magnetic tape unit before executing the command.
            To access remote tape drives, the MIDAS tapeserver daemon must
            be installed and run on the host in question.
              For Midas tables (ascii or binary) only up to 768 columns are
	    supported (no limit on rows).
              The data format of the FITS files will be the FITS format 
            closest to the frame data format;
            including BITPIX=-32,-64 (IEEE-FP) and BINTABLE for tables.
            MIDAS frames are normally stored as floating point numbers.
            Such files will be written using the BITPIX=-32,  i.e. IEEE 
            floating point format. 
            If your MIDAS frames are "essentially" 16bit integer frames
            use the command COPY/II first to convert them to 16bit data
            frames and then do the OUTTAPE/FITS to obtain 16bit FITS files.
              FITS files are written with a physical blocking factor of 10 by
            default. Some old FITS readers may NOT accept that
            and require an explicit blocking factor of 1.
              When a physical device name is used, it MUST specify a
            non-rewinding device (e.g. /dev/nrst3) for this command
            to work correctly.  The operating system may itself rewind
            the tape if a rewinding device is given.

              Since version 99NOVpl2.1, the previous (last) parameter TYPE
            has been dropped. Data is always written using the official
            FITS formats (prev. TYPE=O).
            The 'basic' TYPE option in OUTTAPE/FITS was introduced many years 
            ago when the FITS floating point convention still was young and not 
            all FITS readers could be expected to accept it.
            It was alway slightly troublesome as it involved a scaling of 
            floating point numbers to 4-byte integers. If the data had 
            outliers, one could get loss of precision which has happened a
            number of times.  To avoid such problems and since the FITS
            floating point convention is now more than 10 years old, this
            option was removed.
            If you really need to produce such an integer FITS file,
            the following commands will do that:
                 compute/image  scaled_image = (image-{bzero})/{bscale}
                 copy/ii        scaled_image truncated_image I4
                 write/desc     truncated_image BZERO/R/1/1 {bzero}
                 write/desc     truncated_image BSCALE/R/1/1 {bscale}
                 outtape/fits   truncated_image.bdf  FITS_file.fits
\on\exs
Examples:
\ex
            OUTTAPE/FITS * TAPE2 R
              Write all files in all catalogs which have been SET in FITS
              format (in original format e.g. 32 bit IEEE floating point)
              to the magnetic tape mounted on unit 'TAPE2' (from beginning of
              tape on) and with short display. If both image, ascii, table and
              fit catalogs are set all the files they contain will be written.
\xe\ex
            OUTTAPE/FITS mycat DEVICE=tapedat
              Write all images with an entry in the 'mycat' catalog (file 
              `mycat.cat') in FITS format (in original format e.g. 32 bit
              IEEE floating point) to the DAT cartridge tape mounted on
              unit 'tapedat' (from current position on tape) and with short
              display
\xe\ex
            OUTTAPE/FITS caspeca /dev/nrst8 AN
              Skip to end of local magtape with physical name /dev/nrst8,
              indicated by two consecutive EOFs, append all files from 
              catalog caspeca (file `caspeca.cat') in FITS format, do not
              display FITS header
\xe\ex
            OUTTAPE/FITS caspeca,101-220 TAPE8MM AN
              As above but process only the files with entry numbers in the
              interval [101,220] from 'caspeca' image catalog writing them
              to the device 'TAPE8MM'.
\xe\ex
            OUTTAPE/FITS caspeca,101-220 obs:/dev/nrst8 A 
              As above, but writes FITS files to tape mounted on the tape 
              unit: /dev/nrst8 on host 'obs'. NOTE: MIDAS tape server must
              be running on this host.
\xe\ex
            OUTTAPE/FITS caspeca,101-220 data AN
              As above, but convert files to FITS format files
              and store them in files `data0101.mt' to `data220.mt'.
\xe\ex
            OUTTAPE/FITS galaxy.bdf galaxy.fits
              Write the frame `galaxy.bdf' out in FITS format to the disk
              file `galaxy.fits'.
\xe \sxe
