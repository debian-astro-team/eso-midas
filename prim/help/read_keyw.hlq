% @(#)read_keyw.hlq	19.1 (ESO-IPG) 02/25/03 14:03:52 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      read_keyw.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, READ/KEY
%.PURPOSE    On-line help file for the command: READ/KEY
%.VERSION    1.0  02-FEB-1984 : Creation, KB
%.VERSION    1.1  29-MAY-1991 : Modification for wildcard option, KB
%.VERSION    1.2  13-FEB-1992 : add "since" parameter, KB
%----------------------------------------------------------------
\se
SECTION./KEYW
\es\co
READ/KEYWORD					      28-NOV-2002  KB
\oc\su
READ/KEYWORD [key_list] [disp_flag] [Midunit]
	display contents of keywords
\us\pu
Purpose: 
          Display contents of keywords.
\up\sy
Syntax:
          READ/KEYWORD [key_list] [disp_flag] [since] [Midunit]
\ys\pa
          key_list = one or more keywords separated by a comma (no spaces!)
             or 'xyz*' or '*xyz' where all matching keywords are displayed
             or  '<filename' if keyword names are stored in ASCII file;
             defaulted to '*' 
\ap\pa
          disp_flag = '?', if you want the header line with the name,
             type and size of the keyword displayed as well,
                    = 'h', if you do not want this header line
             defaulted to '?'
\ap\pa
          Midunit = optional unit of another Midas session, then the keys are
             read from the keyfile of that session, this session must have the
             same Midas startup directory, i.e. same MID_WORK
\ap\sa
See also:
          PRINT/KEYWORD, WRITE/KEYWORD, COMPUTE/KEYWORD, COPY/KEYWORD,
          COPY/DK, COPY/KD, SHOW/KEYWORD, HELP/KEYWORD, DELETE/KEYWORD
          also chapter 3 of the MIDAS Users Guide, volume A
\as\exs
Examples:
\ex
          READ/KEYWORD IN_A 
            Display contents of keyword IN_A.
\xe\ex
          READ/KEYWORD in_a h
            As above but omit header line.
\xe\ex
          READ/KEYWORD <keywo.dat
            Display contents of all keywords which are in the ASCII file 
            `keywo.dat'. E.g. if `keywo.dat' contains the two records:
            in_a,in_b
            inputi
            contents of keywords in_a, in_b and inputi would be displayed.
\xe\ex
          READ/KEY in*,out* ? zx
            Display contents of all keywords beginning with 'IN' or 'OUT'
            as they are in the Midas session with unit ZX.
\xe \sxe
