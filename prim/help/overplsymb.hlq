% @(#)overplsymb.hlq	19.1 (ESO-IPG) 02/25/03 14:03:33
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      overplsymb.hlq
%.AUTHOR     RHW, IPG/ESO
%.KEYWORDS   MIDAS, help files, OVERPLOT/SYMBOL
%.PURPOSE    On-line help file for the command: OVERPLOT/SYMBOL
%.VERSION    1.0  17-SEP-1988 : Creation, RHW
%.VERSION    1.1  08-NOV-1999 : Inclusion of screen coordinates, RHW
%----------------------------------------------------------------
\se
SECTION./SYMB
\es\co
OVERPLOT/SYMBOL                                                17-SEP-1988  RHW
\oc\su
OVERPLOT/SYMBOL [s_type] [x_coord,y_coord[,wo|,mm|,sc|,no]] [s_size]
	overplot a symbol
\us\pu
Purpose:    
            Overplot a symbol
\up\sub
Subject:    
            Graphics, symbols
\bus\sy
Syntax:     
            OVERPLOT/SYMBOL [s_type] [x_pos,y_pos[,wo|,mm|,sc|,no]] [s_size]
\ys\pa
            s_type = symbol type. The following symbols are possible:
                     0 means no symbol at all; 1 -- dot; 2 -- circle;
                     3 -- square; 4 -- triangle; 5 -- cross (+); 6 -- cross (x);
                     7 -- asterisk; 8 -- star; 9 -- crossed square (x);
                     10 -- crossed square (x); 11 -- losange; 12 -- hor. bar;
                     13 -- vert. bar; 14 -- right arrow; 15 -- arrow up;
                     16 -- left arrow; 17 -- arrow down; 18 -- filled exagon;
                     19 -- filled square; 20 -- filled triangle; 
                     21 -- filled lozenge; default 5 (cross)
\ap\pa
             x_pos,y_pos[,wo|,mm|,sc|,no] = Position of the text in the plot.
                     Positions can be given via te cursor `C' or in world 
                     coordinates (,wo), in mm (,mm). in screen coordinates 
                     (,sc), or in normalized coordinates (,no).\\ 
                     World coordinates refer to the coordinate box of the 
                     axes, defined and plotted by either a plot command or 
                     plot/overplot axes command. The reference point (origin) 
                     for both screen coordinates and mm coordinates is the 
                     ower left corner (0,0). Screen coordinates are supported 
                     for graphics and display windows only.\\
                     By default, i.e. if no units are given, coordinates 
                     are interpreted as a world coordinates.\\
                     In case NO coordinate pair is entered the graphics cursor 
                     will appear, allowing you to enter the position with the 
                     left mouse button. Exit with the right mouse button.

\ap\pa
            s_size = multiplication factor for size of the symbol; default 1.0
\ap\no
Note:       
            The command allows to overplot a symbol outside the graphics frame.
            In such a case, if one wants to input coordinates an extrapolation
            of the x- and/or y-axis should be applied.
\\
            The command allows you to re-iterate: as long as the exit button 
            isn't pressed you can choose another position of the symbol with 
            the left button.
\on\see
See also:   
            PLOT/TABLE, PLOT/ROW, PLOT/COLUMN, SET/GRAPHICS, SHOW/GRAPHICS
\ees\exs
Examples:
\ex
            PLOT/ROW myframe @125 @100,@440
\xe\ex
            OVER/SYM 9 4010,53 2.0
\xe \sxe

