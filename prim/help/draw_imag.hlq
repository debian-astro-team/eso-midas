% @(#)draw_imag.hlq	19.1 (ESO-IPG) 02/25/03 14:03:17 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      draw_imag.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, DRAW/IMAGE
%.PURPOSE    On-line help file for the command: DRAW/IMAGE
%.VERSION    1.0  02-MAR-1987 : Creation, KB
%.VERSION    1.1  24-JUL-1991 : Update, KB
%----------------------------------------------------------------
\se
SECTION./IMAG
\es\co
DRAW/IMAGE						22-JAN-1993  KB
\oc\su
DRAW/IMAGE frame [chanl] [scale] [center] [cuts] [over] [iaux] [fix]
	draw intensities of a line of an image into display channel
\us\pu
Purpose:  
          Draw intensities of a line of an image into a display channel.
\up\sy
Syntax: 
          DRAW/IMAGE frame [chanl] [scale] [center] [cuts] [over] [iaux] [fix]
\ys\pa
          frame = name of image frame
\ap\pa
          chanl = image display channel no.; defaulted overlay channel
\ap\pa
          scale = scaling in x-dimension of line in image frame which is drawn,
             (meaning like in LOAD/IMAGE); defaulted to 1
\ap\pa
          center = `xcent,yline' the coordinates of center x-pixel and line
             (if 2-dim frame) for drawing, for central x-pixel you can also
             enter the character 'C';
\\
             defaulted to `c,@1' (x-center pixel and 1. line)
\ap\pa
          cuts = `lo,hicut' the cut values for intensity scaling, if omitted
             the cut values from descriptor LHCUTS are used
\ap\pa
          over = Y(es), draw over previously loaded image without clearing;
\\
             if N(o), clear channel first and then draw;
\\
             default = Y (also any input other than N is interpreted as Y !)
\ap\pa
          iaux = `yscal,scroff,colo,angl' defining the scaling in y, offset
             of plot from bottom of screen, color of plot, and angle of
             plot with base line (in degrees);
\\
             defaulted to `y-screensize,0,255,0.0'
\ap\pa
          fix = `framex,screenx' the x-pixel number of the image and the screen
             which should coincide; i.e. the plot is positioned on the screen
             in such a way that pixel `framex' is at screen pixel `screenx'.
\\
             the pixels are entered as integers, e.g `framex' in [1,NPIX(1)]
             and `screenx' in [0,x-screensize]
\\
             If this parameter is given it overrides the center specifications
             of parameter `center'!
\ap\no
Note:   
          The parameters described above can also be accessed via
          FRAME=, CHANL=, SCALE=, CENTER=, CUTS=, OVER=, IAUX=, FIX=
 \ 
\\
          The cut values used for drawing are not written back into
          descriptor LHCUTS .
\\
          The following colours are supported (via name or number) in X11:
\\
          Red(3), Green(4), Blue(5), White(2), Black(1), Yellow (6),
          Magenta(7), Cyan(8) - "colour" 0 is used to erase. All values in
          [9,255] are interpreted as White.
\on\sa
See also:
          DRAW/..., LOAD/IMAGE, CLEAR/OVERLAY, SET/OVERLAY
\as\exs
Examples:
\ex
          DRAW/IMAGE cabra
            Draw first line of image `cabra.bdf' with central x-pixel in
            screen center into overlay channel, use existing cut values.
\xe\ex
          DRAW/IMAGE paloma 0 cent=c,@100 iaux=200,300
            Draw line no. 100 of 2-dim image `paloma.bdf' in channel 0 with
            central x-pixel in screen center and the plot starting in line 300
            on screen.
\xe \sxe
