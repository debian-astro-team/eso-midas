% @(#)crossr.mlc	19.1 (ESO-IPG) 02/25/03 14:03:12 
\leftpage
\noqualifier
\se
SECTION./
\es\co
CROSSREF						30-SEP-1997  KB
\oc\su
CROSSREF labl1 labl2 labl3 labl4 labl5 labl6 labl7 labl8
	define cross reference labels for the 8 parameters
\us\pu
Purpose:   define cross reference labels for the 8 parameters
\up\sy
Syntax:    CROSSREF labl1 labl2 labl3 labl4 labl5 labl6 labl7 labl8
\ys\pa
           labl1,,labl8 = cross reference labels (max. 18 characters)
                  for the parameters you may pass to this MIDAS procedure
\ap\no
Note:      This must (!) be the first command in that procedure.
\\
           When calling the procedure the labels may be truncated as long as
           they are unique.
\on\exs
Examples:
\ex
          CROSSREF FILEA FILEB METHOD
            If this is the first executable command line in a procedure,
            then the MIDAS commands:
\xe\ex
          @@ FILTER GALAXY FILTGAL SMOOTH
            and
\xe\ex
          @@ FILTER meth=SMOOTH FILEA=GALAXY FILEB=FILTGAL
            and
\xe\ex
          @@ FILTER METHOD=SMOOTH FILEA=GALAXY FILEB=FILTGAL
            are equivalent and will both set parameters P1 to GALAXY,
            P2 to FILTGAL and P3 to SMOOTH.
\xe\ex
          @@ FILTER P3=SMOOTH P1=GALAXY P2=FILTGAL
            then you obtain the same result as above, but no CROSSREF command
            is necessary inside the procedure
\xe \sxe
