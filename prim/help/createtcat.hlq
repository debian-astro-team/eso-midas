%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      createtcat.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, CREATE/TCAT
%.PURPOSE    On-line help file for the command: CREATE/TCAT
%.VERSION    1.0  16-JAN-1990 : Creation, KB
%.VERSION    1.1  17-MAR-1992 : Update, KB
%----------------------------------------------------------------
\se
SECTION./TCAT
\es\co
CREATE/TCAT                                             23-FEB-2009  KB
\oc\su
CREATE/TCAT [catname] [dir_spec] [descr]
	create a catalog of tables in the current directory
\us\pu
Purpose: 
          Create a catalog of tables in the current directory.
\up\sy
Syntax:
          CREATE/TCAT [catname] [dir_spec] [descr]
\ys\pa
          catname = name of catalog, defaulted to `tcatalog.cat'
\ap\pa
          dir_spec = file specifications as used in the VMS `$ DIRECTORY'
              or UNIX `$ ls' command;
              or table,label if the filenames are specified in a column of a 
              Midas table;
              or file.ascii if the filenames are specified in the records of
              ASCII file file.ascii (i.e. the file type must be `.ascii')
              if set to `NULL', the catalog is created without entries;
              defaulted to `*.tbl'
\ap\pa
          descr = name of char. descr. used for the Ident field in the
              catalog records (you can sort the catalog according to the
              Ident field);
              defaulted to IDENT
\ap\no
Note:  
             The file type of a MIDAS table catalog is `.cat' . 
	  The catalog contains for each table frame an entry with:
          sequence no., name, Nocols, Norows
          The no. of entries in the created table catalog will be displayed 
	  and also stored in the integer keyword OUTPUTI(10).
             If the catalog 'xyz' is the currently active table catalog (use
	  the command "SET/TCAT xyz" to do that), it is updated automatically
	  by Midas. I.e. creating a new table will also add a new entry to 
	  the catalog, whereas the command ADD/TCAT has to be used explicitly
	  to add an entry to any other table catalog.
             A table with an entry in a table catalog can be accessed either
	  via its name or via \#NO_catname, where NO is its sequence
          number in the table catalog catname. E.g. use #12_mytcat 
	  to access entry 12 in table catalog mytcat.cat.
             Reuse the command CREATE/TCAT whenever your table catalog does 
	  not reflect anymore the actual directory because e.g. you executed
	  some operating system commands like `$ COPY' (for VMS) or `$cp ' 
	  (for Unix) !
             With the command STORE/FRAME you can loop over a table catalog
	  in a Midas procedure in a straight forward way.
\on\sa
See also: 
          READ/TCAT, SET/TCAT, CLEAR/TCAT, SHOW/TCAT, DELETE/TCAT
          ADD/TCAT, SUBTRACT/TCAT, SORT/TCAT, SEARCH/TCAT, PRINT/TCAT
          CREATE/ICAT, CREATE/FCAT, CREATE/ACAT
          STORE/FRAME, @a catalsync
\as\exs
Examples:
\ex
          CREATE/TCAT
            Create the table catalog `tcatalog.cat' with entries for all
            table files with filetype `.tbl' in your current directory. Use
            descriptor IDENT for the Ident field of the catalog.
\xe\ex
          CREATE/TCAT torreon null
            Create the table catalog `torreon.cat' with initially no entries;
            entries may be filled via the ADD/TCAT command later on
\xe\ex
          CREATE/TCAT mazatlan n*.tbl
            Create the table catalog `mazatlan.cat' with entries for all table
            files matching n*.tbl, e.g. n.tbl, n123.tbl net.tbl ...
\xe\ex
          CREATE/TCAT mazatlan n*.tbl TLABL001
            As above, but use char. descr TLABL001 (i.e. the label of the
            first table column) for the Ident field of the catalog.
\xe\ex
          CREATE/TCAT juarez matamoros,:files
            Create the table catalog `juarez.cat' with entries for all table
            files stored in column :files of table `matamoros.tbl'.
\xe\ex
          create/tcat guadalajara n*.tbl,z*
            Create the table catalog `guadalajara.cat' with entries for all
            table files matching n*.tbl and z* .
\xe \sxe
