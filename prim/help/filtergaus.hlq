% @(#)filtergaus.hlq	19.1 (ESO-IPG) 02/25/03 14:03:21 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      filtergaus.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, FILTER/GAUSSIAN
%.PURPOSE    On-line help file for the command: FILTER/GAUSSIAN
%.VERSION    1.0  07-JUN-1990 : Creation, KB
%.VERSION    1.1  31-MAY-1991 : Remove edge comments, KB
%----------------------------------------------------------------
\se
SECTION./GAUS
\es\co
FILTER/GAUSS						18-JUL-1995   KB
\oc\su
FILTER/GAUSS in out [radx,rady] [gauss_specs] [subima] [filtnam] [options]
	use Gaussian filter on an image
\us\pu
Purpose: 
            Use Gaussian filter on an image.
\up\sub
Subject: 
            Image smoothing
\bus\sy
Syntax: 
            FILTER/GAUSS in out [radx,rady] [gauss_specs] [subima]
                         [filtnam] [options]
\ys\pa
            in = input image
\ap\pa
            out = result image
\ap\pa
            radx,rady = radius in x and y.
                Radius is the number of pixels "around" the central pixel
                to be included in the neighbourhood.
                Total no. of pixels in neighbourhood is (2*radx+1)*(2*rady+1);
                defaulted to 9,9 (= 19*19 neighbourhood)
\ap\pa
            gauss_specs = x-mean,x-sigma and y-mean,y-sigma;
\\
                defaulted to 9,3,9,3 pixels;
\\
                for 1-dim case specify xmean,xsigma only
\ap\pa
            subima = optional specification of a subimage on which the filter
                will be applied, may be either
\\
                [xstart,ystart:xend,yend] (see page 3.4 in the MIDAS User
                manual, chapter 3) to indicate the limits of the subimage
                (with coordinates given according to the MIDAS standard), or
\\
                CURSOR if subimage will be chosen interactively;
\\
                table_name if the table "table_name" holds the endpoints of
                the subimage(s),
\\
                defaulted to the complete input frame
\ap\pa
            filtnam = name for the Gaussian filter used; if not given the
                filter is not kept. This option is a debugging tool.
\ap\pa
            options = flag of 2 characters for options;
\\
                (1) L or N for loop or no loop,
\\
                (2) D or N for display of smoothed area or no display;
\\
                defaulted to NN
\ap\see
See also:
            FILTER/DIGITAL, FILTER/SMOOTH, FILTER/MEDIAN, TUTORIAL/FILTER
\ees\no
Note:
            The Gaussian filter will have (2*radx+1)*(2*rady+1) pixels, lower
            left corner will have start values 0.,0. and stepsize is 1.,1.
\\
            New points are calculated as outpix = SUM(inpix*gausspix) where the
            SUM is taken over the pixels in the neighbourhood determined by
            radx and rady.
\\
            If the subimage option is chosen, it must be at least as big as the
            Gaussian filter which has  (2*radx+1) * (2*rady+1) pixels.
\\
            Note, that the flux is not conserved.
\on\exs
Examples:
\ex
            FILTER/GAUSS gordo flaco
             Use the default Gaussian 19*19 filter on image `gordo.bdf' and 
             store resulting image into frame `flaco.bdf'.
\xe\ex
            FILTER/GAUSS in out 7,7 7,2,7,2 CURSOR
             Use a 15*15 Gaussian filter with x-mean = 7,x-sigma = 2,
             y-mean = 7, y-sigma = 2 on cursor selected subframe of image
             `in.bdf' and store resulting image into frame `out.bdf'.
\xe \sxe 
