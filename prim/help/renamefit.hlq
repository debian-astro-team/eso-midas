% @(#)renamefit.hlq	19.1 (ESO-IPG) 02/25/03 14:03:40 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      renamefit.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, RENAME/FIT
%.PURPOSE    On-line help file for the command: RENAME/FIT
%.VERSION    1.0  03-MAY-1985 : Creation, KB
%.VERSION    1.1  29-MAY-1991 : Update, KB
%----------------------------------------------------------------
\se
SECTION./FIT
\es\co
RENAME/FIT						04-NOV-1994  KB
\oc\su
RENAME/FIT old new [history] [overwrite]
	rename a fit file
\us\pu
Purpose: 
          Rename a fit file.
\up\sy
Syntax:
          RENAME/FIT old new [history] [overwrite]
\ys\pa
          old = old name of fit frame
\ap\pa
          new = new name of fit frame
\ap\pa
          history = flag for update of HISTORY descriptor;
\\
              if NO, the history descriptor of the new frame is not updated;
\\
              defaulted to YES
\ap\pa
          overwrite = flag for overwrite check;
\\
              if CONFIRM, we check, if a file with the new name already exists
              and if so, ask for confirmation before overwriting it;
\\
              defaulted to NO_CONFIRM
\ap\sa
See also:
          RENAME/IMAGE, RENAME/TABLE
\as\no
Note: 
          The parameters may also be referenced via:
\\
          OLD=  NEW=  HISTORY=  OVERWRITE=
\\
          The file is renamed and if a fitfile catalog is enabled its entry
          in there is updated as well.
\on\exs
Examples:
\ex
          RENAME/FIT ballena tiburon
           Rename the fit file ballena.fit to tiburon.fit, if tiburon.fit
           already exists it is overwritten (Unix) or a new version 
           created (VMS).
           The descriptor HISTORY of tiburon.fit is updated.
\xe \ex
          RENAME/FIT gato gata ? confirm
           Rename fit file gato.fit to gata.fit, if gata.fit already exists
           we ask for confirmation before actually overwriting that file.
           If so, descriptor HISTORY of gata.fit is updated.
\xe \sxe
