% @(#)debug_modu.hlq	1.1 (ESO-IPG) 7/9/91 13:27:05 
\rightpage
\se
SECTION./MODU
\es\co
DEBUG/MODULE                                    14-JAN-2003  KB
\oc\su
DEBUG/MODULE [low_lev,hi_lev] [switch]
	run MIDAS\ modules in debug mode
\us\pu
Purpose:
          Run Midas (executable) modules in debug mode.
\up\sub
Subject:
          Debugging
\bus\sy
Syntax:
          DEBUG/MODULE [low_lev,hi_lev] [switch]
\ys\pa
          low_lev,hi_lev = \\ defines interval of levels, only if the Midas
              module is executed at a level inside that interval, it is
              run in debug mode. Modules which are executed at a different
              level are not (!) run in debug mode.
              Defaulted to 1,1.
\ap\pa
          switch = ON, NO, TIME or OFF;
              ON - run module with debugger;
              NO - do not execute the module, i.e. continue with procedure;
              TIME - run module normally + show time before and after execution
              OFF - disable debug mode, execute module normally;
              defaulted to ON
\ap\sa
See also: 
          DEBUG/PROCEDURE
\as\no
Note:  
          'Levels' are the levels at which the relevant Midas procedures
          containing the module to be debugged are executed.
          To turn off debug mode at all levels, enter simply  DEBUG/MODU OFF .
          If switch = TIME, the times are also stored in the MIDAS logfile.

	  Since the 02SEP release the char. keyword array MID$DEBUG (of three
	  40 char. strings) is used to store all info for the chosen debugger:
	  MID$DEBUG(1) holds the name of the debugger, e.g. /usr/bin/gdb
          MID$DEBUG(2) holds any debug flags (entered before the executable)
          MID$DEBUG(3) holds any options (following the executable)
	  For example, to use "valgrind" on a Linux system and send all its
	  output to a file valgrind.log, you have to enter the command line:
	     $ valgrind --logfile-fd=9 my_executable 9>valgrind.log
	  in Linux.
	  To achieve this inside Midas, set MID$DEBUG as follows:
	  (1) = "/usr/bin/valgrind"
	  (2) = "--logfile-fd=9"
	  (3) = "9>valgrind.log"
\on\exs
Examples:
\ex
          DEBUG/MODU  1,2
            Run all Midas modules which are executed in a procedure at level 1
            or level 2 in debug mode.
\xe\ex
          DEBUG/MODU off
            Turn debug mode off for all levels.
\xe\ex
          DEBUG/MODU 2,2 no
            Skip all executable modules at procedure level 2.
\xe \sxe
