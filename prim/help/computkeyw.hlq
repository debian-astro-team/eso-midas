%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2008 European Southern Observatory
%.IDENT      computkeyw.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, COMPUTE/KEYWORD
%.PURPOSE    On-line help file for the command: COMPUTE/KEYWORD
%.VERSION    1.0  06-APR-1990 : Creation, KB
%.VERSION    1.2  29-APR-1992 : Additional functions, KB
%----------------------------------------------------------------
\se
SECTION./KEYW
\es\co
COMPUTE/KEYWORD						27-OCT-2008  KB
\oc\su
COMPUTE/KEYWORD key = arithmetic_expression
	compute values of a keyword
\us\pu
Purpose: 
          Compute values of a keyword or compute special function.
\up\sy
Syntax:    
          COMPUTE/KEYWORD key = arithmetic_expression
\ys\pa
          key = keyword with optional parentheses to indicate which element
              of the keyword shall receive the result of the calculation.
              This keyword has to exist already!
\ap\pa
          arithmetic_expression = expression with up to 8 operands, which may
              be functions or/and keywords or/and constants in the usual
              algebraic notation. 
              The operators +, -, *, / and ** are supported for numerical
              keywords, only  // (= concatenation) is supported for character
              keywords
\ap\sa
See also:
          READ/KEYWORD, WRITE/KEYWORD, COPY/KEYWORD, COMPUTE/IMAGE
          Chapter 3 of the MIDAS Users Guide, Vol. A
\as\no
Note: 
             Arithm. expressions may have operands of different type,
          whenever two operands of different type are involved they are
          converted to the more precise type (integer -> real -> double). 
          Once a final result is obtained (and only then!) this value will 
          be converted to the type of the result key.
          That enables pure integer arithmetic if all operands are integer
          as well as the result keyword.
             Supported functions for COMPUTE/KEYWORD are:
          M$ABS(arg) returns absolute value of integer/real key `arg' as
            integer/real
          M$AGL(arg) returns contents of AGL definitions file `agldevices.dat'
            related to char. keyword `arg' as an ASCII string. This function is
            useful if you have procedures working with different output devices.
          M$EXIST(arg) returns 1/0, if file contained in character key `arg'
            exists or not
          M$EXISTC(arg1,arg2) 
	    returns -2, if table contained in char. key `arg1' doesn't exist, 
	    returns the physical number of the column contained in char. 
	    key `arg2' of the table `arg1', 
	    returns -1 if the column doesn't exist
          M$EXISTD(arg1,arg2) returns 1/0, if descriptor contained in char.
            key `arg2' of file contained in char. key `arg1' exists or not
          M$EXISTK(arg) returns 1/0, if keyword contained in char. key `arg'
            exists or not
          M$FILTYP(arg1,arg2) returns a type_no for file name contained in 
            char. key `arg1'. Type_no = 1 for an image, = 2 for a table,
            = 3 for a fitfile, = 9 for a catalog, and = 0 otherwise. 
	    If the file name does not include a file type, the type definition
	    stored in char. key `arg2' is appended to the file name. 
	    If you don't want to modify the file name, set arg2 to " ".
            A special option can be used, to test if a file is in FITS format
            or not: M$FILTYP(arg1,"?fits") returns 1 if `arg1' is a FITS file 
            and returns 0 if not.
          M$FTSET(arg) returns 1 or 0 if a filename stored in char. key `arg'
            contains a filetype or not
          M$INDEX(arg1,arg2) returns index of char. keyword `arg2' in 
	    char.  keyword  `arg1' as integer value (identical to the INDEX
	    function of FORTRAN 77, i.e. first index is 1, not 0 as in C)
          M$INDEXB(arg1,arg2) same as above but search is done backwards
            starting at the end of the string
          M$ISODATE() return ISO date + time string, i.e. result contains 
            current date and time as: yyyy-mm-ddThh:mm:ss
          M$LEN(arg) returns length of operand in character key `arg' until the
            first blank or end of string, e.g. for argument `abcd' 4 is returned
            and for `abc de' 3 will be returned
          M$LN, M$LOG10, M$EXP, M$SIN, M$COS and M$TAN(arg), M$SQRT, 
                M$ASIN, M$ACOS and M$ATAN(arg) 
            return real (double prec.) results for `arg' an integer or real 
            (double prec.) keyword (angles are always expressed in degrees)
          M$LOWER(arg) returns contents of char. keyword `arg' in lower case
          M$NINT(arg) returns nearest integer of real key `arg'
          M$PARSE(arg1,arg2) returns the no. of substrings contained in char.
            key `arg1' (separated by a special char. - no blank!). 
	    These substrings are stored in local char. keywords which are named
	    `arg2'01, `arg2'02, ... 
	    I.e. using content of char. key `arg2' as root.
            The separating char. is stored in char. keyword MID$SPEC(10:10),
            blank is defaulted to ',' (comma).
          M$REPLA(arg1,arg2,arg3) replaces all occurrences of pattern `arg2'
            in `arg1' with pattern `arg3' and returns that string;
            all args are char. keywords or strings.
          M$SECS() returns current time as no. of seconds elapsed since
            1st Jan. 1970 as an integer (this number is also used as timestamp
            for all updates of keywords)
          M$STRCMP(arg1,arg2) does exact string comparison of char.str. arg1,
	    arg2 and returns the same integer result as the C-lib function 
	    strcmp(arg1,arg2) would do, i.e. if arg1 and arg2 are equal this
	    function returns 0
          M$STRLEN(arg) returns string length of operand in char. key `arg',
            thus for argument `abcd' it returns 4, for `abc de' 6 will be
            returned; if the string contains only blanks 0 will be returned
          M$SYMBOL(arg) returns the translation of DCL symbol (VMS) or shell
            variable (Unix) contained in char. keyword `arg' as character string
          M$SYSTEM(arg) executes the command  contained in char. keyword `arg'
            in the host system (VMS, UNIX) and returns the exit status of that
            command in the host system
          M$TIME() returns current time as ASCII string (30 characters)
          M$TIME(op1) - op1 = 1, result contains current date as yyyy-mm-dd
                 op1 = -1, result contains yesterday's date as yyyy-mm-dd
          M$TNULL(table,column,row) returns 1, if table element is NULL,
	    else 0 is returned
	    since  M$TNULL(table,column,row) is just a wrapper for
            M$VALUE(table,column,row) the same reservations apply as stated
	    below for M$VALUE function (OJO).
          M$TRIM(arg) removes all blanks in front and behind char. arg
            and returns that string
          M$TSTNO(arg) returns 1/0, if character key `arg' is a number or not
          M$UPPER(arg) returns contents of char. keyword `arg' in upper case
       OJO:
          The M$VALUE function is special in the following sense:
          > The arguments are not keywords but the other Midas data structures.
          > It's purpose is to provide access to the elements of these
          > structures in binary format, i.e. to avoid the conversion from 
          > binary to ASCII (with its potential precision problems) inherent
          > in the symbol substitution via {xyz}.
          > Furthermore, no functions or arithm. expressions are possible as
          > arguments for M$VALUE.
          M$VALUE(imag[x,y,z]) provides a pixel of an image as real value;
          M$VALUE(imag,descr) provides a descriptor element of a frame 
            according to the descriptor type;
          M$VALUE(table,column,row) provides a single table element according
            to the element type; if this table element is NULL (not set),
	    the user defined Null from keyword NULL(2) is returned instead,
	    and also keyword MID$INFO(8) is set to 1 (else it's 0)

 	  As a short form of this command you may omit the first string
          COMPUTE/KEYWORD. Then it's called an `immediate command'.
\on\exs
Examples:
\ex
          COMPUTE/KEYWORD INPUTI(3) = 7*OUTPUTI(12)
            Multiply the contents of element 12 of key OUTPUTI by 7 and store
            the result into element 3 of key INPUTI.
\xe\ex
          inputi(3) = 7 * outputi(12)
            short form of the command above ...
\xe\ex
          COMPUTE/KEYWORD INPUTI = M$INDEX(IN_A,".tbl")
            Test, if key IN_A holds table name.
\xe\ex
          INPUTC = M$TIME()
            Store current time in ASCII into char. keyword INPUTC. Note, that
            the parentheses are necessary.
\xe\ex
          INPUTI(6) = M$EXIST(P1)
            Depending upon if the file, the name of which is stored in key P1,
            exists or not, INPUTI(6) will be set to 1 or 0.
\xe\ex
          INPUTD(3) = M$EXP(INPUTD(10))
            If INPUTD(10) = val, calculate e**val and store it into INPUTD(3).
\xe\ex
          inputd(3) = m$value(galaxy,step(2))
            Store binary value of descr. STEP(2) of image `galaxy.bdf'
            into keyword inputd(3).
            This is independent of the SET/FORMAT command, so it should be
            used rather then: inputd(3) = {galaxy,step(2)}
\xe\ex
          inputr(11) = 11.1 + m$value(values,:good,@10)
            Add 11.1 to the table element in row 10 of column labeled :good
            of table `values.tbl' as binary value into keyword inputr(11).
            Again, this is more precise then using symbol substitution, like:
            inputr(11) = 11.1 + {values,:good,@10}
\xe\ex
	  inputc = "a,b,c,d"
          inputi(3) = m$parse(inputc,"lola")
            Will create 4 local keywords lola01, ..., lola04 containing the
            strings "a", "b", "c" and "d"; keyword INPUTI(3) is set to 4.
            The command with m$parse is supposed to be inside a Midas procedure.
\xe\ex
          inputi(1) = 5/2
            The integer operands 5 and 2 are used in pure integer arithmetic
            with an integer result of 2 .
\xe\ex
          inputr(1) = 5/2
            Since the result keyword is of type real, we'll get the expected
            result of 2.5 .
\xe\ex
          write/key kbuf/c/1/400 " " all
          kbuf = "abc"
            By default the first and last element of the keyword is used, 
            however, since the string "abc" is only 3 chars. long, only the
            first 3 elements of `kbuf' are filled.
          kbuf(5:9) = "Garching"
            The string "Garching" is truncated to 5 chars. to fill only the
            indicated elements 5-9 of `kbuf'.
\xe\ex
          write/key cbuf/c/1/500 " " all
          cbuf = "123"
            This will lead to an error, since Midas tries to fill cbuf(1:500)
            with the stuff on the right hand side, and we can modify at most
            400 chars. in a COMPUTE/KEYW command.
          cbuf(1:3) = "123"
            That is the correct syntax to fill the first 3 elements of `cbuf'.
\xe\ex
          in_a = "Windows goes Linux"
          inputc = m$repla(in_a,"in","z")
            Then keyword INPUTC will contain the string "Wzdows goes Lzux".
\xe\ex
          write/keyw in_a "ls -l *.tbl | wc"
          inputi = M$SYSTEM(in_a)
            Assuming we're working on a Unix system, this will count the 
            no. of Midas table files in the current directory and return the
            exit status of the `ls' command in integer keyword INPUTI(1).
\xe\ex
          outputi = m$tnull(values,:bad,@4)
            Test if table element in row 4 of column labeled :bad
            of table `values.tbl' is NULL (outputi = 1) or not (outputi = 0).
\xe \sxe
