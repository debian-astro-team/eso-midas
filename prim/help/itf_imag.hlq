% @(#)itf_imag.hlq	19.1 (ESO-IPG) 02/25/03 14:03:27 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      itf_imag.hlq
%.AUTHOR     MR, IPG/ESO
%.KEYWORDS   MIDAS, help files, ITF/IMAGE
%.PURPOSE    On-line help file for the command: ITF/IMAGE
%.VERSION    1.0  02-JUNE-1986 : Creation, MR
%----------------------------------------------------------------
\se
SECTION./IMAG
\es\co
ITF/IMAGE						02-JUNE-1986  MR
\oc\su
ITF/IMAGE inframe table coli,colo scal outframe
	ITF\ correction
\us\pu
Purpose:     
           Apply Intensive Transformation Function correction to frames
           using a calibration table.
\up\sub
Subject:     
           Intensity transformation, calibration.
\bus\sy
Syntax:      
           ITF/IMAGE inframe table coli,colo scal outframe
\ys\pa
           inframe   =   name of input data frame to be transformed
\ap\pa
           table     =   name of calibration table containing the
                         intensity transformation
\ap\pa
           coli      =   column with input intensities
\ap\pa
           colo      =   column with output intensities
\ap\pa
           scal      =   scaling for input column of table
\ap\pa
           outframe  =   name of output frame
\ap\out
Output:      
           outframe
\tuo\no
Note:        
           The output values are linearly interpolated from the
           intensities in the table.
\on\exs
Examples:
\ex
           ITF/IMAGE INPUT ITF \#1,\#2 1 OUT
\xe \sxe
