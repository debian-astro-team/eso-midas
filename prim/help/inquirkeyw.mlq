\rightpage
\se
SECTION./KEYW
\es\co
INQUIRE/KEYWORD						05-MAY-1992  KB
\oc\su
INQUIRE/KEYWORD key [prompt_string] [flush_opt]
	get terminal input in a MIDAS procedure
\us\pu
Purpose:
          Get terminal input in a MIDAS procedure.
\up\sy
Syntax: 
          INQUIRE/KEYWORD key [prompt_string] [flush_opt]
\ys\pa
          key = complete keyword specification;
              either name/type/first_elem/noval, e.g. INPUTI/I/1/2
              or only keyword name, e.g. INPUTI, if the keyword exists already;
              as many as 'noval' data values will be written into the keyword
              'name' of given type, beginning at the first element (this
              is 1 if just keyword name is given).
\ap\pa
          prompt_string = optional prompt string
\ap\pa
          flush_opt = optional flag: if set to `FLUSH' the input buffer is
              flushed, before prompting for the input, i.e. no type-ahead is
              possible
\ap\see
See also:
          WRITE/KEYWORD
\ees\no
Note:  
          If the prompt string contains blanks, it must be enclosed in 
          quotation marks.
          The procedure does not continue until the input from the terminal
          has been entered (i.e. there is no timeout).
          If you do not want to change the contents of the keyword,
          just hit RETURN. The keyword AUX_MODE(7) will contain the number
          of values entered.
\on\exs
Examples:
\ex
          INQUIRE/KEY IN_A "Enter name of input image:"
          The procedure waits until user enters input and hits RETURN.
          Keyword in_a will contain that input.
\xe\ex
          INQUIRE/KEY petrita/c/6/5 "Give me a name:" flush
          Will change the contents of petrita(6:10) with the user input; the
          input buffer is emptied before displaying the prompt string.
\xe\ex
          INQUIRE/KEY inputr "Enter data values: "
          Assuming the user enters the following input terminated by RETURN:
          16,2.3,-7.8
          then the first 3 elements of keyword inputr are set to 16.0,2.3,-7.8;
          AUX_MODE(7) will be set to 3.
\xe \sxe
