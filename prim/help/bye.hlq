% @(#)bye.hlq	19.1 (ESO-IPG) 02/25/03 14:02:59 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      bye.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, BYE/
%.PURPOSE    On-line help file for the command: BYE/
%.VERSION    1.0  25-MAY-1984 : Creation, KB
%.VERSION    1.1  04-OCT-1991 : Update, KB
%----------------------------------------------------------------
\noqualifier
\se
SECTION./
\es\co
BYE							06-DEC-1993  KB
\oc\su
BYE [proc]
	terminate a MIDAS session + return to the host system 
\us\pu
Purpose: 
	  Terminate a MIDAS session + return to the host system 
\up\sy
Syntax: 
          BYE [proc]
\ys\pa
          proc = name of optional Midas procedure you want to execute before
               leaving the Midas environment;
\\
               defaulted to logout
\ap\sa
See also:
          Chapter 3 of MIDAS Users Manual, volume A
\\
          host system commands: SETMIDAS, INMIDAS, GOMIDAS (for VMS)
\\
          host system commands: $setmidas, $inmidas, $gomidas (for Unix)
\as\no
Note:  
          After the last command in the user procedure `proc' MIDAS is
          terminated automatically. I.e. in that procedure you don't need
          another command BYE.
\\
          If you have a procedure named `logout.prg' in your MID_WORK
          directory, that procedure is executed whenever you get out of
          Midas via BYE.
\on\exs
Examples:
\ex
          BYE 
            Terminate Midas. If there is a procedure `logout.prg' in MID_WORK
            it is executed before exiting Midas.
\xe \ex
          BYE lichterfelde
            Execute procedure `lichterfelde.prg' and terminate Midas.
\xe \sxe
