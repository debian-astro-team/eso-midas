% @(#)set_curs.hlq	19.1 (ESO-IPG) 02/25/03 14:03:43 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      set_curs.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, SET/CURSOR
%.PURPOSE    On-line help file for the command: SET/CURSOR
%.VERSION    1.0  22-SEP-1988 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./CURS
\es\co
SET/CURSOR					   29-OCT-1996  KB
\oc\su
SET/CURSOR [curs_no] [curs_form] [curs_coords] [flag]
	set cursor form and position
\us\pu
Purpose: 
          Set cursor form and position.
\up\sy
Syntax: 
          SET/CURSOR [curs_no] [curs_form] [curs_coords] [flag]
\ys\pa
          curs_no = 0, 1 or 2 for cursor 1, 2 or both cursors;
\\
              defaulted to 0
\ap\pa
          curs_form = defines the cursor(s) form as
\\
              (a) C_HAIR for large cross hair
\\
              (b) CROSS,WHITE or CROSS,BLACK for white or black cursor cross
\\
              (c) OPEN_CROSS,WHITE or OPEN_CROSS,BLACK for white or black
                  open cursor cross
\\
              (d) RECTANGLE to use both cursors for a white rectangle
\\
              (e) CIRCLE to use both cursors for up to 3 circles
\\
              defaulted to CROSS,WHITE
\ap\pa
          curs_coords = optional string of cursor coordinates;
\\
              x,y or x1,y1,x2,y2  depending upon, if one or both cursors are
              used. 
\\
              For option (g) above: `curs_coords' = xcen,ycen,rin,rmid,rout
              indicating the center x,y coords and radius of the inner, 
              middle, and outer circle. The radii are defined in screen pixels.
\\
              If curs_coords are omitted, cursors remain where they are;
              this is the default
\ap\pa
          flag = F(RAME), if the cursor coords. in parameter 'curs_coords'
                 have to be interpreted as frame pixels (of the currently 
                 displayed image) according to the MIDAS rules (cf. the help 
                 of READ/IMAGE)
\\
               = S(CREEN), the coords in curs_coords are interpreted as 
                 screen pixels;
\\
               defaulted to S
\ap\sa
See also:
          GET/CURSOR, SET/GCURSOR
\as\no
Note:  
          All cursor shapes are connected to the mouse and will only be visible
          when a subsequent Midas command actually uses the cursor(s).
\\
          Form (d) yields a rectangle. The center of the rectangle is moved
          via the mouse and its size is changed via the `arrow' keys on the
          keyboard.
\\
          Form (e) yields one, two or three circles depending upon which
          radius is nonzero (the inner radius must be > 0). The center of the
          circles is moved via the mouse. The size of the circles is
          changed via the `arrow' keys on the keyboard. Via the function keys
          F1, F2, F3 and F4 of the keyboard you specify which circle is
          affected the next time you hit an `arrow' key:
\\
          F4 enables the updating of the inner circle and the middle and outer
          circle (if they exist) are changed accordingly. This is the default.
\\
          F1 enables the updating of the inner circle only. The radius of the
          inner circle is forced to at least 1 pixel and cannot be larger
          than the middle or outer circle.
\\
          F2 enables the updating of the middle circle only. The radius of the
          middle circle is forced to remain larger than the radius of the inner
          circle and less than the one of the outer circle (if existing).
\\
          F3 enables the updating of the outer circle only. The radius of the
          outer circle is forced to remain larger than the radius of the middle
          circle (if existing).
\\
          The keys 0, 1, ..., 9 control the increments of the changing of size.
\\
          Also, the colour of the cursor is not strictly white but depends on
          the underlying pixel colour...
\\
          The screen coords. of the cursor(s) are store in keyword CURSOR as
          element 1,2 (for cursor \#1) and 3,4 (for cursor \#2).
\on\exs
Examples:
\ex
          SET/CURSOR ? RECTANGLE
            Use white cursor rectangle, specify no position for its corners.
\xe\ex
          SET/CURSOR ? circle 200,200,10,20,30
            Define cursor circles. Initial position will be centered at screen
            pixels (200,200) and the radii of the inner, middle and outer
            circle will be 10, 20 and 30 pixels,respectively.
\xe \sxe
