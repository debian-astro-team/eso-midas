% @(#)set_lut.hlq	19.1 (ESO-IPG) 02/25/03 14:03:44 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      set_lut.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, SET/LUT
%.PURPOSE    On-line help file for the command: SET/LUT
%.VERSION    1.0  14-APR-1986 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./LUT
\es\co
SET/LUT							25-MAY-1998  KB
\oc\su
SET/LUT [sect]
	enable usage of colour lookup tables
\us\pu
Purpose:  
          Enable usage of colour lookup tables which have been loaded into
          given LUT-section previously.
\up\sy
Syntax:  
          SET/LUT [sect]
\ys\pa
          sect = section (or no.) of LUT  - only possible for image displays
               which support multiple LUTs at a time;
\\
               for all other devices this parameter is always set to 0,
               which is also the default
\ap\see
See also:
          CLEAR/LUT, LOAD/LUT, DISPLAY/LUT, MODIFY/LUT
\ees\no
Note: 
          XWindow displays usually can use only one LUT at a time (an exception
          to this is e.g. the (now defunct) Stellar/Stardent system where we
          had implemented 4 LUTs).
\\
          In general, you need SET/LUT only after a CLEAR/LUT or for systems
          supporting multiple LUTs.
          For more details about LUTs see Chapter 6 of the MIDAS Users Guide.
\on\exs
Examples:
\ex
          SET/LUT
            Enable usage of LUT which is loaded into LUT-section 0.
\xe\ex
          SET/LUT 2 
            Switch to LUT previously loaded into LUT-section 2.
\xe \sxe
