%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1988-2008 European Southern Observatory
%.IDENT      assigngrap.hlq
%.AUTHOR     RHW, IPG/ESO
%.KEYWORDS   MIDAS, help files, ASSIGN/GRAPHICS
%.PURPOSE    On-line help file for the command: ASSIGN/GRAPHICS
%.VERSION    880315 RHW Creation, RHW
%.VERSION    920312 RHW Change to ASSIGN/GRAPHICS
%.VERSION    920312 RHW Improves in text
%----------------------------------------------------------------
\se
SECTION./GRAP
\es\co
ASSIGN/GRAPHICS                                                15-JUL-1988  RHW
\oc\su
ASSIGN/GRAPHICS [device] [option]
	define the graphic device for plot output
\us\pu
Purpose:      
              Define the graphic device to which all subsequent plots
              will be sent.
\up\sub
Subject:      
              Hard copy, graphics, HELP [PRINTERS],  HELP [POSTSCRIPT]
\bus\sy
Syntax:      
              ASSIGN/GRAPHICS [device] [option]
\ys\pa
              device = graphics device; possible choices can be:\\
                       G[WINDOW,n] graphics window n (default 0); \\
                       D[WINDOW,n] display window n (default 0); \\
                       T[ERMINAL]  graphics terminal; graphics window emulation
                       D[ISPLAY]   single display window; synonym to D,0\\
                       one of the HARDCOPY devices (see below);\\
                       POSTSCRIPT  generic postscript device;
                       NULL        the null device; in this case a plot file
                                   will be created. Handy if you have no
                                   graphic display capabilities available.
\ap\pa
              option = spool option; Possible choices are:
                       SPOOL       plots routed immediately (default);\\
                       NOSPOOL     plots will be kept on disk and only routed
                                   after a COPY/GRAPHICS command is given.
\ap\no
Note:         
               On window systems (Xterminals, workstations) a maximum of 10 
              graphics/display windows can be used (G,0 to G,9 and D,0 to D,9)
\\
               The choice between SPOOL and NOSPOOL can only been made if a 
              hardcopy device has been assigned. The NOSPOOL parameter can be
              useful in cases where the user wants to produce a plot with a
              number of overplots on a hardcopy device. In that case the plot
              files are not routed to the device but stored on disk. After 
              finishing the user can make the hardcopy with the COPY/GRAPHICS 
              command.
\\
               Hardcopy devices should be specified by the system device names.
              For a number of devices default (MIDAS) names have been 
              implemented. A complete overview of the available devices and 
              their names can be obtained with the command HELP [PRINTERS].
\\
               All postscript printers and the device POSTSCRIPT offer the 
              possibility to print in portrait or in landscape mode. In order 
              to get the desired format one has to extend the printer name 
              with ``.l'' for landscape or ``.p'' for portrait mode. Default 
              (no extension given) is landscape mode.
\\
               The scales of a plot may change if a plot is sent to a plot 
              device other than the original one (pre-specified by
              ASSIGN/GRAPHICS). In general the axis ratio of the frame will 
              have changed, and hence a square frame WILL NOT BE A SQUARE FRAME
              ANYMORE if you use COPY/GRAPHICS to dump your plot on another 
              device.
\on\see
See also:     
              COPY/GRAPHICS, SET/GRAPHICS, SHOW/GRAPHICS
	      HELP [POSTSCRIPT], HELP [PRINTERS]
\ees\exs
Examples:
\ex
              ASSIGN/GRAPHICS G,1 
                Plots are produced on MIDAS graphics window 1. The window will 
                be created if it is not available yet.
\xe\ex
              ASSIGN/GRAPHICS ps2usr1
                Plots (in landscape mode) will be spooled to the 
                printer/plotter ps2usr1; this is a system device name at ESO
		and most probably not available for this MIDAS installation. 
		Check with HELP [PRINTERS] for the device names.
\xe\ex
              ASSIGN/GRAPHICS ps2usr1.p
                The same as for the first example. However with the extension
                ``.p'' plots will be printed in portrait mode.
\xe\ex
              ASSIGN/GRAPHICS LASER
                Plots will be spooled to the default printer/plotter (LASER); 
                check with HELP [PRINTERS] which device that actually is.
                Also here extensions ``.p'' or ``.l'' are allowed.
\xe\ex     
              ASSIGN/GRAPHICS POSTSCRIPT
                Plots will be stored in the encapsulated postscript file 
                postscript.ps. They can be included in a LateX document or 
                be sent to any postcript device. Also here extensions ``.p'' 
                or ``.l'' are allowed.
\xe\ex
              ASSIGN/GRAP VERSA NOSPOOL
                Plots are intended for the Versatec but will not be spooled.
                The plot files will be kept on disk and can be spooled by the
                command COPY/GRAPH device_name.
\xe\sxe
