 
To define output devices for printing use ASSIGN/PRINT, for plot output use
ASSIGN/PLOT and for hardcopies of images ASSIGN/DISPLAY. Subsequent PRINT/...,
PLOT/...  or LOAD/IMAGE commands will then send their output to these devices.

For the commands COPY/GRAPHICS and COPY/DISPLAY the output device name is
a parameter in the command line.

If the printers are not Postscript printers, specific driver information
has to be entered into the file $MIDASHOME/$MIDVERS/systab/ascii/plot
in a format described in the header of that file.
Postscript printers do not have to be put into that file.

If the printer is a colour printer, specify that via `printer_name.c',
e.g. if pacol22 is a colour laser printer, use `pacol22.c' as printer
specfication.

There are also generic names for a line printer (LPRINT), a grayscale postscript 
printer (LASER) and colour postscript (COLOUR) in Midas.

These generic names are set in the file $MIDASHOME/$MIDVERS/monit/devices.sh
for all users, which is executed when typing $ inmidas or $gomidas.

E.g. the entries from the currently shipped devices.sh file represent the 
device setup at ESO from 2001, and are:
LPRINT=lp2usr1   ;export LPRINT     # Line printer, 2nd floor, user room
LASER=ps2usr0    ;export LASER      # Postcript B/W, 2nd floor, user room
COLOUR=pc2usr0.c   ;export COLOUR   # Postcript Color, 2nd floor, user room
SLIDE=sl2usr0    ;export SLIDE      # Chromascript, 2nd floor, user room
PENPLOT=hp2usr0  ;export PENPLOT    # HP plotter, 2nd floor, user room

For example, if you just have a single inkjet (postscript) printer, named 
hp-inkjet99, then you could change the file devices.sh as follows:
LPRINT=hp-inkjet99   ;export LPRINT     
LASER=hp-inkjet99    ;export LASER     
COLOUR=hp-inkjet99.c   ;export COLOUR 

As individual user you can adapt this file to your specific needs and store
the modified devices.sh in your local $MID_WORK directory. 
Midas uses then this device file instead of the global devices.sh in 
$MIDASHOME/$MIDVERS/monit.o
 
