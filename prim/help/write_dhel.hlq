% @(#)write_dhel.hlq	19.1 (ESO-IPG) 02/25/03 14:03:51 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      write_dhel.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, WRITE/DHELP
%.PURPOSE    On-line help file for the command: WRITE/DHELP
%.VERSION    1.0  17-OCT-1983 : Creation, KB
%.VERSION    1.1  16-MAR-1993 : Update, KB
%----------------------------------------------------------------
\se
SECTION./DHEL
\es\co
WRITE/DHELP					25-MAY-1993  KB
\oc\su
WRITE/DHELP frame descr text
	store help-text/comments for an existing descriptor
\us\pu
Purpose: 
	   Store help-text/comments for an existing descriptor.
\up\sy
Syntax:  
           WRITE/DHELP frame descr text
\ys\pa
           frame = name of data file
\ap\pa
           descr = descriptor name 
\ap\pa
           text = help text, has to be enclosed in double quotes (")
\ap\sa
See also:
           READ/DESCR, SHOW/DESCR, WRITE/DESCR, DELETE/DESCR, COPY/DD
\\
           chapter 3 of MIDAS Users guide, volume A
\as\no
Note:  
           The text is extended automatically if different WRITE/DHELP
           commands are done for the same descriptor, but not resized (shrunk)
           if the new text is shorter than the previous one.
\on\exs
Examples:
\ex
           WRITE/DHELP dinosaur xyztime "exposure time for instrument xyz"
             Store the comment for descr. `xyztime' of frame `dinosaur.bdf'.
             The descriptor `xyztime' must exist already.
\xe \sxe
