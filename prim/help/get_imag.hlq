% @(#)get_imag.hlq	19.1 (ESO-IPG) 02/25/03 14:03:23 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      get_imag.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, GET/IMAGE
%.PURPOSE    On-line help file for the command: GET/IMAGE
%.VERSION    1.0  17-OCT-1983 : Creation, KB
%.VERSION    1.1  01-NOV-1991 : Update, KB
%----------------------------------------------------------------
\se
SECTION./IMAG
\es\co
GET/IMAGE					17-OCT-1983  KB
\oc\su
GET/IMAGE frame [input_source] [ITT_flag]
	copy pixels from displayed image channel
\us\pu
Purpose:      
          Copy pixels from displayed image channel and store in Midas image,
          i.e. make a hardcopy of the displayed channel (without overlay).
\up\sy
Syntax:       
          GET/IMAGE frame [input_source] [ITT_flag]
\ys\pa
          frame = name of image frame, where the pixel data will be stored
\ap\pa
          input_source = channel no. (a), 
                         CURSOR (b),
                         CURSOR,xpix,ypix (c);
\\
               for (a) the whole image channel (all pixels displayed)
                       is read out;
\\
               for (b) a cursor rectangle will determine the size of the
                       pixel area to be read out;
\\
               for (c) a single cursor is used to define the center of a
                       pixel rectangle of size `xpix,ypix' ;
\\
               defaulted to currently displayed channel, i.e. option (a)
\ap\pa
          ITT_flag = if set to ITT, pixels are also ITT-mapped, else not;
\\
               defaulted to NOITT
\ap\sa
See also:
          COPY/DISPLAY, EXTRACT/CURSOR, GET/CURSOR
\as\no
Note:         
             The pixels of a displayed image are read out, i.e. you get
          data in the range determined by the depth of the display, which
          usually is 8 bits (or the interval [0,255]). THat means, you 
          don't get the values of the originally displayed image!
             Only the part of the cursor rectangle which covers the displayed
          image is used. Therefore, you may get a smaller image than expected,
          if the displayed image does not fill all the display memory and your
          cursor rectangle is not completely inside the displayed image.
             For a description of how to move and modify the cursor rectangle
          use HELP [ImageDisplay].
\on\exs
Examples:
\ex
          GET/IMAG tecate 0
            Store all displayed pixels of channel 0 into frame `tecate.bdf'.
\xe\ex
          GET/IMAG tecate 0 ITT
            As above, but pixels of channel are first mapped via loaded ITT
            and than copied.
\xe\ex
          GET/IMAG bohemia CURSOR
            Store pixels defined by cursor rectangle into frame `bohemia.bdf'.
\xe\ex
          GET/IMAG dosequis CURSOR,200,220 
            Store image of size 200*220 pixels into frame `dosequis.bdf'.
            The center is determined via a single cursor cross.
\xe\ex
          GET/IMAG dosequis CURSOR,200,220 ITT
            As above, but contents of channel are mapped via loaded ITT.
\xe \sxe
