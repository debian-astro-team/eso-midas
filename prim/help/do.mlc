% @(#)do.mlc	19.1 (ESO-IPG) 02/25/03 14:03:02 
\rightpage
\noqualifier
\se
SECTION./
\es\co
DO							21-JAN-1993  KB
\oc\su
%DO loopvar = start end [step]
%	define a DO loop (as in FORTRAN)
 
\us\pu
Purpose:  
          Define a DO loop (as in FORTRAN).
\up\sy
Syntax: 
          DO loopvar = start end [step]
\ys\pa
          loopvar = name of integer keyword serving as loop variable
\ap\pa
          start = starting integer value of loop, either integer constant or
             name of integer keyword
\ap\pa
          end = end integer value of loop, either integer constant or
             name of integer keyword
\ap\pa
          step = optional integer stepsize, either integer constant or
             name of integer keyword; defaulted to 1
\ap\no
Note:   
          The DO block must be closed with an ENDDO command; please, note
          that contrary to FORTRAN the ENDDO must(!) be written as one word,
          i.e.  END DO  results in an error ...
\\
          Nested loops are supported up to 8 levels deep.
\\
          A DO loop is executed at least once, i.e. the test for loop
          termination is done in the end of the DO block.
\\
          A stepsize = 0 is legal and leads to an infinite loop unless the 
          start value is already larger than the end value.
\on\exs
Examples:
\ex
          DEFINE/LOCAL LL/I/1/1 0
\\
          DO LL = 12 22 2
\\
             WRITE/KEY INPUTI/I/{LL}/1 {LL}
\\
          ENDDO
\\
            This would fill keyword INPUTI(12,14,...,22) with the values
            12,14,...,22.
\xe\ex
 \
\\
          DEFINE/LOCAL LL/I/1/1 0
\\
          OUTPUTI(4) = 10
\\
          DO LL = 22 OUTPUTI(4) -2
\\
             WRITE/OUT {LL}
\\
          ENDDO
\\
            This would display the numbers 22, 20, ..., 12, 10.
\xe \sxe
