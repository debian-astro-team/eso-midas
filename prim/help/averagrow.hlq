% @(#)averagrow.hlq	19.1 (ESO-IPG) 02/25/03 14:02:58 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      averagrow.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, AVERAGE/ROW
%.PURPOSE    On-line help file for the command: AVERAGE/ROW
%.VERSION    1.0  09-MAR-1984 : Creation, JDP
%.VERSION    1.1  17-APR-1991 : Modification, KB
%----------------------------------------------------------------
\se
SECTION./ROW
\es\co
AVERAGE/ROW						17-APR-1991 JDP
\oc\su
AVERAGE/ROW out = in [start,end] [SUM]
	average image rows

\us\pu
Purpose:  
          Average rows in a selected range of rows of the input image. 
          The output is optionally the sum of the rows.
\up\sy
Syntax:
          AVERAGE/ROW out = in [start,end] [SUM]
\ys\pa
          out = resulting 1-dimensional frame
\ap\pa
          in = 2-dim image to be averaged
\ap\pa
          start = first row, defaulted to the first row of the frame
\ap\pa
          end = last row, defaulted to the last row of the frame
\ap\pa
          SUM = optional parameter to define the output as sum of the rows.
\\
              By default the output is the average of them.
\ap\sa
See also:
          AVERAGE/COLUMN, TUTORIAL/AVERAGE
\as\no
Note:  
          First and last row can be specified in pixels or world coords.
\\
          See the help text of READ/IMAGE for a detailed description of the
          MIDAS syntax for coordinates.
\on\exs
Examples:
\ex
          AVERAGE/ROW out = in <,@100
            Average the rows of frame `in.bdf', beginning with first row and
            ending with row no. 100; store result in 1-dim frame `out.bdf'.
\xe \sxe
