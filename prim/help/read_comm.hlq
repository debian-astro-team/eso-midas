% @(#)read_comm.hlq	19.1 (ESO-IPG) 02/25/03 14:03:37 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      read_desc.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, READ/DESCR
%.PURPOSE    On-line help file for the command: READ/DESCR
%.VERSION    1.0  13-APR-1984 : Creation, KB
%.VERSION    1.1  29-MAY-1991 : Update, KB
%----------------------------------------------------------------
\se
SECTION./COMM
\es\co
READ/COMMANDS					03-JUL-1990  KB
\oc\su
READ/COMMANDS [proc]
	read commands from a procedure + store into command buffer
\us\pu
Purpose:  
          Read commands from a procedure + store them into command buffer, at
          the moment limited to 15 commands.
\up\sy
Syntax: 
          READ/COMMANDS [proc]
\ys\pa
          proc = name of the Midas procedure containing the commands we want to
                 store into the command buffer;
\\
                 defaulted to midtempZY.prg, where ZY is the current MIDAS unit,
                 stored in MID_WORK
\ap\sa
See also:
          WRITE/COMMANDS, CLEAR/BUFFER
\as\no
Note:
          We first look for the procedure in the current directory and then
          in MID_WORK.
\on\exs
Examples:
\ex
          READ/COMMANDS mycomms
          Fill command buffer with commands stored in mycomms.prg .
\xe \sxe
