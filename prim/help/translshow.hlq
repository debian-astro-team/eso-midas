%  @(#)translshow.hlq	19.1 (ESO-DMD) 02/25/03 14:03:48
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      translshow.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, TRANSLATE/SHOW
%.PURPOSE    On-line help file for the command: TRANSLATE/SHOW
%.VERSION    1.0  19-SEP-1990 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./
\es\co
TRANSLATE/SHOW          				26-FEB-2002  KB
\oc\su
TRANSLATE/SHOW proc option
	translate MIDAS procedure and display resulting code
\us\pu
Purpose:      
          Translate Midas procedure and display resulting code.
\up\sy
Syntax:       
          TRANSLATE/SHOW proc option
\ys\pa
          proc = name of MIDAS command procedure, file type defaulted to `.prg'
\ap\pa
          option = N (normal) or X (extended);
                 for option = 'X' all Midas commands in the procedure are
                 checked further:
                 a) that they do not contain more than 10 TOKENS
                 b) that commands and qualifiers are fully specified and 
                    not abbreviated;
                 only lines with offending commands are displayed;
                 a) leads to severe warnings, b) to warnings
                 if set to 'X,silent' individual command lines are not
                 displayed, just a final message appears, if some
                 commands/qualifiers are abbreviated;
                 for option  = 'N' the procedure is translated to the internal
                 Midas code and all lines are displayed on the terminal;
                 defaulted to N
\ap\sa
See also:
          SHOW/CODE, TRANSLATE/PROCEDURE, DEBUG/PROCEDURE, @ progcheck.prg
\as\no
Note:         
          To see the translation for a Midas system procedure use
          TRANSLATE/SHOW MID_PROC:procedure .
          For option = 'X' the keyword MID$INFO(1) and (2) are set to the 
          total no. of warnings, and the no. of severe warnings
\on\exs
Examples:
\ex
          TRANSLA/SHOW lobo 
            Translate procedure `lobo.prg' (in current directory) and display
            its code.
\xe \ex
          TRANSLA/SHOW lobo x
            Translate procedure `lobo.prg' (in current directory), check all
            Midas commands in that procedure for completeness and display
            only the lines with incomplete commands or qualifiers.
\xe \ex
          TRANSLA/SHOW MID_WORK:chiva 
            Translate procedure `chiva.prg' (in MID_WORK directory) and display
            its code.
\xe \sxe
