% @(#)sort_tcat.hlq	19.1 (ESO-IPG) 02/25/03 14:03:47 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      sort_tcat.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, SORT/TCAT
%.PURPOSE    On-line help file for the command: SORT/TCAT
%.VERSION    1.0  24-JUN-1991 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./TCAT
\es\co
SORT/TCAT                                                 24-JUN-1991  KB
\oc\su
SORT/TCAT [cat_name] 
	sort a table catalog
\us\pu
Purpose:
          Sort a table catalog.
\up\sy
Syntax:
          SORT/TCAT [cat_name] 
\ys\pa
          cat_name = name of Table Catalog;
\\
              defaulted to the currently active Table Catalog
\ap\sa
See also:
          SORT/ICAT, SORT/FCAT, SET/TCAT, CREATE/TCAT
\\
          READ/TCAT, ADD/TCAT, SUBTRACT/TCAT, SEARCH/TCAT
\as\no
Note:
          Sorting is done according to the contents of the IDENT field in the
          catalog (i.e. usually the same as descr. IDENT).
\\
          The sorted catalog is first stored in a temporary file midtemp.cat,
          then the input catalog is deleted and, finally, midtemp.cat is
          renamed back to the original cat_name.
\\
          Therefore the input catalog should not be opened at the same time by
          e.g. another MIDAS session.
\on\exs
Examples:
\ex
          SORT/TCAT
            Sort the currently active Table Catalog.
\xe\ex
          SORT/TCAT zopilote
            Sort the table catalog `zopilote.cat' .
\xe\sxe
