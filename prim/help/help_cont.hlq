% @(#)help_cont.hlq	19.1 (ESO-IPG) 02/25/03 14:03:24 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      help_cont.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, HELP/CONTRIB
%.PURPOSE    On-line help file for the command: HELP/CONTRIB
%.VERSION    1.0  11-JAN-1995 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./CONT
\es\co
HELP/CONTRIB                                            11-JAN-1995  KB
\oc\su
HELP/CONTRIB [proc]
	display header information of procedures in the Midas `contrib' area
\us\pu
Purpose:  
          Display header information of application procedures which are
          stored in CON_PROC:
\up\sy
Syntax: 
          HELP/CONTRIB [proc]
\ys\pa
          proc = name of procedure without type, ".prg" is appended;
\\
             if omitted a menu of all available application procedures in
             the Midas `contrib' area is displayed;
\\
             this is the default
\ap\sa
See also:
          HELP/APPLIC
\\
          description of the help facility in chapter 3 of the MIDAS Users
          Guide, volume A
\as\no
Note:     
          These application procedures provide you with a set of additional
          Midas commands. They are executed via "@c procedure".
          These procedures involve contributed software (as well as public
          domain software). They provide functionality which is (usually)
          less often used, therefore we did not create Midas command names for
          them.
\on\exs
Examples:
\ex
          HELP/CONTRIB lutedit
            Display help info about the Motif based LUT editor, launched via
            "@c lutedit".
\xe \sxe
