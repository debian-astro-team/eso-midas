% @(#)info_keyw.hlq	19.1 (ESO-IPG) 02/25/03 14:03:26 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      info_keyw.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, INFO/KEYWORD
%.PURPOSE    On-line help file for the command: INFO/KEYWORD
%.VERSION    1.0  14-FEB-1996 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./KEYW
\es\co
INFO/KEYWORD					14-FEB-1996  KB
\oc\su
INFO/KEYWORD keyword
	get type, size, lockno and mod-time of keyword
\us\pu
Purpose: 
          Get type, no. of elements and bytes per element as well as lock no.
          and last modify time of keyword
\up\sy
Syntax:  
          INFO/KEYWORD keyw
\ys\pa
          keyw = name of keyword
\ap\sa
See also:
          SHOW/KEYWORD, READ/KEYWORD, INFO/DESCR
\as\no
Note:  
          This command is especially useful inside MIDAS procedures.
          No output is generated, only the integer keyword MID$INFO is filled
          as follows:
\\
          If the keyword could not be found, MID$INFO(1) = 0.
\\
          Otherwise MID$INFO(1) contains the type of the keyword,
\\
          1 = integer, 2 = real, 3 = character, 4 = double precision.
\\
          MID$INFO(2) = the no. of elements of the keyword,
          MID$INFO(3) = the no. of bytes per element,
          MID$INFO(4) = the lock no. of the keyword,
          MID$INFO(5) = the time of last iwrite access of the keyword.
\\
          The function M$EXISTK of the command COMPUTE/KEYWORD provides
          similar but less detailed information.
\on\exs
Examples:
\ex
          INFO/KEYW durazno 
            If keyword DURAZNO is a double precision keyword of 6 elements
            and unlocked, then:
\\
            MID$INFO(1) = 4, MID$INFO(2) = 6, MID$INFO(3) = 8, MID$INFO(4) = 0.
\xe \sxe
