% @(#)copy_tabl.hlq	19.1 (ESO-IPG) 02/25/03 14:03:08 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      copy_tabl.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, COPY/TABLE
%.PURPOSE    On-line help file for the command: COPY/TABLE
%.VERSION    1.0  12-OCT-1985 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./TABL
\es\co
COPY/TABLE						12-OCT-1985  JDP
\oc\su
COPY/TABLE intable outable [organization]
	copy source table to destination table
\us\pu
Purpose:   
           Copy table files. The output table will consist of the same
           columns as the input table.
           The command can be used in connection with the SELECT/TAB
           command to produce subtables of the input table.
\up\sy
Syntax:    
           COPY/TABLE intable outable [organization]
\ys\pa
           intable      = input table name
\ap\pa
           outable      = output table name
\ap\pa     
           organization = optional organization of the output file as \\
                          RECORD (table stored row per row) or \\
                          TRANSPOSED (table stored columnwise)  \\
                          By default the output table will be organized in \\
                          the same way than the input table
                           
\ap\no
Note:      
           All the non-standard descriptors from the input table will be
           copied into the output table.
           The descriptor HISTORY of the input table will also be copied
           into the output table.  
\on\see
See also:
           PROJECT/TAB, COPY/TT
\ees\exs
Examples:
\ex
           COPY/TABLE TABLE1 OUTPUT
           This will copy 'TABLE1' into output table 'OUTPUT'.
\xe \sxe
