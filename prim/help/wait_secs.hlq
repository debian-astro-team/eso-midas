%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2010 European Southern Observatory
%.IDENT      waitsecs.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, WAIT/SECS
%.PURPOSE    On-line help file for the command: WAIT/SECS
%.VERSION    1.0  15-DEC-1983 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./SECS
\es\co
WAIT/SECS						23-SEP-2010  KB
\oc\su
WAIT/SECS [no_of_secs]
	suspend MIDAS monitor for no_of_secs seconds
\us\pu
Purpose: 
          Suspend MIDAS monitor for a number of seconds.
\up\sy
Syntax:  
          WAIT/SECS [no_of_secs]
\ys\pa
          no_of_secs = no of seconds to wait; defaulted to 1 sec
\ap\sa
See also:
          WAIT/BACK_MIDAS
\as\no
Note:  
          If `no_of_seconds' is < 0.001, 
	  then WAIT/SECS returns immediately (= NOOP, No Operation command).
	  If keyword AUX_MODE(8) is set to 1, 
          then WAIT/SECS ignores any no_of_seconds given => NOOP.
\on\exs
Examples:
\ex
          WAIT/SECS 12
           Wait 12 seconds before continuing with next command.
\xe \ex
          AUX_MODE(8) = 1
	  TUTORIAL/GRAPH auto
           Execute the graphics tutorial in a very fast manner, because 
	   all the internal WAIT/SECS commands in the turorial are
	   ignored.
\xe \ex
          WAIT/SECS 0.
           Do not wait but continue immediately - this may be useful if you
           need a NOOP command in a branch of an IF statement.
\xe \sxe
