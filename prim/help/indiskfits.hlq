%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2010 European Southern Observatory
%.IDENT      indiskfits.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, INDISK/FITS
%.PURPOSE    On-line help file for the command: INDISK/FITS
%.VERSION    1.0  05-NOV-1993 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./FITS
\es\co
INDISK/FITS						17-JUN-02010  KB
\oc\su
INDISK/FITS in_files [out_spec] [option] [INTAPE_flag] [empty_header]
	read FITS files from disk
\us\pu
Purpose:
	  Read FITS files from disk and convert to Midas format.
\up\sy
Syntax: 
          INDISK/FITS in_files [out_spec] [option] [INTAPE_flag] [empty_header]
\ys\pa
          in_files = filename(s) of FITS files separated by a comma;
              or name of ASCII file (indicated by filetype `.cat')
              containing FITS filenames ;
	      or name of Midas catalog with FITS files;
              also wildcard characters are supported.
\ap\pa
          out_spec = corresponding MIDAS filenames separated by a comma;
              or name of ASCII file (indicated by filetype `.cat') with 
              corresponding Midas filenames;
	      or name of Midas catalog with result names;
              or string "ROOT=abcd" to build output file names as
              abcd0001.TYPE, abcd0002.TYPE, ... with TYPE is "bdf", "tbl",
                                 or "fit" depending on type of FITS file;
	      or string "NAME=INPUT" to use input names as result names
	      with the relevant Midas file type (TYPE as above)
              if the param `out_spec' is omitted, output names are built as
              toto0001.TYPE, toto0002.TYPE, ...
\ap\pa
          option = OC or ON (or NO) to keep the original name of the files,
              i.e RESTORE/NAME is executed in the end for each result file,
	      renaming them according to the FITS keyword (descr) FILENAME;
              OC - with overwrite confirmation, ON - without confirmation,
              NO - keep name as specified in parameter `out_spec';
              defaulted to NO
\ap\pa
          INTAPE_flag = 3-character flag as used with INTAPE/FITS command,
	      list, create, keywrd flag:
              list flag:   F(ull), S(hort), N(one) 
              create flag: N(o create), O(original format),
                           F(loating point format) 
              keywrd flag: Y(es history stored), N(o history stored),
                           C(ode hierarchical keywords) or
                           A(ll keywords stored also with empty
                           data matrix) 
              defaulted to NOY - so it's not exactly as in the INTAPE command, 
	      where the default is SOY
\ap\pa
          empty_header = YES or NO, suppress or don't suppress the first
		empty primary header (for tables and images);
	        defaulted to YES
\ap\sa
See also:
          INTAPE/FITS, INDISK/ASCII, INDISK/MFITS,
	  OUTDISK/FITS, OUTTAPE/FITS, RESTORE/NAME [Filename]
	  @a inoutFITS
\as\no
Note:  
          If not as many output file names as input names are given, the names
          `toto0001.TYPE', `toto0002.TYPE', ... are used.
          The command works for images, tables and fitfiles.
             The INTAPE_flag is passed on unchanged to the underlying 
          INTAPE/FITS command.
	     All result names are stored in ASCII file outnames.cat (not a full
          Midas catalog), the no. of created files is stored in MID$INFO(7).
              The parameters may also be cross referenced via
          IN=, OUT=, OPTION=, FLAG= and TBLCOMP=

          Midas does not have an internal 32bit unsigned integer format.
	  FITS data in that format  will be converted to double precision.
\on\exs
Examples:
\ex
          INDISK/FITS xyz
            Convert FITS file `xyz' to `toto0001.bdf' or `toto0001.tbl'
            or `toto0001.fit' depending upon if `xyz' was an image, table
            or fit file.
\xe\ex
          INDISK/FITS tst0003.mt prado.midas
            Convert FITS file `tst0003.mt' to `prado.midas'
\xe\ex
          INDISK/FITS tst0004.mt ? OC
            Assuming that the FITS file `tst0004.mt' has a char. FITS keyword 
            named FILENAME containing `vieux_port.bdf' convert FITS file
            `tst0004.mt' to MIDAS file `vieux_port.bdf'. If a file named
            `vieux_port.bdf' already exist, ask for confirmation before
            overwriting it.
\xe\ex
          INDISK/FITS paradis.cat paradis.out
            Read ASCII file `paradis.cat' and convert the FITS files with
            names in that file to MIDAS files with the corresponding names in
            file `paradis.out' 
\xe\ex
          INDISK/FITS paradis.cat ? ON p4=SFY
            Read ASCII file `paradis.cat' and convert the FITS files with
            names in that file to MIDAS files with names as descr. FILENAME
            of the FITS input files (will usually be the same as the physical
            filename...). All image data will be stored as real values.
\xe\ex
          INDISK/FITS luminy* panier.dat
            Convert all FITS files matching the pattern `luminy*' to MIDAS 
	    files with names according to entries in ASCII file `panier.dat'
\xe\ex
          INDISK/FITS luminy* ROOT=cassis
            As above but names of MIDAS files will be built as
            `cassis0001.bdf', `cassis0002.bdf', ...
\xe\ex
          INDISK/FITS luminy* ROOT=cas@sis
            As above but `cas@sis0001.bdf' is a bad MIDAS filename, therefore
            a warning message will be displayed and names will be built as
            `cas_sis0001.bdf', `cas_sis0002.bdf', ...
\xe\ex
          INDISK/FITS prado.cat NAME=input
            Read ASCII file `prado.cat' and convert the FITS files with
            names in that file to MIDAS files with same name and as 
	    type the relevant Midas type (.bdf, or .tbl, or .fit).
\xe\ex
          INDISK/FITS tst0009.mt 
            Assuming that the FITS file `tst0009.mt' just contains an empty
	    primary header and a table extension, we get as result the
	    Midas table toto0001.tbl. Thus, a result like from INTAPE/FITS.
\xe\ex
          INDISK/FITS tst0009.mt ? ? ? NO
	    We do save the 1st empty primary header (with all its possible
	    descriptors (FITS keywords)) as a Midas image. And, get as 
	    result the Midas image toto0001.bdf (NAXIS = 0) and the table
	    toto0002.tbl. Thus, a result like from INDISK/MFITS.
\xe \sxe
