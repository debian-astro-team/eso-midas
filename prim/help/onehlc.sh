#! /bin/sh
# @(#)onehlc.sh	19.1 (ESO-IPG) 02/25/03 14:03:31
# 
#  Bourne shell script "onehlc.sh"
#  to build single .hlc file from a group of .hlq files
#  901219  KB
#  to use it e.g. for the copy command, do  sh onehlc.sh copy
# 
touch $1.hlc
rm $1.hlc
#
ls $1*.hlq |  sed  's/_.*/ /' | sort -u | sed '/_/d' > temp0
# 
cat temp0 | sed 's/\.hlq//' | awk '{print substr($1,1,6)}' | sort -u > temp1
#
for i in `cat temp1`
do
   for file in `ls $i*.hlq`
   do
      sed  -n '/SECTION./,/\\us/p' $file | sed '/^\\/d' | sed '2d' >> $i.hlc
   done
done
