#!/bin/sh
# @(#)gen_tapes.sh	19.1 (ESO-IPG) 02/25/03 14:03:22
# -----------------------------------------------------------------------
# Bourne shell procedure  to generate output for the tapede.alq file 
# 930630 CG. Implementation
# -----------------------------------------------------------------------

echo ""
echo "At ESO-Garching and for the latest list of tape-devices check the "
echo "ESO WWW page http://http.hq.eso.org/it/tapes.html"
if ( cat ../../monit/devices.sh | egrep "^tape" ) >/dev/null 2>&1
then
  echo ""
  echo "Name:Host:Device:Tape:Cap/Den:Location" |\
  awk -F: '{printf "%-10s%-10s%-15s%-10s%-10s%s\n",$1,$2,$3,$4,$5,$6}'
  echo "--------------------------------------------------------------------------------"
  cat ../../monit/devices.sh | egrep "^tape" | while read entry
  do
    name=`echo $entry | awk -F: '{print $1}' | awk -F= '{print $1}'`
    host=`echo $entry | awk -F: '{print $1}' | awk -F= '{print $2}'`
    dev=`echo $entry | awk -F: '{print $2}' | awk  '{print $1}'`
    mod=`echo $entry | awk -F# '{print $2}' | awk -F: '{print $2}'`
    den=`echo $entry | awk -F# '{print $2}' | awk -F: '{print $3}'`
    loc=`echo $entry | awk -F# '{print $2}' | awk -F: '{print $4}'`
    echo "$name:$host:$dev:$mod:$den:$loc" | \
    awk -F: '{printf "%-10s%-10s%-15s%-10s%-10s%s\n",$1,$2,$3,$4,$5,$6}'
  done
fi
