%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1999-2011 European Southern Observatory
%.IDENT      intapefits.hlq
%.AUTHOR     PJG, IPG/ESO
%.KEYWORDS   MIDAS, help files, INTAPE/FITS
%.PURPOSE    On-line help file for the command: INTAPE/FITS
%.VERSION    1.0  25-FEB-1990 : Creation, PJG
%.VERSION    1.1  16-MAY-1991 : Add note on O_TIME, PJG
%.VERSION    1.2  19-NOV-1991 : Include explicit file names, PJG
%.VERSION    1.3  19-NOV-1992 : Add See Also section, PJG
%.VERSION    1.4  14-DEC-1994 : Physical tape devices, PJG
%.VERSION    1.5  17-JUL-1996 : Note on no-rewinding devices, PJG
%.VERSION    1.6  22-OCT-1996 : Note on new history option, PJG
%.VERSION    1.7  30-MAR-1999 : Add new history option, PJG
%----------------------------------------------------------------
\se
SECTION./FITS
\es\co
INTAPE/FITS						17-APR-2002  PJG, KB
\oc\su
INTAPE/FITS file_specs file-id device [flags] [noexts]
	read frames from magtape/disk in FITS format
\us\pu
Purpose:
           Read image frames from magtape or disk in FITS format.
\up\sy
Syntax: 
           INTAPE/FITS file_specs file_id device [flags] [noexts]
\ys\pa
           files_specs = numbers of frames as stored on tape in ascending
                order, e.g. 2,17,22-33,44 specifies frames 2,17,22 till 33,44
\ap\pa
           file_id = file identification (max. 4 chars), to this string the
                file no. on tape is appended, e.g. file no. 17 will be stored
                in file_id0017.bdf
\ap\pa
           device = logical tape unit (e.g. TAPE1), physical tape unit
                (e.g. /dev/nrst8 or host:/dev/nrst1) or prefix of file 
                name on disk. For disk files the extension .mt is assumed.
                It is also possible to specify a full file name with
                extension in which case the data are taken from it.
\ap\pa
           flag = 3-character flag: list, create, keyword
                list flag: F(ull), S(hort), N(one) \\
                create flag: N(o create), O(original format),
                             F(loating point format) \\
                keyword flag: Y(es history stored), N(o history stored),
                              C(ode hierarchical keywords) or
                              A(ll keywords stored also with empty
                                data matrix) \\
                Default flags are SOY
\ap\pa
           noexts = max. no. of extensions to read from FITS file;
	        defaulted to 9999
\ap\sa
See also:
           OUTTAPE/FITS, INDISK/FITS, RESTORE/NAME, CREATE/xCAT, SET/xCAT
\as\no
Note:  
           The input tape must be loaded on a tape unit before executing
           the command.  All terminal output is stored in the MIDAS
           logfile as well.
           To access remote tape drives, the MIDAS tapeserver demon must
           be installed and run on the host in question.
           A listing of file headers can be made by using the flags FNN
           For FITS format, only the descriptor elements corresponding to
           FITS keywords are created. Thus, some elements (e.g. in O_TIME)
           may not be initiated.
           When the keyword flag 'C' is used, hierarchical FITS keywords
           are coded into a 15 char. MIDAS descriptor name for backwards
           compatibility.  By default, these keywords become a long
           MIDAS descriptor name with '.' separating the different levels.
           Some FITS files containing e.g. IMAGE extensions may start
           with a prime FITS header with interesting keywords but
           with no associated data matrix.  By default, such empty
           data files will not be created.  If you wish to force
           the creation of such files with only header information,
           the history option 'A' can be used.
           If a FITS file has extensions, e.g. an image file with a binary
           table as next extension, as many Midas frames as extensions
           are created. The 4th element of the Midas keyword MID$INFO,
           (i.e. MID$INFO(4)) is set to the no. of Midas frames created
           by INTAPE/FITS.
           When a physical device name is used, it MUST specify a
           none-rewinding device (e.g. /dev/nrst3) for this command
           to work correctly.  The operating system may itself rewind 
           the tape if a rewinding device is given.

           Midas does not have an internal 32bit unsigned integer format.
           FITS data in that format  will be converted to double precision.
\on\exs
Examples:
\ex
           INTAPE/FITS 1-15 ccd TAPE0
             Read image frames no. 1 through 15 on the tape mounted on unit
             TAPE0 and store them into frames `ccd0001.bdf' to `ccd0015.bdf'. 
\xe\ex
           INTAPE/FITS * x /dev/nrst8 FNN
             List headers of all files from magtape mounted on the local
             tape unit /dev/nrst8.
\xe\ex
           INTAPE/FITS * x obs:/dev/nrst8 FNN
             List headers of all files from magtape mounted on the tape 
             unit /dev/nrst8 on host 'obs'. NOTE: MIDAS tape server must
             run on this host.
\xe\ex
           INTAPE/FITS 1-2 data image
             Read from file `image0001.mt' and `image0002.mt' creating the 
             frames `data0001.bdf' and `data0002.bdf' (assuming the FITS
             files are of type IMAGE).
\xe\ex
           INTAPE/FITS 1 gal galaxy.fits
             Read from file `galaxy.fits' and assuming the FITS file consists
             of an image and a binary table extension, create the Midas frames
             `gal0001.bdf' and `gal0001.tbl'. The keyword MID$INFO(4) will be
             set to 2.
\xe\ex
           INTAPE/FITS 1 xxx FORSbadpix.tfits
             Read from FITS file `FORSbadpix.tfits' and create the Midas frame
             `xxx0001.tbl' (assuming the FITS file is of type TABLE).
\xe \sxe
