% @(#)fft_finv.hlq	19.1 (ESO-IPG) 02/25/03 14:03:20 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      fft_finv.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, FFT/FINVERSE
%.PURPOSE    On-line help file for the command: FFT/FINVERSE
%.VERSION    1.0  03-APR-1990 : Creation, KB
%.VERSION    1.1  26-OCT-1992 : Upgrade, KB
%----------------------------------------------------------------
\se
SECTION./FINV
\es\co
FFT/FINVERSE						26-OCT-1992  KB
\oc\su
FFT/FINVERSE inr ini outr outi
        make inverse discrete Fourier transform
\us\pu
Purpose:
           Compute the inverse discrete Fourier transform of real or complex
           frame.
\up\sub
Subject:      
           Transformation, filter.
\bus\sy
Syntax:
           FFT/FINVERSE inr ini outr outi
\ys\pa
           inr = frame containing the real part of input frame;
                defaulted to `fftr.bdf'
\ap\pa
           ini = frame containing the imaginary part of input frame,
\\
                if input frame is only real, omit `ini' or set it to `?',
                if you enter values for any of the following parameters;
\\
                defaulted to `ffti.bdf'
\ap\pa
           outr = frame for real part of result transform;
                defaulted to `zztr.bdf'
\ap\pa
           outi = frame for imaginary part of result transform;
                defaulted to `zzti.bdf'
\ap\sa
See also:
           FFT/FREQUENCY, FFT/FPOWER, FFT/INVERSE, CREATE/FILTER
\as\no
Note:
           All internal calculations are done in double precision and
           only the final result frames are truncated to single precision.
\\
           If the number of pixels in an axis of the input frame is not a power
           of 2, the input frame is expanded to the next higher power of 2.
           The result frames will have the original no. of pixels but the
           result frames with expanded size are also kept with names
           `exp_fftr.bdf' and `exp_ffti.bdf'.
\\
           If the input frames are expanded frames from a previous FFT/FREQU
           with input files which had dimensions not a power of 2, the frames
           `orig_r.bdf' and `orig_i.bdf' with the original size will be
           extracted from the results as well.
\\
           This command corresponds to a previous FFT/FREQUENCY or FFT/FPOWER
           command, not a FFT/IMAGE command!
\on\exs
Examples:
\ex
           FFT/FINV f1 f2
            Do inverse FFT for image with real part in frame `f1.bdf' and
            imaginary part in `f2.bdf', store results in frame `zztr.bdf'
            and `zzti.bdf' .
\xe\ex
           FFT/FINV
            Do inverse FFT for image with real part in frame `fftr.bdf' and
            imaginary part in `ffti.bdf', store results in frame `zztr.bdf'
            and `zzti.bdf' .
\xe \sxe
\rs
Restrictions: \\   Does not work for 3-dim frames.
\sr
