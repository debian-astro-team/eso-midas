%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990-2010 European Southern Observatory
%.IDENT      magnitrect.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, MAGNITUDE/RECTANGLE
%.PURPOSE    On-line help file for the command: MAGNITUDE/RECTANGLE
%.VERSION    1.0  30-JUL-1990 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./RECT
\es\co
MAGNITUDE/RECTANGLE				    16-DEC-2010  KB
\oc\su
MAGNITUDE/RECTANGLE [in_specs] [out_specs] [pix_area] [out_opt]
	            [center_params] [curs_specs] [zw_option]
	compute magnitude within square aperture
\us\pu
Purpose:    
          Compute the magnitude of the specified object by integrating
          over the "flux" area defined by a rectangular aperture.
          The local background as integrated over the given "background" area
          (the sky) is subtracted. The pixels inside the "no_man's_land" 
          area are ignored.
\up\sy
Syntax:     
          MAGNITUDE/RECTANGLE [in_specs] [out_specs] [pix_area] [out_opt]
                              [center_params] [curs_specs] [zw_option]
\ys\pa
          in_specs = input specifications, either
             (a) CURSOR - if the subimages are chosen via the cursor(s)
             (b) image,table - if the subimages are defined in a table in the
                 columns labeled :XSTART, :XEND, :YSTART and :YEND,
             (c1) image - if subimage is defined by `Fszi,Nsiz,Bsiz' around
                         center of the image
             (c2) image,xpix,ypix - if subimage is defined by `Fszi,Nsiz,Bsiz'
                         around pixel (xpix,ypix) of the image
             defaulted to CURSOR
\ap\pa
          out_specs = output specifications:
             (d) table name, may be same as given in in_specs (b),
                 if same table only the magnitude, sigma-mag., sky value,
                 sigma-sky, xcenter, ycenter, no. of pixels used for flux and
                 the flux will be stored in columns 
                 :MAGNITUDE, :MAG_SIGMA, :SKY, SKY_SIGMA, :XCEN, :YCEN,
                 :MAGPIX and :FLUX.
                 If new table, then in addition to the columns explained above,
                 also the columns labeled :XSTART, :YSTART, :XEND and :YEND 
                 will be filled.
             (e) descriptor,D  if the results should be stored in a real
                 descriptor of the involved image frame, data will be stored
                 as xstart, ystart, xend, yend, magnitude, mag-sigma, sky
                 and sky-sigma
             (f) ? , the default - just display results on the terminal
\ap\pa
          pix_area = size of flux area (Fs), no_man's_land area (Ns),
                     and background/sky area (Bs):
             either as "Fs,Ns,Bs" for square flux area,
             or as "Fsx,Fsy,Ns,Bs" for rectangular area;
             given as world coordinates or pixel numbers.
             The innermost, flux square (rectangle) will have a size of 
             Fs*Fs (Fsx*Fsy) pixels, to that a strip of Ns pixels
             and Bs pixels is added on all 4 sides, i.e. always same Ns, Bs 
             value in x- and y-direction.
             Thus total area will be a square of (Fs + Ns*2 + Bs*2)**2 pixels
             or a rectangle of (Fsx + Ns*2 + Bs*2) * (Fsy + Ns*2 + Bs*2) pxs.
             If `in_specs' specifies table input (b), then the `pix_area'
             may also be specified as "+,Ns,Bs" or "-,Ns,Bs". This syntax
             indicates that the table defined sub-window is used as the
             flux area and no_man's_land, sky are added to it (+), or that
             the subwindow less no_man's_land, sky is used as flux area (-);
             defaulted to @13,@2,@2
\ap\pa
          out_opt = options for descriptor or table output:
             A, append data to table/descriptor, else start at the beginning,
             applicable to (d) or (e)
             I, fill column labeled :IDENT in output table with character
             string typed in at the terminal, only applicable to (d) with
             cursor input
             if you want to append rows to the output table and also fill the
             :IDENT column, set `out_opt' = AI;
             defaulted to `+', i.e. none of the above
\ap\pa
          center_params = center_flag,kappa
             center_flag = 1 or 0,
               if 1 we first find the center of the subimage and shift to it
               the center of the window we use for the magnitude calculation;
               else we use the given subimage to calculate the magnitude,
             kappa = factor for kappa-sigma clipping algorithm which is used
               to calculate the mean background (sky);
             defaulted to 0,2. for cursor input, in_specs (a),
             to 1,2. for (b) + (c1), and to 0,2. for (c2)
\ap\pa
          curs_specs = no_curs,drawflg1,drawflg2,max_input:
             no_curs = 1 or 2 for single cursor or cursor rectangle,
             if single cursor a fixed rectangle according to `pix_area',
             i.e. Fs,Bs,Ns or Fsx,Fsy,Ns,Bs around the cursor is used;
             for 2 cursors only Nsiz and Bsiz are relevant, Fsx,Fsy will be
             determined from the size of the cursor rectangle;
             drawflg1 = 1/0 for drawing rectangles in overlay plane or not,
             drawflg2 = 1/0 for drawing labels in overlay plane or not,
             max_input = max. of cursor inputs.
             this parameter is only used/applicable if in_specs = CURSOR (a),
             defaulted to 1,1,0,9999
\ap\pa
          zw_option = zwindow_flag,zoom;
             zwindow_flag = W for zoom_window, N for none,
             zoom = initial zoom factor;
             only applicable to X11 window displays and option (a),
             defaulted to N,4 (see the help of GET/CURSOR for more info about
             the additional functionality provided in that mode);
\ap\sa
See also:
          MAGNITUDE/CENTER, MAGNITUDE/CIRCLE, INTEGRATE/APERTURE
          CENTER/MOMENT
\as\no
Note:       
          The magnitude calculation is done in a two-step procedure:
            First, the center of the subimages is computed (using the same
          algorithm as in CENTER/MOMENT) , which is then used for the
          magnitude calculations.
            Besides the usual errors due to invalid input parameters, the
          algorithm may fail depending upon the chosen region and  
          return Nan, Inf, and other such nasty values as magnitude...
          Keyword OUTPUTR(13) serves as a control flag:
          OUTPUTR(13) < 0.0 if something failed in the execution of the 
          command, otherwise it's > 0.0.
             When using the `image,table' input option (b) and the table
          contains also the columns :XCEN, :YCEN, then these center values
          determine the subimages we work on (instead of using :XSTART, ...).
          Depending upon `center_flag' the resulting subimage may again be
          shifted to the newly determined centers.
            When using the cursor input option (a), 3 squares will be drawn in
          the overlay plane (if `drawflg1' of param. `curs_specs' is set to 1)
          showing the respective areas for the flux calculation,
          the no_man's_land and the background/sky area.
            For X-Windows: Press ENTER button on the mouse to start magnitude
          calculations and draw all the squares. Use the mouse to move
          single cursor or cursor rectangle. Use the arrow keys to change
          the size of the cursor rectangle. To exit press the EXIT button
          on the mouse.
          If you also use the `zw_option', the cursor in the main display
          is only used to define the subwindow to work on. Then move the
          cursor to the auxiliary (zoom) window and proceed as described
          above.
            When using the out_opt ID for table output, computations proceed
          after pressing the ENTER button and(!) entering also an identifier
          of max. 8 characters and hitting RETURN on the keyboard.
          Otherwise the string IDabcd will be written into column :IDENT
          with "abcd" the sequence number.
            Last results of magnitude calculations are also written into
          keyword OUTPUTR.
          Xstart, ystart, xend, yend, xcenter, ycenter are also stored
          as double precision values in keyword OUTPUTD(1 - 6).
            The mean background level is estimated by iterating in a kappa-sigma
          clipping procedure (max_iter=10) with controllable `kappa' (via
          parameter `center_params').
\on\exs
Examples:
\ex
          MAGNITUDE/RECTANGLE
            Use single cursor to define center of subimages, display start- 
            and end-coordinates of subimage as well as magnitude, flux, 
            sky values and associated errors on terminal only. The size of
            flux, noman, sky area is 13, 2, 2 pixels (default) respectively.
\xe\ex
          MAGNITUDE/RECTANGLE CURSOR VALUES,D
            As above but write data to beginning of real descriptor VALUES
            of displayed frame.
\xe\ex
          magnitude/RECTANGLE ? values ? ? 1,3.0
            As above but store data into table `values.tbl' (will be created
            by MIDAS), also we recenter the subimage before computing the 
            magnitudes and use a kappa of 3.0.
\xe\ex
          MAGNITUDE/RECTANGLE CURSOR values @20,@10,@6 I
            As above but enter character identifier via keyboard and use
            different Fs,Ns,Bs
\xe\ex
          MAGNITUDE/RECTANGLE CURSOR values @23,@41,@10,@6 A
            As above but append rows with data to table `values.tbl' (if the
            table does not exist yet, it will be created), use 23 * 41 area 
            for flux calculation, outer rectangle will have  55 * 73 pixels.
\xe\ex
          MAGNITUDE/RECTANGLE ccd001,sources
            Use columns :XSTART, :YSTART, :XEND, :YEND of table `sources.tbl'
            to define the subimages of `ccd001.bdf', just display results
            on terminal.
\xe\ex
          MAGNITUDE/RECTANGLE ccd001,sources sources
            As above but store results also in same table `sources.tbl' .
\xe\ex
          MAGNITUDE/RECTANGLE ccd001,sources ? +,@6,@6
            Use table `sources.tbl' as input, the flux area is defined by the
            size sx*sy of each subimage (sx = :XEND-:XSTART+1 and
            sy = :YEND-:YSTART+1). Size of outermost rectangles will be
            (sx+2*Ns+2*Bs) * (sy+2*Ns+2*Bs).
\xe\ex
          MAGNITUDE/RECT cursor p7=w
            As first example but use zoom window to determine working area
            via cursor, but we need X11 for that...
\xe\ex
          MAGNITUDE/RECT ccd001,@120,@320
            Use the subimage centered at xpix=120,ypix=320 of `ccd001.bdf',
            just show results on terminal.
\xe \sxe
