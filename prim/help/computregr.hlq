% @(#)computregr.hlq	19.1 (ESO-IPG) 02/25/03 14:03:03 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      computregr.hlq
%.AUTHOR     JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, COMPUTE/REGRESSION
%.PURPOSE    On-line help file for the command: COMPUTE/REGRESSION
%.VERSION    1.0  12-OCT-1983 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./REGR
\es\co
COMPUTE/REGRESSION					12-OCT-1983  JDP
\oc\su
COMPUTE/REGRESSION table column = name[(ind)]  [d_type]
	compute result of a regression
\us\pu
Purpose:
              Compute the result of a regression. A new column is created or
              values in a previously existing column are replaced by the
              fitted values in the regression. Regression coefficients
              are referred to by the name assigned in a previous SAVE/REG
              command.
\up\sub
Subject:
              Regression, fitting data
\bus\sy
Syntax:
              COMPUTE/REGRESSION table column = name[(ind)] [d_type]
\ys\pa
              table = table name
\ap\pa
              column = output column, either already existing or a new one
                    created by the command.
\ap\pa
              name = user's name of the regression coefficients saved
                    by a previous SAVE/REGRESSION command
\ap\pa
              ind = optional column containing the independent variable(s).
                    By default the command uses the same column(s) as in the
                    command REGRESSION.
\ap\pa
              d_type = defines the data type for the output column as R*4 or
                    R*8. If the column already exists this parameter is not
                    relevant. By default the output type is R*4. 
\ap\no
Note: 
              The computed result of the regression can be used to select
              on small residuals and repeat the REGRESSION command to get
              better coefficients. The select mask is reset to the complete
              table before the computation.
\on\see
See also:
              REGRESSION/TABLE, SAVE/REGRESSION
\ees\exs
Examples:
\ex
              The following sequence of commands will compute the result
              of a polynomial regression between columns :X and :Y
\xe\ex
              REGRESSION/POLYNOMIAL mytable :Y :X 2
\xe\ex
              SAVE/REGRESSION       mytable test
\xe\ex
              COMPUTE/REGRESSION    mytable :YFIT = test(:X)
\xe \sxe
