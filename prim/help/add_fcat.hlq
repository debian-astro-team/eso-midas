% @(#)add_fcat.hlq	19.1 (ESO-IPG) 02/25/03 14:02:55 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      add_fcat.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, ADD/FCAT
%.PURPOSE    On-line help file for the command: ADD/FCAT
%.VERSION    1.0  21-DEC-1989 : Creation, KB
%.VERSION    1.1  20-JUN-1991 : Update, KB
%----------------------------------------------------------------
\se
SECTION./FCAT
\es\co
ADD/FCAT					25-SEP-1992  KB
\oc\su
ADD/FCAT [cat_name] file_list [lowstr,histr]
	add entries to a fitfile catalog
\us\pu
Purpose:
          Add entries to a fitfile catalog.
\up\su
Subject:  
          Fit files, catalogs
\us\sy
Syntax:  
          ADD/FCAT [cat_name] file_list [lowstr,histr]
\ys\pa
          cat_name = name of Fit file Catalog,
                     defaulted to currently active Fit file Catalog
\ap\pa
          file_list = list of files to be added,
\\
               maybe file specifications, separated by a comma (no spaces!);
\\
               or a single catalog name;
\\
               or wildcard specifications (e.g. a3*,n*.fit)
\ap\pa
          lowstr,histr = optional low and high strings;
\\
          if given, only fit files with names >= `low' and <= `high' will 
          be added
\ap\no
Note:
          The different options for the file_list may not be mixed!
\\
          Entries are added automatically to the currently "active" fit file
          catalog (SET/FCAT command makes a catalog active).
\on\see
See also: 
          SET/FCAT, CREATE/FCAT, CLEAR/FCAT, READ/FCAT, SUBTRACT/FCAT
\\
          ADD/ICAT, ADD/TCAT
\ees\exs
Examples:
\ex
          ADD/FCAT dec88 func01
          add an entry for fit file func01.fit to catalog dec88.cat.
\xe\ex
          ADD/FCAT dec88 func0*
          Add entries for all fit files where the names begin with the
          string "func0" to catalog dec88.cat.
\xe\ex
          ADD/FCAT dec88 d* demo0025.fit,demo0036.fit
          Add entries for fit files with names demo0025.fit -> demo0036.fit
          to catalog dec88.cat.
\xe\ex
          ADD/FCAT dec88 nov89.cat
          Add entries for all files which are in the fit file catalog nov89.cat.
\xe \sxe
