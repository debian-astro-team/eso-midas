% @(#)read_setu.hlq	19.1 (ESO-IPG) 02/25/03 14:03:38 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      read_setu.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, READ/ICAT
%.PURPOSE    On-line help file for the command: READ/SETUP
%.VERSION    1.0  10-OCT-1988 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./SETU
\es\co
READ/SETUP	 	                            27-JAN-1994  KB
\oc\su
READ/SETUP setup
	read the contents of the variables related to a Setup
\us\pu
Purpose: 
	  Read the contents of the variables related to a Setup.
\up\sy
Syntax:
          READ/SETUP setup
\ys\pa
          setup = name of a Setup
\ap\sa
See also:
          INITIALIZE/SETUP, INFO/SETUP, WRITE/SETUP
\as\no
Note:  
          Setups are a collection of variables (keywords) which have to be 
          set up before certain, more complex commands can be executed.
\\
          To get a list of all currently implemented Setups use the
          command INFO/SETUP.
\on\exs
Examples:
\ex
          READ/SETUP catalog
            Display the contents of all the variables related to the Setup 
            `catalog'. These variables will be used by the EXECUTE/CATALOG
            command.
\xe \sxe
