% @(#)averagwind.hlq	19.1 (ESO-IPG) 02/25/03 14:02:58 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      averagwind.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, AVERAGE/WINDOW
%.PURPOSE    On-line help file for the command: AVERAGE/WINDOW
%.VERSION    1.0  06-MAY-1986 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./WIND
\es\co
AVERAGE/WINDOW						06-MAY-1986  KB
\oc\su
AVERAGE/WINDOW out = in_specs [meth] [bgerr,snoise]
	compute average of (consistent) pixel values
\us\pu
Purpose:
          Compute average of (consistent) pixel values
\up\sy
Syntax:
          AVERAGE/WINDOW out = in_specs [meth] [bgerr,snoise]
\ys\pa
          out = result frame
\ap\pa
          in_specs = specification of input frames, either
\\
             in1,in2,...inj  (j < 21)  or
\\
             catalog.cat = name of catalog containing the input frames
                           (at most 20 frames)
\ap\pa
          meth = WINDOW, MEDIAN, MAXIMUM or MINIMUM, defaulted to WINDOW
\ap\pa
          bgerr,snoise = estimated background error and Gaussian noise only
             used for meth = WINDOW, defaulted to 0.1,0.1
\ap\sa
See also:
          AVERAGE/IMAGE, AVERAGE/MEDIAN, AVERAGE/KAPPA, AVERAGE/AVERAGE,
          AVERAGE/WEIGHTS, AVERAGE/COLUMN, AVERAGE/ROW
\as\no
Note:
          Except the method WINDOW, all the functionality of AVERAGE/WINDOW 
          is also provided by the more flexible command AVERAGE/IMAGE. 
\\
          Method = WINDOW:
\\
            The output image will contain the flux in units of the exposure
            time. The background is subtracted from the images before the flux
            is computed. Only the overlapping area of all input frames will be
            compared.
\\
            The following descriptors must exist for each input frame:
\\
            real LHCUTS(5),(6) - low and high limit for valid pixels
\\
            double prec. O_TIME(7) - exposure time in seconds (defaulted to 1.)
\\
            real FLAT_BKG(1) - background value. (defaulted to 0.)
\\
            The pixels of the input frames are compared with the median
            value (over all input frames). A pixel is rejected, if its
            uncertainty does not overlap with the median value. The formula
            for the uncertainty of the flux is
\\
            SQRT( bgerr**2 + (v-backgr)*snoise**2 ) / expo_time .
\\
            Where v is the pixel value and backgr the background. If all pixels
            at a certain (x,y) coordinate are rejected, the corresponding pixel
            of the first input image is used.
\\
            Statistics of rejected pixels will be shown after the comparison.

          Method = MEDIAN, MAXIMUM or MINIMUM:
\\
            The output image will contain the median, maximum or minimum of all
            the input frames.
\on\exs
Examples:
\ex
          AVERAGE/WINDOW good = ccd001,ccd002,ccd008 WINDOW 0.2,0.1
           Use the frames `ccd001.bdf', `ccd002.bdf' and `ccd08.bdf' as input.
\xe\ex
          AVERAGE/WINDOW darkres = dark.cat MEDIAN
           Use all frames with an entry in the catalog `dark.cat' 
           (but max.  20 frames) as input.
\xe \sxe
