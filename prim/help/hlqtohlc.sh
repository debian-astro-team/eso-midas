#! /bin/sh
# @(#)hlqtohlc.sh	19.1 (ESO-IPG) 02/25/03 14:03:24
# 
#  Bourne shell script "hlqtohlc.sh"
#  to build the .hlc files from the .hlq files
#  910529  KB
#  910712  CG. removing temporary files.
# 
mv alll.hlc alll.hlk
rm -f *.hlc
# 
for i in `ls *.hlq | \
	sed  -e 's/_.*//'  -e 's/\.hlq//' | \
	awk '{print substr($1,1,6)}' | \
	sort -u`
do
   for file in `ls $i*.hlq`
   do
      cat $file | sed -n '/SECTION./,/\\us/p' -e '/^\\/d' -e '2d' >> $i.hlc
   done
done
mv alll.hlk alll.hlc
