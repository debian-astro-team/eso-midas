% @(#)info_desc.hlq	19.1 (ESO-IPG) 02/25/03 14:03:25 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      info_desc.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, INFO/DESCRIPTOR
%.PURPOSE    On-line help file for the command: INFO/DESCRIPTOR
%.VERSION    1.0  11-MAY-1992 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./DESC
\es\co
INFO/DESCR					11-MAY-1992  KB
\oc\su
INFO/DESCR frame descr
	get type and size of descriptor
\us\pu
Purpose: 
          Get type, no. of elements and bytes per element of descriptor.
\up\sy
Syntax:  
          INFO/DESCR frame descr
\ys\pa
          frame = name of image or table or fit file
\\
              file type defaulted to `.bdf'
\ap\pa
          descr = descriptor name
\ap\sa
See also:
          SHOW/DESCR, READ/DESCR, INFO/IMAGE, INFO/KEYWORD, COMPUTE/KEYWORD
\as\no
Note:  
          This command is especially useful inside MIDAS procedures.
          No output is generated, only the integer keyword MID$INFO is filled
          as follows:
\\
          If the frame could not be opened MID$INFO(1) = -1.
\\
          IF the descriptor could not be found, MID$INFO(1) = 0.
\\
          Otherwise MID$INFO(1) contains the type of the descriptor,
\\
          1 = integer, 2 = real, 3 = character, 4 = double precision.
\\
          MID$INFO(2) holds the no. of elements of the descriptor and
          MID$INFO(3) has the no. of bytes per element.
\\
          The function M$EXISTD of the command COMPUTE/KEYWORD provides
          similar but less detailed information.
\on\exs
Examples:
\ex
          INFO/DESC durazno start
            If frame `durazno.bdf' is a valid MIDAS image, MID$INFO(1) = 4,
            MID$INFO(2) >= 1 and MID$INFO(3) = 8.
\xe \ex
          INFO/DESC pera.tbl tblcontr
            If table `pera.tbl' exists, MID$INFO(1) = 1, MID$INFO(2) = 10 and 
            MID$INFO(3) = 4.
\xe \sxe
