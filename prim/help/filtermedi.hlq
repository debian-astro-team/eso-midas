% @(#)filtermedi.hlq	19.1 (ESO-IPG) 02/25/03 14:03:21 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      filtermedi.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, FILTER/MEDIAN
%.PURPOSE    On-line help file for the command: FILTER/MEDIAN
%.VERSION    1.0  01-JUL-1988 : Creation, KB
%.VERSION    1.1  31-MAY-1991 : Remove edge comments, KB
%----------------------------------------------------------------
\se
SECTION./MEDI
\es\co
FILTER/MEDIAN					17-MAR-1994   KB
\oc\su
FILTER/MEDIAN frame outfram [filt_specs] [flag] [subima] [options]
	smooth an image with median filter
\us\pu
Purpose: 
            Apply median filter to a frame.
\up\sub
Subject:  
            Image smoothing, image defects, nonlinear filtering.
\bus\sy
Syntax:  
            FILTER/MEDIAN frame outfram [filt_specs] [flag] [subima] [options]
\ys\pa
            frame = input frame
\ap\pa
            outfram = result frame
\ap\pa
            filt_specs = `radx,rady,threshold' or `radx,rady,i_low,i_hi'
\\
                         or `radx,rady,fa,fb';
\\
                 radx, rady is the radius in x and y, i.e. the number of pixels
                 "around" the central pixel which define the neighbourhood 
                 (NBH) of that pixel,
\\
                 total no. of pixels in NBH is then: (2*radx+1)*(2*rady+1);
\\
                 threshold is in [0.,1.] (if relative) or >= 0. (if absolute);
\\
                 i_low, i_hi are the low intensity, high intensity for the
                 fixed interval option (flag(2) = 'F'),
\\
                 < and > may be used to indicate lowest and highest value.
\\
                 fa, fb are the factors used in computing the threshold
                 used with flag(2)='Z'
\\
                 Default: 1,1,0.0
\ap\pa
            flag = two character flag,
\\
                 (1) Y or N, to indicate inclusion of central pixel in the
                     calculation of median over the neighbourhood,
\\
                 (2) A for absolute threshold,
\\
                     R for relative threshold using pixel value,
\\
                     Q for relative threshold using median value,
\\
                     F for fixed intensity interval,
\\
                     Z for computing the threshold according to the formula
                       fa + (fb * SQRT(median_value))
\\
                     Default: YA
\ap\pa
            subima = optional specification of a subimage on which the filter
                 will be applied, may be either [xstart,ystart:xend,yend]
                 (see page 3.4 in the MIDAS User manual, chapter 3) to indicate
                 the limits of the subimage (with coordinates given according
                 the MIDAS standard), or
\\
                 CURSOR if subimage will be chosen interactively, or
\\
                 table_name if the table "table_name" holds the endpoints of
                 the subimage(s) in columns labeled :XSTART, :YSTART, :XEND
                 and :YEND,
\\
                 defaulted to the complete input frame
\ap\pa
            options = flag of 3 characters for options;
\\
                 (1) L or N for loop or no loop,
\\
                 (2) D or N for display of smoothed area or no display;
\\
                 (3) E or N for expanding input frame first, so that result
                     frame has same dimensions as input frame or no expansion
                     with result frame smaller than input
\\
                 defaulted to NNE
\ap\see
See also:
            FILTER/MAX, FILTER/MIN, FILTER/GAUSS, FILTER/DIGITAL,
\\
            FILTER/SMOOTH, TUTORIAL/FILTER,
\\
            FFT/IMAGE, CONVOLVE/IMAGE, DECONVOLVE/IMAGE
\ees\no
Note:
            Smooth the image by
\\
            1) calculating M(x,y), the median of all points in the
               neighbourhood of (x,y), which contains all pixels (x',y') with
               abs(x-x') .LE. radx  and  abs(y-y') .LE. rady
               (radx, rady as given in the 3rd parameter)
               So radx = 1 and rady = 1 yield the usual 3*3 neighbourhood.
\\
            2) replacing the original value I(x,y) by M(x,y), if we have for
               ABS = abs(M(x,y)-I(x,y)):
\\
               ABS > thresh (flag(2) = A),
               ABS > thresh*I(x,y) (flag(2) = R),
               ABS > thresh*M(x,y) (flag(2) = Q),
\\
               or if I(x,y) in [intens_low,intens_hi] (flag(2) = F),
               (thresh and intens_low, intens_hi as given in the 3rd parameter)
\\
            The values radx and rady do not have to be equal, by setting
            radx or rady to 0, also 1-dimensional filters may be used.
\\
            If the subimage option is chosen, it must be at least as big as the
            filter kernel which has  (2*radx+1) * (2*rady+1) pixels.
\\
            If `option(3:3) = 'E' the input image is expanded by 2*radx columns
            and 2*rady lines, so that also the "edge" pixels can be calculated.
\\
            Note, that the flux is not conserved.
\on\exs
Examples:
\ex
            FILTER/MEDIAN gordo flaco 1,1,0.5 yr
              For image `gordo.bdf' calculate median over a 3*3 neighbourhood
              and replace the original value by the median value only,
              if median value differs more than 0.5*original value from the
              original value.
\xe\ex
            FILTER/MED gordo flaco 1,1,0.5 ? CURSOR
              Smooth image `gordo.bdf', get median over a 3*3 neighbourhood
              and replace the original value by the median value only, if the
              median value differs by more than 0.5 from the original value,
              also apply the median filter only to subimages of `gordo.bdf'
              which will be chosen via the cursor.
\\
              It is assumed, that image `gordo.bdf' is loaded into the image
              display!
\xe\ex
            FILTER/MED gordo flaco 1,1,0.5 YA coords
              As above, but the subimage specifications are taken from
              the table `coords.tbl'. This table could e.g. be created by using
              the command GET/CURSOR with the table option (and 2 cursors!)
\xe\ex
            FILTER/MED gordo flaco 3,0,0. NA [@20,<:@50,>]
              Smooth image `gordo.bdf', calculate 1-dim median over 7 x-pixels;
              do not include the central value in the median calculation and
              always replace the original value by the median value.
\\
              Apply the median filter only to the subimage of frame `gordo.bdf'
              which consists of the rows between row no. 20 and row no. 50.
\xe\ex
            FILTER/MED gordo flaco 3,2,22.3,24.6 NF ? NNN
              Apply median filter over a 7*5 neighbourhood only to pixels which
              have an intensity in the interval [22.3,24.6]. Do not include the
              central pixel in the calculation of the median.
              `flaco.bdf' will have xdim = NPX-6 and ydim = NPY-4, with
              NPX, NPY the no. of pixels in x,y of the input frame `gordo.bdf'.
\xe\ex
            FILTER/MED grande chico 3,3,0.9,1.2 YZ 
              Apply median filter over a 7*7 neighbourhood and replace the
              original value by the median value only, if the median value
              differs by more than `d' (with d = 0.9+1.2*SQRT(median)) from
              the original value.
\xe \sxe
