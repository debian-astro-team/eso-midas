/*===========================================================================
  Copyright (C) 1988-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.TYPE		Module
.IDENTIFICATION tvhelp.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	On-line editing facilities
.ENVIRONMENT	TermWindows
.COMMENTS	This module contains routines to show the Help Window.
\begin{TeX}
\\
The Help Window displays the Available Control Characters.
\end{TeX}

.VERSION 3.0	20-Apr-1988: Creation for MIDAS usage

 090831		last modif

----------------------------------------------------------------------------*/

#define		DEBUG		0	/* For debugging only		*/

#define PM_LEVEL 	LEVEL_TV

#include <midas_def.h>

#define TW_import	0	/* Do not use macro window definitions	*/
#define TW_STRUCT	0	/* Do not use window Structures		*/
#include <twset.h>


extern int tv_attr(), tv_nl(), tv_wa(), tv_where();

extern int pm_enter(), pm_iexit();


MID_EXTERN	TERM   *terms;

#if 	DEBUG
#define ENTER_DEBUG(x)	ENTER(x)
#define EXIT_DEBUG(x)	EXIT(x)
#else
#define ENTER_DEBUG(x)	
#define EXIT_DEBUG(x)	return(x)
#endif

MID_STATIC	ACHAR *help_text = NULL_PTR(ACHAR);
MID_STATIC  int hlines = 0;
static int  J;		/* Width of help text */

#define		hcols		27

MID_RSTATIC char *Explanations[] = {
	NULL_PTR(char),			/* TW_cc_NONE	0	*/
	"Interrupt",			/* TW_cc_INT	1	*/
	"Emergency Stop", 		/* TW_cc_QUIT	2	*/
	"Exit (EOF)", 			/* TW_cc_EOF	3	*/
	"Show Help",			/* TW_cc_HELP	4	*/
	"Refresh Input", 		/* TW_cc_REFR	5	*/
	"Switch Insert/Replace", 	/* TW_cc_MODE	6	*/
	"Escape (raw)",			/* TW_cc_RAW	7	*/
	"Return",			/* TW_cc_RET	8	*/
	"Move Up",		 	/* TW_cc_UP	(9+_UP_)*/
	"Move Down", 			/* TW_cc_DOWN	(9+_DOWN_)*/
	"Move Left    <==", 		/* TW_cc_LEFT	(9+_LEFT_)*/
	"Move Right    ==>", 		/* TW_cc_RIGHT	(9+_RIGHT_)*/
	"Move Home",			/* TW_cc_HOME	(9+_HOME_)*/
	"Move to End_of_Line",		/* TW_cc_EOL	14	*/
	"Move to Next Word",		/* TW_cc_NW	15	*/
	"Erase Previous Char",		/* TW_cc_DELC	16	*/
	"Erase Previous Word", 		/* TW_cc_DELW	17	*/
	"Erase to End_of_line", 	/* TW_cc_DELE	18	*/
	"Erase Whole"			/* TW_cc_DELL	19	*/
  };

	MONITOR(TVHELP);


/*==========================================================================
 *			Create
 *==========================================================================*/
static int Create()
/*+++
.PURPOSE Open and Create the Standard Help Text
.RETURNS OK / NOK
.REMARKS 
---*/
{
	char *p;
	ACHAR *pa;
	int i, j, k, cols_per_line;
	MID_STATIC char cta[5] = " ^  ";

  ENTER("Create");

  status = NOK;
  if (help_text)		EXIT(OK);

	/* Compute Required Lines/cols	*/

  cols_per_line = (terms->dim[1] - 1) / hcols;
  for (i = sizeof(terms->tc), hlines = 0, j = 0; --i >=0; )
  {  	if_not (terms->tc[i])		continue;
    	if (terms->tc[i] == TW_cc_RET)	continue;
	if (--j <= 0)	hlines++, j = cols_per_line;
  }
  terms->standout &= ~ChangedControl;

	/* Allocate Memory to Helps Text	*/
  
  J = 1 + (hcols * cols_per_line);
  if_not(help_text = MEM_GET(ACHAR, (hlines+2)*J))	FINISH;
  status = OK;
  	
	/* Fill with Graphic Characters and Blanks */

  for (i = hlines*J, pa = help_text+J; --i >=0; pa++)	*pa = ' ';

  k = J * (hlines+1);	/* Write 1st + last lines */
  for (j = J, pa = help_text; --j >=0; pa++)
  	*pa     = GraphicAchar(_HORIZONTAL_), *(pa+k) = *pa;
  for (i = hlines; --i >= 0; pa++)
  {	*pa = GraphicAchar(_VERTICAL_);
  	for (j = cols_per_line; --j >=0; )
	{	pa += hcols;
		*pa = GraphicAchar(_VERTICAL_);
	}
  }
  pa = help_text, *pa = Agraphic | CornerChar(_UPLEFT_);
  pa += J-1,  *(pa++) = Agraphic | CornerChar(_UPRIGHT_);
  pa += hlines*J, *pa = Agraphic | CornerChar(_DOWNLEFT_);
  pa += J-1,      *pa = Agraphic | CornerChar(_DOWNRIGHT_);

  k = J * (hlines+1);	/* Write 1st + last lines Tees */
  for (j = cols_per_line-1, pa = help_text+hcols; --j >=0; pa += hcols)
  	*pa = Agraphic | TeeChar(_UP_), *(pa+k) = Agraphic | TeeChar(_DOWN_);

	/* Edit the lines   */

  for (i = 0, k = 0; i < sizeof(terms->tc); i++)
  {	if_not(terms->tc[i])		continue;
    	if (terms->tc[i] == TW_cc_RET)	continue;
	pa = help_text + ((k%hlines)+1)*J + (k/hlines)*hcols;
	k++;
	if (i >= 32)	p = "Del ";
	else		/* Issue Control Character */
		cta[2] = '@' + i, p = cta;
	for (j = 4; --j >= 0; p++) *++pa = _BOLD_<<8 | *p;
	p = Explanations[(int)terms->tc[i]];
	for (j = strlen(p); --j >= 0; p++) *++pa = *p;
  }

  FIN:
  EXIT(OK);
}

/*==========================================================================
 *			tv_helps
 *==========================================================================*/
int tv_help()
/*+++
.PURPOSE Edit the Help Window
.RETURNS OK
.REMARKS 
---*/
{
	int i;
	ACHAR *pa;
	static short int pos[2];
	unsigned char old_attr;

  ENTER("tv_helps");

	/* Check if Controls were not changed	*/
  if ( (terms->standout & ChangedControl))
	MEM_FREE(help_text), help_text = NULL_PTR(ACHAR);

  if_not(help_text)
  	if_not(Create())	EXIT(NOK);	/* Creation Failed ... */

	/* Edit on Terminal */
	
  old_attr = terms->attr;
  GetCursor(pos);
  if (pos[1])	NewLine();
  
  for (i = hlines+2, pa = help_text; --i >=0; pa += J)
	WriteAchars(pa, J), NewLine();
  SetAttr(old_attr);

  EXIT(OK);
}

