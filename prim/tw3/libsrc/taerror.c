/*===========================================================================
  Copyright (C) 1989-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.TYPE		Module
.IDENTIFICATION taerror.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Window Management
.ENVIRONMENT	TermWindows
.COMMENTS	This module contains a simple application of writing
	an error onto a dedicated window.
	The name of this window is "error".
	The message is assumed to be in LaTeX when it starts with \;
	normal text is assumed otherwise.

.VERSION 1.0	15-Jun-1989: Creation

 090902         last modif
----------------------------------------------------------------------------*/

#define	DEBUG		0	/* For debugging only		*/

#define	DISPLAY_ATTR	(_BOLD_|_BLINK_|_REVERSE_)

#define PM_LEVEL 	LEVEL_TW-1
#include <stesodef.h>
#include <tw.h>


extern int tx_display();

extern int ta_ret();

extern int tv_dim(), tv_supply(), tv_where(), tv_bell();

extern int tw_fill(), tw_attr(), tw_uattr(), tw_r(), tw_goto(), tw_rule();
extern int tw_st(), tw_uflag(), tw_chars(), tw_copw(), tw_flags();
extern int tw_uw(), tw_write(), tw_il(), tw_clear(), tw_dc();
extern int tw_nl(), tw_upw(), tw_uc(), tw_mvc(), tw_copy();
extern int tw_mrule(), tw_cus(), tw_gc2(), tw_close(), tw_cur();
extern int tw_home(), tw_tra(), tw_wa(), tw_agoto(), tw_mods();
extern int tw_cline();

extern int pm_enter(), pm_iexit(), pm_tr2(), pm_ed_i();
extern int pm_trace();
extern int eh_put1();



static WINDOW *we = NULL_WINDOW;	/* The Error Window */


/*==========================================================================
 *			ta_error
 *==========================================================================*/
int ta_error(msg)
/*+++
.PURPOSE Display the `msg' message on the `error' window; a NULL `msg'
	displays the current error status.
.RETURNS 0 if error window is removed, 1 if error window is present.
.REMARKS When `msg' is NULL and no error exists, the error window is removed.
	The error window is by default the top line.
---*/
	char *msg;	/* IN: Message to display, or NULL 	*/
{
	int	state;
	char	*p;
	TWSAVE	pos;
  
  ENTER("ta_error");

	/* Try first to locate the Error window. If was not opened,
	 * do it.
	 */
	 
  if (we == NULL_WINDOW)	we = GetWindow("error");
  if (we == NULL_WINDOW)	
  	we = OpenWindow("error", 0, 0, 1, 0, DISPLAY_ATTR, _DISPLAY_, 0);

  SaveCursor(&pos);

  p = (msg ? msg : ERR_GET());	/* Take argument, or logged error */
	
  if (p)	
  {
  	state = 1;
  	ClearWindow(we);
  	if (*p == '\\')	DisplayString(we, p);
  	else if (*p)	PutCentered(we, p);
	RaiseWindow(we);
  }
  else	state = 0, RemoveWindow(we);

  RestoreCursor(&pos);

  EXIT(p ? 1 : 0);
}

