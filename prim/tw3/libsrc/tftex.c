/*===========================================================================
  Copyright (C) 1989-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++
.NAME           tftex.c
.TYPE		Module
.LANGUAGE       C
.CATEGORY       User interface, forms handling.
.ENVIRONMENT    TermWindows
.AUTHOR		Alan Richmond, Francois Ochsenbein
.COMMENTS
	This module loads the form structure from a \LaTeX-like
	source text or file.
.VERSION  1.0	08-Jun-1989     Extracted from Proteus

 090902         last modif
---------------------------*/

#define	PM_LEVEL	LEVEL_TF

#define  PASCAL_DEF	0	/* Don't include Pascalisation 	*/

#include <stesodef.h>	/* Standard definitions	*/
#include <tform.h>	/* for form values	*/
#include <str.h>	/* String utilities	*/
#include <tra.h>	/* Conversion utilities	*/
#include <cc.h>		/* for Ccode compilation*/


extern int mm_free(), eh_ed_as();

extern int pm_enter(), pm_iexit(), pm_tr2(), pm_ed_i(), pm_ed_trace();

extern int tx_display(), tx_file();

extern int tw_fill(), tw_attr(), tw_uattr(), tw_r(), tw_goto(), tw_rule();

extern int tff_goto(), tff_convert(), tff_edit(), tff_display();
extern int tff_new(), tff_dset(), tff_oshow();

extern int tf_opwd(), tf_cc(), tff_oshow();




#define  FINISH		goto FIN

static	TFORM	*form;		/* For communication */
static	char	with_file = 0;

/*=====================================================================*/
static int tftex(formname, filename, fw)
/*++++++++++
.PURPOSE Load a form from a screen definition file (default extension sdf)
.REMARKS 
.RETURNS The form created. This new form also becomes the current one.
--------------*/
	char	*formname;	/* IN: Form name		*/
	char	*filename;	/* IN: File Name		*/
	WINDOW	*fw;		/* IN: The window to use	*/
{
	TWFIELD	*fields, *pwf;
	int	nf, status;
	WINDOW	*w;
	char	*saved;


  saved = (char *)0;
  form = NULL_PTR(TFORM);
  if (with_file)	TRACE_ED_STRING("File: ", filename), 
  			saved = strsave(filename);

	/* Allocate a New Window if Necessary	*/

  if_not(w = fw)
  	w = OpenWindow(formname, 0, 0, 0, 0, _NORMAL_, _TITLE_, 0);
	
	/* Display the form in the .sdf */

  if (with_file)	
  	status = DisplayFile(w, NameFile(filename, ".sdf"));
  else	status = DisplayString(w, filename);
  if (status != OK)
  {
	ERR_ED_STRING("Form can't fit from file: ", saved);
	FINISH;
  }
  RaiseWindow(w);
	
  if_not(fields = GetMarkedFields(w))	/* Get the Fields */
  {
	ERR_ED_STRING("No field defined in file: ", saved);
  	FINISH;	
  }

	/* Count the number of Fields.	*/

  for (pwf = fields; pwf->name; pwf++) ;
  nf = (pwf - fields);

	/* Create a New Form. 	*/

  form = tf_new(formname, nf, w);
  
  	/* Create successively the Fields */

  for (pwf = fields; pwf->name; pwf++)
	tff_new(form,	pwf->name,	
			pwf->type,
			pwf->pic,
			pwf->home[0],
			pwf->home[1],
			pwf->dim[0],
			pwf->dim[1],
			(TF_FCT)0);

	/* Remove options for non-echo Fields */

  tf_opwd(form, 0xfff0);

	/*  Compilation of program to derive computed fields, if any */

  tf_cc(form, tx_more("Ccode"), 0);	/* compute	*/
  tf_cc(form, tx_more("Ccheck"), 1);	/* check	*/

	/* Move to First Field	*/

  tff_goto(form, 0);

  FIN:
  if(saved) 	strfree(saved);
  return(status);
}

/*======================================================================*/
/*			Public  Routines				*/
/*======================================================================*/
TFORM *tf_load(formname, filename, fw)
/*++++++++++
.PURPOSE Load a form from a screen definition file (default extension sdf)
.REMARKS On return, the current field is the first one.
.RETURNS The form created. This new form also becomes the current one.
--------------*/
	char	*formname;	/* IN: Form name		*/
	char	*filename;	/* IN: File Name		*/
	WINDOW	*fw;		/* IN: The window to use	*/
{

  ENTER("*tf_load");

  with_file = 1;
  tftex(formname, filename, fw);
  
  EXIT_PTR(TFORM, form);
}

/*=====================================================================*/
TFORM *tf_make(formname, tex_string, fw)
/*++++++++++
.PURPOSE Load a form from a supplied string with TeX text.
.REMARKS On return, the current field is the first one.
.RETURNS The form created. This new form also becomes the current one.
--------------*/
	char	*formname;	/* IN: Form name		*/
	char	*tex_string;	/* IN: Text			*/
	WINDOW	*fw;		/* IN: The window to use	*/
{

  ENTER("*tf_load");

  with_file = 0;
  tftex(formname, tex_string, fw);
  
  EXIT_PTR(TFORM, form);
}

