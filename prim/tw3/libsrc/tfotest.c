/* @(#)tfotest.c	19.1 (ES0-DMD) 02/25/03 14:12:45 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Program
.IDENTIFICATION tfotest.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	OD Display.
.ENVIRONMENT	TermWindows

.VERSION 1.0	08-Jul-1989: Creation

.COMMENTS	Display the .sdf file, and use the TPU editor 
		for modifications.
--------------------------------*/

#define	DEBUG		0	/* For debugging only		*/

#include <tform.h>
#include <stesodef.h>
#include <str.h>
#include <pgm.h>
/*#include <astro.h>		/* For Coordinate Transformations	*/
#include <cc.h>			/* For Compilation	*/
#include <osdev.h>			/* For Compilation	*/

#define ShowError()	DisplayError(NULL_PTR(char))
#define ClearError()	ERR_CLEAR(), ShowError();

static WINDOW 	*wd;	/* The small Dialogue Window */

static TFORM 	*the_form;

static struct devstat info;
static int	fd;		/* File Descriptor	*/

static TFIELD	*contents, *sector, *sector_size;
static char	abuffer[4096];

/*=========================================================================*/
static int underscore(str,l)	
/*+++++++++
.PURPOSE What to do in TeX parsing when Math characters (^ $ _) are encountered.
.RETURNS Number of bytes
.REMARKS Called in TeX parsing. It just dumps these specila charactesr.
------------*/
	char	*str;	/* IN: Underscore */
	int	l;	/* IN: Length	*/
{
  return(tx_out(str,l));
}

/*=========================================================================*/
static TFORM *Load()
/*+++++++++++++
.PURPOSE Ask the end--user for the sdf file.
.RETURNS The loaded Form
.REMARKS
-----------*/
{
	TFORM 	*form;
	TFIELD	*field;

  form = LoadForm("tfotest", "tfotest.sdf", NULL_WINDOW);

  ShowError();

	/* Retrieve Interesting Fields */

  if(form)
  {
	contents = FieldPointer(form, FindField(form, "contents"));
	sector   = FieldPointer(form, FindField(form, "Sector"));
	sector_size = FieldPointer(form, FindField(form, "SectorSize"));
  }

  return(form);

}

/*=========================================================================*/
static int Open(device, sectors)
/*+++++++++++++
.PURPOSE Open the Device.
.RETURNS 0
.REMARKS Called from sdf, at end of Device field. 
-----------*/
	char	*device;	/* IN: Device Concerned	*/
	int	*sectors;	/* OUT: Total Number od Sectors */
{
	
  fd = osuopen(device, 0, sector_size->value.integer);	/* Open for READ */
  if (fd <= 0)		ERROR(osmsg());
  else
  {	osuinfo(fd, &info);
  	*sectors = info.usize;
  }
  return(0);
}

/*=========================================================================*/
static int Read()
/*+++++++++++++
.PURPOSE Read the Buffer
.RETURNS 0
.REMARKS Called from sdf, at end of Sector field. 
-----------*/
{
	int	status;

  oscfill(abuffer, sizeof(abuffer), 0);
  
  status = osubseek(fd, sector->value.integer, 0);
  if (status == sector->value.integer)
  	status = osuread(fd, abuffer, sector_size->value.integer);
  if (status <= 0)	ERROR(osmsg());
  else		oscopy(contents->string, abuffer, contents->string_size);

  return(0);
}

/*=========================================================================*/
static int ReadNext(form)
/*+++++++++++++
.PURPOSE Read the Next Sector 
.RETURNS 0
.REMARKS Called from sdf, at end of Sector field. 
-----------*/
	TFORM	*form;	/* IN: Form Concerned (Unused)	*/
{
	int	status;
	
  sector->value.integer = osubtell(fd);
  status = Read();
  TouchForm(form);	/* Update what's computed */
  return(status);
}

/*======================================================================*/
/*			Main Program					*/
/*======================================================================*/
PGM(tf1test)
/*+++++++
.PURPOSE Just test the Input in a Form.
.RETURNS 
--------*/
{
	int	status, cat_pos, i;
	int	underscore();
	char	key;

  SaveParms();

  OpenLog	(NameFile(ParseFile(GetProgramFile(),_FILE_),".log"),
  		"Forms Interface"); 

  InitWindows(GetTerminalName(), (char *)0, -1);

  wd = OpenWindow("Main Dialogue", -3, -40, 3, 40, _NORMAL_, 
  	_BORDER_, 5);
  /*	_BORDER_|_BORDER2_, 0); */

  /* ActiveWindow(wd); */
  SetMathAction(underscore);		/* To Edit the Underscore */

  DisplayString(wd, "\\input{uif:tw.def}");	/* Standard Definitions	*/

	/* Install the Permanent Functions */

  cc_BindInteger("usize", info.usize);		/* Number of Sectors	*/

  cc_Function("int Read",     0, Read);		/* Read Specific Sector */
  cc_Function("int Open",     2, Open);		/* Open Device	Sector */

  	/* Load sdf File */
  
  if_not (the_form = Load())	return;	/* Terminate */

	/* Install Actions */
  
  SetFieldAction(the_form, "READNEXT", ReadNext);

	/* Navigate in Form	*/

  RaiseForm(the_form); 	/* Make it Visible */

  GetForm(the_form);	/* Loop in Form until EXIT 	*/

  tf_fclose(the_form);
  EndWindows();
  return;
}

