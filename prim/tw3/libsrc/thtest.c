/* @(#)thtest.c	19.1 (ES0-DMD) 02/25/03 14:12:48 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Program
.NAME		thtest.c
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Help 
.COMMENTS	Test of th (hierarchical help) routines
.ENVIRONMENT	TermWindows
.VERSION  2.1	(Version '2' of TermWindows)
.VERSION  2.2	02-Jul-1987: Merging with TermDisplay.
.VERSION  2.3	02-Dec-1987: Usage of WhileInput
------------------------------------------------------------*/

#define	PM_LEVEL	2

#include <twhelp.h>
#include <trace.h>
#include <macrogen.h>
#include <pgm.h>

static TWHELP *help;


/*===========================================================================*/
PGM(thtest)
/*+++++++
.PURPOSE Just test the Help until the Interrupt
.RETURNS 
--------*/
{
	BUFFER *th_file();
	WINDOW	*wd;
	int	status;
	char	ch;
	static  char file[40];

  SaveParms();
  OpenLog	("thtest.log","Testing TH (Help) facilities"); 
  LogLevel	(GetOption('t')); 
  SaveParms();

  if_not (InitWindows(GetTerminalName(), (char *)0, -1)) {
	fi_puts (2, "****Bad Terminal Initialisation (see Log File)");
	return;
  }
  
  wd = OpenWindow("Main Dialogue", -3, -40, 3, 40, _NORMAL_, 
  	_BORDER_, 0);
  /*	_BORDER_|_BORDER2_, 0); */

#if 0
  Put(wd, "Hash factor: ");
  ActiveWindow(wd);
  Gets(wd, file, sizeof(file));
  ClearWindow(wd);
  h_factor(atoi(file));
#endif
  
  ActiveWindow(wd);
#if 0
  Put(wd, "Tracing level: ");
  Gets(wd, file, sizeof(file));
  LogLevel(atoi(file));
  ClearWindow(wd);
#endif

  Put(wd,"File: ");
  while (1) {
  	status = Gets(wd, file, sizeof(file));
  	if (status < 0)	break;
  	ClearWindow(wd);
  	if(help = th_init(file, NULL_WINDOW, NULL_WINDOW, NULL_WINDOW)) 
  		break;
	SetAttr(wd, _BOLD_|_BLINK_|_REVERSE_);
  	Put(wd,"File ? ");
  	SetAttr(wd, _NORMAL_);
  }
  
  while (status >= 0) {
  	th_help(help, "", 0);
	RaiseWindow(wd);
	Put(wd, "Returned in main program...");
	status = GetKey(wd, &ch);
	ClearWindow(wd);
  }

  tx_list();
  EndWindows();
  return;
}
