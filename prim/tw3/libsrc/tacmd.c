/*===========================================================================
  Copyright (C) 1988-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++   
.TYPE           Module
.LANGUAGE       C
.IDENTIFICATION tacmd.c
.AUTHOR         Francois Ochsenbein [ESO-IPG]
.KEYWORDS       Execution of Spawn on a new Window
.ENVIRONMENT    TermWindows
.COMMENTS       
.VERSION  1.0	24-Jun-1988: Based on TermWindows version 3.0
.VERSION  1.1	21-Jul-1989: Added parameter to ta_cmd = input file
.VERSION  1.2	30-Nov-1989: Be sure to resdet correctly the Terminal
.VERSION  1.3	09-Mar-1990: Use HitReturn

 090902		last modif
------------------*/

#define DEBUG 	0 	/* debugging option	*/

#define PM_LEVEL 	LEVEL_TW-1

#include <midas_def.h>

#define  PASCAL_DEF	0
#include <stesodef.h>
#include <tw.h>

#define FINISH	goto FIN


extern int ta_ret();

extern int tv_dim(), tv_supply(), tv_where(), tv_bell();

extern int tw_fill(), tw_attr(), tw_uattr(), tw_r(), tw_goto(), tw_rule();
extern int tw_st(), tw_uflag(), tw_chars(), tw_copw(), tw_flags();
extern int tw_uw(), tw_write(), tw_il(), tw_clear(), tw_dc();
extern int tw_nl(), tw_upw(), tw_uc(), tw_mvc(), tw_copy();
extern int tw_mrule(), tw_cus(), tw_gc2(), tw_close(), tw_cur();
extern int tw_home(), tw_tra(), tw_wa(), tw_agoto(), tw_mods();

extern int pm_enter(), pm_iexit(), pm_tr2(), pm_ed_i();
extern int pm_trace();
extern int eh_put1();



/*=====================================================================*/
int ta_cmd(com, input_file)
/*+++
.PURPOSE Execute DCL Command
.RETURNS OK 
---*/
	char 	*com;		/* IN: Command to execute (NULL => prompted) */
	char 	*input_file;	/* IN: File containing the input \
						(NULL => prompted) */
{
	static WINDOW *wDCL = NULL_WINDOW;
	TWSAVE	pos;	/* Old Position */
	char	bufcmd[160];

  ENTER("ta_cmd");

  if (com)
  {	if (*com == ' ')	com++;	/* Delete initial blank */
  	TRACE(com);
  	SupplyInputs(com);
  }

  if (wDCL == NULL_WINDOW)
  	wDCL = OpenWindow("$", 0, 0, 0, 0, _NORMAL_, _SCROLL_, 20);

  SaveCursor(&pos);
  SetAttr(wDCL, _BOLD_), Put(wDCL, "$ "), SetAttr(wDCL, _NORMAL_);
  RaiseWindow(wDCL), ActiveWindow(wDCL);
  
  if (Gets(wDCL, bufcmd, sizeof(bufcmd)) < 0)	FINISH;
  NewLine(wDCL);
  ostraw(0);			/* Added 30-Nov-1989 */
  if (oshcmd(bufcmd, input_file, NULL_PTR(char), NULL_PTR(char)) < 0)
  	ERROR(osmsg());
  ostraw(1);			/* Added 30-Nov-1989 */

  HitReturn(_REVERSE_|_BLINK_, _DOWNRIGHT_);
  
  FIN:
  ScreenModified(), RemoveWindow(wDCL);
  RestoreCursor(&pos);

  EXIT(OK);
}

