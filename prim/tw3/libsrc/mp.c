/* @(#)mp.c	19.1 (ES0-DMD) 02/25/03 14:12:40 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Module
.IDENTIFICATION	mp.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Main Parameters
.ENVIRONMENT 	
.VERSION   1.0
.VERSION   1.1	13-Oct-1988: Cosmetic Modifications
.COMMENTS
	These routines allow the management of parameters to a main program.
\begin{TeX}
	Arguments to a main program start with a character having the
	following meaning:
\begin{itemize}
\item	{\tt <}\quad redirect input file
\item	{\tt >}\quad redirect output file
\item	{\tt \^{ }}\quad terminal name
\item	{\tt -}\quad option string 
\end{itemize}

An option string is made of an alphabetic (always in lower-case)
character, possibly followed by a number or a string, \eg
\begin{quote}
{\tt -t}	\\
{\tt -l80}
{\tt -fin}
\end{quote}
A {\em normal} argument starts with an alphanumeric character.
\end{TeX}
.VERSION 1.0 	06-Nov-1986: Creation
.VERSION 1.1 	05-Mar-1988: Adapted for MIDAS

-----------------------*/

#define		DEBUG		0	/* For debugging purposes	*/

#define	PM_LEVEL	LEVEL_STR

#define PASCAL_DEF	0	/* Don't include pascalisation ... */
#include <stesodef.h>
#include <atype.h>

MID_STATIC	int argc = 0;
MID_STATIC 	char **argv = (char **)0;
MID_STATIC 	char **envp = (char **)0;
MID_STATIC	unsigned int argi = 0;	/* Current argument index	*/

/*==========================================================================*/
int mp_save(np,p1,p2)    
/*+++
.PURPOSE Save the arguments of a main program.
.RETURNS 0
.REMARKS To be called at the beginning of a program
---*/
	int np;		/* IN: Argument count		*/
	char **p1;	/* IN: List of parameters		*/
	char **p2;	/* IN: List of environment parameters	*/
{
  ENTER("mp_save");
  TRACE_ED_I("Number of main program arguments=",np);

  argc = np;
  argv = p1;
  envp = p2;
  argi = 0;

  EXIT(0) ;
}

/*==========================================================================*/
char *mp_get(n)
/*+++
.PURPOSE Get argument number n
.RETURNS Address of n.th argument, or NULL
.REMARKS 
---*/
	unsigned int	n;	/* IN: Number of wished argument	*/
{
	register char *p;
	
  ENTER("*mp_get");
  TRACE_ED_I("Main argument #",(int)n);

  if (n < argc)	 { p = *(argv+n); TRACE(p); }
   else		   p = NULL_PTR(char);

  EXITp(p);
}

/*==========================================================================*/
char *mp_next()
/*+++
.PURPOSE Get next parameter
.RETURNS Address of next NORMAL (starting with alphanumeric) parameter, or NULL
.REMARKS 
---*/
{
	char *mp_get();

  ENTER("*mp_next");

  while (++argi < argc)
  	if (isalnum(**(argv+argi)))	break;

  EXITp(mp_get(argi));
}

/*==========================================================================*/
char	*mp_flagged(ch)
/*+++
.PURPOSE Retrieve a flagged (starting with a specified char) parameter.
.RETURNS Address of retrieved parameter WITHOUT the flag, or NULL
.REMARKS 
---*/
	char	ch;	/* IN: Starting character	*/
{
	register char **ps;
	register char *p;
	register int i;

  ENTER("*mp_flagged");

  for (i=1, ps = argv; i<argc; i++)
  { 	p = *++ps;
	if (*p == ch)	break;
  }

  if (i < argc)	
  {	TRACE(p);
  	p++;
  }
  else	p = NULL_PTR(char);

  EXITp(p);
}

/*==========================================================================*/
int	mp_option(ch)
/*+++
.PURPOSE Retrieve an option in the parameter
.RETURNS 0 (non-existing option), or value following the option (1 is default)
.REMARKS Example is:
\begin{TeX}
$$\begin{tabular}{llr} \hline
Parameter  & Argument	& Result \\ 
\hline
{\tt	-x,-l-80} & {\tt 'l'} &	-80\\
{\tt	-x,-l-80} & {\tt 'x'} &	1\\
{\tt	-x,-l-80} & {\tt 'g'} &	0\\
\hline
\end{tabular}$$
\end{TeX}
---*/
	char	ch;	/* IN: Starting option character	*/
{
	register char **ps;
	register char *p;
	register int i;
	MID_STATIC 	char  sgn = 0;
	MID_STATIC 	char tracing[] = "Look for `C' option";

  ENTER("mp_option");
  tracing[10] = ch;
  TRACE(tracing);

  sgn = 0;

	/* Method: scan parameters beginning with '-', and
	 * then look for first occurence of specified letter	*/

  for (ps = argv, i = argc; --i>0; )
  {	p = *++ps;
	if (*p != '-')		continue;
	while (*++p)
		if (*p == ch)	break;
	if (*p == ch)	break;
  }

  if (i <= 0)		EXIT(0);
  if (*p != ch)		EXIT(0);

	/* Now, look if characters following the required option
	 * contain a number ...					*/
  i = 0;			/* Returned option	*/
  switch(*++p)
  { case '-' : sgn = 1;		/* Signed number	*/
    case '=' :
    case '+' : p++; break;
    default  : if (!isdigit(*p)) 	i = 1;
  }

	/* Now, extract the number following the option	*/
  while(isdigit(*p))
  {	i *= 10;
	i += *(p++) - '0';
  }

  if (sgn)	
  {	if (i)	i = -i;
	else	i = 1;		/* Followed with a - ...	*/
  }

  EXIT(i);
}

/*==========================================================================*/
char *mp_poption(ch)
/*+++
.PURPOSE Retrieve an option string in the main parameters
.RETURNS Address of string following the option, or NULL
.REMARKS Example: the parameter -oXYZ will return XYZ.
---*/
	char	ch;	/* IN: Starting option character	*/
{
	register char **ps;
	register char *p;
	register int i;
	MID_STATIC 	char tracing[] = "Look for `C' option";

  ENTER("*mp_poption");
  tracing[10] = ch;
  TRACE(tracing);

	/* Method: scan parameters beginning with '-', and
	 * then look for first occurence of specified letter	*/

  for (ps = argv, i = argc; --i>0; )
  {	p = *++ps;
	if (*p != '-')		continue;
	if (*(p+1) == ch)	goto FOUND;
  }
  p = NULL_PTR(char);
  goto FIN;
  
  FOUND:
  TRACE(p);
  p += 2;

  FIN:
  EXITp(p);
}

