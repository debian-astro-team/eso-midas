/* @(#)tf1test.c	19.1 (ES0-DMD) 02/25/03 14:12:43 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Program
.IDENTIFICATION tftest1.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Screen Definition Files for Catalogues
.ENVIRONMENT	TermWindows

.VERSION 1.0	08-Jul-1989: Creation

.COMMENTS	Display the .sdf file, and use the TPU editor 
		for modifications.
--------------------------------*/

#define	DEBUG		0	/* For debugging only		*/

#include <tform.h>
#include <stesodef.h>
#include <str.h>
#include <pgm.h>
/*#include <astro.h>		/* For Coordinate Transformations	*/
/*#include <cc.h>			/* For Compilation	*/

#define ShowError()	DisplayError(NULL_PTR(char))
#define ClearError()	ERR_CLEAR(), ShowError();

static WINDOW *wd;	/* The small Dialogue Window */

static int menu_pos, junk;
static int choice;
static char state_log = 0;
static TFORM *the_form;
static  char file[40];
static	char	*sdf_file;

static double	anything;

/*=========================================================================*/
static int underscore(str,l)	
/*+++++++++
.PURPOSE What to do in TeX parsing when Math characters (^ $ _) are encountered.
.RETURNS Number of bytes
.REMARKS Called in TeX parsing. It just dumps these specila charactesr.
------------*/
	char	*str;	/* IN: Underscore */
	int	l;	/* IN: Length	*/
{
  return(tx_out(str,l));
}

/*=========================================================================*/
static TFORM *Load()
/*+++++++++++++
.PURPOSE Ask the end--user for the sdf file.
.RETURNS The loaded Form
.REMARKS
-----------*/
{
	TFORM *form;
	
  ClearWindow(wd);

  for (form = NULL_PTR(TFORM); !form ; )
  {
  	SetAttr(wd, _REVERSE_);
  	Put(wd,"SDF file:");
  	SetAttr(wd, _NORMAL_);
  	Put(wd, " ");
  	RaiseWindow(wd), ActiveWindow(wd);
  	if (Gets(wd, file, sizeof(file)) < 0)	break;	/* ^Z was hit... */
  	RemoveWindow(wd);
  	ClearWindow(wd);
  	sdf_file = NameFile(file, ".sdf");
  	form = LoadForm(file, sdf_file, NULL_WINDOW);
  	ShowError();
  }

  ERR_CLEAR(); ShowError();
  RemoveWindow(wd);
  return(form);

}

/*======================================================================*/
/*			Main Program					*/
/*======================================================================*/
PGM(tf1test)
/*+++++++
.PURPOSE Just test the Input in a Form.
.RETURNS 
--------*/
{
	int	status, cat_pos, i;
	int	underscore();
	char	key;

  OpenLog	("tf1test.log","Testing Forms Interface"); 

  SaveParms();

  InitWindows(GetTerminalName(), (char *)0, -1);

  wd = OpenWindow("Main Dialogue", -3, -40, 3, 40, _NORMAL_, 
  	_BORDER_, 5);
  /*	_BORDER_|_BORDER2_, 0); */

  ActiveWindow(wd);
  SetMathAction(underscore);		/* To Edit the Underscore */

  DisplayString(wd, "\\input{uif:tw.def}");	/* Standard Definitions	*/

  	/* Loop on sdf File Names */
  
  while(1)
  {
	if_not (the_form = Load())	return;	/* Terminate */

	RaiseForm(the_form); 	/* Make it Visible */

	GetForm(the_form);	/* Loop in Form until EXIT 	*/
  }

  tf_fclose(the_form);
  EndWindows();
  return;
}

