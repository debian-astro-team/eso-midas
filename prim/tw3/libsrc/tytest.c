/* @(#)tytest.c	19.1 (ES0-DMD) 02/25/03 14:13:00 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Program
.NAME		tytest.c
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Terminal Handling
.COMMENTS	Test of tw (window) and tx (TeX-like) routines
.ENVIRONMENT	TermWindows
.VERSION  1.0	28-Nov-1990: Creation
------------------------------------------------------------*/

#define	PM_LEVEL	2

#include <stesodef.h>
#include <tw.h>
#include <str.h>

static WINDOW *w1, *w2, *w3, *wR, *wC;
static int lines;
static short screen_size[2];

/*===========================================================================*/
main()
/*+++++++
.PURPOSE Extensive Test Program for Document Facilities
	(including TeX)
.RETURNS 
--------*/
{
	char *p;
	int doc;
	int i;
	char reply[80];

	OpenLog	("tytest.log","Testing TY (windows) facilities"); 
	LogLevel	(-20); 

  printf("Device ? "); gets(reply);
  p = (reply[0] ? reply : NULL_PTR(char));
  printf ("Choose Large or Small Window: "); gets(reply);

  if_not (InitWindows(p, NULL_PTR(char), -1))
  {	printf("****%s\n", ERR_LOC());
	return;
  }

  ScreenSize(screen_size);

  switch(reply[0]) {
     case 's': case 'S':
	w1 = OpenWindow("Small", 0, -45, screen_size[0]/2,45, _REVERSE_, _BORDER2_ , 0);
	break;
     default:
  	w1 = OpenWindow("Large",0,0,screen_size[0]-1,0, _NORMAL_, _SCROLL_, 10);
  }
  wR = OpenWindow("==More==", -1, 0, 1, 0, _NORMAL_, 0, 0);
  SetAttr(wR, _REVERSE_);
  Put(wR, "===More===");

  ClearWindow(w1);
  ActiveWindow(w1);

  doc = OpenDocFile(w1, "ty.dat");

  while (More(doc) != EOF)	;

  CloseDoc(doc);
  EndWindows();
  CloseLog();
  ospexit(0);
}

/*==========================================================================*/

int More(doc)
/*++++++++++++
.PURPOSE Execute the "more-like"
.RETURNS EOF to Stop
--------------*/
	int doc;	/* IN: Document to Display */
{
	char	mychar;
	int	status, old_cc;


  RaiseWindow(wR);
  old_cc = SetControls(0);
  status = GetChar(&mychar);
  SetControls(old_cc);
  if (status < 0)	return(EOF);
  if (status != _STANDARD_)	mychar = 0;
  return(MoreDoc(doc, mychar));
}
