$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.PRIM.TW3.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:56 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libtw3 termcap.obj,tvgets.obj,tvput.obj,tvclear.obj,tvcursor.obj,tvhelp.obj,tvindel.obj,tvutil.obj,tvinit.obj,tvget.obj,tvout.obj
$ LIB/REPLACE libtw3 twclear.obj,twcursor.obj,twhelp.obj,twindel.obj,twutil.obj,twinit.obj,twget.obj,twout.obj
$ LIB/REPLACE libtw3 txjustify.obj,txdisplay.obj,txprompt.obj,tydoc.obj,thelp.obj,tmenu.obj,tapplic.obj,tacmd.obj,taerror.obj
$ LIB/REPLACE libtw3 tfcc.obj,tfget.obj,tfield.obj,tfile.obj,tform.obj,tfpic.obj,tfshow.obj,tftex.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
