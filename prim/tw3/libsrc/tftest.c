/* @(#)tftest.c	19.1 (ES0-DMD) 02/25/03 14:12:46 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Program
.IDENTIFICATION tftest.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Screen Definition Files for Catalogues
.ENVIRONMENT	TermWindows

.VERSION 1.0	08-Jul-1988: Creation
.VERSION 1.1	13-Dec-1988: Added Ccode compilation

.COMMENTS	Display the .sdf file, and use the TPU editor 
		for modifications.
--------------------------------*/

#define	DEBUG		0	/* For debugging only		*/

#include <tform.h>
#include <stesodef.h>
#include <str.h>
#include <pgm.h>
/*#include <astro.h>		/* For Coordinate Transformations	*/
/*#include <cc.h>			/* For Compilation	*/

#define ShowError()	DisplayError(NULL_PTR(char))

static WINDOW *wM, *wd;

static char *menu_list[] = {
	"0 Load File",
	"1 Show Fields",
	"2 Show Pictures",
	"3 Show DataTypes",
	"4 Show Options",
	"G Get Fields",
	"N Get Notes",
	"O Get Options",
	"W Write to TEST.LIS",
	"R Remove Menu",
	"T Tracing Off"
 };
static int menu_pos, junk;
static int choice;
static char state_log = 0;
static TFORM *the_form;
static  char file[40];
static	char	*sdf_file;

static double	anything;

/*=========================================================================*/
TFORM *Load()
{
	TFORM *form;
	
  ClearWindow(wd);
  SetAttr(wd, _REVERSE_);
  Put(wd,"SDF file:");
  SetAttr(wd, _NORMAL_);
  Put(wd, " ");

  for (form = NULL_PTR(TFORM); !form ; )
  {
  	RaiseWindow(wd);
  	if (Gets(wd, file, sizeof(file)) < 0)	break;
  	RemoveWindow(wd);
  	ClearWindow(wd);
  	sdf_file = NameFile(file, ".sdf");
  	form = LoadForm(file, sdf_file, NULL_WINDOW);
  	ShowError();
  }

  ERR_CLEAR(); ShowError();
  return(form);

}

/*=========================================================================*/
int GetMask()
{
	int	mask;
	char	buffer[24];

  ClearWindow(wd);  
  SetAttr(wd, _REVERSE_);
  Put(wd,"Mask (dec):");
  SetAttr(wd, _NORMAL_);
  Put(wd, " ");

  for (mask=0; !mask ; )
  {
  	RaiseWindow(wd), ActiveWindow(wd);
  	if (Gets(wd, buffer, sizeof(buffer)) < 0)	break;
  	mask = atoi(buffer);
  	RemoveWindow(wd);
  	ClearWindow(wd);
  }

  ERR_CLEAR(); ShowError();
  return(mask);

}

/*======================================================================*/
/*			Main Program					*/
/*======================================================================*/
PGM(tftest)
/*+++++++
.PURPOSE Just test the Form Utilities
.RETURNS 
--------*/
{
	int	status, cat_pos, i;
	int	underscore();
	char	key;

  OpenLog	("tftest.log","Testing TFORMS"); 

  SaveParms();

  InitWindows(GetTerminalName(), (char *)0, -1);

  wd = OpenWindow("Main Dialogue", -3, -40, 3, 40, _NORMAL_, 
  	_BORDER_, 5);
  /*	_BORDER_|_BORDER2_, 0); */

  ActiveWindow(wd);
  SetMathAction(underscore);		/* To Edit the Underscore */

  DisplayString(wd, "\\input{uif:tw.def}");	/* Standard Definitions	*/

	/* Insert the Text in Menu Window */

  wM = OpenMenuWindow("Menu", 3, 0, menu_list, ITEMS(menu_list), _REVERSE_);

  menu_pos = GetPosition(wM);
  
  	/* Loop on Menu Display, Get a Choice, Execute Function */
  
  while(1)
  {	choice = GetMenuChoice(wM, _BOLD_);
	if (choice == EOF)	break;
	switch(*menu_list[choice])
	{
	  case 'R':
		RemoveWindow(wM);
		ERROR("Hit Any Key to Show the Menu");
		ShowError();
		GetChar(&key);
		ERR_CLEAR(), ShowError();
		continue;
	  case 'T':
		state_log ^= 1;
		CursorTo(wM, choice, 11);
		if (state_log)	Put(wM, "n "), 
				TraceOn(-LEVEL_TF);
		else		Put(wM, "ff"), 
				TraceOff(-LEVEL_TF);
		continue;
	  case '0':
	  	if_not (the_form = Load())	return;	/* Terminate */
	  case '1':
		ShowForm(the_form);	
		RaiseForm(the_form); 			break;
	  case '2':
		ShowPictures(the_form);	
		RaiseForm(the_form); 			break;
	  case '3':
		ShowDatatypes(the_form);	
		RaiseForm(the_form); 			break;
	  case '4':
		ShowOptions(the_form, GetMask(), 'X');
		RaiseForm(the_form); 			break;
	  case 'G':
		RaiseForm(the_form);
		GetForm(the_form);			break;
	  case 'O':
		RaiseForm(the_form);
		GetFormOption(the_form, GetMask(), 'X');break;
	  case 'N':
		RaiseForm(the_form);
		GetFormNotes(the_form, (TF_CHECK)0);break;
	  case 'W':
		if (the_form->afileno <= 0)	
			OpenFormFile(the_form, "test.lis");
		if (the_form->acount == 0)	WriteFormTitle(the_form);
		WriteFormFile(the_form);	break;
	}
  }

  tf_fclose(the_form);
  EndWindows();
  return;
}

/*=========================================================================*/
RemoveMenu()
{	
  	
  ChangeAttr(wM, wM->dim[1], _NORMAL_);
  RemoveWindow(wM);

  return(0);
}

underscore(str,l)	
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE	Compile source code if any.
.RETURNS	OK / NOK.
------------------------------------------------------------------------*/
	char	*str;	/* IN: Underscore */
	int	l;	/* IN: Length	*/
{
  return(tx_out(str,l));
}

