/*===========================================================================
  Copyright (C) 1988-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++

.TYPE		Module
.IDENTIFICATION tmenu.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Window Management
.ENVIRONMENT	TermWindows
.COMMENTS	This module contains simple menu application of TermWindows.


.VERSION 3.0	20-Jun-1988: Creation

 090901         last modif
--------------------------------------*/

#define		DEBUG		0	/* For debugging only		*/

#define PM_LEVEL 	LEVEL_TW-1

#include <stesodef.h>
#include <atype.h>
#include <tw.h>
#include <twset.h>


extern int tv_bell();

extern int tw_fill(), tw_attr(), tw_uattr(), tw_r(), tw_goto(), tw_rule();
extern int tw_st(), tw_uflag(), tw_chars(), tw_copw(), tw_flags();
extern int tw_uw(), tw_write(), tw_il(), tw_clear(), tw_dc();
extern int tw_nl(), tw_upw(), tw_uc(), tw_mvc(), tw_copy();
extern int tw_mrule(), tw_cus(), tw_gc2(), tw_close(), tw_cur();
extern int tw_mattr(), tw_home(), tw_tra(), tw_wa();
extern int tw_stopin(), tw_getc();

extern int pm_enter(), pm_iexit(), pm_tr2(), pm_ed_i();



WINDOW *ta_dummy();

static short int J, old_line;
static WINDOW	*ws;

/*==========================================================================*/
static int moda(line, attr)
/*+++
.PURPOSE MOdify the attribute of a line
.RETURNS New line number
.REMARKS 
---*/
	int	line;	/* IN: Line to modify */
	int	attr;	/* IN: How to emphasize selected items	*/
{
  if (old_line >= 0)
  	ChangeAttr(ws, J, _NORMAL_);
  CursorTo(ws, line, 0);
  ChangeAttr(ws, J, attr);
  CursorTo(ws, line, 0);
  TouchWindow(ws);
  old_line = line;

  return(line);
}

/*==========================================================================
 *			tm_open
 *==========================================================================*/
WINDOW *tm_open(title, hl, hc, list, n, attr)
/*+++
.PURPOSE Create a menu window (in reverse)
.RETURNS The allocated window
.REMARKS 
---*/
	char	*title;	/* IN: The title of the Window	*/
	int	hl;	/* IN: Home line (<0 from bottom)	*/
	int	hc;	/* IN: Home column (<0 from right side)	*/
	char	**list;	/* IN: Menu list, first char = symbol	*/
	int	n;	/* IN: Number of items in menu	*/
	int	attr;	/* IN: Window attribute		*/
{
	int	m, l, i;
	WINDOW	*w;

  ENTER("*tm_open");

	/* Compute size of window */
	
  for (m=0, i=0; i < n; i++)
  {	l = strlen(list[i]);
  	if (l > m)	m = l;
  }
  
  w = OpenWindow(title, hl, hc, n+4, m+4, attr,
  	_TITLE_|_BORDER_|_BORDER2_, 0);

  for (i = 0; i < n; i++)
  {	CursorTo(w, i, 0);
  	Put(w, list[i]);
  }
  CursorHome(w);
  
  SetStopping(w, _ARROW_, "*");		/* Return if Arrows */

  EXIT_PTR(WINDOW , w);
}

/*==========================================================================
 *			tm_select
 *==========================================================================*/
int tm_select(w, attr)
/*+++
.PURPOSE Select an item from a menu.
.RETURNS Item number in list (starting with 0) / -1 for ^Z / _INTERRUPT_
.REMARKS No carriage return required. The attribute of the located topic is
	changed to attr.
---*/
	WINDOW	*w;	/* IN: Window concerned */
	int	attr;	/* IN: How to empghasize selected items	*/
{
	int 	i, n, status;
	char	answer;
	ACHAR	*pa;
	WINDOW	*old_helpw;
	MID_STATIC 	WINDOW *helpw = NULL_WINDOW;
	MID_STATIC  char help_text[] = "\
Choose your topic by\
moving  the  up/down\
arrows, and <Return>\
 Alternatively, type\
the key of column 1.";

  ENTER("tm_select");


  if_not(helpw)		/* Create Help Window */
  {	helpw = OpenWindow("Menu Selection", 0, -24, -4-(sizeof(help_text)/20), 
  		24, _REVERSE_, _TITLE_|_BORDER_|_BORDER2_|_DISPLAY_, 0);
	for (i=0; i<sizeof(help_text); i += 20)
		Write(helpw, &help_text[i], 20); 
	CursorHome(w);
  }

  old_helpw = w->help;

  if_not(w->help)	/* Define a Help Window	*/
  	AttachHelpWindow(w, helpw);

  ws = w, n = ws->dim[0];	/* Number of items */
  J = ws->dim[1], i = (ws->pos/J) % n;

		/* Reset Attribute of window to NORMAL */
		
  CursorHome(ws), ChangeAttr(ws, ws->marker[1], _NORMAL_);
  CursorTo(ws, i, 0), ChangeAttr(ws, J, attr), CursorTo(ws, i, 0);
  old_line = i;

  RaiseWindow(ws);
  
  while (1)
  {	status = GetKey(ws, &answer);
  	if (status < 0)	{i = status; goto FOUND; }
  	switch(status)
	{ default:	
  		if (answer == '\r')	goto FOUND;
  		answer = toupper(answer);
		for (i=0; i<n; i++)
		{	pa = Aij(ws, i, 0);
			if ( (*pa & 0xff) == answer)
				goto FOUND;
		}
		if (answer == ' ')	goto NEXT;	/* Blank = Forward */
		Bell();
		continue;
	  case _ARROW_:
		switch(answer)
		{ case _UP_: case _LEFT_:
			i = ws->pos / ws->dim[1];
			if (--i < 0)	i += n;
			break;
		  case _DOWN_ : case _RIGHT_: 	NEXT:
			i = ws->pos / ws->dim[1];
			if (++i >= n)	i -= n;
			break;
		  case _HOME_:
		  	i = 0;
		  	break;
		}
		moda(i, attr);
	}
  }

  FOUND:
  if (i >= 0)	moda(i, attr);
  AttachHelpWindow(ws, old_helpw);		/* Reset Help Window */

  EXIT(i);
}

