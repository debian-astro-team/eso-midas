/*===========================================================================
  Copyright (C) 1987-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.TYPE		Module
.IDENTIFICATION twhelp.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Window Management
.ENVIRONMENT	TermWindows
.COMMENTS	This module contains routines to show the Help Window.
\begin{TeX}
	The Help Window displays the Available Control Characters.
\end{TeX}

.VERSION  2.0	25-Jun-1987	Version '2'

.VERSION 3.0	20-Mar-1988: Version '3'
.VERSION 3.1	22-Feb-1989: Added OpenHelpWindow
.VERSION 3.2	13-Jun-1989: Added AttachHelpWindow (tw_hset).
			Modified tw_hop to allow Normal Characters.
.VERSION 3.3	29-Nov-1990: Check Screen was initialized...
 090901         last modif
----------------------------------------------------------------------------*/

#define		DEBUG		0	/* For debugging only		*/

#define PM_LEVEL 	LEVEL_TW

#define  TW_import	0	/* Do not use macro window definitions	*/
#include <twset.h>


extern int tw_goto(), tw_line(), tw_home(), tw_attr(), tw_wf();
extern int tw_cline(), tw_close(), tw_r(), tw_agoto();;

extern int pm_enter(), pm_iexit();



MID_EXTERN	WINDOW *Screen;
MID_EXTERN	TERM   *terms;

#if 	DEBUG
#define ed_level	3
#define ENTER_DEBUG(x)	ENTER(x)
#define EXIT_DEBUG(x)	EXIT(x)
#define DISPLAY(opt)	tw_ed(w,opt)
#else
#define DISPLAY(opt)
#define ENTER_DEBUG(x)	
#define EXIT_DEBUG(x)	return(x)
#endif

MID_STATIC	WINDOW	*Helps = NULL_WINDOW;

#define	hcols		28

MID_RSTATIC char *Explanations[] = {
	NULL_PTR(char),			/* TW_cc_NONE	0	*/
	"Interrupt",			/* TW_cc_INT	1	*/
	"Emergency Stop", 		/* TW_cc_QUIT	2	*/
	"Exit (EOF)", 			/* TW_cc_EOF	3	*/
	"Show/Remove Help",		/* TW_cc_HELP	4	*/
	"Refresh Screen", 		/* TW_cc_REFR	5	*/
	"Switch Insert/Replace", 	/* TW_cc_MODE	6	*/
	"Escape (raw)",			/* TW_cc_RAW	7	*/
	"Return",			/* TW_cc_RET	8	*/
	"Up (Previous command)", 	/* TW_cc_UP	(9+_UP_)*/
	"Down (Next command)", 		/* TW_cc_DOWN	(9+_DOWN_)*/
	"Move Left    <==", 		/* TW_cc_LEFT	(9+_LEFT_)*/
	"Move Right    ==>", 		/* TW_cc_RIGHT	(9+_RIGHT_)*/
	"Move Home",			/* TW_cc_HOME	(9+_HOME_)*/
	"Move to End_of_Line",		/* TW_cc_EOL	14	*/
	"Move to Next Word",		/* TW_cc_NW	15	*/
	"Erase Previous Char",		/* TW_cc_DELC	16	*/
	"Erase Previous Word", 		/* TW_cc_DELW	17	*/
	"Erase to End_of_line", 	/* TW_cc_DELE	18	*/
	"Erase Whole"			/* TW_cc_DELL	19	*/
  };

  static int index_in_list;

	/* MONITOR(TWHELP); */


/*==========================================================================*/
static int locc(s, cc)
/*+++
.PURPOSE Just check if TW_cc character is present
.RETURNS OK / NOK
.REMARKS 
---*/
	char	*s;	/* IN: List of control chars	*/
	int	cc;	/* IN: TW_cc char to check	*/
{
	char	*p;
	
  for (p=s; *p; p++)	if (*p == cc)	break;
  index_in_list = (p-s);
  
  return(*p);
}

/*==========================================================================
 *			Create
 *==========================================================================*/
WINDOW	*tw_hop	(name, home_line, home_col, cc, texts)
/*+++
.PURPOSE Open and Create a Help Window
.RETURNS The created Help Window / NULL when fail
.REMARKS The list of characters which should appear are in cc,
	as a set of TW_cc_... control characters, or Normal Characters.
	the listed is terminated by TW_cc_NONE. The explanation text
	should be in the texts array, where NULL pointers stand for
	`standard text'.
---*/
	char 	*name;		/* IN: Title of the Window */
	int 	home_line;	/* IN: Home Position	*/
	int 	home_col;	/* IN: Home Position	*/
	char	*cc;		/* IN: List of TW_cc_ characters to display. */
	char	**texts;	/* IN: Explanation for Controls	*/
{
	WINDOW 	*w;
	char 	*p;
	int 	i, j, k, hlines;
	MID_STATIC 	char cta[5] = " ^  ";
	MID_STATIC 	char a5[5]  = "    ";
	

  ENTER("*tw_hop");

  if(!Screen)		EXIT_PTR(WINDOW, 0);

	/* Compute Required Lines/cols	*/

  for (p=cc, hlines = 4; *p; p++)	if_not(iscntrl(*p))	hlines++;

  for (i = sizeof(terms->tc); --i >=0; )
  {	j = terms->tc[i]; 
  	if (j == TW_cc_RET)		continue;
  	if ( j && locc(cc,j))		hlines++;
  }
  if (hlines > Screen->Ni)	hlines = Screen->Ni;

	/* Create the Help Window, centered on lines, right adjusted */

  if (home_col < 0)	home_col = Screen->dim[1]-hcols;
  w = tw_open(NULL_WINDOW, name,
  	home_line, home_col, 		/* Home positions */
  	hlines, hcols, 		/* Negative dim in lines => centered */
	_REVERSE_, _GRAPHICS_|_DISPLAY_|_TITLE_, 0);

  terms->standout &= ~ChangedControl;


	/* Edit the non-control Lines */

  if(w)
  for (p=cc, k = 0; *p; p++)
  {	if(iscntrl(*p))	continue;
	tw_goto(w, k++, 0);
  	a5[2] = *p;
	tw_line(w, a5, 4);
	index_in_list = (p - cc);
	if (texts[index_in_list])
			tw_line(w, ARG_STRING(texts[index_in_list]));
	else		tw_line(w, ARG_TEXT("???"));
  }

	/* Edit the lines   */

  if(w)
  for (i = 0; i < sizeof(terms->tc); i++)
  {	j = terms->tc[i];
  	if_not(j)		continue;
  	if_not(locc(cc,j))	continue;
    	if (j == TW_cc_RET)	continue;
	tw_goto(w, k++, 0);
	if (i >= 32)	p = "Del ";
	else		/* Issue Control Character */
		cta[2] = '@' + i, p = cta;
	tw_line(w, p, 4);
	if (texts[index_in_list])
			tw_line(w, ARG_STRING(texts[index_in_list]));
	else		tw_line(w, ARG_STRING(Explanations[j]));
  }

  EXIT_PTR(WINDOW, w);
}

/*==========================================================================
 *			Create
 *==========================================================================*/
static int Create()
/*+++
.PURPOSE Open and Create a Help Window
.RETURNS The created Help Window
.REMARKS 
---*/
{
	WINDOW 	*w;
	MID_RSTATIC char all_cc[] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,
		15,16,17,18,19,20,0};
	MID_RSTATIC char *all_tt[sizeof(all_cc)] = {(char *)0, (char *)0};

  ENTER_DEBUG("Create");

  if (Helps)		EXIT_DEBUG(OK);

	/* Compute Required Lines/cols	*/

  Helps = tw_hop("<Helps>", 0, Screen->dim[1]-hcols, 	/* Home positions */
	all_cc, all_tt);
  if_not (Helps)		EXIT_DEBUG(NOK);
  	
	/* Change the Title */
	
  w = tw_getw("`<Helps>");
  tw_home(w);
  tw_attr(w,_BOLD_|_REVERSE_);
  tw_wf(w, ' ', 999);	/* wf function anyway tests the length */
  tw_cline(w, ARG_TEXT("On-line Editing Facilities"));

  EXIT_DEBUG(OK);
}

/*==========================================================================
 *			tw_helps
 *==========================================================================*/
int tw_helps(w, k)
/*+++
.PURPOSE Switch Helps window (specifying the On-Line Editing
	Facilities) if k=1, remove it if k=0
.RETURNS OK
.REMARKS If other control characters are chosen, no need to modify anything here.
---*/
	WINDOW *w;	/* IN: Main Window (if no Help associated, 
					use standards)	*/
	int	k;	/* IN: Option	*/
{
	int i, old_pos;
	WINDOW *help_window;

  ENTER("tw_helps");

  if (!Screen)		EXIT(OK);
  
  if_not(help_window = w->help)	help_window = Helps;

	/* Check if Controls were not changed	*/
  if ( (help_window == Helps) && (terms->standout & ChangedControl))
	if (Helps)	tw_close(Helps, 1), 
			Helps = NULL_WINDOW, help_window = Helps;
  if_not(help_window)
  {	if (k == 0)	EXIT(OK);
  	if(Create())	help_window = Helps;
	else		EXIT(NOK);	/* Creation Failed ... */
  }

  old_pos = Screen->pos;

  i = (help_window->flags & Present ? 4 /* Remove */ : 1 /* Refresh */);
 
  if ( (k) || (i == 4) )	tw_rw(help_window, i, NULL_WINDOW);  

  tw_agoto(Screen, old_pos);

  EXIT(OK);
}

/*==========================================================================
 *			tw_hset
 *==========================================================================*/
WINDOW *tw_hset(w, w_help)
/*+++
.PURPOSE Attach the provided Help window
.RETURNS The prreviously attached Help Window
.REMARKS 
---*/
	WINDOW *w;	/* IN: Main Window */
	WINDOW *w_help;	/* IN: Help Window to Attach */
{
	WINDOW *old_help;

  ENTER("tw_hset");
  
  old_help = w->help;
  w->help  = w_help;

  EXIT_PTR(WINDOW, old_help);
}

