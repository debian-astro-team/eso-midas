/*===========================================================================
  Copyright (C) 1988-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Module
.IDENTIFICATION txprompt.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Window Management
.ENVIRONMENT	TermWindows
.COMMENTS	This module deals with TeX display on adjusted windows.
.VERSION 1.0	10-Nov-1988: Creation
.VERSION 1.1	09-Mar-1990: Added tx_aopen 

 090901         last modif
------------------------------------------------------*/

#define	DEBUG		0	/* For debugging only		*/

#define PM_LEVEL 	LEVEL_TW-1

#define  PASCAL_DEF	0
#include <stesodef.h>
#include <tw.h>
#include <atype.h>
#include <twset.h>


extern int tw_fill(), tw_attr(), tw_uattr(), tw_r(), tw_goto(), tw_rule();
extern int tw_st(), tw_uflag(), tw_chars(), tw_copw(), tw_flags();
extern int tw_uw(), tw_write(), tw_il(), tw_clear(), tw_dc();
extern int tw_nl(), tw_upw(), tw_uc(), tw_mvc(), tw_copy();
extern int tw_mrule(), tw_cus(), tw_gc2(), tw_close(), tw_cur();

extern int pm_enter(), pm_iexit(), pm_tr2(), pm_ed_i();

extern int eh_ed_as(), eh_put1(), eh_put();

extern int tex_exec(), tex_unit(), tex_getparm(), tex_mexec();
extern int tex_input(), tex_load(), tex_list();

extern int tx_jr(), tx_jc(), tx_justify();
extern int tx_display(), tx_option();




static WINDOW	*full_window = NULL_WINDOW;

/*==========================================================================
 *			tx_open
 *==========================================================================*/
WINDOW *tx_aopen(name, text, len, attr, border, pos_opt)
/*+++
.PURPOSE Display a TeX-like text a text onto an adjustable window.
.RETURNS Window / NULL
.REMARKS The macro \frametext{text} is automatically included.
---*/
	char	*name;		/* IN: The title of the window		*/
	char	*text;		/* IN: Text to display	*/
	int	len;		/* IN: Length of text	*/
	int	attr;		/* IN: Window Attribute	*/
	int	border;		/* IN: Window border options		*/
	int	pos_opt;  /* IN: Window positioning option _UPLEFT_, etc */
{
	WINDOW	*w;
	ACHAR	*p;
	int	saved_opt, i, j;
	int	lines, cols;
	static  char definitions[] = "\\def\\frametext#1{\\begin{table}\
{| c |}{\\len{#1}}\\Rule #1\\\\ \\Rule\\end{table}}";

  ENTER("*tx_aopen");

	/* Create full-size Window to display text	*/

  if (full_window == NULL_WINDOW)
  {	full_window = OpenWindow("(tx_prp)",0,0,0,0,_NORMAL_,_DISPLAY_,0);
  	DisplayText(full_window, definitions, sizeof(definitions)-1);
  }
	/* Redefine init attribute	*/

  full_window->attr_init &= 0xff;	/* Normal attribute	*/
  SetAttr(full_window, attr);
  full_window->attr_init = full_window->attr;
  
  	/* Edit Text in full_window...	*/
  
  saved_opt = tx_option(0);
  DisplayText(full_window, text, len);
  tx_option(saved_opt);
  
  	/* Find the number of lines 	*/

  cols = full_window->dim[1];
  
  for (lines = full_window->dim[0]; --lines > 0; )
  {	p = Aij(full_window, lines, 0);
  	for (j=0; (j < cols) && (*p == full_window->attr_init); j++, p++) ;
  	if (j < cols)	break;
  }
  
  	/* Find the number of Columns	*/

  for (cols = 0, i = ++lines; --i >=0; )
  {	p = Aij(full_window, i, 0);
  	for (j = full_window->dim[1]; 
  	    (--j > 0) && (*(p+j) == full_window->attr_init); ) ;
  	cols = MAX(cols, j);
  }
  ++cols;	/* Correct Number of Columns	*/
  
  
	/* Use OpenSmartWindow which positions the window,
	   and eventually removes the borders
	 */

  w = OpenSmartWindow (name, lines, cols, attr, border, pos_opt);

	/* Copy text from full_window to newly created one	*/
	
  for (i = 0, j=0; i < lines; i++, j += cols)
  	tw_copy(w, j, Aij(full_window, i, 0), cols);
  
	/* Find a reasonnable position for cursor...	*/

  for (i = lines; --i > 0; )
  {	p = Aij(w, i, 0);
	for (j=0; j<cols; j++, p++)
	{	if(isGraphic(*p))	continue;
  		if (*p == w->attr_init)	continue;
  		break;
  	}
  	if (j < cols)	break;
  }
  p = Aij(w, i, cols-1);
  for (j = cols; --j > 0; p--)
  {	if(isGraphic(*p))	continue;
  	if (*p == w->attr_init)	continue;
  	break;
  }
  if (++j >= cols)	j = cols - 1;
  CursorTo(w, i, j);

  EXIT_PTR(WINDOW, w);
}

/*==========================================================================
 *			tx_prp
 *==========================================================================*/
int tx_prp(text, len, attr, border, pos_opt)
/*+++
.PURPOSE Display a TeX-like text a text onto an adjustable window, 
	waits for any character from the user, then remove the window.
.RETURNS The key typed in (set GetKey2)
.REMARKS 
---*/
	char	*text;	/* IN: Text to display	*/
	int	len;	/* IN: Length of text	*/
	int	attr;	/* IN: Window Attribute	*/
	int	border;	/* IN: Window border options		*/
	int	pos_opt;/* IN: Window positioning option _UPLEFT_, etc */
{
	WINDOW	*w;
	int	stat;
	TWSAVE	saved;

  ENTER("tx_prp");

  stat = 0;
  SaveCursor(&saved);

  if ((w = tx_aopen("TeX-adj", text, len, attr, border, pos_opt)))
  {				/* Show now the Window	*/
  	RaiseWindow(w);
	stat = GetKey2(w);
  	DestroyWindow(w);
  }

  RestoreCursor(&saved);

  EXIT(stat);
}
