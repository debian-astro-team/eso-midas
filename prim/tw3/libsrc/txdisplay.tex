% @(#)txdisplay.tex	19.1 (ESO-IPG) 02/25/03 14:12:58 
\beginofdbc
\settype{Module}
\version{3.7}
\date{22-Nov-1990}
\setname{txdisplay.c}
\author{Francois Ochsenbein [ESO-IPG]}
\language{C}
\class{Display ``TeX-like'' text}
\filename{txdisplay.c}
\enva{TermWindows}
\envb{\#include $<$twset.h$>$}
\envb{\#include $<$tex.h$>$}
\envb{\#include $<$atex.h$>$}
\envb{\#include $<$buffer.h$>$}
\envb{\#include $<$str.h$>$}
\commentsa{This module contains routines to output a ``TeX-like'' text on a window. A complete macro substitution is 
performed (cf tex.c module).}

\commentsb{}

The text to process follows some of the
\LaTeX\footnote{Leslie Lamport, Addison-Wesley Publishing Company} conventions:
it includes both the text and ``macros'' that specify
processing options.
The available macros are listed in the table.

\begin{table}[htbp]
$$\begin{tabular}{|lp{30em}|} \hline
Name & Explanation \\ \hline
{\tt\b b} & Issue a single backslash (\b) \\
{\tt\b$x$} & Issue symbol $x$, with possibilities
\{ \} \% \$ \& \_ \^{ } \# space \\
\hline \hline
\multicolumn{2}{|c|}{{\em Blanks and Line Position}} \\ \hline \hline
{\tt\b\b} & Break the current line \\
{\tt\b quad} {\tt\b qquad} & Issue 2 or 4 blanks \\
{\tt \~{ }} & Issue a single non-breakable blank \\
{\tt\b Hspace\{$n$\}} & Issue $n$ non-breakable blanks \\
{\tt\b tab} & Align to next ``tab'' \\
{\tt\b SkipLine} & Issue a single blank line \\
{\tt\b indent} & Indentation \\
{\tt\b hfill} & Horizontal Stretchable space \\
{\tt\b vfill} & Vertical Stretchable space \\
{\tt\b tabright\{{\em text}\} & Align rightmost letter of {\em text} to
next ``tab'' \\
%
\hline \hline
\multicolumn{2}{|c|}{{\em Line Justifications}} \\ \hline \hline
{\tt\b centerline\{{\em text}\}} & Issue a line with {\em text}
centered.
(see also {\tt center} environment)\\
{\tt\b leftline\{{\em text}\}} & Issue a line with {\em text}
adjusted to the left
(see also {\tt left} environment)\\
{\tt\b rightline\{{\em text}\}} & Issue a line with {\em text}
adjusted to the right
(see also {\tt right} environment)\\
\hline \hline
\multicolumn{2}{|c|}{{\em Video Attributes}} \\ \hline \hline
{\tt\b VB} or {\tt\b bf} & use {\bf Boldface} attribute \\
{\tt\b Vu} or {\tt\b sl} & use {\sl Underscore} attribute \\
{\tt\b Vr} or {\tt\b em} or{ \tt\b it} & use {\em Reverse} attribute \\
{\tt\b Vb} & use {\tt Blink} attribute \\
{\tt\b fbox\{{\em text}\}} & use \fbox{Bold+Reverse+Blink}
attribute \\
%
\hline \hline
\multicolumn{2}{|c|}{{\em Environments}} \\ \hline \hline
{\tt\{verbatim\}} & Display text as it is \\
{\tt\{center\}} & Centering text \\
{\tt\{left\}} \quad{\tt\{right\}} & Left-aligned / Right-adjusted text,
also called raggedright / raggedleft\\
{\tt\{indent\}} & Push left margin \\
{\tt\{quote\}} & Push left and right margins \\
{\tt\{itemize\}} & Itemized list; {\tt\b item}
starts each item \\
{\tt\{enumerate\}} & Start an enumerated list;
{\tt\b item} starts each item \\
{\tt \{alphaenumerate\}} & Enumerated list using
lowercase letters\\
{\tt \{Alphaenumerate\}} & Enumerated list using
uppercase letters\\
{\tt \{more\}}\{{\em name}\} & Text which can be retrieved,
but not displayed \\
{\tt \{table\}\{{\em pos}\}\{{\em w}\}} &
Table environment.
See explanations in the text. \\
\hline \hline
\multicolumn{2}{|c|}{{\em Environment-specific macros}} \\ \hline \hline
%
{\tt\b item} & Specifies a new element in the
itemize / enumerate list\\
{\tt\b crule\{$c_1$--$c_2$\}} {\tt\b Rule} &Issue a horizontal line over
one columns $c_1$ to $c_2$ and whole table \\
{\tt\b len\{{\em text}\}} & Computes length of {\em text} \\
{\tt\b multicolumn\{$n$\}\{{\em j}\}\{{\em text}\}}
& define a column spreading over $n$
columns with {\em j} justification.\\
{\tt\b columns\{{\em pos}\}\{{\em w}\}} & redefine columns in
Table environment\\
\hline \hline
\multicolumn{2}{|c|}{{\em Miscellaneous}} \\ \hline \hline
%
{\tt\b bell} & Ring the terminal bell \\
{\tt\b input\{{\em filename}\}} & Get text to process from {\em filename} \\
{\tt\b def\b{\em name\#}\dots\{{\em equivalence}\}}
& Macro definition \\
{\tt\b today}\quad {\tt\b time}\quad {\tt\b now}
& Date, time, date+time editions\\
{\tt\b EOF} & End of text indicator (stops when found) \\
{\tt\b iftrue} {\tt\b iffalse}
{\tt\b else} {\tt\b fi} & Conditional tests \\
{\tt\b CheckLines\{$n$\}} & Terminate if less than $n$ lines
remain on the current window \\
{\tt\b FormField\{{\em name}\}\{{\em type}\}\{{\em pic}\}\{{\em text}\}}&
Define a named field
which can be retrieved in the calling program
\via {\em GetMarkedFields}\\
\hline\end{tabular}$$
\caption{\label{macro:tex}TermDisplay macros}\end{table}

\medskip
{\bf Table Environment}

\label{env:table}
The 2 parameters of the {\tt\b begin\{table\}} or
{\tt\b columns} macros specify:
\begin{enumerate}
\item The {\em pos} parameter, similar to the \LaTeX:
possibilities of \quad l \quad r \quad c \quad p \quad for left,
right, centered or justified text,
$|$ for a vertical line separating columns,
and blanks before / after the $|$ .
\item The {\em w} parameter specifies the {\em width} of
each column, as number of characters separated
by commas or blanks; if the last column width is
not specified, it is assumed to use the {\em remaining space}.
\end{enumerate}

Examples:
\begin{itemize}
\item {\tt\b begin\{table\}\{|r |p|\}\{5,50\}} will create a table with 2 columns:
a right-adjusted column (with a blank at the left of the
second $|$) of 5 characters, and a justified column
with 50 characters; the complete width of the table is 59 characters
(don't forget the space required by the vertical lines and blanks!)
\item {\tt\b begin\{table\}\{rcp\}\{5,10\}} will create a table with 3 columns:
a right-adjusted column of 5 characters, a centered column of 10
characters, and a justified column of 65 characters if the
width of the window is 80 characters.
\end{itemize}

\medskip
{\bf more environment}
\label{env:Ccode}

The text between {\tt\b begin\{more\}\{Ccode\}} and {\tt\b end\{more\}}
contains C instructions (cf {\bf cc} module) which can be retrieved with
the {\em tx\_more{Ccode}} function, then compiled (see {\em cc\_compile})
and executed.
The text between {\tt\b begin\{more\}\{Ccode\}} and {\tt\b end\{more\}}
will never be displayed on a window.

\medskip

The program uses a {\em tex\_action} routine; the characters passed to
this routine have the following meaning:
$$\begin{tabular}{|ll||ll|} \hline
\multicolumn{2}{|c||}{\em Single character actions} &
\multicolumn{2}{|c|}{\em Two character actions} \\
\hline
\ &single stretchable blank &
& \\
\b r &new line &
& \\
\b n &new paragraph &
& \\
0 &begin / end environments &
F &Field definitions\\
\{ &open a new environment &
f &fill horizontal / vertical\\
\} &return to previous environment &
J &justification specifications\\
/ &issue a blank line &
M &Move margins operations\\
\& &start next column of a table &
m &multicolumn definitions\\
t &start at next `tab' &
V &Video attributes specifications\\
b &ring terminal bell &
& \\
r &rule over one column &
& \\
R &rule all over table &
& \\
\hline
\end{tabular}$$


\historya{[1.0] 24-Sep-1986: Creation}

\historyb{[1.1] 30-Jan-1987: Redefinition of internal functions as static}

\historyb{[1.2] 01-Jun-1987: Added \b ul macro (underline)}

\historyb{[2.0] 02-Jul-1987: Version '2' of TermWindows. Removed LastRule. Added CentreColumns, LeftColumns, 
RightColumns.}

\historyb{[2.1] 28-Sep-1987: For Forms, added Macros}
\historyb{\b Field\{name\}\{text\}}
\historyb{\b Hspace\{len\}}

\historyb{[3.0] 29-Apr-1988: Version '3', with complete macro processing.}

\historyb{[3.1] 08-Dec-1988: \b tab action does not indent if the cursor is at a line beginning. Added environment 
\{Ccode\}, which accumulates the text into the Ccode buffer, and which can be retrieved via tx\_ccode.}

\historyb{[3.2] 31-Jan-1989: \b columns\{\}\{\} to redefine columns of a table}

\historyb{[3.3] 01-Mar-1989: Replaced Ccode by \{more\}\{Ccode\}. Added tx\_symbol() to translate a symbol.}

\historyb{[3.31] 14-Mar-1989: Removed bug in tx\_action, at tx\_item call, and tx\_item (Syndrome: item tag disappeared 
at top of page)}

\historyb{[3.32] 26-May-1989: Added tx\_def to define a symbol.}

\historyb{[3.4] 29-Jun-1989: Redesigned table editions.}

\historyb{[3.5] 24-Jul-1989: Removed bug in tx\_rule / nltab}

\historyb{[3.51] 16-Aug-1989: Added close\_table as a separate routine.}

\historyb{[3.6] 19-Mar-1990: Removed bug in tx\_action.}

\historyb{[3.7] 21-Nov-1990: Added obeylines / obeyspaces + begin\{group\}/end\{group\}}
\endofset
\beginofentry
\entrytype{Function}
\entryname{tx\_tex}
\synopsis{TeX $*$tx\_tex()}
\descriptiona{Retrieve the TeX structure (macro table, etc); initilize if not done.}
\returna{Pointer to the TeX structure used by TermDisplay.}
\remarksa{The macros H-table is created if necessary}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_option}
\synopsis{int tx\_option(opt)}
\paramy{int opt}{IN: New option 0 = Don't Center}
\descriptiona{Change options.}
\returna{Old options.}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_symbol}
\synopsis{char $*$tx\_symbol(str)}
\paramy{char $*$str}{IN: String to translate}
\descriptiona{Retrieve the translation of a definition}
\returna{Pointer to the translated definition}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_def}
\synopsis{int tx\_def(symbol, synonym)}
\parama{char $*$symbol}{IN: Symbol to define}
\paramz{char $*$synonym}{IN: Equivalence}
\descriptiona{Set a symbol in the TeX definitions.}
\returna{1}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_display}
\synopsis{int tx\_display(w, str, len, clear\_option)}
\parama{WINDOW $*$w}{IN: Window to echo the processed text}
\paramb{char $*$str}{IN: text to display, or NULL (continuation )}
\paramb{int len}{IN: Length of text}
\paramz{int clear\_option}{IN: Option 1 to clear window before display}
\descriptiona{Display a text on the window.}
\returna{--- OK (no remaining text)}

\returnb{--- NOK (text did not fit on the window)}

\returnb{--- EOF: the \b EOF macro was fond in the text.}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_mdisplay}
\synopsis{int tx\_mdisplay(w, str, nstrings, clear\_option)}
\parama{WINDOW $*$w}{IN: Window to echo the processed text}
\paramb{char $*$$*$str}{IN: Array of strings to display, as n$*$ (start, end)}
\paramb{int nstrings}{IN: Number of strings}
\paramz{int clear\_option}{IN: Option 1 to clear window before display}
\descriptiona{Display a text on the window made of several pieces of text. The pieces are provided by the array of 
strings str made of (2$*$nstrings) pointers, specifying nstrings times (start, end). (a pointer to the end of a string 
is the byte just following the last byte of the string, i.e. the address of the null character for a standard string)}
\returna{--- OK (no remaining text)}

\returnb{--- NOK (text did not fit on the window)}

\returnb{--- EOF: the \b EOF macro was fond in the text.}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_fdisplay}
\synopsis{int tx\_fdisplay(w, fid, len, clear\_option)}
\parama{WINDOW $*$w}{IN: Window to echo the processed text}
\paramb{int fid}{IN: File number (opened by fi\_open)}
\paramb{int len}{IN: Maximal number of bytes to read}
\paramz{int clear\_option}{IN: Option 1 to clear window before display}
\descriptiona{Display a file extract on the window.}
\returna{--- OK (no remaining text)}

\returnb{--- NOK (text did not fit on the window)}

\returnb{--- EOF the \b EOF macro was found in the text.}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_file}
\synopsis{int tx\_file(w, fname, clear\_option)}
\parama{WINDOW $*$w}{IN: Window to echo the processed text}
\paramb{char $*$fname}{IN: File name}
\paramz{int clear\_option}{IN: Option 1 to clear window before display}
\descriptiona{Display a complete file extract on the window.}
\returna{--- OK (no remaining text)}

\returnb{--- NOK (text did not fit on the window)}

\returnb{--- EOF the \b EOF macro was found in the text.}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_fields}
\synopsis{TWFIELD $*$tx\_fields(w)}
\paramy{WINDOW $*$w}{IN: Window concerned}
\descriptiona{Retrieve the marked fields in a window.}
\returna{Address of first TWFIELD / null pointer if no field was defined.}
\remarksa{The last valid field is followed by a null name, i.e. (char $*$)0.}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_more}
\synopsis{char $*$tx\_more(name)}
\paramy{char $*$name}{IN: `more' name to find}
\descriptiona{Retrieve the `more' (between \b begin\{more\}\{name\} and \b end\{more\})}
\returna{Address of code / NULL if none}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_math}
\synopsis{int tx\_math(f)}
\paramy{int ($*$f)()}{IN: Function with arguments (string, length) to call when \$ \^{ } \_ are encountered}
\descriptiona{Define an action routine in case of mathematical symbols (\$ \^{ } \_)}
\returna{OK}
\remarksa{Use NULL\_FCT to take standard action (ignore)}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_out}
\synopsis{int tx\_out(str, len)}
\parama{char $*$str}{text to output}
\paramz{int len}{Length of text}
\descriptiona{This is the output routine.}
\returna{Number of bytes processed.}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_action}
\synopsis{int tx\_action(str, len)}
\parama{char $*$str}{text to output}
\paramz{int len}{Length of text}
\descriptiona{This is the action routine.}
\returna{Number of bytes processed.}
\endofentry
\beginofentry
\entrytype{Function}
\entryname{tx\_list}
\synopsis{int tx\_list()}
\descriptiona{List, in the Log File, the list of macros}
\returna{Number of non-uused symbols}
\remarksa{For debugging purposes.}
\endofentry
\endofdbc
\beginofdbx
\extrynamea{atoi}
\extrynameb{eh\_ed\_as}
\extrynameb{eh\_put}
\extrynameb{eh\_put1}
\extrynameb{h\_create}
\extrynamez{main\_TeX}
\extrynameb{main\_ascii}
\extrynameb{mm\_ball}
\extrynameb{mm\_bapp}
\extrynameb{mm\_bopen}
\extrynameb{mm\_bst}
\extrynamez{mm\_bunst}
\extrynameb{oscfill}
\extrynameb{osmmfree}
\extrynameb{osmmget}
\extrynameb{pm\_enter}
\extrynameb{pm\_iexit}
\extrynamez{pm\_pexit}
\extrynameb{strcomp}
\extrynameb{strcopy}
\extrynameb{strlen}
\extrynameb{strloc}
\extrynameb{strred}
\extrynamez{tex\_exec}
\extrynameb{tex\_getparm}
\extrynameb{tex\_input}
\extrynameb{tex\_list}
\extrynameb{tex\_load}
\extrynameb{tex\_mexec}
\extrynamez{tex\_symbol}
\extrynameb{tex\_unit}
\extrynameb{tv\_bell}
\extrynameb{tw\_attr}
\extrynameb{tw\_clear}
\extrynameb{tw\_goto}
\extrynamez{tw\_il}
\extrynameb{tw\_mrule}
\extrynameb{tw\_r}
\extrynameb{tw\_rule}
\extrynameb{tw\_st}
\extrynameb{tw\_uflag}
\extrynamez{tw\_write}
\extrynameb{tx\_jc}
\extrynameb{tx\_jr}
\extrynameb{tx\_justify}
\extrynameb{$<$twset.h$>$}
\extrynameb{$<$tex.h$>$}
\extrynamez{$<$atex.h$>$}
\extrynameb{$<$buffer.h$>$}
\extrynameb{$<$str.h$>$}
\endofdbx
