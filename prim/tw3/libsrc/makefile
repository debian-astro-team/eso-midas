# @(#)makefile	19.1 (ESO-IPG) 02/25/03 14:12:40
#++++++++++++++++++
# .COPYRIGHT:	Copyright (c) 1988 European Southern Observatory,
#						all rights reserved
# .TYPE		make file
# .NAME		$MIDASHOME/$MIDVERS/prim/lib/tw3/makefile
# .LANGUAGE	makefile syntax
# .ENVIRONMENT	Unix Systems. 
# .COMMENT	Compiles source files and generates library libtw3.a
# .REMARKS	DINC names refers to include files
# .AUTHOR	Francois Ochsenbein
# .VERSION 1.1	890405:		Implementation
# .VERSION 1.2	900221:		Added dependencies on header files
# .VERSION 2.3  920521:		Removing MLIB CG
# .VERSION 3.0  930308:		Using default.mk file

include ../../../local/default.mk

OBJ1 = 	termcap.o tvgets.o  tvput.o 				\
	tvclear.o tvcursor.o tvhelp.o tvindel.o 		\
	tvutil.o tvinit.o tvget.o tvout.o 			
OBJ2 =	twclear.o twcursor.o twhelp.o twindel.o 		\
	twutil.o twinit.o twget.o twout.o 			
OBJ3 =	txjustify.o txdisplay.o txprompt.o tydoc.o		\
	thelp.o tmenu.o 					\
	tapplic.o tacmd.o taerror.o				
OBJ4 =	tfcc.o tfget.o tfield.o tfile.o tform.o tfpic.o tfshow.o tftex.o 

MLIB = -lm
LLIB = -L$(LIBDIR) -ltw3 -ltc3 -los

LIBOUT = $(LIBDIR)/libtw3.a

LIBS = 	$(LIBDIR)/libtw3.a \
	$(LIBDIR)/libtc3.a \
	$(LIBDIR)/libos.a 

OBJS = 	tvtest.o  twtest.o  txtest.o  thtest.o  thcreate.o tftest.o
OUT =	tvtest.exe twtest.exe txtest.exe thtest.exe thcreate.exe tftest.exe
DINC = 	$(INC)/twset.h $(INC)/tw.h

# DEPENDENCIES:
#all: $(MAKEFILE_VMS) $(LIBOUT) $(OUT) 
all: $(MAKEFILE_VMS) $(LIBOUT)

$(MAKEFILE_VMS): makefile
	$(MAKE_VMS)

$(LIBOUT): $(OBJ1) $(OBJ2) $(OBJ3) $(OBJ4)
	$(AR) $(AR_OPT) $(LIBOUT) $(OBJ1)
	$(AR) $(AR_OPT) $(LIBOUT) $(OBJ2)
	$(AR) $(AR_OPT) $(LIBOUT) $(OBJ3)
	$(AR) $(AR_OPT) $(LIBOUT) $(OBJ4)
	$(RANLIB) $(LIBOUT)

$(OBJ1) $(OBJ2) $(OBJ3) $(OBJ4): $(DINC)

tvtest.exe: tvtest.o $(LIBS)
	$(LDCC) tvtest.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

twtest.exe: twtest.o $(LIBS)
	$(LDCC) twtest.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

txtest.exe: txtest.o $(LIBS)
	$(LDCC) txtest.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

thtest.exe: thtest.o $(LIBS)
	$(LDCC) thtest.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

thcreate.exe: thcreate.o $(LIBS)
	$(LDCC) thcreate.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

tftest.exe: tftest.o $(LIBS)
	$(LDCC) tftest.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

clean:
	rm -f $(OBJ1) $(OBJS)
	rm -f $(OBJ2) $(OBJS)
	rm -f $(OBJ3) $(OBJS)
	rm -f $(OBJ4) $(OBJS)
