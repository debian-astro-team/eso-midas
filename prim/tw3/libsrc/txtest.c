/* @(#)txtest.c	19.1 (ES0-DMD) 02/25/03 14:12:58 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Program
.NAME		txtest.c
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Terminal Handling
.COMMENTS	Test of tw (window) and tx (TeX-like) routines
.ENVIRONMENT	TermWindows
.VERSION  2.1	(Version '2' of TermWindows)
.VERSION  2.2	02-Jul-1987: Merging with TermDisplay.
.VERSION  2.3	02-Dec-1987: Usage of WhileInput
.VERSION  2.4	16-Feb-1990: Test for dumb terminals
------------------------------------------------------------*/

#define	PM_LEVEL	2

#include <stesodef.h>
#include <tw.h>
#include <buffer.h>
#include <str.h>

static short int screen_size[2];

#define STOPPER 	"\\EndOfText"

static WINDOW *w1, *w2, *w3, *wR, *wC;
static char temp,string[5];
static BUFFER *input;
static char string_to_display[1200];


	MONITOR(TXtest);

/*===========================================================================*/
main	()
/*+++++++
.PURPOSE Extensive Test Program for Window Facilities
	(including TeX)
.RETURNS 
--------*/
{
	char *p, ch;
	int i;
	char reply[80];

	OpenLog	("txtest.log","Testing TW (windows) facilities"); 
	LogLevel	(-20); 

  printf("Device ? "); gets(reply);
  p = (reply[0] ? reply : NULL_PTR(char));

  if_not (InitWindows(p, NULL_PTR(char), -1))
  {	printf("****%s\n", ERR_LOC());
	return;
  }

  ScreenSize(screen_size);


  wR = OpenWindow("Return", -3, -35, 3, 35,
  	0, _GRAPHICS_, 0);			/* Return Window	*/
  w1 = OpenWindow("List",0,0,0,0, _NORMAL_, _SCROLL_, 10);
  w2 = OpenWindow("Large",0,0,0,0, _NORMAL_, _GRAPHICS_ , 0);
  w3 = OpenWindow("Small", 0, -42, 0,42, _REVERSE_, _BORDER2_ , 0);
  wC = OpenWindow("Ccode", 0, -45, 0,45, _NORMAL_, 
  	_BORDER_|_BORDER2_|_TITLE_, 0);

  ClearWindow(w2);
  ClearWindow(w3);
  ClearWindow(w1);
  ActiveWindow(w1);

  Puts(w1,"This test program of terminal-independant ");
  Puts(w1,"   displaying facilities uses 4 windows:");
  Puts(w1,"\n1. The present window is used to display ");
  Puts(w1,"       the input text");
  Puts(w1,"\n2. A window of half-size, in reverse, ");
  Puts(w1,"       displays the processed text.");
  Puts(w1,"\n3. A bordered window of full size shows ");
  Puts(w1,"       another look of the same input");
  Puts(w1,"\n4. A 1-line bottom window is used for");
  Puts(w1,"       dialog.");

	/* Try DisplayStringWindow	*/

  while (GetYes("Display your own input on variable window ?") == OK)
  {	ClearWindow(w1);
  	SetAttr(w1, _REVERSE_);
  	PutCentered(w1, "Type your TeX-like text, terminate with <Return>");
  	SetAttr(w1, _NORMAL_);
  	Gets(w1, string_to_display, sizeof(string_to_display));
  	DisplayStringWindow(string_to_display,_REVERSE_, _CENTER_);
  }

  INQUIRE_FILE:
  CursorTo(wR,0,1);
  SetAttr(wR,_BOLD_);
  Put(wR, "File: ");
  SetAttr(wR,_NORMAL_);
  ActiveWindow(wR), RaiseWindow(wR);

  if (Gets(wR, reply, sizeof(reply)) < 0)	goto FIN;
  RemoveWindow(wR);
  ClearWindow(wR); 
  if (reply[0] == EOS)	goto STANDARD_DEMO;
  status = DisplayFile(w2,reply);
  RefreshWindow(w2);
  while(status == NOK)
  {	WaitNext();
	status = DisplayContinuation(w2);
	RefreshWindow(w2);
  }
  ShowCcode(wC);		/* Display the Ccode */
  goto INQUIRE_FILE;

  STANDARD_DEMO:
  while(Read1() != EOF)
  {	if (WaitandSee() < 0)	break;
	status = DisplayText(w3, input->buf, input->used);
	RefreshWindow(w3);
	while(status == NOK)
	{	WaitNext();
		status = DisplayContinuation(w3);
		RefreshWindow(w3);
	}

	ShowFields(w3);
  	ShowCcode(wC);		/* Display the Ccode */
	if (WaitandSee() < 0)	break;

	status = DisplayText(w2, input->buf, input->used);
	RefreshWindow(w2);
	while(status == NOK)
	{	WaitNext();
		status = DisplayContinuation(w2);
		RefreshWindow(w2);
	}

	ShowFields(w2);
	WaitCR();
  }

  FIN:
  EndWindows();
  CloseLog();
  return;
}

/*==========================================================================*/

WaitNext()
{
  char ch;

  ENTER("WaitNext");
  CursorTo(wR,0,5);
  SetAttr(wR,_BLINK_|_REVERSE_);
  Put(wR, "Display not complete...");
  RefreshWindow(wR);
  SetAttr(wR,_NORMAL_);

  GetKey(wR, &ch);
  RemoveWindow(wR);
  ClearWindow(wR);

  EXIT (OK);
}

WaitandSee()
/*++++++++++++
.PURPOSE Just wait the CR to perform the test
.RETURNS Input key
--------------*/
{
	char ch;
	int  stat;

  ENTER("WaitandSee");

  ClearWindow(wR);
  CursorTo(wR,0,1);
  SetAttr(wR,_BLINK_|_REVERSE_);
  Put(wR, "Hit <Return> to perform test");
  SetAttr(wR,_NORMAL_);
  RefreshWindow(wR);

  stat = GetKey(wR, &ch);
  RemoveWindow(wR);
  ClearWindow(wR);

  EXIT(stat);
}

WaitCR()
/*++++++++++++
.PURPOSE Just wait the CR to go blck to Menu
.RETURNS 0
--------------*/
{
	char ch;

  ENTER("WaitCR");

  ClearWindow(wR);
  CursorTo(wR,0,1);
  SetAttr(wR,_BOLD_);
  Put(wR, "Hit <Return> to continue");
  SetAttr(wR,_NORMAL_);
  RefreshWindow(wR);

  GetKey(wR, &ch);
  RemoveWindow(wR);

  EXIT(0);
}

/*==========================================================================*/

Read1()
{	static int fid = 0;
	int	len;
	static char line[200];

  ENTER("Read1");

  if (fid == 0)	fid = fi_open("tx.dat",READ|RECORD_MODE);
  if (fid == 0) EXIT(EOF);
  if_not(input)	input = BUF_Open(char,1024,1024);

  if (fi_gets(fid, line, sizeof(line)) == EOF)	EXIT(EOF);
  ClearWindow(w1);
  ActiveWindow(w1);
  BUF_Clear(input);

  DeactiveWindow(w1);
  RefreshWindow(w1);
  ActiveWindow(w1);
  while(fi_gets(fid,line,sizeof(line)) == OK)
  {	if (oscomp(STOPPER, line, sizeof(STOPPER)-1) == 0)	break;
	Puts(w1, line);
	len = strlen(line);
	line[len++] = '\n';	/* Don't forget the newline!!! */
	BUF_AppendItems(input, char, line, len);
  }

  EXIT (OK);
}
/*==========================================================================*/

ShowFields(w)
	WINDOW *w;
{
	char ch; 
	TWFIELD *a; 
	WINDOW  *subw;

  if_not(a = GetMarkedFields(w))	return(0);

  ClearWindow(wR);
  CursorTo(wR,0,1);
  SetAttr(wR,_BLINK_|_BOLD_|_REVERSE_);
  Put(wR, "Hit <Return> to Show \\Field's");
  SetAttr(wR,_NORMAL_);
  RefreshWindow(wR);
  GetKey(wR, &ch);
  RemoveWindow(wR);

  RefreshWindow(w);
  ActiveWindow(w);
  
  for ( ; a->name; a++)
  {	subw = OpenSubWindow(w, a->name, a->home[0], a->home[1],
  		a->dim[0], a->dim[1], _ATTRIBUTE_, 0, 0);
	ClearWindow(subw);
	/* SetAttr(subw, _GRAPHICS_);
	FillWindow(subw,RubbishChar()); */
	Put(subw, a->name);
	Put(subw,"[");
	Put(subw, a->type);
	Put(subw,"]");
	Put(subw, a->pic);
	LOG_ED_STRING("=Field: ", a->name);
	LOG_ED_STRING("  Type: ", a->type);
	LOG_ED_STRING("   Pic: ", a->pic );
	LOG_ED_I     (" Homes: ", subw->home[0]);
	LOG_ED_I     ("        ", subw->home[1]);
	LOG_ED_I     (" Lines: ", subw->dim[0]);
	LOG_ED_I     ("  Cols: ", subw->dim[1]);
	CloseSubWindow(subw);
  }
  return(1);
}

/*==========================================================================*/

ShowCcode(w)
	WINDOW *w;
{
	char ch; 
	char *a; 

  LOG_ED_STRING("=Symbol: ", GetSymbol("\\Symbol"));

  if_not(a = GetCcode())	return(0);

  ClearWindow(wR);
  CursorTo(wR,0,1);
  SetAttr(wR,_BLINK_|_BOLD_|_REVERSE_);
  Put(wR, "Hit <Return> to Display Ccode");
  SetAttr(wR,_NORMAL_);
  RefreshWindow(wR);
  GetKey(wR, &ch);
  RemoveWindow(wR);
  ClearWindow(wR);

  ClearWindow(w);
  Put(w, a);
  LOG_ED_STRING("Ccode: ", a);
  RaiseWindow(w);
  
  return(1);
}

