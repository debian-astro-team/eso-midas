C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  YSTVALS.FOR +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module YSTVALS.FOR
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C
C 060330	last modif
C ......................................................
C
C
C  call as
C  STVALS(ACTION,A,NAXIS,NPIX,SUBLO,SUBHI,CUTVLS,RSULTS,RESPIX,NOPIX,STAT)
C
C  input par:
C  ACTION:      char. exp.  = MIN for min, max calculation
C                           = MEAN for above + mean values, std deviation
C                           = ALL for above + moments of 2. + 3. order
C  A:           R*4 array   array to work on
C  NAXIS:       I*4         no. of axis of A (maxium of 3, currently)
C  NPIX:        I*4 array   no. of pixels per axis in A
C  SUBLO:       I*4 array   start pixels of subframe
C  SUBHI:       I*4 array   end pixels of subframe
C  CUTVLS:      R*4 array   user supplied cutvalues for subframe
C
C  output par:
C  RSULTS:      R*4 array   depending on ACTION, this array contains
C                           min, max, mean, sigma, 2-moment, 3-moment,
C                           total intensity
C  RESPIX:      I*4 array   holds pixel no. of min + max of array
C  NOPIX:       I*4         total no. of pixels inside subframe
C                           with value in [CUTVLS(1),CUTVLS(2)]
C  STAT:        I*4         return status
C                           = 0 o.k., otherwise error
C
C -------------------------------------------------- 
C 
C
      SUBROUTINE STVALS(ACTION,A,NAXIS,NPIX,SUBLO,SUBHI,CUTVLS,
     +            RSULTS,RESPIX,NOPIX,STAT)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  ACTION
      REAL      A(*), CUTVLS(*), RSULTS(*)
      INTEGER     NAXIS, NPIX(*), SUBLO(*), SUBHI(*)
      INTEGER     RESPIX(*), NOPIX, STAT
C
      CALL STSTR(1,ACTION)
      CALL YST1(A,NAXIS,NPIX,SUBLO,SUBHI,CUTVLS,
     +          RSULTS,RESPIX,NOPIX,STAT)
C
      RETURN
      END
C
      SUBROUTINE ZMSTAT(IMNO,AREA,NAXPIX,START,STEP,RBINS,
     +                  FORMSTR,DEFAUL)
C 
      IMPLICIT NONE
C
      CHARACTER*(*)  AREA, FORMSTR, DEFAUL
      REAL      RBINS(*)
      DOUBLE PRECISION START(*), STEP(*)
      INTEGER     IMNO, NAXPIX(*)
C
      CALL STSTR(1,AREA)
      CALL STSTR(2,FORMSTR)
      CALL STSTR(3,DEFAUL)
      CALL ZST1(IMNO,NAXPIX,START,STEP,RBINS)
C
      RETURN
      END
C


