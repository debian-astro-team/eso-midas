/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tcol2ima.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    table utilities
.COMMENTS    copy a single table column to  a 1-dim image

.VERSION 1.0	060207		created KB

 090701		last modif
---------------------------------------------------------------*/

#include <atype.h>
#include <tbldef.h>
#include <tblsys.h>
#include <tblerr.h>
#include <midas_def.h>
#include <computer.h>
#include <macrogen.h>

#include <stdio.h>
/*

*/

int tcol2ima(tname,colname,colno)
char  *tname;
char  *colname;
int  colno;

/*++++++++++++++++++
.PURPOSE copy selected elements of table column to 1-dim image,
         omit NULL values
.RETURNS status, 0 = o.k.
------------------*/

{
char ident[72], cunit[68], outima[124];

int tid,ncol,nrow,nacol,narow,nrall;
int imno, naxis, npix[3], actvals;
int dummy,nbytes,type,items;
int iRowNum;
int null, count;	
int selflag;	

double start[3], step[3];

float *pntr, rvalue;




/* get name of output image */

(void) SCKGETC("OUT_B",1,80,&actvals,outima);

tid = -1;
(void) TCTOPN(tname,F_I_MODE,&tid);
(void) TCIGET(tid,&ncol,&nrow,&nacol,&narow,&nrall);
if ((colno < 1) || (colno > ncol)) return (-9);

naxis = 1;				/* we only extract 1 column */
start[0] = start[1] = start[2] =  0.0;
step[0] = step[1] = step[2] = 1.0;
(void) strcpy(ident,"                ");
(void) strcpy(cunit,"                ");
 
/* take only the selected rows, PN Oct98*/

(void) TCSCNT(tid,&count);
if (count < 1) return (-7);

npix[0] = count;		/* number of rows currently selected */   
npix[1] = npix[2] = 1;

(void) TCBGET(tid,colno,&type,&items,&nbytes);	/* get column type */
if (items > 1) return (-8);

/* allocate space for the image + create it on disk */   

(void) SCIPUT(outima,D_R4_FORMAT,F_O_MODE,F_IMA_TYPE,naxis,npix,
              start,step,ident,cunit,(char **)&pntr,&imno); 

/* step through all selected rows to fill the image */

count = 0;				/* counter for no. of pixels */
for (iRowNum=0; iRowNum<nrow; iRowNum++) 
   {	
   (void) TCSGET(tid,iRowNum+1,&selflag);
   if (selflag != 0)			/* check if row selected */
      {	  	          				
      (void) TCARDR(tid,iRowNum+1,colno,1,1,&rvalue);
      null = isNULLF(&rvalue);		/* test if NULL */
      if (null != 1)
         {
         *pntr++ = rvalue;		/* only copy if not NULL element */
         count ++;
         }
      }
   }

(void) TCTCLO(tid);

if (count < 1) 
   {
   (void) SCFCLO(imno);
   return (-7);
   }

/* save colno (as string) + original table name + column spec */

(void) sprintf(outima,"%4.4d + ",colno);
(void) strcat(outima,tname);
(void) strcat(outima," + ");
(void) strcat(outima,colname);

dummy = -1;

/* actual no. of pixels copied different? */
if (npix[0] != count)
   (void) SCDWRI(imno,"npix",&count,1,1,&dummy);

(void) SCDWRC(imno,"TBL_INFO",1,outima,1,(int)strlen(outima),&dummy);
(void) SCFCLO(imno);

return(0);
}
/*

*/

int realrow(tname,colno,noloops,irownos,orownos)
char  *tname;
int  colno, noloops, *irownos, *orownos;

/*++++++++++++++++++
.PURPOSE get real row no.s from selected, non-NULL counts
.RETURNS status, 0 = o.k.
------------------*/

{
int tid,ncol,nrow,nacol,narow,nrall;
int stat;
int ka, kb;
int rownum;
int null, count;	
int selflag;	

float rvalue;



if (noloops < 1) return (-11);

tid = -1;
rownum = stat = 0;
(void) TCTOPN(tname,F_I_MODE,&tid);
(void) TCIGET(tid,&ncol,&nrow,&nacol,&narow,&nrall);
if ((colno < 1) || (colno > ncol)) 
   {
   stat = -12;
   goto end_of_it;
   }

(void) TCSCNT(tid,&count);
if (count < 1) 
   {
   stat = -13;
   goto end_of_it;
   }

stat = -15;
for (ka=0; ka <noloops; ka++)
   {
   count = irownos[ka];   
   if (count < 1)
      {
      stat = -14;
      goto end_of_it;
      }

   /* step through all input counts to find corresponding row no. */

   for (kb=0; kb<nrow; kb++) 
      {	
      rownum = kb + 1;
      (void) TCSGET(tid,rownum,&selflag);
      if (selflag != 0)			/* check if row selected */
         {	  	          				
         (void) TCARDR(tid,rownum,colno,1,1,&rvalue);
         null = isNULLF(&rvalue);		/* test if NULL */
         if (null != 1)
            {
            count --;			/* count all "valid" rows */
            }
         }
      if (count == 0)		/* we counted enough selected, non-NULL rows */
         goto outer_loop;
      }

  outer_loop:
   if (count > 0) goto end_of_it;	/* not enough valid rows... */

   orownos[ka] = rownum;
   }
stat = 0;

end_of_it:
(void) TCTCLO(tid);
return(stat);
}



