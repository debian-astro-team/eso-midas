C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  YF2CGEN.FOR +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module YF2CGEN.FOR
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C ......................................................
C

      SUBROUTINE COPWND(PNTRA,NPIXA,PNTRB,NPIXB,BGNA,BGNB,ENDA)
C
      IMPLICIT NONE
C
      REAL   PNTRA(*), PNTRB(*)
      INTEGER  NPIXA,NPIXB,BGNA,BGNB,ENDA
C 
      CALL YY1(NPIXA,NPIXB,BGNA,BGNB)
      CALL YY2(PNTRA,PNTRB,ENDA)
C
      RETURN
      END
C
      SUBROUTINE COPYF1(PNTRA,NPIXA,BGNA,DIMWA,PNTRB,NPIXB,BGNB)
C
      IMPLICIT NONE
C
      REAL   PNTRA(*), PNTRB(*)
      INTEGER  NPIXA,NPIXB,BGNA,BGNB,DIMWA
C
      CALL YY1(NPIXA,NPIXB,BGNA,BGNB)
      CALL YY3(PNTRA,DIMWA,PNTRB)
C
      RETURN
      END
C
      SUBROUTINE JMAGN(JMETH, ARR, NX, NY, NI, NB, FAC, XC, YC,
     +                 MAG, DMAG, SKY, DSKY, NRPIX, FLUX, STAT )
C
      IMPLICIT NONE
C
      REAL   ARR(*), FAC, XC, YC, MAG, DMAG, SKY, DSKY
      REAL   NRPIX, FLUX
      INTEGER  JMETH, NX, NY, NI, NB, STAT
C
      CALL YY1(JMETH,NI,NB,0)
      CALL YY1a(NX, NY,XC, YC)
      CALL YY1b(MAG,DMAG,SKY,DSKY)
      CALL YY4(ARR,FAC,NRPIX,FLUX,STAT)
C
      RETURN
      END
C

      SUBROUTINE STACEN(P_IMG,DIMX,DIMY,METH,IMAGE,XOUT,YOUT,XERR,YERR,
     +             XSIG,YSIG,XYVAL,STAT)
C
      IMPLICIT NONE
C
      CHARACTER*(*) METH
      REAL   P_IMG(*), XOUT,YOUT,XERR,YERR,XSIG,YSIG, XYVAL
      INTEGER  DIMX, DIMY, IMAGE(*), STAT
C
      CALL YY1a(DIMX,DIMY,0.,0.)
      CALL YY1b(XOUT,YOUT,XERR,YERR)
      CALL YY1c(XSIG,YSIG,XYVAL)
      CALL YY5(P_IMG,METH,IMAGE,STAT)

C
      RETURN
      END
C

      SUBROUTINE PIXLIN(XA,YA,XB,YB,STEP,XINDX,YINDX,LIMIT,NINDX)
C
      IMPLICIT NONE
C
      REAL   XA,YA,XB,YB,STEP,XINDX,YINDX
      INTEGER  LIMIT, NINDX
C
      CALL YY1b(XA,YA,XB,YB)
      CALL YY6(STEP,XINDX,YINDX,LIMIT,NINDX)
C
      RETURN
      END
C
      SUBROUTINE ZIMA(P_IN,NPIX,XINDX,YINDX,NDIM,P_OUT,FMIN,FMAX)
C
      IMPLICIT NONE
C
      REAL   P_IN(*),P_OUT(*)
      REAL   XINDX,YINDX,FMIN,FMAX
      INTEGER  NPIX(*), NDIM
C
      CALL YY1b(XINDX,YINDX,FMIN,FMAX)
      CALL YY7(P_IN,NPIX,NDIM,P_OUT)
C
      RETURN
      END
C
      SUBROUTINE DATFIL(INFILE,DATTYP,TOTAL,A,B,MINFLG,FMIN,FMAX)
C
      IMPLICIT NONE
C
      CHARACTER*(*) INFILE
      REAL   A(*),B(*)
      REAL   FMIN,FMAX
      INTEGER  DATTYP,TOTAL,MINFLG
C
      CALL YY1(DATTYP,TOTAL,MINFLG,0)
      CALL YY8(INFILE,A,B,FMIN,FMAX)
C
      RETURN
      END


