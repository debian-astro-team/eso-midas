/*===========================================================================
  Copyright (C) 2009-2011 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
        Internet e-mail: midas@eso.org
        Postal address: European Southern Observatory
                        Data Management Division 
                        Karl-Schwarzschild-Strasse 2
                        D 85748 Garching bei Muenchen 
                        GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.IDENT        sort.c
.LANGUAGE     C
.AUTHOR       K. Banse   ESO/IPG
.KEYWORDS     sort algorithms
.PURPOSE      different sorting routines
.VERSION 
 090630		creation 
 110517         last modif
 150215         making use of hsort and heapSort modules (libsrc/math)
---------------------------------------------------------------------*/

#include <mutil.h>

void hsort2(nsiz,ra,ia)
int      nsiz;		/* no. of elements in array */
float    *ra;		/* pointer to array to be sorted */
int      *ia;		/* pointer to corresp. pixnos */

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   sort array and pixnos in place using heapsort
.RETURN    none
-----------------------------------------------------------------------*/

{
  heapSortFloat(nsiz, ra, ia); 
}


int sortmed(ra,zbins,num,ll,valu)
int   num, ll;
float *ra, *zbins, *valu;
 
/*

sort an array to get the median value
 
we only sort fully until index of median - then we insert
for sorting we use the Heapsort algorithm from standard library
 
use via stat = sortmed(ra,zbins,n,ll,valu)
 
input par:
zbins:	float array	low, high excess bins, if lo >= hi, no excess bins
num:	int		size of array RA
ll:	int		index of array element to be returned
 
in/output par:
ra:	float array	array to be sorted
 
output par:
valu:	float 		value = ra(ll) of sorted array ra
status:	int		return status, = 0 is o.k.
 
*/


{
int   ntrue, lltrue, l, i, status;

float zl, zh;

status = 0;

  /*  first check for excess bins */
  if (zbins[2] > zbins[1])
    {
      zl = zbins[1];
      zh = zbins[2];
      i = 0;
      for (l=0;l<num;l++)
	if ((ra[l] >= zl) && (ra[l] <= zh)) {
	  ra[i] = ra[l];
	  i++;
	}
      if (i > 3)
	{
	  ntrue = i ;
	  lltrue = (ntrue+1)/2;
	}
      else if (i < 1)
	{
	  status = -1;
	  return(status);
	}
      else if (i <= 2)
	{
	  *valu = ra[0];
	  return(status);
	}
      else                     /* i == 3 */
	{
	  *valu = ra[1];
	  return(status);
	}
    }
  else
    {
      ntrue = num;
      lltrue = ll;
    }
 
  // sortr(ra,ntrue);
  std_sortf(ntrue,ra);

  *valu = ra[lltrue];

return 0;
}

