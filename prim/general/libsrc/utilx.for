C @(#)utilx.for	19.1 (ES0-DMD) 02/25/03 14:01:09
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE CNTDAT(INFILE,DAT,TOTAL)
C	
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C	
C.IDENTIFICATION
C  subroutine CNTDAT		version 1.00	911205
C  K. Banse			ESO - Garching
C
C.KEYWORDS
C  ASCII data file 
C
C.PURPOSE
C  get no. of data values in ASCII file
C
C.ALGORITHM
C  read all records of an ASCII file + return exact no. of chars.
C
C.INPUT/OUTPUT
C  call as  CNTDAT(INFILE,DAT,TOTAL)
C
C  input parameter:
C  INFILE:	char		name of ASCII data file
C  DAT:         char            CHAR or NUM for char. or numeric data
C 
C  output parameter:
C  TOTAL:	integer		no. of chars to get
C
C-----------------------------------------------------------------------
C	
      IMPLICIT NONE
C 
      INTEGER  TOTAL,IDUM(1)
      INTEGER  K,N,FB
C 
      CHARACTER*(*)  INFILE,DAT
      CHARACTER   RECORD*80,NEWREC*80
C 
      REAL    RBUF(60)
C 
      DOUBLE PRECISION  DDUM(1)
C 
      TOTAL = 0
C 
      N = INDEX(INFILE,' ') - 1
      IF (N.LE.0) N = LEN(INFILE)
      OPEN(UNIT=33,FILE=INFILE(1:N),STATUS='OLD',ERR=9900)
C 
C  branch on DAT
      IF (DAT(1:1) .EQ. 'C') THEN              !get total no. of chars.
1000     DO 1010, K=1,80
            RECORD(K:K) = '^'
1010     CONTINUE
         READ(33,10000,END=5000) RECORD
         DO 1050, K=80,1,-1
            IF (RECORD(K:K).NE.'^') THEN
               N = K
               GOTO 1100
            ENDIF
1050     CONTINUE
         N = 0                                 !nothing in the record
1100     TOTAL = TOTAL + N
         GOTO 1000
      ELSE          
C                                              !get total no. of numbers
2000     RECORD(1:) = ' '
         NEWREC(1:) = ' '
         READ(33,10000,END=5000) RECORD
C
         K = 1
         FB = 1                        !first blank flag is set in the beginning
         DO 2500 N=1,80
            IF (RECORD(N:N).NE.' ') THEN
               NEWREC(K:K) = RECORD(N:N)
               K = K + 1
               FB = 0
            ELSE
               IF (FB.EQ.0) THEN
                  NEWREC(K:K) = ','
                  K = K + 1
                  FB = 1
               ENDIF
            ENDIF
2500     CONTINUE
C
C  convert to real numbers to get the count
         CALL GENCNV(NEWREC,2,60,IDUM,RBUF,DDUM,K)
         IF (K.GT.0) TOTAL = TOTAL + K
         GOTO 2000
      ENDIF
C 
C  end-of-file reached
5000  CLOSE(UNIT=33)
      RETURN
C
C  file open error
9900  RECORD(1:) = 'Problems with ASCII data file: '
      RECORD(32:) = INFILE(1:)
      CALL STETER(65,RECORD)
      RETURN
C
10000 FORMAT(A)
C
      END

      SUBROUTINE CARFIL(INFILE,TOTAL,A)
C	
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C	
C.IDENTIFICATION
C  subroutine CARFIL		version 1.00	911204
C  K. Banse			ESO - Garching
C
C.KEYWORDS
C  ASCII data file 
C
C.PURPOSE
C  get character data from ASCII file
C
C.ALGORITHM
C  read all records of an ASCII file, fill buffer + return exact no. of chars.
C
C.INPUT/OUTPUT
C  call as  CARFIL(INFILE,TOTAL,A)
C
C  input parameter:
C  INFILE:	char		name of ASCII data file
C  TOTAL:	integer		no. of chars to get
C 
C  output parameter:
C  A:		char array	buffer to be filled
C
C-----------------------------------------------------------------------
C	
      IMPLICIT NONE
C 
      INTEGER  TOTAL
      INTEGER  K,N,I
C 
      CHARACTER*(*)  A,INFILE
      CHARACTER   RECORD*80
C 
      N = INDEX(INFILE,' ') - 1
      IF (N.LE.0) N = LEN(INFILE)
      OPEN(UNIT=33,FILE=INFILE(1:N),STATUS='OLD',ERR=9900)
      K = 0
C 
3000  IF (K.LT.TOTAL) THEN
         DO 3010, I=1,80
            RECORD(I:I) = '^'
3010     CONTINUE
         READ(33,10000,END=5000) RECORD
         DO 3050, I=80,1,-1
            IF (RECORD(I:I).NE.'^') THEN
               N = I
               GOTO 3100
            ENDIF
3050     CONTINUE
         GOTO 3000
C 
3100     IF (K+N.GT.TOTAL) N = TOTAL - K
         A(K+1:K+N) = RECORD(1:N)
         K = K + N
         GOTO 3000
      ENDIF
C 
C  end-of-file reached
5000  CLOSE(UNIT=33)
      RETURN
C
C  file open error
9900  RECORD(1:) = 'Problems with ASCII data file: '
      RECORD(32:) = INFILE(1:)
      CALL STETER(1,RECORD)
      RETURN
C
10000 FORMAT(A)
C
      END
