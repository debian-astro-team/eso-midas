/* @(#)luttbl.c	19.1 (ES0-DMD) 02/25/03 14:01:07 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT:  Copyright (c) 1994 European Southern Observatory,
                                         all rights reserved
.IDENTIFIER  module LUTTBL
.LANGUAGE    C
.AUTHOR      K. Banse			ESO - IPG, Garching
.KEYWORDS    LUT/ITT tables
.PURPOSE     use table interfaces
.CONTAINS    OPNTBL, CRELUT & CREITT
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces

.VERSIONS    1.00       940704  F77 -> C, from TABSUBS.FOR    R.M.van Hees
.VERSIONS    1.10       941109  cleanup + fix it
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1

#include <stdio.h>
#include <midas_def.h>

#define MYLUTSZ		256

/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  OPNTBL
.PURPOSE     open LUT or ITT tables
.INPUT/OUTPUT
  call as    OPNTBL( table, tid, ncols, nrows )
     input:
         char *table :		table name
     output:
         int  *tid   :		id of table
         int  *ncols :		number of columns
         int  *nrows :		number of rows

.RETURNS     nothing
--------------------------------*/

void OPNTBL( table, tid, ncols, nrows )
char *table;
int  *tid, *ncols, *nrows;

{      
int  allcol, allrow, ec, el, ed, nsort, stat;
char file[84];

static int  con_alw = 1, log_no = 0, dis_no = 0;     /* variables for SCECNT */


/* first look for table in current directory,
   then for table in MID_WORK, finally look for system table */

(void) SCECNT( "GET", &ec, &el, &ed );
(void) SCECNT( "PUT", &con_alw, &log_no, &dis_no );
stat = TCTOPN( table, F_I_MODE, tid );

nsort = 0;		/* use as indicator of SCECNT */
if (stat != ERR_NORMAL)
   {
   (void) sprintf( file, "MID_WORK:%-s", table );
   stat = TCTOPN( file, F_I_MODE, tid );
   if ( stat != 0 )
      {
      (void) sprintf( file, "MID_SYSTAB:%-s", table );
      (void) SCECNT( "PUT", &ec, &el, &ed );
      nsort = 1;	
      stat = TCTOPN( file, F_I_MODE, tid );
      }
   }

/* get info about table */

if (nsort == 0) (void) SCECNT( "PUT", &ec, &el, &ed );
(void) TCIGET( *tid, ncols, nrows, &nsort, &allcol, &allrow );
}
/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  CRELUT
.PURPOSE     create + fill LUT tables for Image Display
.INPUT/OUTPUT
  call as    CRELUT( table, rlut )
     input:
         char *table:	table name
         float *rlut:	LUT table (red, green, blue)
         int   lutlen:	LUT size
         int   format:	format = 0 for Midas table, 9 for ASCII file
         int   *range:	low, high range (0,1 or 0,255 supported)

.RETURNS     nothing
--------------------------------*/

void CRELUT (table,rlut,lutlen,format,range)
char  *table;
float *rlut;
int   lutlen, format, *range;

{
register int  nn;
register float *pntrR, *pntrG, *pntrB;

int   idx, tid, tcolnm[3], idata[3];
float data[3];
char  mytab[61];
      
int IndxR, IndxG, IndxB;
static char *tbllab[3] = { "RED", "GREEN", "BLUE" };

idx = CGN_INDEXC(table,' ');
if (idx < 0) idx = strlen(table);
if (idx > 56)
   SCETER(111,"CRELUT: input table name > 56 chars....");

IndxR = 0;
IndxG = lutlen;
IndxB = lutlen + lutlen;
pntrR = rlut + IndxR;
pntrG = rlut + IndxG;
pntrB = rlut + IndxB;


/* check for table or ASCII file option */

if (format == 9) 			/* write to ASCII file */
   {
   int  fp;

   fp = osaopen(table,1);               /* open for writing  */
   if (fp == -1)
      SCETER(123,"Could not open output file...");

   if (range[1] == 1)
      {
      for (nn=1; nn<=lutlen; nn++)
         {
         data[0] = *pntrR++;
         data[1] = *pntrG++;
         data[2] = *pntrB++;
         sprintf(mytab," %8.5f  %8.5f  %8.5f",data[0],data[1],data[2]);
         osawrite(fp,mytab,strlen(mytab));
         }
      }
   else
      {
      float  rm;

      rm = (float) (MYLUTSZ - 1);
      for (nn=1; nn<=lutlen; nn++)
         {
         data[0] = *pntrR++;
         idata[0] = CGN_NINT(data[0]*rm);
         data[1] = *pntrG++;
         idata[1] = CGN_NINT(data[1]*rm);
         data[2] = *pntrB++;
         idata[2] = CGN_NINT(data[2]*rm);
         sprintf(mytab," %d  %d  %d",idata[0],idata[1],idata[2]);
         osawrite(fp,mytab,strlen(mytab));
         }
      }
   osaclose(fp);
   }

else					/* write to Midas table */
   {
   (void) strncpy(mytab,table,idx);
   (void) strcpy(&mytab[idx],".lut");

   (void) TCTINI( mytab, 0, F_O_MODE, 8, MYLUTSZ, &tid );

   for (nn=0; nn<3; nn++)
      (void) TCCINI( tid, D_R4_FORMAT, 1, "e12.5", " ", tbllab[nn], tcolnm+nn );

   for (nn=1; nn<=lutlen; nn++)
      {
      data[0] = *pntrR++;
      data[1] = *pntrG++;
      data[2] = *pntrB++;
      (void) TCRWRR( tid, nn, 3, tcolnm, data );
      }

   (void) TCSINI( tid );		/* release table file properly */
   (void) TCTCLO( tid );
   }
}
/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  CREITT
.PURPOSE     create + fill ITT tables for Image Display
.INPUT/OUTPUT
  call as    CREITT( table, rlut )
     input:
         char *table :		table name
     output:
         float *ritt :		ITT table

.RETURNS     nothing
--------------------------------*/

void CREITT ( table, ritt )
char  *table;
float *ritt;

{
register int nn;

int  idx, tid, tcolnm;
char mytab[61];

static char *tbllab = "ITT";

idx = CGN_INDEXC(table,' ');
if (idx < 0) idx = strlen(table);
if (idx > 56)
   SCETER(111,"CREITT: input table name > 56 chars....");

(void) strncpy(mytab,table,idx);
(void) strcpy(&mytab[idx],".itt");

(void) TCTINI( mytab, 0, F_O_MODE, 4, MYLUTSZ, &tid );
(void) TCCINI( tid, D_R4_FORMAT, 1, "e12.5", " ", tbllab, &tcolnm );

for (nn=1; nn<=MYLUTSZ; nn++)
   (void) TCEWRR( tid, nn, tcolnm, ritt++ );

(void) TCSINI( tid );		/* release table file properly */
(void) TCTCLO( tid );
}
