/*===========================================================================
  Copyright (C) 1995-2007 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT:  Copyright (c) 1994-2012 European Southern Observatory,
                                         all rights reserved
.IDENTIFIER  logview
.LANGUAGE    C
.AUTHOR      K. Banse,                  ESO - IPG, Garching
.KEYWORDS    ImageDisplay
.PURPOSE     provides parallel output in a different xterm
.ALGORITHM   communicates via ASCII files

.CONTENT     holds logview_init, display_it

.VERSIONS    1.00       030116
 120207         last modif

------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <idi.h>
#include <idinumd.h>
#include <midas_def.h>

#define  LIMIT_LINES  100

static int  baselen, count, swindx, pipofd;

static char filename[160], switchy[4] = "AB";

/*

*/

#ifdef __STDC__
int logview_init(char *infofile)
#else
int logview_init(infofile)
char *infofile;
#endif

{
int  mm;

pid_t id ;

char  path[120], *midhome, *midvers;

int  display_it();




/* OJO: parallel logviewer not implemented for VMS ... */

#if vms

goto classic_way;

#endif



/* see, if we just want the classics way ... */

if (infofile[0] == '\0') goto classic_way;


/* we need the path to MID_EXE for module `logview.exe' */

if (!(midvers = getenv("MIDVERS")))
   {
   printf("getenv(MIDVERS) failed...\n");
   goto classic_way;
   }
if (!(midhome = getenv("MIDASHOME")))
   {
   printf("getenv(MIDASHOME) failed...\n");
   goto classic_way;
   }

(void) strcpy(path,midhome);
(void) strcat(path,"/");
(void) strcat(path,midvers);


/* fork a process for parallel logging */

id = fork() ;

/* from now on, we're either child or parent process */

if (id == -1)
   {
   printf("unable to fork child\n");
   goto classic_way;
   }


if (id == 0)				/* Child process */
   {
   (void) strcat(path,"/prim/exec/logviewer.exe");


   /* replace child exec by `xterm' */

   execlp("xterm","xterm","-T","info/log viewer","-bg","white",
          "-fg","black","-sb",
          "-e",path,infofile,(char *)0);


   /* if we're here, it didn't work... */

   printf(">>>>>>>>>>>>> execlp of `xterm -e ...' failed! <<<<<<<<<<<<<\n");
   exit(1);
   }


/* here the parent process continues */


mm = CGN_COPY(path,infofile);			 /* try to delete previous */
(void) strcpy(&path[mm],"A");			 /* pair of info files */
unlink(path);
(void) strcpy(&path[mm],"B");
unlink(path);


/* and start up sync via infofile */

mm =  display_it(infofile,777);
if (mm != 0)
   {
   char frame[124];

   (void) strcpy(frame,infofile);
   (void) strcat(frame,"A");
   (void) printf("could not create infofile %s\n",frame);
   (void) printf("=> switch to classical mode ...\n");
   (void) printf("please, terminate the `info/log-viewer' xterm by hand!\n");
   }
else
   return 0;


classic_way:
(void) display_it(" ",888);

return 0;
}

/*

*/

#ifdef __STDC__
int display_it(char *pipebuf, int pipo)
#else
int display_it(pipebuf,pipo)
char *pipebuf;
int pipo;
#endif

{
int ln, mm;

char  cbuf[120];




/* check, if init call */

if (pipo == 777)
   {					/* yes, we do use logview */
   baselen = CGN_COPY(filename,pipebuf);
   swindx = 0;
   filename[baselen] = switchy[swindx]; filename[baselen+1] = '\0';

   pipofd = open(filename,O_WRONLY|O_CREAT,S_IRWXU|S_IRWXG);
   if (pipofd < 1) return (-1);
    
   count = 0;
   /*
   printf("piping %s via fd = %d\n",filename,pipofd);
   */
   return 0;
   }

else if (pipo == 888)
   {
   pipofd = -1;
   count = 0;
 
   /*
   printf("piping info to terminal\n");
   */
   return 0;
   }



if (pipofd > (-1))
   {
   if (pipo == -9) return 0;		/* `erase line' => omit input ... */

   ln = (int) strlen(pipebuf);
   memset(cbuf,32,100);
   memcpy(cbuf,pipebuf,(size_t)(ln+1));

   mm = write(pipofd,cbuf,100);
   count ++;

   if (count == LIMIT_LINES)
      {
      close(pipofd);
      /* 
      printf(">>>>>>>>>>>>>> switching names...\n"); 
      */ 
      swindx = 1 - swindx;			/* 0 -> 1, 1 -> 0 */
      filename[baselen] = switchy[swindx];
      pipofd = open(filename,O_WRONLY|O_CREAT,S_IRWXU);
      if (pipofd < 1)
         {
         printf("could not create infofile %s\n",filename);
         printf("switch to classical mode ...\n");
         pipofd = -1;
         pipo = 0;
         }
      else
         {
         count = 0;
         printf("now piping %s via fd = %d\n",filename,pipofd);
         mm = 0;
         }
      }
   return (mm);
   }

if (strcmp(pipebuf,"EOF") != 0) 	/* EOF only for parallel display */
   SCTDIS(pipebuf,pipo);
return 0;
}
 
/* 
   rules for SCTDIS(text,bc) 
   -------------------------

   bc = -9	erase line
   bc = -1	write text and stop at end
   bc = 0	write text like SCTPUT
   bc > 0	do that many backspaces after writing text

*/

