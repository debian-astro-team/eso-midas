C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  YEXTRCO.FOR +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module YEXTRCO.FOR
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C ......................................................
C
C
C  call as
C   EXTCOO(IMNO,STRING,PIXDIM,SUBDIM,SUBLO,SUBHI,STAT)
C   EXTCO1(IMNO,STRING,PIXDIM,SUBDIM,SUBLO,STAT)
C
C   input par:
C   IMNO:      I*4            image no
C   STRING:    char.exp.      input string of the form [c1,c2,c3:d1,d2,d3]
C   PIXDIM:    I*4            max. no. of dimensions
C
C   output par:
C   SUBDIM:    I*4            actual dimension of subframe
C   SUBLO:     I*4 array      low pixels
C   SUBHI:     I*4 array      high pixels
C   STAT:      I*4            return status, = 0 o.k., else not o.k.
C
C  and
C 
C   XEXTC2(IMNO,STRING,PIXDIM,SUBDIM,SUBLO,SUBHI,STAT)
C   XEXTC1(IMNO,STRING,PIXDIM,SUBDIM,SUBLO,STAT)
C
C  all parameters as above, except:
C   SUBLO:     R*4 array      low pixels    (for fractions of frame pixels)
C   SUBHI:     R*4 array      high pixels
C
C 001218          last modif
C
C -------------------------------------------------- 
C 
C 
      SUBROUTINE ARTIMA(FLAG,IMNO,NPIX,STRING,SIZE,RETIMNO,STAT)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  STRING
      INTEGER     FLAG, IMNO
      INTEGER     NPIX, SIZE, RETIMNO, STAT
C 
      CALL STSTR(1,STRING)                      !STRIPPED_STRRING
C 
      CALL YART2(IMNO,FLAG,NPIX,SIZE,RETIMNO,STAT)
C
      RETURN
      END
C
      SUBROUTINE EXTCOO(IMNO,STRING,PIXDIM,SUBDIM,SUBLO,SUBHI,STAT)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  STRING
      INTEGER     IMNO, PIXDIM
      INTEGER     SUBDIM,SUBLO,SUBHI,STAT
C
      CALL STSTR(1,STRING)                      !STRIPPED_STRRING
C 
      CALL YEXTC1(IMNO,PIXDIM,SUBDIM,SUBLO,SUBHI,STAT)
C
      RETURN
      END
C
      SUBROUTINE XEXTC2(IMNO,STRING,PIXDIM,SUBDIM,SUBLO,SUBHI,STAT)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  STRING
      INTEGER     IMNO, PIXDIM
      INTEGER     SUBDIM,SUBLO,SUBHI,STAT
C
      CALL STSTR(1,STRING)                      !STRIPPED_STRRING
C 
      CALL YEXTC2(IMNO,PIXDIM,SUBDIM,SUBLO,SUBHI,STAT)
C
      RETURN
      END
C
      SUBROUTINE EXTCO1(IMNO,STRING,PIXDIM,SUBDIM,SUBLO,STAT)
C 
      IMPLICIT NONE
C
      CHARACTER*(*)  STRING
      INTEGER     IMNO, PIXDIM
      INTEGER     SUBDIM,SUBLO,STAT
C
      CALL STSTR(1,STRING)                      !STRIPPED_STRRING
C 
      CALL YEXC1(IMNO,PIXDIM,SUBDIM,SUBLO,STAT)
C
      RETURN
      END
C 
      SUBROUTINE XEXTC1(IMNO,STRING,PIXDIM,SUBDIM,SUBLO,STAT)
C 
      IMPLICIT NONE
C
      CHARACTER*(*)  STRING
      INTEGER     IMNO, PIXDIM
      INTEGER     SUBDIM,SUBLO,STAT
C
      CALL STSTR(1,STRING)                      !STRIPPED_STRRING
C 
      CALL YEXC2(IMNO,PIXDIM,SUBDIM,SUBLO,STAT)
C
      RETURN
      END
C


