$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.PRIM.GENERAL.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:56 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   arthmz.for
$ FORTRAN   brthmz.for
$ FORTRAN   crthmz.for
$ FORTRAN   arthmy.for
$ FORTRAN   arthmd.for
$ FORTRAN   avarea.for
$ FORTRAN   bgval.for
$ FORTRAN   calc.for
$ FORTRAN   dcalc.for
$ FORTRAN   express.for
$ FORTRAN   extrss.for
$ FORTRAN   functions.for
$ FORTRAN   fndedg.for
$ FORTRAN   olddsc.for
$ FORTRAN   lincol.for
$ FORTRAN   sortit.for
$ FORTRAN   minmax.for
$ FORTRAN   polfil.for
$ FORTRAN   util.for
$ FORTRAN   utilx.for
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
