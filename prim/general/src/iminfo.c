/*===========================================================================
  Copyright (C) 1988-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        iminfo.c
.LANGUAGE     C
.AUTHOR       K. Banse   ESO/IPG
.KEYWORDS     Bulk data frames, Images
.PURPOSE      Read File Control Block of given image and display that
	      info in a user friendly format
.VERSION

 090120		last modif

---------------------------------------------------------------------*/

#include   <fileexts.h>

#include <stdio.h>


#define  DISK_REC  512;

/*

*/

int main(argc,argv)
int argc;
char *argv[];


{
char    cbuf[32], infile[128], filetype[24], outbuf[80];

int  imnoa, datformat, nobytes, nval;
int  n, stat, unit, naxis;
int  Mflag, isfits, siprint, minfo[5];
int  e_c, e_log, e_disp, ln, ln1, ln2;
long int  *longtime;

double  dd;

float   rr;

struct  FCT_STRUCT *fctpntr;

struct  FCB_STRUCT *fcbp;





if (argc > 1)				/* we're with command line args. */
   {
   (void) strcpy(infile,argv[1]);
   SCSPRO("-1");
   n = 0;
   if (argc > 2)	
      {
      (void) strncpy(cbuf,argv[2],4);	/* we only need first 4 chars. */
      n = 4;
      }
   cbuf[n] = '\0';
   Mflag = 0;
   }
else
   {
   SCSPRO("iminfo");
   stat = SCKGETC("IN_A",1,120,&nval,infile);  		/* get input frame   */
   stat = SCKGETC("P2",1,4,&nval,cbuf);  		/* get action   */
   Mflag = 1;					/* we're inside Midas */
   }

for (n=0; n<5; n++) minfo[n] = 0;
isfits = 0;


if ((cbuf[0] == 'T') || (cbuf[0] == 't'))
   {
   int  size;
   double  dd[200];			/* test section for SCFMOD */
   float  cuts[4];


   unit = 0;			/* create an image frame using SCFMOD */
   size = 512 * 512;
   stat = SCFCRE(infile,D_I1_FORMAT,F_H_MODE,F_IMA_TYPE,size,&imnoa);

   minfo[0] = 2;
   SCDWRI(imnoa,"NAXIS",minfo,1,1,&unit);
   minfo[0] = minfo[1] = 512;
   SCDWRI(imnoa,"NPIX",minfo,1,2,&unit);
   dd[0] = dd[1] = 0.4;
   SCDWRD(imnoa,"START",dd,1,2,&unit);
   dd[0] = dd[1] = 1.1;
   SCDWRD(imnoa,"STEP",dd,1,2,&unit);
   strcpy(outbuf,"test header file stuff");
   SCDWRC(imnoa,"IDENT",1,outbuf,1,strlen(outbuf),&unit);
   strcpy(outbuf,"intensity    x-pix   y-pix");
   SCDWRC(imnoa,"CUNIT",1,outbuf,1,strlen(outbuf),&unit);
   cuts[0] = cuts[1] = 0.0;
   cuts[2] = 22.2; cuts[3] = 19999.9;
   SCDWRR(imnoa,"LHCUTS",cuts,1,4,&unit);
   strcpy(outbuf,"this descr. holds the cut values");
   SCDWRH(imnoa,"LHCUTS",outbuf,1,strlen(outbuf));

   for (n=0; n<200; n++) dd[n] = (double) n;
   for (n=0; n<20; n++) 
      {
      sprintf(outbuf,"double_descriptor%d",n);
      SCDWRD(imnoa,outbuf,dd,1,200,&unit);
      }

   SCFMOD(imnoa,D_R4_FORMAT,size);

   for (n=20; n<40; n++) 
      {
      sprintf(outbuf,"double_descriptor%d",n);
      SCDWRD(imnoa,outbuf,dd,1,200,&unit);
      }

   goto key_write;
   }


if ((cbuf[0] == 'D') || (cbuf[0] == 'd'))
   {					/* debug section for FCB, FCT */
   MID_SHOWFCT(infile);			/* show FCT + FCB entry */
   stat = MID_SHOWFCB(infile);
   if (stat == 999) 
      SCTPUT("unsupported descriptor format!");
   else if (stat != ERR_NORMAL)
      minfo[0] = -1;

   goto key_write;
   }




if ((cbuf[0] == 'I') || (cbuf[0] == 'i'))
   {                                    /* test section for CGN_FILINFO */
   long int filsize, ltime;
   
   CGN_FILINFO(infile,&filsize,&ltime,&nval);
   (void) printf("frame: %s\n",infile);
   (void) printf("filesize = %ld bytes, creation time = %ld, protection = %d\n",
                 filsize,ltime,nval);
   return (0);
   }




if ((cbuf[0] == 'E') || (cbuf[0] == 'e'))
   {                                    /* show all extensions of FITS file */
   n = MID_fitsin(-1,infile,0,filetype,filetype,&nval,minfo);


   /* if not in Midas just print */

   if (argc > 1)
      {
      if (n != 0) 
         (void) printf("%s - not a valid FITS file...\n",infile);
      else
         (void) printf("total no. of extensions = %d\n",minfo[0]);
      return (0);
      }

   if (n != 0) SCETER(11,"not a valid FITS file...");

   (void) SCKWRI("OUTPUTI",minfo,19,1,&unit);
   (void) sprintf(outbuf,"total no. of extensions = %d\n",minfo[0]);
   SCTPUT(outbuf);
   (void) SCKWRI("INPUTI",&isfits,19,1,&unit);
   
   SCSEPI();
   }


/*

*/

/* here the `normal' INFO/FRAME section */

SCECNT("GET",&ln,&ln1,&ln2);
e_c = 1; e_log = 0; e_disp = 0;
SCECNT("PUT",&e_c,&e_log,&e_disp);

stat = SCFOPN(infile,D_OLD_FORMAT,0,F_OLD_TYPE,&imnoa);
fctpntr = FCT.ENTRIES + imnoa;
fcbp = fctpntr->FZP;

SCECNT("PUT",&ln,&ln1,&ln2);

if (stat != ERR_NORMAL)
   {
   minfo[0] = -1;
   goto key_write;
   }


siprint = 1;
n = (int) strlen(fcbp->BDTYPE);
n --;
if (fcbp->BDTYPE[n] == 'F')
   {
   isfits = 9;
   if (argc < 2) siprint = 0;		/* in Midas + FITS file, don't print */
   (void) strcpy(filetype,"FITS ");
   }
else
   {
   isfits = 0;
   (void) strcpy(filetype,fcbp->BDTYPE);
   }


(void) sprintf(outbuf,"Name: %s - frame type: %s",fctpntr->NAME,filetype);
SCTPUT(outbuf);

(void) sprintf(outbuf,"Version: %s",fcbp->VERSION);
if (siprint == 1) SCTPUT(outbuf);
nval = 0;
(void) CGN_CNVT(&fcbp->VERSION[5],1,1,&nval,&rr,&dd);
minfo[4] = nval;

if (nval < 6) SCTPUT("this version is not supported anymore!");

nobytes = fcbp->NOBYT;
datformat = fcbp->DFORMAT;
minfo[0] = datformat;
minfo[1] = fcbp->NDVAL;
minfo[2] = nobytes;
if (filetype[0] == 'T')
   minfo[3] = 3;
else if (filetype[0] == 'F')
   minfo[3] = 4;
else
   minfo[3] = 1;			/* default to image */

if (datformat == D_R4_FORMAT)
   (void) sprintf(outbuf,
          "data type of pixels = real, %d bytes per pixel",nobytes);
else if (datformat == D_I4_FORMAT)
   (void) sprintf(outbuf,
          "data type of pixels = integer, %d bytes per pixel",nobytes);
else if (datformat == D_I2_FORMAT)
   (void) sprintf(outbuf,
          "data type of pixels = short int, %d bytes per pixel",nobytes);
else if (datformat == D_I1_FORMAT)
   (void) sprintf(outbuf,
          "data type of pixels = byte, %d bytes per pixel",nobytes);
else if (datformat == D_R8_FORMAT)
   (void) sprintf(outbuf,
          "data type of pixels = double prec., %d bytes per pixel",nobytes);
else if (datformat == D_UI2_FORMAT)
   (void) sprintf(outbuf,
          "data type of pixels = unsigned short int, %d bytes per pixel",
          nobytes);
if (siprint == 1) SCTPUT(outbuf);

(void) sprintf(outbuf,"Total no. of pixels = %d",fcbp->NDVAL);
if (siprint == 1) SCTPUT(outbuf);

n = (fcbp->D1BLOCK - 1) * DISK_REC;
(void) sprintf(outbuf,
      "first pixel begins at byte no. %d (counting starts at 0)",n);
if (siprint == 1) SCTPUT(outbuf);

if (minfo[4] < 10)	/* OJO: we have VERS_006 or VERS_007 binary file */
   {
   char  *xptr;

   xptr = (char *) &fcbp->NDVAL + 4;	/* point to previous CREATIM */
   for (n=0; n<16; n++) cbuf[n] = *xptr++;
   cbuf[16] = '\0';
   (void) sprintf(outbuf,
   "created on: %s - OJO: outdated format, convert to FITS!",cbuf);
   }
else
   {
   int  longt[2];		/* offset of CRETIM is 220 bytes - 
				   NOT a multiple of 8 ... */
   longt[0] = fcbp->CRETIM[0];
   longt[1] = fcbp->CRETIM[1];
   longtime = (long int *) longt;
   (void) sprintf(outbuf,"created on: %s  -  %ld seconds",
                  fcbp->CREATE,*longtime);
   }
if (siprint == 1) SCTPUT(outbuf);

naxis = fcbp->DATAINFO[0];
if (naxis == 1)
   (void) sprintf(outbuf,"Naxis = %d  Npix = %d", naxis,fcbp->DATAINFO[1]);
else if (naxis == 2)
   (void) sprintf(outbuf,"Naxis = %d  Npix = %d,%d", naxis,
                  fcbp->DATAINFO[1],fcbp->DATAINFO[2]);
else if (naxis == 3)
   (void) sprintf(outbuf,"Naxis = %d  Npix = %d,%d,%d", naxis,
                  fcbp->DATAINFO[1],fcbp->DATAINFO[2],fcbp->DATAINFO[3]); 
else
   (void) sprintf(outbuf,"Naxis = %d ", naxis);
if (siprint == 1) SCTPUT(outbuf);

key_write:
if (argc < 2)
   {
   (void) SCKWRI("MID$INFO",minfo,1,5,&unit);

   if (minfo[0] != -1)
      (void) SCKWRI("INPUTI",&isfits,19,1,&unit);
   }

SCSEPI();
return (0);			/* just to calm the compiler... */
}

