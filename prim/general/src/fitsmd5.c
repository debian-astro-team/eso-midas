/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        fitsmd5.c
.LANGUAGE     C
.AUTHOR       K. Banse   ESO/IPG
.KEYWORDS     FITS files, signature
.PURPOSE      Midas wrapper for ND's MD5 routines
.VERSION      1.00  010601:   Creation 

 090120		last modif

---------------------------------------------------------------------*/

#include <midas_def.h>
#include <string.h>

/*

*/

int main()

{
char    actio[4], infile[128], md5str[40];

int  nval;
int  update, n, unit;
int Midas_md5();


(void) SCSPRO("fitsmd5");

(void) SCKGETC("file_md5",1,120,&nval,infile);	/* get input frame   */
(void) SCKGETC("ACTION",1,2,&nval,actio);  		/* get action   */
if ((actio[0] == 'c') || (actio[0] == 'C') )	/* just compare with */
   update = 0;
else
   update = 1;

memset((void *)md5str,32,(size_t)32);

n = MID_fitstest(infile);

if (n == -2)
   SCETER(11,"no FITS file...");
else if (n != 0)
   SCETER(13,"file not found...");


(void) Midas_md5(infile,update,md5str);

memset((void *)infile,32,(size_t)80);	/* ensure clean string in OUTPUTC */
(void) strncpy(infile,md5str,32);
(void) SCKWRC("OUTPUTC",1,infile,1,80,&unit);

return SCSEPI();
}

