/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program; if not, write to the Free
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
  MA 02139, USA.

  Correspondence concerning ESO-MIDAS should be addressed as follows:
        Internet e-mail: midas@eso.org
        Postal address: European Southern Observatory
                        Data Management Division
                        Karl-Schwarzschild-Strasse 2
                        D 85748 Garching bei Muenchen
                        GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                         all rights reserved
.IDENTIFIER  logviewer
.LANGUAGE    C
.AUTHOR      K. Banse	DMD-ESO Garching
.KEYWORDS    logging, display
.PURPOSE     this module is executed via
		$ xterm -e logviewer.exe ...
	     thus at termination the xterm window disappears, 
	     => we need to put sleep() into the code, to be
	     able to read any error message

             display info in separate xterm, parallel to application
	     by displaying the contents of a given file

.VERSION     1.00       030121

 090120         last modif

------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1

#define  LIMIT_LINES  100


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

/*

*/

int main(argc,argv)
int argc;
char *argv[];

{
int fd, mm;
static int baselen, count, swindx;

char   cbuf[104];
static char  filename[168], switchy[4] = "AB ";



/* a pair of ASCII infofiles is used to display 
  (lots of) information
   written to the infofiles by the application 
   after LIMIT_LINES lines of text, the infofiles are switched
   from blablaA to blablaB and vice versa
   this limit must be synchronized with `logview_init()'
   in .../prim/general/libsrc/logview.c   */


baselen = (int)strlen(argv[1]);
if (baselen > 160)
   {
   printf(">>> file:\n%s\n>>> way to long for us...\n",argv[1]);
   sleep(5);
   exit(1);
   }

(void) strcpy(filename,argv[1]);
swindx = 0;

filename[baselen] = switchy[swindx]; filename[baselen+1] = '\0';
count = 5;


try_again:
fd = open(filename,O_RDONLY);	/* open for reading */
if (fd < 1)
   {
   if (--count > 0)
      {
      sleep(2);
      goto try_again;
      }
   printf("could not open infofile %s ...\n",filename);
   sleep(3);
   exit(1);
   }

count = 0;

input_loop:				/* use 100 bytes buffer */
mm = read(fd,cbuf,100);			/* in sync with logview.c */
if (mm >0)
      {
      if (strncmp(cbuf,"EOF",3) == 0) 		/* check for end of program */
         {
         printf("\n... terminating parallel log ...\n");
         sleep(3);
         exit(0);
         }
      printf("%s\n",cbuf); 
      count ++;

      if (count == LIMIT_LINES)
         {
         close(fd);
         mm = unlink(filename);
         if (mm != 0)
            {
            printf("we could not delete: %s\n",filename);
            sleep(5);
            exit(1);
            }

         swindx  = 1 - swindx;			/* 0 -> 1, 1 -> 0 */
         filename[baselen] = switchy[swindx];
         count = 5;
         goto try_again;
         }
      }
else
   {
   sleep(1);
   }

goto input_loop;

}
