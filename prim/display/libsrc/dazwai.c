/* @(#)dazwai.c	19.1 (ES0-DMD) 02/25/03 13:59:57 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  DAZWAI_C
.PURPOSE     get waiting time as parameter or 
             read keyword DAZHOLD(12) and wait that many milliseconds
.ALGORITHM   this routine is called from FORTRAN, so mytime is an address...
             use OSY_SLEEP
.INPUT/OUTPUT
  call as    DAZWAI_C( mytime )

  input:
         int *mytime		waiting time in miliseconds

.RETURNS     nothing
------------------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */
#define  _POSIX_SOURCE 1

#include <midas_def.h>


void DAZWAI_C( mytime )
int *mytime;

{
int  iav, knul, unit, milsecs;

if (*mytime < 0) 
   (void) SCKRDI( "DAZHOLD", 12, 1, &iav, &milsecs, &unit, &knul );
else
   milsecs = *mytime;

OSY_SLEEP(milsecs,1);
}

