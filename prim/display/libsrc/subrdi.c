/* @(#)subrdi.c	19.1 (ES0-DMD) 02/25/03 14:00:01 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT:  Copyright (c) 1994 European Southern Observatory,
                                         all rights reserved
.IDENTIFIER  SUBRDI_C
.LANGUAGE    C
.AUTHOR      K. Banse			IPG-ESO Garching
.KEYWORDS    ImageDisplay, memory board
.PURPOSE     read an image frame from Image Display 
             if requested, map data via enabled ITT
.ALGORITHM   use IDI interfaces to read memory
.INPUT/OUTPUT
  call as    SUBRDI_C( action, frame )

  input
          char *action :	= CUR, not the complete image is desired
                        	= OVE, take data from overlay channel
          char *frame  :	name of frame to be created

.RETURNS     nothing
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces

.VERSIONS    1.00       940630  from IDAUXZ.FOR    R.M.van Hees
 
 010423		last modif

------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1

#include <math.h>
#include <stdio.h>
#include <midas_def.h>

#include <idinumd.h>

#define MYMIN(a,b)   ((a) > (b) ? (b) : (a))
#define MYMAX(a,b)   ((b) > (a) ? (b) : (a))

/*

 */

void SUBRDI_C( action, ittflg, ncurs, nrpx, frame )
int  ittflg, ncurs, *nrpx;
char *action, *frame;

{
register int ii;

int    cform, chan, color, dista, imno, memo, size, statA, statB, unit, xm, ym,
       stpx[2], xyA[5], xyB[5];
float  *p_img, cuts[4];
double step[2], start[2];
char   *pntr, cunit[49], ident[73], output[81];

static int coord[4]  = { -1, -1, -1, -1 };



for (ii=0; ii<4; ii++) cuts[ii] = 0.0;			/* init cuts */


/* for ACTION = OVERLAY we have to adapt the channel no.  */

if ( strncmp( action, "OVE", 3 ) == 0 )
   chan = QOVCH;
else
   chan = QIMCH;

if ( strncmp( action, "CUR", 3 ) != 0 )
   {
   (void) DCGICH( chan );
   if ( SOURCE == 0 )                     /* from internal write memory... */
      {
      stpx[0] = 0;                          /* get all of display memory */
      stpx[1] = 0;
      nrpx[0] = QMSZX;
      nrpx[1] = QMSZY;
      }
   else
      {
      stpx[0] = SSPX;                              /* start screen pixel */
      stpx[1] = SSPY;
      nrpx[0] = NSX;                             /* no. of screen pixels */
      nrpx[1] = NSY;
      }
   memo = chan;
   (void) strcpy( ident, "ImageDisplay overlay channel" );
   }
else
   { 
   if ( ncurs == 2 )
      cform = 0;				/* define cursor shape */
   else
      cform = 3;
   color = 2;					/* and cursor color */
   SETCUR_C( QDSPNO, ncurs, cform, color, coord );
   Ccursin( QDSPNO, 0, ncurs, xyA, &statA, xyB, &statB );


/* get cursor position */

   Ccursin( QDSPNO, 1, ncurs, xyA, &statA, xyB, &statB );


/* get relevant memory board */

   if ( QRGBFL == 1 )                                       /* in rgb mode */
      memo = chan;          /* take last displayed channel as memory board */
   else
      memo = xyA[2];


/* if only one cursor involved, we have to build a rectangle
   and get lower left coordinate                               */     

   if ( ncurs < 2 )     
      {
      if ( ncurs == 0 )
         {
         xm = xyA[0];
         ym = xyA[1];
         }
      else
         {
         xm = xyB[0];
         ym = xyB[1];
         }

      dista = nrpx[0] / 2; 
      xyA[0] = MYMAX( xm - dista, SSPX );  /* make sure, it stays inside */
      if ( 2 * dista == nrpx[0] ) dista--;
      xyB[0] = MYMIN( xm + dista, SSPX + NSX - 1 );

      dista = nrpx[1] / 2;
      xyA[1] = MYMAX( ym - dista, SSPY );
      if ( 2 * dista == nrpx[1] ) dista--;
      xyB[1] = MYMIN( ym + dista, SSPY + NSY - 1 );

      coord[0] = ( xyA[0] - SCROLX ) * ZOOMX;
      coord[1] = ( xyA[1] - SCROLY ) * ZOOMY + QDSZY - 1;
      coord[2] = ( xyB[0] - SCROLX ) * ZOOMX;
      coord[3] = ( xyB[1] - SCROLY ) * ZOOMY + QDSZY - 1;


      /* show rectangle + update cursors on screen */

      SETCUR_C(QDSPNO,2,1,2,coord);    /* cursno = 2 indicates roi */
      (void) sprintf( output, "screen pixels of subframe: %8d%8d, %8d%8d",
                          xyA[0], xyA[1], xyB[0], xyB[1] );
      SCTPUT( output );
      }


   /* start point is lower left corner */

   stpx[0] = xyA[0];
   stpx[1] = xyA[1];
   nrpx[0] = xyB[0] - xyA[0] + 1;
   nrpx[1] = xyB[1] - xyA[1] + 1;
   (void) strcpy( ident, "ImageDisplay image channel" );
   }


/*  now get that image */

size = nrpx[0] * nrpx[1];
size = size / 4 + 1;               /* make sure we don't miss the last pixel */
(void) strcpy ( cunit, "screen intens.  " );
(void) strncat( cunit, "screen x-pixels ", 16 );
(void) strncat( cunit, "screen y-pixels ", 16 );
start[0] = stpx[0];
start[1] = stpx[1];
step[0] = step[1] = 1.0;


/* map the new frame + allocate virtual memory for raw pixel image */

(void) SCIPUT( frame, D_R4_FORMAT, F_O_MODE, F_IMA_TYPE, 2, nrpx, 
               start, step, ident, cunit, &pntr, &imno );
p_img = (float *) pntr;


/* and read data from ImageDisplay channel */

RIMG_C( memo, ittflg, stpx, nrpx, p_img );


/* now update LHCUTS + add descr. HISTORY */

cuts[3] = QLUTSZ - 1;
(void) SCDWRR( imno, "LHCUTS", cuts, 1, 4, &unit);
(void) CGN_DSCUPD( imno, imno, " " );
}
