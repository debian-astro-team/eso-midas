/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  LOADWN_C
.LANGUAGE    C
.AUTHOR      K. Banse			ESO - Garching
.KEYWORDS    memory, ImageDisplay
.PURPOSE     write a 2-dimensional image into image memory of Image Display 
.ALGORITHM   set resolution to only low byte transfer
             store image into predefined window in image memory
.INPUT/OUTPUT
  call as    LOADWN_C( flags, imno, npix, stapix, kpix, wsta, cuts )


  input
          int   flags[2] :	[0] init flag, < 0 for first time, > 0 later on
				    to init: -100,-99 => main display window
				    	     -i => aux_window i (i=1, 2, 3, 4)
				             FLAGS[0] is returned as -FLAG[0] 
				subsequent calls of LOADWN may change FLAGS[0]
				          e.g. from 99 to 1 and back to 99 ...
				[1] data type (as D_xx_FORMAT)
          int   imno	 :	id of image frame
          int   npix[2]	 :	no. of pixels of image frame in x- and y-dir.
          int   stapix[2]:	start pixels of window in frame
          int   kpix[2]  :      no. of frame_pixels to load
          int   wsta[4]  :      (0,1) = window start pixel
				(2,3) = scaling values (only for aux_windows!)
          float cuts[4]	 :	low + high cut in z-dir.

.RETURNS     nothing
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces

.VERSIONS  
1.00	940324	converted from LOADWN.FOR    RvH
1.10	940916  correct the parameter calling sequence

 090706		last modif
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1


#include <midas_def.h>
#include <idinumd.h>
#include <proto_II.h>

#include <stdio.h>


#ifndef TRUE
#define TRUE            1
#define FALSE           0
#endif

#define  CHUNK   16000

/*

*/

void LOADWN_C(flags,imno,npix,stapix,kpix,wsta,cuts)
int   *flags, imno, *npix, *stapix, *kpix, *wsta;
float *cuts;

{
int   ec, el, ed, flag, iav, knul, statd, unit;
int   ldspno, limch;
register int ii, xy;

float ccuts[4];

static int scalav, dispda[9], icen[4], khelp[15], scale[3];





if (*flags < 0)		/* initial call */
   {
   int  con_alw = 1, log_no = 0, dis_no = 0;       /* variables for SCECNT */

   scalav = 0;
   (void) SCECNT("GET",&ec,&el,&ed);
   (void) SCECNT("PUT",&con_alw,&log_no,&dis_no);
   statd = SCDRDI(imno,"DISPLAY_DATA",1,9,&iav,dispda,&unit,&knul);

   if (*flags == -100)		/* main window */
      {
      if (SCDRDR(imno,"LHCUTS",1,4,&iav,ccuts,&unit,&knul) != 0)
         {
         int  kimno, num, toto;

         (void) SCKRDI("MONITPAR",20,1,&iav,&num,&unit,&knul);
         num = num * num;                  /* default mapping size */
         toto = npix[0] * npix[1];
         if (flags[1] != D_R4_FORMAT)
            {
            char  name[80];

            (void) MID_RETNAM(imno,name,72);	/* get nam eof frame */
            (void) SCFCLO(imno);
            (void) SCFOPN(name,D_R4_FORMAT,0,F_IMA_TYPE,&kimno);
            MYCUTS_C("3SIGMA",1,kimno,toto,num,ccuts);
            (void) SCFCLO(kimno);
            (void) SCFOPN(name,flags[1],0,F_IMA_TYPE,&imno);
            }
         else
            MYCUTS_C("3SIGMA",1,imno,toto,num,ccuts);

         (void) SCDWRR(imno,"LHCUTS",ccuts,1,4,&unit);
         }
      cuts[0] = ccuts[0];
      cuts[1] = ccuts[1];


      /* descr. display_data not there, so we have to initialize...  */

      if (statd != 0)
         {
         dispda[0] = dispda[1] = 1;
         dispda[2] = npix[0] / 2;
         dispda[3] = npix[1] / 2;
         dispda[4] = 0;
         dispda[5] = dispda[6] = dispda[7] = dispda[8] = -1;
         (void) SCDWRI(imno,"DISPLAY_DATA",dispda,1,9,&unit);
         }

      SCALX  = dispda[0];
      SCALY  = dispda[1];
      scalav = dispda[4];
      if (dispda[5] != -1)     
         {
         icen[0] = dispda[5];
         icen[1] = dispda[6];
         icen[2] = dispda[7];
         icen[3] = dispda[8];
         }
      else
         {
         icen[0] = dispda[2];
         icen[1] = dispda[3];
         icen[2] = QMSZX / 2;
         icen[3] = QMSZY / 2;
         }
      LOADDR = 0;
      SOURCE = 1;
      }

   else
      {
      if (statd == 0) scalav = dispda[4];
/*
         (void) SCDRDI(imno,"DISPLAY_DATA",5,1,&iav,&scalav,&unit,&knul);
*/
      }

   *flags *= -1;		/* change from negative to positive flag */
   khelp[1] = CHUNK;
   khelp[2] = khelp[1]/4;
   (void) SCECNT("PUT",&ec,&el,&ed);
   }

if ((*flags > 4) && (*flags < 99)) SCETER(71,"LOADWN: invalid flags[0] ...");

khelp[0] = flags[1];                       /* get data type */
for (ii=9; ii<15; ii++) khelp[ii] = 0;

if (*flags >= 99)     
   {
   if (*flags == 99) 
      khelp[3] = 1;
   else
      khelp[3] = 0;
   ldspno  = QDSPNO;
   limch  = QIMCH;
   scale[0] = SCALX;
   scale[1] = SCALY;
   scale[2] = scalav;
   }

else
   {
   khelp[3]  = 2;
   ldspno  = ZDSPNO;
   if (ldspno < 0) SCETER(72,"LOADWN: invalid flags[0] ...");
   limch  = 0;
   scale[0] = wsta[2];
   scale[1] = wsta[3];
   scale[2] = 0;
   }

khelp[4] = kpix[0];                      /* khelp[4] = NFX */

for (xy=0; xy<2; xy++)
   {
   if (scale[xy] == 0) scale[xy] = 1;
   }

for (xy=0; xy<2; xy++)            /* khelp[5,6] = NSX, NSY */
   { 
   if (scale[xy] > 1)           
      ii = kpix[xy] / scale[xy];
   else if (scale[xy] < -1)
      ii = - scale[xy] * kpix[xy];
   else
      ii = kpix[xy];
   if (ii > 0)
      khelp[5+xy] = ii;
   else
      {
      (void) printf
      ("nopix[%d] = %d too small for current scaling - loading omitted ...\n",
       xy,kpix[xy]);
      return;
      }
   }
khelp[7]  = stapix[0];
khelp[8] = stapix[1];


/* set up transfer window (load size regs. + CMRs) */

(void) IIMSTW_C(ldspno,limch,LOADDR,khelp[5],khelp[6],QMDEP,wsta[0],wsta[1]);


/* write the image into given memory board of Image Display */

WIMGA_C(ldspno,limch,imno,khelp,LOADDR,npix,icen,cuts,scale);

if (*flags == 100)
   {
   flag = FALSE;
   if (QMSZX > QDSZX)                    /* take care of larger channels */
      {
      *npix = QMSZX / 2;
      SCROLX = *npix - QDSZX/2;
      flag = TRUE;
      }
   else
      SCROLX = 0;

   if (QMSZY > QDSZY)     
      {
      npix[1] = QMSZY / 2;
      SCROLY = npix[1] + QDSZY/2;
      flag = TRUE;
      }
   else
      SCROLY = QMSZY - 1;

   if (flag) (void) Cdazscr(QDSPNO,QIMCH,&SCROLX,&SCROLY);

   ZOOMX = 1;
   ZOOMY = 1;
   }

}

