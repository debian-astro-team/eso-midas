/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  SUBDZT_C
.LANGUAGE    C
.AUTHOR      K. Banse		ESO - IPG Garching
.KEYWORDS    LUT, ITT, Overlay tables, Image Display
.PURPOSE     read/write different tables from/to image display
             store them in a table file (for read option)
.ALGORITHM   use IDI interfaces to write/read the tables
.INPUT/OUTPUT 
  The following keywords are used:

  DAZIN/I/1/2 		section of LUT/ITT/OVR table and no. of values
                        or number of cursors
  HCITTLUT/1/40		ITT[1:20] or LUT[21:41] table name
  P3/C/1/3 		ITT_flag for GET/LUT

  call as    SUBDZT_C( action, dazhld, dsplay, table )

  input
          char *action :	G(et) or L(oad)
 			        and type of table: L, I, C, O
          int  *dazhld :
          int  dsplay  :	display number
          char *table  :	name of table file with optional column labels
                                or name of image with descriptor

.RETURNS     nothing
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces

.VERSIONS    1.00       940630  from IDAUXZ.FOR    R.M.van Hees

 090706		last modif
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1

/* definition of the used functions in this module */

#include <stdio.h>
#include <midas_def.h>
#include <fsydef.h>
#include <idinumd.h>
#include <proto_II.h>

#ifndef TRUE
#define TRUE            1
#define FALSE           0
#endif

#define MYLUTSZ		256
#define MYOVRSZ		1024

static int  tblflg;
static char tblnam[64], tbllab[64], bdfnam[64], dscnam[64];

/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  READ_ITT
.PURPOSE     read ITT from Image Display
.INPUT/OUTPUT
  call as    READ_ITT( table, dsplay, ittn, lutlen )
  
  input:
          char *table  :
          int  dsplay  :
          int  ittn    :
          int  lutlen :

.RETURNS     nothing
------------------------------*/

#ifdef __STDC__
      static void READ_ITT( int dsplay, int ittn, int lutlen )
#else
      static void READ_ITT( dsplay, ittn, lutlen )
      int   dsplay, ittn, lutlen;
#endif

{
register int nn;

int   actvals, column, imf, ncols, nrows, tbnull, tid;

float *rbuf, ritt[MYLUTSZ];

char  tfile[24];

static char *err_labl = "READ_ITT: column :ITT not found in ITT table";


ittn = 0;
if ( dsplay == 0 )
   {
   if ( QLUTSZ == MYLUTSZ )
      (void) IILRIT_C( QDSPNO, QIMCH, ittn, 0, MYLUTSZ, ritt );
   else
      {
      rbuf = (float *) osmmget( QLUTSZ * sizeof( float ));
      (void) IILRIT_C( QDSPNO, QIMCH, ittn, 0, QLUTSZ, rbuf );
      MakeITT( QLUTSZ, rbuf, MYLUTSZ, ritt );
      (void) osmmfree( (char *) rbuf );
      }
   }

else		/* if virtual device get name from HCITTLUT(1:20)  */
   {
   (void) SCKGETC( "HCITTLUT", 1, 20, &actvals, tfile  );
   nn = CGN_INDEXC(tfile,' ');
   if (nn < 0) nn = strlen(tfile);
   (void) strcat(&tfile[nn],".itt");
   OPNTBL( tfile, &tid, &ncols, &nrows );
   (void) TCLSER( tid, "ITT", &column );
   if ( column <= 0 ) SCETER( 1, err_labl );

   for (nn=0; nn<MYLUTSZ; nn++) 
      TCERDR( tid, nn, column, ritt+nn, &tbnull );
   (void) TCTCLO( tid );
   }
 

if ( tblflg )     
   CREITT( tblnam, ritt );
else
   {
   int unit = 0;

   (void) SCFOPN( bdfnam, D_OLD_FORMAT, 0, F_IMA_TYPE, &imf );
   (void) SCDWRR( imf, dscnam, ritt, 1, MYLUTSZ, &unit );
   }
}
/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  READ_LUT
.PURPOSE     read LUT from Image Display
.INPUT/OUTPUT
  call as    READ_LUT(dsplay,lutn,lutlen,format,range)
  
  input:
          int  dsplay:	flag for "real" or 'virtual" display
          int  lutn:	LUT section
          int  lutlen:	LUT size
          int  format:	flag for output format, 
			0 - Midas binary table, 9 = ASCII file
          int  *range:	output data range,
			0,1 or 0,255 (only for ASCII files)

.RETURNS     nothing
------------------------------*/

#ifdef __STDC__
      static void READ_LUT(int dsplay,int lutn,int lutlen,
                           int format, int *range)
#else
      static void READ_LUT(dsplay,lutn,lutlen,format,range)
      int   dsplay, lutn, lutlen, format, *range;
#endif

{
register int   ii, indx;
register float *pntrR, *pntrG, *pntrB;

int   nn, actvals, imf, ittn, ncols, nrows, tid, column[3], tbnull[3];

float *rbuf, data[3], ritt[MYLUTSZ], rlut[3 * MYLUTSZ];

char  ittflg[4], tfile[61], output[81];

enum { IndxR = 0, IndxG = MYLUTSZ, IndxB = MYLUTSZ + MYLUTSZ };

static char *tblabl[3] = { "RED", "GREEN", "BLUE" },
            *err_labl = "READ_LUT: column labeled %-s not found in LUT table";


if (dsplay == 0)
   {
   if ( QLUTSZ == lutlen )
      (void) IILRLT_C(QDSPNO,lutn,0,QLUTSZ,rlut);
   else
      {
      rbuf = (float *) osmmget( 3 * QLUTSZ * sizeof( float ));
      (void) IILRLT_C(QDSPNO,lutn,0,QLUTSZ,rbuf);
      MakeLUT(QLUTSZ,rbuf,lutlen,rlut);
      (void) osmmfree( (char *) rbuf );
      }

   (void) SCKGETC( "P3", 1, 3, &actvals, ittflg );
   CGN_UPSTR( ittflg );
   if ( strcmp( ittflg, "ITT" ) == 0  )     
      {
      ittn = 0;

      if ( QLUTSZ == lutlen )
         (void) IILRIT_C( QDSPNO, QIMCH, ittn, 0, QLUTSZ, ritt );
      else
         {
         rbuf = (float *) osmmget( QLUTSZ * sizeof( float ));
         (void) IILRIT_C( QDSPNO, QIMCH, ittn, 0, QLUTSZ, rbuf);
         MakeITT( QLUTSZ, rbuf, lutlen, ritt );
         (void) osmmfree( (char *) rbuf );
         }

      rbuf = (float *) osmmget( 3 * lutlen * sizeof( float ));
      for ( ii = 0; ii < 3 * lutlen; ii++ ) rbuf[ii] = rlut[ii];


      /* we use register pointers to access array RLUT */

      pntrR = rlut + IndxR;
      pntrG = rlut + IndxG;
      pntrB = rlut + IndxB;
      for ( ii = 0; ii < lutlen; ii++ )
         {
         indx = CGN_NINT( lutlen * ritt[ii] );
         *pntrR++ = rbuf[IndxR + indx];
         *pntrG++ = rbuf[IndxG + indx];
         *pntrB++ = rbuf[IndxB + indx];
         }
      (void) osmmfree( (char *) rbuf );
      }
   }

else        /* if virtual device get table name from keyword HCITTLUT(21:40) */
   {
   (void) SCKGETC( "HCITTLUT", 21, 20, &actvals, tfile  );
   nn = CGN_INDEXC(tfile,' ');
   if (nn < 0) nn = strlen(tfile);
   (void) strcat(&tfile[nn],".lut");
   OPNTBL( tfile, &tid, &ncols, &nrows );
   for (ii=0; ii<3; ii++)
      {
      (void) TCLSER( tid, tblabl[ii], column+ii );
      if ( column[ii] <= 0 )
         {
         (void) sprintf( output, err_labl, tblabl[ii] );
         SCETER( 1, output );
         }
      }


   /* we use register pointers to access array RLUT */

   pntrR = rlut + IndxR;
   pntrG = rlut + IndxG;
   pntrB = rlut + IndxB;
   for ( ii = 0; ii < lutlen; ii++ )
      {
      TCRRDR( tid, ii, 3, column, data, tbnull );
      *pntrR++ = data[0];
      *pntrG++ = data[1];
      *pntrB++ = data[2];
      }
   (void) TCTCLO( tid );
   }


/* check, if output goes to table or descriptor */

if ( tblflg )     
   CRELUT(tblnam,rlut,lutlen,format,range);
else
   {
   int  unit = 0;

   (void) SCFOPN( bdfnam, D_OLD_FORMAT, 0, F_IMA_TYPE, &imf );
   (void) SCDWRR( imf, dscnam, rlut, 1, 3 * lutlen, &unit );
   }
     
}
/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  HANDLE_OVR
.PURPOSE     write LUT from overlay file
.INPUT/OUTPUT
  call as    HANDLE_OVR( lutlen )
  
  input:
          char *table  :

.RETURNS     nothing
------------------------------*/

#ifdef __STDC__
      static void HANDLE_OVR( int lutlen )
#else
      static void HANDLE_OVR( lutlen )
      int lutlen;
#endif

{
register int nn;

int   column, ncols, nrows, tbnull, tid;
float *rovt;

static char *err_labl = "HANDLE_OVR: column :OVERLAY not found in LUT table";


/* first look for system table, then for own table */

OPNTBL( tblnam, &tid, &ncols, &nrows );

if ( *tbllab == '\0' ) (void) strcpy( tbllab, "OVERLAY" );

(void) TCLSER( tid, tbllab, &column );
if ( column <= 0 ) SCETER( 1, err_labl );

if ( lutlen > MYOVRSZ ) lutlen = MYOVRSZ;
rovt = (float *) osmmget( lutlen * sizeof( float ));
for (nn=0; nn<lutlen; nn++)
   (void) TCERDR( tid, nn, column, rovt+nn, &tbnull );


/* and send table to ImageDisplay (section = 4 <==> overlay ) */

(void) IILWLT_C( QDSPNO, 4, 0, lutlen, rovt );


/* release table file properly */

(void) TCTCLO( tid );
}
/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  HANDLE_LUT
.PURPOSE     write LUT from LUT file
.INPUT/OUTPUT
  call as    HANDLE_LUT(lutn,lutlen,format)
  
  input:
	int lutn:	LUT section
	int lutlen:	no. of values to write
	int format:	input format,
			= 0 for Midas binary table, 9 for ASCII file
.RETURNS     nothing
------------------------------*/

#ifdef __STDC__
      static void HANDLE_LUT(int lutn,int lutlen, int format)
#else
      static void HANDLE_LUT(lutn,lutlen,format)
      int lutn, lutlen, format;
#endif

{
register int   nn;
register float *pntrR, *pntrG, *pntrB;

int   ncols, nrows, tid=-1, unit=0, column[3], tbnull[3];
int   begin, ls, kk;
int   IndxR, IndxG, IndxB;

float *rbuf, data[3], rlut[3 * MYLUTSZ];

char  cbuff[61], output[81];


static char *tblabl[3] = { "RED", "GREEN", "BLUE" },
            *err_labl = "HANDLE_LUT: column labeled %-s not found in table";


if ( lutlen > MYLUTSZ ) lutlen = MYLUTSZ;
IndxR = 0;
IndxG = lutlen;
IndxB = lutlen + lutlen;


/* we use register pointers to access array RLUT */

pntrR = rlut + IndxR;
pntrG = rlut + IndxG;
pntrB = rlut + IndxB;


/* check, if ASCII file input */

if (format == 9)
   {
   int mm, idum, irange;
   float rdata[MYLUTSZ][3], range;
   double ddum;
   char *cpntr;

   tid = osaopen(tblnam,0);            /*  means reading only */
   if (tid == -1)
      (void) SCETER(123,"Could not open input file ...");

   nn = 0; 			/* read `lutlen' rows from file */
read_loop:
   mm = osaread(tid,output,80);
   if (mm > 0)
      {
      kk = CGN_INDEXC(output,',');
      if (kk <= 0)
         {		/* replace 1.blank after data values with comma */
         cpntr = output;
loop1:
         if (*cpntr == ' ')		/* skip first blanks */
            {
            cpntr ++;
            goto loop1;
            }
loop2:
         cpntr++;
         if (*cpntr == ' ')
            {
            if ((*(cpntr-1) != ',') && (*(cpntr-1) != ' ')) *cpntr = ',';
            }
         if (*cpntr != '\0') goto loop2;
         }
      kk = CGN_CNVT(output,2,3,&idum,&rdata[nn][0],&ddum);
      if (kk > 2) nn ++;
      if (nn == lutlen) goto loop3;
      }
   else if (mm < 0)
      {
      (void) osaclose(tid);
      (void) SCETER(124,"Invalid (or too few) data in input file ...");
      }
   goto read_loop;

loop3:
   irange = 1;				/* test, if range = 0,1 or 0,255 */
   for (nn=0; nn<lutlen; nn++)
      {
      if ((rdata[nn][0] > 1.) || (rdata[nn][1] > 1.) ||(rdata[nn][2] > 1.))
         {
         irange = MYLUTSZ - 1;
         break;
         }
      }
   if (irange > 1)
      {					/* normalize to [0,1]  */
      range = (float) irange;
      range = 1.0/range;
      for (nn=0; nn<lutlen; nn++)
         {
         *pntrR++ = rdata[nn][0]*range;
         *pntrG++ = rdata[nn][1]*range;
         *pntrB++ = rdata[nn][2]*range;
         }
      }
   else
      {
      for (nn=0; nn<lutlen; nn++)
         {
         *pntrR++ = rdata[nn][0];
         *pntrG++ = rdata[nn][1];
         *pntrB++ = rdata[nn][2];
         }
      }
   }

else
   {

   /* first look for own table, then for system table */

   OPNTBL( tblnam, &tid, &ncols, &nrows );

   begin = 0;
   ls = strlen(tbllab);
   for (nn=0; nn<3; nn++)
      {
      if ( *tbllab == '\0' )
         (void) strcpy( cbuff, tblabl[nn] );
      else
         {
         kk = CGN_EXTRSS(tbllab,ls,',',&begin,cbuff,20);
         if (kk < 1)
            (void) strcpy( cbuff, tblabl[nn] );
         else
            (void) strcpy( cbuff, &cbuff[1] );	/* omit the ':' */
         }
      (void) TCLSER( tid, cbuff, column+nn );
   
      if ( column[nn] <= 0 ) 
         {
         (void) sprintf( output, err_labl, cbuff );
         SCETER( 1, output );
         }
      }

   for (nn=1; nn<=lutlen; nn++)		/* read `lutlen' rows from table */
      {
      TCRRDR( tid, nn, 3, column, data, tbnull );
      *pntrR++ = data[0];
      *pntrG++ = data[1];
      *pntrB++ = data[2];
      }
   }


/* change size to `lutlen' and send the table to the ImageDisplay */

if (QLUTSZ == lutlen)
   (void) IILWLT_C(QDSPNO,lutn,0,QLUTSZ,rlut);
else
   {
   rbuf = (float *) osmmget( 3 * QLUTSZ * sizeof( float ));
   MakeLUT( lutlen,rlut,QLUTSZ,rbuf);		/* interpolate */
   (void) IILWLT_C(QDSPNO,lutn,0,QLUTSZ,rbuf);
   (void) osmmfree( (char *) rbuf );
   }

(void) SCKWRI("DAZHOLD",&lutn,10,1,&unit);	/* LUT sect. -> DAZHOLD(10) */

if (format == 9)		/* release table or ASCII file properly */
   (void) osaclose(tid);
else
   (void) TCTCLO(tid);
}
/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  HANDLE_ITT
.PURPOSE     write ITT from ITT file
.INPUT/OUTPUT
  call as    HANDLE_ITT( ittn )
  
  input:
          char *table  :

.RETURNS     nothing
------------------------------*/

#ifdef __STDC__
      static void HANDLE_ITT(int ittn, int ittlen )
#else
      static void HANDLE_ITT(ittn,ittlen)
      int ittn, ittlen;
#endif

{
register int   nn;

int   column, ncols, nrows, tbnull, tid;
float *rbuf, ritt[MYLUTSZ];

static char *err_labl = "HANDLE_ITT: column :ITT not found in table";



OPNTBL( tblnam, &tid, &ncols, &nrows );

if ( *tbllab == '\0' ) (void) strcpy( tbllab, "ITT" );

(void) TCLSER( tid, tbllab, &column );
if ( column <= 0 ) SCETER( 1, err_labl );

if (ittlen > MYLUTSZ) ittlen = MYLUTSZ;

for (nn=0; nn<ittlen; nn++)
   (void) TCERDR(tid,nn+1,column,ritt+nn,&tbnull);


/* send the table to the ImageDisplay */

ittn = 0;
if (ittlen == QLUTSZ)
   (void) IILWIT_C(QDSPNO,QIMCH,ittn,0,QLUTSZ,ritt);
else
   {
   rbuf = (float *) osmmget( QLUTSZ * sizeof( float ));
   MakeITT(ittlen,ritt,QLUTSZ,rbuf);
   (void) IILWIT_C(QDSPNO,QIMCH,ittn,0,QLUTSZ,rbuf);
   (void) osmmfree( (char *) rbuf );
   }

(void) TCTCLO(tid);			/* release table file properly */
}
/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  WRITE_TBL
.PURPOSE     handle writing of tables to ImageDisplay
.INPUT/OUTPUT
  call as    WRITE_TBL( action, dsplay, lutn, lutlen )
  
  input:
          char *action :
          int  dsplay  :
          int  lutn    :
          int  lutlen  :

.RETURNS     nothing
------------------------------*/
#ifdef __STDC__

      static void WRITE_TBL(char *action,int dsplay,int lutn,int lutlen,
                            int format)
#else
      static void WRITE_TBL(action,dsplay,lutn,lutlen,format)
      char *action;
      int  dsplay, lutn, lutlen, format;
#endif

{
register int nn, indx;

int   actvals, cursno, imf, knul, unit, coord[4];
float *rbuf, ritt[MYLUTSZ], rlut[3 * MYLUTSZ];

if ( action[1] == 'C')     
   { (void) SCKRDI( "DAZIN", 1, 1, &actvals, &cursno, &unit, &knul );


   /* load the cursor table into Display via SETCUR (which calls IICINC) */

   (void) SCKWRC( "IN_A", 1, tblnam, 1, 60, &unit );
   for (nn=0; nn<4; nn++) coord[nn] = -1;

   SETCUR_C( dsplay, cursno, 0, 99, coord );


   /* store cursor no. in DAZHOLD[0] */

   (void) SCKWRI( "DAZHOLD", &cursno, 1, 1, &unit );
   }

else 
   {
   if ( action[1] == 'O' )                               /* handle overlay */
      { 
      if (  strstrs( tblnam, ".ovr" ) == NULL )
         {
         if ( (indx = strlen( tblnam )) > 4 )
            {
            while( indx > 0 && tblnam[--indx] != '.' )
               if ( indx == 0 )
                  (void) strcat( tblnam, ".ovr" );
               else
                  (void) strcpy( tblnam + indx, ".ovr" );
            }
         }
      HANDLE_OVR( lutlen );
      }

   else if ( action[1] == 'I' )                          /* handle ITT     */
      {
      if ( ! tblflg )                                  /*     from image */
         {
         (void) SCFOPN(bdfnam,D_OLD_FORMAT,0,F_IMA_TYPE,&imf);
         (void) SCDRDR(imf,dscnam,1,MYLUTSZ,&actvals,ritt,&unit,&knul) ;
         (void) SCFCLO( imf );
         if ( dsplay == -1 )     
            CREITT( bdfnam, ritt );
         else
            {
            lutn = 0;


            /* shrink to QLUTSZ and send the table to the ImageDisplay */

            if ( MYLUTSZ == QLUTSZ )
               (void) IILWIT_C( QDSPNO, QIMCH, lutn, 0, QLUTSZ, ritt );
            else
               {
               rbuf = (float *) osmmget( QLUTSZ * sizeof( float ));
               MakeITT( MYLUTSZ, ritt, QLUTSZ, rbuf );
               (void) IILWIT_C( QDSPNO, QIMCH, lutn,0, QLUTSZ, rbuf);
               (void) osmmfree( (char *) rbuf );
               }
            }
         }
      else if ( dsplay != -1 )                         /*     from table */
         {
         if (  strstrs( tblnam, ".itt" ) == NULL )
            {
            indx = strlen( tblnam );
            while( indx > 0 && tblnam[--indx] != '.' );
            if ( indx == 0 )
               (void) strcat( tblnam, ".itt" );
            else
               (void) strcpy( tblnam + indx, ".itt" );
            }
         HANDLE_ITT(lutn,lutlen);
         }
      }

   else                                                  /* handle LUT     */
      {
      if ( ! tblflg )                                  /*     from image */
         {
         (void) SCFOPN(bdfnam,D_OLD_FORMAT,0,F_IMA_TYPE,&imf);
         (void) SCDRDR(imf,dscnam,1,3*MYLUTSZ,&actvals,rlut,&unit,&knul) ;
         (void) SCFCLO( imf );
         if ( dsplay == -1 )
            {
            int  dummy=0;
            CRELUT(bdfnam,rlut,MYLUTSZ,0,&dummy);
            }
         else
            {


            /* change size to LUTLEN and send the table to the ImageDisplay */

            if ( QLUTSZ == lutlen )
               (void) IILWLT_C( QDSPNO, lutn, 0, lutlen, rlut );
            else
               {
               rbuf = (float *) osmmget( 3* QLUTSZ* sizeof( float ));
               MakeLUT( lutlen, rlut, QLUTSZ, rbuf );
               (void) IILWLT_C( QDSPNO, lutn, 0, QLUTSZ, rbuf );
               (void) osmmfree( (char *) rbuf );
               }
            }
         }
      else if ( dsplay != -1 )
         {
         if (format == 0)
            {
            indx = CGN_JNDEXS(tblnam,".lut");		/* start from back */
            if (indx < 1)
               {
               for (indx=strlen(tblnam); indx>0; indx--)
                  {
                  if (tblnam[indx] == '.') goto doLUT;
                  if (tblnam[indx] == FSY_DIREND) 
                     {
                     (void) strcpy(tblnam+indx+1,".lut");
                     goto doLUT;
                     }
                  } 
               (void) strcat(tblnam,".lut");
               }
            }
doLUT:
         HANDLE_LUT(lutn,lutlen,format);
         }
      }
   }
}
/*

*/

void SUBDZT_C( action, dazhld, dsplay, table )
char *action, *table;
int  dsplay, *dazhld;

{
int actvals, iav, knul, lutn, lutlen, unit, inputi[5], format, range[2];



/* Output maybe: table[,:label(s)]  or  image,descr
   Store table name & labels or image name & descriptor in global variables */

iav = CGN_INDEXS(table,",:");
if (iav > 0)
   {
   tblflg = TRUE;                         /* show that tables are involved */
   (void) strncpy(tblnam,table,iav);
   tblnam[iav] = '\0';
   (void) strcpy(tbllab,&table[iav+1]);
   }
else
   {
   iav = CGN_INDEXC(table,',');
   if (iav > 0)
      {
      tblflg = FALSE;              /* show that image,descr are involved */
      (void) strncpy(bdfnam,table,iav);
      bdfnam[iav] = '\0';
      (void) strcpy(dscnam,&table[iav+1]);
      }
   else
      {
      tblflg = TRUE;
      (void) strcpy(tblnam,table);
      *tbllab = '\0';
      }
   }


/* Get table section + no. of values */

(void) SCKRDI("DAZIN",1,5,&actvals,inputi,&unit,&knul);
lutn = inputi[0];
lutlen = inputi[1]; 
format = inputi[2];


/* branch according to option */

if ( *action == 'L' ) 
   WRITE_TBL(action,dsplay,lutn,lutlen,format);
else 
   {
   range[0] = inputi[3];
   range[1] = inputi[4];

   if ( action[1] == 'I' )                               /* read ITT table */
      READ_ITT(dsplay,lutn,lutlen);
   else                                                  /* read LUT table */
      READ_LUT(dsplay,lutn,lutlen,format,range);
   }
}

