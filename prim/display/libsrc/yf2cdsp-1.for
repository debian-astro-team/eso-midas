C===========================================================================
C Copyright (C) 1995-2005 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  YF2CDSP.FOR +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module YF2CDSP.FOR
C  AUTHOR         K. Banse
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 050728:  created 
C 
C 050803	last modif
C 
C -----------------------------------------------------------------------------
C
      SUBROUTINE BLDGRA(SHAPE,COORDS,ARCS,XFIG,YFIG,FIGMAX,NOP)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   SHAPE
      INTEGER     COORDS(*),XFIG(*),YFIG(*),FIGMAX,NOP
      REAL        ARCS(*)
C 
      CALL YDSP1(SHAPE,COORDS,XFIG,YFIG,0)
      CALL YDSP2(ARCS,FIGMAX,NOP)
C
      RETURN
      END

      SUBROUTINE CURSIN(DSPLAY,IACT,NOCURS,XYA,MCA,ISCA,XYB,MCB,ISCB)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY, IACT, NOCURS, XYA, MCA, ISCA, XYB, MCB, ISCB
C
      CALL YDSP1('dum',DSPLAY, IACT, NOCURS,0)
      CALL YDSP3(XYA, MCA, ISCA, XYB, MCB, ISCB)
C
      RETURN
      END

      SUBROUTINE GETCUR(ACTION,FRAME,ICUR1,FP1,WC1,VAL1,STAT1,
     +                         ICUR2,FP2,WC2,VAL2,STAT2)
C
      IMPLICIT NONE
C
      CHARACTER*(*) ACTION, FRAME
      INTEGER     ICUR1,STAT1,ICUR2,STAT2
      REAL        FP1(*), FP2(*), WC1(*), WC2(*), VAL1, VAL2
C
      CALL YDSP1(ACTION,ICUR1,STAT1,ICUR2,STAT2)
      CALL YDSP1a(FP1,WC1,VAL1,FP2,WC2,VAL2)
      CALL YDSP4(FRAME)
C
      RETURN
      END

      SUBROUTINE WRITT(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN,NITT,ISTA,COUNT,IDST
      REAL        RITT(*)
C
      CALL YDSP1('dummy',DSPLAY,CHAN,NITT,ISTA)
      CALL YDSP5(COUNT,RITT,IDST)
C
      RETURN
      END

      SUBROUTINE RDITT(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN,NITT,ISTA,COUNT,IDST
      REAL        RITT(*)
C
      CALL YDSP1('dummy',DSPLAY,CHAN,NITT,ISTA)
      CALL YDSP6(COUNT,RITT,IDST)
C
      RETURN
      END







