/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  JOYSTK_C
.LANGUAGE    C
.AUTHOR      K. Banse		IPG, ESO Garching
.KEYWORDS    ImageDisplay, joystick
.PURPOSE     read the joystick coordinates
.ALGORITHM   use IDI routines
.INPUT/OUTPUT
    call as  stat = JOYSTK_C( dsplay, iact, nocurs, jxdis, jydis )

  input:
         int  dsplay :		device id
         int  iact   :		0 = for setting it all up
				1 = for getting the joystick displacement
         int  nocurs :	        cursor no. 0 or 1

  output:
         int *jxdis  :		X joystick displacement in [-128,+127]
         int *jydis  :		Y joystick displacement in [-128,+127]

.RETURNS     return status:    =  0 continue
                               =  1 exit
                               = -1 ENTER button pushed (with a cursor on)
                               = -2 ?

.COMMENTS    this routine has to be synchronized with CURSIN.FOR  !!!
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces

.VERSIONS    1.00       940517  f2c, converted from JOYSTICK.FOR   RvH
 
 090706		last modif
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1

#include <idi.h>
#include <midas_def.h>
#include <proto_II.h>

#include <stdio.h>

/*

*/

int JOYSTK_C( dsplay, iact, nocurs, jxdis, jydis )
int dsplay, iact, nocurs, *jxdis, *jydis;

{
int intype, intid, mycurs, oper, objty, objid, trgsta[MAX_TRG];
int stat = 0;
int exttrg = 0;                                 /* trigger no. 0 for exit  */
int enttrg = 1;                                 /* trigger no. 1 for enter */
int exetrg = 2;                                 /* trigger no. 2 for execute */



mycurs = (nocurs == 1) ? 1 : 0;

if (iact == 0)     /* bind joystick interaction + use EXIT trigger to stop */
   {
   intype = 0;              /* interactor type */
   intid = mycurs;         /* interactor id: 0 or 1 */
   objty = 0;              /* object type  : no visible effect */
   objid = 0;              /* object id */
   oper = 0;              /* interactive operation */

   (void) IIIENI_C( dsplay, intype, intid, objty, objid, oper, exttrg );

   /* bind ENTER trigger + also use EXTTRG to stop */

   intype = 5;              /* trigger */
   intid = enttrg;         /* trigger id  (= 1 in our case) */
   (void) IIIENI_C(dsplay,intype,intid,objty,objid,oper,exttrg);

   /* finally bind also EXECUTE trigger to EXIT trigger */

   intid  = exetrg;         /* trigger id  = 2 */
   (void) IIIENI_C(dsplay,intype,intid,objty,objid,oper,exttrg);

   /* just to clear offsets */

   (void) IIIGLD_C(dsplay,mycurs,jxdis,jydis);
   }

else                    /* and here we read Locator + status of ENTER button */
   {
   (void) IIIEIW_C(dsplay,trgsta);
   if (*trgsta == 1)
      stat = 1;             /* terminated ... */
   else 
      {
      if (trgsta[1] == 1)     
         stat = -1;
      else if (trgsta[2] == 1)     
         stat = -2;
      (void) IIIGLD_C(dsplay,mycurs,jxdis,jydis);
      }
   }

return stat;
}

/*

*/

/*++++++++++++++++++++++++++++++
.PURPOSE  write cursor output to standard output
------------------------------*/

#ifdef __STDC__
   void WR_SCREEN(int *flag,char *cursfl,int ncurs,int circfl, 
                         int radec,float *xyinfoA,float *xyinfoB,char *labl)
#else
   void WR_SCREEN(flag,cursfl,ncurs,circfl,radec,xyinfoA,xyinfoB,labl)
   int   *flag;			/* IN/OUT: action flag */
   int   ncurs;			/* IN: no. of cursors */
   int   circfl;		/* IN: 1 = circle, 0 = no circle */
   int   radec;			/* IN: 1 = RA + DEC as output (WCS) */
   float *xyinfoA;		/* OUT: cursor info of cursor #0 (mouse) */
   float *xyinfoB;              /* OUT: cursor info of cursor #1 (arrows) */
   char  *cursfl;		/* IN: the action flag from GetCursor */
   char  *labl;			/* IN: label to be displayed */
#endif

{
int  ibuff[3];
register int ii;

double  dd;

char output[82], sexa[16], sexb[16];

void form_sexa();



/* initialized variables */

char *fmt_dis = "%8.0f %8.0f  %#12.6g%#12.6g  %#12.6g  ";
char *fmt_disr = "%8.0f %8.0f     %s   \t%s %#12.6g  ";
char *info_labl = 
"     frame pixels           world coords        intensity ";
char *info_lablr = 
"     frame pixels         RA               DEC           intensity ";
     


/* Display header line here, because GETCUR writes the file name */

if (*flag == 1)     
   {
   *flag = 0;
   (void) strcpy(output,"cursor #0 ");
   if ( cursfl[3] == 'Z')     
      {
      (void) SCTDIS(output,80);                  /* write + erase ... */
      (void) SCTDIS(output,-9);
      }

   if ( circfl )
      {
      SCTPUT( output );
      if (radec == 1)
         SCTPUT(info_lablr);
      else
         SCTPUT(info_labl);
      (void) strcpy(output,"     Radius1  ");
      if (xyinfoB[3] > 0.0) (void) strcat(output,"Radius2  ");
      if (xyinfoB[4] > 0.0) (void) strcat(output,"Radius3  ");
         SCTPUT(output);
      }
   else
      {
      if (ncurs > 0) (void) strcpy(output,"cursor #0 + #1");
      SCTPUT(output);
      if (radec == 1)
         SCTPUT(info_lablr);
      else
         SCTPUT(info_labl);
      }
   }

if (*flag == 0)
   {
   if (ncurs == 2)
      {
      if (! circfl) SCTPUT(" ");
      if (radec == 1)
         {
         dd = (double) xyinfoA[4];
         form_sexa(1,dd,sexa);
         dd = (double) xyinfoA[5];
         form_sexa(0,dd,sexb);
         (void) sprintf(output,fmt_disr,xyinfoA[2],xyinfoA[3],sexa,sexb,
                        xyinfoA[6]);
         }
      else
         (void) sprintf(output,fmt_dis,xyinfoA[2],xyinfoA[3], 
                                       xyinfoA[4],xyinfoA[5],xyinfoA[6]);
      if (*labl != '\0') (void) strcat(output,labl);
      SCTPUT(output);

      if (circfl)     
         {
         for (ii=0; ii<3; ii++)
            ibuff[ii] = CGN_NINT(xyinfoB[ii+2]);
         (void) sprintf(output,"%4d",ibuff[0]);
         if (ibuff[1] > 0) 
            (void) sprintf(output,"%s%4d",output,ibuff[1]); 
         if (ibuff[2] > 0) 
            (void) sprintf(output,"%s%4d",output,ibuff[2]); 
         }
      else
         {
         if (radec == 1)
            {
            dd = (double) xyinfoB[4];
            form_sexa(1,dd,sexa);
            dd = (double) xyinfoB[5];
            form_sexa(0,dd,sexb);
            (void) sprintf(output,fmt_disr,xyinfoB[2],xyinfoB[3],sexa,sexb,
                           xyinfoB[6]);
            }
         else
            (void) sprintf(output,fmt_dis,xyinfoB[2],xyinfoB[3], 
                           xyinfoB[4],xyinfoB[5],xyinfoB[6]);
         }
      }
   else
      {
      if (radec == 1)
         {
         dd = (double) xyinfoA[4];
         form_sexa(1,dd,sexa);
         dd = (double) xyinfoA[5];
         form_sexa(0,dd,sexb);

         (void) sprintf(output,fmt_disr,xyinfoA[2],xyinfoA[3],sexa,sexb,
                        xyinfoA[6]);
         }
      else
         (void) sprintf(output,fmt_dis,xyinfoA[2],xyinfoA[3], 
                                       xyinfoA[4],xyinfoA[5],xyinfoA[6]);
      if (*labl != '\0') (void) strcat( output, labl );
      }

   if ( cursfl[3] == 'Z' )
      SCTDIS( output, 80 );
   else
      SCTPUT( output );
   }
}
