/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE  C
.AUTHOR    Richard van Hees                        ESO - Garching
.IDENTIFICATION  Module f2cdsp.fc
.PURPUSE   fortran to C interfaces for low level display routines
.VERSION   [1.00]  940327

 090702		last modif
---------------------------------------------------------*/

#include <midas_def.h>
#include <idinumd.h>
#include <proto_II.h>
#include <stdlib.h>

int  mm;

char *ptr1, *ptr2;
char *loc_pntr();
char *strp_pntr();

#define LUTSIZE 256
#define MAXDIM  3

/*

*/

int tstcolor();
int GetScrPix();
void Plox();


/***  colo = tstcolor(cbuf) ***/

ROUTINE DAZ1(colo)
int  *colo;

{
ptr1 = strp_pntr(1);         /* get stripped string of "cbuf" */

*colo = tstcolor(ptr1);
return 0;
}

/***  Alptxec(cbuf,xp,yp,colo) ***/

ROUTINE DAZ2(xp,yp,colo)
int  *xp;
int  *yp;
int  *colo;

{
ptr1 = strp_pntr(1);         /* get stripped string of "cbuf" */

Alptxec(ptr1,*xp,*yp,*colo);
return 0;
}


/***  Alptext(cbuf,na,nb,colo) ***/

ROUTINE DAZ3(na,nb,colo)
int  *na;
int  *nb;
int  *colo;

{
ptr1 = strp_pntr(1);         /* get stripped string of "cbuf" */

Alptext(99,ptr1,*na,*nb,*colo);
return 0;
}

ROUTINE AUXHLP(flag)
int  *flag;

{
auxhelp(*flag);
return 0;
}

ROUTINE AUXWND(flag,info,xya,xyb,stat)
int  *flag;
int  *info;
int  *xya;
int  *xyb;
int  *stat;

{
*stat = Cauxwnd(*flag,info,xya,xyb);
return 0;
}

ROUTINE SCRPIX(imno,kwc,rval,kxy,scrpix,stat) 
int  *imno;
int  *kwc;
float   *rval;
int  *kxy;
float   *scrpix;
int  *stat;

/* 
imno = image id (IN), kwc = 0/1 for rval = f.p./w.c. value
kxy = 1/2/3 for using x/y/(x+y)/2 screen distance
scrpix = screen pixel value corresponding to  rval 
*/

{
*stat = GetScrPix(*imno,*kwc,*rval,*kxy,scrpix);
return 0;
}

/*** buildgra(shape,coords,arcs,xfig,yfig,figmax,nop) ***/

ROUTINE DAZ4(coords,arcs,xfig,yfig,figmax,nop)
int  *coords;
float   *arcs;
int  *xfig;
int  *yfig;
int  *figmax;
int  *nop;

{
ptr1 = strp_pntr(1);         /* get stripped string of "shape" */

buildgra(ptr1,coords,arcs,xfig,yfig,*figmax,nop);
return 0;
}

ROUTINE CONCHA(dsplay,chan,grflag,value)
int *dsplay;
int *chan;
int *grflag;
int *value;

{
CONCHA_C(*dsplay,*chan,*grflag,*value);
return 0;
}


/***Ccursin(dsplay,iact,nocurs,xya,mca,isca,xyb,mcb,iscb) ***/

ROUTINE CURSIN(dsplay,iact,nocurs,xya,mca,isca,xyb,mcb,iscb)
int *dsplay;
int *iact;
int *nocurs;
int  *xya;
int  *mca;
int  *isca;
int  *xyb;
int  *mcb;
int  *iscb;

{
int  unit, ik[4], kxya[5], kxyb[5];
register int  nr;

for (nr=0; nr<5; nr++)
   {
   kxya[nr] = 0;
   kxyb[nr] = 0;
   }

Ccursin( *dsplay, *iact, *nocurs, kxya, isca, kxyb, iscb );

xya[0] = kxya[0];
xya[1] = kxya[1];
*mca = kxya[2];

xyb[0] = kxyb[0];
xyb[1] = kxyb[1];
*mcb = kxyb[2];

if ((*isca != 0) || (*iscb != 0))
   {
   ik[0] = kxya[3];
   ik[1] = kxya[4];
   ik[2] = kxyb[3];
   ik[3] = kxyb[4];

   (void) SCKWRI("CURSOR",ik,1,4,&unit);	/* save screen coords. */
   }

return 0;
}

ROUTINE DAZVIS( dsplay, chanl, flag, vis )
int  *dsplay;
int  *chanl;
int  *flag;
int  *vis;

{
(void) Cdazvis( *dsplay,* chanl, *flag, *vis );
return 0;
}

ROUTINE DAZSCR( dsplay, chanl, scrx, scry, stat )
int  *dsplay;
int  *chanl;
int  *scrx;
int  *scry;
int  *stat;

{
*stat = Cdazscr(*dsplay,*chanl,scrx,scry);
return 0;
}

ROUTINE DAZZSC( dsplay, chanl, zoom, scrx, scry, stat )
int  *dsplay;
int  *chanl;
int  *zoom;
int  *scrx;
int  *scry;
int  *stat;

{
*stat = Cdazzsc( *dsplay, *chanl, *zoom, scrx, scry );
return 0;
}


/*** GETCUR(action,frame,icur1,fp1,wc1,val1,stat1,icur2,fp2,wc2,val2,stat2) ***/

ROUTINE DAZ5(icur1,fp1,wc1,val1,stat1,icur2,fp2,wc2,val2,stat2)

/* action = char string with action code (input) 
   frame = char string to hold displayed image name (in/output)
   for details see description in getcur.c */

int  *icur1;  /* screen cursor coords (OUT)*/
float  *fp1; /* = f.p. at cursor pos. (OUT) */
float  *wc1; /* = w.c. at cursor pos. (OUT)*/
float  *val1; /* = value a.c.p. (OUT) */
int    *stat1; /* = status of cursor (which keys pressed)*/
int  *icur2;
float  *fp2;
float  *wc2; 
float  *val2;
int    *stat2;
   
{
int  n, kk;
float xya[7], xyb[7];
char  *work, myframe[80];


ptr1 = strp_pntr(1);         /* get stripped string of "action" */
ptr2 = loc_pntr(1,&mm);      /* get location of "frame" and its length */
if (mm > 80) 
   kk = 80;	     /* for GetCursor max 80 char. string */
else
   kk = mm;

if (*ptr2 == ' ')
   {
   myframe[0] = ' ';
   myframe[1] = '\0';
   }
else
   {			/* we expect a char string with ' ' in the end */
   work = ptr2;
   for (n=0; n<(kk-1); n++)	/* at most 79 chars */
      {
      if (*work == ' ')
         {
         myframe[n] = '\0';
         goto next_step;
         }
      myframe[n] = *work++;
      }

   myframe[79] = '\0';		/* last element of myframe */
   }
 
next_step:
GetCursor(ptr1,myframe,xya,stat1,xyb,stat2);

if (*stat1 != 0) 
   {
   icur1[0] = (int) (xya[0]+0.5); icur1[1] = (int) (xya[1]+0.5);
   fp1[0] = xya[2]; fp1[1] = xya[3];
   wc1[0] = xya[4]; wc1[1] = xya[5];
   *val1 = xya[6];
   icur2[0] = (int) (xyb[0]+0.5); icur2[1] = (int) (xyb[1]+0.5);
   fp2[0] = xyb[2]; fp2[1] = xyb[3];
   wc2[0] = xyb[4]; wc2[1] = xyb[5];
   *val2 = xyb[6];
   }

n = (int) strlen(myframe);
if (n > 0) 
   {
   if (n < mm)				/* string + ' ' as endmarker fit */
      {
      (void) strcpy(ptr2,myframe);
      *(ptr2+n) = ' ';
      }
   else
      {					/* no space for end marker... */
      (void) strncpy(ptr2,myframe,(size_t)mm);
      }
   }
else
   *ptr2 = ' ';

return 0;
}

/***  GETSTR( outstr, dim )  ***/

ROUTINE DAZ6(dim)
int  *dim;

{
int   n;


ptr1 = loc_pntr(1,&mm);      /* get location of "outstr" */

Cgetstr(ptr1,dim);

n = (int) strlen(ptr1);
if ((n > 0) && (n < mm)) *(ptr1+n) = ' ';

return 0;
}

ROUTINE HSIRGB( flag, hsi, rgb )
int  *flag;
float   *hsi;
float   *rgb;

{
HSIRGB_C(*flag,hsi,rgb);
return 0;
}

ROUTINE JOYSTK( dsplay, iact, nocurs, jxdis, jydis, stat )
int *dsplay;
int *iact;
int *nocurs;
int *jxdis;
int *jydis;
int *stat;

{
*stat = JOYSTK_C( *dsplay, *iact, *nocurs, jxdis, jydis );
return 0;
}

ROUTINE LOADWN( flags, imno, npix, stapix, kpix, wsta, cuts )
int  *flags;
int  *imno;
int  *npix;
int  *stapix;
int  *kpix;
int  *wsta;
float   *cuts;

{
LOADWN_C( flags, *imno, npix, stapix, kpix, wsta, cuts );
return 0;
}

ROUTINE MAKITT(icount,ritt,ocount,oitt)
int *icount;
float  *ritt;
int *ocount;
float  *oitt;

{
MakeITT(*icount,ritt,*ocount,oitt);
return 0;
}

static void mak1(ic,mlut,qlut)
int   ic;
float *mlut, *qlut;

{
register int  jin, jout, jouta, joutb;

jout = 0;
jouta = ic;
joutb = jouta + jouta;

for (jin=0; jout<ic; jin+=3)
   {
   mlut[jout++] = qlut[jin];
   mlut[jouta++] = qlut[jin+1];
   mlut[joutb++] = qlut[jin+2];
   }
}

static void mak2(oc,mlut,qlut)
int   oc;
float *mlut, *qlut;

{
register int  jin, jout, jouta, joutb;

jout = 0;
jouta = oc;
joutb = jouta + jouta;
for (jin=0; jout<oc; jin+=3)
   {
   qlut[jin] = mlut[jout++];
   qlut[jin+1] = mlut[jouta++];
   qlut[jin+2] = mlut[joutb++];
   }
}

ROUTINE MAKLUT( flag, icount, rlut, ocount, olut )
int *flag;                   /* IN: 1= send to device, 2= get from device */
int *icount;
float  *rlut;
int *ocount;
float  *olut;

{
float  mylut[3*LUTSIZE];


/* 
  r1 g1 b1 r2 g2 b2 ... rN gN bN   =>   r1 ... rN g1 ... gN b1 ... bN  
*/

if ( *flag == 1 )
   {
   mak1(*icount,mylut,rlut);
   MakeLUT(*icount,mylut,*ocount,olut);
   }

/*
  r1 ... rN g1 ... gN b1 ... bN   =>   r1 g1 b1 r2 g2 b2 ... rN gN bN
*/

else
   {
   MakeLUT(*icount,rlut,*ocount,mylut);
   mak2(*ocount,mylut,olut);
   }
return 0;
}

ROUTINE PLOHI(ino)
int *ino;

{
Plox(*ino);
return 0;
}

ROUTINE DAZ8(dsplay,chan,nitt,ista,count,ritt,idst)
int *dsplay;
int *chan;
int *nitt;
int *ista;
int *count;
float  *ritt;
int *idst;

{
int mysta = *ista - 1;              /*  1,... -> 0,... */

*idst = IILRIT_C(*dsplay,*chan,*nitt,mysta,*count,ritt);

return 0;
}

ROUTINE RDLUT(dsplay,nlut,ista,count,rlut,idst)
int *dsplay;
int *nlut;
int *ista;
int *count;
int *idst;
float    *rlut;

{
int mysta = *ista - 1;              /*  1,... -> 0,... */

*idst = IILRLT_C(*dsplay,*nlut,mysta,*count,rlut);
return 0;
}

ROUTINE REFOVR(stat)
int  *stat;

{
*stat = 0;
Crefrovr();
return 0;
}

ROUTINE SPLCNT( splcx, splcy )
int (*splcx)[5];
int (*splcy)[5];

{
SPLCNT_C( splcx, splcy );
return 0;
}

ROUTINE SETCUR( dsplay, cursno, forma, colo, coords, stat )
int *dsplay;
int *cursno;
int *forma;
int *colo;
int *coords;
int *stat;

{
*stat = 0;
SETCUR_C( *dsplay, *cursno, *forma, *colo, coords );
return 0;
}

static int pxx(flag,cb,rbuff,dbuf,tbuf)
int  flag;
char *cb;
float *rbuff;
double *dbuf, *tbuf;

{

if (flag == 1)
   {
   if ((cb[0] == 'I') && (cb[1] == 'N'))		/* action = INIT */
      return (1);

   else
      {
      dbuf[0] = rbuff[0];
      dbuf[1] = rbuff[1];
      tbuf[0] = rbuff[2];		/* for security - maybe not needed... */
      tbuf[1] = rbuff[3];
      }
   }

else
   {
   rbuff[2] = dbuf[0];
   rbuff[3] = dbuf[1];
   rbuff[4] = tbuf[0];
   rbuff[5] = tbuf[1];
   }

return (0);
}

/*  OJO: this routine works only for 1dim or 2dim frames */
/*** PIXXCV (cflag,imno,rbuff,stat) ***/

ROUTINE DAZ9(imno,rbuff,stat)
int    *imno;
float     *rbuff;
int    *stat;

/* cflag = "WRS", ... like explained in pixcnv.c (IN), imno = image id (IN)
   rbuff = 6 elem buffer, rbuff[0,1] "are" dd1 in Pixconv() (IN)
   rbuff[2,3] are dd2 (OUT) , rbuff[4,5] are dd3 (OUT) */

{
int  ipxx;
double dbuf1[MAXDIM], dbuf2[MAXDIM], dbuf3[MAXDIM];

ptr1 = strp_pntr(1);      /* get stripped string of "cflag" */

ipxx = pxx(1,ptr1,rbuff,dbuf1,dbuf2);
if (ipxx == 1)
   {
   *stat = Pixconv("INIT",*imno,dbuf1,dbuf2,dbuf3);
   if (*stat == -1) *stat = 0;				/* FORTRAN wants 0 */
   }
else
   {
   *stat = Pixconv(ptr1,0,dbuf1,dbuf2,dbuf3);
   if (*stat == 0) 
      (void) pxx(2,"RES",rbuff,dbuf2,dbuf3);		/* store results */
   }
return 0;
}

ROUTINE WALPHB(chan,flag)
int  *chan;
int  *flag;

{
*flag = 0;
Alphamem( *chan );
return 0;
}


ROUTINE DAZ7(dsplay,chan,nitt,ista,count,ritt,idst)
int  *dsplay;
int  *chan;
int  *nitt;
int  *ista;
int *count;
int *idst;
float  *ritt;

{
int mysta = *ista - 1;              /*  1,... -> 0,... */

*idst = IILWIT_C(*dsplay,*chan,*nitt,mysta,*count,ritt);
return 0;
}

ROUTINE WRLUT(dsplay,nlut,ista,count,rlut,idst)
int *dsplay;
int *nlut;
int *ista;
int *count;
int *idst;
float    *rlut;

{
int mysta = *ista - 1;              /*  1,... -> 0,... */

*idst = IILWLT_C(*dsplay,*nlut,mysta,*count,rlut);
return 0;
}

ROUTINE K1PACK(rbuf,ibuf,aux,faux,ldata,outaux)
float *rbuf;            /* IN: float image data  */
int *ibuf;           /* IN: int image data  */
int *aux;            /* IN: auxiliary info array:    
                               data type flag (1-R4,2-I4)
                               offset in input data    
                               size of above          
                               scaling factor        
                               scaling_flag, = 0 (no), = 1 (yes scale)  */
float *faux;            /* IN: auxiliary real info array:    
                               factor to map into [0,outmax]
                               artificial minimum and maximum of image data  */
unsigned char *ldata;   /* OUT: scaled line with pixel in byte  */
int *outaux;         /* IN: max. output value (<= 255)  
                               offset in pixel array      */

{
char  *cpntr;


if (aux[0] == 2)
   cpntr = (char *) ibuf;
else
   cpntr = (char *) rbuf;

K1PACK_C(cpntr,aux,faux,ldata,outaux);
return 0;

}
