/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.LANGUAGE: C

.AUTHOR: K.Banse		ESO - Garching

  holds modules Cdazvis, Cdazscr, Crefovr, Getactdsp, Sc2ch,
                MakeLUT, MakeITT, SetupIDI


  routine Cdazvis calls IILBSV_C or IIMSMV_C

  routine Cdazscr converts the DeAnza scroll values (0,511 ...)
  to offsets from 0,0 (lower left corner)

.VERSION: 
 940209		creation, KB ESO - Garching

 090702		last modif
------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include <midas_def.h>
#include <proto_II.h>
#include <idinumd.h>


#define WRITE   1
#define READ    0

/*

*/

int Cdazvis(dsplay,chanl,flag,vis)
int  dsplay, chanl, flag, vis;
{
int stat;

if (flag == 1)     
   stat = IILSBV_C(dsplay,chanl,vis);
else 
   stat = IIMSMV_C(dsplay,&chanl,1,vis);

return (stat);
}

/*

*/

int Cdazscr(dsplay,chanl,scrx,scry)
int  dsplay, chanl, *scrx, *scry;
{
int ysc;


/*
 * In our internal model:
 *    x-scroll = starting pixel at left edge of display
 *    y-scroll = starting line at top of display
 *
 * Definition in IDI model:
 *    x-scroll = starting pixel at left edge of display
 *    y-scroll = starting line at bottom of display
 *
 * => y-scroll => y-scroll - display no. of lines
 */

if (*scrx >= QMSZX)		/* keep scrollx, scrolly in limits */
   *scrx = QMSZY - 1;
else if (*scrx <= -QMSZX)
   *scrx = -(QMSZX - 1);

if (*scry >= 2*QMSZY)
   *scry = 2*QMSZY - 1;
else if (*scry < 0)
   *scry = 0;

ysc = *scry - (QDSZY-1);	/* top line to bottom line of display */
return IIZWSC_C(dsplay,&chanl,1,*scrx,ysc);
}




int Cdazzsc(dsplay,chanl,zoom,scrx,scry)
int  dsplay, chanl, zoom, *scrx, *scry;
{
int ysc;


if (*scrx >= QMSZX)             /* keep scrollx, scrolly in limits */
   *scrx = QMSZY - 1;
else if (*scrx <= -QMSZX)
   *scrx = -(QMSZX - 1);

if (*scry >= 2*QMSZY)
   *scry = 2*QMSZY - 1;
else if (*scry < 0)
   *scry = 0;

ysc = *scry - (QDSZY-1);         /* top line to bottom line of display */
return IIZWSZ_C(dsplay,chanl,*scrx,ysc,zoom);
}

/*

*/

void Crefrovr()
{
DCOPEN( 1 );

(void) Cdazvis( QDSPNO, QOVCH, 2, 1 );

DCCLOS( QDSPNO );
}

/*

*/

void Getactdsp(strsxw,strwnd)
char  *strsxw, *strwnd;
{
int iav, nulo, uni;

char cbuf[16];

(void) SCKRDC("MID$SESS",1,1,12,&iav,cbuf,&uni,&nulo);

(void) strcpy(strsxw,"sxw  ");
strsxw[3] = cbuf[10];		/* MIDAS unit */
strsxw[4] = cbuf[11];
strwnd[0] = cbuf[5];		/* current display window */
strwnd[1] = cbuf[6];		/* current graphics window */
}

/*

*/

#ifdef __STDC__
void Sc2ch(int dirflag, int *x, int *y)

#else
void Sc2ch(dirflag,x,y)
int  dirflag;		/* IN:  1 - screen pixel -> channel pixel,
			       -1 - screen pixel <- channel pixel  */
int  *x;		/* IN/OUT: screen x <=> channel x  */
int  *y;		/* IN/OUT: screen y <=> channel y  */
#endif

{
if (dirflag < 0)
   {
   *x = (*x - SCROLX) * ZOOMX;
   *y = QDSZY - 1 - (SCROLY - *y)*ZOOMY;
   }

else	
   {	
   *x = (*x/ZOOMX) + SCROLX;
   *y = SCROLY - (QDSZY-1-(*y))/ZOOMY;
   }
}

/*

*/

void MakeLUT(icount,rlut,ocount,olut)
int   icount;
float *rlut;
int   ocount;
float *olut;
{
float   step, oend, q1, q2;
float   off, roff;

register int  jin, jout, jouta, joutb;

int   icount2, ocount2;

icount2 = icount + icount;
ocount2 = ocount + ocount;

if (ocount == icount)                           /* nothing to do ...  */
   {
   jouta = icount;
   joutb = icount2;
   for (jout=0; jout<icount; jout++)
      {
      olut[jout] = rlut[jout];
      olut[jouta] = rlut[jouta];
      olut[joutb] = rlut[joutb];
      jouta ++, joutb ++;
      }
   return;
   }

olut[0] = rlut[0];
olut[ocount] = rlut[icount];
olut[ocount2] = rlut[icount2];

oend = ocount - 1;
off = icount - 1;
step = off / oend;
off = step;

for (jout=1; jout<(ocount-1); jout++)
   {
   jin = (int) off;
   roff = jin;          /* `jin' will be integer < `off' */
   if (roff > off) roff = --jin;
   q1 = roff + 1.0 - off;
   q2 = off - roff;

   olut[jout] = (q1 * rlut[jin]) + (q2 * rlut[jin+1]);
   jin += icount;
   olut[jout+ocount] = (q1 * rlut[jin]) + (q2 * rlut[jin+1]);
   jin += icount;
   olut[jout+ocount2] = (q1 * rlut[jin]) + (q2 * rlut[jin+1]);

   off += step;
   }

jout = ocount - 1;
jin = icount - 1; 
olut[jout] = rlut[jin];
olut[jout+ocount] = rlut[jin+icount];
olut[jout+ocount2] = rlut[jin+icount2];
}

/*

*/

void MakeITT(icount,ritt,ocount,oitt)
int   icount;
float *ritt;
int   ocount;
float *oitt;

{
float   step, oend, q1, q2;
float   off, roff;

register int  jin, jout;

if (ocount == icount)                           /* nothing to do ...  */
   {
   for (jout=0; jout<icount; jout++)
      oitt[jout] = ritt[jout];
   return;
   }
oitt[0] = ritt[0];

oend = ocount - 1;
off = icount - 1;
step = off / oend;

off = step;
for (jout=1; jout<(ocount-1); jout++)
   {
   jin = (int) off;
   roff = jin;          /* `jin' will be integer < `off' */
   if (roff > off) roff = --jin;
   q1 = roff + 1.0 - off;
   q2 = off - roff;
   oitt[jout] = (q1 * ritt[jin]) + (q2 * ritt[jin+1]);
   off += step;
   }

oitt[ocount-1] = ritt[icount-1];
}

/*

*/

int SetupIDI(devname,xstatio,dinfo)
char  *devname;         /* IN: device name -
                                'sxw'
                                Midas unit (2 chars)
                                window no. (1 digit)
                                window type, 'g', 'z', 'c' or '\0'  */
char  *xstatio;         /* IN: name of XDisplay, `default' if local display */
int   *dinfo;           /* IN: display info - 21 elements
                                [0] = Alpno
                                [1],[2] = display size
                                [3],[4] = display offset
                                [5] = noLUT
                                [6] = maxLUTsize
                                [7] = minLUTsize
                                [8] = ownLUT
                                [9] = LUToff
                                [10],[11],[12] = font no's
                                [13] = no. of memories
                                [14] = no. of graph segments
                                [15],[16],[17] = x,ysize and depth of memory
                                [18] = RGB mode (0 = pseudo, 1 = truecolor)
                                [19] = index of background color (1 = black) 
			            for wintype 'z', 'c' only 10 elements used  
                                [20] = 24bit flag			*/

{
int  n, fid;
int  kfo[4];

char  cbuf[80];


(void) strcpy(cbuf,"MID_WORK:");
(void) strcpy(&cbuf[9],devname);
(void) strcat(cbuf,".dat");

fid = CGN_OPEN(cbuf,WRITE);
if (fid < 0) return (-1);

(void) sprintf(cbuf,"%s          Xwindow station name\n",xstatio);
(void) osawrite(fid,cbuf,(int)strlen(cbuf));

if (devname[6] == 'z')
   {
   (void) sprintf(
          cbuf,"0,%d,1,0    alpha flag, RGB mode, backcol_no, 24bit flag\n",
                  dinfo[18]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   if (dinfo[3] >= 0)
      {
      kfo[0] = dinfo[3];
      kfo[1] = dinfo[4];
      }
   else
      {
      kfo[0] = QDSZX + 2;
      kfo[1] = 0;
      }
   if (dinfo[1] >= 0)
     {
      kfo[2] = dinfo[1];
      kfo[3] = dinfo[2];
      }
   else
      {
      kfo[2] = QDSZX / 2;
      kfo[3] = QDSZY / 2;
      }
   (void)sprintf(cbuf,"%d,%d,%d,%d,     display offset + size\n",
                 kfo[0],kfo[1],kfo[2],kfo[3]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void)sprintf(cbuf,
         "%d,%d,%d,%d,%d,      noLUT, maxLUTsize, minLUTsize, ownLUT, LUToff\n",
         dinfo[5],dinfo[6],dinfo[7],dinfo[8],dinfo[9]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void)sprintf(cbuf,
                 "-1,-1,-1,            font no.s for small, medium, large\n");
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void)sprintf(cbuf,
                "1,10000,             no. of memories, no. of graph segments\n"
                );
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void) sprintf(cbuf,"%d,%d,-1\n",kfo[2],kfo[3]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));
   }

else if (devname[6] == 'c')
   {
   (void) sprintf(
          cbuf,"0,%d,1,0    alpha flag, RGB mode, backcol_no, 24bit flag\n",
                  dinfo[18]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   if (dinfo[3] >= 0)
      {
      kfo[0] = dinfo[3];
      kfo[1] = dinfo[4];
      }
   else
      {
      kfo[0] = QDSZX - 20;
      kfo[1] = QDSZY - 40;
      }
   if (dinfo[1] >= 0)
      {
      kfo[2] = dinfo[1];
      kfo[3] = dinfo[2];
      }
   else
      {
      kfo[2] = 180;                     /* 9 pixels with zoom=20 */
      kfo[3] = 180;
      }
   (void)sprintf(cbuf,"%d,%d,%d,%d,     display offset + size\n",
                 kfo[0],kfo[1],kfo[2],kfo[3]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void)sprintf(cbuf,
         "%d,%d,%d,%d,%d,      noLUT, maxLUTsize, minLUTsize, ownLUT, LUToff\n",
         dinfo[5],dinfo[6],dinfo[7],dinfo[8],dinfo[9]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void)sprintf(cbuf,
                 "-1,-1,-1,            font no.s for small, medium, large\n");
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void)sprintf(cbuf,
                "1,0,             no. of memories, no. of graph segments\n"
                );
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void) sprintf(cbuf,"%d,%d,-1\n",kfo[2],kfo[3]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));
   }

else
   {
   (void) sprintf(
          cbuf,"%d,%d,%d,%d    alpha flag, RGB mode, backcol_no, 24bit flag\n",
               dinfo[0],dinfo[18],dinfo[19],dinfo[20]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void) sprintf(cbuf,"%d,%d,%d,%d,     display offset + size\n",
                  dinfo[3],dinfo[4],dinfo[1],dinfo[2]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void)sprintf(cbuf,
         "%d,%d,%d,%d,%d,      noLUT, maxLUTsize, minLUTsize, ownLUT, LUToff\n",
         dinfo[5],dinfo[6],dinfo[7],dinfo[8],dinfo[9]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void)sprintf(cbuf,
                 "%d,%d,%d,            font no.s for small, medium, large\n",
                 dinfo[10],dinfo[11],dinfo[12]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void)sprintf(cbuf,
                 "%d,%d,             no. of memories, no. of graph segments\n",
                 dinfo[13],dinfo[14]);
   (void) osawrite(fid,cbuf,(int)strlen(cbuf));

   (void) sprintf(cbuf,"%d,%d,%d\n",dinfo[15],dinfo[16],dinfo[17]);
   for (n=0; n<dinfo[13]; n++)                     /* size of each memory  */
      (void) osawrite(fid,cbuf,(int)strlen(cbuf));
   }

osaclose(fid);
return (0);
}

