/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
                                   all rights reserved
.IDENTIFICATION: Plox
.PURPOSE: 
  Draw the histogram on a graphics device
.AUTHOR: 
  K. Banse
.LANGUAGE:      C
.KEYWORDS:     
  Graphics, bulk data frame, one-dimensional plotting
.ALGORITHM:
  PLOX uses the high level plot routines
.VERSION: 900202 - only read in single line, no more STIGET    KB
.VERSION: 931217 - use new high level plotting routines
.VERSION: 940315 - new C version

 051014		last modif

 -------------------------------------------------------------------- */

#include <stdio.h>

#include <midas_def.h>
#include <plot_def.h>

#include <math.h>

#define MAXBIN 512
#define MAX2BIN 1024


/*

*/

void Plox(ino)

int ino;		/* IN: image id */

{
register int nr, histr;
int  plmode = 1, access = 0;			/* always new plot file */
int uni = 0;		
int histo[MAXBIN], nbins, iav, nulo;

double dy;

float frame[12], scales[4];
float binsiz, histb[4], rval;

char label1[80], label2[80], label3[80];

void Pluff();


(void) SCDRDR(ino,"HIST_BINS",1,4,&iav,histb,&uni,&nulo);
(void) SCDRDI(ino,"HISTOGRAM",1,MAXBIN,&iav,histo,&uni,&nulo);


/* set the scale in x and y */

scales[0] = 0.;
scales[1] = 0.;
scales[2] = 0.;
(void) PCKWRR("SCALE",3,scales);
scales[0] = -999.;
scales[1] = -999.;
(void) PCKWRR("OFFSET",2,scales);
(void) PCKWRC("FRAME","AUTO");


for (nr=0; nr<12; nr++)
   frame[nr] = 0.0;

strcpy(label2,"log frequency"); 		/* fixed to log */


nbins = NINT(histb[0]);
if (nbins > MAXBIN) nbins = MAXBIN;
binsiz = histb[1];
frame[0] = histb[2];
frame[1] = histb[2] + (nbins-1)*binsiz;

for (nr=0; nr<nbins; nr++)			/* loop through data to */
   {						/* get max. function value */
   histr = histo[nr];
   if (histr > 0)
      {
      dy = log10((double) histr);
      rval = (float) dy;
      if (rval > frame[5]) frame[5] = rval;
      }
   }

if (frame[4] == frame[5]) frame[4] = frame[5] + 1.0;	/* show 0 range ... */


/*  calculate the scales + tickmarks */

(void) GETFRM("AUTO",frame);
(void) GETFRM("AUTO",frame+4);
(void) PCKWRR("XWNDL",4,frame);
(void) PCKWRR("YWNDL",4,frame+4);


/*  open the plot device */

(void) PCOPEN(" ","plox.plt",access,&plmode);
(void) AG_SSET("COLOR=1");			/* default colour to black */

(void) strcpy(label1,"pixel value ");
(void) sprintf(label3,"TITLE=histogram (bin size: %12.4g)",binsiz);

(void) PCAXES(frame,frame+4,label1,label2,label3);
Pluff(nbins,histb[2],histo,binsiz);
}

/*

*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE:        Plotting routine which does the actual histogram plotting     
   ------------------------------------------------------- */

void Pluff(nph,x,ifr,bins)

int nph, *ifr;
float  x, bins;

{
register int nr, histr;
int npoint;

double dy;

float *xptr, *yptr;
float xph[MAX2BIN], yph[MAX2BIN];
                                            

npoint = nph + 2;			/* for start + end point */
xptr = xph;
yptr = yph;

*xptr++ = x ;			/* start point */
*yptr++ = 0.0 ;
 
for (nr=0; nr<nph; nr++)
   {
   *xptr++ = x + (nr*bins);
   histr = ifr[nr];

   if (histr > 0) 
      {
      dy = log10((double) histr);
      *yptr++ = (float)dy;
      }
   else
      *yptr++ = 0.0;
   }

*xptr = x + ((nph-1)*bins);			/* end point */
*yptr = 0.0 ;

PCDATA(5,1,1,xph,yph,0.0,npoint);
}
