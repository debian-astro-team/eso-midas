/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE    C
.AUTHOR      K. Banse		ESO - IPG, Garching
.KEYWORDS    image display, channels
.IDENTIFIER  Cgetstr
.PURPOSE     high level interface to read input from keyboard of ImageDisplay
             -- if it exists, else just take it from the terminal --
.INPUT/OUTPUT
  call as    Cgetstr( outstr, dim )
 
 output:
          char *outstr :	string typed on keyboard
 in/output:
          int  *dim     :	input,  max. size of string `outstr'
                                output, number of characters read

.RETURNS     nothing

.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces
 
.VERSIONS    1.00       940429  converted from DAZINF.FOR    RvH

 090706		last modif
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1

#include <midas_def.h>
#include <idinumd.h>
#include <proto_II.h>

#include <stdio.h>


#ifndef TRUE
#define TRUE            1
#define FALSE           0
#endif

/*

*/

void Cgetstr(outstr,dim)
char *outstr;			/* OUT: string entered on keyboard */
int  *dim;			/* IN/OUT: max. length on input, 
					   real length on output */

{
int  kk, ll;

static char cr = '\r',					/* carriage return */
            dl = (char) 127,				/* delete character */
            bs = '\b';					/* back space */


kk = 0;
ll = *dim;


if (IDINUM != 11)     
   {
   char mystr[82];

   (void) IIIGSE_C(QDSPNO,0,mystr,&kk);
   if ( kk > ll ) kk = ll;
   mystr[kk] = '\0';
   (void) strcpy(outstr,mystr);
   SCTPUT(outstr);
   }

else				/* here for X11 stuff */
   {
   char mychar, cbuf[4];

   cbuf[1] = '\0';

loop_1:
   (void) IIIGCE_C(QDSPNO,-1,&mychar);
   if (mychar != cr)                                /* exit on RETURN */
      {
      if (mychar == dl)             /* delete a character */
         {
         if ((--kk) < 0) 
            kk = 0;
         else
            {
            cbuf[0] = bs;
            (void) SCTDIS(cbuf,-1);
            }
         goto loop_1;
         }
      else
         {
         if (kk < ll)
            {
            outstr[kk++] = mychar;
            cbuf[0] = mychar;
            (void) SCTDIS(cbuf,-1);
            goto loop_1;
            }
         }
      }
   }

outstr[kk] = '\0';
*dim = kk;
(void) printf("\n");
}                  
