/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE    C
.AUTHOR      K. Banse			IPG-ESO Garching
.KEYWORDS    Image Display, cursor
.IDENTIFIER  SETCUR_C
.PURPOSE     enable/disable fixed cursor(s) of different shapes or 
             programmable cursors and (optionally) set cursor coordinates
.ALGORITHM   use IDI interfaces for interacting with the Image Display
.INPUT/OUTPUT
   call as   SETCUR_C( dsplay, cursno, forma, colo, coords )

  input:
         int display :		Display unit no.
         int cursno  :		Cursor no. = 0, 1 or 2 for both cursors
                     		           = 3 for two independent cursors
         int forma   :		actual cursor form no.
                     		if = -1, make cursors/ROI not visible
                     		if = 99, use last defined cursor form + colour
         int colo    :		cursor colour
         int *coords :		coordinates
         
.RETURNS     nothing
.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces

.VERSIONS    1.00       940425  F -> C conversion, RvH
.VERSIONS    1.10       941031  cleanup, KB

 090706		last modif
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1

#include <midas_def.h>
#include <idinumd.h>
#include <proto_II.h>

/*

*/

void SETCUR_C(dsplay,cursno,forma,colo,coords)
int dsplay, cursno, forma, colo, *coords;


{
int form, colr, actvals, knul, ibuff[4];
register int  nr;
int roiid = 0;			/* This parameter is not used by IDI */
int unit = 0;


if (cursno == 2)		/* handle ROIs */
   {
   int   kbuff[8];

   if (forma == 99)     
      {
      (void) SCKRDI("CURSOR",5,4,&actvals,ibuff,&unit,&knul);
      form = ibuff[0];
      colr = ibuff[1];
      }
   else
      {
      form = forma;
      colr = colo;
      }

   if (form < 0)                                     /* clear visibility */
      (void) IIRSRV_C(dsplay,roiid,0);
   else 
      {
      if (form == 2)                                        /* circ. ROI */
         (void) IICINR_C(dsplay,-1,colr,*coords,coords[1],coords[2],
                         coords[3],coords[4],&roiid);
      else                                                  /* rect. ROI */
         (void) IIRINR_C(dsplay,-1,colr,*coords,coords[1],coords[2],
                         coords[3],&roiid);

      (void) IIRSRV_C(dsplay,roiid,1);
      (void) SCKWRI("DAZHOLD",&form,16,1,&unit);
      }
    
   for (nr=0; nr<4; nr++) kbuff[nr] = coords[nr];
   kbuff[4] = kbuff[6] = form;
   kbuff[5] = kbuff[7] = colr;
   (void) SCKWRI("CURSOR",kbuff,1,8,&unit);
   }

else				/* handle single cursor */
   {
   if (forma == 99)     
      {
      (void) SCKRDI("CURSOR",5,4,&actvals,ibuff,&unit,&knul);
      if ( cursno == 0 )
         {
         form = ibuff[0];
         colr = ibuff[1];
         }
      else
         {
         form = ibuff[2];
         colr = ibuff[3];
         }
      }
   else
      {
      form = forma;
      colr = colo;
      }

   (void) IICINC_C(dsplay,-1,cursno,form,colr,*coords,coords[1]);

   if (form >= 0)
      (void) IICSCV_C(dsplay,cursno,1);
   else
      (void) IICSCV_C(dsplay,cursno,0);
    
   ibuff[0] = form;
   ibuff[1] = colr;
   if (cursno == 0)
      {
      (void) SCKWRI("CURSOR",ibuff,5,2,&unit);
      (void) SCKWRI("CURSOR",coords,1,2,&unit);
      }
   else 
      {
      (void) SCKWRI("CURSOR",ibuff,7,2,&unit);
      (void) SCKWRI("CURSOR",coords,3,2,&unit);
      }
   }


/* Update also keyword DAZHOLD */

ibuff[0] = cursno;
ibuff[1] = form;
(void) SCKWRI("DAZHOLD",ibuff,1,2,&unit);
}
/*

*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1994 European Southern Observatory
.IDENTIFIER  CURINF_C
.LANGUAGE    C
.AUTHOR      K. Banse			IPG - ESO Garching
.KEYWORDS    Image Display, cursor
.PURPOSE     enable/disable fixed cursor(s) of different shapes or 
             programmable cursors and (optionally) set cursor coordinates
.ALGORITHM   use IDI interfaces for interacting with the Image Display
.INPUT/OUTPUT
   call as   CURINF_C( dsplay, nocurs, roiflg )

  input:
         int display :		Display unit no.

 output:
         int *nocurs :		Numberof available cursors -> 1 or 2
         int *roiflg :		= 1, if ROI is implemented
         
.RETURNS     nothing

.ENVIRONment MIDAS
#include <midas_def.h>   Prototypes for MIDAS interfaces

.VERSIONS    1.00       940425  F -> C conversion, RvH
.VERSIONS    1.10       941031  cleanup, KB
------------------------------------------------------------*/

void CURINF_C( dsplay, nocurs, roiflg )
int dsplay, *nocurs, *roiflg;

{
int ncap;


/* 40 = code for asking no. of cursors */

(void) IIDQCI_C(dsplay,40,1,nocurs,&ncap);   


/* 60 = code for asking for roi implementation */

(void) IIDQCI_C(dsplay,60,1,roiflg,&ncap);
}
/*

*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  GetScrPix
.LANGUAGE    C
.AUTHOR      K. Banse			IPG - ESO Garching
 	     031216	creation
.KEYWORDS    Image Display, cursor
.PURPOSE     get the screen pixels for a given w.c. or f.p. value
.ALGORITHM   use keyword IDIMEMI/IDIMEMR to map w.c.s/f.p.s to screen pixels
.INPUT/OUTPUT
 call as   GetScrPix(imno,wc_flag,inval,xy_flag,scrpix)

 input:
         int imno:	image id
         int wc_flag:	= 1, inval is world coord value
         		= 0, inval is frame pixel value
         float inval:	w.c. or f.p. value 
         int xy_flag:	= 1, use x-direction
         		= 2, use y-direction
         		= 3, use average of x,y-direction

 output:
         int *scrpix:	corresponding screen pixels

 returns:
	int status:	= 0, if o.k.
			> 0, if problems

.VERSION
 031217		last modif

-----------------------------------------------------------*/

int GetScrPix(imno,wc_flag,inval,xy_flag,scrpix)
int imno, wc_flag, xy_flag, *scrpix;
float inval;

{
int  stat, iav, kuni, knul;
int  newpix, idimi[8];

float rval, idimr[6];

double  dd1[3], dd2[3], dd3[3];



dd1[2] = dd2[2] = dd3[2] = 0.0;

stat = Pixconv("INIT",imno,dd1,dd2,dd3);
if (stat > 0) return (stat);

kuni = knul = 0;
(void) SCKRDI("IDIMEMI",1,7,&iav,idimi,&kuni,&knul);
/* idimi[1] = SSPX, idimi[2] = SSPY, idimi[5] = SFPX, idimi[6] = SFPY 
   idimi[3] = NSX, idimi[4] = NSY                                    */

(void) SCKRDR("IDIMEMR",1,6,&iav,idimr,&kuni,&knul);
/* idimr[0] = SWCX, idimr[1] = SWCY, idimr[2] = EWCX, idimr[3] = EWCY */

if (xy_flag == 2)
   {					/* move in y-direction */
   if (wc_flag == 1)
      {				/* use w.c. in y */
      dd1[0] = (double) idimr[0];
      if (idimr[3] > idimr[1])		/* ystep > 0 */
         dd1[1] = (double) (idimr[1] + inval);	/* add offset to y */
      else				/* ystep < 0 */
         dd1[1] = (double) (idimr[1] - inval);	/* add offset to y */
      stat = Pixconv("WRS",imno,dd1,dd2,dd3);
      }
   else
      {				/* use f.p. in y */
      dd1[0] = (double) idimi[5];
      dd1[1] = (double) (idimi[6] + inval);	/* add f.p.s to y */
      stat = Pixconv("_RS",imno,dd1,dd2,dd3);
      }

   if (stat == 0) 
      {
      rval = (float) dd3[1];		/* screen y-pix */
      newpix = CGN_NINT(rval);
      *scrpix = newpix - idimi[2];	/* subtract SSPY */

      /* if (*scrpix > idimi[4]) SCTPUT("screen pixel value > NSY ..."); */
      }
   }
else if (xy_flag == 1)
   {					/* move in x-direction */
   if (wc_flag == 1)
      {				/* use w.c. in x */
      if (idimr[2] > idimr[0])		/* xstep > 0 */
         dd1[0] = (double) (idimr[0] + inval);	/* add offset to x */
      else 				/* xstep < 0 */
         dd1[0] = (double) (idimr[0] - inval);	/* add offset to x */
      dd1[1] = (double) idimr[1];
      stat = Pixconv("WRS",imno,dd1,dd2,dd3);
      }
   else
      {				/* use f.p. in x */
      dd1[0] = (double) (idimi[5] + inval);	/* add f.p.s to x */
      dd1[1] = (double) idimi[6];
      stat = Pixconv("_RS",imno,dd1,dd2,dd3);
      }

   if (stat == 0) 
      {
      rval = (float) dd3[0];		/* screen x-pix */
      newpix = CGN_NINT(rval);
      *scrpix = newpix - idimi[1];	/* subtract SSPX */

      /* if (*scrpix > idimi[3]) SCTPUT("screen pixel value > NSX ..."); */
      }
   }
else
   {					/* move in x and y-direction */
   int  xdif, ydif;

   if (wc_flag == 1)
      {				/* use w.c. in x */
      if (idimr[2] > idimr[0])          /* xstep > 0 */
         dd1[0] = (double) (idimr[0] + inval);  /* add offset to x */
      else                              /* xstep < 0 */
         dd1[0] = (double) (idimr[0] - inval);  /* add offset to x */
      dd1[1] = (double) idimr[1];
      stat = Pixconv("WRS",imno,dd1,dd2,dd3);
      if (stat != 0) return stat;

      rval = (float) dd3[0];		/* screen x-pix */
      newpix = CGN_NINT(rval);
      xdif = newpix - idimi[1];		/* subtract SSPX */

      dd1[0] = (double) idimr[0];
      if (idimr[3] > idimr[1])          /* ystep > 0 */
         dd1[1] = (double) (idimr[1] + inval);  /* add offset to y */
      else                              /* ystep < 0 */
         dd1[1] = (double) (idimr[1] - inval);  /* add offset to y */
      stat = Pixconv("WRS",imno,dd1,dd2,dd3);
      }

   else
      {				/* use f.p. in x */
      dd1[0] = (double) (idimi[5] + inval);	/* add offset to x */
      dd1[1] = (double) idimi[6];
      stat = Pixconv("_RS",imno,dd1,dd2,dd3);
      if (stat != 0) return stat;

      rval = (float) dd3[0];		/* screen x-pix */
      newpix = CGN_NINT(rval);
      xdif = newpix - idimi[1];		/* subtract SSPX */

      dd1[0] = (double) idimi[5];
      dd1[1] = (double) (idimi[6] + inval);	/* add offset to y */
      stat = Pixconv("_RS",imno,dd1,dd2,dd3);
      }

   if (stat == 0) 
      {
      rval = (float) dd3[1];		/* screen y-pix */
      newpix = CGN_NINT(rval);
      ydif = newpix - idimi[2];		/* subtract SSPY */

      *scrpix = (xdif + ydif) / 2;	/* take mean of x, y difference */

      /* if (*scrpix > (idimi[3]+idimi[4])/2) 
            SCTPUT("screen pixel value > (NSX+NSY)/2 ..."); */
      }
   }

return stat;
}

