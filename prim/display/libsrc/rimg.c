/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  ROTA_C
.LANGUAGE    C
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    ImageDisplay, memory board
.PURPOSE     read a 2-D image/mask from given memory board of ImageDisplay
.ALGORITHM   use IDI interfaces to read memory and store into real data array
.INPUT/OUTPUT
   call as   void RIMG_C( chan, ittflg, bgnA, dimA, pntrA )

  input:
        int   chan   :		memory board to read image/mask from
        int   ittflg :		if = 1, map pixel data with current ITT
        int   bgnA[2]:		start screen (x,y)
        int   dimA[2]:		no. of x,y-pixels on screen

  output:
        float *pntrA :		array to be filled with image for RIMG
         
.RETURNS     nothing
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces

.VERSIONS    1.00       940517  F2C, taken from RIMG.FOR   RvH

 090706		last modif
------------------------------------------------------------*/

/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <idinumd.h>
#include <proto_II.h>

/*
 * here starts the code of the function
 */
void RIMG_C( chan, ittflg, bgnA, dimA, pntrA )
int   chan, ittflg, *bgnA, *dimA;
float *pntrA;
{
unsigned char *cdata;
int nopix;

/*
 * Initialised variables
 */
int imx = 0, imy = 0, kpack = 4;           /* data offset and packing factor */

/*
 * Set up transfer window
 */
(void) IIMSTW_C( QDSPNO, chan, LOADDR, *dimA, dimA[1], QMDEP, *bgnA, bgnA[1] );
      
/*
 * Read all data at once from Display memory
 */
nopix = *dimA * dimA[1];
cdata = (unsigned char *) osmmget( nopix * sizeof( unsigned char ) );
(void) IIMRMY_C( QDSPNO, chan, nopix, imx, imy, QMDEP, kpack, ittflg, cdata );

/*
 * Do unscrambling
 */
F1UNPA_C( cdata, nopix, pntrA );

/*
 * That's it folks...
 */
(void) osmmfree( (char *) cdata );
}
