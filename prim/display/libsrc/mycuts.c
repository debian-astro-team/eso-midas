/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  MYCUTS
.LANGUAGE    C
.AUTHOR      K. Banse,		 IPG-ESO Garching
.KEYWORDS    
.PURPOSE     return cut values according to a given method and flag
.INPUT/OUTPUT
  call as    MYCUTS_C( meth, flg, imno, size, pntr, mapsiz, cuts );

      input    char  *meth	available methods:    
                                  xSIGMA  or   xSIGMA,ySIGMA   (x,y in [0,9])
                                  MINMAX
                                  HIGH
               int   flg 	= 0, calculate only min,max
                                = 1, set also the CUTS according to method
               int   imno 	id of image frame
               int   size	size of the image
               int   mapsiz	size of a chunk
      ouput    float cuts[4]	cuts values in x,y and min and max in the data

.RETURNS     nothing
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces

.VERSIONS    1.00	940324	F77 -> C from LOADWN.FOR    RvH
	     1.10	940902  fix it, KB

051021		last modif

------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <midas_def.h>


/*

*/

void MYCUTS_C( meth, flg, imno, size, mapsiz, cuts )
char  *meth;
int   flg, imno, size, mapsiz;
float *cuts;

{
register int ii;

char  *cpntr, comn[6], mychar;
int   mmpix[2], count, nsigma, subhi, trsize;
float *data, mymax[2], rsav, rsults[11];


/* initialized variables */

static char  *scount = "0123456789";
int   felem = 1,
      sublo = 0,
      nopu  = 0;



CGN_UPSTR(meth);
mymax[0] = mymax[1] = 0.0;
for (ii=0; ii<11; ii++) rsults[ii] = 0.0;

ii = mapsiz * sizeof(float);
cpntr = malloc((size_t) ii);
data = (float *) cpntr;

count = size / mapsiz;
if (count*mapsiz < size) count ++;

if (count == 1)
   {
   if (strncmp(meth,"MIN",3) == 0)
      (void) strcpy(comn,"MIN");
   else
      (void) strcpy(comn,"MEAN");
   }
else
   {
   if (strncmp(meth,"MIN",3) == 0) 
      (void) strcpy(comn,"XMIN");
   else
      (void) strcpy(comn,"XMEAN");
   }

for (ii=0; ii<count; ii++)
   {
   (void) SCFGET(imno,felem,mapsiz,&trsize,cpntr);
      
   subhi = trsize - 1;
   if ((ii > 0) && (ii == (count-1))) *comn = 'Z';		/* last run */
   (void) Cstvals(comn,data,1,&trsize,&sublo,&subhi,mymax,rsults,mmpix,&nopu);
   if (ii == 0)
      {
      cuts[2] = rsults[0];
      cuts[3] = rsults[1];
      }
   else
      {
      if (cuts[2] > rsults[0]) cuts[2] = rsults[0];
      if (cuts[3] < rsults[1]) cuts[3] = rsults[1];
      }
   felem += trsize;
   }

(void) free(cpntr);
if (flg == 0) return;


/* also update the user cuts */
  
if (strncmp(meth,"MIN",3) == 0)			/* method MINMAX  */
   {
   cuts[0] = cuts[2];
   cuts[1] = cuts[3];
   }

else if (strncmp(meth,"HI",2) == 0)		/* method HIGH */
   {
   cuts[0] = rsults[2] - (0.1 * cuts[3]);	/* 0.1 * max */
   cuts[1] = cuts[3];
   }

else						/* method SIGMA */
   {
   nsigma = 1;
   for (ii=0; ii<10; ii++)
      {
      if (*meth == scount[ii])
         {
         nsigma = ii;
         break;
         }
      }

   rsav =  nsigma * rsults[3];		/* rsults[3] = sigma */
   cuts[0] = rsults[2] - rsav;

   ii = CGN_INDEXC(meth,',');		/* get no. after the `,' */
   if (ii > 0)
      {
      mychar = meth[ii+1];
      nsigma = 1;
      for (ii=0; ii<10; ii++)
         {
         if (mychar == scount[ii])
            {
            nsigma = ii;
            break;
            }
         }
      rsav = nsigma * rsults[3];
      }

   cuts[1] = rsults[2] + rsav;
   }

if (cuts[0] < cuts[2]) cuts[0] = cuts[2];	/* keep in actual range */
if (cuts[1] > cuts[3]) cuts[1] = cuts[3];
}
