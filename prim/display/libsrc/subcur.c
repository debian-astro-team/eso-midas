/* @(#)subcur.c	19.1 (ES0-DMD) 02/25/03 14:00:01 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT:  Copyright (c) 1994 European Southern Observatory,
                                         all rights reserved



   Not used yet ...



.IDENTIFIER  SUBCUR_C
.LANGUAGE    C
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    ImageDisplay, cursor
.PURPOSE     Enable/Disable fixed cursor(s) of different shapes or 
             programmable cursors and (optionally) set cursor coordinates
.ALGORITHM   use IDI interfaces for interacting with the DeAnza 
             (form = 0,...,15) or programmed shape (form=99) or from=100,101 
             for X11; disable cursors with form=-1
.INPUT/OUTPUT
  The following keywords are used:

  DAZIN/I/1/4		(1) = cursor_form_flag: 
			      98 for fixed cursor_form no. (only DeAnza)
			      99 for programmable
			      100 for IDI cursor shapes
			      101 for IDI region of interest (ROI)
			      200 for ANGLE
			      -1 for CLEAR/CURSOR
			(2) = cursor no. (0,1,2) for cursor 1,2 or both
			(3) = form_no. if (1) = 98
			      if (1) = 100 then
			      shape = 0 (implement. dependent),
				      1 (full cross hair), 2 (cross)
			              3 (open cross), 7 (arrow)
			      if (1) = 101 then
			      shape = 1 (recangular ROI)
			      shape = 2 (circular ROI)
			(4) = colour = 0 (checkered in our case)
			               1 (black), 2 (white)
			cf. IDI-doc. page 44 and 38
  P3/C/1/60 		x1,y1,x2,y2 - cursor coordinates
  			xc,yc,r1,r2,r3 if circular ROI
  P4/C/1/5             = F(RAME) to indicate, that coordinates in P2
                       are not screen coords. but frame related ones
  INPUTR/R/1/2		angle, length in case of DAZIN(1) = 200
  
  call as    SUBCUR_C( jflag )

  input:
          int jflag :		= 1 for display cursor
				  2 for graphics cursor
.RETURNS     nothing
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces

.VERSIONS    1.00       940630  from IDAUXZ.FOR    R.M.van Hees
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <string.h>
#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <idinumd.h>

#ifndef TRUE
#define TRUE            1
#define FALSE           0
#endif


/*++++++++++++++++++++++++++++++
 *
 * here starts the code of the function
 */
void SUBCUR_C( jflag )
int jflag;
{
register int nn;

int    actvals, cindx, cform, color, go_on, imno, knul, ncurs, nrco, 
       num, shape, unit, npix[2], dazzin[4], coord[5];
float  rbuff[6];
double start[2], step[2];
double dbuff;

char   *pntr1, *pntr2, cbuff[61], inputc[61];

/*
 * Get cursor info
 */
(void) SCKRDI( "DAZIN", 1, 4, &actvals, dazzin, &unit, &knul );
cform = dazzin[0];                                           /* internal no. */
ncurs = dazzin[1] ;
if ( ncurs < 0 ) ncurs = 0;
shape = dazzin[3];
color = dazzin[4];

/*
 * Display cursor
 */
if ( jflag == 1 )     
   DCOPEN( 1 );
else
   DCOPEN( 2 );                                      /* open graphics window */
/*
 * See, if it's CLEAR/CURSOR
 */
if ( cform == -1 )     
   { if ( ncurs == 2 )                                 /* region of interest */
        (void) IIRSRV_C( QDSPNO, 0, 0 );
     else if ( ncurs == 3 )                       /* two independent cursors */
        { (void) IIRSRV_C( QDSPNO, 0, 0 );
          (void) IIRSRV_C( QDSPNO, 1, 0 );
        }
     else
        (void) IIRSRV_C( QDSPNO, ncurs, 0 );

     ncurs = -1;                                      /* update key dazin(2) */
     (void) SCKWRI( "DAZIN", &ncurs, 2, 1, &unit );
   }
else
   { if ( ncurs > 1 )     
        { if ( cform == 101 && shape == 2 )     
             nrco = 5;                                       /* circular roi */
          else
             nrco = 4 ;
        }
     else
        nrco = 2;                            /* no. of coordinates needed... */
/*
 * now check, if also the cursor coordinates should be set
 */
     (void) SCKGETC( "P3", 1, 60, &actvals, inputc  );
     if ( *inputc == '+' )     
        { if ( nrco == 5 )                                   /* circular roi */
             { coord[0] = -1;
               coord[1] = -1;
               coord[2] =  5;                /* set inner radius to 5 pixels */
               coord[3] =  0;
               coord[4] =  0;
             }
          else                            /* indicate that we don't use them */
             for ( nn = 0; nn < 5; nn++ ) coord[nn] = -1;

          if ( jflag == 1 )
             SETCUR_C( QDSPNO, ncurs, shape, color, coord );
          else
             SETCUR_C( QDSPNO, ncurs, shape, color, coord );
        }
     else           /*  yes. set coordinates (frame or screen coords. input) */
        { (void) SCKGETC( "P4", 1, 1, &actvals, cbuff );
          if ( *cbuff != 'f' && *cbuff != 'F' )        /* screen coordinates */
             { num = CGN_CNVT( inputc, 1, nrco, coord, rbuff, &dbuff );
               if ( num <= 0 )
                  SCETER( 1,"SUBCUR: invalid cursor coords. given..." );
             }
          else                             /* we work with frame coordinates */
             { (void) DCGICH(QIMCH);
               (void) SCFOPN(dzmemc,D_R4_FORMAT,0,F_IMA_TYPE,&imno);
               (void) SCDRDI(imno, "NPIX" , 1, 2, 
                                             &actvals, npix, &unit, &knul );
               (void) SCDRDD( imno, "START", 1, 2, 
                                             &actvals, start, &unit, &knul );
               (void) SCDRDD( imno, "STEP" , 1, 2, 
                                             &actvals, step, &unit, &knul );
               if (Pixconv("INIT",imno,dd1,dd2,dd3) != 0)
                  SCETER(69,
                         "problems with init of world coord. conversion...");

               if ( ncurs == 1 )
                  cindx = 2;
               else
                  cindx = 0;
/*
 * handle cursor #0 or #1
 */
               go_on = TRUE;
               while ( go_on )
                  { if ( (pntr1 = strchr( inputc, ',' )) == NULL )
                       SCETER( 1,"SUBCUR: invalid cursor coords. given..." );
                    (void) strncpy( cbuff, inputc, (size_t)(pntr1 - inputc));
                    rbuff[0] = Cgetsng( cbuff, *npix, *start, *step );
                    if ( (pntr2 = strchr( pntr1+1, ',' )) == NULL )
                       (void) strcpy( cbuff, pntr1+1 );
                    else
                       { pntr1++;
                         (void) strncpy( cbuff, pntr1, (size_t)(pntr2-pntr1));
                       }
                    rbuff[1] = Cgetsng( cbuff, npix[1], start[1], step[1] );
                    Pixconv( "_RS", rbuff, start, step );
                    coord[cindx++] = rbuff[4];
                    coord[cindx++] = rbuff[5];

                    if ( ncurs == 0 )     
                       go_on = FALSE;                          /* we're done */
                    else if ( nrco == 5 )                    /* circular roi */
                       { (void) strcpy( inputc, pntr2 + 1 );
                         num = CGN_CNVT( inputc, 1, 3, coord+2, rbuff, &dbuff);
                         if ( num <= 0 )
                            SCETER( 1,"SUBCUR: invalid cursor coords. given" );
                         go_on = FALSE;
                       }
                    else
                       (void) strcpy( inputc, pntr2 + 1 );
                  }
             }

          if ( jflag == 1 )
             SETCUR_C( QDSPNO, ncurs, shape, color, coord );
          else
             SETCUR_C( QDSPNO, ncurs, shape, color, coord );
        }
   }
}
