/* @(#)splcnt.c	19.1 (ES0-DMD) 02/25/03 14:00:00 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT:  Copyright (c) 1994 European Southern Observatory,
                                         all rights reserved
.IDENTIFIER  SPLCNT_C
.LANGUAGE    C
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    split screen
.PURPOSE     calculate center values for split mode
.ALGORITHM   straight forward
             
.IN/OUTPUT
  call as    SPLCNT_C( splcx, splcy )
  in/output:
          int (*splcx)[5]	x-center values for different split modes
          int (*splcy)[5]       y-center values for different split modes  

.RETURNS     nothing
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces
 
.VERSIONS    1.00       940413  from LODIMA.FOR    RvH
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <midas_def.h>
 
/*
 * define some macros and constants
 */
#include <idinumd.h>

void SPLCNT_C( splcx, splcy )
int (*splcx)[5], (*splcy)[5];
{
int xcnt1, xcnt2, xcnt3;
int ycnt1, ycnt2, ycnt3;

/*
 * Example for screen size of 512,512:
 *
     splcx =
        { 384,128,128,384,		split_mode = 1 (0,1,2,3)
          384,128,  0,  0,  		split_mode = 2 (1,0)
            0,  0,128,384,  		split_mode = 3 (2,3)
          256,  0,  0,256,  		split_mode = 4 (0,3)
            0,256,256,  0  		split_mode = 5 (1,2)
        }
     splcy  =
        { 384,384,128,128,		split_mode = 1 (0,1,2,3)
          256,256,  0,  0,  		split_mode = 2 (1,0)
            0,  0,256,256,  		split_mode = 3 (2,3)
          384,  0,  0,128,  		split_mode = 4 (0,3)
            0,384,128,   0  		split_mode = 5 (1,2)
        }
 *
 * Build up CNT1, CNT2, CNT3  (e.g. 256, 128, 512-128 = 384)
 */
xcnt1 = QDSZX / 2;
xcnt2 = xcnt1 / 2;
xcnt3 = QDSZX - xcnt2;
ycnt1 = QDSZY / 2;
ycnt2 = ycnt1 / 2;
ycnt3 = QDSZY - ycnt2;

/*
 * Now fill array SPLCX
 */
splcx[0][0] = xcnt3;              /* full split screen */
splcx[1][0] = xcnt2;
splcx[2][0] = xcnt2;
splcx[3][0] = xcnt3;
splcx[0][1] = xcnt3;              /* 1,0 */
splcx[1][1] = xcnt2;
splcx[2][1] = 0;
splcx[3][1] = 0;
splcx[0][2] = 0;                  /* 2,3 */
splcx[1][2] = 0;
splcx[2][2] = xcnt2;
splcx[3][2] = xcnt3;
splcx[0][3] = xcnt1;              /* 0,3 */
splcx[1][3] = 0;
splcx[2][3] = 0;
splcx[3][3] = xcnt1;
splcx[0][4] = 0;                  /* 1,2 */
splcx[1][4] = xcnt1;
splcx[2][4] = xcnt1;
splcx[3][4] = 0;

/*
 * and array SPLCY 
 */ 
splcy[0][0] = ycnt3;             /* full split screen */
splcy[1][0] = ycnt3;
splcy[2][0] = ycnt2;
splcy[3][0] = ycnt2;
splcy[0][1] = ycnt1;             /* 1,0 */
splcy[1][1] = ycnt1;
splcy[2][1] = 0;
splcy[3][1] = 0;
splcy[0][2] = 0;                 /* 2,3 */
splcy[1][2] = 0;
splcy[2][2] = ycnt1;
splcy[3][2] = ycnt1;
splcy[0][3] = ycnt3;             /* 0,3 */
splcy[1][3] = 0;
splcy[2][3] = 0;
splcy[3][3] = ycnt2;
splcy[0][4] = 0;                 /* 1,2 */
splcy[1][4] = ycnt3;
splcy[2][4] = ycnt2;
splcy[3][4] = 0;

return;
}
