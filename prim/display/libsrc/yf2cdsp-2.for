C===========================================================================
C Copyright (C) 1995-2007 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  yf2cdsp.for +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module yf2cdsp.for
C  AUTHOR         K. Banse
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 050728:  created 
C 
C 070314	last modif
C -----------------------------------------------------------------------------
C
      SUBROUTINE TSCOLOR(CBUF,COLOR)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CBUF
      INTEGER     COLOR
C 
      CALL STSTR(1,CBUF)			!STRIPPED_STRING
      CALL DAZ1(COLOR)
C 
      RETURN
      END


      SUBROUTINE ALPTXC(CBUF,XP,YP,COLO)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CBUF
      INTEGER   XP,YP,COLO
C
      CALL STSTR(1,CBUF)                  !STRIPPED_STRING
      CALL DAZ2(XP,YP,COLO)
C
      RETURN
      END

      SUBROUTINE ALPTXT(CBUF,NA,NB,COLO)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CBUF
      INTEGER   NA,NB,COLO
C
      CALL STSTR(1,CBUF)                  !STRIPPED_STRING
      CALL DAZ3(NA,NB,COLO)
C
      RETURN
      END


      SUBROUTINE BLDGRA(SHAPE,COORDS,ARCS,XFIG,YFIG,FIGMAX,NOP)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   SHAPE
      INTEGER     COORDS(*),XFIG(*),YFIG(*),FIGMAX,NOP
      REAL        ARCS(*)
C 
      CALL STSTR(1,SHAPE)                  !STRIPPED_STRING
      CALL DAZ4(COORDS,ARCS,XFIG,YFIG,FIGMAX,NOP)
C
      RETURN
      END


      SUBROUTINE GETCUR(ACTION,FRAME,ICUR1,FP1,WC1,VAL1,STAT1,
     +                         ICUR2,FP2,WC2,VAL2,STAT2)
C
      IMPLICIT NONE
C
      CHARACTER*(*) ACTION, FRAME
      INTEGER     ICUR1(*),STAT1,ICUR2(*),STAT2
      REAL        FP1(*), FP2(*), WC1(*), WC2(*), VAL1, VAL2
C 
      CALL STSTR(1,ACTION)                !STRIPPED_STRING
      CALL STLOC(1,0,FRAME)               !untouched CHAR_LOC
C 
      CALL DAZ5(ICUR1,FP1,WC1,VAL1,STAT1,ICUR2,FP2,WC2,VAL2,STAT2)
C
      RETURN
      END

      SUBROUTINE GETSTR( OUTSTR, DIM )
      IMPLICIT NONE
C
      CHARACTER*(*) OUTSTR
      INTEGER     DIM
C
      CALL STLOC(1,1,OUTSTR)                 !blanked CHAR_LOC
      CALL DAZ6(DIM)
C
      RETURN
      END


      SUBROUTINE WRITT(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN,NITT,ISTA,COUNT,IDST
      REAL        RITT(*)
C
      CALL DAZ7(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C
      RETURN
      END

      SUBROUTINE RDITT(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN,NITT,ISTA,COUNT,IDST
      REAL        RITT(*)
C
      CALL DAZ8(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C
      RETURN
      END


      SUBROUTINE PIXXCV(CFLAG,IMNO,RBUFF,STAT)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CFLAG
      INTEGER     IMNO, STAT
      REAL        RBUFF(*)
C
      CALL STSTR(1,CFLAG)                  !STRIPPED_STRING
C 
      CALL DAZ9(IMNO,RBUFF,STAT)
C
      RETURN
      END


