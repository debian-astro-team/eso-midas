/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++

.IDENTIFICATION
  subroutine WALPHB			version 2.00	850715
  K. Banse				ESO - Garching

.KEYWORDS
  alphanumerics memory, DeAnza system

.PURPOSE
 write image info to A/N memory and/or channel no. depending on split mode

.ALGORITHM
  use IDI interfaces

.VERSION

 0090702	last modif
-------------------------------------------------- */

#include <string.h>
#include <stdio.h>

#include <midas_def.h>
#include <proto_II.h>
#include <idinumd.h>


/*

*/

void Alptext(memo,text,xp,yp,colour)
int memo, xp, yp, colour;
char *text;

{
int kk;


if (QALPNO >= 90)
   {
   if (memo == 99)		/* current alpha memory */
      (void) IIGTXT_C(QDSPNO,QALPNO,text,xp,yp,0,0,colour,0);
   else
      {
      kk = memo + 100;		/* indicate that we want the alpha part... */
      (void) IIGTXT_C(QDSPNO,kk,text,xp,yp,0,0,colour,0);
      }
   }
}

/*

*/

void Alpb2x(value,abuf)
float *value;
char  *abuf;

{
char bbuf[20];

register int nr;


/* process first no. */

(void) sprintf(bbuf,"%-9.5f",*value);
for (nr=(int)strlen(bbuf)-1; nr>=0; nr--)
   {
   if ((bbuf[nr] != ' ') && ((bbuf[nr] != '0') || (bbuf[nr-1] == '.')))
      {
      bbuf[++nr] = ',';
      bbuf[++nr] = '\0';
      break;
      }
   }
(void) strcpy(abuf,bbuf);


/* process second no. */

(void) sprintf(bbuf,"%-9.5f",*(value+1));
for (nr=(int)strlen(bbuf)-1; nr>=0; nr--)
   {
   if ((bbuf[nr] != ' ') && ((bbuf[nr] != '0') || (bbuf[nr-1] == '.')))
      {
      bbuf[nr+1] = '\0';
      break;
      }
   }
(void) strcat(abuf,bbuf);

}

/*

*/

void Alphamem(chan)
int chan;		/* IN: channel no. */

{
char   cbuf[80];


if (IDINUM != 11) return;		/* only X11 supported... */
if (QALPNO < 90) return;		/* no alpha memory there... */


(void) sprintf(cbuf,"CHANL: %d",chan);
Alptext(chan,cbuf,0,0,0);         /* CHANL: chan */


/* test, if something loaded in here... */

if (DCGICH(chan) != 1)    
   {
   char  tbuf[40];

   (void) sprintf(cbuf,"FRAME: %s",dzmemc);
   Alptext(chan,cbuf,0,1,0);		/* FRAME: frame  */


/*  display start + end, only if space enough  */

   if (QDSZX > 320)     
      {
      int  ioff;

      ioff = (QDSZX/16) + 5;		/* begin of second half */

      Alpb2x(dzmemr,tbuf);
      (void) sprintf(cbuf,"START: %s",tbuf);        
      Alptext(chan,cbuf,ioff,0,0);	/* START: xstart, ystart */
      
      Alpb2x(&dzmemr[2],tbuf);
      (void) sprintf(cbuf,"END: %s",tbuf);        
      Alptext(chan,cbuf,ioff,1,0);	/* END: xend, yend */

      Alpb2x(&dzmemr[6],tbuf);
      (void) sprintf(cbuf,"MIN,MAX: %s",tbuf);
      Alptext(chan,cbuf,ioff,2,0);	/* MIN,MAX: min, max */
      }

   Alpb2x(&dzmemr[4],tbuf);
   (void) sprintf(cbuf,"CUTS: %s",tbuf);
   Alptext(chan,cbuf,0,2,0)	;	/* CUTS: low, high_cut */
   }

}
/*

*/

void Alpcurs( cno, flag)
int cno, flag;
      
{
char string[60];


if (IDINUM != 11) return;


/*  put up initial text */

if (flag == 0)     
   {
   memset((void *)string,32,(size_t)58);
   string[58] = '\0';

   if (cno != 1)                    /* cno = 0, 1 or 2 */
      {
      Alptext(QALPNO,string,0,3,0);
      Alptext(QALPNO,"CURS0:",0,3,0);
      }
   if ((cno != 0) && (QDSZX > 320))     
      {
      int n;

      n = (QDSZX/16) + 5;
      Alptext(QALPNO,string,n,3,0);
      Alptext(QALPNO,"CURS1:",n,3,0);
      }
   }
}

/*

*/

void Alptxec(text,xp,yp,colour)
char *text;
int xp, yp, colour;

{
int xm, ym;

float val;
      

val = xp * 80. / QDSZX;
xm = CGN_NINT(val);
val = (QDSZY - yp) * 25. / QDSZY;
ym = CGN_NINT(val);

Alptext(QALPNO,text,xm,ym,colour);

}
