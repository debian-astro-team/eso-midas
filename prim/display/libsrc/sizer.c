/* @(#)sizer.c	19.1 (ESO-DMD) 02/25/03 14:00:00 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1994 European Southern Observatory
.IDENTIFIER  SIZER_C
.LANGUAGE    C
.AUTHOR      K. Banse			ESO Garching
.KEYWORDS    memory size
.PURPOSE     a) compute the size of an image on the image memory channel
             b) compute the size of the window in the frame for above
.ALGORITHM   straight forward
.INPUT/OUTPUT
   call as   SIZER_C(memsz,npix,scale,scale_flag,icen,nima,nssta,nf,nfsta)

  input:
         int  memsz		x/y-size of memory
         int  npix		no. of pixels in one dimension
         int  scale   		scaling factor for image
    				make image smaller, if > 1
    				make image larger, if <  1
         int  scale_flag 	= 0, omit lines; > 0, average over area
         int  icen[2]  		frame pixel and screen pixel which should
				be the fix point

  output:
         int  *nima  		no. of pixels in image memory
         int  *nssta  		start screen pixel
         int  *nf  		no. of frame pixels
         int  *nfsta  		start frame pixel

.RETURNS     nothing
.ENVIRONment MIDAS
             #include <midas_def.h>      Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces

.VERSIONS    1.00       940325  F->C, converted from LOADWN.FOR   RvH
 980507
------------------------------------------------------------*/


/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1



#include <midas_def.h>



/*

*/

void SIZER_C(memsz, npix, scale, scale_flag, icen, nima, nssta, nf, nfsta)
int  memsz, npix, scale, scale_flag, *icen, *nima, *nssta, *nf, *nfsta;

{
int kcomp, nfhi, nshi, nflo, nslo, nn, mfp1, mfp2;


mfp1 = icen[1];				/* screen fix point (center pixel) */
if (mfp1 < 0)     			/* 0,1,... */
   mfp1 = 0;
else if (mfp1 >= memsz)     
   mfp1 = memsz - 1;

mfp2 = icen[0];				/* frame fix point (center pixel) */
if (mfp2 < 1)     			/* 1,2,... */
   mfp2 = 1;
else if (mfp2 > npix)     
   mfp2 = npix;

nslo = mfp1;			/* screen pixels to the left */
nshi = memsz - mfp1;		/* fixpoint + scr. pixels to the right */

nflo = mfp2 - 1;		/* frame pixels to the left */
nfhi = npix - nflo;		/* fixpoint + fr. pixels to the right */


/* we shrink the image */

if (scale > 1)     
   {
   if (scale_flag == 0)		/* omit rows/columns */
      {
      kcomp = nflo / scale;
      if (kcomp < nslo) nslo = kcomp;
      nflo = nslo * scale;
      kcomp = (nfhi+scale-1)/scale;		/* start with center pixel */
      if (kcomp < nshi) nshi = kcomp;

      *nima = nslo + nshi;
      *nf   = *nima * scale;
      if (*nf <= 0)     
         {
         *nf = 1;                       /* if scaling too big ... */
         *nima = 1;
         }
      }
   else				/* we average over areas */
      {
      mfp2 -= (scale/2);		/* left end of center area */
      if (mfp2 < 1) mfp2 = 1;
      nflo = mfp2 - 1;
      kcomp = nflo / scale;
      if (kcomp < nslo) nslo = kcomp;
      nflo = nslo * scale;

      kcomp = (npix-mfp2+1)/scale;	/* start with center area */
      if (kcomp < nshi) nshi = kcomp;

      *nima = nslo + nshi;
      *nf   = *nima * scale;
      if (*nf <= 0)     
         {
         *nf = 1;                       /* if scaling too big ... */
         *nima = 1;
         }
      }
   }


/* we expand the image */

else if (scale < -1)     
   {
   nn = -scale;
   kcomp = nflo * nn;
   if (kcomp < nslo) nslo = kcomp;
   nflo = nslo / nn;
   nslo = nflo * nn;			/* now really multiples */
   kcomp = nfhi * nn;
   if (kcomp < nshi) nshi = kcomp;

   *nima = nslo + nshi;
   *nf   = *nima / nn;
   }


/* we keep the image size */

else 
   {
   if (nflo < nslo) nslo = nflo;
   nflo = nslo;
   if (nfhi < nshi) nshi = nfhi;

   *nima = nslo + nshi;			/* total no. of screen pixels */
   *nf = *nima;				/* total no. of frame pixels */
   }

*nssta = mfp1 - nslo;			/* start screen pixel */
*nfsta = mfp2 - nflo;			/* start frame pixel */
}
