/* @(#)wimclo.c	19.1 (ES0-DMD) 02/25/03 14:00:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)wimclo.c	1.3 (ESO-IPG) 7/4/94 10:27:17 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1994 European Southern Observatory
.IDENTIFIER  WIMCLO_C
.LANGUAGE    C
.AUTHOR      K. Banse			IPG-ESO Garching
.KEYWORDS    Image Display, memory board
.PURPOSE     do the final things to cleanly save all information 
             about an image loaded via LOADWN(-100,...) before
.ALGORITHM   copy the stuff done in WIMGA
.INPUT/OUTPUT
  call as    WIMCLO_C( dsplay, chan, vis, frame, start, step, cuts )

  input:
         int    dsplay		device id
         int    chan		memory channel no.
         int    vis		visibility (1/0)
         char   *frame		name of loaded image
         double *start		world coord start(x,y) of frame
         double *step		world coord step(x,y) of frame
         float  *cuts		contains low cut, high cut, min and max

.RETURNS     nothing
.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces

.VERSIONS    1.00       940325  F2C, taken from LOADWN.FOR   RvH
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <string.h>
#include <idinumd.h>

/*
 * here starts the code of the function
 */
void WIMCLO_C( dsplay, chan, vis, frame, start, step, cuts )
char   *frame;
int    dsplay, chan, vis;
float  *cuts;
double *start, *step;
{
int  nn;

if ( chan != QOVCH ) (void) Cdazvis( dsplay, chan, 1, vis );

/*
 * copy "frame" into global variable "dzmemc"
 */
(void) strcpy( dzmemc, frame );

/*
 * buffer DZMEMR = xsta,ysta,xend,yend,locut,hicut,min,max -> keyword IDIMEMR
 */
dzmemr[0] = (float) (start[0] + (SFPX - 1) * step[0]);
dzmemr[1] = (float) (start[1] + (SFPY - 1) * step[1]);

if ( SCALX < 0 )     
   nn = NSX / - SCALX;
else
   nn = NSX * SCALX;
dzmemr[2] = dzmemr[0] + (float) (nn - 1) * step[0];

if ( SCALY < 0 )     
   nn = NSY / - SCALY;
else
   nn = NSY * SCALY;
dzmemr[3] = dzmemr[1] + (float) (nn-1) * step[1];

for (nn=0; nn<4; nn++) dzmemr[nn+4] = *cuts++;

DCPICH(chan);

/*
 * now also display everything in the Image Display
 * if either we loaded into currently displayed channel or split_screen mode
 */
(void) Cdazvis( dsplay, chan, 2, 1 );

return;
}
