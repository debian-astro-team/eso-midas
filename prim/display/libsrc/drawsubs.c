/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  module DRAWSUBS
.LANGUAGE    C
.AUTHOR      K. Banse			ESO - Garching
.KEYWORDS    
.COMMENTS    holds routines: DRAW_IMA & DRAW_ANY

.ENVIRONment MIDAS
             #include <midas_def.h>   Prototypes for MIDAS interfaces
             #include <idinumd.h>     Global variables for DISPLAY interfaces
 
.VERSIONS    1.00       940712  from DRAW.FOR    R.M. van Hees

 090706		last modif
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */
#define  _POSIX_SOURCE 1

#include <stdio.h>
#include <string.h>
#include <midas_def.h>
#include <idinumd.h>
#include <proto_II.h>
#include <plot_def.h>		/* for NINT etc.  */

#ifndef TRUE
#define TRUE            1
#define FALSE           0
#endif

#define MAXDIM		2
#define MXFIG		1024

int XConvcoo();


/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  DRAW_IMA
.PURPOSE     draw an image in the overlay or image channel
.INPUT/OUTPUT
             nothing
.RETURNS     nothing
------------------------------*/
#ifdef __STDC__
      void DRAW_IMA( void )
#else
      void DRAW_IMA( )
#endif
{
int    actvals, ec, ed, el, imno, knul, naxis, over, size, 
       splmod, unit, icen[2], idisp[2], scale[2], iaux[4], npix[MAXDIM], 
       splcx[4][5], splcy[4][5];
register int nn;
static int dis_no, log_no, con_alw = 1;              /* variables for SCECNT */

float  angle, *p_img, cuts[4], rbuff[6];

double start[MAXDIM], step[MAXDIM];

char   *pntr, defaul[5], ident[33], cunit[49], frame[61], cbuff[81];



memset((void *)ident,32,(size_t)32);
ident[32] = '\0';
memset((void *)cunit,32,(size_t)48);
cunit[48] = '\0';


/* get name of frame to be plotted + data + descriptors */

(void) SCKGETC("IN_A",1,60,&actvals,frame);
(void) SCIGET(frame,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,MAXDIM, 
              &naxis,npix,start,step,ident,cunit,&pntr,&imno);
p_img = (float *) pntr;
if (naxis > 2) naxis = 2;
size = *npix * npix[1];


/* get default setting */

(void) SCKRDR("INPUTR",1,6,&actvals,rbuff,&unit,&knul);
(void) SCKGETC("DEFAULT",1,5,&actvals,defaul);
CGN_UPSTR(defaul);
over = (defaul[3] == 'Y') ? TRUE : FALSE;     


/* get cut values + display values (not necessarily there...) */

SCECNT("GET",&ec,&el,&ed);              /* get current error flags      */
SCECNT("PUT",&con_alw,&log_no,&dis_no); /* disable automatic error stop */
if (defaul[2] == 'Y')     
   {
   if (SCDRDR(imno,"LHCUTS",1,4,&actvals,cuts,&unit,&knul) != 0)
      for(nn=0; nn<4; nn++) cuts[nn] = 0.0;
   }
else
   {
   for(nn=0; nn<4; nn++) cuts[nn] = rbuff[nn];
   }

if ( SCDRDI( imno, "DISPLAY_DATA", 1,1, &actvals, idisp, &unit, &knul ) != 0 )
   *idisp = 1;

SCECNT( "PUT", &ec,& el,& ed );                           /* enable again... */


/* check cut values */

if ( cuts[0] >= cuts[1] )
   {
   if ( cuts[2] < cuts[3] )
      {
      cuts[0] = cuts[2];
      cuts[1] = cuts[3];
      }
   else
      {
      MYCUTS_C( "MINMAX", 1, imno, size, size, cuts );
      if ( cuts[0] < cuts[1] )
         (void) SCDWRR( imno, "LHCUTS", cuts, 1, 4, &unit );
      else                                       /* this will stop it... */
         SCETER( 2, "DRAW_IMA: invalid cut values..." );
      }
   }


/* get scaling factor */

if (*defaul != 'Y')     
   {
   (void) SCKRDI("DAZIN",1,1,&actvals,scale,&unit,&knul);
   *scale *= -1;
   if ((*scale == -1) || (*scale == 0)) *scale = 1;
   *idisp = *scale;
   }
else
   *scale = *idisp;


/* get yscale  + y-screen offset + intensity + angle */

iaux[0] = 1;                        /* default to 1. line */
iaux[1] = NINT(rbuff[2]);
iaux[2] = NINT(rbuff[3]);
iaux[3] = NINT(rbuff[4]);
angle = rbuff[5];


/* get central xpixel */

icen[1] = QMSZX / 2;

if (defaul[1] != 'Y')     
   {
   char  mybuf[80];
   int   kk, moff;
   float  sublo[3], subhi[3];

   (void) SCKGETC("INPUTC",21,40,&actvals,cbuff);
   if (*cbuff == ',')
      {
      mybuf[0] = 'C';
      moff = 1;
      }
   else
      moff = 0;
   (void) strcpy(&mybuf[moff],cbuff);
   kk = CGN_INDEXC(&cbuff[moff],',');
   if (kk < 0) (void) strcat(mybuf,",C");
   
   /* only "full" frame pixels are used */

   if (XConvcoo(0,imno,mybuf,2,&kk,sublo,subhi) != 0)
      SCETER(3,"DRAW_IMA: invalid syntax in x-center coord.");
   idisp[1] = NINT(sublo[0]);
   iaux[0] = NINT(sublo[1]);
   }


/* but don't forget to look for fixpoint pixels (new or previous ones) */

if ( defaul[4] != 'Y')     
   {
   (void) SCKRDI( "INPUTI", 1, 2, &actvals, icen, &unit, &knul );
   idisp[1] = *icen;
   }
*icen = idisp[1];


/* set up transfer window */
 
(void) IIMSTW_C(QDSPNO,QIMCH,LOADDR,QMSZX,QMSZY,QMDEP,0,0);


/* now draw the image in the ImageDisplay */

DRIMG_C(QDSPNO,QIMCH,over,iaux,p_img,*npix,icen,cuts,scale,angle);
DZDRAW = 1;                                              /* mark the channel */


/* get split screen info */

(void) SCKRDI("DAZHOLD",3,1,&actvals,&splmod,&unit,&knul);
SPLCNT_C(splcx,splcy);

if (splmod == 0)                                     /* init scroll values */
   {
   SCROLX = 0;
   SCROLY = QDSZY - 1;
   }
else
   {
   SCROLX = splcx[QIMCH+1][splmod] - QDSZX / 2;
   SCROLY = splcy[QIMCH+1][splmod] - QDSZY / 2;
   ZOOMX = ZOOMY = 1;
   (void) Cdazzsc(QDSPNO,QIMCH,ZOOMX,&SCROLX,&SCROLY);
   (void) Cdazvis(QDSPNO,QIMCH,1,0);            /* turn off color bar */
   }
}

/*

*/

/*++++++++++++++++++++++++++++++
.IDENTIFIER  DRAW_ANY
.PURPOSE     handle the option ANY of the routine DRAW
.INPUT/OUTPUT
  input:
             int  intens    :		number of cursors
.RETURNS     nothing
------------------------------*/
#ifdef __STDC__
      void DRAW_ANY( int intens )
#else
      void DRAW_ANY( intens )
      int   intens;
#endif
{
register int nn;

int actvals, knul, unit, cform, color, iter, ncurs, nmal, mx_curs, coord[5],
    go_on, give_info, npnts, statA, statB, xyA[5], xyB[5], dazhld[2], 
    xfig[2* MXFIG], yfig[2* MXFIG];

static char *info_usr = "switch cursor(s) on - next time we exit...";

iter = 0;
if ( IDINUM >= 11 )
   {
   coord[0] = 100;
   coord[1] = 100;
   coord[2] = 200;
   coord[3] = 200;
   coord[4] = 0;
   }
else
   {
   for ( nn = 0; nn < 5; nn++ ) coord[nn] = -1;
   }


/* read key DAZHOLD and set cursor shape and color */

(void) SCKRDI("DAZHOLD",1,2,&actvals,dazhld,&unit,&knul);
color = 2;
if ( dazhld[1] != -1 )     
   cform = dazhld[1];
else
   cform = 3;


/* set up cursor shape */

ncurs = 0;                                    /* we're using only one cursor */
SETCUR_C(QDSPNO,ncurs,cform,color,coord); 
Ccursin(QDSPNO,0,ncurs,xyA,&statA,xyB,&statB);


/* input cursor positions and check status */

(void) SCKRDI("CURSOR",1,1,&actvals,&mx_curs,&unit,&knul);

nmal = 0;
give_info = go_on = TRUE;
while ( go_on && ++nmal < mx_curs )
   {
   Ccursin(QDSPNO,1,ncurs,xyA,&statA,xyB,&statB);

   if ( statA == 0 )
      {
      if ((nmal == 1) && give_info)
         {
         SCTPUT(info_usr);
         give_info = FALSE;
         Ccursin(QDSPNO,0,ncurs,xyA,&statA,xyB,&statB);
         }
      else if (iter != 2)
         {
         iter = 2;
         Ccursin(QDSPNO,0,ncurs,xyA,&statA,xyB,&statB);
         }
      else
         {
         nmal--;
         go_on =  FALSE;                               /* EXIT-loop */
         }
      }
   else
      {
      if ( iter == 1 )                                /* first time ? */
         {
         float arcs[2];

         coord[2] = xyA[0];			/* save screen pixels */
         coord[3] = xyA[1];
         arcs[0] = arcs[1] = 0.0;		/* only needed for circle */
         buildgra("LI",coord,arcs,xfig,yfig,MXFIG,&npnts);
         coord[0] = coord[2];               /* save screen pixels */
         coord[1] = coord[3];
         (void) IIGPLY_C(QDSPNO,QOVCH,xfig,yfig,npnts,intens,1);
         }
      else
         {
         iter = 1;
         coord[0] = xyA[0];                  /* save screen pixels */
         coord[1] = xyA[1];
         }
      }
   }


/* save no. of coord. entered */

(void) SCKWRI("OUTPUTI",&nmal,1,1,&unit);
(void) SCTDIS(" ",0);
}

/*

*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  tstcolor
.LANGUAGE    C
.AUTHOR      K. Banse, ESO Garching
.KEYWORDS    drawing, color
.PURPOSE     get color no. from ASCII string which is either no. or color name
  
990115
------------------------------------------------------------*/
#ifdef __STDC__
int tstcolor(char *colstring)
#else
int tstcolor(colstring)
int  *colstring;		/* IN: string with color def. */
#endif


{
char  work[8];

int  intens;



CGN_UPCOPY(work,colstring,4);
switch (work[0])
   {
  case 'R':
  case '3':
   intens = 3;			/* red */
   break;

  case 'G':
  case '4':
   intens = 4;			/* green */
   break;

  case '1':
   intens = 1;			/* black */
   break;

  case '5':
   intens = 5;			/* blue */
   break;

  case 'B':
   if (work[2] == 'U') 
      intens = 5;			/* blue */
   else
      intens = 1;			/* black */
   break;

  case '6':
  case 'Y':
   intens = 6;			/* yellow */
   break;

  case '7':
  case 'M':
   intens = 7;			/* magenta (like violet) */
   break;

  case '8':
  case 'C':
   intens = 8;			/* cyan (light blue) */
   break;

  case '0':
  case 'E':
   intens = 0;			/* erase or 0 = clear the area */
   break;

  default:
   intens = 2;			/* white */
   }

return (intens);
}

