/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFIER  idauxx.c
.LANGUAGE    C
.AUTHOR      K. Banse, IPG-ESO Garching
.KEYWORDS    ImageDisplay, cursor, memory channel
.PURPOSE     This module combines the former 
             independent programs MEMORY, DAZTAB, RDIMAG, EXTRAC
.ALGORITHM   see the idividual subroutines
.INPUT/OUTPUT
  the following keys are used:

  DAZIN/I/1/2                if ACTION = CUR and only one cursor given,
                                xpix, ypix are stored here
  P3/C/1/3                   ITT_flag, = ITT if memory should pass through
                                         ITT first, else ignore ITT

  ACTION/C/1/6		option = ME for MEMORY & option for SUBMEM
                                 DA for DAZTAB & option for SUBDZT
                                 RD for RDIMAG & option for SUBRDI
                                 EX for EXTRAC
				 AN for AnyStuff
  IN_A/C/1/60           name of image frame which shall be created
  DAZHOLD/I/1/14	holds current status of ImageDisplay

.VERSION     1.00       940630  converted from IDAUXX.FOR    R.M.van Hees

 090709		last modif
--------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1


#include <stdio.h>
#include <string.h>

#include <midas_def.h>

#include <idinumd.h>
#include <proto_II.h>

int  AnyStuff(), move_sxdc();

/*

*/

int main()

{
int  actvals, dsplay, ittflg, knul, loopfl, ncurs, unit, 
     ovflag, nrpix[2], ibuff[3], dazhld[14];
      
char cbuff[2], action[8], frame[84];

void   getwco();




(void) SCSPRO("IDAUXX");


/* get current ImageDisplay status */

(void) SCKRDI("DAZHOLD",1,14,&actvals,dazhld,&unit,&knul);


/* get desired action */

(void) SCKGETC("ACTION",1,6,&actvals,action);
CGN_UPSTR(action);

if (strncmp(action,"AN",2) == 0)		/* escape to any action */
   {
   int  option, kbuf[22];		/* kbuf is used in IIESDB ... */

   (void) SCKRDI("INPUTI",1,10,&actvals,kbuf,&unit,&knul);
   option = kbuf[0];

   (void) AnyStuff(dazhld,option,&kbuf[1]);
   SCSEPI();
   }


/* determine, if we use a virtual device */

ovflag = 1;
dsplay = -1;
(void) SCKGETC("MID$DISP",1,2,&actvals,frame);
CGN_UPSTR(frame);
 
if (strcmp(frame,"I_") == 0)		/* yes. we use a display */
   {
   (void) SCKGETC("MID$SESS",6,1,&actvals,frame);
   if ((frame[0] != ' ') && (frame[0] != '-') && (frame[0] != '\0'))
      {
      dsplay = 0;
      DCOPEN(1);
      }
   }


if (strncmp(action,"WC",2) == 0)
   {
   ovflag = 0;
   (void) SCKGETC("IN_A",1,80,&actvals,frame);		/* get input frame */
   getwco(dsplay,frame);
   }
else if (dsplay == -1)		
   SCSEPI();		


/* for all following activities we need a display window */

if ( strncmp(action,"ME",2) == 0)
   SUBMEM_C(action+2,dazhld);

else if (strncmp(action,"DA",2) == 0)
   {
   (void) SCKGETC("IN_A",1,80,&actvals,frame);
   SUBDZT_C(action+2,dazhld,dsplay,frame);
   }

else if (strncmp(action,"RD",2) == 0)
   {
   (void) SCKGETC("P3",1,1,&actvals,cbuff);         /* get itt-flag */
   ittflg =  ((*cbuff == 'i') || (*cbuff == 'I')) ? 1 : 0;
   (void) SCKGETC("IN_A",1,80,&actvals,frame);

   /* get involved cursors + init cursor input */

   (void) SCKRDI("DAZIN",1,3,&actvals,ibuff,&unit,&knul);
   ncurs = *ibuff;                 /* ncurs = 0,1,2 for cursor 0,1 or both */
   if (ncurs < 2)
      {
      nrpix[0] = ibuff[1];
      nrpix[1] = ibuff[2];
      }

   SUBRDI_C(action+2,ittflg,ncurs,nrpix,frame);
   }

else if (strncmp(action,"EX",2) == 0)
   {
   (void) SCKRDI("AUX_MODE",9,1,&actvals,&ncurs,&unit,&knul);
   if (ncurs != 0) CONCHA_C(QDSPNO,QOVCH,1,0);		/* clear overlay */

   (void) SCKRDI("CURSOR",1,1,&actvals,&ncurs,&unit,&knul);
   if ( ncurs != 0 ) ncurs = 2;   /* ncurs = 0 or 2 for one cursor or both */

   (void) SCKGETC("P3",1,1,&actvals,cbuff);
   loopfl = ((*cbuff == 'l') || (*cbuff == 'L')) ? 0 : -1;
   (void) SCKGETC("OUT_A",1,80,&actvals,frame);

   SUBEXT_C(ncurs,loopfl,dazhld[2],frame);
   }

else			/*   for action OPen nothing to do anymore */
   ovflag = 0;


if (dsplay == 0) DCCLOS(QDSPNO);
if (ovflag == 1) (void) Crefrovr();

return SCSEPI();
}
/*

*/

void cleany(flag,instr,outstr)
int   flag;
char  *instr, *outstr;

{
char  *pin, *pout;



pin = instr;
pout = outstr;

while (*pin == ' ') pin++;			/* skip leading blanks */

while (*pin != ' ') 
   {
   if (*pin == '\0') goto put_end;
   *pout++ = *pin++;				/* copy 1st part */
   }
pin ++; *pout++ = ':';				/* insert `:' */

while (*pin != ' ') 
   {
   if (*pin == '\0') goto put_end;
   *pout++ = *pin++;				/* copy 2nd part */
   }
pin ++; *pout++ = ':';				/* insert `:' */

while (*pin != ' ') 
   {
   if (*pin == '\0') goto put_end;
   *pout++ = *pin++;				/* copy 3rd part */
   }

put_end:
if (flag == 1) *pout++ = ',';

*pout = '\0';

}   
/*

*/

void getwco(dspflag,infile)
int   dspflag;			/* IN: if = 0, we have a display opened */
char  *infile;			/* IN: input file name */

{
int  nval, nulo, unit, imnoa, pixdim, direc, linflag, nsele;
int  tid, k, mm, off, stat, px3, inull[3], npix[3], sublo[3], subhi[3];
int  tbl_flag, icolref[3], ocolref[3], in_resu, out_resu, nocol, nrow, nloop;
register int  nr;

char  cbuf[80], tbuf[80], intable[80], auxstr[80], convstr[80], subs[3][32];
char  colform[16], colunit[16];
register char  cr;

double   dd1[3], dd2[3], dd3[6], doff[12];

float   rr, roff, rval[3], coordvals[3];

void  form_sexa();



nloop = nocol = out_resu = in_resu = stat = mm = 0;

for (nr=0; nr<12; nr++) doff[nr] = -1.1;
(void) SCKWRD("OUTPUTD",doff,1,12,&unit);	/* init to -1.1 all over */


(void) SCFOPN(infile,D_OLD_FORMAT,0,F_IMA_TYPE,&imnoa);

for (nr=0; nr<3; nr++) npix[nr] = 1;
(void) SCDRDI(imnoa,"NPIX",1,3,&nval,npix,&nulo,&unit);

linflag = fp2wc(0,imnoa,dd1,dd2);	/* init + test, if `real' WCS */

for (nr=0; nr<3; nr++)
  {
  dd1[nr] = 1.0;
  dd2[nr] = 0.0;
  sublo[nr] = 0;
  }

(void) SCKGETC("INPUTC",1,72,&nval,cbuf);	/* get coord. spec */
if ((cbuf[0] >= 'a') && (cbuf[0] <= 'z'))
   tbuf[0] = cbuf[0] + 'A' - 'a';
else 
   tbuf[0] = cbuf[0];

if ((tbuf[0] >= 'A') && (tbuf[0] <= 'Z'))
   {						/* we work on a table */
   if (tbuf[0] == 'S')
      {					/* catch s123 (for screen pixels) */
      if ((cbuf[1] >= '0') && (cbuf[1] <= '9'))
         {
         tbl_flag = 0;
         goto join_all;
         }
      }
   (void) strcpy(intable,cbuf);
   (void) TCTOPN(intable,F_IO_MODE,&tid);

   (void) SCKGETC("P3",1,80,&nval,cbuf);	/* get input columns */
   tbl_flag = 1;
   off = 0;
   nocol = 0;
   for (nr=0; nr<3; nr++)
      {
      k = CGN_EXTRSS(cbuf,(int)strlen(cbuf),',',&off,tbuf,30);
      if (k < 1)
         break;
      else
         {
         if (tbuf[0] == '#')
            {
            if (CGN_CNVT(&tbuf[1],1,1,icolref+nr,&roff,doff) <= 0) 
               {
               (void) sprintf(auxstr,
               "bad column (label = %s) specification ...",tbuf);
               SCETER(65,auxstr);
               }
            }
         else
            {
            (void) TCLSER(tid,&tbuf[1],icolref+nr);
            if (icolref[nr] < 0)
               {
               (void) sprintf(auxstr,
               "Missing column (label = %s) in table ...",tbuf);
               SCETER(66,auxstr);
               }
            }
         nocol = nr + 1;
         }
      }
   if (nocol < 1) SCETER(67,"invalid column specs...");

   (void) SCKGETC("P4",1,80,&nval,tbuf);
   CGN_UPSTR(tbuf);
   if (tbuf[0] == 'P')
      in_resu = 0;				/* from frame pixels */
   else if (tbuf[0] == 'W')
      in_resu = 1;				/* from world coords (dec.) */
   else
      in_resu = 2;				/* from RA, DEC */
   k = CGN_INDEXC(tbuf,'-');
   out_resu = 2;				/* default: to RA, DEC */
   if (k > 0)
      {
      k ++;
      if (tbuf[k] == 'P')
         out_resu = 0;				/* frame pixels */
      else if (tbuf[k] == 'W')
         out_resu = 1;				/* to world coords (dec.) */
      }

   if (in_resu == out_resu)
      {
      SCTPUT("no conversion - we quit...");
      (void) TCTCLO(tid);
      return;
      }

   if (out_resu == 0)
      {
      (void) strcpy(colform,"G12.6");
      (void) strcpy(colunit,"Frame pixels");
      }
   else
      {
      (void) strcpy(colunit,"World coords");
      if (out_resu == 1)
         (void) strcpy(colform,"G14.8");
      else
         {
         (void) strcpy(colform,"S13.6");
         nocol = 2;
         }
      }

   (void) SCKGETC("P5",1,80,&nval,cbuf);	/* get output columns */
   off = 0;
   mm = 0;
   for (nr=0; nr<3; nr++)
      {
      k = CGN_EXTRSS(cbuf,(int)strlen(cbuf),',',&off,tbuf,30);
      if (k < 1)
         break;
      else
         {
         if (tbuf[0] == '#')
            {
            if ((CGN_CNVT(&tbuf[1],1,1,ocolref+nr,&roff,doff) <= 0) ||
                (ocolref[nr] < 1))
               {
               (void) sprintf(auxstr,
               "bad column (label = %s) specification ...",tbuf);
               SCETER(65,auxstr);
               }
            (void) TCFPUT(tid,ocolref[nr],colform);	/* enforce format */
            (void) TCUPUT(tid,ocolref[nr],colunit);	/* enforce unit */
            }
         else
            {
            (void) TCLSER(tid,&tbuf[1],&nval);
            if (nval < 0)			/* create new column */
               {
               if (out_resu == 0)			/* frame pixels */
                  {
                  (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,&tbuf[1],
                                ocolref+nr);
                  }
               else if (out_resu == 1)		/* world coords (decimal) */
                  {
                  (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,&tbuf[1],
                                ocolref+nr);
                  }
               else 				/* world coords RA, DEC */
                  {
                  (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,&tbuf[1],
                                ocolref+nr);
                  }
               }
            else				/* column exists already */
               {
               ocolref[nr] = nval;
               (void) TCFPUT(tid,ocolref[nr],colform);
               (void) TCUPUT(tid,ocolref[nr],colunit);
               }
            }
         mm = nr + 1;
         }
      }

   if (mm < 1) 			/* use default labels for new columns */
      {
      if (out_resu == 0)			/* frame pixels */
         {
         (void) strcpy(tbuf,"X_coordpix");
         (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,tbuf,
                       ocolref+nr);
         if (nocol > 1)
            {
            tbuf[0] = 'Y';
            (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,tbuf,
                          ocolref+nr);
            }
         if (nocol > 2)
            {
            tbuf[0] = 'Z';
            (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,tbuf,
                       ocolref+nr);
            }
         }
      else if (out_resu == 1)		/* world coords (decimal) */
         {
         (void) strcpy(tbuf,"X_coord");
         (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,tbuf,
                       ocolref+nr);
         if (nocol > 1)
            {
            tbuf[0] = 'Y';
            (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,tbuf,
                          ocolref+nr);
            }
         if (nocol > 2)
            {
            tbuf[0] = 'Z';
            (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,tbuf,
                          ocolref+nr);
            }
         }
      else 				/* world coords RA, DEC */
         {
         (void) strcpy(tbuf,"RA");
         (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,tbuf,
                       ocolref+nr);
         (void) strcpy(tbuf,"DEC");
         (void) TCCINI(tid,D_R4_FORMAT,1,colform,colunit,tbuf,
                          ocolref+nr);
         }
      }


   /* get the no. of rows of this table */

   (void) TCIGET(tid,&nval,&nrow,&nval,&nval,&nval);
   nloop = 0;
   nsele = 0;
   }
else
   {
   tbl_flag = 0;
   goto join_all;
   }


/* read the table row by row */

input_string:
if (nsele)
   {						/* store last results */
   (void) SCKRDD("OUTPUTD",1,12,&nval,doff,&nval,&nval);
   if (out_resu == 0)
      {
      for (nr=0; nr<nocol; nr++)
         coordvals[nr] = (float) doff[nr+9];
      (void) TCRWRR(tid,nloop,nocol,ocolref,coordvals);
      }
   else if (out_resu == 1)
      {
      for (nr=0; nr<nocol; nr++)
         coordvals[nr] = (float) doff[nr];
      (void) TCRWRR(tid,nloop,nocol,ocolref,coordvals);
      }
   else
      {
      inull[0] = CGN_NINT((float) doff[3]);
      inull[1] = CGN_NINT((float) doff[4]);
      rr = (float) doff[5];
      sprintf(cbuf,"%d:%d:%f",inull[0],inull[1],rr);
      (void) TCRWRC(tid,nloop,1,ocolref,cbuf);

      inull[0] = CGN_NINT((float) doff[6]);
      inull[1] = CGN_NINT((float) doff[7]);
      rr = (float) doff[8];
      sprintf(cbuf,"%d:%d:%f",inull[0],inull[1],rr);
      (void) TCRWRC(tid,nloop,1,ocolref+1,cbuf);
      }
   }

nloop ++;
if (nloop > nrow)
   {
   (void) TCTCLO(tid);
   return;
   }

(void) TCSGET(tid,nloop,&nsele);		/* is row selected? */
if (nsele)
   {
   (void) TCRRDR(tid,nloop,nocol,icolref,coordvals,inull);
   cbuf[0] = '\0';
   if (in_resu == 0)
      {
      for (nr=0; nr<nocol; nr++)
         {
         nval = sprintf(tbuf,"@%-8.0f",coordvals[nr]);
         k = CGN_INDEXC(tbuf,' ');
         if (k < 1) k = nval;
         tbuf[k++] = ',';
         tbuf[k] = '\0'; 
         (void) strcat(cbuf,tbuf);
         }
      }
   else if (in_resu == 1)
      {
      for (nr=0; nr<nocol; nr++)
         {
         nval = sprintf(tbuf,"%-14.8g",coordvals[nr]);
         k = CGN_INDEXC(tbuf,' ');
         if (k < 1) k = nval;
         tbuf[k++] = ',';
         tbuf[k] = '\0'; 
         (void) strcat(cbuf,tbuf);
         }
      }
   else
      {
      (void) TCRRDC(tid,nloop,1,icolref,tbuf,inull);
      cleany(1,tbuf,auxstr);			/* change to aa:bb:cc.dd, */
      (void) strcpy(cbuf,auxstr);
      (void) TCRRDC(tid,nloop,1,icolref+1,tbuf,inull);
      cleany(2,tbuf,auxstr);			/* change to aa:bb:cc.dd */
      (void) strcat(cbuf,auxstr);
      }
   }
else
   goto input_string;				/* skip non-selected rows */

/*
printf("string we built: %s\n",cbuf);
*/


join_all:
direc = 0;
px3 = 0;
cr = tbuf[0];			/* so we have upper case */

if ((cr == '@') || (cr == '>') || (cr == '<') || (cr == 'C'))
   direc = 1;					/* frame pixels entered */
else if (cr == 'S')				/* screen pixels */
   {
   if (dspflag != 0)
      SCETER(5,"screen pixels only valid for displayed frame ...");
   direc = 2;
   }
else
   {
   k = CGN_INDEXC(cbuf,',');
   if (k > 0)
      {
      cr = cbuf[k+1];
      if ((cr == '@') || (cr == '>') || (cr == '<') || (cr == 'C'))
         direc = 1;
      else
         {
         nr = k + 1;
         k = CGN_INDEXC(&cbuf[nr],',');
         if (k > 0)
            {
            mm = nr + k + 1;
            cr = cbuf[mm];
            if ((cr == '@') || (cr == '>') || (cr == '<') || (cr == 'C'))
               px3 = 1;
            }
         }
      }
   }					/* 3rd coord not checked... */



/*  world coords in, frame pixels out */

if (direc == 0)				/* world coords entered */
   {
   stat = Convcoo(0,imnoa,cbuf,3,&pixdim,sublo,subhi);
   if (stat != 0)
      SCETER(1,"Invalid syntax in world coordinates...");
   if (px3 == 1)
      {
      (void) SCDRDD(imnoa,"START",1,3,&nval,dd1,&nulo,&unit);
      (void) SCDRDD(imnoa,"STEP",1,3,&nval,dd2,&nulo,&unit);
      dd1[0] = dd1[2] + sublo[2]*dd2[2];
      (void) sprintf(tbuf,"%8.8g",dd1[0]);
      k = 7;
      for (nr=0; nr<7; nr++)		/* skip leading blanks */
         {
         if (tbuf[nr] != ' ')
            {
            k = nr;
            break;
            }
         }
      (void) strcpy(&cbuf[mm],&tbuf[k]);
      }
   (void) sprintf(auxstr,"world coords entered = %s",cbuf);
   SCTPUT(auxstr);

   for (nr=0; nr<pixdim; nr++) sublo[nr] ++;
   
   if (pixdim == 1)				/* 1-dim frame */
      {
      (void) sprintf(cbuf,"frame pixels = %d",sublo[0]);
      (void) CGN_CNVT(cbuf,4,1,subhi,rval,dd1);
      (void) SCKWRD("OUTPUTD",dd1,1,3,&unit);      /* save dec. wcoords */
      }

   else if (pixdim == 2)			/* 2-dim frame */
      {
      if (linflag == 0)
         {
         (void) strcpy(convstr,cbuf);		/* prepare string for CNVT */
         for (nr=0; nr<80; nr++)		/*  ':'  ->  ','  */
            {
            if (convstr[nr] == ':')
               convstr[nr] = ',';
            else if (convstr[nr] == '\0')
               break;
            }

         k = CGN_INDEXC(cbuf,',');
         cbuf[k] = '\0';
         off = CGN_INDEXC(cbuf,':'); 		/* off > 0: sexa -> dec */
         (void) move_sxdc(off,1,cbuf,auxstr);		/* RA */
         (void) sprintf(tbuf,"  or %s, ",auxstr);
         k ++;

         if (off > 0)				/* RA, DEC input */
            (void) CGN_CNVT(convstr,4,6,subhi,rval,dd3);
         else					/* decimal input */
            (void) CGN_CNVT(convstr,4,2,subhi,rval,dd1);


         (void) move_sxdc(off,0,&cbuf[k],auxstr);	/* DEC */
         (void) strcat(tbuf,auxstr);
         SCTPUT(tbuf);

         if (off > 0)					/* tbuf: dec. wc */
            (void) CGN_CNVT(&tbuf[5],4,2,subhi,rval,dd1);
         else						/* tbuf: RA,DEC wc */
            {
            (void) strcpy(convstr,&tbuf[5]);        /* save string,  : -> ,  */
            for (nr=0; nr<80; nr++)
               {
               if (convstr[nr] == ':')
                  convstr[nr] = ',';
               else if (convstr[nr] == '\0')
                  break;
               }

            (void) CGN_CNVT(convstr,4,6,subhi,rval,dd3);
            }

         (void) SCKWRD("OUTPUTD",dd3,4,6,&unit);   /* save hours,mins,secs */

         }
      (void) sprintf(cbuf,"frame pixels = %d,%d",sublo[0],sublo[1]);
      (void) SCKWRD("OUTPUTD",dd1,1,3,&unit);      /* save dec. wcoords */
      }

   else						/* 3-dim frame */
      {
      if (linflag == 0)
         {
         (void) strcpy(convstr,cbuf);           /* prepare string for CNVT */
         for (nr=0; nr<80; nr++)                /*  ':'  ->  ','  */
            {
            if (convstr[nr] == ':')
               convstr[nr] = ',';
            else if (convstr[nr] == '\0')
               break;
            }

         k = CGN_INDEXC(cbuf,',');
         cbuf[k] = '\0';
         off = CGN_INDEXC(cbuf,':');            /* off > 0: sexa -> dec */
         (void) move_sxdc(off,1,cbuf,auxstr);           /* RA */
         (void) sprintf(tbuf,"  or %s, ",auxstr);
         k ++;

         if (off > 0)                           /* RA, DEC input */
            (void) CGN_CNVT(convstr,4,6,subhi,rval,dd3);
         else                                   /* decimal input */
            (void) CGN_CNVT(convstr,4,2,subhi,rval,dd1);

         (void) strcpy(cbuf,&cbuf[k]);		/* move to y-, z- pixel */
         k = CGN_INDEXC(cbuf,',');
         cbuf[k] = '\0';
         (void) move_sxdc(off,0,cbuf,auxstr);		/* DEC */
         (void) strcat(tbuf,auxstr);

         if (off > 0)                                   /* tbuf: dec. wc */
            (void) CGN_CNVT(&tbuf[5],4,2,subhi,rval,dd1);
         else                                           /* tbuf: RA,DEC wc */
            {
            (void) strcpy(convstr,&tbuf[5]);        /* save string,  : -> ,  */
            for (nr=0; nr<80; nr++)
               {
               if (convstr[nr] == ':')
                  convstr[nr] = ',';
               else if (convstr[nr] == '\0')
                  break;
               }

            (void) CGN_CNVT(convstr,4,6,subhi,rval,dd3);
            }

         (void) strcat(tbuf,", ");
         k ++;
         (void) CGN_CNVT(&cbuf[k],4,1,subhi,rval,&dd1[2]);
         (void) strcat(tbuf,&cbuf[k]);
         SCTPUT(tbuf);

         (void) SCKWRD("OUTPUTD",dd3,4,6,&unit);   /* save hours,mins,secs */
         }

      (void) sprintf(cbuf,
                     "frame pixels = %d,%d,%d",sublo[0],sublo[1],sublo[2]);
      (void) SCKWRD("OUTPUTD",dd1,1,3,&unit);      /* save dec. wcoords */
      }

   dd2[0] = (double) sublo[0];			/* store frame pixels */
   dd2[1] = (double) sublo[1];
   dd2[2] = (double) sublo[2];
   (void) SCKWRD("OUTPUTD",dd2,10,3,&unit); 
   }



/*  screen or frame pixels in, world coords out */

else					/* pixel coords. entered */
   {
   off = 0;
   pixdim = 0;
   for (nr=0; nr<3; nr++)
      {
      dd1[nr] = dd2[nr] = dd3[nr] = 0.0;
      }
   for (nr=0; nr<3; nr++)
      {
      k = CGN_EXTRSS(cbuf,(int)strlen(cbuf),',',&off,subs[nr],30);
      if (k < 1)
         break;
      else
         pixdim = nr + 1;
      }


   if (direc == 2)			/* we entered screen pixels */
      {
      for (nr=0; nr<pixdim; nr++)
         {
         stat = CGN_CNVT(&subs[nr][1],4,1,&nval,&rr,&dd1[nr]);
         if (stat != 1)
            SCETER(2,"Invalid syntax in screen pixels...");
         }

      stat = Pixconv("SRW",imnoa,dd1,dd2,dd3);
      if (stat != 0) 
         SCETER(4,"problems in conversion to frame pixels + world coords...");

      sublo[0] = (int) dd1[0];
      sublo[1] = (int) dd1[1];
      sublo[2] = (int) dd1[2];
      if (pixdim == 1)
         (void) sprintf(auxstr,"screen pixel entered = %d",sublo[0]);
      else if (pixdim == 2)
         (void) sprintf(auxstr,"screen pixels entered = %d, %d",
                        sublo[0],sublo[1]);
      else if (pixdim == 3)
         (void) sprintf(auxstr,"screen pixels entered = %d, %d, %d",
                        sublo[0],sublo[1],sublo[2]);
      SCTPUT(auxstr);
      (void) SCKWRI("OUTPUTI",sublo,1,3,&unit);

      sublo[0] = (int) dd2[0];
      if (pixdim == 1)
         (void) sprintf(auxstr,"corresponding frame pixel = %d",sublo[0]);
      else if (pixdim == 2)
         {
         sublo[1] = (int) dd2[1];
         (void) sprintf(auxstr,"corresponding frame pixels = %d,%d",
                        sublo[0],sublo[1]);
         }
      else
         {
         sublo[1] = (int) dd2[1];
         sublo[2] = (int) dd2[2];
         (void) sprintf(auxstr,"corresponding frame pixels = %d,%d,%d",
                        sublo[0],sublo[1],sublo[2]);
         }
      SCTPUT(auxstr);
      (void) SCKWRD("OUTPUTD",dd2,10,3,&unit); 

      for (nr=0; nr<3; nr++) dd2[nr] = dd3[nr];

      dspflag = 1;			/* avoid redundant screen pixel calc */
      goto go_world;
      }


   for (nr=0; nr<pixdim; nr++)
      {
      if (subs[nr][0] == '<')
         dd1[nr] = 1.0;
      else if (subs[nr][0] == '>')
         dd1[nr] = (double) npix[nr];
      else if (subs[nr][0] == 'C')
         {
         k = npix[nr]/2;
         dd1[nr] = (double) k;
         }
      else				/* remains only "@" format */
         stat = CGN_CNVT(&subs[nr][1],4,1,&nval,&rr,&dd1[nr]);
         if (stat != 1)
            SCETER(2,"Invalid syntax in frame pixels...");
      }


   /* split according to NAXIS */

   sublo[0] = (int) dd1[0];
   if (pixdim == 1)
      (void) sprintf(auxstr,"frame pixel entered = %d",sublo[0]);

   else if (pixdim == 2)
      {
      sublo[1] = (int) dd1[1];
      (void) sprintf(auxstr,"frame pixels entered = %d,%d",sublo[0],sublo[1]);
      }
   else
      {
      sublo[1] = (int) dd1[1];
      sublo[2] = (int) dd1[2];
      (void) sprintf(auxstr,"frame pixels entered = %d,%d,%d",sublo[0],sublo[1],                     sublo[2]);
      }
   SCTPUT(auxstr);
   (void) SCKWRD("OUTPUTD",dd1,10,3,&unit);      /* save frame pixels */


   stat = fp2wc(1,imnoa,dd1,dd2);		/* convert fp -> wc */
   if (stat != 0) 
      SCETER(4,"problems in conversion to world coords...");


   /* split according to NAXIS */

go_world:
   if (pixdim == 1)
      (void) sprintf(cbuf,"world coords = %10.8g",dd2[0]);

   else if (pixdim == 2)
      {
      if (linflag == 0)
         {
         form_sexa(1,dd2[0],auxstr);			/* RA */
         (void) strcpy(convstr,auxstr);
         for (nr=0; nr<80; nr++)
            {
            if (convstr[nr] == ':')
               convstr[nr] = ',';
            else if (convstr[nr] == '\0')
               break;
            }
         (void) CGN_CNVT(convstr,4,3,subhi,rval,dd3);
         (void) SCKWRD("OUTPUTD",dd3,4,3,&unit);
         (void) sprintf(cbuf,"world coords = %10.8g, %10.8g  or  %s, ",
                        dd2[0],dd2[1],auxstr);

         form_sexa(0,dd2[1],auxstr);			/* DEC */
         (void) strcpy(convstr,auxstr);
         for (nr=0; nr<80; nr++)
            {
            if (convstr[nr] == ':')
               convstr[nr] = ',';
            else if (convstr[nr] == '\0')
               break;
            }
         (void) CGN_CNVT(convstr,4,3,subhi,rval,dd3);
         (void) SCKWRD("OUTPUTD",dd3,7,3,&unit);
         (void) strcat(cbuf,auxstr);
         }
      else
         (void) sprintf(cbuf,"world coords = %10.8g,%10.8g",dd2[0],dd2[1]);
      }

   else
      {
      if (linflag == 0)
         {
         form_sexa(1,dd2[0],auxstr);                    /* RA */
         (void) strcpy(convstr,auxstr);
         for (nr=0; nr<80; nr++)
            {
            if (convstr[nr] == ':')
               convstr[nr] = ',';
            else if (convstr[nr] == '\0')
               break;
            }
         (void) CGN_CNVT(convstr,4,3,subhi,rval,dd3);
         (void) SCKWRD("OUTPUTD",dd3,4,3,&unit);
         (void) sprintf(cbuf,"world coords = %10.8g, %10.8g, %10.8g",
                        dd2[0],dd2[1],dd2[2]);

         SCTPUT(cbuf);
         (void) sprintf(cbuf,"   or  %s, ",auxstr);
         form_sexa(0,dd2[1],auxstr);                    /* DEC */
         (void) strcpy(convstr,auxstr);
         for (nr=0; nr<80; nr++)
            {
            if (convstr[nr] == ':')
               convstr[nr] = ',';
            else if (convstr[nr] == '\0')
               break;
            }
         (void) CGN_CNVT(convstr,4,3,subhi,rval,dd3);
         (void) SCKWRD("OUTPUTD",dd3,7,3,&unit);
         (void) strcat(cbuf,auxstr);
         (void) sprintf(auxstr,", %10.8g",dd2[2]);
         (void) strcat(cbuf,auxstr);
         }
      else
         (void) sprintf(cbuf,"world coords = %10.8g, %10.8g, %10.8g",
                        dd2[0],dd2[1],dd2[2]);
      }

   (void) SCKWRD("OUTPUTD",dd2,1,3,&unit);		/* save dec. wc */
   }
SCTPUT(cbuf);


if (tbl_flag == 1) goto input_string;

if (dspflag != 0) return;		/* no display window there */


/* get display info + check, if our frame is loaded */

stat = DCGICH(QIMCH);
if (stat == 0) 
   {
   (void) SCKGETC("IDIMEMC",1,80,&nval,cbuf);    /* get name of loaded frame */
   stat = CGN_EQUAL(infile,cbuf);
   if (stat == 0)		/* Yes. It's our frame which is loaded */
      {
      stat = Pixconv("INIT",imnoa,dd1,dd2,dd3);

      dd1[0] = (double) sublo[0];		/* sublo holds frame pixels */
      dd1[1] = (double) sublo[1];
      dd1[2] = (double) sublo[2];
      stat = Pixconv("_RS",imnoa,dd1,dd2,dd3);

      sublo[0] = (int) dd3[0];
      sublo[1] = sublo[2] = 0;
      if (pixdim == 1)
         (void) sprintf(auxstr,"corresponding screen pixel = %d",sublo[0]);
      else if (pixdim == 2)
         {
         sublo[1] = (int) dd3[1];
         (void) sprintf(auxstr,"corresponding screen pixels = %d, %d",
                        sublo[0],sublo[1]);
         }
      else if (pixdim == 3)
         {
         sublo[1] = (int) dd3[1];
         sublo[2] = (int) dd3[2];
         (void) sprintf(auxstr,"corresponding screen pixels = %d, %d, %d",
                        sublo[0],sublo[1],sublo[2]);
         }
      SCTPUT(auxstr);
      (void) SCKWRI("OUTPUTI",sublo,1,3,&unit);
      }
   }
}

/*

*/

int AnyStuff(dazhld,option,ibuf)
int  *dazhld, option, *ibuf;

{
int    dispno, actvals, unit, stat, kbuf[20];
int    nr, jr;

float  rdum[20];

char   line[80], cdum[84], cg, cd, cc, attrib[16], parent[8];






(void) SCKGETC("MID$SESS",1,12,&actvals,cdum);


/* option > 100 => inspection of idi structures */

if (option > 100)
   {
   DCOPEN(1);
   dispno = QDSPNO;

   goto send_event;
   }


/* options 10, 11, ... processed here */

if (option > 9)
   {
   if (option == 10)
      {
      (void) sprintf(line,"display/graphics setup for Midas session %c%c",
                     cdum[10],cdum[11]);
      SCTPUT(line);
      SCTPUT("-------------------------------------------");
      if (cdum[5] == '-') cdum[5] = ' ';
      if (cdum[6] == '-') cdum[6] = ' ';
      if ((cdum[5] == ' ') && (cdum[6] == ' '))
         {
         SCTPUT("no display/graphics window exists...");
         return 0;
         }

      (void) SCKRDI("DAZDEVR",1,11,&actvals,kbuf,&unit,&nr);
      cg = kbuf[10] + 48;		/* current graphics w. */
      cd = kbuf[9] + 48;		/* current display w. */

      stat = IIDOPN_C("  ",&dispno);          /* just get a connection */
      stat = IIEGDB_C(dispno,2,0,cdum,kbuf,rdum);
      (void) IIDCLO_C(dispno);

      unit = 0;
      (void) SCKWRI("OUTPUTI",kbuf,1,20,&unit);
      (void) SCKWRC("OUTPUTC",1,cdum,1,20,&unit);

      for (nr=0; nr<20; nr+=2)
         {
         if (cdum[nr] != ' ')		/* entry used */
            {
            if (kbuf[nr+1] == -1)		/* parent window  = root */
               (void) strcpy(parent,"root");
            else
               {
               jr = kbuf[nr+1] * 2;
               if (cdum[jr] == 'g')
                  (void) sprintf(parent,"g%c",cdum[jr+1]);
               else
                  (void) sprintf(parent,"d%c",cdum[jr+1]);
               }
            cc = cdum[nr+1];			/* current display/graph id */
            if (cdum[nr] == 'g')
               {
               (void) strcpy(attrib,"graphics w.");
               if (cc == cg)
                  (void) sprintf(line,
                                 "%s %c (active graphics)  parent w.: %s",
                                 attrib,cc,parent); 
               else
                  (void) sprintf(line,
                                 "%s %c                    parent w.: %s",
                                 attrib,cc,parent); 
               }
            else
               {
               (void) strcpy(attrib,"display w. ");
               if (cc == cd)
                  (void) sprintf(line,
                                 "%s %c (active display)   parent w.: %s",
                                 attrib,cc,parent); 
               else
                  (void) sprintf(line,
                                 "%s %c                    parent w.: %s",
                                 attrib,cc,parent); 
               }
            SCTPUT(line);

            if (kbuf[nr] == 1)			/* are we a parent window? */
               {
               actvals = nr/2;			/* slot no. (!= w. id) */
               (void) strcpy(line," subwindows: ");	/* length = 13 */
               for (jr=0; jr<20; jr+=2)
                  {
                  if (kbuf[jr+1] == actvals)		/* subwindow found */
                     {
                     if (cdum[jr] == 'g')
                        (void) sprintf(attrib,"g%c, ",cdum[jr+1]);
                     else
                        (void) sprintf(attrib,"d%c, ",cdum[jr+1]);
                     (void) strcat(line,attrib);
                     }
                  }
               jr = (int)strlen(line);
               if (jr > 13)		/* found at least 1 subwindow */
                  {
                  line[jr-2] = '\0';	/* strip last comma */
                  SCTPUT(line);
                  } 
               } 
            } 
         } 
      }
   return 0;
   }



/* check that we do have a window on the screen */
 
if (ibuf[0] > 99)			/* was ibuf[1] originally */
   {					/* for graphics window */
   if ((cdum[6] == ' ') || (cdum[6] == '-') || (cdum[6] == '\0'))
      SCETER(11,"No active graphics window exists...");

   DCOPEN(2);
   dispno = GDSPNO;
   ibuf[0] -= 100;			/* reset graphics id to orig. value */
   }

else
   {
   if ((cdum[5] == ' ') || (cdum[5] == '-') || (cdum[5] == '\0'))
      SCETER(11,"No active display window exists...");

   DCOPEN(1);
   dispno = QDSPNO;
   }

send_event:
cdum[12] = '\0';			/* make sure, we have an end... */
DCSINF(dispno,option,ibuf,rdum,cdum);		/* send request to IDIserver */
DCCLOS(dispno);	
return 0;
}

