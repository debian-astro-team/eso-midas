/* @(#)trace.c	19.1 (ESO-DMD) 02/25/03 14:00:30 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT:  Copyright (c) 1994 European Southern Observatory,
                                         all rights reserved
.IDENT       TRACE
.LANGUAGE    C
.AUTHOR      K. Banse		ESO-IPG Garching
.KEYWORDS:   Image Display, graphic trace
.PURPOSE:    a) produce an interactive trace on the display
             b) extract a rotated subimage from the displayed image
.ALGORITHM:  a) use cursor 2 to change the slope of the trace
                and cursor 1 as rotation point 
                (or vice versa), both cursors are moved via the joystick
                Extra: use cursor 1 to create the full tilted rectangle
             b) use one cursor to define the line we are working on

.INPUT/OUTPUT:
  the following keywords are used:

  ACTION/C/1/1		'1' for 1-dim trace  (a)
			'2' for 2-dim trace  (b)
			'3' for 1-dim row trace  (c)
			'4' for 1-dim column trace  (d)
  DAZHOLD/I/1/13	cursor no., form + other IDIDEV info

  for a)
  INPUTD/D/1/1		stepsize for traced line (will be new image)
  INPUTI/I/10/1	max. dimension of extracted spectrum
  DEFAULT/C/1/2	(1:1) = P for plotting, = N for not plotting
  			(2:2) = Y for trace just between cursors, else complete
			        line

  for b)
  DAZHOLD/I/1/2		cursor no., form
  INPUTD/D/1/2			stepsize along base line + in between b.lines


.VERSIONS    1.00       940603  from TRACE.FOR    RvH
------------------------------------------------------------*/

/* Define _POSIX_SOURCE to indicate that this is a POSIX program */

#define  _POSIX_SOURCE 1

#include <midas_def.h>
#include <idinumd.h>
 
/*

*/

int main()

{
int    actvals, imf, knul, mxsize, overlay, unit;

double stepln[2];

char   action[2], outfra[72];


/* initialize MIDAS and global display variables: dzdev & dzmemi */

(void) SCSPRO("TRACE");
DCOPEN(1);


/* read key DAZHOLD */

(void) SCKRDI("DAZHOLD",13,1,&actvals,&overlay,&unit,&knul);


/* clear graphics channel and set scroll values as image channel */

CONCHA_C(QDSPNO,QOVCH,1,0);
if (overlay == QIMCH)
   (void) Cdazzsc(QDSPNO,QOVCH,ZOOMX,&SCROLX,&SCROLY);


(void) SCFOPN(dzmemc,D_R4_FORMAT,0,F_IMA_TYPE,&imf);    /* get image number */

/* get step-size for line, name of output frame and type of trace
   and max. mapping size from key MONITPAR                         */

(void) SCKRDD("INPUTD",1,2,&actvals,stepln,&unit,&knul);
(void) SCKGETC("OUT_A",1,60,&actvals,outfra);
(void) SCKGETC("ACTION",1,1,&actvals,action);
(void) SCKRDI("MONITPAR",20,1,&actvals,&mxsize,&unit,&knul);
mxsize *= mxsize;                                          /* a square frame */


if (*action == '2')
   {
   int errcnt = 0;

   while ( (TRACE2D(imf,stepln,outfra)) != 0 && (++errcnt < 10) )
      SCTPUT("Try again...");

   if (errcnt == 10) SCETER(1,"TRACE2D: Trace outside image...");
   }

else
   TRACE1D(action,mxsize,imf,*stepln,outfra);


DCCLOS(QDSPNO);	 /* close display and save global array dzdev */

return SCSEPI();
}      
