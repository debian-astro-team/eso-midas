/*===========================================================================
  Copyright (C) 1989-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE      C
.IDENT         psbdf.c
.AUTHOR        Preben Grosbol  (ESO-IPG)
.KEYWORDS      hardcopy, PostScript
.ENVIRONMENT   MIDAS
.COMMENT       Program uses OSA-routines to write PostScript file to disk
.PURPOSE       Create a PostScript hardcopy file from a MIDAS frame
.VERSION       1.0   890617: Creation,     PJG
.VERSION       1.10  890622: merge into MIDAS pipeline  KB
.VERSION       1.20  900720: Use PS-image library routines, PJG
.VERSION       1.30  900724: handle keyword HCITTLUT and other things  KB
.VERSION       1.40  900809: Scale I1 images + change psopen, PJG
.VERSION       1.50  900824: Explicit fonts + change of label, PJG
.VERSION       1.60  901205: treat ITTs + LUTs correctly  KB
.VERSION       1.70  910130: Correct resolution, PJG
.VERSION       1.75  910314: ITTs + LUTs are now in MID_WORK, KB
.VERSION       1.80  910604: add flag (in par P6) for text - yes or no, KB
.VERSION       1.85  910920: correct ITT/LUT scaling, PJG
.VERSION       1.90  911004: correct error for color prints, PJG
.VERSION       1.95  911009: adjust default A4 format, PJG
.VERSION       2.00  920729: change format definitions, PJG
.VERSION       2.05  930302: Remove default pixel resolution, PJG
.VERSION       2.10  930309: Change call-seq. of psline, PJG
.VERSION       3.00  930929: long int -> int, unit -> *int (for OSF1) KB
.VERSION       3.10  940513: work on different ITT/LUT sizes, KB

 090709		last modif
------------------------------------------------------------------------*/
 
#include   <midas_def.h>
#include <idinumd.h>

#include <stdio.h>


#define    MAXDIM               2     /* Max. dimension to treat         */
#define    MXITF             256     /* Max. size of ITT's and LUT's    */
#define    PLCOLS              9     /* Current no. of plot colours    */
#define    PSFONT    "Helvetica"     /* Standard PS font used           */
#define    PPCM           28.246     /* PS-points per cm                */

static int   e_c = 1;
static int   e_log = 0; 
static int   e_disp = 0;
static int   ln, ln1, ln2;
static int   RGBmode;


int main()

{
char   c, *cptr;
char   ident[76], fname[64], pname[64], text[84];
char   versio[6], rootfr[64], mode[5];
char   tname[64], lname[64], date[30];
char   *pbi, *pgi, *pri, *px;

int  npl1, datforma, n, err, ioff;
int  lsmode, nbpp, fmode, imode, bwi, bgc, txtyes, i1s;
int  nsx, nsy, hfsize, dfsize, cfsize, lfsize, ditt, dlut;
int  unit, bmap[MXITF], gmap[MXITF], rmap[MXITF];
int  npix[MAXDIM], itt[MXITF], pindx[PLCOLS];
int  ncol[3], tid, idf, ino, rno, itfsize, info[5];
int  size, dno, bpe, iva, inull[3];
int  copydsp, dzdev[20];
int  gettab();
register int i;

float   *fpntr, *pf, cuts[4], val[3];
float   xs, ys, xe, ye, dx, dy, fac, x;
float   fxo, fyo, fxs, fys, cxo, cyo, cxs, cys, coff, als;
float   hxo, hyo, hys, mxo, cl, ch;
float   rarr[MXITF], garr[MXITF], barr[MXITF], psfmt[6];

double  angle, psxsiz, psysiz, psxoff, psyoff, resbuf[2];
double  start[MAXDIM], step[MAXDIM], end[2];




SCSPRO("psbdf");                     /* initiate MIDAS environment   */

rootfr[60] = '\0';
fname[60] = '\0';
fpntr = (float *) 0;


/*

get PostScript parameters defining the page format (portrait mode):

Variable                default      Remark
------------------------------------------------------------------
psfmt[0] = PSRES           0.00      Pixel resolution (used in rebinning)
psfmt[1] = RES             0.00      Dots per cm (Use printer default)
psfmt[2] = XSIZE          21.00      Page size in X axis (cm)
psfmt[3] = YSIZE          29.73      Page size in Y axis (cm)   
psfmt[4] = XMARGIN         2.50      Page margin in X axis (cm)  
psfmt[5] = YMARGIN         3.00      Page margin in Y axis (cm)

*/

(void) SCKRDR("POSTSCRIPT",1,6,&iva,psfmt,&unit,inull);
if (psfmt[0] < 0.0) psfmt[0] = 0.0;	         /* default: no setscreen */
if (iva<6) psfmt[4] = 2.5, psfmt[5] = 3.0;


/* get display characteristics */

(void) SCKRDI("IDIDEV",1,20,&iva,dzdev,&unit,inull);


/* read FRAME name and open it  */

(void) SCKGETC("IN_A",1,60,&iva,fname);
(void) SCFINF(fname,2,info);

RGBmode = QRGBFL;			/* get colour mode in use */
if (RGBmode == 0)
   {					/* handle 16-, 24-bit displays */
   if (QDDEP != 8) RGBmode = 3;
   }
if (info[1] == D_I1_FORMAT)
   datforma = D_I1_FORMAT;
else if (info[1] == D_I4_FORMAT)
   datforma = D_I4_FORMAT;
else
   datforma = D_R4_FORMAT;
inull[0] = inull[1] = inull[2] = 0;


(void) SCFOPN(fname,datforma,F_I_MODE,F_IMA_TYPE,&idf);
(void) SCDRDI(idf,"NAXIS",1,1,&iva,&nsx,&unit,inull);
if (nsx < 2)
   SCETER(19,"invalid NAXIS of input frame...");

(void) SCDRDI(idf,"NPIX",1,MAXDIM,&iva,npix,&unit,inull);
(void) SCDRDD(idf,"START",1,MAXDIM,&iva,start,&unit,inull);
(void) SCDRDD(idf,"STEP",1,MAXDIM,&iva,step,&unit,inull);
(void) SCDRDR(idf,"LHCUTS",1,4,&iva,cuts,&unit,inull);
(void) SCDGETC(idf,"IDENT",1,72,&iva,ident);
(void) SCDFND(idf,"VCOPY-LUTSIZE",&c,&dno,&bpe);
if (c == 'I')
   {
   (void) SCDRDI(idf,"VCOPY-LUTSIZE",1,1,&iva,&itfsize,&unit,inull);
   if (itfsize > MXITF) itfsize = MXITF;
   copydsp = 1;
   }
else
   {
   itfsize = MXITF;
   copydsp = 0;
   }

nsx = npix[0]; nsy = npix[1];
xs = start[0]; ys = start[1];

if (cuts[0] >= cuts[1])
   {
   cl = cuts[2];
   ch = cuts[3];
   }
else
   {
   cl = cuts[0];
   ch = cuts[1];
   }



/* set defaults and define page layout parameters       */

(void) SCKRDC("P6",1,1,5,&iva,mode,&unit,inull);
lsmode = (mode[0]=='L' || mode[0]=='l');	      /* landscape mode  */
bwi = ((mode[1]=='C') || (mode[1]=='c')) ? 0 : 1;     /* colour mode     */
nbpp = (mode[2]=='4') ? 4 : 8;		              /* bits per pixel  */
bgc = (mode[3]=='B' || mode[3]=='b');		      /* background flag */
txtyes = ((mode[4]=='Z') || (mode[4]=='z')) ? 0 : 1;  /* text flag       */
i1s = (itfsize != (ch-cl+1));                 /* I1 scaling      */

fmode = (txtyes == 0) ? 0 : 1;


/* define the page layout        */

lfsize = 8;                       /* Axes Label font size (pt)           */
cfsize = 11;                      /* Caption font size (pt)              */
dfsize = 11;                      /* Header date font size (pt)          */
hfsize = 11;                      /* Header version font size (pt)       */

cxs  = 8.0;                       /* Caption field size in X             */
cys  = 10*(cfsize+4)/PPCM;        /* Caption field size in Y (10 lines)  */
coff = 2.3;                       /* Caption offset for text column      */
hys  = (hfsize+4)/PPCM;           /* Header  field size in Y             */
als  = (lfsize+4)/PPCM;           /* Axes label height                   */

angle = (lsmode) ? 90.0 : 0.0;    /* Angle for landscape or portrait     */

if (txtyes == 0) cxs = cys = hys = als = 0.0;

dx = (lsmode) ? (psfmt[3]-2.0*psfmt[5]-cxs-als)/nsx : /* Pixel step in X */
                (psfmt[2]-2.0*psfmt[4]-als)/nsx;
dy = (lsmode) ? (psfmt[2]-2.0*psfmt[4]-als)/nsy :     /* Pixel step in Y */
                (psfmt[3]-2.0*psfmt[5]-cys-als)/nsy;
fac = (dx<dy) ? dx : dy;                          /* Take smaller step   */
fxs = fac * nsx;                                  /* Frame size in X     */
fys = fac * nsy;                                  /* Frame size in Y     */
fxo = (lsmode) ? als :                            /* Frame origin in X   */
                 0.5*(psfmt[2]-fxs)-psfmt[4]+als; 
fyo = (lsmode) ? psfmt[2]-2.0*psfmt[4]-fys-hys+als : /* Frame origin  Y  */
                 psfmt[3]-2.0*psfmt[5]-fys-hys;
cxo = (lsmode) ? fxo+fxs+1.0 : fxo;               /* Caption origin in X */
cyo = (lsmode) ? fyo+fys-0.3 : fyo-1.5;           /* Caption origin in Y */
hxo = fxo + fxs - 0.37*hfsize;                    /* Header origin in X  */
hyo = fyo + fys + 0.15;                           /* Header origin in Y  */

/*
hxs = fxs;                                        // Header size in X    //
wxo = fxo + fxs + 0.20;                           // Wedge origin in X   //
wyo = fyo;                                        // Wedge origin in Y   //
wxs = 0.30;                                       // Wedge size in X     //
wys = fys;                                        // Wedge size in Y     //
*/

mxo = (lsmode) ? psfmt[3]-2.0*psfmt[5]-4.0 :      /* MIDAS origin in X   */
                 psfmt[2]-2.0*psfmt[4]-4.0;


/* open PostScript file name  */

psxsiz = psfmt[2];
psysiz = psfmt[3];
psxoff = psfmt[4];
psyoff = psfmt[5];
resbuf[0] = psfmt[0];
resbuf[1] = psfmt[1];

if (psopen(fname,ident,psxsiz,psysiz,psxoff,psyoff,angle,resbuf) < 0)
   {
   printf("%s\n",osmsg());
   SCETER(9,"Error: cannot open PostScript output file");
   }

psmode(bgc, 0.0, 0.0, 1.0);


/*  write name of PostScript file into OUT_A keyword */

for (n=0; (c=fname[n]) && c!=' ' && c!='.'; n++) pname[n] = c;
for (i=0; (pname[n++] = ".ps"[i]); i++);
err = SCKWRC("OUT_A",1,pname,1,60,&unit);


/* find out where we get the ITT from  */

fac = itfsize - 1;
tid = gettab("ITT",tname);

if (RGBmode != 0)
   {
   char  dum[4];

   (void) SCKGETC("P3",1,1,&iva,dum);
   iva = itfsize - 1;
   if ((dum[0] == 'p') || (dum[0] == 'P'))
      {
      for (i=0; i<itfsize; i++) rarr[i] = i / fac;
      }
   else
      {
      for (i=0; i<itfsize; i++) rarr[i] = (iva-i) / fac;
      }
   }

else
   {
   if (tid >= 0)
      {
      (void) TCCSER(tid,":ITT",ncol);

      npl1 = 1;
      for (n=0; n<itfsize; n++)
         {
         (void) TCRRDR(tid,npl1,1,ncol,&rarr[n],inull);
         npl1 ++;
         }
      (void) TCTCLO(tid);
      }
   
   else if (tid == -99)       /* read ITT from descriptor MIDAS_ITT in frame  */
      {
      err = SCDFND(idf,"MIDAS_ITT",&c,&dno,&bpe);
      ditt = ((c=='I' || c=='R') &&  dno==itfsize);

      if (ditt) 
         {
         if (c=='I') 
	    {
            err = SCDRDI(idf,"MIDAS_ITT",1,itfsize,&iva,itt,&unit,inull);
	    for (i=0; i<itfsize; i++) rarr[i] = itt[i]/fac;
            }
         else
            err = SCDRDR(idf,"MIDAS_ITT",1,itfsize,&iva,rarr,
                         &unit,inull);
         }
      else
         SCETER(18,"invalid descr. MIDAS_ITT ...");
      }

   else if (tid == -1)			/* use negative conversion */
      {
      iva = itfsize - 1;
      for (i=0; i<itfsize; i++) rarr[i] = (iva-i) / fac;
      } 
   else					/* only positive conversion left */
      {
      for (i=0; i<itfsize; i++) rarr[i] = i / fac;
      }
   }


/* scale ITT  */

if (bwi)
   {
   fac = 255.0;
   for (i=0; i<itfsize; i++)
      itt[i] = CGN_NINT(fac * rarr[i]);      /* 0. -> 1. => 0 -> 255 */

   pindx[0] = CGN_NINT(fac * rarr[0]);	     
   pindx[1] = pindx[0];
   pindx[2] = CGN_NINT(fac * rarr[itfsize-1]);	     
   iva = itfsize * 0.3;
   pindx[3] = CGN_NINT(fac * rarr[iva]);
   pindx[4] = pindx[3];
   pindx[5] = pindx[3];
   iva = itfsize * 0.6;
   pindx[6] = CGN_NINT(fac * rarr[iva]);
   pindx[7] = pindx[6];
   pindx[8] = pindx[6];
   }

else
   {
   tid = gettab("LUT",lname);

   if (RGBmode != 0)
      {
      fac = 255.0;

      for (i=0; i<itfsize; i++)			/*  build linear LUT  */
         {
         rmap[i] = i;
         gmap[i] = i;
         bmap[i] = i;
         itt[i] = i;
         }
      }

   else				/* find out where we get the LUT from  */
      {
      for (i=0; i<itfsize; i++)
         itt[i] = CGN_NINT(fac * rarr[i]);    /* 0. -> 1. => 0 -> (itfsize-1) */

      fac = 255.0;

      if (tid >= 0)
         {
         (void) TCCSER(tid,":RED",ncol);
         (void) TCCSER(tid,":GREEN",&ncol[1]);
         (void) TCCSER(tid,":BLUE",&ncol[2]);

         npl1 = 1;
         for (n=0; n<itfsize; n++)
            {
            (void) TCRRDR(tid,npl1,3,ncol,val,inull);
            if (nsx < 1) 
               {
               printf("nsx = %d, npl1 = %d\n",nsx,npl1);
               printf("val = %f, %f, %f\n",val[0],val[1],val[2]);
               }
            npl1 ++;
            rarr[n] = val[0];
            garr[n] = val[1];
            barr[n] = val[2];
            }
         err = TCTCLO(tid);
         }

      else                 /* read LUT from descriptor MIDAS_LUT in frame  */
         {
         err = SCDFND(idf,"MIDAS_LUT",&c,&dno,&bpe);
         dlut = ((c=='I' || c=='R') &&  dno==3*MXITF);
   
         if (dlut)		/* these guys are always 256 entries long */
            {
            if (c=='I')
               {
               err = SCDRDI(idf,"MIDAS_LUT",1,MXITF,&iva,
                            rmap,&unit,inull);
	       for (i=0; i<MXITF; i++) rarr[i] = rmap[i]/fac;
               ln = MXITF + 1;
               err = SCDRDI(idf,"MIDAS_LUT",ln,MXITF,&iva,
                            gmap,&unit,inull);
	       for (i=0; i<MXITF; i++) garr[i] = gmap[i]/fac;
               ln = 2*MXITF + 1;
               err = SCDRDI(idf,"MIDAS_LUT",ln,MXITF,&iva,
                            bmap,&unit,inull);
	       for (i=0; i<MXITF; i++) barr[i] = bmap[i]/fac;
               }
            else
               {
               err = SCDRDR(idf,"MIDAS_LUT",1,MXITF,&iva,
                            rarr,&unit,inull);
               ln = MXITF + 1;
               err = SCDRDR(idf,"MIDAS_LUT",ln,MXITF,&iva,
                            garr,&unit,inull);
               ln = 2*MXITF + 1;
               err = SCDRDR(idf,"MIDAS_LUT",ln,MXITF,&iva,
                            barr,&unit,inull);
               }
            }
      
         else 
            SCETER(18,"invalid descr. MIDAS_LUT ...");
      }

      for (i=0; i<itfsize; i++)			/*  scale LUT  */
         {
         rmap[i] = CGN_NINT(fac * rarr[i]);	/* 0 -> 1 => 0 -> 255 */
         gmap[i] = CGN_NINT(fac * garr[i]);
         bmap[i] = CGN_NINT(fac * barr[i]);
         }
      }
   }


/* read descriptors ROOT_FRAME, ROOT_CUTS + END if available */

SCECNT("GET",&ln,&ln1,&ln2);
SCECNT("PUT",&e_c,&e_log,&e_disp);
err = SCDGETC(idf,"ROOT_FRAME",1,60,&iva,rootfr);
if (err != 0) strcpy(rootfr,fname);
err = SCDRDR(idf,"ROOT_CUTS",1,2,&iva,val,&unit,inull);
if (err != 0) 
   {
   val[0] = cl;
   val[1] = ch;
   }
err = SCDRDD(idf,"END",1,2,&iva,end,&unit,inull);
if (err != 0) 
   {
   xe = xs + dx*(nsx-1);
   ye = ys + dy*(nsy-1);
   }
else
   {
   xe = end[0];
   ye = end[1];
   }
SCECNT("PUT",&ln,&ln1,&ln2);


/* define Intensity Transfer and Color Look-Up Tables */

if (bwi)
   {
   imode = 0;

   if (copydsp == 1)
      {
      pindx[0] = CGN_NINT(fac * rarr[0]);
      pindx[1] = pindx[0];
      pindx[2] = CGN_NINT(fac * rarr[itfsize-1]);
      iva = itfsize * 0.3;
      pindx[3] = CGN_NINT(fac * rarr[iva]);
      pindx[4] = pindx[3];
      pindx[5] = pindx[3];
      iva = itfsize * 0.6;
      pindx[6] = CGN_NINT(fac * rarr[iva]);
      pindx[7] = pindx[6];
      pindx[8] = pindx[6];

      if (RGBmode == 0)
         {
         iva = itfsize-PLCOLS;
         itt[iva++] = pindx[0];				/* black */
         itt[iva++] = pindx[1];				/* black */
         itt[iva++] = pindx[2];				/* white */
         itt[iva++] = pindx[3];				/* red */
         itt[iva++] = pindx[4];				/* green */
         itt[iva++] = pindx[5];				/* blue */
         itt[iva++] = pindx[6];				/* yellow */
         itt[iva++] = pindx[7];				/* magenta */
         itt[iva] = pindx[8];				/* cyan */
         }
      }

   psitf(imode,itfsize,itt, (int*) 0,(int *) 0, (int *) 0);
   }
else
   {
   if (RGBmode == 0)
      imode = 1;
   else
      imode = 2;
   psitf(imode,itfsize,itt,rmap,gmap,bmap);
   }



/* write text and comments */

err = SCKRDC("MID$SESS",1,16,5,&iva,versio,&unit,inull);
versio[5] = '\0';
OSY_ASCTIM(date);

if (txtyes == 1)
   {
   (void) sprintf(text,"MIDAS version: %s",versio);
   if (8.0<fxs) pslabel(hxo,hyo,PSFONT,hfsize,text);

   pslabel(fxo,hyo,PSFONT,dfsize,date);

   pscapt(cxo,cyo,PSFONT,cfsize,coff);
   pstext("Frame",rootfr);
   pstext("Identifier",ident);
   pstext("ITT-table",tname);
   if (bwi != 1)
      {
      if (strcmp(lname,"middumml.lut") == 0)
         {
         err = SCKGETC("MYTABL",21,20,&iva,text);
         (void) strcpy(lname,text);
         i = CGN_INDEXS(text,".lut");
   
         if (i <= 0) 
            {				/* we have to insert ".lut" */
            i = CGN_INDEXC(text,' ');	/* find first blank */
            if (i > 0)
               {
               n = i;
               text[i] = '\0';
               (void) strcpy(&lname[n],".lut");
               n += 4;
               lname[n] = ' ';
               (void) strcpy(&lname[++n],&text[++i]);
               }
            }
         }
      pstext("LUT-table",lname);
      }
   (void) sprintf(text,"%g, %g : %g, %g",xs,ys,xe,ye);
   pstext("Coordinates",text);
   (void) sprintf(text,"1, 1 : %d, %d",nsx,nsy);
   pstext("Pixels",text);
   (void) sprintf(text,"%g, %g",val[0],val[1]);
   pstext("Cut values",text);
   err = SCKGETC("USER",1,20,&iva,text);
   pstext("User",text);
   (void) sprintf(text,"ESO-MIDAS(%s); %s",versio,date);
   pslabel(mxo,0.0,PSFONT,5,text);
   }

psframe(fmode,fxo,fyo,fxs,fys,PSFONT,lfsize,xs,ys,xe,ye);
psimage(nsx,nsy,0,nbpp);

size = nsx;
pbi = pgi = pri = 0;
ioff = 1;


if (copydsp == 1)
   {
   char  *impntr;

   (void) SCFCRE("dummy",datforma,F_X_MODE,F_IMA_TYPE,size,&ino);
   (void) SCFMAP(ino,F_X_MODE,1,size,&ln,&impntr);
   if (RGBmode == 0)
      {
      for (n=0; n<nsy; n++)
         {
         err = SCFGET(idf,ioff,nsx,&iva,impntr);
         pbi = impntr;
         psline((unsigned char *)pbi, (unsigned char *)pgi, 
                (unsigned char *)pri);
         ioff += nsx;
         }
      }
   else
      {
      int   rgbpix, inr, ing, inb, *jpntr;
      char  *tr, *tg, *tb;
 
      (void) SCFCRE("bdummy",D_I1_FORMAT,F_X_MODE,F_IMA_TYPE,size,&inb);
      (void) SCFMAP(inb,F_X_MODE,1,size,&ln,&pbi);
      if (bwi)
         {
         for (n=0; n<nsy; n++)
            {
            err = SCFGET(idf,ioff,nsx,&iva,impntr);
            jpntr = (int *) impntr;
            tr = pbi; 

            for (i=0; i<nsx; i++)
               {
               rgbpix = *jpntr++;
               *tr++ = (unsigned char) (rgbpix & 0xff);
               }
            psline((unsigned char *)pbi, (unsigned char *)pgi,
                   (unsigned char *)pri);
            ioff += nsx;
            }
         }
      else
         {
         (void) SCFCRE("rdummy",D_I1_FORMAT,F_X_MODE,F_IMA_TYPE,size,&inr);
         (void) SCFMAP(inr,F_X_MODE,1,size,&ln,&pri);
         (void) SCFCRE("gdummy",D_I1_FORMAT,F_X_MODE,F_IMA_TYPE,size,&ing);
         (void) SCFMAP(ing,F_X_MODE,1,size,&ln,&pgi);

         for (n=0; n<nsy; n++)
            {
            err = SCFGET(idf,ioff,nsx,&iva,impntr);
            jpntr = (int *) impntr;
            tr = pri; tg = pgi; tb = pbi;

            for (i=0; i<nsx; i++)
               {
               rgbpix = *jpntr++;
               *tr++ = (unsigned char) (rgbpix & 0xff);
               *tg++ = (unsigned char) ((rgbpix >> 8) & 0xff);
               *tb++ = (unsigned char) ((rgbpix >> 16) & 0xff);
               }
            psline((unsigned char *)pbi, (unsigned char *)pgi, 
                   (unsigned char *)pri);
            ioff += nsx;
            }
         }
      }
   }

else
   {
   (void) SCFCRE("dummy",D_I1_FORMAT,F_X_MODE,F_IMA_TYPE,size,&ino);
   (void) SCFMAP(ino,F_X_MODE,1,size,&ln,&pbi);
   if (datforma == D_R4_FORMAT) 
      {
      (void) SCFCRE("dumreal",D_R4_FORMAT,F_X_MODE,F_IMA_TYPE,size,&rno);
      (void) SCFMAP(rno,F_X_MODE,1,size,&ln,&cptr);
      fpntr = (float *) cptr;
      }

   fac = (itfsize-1) / (ch - cl);

   for (n=0; n<nsy; n++) 
      {
      if (datforma == D_R4_FORMAT) 
         {
         err = SCFGET(idf,ioff,nsx,&iva,cptr);
         pf = fpntr; px = pbi;
         while (iva--) 
            {
            if (*pf<cl) 
               *px = 0;
            else if (ch<*pf)
               *px = itfsize-1;
            else
               *px = fac * (*pf-cl);
            px++; pf++;
            }
         }
      else
         {
         err = SCFGET(idf,ioff,nsx,&iva,pbi);
         if (i1s)
            {
            px = pbi;
            while (iva--) 
               {
               x = *px;
               if (x<cl) 
                  *px = 0;
               else if (ch<x)
                  *px = itfsize-1;
               else
                  *px = fac * (x-cl);
               px++;
               }
            }
         }
      psline((unsigned char *)pbi, (unsigned char *)pgi, (unsigned char *)pri);
      ioff += nsx;
      }
   }

pbi = 0;                        /* flush internal buffer  */
psline((unsigned char *)pbi, (unsigned char *)pbi, (unsigned char *)pbi);

psclose();
SCSEPI();
return 0;
}

/*

*/


int gettab(cp,name)
char  *cp, *name;

{
int  iva, myid;
int   err, n, ncols, nrows;

char  cbuf[24], text[64], dum[4];




if (*cp == 'I')                 /* handle ITT */
   {
   ln1 = 1;
   strcpy(cbuf,"descr. MIDAS_ITT");
   strcpy(text,".itt");
   err = SCKGETC("P3",1,1,&iva,dum);
   }
else                            /* handle LUT */
   {
   ln1 = 21;
   strcpy(cbuf,"descr. MIDAS_LUT");
   strcpy(text,".lut");
   dum[0] = 'L';                        /* so the check later on is o.k. */
   }

err = SCKGETC("HCITTLUT",ln1,20,&iva,name);

if (RGBmode != 0)
   {
   if (RGBmode == 1)			/* real RGB (3 planes) */
      {
      if (ln1 == 1)
         (void) strcpy(name,"none...");
      else
         (void) strcpy(name,"ramp - RGB color mode with 3 image planes");
      }
   else
      {
      (void) SCKGETC("HCITTLUT",41,2,&iva,dum);
      if (ln1 == 1)
         {
         if (dum[0] != 'Y') (void) strcpy(name,"none...");
         }
      else
         {
         if (dum[1] != 'Y') (void) strcpy(name,"ramp");
         }
      }
   return 0;
   }


if (strcmp(name,&cbuf[7]) == 0)
   {					/* MIDAS_ITT or MIDAS_LUT */
   strcpy(name,cbuf);
   return (-99);                        /* indicate descriptor input */
   }

n = CGN_INDEXS(name,text);
if (n <= 0) strcat(name,text);		/* append .lut or .itt */

if (*cp == 'I')                 /* handle ITT */
   {
   if ((dum[0] == 'N') || (dum[0] == 'n'))
      return (-1);                         /* indicate negative conversion */

   if ((dum[0] == 'P') || (dum[0] == 'p'))
      return (-2);                         /* indicate positive conversion */
   }



OPNTBL(name,&myid,&ncols,&nrows);	/* open table file */

return myid;
}
