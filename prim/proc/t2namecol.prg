! @(#)t2namecol.prg	19.1 (ES0-DMD) 02/25/03 14:09:06
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2namecol.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!  901108  KB  fix WRITE/KEYW HISTORY stuff...
!
! .PURPOSE
!
!    implements
!
!  NAME/COLUMN    table-name  column [column] [unit] [format] [type]
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE  "Enter table:"
DEFINE/PARAM P2 ? CHAR   "Enter column label:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:tdatatbl
