! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  extract.prg  for EXTRACT/ROTATE, /RTRACE, /CTRACE, /TRACE
! K. Banse	901108, 930324, 931124 (LaSilla), 950120, 110706
!
! use via EXTRACT/ROTATED_IMA stepx outframe
! or      EXTRACT/RTRACE stepx outframe [plot_opt] zw_option
! or      EXTRACT/CTRACE stepx outframe [plot_opt] zw_option
! or      EXTRACT/TRACE stepx outframe [plot_opt] [cut_opt] zw_option
! 	
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
write/keyw action EX{mid$cmnd(11:12)}		!init key ACTION
write/keyw history "EXTRACT/{mid$cmnd(11:14)} "
!
if dazhold(3) .ne. 0 then		!we cannot work in split mode...
   write/out "Command EXTRACT/{MID$CMND(11:14)}-
             "cannot be used in split mode...!"
   return
endif
! 
if action(3:4) .eq. "RO" then		!EXTRACT/ROTATED_IMAGE
   define/param p1 0.,0. N "Enter stepx,stepy for extracted image: "
   define/param p2 subframe IMA "Enter result frame: "
   define/maxpar 2
!
   action(1:2) = "2X"
   write/keyw inputd {p1}
!
else					!EXTRACT/TRACE, RTRACE, CTRACE
   define/param p1 0. N "Enter stepx for extracted 1-dim frame: "
   define/param p3 D  C "Enter plot option, D(raw), P(lot) or N(one): "
!
   if action(3:3) .eq. "T" then
      define/param p2 trace IMA "Enter name of result frame: "
      define/param p4 N  C "Enter cut option, C(ut) or N(ocut): "
      define/param p5 + C "Enter W,zoom: "
      define/maxpar 5
      action(1:2) = "1X"		!trace in any direction
      define/local zwindow/c/1/20 "{p5} "
   else
      if action(3:3) .eq. "R" then
         define/param p2 row IMA "Enter name of result frame: "
         action(1:2) = "3X"		!row trace 
      else
         define/param p2 column IMA "Enter name of result frame: "
         action(1:2) = "4X"		!column trace 
      endif
      define/param p4 + C "Enter W,zoom: "
      define/maxpar 4
      define/local zwindow/c/1/20 "{p4} "
   endif
! 
   write/keyw inputd {p1}
   write/keyw default {p3(1:1)}{p4(1:1)}
   if p3(1:1) .eq. "P" then	     !for PLOT option we need a graphics window
      compute/keyw dattim = m$time()
      if ididev(18) .eq. 11 .and. dazdevr(11) .lt. 0 create/graph
   endif
! 
   write/keyw dazin/i/1/5 -1,-1,-1,-1,-1
   if zwindow(1:1) .eq. "W" then
      if zwindow(2:2) .eq. "," then
         dazin(1) = {zwindow(3:)}
      else
         dazin(1) = 4                              !default to 4*zoom
      endif
   endif
endif
! 
write/keyw out_a {p2}
! 
if p8(1:1) .ne. "?" then	!test section for fixed extractions
   write/keyw inputr {p8}
else
   inputr(1) = -1.
endif
! 
run MID_EXE:TRACE
!
if p3(1:1) .eq. "P" then		!redraw final image
   overplot/row {out_a}
endif

return 

      write/keyw plcdata/c/1/60 {p2(1:60)}
      write/keyw plcdata/c/61/20 "FRAME    "

