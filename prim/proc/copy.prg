! @(#)copy.prg	19.1 (ESO-DMD) 02/25/03 14:08:45
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! MIDAS procedure copy.prg
! K. Banse	890628, 920116, 931027, 950921, 980922, 020215
!
! use via COPY/XY source_def destination_def opt_flags
!              with X,Y any of Key,Desc,Image 
!     or  COPY/LSXY listfile framea frameb         (frameb only for /LSDD)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local lindx/i/1/1 0,0 ? +lower
action(1:4) = mid$cmnd(11:14)		!save qualifier
! 
! special check for COPY/LSKD key_list image [new]
if action(1:4) .eq. "LSKD" then
   if p3(1:3) .eq. "NEW" then
      create/image {p2} 1,1		!create an image just for descriptors
   endif
endif
!  
! source is a descriptor
! 
if action(1:1) .eq. "D" then
   define/param p1 ? IMA "Enter source image:"
   define/param p2 ? ? "Enter source descriptor(s):"
! 
   if action(2:2) .eq. "D" then				!COPY/DD
      define/param p3 ? IMA "Enter destination image:"
      define/param p4 + ? "Enter destination descriptor(s):"
      define/param p5 0 N "Enter dest_descr_clean_flag (yes/no):"
      inputi(10) = {p5}
      define/maxpar 5
      if p4(1:1) .eq. "+" then		!no output descr. => use descr.exe
         action(1:2) = "CX"
         write/keyw in_a {p1}
         write/keyw out_a {p3}
         if p2(1:2) .ne. "*," .and. p2(1:2) .ne. "* " then
            write/keyw p2 ?{p2}			!so descr.exe knows
         endif
         run MID_EXE:DESCR
         return
      endif
! 
   elseif action(2:2) .eq. "K" then
      define/param p3 ? ? "Enter destination keyword:"
      define/maxpar 3
      lindx = m$index(p3,"/")-1
      if lindx .gt. 0 then
         write/keyw p8 {p3(1:{lindx})}
      else
         write/keyw p8 {p3}
      endif
      mid$mode(7) = mid$mode(7)-1	!decrease prog_level for m$existk ...
      mid$info(5) = m$existk(p8)	!we need global keyword for that
      mid$mode(7) = mid$mode(7)+1
      if mid$info(5) .le. 0 then	!destination key does not exist yet
         if lindx .gt. 0 then		!but is defined in command line
            lindx = lindx+2		!point to type of keyword
            if p3({lindx}:{lindx}) .eq. "C" then
               write/keyw {p3}
            else
               write/keyw {p3} 0 all
            endif
         else				!keyword not defined, use type of descr
            lindx = m$index(p2,"/")-1
            if lindx .gt. 0 then
               info/descr {p1} {p2(1:{lindx})}
            else
               info/descr {p1} {p2}
            endif
            branch mid$info(1) 1,2,3,4 IB,RB,CB,DB
            @ copy,badkey {p2} D
         endif
      endif
! 
   elseif action(2:2) .eq. "I" then
      define/param p3 ? IMA "Enter destination image:"
      define/maxpar 3
! 
   elseif action(2:2) .eq. "A" then		!output to ASCII file
      define/param p3 {p2} C "Enter name of result ASCII file:"
      define/maxpar 3
      if m$exist(p3) .eq. 1 -delete {p3}	!delete file if already there
   endif
! 
! source is a keyword
! 
elseif action(1:1) .eq. "K" then
   define/param p1 ? ? "Enter source keyword:"
! 
   if action(2:2) .eq. "D" then
      define/param p2 ? IMA "Enter destination image:"
      define/param p3 {p1} ? "Enter destination descriptor(s):"
      define/maxpar 3
! 
   elseif action(2:2) .eq. "K" then
      define/param p2 ? ? "Enter destination keyword:"
      define/maxpar 2
      lindx = m$index(p2,"/")-1
      if lindx .gt. 0 then
         write/keyw p8 {p2(1:{lindx})}
      else
         write/keyw p8 {p2}
      endif
      mid$mode(7) = mid$mode(7)-1	!decrease prog_level for m$existk ...
      mid$info(5) = m$existk(p8)	!we need global keyword for that
      mid$mode(7) = mid$mode(7)+1
      if mid$info(5) .le. 0 then	!destination key does not exist yet
         if lindx .gt. 0 then           !but is defined in command line
            lindx = lindx+2             !point to type of keyword
            if p2({lindx}:{lindx}) .eq. "C" then
               write/keyw {p2}
            else
               write/keyw {p2} 0 all
            endif
         else                           !keyword not defined, that's bad...
            lindx = m$index(p1,"/")
            if lindx .gt. 0 then
               write/keyw {p2}{p1({lindx}:)}
            else
               @ copy,badkey {p2} K	
            endif
         endif
      endif
! 
   elseif action(2:2) .eq. "I" then
      define/maxpar 2
      define/param p2 ? IMA "Enter destination image:"
! 
   elseif action(2:2) .eq. "A" then		!output to ASCII file
      define/param p2 {p1} C "Enter name of result ASCII file:"
      define/maxpar 2
      if m$exist(p2) .eq. 1 -delete {p2}	!delete file if already there
   endif
! 
! source is an image
! 
elseif action(1:1) .eq. "I" then
   define/param p1 ? IMA "Enter source image:"
! 
   if action(2:2) .eq. "D" then
      define/param p2 ? IMA "Enter destination image:"
      define/param p3 ? ? "Enter destination descriptor(s):"
      define/maxpar 3
! 
   elseif action(2:2) .eq. "K" then
      define/param p2 ? ? "Enter destination keyword:"
      define/maxpar 2
      lindx = m$index(p2,"/")-1
      if lindx .gt. 0 then
         write/keyw p8 {p2(1:{lindx})}
      else
         write/keyw p8 {p2}
      endif
      mid$mode(7) = mid$mode(7)-1	!decrease prog_level for m$existk ...
      mid$info(5) = m$existk(p8)	!we need global keyword for that
      mid$mode(7) = mid$mode(7)+1
      if mid$info(5) .le. 0 @ copy,badkey {p2} K	!key does not exist yet
! 
   elseif action(2:2) .eq. "A" then		!output to ASCII file
      define/param p2 ? C "Enter name of result ASCII file:"
      define/maxpar 2
      if m$exist(p2) .eq. 1 -delete {p2}	!delete file if already there
   endif
! 
! source is an ASCII file
! 
elseif action(1:1) .eq. "A" then
   define/param p1 ? ? "Enter name of input ASCII file:"
   define/param p2 ? IMA "Enter destination image:"
   define/param p3 {p1} ? "Enter name of destination descriptor:"
   define/param p4 72 n "Enter max_size of line:"
   define/maxpar 4
   write/keyw inputi {p4}
endif
! 
! here all list options fall in as well
!
run MID_EXE:COPIER
return
!
ib:
write/keyw {p3}/i/1/{mid$info(2)} 0 all
goto do_it
rb:
write/keyw {p3}/r/1/{mid$info(2)} 0.0 all
goto do_it
cb:
write/keyw {p3}/c*{mid$info(3)}/1/{mid$info(2)} " " all
goto do_it
db:
write/keyw {p3}/d/1/{mid$info(2)} 0.0 all
do_it:
run MID_EXE:COPIER
! 
entry badkey
! 
define/param p1 + C
define/param p2 + C
! 
if p2 .eq. "K" then
   if lindx .le. 0 then
      write/out Keyword `{p1}' has to be created first.
   else
      write/out Keyword `{p1(1:{lindx}))}' has to be created first.
   endif
else
   if lindx .le. 0 then
      write/out Descriptor `{p1}' has to be created first.
   else
      write/out Descriptor `{p1(1:{lindx}))}' has to be created first.
   endif
endif
write/error 30
return/exit
