! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure vericopy.prg  to copy the general "verify" procedures
!        into the current directory
!  K. Banse     921202
! 
!.VERSION
! 110427	last modif
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 + C 
! 
! general case handled in VMS or UNIX
!
write/out Copying all the necessary procedures/files to current directory 
! 
if aux_mode(1) .le. 1 then		! VMS
   $DELETE/NOLOG/NOCONF *.bdf.*,*.tbl.*,*.cat.*,*.mt.*,*.fits.*,*.tfits.*
   $DELETE/NOLOG/NOCONF *.asc.*,screen*.*.*,graph*.*.*
   $DELETE/NOLOG/NOCONF verit*.*.*,midd* bl*.dat.*,ccdintab.*.*
   $DELETE/NOLOG/NOCONF all_*.prg.*,creamask.prg.*,works.prg.*
   $DELETE/NOLOG/NOCONF verif*.prg.*,kcompare.prg.*,imcompare.prg.*,glue*.prg.*
   $DELETE/NOLOG/NOCONF empty.bdf*.*
   ! 
   define/local testdir/c/1/60 "MID_DISK:[&MIDASHOME.&MIDVERS.TEST.PRIM] "
   define/local defdir/c/1/2 "[]"
   $ COPY {testdir}*.dat {defdir}
   $ COPY {testdir}*.fmt {defdir}
   $ COPY {testdir}*.mt {defdir}
   $ COPY {testdir}*.fit {defdir}
   $ COPY {testdir}*.fits {defdir}
   $ COPY {testdir}*.tfits {defdir}
   $ COPY {testdir}*.ffits {defdir}
   $ COPY {testdir}*.asc {defdir}
   $ COPY {testdir}all_*.prg {defdir}
   $ COPY {testdir}veri*.prg {defdir}
   $ COPY {testdir}*compare.prg {defdir}
   $ COPY {testdir}works.prg {defdir}
   $ COPY {testdir}creamask.prg {defdir}
   !
   define/local fitsdir/c/1/60 "MID_DISK:[&MIDASHOME.&MIDVERS.TEST.FITS] "
   $ COPY {fitsdir}tst*.mt {defdir}
   $ COPY {fitsdir}*.prg {defdir}

else					! UNIX
   $ \rm -f *.asc verit* screen* graph*  midd* bl*.dat
   $ \rm -f *.bdf *.tbl *.cat *.mt *.fit *.fits *.tfits
   $ \rm -f veritb.dat veritx.dat ccdintab.*
   $ \rm -f all_*.prg creamask.prg works.prg
   $ \rm -f verif*.prg 				!leave veriall.prg ...
   $ \rm -f kcompare.prg imcompare.prg glue*.prg
   $ \rm -f empty.bdf*
   ! 
   define/local testdir/c/1/60 "$MID_HOME/test/prim/ "
   define/local defdir/c/1/2 "./"
   $ cp {testdir}*.mt {defdir}
   $ cp {testdir}*.fit {defdir}
   $ cp {testdir}*.fits {defdir}
   $ cp {testdir}*.tfits {defdir}
   $ cp {testdir}*.ffits {defdir}
   $ cp {testdir}*.dat {defdir}
   $ cp {testdir}*.fmt {defdir}
   $ cp {testdir}*.asc {defdir}
   $ cp {testdir}all_*.prg {defdir}
   $ cp {testdir}verif*.prg {defdir}
   $ cp {testdir}*compare.prg {defdir}
   $ cp {testdir}works.prg {defdir}
   $ cp {testdir}creamask.prg {defdir}
   $ cp {testdir}veriall.prg {defdir}
   $ chmod +w {defdir}veriall.prg 
   !
   define/local fitsdir/c/1/60 "$MID_HOME/test/fits/ "
   $ cp {fitsdir}tst*.mt {defdir}
   $ cp {fitsdir}*.prg {defdir}
   !
   if p1(1:3) .eq. "XXX" then
      $ \rm -f Xveri*.prg
      $ cp {testdir}Xveri*.prg {defdir}
   endif
endif
! 
write/out  
write/out "To execute the verification procedures, enter the Midas command:"
write/out "@@ veriall parm1 parm2 parm3 "
write/out "parm1 = display (default): use display/graphics windows"
write/out "        nodisplay: do not use display/graphics windows"
write/out "parm2 = 3-digit flag abc for choice of verifications "
write/out "        a=1/0 for basic verifications or not,"
write/out "        b=1/0 for interactive verification or not, "
write/out "        c=1/0 for QC plot tests or not"
write/out "        - defaulted to 100"
write/out "parm3 = 1/2/3/4 (default=1) for executing the verifications with"
write/out "        data files in Midas(1), FITS(2), M+F(3), or F+M(4) format"
write/out
write/out "If you have an RGB display (24 bit per pixel), execute also" -
 verify55.prg
write/out 
write/out "If you have the /midas/demo/data directory containing -
the Midas demo files,"
write/out "you can also execute `@ superverify' to run the verifications"
write/out followed by automatic (non-interactive) tutorials.
 
