! @(#)t2copykt.prg	19.1 (ES0-DMD) 02/25/03 14:09:04
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2copykt.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 jan 1990
!  901108  KB  fix WRITE/KEYW HISTORY stuff!
! .PURPOSE
!
!    implements
!
!  COPY/KT  keyword  table-name [column ...] row 
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? CHAR  "Enter keyword name:"
DEFINE/PARAM P2 ? TABLE "Enter table:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
WRITE/KEYW MID$CMND/C/1/4 "KTCP"
!
RUN MID_EXE:tdatatbl
