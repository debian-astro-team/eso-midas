! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure findpos.prg to implement FIND/PXPOS
! K. Banse      110502, 110517
!
! use as FIND/PXPOS inframe table option dsp_flag
! where  inframe = input image (also ima[xa,ya:xb,yb])
!        table = table to store the resulting positions
!        option = the different possibilities:
!                 MIN+npos      MIN,val
!                 MAX+npos      MAX,val
!                 lval,hival
!                 median+npos,mpos      median+vala,valb
!                 center+npos,mpos      center,vala,valb
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter name of input image:"
define/param p2 + tab "Enter name of result table:"
define/param p3 + char "Enter option:"
define/param p4 no char "Enter output_spec:"
define/maxpar  4
! 
write/keyw in_a {p1}
write/keyw out_a {p2}
if p3(1:1) .eq. "+" then
   write/out "missing interval specification ..."
   return/error 5
endif
! 
write/keyw outputc {p3}
write/keyw inputc/c/1/20 " " all
inputi(1) = 1
define/local lwork/i/1/3 0,0,0
! 
! first, check for interval with center value
if outputc(1:1) .eq. "(" then
   inputc(1:20) = outputc
   goto execute
endif
! 
! next, check for "plain" interval
if outputc(1:1) .eq. "[" .or. outputc(1:1) .eq. "]" then
   inputc(1:20) = outputc
   goto execute
endif
! 
lwork = m$index(outputc,"+")
if lwork .gt. 0 then		!e.g. min+20
   lwork = lwork - 1
   inputc(1:) = outputc(1:{lwork})//"      "	!drop the + 
   lwork(2) = lwork + 1				!move back to +
   lwork = m$index(outputc({lwork(2)}:),",")
   if lwork .le. 0 then
      inputr(1) = {outputc({lwork(2)}:)}
   else
      lwork(3) = lwork(2) + lwork - 2
      inputr(1) = {outputc({lwork(2)}:{lwork(3)})}
      lwork(2) = lwork(3) + 2			!move past comma 
      inputr(2) = {outputc({lwork(2)}:)}
      inputi(1) = 2
   endif
! 
else				!look for e.g. max,20 
   lwork = m$index(outputc,",")
   if lwork .le. 0 then		!wrong input
      write/out "invalid option..."
      return/error 5
   endif
   ! 
   inputc(1:) = outputc(1:{lwork})//"      "	!keep the comma in the end
   lwork(2) = lwork + 1				!move past 1st comma
   lwork = m$index(outputc({lwork(2)}:),",")
   if lwork .le. 0 then
      inputr(1) = {outputc({lwork(2)}:)}
   else
      lwork(3) = lwork(2) + lwork - 2
      inputr(1) = {outputc({lwork(2)}:{lwork(3)})}
      lwork(2) = lwork(3) + 2			!move past 2nd comma 
      inputr(2) = {outputc({lwork(2)}:)}
      inputi(1) = 2
   endif
endif
!   		
execute:
write/keyw history "FIND/PXPOS {p1} {p2} {p3} {p4}"
run MID_EXE:POSITIONS
 
