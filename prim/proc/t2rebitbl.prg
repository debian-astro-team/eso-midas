! @(#)t2rebitbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:07
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!       Midas procedure  t2rebitbl.prg
!
!.PURPOSE
!       Nonlinear rebinning in one dimension
!
!.KEYWORDS
!       IMAGES, TABLES, DATA CONVERSION, NONLINEAR REBIN
!
!.RESTRICTIONS
!       (See program header blocks.)
!
!.LANGUAGE
!       MIDAS command language
!
!.CALLING SEQUENCE
!       REBIN/II outima inima refima func parm intop
!       REBIN/TI outima intab :i,:d[,:b] refima func parm intop
!       REBIN/IT outtab :i,:d[,:b] inima func parm intop
!       REBIN/TT outtab :i,:d[,:b] intab :i,:d[,:b] func parm intop
!
!             outima, outtab, inima, intab  =  name of output/input files
!             refima = (for image output only) the reference image
!             :i      column name for independent variable
!             :d      column name for dependent variable
!             :b      [optional] column name for binwidth
!             funct   function code (see below) for the mapping of
!                     OUTPUT indep to INPUT indep
!
!             parm    parameters for the function used
!                  1) from command line: a,b,c,d,....(max.12)
!                  2) from keyword: "name,KEY", e.g. INPUTR,K
!
!             intop   interpolation option (see below)
!
!             FUNCTIONS    (parameters A, B, C, ..... )
!
!             LIN     x(old) = A + Bx
!             POL     x      = A + Bx +Cxx +  .... (up to 16 coeff)
!             INV     x      = A + B/x
!	      EXP     x      = A + B*exp(Cx)
!             DEX     x      = A + B*10.**(Cx)
!	      LOG     x      = A + B*LOG(Cx)
!             DLG     x      = A + B*LOG10(x)
!             IPO     x      = A + B/(x-x0) + C/(x-x0)**2 + ...
!	      U01     x      = user defined function.
!
!	      INTERPOLATION/INTEGRATION OPTIONS
!
!	      PIX     Take fraction of pixel
!	      LIN     Use linear interpolation, followed by
!		      straightforward integration
!	      SPG     Spline interpolation, and gaussian integration
!
!	      (For heavy oversampling - 5 or more input pixels mapped
!	      onto one output pixel - the first option above should
!	      be used; for similar pixel (bin) widths in input and
!	      output, the second should be adequate; the third option
!	      is the most accurate and also the most time-consuming.)
!
!.AUTHOR
!       M. Rosa (STECF),	25-Aug-1985
!
!.MODIFICATIONS
!       M. Rosa (STECF),	25-Aug-1985         Version 2.00
!	F. Murtagh (STECF) 	05-Nov-1985         Version 2.01
!	K. Banse		901108, 930423	syntax change for key option !
!       M. Peron                920203        clean up ....
!------------------------------------------------------------------------------
!
write/keyw rebin/c/1/12 "{mid$cmnd(1:12)}"
write/keyw history "{mid$cmnd(1:6)}/{mid$cmnd(11:14)} "
! 
define/local kin/i/1/1 0 ? +lower
!
! option II
!
if mid$cmnd(11:11) .eq. "I" then
   if mid$cmnd(12:12) .eq. "I" then			! option II
      define/param p1 ? ima "Enter output frame:"
      define/param p2 ? ima "Enter input frame:"
      define/param p3 ? ima "Enter reference frame:"
      write/keyw in_a {p2}
      write/keyw in_b {p3}
   else							! option IT
      define/param p1 ? table "Enter output table:"
      define/param p2 ? char "Enter column references:" 
      define/param p3 ? ima "Enter input image:"
      write/keyw colo/c/1/53 {p2}            ! :i,:d[,:b]
      write/keyw in_a {p3}            	! inima
   endif
! 
   define/param p4 LIN
   define/param p5 0.,1.
   define/param p6 PIX
   define/maxpar 6
!
   write/keyw cfunc/c/1/8 {p4}
   @ t2rebitbl,keycheck {p5}
   write/keyw copt/c/1/3 {p6}
!
else
   if mid$cmnd(12:12) .eq. "I" then                     ! option TI
      define/param p1 ? ima "Enter output image:"
      define/param p2 ? table "Enter input table:"
      define/param p3 ? char "Enter reference columns:"
      define/param p4 ? ima "Enter reference image:"
      write/keyw in_a {p2}            	! intab
      write/keyw coli/c/1/53 {p3}            ! :i,:d[,:b]
      write/keyw in_b {p4}            	! refima
   else							! option TT
      define/param p1 ? table "Enter output table:"
      define/param p2 ? char "Enter reference columns:"
      define/param p3 ? table "Enter input table:"
      define/param p4 ? char "Enter reference columns:"
      write/keyw colo/c/1/53 {p2}            ! :i,:d[,:b]
      write/keyw in_a {p3}            	! intab
      write/keyw coli/c/1/53 {p4}            ! :i,:d[,:b]
   endif
! 
   define/param p5 LIN
   define/param p6 0.,1.
   define/param p7 PIX
   define/maxpar 7
!
   write/keyw cfunc/c/1/8 {p5}
   @ t2rebitbl,keycheck {p6}
   write/keyw copt/c/1/3 {p7}
endif
! 
write/keyw out_a {p1}
write/keyw mid$cmnd/c/1/12 "{rebin(1:12)}" 
if cfunc(1:3) .eq. "U01" then
   if aux_mode(1) .le. 1 then
       $ @ MID_PROC:rebcref.com
   else
       $ sh $MID_PROC/rebcref.sh
   endif
   run tintertbl 
else
   run MID_EXE:tintertbl
endif
! 
entry keycheck
write/keyw inputd/d/1/12 0. all
kin = m$index(p1,",")
if kin .le. 1 then
   write/error 5			!invalid syntax
   return/exit
endif
!
kin = kin+1
if p1({kin}:{kin}) .ne. "K" then
   write/keyw inputd {p1}
else
   kin = kin-2
   if p1(1:{kin}) .eq. "INPUTD" then
      write/out "INPUTD is not permitted as keyword name ..."
      return/exit
   endif
   copy/kk {p1(1:{kin})} inputd/d/1/12
endif

