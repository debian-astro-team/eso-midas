! ++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure lutexc.prg
! K. Banse	900703, 010618, 030704
!
! demonstrate different colour lookup tables
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++
write/keyw out_a {p1}		!save plotflag (Plot or NoPlot)
!
write/out "Some of the existing colour lookup tables are:"
write/out
write/out idl2, idl4, idl5, idl6, idl11, idl12, idl14, idl15
write/ouT
!
if ididev(7) .ge. 2 then
   define/local reload/i/1/1 1 ? +low   !for Pseudo on RGB we must reload
else
   define/local reload/i/1/1 0 ? +low
endif
!
set/lut					!enable lookup tables
display/lut				!display colour bar
!
@ lutexc,sublut idl2
@ lutexc,sublut idl4
@ lutexc,sublut idl5
@ lutexc,sublut idl6
@ lutexc,sublut idl11
@ lutexc,sublut idl12
@ lutexc,sublut idl14
@ lutexc,sublut idl15
! 
write/out
write/out "To see all existing LUTs, use the host command"
write/out  -dir MID_SYSTAB:*.lut
!
! here entry SUBLUT
!
entry sublut
! 
write/out LOAD/LUT {p1} yields:
load/lut {p1} 
if reload .eq. 1 load/image {idimemc}   !we know that there is an image...
!
if out_a(1:1) .eq. "P" @a plottab,lut {p1}
