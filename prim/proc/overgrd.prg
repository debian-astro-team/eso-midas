! @(#)overgrd.prg	19.1 (ESO-DMD) 02/25/03 14:08:56
! ++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: OVERGRD.PRG
!.PURPOSE:        Overplot plot a grid onto an existing plot 
!.USE             execute as OVER/GRID par1
!                 par1 = LARGE OR SMALL
!.AUTHOR:         Rein H. Warmels
!                 RHW 880411 creation
!------------------------------------------------------------------
! 
define/param p1 LARGE c  "Large (L) or small (S) tickmark grid:"
define/param p2 XY    c  "Enter X  and/or Y for direction of grid:"
!
run MID_EXE:overgrd
copy/graph
