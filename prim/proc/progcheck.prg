! @(#)progcheck.prg	19.1 (ES0-DMD) 02/25/03 14:08:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992 European Southern Observatory
!.IDENT       progcheck.prg
!.AUTHOR      Klaus Banse,  ESO - Garching
!.KEYWORDS    Midas commands 
!.PURPOSE     Check all procedures in a directory for complete MIdas
!	      command/qualifiers
!.VERSION     1.0    Creation    940411
! 
!  use as  @ progcheck dir option
!  where
!          dir = directory name, if omitted current directory is used
!          option = 'silent' if you only want the name of the procedure 
!		    displayed which contain incomplete Midas commands
!		    else all offending procedure lines are displayed as well
!-------------------------------------------------------
! 
define/para p1 + ? "Enter directory name: "
define/para p2 n ? "Enter option: "
! 
if p1(1:1) .ne. "+" then
   inputc = m$symbol(p1)
   write/out directory: {inputc}
   if aux_mode .lt. 2 then		!VMS
      $delete/noconf/nolog list.dat.*
      $dir/out=list.dat {inputc}*.prg 
   else
      $rm -f list.dat
      $ls {inputc}/*.prg >list.dat
   endif
else
   if aux_mode .lt. 2 then              !VMS
      $delete/noconf/nolog list.dat.*
      $dir/out=list.dat *.prg 
   else
      $rm -f list.dat
      $ls *.prg >list.dat
   endif
endif
! 
write/keyw inputc/c/1/1 {p2(1:1)}
@ progcheck,doit <list.dat
! 
entry doit
translate/show {p1} x,{inputc(1:1)}
