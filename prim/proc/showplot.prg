! @(#)showplot.prg	19.1 (ES0-DMD) 02/25/03 14:09:01
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: SHOWPLOT.PRG
!.PURPOSE: MIDAS procedure SHOWPLOT.PRG for SHOW/PLOT
!.USE:     executed via SHOW/GRAPHICS (it is as simple a one can think of)
!.AUTHOR:  R.H. Warmels  ESO - Garching 
!.VERSION: 870211 RHW  data of creation
!-------------------------------------------------------------------------
RUN MID_EXE:SHOWGRAP
