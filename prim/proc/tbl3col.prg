! ++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  tbl3col.prg  to implement COPY/T3TAB
! K. Banse	
! 111130	creation
! 111130	last modif
!
! use via COPY/T3TAB intab incol inrow outtab outcol new_flag
! to copy all elements of an array column
! 
! with  intab = input 3d table
!       incol = input column
!       inrow = row of incol, defaulted to 1st row
!       outtab = output table
!	outcol = result column (will be created)
!	new_flag = (Yes/No) create new result table, else overwrite
!
! +++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? T "Enter input 3dim table: "
define/param p2 ? C "Enter input column:"
define/param p3 @1 C "Enter input row:"
define/param p4 ? T "Enter result table:"
define/param p5 ? C "Enter result column name:"
define/param p6 YES C "Enter YES or NO to force new result table:"
! 
define/local pos/i/1/1       0
define/local source/c/1/60   {p1}
define/local scolumn/c/1/16  {p2}
if p3(1:1) .eq. "@" then
   define/local srow/i/1/1      {p3(2:)}
else
   define/local srow/i/1/1      {p3}
endif
define/local target/c/1/60   {p4}
define/local tcolumn/c/1/16  {p5}
if p6(1:1) .eq. "N" then
   define/local create/i/1/1    0
else
   define/local create/i/1/1    1
endif
!
run MID_EXE:tb3dtest.exe
 
