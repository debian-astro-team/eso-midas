! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  creadisp.prg 
! 
! use as 
! @ creadisp  display_id  xdim,ydim,xoff,yoff  nom,xm,ym,mdep  alpha_flag
!             graph_size  Xstation_name
! or
! @ creadisp display_id -999           for switching displays
! 
!  K. Banse   ESO/IPG - Garching
!  910131, 961122, 990727, 001123, 030731
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! 
if p2 .eq. "-999" then			 !here for switching displays
   define/param p1 0 N "Enter display id: " 0,9
   action(6:7) = "IS"
   run MID_EXE:wndcrea.exe
   dazdevr(10) = {p1}
   return
else
   define/param p6 default ? "Enter name of XWindowScreen: "
   write/keyw dazdevc {p6}
endif
!
!  test, if it will be a shadow display window
if p2(1:1) .eq. "S" then		!shadow,disp_id
   define/local kk/i/1/1 0
   if p1(1:1) .lt. "A" .or. p1(1:1) .gt. "Z" then
      write/error 100 "Par. 1 bad (is not in [a,z])"
      return/exit
   else
      kk = m$index(p2,",")+1
      if kk .le. 1 then
         write/error 5 "Par. 2 bad (= {p2})" 		!invalid syntax
         return/exit
      endif
   endif
! 
   ACTION(6:8) = "IP{p2({kk}:{kk})}"
   run MID_EXE:wndcrea.exe
   return
endif
! 
! now for create/display	(use INPUTI(1 - 11) except INPUTI(9)
! 
define/param p1 0 N "Enter display id: " 0,9
define/param p2 512,512,630,330 N "Enter xdim,ydim,xoff,yoff of display: "
! 
write/keyw inputi 512,512,0,0,1,512,512,-1,0,99,10000
write/keyw inputi {p2}
if inputi(1) .le. 0 .or. inputi(2) .le. 0 then
   write/out Bad dimensions for display window...
   return
endif
! 
!initialize to 2 or 1 image channels (overlay channel added by creadsp.c)
if {p1} .eq. 0 then
   define/local noch/i/1/2 2,30000
else
   define/local noch/i/1/2 1,10000
endif
define/param p3 -
    {noch(1)},{INPUTI(1)},{INPUTI(2)},-1 N "Enter nomem,xm,ym,depth:"
define/param p5 {noch(2)} N "Enter no. of graphics segments: "  0,900000
write/keyw inputi/i/5/4 {noch(1)},{INPUTI(1)},{INPUTI(2)},-1 
write/keyw inputi/i/5/4 {p3}
! 
if inputi(5) .gt. dazdevr(20) then		!max. dazdevr(20) channels
   set/format i1
   write/out "Max. {dazdevr(20)} channels possible for display ..."
   inputi(5) = dazdevr(20)
   write/out "No. of channels set to {dazdevr(20)}."
endif
inputi(11) = {p5}
! 
define/param p4 Y ? "Enter alpha_flag,backgr_color Y(es) or N(o),colno. : "
if p4(1:1) .eq. "N" inputi(10) = 0
outputi =  m$index(p4,",") 
inputi(12) = 1				!default color = 1 (black)
if outputi .gt. 0 then
   outputi = outputi + 1
   inputi(12) = {p4({outputi}:)}
   if inputi(12) .lt. 1 .or. inputi(12) .gt. 8 then
      inputi(12) = 1
   endif
endif
!
if ididev(1) .lt. 0 @ idistart.prg_o		!start up IDI server
!
action(6:7) = "IC"
run MID_EXE:wndcrea.exe		!the server is supposed to be up already !!
dazdevr(10) = {p1}
inputi(7) = dazdevr(10) + 1
winopen({inputi(7)}) = 1		!set WINOPEN
 
