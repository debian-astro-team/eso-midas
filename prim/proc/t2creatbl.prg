! @(#)t2creatbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:05
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS procedure  t2creatbl.prg to implement CREATE/TABLE
! 
!  J.D.Ponz                ESO - Garching    5 APR 89
!  901107  KB :  redesign HISTORY stuff
!
!  use via CREATE/TABLE  table ncol nrow [datafile] [formatfile] [phformat]
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE  "Enter table:"
DEFINE/PARAM P2 0 ? "Enter no. of columns:"
DEFINE/PARAM P3 0 ? "Enter no. of rows:"
DEFINE/PARAM P4 NULL CHAR   "Data file:"
DEFINE/PARAM P5 NULL CHAR   "Format file:"
DEFINE/PARAM P6 TRAN CHAR   "Physical format (TRANS/RECORD):"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
!
RUN MID_EXE:tdatatbl
