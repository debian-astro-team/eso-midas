! ++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  radec.prg  to implement convert/radec
! K. Banse      020702, 040308
!
! use via convert/radec ref_frame direction in_opt input [output]
! with  ref_frame = the correcponding image
!	direction = TO or FROM
!	in_opt = Table (a), File (ASCII) (b) or String (command line) (c)
!	input = for a) table (with type!),in_columns (with :)
!	      = for b) name of ASCII file
!	      = for c) coord string
!	output = for a) result columns (with :) - must exist already
!	       = for b) name of result file,[option]
!		        option = FULL to include input coord string
!	       = not used for c) results are in keyword OUTPUTD
!
! +++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter frame:"
define/param p2 to c "Enter direction (TO/FROM) for RA,DEC to/from dec. coords:"
define/param p3 str c "Enter source option (table/file/string):"
define/local flag/i/1/2 0,0
set/format i1 f12.7
!
! table input 
if p3(1:1) .eq. "T" then		
   define/param p4 ? c "Enter table name,input columns:"
   define/param p5 + c "Enter result columns:"
   define/local nocols/i/1/1 0
   nocols = m$parse(p4,"incol") - 1		!count of columns
   in_a = incol01				!isolated table name

   define/local loop/i/1/2 1,0
   loop(2) = m$value({in_a},tblcontr(4))
   if p5(1:1) .ne. "+" then
      inputi = m$parse(p5,"outcol")
   endif
   flag(1) = 1
   goto table_read
! 
! command line input
else if  p3(1:1) .ne. "F" then
   define/param p4 ? c "Enter coord string:"
   inputc = p4
   goto convert
endif
! 
! ASCII file input
define/param p4 ? c "Enter input file name:"
define/param p5 + c "Enter output file name:"
flag(1) = 2
! 
define/local infi/i/1/2 -1,0
define/local outfi/i/1/2 -1,0
open/file {p4} read infi
if p5(1:1) .ne. "+" then
   inputi = m$index(p5,",") - 1
   if inputi .gt. 1 then
      in_a = p5(1:{inputi})
      flag(2) = 1
   else
      in_a = p5
   endif
   open/file {in_a} write outfi
endif
! 
! section for file handling...
! 
file_read:
write/keyw inputc/c/1/80 " " all
read/file {infi(1)} inputc 80
if infi(2) .lt. 0 then				!EOF reached
   close/file {infi(1)}
   if outfi(1) .gt. 0 close/file {outfi(1)}
   return
endif
! 
! here the main engine used by all options
! input  for CONVERT/COORDS is expected to be stored in keyword INPUTC
! 
convert:
convert/coord {p1} {inputc} >Null
! 
if p2(1:1) .eq. "F" then			!RA,DEC -> decimal coords
   outputc = "{outputd(1)},{outputd(2)}"
   write/out dec. coords: {outputc}
else						!dec. coords -> RA,DEC 
   outputi(1) = outputd(4)
   outputi(2) = outputd(5)
   outputi(3) = outputd(7)
   outputi(4) = outputd(8)
   outputc = -
 "{outputi(1)}:{outputi(2)}:{outputd(6)},{outputi(3)}:{outputi(4)}:{outputd(9)}"
   write/out RA, DEC: {outputc}
endif
! 
if flag .eq. 0 then			!single coord string
   return
else if flag .eq. 2 then		!get next line of ASCII file
   if flag(2) .eq. 1 then
      p8 = inputc // " =>Z" // outputc
      outputc = m$repla(p8,"Z"," ")	!to get the bloody space in there...
   endif
   if outfi(1) .gt. 0 write/file {outfi(1)},key outputc
   goto file_read			!read next record
else
   goto table_next			!get next table row
endif
! 
! section for table handling...
! 
table_read:
if p2(1:1) .eq. "T" then			!decimal coords -> RA,DEC
   inputc = "{{in_a},{incol02},@{loop}},{{in_a},{incol03},@{loop}}"
else
   if nocols .gt. 2 then
      inputi(1) = {{in_a},{incol02},@{loop}}
      inputi(2) = {{in_a},{incol03},@{loop}}
      inputi(3) = {{in_a},{incol05},@{loop}}
      inputi(4) = {{in_a},{incol06},@{loop}}
      inputc = "{inputi(1)}:{inputi(2)}:{{in_a},{incol04},@{loop}}{inputi(3)}:{inputi(4)}:{{in_a},{incol07},@{loop}}"
   else
      inputd(1) = {{in_a},{incol02},@{loop}} 	!do conversion here...
      inputd(2) = {{in_a},{incol03},@{loop}}
      outputd(1) = inputd(1)*15.0		!RA
      outputd(2) = inputd(2)			!DEC
      write/out "dec. coords: {outputd(1)},{outputd(2)}"
      goto table_next
   endif
endif
goto convert
! 
table_next:
if p5(1:1) .ne. "+" then
   if p2(1:1) .eq. "T" then			!decimal coords -> RA,DEC
      {in_a},{outcol01},@{loop} = outputd(4)
      {in_a},{outcol02},@{loop} = outputd(5)
      {in_a},{outcol03},@{loop} = outputd(6)
      {in_a},{outcol04},@{loop} = outputd(7)
      {in_a},{outcol05},@{loop} = outputd(8)
      {in_a},{outcol06},@{loop} = outputd(9)
   else
      {in_a},{outcol01},@{loop} = outputd(1)
      {in_a},{outcol02},@{loop} = outputd(2)
   endif
endif
! 
loop(1) = loop(1) + 1			!move on to next row
if loop(1) .le. loop(2) goto table_read


