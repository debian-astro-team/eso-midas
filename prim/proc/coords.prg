! @(#)coords.prg	19.1 (ES0-DMD) 02/25/03 14:08:47
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure coords.prg, to pick up coordinates from the image display
! via the cursor
!  K. Banse	900712, 920117, 940301
!  ESO/IPG - Garching
!
!    GET/CURS descr,DESCR append [mark] [max_read,no_curs] [w,zoom] 
! or GET/CURS table         [ID] [mark] [max_read,no_curs] [w,zoom]
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p3 YN ? "Enter mark_flag: "
define/param p4 9999,1 N "Enter max_read,no._of_cur: "
define/param p5 + C "Enter W,zoom: "
define/maxpar 5
!
write/keyw cursor {p4},0
write/keyw dazin/i/1/5 -1,-1,-1,-1,-1
if p5(1:1) .eq. "W" then
   if p5(2:2) .eq. "," then
      dazin(1) = {p5(3:)}
   else
      dazin(1) = 4				!default to 4*zoom
   endif
endif
! 
if cursor(1) .eq. 0 then                        !continuous mode
   p3(3:3) = "Z"
else
   p3(3:3) = "Y"
endif
! 
run MID_EXE:COORD
