! @(#)tloadtbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:09
 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : TLOADTBL.PRG
!  TABLE Subsystem
!
!  J.D.Ponz                ESO - Garching    5 APR 83
!  KB					     860528   (take out Ramtek stuff)
!  MP                                        930510
! .PURPOSE
!
!    implements
!
!    LOAD/TABLE table x-axis y-axis [ident] [symbol] [size] [color] [conn_flag]
!
!
! .MODULE
!
!  TLOADTBL
! ----------------------------------------------------------------------
!
define/param p1 ? TABLE "Enter table:"
define/param p2 ? CHAR "Enter Input Column for abcissa"
define/param p3 ? CHAR "Enter Input Column for ordinate"
define/param p4 + CHAR 
define/param p5 0 ? "Enter symbol flag or column for it:"
define/param p6 3 ? "Enter symbol size or column for it:"
define/param p7 255 ? "Enter symbol color or column for it:"
define/param p8 0 NUM "Enter connection flag:"
! 
write/keyw inputi/i/1/1 {p8}
write/keyw in_a {p1}
run MID_EXE:TLOADTBL
