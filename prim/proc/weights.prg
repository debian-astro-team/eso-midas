! @(#)weights.prg	19.1 (ES0-DMD) 02/25/03 14:09:14
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure weights.prg for COMPUTE/WEIGHTS
! K. Banse	910205
!
! use as COMPUTE/WEIGHTS in1,in2,,,inj xsta,ysta,xend,yend 
!     or COMPUTE/WEIGHTS in1,in2,,,inj CURSOR
!     or COMPUTE/WEIGHTS catalog.cat ...
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? C "Enter input specs: "
define/param p2 CURSOR C -
   "Enter window coords - CURSOR or xsta,ysta,xend,yend: "
define/maxpar 2
!
! display image
if p2(1:1) .eq. "C" then
   get/cursor ? ? nn 1,2		!show rectangle + get single input
   inputc(1:1) = "C" 
else
   write/keyw inputc [{p2}]
endif
! 
action(1:2) = "WE"
run MID_EXE:GENYY1			!finally do it...
