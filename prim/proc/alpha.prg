! @(#)alpha.prg	19.1 (ESO-DMD) 02/25/03 14:08:43
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure alpha.prg to implement LABEL/DISPLAY + CLEAR/ALPHA
! K. Banse      910314, 941116, 961030, 981022
! 
! use via LABEL/DISPLAY string line,column [mode] [option] [size] [key_flag]
!                       mode : A for alpha channel, O for overlay channel
!                       option : for DeAnza - submode for mode = A
!	                                      angle for mode = O
!                                for X11 - color for mode = O
! or      CLEAR/ALPHA
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
CROSSREF TEXT POSITION CHANNEL COLOR SIZE KEY
! 
action(1:3) = "ANX"
if mid$cmnd(1:1) .eq. "C" THEN
   write/keyw p1 " "			!handle CLEAR/ALPHA
   write/keyw p3 A			!make sure we have the alph option
   !
else
   if mid$cmnd(11:11) .eq. "A" then 	!here for LABEL/ANY
      action(3:3) = "F"			!indicate input from ASCII file
   else					!here for LABEL/DISPLAY
      define/param p1 ? c "Enter string: "
      define/param p2 cursor c "Enter line_no,column_no or CURSOR: "
      define/param p3 O c "Enter mode: "
      define/param p4 white c "Enter color: "
      define/param p5 0 n "Enter text size, 0/1/2 for Normal/Large/VeryLarge: "
      define/param p6 n c "Enter key_flag, K(ey) or N(okey): "
   endif
endif
!
run MID_EXE:IDFUNC
