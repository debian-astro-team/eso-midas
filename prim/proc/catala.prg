! @(#)catala.prg	19.1 (ESO-DMD) 02/25/03 14:08:44
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure catala.prg  for READ, ADD, SUBTRACT, SHOW,
!                             SEARCH, PRINT, DELETE/ICAT,TCAT,FCAT
! K.Banse        910228, 920207, 940119, 990706, 020220
!
! use as READ/xCATALOG     cat_name lowno,hino
!        PRINT/xCATALOG    cat_name lowno,hino
!        SEARCH/xCATALOG   cat_name search_string display+search_flag
!        SHOW/xCATAL       cat_name display_flag
!        SORT/xCATAL       cat_name 
!        DELETE/xCATAL     cat_name conf_flag low,hi
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
action(1:3) = mid$cmnd(1:3)		!save first 3 chars. of command...
define/local fopt/c/1/1 {mid$cmnd(11:11)}
define/local nn/i/1/1 0
define/local bounds/i/1/3 1,99999,0
! 
define/local myact/c/1/3 {action(1:3)}
define/local ioff/i/1/3 0,0,0
if fopt .eq. "I" then
   ioff(2) = 1
elseif fopt .eq. "T" then
   ioff(2) = 3
elseif fopt .eq. "F" then
   ioff(2) = 4
elseif fopt .eq. "A" then
   ioff(2) = 2
else
   write/error 3			!invalid qualifier
   return
endif
ioff(1) = catalinf({ioff(2)})
ioff(3) = ioff(1) + catalinf(11) - 1
! 
define/local actcat/c/1/{catalinf(11)} {catalogs({ioff(1)}:{ioff(3)})}
define/param p1 {actcat} c "Enter catalog name: "
if p1(1:1) .eq. " " then
   inquire/keyw p1 "Enter catalog name: "
   if p1(1:1) .eq. " " then
      write/error 3
      return
   endif
endif
! 
if m$ftset(p1) .eq. 0 then
   write/keyw in_a {p1}.cat
else
   write/keyw in_a {p1}
endif
inputi(1) = ioff(2)
!
branch action(1:3) DEL,PRI,REA,SHO,SOR,SEA -
                   DEL,PRI,PRI,SHO,SOR,SEA
write/out "Invalid command..."
return/abort
!
!  here for deleting
!  -----------------
DEL:
!
define/param p2 CONF c "Enter confirm_flag - C(onf)/N(oconf): "
define/param p3 + n "Enter low_hi limits for entries to work on: "
! 
nn = m$tstno(p2)
if nn .eq. 1 then
   write/out "Warning: Confirmation flag should not be a number!"
   write/out "We assume Param2 -> Param3 (= low,hi limits)"
   p3 = p2
   p2 = "CONF"
endif
! 
if p3(1:1) .ne. "+" then
   nn = m$index(p3,",")
   if nn .gt. 0 then
      write/keyw bounds {p3}			!low,hi limits
   else
      write/keyw bounds {p3},{p3}		!single entry
   endif
endif
define/local catal/i/1/1 0
define/local burro/c/1/60 " " all
!
if m$index(p1,".cat") .gt. 1 then
   define/local catnam/c/1/60 {p1}
else
   define/local catnam/c/1/60 {p1}.cat
endif
if catnam .eq. actcat then
   define/local subtra/i/1/1 0
else
   define/local subtra/i/1/1 1
endif
! 
CAT_LOOP:
store/frame burro {catnam} ? end_cat
! 
if catal .lt. bounds(1) then
   goto cat_loop
else if catal .gt. bounds(2) then
   goto end_cat
endif
! 
if fopt .eq. "I" then
   delete/image {burro} {p2}
elseif fopt .eq. "T" then
   delete/table {burro} {p2}
elseif fopt .eq. "F" then
   delete/fit {burro} {p2}
else
   if p2(1:1) .eq. "C" then
      -delcnf {burro}
   else
      -delete {burro} 
   endif
   subtra/{fopt}cat {catnam} {burro}
   subtra = 0
   q1 = "YES"
endif
if q1(1:1) .ne. "Y" goto cat_loop
bounds(3) = bounds(3)+1				!increment file counter
if subtra .eq. 0 goto cat_loop
! 
subtra/{fopt}cat {p1} {burro}
goto cat_loop
! 
END_CAT:					!That's the end of the loop
if bounds(3) .lt. 10 then
   set/format i1
elseif bounds(3) .lt. 100 then
   set/format i2
elseif bounds(3) .lt. 1000 then
   set/format i3
else
   set/format i5
endif
write/out catalog {catnam}: {bounds(3)} file(s) deleted ...
return
!
!  here for reading + printing
!  ---------------------------
PRI:
!
define/param p2 + N "Enter first,last frame_no to be listed: "
define/param p3 FULL C "Enter BRIEF or FULL for display option: "
! 
write/keyw inputi/i/2/2 1,9999
if p2(1:1) .ne. "+" then
   nn = m$index(p2,",")
   if nn .gt. 0 then
      write/keyw inputi/i/2/2 {p2}                    !low,hi limits
   else
      write/keyw inputi/i/2/2 {p2},{p2}               !single entry
   endif
endif
goto run_it
!
!  here for showing
!  ----------------
SHO:
define/param p2 DISPLAY ? "Enter display_flag, DISPLAY/NODISPLAY: "
goto run_it
!
!  here for sorting
!  ----------------
SOR:
goto run_it
!
! here for searching
!  ----------------
!
SEA:
define/param p2 ?       C "Enter search string: "
define/param p3 DB C "Enter display+search-flag (2 chars.): "
!
write/keyw inputc " " all		!clear INPUTC first
write/keyw inputc {p2}	
!
!  ---------------------------
!  execute the catalog utility
!  ---------------------------
!
RUN_IT:
if in_a(1:1) .eq. "+" then
   write/out "no default catalog found - (use" SET/{FOPT}CATALOG first)
   return/exit
endif
! 
if myact(1:2) .eq. "PR" @ print.prg assign		!use intermediate file
run MID_MONIT:CATAL
if myact(1:2) .eq. "PR" @ print.prg out		!print out
