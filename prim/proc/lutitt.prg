! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  lutitt.prg  for commands SET(CLEAR)/ITT, SET(CLEAR)/LUT
! 
! K. Banse	920524, 030704
!
! execute as 1) CLEAR/ITT chanl
!	     2) SET/ITT chanl ITT_sect
!            3) CLEAR/LUT channel
!            4) SET/LUT LUT_section channel
!
! for Pseudo on RGB via LUTs - IDIDEV(7) = 2,3
! the image has to be reloaded to see the effect of these commands...
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
if dazdevr(4) .ne. 1 then                       !only possible with own LUT
   write/out this command only possible with own LUT ...
   return
endif
!
if mid$cmnd(11:11) .eq. "I" then
   define/param p1 + c "Enter channel:"
!
   action(1:4) = "MEIX"
   if mid$cmnd(1:1) .eq. "C" then                  !CLEAR/ITT
      if ididev(7) .ge. 2 then
         load/itt ramp
         display/lut
         return
      endif
      dazin(1) = -1
      hcittlut(41:41) = "N"
!
   else                                            !SET/ITT
      if ididev(7) .ge. 2 return

      define/param p2 0  n "Enter ITT_section:"
      dazin(1) = {p2}
      hcittlut(41:41) = "Y"
   endif
!
else
   action(1:4) = "MELX"
   if mid$cmnd(1:1) .eq. "C" then                  !CLEAR/LUT
      if ididev(7) .ge. 2 then
         load/lut ramp ? d
         return
      endif
      define/param p1 + c "Enter channel:"
      dazin(1) = -1
      hcittlut(42:42) = "N"
!
   else						   !SET/LUT
      if ididev(7) .ge. 2 return

      define/param p1 0 n "Enter LUT_section:"
      define/param p2 + c "Enter channel:"
      dazin(1) = {p1}				 !store LUT section
      hcittlut(42:42) = "Y"
   endif
endif
! 
run MID_EXE:idauxx
