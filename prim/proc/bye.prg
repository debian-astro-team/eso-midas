! @(#)bye.prg	19.1 (ESO-IPG) 02/25/03 14:08:44
! ++++++++++++++++++++++++++++++++++++++++++++++
! 
! MIDAS procedure  bye.prg  executed with BYE command
! 
!  K. Banse	910930, 931122, 941212, 950802, 980129, 990116, 010710
! 
! ++++++++++++++++++++++++++++++++++++++++++++++
! 
if ididev(1) .ge. -1 delete/display all
! 
if pcount(1) .gt. 0 then
   @@ {p1} 
else 
   mid$info(5) = m$exist("MID_WORK:logout.prg")
   if mid$info(5) .eq. 1 @@ MID_WORK:logout
endif
! 
bye
