! @(#)rotate.prg	19.1 (ES0-DMD) 02/25/03 14:09:00
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure rotate.prg to implement ROTATE/CLOCK, /COUNTER_CLOCK
!  				          ROTATE/1DIM
! K. Banse	910205, 920401
!
! use via ROTATE/qualif inframe outframe factor
!         to rotate clock or counter_clockwise,
!         factor = 1,2 or 3 for 90, 180 or 270 degrees
! or      ROTATE/1DIM inframe outframe
!	  to rotate a 1-dim profile around its start point
!	  e.g. to build 2-dim filters from a cross section
!
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter input frame: "
define/param p2 ? ima "Enter output frame: "
! 
write/keyw history "ROTATE/{mid$cmnd(11:14)} "
!
if mid$cmnd(11:11) .eq. "1" then		!handle ROTATE/1DIM
   define/param p3 ODD C "Enter EVEN or ODD option:"
   write/keyw in_a {p1}
   write/keyw out_a {p2}
   if p3(1:1) .eq. "E" then
      inputi(1) = 1			!even dimension for result frame
   else
      inputi(1) = 0			!odd dimension for result frame
   endif
   action(1:2) = "RG"
   run MID_EXE:GENYY1
!
else
   define/param p3 1 n "Enter rotation factor 1,2 or 3:" 1,3
   if mid$cmnd(11:12) .eq. "CL" then		!handle ROTATE/CLOCK
      define/local lr/c/1/3 123		!indicate clockwise
   else
      define/local lr/c/1/3 321 	!indicate counter_clockwise
   endif
   rebin/rotate {p1} {p2} R{lr({p3}:{p3})}
endif
