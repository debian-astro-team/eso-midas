! @(#)md5.prg	19.1 (ESO-DMD) 02/25/03 14:08:54
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure md5.prg to implement COMPUTE/SIGNATURE
! K. Banse      010731
!
! use as COMPUTE/SIGNATURE file option [FITS name]
!       with option = Update (default)
!	or            Calculate_only
!	     FITS name = optional name for conversion to FITS
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? c "Enter file name:"
define/param p2 update c "Enter option:"
define/param p3 + c "Enter FITS name:"
! 
if p3(1:1) .ne. "+" then
   write/keyw action update
   define/local fits_out/i/1/1 1
else
   write/keyw action/c/1/8 p2
   define/local fits_out/i/1/1 0
endif
define/local orig_in/c/1/128 {p1}
if p2(1:1) .ne. "U" then
   define/local temp_fits/c/1/128 y__y.fits
   define/local upda/i/1/1 0
else
   define/local temp_fits/c/1/128 {orig_in}
   define/local upda/i/1/1 1			!Yes. update the descr.
endif
define/local md5/c/1/32 "." all
define/local file_md5/c/1/128 {orig_in}
! 
if aux_mode .lt. 2 then			!VMS
   define/local version/c/1/2 ".*"
else					!Unix/Linux
   define/local version/c/1/1 " "
endif
mid$info(1) = m$filtyp(orig_in,"?fits")		!check, if FITS file
! 
if mid$info(1) .eq. 1 then
   in_a = orig_in // "[0]"			!always use primary FITS header
   inputi = m$existd(in_a,"datamd5")
else
   inputi = m$existd(orig_in,"datamd5")
   if fits_out .ne. 0 then
      inputi = m$filtyp(orig_in," ")
      if inputi .eq. 2 then			!it's a Midas table
         create/image empty 0 ? no
         write/desc empty datamd5/c/1/32 "..." all
         write/dhelp empty datamd5 "MD5 signature of data in FITS format"
         fits_out = 2
      endif
   endif
endif
! 
if inputi(1) .eq. 0 then		!DATAMD5 does not exist
   !
   if mid$info(1) .eq. 1 then			!FITS file
      indisk/mfits {orig_in} x__x NOY no y__y.cat >Null
      write/keyw in_b x__x0000.bdf		!primary header => x__x0000.bdf
      write/desc {in_b} datamd5/c/1/32 "..." all
      write/dhelp {in_b} datamd5 "MD5 signature of data in FITS format"
      outdisk/sfits y__y.cat {temp_fits} 		!(over)write FITS file
      run MID_EXE:fitsmd5.exe				!and update
      delete/icat y__y.cat no >Null		!delete files from indisk/mfits
      -delete y__y.cat{version}
      if p2(1:1) .eq. "C" then
         -delete {temp_fits}{version}
      endif
   !
   else						!Midas file
      if fits_out .ne. 2 then
         write/desc {orig_in} datamd5/c/1/32 "..." all
         write/dhelp {orig_in} datamd5 "MD5 signature of data in FITS format"
      endif
      goto case_3
   endif
else					!DATAMD5 exists
   !
   if mid$info(1) .eq. 1 then			!FITS file
      run MID_EXE:fitsmd5.exe				!update
   !
   else						!Midas file
     case_3:
      if fits_out .ne. 0 then			!create FITS file
         if fits_out .eq. 1 then
            outdisk/sfits {orig_in} {p3} >Null
         else
            outdisk/sfits empty.bdf,{orig_in} {p3} >Null
         endif
         file_md5 = p3
         run MID_EXE:fitsmd5.exe			!signature -> OUTPUTC
      else
         outdisk/sfits {orig_in} x__x.fits >Null
         file_md5 = "x__x.fits"
         run MID_EXE:fitsmd5.exe			!signature -> OUTPUTC
         !
         if upda .eq. 1 then
            write/desc {orig_in} datamd5/c/1/32 {outputc}	!update
         endif
         -delete x__x.fits{version}
      endif
   endif
endif
