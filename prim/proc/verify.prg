! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify.prg  to setup and run the verifivations
!        for the plot related procedures of DFO for QC plots, or
!        for the EFOSC verification  procedures from La Silla
!  K. Banse     100416	creation
! 
! use via: @ verify efosc option	!for EFOSC CCD tests
! 		option = OPT for gcc version > 4.2
!			or NOOPT for older compiler versions
!			defaulted to  OPT 
! 	   @ verify qc			!for QC plotting tests
! 	   @ verify all			!for both tests
! 
! some versions of gcc yield reproducibly different results
! in the INVENTORY package, when the -O flag was set in the
! compilation of that package (/midas/version/contrib/invent)
! therefore, if the EFOSC seeing test fails, execute this procedure with
! param2 = NOOPTImized
! i.e. Midas> @ verify efosc noopt
! 
!.VERSION
! 120131	last modif
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 EFOSC C "test QC plotting or EFOSC:"
! 
if ididev(1) .gt. -1 reset/display
! 
! switch for QC or EFOSC 
! 
delete/keyw name		!keyword 'name(8)' created by some tutorial...
if p1(1:2) .eq. "QC" then
   define/param p2 YES C "copy all data needed for tests (yes/no):"
   if p2(1:2) .ne. "NO" then
      define/local sourcedir/c/1/80 $MIDASHOME/ESO-verifications/qcplot-DARK/
      define/local defdir/c/1/2 "./"
      !
      write/out copying all files from {sourcedir}
      $ cp {sourcedir}* {defdir}
   endif
   reset/display
   create/grap 0 800,480,0,0
   ! 
   ! " @@ verifyQC switch option plotflag"
   ! "switch = switch for execution:"
   ! "        1 = create plots, 2 = compare results,"
   ! "        3 = 1 + 2 (default)"
   ! "option = option for plot generation"
   ! "        CLOSEUP (default) or MAIN"
   ! "plotflag = flag for plotting the final postscript file"
   ! "        NOPLOT (default) or PLOT"
   ! 
   @@ verifyQC 3 ? plot
! 
else if p1(1:2) .eq. "EF" then
   define/param p2 NEW C "enter NEW/OLD for gcc versions > 4.2:"
   define/param p3 YES C "copy all data needed for tests (yes/no):"
   write/keyw mid$verif/c/1/12 "{P2(1:3)}   "   !use keyword mid$verif 
                                                !to pass this option
   if p3(1:2) .ne. "NO" then
      define/local sourcedir/c/1/80 $MIDASHOME/ESO-verifications/EFOSCtest/
      define/local defdir/c/1/2 "./"
      !
      write/out copying all files from {sourcedir}
      $ cp {sourcedir}* {defdir}
   endif
   @@ verifyEFOSC 
else
   set/midas output=red
   write/out "wrong parameter - should be QC or EFOSC or ALL"
   set/midas output=default
endif
