! @(#)plotkey.prg	19.2 (ESO-DMD) 06/05/03 11:35:41
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLOTKEY.PRG
!.PURPOSE:        Plot a "keyword" on the graphic device
!                 execute as @ PLOTKEY par1 [par2] [par3] where:
!                 par1 = name of the keyword (defaulted to OUTPUTR)
!                 par2 = first data, last data point
!                 par3 = scale_x,scale_y,off_x,off_y
!                 or
!                 par3 = offset in y
!.AUTHOR:         Rein Warmels
!.VERSION:        880921 RHW Creation
!.VERSION:        881123 RHW Combining PLOT/ and OVERPLOT/ commands
!.VERSION:        900409 KB  ???
!.VERSION:        920224 RHW Offset in x and y included
! -------------------------------------------------------------------
! 
DEFINE/PARAM P1 OUTPUTR C                           ! default keyword
DEFINE/PARAM P2 0,0 N                               ! default all elements
!
WRITE/KEYW IN_A           {P1}
WRITE/KEYW INPUTI/I/1/2   {P2}
!
IF MID$CMND(1:1) .EQ. "P" THEN
   DEFINE/PARAM P3 0.,0.,-999,-999 NUM
   @ plscoff.prg_o {P3}
   DATTIM = M$TIME()
ELSE
   DEFINE/PARAM P3 0 NUM                           ! scale or offset in y
   WRITE/KEYW  INPUTR/R/1/1 {P3}
ENDIF
!
RUN MID_EXE:PLOTKEY
WRITE/KEYW PLCDATA/C/1/60  {P1}                   ! name and type data struct.
WRITE/KEYW PLCDATA/C/61/20 "KEYWORD    "
copy/graph

