! @(#)t2edittbl.prg	19.1 (ESO-DMD) 02/25/03 14:09:05
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : TEDITTBL.PRG
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching   14 mar 84
!					    01 Aug 85  KB
!					    30 Oct 86  JDP
!                                           04 Oct 90  MP
!                                           28-Oct-90  FO (Options)
! .PURPOSE
!
!    implements
!
!  EDIT/TABLE  table-name [options read-only/update | column row ]
!		(Column + Row only for creation of a new table)
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
!
DEFINE/LOCAL TMPTABL/C/1/14 " " all
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
WRITE/KEYW INPUTI/I/1/2  0,0	! Values of columns + rows
IF PCOUNT .GE. 2 THEN
  IF M$TSTNO(P2) .EQ. 0 THEN
    WRITE/KEYW INPUTI/I/1/1 -1
  ELSE
    WRITE/KEYW INPUTI/I/1/1 {P2}
    WRITE/KEYW INPUTI/I/2/1 {P3}
  ENDIF 
ENDIF
IF INPUTI(1) .EQ. 0  THEN 	! Copy to a temporary table
  IF M$INDEX(P1,".") .EQ. 0  THEN
    -COPY {P1(1:>)}.tbl MIDASEDT{mid$sess(11:12)}.tbl
    WRITE/KEY TMPTABL/C/1/14 MIDASEDT{mid$sess(11:12)}.tbl   
   ELSE
    -COPY {P1(1:>)} MIDASEDT{mid$sess(11:12)}.tbl
    WRITE/KEY TMPTABL/C/1/14 MIDASEDT{mid$sess(11:12)}.tbl
  ENDIF
ELSE
ENDIF
RUN MID_EXE:tedittbl           
