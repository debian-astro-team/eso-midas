! @(#)plscoff.prg	19.2 (ESO-DMD) 03/10/03 16:41:25
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLSCOFF.PRG
!.PURPOSE:        MIDAS procedure reads the input scales and offset of 
!                 the plot
!                 par1 = input string with x scale, y scale, x offset, y offset
!.VERSION:        931021  RHW creation
! 030228	last modif
! 
!-------------------------------------------------------------------------
! 
define/param p1 ? n "Enter x,y scaling and x,y offset:"
!
define/local rtest/r/1/4 0.0,0.0,-999,-999               ! xsc, ysc, xoff, yoff
write/keyw rtest/r/1/4 {p1}                              ! copy input
!
if rtest(1) .ne. 0  then  
   plrgrap(13) = rtest(1)               ! change of default?
else
   plrgrap(13) = plrstat(13)            ! use the defaults
endif
if rtest(2) .ne. 0  then  
   plrgrap(14) = rtest(2)
else
   plrgrap(14) = plrstat(14)
endif
! 
if rtest(3) .ne. -999 then
   plrgrap(29) = rtest(3)
else
   plrgrap(29) = plrstat(21)
endif
if rtest(4) .ne. -999 then
   plrgrap(30) = rtest(4)
else
   plrgrap(30) = plrstat(22)
endif
