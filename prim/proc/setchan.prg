! @(#)setchan.prg	19.1 (ESO-DMD) 02/25/03 14:09:01
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure setchan.prg
! K. Banse	880224, 910318, 920312
!
! execute as   SET/CHAN chanl
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local lcha/i/1/1 0
! 
branch p1(1:1) O,R,G,B,A OVERLAY,RED,GREEN,BLUE,ALL
!
define/param p1 ? N "Enter channel no.: " 0,{IDIDEV(16)}
ididev(15) = {p1}
return
!
OVERLAY:
ididev(15) = ididev(14)
return
!
RED:
ididev(15) = 0					!we always have a channel 0
return
! 
GREEN:
lcha = 1
goto last
! 
BLUE:
lcha = 2
goto last
! 
ALL:
if ididev(18) .eq. 11 then
   write/out Invalid qualifier for X11 display...
   return/exit
else
   ididev(15) = 999
   return
endif
! 
LAST:
if lcha .gt. ididev(16) then
   write/out Invalid channel no. ...
   return/exit
else
   ididev(15) = lcha			!finally update the channel no.
endif
