! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2ttcptbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!
! .PURPOSE
!
!    implements
!
!  COPY/TT     intable incol outable [outcol]
!
! 080401	last modif
! ----------------------------------------------------------------------
!
define/param p1 ? TABLE  "Enter table:"
define/param p2 ? CHAR   "Enter column:"
define/param p3 {P1} TABLE
define/param p4 {P2} CHAR   "Enter column:"
!
define/local in_rows/i/1/1   0                 
define/local allo_rows/i/1/1 0
define/local out_rows/i/1/1  0                 
define/local newrows/i/1/1   0
!
if m$ftset(p1) .eq. 0 then                  !check, if no file extension
   write/keyw in_a {p1}.{mid$types(9:16)}
else 
   in_a = p1
endif
!
if p3(1:1) .eq. ":" .or. p3(1:1) .eq. "#" then  !check if P3 contains column 
   p4 = p3
   p3 = p1

else                                        !P3 contains table
   if m$ftset(p3) .eq. 0 then
      write/keyw in_b {p3}.{mid$types(9:16)}
   else 
      in_b = p3
   endif
   !
   in_rows  = m$value({in_a},TBLCONTR(4))   !this is the number of existing
                                            !rows in the input table
   allo_rows = m$value({in_b},TBLCONTR(2))  !this is the number of allocated
                                            !rows in the output table
   !
   if in_rows .gt. allo_rows then
      out_rows = m$value({in_b},TBLCONTR(4)) !number of existing rows
      newrows = in_rows - out_rows           !how many rows have to be created
      create/row {in_b} @{out_rows} {newrows}    !add these to output table
      mid$cmnd(1:4) = "COPY"
      mid$cmnd(11:14) = "TT " 
   endif
endif   
!
write/keyw history "{mid$cmnd(1:4)}/{mid$cmnd(11:14)} "
!
run MID_EXE:tdatatbl

