! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2projtbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!  070419	last modif
!
! .PURPOSE
!
!    implements
!
!  PROJECT/TABLE  intable outable column_spec
!
! ----------------------------------------------------------------------
!
define/param p1 ? TABLE  "Enter table:"
define/param p2 ? TABLE  "Enter table:"
define/param p3 ? CHAR   "Enter column:"
!
if p2(1:1) .eq. "#" .or. p2(1:1) .eq. ":" then
   write/out "Error: column specs must be last parameter..."
   write/err 5 
   return/exit
else
   write/keyw history "{mid$cmnd(1:4)}/{mid$cmnd(11:14)} "
   run MID_EXE:tdatatbl
endif
