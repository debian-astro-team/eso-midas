! @(#)reblin.prg	19.1 (ESO-IPG) 02/25/03 14:08:59
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure reblin.prg  to implement REBIN/LINEAR, /SPLINE
!					   RESAMPLE/IMAGE
! K. Banse	910206, 920401, 950322, 970626, 010731
!
! execute as 
!  REBIN/LIN infr outfr stepx,stepy offx,offy startx,starty fluxcons proc_opt
!  REBIN/LIN infr outfr refframe fluxcons proc_opt
!
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input frame: "
define/param p2 ? IMA "Enter output frame: "
!
define/local PIXEL/C/1/3 "WC "       ! default are world coordinates
!
if mid$cmnd(1:3) .eq. "RES" then	!RESAMPLE/IMAGE
   define/param p3 half_step ? "Enter half_ or double_step:"
! 
else					!REBIN/...
   define/param p3 1.,1. ? "Enter stepx,stepy  or  reference_image: "
   inputi = m$parse(p3,"step")
   if step01(1:1) .eq. "@" then        ! this part only for pixel input @no.
         step01 = step01(2:)
         if step02(1:1) .eq. "@" step02 = step02(2:)
         p3 = step01 //  "," // step02 
         PIXEL = "PIX"
   endif
endif
!
write/keyw in_a {p1}
write/keyw out_a {p2}
default(3:3) = mid$cmnd(11:11)		!save qualifier
!
define/local num/i/1/1 0
num = m$tstno(p3)			!test, if frame or numbers
!
if num .eq. 0 then
   define/param p3 {p3} IMA "ref_image again:"	!needed for type stuff...
   define/param p4 NO  ?   "Enter flux_conservation_flag: "
   define/param p5 +   ?   "Enter processing option (ROW,PLANE):"
   define/maxpar 5
   write/keyw in_b {p3}
   default(1:1) = p4(1:1)		!flux cons. flag in P4
   default(4:4) = p5(1:1)	
!
else
   define/param p4 0.  ?   "Enter off_x,off_y: "
   define/param p5 +   C   "Enter absolute new start_x,y: "
   define/param p6 NO  ?   "Enter flux_conservation_flag: "
   define/param p7 +   ?   "Enter processing option (ROW,PLANE):"
   define/param p8 {PIXEL}  ?   "Enter type of input x,y (WC/PIX default WC):"
!   
!   define/maxpar 7   --- why this? PN 7/99---
   define/maxpar 8
   in_b(1:1) = "+"
   write/keyw inputd/d/1/2 {p3},1.0	!make sure, we have 2 values...
   write/keyw inputd/d/3/2 {p4},0.0	!make sure, we have 2 values...
   if p5(1:1) .eq. "+" then
      default(2:2) = "N"
   else
      default(2:2) = "Y"
      write/keyw inputd/d/5/2 {p5},{p5}
   endif
   default(1:1) = p6(1:1)		!flux cons. flag in P6
   default(4:4) = p7(1:1)
   default(5:5) = p8(1:1)               !store input type: world coor or pixels  
endif
! 
if default(1:1) .eq. "N" then		!use opposite flag internally
   default(1:1) = "Y"
else
   default(1:1) = "N"
endif
!
if default(3:3) .eq. "L" then
   write/keyw history "REBIN/LINEAR "
else if default(3:3) .eq. "I" then
   write/keyw history "RESAMPLE/IMAGE "
else
   write/keyw history "REBIN/SPLINE "
endif
!
run MID_EXE:REBIN
