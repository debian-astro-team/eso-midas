! @(#)rcosmic.prg	19.1 (ES0-DMD) 02/25/03 14:08:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION:  rcosmic.prg
!.PURPOSE:         MIDAS procedure to remove and delete cosmic ray events 
!                  on single CCD exposure.
!.USE:             FILTER/COSMIC p1 p2 p3 p4  where:
!                     P1: input image containing the cosmic rays,
!                     P2: median filter of the input image 
!                     P3: SKY,RON,G,NS,RC
!                     P4: mask name
!.AUTHOR:          P.Magain, M.Remy, Institut d'Astrophysique de Liege  
!.VERSION:         911121 MP implemented in Portable MIDAS
!-------------------------------------------------------------------------------
! 
define/param p1 ? IMA "Enter input image:"
define/param p2 ? IMA "Enter output image:"
define/param p3 ? NUM "Enter SKY,G,RON,NS,RC:"
define/param p4 + IMA "Enter mask name for cosmics:"
define/param p5 1,1,0.0 N "Enter parameter P3 for FILTER/MEDIAN:"
define/param p6 NA C "Enter parameter P4 for FILTER/MEDIAN:"
define/maxpar 6
! 
write/keyw in_a {p1}
define/local outima/c/1/60 {p2} 
define/local params/r/1/5 -1,-1,-1,4,2
write/keyw params {p3}
define/local cosmic/c/1/60 {p4}
! 
write/out "Beginning of the median filter"
filter/median {in_a} middumma {p5} {p6}
! 
write/out "Beginning of the cosmic rays removal"
write/keyw history "FILTER/COSMIC "
! 
run MID_EXE:RCOSMIC
