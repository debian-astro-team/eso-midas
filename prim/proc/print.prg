! @(#)print.prg	19.1 (ES0-DMD) 02/25/03 14:08:58
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  print.prg  to handle the printing business
! K. Banse	910404, 910531, 910611, 920121, 930208
!
! use via  @ print assign [print_file]
! and      @ print out  
! 
! or       @ print print [print_file]
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p2 midtemp{mid$sess(11:12)}.print C "Enter print file: "
! 
branch p1(1:1) A,O,P ASSIGN,OUT,PRINT
! 
write/out Invalid option for print.prg ...
return
! 
ASSIGN:					!assign stuff
if mid$prnt(1:2) .ne. "F:" then
   mid$prnt(53:102) = mid$prnt(3:52)	!save original device
   write/keyw mid$prnt/c/3/50 {p2}
endif
! 
log(9) = 1					!set print_flag
error(6) =  error(3)				!save current ERROR(3)
error(3) = -1                                   !make sure we continue
return
! 
PRINT:
mid$prnt(53:102) = mid$prnt(3:52)	!fill MID$PRNT(53:102) also
! 
OUT: 					!print stuff
log(9) = 0				!clear print_flag
! 
if mid$prnt(1:2) .ne. "F:" then
   if progstat(1) .eq. 0 then			!only if no error...
      if aux_mode(1) .lt. 2 then
         if mid$prnt(1:2) .ne. "T:" then
            $ {mid$prnt(53:>)} MID_WORK:{p2}
         else
            $ TYPE/PAGE MID_WORK:{p2}
         endif
      else
         if mid$prnt(1:2) .ne. "T:" then
             $ cat $MID_WORK/{p2} | {syscoms(21:39)}{mid$prnt(53:>)}
         else
            $ more $MID_WORK/{p2}
         endif
      endif
   endif
   mid$prnt(3:52) = mid$prnt(53:102)		!always reset...
endif
! 
error(3) = error(6)				!reset error
