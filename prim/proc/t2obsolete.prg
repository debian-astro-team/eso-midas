! @(#)t2obsolete.prg	19.1 (ES0-DMD) 02/25/03 14:09:06
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2obsolete.prg
!  TABLE Subsystem
! .PURPOSE
!
!  Warning: Obsolete command
!
! ----------------------------------------------------------------------
!
IF MID$CMND(1:1) .EQ. "W" THEN
WRITE/OUT Command obsolete. 
WRITE/OUT "Old form: WRITE/TABLE table :label @n value"
WRITE/OUT "New form: table,:label,@n = value"
RETURN
ENDIF
!
IF MID$CMND(11:11) .EQ. "K" THEN
WRITE/OUT Command obsolete. 
WRITE/OUT "Old form: COPY/KT keyword table :label @n" 
WRITE/OUT "New form: table,:label,@n = keyword"
ELSE
WRITE/OUT Command obsolete. 
WRITE/OUT "Old form: COPY/TK table :label @n keyword"
WRITE/OUT "New form: keyword = table,:label,@n"
ENDIF
