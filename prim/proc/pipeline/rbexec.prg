! @(#)rbexec.prg	14.1.1.7 (ESO-DMD) 01/21/00 09:15:06
! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure rbexec.prg  for VLT pipeline
! K. Banse      990729, 991202, 000525, 020806
! 
! this procedure builds a wrapper for an RB and executes it in Midas
! 
! +++++++++++++++++++++++++++++++++++++~
! 
define/param p1 ? ? "Enter RB definition file:"
define/param p2 N c "Enter internal print flag:"
define/param p3 Y c "Enter procedure print flag:"
! 
write/keyw in_a {p1}
if p2(1:1) .ne. "N" then
   inputi(1) = 1
else
   inputi(1) = 0
endif
write/keyw command1/c/1/100 " " all
write/keyw command2/c/1/100 " " all
write/keyw command3/c/1/100 " " all
! 
define/local mm/i/1/1 0
set/format i1
do mm = 1 8				!clear keywords rbs_p1, ..., rbs-p8
   write/keyw rbs_p{mm}/c/1/100  " " all
   rbs_p{mm}(1:1) = "?"
enddo
write/keyw rbs_pcount/i/1/1 0		!no. of recipe parameters
write/keyw rbs_recipe/c/1/200  " " all
! 
$ pwd | write/keyw in_B			!if necessary, move up
inputi(2) = m$len(in_b)
inputi(3) = inputi(2) - 4
if in_b({inputi(3)}:{inputi(2)}) .eq. "_pipe" then
   change/direc ..
endif
! 
run MID_MONIT:rbexec.exe
if outputi .ne. 0 then
   write/error 100
   return/exit		!problems with RB
endif
! 
if p3(1:1) .ne. "N" then
   write/out "the following two commands will be executed now:"
   write/out "@d recipe_aux "
   write/out with 3 parameters:
   write/out {command1}
   write/out {command2}
   write/out {command3(1:>)}
endif
! 
! this may do: CLEAR/CONT -t		which deletes local keywords...
@d recipe_aux {command1} {command2} {command3(1:>)}
! 
define/local kk/i/1/1 0
if p3(1:1) .ne. "N" then
   write/out "@@ {rbs_recipe}" 
   write/out with {rbs_pcount} parameters:
   do kk = 1 rbs_pcount
      write/out {rbs_p{kk}} 
   enddo
   write/out "--- "
   write/out
endif
! 
!  build up command line + write into procedure
! 
define/local fc/i/1/2 0,0
open/file exec_recipe.prg write fc
if fc(1) .lt. 0 then
   write/out Could not create procedure `exec_recipe.prg' ...
   return/exit
endif
! 
write/file {fc(1)} "@@ {rbs_recipe} -"
do kk = 1 rbs_pcount
   if kk .eq. rbs_pcount then
      write/file {fc(1)} {rbs_p{kk}}
   else
      write/file {fc(1)} "{rbs_p{kk}} -" 
   endif
enddo
close/file {fc(1)}
! 
@@ exec_recipe				!finally, execute the recipe 
! 
$ pwd | write/keyw in_B			!if necessary, move up
inputi(2) = m$len(in_b)
inputi(3) = inputi(2) - 4
if in_b({inputi(3)}:{inputi(2)}) .eq. "_pipe" then
   change/direc ..
endif
! 
entry start
define/param p1 NoDisplay C
@d pipeline.start {p1}
! 
show/comm exec/rbs >Null
if outputi(9) .eq. -1 then
   set/context rbs >Null			!because all commdefs are lost 
endif
! 
entry help
write/out current path for CalibDBs:
write/out "   " {calibdb_rul}
write/out "   " {calibdb_rec}
write/out
write/out MID_PROC:/pipeline/rbexec_help.txt

