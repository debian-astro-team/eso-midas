! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure recipe_aux.prg  for VLT pipeline
! K. Banse	980709, 990716, 000929, 011213, 020227, 070228
! 
! this procedure creates the product directory if necessary
! and sets RB definition file, ReductionRecipe, instrument
! and maxtime for a recipe
! also ensures that the instrument related context is set
! 
! previous version used:
! p1 = RB definition file
! p2 = max_time
! p3 = instrument
! 
! +++++++++++++++++++++++++++++++++++++~
! 
define/param p1 ? C "Enter RB definition file:"
define/param p3 ? C "Enter instrument of RB:"
! 
! section for product directory creation
! 
define/local fc/i/1/2 0,0
define/local record/c/1/256 " " all
define/local dir_pro/c/1/256 " " all
define/local idx/i/1/1 0 
!
open/file {p1} r fc
if fc(1) .lt. 0 then
   write/out "Could not read file {p1} ... return"
   return/exit
endif
!
read_loop:
write/keyw record " " all
read/file {fc(1)} record
!
if fc(2) .eq. -1 goto eof
if record(1:1) .eq. "{" goto eof                !already a frame set
!
if fc(2) .lt. 1 goto read_loop                  !blank line
if record(1:1) .eq. " " goto read_loop          !blank line
!
idx = m$index(record,":") + 1
if record({idx}:{idx}) .eq. " " goto read_loop	!label: abc
! 
$dirname {record} | write/keyw dir_pro		!pull out directory
!
eof:
close/file {fc(1)}
if dir_pro(1:1) .eq. " " then
   write/out "Could not find product directory spec in {p1} ... return"
   return/exit
endif
!
if m$exist(dir_pro) .eq. 0 then
   $ mkdir -p {dir_pro}
endif
! 
! end of section
! 
define/local num/i/1/1 0
num = m$tstno(p2)                       !test, if frame or numbers
! 
if num .eq. 1 then			!old version
   define/param p2 ? N "Enter max_time in seconds:"
   recipe_name = "Not Given "
   pipeline(7) = {p2}			!save max exec time of next recipe
else
   define/param p2 ? C "Enter ReductionRecipe:"
   define/param p4 -1 N "Enter max_time in seconds:"
   recipe_name = p2(1:80)		!save ReductionRecipe
   pipeline(7) = {p4} 			!save max exec time of next recipe
endif
! 
rbname = p1				!save RB definition file
pipeline(2) = 1				!always force directory cleanup
pipeline(6) = 0				!always reset super-recipe_flag
! 
! if running with RBS, reset the error switches
if pipeline(12) .ne. 1 -
   write/keyw error/i/1/7 0,0,0,1,0,0,0
write/keyw in_a/c/1/20 " " all
in_a = m$upper(p3)			!compare new instr. with last used one
if rbinstrument(1:20) .ne. in_a(1:20) then
   if rbinstrument(1:1) .ne. " " then
      write/out >>> Change in Pipeline
      write/out >>> switching from instrument {rbinstrument} to {in_a(1:20)}
   else
      write/out >>> Running Pipeline for instrument {in_a(1:20)}
   endif
   rbinstrument = in_a(1:20)
   ! 
   clear/context -total		!if new, clear all contexts first
   set/midas user=exp
   create/comm load/imag @d pipeload
   create/comm log/recipe @d recipe_log
   create/comm send/mess @d sendmess
   set/midas user=user
   if pipeline(12) .eq. 1 then
      set/context rbs >Null		!because all commdefs are lost
   endif
   ! 
   set/context {rbinstrument} >Null
   !
   if m$existk("nice_crea") .eq. 1 nice_crea = "X"
endif
! 
if pipeline(12) .eq. 1 return		!inside Midas don't touch the logfile...
! 
delete/logfile				!start logfile afresh
inputi = log(12)
log(12) = 0
write/out start of logging for RedBlock: {p1} >Null
write/out 
log(12) = inputi
 
