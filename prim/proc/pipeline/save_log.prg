! @(#)save_log.prg	19.1 (ESO-DMD) 02/25/03 14:10:54
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure save_log.prg  
! to save the logfile each day after 12:00 in another file + restart logging
! or save the logfile after an error or after BYE
! K. Banse	970921, 980130, 990604, 000524, 020215
!
! execute via   @d save_log option
! with option = init   - to start
! or            time   - save after 12:00
! or            error  - save after error
! or            bye    - save after command BYE
! or            RB     - save RB specific logfile
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 init c "Enter option:"
define/local k/i/1/1 0 
!
if p1(1:1) .eq. "E" then		!option = error
   @d save_log,logcopy Midas_err.{logdate}
! 
else if p1(1:1) .eq. "B" then		!logfile save after BYE
   set/format i2
   if pipeline(5) .eq. 2 then		!we are in niceRBS
      b,bye
   endif
   do k = 1 99
      if m$exist("DFS_LOG:Midas_bye_{k}.{logdate}") .eq. 0 then
         if mid$mode(9) .gt. 0 then		!after gomidas
            k = k - 1			!overwrite last logfile
         endif
         @d save_log,logcopy Midas_bye_{k}.{logdate}
         return
      endif
   enddo
   write/out "procedure save_log.prg - we should never come here..."
! 
else
   set/format i1
   inputc = m$time()			!get current date + time
   k = m$index(inputc,":") - 2		!point to hours
   ! 
   in_a = m$time(1)			!get today's date as yyyy-mm-dd
   if inputc({k}:{k}) .eq. "0" then	!before 10:mm:ss
      in_a = m$time(-1)			!get yesterday's date as yyyy-mm-dd
   else
      k = k+1
      if inputc({k}:{k}) .lt. "2" then	!before 12:mm:ss
         in_a = m$time(-1)		!get yesterday's date as yyyy-mm-dd
      endif
   endif
   ! 
   if p1(1:1) .eq. "I" then		!option = init
      write/keyw logdate/c/1/10 {in_a(1:10)}	!yyyy-mm-dd is 10 chars
   ! 
   else if p1(1:1) .eq. "R" then	!copy RB specific logfile
      inputi = log(12)
      log(12) = 0
      write/out  
      write/out end of logging for RedBlock: {rbname} >Null
      log(12) = inputi
      ! 
      define/local logfile/c/1/124 " " 
      k = m$indexb(rbname,"/") 
      if k .gt. 0 then
         k = k + 1
         logfile = "{rbname({k}:)} "
      else
         logfile = rbname
      endif
      k = m$len(logfile) - 2
      logfile = "{logfile(1:{k})}rblog"
      @d save_log,logcopy {logfile}
   ! 
   else 				!only option = time remains...
      @d save_log,logcopy Midas.{logdate}
      delete/logfile
      write/keyw logdate/c/1/10 {in_a(1:10)}	!update start date
   endif
endif
! 
entry logcopy
! 
if pipeline(12) .ne. 1 then			!inside Midas no logifle copy...
   log/off
   $cp $MID_WORK/FORGR{mid$sess(11:12)}.LOG $DFS_LOG/{p1}
   log/on
endif
