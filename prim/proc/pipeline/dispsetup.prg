! @(#)dispsetup.prg	19.1 (ESO-DMD) 02/25/03 14:10:52 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  dispsetup.prg  for starting up an interactive Midas for
!                                 the display in parallel with the RBS Midas
! K. Banse      970204, 981214, 990120, 001117
!
! this procedure is used with the startup of a display Midas (with unit XY)
! which is done inside the shell script `niceRBS'
! in `niceRBS' also the RBS Midas is started with unit UV 
! it is assumed that UV = XY-1 (e.g. XY = 56, UV = 55)
!
! use via @d dispsetup Xdisplay		(Xdisplay e.g. xt119:0)
! M. Peron     990109  Replace wg5pl by wu1pl. We are in Paranal now! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
set/format i1
define/local host/c/1/10 " " all
host = m$symbol("DISPLAY")
! 
if host(1:5) .eq. "wu1pl" then				!VLT/UT1
   define/param p1 wu1pl:0.1 c "Enter display:"
elseif host(1:5) .eq. "wg5pl" then			!NTT
   define/param p1 wg5pl:0.1 c "Enter display:"
   @@ xobs					!start observing GUI at NTT
else
   define/param p1 + c "Enter display:"
endif
! 
if p1(1:1) .ne. "+" then
   write/keyw pipeline_disp/c/1/20 "{p1}  "
   create/disp 0 1124,870 ? ? 80000 {p1}
   create/graph 0 625,425,900,0 p4={p1}
else
   write/keyw pipeline_disp/c/1/20 "local  "
   create/disp 0 1124,870 ? ? 80000
   create/graph 0 625,425,900,0
endif
! 
load/lut heat ? d
@d pipeline.control,getFont			!search for Font
! 
! create temporary directory with Midas unit
! 
define/local Munit/i/1/2 {mid$sess(11:12)},0		!convert to number
if Munit .lt. 10 then
   write/keyw direc1/c/1/24 tmp_0{Munit}_disp
else
   write/keyw direc1/c/1/24 tmp_{Munit}_disp
endif
if Munit .eq. 0 then
   Munit(2) = 99
else
   Munit(2) = Munit(1) - 1
endif
if Munit(2) .lt. 10 then
   write/keyw direc2/c/1/24 tmp_0{Munit(2)}_pipe
else
   write/keyw direc2/c/1/24 tmp_{Munit(2)}_pipe
endif
if m$exist(direc1) .ne. 1 $mkdir {direc1}		!move to ./tmp_XY_disp
change/direc ./{direc1}
set/midas dpath=../{direc2}	!to get access to the other Midas' data
! 
! keyword PIPELINE_AUX and PIPELINE were created in `pipeline.control'
pipeline(5) = 1				!we are the display Midas ...
