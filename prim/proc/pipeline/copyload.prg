! @(#)copyload.prg	19.1 (ESO-DMD) 02/25/03 14:10:52 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure copyload.prg  to first copy from RBS-Midas workspace to
!				display-Midas workspace
!				and then use pipeload.prg
! K. Banse	990115
! execute via 
! @ copyload frame chanl xscal,yscal[,A] xc,yc cut_lo,cut_hi direcs fixpoint
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref FRAME CHANL SCALE CENTER CUTS DIRS FIX
! 
$pwd | write/keyw inputc               !get current directory
inputi = m$len(inputc)
if inputi .gt. 10 then
   inputi = inputi-4
   if inputc({inputi}:) .eq. "_disp" then	!o.k. in the right directory
      $rm -f *
      -copy ../{direc2}/{p1} {p1}
      ! 
      @d pipeload {p1} {p2} {p3} {p4} {p5} {p6} {p7} 
      return
   endif
endif
! 
write/out "current dir:" {inputc}
write/out "we are NOT in the display-Midas directory..."
write/out command LOAD/IMAGE not executed!
