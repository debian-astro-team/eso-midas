#! /bin/sh
# @(#)DHSlink.sh	19.1 (ESO-IPG) 02/25/03 14:10:52
#
# Bourne shell script  DHSlink.sh  for VLT pipeline
# K. Banse      980605, 981215
#
# this script sets a link in the directory $DFS_DATA_REDUCED_OLAS 
# to the result file
# (usually with path to DFS_PRODUCT) and sets resfile to readonly protection
#
#
chmod a-w+r $1
# 
ln -s $1 $DFS_DATA_REDUCED_OLAS/$2 

