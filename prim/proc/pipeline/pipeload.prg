! @(#)pipeload.prg	19.1 (ESO-DMD) 02/25/03 14:10:53 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure loadima.prg  to load an image frame into image display
!                              or other hardcopy devices
! K. Banse	981117, 990909, 000209, 010125
! execute via 
! LOAD/IMAGE frame chanl xscal,yscal[,A] xc,yc cut_lo,cut_hi direcs fixpoint
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref FRAME CHANL SCALE CENTER CUTS DIRS FIX
! 
if pipeline(5) .eq. 0 return
! 
clear/chan over
@% loadima {p1} {p2} {p3} {p4} {p5} {p6} {p7} 
! 
define/param p8 YES c
if p8(1:1) .ne. "Y" return		!we don't want any labels...
! 
define/local mycol/c/1/8 "white   "	!set color to black, blue or white
pipeline_aux(1) = 2			!1 = black, 5 = blue, 2 = white
error(3) = 4
error(4) = 0
@a getcoord {p1} s10,s25 >Null			!get lower left frame pixels
error(4) = 1
! 
if progstat .eq. 0 then
   outputi(1) = outputd(10)
   outputi(2) = outputd(11)
   inputi(1) = outputi(1)+180			!area = 180 x 80 for stats
   inputi(2) = outputi(2)+80
   outputi(10) = m$value({p1},npix(1))
   outputi(11) = m$value({p1},npix(2))
   if inputi(1) .gt. outputi(10) inputi(1) = outputi(10)
   if inputi(2) .gt. outputi(11) inputi(2) = outputi(11)
   statistics/image {p1} [@{outputi},@{outputi(2)}:@{inputi},@{inputi(2)}] >Null
   define/local rmedian/r/1/1 {outputr(8)}
   inputr(1) = m$value({p1},lhcuts(1))	     !compare median with CUTS values
   inputr(2) = m$value({p1},lhcuts(2))
   inputr(3) = (inputr(2)-inputr(1))/3.		!divide CUTS interval 
   inputr(4) = inputr(1)+2*inputr(3)
   ! 
   if rmedian .ge. inputr(4) then
      mycol = "black   "
      pipeline_aux(1) = 1
   else
      inputr(4) = inputr(4)-inputr(3)
      if rmedian .ge. inputr(4) then
         mycol = "blue    "
         pipeline_aux(1) = 5
      endif
   endif
else
   progstat = 0
endif
error(3) = 0
! 
define/local label/c/1/100 " " all
define/local labk/i/1/1 0
write/keyw label "OBJECT: "
if m$existd(p1,"IDENT") .eq. 1 then
   label(9:) = "{{p1},IDENT}"
   labk = m$strlen(label)
else
   labk = 8
endif
label/display "{label(1:{labk})}" 55,5 o {mycol} {pipeline_aux(2)}
! 
write/keyw label "ORIGFILE: "
if m$existd(p1,"ORIGFILE") .eq. 1 then
   label(11:) = "{{p1},ORIGFILE}"
   labk = m$strlen(label)
else
   labk = 11
endif
label/display "{label(1:{labk})}" 40,5 o {mycol} {pipeline_aux(2)}
! 
write/keyw label "ARCFILE: "
if m$existd(p1,"ARCFILE") .eq. 1 then
   label(10:) = "{{p1},ARCFILE}"
   labk = m$strlen(label)
else
   labk = 10
endif
label/display "{label(1:{labk})}" 25,5 o {mycol} {pipeline_aux(2)}
