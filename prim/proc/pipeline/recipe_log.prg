! @(#)recipe_log.prg	19.1 (ESO-DMD) 02/25/03 14:10:54
! +++++++++++++++++++++++++++++++++++++
! 
! Midas procedure recipe_log.prg  for NTT/VLT pipeline
! K. Banse	961203, 970516, 980605, 990908, 020215
! 
! this procedure must be called in the beginning and end of each
! top level recipe (procedure) called from the Pipeline
! 
! +++++++++++++++++++++++++++++++++++++~
! 
define/param p1 + c		!name of calling procedure
define/param p2 IN c		!IN, OUT (at start, end of recipe)
define/param p3 normal c "Enter Normal or Super recipe:"
! 
recipe_time = m$time()
define/local dazunit/c/1/2 {mid$sess(11:12)}
! 
! set up before recipe execution
! 
if p2(1:1) .eq. "I" then
   if pipeline(6) .ne. 1 then
      close/file *			!close any outstanding ASCII file
      ! 
      recipe_secs(1) = m$secs()
      write/out "Recipe: {p1} started at:" {recipe_time(1:30)}
      pipeline(1) = -1			!set to BAD return
      pipeline(3) = 0		!init name counters for `set_of_frames.prg'
      pipeline(4) = 0
      ! 
      if pipeline(2) .eq. 1 then
         $pwd | write/keyw inputc		!get current directory
         inputi(1) = m$len(inputc)
         inputi(2) = m$indexb(inputc,"/tmp_{dazunit}_pipe") 
         if inputi(2) .gt. 0 then
            inputi(1) = inputi(1)-11		!index of /tmp... if at end
            if inputi(1) .eq. inputi(2) then	!we are already in /tmp_XY_pipe
               $rm -f *
               goto set_time_out
            endif
         endif
         ! 
         $sh $MID_PROC/pipeline/recipe.sh {dazunit}
         change/direc ./tmp_{dazunit}_pipe
      endif
! 
set_time_out:
      inputi = mid$mode(7)-1			!set MAXTIME 
      set/midas maxtime={pipeline(7)},{inputi}
   endif
   ! 
   if p3(1:1) .eq. "S" then		!check if super recipe
      pipeline(2) = 0
      pipeline(6) = 1
      ! 
      recquali = 0			!clear recipe quality status
      inputi = mid$mode(7)-1
      set/midas maxtime={pipeline(7)},{inputi}
   endif
   !
   progstat(6) = 0;			!clear last errors
! 
else 					!clean up after recipe execution
! 
   if p3(1:1) .eq. "S" then		!check if super recipe
      pipeline(2) = 1
      pipeline(6) = 0
   endif
   !
   if pipeline(6) .ne. 1 then
      if longlog(1:1) .ne. " " then		!we have a long logfile
         if m$exist(longlog) .ne. 0 then
            $cp {longlog} {base_name}.longlog
            longlog = "   "			!clear the keyword
         endif
      endif
      !
      recipe_secs(2) = m$secs()
      recipe_secs(3) = recipe_secs(2)-recipe_secs(1)
      set/format i1
      write/out -
      "Recipe: {p1} finished at:" {recipe_time(1:30)} ({recipe_secs(3)} sec)
      pipeline(1) = 0
      @d save_log RB				!move logfile of this RB
   endif
endif
