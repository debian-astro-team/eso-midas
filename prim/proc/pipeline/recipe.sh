#! /bin/sh
# @(#)recipe.sh	19.1 (ESO-IPG) 02/25/03 14:10:53
# 
# Bourne shell script  recipe.sh  for VLT/NTT pipeline
# K. Banse	961203
# 
# this script checks if a sub-directory ./tmp_{MidasUnit}_pipe exists
# if so, this subdir is deleted and created afresh
# 
# 
if [ -d ./tmp_$1_pipe ]; then
   rm -f ./tmp_$1_pipe/*
else
   mkdir ./tmp_$1_pipe
fi
# 
