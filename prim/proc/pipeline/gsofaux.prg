! @(#)gsofaux.prg	19.1 (ESO-DMD) 02/25/03 14:10:53 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure gsofaux.prg for NTT/VLT pipeline
! K. Banse	020930
!
! serve as sub-procedure for `getset_of_frames.prg'
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!
entry readin
! ----------+
! 
! read loop for input files
! 
define/param p1 ? c "Enter CONVERT/LIST option:"
define/local kaux/i/1/2 0,0
set/format i1
!
if p1(1:1) .eq. "C" then
   define/local cc/c/1/1 X
   define/local loop/i/1/2 0,0
   define/local ftype/c/1/4 ".bdf"

  read_loop:
   read/file {fc(1)} fname
   if fc(2) .lt. 0 return
   if fc(2) .eq. 0 goto read_loop
   !
   write/keyw classif " " all
   k(3) = m$index(fname," ")+1       !look for classification from DO
   if k(3) .gt. 1 .and. k(3) .le. fc(2) then
      classif = fname({k(3)}:)
      k(4) = 129-k(3)                !length of fname - k(3) + 1
      write/keyw fname/c/{k(3)}/{k(4)} " " all
   else
      classif = "No classification "
   endif
   !
   write/out reading frame: {fname}
   write/out with classification: {classif}
   kaux(2) = 0				!reset process flag
   !
   mid$info(4) = -1			!FITS extension counter
   if catflag .eq. 1 then		!for catalog use simple file names...
      indisk/mfits {fname} x__x ? no
      if mid$info(4) .lt. 1 then
         write/out Invalid FITS file {fname} - we stop!
         return/exit 
      endif
      kaux = mid$info(4)
      do loop = 1 kaux			!go through all extensions
         loop(2) = loop - 1
         cc = outputc({loop}:{loop})
         if cc .eq. "U" .or. cc .eq. "X" then
            write/out -
            "Unknown type in FITS extension # {loop} of {fname} - we skip!"
            goto move_on
         endif
         set/format i4
         in_a = "x__x{loop(2)}"
         set/format i1
         if cc .eq. "E" .or.  cc .eq. "I" then
            ftype = ".bdf"
         else
            ftype = ".tbl"
         endif
         write/descr {in_a}{ftype} DO_CLASSIFICATION/c/1/32 "{classif}"
         write/descr {in_a}{ftype} DO_FILENAME/c/1/128 {fname}
         $ mv {in_a}{ftype} rbf_{pipeline(4)}{ftype}
         add/icat {catname} rbf_{pipeline(4)}{ftype} >Null 
         pipeline(4) = pipeline(4)+1
      enddo
      !
   else
      write/keyw base_name {fname}
      k = m$indexb(base_name,"/")
      if k .gt. 0 then
         k = k + 1
         write/keyw base_name "{base_name({k}:)} "
      endif
      !
      k = m$indexb(base_name,".")            !look for FITS file type
      if k .le. 0 then
         write/out "invalid input FITS frame" {base_name} "missing type"...
         goto read_loop
      else
         write/keyw base_name/c/{k}/8 "        "
      endif
      ! 
      indisk/mfits {fname} x__x ? no
      if mid$info(4) .lt. 1 then
         write/out Invalid FITS file {fname} - we stop!
         return/exit
      endif
      kaux = mid$info(4)
      do loop = 1 kaux                  !go through all extensions
         loop(2) = loop - 1
         cc = outputc({loop}:{loop})
         if cc .eq. "U" .or. cc .eq. "X" then
            write/out -
            "Unknown type in FITS extension # {loop} of {fname} - we skip!"
            goto move_on
         endif
         set/format i4
         in_a = "x__x{loop(2)}"
         set/format i1
         if cc .eq. "E" .or.  cc .eq. "I" then
            ftype = ".bdf"
         else
            ftype = ".tbl"
         endif
         write/descr {in_a}{ftype} DO_CLASSIFICATION/c/1/32 "{classif}"
         write/descr {in_a}{ftype} DO_FILENAME/c/1/128 {fname}
         $ mv {in_a}{ftype} {base_name}
         add/icat {catname} {base_name} >Null
         pipeline(4) = pipeline(4)+1
      enddo
   endif
   !
  move_on:
   catflag(2) = catflag(2) + kaux               !increment file counter
   goto read_loop
   !
else                 ! here for just storing/listing all entries of SoF
   !
 readd_loop:
   read/file {fc(1)} fname
   if fc(2) .lt. 0 return
   if fc(2) .eq. 0 goto readd_loop
   !
   k(3) = m$index(fname," ")         !look for classification from DO
   if k(3) .gt. 0 .and. k(3) .lt. fc(2) then
   classif = fname({k(3)}:)//"    "
      write/keyw fname {fname}
   else
      classif = "No classification "
   endif
   !
   if catflag .eq. 1 then
      add/icat {catname} fname >Null
   endif
   write/out {fname} "   " {classif}
   !
   catflag(2) = catflag(2) + 1               !increment file counter
   goto readd_loop
endif
!
! 
entry descfile
! ----------+
! 
! prepare file for all the `write/descr' commands
! 
define/param p1 ? c "Enter name of output file with commands:"
set/format i1
! 
open/file {p1} WRITE fc
if fc(1) .lt. 0 then
   write/keyw mid$errmess -
         "Could not create auxiliary descriptor list `{p1}'..."
   return BAD
endif
k(1) = m$indexb(inn_a,"/")+1			!skip path specs
write/file {fc(1)} pipefile/c/1/64 {inn_a({k(1)}:)}
write/file {fc(1)} pipefile/h/1/40 "Filename of data product "
write/file {fc(1)} eso.pro.did/c/1/30 "{pipeline_vers(3)} "
write/file {fc(1)} eso.pro.did/h/1/40 "Data dictionary for PRO "
write/file {fc(1)} eso.pro.do.id/c/1/30 "{pipeline_vers(1)} "
write/file {fc(1)} eso.pro.do.id/h/1/40 "Data Organizer identification ID "
write/file {fc(1)} eso.pro.rbs.id/c/1/30 "{pipeline_vers(2)} "
write/file {fc(1)} eso.pro.rbs.id/h/1/50 -
              "Reduction Block Scheduler identification ID "
if m$existk("recipe_name") .eq. 1 then
  write/file {fc(1)} eso.pro.rec1.id/c/1/50 {recipe_name}
  write/file {fc(1)} eso.pro.rec1.id/h/1/40 -
              "Pipeline recipe (unique) identifier "
endif
write/file {fc(1)} eso.pro.rec1.drs.id/c/1/30 -
              "MIDAS/{mid$sess(16:20)}{mid$sess(21:25)}"
write/file {fc(1)} eso.pro.rec1.drs.id/h/1/50 -
              "Data Reduction System identifier "
write/file {fc(1)} eso.pro.rec1.pipe.id/c/1/30 "{pipeline_vers(4)} "
write/file {fc(1)} eso.pro.rec1.pipe.id/h/1/50 -
              "Pipeline (unique) identifier "
inputc = m$isodate()//"            "		!get date in ISO 8601 format
k(1) = m$indexb(rbname,"/")+1		!skip path specs
write/file {fc(1)} eso.pro.rec1.rb.id/c/1/50 {rbname({k(1)}:)}
write/file {fc(1)} eso.pro.rec1.rb.id/h/1/40 "Reduction Block identifier "
close/file {fc(1)}
! 
return OK
!
! 
entry resdisp
! ----------+
! 
! display major product
! 
set/format i1
! 
if pipeline(5) .eq. 2 then
   if ftype(2:2) .ne. "t" then
      inputi(1) = ididev(2)/2             !use NR's cuts selection
      inputi(2) = ididev(3)/2
      write/keyword nice_dscr/i/1/2 {inputi(1)},{inputi(2)}
      k(5) = outputi(1)                   !save because of `fors_nicedisp'
      @p fors_nicedisp {work} scan=0,0 vals=0.5,3.0 out=n
      load/image {work} ? {outputi(1)},avg c,c {outputr(16)},{outputr(17)}
      outputi(1) = k(5)
      !
      if recquali .ne. 0 then             !display warning from recipes
         wait/secs 10
         if ididev(2) .le. 512) then
            label/display "{recqualm}" 0.92,0.06 ov red -238
         else
            label/display "{recqualm}" 0.92,0.06 ov red -585
         endif
      endif
   endif
endif
! 

