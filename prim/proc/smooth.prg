! @(#)smooth.prg	19.1 (ESO-DMD) 02/25/03 14:09:02
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure smooth.prg to implement FILTER/SMOOTH, /MEDIAN,
!                                               /GAUSS, /DIGITAL
!                                               /MAX, /MIN
! K. Banse	901108, 920210, 921207, 940317, 950718
! 	
! use via FILTER/SMOOTH inframe outframe radx,rady,threshold [flags] [area]
!                       [options]
! or      FILTER/MEDIAN "same parameters as above..."
! threshold > 0.
! flags(1:1) = Y/N for inclusion of center point in calculations
! flags(2:2) = A/R/Q/F for absolute or relative threshold
! or
!         FILTER/DIGITAL inframe outframe weights_spec [area] [options]
!         FILTER/GAUSS inframe outframe radx,rady xmean,xsigma,ymean,ysigma
!                      [area] [filter_name] [options]
! 
! area = [...,...:...,...], table_name, CURSOR or + (for complete frame)
! options = (1:1) - L or N, (2:2) - D or N, (3:3) - E or N
! or
!         FILTER/MIN inframe outframe radx,rady [area] [options]
!         FILTER/MAX inframe outframe radx,rady [area] [options]
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter input frame: "
define/param p2 ? ima "Enter result frame: "
! 
write/keyw history "{mid$cmnd(1:6)}/{mid$cmnd(11:14)} "
!
if mid$cmnd(11:11) .eq. "S" then		!FILTER/SMOOTH
   action(1:1) = "A"
! 
elseif mid$cmnd(11:11) .eq. "M" then
   if mid$cmnd(12:12) .eq. "E" then		!FILTER/MEDIAN
      action(1:1) = "M"
! 
   else					      !here for FILTER/MIN or FILTER/MAX
      if mid$cmnd(12:12) .eq. "A" then		!maximum filter
         action(1:1) = "K"
      else
         action(1:1) = "L"			!minimum filter
      endif
      define/param p3 1,1 N "Enter radx,rady: "
      define/param p4 + ? "Enter area: "
      define/param p5 NNE ? "Enter options: "
      write/keyw inputi 3,0		!make sure, that 1-dim case works...
      write/keyw inputi {p3}
      write/keyw inputc/c/3/60 {p4(1:60)}
      write/keyw action/c/2/3 {p5}
      goto run_it
   endif
! 
else
   action(1:1) = mid$cmnd(11:11)
   if action(1:1) .eq. "G" then				!here for FILTER/GAUSS
      define/param p3 9,9 N "Enter radx,rady: "
      define/param p4 9,3,9,3 N "Enter xmean,xsigma,ymean,ysigma: "
      define/param p5 + ? "Enter area: "
      define/param p6 + ima "Enter name of filter frame: "
      define/param p7 NNE ? "Enter options: "
      write/keyw inputi 3,0		!make sure, that 1-dim case works...
      write/keyw inputi {p3}
      write/keyw inputr 9,3,9,3
      write/keyw inputr {p4}
      write/keyw inputc/c/3/60 {p5(1:60)}
      write/keyw in_b {p6}
      write/keyw action/c/2/3 {p7}
! 
   elseif action(1:1) .eq. "D" then		!here for FILTER/DIGITAL
      define/param p3 POINT ? ?
      define/param p4 + ? "Enter area: "
      define/param p5 NNE ? "Enter options: "
      write/keyw in_b ++			!init IN_B to "no filter name"
      write/keyw inputc/c/3/60 {p4(1:60)}
      define/local ll/i/1/1 0
      ll = m$index(p3,":")			!check for 1-dim kernel
      if ll .gt. 0 then
         inputc(63:) = p3({ll}:)
         p3({ll}:>) = " "
      else
         inputc(63:>) = " "
      endif
! 
      ll = m$tstno(p3)				!check, if P3 holds numbers...
      if ll .eq. 1 then
         write/keyw inputr {p3}			!really numbers entered
      else
         write/keyw in_b {p3}{inputc(63:)}	!image name entered
      endif
      write/keyw action/c/2/3 {p5}
   else						!here for FILTER/DIGITAL
      write/error 3				!invalid qualifier
   endif
   goto run_it
endif
! 
!  here for FILTER/SMOOTH or FILTER/MEDIAN
! 
define/param p3 1,1,0. ? "Enter radx,rady,threshold: "
define/param p4 ya ? "Enter inclusion+threshold flag: "
define/param p5 + ? "Enter area: "
define/param p6 NNE ? "Enter options: "
define/maxpar 6
!
write/keyw inputr 1,1,0,0
if p4(2:2) .ne. "F" .and. p4(2:2) .ne. "Z" then
   write/keyw inputr {p3}
else
   write/keyw inputr/r/1/2 {p3}		!write only the radii
endif
! 
write/keyw inputc {p4}
write/keyw inputc/c/3/60 {p5(1:60)}
action(2:4) = p6(1:3)
!
! here we finally do it !!!
! 
RUN_IT:
write/keyw in_a {p1}
write/keyw out_a {p2}
run MID_EXE:SMOOTH
