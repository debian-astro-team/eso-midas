! @(#)tutfilt.prg	19.1 (ES0-DMD) 02/25/03 14:09:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! procedure tutfilt.prg  to implement TUTORIAL/FILTER
! K. Banse	880915
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 NOAUTO ? "Enter Auto/NoAuto"
!
set/overlay
!
! make sure, we have an image loaded ...
@ testima centaur {p1}
!
define/local lc/c/1/60 {in_a}
! 
write/out "We now show the effect of different digital filters:"
write/out filter/digit {lc} &x laplace
filter/digit {lc} &x laplace
load/image &x 
write/out filter/digit {lc} &y low_pass
filter/digit {lc} &y low_pass
load/image &y 
write/out filter/digit {lc} &z point
filter/digit {lc} &z point
load/image &z 
write/out
write/out Now let us smooth the image.
write/out filter/smooth {lc} &w 1,1,0.0 Y
filter/smooth {lc} &w 1,1,0.0 Y
load/image &w 
write/out
write/out Finally we use a Gaussian filter on the image.
write/out filter/gauss {lc} &v
filter/gauss {lc} &v
load/image &v
if p1(1:4) .eq. "AUTO" then
  -DELETE middumm?.bdf
  -DELETE testima.bdf
endif
write/out "End of TUTORIAL/FILTER"

