! @(#)printsys.prg	19.1 (ESO-DMD) 02/25/03 14:08:59
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  printsys.prg  to implement PRINT/LOG, PRINT/HELP
! K. Banse	910104, 910621, 920207
!
! executed via @ printsys string
! 
! for PRINT/LOG: string = page_def
! with page_def = pstart,pend		from pstart to pend (incl)
!                 page_no		only that page
!                 LAST			only last page
!                 -no  			last 'no' pages
!
! for PRINT/HELP: string = command or command/qualif
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
if mid$cmnd(11:11) .eq. "L" then		!PRINT/LOG
   define/param p1 1,9999 N "Enter pages to print: "
   write/keyw inputi/i/15/2 {p1},{p1}
   define/local oldlog/i/1/1 {log(1)}
   log/off				!turn logging off
! 
   run MID_MONIT:LOGGER
! 
   if oldlog .ne. 0 log/on
   if mid$prnt(1:1) .eq. "T" return
   if mid$prnt(1:1) .eq. "F" return
! 
   @ print.prg print 			!send file to chosen printer
! 
else					!PRINT/HELP
   log(3) = 1
   @ print.prg assign
   help {p1}
   log(3) = 0
   @ print.prg out 			!send file to chosen printer
endif
