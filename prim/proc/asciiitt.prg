! @(#)asciiitt.prg	19.1 (ESO-DMD) 02/25/03 14:08:43
! ++++++++++++++
!  
!  MIDAS procedure asciiitt.prg
!  to convert all ascii ITT files into MIDAS tables
!  K. Banse     ESO - Garching   910724, 920318, 930706, 940113, 980928
! 
! ++++++++++++++
! 
! handle in VMS or UNIX
! 
write/out "Copying all ascii ITTs to current MIDAS default directory ..."
! 
if aux_mode(1) .le. 1 then		! VMS
   $ DELETE/NOCONF *.iasc.*,itt.lis.*
   $ COPY {ascdir}*.iasc {defdir}
else					! UNIX
   $ touch itt.lis
   $ rm -f *.iasc itt.lis
   $ cp {ascdir}*.iasc {defdir}
endif
! 
@ ascitt equa
@ ascitt expo
@ ascitt gamma
@ ascitt jigsaw
@ ascitt lasritt
@ ascitt log
@ ascitt neg
@ ascitt neglog
-rename null.iasc mnull.iasc		!table commands don't like name `null'
@ ascitt mnull
-rename mnull.itt null.itt
@ ascitt ramp
@ ascitt stairs
! 
write/out Copying all binary ITTs to relevant MIDAS directory ...
! 
if aux_mode(1) .le. 1 then		! VMS
   $ SET PROT=W:RWE *.itt
   $ COPY *.itt {bindir}
   $ DELETE/NOCONF *.iasc.*,*.itt.*,itt.lis.*
else					! UNIX
   $ chmod 666 *.itt *.iasc 
   $ mv  *.itt {bindir}.
   $ rm -f *.iasc itt.lis
endif
