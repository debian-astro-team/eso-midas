! @(#)t2comphis.prg	19.1 (ES0-DMD) 02/25/03 14:09:03
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure t2comphis.prg
! .PURPOSE
!	COMPUTE/HIST
!
!  901108  KB correct WRITE/KEYW HISTORY line
! ----------------------------------------------------------------------
DEFINE/PARAM P1 ? IMAG  "Enter image or table :"
DEFINE/PARAM P2 ? CHAR  "Separator (=):"
DEFINE/PARAM P3 ? IMAG  "Enter image or table :"
!
WRITE/KEYW HISTORY "{MID$CMND(1:6)}/{MID$CMND(11:14)} "
!
WRITE/KEYW MID$CMND/C/1/4 HIST
RUN MID_EXE:topertbl
