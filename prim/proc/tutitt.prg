! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! procedure tutitt.prg to implement TUTORIAL/ITT
! K. Banse	871207, 910425, 030707
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 NOPLOT ? "Enter plotflag Plot/NoPlot/Auto"
define/local option/c/1/6 "PLOT"
!
if p1(1:3) .eq. "NOP" then
  write/keyw option/c/1/6 "NOPLOT"
endif
!
@ testima sombrero {p1}
!
clear/itt               !avoid any crazy ITT on Display
!
write/out
write/out We now load the LUT heat.lut + display it:
write/out load/lut heat
load/lut heat
!
write/out
write/out "And show the effect of the different ITTs via ittexa.prg:"
write/out @ ittexa
@ ittexa {option}
!
write/out
write/out Histogram equalisation is also obtained via an ITT.
write/out For that we first need the image histogram using 256 bins, so
write/out
write/out statistics/image * ? #256
statistics/image * ? #256
write/out
write/out and now the real thing...
write/out
write/out equalize/histogram {in_a}
equalize/histogram {in_a}
load/image {idimemc}
!
write/out
write/out To disable the mapping via an ITT, use
write/out
write/out clear/itt
clear/itt
!
write/out
write/out And enable again...
write/out
write/out set/itt
set/itt
write/out
if p1(1:4) .eq. "AUTO" then
  -delete testima.bdf
  -delete graph_wnd000.plt
endif
write/out "End of TUTORIAL/ITT"
