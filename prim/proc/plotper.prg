! @(#)plotper.prg	19.1 (ESO-DMD) 02/25/03 14:08:57
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLOTPER.PRG
!.PURPOSE:        MIDAS procedure to produce a prespective plot
!.USE:            PLOT/PER P1 [P2] [P3] [P4] [P5] where:
!                          P1: image is the image to be plotted
!                          P2: coord_string is the area
!                          P3: the view angles (default -45.0, 30.0)
!                          P4: scale and offset parameter (1/DATAMAX,0.0)
!                          P5: smooth_par is the parameter which represents 
!                              the size of smoothing box
!                          P6: XY for perspective lines in x and y; X or Y
!                              only in one dimension
!.AUTHOR:        R.H. Warmels, ESO - Garching 860421
!.VERSION: 870529 RHW
!          880922, 900409, 900712, 920413  KB
!          900718 RHW Scale and offset parameter included
!.VERSION: 920224 RHW Offset in x and y included
!----------------------------------------------------------------------------
! 
DEFINE/PARAM P1 *         IMA
define/local cmnd/c/1/20 "{mid$cmnd(1:20)}"		!save MID$CMND
!
IF PLRSTAT(1) .EQ. 0.0 .AND. PLRSTAT(2) .EQ. 0.0 THEN
   IF PLRSTAT(5) .EQ. 0.0 .AND. PLRSTAT(6) .EQ. 0.0 THEN
      DEFINE/PARAM P2 [<,<:>,>] C
      IF P2(1:1) .EQ. "M" THEN
         WRITE/OUT "*** FATAL: axes not properly set; use SET/GRAPHICS"
         RETURN
      ENDIF
   ELSE
      DEFINE/PARAM P2 MANUAL   C
   ENDIF
ELSE
   DEFINE/PARAM P2 MANUAL   C
ENDIF
!
DEFINE/PARAM P3 45.0,30.0  NUM                      !default view angles
DEFINE/PARAM P4 1.0,0.0    NUM                      !vert. scaling and offset
DEFINE/PARAM P5 0          NUM                      !smoothing parameter
DEFINE/PARAM P6 XY         C                        !graph in both x and y
!
!if mid$sess(7:7) .eq. " " .or. mid$sess(7:7) .eq. "-" then
!   create/graphics
!   write/keyw  mid$cmnd/c/1/20 "{cmnd(1:20)}"
!endif
! 
IF P2(1:1) .EQ. "C" THEN
   set/curs ? rectangle                  ! set cursor to rectangle
   IF P1 .NE. "?" LOAD/IMAGE {P1}
   GET/CURSOR ? ? NN 1,2		          ! use cursor rectangle once
   write/keyw  mid$cmnd/c/1/20 "{cmnd(1:20)}"
ENDIF
!
WRITE/KEYW IN_A           {P1}
WRITE/KEYW IN_B           {P2}
WRITE/KEYW INPUTR/R/1/2   {P3}
WRITE/KEYW INPUTR/R/3/2   {P4}
WRITE/KEYW INPUTI/I/1/1   {P5}
WRITE/KEYW INPUTC/C/1/2   {P6}
WRITE/KEYW PLRGRAP/R/13/2  0.0,0.0
WRITE/KEYW PLRGRAP/R/29/2 -999,-999
DATTIM = M$TIME()
!
RUN MID_EXE:PLOTPER                               ! make the bloddy plot
WRITE/KEYW PLCDATA/C/1/60  {P1}                    ! name, t of data structure
WRITE/KEYW PLCDATA/C/61/20 "FRAME        "
copy/graph

