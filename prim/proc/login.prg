! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  general login procedure for Portable MIDAS 
!  to be run each time at start of interactive monitor for MIDAS
!  K. Banse     ESO - Garching
!  910428       creation
!  100316       last modif
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! ECHO/full ALL
!
assign/defaults					!assign all defaults
if aux_mode .eq. 1 then			!in VMS set up SYS$ERROR as Null device
   $ASSIGN _NL: SYS$ERROR        	!to avoid double DCL error messages
endif
!
inputc = m$symbol("DFO_MODE")		! just for DFO
if inputc(1:1) .eq. "Y" DFO_MODE/ON	! just for DFO
!
if mid$mode(9) .eq. -1 then		!inmidas     (activities at start up)
   if mid$sess(1:4) .ne. "NULL" then	!if session at an ImageDisplay
      @ dazlogin login			!fill display relevant keywords
      if mid$mode(5) .eq. 0 -TYPE MID_MONIT:xnews.txt	! = 1 for -noh option
   endif
endif
!
!  execute personal login procedure 
! 
error(3) = 79; error(4) = 0		!do not display error, if no login.prg
@@ login	
error(4) = 1
if progstat .eq. 79 progstat = 0
! 
if mid$mode(9) .eq. -1 then
   if mid$mode(3) .eq. 1 .and. p1(1:1) .ne. "?" then
      @@ {p1}		!job mode: test for parameters + execute midjobXY.prg
   endif				
else					!here for gomidas
   read/comm 				!get last command buffer back in 
endif
! 
mid$mode(9) = mid$mode(9)+1		! = 0 after inmidas, > 0 after gomidas
! 
! if progstat .ne. 0, next command will stop this procedure ...
! 
error(3) = 0				!reset to stop on errors

