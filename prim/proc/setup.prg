! @(#)setup.prg	19.1 (ES0-DMD) 02/25/03 14:09:01
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS system procedure setup.prg
!  to be run when the command ".../SETUP xxx" is given
!  K. Banse	901206, 921218, 940127
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
action(1:4) = mid$cmnd(1:4)				!save command
!
define/param p1 + C "Enter name of SetUp: "
if p1 .eq. "+" then
   if action(1:3) .EQ. "INF" THEN
      write/out The following setups are currently supported:    
      write/out    
      write/out catalog "                "  to setup values for EXECUTE/CATALOG
      write/out    
   else
      write/error 5
   endif
   return
endif
!
define/local sp1/c/1/80 " " all
sp1 = m$lower(p1)			!make sure, that it's lower case
define/local file/c/1/80 MID_SETUP:{sp1}.prg
!
if m$exist(file) .eq. 1 then
   @@ MID_SETUP:{sp1},{action(1:4)} {p2} {p3} {p4} {p5} {p6} {p7}    !try there
else
   @@ {sp1},{action(1:4)} {p2} {p3} {p4} {p5} {p6} {p7}	      !try in MID_WORK:
endif
