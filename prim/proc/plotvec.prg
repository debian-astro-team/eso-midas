! @(#)plotvec.prg	19.2 (ESO-DMD) 06/05/03 11:32:58
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLOTVEC.PRG
!.PURPOSE: MIDAS procedure to produce a contour map of a two-dim. image
!          execute by
!          PLOT/VEC P1 P2 P3 P4 P5 where:
!          P1: image containing the intensity information
!          P2: image containing the pos. angle information
!          P3: coord_string is the area
!          P4: scal_x,scal_y,off_x,off_y
!          P5: scale of the vector lenght
!          P6: intensity interval, pos. angle interval
!          P7: smooth_par is parameter which represents size of smoothing box
!          P8: head
!.AUTHOR:  R.H. Warmels, ESO-Garching
!.VERSION: 900812 RHW Implementation
!.VERSION: 920224 RHW Offset in x and y included
!.VERSION: 920413 KB  What the hell was modified ???
!.VERSION: 920413 RvH removed bugs with P2 where P3 should be used
!----------------------------------------------------------------------------
! 
DEFINE/PARAM P1 * IMA
DEFINE/PARAM P2 * IMA
define/local cmnd/c/1/20 "{mid$cmnd(1:20)}"            !save MID$CMND
!
IF PLRSTAT(1) .EQ. 0.0 .AND. PLRSTAT(2) .EQ. 0.0 THEN
   IF PLRSTAT(5) .EQ. 0.0 .AND. PLRSTAT(6) .EQ. 0.0 THEN
      DEFINE/PARAM P3 [<,<:>,>] C
      IF P3(1:1) .EQ. "M" THEN
         WRITE/OUT "*** FATAL: axes not properly set; use SET/GRAPHICS"
         RETURN
      ENDIF
   ELSE
      DEFINE/PARAM P3 MANUAL   C
   ENDIF
ELSE
   DEFINE/PARAM P3 MANUAL   C
ENDIF
!
IF P3 .EQ. "C" THEN
   IF P1 .NE. "?" LOAD/IMAGE {P1}
   GET/CURSOR ? ? NN 1,2		         ! use cursor rectangle once
   write/keyw  mid$cmnd/c/1/20 "{cmnd(1:20)}"
ENDIF
!
WRITE/KEYW IN_A   {P1}
WRITE/KEYW IN_B   {P2}
WRITE/KEYW INPUTC {P3}
! 
IF MID$CMND(1:1) .EQ. "P" THEN
   DEFINE/PARAM P4 0.,0.,-999,-999 NUM
   @ plscoff.prg_o {P4}
   DEFINE/PARAM P5 0.0 N
   WRITE/KEYW INPUTR/R/1/1   {P5}
   DEFINE/PARAM P6 0,0,0,360 N
   WRITE/KEYW INPUTR/R/2/4   {P6}
   DEFINE/PARAM P7 0 N
   WRITE/KEYW INPUTI/I/1/1   {P7}
   DEFINE/PARAM P8 1 N
   WRITE/KEYW INPUTI/I/2/1   {P8}
   DATTIM = M$TIME()
ELSE
   DEFINE/PARAM P4 0.0 N
   WRITE/KEYW INPUTR/R/1/1   {P4}
   DEFINE/PARAM P5 0,0,0,360 N
   WRITE/KEYW INPUTR/R/2/4   {P5}
   DEFINE/PARAM P6 0 N
   WRITE/KEYW INPUTI/I/1/1   {P6}
   DEFINE/PARAM P7 1 N
   WRITE/KEYW INPUTI/I/2/1   {P7}
ENDIF
!
RUN MID_EXE:PLOTVEC                              ! make the bloddy plot
WRITE/KEYW PLCDATA/C/1/60  {P1}
WRITE/KEYW PLCDATA/C/61/20 "FRAME       "
copy/graph
 
