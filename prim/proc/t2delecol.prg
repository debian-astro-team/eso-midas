! @(#)t2delecol.prg	19.1 (ES0-DMD) 02/25/03 14:09:05
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2delecol.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!  901108  KB  fix WRITE/KEYW HISTORY stuff ...
!
! .PURPOSE
!
!    implements
!
!  DELETE/COLUMN    table-name  column [column...] 
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE  "Enter table:"
DEFINE/PARAM P2 ? CHAR   "Enter column:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:tdatatbl
