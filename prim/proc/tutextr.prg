! @(#)tutextr.prg	19.1 (ES0-DMD) 02/25/03 14:09:09
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! procedure tutextr.prg  to implement TUTORIAL/EXTRACT
! K. Banse	910425, 920413, 940517
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 NOAUTO ? "NOAUTO/AUTO"
!
clear/display
set/over
!
! make sure, we have an image loaded ...
@ testima sombrero {p1}
!
write/out "Let us use the cursor(s) to extract a subframe from the displayed image."
write/out First we use EXTRACT/CURSOR subfr1
write/out    
if ididev(18) .ne. 11 then
   write/out "There will appear a cursor rectangle on the display. To move the complete"
   write/out "rectangle leave both cursors on and move the joystick. To change the size"
   write/out "of the rectangle set one cursor off."
   write/out "Once you got the desired rectangle, press ENTER."
   write/out "Note, that TRACK has to be off and RATE on...!"
else
  write/out "Put focus on the Midas display window (e.g. put the mouse in it)."
  write/out "Use the arrow keys to modify the size of the rectangle."
  write/out "Use the mouse to move the center of the rectangle."
  write/out "Once you got the desired rectangle, press the left mouse button."
endif
! 
extract/cursor subfr1
write/out
write/out "And let us look at the extracted subimage..."
write/out
load/image subfr1
wait/secs 2.0				!wait for 2 secs
!
write/out "We now use the command EXTRACT/TRACE to get a 1-dimensional slice of the"
write/out "displayed (2-dimensional) image:"
write/out    
write/out "A line passing through the two cursors will appear on the display."
if ididev(18) .ne. 11 then
   write/out "With both cursors on you can move the line as it is around."
   write/out "Setting one cursor off, you can rotate the line around the" -
    "fixed cursor point"
   write/out "by moving the other cursor. To terminate, set both cursors off."
   write/out "Note, that TRACK has to be off and RATE on...!"
else
   write/out "Use the mouse to move cursor 0 and the arrow keys of the keyboard"
   write/out "to move cursor 1, to define a trace across the currently displayed"
   write/out "image. The trace is actually drawn only after pushing the Enter"
   write/out "button (left button on the mouse).
   write/out "To extract the chosen line and exit, press the EXIT button (the"
   write/out "button to the right of the ENTER button)."
endif
write/out 
write/out "EXTRACT/TRACE ? tracy"
extract/trace ? tracy
! 
write/out "And let us look at the extracted line..."
write/out
!
write/out "PLOT/ROW tracy"
plot/row tracy
IF P1(1:4) .EQ. "AUTO" THEN
  -DELETE testima.bdf
  -DELETE graph_wnd000.plt
ENDIF
WRITE/OUT "End of TUTORIAL/EXTRACT"
