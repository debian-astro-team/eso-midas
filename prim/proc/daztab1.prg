! ++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure daztab1.prg to implement GET/LUT, ITT
! K. Banse	901203, 920524, 930324, 941109, 950105, 071005
!
! execute via GET/LUT out_specs sect,noval ittflag format range
! out_specs = table (name of result table) or
!             image,descr (name of result descriptor)
! sect,noval = section no., no_of_values (default = 0,256)
! ittflag = if set to ITT, the LUT values are mapped via the current ITT
! format = TABLE or ASCII, defaulted to TABLE
! range = 0,1 or 0,255 to indicate range of data (only used for format = ASCII)
! 
!         and GET/ITT out_specs 
! out_specs = table (name of result table) or
!             image,descr (name of result descriptor)
!
! +++++++++++++++++++++++++++++++++++++++++++++++
!
if ididev(18) .eq. 11 then
   if dazdevr(4) .ne. 1 then                     !only possible with own LUT
      write/out this command only possible with own LUT ...
      return
   endif
endif
! set up ACTION
write/keyw action/c/1/4 DA{mid$cmnd(1:1)}{mid$cmnd(11:11)}   
!
define/param p1 ? C "Enter out_specs - table or image,descr: "
write/keyw in_a {p1}
!
if mid$cmnd(11:11) .eq. "L" then
   define/param p2 {dazhold(10)},256 N
   define/param p3 + C "Enter ITT or NoITT: "
   define/param p4 table C "Enter TABLE or ASCII format: "
   define/param p5 0,1 C "Enter range = 0,1 or 0,255: "
   write/keyw dazin {p2},256		!in case just the `section '
   if p4(1:2) .eq. "AS" then
      write/keyw dazin/i/3/3 9,{p5}
   else
      write/keyw dazin/i/3/3 0,{p5}
   endif
   dazhold(10) = dazin(1)		!save sect also in DAZHOLD(10)
else
   write/keyw dazin 99			!store section
endif
!
run MID_EXE:IDAUXX
 
