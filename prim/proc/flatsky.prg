! @(#)flatsky.prg	19.1 (ES0-DMD) 02/25/03 14:08:51
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  flatsky.prg  to implement FIT/FLAT_SKY
! K. Banse	901108, 910207, 920331, 940414
!
! execute via FIT/FLAT_SKY outframe = image in_spec order surface_frame
! or          FIT/FLAT_SKY image in_spec order surface_frame
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local ord/i/1/2 1,1
!
if p2 .eq. "=" then
   define/para p1 ? ima "Enter outframe:"
   define/para p3 ? ima "Enter image name:"
   define/para p4 CURSOR
   define/para p5 1,1
   define/para p6 midtemp
!
   define/local out/c/1/60 {p1}
   define/local in/c/1/60 {p3}
   define/local spec/c/1/60 {p4}
   write/keyw ord/i/1/2 {p5}
   define/local surf/c/1/60 {p6}
!
else
   define/para p1 ? ima "Enter image name:"
   define/para p2 CURSOR
   define/para p3 1,1
   define/para p4 midtemp
!
   define/local out/c/1/60 ?
   define/local in/c/1/60 {p1}
   define/local spec/c/1/60 {p2}
   write/keyw ord/i/1/2 {p3}
   define/local surf/c/1/60 {p4}
endif
!
write/keyw history "FIT/FLAT_SKY "
if spec .eq. "CURSOR" then
   average/kappa CURSOR midtempa,:MEDIAN C
   write/keyw spec midtempa
else
   average/kappa {in},{spec} :MEDIAN C
endif
!
!  find background surface
convert/table {surf} = {spec} :XCEN,:YCEN :MEDIAN {in} POLY {ord(1)},{ord(2)}
!
! get cuts of reference frame
copy/dd {in} lhcuts/r/1/2 {surf} lhcuts/r/1/2
!
!  if so desired, subtract background from input frame
if out(1:1) .ne. "?" compute/image {out} = {in}-{surf}
