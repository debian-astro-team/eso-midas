! @(#)tutmod.prg	19.1 (ESO-DMD) 02/25/03 14:09:11
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! demo of MODIFY/PIX + MODIFY/COL
! KB 890417, 990804
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 yes C "Enter yes, if NAG is available, else no: "
!
write/out "Copying test data (a CCD frame) ..."
indisk/fits MID_TEST:ccd.fits middumma.bdf >Null
! 
write/out "We load the image + use the LUT rainbow"
clear/chan over
clear/chan 
load/image &a
load/lut rainbow
! 
if p1(1:1) .eq. "y" then
   write/out "Now we use MODIFY/PIXEL to clean up CCD defects"
   modify/pixel cursor &b 
   load/image &b
endif
! 
write/out "Now we show how MODIFY/AREA works"
modify/area cursor &b 
! 
write/out "We load the modified frame (will look the same)"
load/image &b
! 
write/out "And use MODIFY/COLUMN to get rid of a bad column"
modify/column middummb middummb v @53
load/image &b
