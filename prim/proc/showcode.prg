! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! Midas procedure  showcode.prg  to display the related procedure code for
!                                a Midas command
! K. Banse	910514, 920401, 050119
!
! use as @ showcode comstr flag
! where         comstr = command/qualif
!               flag = o/t for original or translated code
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 load/image c "Enter command: "
define/param p2 o c "Enter flag = original/translated: "
! 
define/local mm/i/1/1 4
show/comm {p1} 			!get procedure name in key OUTPUTC
write/out 			!one blank line
! 
!  handle @ procedure
! 
write/keyw inputc " " all
! 
if outputc(1:2) .eq. "@ " then
   mm = 3
   define/local direc/c/1/12 MID_PROC:
!
!  handle @% procedure
!
elseif outputc(1:3) .eq. "@% " then
   define/local direc/c/1/12 MID_PROC:
! 
!  handle @a procedure
! 
elseif outputc(1:3) .eq. "@a " then
   define/local direc/c/1/12 APP_PROC:
! 
!  handle @s procedure
! 
elseif outputc(1:3) .eq. "@s " then
   define/local direc/c/1/12 STD_PROC:
! 
! 
!  handle @c procedure
! 
elseif outputc(1:3) .eq. "@c " then
   define/local direc/c/1/12 CON_PROC:
! 
! 
!  handle @@ procedure
! 
elseif outputc(1:3) .eq. "@@ " then
   define/local direc/c/1/1 " "
! 
! 
!  handle Midas basic commands
! 
elseif outputc(1:3) .eq. "pri" then
   return				!primitive command
! 
else
   write/out "No Midas procedure found for this command... "
   return
endif
! 
! now, copy procedure name
! 
write/keyw inputc {outputc({mm}:)}
! 
if p2(1:1) .eq. "T" then
   translate/show {direc}{inputc}
   return
endif
! 
define/local fc/i/1/2 0,0
define/local fname/c/1/120 " " all
! 
write/keyw fname {direc}{inputc}
open/file {fname} READ fc
if fc(1) .lt. 0 then
   open/file {fname}.prg READ fc
   if fc(1) .lt. 0 then
      write/out Could not open {fname} ...
      return/exit
   endif
   write/keyw fname {fname}.prg 
endif
close/file {fc(1)}
! 
write/_out {fname}			!display whole ASCII file
 
