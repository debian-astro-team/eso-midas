! @(#)info.prg	19.1 (ES0-DMD) 02/25/03 14:08:52
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure info.prg to implement the command HELP/SUBJECT
! K. Banse      870127, 911206, 921217
! 
! execute via  @ info subject
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 + ? "Enter subject: "
if p1(1:1) .eq. "+" p1(1:1) = "?"
! 
write/keyw in_a "H/WHAT {p1}"
run MID_MONIT:helper
