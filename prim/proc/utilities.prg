! +++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure utilities.prg to implement CONVERT/DESCR
! K. Banse	010220, 040426
!
! use via CONVERT/DESCR image  table  direction 
!             dscroot[,first[,last]]  dscsub1,dscsub2,...  del_flag
! where    direction = ds-t (descriptors to table)
! 		  or   t-ds
!          del_flag = 0/1 for NO/YES delete descriptors
!
! and  MAP/IMAGE in_image map_image result_image lo-,hi-cuts(input) scale_flag
! 
! +++++++++++++++++++++++++++++++++++++++++++++++
!
entry convdsc
define/param p1 ? IMA "Enter image:"
define/param p2 ? IMA "Enter table:"
define/param p3 ds-t c "Enter from-to:"
define/param p4 + c "Enter descr_root:"
define/param p5 + c "Enter descr_fields separated by comma:"
define/param p6 0 n "Enter delete flag (0 or 1, for NO or YES):"
!
write/keyw in_a {p1}
write/keyw out_a {p2}
write/keyw inputi {p6}
! 
action(1:3) = "CON"
write/keyw history "CONVERT/DESCR"
! 
run MID_EXE:GENXY1

entry map
define/param p1 ? IMA "Enter input image:"
define/param p2 ? IMA "Enter map image:":"
define/param p3 ? IMA "Enter result image:":"
define/param p4 + c "Enter low,hi map limits (applied to input):"
define/param p5 scale c "Enter SCALE/NOSCALE for scaling (not scaling):"
!
write/keyw in_a {p1}
write/keyw in_b {p2}
write/keyw out_a {p3}
if p4(1:1) .eq. "+" then
   write/keyw inputr 0,0
else
   write/keyw inputr {p4}
endif
! 
action(1:3) = "MAP"
write/keyw history "MAP/IMAGE "
! 
run MID_EXE:GENXY1

