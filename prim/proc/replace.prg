! @(#)replace.prg	19.1 (ES0-DMD) 02/25/03 14:09:00
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  replace.prg  to implement REPLACE/IMAGE, /POLYGON
! K. Banse	901023, 920109, 920401
!
! use via REPLACE/IMAGE inframe outframe test/lo,hi=repla
!         inframe = input image
!         outframe = output image
!         test = test image 
!         low,hi = real intensity interval for replacement
!         repla = constant or
!                 second input image or
!                 second input image 'op' constant
!
!         REPLACE/POLYGON in,intable out test/lo,hi=repla
!         in = input image 
!         intable = optional table with subimages of 'in'
!         test = test image 
!         out = output image
!         low,hi = real low + high threshold for polygon detection
!         repla = constant or second input image
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref in out test
! 
define/param p2 ? IMA "Enter output image: "
! 
if mid$cmnd(11:11) .eq. "I" then
   !
   ! REPLACE/IMAGE
   !
   define/param p1 ? IMA "Enter input image: "
   define/param p3 ? C "Enter replacement specs: "
   write/keyw in_a {p1}
   if p2(1:1) .eq. "+" then
      write/keyw out_a {p1}			!output = input frame
   else
      write/keyw out_a {p2}
   endif
   write/keyw history "REPLACE/IMAGE "
   define/maxpar 3
   run MID_EXE:REPLA
else
   !
   ! REPLACE/POLYGON
   !
   define/local itab/i/1/1 0			!look for input table
   itab = m$index(p1,",")
   if itab .gt. 1 then
      itab = itab-1
      write/keyw in_a {p1(1:{itab})}
      itab = itab+2
      write/keyw inputc {p1({itab}:>)}
   else
      write/keyw in_a {p1}
      write/keyw inputc +
   endif
   write/keyw out_a {p2}
   !
   itab = m$index(p3,"/")
   if itab .gt. 0 then
      itab = itab-1
      write/keyw in_b {p3(1:{itab})}
      itab = itab+2
      write/keyw p4 {p3({itab}:>)}		!fill P4 correctly
   else
      write/keyw in_b {in_a}			!test frame = inframe
      write/keyw p4 {p3}
   endif
   !
   action(1:2) = "PO"
   write/keyw history "REPLACE/POLYGON "
   define/maxpar 3
   run MID_EXE:GENZZ1
endif
