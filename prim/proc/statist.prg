! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure statist.prg  to implement  STATISTICS/IMAGE
! K. Banse	900409, 920708, 940429, 950724, 981014, 080725
!
! execute via 
! STATISTICS/IMAGE image sub_area binsize low,high_excess option 
!		   out_table plotlfg format
!
!  where 
!     image = input image
!     sub_area = + for full image, default
!		 [xa,ya:xe,ye] for subframe
!		 [`xsize'x`ysize'] for subframes/intervals covering whole image
! 	 	 CURSOR[,max_no]  for cursor defined sub-images
! 	 	 ROW for processing the input image along rows
! 	 	 COLUMN for processing the input image along columns
! 	 	 PLANE for processing the input image in planes
! 	 	 name for input table with subarea specifications
!     binsize = size of bins or #no. of bins, defaulted to 256.
!     low,high-excess = lowcut, highcut values, defaulted to 0.,0.
! 			which implies physical min,max
!     option = 4 character flag,
!              1: G,V,W,F(ull), R(educed), S(hort), H(istogram), M(inmax)
!              2: F(ull), S(hort) or N(o) display
!              3. char.: Y or N for drawing rectangles around subframe(s)
!	       4: M(erge), N(oMerge) of subframes
!     out_table = optional output table for ROW, COLUMN, CURSOR and table input
!     plotflg = PLOT or NOPLOT for histogram
!     format = format string which is used for displaying the minimum, maximum,
!	       mean and standard deviation of the file
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref FRAME AREA BINSIZE EXCESS OPTION OUTTAB PLOT FORMAT
!
!
define/param p1 * IMA "Enter input image: " 	!default to displayed image
define/param p2 + C "Enter sub_area: "
define/param p3 #256 C -
   "Enter binsize, a number, or no. of bins, preceded by #: "
define/param p4 0.,0. N -
   "Enter low,high excess if high .LE. low, min,max are used: "
define/param p5 GFYN C  "Enter option_chars, G/F/R/S/H/M and F/S/X/N, e.g. RS: "
define/param p6 + TBL "Enter output table: "
define/param p7 NOPLOT ?  "Enter histogram plot_flag PLOT/NOPLOT: "
define/param p8 E15.6 ? "Enter format string used for min,max,mean,stddev:"
! 
write/keyw in_a {p1}			!store translated frame
if p2(1:1) .eq. "+" then
   default(1:1) = "Y"			!use complete frame
else
   if p5(4:4) .eq. "M" then
      default(1:1) = "M"		!use + merge subframes from P2
   else
      default(1:1) = "N"		!use subframe(s) given in P2
   endif
endif
!
default(3:5) = "{p5(1:>)}FY" 		!copy OPTION
write/keyw out_a {p6}
! 
! test for plot option
if p7(1:1) .eq. "P" then
   default(6:6) = "P"
   dattim = m$time()
   if dazdevr(11) .lt. 0 create/graphics   !make sure we have a graphics window
else
   default(6:6) = "N"
endif
!
run MID_EXE:STATIS
