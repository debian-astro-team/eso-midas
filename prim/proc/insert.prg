! @(#)insert.prg	19.1 (ES0-DMD) 02/25/03 14:08:53
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure insert.prg to implement INSERT/IMAGE
! K. Banse	901108, 910514, 920401
! 
! execute via INSERT/IMAGE subframe modframe startx,y,z
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter subframe: "
define/param p2 ? IMA "Enter modframe: "
!
write/keyw history "INSERT/IMAGE "
write/keyw in_a {p1}
write/keyw out_a {p2}
action(1:2) = "IN"
!
run MID_EXE:GENXX1
