! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure superverify.prg will
! execute the verifications  and all TUTORIAL commands
! and optionally the ESO verifications 
! 
! K. Banse   971027, 991214, 040901, 070822, 120131
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 NO C "Enter wait/nowait in tutorials:"
if p1(1:1) .eq. "N" then
   aux_mode(8) = 1			!wait/secs nn => NoOp
endif
! 
! see, if we have the ESO verifications installed
inputi = m$exist("MIDASHOME:ESO-verifications/README")
if inputi .eq. 0 then		!no, not installed
   define/param p2 NO C 
else
   define/param p2 YES C "Enter YES or NO for ESO verifications:"
endif
define/param p3 NEW C "Enter param2 (NEW/OLD) of the verify procedure:"
!
WRITE/OUT CLEAR/CONTEXT -ALL
CLEAR/CONTEXT -ALL
set/midas keyw=500,300			!enlarge keyword base
!
@ vericopy
WRITE/OUT "@@ veriall"
@@ veriall
!
RESET/DISPLAY
WRITE/OUT TUTORIAL/FILTER AUTO
TUTORI/FILT AUTO
RESET/DISPLAY
!
WRITE/OUT TUTORIAL/GRAPHIC AUTO
TUTORI/GRAP AUTO
RESET/DISPLAY
!
WRITE/OUT TUTORIAL/ITT AUTO
TUTORI/ITT AUTO
RESET/DISPLAY
!
WRITE/OUT TUTORIAL/TABLE AUTO
TUTORI/TABL AUTO
RESET/DISPLAY
!
WRITE/OUT TUTORIAL/FIT AUTO
TUTORI/FIT AUTO
RESET/DISPLAY
!
WRITE/OUT SET/CONTEXT CLOUD
SET/CONTEXT CLOUD
WRITE/OUT TUTORIAL/CLOUD AUTO
TUTORIAL/CLOUD AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
WRITE/OUT SET/CONTEXT ECHELLE
SET/CONTEXT ECHELLE
WRITE/OUT TUTORIAL/ECHELLE AUTO
TUTORIAL/ECHELLE AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
WRITE/OUT Tutorial of the inventory package
@c invdemo
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
WRITE/OUT SET/CONTEXT IRSPEC
SET/CONTEXT IRSPEC
WRITE/OUT TUTORIAL/IRSPEC AUTO
TUTORIAL/IRSPEC AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
WRITE/OUT SET/CONTEXT LONG
SET/CONTEXT LONG
WRITE/OUT TUTORIAL/LONG AUTO
TUTORIAL/LONG AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
WRITE/OUT SET/CONTEXT MOS
SET/CONTEXT MOS
WRITE/OUT TUTORIAL/MOS AUTO
TUTORIAL/MOS AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
WRITE/OUT SET/CONTEXT WAVELET
SET/CONTEXT WAVELET
WRITE/OUT TUTORIAL/WAVELET
TUTORIAL/WAVELET
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
WRITE/OUT SET/CONTEXT DO
SET/CONTEXT DO
WRITE/OUT TUTORIAL/DO AUTO
TUTORIAL/DO AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
! 
write/out set/context ccdred
set/context ccdred
write/out tutorial/mosaic
tutorial/mosaic
clear/context -ALL
reset/display
! 
write/out set/context TSA
set/context TSA
write/out testing TSA
$$ cp $MIDASHOME/$MIDVERS/contrib/tsa/test/* .
@@ test
if tsa_result(1:6) .ne. "PASSED" return/exit
! 
aux_mode(8) = 0			!reenable wait/secs command
!
! ................................
! finally do the ESO verifications
! ................................
! 
if p2(1:1) .eq. "Y" then
   @ verify QC
   if p3(1:1) .eq. "N" then		!ensure that we pass option=NEW
      @ verify EFOSC NEW
   else
      @ verify EFOSC {p3}
   endif
   ! 
   write/out  
   write/out ... procedure `superverify' with ESO verifications successfully terminated ...
else
   write/out  
   write/out ... procedure `superverify' successfully terminated ...
endif
write/out  
 
