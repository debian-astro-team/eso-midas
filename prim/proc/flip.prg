! @(#)flip.prg	19.1 (ES0-DMD) 02/25/03 14:08:51
! +++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  flip.prg  to implement FLIP/IMA, SHIFT/IMA
! K. Banse	900313, 910115, 920113, 920401
!
! use via FLIP/IMAGE frame flag
! where    frame = image to be flipped
!          flag = X, for flipping in x
!               = Y,  ...            y
!               = XY, ...            x and y
! or via  SHIFT/IMAGE inframe outframe shiftx,shifty
!
! +++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input image: "
! 
if mid$cmnd(1:1) .eq. "F" then 				!FLIP/IMA 
   define/param p2 X
   write/keyw action {p2}
   write/keyw history "FLIP/IMAGE "
!
else							!SHIFT/IMA
   define/param p2 ? IMA "Enter output image: "
   define/param p3 1,0 N "Enter xshift,yshift: "
   write/keyw out_a {p2}
   write/keyw inputi {p3}
   write/keyw action/c/1/2 SH
   write/keyw history "SHIFT/IMAGE "
endif
! 
write/keyw in_a {p1}
run MID_EXE:FLIP
