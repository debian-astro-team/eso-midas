! @(#)t2jointbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:06
! 
! .IDENTIFICATION
!
!  MIDAS Command : t2jointbl.prg to implement JOIN/TABLE
!
!  Michele Peron ESO-Garching SEP 92
!
! .PURPOSE
!
!    implements :
!    JOIN/TABLE table1 :X,:Y table2 :X,:Y
!  
!
! ----------------------------------------------------------------------
!
!
DEFINE/PARAM P1 ? TABLE "Enter name of first table "
DEFINE/PARAM P2 ? CHAR  " Enter column reference"
DEFINE/PARAM P3 ? TABLE "Enter name of second table"
DEFINE/PARAM P4 ? CHAR  "Enter column reference"
DEFINE/PARAM P5 ? TABLE "Enter name of the output table"
DEFINE/PARAM P6 0,0 N  "Enter incertainties"
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:tdatatbl
