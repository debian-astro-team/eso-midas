! @(#)tuthelp.prg	19.1 (ES0-DMD) 02/25/03 14:09:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! procedure tuthelp.prg to implement TUTORIAL/HELP
! K. Banse	910708, 911113, 911121
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
WRITE/OUT "To get the main help menu, we type HELP or help"
WRITE/OUT "MIDAS is case insensitive..."
WRITE/OUT HELP
HELP
! 
WRITE/OUT 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT "This gives us all the currently available comands in MIDAS."
WRITE/OUT "Among them is also the command HELP itself,"
WRITE/OUT "so let us apply HELP to the HELP command itself:"
WRITE/OUT 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT HELP HELP
HELP HELP
! 
WRITE/OUT 
WRITE/OUT "We want to see how: HELP help_topic    works, so we enter"
WRITE/OUT "the command HELP HELP/ (no qualifier there...)"
WRITE/OUT 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT HELP HELP/
HELP HELP/
! 
WRITE/OUT 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT "O.K. now we know how to get the help about any of the commands"
WRITE/OUT "listed in the general help menu. But what if we do not know if"
WRITE/OUT "there is a command for a given task and how it is called?"
WRITE/OUT "Well, there is the command HELP/SUBJECT to help you in finding"
WRITE/OUT "the right MIDAS command if possible. HELP/SUBJECT without parameters"
WRITE/OUT "displays all subjects which are stored in our small help data base."
WRITE/OUT "So, let us type HELP/SUBJECT"
WRITE/OUT 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT HELP/SUBJECT
HELP/SUBJECT
! 
WRITE/OUT 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT "Let us assume, we want to know which commands might be useful"
WRITE/OUT "for rotating images."
WRITE/OUT 
WRITE/OUT HELP/SUBJECT rotate
HELP/SUBJECT rotate
! 
WRITE/OUT 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT "Now we got a list of commands which we could explore further"
WRITE/OUT "with the usual HELP command, e.g. HELP REBIN/ROTA"
! 
WRITE/OUT "Besides the commands of the main help menu there are still more"
WRITE/OUT "but less used MIDAS functions. For these functions we did not"
WRITE/OUT "create specific command names, they are accessed simply via the"
WRITE/OUT "procedure name. Help for these functions is obtained via the"
WRITE/OUT "command HELP/APPLICATION. Again HELP/APPLIC without parameters"
WRITE/OUT "gives us a list of all available functions (procedures)"
WRITE/OUT 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT HELP/APPLIC
HELP/APPLIC
! 
WRITE/OUT 
WRITE/OUT "Now we got a list of procedures  which we could explore further"
WRITE/OUT "with the HELP/APPLIC command, e.g. HELP/APPLIC slicube"
WRITE/OUT 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT HELP/APPLIC slicube
HELP/APPLIC slicube
! 
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT "If we want to know which commands operate on a specific object"
WRITE/OUT "like for instance tables, we use HELP/QUALIFIER."
WRITE/OUT "We type HELP/QUALIF TABLE to get all commands with qualifier TABLE."
WRITE/OUT HELP/QUAL TABLE
HELP/QUAL TABLE
INQUIRE/KEYW INPUTC "...Press RETURN to go on ..."
WRITE/OUT "We got a list of all commands with qualifier TABLE (e.g. SORT/TABLE)"
WRITE/OUT "but there may be more commands with the given qualifier;"
WRITE/OUT "these are the commands with a <wild card> qualifier (...)"
WRITE/OUT "so let us also use HELP/QUAL ..."
HELP/QUAL ...
WRITE/OUT "Indeed, with HELP RENAME we find out, that there is also "
WRITE/OUT "RENAME/TABLE but for example SPFFT/TABLE does not exist."
! 
WRITE/OUT
WRITE/OUT "In the same way as demonstrated here you should also explore"
WRITE/OUT "the other options of the HELP command like HELP/KEYW "
