! @(#)overlin.prg	19.1 (ESO-DMD) 02/25/03 14:08:56
! ++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: OVERLIN.PRG
!.PURPOSE:        Overplot a line in a given plot
!.USE             execute as OVERPLOT/LINE par1 par2 par3 par4 par5, where: 
!                 par1 = start position
!                 par2 = end position
!                 par3 = line type
!.AUTHOR:         Rein H. Warmels
!.VERSION:        RHW  880408  Creation
!.VERSION:        RHW  930512  Addition for nomn existing plot file
!------------------------------------------------------------------
! 
DEFINE/PARAM P1 1 N
DEFINE/PARAM P2 C
!
IF PLRGRAP(31) .EQ. 0.0 THEN
   DEFINE/LOCAL PSAV/I/1/1 {PLISTAT(1)}
   SET/GRAPH PMODE=0
   PLOT/AXES 0,1 0,1 0.0,0.0,0.01,0.01 " " " "
   WRITE/KEYW PLISTAT/I/1/1 {PSAV}
ENDIF
!
WRITE/KEYW INPUTI/I/1/1 {P1}
!
IF P2(1:1) .NE. "C" THEN
   DEFINE/LOCAL  NK/I/1/1 0
   NK = M$INDEX(P2,",")+1
   IF NK .LE. 1 THEN
      WRITE/KEYW P2/C/1/80 "? "
      DEFINE/PARAM P2 ? C "Enter start world coordinate:"
   ENDIF
!
   NK = M$INDEX(P3,",")+1
   IF NK .LE. 1 THEN
      WRITE/KEYW P3/C/1/80 "? "
      DEFINE/PARAM P3 ? C "Enter end world coordinate:"
   ENDIF
ENDIF
!  
RUN MID_EXE:OVERLIN
copy/graph
 
