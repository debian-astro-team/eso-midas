! @(#)outtape.prg	19.1 (ESO-DMD) 02/25/03 14:08:55
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (c)   European Southern Observatory
!.IDENT       outtape.prg
!.AUTHOR      P.Grosbol,   ESO/IPG
!.PURPOSE     Convert disk or tape files in internal MIDAS format into
!             the FITS format
!.USAGE       OUTTAPE  catalog[,list] device [flags] [dens,blck] [type]
!.VERSION     1.0   1988-Nov-17 : Creation,  PJG
!.VERSION     1.3   900831: change defaults bf=10 P5=O,  PJG
!.VERSION     1.4   910316: change default CATALOG='*',  PJG
! 000316
! ------------------------------------------------------------------
!
crossref CATALOG DEVICE FLAGS DENS,BLCK 
!
define/param p1 * ? "Enter catalog[,list] : "
if p1(1:1) .eq. "&" then		!check for &a, &b, ...
   define/param p8 {p1} ima
   write/keyw p1 {p8}
endif
!
if p2 .eq. "?" then
   write/out "enter magtape unit (e.g. TAPE1) if writing to magtape"
   inquire/keyw p2 "or enter disk file prefix: "
endif
!
define/param p3 NSN     ? "Enter append,display,cut flags: "
define/param p4 6250,10 N "Enter density,block_factor for tape: "
!
write/keyw inputi/i/1/2 {p4}
!
run MID_EXE:OUTTAPE
