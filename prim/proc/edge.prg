! @(#)edge.prg	19.1 (ESO-DMD) 02/25/03 14:08:49
! +++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  edge.prg  to implement APPLY/EDGE
! K. Banse	890704, 910620
! 	
! APPLY/EDGE inframe outframe threshold method 
! 	
! +++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter input frame: "
define/param p2 ? ima "Enter output frame: "
define/param p3 + n "Enter threshold: "
define/param p4 B				!work on the real data
!
write/keyw in_a {p1}
write/keyw out_a {p2}
if p3(1:1) .ne. "+" then
   default(1:1) = "N"			!threshold explicitely given...
   write/keyw inputr/r/1/1 {p3}
else
   default(1:1) = "Y"			!use max + min to get threshold
endif
! 
action(1:2) = "ED"
run MID_EXE:GENXX1
