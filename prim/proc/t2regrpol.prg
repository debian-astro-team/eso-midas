! @(#)t2regrpol.prg	19.1 (ES0-DMD) 02/25/03 14:09:07
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2regrpol.prg
!  TABLE Subsystem
!
!  J.D.Ponz                ESO - Garching    5 AUG 89
!    M.Peron                                13 JUN 91
!  P. Ballester                             17 SEP 91 (Extended keywords)
!
! .PURPOSE
!
!    implements
!
!  REGRESSION/POLY   table  y  x1[,x2] [DEG1,DEG2]  [outkey]
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ?      TABLE "Enter table:"
DEFINE/PARAM P2 ?      CHAR  "Enter column with dep. variable:"
DEFINE/PARAM P3 ?      CHAR  "Enter column(s) with indep. vars.:"
DEFINE/PARAM P4 1,0    NUMBER
DEFINE/PARAM P5 OUTPUT CHAR  "Output keyword generic name : "
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
WRITE/KEYW MID$CMND/C/1/4 POLY
WRITE/KEYW INPUTR/R/1/2 0.,0.
WRITE/KEYW INPUTR/R/1/2 {P4}
!
RUN MID_EXE:topertbl

