!  @(#)descr.prg	19.1 (ESO-DMD) 02/25/03 14:08:49 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  descr.prg  to implement
!                             DELETE, PRINT, READ, SHOW, WRITE/DESCR
! K. Banse	910228, 920207, 930316, 990120, 020221
!
! execute via DELETE/DESCR frame descr stop_flag
! or          READ/DESCR frame descr_list display_flag
!             PRINT/DESCR frame descr_list display_flag
!             FRITE/DESCR frame descr data [ALL]
!		= WRITE/DESCR with data input from file
!             SHOW/DESCR frame descr_list display_flag
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter frame name:"
branch mid$cmnd(1:1) R,F,P,S read,frite,read,show
! 
! fall through to DELETE/DESCR
define/param p2 ? c "Enter descriptor name:"
define/param p3 Yes c "Enter stop_flag:"
goto run_it
! 
! READ/DESCR
read:
inputi = m$index(p1,".tbl")
if inputi .gt. 1 then
   define/param p2 tblength,tbloffst,tblcontr,tseltabl c -
                 "Enter descriptor names(s): "
else
   define/param p2 naxis,npix,start,step,ident,cunit,lhcuts c -
                 "Enter descriptor names(s): "
endif
define/param p3 Brief ? "Enter display flag (Full or Brief):"
goto run_it
! 
! WRITE/DESCR with data input from a file
frite:
define/param p2 ? c "Enter descriptor name:"
define/param p3 + c "Enter descriptor data:"
define/param p4 no c "Enter ALL flag (All or NoAll):"
goto run_it
! 
! SHOW/DESCR
show:
define/param p2 * c "Enter descriptor name(s):"
define/param p3 Full ? "Enter display flag (Full or Hidden):"
!
run_it:
write/keyw in_a {p1}
write/keyw action {mid$cmnd(1:1)}{p3(1:1)}
!
if mid$cmnd(1:1) .eq. "P" then
   @ print.prg assign
   run MID_EXE:descr
   @ print.prg out
else
   run MID_EXE:descr
endif

