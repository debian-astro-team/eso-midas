! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure dsc_cleaner.prg  to remove descr from file
!  K. Banse     080324
!
!.VERSION
! 080423        last modif
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? C "Enter input file: "
define/param p2 ? C "Enter descr to remove:"
! 
loop:
show/descr {p1} {p2} H
if outputi(1) .eq. 1 then
   delete/descr {p1} {p2}
   goto loop
endif

