! @(#)grow.prg	19.1 (ES0-DMD) 02/25/03 14:08:52
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  grow.prg  to handle GROW/IMA, APPLY/CONVERSION
! J.D.Ponz	840207
! KB		860911, 891220, 901011, 920220, 930222
!
! GROW/IMAGE out = in start,step,no line/column lincol_flag
! GROW/CUBE cube no_planes list
! APPLY/CONV IMTB image table threshold
! APPLY/CONV TBIM table image npix1,npix2 start,..,step.. bgr,fgr
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
if mid$cmnd(1:1) .eq. "G" then			!here for GROW/qualif
   if mid$cmnd(11:11) .eq. "C" then		!GROW/CUBE
      define/param p1 ? IMA "Enter name of 3-dim image:"
      define/param p2 ? N "Enter no. of planes to add:"
      define/param p3 ? C "Enter list of frames:"
! 
      define/local kk/i/1/2 {{p1},naxis},{p2}
      if kk(2) .lt. 1 then
         write/error 5
         return/exit
      endif
      if kk(1) .lt. 2 then
         write/out "input/output image has to be at least 2-dim image..."
         return/exit
      else if kk(1) .eq. 2 then
         define/local ioff/i/1/2 1,0
         define/local sta/d/1/3 {{p1},start(1)},{{p1},start(2)},0.0
         define/local ste/d/1/3 {{p1},step(1)},{{p1},step(2)},1.0
      else
         define/local ioff/i/1/2 {{p1},npix(3)},0
         define/local sta/d/1/3 {{p1},start(1)},{{p1},start(2)},{{p1},start(3)}
         define/local ste/d/1/3 {{p1},step(1)},{{p1},step(2)},{{p1},step(3)}
      endif
      define/local npi/i/1/2 {{p1},npix(1)},{{p1},npix(2)}
      ioff(2) = ioff+kk(2)			!new size
      ioff(1) = ioff(1)+1			!offset for first new plane
      create/image &expa 3,{npi(1)},{npi(2)},{ioff(2)} -
        {sta(1)},{sta(2)},{sta(3)},{ste(1)},{ste(2)},{ste(3)} poly {null(2)}
      insert/image {p1} &expa 			!insert original image
      @a listcrea &expa 3,{npi(1)},{npi(2)},{ioff(2)} {ioff(1)} {p3} >Null
      define/local n/i/1/1 {q1}
      rename/image &expa {p1}
      ioff(2) = ioff(2)-ioff(1)+1
      set/format i1
      if n .lt. ioff(2) then
         set/format f12.4
         n = ioff(2)-n
         write/out {ioff(2)} -
                   "planes added to {p1} (last {n} planes set to {null(2)})"
      else
         write/out {ioff(2)} planes added to {p1}
      endif
      return
   endif
! 
   define/param p1 ? IMA "Enter result image: "
   define/param p3 ? IMA "Enter input image: "
   define/param p4 0.,1.,-1 N  "Enter start,step,no_lines/no_cols: "
   define/param p5 < ? "Enter optional input line/column no via coord,L/C:"
   define/param p6 LIN C "Enter LIN/COLUMN for LIN(e) or COL(umn)wise growing: "
!
   write/keyw in_a {p3}
   write/keyw out_a {p1}
   write/keyw inputd {p4}
   action(1:3) = "GR{p6(1:1)}"
   write/keyw history "GROW/IMAGE "
! 
else						!here for APPLY/CONVERSION
   define/param p1 IMTB ? "Enter IMTB(image -> table) or TBIM(table -> image): "
   if p1(1:1) .eq. "I" then
      define/param p2 ? IMA "Enter input image: "
      define/param p3 ? TAB "Enter result table: "
      define/param p4 1. N  "Enter threshold: "
! 
      write/keyw in_a {p2}
      write/keyw in_b {p3}
      write/keyw inputr {p4}
      action(1:2) = "IM"
   else
      define/param p2 ? TAB "Enter input table: "
      define/param p3 ? IMA "Enter result image: "
      define/param p4 200,200 N "Enter Npix1,Npix2: "
      define/param p5 0.,0.,1.,1. N "Enter Start1,Start2,Step1,Step2: "
      define/param p6 0.,1. N  "Enter background,foreground value: "
! 
      write/keyw in_b {p2}
      write/keyw in_a {p3}
      write/keyw inputi {p4}
      write/keyw inputd {p5}
      write/keyw inputr {p6}
      action(1:2) = "TB"
      write/keyw history "APPLY/CONV "
   endif
endif
run MID_EXE:genyy1
