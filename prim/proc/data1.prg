! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure data1.prg implements READ/IMA, PRINT/IMA
! K. Banse	910104, 930324, 950306, 970624, 040623
!
! execute via READ/IMAGE frame pixel_specs hide_header_flag nopix-per-line
!          or READ/IMAGE CURSOR,option 
! 
!          or PRINT/IMAGE frame pixel_specs hide_header_flag nopix-per-line
!          or PRINT/IMAGE CURSOR,option 
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! save first letter of command in keyword ACTION
action(1:1) = mid$cmnd(1:1)
! 
define/param p1 ? ima "Enter image name: "
define/local hidehead/c/1/1 N
!
if p1(1:6) .eq. "cursor" then		!CURSOR option
   if p1(8:8) .eq. "1" then
      in_b(1:7) = "_CUR_1_"
   else if p1(8:8) .eq. "C" then	!window around central pixel 
      in_b(1:7) = "_CUR_C_"
   else
      in_b(1:7) = "_CUR_2_"
   endif
   write/keyw inputi/i/1/2 0,5
   if  p1(9:9) .eq. "," then		!cursor,curs_opt,enter_opt
      if p1(10:10) .eq. "C" inputi(1) = 1	!continuous input
      if p1(11:11) .eq. "," inputi(2) = {p1(12:)}	!nolines for window
   endif
   cursor  = 9999
   write/keyw in_a * 			!this means the displayed frame
else					!here with "real" image name
   define/param p2 <,<,20 C "Enter xsta,ysta,nopix: "
   define/param p4 0 number "Enter no. of values per line:"
   inputi(1) = {p4}
   define/maxpar 4
   write/keyw in_a {p1}
   write/keyw in_b {p2}
   hidehead = p3(1:1)
endif
!
if action(1:1) .eq. "p" then
   @ print.prg assign		!store stuff in intermediate file
   run MID_EXE:DATA
   @ print.prg out			!print out intermediate file
else
   run MID_EXE:DATA
endif
