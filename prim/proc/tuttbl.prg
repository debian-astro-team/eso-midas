!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT (c) 1992-2007 European Southern Observatory
!.IDENT     tuttbl.prg
!.AUTHOR    J.D.Ponz  - ESO/IPG
!.KEYWORDS  tutorial, demo, test, tables
!.PURPOSE
!      Demo. procedure for table files. 
!      The following files should exist in MID_TEST:
!            ugc.dat, ugc.fmt
!.USAGE
!      Execute as TUTORIAL/TABLE
!
!.VERSION   2.0     22-Sep-1988
!  010618  KB
! 
!------------------------------------------------------------
!
-COPY MID_TEST:ugc.dat tugc.dat
-COPY MID_TEST:ugc.fmt tugc.fmt
!
WRITE/KEYW NAME/C/1/8 "tugc"
SET/GRAPH 
SET/GRAPH PMODE=1
!
WRITE/OUT Create table file
!
WRITE/OUT CREATE/TABLE {NAME} 10 700 {NAME} {NAME}
CREATE/TABLE {NAME} 10 700 {NAME} {NAME}   ! create the table file
wait/secs 1
WRITE/OUT NAME/COLUMN {NAME} #1 F11.3           ! change format
NAME/COLUMN {NAME} #1 F11.3           ! change format
WRITE/OUT NAME/COLUMN {NAME} #2 G12.6              ! change format
NAME/COLUMN {NAME} #2 G12.6           ! change format
wait/secs 1
WRITE/OUT SHOW/TABLE   {NAME}                ! display structure
SHOW/TABLE   {NAME}                ! display structure
WRITE/OUT READ/TABLE   {NAME}  @1 @30        ! display a few entries
READ/TABLE   {NAME}  @1 @30        ! display a few entries
wait/secs 2
!
WRITE/OUT Plot blue and red diameters
!
WRITE/OUT PLOT/TABLE  {NAME} :DR :DB      ! plot diameters in red and blue bands
PLOT/TABLE  {NAME} :DR :DB       ! plot diameters in red and blue bands
!
WRITE/OUT
WRITE/OUT Compute regression coefficients
!
WRITE/OUT REGR/LINEAR {NAME} :DB :DR      ! linear regression on these variables
REGRES/LINEAR {NAME} :DB :DR       ! linear regression on these variables
WRITE/OUT READ/KEYW    OUTPUTD           ! and display stored coefficients
READ/KEYW    OUTPUTD           ! and display stored coefficients
WRITE/OUT
wait/secs 2
WRITE/OUT Compute distribution of apparent magnitudes
!
WRITE/OUT READ/HIST   {NAME} :BT           ! display results on terminal
READ/HIST   {NAME} :BT           ! display results on terminal
WRITE/OUT PLOT/HIST   {NAME} :BT           ! and plot device
PLOT/HIST   {NAME} :BT           ! and plot device
!
WRITE/OUT
WRITE/OUT Select galaxies brighter than 13.5 magnitude
!
WRITE/OUT SELECT/TABLE {NAME} :BT.LT.13.5
SELECT/TABLE {NAME} :BT.LT.13.5    ! select brightest objects
WRITE/OUT
wait/secs 2
!
WRITE/OUT Compute distribution of red diameters on the subset
!
WRITE/OUT STAT/TABLE   {NAME} :DR            ! do statistics on the subset
statist/table   {NAME} :DR            ! do statistics on the subset,
WRITE/OUT READ/HIST  {NAME} :DR            ! display the result
READ/HIST  {NAME} :DR            ! display the result
WRITE/OUT and display the subset
!
WRITE/OUT PLOT/TABLE   {NAME} :BT :RV
PLOT/TABLE   {NAME} :BT :RV        ! and plot the selected set
!
WRITE/OUT Select new subset with rad.vel. greather than 4000 Km/s
WRITE/OUT SELECT/TABLE {NAME} :RV.GT.4000.0
SELECT/TABLE {NAME} :RV.GT.4000.0  
WRITE/OUT
wait/secs 2
!
WRITE/OUT    Compute absolute blue magnitude
WRITE/OUT COMPUTE/TABLE {NAME} :MBT = :BT-25.-5.*LOG10(:RV/50)
COMPUTE/TABLE {NAME} :MBT = :BT-25.-5.*LOG10(:RV/50)       ! compute abs.magnitude
WRITE/OUT
WRITE/OUT NAME/COLUMN    {NAME} :MBT "ABS.B.MAG."
NAME/COLUMN    {NAME} :MBT "ABS.B.MAG."                     ! include units
wait/secs 2
!
WRITE/OUT
WRITE/OUT    Compute linear diameter in Kpc
WRITE/OUT COMPUTE/TABLE {NAME} :SIZE = :RV*SIN(0.000291*:DB)*20
COMPUTE/TABLE {NAME} :SIZE = :RV*SIN(0.000291*:DB)*20      ! diameter
WRITE/OUT
WRITE/OUT NAME/COLUMN    {NAME} :SIZE "KPC"
NAME/COLUMN    {NAME} :SIZE "KPC"                           ! include units
WRITE/OUT
wait/secs 2
!
WRITE/OUT    Plot linear size as function of absolute magnitude
WRITE/OUT PLOT/TABLE   {NAME} :MBT :SIZE
PLOT/TABLE   {NAME} :MBT :SIZE                            ! display result
WRITE/OUT END of TUTORIAL/TABLE
