! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure inmultfits.prg  to implement  INDISK/MFITS
! K. Banse      000918, 010713, 020730, 031028
!
! execute via 
! INDISK/MFITS FITSfile prefix options backw_flag catalog copy_fhead
!
!  where 
!     FITSfile = name of FITS file
!     prefix = root for name generation
!     options = print,format,history option
!     backw_flag = Yes/No (backwards compatibility flag with intape/fits)
!     catalog = optional name of catalog to contain all created file names
!     copy_fhead = option for copying FITS header to extensions
!     		   OJO! this flag is only applicable in Midas working env.
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref NAME PREFIX OPTION BACKWFLAG OUTCATAL FHCOPY
!
define/param P1 ? ? "Enter your name of FITS file:"
define/param P2 + ? "Enter your file identification, max. 4 chars.:"
if p2(1:1) .eq. "+" then
   write/out We miss the prefix... so we use `toto'
   write/keyw inputc "toto   "
else
   write/keyw inputc {p2}
endif
!
define/param p3 SOY ? "Enter print,format,history option:"
define/param p4 Y ? "Enter Yes/No for backwards comp. with INTAPE/FITS:"
define/param p5 + ? "Enter optional catalog name:"
if p5(1:1) .ne. "+" create/icat {p5} NULL >Null
define/param p6 + ? "Enter FITS header copy option:"
!
write/keyw F$OUTNAM/c/1/40 " " all
! 
if aux_mode(13) .eq. 1 then           !we're in true FITS environment
   define/local zeroframe/c/1/80 " " all

   set/midas newfil=midas
   if p5(1:1) .eq. "+" then
      outputi(20) = 0
   else
      outputi(20) = 99
   endif				!we need an intermediate catalog
   out_b = "mid__dummy.cat"
   create/icat {out_b} NULL >Null
   p5 = out_b
   run MID_EXE:INDISK 
   define/local catal/i/1/3 0,-1,0
  cat_loop:
   store/frame in_a {out_b} 1 fin 
   catal(2) = catal(2) + 1
   if catal(2) .eq. 0 then		!omit empty FITS header
      if M$exist(in_a) .eq. 0 .and. mid$info(4) .eq. 1 then
         catal(3) = m$len(in_a) - 4		!single image abc0000.bdf
         in_a({catal(3)}:{catal(3)}) = "1"	!is renamed to abc0001.bdf
         zeroframe = in_a
         outputc = in_a(1:{catal(3)})//".fits"
         goto out
      else
         goto cat_loop
      endif
   endif
   catal(3) = m$len(in_a) - 3		!now convert temp files to FITS
   if m$filtyp(in_a," ") .eq. 2 then
      outputc = in_a(1:{catal(3)})//"tfits"
   else
      outputc = in_a(1:{catal(3)})//"fits"
   endif
  out:
   outdisk/fits {in_a} {outputc}  >Null
   write/out FITS file {outputc} written
   if outputi(20) .eq. 99 add/icat {p5} {outputc} >Null
   goto cat_loop
  fin:
   if zeroframe(1:1) .ne. " " then			!remove temp BDFS
      delete/image {zeroframe} no >Null
   else
      delete/icat mid__dummy.cat no >Null
   endif
   set/midas newfil=fits
else
   run MID_EXE:INDISK
   if p6(1:1) .ne. "+" run MID_EXE:FHEADCOPY		!copy FITS header
endif

