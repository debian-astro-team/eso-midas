! @(#)oversym.prg	19.1 (ESO-DMD) 02/25/03 14:08:56
! ++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: OVERSYM.PRG
!.PURPOSE:        plot a symbol in a given plot in overplot mode
!.USE:            execute as OVER/SYM par1 par2 par3, where: 
!                 par1 = world coordinate or "C"  for using the cursor
!                 par2 = symbol type
!                 par3 = symbol size
!.AUTHOR:         Rein H. Warmels
!.VERSION:        RHW  880411  creation
!.VERSION:        RHW  930512  addition in case of non existing plot file
!------------------------------------------------------------------
! 
DEFINE/PARAM P1 5   N
DEFINE/PARAM P2 C
DEFINE/PARAM P3 1.0 N
!
IF PLRGRAP(31) .EQ. 0.0 THEN
   DEFINE/LOCAL PSAV/I/1/1 {PLISTAT(1)}
   SET/GRAPH PMODE=0
   PLOT/AXES 0,1 0,1 0.0,0.0,0.01,0.01 " " " "
   WRITE/KEYW PLISTAT/I/1/1 {PSAV}
ENDIF
!
WRITE/KEYW INPUTI/I/1/1  {P1}
WRITE/KEYW INPUTC/C/1/80 {P2}
WRITE/KEYW INPUTR/R/1/1  {P3}
!
RUN MID_EXE:OVERSYM
copy/graph
 
