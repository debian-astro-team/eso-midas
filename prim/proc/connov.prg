! @(#)connov.prg	19.1 (ESO-DMD) 02/25/03 14:08:46
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure connov.prg
! 
! K. Banse	880614, 920312, 000322		ESO - IPG
!
! use via CONNECT/OVERLAY im_chan
!         im_chan = channel to which the overlay shall be connected
!                   -1 if overlay connected to no image channel at all
! 	
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 + C "Enter image channel: "
! 
inputi(19) = -1
! 
if p1(1:3) .eq. "OFF" goto ende
if p1(1:2) .eq. "-1" goto ende
! 
if p1(1:1) .ne. "+" then
   inputi(18) = ididev(15)		!save current active channel   
   set/chan {p1}
   inputi(19) = ididev(15)
   ididev(15) = inputi(18)		!reset it
else
   inputi(19) = ididev(15)
endif
! 
ENDE:
dazhold(13) = inputi(19)
