! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  average.prg  for AVERAGE/IMAGE, /WEIGHTS, /WINDOW
! K. Banse	880919, 910318, 931103, 000426, 090729
! 
! use via
! AVER/IMAGE out = inframes [mergeflg] [null] [option] [valid_interval]
! with inframes: frame1,frame2,...,framen  or  catalog.cat
! 
! or      AVER/WEIGHT out = inframes [mergeflg] [null]
! and     AVER/ROW out = input start,end [NO]
! and     AVER/COLUMN out = input start,end [NO]
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter result frame: "
if p2(1:2) .ne. "= " then
   write/out "wrong syntax - should be: result = ..."
   write/error 30
   return/exit
endif
! 
write/keyw history "AVERAGE/{MID$CMND(11:14)}"
action(2:2) = mid$cmnd(11:11)
write/keyw out_a {p1}
!
if mid$cmnd(11:13) .eq. "ROW" then		!AVERAGE/ROW
   define/param p3 ? ima "Enter input frame: "
   define/param p4 <,> c "Enter start,endpixel in row: "
   define/param p5 Y C "Enter SUM or NO for summing up: "
   define/maxpar 5
   write/keyw in_a {p3}
   run MID_EXE:AVEROW
!
elseif mid$cmnd(11:13) .eq. "COL" then		!AVERAGE/COL
   define/param p3 ? ima "Enter input frame:"
   define/param p4 <,> c "Enter start,endpixel in column: "
   define/param p5 Y C "Enter SUM or NO for summing up: "
   define/maxpar 5
   write/keyw in_a {p3}
   run MID_EXE:AVEROW
!
else  					!AVERAGE/IMAGE + /WEIGHT
   define/param p3 ? c "Enter frame_list or catalog.cat: "
   define/param p4 N C "Enter merging option M(erge) or N(oMerge): "
   action(1:1) = p4(1:1)
   define/param p5 {null(2)} c "Enter Null value for undefined pixels: "
   if action(2:2) .eq. "W" then		!here the WEIGHT option
      run MID_EXE:AVERAGW
   else
      define/param p6 AV C "Enter option for averaging: "
      define/param p7 + n "Enter valid interval xlo,xhi: "
      define/maxpar 7
      run MID_EXE:AVERAG
   endif
!
endif
