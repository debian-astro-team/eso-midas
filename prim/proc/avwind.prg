! @(#)avwind.prg	19.1 (ES0-DMD) 02/25/03 14:08:44
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  avwind.prg  for AVERAGE/WINDOW
! K. Banse      880919, 910318, 920326, 930923, 940315
!
! use via
! AVER/WIN out = inframes method [bgerr,snoise]
! with inframes: frame1,frame2,...,framen  or  catalog.cat
!
! reinstalled because the WINDOW option is not yet in the AVERAGE/IMAGE
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter result frame: "
DEFINE/PARAM P3 ? C "Enter frame_list or catalog.cat: "
DEFINE/PARAM P4 WINDOW C "Enter method, WINDOW, MAX, or MIN : "
DEFINE/PARAM P5 0.1,0.1 N "Enter : "
!
WRITE/KEYW OUT_A {P1}
DEFINE/LOCAL NN/I/1/1 0
NN = M$TSTNO(P4)
IF NN .EQ. 1 THEN
   WRITE/KEYW INPUTR/R/1/2 {P4}
   WRITE/KEYW ACTION/C/1/3 WIN
ELSE
   WRITE/KEYW ACTION/C/1/3 {P4}
   WRITE/KEYW INPUTR/R/1/2 {P5}
ENDIF
! 
write/keyw history "AVERAGE/WINDOW"
RUN MID_EXE:AVWNDW
