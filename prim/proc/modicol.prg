! @(#)modicol.prg	19.1 (ESO-DMD) 02/25/03 14:08:55
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  modicol.prg  to implement MODIFY/COLUMN + /ROW
! K. Banse	880912
!
! use via MODIFY/COLUMN inframe outframe C/V coord1,...,coord10
!      or MODIFY/COLUMN inframe,intable outframe C/V
!         MODIFY/ROW inframe outframe C/V coord1,...,coord10
!      or MODIFY/ROW inframe,intable outframe C/V
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 + C "Enter input frame or inframe,intable: "
define/param p2 result IMA "Enter result frame: "
define/param p3 C C "Enter C or V for constant or variable replacement: "
!
if p1(1:1) .eq. "+" p1 = "*"	!default to displayed image
write/keyw in_a {p1}
write/keyw action {mid$cmnd(11:11)}{p3(1:1)}
write/keyw history "{mid$cmnd(1:6)}/{mid$cmnd(11:14)} "
write/keyw out_a {p2}
!
run MID_EXE:modcol
