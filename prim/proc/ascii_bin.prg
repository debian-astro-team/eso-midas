! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  MIDAS procedure ascii_bin.prg 
!  to convert ascii LUT + ITT files to binary MIDAS tables
!  and to create viewluts.aux (needed for VIEW/IMA)
!  and to build (or copy) necessary X11-Font file
!  K. Banse	ESO - Garching	910506
! 
!  use via: @ ascii_bin [no_fontfile_creation]
! 
!.VERSION
! 06410	last modif
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! prepare the different directory names 
! 
if aux_mode(1) .le. 1 then              ! VMS
   define/local ascdir/c/1/60 -
     "MID_DISK:[&MIDASHOME.&MIDVERS.SYSTAB.ASCII.DISPLAY] " ? +lower
   define/local bindir/c/1/60 -
     "MID_DISK:[&MIDASHOME.&MIDVERS.SYSTAB.BIN] " ? +lower
   define/local defdir/c/1/2 "[]" ? +lower
else                                    ! UNIX
   define/local ascdir/c/1/60 "$MID_HOME/systab/ascii/display/ " ? +lower
   define/local bindir/c/1/60 "$MID_HOME/systab/bin/ " ? +lower
   define/local defdir/c/1/2 "./" ? +lower
endif
!
!  first the LUTs
write/out Creating LUTs ...
@ asciilut
write/out -----------------------------------------------------
!
!  then the ITTs
write/out Creating ITTs ...
@ asciiitt
write/out -----------------------------------------------------
!
!  and also create the frame idisave.dat in $MIDASHOME/$MIDVERS/incl
write/out Creating auxiliary file idisave.dat ...
@ idisave
write/out -----------------------------------------------------
! 
!  and also create the frame viewluts.aux in $MID_SYSTAB
write/out Creating auxiliary file viewluts.aux ...
@ viewluts
write/out -----------------------------------------------------
! 
!  and also create the file FORGRdrs.KEY in $MID_MONIT (from current keyfile)
-delete MID_WORK:FORGRdrs.KEY
! 
write/out "Creating binary keyword file FORGRdrs.KEY in $MID_MONIT ..."
if aux_mode .ne. 1 then				!Unix
   $ cp $MID_WORK/FORGR{mid$sess(11:12)}.KEY $MID_MONIT/FORGRdrs.KEY
else						!VMS
   $ copy MID_WORK:FORGR{mid$sess(11:12)}.KEY MID_MONIT:FORGRdrs.KEY 
endif
write/out -----------------------------------------------------
! 
!  and also rename the files in MID_TEST if not in cygwin
! 
if mid$sys(1:5) .ne. "PC/Cy" then
   write/out "Copying data files in testing dir. to official VLT names"
   if aux_mode .ne. 1 then				!Unix
      $cp $MIDASHOME/$MIDVERS/test/prim/VISIR.2004-09-30T03c17c49.095.fits -
          $MIDASHOME/$MIDVERS/test/prim/VISIR.2004-09-30T03:17:49.095.fits
   else							!VMS
    $copy -
    MID_DISK:[&MIDASHOME.&MIDVERS.TEST.PRIM]VISIR.2004-09-30T03c17c49.095.fits -
    MID_DISK:[&MIDASHOME.&MIDVERS.TEST.PRIM]VISIR.2004-09-30T03:17:49.095.fits
   endif
! 
   write/out "Renaming the data files in MID_TEST with official VLT names"
  -rename MID_TEST:FORS.1999-01-27T05c43c50.495.fits MID_TEST:FORS.1999-01-27T05:43:50.495.fits
  -rename MID_TEST:GIRAF.2004-05-19T04c45c53.521.fits MID_TEST:GIRAF.2004-05-19T04:45:53.521.fits
  -rename MID_TEST:GIRAF.2004-06-11T20c19c52.169.fits MID_TEST:GIRAF.2004-06-11T20:19:52.169.fits
  -rename MID_TEST:UVES.2002-11-18T05c49c19.789.fits MID_TEST:UVES.2002-11-18T05:49:19.789.fits
  -rename MID_TEST:WFI.2000-12-19T01c10c03.706.fits MID_TEST:WFI.2000-12-19T01:10:03.706.fits
  -rename MID_TEST:r.VIMOS.2004-07-13T08c27c31.790_0000.fits MID_TEST:r.VIMOS.2004-07-13T08:27:31.790_0000.fits 
endif
! 
write/out -----------------------------------------------------
! 
!  and finally build x11fonts.dat for $MID_SYSTAB 
inputc = m$symbol("DISPLAY")
if p1(1:2) .eq. "NO" then
   write/out We copy default (created at ESO) X11-Fonts-file: x11fonts.dat 
   write/out from /midas/{MID$SESS(16:20)}/systab/ascii/display to $MID_SYSTAB
   @ ascii_bin,copy_x11fonts
else if inputc(1:4) .eq. "DISP" then
   write/out OJO - environment variable DISPLAY not set.
   write/out We copy default (created at ESO) X11-Fonts-file: x11fonts.dat 
   write/out from /midas/{MID$SESS(16:20)}/systab/ascii/display to $MID_SYSTAB
   write/out You should create a local x11fonts.dat once you are running Midas
   write/out with a display (and DISPLAY var. set) via: @a showfonts all new
   write/out Check the command options of that procedure via: -
             "help/applic showfonts"
   @ ascii_bin,copy_x11fonts
else
   write/out Creating Fonts file x11fonts.dat for current X11 server ...
   outputi(15) = -1
   @a showfonts all new nn >Null
   if outputi(15) .gt. 0 then
      write/out x11fonts.dat succesfully created + copied to MID_SYSTAB
   endif
   write/out Now, get rid of previous x11fonts.dat in current dir. (if any)
   if aux_mode(1) .le. 1 then              !delete Fontfile in current dir.
      $ DELETE x11fonts.dat.*
   else                                    ! UNIX
      $ rm x11fonts.dat
   endif
endif
write/out -----------------------------------------------------
! 
entry copy_x11fonts
if aux_mode(1) .le. 1 then              ! VMS
   $ COPY MID_DISK:[&MIDASHOME.&MIDVERS.SYSTAB.ASCII.DISPLAY]x11fonts.dat MID_DISK:[&MIDASHOME.&MIDVERS.SYSTAB.BIN]
else
   $ cp $MID_HOME/systab/ascii/display/x11fonts.dat $MID_SYSTAB
endif

