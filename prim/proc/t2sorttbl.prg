! @(#)t2sorttbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:08
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2sorttbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 82
!
! .PURPOSE
!
!    implements
!
!  SORT/TABLE    table-name   column [ASC/DESC]
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
DEFINE/PARAM P2 ? CHAR  "Enter column reference:"
!DEFINE/PARAM P3 ASC CHAR "Sorting sequence"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:tdatatbl
