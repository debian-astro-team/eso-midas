! @(#)snapshot.prg	19.2 (ESO-DMD) 05/27/03 16:20:52
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure snapshot.prg to implement SHOW/DISPLAY, SHOW/MIDAS_SYS
! K. Banse      890628
!
! use via  SHOW/DISPLAY or SHOW/MIDAS
! 
!.VERSION
! 030527	last modif
! 
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
set/format i1
! 
define/local indx/i/1/1  0
define/local state/c/1/20 " " all
! 
! display section
! 
if mid$cmnd(11:11) .eq. "D" then
   define/param p1 N C "Enter ALL for complete display status:"
   !   
   if ididev(18) .ne. 11 then
      write/out "SHOW/DISPLAY only supported for X11 Displays"
      return
   endif
   !
   if p1(1:1) .eq. "I" then		!get IDI structures for debugging
      if p2(1:1) .eq. "I" then
         inputi(1) = 101			!get ididev...
      else if  p2(1:1) .eq. "X" then
         inputi(1) = 102			!get xworkst...
      else if  p2(1:1) .eq. "M" then
         inputi(1) = 103			!get memdata...
         inputi(2) = {p3}	
      endif
      action(1:2) = "AN"
      run MID_EXE:idauxx.exe
      return
   endif
   !
   if p1(1:1) .eq. "A" then
      inputi(1) = 10                           !flag for getting IDIDEV data 
      action(1:2) = "AN"
   else
      outputi(1) = -1
      outputi(12) = -1
      action(1:2) = "OP"
   endif
   ! 
   run MID_EXE:idauxx.exe
   outputi(1) = dazdevr(10)
   if dazdevr(10) .eq. -1 then
      write/out "no active display window..."
      goto graph_section
   endif
   !
   indx = dazdevr(10) + 1
   if winopen({indx}) .eq. 2 then
      write/keyw state/c/1/20 "(in Icon state) "
   endif
   !
   write/out "currently active display window =" {outputi(1)} " {state}"
   write/out "display x-dim, y-dim = {ididev(2)},{ididev(3)} pixels," -
             depth = {ididev(13)} bits
   outputi(2) = ididev(2)
   outputi(3) = ididev(3)
   outputi(4) = ididev(13)
   write/out "channels are numbered 0, 1, ..."
   write/out "{ididev(16)} channels with x-dim, y-dim ="-
             {ididev(11)},{ididev(12)} -
             "pixels, depth = {ididev(13)} bits exist"
   outputi(5) = ididev(16)
   outputi(6) = ididev(11)
   outputi(7) = ididev(12)
   outputi(8) = ididev(13)
   write/out "channel no. {ididev(16)} serves as overlay channel" -
             (emulated by software) 
   outputi(9) = ididev(15)
   write/out "currently active image channel (memory) = no. {ididev(15)}"

   outputi(10) = ididev(21)
   outputi(11) = ididev(17)
   if ididev(21) .gt. 1 then
      write/out "there are {ididev(21)} LUTs with" {ididev(17)} entries
   else
      write/out "there is one LUT with" {ididev(17)} entries
   endif
   if hcittlut(42:42) .eq. "Y" then
      write/out "current LUT =" {hcittlut(21:40)}
   endif
   if hcittlut(41:41) .eq. "Y" then
      write/out "current ITT =" {hcittlut(1:20)}
   endif
   write/out 

 graph_section:
   outputi(12) = dazdevr(11)
   if dazdevr(11) .eq. -1 then
      write/out "no active graphics window..."
   else
      indx = dazdevr(11) + 11
      if winopen({indx}) .eq. 2 then
         write/keyw state/c/1/20 "(in Icon state) "
      else
         write/keyw state/c/1/20 " " all
      endif
      ! 
      outputi(13) = ididev(32)
      outputi(14) = ididev(33)
      write/out "currently active graphics window =" -
                {dazdevr(11)} " {state}"
      write/out "graphics x-dim, y-dim = {ididev(32)},{ididev(33)} pixels" 
   endif
   ! 
   write/out 
   write/out -
 "screen size = {dazdevr(12)} x {dazdevr(13)} pixels (depth = {ididev(13)} bits)"
   write/out 
   if dazdevr(18) .eq. 0 then
      write/out we work in PseudoColor mode
   else if dazdevr(18) .eq. 1 then 
      write/out we work in RGB mode (3 image planes needed)
   else if dazdevr(18) .eq. 3 then 
      write/out "we work in" -
                 "PseudoColor mode on {ididev(4)} bit graphics (TrueColor)"
      write/out "(LUTs are emulated)"
   else 
      write/out "we work in" -
                 "PseudoColor mode on {ididev(4)} bit graphics (DirectColor)"
      write/out "(LUTs are emulated)"
   endif
   ! 
   if ididev .lt. 0 then		!show if IDIserver is idle or stopped
      write/out
      if ididev .eq. -1 then
         write/out IDIserver idle (but running)
      else
         write/out IDIserver not running
      endif
   endif
! 
! Midas system section		(SHOW/MIDAS)
! 
else
   set/midas keyw=?
   set/midas comm=?
   write/out "internal allocation buffersize =" -
             {monitpar(20)} x {monitpar(20)} real image
   write/out 
   !
   define/local mm/i/1/10 0 all
   mm = (log(5) - 1)*log(7) + log(6)
   if log(5) .gt. 1 then
      write/out {mm} "lines written to logfile"  -
       MID_WORK:FORGR{mid$sess(11:12)}.LOG ({log(5)} pages)
   else
      write/out {mm} "lines written to logfile"  -
       MID_WORK:FORGR{mid$sess(11:12)}.LOG 
   endif
   !
   set/midas workenv=?
endif
   
