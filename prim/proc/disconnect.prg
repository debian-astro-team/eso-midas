! @(#)disconnect.prg	19.1 (ES0-DMD) 02/25/03 14:08:49
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure disconnect.prg  is used with DISCONNECT/BACK_MIDAS
! K. Banse	961111
!
! execute via @ disconnect serv_unit b_char method close_flag 
! with serv_unit = the unit of background Midas (server)
!      b_char = the background connection char.
!      method = the connection method: F - files, S - local sockets,
!			               R - remote sockets
!      close_flag = 'X' for closing connection on server side also
!                   'Z' as above but also terminate background Midas
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 + C "Enter server unit:"
define/param p2 b C "Enter background char:"
define/param p3 S C "Enter method:"
define/param p4 X C "Enter discon_server_flag:"
! 
if p3(1:1) .eq. "S" then
   define/local wait/c/1/1 2
else if p3(1:1) .eq. "R" then
   define/local wait/c/1/1 5
else
   define/local wait/c/1/1 " "
endif
! 
if p4(1:1) .eq. "X" then
   {p2},clear/back {wait}		!{wait} seconds delay
else
   {p2},clear/back {wait}\;bye		!{wait} seconds delay + terminate
endif
! 
disconnect/back	{p1}			!do this on client side
