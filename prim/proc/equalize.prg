! @(#)equalize.prg	19.1 (ES0-DMD) 02/25/03 14:08:50
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure EQUALIZE to implement EQUALIZE/HISTOGRAM
!  K. Banse 	860910
!
!  execute via EQUA/HIST frame [descr]
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter image name:"
!
WRITE/KEYW IN_A {P1}
RUN MID_EXE:HISTEQU			!DeAnza, send ITT in program
