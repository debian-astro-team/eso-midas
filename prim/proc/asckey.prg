! @(#)asckey.prg	19.1 (ES0-DMD) 02/25/03 14:08:43
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  MIDAS procedure asckey.prg to fill keywords from ASCII files
!  K. Banse  930614
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? c -
"Enter name of keyword or keyword/type/1./last_elem: "
define/param p2 ? c "Enter name of ASCII file with keyword values: "
! 
define/local fcnt/i/1/2 0,0 
define/local record/c/1/80 " " all
! 
open/file {p2} r fcnt
if fcnt(1) .lt. 0 then
   write/out Invalid file name {p2}...
   return/exit
endif
! 
read/file {fcnt(1)} record 80
if fcnt(2) .lt. 0 then
   write/out No data values in file...
   close/file {fcnt(1)}
   return/exit
else
   write/keyw {p1} {record}
   close/file {fcnt(1)}
endif
