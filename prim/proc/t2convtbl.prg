! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS procedure: t2convtbl.prg
!  TABLE Subsystem
!
!  J.D.Ponz                ESO - Garching   1.0  5 OCT 89
!  120210	last modif  (KB)
!
! .PURPOSE
!    implements
!    CONV/TABLE image = table x[,y] [z] ref-ima method [par]
!					      method = FREQ
!  						       POLY
!						       PLOT
!						       SPEC
!    mapping tables into images
! ----------------------------------------------------------------------
!
define/param P1 ? IMAGE "Enter output image:"
define/param P3 ? TABLE "Enter input table:"
define/param P4 ? ?     "Enter column(s) with ind. vars.:"
! 
! P5 either column of dependent var. or the reference image
! 
! Fix provided by Panos Patsis 30.06.20
!    "convert/table" command with the "FREQUENCY" option
!
define/param P5 ? ?     "Enter column with dep. var. or ref. image:"
if p5(1:1) .eq. ":" .or. p5(1:1) .eq. "#" then
   define/param P6 ? IMAGE "Enter reference image:"
   define/param P7 SPLINE C  "Enter method (POLY,PLOT,SPLINE):"
else
   define/param P6 FREQ C		!only possibility
   define/param P7 +
   write/keyw MID$CMND/C/1/4 CMAP
   goto run_it
endif
!
! use MID$CMND to distinguish the different options
! 
if p7(1:1) .eq. "S" then
   if {pcount} .eq. 8 then
      write/keyw INPUTR/R/1/2 {P8}
      write/keyw MID$CMND/C/1/4 CSPL
   endif
! 
else
   write/keyw MID$CMND/C/1/4 CMAP
   if {pcount} .eq. 8 then
      write/keyw INPUTR/R/1/2 {P8}
   else
      write/keyw INPUTR/R/1/2 0.,255.
   endif
endif
!
run_it:
write/keyw HISTORY "CONVERT/TABLE "
run MID_EXE:topertbl
