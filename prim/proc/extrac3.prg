! @(#)extrac3.prg	19.1 (ES0-DMD) 02/25/03 14:08:50
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  extrac3.prg  for EXTRACT/SLIT
! K. Banse	901108, 930324
!
! use via EXTRACT/SLIT CURSOR outframe slitwidth,slitlength,X/Y stepx,stepy
!         EXTRACT/SLIT CURSOR outframe refslit
!         EXTRACT/SLIT inframe outframe refslit slit_angle
! 	
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 CURSOR IMA "Enter input frame or CURSOR : "
define/param p2 slit IMA "Enter result frame: "
define/param p3 @20,@60,X C "Enter size of slit as width,length,ref_cord: "
define/param p4 0.0,0.0 N "Enter stepx,stepy for extracted image: "
define/maxpar 4
!
if dazhold(3) .ne. 0 then 		!we cannot work in split mode...
   write/out Command EXTRACT/SLIT "cannot be used in split mode...!"
   return
endif
! 
write/keyw history "EXTRACT/SLIT "
inputi(10) = 40000		!max. dimension in x, has to be changed maybe...
write/keyw out_a {p2}
!
action(2:2) = "S"		!default to no_reference_slit option
if p1 .eq. "CURSOR" then
   action(1:1) = "C"
else
   action(1:1) = "F"
   write/keyw in_a {p1}
endif
!
!
if p3(1:1) .ne. "@" then
   define/local num/i/1/1 0
   num = m$tstno(p3)
   if num .eq. 0 then			!we use a reference slit
      action(2:2) = "F"
      define/param p3 ? ima "Enter reference slit: "
      define/param p4 45. n "Enter angle of baseline in degrees: "
      write/keyw in_b {p3}
      write/keyw inputr {p4}
   endif
endif
!
write/keyw inputd {p4}
run MID_EXE:EXSLIT
