!
! Midas procedure tbl3bdf.prg
! Copy image file from/to 3Dtable column
! 
! K.Banse	ESO/SDD
! 
! 100813	creation
! 100831	last modif
! 
if MID$CMND(11:11) .eq. "I" then
   ! COPY/IMT3
   ! use via: @ tbl3bdf input_ima table colref,rowno felem,noelms upda_flag

   define/para p1  ?  I "input image:"
   define/para p2  ?  T "result table:"
   define/para p3  *  C "col_ref,row_no:"
   define/para p4  *  C "1st element,no. of elements:"
   define/para p5  NO  C -
      "flag (Update/NO_update) OR no_of_rows to allocate:"
   !
   write/keyw  in_a/c/1/80     {p1}
   write/keyw  out_a/c/1/64    {p2}
   write/keyw  inputc/c/1/24   {p3}
   write/keyw  inputc/c/31/20   {p4}
   !
   inputc(26:26) = "?"
   if m$tstno(p5) .eq. 1 then
      inputc(26:26) = "X"
      write/keyw inputi {p5}
   else
      if p5(1:1) .eq. "U" then
         if m$exist(out_a) .eq. 1 then
            inputc(26:26) = "U"
         else
            outputc(1:) = out_a // ".tbl"
            if m$exist(outputc) .eq. 1 then
               out_a(1:) = outputc(1:)
               inputc(26:26) = "U"
            endif
         endif
      endif
   endif

   if p3(1:1) .eq. "*" p3(1:2) = "? "
   if p4(1:1) .eq. "*" p4(1:2) = "? "
   write/keyw  history/c/1/80  "COPY/IMT3 {p1} {p2} {p3} {p4} {p5}"
   run MID_EXE:bdf3tbl
   !
else
   ! COPY/T3IMA
   ! use via: @ tbl3bdf table column nrlo,nrhi felem,noelem startx,stepx result

   define/para p1  ?    T  "input table:"
   define/para p2  ?    C  "column:"
   define/para p3  1,1  C  "1st row, no. of rows:"
   define/para p4  *    C  "1st elem, no. of elements in array column:"
   define/para p5  *    C  "xstart,xstep[,ystart,ystep] :"
   define/para p6  tbimage  I  "result image:"
   !
   write/keyw  in_a/c/1/60     {p1}
   write/keyw  inputc/c/1/60   {p2}
   define/local row/c/1/60      {p3}
   define/local dim/c/1/60      {p4}
   define/local coor/c/1/60     {p5}
   write/keyw  out_a/c/1/80     {p6}
   !
   if p4(1:1) .eq. "*" p4(1:2) = "? "
   if p5(1:1) .eq. "*" p5(1:2) = "? "
   write/keyw  history/c/1/80  "COPY/T3IMA {p1} {p2} {p3} {p4} {p5} {p6}"
   !
   run MID_EXE:tbl3bdf
endif
