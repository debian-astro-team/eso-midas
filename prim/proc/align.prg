! @(#)align.prg	19.1 (ES0-DMD) 02/25/03 14:08:42
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
!
! MIDAS procedure align.prg to implement ALIGN/IMA, /CENTER
! K. Banse	900109, 940503
!
! use via ALIGN/IMAGE table1 table2 method overlay_flag,intensity residu_flg
! with method = FREE	(all parameters are free)
!             = EQUAL	(x-, y-scalings are equal)
! 	      = UNIT	(x-, y-scalings are equal 1)
!      overlay_flag  = ov or no
!      if overlay_flag given and no intensity, it is defaulted to 255
!      residu_flg = yes or no
! 
! and via ALIGN/CENT son father scx,scy fcx,fcy
!     scx,scy center coords of son
!     fcx,fcy center coords of father
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
!
if mid$cmnd(11:11) .eq. "I" goto ali_ima
! 
!  here for ALIGN/CENTER
define/param p1 ? IMA  "Enter son frame: "
define/param p2 ? IMA  "Enter father frame: "
define/param p3 ? C    "Enter center coords of son as cx,cy: "
define/param p4 ? C    "Enter center coords of father as cx,cy: "
define/maxpar 4
! 
write/keyw in_a {p1}
write/keyw in_b {p2}
action = "CC"
run MID_EXE:GENYY1
return
! 
!  here for ALIGN/IMAGE
ALI_IMA:
define/param p1 ?    TABLE    "Enter table1:"
define/param p2 ?    TABLE    "Enter table2:"
define/param p3 UNIT ?        "Enter method:"
define/param p4 NO   ?        "Enter overlay_flag:"
define/param p5 NO   ?        "Enter residual_flag: "
define/maxpar 5
!
write/keyw in_a {p1}
write/keyw in_b {p2}
write/keyw inputc {p3(1:1)}{p5(1:1)}
write/keyw transfrm/d/1/9 0. all		!clear key TRANSFRM(1 - 9)
!
action(1:1) = "A"			!tell ALIGN that we really want to align
run MID_EXE:ALIGN
