! @(#)plotgra.prg	19.2 (ESO-DMD) 06/05/03 11:34:02
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLOTGRA.PRG
!.PURPOSE: MIDAS procedure to produce a gray scale map of a two-dim.
!.USE:     PLOT/GRAY P1 [P2] [P3] [P4] [P5] [P6] [P7] 
!          OVERPLOT/GRAY P1 [P2] [P3] [P4] [P5] [P6] where:
!          P1:    image is the image to be plotted
!          P2:    coord_string is the area
!          P3:    scal_x,scal_y, off_x,off_y 
!          P4/P3: grey scales
!          P5/P4: smooth_par is the parameter which represents the
!                 size of smoothing box
!          P6/P5: determines the greyness of the plot (0.0 --> 1.0)
!          P7/P6: options in the format option1[,option2[,option3]]
!                 options can be: 
!                 LOG  = logarythmic greyness (def.: LINEAR)
!                 NEG  = inverse grey scale (def.: original values)
!                 ABS  = abs. values are taken (def.: not absolute)
!                 STAP = greyness increases stepw. (def.: CONTINUOUS)
!
!.AUTHOR:  R.H. Warmels, ESO - Garching 870806
!.VERSION: K.Banse                      880922, 900409, 900712, 920413
!.VERSION: R.H. Warmels                 890601
!.VERSION: 920224 RHW Offset in x and y included
!----------------------------------------------------------------------------
! 
DEFINE/PARAM P1 *         IMA
define/local cmnd/c/1/20 "{mid$cmnd(1:20)}"          !save initial command
!
IF PLRSTAT(1) .EQ. 0.0 .AND. PLRSTAT(2) .EQ. 0.0 THEN
   IF PLRSTAT(5) .EQ. 0.0 .AND. PLRSTAT(6) .EQ. 0.0 THEN
      DEFINE/PARAM P2 [<,<:>,>] C
      IF P2(1:1) .EQ. "M" THEN
         WRITE/OUT "*** FATAL: axes not properly set; use SET/GRAPHICS"
         RETURN
      ENDIF
   ELSE
      DEFINE/PARAM P2 MANUAL   C
   ENDIF
ELSE
   DEFINE/PARAM P2 MANUAL   C
ENDIF
!
!if mid$sess(7:7) .eq. " " .or. mid$sess(7:7) .eq. "-" then
!   create/graphics
!   write/keyw mid$cmnd/c/1/20 "{cmnd(1:20)}"            !reset MID$CMND
!endif
!
IF P2 .EQ. "C" THEN
   IF P1 .NE. "?" LOAD/IMAGE {P1}
   GET/CURSOR ? ? NN 1,2		         ! use cursor rectangle once
   write/keyw mid$cmnd/c/1/20 "{cmnd(1:20)}"                  !reset MID$CMND
ENDIF
!
WRITE/KEYW IN_A {P1}
WRITE/KEYW IN_B {P2}
!
IF MID$CMND(1:1) .EQ. "P" THEN
   DEFINE/PARAM P3 0.0,0.0,-999,-999 NUM
   @ plscoff.prg_o {P3}
   DEFINE/PARAM P4 DEF       C
   DEFINE/PARAM P5 0         NUM
   DEFINE/PARAM P6 1.0       NUM
   DEFINE/PARAM P7 "CONT,LINE,POS"
!
   WRITE/KEYW INPUTC/C/1/40  {P4}
   WRITE/KEYW INPUTI/I/1/1   {P5}                 ! smoothing parameter
   WRITE/KEYW INPUTR/R/3/1   {P6}                 ! grayness parameter
   WRITE/KEYW INPUTC/C/41/40 {P7}                 ! options
   DATTIM = M$TIME()
ELSE
   DEFINE/PARAM P3 DEF     C
   DEFINE/PARAM P4 0       NUM
   DEFINE/PARAM P5 1.0     NUM
   DEFINE/PARAM P6 "CONT,LINE,POS"
!
   WRITE/KEYW INPUTC/C/1/40  {P3}
   WRITE/KEYW INPUTI/I/1/1   {P4}                 ! smoothing parameter
   WRITE/KEYW INPUTR/R/3/1   {P5}                 ! grayness parameter
   WRITE/KEYW INPUTC/C/41/40 {P6}                 ! options
ENDIF
!
RUN MID_EXE:PLOTGRA                              ! make the bloddy plot
WRITE/KEYW PLCDATA/C/1/60  {P1}                   ! name, type data struct.
WRITE/KEYW PLCDATA/C/61/20 "FRAME       "
copy/graph

