! @(#)tutorial.prg	19.1 (ES0-DMD) 02/25/03 14:09:11
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure tutorial.prg to implement the TUTORIAL commands
! which are used in the Core Midas and Applic
! K. Banse	901029, 901212, 910322, 910419
! R.H. Warmels  881005
! currently exist: TUT//ITT, /LUT, /FILTER, /TABLE, /SPLIT, /EXTRACT
!                  TUT/ALIGN, /GRAP, /FIT, /MODIFY, /HELP, /AVERAGE
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!
define/param p1 + C "Enter PLOT or AUTO:"
! 
define/local qua/c/1/3 {mid$cmnd(11:13)}		!save qualifier
define/local at/c/1/2 "@ "
define/local procy/c/1/20 " " all
define/local dspgra/i/1/1 0
! 
branch qua  hel,ext,spl,tab,gra        HEL,EXT,SPL,TAB,GRA
branch qua  fil,ali,fit,mod,lut,itt,ave    FIL,ALI,FIT,MOD,LUT,ITT,AVE
write/error 5
return
!
! Tutorial/Help
HEL:
write/keyw procy tuthelp 
goto tuto_it
!
! Tutorial/Extract
EXT:
dspgra = 2				!we need a display window
write/keyw procy tutextr
goto tuto_it
!
! Tutorial/Split
SPL:
dspgra = 2				!we need a display window
write/keyw procy tutsplit
goto tuto_it
! 
! Tutorial/Table
TAB:
dspgra = 1				!we need a graphics window
write/keyw procy tuttbl
goto tuto_it
! 
! Tutorial/Graphics
GRA:
dspgra = 1				!we need a graphics window
write/keyw procy tutgrp
goto tuto_it
! 
! Tutorial/LUT
LUT:
if p1(1:1) .eq. "+" then
   dspgra = 2				!we need a display window
else
   dspgra = 3				!we need display + graphics window
endif
write/keyw procy tutlut
goto tuto_it
! 
! Tutorial/ITT
ITT:
if p1(1:1) .eq. "+" then
   dspgra = 2				!we need a display window
else
   dspgra = 3				!we need display + graphics window
endif
write/keyw procy tutitt
goto tuto_it
! 
! Tutorial/Filter
FIL:
dspgra = 2				!we need a display window
write/keyw procy tutfilt
goto tuto_it
! 
! Tutorial/Align
ALI:
dspgra = 2				!we need a display window
at(2:2) = "a"
write/keyw procy tutali
goto tuto_it
! 
! Tutorial/Fit
FIT:
dspgra = 1				!we need a graphics window
at(2:2) = "a"
write/keyw procy tutfit
goto tuto_it
! 
MOD:
dspgra = 2				!we need a display window
write/keyw procy tutmod
goto tuto_it
!
AVE:
!   we need display windows which will be created with a certain size
write/keyw procy tutave
goto tuto_it
! 
! here we actually set up the display/graphics windows if necessary + do it
! 
TUTO_IT:
if dspgra .ne. 0 then
   @ creifnot {dspgra}	!make sure, we have gr/disp window
   if  dspgra .eq. 2  p1 = "NOPLOT"
endif
! 
{at} {procy} {p1} {p2} {p3} {p4} {p5} {p6} {p7} {p8}
! 
