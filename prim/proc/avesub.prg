! @(#)avesub.prg	19.1 (ES0-DMD) 02/25/03 14:08:44
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  avesub.prg  for AVERAGE/AVERAGE, /KAPPA, /MEDIAN
! K. Banse	901108
!
! use via AVERAGE/method in_specs out_specs [out_option] [draw] [method_parms]
! 
!  in_specs = CURSOR  or  inframe,intable
!  out_specs = +                 just display data
!	     = :label            label of column for intable
!	     = table,:label      if CURSOR input used
!	     = descr             descriptor name (of inframe)
!  out_option = A, append flag for descriptor
!	      = C, center_columns flag for table
!  draw = Y or N for drawing cursor rectangle (only with CURSOR option)
!  method_parms = no. of iterations for kappa-sigma clipping
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 CURSOR C "Enter inframe,table or CURSOR: "
define/param p2 + C "Enter output specs: "
define/param p3 + C "Enter output options: "
!
!  check, if it's cursor or table input
inputi = m$parse(p1,"sub")
if inputi .gt. 1 then				!inframe,intable
   write/keyw in_a {sub01}
   write/keyw in_b {sub02}
else
   in_b(1:2) = "+ "				!means cursor input
   define/param p4 Y C "Enter draw flag - Y(es) or N(o): "
endif
!
action(1:1) = mid$cmnd(11:11)			!save qualifier
if action(1:1) .eq. "K" then
   define/param p5 1 N "Enter iteration no. for kappa-sigma clipping"
   write/keyw inputi {p5}
elseif action(1:1) .eq. "M" then
   inputi(2) = 20000                !size of internal buffer (only for Median)
endif
!
write/keyw inputc {p2}				!fill output_specs
write/keyw inputc/c/51/1 {p3}
!
run MID_EXE:AVESUB
