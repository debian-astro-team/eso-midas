! @(#)bdf2ps.prg	19.1 (ES0-DMD) 02/25/03 14:08:44
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure bdf2ps.prg to convert an image to a PostScript file
!
! K. Banse	941012, 941109, 970217
!
! execute via  
!      COPY/PSCR image PS_file options print_flags scales LUT_spec
!      @ bdf2ps image PS_file ITT_flag ? ? P_flags
!
! with  image = name of Midas image
!       PS_file = name of new Postscript file
!       P_flags  = flags like in COPY/DISPLAY
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input image: "
define/param p2 {p1}.ps C "Enter output Postscript file name: "
! 
if mid$cmnd(11:14) .eq. "PSCR" then
   define/param p3 + C "Enter options (wbneavfNE):"
   define/param p4 + C "Enter print_flags (page_size,resolution):"
   define/param p5 + C "Enter scales:"
   define/param p6 + C "Enter LUT name or no. (0,1,2,3):"
   !
   if p3(1:1) .ne. "+" then
      define/local zflags/c/1/12 -{p3}
   else
      define/local zflags/c/1/1 " "
   endif
   if p4(1:1) .eq. "+" then
      define/local zp/c/1/1 " "
      define/local zr/c/1/1 " "
   else
      inputi = m$index(p4,",")
      if inputi .lt. 1 then 
         define/local zr/c/1/1 " "
         define/local zp/c/1/6 "-p {p4}"
      else
         inputi = inputi-1
         define/local zp/c/1/6 "-p {p4(1:{inputi})}"
         inputi = inputi+2
         define/local zr/c/1/20 "-r {p4({inputi}:)}"
      endif
   endif
   if p5(1:1) .ne. "+" then
      define/local zs/c/1/60 "-s {p5}"
   else
      define/local zs/c/1/1 " "
   endif
   if p6(1:1) .ne. "+" then
      define/local zc/c/1/60 "-c {p6}"
   else
      define/local zc/c/1/1 " "
   endif
   ! 
   ! now run bdf2ps.exe with the options
   !
   define/local x/c/1/22 /util/exec/bdf2ps.exe
   $ $MIDASHOME/$MIDVERS{x} {zflags} -i {p1} -o {p2} {zp} {zr} {zc} {zs}
! 
else
   define/param p3 P C "Enter ITT-flag, Pos., Neg. or Itt: "
   ! 
   ! the following is extracted from `loadima.prg'
   ! 
   define/local sizin/i/1/2 32000,4000
   define/local cstuff/c/1/81 " " all
   define/local fixpt/i/1/4 1,1,0,0
   define/local imacuts/r/1/2 0.,0.
   ! 
   write/keyw in_a {p1}
   write/keyw default/c/1/6 NYUYNY
   write/keyw cstuff/c/1/20 "1,1 "		!scale = 1,1
   write/keyw cstuff/c/21/40 "C "		!center = C
   !
   ! load into frame middumXY.ima				!XY = Midas unit
   !
   if aux_mode .lt. 2 then
      -delete middum{mid$sess(11:12)}.ima.*	!for VMS only
   endif
   ! 
   define/local msave/c/1/20 "{mid$disp(1:20)}"
   write/keyw mid$disp F_middum{mid$sess(11:12)}.ima
   run MID_EXE:WRIMA
   write/keyw mid$disp/c/1/20 "{msave(1:20)}"
   ! 
   ! the following is extracted from `hardcopy.prg'
   ! 
   write/keyw in_a middum{mid$sess(11:12)}.ima
   run MID_EXE:PSBDF
   ! 
   -rename middum{mid$sess(11:12)}.ps {p2}
endif
