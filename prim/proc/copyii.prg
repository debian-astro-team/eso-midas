! @(#)copyii.prg	19.1 (ESO-DMD) 02/25/03 14:08:47
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! MIDAS procedure copyii.prg
! K. Banse	970729, 980121
!
! use via COPY/II source_ima dest_ima dest_fmt delete_flag,update_flag
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter source image:"
define/param p2 ? IMA "Enter destination image:"
define/param p3 R4 c "Enter destination format: "
define/param p4 n,u C "Enter delete,update flag, d/no,upd/noupd:"
! 
write/keyw in_a {p1}
write/keyw out_a {p2}
write/keyw history "COPY/II "
action(1:3) = "COP"
run MID_EXE:GENXY1
