! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure center.prg for CENTER/GAUSS, /MOMENT, /1DGAUSS
! J.D.Ponz	861128
! K. Banse	890509, 900220, 940321, 071018, 110629
! P. Ballester  910315
!
! use via CENTER/method in_specs out_specs out_option
!                       curs_option xsize,ysize zw_option invert_flag
!
!  in_specs = CURSOR  
!             or  inframe,fxpix,fypix  
!             or  inframe,intable  
!             or  inframe
!             or  GCURSOR[,color] 
!	          for one-dimensional centering via graphics cursor
! 
!  out_specs = ?      just display data
!	     = table  if output to table (may be same as intable or new one)
!	     = descr/D   descriptor name (of inframe)
! 
!  out_option = ID, for table
!		A, append flag for descriptor
!               or
!               EMISSION ABSORBTION  for GCURSOR or /1DGAUSS
! 
!  add_flags = no_of_cursors,draw_flag,maxno_curs
!              or
!	       Append, NoAppend for GCURSOR or /1DGAUSS
!  xsize,ysize = size of window in frame pixels  (used where applicable)
!  zw_option = zoom_window_flag,zoom;
!                zoom_window_flag = W for zoom_window, N for none,
!                zoom = initial zoom factor;
!  invert_flag = YES or NO, if the center is not at a Gaussian peak but a 
!                Gaussian valley, defaulted to NO
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!  
action(1:1) = mid$cmnd(11:11)
! 
if p1(1:5) .eq. "GCURS" then			!1-dim frame
   if action(1:1) .eq. "1" action(1:1) = "G"		!1DGAUSS -> GAUSS
   if action(1:1) .ne. "G" .and. mid$cmnd(11:11) .ne. "M" then
      write/out "Unknown option for GCURSOR (gravity, gauss and minmax only)"
      return/exit
   endif
  
   inputi = m$index(p1,",") 		!use a different color?
   define/local mycols/i/1/2 -1,-1
   if inputi .gt. 0 then
      mycols(2) = plistat(9)		!save original colour
      inputi = inputi + 1
      p1 = p1({inputi}:)
      inputi = m$tstno(p1)
      if inputi .eq. 1 then		!it's a no.
         mycols = {p1}
         if mycols .lt. 1 .or. mycols .gt. 8 then
            mycols = 1			!use default black instead
         endif
      else
         mycols = 1                  !default is black 
    !     in_a = m$upper(p1)
         branch p1(1:1) B,R,G,Y,M,C,W  BL,RE,GR,YE,MA,CY,WH
         goto setcol
        BL:
         if p1(1:3) .eq. "BLU" then
            mycols = 4
         endif
         goto setcol
        RE:
         mycols = 2
         goto setcol
        GR:
         mycols = 3
         goto setcol
        YE:
         mycols = 5
         goto setcol
        MA:
         mycols = 6
         goto setcol
        CY:
         mycols = 7
         goto setcol
        WH:
         mycols = 8
      endif

     setcol:
      plistat(9) = mycols
   endif
            
   define/param p2 gcursor  TAB "Enter table: "
   define/param p3 EMISSION C   "Enter type of lines: "
   define/param p4 NoAppend C   "Enter append flag: "
   run MID_EXE:centerrow

   if mycols(2) .ne. -1 plistat(9) = mycols(2)
   return

else if action(1:1) .eq. "1" then		!CENTER/1DGAUSS
   define/param p1 ?        C "Enter 1dim_image,table: "
   define/param p2 center1d TAB "Enter Table: "
   define/param p3 EMISSION C   "Enter type of lines: "
   define/param p4 NoAppend C   "Enter append flag: "
   if p3(1:1) .eq. "E" then
      action(2:2) = "N"
   else
      action(2:2) = "Y"		!invert_flag YES -> absorbtion lines
   endif
   !
   write/keyw inputc {p2}				!fill output_specs
   write/keyw inputc/c/31/2 {p4}
   ! 
   run MID_EXE:center
   return
endif
! 
!
! here for the 2-d stuff
!
!
define/param p1 CURSOR C "Enter input_specs: "
define/param p2 + C "Enter output_specs: "
define/param p3 + C "Enter out_options: "
define/param p5 50,50 N "Enter size of window in frame pixels:"
write/keyw inputi {p5},{p5}
define/param p7 NO C "Enter invert_flag, YES/NO: "
action(2:2) = p7(1:1)
! 
if p1(1:6) .eq. "CURSOR" then		!interactive input via cursor
   define/param p4 2,1,9999 C  "Enter no_of_cursors,draw_flag,maxno_curs: "
   define/param p6 + ? "Enter zoom_window_flag,zoom: "
   write/keyw cursor 2,1,9999
   write/keyw cursor {p4}
   write/keyw in_a *			!filename here
   !
   dazin(1) = -1
   if p6(1:1) .eq. "W" then
      if p6(2:2) .eq. "," then
         dazin(1) = {p6(3:)}
      else
         dazin(1) = 4                          !default to 4*zoom
      endif
      in_b(1:1) = "?"                          !force to default Xstation
   endif
endif
! 
write/keyw inputc {p2}				!fill output_specs
write/keyw inputc/c/31/2 {p3}
! 
run MID_EXE:center
 
