!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION plothst.prg
!.PURPOSE        Plot a histogram of image pixel or table values
!                execute via
!                PLOT/HISTOGRAM table col-ref [scal_x,scal_y,off_x,off_y] 
!                               [bin,[min,[max]]] [exc] [log] [opt]
!                or 
!                PLOT/HISTOGRAM image [scal_x,scal_y,off_x,off_y] [log] [opt]
!.AUTHOR         830813 J.D.Ponz
!.VERSION        860109, 900409, 910225, 920913   K. Banse
!.VERSION        870601 RHW ??
!.VERSION:       920224 RHW Offset in x and y included
! 030722	last modif
!----------------------------------------------------------------------
! 
define/local cmnd/c/1/20 "{mid$cmnd(1:20)}"          !save initial command
!
!if mid$sess(7:7) .eq. " " .or. mid$sess(7:7) .eq. "-" then
!   create/graphics
!   write/keyw mid$cmnd/c/1/20 "{cmnd(1:20)}"            !reset MID$CMND
!endif
!
branch p2(1:1) :,# tables,tables	         ! test for column reference
!
! handle images here
define/param   p1 ? IMA "Enter table or frame name:"
define/param   p3 "YY" C                             ! excess bin plotted
!
write/keyw    in_a {p1}
!
define/local  lola/i/1/1 0                  !check, if HISTOGRAM descr. there
lola = m$existd(in_a,"HISTOGRAM")
if lola .eq. 0 then
   write/out "We have to execute the command STATISTICS/IMAGE first,"
   write/out "since the descriptor HISTOGRAM does not exist"
   ! for FITS files we use keywords instead of descr.
   if m$filtyp(in_a,"?fits") .eq. 1 then
      define/local fits__histogram/i/1/256 0 all +lower
      define/local fits__hist_bins/r/1/5 0.0 all +lower
      write/out (histogram not stored as descr. in {in_a})
   endif
   statistics/image {in_a} ? #256	         ! build up histogram first
   write/keyw mid$cmnd/c/1/20 "{cmnd(1:20)}" 
endif
!
if mid$cmnd(1:1) .eq. "P" then    
   define/param p2 0.,0.,-999,-999 NUM
   @ plscoff.prg_o {P2}
   dattim = m$time()
else
   define/param p2 0.0 NUMBER
   write/keyw inputr/r/1/1 {p2}
endif
!
define/param   p4 "LIN"  C                         ! default scales
define/param   p5 0.0,-999999.,0.0 N               ! standard layout
write/keyw     inputc/c/1/2 {p3}
write/keyw     inputc/c/3/3 {p4}
write/keyw     inputr/r/2/3 {p5}
!
run MID_EXE:PLOTHBDF  
goto common_end
!
!
! handle tables
TABLES:
define/param p1 ? TBL "Enter table or frame name:"
define/param p4 "?,?,?"
define/param p5 "YY" C                             ! excess bin plotted
define/param p6 "LIN" C                            ! linear display
define/param p7 0.0,-999999.,0.0 N                 ! standard layout
!
write/keyw  in_a         {p1}
write/keyw  inputc/c/1/2 {p5}
write/keyw  inputc/c/3/3 {p6}
write/keyw  inputr/r/2/3 {p7}
!
define/local k1/i/1/1 0
define/local r1/c/1/20 "?"
define/local r2/c/1/20 "?"
define/local r3/c/1/20 "?"
define/local strike/c/1/60 {p4} 
!
k1 = m$index(strike,",")-1
if k1 .eq. -1 then
   r1 = "{strike}"
else
   r1 = "{strike(1:{k1})}"
   k1 = k1 + 2
   strike = "{strike({k1}:>)}"
   k1 = m$index(strike,",")-1
   if k1 .eq. -1 then
      r2 = "{strike}"
   else
      r2 = "{strike(1:{k1})}"
      k1 = k1 + 2
      r3 = "{strike({k1}:>)}"
   endif
endif
!
if mid$cmnd(1:1) .eq. "P" then
   define/param p3 0.,0.,-999,-999 NUM
   @ plscoff.prg_o {P3}
   dattim = m$time()
else
   define/param p3 0.0 NUMBER
   write/keyw inputr/r/1/1 {p3}
endif
!
@ t2stattbl {p1} {p2} {r1} {r2} {r3}
run MID_EXE:PLOTHTBL
! 
! 
common_end:
write/keyw plcdata/c/1/60  "{p1},HISTOGRAM"
write/keyw plcdata/c/61/20 "DESCRIPTOR"
copy/graph

