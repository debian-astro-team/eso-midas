! @(#)asclut.prg	19.1 (ES0-DMD) 02/25/03 14:08:43
! ++++++++++++++
!  
!  Midas procedure  asclut.prg
!  convert ASCII file xyz.lasc to LUT file (table format) of host system
!  via CREATE/TABLE 
!  xyz.lut  <-  xyz.lasc
! 
!  K. Banse     ESO - Garching   890116, 890626, 910204, 93706, 980928
!  C. Guirao    ESO - Garching   890526: Changing protections flags
!
! ++++++++++++++
! 
create/table {p1}.lut 3 256 {p1}.lasc 
name/column {p1}.lut :lab001 :red
name/column {p1}.lut :lab002 :green
name/column {p1}.lut :lab003 :blue
! 
if aux_mode(1) .le. 1 then		! VMS
   $ SET PROT=(G:RW,W:RW) {p1}.lut
else					! UNIX
   $ chmod a+rw {p1}.lut
endif
write/out table {p1}.lut converted ...
