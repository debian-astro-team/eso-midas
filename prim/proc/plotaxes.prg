! @(#)plotaxes.prg	19.2 (ESO-DMD) 06/05/03 11:32:27
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLOTAXES.PRG
!.PURPOSE:        MIDAS procedure to plot box with coordinate ticks along the 
!                 x and y axes.
!                 par1 = x-axis coordinate string
!                 par2 = y-axis coordinate string
!                 par3 = sc_x,sc_y,off_x,off_y (defaults device filling)
!                 par4 = label along the X-axis
!                 par5 = label along the Y-axis
!.VERSION:        881005  RHW creation
!                 901108  KB  {','} business ...
!                 920224  RHW Offset in x and y included
!                 940201  RHW For display plotting: p8 contains loaded image
!		  990505  RHW For display plotting: default labels from CUNIT
!-------------------------------------------------------------------------
! 
IF MID$PLOT(1:5) .EQ. "IMAGE" THEN                  ! here for display window
   DEFINE/LOCAL NK1/I/1/1 0
   SET/FORMAT I1
   define/param p8 * ima
   copy/keyw p8 in_a
   IF in_a .NE. " " THEN
      define/param p1 [{idimemr(1)},{idimemr(2)}:{idimemr(3)},{idimemr(4)}]
      IF P1(1:1) .EQ. "[" THEN
         DEFINE/PARAM P1 [<,<:>,>]
         DEFINE/PARAM P2 " " C
         DEFINE/PARAM P3 " " C
         DEFINE/LOCAL LABELX/C/1/80 {P2}
         DEFINE/LOCAL LABELY/C/1/40 {P3}
         GOTO FINISH
      ENDIF
   ENDIF
ENDIF
!
DEFINE/PARAM P3 0.,0.,-999,-999 NUM
@ plscoff.prg_o {P3}                            ! get the scales and offsets
DEFINE/PARAM   P4 "" C
DEFINE/PARAM   P5 "" C
DEFINE/LOCAL LABELX/C/1/80 {P4}
DEFINE/LOCAL LABELY/C/1/80 {P5}
!
FINISH:
IF MID$CMND(1:1) .EQ. "P" THEN
   IF PLRSTAT(1) .EQ. 0 .AND. PLRSTAT(2) .EQ. 0 THEN     ! x axis manually set?
      DEFINE/PARAM P1 0.0,1.0,0.25,0.05 C
   ELSE
      DEFINE/PARAM P1 {PLRSTAT(1)},{PLRSTAT(2)},{PLRSTAT(3)},{PLRSTAT(4)} C
   ENDIF
   IF PLRSTAT(5) .EQ. 0 .AND. PLRSTAT(6) .EQ. 0 THEN     ! y axis manually set?
      DEFINE/PARAM P2 0.0,1.0,0.25,0.05 C
   ELSE
      DEFINE/PARAM P2 {PLRSTAT(5)},{PLRSTAT(6)},{PLRSTAT(7)},{PLRSTAT(8)} C
   ENDIF
ELSE                                                     ! over plot mode
   DEFINE/PARAM P1 {PLRSTAT(1)},{PLRSTAT(2)},{PLRSTAT(3)},{PLRSTAT(4)} C
   DEFINE/PARAM P2 {PLRSTAT(5)},{PLRSTAT(6)},{PLRSTAT(7)},{PLRSTAT(8)} C
ENDIF
!
! *** all done: now do the job
RUN MID_EXE:PLOTAXES
WRITE/KEYW    PLCDATA/C/1/60   "unknown"
WRITE/KEYW    PLCDATA/C/61/20  "unknown"
copy/graph
