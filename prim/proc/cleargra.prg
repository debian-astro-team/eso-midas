! @(#)cleargra.prg	19.1 (ESO-DMD) 02/25/03 14:08:46
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT: 	cleargra.prg 
!.PURPOSE:	Clear image and graphics display (CLEAR/DISPL, /GRAPH, /TEXT)
!.Author:	K. Banse  890703
!.Version:	931019 RHW Change of graphics keywords 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
if mid$cmnd(11:11) .eq. "D" then
   if mid$disp(1:1) .eq. "I" then
      action(1:4) = "MESX"
      run MID_EXE:idauxx
   ENDIF
!
else
    run MID_EXE:cleargra			! clear graphics device
    write/keyw plrgrap/r/31/1 0.0
    write/keyw plcdata/c/1/60 "unknown" ALL      ! clear data file in PLCDATA
    write/keyw plcdata/c/61/20 "unknown" ALL     ! clear data type in PLCDATA
    write/keyw plcmeta/c/1/80 "none"             ! clear meta file name
endif
