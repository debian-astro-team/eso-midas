! @(#)plttbl3.prg	19.1 (ES0-DMD) 02/25/03 14:08:58
! @(#)plttbl3.prg	1.1 (ESO-IPG) 10/29/93 15:18:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLTDEMO.PRG
!.PURPOSE:        Demonstration procedure for the graphics package, 3-D. 
!.AUTHOR:         Richar van Hees  ESO - Garching Oct. 29 , 1993
!.USE:            Execute as TUTORIAL/GRAPHICS
!.NOTE:           The following files should exist: array.tbl in MID_TEST
!.VERSION:        931029 Creation RvH
! -----------------------------------------------------------------------
!                        Here for the table stuff
!
write/out "A 3-D table is copied to your directory (with artificial data)"
indisk/fits MID_TEST:array.tfits exampl.tbl >Null
select/table exampl all
show/table exampl
wait/secs 3
!
write/out "Two planes in the table will be plotted in the default plot setup."
set/grap default                                      ! default graphics setup
! create/graph 0 600,450,0,325                          ! for the SONY
create/graph 0 600,450,0,450                            ! for the SUN
write/out "   PLOT/TABLE EXAMPL #3 #2 " 
plot/table exampl #3 #2
wait/secs 5
!
write/out " " 
write/out "MIDAS can do better; let's make use of PLOT/TABLE facilities"
write/out "   PLOT/TABLE EXAMPL #3 #2 ? 3,5,7 1,2"
plot/table exampl #3 #2 ? 3,5,7 1,2
wait/secs 5
!
write/out " " 
write/out "We have connected the data points found in each column,"
write/out "but we can also connect data points found in the depth of the table"
write/out "   PLOT/TABLE EXAMPL #3 #2 ? 3,5,7 1,2 O,O "
plot/table exampl #3 #2 ? 3,5,7 1,2 O,O
wait/secs 5
!
write/out " " 
write/out "We do not have to take all the array elements..."
write/out "   PLOT/TABLE EXAMPL #3 #2[2..4] ? 3,5,7 1,2 O,O "
plot/table exampl #3 #2[2..4] ? 3,5,7 1,2 O,O
wait/secs 5
!
write/out " " 
write/out "Or we can take a vector along a column as abscissa."
write/out "Note the direction flags!"
write/out "   PLOT/TABLE EXAMPL #3[3] #2 ? 3,5,7 1,2"
plot/table exampl #3[3] #2 ? 3,5,7 1,2
wait/secs 5
!
write/out " " 
write/out "Or take a vector along a row as abscissa"
write/out "   PLOT/TABLE EXAMPL @8[3] #2 ? 3,5,7 1,2 O,O"
write/out "Oops, there are NULL values in first column; get dimensions right"
write/out "   PLOT/TABLE EXAMPL @8#2..5[3] #2[1..4] ? 3,5,7 1,2 O,O"
write/out "Note that the units are not the same along a row"
plot/table exampl @8#2..5[3] #2[1..4] ? 3,5,7 1,2 o,o
write/out "Try to understand what is going on"
wait/secs 7
!
set/grap
IF AUX_MODE(1) .EQ. 1 THEN
   -delete exampl.tbl.
ELSE
   -delete exampl.tbl
ENDIF
wait/secs 10
delete/grap
