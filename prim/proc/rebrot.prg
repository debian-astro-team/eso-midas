! @(#)rebrot.prg	19.1 (ES0-DMD) 02/25/03 14:08:59
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure rebrot.prg to implement REBIN/ROTATE
! K. Banse	910204, 920401, 940504
!
! execute as REBIN/ROTATE inframe outframe rot_specs frame refflag
!
! or         REBIN/ROTATE inframe outframe KEYWORD frame refflag
!     indicating that parameters should be taken from keyword TRANSFRM
!
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ?     IMA "Enter input frame: "
define/param p2 ?     IMA "Enter output frame: "
define/param p3 45.,C,C,1.,1. ?  "Enter rotation_specs or KEYWORD: "
define/param p4 + IMA "Enter reference : "
define/param p5 NO ?  "Enter ref_flag: "
define/maxpar 5
!
write/keyw in_a {p1}
write/keyw out_a {p2}
!
if p2(1:5) .eq. "KEYWO" then
   write/out "It looks like you forgot to enter the output frame..."
   return
endif
!
write/keyw history "REBIN/ROTATE "
! 
if p3(1:3) .eq. "KEY" then
   inputc(1:1) = "K"
   write/keyw in_b {p4}
   action(1:1) = "R"			!tell ALIGN that we rotate
   run MID_EXE:ALIGN
else
   write/keyw inputc {p3},~		! "~" marks the end ...
   write/keyw in_b {p4}
   action(1:2) = "RO"
   run MID_EXE:GENYY1
endif
! 
if p5(1:1) .eq. "Y" .and. p4(1:1) .ne. "+" then
   create/image middumna = {in_b} poly {null(2)}
   insert/image {p2} middumna >Null
   cuts/image middumna {in_b}
   rename/image middumna {p2}
endif
