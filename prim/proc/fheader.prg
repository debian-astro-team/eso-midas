! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  fheader.prg for GET/FHEADER, GET/FEXTENS
! K. Banse	021115, 090119
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? I "Enter FITS file:"
write/keyw in_a {p1}
!
! GET/FHEADER FITSfile
if mid$cmnd(11:12) .ne. "FE" then
   define/param p2 + C "Enter viewer (less/vi/emacs/xedit/...):"
   !
   write/keyw in_b ?
   outputi = -7
   run MID_EXE:fheader
   ! 
   if outputi .eq. 0 then
      if p2(1:1) .ne. "+" then		!we look at the header right away
         ${p2} {out_a} 
      endif
   endif
! 
! GET/FEXTENS FITSfile extname
else
   write/keyw in_b {p2}
   outputi = -999
   run MID_EXE:fheader
endif

