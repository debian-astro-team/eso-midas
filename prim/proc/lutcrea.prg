! @(#)lutcrea.prg	19.1 (ES0-DMD) 02/25/03 14:08:54
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure lutcrea.prg to implement CREATE/LUT
! K. Banse	901108
!
! execute as CREATE/LUT LUT_name H_specs S_specs I_specs cyclic_flag
! LUT_name - name of optional LUT table, defaulted to ? which means no table
! H_specs - start,end,increments for hue, defaulted to 0.0,360.,-1.
! S_specs - start,end,increments for saturation, defaulted to 0.0,1.-1.
! I_specs - start,end,increments for intensity, defaulted to 0.0,1.-1.
!         if increments < 0. increment is set to (end-start)/255.
! cyclic_flag = C or N
! or
! as CREATE/LUT LUT_name CURSOR [start_LUT] [new_LUT]
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 + C "Enter name of new LUT:"
!
if p2(1:4) .eq. "CURS" goto cursor_lut
!
define/param p2 0.0,360.,-1. N "Enter start,end,increment for hue:"
define/param p3 0.0,1.0,-1. N "Enter start,end,increment for saturation:"
define/param p4 0.0,1.0,-1. N "Enter start,end,increment for intensity:"
define/param p5 C ? "Enter Cyclic or No: "
define/maxpar 5
!
write/keyw history "CREATE/LUT "
!
write/keyw inputr/r/1/3 {p4}
write/keyw inputr/r/4/3 {p3}
write/keyw inputr/r/7/3 {p2}
if p5(1:1) .eq. "C" then
   inputr(10) = 1.0
else
   inputr(10) = -1.0
endif
!
action = "HU"
run MID_EXE:IDFUNC
return
!
CURSOR_LUT:
if ididev(18) .eq. 11 then
   write/out CURSOR option not supported for X11 displays...
   return/exit
endif
! 
define/param p3 + C "Enter LUT used as starting LUT:"
define/param p4 ? C "Enter LUT used via cursor input:"
define/maxpar 4
!
if m$exist("lutdisp.bdf") .le. 0 -
   create/image lutdisp 2,512,20 ? POLY 0.,1.
!
set/split 1,2
display/chan 1 1				!channel 1 with LUTsection 1
load/image lutdisp 1
!
if p3(1:1) .ne. "+" load/lut {p3} 0,256
display/chan 2 0				!channel 2 with LUTsection 0
display/lut
!
write/keyw in_a {p1}				!save new LUT name
set/cursor 0 PROG
!
LOOP:
load/lut {p4} 1,256
p1(1:1) = "C"
default(16:16) = "C"
run MID_EXE:LUTMOD
!
inquire/keyw p4 "Enter new LUT for input or hit RETURN to exit:"
if aux_mode(7) .ne. 0 goto loop
!
!  now we pull out the new LUT
display/chan 2 0
get/lut {in_a}
