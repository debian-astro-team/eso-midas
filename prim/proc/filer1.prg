! @(#)filer1.prg	19.1 (ES0-DMD) 02/25/03 14:08:51
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure filer1.prg  to implement DELETE/IMA, TABLE, FIT, TEMP
! K.Banse       901108, 910920, 920214, 930706, 941104, 941215
! M.Peron       890118
! use via DELETE/IMA, /TABLE, /FIT   frame   conf_flag
!      or DELETE/TEMP 
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local myact/c/1/2 {mid$cmnd(11:12)}	!save type
define/local dflag/c/1/1 N
define/local subcom/c/1/10 subtr/{myact(1:1)}cat
define/local file/c/1/60 " " ALL
define/local k/i/1/1 0
define/local j/i/1/1 0
!
if aux_mode .lt. 2 then
   define/local aux/c/1/2  ".*"			!VMS
   define/local dirend/c/1/1 "]" 
else
   define/local aux/c/1/2  "  "			!Unix
   define/local dirend/c/1/1 "/" 
endif
! 
! branch according to file type
!
branch myact IM,TA,FI,TE D_IMA,D_TAB,D_FIT,D_TEMP
!
! invalid qualifier...
write/error 3
return/exit
! 
D_TEMP:						!default to DELETE/TEMP
-DELETE middumm*{aux}{aux}			!no confirmation here...
-DELETE midtemp*{aux}{aux}
-DELETE FITS*{aux}{aux}
return
!
D_IMA:
define/param p1 ? IMA "enter image name: "
define/param p2 CONF C "Enter confirm_flag - C(onf)/N(oconf): "
define/local filtyp/c/1/4 .bdf
k = 6
goto next
!
D_TAB:
define/param p1 ? T "enter table name: "
define/param p2 CONF C "Enter confirm_flag - C(onf)/N(oconf): "
k = 8
define/local filtyp/c/1/4 .tbl
goto next
!
D_FIT:
define/param p1 ? F "enter fit file name: "
define/param p2 CONF C "Enter confirm_flag - C(onf)/N(oconf): "
k = 9
define/local filtyp/c/1/4 .fit
!
NEXT:
write/keyw in_a {p1}
!
! first delete in host system then subtract entry
! 
j = m$indexb(in_a,dirend)+1
if m$index(in_a({j(1)}:),".") .le. 0 then	!check, if already file type
   write/keyw file {in_a}{filtyp}
else
   write/keyw file {in_a}
endif
! 
if p2(1:1) .ne. "N" then
   dflag = "N"
   inquire/keyw dflag "Delete {file}? "
else
   dflag = "Y"
endif
! 
if dflag .eq. "Y" -DELETE {file}{aux}
!
!  if we have a catalog, remove the corresponding entry
! 
if catalinf({k}) .gt. 0 then
   k = m$index(file,"*")		!look for wild card specs in file name
   if k .gt. 0 then
      write/out "no catalog processing with wild card filenames..."
   else
      if dflag .eq. "Y" {subcom} ? {file} 	!update active catalog
   endif
endif
!
!  return "Y" in keyword Q1, if really deleted
return {dflag}
