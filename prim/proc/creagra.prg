! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  creagra.prg 
!  K. Banse   ESO/IPG - Garching	910102
! 
! use as @ creagra  graphics_id  xdim,ydim,xoff,yoff  graph_size  Xstation_name
! or 
!        @ creagra  graphics_id  -999       for switching graphic displays
! 
!.VERSION
! 030731	last modif
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! 
if p2 .eq. "-999" then			 !here for switching displays
   define/param p1 0 n "Enter graphics id: "  0,9
   action(6:7) = "GS"
   run MID_EXE:wndcrea.exe
   dazdevr(11) = {p1}
   return
else
   define/param p4 default ? "Enter name of XWindowScreen: "
   write/keyw dazdevc {p4}
endif
!
!  test, if it will be a shadow graphics window
if p2(1:1) .eq. "S" then
   define/local kk/i/1/1 0
   if p1(1:1) .lt. "A" .or. p1(1:1) .gt. "Z" then
      write/error 100 "Par. 1 bad (is not in [a,z])"
      return/exit
   else
      kk = m$index(p2,",")+1
      if kk .le. 1 then
         write/error 5 "Par. 2 bad (= {p2})"            !invalid syntax
         return/exit
      endif
   endif
   !
   action(6:8) = "GP{p2({kk}:{kk})}"
   run MID_EXE:wndcrea.exe
   return
endif
!
!  now for create/graphics		use INPUTI(1 - 11), (8) spare
! 
define/param p1 0 n "Enter graphics id: "  0,9
define/param p2 625,425,0,469 N "Enter xdim,ydim,xoff,yoff of graphics window: "
! 
write/keyw inputi 625,425,0,469
write/keyw inputi {p2}
if inputi(1) .le. 0 .or. inputi(2) .le. 0 then
   write/out Bad dimensions for graphics window...
   return
endif
! 
define/param p3 100000 N "Enter no. of graph segments: "  0,900000
write/keyw inputi/i/5/7 1,{inputi(1)},{inputi(2)},-1,0,0,{p3}
! 
if ididev .lt. 0 @ idistart.prg_o		!start up IDI server
action(6:7) = "GC"
run MID_EXE:wndcrea.exe		!the server is supposed to be up already !!
dazdevr(11) = {p1}
! 
! assign also the plotting stuff to new window (as in ASSIGN/PLOT)
write/keyw mid$plot/c/1/20  GRAPH_WND{p1} 
write/keyw mid$plot/C/21/10 "NOSPOOL"		!not a spooled device
! 
inputi(7) = dazdevr(11) + 11
winopen({inputi(7)}) = 1			!set WINOPEN 


