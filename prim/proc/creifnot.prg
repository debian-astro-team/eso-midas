! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure creifnot.prg to create display/graphics window
! if not already there
! K. Banse	910425, 940517, 030812
! 
! use via  @ creifnot flag [LUT]
! with flag = 1 for graphics window
!           = 2 for display window
!           = 3 for graphics + display window
!      for flag = 2/3 optional; LUT name as 2. par.
!  
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 2 N "Enter flag for desired window: "
define/param p2 rainbow ? "Enter LUT: "
! 
if {p1} .eq. 2 then
   if dazdevr(10) .lt. 0 then
      create/display
      if p2(1:1) .ne. "+" load/lut {p2}
   endif
! 
elseif {p1} .eq. 1 then
   if dazdevr(11) .lt. 0 create/graph
! 
elseif {p1} .eq. 3 then
   if dazdevr(10) .lt. 0 then
      create/display
      if p2(1:1) .ne. "+" load/lut {p2}
   endif
   if dazdevr(11) .lt. 0 create/graph
endif
