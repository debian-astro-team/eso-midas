! @(#)overerr.prg	19.1 (ESO-DMD) 02/25/03 14:08:55
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: OVERERR.PRG
!.PURPOSE: MIDAS procedute to overplot error bars contained in a table
!.USE:     executed via OVERPLOT/ERROR table [col1 [col2]] col3 [bar]
!          where
!          table: MIDAS table
!          col1:  column 1 of the table (x-coordinates)
!          col2:  column 2 of the table (y-coordinates)
!          col3:  the error column of the table
!          bar:   integer number indicating the type of error bar
!.AUTHOR:  Rein H. Warmels  ESO - Garching    
!.VERSION: 861203 RHW creation
! ----------------------------------------------------------------------
! 
define/param p1 ? table "Enter table name:"
define/param p5 6 number
define/param p6 y c
write/keyw inputi/i/1/1 {p5}			! orientation of error bar
write/keyw inputc/c/1/1 {p6}			! cross bar at the end
!
run MID_EXE:overerr
copy/graph
