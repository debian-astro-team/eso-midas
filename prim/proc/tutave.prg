!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! .COPYRIGHT (c) European Southern Observatory
!.IDENT     tutave.prg
!.AUTHOR    P.Nass - ESO
!.KEYWORDS  tutorial, demo, test, average
!.PURPOSE
!      MIDAS procedure to demonstrate how 
!                 average/col 
!                 average/row 
!      work
!
!.USAGE Execute as TUTORIAL/AVERAGE 
!.VERSION      1.0  13-April-1999
!              1.1  14-July-1999
!
reset/display >Null
write/out **********************************************************************
write/out This is a tutorial to show you how average/column and average/row work
write/out 
write/out       MIDAS command lines will start with:       Midas>
write/out       and have to be confirmed by hitting RETURN
write/out       
write/out       Comments are surrounded by:                *** comment ***
write/out
write/out *********************************************************************
!
write/out Please drag your window with your MIDAS session 
write/out into the lower right corner and hit RETURN once you're ready
inquire/key inputi "Ready? (hit RETURN):"
!
! create a display to get the monitor parameters
! create/display 0 100,100,0,0
! label/display "Tutorial: average" 0.5,0 p5=1
!
! write/out Let us start with a display
! calculate the position according to monitor settings
create/dis 1 512,512,0,360 p4=no
load/lut smooth2
!
write/out
write/out "Please confirm every MIDAS command line you see with RETURN "
write/out "to see what happens during a MIDAS session "
write/out
write/out "*** Create an image called twodim with the MIDAS command: ***"
INQUIRE/KEY inputi "Midas> create/image twodim 2,4,2"
create/image twodim 2,4,2
!
write/out
write/out "*** After its creation the image was filled with different values***"
write/out "*** Now load this image: ***"
write/image twodim @1,@1,1 2 
write/image twodim @1,@2,1 -9 
write/image twodim @2,@1,1 3 
write/image twodim @2,@2,1 -5 
write/image twodim @3,@1,1 6 
write/image twodim @3,@2,1 -4 
write/image twodim @4,@1,1 7 
write/image twodim @4,@2,1 -8 
!
INQUIRE/KEY INPUTI "Midas> load/image twodim scale=50 cuts=-10,10"
load/image twodim scale=50 cuts=-10,10
!
label/display "Our image twodim.bdf" 0.8,0.1 p5=1 white
label/display "contains these values:" 0.7,0.1 p5=1 white
! careful, if you use label/display, because x and y are here y,x
label/display "2" @1,@1,f p5=1 white
label/display "-9" @2,@1,f p5=1 white
label/display "3" @1,@2,f p5=1 white
label/display "-5" @2,@2,f p5=1 white
label/display "6" @1,@3,f p5=1 white
label/display "-4" @2,@3,f p5=1 white
label/display "7" @1,@4,f p5=1 white
label/display "-8" @2,@4,f p5=1 white
!
label/display "columns ----->" 0.25,0.3 p5=1 white
label/display "No. 1 " 0.3,0.3 p5=1 white
label/display "No. 2 " 0.3,0.4 p5=1 white
label/display "No. 3 " 0.3,0.5 p5=1 white
label/display "No. 4 " 0.3,0.6 p5=1 white
!
label/display "rows" 0.5,0.05 p5=1 white
label/display "|" 0.4,0.15 p5=1 white
label/display "|" 0.45,0.15 p5=1 white
label/display "|" 0.5,0.15 p5=1 white
label/display "|" 0.55,0.15 p5=1 white
label/display "^" 0.6,0.14 p5=1 white
label/display " No. 1 " 0.4,0.15 p5=1 white
label/display " No. 2 " 0.5,0.15 p5=1 white
!
write/out
write/out "*** average/row: ***"
!
INQUIRE/KEY INPUTI "Midas> average/row onedim_avrow = twodim"
average/row onedim_avrow = twodim 
!
write/out "***Display 2 shows the result of the MIDAS command average/row***"
create/display 2 512,256,0,0 p4=no
load/image onedim_avrow scale=50 cuts=-10,10
!label/display "Result of" 0.9,0.1 p5=1 white
label/display "Midas> average/row onedim_avrow = twodim" 0.8,0.1 p5=1 white
label/display "-3.5" 0.55,0.30 p5=1 white
label/display "-1"  0.55,0.45 p5=1 white
label/display "1"   0.55,0.55 p5=1 white
label/display "0.5" 0.55,0.65 p5=1 white
label/display "The result is the average over all rows in one column" 0.25,0.1 p5=1 white
!
write/out
write/out "*** average/col: ***"
!
INQUIRE/KEY INPUTI "Midas> average/column onedim_avcol = twodim"
average/column onedim_avcol = twodim
!
write/out "***Display 3 shows the result of the MIDAS command average/column***"
create/display 3 512,316,522,440 p4=no
!
create/image dummy 2,1,2
write/image dummy @1,@1,1 5 
write/image dummy @1,@2,1 6 
label/display "Midas> average/column onedim_avcol = twodim" 0.8,0.1 p5=1 white
label/display "The result is the average over all columns in one row" 0.25,0.1 p5=1 white
load/image dummy scale=50 cuts=-10,10
label/display "4.5" 0.35,0.50 p5=1 white
label/display "-6.5" 0.55,0.50 p5=1 white
label/display "and would in principle look like this"  0.20,0.1 p5=1 white
wait/secs 3
! 
label/display "However:" 0.20,0.75 p5=1 white
label/display "In MIDAS, one-dimensional images"  0.15,0.1 p5=1 white
label/display "are aligned along the x-axis." 0.10,0.1 p5=1 white
!
write/out
write/out "*** To see the real result of average/column load the image: ***"
!
INQUIRE/KEY INPUTI "Midas> load/image onedim_avcol scale=50 cuts=-10,10"
load/image onedim_avcol scale=50 cuts=-10,10
label/display "and would in principle look like this"  0.20,0.1 p5=1 black
label/display "However:" 0.20,0.75 p5=1 black
label/display "4.5" 0.35,0.50 p5=1 black 
label/display "Therefore, the result looks like this" 0.7,0.05 p5=1 white
label/display "4.5" 0.55,0.45 p5=1 white
!
!write/out "*** and load the real output image: ***"
!INQUIRE/KEY INPUTI "Midas> load/image onedim_avcol scale=50 cuts=-10,10"
!load/image onedim_avcol scale=50 cuts=-10,10
!
write/out "End of TUTORIAL/AVERAGE"
