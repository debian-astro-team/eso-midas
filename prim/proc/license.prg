! @(#)license.prg	19.1 (ES0-DMD) 02/25/03 14:08:53
! @(#)license.prg	19.1 (ESO-DMD) 02/25/03 14:08:53
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: license.prg
!.PURPOSE: MIDAS procedure showing the license
!.USE:     executed via @ license c 
!                       @ license w
!.AUTHOR:  R.H. Warmels  ESO - Garching 
!.VERSION: 960112 RHW  data of creation
!-------------------------------------------------------------------------
IF P1 .EQ. "W" THEN
   write/out MID_HELP:midas_license.txt Para war

ELSEIF P1 .EQ. "C" THEN
   write/out MID_HELP:midas_license.txt Para con
   write/out MID_HELP:midas_license.txt Para war

ELSEIF P1 .EQ. "P" THEN
   write/out MID_HELP:midas_license.txt Para pre

ELSE 
   write/out MID_HELP:midas_license.txt Para pre
   write/out MID_HELP:midas_license.txt Para con
   write/out MID_HELP:midas_license.txt Para how
ENDIF
