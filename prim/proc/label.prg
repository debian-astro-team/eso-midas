! @(#)label.prg	19.1 (ESO-DMD) 02/25/03 14:08:53
! ++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: LABEL.PRG
!.PURPOSE:        write a text (80 char. max.) on a graphic device
!.USE             execute as LABEL par1 par2 par3 par4 par5, where: 
!                 par1 = "text to write"
!                 par2 = text angle
!                 par3 = character expansion factor
!                 par4 = position in world units
!                 par5 = centering parameter
!.AUTHOR:         Ch. Ounnas   820705
!.VERSION         K. Banse     860911  ???
!                 R.H. Warmels 880303  centering par. added, free angle and size
!------------------------------------------------------------------
!
DEFINE/PARAM P1 " " C
DEFINE/PARAM P2 " " C
DEFINE/PARAM P3 0 N
DEFINE/PARAM P4 1 N
DEFINE/PARAM P5 0 N
!
WRITE/KEYW INPUTC/C/1/80 {P1(1:80)}
WRITE/KEYW INPUTR/R/1/1 {P3}
WRITE/KEYW INPUTR/R/2/1 {P4}
WRITE/KEYW INPUTI/I/1/1 {P5}
!
RUN MID_EXE:LABEL
