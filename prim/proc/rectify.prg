! @(#)rectify.prg	19.1 (ESO-DMD) 02/25/03 14:09:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS command procedure RECTIFY to implement RECTIFY /IAGE, SPECTRUM
! D. Baade 	850813		ST-ECF
!               870520, 010618  KB
!
! use via   RECTIFY/IMAGE, SPECTRUM
!
! Drive MIDAS modules REGRESSION/POLYNOMIAL, pass coefficients
! to MIDAS module RECTIMAG which does the rebinning to geometrically
! rectify the input frame
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter first image:"
DEFINE/PARAM P2 ? IMA "Enter second image:"
DEFINE/PARAM P3 ? TABLE "Enter table:"
DEFINE/PARAM P4 1 N
DEFINE/PARAM P5 N			
!
DEFINE/LOCAL MYACT/C/1/2 {MID$CMND(11:12)}	!save qualifier...
!
DEFINE/LOCAL ERRSAV/I/1/1 {ERROR(3)}	!disable errors in regression...
ERROR(3) = -1			
!
! do the regression analysis in x
!
REGRESSION/POLY {P3} #1 #3,#4 3,3
! copy key OUTPUTD to hold coefficients
COPY/KK OUTPUTD COEFFX
!
! and now same thing for y
!
REGRESSION/POLY {P3} #2 #3,#4 3,3
! copy key OUTPUTD to hold coefficients
COPY/KK OUTPUTD COEFFY
ERROR(3) = ERRSAV				!reset key ERROR
!
!  fill relevant keywords
!
WRITE/KEYW IN_A {P1}
WRITE/KEYW OUT_A {P2}
WRITE/KEYW INPUTI/I/1/1 {P4}
IF P5(1:1) .EQ. "Y" THEN
   INPUTR(1) = 3.				!SUBFACTOR in RECTIMAG/RECTSPEC
ELSE
   INPUTR(1) = 1.
ENDIF
!
IF MYACT(1:2) .EQ. "IM" THEN
   RUN APP_EXE:RECTIMAG				!RECTIFY/IMAGE
ELSE
   RUN APP_EXE:RECTSPEC				!RECTIFY/SPECTRUM
ENDIF
