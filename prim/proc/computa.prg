! @(#)computa.prg	19.1 (ES0-DMD) 02/25/03 14:08:46
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  computa.prg  to implement COMPUTE/ROW, /COL
! K. Banse	, 910614, 920331, 920506, 921221, 930105
!
! execute via COMPUTE/qualif [out_frame =] expression
!
! if out_frame and = are omitted, the command works like a pocket calculator
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! handle COMPUTE/ROW  frame.row = expression
!
write/keyw history "COMPUTE/{mid$cmnd(11:14)}"
if mid$cmnd(11:12) .eq. "RO" then
   define/param p1 ? ? "Enter output_spec as frame.resrow:"
   default(16:16) = "R"
!
! handle COMPUTE/COLUMN  frame.column = expression
!
else
   define/param p1 ? ? "Enter output_spec as frame.rescolumn:"
   default(16:16) = "C"
endif
!
if pcount .lt. 3 then
   write/error 5
else
   run MID_EXE:RARTHM
endif
