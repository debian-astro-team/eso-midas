! @(#)context.prg	19.1 (ESO-DMD) 02/25/03 14:08:47
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS system procedure 'context.prg'  for loading/clearing a context 
!  to be run when the command "SET(CLEAR)/CONTEXT xxx" is given
!  K. Banse	881024, 901213, 911009, 921002, 930224, 990116, 000713
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? c "Enter action:"
define/param p2 ? c "Enter context:"
define/param p3 ? c "Enter directory:"
! 
if p3(1:1) .ne. "?" then		!we also pass context directory
   define/local fullfile/c/1/200 {p3}{p2}
   if p1 .eq. "add" then
      write/keyw in_a ^{p2}
      run MID_MONIT:helper 
   endif
else
   define/local fullfile/c/1/20 {p2}
endif
error(3) = -1				!do not abort on errors...
! 
! add a context
! 
if p1 .eq. "add" then
   if m$exist(fullfile) .eq. 1 then
      @@ {fullfile}
      if progstat(1) .ne. 0 then
         write/out Problems with context {fullfile} ...
         define/local kcom/c/1/14 "clear/context " 	!undo context stuff...
      else
         define/local kcom/c/1/14 "wait/secs 0   " 	!keep context stuff...
      endif
   else
      write/out Could not find context file {fullfile} ...
      define/local kcom/c/1/14 "clear/context " 	!undo context stuff...
   endif
   !
   set/context -1			!decrement active context counter
   {kcom}				!if error clear/context
!
! remove a context
! 
else
   error(4) = 0
   @@ {fullfile},clear			!call the context with entry "clear"
   error(4) = 1
endif
! 
progstat = 0					!clear eventual error status
error(3) = 0
