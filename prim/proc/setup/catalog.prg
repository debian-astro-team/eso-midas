! @(#)catalog.prg	19.1 (ESO-DMD) 02/25/03 14:11:02
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!                                                                       
!  MIDAS setup procedure   catalog.prg   in MID_SETUP
!  for implementing the commands  "xxxx/SETUP catalog"                   
!  K. Banse	901203, 921218, 940421
!                                                                       
!  this procedure must have the 4 entries: WRIT, READ, INIT and INFO
!  to support the commands WRITE, READ, INIT and INFO/SETUP       
!                                                                       
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
! 
entry WRIT  				!WRITE/SETUP
! --------
! 
define/param p1 1,4 N -
   "Enter first,last index for extraction of part of input name:"
define/param p2 1 N "Enter method for building of output name:"
define/param p3 N C "Enter delete flag - Y/N for input files after processing:"
define/param p4 0,999999 N -
   "Enter low,high limit for input catalog entry numbers:"
define/param p5 0 N "Enter no. of the parameter serving as output catalog: "
define/param p6 xyz C "Enter string for keyword CATLC:"
! 
write/keyw catli/i/1/7 1,4,1,0,0,999999,0 
write/keyw catli/i/1/2 {p1},{p1}
write/keyw catli/i/3/1 {p2}
! 
if p3(1:1) .eq. "Y" catli(4) = 1
write/keyw catli/i/5/2 {p4}
write/keyw catli/i/7/1 {p5}
! 
write/keyw catlc/c/1/20 " " all
write/keyw catlc {p6}
! 
if catli(3) .eq. 5 then			!we use catalog with output names
   if m$index(catlc,".cat") .lt. 2 write/keyw catlc {catlc}.cat
endif
! 
entry READ   				!READ/SETUP
! --------
! 
if m$existk("catli") .ne. 1 then
   write/keyw catli/i/1/7 1,4,1,0,0,999999,0 
endif
if m$existk("catlc") .ne. 1 then
   write/keyw catlc/c/1/20 "xyz     " 
endif
! 
write/out "Current Setup for EXECUTE/CATALOG command:"
write/out "------------------------------------------ "
set/format i1
if catli(3) .ne. 5 then
   write/out "first, last index for extraction of string from input name =" -
          {catli(1)},{catli(2)}
else
   write/out "first, last index not used..."
endif
write/out "---- "
write/out "method chosen for building up output frame name is:"
! 
branch catli(3) 2,3,4,5 M2,M3,M4,M5
! 
!  default to method 1
write/out "output name = extracted string+contents of keyword CATLC "
goto r_5
M2:
write/out "output name = extracted string+entry no. in input catalog "
goto r_5
M3:
write/out "output name = contents of keyword CATLC+extracted string"
goto r_5
M4:
write/out "output name = contents of keyword CATLC+entry no. in input catalog "
goto r_5
M5:
write/out "output name taken from catalog with name in keyword CATLC"
! 
R_5:
write/out "---- "
if catli(4) .eq. 0 then
   write/out "frames of first input catalog are not deleted after processing"
else
   write/out "frames of first input catalog are deleted after processing"
endif
! 
write/out "---- "
write/out "catalog entries (of input catalogs) from" -
          {catli(5)} to {catli(6)} "will be used"
! 
write/out "---- "
if catli(7) .gt. 0 then
   set/format i1
   write/out -
   "param. P{catli(7)} of EXECUTE/CATALOG will hold the output catalog name"
else
   write/out "no output catalog used"
endif
! 
write/out "---- "
write/out "contents of keyword CATLC are: " {catlc}
return
! 
entry INFO 				!INFO/SETUP
! --------
! 
define/param p1 short c "Enter display flag (short/full):"
! 
write/out "The Setup  CATALOG  is needed for setting up the various control" -
          "values "
write/out -
equired for the MIDAS command EXECUTE/CATALOG and uses two special keywords,"
write/out -
"namely CATLI/I/1/7 and CATLC/C/1/20 to store these control values"
write/out 
write/out "Use: INFO/SETUP CATALOG   to get this text"
write/out "Use: INIT/SETUP CATALOG   to initialize the keywords of the Setup"
write/out "Use: READ/SETUP CATALOG   to display the current Setup" -
          for execute/catalog
write/out -
"Use: WRITE/SETUP CATALOG  to change the Setup for execute/catalog as follows"
write/out 
write/out "WRITE/SETUP CATALOG  first,last  option  del_flag" -
          "low,hi  out_indx  string "
write/out 
write/out "first,last = start,last_index for extracted string from input name"
if p1(1:1) .eq. "S" then
   write/out (default = 1,4)
else
   write/out "(default = 1,4  ->  CATLI(1,2))"
endif
write/out "option = naming option for result frames - supported" -
          "options are:"
write/out "         1 = use extracted input and append what is in parameter" -
          "`string'"
write/out "         2 = use extracted input and append catalog_entry no."
write/out "         3 = use what is in `string' and append extracted input "
write/out "         4 = use what is in `string' and append" catalog_entry no.
write/out "         5 = use what is in `string' as name of a catalog" -
          "containing the"
write/out "             output names (options 4,5 ignore param. `first,last')"
if p1(1:1) .eq. "S" then
   write/out (default option = 1)
else
   write/out "(default option = 1  ->  CATLI(3))"
endif
write/out "del_flag = YES or NO, to delete the frames of first" -
          "input catalog after"
write/out "                      processing or not"
if p1(1:1) .eq. "S" then
   write/out (default = NO)
else
   write/out "(default = NO (YES => 1, NO => 0)  ->  CATLI(4))"
endif
write/out "low,hi = first,last entry no. of first input catalog to be used"
if p1(1:1) .eq. "S" then
   write/out (default = 0,999999)
else
   write/out (default = 0,999999  ->  CATLI(5,6))
endif
write/out -
"out_indx = no. of param. (of EXEC/CATAL command!) serving as output catalog," 
write/out -
"           e.g.: \"EXEC/CATAL comm/qualif result.cat ...\" => out_index = 2"
write/out "           set to 0, if not output catalog used" 
if p1(1:1) .eq. "S" then
   write/out (default = 0 (no result catalog))
else
   write/out "(default = 0 (no result catalog)  -> CATLI(7))"
endif
!
write/out -
"string = the string (max. 20 chars.) used for the options described above"
if p1(1:1) .eq. "S" then
   write/out (default = xyz)
else
   write/out "(default = xyz  ->  CATLC(1:20))"
endif
! 
entry INIT 				!INIT/SETUP
! --------
! 
write/keyw catli/i/1/7 1,4,1,0,0,999999,0 
write/keyw catlc/c/1/20 "xyz     "
