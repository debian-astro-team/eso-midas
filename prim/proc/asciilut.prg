! @(#)asciilut.prg	19.1 (ESO-DMD) 02/25/03 14:08:43
! ++++++++++++++
!  
!  MIDAS procedure  asciilut.prg
!  to convert all ascii LUT files into MIDAS tables
!  K. Banse     ESO - Garching    910307, 920318, 940113, 980928
! 
! ++++++++++++++
! 
! handle in VMS or UNIX
! 
write/out "Copying all ascii LUTs to current MIDAS default directory ..."
! 
if aux_mode(1) .le. 1 then		! VMS
   $ DELETE/NOCONF *.lasc.*,lut.lis.*
   $ COPY {ascdir}*.lasc {defdir}
else					! UNIX
   $ touch lut.lis
   $ rm -f *.lasc  lut.lis
   $ cp {ascdir}*.lasc {defdir}
endif
! 
@ asclut aips0
@ asclut backgr
@ asclut blue
@ asclut blulut
@ asclut color
@ asclut green
@ asclut heat
@ asclut idl11 
@ asclut idl12 
@ asclut idl14 
@ asclut idl15 
@ asclut idl2 
@ asclut idl4 
@ asclut idl5 
@ asclut idl6 
@ asclut isophot 
@ asclut light 
@ asclut manycol 
@ asclut pastel 
@ asclut rainbow 
@ asclut rainbow1 
@ asclut rainbow2 
@ asclut rainbow3 
@ asclut rainbow4 
@ asclut ramp 
@ asclut rampred 
@ asclut random 
@ asclut random1 
@ asclut random2 
@ asclut random3 
@ asclut random4 
@ asclut random5 
@ asclut random6 
@ asclut real 
@ asclut red 
@ asclut smooth 
@ asclut smooth1 
@ asclut smooth2 
@ asclut smooth3 
@ asclut staircase 
@ asclut stairs8 
@ asclut stairs9 
@ asclut standard 
! 
write/out Copying all binary LUTs to relevant MIDAS directory ...
! 
if aux_mode(1) .le. 1 then		! VMS
   $ SET PROT=W:RWE *.lut
   $ COPY *.lut {bindir}
   $ DELETE/NOCONF *.lasc.*,*.lut.*,lut.lis.*
else					! UNIX
   $ chmod 666 *.lut *.lasc
   $ mv *.lut {bindir}.
   $ rm -f *.lasc lut.lis
endif
