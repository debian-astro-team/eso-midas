!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: plotcon.prg
!.PURPOSE: MIDAS procedure to produce a contour map of a two-dim. image
!          execute by
!          PLOT/CON P1 P2 P3 P4 P5 P6 where:
!          P1: image is the image to be plotted
!          P2: coord_string is the area
!          P3: scal_x,scal_y, off_x and off_y (default device filling)
!          P4: contours are the contour levels
!          P5: contour type 
!          P6: smooth_par is parameter which represents size of smoothing box
!.AUTHOR:  Ch. Ounnas, R.H. Warmels, ESO-Garching
!.VERSION: 860911 RHW creation
! 060524	last modif
!-----------------------------------------------------------------------------
! 
define/param p1 * ima
define/local cmnd/c/1/20 "{mid$cmnd(1:20)}"            !save MID$CMND
define/local more_in/i/1/1 0		     !will be changed by plotcon.exe
!
if plrstat(1) .eq. 0.0 .and. plrstat(2) .eq. 0.0 then
   if plrstat(5) .eq. 0.0 .and. plrstat(6) .eq. 0.0 then
      define/param p2 [<,<:>,>] C
      if p2(1:1) .eq. "M" then
         write/out "*** FATAL: axes not properly set; use SET/GRAPHICS"
         return
      endif
   else
      define/param p2 MANUAL   C
   endif
else
   define/param p2 MANUAL   C
endif
!
! select area via cursor
if p2 .eq. "C" then
   if p1 .ne. "?" load/image {p1}
   get/cursor ? ? NN 1,2		         ! use cursor rectangle once
   write/keyw  mid$cmnd/c/1/20 "{cmnd(1:20)}"
endif
!
write/keyw in_a {p1}
write/keyw in_b {p2}
!
again:

if mid$cmnd(1:1) .eq. "P" then
   define/param p3 0.,0.,-999,-999 NUM
   @ plscoff {p3}                                 ! get the scales and offsets
   define/param p4 1.0 C
   write/keyw inputc/c/1/72  {p4}   
   define/param p5 ODD C
   write/keyw inputc/c/73/4  {p5}
   define/param p6 0 N
   write/keyw inputi/i/1/1   {p6}
   dattim = m$time()
else
   define/param p3 1.0 C
   write/keyw inputc/c/1/72  {p3}
   define/param p4 ODD C
   write/keyw inputc/c/73/4  {p4}
   define/param p5 0 N
   write/keyw inputi/i/1/1   {p5}
endif
!
run MID_EXE:PLOTCON                               ! make the bloddy plot
write/keyw plcdata/c/1/60  {p1}                   ! name of data structure
write/keyw plcdata/c/61/20 "FRAME       "         ! type ofdat astructure
copy/graph
if more_in .eq. 1 then
   outputc(1:1) = "x"
   inquire/keyw outputc "more contours? Enter Yes or No"
   if outputc(1:1) .eq. "Y" then
      get/cursor ? ? NN 1,2	
      write/keyw  mid$cmnd/c/1/20 "{cmnd(1:20)}"
      goto again
   endif
endif
  

