! @(#)t2regrsav.prg	19.1 (ES0-DMD) 02/25/03 14:09:07
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2regrsav.prg
!  TABLE Subsystem
!
!  J.D.Ponz                ESO - Garching    5 AUG 89
!  P. Ballester            ESO - Garching   17 SEP 91  (Extended keywords)
!
! .PURPOSE
!
!    implements
!
!    SAVE/REGRESSION table name  [inpkey]
!
!
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ?       TABLE "Enter table:"
DEFINE/PARAM P2 ?       CHAR  "Enter regression name:"
DEFINE/PARAM P3 OUTPUT  CHAR  "Input keyword generic name :"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:topertbl
