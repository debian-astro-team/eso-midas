#! /bin/sh
# @(#)testaccs.sh	19.1 (ESO-IPG) 02/25/03 14:09:14
#
# testaccs - test access to given dir.
#
#  K. Banse    last modif:	030124
#

if `touch $1 2>/dev/null`; then
   echo 1
else
   echo 0
fi


