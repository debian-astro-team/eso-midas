! @(#)modgcur.prg	19.1 (ES0-DMD) 02/25/03 14:08:55
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: MODGCUR.PRG
!.PURPOSE:        Change data in a line (the input image) interactively
!                 via cursor input, by interpolating polynomially between
!                 cursor positions (currently up to 100 cursor-positions and
!                 10th degree)
!.NOTE:           Note that the original data is overwritten.
!.KEYWORDS:       Modify
!.LANGUAGE:       MIDAS command language.
!.SYNTAX:         MODIFY/GCURSOR in_ima out_ima line [pstart:pend] curs,deg
!                 where: in_ima = input file name
!                        out_ima = output frame
!                        line  = line number
!                        [pstart:pend] = first and last point on the line to be
!                                displayed (entered as pixel nos. when prefixed
!                                by @, and otherwise as real world coordinates;
!                                Note that the square brackets are obligatory).
!                        curs  = no. of cursor positions.
!                        deg  = degree of polynomial for the interpolation.
!.DESCRIPTION:   To implement, type any key except BAR or RETURN to validate
!                a cursor position.  When the specified number of cursor
!                positions are available, the interpolated line is drawn in
!                the selected line type.
!                To visualize the modified line, strike BAR.  At this step,
!                you can continue to send new positions or you can exit by
!                striking BAR again.
!.AUTHOR:        F. Murtagh
!.VERSION:       F. Murtagh, ST-ECF.                  13.11.1985
!.VERSION:       F. Murtagh, ST-ECF.                  09.04.1986
!               (Extend i/p parameters to allow pixel pixel nos. as well as
!                as world coords.)
!                920413  KB, `\' -> ` '
!---------------------------------------------------------------------------
DEFINE/PARAM P1  ?   IMA "Enter input image name: "
DEFINE/PARAM P3  @1
!
IF {PLRSTAT(1)} .NE. 0.0 .AND. {PLRSTAT(2)} .NE. 0.0 THEN
   DEFINE/PARAM P4 MANUAL C
ELSE
   DEFINE/PARAM P4 <,> C
   IF P4(1:1) .EQ. "M" THEN
      WRITE/OUT "*** FATAL: X axis not manually set; use SET/GRAPHICS"
      RETURN
   ENDIF
ENDIF
!
DEFINE/PARAM P5 2,1 N
!
IF P2 .NE. "?" THEN
   COPY/II {P1} {P2}
ELSE
   P2 = "{P1}"
ENDIF
!
WRITE/KEYW IN_A   {P2}
WRITE/KEYW IN_B   {P3}
WRITE/KEYW INPUTC {P4}
WRITE/KEYW INPUTI/I/1/2 {P5}
WRITE/KEYW PLISTAT/I/1/1 1
DATTIM = M$TIME()
!
IF INPUTI(1) .LE. INPUTI(2) THEN
   WRITE/OUT -
         "*** FATAL: Degree must be less than the number of cursor positions"
   RETURN
ENDIF
RUN MID_EXE:MODGCUR
!
WRITE/KEYW PLCDATA/C/1/60 {P2}
WRITE/KEYW PLCDATA/C/61/20 "IMAGE       "
