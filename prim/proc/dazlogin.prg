! @(#)dazlogin.prg	19.1 (ESO-DMD) 02/25/03 14:08:48
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
!                                                                       
!  MIDAS procedure  dazlogin.prg  to clean up the image display
!                                 related keywords
!  K. Banse	901221
!                                                                       
!  keyword IDIDEV = 50 integer values for an Image Display
!  for a detailed description see: HELP/KEYW ididev
! 
!.VERSION
! 020731	last modif
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
!                                                                       
define/param p1 + C
if p1(1:1) .eq. "L" then
   write/keyw dazdevr 4,1,210,1,0,0,0,0,0,-1,-1,0,0,0,0,0,0,-1,0,12
endif
! 
write/keyw ididev/i/1/50 0 all
write/keyw winopen/i/1/20 0 all
write/keyw dazdevc/c/1/80 "default "
! 
! ididev(7,17,21) = dazdevr(18,3,2)
! get back Color Mode, initial LUTsize, noLUT from keyword dazdevr 
write/keyw ididev/i/1/10 -2,512,512,8,0,0,{dazdevr(18)},0,0,99
write/keyw ididev/i/11/11 512,512,8,2,0,2,{dazdevr(3)},11,0,0,{dazdevr(2)}

