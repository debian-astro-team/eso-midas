! @(#)execat.prg	19.1 (ESO-DMD) 02/25/03 14:08:50
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  execat.prg   to implement EXECUTE/CATALOG
!
! K. Banse	901203, 940128, 940919, 980107, 000323
! execute via EXECUTE/CATAL proc.prg       parm1 ... parm6 parm7
! or          EXECUTE/CATAL command/qualif parm1 ... parm6 parm7
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/para p1 ? C "Enter procedure (with type .prg) or command/qualif: "
! 
define/local dummy/c/1/10  " "  ALL
define/local incat/I/1/1 0
define/local outcat/i/1/1 0
define/local temp/c/1/60 " " all
define/local ownfile/c/1/60 " " all
define/local flag/i/1/1 0
define/local n/i/1/5 0,{pcount},0,0,0
define/local iq/i/1/1 0
define/local catal/i/1/8 0 all
! 
! check, if P1 procedure or command/qualif
! 
if m$index(p1,".prg") .gt. 0 flag = 1
!
! test, if keywords CATLI, CATLC exist
! 
if m$existk("catli") .ne. 1 then
   write/keyw catli/i/1/7 1,4,1,0,0,999999,0		!init with defaults...
   write/keyw catlc/c/1/20 "xyz     "
   write/out -
   "No  WRITE/SETUP CATALOG  was done before - keywords initialized to defaults"
endif
!
! 
!  define Z2, Z3, ... 
! 
set/format i1
if catli(7) .gt. 0 then
   outcat = catli(7)			!if > 0, indx of output catalog
   if m$index(p{outcat},".cat") .le. 0 then	!check, if really a catalog
      outcat = 0
   else if m$exist(p{outcat}) .ne. 1 then
      create/icat {p{outcat}} null >Null
   endif
endif
do n = 2 8
   if n .gt. n(2) then			!parameters not given get small Zi's
      define/local z{n}/c/1/1 " "
   else
      define/local z{n}/c/1/100 " " ALL
      z{n}(1:1) = "?"
      if incat .eq. 0 then		!do it only once
         n(5) = m$index(p{n},".cat")		!find the 1. input catalog
         if n(5) .gt. 1 .and. n .ne. outcat then
            incat = n
         endif
      endif
   endif
enddo
!
!
if incat .le. 0 then
   write/out "No catalog involved => no use for EXECUTE/CATALOG ..."
   return
endif
! 
! here comes the real loop over the input catalog
! -----------------------------------------------
! 
CATAL_LOOP:
do n = 2 {n(2)}
   if n .ne. outcat then
      store/frame z{n} {p{n}} {n} finis
   endif
enddo
!
! omit first CATLI(5) catalog entries
! 
n(3) = catal({incat})			!is set in the STORE/FRAME above
if catli(5) .gt. n(3) goto catal_loop
n(4) = n(4)+1				!increment counter
! 
! for output catalog build output name according to different methods
! keyword `ownfile' holds that name
! 
if outcat .gt. 0 then
   if catli(3) .eq. 4 then
      write/keyw ownfile {catlc}{catal({incat})}
   else if catli(3) .eq. 5 then
      store/frame ownfile {catlc} 1 finis		!use catal(1)
   else
      write/keyw dummy/c/1/10 {catli(1)}:{catli(2)}
      temp = z{incat}({dummy})
      iq = m$index(temp,".")
      if iq .gt. 0 temp({iq}:) = "      "
         if catli(3) .eq. 2 then
         write/keyw ownfile {temp}{catal({incat})}
         goto do_it
      endif
      if catli(3) .eq. 3 then
         write/keyw ownfile {catlc}{temp}
         goto do_it
      endif
      write/keyw ownfile {temp}{catlc}
   endif
do_it:
   write/keyw z{outcat} {ownfile}
endif
!
if flag .eq. 0 then
   write/out "{p1} {z2} {z3} {z4} {z5} {z6} {z7} {z8}"
   {p1} {z2} {z3} {z4} {z5} {z6} {z7} {z8}
else
   write/out "@@ {p1} {z2} {z3} {z4} {z5} {z6} {z7} {z8}"
   @@ {p1} {z2} {z3} {z4} {z5} {z6} {z7} {z8}
endif
if outcat .gt. 1  then
   add/icat {p{outcat}} {ownfile} >null
endif
!
! final last tests + we loop
! --------------------------
! 
if catli(4) .eq. 1 delete/image {z{incat}} NO		!don't ask...
!
! test upper limit of entry numbers
! 
if catli(6) .gt. n(3) goto catal_loop
! 
FINIS:
if outcat .gt. 1  then
   write/out {n(4)} frames processed + added to catalog {p{outcat}} ...
else
   write/out {n(4)} frames processed 
endif
