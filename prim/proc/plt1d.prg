! @(#)plt1d.prg	19.1 (ES0-DMD) 02/25/03 14:08:58
! @(#)plt1d.prg	6.1 (ESO-IPG) 7/16/93 16:34:49
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: plt1d.prg
!.PURPOSE:        Demonstration procedure for the graphics package. 
!.AUTHOR:         Rein Warmels  ESO - Garching April. 25 , 1988
!.USE:            Execute as TUTORIAL/GRAPHICS
!.NOTE:           The following files should exist UGC.TBL in MID_TEST
!.VERSION:        880425 RHW creation
! -----------------------------------------------------------------------
!              Here for the one-dimensional (spectral) stuff
!
write/out "First,the frame spiral.bdf is copied into your directory"
write/out "and the graphics package is set in the default mode."
echo/on
!
indisk/fits MID_TEST:spiral.fits  tspiral.bdf >Null
! 
set/grap default                                     ! default graphics setup
echo/off
write/desc tspiral start/d/1/2  -2940,-2980
write/desc tspiral step/d/1/2  10.,10.
!
write/out " "
write/out "Now let's compute the intensity distribution of the pixel values"
write/out "and plot the histogram of this distribution:"
echo/on
create/graph 0 1000,450,0,450
! create/graph 0 1000,450,0,325                       ! for the SONY screen
plot/hist tspiral ? 
echo/off
write/out " "
write/out "Use the whole device area and plot in logarithmic mode:"
echo/on
set/grap pmode=1
plot/hist tspiral ? LOG10
echo/off
write/out " "
!
write/out "Plot and overplot lines of the frame; use some plot settings:"
echo/on
set/grap pmode=1
set/grap xaxis=-1800,1800,600,150 yaxis=-2,10,4,1
set/grap ltype=0 stype=1
plot/row tspiral @300 ? ?
set/grap ltype=1
overplot/row tspiral @300 ? 
!over/table tspir300 #1 #2
label/grap "line number 300" 1200,1 ? 1. 0
set/grap ltype=3
overplot/row tspiral @305 -900,900 2
!over/table tspir305 #1 #2
label/grap "line number 305" 1200,3 ? 1. 0
set/grap binmode=on ltype=1
overplot/row tspiral @310 -900,900 4
!over/table tspir310 #1 #2
label/grap "line number 310" 1200,5 ? 1. 0
echo/off
set/grap
wait/secs 10
delete/graphics
