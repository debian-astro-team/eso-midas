! @(#)modlut.prg	19.1 (ESO-DMD) 02/25/03 14:08:55
! +++++++++++++++++++++++++++++++++++++
!
! command procedure  modlut.prg  to implement MODIFY/LUT, /ITT
! K. Banse	910227, 910611, 920523, 940216, 980907
! 	
! execute via MODIFY/LUT method colour print_flag
! 	method = Band, Rotate, Graphics, Squeeze, HSI
! 	colour = Red (def), Green, Blue, White + Dark  or
!                individual RGB colour values as integer (Ir,Ig,Ib) or
!		 as hex. values (0xrrggbb) for for method B
! 	         Red, Green, Blue + All (def) for method R
! 	         Red, Green, Blue + All (def) for method G
! 	         Hue, Saturation, Intensity for method HSI
! 	         n.a. for method S
!   print_flag = Y(es) for showing the corresponding intensity interval
!                N(o) if not, only used for method = Band
! 
! or          MODIFY/ITT method value print_flag
! 	method = Band, Rotate, Squeeze, Convolve, Contrast
!       value  = in [0,255]
!   print_flag = Y(es) for showing the corresponding intensity interval
!                N(o) if not
! 
! or          EQUALIZE/HISTOGRAM frame [descr] [itt_name]
!
! +++++++++++++++++++++++++++++++++++++
!
if ididev(18) .eq. 11 then
   if dazdevr(4) .ne. 1 then                       !only possible with own LUT
      write/out this command only possible with own LUT ...
      return
   endif
   define/local modscale/i/1/2 8,3		!scaling of joystick delta
else
   define/local modscale/i/1/2 8,12
endif
! 
if mid$cmnd(1:1) .eq. "E" then		!EQUALIZE/HISTOGRAM
   define/param p1 * IMA "Enter image name: "
   define/param p2 + C "Enter optional descr name: "
   define/param p3 + C "Enter name for resulting ITT: "
!
   write/keyw in_a {p1}
   default(15:16) = "IH"
   run MID_EXE:LUTMOD
   return
endif
! 
!  here the MODIFY/...  commands
!  -----------------------------
! 
define/param p1 B ? "Enter method:"
default(15:15) = "I"
!
!  MODIFY/ITT
!
if mid$cmnd(11:11) .eq. "I" then	!MODIFY/ITT
   define/param p2 255 N "Enter value in [0,255]: "
   define/param p3 N C "Enter print_flag: "
   default(16:16) = p1(1:1)
! 
   if p1(1:1) .eq. "C" then
      if p1(2:4) .eq. "ONT" then	!option CONTRAST => K
         default(16:16) = "K"
      else         
         if p2(1:1) .eq. "C" then	!for option CONVOLVE
            inputr(1) = 1		!use INPUTR(1) for passing the
         else				!CONTINUE option...
            inputr(1) = 0
         endif
      endif
   else
      inputr(1) = {p2}
   endif
! 
   define/local izz/i/1/2 1,20
else
! 	
!  MODIFY/LUT
!
   default(15:15) = "L"
   branch p1(1:1) R,G,S,H ROTA,GRAPH,SQU,HSI   !branch according to method...
!
! default is overlay a colour band...
   define/param p2 R ? "Enter colour value - R/G/B/W/D or Ir,Ig,Ib or 0xrrggbb:"
   define/param p3 N C "Enter print_flag: "
!
   if p2(1:2) .eq. "0x" then		!input in hex. as in X11 notation
      define/local hex/c/1/14 "0x  ,0x  ,0x  "
      hex(3:4) = p2(3:4)
      hex(8:9) = p2(5:6)
      hex(13:14) = p2(7:8)
      write/keyw inputi {hex}
      write/keyw inputr {inputi(1)},{inputi(2)},{inputi(3)}
   else if m$tstno(p2) .eq. 1 then
      write/keyw inputr {p2}
   else
      @ colourdef {p2}
   endif
   goto runluta				!continue as for rotation
!
ROTA:
   define/param p2 A ? "Enter colour value - R,G,B or  for all colours:"
!
RUNLUTA:
   default(16:16) = "A"
   goto modi_lut
!
GRAPH:
   define/param p2 A ? "Enter colour value - R,G,B or  for all colours:"
   default(16:16) = "B"
   goto modi_lut
!
SQU:					!squeeze...
   default(16:16) = "C"
   goto modi_lut
!
HSI:					!HSI modification
   define/param p2 H ? "Enter colour value - H, S or I:"
   default(16:16) = "D"
! 
MODI_LUT:
   define/local izz/i/1/2 21,40
endif
! 
define/maxpar 3
run MID_EXE:LUTMOD
! 
write/keyw inputc/c/1/20 {hcittlut({izz(1)}:{izz(2)})}
inputi = m$len(inputc)+2
write/keyw inputc/c/{inputi}/8 modified
write/keyw hcittlut/c/{izz(1)}/20 "{inputc(1:20)}"
