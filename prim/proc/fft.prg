! @(#)fft.prg	19.1 (ES0-DMD) 02/25/03 14:08:50
! ++++++++++++++++++++++++++++++++++++++++++++++
!
!       MIDAS procedure fft.prg
!
! 	calculate the Fourier Transformation of an image
!
! execute in MIDAS as FFT/IMAGE   par1 par2 par3 par4 
! or		      FFT/FREQU par1 par2 par3 par4 
! or                  FFT/INV   par1 par2 par3 par4 
! or                  FFT/FINV   par1 par2 par3 par4 
! or                  FFT/POWER   par1 par2 par3 par4 par5 
! or                  FFT/FPOWER   par1 par2 par3 par4 par5 
! where:
! par1 = name of real input frame
! par2 = name of imaginary input frame
! par3 = name of frame to hold real part of Fourier transform
! par4 = name of frame to hold imaginary part of Fourier transform
! default for par5 is power
!
! if qualifier FREQU is given, frequency scaling + shifting of the
! origin to the center is done
!
! FFT works in double precision,
!
!        K. Banse               900727, 921026
!
! +++++++++++++++++++++++++++++++++++++++++++++++
!
if mid$cmnd(11:11) .eq. "F" then
   define/local cindx/C/1/5 12:13
else
   define/local cindx/C/1/5 11:12
endif
define/local tmpbuf/c/1/3 NNN
! 
! here for inverse FFT
if mid$cmnd({cindx}) .eq. "IN" then
   define/param p1 fftr IMA "Enter real input image:"
   define/param p2 ffti IMA "Enter imaginary input image:"
   define/param p3 zztr IMA "Enter real output image:"
   define/param p4 zzti IMA "Enter imaginary output image:"
   tmpbuf(2:2) = "I"			!indicate INVERSE stuff
else
! 
! here for forward FFT
   define/param p1 ? IMA "Enter real input image:"
   define/param p2 + IMA "Enter imaginary input image:"
   define/param p3 fftr IMA "Enter real output image:"
   define/param p4 ffti IMA "Enter imaginary output image:"
endif
! 
if mid$cmnd({cindx}) .eq. "PO" then
   define/param p5 power IMA "Enter output image for power spectrum:"
   tmpbuf(2:2) =  "P"			!also indicate POWER stuff
endif
!
! save command + qualifier
if mid$cmnd(11:13) .ne. "IMA" then
   define/local cactio/c/1/3 {mid$cmnd(11:13)}
else
   define/local cactio/c/1/3 ???
endif
!
write/keyw history "FFT/{mid$cmnd(11:13)} "
run MID_EXE:FOURI
! 
if tmpbuf(3:3) .eq. "Y" then            !we really had an expansion
   if error(2) .lt. 2 then
      write/out -
      "expanded result frames exp_fftr.bdf, exp_ffti.bdf have been created"
      if tmpbuf(2:2)) .eq. "P" write/out -
         "expanded result frame exp_pow.bdf has been created"
   endif
elseif tmpbuf(3:3) .eq. "Z" then		!we had an extraction
   if error(2) .lt. 2 then
      write/out -
      "original sized result frames orig_r.bdf, orig_i.bdf have been extracted" 
   endif
endif
