! @(#)alldisconn.prg	19.1 (ES0-DMD) 02/25/03 14:08:42
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure alldisconn.prg  is used with DISCONNECT/BACK_MIDAS
! K. Banse	961111
!
! execute via
! @ alldisconn unit_1,... bchar_1,... method_1,... flag
! with unit_i = the unit of background Midas (server) (i=1,...)
!      bchar_i = the b_char of background Midas (server) (i=1,...)
!      method_i = the method of background Midas (server) (i=1,...)
!      flag = discon_server_flag of DISCONNECT command
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 + C "Enter server units:"
define/param p2 b C "Enter b_chars:"
define/param p3 S C "Enter method:"
define/param p4 X C "Enter discon_server_flag:"
! 
define/local indx/i/1/2 0,1
define/local cbuf/c/1/60 " " all
cbuf = p1
define/local bbuf/c/1/30 {p2}
define/local mbuf/c/1/30 {p3}
define/local unit/c/1/2 "  "
define/local bchar/c/1/1 b
define/local method/c/1/1 S
! 
loop:
unit = cbuf(1:2)				!unit of connection
bchar = bbuf({indx(2)}:{indx(2)})		!b_char of connection
method = mbuf({indx(2)}:{indx(2)})		!method of connection
indx(2) = indx(2)+2
! 
@ disconnect {unit} {bchar} {method} {p4}
! 
indx = m$index(cbuf,",")
if indx .eq. 3 then				!shift units
   write/keyw cbuf {cbuf(4:)}
   if cbuf(1:1) .ne. " " goto loop
endif
