! ++++++++++++++++++++++++++++++++++++++++++++++++++++
! MIDAS procedure ittexa.prg
! K. Banse	890616, 910425, 030704
!
! demonstrate different intensity transfer tables
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++
!
write/keyw out_a {p1}            !save plotflag (Plot or NoPlot)
!
write/out the following intensity transfer tables exist currently
write/out
write/out "neg, log, neglog, expo, gamma, jigsaw, stairs, ramp"
write/out
!
if ididev(7) .ge. 2 then
   define/local reload/i/1/1 1 ? +low   !for Pseudo on RGB we must reload
else
   define/local reload/i/1/1 0 ? +low
   set/lut				!enable lookup tables
endif
!
display/lut				!display colour bar
!
@ ittexa,subitt neg
@ ittexa,subitt log 
@ ittexa,subitt neglog 
@ ittexa,subitt expo 
@ ittexa,subitt gamma
@ ittexa,subitt jigsaw
@ ittexa,subitt stairs
@ ittexa,subitt ramp
!
! here entry SUBITT
!
entry subitt
!
write/out LOAD/ITT {p1} yields:
load/itt {p1}
if reload .eq. 1 load/image {idimemc}   !we know that there is an image...
if out_a(1:1) .eq. "P" then
   @a plottab,itt {p1}
   wait/secs 4
else
   wait/secs 2.5
endif
