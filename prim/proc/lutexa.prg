! ++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure lutexa.prg
! K. Banse	900703, 970929, 030704
!
! demonstrate different colour lookup tables
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
write/keyw out_a {p1}		!save plotflag (Plot or NoPlot)
!
write/out "Some of the existing colour lookup tables are:"
write/out
write/out staircase, rainbow1, random, random5, smooth1, rainbow3, light, heat,
write/out color, backgr
write/out
!
if ididev(7) .ge. 2 then
   define/local reload/i/1/1 1 ? +low	!for Pseudo on RGB we must reload
else
   define/local reload/i/1/1 0 ? +low
endif
! 
set/lut					!enable lookup tables
display/lut				!display colour bar
!
@ lutexa,sublut staircase 
@ lutexa,sublut rainbow1 
@ lutexa,sublut random 
@ lutexa,sublut random5
@ lutexa,sublut smooth1
@ lutexa,sublut rainbow3
@ lutexa,sublut light 
@ lutexa,sublut heat 
@ lutexa,sublut color
@ lutexa,sublut backgr 
@ lutexa,sublut rainbow
! 
write/out
write/out "To see all existing LUTs, use the host command"
write/out  -DIR MID_SYSTAB:*.lut
-dir MID_SYSTAB:*.lut
!
! here entry SUBLUT
!
entry sublut
! 
write/out LOAD/LUT {p1} yields:
load/lut {p1} 
if reload .eq. 1 load/image {idimemc}	!we know that there is an image...
! 
if out_a(1:1) .eq. "P" then
   @a plottab,lut {p1}			!draw LUT 
   wait/secs 4
else
   wait/secs 2.5
endif
