! @(#)t2regrtbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:07
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2regrtbl.prg
!  TABLE Subsystem
!
!  J.D.Ponz                ESO - Garching    5 AUG 89
!
! .PURPOSE
!
!    implements
!
!  REGRESSION/TABLE  table x[,order] catalogue y1[,y2] degree tol
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
DEFINE/PARAM P2 ? CHAR  "Enter column(s):"
DEFINE/PARAM P3 ? TABLE "Enter table:"
DEFINE/PARAM P4 ? CHAR "Enter column(s):"
DEFINE/PARAM P5 ? NUM   "Degree of the polynomial:"
DEFINE/PARAM P6 ? NUM   "Tolerance:"
!
WRITE/KEYW INPUTI/I/1/2 0,0
WRITE/KEYW INPUTI/I/1/2 {P5}
WRITE/KEYW INPUTR/R/1/1 {P6}
!
RUN MID_EXE:topertbl
