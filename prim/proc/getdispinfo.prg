! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure  getdispinfo.prg  to get display characteristics
!  K. Banse     000221
!
!.VERSION
! 070710        last modif
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! get the display info via the command `xwininfo'
! 
if aux_mode .ne. 2 .or. m$symbol("DISPLAY") .eq. "DISPLAY" then
   ididev(13) = 8		!overall default is 8 bits per pixel
   dazdevr(18) = 0		!and PseudoColor
   return
endif
! 
define/local inpi/i/1/2 0,0
define/local outpi/i/1/2 0,0
define/local cc/c/1/1 z
! 
$ rm -f $MID_WORK/getXdepth{mid$sess(11:12)}.env
if dazdevc(1:7) .eq. "default" then
   $ xwininfo -root > $MID_WORK/getXdepth{mid$sess(11:12)}.env 
else
   $ xwininfo -root -display {dazdevc} > $MID_WORK/getXdepth{mid$sess(11:12)}.env
endif
! 
outputc = m$symbol("MID_WORK")
open/file {outputc}/getXdepth{mid$sess(11:12)}.env read inpi
if inpi .lt. 1 then
   write/out could not open {outputc}/getXdepth{mid$sess(11:12)}.env ...
   goto error_return
endif
! 
read_loop:
read/file {inpi(1)} out_b 20
if inpi(2) .lt. 0 then
   write/out "We could not get all the required info (via `xwininfo') ..."
   close/file {inpi(1)}
   return
else if inpi(2) .lt. 10 then		!avoid empty lines
   goto read_loop
endif
! 
outpi = m$index(out_b,"Width")
if outpi .gt. 0 then
   outpi = outpi + 6
   dazdevr(12) = {out_b({outpi}:)}
   outpi(2) = outpi(2) + 1
else
   outpi = m$index(out_b,"Height")
   if outpi .gt. 0 then
      outpi = outpi + 7
      dazdevr(13) = {out_b({outpi}:)}
      outpi(2) = outpi(2) + 1
   else
      outpi = m$index(out_b,"Depth")
      if outpi .gt. 0 then
         outpi = outpi + 6
         ididev(13) = {out_b({outpi}:)}
         outpi(2) = outpi(2) + 1
      else
         outpi = m$index(out_b,"Visual Class")
         if outpi .gt. 0 then
            outpi = outpi + 14
            cc = out_b({outpi}:{outpi})
            if cc .eq. "T" then
               dazdevr(18) = 3		!Pseudo on TrueColor
            else if cc .eq. "D" then
               dazdevr(18) = 2		!Pseudo on DirectColor
            else
               dazdevr(18) = 0		!PseudoColor
            endif
            outpi(2) = outpi(2) + 1
         endif
      endif
   endif
endif
if outpi(2) .lt. 4 goto read_loop   
! 
! we have all the info we want, let's go
close/file {inpi(1)}

