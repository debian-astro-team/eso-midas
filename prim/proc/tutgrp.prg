! @(#)tutgrp.prg	19.1 (ES0-DMD) 02/25/03 14:09:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: TUTPLT.PRG
!.PURPOSE:        Demonstration procedure for the graphics package. 
!.AUTHOR:         Rein Warmels  ESO - Garching April. 25 , 1988
!.USE:            Execute as TUTORIAL/GRAPHICS
!.NOTE:           The following files should exist
!                     UGC.TBL & EXAMPL.TBL in MID_TEST
!.VERSION:        880425 RHW creation
!.VERSION:        881005 RHW split up into several parts
!.VERSION:        931029 RvH added 3-D table tutorial
! -----------------------------------------------------------------------
DEFINE/PARAM P1 GENERAL C                              ! default tutorial
!
BRANCH P1(1:2) GE,AX,TA,1D,2D,TB,AL,LO,AU GENERAL,AXES,TABLE,DIM1,DIM2,TABLE3,ALL,LOOP,ALL
!
! Here for a general demonstration
GENERAL:
@ pltgen
RETURN
!
! Here you will find a number of examples to draw axes
AXES:
@ pltaxs
RETURN
!
! Here for the table stuff
TABLE:
@ plttbl
RETURN
!
! Here you will find a number of examples to draw spectra
DIM1:
@ plt1d
RETURN
!
! Here for the two-dimensional stuff
DIM2:
@ plt2d
RETURN
!
! Here for the three-dimensional table stuff
TABLE3:
@ plttbl3
RETURN
!
! Here for all
ALL:
@ pltgen
@ pltaxs
@ plttbl
@ plt1d
@ plt2d
@ plttbl3
IF P1(1:4) .EQ. "AUTO" THEN
  -DELETE tspiral.bdf
  -DELETE graph_wnd000.plt
  -DELETE image_wnd000.plt
  -DELETE texample.plt
  -DELETE tugc.dat
  -DELETE tugc.fmt
   write/out successful end of graphics subsystem tutorial...
ENDIF
RETURN
!
! Here for all
LOOP:
@ pltgen
@ pltaxs
@ plttbl
@ plt1d
@ plt2d
@ plttbl3
GOTO LOOP
