! @(#)assign.prg	19.1 (ES0-DMD) 02/25/03 14:08:44
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure assign.prg for ASSIGN/PRINT, /INPUT, /DISPLAY, /DEFAULTS
! K. Banse	910404, 920423, 930714, 940511, 021213
!
! execute via ASSIGN/qualif device
! qualif = PRINT, INPUT, DISPLAY, DEFAULTS
! device = TERMINAL, LPRINT, LASER, COLOUR, SLIDE, VERSA, DISPLAY
!     or = FILE file_name
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! branch according to qualifier...
branch mid$cmnd(11:12) PR,IN,DI,DE PRINT,INPUT,DISPLAY,DEFAULTS
!
! invalid qualifier - display relevant message
write/error 3
return
!
! handle ASSIGN/PRINT
!
PRINT:
define/param p1 LPRINT ? "Enter print device: "
!
define/local symbol/c/1/60 " " all
write/keyw mid$prnt/c/1/60 " " all		!clear keyword first
!
if p1(1:3) .eq. "TER" then
   write/keyw mid$prnt T:User_Terminal 		!User terminal
!
elseif p1(1:3) .eq. "LAS" then
   symbol = m$symbol("PLASER")			!use procedure for
   write/keyw mid$prnt "L:{SYMBOL(1:58)}"	!Laser printers
!
elseif p1(1:3) .eq. "FIL" then
   define/param p2 ? ? "Enter filename: "
   write/keyw mid$prnT F:{p2} 			!user defined file
!
elseif p1(1:3) .eq. "PRI" then
   symbol = m$symbol("LPRINT")			!use procedure for
   write/keyw mid$prnt "P:{symbol(1:58)}"	!Line printers
!
else						!the default
   symbol = m$symbol(p1)
   if symbol(1:1) .eq. " " then                
      if aux_mode(1) .lt. 2 then
         write/keyw mid$prnt "P:PRINT/QUEUE={p1}"
      else
         write/keyw mid$prnt "P:{p1(1:58)}"
      endif
   else
      write/keyw mid$prnt "P:{symbol(1:58)}"
   endif
endif
return
!
! handle ASSIGN/INPUT
!
INPUT:
write/keyw mid$in/c/1/20 " " all			!clear keyword first
if p1(1:1) .eq. "F" then
   define/param p2 ? ? "Enter filename: "
   write/keyw mid$in F:{p2}
else
   if p1(1:1) .ne. "T" -
      write/out "Unsupported device - defaulted to TERMINAL"
   write/keyw mid$in T:USER_TERM 		!User terminal
endif
return
!
! handle ASSIGN/DISPLAY
!
DISPLAY:
define/param p1 DISPLAY ? "Enter display device: "
!
define/local symbol/c/1/20 " " all
write/keyw mid$disp/c/1/20 " " all		!clear keyword first
define/local ll/i/1/2 0,0 
!
if p1(1:3) .eq. "LAS" then
   symbol = m$symbol("LASER")			!use full name of laser device
   write/keyw mid$disp L_{symbol}		!PostScript laser printer
!
elseif p1(1:3) .eq. "FIL" then
   define/param p2 ? ? "Enter filename: "
   write/keyw mid$disp F_{p2}
!
elseif p1(1:1) .eq. "D" then
   ll = m$index(p1,",")+1
   if ll .le. 1 then
      write/keyw ll/i/1/2 1,{dazdevr(10)}
   else
      write/keyw ll/i/2/1 {p1({ll}:)} 
   endif
   goto disp_check
! 
elseif m$tstno(p1) .eq. 1 then		!handle simple number as if d,no.
   write/keyw ll/i/1/2 1,{p1} 
   goto disp_check
! 
elseif p1(1:3) .eq. "COL" then
   symbol = m$symbol("COLOUR")			!use full name
   write/keyw mid$disp C_{symboL}		!colour Postscript laser printer
! 
elseif p1(1:3) .eq. "SLI" then
   symbol = m$symbol("SLIDE")			!use full name
   write/keyw mid$disp C_{symbol}		!colour filme recorder
! 
else
   write/keyw mid$disp L_{p1}			!the default
endif
return
! 
disp_check:
write/keyw mid$disp I_ImageDisplay
! 
if ll(2) .lt. 0 .or. ll(2) .gt. 9 goto error_back
set/forma i1
!
!  check, if we have a display...
if mid$sess(6:6) .eq. " " .or. mid$sess(6:6) .eq. "-" then
   write/out "display window {ll(2)} created by MIDAS ..."
   create/display {ll(2)}
else
   @ creadisp.prg_o {ll(2)} -999	!execute the former switch/display
endif
return
!
!
! handle ASSIGN/DEFAULTS - set default devices
!
DEFAULTS:
define/local symbol/c/1/60 " " all
symbol = m$symbol("LPRINT")			!default is line printer
write/keyw mid$prnt "P:{symbol(1:58)}"
write/keyw mid$in T:User_Terminal		!User terminal
write/keyw mid$disp I_ImageDisplay		!image display is default
write/keyw mid$plot/c/1/20 GRAPH_WND{mid$sess(7:7)}
write/keyw mid$plot/c/21/10 "NOSPOOL"	        !default to spool
return
! 
ERROR_BACK:
write/out "Invalid input..." 
