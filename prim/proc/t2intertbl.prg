! @(#)t2intertbl.prg	19.1 (ESO-DMD) 02/25/03 14:09:06
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!       t2intertbl.prg
!
!.PURPOSE
!       Spline interpolation in one dimension
!
!.KEYWORDS
!       IMAGES, TABLES, SPLINE INTERPOLATION
!
!
!.CALLING SEQUENCE
!               " target source [ref] function parameters option"
!
!       INTERPOLATE/II outima inima refima [s] [degree] [deriv]
!       INTERPOLATE/TI outima intab :i,:d refima [s] [degree] [deriv]
!       INTERPOLATE/IT outtab :i,:d inima [s] [degree] [deriv]
!       INTERPOLATE/TT outtab :i,:d intab :i,:d [s] [degree] [deriv]
!
!             outima, outtab, inima, intab  =  name of output/input files
!             refima = (for image output only) the reference image
!             :i      column name for independent variable
!             :d      column name for dependent variable
!
!------------------------------------------------------------------------------
!
WRITE/KEYW HISTORY "{MID$CMND(1:6)}/{MID$CMND(11:14)} "
BRANCH MID$CMND(11:12) II,TI,IT,TT II,TI,IT,TT
!
! option II
!
II:					! default to Image Image
  DEFINE/PARAM P1 ? IMA "Enter output image:"
  DEFINE/PARAM P2 ? IMA "Enter input image:"
  DEFINE/PARAM P3 ? IMA "Enter reference image:"
  DEFINE/PARAM P4 1.
  DEFINE/PARAM P5 3
!  DEFINE/PARAM P6 0
!
  WRITE/KEYW OUT_A {P1}
  WRITE/KEYW IN_A {P2}
  WRITE/KEYW IN_B {P3}
  WRITE/KEYW INPUTR/R/1/3 {P4},{P5},0
!
  GOTO RUN_IT
!
! option IT
!
IT:					! Image to Table
  DEFINE/PARAM P1 ? TABLE "Enter output table:"
  DEFINE/PARAM P2 ? CHAR "Enter reference columns:"
  DEFINE/PARAM P3 ? IMA "Enter input image:"
  DEFINE/PARAM P4 1.
  DEFINE/PARAM P5 3
!  DEFINE/PARAM P6 0
!
  WRITE/KEYW OUT_A {P1}          	 ! outtab
  WRITE/KEYW COLO/C/1/53 {P2}            ! :i,:d
  WRITE/KEYW IN_A {P3}            	! inima
  WRITE/KEYW INPUTR/R/1/3 {P4},{P5},0
!
  GOTO RUN_IT
!
! option TI
!
TI:					! Table to Image
  DEFINE/PARAM P1 ? IMA "Enter output image:"
  DEFINE/PARAM P2 ? TABLE "Enter input table:"
  DEFINE/PARAM P3 ? CHAR "Enter reference columns:"
  DEFINE/PARAM P4 ? IMA "Enter reference image:"
  DEFINE/PARAM P5 1.
  DEFINE/PARAM P6 3
!  DEFINE/PARAM P7 0
!
  WRITE/KEYW OUT_A {P1}           	! outima
  WRITE/KEYW IN_A {P2}            	! intab
  WRITE/KEYW COLI/C/1/53 {P3}            ! :i,:d
  WRITE/KEYW IN_B {P4}            	! refima
  WRITE/KEYW INPUTR/R/1/3 {P5},{P6},0
!
  GOTO RUN_IT
!
! option TT
!
TT:					! Table to Table
  DEFINE/PARAM P1 ? TABLE "Enter output table:"
  DEFINE/PARAM P2 ? CHAR "Enter reference columns:"
  DEFINE/PARAM P3 ? TABLE "Enter input table:"
  DEFINE/PARAM P4 ? CHAR "Enter reference columns:"
  DEFINE/PARAM P5 1.
  DEFINE/PARAM P6 3
!  DEFINE/PARAM P7 0
!
  WRITE/KEYW OUT_A {P1}           	! outtab
  WRITE/KEYW COLO/C/1/53 {P2}            ! :i,:d
  WRITE/KEYW IN_A {P3}            	! intab
  WRITE/KEYW COLI/C/1/53 {P4}            ! :i,:d
  WRITE/KEYW INPUTR/R/1/3 {P5},{P6},0
!
RUN_IT:
RUN MID_EXE:tintertbl
