! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2copytbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!  901108 KB:   WRITE/KEYW HISTORY stuff...
!  930204 MP: call C routines
! .PURPOSE
!
!    implements
!
!  COPY/TI  table image [column_label]
!  COPY/IT  image table [column_label]
!
! 080407	last modif
! 
! ----------------------------------------------------------------------
!
if mid$cmnd(11:11) .eq. "T" then
   define/param p1 ? TAB  "Input table:"
   define/param p2 ? IMA  "Output image:"
   define/param p3 + CHAR
   write/keyw mid$cmnd/c/1/4 "coti"

else
   define/param p1 ? IMA  "Input image:"
   define/param p2 ? TAB  "Output table:"
   define/param p3 + CHAR
   write/keyw mid$cmnd/c/1/4 "coit"
ENDIF
!
write/keyw in_a {p1}
write/keyw out_a {p2}
write/keyw inputc {p3}
write/keyw history "{mid$cmnd(1:4)}/{mid$cmnd(11:14)} "
!
write/keyw action/c/1/1  {mid$cmnd(11:11)}
!
run MID_EXE:tdatatbl
