! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (c)   European Southern Observatory
!.IDENT       outfits.prg
!.AUTHOR      P.Grosbol,   ESO/DPG
!.PURPOSE     Convert disk files in MIDAS format to single FITS file
!.USAGE       OUTDISK/SFITS  in1[,in2[,..]] [out] [flags] [keyw_copy_flag]
!        or   OUTDISK/SFITS  in.cat [out] [flags] [keyw_copy_flag]
!.VERSION     1.0   1998-May-14 : Creation,  PJG
! 	      KB 060127		last modif
! ------------------------------------------------------------------
!
define/param p1 * ? "Enter MIDAS file names : "
if p1(1:1) .eq. "&" then                !check for &a, &b, ...
   define/param p8 {p1} ima
   write/keyw p1 {p8}
endif
! 
define/local extidx/i/1/1 0
define/param p3 OSN     ? "Enter format,display,cut flags: "
define/parameter p4 + ? "Enter keyword copy flag:"
if p4(1:2) .ne. "CO" goto process_files
!
! for a single table file `blabla.tbl'
! copy all FITS keywords from secondary header to prime header
! 
define/local inname/c/1/120 {p1}
define/local fitsname/c/1/120 " " all
!
extidx = m$indexb(p1,",")
if extidx .gt. 0 then
   write/error 100
   return/exit
endif
! 
extidx = m$indexb(p1,".")
if extidx .eq. 0 then			!add type `.tbl' if needed
   inname = p1//".tbl"
else
   inname = p1
endif
! 
if p2(1:1) .eq. "?" then		!build output name with default type
   extidx = m$indexb(inname,".")
   fitsname = inname(1:{extidx})//"tfits"
else if p2(1:1) .eq. "." then		!build output name with given type
   extidx = m$indexb(inname,".")
   fitsname = inname(1:{extidx})//{p2(2:)}
else
   fitsname = p2
endif
!
create/image empty.bdf 1,1 0.,0. nodata +
empty,NAXIS = 0
empty,NPIX = 0
!
! copy FITS keywords from secondary table header to prime h. of empty image
copy/dd {inname} *,5 empty.bdf
outdisk/sfits empty.bdf,{inname} {fitsname} {p3}	!recursive call (once)
-delete empty.bdf
return
! 
! here the usual section for OUTDISK/SFITS
! 
process_files:
define/param p2 .fits   ? "Enter FITS file name: "
! 
define/local snam_counter/i/1/1 1 
define/local snames1/c/1/400 " " all	+lower
! 
! if p1 is a catalog, build up input string
extidx = m$filtyp(p1," ")
if extidx .eq. 9 then			!Yes, it's a catalog
   set/format i1
   define/local catal/i/1/1 0
   define/local fillname/c/1/8 "snames{snam_counter} "
   in_b(1:1) = ","

  cat_loop:
   store/frame in_a {p1} 1 finito
   if catal .eq. 1 then
      {fillname} = in_a
   else
      in_b(2:) = in_a
      inputi =  m$len({fillname})		!check length before appending
      inputi(2) =  m$len(in_b)
      inputi(3) = inputi(1) + inputi(2)
      if inputi(3) .gt. 400 then
         snam_counter = snam_counter + 1
         if snam_counter .gt. 10 then
            write/error 100,APPLIC "Too many files in catalog..."
            return/exit
         endif
         ! write/out "length = {inputi} => switch to snames{snam_counter}"
         define/local snames{snam_counter}/c/1/400 " " all +lower
         fillname = "snames{snam_counter} "
         {fillname} = in_a
      else
         {fillname} = {fillname}//in_b		!this works better
      endif
   endif
   goto cat_loop;
   ! 
  finito:
else
   write/keyw snames1 {p1}
endif
!
run MID_EXE:OUTFITS
