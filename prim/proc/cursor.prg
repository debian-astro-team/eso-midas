! @(#)cursor.prg	19.1 (ES0-DMD) 02/25/03 14:08:48
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  cursor.prg  to implement SET/CURSOR and SET/GCURSOR
! K. Banse	901220, 910429, 920305, 930324, 941031, 991215
!
! use via SET/CURSOR curs_no curs_form coords flag
! or      SET/GCURSOR curs_no curs_form
!
! where   curs_no = 0, 1 or 2 (for both cursors) or 3 for 2 independent cursors
!         curs_form = C_HAIR
!                     CROSS,WHITE or CROSS,BLACK
!                     OPEN_CROSS,WHITE or OPEN_CROSS,BLACK
!                     CIRCLE
!                     ARROW,WHITE
!                     RECTANGLE,WHITE or  RECTANGLE,BLACK
!                     
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 0 N "Enter cursor no. - 0 = #1, 1 = #2, 2 = both: "
define/param p2 CROSS,WHITE C "Enter cursor_form: "
define/param p3 + C "Enter cursor coords.: "
define/param p4 S C "Enter coords. reference - F(rame) or S(creen): "
!
if mid$cmnd(11:11) .eq. "G" then
   action = "CG"
   p3(1:1) = "+"			!force to "+" for graphics cursors
else
   action = "CD"
endif
! 
define/local m/i/1/1 0
! 
!  DAZIN(1) = internal no.
!		100 - single cursor
!		101 - ROI (2 cursors used for that)
!  DAZIN(2) = cursor no.
!		0, 1 - single cursor #0, #1
!		2    - both cursors used for ROI
!		3    - both cursors independently (e.g. EXTRACT/TRACE)
!  DAZIN(3) = shape
!		for single cursor:
!			1 - cross_hair
!			2 - closed cross
!			3 - open cross
!		for two cursors (ROI):
!			1 - rectangle
!			2 - circle
!  DAZIN(4) = colour
!		0 - black+white
!		1 - black
!		2 - white
! 
branch p2(1:2) RE,CR,OP,CI,AR,C_ REC,CROSS,OPEN_CR,CIRCLE,ARROW,CROSS_H
!
! default to single cursor, white cross
!
write/keyw dazin 100,{p1},0,{p2}
dazin(4) = dazin(4)+1
goto run
! 
! handle rectangle
!
REC:
write/keyw dazin 101,2,1,2		!no. = 101 for ROI, rect. shape = 1
m = m$index(p2,",")+1
if m .gt. 1 .and. p2({m}:{m}) .eq. "B" then
   write/keyw dazin 101,2,1,1
endif
if p3(1:1) .eq. "+" write/keyw p3 50,50,80,80
goto run		
!
! handle cross 
!
CROSS:
write/keyw dazin 100,{p1},2,2
m = m$index(p2,",")+1
if m .gt. 1 .and. p2({m}:{m}) .eq. "B" then
   write/keyw dazin 100,{p1},2,1
endif
goto run
!
! handle large cross_hair
!
CROSS_H:
write/keyw dazin 100,{p1},1,2
goto run		
!
! handle open cross
!
OPEN_CR:
write/keyw dazin 100,{p1},3,2
m = m$index(p2,",")+1
if m .gt. 0 .and. p2({m}:{m}) .eq. "B" then
   write/keyw dazin 100,{p1},3,1
endif
goto run
!
! handle circles
!
CIRCLE:
if ididev(18) .ne. 11 then
   write/out Circle only supported with X11 ...
   goto rec
else
   write/keyw dazin 101,2,2,2		!currently only 2-cursor circle-ROI
   goto run
endif
!
! handle arrow
!
ARROW:
write/keyw dazin 100,{p1},7,2
!
RUN:
if action(1:2) .eq. "CG" then
   define/local dazh/i/1/2 {dazhold(1)},{dazhold(2)}
   run MID_EXE:IDAUXZ
   copy/keyw dazh/i/1/2 dazhold/I/1/2
else
   run MID_EXE:IDAUXZ
endif
