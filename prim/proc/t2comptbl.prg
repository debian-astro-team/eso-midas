! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2comptbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!
! .PURPOSE
!
!    implements
!
!  COMPUTE/TABLE table-name  column = expression
!
!.VERSION
!  070810	last modif
! ----------------------------------------------------------------------
!
define/param p1 ? table  "Enter table:"
define/param p2 ? char   "Enter column label:"
!
write/keyw history "{mid$cmnd(1:4)}/{mid$cmnd(11:14)} "
write/keyw in_a {p1}
!
set/format i1
define/local string/c/1/200 {p2} 
define/local l/i/1/2   0,0
define/local cc/c/1/1 "
!
if pcount .ge. 3 then
  do l = 3 {pcount}
    l(2) = m$index(p{l},cc)
    if l(2) .gt. 0 then
       write/keyw string {string}{p{l}}
    else
       write/keyw string "{string}{p{l}}"
    endif
  enddo
endif
!
run MID_EXE:tdatatbl
