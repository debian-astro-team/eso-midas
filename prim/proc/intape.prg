! @(#)intape.prg	19.1 (ESO-DMD) 02/25/03 14:08:53
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (c) 1990  European Southern Observatory
!.TYPE        Procedure
!.IDENT       intape.prg
!.AUTHOR      P.Grosbol,   ESO/IPG
!.PURPOSE     Convert disk or tape files in FITS/IHAP format into
!             the internal MIDAS format
!.USAGE       INTAPE  file_list file_id  device options
!.VERSION     1.0   1988-Sep-06 : Creation,  PJG
!.VERSION     1.1   1989-Feb-06 : Add list option,  PJG
!.VERSION     1.2   1990-Jan-08 : Add/fix option INTAPE/BACK  KB
!.VERSION     1.3   1990-Mar-21 : Change default flag to SOY, PJG
!.VERSION     020730	last modif
! ------------------------------------------------------------------
!
crossref FILES NAME DEVICE FLAGS
!
if p1 .eq. "?" then
   write/out -
   "enter your file specifications in ascending" order (e.g. 17,22-34,89)
   inquire/keyw p1 "for file 17, 22 through file 34 and file 89:"
endif
!
define/param p2 ? ? "Enter your file identification, max. 4 chars. :"
!
if p3 .eq. "?" then
   write/out "enter magtape unit (e.g. TAPE1) if reading from magtape"
   inquire/keyw p3 "or FITS file name:"
endif
!
define/param p4 SOY ? "Enter print,format,history option:"
!
if mid$cmnd(11:13) .eq. "LIS" then
   write/keyw p4/c/2/2 NN
endif
!
if aux_mode(13) .eq. 1 then           !we're in true FITS environment
   write/out "We are already working directly with FITS files..."
   write/out "FITS INTAPE module skipped!"
   write/out "For splitting up all extensions to single FITS files," -
             "use INDISK/MFITS."
   return/exit				!get out all the way
else
   run MID_EXE:INTAPE
endif

