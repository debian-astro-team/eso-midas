! @(#)setplot.prg	19.1 (ES0-DMD) 02/25/03 14:09:01
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION:  SETPLOT.PRG
!.AUTHOR:  R.H. Wamels - ESO Garching
!.PURPOSE: set the plot charateristics by executing the command :
!          SET/PLOT [PAR1[=value1]] [PAR2[=value2]] [PAR3[=value3]] ...
!
!.VERSION  1.1      94/05/11 made some small changes, RvHees
! -----------------------------------------------------
!
RUN MID_EXE:SETGRAP
