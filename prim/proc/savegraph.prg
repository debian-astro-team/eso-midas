! @(#)savegraph.prg	19.1 (ESO-DMD) 02/25/03 14:09:00
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: SAVEGRAPH.PRG
!.PURPOSE: MIDAS procedure for saving the graphic setup.
!.AUTHOR:  R.H. Warmels, ESO - Garching
!.USE:     execute via the command SAVE/GRAPH [save_file]
!.VERSION: 920522  RHW  date of creation
! --------------------------------------------------------------------------
IF MID$PLOT(41:44) .EQ. "none" THEN
   WRITE/OUT "*** FATAL: Plotfile name and plot setup unknown"
   RETURN


ELSEIF MID$PLOT(41:45) .EQ. "undef" THEN
   WRITE/OUT "*** FATAL: Plotfile name and plot setup unknown"
   RETURN

ELSE
   IF M$EXIST("{MID$PLOT(41:>)}") .EQ. 0 THEN
      WRITE/OUT "*** INFO: Plotfile doesn't exist; do you delete it?"
   ENDIF
   DEFINE/PARAMETER P1 {MID$PLOT(41:>)}sav                         ! save file
   WRITE/OUT "*** INFO: Graphic setup will be stored in file {P1}"
ENDIF

CREATE/IMAGE {P1} 1,1 ? NODATA
COPY/KD PLCSTAT/C/1/80  {P1} PLCSTAT/C/1/80
COPY/KD PLRSTAT/R/1/40  {P1} PLRSTAT/R/1/40
COPY/KD PLISTAT/I/1/40  {P1} PLISTAT/I/1/40
COPY/KD PLCURSOR/C/1/60 {P1} PLCURSOR/C/1/60
COPY/KD MID$PLOT/C/1/80 {P1} MID$PLOT/C/1/80



