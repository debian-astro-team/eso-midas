! @(#)hardcopy.prg	19.1 (ESO-DMD) 02/25/03 14:08:52
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure hardcopy.prg to load an image frame to a hardcopy device
!
! currently supported devices are: 
!    PostScript laser printer (grayscale + colour)
!    Versatec (only implemented for VAX)
!
! K. Banse	901204, 910213, 920327, 930623, 940517, 020226
!
! executed via   @ hardcopy device image ITT_def LUT print_flag print_mode 
! with  print_mode abcde, a = P/L, b = B/C, c = 4/8, d = N/B, e=T/Z
!       Z means no text, just the image
!
! called from loadima.prg or from vcopy.prg
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 {mid$disp(3:>)} C "Enter display device: "
define/param p2 * IMA "Enter frame: "		!default  to displayed image
define/param p5 PRINT  C   "Print flag, PRINT/NOPRINT: "
!
define/local mytabl/c/1/40 "{hcittlut(1:40)}"	!to save ITT + LUT
define/local color/c/1/2 BB			!default to b+w
! 
if p1(1:4) .ne. "NONE" then
   @ checkdev.prg_o {p1}			!check device type
else
   if p5(1:1) .ne. "P" then			!if print_flag = NOPRINT 
      if p6(2:2) .eq. "C" then
         color = "CC"				!adapt to print_mode
         goto step_2
      endif
   endif
   @ checkdev.prg_o LASER		!if we print default to laser printer
endif
! 
define/local cname/c/1/40  {q1}			!as returned
define/local agldefs/c/1/40 {q2}
define/local kk/i/1/1 0
kk = m$index(agldefs,"pscript.c")
if kk .gt. 0   color = "CC"
! 
step_2:
if idimemi(15) .gt. 99  write/keyw hcittlut/c/1/20 "ramp "
!
!  here for grayscale laser
!
if color(1:1) .eq. "B" then
   define/param p3 N C "Enter ITT_def, N or P or I,ITT_name: "
   if p3(1:1) .eq. "I" write/keyw hcittlut/c/1/20 {p3(3:>)}
   define/param p6 PB8NT C -
   "Enter print_mode abcde, a = P/L, b = B/C, c = 8/4, d = N/B, e = T/Z: "
   p6(2:2) = "B"			!force to b+w
else
! 
!  here for colour laser  or film recorder
!
   define/param p3 P C "Enter ITT_def, N or P or I,ITT_name: "
   if p3(1:1) .eq. "I" write/keyw hcittlut/c/1/20 {p3(3:>)}
   define/param p4 + C "Enter LUT: "
   if p4(1:1) .eq. "+" then
      wait/secs 0			!NoOp, use HCITT(21:40) as it is
   elseif p4(1:7) .eq. "DISPLAY" then
      write/keyw hcittlut/c/21/20 middumml.lut
   else
      write/keyw hcittlut/c/21/20 {p4}
   endif
   if color(2:2) .eq. "C" then
      define/param p6 PC8NT C -
      "Enter print_mode abcde, a = P/L, b = C/B, c = 8/4, d = N/B, e = T/Z: "
   else
      define/param p6 PC8BT C -
      "Enter print_mode abcde, a = P/L, b = C/B, c = 8/4, d = N/B, e = T/Z: "
   endif
   p6(2:2) = "C"			!force to colour
endif
! 
write/keyw in_a {p2}
run MID_EXE:PSBDF
write/keyw hcittlut/c/1/40 "{mytabl(1:40)}"
! 
if p5(1:1) .ne. "P" return		!test print_flag
! 
if aux_mode(1) .lt. 2 then
   $ PRINT/NOTIFY/QUEUE={cname} {out_a}
   write/out -
   "File {out_a} sent to" {cname} "this will take some time."
   write/out "A message will be sent when finished..."
else
    $ {syscoms(1:20)}{cname} {out_a}
   write/out "File {out_a} sent to" {cname} "this will take some time..."
endif
