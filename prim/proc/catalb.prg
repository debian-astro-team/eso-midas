! @(#)catalb.prg	19.1 (ESO-DMD) 02/25/03 14:08:44
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  catalb.prg   for CREATE, SET, CLEAR/xCAT
!
! K.Banse    910121, 920908, 970102, 990706, 000523
!
! use as CREATE/xCAT cat_name [dir_specs] [cat_descr]
!        SET/xCAT    cat_name 
!	 CLEAR/xCAT  
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local action/c/1/3 {mid$cmnd(1:3)}	!save command for catal.exe
define/local cattyp/c/1/1 {mid$cmnd(11:11)}	!save catalog type
!
define/local ioff/i/1/2 0,0
define/local mycat/c/1/{catalinf(11)} " " ALL
!
if cattyp .eq. "I" then			!Images
   if mid$cmnd(12:12) .eq. "M" then
      write/error 3
      return/exit
   endif
! 
   define/param p1 icatalog.cat I "Enter name of catalog:"
   define/local name/c/1/5 image
   define/local filtyp/c/1/3 bdf
   ioff(2) = 1
! 
elseif cattyp .eq. "T" then			!Tables
   define/param p1 tcatalog.cat I "Enter name of catalog:"
   define/local name/c/1/4 table
   define/local filtyp/c/1/3 tbl
   ioff(2) = 3
! 
elseif cattyp .eq. "F" then			!Fit files
   define/param p1 fcatalog.cat I "Enter name of catalog:"
   define/local name/c/1/8 fit_file
   define/local filtyp/c/1/3 fit
   ioff(2) = 4
! 
elseif cattyp .eq. "A" then			!ASCII files
   define/param p1 acatalog.cat I "Enter name of catalog:"
   define/local name/c/1/5 ASCII
   define/local filtyp/c/1/3 "*  "
   ioff(2) = 2
else
   write/error 3
   return/exit
endif
! 
inputi = m$index(p1,"*") + m$index(p1,"?")   !avoid create/xcat lola*.bdf ...
if inputi .gt. 0 then
   write/error 100
   return/exit
endif
! 
inputi = ioff(2)			!save catalog type in INPUTI
ioff = catalinf({inputi})
ioff(2) = ioff(2) + 5
! 
if m$ftset(p1) .ne. 1 then
   write/keyw mycat {p1}.cat
else
   write/keyw mycat {p1}
endif
! 
!  here for CLEAR/xCAT
!  -------------------
if mid$cmnd(1:2) .eq. "CL" then
   catalinf({ioff(2)}) = 0
   write/keyw catalogs/c/{ioff}/{catalinf(11)} " " ALL
   return
endif
!
write/keyw in_a {p1}
!
! here for SET/xCAT cat_name
!  -------------------------
if mid$cmnd(1:2) .eq. "SE" then
   catalinf({ioff(2)}) = 1
   write/keyw catalogs/c/{ioff}/{catalinf(11)} {mycat}
   write/out "Catalog {mycat} now the active {name} catalog."
   if m$exist(mycat) .ne. 1 -
      write/out "Warning: Catalog {mycat} does not exist yet..."
   return
endif
!
!  as default here for CREATE/xCAT
!  -------------------------------
define/param p2 + C "Enter file_specs, e.g. as3*.bdf: "
define/param p3 IDENT C "Enter char. descr used for Ident field:"
! 
if aux_mode .lt. 2 then			!in VMS we do purge ...
   $DELETE/NOLOG/NOCONF {mycat}.*	!try to delete catalog first
   $DELETE/NOLOG/NOCONF dirfile.ascii.*	!try to delete dirfile.ascii
   if error(2) .lt. 2 then
      write/out "We now try to purge the relevant files"
      write/out -
      "Please, ignore all Error/Warning messages from the operating system ..."
   endif
   $PURGE *.{filtyp}
! 
else
   if m$exist(mycat) .eq. 1 $rm -f {mycat}	!try to delete catalog first
   $rm -f dirfile.ascii			!try to delete dirfile.ascii
endif
! 
write/keyw in_b " " all			!clear keyword IN_B first
if p2(1:1) .eq. "+" then
   write/keyw in_b *.{filtyp}
else
   write/keyw in_b {p2}
endif
! 
if in_b(1:4) .ne. "NULL" then		!if root is given, build from there
   ioff = m$index(in_b,",:")		!check, if table,:label specified
   if ioff .lt. 1 ioff = m$index(in_b,",#")
   if ioff .gt. 1 then
      default(1:1) = "T"
   else
      ioff = m$index(in_b,".ascii")		!check, if file.ascii
      if ioff .lt. 1 then
         if aux_mode .lt. 2 then		!VMS 
            $DIREC/SIZE/OUT=dirfile.ascii {in_b}
         else					!Unix
            out_b = m$repla(in_b,","," ")	!replace all `,' by ` '
            $ls {out_b} > dirfile.ascii
         endif
      else
         if in_b .ne. "dirfile.ascii" then
            -copy {in_b} dirfile.ascii
         endif
      endif
      default(1:1) = "Y"		
   endif
else
   default(1:1) = "N"		
endif
!
run MID_MONIT:catal
