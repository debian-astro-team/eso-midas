#! /bin/sh
# @(#)reset.sh	19.1 (ESO-IPG) 02/25/03 14:09:00
#
# kill all the idiserv.exe's we can find
#
#  K. Banse    920525, 010206
#

cd $MID_WORK
rm -f midas_xw${1}* IDISERV*
rm -f idiserv${1}.log sxw${1}*.dat

KLAUS=`ps -ef | grep idiserv.exe | grep -v grep | awk '{print $2}'`

if [ -n "$KLAUS" ]; then
   kill -9 $KLAUS
fi


