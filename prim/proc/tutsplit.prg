! @(#)tutsplit.prg	19.1 (ESO-DMD) 02/25/03 14:09:12
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! procedure tutsplit.prg  to implement TUTORIAL/SPLIT
! K. Banse	910322, 911011, 920428, 980525, 990804
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/param P1 0 ?	"0: Local data/Auto" 
DEFINE/LOCAL OPTION/I/1/1 0

IF P1(1:4) .NE. "AUTO" THEN
   IF P1 .EQ. "+" P1 = "0"
   WRITE/KEYW OPTION/I/1/1 {P1}
ENDIF 
! 
! in X windows
! we have no split screen, but can create different display windows
! 
DEFINE/LOCAL COPFLA/I/1/1 {OPTION}
! 
WRITE/OUT "There is no inherent split screen capability on this workstation."
WRITE/OUT "But we can create and use different display windows."
WRITE/OUT "We'll show you how..."
! 
WRITE/OUT "We assume that the" "default display window is already"-
 " there ... !"
WRITE/OUT "Let's load a nice Lookup Table"
LOAD/LUT rainbow
DISPLAY/LUT
! 
WRITE/OUT "Now let's create more display windows and load images into them"
!
ECHO/ON
IF COPFLA .EQ. 0 then
   indisk/fits MID_TEST:spiral.fits middumma.bdf >Null
   indisk/fits MID_TEST:ccd.fits middummb.bdf >Null
   indisk/fits MID_TEST:sombrero.fits middummc.bdf >Null
endif
create/display 1 400,400,0,450 1,400,400 0
ECHO/OFF
WRITE/OUT "This command created a display with a single channel and no overlay"
WRITE/OUT "Now load the copied image file"
LOAD/image &a scale=1
! 
ECHO/ON
create/display 2 340,520,760,280 2,340,520 1
ECHO/OFF
WRITE/OUT "Since the CCD images have dimension 337*520 we create the display"
WRITE/OUT "accordingly. We also use an overlay channel."
WRITE/OUT "Display 2 is now the active display and all commands refer to it"
WRITE/OUT LOAD/image &b
LOAD/image &b scale=1
!
WRITE/OUT "Let's pick up some cursor values."
WRITE/OUT "Since the pixel size is so tiny, we'll use a zoomed cursor window"
WRITE/OUT create/zoom_window 2 400,400,360,440
create/zoom_window 2 400,400,360,440
WRITE/OUT get/cursor ? ? ? ? w,4 
get/cursor ? ? ? ? w,4 
! 
WRITE/OUT "Now let's return to the default display, display 0 "
WRITE/OUT "We use   assign/display   to make display 0 the active one"
ECHO/ON
assign/display d,0
load/image &c scale=1
ECHO/OFF
display/lut
!
IF P1(1:4) .EQ. "AUTO" THEN
  -DELETE middumma.bdf
  -DELETE middummb.bdf
  -DELETE middummc.bdf
ENDIF
WRITE/OUT "To get rid of all the windows, use  reset/display  "
WRITE/OUT End of TUTORIAL/SPLIT...
