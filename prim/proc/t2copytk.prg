! @(#)t2copytk.prg	19.1 (ES0-DMD) 02/25/03 14:09:04
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2copytk.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 jan 1990
!  901108  KB  fix WRITE/KEYW HISTORY stuff...
!
! .PURPOSE
!
!    implements
!
!  COPY/TK    table-name [column ...] row keyword
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
WRITE/KEYW MID$CMND/C/1/4 "TKCP"
!
RUN MID_EXE:tdatatbl
