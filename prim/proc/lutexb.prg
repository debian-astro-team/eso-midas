! ++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure lutexb.prg
! K. Banse	900703, 030704
!
! demonstrate different colour lookup tables
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
write/keyw out_a {p1}		!save plotflag (Plot or NoPlot)
!
write/out "Some of the existing colour lookup tables are:"
write/out
write/out aips0, random6, isophot, manycol, pastel, rainbow2, smooth, smooth2
write/out stairs8, standard
write/out
!
if ididev(7) .ge. 2 then
   define/local reload/i/1/1 1 ? +low   !for Pseudo on RGB we must reload
else
   define/local reload/i/1/1 0 ? +low
endif
!
set/lut					!enable lookup tables
display/lut				!display colour bar
!
@ lutexb,sublut aips0
@ lutexb,sublut random6
@ lutexb,sublut isophot
@ lutexb,sublut manycol
@ lutexb,sublut pastel
@ lutexb,sublut rainbow2 
@ lutexb,sublut smooth
@ lutexb,sublut smooth2
@ lutexb,sublut stairs8
@ lutexb,sublut standard
! 
write/out
write/out "To see all existing LUTs, use the host command"
write/out  -dir MID_SYSTAB:*.lut
!
! here entry SUBLUT
!
entry sublut
! 
write/out LOAD/LUT {p1} yields:
load/lut {p1} 
if reload .eq. 1 load/image {idimemc}   !we know that there is an image...
!
if out_a(1:1) .eq. "P" @a plottab,lut {p1}
