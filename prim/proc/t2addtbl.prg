! @(#)t2addtbl.prg	19.1 (ESO-DMD) 02/25/03 14:09:03
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2addtbl.prg
!  TABLE Subsystem
!  M.Peron               ESO - Garching     18 01 93
!
! .PURPOSE
!
!    implements
!
!  CREATE/ROW   table-name  irow  nrow
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
DEFINE/PARAM P2 ? CHAR  "Enter row position:"
DEFINE/PARAM P3 ? CHAR  "Enter number of rows to be added:"
!
WRITE/KEYW MID$CMND/C/1/4 "ADD "
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:TDATATBL
