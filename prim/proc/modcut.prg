! @(#)modcut.prg	19.1 (ESO-DMD) 02/25/03 14:08:54
! +++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  modcut.prg  to implement MODIFY/CUTS
! K. Banse	890630, 950929
! 	
! execute via MODIFY/CUTS [image] [cursor]
! where 
!        image = name of image to load first
!        cursor = C(ursor) to indicate that we work on cursor windows
!                 N(o(cursor) we work on the full image
!
! +++++++++++++++++++++++++++++++++++++
!
define/param p1 + ima "Enter image to load:"
define/param p2 No C -
"Enter Cursor/Nocursor for working on Cursor windows or whole image:"
! 
if p1(1:1) .ne. "+" load/image {p1}
! 
dattim = m$time()
if dazdevr(11) .lt. 0 then
   create/graphics 0 925,425,0,469  !make sure we have a graphics window
endif
! 
action = "MC"
run MID_EXE:MODIF
