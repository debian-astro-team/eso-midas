! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  MIDAS procedure viewluts.prg 
!  to store the LUTs heat, rainbow1, backgr and random4
!  as well as the ITTs neg, log, gamma and neglog in frame viewluts.aux
!  this frame is needed for VIEW/IMA
!  K. Banse	ESO - Garching
!               901210, 910911, 920318, 930618, 101018
! 
!  use via @ viewluts
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  no. of lines: 4*3 (LUTs) + 4*1 (ITTs) = 16
! 
create/image viewluts.aux 2,256,16 1.,1.,1.,1.
! 
!  get the LUTs  heat, rainbow1, backgr, random4
! 
-COPY {bindir}heat.lut view1.tbl
-COPY {bindir}rainbow1.lut view2.tbl
-COPY {bindir}backgr.lut view3.tbl
-COPY {bindir}random4.lut view4.tbl
! 
copy/ti view1.tbl &a
insert/image &a viewluts.aux <,<
copy/ti view2.tbl &a
insert/image &a viewluts.aux <,4.
copy/ti view3.tbl &a
insert/image &a viewluts.aux <,7.
copy/ti view4.tbl &a
insert/image &a viewluts.aux <,10.
! 
!  get the ITTs  neg, log, neglog
! 
-COPY {bindir}neg.itt view1.tbl
-COPY {bindir}log.itt view2.tbl
-COPY {bindir}neglog.itt view3.tbl
-COPY {bindir}gamma.itt view4.tbl
! 
copy/ti view1.tbl &a
insert/image &a viewluts.aux <,13.
copy/ti view2.tbl &a
insert/image &a viewluts.aux <,14.
copy/ti view3.tbl &a
insert/image &a viewluts.aux <,15.
copy/ti view4.tbl &a
insert/image &a viewluts.aux <,16.
! 
! copy viewluts.aux to MID_SYSTAB and delete all temporary files
! 
if aux_mode(1) .le. 1 then              ! VMS
   $ SET PROT=W:RWE viewluts.aux
else                                    ! UNIX
   $ chmod 666 viewluts.aux
endif
-COPY viewluts.aux {bindir}viewluts.aux
! 
delete/image viewluts.aux no
delete/image &a no
delete/table view1.tbl no
delete/table view2.tbl no
delete/table view3.tbl no
delete/table view4.tbl no
