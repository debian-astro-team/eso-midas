! @(#)t2creaview.prg	19.1 (ES0-DMD) 02/25/03 14:09:05
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS procedure  t2creaview.prg to implement CREATE/VIRTUAL
! 
!  use via CREATE/VIRTUAL  virtual reference_table
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE  "Enter virtual:"
DEFINE/PARAM P2 ? TABLE  "Enter refrence_table:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
!
WRITE/KEYW MID$CMND/C/1/4 "CREV"
RUN MID_EXE:tdatatbl
