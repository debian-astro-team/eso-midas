! @(#)idisave.prg	19.1 (ESO-DMD) 02/25/03 14:08:52
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  procedure  idisave.prg     
!  K.Banse       900529, 911220, 920723, 930121, 940128, 950208, 980127
!  to create the frame idisave.dat
!  which is needed for "create/display" command (X Windows)
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! the default value of p1 is `MAX_DEV' of idi.h (has to be in sync!)
! 
define/param p1 12 number "Enter the no. of displays to support:"
define/local dno/i/1/2 {p1},0
! 
create/image idisave.dat 1,10
! 
write/descr idisave.dat ident -
"this file keeps the info for all opened display/graphics windows"
! 
write/descr idisave.dat no_of_supported_displays/i/1/1 {dno(1)}
dno(2) = dno(1)*2
write/descr idisave.dat ididevnam/c/1/{dno(2)} " " all
write/dhelp idisave.dat ididevnam "opened display IDs, 2 chars. each"
dno(2) = dno(1)*50
write/descr idisave.dat ididevi/i/1/{dno(2)} 0 all
write/dhelp idisave.dat ididevi "keyword ididev for display IDs, 50 elems. each"
dno(2) = dno(1)*20
write/descr idisave.dat dazdevr/i/1/{dno(2)} 0 all
write/dhelp idisave.dat dazdevr -
"keyword dazdevr for display IDs, 20 elems. each"
dno(2) = dno(1)*42
write/descr idisave.dat hcittlut/c/1/{dno(2)} " " all
write/dhelp idisave.dat hcittlut -
"keyword hcittlut for display IDs, 42 elems. each"
dno(2) = dno(1)*40
write/descr idisave.dat plrgrap/r/1/{dno(2)} 0 all
write/dhelp idisave.dat plrgrap "keyword plrgrap for display IDs, 40 elems. each"
dno(2) = dno(1)*80
write/descr idisave.dat plcmeta/c/1/{dno(2)} " " all
write/dhelp idisave.dat plcmeta -
"keyword plcmeta for display IDs, 80 elems. each"
write/descr idisave.dat plcdata/c/1/{dno(2)} " " all
write/dhelp idisave.dat plcdata -
"keyword plcdata for display IDs, 80 elems. each"
! 
if aux_mode .lt. 2 then				!VAX/VMS
   $copy idisave.dat MID_INCLUDE:idisave.dat
   $delete/noconf/nolog idisave.dat.*
else						!Unix
   $cp idisave.dat $MID_INCLUDE/idisave.dat
   $rm idisave.dat
endif
