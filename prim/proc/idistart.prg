! @(#)idistart.prg	19.1 (ESO-DMD) 02/25/03 14:08:52
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure  idistart.prg  to start the IDI server
!  K. Banse     000221
!
!.VERSION
! 021021	last modif
!  
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
if ididev(1) .ge. 0 return		!nothing to do ...
! 
if m$symbol("DISPLAY") .eq. "DISPLAY" then
   ididev(1) = 0
   ididev(13) = 8		!no DISPLAY variable => default
   write/out Warning: DISPLAY var. not set...
   return
endif
! 
if dazdevr(18) .lt. 0 then		!no Color Mode set yet
   init/display ? ? ? ? ? {dazdevc}
endif
if ididev(1) .eq. -1 return		!IDI server already running
! 
! ... here for VMS ...
! 
if aux_mode .lt. 2 then                   !in VMS
   if mid$sess(12:12) .ne. "X" then
      $SPAWN/NOWAIT/OUTPUT=MID_WORK:idiserver.log -
          /process=idiserv{mid$sess(11:12)} -
          @ MID_MONIT:idistart {MID$SESS(11:12)}
   endif
   ! 
   $ COPY MID_INCLUDE:idisave.dat MID_WORK:idisave{MID$SESS(11:12)}.dat
   ididev(13) = 8		!for VMS default to 8 bits per pixel...
else
! 
! ... here for Unix ...
! 
   if mid$sess(12:12) .ne. "X" then
      $rm -f $MID_WORK/idiserv{MID$SESS(11:12)}.log
      set/format i1
      $ $MIDASHOME/$MIDVERS/system/exec/idiserv.exe {dazdevr(19)} &
   endif
   $cp $MID_INCLUDE/idisave.dat $MID_WORK/idisave{MID$SESS(11:12)}.dat
   $chmod 666 $MID_WORK/idisave{MID$SESS(11:12)}.dat
   !
endif
! 
ididev(1) = 0			!indicate that IDI server is running 
if mid$sys(1:3) .eq. "MAC" then
   wait/sec 1.0
else
   wait/sec 0.5			!server should not need more to start up...
endif
 
