! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! procedure tutlut.prg to implement TUTORIAL/LUT
! K. Banse	910425, 990804, 030707
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 NOPLOT ? "Enter plotflag Plot/NoPlot/Auto: "
define/local option/c/1/6 "PLOT"
! 
if p1(1:3) .eq. "NOP" then
  write/keyw option/c/1/6 "NOPLOT"
endif
!
! make sure, we have an image loaded ...
@ testima sombrero {p1}
!
clear/itt		!avoid any crazy ITT on Display
! 
write/out "We show the effect of some standard LUTs:"
@ lutexa {OPTION}
! 
write/out 
write/out "Put focus on the Midas display window (e.g. put the mouse in it)."
write/out "Use the arrow keys to modify the current LUT with a colour band"
write/out "Use the number keys to change the speed"
write/out "Press the mouse button to the right of the leftmost b. to exit"
! 
write/out modify/lut band red
modify/lut band red
load/image {idimemc}
write/out modify/lut band white
modify/lut band white
write/out
load/image {idimemc}
write/out "Finally, we rotate the complete LUT (again use the  arrow keys)"
write/out modify/lut rotate all
write/out
if p1(1:4) .eq. "AUTO" THEN
  -delete testima.bdf
  -delete graph_wnd000.plt
endif
modify/lut rotate all
load/image {idimemc}
! 
write/out "End of TUTORIAL/LUT"
