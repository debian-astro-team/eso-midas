! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS procedure : t2setrtbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!
! .PURPOSE
!
!    implements:   SET/REFCOL  table column
!	default column now: Sequence
!
! .VERSION
! 080418	last modif
! ----------------------------------------------------------------------
!
define/param p1 ? TABLE  "Enter table:"
define/param p2 : CHAR   "Enter column:"
!
write/keyw history "{mid$cmnd(1:4)}/{mid$cmnd(11:14)} "
!
run MID_EXE:tdatatbl
