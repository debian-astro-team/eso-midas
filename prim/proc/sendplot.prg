! @(#)sendplot.prg	19.1 (ESO-DMD) 02/25/03 14:09:01
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: SENDPLOT.PRG
!.PURPOSE: MIDAS procedure SENDPLOT for sending the plotfile to the hardcopy
!          device being the versatec(s), penplotter(s) or laser printer(s).
!.AUTHOR:  R.H. Warmels, ESO - Garching
!.USE:     execute via the command COPY/GRAPH [device] [meta_file]
!.VERSION: 870402  RHW  date of creation
!.VERSION: 920709  RHW  Rewritten to make in system dependent (all info 
!.                      now retrieved from agldevs.dat
!.VERSION: 930923  RHW  device check for all device; 
!.NOTE:    The procedure either takes the default output device stored in the 
!.         keyword MID$PLOT or the parameter P1.
!.         The device can either be a graphics window, a MIDAS logical name
!.         or a system device name. 
!          Graphical and display windows as well as simple terminal are taken
!          care of the the first part of this procedure. The second part deals
!          with the hardcopy devices.
!
!          All device checking is done the the separate procedure checkdev.prg.
!          A fatal message will be issued if the device is unknown or missing.
!          The file agldevs.dat describes the syntax used internally.
! 
! 020226	last modif
! 
!----------------------------------------------------------------------------
! 
define/parameter p1 default   c     !get the default device or input from user
define/parameter p2 {plcmeta} c     !take the plot file from PLCMETA
!
! *** local keywords
define/local nk/i/1/1  0            !window number
define/local dev/c/1/1  " "
set/format i1
!
! ***** here the part for default copy/graphics from other procedures
if p1(1:3) .eq. "DEF" then            ! P1 input comes from plot procedure
   write/keyw p1 {mid$plot(1:20)}
   if mid$plot(21:25) .eq. "NOSPO" return
endif
!
! *** here the part for the windows
if p1(1:1) .eq. "G" then                    !here for a graphics window
   if ididev(18) .ne. 11 then
      write/keyw p1 graph_term
   else
      nk = m$index(p1,",")+1
      if nk .le. 1 then
         write/keyw p1 graph_wnd{mid$sess(7:7)}
      else
         define/local gid/c/1/1 {p1({nk}:{nk})}
         @ creagra.prg_o {gid} -999
         write/keyw p1 graph_wnd{gid}
      endif
   endif
   dev = "D"

elseif p1(1:1) .eq. "T" then                    !here for Terminal assignment
   if ididev(18) .ne. 11 then
      write/keyw p1 graph_term
   else
      nk = m$index(p1,",")+1
      if nk .le. 1 then
         write/keyw p1 grap_wnd{mid$sess(7:7)}
      else
         define/local gid/c/1/1  {p1({nk}:{nk})}
         @ creagra.prg_o {gid} -999
         write/keyw p1 graph_wnd{gid}
      endif
   endif
   dev = "D"

elseif p1(1:1) .eq. "I" then                    !here for image display
   if ididev(18) .ne. 11 then
      write/keyw p1 image_displ
   else
      nk = m$index(p1,",")+1
      if nk .le. 1 then
         write/keyw p1 image_wnd{mid$sess(6:6)}
      else
         define/local gid/c/1/1 {p1({nk}:{nK})}
         @ creadisp.prg_o {gid} -999
         write/keyw p1 image_wnd{p1({nk}:>)}
      endif
   endif
   dev = "D"

elseif p1(1:1) .eq. "D" then                    !here for image display (I)
   if ididev(18) .ne. 11 then
      write/keyw p1 image_displ
   else
      nk = m$index(p1,",")+1
      if nk .le. 1 then
         write/keyw p1 image_wnd{mid$sess(6:6)}
      else
         define/local gid/c/1/1  {p1({nk}:{nk})}
         @ creadisp.prg_o {gid} -999
         write/keyw p1 image_wnd{p1({nk}:>)}
      endif
   endif
   dev = "D"
endif
!
if dev .eq. "D" then				!valid display device ?
   @ checkdev.prg_o {p1}			!use compiled code
   p1 = "{q1}"                                   
   run MID_EXE:copygrap				!redraw the meta file
   return
endif
!
! *** here we send the plot to the hardcopy devices
! 
@ checkdev.prg_o {p1}				!first, check the device
! 
define/local ind/i/1/1 0			!used for postscript option
ind = m$index(q2,".")				!look for portrait, landscape
p1 = "{q1}"
if ind .ne. 0 then
   if q2(1:3) .ne. "tkg" .and. q2(1:2) .ne. "hp" then
      p1 = "{q1}{q2({ind}:>)}"
   endif
endif
define/local device/c/1/20 {q1}                  !device name
define/local type/c/1/20 {q2}                    !contains the driver
!
! *** here for checking the harcopy devices
!     first the postscript (filter out the ecolour extension .c)
if type(1:2) .eq. "ps" then                      !postscript device
   if aux_mode(1) .lt. 2 then                    !delete previous files
      -delete PSCRPLOT.*.
   else
      -delete pscrplot*
   endif
!
   if device .eq. "POSTSCRIPT" then
       run MID_EXE:copygrap
      -rename pscrplot.0 {device}.ps
   else
      if type(8:9) .eq. ".c" then
         if aux_mode(1) .lt. 2 then
            run MID_EXE:copygrap
            -rename pscrplot.0 {device}.ps
            write/out -
            "*** WARNING: Plot file exists, BUT COLOUR printer not available"
         else
            run MID_EXE:copygrap
            -rename pscrplot.0 {device}.ps
            $ {syscoms(1:20)} {device} {device}.ps
         endif   

      else                                      !all other postscript devices
         if aux_mode(1) .lt. 2 then
            run MID_EXE:copygrap
            -rename pscrplot.0 {device}.ps
            $PRINT/QUEUE={DEVICE} {device}.ps
         else
            run MID_EXE:copygrap
            -rename pscrplot.0 {device}.ps
            $ {syscoms(1:20)} {device} {device}.ps
         endif
      endif
   endif
!
elseif type(1:2) .eq. "hp" then                  !HP penplotter
   if aux_mode(1) .lt. 2 then
      -delete HPGLPLOT.*.
      run MID_EXE:copygrap
      -rename hpglplot.0 {device}.hpgl
      write/out "*** WARNING: Plot file exists, BUT PENPLOTTER not available"
   else
      -delete hpglplot.*
      run MID_EXE:copygrap
      -rename hpglplot.0 {device}.hpgl
      $ {syscoms(1:20)} {device} {device}.hpgl
   endif
endif

