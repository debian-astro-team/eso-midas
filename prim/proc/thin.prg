! @(#)thin.prg	19.1 (ES0-DMD) 02/25/03 14:09:09
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  thin.prg  for APPLY/THIN
! K. Banse	890504
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? I "Enter input image: "
define/param p2 ? I "Enter output image: "
!
write/keyw in_a {p1}
write/keyw out_a {p2}
write/keyw action T
run MID_EXE:FLIP
