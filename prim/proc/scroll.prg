! @(#)scroll.prg	19.1 (ESO-DMD) 02/25/03 14:09:00
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  scroll.prg  to execute SCROLL/IMA, OVERLAY + CLEAR/SCROLL
! K. Banse	901005, 920312
!
! executed via CLEAR/SCROLL chanl + SCROLL/IMA,OVERLAY chanl scroll_values
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
if p1(1:1) .ne. "?" set/chan {p1}	!update IDIDEV(15), if chanl is given...
!
!      handle CLEAR/SCROLL ...
!
if mid$cmnd(11:11) .eq. "S" then
   default(1:1) = "C"
else
!
!   indicate, if also overlay channel to scroll
!
   default(2:2) = mid$cmnd(11:11)
   if p2(1:1) .eq. "?" then
      default(1:1) = "Y"		!scroll via joystick
   else
      default(1:1) = "N"		!fixed scroll values
      write/keyw dazin {p2}
   endif
endif
!
default(16:16) = "S"
action = "SC"
run MID_EXE:IDAUXZ
