! +++++++++++++++++++++++++++++++++++++
!
! command procedure dazinit.prg to implement command INITIALIZE/DISPLAY
! K. Banse      901221
! 
! execute via INITIALIZE/DISPLAY 
!             noLUTs,maxLUTsize,minLUTsize  ownLUT  M_unit
!             font_s,m,l  RGB_mode  display 24bit_flag special_cursor_mode
! 	
!.VERSION
! 110222	last modif
! 
! +++++++++++++++++++++++++++++++++++++
!
if mid$sess(1:4) .eq. "NULL" then
   write/out "MIDAS was not started in image/graphics mode ..."
   return
endif
! 
define/param p1 {dazdevr(2)},{dazdevr(3)},4 n -
             "Enter noLUTs,maxLUTsize,minLUTsize on device: "
define/param p2 {dazdevr(4)} n "Enter ownLUT: "
define/param p4 -1,-1,-1 N "Enter font no.s for small, medium, large font: "
define/param p5 RGBQ C "Enter RGB/RGBQ/RGBD/PSEUDO for Color mode:"
define/param p6 default C "Enter display:"
define/param p7 0 N "Enter special flag for 24 bit mode:"
define/param p8 NO C -
   "Enter special_cursor_mode_flg, AUTO_CURSOR or RECORD_CURSOR or NO:"
define/local minp/i/1/20 0 all
!
dazdevr(4) = {p2}
write/keyw dazdevr/i/15/3 {p4}
write/keyw dazdevr/i/9/1 {p7}		!save 24bit flag
write/keyw dazdevc {p6}
! 
if dazdevr(18) .lt. 0 @ getdispinfo.prg 	!we get once the display info:
						!dazdevr(12,13,18) + ididev(13)
if p5(1:1) .eq. "R" then		!RGB mode ?
   if p5(2:4) .eq. "GBD" then
      dazdevr(18) = 2		!Pseudo on RGB (DirectColor visual) 
   else if p5(2:4) .eq. "GBQ" then
      dazdevr(18) = 3		!Pseudo on RGB (TrueColor visual) 
   else
      dazdevr(18) = 1		!true RGB mode (3 colour planes)
   endif
else 
   dazdevr(18) = 0		!Pseudo colour mode (PseudoColor visual)
endif
! 
write/keyw winopen/i/1/20 0 all 	!clear the display/graphics OPEN flags
! 
if dazdevr(4) .lt. 0 then		!in companion mode we copy ...
   define/param p3 ? c "Enter unit of companion Midas: "
   copy/keyw dazdevr minp {p3}
   copy/keyw ididev/i/17/1 ididev/i/17/1 {p3}
   dazdevr(1) = minp(1)
   dazdevr(2) = minp(2)
   dazdevr(3) = ididev(17)		!we need the updated LUT size
   dazdevr(5) = minp(5)			!get LUT offset
   dazdevr(6) = {p3}			!save `master' MIDAS unit
else
   define/param p3 + 			!in normal mode not used ...!
   write/keyw minp/i/1/3 {p1}
   dazdevr(1) = minp(3)
   dazdevr(2) = minp(1)
   dazdevr(3) = minp(2)
endif
!
ididev(4) = ididev(13)			!same depth all over...
ididev(7) = dazdevr(18)
ididev(15) = 0				!we start with channel 0
! 
if p8(1:1) .eq. "A" then		!AUTO_CURSOR ?
   dazdevr(19) = 1
else if p8(1:1) .eq. "R" then		!or RECORD_CURSOR ?
   dazdevr(19) = 2
else
   dazdevr(19) = 0
endif
if ididev(1) .gt. -2 then
   write/out -
   "IDIserver running - changes applied only at next restart of server..."
endif
 
