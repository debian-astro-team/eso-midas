! @(#)plttbl.prg	19.1 (ES0-DMD) 02/25/03 14:08:58
! @(#)plttbl.prg	6.1 (ESO-IPG) 7/16/93 16:35:49
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLTDEMO.PRG
!.PURPOSE:        Demonstration procedure for the graphics package. 
!.AUTHOR:         Rein Warmels  ESO - Garching April. 25 , 1988
!.USE:            Execute as TUTORIAL/GRAPHICS
!.NOTE:           The following files should exist UGC.TBL in MID_TEST
!.VERSION:        880425 RHW creation
!                 931022 RvH some little improvements
! -----------------------------------------------------------------------
!                        Here for the table stuff
!
write/out "First, a small part  of the UGC catalogue is copied:"
-copy MID_TEST:ugc.dat tugc.dat
-copy MID_TEST:ugc.fmt tugc.fmt
create/table tugc 10 700 tugc tugc
select/table tugc all
show/table tugc
wait/secs 3
!
write/out "Two columns in the table will be plotted in the default plot setup."
echo/on
set/grap default                                      ! default graphics setup
! create/graph 0 600,450,0,325                          ! for the SONY
create/graph 0 600,450,0,450                            ! for the SUN
plot/table tugc #1 #2
echo/off
write/out " " 
!
write/out "MIDAS can do better; let's make the plot really nice."
write/out "You can change these parameters by the SET/GRAP command:"
echo/on
set/grap font=1 pmode=1                               ! set mode publ. quality
set/grap xaxis=0,24,6,3 yaxis=-90,90,30,15 yfor=.0g   ! set ticks and format
plot/table tugc #1 #2                                   ! make the nice plot
overplot/line 3 0.0,0.0 24.0,0.0
overplot/symbol 7 17.71,-28.55 2                      ! overpl the gal. center
label/graphic "\1Galactic Center" 17.71,-35           ! overplot text
overplot/symbol 15 12.5,-20.0 5                           ! overplot up arrow
label/graphic "\1Virgo Cluster" 12.0,-15.0 45 1.5 2   ! over plot text
echo/off
write/out " "
!
write/out "Let's go back in standard graphics mode:" 
echo/on
set/grap default                                      ! layout back to default
echo/off
write/out " " 
!
write/out "Select galaxies:"
echo/on
select/table  tugc :BT.LT.13.5                      ! select brightest objects
select/table  tugc :RV.LT.4000.0                        ! select on velocity
compute/table tugc :MBT = :BT-25.-5.*LOG10(:RV/50)      ! compute abs.magnitude
name/column    tugc :MBT "Absolute_B_mag."               ! include units
compute/table tugc :SIZE = :RV*SIN(0.000291*:DB)*20000  ! lin. diameter in pc
name/column    tugc :SIZE "pc"                           ! include units
echo/off
write/out " " 
!
write/out "Plot the new column:"
echo/on
plot/table tugc :SIZE :MBT                           ! lin. size vs. abs. mag.
set/grap    stype=2 yformat=.1g xform=g               ! change symbol + format
set/grap    xaxis=0,4,1,-1 yaxis=-12,-24,4,1          ! x log scale; y reverse
select/table  tugc :RV.LT.1000.0                        ! select on velocity
plot/table tugc :SIZE :MBT                           ! lin. size vs. abs. mag.
select/table  tugc :RV.GT.1000.0.AND.:RV.LT.4000.0      ! select on velocity
overplot/table tugc :SIZE :MBT 5                     ! lin. size vs. abs. mag.
set/grap    ltype=1 lweight=3
overplot/grid   large                                     ! overplot a grid 
set/grap    ltype=2 lweight=1
overplot/grid   small                                     ! overplot a grid 
echo/off
set/grap
IF AUX_MODE(1) .EQ. 1 THEN
   -delete tugc.tbl.
ELSE
   -delete tugc.tbl
ENDIF
wait/secs 10
delete/grap
