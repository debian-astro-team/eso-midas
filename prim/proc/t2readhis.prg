! @(#)t2readhis.prg	19.1 (ES0-DMD) 02/25/03 14:09:07
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!  MIDAS Command : t2readhis.prg
!
!  J.D.Ponz  890405              ESO - Garching
!  KB  910104, 920207
!
! .PURPOSE
!    implements
!    READ/HISTOGRAM table col-ref1 [bin [min [max]]]
!    PRINT/HISTOGRAM table col-ref1 [bin [min] [max]]]
!
! ----------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter input table:"
DEFINE/PARAM P2 ? CHAR  "Enter column:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
IF MID$CMND(1:1) .EQ. "P" @ print.prg assign
RUN MID_EXE:topertbl
IF MID$CMND(1:1) .EQ. "P" @ print.prg out
