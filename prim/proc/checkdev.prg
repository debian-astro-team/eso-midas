! @(#)checkdev.prg	19.2 (ESO-DMD) 03/18/03 17:15:31
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: checkdev.prg
!.PURPOSE: MIDAS procedure checkdev.prg check the hardcopy device
!.AUTHOR:  R.H. Warmels, ESO - Garching
!.USE:     @ checkdev p1
!          where p1 is the device name that can a MIDAS logical name or a 
!          system device name. 
!          The procedure returns the device and the associated driver in the 
!          keywords Q1 and Q2.
!          The input can be either a MIDAS device name or a system name.
!
!          In case the input is a MIDAS local name, it is first converted to 
!          a system name, using the file /$MIDASHOME/$MIDVERS/monit/devices.sh
!          After having obtained the system device name this name is looked 
!          for in the file /$MIDASHOME/$MIDVERS/systab/ascii/plot/agldevs.dat 
!          which contains all legal devices for the graphics and their drives,
!          and using the MIDAS function M$AGL.
!
!          If the device is unknown or missing the postscript driver
!	   will be used as default.
!          The file agldevs.dat describes the syntax used internally.
! 
!.VERSION: 930503  RHW Creation
! 
! 110516	last modif 
! 
!----------------------------------------------------------------------------
! 
define/parameter p1 LASER  c        ! get the default device or input from user
!
set/format i1
!
! *** MIDAS Logical names
if p1(1:3) .eq. "las" .or. p1(1:3) .eq. "col" then     ! default laser
   define/local psc/c/1/20 " " all     ! used for postscript option
   define/local ind/i/1/1 0            ! used for postscript option
! 
   ind = m$index(p1,".")
   if ind .gt. 0 then
      define/local len/i/1/1 0
      define/local opt/c/1/1 " "
      ind = ind-1
      psc = m$upper("{p1(1:{ind})}")
      ind = ind+2
      opt = "{p1({ind}:{ind})}"
      p1  = m$symbol(psc)                  
      len = m$len(p1) + 1
      write/keyw p1/c/{len}/2 ".{opt}"
   else
      if p1(1:4) .eq. "LAS " .or. p1 .eq. "LASER" then
         write/keyw psc LASER
      elseif P1(1:4) .eq. "COL " .or. p1 .eq. "COLOUR" then
         write/keyw psc COLOUR
      else
         psc = m$upper(p1)
      endif
      p1  = m$symbol(psc)                  
   endif
!
elseif p1 .eq. "SLIDE" then                         ! default slide writter 
   p1 = m$symbol("SLIDE")  
!
elseif p1 .eq. "PENPLOT"  then                         ! default penplotter
   p1 = m$symbol("PENPLOT")
!
elseif p1(1:4) .eq. "null" then                        ! default null device   
   p1 = "null"
endif
!
! Now we have all names converted into display OR system names
! They should be listed in MIDASHOME/MIDVERS/systab/ascii/plot/agldevs.dat
! lets get drivers for special devices (other than postscript) 
! 
define/local agldev/c/1/40 " " all                    !device plus driver
define/local device/c/1/20 " " all                    !device name
define/local driver/c/1/20 " " all                    !driver
define/local kk/i/1/1/ 0
define/local lowp1/c/1/60 " " all
! 
lowp1 = m$lower(p1)			!convert to lowercase
agldev = m$agl(lowp1)			!get system+driver from agldevs.dat
if agldev(1:1) .eq. " " then		!no entry in agldevs.dat
   kk = m$index(lowp1,".")
   if kk .gt. 0 then
      device = lowp1({kk}:>)		!use postscript driver as default
      write/keyw lowp1/c/{kk}/4 "    "
      agldev = lowp1 // ":pscript" // device
   else
      agldev = lowp1 // ":pscript"
   endif
endif
!
! *** So, now we have a valid devices name
kk = m$index(agldev,":") - 1                          ! get the device name
write/keyw device/c/1/20 {agldev(1:{kk})}
kk = m$index(device,".")
if kk .ne. 0 write/keyw device/c/{kk}/3 "   "
!
kk = m$index(agldev,":") + 1                         ! get the device type
write/keyw driver/c/1/20 {agldev({kk}:>)}
!
return {device} {driver}

