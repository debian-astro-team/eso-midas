! @(#)printlog.prg	19.1 (ES0-DMD) 02/25/03 14:08:59
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  printlog.prg  to implement PRINT/LOG
! K. Banse	910104, 920121, 920207
!
! executed via @ printlog page_def
! where page_def = pstart,pend		from pstart to pend (incl)
!                  page_no		only that page
!                  LAST			only last page
!                  -no  		last 'no' pages
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 1,9999 N "Enter pages to print: "
WRITE/KEYW INPUTI/I/15/2 {P1},{P1}
! 
DEFINE/LOCAL OLDLOG/I/1/1 {LOG(1)}
LOG/OFF					!turn logging off
! 
RUN MID_MONIT:LOGGER
! 
IF OLDLOG .NE. 0 LOG/ON
IF MID$PRNT(1:1) .EQ. "T" RETURN
IF MID$PRNT(1:1) .EQ. "F" RETURN
! 
ERROR(6) =  ERROR(3)			!save current error flag
@ print.prg print 			!send file to chosen printer
