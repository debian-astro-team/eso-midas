!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLOTTBL.PRG
!.PURPOSE: MIDAS procedute to plot one or two planes of a 3D table
!.USE:     execute as @@ PLOTTBL par1 [par2] [par3] [par4] [par5] [par6] 
!          where:
!          par1 = input table
!          par2 = plane 1
!          par3 = plane 2
!          par4 = sc_x,sc_y,off_x,off_y (defaults device filling)
!          par5 = symbol types
!          par6 = line types
!          par7 = direction in which way the plane is read
!.AUTHOR:  R.M. van Hees	931001	creation
!.VERSION: 
! 100817	last modif
! ----------------------------------------------------------------------
! 
define/param p1 ?  table "Enter table:"
define/local qualif/c/1/4 "{mid$cmnd(11:14)}"		!save qualifier
!
if p2(1:1) .eq. "?" then
   define/param p3 ? C "Enter input for the ordinate column:"
   if p3(1:3) .eq. "SEQ" then
      write/out "*** FATAL: Illegal combination of column parameters"
      return
   endif
endif
!
if p2(1:3) .eq. "SEQ" then
   define/param p3 ? C "Enter input for the ordinate column:"
   if p3(1:3) .eq. "SEQ" then
      write/out "*** FATAL: Illegal combination of column parameters"
      return
   endif
endif
!
write/keyw in_a {p1}
! 
if qualif(1:1) .eq. "S" then		! plot/selected
   define/param p7 red C "Enter color for selected table elements:"
   write/keyw inputc/c/1/20 "DEFAULT,DEFAULT  C   "
   write/keyw inputc/c/21/22 {p7}
else
   define/param p7 DEFAULT,DEFAULT  C
   write/keyw inputc/c/1/20 {p7}
   write/keyw inputc/c/21/8 "NOCOL   "
endif
! 
if mid$cmnd(1:1) .eq. "P" then
   define/param p4 0.0,0.0,-999,-999 NUM
   @ plscoff.prg_o {P4}
   dattim = m$time()
   run MID_EXE:PLOTTBL
else
   define/param p4 {plistat(6)} NUM
   define/local itype/i/1/1 {plistat(6)}
   write/keyw plistat/i/6/1 {p4}
   run MID_EXE:PLOTTBL
   write/keyw plistat/i/6/1 {itype} 
endif
!
write/keyw plcdata/c/1/60  {p1}
write/keyw plcdata/c/61/20 "table       " 
copy/graph

