! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2showtbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!  070416
!
! .PURPOSE
!
!    implements
!
!  SHOW/TABLE    table-name  short/full
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
DEFINE/PARAM P2 full C "Enter display_flag (full/short):"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:tdatatbl
