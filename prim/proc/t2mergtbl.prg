! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!  MIDAS Command : t2mergtbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!
!.PURPOSE
!    implements
!
!  COPY/TABLE  intable outable [organization]
!  MERGE/TABLE intable1 intable2 ... outable
!
!.VERSION
! 041210	last modif
! 
! ----------------------------------------------------------------------
!
define/parameter P1 ? TABLE  "Enter input table:"
!
if mid$cmnd(1:1) .eq. "C" then
   define/parameter P2 ? TABLE  "Enter output table:"
   write/keyword history "{mid$cmnd(1:4)}/{mid$cmnd(11:14)} "
else
   write/keyword history "{mid$cmnd(1:5)}/{mid$cmnd(11:14)} "
endif   
!
!
action(1:1) = mid$cmnd(1:1) 
write/keyword mid$cmnd/C/1/4 "MERG"
!
run MID_EXE:tdatatbl
