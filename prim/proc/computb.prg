! @(#)computb.prg	19.1 (ESO-DMD) 02/25/03 14:08:46
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  computb.prg  to implement COMPUTE/IMAGE, /PIXEL, /PLANE
!					     COMPUTE/DIMAGE, /DPIXEL, /DPLANE
! K. Banse	910614, 920506, 930105, 950728, 970630, 980318
!
! execute via COMPUTE/qualif [out_frame =] expression
!
! if out_frame and = are omitted, the command works like a pocket calculator
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
write/keyw history "COMPUTE/{mid$cmnd(11:14)}"
define/local qualif/c/1/3  "   "
! 
if mid$cmnd(11:11) .eq. "D" then
   default(3:3) = "D"				!double precision...
   qualif(1:3) = mid$cmnd(12:14)
else
   default(3:3) = "R"				!real calculations...
   qualif(1:3) = mid$cmnd(11:13)
endif
if qualif(2:2) .eq. "L" then
   default(2:2) = "X"			!COMPUTE/PLANE
else
   default(2:2) = qualif(1:1)		!COMPUTE/IMAGE, /PIX 
endif
!
if p2 .eq. "=" then
   default(1:1) = "Y"			!we have a result frame...
   define/param p1 ? IMA "Enter name of result frame:"
   write/keyw out_a {p1}
   goto run_it
   return
endif
! 
define/local kk/i/1/1 {pcount(2)}		!length of P1
if p1({kk}:{kk}) .eq. "=" then		!look for resframe= ...
   if pcount .lt. 2 then
      write/error 5
      return
   endif
   p1({kk}:{kk}) = " "
   goto fix_it
! 
elseif p2(1:1) .eq. "=" then		!look for resframe =frame1 ...
   write/keyw p2 {p2(2:)}
fix_it:
   write/out We assume you wanted {p1} = {p2} ...
   if pcount .eq. 2 then
      write/keyw p3 {p2}
   else
      write/keyw p3 {p2}{p3}		!now p2 + p3 in p3
   endif
   default(1:1) = "Y"			!we have a result frame...
   define/param p1 ? IMA "Enter name of result frame:"
   write/keyw out_a {p1}
! 
else
   default(1:1) = "N"			!pocket calculator...
endif
! 
run_it:
if default(3:3) .eq. "D" then
   run MID_EXE:DARITHM
else
   run MID_EXE:ARITHM
endif
