! @(#)rebstat.prg	19.1 (ESO-DMD) 02/25/03 14:09:00
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure rebstat.prg
! K. Banse      980921, 990117
! execute via:  REBIN/STATISTICS in out xpix,ypix stat_quantity 
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input frame: "
define/param p2 rebstat IMA "Enter output frame: "
define/param p3 8,8 n "Enter xpix,ypix for new pixel:"
define/parameter p4 mean ? "Enter statistical value for result image:"
! 
define/local myframe/c/1/100 {p1}
define/local myres/c/1/100 {p2}
! 
set/format i1
define/local tpix/i/1/2 {p3},{p3}		!ensure x + y value
define/local mynax/i/1/1 1
mynax = m$value({myframe},naxis)
if mynax .lt. 2 tpix(2) = 1
! 
define/local mycomnd/c/1/80 "REBIN/STATISTICS " ? +lower
inputc(1:) = myframe // " {myres}"
outputc(1:1) = " "
outputc(2:) = "{p3} " // " {p4}"
mycomnd(1:) = mycomnd(1:17)//inputc//outputc
! 
! choose statistics option
branch  p4(1:3) MIN,MAX,MOM,MOD,MED MINMAX,MINMAX,MOMENT,MODE,MEDIAN
action(1:2) = "sn"		!that's needed for MEAN, STD
goto run_it
minmax:
action = "mn"
goto run_it
moment:
action = "rn"
goto run_it
mode:
action = "hn"
goto run_it
median:
action = "xn"
!  
run_it:
statist/image {myframe} [{tpix(1)}x{tpix(2)}] ? ? {action(1:2)} -
              {myres},image,{p4}
! 
if mynax .eq. 1 then			!1-dim input frame
   {myres},npix(1) = m$value({myres},intvals(3))
   {myres},start(1) = m$value({myframe},start(1))
   {myres},step(1) = tpix(1)*m$value({myframe},step(1))
else
   {myres},naxis = 2
   {myres},npix(1) = m$value({myres},intvals(3))
   {myres},npix(2) = m$value({myres},intvals(4))
   {myres},start(1) = m$value({myframe},start(1))
   {myres},start(2) = m$value({myframe},start(2))
   {myres},step(1) = tpix(1)*m$value({myframe},step(1))
   {myres},step(2) = tpix(2)*m$value({myframe},step(2))
endif

