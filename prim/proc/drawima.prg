! @(#)drawima.prg	19.1 (ESO-DMD) 02/25/03 14:08:49
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure drawima.prg to implement DRAW/IMAGE
! K. Banse	910109S, 920312, 930122
!
! use via  DRAW/IMAGE frame channel xscale xcenter,yline locut,hicut over_flag
!                     yscale,screen_off,intensity,angle fixpix
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
CROSSREF FRAME CHANL SCALE CENTER CUTS OVER IAUX FIX
!
define/local chan/i/1/1 {ididev(15)}		!save current image channel
define/local myact/c/1/2 "XI"			!save qualifier
!
define/param p1 ? IMA "Enter input image: "
define/param p2 ov ?  "Enter channel: "
define/param p3 + ?   "Enter x-scaling: "
define/param p4 + ?   "Enter coords of xcenter,yline: "
define/param p5 + ?   "Enter cut values: "
define/param p6 YES ? "Enter over_plot flag - YES/NO: "
define/param p7 + N  "Enter yline,yscale,screen_off,intensity,angle(degrees): "
define/param p8 + ?   "Enter fixpixel (framex,screenx): "
!
write/keyw in_a {p1}
write/keyw default YYYYY			!init 5 elements of DEFAULT
!
set/chan {p2}					!update current channel
!
!  handle scaling factors...
! 
if p3(1:1) .ne. "+" then
   default(1:1) = "N"
   write/keyw dazin/i/1/1 {P3}	
endif
!
!  handle x-center coord + fixpixel pixels
! 
if p4(1:1) .ne. "+" then
   default(2:2) = "N"
   write/keyw inputc/c/21/40 {p4}
endif
! 
if p8(1:1) .ne. "+" then
   write/keyw inputi/i/1/2 1,0
   write/keyw inputi {p8}
   default(5:5) = "N"
endif
!
if p5(1:1) .ne. "+" then
   default(3:3) = "N"
   write/keyw inputr {p5}	
endif
!
default(4:4) = p6(1:1)
dazin(2) = ididev(12)-1				!y-size of channel memory
write/keyw inputr/r/3/4 {dazin(2)},0.,255.,0.
if p7(1:1) .ne. "+" write/keyw inputr/r/3/4 {p7}
!
!  now draw
!
run MID_EXE:DRAW
! 
if ididev(14) .eq. ididev(15) ididev(15) = chan
