! @(#)pltaxs.prg	19.1 (ES0-DMD) 02/25/03 14:08:58
! @(#)pltaxs.prg	6.1 (ESO-IPG) 7/16/93 16:36:33
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: pltaxs.prg
!.PURPOSE:        Demonstration procedure for the graphics package. 
!.AUTHOR:         Rein Warmels  ESO - Garching April. 25 , 1988
!.USE:            Execute as TUTORIAL/GRAPHICS
!.NOTE:           The following files should exist UGC.TBL in MID_TEST
!.VERSION:        880425 RHW creation
! -----------------------------------------------------------------------
!         Here you will find a number of examples to draw axes
!
write/out "This tutorial shows you how to use the PLOT/AXES command."
write/out "First, a table with some data is copied into your directory:"
set/grap default
indisk/fits MID_TEST:example.tfits texample.tbl >Null
!
write/out "We switch off the x-axis tick labels and plot the first frame."
write/out "It will be 40 by 80 mm and put top left in the graphic display."
echo/on
create/graph 0 600,450,0,450                       ! for the SUN
set/grap xformat=none
plot/axes -2,8,4,1.0 -1.2,1.2,.5,.25 -40,-75,25,45 "" "Position (cm)"
echo/off
!
write/out "Next we overplot the data points and the model"
echo/on
overplot/table texample #1 #2
set/grap colour=4 ltype=3 stype=0
overplot/table texample #1 #2
!
echo/off
write/out "Now we overplot a second frame in overplot mode and"
write/out "overplot the residuals and errorbars"
set/grap colour xform=.1g yform=.2g
echo/on
overplot/axes -2,8,4,1.0 -.3,.3,.2,.1 -40,-30,25,15 "Time (sec)" "Position (cm)"
set/grap colour=6 stype=6 
overplot/table texample #1 #6
set/grap colour 
overplot/error texample #1 #6 #4
!
echo/off
write/out "To the right we make a third frame, in overplot mode:"
echo/on
overplot/axes -1,1,1,-1 -1,1,1,-1 -40,-105,85,15 "Time (sec)" "Position (cm)"
set/grap stype=10 colour=2
overplot/table texample #1 #3
set/grap colour=1
overplot/grid small
!
echo/off
write/out "Finally, we make a description to the plots:"
echo/on
set/grap font=1
label/graph DESCRIPTION 130,118,mm ? 1 1
set/grap font=0
label/graph "Figure 1 (top left):" 130,105,mm  ? ? 1
label/graph "data and model" 130,100,mm ? ? 1
label/graph "Figure 2 (bottom left):" 130,90,mm ? ? 1
label/graph "residuals and errors" 130,85,mm ? ? 1
label/graph "Figure 3 (right):" 130,75,mm ? ? 1
label/graph "log time v. position" 130,70,mm ? ? 1
set/grap font=4
label/graphic "\1MIDAS Graphics" 130,40,mm 0 2.0 1
!
indisk/fits MID_TEST:spiral.fits tspiral.bdf >Null
! 
create/display 0 512,512,610,450
load/lut rainbow
load/image tspiral
assign/grap display
set/grap colour=0 xf=.0d yf=.0d font=1
echo/full
plot/axes [@150,@150:@450,@450]
set/grap font=1
label/graphic "NGC 5247" 45,105,mm 0 1.5 1
echo/off
wait/secs 5
delete/grap
wait/secs 5
delete/display
set/grap
