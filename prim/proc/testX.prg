! @(#)testX.prg	19.1 (ESO-DMD) 02/25/03 14:09:06
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! procedure testX.prg to test the Xserver setup
! K. Banse	980629, 021120
! 
!  execute via @ testX switch [Xtest_param]
!        with switch = 0, to run Xtest.exe 
!                    = 1, to run Xcommand `xdpyinfo'
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 0 n "Enter switch (0/1) for Xtest.exe or xdpyinfo:"
! 
if {p1} .eq. 0 then
   if aux_mode(1) .le. 1 then			!VMS
      RUN MID_DISK:[&MIDASHOME.&MIDVERS.SYSTEM.IDISERV.SRC]XTEST.EXE
   else						!Unix
      $ $MIDASHOME/$MIDVERS/system/idiserv/src/Xtest.exe 
   endif
else
   $xdpyinfo > testX.dat
   $more testX.dat
   write/out
   write/out >>> "this info is also stored in the ASCII file `testX.dat'" <<<
endif
