! @(#)plotdes.prg	19.2 (ESO-DMD) 06/05/03 11:33:33
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLOTDES.PRG
!.PURPOSE:        Plot a "descriptor" on the graphic device
!                 execute as @@ PLOTDES par1 [par2] [par3] [par4] where:
!                 par1 = input frame
!                 par2 = name of the descriptor (defaulted to HISTOGRAM)
!                 par3 = first data, last data point
!                 par4 = scale_x,scale_y,off_x,off_y
!.AUTHOR:         Ch. Ounnas ESO-Garching
!.VERSION:        ?????? ChO + RHW
!.VERSION:        860924, 900409   KB
!.VERSION:        870527 RHW
!.VERSION:        880229 RHW compute key for time and day included
!.VERSION:        920224 RHW Offset in x and y included
!.--------------------------------------------------------------------------
! 
DEFINE/PARAM P1 ? IMA "Enter frame:"
DEFINE/PARAM P2 HISTOGRAM C
DEFINE/PARAM P3 0,0 N                        ! default all descriptor elements
!
WRITE/KEYW IN_A           {P1}
WRITE/KEYW INPUTC/C/1/20  {P2}
WRITE/KEYW INPUTI/I/1/2   {P3}
!
IF MID$CMND(1:1) .EQ. "P" THEN
   DEFINE/PARAM P4 0.,0.,-999,-999 NUM
   @ plscoff.prg_o {P4}                           ! get the scales and offsets
   COMPUTE/KEYW DATTIM = M$TIME()
ELSE
   DEFINE/PARAM P4 0.0 NUM
   WRITE/KEYW  INPUTR/R/1/1 {P4}
ENDIF
!
RUN MID_EXE:PLOTDES
WRITE/KEYW PLCDATA/C/1/60  "{P1},{P2}"            ! name, type data structure
WRITE/KEYW PLCDATA/C/61/20 "DESCRIPTOR"
copy/graph


