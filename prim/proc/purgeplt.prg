! @(#)purgeplt.prg	19.1 (ES0-DMD) 02/25/03 14:08:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PURGEPLT.PRG
!.PURPOSE: MIDAS procedure to purge plotfiles
!.USE:     @ purgeplt p1
!          par1 = name of the plot file
!.AUTHOR:  R.H. Warmels  ESO - Garching
!.VERSION: 160190  Remove name behind the "."
!  901108  KB:  {','} business..
! ----------------------------------------------------------------------
DEFINE/LOCAL NK/I/1/1 0
!
NK = M$INDEX(P1,".")-1
WRITE/KEYW Q1 {P1(1:{NK})}
!
IF MID$CMND(1:1) .EQ. "P" THEN
   IF AUX_MODE .LT. 2 THEN
      $PURGE/NOLOG {Q1}.plt
   ENDIF
ENDIF
