! @(#)extrac2.prg	19.1 (ES0-DMD) 02/25/03 14:08:50
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  extrac2.prg  for EXTRACT/CURSOR
! K. Banse	901108, 930323
!
! use via EXTRACT/CURSOR out nx,ny
! 	
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 subframe IMA "Enter result frame: "
define/param p2 + N "Enter xpix,ypix for extracted frame: "
define/param p3 NO C "Enter loop_flag, Loop/NoLoop: "
define/maxpar 3
!
write/keyw history "EXTRACT/{mid$cmnd(11:14)} "
write/keyw out_a {p1}			!store name of result frame
if p2 .eq. "+" then
   cursor = 2				!we use ROI
else
   cursor = 0				!use single cursor
   write/keyw inputi/i/10/2 {p2}		!and store no. of pixels in x,y
endif
!
action(1:4) = "EXCU"
run MID_EXE:IDAUXX			!use Cursor on ImageDisplay
