! @(#)t2creacol.prg	19.1 (ES0-DMD) 02/25/03 14:09:05
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2creacol.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!  901108  KB  fix WRITE/KEYW HISTORY stuff...
!
! .PURPOSE
!
!    implements
!
!  CREATE/COLUMN    table-name  column [unit] [format] [type]
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE  "Enter table:"
DEFINE/PARAM P2 ? CHAR   "Enter column label:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
WRITE/KEYW MID$CMND/C/1/4 "CCREA"
!
RUN MID_EXE:tdatatbl
