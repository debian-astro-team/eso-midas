! @(#)getima.prg	19.1 (ES0-DMD) 02/25/03 14:08:52
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure getima.prg to implement GET/IMAGE
! K. Banse	890703, 911101, 920401
!
!     GET/IMAGE subframe chanl [ITT]
! or  GET/IMAGE subframe CURSOR,[xpix,ypix] [ITT]
! or  GET/IMAGE subframe OVERLAY [ITT]
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter name of result image:"
DEFINE/PARAM P2 + ? "Enter channel or CURSOR or CURSOR,xsize,ysize:"
DEFINE/PARAM P3 NO ? "Enter ITT_flag: "
! 
WRITE/KEYW IN_A {P1}
ACTION(1:5) = "RDXXX"			!default to reading from image channel
!   						!test, if we want whole image...
IF P2(1:1) .EQ. "C" THEN
   ACTION(3:5) = "CUR"
   DEFINE/LOCAL L/I/1/1 0
   L = M$INDEX(P2,",")
   IF L .GT. 0 THEN
      L = L+1
      WRITE/KEYW DAZIN/I/1/3 0,{P2({L}:>)} 		!use cursor cross
   ELSE
      DAZIN(1) = 2				!use rectangle (2 cursors)
   ENDIF
!
ELSEIF P2(1:1) .EQ. "O" THEN
   ACTION(3:5) = "OVE"				!read from overlay channel
!
ELSE
   IF P2(1:1) .NE. "+" @% setchan {P2}		!read from image channel
ENDIF
!
WRITE/KEYW HISTORY "GET/IMAGE "
RUN MID_EXE:IDAUXX
