! @(#)dazdisp.prg	19.1 (ESO-DMD) 02/25/03 14:08:48
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure dazdisp.prg to implement MODIFY/DISPLAY, /GRAPHICS
! K. Banse	960626, 980903, 990727, 001019
!
! use via MODIFY/DISPLAY (GRAPHICS) option
!         where  option = Icon 	 : active Display window -> Icon
! 			  Window : Icon -> active Display window
! 			  Parent : active Display window becomes parent w.
! 			  Parent=Root : root w. becomes parent w. again
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 Icon C "Enter option, ICON/WINDOW or PARENT:"
define/maxpar 1
define/local iaux/i/1/1 0
set/format i1
! 
if p1(1:1) .eq. "P" goto parent_w
! 
! here for Icon stuff
! 
if mid$cmnd(11:11) .eq. "G" then
   action(1:2) = "IK"
   outputc = "Graphics"
   inputc = mid$sess(7:7)
   iaux = {inputc} + 11		!index into keyword WINOPEN
else
   action(1:2) = "IC"
   outputc = "Display"
   inputc = mid$sess(6:6)
   iaux = {inputc} + 1		!index into keyword WINOPEN
endif
! 
if inputc(1:1) .eq. " " .or. inputc(1:1) .eq. "-" then
   write/error -101 "No active window..."
endif
! 
if winopen({iaux}) .eq. 0 then
   write/error -101 "The window is closed - out of sync...!"
endif
! 
if p1(1:1) .eq. "W" then 		!icon -> disp
   if winopen({iaux}) .eq. 1 then
       write/out {outputc} window {inputc} already on screen...
       return
   else
      write/keyw dazin/i/1/1 0
   endif
else if p1(1:1) .eq. "I" then 		!disp -> icon
   if winopen({iaux}) .eq. 2 then
       write/out {outputc} window {inputc} already an icon ...
       return
   else
      write/keyw dazin/i/1/1 1
   endif
else
   write/error 30
endif
! 
run MID_EXE:IDFUNC
winopen({iaux}) = dazin + 1		! 1 = on screen, 2 = an icon
return
! 
! here for changing parent window
! 
PARENT_W: 
write/keyw action/c/1/2 AN
! 
if m$index(p1,"=") .gt. 0 then		!we don't even test for =root ...
   write/out  parent window set back to root window of screen
   inputi(2) = -1
else
   if mid$cmnd(11:11) .eq. "G" then
      iaux = dazdevr(11)
      inputi(2) = iaux + 100
      write/out new parent window = graphics window {iaux}
   else
      iaux = dazdevr(10)
      inputi(2) = iaux 
      write/out new parent window = display window {iaux}
   endif
endif
! 
inputi(1) = 1				!flag for parent window stuff
run MID_EXE:idauxx.exe
! 
dazdevr(21) = inputi(2)			!save parent window


