! @(#)testima.prg	19.2 (ESO-DMD) 06/02/03 16:06:38
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! procedure testima.prg to test, if an image is loaded into the display
! if not an image from MID_TEST is loaded
! the name of the loaded image will be stored in keyw. IN_A
! K. Banse	890301, 920413, 950206, 990804, 030602
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 centaur Ima
define/param p2 NOAUTO C
! 
if p2(1:4) .eq. "AUTO" then
   @ testima,copy {p1}
else if dazdevr(10) .eq. -1 then
   @ testima,copy {p1}
else if idimemc(1:1) .eq. " " then
   if p1(1:1) .eq. "+" then
      write/out "We need an image to load into the image display."
      write/out -
"Enter the name of an image frame or hit RETURN, if no image in your directory."
      write/keyw in_a/c/1/80 " " all
      inquire/keyw in_a "Image name: "
      if aux_mode(7) .le. 0 @ testima,copy centaur
   else
      @ testima,copy {p1}
   endif
else				!an image is already displayed
   write/keyw in_a {idimemc}
   return
endif
!
write/out load/image {in_a} scale=1
load/image {in_a} scale=1
! 
entry copy
! 
write/out "O.k. we copy an image," {p1}.fits" from MID_TEST"
indisk/fits MID_TEST:{p1}.fits testima.bdf >Null
write/out Converted MID_TEST:{p1}.fits to testima.bdf
write/keyw in_a testima

