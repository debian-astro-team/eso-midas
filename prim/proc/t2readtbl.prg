! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!  MIDAS Command : t2readtbl.prg
!  TABLE Subsystem
!  J.D.Ponz  890405              ESO - Garching
!  KB  910104, 920207, 080728
!  MP 920728
! .PURPOSE
!    implements
!  READ/TABLE    table-name   [optional parameters]
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
WRITE/KEYW ACTION/C/1/2  {MID$CMND(1:2)}
IF MID$CMND(1:1) .EQ. "P" @ print.prg assign
RUN MID_EXE:tdatatbl
IF MID$CMND(1:1) .EQ. "P" @ print.prg out
