! ++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure daztable.prg to implement  LOAD/LUT, /ITT
! K. Banse	901203, 910211, 920524, 930415, 950106, 071005
!
! execute via LOAD/LUT        in_specs sect,noval disp_flag format 
! 	      LOAD/ITT        in_specs 
! in_specs = table[,red_labl,green_labl,blue_labl] (table input)  or
!          = image,descr (input from descriptor)
! sect,noval = 0,256 - just for backwards compatibility
! disp_flag = D(isplay) or N(oDisplay) 
! format = TABLE or ASCII, defaulted to TABLE 
!
! +++++++++++++++++++++++++++++++++++++++++++++++
!
if ididev(18) .eq. 11 then
   if dazdevr(4) .ne. 1 then			!only possible with own LUT
      write/out this command only possible with own LUT ...
      return
   endif
endif
!
define/local didi/c/1/3 +++
write/keyw action/c/1/4 DA{mid$cmnd(1:1)}{mid$cmnd(11:11)}  	!set up ACTION
!
define/param p1 ? C "Enter in_specs - table or image,descr: "
write/keyw in_a {p1}
!
if mid$cmnd(11:11) .eq. "I" then		!LOAD/ITT
   write/keyw dazin 99,256 			!always 256 values...
   write/keyw hcittlut/c/1/20 {p1}		!save ITT name
   hcittlut(41:41) = "Y"
! 
else
   define/param p2 {dazhold(10)},256 N		!LOAD/LUT
   define/param p3 + C "Enter D(isplay) or N(oDisplay) for LUT display: "
   define/param p4 table C "Enter TABLE or ASCII format: "

   write/keyw dazhold/i/10/1 {p2}		!update DAZHOLD(10)
   write/keyw hcittlut/c/21/20 {p1}		!save LUT name
   hcittlut(42:42) = "Y"

   ! "hidden" display on/off parameter ...
   if p3(1:1) .eq. "D" then
      didi = "ON "
   elseif p3(1:1) .eq. "N" then
      didi = "OFF"
   endif
   if p4(1:2) .eq. "AS" then
      dazin(3) = 9
   else
      dazin(3) = 0
   endif
   write/keyw dazin {p2}			!store section,values
endif
! 
run MID_EXE:IDAUXX
if didi(1:1) .ne. "+" display/lut {didi}
