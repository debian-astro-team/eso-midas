! @(#)vcopy.prg	19.1 (ESO-DMD) 02/25/03 14:09:12
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure vcopy.prg to implement COPY/DISPLAY, /ZOOM
!  K. Banse   910117, 911008, 911011, 940708, 940803, 980713
!
!  execute via COPY/DISPLAY out_device stop_flag conv_flag LUT_name
!                           print_flag print_mode
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref OUT_DEV STOP_FLG ITTDEF LUTNAM PRFLAG PRMODE
! 
define/param p1 + C "Enter hardcopy device: "
define/param p2 NO_STOP ? "Enter stop_flag, STOP or NO_STOP:"
define/param p3 + ? "Enter conversion flag, N or P or I,ITT_name: "
define/param p4 DISPLAY  ? "Enter LUT: "
define/param p5 PRINT,SAMEFILE ? "Enter print_flag: "
define/param p6 +  C -
   "Enter print_mode `abcde`, a=P/L, b=B/C, c=8/4, d=N/B, e=T/Z: "
define/maxpar 6
! 
default(1:1) = mid$cmnd(11:11)		!save qualifier
define/local filnam/c/1/40 " " all
define/local kk/i/1/1 0
define/local new/i/1/1 0
!
kk = m$index(p5,",")			!move to 2nd string
if kk .gt. 0 then
   kk = kk+1
   if p5({kk}:{kk}) .eq. "N" then	!yes, we want always new files
      new = 1
      dattim = m$time()
   endif
   if kk .eq. 2 p5(1:1) = "P"		!if p5 was ",xyz" then default to Print
endif
! 
if ididev(18) .eq. -1 then		!NULL display - do nothing
   create/image vcopy 2,{ididev(11)},{ididev(12)}
   write/descr vcopy ident "dummy display image                 "
   -copy MID_SYSTAB:ramp.lut middumml.lut
else
   if mid$sess(6:6) .eq. " " .or. mid$sess(6:6) .eq. "-" then
      write/out no active display window...
      return
   endif
   inputi(7) = {mid$sess(6:6)} + 1		!index in WINOPEN
   ! 
   if winopen({inputi(7)}) .eq. 2 then
      write/out "Copy from display window Icon"
   else if error(2) .lt. 2 then                 !if not an expert...
      write/out "Make sure, that your Midas display window is completely"
      write/out "inside the area of your workstation screen"
      write/out "Otherwise XWindow will go wild with an XBadMatch error ... "
   endif
   ! 
   run MID_EXE:VCOPY			!read ImageDisplay
endif
! 
if aux_mode .eq. 1 then
   if new .eq. 1 then
      write/keyw filnam screen{mid$sess(11:12)}:{dattim(13:20)}
   else
      write/keyw filnam screen{mid$sess(11:12)}
   endif
else
   if new .eq. 1 then
      write/keyw filnam screen{mid$sess(11:12)}:{dattim(19:26)}
   else
      write/keyw filnam screen{mid$sess(11:12)}
   endif
endif
! 
-rename vcopy.bdf {filnam}.ima		!same format ...
!
if p2(1:1) .eq. "S" then		!handle stop_flag
   write/out "Contents of ImageDisplay stored in frame {filnam}.ima "
else
! 
   if p3(1:1) .eq. "+" write/keyw p3/c/1/2 "? "
   if p6(1:1) .eq. "+" write/keyw p6/c/1/2 "? "
! 
   if p1(1:1) .eq. "+" then
      @ hardcopy NONE {filnam}.ima {p3} {p4} {p5} {p6}
   else
      @ hardcopy {p1} {filnam}.ima {p3} {p4} {p5} {p6}
   endif
endif
! 
if aux_mode(1) .eq. 1 then		!VMS
   -delete MID_WORK:middumml.lut.*
else					!Unix
   -delete MID_WORK:middumml.lut
endif
