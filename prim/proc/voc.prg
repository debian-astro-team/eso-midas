! @(#)voc.prg	19.1 (ESO-DMD) 02/25/03 14:09:13
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure voc.prg to implement SET(CLEAR)/OVERLAY, COPY/OVER
! K. Banse	900605, 980626
!
! use via SET/OVERLAY  or  CLEAR/OVERLAY
! 	  COPY/OVERLAY  chanl
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
if mid$cmnd(1:2) .eq. "CO" then
   define/param p1 0 N "Enter channel:"
   define/param p2 AP C "Enter append_flag, APPEND or NOT:"
   define/maxpar 2
   if p2(1:1) .eq. "A" then
      write/keyw dazin/i/1/2 {p1},1
   else
      write/keyw dazin/i/1/2 {p1},0
   endif
   action(1:2) = "CP"
else
   action(1:2) = "GE"
   if mid$cmnd(1:1) .eq. "S" then
      dazhold(9) = 1			!SET/OVERLAY
      dazin(1) = 1
   else
      dazhold(9) = 0			!CLEAR/OVERLAY
      dazin(1) = 0
   endif
endif
!
run MID_EXE:IDFUNC
