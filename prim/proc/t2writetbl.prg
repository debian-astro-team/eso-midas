! @(#)t2writetbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:08
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2writetbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!
! .PURPOSE
!
!    implements
!
!  WRITE/TABLE    table-name column_reference row value
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
DEFINE/PARAM P2 ? CHAR  "Enter column:"
DEFINE/PARAM P3 ? CHAR  "Enter row number:"
DEFINE/PARAM P4 ? CHAR  "Enter value:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:tdatatbl
