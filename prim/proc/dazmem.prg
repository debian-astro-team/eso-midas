! @(#)dazmem.prg	19.1 (ES0-DMD) 02/25/03 14:08:48
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  dazmem.prg  for commands
!       DISPLAY/LUT,
!       BLINK/CHANNEL, DISPLAY/CHANNEL
!       CLEAR/CHANNEL, SHOW/CHANNEL
! 
! K. Banse	900323, 910920, 920312, 920523, 940704
!
! execute as 1) BLINK/CHANNEL chan1,chan2,... delay
!            2) CLEAR/CHANNEL chanl constant
!            3) DISPLAY/CHANNEL chanl LUT_section
!            4) SHOW/CHANNEL chanl
!            5) DISPLAY/LUT [OFF] [intensity]
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
branch mid$cmnd(1:2) CL,DI,SH CLEAR,DISPLAY,SHOW
! 
! 1) BLINK/CHANNEL
!
define/param p1 0,1 n "Enter channels to blink on:"
define/param p2 0.1 n "Enter blink delay in secs.:"
define/param p3 + c "Enter blink option:"
! 
define/local chanls/i/1/40 -1 all 	!init channel no's
write/keyw chanls {p1}
inputr(1) = {p2}
action(1:4) = "MEBX"
if dazhold(3) .eq. 1 clear/split             !if split screen set, clear it
! 
if error(2) .lt. 2 then
   if ididev(18) .lt. 11 then
      write/out Hit CTRL/C to stop...
   else
      write/out "Place cursor/mouse in the display window and"
      write/out "press the Exit button to stop..."
   endif
endif
run MID_EXE:idauxx
return
!
! 2) CLEAR/CHANNEL
!
CLEAR:
define/param p1 + c "Enter channel:"
define/param p2 0 n "Enter constant:"
dazin(1) =  {p2}
action(1:4) = "MECX"
run MID_EXE:idauxx
return
!
! 3) DISPLAY/CHANNEL    and      5) DISPLAY/LUT
! 
DISPLAY:
if mid$cmnd(11:11) .eq. "C"  then
   define/param p1 + c "Enter channel:"
   define/param p2 99 n "Enter LUT_section:"
   action(1:4) = "MEMX"
   dazin(1) =  {p2} 			!store LUT-section in DAZIN(1)
!
else
   define/param p1 ON c "Enter ON/OFF:"
   define/param p2 200 ? "Enter intensity:"
!
   action(1:4) = "MERX"
   if p1(1:2) .eq. "OF" then
      dazhold(8) = 0
      dazin(1) = 0				!use intensity = 0
   else
      dazhold(8) = 1
      if p2(1:1) .eq. "D"  then
         dazin(1) = 30		!dark intensity = 30
      elseif p2(1:1) .eq. "L"  then
         dazin(1) = 200		!light intensity = 200
      else
         dazin(1) = {p2}
      endif
   endif
endif
run MID_EXE:idauxx
return
!
! 4) SHOW/CHANNEL
!
SHOW:
define/param p1 + c "Enter channel:"
action(1:4) = "MEDX"
run MID_EXE:idauxx
