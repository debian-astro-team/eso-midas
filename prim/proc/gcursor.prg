! @(#)gcursor.prg	19.1 (ES0-DMD) 02/25/03 14:08:51
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION:  GCURSOR.PRG
!.PURPOSE:  Pick up coordinates from an iteractive graphic device with a cursor
!.AUTHOR:   Ch. Ounnas  ESO- Garching
!.USE:      GET/GCURSOR descr/DES     app_flag [max_no_inputs]
!        or GET/GCURSOR table         app_flag [max_no_inputs]
!        or GET/GCURSOR
!.VERSION:  831020  ChO data of creation
!.VERSION   840228  J.D.Ponz
!.VERSION   860911  K. Banse
!--------------------------------------------------------------------
DEFINE/PARAM P2    N CHARACTER
DEFINE/PARAM P3 9999 NUMBER
!
WRITE/KEYW IN_A/C/1/60  {P1} 
WRITE/KEYW INPUTC/C/1/1 {P2(1:1)}
WRITE/KEYW CURSOR       {P3}                           !init CURSOR(1) to max...
RUN MID_EXE:GCURSOR
