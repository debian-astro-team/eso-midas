! @(#)copylocal.prg	19.1 (ESO-DMD) 02/25/03 14:08:47
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! MIDAS procedure copylocal.prg
! K. Banse	970416, 020215
!
! use via @ copylocal in/out pause_option
!     or  COPY/LSXY listfile framea frameb         (frameb only for /LSDD)
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 OUT ? "Enter direction (IN or OUT):"
! 
! 
if p1(1:1) .eq. "O" then
   create/image Midlocalsave 1,1	!create an image just for descriptors
   action(1:4) = "LSKD"
   write/keyw p1 localkd.copy
   write/keyw p2 Midlocalsave
! 
else
   define/local ff/i/1/3 0,0
   define/local record/c/1/40
   open/file localkd.copy read ff
   if ff(1) .lt. 0 return
! 
   read/file {ff(1)} record 		!check 1. record only
   if record .eq. "_NO_LOCAL_KEYS_" then
      close/file {ff(1)}
      return				!nothing to do
   else
      goto eof_test
   endif
! 
   read_loop:
   read/file {ff(1)} record 
! 
   eof_test:
   if ff(2) .gt. 1 then
      ff(3) = m$index(record," ")-1
      write/keyw mid$line {record(1:{ff(3)})}
      mid$mode(7) = mid$mode(7)-1		!get to previous proc_level
      define/local {mid$line}
      mid$mode(7) = mid$mode(7)+1
      goto read_loop
   endif
!
   close/file {ff(1)}
   action(1:4) = "LSDK"
   write/keyw p1 localkd.copy
   write/keyw p2 Midlocalsave
endif
!  
run MID_EXE:COPIER
