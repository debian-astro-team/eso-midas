#!/bin/sh
# @(#)rebcref.sh	19.1 (ESO-IPG) 02/25/03 14:09:14
# 
# Shell procedure rebcref.sh
# called from the midas procedure t2rebitbl.prg
# used to compile user functions
# author: M. Peron


cp $MIDASHOME/$MIDVERS/prim/table/src/tintertbl.mod .
$MIDASHOME/$MIDVERS/system/exec/esoext.exe -f rebu01.for
f77 -c rebu01.f
f77 tintertbl.mod  $MID_LIB/ftablib.a $MID_LIB/genlib.a $MID_LIB/twlib.a  $MID_LIB/tabuser.a $MID_LIB/midaslib.a -o tintertbl.exe
strip tintertbl.exe

