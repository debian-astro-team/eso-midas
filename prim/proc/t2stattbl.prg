! @(#)t2stattbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:08
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2stattbl.prg
!  TABLE Subsystem
!
!  J.D.Ponz                ESO - Garching    5APR89
!
! .PURPOSE
!
!    implements
!
!  STATISTICS/TABLE in-table column-ref [bin [min [max]]]
!
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter input table:"
DEFINE/PARAM P2 ? CHAR  "Enter column:"
!
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
! 
RUN MID_EXE:topertbl
