! @(#)ascitt.prg	19.1 (ESO-DMD) 02/25/03 14:08:43
! ++++++++++++++
!  
!  Midas procedure  ascitt.prg
!  convert ASCII file xyz.iasc to ITT file (table format) of host system
!  via CREATE/TABLE 
!  xyz.itt  <-  xyz.iasc
! 
!  C. Guirao    ESO - Garching   890526: Changing protections flags
!  K. Banse     ESO - Garching   890628, 910204, 930706, 980928
! 
! ++++++++++++++
! 
create/table {p1}.itt 1 256 {p1}.iasc 
name/column {p1}.itt :lab001 :itt
! 
if aux_mode(1) .le. 1 then		! VMS
   $ SET PROT=(G:RW,W:RW) {P1}.itt
else					! UNIX
   $ chmod a+rw {p1}.itt
endif
write/out table {p1}.itt converted ...
