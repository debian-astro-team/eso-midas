! @(#)t2avertbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:03
 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2avertbl.prg
!  TABLE Subsystem
!
!  J.D.Ponz                ESO - Garching  1.0     5OCT89
!
! .PURPOSE
!
!    implements
!
!    AVER/TABLE image table x,y z [siz]
!
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? IMAGE  "Enter image:"
DEFINE/PARAM P2 ? TABLE  "Enter table:"
DEFINE/PARAM P3 ? ?      "Enter columns with positions:"
DEFINE/PARAM P4 ? ?      "Enter output column:"
DEFINE/PARAM P5 0 NUMBER
!
WRITE/KEYW INPUTI/I/1/1 {P5}
!
RUN MID_EXE:topertbl
