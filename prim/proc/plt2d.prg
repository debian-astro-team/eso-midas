! @(#)plt2d.prg	19.1 (ES0-DMD) 02/25/03 14:08:58
! @(#)plt2d.prg	6.1 (ESO-IPG) 7/16/93 16:34:51
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: plt2d.prg
!.PURPOSE:        Demonstration procedure for the graphics package. 
!.AUTHOR:         Rein Warmels  ESO - Garching April. 25 , 1988
!.USE:            Execute as TUTORIAL/GRAPHICS
!.NOTE:           The following files should exist UGC.TBL in MID_TEST
!.VERSION:        880425 RHW creation
! -----------------------------------------------------------------------
!                    Here for the two-dimensional stuff
!
write/out "First, the frame spiral.bdf is copied into your directory:"
if m$exist("tspiral.bdf") .eq. 0 then
   indisk/fits MID_TEST:spiral.fits tspiral.bdf >Null
endif
echo/on
!
write/out "The graphics package is set in the default mode:"
set/graphics default                                   ! default plot setup
echo/off
write/descriptor tspiral start/d/1/2  -2940,-2980
write/descriptor tspiral step/d/1/2  10.,10.
write/out
!
create/graphics 0 600,450,0,450                           ! for the SUN
! create/graphics 0 600,450,0,325                         ! for the SONY
!
write/out "Draw a perspective plot of the galaxy center"
echo/on
plot/perspective tspiral [@250,@250:@350,@350] 20,30
wait/secs 5
set/graphics pmode=1
write/out "We can also look at it from an other view point and smooth it:
plot/perspective tspiral [@250,@250:@350,@350] 45,225 ? 2 x
wait/secs 5
!
write/out "Plot a contour and gray scale of a two-dim. frame and use some"
write/out "new plot settings:"
set/graphics pmode=1 fram=squa xformat=am yformat=.1g xscal=-100 yscal=-100
set/graphics xaxis=-1800,1800,1800,600 yaxis=-1800,1800,900,300
plot/contour tspiral [@200,@200:@400,@400] ? 1,2,3
overplot/contour tspiral [@250,@250:@350,@350] 4,5,6
overplot/gray tspiral [@275,@275:@325,@325] 1,6
echo/off
write/out " "
!
write/out "Let's fill the graph a bit with useful information:"
echo/on
set/graphics font=4
label/graphic "MIDAS" -500,600  0 2
label/graphic "Graphics" 500,-600 0 2
!
create/display 0 512,512
cuts/imag tspiral 0.0,1.5
load/image tspiral
assign/graphics display
set/graphics colour=0 font=1 fram xaxis yaxis
plot/axes [@150,@150:@450,@450] 
set/graphics colour=2
overplot/contour tspiral [@150,@150:@450,@450] 1,2,3
set/graphics colour=4
overplot/contour tspiral [@150,@150:@450,@450] 4,5,6
set/graphics font=3 colour=6
label/graphic "NGC 5247" 40,105,mm 0 1.5 1
echo/off
set/graphics
wait/secs 5
delete/graphics
wait/secs 5
delete/display
