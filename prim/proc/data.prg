! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure data.prg implements WRITE/IMAGE 
! K. Banse	910104, 930324, 950306, 070314, 110125
!
! execute via WRITE/IMAGE  frame  pix_specs  data  all_flg  process_flg
!          or WRITE/IMAGE  CURSOR,option,[npx],[nln]  constant  process_flg
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! save first letter of command in keyword ACTION
action(1:1) = mid$cmnd(1:1)
write/keyw history "WRITE/IMAGE "
! 
define/param p1 ? IMA "Enter image name: "
define/local hidehead/c/1/1 N		!needed because of READ/IMAGE
!
if p1(1:6) .eq. "cursor" then		!here with CURSOR option
   define/maxpar 3
   write/keyw in_a * 			!this means the displayed frame
   define/param p2 ? N "Enter constant: "
   define/param p3 S C "Enter process flag: "
! 
   write/keyw inputi/i/1/3 0,5,5
   write/keyw inputc {p2}		!store constant to write
   if p1(8:8) .eq. "1" then
      in_b(1:7) = "_CUR_1_"
   elseif p1(8:8) .eq. "C" then
      in_b(1:7) = "_CUR_C_"
      if  p1(9:9) .eq. "," then		!cursor,C,nolines
         if m$index(p1(10:),",") .gt. 1 then
            write/keyw inputi/i/2/2 {p1(10:)}	!npix[,nline] for window
         else
            inputi(2) = {p1(10:)}		!nline = npix for square w.
            inputi(3) = inputi(2)
         endif
      endif
   else
      in_b(1:7) = "_CUR_2_"
   endif
   cursor  = 9999
   if p3(1:2) .eq. "AL" then		!the mixup of the all flag...
      define/local process/c/1/1 S
   else
      define/local process/c/1/1 {p3(1:1)}	
   endif
! 
else					!here with "real" image name
   define/maxpar 5
   write/keyw in_a {p1}
   define/param p2 <,<,20 C "Enter xsta,ysta,nopix: "
   define/param p5 S C "Enter process flag: "
! 
   write/keyw in_b {p2}
   if mid$in(1:1) .ne. "F" then
      define/local nn/i/1/1 0
      nn = m$index(p2,",t")		!test for name,table
      if nn .lt. 1 nn = m$index(p2,",T")
      if nn .lt. 1 then
         define/param p3 ? N "Enter data values: "	!data required
      else
         define/param p3 + N "Enter data values: "	!data possible
      endif
   endif
   write/keyw inputc {p3}
   define/local process/c/1/1 {p5(1:1)}	
endif
!
run MID_EXE:DATA
