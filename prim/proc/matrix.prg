! @(#)matrix.prg	19.1 (ES0-DMD) 02/25/03 14:08:54
! +++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure matrix.prg to implement TRANSPOSE/IMAGE, TRANSPOSE/CUBE
! K. Banse	890704, 920113, 950921
!
! use via TRANSPOSE/IMAGE inframe outframe diagonal
! where    inframe = input image (matrix)
!          outframe = output image (matrix)
!          diagonal = MAJOR or MINOR, defaulted to MAJOR
! or via  TRANSPOSE/CUBE inframe plane_specs [outframe] 
!
! +++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input image: "
define/param p2 + IMA "Enter output image: "
action(1:1) = mid$cmnd(11:11)			!save qualifier
! 
write/keyw in_a {p1}
write/keyw out_a {p2}
! 
if action(1:1) .eq. "I" then			!TRANSPOSE/IMAGE
   define/param p3 MAJOR ? "Enter MAJOR/MINOR diagonal:"
   if p3(1:2) .EQ. "MI" then
      action(1:4) = "MALC"			!i.e. change lines into columns
      write/keyw inputi 128,256			!size for working buffer
   else
      action(1:4) = "MATR"
   endif
   write/keyw history "TRANSPOSE/IMAGE "
   run MID_EXE:GENXX1
! 
else						!TRANSPOSE/CUBE 
   define/param p3 YZ ? "Enter plane direction, YZ or XZ: "
   write/keyw history "TRANSPOSE/CUBE "
   action(1:3) = "TRA"
   run MID_EXE:GENXY1
endif
