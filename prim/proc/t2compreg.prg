! @(#)t2compreg.prg	19.1 (ES0-DMD) 02/25/03 14:09:03
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure t2compreg.prg
!  901108   KB: modify HISTORY stuff
!
! ----------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter table :"
DEFINE/PARAM P2 ? CHAR  "Enter column :"
DEFINE/PARAM P3 ? CHAR  "Separator (=):"
DEFINE/PARAM P4 ? CHAR  "Descriptor name :"
DEFINE/PARAM P5 R*4 CHAR  "Default output format :"
!
WRITE/KEYW HISTORY "{MID$CMND(1:6)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:topertbl
