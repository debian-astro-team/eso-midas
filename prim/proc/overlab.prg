! ++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: overlab.prg
!.PURPOSE:        write a text (80 char. max.) on a graphic device
!.USE             execute as LABEL par1 par2 par3 par4 par5, where: 
!                 par1 = "text to write"
!                 par2 = position in world units
!                 par3 = text angle
!                 par4 = character expansion factor
!                 par5 = centering parameter
!.AUTHOR:         Ch. Ounnas   820705
!.VERSION         860911 KB  ???
!                 880303 RHW centering par. added, free angle and size
!.VERSION:        930512 RHW addition for non-existing plot file
! 070928	last modif
! 
!------------------------------------------------------------------
! 
define/param p1 ? c "Enter text to be overplotted: "
define/param p2 c c
define/param p3 0 n
define/param p4 1 n
define/param p5 0 n
!
inputi = m$index(p1,"\aglquotes")
if inputi .gt. 0 then
   write/keyw outputc/c/1/80 {p1}       !get rid of any outer "'s
   inputi = m$index(outputc,"\aglquotes")
   inputi(2) = inputi+10

   if inputi .eq. 1 then
      write/keyw inputc/c/1/5 "\{"\}"
      inputc(6:) = outputc({inputi(2)}:)
   else
      inputc = outputc(1:{inputi})	!last char overwritten in next line...
      write/keyw inputc/c/{inputi}/5 "\{"\}"
      inputi = inputi+5
      inputc({inputi}:) = outputc({inputi(2)}:)
   endif
else
   write/keyw inputc/c/1/80 {p1}
endif
write/keyw inputr/r/1/2  {p3},{p4}
write/keyw inputi/i/1/1  {p5}
!
run MID_EXE:overlab
copy/graph
  
