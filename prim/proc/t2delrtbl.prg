! @(#)t2delrtbl.prg	19.1 (ES0-DMD) 02/25/03 14:09:05
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2delrtbl.prg
!  TABLE Subsystem
!   M. Peron         180193 ESO-Garching
!
! .PURPOSE
!
!    implements
!
!  DELETE/ROW   table-name   row_sel
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
DEFINE/PARAM P2 ? CHAR  "Enter row positions:"
!
WRITE/KEYW MID$CMND/C/1/4 "DELR"
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
RUN MID_EXE:TDATATBL
