! @(#)xwindow.prg	19.1 (ESO-IPG) 02/25/03 14:09:14 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  xwindow.prg  for CREATE/DISPLAY, /GRAPHICS
!                                   DELETE/DISP, GRAPH, ZOOM, CURSOR
! K. Banse  ESO - Garching 
!           901206, 921009, 940302, 950912, 001121, 010508, 020226
!
! use via CREATE/GRAPH graph_id xoff,yoff,xdim,ydim ...
! or      CREATE/DISPL displ_id xoff,yoff,xdim,ydim nomem,xm,ym ...
!         DELETE/GRAPH dspno  or DELETE/GRAPH   ALL
!         DELETE/DISPL, /ZOOM, /CURSOR   with same parameters
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
if ididev(18) .ne. 11  return		!do nothing if not X11 ...
! 
define/local comqual/c/1/2 {mid$cmnd(1:1)}{mid$cmnd(11:11)}	!save com/qual
! 
! Xwindows should have MIDAS unit 00, 01, ..., 99 !!
! 
if mid$sess(11:12) .ne. "AX" then
   if mid$sess(11:11) .lt. "0" .or. mid$sess(11:11) .gt. "9" then
      write/out  This is not a X-Window session ...!
      return
   endif
endif
! 
set/format I1
if p1(1:1) .eq. "0" .and. p1(2:2) .ne. " " then
   inputi = {p1}			!convert to integer
   write/keyw p1 {inputi}
endif
! 
! handle DELETE/DISPLAY, /GRAPHICS, /ZOOM, /CURSOR
! 
if comqual(1:1) .eq. "D" then
   define/param p1 * C "Enter display id, ALL or * : "
   ! 
   if p1(1:1) .eq. "A" then
      if aux_mode .lt. 2 then
         -delete MID_WORK:IDISERV..* 		!get rid of IDISERV
         $ run SYS_EXE:killidi.exe
      else
         -delete MID_WORK:IDISERV		!get rid of IDISERV
         $ $SYS_EXE/killidi.exe
      endif
      !
      @ dazlogin                              !force a clean keyword IDIDEV
      dazdevr(10) = -1
      dazdevr(11) = -1
      ididev(1) = -2
      mid$sess(6:7) = "--"
      return
   endif
   !
   outputi(17) = -1			!init return stat for display deletion
   action(6:6) = comqual(2:2)
   action(7:7) = "D"
   ! 
   if comqual(2:2) .eq. "G" then
      if mid$sess(7:7) .eq. "-" then
         define/local cx/c/1/2 dg
         goto nothing_to_do
      endif
      ! 
      run MID_EXE:wndcrea
      if outputi(17) .lt. 0 then		!OUTPUTI(17) is return status
         if outputi(17) .ne. -2 write/error 30	!`wndcrea' did not write yet
         return
      endif
      if outputi(20) .ne. -1 then		!OUTPUTI(20) = graphics id
         if outputi(20) .ne. dazdevr(11) then
            assign/graph g,{outputi(20)}
            write/out Active graphics window now: {outputi(20)}
         endif
      elseif mid$sess(6:6) .eq. "-" then
         define/local cx/c/1/2 zz
         goto clean_up			!all displays are gone
      else
         mid$sess(7:7) = "-"
         dazdevr(11) = -1
      endif
   else
      if mid$sess(6:6) .eq. "-" then
         define/local cx/c/1/2 dd
         goto nothing_to_do
      endif
      ! 
      run MID_EXE:wndcrea
      if comqual(2:2) .ne. "D" return  !we deleted zoom or cursor window(s)
      ! 
      if outputi(17) .lt. 0 then	!OUTPUTI(17) is return status
         if outputi(17) .ne. -2 write/error 30	!`wndcrea' did not write yet
         return
      endif
      if outputi(19) .ne. -1 then		!OUTPUTI(19) = display id
         if outputi(19) .ne. dazdevr(10) then
            assign/disp d,{outputi(19)}
            write/out Active display window now: {outputi(19)}
         endif
      elseif mid$sess(7:7) .eq. "-" then
         define/local cx/c/1/2 zz
         goto clean_up			!all displays are gone
      else
         mid$sess(6:6) = "-"
         dazdevr(10) = -1
      endif
   endif
   return
   ! 
  CLEAN_UP:
   @ dazlogin                              !force a clean keyword IDIDEV
   dazdevr(10) = -1
   dazdevr(11) = -1
   ididev(1) = -1
   mid$sess(6:7) = "--"
else
! 
! handle CREATE/DISPLAY, CREATE/GRAPHICS
! 
   if comqual(2:2) .eq. "G" then
      @ creagra.prg_o {p1} {p2} {p3} {p4}
   else
      @ creadisp.prg_o {p1} {p2} {p3} {p4} {p5} {p6} 
   endif
   set/midas dpath=??		!that sends data path to display server
endif
return
! 
NOTHING_TO_DO:
if error(2) .lt. 2 then
   if cx .eq. "DG" then
      write/out No graphics window there...
   elseif cx .eq. "DD" then 
      write/out No display window there...
   else
      write/out No display or graphics window there...
   endif
endif
