! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!  MIDAS procedure tbrow.prg
!  TABLE Subsystem
!  K. Banse 	080724		ESO - Garching
! .PURPOSE
!  use via: COPY/TBROW  table-name [col_sel] row-no  dest_key
!           copy single table row to char. dest_key
!  this command is based on READ/TABLE
!.VERSION
! 100115		last modif
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter name of table:"
DEFINE/PARAM P2 ? C "Enter optional column selection:"
DEFINE/PARAM P3 ? C "Enter row no:"
! 
! use last parameter as destination key
set/format i1
q1 = p{pcount}			!clean up for READ/TABLE
p{pcount} = "? "		!READ/TABLE should not use that par.
! 
! now execute READ/TABLE with modified options
MID$CMND(1:6) = "READ  "
MID$CMND(11:14) = "TABL"
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)} "
!
WRITE/KEYW ACTION/C/1/2 Rc
RUN MID_EXE:tdatatbl

