! @(#)t2regrlin.prg	19.1 (ESO-IPG) 02/25/03 14:09:07
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!    MIDAS Command : t2rerglin.prg
!    TABLE Subsystem
!    J.D.Ponz                ESO - Garching    5 AUG 89
!
! .PURPOSE
!    implements
!    REGRESSION/LINEAR table  y[,weights] x1,x2,x3,... [F1]
!
! .MODIFICATIONS
!    N.Rainer, DPG/DMD/ESO, 00/04/28:  - Also only 2 values possible
!                                        (for 1 independent variable) !
!                                      - Also stddev. of zero point !
!    P Nass 01/01/25     some keywords were not updated 
!                        fix problem with outputc and outputi
! .RETURN
!    Q1:  0: successful return
!         1: an error occured
!
! 020522	last modif
! 
!------------------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table:"
DEFINE/PARAM P2 ? CHAR  "Enter column with dep. variable:"
DEFINE/PARAM P3 ? CHAR  "Enter column(s) with indep. vars.:"
DEFINE/PARAM P4 2.5 NUMBER
!
define/local tmptab/c/1/20 "midtmptmp{mid$sess(11:12)}.tbl"
define/local wrktab/c/1/20 "midtmp{mid$sess(11:12)}.tbl"
define/local wrkcol/c/1/16 ":midtmp{mid$sess(11:12)}"
define/local presel/i/1/1 0
define/local INDEPNO/i/1/1 0  ! number of independent variables
define/local indloop/i/1/1 0
define/local N/i/1/1 0        ! number of values
define/local ey/r/1/1 0.
define/local Sx/d/1/1 0.
define/local Sx_2/d/1/1 0.
define/local Sy/d/1/1 0.
define/local Sxy/d/1/1 0.
define/local S_x2/d/1/1 0.
define/local D/d/1/1 0.
define/local A/r/1/1 0.
define/local B/r/1/1 0.
define/local eB/r/1/1 0.
define/local eA/r/1/1 0.
define/local colnode/i/1/1 0 ! col. no. of dependent variable
define/local colnoin/i/1/1 0 ! col. no. of independent variable
!
set/format I1 E14.7,E23.15
!
!
! Checking "NULL" values:
! ----------------------
! 
if m$index(p1,".tbl") .eq. 0 p1 = p1//".tbl"
define/local table/c/1/80 {P1}
-delete {tmptab}
-copy {p1} {tmptab}
if aux_mode(1) .le. 1 then              ! VMS
  $ SET PROT=(OWN:RW,GR:RW,W:RW) {tmptab}
else
  $ chmod u+w {tmptab}
endif
presel = {{tmptab},TBLCONTR(10)}  ! tot.no. of sel.rows
if p3(1:1) .ne. ":" .and. p3(1:1) .ne. "#" p3 = ":"//p3
if p2(1:1) .ne. ":" .and. p2(1:1) .ne. "#" p2 = ":"//p2
INDEPNO = m$parse(p3,"ind")  ! number of indep. var.
select/table {tmptab} sel.and.{p2}.ne.NULL >Null
set/format I2
do indloop = 1 INDEPNO
  select/table {tmptab} sel.and.{ind{indloop}}.ne.NULL >Null
enddo
N = outputi(1)
set/format I1
if N .lt. 2 then
  write/out -
    "ERROR   [regress/linear]:   Too few non-NULL values in {p1} !"
  return 1
elseif N .lt. presel then
  inputi(1) = presel-N
  write/out -
    "WARNING [regress/linear]:   Ignoring {inputi(1)} NULL values !"
endif
copy/table {tmptab} {wrktab}
-delete {tmptab}
!
!
! Branching according to number of independent variables
! (and number of values):
! ------------------------------------------------------
! 
create/column {wrktab} {wrkcol} " " E24.15 R*8
if INDEPNO .gt. 1 then
  !
  if N .eq. 2 then
    write/out -
      "ERROR   [regress/linear]:   Only 2 vals. / >1 indep.vars. - no calc. !"
    return 1
  endif
  !
  P1 = wrktab
  write/keyw history "REGR/LINE "
  write/keyw inputr/r/1/1   {p4}
  write/keyw mid$cmnd/c/1/4 LINE
  write/keyw outputc " " all
  write/keyw outputi 0 all
  write/keyw outputd 0. all
  write/keyw outputr 0. all
  !
  run MID_EXE:topertbl 
  !
  ey = outputr(3)
  goto after_output
  !
elseif INDEPNO .eq. 1 then
  !
  !  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  !  Taylor John R., 1988, "Fehleranalyse"/1.Auflage/S.136-144, VCH,
  !  ISBN 3-527-26878-2 :
  !  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  !  y = A + B*x  =>
  !  ("S"="sum of all"; "N"="total number of values"; "e"="stddev.")
  !
  !  D := (N*S(x^2) - (Sx)^2)
  !  A = (S(x^2)*Sy - Sx*S(xy)) / D
  !  B = (N*S(xy) - Sx*Sy) / D
  !
  !  (ey)^2 = S((y - A - B*x)^2) / (N - 2)  ! if N=2, set to 0.
  !  (eA)^2 = (ey)^2 * S(x^2) / D           ! if N=2, set to 0.
  !  (eB)^2 = N * (ey)^2 / D                ! if N=2, set to 0.
  !  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  !
  statistics/table {wrktab} {p3} >Null
  Sx = N*outputr(3)
  Sx_2 = Sx**2
  statistics/table {wrktab} {p2} >Null
  Sy = N*outputr(3)
  compute/table {wrktab} {wrkcol} = {p3}*{p2}
  statistics/table {wrktab} {wrkcol} >Null
  Sxy = N*outputr(3)
  compute/table {wrktab} {wrkcol} = {p3}**2
  statistics/table {wrktab} {wrkcol} >Null
  S_x2 = N*outputr(3)
  D = N*S_x2 - Sx_2
  !
  A = (S_x2*Sy - Sx*Sxy) / D
  B = (N*Sxy - Sx*Sy) / D
  if N .eq. 2 then
    ey = 0.
    eB = 0.
  elseif N .gt. 2 then
    compute/table {wrktab} {wrkcol} = ({p2}-{A}-{B}*{p3})**2
    statistics/table {wrktab} {wrkcol} >Null
    ey = m$sqrt(N*outputr(3) / (N-2))
    eB = m$sqrt(N * ey**2 / D)
  endif
  !
  ! keyword outputc is important for SAVE/REGRESS and COMPUTE/REGRESS
  write/keyw outputc/c/9/8 {table} ! outputc contains tablename from 9 to 16
  write/keyw outputc/c/17/4 "LINE" ! outputc contains LINE from 17 to 20
  
  ! keyword outputi is important for SAVE/REGRESS and COMPUTE/REGRESS
  write/keyw outputi/i/1/1 {N} ! OUTPUTI(1)- N,no.of data
  write/keyw outputi/i/2/1 {INDEPNO} ! OUTPUTI(2)- M,no.of ind.var.
  !
  ! search for the col. no. of the variables:  
  ! Get column number of P2 (this is the dependent variable)
  colnode = M$EXISTC("{table}","{P2}")
  
  if colnode .lt. 0 then
    write/out "Negative result - severe error!"
    return/exit
  endif
  
  write/keyw outputi/i/3/1 {colnode} !OUTPUTI(3)- col.no. of dep.var.
  ! Get column number of P3 (this is the independent variable)
  colnoin = M$EXISTC("{table}","{P3}")
  ! OUTPUTI(4)- col.no. of ind.var.i=4,...,M+3
  write/keyw outputi/i/4/1 {colnoin} 

  ! these keywords will be filled below:
  write/keyw outputd 0. all ! constant terms
  write/keyw outputr 0. all !
  
  outputd(1) = A
  outputd(2) = B
  outputr(3) = ey
  outputr(4) = eB
  !
endif
!
set/format ,E14.7
write/out " REGRESSION          Input Table: {table}    Type: LINEAR"
write/out " N. Cases:     {N};  N.Ind. Vars.: {INDEPNO}"
write/out " Dependent variable   {P2} column # {colnode} "

write/out " Var. {P3} column # {colnoin}"
write/out "   Slope :  {outputd(2)}"
write/out "   Const.:  {outputd(1)}"
write/out "   R.M.S. Error    :  {outputr(3)}"
write/out "   Std.dev. of Slope :  {outputr(4)}"
!
after_output:
set/format ,E23.15
!
! Calculating std.dev. of zero point for 1 independent variable:
! -------------------------------------------------------------
! 
if INDEPNO .eq. 1 then
  !
  if N .eq. 2 then
    eA = 0.
  elseif N .gt. 2 then
    eA = m$sqrt(ey**2 * S_x2 / D)
  endif
  !
  outputr(2) = eA
  write/out "   Std.dev. of Const.:  {outputr(2)}"
  !
endif
!
!
-delete {wrktab}
write/keyw history "REGR/LINE "
return 0
 
