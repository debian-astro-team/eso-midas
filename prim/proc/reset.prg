! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  reset.prg  for command  RESET/DISPLAY
! 
! K. Banse	910228
!
! execute as  RESET/DISPLAY [ALL]
!
!.VERSION
! 050429	last modif
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 No C "Enter NO/ALL for complete (full) reset:"
if p1(1:1) .ne. "N" then
   @ dazlogin L				!full reset of keyword `dazdevr'
   if aux_mode(1) .ne. 1 then 			!only Unix
      $ sh $MID_PROC/reset.sh {MID$SESS(11:12)}	  !get rid of all idiserv's
      mid$sess(6:7) = "--"
      return
   endif
else 
   if ididev(18) .eq. -1  return		!it's the NULL device...
endif
! 
define/local success/i/1/1 1
define/local filecntr/i/1/2 0,0
define/local reco/c/1/80 " " all
! 
open/file MID_WORK:idiserv{MID$SESS(11:12)}.log read filecntr
if filecntr(1) .lt. 0 then
   write/out "Could not find file MID_WORK:idiserv{MID$SESS(11:12)}.log ..."
   success = 0
else
   read/file {filecntr(1)} reco 80
   write/keyw reco " " all
   read/file {filecntr(1)} reco 80	!pid of IDI server is in 2. record
   close/file {filecntr(1)}
endif
if aux_mode(1) .eq. 1 then              !VAX/VMS
   -DELETE MID_WORK:IDISERV.*.*
   if success .eq. 1 then
      $ stop/id={reco}
   else
      write/out "You have to kill the process IDISERV{mid$sess(11:12)}" -
                by hand!"
   endif
   -DELETE MID_WORK:idiserv{MID$SESS(11:12)}.log.*
   -DELETE MID_WORK:sxw{MID$SESS(11:12)}*.dat.*
else                                    !Unix
   -DELETE MID_WORK:midas_xw{MID$SESS(11:12)}*	
   -DELETE MID_WORK:IDISERV
   if success .eq. 1 then
      $ kill -9 {reco}
   else
      write/out "You have to kill the process `idiserv.exe' by hand!"
   endif
   -DELETE MID_WORK:idiserv{MID$SESS(11:12)}.log
   -DELETE MID_WORK:sxw{MID$SESS(11:12)}*.dat
endif
! 
! now reset all stuff
! 
mid$sess(6:7) = "--"			!these two lines clear up
write/keyw dazdevr/i/10/2 -1,-1		!the actual display, graphics window
dazdevr(4) = 1				!set ownLUT flag to "yes, we do"
@ dazlogin 				!force a clean keyword IDIDEV
if success .eq. 0 WRITE/OUT After that
! 
WRITE/OUT -
"use CREATE/DISPLAY or CREATE/GRAPHICS to recreate your windows..."
