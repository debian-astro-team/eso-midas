! @(#)cuts.prg	19.1 (ES0-DMD) 02/25/03 14:08:48
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  cuts.prg  to set user defined values in descr. LHCUTS
! K. Banse	901023, 910903, 920522, 920925
!
! execute as CUTS frame clow,chi  set both cut values
!         or CUTS frame ,chi	  set hi cut value
!         or CUTS frame clo	  set low cut value
!         or CUTS frame frame1	  set cut values as in frame1
!         or CUTS frame 	  to look at the cuts values
!         or CUTS frame =option   to set cuts according to `option'
!            with option - sigma for cuts = mean-1/2/3*sig, mean+1/2/3*sig
!		         - high for cuts = mean-0.1*max, max
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter frame name:"
!
IF P2(1:1) .EQ. "?" THEN
   READ/DESC {P1} LHCUTS			!just show curent cuts
   RETURN
ENDIF
!
DEFINE/PARAM P2 ? ? "Enter cut specs: "
DEFINE/LOCAL NN/I/1/2 0,0
NN = M$TSTNO(P2)
!
IF P2(1:1) .NE. "=" THEN		!it's the =option mode
   IF NN .EQ. 0 THEN
      IF P2(1:1) .EQ. "," THEN			!only high cut value...
         WRITE/DESCR {P1} LHCUTS/R/2/1 {P2(2:)}
      ELSE 					!reference frame...
         COPY/DD {P2} LHCUTS/R/1/2 {P1} LHCUTS/R/1/2
      ENDIF
!
   ELSE						!low[,high] cuts...
      WRITE/DESCR {P1} LHCUTS/R/1/2 {P2}
   ENDIF
! 
! it's the =option mode
! 
ELSE
   DEFINE/LOCAL MYCUTS/R/1/2 0.,0.
   DEFINE/LOCAL WW/R/1/2 0.,0.
! 
   IF P2(2:3) .EQ. "SI" THEN
      STATIST/IMAG {P1} P5=RN
      NN(2) = 10
      DO NN = 1 3
         WW(1) = NN * OUTPUTR(4)		!1/2/3 * sigma         
! 
         WW(2) = OUTPUTR(3) - WW(1)		!mean - x*sigma
         IF WW(2) .LT. OUTPUTR(1) THEN
            MYCUTS(1) = OUTPUTR(1)
         ELSE
            MYCUTS(1) = WW(2)
         ENDIF
         WW(2) = OUTPUTR(3) + WW(1)		!mean + x*sigma
         IF WW(2) .GT. OUTPUTR(2) THEN
            MYCUTS(2) = OUTPUTR(2)
         ELSE
            MYCUTS(2) = WW(2)
         ENDIF
         NN(2) = NN(2) + 1
         OUTPUTR({NN(2)}) = MYCUTS(1)
         NN(2) = NN(2) + 1
         OUTPUTR({NN(2)}) = MYCUTS(2)
      ENDDO
      WRITE/OUT -
      "Values computed for mean +- 1,2,3*sigma" are stored -
      in OUTPUTR(11-16):
      WRITE/OUT -
      {OUTPUTR(11)},{OUTPUTR(12)}, {OUTPUTR(13)},{OUTPUTR(14)}, -
      {OUTPUTR(15)},{OUTPUTR(16)} 
      WRITE/OUT Cut values for {P1} set to: {MYCUTS(1)}, {MYCUTS(2)}
! 
   ELSEIF P2(2:3) .EQ. "HI" THEN
      STATIST/IMAG {P1} P5=RN
      WW(1) = 0.1*OUTPUTR(2)		!0.1 * max
      WW(2) = OUTPUTR(3)-WW(1)          !mean - 0.1*max
      IF WW(2) .LT. OUTPUTR(1) THEN
         MYCUTS(1) = OUTPUTR(1)
      ELSE
         MYCUTS(1) = WW(2)
      ENDIF
      MYCUTS(2) = OUTPUTR(2)
      WRITE/OUT Cut values for {P1} set to: {MYCUTS(1)}, {MYCUTS(2)}
! 
   ELSE
      WRITE/OUT Invalid cut option (see the HELP)
      WRITE/OUT Cut values for {P1} not updated ...
      RETURN
   ENDIF
! 
   WRITE/DESCR {P1} LHCUTS/R/1/2 {MYCUTS(1)},{MYCUTS(2)}
ENDIF
