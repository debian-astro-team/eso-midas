! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure shrink.prg for SHRINK/IMAGE
! K. Banse      970310
!
! use via SHRINK/IMAGE input output option
!     with option = minimum, maximum, mean, median, ...
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!  
define/param p1 ? ima "Enter input image:"
define/param p2 ? ima "Enter output image:"
define/param p3 mean C "Enter option: "
define/maxpar 3
! 
define/local dim/i/1/1 0
dim = m$value({p1},naxis)
if dim .eq. 2 then
   define/local area/c/1/6 row
else if dim .eq. 3 then
   define/local area/c/1/6 plane
else
   write/error 100
endif
define/local optio/c/1/2 sn
define/local statval/c/1/6 mean
define/local statname/c/1/12 "mean    "
! 
branch p3(1:3) MIN,MAX,MED,MOM,MOD,STD,TOT MIN,MAX,MED,MOM,MOD,STD,TOT 
goto run_it				!all local keyw. defaulted to Mean
! 
MIN:
optio(1:1) = "m"
statval = "min "
statname = "minimum "
goto run_it
! 
MAX:
optio(1:1) = "m"
statval = "max "
statname = "maximum "
goto run_it
! 
MED:
optio(1:1) = "g"			!get exact median
statval = "median"
statname = "median "
goto run_it
! 
MOM:
optio(1:1) = "r"			!get 3rd or 4th moment
if p3(4:4) .eq. "3" then
   statval = "mom3 "
   statname = "3. moment "
else
   statval = "mom4 "
   statname = "4. moment "
endif
goto run_it
! 
MOD:
optio(1:1) = "f"			!get 1st mode or mode
if p3(4:5) .eq. "E1" then
   statval = "mode1 "
   statname = "1. mode "
else
   statval = "mode "
   statname = "mode "
endif
goto run_it
! 
STD:
optio(1:1) = "s"			!get standard deviation
statval = "std"
statname = "std. dev. "
goto run_it
! 
TOT:
optio(1:1) = "r"			!get total intensity
statval = "tot"
statname = "tot. intens "
! 
RUN_IT:
statistics/image {p1} {area} ? ? {optio} {p2},i,{statval}
! 
write/out image `{p2}' with {statname} of {area}s "of input_image created"
