!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: plotrow.prg
!.PURPOSE:        MIDAS procedure to plot a row of an image
!                 execute as @ plotrow par1 [par2] [par3] [par4] 
!                 where:
!                 par1 = input frame
!                 par2 = line number (defaulted to 1)
!                 par3 = 1st point,last point on the line (default to 1,npix)
!                        this parameters is read in auto scaling mode only
!                 par4 = sc_x,sc_y,off_x,off_y (defaults device filling)
!                 or
!                 par4 = line type in overplot mode (OVERPLOT/ROW) 
!.AUTHOR:         Ch. Ounnas
!.VERSION:        RHW 861221, 870502, 880224, 920224
! 080306	last modif
! -----------------------------------------------------------------------
! 
define/param p1 ? IMA "Enter image:"                      ! frame 
define/param p2 @1                                        ! line number
!
if plrstat(1) .ne. 0.0 .and. plrstat(2) .ne. 0.0 then
   define/param p3 MANUAL C
else
   define/param p3 <,> C
   if p3(1:1) .eq. "M" then
      write/out "*** FATAL: X axis not manually set; use SET/GRAPHICS"
      return/exit
   endif
endif
!
write/keyw  in_a   {p1}					! frame
write/keyw  inputc {p3}					! start and end coord.
!
if mid$cmnd(1:1) .eq. "P" then                          ! PLOT/ROW
   define/param p4 0.,0.,-999,-999 NUM
   @ plscoff.prg_o {p4}                          ! get the scales and offsets
   dattim = m$time()
   run MID_EXE:plotrow
else                                                    ! OVER/ROW
   define/param p4 0.0 NUM
   write/keyw  inputr/r/1/1 {p4}
   define/param p5 {plistat(5)} NUM
   define/local itype/i/1/1 {plistat(5)}
   plistat(5) = {p5}
   run MID_EXE:plotrow
   plistat(5) = itype
endif
!
write/keyw plcdata/c/1/60  {p1}                    ! name of data structure
write/keyw plcdata/c/61/20 "FRAME       "          ! type of data structure
copy/graph

