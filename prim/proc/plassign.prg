! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLASSIGN.PRG
!.PURPOSE: MIDAS procedute to assign the graphic device
!          All other ASSIGN/... commands are done in ASSIGN.PRG
!.USE:     executed via ASSIGN/PLOT device_name
!.AUTHOR:  R.H. Warmels  ESO - Garching 
!.VERSION: 870211  RHW  data of creation
!.VERSION: 940207  RHW  file and file names implemented; NOSPOOL option removed
!.NOTE:    The procedure either takes the default output device stored in the 
!          keyword MID$PLOT or the input from P1.
!          The input device can either be a graphics window, a MIDAS logical 
!          name or a system device name. 
!          Graphical and display windows as well as simple terminal are taken
!          care of the the first part of this procedure. The second part of 
!          the procedure deals with hardcopy devices.
!
!          All hardcopy device checking is done in the procedure checkdev.prg
!          A fatal message will be issued if the device is unknown or missing.
!          The file agldevs.dat describes the syntax used internally.
! 
! 030709	last modif
!----------------------------------------------------------------------------
! 
define/param p1 GRAPHICS,0 C
define/local nk/i/1/1 0
define/local symbol/c/1/20 " " ALL
define/local ind/i/1/1/ 0 
define/local len/i/1/1 0
define/local meta/c/1/20 " " all
define/local type/c/1/1 " " 
set/format   i1
!
! *** HERE FOR ALL DISPLAY STUFF
IF P1(1:1) .EQ. "g" THEN                         ! here for graphics windows
   IF IDIDEV(18) .NE. 11 THEN
      WRITE/KEYW MID$PLOT/C/1/20 graph_term
   ELSE
      nk = m$index(p1,",")+1
      if nk .le. 1 then
         write/keyw nk/i/1/1 0
      else
         write/keyw nk/i/1/1 {p1({nk}:{nk})}
      endif
      write/keyw mid$plot/c/1/20 graph_wnd{NK}
      !
      if nk .lt. 0 .or. nk .gt. 9 goto error_back            ! graphics window?
      if mid$sess(7:7) .eq. " " .or. mid$sess(7:7) .eq. "-" then
         write/out "*** INFO: Graphics window {nk} created ..."
         create/graph {nk}
      elseif nk .ne. dazdevr(11) then
         @ creagra.prg_o {nk} -999 			! former switch/display
      endif
   endif
   write/keyw mid$plot/c/21/10 "NOSPOOL"
!
elseif p1(1:1) .eq. "T" then                    ! here for graphics terminal
   if ididev(18) .ne. 11 then
      write/keyw mid$plot/c/1/20 graph_term
   else
      nk = m$index(p1,",")+1
      if nk .le. 1 then
         write/keyw nk/i/1/1 0
      else
         write/keyw nk/i/1/1 {p1({nk}:{nk})}
      endif
      write/keyw mid$plot/c/1/20 graph_wnd{NK}
      !
      if nk .lt. 0 .or. nk .gt. 9 goto error_back            ! graphics window?
      if mid$sess(7:7) .eq. " " .or. mid$sess(7:7) .eq. "-" then
         write/out "*** INFO: Graphics window {nk} created ..."
         create/graph {nk}
      elseif nk .ne. dazdevr(11) then
         @ creagra.prg_o {nk} -999			! former switch/display
      endif
   endif 
   write/keyw mid$plot/c/21/10 "NOSPOOL"
!
ELSEIF P1(1:1) .EQ. "i" THEN                       ! here for display window
   IF IDIDEV(18) .NE. 11 THEN
      WRITE/KEYW MID$PLOT/C/1/20 image_displ
   ELSE
      NK = M$INDEX(P1,",")+1
      IF NK .LE. 1 THEN
         WRITE/KEYW NK/I/1/1 0
      ELSE
         WRITE/KEYW NK/I/1/1 {P1({NK}:{NK})}
      ENDIF
      WRITE/KEYW MID$PLOT/C/1/20 image_wnd{NK}
      !
      if nk .lt. 0 .or. nk .gt. 9 goto error_back             ! display window?
      if mid$sess(6:6) .eq. " " .or. mid$sess(6:6) .eq. "-" then
         write/out "*** INFO: Display window {nk} created ..."
         create/display {nk}
      elseif nk .ne. dazdevr(10) then
         @ creadisp.prg_o {nk} -999               ! former switch/display
      endif
   ENDIF
   write/keyw mid$plot/c/21/10 "NOSPOOL"
!
ELSEIF P1(1:1) .EQ. "d" THEN                       ! here for display window
   IF IDIDEV(18) .NE. 11 THEN
      WRITE/KEYW MID$PLOT/C/1/20 image_displ
   ELSE
      NK = M$INDEX(P1,",")+1
      IF NK .LE. 1 THEN
         WRITE/KEYW NK/I/1/1 0
      ELSE
         WRITE/KEYW NK/I/1/1 {P1({NK}:{NK})}
      ENDIF
      WRITE/KEYW MID$PLOT/C/1/20 image_wnd{nk}
      !
      if nk .lt. 0 .or. nk .gt. 9 goto error_back             ! display window?
      if mid$sess(6:6) .eq. " " .or. mid$sess(6:6) .eq. "-" then
         write/out "*** INFO: Display window {nk} created ..."
         create/display {nk}
      elseif nk .ne. dazdevr(10) then
         @ creadisp.prg_o {nk} -999               ! former switch/display
      endif
   ENDIF 
   write/keyw mid$plot/c/21/10 "NOSPOOL"
!
! *** here for hardcopy devices (MIDAS logical names and system names)
else
   @ checkdev.prg_o {P1}				! check name of device
   ind = m$index(p1,".")
   if ind .gt. 0 then
      write/keyw mid$plot/c/1/20 {q1}{p1({ind}:>)}      ! write name in keyword
   else
      write/keyw mid$plot/c/1/20 {q1}                   ! write name in keyword
   endif
   meta = m$lower("{q1}{mid$sess(11:12)}")              ! meta file name
   write/keyw plcmeta/c/1/80 "{meta}.plt"               
   if q1 .eq. "POSTSCRIPT" then                         ! spool option
      write/keyw mid$plot/c/21/10 "SPOOL"               ! spool for postscript
   else                                               
      define/parameter P2 S C "Enter spool or nospool option:"
      if p2 .eq. "S" then
         write/keyw mid$plot/c/21/10 "SPOOL"            ! spool
      else
         write/keyw mid$plot/c/21/10 "NOSPOOL"          ! nospool
      endif
   endif
endif 
meta = m$lower("{mid$plot(1:20)}")
write/keyw plcmeta/c/1/80 "{meta}{mid$sess(11:12)}.plt"
return
!
error_back:
write/out "*** FATAL: Invalid input..."

