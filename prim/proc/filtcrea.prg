! @(#)filtcrea.prg	19.1 (ESO-DMD) 02/25/03 14:08:51
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure FILTCREA to implement CREATE/FILTER
! K. Banse	900918, 010618
!
! execute via CREATE/FILTER name p2 p3 filter_type coefs
!
! with
!       p2 = NAXIS,NPIX(1),NPIX(2),NPIX(3)
!       p3 = START(1),...,STEP(3)
!	filter_type = BLPF - Butterworth low-pass filter
!		      ELPF - Exponential low-pass filter
!		      BHPF - Butterworth high-pass filter
!		      EHPF - Exponential high-pass filter
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter name of result frame:"
DEFINE/PARAM P4 BLPF ? "Enter filter type: "
DEFINE/PARAM P5 + N "Enter parameters for chosen filter: "
!
CREATE/IMAGE {P1} {P2} {P3} FILT_{P4} {P5}
