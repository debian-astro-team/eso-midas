! @(#)draw.prg	19.1 (ESO-DMD) 02/25/03 14:08:49
! ++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  draw.prg  to implement DRAW/qualifier
! qualifier: REC, CIRCLE, ELLIPS, SLIT, ARROW, LINE, CROSS, ANY, TRIANGLE
! K. Banse	910125, 941117, 980505
!
! use via DRAW/qualif 
!         in_spec pix_ref draw_opt colour,rot_ang nocurs key_flag [arc_spec]
! with  in_spec = CURSOR
!	        = table name
! 	        = x1,y1 or x1,y1,x2,y2
!       pix_ref = S(creen) or F(rame) - related pixel numbers
!       draw_opt : (1:1) = F or N(ot), fill_flag for RE,CI,EL,SL
!		   (1:2) = RU,RD,LU,LD direction of arrow for AR
!		   (1:3) = size of arms of cross in screen units (format I3)
!       colour,rot_ang = colour, rotation angle (in degrees)
!                        colour = name or number
!       nocurs = no. of cursors to use (where appropriate) 1 or 2
!       key_flag = K(ey) of N(okey)
!	arc_spec = start angle, end angle (for CIRCLE< ELLIPS only)
!
! +++++++++++++++++++++++++++++++++++++++++++++++
!
crossref INSPEC COOREF DROPT INTENS NOCURS KEY
!
define/local myact/c/1/2 {MID$CMND(11:12)}		!save qualifier
define/local drawcolor/c/1/80 " " all
! 
if myact(1:2) .eq. "AN" then			!DRAW/ANY
   define/param p1 white C "Enter colour: "
   if m$index(p1,".") .gt. 0 then		!we have a DRAW command file
      myact = "FI"
   else
      write/keyw cursor 99999,1
   endif
   write/keyw drawcolor {p1}
   run MID_EXE:DRAW
   return
endif
! 
define/param p1 CURSOR C "Enter input specification: "
define/param p2 + C "Enter coordinate reference, S or F: "
! 
define/param P4 white,0. C "Enter colour,rotation angle: "
! 
define/local coldx/i/1/1 0
coldx = m$index(p4,",")
if coldx .gt. 1 then
   coldx = coldx-1
   write/keyw drawcolor {p4(1:{coldx})}
   coldx = coldx+2
   write/keyw inputr {p4({coldx}:)}
else
   inputr = 0.0
   write/keyw drawcolor {p4}
endif
! 
define/param p6 N c "Enter key_flag, K(ey) or N(okey): "
! 
if myact(1:2) .eq. "CI" .or. myact(1:2) .eq. "EL" then
   define/param p7 0.,360. n "Enter begin, end of arc (in degrees):"
   write/keyw inputr/r/2/2 {p7}
else
   write/keyw inputr/r/2/2 0.,0.
endif
! 
branch myact(1:2) AR,CR,LI ARROW,CROSS,LINE
define/param p3 N c "Enter drawing option, depends on qualifier: "
define/param p5 2 n "Enter no. of cursors to use, 1 or 2: "
goto next
!
ARROW:	
define/param p3 RU c "Enter direction of arrow, RU/RD/LU/LD: "
define/param p5 2 n "Enter no. of cursors to use, 1 or 2: "
goto next
!
CROSS:
define/param p3 3 n "Enter size (in pixels) of cross: "
define/param p5 1 n "Enter no. of cursors to use, 1 or 2: "
goto next
!
LINE:
define/param p3 RU c "Enter direction of line, RU or RD: "
define/param p5 2 n "Enter no. of cursors to use, 1 or 2: "
goto next
!
NEXT:
write/keyw cursor 9999,{p5}
run MID_EXE:DRAW
