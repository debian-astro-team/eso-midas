! @(#)iminfo.prg	19.1 (ESO-DMD) 02/25/03 14:08:52
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure iminfo.prg to implement INFO/FRAME (former INFO/IMAGE)
! K. Banse	950712, 970520, 000911
!
! execute via INFO/FRAME name [option]
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ima "Enter image name:"
define/param p2 + C "Enter option:"
! 
write/keyw in_a {p1}
run MID_EXE:IMINFO
! 
if inputi(19) .eq. 9 then		!we have a FITS file
   indisk/mfits {in_a} midd hnn 
endif

