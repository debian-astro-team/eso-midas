! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  extrac1.prg  for EXTRACT/LINE, /IMAGE, /REFERENCE
! K. Banse	901108, 930323, 080122
!
! use via EXTRACT/LINE out = in[x1,y1,z1:x2,y2,z2] stepsize
! or      EXTRACT/IMAGE out = in[x1,y1,z1:x2,y2,z2]
! or      EXTRACT/IMAGE out = in[xc,yc,zc] xl,yl,zl xr,yr,zr
! or      EXTRACT/REFERENCE in ref out lo[,hi] [inout_flag]
! 	
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
write/keyw action EX{mid$cmnd(11:12)}		!init key ACTION
write/keyw history "EXTRACT/{mid$cmnd(11:14)} "
!
! branch on qualifier...
!
if action(3:4) .eq. "RE" then
   define/param p1 ? ima "Enter input image: "
   define/param p2 + ima "Enter reference image: "
   define/param p3 ? ima "Enter output image: "
   define/param p4 1.0 c "Enter thresholds: "
   define/param p5 + c "Enter inout_flag: "
   define/maxpar 5
   write/keyw in_a {p1}
   if p2(1:1) .eq. "+" then
      write/keyw in_b {p1}
   else
      write/keyw in_b {p2}
   endif
   write/keyw out_a {p3}
   if p5(1:1) .eq. "+" then
      action(1:4) = "EZX " 		!we expect a single threshold
      define/local thr/r/1/1 {p4}
   else
      define/local intval/c/1/60 {p4}
      if p5(1:1) .eq. "O" then
         action(1:4) = "EZO " 
      else
         action(1:4) = "EZI " 
      endif
   endif
! 
else
   define/param p1 ? ima "Enter result image: "
   if p2(1:2) .ne. "= " then
      write/out Error: Parameter2 is not `=' ... 
      return/exit
   endif
   define/param p3 ? ? "Enter input image and coords: "
   define/local kk/i/1/1  0
! 
   if p4(1:1) .eq. "[" then          !optional space ...
      kk = -9
   else
      kk = m$index(p3,"[")
      if kk .lt. 1 then
         write/out "Error: Parameter3 is not of the form" `frame[coords]'
         return/exit
      endif
   endif

   if action(3:4) .eq. "LI" then  		! EXTRACT/LINE
      define/local stepsize/c/1/80 " " all
! 
      if kk .eq. -9 then		!optional space ...
         write/keyw in_a {p3}
         write/keyw inputc {p4}
         write/keyw stepsize {p5}
         if stepsize(1:1) .ne. "?" then
            write/keyw inputd {stepsize}
         else
            write/keyw inputd 0.0		!indicate default stepsize
         endif
      else
         write/keyw inputc {p3({kk}:)}
         kk = kk-1
         write/keyw in_a {p3(1:{kk})}
         write/keyw stepsize {p4}
         if stepsize(1:1) .ne. "?" then
            write/keyw inputd {stepsize}
         else
            write/keyw inputd 0.0		!indicate default stepsize
         endif
      endif
   else						! EXTRACT/IMAGE
      define/local leftcoord/c/1/80 " " all
      define/local rightcoord/c/1/80 " " all
!  
      if kk .eq. -9 then		!optional space ...
         write/keyw in_a {p3}
         write/keyw inputc {p4}
         write/keyw leftcoord {p5}
         write/keyw rightcoord {p6}
      else
         write/keyw inputc {p3({kk}:)}
         kk = kk-1
         write/keyw in_a {p3(1:{kk})}
         write/keyw leftcoord {p4}
         write/keyw rightcoord {p5}
      endif
! 
      define/local inqxi/i/1/6 0 all
      if leftcoord(1:1) .ne. "?" then
         action(4:4) = "C"
         write/keyw inqxi/i/1/3 {leftcoord}
         write/keyw inqxi/i/4/3 {rightcoord}
      endif
   endif
   write/keyw out_a {p1}
endif
run MID_EXE:GENXX1
