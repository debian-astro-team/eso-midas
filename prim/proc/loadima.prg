! @(#)loadima.prg	19.1 (ESO-DMD) 02/25/03 14:08:53 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure loadima.prg  to load an image frame into image display
!                              or other hardcopy devices
! K. Banse	910322, 920312, 940204, 950526, 971113, 990909
! execute via 
! LOAD/IMAGE frame chanl xscal,yscal[,A] xc,yc cut_lo,cut_hi direcs fixpoint
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref FRAME CHANL SCALE CENTER CUTS DIRS FIX
!
! IDI device or "virtual" device
!
define/param p1 ? IMA "Enter input image: "
define/param p2 + ? "Enter channel: "
define/param p3 + ? "Enter scaling factors: "
define/param p4 + ? "Enter center pixels: "
define/param p5 + ? "Enter locut,hicut values or D/F,method: "
define/param p6 Up,No C "Enter 2-string load-directives, Up/Down,No/Overwrite: "
define/param p7 + N "Enter fixpoint pixels (framex,y,screenx,y): "
!
write/keyw in_a {p1}
! 
define/local cstuff/c/1/122 " " all
define/local fixpt/r/1/4 1.,1.,0.,0.
define/local imacuts/r/1/2 0.,0.
!
!  init DEFAULT + take care of loading direction
! 
define/local didi/i/1/1 0
didi = m$index(p6,",")
if didi .le. 0 then
   write/keyw default/c/1/6 YY{p6(1:1)}YNY
else
   didi = didi+1
   write/keyw default/c/1/6 YY{p6(1:1)}Y{P6({didi}:{didi})}Y
endif
!
!  handle Mapping size
! 
define/local sizin/i/1/2 {monitpar(20)},4000		!4000 = max. for osx 
sizin = (sizin*sizin)/2
!
!  handle scaling factors 
! 
if p3 .ne. "+" then
   default(1:1) = "N"
   write/keyw cstuff/c/1/30 {p3}
endif
!
!  handle center and fixpoint pixels
! 
if p4 .ne. "+" then
   default(2:2) = "N"
   write/keyw cstuff/c/31/60 {p4}
endif
if p7 .ne. "+" then
   write/keyw fixpt {p7}	
   default(6:6) = "N"	
   default(2:2) = "Y"		!fixpoint stuff overrides center ...
endif
!
!  handle cut values
!
if p5 .ne. "+" then
   write/keyw cstuff/c/91/30 {p5}
   if p5(1:1) .eq. "D" .or. p5(1:1) .eq. "F" then
      if p5(2:3) .ne. ",I" then
         default(4:4) = p5(1:1)
      else 			 		!use IHAP method (Cristian L.)
         define/local savdef/c/1/6 {default(1:6)}	!save key DEFAULT
         define/local xpix/i/1/1 0
         define/local ypix/i/1/1 0
         define/local xstart/i/1/1 0
         define/local xend/i/1/1 0
         define/local ystart/i/1/1 0
         define/local yend/i/1/1 0
         define/local mini/r/1/1 0.0
         define/local maxi/r/1/1 0.0
! 
         if p5(1:1) .eq. "D" then		!find out, if valid cuts
            inputi = m$existd(in_a,"lhcuts")	!exist already
            if inputi .eq. 1 .and. -
               {{in_a},lhcuts(1)} .lt. {{in_a},lhcuts(2)} goto load_it 
         endif
! 
         set/midas output=no
! 
         xpix = m$value({in_a},npix(1))
         ypix = m$value({in_a},npix(2))
         xstart = xpix*0.20+0.5			!to have at least 1 
         xend   = xpix*0.80+0.5
         ystart = ypix*0.10+0.5
         yend   = ypix*0.12+0.5
         statist/image {in_a} [@{xstart},@{ystart}:@{xend},@{yend}]
! 
         mini = outputr(3)
         maxi = mini
         ystart = ypix*0.50
         yend   = ypix*0.52
         statist/image {in_a} [@{xstart},@{ystart}:@{xend},@{yend}]
! 
         if mini .gt. outputr(3) mini = outputr(3)
         if maxi .lt. outputr(3) maxi = outputr(3)
         ystart = ypix*0.88
         yend   = ypix*0.90
         statist/image {in_a} [@{xstart},@{ystart}:@{xend},@{yend}] 
! 
         write/keyw default/c/1/6 {savdef(1:6)}		!reset DEFAULT
         if mini .gt. outputr(3) mini = outputr(3)
         if maxi .lt. outputr(3) maxi = outputr(3)
! 
         set/midas output=yes
         write/out Mean level = {outputr(3)}
         mini = 0.9*mini
         maxi = 1.2*maxi
         default(4:4) = "N"
         write/keyw imacuts {mini},{maxi}
      endif
   else
      default(4:4) = "N"
      write/keyw imacuts {p5}	
   endif
endif
!
!  now load
! 
LOAD_IT:
!
!  test, if it is a virtual device... ( = a display frame)
!
if mid$disp(1:1) .ne. "I" then
   run MID_EXE:WRIMA
   if mid$disp(1:1) .ne. "F" .and. mid$disp(1:1) .ne. "N" then
      @ hardcopy ? {in_b} i,{hcittlut(1:20)}
   endif
else
   if mid$sess(6:6) .eq. " " .or. mid$sess(6:6) .eq. "-" then
      create/display
   endif
   run MID_EXE:WRIMA
endif
