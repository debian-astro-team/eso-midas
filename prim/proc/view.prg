! @(#)view.prg	19.1 (ES0-DMD) 02/25/03 14:09:13
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure view.prg to implement VIEW/IMAGE
! K. Banse      901009, 911203, 920722, 941017, 941214, 950724
!
! use via VIEW/IMAGE frame outtab plotflag g_hardcopy,z_hardcopy
! 
! with  frame = image name
!       outtab = optional output table
!       plotflag = character flag: P/N for plotting or not plotting
!	g_hardcopy,z_hardcopy = name of printer for graph,zoom_copy
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 + IMA "Enter image name: "
define/param p2 + TAB "Enter output table: "
define/param p3 P C "Enter plotflag P/N for plot/noplot: "
define/param p4 LASER,LASER C -
   "Enter plotters for graphics,zoom hardcopy output: "
! 
define/local origfile/c/1/60 {p1}		!save original file name
define/local cflags/c/1/2 {p3(1:1)}{p4(1:1)}
define/local sizin/i/1/2 32000,4000		!needed for LOAD/IMA
define/local chan/i/1/1 {ididev(15)}
!
default(11:12) = "XN"
if p1(1:1) .ne. "+" then
   write/keyw in_a {p1}
   default(11:11) = "L"
else
   write/keyw in_a "* "			!default to displayed frame
endif
write/keyw out_a {p2}			!store output table
!
!  check, if we have a display...
!
if mid$sess(6:6) .eq. " " .or. mid$sess(6:6) .eq. "-" then
   if p1(1:1) .eq. "+" then
      write/out "missing image name ..."
      return/exit
   else
      create/display 0 
   endif
endif
if p3(1:1) .ne. "N" .and. dazdevr(11) .lt. 0 then
   create/graphics                      !make sure we have a graphics window
endif
! 
write/keyw dazin/i/1/5 4,-1,-1,-1,-1
!
define/local kk/i/1/2 0,0
kk = m$index(p4,",")
if kk .lt. 2 then
   define/local ghcop/c/1/20 {p4}
   define/local zhcop/c/1/20 {p4}
else
   kk = kk - 1
   define/local ghcop/c/1/20 {p4(1:{kk})}
   kk = kk + 2
   define/local zhcop/c/1/20 {p4({kk}:)}
endif
! 
if aux_mode(1) .lt. 2 then		!remove old extracted frames 
   -delete view_*.*.*
else
   $rm -f view_*
endif
! 
dattim = m$time()
in_b(1:1) = "?"                         !force to default Xstation
run MID_EXE:VIEW
! 
if aux_mode(1) .gt. 1 return
! 
! for VMS we still have to get rid of the hard copies 
! 
write/keyw action/c/5/3 xxx			!clean that keyword...
set/format I2
! 
!  check keyword OUTPUTI(11) to see if we have graphic hardcopies
if outputi(11) .gt. 0 then
   kk(2) = outputi(11)-1
   do kk(1) = 0 kk(2)
      copy/graph {ghcop} view_gcopy{kk(1)} 
      write/out "plot file view_gcopy{kk(1)} sent to plotter" {ghcop}
   enddo
endif
! 
!  check keyword OUTPUTI(12) to see if we have image hardcopies
if outputi(12) .gt. 0 then
   kk(2) = outputi(12)-1
   do kk(1) = 0 kk(2)
      @ hardcopy {zhcop} view_zcopy{kk(1)} P ? ? PC8NT
   enddo
endif
