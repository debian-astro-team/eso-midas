! @(#)creaux.prg	19.1 (ES0-DMD) 02/25/03 14:08:47
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  creaux.prg  for CREATE/ZOOM, /CURSOR
! 
! use as 
!  create/zoom_window  display_id  xdim,ydim,xoff,yoff  Xstation_name
! 
!  K. Banse     920117, 920523, 920723, 940228, 950704
!  ESO/IPG - Garching
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 0 n "Enter display id: " 0,9
define/param p2 + n "Enter x-,y-size,x-,y-off for aux_window: "
define/param p3 + ? "Enter name of XWindowScreen: "
! 
if {p1} .ne. dazdevr(10) then		!main display window must be active...
   write/out Display window {p1} currently not active.
   write/out Use ASSIGN/DISPLAY d,{p1} to switch ...
   return
endif
! 
! only create zoom_window (cursor_window) if not there yet
! 
write/keyw inputi -1,-1,-1,-1		!init to defaults
if mid$cmnd(11:11) .eq. "Z" then	!zoom window
   if ididev(19) .gt. 0 return
   write/keyw in_b zoom
   action(6:7) = "ZC"
else					!cursor window
   if ididev(26) .gt. 0 return
   write/keyw in_b curs
   action(6:7) = "CC"
endif
! 
if p2(1:2) .ne. "+ " then
   write/keyw inputi {p2}
   if inputi(1) .le. 0 .or. inputi(2) .le. 0 goto bad_dims
endif
if p3(1:1) .ne. "+" write/keyw in_b/c/5/50 ,{p3}
run MID_EXE:wndcrea.exe
return
! 
bad_dims:
write/out Bad dimensions for zoom window...
