! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure imcreate.prg to implement CREATE/IMAGE
! K. Banse	910108 - creation
!
! execute via CREATE/IMAGE name p2 p3 func_type coefs data_type
! or          CREATE/IMAGE name = ref_frame func_type coefs data_type
!             CREATE/IMAGE name = GCURSOR [mode]
! 
! (implicitely also) CREATE/FILTER name p2 p3 filter_type coefs
!
! with
!       p2 = NAXIS,NPIX(1),NPIX(2),NPIX(3)
!       p3 = START(1),...,STEP(3)
!       func_type = POLY, SEQUENCE or GAUSS
!                   ASCII for reading in data from a file
!                   TABLE for using x,y values from a table
!                   LIST for building image from list of frames of dim-1
!	coefs = coefficients for polynomial a + bx + cy + dxy + exx + fyy
!               or the other functions
!               or the data file name (table name) if ASCII (TABLE)
!       mode = (default) create new table CONV1D, input from graphics cursor
!              ADD     - add new points to table CONV1D
!              DELETE  - delete points from table CONV1D
!	data_type = R (default), I4, I2, UI2, I1, D or R8
! 
!.VERSION: 
! 910628	RHW  Keyword PLCURSOR 20 characters copied (was 8)
! 911112	MP   keyword FILE also 20 characters
! 
! 060601	last modif
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter image name:"
define/param p4 poly c "Enter function type: "
define/param p5 + C "Enter coefficients (or file name for ASCII function): "
define/param p6 R C "Enter data type:"
define/local pp6/c/1/4 {p6}
! 
if p2(1:1) .eq. "=" then		!check for reference frame
   !
   ! 	here the reference file option
   !
   if p3(1:7) .eq. "GCURSOR" then
      define/param p4 G
      define/local file/c/1/60 REFIMA
      !
      if p4(1:1) .eq. "A"  goto add
      if p4(1:1) .eq. "D"  goto delete
      !
      copy/kk plcdata/c/1/60 file/c/1/60
      if file(1:3) .eq. "   " .or. file(1:3) .eq. "unk" then
         write/out "we need an image on the graphics screen ..."
         return/exit
      endif
      write/out "                     >>> Use graphics cursor to enter data <<<"
      get/gcurs CONV1D
      goto fit
      !
     ADD: 	 !2nd case: add points to previoulsy created table FIT1D
      create/image REFIMA = {p1} NODATA
      plot/row {p1}
      overplot/table CONV1D :X_AXIS :Y_AXIS
      get/gcurs CONV1D ADD
      goto fit
      !
     DELETE: 	! 3rd case: delete points from previously created table FIT1D
      create/image REFIMA = {P1} NODATA
      plot/row {p1}
      overplot/table CONV1D :X_AXIS :Y_AXIS
      identify/gcurs CONV1D :X_AXIS :X_AXIS :Y_AXIS 3
      !
     FIT: 				! do the spline fit
      sort/table CONV1D :X_AXIS
      convert/table {p1} = CONV1D :X_AXIS :Y_AXIS {FILE} SPLINE
      overplot/row {p1}
      return
      !
   else
      ! real image stuff...
      define/param p3 ? IMA "Enter reference image name:"
      default(1:1) = "N"
      write/keyw in_a {p3}
      write/keyw inputi {{in_a},naxis}		!only NAXIS
   endif
else
   !
   ! 	here the NAXIS,NPIX(i) START(i),STEP(i) option
   !
   define/param p2 1,512 N "Enter Naxis,Npix(1),Npix(2),..,Npix(Naxis):"
   default(1:1) = "Y"
   write/keyw inputi 1,1,1,1,1,1
   write/keyw inputi {p2}		!NAXIS + NPIX
endif
! 
! ===================
! 
! now the common part
! 
! ===================
! 
! 
set/format i1
define/local save5/c/1/80 "+- "
write/keyw in_a {p1}				!name of new frame
write/keyw history "CREATE/IMAGE "
! 
!  check for option SEQUENCE (because we handle it already here...)
if p4(1:2) .eq. "SE" then
   if inputi(1) .eq. 3 then		!start = 0., step = 1.
      write/keyw inputd 0.,0.,0.,1.,1.,1.
   elseif inputi(1) .eq. 2 then
      write/keyw inputd 0.,0.,1.,1.
   elseif inputi(1) .eq. 1 then
      write/keyw inputd 0.,1.
   else
      write/out option SEQUENCE for max. 3-dim frames only...
      return/exit
   endif
   ! 
   write/keyw inputr {p5}		!get start,end of interval
   inputr(2) = (inputr(2)-inputr(1))/(inputi(2)-1) 	 !(a1-a0)/(nopix-1)
   write/keyw p5 {inputr(1)},{inputr(2)}
   write/keyw p4 POLY			!now continue like POLYNOMIAL
   ! 
else
   if default(1:1) .ne. "N" then
      if inputi(1) .le. 1 then			!NAXIS = 0 also possible
         define/param p3 0.,1. N "Enter Start(1),..,Step(1),..: "
      elseif inputi(1) .eq. 2 then
         define/param p3 0.,0.,1.,1. N "Enter Start(1),..,Step(1),..: "
      elseif inputi(1) .eq. 3 then
         define/param p3 0.,0.,0.,1.,1.,1. N "Enter Start(1),..,Step(1),..: "
      else
         define/param p3 ? N "Enter Start(1),..,Step(1),..: "
      endif
      write/keyw inputd {p3}				!START + STEP
   endif
   ! 
   !  check for option LIST (because we need listcrea.prg to finish the job)
   if p4(1:2) .eq. "LI" then
      if inputi(1) .lt. 2 .or. inputi(1) .gt. 3 then
         write/error 100
      return
      endif
      write/keyw save5/c/1/80 {p5}
      p5 = "0  "
      write/keyw p4 POLY			!continue like POLYNOMIAL
   endif
endif
! 
run MID_EXE:CRFRAM
if save5(1:2) .ne. "+-" then			!finish LIST or NoData option
   if default(1:1) .ne. "Y" then
      if inputi(1) .eq. 2 then
         define/local pp2/c/1/60 {{p3},npix(1)},{{p3},npix(2)}
      else
         define/local pp2/c/1/60 {{p3},npix(1)},{{p3},npix(2)},{{p3},npix(3)}
      endif
      @a listcrea {in_a} {inputi(1)},{pp2} 1 {save5}
   else
      @a listcrea {in_a} {p2} 1 {save5}
   endif
endif
