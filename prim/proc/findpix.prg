! @(#)findpix.prg	19.1 (ES0-DMD) 02/25/03 14:08:51
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  findpix.prg  to implement FIND/PIXEL, /MINMAX
! K. Banse	890705, 920409
!
! execute via FIND/PIXEL frame xlow,xhi IN/OUT FIRST/ALL table
! or          FIND/MINMAX frame
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "enter image name: "
write/keyw in_a {P1}
!
if mid$cmnd(11:11) .ne. "P" then	!FIND/MINMAX is default
   write/keyw action FNMIN?
! 
else					!FIND/PIXEL
   define/param p2 ? C "Enter xlow,xhigh: "
   define/param p3 IN C "Enter IN(side) or OUT(side) of given interval: "
   define/param p4 F C "Enter F(irst) or A(ll) pixels: "
   if p4(1:1) .ne. "F" then
      define/param p5 + TABLE "Enter optional table name: "
      write/keyw in_b {p5}
   endif   
! 
   define/param p6 0 N -
                "Enter max. no. of pixels to be selected (0 for no limit):"
   inputi(1) = {p6}
   write/keyw action/c/1/5 FN{p3}
   action(6:6) = p4(1:1)
   write/keyw history "FIND/PIXEL "
endif
! 
run MID_EXE:GENZZ1
