! @(#)pltgen.prg	19.1 (ES0-DMD) 02/25/03 14:08:58
! @(#)pltgen.prg	6.1 (ESO-IPG) 7/16/93 16:34:54
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLTDEMO.PRG
!.PURPOSE:        Demonstration procedure for the graphics package. 
!.AUTHOR:         Rein Warmels  ESO - Garching April. 25 , 1988
!.USE:            Execute as TUTORIAL/GRAPHICS
!.NOTE:           The following files should exist UGC.TBL in MID_TEST
!.VERSION:        880425 RHW creation
! -----------------------------------------------------------------------
!             Here for the general stuff
!
write/out "First, let's have a look at the default graphics setup:"
echo/on
set/grap default                                     ! default graphics setup
show/grap                                            ! show the setup
wait/secs 3
echo/off
!
write/out "Assign the graphics to the terminal or graphics window, and"
write/out "avoid that a plot file is created."
echo/on
create/graph 0 600,450,0,450                            ! for the SUN
! create/graph 0 600,450,0,325                          ! for the SONY
assign/grap G,0
echo/off
write/out " "
!
write/out "Now, plot a set of axes using some command defaults:"
echo/on
set/grap pmode=-1
plot/axes 0,1.0 0,1.8
echo/off
write/out " " 
!
write/out "There are a number line types and fonts available:"
echo/on
set/grap colour=1
overplot/line 1 0.0,0.1 0.5,0.1
label/graphic "Line type 1, colour 1" 0.8,0.1 0 1 0
!
set/grap colour=2
overplot/line 2 0.0,0.2 0.5,0.2
label/graphic "Line type 2, colour 2" 0.8,0.2 0 1 0
!
set/grap colour=3
overplot/line 3 0.0,0.3 0.5,0.3 
label/graphic "Line type 3, colour 3" 0.8,0.3 0 1 0
!
set/grap colour=4
overplot/line 4 0.0,0.4 0.5,0.4
label/graphic "Line type 4, colour 4" 0.8,0.4 0 1 0
!
set/grap colour=5
overplot/line 5 0.0,0.5 0.5,0.5 
label/graphic "Line type 5, colour 5" 0.8,0.5 0 1 0
!
set/grap colour=6
overplot/line 6 0.0,0.6 0.5,0.6 
label/graphic "Line type 6, colour 6" 0.8,0.6 0 1 0
echo/off
write/out " " 
!
write/out "With the font option you can have a number of fonts:"
echo/on
set/grap colour=1
set/grap font=1
label/graphic "Roman Font:  ABCDE abcde 12345" 0.5,0.8
set/grap font=2
label/graphic "Greek Font:  ABCDE abcde 12345" 0.5,0.9
set/grap font=3
label/graphic "Script Font: ABCDE abcde 12345" 0.5,1.0
set/grap font=4
label/graphic "Old English: ABCDE abcde 12345" 0.5,1.1
set/grap font=5
label/graphic "Tiny Font:   ABCDE abcde 12345" 0.5,1.2
echo/off
write/out " " 
!
write/out "You have complete freedom in make labels (size, angle, position):"
echo/on
set/grap pmode=0
label/graphic "ssize=1.0" 0.9,1.4 315.0 1.0 1
label/graphic "ssize=0.5" 0.1,1.4 45.0  0.5 2
label/graphic "ssize=4.0" 0.5,1.4 0.0   4.0 0
echo/off
!
overplot/symb 1  .05,1.6 1.0
overplot/symb 2  .10,1.6 1.0
overplot/symb 3  .15,1.6 1.5
overplot/symb 4  .20,1.6 1.5
overplot/symb 5  .25,1.6 2.0
overplot/symb 6  .30,1.6 2.0
overplot/symb 7  .35,1.6 2.5
overplot/symb 8  .40,1.6 2.5
overplot/symb 9  .45,1.6 2.0
overplot/symb 10 .50,1.6 2.0
overplot/symb 11 .55,1.6 1.5
overplot/symb 12 .60,1.6 1.5
overplot/symb 13 .65,1.6 1.0
overplot/symb 14 .70,1.6 1.0
overplot/symb 15 .75,1.6 0.5
overplot/symb 16 .80,1.6 0.5
overplot/symb 17 .85,1.6 1.0
overplot/symb 18 .90,1.6 1.0
overplot/symb 19 .95,1.6 1.5
!
set/grap font=4
label/graphic "MIDAS Graphics" 0.5,0.8 30 4.0
set/grap
wait/secs 10
delete/grap
