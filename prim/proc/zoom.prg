! @(#)zoom.prg	19.1 (ES0-DMD) 02/25/03 14:09:14
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  zoom.prg  to handle ZOOM/CHANNEL, CLEAR/ZOOM
! K. Banse	901017, 920312, 950119
!
! use via ZOOM/CHAN [zoom [xpos,ypos]]
!         ZOOM/CHAN UP, ZOOM/CHAN DOWN 
! or      CLEAR/ZOOM chanl
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
if mid$cmnd(11:11) .eq. "Z" then		!handle CLEAR/ZOOM specially
   if p1(1:1) .ne. "?" @% setchan {p1(1:1)}	!P1 denotes a channel...
   write/keyw action/c/3/2 XI
   goto run_it
endif
!
define/param p1 C ? "Enter zoom or C for cursor rectangle:"
define/param p2 + ? "Enter xcenter,ycenter for zoom:"
define/maxpar 2
!
!  init keyword ACTION
write/keyw action/c/3/3 {p1(1:1)}{mid$cmnd(11:11)}Z	
!
if m$tstno(p1(1:1)) .eq. 1 then
   action(3:3) = "F"
   write/keyw dazhold/i/6/1 {p1}		!store explicit zoom factor
   dazhold(7) = dazhold(6)
!
   if p2(1:1) .ne. "+" then
      action(5:5) = "X"
      write/keyw inputi/i/1/2 {p2},{p2}		!force 2 numbers...
   endif
endif
!
RUN_IT:
default(16:16) = "Z"
action(1:2) = "SC"
run MID_EXE:IDAUXZ
