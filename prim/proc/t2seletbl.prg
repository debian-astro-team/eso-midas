! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : t2seletbl.prg
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching    5 APR 89
!
! .PURPOSE
!
!    implements
!
! .ALGORITHM
!   The index array mid$selidx has to be defined globally, in order to access
!   all rows found by the selection criterium. Otherwise only the first row
!   matching the selection parameter will be returned in Q1.
!   (ADDED by: swolf@eso.org)
!
!  SELECT/TABLE table-name  expression
!
! 070810		last modif
! 
! ----------------------------------------------------------------------
!
define/param p1 ? table  "Enter table:"
define/param p2 ? char   "Enter expression:"
!
write/keyw history "{mid$cmnd(1:4)}/{mid$cmnd(11:14)} "
write/keyw in_a {p1}
!
info/key mid$selidx
if mid$info(1) .eq. 0 then		!index-array does not exist
  define/local mid$msel/i/1/1 1
  define/local mid$selidx/i/1/1 0
else if mid$info(1) .ne. 1 then		!wrong type of index array
  write/out "ERROR: the index array has to be of type INTEGER"
  return
else					!ok.; get number of elements
  define/local mid$msel/i/1/1 {mid$info(2)}
  write/keyw mid$selidx/i/1/{mid$msel} 0 ALL
endif
!
set/format   i1
define/local string/c/1/200 {p2}
define/local l/i/1/1 0
!
if pcount .ge. 3 then
  do l = 3 {pcount}
     string(1:) = string(1:) // p{l}
  enddo
endif
!
run MID_EXE:tdatatbl
!
return {mid$selidx(1)}
