/* @(#)edterror.c	19.1 (ES0-DMD) 02/25/03 14:00:48 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        edterror.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table editor
.PURPOSE
             Display message on dialogue window.
.RETURNS     error status.
.VERSION 1.0 25-Mar-1988   Implementation     JDP
.VERSION 1.1 30-Oct-1990   New table version  FO
------------------------------------------------------------*/

#include <tbledt.h>
#include <tw.h>
#include <proto_tbl.h>

static int error_state = 0;
static WINDOW *error_window = NULL_WINDOW;

int ShowError(mess)
   char *mess                           /* IN :  */;
{
  int status;
   char *ch;

    if (!mess)	mess = "";
    if (!error_window)
	error_window = OpenWindow("edt_err", 0,0, 1,0, 
		_BLINK_|_REVERSE_|_BOLD_, _DISPLAY_, 0);

    ClearWindow(error_window);
/*     status = Put(error_window, ">>> Error : "); */
    status = Put(error_window, mess);
    RaiseWindow(error_window), Bell();
    error_state = 1;

    return (status);
}


int ClearError()

{  int status;
   char *ch;

    
    if (error_state)   RemoveWindow(error_window);
  
    error_state = 0;

    return (error_state);
}
