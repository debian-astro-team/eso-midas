/* @(#)edtinit.c	19.1 (ES0-DMD) 02/25/03 14:00:48 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        edtinit.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table editor
.PURPOSE
             Initializes editor windows and displays
             general information in dialogue window.

.RETURNS     action code.
.VERSION     1.0 25-Mar-1988   Implementation     JDP
.VERSION     1.1 07-JAn-1991                      MP
------------------------------------------------------------*/

#include <tbledt.h>
#include <macrogen.h>		/* For MAX, MIN	*/
#include <proto_tbl.h>

   static int  swidth = 9;	/* Width of Sequence Column */	

   int edt_init(table)

   char table[]                       /* IN : table name */;

{  int status;
   short int pos2[2];
   int  i, help_lines;
   WINDOW *help_title, *help_display, *help_prompt;
   char *name, *oshenv();

#define dlen 3
                                     /* initialize window package */

/*   status = InitWindows(edt_term, edt_termfile, edt_env); */

                               /* init editor window and related subwindows*/

/*   i = 0; while (table[i] != ' ' ) i++;  table[i] = '\0'; MP */

 header_window = OpenWindow(table,0,0,dlen,0,_NORMAL_,_TITLE_ | _DISPLAY_,0);

   status = 0;
   ScreenSize(pos2);
   data_lines = pos2[0] - dlen;
   data_columns = pos2[1] - swidth;

 editor_window = OpenWindow("editor",dlen,0,data_lines,0,_NORMAL_,_DISPLAY_,0);

   sequence_subwindow = OpenSubWindow(editor_window,
                        "sequence",0,0,0,swidth,_NORMAL_,_DISPLAY_,0);

   data_subwindow = OpenSubWindow(editor_window,
                        "data",0,swidth,0,0,_NORMAL_,_NORMAL_,0);

   SetStopping(data_subwindow,_STANDARD_, "I");	/* Exit on Tab  = Control_I */
   SetStopping(data_subwindow,_FK_, "*");	/* Exit on Function Keys */
   EnableArrows(data_subwindow);   
   EnableKeypad(data_subwindow);   
                                     /* init dialogue window */

   dialogue_window = OpenWindow("dialogue",
                      pos2[0]-4,0,4,pos2[1],_NORMAL_,_BORDER_ | _SCROLL_,10);

                                     /* init help window */;
   help_lines = MIN(24, pos2[0]);
   help_display = OpenWindow("Help", 0,0, help_lines,0, _NORMAL_, _DISPLAY_, 0);

   help_title  = OpenSubWindow(help_display, "Helpt", 0,0, 1,0, _NORMAL_,
		_DISPLAY_, 0);
   help_display  = OpenSubWindow(help_display, "Helpd", 1,0, help_lines-2,0, 
		_NORMAL_, _DISPLAY_, 0);

   help_prompt = OpenWindow("HelpD", -1,0, 0,0, _NORMAL_, 0, 1);
   EnableArrows(help_prompt);   
   EnableKeypad(help_prompt);   
   SetStopping(help_prompt, _FK_, "*");
   SetStopping(help_prompt, 0, "\t");

   name = oshenv("MID_TEDIT", (char *)0);
   if (!name)	name = "help.twh";		/* Default Help File Name */
   twh = InitHelp(name, help_title, help_display, help_prompt);
                                                /* Disable error logging */
   pm_open((char *)0, (char *)0);

   return (status);
}
