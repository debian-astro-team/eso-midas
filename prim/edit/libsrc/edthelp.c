/* @(#)edthelp.c	19.1 (ES0-DMD) 02/25/03 14:00:48 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        edthelp.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table editor
.PURPOSE
             Help facility

.RETURNS     action code.
.VERSION     1.0 25-Mar-1988   Implementation     JDP
------------------------------------------------------------*/

#include <tbledt.h>
#include <proto_tbl.h>

char *tk_find();

   int edt_help0()

{  int   key_group;
   short key1, key2;
   char  key_hit;

   DisplayHelpTopic(twh, "Keypad");       /* display help keypad */;
   while (1) 
      {
       key_group = GetKey(twh->w[2], &key_hit);
       if (key_group == _STANDARD_ && key_hit == ' ')          break;
       if (key_group == _STANDARD_)  	key_group = 0;
       key1 = key_group<<8 | key_hit;
       key2 = 0;
       switch(tk_check(key1))
	{
	  case 0: DisplayHelpTopic(twh, "_bad_key");            continue;
	  case 2: key_group = GetKey(twh->w[2], &key_hit);
  		  key2      = key_group<<8 | key_hit;
	  case 1: DisplayHelpTopic(twh, tk_find(key1, key2));  continue;
	}
   }
   RemoveWindow(twh->w[1]);
   RemoveWindow(twh->w[2]);
   return (NULLACTION);
}

   int edt_help1()

{  int   key_group;
   short key1, key2;
   char  key_hit;

   DisplayHelpTopic(twh, "function");       /* display help function */;
   while (1) 
      {
       key_group = GetKey(twh->w[2], &key_hit);
       if (key_group == _STANDARD_ && key_hit == ' ')          break;
       if (key_group == _STANDARD_)  	key_group = 0;
       key1 = key_group<<8 | key_hit;
       key2 = 0;
       switch(tk_check(key1))
	{
	  case 0: DisplayHelpTopic(twh, "_bad_key");            continue;
	  case 2: key_group = GetKey(twh->w[2], &key_hit);
  		  key2      = key_group<<8 | key_hit;
	  case 1: DisplayHelpTopic(twh, tk_find(key1, key2));  continue;
	}
   }
   RemoveWindow(twh->w[1]);
   RemoveWindow(twh->w[2]);
   return (NULLACTION);
}

   int edt_help()

{  int   key_group;
   short key1, key2;
   char  key_hit;

   DisplayHelpTopic(twh, "Help");       /* display main help */;
   while (1) 
      {
       key_group = GetKey(twh->w[2], &key_hit);
       if (key_group == _STANDARD_ && key_hit == ' ')          break;
       if (key_group == _STANDARD_)  	key_group = 0;
       key1 = key_group<<8 | key_hit;
       key2 = 0;
	
       switch(tk_check(key1))
	{
	  case 0: DisplayHelpTopic(twh, "_bad_key");            continue;
	  case 2: key_group = GetKey(twh->w[2], &key_hit);
  		  key2      = key_group<<8 | key_hit;
	  case 1: DisplayHelpTopic(twh, tk_find(key1, key2));  continue;
	}
   }
   RemoveWindow(twh->w[1]);
   RemoveWindow(twh->w[2]);
   return (NULLACTION);
}
