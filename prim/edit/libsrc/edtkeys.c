/* @(#)edtkeys.c	19.1 (ES0-DMD) 02/25/03 14:00:48 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)edtkeys.c	4.1.1.1 (ESO-IPG) 7/30/92 17:31:09 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        edtkeys.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table editor
.PURPOSE
             Initializes editor key functions.

.RETURNS     action code.
.VERSION     1.0 25-Mar-1988   Implementation     JDP
.VERSION     1.1 28-Feb-1991	Added a few keys (F1 + KeyPad)
------------------------------------------------------------*/

#include <stesodef.h>
#include <buffer.h>
#include <str.h>
#include <tw.h>
#include <proto_tbl.h>

#define PF(k)   (_PF_<<8) | k             /* define PF function Keys */
#define F(k)    (_FK_<<8) | k             /* define Function Keys */
#define KP(c)   (_KEYPAD_<<8) | c         /* define Keypad Keys */
#define AR(k)   (_ARROW_<<8) | k         /* define arrow keys where 
                                           k = _UP_, _DOWN_, _LEFT_, _RIGHT_*/


int edt_keys()


{  
 int edt_help0(),    edt_prevline(),  edt_nextline(), edt_prevcol(),
     edt_nextcol(),  edt_fndnxt(),    edt_dell(),     edt_find(),
     edt_undell(),   edt_line(),      edt_lastcol(),  edt_help1(),
     edt_chgfmt(),   edt_adv(),       edt_backup(),   edt_creacol(),
     edt_newpage(),  edt_newsection(),edt_rightpage(),edt_delw(),
     edt_delc(),     edt_badkey(),    edt_screen(),   edt_bottomtbl(),
     edt_toptbl(),   edt_delcol(),    edt_command(),  edt_badkey(),
     edt_leftpage(), edt_undelw(),    edt_undelc(),   edt_noaction(),
     edt_exit(),     edt_quit(),      edt_findrow(),  edt_showtable(),
     edt_tabular(),  edt_shstat(),    edt_sort(),     edt_addrow(),
     edt_delrow();


						/* Control Keys */
tk_add('\t', 0, edt_tabular, "tabular");
tk_add(Control_('n'),0,edt_nextline,  "_next_line");   
                                                     /* arrow keys */
tk_add(AR(_UP_),   0,edt_prevline,"_arrow");   
tk_add(AR(_DOWN_), 0,edt_nextline,"_arrow");   
tk_add(AR(_LEFT_), 0,edt_prevcol ,"_arrow");   
tk_add(AR(_RIGHT_),0,edt_nextcol ,"_arrow");   
                                                     /* PF 2, 3, 4 */
tk_add(PF(2),0,edt_help0,     "keypad");   
tk_add(PF(3),0,edt_fndnxt,    "next");   
tk_add(PF(4),0,edt_delrow,      "delrow");   

				/* Synonyms for PF2 PF3 PF4 */
tk_add(F(2),0,edt_help0,     "keypad");   
tk_add(F(3),0,edt_fndnxt,    "next");   
tk_add(F(4),0,edt_delrow,      "delrow");   

tk_add(PF(1),PF(2),edt_help0, "keypad");   
tk_add(PF(1),PF(3),edt_find,  "find");   
tk_add(PF(1),PF(4),edt_addrow,"addrow");   

tk_add(F(1),F(2),edt_help0, "keypad");   		/* 28-Feb-1991 */
tk_add(F(1),F(3),edt_find,  "find");   
tk_add(F(1),F(4),edt_addrow,"addrow");   
							/* keypad */
tk_add(KP('0'), 0,edt_line,   "line");   
tk_add(KP('1'), 0,edt_tabular,"word");
tk_add(KP('2'), 0,edt_lastcol,"_last_column");
tk_add(KP('3'), 0,edt_chgfmt, "change_format");   
tk_add(KP('4'), 0,edt_adv,    "advance");   
tk_add(KP('5'), 0,edt_backup, "backup");   
tk_add(KP('6'), 0,edt_creacol,"create_column");   
tk_add(KP('7'), 0,edt_newpage,"page");   
tk_add(KP('8'), 0,edt_newsection,"section");   
tk_add(KP('9'), 0,edt_rightpage, "right_page");   
tk_add(KP('-'), 0,edt_delw,      "_delete_word");   
tk_add(KP(','), 0,edt_delc,      "_delete_character");   
tk_add(KP('.'), 0,edt_findrow,   "row");   
tk_add(KP('\r'),0,edt_nextline,  "_next_line");   

tk_add(PF(1),KP('0'), edt_screen,    "screen");   
tk_add(PF(1),KP('1'), edt_showtable, "show");   
tk_add(PF(1),KP('2'), edt_badkey,    "_bad_key");   
tk_add(PF(1),KP('3'), edt_sort,      "sort");   
tk_add(PF(1),KP('4'), edt_bottomtbl, "bottom_of_table");   
tk_add(PF(1),KP('5'), edt_toptbl,    "top_of_table");   
tk_add(PF(1),KP('6'), edt_delcol,    "delete_column");   
tk_add(PF(1),KP('7'), edt_command,   "command");   
tk_add(PF(1),KP('8'), edt_badkey,    "_bad_key");   
tk_add(PF(1),KP('9'), edt_leftpage,  "left_page");   
tk_add(PF(1),KP('-'), edt_undelw,    "_undel_word");   
tk_add(PF(1),KP(','), edt_undelc,    "_undel_character");   
tk_add(PF(1),KP('.'), edt_shstat,    "status");   
tk_add(PF(1),KP('\r'),edt_nextline,  "_next_line");   

			/* Added 28-Feb-1991 */
tk_add(F(1),KP('0'), edt_screen,    "screen");   
tk_add(F(1),KP('1'), edt_showtable, "show");   
tk_add(F(1),KP('2'), edt_badkey,    "_bad_key");   
tk_add(F(1),KP('3'), edt_sort,      "sort");   
tk_add(F(1),KP('4'), edt_bottomtbl, "bottom_of_table");   
tk_add(F(1),KP('5'), edt_toptbl,    "top_of_table");   
tk_add(F(1),KP('6'), edt_delcol,    "delete_column");   
tk_add(F(1),KP('7'), edt_command,   "command");   
tk_add(F(1),KP('8'), edt_badkey,    "_bad_key");   
tk_add(F(1),KP('9'), edt_leftpage,  "left_page");   
tk_add(F(1),KP('-'), edt_undelw,    "_undel_word");   
tk_add(F(1),KP(','), edt_undelc,    "_undel_character");   
tk_add(F(1),KP('.'), edt_shstat,    "status");   
tk_add(F(1),KP('\r'),edt_nextline,  "_next_line");   

                                               /* Function Keys */
tk_add(F(2), 0,edt_help1,     "Function");   

tk_add(F(11),0,edt_newpage,   "page");   
tk_add(F(12),0,edt_fndnxt,    "next");   
tk_add(F(13),0,edt_adv,       "advance");   
tk_add(F(14),0,edt_backup,    "backup");   
tk_add(F(15),0,edt_rightpage, "right_page");   
tk_add(F(16),0,edt_creacol,   "create_column");   
tk_add(F(17),0,edt_tabular,   "word");   
tk_add(F(18),0,edt_chgfmt,    "change_format");   
tk_add(F(19),0,edt_line,      "line");   
tk_add(F(20),0,edt_findrow,   "row");   

tk_add(F(1),F(11),edt_command,   "command");   
tk_add(F(1),F(12),edt_find,      "find");   
tk_add(F(1),F(13),edt_bottomtbl, "bottom_of_table");   
tk_add(F(1),F(14),edt_toptbl,    "top_of_table");   
tk_add(F(1),F(15),edt_leftpage,  "left_page");   
tk_add(F(1),F(16),edt_delcol,    "delete_column");   
tk_add(F(1),F(17),edt_showtable, "show");   
tk_add(F(1),F(18),edt_sort,      "sort");   
tk_add(F(1),F(19),edt_screen,    "screen");   
tk_add(F(1),F(20),edt_shstat,    "status");   

/*
tk_add(F(4), 0,edt_dell,      "_delete_line");   
tk_add(F(6), 0,edt_undell,    "_undel_line");   
tk_add(F(8), 0,edt_nextcol,   "_next_column");   
tk_add(F(9), 0,edt_lastcol,   "_last_column");   
tk_add(F(16),0,edt_newsection,"section");   
tk_add(F(18),0,edt_delw,      "_delete_word");   
tk_add(F(19),0,edt_delc,      "_delete_character");   
tk_add(F(1),F(2),edt_nextline,  "_next_line");   
tk_add(F(1),F(9),edt_undelw,    "_undel_word");   
tk_add(F(1),F(11),edt_undelc,   "_undel_character");   
*/
                                                    /* EXIT or QUIT explicit */
tk_add(0,0,edt_quit,   "quit");   
tk_add(0,1,edt_exit,   "exit");   

return (0);
}
