/* @(#)edtpage.c	19.1 (ES0-DMD) 02/25/03 14:00:49 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        edtpage.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table editor
.PURPOSE
             Displays a new page into the data_window and
             updates the sequence numbers in the sequence_window.
             Table elements are displayed according to the
             control variables edt_row and edt_column.
.RETURNS     error status.
.VERSION     1.0 25-Mar-1988   Implementation     JDP
.VERSION     1.1 25-Nov-1989   Modification cursor doesn't go home
------------------------------------------------------------*/

#include <tbldef.h>
#include <tbledt.h>
#include <tfmdef.h>
#include <tblfmt.h>
#include <proto_tbl.h>

   int edt_page(tid)
   int tid                           /* IN : table ident */;

{  int j, k, status, null;
   int  i, ii,  l, len, mylen, dummy;
   int	previous_active;
   char string[EDT_MAXCHA],dums[EDT_MAXCHA],form[10];
   static char vertical = RuleChar(_VERTICAL_);

/*   for (i=0; i<edt_width; i++) string[i] = ' '; */
   for (i=0; i<EDT_MAXCHA; i++) string[i] = ' ';
   previous_active = DeactiveWindow(editor_window);
   ClearWindow(editor_window);
                                     /* read table info and display */;
   for ( i=0; i<edt_nr; i++)
       { j = edt_row[i];
         mylen = 1;
         for (ii=0; ii<EDT_MAXCHA; ii++) string[ii] = ' ';
         for (l=0; l<edt_nc;l++) {
             TCFGET(edt_tid,edt_column[l],form,&len,&dummy);
             TCERDC(tid,j,edt_column[l],dums,&null);
             if (!null)   strncpy(string+mylen,dums,strlen(dums));
             mylen += len+1;
             }
         string[mylen] = '\0';
         status = CursorTo(data_subwindow,i,0);
         status = Put(data_subwindow, string+1);   /* Skip First col. sep. */
       }                  
                                    /* update sequences */;
    for (i = 0; i<edt_nr; i++)
       { ed_pic(string,"00000009",edt_row[i]);
         status = CursorTo(sequence_subwindow,i,0);
         status = Put(sequence_subwindow, string);
         SetAttr(sequence_subwindow, _GRAPHICS_);
         Write(sequence_subwindow, &vertical, 1);
         SetAttr(sequence_subwindow, _NORMAL_);
       }
    SetAttr(data_subwindow, _GRAPHICS_);
    for (j = 0; j<edt_nc; j++)
       {k = FMT[j].LAST;
       for (i = 0; i<edt_nr; i++)
          { status = CursorTo(data_subwindow,i,k);
            Write(data_subwindow, &vertical, 1);
          }
       }
    SetAttr(data_subwindow, _NORMAL_);
                                       /* set cursor to home */;
    if(previous_active)	ActiveWindow(editor_window);
    status = CursorTo(data_subwindow,cursor_pos[0],cursor_pos[1]);
/*    GetCursor(data_subwindow,cursor_pos);*/
/*    status  = CursorHome(data_subwindow); */

    return (status);
}
