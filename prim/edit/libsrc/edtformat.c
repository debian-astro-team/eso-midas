/* @(#)edtformat.c	19.1 (ES0-DMD) 02/25/03 14:00:48 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)edtformat.c	4.1.1.1 (ESO-IPG) 7/30/92 17:31:00 */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        edtformat.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table editor
.PURPOSE
             Find the column layout to the used for edition.
             The variables updated are edt_nc (number of displayed
             columns) and edt_column (indexes).

             The routine is used when a new page is required.
             There are three modes in the argument:
             FIRSTPAGE, RIGHTPAGE and LEFTPAGE.
             Put label information in the header and display header.

.RETURNS     error status.
.VERSION     1.0 25-Mar-1988   Implementation     JDP
.VERSION     1.1 24-Nov-1989   Modification labels edition, F.O.
------------------------------------------------------------*/

#include <tbldef.h>
#include <tbledt.h>
#include <tfmdef.h>
#include <tblfmt.h>
#include <macrogen.h>		/* For MAX / MIN */
#include <proto_tbl.h>

static int offset = 9;		/* Space used for the Sequence Number */

   int edt_format(mode)

   int  mode                       /* IN : page mode */;

{  int status, len, dtype;
   char form[10], ws[18];
   int  i, j, k, icol, imax, ind;
   static char vertical = RuleChar(_VERTICAL_);
   static char cross    = CornerChar(_CROSS_);
   char		last_sep;

   status = 0;
   

   switch (mode) {
          case RESETPAGE:  icol = edt_column[0];                 break;
          case PREVCOL:    icol = edt_column[0]-1;               break;
          case NEXTCOL:    icol = edt_column[1];                 break;
/*?          case RIGHTPAGE:  icol = edt_column[edt_nc] + 1;        break;*/
          case RIGHTPAGE:  icol = edt_column[edt_nc] ;        break;
          case FIRSTPAGE:
          case LEFTPAGE:   icol = 1;                             break;
          }
   imax = 0; ind = 0;

   if (icol == 0) icol =1;
   while (imax < data_columns && icol <= edt_ncol)
       {status = TCFGET(edt_tid, icol, ws, &len, &dtype);
        FMT[ind].colno = icol;
        FMT[ind].FIRST = imax;
        FMT[ind].LAST  = imax + len ;
        imax           = FMT[ind].LAST + 1;
        strcpy(FMT[ind].FORMAT,ws);
        status = TCLGET(edt_tid, icol, ws);
        i = 0; while(ws[i] != ' ' &&ws[i] != '\0') i++; ws[i] = '\0';
        strcpy(FMT[ind].LABEL,ws);
        edt_column[ind] = icol;
        icol++;
        ind++;
       }
   if (imax >= data_columns) ind = ind - 1;
   edt_nc = ind;
   edt_width = FMT[edt_nc-1].LAST;
   

   ClearWindow(header_window);
   CursorTo(header_window, 0, 0);
   Put(header_window,"Sequence");
                                        /* include labels */
   for (i=0; i<ind; i++)
       {k = FMT[i].FIRST + offset-1;
        CursorTo(header_window,0,k);
        SetAttr(header_window, _GRAPHICS_);
        Write(header_window, &vertical, 1);
        SetAttr(header_window, _NORMAL_);
	len = strlen(FMT[i].LABEL);
	len = MIN(len, FMT[i].LAST - FMT[i].FIRST);
        Write(header_window, FMT[i].LABEL, len);
       }
   k = FMT[ind-1].LAST + offset;
   CursorTo(header_window,0,k);
   SetAttr(header_window, _GRAPHICS_);
   Write(header_window, &vertical, 1);
   SetAttr(header_window, _NORMAL_);


   SetAttr(header_window, _GRAPHICS_);
   CursorTo(header_window, 1, 0);
   FillWindow(header_window, RuleChar(_HORIZONTAL_));
   CursorTo(header_window, 1, offset);
   Write(header_window, &cross, 1);
   
   for (i=0; i<ind; i++)
       {k = FMT[i].FIRST + offset-1;
        len = FMT[i].LAST - FMT[i].FIRST;
        CursorTo(header_window,1,k);
        Write(header_window, &cross, 1);
        Fill(header_window, RuleChar(_HORIZONTAL_),len);
       }
   i = ind - 1;
   if (edt_column[i] == edt_ncol)	/* When at rightmost pos, Use Tee Char */
   	last_sep = TeeChar(_RIGHT_);
   else	last_sep = CornerChar(_CROSS_); 

   k = FMT[i].LAST + offset;
   CursorTo(header_window,1,k);
   Write(header_window, &last_sep, 1);
   SetAttr(header_window, _NORMAL_);
   if (	last_sep == TeeChar(_RIGHT_))
   	ClearRight(header_window);
   else	FillWindow(header_window, '.');


   ActiveWindow(header_window);

   return (status);
}
