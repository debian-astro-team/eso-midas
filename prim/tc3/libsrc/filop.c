/*===========================================================================
  Copyright (C) 1987-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE           Module
.NAME           filop.c
.LANGUAGE       C
.AUTHOR         Francois Ochsenbein [ESO-IPG]
.CATEGORY       File Management
.ENVIRONMENT    VMS / UNIX
.COMMENTS       Here are utilities to create / delete / rename / change
		protection of files.
.VERSION 1.0	05-Mar-1987: Creation 
.VERSION 1.1	23-Jun-1989: Allow file translation

 090831		last modif
----------------*/

#define PM_LEVEL LEVEL_FI  	/* The Monitoring level for this file */

#define	DEBUG		0	/* Debugging option	*/

#include <osfile.h>
#include <midas_def.h>
#include <macrogen.h>
#include <trace.h>
#include <levels.h>
#include <error.h>
#include <ok.h>


extern int pm_enter(), pm_trace(), pm_iexit(), pm_ed_trace();
extern int eh_ed_as();



/*==========================================================================*/
int fi_create(name, nobyt, protec)
/*++++++++
.PURPOSE Creates a NEW file.
         The argument protec gives the file protection code, in UNIX 
         convention: it is made of 3 octal digits `OGW' for Owner / 
	Group / World; each octal digit is made of 3 bits `rwe' 
	for read / write (delete) / execute protection set to 1 to
	 PERMIT access. As an example, 755 allows read / execute
	 to everybody, but rewriting the file is only allowed to the owner.
	The protection code of 0 converted to default protection 
	(defined as PROTECTION in osparms.h)
.RETURNS OK / NOK . 
         Note that trying to create a new file with the same
         name as an existing file is an error.
.REMARKS 
-------------------------*/
	char *name;	/* IN : Filename */
	long  nobyt;	/* IN : number of bytes to be allocated */
	int   protec;  	/* IN : protection code `OGW' (3 octal digits)	*/
{ 
	int	status;
	char	*phname;

  ENTER("fi_create");

  TRACE(name);

  phname = osftr(name);		/* Physical Name */
  status = osfcreate(phname, nobyt, protec);
  if (status < 0)	ERR_ED_STRING(osmsg(), name), status = NOK;
  else			status = OK;
  
  EXIT(status);
}	

/*==========================================================================*/
int fi_chmod(name, protec)
/*++++++++
.PURPOSE Change the protection of a file.
         The argument protec has the same Unix conventions as
         fi_create.
.RETURNS OK / NOK . 
.REMARKS 
-------------------------*/
	char 	*name;	/* IN : Filename */
	int   protec;  	/* IN : protection code `OGW' (3 octal digits)	*/
{ 
	int	status;
	char	*phname;

  ENTER("fi_chmod");

  TRACE(name);

  phname = osftr(name);		/* Physical Name */
  status = osfcontrol(phname, CHMOD, protec, 0);
  if (status < 0)	ERR_ED_STRING(osmsg(), name), status = NOK;
  else			status = OK;
  
  EXIT(status);
}	

/*==========================================================================*/
int fi_delete(name)
/*++++++++
.PURPOSE Delete a file.
.RETURNS OK / NOK . 
.REMARKS 
-------------------------*/
	char 	*name;	/* IN : File to delete */
{ 
	int	status;
	char	*phname;

  ENTER("fi_delete");

  TRACE(name);

  phname = osftr(name);		/* Physical Name */
  status = osfdelete(phname);
  if (status < 0)	ERR_ED_STRING(osmsg(), name), status = NOK;
  else			status = OK;
  
  EXIT(status);
}	

/*==========================================================================*/
int fi_rename(name, newname)
/*++++++++
.PURPOSE Change name of a file.
.RETURNS OK / NOK . 
.REMARKS 
-------------------------*/
	char *name;	/* IN : old filename */
	char *newname;	/* IN : new filename */
{ 
	int	status;

  ENTER("fi_rename");

  TRACE_ED_STRING("Old name: ", name);
  TRACE_ED_STRING("New name: ", name);

  status = osfrename(name, newname);
  if (status < 0)	ERR_ED_STRING(osmsg(), name), status = NOK;
  else			status = OK;
  
  EXIT(status);
}	

/*==========================================================================*/
long fi_size(name)
/*++++++++
.PURPOSE Retrieve size (bytes) of a file.
.RETURNS Size in bytes / 0 if file does not exist or error
.REMARKS 
-------------------------*/
	char *name;	/* IN : file name */
{ 
	long size;
	char	*phname;
	
  ENTER(".fi_size");

  TRACE_ED_STRING("File name: ", name);

  phname = osftr(name);		/* Physical Name */
  size = osfsize(phname);
  if (size == -1)
  	size = 0, ERR_ED_STRING(osmsg(), name), size = 0;
  
  EXITl(size);
}	

/*==========================================================================*/
long fi_date(name)
/*++++++++
.PURPOSE Retrieve last modification date of a file (seconds elapsed since
		January 1, 1970)
.RETURNS Date in seconds, or 0 for error (message logged)
.REMARKS 
-------------------------*/
	char *name;	/* IN : file name */
{ 
	long date;
	char	*phname;
	
  ENTER(".fi_date");

  TRACE_ED_STRING("File name: ", name);

  phname = osftr(name);		/* Physical Name */
  date = osfdate(phname);
  if (date == -1)
  	date = 0, ERR_ED_STRING(osmsg(), name), date = 0;

  EXITl(date);
}	

