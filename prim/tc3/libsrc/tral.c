/*===========================================================================
  Copyright (C) 1988-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++ 
.MODULE    tral.c
.AUTHOR    Francois Ochsenbein [ESO]
.LANGUAGE  C
.CATEGORY  Conversion from character to long integer.

.COMMENTS  

.VERSION   1.0	20-Oct-1988: Creation
.VERSION   1.1	08-Dec-1989: Numbers may be followed by K.
			Also allow characters (e.g. a) and Controls (^X)
			within apostrophes

 090831		last modif
-----------------------------------------------*/
 
#include <midas_def.h>
#include <tra.h>
#include <atype.h>

#define LEN	80

#define issign(c)		((c == '+') || (c == '-'))
#define FINISH			goto FIN
#define GobbleSpaces(s,l)	s += oscspan((unsigned char *)s, l, _SPACE_, main_ascii)

extern int tr_error();


MID_EXTERN struct trerr_s	*trerror;

MID_STATIC	long 	accu;
MID_STATIC  int	base = 10;
MID_STATIC	char	sign;

/*===========================================================================*/
static int Number(str, len)
/*++++++
.PURPOSE Just scan a string until non-valid characters are found.
	The number is stored in accu.
.RETURNS Number of valid characters
.REMARKS Not traced. 
--------*/
	char *str;	/* IN: The string to scan	*/
	int  len;	/* IN: Length of the string	*/
{
	char *p, *pe;
	int  i;
  
  accu = 0;
  if (len <= 0)		return(0);
  
  for (p = str, pe = p + len; (p < pe) && (isxdigit(*p)); p++)
  {	if (isdigit(*p))	i = '0';
  	else if (isupper(*p))	i = 'A' - 10;
	else			i = 'a' - 10;
	i = *p - i;
	if (i >= base)		break;
	accu = accu * base + i;
  }
  i = p - str;		/* Number of Digits */

  return(i);
}	

/*===========================================================================*/
static int Sign(str, len)
/*++++++
.PURPOSE Just scan the beginning of a string: skip leading blanks,
	and get the sign.
.RETURNS Number of valid characters
.REMARKS Not traced. 
--------*/
	char *str;	/* IN: The string to scan	*/
	int  len;	/* IN: Length of the string	*/
{
	char *p, *pe;
  
  sign = 0;
  if (len <= 0)		return(0);

  p = str, pe = p + len;
  GobbleSpaces(p, len);
  
  if (p < pe)
  {	switch(*p)
  	{ case '-':	sign = 1;
  	  case '+':	p++; 	break;
  	}
	GobbleSpaces(p, pe-p);		/* Skip blanks */
  }

  return(p-str);
}	

/*===========================================================================*/
int tr_al(str, len, value)
/*++++++
.PURPOSE Converts a string to a long number. 
	Input is assumed to be in decimal, unless the string starts
	with 'X' (hexadecimal) or 'O' (octal).
	The digits may be followed by K (implies a 1024 factor)
.RETURNS Number of digits (excluding spaces) scanned, -1 for 
	error (message logged)
.REMARKS Return 0 if string contains only spaces or a sign. 
	Value is then set to zero.
--------*/
	char	*str;	/* IN: String to scan	*/
	int	len;	/* IN: Length of str	*/
	long	*value;	/* OUT: result		*/
{
	char 	*p, *pe;
	int	nd;

  trerror->errno = 0;

  base = 10;
  nd = 1;
  accu = 0;
    
  p = str + Sign(str, len);	/* Skip trailer + sign	*/
  pe = str + len;
  
  if (*p == '\'')		/* Get a Character */
  {	p++;
	if (*p != '\'')	accu = *(p++);
	if (*p == '\'')	p++;
	goto check_remaining_bytes;
  }
  if (*p == '^')		/* Get a Control Character */
  {	p++;
  	accu = tocntrl(*p), p++;
	goto check_remaining_bytes;
  }

  nd = Number(p, pe-p);		/* Get number		*/
  p += nd;			/* End of number	*/

	/* If the string is not completely understood, look if
	   it can be due to O(octal), X(hexadecimal), k (k-factor)
    	   Note that X and O can only be at the beginning, i.e.
    	   when accu == 0; k can only be a suffix.
	 */

  if (p < pe)	switch(toupper(*p))
  { case 'O':	base = 8;
  	if (accu == 0)	goto special_base;
  	break;
    case 'X':	base = 16;
  	if (accu == 0)	goto special_base;
	break;
    special_base:
	p++;
	nd = Number(p, pe-p);
	p += nd;
	break;
    case 'K':	
    	accu *= 1024;
    	p++;
	break;
  }

	/* Take Sign into account, and check remaining bytes are only
	   white spaces
	 */

  check_remaining_bytes:
  *value = (sign ? -accu : accu);

  GobbleSpaces(p, pe-p);
  
  if (p < pe)		/* Bad String	*/
  {	switch(base)
  	{ case 8:	trerror->errno = TRERR_OCTAL;	break;
  	  case 10:	trerror->errno = TRERR_DIGIT;	break;
  	  case 16:	trerror->errno = TRERR_HEXA;	break;
  	}
	trerror->str = str;
	trerror->len = len;
	trerror->offset = (p-str);
	nd = -1;
	tr_error();
  }

  return(nd);
}
  
/*===========================================================================*/
int tr_al1(str, len, value, n)
/*++++++
.PURPOSE Converts a string to a vector of numbers.
	Numbers may be separated by blanks, commas,  or any punctuation character,
	and may start with 'X' (hexadecimal) or 'O' (octal).
.RETURNS Number of found numbers (<= n) / -1 for
	error (message logged)
.REMARKS Return 0 if string contains only spaces.
--------*/
	char	*str;	/* IN: String to scan	*/
	int	len;	/* IN: Length of str	*/
	long	*value;	/* OUT: array result	*/
	int	n;	/* IN: Number of values	*/
{
	char 	*p, *pe, *q;
	int	i, nd;

  trerror->errno = 0;

  p = str, pe = p + len;
  
  for (i=0; (p<pe) && (i < n); i++)
  {	GobbleSpaces(p, pe-p), q = p;
  	if (p == pe)	break;
	if (issign(*q))	q++;
  	q += oscscan((unsigned char *)q, pe-q, _PUNCT_|_SPACE_, main_ascii);
  	nd = tr_al(p, q-p, value+i);
  	if (nd < 0)	FINISH;		/* Error Encountered	*/
  	if (nd == 0)			/* No number		*/
  		{ trerror->errno = TRERR_VOID; break; }
	p = q;
	if (p == pe)	continue;
	if (issign(*p))	continue;
	if (ispunct(*p))	p++;
  }

	/* Check string exhausted */

  if (trerror->errno == 0)	
  {	GobbleSpaces(p, pe-p);
  	if (p < pe)	trerror->errno = TRERR_TOOMANY;
  }
  if (trerror->errno)
  {	trerror->str = str, trerror->len = len, 
  	trerror->offset = (p - str);
  	tr_error();
  }
  
  FIN:
  return((trerror->errno ? -1 : i));
}

/*===========================================================================*/
int tr_aln(str, len, value, n)
/*++++++
.PURPOSE Converts a string to a vector of numbers.
	Numbers may be separated by blanks or any punctuation character,
	and may start with 'X' (hexadecimal) or 'O' (octal).
	There must be exactly the required number of values.
.RETURNS  n for success / -1 for failure
.REMARKS Return 0 if string contains only spaces or a sign. 
	Value is then set to zero.
--------*/
	char	*str;	/* IN: String to scan	*/
	int	len;	/* IN: Length of str	*/
	long	*value;	/* OUT: array result	*/
	int	n;	/* IN: Number of values	*/
{
	int	i;

  i = tr_al1(str, len, value, n);

  if ((i >= 0) && (i < n))	/* Too few numbers */
  {	trerror->errno = TRERR_TOOFEW;
	trerror->str = str, trerror->len = len, 
  	trerror->offset = len;
  	tr_error();
  }
  
  return((trerror->errno ? -1 : i));
}
