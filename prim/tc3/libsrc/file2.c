/*===========================================================================
  Copyright (C) 1988-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE           Module
.NAME           file2.c
.LANGUAGE       C
.AUTHOR         Francois Ochsenbein [ESO-IPG]
.CATEGORY       Further Interface to disk file access
.ENVIRONMENT    
.COMMENTS       
.VERSION 1.0	20-Jun-1988: Creation

 090831		last modif
-------------------------------------------------------------*/

#define	DEBUG		0	/* Debugging option	*/

#define  PM_LEVEL	LEVEL_FI
#define  PASCAL_DEF	0	/* Don't include pascalisation ... */
#include <stesodef.h>
#include <midas_def.h>


extern int pm_enter(), pm_iexit(), fi_write();



#define FINISH		goto FIN

/*======================================================================*/
int fi_wu (file, record, nbytes)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Write underlined text in file
.RETURNS Number of bytes actually written, or NOK for error
.REMARKS 
-------------------------------------------------------------*/
        int     file;                   /* IN: File number          */
        char    *record;                /* IN: record buffer        */
        int     nbytes;                 /* IN: size of buffer       */
{
	MID_STATIC char *buf = NULL_PTR(char);
	int	status;
	
  ENTER("+fi_wu");	

  status = nbytes * 2;
  if (status == 0)	FINISH;
  
  buf = MEM_EXP(char, buf, status);
  
  oscfill(buf, nbytes, '_');
  oscfill(buf+nbytes, nbytes, '\b');	/* Backspaces */
  fi_write(file, buf, status);
  
  status = fi_write(file,record,nbytes);

  FIN:
  EXIT (status);
}

