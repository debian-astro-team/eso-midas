/*===========================================================================
  Copyright (C) 1988-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++
.MODULE		atex.c
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.CATEGORY  	TeX character classification
.ENVIRONMENT	ASCII
.COMMENTS     	This file includes the definition of TeX attributes.
		Characters flagged _TeX_ESCAPE are
		\ # & { } % ~ $ ^ _
.VERSION 1.0	19-Apr-1988: Creation
.VERSION 1.1	19-Mar-1989: Added ed_tex function
.VERSION 1.2	27-Oct-1990: Added ATEX_C conditional. 
		Added atex_ entry point to force inclusion of this module
		for this *&^$%CRAZY*&$% gcc compiler on VAX

 090831		last modif
-------------------------------------------*/
 
#define ATEX_C	1	/* To avoid insertion of MID_EXTERN main_TeX	*/
#include <atex.h>
#include <string.h>

#define ___	0x00
#define _U_	_TeX_UPPER_		/* UPPER-case letter A...Z	*/
#define _L_	_TeX_LOWER_		/* lower-case letter a...z	*/
#define _D_	_TeX_DIGIT_		/* digit             0...9	*/
#define _S_	_TeX_SPACE_		/* space			*/
#define _P_	_TeX_PUNCT_		/* punctuation			*/
#define _M_	_TeX_MATH_		/* Math env.			*/
#define _Z_	_TeX_NULL_		/* Null character		*/
#define _E_	_TeX_ESCAPE_		/* \ # & { } % ~		*/

		/* Attributes of TeX characters	*/
MID_GLOBAL unsigned char	main_TeX[256] = /* TeX attributes	*/
  {  /*  UPP|low|dig|sp |pun|ctr|ign|escape			
      *     |   |   |   |   |   |   |   		*/    
	 ___|___|___|___|___|___|_Z_|___,	/* NUL	*/
	 ___|___|___|___|___|___|___|___,	/* SOH	*/
	 ___|___|___|___|___|___|___|___,	/* STX	*/
	 ___|___|___|___|___|___|___|___,	/* ETX	*/
	 ___|___|___|___|___|___|___|___,	/* EOT	*/
	 ___|___|___|___|___|___|___|___,	/* ENQ	*/
	 ___|___|___|___|___|___|___|___,	/* ACK	*/
	 ___|___|___|___|___|___|___|___,	/* BEL	*/
	 ___|___|___|___|___|___|___|___,	/* BS	*/
	 ___|___|___|_S_|___|___|___|___,	/* HTab	*/
	 ___|___|___|_S_|___|___|___|___,	/* LF	*/
	 ___|___|___|_S_|___|___|___|___,	/* VTab	*/
	 ___|___|___|_S_|___|___|___|___,	/* FF	*/
	 ___|___|___|_S_|___|___|___|___,	/* CR	*/
	 ___|___|___|___|___|___|___|___,	/* SO	*/
	 ___|___|___|___|___|___|___|___,	/* SI	*/
	 ___|___|___|___|___|___|___|___,	/* DLE	*/
	 ___|___|___|___|___|___|___|___,	/* DC1	*/
	 ___|___|___|___|___|___|___|___,	/* DC2	*/
	 ___|___|___|___|___|___|___|___,	/* DC3	*/
	 ___|___|___|___|___|___|___|___,	/* DC4	*/
	 ___|___|___|___|___|___|___|___,	/* NAK	*/
	 ___|___|___|___|___|___|___|___,	/* SYN	*/
	 ___|___|___|___|___|___|___|___,	/* ETB	*/
	 ___|___|___|___|___|___|___|___,	/* CAN	*/
	 ___|___|___|___|___|___|___|___,	/* EM	*/
	 ___|___|___|___|___|___|___|___,	/* SUB	*/
	 ___|___|___|___|___|___|___|___,	/* ESC	*/
	 ___|___|___|___|___|___|___|___,	/* FS	*/
	 ___|___|___|___|___|___|___|___,	/* GS	*/
	 ___|___|___|___|___|___|___|___,	/* RS	*/
	 ___|___|___|___|___|___|___|___,	/* US	*/
	 ___|___|___|_S_|___|___|___|___,	/* sp	*/
	 ___|___|___|___|_P_|___|___|___,	/* !	*/
	 ___|___|___|___|_P_|___|___|___,	/* "	*/
	 ___|___|___|___|___|___|___|_E_,	/* #	*/
	 ___|___|___|___|___|___|___|_E_,	/* $	*/
	 ___|___|___|___|___|___|___|_E_,	/* %	*/
	 ___|___|___|___|___|___|___|_E_,	/* &	*/
	 ___|___|___|___|_P_|___|___|___,	/* '	*/
	 ___|___|___|___|_P_|___|___|___,	/* (	*/
	 ___|___|___|___|_P_|___|___|___,	/* )	*/
	 ___|___|___|___|_P_|_M_|___|___,	/* *	*/
	 ___|___|___|___|_P_|___|___|___,	/* +	*/
	 ___|___|___|___|_P_|___|___|___,	/* ,	*/
	 ___|___|___|___|_P_|___|___|___,	/* -	*/
	 ___|___|___|___|_P_|___|___|___,	/* .	*/
	 ___|___|___|___|_P_|___|___|___,	/* /	*/
	 ___|___|_D_|___|___|___|___|___,	/* 0	*/
	 ___|___|_D_|___|___|___|___|___,	/* 1	*/
	 ___|___|_D_|___|___|___|___|___,	/* 2	*/
	 ___|___|_D_|___|___|___|___|___,	/* 3	*/
	 ___|___|_D_|___|___|___|___|___,	/* 4	*/
	 ___|___|_D_|___|___|___|___|___,	/* 5	*/
	 ___|___|_D_|___|___|___|___|___,	/* 6	*/
	 ___|___|_D_|___|___|___|___|___,	/* 7	*/
	 ___|___|_D_|___|___|___|___|___,	/* 8	*/
	 ___|___|_D_|___|___|___|___|___,	/* 9	*/
	 ___|___|___|___|_P_|___|___|___,	/* :	*/
	 ___|___|___|___|_P_|___|___|___,	/* ;	*/
	 ___|___|___|___|_P_|_M_|___|___,	/* <	*/
	 ___|___|___|___|_P_|___|___|___,	/* =	*/
	 ___|___|___|___|_P_|_M_|___|___,	/* >	*/
	 ___|___|___|___|_P_|___|___|___,	/* ?	*/
	 ___|___|___|___|_P_|___|___|___,	/* @	*/
	 _U_|___|___|___|___|___|___|___,	/* A	*/
	 _U_|___|___|___|___|___|___|___,	/* B	*/
	 _U_|___|___|___|___|___|___|___,	/* C	*/
	 _U_|___|___|___|___|___|___|___,	/* D	*/
	 _U_|___|___|___|___|___|___|___,	/* E	*/
	 _U_|___|___|___|___|___|___|___,	/* F	*/
	 _U_|___|___|___|___|___|___|___,	/* G	*/
	 _U_|___|___|___|___|___|___|___,	/* H	*/
	 _U_|___|___|___|___|___|___|___,	/* I	*/
	 _U_|___|___|___|___|___|___|___,	/* J	*/
	 _U_|___|___|___|___|___|___|___,	/* K	*/
	 _U_|___|___|___|___|___|___|___,	/* L	*/
	 _U_|___|___|___|___|___|___|___,	/* M	*/
	 _U_|___|___|___|___|___|___|___,	/* N	*/
	 _U_|___|___|___|___|___|___|___,	/* O	*/
	 _U_|___|___|___|___|___|___|___,	/* P	*/
	 _U_|___|___|___|___|___|___|___,	/* Q	*/
	 _U_|___|___|___|___|___|___|___,	/* R	*/
	 _U_|___|___|___|___|___|___|___,	/* S	*/
	 _U_|___|___|___|___|___|___|___,	/* T	*/
	 _U_|___|___|___|___|___|___|___,	/* U	*/
	 _U_|___|___|___|___|___|___|___,	/* V	*/
	 _U_|___|___|___|___|___|___|___,	/* W	*/
	 _U_|___|___|___|___|___|___|___,	/* X	*/
	 _U_|___|___|___|___|___|___|___,	/* Y	*/
	 _U_|___|___|___|___|___|___|___,	/* Z	*/
	 ___|___|___|___|_P_|___|___|___,	/* [	*/
	 ___|___|___|___|___|___|___|_E_,	/* \	*/
	 ___|___|___|___|_P_|___|___|___,	/* ]	*/
	 ___|___|___|___|___|___|___|_E_,	/* ^	*/
	 ___|___|___|___|___|___|___|_E_,	/* _	*/
	 ___|___|___|___|_P_|___|___|___,	/* `	*/
	 ___|_L_|___|___|___|___|___|___,	/* a	*/
	 ___|_L_|___|___|___|___|___|___,	/* b	*/
	 ___|_L_|___|___|___|___|___|___,	/* c	*/
	 ___|_L_|___|___|___|___|___|___,	/* d	*/
	 ___|_L_|___|___|___|___|___|___,	/* e	*/
	 ___|_L_|___|___|___|___|___|___,	/* f	*/
	 ___|_L_|___|___|___|___|___|___,	/* g	*/
	 ___|_L_|___|___|___|___|___|___,	/* h	*/
	 ___|_L_|___|___|___|___|___|___,	/* i	*/
	 ___|_L_|___|___|___|___|___|___,	/* j	*/
	 ___|_L_|___|___|___|___|___|___,	/* k	*/
	 ___|_L_|___|___|___|___|___|___,	/* l	*/
	 ___|_L_|___|___|___|___|___|___,	/* m	*/
	 ___|_L_|___|___|___|___|___|___,	/* n	*/
	 ___|_L_|___|___|___|___|___|___,	/* o	*/
	 ___|_L_|___|___|___|___|___|___,	/* p	*/
	 ___|_L_|___|___|___|___|___|___,	/* q	*/
	 ___|_L_|___|___|___|___|___|___,	/* r	*/
	 ___|_L_|___|___|___|___|___|___,	/* s	*/
	 ___|_L_|___|___|___|___|___|___,	/* t	*/
	 ___|_L_|___|___|___|___|___|___,	/* u	*/
	 ___|_L_|___|___|___|___|___|___,	/* v	*/
	 ___|_L_|___|___|___|___|___|___,	/* w	*/
	 ___|_L_|___|___|___|___|___|___,	/* x	*/
	 ___|_L_|___|___|___|___|___|___,	/* y	*/
	 ___|_L_|___|___|___|___|___|___,	/* z	*/
	 ___|___|___|___|___|___|___|_E_,	/* {	*/
	 ___|___|___|___|_P_|_M_|___|___,	/* |	*/
	 ___|___|___|___|___|___|___|_E_,	/* }	*/
	 ___|___|___|___|___|___|___|_E_,	/* ~	*/
	 ___|___|___|___|___|___|___|___,	/* DEL	*/

         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	/* 0x8.	*/
         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	/* 0x9.	*/
         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	/* 0xA.	*/
         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	/* 0xB. */
         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	/* 0xC. */
         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	/* 0xD. */
         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	/* 0xE. */
         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0	/* 0xF. */
  };

/*===========================================================================*/
static char *sub(c)
/*+++++++++++++++++++++++
.PURPOSE Check special TeX char, and provides substitution string
.RETURNS Address of special string (NULL)
.REMARKS 
------------------------*/
	char 	c;			/* IN: The character		*/
{
	static	char qesc[] =   "\\q";
	static	char qmath[] =  "$q$";
	char	*q;	/* The returned pointer */

  if (isTeX(c, _TeX_ESCAPE_))
  {	if (c == '\\')		q = "\\b{}";
	else if (c == '^')	q = "\\^{ }";
	else if (c == '~')	q = "\\~{ }";
	else			qesc[1] = c, q = qesc;
  }
  else if (isTeX(c, _TeX_MATH_))
	qmath[1] = c, 	q = qmath;
  else if (c == '\n')	q = "\\\\";
  else			q = (char *)0;
  
  return(q);
}

/*===========================================================================*/
int ed_tex (dest, size, source)
/*+++++++++++++++++++++++
.PURPOSE Perform transformations of special TeX characters: 
	add the `\' escape in front of characters { } $ & # % _ ,
	replace EOL by \\, 
	and replace \ by `\b '
	assuming non-overlapping strings.
.RETURNS Length of new string (== 0 if too small)
.REMARKS Result is correct if strings overlap. 
	Destination is "%" if dest too small.
------------------------*/
	char 	*dest;			/* OUT: destination string	*/
	int 	size;			/* IN: size of dest		*/
	char 	*source;		/* IN: source string		*/
{
	char 	*p1, *p2;
  	char 	*p1e, *p2e, *q;
	int	i;
	char	*sub();

  p1 = source;

	/* Compute number of required characters */

  for (i = 0, p1e = p1; *p1e; p1e++)
  {
  	if (isTeX(*p1e, _TeX_ALNUM_))	continue;
	if ((p2 = sub(*p1e)))	i += strlen(p2) - 1;
  }

  p2  = dest;
  p2e = p2 + (p1e-p1) + i;

	/* Check if destination is large enough	*/

  if (p2e >= (p2 + size))		/* Too long */
  {
	*(p2++) = '%', *p2 = '\0';
	return(0);
  }

  if      ((p2 == p1) && (i == 0))	;			/* No Modif */
  else if ((p2 >= p1) && (p2 <= p1e))	for (p2=p2e; p1e>=p1; p1e--)
  {								/* Overlap */
  	if (isTeX(*p1e, _TeX_ALNUM_))	q = (char *)0;
	else				q = sub(*p1e);
	if (q)	for (i=strlen(q), q+=i; --i >= 0; )	*(p2--) = *(--q);
	else	*(p2--) = *p1e;
  }
  else					for (; p1 <= p1e; p1++)
  {						/* Normal Left to Right */
  	if (isTeX(*p1, _TeX_ALNUM_))	q = (char *)0;
	else				q = sub(*p1);
	if (q)	while (*q)	*(p2++) = *(q++);
	else			*(p2++) = *p1;
  }

  return(p2e - dest);
}

unsigned char *atex_()
/*+++++++++++++++++++++++
.PURPOSE Force the inclusion of this module. This is to get around a problem
	that occurs with the Gnu compiler on VAX/VMS (bad attributes of 
	external variables)
.RETURNS The address of the table characteristics
------------------------*/
{
  return(main_TeX);
}
