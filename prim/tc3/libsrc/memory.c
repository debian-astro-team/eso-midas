/*===========================================================================
  Copyright (C) 1986-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Module
.IDENTIFICATION	MM_MEMORY
.AUTHOR    	Marc Wenger [CDS], Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Memory Management
.ENVIRONMENT 	
.COMMENTS
	These routines allow the management of memory to allocate
	pieces of memory, e.g. for temporary copy of 
	strings. It's especially useful for string communication
	between FORTRAN and C.
\begin{TeX}
\\
	Note that the allocated memory is never freed --- but may be
	reused. The memory is decomposed into ``areas'', in principle multiple
	of the page size (defined as MEM\_AMOUNT), and each area
	may contain several ``zones''. Areas and zones are preceded
	by a `memory header' made of :
\begin{itemize}
\item	the length of the area / zone
\item	chain pointer (to next for free zones, to preceding for occupied
	zones)
\end{itemize}

The complete memory structure is described via a ``memory packet'' 
internal to this module.
\end{TeX}

.VERSION 1.0	28-Oct-1986
.VERSION 2.0	04-Mar-1988: Use OS routines
.VERSION 2.1	22-Jun-1989: Added mm_save mm_ssave

 090831		last modif
----------------------------------------------------------------------------*/

#define TRACING		0	/* No tracing, since these routines may
				 * be used by Program Monitoring Routines ... */

#include <midas_def.h>
#include <error.h>
#include <string.h>


extern int eh_put1();


/*==========================================================================*/
char	*mm_alloc(len)
/*+++
.PURPOSE Allocate memory
.RETURNS Address of usable zone, or NULL (failed)
.REMARKS Error traced 
---*/
	int  len ;	/* IN: Required length (bytes)	*/
{
	register char *p;
	
  p = osmmget(len);
  if (!p)	ERROR(osmsg());

  return(p);
}

/*==========================================================================*/
int mm_free(a)
/*+++
.PURPOSE Allocate memory
.RETURNS OK / NOK
.REMARKS Error traced 
---*/
	char	*a;	/* IN: Address to free	*/
{
osmmfree((void *) a);
return(1);			/* that's O.K. in FO parlance... */
}

/*==========================================================================*/
char	*mm_expand(addr, len) 
/*+++
.PURPOSE Reallocates a piece of memory allocated via mm_alloc
.RETURNS Address of new zone, or NULL if failed.
.REMARKS Values are copied to new zone. Identical to mm_alloc if addr 
	is NULL.
---*/
	char *addr ; /* MOD: adress to realloc	*/
	int len;	  /* IN: New length		*/
{
	register char *p;

  p = osmmexp(addr, len);
  if (!p)	ERROR(osmsg());

  return(p);
}

/*==========================================================================*/
char *mm_save(addr, len)
/*+++
.PURPOSE Save a structure into a new piece of memory.
.RETURNS Address of copied staff.
.REMARKS Error traced 
---*/
	char	*addr;	/* IN: addr of start of copy	*/
	int  	len ;	/* IN: Length (bytes)		*/
{
	register char *p;
	
  if ((p = osmmget(len)))		oscopy(p, addr, len);
  else				ERROR(osmsg());

  return(p);
}

/*==========================================================================*/
char *mm_ssave(addr)
/*+++
.PURPOSE Save a string into a new piece of memory.
.RETURNS Address of copied staff.
.REMARKS Error traced 
---*/
	char	*addr;	/* IN: addr of string to copy	*/
{
	register char *p;
	int	l;
	
  l = (addr ? strlen(addr) : 0);
  
  if ((p = osmmget(l+1)))		*(p + oscopy(p, addr, l)) = '\0';
  else				ERROR(osmsg());

  return(p);
}

