$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.PRIM.TC3.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:57 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libtc3 traf.obj,tral.obj,trpic.obj,trfpic.obj,trerror.obj,error.obj,trace.obj,edpic.obj,edtab.obj,edtime.obj,trtime.obj
$ LIB/REPLACE libtc3 atex.obj,buffer.obj,file.obj,file2.obj,filop.obj,hash.obj,memory.obj,str1.obj,str2.obj,tex.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
