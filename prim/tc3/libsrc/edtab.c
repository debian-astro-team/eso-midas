/*===========================================================================
  Copyright (C) 1985-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++  
.MODULE    edtab.c
.AUTHOR    Francois Ochsenbein [ESO]
.LANGUAGE  C
.CATEGORY  String transformations

.COMMENTS
Replacement of tabs by the correct amount of spaces.

.VERSION 1.0 	19-Dec-1985: Creation 
.VERSION 2.0 	05-Jul-1988: All functions return int.

 090831		last modif
--------------------------------------------*/

#define	PM_LEVEL	LEVEL_STR

#include <midas_def.h>
#include <te.h>
#include <str.h>


extern int pm_enter(), eh_ed_str2(), pm_iexit();



/*===========================================================================
 *                   ed_tab
 *===========================================================================*/
int ed_tab (dest, size, source, ls, tab_len)
/*++++++
.PURPOSE Perform transformations of tabs by the correct amount of blanks
.RETURNS Length of transformed text. The NUL byte is appended if there is space.
.REMARKS An error is passed to the Error Handler if the destination buffer is
	too small.
	Strange results if strings overlap !!!
----------*/
	char 	*dest;		/* OUT: destination string	*/
	int 	size;		/* IN: size of destination string */
	char 	*source;	/* IN: source string		*/
	int 	ls;		/* IN: length of source string	*/
	int 	tab_len;	/* IN: Column adjustment (8 is zero)	*/
{
	char 	*p1, *p2, *p1e, *p2e, truncated;
	int	len, i;

  ENTER("+ed_tab");

  p1e = source + ls; 
  p2e = dest + size;
  truncated = 0;
  if (tab_len <= 0)	tab_len = 8;
  					/* Perform transformations	*/
  for (p1=source, p2=dest; (p1<p1e) && (truncated == 0) ; )
  {   	if (p2 >= p2e)			{ truncated = 1; continue; }
	if (*p1 == '\t')
	{	i = tab_len - ((p2 - dest)%tab_len);
		if ((p2+i) >= p2e)	{ truncated = 1; continue; }
		p2 += oscfill(p2, i, ' ');
		p1++;
		continue;
	}
  	len = MIN(p2e-p2, p1e-p1);
	i = oscopuc(p2, p1, len, '\t');
	p1 += i, p2 += i;
  }

  if (truncated) ERR_ED_STR2("Truncated: ",dest, size);
  else	if (p2 < p2e)	*p2 = EOS;

  EXIT(p2 - dest);       
}

