$ ! @(#)midasjob.com	19.1 (ESO-IPG) 02/25/03 13:58:41 
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! example command procedure MIDASJOB.COM	K. Banse	910322
$ ! to execute MIDAS commands inside a VAX/VMS batch job
$ ! 
$ ! execute via: $ SUBMIT MIDASJOB
$ !
$ ! this is an example of a MIDAS session with DCL commands in the MIDAS
$ ! command stream
$ !
$ ! take this as a template and edit it according to your real configuration
$ ! and your own MIDAS commands
$ !
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ASSIGN _NL: SYS$PRINT			!inhibit printout...
$ !
$ ! only if you want to work in parallel mode, change the default
$ MIDOPTION :== PARALLEL
$ !
$ ! only if you want to change the default MIDAS startup directory
$ ASSIGN DKA200:[KLAUS.MYWORK] MID_WORK
$ !
$ INMIDAS 				!INMIDAS
ZK					!MIDAS unit 
$ !
$ ! we are in SYS$LOGIN, so we have to switch to the data directory
$ ! if we have our data in DBA2:[DATA], we must do:
$ SET DEF DBA2:[DATA]
$ !
$ GOMIDAS				!GOMIDAS
$ DECK					!so VMS knows, that all following input
READ/KEY MODE                           !is for MIDAS
READ/KEY MID$SESS			!any MIDAS command
WRITE/KEY INPUTC YUPPIE
READ/KEY INPUTC
$ DIR           			!use DCL command 
READ/DESCR ITEST NAXIS
BYE
$ EOD					!end of deck
$ !					!the MIDAS logfile MID_WORK:FORGZK.LOG
$ !					!contains all the output from MIDAS
$ !					!also a file MIDASJOB.LOG will be
$ EXIT					!created in your login directory...
