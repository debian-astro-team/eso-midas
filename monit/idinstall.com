$ ! @(#)idinstall.com	19.1 (ESO-IPG) 02/25/03 13:58:40
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! file IDINSTALL.COM to install the module IDISERV.EXE as an
$ ! image. This task should be executed by an account with the system
$ ! privilege CMKRNL. Ask your system manager for help!.
$ ! K. Banse    910218
$ ! M. Peron    931124
$ ! ESO - Garching
$ ! J-P De Cuyper 960724
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! MIDVERS and MIDASHOME have to be modified according
$ ! to current MIDAS release...
$ !
$ MIDVERS :== 95NOV
$ MIDASHOME :== MIDAS
$ !
$ ASSIGN MID_DISK:['MIDASHOME'.'MIDVERS'.SYSTEM.EXEC]        SYS_EXE
$ INSTALL
DELETE SYS_EXE:IDISERV.EXE
ADD/SHARE/HEADER_RESIDENT    SYS_EXE:IDISERV.EXE/PRIV=(SYSNAM,PRMMBX)
list   SYS_EXE:IDISERV.EXE
EXIT
$ EXIT
