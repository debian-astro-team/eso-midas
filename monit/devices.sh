#!/bin/sh
# -----------------------------------------------------------------------
#
# Bourne shell procedure  devices.sh
# define logical devices names (Environment) for MIDAS on UNIX system
# 901203 CG. New directory structure.
# 910920 KB. Change PRINTER to LPRINT, add PLASER
#
# 110516	last modif
# 
# -----------------------------------------------------------------------
#
# Tape devices:
# Syntax: <MIDAS_name>=<host>:<device_name> ;export <MIDAS_name> # Comment
#
# Automatic generation of "./prim/help/tapede.alq" for the MIDAS command:
#
#   Midas> HELP [TapeDevices]
#
# if comments follows the convention: 	# :tape:model:capacity/density:location
#
# Example. The entry:
#   tape0:ns0:/dev/nrst17 ;export tape0 # :1/2 inch:6250 dpi:Tape-room
#
# will generate an entry in the "tapede.alq" file as follow: 
#
#   01234567890123456789012345678901234567890123456789012345678901234567890
#   Name      Host      Device         Tape      Cap/Den   Location
#   tape0     ns0       /dev/nrst17    1/2 inch  6250 bpi  Tape-room
#
# If your output does not fit the format you are welcome to modify the script
# "./prim/help/gentapedev.sh"
#
# To generate your "tapede.alq" type in the directory ./prim/help/:
#   % make tapede.alq
#
#tape1=ws1:/dev/nrst12  ;export tape1  #:1/2 in.:6250 bpi:User room 2nd floor
#tape1m=ws1:/dev/nrst12 ;export tape1m #:1/2 in.:1600 bpi:User room 2nd floor
#tape1h=ws1:/dev/nrst20 ;export tape1h #:1/2 in.:6250 bpi:User room 2nd floor
#
#tapedat0=ip1:/dev/nrst1 ;export tapedat0 #:DAT/DDS:1.2Gb:IPG room 4th floor
#
#tape8mm0=ip1:/dev/nrst2 ;export tape8mm0 #:8mm:2.2 Gb:IPG room 4th floor 
#tape8mm1=ws8:/dev/nrst2 ;export tape8mm3 #:8mm:2.2 Gb:SkyLight room 5th floor
#
#tapect1=ws0:/dev/nrst8 ;export tapect1	 #:QIC-24:60 Mb:User room 2nd floor
#tapect2=ws5:/dev/nrst0 ;export tapect2   #:QIC-24:60 Mb:User room 2nd floor
#
# Printer devices:
# Syntax: <MIDAS_name>=<printer_name> ; export <MIDAS_name>	# comment
#
LPRINT=lp2usr1   ;export LPRINT     # Line printer, 2nd floor, user room
LASER=ps2usr0    ;export LASER      # Postcript B/W, 2nd floor, user room
COLOUR=pc2usr0.c   ;export COLOUR   # Postcript Color, 2nd floor, user room
SLIDE=sl2usr0    ;export SLIDE      # Chromascript, 2nd floor, user room
PENPLOT=hp2usr0  ;export PENPLOT    # HP plotter, 2nd floor, user room
