$ ! @(#)idistart.com	4.1.1.1 (ESO-IPG) 7/30/92 19:47:46 
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! file IDISTART.COM to startup the idiserver for DECWindows
$ ! K. Banse	910218
$ ! ESO - Garching
$ !
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! MIDVERS and MIDASHOME have to be modified according 
$ ! to current MIDAS release...
$ !
$ MIDVERS :== 95NOV
$ MIDASHOME :== MIDAS
$ !
$ @ MID_DISK:['MIDASHOME'.'MIDVERS'.MONIT]MIDLOGS
$ !
$ DAZUNIT :== 'P1'
$ I_TYPE :== "SXW  "
$ ASSIGN idi.sxw0g AGL3DEV
$ !
$ RUN MID_DISK:['MIDASHOME'.'MIDVERS'.SYSTEM.EXEC]idiserv.exe
$ !
$ EXIT
