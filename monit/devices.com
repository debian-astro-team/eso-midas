$ ! @(#)devices.com	19.1 (ESO-IPG) 02/25/03 13:58:39
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command file DEVICES.COM for ESOMC0 
$ !     K. Banse            ESO - Garching        910709
$ ! procedure to set up the device procedures or names for Midas
$ !
$ !---------------------------------------------------------
$ !
$ ! procedures for printing:
$ !
$ LPRINT  :== "PRINT/QUEUE=SYS$PRINT"
$ PLASER  :== "@MISC$DISK:[POSTSCRIPT]PS2USR0.COM _"
$ ! 
$ VERSATEC:== "$SYS_EXE:VERSATEC"
$ SENDV80 :== "$SYS_EXE:SENDV80"
$ !
$ ! real devices:
$ !
$ LASER     :== PS2USR0
$ VERSA     :== LVA0 
$ USER_TERM :== 'TERMINAL'
$ !
$ ASSIGN MKB500      TAPE0
$ !
$ EXIT
