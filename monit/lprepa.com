$ ! @(#)lprepa.com	19.1 (ESO-IPG) 02/25/03 13:58:41 
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command procedure LPREPA		890116
$ ! K. Banse
$ !
$ ! link all modules of the C-MIDAS interactive monitor PREPA
$ !
$ ! the options are: @ LPREPA D - link with debugger
$ !
$ !------------------------------------------------------------------
$ !
$ IF P1 .EQS. "D" THEN GOTO DEBUG
$ LINK/EXE=PREPA.EXE PREPA,PREPLIB/L,-
  MID_LIB:TBLLIB/L,STLIB/L,TWLIB/L,OSLIB/L
$ EXIT
$ !
$ DEBUG:
$ LINK/DEBUG/EXE=PREPA.DEBUG PREPA,PREPLIB/L,-
  MID_LIB:TBLLIB/L,STLIB/L,TWLIB/L,OSLIB/L
$ EXIT
