#!/bin/sh
# -----------------------------------------------------------------------
# 
# Bourne-shell procedure  midlogs.sh  
# define logical names needed for running MIDAS
# K. Banse  910211, 920317, 980513, 991014, 000918
# C. Guirao 920221, 940121
# 050427	last modif
# 
# -----------------------------------------------------------------------
# 
MID_HOME=$MIDASHOME/$MIDVERS    ; export MID_HOME
if [ -z "$PIPE_HOME" ]; then
   PIPE_HOME=$MID_HOME/pipeline ; export PIPE_HOME
fi
MID_INCLUDE=$MID_HOME/incl 	; export MID_INCLUDE
#
SYS_EXE=$MID_HOME/system/exec	; export SYS_EXE
MID_EXE=$MID_HOME/prim/exec	; export MID_EXE
APP_EXE=$MID_HOME/applic/exec	; export APP_EXE
STD_EXE=$MID_HOME/stdred/exec	; export STD_EXE
CON_EXE=$MID_HOME/contrib/exec	; export CON_EXE
GUI_EXE=$MID_HOME/gui/exec      ; export GUI_EXE
UVES_EXE=$PIPE_HOME/uves/exec   ; export UVES_EXE
#
MID_PROC=$MID_HOME/prim/proc	; export MID_PROC # executed via @
APP_PROC=$MID_HOME/applic/proc	; export APP_PROC # executed via @a
STD_PROC=$MID_HOME/stdred/proc	; export STD_PROC # executed via @s
CON_PROC=$MID_HOME/contrib/proc	; export CON_PROC # executed via @c
GUI_PROC=$MID_HOME/gui/proc	; export GUI_PROC # executed via @g
XDO_HELP=$MID_HOME/gui/XDo/help ; export XDO_HELP
#
MID_MONIT=$MID_HOME/monit		; export MID_MONIT
MID_CONTEXT=$MID_HOME/context		; export MID_CONTEXT
MID_SETUP=$MID_PROC/setup		; export MID_SETUP
MID_LIB=$MID_HOME/lib			; export MID_LIB
XAPPLRESDIR=$MID_HOME/gui/resource	; export XAPPLRESDIR
# 
MID_SYSTAB=$MID_HOME/systab/bin 	; export MID_SYSTAB
AGL3CONFIG=$MID_HOME/systab/ascii/plot/	; export AGL3CONFIG
MID_HELP=$MID_HOME/prim/help		; export MID_HELP
MID_FIT=$MID_HOME/applic/fit/src	; export MID_FIT
#
MID_CASPEC=$MIDASHOME/calib/data/inst/caspec	; export MID_CASPEC
MID_EFOSC=$MIDASHOME/calib/data/inst/efosc	; export MID_EFOSC
MID_IRSPEC=$MIDASHOME/calib/data/inst/irspec	; export MID_IRSPEC
MID_ARC=$MIDASHOME/calib/data/spec/line		; export MID_ARC
MID_STANDARD=$MIDASHOME/calib/data/spec/flux	; export MID_STANDARD
MID_EXTINCTION=$MIDASHOME/calib/data/spec/exti	; export MID_EXTINCTION
MID_PEPSYS=$MIDASHOME/calib/data/pepsys		; export MID_PEPSYS
MID_FILTERS=$MIDASHOME/calib/data/datatrans	; export MID_FILTERS
MID_EMMI=$MIDASHOME/calib/data/inst/emmi	; export MID_EMMI
MID_FEROS=$MIDASHOME/calib/data/inst/feros	; export MID_FEROS
#
MID_TEDIT=$MID_HELP/tbledit.twh		; export MID_TEDIT
#
# only if directory /midas/version/demo/data exists, use it
# 
if [ -d "$MIDASHOME/demo/data" ]; then
   MID_TEST=$MIDASHOME/demo/data		; export MID_TEST
fi
#
# From start.csh:
TERMCAPFILE=$MID_INCLUDE/termcapfile	; export TERMCAPFILE
DEVCAPFILE=$MID_INCLUDE/devcap.dat	; export DEVCAPFILE
# the end ...
