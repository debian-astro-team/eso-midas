$ ! @(#)startb.com	19.1 (ESO-IPG) 02/25/03 13:58:47 

$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command file STARTB.COM for ESOMC1 (or ESOB)
$ !     K. Banse            ESO - Garching        881208
$ !     M. Peron            ESO - Garching        890704
$ !     900131, 900214, 900308,900709  KB	
$ ! procedure to set up the environment for an interactive MIDAS session
$ !
$ !---------------------------------------------------------
$ !
$ !
$ ! handle the well established work stations (terminal + image display)
$ !
$ IF TERM .EQS. "TXA0:" THEN GOTO DAZC0
$ IF TERM .EQS. "TXA1:" THEN GOTO DAZC1
$ IF TERM .EQS. "TXB0:" THEN GOTO DAZC2
$ !
$ ! Here for "dummy" WORK_STATIONs...
$ INQUIRE/NOPUNCT USER "Enter MIDAS unit (XA, XB, ..., ZZ)"
$ DAZUNIT :== 'F$EXTRACT(0,2,USER)'
$ BAK := "'F$EXTRACT(0,1,USER)'"
$ IF BAK .EQS. "-" THEN GOTO DAZXYZ		!background Midas
$ !
$ ! we allow also batch jobs to work with the image displays...
$ !
$ IF BATCH .NES. "YES" THEN GOTO STEP00
$ !
$ IF DAZUNIT .EQS. "C0" THEN GOTO DAZC0
$ IF DAZUNIT .EQS. "C1" THEN GOTO DAZC1
$ IF DAZUNIT .EQS. "C2" THEN GOTO DAZC2
$ !
$ !------
$ STEP00:
$ !------
$ !
$ IF DAZUNIT .LTS. "PA" THEN GOTO BAD_UNIT
$ IF DAZUNIT .LES. "ZZ" THEN GOTO DAZXYZ
$ !
$ BAD_UNIT:
$ WRITE SYS$OUTPUT "invalid MIDAS unit..."
$ GOTO ERROR
$ !
$ ! terminal for unit A, WORK_STATION 0	
$ !
$ DAZC0:
$ DAZUNIT :== C0
$ ALLOCATE EPA0: IMAGE_DISPLAY
$ IF .NOT.$STATUS THEN GOTO ERROR		!EPA0: already allocated
$ ASSIGN EPA0: IP0
$ ASSIGN EPA0: IP1
$ WORK_STATION := DAZC0
$ I_TYPE := gd8
$ ASSIGN TXA2: GRAPH_TERM
$ ASSIGN TKG.T4010 AGL3DEV
$ GOTO STEP01
$ !
$ ! terminal for unit A, WORK_STATION 1	
$ !
$ DAZC1:
$ DAZUNIT :== C1
$ ALLOCATE EPA1: IMAGE_DISPLAY
$ IF .NOT.$STATUS THEN GOTO ERROR
$ ASSIGN EPA1: IP0
$ ASSIGN EPA1: IP1
$ WORK_STATION := DAZC1
$ I_TYPE := gd8
$ ASSIGN TXA3: GRAPH_TERM
$ ASSIGN TKG.T4010 AGL3DEV
$ GOTO STEP01
$ !
$ ! terminal for unit A, WORK_STATION 2
$ !
$ DAZC2:
$ DAZUNIT :== C2
$ ALLOCATE EPA2: IMAGE_DISPLAY
$ IF .NOT.$STATUS THEN GOTO ERROR		!EPA2: already allocated
$ ASSIGN EPA2: IP0
$ ASSIGN EPA2: IP1
$ WORK_STATION := DAZC2
$ I_TYPE := gd8
$ ASSIGN TXB1: GRAPH_TERM
$ ASSIGN TKG.T4010 AGL3DEV
$ GOTO STEP01
$ !
$ ! any terminal for testing purposes - no image display is connected...
$ !
$ DAZXYZ:
$ ASSIGN 'DAZUNIT'XXX IMAGE_DISPLAY	!..XXX needed, to obtain correct logical translation in MIDASGO
$ WORK_STATION := DAZ'DAZUNIT'
$ I_TYPE := NULL 
$ ASSIGN 'TERMINAL' GRAPH_TERM
$ ASSIGN TKG.VT640 AGL3DEV
$ !
$ !------
$ STEP01:
$ !------
$ !
$ MODEM_FLAG = F$GETDVI("TT","TT_MODEM")	!test, if terminal is a modem
$ !
$ SET NOON
$ ASSIGN _NL: SYS$ERROR			!send any info messages to bit bucket...
$ ASSIGN _NL: SYS$OUTPUT
$ !
$ @ MID_MONIT:DEVICESB.COM
$ !
$ ! if "parallel" session, do not delete intermediate files
$ !
$ IF PARALLEL .EQ. 1 THEN GOTO STEP02
$ !
$ DELETE/NOCONF MID_WORK:CO*.SAV.*,FORGR*.*.*
$ DELETE/NOCONF MID_WORK:MIDJOB*.*.*,MIDDUM*.*.*    !delete all scratch frames
$ DELETE/NOCONF MID_WORK:MIDTEMP*.*.*
$ ! 
$ DELETE/NOCONF MIDDUM*.*,MIDTEMP*.*.*   
$ !
$ !------
$ STEP02:
$ !------
$ !
$ DEASSIGN SYS$ERROR				!use terminal again...
$ DEASSIGN SYS$OUTPUT
$ !
$ ! finally change process name to MIDASxy 
$ SET PROCESS/NAME=MIDAS'DAZUNIT'
$ !
$ ! run MIDASGO
$ !
$ CONT = ""				!find username of current process
$ USER_LOOP:
$ PID = F$PID(CONTEXT)
$ PROCESS = F$PROCESS()
$ PROC1 = F$GETJPI(''PID',"PRCNAM") 
$ IF PROCESS .NES. PROC1 THEN GOTO USER_LOOP
$ USER = F$GETJPI(''PID',"USERNAME")
$ !
$ RUN MID_MONIT:MIDASGO
$ IF $STATUS .NE. 1 THEN GOTO ERROR
$ SET ON
$ !
$ IF BATCH .EQS. "YES" THEN GOTO END_OF_IT
$ !
$ ASSIGN SYS$COMMAND SYS$INPUT
$ GOMIDAS
$ !
$ END_OF_IT:
$ EXIT
$ !
$ ERROR:
$ WRITE SYS$OUTPUT "...MIDAS session aborted..."
$ EXIT
