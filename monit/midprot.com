$ ! @(#)midprot.com	19.1 (ESO-IPG) 02/25/03 13:58:42
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! MIDPROT.COM to change protections
$ ! J-P De Cuyper 970305
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ set prot=w:re mid_disk:['midashome'.'midvers'...]*.*
$ set prot=w:rwe mid_monit:newcom.bin
$ set prot=w:rwe mid_monit:syskeys.vms
$ set prot=w:rwe mid_proc:internal.cod
$ set prot=w:rwe mid_systabpl:agldevs.dat
$ set prot=w:rwe mid_systab:*.*
$ set prot=w:rwe mid_context:*.*
