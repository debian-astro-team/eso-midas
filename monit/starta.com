$ ! @(#)starta.com	19.1 (ESO-IPG) 02/25/03 13:58:47 
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command file STARTA.COM for ESOMC0 
$ !     K. Banse            ESO - Garching        890726
$ !     890704 (MP), 891031, 900131, 900214, 900308, 900709, 901219
$ !     910102, 921007, 921123  KB
$ ! procedure to set up the environment for an interactive MIDAS session
$ !
$ !---------------------------------------------------------
$ !
$ !
$ ! handle the well established work stations (terminal + image display)
$ !
$ IF TERM .EQS. "TXA0:" THEN GOTO DAZA0
$ IF TERM .EQS. "TXA1:" THEN GOTO DAZA1
$ IF TERM .EQS. "TXB0:" THEN GOTO DAZB0
$ IF TERM .EQS. "TXB1:" THEN GOTO DAZB1
$ !
$ ! Here for "dummy" WORK_STATIONs...
$ !INQUIRE/NOPUNCT USER "Enter MIDAS unit (XA, XB, ..., ZZ)"
$ ! Here for "X11" Decwindows.
$ INQUIRE/NOPUNCT USER "Enter MIDAS unit (00, 01, ..., 99, XA, XB, ..., ZZ)"
$ DAZUNIT :== 'F$EXTRACT(0,2,USER)'
$ BAK := "'F$EXTRACT(0,1,USER)'"
$ IF BAK .EQS. "-" THEN GOTO DAZXYZ		!background Midas
$ !
$ !------
$ STEP00:
$ !------
$ !
$ IF DAZUNIT .EQS. "AX" THEN GOTO X11_ANY
$ IF DAZUNIT .GTS. "99" THEN GOTO NEXT_STEP
$ IF DAZUNIT .GES. "00" THEN GOTO X11_ANY
$ !
$ NEXT_STEP:
$ IF DAZUNIT .LTS. "PA" THEN GOTO BAD_UNIT
$ IF DAZUNIT .LES. "ZZ" THEN GOTO DAZXYZ
$ !
$ BAD_UNIT:
$ WRITE SYS$OUTPUT "invalid MIDAS unit..."
$ GOTO ERROR
$ !
$ ! here for DEC Windows (station 00, ..., 99)
$ !
$ X11_ANY:
$ WORK_STATION := DAZ'DAZUNIT'
$ I_TYPE := "SXW  "
$ ASSIGN idi.sxw0g AGL3DEV         !tell AGL that we are in window environment
$ GOTO STEP01
$ !
$ ! terminal for unit A, WORK_STATION 0	(DeAnza)
$ !
$ DAZA0:
$ DAZUNIT :== A0
$ ALLOCATE EPA0: IMAGE_DISPLAY
$ IF .NOT.$STATUS THEN GOTO ERROR		!EPA0: already allocated
$ ASSIGN EPA0: IP0
$ ASSIGN EPA0: IP1
$ WORK_STATION := DAZA0
$ I_TYPE := gd8
$ ALLOCATE TXA2: GRAPH_TERM
$ ASSIGN TKG.VT640 AGL3DEV         !tell AGL what graphics terminal we have...
$ GOTO STEP01
$ !
$ ! terminal for unit A, WORK_STATION 1	(DeAnza)
$ !
$ DAZA1:
$ DAZUNIT :== A1
$ ALLOCATE EPA1: IMAGE_DISPLAY
$ IF .NOT.$STATUS THEN GOTO ERROR
$ ASSIGN EPA1: IP0
$ ASSIGN EPA1: IP1
$ WORK_STATION := DAZA1
$ I_TYPE := gd8
$ ALLOCATE TXA3: GRAPH_TERM
$ ASSIGN TKG.VT640 AGL3DEV         !tell AGL what graphics terminal we have...
$ GOTO STEP01
$ !
$ ! terminal for unit B, WORK_STATION 2
$ !
$ DAZB0:
$ DAZUNIT :== B0
$ ALLOCATE EPB0: IMAGE_DISPLAY
$ IF .NOT.$STATUS THEN GOTO ERROR
$ ASSIGN EPB0: IP0
$ ASSIGN EPB0: IP1
$ WORK_STATION := DAZB0
$ I_TYPE := gd8
$ ALLOCATE TXB2: GRAPH_TERM
$ ASSIGN TKG.VT640 AGL3DEV         !tell AGL what graphics terminal we have...
$ GOTO STEP01
$ !
$ ! terminal for unit B, WORK_STATION 3
$ !
$ DAZB1:
$ DAZUNIT :== B1
$ ALLOCATE EPB1: IMAGE_DISPLAY
$ IF .NOT.$STATUS THEN GOTO ERROR
$ ASSIGN EPB1: IP0
$ ASSIGN EPB1: IP1
$ WORK_STATION := DAZB1
$ I_TYPE := gd8
$ ALLOCATE TXB3: GRAPH_TERM
$ ASSIGN TKG.T4010 AGL3DEV            !tell AGL what graphics terminal we have...
$ GOTO STEP01
$ !
$ ! any terminal - no image display connected
$ !
$ DAZXYZ:
$ ASSIGN 'DAZUNIT'XXX IMAGE_DISPLAY	!..XXX needed, to obtain correct logical translation in MIDASGO
$ WORK_STATION := DAZ'DAZUNIT'
$ ASSIGN 'TERMINAL' GRAPH_TERM
$! ASSIGN NULL AGL3DEV
$ ASSIGN TKG.VT640 AGL3DEV
$ I_TYPE := NULL 
$ !
$ !------
$ STEP01:
$ !------
$ !
$ SET NOON
$ ASSIGN _NL: SYS$ERROR			!send any info messages to bit bucket...
$ ASSIGN _NL: SYS$OUTPUT
$ !
$ ASSIGN 'TERMINAL'  USER_TERM
$ @ MID_MONIT:DEVICES.COM
$ !
$ ! if "parallel" session, do not delete intermediate files
$ !
$ IF PARALLEL .EQ. 1 THEN GOTO STEP02
$ !
$ DELETE/NOCONF MID_WORK:CO*.SAV.*,FORGR*.*.*
$ DELETE/NOCONF MID_WORK:MIDJOB*.*.*,MIDDUM*.*.*     !delete all scratch frames
$ DELETE/NOCONF MID_WORK:MIDTEMP*.*.*
$ ! 
$ DELETE/NOCONF MIDDUM*.*.*,MIDTEMP*.*.*
$ !
$ ! delete temporary files from hardcopies
$ IF I_TYPE .EQS. "SXW  " THEN $ DELETE/NOCONF -
  MID_WORK:SXW*.DAT.*,IDISERV*.*.*,IDISAVE*.DAT.*
$ !
$ !------
$ STEP02:
$ !------
$ !
$ DEASSIGN SYS$ERROR				!use terminal again...
$ DEASSIGN SYS$OUTPUT
$ !
$ ! finally change process name to MIDASxy 
$ SET PROCESS/NAME=MIDAS'DAZUNIT'
$ !
$ ! run MIDASGO 
$ !
$ CONTEXT = ""
$ USER_LOOP:
$ PID = F$PID(CONTEXT)
$ PROCESS = F$PROCESS()
$ PROC1 = F$GETJPI(''PID',"PRCNAM") 
$ IF PROCESS .NES. PROC1 THEN GOTO USER_LOOP
$ USER = F$GETJPI(''PID',"USERNAME")
$ !
$ RUN MID_MONIT:MIDASGO
$ IF $STATUS .NE. 1 THEN GOTO ERROR
$ SET ON
$ !
$ IF BATCH .EQS. "YES" THEN GOTO END_OF_IT
$ !
$ ASSIGN SYS$COMMAND SYS$INPUT
$ GOMIDAS
$ !
$ END_OF_IT:
$ EXIT
$ !
$ ERROR:
$ WRITE SYS$OUTPUT "...MIDAS session aborted..."
$ EXIT
