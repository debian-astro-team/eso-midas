$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.MONIT]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:50 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libprep prepb.obj,prepc1.obj,prepc2.obj,prepc3.obj,prepc4.obj,prepd.obj,prepe.obj,prepf.obj,prepg.obj,preph.obj,prepi.obj,prepj.obj,prepk.obj,prepx.obj,prepz1.obj,prepz2.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
