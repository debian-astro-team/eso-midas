*****************************************************************************
**                                                                         **
**       Copyright (C) 1996-2023 European Southern Observatory             **
**                                                                         **
**   ESO-MIDAS comes with ABSOLUTELY NO WARRANTY; for details type         **
**   `@ license w'. This is free software, and you are welcome to          **
**   redistribute it under certain conditions; type `@ license c'          **
**   for details.                                                          **
**                                                                         **
*****************************************************************************


