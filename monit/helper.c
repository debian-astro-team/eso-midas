/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE         C 
.IDENTIFICATION   main module HELPER
.AUTHOR           K. Banse                        ESO - Garching
.ENVIRONMENT      VMS + UNIX
.KEYWORDS         help facility
.PURPOSE
  display help information
.ALGORITHM
  real work is done in function `helpme'  (in file helpme.c)
.INPUT/OUTPUT
  the following keywords are used:

  IN_A/C/1/60		help command/qualif help_string (=command/qualif)
                        or ^context_name if called from context.prg
			in that case, save context dir in FORGRxy.CTX
  INPUTC/C/1/80		currently enabled contexts
  OUTPUTC/C/1/80	currently enabled contexts
       
.VERSION   [9.00]  920423: split up into more subroutines + prepare for UIMX

 090429		last modif
 -------------------------------------------------- */
 

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <fileexts.h>

#include <monitdef.h>
#include <midback.h>
#include <osparms.h>
/* #include <ok.h> */




static int   nolns, exemod, ulevl, lcount;




int main()

{
int  ec, ed, el, ln0, ln1;
int  iav, logfl[11], nullo, nlog, mmod[2];
int  kk, prlog;
int  unit;
int  helpme();

char   versio[16], conxt[10], cbufa[82], cbuf[82], lina[80], *pbuf;


 
/*  get into MIDAS, save current error values + do not stop on errors */

(void)SCSPRO("HELPER");
(void)SCECNT("GET",&ec,&el,&ed);
ln0 = 0;  ln1 = 1;
(void)SCECNT("PUT",&ln1,&ln0,&ln0);

 
/*  get log flags + user level  */

(void)SCKRDI("MID$MODE",3,2,&iav,mmod,&unit,&nullo);
exemod = mmod[0];
(void)SCKRDI("AUX_MODE",1,8,&iav,logfl,&unit,&nullo);

(void)SCKRDI("LOG",1,11,&iav,logfl,&unit,&nullo);
nlog = logfl[0];
prlog = logfl[8];
nolns = logfl[10] - 2;
if ((prlog == 0) && (logfl[2] == 0)) SCKWRI("LOG",&logfl[2],1,1,&unit);

(void)SCKRDI("ERROR",2,1,&iav,&ln0,&unit,&nullo);
ulevl = ln0;
if ((mmod[1] > 0) || (prlog != 0))
   ulevl = 2;                /* force to expert in batch + printmode  */

lcount = 0;			/* init line count */
(void)SCKGETC("MID$SESS",11,2,&iav,versio);	/* get Midas unit */
pbuf = getenv("MIDVERS");
(void) strcpy(&versio[2],pbuf);	/* append Midas version */


/*  get enabled contexts  */

memset((void *)cbufa,32,(size_t)80);
cbufa[80] = '\0';
(void)SCKRDC("INPUTC",1,1,80,&iav,cbufa,&unit,&nullo);
cbuf[80] = '\0';
(void)SCKRDC("OUTPUTC",1,1,80,&iav,cbuf,&unit,&nullo);


/*  get command line  */
 
(void)SCKGETC("IN_A",1,60,&iav,lina);


/* is it ^blabla from context.prg   or  normal help command ?  */

if (lina[0] == '^')			/* special context dir command */
   {
   int  fp, mm;
   char  ctxdir[104], frame[120], record[120], mwdir[100];

   (void) strncpy(conxt,&lina[1],8);	/* get 8 char context name */
   conxt[8] = '\0';
   mm = CGN_INDEXC(conxt,'.');		/* abc.ctx */
   if (mm > 0)
      {
      for (kk=mm; kk<8; kk++) conxt[kk] = ' ';
      }
   else
      mm = 8;

   /* get full context specs + isolate directory */

   (void) SCKGETC("FULLFILE",1,100,&iav,ctxdir);
   ctxdir[iav-mm-4] = '\0';

   (void) OSY_TRNLOG("MID_WORK",mwdir,99,&iav);
   (void) strcpy(frame,mwdir);
   (void) strcpy(lina,"FORGR  .CTX");
   lina[5] = versio[0];
   lina[6] = versio[1];
   (void) strcat(frame,lina);

   fp = CGN_OPEN(frame,READ);		/* just to see, if file exists */
   if (fp == -1) 
      {
      fp =  CGN_OPEN(frame,WRITE);	/* create new file */
      if (fp == -1) 
         {
         sprintf(frame,"Could not open FORGR%c%c.CTX in MID_WORK...",
                        versio[0],versio[1]);
         SCTPUT(frame);
         }
      else
         {
         (void) strncpy(record,conxt,8);
         (void) strcpy(&record[8],ctxdir);
         (void) osawrite(fp,record,(int)strlen(record));
         (void) osaclose(fp);
         }
      }
   else
      {
      int  gp;
      char  grame[124];

      (void) strcpy(grame,frame);
      (void) strcat(grame,"new");
      gp = CGN_OPEN(grame,WRITE);		/* open new file for copying */
      mm = 0;

    read_loop:
      memset((void *)record,32,(size_t)100);
      iav = osaread(fp,record,100);		/* look for context in file */

      if (iav < 1)
         {				/* not found, so add to the end */
         (void) osaclose(fp);
         if (mm == 0)
            {					/* append new record */
            (void) strncpy(record,conxt,8);
            (void) strcpy(&record[8],ctxdir);
            (void) osawrite(gp,record,(int)strlen(record));	
            }
         (void) osaclose(gp);
         (void) osfrename(grame,frame);
         }
      else
         {
         if (strncmp(conxt,record,8) == 0)
            {
            (void) strcpy(&record[8],ctxdir);	/* direc could have changed */
            mm = 99;
            }					/* copy to new frame */
         (void) osawrite(gp,record,(int)strlen(record));
         goto read_loop;
         }
      }
   }

else					/* we do everything in helpme  */
   helpme(1,versio,lina,cbufa,cbuf);


logfl[0] = nlog;			/* reset key LOG(1) */
(void)SCKWRI("LOG",logfl,1,1,&unit);
(void)SCSEPI();
return 0;			
}

/*

*/

/* 	this function must be synchronized with the one in dspout.c !!!  */

int dspout(record)
char  *record;		/* IN: string to be displayed */

{
char    reco[100];

int  n;
register int  nr;



/*  throw away the '\' and '|' characters */

if (record[0] != '\0')
   {
   n = 0;
   for (nr=0; nr<99; nr++)
      {
      if (record[nr] == '\0')
         break;
      else if ( (record[nr] == '\\') && 
                ((record[nr+1] != 'a') || (record[nr+2] != 'g')) )
         {
         if (nr == 0) return (0);		/* new line */
         }
      else if (record[nr] == '|')
         {
         if (nr == 0) 
            return (0);				/* new line */
         else if (record[nr-1] == '\\')
            reco[n++] = record[nr];		/* "\|" -> "|"  */
         else if ( (record[nr-1] != ' ') && (record[nr+1] != ' ') )
            reco[n++] = record[nr];		/* x|y is o.k. */
         }
      else
         reco[n++] = record[nr];		/* copy character */
      }
   reco[n] = '\0';
   }
else
   {
   reco[0] = ' ';
   reco[1] = '\0';
   }

SCTPUT(reco);                                /*  display on terminal + log */
 

/*  if exemod != 1, check no. of lines on terminal screen ... */

if (exemod != 1) 
   {
   lcount++ ;

   if (lcount > nolns) 
      {
      if (ulevl >= 2) 
         {
         lcount = 0;
         return (0);
         }
  

/*  for novice + user level we ask */

      (void)printf("\n\rhit return to continue, 'q' + return to quit ");
      reco[0] = ' ';
      reco[0] = getchar();
      if ((reco[0] == 'Q') || (reco[0] == 'q')) 
         return (1);			/* indicate, that we quit */
      
      lcount = 0;
      }
   }

return (0); 
}

/* ARGSUSED */

/* 	this function must be synchronized with the one in dspout.c !!!  */

void prepmem(yourpntr)

char **yourpntr;

{

return;

}
