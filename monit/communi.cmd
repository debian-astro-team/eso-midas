! @(#)communi.cmd	19.1 (ESO-DMD) 02/25/03 13:58:39 
! 
! verification procedure for communication interfaces
! this is executed automatically by entering:
! communi.exe verify xy
! with xy = Midas unit (defaulted to 00)
! 
! K. Banse	ESO-Garching	960116, 990830
! 
c
write/key klaus/c*3/1/5 abc all
c 
read/key klaus
XCKWRC*3 klaus,3,3,j
c 
read/key klaus
XCKRDC*3 klaus,4,1
XCKRDC*6 klaus,2,1
! 
c,read/keyw aux_mode
! 
XCKRDI mode
! 
c,create/image &a 2,256,256 ? gauss 128,32,128,32
! 
c,create/disp 0 360,360,660,400
c,load/image &a
c,load/lut rainbow4
c,disp/lut
c,label/display Gauss_Image 290,64 ? ? 1
c 0
fft/freq &a
! 
w
! 
c,clear/chan ov
c,load/image fftr cuts=-0.00000000001,0.00000000001
c,label/display Real_FFT 290,64 ? black 1
c,wait/secs 2
xx
! 
