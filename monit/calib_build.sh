#!/bin/sh
# 
# shell script calib_build to install the calibration data tables
# optional parameter = directory containing compressed tarfile
# calib.tar.Z
# 
# variables MIDASHOME and MIDVERS must be set already!
# 
# K. Banse      041126
#
#.VERSION
# 041126	last modif

if [ -d $MIDASHOME/$MIDVERS ] ; then
   echo "MIDASHOME/MIDVERS = $MIDASHOME/$MIDVERS"
else
   echo env. var. MIDASHOME and/or MIDVERS not set... we abort
   exit 1
fi

cd $MIDASHOME
if  [ $# = 1 ]; then
   if [ ! -f $1/calib.tar.Z ] ; then
      echo file calib.tar.Z not found in $1 ... check that directory
      exit 1
   else
      echo copying calib.tar.Z from $1
      sleep 1
      cp $1/calib.tar.Z .
   fi
else
   if [ ! -f calib.tar.Z ] ; then
      if [ -f calib.tar ] ; then
         echo we use uncompressed file calib.tar found in $MIDASHOME
         sleep 2
      else
         echo neither file calib.tar.Z nor calib.tar found in $MIDASHOME
         echo use: calib_build dir_containing_compressed_calib_tarfile
         echo to copy the file first
         exit 1
      fi
   fi
fi

if [ -f calib.tar.Z ] ; then
   uncompress calib.tar.Z
fi

tar xvf calib.tar

cd calib/install
. dbassign.sh

echo "  "
echo in `pwd` we execute Midas
echo inmidas -j "@@ caldb; @@ filters; bye"
sleep 2

$MIDASHOME/$MIDVERS/system/unix/inmidas -j "@@ caldb; @@ filters; bye"

# get rid of temp. dirs

cd ..
rm -rf raw install

echo "   "
echo finishing up in `pwd`
 
