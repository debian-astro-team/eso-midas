$ ! @(#)preinstall.com	19.1 (ESO-IPG) 02/25/03 13:58:43 
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1987 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Jean-Pierre De Cuyper - [KSB/ORB]
$ ! .IDENT       PREINSTALL.COM
$ ! .COMMENTS    Preinstallation procedure in ['MIDASHOME'.'MIDVERS'.MONIT]
$ !              Create Midas libraries.
$ ! .REMARKS     Genereted by hand.
$ ! .DATE        Thu April 27 2000
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ !
$ LIB/CREATE/NOLOG LIBPREP	
$ !
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
$ PURGE/NOLOG/NOCONFIRM LIBPREP.OLB
