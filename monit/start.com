$ ! @(#)start.com	19.1 (ESO-IPG) 02/25/03 13:58:47 
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! command file START.COM
$ !   K. Banse     ESO - Garching        900308, 910130, 920908, 940104
$ !   C. Guirao    ESO - Garching        890425
$ !
$ ! start command procedure for MIDAS 
$ ! when installing MIDAS you have to modify the sections which are indicated
$ ! via:  $ ! ****************
$ !       .....
$ !       $ ! ****************
$ ! according to your installation.
$ !
$ !---------------------------------------------------------------------------
$ !
$ ! ****************************************
$ !  modify MIDVERS and MIDASHOME according to your local installation 
$ !
$ MIDVERS :== 95NOV
$ MIDASHOME :== MIDAS
$ ! ****************************************
$ !
$ ASSIGN _NL: SYS$ERROR			!send any info messages to bit bucket...
$ ASSIGN _NL: SYS$OUTPUT
$ ! 
$ IF TERMWIN .EQS. "NO" THEN GOTO STEPT
$ ASSIGN yes TERMWIN
$ GOTO STEP0
$ !
$ STEPT:
$ ASSIGN no TERMWIN
$ ! 
$ STEP0:
$ @ MID_DISK:['MIDASHOME'.'MIDVERS'.MONIT]MIDLOGS
$ !
$ ! if necessary create MIDAS work directory
$ !
$ SHO LOGI MID_WORK
$ IF $STATUS .EQ. %X10000001 THEN GOTO STEP1
$ HERE := 'F$LOGICAL("SYS$LOGIN")'
$ KSTR := 'F$EXTRACT(0,F$LOCATE("]",HERE),HERE)'
$ ASSIGN 'KSTR'.MIDWORK] MID_WORK
$ !
$ STEP1:
$ SET NOON
$ CREATE/DIR MID_WORK
$ MYSTAT == $STATUS
$ SET ON
$ DEASSIGN SYS$ERROR				!use terminal again...
$ DEASSIGN SYS$OUTPUT
$ IF MYSTAT .EQ. %X10911293 THEN GOTO STEP2
$ IF MYSTAT .EQ. %X00000001 THEN GOTO STEP2
$ IF MYSTAT .EQ. %X00038030 THEN GOTO STEP2
$ ! we could not create the directory MID_WORK !!
$ SHO LOGI MID_WORK
$ WRITE SYS$OUTPUT "We could not create above directory...!"
$ EXIT
$ !
$ ! -----------------------------------------------------------
$ ! 
$ STEP2:
$ !
$ ! if we are working in a VAXcluster
$ ! depending upon the node we're on, STARTA.COM or STARTB.COM, etc.
$ ! should be used
$ ! 
$ ! 
$ VAX := 'F$GETSYI("SCSNODE")'
$ HERE := 'F$LOGICAL("SYS$DISK")''F$DIRECTORY()'
$ START_FILE := STARTA
$ !
$ ! ****************************************
$ !  for a VAX cluster modify the name "ESOMC1" according to your installation
$ ! 
$ IF VAX .EQS. "ESOMC1" THEN $ START_FILE := STARTB
$ ! ****************************************
$ !
$ ! handle the optional parameters with INMIDAS
$ !
$ !
$ TERMINAL :== 'F$LOGICAL("TT")'
$ TERM :== 'F$EXTRACT(1,5,TERMINAL)'
$ !
$ BATCH :== NO 
$ BACKGROUND :== NO
$ PARALLEL == 0
$ !
$ ! check, if we are submitted
$ IF "''F$MODE()'" .EQS. "BATCH" THEN   BATCH :== YES
$ !
$ ! Now execute the node specific startup file
$ !
$ TYPE = F$TYPE(MIDOPTION)
$ IF TYPE .EQS. "" THEN GOTO GO_MIDAS_GO
$ IF MIDOPTION .EQS. "PARALLEL" THEN PARALLEL == 1
$
$ GO_MIDAS_GO:
$ @ MID_MONIT:'START_FILE' 
$ !
$ EXIT
