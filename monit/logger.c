/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++ Module LOGGER +++++++++++++++++++++++++++++++++++++++
.LANGUAGE   C
.IDENTIFICATION  Module logger.c
.AUTHOR   Klaus Banse
.KEYWORDS Midas logging 
.ENVIRONMENT VMS and UNIX
.COMMENTS
prints out the LOG file of a MIDAS session

.VERSION  [1.00] 881107: creation from MID_LOG.C 

 090429		last modif
------------------------------------------------------------------------*/
 
 
#include <stdlib.h>
#include <stdio.h>

#include <fileexts.h>
#include <fsydef.h>
 
#define  READ         0
#define  WRITE        1
 
 
int main()
 
{
int   idum, iav, nulo, log[8], mode[4], page[2];
int   mm, stat;
int   reclen;
int   uni;
int   logpage, print_it;
int    lp=-1, outp=-1;

float  rdum;
double ddum;

char  work[84], miduni[2], myfile[40], pri[24];
 
 

(void) SCSPRO("LOGGER");			 /*  get into MIDAS  */


/*  get working area + MIDAS unit   */

(void) SCKRDI("MID$MODE",1,4,&iav,mode,&uni,&nulo);
(void) SCKRDI("LOG",1,8,&iav,log,&uni,&nulo);
(void) SCKRDC("MID$SESS",1,11,2,&iav,miduni,&uni,&nulo);


/*  build up logfile name + open it  */

(void)memcpy(myfile,"MID_WORK:FORGR  .LOG",(size_t)21);
myfile[14] = miduni[0];
myfile[15] = miduni[1];

lp = CGN_OPEN(myfile,READ);
if (lp < 0)                          /* we could not get started... */
   {
   stat = ERR_INPINV;
   (void) sprintf(work,"LOGGER: cannot open logfile %s",myfile);
   MID_ERROR("MIDAS",work,stat,1);
   goto end_of_it;
   }
 
 
/*  get print device */

(void) SCKRDC("MID$PRNT",1,1,20,&iav,pri,&uni,&nulo);
if ((pri[0] == ' ') || (pri[0] == '\0'))
   SCETER(17,"Invalid print device...");

pri[20] = '\0';
for (mm=1; mm<20; mm++)
   {
   if (pri[mm] == ' ')
      {
      pri[mm] = '\0';
      break; 
      }
   }


/*  get page specification  */

(void) SCKRDI("INPUTI",15,2,&iav,page,&uni,&nulo);

if (page[0] == 0) page[0] = 1;
if (page[1] == 0) page[1] = 1;


/*  handle negative 'no' = last 'no' pages  */

   if (page[0] < 0)
   {
   page[0] += (log[4] + 1);			/*  add current page count  */
   if (page[0] <= 0) page[0] = 1;
   page[1] = 9999;
   }

print_it = 0;

if (pri[0] != 'T')
   {
   if (pri[0] != 'F')			/* use default file name */
      {
      (void)memcpy(myfile,"MID_WORK:midtemp  .print",(size_t)25);
      myfile[16] = miduni[0];
      myfile[17] = miduni[1];
      }
   else					/* use given file name */
      (void) strcpy(myfile,&pri[2]);

   outp = CGN_OPEN(myfile,WRITE);
   if (outp < 0) 
      {
      stat = ERR_INPINV;
      (void) sprintf(work,"LOGGER: cannot open output file %s\n",myfile);
      MID_ERROR("MIDAS",work,stat,1);
      goto end_of_it;
      }         
   }

 
read_loop:
 
reclen = osaread(lp,work,82);
if (reclen == 0) goto read_loop;
 
if (reclen != -1)			/*  -1 = EOF */
   {
   mm = CGN_INDEXS(work,"Page");
   if (mm <= 0) 
      {
      if (print_it == 0) goto read_loop;
      }

   else
      {
      iav = CGN_CNVT(&work[mm+4],1,1,&idum,&rdum,&ddum);
      if (iav == 1)
         logpage = idum;
      else
         logpage = page[0];

      if (logpage < page[0]) goto read_loop;
      if (logpage > page[1]) goto end_of_it;
      print_it = 1;
      }
         
   if (pri[0] != 'T')
      stat = osawrite(outp,work,(int)strlen(work));
   else
      (void) printf("%s\n",work);

   goto read_loop;
   }

osaclose(lp);
if (pri[0] != 'T') osaclose(outp);

 
end_of_it:
SCSEPI();

return 0;			/* just for pleasing "gcc -Wall" ... */
}
