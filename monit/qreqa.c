/*===========================================================================
  Copyright (C) 1995-2006 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

 
/*+++++++++++++++++++++++++++++++  QREQA  +++++++++++++++++++++++++++++++

.LANGUAGE     C

.IDENTIFICATION
MIDAS front end (main module QREQA) for executing MIDAS applications
from the UNIX command line

.AUTHOR       K. Banse / IPG                  	ESO - Garching

.KEYWORDS     MIDAS front end

.ENVIRONMENT  UNIX

.PURPOSE
get the MIDAS command string from command line + take action accordingly

.ALGORITHM
execute command and execute upon return from PREPX

.RETURNS  Midas return status

.VERSION [1.00] 990107: built up from prepa.c

 060125		last modif
------------------------------------------------------------------------*/
 

# include <stdio.h>
# include <stdlib.h>

#include <fileexts.h>

#include <osyparms.h>
#include <monitdef.h>
#include <midfront.h>
#include <midback.h>
#include <fsydef.h>
 

int    is_a_tty = 1;                        /* Is this a terminal, (yes=1) */


/*

*/

void intermail()

{
return;
}

/*

*/


int main(argc,argv)
int argc;
char *argv[];

 
{
int   kk, jj, iwa, pxinfo[6];
 
char  *argptr, cxp[4];




for (jj=0; jj<4; jj++) cxp[jj] = 'N';

help_sect:
if (argc < 2) 
   {				/* display contents of drs help file */
   char  work[84];
   int   reclen, fp;		/* we cannot use CGN_DISPFIL, because */
				/* no keywords there, yet */
   fp = CGN_OPEN("MID_HELP:drs.help",0);
   if (fp == -1) 	
      {
      (void) printf("Could not open drs helpfile `MID_HELP:drs.help'...\n");
      return(-99);
      }
   reclen = osaread(fp,work,80);	/* skip SCCS header of helpfile */

   read_loop:
   reclen = osaread(fp,work,80);
   if (reclen == 0)
      goto read_loop;
   else if (reclen == -1)
      {
      (void) osaclose(fp);
      return (-99);
      }

   (void) printf("%s\n",work);
   goto read_loop;
   }

qinit_here();		/* initialize */
iwa = 1;
kk = argc - 1;

testa:
if (*argv[iwa] == '-')
   {
   argptr = argv[iwa] + 1;
 
   if (*argptr == 'd')		/* -debug */
      {
      cxp[0] = 'Y';
      iwa ++;
      kk --;
      goto testa;
      }

   if (*argptr == 'u') 		/* -update (of FITS files) */
      {
      cxp[1] = 'Y';
      iwa ++;
      kk --;
      goto testa;
      }

   if (*argptr == 'f') 		/* -fits (format of new files) */
      {
      cxp[2] = 'Y';
      iwa ++;
      kk --;
      goto testa;
      }

   argc = 1;			/* for all other options display help text */
   goto help_sect;
   }


if (kk > 10)
   {
   printf("drs: no. of arguments (= %d) truncated to MAX_TOKEN (= 10)\n",kk);
   kk = 10;
   }

for (jj=0; jj<kk; jj++)
   {
   TOKEN[jj].LEN = CGN_COPY(TOKEN[jj].STR,argv[iwa++]);
   if ( (TOKEN[jj].STR[0] == '\\') && (TOKEN[jj].STR[1] == '*') )
      {
      TOKEN[jj].STR[0] = '*';
      TOKEN[jj].STR[1] = '\0';
      TOKEN[jj].LEN --;
      }
   /* printf("token[%d] = %s\n",jj,argv[iwa-1]); */
   }

LINE.LEN = TOKBLD(0,LINE.STR,MAX_LINE,1,kk);


ERRORS.OFFSET = FRONT.PEND + 5;

pxinfo[0] = 80;
pxinfo[1] = 24;
iwa = prepx(-2,cxp,pxinfo);
if (iwa != 0) printf("drs: return status from prepx() = %d\n",iwa);

(void) MID_MOVKEY("O"," ");              /* save keywords in FORGRdrs.KEY */

ospexit(ERRORS.SYS);
return 0;
}

/*

*/
 
void qinit_here()

{
int  stat, jj;

char   wstr[160];




FRONT.DAZUNIT[0] = '6';			/* use unit 66 */
FRONT.DAZUNIT[1] = '6';

stat = OSY_TRNLOG("MID_WORK",wstr,160,&jj);    /* Decode startup directory */
if (stat != 0) 
   {
   stat = OSY_TRNLOG("HOME",wstr,160,&jj);	/* use $HOME/midwork/ */
   if (wstr[jj-1] != FSY_DIREND) wstr[jj++] = FSY_DIREND;
   (void) strcpy(&wstr[jj],"midwork/");
   }
else if (wstr[jj-1] != FSY_DIREND)
   {
   wstr[jj++] = FSY_DIREND;
   wstr[jj] = '\0';
   }

(void) strcpy(FRONT.STARTUP,wstr);	/* save name of startup directory */

FRONT.ENV = '*';		/* to indicate Unix command line input */
FRONT.PEND = 6;			/* length of prompt + 1 */
FRONT.PID = 0;			/* not used */
FRONT.PP = -1;
FRONT.PLAYBACK = 0;
FRONT.INTERM = 0;

server.MODE = 0;
server.ECKO = 'N';

}
