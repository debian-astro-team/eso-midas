/*===========================================================================
  Copyright (C) 1995-2007 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++
 
.IDENTIFICATION
  dspout.c
  K. Banse                        ESO - Garching
 
.KEYWORDS
  help facility
 
.PURPOSE
  display help information
 
.ALGORITHM
  cumbersome but rather straight forward ...
  but do not write that into the logfile (to save space...) unless
  keyword LOG(3) is set

.INPUT/OUTPUT
  the following keywords are used:

  INPUTC/C/1/40      help command/qualif help_string (=command/qualif)
  CONTEXT/C/1/64      currently enabled contexts
       
.VERSIONS [5.00]            move towards FORTRAN 77 and UNIX environment
 070831		last modif

 -------------------------------------------------- */

#include <midas_def.h>
 
static char  *cpntr, *savpntr;




/*

*/

/*      this function must be synchronized with the one in helper.c !!!  */

int dspout(record)
char  *record;		/* IN: string to be displayed */
 
{
char    reco[88];

int  nn, n;
register int nr;
 


/*  throw away the '\' characters */

nn = strlen(record);
if (nn > 0)
   {
   n = 0;
   for (nr=0; nr<nn; nr++)
      {
      if ( (record[nr] != '\\') || 
           ((record[nr+1] == 'a')&&(record[nr+2] == 'g')) )
         reco[n++] = record[nr];
      else
         {
         if (nr == 0) return 0;			/* do nothing */
         }
      }
   reco[n] = '\0';
   }
else
   {
   reco[0] = ' ';
   reco[1] = '\0';
   }


(void) strcpy(cpntr,reco);
cpntr += (int) strlen(reco);		/* now we point to the '\0' */
*cpntr++ = '\n';
*cpntr = '\0';				/* make sure we'll have an end */
return 0;
}

/*

*/

/*      this function must be synchronized with the one in helper.c !!!  */

void prepmem(yourpntr)
char  **yourpntr;

{
long nobytes;
static int  firsty = 0;



if (firsty == 0)
   {
   firsty = 1;
   nobytes = 800000;			/* allocate virtual memory for text */
   savpntr = osmmget(nobytes);
   }

*yourpntr = savpntr;
cpntr = savpntr;
}
