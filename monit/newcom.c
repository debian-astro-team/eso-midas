/*===========================================================================
  Copyright (C) 1995-2011 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE    C
.IDENTIFICATION     main module NEWCOM
.AUTHOR   Klaus Banse
.KEYWORDS MIDAS command initialization
.ENVIRONMENT  UNIX / VMS
.PURPOSE
  read file MID_MONIT:NEWCOM. or NEWTOM. and store its contents into binary file
  MID_MONIT:NEWCOM.DAT or NEWTOM.DAT
.ALGORITHM
  read command, qualifier and corresponding command line from NEWCOM.
  and store them into the relevant data structures, which are then
  written into binaray file NEWCOM.DAT for fast access
 
  e.g. CLEAR/ZOOM @ CLEAZOOM
       FFT/FREQ   @/BACK FFT F

.RETURNS  nothing

.VERSION  [1.20] 870714: use osa-routines for ASCII file treatment

 110427		last modif
-------------------------------------------------------------------*/
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <fileexts.h>
#include <proto_monit.h>
#include <stlibvars.h>
#include <commdef.h>
#include <osparms.h>

void DEFCOM();


 
 
int main()
 
{
char	record[132], cline[82], atom[82], comnd[6], qualif[4], defqual [4];
char	file[80], *cpntr, *tmpntr;
	
int   n, nn, mm, status, first_time, allover;
int   latom, atom_len, reclen, start;
int   totsize, comsize, qualsize, linesize;
int   fp, gp;
 
 
/*  initialize   */

COMN.CMAX = MAXSTA_COM;
COMN.QMAX = 4 * MAXSTA_COM;
COMN.ENDLIN = 24 * MAXSTA_COM;
COMN.FIRST = 0;
COMN.INUSEC = -1;
COMN.INUSEQ = -1;
 

/*  allocate memory for command, qualifier structures + command lines */

totsize = sizeof(struct COMND_ALL);

comsize = (sizeof(struct COMND_STRUCT)) * (COMN.CMAX+1);   /* for security */
tmpntr = (char *) malloc((size_t)comsize);
if (tmpntr == NULL)
   {
   (void) printf("could not allocate %d bytes for COMND_STRUCT\n",comsize);
   ospexit(0);
   }

memset((void *)tmpntr,32,(size_t)comsize);		/* initialize it */
COMN.CP = (struct COMND_STRUCT *) tmpntr;

qualsize = (sizeof(struct QUALIF_STRUCT)) * (COMN.QMAX+1);   /* for security */
tmpntr = (char *) malloc((size_t)qualsize);
if (tmpntr == NULL)
   {
   (void) printf("could not allocate %d bytes for QUALIF_STRUCT\n",qualsize);
   ospexit(0);
   }

memset((void *)tmpntr,32,(size_t)qualsize);		/* initialize it */
COMN.QP = (struct QUALIF_STRUCT *) tmpntr;

linesize = COMN.ENDLIN+4;
COMN.LINE = (char *) malloc((size_t)linesize);
if (COMN.LINE == NULL)
   {
   (void) printf("could not allocate %d bytes for COMLINE\n",linesize);
   ospexit(0);
   }
else
   memset((void *)COMN.LINE,32,(size_t)linesize);	/* initialize it */

allover = 0;
first_time = 0;
memset((void *)defqual,32,(size_t)4);	/* use blank as default qualifier  */
 

/*  get file name of ASCII command list  */
 
#if vms
strcpy(record,"newcomVMS.in");		/* here we only have @ instead of @% */
#else
strcpy(record,"newcom.in");
#endif
CGN_LOGNAM(record,file,72);

fp = osaopen(file,READ);
if (fp == -1)
   {
   (void) printf("problems opening the initial commandfile %s ...\n",file);
   ospexit(0);
   }
	

/*  create new binary output file newcom.bin + open it  */
 
nn = strlen(file);
#if vms
(void) strcpy(&file[nn-6],".bin");		/* newcomVMS.in => newcom.bin */
#else
(void) strcpy(&file[nn-3],".bin");		/* newcom.in => newcom.bin */
#endif

gp = osdopen(file,WRITE);
if (gp == -1)
   {
   (void) printf("problems in creating binary command file...\n");
   ospexit(0);
   }
	
atom_len = 30;
 

/*  loop + read input records in rather free format  */
 
read_loop:
reclen = osaread(fp,record,130);
if (reclen == 0)
   goto read_loop;
else if (reclen < 0) 
   goto close_it;
 
 
/*  cut out comments  */
 
mm = CGN_INDEXC(record,'!');
if (mm == 0)				/* skip comment lines */
   goto read_loop;
else if ((mm > 0) && (mm < reclen))
   {
   record[mm] = '\0';
   reclen = mm;
   }

 
/* convert tabs to blanks +  skip blank records  */
 
CGN_REPLA(record,mm,'\t',' ');
if ( (n = CGN_SKIP(record,' ','f',&mm) ) == 0 ) goto read_loop;
	
start = 0;
latom = CGN_EXTRSS(record,reclen,' ',&start,atom,atom_len);
atom[latom] = '\0';				/* force end of string  */
 
 
/* separate command and (optional) qualifier  */
 
EXTRACOM(atom,comnd,qualif);
	
 
/* extract actual command line  (without leading blanks)  */
 
cline[0] = '\0';
for (n=latom+1;n<reclen; n++)
   {
   if (record[n] != ' ')
      {
      (void) strcpy(cline,&record[n]);
      break;
      }
   } 
	
 
/* test, if primitive command or not  */
 
if (cline[0] == '\0')
   status = ADDCOM(comnd,qualif,-2,1,cline);	    /* add primitive command  */

else
   {
   if (first_time == 0)
      {
      first_time = 1;
      COMN.LPRIMC = COMN.INUSEC;
      COMN.LPRIMQ = COMN.INUSEQ;
      }
   status = ADDCOM(comnd,qualif,-1,1,cline);		/* add fixed command  */
   }
 
 
if (status != 0) 				/* everything o.k. ?  */
   {
   allover = 1;
   (void) printf("command %6.6s/%4.4s gave the following problem:\n",
                 comnd,qualif);
   if (status == 5)
      (void) printf("no command line ...\n");
   else if (status == 6)
      (void) printf("ambiguous command or qualifier ...\n");
   else if (status == 10)
      (void) printf("overflow in data structure ...\n");
   else if (status == 42)
      (void) printf("overflow in command_line buffer ...\n");
   else
      (void) printf("unknown error code ...\n");
   goto close_it;
   }
	
goto read_loop;
 
	
close_it:
(void) osaclose(fp);				/*  close newcom.in */
COMN.FDEL = COMN.FIRST;
 
 
/*  now copy tables to binary file newcom.bin  */
 
if (allover != 0)
   {
   (void) printf("Something wrong - check again the command input file ! \n");
   ospexit(1);
   }
 
/* put default qualifier IMAG to all commands which have 
   a qualifier IMAG or ...                                    */

DEFCOM("*","IMAG");		/* force default to IMAG  */
DEFCOM("PLOT","ROW ");		/* for PLOT, OVERPLOT set it to ROW  */
DEFCOM("OVERPLOT","ROW ");
DEFCOM("SET","CONT");		/* for SET use CONTEXT as default */


/*  open ASCII file newcom.out for listing of commands/qualifiers  */
 
nn = strlen(file);
(void) strcpy(&file[nn-3],"out");		/* newcom.bin => newcom.out */
fp = osaopen(file,1);
if (fp == -1)
   {
   (void) printf("problems in opening the ASCII output file...\n");
   ospexit(0);
   }
 
(void) SHOWCOM(fp,"-D","  ");			/*  diagnostic display  */
(void) osaclose(fp);


/*  write the structures into binary output file  */

cpntr = (char *) &COMN;
status = osdwrite(gp,cpntr,(unsigned int)totsize);
if (status < totsize) goto osd_error;

cpntr = (char *) COMN.CP;
status = osdwrite(gp,cpntr,(unsigned int)comsize);
if (status < comsize) goto osd_error;

cpntr = (char *) COMN.QP;
status = osdwrite(gp,cpntr,(unsigned int)qualsize);
if (status < qualsize) goto osd_error;

status = osdwrite(gp,COMN.LINE,(unsigned int)linesize);
if (status < linesize) goto osd_error;

(void) osdclose(gp);
(void) printf("Commands successfully stored in newcom.bin. \n");
ospexit(0);

osd_error:
(void) printf("problems in writing into binary command file ...\n");
ospexit(1);

return 0;			/* for gcc -Wall ... */
}
 
