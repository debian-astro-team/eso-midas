# .COPYRIGHT:	Copyright (c) 1988-2012 European Southern Observatory,
#						all rights reserved
# .TYPE		make file
# .NAME		$MIDASHOME/$MIDVERS/monit/makefile 
# .LANGUAGE	makefile syntax
# .ENVIRONMENT	Unix Systems. 
# .COMMENT	Compiles source files and generates "prepa.a" library
#		and commands.
#
# .REMARKS	
# .AUTHOR	Carlos Guirao
# .VERSION 1.1	31-Aug-1988:	Implementation
# .VERSION 1.2	08-NOV-1988  	(KB)
# .VERSION 1.3	04-Jan-1989:	Generating makefile.com for VMS systems
# .VERSION 1.4	20-JAN-1989  	(KB)
# .VERSION 2.2  920521:         Removing MLIB CG
# .VERSION 3.0  930308:		Using default.mk file
# .VERSION 3.1  930706:		start.sh self-defined. CG
# 
# 120131	last modif


include ../local/default.mk

CFLAGS += $(C_OPT) $(EDITSWITCH) $(DEBUG) $(MEM_OPT) $(SYS) -I$(INC) -I../libsrc

LLIB  = libprep.a -L$(LIBDIR) -lmidas 
LLIB1 =	libprep.a -L$(LIBDIR) -lgen  -lxcon -lgmidas -lmidas 
LLIB2 =	-L$(LIBDIR)  -lmidas 
LLIB3 =	-L$(LIBDIR) -lxcon -lmidas

LIBS =	libprep.a \
	$(LIBDIR)/libmidas.a

LIBS1 =	libprep.a \
	$(LIBDIR)/libgen.a \
	$(LIBDIR)/libxcon.a \
	$(LIBDIR)/libgmidas.a \
	$(LIBDIR)/libmidas.a

LIBS2 =	$(LIBDIR)/libmidas.a

LIBS3 =	$(LIBDIR)/libxcon.a \
	$(LIBDIR)/libmidas.a

OUT =	prepa.exe communi.exe midasgo.exe catal.exe newcom.exe internal.exe \
	qreqa.exe \
	rbexec.exe \
        internal1.exe helper.exe logger.exe newcom.out \
	syskeys.unix syskeys.vms \
	calib_build

LIBOUT = libprep.a

OBJ =	prepb.o prepc1.o prepc2.o prepc3.o prepc4.o \
	prepd.o prepe.o prepf.o \
	prepg.o preph.o prepi.o \
	prepj.o prepk.o \
	prepx.o prepz1.o prepz2.o

OBJ1 =	prepa.o prepa1.o prepa2.o \
	qreqa.o \
	rbexec.o \
	communi.o \
	midasgo.o catal.o newcom.o internal.o \
	internal1.o logger.o helper.o helpme.o

OBJ2 =	dspout.o


# DEPENDENCIES:
all: $(MAKEFILE_VMS) $(LIBOUT) $(OUT) $(OBJ2)

$(MAKEFILE_VMS): makefile
	$(MAKE_VMS)

$(LIBOUT): $(OBJ) 
	$(AR) $(AR_OPT) $(LIBOUT) $(OBJ)
	$(RANLIB) $(LIBOUT)

prepa.exe: prepa.o prepa1.o prepa2.o $(LIBS1) 
	$(LDCC) prepa.o prepa1.o prepa2.o $(LLIB1) $(EDITLIBS) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

qreqa.exe: qreqa.o $(LIBS1) 
	$(LDCC) qreqa.o $(LLIB1) $(EDITLIBS) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

rbexec.exe: rbexec.o $(LIBS) 
	$(LDCC) rbexec.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

communi.exe: communi.o $(LIBS3)
	$(LDCC) communi.o $(LLIB3) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

midasgo.exe: midasgo.o $(LIBS)
	$(LDCC) midasgo.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

catal.exe: catal.o $(LIBS)
	$(LDCC) catal.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

newcom.exe: newcom.o $(LIBS)
	$(LDCC) newcom.o $(LLIB1) $(EDITLIBS) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

internal.exe: internal.o $(LIBS)
	$(LDCC) internal.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

internal1.exe: internal1.o $(LIBS)
	$(LDCC) internal1.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

helper.exe: helper.o helpme.o $(LIBS2)
	$(LDCC) helper.o helpme.o $(LLIB2) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

logger.exe: logger.o $(LIBS)
	$(LDCC) logger.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

newcom.out: newcom.in newcom.exe
	$(RM) newcom.bin newcom.out
	LD_LIBRARY_PATH=../lib ./newcom.exe

syskeys.unix: syskeys.datorg 
	$(RM) $@
	cat syskeys.datorg | sed -e 's/^-12345,0,0,1,/2,0,0,1,/' > $@
	chmod ug+w $@

syskeys.vms: syskeys.datorg 
	$(RM) $@
	cat syskeys.datorg | sed -e 's/^-12345,0,0,1,/1,0,0,1,/' > $@
	chmod ug+w $@

clean_exec:
	$(RM) $(OUT)

clean:
	$(RM) $(OBJ)
	$(RM) $(OBJ1)
	$(RM) $(OBJ2)
	$(RM) libprep.a
	$(RM) newcom.out
