/*===========================================================================
  Copyright (C) 1995-2012 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE    C
.IDENTIFICATION  main module MIDASGO
.AUTHOR  K. Banse 	ESO - Garching
.KEYWORDS MIDAS environment, keywords, logfile
.ENVIRONMENT   VMS and UNIX
.PURPOSE
  initialize the system keywords from file SYSKEYS.DAT (ASCII file)
  and write keywords to binary file MONITOR.KEY,
  where MONITOR is the image display station used by MIDAS
  use OSY_TRNLOG to determine actual image system, user + graphics terminal
  and initialize
.ALGORITHM
  the keywords are read from file SYSKEYS.DAT as follows:
  record 1: name / type / length / R
  name = keyword name
  type = I or R or C or D
  length = length of keyword values in longwords (type I or R)
  or bytes (type C) or quadwords (type D)
  R  only, if it is a MIDAS reserved keyword
  the reserved keywords must be the first ones in the file!
  record 2: keyword values in a format according to type

.VERSION  [1.70] 880513: initialize pointers in source code only ...
 120201		last modif

-----------------------------------------------------------------*/
 
#include <osyparms.h>
#include <fileexts.h>
#include <fsydef.h>

/*

*/
 
int main()
 
{
int   status, m;
int   totsize, keysize, datsize;
int   gp;
 
char	keyfile[200];
char	*cpntr, station[4];
char	*osmsg();

 

/*  we do the real work in SCSKIN ...  */

KEYALL.ORIGIN = 0;			/* so SCSKIN knows who we are ... */

#if vms
SCSKIN("MID_MONIT:syskeys.vms");
#else
SCSKIN("MID_MONIT:syskeys.unix");
#endif


totsize = sizeof(struct KEY_ALL);
keysize = (sizeof(struct KEY_STRUCT)) * (KEYALL.LOCENT+1);   /* for security */
datsize = KEYALL.LOCDAT + 2;


/*  create new keyfile in MID_WORK: + store keyword buffers into new file  */
 
status = OSY_TRNLOG("MID_WORK",keyfile,200,&m);  /* Decode default directory */
if (status != ERR_NORMAL) 
   {
   (void) printf("We could not translate MID_WORK\n");
   ospexit(1);
   }
#if vms
if (keyfile[m-1] != FSY_DISKEND)
   {
   if (keyfile[m-1] != FSY_DIREND) keyfile[m++] = FSY_DIREND;
   }
#else
if (keyfile[m-1] != FSY_DIREND) keyfile[m++] = FSY_DIREND;
#endif

OSY_GETSYMB("DAZUNIT",station,4);

(void) strcpy(&keyfile[m],"FORGR  .KEY");
keyfile[m+5] = station[0];
keyfile[m+6] = station[1];
 
gp = osdopen(keyfile,WRITE);
if (gp == -1)
   {
   (void) printf("Problems in creating binary keyword file - %s\n",osmsg());
   ospexit(1);
   }
 
cpntr = (char *) &KEYALL;
status = osdwrite(gp,cpntr,(unsigned int)totsize);
if (status < totsize) goto osd_error;

cpntr = (char *) KEYALL.KEYNAMES;
status = osdwrite(gp,cpntr,(unsigned int)keysize);
if (status < keysize) goto osd_error;

cpntr = (char *) KEYALL.KEYWORDS;
status = osdwrite(gp,cpntr,(unsigned int)datsize);
if (status < 0) goto osd_error;

status = osdclose(gp);
if (status == -1)
   {
   puts(osmsg());
   ospexit(1);
   }
else
   ospexit(0);
 
 
osd_error:
cpntr = (char *) KEYALL.KEYWORDS;	/* write once more to get `oserror' */
status = osdwrite(gp,cpntr,(unsigned int)datsize);

(void) printf("Problems in writing to binary keyfile - %s\n",osmsg());
ospexit(1);
return 0;
}
