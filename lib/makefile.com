$ ! @(#)makefile.com	19.1 (ESO-IPG) 02/25/03 13:52:44 
$ verify = f$verify(1)
$ set noverify
$ set noon
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1987 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    installation procedure in ['MIDASHOME'.'MIDVERS'.LIB]
$ !              Create LIBMIDAS library.
$ ! .REMARKS     Genereted by hand.
$ ! .DATE        910101  New Directory Structure. CG
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! Definitions:
$ !
$ echo == "WRITE SYS$OUTPUT"
$ !
$ ! Start here:
$ !
$ START:
$ on control_y then goto INTERRUPT
$ on control_c then goto INTERRUPT
$ !
$ CREALIB:= "LIBMIDAS"
$ MLIB:= "LIBOS"
$ gosub LIST_DIRS
$ MLIB := "LIBFTOC"
$ gosub LIST_DIRS
$ MLIB := "LIBDIO"
$ gosub LIST_DIRS
$ MLIB := "LIBST"
$ gosub LIST_DIRS
$ MLIB := "LIBTBL"
$ gosub LIST_DIRS
$ !
$ CREALIB:= "LIBGMIDAS"
$ MLIB:= "LIBIDICL"
$ gosub LIST_DIRS
$ MLIB := "LIBAGL3"
$ gosub LIST_DIRS
$ MLIB := "LIBPLOT"
$ gosub LIST_DIRS
$ MLIB := "LIBDSP"
$ gosub LIST_DIRS
$ !
$ if verify .eq. 0 then set noverify
$ exit
$ !
$ INTERRUPT:
$ !delete *.obj.*
$ close lis_file
$ if verify .eq. 0 then set noverify
$ exit
$ !
$ !--------------------------------------------------------------------------
$ ! .NAME        EXTRACT
$ ! .TYPE        DCL subroutine
$ ! .COMMENTS    Extract modules from 'MLIB' and replace them in 'CREALIB'
$ ! .REMARKS
$ ! .DATE	 990111   C.Guirao	Creation
$ !--------------------------------------------------------------------------
$ LIST_DIRS:
$ library/list=lib.lis 'MLIB'
$ open/read lis_file lib.lis
$ !
$ BEGIN:
$ read/end_of_file=EOF lis_file RECORD
$ !
$ ! skip in lib.lis the header, so
$ ! all those lines untill a blank line is found
$ !
$ nn = 'f$length(RECORD)'
$ if nn .ne. 0 then goto BEGIN
$ !
$ BEGINB:
$ !
$ ! Extract a module from 'MLIB' and replace it in 'CREALIB'
$ !
$ read/end_of_file=EOF lis_file RECORD
$ library/extract='RECORD'/output='RECORD'.OBJ 'MLIB'
$ MSG0 = "$ LIB/REPLACE " + CREALIB + " " + RECORD + ".OBJ"
$ library/replace 'CREALIB' 'RECORD'.OBJ
$ echo MSG0
$ delete 'RECORD'.OBJ.*
$ goto BEGINB
$ !
$ EOF:
$ close lis_file
$ return 
$ !--------------------------------------------------------------------------
