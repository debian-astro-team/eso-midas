# .COPYRIGHT:	Copyright (c) 1988-2014 European Southern Observatory,
#						all rights reserved
# .TYPE		make file
# .NAME		$MIDASHOME/$MIDVERS/lib/makefile
# .LANGUAGE	makefile syntax
# .ENVIRONMENT	Unix Systems. 
# .COMMENT	Compiles source files and generates "midaslib.a"
#		and "appliclib.a" libraries
# .REMARKS	
# .AUTHOR	C. Guirao
# .VERSION 1.1	881005:		Implementation
# .VERSION 1.2	910105:		New directory structure. CG
# .VERSION 1.3	910207:		Adding appliclib.a . CG
# .VERSION 2.1	920226:		Creating Shared Libraries form MIDAS and FTOC
# .VERSION 3.1	930304:		Creating Graphical MIDAS Shared Library.
#  
# 131222	last modif

include ../local/default.mk

DEV_NULL =

LIBMIDAS = $(LIBDIR)/libmidas.a 
LIBGMIDAS = $(LIBDIR)/libgmidas.a 
LIBMIDAS_SH = libmidas.$(SH_EXT)
LIBGMIDAS_SH =libgmidas.$(SH_EXT)

LIBS0 = $(LIBDIR)/libos.a \
	$(LIBDIR)/libftoc.a \
	$(LIBDIR)/libdio.a \
	$(LIBDIR)/libst.a \
	$(LIBDIR)/libtbl.a

LIBS1 = $(LIBDIR)/libidicl.a \
	$(LIBDIR)/libagl3.a \
	$(LIBDIR)/libplot.a \
	$(LIBDIR)/libdsp.a

# DEPENDENCIES:
all: $(LIBMIDAS) $(LIBGMIDAS)

$(LIBMIDAS): $(LIBS0)
	rm -f *.o __*
	$(AR) $(AR_XOPT) $(LIBDIR)/libos.a
	rm -f __*
	$(AR) $(AR_XOPT) $(LIBDIR)/libdio.a
	rm -f __*
	$(AR) $(AR_XOPT) $(LIBDIR)/libftoc.a
	rm -f __*
	$(AR) $(AR_XOPT) $(LIBDIR)/libst.a
	rm -f __*
	$(AR) $(AR_XOPT) $(LIBDIR)/libtbl.a
	$(AR) $(AR_OPT) $(LIBMIDAS) *.o
	rm -f __*
	$(RANLIB) $(LIBMIDAS)
## 
## avoid problems with loader of Ubuntu (based) distros from 
## Ubuntu version 13.10 on
## 
##	$(SH_CMD) -o $(LIBMIDAS_SH) *.o $(DEV_NULL)
	$(SH_CMD) -o $(LIBMIDAS_SH) *.o -lm $(F2C_LIBS) $(DEV_NULL)

# for the AltLinux distro we had to change the line above to:
#	$(SH_CMD) -o $(LIBMIDAS_SH) *.o -lm $(DEV_NULL)
# otherwise the linker would always complain about undefined refs for libm
# like sqrt, asin, ...		090122

# because of the same problems with Ubuntu 13.10 we do that 
# now, again!			131220

	rm -f *.o

$(LIBGMIDAS): $(LIBS1)
	rm -f *.o __*
	$(AR) $(AR_XOPT) $(LIBDIR)/libidicl.a
	rm -f __*
	$(AR) $(AR_XOPT) $(LIBDIR)/libagl3.a
	rm -f __*
	$(AR) $(AR_XOPT) $(LIBDIR)/libplot.a
	rm -f __*
	$(AR) $(AR_XOPT) $(LIBDIR)/libdsp.a
	$(AR) $(AR_OPT) $(LIBGMIDAS) *.o
	rm -f __*
	$(RANLIB) $(LIBGMIDAS)
	$(SH_CMD) -o $(LIBGMIDAS_SH) *.o -L$(LIBDIR) $(SLIB) -lmidas $(DEV_NULL)
	rm -f *.o

clean:
	rm  -f *.o *SYMDEF* `ls *.a | fgrep -v libnag.a`
 
