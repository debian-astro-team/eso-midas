C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  YSTF.FOR +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module YSTF.FOR
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C 
C 060324	last modif
C -----------------------------------------------------------------------------
C

      SUBROUTINE TBCINI(TID,DTYP,ALEN,FORM,TUNIT,LABEL,COL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   FORM, TUNIT, LABEL
      INTEGER    TID, DTYP, ALEN, COL
      INTEGER    STATUS
C 
      CALL STSTR(1,FORM)                     !STRIPPED_STRING
      CALL STSTR(2,TUNIT)                    !STRIPPED_STRING
      CALL STSTR(3,LABEL)                    !STRIPPED_STRING
C 
      CALL TBC1(TID,DTYP,ALEN,COL,STATUS)
C
      RETURN
      END
C
C 
      SUBROUTINE TBCMAP(TID,COL,PNTR,STATUS)
C
      IMPLICIT NONE
C
      INTEGER    TID, COL
      INTEGER*8  PNTR
      INTEGER    STATUS
C 
      CALL TBC2(TID,COL,PNTR,STATUS)
C
      RETURN
      END
C
C
      SUBROUTINE TBCSEL(TID,TEXT,MAX_COLS,COLS,FLAGS,FOUND_COLS,STATUS)
C 
      IMPLICIT NONE
C
      CHARACTER*(*)   TEXT
      INTEGER     TID, MAX_COLS,COLS,FLAGS,FOUND_COLS
      INTEGER     STATUS
C
      CALL STSTR(1,TEXT)                     !STRIPPED_STRING
      CALL TBC3(TID,MAX_COLS,COLS,FLAGS,FOUND_COLS,STATUS)
C
      RETURN
      END
C 
C 
      SUBROUTINE TBCSER(TID,LABEL,COL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   LABEL
      INTEGER     TID, COL
      INTEGER     STATUS
C
      CALL STSTR(1,LABEL)                     !STRIPPED_STRING
      CALL TBC4(TID,COL,STATUS)
C
      RETURN
      END

CC SUBROUTINE TBCDEL(TID,COL,NCOL,STATUS)
CC directly done in tbc.fc ...

CC SUBROUTINE ROUTINE TBCSRT(TID,NC,COL,SORTFL,STATUS)
CC directly done in tbc.fc ...

