/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  stk.fc +++++++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module stk.fc
.COMMENTS
Module contains layer between the keyword related FORTRAN STxxxx interfaces
and the SC_interfaces written in (hopefully independent) C
.AUTHOR         K. Banse  	ESO - Garching
.KEYWORDS       standard interfaces.
.ENVIRONMENT    FORTRAN and C standards
.VERSION  [1.00] 871207:  created from SXFTOC.C
.VERSION  [2.60] 880411:  modified new version - the last one
.VERSION  [2.70] 900316:  fix problems with TEXT_LEN+2

 090331		last modif
-----------------------------------------------------------------------------*/


#include <midas_def.h>

#include <stdlib.h>


static int  kunit = 0, knull = -1;

int   mm;
char *ptr1, *ptr2, *ptr3, *ptr4;
char *loc_pntr();
char *strp_pntr();

/*

*/

/*==========================================================================*/

/*** stat = SCKRDI(key,felem,maxvals,actvals,values,kunit,knull) ***/

ROUTINE STK1(felem,maxvals,actvals,values,status)
int  *felem;         /* IN : first data item to be read */
int  *maxvals;       /* IN : max. vals to read */
int  *actvals;       /* OUT: actual no. of elements returned */
int  *values;        /* OUT: buffer for data values */
int  *status;

{
ptr1 = strp_pntr(1);            /* get stripped string of "key" */

*status = SCKRDI(ptr1,*felem,*maxvals,actvals,values,&kunit,&knull);

return 0;
}


/*==========================================================================*/

/*** stat = SCKRDR(key,felem,maxvals,actvals,values,kunit,knull) ***/

ROUTINE STK2(felem,maxvals,actvals,values,status)
int  *felem;       /* IN : first data item to be read */
int  *maxvals;     /* IN : max. vals to read */
int  *actvals;     /* OUT: actual no. of elements returned */
float  *values;    /* OUT: buffer for data values */
int  *status;

{
ptr1 = strp_pntr(1);            /* get stripped string of "key" */

*status = SCKRDR(ptr1,*felem,*maxvals,actvals,values,&kunit,&knull);

return 0;
}


/*==========================================================================*/

/*** stat = SCKRDD(key,felem,maxvals,actvals,values,kunit,knull) ***/

ROUTINE STK3(felem,maxvals,actvals,values,status)
int  *felem;        /* IN : first data item to be read */
int  *maxvals;      /* IN : max. vals to read */
int  *actvals;      /* OUT: actual no. of elements returned */
double  *values;    /* OUT: buffer for data values */
int  *status;

{
ptr1 = strp_pntr(1);            /* get stripped string of "key" */

*status = SCKRDD(ptr1,*felem,*maxvals,actvals,values,&kunit,&knull);

return 0;
}


/*==========================================================================*/

/*** stat = SCKRDC(key,noelm,felem,maxvals,actvals,values,kunit,knull) ***/

ROUTINE STK4(noelm,felem,maxvals,actvals,status)
int  *noelm;     /* IN : no. of bytes (chars) per element */
int  *felem;         /* IN : first data item to be read */
int  *maxvals;       /* IN : max. vals to read */
int  *actvals;       /* OUT: actual no. of elements returned */
int  *status;

{
int  n;

ptr1 = strp_pntr(1);		/* get stripped string of "key" */
ptr2 = loc_pntr(1,&mm);		/* get location of "values" */

*status = SCKRDC(ptr1,*noelm,*felem,*maxvals,actvals,ptr2,&kunit,&knull);

n = *actvals;
if ((n > 0) && (n < mm)) *(ptr2+n) = ' ';

return 0;
}


/*==========================================================================*/

/*** stat = SCKPRC(prompt,key,noelm,felem,maxvals,actvals,values,kunit,knull) ***/

ROUTINE STK5(noelm,felem,maxvals,actvals,status)
int  *noelm;     /* IN : no. of bytes (chars) per element */
int  *felem;     /* IN : first data item to be read */
int  *maxvals;   /* IN : max. vals to read */
int  *actvals;   /* OUT: actual no. of elements returned */
int  *status;

{
int  n;

ptr1 = strp_pntr(1);		/* get stripped string of "key" */
ptr1 = strp_pntr(1);			/* get stripped string of "key" */
ptr2 = strp_pntr(2);			/* get stripped string of "prompt" */
ptr3 = loc_pntr(1,&mm);			/* get location of "values" */


*status = SCKPRC(ptr2,ptr1,*noelm,*felem,*maxvals,actvals,ptr3,&kunit,&knull);

n = *actvals;
if ((n > 0) && (n < mm)) *(ptr3+n) = ' ';

return 0;
}


/*==========================================================================*/

/*** stat = SCKPRI(prompt,key,felem,maxvals,actvals,values,kunit,knull) ***/

ROUTINE STK6(felem,maxvals,actvals,values,status)
int  *felem;         /* IN : first data item to be read */
int  *maxvals;       /* IN : max. vals to read */
int  *actvals;       /* OUT: actual no. of elements returned */
int  *values;        /* OUT: buffer for data values */
int  *status;

{
ptr1 = strp_pntr(1);			/* get stripped string of "key" */
ptr2 = strp_pntr(2);			/* get stripped string of "prompt" */
 
*status = SCKPRI(ptr2,ptr1,*felem,*maxvals,actvals,values,&kunit,&knull);

return 0;
}


/*==========================================================================*/

/*** stat = SCKPRR(prompt,key,felem,maxvals,actvals,values,kunit,knull) ***/

ROUTINE STK7(felem,maxvals,actvals,values,status)
int  *felem;         /* IN : first data item to be read */
int  *maxvals;       /* IN : max. vals to read */
int  *actvals;       /* OUT: actual no. of elements returned */
float  *values;      /* OUT: buffer for data values */
int  *status;

{
ptr1 = strp_pntr(1);			/* get stripped string of "key" */
ptr2 = strp_pntr(2);			/* get stripped string of "prompt" */
 
*status = SCKPRR(ptr2,ptr1,*felem,*maxvals,actvals,values,&kunit,&knull);

return 0;
}

/*==========================================================================*/

/*** stat = SCKPRD(prompt,key,felem,maxvals,actvals,values,kunit,knull) ***/

ROUTINE STK8(felem,maxvals,actvals,values,status)
int  *felem;         /* IN : first data item to be read */
int  *maxvals;       /* IN : max. vals to read */
int  *actvals;       /* OUT: actual no. of elements returned */
double  *values;     /* OUT: buffer for data values */
int  *status;

{
ptr1 = strp_pntr(1);			/* get stripped string of "key" */
ptr2 = strp_pntr(2);			/* get stripped string of "prompt" */

*status = SCKPRD(ptr2,ptr1,*felem,*maxvals,actvals,values,&kunit,&knull);

return 0;
}

/*==========================================================================*/

/*** stat = SCKWRC(key,noelm,values,felem,maxvals,kunit)  ***/

ROUTINE STK9(noelm,felem,maxvals,status)
int *noelm;     /* IN: no. of chars (bytes) per data value */
int  *felem;         /* IN : first data item to be read */
int  *maxvals;       /* IN : max. vals to read */
int  *status;

{
ptr1 = strp_pntr(1);                    /* get stripped string of "key" */
ptr2 = loc_pntr(1,&mm);                 /* get location of "values" */

*status = SCKWRC(ptr1,*noelm,ptr2,*felem,*maxvals,&kunit);

return 0;
}

/*==========================================================================*/

/*** stat = SCKWRI(key,values,felem,maxvals,kunit)  ***/

ROUTINE STK10(values,felem,maxvals,status)
int  *values;     /* IN : buffer with data values */
int  *felem;      /* IN : first data item to be read */
int  *maxvals;    /* IN : max. vals to read */
int  *status;

{
ptr1 = strp_pntr(1);                    /* get stripped string of "key" */

*status = SCKWRI(ptr1,values,*felem,*maxvals,&kunit);

return 0;
}

/*==========================================================================*/

/*** stat = SCKWRD(key,values,felem,maxvals,kunit)  ***/

ROUTINE STK11(values,felem,maxvals,status)
double	*values;  /* IN : buffer with data values */
int  *felem;      /* IN : first data item to be read */
int  *maxvals;    /* IN : max. vals to read */
int  *status;

{
ptr1 = strp_pntr(1);                    /* get stripped string of "key" */

*status = SCKWRD(ptr1,values,*felem,*maxvals,&kunit);

return 0;
}

/*==========================================================================*/

/*** stat = SCKWRR(key,values,felem,maxvals,kunit)  ***/

ROUTINE STK12(values,felem,maxvals,status)
float  *values;   /* IN : buffer with data values */
int  *felem;      /* IN : first data item to be read */
int  *maxvals;    /* IN : max. vals to read */
int  *status;

{
ptr1 = strp_pntr(1);                    /* get stripped string of "key" */

*status = SCKWRR(ptr1,values,*felem,*maxvals,&kunit);

return 0;
}

/*==========================================================================*/

/*** stat = SCKFND(key,type,noelem,bytelem,status)  ***/

ROUTINE STK13(noelem,bytelem,status)
int  *noelem;        /* OUT: no. of elements  */
int  *bytelem;       /* OUT: no. of bytes per element  */
int  *status;

{
int  n;

ptr1 = strp_pntr(1);		/* get stripped string of "key" */
ptr1 = strp_pntr(1);                    /* get stripped string of "key" */
ptr2 = loc_pntr(1,&mm);                 /* get location of "type" */

*status = SCKFND(ptr1,ptr2,noelem,bytelem);

n = (int) strlen(ptr2);
if ((n > 0) && (n < mm)) *(ptr2+n) = ' ';

return 0;
}

/*==========================================================================*/

/*** stat = SCKINF(npos,field,buf,numbuf)  ***/

ROUTINE STK14(npos,field,numbuf,status)
int  *npos;      /* IN : position of keyword */
int  *field;     /* IN :specifies desired info, */
                 /*     1 = NAME, 2 = TYPE, 3 = SIZE  */
int  *numbuf;    /* IN : max. length of buffer above  */
int  *status;

{
int  n;

ptr1 = loc_pntr(1,&mm);                 /* get location of "buf" */

n = *field;
*status = SCKINF(*npos,*field,ptr1,(mm-1),numbuf);

if (n != 3)                    /* treat char. result buffer */
   {
   n = (int) strlen(ptr1);
   if ((n > 0) && (n < mm)) *(ptr1+n) = ' ';
   }

return 0;
}



