C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  TBD.FOR +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module TBD.FOR
C  AUTHOR         K. Banse
C  Module contains layer between the table related FORTRAN TBxxxx interfaces
C  and the TC_interfaces written in (hopefully independent) C
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C 
C 060328	last modif
C 
C -----------------------------------------------------------------------------
C
      SUBROUTINE TBFGET(TID,COLUMN,FORM,LEN,DTYPE,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   FORM
      INTEGER     TID, COLUMN, LEN, DTYPE
      INTEGER     STATUS
C 
      CALL STLOC(1,1,FORM)                      !blanked CHAR_LOC
C 
      CALL TBF1(TID, COLUMN, LEN, DTYPE, STATUS)
C
      RETURN
      END
C 
      SUBROUTINE TBFPUT(TID,COLUMN,FORM,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   FORM
      INTEGER     TID, COLUMN
      INTEGER     STATUS
C
      CALL STSTR(1,FORM)                        !STRIPPED_STRING
C
      CALL TBF2(TID, COLUMN, STATUS)
C
      RETURN
      END
C 
C 
      SUBROUTINE TBLGET(TID, COL, LABEL, STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   LABEL
      INTEGER     TID, COL 
      INTEGER     STATUS
C
      CALL STLOC(1,1,LABEL)                      !blanked CHAR_LOC
C 
      CALL TBL1(TID, COL, STATUS)
C
      RETURN
      END

C
C
      SUBROUTINE TBLPUT(TID, COL, LABEL, STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   LABEL
      INTEGER     TID, COL
      INTEGER     STATUS
C
      CALL STSTR(1,LABEL)                        !STRIPPED_STRING
C
      CALL TBL2(TID, COL, STATUS)
C
      RETURN
      END
C 
C 
      SUBROUTINE TBLSER(TID, LABEL, COL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   LABEL
      INTEGER     TID, COL
      INTEGER     STATUS
C
      CALL STSTR(1,LABEL)                        !STRIPPED_STRING
C 
      CALL TBL3(TID, COL, STATUS)
C
      RETURN
      END
C
C
      SUBROUTINE TBUGET(TID,COL,TUNIT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   TUNIT
      INTEGER     TID, COL
      INTEGER     STATUS
C
      CALL STLOC(1,1,TUNIT)                      !blanked CHAR_LOC
C
      CALL TBU1(TID,COL,STATUS)
C
      RETURN
      END
C
C
      SUBROUTINE TBUPUT(TID,COL,TUNIT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   TUNIT
      INTEGER     TID, COL
      INTEGER     STATUS
C
      CALL STSTR(1,TUNIT)                        !STRIPPED_STRING
C
      CALL TBU2(TID,COL,STATUS)
C
      RETURN
      END



