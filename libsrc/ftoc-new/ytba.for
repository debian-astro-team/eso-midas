C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  TBA.FOR +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module TBA.FOR
C  AUTHOR         M. Peron (ESO-IPG))
C  Module contains layer between the table related FORTRAN TBxxxx interfaces
C  and the TC_interfaces written in (hopefully independent) C
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C -----------------------------------------------------------------------------
C

      SUBROUTINE TBAWRC(TID,ROW,COL,INDEX,ITEMS,VALUE,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   VALUE
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     STATUS
C 
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBWRC(ITEMS,VALUE,STATUS)
C
      RETURN
      END
C
      SUBROUTINE TBAWRD(TID, ROW, COL, INDEX, ITEMS, VALUE,STATUS)
C
      IMPLICIT NONE
C
      DOUBLE PRECISION   VALUE(*)
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBWRD(ITEMS,VALUE,STATUS)
C
      RETURN
      END
C

      SUBROUTINE TBAWRI(TID, ROW, COL, INDEX, ITEMS, VALUE,STATUS)
C
      IMPLICIT NONE
C
      INTEGER   VALUE(*)
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBWRI(ITEMS,VALUE,STATUS)
C
      RETURN
      END
C
      SUBROUTINE TBAWRR(TID, ROW, COL, INDEX, ITEMS, VALUE,STATUS)
C
      IMPLICIT NONE
C
      REAL   VALUE(*)
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBWRR(ITEMS,VALUE,STATUS)
C
      RETURN
      END
C

      SUBROUTINE TBARDC(TID, ROW, COL, INDEX, ITEMS, VALUE,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   VALUE
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBRDC(ITEMS,VALUE,STATUS)
C
      RETURN
      END
C

      SUBROUTINE TBARDD(TID, ROW, COL, INDEX, ITEMS, VALUE,STATUS)
C
      IMPLICIT NONE
C
      DOUBLE PRECISION   VALUE(*)
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBRDD(ITEMS,VALUE,STATUS)
C
      RETURN
      END
C
      SUBROUTINE TBARDI(TID, ROW, COL, INDEX, ITEMS, VALUE,STATUS)
C
      IMPLICIT NONE
C
      INTEGER   VALUE(*)
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBRDI(ITEMS,VALUE,STATUS)
C
      RETURN
      END
C

      SUBROUTINE TBARDR(TID, ROW, COL, INDEX, ITEMS, VALUE,STATUS)
C
      IMPLICIT NONE
C
      REAL   VALUE(*)
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBRDR(ITEMS,VALUE,STATUS)
C
      RETURN
      END
C

      SUBROUTINE TBASCC(TID,ROW,COL,INDEX,ITEMS,VALUE,NEXT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*) VALUE
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     NEXT, STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBSCC(ITEMS,VALUE,NEXT,STATUS)
C
      RETURN
      END
C

      SUBROUTINE TBASCD(TID,ROW,COL,INDEX,ITEMS,VALUE,NEXT,STATUS)
C
      IMPLICIT NONE
C
      DOUBLE PRECISION   VALUE(*)
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     NEXT, STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBSCD(ITEMS,VALUE,NEXT,STATUS)
C
      RETURN
      END
C

      SUBROUTINE TBASCI(TID,ROW,COL,INDEX,ITEMS,VALUE,NEXT,STATUS)
C
      IMPLICIT NONE
C
      INTEGER   VALUE(*)
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     NEXT, STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBSCI(ITEMS,VALUE,NEXT,STATUS)
C
      RETURN
      END
C

      SUBROUTINE TBASCR(TID,ROW,COL,INDEX,ITEMS,VALUE,NEXT,STATUS)
C
      IMPLICIT NONE
C
      REAL   VALUE(*)
      INTEGER     TID, ROW, COL, INDEX, ITEMS
      INTEGER     NEXT, STATUS
C
      CALL TBA1(TID,ROW,COL,INDEX)
      CALL TBSCR(ITEMS,VALUE,NEXT,STATUS)
C
      RETURN
      END
C

