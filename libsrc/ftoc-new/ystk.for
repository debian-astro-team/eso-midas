C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  ystk.for +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module ystk.for
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C 
C 060330	last modif
C -----------------------------------------------------------------------------
C
      SUBROUTINE STKRDI(KEY,FELEM,MAXVALS,ACTVALS,VALUES,KUNIT,
     +                  KNULL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KEY        
      INTEGER     FELEM, MAXVALS, ACTVALS
      INTEGER     VALUES(*)
      INTEGER     KUNIT, KNULL
C 
      KUNIT = 0
      KNULL =-1
C 
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
C 
      CALL STK1(FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C
      SUBROUTINE STKRDR(KEY,FELEM,MAXVALS,ACTVALS,VALUES,KUNIT,
     +                  KNULL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KEY        
      INTEGER     FELEM, MAXVALS, ACTVALS
      REAL     VALUES(*)
      INTEGER     KUNIT, KNULL
C 
      KUNIT = 0
      KNULL = 0
C 
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
C
      CALL STK2(FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C
      SUBROUTINE STKRDD(KEY,FELEM,MAXVALS,ACTVALS,VALUES,KUNIT,
     +                  KNULL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KEY        
      INTEGER     FELEM, MAXVALS, ACTVALS
      DOUBLE PRECISION     VALUES(*)
      INTEGER     KUNIT, KNULL
C 
      KUNIT = 0
      KNULL = 0
C 
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
C
      CALL STK3(FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C 
C
      SUBROUTINE STKRDC(KEY,NOELM,FELEM,MAXVALS,ACTVALS,VALUES,KUNIT,
     +                  KNULL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KEY, VALUES
      INTEGER     NOELM, FELEM, MAXVALS, ACTVALS
      INTEGER     KUNIT, KNULL
C 
      KUNIT = 0
      KNULL = 0
C 
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
      CALL STLOC(1,1,VALUES)                      !blanked CHAR_LOC
C
      CALL STK4(NOELM,FELEM,MAXVALS,ACTVALS,STATUS)
C
      RETURN
      END
C 
C 
      SUBROUTINE STKPRC(PROMPT,KEY,NOELM,FELEM,MAXVALS,ACTVALS,VALUES,
     +                  KUNIT,KNULL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   PROMPT, KEY, VALUES
      INTEGER     NOELM, FELEM, MAXVALS, ACTVALS
      INTEGER     KUNIT, KNULL
C 
      KUNIT = 0
      KNULL = 0
C 
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
      CALL STSTR(2,PROMPT)                        !STRIPPED_STRING
      CALL STLOC(1,1,VALUES)                      !blanked CHAR_LOC
C
      CALL STK5(NOELM,FELEM,MAXVALS,ACTVALS,STATUS)
C
      RETURN
      END
C 
C
C
      SUBROUTINE STKPRI(PROMPT,KEY,FELEM,MAXVALS,ACTVALS,VALUES,
     +                  KUNIT,KNULL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   PROMPT, KEY
      INTEGER     FELEM, MAXVALS, ACTVALS
      INTEGER   VALUES(*)
      INTEGER     KUNIT, KNULL
C
      KUNIT = 0
      KNULL = 0
C
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
      CALL STSTR(2,PROMPT)                        !STRIPPED_STRING
C
      CALL STK6(FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C
C
C
      SUBROUTINE STKPRR(PROMPT,KEY,NOELM,FELEM,MAXVALS,ACTVALS,VALUES,
     +                  KUNIT,KNULL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   PROMPT, KEY
      INTEGER     FELEM, MAXVALS, ACTVALS
      REAL        VALUES(*)
      INTEGER     KUNIT, KNULL
C
      KUNIT = 0
      KNULL = 0
C
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
      CALL STSTR(2,PROMPT)                        !STRIPPED_STRING
C
      CALL STK7(FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C
C
C
      SUBROUTINE STKPRD(PROMPT,KEY,NOELM,FELEM,MAXVALS,ACTVALS,VALUES,
     +                  KUNIT,KNULL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   PROMPT, KEY
      INTEGER     FELEM, MAXVALS, ACTVALS
      DOUBLE PRECISION   VALUES(*)
      INTEGER     KUNIT, KNULL
C
      KUNIT = 0
      KNULL = 0
C
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
      CALL STSTR(2,PROMPT)                        !STRIPPED_STRING
C
      CALL STK8(FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C
      SUBROUTINE STKWRC(KEY,NOELM,VALUES,FELEM,MAXVALS,KUNIT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KEY, VALUES
      INTEGER     NOELM, FELEM, MAXVALS
      INTEGER     KUNIT
C
      KUNIT = 0
C 
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
      CALL STLOC(1,0,VALUES)                      !untouched CHAR_LOC
C 
      CALL STK9(NOELM,FELEM,MAXVALS,STATUS)

C
      RETURN
      END
C 
      SUBROUTINE STKWRI(KEY,VALUES,FELEM,MAXVALS,KUNIT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KEY
      INTEGER     FELEM, MAXVALS
      INTEGER   VALUES(*)
      INTEGER     KUNIT
C
      KUNIT = 0
C
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
C
      CALL STK10(VALUES,FELEM,MAXVALS,STATUS)
C
      RETURN
      END
C
      SUBROUTINE STKWRD(KEY,VALUES,FELEM,MAXVALS,KUNIT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KEY
      INTEGER     FELEM, MAXVALS
      DOUBLE PRECISION   VALUES(*)
      INTEGER     KUNIT
C
      KUNIT = 0
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
C
      CALL STK11(VALUES,FELEM,MAXVALS,STATUS)
C
      RETURN
      END
C
      SUBROUTINE STKWRR(KEY,VALUES,FELEM,MAXVALS,KUNIT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KEY
      INTEGER     FELEM, MAXVALS
      REAL        VALUES(*)
      INTEGER     KUNIT
C
      KUNIT = 0
C
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
C
      CALL STK12(VALUES,FELEM,MAXVALS,STATUS)
C
      RETURN
      END
C 
      SUBROUTINE STKFND(KEY,CTYPE,NOELEM,BYTELEM,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KEY, CTYPE
      INTEGER     NOELEM, BYTELEM, STATUS
C 
      CALL STSTR(1,KEY)                           !STRIPPED_STRING
      CALL STLOC(1,1,CTYPE)                       !blanked CHAR_LOC
C 
      CALL STK13(NOELEM,BYTELEM,STATUS)
C
      RETURN
      END
C
      SUBROUTINE STKINF(NPOS,FIELD,BUF,NUMBUF,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   BUF
      INTEGER     FIELD, NUMBUF, STATUS
C
      CALL STLOC(1,1,BUF)                         !blanked CHAR_LOC
C
      CALL STK14(NPOS,FIELD,NUMBUF,STATUS)
C
      RETURN
      END

