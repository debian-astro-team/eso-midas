C===========================================================================
C Copyright (C) 1995-2008 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  ygen.for +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module ygen.for
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C 
C 080722		last modif
C -----------------------------------------------------------------------------
C
      SUBROUTINE GENCNV(TEXT,TYPE,MAXVAL,IBUF,RBUF,DBUF,ACTVAL)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  TEXT
      INTEGER     TYPE,MAXVAL
      INTEGER     IBUF(*)
      REAL        RBUF(*)
      DOUBLE PRECISION   DBUF(*)
      INTEGER     ACTVAL
C 
      CALL STSTR(1,TEXT)                          !STRIPPED_STRING
C 
      CALL STGN1(TYPE,MAXVAL,IBUF,RBUF,DBUF,ACTVAL)
C
      RETURN
      END
C
C 
      SUBROUTINE GENBLD(SIMNO,NAME,DATFORM,SIZE,IMNO,CLONED,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     SIMNO
      CHARACTER*(*)  NAME
      INTEGER     DATFORM,SIZE,IMNO,CLONED
      INTEGER     STATUS
C
      CALL STSTR(1,NAME)                          !STRIPPED_STRING
C 
      CALL STGN2(SIMNO,DATFORM,SIZE,IMNO,CLONED,STATUS)
C
      RETURN
      END
C
C 
      INTEGER FUNCTION GENNUM(TEXT)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  TEXT
      INTEGER     STATUS
C
      CALL STSTR(1,TEXT)                          !STRIPPED_STRING
C
      CALL STGN3(STATUS)
      GENNUM = STATUS
C
      RETURN
      END
C
C 
      SUBROUTINE GENLGN(INFILE,OUTFILE,MAXOUT)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  INFILE, OUTFILE
      INTEGER     MAXOUT
C
      CALL STSTR(1,INFILE)                          !STRIPPED_STRING
      CALL STLOC(1,1,OUTFILE)                       !blanked CHAR_LOC
C
      CALL STGN4(MAXOUT)
C
      RETURN
      END
C
C 
      SUBROUTINE GENTIM(TIME)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  TIME
      INTEGER     STATUS
C
      CALL STLOC(1,1,TIME)                          !blanked CHAR_LOC
C
      CALL STGN5(STATUS)
C
      RETURN
      END
C
C 
      SUBROUTINE GENEQF(AFILE,BFILE,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  AFILE, BFILE
      INTEGER     STATUS
C
      CALL STSTR(1,AFILE)                          !STRIPPED_STRING
      CALL STSTR(2,BFILE)                         !STRIPPED_STRING
C
      CALL STGN6(STATUS)
C
      RETURN
      END
C
C 
      SUBROUTINE GENLEN(STRUNG,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  STRUNG
      INTEGER     STATUS
C
      CALL STSTR(1,STRUNG)                          !STRIPPED_STRING
C
      CALL STGN7(STATUS)
C
      RETURN
      END
C
C 
      SUBROUTINE ISSUBF(INFILE,INDX)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  INFILE
      INTEGER     INDX
C
      CALL STSTR(1,INFILE)                          !STRIPPED_STRING
C
      CALL STGN8(INDX)
C
      RETURN
      END
C
C 
      SUBROUTINE DSCUPT(INNO,OUTNO,HISTORY,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)  HISTORY
      INTEGER     INNO,OUTNO,STATUS
C
      CALL STSTR(1,HISTORY) 
C
      CALL STGN9(INNO,OUTNO,STATUS)
C
      RETURN
      END
C

