/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  sti.fc +++++++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module sti.fc
.COMMENTS
Module contains layer between the Image realted FORTRAN STxxxx interfaces
and the SC_interfaces written in (hopefully independent) C
.AUTHOR         K. Banse  	ESO - Garching
.KEYWORDS       standard interfaces.
.ENVIRONMENT    FORTRAN and C standards
.VERSION  [1.00] 871207:  created from SXFTOC.C
.VERSION  [2.60] 880411:  modified new version - the last one
.VERSION  [2.70] 880919:  use STRING_FCOPY to get character stuff back
.VERSION  [2.80] 881028:  START + STEP are now double
.VERSION  [2.90] 900316:  fix problems with FNAME_LEN+2
.VERSION  [3.00] 901213:  Master file. CG.
.VERSION  [3.01] 911217:  SCIGET needs to fill temporary strings. CG.

 090611		last modif
-----------------------------------------------------------------------------*/

#include <ftoc_comm.h>
#include <midas_def.h>
	

int   mm;

char *ptr1, *ptr2, *ptr3, *ptr4;
char *loc_pntr();
char *strp_pntr();

/*

*/

/*==========================================================================*/

/*** stat = SCIGET(name,dattype,iomode,filtype,maxdim,naxis,npix,
            start,step,ident,cunit,pntr,no)                        ***/

ROUTINE STI1(dattype,iomode,filtype,maxdim,naxis,npix,start,step,pntr,no,status)
int  *dattype;  /* IN : data type */
int  *iomode;   /* IN : mode for opening of frame */
int  *filtype;  /* IN : type no: 1 = image, 2 = mask, 3 = table  */
int  *maxdim;   /* IN : maximum number of dimensions */
int  *naxis;	/* OUT : naxis */
int  *npix;	/* OUT : no. of pixels per axis */
double *start;	/* OUT: start coordinates: start is the address of an */
		/*      array of naxis elements */
double *step; 	/* OUT: stepsizes : step is the address of an array of*/
		/*      naxis elements */
long int *pntr; /* OUT : */
int  *no;	/* OUT: file no. of data frame */
int  *status;

{	
char *mypntr;

int  n, m1, m2;
int  lowmadr;
long int  diff;

ptr1 = strp_pntr(1);			/* get stripped string of "name" */
ptr2 = loc_pntr(1,&m1);			/* get location of "ident" */
ptr3 = loc_pntr(2,&m2);			/* get location of "cunit" */

*status = SCIGET(ptr1,*dattype,*iomode,*filtype,*maxdim,
                 naxis,npix,start,step,ptr2,ptr3,&mypntr,no);

lowmadr = (int) IS_HIGH(mypntr);
diff = (long int) COMMON_INDEX(mypntr);
if (lowmadr == 1)
   *pntr = diff;
else
   *pntr = -diff;

n = (int) strlen(ptr2);
if ((n > 0) && (n < m1-1)) *(ptr2+n) = ' ';

n = (int) strlen(ptr3);
if ((n > 0) && (n < m2-1)) *(ptr3+n) = ' ';

return 0;
}

/*==========================================================================*/

/*** stat = SCIPUT(name,dattype,iomode,filtype,naxis,npix,
            start,step,ident,cunit,pntr,no)                        ***/

ROUTINE STI2(dattype,iomode,filtype,naxis,npix,start,step,pntr,no,status)
int  *dattype;  /* IN : data type */
int  *iomode;   /* IN : mode for opening of frame */
int  *filtype;  /* IN : type no: 1 = image, 2 = mask, 3 = table  */
int  *naxis;    /* IN : naxis */
int  *npix;     /* IN : no. of pixels per axis */
double *start;  /* IN: start coordinates: start is the address of an */
                /*      array of naxis elements */
double *step;   /* IN: stepsizes : step is the address of an array of*/
                /*      naxis elements */
long int *pntr; /* OUT : */
int  *no;       /* OUT: file no. of data frame */
int  *status;

{
char *mypntr;

int  lowmadr;
long int  diff;


ptr1 = strp_pntr(1);			/* get stripped string of "name" */
ptr2 = strp_pntr(2);			/* get stripped string of "ident" */
ptr3 = strp_pntr(3);			/* get stripped string of "cunit" */

*status = SCIPUT(ptr1,*dattype,*iomode,*filtype,
                *naxis,npix,start,step,ptr2,ptr3,&mypntr,no);


lowmadr = (int) IS_HIGH(mypntr);
diff = (long int) COMMON_INDEX(mypntr);
if (lowmadr == 1)
   *pntr = diff;
else
   *pntr = -diff;

return 0;
}

