C===========================================================================
C Copyright (C) 1995-2009 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  ystd.for +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module ystd.for
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  
C 
C 090323	last modif
C -----------------------------------------------------------------------------
C
      SUBROUTINE STDRDI(NO,DESCR,FELEM,MAXVALS,ACTVALS,VALUES,DUNIT,
     +                  DNULL,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR        
      INTEGER     FELEM, MAXVALS, ACTVALS
      INTEGER     VALUES(*)
      INTEGER     DUNIT, DNULL
      INTEGER    STATUS
C 
      DUNIT = 0
      DNULL = 0
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C 
      CALL STD1(NO,FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C


      SUBROUTINE STDRDL(NO,DESCR,FELEM,MAXVALS,ACTVALS,VALUES,DUNIT,
     +                  DNULL,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR        
      INTEGER     FELEM, MAXVALS, ACTVALS
      INTEGER     VALUES(*)
      INTEGER     DUNIT, DNULL
      INTEGER    STATUS
C 
      DUNIT = 0
      DNULL = 0
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C
      CALL STD2(NO,FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C
C 
      SUBROUTINE STDRDR(NO,DESCR,FELEM,MAXVALS,ACTVALS,VALUES,DUNIT,
     +                  DNULL,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR        
      INTEGER     FELEM, MAXVALS, ACTVALS
      REAL     VALUES(*)
      INTEGER     DUNIT, DNULL
      INTEGER    STATUS
C 
      DUNIT = 0
      DNULL = 0
C 
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C
      CALL STD3(NO,FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C
C 
      SUBROUTINE STDRDD(NO,DESCR,FELEM,MAXVALS,ACTVALS,VALUES,DUNIT,
     +                  DNULL,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR        
      INTEGER     FELEM, MAXVALS, ACTVALS
      DOUBLE PRECISION     VALUES(*)
      INTEGER     DUNIT, DNULL
      INTEGER    STATUS
C 
      DUNIT = 0
      DNULL = 0
C 
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C
      CALL STD4(NO,FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C 
C
      SUBROUTINE STDRDS(NO,DESCR,FELEM,MAXVALS,ACTVALS,VALUES,DUNIT,
     +                  DNULL,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR        
      INTEGER     FELEM, MAXVALS, ACTVALS
      INTEGER*8     VALUES(*)
      INTEGER     DUNIT, DNULL
      INTEGER    STATUS
C 
      DUNIT = 0
      DNULL = 0
C 
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C
      CALL STD4a(NO,FELEM,MAXVALS,ACTVALS,VALUES,STATUS)
C
      RETURN
      END
C 
C
      SUBROUTINE STDRDC(NO,DESCR,NOELM,FELEM,MAXVALS,ACTVALS,VALUES,
     +                  DUNIT,DNULL,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR, VALUES
      INTEGER     NOELM, FELEM, MAXVALS, ACTVALS
      INTEGER     DUNIT, DNULL
      INTEGER    STATUS
C 
      DUNIT = 0
      DNULL = 0
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
      CALL STLOC(1,1,VALUES)                       !blanked CHAR_LOC
C
      CALL STD5(NO,NOELM,FELEM,MAXVALS,ACTVALS,STATUS)
C
      RETURN
      END
C 
C
      SUBROUTINE STDRDH(NO,DESCR,FELEM,MAXVALS,ACTVALS,VALUES,
     +                  TOTAL,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR, VALUES
      INTEGER     TOTAL, FELEM, MAXVALS, ACTVALS
      INTEGER    STATUS
C 
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
      CALL STLOC(1,1,VALUES)                       !blanked CHAR_LOC
C
      CALL STD6(NO,FELEM,MAXVALS,ACTVALS,TOTAL,STATUS)
C
      RETURN
      END
C 

      SUBROUTINE STDFND(NO,DESCR,CTYPE,NOELEM,BYTELEM,STATUS)
C 
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR, CTYPE
      INTEGER     NOELEM, BYTELEM
      INTEGER     STATUS
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
      CALL STLOC(1,1,CTYPE)                        !blanked CHAR_LOC
C
      CALL STD7(NO,NOELEM,BYTELEM,STATUS)
C
      RETURN
      END
C
C 
      SUBROUTINE STDINF(NO,NPOS,FIELD,BUF,NUMBUF,STATUS)
C 
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   BUF
      INTEGER     NPOS, FIELD, NUMBUF
      INTEGER     STATUS
C
      CALL STLOC(1,1,BUF)                          !blanked CHAR_LOC
C
      CALL STD8(NO,NPOS,FIELD,NUMBUF,STATUS)
C
      RETURN
      END
C
C
      SUBROUTINE STDRDX(NO,FLAG,DESCR,CTYPE,BYTEL,NOEL,HNC,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR, CTYPE
      INTEGER     FLAG, BYTEL, NOEL, HNC
      INTEGER     STATUS
C
      CALL STLOC(1,1,DESCR)                          !blanked CHAR_LOC
      CALL STLOC(2,1,CTYPE)                          !blanked CHAR_LOC
C
      CALL STD9(NO,FLAG,BYTEL,NOEL,HNC,STATUS)
C
      RETURN
      END


      SUBROUTINE STDWRC(NO,DESCR,NOELM,VALUES,FELEM,MAXVALS,
     +                  DUNIT,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR, VALUES
      INTEGER     NOELM, FELEM, MAXVALS
      INTEGER     DUNIT
      INTEGER     STATUS
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
      CALL STLOC(1,0,VALUES)                       !untouched CHAR_LOC
C 
      CALL STD10(NO,NOELM,FELEM,MAXVALS,STATUS)
C
      RETURN
      END


      SUBROUTINE STDWRD(NO,DESCR,VALUES,FELEM,MAXVALS,DUNIT,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR
      INTEGER     FELEM, MAXVALS
      DOUBLE PRECISION     VALUES(*)
      INTEGER     DUNIT
      INTEGER     STATUS
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C 
      CALL STD11(NO,VALUES,FELEM,MAXVALS,STATUS)
C
      RETURN
      END


      SUBROUTINE STDWRH(NO,DESCR,VALUES,FELEM,MAXVALS,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR, VALUES
      INTEGER     FELEM, MAXVALS
      INTEGER     STATUS
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
      CALL STSTR(2,VALUES)           !STRIPPED_STRING (not too long)
C
      CALL STD12(NO,FELEM,MAXVALS,STATUS)
C
      RETURN
      END


      SUBROUTINE STDWRI(NO,DESCR,VALUES,FELEM,MAXVALS,DUNIT,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR
      INTEGER     FELEM, MAXVALS
      INTEGER     VALUES(*)
      INTEGER     DUNIT
      INTEGER     STATUS
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C 
      CALL STD13(NO,VALUES,FELEM,MAXVALS,STATUS)
C
      RETURN
      END


      SUBROUTINE STDWRL(NO,DESCR,VALUES,FELEM,MAXVALS,DUNIT,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR
      INTEGER     FELEM, MAXVALS
      INTEGER     VALUES(*)
      INTEGER     DUNIT
      INTEGER     STATUS
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C
      CALL STD14(NO,VALUES,FELEM,MAXVALS,STATUS)
C
      RETURN
      END


      SUBROUTINE STDWRR(NO,DESCR,VALUES,FELEM,MAXVALS,DUNIT,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR
      INTEGER     FELEM, MAXVALS
      REAL     VALUES(*)
      INTEGER     DUNIT
      INTEGER     STATUS
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C
      CALL STD15(NO,VALUES,FELEM,MAXVALS,STATUS)
C
      RETURN
      END

      SUBROUTINE STDDEL(NO,DESCR,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     NO
      CHARACTER*(*)   DESCR
      INTEGER     STATUS
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C
      CALL STD16(NO,STATUS)
C
      RETURN
      END

      SUBROUTINE STDCOP(FROM,TO,MASK,DESCR,STATUS)
C
      IMPLICIT NONE
C
      INTEGER     FROM,TO,MASK
      CHARACTER*(*)   DESCR
      INTEGER     STATUS
C
      CALL STSTR(1,DESCR)                          !STRIPPED_STRING
C
      CALL STD17(FROM,TO,MASK,STATUS)
C
      RETURN
      END

