/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.

  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  tbe.fc +++++++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module tbe.fc
.COMMENTS
Module contains layer between the frame related FORTRAN STxxxx interfaces
and the SC_interfaces written in (hopefully independent) C
.AUTHOR         J.D. Ponz, F. Ochsenbein (ESO-IPG))
.KEYWORDS       Table system, FORTRAN interface
.ENVIRONMENT    FORTRAN and C standards
.VERSION  [1.00] 871207:  created from SXFTOC.C
.VERSION  [2.60] 880415:  modified new version - the last one
.VERSION  [2.70] 890119:  add SZCFRNM
.VERSION  [2.80] 900316:  fix problem with FNAME_LEN+2 ...
.VERSION  [3.00] 901213:  Master file. CG.

 090611		last modif
-----------------------------------------------------------------------------*/

#include <ftoc_log.h>           /* LOGICAL    */
#include <tbldef.h>
#include <midas_def.h>

#include <stdlib.h>
 

int   mm;

char *ptr1, *ptr2, *ptr3, *ptr4;
char *loc_pntr();
char *strp_pntr();

/*

*/

/*==========================================================================*/

static int dtype(t)
/*++++++++++++++++
.PURPOSE Convert the datatype for TBEGET, TBEPUT
.RETURNS 0 for Char / 1 for Int / 2 for R*4 / 3 for Double
-----------------*/
char    *t;     /* IN: Character Type */

{
switch(*t) 
   {
   case 'J':
   case 'I':   return(1);
   case 'D':   return(3);
   case 'C':
   case 'A':   return(0);
   case 'R':
       if ((t[1] == '*') && (t[2] >= '8'))     return(3);
   default:    return(2);
   }
}

/*==========================================================================*/

/*** stat = TCEDEL(tid,row,col) ***/

ROUTINE TBEDEL(tid, row,col,status)
int *tid; 	/* IN : table identifier */
int  *row;      /* IN : Row concerned    */
int  *col;      /* IN : column no.    */
int *status;	/* OUT: status return */

{
*status = TCEDEL(*tid,*row,*col);

return 0;
}

/*==========================================================================*/

/*** stat = TCERDC(tid, row, col, value, null) ***/

ROUTINE TBE1(tid,row,col,null,status)
int *tid; 	/* IN : table identifier */
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
int *null;	/* OUT: Array of Null Flags */
int *status;	/* OUT: status return */

{
int  n, klen, ktyp;
char  myform[32];


ptr1 = loc_pntr(1,&mm);            /* get location of "value" */

(void) TCFGET(*tid,*col,myform,&klen,&ktyp);	/* get format of column */
if (klen < mm)
   {
   *status = TCERDC(*tid,*row,*col,ptr1,null);
   n = (int) strlen(ptr1);
   if ((n > 0) && (n < mm)) *(ptr1+n) = ' ';
   }
else
   {
   char   *myelem;

   myelem = (char *) malloc((size_t)(klen+1));
   *status = TCERDC(*tid,*row,*col,myelem,null);
   (void) strncpy(ptr1,myelem,(size_t) mm);
   free(myelem);
   }

*null = (*null ? F77TRUE : F77FALSE);

return 0;
}

/*==========================================================================*/

/*** stat = TCERDD(tid, row, col, value, null) ***/

ROUTINE TBERDD(tid, row, col, value, null,status)
int *tid;       /* IN : table identifier */
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
double *value;  /* OUT: Value       */
int *null;      /* OUT: Array of Null Flags */
int *status;    /* OUT: status return */

{
*status = TCERDD(*tid,*row,*col,value,null);
*null = (*null ? F77TRUE : F77FALSE);

return 0;
}

/*==========================================================================*/

/*** stat = TCERDI(tid, row, col, value, null) ***/

ROUTINE TBERDI(tid, row, col, value, null,status)
int *tid;       /* IN : table identifier */
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
int *value;     /* OUT: Value       */
int *null;      /* OUT: Array of Null Flags */
int *status;    /* OUT: status return */

{
*status = TCERDI(*tid,*row,*col,value,null);
*null = (*null ? F77TRUE : F77FALSE);

return 0;
}

/*==========================================================================*/

/*** stat = TCERDR(tid, row, col, value, null) ***/

ROUTINE TBERDR(tid, row, col, value, null,status)
int *tid;       /* IN : table identifier */
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
float *value;   /* OUT: Value       */
int *null;      /* OUT: Array of Null Flags */
int *status;    /* OUT: status return */

{
*status = TCERDR(*tid,*row,*col,value,null);
*null = (*null ? F77TRUE : F77FALSE);

return 0;
}

/*==========================================================================*/

/*** stat = TCEWRC(tid, row, col, value) ***/

ROUTINE TBE2(tid,row,col,status)
int *tid; 	/* IN : table identifier */
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
int *status;	/* OUT: status return */

{
char  cbuf[64], *cpntr;


ptr1 = loc_pntr(1,&mm);			/* get location of char. value */

if (mm < 64)			/* fits into internal buffer? */
   {				
   cpntr = cbuf;
   }
else
   {
   cpntr = (char *) malloc((size_t)(mm+1));
   }

(void) strncpy(cpntr,ptr1,mm);
*(cpntr+mm) = '\0';

*status = TCEWRC(*tid,*row,*col,cpntr);

if (mm > 63) free(cpntr);

return 0;
}

/*==========================================================================*/

/*** stat = TCEWRR(tid, row, col, value) ***/

ROUTINE TBEWRR(tid, row, col, value, status)

int *tid;       /* IN : table identifier */
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
float *value;   /* IN: Value       */
int *status;    /* OUT: status return */

{
*status = TCEWRR(*tid,*row,*col,value);

return 0;
}

/*==========================================================================*/

/*** stat = TCEWRD(tid, row, col, value) ***/

ROUTINE TBEWRD(tid, row, col, value, status)

int *tid;       /* IN : table identifier */
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
double *value;   /* IN: Value       */
int *status;    /* OUT: status return */

{
*status = TCEWRD(*tid,*row,*col,value);

return 0;
}

/*==========================================================================*/

/*** stat = TCEWRI(tid, row, col, value) ***/

ROUTINE TBEWRI(tid, row, col, value, status)

int *tid;       /* IN : table identifier */
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
int *value;     /* IN: Value       */
int *status;    /* OUT: status return */

{
*status = TCEWRI(*tid,*row,*col,value);

return 0;
}

/*==========================================================================*/

/*** stat = TCEGET(tid,ctype,row,col,value,null) ***/

ROUTINE TBE3(tid,row,col,value,null,status)
int *tid;
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
int *value;     /* OUT: Value       */
int *null;      /* OUT: Array of Null Flags */
int *status;

{
ptr1 = strp_pntr(1);            /* get stripped string of "ctype" */
mm  = dtype(ptr1);		/* convert data type to flag */

if (mm == 0)
   {
   ptr2 = (char *) value;
   *status = TCERDC(*tid,*row,*col,ptr2,null);
   }
else if (mm == 1)
   *status = TCERDI(*tid,*row,*col,value,null);
else if (mm == 2)
   {
   float  *rptr;
   rptr = (float *) value;
   *status = TCERDR(*tid,*row,*col,rptr,null);
   }
else 
   {
   double  *dptr;
   dptr = (double *) value;
   *status = TCERDD(*tid,*row,*col,dptr,null);
   }

*null = (*null ? F77TRUE : F77FALSE);

return 0;
}

/*==========================================================================*/

/*** stat = TCEPUT(tid,ctype,row,col,value) ***/

ROUTINE TBE4(tid,row,col,value,status)
int *tid;
int  *row;      /* IN : Row concerned    */
int *col;      /* IN: column number */
int *value;     /* IN: Value       */
int *status;

{
ptr1 = strp_pntr(1);            /* get stripped string of "ctype" */
mm  = dtype(ptr1);              /* convert data type to flag */

if (mm == 0)
   {
   ptr2 = (char *) value;
   *status = TCEWRC(*tid,*row,*col,ptr2);
   }
else if (mm == 1)
   *status = TCEWRI(*tid,*row,*col,value);
else if (mm == 2)
   {
   float  *rptr;
   rptr = (float *) value;
   *status = TCEWRR(*tid,*row,*col,rptr);
   }
else
   {
   double  *dptr;
   dptr = (double *) value;
   *status = TCEWRD(*tid,*row,*col,dptr);
   }

return 0;
}


/*==========================================================================*/

/*** stat = TCESRC(tid, col,value, start, len, first, next) ***/

ROUTINE TBE5(tid,col,start,len,first,next,status)
int *tid;      /* IN : table identifier        */
int *col;      /* IN : column number           */
int *start;    /* IN : Starting position=index */
int *len;      /* IN : Number of bytes to compare */
int *first;    /* IN : First row to examine    */
int *next;     /* OUT: Found row number        */
int *status;

{
ptr1 = strp_pntr(1);            /* get stripped string of "value" */

*status = TCESRC(*tid,*col,ptr1,*start,*len,*first,next);

return 0;
}

/*==========================================================================*/

/*** stat = TCESRD(tid, col,value, tolerance, first, next) ***/

ROUTINE TBESRD(tid, col,value, tolerance, first, next,status)
int *tid;      /* IN : table identifier        */
int *col;      /* IN : column number           */
double *value;    /* IN : value to search */
double *tolerance;  /* IN : tolerance in that search */
int *first;    /* IN : First row to examine    */
int *next;     /* OUT: Found row number        */
int *status;

{
*status = TCESRD(*tid,*col,*value,*tolerance,*first,next);

return 0;
}

/*==========================================================================*/

/*** stat = TCESRI(tid, col,value, tolerance, first, next) ***/

ROUTINE TBESRI(tid, col,value, tolerance, first, next,status)
int *tid;      /* IN : table identifier        */
int *col;      /* IN : column number           */
int *value;    /* IN : value to search */
int *tolerance;  /* IN : tolerance in that search */
int *first;    /* IN : First row to examine    */
int *next;     /* OUT: Found row number        */
int *status;

{
*status = TCESRI(*tid,*col,*value,*tolerance,*first,next);

return 0;
}

/*==========================================================================*/

/*** stat = TCESRR(tid, col,value, tolerance, first, next) ***/

ROUTINE TBESRR(tid, col,value, tolerance, first, next,status)
int *tid;      /* IN : table identifier        */
int *col;      /* IN : column number           */
float *value;    /* IN : value to search */
float *tolerance;  /* IN : tolerance in that search */
int *first;    /* IN : First row to examine    */
int *next;     /* OUT: Found row number        */
int *status;

{
*status = TCESRR(*tid,*col,*value,*tolerance,*first,next);

return 0;
}

