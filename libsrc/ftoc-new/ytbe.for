C===========================================================================
C Copyright (C) 1995-2010 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  ytbe.for +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module ytbe.for
C  AUTHOR         K. Banse
C  Module contains layer between the table related FORTRAN TBxxxx interfaces
C  and the TC_interfaces written in (hopefully independent) C
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C 
C 100615	last modif
C -----------------------------------------------------------------------------
C
      SUBROUTINE TBERDC(TID, ROW, COL, VALUE, NULL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   VALUE
      INTEGER     TID, ROW, COL, NULL
      INTEGER     STATUS
C 
      CALL STLOC(1,1,VALUE)                              !blanked CHAR_LOC
C 
      CALL TBE1(TID,ROW,COL,NULL,STATUS)
C
      RETURN
      END
C 
C 
      SUBROUTINE TBEGET(TID, CTYPE, ROW, COL, VALUE, NULL, STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CTYPE
      INTEGER     TID, ROW, COL, NULL
      INTEGER     VALUE(*)
      INTEGER     STATUS
C
      CALL STSTR(1,CTYPE)                     !STRIPPED_STRING
C 
      CALL TBR3(TID,ROW,COL,VALUE,NULL,STATUS)
C
      RETURN
      END
C
C
      SUBROUTINE TBEPUT(TID, CTYPE, ROW, COL, VALUE, STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CTYPE
      INTEGER     TID, ROW, COL
      INTEGER     VALUE(*)
      INTEGER     STATUS
C
      CALL STSTR(1,CTYPE)                     !STRIPPED_STRING
C
      CALL TBR4(TID,ROW,COL,VALUE,STATUS)
C
      RETURN
      END
C 
C
      SUBROUTINE TBEWRC(TID, ROW, COL, VALUE, STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   VALUE
      INTEGER     TID, ROW, COL
      INTEGER     STATUS
C
      CALL STLOC(1,0,VALUE)                              !untouched CHAR_LOC
      CALL TBE2(TID,ROW,COL,STATUS)
C
      RETURN
      END
C
      SUBROUTINE TBESRC(TID, COL,VALUE, START, LEN, FIRST, NEXT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   VALUE
      INTEGER     TID, COL, START, LEN, FIRST, NEXT
      INTEGER     STATUS
C
      CALL STSTR(1,VALUE)   
      CALL TBE5(TID,COL,START,LEN,FIRST,NEXT,STATUS)
C
      RETURN
      END
CC
CC all other TBExyz functions
CC handled directly in tbe.fc
CC 
C 
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C.IDENTIFICATION
C  subroutine FT_EOS            version 1.00    100611
C  K. Banse                     ESO - Garching
C
C.PURPOSE
C find end of string which is returned the C way ('\0' in the end)
C like e.g. from CALL TBERDC(...)
C
C also a blank is considered to indicate the end_of_string
C 
C return status = 0, it' o.k.
C		  1, invalid param. inlen
C
C.VERSION
C 100723        last modif
C
C-------------------------------------------------------

      SUBROUTINE FT_EOS(instr,inlen,outstr,stat)

      IMPLICIT NONE

      INTEGER INLEN,STAT
      CHARACTER INSTR*1, OUTSTR*1

      INTEGER K,KI

      CHARACTER*1 COMPI



      IF (INLEN .LT. 1) THEN         !must be > 0
         STAT = 1
         RETURN
      ENDIF
      STAT = 0

CC      alphab(1:37) = alpha1(1:37)
CC      alphab(38:63) = alpha2(1:26)

      COMPI = CHAR(0)                !compi = '\0'
      KI = 0

      DO K=1,INLEN
         IF (INSTR(K:K) .EQ. COMPI) THEN
            KI = K                   !'\0' found => end of string
            GOTO 1000
         ELSE
            OUTSTR(K:K) = INSTR(K:K)
         ENDIF
      ENDDO
C
C no Null char. found => we used full input string
      RETURN

1000  DO K=KI,INLEN                  !we pad with blanks 
         OUTSTR(K:K) = ' '
      ENDDO

      RETURN
      END


