/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  stloc.fc +++++++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module stloc.fc
.COMMENTS
Module contains layer between the keyword related FORTRAN STxxxx interfaces
and the SC_interfaces written in (hopefully independent) C
.AUTHOR         K. Banse  	ESO - Garching
.KEYWORDS       standard interfaces.
.ENVIRONMENT    FORTRAN and C standards
.VERSION  [1.00] 871207:  created from SXFTOC.C
.VERSION  [2.60] 880411:  modified new version - the last one
.VERSION  [2.70] 900316:  fix problems with TEXT_LEN+2

 090902		last modif
-----------------------------------------------------------------------------*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <midas_def.h>


static char  Strip1[258];
static char  *str_strip1 = Strip1;
static char  Strip2[258];
static char  *str_strip2 = Strip2;
static char  Strip3[258];
static char  *str_strip3 = Strip3;

static char  *str_loc1;
static char  *str_loc2;
static char  *str_loc3;
static int  lenloc1, lenloc2, lenloc3;

/*

*/

/*==========================================================================*/

ROUTINE STLOC(idx,flag,str,lstr)
int  *idx;
int  *flag;
char *str;       /* F77 String */
int  lstr;

{
int  kk;


if (*idx == 1)
   {
   str_loc1 = str;
   lenloc1 = lstr;
   }
else if (*idx == 2)
   {
   str_loc2 = str;
   lenloc2 = lstr;
   }
else 
   {
   str_loc3 = str;
   lenloc3 = lstr;
   }

/*
printf("idx = %d, flag = %d, str = %x, lstr = %d\n",*idx,*flag,str,lstr);
printf("str_loc1 = %x, lenloc1 = %d\n",str_loc1,lenloc1);
...goes into the next block...
printf("str[0...3] as int =%d,%d,%d,%d,\n",*str,*(str+1),*(str+2),*(str+3));
printf("str[0...3] =%c,%c,%c,%c, kk = %d\n",
*str,*(str+1),*(str+2),*(str+3),kk);
*/


if (*flag != 0)
   {
   kk = lstr - 1;
   memset((void *)str,32,kk);         /* blank out char_loc */
   *(str+kk) = '\0';
   }

return 0;
}

/*==========================================================================*/

ROUTINE STSTR(idx,str,lstr)
int  *idx;
char *str;      /* F77 String */
int  lstr;

{
register char cc;

register int m, ln;



ln = lstr;
if (ln > 256)
   {
   char  txt[80];

   (void) sprintf(txt,
          "!OJO! STRSTR called with string of length = %d, i.e. > 256",ln);
   SCTPUT(txt);
   ln = 256;
   }
else
   {
   for (m=ln-1; m>0 ;m--)
      {
      cc = str[m];
      if ((cc != ' ') && (cc != '\0') && (cc != '\t'))
         break;
      else
         ln = m;
      }
   }


if (*idx == 1)
   {
   (void) strncpy(Strip1,str,ln);
   Strip1[ln] = '\0';
   }
else if (*idx == 2)
   {
   (void) strncpy(Strip2,str,ln);
   Strip2[ln] = '\0';
   }
else
   {
   (void) strncpy(Strip3,str,ln);
   Strip3[ln] = '\0';
   }

return 0;
}

/*==========================================================================*/

char * strp_pntr(jdx)
int  jdx;

{
if (jdx == 1)
   return (str_strip1);
else if (jdx == 2)
   return (str_strip2);
else if (jdx == 3)
   return (str_strip3);
else 
   return ((char *) 0);
}

/*==========================================================================*/

char * loc_pntr(jdx,loclen)
int  jdx, *loclen;

{
if (jdx == 1)
   {
   *loclen = lenloc1;
   return (str_loc1);
   }
else if (jdx == 2)
   {
   *loclen = lenloc2;
   return (str_loc2);
   }
else if (jdx == 3)
   {
   *loclen = lenloc3;
   return (str_loc3);
   }
else 
   return ((char *) 0);
}

