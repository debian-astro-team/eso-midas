C===========================================================================
C Copyright (C) 1995-2008 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  YSTF.FOR +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module YSTF.FOR
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  created from SXFTOC.C
C 
C 080722	last modif
C -----------------------------------------------------------------------------
C

      SUBROUTINE STFOPN(NAME,DATTYPE,IOMODE,FILTYPE,NO,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   NAME        
      INTEGER    DATTYPE, IOMODE, FILTYPE, NO
      INTEGER    STATUS
C 
      CALL STSTR(1,NAME)                              !STRIPPED_STRING
C 
      CALL STF1(DATTYPE,IOMODE,FILTYPE,NO,STATUS)
C
      RETURN
      END
C


      SUBROUTINE STFCRE(NAME,DATTYPE,IOMODE,FILTYPE,ISIZE,NO,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   NAME        
      INTEGER    DATTYPE, IOMODE, FILTYPE, ISIZE, NO
      INTEGER    STATUS
C 
      CALL STSTR(1,NAME)                              !STRIPPED_STRING
C 
      CALL STF2(DATTYPE,IOMODE,FILTYPE,ISIZE,NO,STATUS)
C
      RETURN
      END
C


      SUBROUTINE STFXCR(NAME,DATTYPE,IOMODE,FILTYPE,ISIZE,OPTFLAGS,
     +                  NO,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   NAME        
      INTEGER    DATTYPE, IOMODE, FILTYPE, ISIZE, OPTFLAGS(*), NO
      INTEGER    STATUS
C 
      CALL STSTR(1,NAME)                              !STRIPPED_STRING
C 
      CALL STF3(DATTYPE,IOMODE,FILTYPE,ISIZE,OPTFLAGS,NO,STATUS)
C
      RETURN
      END
C


      SUBROUTINE STFMAP(NO,IOMODE,FELEM,ISIZE,ACTSIZE,PNTR,STATUS)
C
      IMPLICIT NONE
C
      INTEGER    NO, IOMODE, FELEM, ISIZE, ACTSIZE
      INTEGER*8  PNTR
      INTEGER    STATUS
C 
      CALL STF4(NO,IOMODE,FELEM,ISIZE,ACTSIZE,PNTR,STATUS)
C
      RETURN
      END
C

CC   SUBROUTINE STFCLO(NO,STATUS)
CC   directly done in stf.fc ...

CC   SUBROUTINE STFUNM(NO,STATUS)
CC   directly done in stf.fc ...


      SUBROUTINE STFDEL(NAME,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   NAME        
      INTEGER    STATUS
C
      CALL STSTR(1,NAME)                              !STRIPPED_STRING
C
      CALL STF5(STATUS)
C
      RETURN
      END
C



      SUBROUTINE STFRNM(OLDNAME,NEWNAME,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   OLDNAME, NEWNAME
      INTEGER    STATUS
C
      CALL STSTR(1,OLDNAME)                           !STRIPPED_STRING
      CALL STSTR(2,NEWNAME)                           !STRIPPED_STRING
C
      CALL STF6(STATUS)
C
      RETURN
      END
C


CC   SUBROUTINE STFGET(NO,FELEM,ISIZE,ACTSIZE,BUFADR,STATUS)
CC   directly done in stf.fc ...

CC   SUBROUTINE STFPUT(NO,FELEM,ISIZE,BUFADR,STATUS)
CC   directly done in stf.fc ...


      SUBROUTINE STFINF(NAME,FNO,IBUF,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   NAME
      INTEGER    FNO, IBUF(*)
      INTEGER    STATUS
C
      CALL STSTR(1,NAME)                           !STRIPPED_STRING
C
      CALL STF7(FNO,IBUF,STATUS)
C
      RETURN
      END
C


      SUBROUTINE STFNAM(NO,NAME,NAMLEN,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   NAME
      INTEGER    NO, NAMLEN
      INTEGER    STATUS
C
      CALL STLOC(1,1,NAME)                            !blanked CHAR_LOC
C
      CALL STF8(NO,NAMLEN,STATUS)
C
      RETURN
      END
C

CC   SUBROUTINE STFXMP(NOPIX,DATFORM,PNTR,STATUS)
CC   directly done in stf.fc ...

CC   SUBROUTINE STFYMP(NOPIX,DATFORM,IMNO,PNTR,STATUS)
CC   directly done in stf.fc ...


