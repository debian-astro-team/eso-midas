C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  YSTC.FOR +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module YSTC.FOR
C .COMMENTS
C Module contains layer between the keyword related FORTRAN STxxxx interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  
C 
C 060322		last modif
C -----------------------------------------------------------------------------
C
      SUBROUTINE STCGET(CATFILE,FLAG,NAME,IDENT,NO,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CATFILE        
      INTEGER     FLAG, NO
      CHARACTER*(*)    NAME, IDENT
      INTEGER    STATUS
C 
      CALL STSTR(1,CATFILE)                          !STRIPPED_STRING
      CALL STLOC(1,1,NAME)                           !blanked CHAR_LOC
      CALL STLOC(2,1,IDENT)                          !blanked CHAR_LOC
C 
      CALL STC1(FLAG,NO,STATUS)
C
      RETURN
      END
C


      SUBROUTINE STCADD(CATFILE,NAME,IDENT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CATFILE        
      CHARACTER*(*)   NAME, IDENT
      INTEGER    STATUS
C 
      CALL STSTR(1,CATFILE)
      CALL STSTR(2,NAME)
      CALL STSTR(3,IDENT)
C 
      CALL STC2(STATUS)
C
      RETURN
      END
C 

      SUBROUTINE STCSUB(CATFILE,NAME,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CATFILE        
      CHARACTER*(*)   NAME
      INTEGER    STATUS
C 
      CALL STSTR(1,CATFILE)
      CALL STSTR(2,NAME)
C 
      CALL STC3(STATUS)
C
      RETURN
      END


      SUBROUTINE STCCRE(CATFILE,ITYPE,FLAG,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CATFILE
      INTEGER    ITYPE,FLAG
      INTEGER    STATUS
C
      CALL STSTR(1,CATFILE)
C
      CALL STC4(ITYPE,FLAG,STATUS)
C
      RETURN
      END
 

      SUBROUTINE STCLIS(CATFILE,INTVAL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CATFILE
      INTEGER    INTVAL(2)
      INTEGER    STATUS
C
      CALL STSTR(1,CATFILE)
C
      CALL STC5(INTVAL,STATUS)
C
      RETURN
      END


      SUBROUTINE STCSHO(CATFILE,TOTENT,LASTENT,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CATFILE
      INTEGER    TOTENT, LASTENT
      INTEGER    STATUS
C
      CALL STSTR(1,CATFILE)
C
      CALL STC6(TOTENT,LASTENT,STATUS)
C
      RETURN
      END


      SUBROUTINE STCFND(CATFILE,FRMNO,NAME,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CATFILE
      CHARACTER*(*)   NAME
      INTEGER    FRMNO, STATUS
C
      CALL STSTR(1,CATFILE)
      CALL STLOC(1,1,NAME)
C
      CALL STC7(FRMNO,STATUS)
C
      RETURN
      END


