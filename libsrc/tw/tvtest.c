/* @(#)tvtest.c	19.1 (ES0-DMD) 02/25/03 13:58:15 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Program
.NAME		tvtest.c
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Terminal Management.
.PURPOSE	Test of Terminal Routines (basic functions for
		TermWindows Implementation)
.ENVIRONMENT	TermWindows
.VERSION 2.0	02-Jul-1987: Creation.
.VERSION 2.1	02-Dec-1987: Testing WhileInput
.VERSION 3.0	20-Mar-1988: Version '3'
------------------------------------------------------------*/

#define	PM_LEVEL	2

#define TW_import	0
#include <macrogen.h>
#include <ok.h>
#include <trace.h>
#include <error.h>
#include <tw.h>
#include <atype.h>

char *gets();
char WaitandSee();

#define curc tpos[1]
#define curl tpos[0]
#define	v_bold		_BOLD_
#define	v_underscore	_UNDERSCORE_
#define	v_blink		_BLINK_
#define	v_inverse	_REVERSE_
#define	v_graphics	_GRAPHICS_

static char msg[140];
static int last_line, colCR;
static short int tpos[2];
static short int dim[2];
static int  three= 2,four= 3;
static char temp,string[5];
static char *char_type[7] = {(char *)0};
static char arrows[5] = {'u', 'd', 'l', 'r', 'h'};
static char aux[20];

	MONITOR(tvtest);

main	()
{
	char *p;
	int i;
	int ring_bell();

  printf("On-line editing preference: Vms, Unix, Other, Default (VMS): ");
  gets(msg); switch(toupper(msg[0]))
  { case 'V': i = TW_cc_VMS; break;
    case 'U': i = TW_cc_UNIX; break;
    case 'O': i = TW_cc_USER; break;
    default : i = -1;
  }

  printf("Device type ? "); if (gets(msg) == (char *)0) return;
  if (strlen(msg)) status = OpenTerm(msg,(char *)0,i);
	else status = OpenTerm((char *)0,(char *)0,i);
  if (!status)
  {	printf("****%s\n", ERR_LOC());
	return;
  }

  ScreenSize(dim);	/* Get Screen Dimensions */
  last_line = dim[0] - 1;
  colCR = 2*(dim[1])/3;

  char_type[0]		= " ?????????? ";
  char_type[_STANDARD_]	= " (ASCII)    ";
  char_type[_KEYPAD_] 	= " (Keypad)   ";
  char_type[_PF_] 	= " (PF key)   ";
  char_type[_ARROW_] 	= " (Arrow)    ";
  char_type[_FK_] 	= " (other key)";

  WhileInput(ring_bell); 

  Title("TEST 1 - Clear Screen And Display Message  ");
  Puts("This is a Test Message");
  Puts("......................");
  if (WaitCR() == 'i')	goto Input;

  Title("TEST 2 - Single-character input");
  Put("Type any character to perform test--");
  GetChar(&temp);
  Puts("OK");
  Put("The character you typed was:   ");
  string[0]= temp;
  WriteBinary(string,1);
  if (WaitCR() == 'i')	goto Input;
  

  Title("TEST 3 - Cursor Down and Up");
  CursorDown(4);
  Puts("This should be down four lines.");
  if (CursorUp(2)  == OK)
	  Puts("This should be above the `down four' message");
	else Impossible();
  
  if (WaitCR() == 'i')	goto Input;

  Title("TEST 4 - Line wrap and Newline");
  Put("Type any character to perform test--");
  GetChar(&temp);
  Put("::::::::::::This should cause a line wrap, and");
  Puts(" This should cause a line wrap..................\
.................................OKAY?");
  Puts("This should appear\non two lines...");
  NewLine();
  Put("The character you typed was:   ");
  string[0]= temp;
  WriteBinary(string,1);
  
  if (WaitCR() == 'i')	goto Input;
 
  Title("TEST 5 - Cursor Left and Right");
  Put("A");
  CursorRight(5);
  Put("G");
  WaitandSee();
  CursorLeft(6);
  Put("B");
  CursorRight(3);
  Put("F");
  CursorLeft(4);
  Put("C");
  CursorRight(1);
  Put("E");
  CursorLeft(2);
  Put("D");
  CursorLeft(7);
  CursorDown(2);
  Puts("The above should read ABCDEFG");
  
  if (WaitCR() == 'i')	goto Input;

  Title("TEST 6 - Cursor positioning");
  i = dim[0] / 3;
  CursorDown(1);
  CursorTo(i,i);
  Put("A");
  CursorTo(i,i*2);
  Put("A");
  CursorTo(2*i,i);
  Put("A");
  CursorTo(2*i,2*i);
  Put("A");
  CursorTo(9,0);
  Put("This should be on line 9");
  if (CursorTo(three,0) == OK)
	  Put("This should be on line 3");
	else Impossible();
  
  if (WaitCR() == 'i')	goto Input;
 
  Title("TEST 7L - Clear line ");
  CursorTo(three,0);
  Puts("ABCDEFGHIJKLMNOPQRSTUVWXYZ\n\n");
  Put("About to clear line 3...");
  WaitandSee();
  CursorTo(three,4);
  ClearLine();
  
  if (WaitCR() == 'i')	goto Input;
 

  Title("TEST 7C - Clear characters.");
  NewLine();
  Puts("This test will erase characters F-K.");
  NewLine();
  Put("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  GetCursor(tpos);
  WaitandSee();
  CursorTo(tpos[0],5);
  ClearChars(6);
  
  if (WaitCR() == 'i')	goto Input;

  Title("TEST 8 - Clear to end of line and clear to end of screen");
  GetCursor(tpos);
  Puts("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
  Puts("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
  Puts("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
  WaitandSee();
  CursorTo(tpos[0]+1,10);
  ClearRight();
  CursorTo(last_line-5,0);
  Puts("Cleared from K on line 3");
  Put("Next should clear from H of line 2 to the bottom");
  WaitandSee();
  CursorTo(tpos[0],7);
  ClearDown();
  
  if (WaitCR() == 'i')	goto Input;

  Title("TEST 9L - Clear from beginning of line ");
  NewLine();
  Puts("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  NewLine();
  Puts("This test should erase characters A-F.");
  WaitandSee();
  CursorTo(three,5);
  ClearLeft();
  
  if (WaitCR() == 'i')	goto Input;
 
  Title("TEST 9S - Clear from beginning of screen");
  CursorTo(four,0);
  Puts("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  NewLine();
  Puts("This test will clear the screen up to F.");
  WaitandSee();
  CursorTo(four,5);
  ClearUp();
  
  if (WaitCR() == 'i')	goto Input;
 
  Title("TEST 10 - Character attributes");
  NewLine();
  SetAttr(v_bold);
  Put("BOLD");
  SetAttr(v_blink);
  Put("BLINK");
  SetAttr(v_underscore);
  Put("UNDERSCORE");
  SetAttr(v_inverse);
  Puts("INVERSE");
  SetAttr(v_bold+v_underscore);
  Put("BOLD+UNDERSCORE");
  SetAttr(v_bold+v_blink);
  Put("BOLD+BLINK");
  SetAttr(v_bold+v_inverse);
  Puts("BOLD+INVERSE");
  SetAttr(v_blink+v_underscore);
  Put("UNDERSCORE+BLINK");
  SetAttr(v_underscore+v_inverse);
  Put("UNDERSCORE+INVERSE");
  SetAttr(v_blink+v_inverse);
  Puts("BLINK+INVERSE");
  SetAttr(v_bold+v_blink+v_underscore);
  Puts("BOLD+UNDERSCORE+BLINK");
  SetAttr(v_bold+v_underscore+v_inverse);
  Puts("BOLD+UNDERSCORE+INVERSE");
  SetAttr(v_bold+v_blink+v_inverse);
  Puts("BOLD+BLINK+INVERSE");
  SetAttr(v_underscore+v_blink+v_inverse);
  Puts("UNDERSCORE+BLINK+INVERSE");
  SetAttr(v_bold+v_underscore+v_blink+v_inverse);
  Puts("BOLD+UNDERSCORE+BLINK+INVERSE");
  NewLine();
  SetAttr(0); Put(" Graphic chars =>");
  Put("abcdefghijklmnopqrstuvwxyz\n");

  GetCursor(tpos);
  DrawBox(curl, curc, 7, 44);
  CursorTo(++curl,1);
  SetAttr(0); Put("Graphic chars =>");
  SetAttr(_GRAPHICS_); 
  Put("abcdefghijklmnopqrstuvwxyz");
  CursorTo(++curl,1);
  SetAttr(_BOLD_); Put("Graphic chars =>");
  SetAttr(_GRAPHICS_+_BOLD_); 
  Put("abcdefghijklmnopqrstuvwxyz");
  CursorTo(++curl,1);
  SetAttr(_UNDERSCORE_); Put("Graphic chars =>");
  SetAttr(_GRAPHICS_+_UNDERSCORE_); 
  Put("abcdefghijklmnopqrstuvwxyz");
  CursorTo(++curl,1);
  SetAttr(_REVERSE_); Put("Graphic chars =>");
  SetAttr(_GRAPHICS_+_REVERSE_); 
  Put("abcdefghijklmnopqrstuvwxyz");
  CursorTo(++curl,1);
  SetAttr(_BLINK_); Put("Graphic chars =>");
  SetAttr(_GRAPHICS_+_BLINK_); 
  Put("abcdefghijklmnopqrstuvwxyz");
  SetAttr(0);
  
  if (WaitCR() == 'i')	goto Input;


  Title ("TEST 11 - Scrolling");
  Puts("First scrolling down three lines, then three lines up.");
  i = dim[0]/2;
  CursorTo(i,0);
  Puts("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
  Puts("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
  Puts("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
  WaitandSee();
  if (ScrollDown(3) != OK)	Impossible();
  WaitandSee();
  if (ScrollUp(3) != OK)	Impossible();
  
  if (WaitCR() == 'i')	goto Input;


  Title ("TEST 12L - Delete and Insert Lines");
  i = dim[0]/2 - dim[0] / 5;
  CursorTo(i,0);
  Puts("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
  Puts("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
  Puts("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
  CursorTo(3,0);
  Puts("Will insert blank lines");
  WaitandSee();
  CursorTo(i+1,0);
  if (InsertLines(1) == OK)
  {	CursorDown(2);
	InsertLines(1);
	CursorTo(3,0);
	ClearRight();
	Puts("Will delete blank lines");
	WaitandSee();
	CursorTo(i+1,0);
	DeleteLines(1);
	CursorDown(1);
	DeleteLines(1);
  }	else Impossible();
  
  if (WaitCR() == 'i')	goto Input;

  Title ("TEST 12C - Delete and Insert Chars");
  i = dim[0]/2;
  CursorTo(i,0);
  Puts("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  CursorTo(3,0);
  Puts("Will insert 3 blanks after A");
  WaitandSee();
  CursorTo(i,1);
  if (InsertBlanks(3))
 {	CursorTo(3,0);
	ClearRight();
	Puts("... and delete them...");
	WaitandSee();
        CursorTo(i,1);
	DeleteChars(3);
  }	else Impossible();
	
  if (WaitCR() == 'i')	goto Input;



  Title("TEST 13 - Terminal type");
  NewLine();
  sprintf(msg,"I'm made of %d lines x %d cols",
	dim[0],dim[1]);
  Put(msg);
  
  if (WaitCR() == 'i')	goto Input;

  Input:
  Title("TEST 14 - Keypad input");
  i = 2*(dim[0])/3;

  while(1)
  {	CursorTo(3,0);
	ERR_CLEAR();
	Put("Please type any key -- The EOF character will stop the loop");
        status = GetChar(&temp);			/* Unknown char	*/
	if (status == _EOF_) break;
	ClearDown();
	if(status < _STANDARD_)
	{ 	CursorTo(i,i);
		SetAttr(_REVERSE_);
		p = ERR_LOC();
		if (p == (char *)0)	p = 
			"TERMCAP does not understand the typed key";
		Put(p);
		while ((temp = WaitandSee()) > ' ')   
			SetAttr(_REVERSE_),
			Write(&temp,1);
		SetAttr(_NORMAL_);
  		CursorTo(3,0);
		ClearDown();
		continue;
	}
        NewLine();
	Put("Character type is ");
	string[0] = '0' + status;
	Write(string,1);
	Put(char_type[status]);
	Put(" -- Character is ");
	string[1] = temp; string[2] = '\''; string[0] = '\'';
	TRACE_ED_I("Got: ", temp);
	if (status == _ARROW_)	string[1] = arrows[temp];
	else if (temp < ' ') 
		{ 	string[0] = '^';
			string[1] = temp + '@';
			string[2] = ' ';
		 } 
	WriteBinary(string,3);
  }
  if (WaitCR() == 'i')	goto Input;
  
  Title("TEST 15 - Command input");
  CursorTo(3,0); 
  WhileInput(NULL_FCT(int));	/* Nothing while waiting for input */

  for (i=0; ; i++)
  {	SetAttr(_BOLD_); Put("\nPrompt=>"); SetAttr(_NORMAL_);
	if (i == 0)				/* First test	*/
		strcpy(msg,"Template command ===> Try to edit it !");
	else	msg[0] = 0;
	status = Mods(msg, sizeof(msg));
	if (status == _EOF_) break;
	NewLine();
	sprintf(aux,"%6d: ", i);
	Put(aux);
	SetAttr(_REVERSE_);
	WriteBinary(msg, strlen(msg));
	SetAttr(_NORMAL_);
  }
  if (WaitCR() == 'i')	goto Input;
  
 
  ClearScreen();
  Puts("End of tests.");
  CloseTerm();
}
int WaitCR()
{
	MID_STATIC short int tmp[2];
	char ch;

	ENTER("WaitCR");
	GetCursor(tmp);
	CursorTo(last_line,colCR);
	Put("Hit <Return> to continue");
	GetChar(&ch);
	CursorTo(tmp[0],tmp[1]);
	EXIT (ch);
}
char WaitandSee()
{
	MID_STATIC short int tmp[2];
	char ch;
	ENTER("WaitandSee");
	GetCursor(tmp);
	CursorTo(last_line,colCR);
	SetAttr(_BLINK_|_REVERSE_);
	Put("Hit <Return> to continue");
	GetChar(&ch);
	SetAttr(0);
	CursorTo(tmp[0],tmp[1]);
	EXIT (ch);
}
Impossible()
{
	ENTER("Impossible");
	CursorTo(last_line,4);
	SetAttr(_BLINK_|_BOLD_|_REVERSE_);
	Put("Not Supported");
	SetAttr(0);
	EXIT (OK);
}
Title(c)
	char *c;
{        
	ENTER("Title");
	ClearScreen();
        Puts(c);
	EXIT(OK);
}

int ring_bell(n)	/* Routine excuted while waiting for input */
	int n;		/* IN: Count for waiting clicks		*/
{  
	TVSAVE	pos;

  ENTER("+ring_bell");

  if (n > 20)	EXIT(20);
  if (n == 1)
  {	ClearChars(1);
	SupplyInputs("\r");
	Bell();
	EXIT(0);
  }
  if (n == 4) 
  {	SaveCursor(&pos);
	SetAttr(_BOLD_|_BLINK_|_REVERSE_);
	Bell();
	Put("?");
	RestoreCursor(&pos);
  }

  EXIT(--n);
}
