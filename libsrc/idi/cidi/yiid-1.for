C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or
C modify it under the terms of the GNU General Public License as
C published by the Free Software Foundation; either version 2 of
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public
C License along with this program; if not, write to the Free
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge,
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C       Internet e-mail: midas@eso.org
C       Postal address: European Southern Observatory
C                       Data Management Division
C                       Karl-Schwarzschild-Strasse 2
C                       D 85748 Garching bei Muenchen
C                       GERMANY
C===========================================================================

      SUBROUTINE IIDSSS(DSPLAY,MEMIDS,XOFFS,YOFFS,SPLITF,SPLX,SPLY,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,MEMIDS,XOFFS,YOFFS
      INTEGER     SPLITF,SPLX,SPLY,IDST
C
      CALL IID1(DSPLAY,MEMIDS,XOFFS,YOFFS,0,0)
      CALL IID2(SPLITF,SPLX,SPLY,IDST)

C
      RETURN
      END

      SUBROUTINE IIDSNP(DSPLAY,COLMODE,NPIXEL,XOFF,YOFF,
     +                  DEPTH,PACKF,DATA,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,COLMODE,NPIXEL,XOFF,YOFF
      INTEGER     DEPTH,PACKF,DATA,IDST
C
      CALL IID1(DSPLAY,COLMODE,NPIXEL,XOFF,0,0)
      CALL IID3(YOFF,DEPTH,PACKF,DATA,IDST)
C
      RETURN
      END

      SUBROUTINE IIDQDC(DSPLAY,CONFNO,MEMTYP,TOTMEM,CONFMD,MEMID,
     +                  MEMSZX,MEMSZY,MEMDEP,ITTDEP,NOMEM,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CONFNO,MEMTYP,TOTMEM,CONFMD,MEMID
      INTEGER     MEMSZX,MEMSZY,MEMDEP,ITTDEP,NOMEM,IDST
C
      CALL IID1(DSPLAY,CONFNO,MEMTYP,TOTMEM,CONFMD,MEMID)
      CALL IID4(MEMSZX,MEMSZY,MEMDEP,ITTDEP,NOMEM,IDST)
C
      RETURN
      END

      SUBROUTINE IIDQDV(DSPLAY,TOTCNF,SZX,SZY,DEPTH,
     +                  NLUT,NITT,NCURS,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,TOTCNF,SZX,SZY,DEPTH,NLUT,NITT,NCURS,IDST
C
      CALL IID1(DSPLAY,TOTCNF,SZX,SZY,0,0)
      CALL IID5(DEPTH,NLUT,NITT,NCURS,IDST)

C
      RETURN
      END

