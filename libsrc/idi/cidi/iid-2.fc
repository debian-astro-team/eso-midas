/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*                     iid.fc

.VERSION

090611          last modif

*/



#include <stdlib.h>
#include <string.h>

int   mm;

char *ptr1;
char *loc_pntr();

/*

*/

ROUTINE IIDSSS(dsplay,memids,xoffs,yoffs,splitf,splx,sply,idst)
int *dsplay;
int *memids;
int *xoffs;
int *yoffs;
int *splitf;
int *splx;
int *sply;
int *idst;

{
int  IIDSSS_C();

*idst = IIDSSS_C(*dsplay,memids,xoffs,yoffs,*splitf,*splx,*sply);
return 0;
}

/*==========================================================================*/

/*** idst = IIDERR_C(erno,erstr,strl) ***/

ROUTINE IID1(erno,strl)
int *erno;
int *strl;

{
int  n;
int  IIDERR_C();

ptr1 = loc_pntr(1,&mm);         /* get location of "erstr" */

IIDERR_C(*erno,ptr1,strl);

n = (int) strlen(ptr1);
if ((n > 0) && (n < mm)) *(ptr1+n) = ' ';

return 0;
}


ROUTINE IIDRST(dsplay,idst)
int *dsplay;
int *idst;

{
int  IIDRST_C();

*idst = IIDRST_C(*dsplay);
return 0;
}

 
ROUTINE IIDUPD(dsplay,idst)
int *dsplay;
int *idst;

{
int  IIDUPD_C();

*idst = IIDUPD_C(*dsplay);
return 0;
}

ROUTINE IIDICO(dsplay,flag,idst)
int *dsplay;
int *flag;
int *idst;

{
int  IIDICO_C();

*idst = IIDICO_C(*dsplay,*flag);
return 0;
}

 
/* CG. ATTENTION to local cconfno */
ROUTINE IIDSEL(dsplay,confno,idst)
int *dsplay;
int *confno;
int *idst;

{
int  cconfno;
int  IIDSEL_C();

cconfno = *confno;
cconfno --;			/* in C we count from 0 on ...  */

*idst = IIDSEL_C(*dsplay,cconfno);
return 0;
}


ROUTINE IIDSDP(dsplay,chans,nochan,lutf,ittf,idst)
int *dsplay;
int *idst;
int *chans;
int *nochan;
int *lutf;
int *ittf;

{
int  IIDSDP_C();

*idst = IIDSDP_C(*dsplay,chans,*nochan,lutf,ittf);
return 0;
}

ROUTINE IIDIAG(dsplay,luno,idst)
int *dsplay;
int *luno;
int *idst;

{
int  IIDIAG_C();

*idst = IIDIAG_C(*dsplay,*luno);
return 0;
}

 
ROUTINE IIDQDV(dsplay,totcnf,szx,szy,depth,nlut,nitt,ncurs,idst)

int *dsplay;
int *totcnf;
int *szx;
int *szy;
int *depth;
int *nlut;
int *nitt;
int *ncurs;
int *idst;

{
int  IIDQDV_C();


*idst = IIDQDV_C(*dsplay,totcnf,szx,szy,depth,nlut,nitt,ncurs);
return 0;
}

 
ROUTINE IIDQDC(dsplay,confno,memtyp,totmem,confmd,memid,
               memszx,memszy,memdep,ittdep,nomem,idst)
int *dsplay;
int *confno;
int *memtyp;
int *totmem;
int *confmd;
int *memid;
int *memszx;
int *memszy;
int *memdep;
int *ittdep;
int *nomem;
int *idst;

{
int  IIDQDC_C();


*idst = IIDQDC_C(*dsplay,*confno,*memtyp,*totmem,
                 confmd,memid,memszx,memszy,memdep,ittdep,nomem);
return 0;
}
 

ROUTINE IIDQCI(dsplay,capa,outsiz,outdat,total,idst)
int *dsplay;
int *capa;
int *outsiz;
int *outdat;
int *total;
int *idst;
{
int  IIDQCI_C();

*idst = IIDQCI_C(*dsplay,*capa,*outsiz,outdat,total);
return 0;
}


ROUTINE IIDSNP(dsplay,colmode,npixel,xoff,yoff,depth,packf,data,idst)
int *dsplay;
int *colmode;
int *npixel;
int *xoff;
int *yoff;
int *depth;
int *packf;
int *data;
int *idst;

{
int  IIDSNP_C();

*idst = IIDSNP_C(*dsplay,*colmode,*npixel,*xoff,*yoff,*depth,*packf,data);
return 0;
}
 
