/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


#include  <ftoc.h>


static int  savdsp, *pin1, *pin2, *pin3, *pin4, *pin5;

ROUTINE IID1(dsplay,in1,in2,in3,in4,in5)
fint2c *dsplay;
fint2c *in1;
fint2c *in2;
fint2c *in3;
fint2c *in4;
fint2c *in5;

{
savdsp = *dsplay;
pin1 = in1;
pin2 = in2;
pin3 = in3;
pin4 = in4;
pin5 = in5;
return 0;
}


SUBROUTINE IIDERR(erno,erstr,strl)
CHARACTER erstr;
fint2c *erno;
fint2c *strl;

{
int  IIDERR_C();

IIDERR_C(*erno,CHAR_LOC(erstr),strl);
return 0;
}

ROUTINE IIDRST(dsplay,idst)
fint2c *dsplay;
fint2c *idst;

{
int  IIDRST_C();

*idst = IIDRST_C(*dsplay);
return 0;
}

ROUTINE IIDUPD(dsplay,idst)
fint2c *dsplay;
fint2c *idst;

{
int  IIDUPD_C();

*idst = IIDUPD_C(*dsplay);
return 0;
}

ROUTINE IIDICO(dsplay,flag,idst)
fint2c *dsplay;
fint2c *flag;
fint2c *idst;

{
int  IIDICO_C();

*idst = IIDICO_C(*dsplay,*flag);
return 0;
}

/* CG. ATTENTION to local cconfno */
ROUTINE IIDSEL(dsplay,confno,idst)
fint2c *dsplay;
fint2c *confno;
fint2c *idst;

{
int  cconfno;
int  IIDSEL_C();

cconfno = *confno;
cconfno --;			/* in C we count from 0 on ...  */

*idst = IIDSEL_C(*dsplay,cconfno);
return 0;
}


ROUTINE IIDSDP(dsplay,chans,nochan,lutf,ittf,idst)
fint2c *dsplay;
fint2c *idst;
fint2c *chans;
fint2c *nochan;
fint2c *lutf;
fint2c *ittf;

{
int  IIDSDP_C();

*idst = IIDSDP_C(*dsplay,chans,*nochan,lutf,ittf);
return 0;
}

ROUTINE IIDIAG(dsplay,luno,idst)
fint2c *dsplay;
fint2c *luno;
fint2c *idst;

{
int  IIDIAG_C();

*idst = IIDIAG_C(*dsplay,*luno);
return 0;
}

ROUTINE IID5(depth,nlut,nitt,ncurs,idst)
fint2c *depth;
fint2c *nlut;
fint2c *nitt;
fint2c *ncurs;
fint2c *idst;

{
int  IIDQDV_C();

/* CALL IID1(DSPLAY,TOTCNF,SZX,SZY,0,0) */

*idst = IIDQDV_C(savdsp,pin1,pin2,pin3,depth,nlut,nitt,ncurs);
return 0;
}

ROUTINE IID4(memszx,memszy,memdep,ittdep,nomem,idst)
fint2c *memszx;
fint2c *memszy;
fint2c *memdep;
fint2c *ittdep;
fint2c *nomem;
fint2c *idst;

{
int  IIDQDC_C();

/*  CALL IID1(DSPLAY,CONFNO,MEMTYP,TOTMEM,CONFMD,MEMID) */

*idst = IIDQDC_C(savdsp,*pin1,*pin2,*pin3,
                 pin4,pin5,memszx,memszy,memdep,ittdep,nomem);
return 0;
}

ROUTINE IIDQCI(dsplay,capa,outsiz,outdat,total,idst)
fint2c *dsplay;
fint2c *capa;
fint2c *outsiz;
fint2c *outdat;
fint2c *total;
fint2c *idst;
{
int  IIDQCI_C();

*idst = IIDQCI_C(*dsplay,*capa,*outsiz,outdat,total);
return 0;
}


ROUTINE IID2(splitf,splx,sply,idst)
fint2c *splitf;
fint2c *splx;
fint2c *sply;
fint2c *idst;
{
int  IIDSSS_C();

*idst = IIDSSS_C(savdsp,pin1,pin2,pin3,*splitf,*splx,*sply);
return 0;
}

ROUTINE IID3(yoff,depth,packf,data,idst)
fint2c *yoff;
fint2c *depth;
fint2c *packf;
fint2c *data;
fint2c *idst;

{
int  IIDSNP_C();

*idst = IIDSNP_C(savdsp,*pin1,*pin2,*pin3,*yoff,*depth,*packf,data);
return 0;
}
