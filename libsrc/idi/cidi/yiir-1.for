C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or
C modify it under the terms of the GNU General Public License as
C published by the Free Software Foundation; either version 2 of
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public
C License along with this program; if not, write to the Free
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge,
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C       Internet e-mail: midas@eso.org
C       Postal address: European Southern Observatory
C                       Data Management Division
C                       Karl-Schwarzschild-Strasse 2
C                       D 85748 Garching bei Muenchen
C                       GERMANY
C===========================================================================

      SUBROUTINE IIRINR(DSPLAY,CHAN,COLOUR,XA,YA,XB,YB,ROID,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN
      INTEGER     COLOUR,XA,YA,XB,YB,ROID,IDST
C
      CALL IIR1(DSPLAY,CHAN,COLOUR,XA,YA)
      CALL IIR2(XB,YB,ROID,IDST)
C
      RETURN
      END

      SUBROUTINE IIRRRI(DSPLAY,CHAN,ROID,XA,YA,XB,YB,MEMO,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN
      INTEGER     ROID,XA,YA,XB,YB,MEMO,IDST
C
      CALL IIR1(DSPLAY,CHAN,ROID,XA,YA)
      CALL IIR3(XB,YB,MEMO,IDST)
C
      RETURN
      END

      SUBROUTINE IIRWRI(DSPLAY,CHAN,ROID,XA,YA,XB,YB,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN
      INTEGER     ROID,XA,YA,XB,YB,IDST
C
      CALL IIR1(DSPLAY,CHAN,ROID,XA,YA)
      CALL IIR4(XB,YB,IDST)
C
      RETURN
      END
C 
      SUBROUTINE IICINR(DSPLAY,CHAN,COLOUR,XC,YC,RAD1,RAD2,RAD3,
     +                  ROID,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN
      INTEGER     COLOUR,XC,YC,RAD1,RAD2,RAD3,ROID,IDST
C
      CALL IIR1(DSPLAY,CHAN,COLOUR,XC,YC)
      CALL IIR5(RAD1,RAD2,RAD3,ROID,IDST)
C
      RETURN
      END
 
      SUBROUTINE IICRRI(DSPLAY,CHAN,ROID,XC,YC,RAD1,RAD2,RAD3,MEMO,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN
      INTEGER     MEMO,XC,YC,RAD1,RAD2,RAD3,ROID,IDST
C
      CALL IIR1(DSPLAY,CHAN,ROID,XC,YC)
      CALL IIR6(RAD1,RAD2,RAD3,MEMO,IDST)
C
      RETURN
      END
 
      SUBROUTINE IICWRI(DSPLAY,CHAN,ROID,XC,YC,RAD1,RAD2,RAD3,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN
      INTEGER     XC,YC,RAD1,RAD2,RAD3,ROID,IDST
C
      CALL IIR1(DSPLAY,CHAN,ROID,XC,YC)
      CALL IIR7(RAD1,RAD2,RAD3,IDST)
C
      RETURN
      END

