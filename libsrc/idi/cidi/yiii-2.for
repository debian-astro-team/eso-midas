C===========================================================================
C Copyright (C) 1995-2006 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or
C modify it under the terms of the GNU General Public License as
C published by the Free Software Foundation; either version 2 of
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public
C License along with this program; if not, write to the Free
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C       Internet e-mail: midas@eso.org
C       Postal address: European Southern Observatory
C                       Data Management Division
C                       Karl-Schwarzschild-Strasse 2
C                       D 85748 Garching bei Muenchen
C                       GERMANY
C===========================================================================

      SUBROUTINE IIIGSE(DSPLAY,EVALNO,KSTRING,LL,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KSTRING
      INTEGER     DSPLAY,EVALNO,LL,STATUS
C
      CALL STLOC(1,1,KSTRING)                       !blank CHAR_LOC
C 
      CALL III1(DSPLAY,EVALNO,LL,STATUS)            !get string
C
      RETURN
      END


      SUBROUTINE IIIGCE(DSPLAY,EVALNO,KSTRING,STATUS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   KSTRING
      INTEGER     DSPLAY,EVALNO,STATUS
C
      CALL STLOC(1,0,KSTRING)                       !untouched CHAR_LOC
C
      CALL III2(DSPLAY,EVALNO,STATUS)               !get char
C
      RETURN
      END

