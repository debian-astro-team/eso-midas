/* @(#)iig.c	19.1 (ESO-DMD) 02/25/03 13:54:48 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/



/* ----------------------------------------------------------------- */
/* ---------  IIG -------------------------------------------------- */
/* ----------------------------------------------------------------- */

/* 
***********************************************************************
* IIGPLY_C:   Polyline                                                *
* IIGTXT_C:   Plot Text                                               *
* IIGCPY_C:   Copy graphics + text
*                                                                     *
* V 1.1  871201:  P. Santin - Trieste Astronomical Observatory        *
* V 2.0  881223:  K. Banse  - ESO Garching                            *

 010514		last modif

***********************************************************************
*/


# include    <idi.h>             
# include    <idistruct.h>
# include    <proto_idi.h>

static int      i, dysiz;

static CONF_DATA  *conf;
static MEM_DATA   *mem, *dmem;

/*

*/

/************************************************************************
* IIGPLY_C routine : polyline                                           *
*                                                                       *
* synopsis   IIGPLY_C (display , memid , x , y , np , color , style)    *
*                                                                       *
*       int display;      input   display identifier                    *
*       int memid;        input   memory identifier                     *
*       int x[];          input   X position list                       *
*       int y[];          input   Y position list                       *
*       int np;           input   number of points                      *
*       int color;        input   pixel color value                     *
*       int style         input   line style                            *
************************************************************************/

int IIGPLY_C (display,memid,x,y,np,color,style)

int  display, memid, np, color, style;
int  *x, *y;

{

if (ididev[display].opened == 0) return(DEVNOTOP);

conf = ididev[display].confptr;
if (conf->RGBmode == 1)
   {
   if (memid == 3) 
      memid = conf->overlay;
   else
      memid = 0;
   }
else
   {
   if ((memid < 0) || (memid >= conf->nmem)) return(ILLMEMID);
   }

mem = conf->memory[memid];
if (mem->mmbm == (char *) 0)		/* allocate memory if necessary  */
   i = allo_mem(display,mem,memid);
   
dysiz = ididev[display].ysize - 1;

/*  erase old polylines  */

if (color <= 0)
   {
   if (memid != conf->overlay)        /* if not overlay, display chan again */
      allrefr (display,mem,memid,1);

   else
      {
      for (i=0; i<conf->nmem; i++)
         {
         if (i != memid)
            {
            dmem = conf->memory[i];
            if (dmem->visibility == 1)
               {
               allrefr(display,dmem,i,1);
               goto clear_poly;
               }
            }
         }
      }

clear_poly:
   polyclear(display,mem,x,y,np);
   }


/*  or draw new polyline  */

else
   polyline(display,dysiz,mem,color,style,x,y,np);

return(II_SUCCESS);
}
/*

*/

/************************************************************************
* IIGTXT_C routine : plot text                                          *
*                                                                       *
* synopsis   IIGTXT_C (display , memid , txt , x0 , y0 , path , orient ,*
*                      color , txtsize);                                *
*                                                                       *
*       int  display;      input   display identifier                   *
*       int  memid;        input   memory identifier                    *
*       char txt[];        input   text string                          *
*       int  x0;           input   X position                           *
*       int  y0;           input   Y position                           *
*       int  path;         input   text path                            *
*       int  orient;       input   text orientation                     *
*       int  color;        input   pixel color value                    *
*       int  txtsize;      input   text size                            *
************************************************************************/

int IIGTXT_C (display , memid , txt , x0 , y0 , path , orient , color , 
          txtsize)

int  display , memid , x0 , y0 , path , orient , color , txtsize;
char txt[];

{
int   np;
ALPH_DATA  *alph;


if (ididev[display].opened == 0) return(DEVNOTOP);

dysiz = ididev[display].ysize - 1;
np = (int) strlen(txt);
conf = ididev[display].confptr;


/*  look, if we want the alpha memory ...    */

if (memid >= 90)
   {
   if (ididev[display].alpno < 90)
      return (NOALPHA);
   else
      {
      if (memid == 99) 
         alph = conf->alpmem[conf->memid];	/* use current alpha data */
      else
         {
         memid -= 100;
         if (conf->RGBmode == 1)
            {
            if (memid == 3)
               memid = conf->overlay;
            else
               memid = 0;
            }
         else
            {
            if ((memid < 0) || (memid >= conf->nmem)) return(ILLMEMID);
            }
         alph = conf->alpmem[memid];	/* use alpha data of that memory */
         }

      alptext(0,display,alph,txt,x0,y0);	/* only store the data */
      return(II_SUCCESS);
      }
   }

if (conf->RGBmode == 1)
   {
   if (memid == 3)
      memid = conf->overlay;
   else
      memid = 0;
   }
else
   {
   if ((memid < 0) || (memid >= conf->nmem)) return(ILLMEMID);
   }
mem = conf->memory[memid];

if (mem->mmbm == (char *) 0)		/* allocate memory if necessary  */
   i = allo_mem(display,mem,memid);
   
/*  erase old text  */

if (color <= 0)
   {
   if (memid != conf->overlay)	     /* if not overlay, display chan again */
      allrefr (display,mem,memid,1);
   else
      {
      for (i=0; i<conf->nmem; i++)
         {
         if (i != memid)
            {
            dmem = conf->memory[i];
            if (dmem->visibility == 1)
               {
               allrefr(display,dmem,i,1);
               goto clear_text;
               }
            }
         }
      }

clear_text:
   txtclear(display,mem,x0,y0,np);
   }


/*  or draw new text  */

else
   txtline(display,dysiz,mem,txt,x0,y0,path,orient,color,txtsize);

return(II_SUCCESS);
}

/*

*/

/************************************************************************
* IIGCPY_C routine : Copy graphics + text
*                                                                       *
* synopsis   IIGCPY_C (display,memid)
*                                                                       *
*       int display;      input   display identifier                    *
*       int memid;        input   memory identifier                     *
*       int append;       input   append flag (1 = yes)
************************************************************************/

int IIGCPY_C (display,memid,append)
int  display, memid, append;

{
if (ididev[display].opened == 0) return(DEVNOTOP);

conf = ididev[display].confptr;
if (conf->RGBmode == 1)
   {
   if (memid == 3)
      memid = conf->overlay;
   else
      memid = 0;
   }
else
   {
   if ((memid < 0) || (memid >= conf->nmem)) return(ILLMEMID);
   }

copy_over(display,memid,append);

return(II_SUCCESS);
}
