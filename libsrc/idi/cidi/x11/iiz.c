/* @(#)iiz.c	19.1 (ESO-DMD) 02/25/03 13:54:49 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* ----------------------------------------------------------------- */
/* ---------  IIZ -------------------------------------------------- */
/* ----------------------------------------------------------------- */

/* file iiz.c : contains the following routines
*
*   IIZWSC_C      : Write Memory Scroll;
*   IIZWZM_C      : Write Memory Zoom;
*   IIZRSZ_C      : Read  Memory Scroll and Zoom;
*   IIZWSZ_C      : Write  Memory Scroll and Zoom;
*
*   IIZWZP_C      : Write Display Zoom and Pan;      not supported
*   IIZRZP_C      : Read  Display Zoom and Pan;      not supported
*                                                                    *
**********************************************************************
* V 1.1  871201: P. Santin - Trieste Astronomical Observatory        *
* V 2.0  881202: K. Banse  - ESO                                     *
 
 010514		last modif

**********************************************************************
*/

# include    <idi.h>             
# include    <idistruct.h>
# include    <proto_idi.h>


static CONF_DATA   *conf;
static MEM_DATA    *mem;


/*

*/

/************************************************************************
* IIZWSC_C routine : write memory scroll                                *
*                                                                       *
* synopsis  IIZWSC_C (display , memlist , nmem , xscr , yscr);          *
*                                                                       *
*       int display;      input   display identifier                    *
*       int memlist[];    input   memory list                           *
*       int nmem;         input   number of memories                    *
*       int xscr;         input   x scroll                              *
*       int yscr;         input   y scroll                              *
************************************************************************/

int IIZWSC_C (display, memlist, nmem, xscr, yscr)

int display , nmem , xscr , yscr;
int memlist[];

{
int   i, curmem;


if (ididev[display].opened == 0) return(DEVNOTOP);
conf = ididev[display].confptr;

for (i=0; i<nmem; i++)
   {
   curmem = memlist[i];
   if (conf->RGBmode == 1)
      {
      if (curmem == 3)
         curmem = conf->overlay;
      else
         curmem = 0;                /* single memory for 3 channels */
      }
   else
      {
      if ((curmem < 0) || (curmem >= conf->nmem)) return(ILLMEMID);
      }

   mem = conf->memory[curmem];
   mem->xscroll = xscr;
   mem->yscroll = yscr;
   allrefr(display,mem,curmem,0);
   }

return(II_SUCCESS);
}
 
/*

*/
 
/************************************************************************
* IIZWZM_C routine : write memory zoom                                  *
*                                                                       *
* synopsis   IIZWZM_C (display , memlist , nmem , zoom);                *
*                                                                       *
*       int display;      input   display identifier                    *
*       int memlist[];    input   memory list                           *
*       int nmem;         input   number of memories                    *
*       int zoom;         input   zoom factor                           *
************************************************************************/

int IIZWZM_C (display , memlist , nmem , zoom)

int display , nmem , zoom;
int memlist[];

{
int  i, curmem;


if (ididev[display].opened == 0) return(DEVNOTOP);

if (zoom < 1) zoom = 1;
if (zoom > MAX_ZOOM) zoom = MAX_ZOOM;

conf = ididev [display].confptr;

for (i=0; i<nmem; i++)
   {
   curmem = memlist[i];
   
   curmem = memlist[i];
   if (conf->RGBmode == 1)
      {
      if (curmem == 3)
         curmem = conf->overlay;
      else
         curmem = 0;                /* single memory for 3 channels */
      }
   else
      {
      if ((curmem < 0) || (curmem >= conf->nmem)) return(ILLMEMID);
      }

   mem = conf->memory[curmem];
   if (zoom != mem->zoom)
      {
      mem->zoom = zoom;
      if (zoom != 1)
         {
         if (mem->zmbm == (char*) 0)
            allo_zmem(display,mem,curmem);
         copy_zmem(display,mem);
         }
      allrefr(display,mem,curmem,2);
      }
   }

return(II_SUCCESS);
}
 
/*

*/
 
/************************************************************************
* IIZRSZ_C routine : read memory scroll and zoom                        *
*                                                                       *
* synopsis   IIZRSZ_C (display , memid , xscr , yscr , zoom);           *
*                                                                       *
*       int display;      input   display identifier                    *
*       int memid;        input   memory identifier                     *
*       int *xscr;        output  X scroll                              *
*       int *yscr;        output  Y scroll                              *
*       int *zoom;        output  zoom factor                           *
************************************************************************/

int IIZRSZ_C (display , memid , xscr , yscr , zoom)

int display , memid , *xscr , *yscr , *zoom;

{

if (ididev[display].opened == 0) return(DEVNOTOP);

conf = ididev [display].confptr;
if (conf->RGBmode == 1)
   {
   if (memid == 3)
      memid = conf->overlay;
   else
      memid = 0;                /* single memory for 3 channels */
   }
else
   {
   if ((memid < 0) || (memid >= conf->nmem)) return(ILLMEMID);
   }
 
mem = conf->memory[memid];
*xscr = mem->xscroll;		/* read scroll & zoom parameters */
*yscr = mem->yscroll;
*zoom = mem->zoom;

return(II_SUCCESS);
}
 
/*

*/
 
/************************************************************************
* IIZWSZ_C routine : write memory scroll and zoom                        *
*                                                                       *
* synopsis   IIZWSZ_C (display , memid , xscr , yscr , zoom);           *
*                                                                       *
*       int display;      input   display identifier                    *
*       int memid;        input   memory identifier                     *
*       int xscr;        input  X scroll                              *
*       int yscr;        input  Y scroll                              *
*       int zoom;        input  zoom factor                           *
************************************************************************/

int IIZWSZ_C (display , memid , xscr , yscr , zoom)

int display , memid , xscr , yscr , zoom;

{
int  old_zoom;

if (ididev[display].opened == 0) return(DEVNOTOP);

conf = ididev [display].confptr;
if (conf->RGBmode == 1)
   {
   if (memid == 3)
      memid = conf->overlay;
   else
      memid = 0;                /* single memory for 3 channels */
   }
else
   {
   if ((memid < 0) || (memid >= conf->nmem)) return(ILLMEMID);
   }

mem = conf->memory[memid];

mem->xscroll = xscr;
mem->yscroll = yscr;

if (zoom < 1) zoom = 1;
if (zoom > MAX_ZOOM) zoom = MAX_ZOOM;

old_zoom = mem->zoom;			/* save previous zoom factor */
mem->zoom = zoom;
if (zoom != 1)
   {
   if (mem->zmbm == (char*) 0)
      allo_zmem(display,mem,memid);
   copy_zmem(display,mem);		/* fill zoom memory */
   }

if (zoom != old_zoom)			/* zoom factor changed? */
   allrefr(display,mem,memid,2);
else
   allrefr(display,mem,memid,0);

return(II_SUCCESS);
}
 
