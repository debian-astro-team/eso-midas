$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.LIBSRC.IDI.CIDI.X11]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:57 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libidi idilocal1.obj,idilocal2.obj,idilocal3.obj,idilocal4.obj,idiutil1.obj,idiutil2.obj,iic.obj,iid1.obj,iid2.obj,iie.obj,iig.obj,iii.obj,iil.obj,iim.obj,iir.obj,iis.obj,iiz.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
