/* @(#)iis.c	19.1 (ESO-DMD) 02/25/03 13:54:49 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

 

/* file IIS.C : contains the following routines
 
    IISSIN_C: Send system info 
                                                                      
V 1.0  940802: KB	Creation

 010515		last modif

*/


extern int oshchdir();


# include    <idi.h>             
# include    <idistruct.h>
# include    <proto_idi.h>

extern char DATA_PATH[328];

/*

*/

/************************************************************************
* IISSIN_C routine: send system information
*                                                                       *
* synopsis  IISSIN_C (display , flag, cbuf)
*                                                                       *
*       int display;      input   display identifier                    *
*       int flag;	  input	  action flag
*       char *cbuf;	  input	  infor depending on `flag'
************************************************************************/

int   IISSIN_C (display , flag, cbuf)

int display, flag;
char *cbuf;

{

int   m;

if (flag == 1) 
   {
   m = oshchdir(cbuf);
   }
else if (flag == 2) 
   {
   strcpy(DATA_PATH,cbuf);
   }


return(II_SUCCESS);
}
