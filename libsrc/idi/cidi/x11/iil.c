/* @(#)iil.c	19.1 (ESO-DMD) 02/25/03 13:54:48 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* ----------------------------------------------------------------- */
/* ---------  IIL -------------------------------------------------- */
/* ----------------------------------------------------------------- */

/* file iil.c : contains the following routines
*
*   IILWIT_C      : Write Intensity Transformation Table;
*   IILRIT_C      : Read  Intensity Transformation Table;
*   IILWLT_C      : Write Video Look-up Table;
*   IILRLT_C      : Read  Video Look-up Table;
*   IILSBV_C      : Set Bar Visibility;
*
* V 1.1  871201: P. Santin - Trieste Astronomical Observatory  
* V 2.0  881221: K. Banse - ESO  Garching       

 020527		last modif

**********************************************************************
*/

# include    <idi.h>             
# include    <idistruct.h>
# include    <proto_idi.h>


static CONF_DATA  *conf;
static MEM_DATA   *mem;
static LUT_DATA   *lut;
static ITT_DATA   *itt;

/*

*/

/************************************************************************
* IILWIT_C routine : write intensity transformation table               *
*                                                                       *
* synopsis   IILWIT_C (display , memid , ittn , ittstart , ittlen ,     *
*                      ittdata);                                        *
*                                                                       *
*       int   display;      input   display identifier                  *
*       int   memid;        input   memory identifier                   *
*       int   ittn;         input   ITT identifier                      *
*       int   ittstart;     input   ITT offset                          *
*       int   ittlen;       input   ITT length                          *
*       float *ittdata;     input   ITT data                            *
************************************************************************/

int IILWIT_C (display, memid, ittn, ittstart, ittlen, ittdata)

int display , memid , ittn , ittstart , ittlen;
float ittdata [];

{
register float   fq;

int  no;
register int i, k;



no = ididev[display].screen;
if ((Xworkst[no].visual != 4) && (Xworkst[no].ownlut != 1)) return(II_SUCCESS);

if (ididev[display].opened == 0) return(DEVNOTOP);
if (ittstart + ittlen > Xworkst[no].lutlen) return(ITTLENERR);

/* store ITT values */

conf = ididev[display].confptr;
if ((memid < 0) || (memid>= conf->nmem)) return(ILLMEMID);

mem = conf->memory[memid];
itt = mem->ittpntr;
fq = Xworkst[no].lutlen - 1.0;

for (i=0,k=ittstart; i<ittlen; i++,k++)
   itt->val[k] = (int) (ittdata[i] * fq);		/* => [0,LUTsize-1]  */

itt->vis = 1;

lut = ididev[display].lookup;
wr_lut(display,lut,lut->vis);		/* make the ITT visible  */

return(II_SUCCESS);
}

/*

*/

/************************************************************************
* IILRIT_C routine : read intensity transformation table                *
*                                                                       *
* synopsis   IILRIT_C (display , memid , ittn , ittstart , ittlen ,     *
*                      ittdata);                                        *
*                                                                       *
*       int   display;      input   display identifier                  *
*       int   memid;        input   memory identifier                   *
*       int   ittn;         input   ITT identifier                      *
*       int   ittstart;     input   ITT offset                          *
*       int   ittlen;       input   ITT length                          *
*       float *ittdata;     input   ITT data                            *
************************************************************************/

int IILRIT_C (display, memid, ittn, ittstart, ittlen, ittdata)

int display , memid , ittn , ittstart , ittlen;
float ittdata [];

{
register float fq, rval;

int   no;
register int  i, k;



no = ididev[display].screen;
if ((Xworkst[no].visual != 4) && (Xworkst[no].ownlut != 1)) return(II_SUCCESS);

if (ididev[display].opened == 0) return(DEVNOTOP);
if (ittstart + ittlen > Xworkst[no].lutlen) return(ITTLENERR);

conf = ididev[display].confptr;
if ((memid < 0) || (memid>= conf->nmem)) return(ILLMEMID);
mem = conf->memory[memid];
itt = mem->ittpntr;

/*  now copy from itt structure to return data  */

fq = 1.0 / (Xworkst[no].lutlen - 1.0);
for (i=0,k=ittstart; i<ittlen; i++,k++)
   {
   rval = (float) itt->val[k];
   ittdata[i] = rval * fq;			/* => [0.,1.]  */
   }

return(II_SUCCESS);
}

/*

*/

/************************************************************************
* IILWLT_C routine : write video look-up table                          *
*                                                                       *
* synopsis   IILWLT_C (display , lutn , lutstart , lutlen , lutdata)    *
*                                                                       *
*       int   display;      input   display identifier                  *
*       int   lutn;         input   look-up identifier                  *
*       int   lutstart;     input   look-up offset                      *
*       int   lutlen;       input   look-up length                      *
*       float *lutdata;     input   look-up data                        *
************************************************************************/

int IILWLT_C (display , lutn , lutstart , lutlen , lutdata)

int display , lutn , lutstart , lutlen;
float lutdata [];

{
register float fq;

int     no;
register int  ltlen2, i, k, icolr, icolg, icolb;



no = ididev[display].screen;
if ((Xworkst[no].visual != 4) && (Xworkst[no].ownlut != 1)) return(II_SUCCESS);

if (ididev[display].opened == 0) return(DEVNOTOP);

if (lutn < -1) return(LUTIDERR);
if (lutstart + lutlen > Xworkst[no].lutlen) return(LUTLENERR);

lut = ididev[display].lookup;

ltlen2 = lutlen + lutlen;


/* store LUT values for R G B */

fq = Xworkst[no].lutfct;
for (i=0,k=lutstart; i<lutlen; i++,k++)
   {
   icolr = fq * lutdata[i];
   icolg = fq * lutdata[lutlen + i];
   icolb = fq * lutdata[ltlen2 + i];
   lut->lutr[k] = icolr;
   lut->lutg[k] = icolg;
   lut->lutb[k] = icolb;
   }
   
lut->vis = 1;
wr_lut(display,lut,lut->vis);

return(II_SUCCESS);
}

/*

*/

/************************************************************************
* IILRLT_C routine : read video look-up table                           *
*                                                                       *
* synopsis   IILRLT_C (display , lutn , lutstart , lutlen , lutdata)    *
*                                                                       *
*       int   display;      input   display identifier                  *
*       int   lutn;         input   look-up identifier                  *
*       int   lutstart;     input   look-up offset                      *
*       int   lutlen;       input   look-up length                      *
*       float *lutdata;     output  look-up data                        *
************************************************************************/

int IILRLT_C (display , lutn , lutstart , lutlen , lutdata)

int display , lutn , lutstart , lutlen;
float lutdata [];

{
register float  fq;

int  no;
register int  ltlen2, i, k;



no = ididev[display].screen;
if ((Xworkst[no].visual != 4) && (Xworkst[no].ownlut != 1)) return(II_SUCCESS);

if (ididev[display].opened == 0) return(DEVNOTOP);

if (lutn < -1) return(LUTIDERR);
if (lutstart + lutlen > Xworkst[no].lutlen) return(LUTLENERR);

lut = ididev[display].lookup;
ltlen2 = lutlen + lutlen;
fq = Xworkst[no].lutinv;


/* lutn = 99, means plot colours ... */

if (lutn == 99)
   {
   LUT_DATA   plut;

   rd_pllut(display,&plut);		/* read LUT values for plot colours */


   for (i=0; i<PLOT_COLOURS; i++)	/* copy from pLUT to return data  */
      {
      lutdata[i] = fq * plut.lutr[i];
      lutdata[lutlen + i] = fq * plut.lutg[i];
      lutdata[ltlen2 + i] = fq * plut.lutb[i];
      }
   }

else
   {
   rd_lut(display,lut);			/* read LUT values for R G B */

   for (i=0,k=lutstart; i<lutlen; i++,k++)  /* copy from LUT to return data */
      {
      lutdata[i] = fq * lut->lutr[k];
      lutdata[lutlen + i] = fq * lut->lutg[k];
      lutdata[ltlen2 + i] = fq * lut->lutb[k];
      }
   }
 
return(II_SUCCESS);
}

/*

*/

/************************************************************************
* IILSBV_C routine : set bar visibility                                 *
*                                                                       *
* synopsis   IILSBV_C (display , memid , vis);                          *
*                                                                       *
*       int display;      input   display identifier                    *
*       int memid;        input   memory identifier                     *
*       int vis;          input   bar visibility [0/1]                  *
************************************************************************/

int IILSBV_C(display, memid, vis)
int  display, memid, vis;

{
INTBAR      *bar;
MEM_DATA    *ovmem;

int  i, no;


no = ididev[display].screen;
if (ididev[display].opened == 0) return(DEVNOTOP);

bar = ididev[display].bar;
if (vis == 1)
   {
   if (bar->wp == 0)
      crelutbar(display,bar);
   else
      vislutbar(display,bar,vis);
   }

else
   {
   if (bar->vis == 1) 
      {
      conf = ididev[display].confptr;
      if (conf->RGBmode == 1)
         {
         if (memid == 3)
            memid = conf->overlay;
         else
            memid = 0;                /* single memory for 3 channels */
         }
      else
         {
         if ((memid < 0) || (memid >= conf->nmem)) return(ILLMEMID);
         }
      mem = conf->memory[memid];

      vislutbar(display,bar,vis);
      allrefr(display,mem,memid,1);
      i = conf->overlay;
      if (memid != i)
         {
         ovmem = conf->memory[i];
         allrefr(display,ovmem,i,1);
         }
      }
   }

bar->vis = vis;

return(II_SUCCESS);
}

