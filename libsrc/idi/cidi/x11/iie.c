/*===========================================================================
  Copyright (C) 1995-2007 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* ----------------------------------------------------------------- */
/* ---------  IIE -------------------------------------------------- */
/* ----------------------------------------------------------------- */

/* file iie.c : contains the following `Escape' routines
*
*   IIEGDB_C      : Get a datablock from IDI server
*   IIESDB_C      : Send a datablock to IDI server
* 
*   show_misc	  : Display various data from idistruct.h
* 
* V 1.0 950203  Klaus Banse - ESO Garching                              

 070523		last modif

*************************************************************************
*/

# include    <idi.h>             
# include    <idistruct.h>
# include    <proto_idi.h>

# include    <stdio.h>


/*

*/

/************************************************************************
* IIEGDB_C routine : get a block of data from IDI server
*                                                                       *
* synopsis   IIEGDB_C(display,flag,auxid,cbuf,ibuf,rbuf)
*                                                                       *
*       int display;      input    display identifier                   *
*       int flag;	  input    for different options
*				   = 1 for memory data block
*       int auxid;	  input    object id, 
*				   for flag = 1, it's the memory id
*       char *cbuf;       output   char. info
*       int *ibuf;        output   int info
*       float *rbuf;      output   float info
*     
*  the no. of elements in ibuf + rbuf depends upon flag
*  flag = 1,  ibuf 17 elements, rbuf 8 elements
*             corresponding to the IDIMEMC keyword*
************************************************************************/

int IIEGDB_C(display,flag,auxid,cbuf,ibuf,rbuf)

int  display, flag, auxid, *ibuf;
char *cbuf;
float *rbuf;

{

CONF_DATA      *conf;
MEM_DATA       *mem;
ITT_DATA       *itt;
TLIST          *tlist;
GLIST          *glist;

int   mm, i;




if (flag == 1)			/* handle the memory data stuff */
   {
   if (ididev[display].opened == 0) return(DEVNOTOP);

   conf = ididev[display].confptr;
   if (conf->RGBmode == 1)
      {
      if (auxid == 3)
         auxid = conf->overlay;
      else
         auxid = 0;                /* single memory for 3 channels */
      }
   else
      {
      if ((auxid < 0) || (auxid >= conf->overlay)) return(ILLMEMID);
      }

   
   mem = conf->memory[auxid];

   (void)strcpy(cbuf,mem->frame);

   *ibuf++ = mem->load_dir;
   *ibuf++ = mem->sspx;
   *ibuf++ = mem->sspy;
   *ibuf++ = mem->nsx;
   *ibuf++ = mem->nsy;
   *ibuf++ = mem->sfpx;
   *ibuf++ = mem->sfpy;
   *ibuf++ = mem->xscale;
   *ibuf++ = mem->yscale;
   *ibuf++ = mem->xscroll;
   *ibuf++ = mem->yscroll;
   *ibuf++ = mem->zoom;
   *ibuf++ = mem->zoom;			/* same zoom in y */
   *ibuf++ = mem->source;
   itt = mem->ittpntr;
   if (itt != (ITT_DATA *)0)
      *ibuf++ = itt->vis;
   else
      *ibuf++ = 0;
   *ibuf = 0;
   glist = mem->gpntr;
   if ((glist != (GLIST *)0) && (glist->geln > 0))
      *ibuf = 1;
   else 
      {
      tlist = mem->tpntr;
      if ((tlist != (TLIST *)0) && (tlist->teln > 0)) *ibuf = 1;
      }
   ibuf++;
   *ibuf = mem->plane_no;

   for (mm=0; mm<8; mm++) *rbuf++ = mem->rbuf[mm];
   }

else if (flag == 2)			/* get complete setup */
   {
   char  *mytype, cc;

   mytype = cbuf;

   for (i=0; i<MAX_DEV; i++)		/* return w. type and no. */
      {
      if (ididev[i].devname[0] == '\0')
         {
         *mytype++ = ' '; 		/* indicate unused slot */
         *mytype++ = ' '; 
         *ibuf++ = -9;
         *ibuf++ = -9;
         }
      else
         {
         cc = ididev[i].devname[6];
         if (cc == '\0') cc = 'i';		/* nothing = image */
         *mytype++ = cc;
         *mytype++ = ididev[i].devname[5];

         /* printf("IIE: links at %d: %d, %d\n",
            i,ididev[i].link[0],ididev[i].link[1]); */

         *ibuf++ = ididev[i].link[0];		/* also save links */
         *ibuf++ = ididev[i].link[1];
         }
      }
   *mytype = '\0';
   }
         

return(II_SUCCESS);
}
/*

*/

/************************************************************************
* IIESDB_C routine : send a block of data to IDI server
*                                                                       *
* synopsis   IIESDB_C(display,flag,auxid,cbuf,ibuf,rbuf)
*                                                                       *
*       int display;      input    display identifier                   *
*       int flag;         input    for different options
*                                  = 1 for memory data block
*       int auxid;        input    object id,
*                                  for flag = 1, it's the memory id
*                                  for flag = 2, it's ?
*       char *cbuf;       input    char. info
*       int *ibuf;        input    int info
*       float *rbuf;      input    float info
*
*  the no. of elements in ibuf + rbuf depends upon flag
*  flag = 1:  ibuf 17 elements, rbuf 8 elements
*  flag = 2:  ibuf 2 elements
*
************************************************************************/

int IIESDB_C(display,flag,auxid,cbuf,ibuf,rbuf)

int  display, flag, auxid, *ibuf;
char *cbuf;
float *rbuf;

{

CONF_DATA      *conf;
MEM_DATA       *mem;

int   mm;

int  show_misc();




if (ididev[display].opened == 0) return(DEVNOTOP);


if (flag == 1)                  /* handle the memory data stuff */
   {
   conf = ididev[display].confptr;
   if ((auxid < 0) || (auxid >= conf->overlay)) return(ILLMEMID);

   mem = conf->memory[auxid];

   (void)strcpy(mem->frame,cbuf);

   ibuf++;			/* we don't need to update mem->load_dir */
   mem->sspx = *ibuf++;
   mem->sspy = *ibuf++;
   mem->nsx = *ibuf++;
   mem->nsy = *ibuf++;
   mem->sfpx = *ibuf++;
   mem->sfpy = *ibuf++;
   mem->xscale = *ibuf++;
   mem->yscale = *ibuf++;

   /* omit mem->xscroll, yscroll, zoom (2*), source, ITTvis and draw-flag  */

   mem->plane_no = *(ibuf+7);


   for (mm=0; mm<8; mm++) mem->rbuf[mm] = *rbuf++;
   }

else if (flag > 100)
   {
   flag -= 100;
   if (flag > 100)		/* > 200  => display idi structs */
      {
      flag -= 100;
      show_misc(display,flag,auxid,cbuf,ibuf,rbuf);
      }
   else
      do_misc(display,flag,auxid,cbuf,ibuf,rbuf);
   }


return(II_SUCCESS);
}
/*

*/

int show_misc(display,flag,auxid,cbuf,ibuf,rbuf)
int  display, flag, auxid, *ibuf;
char *cbuf;
float *rbuf;

{

if (flag == 1)		/* display ididev */
   {
   printf("struct ididev[%d]\n---------\n",display);

   printf("devname = %s\t\tdevtyp = %c\n",
       ididev[display].devname,ididev[display].devtyp);
   printf("ref = %c\t\t\tscreen = %d\n",
       ididev[display].ref,ididev[display].screen);
   printf("xsize = %d\t\tysize = %d\t\tdepth = %d\n",
       ididev[display].xsize,ididev[display].ysize,ididev[display].lutoff);
   printf("ncurs = %d\t\tlutsect = %d\t\tlutoff = %d\n",
       ididev[display].ncurs,ididev[display].lutsect,ididev[display].lutoff);
   printf("n_inter = %d\t\texit trigger = %d\tinter_mask = %ld\n",
    ididev[display].n_inter,ididev[display].trigger,ididev[display].inter_mask);
   printf("alpno = %d\t\talphx = %d\talphy = %d\n",
       ididev[display].alpno,ididev[display].alphx,ididev[display].alphy);
   printf("alphxscal = %d\t\talphlinsz = %d\n",
       ididev[display].alphxscal,ididev[display].alphlinsz);
   printf("backpix = 0x%lx\t\tlink[0] = %d\tlink[1] = %d\n",
       ididev[display].backpix,ididev[display].link[0],ididev[display].link[1]);
   }

else if (flag == 2)			/* display Xworkst */
   {
   int   mm;

   mm = ididev[display].screen;

   printf("struct Xworkst[%d]\n-----------\n",mm);

   printf("name = %s\t\tMidas unit = %4.4s\n",
       Xworkst[mm].name,Xworkst[mm].miduni);
   printf("visual = %d\t\tRGBord = %d\n",
       Xworkst[mm].visual,Xworkst[mm].RGBord);
   printf("lutflag = %d\t\townlut = %d\t\tauxcol = %d\n",
       Xworkst[mm].lutflag,Xworkst[mm].ownlut,Xworkst[mm].auxcol);
   printf("width = %d\t\theight = %d\t\tdepth = %d\n",
       Xworkst[mm].width,Xworkst[mm].height,Xworkst[mm].depth);
   if (Xworkst[mm].nobyt > 0)
      printf("bytes_pixel = %d\t\tflag24 = %d,%d,%d\n",
         Xworkst[mm].nobyt,
         Xworkst[mm].flag24[0],Xworkst[mm].flag24[1],Xworkst[mm].flag24[2]);
   else
      printf("bits_pixel = %d\t\tflag24 = %d,%d,%d\n",
         -Xworkst[mm].nobyt,
         Xworkst[mm].flag24[0],Xworkst[mm].flag24[1],Xworkst[mm].flag24[2]);
   printf("fixpix = \n%d, %d, %d, %d, %d, %d, %d, %d, %d\n",
       Xworkst[mm].fixpix[0], Xworkst[mm].fixpix[1],
       Xworkst[mm].fixpix[2], Xworkst[mm].fixpix[3],
       Xworkst[mm].fixpix[4], Xworkst[mm].fixpix[5],
       Xworkst[mm].fixpix[6], Xworkst[mm].fixpix[7],
       Xworkst[mm].fixpix[8]);
   printf("mapin[0], mapin[1], mapin[2] = %d, %d, %d\n",
       Xworkst[mm].mapin[0], Xworkst[mm].mapin[1],
       Xworkst[mm].mapin[2]);
   printf("mapout[0], mapout[1], mapout[2] = %d, %d, %d\n",
       Xworkst[mm].mapout[0], Xworkst[mm].mapout[1],
       Xworkst[mm].mapout[2]);
   printf("nolut = %d\t\tlutsize = %d\t\tlutlen = %d\n",
       Xworkst[mm].nolut,Xworkst[mm].lutsize,Xworkst[mm].lutlen);
   printf("lutfct = %f\t\tlutinv = %f\n",
       Xworkst[mm].lutfct,Xworkst[mm].lutinv);
   printf("blackpixel = 0x%lx\t\twhitepixel = 0x%lx\tblmask = 0x%x\n",
       Xworkst[mm].black,Xworkst[mm].white,Xworkst[mm].blmask);
   }

else	 		/* display mem_data */
   {
   CONF_DATA      *conf;
   MEM_DATA       *mem;

   conf = ididev[display].confptr;
   if ((auxid < 0) || (auxid >= conf->overlay)) return(ILLMEMID);

   mem = conf->memory[auxid];
   printf("struct mem_data[%d]\n-----------\n",auxid);

   printf("pixmap = %d\t\tvisibility = %d\t\tmemtype = %d\n",
       mem->pixmap,mem->visibility,mem->type);
   printf("xsize = %d\t\tysize = %d\t\tdepth = %d\n",
       mem->xsize,mem->ysize,mem->depth);
   printf("xwoff = %d\tywoff = %d\txwdim = %d\tywdim = %d\n",
       mem->xwoff,mem->ywoff,mem->xwdim,mem->ywdim);
   printf("load_dir = %d\t\tlut_id = %d\n",
       mem->load_dir,mem->lut_id);
   printf("xscroll = %d\t\tyscroll = %d\t\tzoom = %d\n",
       mem->xscroll,mem->yscroll,mem->zoom);
   printf("xscale = %d\t\tyscale = %d\n",
       mem->xscale,mem->yscale);
   printf("sspx = %d\tsspy = %d\tnsx = %d\tnsy = %d\n",
       mem->sspx,mem->sspy,mem->nsx,mem->nsy);
   printf("sfpx = %d\tsfpy = %d\tsource = %d\tplane_no = %d\n",
       mem->sfpx,mem->sfpy,mem->source,mem->plane_no);
   printf("frame = %s\n",mem->frame);
   printf("start, end, ... = \n%f, %f, %f, %f, %f , %f, %f, %f\n",
       mem->rbuf[0], mem->rbuf[1], mem->rbuf[2], mem->rbuf[3],
       mem->rbuf[4], mem->rbuf[5], mem->rbuf[6], mem->rbuf[7]);
   }

return 0;
}


