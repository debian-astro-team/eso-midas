/* @(#)iic.c	19.1 (ESO-DMD) 02/25/03 13:54:47 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/



/* ----------------------------------------------------------------- */
/* ---------  IIC -------------------------------------------------- */

/* ----------------------------------------------------------------- */
/* !!  all cursor coords are relative to the associated memory !!    */
/* !!  (or to the ididev in case of memory = -1)               !!    */
/* !!  (0,0 = bottom left)                                           */
/* ----------------------------------------------------------------- */

/* file IIC.C : contains the following routines
*
*   IICINC_C      : Initialize Cursor;
*   IICSCV_C      : Set Cursor Visibility;
*   IICRCP_C      : Read Cursor Position;
*   IICWCP_C      : Write Cursor Position;
*
***********************************************************************
* V 1.1  871201: P. Santin - Trieste Astronomical Observatory         *
* V 2.0  881208: K. Banse - ESO Garching                              *
 
 010514		last modif

***********************************************************************
*/


# include    <idi.h>             
# include    <idistruct.h> 
# include    <proto_idi.h> 

static int  i, dxsize, dysize;

static  CURS_DATA  *curs;
static  CONF_DATA  *conf;
static  MEM_DATA   *mem;


/************************************************************************
* IICINC_C routine : initialize cursor                                  *
*                                                                       *
* synopsis   IICINC_C (display , memid , curn , cursh , curcol , xcur , *
*                      ycur);                                           *
*                                                                       *
*       int display;      input   display identifier                    *
*       int memid;        input   memory identifier                     *
*       int curn;         input   cursor number                         *
*       int cursh;        input   cursor shape                          *
*       int curcol;       input   cursor color                          *
*       int xcur;         input   X cursor position                     *
*       int ycur;         input   Y cursor position                     *
************************************************************************/

int IICINC_C (display , memid , curn , cursh , curcol , xcur , ycur)

int display , memid , curn , cursh , curcol , xcur , ycur;

{

if (ididev[display].opened == 0) return(DEVNOTOP);

/* check cursor number */

if ((curn < 0) || (curn >= ididev[display].ncurs)) return(ILLCURID);

conf = ididev [display].confptr;
curs = ididev[display].cursor[curn];

/* set cursor parameters */

curs->sh = cursh;
curs->col = curcol;
curs->vis = 0;
if (xcur >= 0)
   {
   curs->xpos = xcur;
   curs->ypos = ycur;
   }

return(II_SUCCESS);
}

/************************************************************************
* IICSCV_C routine : set cursor visibility                              *
*                                                                       *
* synopsis   IICSCV_C (display , curn , vis);                           *
*                                                                       *
*       int display;      input   display identifier                    *
*       int curn;         input   cursor number                         *
*       int vis;          input   visibility [1 / 0]                    *
*************************************************************************/

int IICSCV_C (display , curn , vis)

int display , curn , vis;

{

if (ididev[display].opened == 0) return(DEVNOTOP);

					/* check cursor number */
if ((curn < 0) || (curn >= ididev[display].ncurs)) return(ILLCURID);
   
curs = ididev[display].cursor[curn];

if (curs->sh == -1) return(CURNOTDEF);
if (curs->vis == vis) return(II_SUCCESS);		/* nothing to do  */

dysize = ididev[display].ysize - 1;

if (vis == 0)				/* so curs->vis = 1  */
   draw_curs(display,2,dysize,curn,curs->xpos,curs->ypos,curs->sh,curs->col);

else					/* so curs->vis = 0  */
   draw_curs(display,0,dysize,curn,curs->xpos,curs->ypos,curs->sh,curs->col);

curs->vis = vis;			/* curs->vis updated ...  */
 
return(II_SUCCESS);
}

/************************************************************************
* IICRCP_C routine : read cursor position                               *
*                                                                       *
* synopsis  IICRCP_C (display, inmemid, curn, xcur, ycur, outmemid)*
*                                                                       *
*       int display;      input   display identifier                    *
*       int inmemid;      input   input memory identifier               *
*       int curn;         input   cursor number                         *
*       int *xcur;        output  X cursor position                     *
*       int *ycur;        output  Y cursor position                     *
*       int *outmemid;    output  output memory identifier              *
*************************************************************************/

int IICRCP_C (display , inmemid , curn , xcur , ycur , outmemid)

int display , inmemid , curn , *xcur , *ycur , *outmemid;

{

if (ididev[display].opened == 0) return(DEVNOTOP);

conf = ididev [display].confptr;

				/* check cursor number */
if ((curn < 0) || (curn >= ididev[display].ncurs)) return(ILLCURID);

curs = ididev[display].cursor[curn];
if (curs->sh == -1)              /* shape = -1 : not initialized */
   return(CURNOTDEF);

*xcur = curs->xpos;
*ycur = curs->ypos;
   
/* search current memory for cursor */

*outmemid = 0;
for (i=0; i<conf->nmem; i++)
   {
   mem = conf->memory[i];
   if (mem->visibility == 1) 
      {
      *outmemid = i;
      break;
      }
   }

return(II_SUCCESS);
}

/************************************************************************
* IICWCP_C routine : write cursor position                              *
*                                                                       *
* synopsis   IICWCP_C (display , memid , curn , xcur , ycur);           *
*                                                                       *
*       int display;      input   display identifier                    *
*       int memid;        input   memory identifier                     *
*       int curn;         input   cursor number                         *
*       int xcur;         input   X cursor position                     *
*       int ycur;         input   Y cursor position                     *
************************************************************************/

int IICWCP_C (display , memid , curn , xcur , ycur)

int display , memid , curn , xcur , ycur;

{

if (ididev[display].opened == 0) return(DEVNOTOP);

conf = ididev [display].confptr;

						/* check cursor number */
if ((curn < 0) || (curn >= ididev[display].ncurs))
   return(ILLCURID);

dxsize = ididev[display].xsize - 1;
dysize = ididev[display].ysize - 1;

/* printf("IICCWP: xcur, ycur, dxsize, dysize = %d, %d, %d, %d\n",
       xcur,ycur,dxsize,dysize); */

curs = ididev[display].cursor[curn];
if (curs->sh == -1) return(CURNOTDEF);

/* store new cursor position */

if (xcur < 0)
   xcur = 0;
else
   {
   if (xcur > dxsize) xcur = dxsize;
   }
   
if (ycur < 0)
   ycur = 0;
else
   {
   if (ycur > dysize) ycur = dysize;
   }

curs->xpos = xcur;
curs->ypos = ycur;

return(II_SUCCESS);

}
