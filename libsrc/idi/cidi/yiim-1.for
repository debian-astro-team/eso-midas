C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or
C modify it under the terms of the GNU General Public License as
C published by the Free Software Foundation; either version 2 of
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public
C License along with this program; if not, write to the Free
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge,
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C       Internet e-mail: midas@eso.org
C       Postal address: European Southern Observatory
C                       Data Management Division
C                       Karl-Schwarzschild-Strasse 2
C                       D 85748 Garching bei Muenchen
C                       GERMANY
C===========================================================================

      SUBROUTINE IIMSTW(DSPLAY,CHAN,LOADFL,SZX,SZY,DEPTH,
     +                  XBEG,YBEG,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY
      INTEGER     CHAN,LOADFL,SZX,SZY,DEPTH,XBEG,YBEG,IDST
C
      CALL IIM1(DSPLAY,CHAN,LOADFL,SZX,SZY)
      CALL IIM2(DEPTH,XBEG,YBEG,IDST)
C
      RETURN
      END
C
      SUBROUTINE IIMWMY(DSPLAY,CHAN,PIX,SIZE,DEPTH,PACK,XBEG,YBEG,IDST)
C
      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN
      INTEGER     PIX,SIZE,DEPTH,PACK,XBEG,YBEG,IDST
C
      CALL IIM1(DSPLAY,CHAN,PIX,SIZE,0)
      CALL IIM3(DEPTH,XBEG,YBEG,IDST)
C
      RETURN
      END
C
      SUBROUTINE IIMRMY(DSPLAY,CHAN,SIZE,XBEG,YBEG,DEPTH,PACK,
     +                  ITTFLG,PIX,IDST)

      IMPLICIT NONE
C
      INTEGER     DSPLAY,CHAN,ITTFLG,PIX
      INTEGER     SIZE,DEPTH,PACK,XBEG,YBEG,IDST
C
      CALL IIM1(DSPLAY,CHAN,SIZE,XBEG,YBEG)
      CALL IIM4(DEPTH,PACK,ITTFLG,PIX,IDST)
C
      RETURN
      END
C


