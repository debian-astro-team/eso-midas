C @(#)iii.for	19.1 (ES0-DMD) 02/25/03 13:55:01
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE 
     +  IIIENI(DSPLAY,INTTYP,INTID,OBJTYP,OBJID,INTOP,EXTRIG,IDST)
C	
C
      IMPLICIT NONE
C	
      INTEGER   DSPLAY,INTTYP,INTID,OBJTYP,OBJID
      INTEGER   INTOP,EXTRIG,IDST
C 
      INCLUDE  'MID_INCLUDE:IDIDEV.INC'
C		
      IF (IDINUM.EQ.0) THEN
         CALL GD8037
     +  (DSPLAY,INTTYP,INTID,OBJTYP,OBJID,INTOP,EXTRIG,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW037
     +  (DSPLAY,INTTYP,INTID,OBJTYP,OBJID,INTOP,EXTRIG,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END
C 
C 
C -----------------------------------------------------------------
C 
C 
      SUBROUTINE IIIEIW(DSPLAY,TRIGST,IDST)
C	
      IMPLICIT NONE
C	
      INTEGER   DSPLAY,TRIGST(1),IDST
C 
      INCLUDE  'MID_INCLUDE:IDIDEV.INC'
C		
      IF (IDINUM.EQ.0) THEN
         CALL GD8038(DSPLAY,TRIGST,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW038(DSPLAY,TRIGST,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END
C

      SUBROUTINE IIISTI(DSPLAY,IDST)
C	
      IMPLICIT NONE
C	
      INTEGER   DSPLAY,IDST
C 
      INCLUDE  'MID_INCLUDE:IDIDEV.INC'
C		
      IF (IDINUM.EQ.0) THEN
         CALL GD8039(DSPLAY,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW039(DSPLAY,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END
C 
C 
C -----------------------------------------------------------------
C 
C 
      SUBROUTINE IIIQID(DSPLAY,INTTYP,INTID,DEVDSC,LL,IDST)
C
      IMPLICIT NONE
C 
      INTEGER   DSPLAY,INTTYP,INTID,LL,IDST
C	
      CHARACTER*(*)  DEVDSC
C 
      INCLUDE  'MID_INCLUDE:IDIDEV.INC'
C		
      IF (IDINUM.EQ.0) THEN
         CALL GD8040(DSPLAY,INTTYP,INTID,DEVDSC,LL,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW040(DSPLAY,INTTYP,INTID,DEVDSC,LL,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END

      SUBROUTINE IIIGLD(DSPLAY,LOCNO,JX,JY,IDST)
C
      IMPLICIT NONE
C	
      INTEGER   DSPLAY,LOCNO,JX,JY,IDST
C 
      INCLUDE  'MID_INCLUDE:IDIDEV.INC'
C		
      IF (IDINUM.EQ.0) THEN
         CALL GD8041(DSPLAY,LOCNO,JX,JY,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW041(DSPLAY,LOCNO,JX,JY,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END
C 
C 
C -----------------------------------------------------------------
C 
C 
      SUBROUTINE IIIGLE(DSPLAY,EVAL,JJ,IDST)
C 
      IMPLICIT NONE
C 
      INTEGER      DSPLAY,EVAL,JJ,IDST
C 
      INCLUDE  'MID_INCLUDE:IDIDEV.INC'
C		
      IF (IDINUM.EQ.0) THEN
         CALL GD8044(DSPLAY,EVAL,JJ,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW044(DSPLAY,EVAL,JJ,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
C 
      RETURN
      END

      SUBROUTINE IIIGSE(DSPLAY,EVAL,KSTR,JJ,IDST)
C 
      IMPLICIT NONE
C 
      INTEGER      DSPLAY,EVAL,JJ,IDST
C 
      CHARACTER*(*)    KSTR
C 
      INCLUDE  'MID_INCLUDE:IDIDEV.INC'
C		
      IF (IDINUM.EQ.0) THEN
         CALL GD8045(DSPLAY,EVAL,KSTR,JJ,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW045(DSPLAY,EVAL,KSTR,JJ,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END

      SUBROUTINE IIIGCE(DSPLAY,EVAL,KSTR,IDST)
C 
      IMPLICIT NONE
C 
      INTEGER      DSPLAY,EVAL,IDST
C 
      CHARACTER*(*)    KSTR
C 
      INCLUDE  'MID_INCLUDE:IDIDEV.INC'
C		
      IDST = 0
C 
      RETURN
      END
