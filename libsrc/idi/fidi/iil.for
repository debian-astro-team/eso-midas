C @(#)iil.for	19.1 (ES0-DMD) 02/25/03 13:55:01
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE IILWLT(DSPLAY,NLUT,ISTA,COUNT,RLUT,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,NLUT,ISTA,COUNT,IDST
C      
      REAL         RLUT(3,COUNT)
C      
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8025(DSPLAY,NLUT,ISTA,COUNT,RLUT,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW025(DSPLAY,NLUT,ISTA,COUNT,RLUT,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END      
C      
C      
C --------------------------------------------------------------------
C
C 
      SUBROUTINE IILRLT(DSPLAY,NLUT,ISTA,COUNT,RLUT,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,NLUT,ISTA,COUNT,IDST
C      
      REAL            RLUT(3,COUNT)
C
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8026(DSPLAY,NLUT,ISTA,COUNT,RLUT,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW026(DSPLAY,NLUT,ISTA,COUNT,RLUT,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END      

      SUBROUTINE IILWIT(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,NITT,ISTA,COUNT,IDST
C      
      REAL            RITT(1)
C      
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C 
      IF (IDINUM.EQ.0) THEN
         CALL GD8023(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW023(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END      
C      
C      
C --------------------------------------------------------------------
C
C 
      SUBROUTINE IILRIT(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,NITT,ISTA,COUNT,IDST
C      
      REAL            RITT(1)
C      
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C 
      IF (IDINUM.EQ.0) THEN
         CALL GD8024(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW024(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END      

      SUBROUTINE IILSBV(DSPLAY,CHAN,VISI,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,VISI,IDST
C      
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8049(DSPLAY,CHAN,VISI,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW049(DSPLAY,CHAN,VISI,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END      
