C @(#)iid.for	19.1 (ES0-DMD) 02/25/03 13:55:00
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE IIDOPN(DEVICE,DSPLAY,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,IDST
C      
      CHARACTER*(*)   DEVICE
C      
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C 
C  branch according to ImageDisplay + force IDINM again...
      IF (DEVICE(1:3).EQ.'gd8') THEN
         IDINUM = 0
         CALL GD8001(DEVICE,DSPLAY,IDST)
      ELSE IF (DEVICE(1:3).EQ.'MXW') THEN
         IDINUM = 10
         CALL MXW001(DEVICE,DSPLAY,IDST)
      ELSE
         IDINUM = -1
         IDST = 0
      ENDIF
C      
      RETURN
      END

      SUBROUTINE IIDCLO(DSPLAY,IDST)
C
      IMPLICIT NONE
C      
      INTEGER     DSPLAY,IDST
C      
      INCLUDE     'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8002(DSPLAY,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW002(DSPLAY,IDST)
      ELSE
         IDST = 0
      ENDIF
C      
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDRST(DSPLAY,IDST)
C 
      INTEGER  DSPLAY,IDST
C 
      INCLUDE     'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8003(DSPLAY,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW003(DSPLAY,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END

      SUBROUTINE IIDUPD(DSPLAY,IDST)
C 
      INTEGER    DSPLAY,IDST
C 
      INCLUDE   'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8004(DSPLAY,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW004(DSPLAY,IDST)
      ELSE
         IDST = 0
      ENDIF
C 
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDSEL(DSPLAY,CONFNO,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CONFNO,IDST
C            
      INCLUDE     'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8010(DSPLAY,CONFNO,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW010(DSPLAY,CONFNO,IDST)
      ELSE
         IDST = 0
      ENDIF
C      
      RETURN
      END

      SUBROUTINE IIDSDP(DSPLAY,CHANS,NOCHAN,LUTF,ITTF,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,IDST
      INTEGER      CHANS(1),NOCHAN,LUTF(1),ITTF(1)
C            
      INCLUDE     'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8016(DSPLAY,CHANS,NOCHAN,LUTF,ITTF,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW016(DSPLAY,CHANS,NOCHAN,LUTF,ITTF,IDST)
      ELSE
         IDST = 0
      ENDIF
C      
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDIAG(DSPLAY,LUNO,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,LUNO,IDST
C            
      INCLUDE     'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8050(DSPLAY,LUNO,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW050(DSPLAY,LUNO,IDST)
      ELSE
         IDST = 0
      ENDIF
C      
      RETURN
      END

      SUBROUTINE
     +  IIDQDV(DSPLAY,TOTCNF,SZX,SZY,DEPTH,NLUT,NITT,NCURS,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER   DSPLAY,TOTCNF,SZX,SZY,DEPTH
      INTEGER   NLUT,NITT,NCURS,IDST
C            
      INCLUDE   'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8006
     +   (DSPLAY,TOTCNF,SZX,SZY,DEPTH,NLUT,NITT,NCURS,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW006
     +   (DSPLAY,TOTCNF,SZX,SZY,DEPTH,NLUT,NITT,NCURS,IDST)
      ELSE
         IDST = 0
      ENDIF
C      
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDQDC(DSPLAY,CONFNO,MEMTYP,TOTMEM,
     +                  CONFMD,MEMID,MEMSZX,MEMSZY,MEMDEP,
     +                  ITTDEP,NOMEM,IDST)
C 
      IMPLICIT NONE
C 
      INTEGER    DSPLAY,CONFNO,MEMTYP,TOTMEM
      INTEGER    CONFMD,MEMID(1),MEMSZX(1),MEMSZY(1),MEMDEP(1)
      INTEGER    ITTDEP(1),NOMEM,IDST
C            
      INCLUDE   'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
          CALL GD8009(DSPLAY,CONFNO,MEMTYP,TOTMEM,CONFMD,
     +               MEMID,MEMSZX,MEMSZY,MEMDEP,ITTDEP,NOMEM,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW009(DSPLAY,CONFNO,MEMTYP,TOTMEM,CONFMD,
     +               MEMID,MEMSZX,MEMSZY,MEMDEP,ITTDEP,NOMEM,IDST)
      ELSE
         IDST = 0
      ENDIF
C      
      RETURN
      END

      SUBROUTINE IIDQCI(DSPLAY,CAPA,OUTSIZ,OUTDAT,TOTAL,IDST)
C
      IMPLICIT NONE
C 
      INTEGER    DSPLAY,CAPA,OUTSIZ,OUTDAT(1),TOTAL,IDST
C            
      INCLUDE   'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8007(DSPLAY,CAPA,OUTSIZ,OUTDAT,TOTAL,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW007(DSPLAY,CAPA,OUTSIZ,OUTDAT,TOTAL,IDST)
      ELSE
         IDST = 0
      ENDIF
C      
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDSSS(DSPLAY,MEMIDS,XOFFS,YOFFS,SPLITF,
     +                  SPLX,SPLY,IDST)
C 
      IMPLICIT NONE
C	
      INTEGER   DSPLAY,MEMIDS(1),SPLITF,XOFFS(1),YOFFS(1)
      INTEGER   SPLX,SPLY,IDST
C            
      INCLUDE   'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8048(DSPLAY,MEMIDS,XOFFS,YOFFS,SPLITF,
     +               SPLX,SPLY,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW048(DSPLAY,MEMIDS,XOFFS,YOFFS,SPLITF,
     +               SPLX,SPLY,IDST)
      ELSE
         IDST = 0
      ENDIF
C      
      RETURN
      END

      SUBROUTINE IIDERR(ERNO,ERSTR,LSTR)
C
      IMPLICIT NONE
C	
      INTEGER   ERNO,LSTR
C 
      CHARACTER*(*)  ERSTR
C            
      INCLUDE   'MID_INCLUDE:IDIDEV.INC/NOLIST'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8005(ERNO,ERSTR,LSTR)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW005(ERNO,ERSTR,LSTR)
      ENDIF
C      
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDSNP(DSPLAY,COLMOD,NPIX,XOFF,YOFF,
     +                  DEPTH,PACK,DATA,IDST)
C 
      IMPLICIT NONE
C	
      INTEGER   DSPLAY,COLMOD,NPIX,XOFF,YOFF,DEPTH,PACK,IDST
      INTEGER   DATA(1)
C 
C  currently not implemented for the DeAnza ...!
C 
      RETURN
      END
