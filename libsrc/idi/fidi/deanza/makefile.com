$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.LIBSRC.IDI.FIDI.DEANZA]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:58 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   gd8aux.for
$ FORTRAN   gd8d2.for
$ FORTRAN   gd8i2.for
$ FORTRAN   gd8m2.for
$ FORTRAN   gd8c.for
$ FORTRAN   gd8g.for
$ FORTRAN   gd8l.for
$ FORTRAN   gd8r.for
$ FORTRAN   gd8d1.for
$ FORTRAN   gd8i1.for
$ FORTRAN   gd8m1.for
$ FORTRAN   gd8z.for
$ LIB/REPLACE libsubmid gd8aux.obj,gd8d2.obj,gd8i2.obj,gd8m2.obj,gd8c.obj,gd8g.obj,gd8l.obj,gd8r.obj,gd8d1.obj,gd8i1.obj,gd8m1.obj,gd8z.obj,dazidis.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
