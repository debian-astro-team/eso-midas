$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.LIBSRC.IDI.FIDI.NODEANZA]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:58 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   gd8aux.for
$ FORTRAN   gd8c.for
$ FORTRAN   gd8g.for
$ FORTRAN   gd8l.for
$ FORTRAN   gd8r.for
$ FORTRAN   gd8d.for
$ FORTRAN   gd8i.for
$ FORTRAN   gd8m.for
$ FORTRAN   gd8z.for
$ LIB/REPLACE libsubmid gd8aux.obj,gd8c.obj,gd8g.obj,gd8l.obj,gd8r.obj,gd8d.obj,gd8i.obj,gd8m.obj,gd8z.obj,dazidis.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
