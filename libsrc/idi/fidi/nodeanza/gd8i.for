C @(#)gd8i.for	19.1 (ES0-DMD) 02/25/03 13:55:25
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE 
     +  GD8037(DSPLAY,INTTYPE,INTID,OBJTYPE,OBJID,INTOP,EXTRIG,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,INTTYPE,INTID,OBJTYPE,OBJID
      INTEGER      INTOP,EXTRIG,IDST
C 
      IDST = 0
      RETURN
      END

      SUBROUTINE GD8038(DSPLAY,TRIGSTAT,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,TRIGSTAT(1),IDST
C 
      IDST = 0
      RETURN
      END

      SUBROUTINE GD8039(DSPLAY,IDST)
C
      IMPLICIT NONE
C 
      INTEGER      DSPLAY,IDST
C 
      IDST = 0
      RETURN
      END

      SUBROUTINE GD8040(DSPLAY,INTTYPE,INTID,DEVDSC,LL,IDST)
C      
      IMPLICIT NONE
C 
      INTEGER      DSPLAY,INTTYPE,INTID,LL,IDST
C 
      CHARACTER*(*)  DEVDSC
C 
      IDST = 0
      RETURN
      END

      SUBROUTINE GD8041(DSPLAY,LOCNO,JX,JY,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,LOCNO,JX,JY,IDST
C 
      IDST = 0
      RETURN
      END

      SUBROUTINE GD8044(DSPLAY,EVAL,JJ,IDST)
C 
      IMPLICIT NONE
C 
      INTEGER      DSPLAY,EVAL,JJ,IDST
C 
      IDST = 0
      RETURN
      END

	SUBROUTINE GD8045(DISPLAY,EVAL,OUTSTR,JJ,IDST)
C 
	IMPLICIT NONE
C 
	INTEGER*4	DISPLAY,EVAL,JJ,IDST
C 
	CHARACTER*(*)   OUTSTR
C 
	IDST = 0
	RETURN
	END
