/* @(#)dazidis.c	19.1 (ES0-DMD) 02/25/03 13:55:24 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

IICINC_C(dsplay,chan,cursno,shape,colour,xc,yc,idst)
 
long int   *dsplay,*chan,*cursno,*shape,*colour,*xc,*yc,*idst;
 
{
*idst = 0;
return;
}
 

IICSCV_C(dsplay,cursno,vis,idst)

long int   *dsplay,*cursno,*vis,*idst;

{
*idst = 0;
return;
}



IICWCP_C(dsplay,chan,cursno,xc,yc,idst)

long int   *dsplay,*chan,*cursno,*xc,*yc,*idst;

{
*idst = 0;
return;
}

 

IICRCP_C(dsplay,chan,cursno,xc,yc,memo,idst)

long int   *dsplay,*chan,*cursno,*xc,*yc,*memo,*idst;

{
*idst = 0;
return;
}



IIDOPN_C(device,dsplay,idst)

long int *device, *dsplay,*idst;	/* device is really a string... */

{
*idst = 0;
return;
}



IIDCLO_C(dsplay,idst)

long int *dsplay,*idst;
{
*idst = 0;
return;
}



IIDQDV_C(dsplay,totcnf,szx,szy,depth,nlut,nitt,ncurs,idst)
	
long int   *dsplay,*totcnf,*szx,*szy,*depth;
long int   *nlut,*nitt,*ncurs,*idst;

{
*idst = 0;
return;
}



IIGPLY_C(dsplay,chan,xfig,yfig,ndim,intens,style,idst)

long int    *dsplay,*chan,*xfig,*yfig,*ndim,*idst;
long int    *intens,*style;

{
*idst = 0;
return;
}



IIIENI_C(dsplay,inttyp,intid,objtyp,objid,intop,extrig,idst)
	
long int   *dsplay,*inttyp,*intid,*objtyp,*objid;
long int   *intop,*extrig,*idst;

{
*idst = 0;
return;
}

 
 
IIIEIW_C(dsplay,trigst,idst)
	
long int   *dsplay,*trigst,*idst;

{
*idst = 0;
return;
}



IIISTI_C(dsplay,idst)
	
long int   *dsplay,*idst;

{
*idst = 0;
return;
}



IIMCMY_C(dsplay,chan,nochan,value,idst)

long int *dsplay,*chan,*nochan,*value,*idst;

{
*idst = 0;
return;
}
 


IIMSTW_C(dsplay,chan,loadfl,szx,szy,depth,xbeg,ybeg,idst)

long int *dsplay,*chan,*szx,*szy,*loadfl,*depth,*idst,*xbeg,*ybeg;

{
*idst = 0;
return;
}
 

 
IIMSMV_C(dsplay,chan,nochan,vis,idst)

long int *dsplay,*chan,*nochan,*vis,*idst;

{
*idst = 0;
return;
}
 
