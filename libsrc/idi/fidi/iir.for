C @(#)iir.for	19.1 (ES0-DMD) 02/25/03 13:55:01
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE IIRINR(DSPLAY,CHAN,COLOUR,XA,YA,XB,YB,ROID,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,COLOUR,XA,YA,XB,YB,ROID,IDST
C      
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8033(DSPLAY,CHAN,COLOUR,XA,YA,XB,YB,ROID,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW033(DSPLAY,CHAN,COLOUR,XA,YA,XB,YB,ROID,IDST)
      ELSE      
         IDST = 0
      ENDIF
C      
      RETURN
      END
C 
C      
C--------------------------------------------------
C
C
      SUBROUTINE IICINR(DSPLAY,CHAN,COLOUR,XA,YA,XB,YB,ZB,ROID,IDST)
C
      IMPLICIT NONE
C
      INTEGER      DSPLAY,CHAN,COLOUR,XA,YA,XB,YB,ZB,ROID,IDST
C
      IDST = -1               !no support for DeAnza
C
      RETURN
      END
C
C
C--------------------------------------------------
C 
C
      SUBROUTINE IIRRRI(DSPLAY,CHAN,ROID,XA,YA,XB,YB,MEMO,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,ROID,XA,YA,XB,YB,MEMO,IDST
C 
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8034(DSPLAY,CHAN,ROID,XA,YA,XB,YB,MEMO,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW034(DSPLAY,CHAN,ROID,XA,YA,XB,YB,MEMO,IDST)
      ELSE      
         IDST = 0
      ENDIF
C      
      RETURN
      END
C
C
C--------------------------------------------------
C
C
      SUBROUTINE IICRRI(DSPLAY,CHAN,ROID,XA,YA,XB,YB,ZB,MEMO,IDST)
C
      IMPLICIT NONE
C
      INTEGER      DSPLAY,CHAN,ROID,XA,YA,XB,YB,ZB,MEMO,IDST
C
      IDST = -1               !no support for DeAnza
C
      RETURN
      END
C
C
C--------------------------------------------------
C
C
      SUBROUTINE IIRWRI(DSPLAY,CHAN,ROID,XA,YA,XB,YB,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,ROID,XA,YA,XB,YB,IDST
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8035(DSPLAY,CHAN,ROID,XA,YA,XB,YB,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW035(DSPLAY,CHAN,ROID,XA,YA,XB,YB,IDST)
      ELSE      
         IDST = 0
      ENDIF
C      
      RETURN
      END
C
C
C--------------------------------------------------
C
C
      SUBROUTINE IICWRI(DSPLAY,CHAN,ROID,XA,YA,XB,YB,ZB,IDST)
C
      IMPLICIT NONE
C
      INTEGER      DSPLAY,CHAN,ROID,XA,YA,XB,YB,ZB,IDST
C 
      IDST = -1               !no support for DeAnza
C
      RETURN
      END
C 
C      
C--------------------------------------------------
C 
C
      SUBROUTINE IIRSRV(DSPLAY,ROID,VIS,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,ROID,VIS,IDST
C
      INCLUDE      'MID_INCLUDE:IDIDEV.INC'
C      
      IF (IDINUM.EQ.0) THEN
         CALL GD8036(DSPLAY,ROID,VIS,IDST)
      ELSE IF (IDINUM.EQ.10) THEN
         CALL MXW036(DSPLAY,ROID,VIS,IDST)
      ELSE      
         IDST = 0
      ENDIF
C      
      RETURN
      END
