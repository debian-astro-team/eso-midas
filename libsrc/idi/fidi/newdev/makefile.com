$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.LIBSRC.IDI.FIDI.NEWDEV]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:58 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   mxwc.for
$ FORTRAN   mxwg.for
$ FORTRAN   mxwl.for
$ FORTRAN   mxwr.for
$ FORTRAN   mxwd.for
$ FORTRAN   mxwi.for
$ FORTRAN   mxwm.for
$ FORTRAN   mxwz.for
$ LIB/REPLACE libsubmid mxwc.obj,mxwg.obj,mxwl.obj,mxwr.obj,mxwd.obj,mxwi.obj,mxwm.obj,mxwz.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
