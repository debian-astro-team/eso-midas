C @(#)mxwl.for	19.1 (ES0-DMD) 02/25/03 13:55:18
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE MXW025(DSPLAY,NLUT,ISTA,COUNT,RLUT,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,NLUT,ISTA,COUNT,IDST
C      
      REAL            RLUT(3,COUNT)
C 
      IDST = 0
      RETURN
      END      

      SUBROUTINE MXW026(DSPLAY,NLUT,ISTA,COUNT,RLUT,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,NLUT,ISTA,COUNT,IDST
C      
      REAL            RLUT(3,COUNT)
C 
      IDST = 0
      RETURN
      END      

      SUBROUTINE MXW023(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,NITT,ISTA,COUNT,IDST
C      
      REAL            RITT(1)
C 
      IDST = 0
      RETURN
      END      

      SUBROUTINE MXW024(DSPLAY,CHAN,NITT,ISTA,COUNT,RITT,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,NITT,ISTA,COUNT,IDST
C      
      REAL            RITT(1)
C 
      IDST = 0
      RETURN
      END      

      SUBROUTINE MXW049(DSPLAY,CHAN,VISI,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,VISI,IDST
C 
      IDST = 0
      RETURN
      END      
