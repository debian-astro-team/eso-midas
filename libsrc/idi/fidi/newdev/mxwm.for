C @(#)mxwm.for	19.1 (ES0-DMD) 02/25/03 13:55:18
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE MXW018(DSPLAY,CHAN,NOCHAN,VALUE,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN(1),NOCHAN,VALUE,IDST
C      
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW020(DSPLAY,CHAN,LOADFL,SZX,SZY,DEPTH,
     +                      XBEG,YBEG,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,SZX,SZY,LOADFL,DEPTH,IDST,XBEG,YBEG
C      
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW017(DSPLAY,CHAN,PIX,SIZE,DEPTH,PACK,XBEG,YBEG,IDST)
C
      IMPLICIT NONE
C      
      INTEGER       DSPLAY,CHAN,SIZE,DEPTH,PACK
      INTEGER      XBEG,YBEG,PIX(1),IDST
C      
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW019(DSPLAY,CHAN,SIZE,XBEG,YBEG,DEPTH,
     +                    PACK,ITTFLG,PIX,IDST)
C
      IMPLICIT NONE
C      
      INTEGER       DSPLAY,CHAN,SIZE,DEPTH,PACK,ITTFLG
      INTEGER      XBEG,YBEG,IDST,PIX(1)
C      
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW011(DSPLAY,CHAN,NOCHAN,VIS,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN(1),NOCHAN,VIS,IDST
C      
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW015(DSPLAY,CHAN,LUT,ITT,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CHAN,ITT,LUT,IDST
C      
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW047(DSPLAY,MEMIDS,MEMLIM,DELAY,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,MEMIDS(1),MEMLIM,IDST
C
      REAL            DELAY
C      
      IDST = 0
      RETURN
      END
