C @(#)mxwd.for	19.1 (ES0-DMD) 02/25/03 13:55:18
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE MXW001(DEVICE,DSPLAY,IDST)
C
      IMPLICIT NONE
C      
      INTEGER*4       CHANL,DSPLAY,IDST
C      
      CHARACTER*(*)      DEVICE
C       
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW002(DSPLAY,IDST)
C
      IMPLICIT NONE
C      
      INTEGER*4       DSPLAY,IDST
C       
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW003(DSPLAY,IDST)
C 
      INTEGER*4  DSPLAY,IDST
C 
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW004(DSPLAY,IDST)
C 
      INTEGER*4  DSPLAY,IDST
C 
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW010(DSPLAY,CONFNO,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER*4      DSPLAY,CONFNO,IDST
C       
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW016(DSPLAY,CHANS,NOCHAN,LUTF,ITTF,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER*4      DSPLAY,CHANS(1),NOCHAN,LUTF(1),ITTF(1),IDST
C       
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW050(DSPLAY,LUNO,IDST)
C
      IMPLICIT NONE
C      
      INTEGER*4       DSPLAY,LUNO,IDST
C       
      IDST = 0
      RETURN
      END

      SUBROUTINE
     +      MXW006(DSPLAY,TOTCONF,SZX,SZY,DEPTH,NLUT,NITT,NCURS,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER*4      DSPLAY,TOTCONF,SZX,SZY,DEPTH,
     +                  NLUT,NITT,NCURS,IDST
C       
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW009(DSPLAY,CONFNO,MEMTYP,TOTMEM,
     +                    CONFMODE,MEMID,MEMSZX,MEMSZY,MEMDEP,
     +                        ITTDEP,NOMEM,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER*4      DSPLAY,CONFNO,MEMTYP,TOTMEM
      INTEGER*4      CONFMODE,MEMID(1),MEMSZX(1),MEMSZY(1),MEMDEP(1)
      INTEGER*4      ITTDEP(1),NOMEM,IDST
C       
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW007(DSPLAY,CAPA,OUTSIZ,OUTDAT,TOTAL,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER*4      DSPLAY,CAPA,OUTSIZ,OUTDAT(1),TOTAL,IDST
C       
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW048(DSPLAY,MEMIDS,XOFFS,YOFFS,SPLFLG,
     +                    SPLX,SPLY,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER*4      DSPLAY,MEMIDS(1),SPLFLG,XOFFS(1),YOFFS(1)
      INTEGER*4      SPLX,SPLY,IDST
C       
      IDST = 0
      RETURN
      END

      SUBROUTINE MXW005(ERNO,ERSTR,LSTR)
C
      IMPLICIT NONE
C      
      INTEGER*4      ERNO,LSTR
C 
      CHARACTER*(*)      ERSTR
C       
      RETURN
      END
