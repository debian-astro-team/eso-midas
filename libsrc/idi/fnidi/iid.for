C @(#)iid.for	19.1 (ES0-DMD) 02/25/03 13:55:32
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE IIDOPN(DEVICE,DSPLAY,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,IDST
C      
      CHARACTER*(*)   DEVICE
C      
      IDST = 0
C      
      RETURN
      END

      SUBROUTINE IIDCLO(DSPLAY,IDST)
C
      IMPLICIT NONE
C      
      INTEGER     DSPLAY,IDST
C      
      IDST = 0
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDRST(DSPLAY,IDST)
C 
      INTEGER  DSPLAY,IDST
C 
C 
      RETURN
      END

      SUBROUTINE IIDUPD(DSPLAY,IDST)
C 
      INTEGER    DSPLAY,IDST
C 
      IDST = 0
C 
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDSEL(DSPLAY,CONFNO,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,CONFNO,IDST
C            
      IDST = 0
C      
      RETURN
      END

      SUBROUTINE IIDSDP(DSPLAY,CHANS,NOCHAN,LUTF,ITTF,IDST)
C      
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,IDST
      INTEGER      CHANS(1),NOCHAN,LUTF(1),ITTF(1)
C      
      IDST = 0
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDIAG(DSPLAY,LUNO,IDST)
C
      IMPLICIT NONE
C      
      INTEGER      DSPLAY,LUNO,IDST
C            
      IDST = 0
C      
      RETURN
      END

      SUBROUTINE
     +  IIDQDV(DSPLAY,TOTCNF,SZX,SZY,DEPTH,NLUT,NITT,NCURS,IDST)
C 
      IMPLICIT NONE
C      
      INTEGER   DSPLAY,TOTCNF,SZX,SZY,DEPTH
      INTEGER   NLUT,NITT,NCURS,IDST
C            
      IDST = 0
C      
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDQDC(DSPLAY,CONFNO,MEMTYP,TOTMEM,
     +                  CONFMD,MEMID,MEMSZX,MEMSZY,MEMDEP,
     +                  ITTDEP,NOMEM,IDST)
C 
      IMPLICIT NONE
C 
      INTEGER    DSPLAY,CONFNO,MEMTYP,TOTMEM
      INTEGER    CONFMD,MEMID(1),MEMSZX(1),MEMSZY(1),MEMDEP(1)
      INTEGER    ITTDEP(1),NOMEM,IDST
C            
      IDST = 0
C      
      RETURN
      END

      SUBROUTINE IIDQCI(DSPLAY,CAPA,OUTSIZ,OUTDAT,TOTAL,IDST)
C
      IMPLICIT NONE
C 
      INTEGER    DSPLAY,CAPA,OUTSIZ,OUTDAT(1),TOTAL,IDST
C            
      IDST = 0
C      
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDSSS(DSPLAY,MEMIDS,XOFFS,YOFFS,SPLITF,
     +                  SPLX,SPLY,IDST)
C 
      IMPLICIT NONE
C	
      INTEGER   DSPLAY,MEMIDS(1),SPLITF,XOFFS(1),YOFFS(1)
      INTEGER   SPLX,SPLY,IDST
C            
      IDST = 0
C      
      RETURN
      END

      SUBROUTINE IIDERR(ERNO,ERSTR,LSTR)
C
      IMPLICIT NONE
C	
      INTEGER   ERNO,LSTR
C 
      CHARACTER*(*)  ERSTR
C            
      RETURN
      END
C      
C
C --------------------------------------------------
C
C 
      SUBROUTINE IIDSNP(DSPLAY,COLMOD,NPIX,XOFF,YOFF,
     +                  DEPTH,PACK,DATA,IDST)
C 
      IMPLICIT NONE
C	
      INTEGER   DSPLAY,COLMOD,NPIX,XOFF,YOFF,DEPTH,PACK,IDST
      INTEGER   DATA(1)
C 
      IDST = 0
C 
      RETURN
      END
