$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.LIBSRC.IDI.FNIDI]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:58 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   iic.for
$ FORTRAN   iid.for
$ FORTRAN   iig.for
$ FORTRAN   iii.for
$ FORTRAN   iil.for
$ FORTRAN   iim.for
$ FORTRAN   iir.for
$ FORTRAN   iiz.for
$ LIB/REPLACE libsubmid iic.obj,iid.obj,iig.obj,iii.obj,iil.obj,iim.obj,iir.obj,iiz.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
