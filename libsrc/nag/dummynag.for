C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
       SUBROUTINE E04FDF(M,N,X,FSUMSQ,IW,LIW,W,LW,IFAIL)
       INTEGER  M,N,LIW,IW(LIW),LW,IFAIL,ISTAT
       DOUBLE PRECISION X(N),FSUMSQ,W(LW)

C      dummy routine
       CALL STTPUT('Sorry but NAG is not implemented...',ISTAT)
       CALL STTPUT('Try to use the NR method.',ISTAT)
       CALL STSEPI
       END


       SUBROUTINE E04FCF(M,N,LSQFUN,LSQMON,IPRINT,
     1           MAXCAL,ETA,XTOL,STEPMX,X,FSUMSQ,FVEC,
     2           FJAC,LJ,S,V,LV,NITER,NF,IW,LIW,W,
     3           LW,IFAIL)
       INTEGER M,N,IPRINT,MAXCAL,LJ,LV,NITER,NF,
     1        LIW,IW(LIW),LW,IFAIL,ISTAT
       DOUBLE PRECISION  ETA,XTOL,STEPMX,X(N),FSUMSQ,FVEC(M),
     2        FJAC(LJ,N),S(N),V(1,1),W(LW)
       EXTERNAL  LSQFUN,LSQMON
       CALL STTPUT('Sorry but NAG is not implemented...',ISTAT)
       CALL STTPUT('Try to use the NR method.',ISTAT)
       CALL STSEPI
C      dummy routine
       END


       SUBROUTINE E04GCF(M,N,X,FSUMSQ,IW,LIW,W,LW,IFAIL)
       INTEGER  M,N,LIW,IW(LIW),LW,IFAIL,ISTAT
       DOUBLE PRECISION X(N),FSUMSQ,W(LW)

       CALL STTPUT('Sorry but NAG is not implemented...',ISTAT)
       CALL STTPUT('Try to use the NR method.',ISTAT)
C      dummy routine
       CALL STSEPI
       END     

       SUBROUTINE E04GEF(M,N,X,FSUMSQ,IW,LIW,W,LW,IFAIL)
       INTEGER  M,N,LIW,IW(LIW),LW,IFAIL,ISTAT
       DOUBLE PRECISION X(N),FSUMSQ,W(LW)

C      dummy routine
       CALL STTPUT('Sorry but NAG is not implemented...',ISTAT)
       CALL STTPUT('Try to use the NR method.',ISTAT)
       CALL STSEPI
       END     


       SUBROUTINE E04GBF(M,N,LSQLIN,LSQFUN,LSQMON,IPRINT,
     1           MAXCAL,ETA,XTOL,STEPMX,X,FSUMSQ,FVEC,
     2           FJAC,LJ,S,V,LV,NITER,NF,IW,LIW,W,
     3           LW,IFAIL)
       INTEGER M,N,IPRINT,MAXCAL,LJ,LV,NITER,NF,
     1        LIW,IW(LIW),LW,IFAIL,ISTAT
       DOUBLE PRECISION  ETA,XTOL,STEPMX,X(N),FSUMSQ,FVEC(M),
     2        FJAC(LJ,N),S(N),V(1,1),W(LW)
       EXTERNAL  LSQFUN,LSQMON,LSQLIN

       CALL STTPUT('Sorry but NAG is not implemented...',ISTAT)
       CALL STTPUT('Try to use the NR method.',ISTAT)
C      dummy routine
       CALL STSEPI
       END

       SUBROUTINE E04GDF(M,N,LSQFUN,LSQMON,IPRINT,
     1           MAXCAL,ETA,XTOL,STEPMX,X,FSUMSQ,FVEC,
     2           FJAC,LJ,S,V,LV,NITER,NF,IW,LIW,W,
     3           LW,IFAIL)
       INTEGER M,N,IPRINT,MAXCAL,LJ,LV,NITER,NF,
     1        LIW,IW(LIW),LW,IFAIL,ISTAT
       DOUBLE PRECISION  ETA,XTOL,STEPMX,X(N),FSUMSQ,FVEC(M),
     2        FJAC(LJ,N),S(N),V(1,1),W(LW)
       EXTERNAL  LSQFUN,LSQMON

       CALL STTPUT('Sorry but NAG is not implemented...',ISTAT)
       CALL STTPUT('Try to use the NR method.',ISTAT)
C      dummy routine
       CALL STSEPI
       END



       SUBROUTINE E04YCF(JOB,M,N,FSUMSQ,S,V,LV,CJ,WORK,IFAIL)
       INTEGER JOB,M,N,IFAIL,LV
       DOUBLE PRECISION FSUMSQ,S(N),V(LV,N),CJ(N),WORK(N)

C      dummy routine
       RETURN
       END



       SUBROUTINE E04JAF(N,IBOUND,BL,BU,X,F,IW,LIW,W,LW,IFAIL)
       INTEGER N,IBOUND,IFAIL,LW,LIW,IW
       DOUBLE PRECISION BL(N),BU(N),X(N),F,W(N)

C      dummy routine
       RETURN
       END


       SUBROUTINE E04HBF(N,FUNCT,X,NF,DELTA,HESL,LH,HESD,F,G,IW,LIW,
     1                  W,LW,IFAIL)
       INTEGER N,NF,LH,LIW,IW(LIW),LW,IFAIL
       DOUBLE PRECISION X(N),DELTA(N),HESL(LH),HESD(N),F,G(N),W(LW)
       EXTERNAL FUNCT
 
C      dummy routine
       RETURN
       END





       SUBROUTINE E04JBF(N,FUNCT,MONIT,IPRINT,LOCSCH,INTYPE,
     1           MINLIN,MAXCAL,ETA,XTOL,STEPMX,FEST,DELTA,
     2           IBOUND,BL,BU,X,HESL,LH,HESD,ISTATE,F,G,
     3           IW,LIW,W,LW,IFAIL)
      
       INTEGER N,IPRINT,MAXCAL,IBOUND,LH,ISTATE(N),
     1        LIW,IW(LIW),LW,IFAIL,INTYPE
       DOUBLE PRECISION ETA,XTOL,STEPMX,FEST,DELTA(N),BL(N),BU(N),
     2         X(N),HESL(LH),HESD(N),F,G(N),W(LW)
       LOGICAL LOCSCH
       EXTERNAL FUNCT,MONIT,MINLIN
C      dummy routine
       RETURN
       END



       SUBROUTINE E04KAF(N,IBOUND,BL,BU,X,F,G,IW,LIW,W,LW,IFAIL)
       INTEGER N,IBOUND,LIW,IW(LIW),LW,IFAIL
       DOUBLE PRECISION BL(N),BU(N),X(N),F,G(N),W(LW)
C      dummy routine
       RETURN
       END
       


       SUBROUTINE E04HCF(N,FUNCT,X,F,G,IW,LIW,W,LW,IFAIL)
       INTEGER N,LIW,LW,IFAIL,IW(LIW)
       DOUBLE PRECISION X(N),G(N),W(LW),F
       EXTERNAL FUNCT

C      dummy routine
       RETURN
       END

       SUBROUTINE E04KBF(N,FUNCT,MONIT,IPRINT,LOCSCH,INTYPE,
     1           MINLIN,MAXCAL,ETA,XTOL,STEPMX,FEST,
     2           IBOUND,BL,BU,X,HESL,LH,HESD,ISTATE,F,G,
     3           IW,LIW,W,LW,IFAIL)
      
       INTEGER N,IPRINT,MAXCAL,IBOUND,LH,ISTATE(N),
     1        LIW,IW(LIW),LW,IFAIL,INTYPE
       DOUBLE PRECISION ETA,XTOL,STEPMX,FEST,BL(N),BU(N),
     2         X(N),HESL(LH),HESD(N),F,G(N),W(LW)
       LOGICAL LOCSCH
       EXTERNAL FUNCT,MONIT,MINLIN

C     dummy routine
       RETURN
       END



       SUBROUTINE E04KCF(N,IBOUND,BL,BU,X,F,G,IW,LIW,W,LW,IFAIL)
       INTEGER N,IBOUND,LIW,IW(LIW),LW,IFAIL
       DOUBLE PRECISION BL(N),BU(N),X(N),F,G(N),W(LW)
C      dummy routine
       RETURN
       END





       SUBROUTINE E04KDF(N,FUNCT,MONIT,IPRINT,
     1           MAXCAL,ETA,XTOL,DELTA,STEPMX,
     2           IBOUND,BL,BU,X,HESL,LH,HESD,ISTATE,F,G,
     3           IW,LIW,W,LW,IFAIL)
      
       INTEGER N,IPRINT,MAXCAL,IBOUND,LH,ISTATE(N),
     1        LIW,IW(LIW),LW,IFAIL
       DOUBLE PRECISION ETA,XTOL,STEPMX,DELTA,BL(N),BU(N),
     2         X(N),HESL(LH),HESD(N),F,G(N),W(LW)
       EXTERNAL FUNCT,MONIT

C     dummy routine
       RETURN
       END


       DOUBLE PRECISION FUNCTION F01DEF(A,B,N)
       INTEGER N
       DOUBLE PRECISION A(N),B(N)
C     dummy routine
       F01DEF = 0
       RETURN
       END

       DOUBLE PRECISION FUNCTION F06EAF(N,X,
     1                            NINX,Y,NINY)
       INTEGER N,NINX,NINY
       DOUBLE PRECISION X(N),Y(N)
       F06EAF = 0
       RETURN
       END


       SUBROUTINE E01AAF(A,B,C,N1,N2,N,X)
       INTEGER N1,N2,N,ISTAT
       DOUBLE PRECISION A(N1),B(N1),C(N2),X
       CALL STTPUT('Sorry, NAG is not implemented',ISTAT)
       CALL STSEPI 
       RETURN
       END

       SUBROUTINE E01BAF(M,X,Y,K,C,LCK,WRK,LWRK,IFAIL)
       INTEGER           IFAIL, LCK, LWRK, M, ISTAT
       DOUBLE PRECISION  C(LCK), K(LCK), WRK(LWRK), X(M), Y(M)
       CALL STTPUT('Sorry, NAG is not implemented',ISTAT)
       CALL STSEPI 
       RETURN
       END

       SUBROUTINE E02BCF(NCAP7,K,C,X,LEFT,S,IFAIL)
       DOUBLE PRECISION  X
       INTEGER           IFAIL, LEFT, NCAP7, ISTAT
       DOUBLE PRECISION  C(NCAP7), K(NCAP7), S(4)
       CALL STTPUT('Sorry, NAG is not implemented',ISTAT)
       CALL STSEPI 
       RETURN
       END

       DOUBLE PRECISION FUNCTION E04HEV(N,X)
       INTEGER N
       DOUBLE PRECISION X(N)
C     dummy routine
       E04HEV = 0
       RETURN
       END


       DOUBLE PRECISION FUNCTION E04JBQ(N,X)
       INTEGER N
       DOUBLE PRECISION X(N)
C     dummy routine
       E04JBQ = 0 
       RETURN
       END


       DOUBLE PRECISION FUNCTION E04LBS(N,X)
       INTEGER N
       DOUBLE PRECISION X(N)
C     dummy routine
       E04LBS = 0
       RETURN
       END
