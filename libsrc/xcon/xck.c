/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++ XC interface module XCK +++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module XCK
.AUTHOR         K. Banse  ESO - Garching
.KEYWORDS       standard interfaces, keyword data base
.ENVIRONMENT    VMS and UNIX
.COMMENTS    
holds XCKRDx (x=I,R,D,C) routines
.VERSION  [1.00] 940426:  creation
.VERSION  [1.10] 940505:  add XCKWRx interfaces

 090407		last modif
-----------------------------------------------------------------------------*/

#include <midas_def.h>
#include <xconndef.h>

#include <stdio.h>


/*

*/

int XCKRDI(cid,key,felem,maxvals,actvals,values,unit,null,kstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
use background Midas to read data from integer keyword
.ALGORITHM
straight forward
.RETURNS
return status ( 0 = ok )
  =   1; background Midas busy
  =  -1; problems with socket communication (kstat = error from communication)
  = -90; invalid input to this interface
  = -91; only non-socket communication established
--------------------------------------------------*/

int  cid;	/* IN: id of background Midas */
char *key;	/* IN: name of keyword  */
int  felem;	/* IN: first data item to be read */
int  maxvals;	/* IN: no. of elements to get  */
int  *actvals;	/* OUT: actual no. of elements returned */
int  *values;	/* OUT: buffer for data values */
int  *unit;  	/* OUT: unit-pointer */
int  *null;	/* OUT: no. of null values in keyword */
int  *kstat;	/* OUT: status from SCKRDI */

{
int  stat, retstat, karr[4];
char  Comndline[40];


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (-90);
else if (BKMIDAS[cid].METHOD != 's')
   return (-91);
else if (BKMIDAS[cid].ACTIVE != 'N')
   return (1);

Comndline[0] = XCONNECT.MYUNIT[0];      /* include return address */
Comndline[1] = XCONNECT.MYUNIT[1];
(void)sprintf(&Comndline[2],
        "READ/KEYW %s/i/%d/%d XCONNECT",key,felem,maxvals);

stat = ClientKRead(BKMIDAS[cid].CHAN,1,Comndline,(char *)values,
                   karr,&retstat);

if (stat == 0)
   {
   *actvals = karr[0];
   *unit = karr[1];
   *null = karr[2];
   *kstat = karr[3];
   return (0);
   }
else
   {
   *kstat = retstat;
   return (-1);
   }
}	
/*

*/

int XCKRDR(cid,key,felem,maxvals,actvals,values,unit,null,kstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
use background Midas to read data from integer keyword
.ALGORITHM
straight forward
.RETURNS
return status ( 0 = ok )
  =   1; background Midas busy
  =  -1; problems with socket communication (kstat = error from communication)
  = -90; invalid input to this interface
  = -91; only non-socket communication established
--------------------------------------------------*/

int  cid;	/* IN: id of background Midas */
char *key;	/* IN: name of keyword  */
int  felem;	/* IN: first data item to be read */
int  maxvals;	/* IN: no. of elements to get  */
int  *actvals;	/* OUT: actual no. of elements returned */
float  *values;	/* OUT: buffer for data values */
int  *unit;  	/* OUT: unit-pointer */
int  *null;	/* OUT: no. of null values in keyword */
int  *kstat;	/* OUT: status from SCKRDR */

{
int  stat, retstat, karr[4];
char  Comndline[40];


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (90);
else if (BKMIDAS[cid].METHOD != 's')
   return (-91);
else if (BKMIDAS[cid].ACTIVE != 'N')
   return (1);


Comndline[0] = XCONNECT.MYUNIT[0];      /* include return address */
Comndline[1] = XCONNECT.MYUNIT[1];
(void)sprintf(&Comndline[2],
        "READ/KEYW %s/r/%d/%d XCONNECT",key,felem,maxvals);

stat = ClientKRead(BKMIDAS[cid].CHAN,2,Comndline,(char *)values,
                   karr,&retstat);
if (stat == 0)
   {
   *actvals = karr[0];
   *unit = karr[1];
   *null = karr[2];
   *kstat = karr[3];
   return (0);
   }
else
   {
   *kstat = retstat;
   return (-1);
   }
}	
/*

*/

int XCKRDD(cid,key,felem,maxvals,actvals,values,unit,null,kstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
use background Midas to read data from integer keyword
.ALGORITHM
straight forward
.RETURNS
return status ( 0 = ok )
  =   1; background Midas busy
  =  -1; problems with socket communication (kstat = error from communication)
  = -90; invalid input to this interface
  = -91; only non-socket communication established
--------------------------------------------------*/

int  cid;	/* IN: id of background Midas */
char *key;	/* IN: name of keyword  */
int  felem;	/* IN: first data item to be read */
int  maxvals;	/* IN: no. of elements to get  */
int  *actvals;	/* OUT: actual no. of elements returned */
double  *values;	/* OUT: buffer for data values */
int  *unit;  	/* OUT: unit-pointer */
int  *null;	/* OUT: no. of null values in keyword */
int  *kstat;	/* OUT: status from SCKRDD */

{
int  stat, retstat, karr[4];
char  Comndline[40];


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (90);
else if (BKMIDAS[cid].METHOD != 's')
   return (-91);
else if (BKMIDAS[cid].ACTIVE != 'N')
   return (1);


Comndline[0] = XCONNECT.MYUNIT[0];      /* include return address */
Comndline[1] = XCONNECT.MYUNIT[1];
(void)sprintf(&Comndline[2],
        "READ/KEYW %s/d/%d/%d XCONNECT",key,felem,maxvals);

stat = ClientKRead(BKMIDAS[cid].CHAN,4,Comndline,(char *)values,
                   karr,&retstat);
if (stat == 0)
   {
   *actvals = karr[0];
   *unit = karr[1];
   *null = karr[2];
   *kstat = karr[3];
   return (0);
   }
else
   {
   *kstat = retstat;
   return (-1);
   }
}	
/*

*/

int XCKRDC(cid,key,noelm,felem,maxvals,actvals,values,unit,null,kstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
use background Midas to read data from integer keyword
.ALGORITHM
straight forward
.RETURNS
return status ( 0 = ok )
  =   1; background Midas busy
  =  -1; problems with socket communication (kstat = error from communication)
  = -90; invalid input to this interface
  = -91; only non-socket communication established
--------------------------------------------------*/

int  cid;       /* IN: id of background Midas */
char *key;      /* IN: name of keyword  */
int  noelm;     /* IN: no. of bytes per element */
int  felem;     /* IN: first data item to be read */
int  maxvals;   /* IN: no. of elements to get  */
int  *actvals;  /* OUT: actual no. of elements returned */
char *values;   /* OUT: buffer for data values */
int  *unit;     /* OUT: unit-pointer */
int  *null;     /* OUT: no. of null values in keyword */
int  *kstat;    /* OUT: status from SCKRDC */

{
int  stat, retstat, karr[4];
char  Comndline[40];


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (90);
else if (BKMIDAS[cid].METHOD != 's')
   return (-91);
else if (BKMIDAS[cid].ACTIVE != 'N')
   return (1);


Comndline[0] = XCONNECT.MYUNIT[0];      /* include return address */
Comndline[1] = XCONNECT.MYUNIT[1];
if (noelm > 1)
   (void)sprintf(&Comndline[2],
        "READ/KEYW %s/c*%d/%d/%d XCONNECT",key,noelm,felem,maxvals);
else
   (void)sprintf(&Comndline[2],
        "READ/KEYW %s/c/%d/%d XCONNECT",key,felem,maxvals);

stat = ClientKRead(BKMIDAS[cid].CHAN,3,Comndline,values,karr,&retstat);
if (stat == 0)
   {
   *actvals = karr[0];
   *unit = karr[1];
   *null = karr[2];
   *kstat = karr[3];
   return (0);
   }
else
   {
   *kstat = retstat;
   return (-1);
   }
}
/*

*/

int XCKWRI(cid,key,values,felem,maxvals,unit,kstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
use background Midas to write data into integer keyword
.ALGORITHM
straight forward
.RETURNS
return status ( 0 = ok )
  =   1; background Midas busy
  =  -1; problems with socket communication (kstat = error from communication)
  = -90; invalid input to this interface
  = -91; only non-socket communication established
--------------------------------------------------*/

int  cid;	/* IN: id of background Midas */
char *key;	/* IN: name of keyword  */
int  *values;	/* IN: buffer for data values */
int  felem;	/* IN: first data item to be written */
int  maxvals;	/* IN: no. of elements to write  */
int  *unit;  	/* IN: unit-pointer */
int  *kstat;    /* OUT: status from SCKWRI */

{
int  stat, retstat, karr[4];
char  Comndline[40];


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (-90);
else if (BKMIDAS[cid].METHOD != 's')
   return (-91);
else if (BKMIDAS[cid].ACTIVE != 'N')
   return (1);


Comndline[0] = XCONNECT.MYUNIT[0];      /* include return address */
Comndline[1] = XCONNECT.MYUNIT[1];

(void)sprintf(&Comndline[2],
        "WRITE/KEYW %s/i/%d/%d ? XCONNECT",key,felem,maxvals);

karr[0] = maxvals;
karr[1] = *unit;
karr[2] = 0;			/* `null' param. not used */
stat = ClientKWrite(BKMIDAS[cid].CHAN,1,Comndline,(char *)values,
                    karr,&retstat);
if (stat == 0)
   {
   *kstat = karr[3];
   return (0);
   }
else
   {
   *kstat = retstat;
   return (-1);
   }
}
/*

*/

int XCKWRR(cid,key,values,felem,maxvals,unit,kstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
use background Midas to write data into integer keyword
.ALGORITHM
straight forward
.RETURNS
return status ( 0 = ok )
  =   1; background Midas busy
  =  -1; problems with socket communication (kstat = error from communication)
  = -90; invalid input to this interface
  = -91; only non-socket communication established
--------------------------------------------------*/

int  cid;	/* IN: id of background Midas */
char *key;	/* IN: name of keyword  */
float *values;	/* IN: buffer for data values */
int  felem;	/* IN: first data item to be written */
int  maxvals;	/* IN: no. of elements to write  */
int  *unit;  	/* IN: unit-pointer */
int  *kstat;    /* OUT: status from SCKWRI */

{
int  stat, retstat, karr[4];
char  Comndline[40];


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (-90);
else if (BKMIDAS[cid].METHOD != 's')
   return (-91);
else if (BKMIDAS[cid].ACTIVE != 'N')
   return (1);


Comndline[0] = XCONNECT.MYUNIT[0];      /* include return address */
Comndline[1] = XCONNECT.MYUNIT[1];

(void)sprintf(&Comndline[2],
        "WRITE/KEYW %s/r/%d/%d ? XCONNECT",key,felem,maxvals);

karr[0] = maxvals;
karr[1] = *unit;
karr[2] = 0;			/* `null' param. not used */
stat = ClientKWrite(BKMIDAS[cid].CHAN,2,Comndline,(char *)values,
                    karr,&retstat);
if (stat == 0)
   {
   *kstat = karr[3];
   return (0);
   }
else
   {
   *kstat = retstat;
   return (-1);
   }
}
/*

*/

int XCKWRD(cid,key,values,felem,maxvals,unit,kstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
use background Midas to write data into integer keyword
.ALGORITHM
straight forward
.RETURNS
return status ( 0 = ok )
  =   1; background Midas busy
  =  -1; problems with socket communication (kstat = error from communication)
  = -90; invalid input to this interface
  = -91; only non-socket communication established
--------------------------------------------------*/

int  cid;	/* IN: id of background Midas */
char *key;	/* IN: name of keyword  */
double  *values;	/* IN: buffer for data values */
int  felem;	/* IN: first data item to be written */
int  maxvals;	/* IN: no. of elements to write  */
int  *unit;  	/* IN: unit-pointer */
int  *kstat;    /* OUT: status from SCKWRI */

{
int  stat, retstat, karr[4];
char  Comndline[40];


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (-90);
else if (BKMIDAS[cid].METHOD != 's')
   return (-91);
else if (BKMIDAS[cid].ACTIVE != 'N')
   return (1);


Comndline[0] = XCONNECT.MYUNIT[0];      /* include return address */
Comndline[1] = XCONNECT.MYUNIT[1];

(void)sprintf(&Comndline[2],
        "WRITE/KEYW %s/d/%d/%d ? XCONNECT",key,felem,maxvals);

karr[0] = maxvals;
karr[1] = *unit;
karr[2] = 0;			/* `null' param. not used */
stat = ClientKWrite(BKMIDAS[cid].CHAN,4,Comndline,(char *)values,
                    karr,&retstat);
if (stat == 0)
   {
   *kstat = karr[3];
   return (0);
   }
else
   {
   *kstat = retstat;
   return (-1);
   }
}
/*

*/

int XCKWRC(cid,key,noelm,values,felem,maxvals,unit,kstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
use background Midas to write data into integer keyword
.ALGORITHM
straight forward
.RETURNS
return status ( 0 = ok )
  =   1; background Midas busy
  =  -1; problems with socket communication (kstat = error from communication)
  = -90; invalid input to this interface
  = -91; only non-socket communication established
--------------------------------------------------*/

int  cid;	/* IN: id of background Midas */
char *key;	/* IN: name of keyword  */
int  noelm;     /* IN: no. of bytes per element */
char *values;	/* IN: buffer for data values */
int  felem;	/* IN: first data item to be written */
int  maxvals;	/* IN: no. of elements to write  */
int  *unit;  	/* IN: unit-pointer */
int  *kstat;    /* OUT: status from SCKWRI */

{
int  stat, retstat, karr[4];
char  Comndline[40];


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (-90);
else if (BKMIDAS[cid].METHOD != 's')
   return (-91);
else if (BKMIDAS[cid].ACTIVE != 'N')
   return (1);


Comndline[0] = XCONNECT.MYUNIT[0];      /* include return address */
Comndline[1] = XCONNECT.MYUNIT[1];

if (noelm > 1)
   (void)sprintf(&Comndline[2],
        "WRITE/KEYW %s/c*%d/%d/%d XCONNECT",key,noelm,felem,maxvals);
else
   (void)sprintf(&Comndline[2],
        "WRITE/KEYW %s/c/%d/%d XCONNECT",key,felem,maxvals);

karr[0] = maxvals * noelm;
karr[1] = *unit;
karr[2] = 0;			/* `null' param. not used */
stat = ClientKWrite(BKMIDAS[cid].CHAN,3,Comndline,values,karr,&retstat);
if (stat == 0)
   {
   *kstat = karr[3];
   return (0);
   }
else
   {
   *kstat = retstat;
   return (-1);
   }
}
