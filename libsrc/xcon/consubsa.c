/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++ module consubs +++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module consubs
.AUTHOR         K. Banse  ESO - Garching
.KEYWORDS
  connection, sockets
.COMMENTS
  holds  inxcon, Mrunning
.ENVIRONMENT  VMS and UNIX
.VERSION  [1.00] 940426:  pulled out from prepb.c
 990830

 090407		last modif
------------------------------------------------------------------------*/

#include <midas_def.h>
#include <fsydef.h>
#include <osyparms.h>
#include <xconndef.h>
#include <string.h>

#define  READ  0


/*

*/


void inxcon(munit,direc)

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  build up file names + clean up structures (partly)
.ALGORITHM
  use translation of MID_WORK stored in XCONNECT.DIREC
.RETURNS
  success code = 0, = 1 for problems
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

char *munit;            /* IN: Midas unit of sender */
char *direc;            /* IN: directory for file transfer */

{
register int  nr;

int  offs;


XCONNECT.MYPID = oshpid();
XCONNECT.MYUNIT[0] = munit[0];
XCONNECT.MYUNIT[1] = munit[1];

if (*direc != '\0')
   offs = CGN_COPY(XCONNECT.SBOX,direc);    /* get Midas startup directory */
else
   offs = 0;

(void)strcpy(&XCONNECT.SBOX[offs],"FORGR  .SBOX");         /* get send_box  */

(void)strcpy(XCONNECT.RBOX,direc);
(void)strcat(XCONNECT.RBOX,"FORGR    .RBOX");      /* get receive_box  */

XCONNECT.OFFS = offs + 5;                    /* point to sender unit */
XCONNECT.OFFR = offs + 7;                    /* point to receiver unit */


for (nr=0; nr<MAX_BACK; nr++)
   {
   BKMIDAS[nr].PID = -1;
   BKMIDAS[nr].WAIT = 0;
   BKMIDAS[nr].CHAN = 0;
   BKMIDAS[nr].UNIT[0] = ' ';
   BKMIDAS[nr].HOST[0] = '\0';
   }
}

/*

*/

int Mrunning(munit,maxtime)
char munit[];
int  maxtime;

{
int  fid, loop, n, retval;

char  cbuf[120], kbuf[120];


OSY_TRNLOG("MID_WORK",kbuf,112,&n);
if (strcmp(kbuf,"MID_WORK") == 0)
   {					/* if MID_WORK not defined, */
   OSY_TRNLOG("HOME",kbuf,112,&n);	/* use $HOME/midwork/ instead */
   (void) strcat(kbuf,"/midwork/");
   (void) sprintf(cbuf,"%sRUNNING%c%c",kbuf,munit[0],munit[1]);
   }

else
   {
   if (kbuf[n-1] != FSY_DIREND)
      {
      char  cc;

      cc = FSY_DIREND;
      (void) sprintf(cbuf,"%s%cRUNNING%c%c",kbuf,cc,munit[0],munit[1]);
      }
   else
      (void) sprintf(cbuf,"%sRUNNING%c%c",kbuf,munit[0],munit[1]);
   }

if (maxtime > 0)
   {
   retval = -1;
   loop = maxtime*2;
   while (loop > 0)
      {
      fid = osaopen(cbuf,READ);
      if (fid > -1)
         {
         n = osaread(fid,cbuf,20);
         osaclose(fid);

         if (n < 20)
            retval = 0;                 /* running, but not ready yet */
         else
            return 1;
         }
      ospuwait(500000);                 /* wait 500 msecs */
      loop --;
      }
   return retval;                       /* no Midas running */
   }

else
   {
   fid = osaopen(cbuf,READ);
   if (fid < 0)
      return -1;
   else
      {
      n = osaread(fid,cbuf,20);
      if (n < 20)
         retval = 0;            /* running, but not ready yet */
      else
         retval = 1;
      osaclose(fid);
      return retval;
      }
   }
}

