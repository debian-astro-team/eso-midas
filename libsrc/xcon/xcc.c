/* @(#)xcc.c	19.1 (ESO-DMD) 02/25/03 13:58:26 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++ XC interface module XCC +++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module XCC
.AUTHOR         K. Banse 		ESO - Garching
.KEYWORDS       background Midas, communication
.ENVIRONMENT    VMS and UNIX
.COMMENTS 
holds XCCSND, XCCWAI, XCCRDX
.REMARKS
.VERSION  [1.00] 940425: creation
981130
----------------------------------------------------------------------------*/

#include <midas_def.h>
#include <xconndef.h>



/*

*/

int XCCSND(cid,comline,secs,retstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
send a command to background Midas with/without waiting for its termination
.ALGORITHM
use `outmail'
.RETURNS
return status ( 0 = o.k )
   = 1; socket connection was closed by command just sent
   = 2; time out, `secs' seconds have passed without return 
   = -1; problems with socket/file communication
   = -90; invalid input to this interface
--------------------------------------------------------*/

int   cid;	 	/* IN: connection id */
char  *comline;		/* IN : command line to be executed by background
				Midas */
int   secs;              /* IN: max. no. of seconds to wait,
				=  0; indicates no wait 
				= -1; indicates that we wait forever  */
int   retstat[2];	/* IN: return status from Background Midas (if wait),
			       in case of status < 0 above, retstat[0]
			       holds the specific error code  */

{
int  stat;

char Comndline[MAX_MIDAS_LINE+4];


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (-90);


if (secs == 0)
   {
   BKMIDAS[cid].WAIT = 0;
   BKMIDAS[cid].FLAG = 'N';
   }
else
   {
   if (secs < 0)
      BKMIDAS[cid].WAIT = -1;              /* wait forever */
   else
      BKMIDAS[cid].WAIT = secs;
   BKMIDAS[cid].FLAG = 'Y';
   }

Comndline[0] = XCONNECT.MYUNIT[0];	/* include return address */
Comndline[1] = XCONNECT.MYUNIT[1];
(void)strncpy(&Comndline[2],comline,MAX_MIDAS_LINE);
Comndline[MAX_MIDAS_LINE+2] = '\0';		/* for security */

stat = outmail(1,Comndline,cid,retstat);
if (stat != 0)
   {
   if (stat == 4)
      stat = -1;
   else if (stat == 5)
      stat = -1;
   else if (stat == 7)			/* socket connection was closed */
      stat = 1;
   }

return (stat);
}	

/*

*/

int XCCWAI(cid,retstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
wait for return from a command which was sent to background Midas before
.ALGORITHM
use `outmail'
.RETURNS
   = 0; o.k., parameter `retstat' holds status from background Midas
   = 1; background Midas busy
   = 2; no command sent before to background unit
   = -1; problems with socket/file communication
   = -90; invalid input to this interface
--------------------------------------------------------*/

int   cid;              /* IN: connection id */
int   retstat[2];       /* IN: status from command execution in background
			       Midas if `stat' = 0;
                               if `stat' < 0  then retstat[0] holds the
			       communication error code  */

{
int  stat;

char  wstr[8];                  /* just placeholder */


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (-90);


stat = outmail(2,wstr,cid,retstat);
if (stat != 0)
   {
   if (stat == 4)
      stat = -1;
   else if (stat == 5)
      stat = -1;
   else if (stat == 9)
      stat = 2;
   }
   
return (stat);
}

/*

*/

int XCCRDX(cid,retstat)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
read the exec status of a command which was sent to background Midas before
and has terminated by now
.ALGORITHM
use `outmail'
.RETURNS
   = 0; o.k., parameter `retstat' holds exec status of command
   = 1; background Midas busy
   = 2; no command sent before to background unit
   = -1; problems with socket/file communication
   = -90; invalid input to this interface
--------------------------------------------------------*/

int   cid;              /* IN: connection id */
int   retstat[2];       /* IN: status from command execution in background
			       Midas if `stat' = 0;
                               if `stat' < 0  then retstat[0] holds the
			       communication error code  */

{
int  stat;

char  wstr[8];                  /* just placeholder */


if ((cid < 0) || (cid >= MAX_BACK) || (BKMIDAS[cid].UNIT[0] == ' '))
   return (-90);


stat = outmail(4,wstr,cid,retstat);	/* read status of last command */
if (stat != 0)
   {
   if (stat == 4)
      stat = -1;
   else if (stat == 5)
      stat = -1;
   else if (stat == 9)
      stat = 2;
   }

return (stat);
}


