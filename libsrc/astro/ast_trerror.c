/* @(#)ast_trerror.c	19.1 (ES0-DMD) 02/25/03 13:53:54 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++  
.MODULE    trerror.c
.AUTHOR    Francois Ochsenbein [ESO]
.LANGUAGE  C
.CATEGORY  Error handler for Conversions char <-> binary

.COMMENTS  This implementation passes the error to the general error handler.
		It's possible to change the tr_error routine.
.VERSION   1.0	13-Oct-1988: Creation

-------------------------*/
 
#include <osdefos.h>		/* For classes of memory (readonly) */
#include <error.h>
#include <tra.h>
#include <macrogen.h>

STATIC	struct trerr_s errs = {
	0, NULL_PTR(char), NULL_PTR(char), 0, 0};

RGLOBAL struct trerr_s *trerror = &errs;

RSTATIC	char *errlist[] = {
	"",			/* TRERR_OK		*/
	"Digit",		/* TRERR_DIGIT		*/
	"Octal",		/* TRERR_OCTAL		*/
	"Hexadecimal",		/* TRERR_HEXA		*/
	"Floating number",	/* TRERR_FLOAT		*/
	"Too large number", 	/* TRERR_TOOLARGE	*/
	"Too small number", 	/* TRERR_TOOSMALL	*/
	"Too many numbers", 	/* TRERR_TOOMANY	*/
	"Too few numbers", 	/* TRERR_TOOFEW		*/
	"No number", 		/* TRERR_VOID		*/
	"Sexagesimal", 		/* TRERR_SEXA		*/
	"Spectral Temp. Class", 	/* TRERR_SPT		*/
	"Spectral Lum. Class" 	/* TRERR_SPL		*/
	};

STATIC char *buf = NULL_PTR(char);

#define terminator	"? "

#define FINISH			goto FIN

/*===========================================================================*/
struct trerr_s *tr_errs()
/*++++++
.PURPOSE Get `packet' of last encountered translation error.
.RETURNS Address of `packet' / NULL if no error
.REMARKS Error codes are translated to comprehensive messages.
--------*/
{
  if (errs.errno == 0)	return(NULL_PTR(struct trerr_s));
  
  if (errs.errno > 0)	/* Convert code	*/
  {	if (errs.errno < ITEMS(errlist)) 
  		errs.msg = errlist[errs.errno];
  	else errs.errno  = TRERR_UNDEF;
  }
  if_not(errs.msg)	errs.msg = "Undefined";

  return(&errs);
}
  
/*===========================================================================*/
int tr_error()
/*++++++
.PURPOSE Log the error message
.RETURNS Found error number
.REMARKS Error codes are translated to comprehensive messages.
--------*/
{
	int	l;
	char	*p;
	char 	*osmmexp();	/* Memory allocation */
	
  if_not(tr_errs())	return(0);
  
  l = strlen(errs.msg);
  buf = osmmexp(buf, l + errs.len + sizeof(terminator) + 2);
  
  p = buf;
  p += oscopy(p, errs.msg, l);
  p += oscopy(p, terminator, sizeof(terminator)-1);
  if (errs.len > 0)
  {	p += oscopy(p, errs.str, errs.offset);
  	*(p++) = '^';
  	if (errs.offset < errs.len)
  	{	l = errs.offset;
  		*(p++) = *(errs.str + l++);
  		*(p++) = '^';
  		p += oscopy(p, errs.str + l, errs.len - l);
  	}
  }
  *p = '\0';
  
  ERROR(buf);			/* Log message	*/
  
  return(errs.errno);
}
