$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.LIBSRC.ASTRO]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:59 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libastro ast_bgal.obj,ast_ecl.obj,ast_jgal.obj,ast_trace.obj,ast_trigoc.obj,ast_bprec.obj,ast_edq.obj,ast_jprec.obj,ast_traq.obj,ast_trigod.obj,ast_coos.obj,ast_error.obj,ast_project.obj,ast_tratime.obj,ast_trjd.obj,ast_cotr.obj,ast_fk4fk5.obj,ast_supergal.obj,ast_trerror.obj,ast_trtime.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
