/* @(#)ast_jgal.c	19.1 (ES0-DMD) 02/25/03 13:53:53 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++
.TYPE           Module
.IDENTIFICATION jgal.c
.VERSION 1.0	27-Feb-1987: Creation
.VERSION 1.1	24-Oct-1988: Applies on Unit Vectors
.Language       C
.AUTHOR         Francois Ochsenbein [ESO-IPG]  
.KEYWORDS       J2000 / Galactic coordinate transformations

.ENV            

.COMMENTS
All spherical coordinates are assumed to be expressed in DEGREES.
No function is traced.

\begin{TeX}

The parameter mnemonics are:

\begin{itemize}
 \item f = array $[\alpha,\delta]$ of  Fundamental (J2000) equatorial coordinates 
 \item g = array $[\ell,b]$    of  Galactic   coordinates
\end{itemize}
\end{TeX}

-----------------------------------------------------------------------*/
 

#include <math.h>
#include <osdefos.h>

#define DEBUG		0	/* Debugging option	*/


RGLOBAL double gal_2000[3][3] = {	/* J2000 to galactic rotation matrix */
     	{-0.054875539726e0, -0.873437108010e0, -0.483834985808e0},
     	{ 0.494109453312e0, -0.444829589425e0,  0.746982251810e0},
     	{-0.867666135858e0, -0.198076386122e0,  0.455983795705e0}
	};

/*============================================================================*/
int tr_f5g (uf5 , ug)  
/*+++
.PURPOSE   transformation from J2000 to Galactic 
.RETURNS   OK
---*/
	double uf5[3];  /* IN: J2000 Unit Vector */
	double ug[3];  	/* OUT: Galactic Unit Vector */
{ 
  return (tr_uu ( uf5, ug, gal_2000));
}

/*============================================================================*/
int tr_gf5 (ug , uf5)  
/*+++	
.PURPOSE  transformation from Galactic to J2000
.RETURNS  OK
---*/
	double ug[3]; 	/* IN: Galactic Unit Vector 	*/
	double uf5[3];   /* OUT: J2000 Unit Vector	*/
{ 
  return (tr_uu1 ( ug, uf5, gal_2000));
}

