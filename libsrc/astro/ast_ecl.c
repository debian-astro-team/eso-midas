/* @(#)ast_ecl.c	19.1 (ES0-DMD) 02/25/03 13:53:53 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++       
.TYPE           Module
.IDENTIFICATION ecl.c
.VERSION 1.0	05-Mar-1987: Creation .
.VERSION 1.1	24-Oct-1988: Application on Unit Vectors
.Language       C
.AUTHOR         Francois Ochsenbein [ESO-IPG]  
.KEYWORDS       Ecliptic / Equatorial Coordinate conversions

.COMMENTS
All spherical coordinates are assumed to be expressed in DEGREES.

\begin{TeX}

The parameter mnemonics are:

\begin{itemize}
 \item {\bf e} = array $[ x , y , z ]$    of  Ecliptic coordinates,
		in B1950 or J2000 frame
 \item {\bf f5} = array $[\alpha_{2000},\delta_{2000}]$ of Fundamental coordinates 
	at epoch J2000
 \item {\bf f} = array $[\alpha_{1950},\delta_{1950}]$ of eQuatorial coordinates 
	at epoch B1950.
\end{itemize}


The routines provided here only allows to compute ecliptic coordinates
from/to B1950 or J2000 epochs. If other epochs are required, the following
procedure can be used:
\begin{enumerate}
\item	Determine the rotation matrix $R$ from equatorial to ecliptic
	at epoch $t$ (in Julian Years) with {\tt ecl\_R}($t$, $R$).
\item	Rotate to ecliptic frame, using 
	{\tt tr\_oo}($q$, $e$, $R$).
\end{enumerate}

If a transformation from Ecliptic to Equatorial is wished, the only
modification is to replace the {\tt tr\_oo} function of step 2 by
the {\tt tr\_oo1} function (inverse rotation).

Reference: Murray, Vectorial Analysis, section 4.3; P.T. Wallace, Starlink.
\end{TeX}

-----------------------------------------------------------------------*/

#include <osdefos.h>
#include <astro.h>
#include <trigo.h>
#include <ok.h>

static double 	RJ[3][3] = {	{1.e0, 0.e0, 0.e0},	/* Rotation in J2000 */
			{0.e0, 1.e0, 0.e0},
			{0.e0, 0.e0, 1.e0}};
static double  yj = -99999999.e0;

RGLOBAL double ecl_2000[3][3] = {		/* From J2000 => Ecl	*/
  {1.0000000000000000, 0.0000000000000000, 0.0000000000000000}, 
  {0.0000000000000000, 0.9174820620691818, 0.3977771559319137},
  {0.0000000000000000,-0.3977771559319137, 0.9174820620691818}
  };

#define DEBUG		0	/* Debugging option	*/

/*============================================================================*/
int ecl_R  (R, y)   
/*+++
.PURPOSE   Computation of Equatorial to Ecliptic rotation matrix
		at an epoch specified in Julian Years.
	The resulting matrix is such that
\begin{TeX}
\quad $\vec{u}_{ecl} = R \cdot \vec{u}_{eq}$ \quad (equatorial to ecliptic).
\end{TeX}
.RETURNS   OK
------------*/
	double y; 	/* IN: Epoch, in Julian Years		*/
	double R[3][3];	/* OUT: rotation matrix */
{
 	double eps, dt;

  dt = (y-2000.e0)/100.e0;		/* In centuries		*/

  eps = (84381.448e0 + (-46.8150e0 + (-0.00059e0+0.001813e0*dt)*dt)*dt)
	/3600.e0;			/* Mean obliquity	*/

	/* Compute Matrix (rotation around x-axis	*/

      R[0][0] = 1.e0;
      R[0][1] = 0.e0;
      R[0][2] = 0.e0;
      R[1][0] = 0.e0;
      R[2][0] = 0.e0;
      R[1][1] = cosd(eps);
      R[1][2] = sind(eps);
      R[2][1] = -R[1][2];
      R[2][2] =  R[1][1];

  return(OK);
}

/*============================================================================
 *			Transformations on Unit Vector
 *============================================================================*/

/*============================================================================*/
int tr_ef (e , f , y)  
/*+++
.PURPOSE Transformation from Ecliptic to eQuatorial.
.RETURNS OK
.REMARKS Same equinox for input and output coordinates.
---*/
	double e[2]; 	/* IN: Ecliptic angles at epoch y (degrees)	*/
	double f[3];	/* OUT: Position in Frame at equinox y		*/
	double y;	/* IN: Date for Ecliptic (Julian Years)		*/
{ 
  if (y != yj)		/* Compute Rotation Matrix	*/
	yj = y, ecl_R( RJ, yj);

  return (tr_uu1 ( e, f, RJ));
}

/*============================================================================*/
int tr_fe (f , e , y)  
/*+++
.PURPOSE Transformation from eQuatorial (B1950) to B1950 Ecliptic.
.RETURNS OK
.REMARKS Same equinox for input and output coordinates.
---*/
	double f[3];	/* IN: Vector in Equatorial frame	*/
	double e[3]; 	/* OUT: Position in Ecliptic Frame, epoch y	*/
	double y;	/* IN: Date for Ecliptic (Julain Years)		*/
{ 
  if (y != yj)		/* Compute Rotation Matrix	*/
	yj = y, ecl_R( RJ, yj);

  return (tr_uu ( f, e, RJ));
}

