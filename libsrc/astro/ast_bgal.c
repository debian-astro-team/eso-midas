/* @(#)ast_bgal.c	19.1 (ES0-DMD) 02/25/03 13:53:52 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++       
.TYPE           Module
.IDENTIFICATION bgal.c
.VERSION 1.0	27-Feb-1987: Creation .
.VERSION 1.1	24-Oct-1988: Changed to Unit Vectors
.Language       C
.AUTHOR         Francois Ochsenbein [ESO-IPG]  
.KEYWORDS       Galactic / B1950 conversions

.ENVIRONMENT	Any

.COMMENTS
All spherical coordinates are assumed to be expressed in DEGREES.
No function is traced.

\begin{TeX}

The parameter mnemonics are:

\begin{itemize}
 \item g = array $[\ell,b]$    of  Galactic   coordinates
 \item q = array $[\alpha,\delta]$ of eQuatorial coordinates (in principle B1950)
\end{itemize}
\end{TeX}

-----------------------------------------------------------------------*/
 
#include <osdefos.h>

#define DEBUG		0	/* Debugging option	*/

RGLOBAL double gal_1950[3][3] = { /* B1950 to galactic rotation matrix */
       {-0.066988739415e0,-0.872755765852e0,-0.483538914632e0},
       { 0.492728466075e0,-0.450346958020e0, 0.744584633283e0},
       {-0.867600811151e0,-0.188374601723e0, 0.460199784784e0}
                        };

/*============================================================================*/
int tr_gf4 (g , f4)  
/*+++
.PURPOSE   transformation from Galactic to (B1950)
.RETURNS   OK
---*/
	double g[3]; 	/* IN: galactic Unit Vector	*/
	double f4[3]; 	/* OUT: B1950 Unit Vector	*/
{ 
  return (tr_uu1 ( g, f4, gal_1950));
}


/*============================================================================*/
int tr_f4g (f4 , g)  
/*+++
.PURPOSE transformation from (B1950) to Galactic 
.RETURNS OK
---*/
	double f4[3];	/* IN: B1950 Unit Vector	*/
	double g[3]; 	/* OUT: galactic Unit Vector	*/
{ 
  return (tr_uu ( f4, g, gal_1950));
}

