/* @(#)ast_supergal.c	19.1 (ES0-DMD) 02/25/03 13:53:54 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++       
.TYPE           Module
.IDENTIFICATION supergal.c
.VERSION 1.0	27-Feb-1987: Creation .
.VERSION 1.1	27-Oct-1988: Use Unit Vectors
.Language       C
.AUTHOR         Francois Ochsenbein [ESO-IPG]  
.KEYWORDS       Galactic / Supergalactic conversions

.ENV            

.COMMENTS
All spherical coordinates are assumed to be expressed in DEGREES.
No function is traced.

\begin{TeX}

The SuperGalactic system is defined by the positions of
$$\begin{tabular}{|rr|rr|} \hline
	$L$ & $B$ & $\ell$ & $b$ \\ \hline
	 $0\d$ & $+90\d$ & $47.37\d$ & $+6.32\d$ \\
	$0\d$  & $0\d$   & $137.37\d$ & $0.00\d$ \\
	\hline
\end{tabular}$$


The parameter mnemonics are:

\begin{itemize}
 \item g = array $[\ell,b]$    of  Galactic   coordinates
 \item G = array $[L,B]$ of SuperGalactic coordinates.
\end{itemize}
\end{TeX}

-----------------------------------------------------------------------*/
 

#include <astro.h>
#include <osdefos.h>	
#include <trigo.h>
#include <ok.h>

#define DEBUG		0	/* Debugging option	*/

RGLOBAL double supergal[3][3] = {	/* Galactic to SuperGalactic	*/
	{-0.7357426e0, 0.6772613e0,        0.e0},
	{-0.0745538e0,-0.0809915e0, 0.9939226e0},
	{ 0.6731453e0, 0.7312712e0, 0.1100813e0}
			};

/*============================================================================*/
int tr_gsup (g , G)  
/*+++
.PURPOSE   transformation from Galactic to SuperGalactic
.RETURNS   OK
---*/
	double g[3]; 	/* IN: galactic angles l and b in degrees */
	double G[3]; 	/* OUT: Supergalactic angles in degrees */
{ 
  return (tr_uu ( g, G, supergal));
}


/*============================================================================*/
int tr_supg (G , g)  
/*+++
.PURPOSE transformation from SuperGalactic to Galactic 
.RETURNS OK
---*/
	double G[3];	/* IN: Supergalactic angles in degrees */
	double g[3]; 	/* OUT: galactic angles l and b */
{ 
  return (tr_uu1 ( G, g, supergal));
}

