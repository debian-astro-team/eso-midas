/* @(#)ast_trigoc.c	19.1 (ES0-DMD) 02/25/03 13:53:54 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++	Trigonometric Function in degrees
.TYPE		Module
.IDENTIFICATION	trigoc.c
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Special Functions using trigonometry
.COMMENTS	Arguments in RADIANS

.VERSION  1.0	09-Sep-1988: Creation
________________________________________*/

#include <math.h>	


/*=========================================================================
 *			sinc
 *=========================================================================*/
double sinc (x)
/*+++
.DES Computation of sin(x)/x
.RET value
.METHOD 
\begin{TeX}
For small argument ($x \leq 10^{-3}$), use approximation 
	$$ {\rm sinc} x = 1 - \frac{x^2}{3!} + \frac{x^4}{5!} $$
\end{TeX}

---*/
  	double x;      /* IN: argument in degrees */
{ 
	double ax, y;
	
  ax = fabs(x);

  if (ax <= 1.e-3)
  	ax *= ax, 
  	y = 1 - ax*(1.e0-ax/20.e0)/6.e0;
  else	y = sin(ax)/ax;

  return (y);
}

/*=========================================================================
 *			asinc
 *=========================================================================*/
double asinc(x)
/*+++
.DES Computes asin(x)/x
.RET value
.METHOD 
\begin{TeX}
For small argument ($x \leq 10^{-3}$), use approximation 
	$$ {\rm asinc} x = 1 + \frac{x^2}{6} + \frac{3x^4}{40} $$
\end{TeX}
---*/
  	double x;      /* argument in degrees */
{ 
	double ax, y;
	
  ax = fabs(x);

  if (ax <= 1.e-3)
  	ax *= ax, 
  	y = 1 + ax*(6.e0 + ax*(9.e0/20.e0))/6.e0;
  else	y = asin(ax)/ax;

  return (y);
}

