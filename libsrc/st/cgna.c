/*===========================================================================
  Copyright (C) 1995-2007 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++ CGN interfaces part A  +++++++++++++++++++++++++++++++
.LANGUAGE   C
.IDENTIFICATION Module CGNA
.COMMENTS
  holds  CLEANF, INDEXC, INDEXS
.AUTHOR  K. Banse	ESO - Garching
.KEYWORDS
  name translation, parsing, filling
.ENVIRONMENT VMS and UNIX

.VERSION [1.40]	870715: C-version
 070523		last modif

-----------------------------------------------------------------------------*/


#include <stdlib.h>
#include <fsydef.h>
#include <fileexts.h>
#include <osfile.h>


/*

*/

int CGN_CLEANF(infile,deftypno,outfile,lout,namtype,ext_flg)
 
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  translate file name and append default type, if required.
  also indicate, if extracted file is wanted
  cleaned filename is terminated by \0
.ALGORITHM
  straight forward via CGN_INDEXC
  we have to use FSYDEF.H, which defines file-system dependant values  !!
.RETURNS
  return status  = 0, if all o.k.
                 = -1, if bad syntax
------------------------------------------------------------------------*/
 
char   *infile;		/* IN : (complete) file name (terminated by \0) */
int    deftypno;	/* IN : default_type_no. 0 = file type will decide 
                                1 = image (.bdf), 
  		                3 = table (.tbl), 4 = fit file (.fit) 
			        5 = logfile (.log), 
 			        6 = procedure (.prg),
			        7 = catalog (.cat)        */
int    lout;		/* IN : max length of outfile */
char   *outfile;	/* OUT: "cleaned" file name */
int    *namtype;	/* OUT: type of frame:                        
                                1 = image (.bdf), 2 = any (.any)       
			        3 = table (.tbl), 4 = fit file (.fit) 
			        5 = logfile (.log), 6 = procedure (.prg) 
			        7 = catalog (.cat), 8 = test (.tst)        
			        9 = executable (.exe)    
			        0 = if something wrong...    */
int    *ext_flg;	/* OUT: = 0, if none; 
			        > 0 if index of '[' or '@' 
			        < 0 if index of [extens]   */


{
int   k, tt, inlen, typeno;
register int nr;

char	work[200];
char	*cpntr;
register char  cr;




*ext_flg = 0;
*namtype = 0;

cr = *infile;
if ((cr == ' ') || (cr == '\0')) return (-2);

if ((deftypno < 1) || (deftypno > 7))
   typeno = 0;
else 
   typeno = deftypno - 1;

inlen = (int)strlen(infile);
k = inlen - 1;


/* check if name is enclosed in special char. XFNAMC (currently = ")
   then we just strip them off  ...  */

if (cr == XFNAMC)
   {
   if (infile[k] == XFNAMC)                /* it's "name" */
      {
      if (lout < k) 
         k = lout - 1;
      else
         k --;                             /* new length of string */
      (void) memcpy(outfile,&infile[1],(size_t)k);
      outfile[k] = '\0';
      *namtype = 99;                    /* we don't check ... */
      return (0);
      }
   }


/* first look for file[... : ...] or file[extens] */

cpntr = infile;
if (infile[k] == ']')
   {
   for (nr=(k-1); nr>0; nr--)
      {
      if (infile[nr] == '[')
         {
         k = nr;
         goto ext_yes;
         }
      }
   return (-1);

  ext_yes:
   *ext_flg = k;
   (void) memcpy(work,infile,(size_t)k);
   work[k] = '\0';
   cpntr = work;

   for (nr=(k+1); nr<inlen; nr++)
      {
      if (infile[nr] == ':') goto get_logname;
      }
   *ext_flg = -k;		/* we have a FITS extension */
   }
         
/* now check for file@plane/line/pixel */

else
   {		
   k = CGN_INDEXC(infile,'@');
   if (k > 0)
      {
      *ext_flg = k;
      (void) memcpy(work,infile,(size_t)k);
      work[k] = '\0';
      cpntr = work;
      }
   } 

get_logname:
CGN_LOGNAM(cpntr,outfile,lout);		/* returns outfile with \0  */

tt = CGN_APPNDTYP(outfile,typeno);

if (deftypno == 0)
   {
   if (tt > -1)
      {
      cpntr = outfile + tt;
      for (nr=0; nr<9; nr++)
         {
         if (strcmp(cpntr,FSY_DEFPNTR[nr]) == 0)
            {
            *namtype = nr + 1;
            return(0);
            }
         }
      }
   *namtype = 1;				/* default to images */
   }

else
   *namtype = deftypno;


return (0);
}	

/*

*/

#ifdef __STDC__
int CGN_APPNDTYP(char *s, int typeno)
#else
int CGN_APPNDTYP(s, typeno)
char  *s;
int   typeno;
#endif

/* ++++++++++++++++++++++++++++++++++++++++++++++

a) append correct file type if not set yet
b) return index of .filetype if any

----------------------------------------------- */

{
int  k, tt, inlen;
register int  nr;

char  work[24], *defpntr;




k = (int) strlen(s);
tt = -1;
for (nr=k; nr>0; nr--)
   {
   if (s[nr] == FSY_TYPMARK)      /* now, look for type */
      {
      tt = nr;          /* we found no dir_specs before */
      break;
      }
   else if (s[nr] == FSY_DIREND)  /* and directory endmarker */
      break;
   }

if (tt < 0)
   {
   defpntr = FSY_DEFPNTR[typeno];               /* point to correct deftype  */
   if (KIWORDS[OFF_AUX+12] == 1)
      {                                         /* we work with FITS, */
      work[0] = '.';
      if (typeno == 0)                          /* so look for default */
         {                                      /* image/table types */
         (void) SCKGETC("MID$TYPES",1,8,&inlen,work+1);
         defpntr = work;
         }
      else if (typeno == 2)
         {
         (void) SCKGETC("MID$TYPES",9,8,&inlen,work+1);
         defpntr = work;
         }
      }

   (void) strcpy(s+k,defpntr);           /* append def type to file */
   }

return tt;
}
/*

*/

#ifdef __STDC__
int CGN_INDEXC(char *s, char t)
#else
int CGN_INDEXC(s,t)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
 returns position of single character in input string
.RETURN
 returns position of single character in input string, 
         -length(s) if not there
--------------------------------------------------*/

char *s;    /* input string  */
char t;     /* test character */
#endif

{
register int i;

char   *cp;


cp = s;
for (i=0; *cp != '\0'; i++)
   {
   if (t == *cp++) return (i);
   }

return (-1);		/* not found, return length of string (negative) */
}

/*

*/

int CGN_INDEXS(s,t)
 
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
 find position of substring in input string.

.RETURN
 returns index of substring in input string (= 0,1,2,...),
         -length(s) if not there
--------------------------------------------------*/
 
char *s;    /* input string  */
char *t;    /* substring */

{
char  *ptr;



/* library routine strstr() returns pointer to substring or NULL */

ptr = strstr(s,t);		

if (ptr == (char *) 0)
   return (-1);
else
   return (ptr - s);
}
/*

*/

int CGN_FILINFO(file,size,time,prot)

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  get file info
.ALGORITHM
  use osfinfo
.RETURNS
  return status  = 0, if all o.k.
                 = -1, if file could not be accessed
------------------------------------------------------------------------*/
char *file;
long int *size, *time;
int  *prot;

{
int  n;

struct filestatus fstat;



n = osfinfo(file,&fstat);
if (n != 0) return (-1);

*size = fstat.filesize;
*time = (long int) fstat.date;
*prot = fstat.protection;

return 0;
}

