/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++ SC interface module SCKA ++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module SCKA
.AUTHOR         K. Banse  ESO - Garching
.KEYWORDS       standard interfaces, keyword data base
.ENVIRONMENT    VMS and UNIX
.COMMENTS    
 holds SCKFND, SCKINF, SCKPRx routines

.VERSION  [1.00] 920210:  copy all the relevant stuff of SCK

 090318		last modif
-----------------------------------------------------------------------------*/

#include <fileexts.h>


/*

*/

int SCKFND(key,type,noelem,bytelem)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
provide info about keyword 'key'
.ALGORITHM
use MID_FNDKEY to find key + related info
.RETURNS
always status = 0 (ok)
--------------------------------------------------*/

char   *key     /* IN:	keyword name  */;
char   *type	/* OUT: type of key - I, R, C, D or ' '   */;
int   *noelem	/* OUT: no. of elements  */;
int   *bytelem	/* OUT: no. of bytes per element  */;

{ 	
int  kunit;


*type = ' ';			/*  find keyword */
(void) MID_FNDKEY(key,type,bytelem,noelem,&kunit);
 
return ERR_NORMAL;	/*  ignore status - always return ERR_NORMAL  */
}

/*

*/

int SCKINF(npos,fno,buf,lbuf,numbuf)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  provide info about keyword at position 'npos'
.ALGORITHM
  go through the keywords directory and provide the desired info
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

int  npos	/* IN : position of keyword */;
int  fno   	/* IN :specifies desired info, 
		       1 = NAME, 2 = TYPE, 3 = SIZE */;
char   *buf	/* OUT : buffer for NAME + TYPE info */;
int lbuf	/* IN : max. length of buffer above  */;
int *numbuf 	/* OUT: buffer for SIZE info  */;

{ 	
register int nr;
int   nn, found;
short int  bytty;
static int   old_pos = -1;

struct  KEY_STRUCT  *keypntr;




if ( (fno < 1) || (fno > 4) || (npos < 0) )
   {					/* wrong argument `fno' or `npos'  */
   old_pos = -1;			/* force new beginning...  */
   MID_E1(8,"...",ERR_INPINV,1);
   return ERR_INPINV;
   }
	
found = -1;


/*  if different keyword number (position) we have to search again  */

if (npos != old_pos)
   {
   old_pos = npos;
   nn = -1;


/*  first global, then local keys  */

   keypntr = KEYALL.KEYNAMES;
   for (nr=0; nr<=KEYALL.GLOBNO; nr++,keypntr++)
      {
      if (likely(keypntr->IDENT[0] != '/'))
         {
         nn ++;
         if (nn == npos)
            {
            found = nr;
            goto do_it;
            }
         }
      }

   keypntr = KEYALL.KEYNAMES + KEYALL.GLOBENT;
   for (nr=KEYALL.GLOBENT; nr<KEYALL.LOCNO; nr++,keypntr++)
      {
      if (keypntr->IDENT[0] != '/')
         {
         nn ++;
         if (nn == npos)
            {
            found = nr;
            goto do_it;
            }
         }
      }

   /* nothing found  at npos */

   memset((void *)buf,32,(size_t)lbuf);
   *numbuf = 0;
   old_pos = -1;				/* =>  new search next time */
   return (ERR_NORMAL);
   }
	
do_it:
keypntr = KEYALL.KEYNAMES + found;

if (fno == 1) 
   {
   if (lbuf > 15)			/* keywords, max. length = 15 */
      {
      (void) strncpy(buf,keypntr->IDENT,(size_t)15);
      buf[15] = '\0';
      }
   else
      (void) strncpy(buf,keypntr->IDENT,(size_t)lbuf);
   if (lbuf > KEY_NAMELEN) buf[KEY_NAMELEN] = '\0';
   }
else if (fno == 2)
   {
   bytty = keypntr->BYTELEM;
   MID_TYPINFO(keypntr->IDENT[15],bytty,buf,lbuf);
   }
else 
   *numbuf = keypntr->NOELEM;

return ERR_NORMAL;

}

/*

*/

int SCKPRC(prompt,key,noelm,felem,maxvals,actvals,values,unit,null)

/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
display prompt string and read keyword data from terminal
.ALGORITHM
use lower level routines MID_CPROMPT
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char *prompt    /* IN : prompt string (null terminated) */;
char *key       /* IN : name of keyword */;
int  noelm	/* IN : for character array, CHAR*noelm */;
int  felem	/* IN : 1st data item to be read */;
int  maxvals    /* IN : no. of elements to get */;
int  *actvals   /* OUT: actual no. of elements returned */;
char *values    /* OUT: buffer for data values */;
int  *unit  	/* OUT: address of unit-pointer */;
int *null	/* OUT: no. of null values in keyword */;

{
int   status, e_c;

 
*actvals = maxvals;
status = MID_CPROMPT(prompt,actvals,values,null);

if (status == ERR_NODATA) 
   {
   *actvals = 0;			/* signal, if nothing came in */
   *values = ' ';
   return status;
   }

if (status == ERR_NORMAL) 
   {			/* store the stuff into the relevant keyword  */
   e_c = ERRO_CONT;
   ERRO_CONT = 1;		/* we don't want to stop inside SCKWRC */
   status = SCKWRC(key,noelm,values,felem,*actvals,unit);
   ERRO_CONT = e_c;
   }

if (status != ERR_NORMAL) MID_E1(6,key,status,1);
return status;
}
/*

*/

int SCKPRD(prompt,key,felem,maxvals,actvals,values,unit,null)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
display prompt string and read keyword data from terminal
.ALGORITHM
use lower level routines MID_DPROMPT
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char   *prompt  /* IN : prompt string (null terminated) */;
char   *key     /* IN : name of keyword */;
int    felem	/* IN : 1st data item to be read */;
int    maxvals  /* IN : no. of elements to get */;
int    *actvals /* OUT: actual no. of elements returned */;
double *values  /* OUT: buffer for data values */;
int    *unit	/* OUT: address of unit-pointer */;
int    *null	/* OUT: no. of null values in keyword */;

{
int   status, e_c;

 
*actvals = maxvals;
status = MID_DPROMPT(prompt,actvals,values,null);

if (status == ERR_NODATA) 
   {
   *actvals = 0;			/* signal, if nothing came in */
   *values = 0.0;
   return status;
   }

if (status == ERR_NORMAL) 
   {			/* store the stuff into the relevant keyword  */
   e_c = ERRO_CONT;
   ERRO_CONT = 1;		/* we don't want to stop inside SCKWRD */
   status = SCKWRD(key,values,felem,*actvals,unit);
   ERRO_CONT = e_c;
   }

if (status != ERR_NORMAL) MID_E1(6,key,status,1);
return status;
}
/*

*/

int SCKPRI(prompt,key,felem,maxvals,actvals,values,unit,null)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
display prompt string and read keyword data from terminal
.ALGORITHM
use lower level routines MID_IPROMPT
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char *prompt    /* IN : prompt string (null terminated) */;
char *key       /* IN : name of keyword */;
int  felem	/* IN : 1st data item to be read */;
int  maxvals    /* IN : no. of elements to get */;
int  *actvals   /* OUT: actual no. of elements returned */;
int  *values    /* OUT: buffer for data values */;
int  *unit  	/* OUT: address of unit-pointer */;
int  *null	/* OUT: no. of null values in keyword */;

{
int   status, e_c;

 
*actvals = maxvals;
status = MID_IPROMPT(prompt,actvals,values,null);

if (status == ERR_NODATA) 
   {
   *actvals = 0;			/* signal, if nothing came in */
   *values = 0;
   return status;
   }

if (status == ERR_NORMAL) 
   {			/* store the stuff into the relevant keyword  */
   e_c = ERRO_CONT;
   ERRO_CONT = 1;		/* we don't want to stop inside SCKWRI */
   status = SCKWRI(key,values,felem,*actvals,unit);
   ERRO_CONT = e_c;
   }

if (status != ERR_NORMAL) MID_E1(6,key,status,1);
return status;
}
/*

*/

int SCKPRR(prompt,key,felem,maxvals,actvals,values,unit,null)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
display prompt string and read keyword data from terminal
.ALGORITHM
use lower level routines MID_RPROMPT
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char  *prompt   /* IN : prompt string (null terminated) */;
char  *key      /* IN : name of keyword */;
int   felem	/* IN : 1st data item to be read */;
int   maxvals   /* IN : no. of elements to get */;
int   *actvals  /* OUT: actual no. of elements returned */;
float *values   /* OUT: buffer for data values */;
int   *unit	/* OUT: address of unit-pointer */;
int   *null	/* OUT: no. of null values in keyword */;

{
int   status, e_c;

 
*actvals = maxvals;
status = MID_RPROMPT(prompt,actvals,values,null);

if (status == ERR_NODATA) 
   {
   *actvals = 0;			/* signal, if nothing came in */
   *values = 0.0;
   return status;
   }

if (status == ERR_NORMAL) 
   {			/* store the stuff into the relevant keyword  */
   e_c = ERRO_CONT;
   ERRO_CONT = 1;		/* we don't want to stop inside SCKWRR */
   status = SCKWRR(key,values,felem,*actvals,unit);
   ERRO_CONT = e_c;
   }

if (status != ERR_NORMAL) MID_E1(6,key,status,1);
return status;
}
/*

*/

int SCKPRS(prompt,key,felem,maxvals,actvals,values,unit,null)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
display prompt string and read keyword data from terminal
.ALGORITHM
use lower level routines MID_SPROMPT
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char   *prompt  /* IN : prompt string (null terminated) */;
char   *key     /* IN : name of keyword */;
int    felem	/* IN : 1st data item to be read */;
int    maxvals  /* IN : no. of elements to get */;
int    *actvals /* OUT: actual no. of elements returned */;
size_t *values  /* OUT: buffer for size_t values */;
int    *unit	/* OUT: address of unit-pointer */;
int    *null	/* OUT: no. of null values in keyword */;

{
int   status, e_c;

 
*actvals = maxvals;
status = MID_SPROMPT(prompt,actvals,values,null);

if (status == ERR_NODATA) 
   {
   *actvals = 0;			/* signal, if nothing came in */
   *values = 0;
   return status;
   }

if (status == ERR_NORMAL) 
   {			/* store the stuff into the relevant keyword  */
   e_c = ERRO_CONT;
   ERRO_CONT = 1;		/* we don't want to stop inside SCKWRD */
   status = SCKWRS(key,values,felem,*actvals,unit);
   ERRO_CONT = e_c;
   }

if (status != ERR_NORMAL) MID_E1(6,key,status,1);
return status;
}


