/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++ Module MIDINFO +++++++++++++++++++++++++++++++++++++++
.LANGUAGE   C
.IDENTIFICATION  Module MIDINFO.C
.AUTHOR   Klaus Banse		ESO - Garching
.COMMENTS
holds TYPINFO, SHOWFCB, SHOWFCT
.KEYWORDS MIDAS Descriptors, Keywords
.ENVIRONMENT VMS and UNIX

.VERSION
 
 090331		last modif
------------------------------------------------------------------------*/
 
#include <fileexts.h>
 
#include <stdio.h>
#include <string.h>

static struct  FCT_STRUCT  FCT_X, *fctpntr;

/*

*/ 
 
#ifdef __STDC__
void MID_TYPINFO(char dtype , int dbytes , char * type , int ltype)
#else
void MID_TYPINFO(dtype,dbytes,type,ltype)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE 
return descriptor/keyword type at current position
.RETURNS 
nothing
-----------------------------------------------------------------*/
char      dtype   /* IN: input character type I, R, D, L or C  */;
int	  dbytes  /* IN: no. of bytes per descriptor element  */;
char	  *type	  /* OUT: expanded descriptor type (e.g CHARACTER*22) */;
int	  ltype   /* IN: max length of above  */;
#endif 
{

static char	charac[9] = {'C','H','A','R','A','C','T','E','R'};
 
int   n, mm, cl;
 
 


/*  first we clear the type string  */
 
memset((void *)type,32,(size_t)ltype);
	
*type = dtype;

 
/*  already done for integer, logical, real + double precision   */
 
if (dtype != 'C') return;

 
/*  here for character arrays - that's where all the work is  */

if (dbytes == 1) return;


if (dbytes < 10)
   mm = 1;
else if (dbytes < 100)
   mm = 2;
else if (dbytes < 1000)
   mm = 3;
else if (dbytes < 10000)
   mm = 4;
else
   mm = 5;
 
cl = mm + 1;					/* bytes needed for *xxx */
	

/*  now see, how much is left  */
 
n = ltype - cl;				
if (n < 1)
   {
   *type = '*';
   *(type+1) = '*';
   *(type+2) = '*';
   return;
   }
else
   if (n > 9) n = 9;		/* minimize with length of  charac */
   	
(void) strncpy(type,charac,(size_t)n);
(void) snprintf((type+n),(size_t)6,"%d\n",dbytes);
}

/*

*/

 
#ifdef __STDC__
int MID_SHOWFCB(char *name)
#else
int MID_SHOWFCB(name)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE 
display contents of FCB of that file
.RETURNS 
 = 0 (ERR_NORMAL), if o.k.
 else, there is a problem
= 999, if outdated, not supported file format
-----------------------------------------------------------------*/
char      *name;	/* IN: file name */
#endif 


{
char    cbuf[32], tbuf[64], outbuf[80];

int     stat, imnoa;
long int  *lptr;

size_t  *sptr;

struct  FCB_STRUCT  myFCB, *fcbp;




stat = SCFOPN(name,D_OLD_FORMAT,0,F_OLD_TYPE,&imnoa);
fctpntr = FCT.ENTRIES + imnoa;
fcbp = fctpntr->FZP;

(void) snprintf(outbuf,(size_t)80,"FCB of file %s (imno %d)",name,imnoa);
SCTPUT(outbuf);
SCTPUT("   {");
(void) snprintf(outbuf,(size_t)80,"   CLONY       = %d",fcbp->CLONY);
SCTPUT(outbuf);
(void) strncpy(cbuf,fcbp->BDTYPE,8);
cbuf[8] = '\0';
(void) snprintf(outbuf,(size_t)80,"   BDTYPE[8]   = '%s'",cbuf);
SCTPUT(outbuf);
sptr = (size_t *) fcbp->XNDVAL;
(void) snprintf(outbuf,(size_t)80,"   NDVAL       = %d (as unsigned int), %zd (as size_t)", fcbp->NDVAL, *sptr);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   SWPSHORT    = '%c'",fcbp->SWPSHORT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   SWPINT      = '%c'",fcbp->SWPINT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   FLOTFMT     = '%c'",fcbp->FLOTFMT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   DSCFLAG     = '%c'",fcbp->DSCFLAG);
SCTPUT(outbuf);
(void) strncpy(cbuf,fcbp->VERSION,8);
cbuf[8] = '\0';
if ((strcmp(&cbuf[5],"006") == 0) ||
    (strcmp(&cbuf[5],"007") == 0))
   (void) strcpy(tbuf,"very old - before 96NOV");
else if (strcmp(&cbuf[5],"010") == 0)
   (void) strcpy(tbuf,"old - before 01SEP");
else if (strcmp(&cbuf[5],"100") == 0)
   (void) strcpy(tbuf,"before 02SEP");
else if (strcmp(&cbuf[5],"101") == 0)
   (void) strcpy(tbuf,"before 03FEB");
else if (strcmp(&cbuf[5],"105") == 0)
   (void) strcpy(tbuf,"before 06SEP");
else if (strcmp(&cbuf[5],"110") == 0)
   (void) strcpy(tbuf,"since 06SEP");
else if (strcmp(&cbuf[5],"120") == 0)
   (void) strcpy(tbuf,"since 09SEP");
else 
   {
   (void) strcpy(tbuf,"very, very old descr. format - not supported anymore!");
   stat = 999;
   }
(void) snprintf(outbuf,(size_t)80,"   VERSION[8]  = '%s (%s)'",cbuf,tbuf);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PROT        = %d",fcbp->PROT);
SCTPUT(outbuf) ;
(void) snprintf(outbuf,(size_t)80,
       "   DATAINFO    = %d, %d, %d, %d, %d, %d (Naxis, Npix,d_fmt,1. d_byte)",
               fcbp->DATAINFO[0],fcbp->DATAINFO[1],fcbp->DATAINFO[2],
               fcbp->DATAINFO[3],fcbp->DATAINFO[4],fcbp->DATAINFO[5]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   INCARN      = %d",fcbp->INCARN);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PTRLDB      = %d",fcbp->PTRLDB);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   LEXBDF      = %d", fcbp->LEXBDF);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PEXBDF      = %d", fcbp->PEXBDF);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   ENDLDB      = %d, %d",
               fcbp->ENDLDB,fcbp->ENDLDB_OFF);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   NOLDB       = %d",fcbp->NOLDB);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   DIREXT      = %d",fcbp->DIREXT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   DIRENTRY    = %d",fcbp->DIRENTRY);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,
       "   INLDB       = %d (dscr-dir), %d dscr-data)",
       fcbp->INLDB[0],fcbp->INLDB[1]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   DBEGIN      = %d",fcbp->DBEGIN);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,
       "   DFILLED     = %d (dscdir in use)",fcbp->DFILLED);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,
       "   DSIZE       = %d (dscdir allocated)",fcbp->DSIZE);
SCTPUT(outbuf) ;
(void) snprintf(outbuf,(size_t)80,"   NOBYT       = %d", fcbp->NOBYT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   DFORMAT     = %d",fcbp->DFORMAT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PIXPBL      = %d",fcbp->PIXPBL);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   D1BLOCK     = %d", fcbp->D1BLOCK);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   DLBLOCK     = %d", fcbp->DLBLOCK);
SCTPUT(outbuf);
(void) strncpy(cbuf,fcbp->CREATE,28);
cbuf[28] = '\0';
(void) snprintf(outbuf,(size_t)80,"   CREATE[28]  = '%s'",cbuf);
SCTPUT(outbuf);
lptr = (long int *) fcbp->CRETIM;
(void) snprintf(outbuf,(size_t)80,"   CRETIM      = %ld (above in seconds)",*lptr);
SCTPUT(outbuf);
sptr = (size_t *) fcbp->XFITSINF1;
(void) snprintf(outbuf,(size_t)80,
       "   FITSINF1    = %d (nopix in FITS file), %zd (as size_t)",
       fcbp->FITSINF1,*sptr);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,
       "   FITSINF2    = %d (offset in FITS file)", fcbp->FITSINF2);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   NEXT        = %d",fcbp->NEXT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   }           size of FCB = %d bytes",
                (int) sizeof(myFCB));
SCTPUT(outbuf);
SCTPUT("    ");

/*
if (sizeof(myFCB) > 0)
{
char  *cpt1, *cpt2;

cpt1 = (char *) &fcbp->CLONY;
cpt2 = (char *) fcbp->RESERV2;
if (cpt2 > cpt1) 
   stat = (cpt2 - cpt1);
else
   stat = (cpt1 - cpt2);
printf("&fcb->CLONY = %x, fcb->RESERV2 = %x, offset = %d\n", cpt1,cpt2,stat);
cpt2 = (char *) &fcbp->DLBLOCK;
if (cpt2 > cpt1) 
   stat = (cpt2 - cpt1);
else
   stat = (cpt1 - cpt2);
printf("&fcb->CLONY = %x, fcb->DLBLOCK = %x, offset = %d\n", cpt1,cpt2,stat);
cpt2 = (char *) &fcbp->NEXT;
if (cpt2 > cpt1) 
   stat = (cpt2 - cpt1);
else
   stat = (cpt1 - cpt2);
printf("&fcb->CLONY = %x, fcb->NEXT = %x, offset = %d\n", cpt1,cpt2,stat);
}
*/


return stat;
}

 
#ifdef __STDC__
void MID_SHOWFCT(char *name)
#else
void MID_SHOWFCT(name)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE 
display contents of FCT of that file
.RETURNS 
nothing
-----------------------------------------------------------------*/
char      *name;	/* IN: file name */
#endif

{
char   outbuf[80];

int  imnoa;





(void) SCFOPN(name,D_OLD_FORMAT,0,F_OLD_TYPE,&imnoa);

(void) snprintf(outbuf,(size_t)80,"FCT of file %s (imno %d)",name,imnoa);
SCTPUT(outbuf);
SCTPUT("   {");

fctpntr = FCT.ENTRIES + imnoa;
(void) snprintf(outbuf,(size_t)80,"   NAMLEN      = %d",fctpntr->NAMLEN);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   IOCHAN      = %d",fctpntr->IOCHAN);
SCTPUT(outbuf);

#if vms
(void) snprintf(outbuf,(size_t)80,"   FILEID(a),(b) = %d,%d",
              fctpntr->FILEIDA,fctpntr->FILEIDB);
#else
(void) snprintf(outbuf,(size_t)80,"   FILEID      = %d",fctpntr->FILEID);
#endif

SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   KAUX        = %d, %d, %d, %d",
         fctpntr->KAUX[0],fctpntr->KAUX[1],fctpntr->KAUX[2],fctpntr->KAUX[3]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   SIZE        = %zd",fctpntr->SIZE);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PROT        = %d",fctpntr->PROT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   COMPRESS    = %d",fctpntr->COMPRESS);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   NOBYTE      = %d",fctpntr->NOBYTE);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   FORMAT      = %d",fctpntr->FORMAT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PIXPBL      = %d",fctpntr->PIXPBL);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   STBLOK      = %d",fctpntr->STBLOK);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   DATTYP      = %d",fctpntr->DATTYP);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   FILTYP      = %d",fctpntr->FILTYP);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   FITSEXT     = %d",fctpntr->FITSEXT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   LINK        = %d, %d",
               fctpntr->LINK[0],fctpntr->LINK[1]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   CR_FLAG     = %d",fctpntr->CR_FLAG);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   O_NAMLEN    = %d",fctpntr->O_NAMLEN);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   NAME        = '%s'",fctpntr->NAME);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   BDADDR      = %p, %p, %p, %p",
               fctpntr->BDADDR[0],fctpntr->BDADDR[1],
               fctpntr->BDADDR[2],fctpntr->BDADDR[3]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PNTR        = %p",fctpntr->PNTR);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   CATALOG     = '%c%c'",
               fctpntr->CATALOG[0],fctpntr->CATALOG[1]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   ACCESS      = '%c'",fctpntr->ACCESS);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   }           size of FCT = %d bytes",
               (int) sizeof(FCT_X));
SCTPUT(outbuf);
SCTPUT("    ");

}

 
#ifdef __STDC__
void MID_SHOWFCT1(int imno)
#else
void MID_SHOWFCT1(imno)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE 
display contents of FCT of file with FCT entry = imno
this is like MID_SHOWFCT with FCT entry already known
.RETURNS 
nothing
-----------------------------------------------------------------*/
int    imno;	/* IN: entry in FCT */
#endif

{
char   outbuf[80];






fctpntr = FCT.ENTRIES + imno;

(void) 
snprintf(outbuf,(size_t)80,"FCT of file %s (imno %d)",fctpntr->NAME,imno);
SCTPUT(outbuf);

SCTPUT("   {");
(void) snprintf(outbuf,(size_t)80,"   NAMLEN      = %d",fctpntr->NAMLEN);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   IOCHAN      = %d",fctpntr->IOCHAN);
SCTPUT(outbuf);

#if vms
(void) snprintf(outbuf,(size_t)80,"   FILEID(a),(b) = %d,%d",
              fctpntr->FILEIDA,fctpntr->FILEIDB);
#else
(void) snprintf(outbuf,(size_t)80,"   FILEID      = %d",fctpntr->FILEID);
#endif

SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   KAUX        = %d, %d, %d, %d",
         fctpntr->KAUX[0],fctpntr->KAUX[1],fctpntr->KAUX[2],fctpntr->KAUX[3]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   SIZE        = %zd",fctpntr->SIZE);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PROT        = %d",fctpntr->PROT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   COMPRESS    = %d",fctpntr->COMPRESS);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   NOBYTE      = %d",fctpntr->NOBYTE);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   FORMAT      = %d",fctpntr->FORMAT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PIXPBL      = %d",fctpntr->PIXPBL);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   STBLOK      = %d",fctpntr->STBLOK);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   DATTYP      = %d",fctpntr->DATTYP);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   FILTYP      = %d",fctpntr->FILTYP);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   FITSEXT     = %d",fctpntr->FITSEXT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   LINK        = %d, %d",
               fctpntr->LINK[0],fctpntr->LINK[1]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   CR_FLAG     = %d",fctpntr->CR_FLAG);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   O_NAMLEN    = %d",fctpntr->O_NAMLEN);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   NAME        = '%s'",fctpntr->NAME);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   BDADDR      = %p, %p, %p, %p",
               fctpntr->BDADDR[0],fctpntr->BDADDR[1],
               fctpntr->BDADDR[2],fctpntr->BDADDR[3]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   PNTR        = %p",fctpntr->PNTR);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   CATALOG     = '%c%c'",
               fctpntr->CATALOG[0],fctpntr->CATALOG[1]);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   ACCESS      = '%c'",fctpntr->ACCESS);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   }           size of FCT = %d bytes",
               (int) sizeof(FCT_X));
SCTPUT(outbuf);
SCTPUT("    ");

}

 
#ifdef __STDC__
void MID_SHOWFCT2(int imno)
#else
void MID_SHOWFCT2(imno)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE 
display contents of FCT of file with FCT entry = imno
this is a reduced version of  MID_SHOWFCT1
.RETURNS 
nothing
-----------------------------------------------------------------*/
int    imno;	/* IN: entry in FCT */
#endif

{
char   outbuf[80];




fctpntr = FCT.ENTRIES + imno;
(void) 
snprintf(outbuf,(size_t)80,"FCT of file %s (imno %d)",fctpntr->NAME,imno);
SCTPUT(outbuf);

SCTPUT("   {");
(void) snprintf(outbuf,(size_t)80,"   IOCHAN      = %d",fctpntr->IOCHAN);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   SIZE        = %zd",fctpntr->SIZE);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   FILTYP      = %d",fctpntr->FILTYP);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   FITSEXT     = %d",fctpntr->FITSEXT);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   O_NAMLEN    = %d",fctpntr->O_NAMLEN);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   ACCESS      = '%c'",fctpntr->ACCESS);
SCTPUT(outbuf);
(void) snprintf(outbuf,(size_t)80,"   }           size of FCT = %d bytes",
               (int) sizeof(FCT_X));
SCTPUT(outbuf);
SCTPUT("    ");

}


