/*===========================================================================
  Copyright (C) 1995-2007 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++ Module MIDLOG +++++++++++++++++++++++++++++++++++++++
.LANGUAGE   C
.IDENTIFICATION  Module MIDLOG.C
.AUTHOR   Klaus Banse
.KEYWORDS Midas logging utility
.ENVIRONMENT VMS and UNIX
.COMMENTS
holds MID_LOG

.VERSION  [1.30] 861110: (K. Banse)

 070730		last modif
------------------------------------------------------------------------*/
 
#include <stdio.h>

#include <fileexts.h>
#include <fsydef.h>

 
/*

*/ 
 
#ifdef __STDC__
int MID_LOG(char flag , char * line, int linlen)
#else
int MID_LOG(flag,line,linlen)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.KEYWORDS
  logfile
.PURPOSE
  manage the logfile
.ALGORITHM
  The logfile is a sequential "stream" file with fixed records of 80 bytes,
  it is a shared file (between the MIDAS monitor + application program)
.RETURNS
  status:	I*4		return status
.REMARKS
values for flag are: 
S  - for starting up (initializing) of the logfile; 
I - for opening; O - for closing; 
G (L) - for writing a new record;
P - for starting a new page

--------------------------------------------------*/

char  flag;	/* IN: action = S, I, O, G, L, P */
char  *line;	/* IN: record to be written to logfile \
                       if 'flag' = I, it's the unit   */
int   linlen;	/* IN: length of line */

#endif 

{
register int   nr;
int   mm, fstat;
int   lbase, baseindx, pageindx;

char    strbuf[40], work[88];
 

static int    lp = -1;				/* logfile file id */
static int    lppr = -1;			/* print file id */
static char   blank_line[] = " ";
static char   new_page[] = " \f";
static char   lpframe[200];			/* logfile in MID_WORK */
 


 
if (KEYALL.ORIGIN > 9)			/* outside MIDAS */
   return (ERR_NORMAL);

/*						// revive if needed... //
if (KIWORDS[OFF_MODE+7] == 1)
   (void) printf("MID_LOG: flag = %c\n",flag);
*/

if (KIWORDS[OFF_LOG] == 0) 
   return ERR_NORMAL;		/* test log flag  */
 
	
/* -------------------------------------------------------------------
   flag = G :  enter line into logfile
   ------------------------------------------------------------------- */
 
if (flag == 'G')
   {
   if (lppr >= 0)
      {
      fstat = osawrite(lppr,line,linlen);
      if (fstat == 0) 
         {
         lppr = -1;
         KIWORDS[OFF_LOG+8] = 0;
         (void)printf("MID_LOG: could not write into print file\n");
         }
      return ERR_NORMAL;			/* job already done */
      } 
   
   if (lp < 0)
      {
      (void)strcpy(strbuf,"sync");
      goto error;
      }
 
   baseindx = OFF_LOG + 5;
   lbase = KIWORDS[baseindx];			/* get base reg. */
	
   if (lbase >= KIWORDS[OFF_LOG+6])      /*  see, if at page limit  */
      {
      lbase = 2;				/* update base reg.  */
      pageindx = OFF_LOG + 4;
      KIWORDS[pageindx] ++;
      OSY_ASCTIM(strbuf);			/* get current time  */

      (void) osawrite(lp,new_page,2);
      nr = OFF_SESS + 15;
      mm = snprintf(work,(size_t) 84,
                "*** MIDAS logfile (%10.10s) *** %s   Page %d\n",
                &KCWORDS[nr],strbuf,KIWORDS[pageindx]);
      (void) osawrite(lp,work,mm);
      (void) osawrite(lp,blank_line,1);
      }
	
   fstat = osawrite(lp,line,linlen);  /* only check last osawrite */
   if (fstat == 0)
      {
      (void)strcpy(strbuf,"write");
      goto error;
      }
 
   KIWORDS[baseindx] = ++lbase;		/* save incremented base counter  */
   }
 
 
/* -------------------------------------------------------------------
   flag = I :  open 'logfile'  inside SCSPRO 
   ------------------------------------------------------------------- */
 
 
else if (flag == 'I')
   {
   register int mr;

   lppr = -1;			/* just to make sure  */

   if (KIWORDS[OFF_LOG+8] > 0)		/* we want to print */
      {
      nr = OFF_PRINT;
      if (KCWORDS[nr] != 'F')
         {
         (void)strncpy(work,"MID_WORK:",9);
         mm = 9;
         }
      else
         mm = 0;

      (void)memcpy(&work[mm],&(KCWORDS[nr+2]),(size_t)40);
      work[mm+40] = '\0';
      for (mr=mm+1; mr<(mm+40); mr++)
         {
         if (work[mr] == ' ')
            {
            work[mr] = '\0';
            break;
            }
         }

      lppr = CGN_OPEN(work,WRITE);
      if (lppr < 0)
         (void)printf("MID_LOG: could not open print file %s\n",work);
      }

   if (lp < 0)		     /* only needed, if not already opened */
      {
      (void)memcpy(lpframe,"MID_WORK:FORGR  .LOG",(size_t)21);
      lpframe[14] = *line;
      lpframe[15] = *(line+1);
      lp = CGN_OPEN(lpframe,APPEND);
      if (lp < 0)
         {
         (void)strcpy(strbuf,"open");
         goto error;
         }
      }
   }
	
 
/* -------------------------------------------------------------------
   flag = O :  close logfile 
   ------------------------------------------------------------------- */

	
else if (flag == 'O')
   {	
   if (lppr >= 0)
      {
      osaclose(lppr);
      lppr = -1;
      }
   
   if (lp >= 0)
      {
      osaclose(lp);
      lp = -1;
      }
   }
 
/* -------------------------------------------------------------------
   flag = S :  open logfile for the first time + write first lines
   ------------------------------------------------------------------- */
	
else if (flag == 'S')
   {
   (void)memcpy(lpframe,"MID_WORK:FORGR  .LOG",(size_t)21);
   lpframe[14] = *line;
   lpframe[15] = *(line+1);
   lp = CGN_OPEN(lpframe,WRITE);
   if (lp < 0) 				/* we could not get started... */
      {
      (void)strcpy(strbuf,"open");
      goto error;
      }
 

   /*  now init everything  */
 
   baseindx = OFF_LOG + 5;
   pageindx = OFF_LOG + 4;
   OSY_ASCTIM(strbuf);
   KIWORDS[pageindx] = 1;	/* init page counter + base page reg. */
   KIWORDS[baseindx] = 2;		/* and base reg...  */
 
   nr = OFF_SESS + 15;
   mm = snprintf(work,(size_t) 84,
           "*** MIDAS logfile (%10.10s) *** %s   Page 1\n",
           &KCWORDS[nr],strbuf);
 
   (void) osawrite(lp,work,mm);
   fstat = osawrite(lp,blank_line,1);
   if (fstat == 0)				/* only check last osawrite */
      {
      (void)strcpy(strbuf,"write");
      goto error;
      }
 
   osaclose(lp);
   lp = -1;
   }
 
/* -------------------------------------------------------------------
   only left is flag = P :  start new page in logfile
   ------------------------------------------------------------------- */

else
   {
   if (lp < 0) 
      {
      (void)strcpy(strbuf,"sync");
      goto error;
      }
 
   fflush(stdout);			/* first, flush out buffer */

   baseindx = OFF_LOG + 5;
   pageindx = OFF_LOG + 4;
	
   KIWORDS[pageindx] ++;
   OSY_ASCTIM(strbuf);				/* get current time  */
 
   (void) osawrite(lp,new_page,2);
   nr = OFF_SESS + 15;
   mm = snprintf(work,(size_t) 84,
           "*** MIDAS logfile (vers. %10.10s) *** %s   Page %d\n",
           &KCWORDS[nr],strbuf,KIWORDS[pageindx]);
   (void) osawrite(lp,work,mm);
   fstat = osawrite(lp,blank_line,1);
   if (fstat == 0) 				/* only check last osawrite */
      {
      (void)strcpy(strbuf,"write");
      goto error;
      }
 
   KIWORDS[baseindx] = 2;		/* reset base counter  */
   }
 
return ERR_NORMAL;				/* get out of here */
 
 
/* -------------------------------------------------------------------
     ... here for errors ...  
-----------------------------------------------------------------------*/
 
error:
(void)printf("MID_LOG: %s error with logfile: %s\n",strbuf,lpframe);
osaclose(lp);


/* clear actual + permanent log flag */

KIWORDS[OFF_LOG] = KIWORDS[OFF_LOG+7] = 0;
(void)printf("Logging switched off now ...\n");
    
return ERR_INPINV;
}
