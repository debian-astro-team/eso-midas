/*===========================================================================
  Copyright (C) 1995-2010 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++  Module SCCA   ++++++++++++++++++++++++++++++++++++
.LANGUAGE   C
.IDENTIFICATION  Module scca.c
.COMMENTS
Holds catalog interfaces:
SCCGET, SCCLIS, SCCSHO, SCCFND, SCCSEA
.AUTHOR   Klaus Banse	ESO - Garching
.KEYWORDS MIDAS catalogues
.ENVIRONMENT independant
 
.VERSION  [1.00]  920212: pulled over from scc.c

 100616		last modif
------------------------------------------------------------------------*/
 
#include <fileexts.h>
#include <fsydef.h>

#include <stdio.h>
#include <string.h>

#define   DEFAULT_LEN   20

/*

*/
 
int SCCLIS(catfile,intval)
 
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  list contents of catalog file
.ALGORITHM
  read + display record by record
.RETURNS
  int return status
-----------------------------------------------------------------------------*/
 
char    *catfile;	/* IN: catalog file  */
int   *intval;		/* IN: listing interval [low,hi]  */
 
{
int  stat;

stat = outcat(catfile,0,intval);
return stat;
}

/*

*/

int SCCSHO(catfile,totent,lastent)
 
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  get total no. of entries and last entry in a catalog
.ALGORITHM
  straight forward
.RETURNS
  int return status
-----------------------------------------------------------------------------*/
 
char    *catfile;	/* IN: catalog file  */
int *totent;	/* OUT: no. of entries */
int *lastent;	/* OUT: last entry no. */
 
{
int  stat;
int  cattyp, cimno, catid, catlen;
int  valid, nrec, lasty=0;

char catrec[CATREC_LEN+4];

 

*totent = -1;			/*  initialize to very bad ...   */
*lastent = -1;

stat = MID_COPN(catfile,&cattyp,&cimno);
if (stat != ERR_NORMAL) 
   {
   MID_ERROR("MIDAS","SCCSHO: ",stat,1);
   return stat;
   }

catid = CATAL[cimno].FID;

stat = rewicat(catid,cimno);
if (stat < 0) 
   {
   stat = ERR_CATBAD;
   MID_ERROR("MIDAS","SCCSHO: ",stat,1);
   return stat;
   }


/*  loop through catalog + search for record no. `lorec'  */

nrec = 0;

read_loop:
catlen = readcat(catid,cimno,catrec,&valid);
if (catlen < 0) goto end_of_it;			/*  EOF reached  */
if (valid != 0)			/* valid (= non-deleted) record read */
   {
   lasty = CATAL[cimno].RECNO;
   nrec ++;
   }

goto read_loop;
 

end_of_it: 
*totent = nrec;
*lastent = lasty - 1;
return ERR_NORMAL;
	
}
/*

*/

int outcat(catfile,option,intval)
 
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  print contents of catalog file in different formats
.ALGORITHM
  read + display record by record
.RETURNS
  int return status
-----------------------------------------------------------------------------*/
 
char    *catfile;	/* IN: catalog file  */
int   option;		/* IN: = 0, normal print out \
                               = 1, just print the filenames */
int   *intval;		/* IN: listing interval [low,hi]  */
 
{
char output[CATREC_LEN+4];
char catrec[CATREC_LEN+4];
 
int   stat, valid, i, outoff;
int   cattyp, cimno, catoff, catid, catlen;
int   lorec, hirec;


lorec = intval[0];
hirec = intval[1];

stat = MID_COPN(catfile,&cattyp,&cimno);
if (stat != ERR_NORMAL) 
   {
   MID_ERROR("MIDAS","SCCLIS: ",stat,1);
   return stat;
   }

catid = CATAL[cimno].FID;

if (lorec < CATAL[cimno].RECNO)
   {
   stat = rewicat(catid,cimno);
   if (stat < 0) 
      {
      stat = ERR_CATBAD;
      MID_ERROR("MIDAS","SCCLIS: ",stat,1);
      return stat;
      }
   }

else if (lorec > CATAL[cimno].RECNO)
   {
   for (i=0; i<99999; i++)
      {
      catlen = readcat(catid,cimno,catrec,&valid);
      if (catlen < 0) goto end_of_file;			/*  EOF reached  */

      if (CATAL[cimno].RECNO == lorec) goto start_of_interval;
      }
   }


/*                  display header line                          */

start_of_interval:
lorec --;

if (option == 1) goto read_loop;		/* skip header lines */

if (CATAL[cimno].TYPE == F_TBL_TYPE)
   {
   (void)sprintf(output,"Table Catalog:  %s\n",CATAL[cimno].NAME);
   SCTPUT(output);
   (void)
   strcpy(output, "No    Name                Ident                         ");
   (void)strcat(output,"        columns rows");
   }
else if (CATAL[cimno].TYPE == F_FIT_TYPE)
   {
   (void)sprintf(output,"FitFile Catalog:  %s\n",CATAL[cimno].NAME);
   SCTPUT(output);
   (void)strcpy(output, "No    Name                Ident                         ");
   }
else if (CATAL[cimno].TYPE == F_IMA_TYPE)
   {
   (void)
   sprintf(output,"Image Catalog:  %s\n--------------",CATAL[cimno].NAME);
   SCTPUT(output);
   (void)
   strcpy(output, "No    Name                Ident                         ");
   (void)strcat(output,"     Naxis  Npix");
   }
else
   {
   (void)sprintf(output,"ASCII file Catalog:  %s\n",CATAL[cimno].NAME);
   SCTPUT(output);
   (void)strcpy(output, "No    Name                ");
   }

SCTPUT(output);


/*  loop through catalog + search for record no. 'lorec'  */

read_loop:
if (CATAL[cimno].RECNO > hirec) goto end_of_it;

lorec ++;
catlen = readcat(catid,cimno,catrec,&valid);
if (catlen < 0) goto end_of_it;			/*  EOF reached  */
if (valid == 0) goto read_loop;
 
memset((void *)output,32,(size_t)CATREC_LEN);
catoff = CGN_INDEXC(catrec,' ');
if (option == 0)
   {
   (void) sprintf(output,"#%-4.4d",lorec);
   output[5] = ' ';
   outoff = 6;
   (void) strncpy(&output[outoff],catrec,catoff);
   if (catoff < DEFAULT_LEN)
      outoff = 25;
   else
      outoff += catoff;
   CGN_strcpy(catrec,&catrec[catoff]);
   catoff = CGN_INDEXC(catrec,'^');

   if (catoff < 1)
      output[outoff] = '\0';
   else
      {
      (void) strncpy(&output[outoff],catrec,catoff);
      outoff = 63;
      output[outoff++] = ' ';
      CGN_strcpy(catrec,&catrec[catoff+1]);
      (void) strcpy(&output[outoff],catrec);
      }
   }   
else
   {
   (void) strncpy(output,catrec,catoff);
   output[catoff] = '\0';
   }

SCTPUT(output);
 
goto read_loop;


end_of_it: 
return ERR_NORMAL;
	
end_of_file:
stat = ERR_INPINV;
MID_ERROR("MIDAS","SCCLIS: ",stat,1);
return stat;
}

/*

*/

int SCCGET(catfile,flag,name,ident,no)
 
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  get next entry in catalog
.ALGORITHM
  return next name + ident of catalog file
  return a ' ' in name[0], if at EOF
.RETURNS
  int return status
------------------------------------------------------------------------- */
 
char    *catfile;	/* IN: catalog file  */
int flag;		/* IN: = 0 for name only,    \
                               = 1 for name + ident */
char	*name;		/* OUT: name of frame in next record of catalog */
char	*ident;		/* OUT: ident of that frame  */
int *no;		/* IN/OUT: current record no.  */
 
{
int   catid, catlen, i, stat, cattyp;
int   cimno, valid;

char   catrec[CATREC_LEN+4];



stat = MID_COPN(catfile,&cattyp,&cimno);
if (stat != ERR_NORMAL) 
   {
   MID_ERROR("MIDAS","SCCGET: ",stat,1);
   return stat;
   }

catid = CATAL[cimno].FID;

if (*no < 1)
   {
   if (CATAL[cimno].RECNO > 1)
      {
      stat = rewicat(catid,cimno);
      if (stat < 0) goto osa_error;
      }
   goto read_loop;
   }

else if (*no == (CATAL[cimno].RECNO - 1)) 
   goto read_loop;

else if (*no < CATAL[cimno].RECNO)
   {
   stat = rewicat(catid,cimno);
   if (stat < 0) goto osa_error;
   }

forward:
catlen = readcat(catid,cimno,catrec,&valid);
if (catlen < 0) 
   {
   *name = ' ';
   *(name+1) = '\0';
   *no = -1;
   return ERR_NORMAL;
   }

i = CATAL[cimno].RECNO - 1;
if (i == *no)
   goto read_loop;
else
   goto forward;


read_loop:
catlen = readcat(catid,cimno,catrec,&valid);
if (catlen < 0) 
   {
   *name = ' ';
   *(name+1) = '\0';
   *no = -1;
   }
 
else
   {
   if (valid == 0) goto read_loop;

   *no = CATAL[cimno].RECNO - 1;

   i = CGN_INDEXC(catrec,' ');            /* find end of name */
   if (i < 1)
      {
      (void) printf("SCCGET: no file delimiter...\n");
      i = 1;
      }
   (void) strncpy(name,catrec,i);
   name[i] = '\0';

   if (flag > 0)
      {
      CGN_strcpy(catrec,&catrec[i+1]);
      i = CGN_INDEXC(catrec,'^');
      if (i < 0) 
         (void) strcpy(ident,catrec);
      else
         {
         catrec[i] = '\0';
         (void) strcpy(ident,catrec);
         }
      }
   } 
	
return ERR_NORMAL;

osa_error:
stat = ERR_CATBAD;
MID_ERROR("MIDAS","SCCGET: ",stat,1);
return stat;
}

/*

*/

int SCCFND(catfile,frmno,frame)
 
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  find corresponding frame for given entry no.
.ALGORITHM
  search for given ntry no. + look up connected frame name
.RETURNS
  int return status
---------------------------------------------------------------------*/
 
char    *catfile;	/* IN: catalog file  */
int     frmno;		/* IN: entry no. in catalog   */
char	*frame;		/* OUT: corresponding frame name (terminated by \0)  */
 
{
char  catrec[CATREC_LEN+4];

int   catid, catlen;
int   i, n, stat;
int   cimno, cattyp, valid;


stat = MID_COPN(catfile,&cattyp,&cimno);
if (stat != ERR_NORMAL) 
   {
   MID_ERROR("MIDAS","SCCFND: ",stat,1);
   return stat;
   }

catid = CATAL[cimno].FID;

if (frmno < CATAL[cimno].RECNO)
   {
   stat = rewicat(catid,cimno);
   if (stat < 0) 
      {
      stat = ERR_CATBAD;
      MID_ERROR("MIDAS","SCCFND: ",stat,1);
      return stat;
      }
   }


read_loop:
if (frmno < CATAL[cimno].RECNO) goto not_found;

catlen = readcat(catid,cimno,catrec,&valid);
if (catlen < 0) goto not_found;			/*  EOF reached  */
if (valid == 0) goto read_loop;

n = CATAL[cimno].RECNO - 1;
if (frmno == n) 
   {
   i = CGN_INDEXC(catrec,' ');            /* find end of name */
   if (i < 1)
      {
      (void) printf("SCCFND: no file delimiter...\n");
      i = 1;
      }
   (void) strncpy(frame,catrec,i);
   frame[i] = '\0';
   }   

else
   goto read_loop; 

return ERR_NORMAL;

not_found:
 stat = ERR_INPINV;
 MID_ERROR("MIDAS","SCCFND: ",stat,1);
 return stat;
}
