/*===========================================================================
  Copyright (C) 1995-2010 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++ Module FSY ++++++++++++++++++++++++++++++++
.TYPE Module
.NAME FSY
.LANGUAGE C
.AUTHOR
  K. Banse                              ESO - Garching
.CATEGORY Image files handling.
.COMMENTS 
 holds FSY_EXTBDF, FSY_MODBDF
.VERSION 
 100222		last modif

------------------------------------------------------------------------ */

#include  <sys/types.h>   /* to be used in extbdf !!! temporarily */
#include  <sys/stat.h>
#include  <fcntl.h>
#include  <errno.h>


#include  <osyparms.h>
#include  <osparms.h>
#include  <fileexts.h>


static struct stat buf;
static off_t  filsize;

/*

*/

int FSY_EXTBDF(fileid,exalq,nvb)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE  Extend Bulk Data File
.METHOD This routine is used to extend an EXISTING Bulk Data File. The
	file is opened using osdseek and extended by the specified number 
	of blocks; the new file size is then
	calculated and returned to the calling program.
.RETURNS  status (0 if all o.k. (VMS heritage), else 1,2,3)  
-------------------------------------------------------------------------*/
int   fileid         /* file identification */;
int   exalq          /* extension allocation quantity */;
int   *nvb           /* number of virtual blocks actually allocated */;

{
off_t addr;

char c = ' ';



/* the file is supposed to have previously been opened */

addr = ((off_t)exalq * OUR_BLOCK_SIZE) - 1;
if (osdseek(fileid,addr,FILE_START) == -1L)	/* position to `nobyt-1' */
   return 1;

if (write(fileid,&c,1) != 1) return 2;		/* write only last byte */


fstat(fileid,&buf);		/* get new file size */
filsize = buf.st_size / OUR_BLOCK_SIZE;
*nvb = (int) filsize;
return 0;
}
/*
^L
*/

void FSY_OPNFIL(name, len, iochan, status)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Open Binary File
.METHOD
        This routine is used to open an EXISTING Data File for
        subsequent processing. The filename, logical or physical, is
        supplied by the calling program and the file is opened 
	via Unix open call
.RETURNS nothing
-------------------------------------------------------------------------*/

char  *name           /* file name */;
int   len             /* length of file name, not used in Unix!  */;
int   *iochan         /* iochannel (file identifier in UNIX) */;
int   *status         /* return status (1 if correct, 0 elsewhere )!**/;
   
{

int  filid;


filid = open(name,O_RDWR);


/* if no permission for attempted operation, try to open it for read only */

if (filid == -1) filid = open(name,O_RDONLY);


*iochan = filid;
if (filid < 0)              /* open has failed anyway */
   *status = 0;
else
   *status = 1;

}

/*

*/

int FSY_MODBDF(fileid, nobyt, nvb)
int   fileid;		/* file identification */
long int nobyt;		/* no. (position) of byte to be written */
int *nvb;		/* number of blocks allocated for file*/

{
char c = ' ';

off_t  addr;



addr = (off_t) (nobyt-1);

if (osdseek(fileid,addr,FILE_START) == -1L) 
   {						/* position to `nobyt-1' */
   oserror = errno;
   close(fileid);
   return(-1);
   }

if (write(fileid,&c,1) != 1) 
   {						/* Write last byte */
   oserror = errno;
   close(fileid);
   return(-1);
   }

fstat(fileid,&buf);

filsize = buf.st_size;
filsize /= OUR_BLOCK_SIZE;
*nvb = (int) filsize;
return 0;
}

