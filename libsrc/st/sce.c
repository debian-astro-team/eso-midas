/*===========================================================================
  Copyright (C) 1995-2007 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++ SC interface module SCE +++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module SCE
.AUTHOR         K. Banse 		ESO - Garching
.KEYWORDS       standard interfaces, errors
.ENVIRONMENT    VMS and UNIX
.COMMENTS 
holds SCECNT, SCETER

.VERSION 
 070703		last modif
 
----------------------------------------------------------------------------*/


#include <fileexts.h>

#include <stdlib.h>
#include <string.h>
 
/*

*/

int SCECNT(action,cont,log,disp)

/*+++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  implement the C-version of the module ERRCNTRL_ST
.ALGORITHM
  straight forward 
.RETURNS
  status	I*4		always = 0
--------------------------------------------------------*/

char   *action	  /* IN : 	GET  or  PUT   */;
int   *cont	  /* IN : pointer to continuation flag */;
int   *log	  /* IN : pointer to log flag */;
int   *disp      /* IN : pointer to display flag */;

{


if ((*action == 'G') || (*action == 'g'))	/*  get ERRO flags  */
   {
   *cont = ERRO_CONT;
   *log = ERRO_LOG;
   *disp = ERRO_DISP;
   }

else					/*  put ERRO flags  */
   {
   ERRO_CONT = *cont;
   ERRO_LOG = *log;
   ERRO_DISP = *disp;
   }
	
return ERR_NORMAL;

}
/*

*/

void SCETER(myerrno,text)

/*+++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
put error no. 'myerrno' into key PROGSTAT + stop execution
.ALGORITHM
straight forward 
.RETURNS
nothing
--------------------------------------------------------*/

int   myerrno	  /* IN: error no.  */;
char   *text	  /* IN: message to display  */;

{
int  unit=0;

char  mytext[82];



if (KIWORDS[OFF_ERROR+3] != 0)
   {
   MID_ERROR("APP",text,myerrno,1);
   SCTMES(M_RED_COLOR,text);		/* display + log error message */
   }
else
   MID_ERROR("APP",text,myerrno,0);


memset((void *)mytext,32,(size_t)80);	/* int 32 = ' '  */
(void) memcpy(mytext,text,(size_t)strlen(text));
mytext[80] = '\0';
(void) SCKWRC("MID$ERRMESS",1,mytext,1,80,&unit);

MID_ABORT(myerrno,100);		/* and disappear like anybody else... */
}

