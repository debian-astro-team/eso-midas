	.TITLE	FSY_CREBDF
; @(#)fsyvmsa.mar	19.1 (ESO-IPG) 02/25/03 13:57:17
;++
;	CREBDF - Create Bulk Data File
;
;	This routine is used to create a bulk data file. The calling
;	program specifies the filename and the initial allocation
;	quantity measured in virtual blocks.   Because of  block
;	'clustering' on FILES-11 discs, the actual number of blocks
;	allocated to the file may not be the same as that specified
;	by the calling program; allocation is always to the next
;	cluster boundary. However, the end-of-file marker will point
;	to the last block of the initial allocation quantity, rather
;	than to the boundary. Therefore, a BLOCK I/O write operation
;	is performed on the last physical block within the file. The
;	end-of-file marker will now point to this block.
;
;
; +++
;	This is the version of MID_CREBDF for C modules,
;	so character strings are handled more directly.
; +++
;	use as FSY_CREBDF(NAME,LEN,INALQ,NVB,STATUS)
;
;	Input arguments:
;	---------------
;	NAME:    char expr	Name of Bulk Data file.
;	LEN:	 I*4		Length of name 
;	INALQ:   I*4		Initial Allocation Quantity,
;				(measured in virtual blocks)
;
;	Output arguments:
;	----------------
;	NVB:     I*4		Number of Virtual Blocks
;				actually allocated for file.
;	STATUS:  I*4		Status return
;
;
;	K. Banse	version 1.00	860127
;--

BDFFAB:	$FAB	FOP=<CBT>, -
		FAC=<PUT,GET,BIO>, -
		RFM=FIX, -
		MRS=512, -
		SHR=<UPI,PUT,GET>

BDFRAB:	$RAB	FAB=BDFFAB, -
		RBF=BUFFER, -
		RSZ=512, -
		ROP=BIO

BUFFER:	.BLKB	512

FSY_CREBDF::

	.WORD	^M<>
	MOVL	04(AP),BDFFAB+FAB$L_FNA		;addr of NAME string
	MOVB	08(AP),BDFFAB+FAB$B_FNS		;size of NAME string
	MOVL	12(AP),BDFFAB+FAB$L_ALQ		;store INALQ

	$CREATE	FAB=BDFFAB
	BLBC	R0,20$

	MOVL	BDFFAB+FAB$L_ALQ,@16(AP)	;return NVB
	MOVL	@16(AP),BDFRAB+RAB$L_BKT	;store NVB also in RAB

	$CONNECT RAB=BDFRAB
	$WRITE   RAB=BDFRAB

20$:	MOVL	R0,@20(AP)			;return STATUS
	$CLOSE 	FAB=BDFFAB

	RET
	.END
