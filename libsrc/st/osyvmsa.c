/* @(#)osyvmsa.c	19.1 (ESO-DMD) 02/25/03 13:57:20 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

 02021129		last update

===========================================================================*/


#include <midas_def.h>
#include <secdef.h>
#include <ssdef.h>
#include <descrip.h>
#include <iodef.h>

#include <string.h>

#include <fctext.h>


#define  BIT_0 0x1
 
static int   status;

struct FCT_STRUCT  *fctpntr;


/*

*/
 
int OSY_ASCTIM(time)
/* ++++++++++++++++++++++++++++++++++++++++++++++++++
 
.IDENTIFICATION
  function OSY_ASCTIM		version 1.10	880112
  K. Banse			ESO - Garching
 
.KEYWORDS
  time
 
.PURPOSE
  get current time as ASCII string
 
.ALGORITHM
  use system services SYS$ASCTIM
 
.INPUT/OUTPUT
  use as  status = OSY_ASCTIM(TIME)
 
  output par:
  TIME:	char. string	will hold the returned time in ASCII
                        terminated by \0
 
-------------------------------------------------- */
 
char	*time;
 
{
 
struct dsc$descriptor_s name_descr;
 
char	mytime[24];
int     ii,status;
 
mytime[23] = '\0';		/* make sure, we have an end */
 

/* set up the VAX-11 descriptor structure */
 
name_descr.dsc$w_length = 23;
name_descr.dsc$a_pointer = mytime;
name_descr.dsc$b_class = DSC$K_CLASS_S;
name_descr.dsc$b_dtype = DSC$K_DTYPE_T;
 

/* now execute the system service  */
 
status = SYS$ASCTIM(&ii,&name_descr,0,0);
strcpy(time,mytime);		/* copy the char. string */
 
return status;
}

/*

*/
 
int OSY_ASSIGN(devnam,ldev,chanl)
/* ++++++++++++++++++++++++++++++++++++++++++++++++++

.IDENTIFICATION
  function OSY_ASSIGN		C-version 1.00	860620
  function OSY_DASSGN		C-version 1.00	890510
  K. Banse			ESO - Garching
  on source file OSYASSIGN.C

.KEYWORDS
  exit

.PURPOSE
  a) assign an I/O channel
  b) deassign an I/O channel

.ALGORITHM
  use system service SYS$ASSIGN, SYS$DASSGN
  !!! this code is operating system dependant !!!

.INPUT/OUTPUT
  use as  a) status = OSY_ASSIGN(DEVNAM,LDEV,CHANL)
  	   b) status = OSY_DASSGN(FCT_entry,iochan)

  a)
  input par:
  DEVNAM:	char.exp.	device name
  LDEV:	I*4		length of DEVNAM

  output par:
  CHANL:	I*4		I/O channel number

  return value:
  status:	I*4		MIDAS error code

  b)
  input par:
  FCT_ENTRY:	I*4		entry no. of file in FCT, -1 if not applicable
  iochan:	I*4		channel no.

  return value:
  status:	I*4		MIDAS error code

-------------------------------------------------- */
	
char	*devnam;
int   ldev, *chanl;
 
{
 
struct dsc$descriptor_s name_descr;
 

/* set up the VAX-11 descriptor structure */
 
name_descr.dsc$w_length = ldev;
name_descr.dsc$a_pointer = devnam;
name_descr.dsc$b_class = DSC$K_CLASS_S;
name_descr.dsc$b_dtype = DSC$K_DTYPE_T;
 

/* now execute the system service  */
 
status = SYS$ASSIGN(&name_descr,chanl,0,0);
 
if (status & BIT_0)
   status = ERR_NORMAL;
else
   {
   MID_ERROR("OSY","OSY_ASSIGN",status,0);
   status = ERR_OPSSYS;
   }
 
return status;
 
}
 
/*

*/
 
int OSY_CANCEL(iochan)
 
int   iochan;
 
{
 
 
status = SYS$CANCEL(iochan);
 
/* check return status from system service calls  */
 
if (status & BIT_0)
   status = ERR_NORMAL;
else
   {
   MID_ERROR("OSY","OSY_CANCEL",status,0);
   status = ERR_INPINV;
   }
 
return status;
 
}
 
/*

*/
 
int OSY_CREMBX(mailbox,chan)
 
char   *mailbox;
int   *chan;
 
{
 
struct dsc$descriptor_s name_descr;
 
 
/* set up the VAX-11 descriptor structure  */

name_descr.dsc$w_length = strlen(mailbox);
name_descr.dsc$a_pointer = mailbox;
name_descr.dsc$b_class = DSC$K_CLASS_S;
name_descr.dsc$b_dtype = DSC$K_DTYPE_T;
 
status = SYS$CREMBX(0,chan,0,0,0,255,&name_descr);
 
/* check return status from system service calls  */
 
if (status & BIT_0)
   status = ERR_NORMAL;
else
   {
   MID_ERROR("OSY","OSY_CREMBX",status,1);
   status = ERR_INPINV;
   }
 
return status;
 
}
 
/*

*/
 
int OSY_DASSGN(entrx,chanl)
 
int   entrx, chanl;
 
{
 

/* if entrx is a FCT entry, pull out channel from there  */

if (entrx >= 0) 
   {
   fctpntr = FCT.ENTRIES + entrx;
   chanl = fctpntr->IOCHAN;	

   if (chanl < 0)                       /* it's a memory file */
      {
      int  idx;

      idx = -1 - chanl;                 /* recalculate VMEM index */
      (void) MID_VMEM(3,idx,&status);
      return (ERR_NORMAL);
      }

   }
status = SYS$DASSGN(chanl);
 
if (status & BIT_0)
   status = ERR_NORMAL;
else
   {
   MID_ERROR("OSY","OSY_DASSGN",status,0);
   status = ERR_OPSSYS;
   }
 
return status;
 
}
 
/*

*/
 
int OSY_DCLEXH(fct)
 
int (* fct)();
 
{
 
static int   ixh[4], iadr;
 
/* fill exit control block  */
 
 
ixh[1] = (int) fct;
ixh[2] = 1;
ixh[3] = (int) &iadr;
 
status = SYS$DCLEXH(ixh);
 
if (status & BIT_0)
   status = ERR_NORMAL;
else
   {
   MID_ERROR("OSY","OSY_DCLEXH",status,0);	
   status = ERR_INPINV;
   }
 
return status;
}
 
/*

*/
 
int OSY_DELPRC(pid)
 
int   *pid;
 
{
 
 
/*  delete process  */
 
status = SYS$DELPRC(pid,0);
 
if (status & BIT_0)
   status = ERR_NORMAL;
else
   {
   MID_ERROR("OSY","OSY_DELPRC",status,0);
   status = ERR_INPINV;
   }
 
return status;
}
 
/*

*/
 
int OSY_ENAST(iochan,function,astmodule,astparm)
 
int   iochan, (* astmodule)(), astparm;
char	*function;
 
{
	
short int   iosb[4];
int    aux;
	
/* use QIOW calls to do the job  */
 
if (strcmp(function,"READATT") == 0)
   aux = IO$_SETMODE | IO$M_READATTN;
else if (strcmp(function,"CNTRLC") == 0)
   aux = IO$_SETMODE | IO$M_CTRLCAST;
else
   return ERR_INPINV;
 
status = SYS$QIOW(0,iochan,aux,iosb,0,0,astmodule,astparm,0,0,0,0);
 
if (status & BIT_0)
   status = ERR_NORMAL;
else
   {
   MID_ERROR("OSY","OSY_ENAST",status,0);	
   status = ERR_INPINV;
   }
 
return status;
}
 
/*

*/
 
int OSY_FORCEX(process)
 
char   *process;
 
{
 
struct dsc$descriptor_s name_descr;
 
 
name_descr.dsc$w_length = strlen(process);	
name_descr.dsc$a_pointer = process;
name_descr.dsc$b_class = DSC$K_CLASS_S;
name_descr.dsc$b_dtype = DSC$K_DTYPE_T;
 
status = SYS$FORCEX(0,&name_descr,1);
 
/* check return status from system service calls  */
 
if (status & BIT_0)
   status = ERR_NORMAL;
else
   {
   MID_ERROR("OSY","OSY_FORCEX",status,0);
   status = ERR_INPINV;
   }
 
return status;
 
}
 
/*

*/
 
void OSY_GETSYMB(symbol,name,lname)
/* ++++++++++++++++++++++++++++++++++++++++++++++++++

.IDENTIFICATION
  OSY_GETSYMB			C-version 1.10	870709
  K. Banse                   	ESO - Garching

.KEYWORDS
  symbols

.PURPOSE
  get symbols from hosa system

.ALGORITHM
  is done via LIB$GET_SYMBOL

.INPUT/OUTPUT
  use as   OSY_GETSYMB(symbol,name,lname)

  input par:
  symbol:	char. string	symbol name (terminated)
  lname:	I*4		length of 'name'

  output par:
  name:	char. string	returned name

------------------------------------------------------- */
 
int   lname;
char	*name, *symbol;
 
{
struct dsc$descriptor_s name_descr1, name_descr2;
 
int   mm;
 
 

/* set up the VAX-11 descriptor structure */
 
name_descr1.dsc$w_length = strlen(symbol);
name_descr1.dsc$a_pointer = symbol;
name_descr1.dsc$b_class = DSC$K_CLASS_S;
name_descr1.dsc$b_dtype = DSC$K_DTYPE_T;
 

/* clear result name */
 
memset((void*)name,32,(size_t)lname);
 
name_descr2.dsc$w_length = lname;
name_descr2.dsc$a_pointer = name;
name_descr2.dsc$b_class = DSC$K_CLASS_S;
name_descr2.dsc$b_dtype = DSC$K_DTYPE_T;
 

/* now execute the system service  */
 
status = LIB$GET_SYMBOL(&name_descr1,&name_descr2,&mm,&mm);
 
*(name+lname-1) = '\0';
}
 
/*

*/

int OSY_TRANSLA(name,sysname,lsysout)
/*++++++++++++++++++++++++++ Function OSY_TRNLOG +++++++++++++++++++++++++
.PURPOSE Do logical name translation.
.REMARKS uses the os interface service osftranslate.
.RETURNS
 length of translated name (if > lsysout, sysname not filled)
 < 1 if no translation
-------------------------------------------------------------------------*/
char  *name;      /* IN: logical file name */
char  *sysname;   /* OUT: physical (system dependant) file name */
int   lsysout;    /* OUT: max. length of sysname */

{
int ret;

char  tmp[256];




/* translate first into temp_buf + get length */

ret = osftranslate(name,tmp);
if ((ret > 0) && (ret < lsysout))
   {                                            /* fits into result */
   (void) memcpy(sysname,tmp,(size_t)(ret+1));
   }

return (ret);
}
 
/*

*/

int OSY_HIBER()

{
status = SYS$HIBER();

if (status & BIT_0)
   return ERR_NORMAL;
else
   return ERR_OPSSYS;
}
 
