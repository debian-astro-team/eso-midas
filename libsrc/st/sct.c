/*===========================================================================
  Copyright (C) 1995-2010 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++ SC interface module SCT +++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module SCT
.AUTHOR         K. Banse  ESO - Garching	870721
.KEYWORDS       standard interfaces.
.ENVIRONMENT    theoretically environment independant.
.COMMENTS       all SC_interfaces related to text display:
                SCTPUT, SCTSYS, SCTDIS, SCTMES

.VERSION  [1.00] 920212:  pulled out from scst.c
.VERSION  [1.10] 931025:  support ASCII file output via " >file "

 101021		last modif
-----------------------------------------------------------------------------*/
 
#include <fileexts.h>
#include <osyparms.h>
 
#include <string.h>
 
/*

*/

int SCTPUT(text)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
display a given character string and store it in the logfile
.ALGORITHM
.RETURNS
return status  ( 0 = o.k )
--------------------------------------------------*/
 
char    *text	/* IN : text string to be displayed */;
 
{
char  *txtpntr, record[8];

int ltext, outflg, kk, fp;

 



if (KIWORDS[OFF_LOG+3] == 2) return 0;	/* no output wanted ... */

outflg = 0;
if (KIWORDS[OFF_LOG+8] == 0)		/* check print flag */
   {
   if (KIWORDS[OFF_LOG+3] != 1) outflg = 1;
   }

	
ltext = (int) strlen(text);
if (ltext == 0)
   {
   ltext = CGN_COPY(record,"   ");
   txtpntr = record;
   }
else
   txtpntr = text;			/* point to original buffer */ 


if (outflg == 1)
   {
   if (KIWORDS[OFF_OUTFLG] > KIWORDS[OFF_MODE+6])
      (void)printf("%s\n",txtpntr);			/* display text  */
   else
      {					/* output => ASCII file */
      if (KIWORDS[OFF_OUTFLG+2] < 0)
         {
         if ((strncmp(&KCWORDS[OFF_OUTNAM],"Null",4) == 0) ||
/*
             (strncmp(&KCWORDS[OFF_OUTNAM],"NULL",4) == 0) ||
             (strncmp(&KCWORDS[OFF_OUTNAM],"null",4) == 0) ||
*/
             (strncmp(&KCWORDS[OFF_OUTNAM],"/dev/null",9) == 0))
            {
            if (KIWORDS[OFF_LOG+11] == -1)
               fp = 1000;
            else
               fp = 999;
            }
         else
            {
            if (KIWORDS[OFF_OUTFLG+3] == -2)
               kk = APPEND;
            else
               kk = WRITE;
            fp = CGN_OPEN(&KCWORDS[OFF_OUTNAM],kk);
            if (fp < 0)
               {
               (void)printf(
             "Problems opening ASCII output file ... output -> terminal!\n");
               KIWORDS[OFF_OUTFLG] = 99;	/* undo ASCII file output */
               (void)printf("%s\n",txtpntr);	/* display text as usual */
               (void) MID_LOG('G',txtpntr,ltext);
               return 0;
               }
            }
         KIWORDS[OFF_OUTFLG+2] = fp;
         }
      else
         fp = KIWORDS[OFF_OUTFLG+2];

      if (fp < 999) 
         osawrite(fp,txtpntr,ltext);
      else if (fp > 999) 
         return 0;

      if (KIWORDS[OFF_OUTFLG+1] > 0)
         (void)printf("%s\n",txtpntr);		/* also display text */
      }
   }

(void) MID_LOG('G',txtpntr,ltext);		/* always log it */
 
return 0;
}	
/*

*/ 

int SCTSYS(color,text)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
same as SCTPUT but ignore I/O redirection
.ALGORITHM
.RETURNS
nothing
--------------------------------------------------*/
int    color;   /* IN:  color no. for string, like for SET/GRAPH:
                        0 = default, i.e. no colour setting,
                        1 = black, 2 = red, 3 = green,
                        4 = blue, 5 = yellow, 6 = magenta,
                        7 = cyan, 8 = white */
char    *text   /* IN : text string to be displayed */;

{
char  *txtpntr, record[8];
char  colnum, colcode[10] = "081240650";

int  ltext, outflg;




if (KIWORDS[OFF_LOG+3] == 2) return 0;	/* no output wanted ... */

outflg = 0;
if (KIWORDS[OFF_LOG+8] == 0)		/* check print flag */
   {
   if (KIWORDS[OFF_LOG+3] != 1) outflg = 1;
   }

/* check colour*/

if ((color < 1) || (color > 8))
   colnum = 0;                  /* indicate no coloring */
else
   colnum = colcode[color];



ltext = (int) strlen(text);
if (ltext == 0)
   {
   ltext = CGN_COPY(record,"   ");
   txtpntr = record;
   }
else
   txtpntr = text;                      /* point to original buffer */


if (outflg == 1) 
   {						/* display text  */
   if (colnum == 0)
      (void)printf("%s\n",txtpntr);
   else
      (void)printf("\033[3%cm%s\033[0m\n",colnum,txtpntr);
   }

(void) MID_LOG('G',txtpntr,ltext);		/* inside Midas log it */

return 0;
}
/*

*/ 

int SCTDIS(text,bc)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
display a given character string as it is 
do not (!) store it in the logfile
.ALGORITHM
.RETURNS
return status  ( 0 = o.k )
--------------------------------------------------*/
 
char    *text	/* IN: text string to be displayed */;
int  bc    	/* IN: backspace count  \          
                       = -9, erase line \
                       = -1, write string + stop at end of it \
                       = 0, write line normally  \
                       = [1,80], do that many backspaces  */;
 
{
char  *txtpntr, record[8];

int ltext, kk, mcols, fp;




if (KIWORDS[OFF_LOG+3] == 2) return 0;		/* no output wanted ... */
   
mcols = 80;
if (bc == -9)			/* erase current line */
   {
   char space[80];
   
   memset((void *)space,32,(size_t)79);			/* int 32 = ' ' */
   space[79] = '\0';
   MID_TDISP(space,mcols,mcols);		/* display text  */
   return 0; 
   }


 
ltext = (int) strlen(text);
if (ltext == 0)
   {
   ltext = CGN_COPY(record,"   ");
   txtpntr = record;
   }
else
   txtpntr = text;    


if (bc == -1)
   {
   (void)printf("%s",txtpntr);
   return 0;
   }
else if (bc == 0)
   {
   (void)printf("%s\n",txtpntr);
   return 0;
   }


/* now bc is > 0 */

if (mcols < ltext) ltext = mcols;	/* no text longer than mcols */
if (bc > ltext) bc = ltext;		/* and not more '\b's than text size */

if (KIWORDS[OFF_OUTFLG] > KIWORDS[OFF_MODE+6])
   MID_TDISP(txtpntr,bc,ltext);		/* display text  */
else
   {                                      /* output => ASCII file */
   if (KIWORDS[OFF_OUTFLG+2] < 0)
      {
      if ((strncmp(&KCWORDS[OFF_OUTNAM],"Null",4) == 0) ||
          (strncmp(&KCWORDS[OFF_OUTNAM],"/dev/null",9) == 0))
         {
         if (KIWORDS[OFF_LOG+11] == -1)
            fp = 1000;
         else
            fp = 999;
         }
      else
         {
         if (KIWORDS[OFF_OUTFLG+2] == -2)
            kk = APPEND;
         else
            kk = WRITE;
         fp = CGN_OPEN(&KCWORDS[OFF_OUTNAM],kk);
         if (fp < 0)
            {
            (void)printf(
            "Problems opening ASCII output file ... output -> terminal!\n");
            KIWORDS[OFF_OUTFLG] = 99;        /* undo ASCII file output */
            MID_TDISP(txtpntr,bc,ltext);     /* display text as usual */
            return 0;
            }
         }
      KIWORDS[OFF_OUTFLG+2] = fp;
      }
   else
      fp = KIWORDS[OFF_OUTFLG+2];

   if (fp < 999) 
      osawrite(fp,txtpntr,ltext);
   else if (fp > 999) 
      return 0;

   if (KIWORDS[OFF_OUTFLG+1] > 0)
      MID_TDISP(txtpntr,bc,ltext);	   /* also display text  */
   }

return 0;
}	
/*

*/

int SCTMES(color,text)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
like SCTPUT with color for text
.ALGORITHM
.RETURNS
return status  ( 0 = o.k )
--------------------------------------------------*/
 
int    color;	/* IN:  color no. for string, like for SET/GRAPH:
                        0 = default, i.e. no colour setting,
                        1 = black, 2 = red, 3 = green,
                        4 = blue, 5 = yellow, 6 = magenta,
                        7 = cyan, 8 = white */
char    *text	/* IN : text string to be displayed */;
 
{
char  *txtpntr, record[8];
char  colnum, colcode[10] = "081243650";

int ltext, outflg, kk, fp;

 



if (KIWORDS[OFF_LOG+3] == 2) return 0;	/* no output wanted ... */

outflg = 0;
if (KIWORDS[OFF_LOG+8] == 0)		/* check print flag */
   {
   if (KIWORDS[OFF_LOG+3] != 1) outflg = 1;
   }

	
/* check colour*/

if ((color < 1) || (color > 8))
   colnum = 0;			/* indicate no coloring */
else
   colnum = colcode[color];

 
 
ltext = (int) strlen(text);
if (ltext == 0)
   {
   ltext = CGN_COPY(record,"   ");
   txtpntr = record;
   }
else
   txtpntr = text;			/* point to original buffer */ 


if (outflg == 1)
   {
   if (KIWORDS[OFF_OUTFLG] > KIWORDS[OFF_MODE+6])
      {						/* display text  */
      if (colnum == 0)
         (void)printf("%s\n",txtpntr);
      else
         (void)printf("\033[3%cm%s\033[0m\n",colnum,txtpntr);
      }
   else
      {					/* output => ASCII file */
      if (KIWORDS[OFF_OUTFLG+2] < 0)
         {
         if ((strncmp(&KCWORDS[OFF_OUTNAM],"Null",4) == 0) ||
             (strncmp(&KCWORDS[OFF_OUTNAM],"/dev/null",9) == 0))
            {
            if (KIWORDS[OFF_LOG+11] == -1)
               fp = 1000;
            else
               fp = 999;
            }
         else
            {
            if (KIWORDS[OFF_OUTFLG+3] == -2)
               kk = APPEND;
            else
               kk = WRITE;
            fp = CGN_OPEN(&KCWORDS[OFF_OUTNAM],kk);
            if (fp < 0)
               {
               (void)printf(
               "Problems opening ASCII output file ... output -> terminal!\n");
               KIWORDS[OFF_OUTFLG] = 99;	/* undo ASCII file output */
               (void)printf("%s\n",txtpntr);	/* display text as usual */
               (void) MID_LOG('G',txtpntr,ltext);
               return 0;
               }
            }
         KIWORDS[OFF_OUTFLG+2] = fp;
         }
      else
         fp = KIWORDS[OFF_OUTFLG+2];

      if (fp < 999) 
         osawrite(fp,txtpntr,ltext);
      else if (fp > 999) 
         return 0;

      if (KIWORDS[OFF_OUTFLG+1] > 0)
         (void)printf("%s\n",txtpntr);		/* also display text */
      }
   }

(void) MID_LOG('G',txtpntr,ltext);		/* always log it */
 
return 0;
}	

