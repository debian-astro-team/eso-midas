/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++ SC interface module SCKB +++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module SCKB
.AUTHOR         K. Banse  ESO - Garching
.KEYWORDS       standard interfaces, keyword data base
.ENVIRONMENT    VMS and UNIX
.COMMENTS    
holds SCKWRx routines

.VERSION  [1.00] 920213: pulled over from sck.c
 
 090318		last modif
-----------------------------------------------------------------------------*/

#include <fileexts.h>



static char k_typ[4];

static int   kunit, status;
static int   bytelem, noelem, nn, kentry, lastval;

static struct  KEY_STRUCT  *keypntr;
/*

*/

int SCKWRC(key,noelm,values,felem,maxvals,unit)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
write data into character keyword
.ALGORITHM
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char   *key	/* IN : name of keyword */;
int noelm	/* IN : for char. array, CHAR*noelm  */;
char   *values	/* IN : buffer with data values */;
int felem    /* IN : first data item to be written */;
int maxvals	/* IN : no. of elements to write */;
int   *unit	/* IN : unit-pointer */;

{	

int  offset;
register int  nr;

char *cdest;
register char  cc;



/*  find kentry in keyword control table  */
 
kentry = MID_FNDKEY(key,k_typ,&bytelem,&noelem,&kunit);
if (unlikely(kentry < 0))
   {
   status = ERR_KEYBAD;
   goto end_of_it;
   }

if ( (k_typ[0] != 'C') || (bytelem != noelm) ) 
   {				/* type + bytes-per-element have to match */
   status = ERR_KEYTYP;
   goto end_of_it;
   }

keypntr = KEYALL.KEYNAMES + kentry;		/* point to keyword entry */
nn = felem - 1;					/* offset in keyword array */
lastval = nn + maxvals;				/* get last char.  */
	

/*  check first element + no. of values  */
 
if ( (felem <= 0) || (lastval > noelem ) || (maxvals <=0) ) 
   {
   status = ERR_OUTLIM;
   goto end_of_it;
   }
	

offset = keypntr->OFFSET + (nn*bytelem);	/*  get offset in KCWORDS  */
nn = maxvals * bytelem;

/* *****************************
kk = strlen(values);		

if (kk >= nn)
   strncpy(KCWORDS+offset,values,nn);
else
   {
   strcpy(KCWORDS+offset,values);
   offset += kk;
   for (nr=kk; nr<nn; nr++) KCWORDS[offset++] = ' ';	
   }
********************************** */

cdest = KCWORDS + offset;
for (nr=0; nr<nn; nr++)			/* fill KCWORDS */
   {
   cc = *values++;
   *cdest++ = cc;
   if (cc == '\0')			/* check for Null */
      {
      register int mr;

      for (mr=nr+1;mr<nn; mr++) *cdest++ = ' ';		/* pad with blanks */
      goto next_step;
      }
   }

next_step:
keypntr->UNIT = 0;			/* not used yet */
 
return ERR_NORMAL;
 
		/*  here for errors  */
end_of_it:
MID_E1(7,key,status,1);
return status;
}
/*

*/

int SCKWRD(key,values,felem,maxvals,unit)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
write data into character keyword
.ALGORITHM
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char   *key	/* IN : name of keyword */;
double *values	/* IN : buffer with data values */;
int  felem    /* IN : first data item to be written */;
int  maxvals	/* IN : no. of elements to write */;
int   *unit	/* IN : unit-pointer */;

{	

register int  nr;

double  *dpntr;


/*  find kentry in keyword control table  */
 
kentry = MID_FNDKEY(key,k_typ,&bytelem,&noelem,&kunit);
if (unlikely(kentry < 0))
   {
   status = ERR_KEYBAD;
   goto end_of_it;
   }

if (k_typ[0] != 'D') 	/* types have to match... */
   {
   status = ERR_KEYTYP;
   goto end_of_it;
   }


/*  keyname o.k. - check first element + no. of values  */
 
lastval = felem + maxvals - 1;		/* last element to write */
if ( (felem <= 0) || (lastval > noelem) || (maxvals <=0) ) 
   {
   status = ERR_OUTLIM;
   goto end_of_it;
   }
	
keypntr = KEYALL.KEYNAMES + kentry;                      /*  point to entry */


/*  fill KDWORDS  */

dpntr = KDWORDS + keypntr->OFFSET + felem - 1; 	   /* get offset in KDWORDS */
for (nr=0; nr<maxvals; nr++)
   *dpntr++ = *values++;

keypntr->UNIT = 0;			/* not used yet */

return ERR_NORMAL;
 
		/*  here for errors  */
end_of_it:
MID_E1(7,key,status,1);
return status;
}
/*

*/

int SCKWRI(key,values,felem,maxvals,unit)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
write data into character keyword
.ALGORITHM
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char   *key	/* IN : name of keyword */;
int *values	/* IN : buffer with data values */;
int  felem    /* IN : first data item to be written */;
int  maxvals	/* IN : no. of elements to write */;
int   *unit	/* IN : unit-pointer */;

{	

register int  nr;

int  *ipntr;


/*  find kentry in keyword control table  */
 
kentry = MID_FNDKEY(key,k_typ,&bytelem,&noelem,&kunit);
if (unlikely(kentry < 0))
   {
   status = ERR_KEYBAD;
   goto end_of_it;
   }

if (k_typ[0] != 'I') 	/* types have to match... */
   {
   status = ERR_KEYTYP;
   goto end_of_it;
   }


/*  keyname o.k. - check first element + no. of values  */
 
lastval = felem + maxvals - 1;		/* last element to write */
if ( (felem <= 0) || (lastval > noelem) || (maxvals <=0) ) 
   {
   status = ERR_OUTLIM;
   goto end_of_it;
   }
	
keypntr = KEYALL.KEYNAMES + kentry;                      /*  point to entry */


/*  fill KIWORDS  */

ipntr = KIWORDS + keypntr->OFFSET + felem - 1; 	   /* get offset in KIWORDS  */
for (nr=0; nr<maxvals; nr++)
   *ipntr++ = *values++;

keypntr->UNIT = 0;			/* not used yet */

return ERR_NORMAL;
 
		/*  here for errors  */
end_of_it:
MID_E1(7,key,status,1);
return status;
}
/*

*/

int SCKWRR(key,values,felem,maxvals,unit)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
write data into character keyword
.ALGORITHM
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char   *key	/* IN : name of keyword */;
float *values	/* IN : buffer with data values */;
int  felem    /* IN : first data item to be written */;
int  maxvals	/* IN : no. of elements to write */;
int   *unit	/* IN : unit-pointer */;

{	

register int  nr;

float  *rpntr;


/*  find kentry in keyword control table  */
 
kentry = MID_FNDKEY(key,k_typ,&bytelem,&noelem,&kunit);
if (unlikely(kentry < 0))
   {
   status = ERR_KEYBAD;
   goto end_of_it;
   }

if (k_typ[0] != 'R') 	/* types have to match... */
   {
   status = ERR_KEYTYP;
   goto end_of_it;
   }


/*  keyname o.k. - check first element + no. of values  */
 
lastval = felem + maxvals - 1;		/* last element to write */
if ( (felem <= 0) || (lastval > noelem) || (maxvals <=0) ) 
   {
   status = ERR_OUTLIM;
   goto end_of_it;
   }
	
keypntr = KEYALL.KEYNAMES + kentry;                      /*  point to entry */


/*  fill KRWORDS  */

rpntr = KRWORDS + keypntr->OFFSET + felem - 1; 	  /* get offset in KRWORDS  */
for (nr=0; nr<maxvals; nr++)
   *rpntr++ = *values++;

keypntr->UNIT = 0;			/* not used yet */

return ERR_NORMAL;
 
		/*  here for errors  */
end_of_it:
MID_E1(7,key,status,1);
return status;
}
/*

*/

int SCKWRS(key,values,felem,maxvals,unit)
/*++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
write data into size_t keyword
.ALGORITHM
.RETURNS
return status ( 0 = ok )
--------------------------------------------------*/

char   *key	/* IN : name of keyword */;
size_t *values	/* IN : buffer with data values */;
int  felem    /* IN : first data item to be written */;
int  maxvals	/* IN : no. of elements to write */;
int   *unit	/* IN : unit-pointer */;

{	

register int  nr;

size_t  *spntr;


/*  find kentry in keyword control table  */
 
kentry = MID_FNDKEY(key,k_typ,&bytelem,&noelem,&kunit);
if (unlikely(kentry < 0))
   {
   status = ERR_KEYBAD;
   goto end_of_it;
   }

if (k_typ[0] != 'S') 	/* types have to match... */
   {
   status = ERR_KEYTYP;
   goto end_of_it;
   }


/*  keyname o.k. - check first element + no. of values  */
 
lastval = felem + maxvals - 1;		/* last element to write */
if ( (felem <= 0) || (lastval > noelem) || (maxvals <=0) ) 
   {
   status = ERR_OUTLIM;
   goto end_of_it;
   }
	
keypntr = KEYALL.KEYNAMES + kentry;                      /*  point to entry */


/*  fill KSWORDS  */

spntr = KSWORDS + keypntr->OFFSET + felem - 1; 	  /* get offset in KSWORDS  */
for (nr=0; nr<maxvals; nr++)
   *spntr++ = *values++;

keypntr->UNIT = 0;			/* not used yet */

return ERR_NORMAL;
 
		/*  here for errors  */
end_of_it:
MID_E1(7,key,status,1);
return status;
}

