$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.LIBSRC.ST]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:59 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ COPY osyunixc.c osyc.c
$ COPY fsyunix.c fsy.c
$ LIB/REPLACE libst cgna.obj,cgnb.obj,cgnc.obj,cgnd.obj,cgne.obj,midcat.obj,middsc.obj,middsca.obj,midfct.obj,midfcta.obj,midfctb.obj,midkey.obj,midkeya.obj,midkeyb.obj,midkeyc.obj
$ LIB/REPLACE libst miderr.obj,midinfo.obj,midldb.obj,midlog.obj,midterm.obj,midmessage.obj,scc.obj,scca.obj,scd.obj,scda.obj,scdb.obj,scdc.obj,scdx.obj,sce.obj
$ LIB/REPLACE libst scf.obj,scfa.obj,scfb.obj,scfe.obj,scfaux.obj,sci.obj,sck.obj,scka.obj,sckb.obj,scs.obj,scsa.obj,sct.obj,scp.obj,osyc.obj,fsy.obj,scaux.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
