/* @(#)testvms.c	19.1 (ES0-DMD) 02/25/03 13:57:24 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        testvms 
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Preinstallation test for the operating system. 
.COMMENTS    It writes into a logfile if this is an OPEN_VMS ("ALPHA_VMS")
             or the old VMS system ("OLD_VMS").
             The logfile will be checked in the MIDAS preinstallation 
             ("preinstall.com") to take different actions.
.VERSION 1.0 93.01.18:	Implementation. C.Guirao
------------------------------------------------------------*/

#include <stdio.h>
main(argc,argv)
int argc;
char **argv;
{

  if (argc > 1) freopen(argv[1],"w",stdout);
  else          freopen("test.log","w",stdout);

#ifdef __alpha
	printf("ALPHA_VMS\n");
#else
	printf("OLD_VMS\n");
#endif
}
