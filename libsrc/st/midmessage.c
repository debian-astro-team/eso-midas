/*===========================================================================
  Copyright (C) 1995-2007 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program; if not, write to the Free
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
  MA 02139, USA.

  Correspondence concerning ESO-MIDAS should be addressed as follows:
        Internet e-mail: midas@eso.org
        Postal address: European Southern Observatory
                        Data Management Division
                        Karl-Schwarzschild-Strasse 2
                        D 85748 Garching bei Muenchen
                        GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE      C
 
 .COPYRIGHT	(C) 1998,2004  European Southern Observatory
 .IDENT		MIDAS Monitor module pipe_message()
 .KEYWORDS	Put a formatted string to stdout and MIDAS log
 .OUTPUT	terminal and MIDAS-log output
 .RETURN	-1: the message identifier is wrong
                0:  no error occured
 .PURPOSE	Put a formatted string to stdout. The message identifier
                controls the format of the messagetext:

		id  | format                   | BELL 
		----+--------------------------+-----
		1   | ERROR   [prgname]: text  | ON
		2   | WARNING [prgname]: text  | OFF
		3   | INFO    [prgname]: text  | OFF
		4   | INFO    [prgname]: text  | OFF
 
		In case the identifier is the negative compliment then
		NO  \n  will be appended to the text.
		Following printf() may continue at that point.
 .ALGORITHM	The string passed through messagetext will be formatted and
                send to stdout (printf()). Furthermore this messagetext will
		be logged in the standard MIDAS log.
		The format of the messagetext depends on the message
		identifier id.
 .ENVIRON	MIDAS
 .LANGUAGE	C
 .AUTHOR	original pipe_message(): Sebastian Wolf, ESO-DMO
 .COMMENT       The message may contain special characters to format your
                text:   \n, \r, \t

 .VERSION	1.0 1998.01.05 created.
		moved into the Midas library (with bleeding heart...) KB
 070525		last modif	KB

-----------------------------------------------------------------------------*/

#include <fileexts.h>
#include <proto_st.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#define	BELL  0x7
#define MXID  5
#define MXLN  12
#define MAX_TBUF  120
#define MAX_TEXT  MAX_TBUF + 4
#define MAXPLEN  16
#define MAXP MAXPLEN + 3
#define OFFSET   MAXPLEN + MXLN


int  indent = 0; 

char  *Mtyp[MXID] = 
      {"        ","ERROR   ","WARNING ","INFO    ","INFO    "};

/*

*/

int check_special(inbuf,outbuf)
char *inbuf, *outbuf;

{
int i, j, n, jj;

char mybuf[MAX_TEXT];



n = (int) strlen(inbuf);
if (n > MAX_TBUF)
   {
   SCTPUT("MID_message: strlen(message-text) > 120, message truncated!");
   (void) strncpy(mybuf,inbuf,MAX_TBUF);
   mybuf[MAX_TBUF] = '\0';
   inbuf = mybuf,
   n = MAX_TBUF; 
   }

i = 0;
j = 0;
while(j < n)
   {
   outbuf[i] = inbuf[j];
   if (inbuf[j] == '\\')
      {
      jj = j + 1;
      if (inbuf[jj] == 't')
         {
         outbuf[i] = '\t';
         j ++;
         }
      else if (inbuf[jj] == 'n')
         {
         outbuf[i++] = '\n';
         outbuf[i] = '\0';
         return (jj);
         }
      else if (inbuf[jj] == 'r')
         {
         outbuf[i++] = '\r';
         outbuf[i] = '\0';
         return (jj);
         }
      }
   i++, j++;
   }

outbuf[i] = '\0';
return (0);
}
/*

*/

/* +++ new_message +++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 .IDENT		new_message()
 .KEYWORDS	Put a formatted string to stdout and MIDAS-log
 .USAGE         message(messlevel, id, prgname, messagetext)
 .INPUT		id             (int) message identifier
		               ( 1: ERROR, 2: WARNING, 3,4: INFO)
			       negative id has the same meaning except that
			       NO  \n  will be append at the end of the
			       message text!
	        *prgname       (char) name of the calling program
		*messagetext   (char) message text
 .OUTPUT	terminal and MIDAS-log output
 .RETURN	-1: the message identifier is wrong
                0:  no error occured
 .PURPOSE	Put a formatted string to stdout. The message identifier
                controls the format of the messagetext:

		id  | format                   | BELL 
		----+--------------------------+-----
		1   | ERROR   [prgname]: text  | ON
		2   | WARNING [prgname]: text  | OFF
		3   | INFO    [prgname]: text  | OFF
		4   | INFO    [prgname]: text  | OFF
 
 .ALGORITHM	The string passed through messagetext will be formatted and
                send to stdout (printf()). Furthermore this messagetext will
		be logged in the standard MIDAS log.
		The format of the messagetext depends on the message
		identifier id.
 .COMMENT       The message may contain special characters to format your
                text:   \n, \r, \t, \b
---------------------------------------------------------------------------- */


static int new_message(id, pnam, line)
int  id;
char *pnam, *line;

{
char bell, *myptr;
char  txt[MAX_TEXT+MAXPLEN+20], mes[MAX_TEXT];

int   n, k, mm, plen;





plen = (int)strlen(pnam);
if (plen>MAXPLEN)
   {
   pnam[MAXPLEN] = '\0';	/* cut program name if too long */
   plen = MAXPLEN;
   }
k = plen + 9;

myptr = line;			/* point to begin of message buffer */
mm = check_special(myptr,mes);	/* look for \t, \n, \r */

if (id == 1)			/* for errors we beep! */
   {
   bell = BELL;
   n = snprintf(txt,MAX_TEXT+MAXPLEN+20,
               "%-8s[%-19s%s%c",Mtyp[id],pnam,mes,bell);
   }
else
   n = snprintf(txt,MAX_TEXT+MAXPLEN+20,
               "%-8s[%-19s%s",Mtyp[id],pnam,mes);

txt[k++] = ']';
txt[k] = ':';
(void) printf("%s",txt);

if (mm > 0) 
   {
   txt[--n] = '\0';		/* remove special chars */
   MID_LOG('G',txt,n);
   }
else
   {
   MID_LOG('G',txt,n);
   printf("\n");
   return (0);
   }


replace_loop:
myptr += (mm+1);
if (*myptr != '\0') 
   {
   mm = check_special(myptr,mes);
   memset((void *)txt,32,(size_t)(OFFSET+indent));
   (void) strcpy(&txt[OFFSET+indent],mes);
   (void) printf("%s",txt);
   n = (int) strlen(txt);
   if (mm > 0) 
      {
      txt[--n] = '\0';		/* remove special chars */
      MID_LOG('G',txt,n);
      goto replace_loop;
      }

   MID_LOG('G',txt,n);
   printf("\n");
   }

return(0);
}
/*

*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++

 wrapper for new_message() to handle the "extern int indent"

   +++++++++++++++++++++++++++++++++++++++++++++++++ */

int MID_message(id,nam,buf,indnt)
int  id, indnt;
char *nam, *buf;

{
indent = indnt;				/* needed to get same behaviour
					   as before with extenal `indent' */
return (new_message(id,nam,buf));
}





/* ----------------------------------- */
/* interface for old pipeline programs */
/* ----------------------------------- */

int messout(messlevel,id,nam,buf)
int  id, messlevel;
char *nam, *buf;

{
id = abs(id);
if (id > messlevel)
   return(-1);
else
   return (new_message(id,nam,buf));
}
 
