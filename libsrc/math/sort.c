/* @(#)sort.c	19.1 (ES0-DMD) 02/25/03 13:55:39 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c)  1995  European Southern Observatory
.IDENT       hsort.c
.LANGUAGE    C
.AUTHOR      P.Grosbol,  IPG/ESO
.ENVIRON     UNIX
.KEYWORDS    sort, heapsort
.COMMENT     Making use of heapsort.c module
.VERSION     1.0  1995-Mar-09 : Creation,  PJG
.VERSION     2.0  2015-Feb-09 : Using heapsort module, PBA
-----------------------------------------------------------------------*/

#include <stdlib.h>
#include "mutil.h" 

void hsort(n, ra)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   sort array in place using heapsort
.RETURN    none
-----------------------------------------------------------------------*/
int        n;                    /* no. of elements in array           */
float      *ra;                   /* pointer to array to be sorted      */
{

  int *ia = (int*)malloc((size_t)n*sizeof(int)); 
  heapSortFloat(n,ra,ia); 
  free(ia); 

}

int compare_doubles (
   const void *a, 
   const void *b)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   compares doubles the way strncmp does
.RETURN    integer value, negative if the first argument is "less" than 
           the second, zero if they are "equal", and positive if the first 
           argument is "greater". 
-----------------------------------------------------------------------*/
{
  return (int) (*(double*)a - *(double*)b);
}

int compare_floats (
   const void *a, 
   const void *b)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   compares doubles the way strncmp does
.RETURN    integer value, negative if the first argument is "less" than 
           the second, zero if they are "equal", and positive if the first 
           argument is "greater". 
-----------------------------------------------------------------------*/
{
  return (int) (*(float*)a - *(float*)b);
}


int compare_ints (
   const void *a, 
   const void *b)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   compares integers the way strncmp does
.RETURN    integer value, negative if the first argument is "less" than 
           the second, zero if they are "equal", and positive if the first 
           argument is "greater". 
-----------------------------------------------------------------------*/
{
  return (int) (*(int*)a - *(int*)b);
}

void std_sorti(n, ra)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   sort array in place using quicksort algorithm for stdlib.h
.RETURN    none
-----------------------------------------------------------------------*/
int        n;                    /* no. of elements in array           */
int        *ra;                   /* pointer to array to be sorted      */
{
  qsort(ra, n, sizeof(int), compare_ints); 
}

void std_sortf(n, ra)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   sort array in place using quicksort algorithm for stdlib.h
.RETURN    none
-----------------------------------------------------------------------*/
int        n;                    /* no. of elements in array           */
float      *ra;                   /* pointer to array to be sorted      */
{
  qsort(ra, n, sizeof(float), compare_floats); 
}

void std_sortd(n, ra)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   sort array in place using quicksort algorithm for stdlib.h
.RETURN    none
-----------------------------------------------------------------------*/
int        n;                    /* no. of elements in array           */
double     *ra;                   /* pointer to array to be sorted      */
{
  // Calling STDLIB qsort
  qsort(ra, n, sizeof(double), compare_doubles); 
}

void quick_sort(n, ra)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   sort array in place using heapsort
.RETURN    none
-----------------------------------------------------------------------*/
int        n;                    /* no. of elements in array           */
float      *ra;                  /* pointer to array to be sorted      */
{
  std_sortf(n, ra); 
}


