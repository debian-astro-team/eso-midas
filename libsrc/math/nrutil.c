/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       .prg                                       */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    Spectroscopy, Long-Slit                    */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Creation  17-MAR-1993 

051021		last modif			           */
/* ------------------------------------------------------- */

/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

#include <stdlib.h>
#include <stdio.h>

void nrerror(error_text)
char error_text[];
{
	fprintf(stderr,"Exiting to system on run-time error...\n");
	fprintf(stderr,"%s\n",error_text);
	fprintf(stderr,"...now exiting to system...\n");
	exit(1);
}



float *vector(nl,nh)
int nl,nh;
{
	float *v;

	v = (float *)malloc((size_t)(nh-nl+1)*sizeof(float));
	if (!v) nrerror("allocation failure in vector()");
	return(v-nl);
}

int *ivector(nl,nh)
int nl,nh;
{
	int *v;

        v = (int *)malloc((size_t) (nh-nl+1)*sizeof(int)); 
	if (!v) nrerror("allocation failure in ivector()");
	return(v-nl);
}

double *dvector(nl,nh)
int nl,nh;
{
	double *v;

	v = (double *)malloc((size_t) (nh-nl+1)*sizeof(double));
	if (!v) nrerror("allocation failure in dvector()");
	return(v-nl);
}

float *fvector(nl,nh) 
int nl,nh;
{
	float *v;

	v = (float *)malloc((size_t) (nh-nl+1)*sizeof(float));
	if (!v) nrerror("allocation failure in fvector()");
	return(v-nl);
}

char *cvector(nl,nh) 
int nl,nh;
{
	char *v;

	v = (char *)malloc((size_t) (nh-nl+1) * sizeof(char));
	if (!v) nrerror("allocation failure in cvector()");
	return(v-nl);
}


float **matrix(nrl,nrh,ncl,nch)
int nrl,nrh,ncl,nch;
{
	int i;
	float **m;

	m = (float **)malloc((size_t) (nrh-nrl+1)*sizeof(float*));
	if (!m) nrerror("allocation failure 1 in matrix()");
	m -= nrl;

	for(i=nrl;i<=nrh;i++) {
		m[i]= (float *)malloc((size_t) (nch-ncl+1)*sizeof(float));
		if (!m[i]) nrerror("allocation failure 2 in matrix()");
		m[i] -= ncl;
	}
	return(m);
}

float **fmatrix(nrl,nrh,ncl,nch)
int nrl,nrh,ncl,nch;
{
    return matrix(nrl,nrh,ncl,nch);
}

double **dmatrix(nrl,nrh,ncl,nch)
int nrl,nrh,ncl,nch;
{
	int i;
	double **m;

	m = (double **)malloc((size_t) (nrh-nrl+1)*sizeof(double*));
	if (!m) nrerror("allocation failure 1 in dmatrix()");
	m -= nrl;

	for(i=nrl;i<=nrh;i++) {
		m[i] = (double *)malloc((size_t) (nch-ncl+1)*sizeof(double));
		if (!m[i]) nrerror("allocation failure 2 in dmatrix()");
		m[i] -= ncl;
	}
	return(m);
}

int **imatrix(nrl,nrh,ncl,nch)
int nrl,nrh,ncl,nch;
{
	int i,**m;

	m = (int **)malloc((size_t) (nrh-nrl+1)*sizeof(int*));
	if (!m) nrerror("allocation failure 1 in imatrix()");
	m -= nrl;

	for(i=nrl;i<=nrh;i++) {
		m[i] = (int *)malloc((size_t) (nch-ncl+1)*sizeof(int));
		if (!m[i]) nrerror("allocation failure 2 in imatrix()");
		m[i] -= ncl;
	}
	return(m);
}

float **submatrix(a,oldrl,oldrh,oldcl,oldch,newrl,newcl)
float **a;
int oldrl,oldrh,oldcl,oldch,newrl,newcl;
{
	int i,j;
	float **m;

 	oldch++;		/* Not use. This is to avoid LintPlus warning */
	m = (float **)malloc((size_t) (oldrh-oldrl+1)*sizeof(float*));
	if (!m) nrerror("allocation failure in submatrix()");
	m -= newrl;

	for(i=oldrl,j=newrl;i<=oldrh;i++,j++) m[j]=a[i]+oldcl-newcl;

	return(m);
}

char **cmatrix(nrl,nrh,ncl,nch) 
int nrl,nrh,ncl,nch;
{
	int i;
	char **m;

	m = (char **)malloc((size_t) (nrh-nrl+1) * sizeof(char *));
	if (!m) nrerror("allocation failure in cmatrix()");
    	m -= nrl;

	for (i=nrl;i<=nrh;i++) {
		m[i] = (char *)malloc((size_t) (nch-ncl+1)*sizeof(char));
        	m[i] -= ncl;
    	}
	return(m);
}

float **convert_matrix(a,nrl,nrh,ncl,nch)
float *a;
int nrl,nrh,ncl,nch;
{
	int i,j,nrow,ncol;
	float **m;

	nrow=nrh-nrl+1;
	ncol=nch-ncl+1;
	m = (float **)malloc((size_t) (nrow)*sizeof(float*));
	if (!m) nrerror("allocation failure in convert_matrix()");
	m -= nrl;
	for(i=0,j=nrl;i<=nrow-1;i++,j++) m[j]=a+ncol*i-ncl;
	return(m);
}

void free_vector(v,nl,nh)
float *v;
int nl,nh;
{
 	nh++;		/* No use. Just to avoid LintPlus warning */
	free((v+nl));
}

void free_ivector(v,nl,nh)
int *v,nl,nh;
{
 	nh++;		/* No use. Just to avoid LintPlus warning */
	free((v+nl));
}

void free_dvector(v,nl,nh)
double *v;
int nl,nh;
{
 	nh++;		/* No use. Just to avoid LintPlus warning */
	free((v+nl));
}

void free_fvector(v,nl,nh) 
float *v;
int nl,nh;
{
 	nh++;		/* No use. Just to avoid LintPlus warning */
	free((v+nl));
}


void free_cvector(v,nl,nh ) 
char *v;
int nl,nh;
{
 	nh++;		/* No use. Just to avoid LintPlus warning */
	free((v+nl));
}

void free_matrix(m,nrl,nrh,ncl,nch)
float **m;
int nrl,nrh,ncl,nch;
{
	int i;

 	nch++;		/* No use. Just to avoid LintPlus warning */
	for(i=nrh;i>=nrl;i--) free((m[i]+ncl));
	free((m+nrl));
}

void free_dmatrix(m,nrl,nrh,ncl,nch)
double **m;
int nrl,nrh,ncl,nch;
{
	int i;

 	nch++;		/* No use. Just to avoid LintPlus warning */
	for(i=nrh;i>=nrl;i--) free((m[i]+ncl));
	free((m+nrl));
}

void free_imatrix(m,nrl,nrh,ncl,nch)
int **m;
int nrl,nrh,ncl,nch;
{
	int i;

 	nch++;		/* No use. Just to avoid LintPlus warning */
	for(i=nrh;i>=nrl;i--) free((m[i]+ncl));
	free((m+nrl));
}

void free_submatrix(b,nrl,nrh,ncl,nch)
float **b;
int nrl,nrh,ncl,nch;
{
	nrh++;ncl++;nch++; /* No use. Just to avoid LintPlus warning */
	free((b+nrl));
}

void free_fmatrix(m,nrl,nrh,ncl,nch ) 
float **m;
int nrl,nrh,ncl,nch;
{
	int i;

 	nch++;		/* No use. Just to avoid LintPlus warning */
	for (i=nrh;i>=nrl;i--)
		free((m[i]+ncl));
	free((m+nrl));
}

void free_cmatrix(m,nrl,nrh,ncl,nch ) 
char **m;
int nrl,nrh,ncl,nch;
{
	int i;

 	nch++;		/* No use. Just to avoid LintPlus warning */
	for (i=nrh;i>=nrl;i--)
		free((m[i]+ncl));
	free((m+nrl));
}

void free_convert_matrix(b,nrl,nrh,ncl,nch)
float **b;
int nrl,nrh,ncl,nch;
{
	nrh++;ncl++;nch++; /* No use. Just to avoid LintPlus warning */
	free((b+nrl));
}
