/* @(#)oshtest.c	19.1 (ES0-DMD) 02/25/03 13:56:12 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++   
.TYPE           Program
.LANGUAGE       C
.IDENTIFICATION	TESTosf
.AUTHOR         Francois Ochsenbein [ESO-IPG]
.KEYWORDS       Test of osh routines
.ENVIRONMENT    
.COMMENTS       
.VERSION 1.0	23-Oct-1987: Creation
.VERSION 1.1	01-Aug-1990: Added oshproc services
.VERSION 1.2	09-Feb-1993: Removed GNU-nonunderstandable code.(oshostname)CG.

-------------------------------------------------------------------------*/

#include   <osdefos.h>

char *osmsg(), *oshenv(), *oshuser();
long oshtime(), oshgmt();

static char msg[80];
static char c1[80], c2[80];

main()
{ 	int i, l, n, n1, n2;
	long int tim;
	char *p, *r;

  puts("Test of osh (Host Services) Routines");

  while (1)
  {	printf("Operation: ");
	if (gets(msg) == 0)	break;
	switch(msg[0])
	{ case '?': help(); break;
	  case 'l': case 'L':		/* Logical translation	*/
		printf("Logical to translate: "); 	gets(c1);
#		if VMS
		printf("Table to use: "); 		gets(c2);
#		endif
		r = oshenv(c1, c2);
		if (r)	printf("Translation: %s\n", r);
		else 	puts(osmsg());
		break;

	  case 'p': case 'P':		/* Put Logical	*/
		printf("Logical to define: "); 		gets(c1);
		printf("Equivalence: "); 		gets(c2);
		l = oshset(c1, c2);
		if (l)	puts(osmsg());
		break;

	  case 't': case 'T':		/* Time		*/
		printf("Time is now: %d\n", oshtime());
#if 0
		printf("UTime is now: %d\n", oshgmt());
#endif
		break;

	  case 'u': case 'U':		/* UserName	*/
		printf("You should be user `%s'\n", oshuser());
		break;

#if 0
	  case 's': case 'S':		/* Set time	*/
		switch(msg[1])
		{ case 'c': case 'C':
			tim = oshcpu(INIT_CLOCK, NULL_PTR(float)); break;
		  case 'e': case 'E':
			tim = oshela(INIT_CLOCK, NULL_PTR(long)); break;
		  default: puts("***BAD***"); 
		}
		break;

	  case 'g': case 'G':		/* Get time	*/
		switch(msg[1])
		{ case 'c': case 'C':
			tim = oshcpu(GET_CLOCK, NULL_PTR(float)); break;
		  case 'e': case 'E':
			tim = oshela(GET_CLOCK, NULL_PTR(long)); break;
		  default: 
			tim = oshela(-1); break;
		}
		if (tim >= 0)	printf("Time: %d\n", tim);
		else 	puts(osmsg());
		break;
#endif

	  case 'e': case 'E':		/* Execute	*/
		printf("Specify Command: "); 	if (gets(msg) == 0) break;
#if 1
		printf("Specify stdin  : ");   	if (gets(c1) == 0) break;
		printf("Specify stdout : ");   	if (gets(c2) == 0) break;
		i = oshcmd(msg, c1 , c2, (char *)0);
#else
		i = oshexec(msg);
#endif
		puts(osmsg());
		break;

	  default: puts("***BAD***"); 
		break;
	}
  }
}
help()
{
  puts("Possibilities are: ?           Help");
  puts("                   L           translate Logical");
  puts("                   SC|SE       Set time Cpu|Elapsed");
  puts("                   GC|GE       Get Time Cpu|Elapsed");
  puts("                   N           tell Node name");
  puts("                   P           Put Logical Name in Environment");
  puts("                   T           tell Time");
  puts("                   U           tell User name");
  puts("                   E           Execute a command");
  return(0);
}
