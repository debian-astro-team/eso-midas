/* @(#)ospwait.c	19.1 (ES0-DMD) 02/25/03 13:56:13 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        ospwait
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Host operating system interfaces, Simplest Process controls.
.ENVIRONMENT	VAX/VMS Version
.COMMENTS    These routines give the capabilitites to the current process
		to wait unconditionnaly.
.VERSION 1.0 	20-Jan-1988 Creation, F. Ochsenbein
.VERSION 1.1	02-May-1990 Added ospexit
------------------------------------------------------------*/

#include <osdefos.h>

# include <errno.h>	/* System	*/
# include <ssdef.h>	/* System	*/
# define  ENTER()	oserror = 0; vmserror = 0;
# define  EXIT(x)	FIN: if ((!(vmserror & 1))&&(oserror==0))	\
					oserror = EINTR;		\
				return(oserror ? -1 : x);
MID_EXTERN int vmserror;
MID_EXTERN int oserror;

#define FINISH	goto FIN

/*=========================================================================
 		ospwait
 *=========================================================================*/
int ospwait(s)
/*+++
.PURPOSE Wait for a specified number of seconds
.RETURNS 0 (success) / -1 (failure)
.REMARKS 
---*/
	int s;		/* Time to wait in s	*/
{ 
  return(ospwms(s*1000));
}

/*=========================================================================
 		ospuwait
 *=========================================================================*/
int ospuwait(ms)
unsigned int ms;                      /* IN : time delay in milliseconds */

{
  return(ospwms(ms));
}


/*

*/

/*=========================================================================
 		ospwms
 *=========================================================================*/
int ospwms(ms)
/*+++
.PURPOSE Wait for a specified number of milliseconds
.RETURNS 0 / -1
.REMARKS 
---*/
	unsigned int ms;		/* Time to wait in ms	*/
{ 
	MID_STATIC unsigned long int efn = 0;	/* Event flag	*/
	MID_STATIC long int btime[2] = {-1,-1};
	register unsigned int mod;

  ENTER();

	/* If no efn, get one	*/
  if (efn == 0)
  {	LIB$GET_EF(&efn);		/* Get an event flag ...	*/
	SYS$CLREF(efn);			/* ... and clear it       	*/
  }

  vmserror = SS$_NORMAL;

  for (mod = ms; mod != 0; mod /= 180000)	/* Use a loop, (3 mn) */
  {	btime[0] = -10000 * (mod%180000 ? mod%180000 : 180000);
	if (!((vmserror = SYS$SETIMR(efn, btime, 0,0)) &1))	FINISH;
	if (!((vmserror = SYS$WAITFR(efn)) &1))			FINISH;
  }

  EXIT(0);
}

/*=========================================================================*/
int ospexit(status)
/*+++
.PURPOSE Terminate propoerly the Main program
.RETURNS Nothing
.REMARKS 
---*/
     int status;	/* IN: Status 0 for OK / 1 for ABORT / Other */
{
    switch (status) {
    	case 0:  SYS$EXIT(1);
    	case 1:	 SYS$EXIT(SS$_ABORT /* 44 */); 
    	default: SYS$EXIT(status);
    }
    return(0);
}
