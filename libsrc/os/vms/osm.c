/* @(#)osm.c	19.1 (ES0-DMD) 02/25/03 13:56:13 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c)   1988  European Southern Observatory
.LANGUAGE    C
.IDENT       osm.c
.AUTHOR      P.Grosbol  ESO/IPG
.KEYWORDS    dynamic memory allocation
.PURPOSE     obtain or free memory
.COMMENT     just use other routines of F.Ochsenbein
.VERSION     1.0  1988-Nov-29 : Creation,  PJG
----------------------------------------------------------------------*/
int  osmalloc(buf,size)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPUSE     allocate dynamic memory
.RETURN      Number of bytes allocated, otherwise -1
----------------------------------------------------------------------*/
char  **buf;               /* OUT: address of allocated memory        */
int    size;               /* IN:  no. of bytes to allocate           */
{
  char  *osmmget();

  *buf = osmmget(size);
  return (*buf) ? size : -1;
}
int  osmfree(buf)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPUSE     free dynamic memory
.RETURN      status  0:OK, else error
----------------------------------------------------------------------*/
char   *buf;               /* IN: address of memory to free           */
{
  osmmfree(buf);
  return 0;
}
