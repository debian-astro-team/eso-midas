/* @(#)oss.c	19.1 (ES0-DMD) 02/25/03 13:56:14 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        oss
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Host operating system interfaces. Interprocess signals.
.COMMENTS    Dummy version for VMS system.
.VERSION     [1.1] 02-Mar-1990  Implementation. C.Guirao 
------------------------------------------------------------*/

int osssend(pid, sig)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE send a signal to a process
.RETURNS Upon successful completion a value of zero is returned, else 0
         is returned and ``oserror'' is set to the actual error code.
.REMARKS Dummy version.
------------------------------------------------------------*/
int  pid;			/* IN : process identification */
int  sig;  			/* IN : signal (interruption) logical name */
{
	return(0);
}

int ossinhib(sig)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Inhibit the receipt of a signal (Ignore the signal if it 
         comes)
.RETURNS Value 0 on normal return.
.REMARKS Dummy version.
.REMARKS System dependencies:
.REMARKS This routine acts as a declarative statement : it means that
         its effect continues until the end of the calling process or
         until a new call to this routine for the same signal type.
         It is then better located in the beginning of a program.
------------------------------------------------------------*/
int sig;		/* IN : signal identification as in <signal.h> */
{
	return(0);
}

int osscatch(sig, function)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE  Catch a signal and execute a given function when received.
.RETURNS Value 0 on normal return. 
.REMARKS System dependencies:
.REMARKS Dummy version.
.REMARKS This routine acts as a declarative statement : it means that
         its effect continues until the end of the calling process or
         until a new call to this routine for the same signal type.
         It is then better located in the beginning of a program.
------------------------------------------------------------*/
int sig;			/* IN : signal identification (see signal.h) */
int (* function)();             /* IN : address of the function to be */
				/*      executed when signal is caught */
{
	return(0);
}

int osswait(sig,time_out)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Waits for signal to be received.
.RETURNS Value -1 if something wrong happened, 0 is a signal has been
         received before the timeout, 1 if stopped for time out .
.REMARKS System dependencies:
.REMARKS Dummy version.
.REMARKS This routine acts as an executable statement : it means that
         its effect will be to wait for the given signal (!! One can only
	 wait for a signal that it is possible to send (INT, ALRM, TERM, USR1
	 or USR2). The execution
         of the calling process will be interrupted during this wait
         time. The execution will continue if the given signal is received 
         or if the time out period is expired.
         This routine is best used with ``osssend'' to suspend and resume
         programmes execution.
------------------------------------------------------------*/
int sig;		/* IN : signal identification (see signal.h) */
unsigned time_out;	/* IN : amount of seconds allowed for the */
			/*      wait (0 = infinite) */
{
	return(0);
}

int catch() 
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE dummy routine to allow the pause call to return 
.RETURNS always zero
.REMARKS ``sig'' is mandatory parameter always passed to a routine
         called from osscatch.
.REMARKS sets the status variable to one if called.
------------------------------------------------------------------*/
{
	return(0);
}
