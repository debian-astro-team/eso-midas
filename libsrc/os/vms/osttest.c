/* @(#)osttest.c	19.1 (ES0-DMD) 02/25/03 13:56:14 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++   
.TYPE           Program
.LANGUAGE       C
.IDENTIFICATION	ostTEST
.VERSION 1.0	06-Oct-1987: Creation
.AUTHOR         Francois Ochsenbein [ESO-IPG]
.KEYWORDS       Test of ost routines
.ENVIRONMENT    
.COMMENTS       
-------------------*/

#include   <osterm.h>
#include   <atype.h>
#include   <macrogen.h>
#include   <osdefos.h>

static char msg[132];
static char output[] = 
	"Use Interrupt (^C) to stop... Use Interrupt (^C) to stop...";
static char flag = 0;
static struct termstatus tst = {0};

main()
{ long int st, n;
  int i, l, k;
  char c; char *p;
  int ctrlc(), oststop();

  puts("==== Testing OST (Terminal access) routines ====\n");

	/* Get info about the Terminal, and display it */
  if (ostinfo(&tst) < 0)
  	printf("Can't get terminal info: ", osmsg());
  printf("Terminal ");
  if (tst.termname)	printf("%s ", tst.termname);
  printf("Baud=%d, Lines=%d, Cols=%d\r\n", 
  	tst.baud_rate, tst.lines, tst.columns);
  printf("\tControls: INT=^%c, QUIT=^%c\r\n", 
  	tst.cc_INT+'@', 
  	tst.cc_QUIT+'@');


  printf("\nInterrupt char redefined as ^C, Stop as ^Y\n");
  	tst.cc_INT	= Control_('C'),
  	tst.cc_QUIT	= Control_('Y');

  printf("\n\n              Hit <Return> to start");
  if (!gets(msg))	return;

  if (ostopen() < 0)	{ printf("Can't open Terminal: %s\n", osmsg()); return;}
  ostset(&tst);

  while(1)
  { flag = 0;
    ostput("\r\nYour choice: "); 
    k = ostread(msg,1,30);	/* 30s of time-out */
    if (k < 0)	/* There was an error */
    	err();
    if (k == 0)	/* Nothing */
    {	ostput("\07 Are you sleeping ???\r\n");
    	continue;
    }
    ostwrite(msg,k);
    switch(toupper(msg[0]))
    { default: ostput("*** Uknown Command!\r\n");
      case 'H': case '?': help(); continue;

      case 'I':		/* Cancel */
      if (ostint(ctrlc) != 0)	err();    /* `>' changed to `!=' tm 07.09.89 */
	break;

      case 'C':		/* Cancel */      /* `>' changed to `!=' tm 07.09.89 */
	if (ostint(oststop) != 0)	err();
	break;

      case 'R':		/* Input	*/
	ostput("\r\nText [no echo!] : ");
	for (i=0, c = 0; c != '\r'; )
	{	if (i >= sizeof(msg))	break;
		k = ostread(&msg[i], sizeof(msg)-i, 30);
		if (k < 0)	err();
		i += k;
		if (k == 0)	ostput("\07");
		else		c = msg[i-1];
	}
	ostwrite(msg, i);
	ostput("\r\n");
	continue;

      case 'W':		/* Output */
	ostput("\r\n");
	for (i=0;i < 1000 ; i++)	
	{	if (flag)	break;
		sprintf(msg,"[%d] %s [%d]\r\n",i,output,i);
		if (flag)	break;
		ostput(msg);
	}
	continue;

      case 'Q':		/* Quit  */
	ostclose();
	return;

      case 'T':		/* Tell	*/
	if (ostinfo(&tst) < 0)
	  	printf("Can't get terminal info: ", osmsg());
	printf("\r\nTerminal ");
	if (tst.termname)	printf("%s ", tst.termname);
	printf("Baud=%d, Lines=%d, Cols=%d\r\n", 
  	tst.baud_rate, tst.lines, tst.columns);
	printf("\tControls: INT=^%c, QUIT=^%c\r\n", 
  	tst.cc_INT+'@', 
  	tst.cc_QUIT+'@');
    }
  }
  ospexit(0);
}

ctrlc()
{
  int oststop();
  flag = 1;
  ostput("\n\r***INTERRUPT\r\n");
  ostint(oststop);
  return(0);
}

help()
{
  ostput("\r\nPossibilities are  ?     Help");
  ostput("\r\n                   C     Define Interrupt = Cancel");
  ostput("\r\n                   I     Define Interrupt");
  ostput("\r\n                   Q     Quit");
  ostput("\r\n                   R     Read until CR");
  ostput("\r\n                   T     Tell terminal info");
  ostput("\r\n                   W     Write until Interrupt");
  return(0);
}
err()
{    	printf("\n****Error: %s\r\n", osmsg());
    	return;
}
