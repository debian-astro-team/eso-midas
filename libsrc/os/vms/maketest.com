$ ! @(#)maketest.com	19.1 (ESO-IPG) 02/25/03 13:56:11 
$ ver=f$verify(0)
$ set verify
$!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$!.IDENTIFICATION       maketest.COM
$!.LANGUAGE             DCL
$!.AUTHOR               Francois Ochsenbein ESO-IPG
$!.PURPOSE              Compile test programs related to OSLIB
$!.VERSION  3.0		06-Jul-1988
$!.P1                   D for debug version, S for Shareable Version
$!-----------------------------------------------------------------------
$ set nover
$ ON CONTROL_Y then goto FIN
$ ON CONTROL_C then goto FIN
$! 
$!	Definition of Symbols
$!
$ OP = "/INCL=[---.incl]"	! Compiler options
$ IF (P1.eqs."D")	then OP = OP + "/deb/noop"
$ FLIB = "[--]OSLIB"
$ IF (p1.eqs."D")	then FLIB = "[--]OSDEB"
$ OPL = ""
$ IF (P1.eqs."D")	then OPL = "/deb"
$ OPB = "/lib"
$ IF (P1.eqs."S")	then OPB = "/opt"
$!
$!
$!========================================================================
$!		List of Files to Process
$!========================================================================
$!
$!		List of Programs
$!
$ PGMS    = "osttest,osatest,osdtest,osftest,oshtest,osrtest,osutest"
$!
$!=============================================================
$!		Compile Test Programs
$!=============================================================
$!
$ I = 0
$PGM_LOOP:
$ ver=f$verify(0)
$ set verify
$ LPGM   = F$LENGTH(PGMS)
$ if (I .ge. LPGM)	then goto PGM_END
$ LIST   = F$EXTRACT(I, LPGM-I, PGMS)
$ comma  = F$LOCATE(",",LIST)
$ source = F$EXTRACT(0,comma,LIST)
$ I = I + comma + 1
$ tojb = "0"
$ tsource = "9"
$ if (F$SEARCH(source+".obj") .eqs. "") then goto PGM_COMPILE
$ tsource = F$CVTIME(F$FILE_ATTRIBUTES(source+".C","CDT"))
$ tobj    = F$CVTIME(F$FILE_ATTRIBUTES(source+".OBJ","CDT"))
$ if (tobj .gts. tsource) then goto PGM_LINK
$PGM_COMPILE:
$ set verify
$ cc'OP' 'source'.C
$PGM_LINK:
$ set verify
$ LIN'OPL' 'source','FLIB''OPB'
$ ver = f$verify(ver)
$goto PGM_LOOP
$!
$PGM_END:
$ I = 0
$!-------------------------------------------------------------
$FIN:
$ ver = f$verify(ver)
$ EXIT
