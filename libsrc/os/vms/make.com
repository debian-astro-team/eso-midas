$ ! @(#)make.com	19.1 (ESO-IPG) 02/25/03 13:56:10 
$ ver=f$verify(0)
$ set verify
$!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$!.IDENTIFICATION       make.COM
$!.LANGUAGE             DCL
$!.AUTHOR               Francois Ochsenbein ESO-IPG
$!.PURPOSE              Compile and create OSLIB
$!.VERSION  3.0		06-Jul-1988
$!.P1                   D for debug version, S for Shareable Version
$!.P2                   NAME OF FILE(s), or TEST
$!-----------------------------------------------------------------------
$ set nover
$ ON CONTROL_Y then goto FIN
$ ON CONTROL_C then goto FIN
$! 
$!	Definition of Symbols
$!
$ OP = "/INCL=[---.incl]"	! CC options
$ FLIB = "[--]OSLIB"		! Library Name
$ IF (P1.eqs."D")	then OP = OP + "/deb/noop"
$ OPL = ""				! Link Options
$ IF (P1.eqs."D")	then OPL = "/deb"
$ OPB = "/lib"
$ if (F$SEARCH(FLIB+".OLB") .nes. "") then goto LIB_EXISTS
$!
$!	Create the Library
$!
$ set ver
$ LIB/CREATE	'FLIB'
$ set nover
$!
$LIB_EXISTS:
$!
$!========================================================================
$!		List of Files to Process
$!========================================================================
$!
$ MODULES = "osa,osd,osc,osf,osfvms,osfparse,osh,oshproc,osmemory,osmessage,"
$ MODULES = MODULES + "ospwait,osr,ost,osu,iodev,iodev0,iodev1,iodev2"
$!
$ if (P2 .nes. "") then	MODULES = P2
$!
$!========================================================================
$!
$ I = 0
$ MODIF = 0
$!
$!
$MODU_LOOP:
$ ver=f$verify(0)
$ LMODU = F$LENGTH(MODULES)
$ if (I .ge. LMODU)	then goto MODU_END
$ LIST   = F$EXTRACT(I, LMODU-I, MODULES)
$ comma  = F$LOCATE(",",LIST)
$ source = F$EXTRACT(0,comma,LIST)
$ I = I + comma + 1
$ tojb = "0"
$ tsource = "9"
$ if (F$SEARCH(source+".obj") .eqs. "") then goto MODU_COMPILE
$ tsource = F$CVTIME(F$FILE_ATTRIBUTES(source+".C","CDT"))
$ tobj    = F$CVTIME(F$FILE_ATTRIBUTES(source+".OBJ","CDT"))
$ if (tobj .gts. tsource) then goto MODU_LOOP
$MODU_COMPILE:
$ set verify
$ cc'OP' 'source'.C
$ MODIF = MODIF + 1		! At least 1 module was modified...
$ ver = f$verify(ver)
$goto MODU_LOOP
$!
$MODU_END:
$ if (F$LOCATE("TEST",P2) .lt. F$LENGTH(P2))	then goto PGM_LINK
$ set ver
$ LIB 'FLIB' 'MODULES'
$ set nover
$ if (P1 .nes. "S")	then goto DEL_LIS
$!
$! Creation of Shareable Library
$!
$ if (MODIF .eq. 0)	then goto DEL_LIS
$ set ver
$ WRITE SYS$OUTPUT "....Creating the Shareable Library"
$ MAC 'FLIB'/obj='FLIB'
$ LINK/SHARE='FLIB' 'FLIB'.opl/opt
$ @'FLIB'.inst
$ set nover
$ goto DEL_LIS
$!
$!=============================================================
$!		Link Test Program
$!=============================================================
$!
$PGM_LINK:
$ set verify
$ LIN'OPL' 'P2','FLIB''OPB'
$ ver = f$verify(ver)
$ goto DEL_LIS
$!
$!-------------------------------------------------------------
$!
$DEL_LIS:
$ DELETE *.LIS;*/lo
$ I = 0
$!-------------------------------------------------------------
$FIN:
$ ver = f$verify(ver)
$ EXIT
