/* @(#)osrtest.c	19.1 (ES0-DMD) 02/25/03 13:56:14 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++   
.TYPE           Program
.LANGUAGE       C
.IDENTIFICATION	TESTosr
.VERSION 1.0	06-Oct-1987: Creation
.AUTHOR         Francois Ochsenbein [ESO-IPG]
.KEYWORDS       Test of osr routines
.ENVIRONMENT    
.COMMENTS       

-------------------------------------------------------------------------*/

#include   <osremote.h>
#include   <atype.h>
#include   <macrogen.h>

#ifndef NULL
#define NULL NULL_PTR(char)
#endif

extern char *oshenv();

static char msg[132];
static char buf[1000];
static struct remstat info;
static int f = 0;
static int timeout = 1;

main()
{ int s, i;
  char *p, *u;

  puts("==== Testing OSR (Remote Access) routines ====");
  puts("     Type ? for Help");

  while(1)
  { printf("Your choice: "); if (!gets(msg)) break;
    s = 0;
    switch(msg[0])
    { default: puts("*** Uknown Command!");
      case 'H': case '?': help(); continue;

      case 'C': case 'c':	/* Close */
      	s = osrclose(f);	break;
      
      case 'O':	case 'o':	/* Open */
	if (msg[1] != ' ') { puts("Specify Destination!"); break;}
	p = msg + 2, u = NULL;
	if (isalpha(*p))
	{	p = oshenv(p, "PSI$DTE_TABLE");
		if (p)	oscopuc(buf, p, 100, EOS);
		p = buf;
		i = strlen(msg);	msg[i++] = '_';
		msg[i++] = 'U';		msg[i++] = EOS;
		u = oshenv(msg+2, "PSI$DTE_TABLE");
		if (u) 	oscopuc(buf+100, u, 100, EOS);
		u = buf + 100;
	}
	s = osropen(p, NULL, u, NULL);
	if (s >= 0)	f = s;
	break;

      case '@':		/* Select File Number */
        f = atoi(&msg[1]);
	printf("Switching to Circuit %d\n", f);
	break;

      case 'T': case 't':	/* Modify timeout */
        timeout = atoi(&msg[1]);
	printf("Timeout is now %ds.\n", timeout);
	break;
      
      case 'I': case 'i': 	/* Get status */
	s = osrinfo(f, &info);
        printf("Circuit %d: dte=%s, net=%s, device=%s, unit=%d\n",
        	f, info.dte, info.net, info.device, info.unit);
	break;
        continue;
      
      case 'G':	case 'g':	/* Gets n bytes */
      	s = osrgets(f, buf, atoi(msg+1), timeout);
	if (s >= 0)	
	{	printf("Got: ");
		for (p = buf; *p; p++)
		{	if (iscntrl(*p))	printf("^%c", *p+'@');
			else			printf("%c",*p);
		}
		puts("");
	}
        break;
        
      case 'P': case 'p':		/* Write text */
	if (msg[1] == EOS) { printf("Text: ") ; gets(&msg[2]);}
      	s = osrputs(f, &msg[2]);
      	break;
    }
    printf("Status is %d", s);
    if (s < 0) 	printf(": %s",osmsg());
    puts("");
  }
  osrend();
  ospexit(0);
}

help()
{
  printf("Current Circuit is %d, with timeout of %ds.\n", f, timeout);
  puts("Possibilities are: ? Help");
  puts("                   @ f         select Circuit f");
  puts("                   C           Close Current Circuit");
  puts("                   G n         Gets n bytes");
  puts("                   I           Display status of Current Circuit");
  puts("                   O dte       Open Circuit");
  puts("                   P text      Puts text to Remote");
  puts("                   T seconds   Modify Time-out");
  return(0);
}
