/* @(#)osstr.c	19.1 (ES0-DMD) 02/25/03 13:56:14 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        str
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Independant string handling interfaces.
.COMMENTS    
 Handling of character strings. These routines normally return
             a non-negative integer number on successful return.
             Otherwise, a value of -1 is set to indicate an error
             condition and the variable ``oserror'' contains the 
             symbolic error code.

.HISTORY     [0.0] 25-Aug-1986   Definition     J. D. Ponz
             [1.0] 10-Apr-1987   Programmation  B. Pirenne
             [1.1] 04-Aug-1987   VMS Includes
             [1.2] 910911	 bzero removed CG.(memset can be used instead)
------------------------------------------------------------*/
#include <errno.h>

#include  <osparms.h>

int strsglen(str,cha)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Compute the length of the string up to '\0' or the character "cha"
	 (typically a ' ').
.RETURNS Upon successful completion a positive or zero integer number is
returned.
.REMARKS System dependencies:
 -- UNIX:  none 
------------------------------------------------------------*/

char *str;	/* IN : string to check */
char cha;	/* IN : open mode */

{
   register i; 

   for (i=0;*(str+i) != '\0' && *(str+i) != cha;i++);
   *(str+i) = '\0';
   return(i);
}

strtolower(s1,s2)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
  translate the s2 character string to lower case and store the 
  result in s1.
.RETURNS The number of characters actually translated.

---------------------------------------------------------------*/

char *s1; /* OUT: ouptput string */
char *s2; /* IN : input string   */

{

register i,c;

  c = 0;
  for (i=0;*(s2+i) != '\0';i++) {
     if (*(s2+i) >= 'A' && *(s2+i) <= 'Z')
       {
	*(s1+i) = *(s2+i) + 32;
	c++;
       }
      else
	*(s1+i) = *(s2+i);
  }
  return(c);
}


strgetline(buf)
/*+++++++++++++++++++++++++++++ getline +++++++++++++++++++++++++
.PURPOSE get one record from standard input.
.RETURNS number of characters fetched.
-----------------------------------------------------------------*/
char *buf; /* buffer to contain the string read */
{
   int      i;
   char     c;

   for(i=0;(c=getchar()) != '\n';i++) *(buf+i) = c;
   *(buf+i+1) = '\0';
   return(i);
}


truelen(string)
/*++++++++++++++++++++++ truelen ++++++++++++++++++++++++++++++++
.PURPOSE Compute the 'true length' of a string of character by checking for
	 all valid characters. Valid characters taken into account are :
	 a..z, A..Z, ., *, -, _, 0..9.
.RETURNS The number of contiguous characters in the string satisfying the 
	 above condition.
------------------------------------------------------------------*/
char   *string;  /* The character string to check */
{
     register   i,c;

     for (i = 0; (c = *(string + i)) == '.' || c == '-' || c == '_' ||
		  c >= 'a' && c <= 'z'     ||
		  c >= 'A' && c <= 'Z'     ||
		  c >= '0' && c <= '9'      ;
		  i++)
     ;
     *(string+i) = '\0';
     return(i);
}

int strin(s1,s2)
/*+++++++++++++++++++++++++++++ strin ++++++++++++++++++++++++++++++++++++++
.PURPOSE  Pattern matching
.RETURNS  0 if pattern is not in string, 1 if found.
-------------------------------------------------------------------------*/

   char  *s1              /* IN : string to search */;
   char  *s2              /* IN : pattern to find */;
   
{
   register char  *t1,*t2;
   int            found;
   
   found = 0;
   t2 = s2;
   t1 = s1;
   for (;*t1 != '\0' && *t2 != '\0' && !found;t1++) {
     if (*t1 == *t2)
       t2++;
       else {
         if (*t2 != *s2)
           {
             t1 -= t2 - s2;
	     t2 =  s2; 
	    }
         }
   }
   if (*t2 == '\0')  found = 1;
   return(found);
}

char * strstrs(s1,s2)
/*+++++++++++++++++++++++++++++ strstrs +++++++++++++++++++++++++++++++++++
.PURPOSE  Pattern matching
.RETURNS  returns a pointer to the first occurrence of the pattern string s2
          in s1. Otherwise returns NULL
-------------------------------------------------------------------------*/
   char  *s1              /* IN : string to search */;
   char  *s2              /* IN : pattern to find */;
   
{
   register char  *t1,*t2;
   char            *ptr;
   
   ptr = (char *)0;
   t2 = s2;
   t1 = s1;
   for (;*t1 != '\0' && *t2 != '\0' ;t1++) {
     if (*t1 == *t2) t2++;
     else {
       if (*t2 != *s2) {
         t1 -= t2 - s2;
	 t2 =  s2; 
       }
     }
   }
   if (*t2 == '\0')  ptr = t1 - strlen(s2);
   return(ptr);
}
