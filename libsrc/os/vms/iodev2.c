/* @(#)iodev2.c	19.1 (ES0-DMD) 02/25/03 13:56:10 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        	Module
.NAME        	iodev2
.LANGUAGE    	C
.AUTHOR      	IPG-ESO Garching
.CATEGORY    	Host operating system interfaces. TBD
.COMMENTS    	============= TEMPLATE ==============
.VERSION 0.0	12-Oct-1989: Design, F. Ochsenbein, C. Guirao
.ENVIRONMENT 	UNIX
------------------------------------------------------------*/
static char class_name[] = "template";	

#include <osdefos.h>
#include <osudef.h>	

MID_EXTERN int oserror;
MID_EXTERN char *oserrmsg;

/*=====================================================================
 * 		Definition of the structure returned to osu
 *=====================================================================*/

/* struct iolist *iodev3(); */

	/* In the following list, zero'ed functions are replaced by inherited 
	   from class "dumb"
	 */

static OPITEM list_of_functions[] = {
	{ U_INFO,	(int (*)())0}
/*	{ U_OPEN,	ioopen},			*/
/*	{ U_CLOSE,  	ioclose},			*/
/*	{ U_READ,	ioread},			*/
/*	{ U_WRITE,	iowrite},			*/
/*	{ U_REWIND, 	iorew},				*/
/*	{ U_WEOF,	ioweof},			*/
/*	{ U_FMF,	iofsf},	/* File Move Forward  	*/
/*	{ U_FMB,	iobsf},	/* File Move Backward 	*/
/*	{ U_BMF,	iofsr},	/* Block Move Forward 	*/
/*	{ U_BMB,	iobsr}	/* Block Move Backward 	*/
 };

static struct iolist this_dev = {
	(IODEV)0,		/* Next iodev in List */
	class_name,		/* How it's written in DEVCAPFILE */
	sizeof(list_of_functions)/sizeof(list_of_functions[0]),
	list_of_functions
 };
 
struct iolist *iodev2()
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Get All definitions concerning this class of Devices
.RETURNS The iolist
.REMARKS Just a template here: inherits zero'ed functions.
------------------------------------------------------------*/
{
	OPITEM	*pop;
	int	i;

  for (pop = list_of_functions, i = this_dev.nop; --i >= 0; pop++)
   	if (!pop->opf)	osugrep("dumb", pop);
   
  return(&this_dev);
}
