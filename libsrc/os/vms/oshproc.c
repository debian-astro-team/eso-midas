/* @(#)oshproc.c	19.1 (ES0-DMD) 02/25/03 13:56:12 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        oshproc
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Host operating system interfaces. Host services.
.ENVIRONMENT	VAX/VMS Version
.COMMENTS    
	Provide process host services. 
	These routines return process information.
	The routines return always
	a non-negative integer number on successful return.
	Otherwise, a value of -1 is set to indicate an error
	condition and the variable ``oserror'' contains the 
	symbolic error code.
.VERSION  0.0 	25-Aug-1986   Definition.       J. D. Ponz
.VERSION  1.0 	13-Jan-1989   Programmation.    B.    Pirenne (ST--ECF) 
					      Fr. Ochsenbein (ESO - IPG)
.VERSION  1.1 	31-Aug-1990   Added oshost (Host Name) F.O.
.VERSION 1.2	17-Oct-1990: Removed GNU-nonunderstandable code...
.VERSION 1.3	09-Feb-1993: Removed GNU-nonunderstandable code.(oshostname)CG.
------------------------------------------------------------*/

#define DEBUG	0

#include <osdefos.h>
#include <macrogen.h>
#ifdef EOF
#undef EOF
#endif

MID_EXTERN int oserror;
MID_EXTERN int vmserror;

#include <errno.h>	/* System	*/
#include <ssdef.h>	/* System	*/
#include <jpidef.h>	/* System	*/
#include <syidef.h>	/* System	*/

 	typedef struct {
	unsigned short len;
	unsigned short item_code;
	char  *buffer;
	long  *return_length;
  	} ITMLST;


int oshpid()
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   Get process number of current process.
.RETURNS   Process id.
.REMARK    Only for VMS.
-------------------------------------------------------------------- */
{
        static int PID;

	static struct {
	   ITMLST itm_1;
	   long   itm_end;
	} items = {
	{ sizeof(int), JPI$_PID, (char *)&PID, (long *)0}, /* PID NUMBER  */
		0					/* end of items list*/
	 };

	vmserror = sys$getjpi(0,0,0,&items,0,0,0);
	if (vmserror & 1) ;
	else {
	   PID = -1;
	   oserror = EVMSERR;
	}
	return(PID);
}

char *oshprn()
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   Get process name of current process.
.RETURNS   Process name string.
.REMARK    Only for VMS.
-------------------------------------------------------------------- */
{
        static	char PRN[16];
        static	long PRN_len;

	static struct {
	   ITMLST itm_2;
	   long   itm_end;
	} items = {
	{ sizeof(PRN)-1, JPI$_PRCNAM, PRN, &PRN_len},  /* Proc name */
		0				/* end of items list*/
	 };

	vmserror = sys$getjpi(0,0,0,&items,0,0,0);
	if (vmserror & 1) 
	   PRN[PRN_len] = EOS;
	 else {
	   PRN[0] = EOS;
	   oserror = EVMSERR;
	 }
	return(PRN);
}


char *oshuser()
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   Get name of user who logged in.
.RETURNS   User's name string
.REMARK    
-------------------------------------------------------------------- */
{
        static	char username[24];
        static	long username_len;

	static struct {
	   ITMLST itm_2;
	   long   itm_end;
	} items = {
	   { sizeof(username)-1, JPI$_USERNAME, username, &username_len},
		0	/* end of items list*/
	};

	vmserror = sys$getjpi(0,0,0,&items,0,0,0);
	if (vmserror & 1) {	/* Remove useless blanks... */
		while (username_len > 0) 
			if (username[--username_len] != ' ')	break;
	   	username[++username_len] = EOS;
	}
	else {
	   username[0] = EOS;
	   oserror = EVMSERR;
	}
	return(username);
}
