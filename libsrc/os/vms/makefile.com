$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.LIBSRC.OS.VMS]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:57 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libos osa.obj,osd.obj,osc.obj,osf.obj,osfvms.obj,osfparse.obj,osh.obj,oshproc.obj,osmemory.obj,osmessage.obj,osx.obj,ospsystem.obj,ospwait.obj,ost.obj,osu.obj,osm.obj,oss.obj,osl.obj,osstr.obj,iodev.obj,iodev0.obj,iodev1.obj,iodev2.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
