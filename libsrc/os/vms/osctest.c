/* @(#)osctest.c	19.1 (ES0-DMD) 02/25/03 13:56:11 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++   
.TYPE           Program
.LANGUAGE       C
.IDENTIFICATION	osctest.c
.VERSION 1.0	23-Oct-1987: Creation
.AUTHOR         Francois Ochsenbein [ESO-IPG]
.KEYWORDS       Test of osc routines
.ENVIRONMENT    
.COMMENTS       

-------------------------------------------------------------------------*/

#include   <atype.h>

static char msg[80];
static char s1[500], s2[500];
static unsigned char table[256] =
       {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
	20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
	41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,
	'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r',
	's','t','u','v','w','x','y','z',0133,0134,0135,0136,0137,0140,
	'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R',
	'S','T','U','V','W','X','Y','Z',0173,0174,0175,0176,0177};

char *s();

main()
{ 	int i, n;
	char *p;

  puts("Test of osc (Char Strings) Routines");

  while (1)
  {	
  	printf("Operation: ");
	if (!gets(msg))	break;
	switch(msg[0])
	
	{ case '?': help(); break;
	  case 'f': case 'F':		/* Fill strings		*/
	  	p = s(msg+1);
	  	if (p) { printf("Length: "); gets(msg); n = atoi(msg);
	  		 if (n >= sizeof(s1)-1) n = sizeof(s1)-1;
	  		 printf("Fill char: "); gets(msg);
	  		 *(p + oscfill(p, n, msg[0])) = 0;
	  		}
		break;
	  case 'u': case 'U':		/* Copy Until char	*/
	  	n = oscopuc(s2, s1, strlen(s1)+1, msg[2]);
		*(s2+n) = '\0';
	  	printf("Returned [%d]: %s\n", n, s2);
		break;
	  case 'g': case 'G':		/* Get strings		*/
	  	p = s(msg+1);
	  	if (p) { printf("String: "); gets(p); break;}
	  case 'd': case 'D':		/* Display strings	*/
	  	p = s(msg+1);
	  	if (p) 	puts(p);
	  	break;
	  case 'm': case 'M':		/* Copy char 	*/
		printf("Copied s1 to s2 =>%d\n",
			oscopy(s2, s1, strlen(s1)+1)); 
		break;
	  case 'l': case 'L':		/* Locate char 	*/
		printf("Locate `%c' in s1 =>%d\n", msg[2],
			oscloc(s1, strlen(s1), msg[2])); 
		break;
	  case 'k': case 'K':		/* Skip char	*/
		printf("Locate `%c' in s1 =>%d\n", msg[2],
			oscskip(s1, strlen(s1), msg[2])); 
		break;
	  case 's': case 'S':		/* Scan char	*/
		n = atoi(&msg[2]);
		printf("Scan `%2X' in s1 =>%d\n", n,
			oscscan(s1, strlen(s1)+1, n, main_ascii)); 
		break;
	  case 'p': case 'P':		/* Span char	*/
		n = atoi(&msg[2]);
		printf("Span `%2X' in s1 =>%d\n", n,
			oscspan(s1, strlen(s1)+1, n, main_ascii)); 
		break;
	  case 'b': case 'B':		/* Backwards 	*/
	    switch(msg[1]){
	    case 'l': case 'L':		/* Locate char 	*/
		printf("Locate `%c' in s1 =>%d\n", msg[3],
			oscbloc(s1, strlen(s1), msg[3])); 
		break;
	    case 'k': case 'K':		/* Skip char	*/
		printf("Locate `%c' in s1 =>%d\n", msg[3],
			oscbskip(s1, strlen(s1), msg[3])); 
		break;
	    case 's': case 'S':		/* Scan char	*/
		n = atoi(&msg[3]);
		printf("Scan `%2X' in s1 =>%d\n", n,
			oscbscan(s1, strlen(s1)+1, n, main_ascii)); 
		break;
	    case 'p': case 'P':		/* Span char	*/
		n = atoi(&msg[3]);
		printf("Span `%2X' in s1 =>%d\n", n,
			oscbspan(s1, strlen(s1)+1, n, main_ascii)); 
		break;
	    default: puts("***BAD***"); 
		} break;
	  case 't': case 'T':		/* Translate s1 -> s2	*/
		printf("Translate =>%d\n", osctr(s2, s1, strlen(s1)+1, table));
		break;

	  case 'i': case 'I':		/* Index s2 in s1	*/
		printf("Index =>%d\n", oscindex(s1, strlen(s1), 
			s2, strlen(s2)));
		break;
	  case 'c': case 'C':		/* Compare s1 / s2	*/
		if (msg[1])	i = osccomp(s1, s2, strlen(s1)+1);
		else		i = oscomp (s1, s2, strlen(s1)+1);
		printf("Compare =>%d\n", i);
		break;

	  default: puts("***BAD***"); 
		break;
	}
  }
  ospexit(0);
}
help()
{
  puts("Possibilities are: ?           Help");
  puts("                   C           Compare s1 to s2");
  puts("                   CC          Compare case insensitive s1 to s2");
  puts("                   D1|D2       Display s1|s2");
  puts("                   G1|G2       Get s1|s2");
  puts("                   M           Copy s1 -> s2");
  puts("                   F1|F2       Fill s1|s2");
  puts("                   L c         Locate char c in s1");
  puts("                   BL c        Locate Backwards c in s1");
  puts("                   K c         Skip char c in s1");
  puts("                   BK c        Skip Backwards char c in s1");
  puts("                   P m         Span mask m in s1");
  puts("                   BP c        Span Backwards mask m in s1");
  puts("                   S m         Scan mask m in s1");
  puts("                   BS c        Scan Backwards mask m in s1");
  puts("                   T           Translate s1 into s2");
  puts("                   U c         Copy s1 -> s2 Until char c");
  puts("                   I           Index of s2 in s1");
  puts("Masks:  1=U,  2=L, 4=D,  8=S,  16=P,  32=C,  64=X");
  return(0);
}

char *s(str)	/* Returns s1 or s2 or 0 */
	char *str;
{
  int i; 
  
  i = atoi(str);
  switch(i)
  { case 1: return(s1);
    case 2: return(s2);
    default: return(0);
  }
}
