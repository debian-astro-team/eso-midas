/* @(#)osp.c	19.1 (ES0-DMD) 02/25/03 13:56:13 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++
.IDENTIFICATION osp.c
.LANGUAGE       C
.AUTHOR         ESO-IPG, Garching
.ENVIRONMENT    VAX / VMS
.KEYWORDS       Ending a program
.VERSION  1.0   16-Oct-1990
.COMMENTS       Just an implementation of ospexit
---------------*/
int ospexit(status)
/*++++++++++++++++
.PURPOSE  Convert Unix status to VMS
.RETURNS  to OS
-----------------*/
     int status;
{
  switch (status)
    {
    case 0:
      exit(1);
    case 1: case -1:
      exit(8516);  /* SS$_FORCEDERROR defined in SSDEF.H */
    default:
      exit(status);
    }
}
