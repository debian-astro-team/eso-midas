/* @(#)osmessage.c	19.1 (ES0-DMD) 02/25/03 13:56:13 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.TYPE		Module
.IDENTIFICATION	osmessage
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.VERSION 1.0	08-Oct-1987: Creation
.VERSION 1.1	19-Jan-1988: Added external pointer to be used
		as message when oserror = -1.
.KEYWORDS	System error messages
.ENVIRONMENT	VAX/VMS Version
.COMMENTS	The ``message'' list can be generated with
		a program listed below as a comment. The following externals
		are defined here:
\begin{TeX}
\begin{itemize}
\item oserror ({\em int}), system-independant error number
\item vmserror ({\em int}), VMS error code (on VMS only)
\item oserrmsg ({\em char *}), pointer to a text string to be used if
		oserror has the value $-1$.
\end{itemize}
\end{TeX}

----------------------------------------------------------------------------*/

#include <osdefos.h>
#include <macrogen.h>

#define	 dsc$w_length	len
#define	 dsc$a_pointer	s
#include <descrip.h>	/* System	*/
MID_GLOBAL int  vmserror = 0;	/* VMS-specific error code */
#define	LINESIZE	120	/* Maximum size of message 	*/

	MID_STATIC char msg_s[LINESIZE] = "?";
	static short int msglen;		/* Returned length from SYS$GETMSG */
	MID_STATIC struct dsc$descriptor_s msg = 
	{ sizeof(msg_s)-1, DSC$K_DTYPE_T, DSC$K_CLASS_S, msg_s };
	MID_RSTATIC char *sys_errlist[] = { "?"
	,"not owner"                                    /* EPERM */
	,"no such file or directory"                    /* ENOENT */
	,"no such process"                              /* ESRCH */
	,"interrupted system call"                      /* EINTR */
	,"i/o error"                                    /* EIO */
	,"no such device or address"                    /* ENXIO */
	,"arg list too long"                            /* E2BIG */
	,"exec format error"                            /* ENOEXEC */
	,"bad file number"                              /* EBADF */
	,"no children"                                  /* ECHILD */
	,"no more processes"                            /* EAGAIN */
	,"not enough core"                              /* ENOMEM */
	,"permission denied"                            /* EACCES */
	,"bad address"                                  /* EFAULT */
	,"block device required"                        /* ENOTBLK */
	,"mount device busy"                            /* EBUSY */
	,"file exists"                                  /* EEXIST */
	,"cross-device link"                            /* EXDEV */
	,"no such device"                               /* ENODEV */
	,"not a directory"                              /* ENOTDIR */
	,"is a directory"                               /* EISDIR */
	,"invalid argument"                             /* EINVAL */
	,"file table overflow"                          /* ENFILE */
	,"too many open files"                          /* EMFILE */
	,"not a typewriter"                             /* ENOTTY */
	,"text file busy"                               /* ETXTBSY */
	,"file too large"                               /* EFBIG */
	,"no space left on device"                      /* ENOSPC */
	,"illegal seek"                                 /* ESPIPE */
	,"read-only file system"                        /* EROFS */
	,"too many links"                               /* EMLINK */
	,"broken pipe"                                  /* EPIPE */
	,"math argument"                                /* EDOM */
	,"result too large"                             /* ERANGE */
	,"I/O operation would block channel"            /* EWOULDBLOCK */
	};
	MID_STATIC int    sys_nerr = ITEMS(sys_errlist);

MID_GLOBAL int  oserror = 0;			/* System error code	*/
MID_GLOBAL char *oserrmsg = NULL_PTR(char);	/* If oserror == -1	*/

/*==========================================================================*/
char *osmsg()
/*+++
.PURPOSE Provide a text explaining the error.
.RETURNS Pointer to the relevant message. VMS messages are included.
---*/
{ 
	register char *p;
	register char *q;

  if (oserror < 0 )	p = oserrmsg;
  else 
  {  	if (oserror >= sys_nerr)	oserror = 0;
	p = sys_errlist[oserror];

	for (q = msg_s; *p != EOS; p++, q++)	*q = *p;
	*(q++) = ' ';
	msg.s  = q;
	msg.len= sizeof(msg_s) - 3 - (q-msg_s);
	if (vmserror)	SYS$GETMSG(vmserror,&msglen,&msg,15,0);
	else		msglen = 0;
	q += msglen;
	*(q++) = ':';
	*(q++) = ' ';
	*q = EOS;
	p  = msg_s;
  }
  
  return(p);
}


/* ------------- The following is the program to retrieve error messages */
#if 0
\begin{TeX}
\begin{verbatim}
#include <osparms.h>
#include <atype.h>

 main()
 { 
	int fin, fout, n, n0, i, j, i0;
	static char line[200];

#if MID_OS_VMS
  fin = osdopen("SYS$LIBRARY:errno.h", READ);
#else
  fin = osdopen("/usr/include/errno.h", READ);
#endif
  fout = osdopen("errno.lis", WRITE);
  if (fout < 0)	printf("****Bad Output File***");
  n0 = 1;
  osdputs(fout, "static char *sys_errlist[] = { \"?\"");

  while (osdgets(fin, line, sizeof(line)) > 0)
  {	if (line[0] != '#')	continue;
  	if (line[1] != 'd')	continue;
	i = 2;
	while(isgraph(line[i]))	i++;
	while(isspace(line[i]))	i++;
	i0 = i;
	while(isgraph(line[i]))	i++;
	while(isspace(line[i]))	i++;
	n = atoi(&line[i]);
	if (n != n0)	{ printf("Bad number in: %s\n", line);
			  continue;
			}
	n0++;
	while(line[i] != '/')	i++;
	i++;
	i++;
	while(isspace(line[i]))	i++;
	j = strlen(line) - 3;
	while(isspace(line[j]))	j--;
	line[--i] = '"';
	line[--i] = ',';
	line[--i] = '\t';
#	if MID_OS_VMS
#	else
	line[++j] = ':';	
	line[++j] = ' ';	
#	endif
	line[++j] = '"';	
	while((j-i) < 48)	line[++j] = ' ';
	line[++j] = '/';
	line[++j] = '*';
	line[++j] = ' ';	
	while(isgraph(line[i0]))	line[++j] = line[i0++];
	line[++j] = ' ';	
	line[++j] = '*';
	line[++j] = '/';
	line[++j] = EOS;	
	osdputs(fout, &line[i]);
  }

  osdputs(fout, "\t};");
  osdclose(fout);
  ospexit(0);
 }
#endif
