/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.TYPE		Module
.IDENTIFICATION	oserror.c
.AUTHOR    	Carlos Guirao [ESO-IPG]
.LANGUAGE  	C
.VERSION 1.1	09-Jul-1992: Creation
.KEYWORDS	System error messages
.ENVIRONMENT	UNIX 
\begin{TeX}
\begin{itemize}
\item oserror ({\em int}), system-independant error number
\item oserrmsg ({\em char *}), pointer to a text string to be used if
		oserror has the value $-1$.
\end{itemize}
\end{TeX}
.VERSION
 
 090324		last modif
----------------------------------------------------------------------------*/

#include <fcntl.h>
#include <osdefos.h>
#include <macrogen.h>
#include <proto_os.h>		/* ANSI-C prototyping */

#ifdef OSERROR_D	/* oserror is already defined (Silicon Graphics) */
#define oserror midaserror
#endif 

int  oserror = 0;			/* System error code	*/
char *oserrmsg = NULL_PTR(char);		/* If oserror == -1	*/
