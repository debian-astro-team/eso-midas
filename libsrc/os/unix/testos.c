/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* this code just checks, that all os routines are there and can be linked... */


int main()
{

void osscatch(), osssend(), osswait();
void oshcmd(), oshcpu(), oshdate(), oshmsg(), oshtime(), oshtl(), oshtm();
void oshchdir(), oshmidvers();
void osaclose(), osaopen(), osaread(), osaseek(), osasize(), osaunix(), osawait(), osawrite();
void osdclose(), osdopen(), osdputs(), osdread(), osdseek(), osdwrite();
void osfcontrol(), osfcreate(), osfdelete(), osfinfo(), osflgname(), osfmkdir(), osfphname(), osfrename();
void ospcreate(), ospexit(), ospwait(), osppid();
void ospuwait();
void osxclose(), osxinfo(), osxopen(), osxread(), osxwrite();
void osubsize(), osuclose(), osufclose(), osufseek(), osuftell(), osuopen(), osuread(), osustatus(), osuwrite();
void oscbloc(), oscbscan(), oscbskip(), oscbspan(), osccomp(), oscfill(), oscindex(), oscloc(), oscomp(), oscopuc(), oscopy(), oscscan(), oscskip(), oscspan(), osctr();
void osmsg(), osmmexp(), osmmfree(), osmmget();
void oshenv(), oshpid();
void ostclose(), ostin(), ostinfo(), ostint(), ostopen(), ostraw(), ostread(), ostset(), ostwinch(), ostwrite();
void osfdate(), osfop(), osfsize(), osfunix();
void osfparse(), osfsupply(), osftr();
void strgetline(), strin(), strsglen(), strtolower(), truelen();




/* oss.o: */
osscatch();
osssend();
osswait();

/* osh.o: */
oshcmd();
oshcpu();
oshdate();
oshmsg();
oshtime();
oshtl();
oshtm();
oshchdir();
oshmidvers();

/* osa.o: */
osaclose();
osaopen();
osaread();
osaseek();
osasize();
osaunix();
osawait();
osawrite();

/* osd.o: */
osdclose();
osdopen();
osdputs();
osdread();
osdseek();
osdwrite();

/* osf.o: */
osfcontrol();
osfcreate();
osfdelete();
osfinfo();
osflgname();
osfmkdir();
osfphname();
osfrename();

/* osp.o: */
ospcreate();
ospexit();
ospwait();
osppid();

/* ospuwait.o: */
ospuwait();

/* osx.o: */
osxclose();
osxinfo();
osxopen();
osxread();
osxwrite();

/* osu.o: */
osubsize();
osuclose();
osufclose();
osufseek();
osuftell();
osuopen();
osuread();
osustatus();
osuwrite();

/* osc.o: */
oscbloc();
oscbscan();
oscbskip();
oscbspan();
osccomp();
oscfill();
oscindex();
oscloc();
oscomp();
oscopuc();
oscopy();
oscscan();
oscskip();
oscspan();
osctr();

/* ost.o: */
ostclose();
ostin();
ostinfo();
ostint();
ostopen();
ostraw();
ostread();
ostset();
ostwinch();
ostwrite();

/* osmessage.o: */
osmsg();

/* osmemory.o: */
osmmexp();
osmmfree();
osmmget();

/* oshenv.o: */
oshenv();
oshpid();

/* osfdate.o: */
osfdate();
osfop();
osfsize();
osfunix();

/* osfparse.o: */
osfparse();
osfsupply();
osftr();

/* osstr.o: */
strgetline();
strin();
strsglen();
strtolower();
truelen();

return 0;
}
