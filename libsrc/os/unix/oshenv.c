/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        oshenv.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Host operating system interfaces. Host services.
.ENVIRONMENT	UNIX (BSD / SYSV)
.COMMENTS    
	Provide miscellaneous host services. 
	There are routines translating logicals (environment),
	getting current process and login info.
.VERSION  0.0 25-Aug-1986   Definition.       J. D. Ponz
.VERSION  1.0 28-Oct-1986   Programmation.    B.    Pirenne
.VERSION  1.5 14-Apr-1987   bsd-sysv compatible.  B.    Pirenne
.VERSION  1.6 15-Jul-1987   oshcpu returns a real number; date through a
	          structure. B. Pirenne
.VERSION  1.7 10-Dec-1987   Cosmetic changes    I. Suisalu
.VERSION  2.0 08-Feb-1989   putenv() does not exist in pure BSD. CGS
.VERSION  2.1 06-Mar-1990   Added oshpid oshuser oshprn (login infos)
.VERSION  2.2 23-May-1990   Corrected bug in BSD version FO
.VERSION  2.3 01-Aug-1990   Added oshostname routine (FO)
.VERSION  2.4 910911	    oshset removed because of putenv. CG
.VERSION  2.5 920120	    lint: table argument in oshenv removed. CG.
.VERSION  2.6 930209	    ohsostname routine removed. CG.

 090324		last modif
------------------------------------------------------------*/

#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <osparms.h>
#include <proto_os.h>		/* ANSI-C prototyping */

#define DEBUG	0


	char *getenv();

#define FINISH		goto FIN

/*==========================================================================
 *		Logical Name Translations
 *==========================================================================*/

/*==========================================================================*/
/* ARGSUSED */
char *oshenv(logname,table)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
    Translate logical names into physical names., according to a table.
.METHOD
    Searches the system variable list for the logname.
.RETURNS Address of equivalence name / NULL when logname not found 
.REMARKS table is used only in VMS environment (default is LNM$FILE_DEV).
	Translation is not case-dependent on VMS. Use recursive translation.
------------------------------------------------------------*/
char *logname;	/* IN : logical name */
char *table;	/* IN : name of the table to look for (VMS only) */
{
  register char *p;

  oserror = 0;

	/* 1. Skip the eventual trailing $ 	*/

  p = logname;
  if (*p == '$')	p++;

	/* 2. Do the logical translation 	*/

  p = getenv(p);
  if (!p)		oserror = errno;

  return(p);
}

int oshpid()
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE   Get process number of current process.
.RETURNS   Process id.
.REMARK    Only for VMS.
-------------------------------------------------------------------- */
{
    return(getpid());
}
