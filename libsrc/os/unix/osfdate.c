/*===========================================================================
  Copyright (C) 1988-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        osfdate.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Host operating system interfaces. File management.
.ENVIRONMENT	UNIX (BSD / SYSV)
.COMMENTS    
	 Date of last modification of a file.
.VERSION     1.0 04-Oct-1988   F. Ochsenbein

 090324		last modif
-----------------------------------------------*/

#define DEBUG		0 	/* Set to 1 for Debugging Option	*/

#include <osdefos.h>		/* OS Midas definitions */
#include <errno.h>		/* System error definitions */
#include <oserror.h>		/* Midas error definitions */
#include <unistd.h>		/* Midas error definitions */
#include <sys/types.h>
#include <sys/stat.h>
#include <proto_os.h>		/* ANSI-C prototyping */

#ifndef F_OK
#define F_OK	0
#endif

#define RETURN(x)	FIN: return(oserror ? -1 : x)
#define	FINISH		goto FIN

static struct stat	buf;


/*======================================================================*/
/* ARGSUSED */
#ifdef __STDC__
int osfop(char fmt, int length)
#else
int osfop(fmt, length)
/*++++++++
.PURPOSE Initializes the Format for the next Open / Create
.RETURNS 0
.REMARKS Only for VMS (useless in Unix context)
------------------------------------------------------------*/
char fmt;		/* IN : 'f' for Fixed, 'v' for Variable */
int  length;		/* IN : Length of longest record	*/
#endif
{
  return(0);
}

/*======================================================================*/
int osfunix(/*filename*/)
/*++++++++
.PURPOSE Tells if a file is Unix--compatible
.RETURNS 1 (always valid)
.REMARKS Only for VMS compatibility
------------------------------------------------------------*/
/*char *filename;*/		/* IN : File to Check */
{
  return(1);
}

/*==========================================================================*/
long int osfdate(phname)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Retrieve last modification date of a file.
.RETURNS The date in seconds
.REMARKS 
------------------------------------------------------------*/
	char *phname;  			/* IN : physical filename */
{
  oserror = 0;

  if (access(phname,F_OK) != 0) 	{ oserror = ENOENT; FINISH; }
				/* Check for accessibility of file first  */

  if (stat(phname,&buf) != 0)		{ oserror = errno; FINISH; }

  FIN:
#if DEBUG
  printf("Date of file %s = %d(oserror=%d)\n", phname, buf.st_mtime,oserror);
#endif 
  return(oserror ? -1L : buf.st_mtime);
}

/*==========================================================================*/
long int osfsize(phname)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Get the size of a file.
.RETURNS Size of file in bytes (-1 if file does not exist).
.REMARKS 
------------------------------------------------------------*/
	char *phname;  			/* IN : physical filename */
{
	
  oserror = 0;

  if (access(phname,F_OK) != 0) 	{ oserror = ENOENT; FINISH; }
				/* Check for accessibility of file first  */

  if (stat(phname,&buf) != 0)		{ oserror = errno; FINISH; }

  FIN:
  return(oserror ? -1L : buf.st_size);
}

