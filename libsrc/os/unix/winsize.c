/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++
.TYPE		Module
.IDENTIFICATION winsize
.AUTHOR    	Carlos Guirao
.LANGUAGE  	C
.KEYWORDS	Terminal i/o
.ENVIRONMENT	Unix (Non-Posix compliant)
.COMMENTS	Used by "ost" interface to get the window size.
.VERSION 1.0	05-Apr-1993:	Creation

 090324		last modif
----------------------------------------------------------------------------*/

#include <fcntl.h>
#include <proto_os.h>		/* ANSI-C prototyping */
#include <termios.h>
#ifndef TIOCGWINSZ
#include <sys/ioctl.h>
#endif

#ifdef PC_SCO			/* PC/SCO needs the following incl. */
#       include <sys/types.h>
#       include <sys/stream.h>
#       include <sys/ptem.h>
#endif


void winsize(fd,col,row)
/*++++++++++++
.PURPOSE Get the window size in columns and rows.
.RETURNS void
.REMARKS If window size unknown, col and row are set to 0.
-------------*/
int fd;		/* IN: File descriptor */
unsigned short *col;	/* OUT: Number of columns */
unsigned short *row;	/* OUT: Number of rows */
{
#ifdef TIOCGWINSZ       /* This ioctl seems to exist only in BSD */
  struct   winsize tw;
#endif

  *row = 0;
  *col = 0;
#ifdef TIOCGWINSZ
  if (ioctl(fd,TIOCGWINSZ,&tw) == -1) { 	/* Get Window Size */
    *row = 0;
    *col = 0;
    }
  else {
    *row = tw.ws_row;
    *col = tw.ws_col;
    }
#else
  *row = 0;
  *col = 0;
#endif /* TIOCGWINSZ */
}
