/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*--------------------------ospsystem--------------------------------------
.TYPE        routine
.NAME        ospsystem
.MODULE      osp (operating system independant interface, processes)
.ENVIRONMENT  unix
.LANGUAGE    C
.AUTHOR      Trond Melen (IPG-ESO Garching)
.PURPOSE
  Execute a command in a shell (The Borne shell under UNIX) or to a 
  command line interpreter (DCL under VMS).
.ALGORITHM
  UNIX: use the "system()" library call with no changes.

.RETURNS
  Error status of the command (UNIX only).
----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

int ospdebug = 0;         /* flag controlling debug printings */

/* The very simple UNIX implementation. */

long ospsystem(command)
char command[];
{
  return(system(command));
}
