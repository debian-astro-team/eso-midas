/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        iodev (dummy version)
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Host operating system interfaces. Tape management.
.COMMENTS    Tape management. 
	     The functions of this module perform basic i/o to
	     magnetic tapes on Ultrix enviroments
.REMARK	     This is a dummy version of iodev.c for systems without
	     1/2" tape interface.
	     Because every IO interface for tapes  is an implementation
	     dependent, and that varies from one system to other.
	     The real iodev.c must be implemented for every system 
	     particularly, and must be included in the $MIDASHOME/local
	     directory.
.VERSION 1.0 12-Aug-1988   Implementation     C. Guirao
.VERSION 1.1 12-Oct-1989   New design (several iodev's)
.VERSION 1.2 18-May-1990   osuclose has 2 parameters...

 090324		last modif
------------------------------------------------------------*/

#include <fcntl.h>
#include <osudef.h>
#include <oserror.h>
#include   <proto_os.h>		/* ANSI-C prototyping */

/*#define next_iodev*/			/* NO NEXT iodev in chain */
static char class_name[] = "mt";	/* MAgnetic Tape Class */

#ifdef __STDC__
static int ioinfo(int fd , struct osustat * s , int * fileno , long * blkno)
#else
static int ioinfo(fd, s, fileno, blkno)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Retrieve Info concerning an opened device
.RETURNS 0 (success) / -1 (error)
------------------------------------------------------------*/
int	fd;		/* IN: File Descriptor		*/
struct osustat	*s;	/* OUT: The filled components */
int     *fileno;        /* OUT: Where we are    */
long    *blkno;         /* OUT: Where we are    */
#endif
{
  oserror = -1;	
  oserrmsg = "iodev: dummy version. \
Implement your own Tape interface in your local directory";
  return(-1);
}

#if 0		/*================== No Function Implemented Here ==========*/
static int ioopen(name,mode,den)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Open a tape device
.RETURNS File descriptor / -1 (error)
.REMARKS 
------------------------------------------------------------*/
char *name;		/* IN:	Physical name of tape device */
int mode;		/* IN:	Open mode */
int den;                /* IN:  Density. Not used */
{
}

static int ioclose(fd, option)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Close the opened unit
.RETURNS 0 (success) / -1 (failure)
.REMARKS 
------------------------------------------------------------*/
int fd;			/* IN:	Tape file descriptor */
int option;		/* IN:	Option OPU_DISMOUNT	*/
{
}


static int ioread(fd, buffer, size)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Read a block from a magnetic tape.
.RETURNS Bytes read / 0 for EOF (TapeMark) / -1 if error
.REMARKS 
------------------------------------------------------------*/
int fd;			/* IN:	Tape device file descriptor */
char *buffer;		/* IN:	Buffer for reading */
int size;		/* IN:	Length of bytes to be read */
{
}


static int iowrite(fd, buffer, size)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Write a block on a magnetic tape.
.RETURNS Bytes written / -1 (error)
.REMARKS 
------------------------------------------------------------*/
int fd;			/* IN:	Tape device file descriptor */
char *buffer;		/* IN:	Buffer for reading */
int size;		/* IN:	Length of bytes to be read */
{
}


static int ioweof(fd)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Write end-of-file record (tape_mark) on the tape.
.RETURNS 0 (success) / -1 (error)
.REMARKS 
------------------------------------------------------------*/
int fd;			/* IN:	Tape device file descriptor */
int ntm;		/* IN:	Number of tape marks */
{
}


static int iofsf(fd, ntm)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Skip forward space file on a tape. 
.RETURNS Tape marks skipped / -1 (error)
.REMARKS 
------------------------------------------------------------*/
int fd;			/* IN:	Tape device file descriptor */
int ntm;		/* IN:	Number of tape marks */
{
}


static int iobsf(fd, ntm)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Skip backward space file on a tape. 
.RETURNS Tape marks skipped / -1 (error)
.REMARKS 
------------------------------------------------------------*/
int fd;			/* IN:	Tape device file descriptor */
int ntm;		/* IN:	Number of tape marks */
{
}


static int iorew(fd)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Rewind tape
.RETURNS 0 (success) / -1 (error)
.REMARKS 
------------------------------------------------------------*/
int fd;			/* IN:	Tape device file descriptor */
{
}


static int iosread(fd, spos, ssize, buffer, size)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Read a block from a unit
.RETURNS Bytes read / -1 if error
------------------------------------------------------------*/
int     fd;     	/* IN:  File descriptor */
long    spos;   	/* IN:  Sector Number */
int     ssize;  	/* IN:  Size of one sector */
char    *buffer;        /* IN:  Buffer for reading */
int     size;   	/* IN:  Length of bytes to be read */
{
}

static int ioswrite(fd, spos, ssize, buffer, size)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Read a block from a unit
.RETURNS Bytes read / -1 if error
------------------------------------------------------------*/
int     fd;     	/* IN:  File descriptor */
long    spos;   	/* IN:  Sector Number */
int     ssize;  	/* IN:  Size of one sector */
char    *buffer;        /* IN:  Buffer for reading */
int     size;   	/* IN:  Length of bytes to be read */
{
}

#endif		/*============= END of Functions Templates ===============*/


/*=====================================================================
 * 		Definition of the structure returned to osu
 *=====================================================================*/

#ifdef next_iodev
struct iolist *next_iodev();
#else
#define next_iodev	(IODEV)0
#endif

static OPITEM list_of_functions[] = {
	{ U_INFO,	ioinfo},
/*	{ U_OPEN,	ioopen},	*/
/*	{ U_CLOSE,  	ioclose},	*/
/*	{ U_READ,	ioread},	*/
/*	{ U_WRITE,	iowrite},	*/
/*	{ U_REWIND, 	iorew},		*/
/*	{ U_WEOF,	ioweof},	*/
/*	{ U_FMF,	iofsf},		*/ /* File Move Forward  	*/
/*	{ U_FMB,	iobsf},		*/ /* File Move Backward 	*/
/*	{ U_EOM,	ioeom}		*/ /* Move to EOMedia 		*/
 };

static struct iolist this_dev = {
	next_iodev,	/* Next iodev in List */
	class_name,	/* How it's written in DEVCAPFILE */
	sizeof(list_of_functions)/sizeof(list_of_functions[0]),
	list_of_functions
 };
 
struct iolist *iodev()
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Get All definitions concerning this class of Devices
.RETURNS The iolist
.REMARKS Simply returns the local iolist address...
------------------------------------------------------------*/
{
  return(&this_dev);
}
