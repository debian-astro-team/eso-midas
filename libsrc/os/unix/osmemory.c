/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Module
.IDENTIFICATION	osmemory
.AUTHOR    	Marc Wenger [CDS], Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	Memory Management
.ENVIRONMENT 	UNIX
.COMMENTS
	Simple interface to malloc / realloc / free,
	(osmalloc requires the full osm module and has badly designed
	parameters, realloc is missing...)
.VERSION     [1.1] 04-Oct-1987	Adapted by Francois Ochsenbein 28-Oct-1986
.VERSION     [3.1] 21-Apr-1993	Posix compliant CG.

 090324		last modif
----------------------------------------------------------------------------*/

/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

#include <fcntl.h>
#include <proto_os.h>		/* ANSI-C prototyping */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>



#ifdef OSERROR_D                        /* oserror already defined (Silicon G)*/
#define oserror midaserror
#endif

extern int oserror;

char	*osmmget(nmemb)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Allocate a zone
.RETURNS Address of usable zone, or NULL (failed); oserror contains
	then the error code.
.REMARKS 
-------------------------------------------------------------*/
size_t  nmemb;		/* IN: Required length (bytes)	*/

{
char *p;
	

p = (char *) calloc(nmemb,(size_t)1);
if (!p)	oserror = ENOMEM;

return(p);
}

void osmmfree(address) 
/*+++
.PURPOSE Free a zone allocated via osmmget
.RETURNS 0 / -1 if error.
.REMARKS A null address to free returns 0 (no error).
         Unix implementation always returns 0.
---*/
void *address ; 	/* IN: adress to free	*/
{
  free(address);
}

char	*osmmexp(address, nbytes) 
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Reallocates a piece of memory allocated via osmmget
.RETURNS Address of new zone, or NULL if failed.
.REMARKS Values are copied to new zone.
------------------------------------------------------------*/
char *address ; 	/* MOD: adress to realloc	*/
size_t nbytes;	  	/* IN: New length		*/

{
register char *p;


if (address)
   p = (char *) realloc((void *)address, nbytes);
else
   p = (char *) malloc(nbytes);

if (!p)	oserror = ENOMEM;
return(p);
}
