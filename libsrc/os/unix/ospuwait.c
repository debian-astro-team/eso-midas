/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        ospuwait
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Host operating system interfaces, Process control.
.COMMENTS    ospuwait() has been stripped from it original osp.c file
	     because it can not be compiled with the POSIX_SOURCE definition
	     on HP-UX.
.VERSIONS

 090324		last mdoif
-------------------------------------------------------------------------*/

#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <proto_os.h>		/* ANSI-C prototyping */



int ospuwait(time)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Delays the execution of a process for a time interval.
         Resolution of time interval is in microseconds.
.RETURNS Always 0
.REMARKS System dependencies:
 -- UNIX: select(2)
------------------------------------------------------------*/
unsigned int time;			/* IN : time delay in microseconds */
{
  struct timeval tval;

  tval.tv_sec = time / 1000000;
  tval.tv_usec = time % 1000000;
  select(0,NULL,NULL,NULL, &tval);
  return(0);
}
