/* @(#)osl.c	19.1 (ES0-DMD) 02/25/03 13:55:50 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE           Module
.IDENTIFICATION osl
.AUTHOR         Michele Peron
.LANGUAGE       C
.KEYWORDS       Directory searching
.ENVIRONMENT    POSIX complaint.
---------------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 * With _POSIX_SOURCE defined fails in ULTRIX V4.3
#define _POSIX_SOURCE 1
 */

#include <sys/types.h>		/* Needed for Convex V10.0.2 */
#include <dirent.h>
#include <proto_os.h>		/* ANSI-C prototyping */
#include <stdio.h>
static DIR *dp;
static char patmatch[80];

int oslopen(dirname,pattern)
/*++++++++++++++++
.PURPOSE opens one directory
.RETURNS number of entries in the directory matching the given pattern
---------*/
char *dirname;
char *pattern;
{
  int noentries,res;
  struct dirent *dirp;
  strcpy(patmatch,pattern);
  if ( (dp = opendir(dirname)) == NULL) {
    noentries = -1;
    return(noentries);
    }
  noentries = 0;
  while ( (dirp = readdir(dp)) != NULL) {
    res = amatch(dirp->d_name,pattern);
    if (res == 0) noentries++;
    }
  rewinddir(dp);
  return(noentries);
}


struct dirent *oslread()
/*++++++++++++++++
.PURPOSE look for the next entry in the directory matching a given pattern
.RETURNS returns NULL if not found
---------*/
{
  struct dirent *dirp;

  while ( (dirp = readdir(dp)) != NULL)
    if ( amatch(dirp->d_name,patmatch) == 0 ) return(dirp);
  return(NULL);
}

int oslclose()
/*++++++++++++++++
.PURPOSE closes one directory
.RETURNS 0 (normal) or -1 (failure)
---------*/
{
  int status;
  status = closedir(dp);
  return(status);
}

int amatch(lin,arg)
/*++++++++++++++++
.PURPOSE checks whether lin matches the given pattern arg
.RETURNS 0 (yes) or 1 (no)
---------*/
char *lin;
char *arg;
{
  int i;
  while (*arg) {
    if (!*lin && (*arg != '*' || (*arg == '*' && *(arg+1))))   return(1);
    else if (!*lin && *arg == '*') return(0);
    else if (*arg == '[') {
      arg++;
      while (*arg != ']') {
        if (*lin == *arg) {
          lin++;
          arg+=2;
          return(0);
          }
        else arg++;
        }
      return(1);
      }
    else if (*arg == '?') lin++;
    else if (*arg == '*') {
      arg++;
      while (*lin) 
        if (!*arg) return(0);
	else if ((i = amatch(lin,arg)) == 0) return(0); 
	else lin++;
      if (i == 1) return(i);
      }
    else {
      if (*arg != *lin) return (1);
      else lin++; 
      }
    arg++;
    }
  if (*lin) return(1); else return(0);
}

int omatch(ch,s,j)
char *ch,*s;
int j;
{
  while (j--) if (*s++==*ch) return(1);
  return(0);
}
