/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.TYPE		Module
.IDENTIFICATION	osmessage
.AUTHOR    	Francois Ochsenbein [ESO-IPG]
.LANGUAGE  	C
.KEYWORDS	System error messages
.ENVIRONMENT	UNIX (BSD / SYSV)
.COMMENTS	The ``message'' list can be generated with
		a program listed below as a comment. The following externals
		are defined here:
\begin{TeX}
\begin{itemize}
\item oserror ({\em int}), system-independant error number
\item oserrmsg ({\em char *}), pointer to a text string to be used if
		oserror has the value $-1$.
\end{itemize}
\end{TeX}
.VERSION 1.0	08-Oct-1987: Creation
.VERSION 1.1	19-Jan-1988: Added external pointer to be used
		as message when oserror = -1.
.VERSION 

 090324		last modif
----------------------------------------------------------------------------*/

#include <fcntl.h>
#include <errno.h>
#include <osdefos.h>
#include <macrogen.h>
#include <oserror.h>
#include <proto_os.h>		/* ANSI-C prototyping */


/*==========================================================================*/
char *osmsg()
/*+++
.PURPOSE Provide a text explaining the error.
.RETURNS Pointer to the relevant message. 
---*/
{ 
static char *p;

if (oserror < 0 )	
   p = oserrmsg;
else if (oserror)
   p = strerror(oserror);
else
   p = "";

if (!p)	p = "????";
  
return(p);
}
