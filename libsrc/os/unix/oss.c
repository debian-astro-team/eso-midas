/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        oss
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Host operating system interfaces. Interprocess signals.
.COMMENTS    These routines will handle the interprocess events and signals.
             They will allow the following :
                 * to kill processes
                 * to send signals to other processes
                 * to catch or to ignore these signals
                 * to do something when a signal is caught
                 * to handle system interrupts like arithmetic traps and so ...
                 * to suspend and resume processes after some time.
                 * to wait for a signal (process synchronization).
.VERSION     [0.0] 08-Dec-1986   Definition      B. Pirenne
.VERSION     [0.1] 09-Dec-1986   Programmation   B. Pirenne
.VERSION     [0.2] 30-Jan-1987   Allow more signals to be catched B. Pirenne
.VERSION     [0.3] 30-Jan-1987   Add a time out in osswait        B. Pirenne
.VERSION     [1.0] 07-Jul-1987   Add SIGINT to the allowed signals B.  Pirenne.
.VERSION     [1.1] 05-May-1987   Cosmetic changes   C. Guirao.
.VERSION     [1.2] 25-Apr-1991   VOID + signal. 
.VERSION     [2.1] 14-Jul-1992   VOID is called now VOIDSIG
.VERSION     [3.1] 21-Apr-1993   Posix compliant

 090324		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

#include <fcntl.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <proto_os.h>		/* ANSI-C prototyping */

#ifdef OSERROR_D                        /* oserror already defined (Silicon G)*/
#define oserror midaserror
#endif

extern int oserror;
static int flag;

typedef void (*FUNCTION)();	/* Pointer to Function  */

/* ARGSUSED */
#ifdef __STDC__
static void alarm_handler(int sig) 
#else
static void alarm_handler(sig) 
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE dummy routine to catch SIGALRM avoiding killing the process 
-----------------------------------------------------------*/
int sig;
#endif
{
} 

/* ARGSUSED */
#ifdef __STDC__
static void catch(int sig)
#else
static void catch(sig)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE dummy routine to allow the pause call to return
.REMARKS ``sig'' is mandatory parameter always passed to a routine
         called from osscatch.
.REMARKS sets the status variable to one if called.
------------------------------------------------------------------*/
int sig;
#endif
{
  flag = 0;
}


int osssend(pid, sig)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE send a signal to a process
.RETURNS Upon successful completion a value of zero is returned, else 0
         is returned and ``oserror'' is set to the actual error code.
.REMARKS System dependencies:
 --UNIX: kill(2)
.REMARKS the allowed signals are only SIGUSR1,SIGUSR2,
SIGTERM and SIGALRM.
------------------------------------------------------------*/
int  pid;		/* IN : process identification */
int  sig;  		/* IN : signal (interruption) logical name */
{
  switch (sig) {
  case SIGKILL:
  case SIGUSR1:
  case SIGUSR2:
  case SIGTERM:
  case SIGALRM:      /* allowed signals ... */
  case SIGINT:
  case SIGCHLD:
    if(kill (pid,sig) < 0) {
      oserror = errno;
      return(-1);
      }
    break;
  default:           /* not allowed signals */;
    oserror = EINVAL;
    return(-1);
    }
  return(0);
}

int osscatch(sig, f)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE  Catch a signal and execute a given function when received.
.RETURNS Value 0 on normal return. 
.REMARKS System dependencies:
 --UNIX: sigaction()
.REMARKS This routine acts as a declarative statement : it means that
         its effect continues until the end of the calling process or
         until a new call to this routine for the same signal type.
         It is then better located in the beginning of a program.
------------------------------------------------------------*/
int sig;		/* IN : signal identification (see signal.h) */
FUNCTION f;      	/* IN : Function to call in case of Interrupt */
{
  struct sigaction act, oact;

  act.sa_handler = f;
  sigemptyset(&act.sa_mask);
  act.sa_flags = 0;
  if (sigaction(sig,&act,&oact) != 0) { oserror = errno; return(-1); };

  return(0);
}


int osswait(sig,time_out)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Waits for signal to be received.
.RETURNS Value -1 if something wrong happened, 0 is a signal has been
         received before the timeout, 1 if stopped for time out .
.REMARKS System dependencies:
 --UNIX: sigaction(2), pause(2), sleep(3)
.REMARKS This routine acts as an executable statement : it means that
         its effect will be to wait for the given signal (!! One can only
	 wait for a signal that it is possible to send (INT, ALRM, TERM, USR1
	 or USR2). The execution
         of the calling process will be interrupted during this wait
         time. The execution will continue if the given signal is received 
         or if the time out period is expired.
         This routine is best used with ``osssend'' to suspend and resume
         programmes execution.
.REMARKS It could have been implemented with sleep(). With Posix sleep should
   	 have returned when any signal is received, but not with BSD systems
 	 (SunOS) where sleep elapsed the time even when signals are received
------------------------------------------------------------*/
int sig;		/* IN : signal identification (see signal.h) */
unsigned time_out;	/* IN : amount of seconds allowed for the */
			/*      wait (0 = infinite) */
{
  struct sigaction act, oact, oalrm;
  sigset_t set;
	
  flag=1;
  /* check the value of the signal to wait for ... */
  switch (sig) {
  case SIGUSR1:
  case SIGUSR2:
  case SIGTERM:
  case SIGALRM:
  case SIGINT:
  case SIGCHLD:

    /* Catch the signal ALRM to avoid proccess to terminate */
    act.sa_handler = alarm_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    if (sigaction(SIGALRM,&act,&oalrm) != 0) { oserror = errno; return(-1); }

    /* Catch the signal sig to change value of flag */
    act.sa_handler = catch;
    if (sigaction(sig,&act,&oact) != 0) { oserror = errno; return(-1); }

    /* Suspend process all signal except sig & SIGALRM (for a */
    sigfillset(&set);
    sigdelset(&set,sig);
    sigdelset(&set,SIGALRM);

    /* Set alarm if timeout */
    if (time_out != 0) (void) alarm(time_out);

    /*  wait for signal to come  or timeout */
    (void) sigsuspend(&set);
    
    /* If timeout cancel alarm */
    if (time_out != 0) (void) alarm(0);

    /* restore previous status for sig and SIGALRM */
    if (sigaction(sig,&oact,&act) != 0) { oserror = errno; return(-1); }
    if (sigaction(sig,&oalrm,&act) != 0) { oserror = errno; return(-1); }
    return(flag);	/* flag=0 (signal arrived), 1 (timeout first) */
  default:
    oserror = EINVAL;
    return(-1);
  }
}
