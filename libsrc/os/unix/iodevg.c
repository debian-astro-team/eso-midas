/*===========================================================================
  Copyright (C) 1995-2010 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        iodevd
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Host operating system interfaces. 
.COMMENTS    Tape management. 
             The functions of this module perform basic i/o to
             magnetic tapes on Unix enviroments
.VERSION 1.0	08-Jan-1993   Implementation     C. Guirao

 100426		last modif


We don't work with magtapes anymore. Here's just a skeleton left
of the original iodevg.c module...

------------------------------------------------------------*/

#define next_iodev	iodev		/* Next iodev in chain */

static char class_name[] = "generic";	/* Default Tape Class */

#include <osudef.h>

extern int oserror;
extern char *oserrmsg;




/*=====================================================================
 * 		Definition of the structure returned to osu
 *=====================================================================*/

#ifdef next_iodev
struct iolist *next_iodev();
#else
#define next_iodev	(IODEV)0
#endif


static struct iolist this_dev = {
	next_iodev,	/* Next iodev in List */
	class_name,	/* How it's written in DEVCAPFILE */
	0,
	(OPITEM *)0
 };


struct iolist *iodevg()
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Get All definitions concerning this class of Devices
.RETURNS The iolist
.REMARKS Simply returns the local iolist address...
------------------------------------------------------------*/
{
  return(&this_dev);
}
 
