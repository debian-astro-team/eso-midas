/* @(#)tz5.c	19.1 (ES0-DMD) 02/25/03 13:58:00 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        TZ5.C
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table interface (Design 2.0) low level routines.
.COMMENTS    This module contains the lower level routines handling tables.
             The module handles the table file control block (TBL_FCT)
             to include a new table, release and find table identification.

.VERSION 1.0 	25-Mar-1987   Implementation     JDP
.VERSION 1.1 	20-Oct-1987   Modify calling sequence 
.VERSION 3.0 	01-Jul-1990   New Version with Arrays / Elementary IO
.VERSION 3.1 	17-Oct-1990   Modified TBL_tid (retrieve Number from Name)
.VERSION 3.2 	03-Dec-1990 Keep table number compatible with SC routines
.VERSION 3.21 	17-Dec-1990 TBL_kill: check input parameter
------------------------------------------------------------*/

#include <midas_def.h>		/* General MIDAS Symbols	*/
#include <fctext.h>

#include <tblsys.h>		/* Table System parameters	*/
#include <tblerr.h>		/* List of Table Errors		*/
#include <tbldef.h>		/* Symbols used for Tables	*/

#include <macrogen.h>		/* Classical macros		*/

char *osmmget();
				/* Array of opened tables	*/
static TABLE *tables[TBL_NUMBER] = { (TABLE *)0 };
static int tno[TBL_NUMBER];	/* holds out_of_bounds `tid's */

/*==============================================================*/
int TBL_new(imno)
/*+++++++++
.PURPOSE Find a free table identifier --- use fid if possible
.RETURNS The tid number / -1 if error
----------*/
int imno;	/* IN: Forced number (-1 for default)	*/

{
int tid;




/* Try to assign a number to the resulting tid;
   if this number is not OK, look for first avaiable item */


if (imno >= TBL_NUMBER)		/* imno > size of `tables' array */
   {				/* thus, we need indirect indexing */
   for (tid=0; (tid<TBL_NUMBER) && (tables[tid]); tid++)	;

   if (tid < TBL_NUMBER) 
      {
      tables[tid] = (TABLE *)osmmget(sizeof(TABLE));
      tno[tid] = imno;		/* save original `imno' */
      return (tid);
      }

   return (-1);			/* no free entry found */
   }

tid = imno;
if ((tid >= 0) && (tables[tid])) return (tid);	/* table  already open */

/* use same index as in FCT (= imno) */

tables[tid] = (TABLE *)osmmget(sizeof(TABLE));
tno[tid] = tid;			/* point to itself */

return (tid);
}

int TBL_kill(tid)
/*+++++++++
.PURPOSE Remove the table created by TBL_new
.RETURNS Status
.REMARKS No check made on tid, assumed to be valid...
----------*/
	int	tid;	/* IN: Table identifier	*/
{
int  nr;



if (tid >= 0) 
   {
   if (tid >= TBL_NUMBER)
      {
      for (nr=0; nr<TBL_NUMBER; nr++)
         {					/* look for matching entry */
         if (tno[nr] == tid)
            {
            tid = nr;
            goto cleanup;
            }
         }
      return (ERR_INPINV);
      }
       
  cleanup:
   osmmfree ((char *)tables[tid]);
   tables[tid] = (TABLE *)0;
   return (ERR_NORMAL);
   }

else
   return (ERR_INPINV);
}

TABLE *TBL_ptr(tid)
/*+++++++++
.PURPOSE Convert a table identifier into a TABLE pointer
.RETURNS Corresponding TABLE pointer / NULL if fails
----------*/
int	tid;	/* IN: Table identifier	*/

{
int  nr;

TABLE *tp;
    


    if ( (tid >= 0) && (tid < TBL_NUMBER))
       tp = tables[tid];
    else 
       {
       for (nr=0; nr<TBL_NUMBER; nr++)
          {
          if (tno[nr] == tid)
             {
             tp = tables[nr];			/* use this entry */
             return (tp);
             }
          }
       tp = (TABLE *)0;
       }
     
    return (tp);

}

/*==============================================================*/
int TBL_tid(name)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Derive table number from its name
.RETURNS Corresponding tid / -1 if not found
------------------------------------------------------------------*/
	char	*name		/* IN: table name to look for	*/;
{
	int	i;
	TABLE	*tp;

    for (i = 0 ; i < TBL_NUMBER; i++) {
        if_not (tp = tables[i])	continue;
        if (strcmp(tp->phname,name)==0) 
	    break;
    }
    return (i < TBL_NUMBER ? i : -1);
}

