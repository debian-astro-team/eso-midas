/* @(#)sort.c	19.1 (ES0-DMD) 02/25/03 13:57:58 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        sort.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table interface (Design 2.0) low level routines.
.COMMENTS    This module contains the lower level routines handling tables.
             Sorting routines.
             Temporary modification to bubble sort for testing purposes
             of the routine QSRTIAT.
             This module has to be recoded in an optimal way.

.VERSION     1.0 25-Mar-1987   Definition JDP
.VERSION     1.0 25-Aug-1989   Definition JDP
------------------------------------------------------------*/

static int sort_order;	/* 1 for ascending, -1 for descending	*/
static int len;
static int (*diff)(), (*swap)();

static int diff1 (s1, s2)
	char *s1, *s2;
{
	int	dif, i;
	
   for (i = len; --i >= 0; s1++, s2++){
	if (dif = *s1 - *s2) 	return (sort_order * dif);
   }
   return(0);
}

qsort(data, first, last)
	char **data;
	long first, last;
{
	char *px, *t;
	long i,j,m;

  i = first; j = last;
  m = (first+last)/2;
  px = data[m];
  
  while(1) {
	while ( (i < last ) && ((*diff)(data[i], px) < 0))	i++;
	while ( (j > first) && ((*diff)(data[j], px) > 0))	j--;
	if (i <= j) {
		if (i < j)	t = data[i], data[i] = data[j], data[j] = t;
		i++; j--;
	}
	else	break;
  } 

  if (first < j)	qsort (data, first, j);
  if (i < last)		qsort (data, i, last );
}

main()
{
   static char *index[10];
   static char list[100] = "";
   char msg[80];
   
   int	i;
   
   diff = diff1;
   
   for (i=0; i < (sizeof(index)/sizeof(long)); i++) {
	index[i] = list+i*10;
	printf ("String #%d: ", i+1);
	gets(msg);
	oscopy (index[i], msg, 10);
   }
   len = 10;
   
   sort_order = 1;
   
   qsort (index, 0, 9);
   
   puts("Sorted List (Ascending)");
   for (i=0; i < (sizeof(index)/sizeof(long)); i++) 
	printf("%s (%d)\n", index[i], (index[i]-list)/10);
   
   
   sort_order = -1;
   qsort (index, 0, 9);
   
   puts("Sorted List (Descending)");
   for (i=0; i < (sizeof(index)/sizeof(long)); i++) 
	printf("%s (%d)\n", index[i], (index[i]-list)/10);
}
