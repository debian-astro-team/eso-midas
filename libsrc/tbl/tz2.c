/* @(#)tz2.c	19.1 (ES0-DMD) 02/25/03 13:58:00 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        tz2.c
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table interface (Design 2.0). 
.KEYWORDS    tables, file name
.PURPOSE     Some string utilities are here.
.VERSION  1.0 	25-Mar-1988   Implementation     JDP
.VERSION  3.0 	01-Jul-1990   Extracted from tz2
------------------------------------------------------------*/

#include <midas_def.h>		/* General MIDAS Symbols	*/
#include <tblsys.h>		/* Table System parameters	*/
#include <atype.h>		/* Character classification	*/

				/* Basic parameters		*/

static	char	label_case = TBL_LABCASE;	/* 0 for ignore case	*/

/*==========================================================================*/

int TBL_s0 (s, len)
/*+++++++++++
.PURPOSE Remove trailing spaces
.RETURNS Length of string (without trailing spaces)
--------------*/
	char	*s;	/* MOD: String		*/
	int	len;	/* IN: Size of string	*/
{
   	int	i;
   
  i = oscbspan ((unsigned char *)s, len, _SPACE_, main_ascii);
  s[++i] = '\0';
  
  return(i);
}

int TBL_s1 (s, len)
/*+++++++++++
.PURPOSE Add trailing spaces
.RETURNS Length of string (with trailing spaces)
--------------*/
	char	*s;	/* MOD: String		*/
	int	len;	/* IN: Size of string	*/
{
   	int	i;
   
  i = strlen (s);
  while (i < len)	s[i++] = ' ';
  
  return(i);
}

/*==========================================================================*/

int TBL_sdiff(s1,s2)
/*+++++++++++
.PURPOSE Compute difference of two strings; case sensitivity depends
		on TCOSET("label_case").
.RETURNS A positive value if s1 > s2, zero if strings are identical,
	a negative value if s1 < s2.
-----------*/
     char *s1;	/* IN: address of first string 	*/
     char *s2;	/* IN: address of 2nd string  	*/
{
	char 	*p, *q;
	char 	cp, cq;

  if (label_case) {		/* Don't ignore case	*/
  	for (p=s1, q=s2;*p ; p++, q++)
		if (*p != *q)	break;
  }
  else		  {		/* Ignore case		*/
  	for (p=s1, q=s2;*p ; p++, q++)
  	{	cp = toupper(*p);
        	cq = toupper(*q);
		if (cp != cq)	break;
  	}
  }
	  
  return(*p - *q);
}

int TBL_labcase(value)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Set case sensitivity of Labels
.RETURNS Previous option
.REMARKS 
-------------------------------------------------------------*/
	int	value	/* IN: 0 for case insensitivity	*/;
{
	int	old_option;
	
  old_option = label_case;
  label_case = value;
  return(old_option);
}

/*==========================================================================*/

char *TBL_ssave(s)
/*+++++++++++
.PURPOSE Clone (copy) a string into a new piece of Memory
.RETURNS Address of copied string
--------------*/
	char	*s;	/* MOD: String		*/
{
	char	*p, *osmmget();
   
  p = osmmget (1 + strlen(s));
  if (p)	strcpy (p, s);
  
  return(p);
}

