/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        TCM.C
.LANGUAGE    C
.AUTHOR      J.D.Ponz  IPG-ESO Garching
.CATEGORY    table interface (Design 2.0)
.COMMENTS    This module contains the routines handling the table
             structure as a matrix. The following operations can be 
             performed:
             - Allocate memory for the matrix (TCMALL)
             - Release memory associated to the matrix (TCMFRE)
             - Copy (selected) table entries into the matrix, with
               implicit type conversion (TCMGET)
             - Copy the matrix into the (selected entries of the) table
               (TCMPUT)
             - Obtain the bit pattern used for the representation
               of NULL values in memory (TCMNUL).
             
.VERSION     1.0 25-Mar-1988   Implementation     JDP
.VERSION  3.0 	01-Jul-1990   	New Version with Arrays / Elementary IO

 090407		last modif
------------------------------------------------------------*/

#include <midas_def.h>		/* ANSI-C Prototyping 		*/
#include <tblsys.h>		/* Table System parameters	*/
#include <tbldef.h>		/* Symbols used for Tables	*/
#include <tblerr.h>		/* List of Table Errors		*/

/*===============================================================*/

int TCMALL(type, ncol, nrow, pointer)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
        Allocates memory for the matrix.
.ALGORITHM
        The routine returns the pointer of the allocated memory for
        the matrix with `ncol' columns and `nrow' rows. 
        The actual space allocated depends on the data type defined
        by the first argument. 
        Up to 16 different memory areas can be allocated.
.RETURNS
        Status 
------------------------------------------------------------------*/
	char	*type	  /* IN: column type		*/;
        int    ncol      /* IN: number of columns        */;
        int    nrow      /* IN: number of rows           */;
        int    *pointer  /* OUT: pointer to the allocated matrix */;
{
  	int	status;

  status = ERR_TBLIMP;

  return (TBL_errf(status, "Matrix allocation not available"));
}

int TCMFRE(pointer)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
        Releases the memory associated to the pointer.
.ALGORITHM
        The routine uses the pointer to identify the allocated memory
        and releases it.
.RETURNS
        Status 
------------------------------------------------------------------*/
        int    *pointer           /* IN: pointer to the allocated matrix */ ;
{
  	int	status;

  status = ERR_TBLIMP;

  return (TBL_errf(status, "Matrix allocation not available"));
}

int TCMGET(tid, pointer, nac, nar)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
        Copies the table information into the matrix.
.ALGORITHM
        The information in the table is converted into the 
        matrix internal format, only selected columns and rows are 
        considered. NULL values are also tranfered, use TCMNUL
        to find out the bit pattern used to represent NULLs 
        in memory.
        The routine returns the actual number of columns and rows copied.
        The table must be open (TCTOPN or TCTINI).
.RETURNS
        Status 
------------------------------------------------------------------*/
        int    tid          /* IN : table id */;
        int    *pointer     /* IN : pointer to the allocated matrix */;
        int    *nac         /* OUT: actual number of columns        */;
        int    *nar         /* OUT: actual number of rows           */;
{
  	int	status;

  status = ERR_TBLIMP;

  return (TBL_errf(status, "Matrix allocation not available"));
}


int TCMPUT(tid, pointer, nac, nar)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
        Copies the matrix data into the table.
.ALGORITHM
        The information in the matrix is converted into the 
        table internal format, only selected columns and rows are 
        considered. NULL values are also tranfered.
        The table must be open (TCTOPN or TCTINI).
.RETURNS
        Status 
------------------------------------------------------------------*/
        int    tid          /* IN : table id */;
        int    *pointer     /* IN : pointer to the allocated matrix */;
        int    nac          /* IN : actual number of columns        */;
        int    nar          /* IN : actual number of rows           */;
{
  	int	status;

  status = ERR_TBLIMP;

  return (TBL_errf(status, "Matrix allocation not available"));
}


int TCMCON(tblsel, tdtrue, tdfalse)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
        Obtain the representation in memory of some system constants.
.ALGORITHM
        The routine gives the selection flag and the true and false values 
        in double precision.
.RETURNS
        Status 
------------------------------------------------------------------*/
        float   *tblsel         /* OUT: selection flag on      */;
        double  *tdtrue         /* OUT: double precision true  */;
        double  *tdfalse        /* OUT: double precision false */;
{

  *tblsel  = TBL_TRUE;
  *tdtrue  = TBL_TRUE;
  *tdfalse = TBL_FALSE;

  return (0);
}


int TCMNUL(inull, rnull, dnull)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE
        Obtain the representation in memory of NULL values.
.ALGORITHM
        Depending on the system, the representation of NULL
        values in double and single precision are returned.
.RETURNS
        Status 
.MODIF  M.P 070291 take care of number of items
------------------------------------------------------------------*/
        int    *inull        /* OUT: integer null value */;
        float   *rnull        /* OUT: float null value */;
        double  *dnull        /* OUT: double null value */;
{
  
   TBL_toNULL((TBL_D_I4<<TBL_D_BITS)|1, (char *)inull);
   TBL_toNULL((TBL_D_R4<<TBL_D_BITS)|1, (char *)rnull);
   TBL_toNULL((TBL_D_R8<<TBL_D_BITS)|1, (char *)dnull);

  return (0);
}

