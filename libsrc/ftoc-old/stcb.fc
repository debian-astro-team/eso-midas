/* @(#)stcb.fc	19.1 (ES0-DMD) 02/25/03 13:54:13 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  STC.FC +++++++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module STC.FC
.COMMENTS
Module contains layer between the catalog related FORTRAN STxxxx interfaces
and the SC_interfaces written in (hopefully independent) C
.AUTHOR         K. Banse  	ESO - Garching
.KEYWORDS       standard interfaces.
.ENVIRONMENT    FORTRAN and C standards
.VERSION  [1.00] 880919:  created
.VERSION  [1.20] 881010:  add SZCSHO
.VERSION  [1.30] 890119:  updated
.VERSION  [1.40] 900219:  update parameters of SZCSHO
.VERSION  [3.00] 901213:  Master file. CG.
-----------------------------------------------------------------------------*/


#include <ftoc.h>
#include <midas_def.h>
	


 
SUBROUTINE STCLIS(catfile,intval,status)
CHARACTER   catfile;	/* IN: catalog file  */
fint2c *intval;		/* IN: listing interval [low,hi]  */
fint2c	*status;
{
    *status = SCCLIS(STRIPPED_STRING(catfile),intval);
}



SUBROUTINE STCSHO(catfile,totent,lastent,status)
CHARACTER   catfile;	/* IN: catalog file  */
fint2c	*totent;	/* OUT: no. of entries */
fint2c	*lastent;	/* OUT: last entry no. */
fint2c	*status;
{
    *status = SCCSHO(STRIPPED_STRING(catfile),totent,lastent);
}
