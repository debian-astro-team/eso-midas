/* @(#)stfa.fc	19.1 (ES0-DMD) 02/25/03 13:54:14 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  STFA.FC +++++++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module STFA.FC
.COMMENTS
Module contains layer between the frame related FORTRAN STxxxx interfaces
and the SC_interfaces written in (hopefully independent) C
.AUTHOR         K. Banse  	ESO - Garching
.KEYWORDS       standard interfaces.
.ENVIRONMENT    FORTRAN and C standards
.VERSION  [1.00] 871207:  created from SXFTOC.C
.VERSION  [2.60] 880415:  modified new version - the last one
.VERSION  [2.70] 890119:  add SZCFRNM
.VERSION  [2.80] 900316:  fix problem with FNAME_LEN+2 ...
.VERSION  [3.00] 901213:  Master file. CG.
-----------------------------------------------------------------------------*/

#include <ftoc.h>
#include <ftoc_comm.h>
#include <midas_def.h>


SUBROUTINE STFDEL(name,status)
CHARACTER   name;	/* IN: name of object */
fint2c *status;		
{
    *status = SCFDEL(STRIPPED_STRING(name));
}
 
 

SUBROUTINE STFRNM(oldname,newname,status)
CHARACTER   oldname;	/* IN: old name  */
CHARACTER   newname;	/* OUT: new name  */
fint2c *status;
{
    *status = SCFRNM(STRIPPED_STRING(oldname),STRIPPED_STRING(newname));
}
