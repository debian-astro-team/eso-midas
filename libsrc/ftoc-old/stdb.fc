/* @(#)stdb.fc	19.1 (ES0-DMD) 02/25/03 13:54:14 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  STDB.FC +++++++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module STDB.FC
.COMMENTS
Module contains layer between the descriptor related FORTRAN STxxxx interfaces
and the SC_interfaces written in (hopefully independent) C
.AUTHOR         K. Banse  	ESO - Garching
.KEYWORDS       standard interfaces.
.ENVIRONMENT    FORTRAN and C standards
.VERSION  [1.00] 871207:  created from SXFTOC.C
.VERSION  [1.80] 871210:  add data type option
.VERSION  [1.90] 880125:  take care of base address in 'vmr'
.VERSION  [2.00] 880127:  add STFPUT, STFGET
.VERSION  [2.10] 880208:  make sure, that CHARACTER is used only for a single
                          variable
.VERSION  [2.50] 880401:  new version - split up numeric + character stuff !
.VERSION  [2.60] 880411:  modified new version - the last one
.VERSION  [3.00] 901213:  Master file. CG.
-----------------------------------------------------------------------------*/

#include <ftoc.h>
#include <midas_def.h>
	


SUBROUTINE STDDEL(no,descr,status)
fint2c	*no;		/* IN: no. of data frame */
CHARACTER   descr;	/* IN: descriptor name */
fint2c	*status;
{
   *status = SCDDEL(*no,STRIPPED_STRING(descr));
}

