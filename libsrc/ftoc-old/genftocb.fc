/* @(#)genftocb.fc	19.1 (ESO-DMD) 02/25/03 13:54:12 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  GENFTOCB.FC  +++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module genftocb.fc
.COMMENTS
Contains GENBLD, GENEQF, GENLEN, ISSUBF, DSCUPT
.AUTHOR         K. Banse  	ESO - Garching
.KEYWORDS       tools
.ENVIRONMENT    FORTRAN and C standards
.VERSION  
 [1.00] 920507: Created + extracted from genftoc.fc + utilz.fc

 021211		last modif

-----------------------------------------------------------------------------*/

#include <ftoc.h>
#include <midas_def.h>





SUBROUTINE GENBLD(simno,name,datform,size,imno,cloned,status)
fint2c  *simno;		/* IN: = image id of source image */
CHARACTER   name;	/* IN: name of new image */
fint2c  *datform;	/* IN: = data format of new image */
fint2c  *size;		/* IN: = size of new image */
fint2c  *imno;		/* OUT: = image id of new image */
fint2c  *cloned;	/* OUT: =1 if descr's were cloned, else =0 */
fint2c *status;

{
*status = CGN_IBUILD(*simno,STRIPPED_STRING(name),*datform,*size,imno,cloned);
}



SUBROUTINE GENEQF(afile,bfile,retval)
CHARACTER   afile;	/* IN: 1. file name */
CHARACTER   bfile;	/* IN: 2. file name */
fint2c	*retval;	/* OUT: =1 if Equal, =0 if Differ */
{
int retstat;

retstat = CGN_EQUAL(STRIPPED_STRING(afile),STRIPPED_STRING(bfile));
*retval = 1 - retstat;		/* retstat = 0 if equal and 1 if not ...  */

}



SUBROUTINE GENLEN(strung,retval)
CHARACTER   strung;	/* IN: 1. file name (terminated by \0) */
fint2c    *retval;
{
*retval = strlen(STRIPPED_STRING(strung));
}




SUBROUTINE ISSUBF(infile,indx)
CHARACTER   infile;     /* IN: input file name                  */
fint2c *indx;		/* OUT: index of substring specs, else = 0 */

{

int nx;

nx = issub(STRIPPED_STRING(infile));
if (nx > 0)
   *indx = nx + 1;		/* in C we start from 0 ...  */
else
   *indx = 0;
}





SUBROUTINE DSCUPT(inno,outno,history,status)

fint2c    *inno;		/* IN: source file no. */
fint2c    *outno;		/* IN: dest. file no. */
CHARACTER   history;    /* IN: eventual history string */
fint2c *status;
 
{
*status = CGN_DSCUPD(*inno,*outno,C_STRING(history));
}

