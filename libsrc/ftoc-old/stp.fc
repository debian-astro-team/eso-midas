/* @(#)stp.fc	19.1 (ES0-DMD) 02/25/03 13:54:16 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  STP.FC +++++++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module STP
.COMMENTS
Module contains layer between the plane/line related FORTRAN STxxxx interfaces
and the SC_interfaces written in (hopefully independent) C
.AUTHOR         K. Banse  	ESO - Garching
.KEYWORDS       standard interfaces.
.ENVIRONMENT    FORTRAN and C standards
.VERSION  [1.00] 950622:  created
-----------------------------------------------------------------------------*/

#include <ftoc.h>
#include <ftoc_comm.h>
#include <midas_def.h>

 

ROUTINE STPGET(no,plandir,planno,bufadr,status)
fint2c *no;		/* IN : file no. of data frame */
fint2c *plandir;	/* IN : plane direction */
fint2c *planno;		/* IN : no. of plane */
char *bufadr;		/* IN: address of data buffer  */
fint2c *status;
{
    *status = SCPGET(*no,*plandir,*planno,bufadr);
}


 

ROUTINE STPPUT(no,plandir,planno,bufadr,status)
fint2c *no;		/* IN : file no. of data frame */
fint2c *plandir;	/* IN : plane direction */
fint2c *planno;		/* IN : no. of plane */
char *bufadr;		/* IN: address of data buffer  */
fint2c *status;
{
    *status = SCPPUT(*no,*plandir,*planno,bufadr);
}
