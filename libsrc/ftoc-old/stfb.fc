/* @(#)stfb.fc	19.1 (ES0-DMD) 02/25/03 13:54:14 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++  STFB.FC +++++++++++++++++++++++++++++++++++++++
.LANGUAGE C
.IDENTIFICATION Module STFB.FC
.COMMENTS
Module contains layer between the frame related FORTRAN STxxxx interfaces
and the SC_interfaces written in (hopefully independent) C
.AUTHOR         K. Banse  	ESO - Garching
.KEYWORDS       standard interfaces.
.ENVIRONMENT    FORTRAN and C standards
.VERSION  [1.00] 871207:  created from SXFTOC.C
.VERSION  [2.60] 880415:  modified new version - the last one
.VERSION  [2.70] 890119:  add SZCFRNM
.VERSION  [2.80] 900316:  fix problem with FNAME_LEN+2 ...
.VERSION  [3.00] 901213:  Master file. CG.
-----------------------------------------------------------------------------*/

#include <ftoc.h>
#include <ftoc_comm.h>
#include <midas_def.h>


ROUTINE STFCLO(no,status)
fint2c *no;		/* IN: file no. of data frame */
fint2c *status;
{
    *status = SCFCLO(*no);
}
 

 
SUBROUTINE STFOPN(name,dattype,newopn,filtype,no,status)
CHARACTER   name;	/* IN: name of data frame */
fint2c *dattype;		/* IN: data types as defined in Module header  */
fint2c *newopn;		/* IN: 0: open normally, 1: open same file again */
fint2c *filtype;		/* IN: filetype, e.g. F_IMA_TYPE, ... */
fint2c *no;		/* OUT: file no. of the frame   */
fint2c *status;
{
    *status = SCFOPN(STRIPPED_STRING(name),*dattype,*newopn,*filtype,no);
}

