/* @(#)getfrm.c	19.1 (ESO-DMD) 02/25/03 13:56:34 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT   (c) 1993 European Southern Observatory
.IDENTifer   GETFRM
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    low level plot routine
.LANGUAGE    C
.PURPOSE     determine the size and position of the tickmarks along a axis  
     input:    char  *frame    MANUal or AUTO scaling
 in/output:    float *wcfram   [0]  begin of axis in world coordinates
                               [1]  end of the axis
                               [2]  big tickmarks spacing
                               [3]  small tickmarks spacing

.ALGORITHM   - small tickmarks spacing >= 0.0
                  - big tickmarks spacing == 0.0
                       ==> big tickmarks are calculated
                  - small tickmarks spacing == 0.0
                       ==> small tickmarks are calculated
             - small tickmarks spacing < 0.0
                  logaritmic scaling, big tickmarks are set to 1.0

             - frame == "AUTO"
                  ==> the length of the axis is enlarged by one small tickmark
                      at both sides
.COMMENTS    none
.ENVIRONment MIDAS
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      Symbols used by the PLT interfaces

.VERSION     1.1     20-Sep-1993   FORTRAN --> ANSI-C   RvH

 010423		last mdofi

------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <string.h>
#include <math.h>

/*
 * define some macros and constants
 */
#include <midas_def.h>
#include <plot_def.h>

#ifndef MINFLOAT
#define MINFLOAT	(float) PLT_EPS
#endif

/*
 * here start the code of the function
 */
void GETFRM( frame, wcfram )
char  *frame;
float *wcfram;

{
int     nsmall, nlabel, num;
double  amin, amax, abig, asmall, diff, amant, step, iexp;

/*
 * the constants used
 */
char   *errmess = "*** WARNING: axis start value = end value range adjusted";

/*
 * for the reader of the code...
 */
amin   = *wcfram;
amax   = *(wcfram+1);
abig   = *(wcfram+2);
asmall = *(wcfram+3);

/*
 * we nicely give alternative values, although the input is flat
 */
if ( fabs ( amax - amin ) < PLT_EPS )
   { SCTPUT( errmess );
     if ( fabs( amin ) < PLT_EPS )                /*around zero -0.5 --> 0.5 */
        { amin = -0.5;
          amax =  0.5; 
        }
     else                                            /*around an other value */
        { amin -= 0.5 * fabs( amin );
          amax += 0.5 * fabs( amax );
        } 
   }

if ( *(wcfram+3) > - MINFLOAT )
   { nsmall = 5;           /*number of small tickmarks between a big tickmark*/
     nlabel = 4;
     diff = log10( fabs( amax - amin )/ nlabel );
     iexp = floor (diff);

     amant = diff - iexp;
     if ( amant < 0.15 )
        num = 1;
     else if ( amant < 0.5 )
        { num = 2;
          nsmall = 4;
        }
     else if ( amant < 0.85 )
        num = 5;
     else
        num = 10;

     step =  num * pow( 10.0, iexp );

     if ( fabs( abig ) < PLT_EPS ) abig   = fabs( step );
     if ( fabs( asmall ) < PLT_EPS ) asmall = abig / nsmall;
   }
else
   abig = MYMAX( abig, 1.0 );
   
if (strncmp( frame, "AUTO", 4 ) == 0 || strncmp( frame, "auto", 4 ) == 0)
   { if ( amax < amin )
        { if ( (float) asmall > - MINFLOAT )
             { amin += asmall;
               amax -= asmall;
             }
          else
             { amin = floor( amin ) + 1;
               amax = floor( amax );
             }
        }
     else
        { if ( (float) asmall > - MINFLOAT )
             { amin -= asmall;
               amax += asmall;
             }
          else
             { amin = floor( amin );
               amax = floor( amax ) + 1;
             }
        }
   }
/*
 * set return values
 */
*(wcfram)   = (float) amin;
*(wcfram+1) = (float) amax;
*(wcfram+2) = (float) abig;
*(wcfram+3) = (float) asmall;
}
