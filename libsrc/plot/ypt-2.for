C===========================================================================
C Copyright (C) 1995-2008 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or
C modify it under the terms of the GNU General Public License as
C published by the Free Software Foundation; either version 2 of
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public
C License along with this program; if not, write to the Free
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C       Internet e-mail: midas@eso.org
C       Postal address: European Southern Observatory
C                       Data Management Division
C                       Karl-Schwarzschild-Strasse 2
C                       D 85748 Garching bei Muenchen
C                       GERMANY
C 
C 80723	last modif
C 
C===========================================================================

      SUBROUTINE GETAXS(FRAME,WCFRAM)
C 
      CHARACTER  FRAME
      REAL     WCFRAM(*)
 
      CALL STSTR(1,FRAME)                       !STRIPPED_STRING
C 
      CALL PPP1(WCFRAM)
C
      RETURN
      END


      SUBROUTINE PTAXES(XMNMX,YMNMX,LABELX,LABELY,AGLOPT)
C
      IMPLICIT NONE
C
      CHARACTER*(*)    LABELX,LABELY,AGLOPT
      REAL     XMNMX,YMNMX
C
      CALL STSTR(1,LABELX)                       !STRIPPED_STRING
      CALL STSTR(2,LABELY)                       !STRIPPED_STRING
      CALL STSTR(3,AGLOPT)                       !STRIPPED_STRING
C 
      CALL PPP2(XMNMX,YMNMX)
C
      RETURN
      END

      SUBROUTINE PTFRAM(XWCFRAM,YWCFRAM,LABELX,LABELY)
C
      IMPLICIT NONE
C
      CHARACTER*(*)    LABELX,LABELY
      REAL     XWCFRAM,YWCFRAM
C
      CALL STSTR(1,LABELX)                       !STRIPPED_STRING
      CALL STSTR(2,LABELY)                       !STRIPPED_STRING
C
      CALL PPP3(XWCFRAM,YWCFRAM)
C
      RETURN
      END

      SUBROUTINE PTTEXT(TEXT,XC,YC,ANGLE,CHSIZ,IPOS)
C
      IMPLICIT NONE
C
      CHARACTER*(*)    TEXT
      REAL     XC,YC,ANGLE,CHSIZ
      INTEGER     IPOS
C
      CALL STSTR(1,TEXT)                       !STRIPPED_STRING
C
      CALL PPP4(XC,YC,ANGLE,CHSIZ,IPOS)
C
      RETURN
      END

      SUBROUTINE PTOPEN(DEVNAM,PLNAME,ACCESS,PLMODE)
C
      IMPLICIT NONE
C
      CHARACTER*(*)    DEVNAM,PLNAME
      INTEGER     ACCESS,PLMODE
C
      CALL STSTR(1,DEVNAM)                       !STRIPPED_STRING
      CALL STSTR(2,PLNAME)                       !STRIPPED_STRING
C
      CALL PPP5(ACCESS,PLMODE)
C
      RETURN
      END

      SUBROUTINE PTKRDC(CPAR,MAXVALS,ACTVALS,CVAL)
C 
      IMPLICIT NONE
C
      CHARACTER*(*)    CPAR,CVAL
C
      CALL STSTR(1,CPAR)                       !STRIPPED_STRING
      CALL STLOC(1,1,CVAL)                     !blanked CHAR_LOC
C
      CALL PPP6(MAXVALS,ACTVALS)
C
      RETURN
      END


      SUBROUTINE PTKRDI(IPAR,MAXVALS,ACTVALS,IVAL)
C
      IMPLICIT NONE
C
      CHARACTER*(*)    IPAR
      INTEGER     MAXVALS,ACTVALS,IVAL(1)
C
      CALL STSTR(1,IPAR)                       !STRIPPED_STRING
C
      CALL PPP7(MAXVALS,ACTVALS,IVAL)
C
      RETURN
      END

      SUBROUTINE PTKRDR(RPAR,MAXVALS,ACTVALS,RVAL)
C
      IMPLICIT NONE
C
      CHARACTER*(*)    RPAR
      INTEGER     MAXVALS,ACTVALS
      REAL   RVAL(1)
C
      CALL STSTR(1,RPAR)                       !STRIPPED_STRING
C
      CALL PPP8(MAXVALS,ACTVALS,RVAL)
C
      RETURN
      END

      SUBROUTINE PTKWRC(CPAR,VALUE)
C
      IMPLICIT NONE
C
      CHARACTER*(*)    CPAR,VALUE
      INTEGER     IDUM
C
      CALL STSTR(1,CPAR)                       !STRIPPED_STRING
      CALL STSTR(2,VALUE)                      !STRIPPED_STRING
C
      CALL PPP9(IDUM)
C
      RETURN
      END

      SUBROUTINE PTKWRI(IPAR,NRVAL,IVAL)
C
      IMPLICIT NONE
C
      CHARACTER*(*)    IPAR
      INTEGER     NRVAL,IVAL(1)
C
      CALL STSTR(1,IPAR)                       !STRIPPED_STRING
C
      CALL PPP10(NRVAL,IVAL)
C
      RETURN
      END


      SUBROUTINE PTKWRR(RPAR,NRVAL,RVAL)
C
      IMPLICIT NONE
C
      CHARACTER*(*)    RPAR
      INTEGER     NRVAL
      REAL   RVAL(1)
C
      CALL STSTR(1,RPAR)                       !STRIPPED_STRING
C
      CALL PPP11(NRVAL,RVAL)
C
      RETURN
      END

