/*===========================================================================
  Copyright (C) 1995-2011 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   PCD
.PURPOSE     Plot data by symbols and lines, plot histogram; plot text
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    High level plot interface
.LANGUAGE    C
.COMMENTS    holds PCDATA, PCHIST and PCTEXT
.ENVIRONment MIDAS and AGL
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      Symbols used by the PLT interfaces

.VERSION     1.0     23-Sep-1993   created by R.M. van Hees

 110704		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>
#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <plot_def.h>

/*++++++++++++++++++++++++++++++
.IDENTifer   PCDATA
.PURPOSE     Plot a set of points connected with lines, or indicated by symbols
     input:  int    stype     symbol type, symbols are only plotted 
                                           when ltype equals zero
             int    ltype     line type
             int    binmod    bin mode OFF (= 0) or ON (= 1)
             float  *x_data   X values of the data points
             float  *y_data   Y values of the data points
             float  y_off     offset along the Y-axis
             int    nrdata    number of points to be plotted

.COMMENTS    The data points are connected with lines if ltype is not equal
             to zero. Symbols are only plotted when ltype equals zero. 
             A graphic device must be defined. Coordinates refer to the 
             current active coordinate system.
-------------------------------*/
#ifdef __STDC__
      void PCDATA( int stype, int ltype, int binmod, 
                   float *x_data, float *y_data, float y_off, int nrdata )
#else
      void PCDATA( stype, ltype, binmod, x_data, y_data, y_off, nrdata )
      int   stype, ltype, binmod, nrdata;
      float *x_data, *y_data, y_off;
#endif
{
int    ii, npoint;
float  xbin, *xdata, *ydata=(float *)NULL;
char   buff[10];

char *errbl0 = "*** WARNING: STYPE and LTYPE equal zero: no data plotted";
char *errbl1 = "*** WARNING: no. of points < 1: no data plotted";
/*
 * do the plotting
 */
if (ltype == 0 && stype == 0 ) 
   {
   SCTPUT(errbl0);
   return;
   }
else if (nrdata < 1) 
   {
   SCTPUT(errbl1);
   return;
   }

else if (ltype > 0 && binmod)
   {
   npoint = 2 * nrdata ;
   xdata = (float *) osmmget( npoint * sizeof( float ));
   ydata = (float *) osmmget( npoint * sizeof( float ));
   if ( xdata == NULL || ydata == NULL )
      { 
      if ( xdata != NULL ) (void) osmmfree( (char *) xdata );
      if ( ydata != NULL ) (void) osmmfree( (char *) ydata );
      SCETER( 2, "*** FATAL: troubles with memory allocation" );
      }
   xbin = (*(x_data + 1) - *x_data) / 2;
   *xdata = *x_data - xbin;
   for ( ii = 1; ii < nrdata; ii++)
       { 
       register int jj = 2 * ii;
       *(xdata+jj-1) = (*(x_data+ii-1)+*(x_data+ii))/2;
       *(xdata+jj) = *(xdata+jj-1);
       }
   xbin = (*(x_data + nrdata-1) - *(x_data + nrdata-2)) / 2;
   *(xdata + npoint-1) = *(x_data + nrdata-1) + xbin;
    
   for ( ii = 0; ii < nrdata; ii++ )
       { 
       register int jj = 2 * ii;
       *(ydata + jj + 1) = *(ydata + jj) = *(y_data + ii) + y_off;
       }
   (void) sprintf( buff, "lstyl=%1d", ltype-1 );
   AG_SSET( buff );
   AG_GPLL( xdata, ydata, npoint );
   (void) osmmfree( (char *) xdata );
   }

else
   { 
   ydata = y_data;
   if ( y_off != 0.0 )
      { 
      ydata = (float *) osmmget( nrdata * sizeof( float ));
      if ( ydata == NULL )
         SCETER( 2, "*** FATAL: troubles with memory allocation" );
      else
         for ( ii = 0; ii < nrdata; ii++ ) 
            ydata[ii] = y_data[ii] + y_off;
      }

   if (ltype > 0 ) 
      {
      (void) sprintf( buff, "lstyl=%1d", ltype-1 );
      AG_SSET( buff );
      AG_GPLL( x_data, ydata, nrdata );
      }
   else
      AG_GPLM( x_data, ydata, nrdata, stype-1 );
   }

if ( y_off != 0 || (ltype > 0 && binmod) ) (void) osmmfree( (char *) ydata );
}
 
/*++++++++++++++++++++++++++++++
.IDENTifier  PCHIST
.PURPOSE     draw a histogram with hashing posibility
        input: int   nbins    number of data points
               float *cl      values along the X-axis
               float *rfr     number of occurences per bin
               float fopt[3]  [0] histogram type selector
                                 0: simple staircase
                                 1: staircase steps joined to X-axis
                                >1: boxes with width starting from 0 
                                    (simple line) and increasing 
                                    with mode value in steps of small 
                                    character width
                              [1] space between the filling lines
                              [2] angle of the filling lines w.r.t. X-axis
                                 
.COMMENTS    PCHIST draws a histogram with an option to fill. A graphic 
             device must be defined. Coordinates refer to the current 
             active coordinate system.

             PCHIST uses the AGL routines AG_HIST and AG_FILL
-------------------------------*/
#ifdef __STDC__
      void PCHIST( int nbins, float *cl, float *rfr, float *fopt )
#else
      void PCHIST( nbins, cl, rfr, fopt )
      int   nbins;
      float *cl, *rfr, fopt[3];
#endif
{
register int   ii;
int      mode, npoints;
float    binsize, halfstep, *xval, *yval;

/*
 * dram the histogram in the requested mode
 */
mode = CGN_NINT( fopt[0] );
AG_HIST( cl, rfr, nbins, mode, 0 );

if ( fopt[1] > -999 )
   { binsize = *(cl+1) - *cl;
     halfstep = binsize / 2;
     npoints = 2 * (nbins + 1);
/*
 * allocate memory for the filling XY-values
 */
     xval = (float *) osmmget( npoints * sizeof( float ));
     yval = (float *) osmmget( npoints * sizeof( float ));
     if ( xval == NULL || yval == NULL )
        { if ( xval != NULL ) (void) osmmfree( (char *) xval );
          if ( yval != NULL ) (void) osmmfree( (char *) yval );
          SCETER( 1, "***FATAL: troubles with memory allocation" );
        }
/*
 * set the XY-values for the filling procedure
 */
     *xval = *(xval+1) = *cl - halfstep;
     for ( ii = 1; ii <= nbins; ii++ )
         { register int jj = 2 * ii;
           *(xval+jj+1) = *(xval+jj) = *(xval+jj-1) + binsize;
         }

     *yval = 0.0;
     for ( ii = 0; ii < nbins; ii++ )
         { register int jj = 2 * ii + 1;
           *(yval+jj+1) = *(yval+jj) = *(rfr+ii);
         }
     *(yval+npoints-1) = 0.0;
/*
 * fill the histogram
 */
     AG_FILL( xval, yval, npoints, fopt[1], fopt[2], "" );
/*
 * free memory
 */
     (void) osmmfree( (char *) xval ); 
     (void) osmmfree( (char *) yval ); 
   }
}

/*++++++++++++++++++++++++++++++
.IDENTifer   PCTEXT
.PURPOSE     Draw text in the current viewport
        input: char  *text     text to be drawn
                     float  xc       string position in x
                     float  yc       string position in y
                     float  angle    text angle, in degrees
                     float  chsiz    character expansion factor
                     int    ipos     centering parameter
                                     0) centred on (xc,yc)
                                     1) starts at (xc,yc)
                                     2) ends at (xc,yc)
                                     or centred on (xc,yc)
 
.COMMENTS    PCTEXT draws a text in the current active viewport.
             Coordinate refer to the current active system. The routine
             makes use of the character size as stored in the graphic
             keywords, which is multiplied by CHSIZ.
-------------------------------*/
#ifdef __STDC__
      void PCTEXT( char  *text, float xc, float yc, 
                   float angle, float chsiz, int ipos )
#else
      void PCTEXT( text, xc, yc, angle, chsiz, ipos )
      char  *text;
      int   ipos;
      float xc, yc, angle, chsiz; 
#endif
{
int   actvals;
float scale, tsize;
char  buff[81];

char *err_ipos = "*** WARNING: illegal positioning value given, label will be centred ",
     *fmt_chdi = "chdi=%-.3f,%-.3f";
/*
 * positioning of the string, in AGL code
 */
switch (ipos)
   { case 0:       /* centred */
         ipos = 0;
         break;
     case 1:       /* start at */
         ipos = 8;
         break;
     case 2:       /* ends at */
         ipos = 4;
         break;
     default:
         SCTPUT( err_ipos );
         ipos = 0;
         break;
  }

/*
 * determine the expansion factor for the characters
 */
(void) AG_RGET( "scale", &scale );
PCKRDR( "TSIZE", 1, &actvals, &tsize );
(void) sprintf( buff, fmt_chdi, scale * chsiz * tsize, scale * chsiz * tsize );
AG_SSET( buff );

/*
 * set text angle
 */
AG_SSET( "degr" );
(void) sprintf( buff, "chang=%-.3f", angle );
AG_SSET( buff );

/*
 * write the text
 */
AG_GTXT( (double) xc, (double) yc, text, ipos );

/*
 * reset
 */
AG_SSET( "LFRG" );
(void) sprintf( buff, fmt_chdi, scale * tsize, scale * tsize );
AG_SSET( buff );

return;
}
