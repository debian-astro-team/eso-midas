/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   PCTSET
.PURPOSE     determine global scaling for symbol and character sizes 
             to get the MIDAS layout
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    High level plot interface
.LANGUAGE    C
.COMMENTS    none
.ENVIRONment MIDAS and AGL
             #include <agl.h>           Prototypes for AGL application programs
             #include <midas_def.h>     Prototypes for MIDAS interfaces
             #include <plot_def.h>      Symbols used by the PLT interfaces

.VERSION     1.1     23-Apr-1994    made it a high level routine, RvH
             1.0     08-Sep-1993    created by R.M. van Hees

090422		last modif
------------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions in this module
 */
#include <stdio.h>

#include <midas_def.h>

/*
 * define some macros and constants
 */
#include <plot_def.h>

/*
 * here start the code of the function
 */
void PCTSET()
{
register int   xy;

int    actvals;
float  expand, ywid, yhgt, ssize, tsize, 
       devmm[2], dval[2], frmmm, nchdi[2], clpl[4];
char   text[81];

char   *fmt_sydi = "sydi=%-.3f;chdi=%-.3f,%-.3f";

/*
 * get the symbol and character dimensions, from the MIDAS keywords
 */
PCKRDR( "SSIZE", 1, &actvals, &ssize );
PCKRDR( "TSIZE", 1, &actvals, &tsize );

/*
 * set symbol and character dimension to "basis"
 */
AG_SSET( "scale=1.0" );
(void) sprintf( text, fmt_sydi, ssize, tsize, tsize );
AG_SSET( text );

/*
 * current clipping area limits
 */
(void) AG_RGET( "clpl", clpl );

/*
 * current device dimensions (cm)
 */
(void) AG_RGET( "DEVD", dval );
for ( xy = 0; xy < 2; xy++ ) devmm[xy] = dval[xy] * 10.0;

/*
 * wanted height (mm)
 */
(void) AG_RGET( "nchdi", nchdi );
ywid = nchdi[1] * devmm[1];

/*
 * The next formula gives nice results, we take the minimum of the
 * size of the frame (in mm). Divide it by 50 and do a NINT.
 * But due to roundoff error we can get nasty results if we make
 * multiple plots on one window/paper
 */
frmmm = MYMIN( *devmm * (clpl[1] - clpl[0]), devmm[1] * (clpl[3] - clpl[2]) );
frmmm = MYMIN( CGN_NINT( frmmm/50 ), CGN_NINT( (frmmm - 0.001)/50 ));
yhgt = (frmmm + 3) / 2.0;

/*
 * set expand factor
 */
expand = yhgt / ywid;
(void) sprintf( text, "scale=%-.3f", expand );
AG_SSET( text );
}

