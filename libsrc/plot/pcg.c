/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTifer   PCG
.AUTHOR      R.M. van Hees IPG-ESO Garching
.KEYWORDS    High level plot interface
.LANGUAGE    C
.COMMENTS    holds PCGCUR
.ENVIRONment MIDAS and AGL
             #include <midas_def.h>     Prototypes for MIDAS interfaces

.VERSION     1.0     23-Sep-1993   created by R.M. van Hees

 090422		last modif
----------------------------------------------------------*/
/*
 * Define _POSIX_SOURCE to indicate
 * that this is a POSIX program
 */
#define  _POSIX_SOURCE 1

/*
 * definition of the used functions
 */
#include <midas_def.h>

/*
 * define some macros and constants
 */
#define WARNING		1
#define KEY_ENTER       13                 /* AGL key code for the ENTER key */
#define KEY_EXIT       32                 /* AGL key code for the SPACE key */

/*++++++++++++++++++++++++++++++
.IDENTifer   PCGCUR
.PURPOSE     retrieve interactively positions from a graphic device
 in/output:  float *xval  input : required inital position in x, y
             float *yval  output: final locator position
    output:  int   *key   key code

.RETURNS     zero if the final position is within the clipping area

.COMMENTS    PCGCUR gets a graphic cursur and returns coordinates refering
             to the current active coordinate system. It gives an error
             message if the cursor is outside the clipping area.

             PCGCUR uses AGL routine AG_VLOC
-------------------------------*/
int  PCGCUR( xval, yval, key )
int *key;
float *xval, *yval; 

{
int    ierr, pixval;
float  xn, yn, clpl[4];

char *err_fram  = "*** WARNING: Graphic cursor outside plotted frame",
     *err_enter = "*** WARNING: do not use ENTER key";


ierr = ERR_NORMAL;
(void) AG_RGET( "clpl", clpl );

AG_VLOC( xval, yval, key, &pixval );
if (*key == KEY_ENTER)
   {
   ierr = WARNING;
   SCTPUT( err_enter );
   }

else if (*key != KEY_EXIT)
   {
   AG_VU2N( *xval, *yval, &xn, &yn );
   if ( xn < clpl[0] || xn > clpl[1] || yn < clpl[2] || yn > clpl[3] )
      {
      ierr = WARNING;
      SCTPUT( err_fram );
      }
   }
return ierr;
}
 
