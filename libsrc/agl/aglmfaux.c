/* @(#)aglmfaux.c	19.1 (ES0-DMD) 02/25/03 13:53:04 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)aglmfaux.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:04   */
/*
 * HEADER : aglmfaux.c     - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA
 */



#ifndef NO_METAFILE_SUPPORT


/*                       METAFILE MANAGEMENT UTILITIES                      */

#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*                         AGL_mopn (Internal AGL use)                        */

/* Open a metafile                                                           */

void AGL_mopn(name)
char *name;                               /* Metafile name [/A]              */
{
extern char *AG_SCAN();
char intnam[PHNAMLNG];
char *idx;
char flag='\0';
extern void AG_DMSG();

AGL_status.errstat = AGLNOERR;

AG_SCAN(name,'\0',PHNAMLNG,intnam);

idx=strrchr(intnam,'/');

if(idx != NULL) {
	char *id = idx+1;
	if(TOUPPER(*id)=='A') {
		id++;
		if(isspace(*id)||(~isprint(*id))) {
			flag = 'a';
			*idx = '\0';
		}
	}
}

if(flag != 'a') {
	DY(metafile) = fopen(intnam,B_CREATE);
	AG_DMSG("Mfile (open):",intnam);
} else {
	DY(metafile) = fopen(intnam,B_APPEND);
	AG_DMSG("Mfile (append):",intnam);
}

if(DY(metafile) != NULL) {
	DY(filact) = DY(metamode);
	if(flag != 'a') 
		fwrite(AGL_mfhead,sizeof(char),MFHDRLEN,DY(metafile));
} else
	AGL_status.errstat=MFOPENERR;				/* open error  */
}



/*****************************************************************************/
/*                         AGL_mput (Internal AGL use)                        */

/* Write a polyline body to the currently open metafile. Code must be put    */
/* into file before the call                                                 */

void AGL_mput(mfile,inpoly,pflag)
FILE *mfile;                             /* Metafile file pointer           */
struct polyline *inpoly;                 /* polyline structure              */
int pflag;				/* If TRUE, also put again first    */
					/* point after the last (to join as */
					/* a polygon)                       */
{
int np = inpoly->nvects;
int ic,i;
float *XP=inpoly->XV;
float *YP=inpoly->YV;

AGL_status.errstat=AGLNOERR;

if(pflag)
	np++;

ic = fwrite((void *)&np,sizeof(int),1,mfile);

for(i=0;i<np;i++) {
	float xxx[2];
	xxx[0] = *XP++;
	xxx[1] = *YP++;
	ic = fwrite((void *)xxx,sizeof(float),2,mfile);
}
if(pflag) {
	float xxx[2];
	xxx[0] = inpoly->XV[0];
	xxx[1] = inpoly->YV[0];
	ic = fwrite((void *)xxx,sizeof(float),2,mfile);
}
if(ic!=2)
	AGL_status.errstat=MFWRITERR;
}

#endif
