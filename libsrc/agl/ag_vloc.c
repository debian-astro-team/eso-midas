/* @(#)ag_vloc.c	19.1 (ES0-DMD) 02/25/03 13:53:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_vloc.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:02   */
/*
 * HEADER : ag_vloc.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                         AG_VLOC (C callable)                            */

/* AG_VLOC    Get locator position                                           */


/* Get the position from a locating device. The locating device use depends  */
/* on the particular hardware. It has usually the form of a cross which can  */
/* be interactively moved by a joy-stick, arrow keys and the like. The       */
/* routine returns when the needed key (again depending on the device) is    */
/* depressed. Starting and returned positions are referred to the current    */
/* graphic mode (either USER or NORMALIZED) and are mapped accordingly.      */

/* Cursor style (shape and/or dimension) may be varied via a suitable call   */
/* to AG_SSET, default style is 0, other styles may be optionally supported  */
/* by specific device drivers. If style is <0 then no cursor is displayed    */
/* and the only significant information returned by the routine is the key   */
/* code.                                                                     */

/* A warning status is set if the current locator position is outside the    */
/* currently active viewport                                                 */

/* An information status is set if the current locator position is outside   */
/* the clipping area and the current graphic mode is USER                    */


void AG_VLOC (xv,yv,key,pixval)

 float  *xv,*yv;              /* Input: required locator initial position.   */
                              /* Some devices may ignore this values (it is  */
                              /* an optional driver capability).             */
                              /* Return: final locator position.             */
                              /* Both values are referred to the current     */
                              /* coordinate system: USER or NORMALIZED.      */
 int  *key;                   /* Return: key code (device dependent).        */
 int  *pixval;                /* Pixel value (color). Some devices will      */
                              /* always return 0 (it is an optional driver   */
                              /* capability).                                */
                                                                         /*--*/
{
static char *modnam = "VLOC";
char aux;
extern void AG_DMSG();

AG_DMSG(modnam,(char *)0);

AGL_loc (xv,yv,1,&aux,pixval);
*key = aux;
if(AGL_status.errstat!=AGLNOERR) 
	AGL_siger(modnam);
}

