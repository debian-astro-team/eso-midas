/* @(#)ag_vlos.c	19.1 (ES0-DMD) 02/25/03 13:53:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_vlos.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:02   */
/*
 * HEADER : ag_vlos.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                         AG_VLOS (C callable)                            */

/* AG_VLOS    Get locator position. Also return a string                     */


/* Get the position from a locating device. This routine has a functionality */
/* very close to AG_VLOC. The only difference is that a sequence of chara-   */
/* ters may be returned to the caller into a string. The string input is     */
/* terminated when a "newline" or other control character is pressed. The    */
/* newline or control character is NOT included in the string, and a null    */
/* character is appended (when calling from C language routine).             */

/* NOTE: if the locating device is moved during interaction (between some    */
/*       keypresses) the position result is unpredictable.                   */


void AG_VLOS (xv,yv,maxlen,chstrg,pixval)

 float  *xv,*yv;              /* Input: required locator initial position.   */
                              /* Some devices may ignore this values (it is  */
                              /* an optional driver capability).             */
                              /* Return: final locator position.             */
                              /* Both values are referred to the current     */
                              /* coordinate system: USER or NORMALIZED       */
 int  maxlen;                 /* Maximum length of return string. If more    */
                              /* than maxlen characters are typed, they are  */
                              /* ignored. If the input is terminated due to  */
                              /* a control character, the string is null     */
                              /* terminated.                                 */
 char *chstrg;                /* Typed string. String length in the calling  */
                              /* program must be at least maxlen characters. */
 int  *pixval;                /* Pixel value (color). Some devices will      */
                              /* always return 0 (it is an optional driver   */
                              /* capability).                                */
                                                                         /*--*/
 {
  static char *modnam = "VLOS";
  extern void AG_DMSG();

  AG_DMSG(modnam,(char *)0);

  AGL_loc (xv,yv,maxlen,chstrg,pixval);
  if(AGL_status.errstat!=AGLNOERR) AGL_siger(modnam);
 }

