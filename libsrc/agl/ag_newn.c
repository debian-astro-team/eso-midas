/*===========================================================================
  Copyright (C) 1995-2012 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
 * HEADER : ag_newn.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *                           C INTERFACE MODULE
 *
 *
 * 120123	last modif
 */

#include <unistd.h>

#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*CC                    AG_NEWN (C callable)                                 */
 
/* AG_NEWN   Generates an unique (and new) file name based on a template     */


/* This routine generates a unique file name appending a number to a given   */
/* string. The generated name is tested against the current directory  con-  */
/* tent and the number is increased from zero up until a  unique name is     */
/* found.                                                                    */

/* This routine has been developed mainly to be used in user written device  */
/* drivers. It has no FORTRAN counterpart                                    */


void AG_NEWN (templa)

char *templa;                 /* Input: template string. The filename is     */
                              /* built by appending the character '.' and up */
                              /* to three decimal digits to it.              */
                              /* Output: The resulting string is stored into */
                              /* the same character array, so that it MUST   */
                              /* BE at least 4 characters longer than its    */
                              /* string content. If the filename cannot be   */
                              /* generated the routine returns a null string.*/
                                                                         /*--*/
{
char *pntr;
int nfile=0;

pntr = templa+strlen(templa);

do {
	sprintf(pntr,".%d",nfile);
	if(access(templa,0) != 0) 
		break;
	nfile++;
} while(nfile<=999);

if(nfile>999) {
	*templa = '\0';
	AG_DMSG("Filename","error");
}
}

