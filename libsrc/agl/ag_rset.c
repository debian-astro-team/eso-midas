/* @(#)ag_rset.c	19.1 (ES0-DMD) 02/25/03 13:53:01 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_rset.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:01   */
/*
 * HEADER : ag_rset.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>



/*****************************************************************************/
/*++                         AG_RSET (C callable)                            */

/* AG_RSET    Set float status parameters                                    */


/* Set up to ten (10) float values into the user defined parameters area.    */
/* This call may be used by applications in order to store numerical values  */
/* to be retrieved later on. These values remain associated with the current */
/* viewport and can be retrieved by  a call to AG_RGET (item USER)           */

void AG_RSET (number,values)

 int number;                  /* Number of items to store (n<=10)            */
 float values[];              /* Array of values to be stored                */
                              /* If n<=0 the parameter area is cleared       */
                                                                         /*--*/
{
static char *modnam = "RSET";
int i;

if(number<=0) {
	DY(n_float_user)=0;
	return;
}

if(number>10) {
	number=10;
	AGL_status.errstat=TOOMANYWNG;
	AGL_siger(modnam);
}
for(i=0;i<number;i++)
	DY(float_user)[i] = values[i];

DY(n_float_user)=number;
}

