/* @(#)ag_vupd.c	19.1 (ES0-DMD) 02/25/03 13:53:03 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_vupd.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:03   */
/*
 * HEADER : ag_vupd.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                             AG_VUPD                                     */

/* AG_VUPD    Force graphic update                                           */


/* Forcefully empty the graphic buffer. This operation is usually performed  */
/* in an automatic way by AGL, but the function is provided to cope with     */
/* special cases                                                             */


void AG_VUPD ()
                                                                         /*--*/
{
static char *modnam = "VUPD";
extern void AG_DMSG();

AG_DMSG(modnam,(char *)0);

AGL_vflsh();
if(AGL_status.errstat!=AGLNOERR) 
	AGL_siger(modnam); 
}

