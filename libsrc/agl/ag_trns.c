/* @(#)ag_trns.c	19.1 (ES0-DMD) 02/25/03 13:53:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_trns.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:02   */
/*
 * HEADER : ag_trns.c      - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                       AG_TRNS (C callable)                              */

/* AG_TRNS     Defines a User specified coordinate transformation            */

/* This call defines a User specified routine to be called by the AGL        */
/* internal coordinate transformation program when the USER graphic mode is  */
/* active. The call must also define an initialization routine.              */

/* Coordinate transformations are applied according to selected graphic mode */
/* in the following order:                                                   */

/* Graphic                                                                   */
/* Mode(s)		Transformation             Performed by              */

/* USER or SPECIAL      Logarithmic (when set)     AGL                       */
/* SPECIAL              User specified             user provided routines    */
/* USER or SPECIAL      User to Normalized         AGL                       */

/* The initialization routine will be called whenever a viewport switch is   */
/* performed. It is provided in order to allow the calling program to set    */
/* parameters needed by the transformation routine (e.g.: to modify scales   */
/* which may be different for different viewports).                          */

/* In order to allow for specific data items needed for initialization to be */
/* stored together with viewport, data buffers for up to 10 float and 10     */
/* integer are provided within the viewport data area. Data can be stored by */
/* means of a call to AG_RSET and AG_ISET when the transformation is set up. */
/* those values can be retrieved to be used by the initialization routine by */
/* calling AG_RGET("user") and AG_IGET("user").                              */

/* The routines specified in the call must be defined as follow:             */

/*        void my_init()                                                     */
/*        int  my_transf(double *c0, double *c1)  Direct transformation      */
/*        int  my_rtransf(double *c0, double *c1) Reverse transformation     */

/* The direct and reverse transformation routines will return an integer     */
/* value which encode the point position with respect to the user data       */
/* window (the user data window is assumed to be rectangular in the space    */
/* of the coordinates defined by the transformation) as shown in the         */
/* following scheme:                                                         */

/*                  c0low      c0up                                          */
/*                    |          |                                           */
/*            ret = 9 |  ret = 8 |  ret = 10                                 */
/*     c1up ----------+----------+----------                                 */
/*            ret = 1 |  ret = 0 |  ret = 2                                  */
/*     c1low----------+----------+----------                                 */
/*            ret = 5 |  ret = 4 |  ret = 6                                  */
/*                    |          |                                           */

/* The retuned value on error must be (-1)                                   */

/* To reset the transformation you may call: AG_TRNS(NULL,NULL,NULL)         */

/* When data are stored into metafiles the user transformation information   */
/* is lost, so that redrawing will not be performed correctly (unless the    */
/* HARD metafile mode is used).                                              */

/* Note: this routine is callable through the C interface only               */


void AG_TRNS (my_init,my_transf,my_rtransf)

void (*my_init)();                 /* User initialization routine entry      */
int  (*my_transf)();               /* User transformation routine entry      */
int  (*my_rtransf)();              /* User inverse transformation entry      */
                                                                         /*--*/
{
DY(UsrInit) = my_init;
DY(UsrTransf) = my_transf;
DY(UsrRTransf) = my_rtransf;

DY(modeflag) = SPECIAL;
AGL_sstat();
}

