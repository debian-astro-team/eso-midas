/* @(#)aglddef.c	19.1 (ES0-DMD) 02/25/03 13:53:04 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)aglddef.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:04   */
/*
 * HEADER : aglddef.c      - Vers 3.6.000  - Dec 1991 -  L. Fini, Oss. Arcetri
 *
 *
 *  Supported driver definitions
 *
 *  An entry of the type #include "<driver>def.c" must be included here in
 *  order to get address table definitions for the given driver
 *  The conditional compilation symbol is used by the makefile to select
 *  drivers to include.
 */

#ifdef VT125DRV
static void vt125def() 
#include      "vt125def.c"
#endif

#ifdef IBMPCDRV
static void ibmpcdef() 
#include      "ibmpcdef.c"
#endif

#ifdef WINDDRV
static void winddef()
#include      "winddef.c"
#endif

#ifdef AOWDRV
static void aowdef()
#include      "aowdef.c"
#endif

#ifdef NULLDRV
static void nulldef()
#include      "nulldef.c" 
#endif

#ifdef HPGLDRV
static void hpgldef()
#include      "hpgldef.c"
#endif

#ifdef PSCRDRV
static void pscrdef()
#include      "pscrdef.c"
#endif

#ifdef RASTDRV
static void rastdef()
#include      "rastdef.c"
#endif

#ifdef TEKDRV
static void tekdef()
#include      "tekdef.c"
#endif

#ifdef IDIDRV
static void ididef()
#include      "ididef.c"
#endif

#ifdef X11DRV
static void x11def()
#include      "x11def.c"
#endif

static void drvinit()
{
AGL_status.ndevs=0;

#ifdef VT125DRV
vt125def();
#endif

#ifdef IBMPCDRV
ibmpcdef();
#endif

#ifdef WINDDRV
winddef();
#endif

#ifdef AOWDRV
aowdef();
#endif

#ifdef NULLDRV
nulldef(); 
#endif

#ifdef HPGLDRV
hpgldef();
#endif

#ifdef PSCRDRV
pscrdef();
#endif

#ifdef RASTDRV
rastdef();
#endif

#ifdef TEKDRV
tekdef();
#endif

#ifdef IDIDRV
ididef();
#endif

#ifdef X11DRV
x11def();
#endif
}

