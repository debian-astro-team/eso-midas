/* @(#)nulldrv.c	19.1 (ES0-DMD) 02/25/03 13:53:07 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)nulldrv.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:07   */
/*
 * HEADER : nulldrv.c      - Vers 3.6.002  - Oct 1993 -  L. Fini, OAA
 *                         - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 */



/*****************************************************************************/

/*  AGL null driver                                                          */

/*                                                                           */
/*  The following entry points have been defined:                            */

/*  Function    Entry point                                                  */
/*                                                                           */
/*  Initialize  AGLINNUL                                                     */
/*  Cursor en.  AGLCUNUL                                                     */
/*  Erase       AGLERNUL                                                     */
/*  Escape      AGLESNUL                                                     */
/*  Draw        AGLPLNUL                                                     */
/*  Finish      AGLTENUL                                                     */
/*  Flush buff. AGLSENUL                                                     */

#include <aglsys.h>

#include <aglerror.h>
#include <agldcom.h>
#include <agldcaps.h>

#define VERSCODE 36			/* Declare driver version            */

#define DEFRATIO 691		/* Default aspect Ratio*1000                 */
#define XDIM     27.5		/* Default X length (cm)                     */
#define NLPIXCM  100.  		/* Fake resolution                           */
#define NLCHARF  3.819 

#define NLFLAGS  SEPALPHA | EXECOMMND | SETBACKGR | PARTERASE

#define NLCOLORS        7
#define NLBLACK         0       /* Bot index and grayscale value          */
#define NLWHITE         1       /* Bot index and grayscale value          */
#define NLRED           2
#define NLGREEN         3
#define NLBLUE          4
#define NLYELLOW        5
#define NLMAGENTA       6
#define NLCYAN          7


/*****************************************************************************/
/*                          CURSOR READ function                             */

void AGLCUNUL (AGLDVCOM)                  /* Read virtual cursor position  */
struct bufcom *AGLDVCOM;
{
ERRCODE=AGLNOERR;
}



/*************************************************************************   */
/*                          DEVICE INIT function                             */

void AGLINNUL (AGLDVCOM)
struct bufcom *AGLDVCOM;
{
static int ARatio;
char *pt;

ERRCODE=AGLNOERR;

switch((int)IBUFFR(0)) {
case 0:
	ARatio=DEFRATIO;
	pt=CHARBUF;
	while(*pt++);                   /* Get  SYSAUX info          */
	if(*pt!='\0') 
		ARatio=atoi(pt);
	while(*pt++);                   /* Get USRAUX string          */
	if(*pt!='\0') 
		ARatio=atoi(pt);
	break;

case 1:                          /* Pass back device characteristics  */

	*(CHARBUF) = '\0';

	RBUFFR(0) = XDIM;
	RBUFFR(1) = XDIM*(double)ARatio*0.001;

	IBUFFR(1) = NLFLAGS;
	IBUFFR(2) = NLCOLORS;
	IBUFFR(3) = RBUFFR(0)*NLPIXCM;
	IBUFFR(4) = RBUFFR(1)*NLPIXCM;
	IBUFFR(5) = VERSCODE;
	IBUFFR(6) = 10;
	IBUFFR(7) = NLWHITE;    /* Default Background */
	IBUFFR(8) = NLBLACK;    /* Default foreground */
	IBUFFR(9) = NLBLACK;
	IBUFFR(10) = NLRED;
	IBUFFR(11) = NLGREEN;
	IBUFFR(12) = NLBLUE;
	IBUFFR(13) = NLYELLOW;
	IBUFFR(14) = NLMAGENTA;
	IBUFFR(15) = NLCYAN;
	IBUFFR(16) = NLWHITE;

	RBUFFR(2) = NLCHARF;
	RBUFFR(3) = 0.0;
	RBUFFR(4) = 0.0;
	RBUFFR(5) = 0.0;
	RBUFFR(6) = RBUFFR(0);
	RBUFFR(7) = RBUFFR(1);
	RBUFFR(8) = RBUFFR(0);
	RBUFFR(9) = RBUFFR(1);
	break;
default:
	*(CHARBUF) = '\0';
	ERRCODE=AGLNOERR;
	return;
}
}


/*****************************************************************************/
/*                          ERASE SCREEN function                            */

void AGLERNUL (AGLDVCOM)
struct bufcom *AGLDVCOM;
{
ERRCODE=AGLNOERR;
}



/*************************************************************************   */
/*                          ESCAPE function                                  */

void AGLESNUL (AGLDVCOM)
struct bufcom *AGLDVCOM;
{
ERRCODE=UNSFEATINF;
}


/*****************************************************************************/
/*                            BUFFER SEND function                           */

void AGLSENUL (AGLDVCOM)
struct bufcom *AGLDVCOM;
{
ERRCODE = AGLNOERR;
}


/*************************************************************************   */
/*                          TERMINATE Function                               */

void AGLTENUL (AGLDVCOM)
struct bufcom *AGLDVCOM;
{
ERRCODE=AGLNOERR;
}


/*****************************************************************************/
/*                          DRAW function                                    */

void AGLPLNUL (AGLDVCOM)
struct bufcom *AGLDVCOM;
{
ERRCODE=AGLNOERR;
}

