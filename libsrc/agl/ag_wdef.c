/* @(#)ag_wdef.c	19.1 (ES0-DMD) 02/25/03 13:53:03 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_wdef.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:03   */
/*
 * HEADER : ag_wdef.c      - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                          AG_WDEF (C callable)                           */

/* AG_WDEF    Define user window                                             */


/* User window definition. Sometimes referred to as "world coordinates" defi-*/
/* ne the user data coordinate systems. User data in the given rectangular   */
/* range are mapped onto the current clipping area (see AG_CDEF). After the  */
/* call the mode is set to USER.                                             */

/* User data transformation may be controlled in various ways (see AG_SSET,  */
/* items: NORMAL, USER, LOGX, LOGY, LINX, LINY, GEOM, NGEOM).                */

void AG_WDEF (x1,x2,y1,y2)

 double x1,x2,y1,y2;          /* Window bounds                               */
                                                                         /*--*/ 
{
static char *modnam = "WDEF";
float xxx[4];
double dx,dy;
int is;

AG_DMSG(modnam,(char *)0);

xxx[0]=x1; xxx[1]=x2; xxx[2]=y1; xxx[3]=y2;

			/* User window data are stored in global variables:  */
			/*     XUSUP, XUSLOW, YUSUP, YUSLOW                  */

#ifndef NO_METAFILE_SUPPORT
if ( DY(filact) == SOFTMETAFILE ) {		/* store data into metafile  */
	enum METACODE cod = MFWDEF;
	is = fwrite((char *)&cod,sizeof(int),1,DY(metafile));
	is = fwrite((char *)xxx,sizeof(float),4,DY(metafile));
	if(is != 4) 
		AGL_puterr(MFWRITERR,modnam);
}
#endif

if(AGL_status.curvwp == VWPEMPTY) {		/* Go on only if viewp.active */
	AGL_puterr(NOVWPERR,modnam); 
	return;
}
dx = fabs((double)(x2-x1));
dy = fabs((double)(y2-y1));

if((dx <= 0.0)||(dy <= 0.0)) { 			/* Illegal window bounds  */
	AGL_puterr(ILLWNDERR,modnam); 
	return;
}

XUSLOW = x1;                              /* Set window bounds             */
XUSUP = x2; 
YUSUP = y2;
YUSLOW = y1;

DY(window) = TRUE;                        /* Set window flag               */
DY(modeflag) = USER;                      /* Set mode flag to USER         */

AGL_sgbl();                                /* Check and set the status      */
if(AGL_status.errstat!=AGLNOERR) 
	AGL_siger(modnam);
}



