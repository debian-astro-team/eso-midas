/* @(#)ag_gets.c	19.1 (ES0-DMD) 02/25/03 13:52:58 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_gets.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:58   */
/*
 * HEADER : ag_gets.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*CC                            AG_GETS (C callable)                         */

/* AG_GETS    Gets next line from an open file                               */


/* Gets next line from an open file discarding comments, leading blanks,     */
/* blank lines, and so on. Comments are (parts of) lines starting with #.    */
/* # chars not intended to be start of comments can be escaped with another  */
/* # character.                                                              */

/* Note: this routine is callable through the C interface only               */


char *AG_GETS (buffr,len,filpt)
                              /* Returns pointer to first character, NULL at */
                              /* eof and for input errors.                   */

 char *buffr;                 /* User provided buffer to hold input lines.   */
                              /* It must be longer than the longest input    */
                              /* line (including comments).                  */
 int  len;                    /* User  buffer length                         */
 FILE *filpt;                 /* File pointer  to read from                  */
                                                                         /*--*/
{
extern char *AG_GETN();
int nlin;
return AG_GETN(buffr,len,filpt,&nlin);
}

