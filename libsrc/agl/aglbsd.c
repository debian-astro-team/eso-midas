/* @(#)aglbsd.c	19.1 (ES0-DMD) 02/25/03 13:53:03 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)aglbsd.c	19.1 (OAA-ASTRONET) 02/25/03 13:53:03 */
/*
 * HEADER : aglbsd.c       - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *                                                       C. Guirao, ESO-IPG
 *
 * This file contains system dependent features which are common to 
 * Unix BSD systems
 *
 * CG: Modifications to do it really BSD system.
 * LF: Bug fixes to modifications (hope it is still BSD !)
 */


/***************************************************************************/
/*                      AGL_setraw  AGL_reset                              */

/* This routines are used to set the specified device handler in RAW mode  */
/* and to reset it in COOKED mode thereafter                               */

static struct sgttyb argp;

static unsigned long flags;

void AGL_setraw (filpt)
FILE * filpt;
{
int handler = fileno(filpt);
ioctl(handler,TIOCGETP,&argp);		/* Get current terminal status  */
flags = argp.sg_flags;			/* Save flags */
argp.sg_flags = (flags & (~ECHO)) | RAW;	/* LF 920117            */
ioctl(handler,TIOCSETP,&argp);		/* Force RAW mode               */
}

void AGL_reset(filpt)
FILE * filpt;
{
int handler = fileno(filpt);
argp.sg_flags = flags;
ioctl(handler,TIOCSETP,&argp);  /* Reset terminal status         */
}
