/* @(#)ag_vsel.c	19.1 (ES0-DMD) 02/25/03 13:53:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_vsel.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:02   */
/*
 * HEADER : ag_vsel.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                            AG_VSEL (C callable)                         */

/* AG_VSEL    Select viewport                                                */


/* Select a previously saved viewport after saving the current one. The      */
/* specified viewport must have been previously defined and saved (see also: */
/* AG_VDEF).                                                                 */

void AG_VSEL (id)

 int id;                      /* Viewport identifier as returned by a pre-   */
                              /* vious call to AG_VDEF.                      */
                                                                         /*--*/
{
static char *modnam = "VSEL";

AG_DMSG(modnam,(char *)0);

AGL_vflsh();			/* Empty current viewport buffer         */

if ( (id<0 || id>=MAXVWPTS) ||
       (AGL_status.vwpts[id] == NULL) )       { 
	AGL_puterr(VWPSELERR,modnam); 
	return; 
}
AGL_status.vwpoint = AGL_status.vwpts[id];
AGL_status.curvwp = id;
AGL_status.devid = DY(devidx);
AGL_sdrv();
AGL_sclr();
AGL_swdt(DY(lwidth));
AGL_sstyl(DOTLEN(AGL_status.devid));
if(DY(UsrInit) != NULL)
	(*DY(UsrInit))();
AGL_sstat();
}

