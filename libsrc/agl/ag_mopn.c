/* @(#)ag_mopn.c	19.1 (ES0-DMD) 02/25/03 13:53:00 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_mopn.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:00   */
/*
 * HEADER : ag_mopn.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


#ifndef NO_METAFILE_SUPPORT

/*****************************************************************************/
/*++                          AG_MOPN (C callable)                           */

/* AG_MOPN    Open metafile                                                  */


/* Open an output metafile associated to the current viewport. If a metafile */
/* is already open a warning is issued and the metafile is not opened. After */
/* opening the metafile is active. If a metafile with the same name already  */
/* exists on the designated path, the result will depend on system standard  */
/* action in similar cases.                                                  */
/* The metafile name can optionally be flagged with /a in which case the     */
/* metafile will be opened in append mode (if a file of the given name       */
/* already exists in the designated path).                                   */
 
/* In order to allow full path specification on Unix, the /A flag must be    */
/* appended as such to the end of the file name. E.g.: the name "/meta/a"    */
/* will use the name "/meta" as metafile name and open it in append mode,    */
/* the name "/meta/append" will open the file "/meta/append". Note that it   */
/* is not possible to open a metafile named "anything/a" because the last    */
/* part of the name will be interpreted as append mode flag.                 */

/* Metafile recording has two different modes: in SOFT mode the records      */
/* stored into the metafile have a one to one correspondance to the AG_      */
/* routines calls (e.g. texts are stored as character strings, polylines     */
/* vectors generated when in USER mode are stored in world coordinates, and  */
/* so on). In HARD mode only the NORMALIZED coordinates polylines resulting  */
/* from any kind of operation are stored (E.g. text will be stored as the    */
/* generated sequence of polylines).                                         */

/* The SOFT mode is usually more efficient and compact and suitable for      */
/* storing data for subsequent redrawing, while HARD mode allows to redraw   */
/* stored drawings into differently defined viewports (e.g. to produce hard  */
/* copies of different dimensions and so on).                                */

/* Metafile recording mode can be set by a call to AG_SSET with keywords     */
/* MFHARD and MFSOFT. SOFT mode is the default.                              */


void AG_MOPN (fname)

char *fname;                  /* Metafile name (see above)                   */
                                                                         /*--*/
{
static char *modnam = "MOPN";
extern void AG_DMSG();

AG_DMSG(modnam,(char *)0);

if (AGL_status.curvwp == VWPEMPTY) {		/* Test if viewp. active   */
	AGL_puterr(NOVWPERR,modnam); 
	return;
}

if (DY(metafile) != NULL) {			/* Test if already open    */
	AGL_puterr(MFACTVWNG,modnam); 
	return;
}

AGL_mopn(fname);

if ( AGL_status.errstat != AGLNOERR ) 
	AGL_siger(modnam);
}


#else

void AG_MOPN (fname) char *fname; {}

#endif
