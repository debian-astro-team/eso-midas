#
# HEADER : makefile.mid   - Vers 3.6.000  - Jul 1990 -  R.H. Warmels, ESO-IPG
#                                         - Aug 1990 -  L. Fini, OAA
#                                         - Dec 1990 -  C. Guirao ESO-IPG
#					  - Jan 1992 -  L. Fini, OAA
#					  - Jan 1993 -  C. Guirao ESO-IPG
#					  - Mar 1993 -  C. Guirao ESO-IPG
#
# AGL makefile for the Midas environment
#
# - Copy all source files onto              /midas/.../agl 
# - Copy the content of ./include onto      /midas/.../incl
# - Copy the content of ./doc onto          /midas/.../doc/agl
# - Copy the content of ./tables onto       /midas/.../tables/ascii/plot
#
# Then cd to Midas agl directory
#
# 1. make  clean                      - Clean the directory
#
# 2. make                             - Build the library and test programs
#
#
# Check the following symbols for proper definitions
#
# 060328	last modif


include ../../local/default.mk

CFLAGS += $(C_OPT) $(DEBUG) $(SH_OPT) $(OSSYS) $(SYS) -I$(INC) $(X11INC) -DMIDAS
FFLAGS += $(SH_OPT)

LIB = $(LIBDIR)/libagl3.a
M = ../../system/exec

DRIVERS = -DNULLDRV -DPSCRDRV -DHPGLDRV -DTEKDRV -DRASTDRV \
          -DIDIDRV

LIBS  = $(LIBDIR)/libagl3.a \
	$(LIBDIR)/libidicl.a \
	$(LIBDIR)/oslib.a 
LLIB =  -L$(LIBDIR) -lagl3 -lidicl -los

# VERSATEC =  $(M)/versatec.exe $(M)/sendv80.exe

SYS_DEP_C= aglunix.c aglbsd.c aglsysv.c

DRIVERS_O = hpgldrv.o nulldrv.o pscrdrv.o rastdrv.o tekdrv.o  

WINDDRV_O = ididrv.o 
    
CI0_O = ag_cdef.o ag_cls.o  ag_iset.o ag_rset.o ag_sset.o \
        ag_vdef.o ag_vkil.o ag_vsel.o ag_wdef.o

CI1_O = ag_gplg.o ag_gpll.o ag_gplm.o ag_gtxt.o ag_gint.o \
        ag_vers.o ag_vupd.o

CI2_O = ag_idn.o  ag_iget.o ag_rget.o ag_tget.o ag_vloc.o ag_vlos.o

CI3_O = ag_mcls.o ag_mopn.o ag_mrdw.o ag_mres.o ag_msus.o

CI4_O = ag_driv.o ag_esc.o  ag_gerr.o ag_ival.o ag_magn.o \
        ag_rval.o ag_scan.o ag_sval.o ag_trot.o \
        ag_trns.o ag_vn2u.o ag_vu2n.o

CI5_O = ag_dmsg.o ag_getn.o ag_gets.o ag_newn.o ag_stdo.o
HAG_O= ag_axes.o ag_axis.o ag_fill.o ag_hist.o ag_nlin.o ag_orax.o


AGL0_O = aglinit.o aglerror.o aglsys.o aglutil.o aglstat.o aglset.o
AGL1_O = aglpoly.o agldash.o aglgraph.o aglchar.o
AGL2_O = aglloc.o aglget.o
AGL3_O = aglmfaux.o

HAG0_O = aglaxutl.o

FINTF_O =  fintf.o
YFINTF_O =  yagl.o

SOURCES = $(HAG_S) $(CI0_S) $(CI1_S) $(CI2_S) $(CI3_S) $(CI4_S) $(CI5_S) \
          $(HAG0_S) $(AGL0_S) $(AGL1_S) $(AGL2_S) $(AGL3_S)

SYS_DEP_H= $(INC)/aglsys.h $(INC)/aglunix.h \
           $(INC)/aglbsd.h $(INC)/aglsysv.h \
	   $(INC)/aglvms.h 

HFILES= $(INC)/agl.h $(INC)/aglenvr.h $(INC)/aglproto.h \
        $(INC)/aglerror.h $(INC)/agldcaps.h $(INC)/agldcom.h \
        $(INC)/aglstruc.h $(INC)/aglmacro.h $(INC)/aglparam.h \
        $(INC)/osdefos.h $(INC)/aglstat.h $(SYS_DEP_H)

OBJ = $(CI0_O) $(CI1_O) $(CI2_O) $(CI3_O) $(CI4_O) $(CI5_O) $(HAG_O)\
      $(AGL0_O) $(AGL1_O) $(AGL2_O) $(AGL3_O) $(HAG0_O)\
      $(DRIVERS_O) $(WINDDRV_O) $(FINTF_O) $(YFINTF_O)

#all: $(MAKEFILE_VMS) $(LIB) $(VERSATEC)
all: $(MAKEFILE_VMS) $(LIB)

$(MAKEFILE_VMS): makefile
	$(MAKE_VMS)

aglsys.c: $(SYS_DEP_C)

ag_axes.o : ag_axes.c $(HFILES)
ag_axis.o : ag_axis.c $(HFILES)
ag_orax.o : ag_orax.c $(HFILES)
ag_cdef.o : ag_cdef.c $(HFILES)
ag_cls.o  : ag_cls.c $(HFILES)
ag_dmsg.o : ag_dmsg.c $(HFILES)
ag_driv.o : ag_driv.c $(HFILES)
ag_esc.o  : ag_esc.c $(HFILES)
ag_fill.o : ag_fill.c $(HFILES)
ag_gerr.o : ag_gerr.c $(HFILES)
ag_getn.o : ag_getn.c $(HFILES)
ag_gets.o : ag_gets.c $(HFILES)
ag_gint.o : ag_gint.c $(HFILES)
ag_gplg.o : ag_gplg.c $(HFILES)
ag_gpll.o : ag_gpll.c $(HFILES)
ag_gplm.o : ag_gplm.c $(HFILES)
ag_gtxt.o : ag_gtxt.c $(HFILES)
ag_hist.o : ag_hist.c $(HFILES)
ag_idn.o  : ag_idn.c $(HFILES)
ag_iget.o : ag_iget.c $(HFILES)
ag_iset.o : ag_iset.c $(HFILES)
ag_ival.o : ag_ival.c $(HFILES)
ag_mcls.o : ag_mcls.c $(HFILES)
ag_mopn.o : ag_mopn.c $(HFILES)
ag_mrdw.o : ag_mrdw.c $(HFILES)
ag_mres.o : ag_mres.c $(HFILES)
ag_msus.o : ag_msus.c $(HFILES)
ag_newn.o : ag_newn.c $(HFILES)
ag_nlin.o : ag_nlin.c $(HFILES)
ag_rget.o : ag_rget.c $(HFILES)
ag_rset.o : ag_rset.c $(HFILES)
ag_rval.o : ag_rval.c $(HFILES)
ag_scan.o : ag_scan.c $(HFILES)
ag_sset.o : ag_sset.c $(HFILES)
ag_stdo.o : ag_stdo.c $(HFILES)
ag_sval.o : ag_sval.c $(HFILES)
ag_tget.o : ag_tget.c $(HFILES)
ag_trns.o : ag_trns.c $(HFILES)
ag_vdef.o : ag_vdef.c $(HFILES)
ag_vers.o : ag_vers.c $(HFILES)
ag_vkil.o : ag_vkil.c $(HFILES)
ag_vloc.o : ag_vloc.c $(HFILES)
ag_vlos.o : ag_vlos.c $(HFILES)
ag_vn2u.o : ag_vn2u.c $(HFILES)
ag_vsel.o : ag_vsel.c $(HFILES)
ag_vu2n.o : ag_vu2n.c $(HFILES)
ag_vupd.o : ag_vupd.c $(HFILES)
ag_wdef.o : ag_wdef.c $(HFILES)

aglinit.o:  aglinit.c $(HFILES)
	$(CC) $(CPPFLAGS) $(CFLAGS) $(WINDOW) $(DRIVERS) $(CLINK) aglinit.c

aglchar.o:  aglchar.c aglfonts.c $(HFILES)
agldash.o:  agldash.c  $(HFILES)
aglerror.o: aglerror.c $(HFILES)
aglget.o:   aglget.c   $(HFILES)
aglgraph.o: aglgraph.c $(HFILES)
aglloc.o:   aglloc.c   $(HFILES)
aglmfaux.o: aglmfaux.c $(HFILES)
aglpoly.o:  aglpoly.c  $(HFILES)
aglstat.o:  aglstat.c  $(HFILES)
aglset.o:   aglset.c   $(HFILES)
aglsys.o:   aglsys.c   $(HFILES)
aglutil.o:  aglutil.c  $(HFILES)




# versatec.o sendv80.o plotv80: $(INC)/versatec.h $(INC)/aglvcmd.h

#vcmd.h: 
#	@if [ -f /usr/include/sys/vcmd.h ]; then \
#		ln -s /usr/include/sys/vcmd.h; \
#	else    ln -s $(INC)/aglvcmd.h vcmd.h; fi

$(LIB): $(OBJ) 
	$(AR) $(AR_OPT) $(LIB) $(CI0_O) 
	$(AR) $(AR_OPT) $(LIB) $(CI1_O) 
	$(AR) $(AR_OPT) $(LIB) $(CI2_O) 
	$(AR) $(AR_OPT) $(LIB) $(CI3_O) 
	$(AR) $(AR_OPT) $(LIB) $(CI4_O) 
	$(AR) $(AR_OPT) $(LIB) $(CI5_O) 
	$(AR) $(AR_OPT) $(LIB) $(HAG_O) 
	$(AR) $(AR_OPT) $(LIB) $(AGL0_O) 
	$(AR) $(AR_OPT) $(LIB) $(AGL1_O) 
	$(AR) $(AR_OPT) $(LIB) $(AGL2_O) 
	$(AR) $(AR_OPT) $(LIB) $(AGL3_O) 
	$(AR) $(AR_OPT) $(LIB) $(HAG0_O) 
	$(AR) $(AR_OPT) $(LIB) $(DRIVERS_O) 
	$(AR) $(AR_OPT) $(LIB) $(WINDDRV_O) 
	$(AR) $(AR_OPT) $(LIB) $(FINTF_O) 
	$(AR) $(AR_OPT) $(LIB) $(YFINTF_O) 
	$(RANLIB) $(LIB)

# $(M)/versatec.exe:: versatec.o
# 	$(LDCC) versatec.o $(MLIB) $(SLIB) -o $@
# 	$(STRIP) $@

# $(M)/sendv80.exe: sendv80.o
# 	$(LDCC) sendv80.o $(MLIB) $(SLIB) -o $@
# 	$(STRIP) $@

# $(M)/plotv80.exe: plotv80.o
# 	$(LDCC) plotv80.o $(MLIB) $(SLIB) -o $@
# 	$(STRIP) $@

$(M)/ascii2mf.exe: ascii2mf.o $(LIBS)
	$(LDCC) ascii2mf.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

$(M)/mf2ascii.exe: mf2ascii.o $(LIBS)
	$(LDCC) mf2ascii.o $(LLIB) $(MLIB) $(SLIB) -o $@
	$(STRIP) $@

clean:
	rm -f *.o
	rm -f *.f
	rm -f fintf.c

clean_exec:
	rm -f $(VERSATEC)
