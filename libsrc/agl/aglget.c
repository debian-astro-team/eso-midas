/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
 * HEADER : aglget.c       - Vers 3.6.002  - Oct 1993 -  L. Fini, OAA
 *                         - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA
.VERSION

 090630		last modif
 */



/*                          GET parameter routines                         */





#include <aglenvr.h>
#include <aglstat.h>



/*****************************************************************************/
/*                            aglills (Internal use)                         */

/* Set AGL error code ILLCODWNG                                              */


static int aglills()
{
AGL_status.errstat=ILLCODWNG; 
return 0;
}



/*****************************************************************************/
/*                            aglgetfA (Internal use)                        */

/* Get float parameters with name "A...."                                    */

static int aglgetfA(pntr,fval)
char *pntr;
float fval[];              /* return values array                           */
{
int n=-1;                    /* Number of returned values                     */


switch(*pntr++) {

case 's':
	if(AGL_status.curvwp==VWPEMPTY) {
		AGL_status.errstat=NOVWPERR;
		n = 0;
	} else {
		fval[0]=RATYX(AGL_status.devid);
		n = 1;
	}
	break;
case 'n':
	fval[0]=DY(angfct);
	n = 1;
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}

return n;
}


/*****************************************************************************/
/*                            aglgetfC (Internal use)                        */

/* Get float parameters with name "C...."                                    */

static int aglgetfC(pntr,fval)
char *pntr;                /* pointer to the keyword (after the C)           */
float fval[];              /* return values array                            */
{
int n=0;

switch(*pntr++) {
double hd,vd;

case 'l':		 		/* keywd="CL....."   */
	pntr++;
	switch(*pntr++) {
	case 'a':		/* keywd="CLPA.."    */
		fval[0]=YXRATIO;
		n = 1;
		break;
	case 'l':		/* keywd="CLPL..."   */
		fval[0] = XCLOW;
		fval[1] = XCLUP;
		fval[2] = YCLOW;
		fval[3] = YCLUP;
		n = 4;
		break;
	default: AGL_status.errstat=ILLCODWNG; break;
	}
	break;
case 'h':					/* keywd="CH...."    */
	switch(*pntr) {
	case 'd':				/* keywd="CHD..."    */
	case 'n':				/* keywd="CHN..."    */
		AGL_strd("X",DY(curfont),&hd,&vd);

		fval[0]=fval[1] = DY(scale)* CHARMULT(AGL_status.devid);
		fval[0] *= hd*DY(chrhsize)*RSLX(AGL_status.devid);
		fval[1] *= vd*DY(chrvsize)*RSLY(AGL_status.devid);
		n = 2;
		if(*pntr=='n')
			break;
						/* keywd="CHD..."    */
		fval[0] *= XCURL(AGL_status.devid);
		fval[1] *= YCURL(AGL_status.devid);
		break;
	default: 
		AGL_status.errstat=ILLCODWNG; 
		break;
	}
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}




/*****************************************************************************/
/*                            aglgetfD (Internal use)                        */

/* Get float parameters with name "D...."                                    */

static int aglgetfD(pntr,fval)
char *pntr;                /* pointer to the keyword (after the D)          */
float fval[];              /* return values array                           */
{
int n=0;

switch(*pntr++) {

case 'e':                                /* keywd="DE...."           */
	fval[0]=XCURL(AGL_status.devid);
	fval[1]=YCURL(AGL_status.devid);
	n = 2;
	break;

default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}



/*****************************************************************************/
/*                            aglgetfN (Internal use)                        */

/* Get float parameters with name "N...."                                    */

static int aglgetfN(pntr,fval)
char *pntr;                /* pointer to the keyword (after the N)          */
float fval[];              /* return values array                           */
{
int n=0;

switch(*pntr++) {
	double hd,vd;
case 'c':                                /* keywd="NC...."           */
	AGL_strd("X",DY(curfont),&hd,&vd);
	fval[0] = fval[1] = DY(scale)*CHARMULT(AGL_status.devid);
	fval[0] *=  hd*RSLX(AGL_status.devid);
	fval[1] *=  vd*RSLY(AGL_status.devid);
	n = 2;
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}



/*****************************************************************************/
/*                            aglgetfR (Internal use)                        */

/* Get float parameters with name "R...."                                    */

static int aglgetfR(pntr,fval)
char *pntr;
float fval[];              /* return values array                           */
{
int n=0;
switch(*pntr++) {

case 'e':
	n = 2;
	fval[0] = RSLX(AGL_status.devid);
	fval[1] = RSLY(AGL_status.devid);
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}




/*****************************************************************************/
/*                            aglgetfS (Internal use)                        */

/* Get float parameters with name "S...."                                    */

static int aglgetfS(pntr,fval)
char *pntr;                  /* pointer to the keyword (after the V)          */
float fval[];              /* return values array                           */
{
int n=0;

switch(*pntr++) {

case 'c':                                 /* keywd="SC...."          */
	fval[0] = DY(scale);
	n = 1;
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}




/*****************************************************************************/
/*                            aglgetfV (Internal use)                        */

/* Get float parameters with name "V...."                                    */

static int aglgetfV(pntr,fval)
char *pntr;                  /* pointer to the keyword (after the V)          */
float fval[];              /* return values array                           */
{
int n=0;

switch(*pntr++) {

case 'w':                                 /* keywd="VW...."          */
	fval[0] = XVWLOW;
	fval[1] = XVWUP;
	fval[2] = YVWLOW;
	fval[3] = YVWUP;
	n = 4;
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}




/*****************************************************************************/
/*                            aglgetfU (Internal use)                        */

/* Get float parameters with name "U...."                                    */

static int aglgetfU(pntr,fval)
char *pntr;			/* pointer to the keyword (after the U)      */
float fval[];			/* return values array                       */
{
int n=0;
int i;

switch(*pntr++) {
case 's':
	n = DY(n_float_user);
	for(i=0;i<n;i++)
		fval[i] = DY(float_user)[i];
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}


/*****************************************************************************/
/*                            aglgetfW (Internal use)                        */

/* Get float parameters with name "W...."                                    */

static int aglgetfW(pntr,fval)
char *pntr;                  /* pointer to the keyword (after the W)          */
float fval[];              /* return values array                           */
{
int n=0;

switch(*pntr++) { 

case 'n':                                /* keywd="WN...."          */
	fval[0] = XUSLOW;
	fval[1] = XUSUP;
	fval[2] = YUSLOW;
	fval[3] = YUSUP;
	n = 4;
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}




/*****************************************************************************/
/*                         AGL_gfloat (Internal use)                         */

/* get float   parameter(s)                                                  */


int AGL_gfloat(keywd,fval)	/* Returns number of values                  */
char *keywd;			/* Status specifier                          */
float fval[];			/* Return values. NOTE: an array of at least */
				/* the number of elements specified in the   */
				/* keyword description above MUST be provided*/
{
#define LASTFGETKEY   23
static int (*kwtable[LASTFGETKEY+1])() = {
					aglills,	/*      0            */
					aglgetfA,	/*  a - 1            */
					aglills,	/*  b - 2            */
					aglgetfC,	/*  c - 3            */
					aglgetfD,	/*  d - 4            */
					aglills,	/*  e - 5            */
					aglills,	/*  f - 6            */
					aglills,	/*  g - 7            */
					aglills,	/*  h - 8            */
					aglills,	/*  i - 9            */
					aglills,	/*  j - 10           */
					aglills,	/*  k - 11           */
					aglills,	/*  l - 12           */
					aglills,	/*  m - 13           */
					aglgetfN,	/*  n - 14           */
					aglills,	/*  o - 15           */
					aglills,	/*  p - 16           */
					aglills,	/*  q - 17           */
					aglgetfR,	/*  r - 18           */
					aglgetfS,	/*  s - 19           */
					aglills,	/*  t - 20           */
					aglgetfU,  	/*  u - 21           */
					aglgetfV,  	/*  v - 22           */
					aglgetfW };  	/*  w - 23           */
int jump;

AGL_status.errstat=AGLNOERR;

jump= *keywd-'a'+1;

jump= (jump<0)?0:jump;
jump = (jump>LASTFGETKEY)?0:jump;

return (*kwtable[jump])(keywd+1,fval);
}




/*****************************************************************************/
/*                            aglgetiB (Internal use)                        */

/* Get int   parameters with name "B...."                                    */

static int aglgetiB(pntr,ival)
char *pntr;                /* pointer to the keyword (after the B)          */
int   ival[];              /* return values array                           */
{
int n=0;

switch(*pntr++) {
	case 'a':			/* keywd="BAckgr."                 */
		ival[0] = CURBACKGR(AGL_status.devid);
		n = 1;
		break;
	case 'l':			/* keywd="BL..."                     */
		switch(*pntr++) {
			case 'a':	/* keywd="BLAck."                    */
				ival[0] = AGLBLACK(AGL_status.devid);
				n = 1;
				break;
			case 'u':	/* keywd="BLUe.."                    */
				ival[0] = AGLBLUE(AGL_status.devid);
				n = 1;
				break;
			default: 
				AGL_status.errstat=(ILLCODWNG); 
				break;
		}
	default: 
		AGL_status.errstat=(ILLCODWNG); 
		break;
}
return n;
}



/*****************************************************************************/
/*                            aglgetiC (Internal use)                        */

/* Get int   parameters with name "C...."                                    */

static int aglgetiC(pntr,ival)
char *pntr;                /* pointer to the keyword (after the C)          */
int   ival[];              /* return values array                           */
{
int n=0;

switch(*pntr++) {
	case 'o':				/* keywd="CO...."           */
		ival[0] = DY(color);
		n = 1;
		break;

	case 'l':				/* keywd="CL...."           */
		pntr++;
		switch(*pntr++) {
			case '1':		/* keywd="CLR1.."           */
				ival[0]=DEFFOREGR(AGL_status.devid);
				n = 1;
				break;

			case '2':		/* keywd="CLR1.."           */
				ival[0]=AGLRED(AGL_status.devid);
				n = 1;
				break;

			case '3':		/* keywd="CLR1.."           */
				ival[0]=AGLBLUE(AGL_status.devid);
				n = 1;
				break;

			default: 
				AGL_status.errstat=(ILLCODWNG); 
				break;
		}
		break;

	case 'y':				/* keywd="CYan.."           */
		ival[0] = AGLCYAN(AGL_status.devid);
		n = 1;
		break;

	default: 
		AGL_status.errstat=(ILLCODWNG); 
		break;
}
return n;
}




/*****************************************************************************/
/*                            aglgetiD (Internal use)                        */

/* Get int   parameters with name "D...."                                    */

static int aglgetiD(pt,ival)
char *pt;			/* Keyword pointer                           */
int ival[];			/* return values array                       */
{
int n=0;
switch(*pt++) {
case 'e':					/* keywd="DEVTyp"           */
	n = 1;
	ival[0]=FLAGS(AGL_status.devid)&INTERACTIVE;
	break;

default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}



/*****************************************************************************/
/*                            aglgetiE (Internal use)                        */

/* Get int   parameters with name "E...."                                    */

static int aglgetiE(pntr,ival)
char *pntr;                  /* pointer to the keyword (after the E)          */
int ival[];                /* return values array                           */
{
int n=0;

switch(*pntr++) {
	case 'r':			/* keywd="ER...."           */
		ival[0] = AGL_status.lasterr;
		n = 1;
		break;

	default: 
		AGL_status.errstat=(ILLCODWNG); 
		break;
}
return n;
}




/*****************************************************************************/
/*                            aglgetiF (Internal use)                        */

/* Get int   parameters with name "F...."                                    */

static int aglgetiF(pntr,ival)
char *pntr;                /* pointer to the keyword (after the F)          */
int ival[];                /* return values array                           */
{
int n=0;
int flgid;
n = 0;

switch(*pntr++) {
	case 'l':                                 /* keywd="FL...."           */
		pntr++;
		pntr++;
		flgid=atoi(pntr);
		if(flgid<NCHFLAGS)  {
			ival[0] = (FLAGS(AGL_status.devid)&(1<<flgid))?1:0;
			n = 1;
		} else
			AGL_status.errstat=(ILLCODWNG); 
		break;
	default: 
		AGL_status.errstat=(ILLCODWNG); 
		break;
}
return n;
}



/*****************************************************************************/
/*                            aglgetiG (Internal use)                        */

/* Get int   parameters with name "G...."                                    */

static int aglgetiG(pntr,ival)
char *pntr;			/* pointer to the keyword (after the G)      */
int ival[];			/* return values array                       */
{
int n=0;
switch(*pntr++) {

case 'r':				/* keywd: GReen                      */
	ival[0] = AGLGREEN(AGL_status.devid);
	n = 1;
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}



/*****************************************************************************/
/*                            aglgetiL (Internal use)                        */

/* Get int   parameters with name "L...."                                    */

static int aglgetiL(pntr,ival)
char *pntr;			/* pointer to the keyword (after the L)      */
int ival[];			/* return values array                       */
{
int n=0;

switch (*pntr++) {
case 'i':					/* keywd="LI...."           */
	pntr++;
	n = 1;
	switch (*pntr++) {
	case 'x':				/* keywd="LINX..."          */
		ival[0] = (!DY(flogx));
		break;
	case 'y':				/* keywd="LINY..."          */
		ival[0] = (!DY(flogy));
		break;

	default: 
		AGL_status.errstat=(ILLCODWNG); 
		break;
	}
	break;

case 'o':					/* keywd="LO...."           */
	pntr++;
	switch (*pntr++) {
	case 'x':				/* keywd="LOGX..."          */
		n = 1;
		ival[0] = DY(flogx);
		break;
	case 'y':				/* keywd="LOGY..."          */
		n = 1;
		ival[0] = DY(flogy);
		break;
	default: 
		AGL_status.errstat=(ILLCODWNG); 
		break;
	}
	break;

case 's':					/* keywd="LS...."           */
	n = 1;
	ival[0]=DY(lstyle);
	break;

case 'w':					/* keywd="LW...."           */
	n = 1;
	ival[0]=DY(lwidth);
	break;

default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}





/*****************************************************************************/
/*                            aglgetiM (Internal use)                        */

/* Get int   parameters with name "M...."                                    */

static int aglgetiM(pntr,ival)
char *pntr;			/* pointer to the keyword (after the M)      */
int ival[];			/* return values array                       */
{
int n=0;

 switch(*pntr++) {

	case 'f':                                 /* keywd="MF..."            */
		n = 1;
		ival[0]=DY(filact);
		break;
	case 'o':                                 /* keywd="MOde.."           */
		n = 1;
		ival[0] = (int)DY(modeflag);
		break;

	case 'a':                                 /* keywd="MAgenta.."        */
		n = 1;
		ival[0] = AGLMAGENTA(AGL_status.devid);
		break;

	default: 
		AGL_status.errstat=(ILLCODWNG); 
		break;
}
return n;
}




/*****************************************************************************/
/*                            aglgetiN (Internal use)                        */

/* Get int   parameters with name "N...."                                    */

static int aglgetiN(pntr,ival)
char *pntr;                  /* pointer to the keyword (after the N)          */
int ival[];                /* return values array                           */
{
int n=0;
struct AGL_font *finfo;

switch(*pntr++) {
case 'c':					/* keywd="NC...."           */
	n = 1;
	ival[0]=NPLANS(AGL_status.devid);
	break;
case 's':					/* keywd="NS...."           */
	switch(*pntr++) {
	case 't':				/* keywd="NST..."           */
		n = 1;
		ival[0]=MAXLSTYL;
		break;

	case 'y':				/* keywd="NSY..."           */
		finfo=AGL_chinf(SYMBFONT);
		if(finfo != NULL) { 
			n = 1; 
			ival[0]=finfo->nchars; 
		}
		break;

	default: 
		AGL_status.errstat=(ILLCODWNG); 
		break;
	}
	break;

case 'w':				/* keywd="NW...."           */
	n = 1;
	ival[0]=MAXLWIDTH(AGL_status.devid);
	break;

default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}



/*****************************************************************************/
/*                            aglgetiR (Internal use)                        */

/* Get int   parameters with name "R...."                                    */

static int aglgetiR(pntr,ival)
char *pntr;			/* pointer to the keyword (after the R)      */
int ival[];			/* return values array                       */
{
int n=0;
switch(*pntr++) {
case 'e':				/* keywd: REd                        */
	n = 1;
	ival[0] = AGLRED(AGL_status.devid);
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}



/*****************************************************************************/
/*                            aglgetiT (Internal use)                        */

/* Get int   parameters with name "T...."                                    */

static int aglgetiT(pntr,ival)
char *pntr;			/* pointer to the keyword (after the R)      */
int ival[];			/* return values array                       */
{
int n=0;
switch(*pntr++) {
case 'w':				/* keywd: TW.....                    */
	n = 1;
	ival[0] = DY(textlw);
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}





/*****************************************************************************/
/*                            aglgetiU (Internal use)                        */

/* Get int   parameters with name "U...."                                    */

static int aglgetiU(pntr,ival)
char *pntr;			/* pointer to the keyword (after the W)      */
int ival[];			/* return values array                       */
{
int n=0;
int i;

switch(*pntr++) {
case 's':				/* keywd: USER                       */
	n = DY(n_int_user);

	for(i=0;i<n;i++)
		ival[i] = DY(int_user)[i];
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}


/*****************************************************************************/
/*                            aglgetiW (Internal use)                        */

/* Get int   parameters with name "W...."                                    */

static int aglgetiW(pntr,ival)
char *pntr;			/* pointer to the keyword (after the W)      */
int ival[];			/* return values array                       */
{
int n=0;

switch(*pntr++) {
case 'h':				/* keywd = WHITe                     */
	n = 1;
	ival[0] = AGLWHITE(AGL_status.devid);
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}




/*****************************************************************************/
/*                            aglgetiY (Internal use)                        */

/* Get int   parameters with name "Y...."                                    */

static int aglgetiY(pntr,ival)
char *pntr;			/* pointer to the keyword (after the W)      */
int ival[];			/* return values array                       */
{
int n=0;
switch(*pntr++) {
case 'e':				/* keywd = YELLow                    */
	n = 1;
	ival[0] = AGLYELLOW(AGL_status.devid);
	break;
default: 
	AGL_status.errstat=(ILLCODWNG); 
	break;
}
return n;
}





/*****************************************************************************/
/*                           AGL_gint (Internal use)                         */

/* get integer parameter(s)                                                  */

int AGL_gint(keywd,ival)	/* Returns number of values                  */
char *keywd;			/* Status specifier                          */
int  ival[];			/* Return values. NOTE: an array of at least */
				/* the number of elements specified in the   */
				/* keyword description above MUST be provided*/
{
#define LASTGETKEY   25
static int (*kwtable[LASTGETKEY+1])() = {
					aglills,	/*      0            */
					aglills,	/*  a - 1            */
					aglgetiB,	/*  b - 2            */
					aglgetiC,	/*  c - 3            */
					aglgetiD,	/*  d - 4            */
					aglgetiE,	/*  e - 5            */
					aglgetiF,	/*  f - 6            */
					aglgetiG,	/*  g - 7            */
					aglills,	/*  h - 8            */
					aglills,	/*  i - 9            */
					aglills,	/*  j - 10           */
					aglills,	/*  k - 11           */
					aglgetiL,	/*  l - 12           */
					aglgetiM,	/*  m - 13           */
					aglgetiN,	/*  n - 14           */
					aglills,	/*  o - 15           */
					aglills,	/*  p - 16           */
					aglills,	/*  q - 17           */
					aglgetiR,	/*  r - 18           */
					aglills,	/*  s - 19           */
					aglgetiT,	/*  t - 20           */
					aglgetiU,  	/*  u - 21           */
					aglills,  	/*  v - 22           */
					aglgetiW,  	/*  w - 23           */
					aglills,  	/*  x - 24           */
					aglgetiY };  	/*  y - 25           */
int jump;
AGL_status.errstat=AGLNOERR;

jump= *keywd-'a'+1;

jump= (jump<0)?0:jump;
jump = (jump>LASTGETKEY)?0:jump;

return (*kwtable[jump])(keywd+1,ival);
}
