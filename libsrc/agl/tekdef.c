/* @(#)tekdef.c	19.1 (ES0-DMD) 02/25/03 13:53:08 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)tekdef.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:08   */
/*
 * HEADER : tekdef.c       - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *  Driver entry point table for the Tektronix 4010 Terminal driver
 *
 */

{
DEF(AGLCUTKG);
DEF(AGLERTKG);
DEF(AGLESTKG);
DEF(AGLINTKG);
DEF(AGLSETKG);
DEF(AGLTETKG);
DEF(AGLPLTKG);
(void)setdev("tkg",AGLCUTKG,AGLERTKG,AGLESTKG,AGLINTKG,
                   AGLSETKG,AGLTETKG,AGLPLTKG);
}
