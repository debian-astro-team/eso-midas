/* @(#)ag_dmsg.c	19.1 (ES0-DMD) 02/25/03 13:52:57 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_dmsg.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:57   */
/*
 * HEADER : ag_dmsg.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*CC                    AG_DMSG (C callable)                                 */
 
/* AG_DMSG   print debug message                                             */


/* If debugging flag is on (see AG_SSET) prints debugging messages to the    */
/* AGL_err stream. AGL_err is usually equal to stderr, but can be redirected   */
/* to a file (see AG_SSET).                                                  */

/* This routine has been developed mainly to be used in user written device  */
/* drivers. It has no FORTRAN counterpart                                    */


static char *ent = "Entered";

void AG_DMSG (m1,m2)

 char *m1,*m2;                /* The two strings are printed on stderr       */
                                                                         /*--*/
{
if(m2==NULL)
	m2=ent;

if(AGL_DEBUG) 
	fprintf(AGL_err,"DBG> %s %s\n",m1,m2);
}

