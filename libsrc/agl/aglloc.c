/* @(#)aglloc.c	19.1 (ES0-DMD) 02/25/03 13:53:04 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)aglloc.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:04   */
/*
 * HEADER : aglloc.c       - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA
 */





/*                       INTERNAL LOCATOR SUPPORT                            */




#include <aglenvr.h>
#include <aglstat.h>





/*****************************************************************************/
/*                      AGL_loc (Internal AGL use)                           */

/* This routine is the internal version of AG_VLOC and AG_VLOS               */

void AGL_loc (xv,yv,stmax,str,pixval)
float *xv,*yv;				/* Input: preset locator position    */
					/* Output: final locator position    */
int stmax;				/* max output string length          */
					/* If stmax = 1, then a single char  */
					/* is expected. If stmax > 1 then a  */
					/* maximum of (stmax-1) chars are    */
					/* read. Input is also terminated by */
					/* a NUL character                   */
char *str;				/* Output string. Length >= stmax    */
int *pixval;				/* return pixel value                */
{
double xt,yt;
int pix,count;
int AGL_cnvt();
int AGL_rcvt();
DEFINE_AGLDVCOM;

AGL_status.errstat=AGLNOERR;

if(!(FLAGS(AGL_status.devid)&INTERACTIVE)) {
	AGL_status.errstat = UNSFEATERR;
	return;
}

CHANNEL = DY(curchan);
SENDFUNCT(AGLDVCOM);
AGL_status.errstat=ERRCODE;
if(AGL_status.errstat != AGLNOERR) 		/* Test send buffer errors   */
	return;

if(DY(curstyl)>=0) {
	xt = *xv; yt = *yv;
	AGL_cnvt(&xt,&yt);			/* Convert coordinates       */

	xt = MAX(xt,0.0);
	xt = MIN(xt,1.0);

	yt = MAX(yt,0.0);
	yt = MIN(yt,1.0);

	RBUFFR(0) = xt;
	RBUFFR(1) = yt;

	RBUFFR(2) = XVWLOW;
	RBUFFR(3) = XVWUP;
	RBUFFR(4) = YVWLOW;
	RBUFFR(5) = YVWUP;
}

IBUFFR(1) = DY(curstyl);
CHANNEL = DY(curchan);
AGLDVCOM->cnvrt=AGL_rcvt;
CURSFUNCT(AGLDVCOM);
*str = IBUFFR(1);
count = 1;
pix = IBUFFR(2);

xt=RBUFFR(0); yt=RBUFFR(1);

if(stmax > 1) {
	char *pntr = str+1;
	while(++count<stmax) {
		RBUFFR(0) = xt;
		RBUFFR(1) = yt;
		IBUFFR(1) = (-1);
		CHANNEL = DY(curchan);
		CURSFUNCT(AGLDVCOM);
		if(IBUFFR(1)<' ') 
			break;		/* Loop exit on Control characters */
		*pntr++ = IBUFFR(1);
	}
	*pntr = '\0';
}

if(DY(curstyl)>=0) {
	AGL_status.errstat = AGL_rcvt(&xt,&yt);
	*xv=xt; *yv=yt;
}

if(pixval != NULL)
   {
	if(FLAGS(AGL_status.devid)&RETPIXVAL)
		*pixval = pix;
	else
		if(AGL_status.errstat==AGLNOERR) 
			AGL_status.errstat = NOPIXINF ;
   }
}
