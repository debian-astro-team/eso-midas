/* @(#)aglpoly.c	19.1 (ES0-DMD) 02/25/03 13:53:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)aglpoly.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:05   */
/*
 * HEADER : aglpoly.c      - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA
 */




/*                            POLYLINES  OPERATIONS                          */




#include <aglenvr.h>
#include <aglstat.h>




/*****************************************************************************/
/*                        AGL_plmk (Internal AGL Use)                        */

/* Creates a polyline structure from coordinate vectors                      */

void AGL_plmk(xv,yv,nmax,np,mode,poly)  
float *xv, *yv;			/* Coordinate arrays                     */
int   nmax;			/* Max number of coordinates             */
int   np;			/* Number of coordinates                 */
enum GRAPH_MODES mode;		/* Graphic mode to be attached.          */
struct polyline *poly;		/* Output polyline                       */
{
poly->maxvects = nmax;
poly->nvects = np;
poly->XV = xv;
poly->YV = yv;
poly->grmode = mode;
poly->clipmode = mode;
poly->marker = (-1);
poly->flush = NULL;
poly->join = FALSE;
}

/*****************************************************************************/
/*                      AGL_pcoo (Internal AGL Use)                          */

/* This function adds a couple of coordinates to a polyline. When the poly-  */
/* line buffer is full the polyline is drawn with a call to the function     */
/* PREVIOUSLY specified by a call to AGL_spfls. Any function operating on a   */
/* polyline can be specified as flushing function.                           */
/* If the function is auxgpll or auxnpll the last point of the polyline is   */
/* copied into the first point (so that next polyline is joined)             */

void AGL_pcoo(xc,yc,poly)
double xc,yc;
struct polyline *poly;
{
void AGL_pflsh();

if(poly->nvects==poly->maxvects) 	/* The polyline buffer is full      */
	if(poly->flush != NULL) 
		AGL_pflsh(poly);		/* Execute flushing routine */


*(poly->XV+poly->nvects)=xc;
*(poly->YV+poly->nvects)=yc;
poly->nvects++;
}

/*****************************************************************************/
/*                        AGL_spfls (Internal AGL Use)                        */

/* Sets the function to call for flushing the polyline buffer                */

  
void AGL_spfls(funct,poly)
void (*funct)();
struct polyline *poly;
{
 poly->flush = funct;
}



/*****************************************************************************/
/*                       AGL_pflsh (Internal AGL Use)                        */

/* Draws the polyline (if necessary) by calling the function PREVIOUSLY set  */
/* by calling AGL_spfls, and clears the polyline counter.                    */
/* The routines also clears the dash pattern reset flag so that subsequent   */
/* calls will join to the current, unless the flag is explicitly reset       */


void AGL_pflsh(poly)		/* Draw the polyline                         */
struct polyline *poly;
{
int nvects;
if(poly->marker<0)
	nvects=1;
else
	nvects=0;

if((poly->nvects>nvects) && (poly->flush != NULL))
	(*(poly->flush))(poly);

if(poly->join) {			/* Join to previous polyline        */
	int last=poly->nvects-1;

	if(last>=0) {
		poly->XV[0]=poly->XV[last];
		poly->YV[0]=poly->YV[last];
		poly->nvects=1;
	}
} else
	poly->nvects=0;
}


