/* @(#)ag_gplg.c	19.1 (ES0-DMD) 02/25/03 13:52:58 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_gplg.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:58   */
/*
 * HEADER : ag_gplg.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>





/*****************************************************************************/
/*++                         AG_GPLG (User callable)                         */

/* AG_GPLG    Draw polygon                                                   */


/* Polygon  drawing. The given set of points are connected with straight     */
/* lines starting at the first point. The last point is joined to the first. */

/* Line aspect (style, width, color) may be selected via AG_SSET.            */

/* The coordinate values are referred to the current graphic mode (either    */
/* NORMALIZED, USER or SPECIAL) and mapped accordingly.  Graphic mode is     */
/* affected by: AG_WDEF, AG_TRNS, and AG_SSET items "NORMAL","USER" and      */
/* "SPECIAL"                                                                 */

void AG_GPLG (xv,yv,np)

 float xv[],yv[];             /* Coordinate arrays                           */
 int np;                      /* Array length                                */
                                                                         /*--*/ 
{
static char *modnam = "GPLG";
struct polyline inpoly;
extern void AG_DMSG();
int svstat;

if(AGL_DEBUG) 
	AG_DMSG(modnam,(char *)0);

AGL_plmk(xv,yv,np,np,DY(modeflag),&inpoly);

#ifndef NO_METAFILE_SUPPORT
if ( DY(filact) == SOFTMETAFILE ) {
	enum METACODE cod = MFGPLL;

	(void)fwrite((void *)&cod,sizeof(int),1,DY(metafile));
	AGL_mput(DY(metafile),&inpoly,TRUE);
	if(AGL_status.errstat != AGLNOERR)
	AGL_siger(modnam);
}
#endif

if(AGL_status.dashon) {
	AGL_dshz();
	AGL_dpll(&inpoly);
} else
	AGL_gpll(&inpoly);

svstat=AGL_status.errstat;

if(np>2) {
	struct polyline auxpoly;
	float xx[2],yy[2];

	xx[0]=xv[np-1];
	xx[1]=xv[0];
	yy[0]=yv[np-1];
	yy[1]=yv[0];

	AGL_plmk(xx,yy,2,2,DY(modeflag),&auxpoly);

	if(AGL_status.dashon)
		AGL_dpll(&auxpoly);
	else
		AGL_gpll(&auxpoly);
}
AGL_status.errstat = (svstat>AGL_status.errstat) ? svstat : AGL_status.errstat;
if(AGL_status.errstat != AGLNOERR) 
	AGL_siger(modnam);
}

