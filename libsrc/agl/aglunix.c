/* @(#)aglunix.c	19.1 (ES0-DMD) 02/25/03 13:53:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)aglunix.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:05   */
/*
 * HEADER : aglunix.c      - Vers 3.6.001  - Oct 1992 -  L. Fini, OAA
 *
 * This file contains system dependent features which are common to all
 * blends of Unix systems
 *
 */



void AGL_trns(inpname,maxlng,trnslnam)  /* Get environment variable        */
					/* Returns equivalence string (or  */
					/* the input string if not defined)*/
char *inpname;				/* Input name (Environment var.)   */
int  maxlng;				/* Max output string length        */
char *trnslnam;				/* Output equivalence name         */
{
char wrk_str[PHNAMLNG];
char *getenv();
char *work;
int retlng;

strncpy(wrk_str,inpname,PHNAMLNG);

work=getenv(wrk_str);

if(work==NULL) 
	work=wrk_str;

retlng=strlen(work);

if(retlng>=maxlng) 
	retlng=maxlng-1;

strncpy(trnslnam,work,retlng);
*(trnslnam+retlng)='\0';
}



/***************************************************************************/
/*                              AGL_fopen                                   */

/* This routine is used to open an i/o channel to a terminal. It must take */
/* into account system specific details. E.g. VMS systems will always open */
/* an I/O channel to be used for subsequent SYS$QIO calls, whereas Unix    */
/* systems will probably simply issue a standard open call.                */
/* The function must recognize the device name "tt" and return stdout      */

FILE *AGL_fopen (fname)
char * fname;                 /* Device name (if it is tt, unix systems    */
                              /* will simply return stdout)                */
{
if((*fname == 't')&&(*(fname+1)=='t')) 
	return(stdout);
else
	return(fopen(fname,"w+"));
}

