/* @(#)ag_vu2n.c	19.1 (ES0-DMD) 02/25/03 13:53:03 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_vu2n.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:03   */
/*
 * HEADER : ag_vu2n.c      - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                            AG_VU2N (C callable)                         */

/* AG_VU2N    User to normalized coord. conversion                           */


/* Convert an (x,y) couple of USER coordinates into the corresponding NORMAL-*/
/* ized coordinates.                                                         */


void AG_VU2N (xu,yu,xpn,ypn)

double xu,yu;                 /* User input coordinates                      */
float  *xpn,*ypn;             /* Normalized output coordinates               */
                                                                         /*--*/
{
static char *modnam = "VU2N";
double xt=xu;
double yt=yu;

if(!DY(window)) { 
	AGL_puterr(NOWNDERR,modnam); 
	return; 
}

if(DY(flogx)) {
	if(xt<=0.0) { 
		AGL_puterr(POSCNVERR,modnam); 
		return; 
	}
	xt = log((double)xt);
}

if(DY(flogy)) {
	if(yt<=0.0) { 
		AGL_puterr(POSCNVERR,modnam); 
		return; 
	}
	yt = log((double)yt);
}

if(AGL_status.UTransf) 
	(*DY(UsrTransf))(&xt,&yt);

*xpn = (AGL_status.aglfcx * xt) + AGL_status.aglofx;
*ypn = (AGL_status.aglfcy * yt) + AGL_status.aglofy;

if ( (*xpn>XVWUP)||(*xpn<XVWLOW) ||
     (*ypn>YVWUP)||(*ypn<YVWLOW) )
	AGL_puterr(VWPOUTWNG,modnam); 
}

