/* @(#)ag_gerr.c	19.1 (ES0-DMD) 02/25/03 13:52:58 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_gerr.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:58   */
/*
 * HEADER : ag_gerr.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                           AG_GERR (C callable)                          */

/* AG_GERR    Generate an error condition                                    */

/* Generate an error condition. The error is treated as AGL generated errors */
/* with respect to message printing and process stopping (see AG_SSET items: */
/* MSGALL,MSGWARN,MSGERR,MSGSEV,MSGNONE,STPERROR,STPNEVER,STPSEVERE).        */

void AG_GERR (code,message) 

 int code;                    /* Error code in the range                     */
                              /* Severity ranges:    0 -  99   informaton    */
                              /*                   100 - 199   warning       */
                              /*                   200 - 299   error         */
                              /*                   300 - 399   severe        */
 char *message;               /* Error message                               */
                                                                         /*--*/
{
int sever;
code=(code<1)?1:code;
code=(code>399)?399:code;

sever = code/100;			/* Test error severity             */
if ( sever >= AGL_status.sevmsg ) {
	fprintf(AGL_err,"USER code %4d -- %s\n",code,message );
}

if(sever>=AGL_status.sevterm) 
	AGL_stop();
}

