/* @(#)sendv80.c	19.1 (ES0-DMD) 02/25/03 13:53:08 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)sendv80.c	19.1 (OAA-ASTRONET) 02/25/03 13:53:08 */
/*
 * HEADER : sendv80.c      - Vers 3.6.000  - Dic. 1991 -  F. Tribioli, OAA
 *
 * Spooler for Versatec V-80 printers
 * It must be used with file produced by "versatec" utility. It doesn't work
 * with normal bitmaps but uses compressed ones.
 *
 * Usage: sendv80 filename
 *
 * 910228 CG: exit(); -> exit(1); 
 * 911122 LF: casting to (char *) in setbuf()
 *
 */

#include <math.h>
#include <stdio.h>

#ifdef VMS
#include <descrip.h>
#include <ssdef.h>
#include <iodef.h>
#endif

#include <versatec.h>

#ifndef VMS
static unsigned char myinbuffer[BUFSIZ];
static unsigned char myoutbuffer[BUFSIZ];
#endif

static unsigned char buffer[VERSATECBUFSIZE];
FILE *infile;

unsigned char bmasks[]={0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};

/*
 * routine: decode. Decode input from file and exapnd it in a bitmap row
 */

decode()
{
  register i,sum=0;
  short counter;
  unsigned char bitstatus,countlo,counthi;

  fread(&bitstatus,1,1,infile);		/* read first pixel value	*/

  if(bitstatus)			/* reset to correct value to begin	*/
    bitstatus=0;
  else
    bitstatus=1;

  while(sum < VERSATECBUFSIZE*8 && !feof(infile))
  {
    fread(&counthi,1,1,infile);
    fread(&countlo,1,1,infile);
    counter=((short)counthi << 8) | countlo;
    if(!bitstatus)
      for(i=0;i<counter;i++)
      {
        long ind,rest;

        ind = sum >> 3;
        rest = sum & 7;

        buffer[ind] |= bmasks[rest];	/* write a pixel		*/
        bitstatus=1;			/* exchange status		*/
        sum++;				/* update pointer		*/
      }
    else
      {
        bitstatus=0;			/* exchange status		*/
        sum+=counter;			/* update pointer		*/
      }
  }
}

main(argc,argv)
int argc;
char *argv[];
{
  int i;

#ifdef VMS
  unsigned res;
  short channel;
  unsigned leng;
  unsigned ff=0x10;
  $DESCRIPTOR(devname,VERSATECDEV);

  leng=VERSATECBUFSIZE;
#else
  int nout,fno;
  int newmode[3];
  FILE *channel;
#endif

  if(argc!=2)
  {
    printf("\n\n");
    printf("Spooler for Versatec V-80 printers.");
    printf(" Vers. 3.5 - F. Tribioli, Dec 1989.\n\n");
    printf("Usage: sendv80 [input] \n");
    printf("where:  input  is the input file, produced by ");
    printf("versatec rasterizer\n\n");
    printf("Copyright 1989 by F. Tribioli. This program may be freely ");
    printf("copied and used\n");
    printf("provided this copyright notice will remain attached to it.\n\n");
    exit(1);
  }

  if((infile=fopen(argv[1],"r"))==NULL)
  {
    fprintf(stderr,"Cannot find input file");
    exit(1);
  }

#ifdef VMS
  if((res=SYS$ALLOC(&devname,0,0,0))!=SS$_NORMAL) /* allocate device  */
  {
    fprintf(stderr,"ERROR! Cannot allocate device %s",VERSATECDEV);
    exit(1);
  }

  if((res=SYS$ASSIGN(&devname,&channel,0,0))!=SS$_NORMAL) /* assign a channel */
  {
    fprintf(stderr,"ERROR! Cannot assign channel to device %s",VERSATECDEV);
    exit(1);
  }
#else
  setbuf(infile,(char *)myinbuffer);

  if((channel=fopen(VERSATECDEV,"ab"))==NULL)
  {
    fprintf(stderr,"ERROR! Device %s not present",VERSATECDEV);
    exit(1);
  }
  setbuf(channel,(char *)myoutbuffer);
  fno=fileno(channel);
  newmode[0]=VPLOT;
  newmode[1]=0;
  newmode[2]=0;
  ioctl(fno,VSETSTATE,newmode);
#endif

  while(!feof(infile))
  {
    for(i=0;i<VERSATECBUFSIZE;i++) buffer[i]=0;

    decode();				/* decode input			     */

#ifdef VMS
					/* write on Versatec		     */
    if((res=SYS$QIOW(0,channel,(short)(IO$_WRITEVBLK|IO$M_PLOT),0,0,0,buffer,
	leng,0,0,0,0))!=SS$_NORMAL)
    {
      printf("ERROR! Cannot write data to printer");
      break;
    }
#else
    if((nout=fwrite(buffer,1,VERSATECBUFSIZE,channel))!=VERSATECBUFSIZE)
    {
      printf("ERROR! Cannot write data to printer");
      break;
    }
#endif
  }
#ifdef VMS
					/* send a form feed		     */
  if((res=SYS$QIOW(0,channel,(short)(IO$_WRITEVBLK|IO$M_REMOTE),0,0,0,0,0,ff,
	0,0,0))!=SS$_NORMAL) printf("ERROR! Cannot write data to printer");
#else
  if(fflush(channel)) printf("ERROR! Cannot write data to printer");
  newmode[0]=VPRINT;
  ioctl(fno,VSETSTATE,newmode);
  write(fno,"\04",1);
#endif

					/* shut all                          */
#ifdef VMS
  if((res=SYS$DASSGN(channel))!=SS$_NORMAL)
  {
    printf("ERROR! Cannot deassign channel");
    exit(1);
  }

  if((res=SYS$DALLOC(&devname,0))!=SS$_NORMAL)
    printf("ERROR! Cannot dealloc device %s",VERSATECDEV);

  fclose(infile);
#else
  fclose(infile);
  fclose(channel);
#endif
  exit(0);
}
