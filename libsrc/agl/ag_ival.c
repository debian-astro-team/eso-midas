/* @(#)ag_ival.c	19.1 (ES0-DMD) 02/25/03 13:52:59 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_ival.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:59   */
/*
 * HEADER : ag_ival.c      - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*CC                           AG_IVAL (C callable)                          */

/* AG_IVAL    Integer conversion                                             */


/* Converts the (decimal integer) value(s) after the = sign from a string.   */

/* String syntax is as follows:                                              */

/*     "xxxxx=<value>[,<value>,...,<value>]"                                 */

/* where xxxx is any string and value must have the syntax required by the   */
/* atoi standard library service. Multiple values are separated by commas.   */

/* e.g.:   COLORS=2,5;                                                       */

/* Note: this routine is callable through the C interface only               */


int AG_IVAL (chstrg,nvals,vals) 
                              /* Returns number of values converted          */

 char *chstrg;                /* Input string                                */
 int  nvals;                  /* number of values to be converted            */
 int  vals[];                 /* Output array (at least nvals long)          */
                                                                         /*--*/
{
char *pntr;
int i,nv=0;

pntr=chstrg;
while( ((*pntr)!='=')&&((*pntr)!='\0') ) 
	pntr++;  				/* scan till the '='         */

for(i=0; i<nvals; i++) {
	if(*pntr == '\0') 
		vals[i]=0;
	else {
		pntr++; 
		vals[i]=atoi(pntr);
		while( ((*pntr)!=',')&&((*pntr)!='\0')) 
			pntr++; 		/* scan till the ','        */
		nv++;				/* count convrted values    */
	}
}
return(nv);
}

