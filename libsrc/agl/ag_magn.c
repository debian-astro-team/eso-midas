/* @(#)ag_magn.c	19.1 (ES0-DMD) 02/25/03 13:52:59 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_magn.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:59   */
/*
 * HEADER : ag_magn.c      - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>



/*****************************************************************************/
/*++                         AG_MAGN (User callable)                         */

/* AG_MAGN    Rotate and translate a polyline                                */


/* This module multiplies every element of a  couple of X-Y vectors by given */
/* factors.                                                                  */


void AG_MAGN (xfact,yfact,xv,yv,np)

 double xfact,yfact;          /* Multiplying factors                         */
 float xv[],yv[];             /* Coordinate arrays                           */
 int np;                      /* Array length                                */
                                                                         /*--*/ 
{
float *XP=xv;
float *YP=yv;

while(np-- > 0) {
	*XP++ *= xfact;
	*YP++ *= yfact;
}
}

