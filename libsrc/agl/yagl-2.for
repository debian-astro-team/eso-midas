C===========================================================================
C Copyright (C) 1995-2008 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================

C ++++++++++++++++++++++++  yagl.for +++++++++++++++++++++++++++++++++++++++
C .LANGUAGE Fortran 77
C .IDENTIFICATION Module yagl.for
C .COMMENTS
C Module contains layer for the AGL related FORTRAN interfaces
C and the SC_interfaces written in (hopefully independent) C
C .AUTHOR         K. Banse        ESO - Garching
C .KEYWORDS       standard interfaces.
C .ENVIRONMENT    FORTRAN and C standards
C .VERSION  [1.00] 871207:  
C 
C 080723		last modif
C -----------------------------------------------------------------------------
C
      SUBROUTINE AGSSET(COMMAND)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   COMMAND
      INTEGER   STATUS
C 
      CALL STSTR(1,COMMAND)                          !STRIPPED_STRING
C 
      CALL AGL1(STATUS)
C
      RETURN
      END
C


      SUBROUTINE AGVDEF(DEVICE,XA,XB,YA,YB,XLIM,YLIM)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   DEVICE
      INTEGER   XA,XB,YA,YB,XLIM,YLIM
C 
      CALL STSTR(1,DEVICE)                             !STRIPPED_STRING
C 
      CALL AGL2(XA,XB,YA,YB,XLIM,YLIM)
C
      RETURN
      END

      SUBROUTINE AGGTXT (XC,YC,STR,CNTR)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   STR
      INTEGER   XC,YC,CNTR
C
      CALL STSTR(1,STR)                             !STRIPPED_STRING
C
      CALL AGL3(XC,YC,CNTR)
C
      RETURN
      END

      SUBROUTINE AGGERR (CODE,MESG)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   MESG
      INTEGER   CODE
C
      CALL STSTR(1,MESG)                             !STRIPPED_STRING
C
      CALL AGL4(CODE)
C
      RETURN
      END

      SUBROUTINE AGIGET (ITEM,IVAL)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   ITEM
      INTEGER   IVAL
C
      CALL STSTR(1,ITEM)                             !STRIPPED_STRING
C
      CALL AGL5(IVAL)
C
      RETURN
      END

      SUBROUTINE AGRGET (ITEM,RVAL)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   ITEM
      REAL   RVAL
C
      CALL STSTR(1,ITEM)                             !STRIPPED_STRING
C
      CALL AGL6(RVAL)
C
      RETURN
      END

      SUBROUTINE AGTGET (ITEM,XD,YD)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   ITEM
C
      CALL STSTR(1,ITEM)                             !STRIPPED_STRING
C
      CALL AGL7(XD,YD)
C
      RETURN
      END

      SUBROUTINE AGVLOS (XV,YV,MAXLEN,CHSTRG,PIXVAL)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CHSTRG
      INTEGER   XV,YV,MAXLEN,PIXVAL
C
      CALL STSTR(1,CHSTRG)                             !STRIPPED_STRING
C
      CALL AGL8(XV,YV,MAXLEN,PIXVAL)
C
      RETURN
      END

      SUBROUTINE AGMOPN (FNAME)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   FNAME
      INTEGER   STATUS
C
      CALL STSTR(1,FNAME)                             !STRIPPED_STRING
C
      CALL AGL9(STATUS)
C
      RETURN
      END

      SUBROUTINE AGMRDW (METAFILE)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   METAFILE
      INTEGER   STATUS
C
      CALL STSTR(1,METAFILE)                             !STRIPPED_STRING
C
      CALL AGL10(STATUS)
C
      RETURN
      END


      SUBROUTINE AGESC (CMD,CMDLEN)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   CMD
      INTEGER   CMDLEN
C
      CALL STSTR(1,CMD)                             !STRIPPED_STRING
C
      CALL AGL11(CMDLEN)
C
      RETURN
      END

      SUBROUTINE AGAXES (X1,X2,Y1,Y2,STRING) 
C
      IMPLICIT NONE
C
      CHARACTER*(*)   STRING
      INTEGER   X1,X2,Y1,Y2
C
      CALL STSTR(1,STRING)                             !STRIPPED_STRING
C
      CALL AGL12(X1,X2,Y1,Y2)
C
      RETURN
      END


      SUBROUTINE AGAXIS (TYPE, DATA, LSPACE, FORM, LABEL) 
C
      IMPLICIT NONE
C
      CHARACTER*(*)   FORM, LABEL
      INTEGER   TYPE, DATA, LSPACE
C
      CALL STSTR(1,FORM)                             !STRIPPED_STRING
      CALL STSTR(2,LABEL)                             !STRIPPED_STRING
C
      CALL AGL13(TYPE, DATA, LSPACE)
C
      RETURN
      END

      SUBROUTINE AGORAX ( TYPE, ENDS, DATA, FORM, LABEL ) 
C
      IMPLICIT NONE
C
      CHARACTER*(*)   FORM, LABEL
      INTEGER   TYPE, DATA, ENDS
C
      CALL STSTR(1,FORM)                             !STRIPPED_STRING
      CALL STSTR(2,LABEL)                             !STRIPPED_STRING
C
      CALL AGL14(TYPE, ENDS, DATA)
C
      RETURN
      END

      SUBROUTINE AGNLIN(X1,X2,Y1,Y2,STRING)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   STRING
C
      CALL STSTR(1,STRING)                             !STRIPPED_STRING
C
      CALL AGL15(X1,X2,Y1,Y2)
C
      RETURN
      END

      SUBROUTINE AGFILL(X,Y,N,SPACE,ANGLE,SSET)
C
      IMPLICIT NONE
C
      CHARACTER*(*)   SSET
      INTEGER   X,Y,N,SPACE,ANGLE
C
      CALL STSTR(1,SSET)                             !STRIPPED_STRING
C
      CALL AGL16(X,Y,N,SPACE,ANGLE)
C
      RETURN
      END

