/* @(#)ag_rget.c	19.1 (ES0-DMD) 02/25/03 13:53:01 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_rget.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:01   */
/*
 * HEADER : ag_rget.c      - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                            AG_RGET (C callable)                         */

/* AG_RGET    Get real information                                           */


/* Get real pieces of information. Required information is specified by a    */
/* command string.                                                           */


int AG_RGET (item,fval)
                              /* Returns number of values in fval            */ 

 char *item;                  /* Information select string                   */
 float fval[];                /* Return value(s). The array must be long     */
                              /* enough to contain the required number of    */
                              /* values specified in the list below.         */

/* The select string contains a keyword which specifies the piece of informa-*/
/* tion required. Keywords can be truncated at the minimum number of charac- */
/* ters needed to recognize them (shown uppercase in the list below) but can */
/* be typed both in upper and lower case                                     */

/* Name     items    Description                                             */

/* ANGFct      1     Angular values conversion factor. It is = 1.0 if current*/
/*                   interpretation is "Radians" else is equal to the requi- */
/*                   red conversion factor                                   */
/* ASPEct      1     Current device aspect ratio                             */
/* CLPAspect   1     Current clipping area aspect ratio                      */
/* CLPLimits   4     Current clipping area limits (Xlow,Xup,Ylow,Yup)        */
/* CHDIms      2     Current character dimensions (x,y centimeters)          */
/* CHNDIms     2     Current character dimensions (x,y normalized)           */
/* DEVDims     2     Current device dimensions (centimeters)                 */
/* NCHDIms     2     Basic character dimensions (x,y normalized)             */
/* RESOlution  2     Current x and y axis resolutions (1./N.of pixels)       */
/* SCALE       1     Current overall character and symbol scale              */
/* USER        10    Get up to 10 user specified data items. See AG_RSET.    */
/* VWPLimits   4     Current viewport limits (Xlow,Xup,Ylow,Yup)             */
/* WNDLimits   4     Current window limits (Xlow,Xup,Ylow,Yup)               */
                                                                         /*--*/
{
extern char* AG_SCAN();
static char *modnam = "RGET";
char keywd[MAXKEYLNG];
extern void AG_DMSG();
int nvalues=0;

AG_DMSG(modnam,(char *)0);

AG_SCAN(item,';',MAXKEYLNG,keywd);

if(*keywd=='\0') 
	return nvalues;

nvalues=AGL_gfloat(keywd,fval);

if ( AGL_status.errstat != AGLNOERR ) 
	AGL_siger(modnam);

return nvalues;
}

