/* @(#)ag_idn.c	19.1 (ES0-DMD) 02/25/03 13:52:59 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_idn.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:59   */
/*
 * HEADER : ag_idn.c       - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>

/*FF                           AG_IDN  (C callable)                          */

/* AGIDN     Version identification                                          */

/* Returns a character string containing AGL version identification with the */
/* following format:                                                         */

/*    'AGL - V:3.61 (F)'                                                     */
/*                                                                           */

/*SUBROUTINE AGIDN(string)                                                   */

/* CHARACTER*(16) string;                                                    */
                                                                         /*--*/

/*****************************************************************************/
/*CC                           AG_IDN  (C callable)                          */

/* AG_IDN     Version identification                                         */

/* Return a pointer to the identification string (AGL V x.yy  )              */
/*                                                           |               */
/*                  'm' : no metafile support ---------------+               */
/*                                                                           */


char *AG_IDN ()
                              /* Returns pointer to identification string    */
                                                                         /*--*/
{
                 /* 0123456789012345      */
static char *ident="AGL V 3.61 (C)  ";

#ifdef NO_METAFILE_SUPPORT
ident[15] = 'm';
#endif

return ident;
}

