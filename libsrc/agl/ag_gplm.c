/* @(#)ag_gplm.c	19.1 (ES0-DMD) 02/25/03 13:52:59 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_gplm.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:59   */
/*
 * HEADER : ag_gplm.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>





/*****************************************************************************/
/*++                              AG_GPLM (C callable)                       */

/* AG_GPLM    Draw polymarker                                                */


/* Trace a set of symbols or markers. Symbols my be chosen from a set of     */
/* seventeen.  Symbol dimensions may be controlled via suitable AG_SSET      */
/* commands.                                                                 */

/* The coordinate values are referred to the current graphic mode (either    */
/* NORMALIZED, USER or SPECIAL) and mapped accordingly.  Graphic mode is     */
/* affected by: AG_WDEF, AG_TRNS, and AG_SSET items "NORMAL","USER" and      */
/* "SPECIAL"                                                                 */


void AG_GPLM (xv,yv,np,mark)

 float xv[],yv[];             /* Symbol position arrays                      */
 int  np;                     /* Position array length                       */
 int  mark;                   /* Symbol specifier (0..20)                    */

                              /* Currently defined marker symbols:           */

                              /*   Specifier     Symbol                      */

                              /*       0         Dot                         */
                              /*       1         Exagon                      */
                              /*       2         Square box                  */
                              /*       3         Triangle                    */
                              /*       4         +                           */
                              /*       5         X                           */
                              /*       6         + over X                    */
                              /*       7         Star                        */
                              /*       8         + over square box           */
                              /*       9         X over square box           */
                              /*      10         Lozenge                     */
                              /*      11         Horizontal bar              */
                              /*      12         Vertical bar                */
                              /*      13         Rightward oriented arrow    */
                              /*      14         Upward oriented arrow       */
                              /*      15         Leftward oriented arrow     */
                              /*      16         Downward oriented arrow     */
                              /*      17         Filled exagon               */
                              /*      18         Filled square               */
                              /*      19         Filled triangle             */
                              /*      20         Filled lozenge              */

                              /* Note: simbolic names for the marker values  */
                              /*       are defined into include file <agl.h> */
                              /*       for the convenience of the C program- */
                              /*       mer.                                  */

                              /* Values greather than 32 are taken as ASCII  */
                              /* codes and the corresponding character of    */
                              /* the current font is drawn centered in the   */
                              /* point of given coordinates.                 */
                                                                         /*--*/
{
static char *modnam = "GPLM";
int i,first;
double xstart,ystart;
double rescalx,rescaly;
double xoff,yoff;
int last;
int font = SYMBFONT;
struct AGL_font *finfo;
struct AGL_char my_char;

DEFINE_AGLDVCOM;

if(AGL_DEBUG) 
	AG_DMSG(modnam,(char *)0);


if(AGL_status.curvwp == VWPEMPTY) { 
	AGL_puterr(NOVWPERR,modnam); 
	return; 
}

if ( np<=0 ) { 
	AGL_puterr(VECLNGWNG,modnam); 
	return; 
}

if(mark<0) { 
	AGL_puterr(ILLMRKWNG,modnam); 
	mark=0; 
} else if(mark>32) {
	font=DY(curfont);
}

finfo=AGL_chinf(font);
last = finfo->fcode+finfo->nchars;

if(mark>last) { 
	AGL_puterr(ILLMRKWNG,modnam); 
	mark = (mark-finfo->fcode)%(finfo->nchars) + finfo->fcode; 
}

#ifndef NO_METAFILE_SUPPORT
if(DY(filact)==SOFTMETAFILE) {
	struct polyline inpoly;
	enum METACODE cod = MFGPLM;

	AGL_plmk(xv,yv,np,np,DY(modeflag),&inpoly);
	(void) fwrite((void *)&cod,sizeof(int),1,DY(metafile));
	(void) fwrite((void *)&mark,sizeof(int),1,DY(metafile));
	AGL_mput(DY(metafile),&inpoly,FALSE);
	if(AGL_status.errstat != AGLNOERR) 
		AGL_siger(modnam);
}
#endif

my_char.modifier.the_char=mark;
my_char.modifier.font=font;
my_char.modifier.slant=0.0;

rescalx = rescaly = DY(symbsize)*DY(scale)*CHARMULT(AGL_status.devid);

rescalx *= RSLX(AGL_status.devid);
rescaly *= RSLY(AGL_status.devid);

AGL_gcdes(&my_char);

AG_TSET(0.0,0.0,0.0,3);

AGL_ch2pl(&my_char,0);

if(AGL_status.errstat != AGLNOERR) 
	AGL_siger(modnam);

for(i=0;i<my_char.descriptor.nstrokes;i++) {
	AG_MAGN(rescalx,rescaly, 
		my_char.descriptor.strokes[i].XV, 
		my_char.descriptor.strokes[i].YV,
	        my_char.descriptor.strokes[i].nvects);
	my_char.descriptor.strokes[i].clipmode = DY(modeflag);
}

first=TRUE;

xoff=0.0; yoff=0.0;

while ( np-- > 0 ) {
	xstart= *xv++; ystart= *yv++;

	if(DY(modeflag)!=NORMAL)
		AGL_cnvt(&xstart,&ystart);	/* Convert coordinates       */

	RBUFFR(0)=xstart;
	RBUFFR(1)=ystart;
	IBUFFR(0)=6;
	CHANNEL = DY(curchan);
	INITFUNCT(AGLDVCOM);
	xstart=RBUFFR(0);
	ystart=RBUFFR(1);

	if(first) {
		AG_TSET(xstart,ystart,0.0,1);
		first=FALSE;
	} else
		AG_TSET(xstart-xoff,ystart-yoff,0.0,1);

	for(i=0;i<my_char.descriptor.nstrokes;i++) {
		AG_TROT(my_char.descriptor.strokes[i].XV,
		        my_char.descriptor.strokes[i].YV,
		        my_char.descriptor.strokes[i].nvects);
		AGL_gpll(&my_char.descriptor.strokes[i]);
	}
	xoff=xstart;
	yoff=ystart;
}
if(AGL_status.errstat != AGLNOERR) 
	AGL_siger(modnam);
}

