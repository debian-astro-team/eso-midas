/* @(#)ag_vers.c	19.1 (ES0-DMD) 02/25/03 13:53:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_vers.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:02   */
/*
 * HEADER : ag_vers.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                               AG_VERS (C callable)                      */

/* AG_VERS    Erase current viewport                                         */


/* Erase current viewport (Note: most devices do not allow partial erase of  */
/* the screen, so the full screen will be erased even if the viewport only   */
/* covers a portion of it)                                                   */

/* If the device drivers allows background color selection, then the current */
/* background color is used. Background color can be set by means of the     */
/* SSET routine.                                                             */


void AG_VERS ()
                                                                         /*--*/
{
static char *modnam = "VERS";
int is;
extern void AG_DMSG();
DEFINE_AGLDVCOM;

AG_DMSG(modnam,(char *)0);

#ifndef NO_METAFILE_SUPPORT
if ( DY(filact) != NOMETAFILE ) {	/* store data into metafile         */
	enum METACODE cod = MFVERS;
	is = fwrite((void *)&cod,sizeof(int),1,DY(metafile));
	if(is != 1) 
		AGL_puterr(MFWRITERR,modnam); 
}
#endif

if(AGL_status.curvwp == VWPEMPTY) {	/* test if viewport is active   */
	AGL_puterr(NOVWPERR,modnam); 
	return;
}

CHANNEL = DY(curchan);
SENDFUNCT(AGLDVCOM);
if(ERRCODE != AGLNOERR) { 
	AGL_puterr(ERRCODE,modnam); 
	return; 
} 

RBUFFR(0) = XVWLOW;
RBUFFR(1) = XVWUP;
RBUFFR(2) = YVWLOW;
RBUFFR(3) = YVWUP;

if( ( (XVWLOW>0.0)||(XVWUP<1.0)||(YVWLOW>0.0)||(YVWUP<1.0)   )
    &&(!(FLAGS(AGL_status.devid)&PARTERASE)     ) ) 
	AGL_puterr(UNSFEATINF,modnam);

CHANNEL = DY(curchan);
IBUFFR(0)=DEFBACKGR(AGL_status.devid);
ERASFUNCT(AGLDVCOM);
if(ERRCODE != AGLNOERR) 
	AGL_puterr(ERRCODE,modnam);
}

