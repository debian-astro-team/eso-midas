/* @(#)ag_getn.c	19.1 (ES0-DMD) 02/25/03 13:52:58 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_getn.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:58   */
/*
 * HEADER : ag_getn.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*CC                            AG_GETN (C callable)                         */

/* AG_GETN    Gets next line from an open file. Also counts lines scanned    */


/* Gets next line from an open file discarding comments, leading blanks,     */
/* blank lines, and so on. Comments are (parts of) lines starting with #.    */
/* # chars not intended to be start of comments can be escaped with another  */
/* # character.                                                              */

/* It differs from the routine AG_GETS only because returns the number of    */
/* input lines read (which may be greater than 1 due to blank or comment     */
/* lines).                                                                   */

/* Note: this routine is callable through the C interface only               */


char *AG_GETN (buffr,len,filpt,nlin)
                              /* Returns pointer to first character, NULL at */
                              /* eof and for input errors.                   */

 char *buffr;                 /* User provided buffer to hold input lines.   */
                              /* It must be longer than the longest input    */
                              /* line (including comments).                  */
 int  len;                    /* User  buffer length                         */
 FILE *filpt;                 /* File pointer  to read from                  */
 int  *nlin;                  /* Returns number of lines read                */
                                                                         /*--*/
{
char *pntr;
*nlin =0;
do {					/* repeat until non blank line or eof*/
	pntr=fgets(buffr,len,filpt);	/* get next line from file           */
	if(pntr==NULL) 
		return pntr;

	(*nlin) ++;

	pntr = strchr(buffr,'\n');	/* Get rid of newline character      */
	if(pntr!=NULL) 
		*pntr = '\0';

	pntr = strchr(buffr,'#');	/* Get rid of comments               */
	if(pntr!=NULL)
           {
	   if(*(pntr+1)=='#')
			strcpy(pntr,(pntr+1));
	    else
			*pntr = '\0';
           }

	for(pntr=buffr+strlen(buffr)-1; /* clean trailing spaces */
	    pntr>=buffr; 
	    pntr--)  
		if(isspace(*pntr))
			*pntr='\0';
		else
			break;

	for(pntr=buffr;			/* Clean leading spaces      */
	    isspace(*pntr); 
	    pntr++);
} while(*pntr == '\0');

return pntr;
}

