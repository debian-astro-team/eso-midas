/* @(#)ag_sset.c	19.1 (ES0-DMD) 02/25/03 13:53:01 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_sset.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:01   */
/*
 * HEADER : ag_sset.c      - Vers 3.6.002  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.001  - Aug 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                         AG_SSET (C callable)                            */

/* AG_SSET    Set status                                                     */


/* Set various status parameters. Parameters and values are specified by a   */
/* command string.                                                           */

void AG_SSET (commnd)

 char *commnd;                /* Command string.                             */

/* Command strings have the following syntax:                                */

/*   "keywd[=value];keywd[=value];....."                                     */

/* Keywords can be truncated at the minimum number of characters needed to   */
/* recognize them (shown uppercase in the list below) and can be typed both  */
/* in upper and lower case; string values must be quoted.                    */

/* Keyword      Description                                                  */

/* BACKgr=color Set background color as specified "color". Each color value  */
/*              is either a number (device dependent) or a standard color    */
/*              x`name. For a list of standard colors see the entry "COLOR"  */
/*              below.                                                       */
/*              Note: the capability to set background color is device de-   */
/*                    pendent, some drivers will not respond to this command.*/
/*                    It is also recommended to send this command before any */
/*                    other graphic output command. Changing the background  */
/*                    color will also usually erase the device.              */

/* BOTTomup     Set string drawing direction bottom up (angle=90 deg)        */
/*              See also: LFRG, RGLF and UPDOWN                              */

/* CHANGle=a    Set string drawing angle to a (see also: bottomup, updown,   */
/*              lfrg, rglf, degrees, radian)                                 */

/* CHBAsic      Set character dimension to "basic"  (See Note 1)             */

/* CHDIms=h,v   Set character dims h (horiz.) v (verti.) (See Note 1)        */

/* CHLArge      Set character dimension to "large"  (See Note 1)             */

/* CHMEdium     Set character dimension to "medium" (See Note 1)             */

/* CHSMall      Set character dimension to "small"  (See Note 1)             */

/* CLR1         Set standard color one (usually black, if background is      */
/*              white, or white if viceversa)                                */
/* CLR2         Set standard color two (usually red, if possible)            */

/* CLR3         Set standard color three (usually red, if possible)          */

/* COLOr=color  Set current drawing color as specified by "color". Color is  */
/*              either a device dependent numeric value or one of the follo- */
/*              wing strings:                                                */

/*              background,black,red,green,blue,yellow,magenta,cyan,white.   */

/*              where 'background' stands for the default background color   */

/*              The color capability is obviously device dependent. Drivers  */
/*              will set the "closest" color.                                */

/* CURSOR=xx    Set cursor style (default is 0, if<0 no cursor is displayed) */
/*              see also AG_VLOC                                             */

/* DEBUg=xx     Set debugging (1: on, 0: off. Default off)                   */

/* DEGRees      Set angular units in degrees (Default. See changle)          */

/* ERRFile=name Redirect error output to file (default is stderr)            */

/* FONT=font    Set character font.                                          */
/*              font may be a numeric value between 0 and 5, or one of the   */
/*              following font names (only the first three characters are    */
/*              significant):                                                */

/*              default (or font 0) : the AGL builtin font                   */
/*              quality (or font 1) : high quality roman font                */
/*              greek   (or font 2) : Greek font                             */
/*              script  (or font 3) : Script font                            */
/*              old     (or font 4) : Old English font with special astro-   */
/*                                    nomical symbols                        */
/*              tiny    (or font 5) : Tiny roman font. Simpler than font 1.  */

/* GEOm         Activate geometrical aspect ratio control                    */

/* IBAckg=color Define the colour of the background to be set at initializa- */
/*              of any graphic device supporting background color setting.   */
/*              Usually the default background color is a device dependent   */
/*              parameter. If a call: AG_SSET("IBACK=<color>"); is issued    */
/*              before a viewport activation which also initialize a graphic */
/*              device, then the default background is set to <color> (if    */
/*              possible). <color> is one of the following strings:          */

/*              background,black,red,green,blue,yellow,magenta,cyan,white.   */

/*              Color=background, or no value, means that the device         */
/*              dependent default background must be set                     */

/*              The color assigned with the call is used for any device      */
/*              activated after the call until a new color or the default    */
/*              one is preset with another call to AG_SSET()                 */

/* LINX         Activate linear transformation on X axis                     */

/* LINY         Activate linear transformation on Y axis                     */

/* LOGX         Activate log transformation on X axis                        */

/* LOGY         Activate log transformation on Y axis                        */

/* LFRG         Set string drawing direction to left to right (angle=0 deg)  */
/*              See also: BOTTOMUP, RGLF and UPDOWN                          */

/* LSTYLe=xx    Set line style. patterns are: 0: solid, 1: dot, 2: dash,     */
/*              3: dot dash, 4: long dash, 5: dot dot dash. Default is 0.    */ 

/* LWIDTh=xx    Set line width. (Optionally supported by device drivers).    */
/*              Values specify increasing line width. Default is 0.          */

/* MFHARd       Set metafile option to "hard" (see AG_MOPN)                  */

/* MFSOFt       Set metafile option to "soft" (default) (see AG_MOPN)        */

/* MOde=mode    Set graphic write mode. The value mode can be one of: Subst, */
/*              Or, Xor, Eqv (the first char only is significant). Default   */
/*              mode is Subst (the new pixel value substitutes the previous  */
/*              one, other values are optionally supported by device drivers.*/

/* MSGAll       Display all information messages                             */

/* MSGError     Display error and severe error messages                      */

/* MSGNone      Suppress any error message                                   */

/* MSGSevere    Display only severe error messages                           */

/* MSGWarning   Display warning, error and severe error messages             */

/* NORMalized   Set graphic mode to "normalized" (see: also USER and SPECIAL)*/

/* NGEOm        Suppress geometrical aspect ratio control (see: GEOM)        */

/* RADIans      Set angular units in radians (Default is deg. See changle)   */

/* RGLF         Set string drawing direction to right to left (angle=180 deg)*/
/*              See also: BOTTOMUP, LFRG and UPDOWN                          */

/* SCALE=nn     Set a global scaling factor for symbols and characters.      */
/*              The dimensions of symbols and characters may be separately   */
/*              modified with: CHSMALL, CHBASIC, CHMEDIUM, CHLARGE and CHDIM,*/
/*              and SYSMALL, SYBASIC, SYMEDIUM, SYLARGE and SYDIM.           */
/*              Default is 1.0                                               */

/* SPECial      Set graphic mode to SPECIAL. In order to set this mode a     */
/*              user defined coordinate transformation must have been set via*/
/*              a call to AG_TRNS. See also items USER and NORMAL            */

/* STPError     Set prog. termination threshold to error                     */

/* STPNever     Set prob. termination threshold to never terminate           */

/* STPSevere    Set prog. termination threshold to severe error              */

/* SYBAsic      Set symbol (marker) dimension to "basic"                     */

/* SYDIm=h      Set symbol dimension to h times "basic" (h is a float value) */

/* SYLArge      Set symbol (marker) dimension to "large"                     */

/* SYMEdium     Set symbol (marker) dimension to "medium"                    */

/* SYSMall      Set symbol (marker) dimension to "small"                     */

/* TWIDTh=xx    Set line width for text strings. (Optionally supported by    */
/*              device drivers). Values specify increasing line width.       */
/*              Default is 0.                                                */
/* USER         Set graphic mode to USER (see also items NORMAL and SPECIAL) */

/* UPDOwn       Set string drawing direction to up down (angle=270 deg)      */
/*              See also: BOTTOMUP, LFRG and LFRG                            */

                                                                         /*--*/
#define FOREVER 1
{
static char *modnam = "SSET";
char *pntr;

if(!(AGL_wasinit&AGL_INIT)) 
	AGL_init();

if(AGL_DEBUG)
	AG_DMSG(modnam,commnd);

#ifndef NO_METAFILE_SUPPORT
if(AGL_status.curvwp != VWPEMPTY)
	if ( DY(filact) != NOMETAFILE ) { 	/* store data into metafile  */
	enum METACODE cod = MFSSET;
	int is;
	int nc=strlen(commnd);
		is = fwrite((char *)&cod,sizeof(int),1,DY(metafile));
		is = fwrite((char *)&nc,sizeof(int),1,DY(metafile));
		is = fwrite(commnd,sizeof(char),nc,DY(metafile));
		if(is != nc) 
			AGL_puterr(MFWRITERR,modnam);
	}
#endif

pntr=commnd;

for(;;) {
char keywd[MAXKEYLNG];

	pntr=AG_SCAN(pntr,';',MAXKEYLNG,keywd);
	if(*keywd=='\0') 
		break;
	(void)AGL_set(keywd);
	if ( AGL_status.errstat != AGLNOERR ) 
		AGL_siger(modnam);
	if(*pntr=='\0') 
		break;
}
}

