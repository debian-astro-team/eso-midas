/* @(#)ag_mres.c	19.1 (ES0-DMD) 02/25/03 13:53:00 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_mres.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:00   */
/*
 * HEADER : ag_cdef.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


#ifndef NO_METAFILE_SUPPORT

/*****************************************************************************/
/*++                             AG_MRES (C callable)                        */

/* AG_MRES    Resume metafile recording                                      */


/* Resumes recording onto the metafile previously suspended by means of a    */
/* call to AG_MSUS function.                                                 */


void AG_MRES ()
                                                                         /*--*/ 
{
static char *modnam = "MRES";
extern void AG_DMSG();

AG_DMSG(modnam,(char *)0);

if (DY(metafile) != NULL)
	DY(filact) = DY(metamode);
else
	AGL_puterr(MFNOTWNG,modnam);
}


#else

void AG_MRES () {}

#endif
