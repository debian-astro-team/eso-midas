/* @(#)ag_stdo.c	19.1 (ES0-DMD) 02/25/03 13:53:01 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_stdo.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:01   */
/*
 * HEADER : ag_stdo.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>



/*****************************************************************************/
/*CC                           AG_STDO (C callable)                          */

/* AG_STDO    Open given file for read on hierarchy of directories           */


/* This function opens a given file for read on a fixed set of directories   */
/* In the following order:                                                   */


FILE *AG_STDO (fname,ext,flag)
                              /* returns file pointer or NULL on failure     */

 char *fname;                 /* File name                                   */
 char *ext;                   /* File extension. This string is concatenated */
                              /* to file name above to get the full name     */
 int  flag;                   /* Directory search disable flag. It is the    */
                              /* sum of the following values                 */

                              /*   1 - Disable search on current directory   */
                              /*   2 - Disable search on HOME directory (*)  */
                              /*   4 - Disable search on AGL standard dir.   */
                              /* Note: the AGL standard directory name is    */
                              /*       searched among environment variable in*/
                              /*       order to allow a runtime definition   */

                              /* (*): not yet implemented                    */
                                                                         /*--*/
{
char aux[PHNAMLNG];
FILE *temp=NULL;
static char *try = "Opening file:";
static char *done = "... done";
static char *notdone = "... not found";

strcpy(aux,fname);
strcat(aux,ext);

if(!(flag&1)) {			/* Search on current directory             */
	AG_DMSG(try,aux);
	temp=fopen(aux,"r");
	if(temp!=NULL) {
		AG_DMSG(done,"");
		return(temp);
	} else
		AG_DMSG(notdone,"");
}

if(!(flag&4)) {
	AG_DMSG(AGLSTDIR,"Translated");
	AGL_trns(AGLSTDIR,PHNAMLNG,aux);	/* Search into environment */
	AG_DMSG("..into",aux);
	strcat(aux,fname);
	strcat(aux,ext);
	AG_DMSG(try,aux);
	temp=fopen(aux,"r");
	if(temp!=NULL) {
		AG_DMSG(done,"");
		return(temp);
	} else
		AG_DMSG(notdone,"");
}
return temp;
}

