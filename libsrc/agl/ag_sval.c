/* @(#)ag_sval.c	19.1 (ES0-DMD) 02/25/03 13:53:01 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_sval.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:01   */
/*
 * HEADER : ag_sval.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>



/*****************************************************************************/
/*CC                           AG_SVAL (C callable)                          */

/* AG_SVAL    Get string argument                                            */


/* Gets the string after the = sign from a string with the following syntax: */

/*                "xxxxx=string"                                             */

/* where xxxx is any string not including the character '='.                 */

/* Note: this routine is callable through the C interface only               */


int AG_SVAL (in,nvals,out)  
                              /* Returns output string length                */

 char *in;                    /* Input string                                */
 int  nvals;                  /* Max output string length                    */
 char *out;                   /* Output string (at least nvals+1 long)       */
                                                                         /*--*/
{
int nv=0,clean=TRUE;

*out = '\0';

for(;;) { 				/* scan till the '='           */
	if(*in=='\0')
		break;
	if(*in++ == '=')
		break;
}

while( isspace(*in) ) 
	in++;				/* discard leading blanks      */

if(*in == '\0') {
	*out='\0';
	return (nv);
} 

if(*in == '"') {
	clean=FALSE;
	in++;
}

while( (*in!='\0')&&(*in!='"') ) {
	if(nv>=nvals) 
		break;
	*out++ = *in++;
	nv++;
}
*out--='\0';

if(clean)
	while(isspace(*out)) {
		*out-- = '\0';
		nv--;
	}

return nv;
}
