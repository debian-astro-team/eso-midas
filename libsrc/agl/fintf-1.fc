/* @(#)fintf.fc	19.1 (ES0-DMD) 02/25/03 13:53:06 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)fintf.fc	19.1  (OAA-ASTRONET) 02/25/03 13:53:06   */
/*
 * HEADER : fintf.fc    - Vers 3.6.001  - Sep 1993 -  L. Fini, M. Pucillo
 *                      - Vers 3.6.000  - Oct 1991 -  L. Fini, M. Pucillo
 *
 *
 * AGL FORTRAN to C interface -- 	Generic version to be filtered 
 *					in order to generate the machine 
 *					dependent code.
 *
 *
 * This module is a C layer needed to interface the AGL FORTRAN interface
 * Layer to the AGL standard C interface
 *
 * Entry points correspond one-to-one to standard C interface (except for
 * AGIDN routine which is directly implemented in FORTRAN in file agidn.for
 *
 * The two routines: AG_SCAN, AG_DMSG are available in the C interface only
 *
 * Modified: 910617. CG: Master Fortran to C file. 
 * Modified: 910825. CG: This code only compiled for Unix systems.
 * Modified: 920115. CG: For both VMS and UNIX.
 *
 *
 * ===============================
 *  AGL FORTRAN STANDARD INTERFACE
 *
 *
 *
 *
 *
 *  The following entry points define the AGL standard FORTRAN interface
 *
 *
 * I   - Status operations
 *
 * AGCLS
 *
 * AGCDEF ( XCLIP0, XCLIP1, YCLIP0, YCLIP1 ) 
 * 
 * AGSSET (STRING)
 *
 * AGVDEF ( DEVICE, IDENT, XA, XB, YA, YB, XLIM, YLIM )
 *
 * AGVKIL
 * 
 * AGVSEL ( IDENT )
 *
 * AGWDEF ( X0, X1, Y0, Y1 )
 *
 * 
 * II  - Graphic primitives
 *
 * AGGPLG ( X, Y, N )
 *
 * AGGPLL ( X, Y, N )
 * 
 * AGGPLM ( X, Y, N, MARK )
 *
 * AGGINT ( X, Y, N )
 *
 * AGGTXT ( XT, YT, STRING, CENTRE )
 *
 * AGVERS
 * 
 *
 * III - Get information
 *
 * AGIGET (STRING,IVECT)
 * 
 * AGRGET (STRING,RVECT)
 * 
 * AGTGET ( STRING, XL, YL )
 * 
 * AGVLOC ( X, Y, CHAR, PIXVAL )
 *
 * AGVLOS ( X, Y, MAXLEN, STR, PIXVAL )
 *
 *
 * IV  - Metafile manipulation
 *
 * AGMCLS
 * 
 * AGMOPN ( NAME )
 * 
 * AGMRDW ( MFILE )
 *
 * AGMRES
 * 
 * AGMSUS
 *
 *
 * V   - Miscellaneous
 *
 * AGESC ( CMD, CMDLEN )
 *
 * AGDRIV 
 *
 * AGIDN ( IDENT )
 * 
 * AGMAGN ( XFACT, YFACT, X, Y, N )
 *
 * AGTROT ( X, Y, N )
 *
 * AGTSET ( XFACT, YFACT, ANGLE, ITEM )
 *
 * AGVN2U ( XN, YN, XV, YV )
 * 
 * AGVU2N ( XU, YU, XN, YN )
 *
 * AGVUPD 
 *
 * VI   - High level routines
 *
 * AGAXES ( X0, X1, Y0, Y1, CMDSTR )
 *
 * AGAXIS ( TYPE, DATA, LSPACE, FORM, LABEL )
 *
 * AGORAX ( TYPE, ENDS, DATA, FORM, LABEL )
 *
 * AGHIST ( X, Y, N, MODE, JOIN )
 *
 * AGNLIN ( X0, X1, Y0, Y1, CMDSTR )
 *
 */

#include <ftoc.h>

#ifndef MIN
#define MIN(x,y)        ((x) <= (y) ? (x):(y) )    /* Minimum */
#endif /* MIN */

/* I   - Status operations */

ROUTINE AGCDEF (x1,x2,y1,y2)
float *x1;
float *x2;
float *y1;
float *y2; 
{ 
	AG_CDEF ( *x1, *x2, *y1, *y2 ); 
}

ROUTINE AGCLS () { AG_CLS (); }

SUBROUTINE AGSSET (command)
CHARACTER command;       
{ 
	AG_SSET ( STRIPPED_STRING(command) ); 
}

SUBROUTINE AGVDEF ( device, xa,xb,ya,yb,xlim,ylim)
CHARACTER device;
float *xa;
float *xb;
float *ya;
float *yb;
float *xlim;
float *ylim;
{
  int ret;
  ret = AG_VDEF(STRIPPED_STRING(device), *xa, *xb, *ya, *yb, *xlim, *ylim);
  return(ret);
}

ROUTINE AGVKIL () { AG_VKIL (); }

ROUTINE AGVSEL (id)
fint2c *id;        
{ 
	AG_VSEL ( *id ); 
}

ROUTINE AGWDEF (x1,x2,y1,y2)
float *x1;
float *x2;
float *y1;
float *y2; 
{ 
	AG_WDEF ( *x1, *x2, *y1, *y2 ); 
}

/* II  - Graphic primitives */

ROUTINE AGGPLG (xv,yv,np)
float xv[];
float yv[];
fint2c *np;
{ 
	AG_GPLG ( xv, yv, *np ); 
}

ROUTINE AGGPLL (xv,yv,np)
float xv[];
float yv[];
fint2c *np;
{ 
	AG_GPLL ( xv, yv, *np ); 
}

ROUTINE AGGPLM (xv,yv,np,mark)
float xv[];
float yv[];
fint2c  *np;
fint2c  *mark;
{ 
	AG_GPLM ( xv, yv, *np, *mark ); 
}

ROUTINE AGGINT (xv,yv,np)
float xv[];
float yv[];
fint2c *np;
{ 
	AG_GINT ( xv, yv, *np ); 
}

SUBROUTINE AGGTXT (xc,yc,str,cntr)
float *xc;
float *yc;
CHARACTER str;
fint2c  *cntr;
{ 
	AG_GTXT ( *xc, *yc, STRIPPED_STRING(str), *cntr ); 
}

ROUTINE AGVERS () { AG_VERS (); }

SUBROUTINE AGGERR (code,mesg)
fint2c *code;
CHARACTER mesg; 		
{ 
	AG_GERR( *code, STRIPPED_STRING(mesg) ); 
}


ROUTINE AGISET (ints,numb) 
fint2c ints[],numb;  
{ 
	AG_ISET ( ints, numb ); 
}

ROUTINE AGRSET (floats,numb) 
float floats[]; int numb; 
{ 
	AG_RSET(floats,numb); 
}

/* III - Get information */

SUBROUTINE AGIGET (item,ival) 
CHARACTER item;
fint2c *ival;
{
	AG_IGET (  STRIPPED_STRING(item), ival ); 
}

SUBROUTINE AGRGET (item,fval) 
CHARACTER item;
float *fval;
{ 
	AG_RGET ( STRIPPED_STRING(item), fval ); 
}

SUBROUTINE AGTGET ( chstrg, xd, yd )
CHARACTER chstrg;
float  *xd;
float  *yd;
{
	AG_TGET ( STRIPPED_STRING(chstrg), xd, yd ); 
}

ROUTINE AGVLOC (xv,yv,key,pixval)
float *xv;
float *yv;
fint2c *key;
fint2c *pixval;
{
	AG_VLOC ( xv, yv, key, pixval ); 
}

SUBROUTINE AGVLOS (xv,yv,maxlen,chstrg,pixval)
float *xv;
float *yv;
fint2c  *maxlen;
CHARACTER chstrg;
fint2c  *pixval;
{
	int lng;
	char tmpstrg[512];

	lng = MIN(*maxlen,512);
	lng = MIN(lng,sizeof(STRIPPED_STRING(chstrg)));
	AG_VLOS ( xv, yv, lng, tmpstrg, pixval ); 
	STRFCOPY(chstrg,tmpstrg);
}

/* IV  - Metafile manipulation */

ROUTINE AGMCLS () { AG_MCLS (); }

SUBROUTINE AGMOPN ( fname )
CHARACTER fname;
{
	AG_MOPN ( STRIPPED_STRING(fname) ); 
}

SUBROUTINE AGMRDW ( metafile )
CHARACTER metafile;
{
	AG_MRDW ( STRIPPED_STRING(metafile) ); 
}

ROUTINE AGMRES () { AG_MRES (); }

ROUTINE AGMSUS () { AG_MSUS (); }

/* V  - Miscellaneus */

SUBROUTINE AGESC (cmd,cmdlen)
CHARACTER cmd;
fint2c *cmdlen;
{
	AG_ESC ( STRIPPED_STRING(cmd), *cmdlen ); 
}

ROUTINE AGDRIV () { AG_DRIV (); }

ROUTINE AGMAGN (xfact,yfact,xv,yv,np)
float *xfact;
float *yfact;
float xv[];
float yv[];
fint2c *np;
{
	AG_MAGN( *xfact, *yfact, xv, yv, *np ); 
}

ROUTINE AGTROT (xv,yv,np)
float xv[];
float yv[];
fint2c *np;
{
	AG_TROT ( xv, yv, *np ); 
}

ROUTINE AGTSET (xoff,yoff,angle,item)
float *xoff;
float *yoff;
float *angle;
fint2c   *item;
{
	AG_TSET ( *xoff, *yoff, *angle, *item ); 
}

ROUTINE AGVN2U (xn,yn,xu,yu)
float *xn;
float *yn;
float *xu;
float *yu;
{
	AG_VN2U ( *xn, *yn, xu, yu ); 
}

ROUTINE AGVU2N (xu,yu,xn,yn)
float *xu;
float *yu;
float *xn;
float *yn;
{
	AG_VU2N ( *xu, *yu, xn, yn ); 
}

ROUTINE AGVUPD () { AG_VUPD (); }

/* VI - High level routines */

SUBROUTINE AGAXES (x1,x2,y1,y2,string)
float *x1;
float *x2;
float *y1;
float *y2; 
CHARACTER string;
{ 
	AG_AXES ( *x1, *x2, *y1, *y2, STRIPPED_STRING(string) ); 
}

SUBROUTINE AGAXIS ( type, data, lspace, form, label )
fint2c *type;
float *data;
float *lspace;
CHARACTER form;
CHARACTER label;
{ 
	AG_AXIS( *type, data, *lspace, STRIPPED_STRING(form), STRIPPED_STRING(label) );
}

SUBROUTINE AGORAX ( type, ends, data, form, label )
fint2c *type;
float *ends;
float *data;
CHARACTER form;
CHARACTER label;
{ 
	AG_ORAX( *type, ends, data, STRIPPED_STRING(form), STRIPPED_STRING(label) );
}

ROUTINE AGHIST (xv,yv,np,mode,join)
float *xv;
float *yv; 
fint2c *np;
fint2c *mode;
fint2c *join;
{ 
	AG_HIST ( xv, yv, *np, *mode, *join ); 
}

SUBROUTINE AGNLIN (x1,x2,y1,y2,string)
float *x1;
float *x2;
float *y1;
float *y2; 
CHARACTER string;
{ 
	AG_NLIN ( *x1, *x2, *y1, *y2, STRIPPED_STRING(string) ); 
}

SUBROUTINE AGFILL(x,y,n,space,angle,sset)
float *x;
float *y;
fint2c   *n;
float *space;
float *angle;
CHARACTER sset;
{
       AG_FILL( x, y, *n, *space, *angle, STRIPPED_STRING(sset) );
}

