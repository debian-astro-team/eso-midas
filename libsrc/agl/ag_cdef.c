/* @(#)ag_cdef.c	19.1 (ES0-DMD) 02/25/03 13:52:57 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_cdef.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:57   */
/*
 * HEADER : ag_cdef.c      - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>





/*****************************************************************************/
/*++                         AG_CDEF (C callable)                            */

/* AG_CDEF    Define clipping area                                           */


/* Redefine the clipping area. Clipping area is a subset of the current view-*/
/* port onto which graphic data are mapped when in USER mode. It defaults to */
/* the current viewport limits, but may be defined smaller than that, e.g.   */
/* in order to leave space for labels and comments around the plot.          */

/* Clipping area redefinition also resets aspect ratio control (see AG_SSET, */
/* item: "GEOM").                                                            */

void AG_CDEF (x1,x2,y1,y2)

 double x1,x2,y1,y2;          /* Clipping area bounds [0.0..1.0]. They must  */
                              /* be also enclosed into the current viewport. */
                              /* (x1<x2) and (y1<y2)                         */
                                                                         /*--*/
{
static char *modnam = "CDEF";
float xxx[4];
int is;
extern void AG_DMSG();
extern void AGL_sgbl();

AGL_status.errstat=AGLNOERR;

AG_DMSG(modnam,(char *)0);

xxx[0]=x1; xxx[1]=x2; xxx[2]=y1; xxx[3]=y2;

#ifndef NO_METAFILE_SUPPORT
if ( DY(filact) == SOFTMETAFILE ) { 		/* store data into metafile  */
	enum METACODE cod = MFCDEF;
	is = fwrite((char *)&cod,sizeof(int),1,DY(metafile));
	is = fwrite((char *)xxx,sizeof(float),4,DY(metafile));
	if(is != 4) 
		AGL_puterr(MFWRITERR,modnam);
}
#endif

if(AGL_status.curvwp == VWPEMPTY) { 	/* test if viewport active       */
	AGL_puterr(NOVWPERR,modnam);
	return;
}


if((x1<XVWLOW)||(x2>XVWUP)||           /* test params. validity            */
   (y1<YVWLOW)||(y2>YVWUP)||
   ((x2-x1) <= 0.0)||
   ((y2-y1) <= 0.0)  )  { 
	AGL_puterr(ILLBOUWNG,modnam); 
	return; 
}

XCLOW = x1;                            /* Current clipping area            */
XCLUP = x2;                            /* (incl. aspect ratio control)     */
YCLOW = y1;
YCLUP = y2;

XSVLOW = x1;                           /* Original clipping area           */
XSVUP = x2;                            /* (No aspect ratio control)        */
YSVLOW = y1;
YSVUP = y2;

AGL_sgbl();
if(AGL_status.errstat != AGLNOERR) 
	AGL_siger(modnam);
}

