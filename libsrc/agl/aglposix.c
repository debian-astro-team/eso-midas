/* @(#)aglposix.c	19.1 (ES0-DMD) 02/25/03 13:53:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
 * HEADER : aglposix.c     - Vers 3.6.001  - Sep 1993 -  C.Guirao, ESO. L.Fini, OAA
 *                         - Vers 3.6.000  - May 1993 -  C.Guirao, ESO
 *
 * This file contains system dependent features which are common to all
 * Unix POSIX systems
 */


/***************************************************************************/
/*                      AGL_setraw  AGL_reset                              */

/* This routines are used to set the specified device handler in RAW mode  */
/* and to reset it in COOKED mode thereafter                               */

static struct termios argp, oargp;

void AGL_setraw (filept)
FILE * filept;
{
int handler = fileno(filept);
(void)tcgetattr(handler,&oargp);        /* Get current terminal status  */
(void)tcgetattr(handler,&argp);         /* Save current terminal status  */
argp.c_lflag &= ~(ICANON|ISIG|ECHO);
argp.c_cc[VTIME] = 0;
argp.c_cc[VMIN]  = 1;
(void)tcsetattr(handler,TCSANOW, &argp); /* Force RAW mode               */
}


void AGL_reset(filept)
FILE * filept;
{
int handler = fileno(filept);
(void)tcsetattr(handler,TCSANOW, &oargp); /* Reset terminal status         */
}
