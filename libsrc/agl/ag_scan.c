/* @(#)ag_scan.c	19.1 (ES0-DMD) 02/25/03 13:53:01 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_scan.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:01   */
/*
 * HEADER : ag_scan.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*CC                          AG_SCAN (C callable)                           */


/* AG_SCAN    Parse a string                                                 */


/* Scans a string and gets the characters up to the next delimiter or to the */
/* end of the string. During the scanning characters before an '=' sign are  */
/* converted in lower-case and blanks are suppressed. After the '=' sign (if */
/* any) character are left unaltered and blanks are retained as such.        */

/* Note: this routine is callable through the C interface only               */


char *AG_SCAN (chstrg,delim,tklen,token)
                              /* Returns pointer after delim (it will point  */
                              /* to '\0' at the end)                         */

 char *chstrg;                /* Input string                                */
 int delim;                   /* Token delimiter character                   */
                              /* A delimiter escaped with a  preceeding '\'  */
                              /* will not be considered as delimiter, but    */
                              /* will be included in the string. The escape  */
                              /* character '\' CANNOT be used as delimiter.  */
 int tklen;                   /* Max token length                            */
 char *token;                 /* Output token (truncated at tklen)           */
                                                                         /*--*/

#define EQUAL 	'='
#define ESCAPE 	'\\'

{
char *src, *dst;
int lowcas=TRUE;

src=chstrg; dst=token;
while ( --tklen ) {
	register int ch;
	
	if((ch= *src++)==ESCAPE) {
		if(*src==delim)
			ch= *src++;
	} else if(ch==delim)
		break;
	if(ch=='\0') {
		src--;
		break;
	}
	if( ch == EQUAL ) 
		lowcas = FALSE;
	if(lowcas) {
		*dst++ = TOLOWER(ch);
	} else
		*dst++ = ch;
}
*dst='\0';
return (src);
}

