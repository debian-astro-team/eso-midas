/* @(#)ag_vkil.c	19.1 (ES0-DMD) 02/25/03 13:53:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_vkil.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:02   */
/*
 * HEADER : ag_vkil.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>




/*****************************************************************************/
/*                               killvw (AGL  internal use)                  */

/* Kill the current viewport                                                 */

static void killvw()
{
DEFINE_AGLDVCOM;
void AGL_fvwp();

if(AGL_status.vwpoint==NULL) { 
	AGL_status.errstat=(NOVWPERR); 
	return; 
}

#ifndef NO_METAFILE_SUPPORT
if(DY(metafile) != NULL) 
	fclose(DY(metafile)); 		/* Close current metafile  */
#endif

CHANNEL = DY(curchan);
SENDFUNCT(AGLDVCOM);				/* Empty viewport buffer   */

if(--ACTREQST(AGL_status.devid) == 0)	/* Decrement device request counter */
	AGL_dstop(AGL_status.devid);	/* Close device                     */

AGL_fvwp(AGL_status.curvwp);
AGL_status.errstat=(ERRCODE);
}


/*****************************************************************************/
/*++                              AG_VKIL (C callable)                       */

/* AG_VKIL    Kill current viewport                                          */


/* Kill the current viewport. If a metafile is currently associated to the   */
/* viewport it is closed.                                                    */

void AG_VKIL ()
                                                                         /*--*/
{
static char *modnam = "VKIL";
extern void AG_DMSG();

AG_DMSG(modnam,(char *)0);

AGL_vflsh();                      /* Empty viewport buffer                  */
killvw();
if(AGL_status.errstat != AGLNOERR) AGL_siger(modnam);
}

