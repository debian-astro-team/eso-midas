/* @(#)aglutil.c	19.1 (ES0-DMD) 02/25/03 13:53:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)aglutil.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:05   */
/*
 * HEADER : aglutil.c      - Vers 3.6.001  - Sep 1993 -  L. Fini, Oss. Arcetri
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, Oss. Arcetri
 */




/*                            GENERIC UTILITIES                              */




#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*                         AGL_scmp (Internal AGL use)                      */

/* string comparison (matching only) with wildcards                          */

int AGL_scmp(string1,string2)
char *string1, *string2;
{
int is = TRUE;

do {
	if((*string1=='*')||(*string2=='*')) 
		break;

	if(*string1 != *string2) { 
		is=FALSE; 
		break; 
	}
}
while((*string1++)&&(*string2++));

return(is);
}





/*****************************************************************************/
/*                      AGL_cnvt  (Internal AGL use)                         */

/* Convert a couple of user mode coordinates into the corresponding          */
/* normalized coordinates taking into account current active transformations.*/
/* Output values are stored into input variables.                            */

/* METHOD:                                                                   */

/* Variables aglfcx, aglfcy, aglofx, aglofy define current transformation    */
/* parameters. If the log transformation is active on any axis logarithm of  */
/* the value is firstly computed. If a user transformation is currently      */
/* active it is also applied.                                                */

/* The routine returns a cursor position code according to the following     */
/* table:                                                                    */

/*                   xlow       xup                                          */
/*                    |          |                                           */
/*           stat = 9 | stat = 8 | stat = 10                                 */
/*      yup ----------+----------+----------                                 */
/*           stat = 1 | stat = 0 | stat = 2                                  */
/*      ylow----------+----------+----------                                 */
/*           stat = 5 | stat = 4 | stat = 6                                  */
/*                    |          |                                           */

/* If any error is detected the routine returns with CRSUNDEF (-1)           */


int AGL_cnvt(xpn,ypn)
double *xpn,*ypn;			/*  Coordinate variables.            */
{
double X_aux = *xpn, Y_aux = *ypn;
int ret;

CNVRT_BEGIN
CNVRT_LOGX(ret,X_aux,Y_aux)
CNVRT_LOGY(ret,X_aux,Y_aux)
CNVRT_USRT0(ret,X_aux,Y_aux)
CNVRT_USRT1(ret,X_aux,Y_aux)
CNVRT_USER0(ret,X_aux,Y_aux)
CNVRT_USER1(ret,X_aux,Y_aux)

*xpn = X_aux;
*ypn = Y_aux;
return ret;
}


/*****************************************************************************/
/*                  Reverse coordinate conversion (Internal AGL use)         */

int AGL_n2u (xpn,ypn,xu,yu)	/* return status                     */
double  xpn,ypn;			/* Normalized input coordinates      */
double  *xu,*yu;			/* User output coordinates           */
                                                                         /*--*/
{
double wrkx,wrky;

int status=AGLNOERR;

if(!DY(window)) 
	return NOWNDERR;

if ( (xpn<XVWLOW)||(xpn>XVWUP) ) status=VWPOUTWNG;
if ( (ypn<YVWLOW)||(ypn>YVWUP) ) status=VWPOUTWNG;

if ( (xpn<XCLOW)||(xpn>XCLUP) ) status=CLPOUTINF;
if ( (ypn<YCLOW)||(ypn>YCLUP) ) status=CLPOUTINF;

wrkx = (xpn - AGL_status.aglofx) / AGL_status.aglfcx;
wrky = (ypn - AGL_status.aglofy) / AGL_status.aglfcy;

if(AGL_status.UTransf) 
	(*DY(UsrRTransf))(&wrkx,&wrky);

if(DY(flogx)) 
	*xu = exp(wrkx);
else
	*xu = wrkx;

if(DY(flogy)) 
	*yu = exp(wrky);
else
	*yu = wrky;

return status;
}







/*****************************************************************************/
/*                      AGL_rcvt  (Internal AGL use)                         */

/* Converts a couple of normalized coordinates into corresponding            */
/* coordinates according to current conversion mode (i.e.: if the mode is    */
/*  user it converts to user coordinates, else returns the same values       */


int AGL_rcvt(xpn,ypn)
double *xpn,*ypn;			/*  Coordinate variables.            */
{
int AGL_n2u();
int status=AGLNOERR;

if(DY(modeflag)!=NORMAL) 
	status=AGL_n2u(*xpn,*ypn,xpn,ypn);
else 
	if( (*xpn<XVWLOW)||(*xpn>XVWUP)||
	    (*ypn<YVWLOW)||(*ypn>YVWUP)  ) 
			status = VWPOUTWNG ;

return status;
}
