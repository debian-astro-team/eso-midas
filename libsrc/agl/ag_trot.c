/* @(#)ag_trot.c	19.1 (ES0-DMD) 02/25/03 13:53:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_trot.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:02   */
/*
 * HEADER : ag_trot.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


static double xoffset=0.0;
static double yoffset=0.0;
static double cosa=1.0;
static double sinx=0.0;
static double siny=0.0;



/*****************************************************************************/
/*++                         AG_TROT (User callable)                         */

/* AG_TROT    Rotate and translate a polyline                                */


/* This module rotates and translates a couple of X-Y coordinate vectors     */
/* In the rotation the different resolution along the two axes is taken into */
/* account                                                                   */

/* The amount of translation and the rotation angle must be set via a call   */
/* to AG_TSET prior of the call to AG_TROT and they will remain in effect    */
/* until modified by another call to AG_TSET.                                */


void AG_TROT (xv,yv,np)

 float xv[],yv[];             /* Coordinate arrays                           */
 int np;                      /* Array length                                */
                                                                         /*--*/ 
{
float *XP=xv;
float *YP=yv;

while(np-- > 0) {
	register double xx,yy;
	xx = *XP;
	yy = *YP;
	*XP++ = xx*cosa - yy*sinx + xoffset;
	*YP++ = xx*siny + yy*cosa + yoffset;
}
}


/*****************************************************************************/
/*++                         AG_TSET (User callable)                         */

/* AG_TSET    Preset translation and angle for routine AG_TROT               */


/* This module presets parameters (translation amount and rotation angle) for*/
/* routine AG_TROT. The values remain into effect until modified with another*/
/*  call to AG_TSET.                                                         */


void AG_TSET (xoff,yoff,angle,item)

 float xoff,yoff;             /* X and Y offset                              */
 float angle;                 /* Rotation angle (rad).                       */
 int item;                    /* Specifies which items are to be modified.   */
                              /* values can be:                              */

                              /* 0: do nothing. Xoff, yoff and angle ignored */
                              /* 1: modify X and Y offset only. Angle is     */
                              /*    ignored                                  */
                              /* 2: modify angle only. Xoff anf yoff are     */
                              /*    ignored                                  */
                              /* 3: modify X and Y offsets and angle         */
                                                                         /*--*/ 
{
if(item&1) {
	xoffset=xoff;
	yoffset=yoff;
}
if(item&2) {
	cosa=cos(angle);
	sinx=sin(angle);
	siny=sinx*RATXY(AGL_status.devid);
        sinx *= RATYX(AGL_status.devid);
}
}


