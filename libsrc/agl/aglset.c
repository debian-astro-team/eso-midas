/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
 * HEADER : aglset.c       - Vers 3.6.006  - Dec 1993 -  L. Fini, OAA
 *                         - Vers 3.6.005  - Dec 1993 -  L. Fini, OAA
 *                         - Vers 3.6.004  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.003  - Aug 1993 -  L. Fini, OAA
 *
 * 090416	last modi
 */



/*                           SET parameter routines                          */





#include <aglenvr.h>
#include <aglstat.h>





/************************************************************************/
/*                      setbckgr (internal AGL use)                     */

/* This routine commands the current driver to set the currently defi-  */
/* ned background color                                                 */

static void setbckgr()
{
DEFINE_AGLDVCOM;

if((FLAGS(AGL_status.devid)&SETBACKGR)==0) {
	CURBACKGR(AGL_status.devid)=DEFBACKGR(AGL_status.devid);
	AGL_status.errstat = UNSFEATWNG;
}
IBUFFR(0)=CURBACKGR(AGL_status.devid);
RBUFFR(0) = XVWLOW;
RBUFFR(1) = XVWUP;
RBUFFR(2) = YVWLOW;
RBUFFR(3) = YVWUP;
CHANNEL=DY(curchan);
ERASFUNCT(AGLDVCOM);
AGL_status.errstat=(ERRCODE<AGL_status.errstat)?AGL_status.errstat: ERRCODE;
}



/**************************************************************************/
/*                      setwrmo (internal AGL use)                        */

/* This routine commands the current driver to set the current write mode */

static void setwrmo()
{
DEFINE_AGLDVCOM;

if(AGL_status.vwpoint==NULL)
	AGL_status.errstat=NOVWPERR;
else {

	IBUFFR(0)=5; 
	*CHARBUF=DY(wrmode); 
	CHANNEL=DY(curchan);
	INITFUNCT(AGLDVCOM);
	AGL_status.errstat=(ERRCODE);
}
}

/*****************************************************************************/
/*                            parse    (Internal use)                        */

/* parse a numerical/string value                                            */

static int parse(intstr,tbl,lng)
char *intstr;			/* String to parse. If number, return it.    */
                                /* else search string in the table and       */
                                /* return the index (or -1 if not found)     */
char tbl[][3];			/* Table                                     */
int  lng;			/* Table length                              */
{
char aux[11];
char *pntr;
int k;

pntr=aux;
if(AG_SVAL(intstr,10,aux)<=0)
	return (-1);

if(isdigit(*pntr)) {
	k=atoi(pntr);
} else {
	for(k=0; k<lng; k++) 
		if((pntr[0]==tbl[k][0])&&
		   (pntr[1]==tbl[k][1])&&
		   (pntr[2]==tbl[k][2])  ) break;
	if(k>=lng)
		k=(-1);
}
return k;
}


/*****************************************************************************/
/*                            parsecol (Internal use)                        */

/* parse color string to get color index                                     */

static char color_tbl[AGLCOLORS][3] 	= { 	{ 'b','a','c'},
						{ 'b','l','a'},
						{ 'r','e','d'},
						{ 'g','r','e'},
						{ 'b','l','u'},
						{ 'y','e','l'},
						{ 'm','a','g'},
						{ 'c','y','a'},
						{ 'w','h','i'}  };

static int parsecol(intstr)
char *intstr;
{
int color;

color=parse(intstr,color_tbl,AGLCOLORS);

if(color==0)
	color = DEFBACKGR(AGL_status.devid);
else if (color>0)
   {
	if(color>=AGLCOLORS)
		color = DEFFOREGR(AGL_status.devid);
	else
		color = AGLCOLOR(AGL_status.devid,color);
   }
return color;
}


/*****************************************************************************/
/*                           parsefont (Internal use)                        */

/* parse font string to get font index                                       */

static char font_tbl[6][3] 	= { 	{ 'd','e','f' },
					{ 'q','u','a' },
					{ 'g','r','e' },
					{ 's','c','r' },
					{ 'o','l','d' },
					{ 't','i','n' }
};

static int parsefont(intstr)
char *intstr;
{
int the_font;

the_font=parse(intstr,font_tbl,AGLCOLORS);

if(the_font<0)
	the_font = 0;

return the_font;
}


/*****************************************************************************/
/*                            aglsetB (Internal use)                         */

/* Set status parameters for keywords: B...                                  */

static int aglsetB(pntr)
char *pntr;
{ 
int auxcl;

switch(*pntr++) {
case 'o':				/* keywd="BO...."            */
	DY(changle)=1.5707963; 
	break;

case 'a':				/* keywd="BA...."            */
	if((auxcl=parsecol(pntr)) >= 0) {
		CURBACKGR(AGL_status.devid) = auxcl;
		setbckgr();
	} else
		AGL_status.errstat=ILLCLRWNG;
	break;

default: 
	AGL_status.errstat=ILLCODWNG; 
	break;
}
return 0;
}


/*****************************************************************************/
/*                            aglsetC (Internal use)                         */

/* Set status parameters for keywords: C...                                  */

static int aglsetC(pntr)
char *pntr;                    /* Keyword pointer (after the C)             */
{
int lvl;
float dd[2];

switch(*pntr++) {
	case 'h':			/* keywd="CH...."                   */
		switch(*pntr++) {
			case 'a':			/*  keywd="CHA..."  */
				AG_RVAL(pntr,(int)1,dd);
				DY(changle) = dd[0] * DY(angfct);
				break;
			case 'd':			/*  keywd="CHD..."  */
				AG_RVAL(pntr,(int)2,dd);
				DY(chrhsize)=dd[0];
				DY(chrvsize)=dd[1];
				break; 
			case 's':			/*  keywd="CHS..."  */
				DY(chrhsize)=0.5;
				DY(chrvsize)=0.5;
				break;
			case 'b':			/*  keywd="CHB..."  */
				DY(chrhsize)=1.0;
				DY(chrvsize)=1.0;
				break;
			case 'm':			/*  keywd="CHM..."  */
				DY(chrhsize)=2.0;
				DY(chrvsize)=2.0;
				break;
			case 'l':			/*  keywd="CHL..."  */
				DY(chrhsize)=3.0;
				DY(chrvsize)=3.0;
				break;
			default: 
				AGL_status.errstat=ILLCODWNG; 
				break;
		}
		break;
	case 'l':			/*  keywd="CL..."                   */
		pntr++;
		switch(*pntr++) {
			case '1':			/*  keywd="CLR1..." */
				DY(color)=DEFFOREGR(AGL_status.devid);
				AGL_sclr();
				break;
			case '2':			/*  keywd="CLR2..." */
				DY(color)=AGLRED(AGL_status.devid);
				AGL_sclr();
				break;
			case '3':			/*  keywd="CLR3..." */
				DY(color)=AGLBLUE(AGL_status.devid); 
				AGL_sclr();
				break;
			default: AGL_status.errstat=ILLCODWNG; break;
		}
		break;
	case 'o':			/*  keywd="CO...."                  */
		lvl = parsecol(pntr);
		if( lvl<0 ) { 
			lvl=0; 
			AGL_status.errstat=ILLCLRWNG; 
		}
		if ( lvl>NPLANS(AGL_status.devid) ) { 
			lvl=NPLANS(AGL_status.devid); 
			AGL_status.errstat=ILLCLRWNG; 
		}
		DY(color)=lvl;
		AGL_sclr();
		break;
	case 'u':			/*  keywd="CU...."                  */
		AG_IVAL(pntr,(int)1,&lvl);
		DY(curstyl)=lvl;
		break;
	default: 
		AGL_status.errstat=ILLCODWNG; 
		break;
}
return 0;
}

/*****************************************************************************/
/*                            aglsetD (Internal use)                         */

/* Set status parameters for keywords: D...                                  */

static int aglsetD(pntr)
char *pntr;                      /* Keyword pointer (after the D)             */
{
switch(*pntr++) {
case 'e':			/* keywd="DE...."                     */
	switch(*pntr++) {
	case 'b':		/* keywd="DEB..."             */
		AG_IVAL(pntr,(int)1,&AGL_DEBUG);
		break;

	case 'g':		/* keywd="DEG..."             */
		if(AGL_status.curvwp == VWPEMPTY)      /* test viewport */
			AGL_status.errstat=NOVWPERR; 
		else
			DY(angfct)=0.017453293;
		break;
	default: 
		AGL_status.errstat=ILLCODWNG; 
		break;
	}
	break;

default: 
	AGL_status.errstat=ILLCODWNG; 
	break;
}
return 0;
}



/*****************************************************************************/
/*                            aglsetE (Internal use)                         */

/* Set status parameters for keywords: E...                                  */

static int aglsetE(pntr)
char *pntr;                      /* Keyword pointer (after the C)             */
{
while(*pntr) { if(*pntr++ == '=') break; }

if((AGL_err!=NULL)&&(AGL_err!=stderr)) 
	fclose(AGL_err);

AGL_err = fopen(pntr,"w");
if(AGL_err==NULL) {
	AGL_err=stderr;
	AGL_status.errstat = ERRFILWNG;
} 
return 0;
}




/*****************************************************************************/
/*                            aglsetF (Internal use)                         */

/* Set status parameters for keywords: F...                                  */

static int aglsetF(pntr)
char *pntr;                    /* Keyword pointer (after the F)             */
{
int font;

font = parsefont(pntr);
if( font<0 ) { 
	font=0; 
	AGL_status.errstat=ILLFONTWNG; 
}

if(AGL_chinf(font)!=NULL) 
	DY(curfont)=font;
else {
	DY(curfont)=0; 
	AGL_status.errstat=ILLFONTWNG; 
}
return 0;
}


/*****************************************************************************/
/*                            aglsetG (Internal use)                         */

/* Set status parameters for keywords: G...                                  */

static int aglsetG()
{
DY(autoasp)=ON;
AGL_sgeom(ON);
AGL_sstat(); 
return 0;
}




/*****************************************************************************/
/*                            aglsetI (Internal use)                         */

/* Set status parameters for keywords: I...                                  */

static int aglsetI(pntr)
char *pntr;                      /* Keyword pointer (after the I)            */
{
char aux[40];

if(AG_SVAL(pntr,10,aux)>0) {
	if(isdigit(*aux)) {
		AGL_status.errstat=ILLSETWNG; 
		return 0;
	} else {
		AGL_status.DefBkgr = parse(pntr,color_tbl,AGLCOLORS);
		AGL_wasinit |= AGL_BKGPRESET;
	}
}

return 0;
}



/*****************************************************************************/
/*                            aglsetL (Internal use)                         */

/* Set status parameters for keywords: L...                                  */

static int aglsetL(pntr)
char *pntr;                      /* Keyword pointer (after the L)            */
{
int aux,intpar;

switch(*pntr++) {
case 'f':				/*  keywd="LF...."           */
	DY(changle)=0.0; 
	break;

case 'i':				/*  keywd="LI...."           */
	pntr++;
	switch(*pntr++) {
	case 'x':		/* keywd="LINX..."           */
		DY(flogx)=FALSE;
		AGL_sgbl();
		break;

	case 'y':		/* keywd="LINY.."            */
		DY(flogy)=FALSE;
		AGL_sgbl();
		break;

	default: 
		AGL_status.errstat=ILLCODWNG; 
		break;
	}
	break;

case 'o':			 	/*  keywd="LO...."          */
	pntr++;
	switch(*pntr++) {
	case 'x':		/* keywd="LOGX..."          */
		DY(flogx)=TRUE;
		AGL_sgbl();
		break;

	case 'y':		/* keywd="LOGY..."          */
		DY(flogy)=TRUE;
		AGL_sgbl();
		break;

	default: 
		AGL_status.errstat=ILLCODWNG; 
		break;
	}
	break;

case 's':				/*  keywd="LS...."          */
	aux=MAXLSTYL;
	AG_IVAL(pntr,(int)1,&intpar);
	if(intpar<0) 
		intpar=0;
	if(intpar>aux) 
		intpar=aux;
	DY(lstyle)=intpar;
	AGL_sstyl(DOTLEN(AGL_status.devid));
	break;

case 'w':				/*  keywd="LW...."          */
	AG_IVAL(pntr,(int)1,&intpar);
	if(intpar<0) 
		intpar=0;
	if(intpar>MAXLWIDTH(AGL_status.devid)) 
		intpar=MAXLWIDTH(AGL_status.devid);
	DY(lwidth)=intpar;
	AGL_swdt(intpar);
	break;

default: 
	AGL_status.errstat=ILLCODWNG; 
	break;
}
return 0;
}


/*****************************************************************************/
/*                            aglsetM (Internal use)                         */

/* Set status parameters for keywords: M...                                  */

static int aglsetM(pntr)
char *pntr;			/* Keyword pointer (after the M)             */
{
char mode[2];

if(*pntr == 's') { 			/* keywd="MS..."                     */
	pntr += 2;
	switch(*pntr++) {
	case 'e':				/* keywd="MSgE.."            */
		AGL_status.sevmsg=2; 
		break;
	case 'n':				/* keywd="MSgN.."            */
		AGL_status.sevmsg=4; 
		break;
	case 's':				/* keywd="MSgS.."            */
		AGL_status.sevmsg=3; 
		break;
	case 'w':				/* keywd="MSgW.."            */
		AGL_status.sevmsg=1; 
		break;
	case 'a':				/* keywd="MSgW.."            */
		AGL_status.sevmsg=(-999); 
		break;
	default: 
		AGL_status.errstat=ILLCODWNG;
		break;
	}
} else {
	if(AGL_status.curvwp == VWPEMPTY)      /* test if viewport active    */
		AGL_status.errstat=NOVWPERR; 
	else {
		switch(*pntr++) {
		case 'f':				/* keywd="MF..."     */
			if(*pntr++ == 'h')
				DY(metamode) = HARDMETAFILE;
			else
				DY(metamode) = SOFTMETAFILE;
			if(DY(metafile)!=NULL)
				DY(filact)=DY(metamode);
			break;
		case 'o':				/* keywd="MO..."     */
			AG_SVAL(pntr,1,mode);		/* get mode          */
			if( *mode == '\0' )
				AGL_status.errstat=SETSYNWNG;
			else
				DY(wrmode) = TOUPPER(*mode);
			setwrmo();
			break;
		default: 
			AGL_status.errstat=ILLCODWNG; 
			break;
		}
	}
}
return 0;
}


/*****************************************************************************/
/*                            aglsetN (Internal use)                         */

/* Set status parameters for keywords: N...                                  */

static int aglsetN(pntr)
char *pntr;			/* Keyword pointer (after the N)             */
{
AGL_status.errstat=AGLNOERR;
switch(*pntr++) {
case 'o':				/* keywd="NOrmal"                    */
	DY(modeflag) = NORMAL;
	AGL_sstat();
	break;

case 'g':				/* keywd="NGeom."                    */
	DY(autoasp)=OFF;
	AGL_sgeom(OFF);
	AGL_sstat();
	break;
default: 
	AGL_status.errstat=ILLCODWNG; 
	break;
}
return 0;
}



/*****************************************************************************/
/*                            aglsetR (Internal use)                         */

/* Set status parameters for keywords: R...                                  */

static int aglsetR(pntr)
char *pntr;                    /* Keyword pointer (after the R)             */
{
AGL_status.errstat=AGLNOERR;

switch(*pntr++) {
case 'a':				/* keywd="RA...."                   */
	DY(angfct)=1.0;
	break;
case 'g':				/* keywd="RG...."                   */
	switch(*pntr++) {

	case 'l':			/* keywd="RGL..."                   */
		DY(changle)=3.1415927;
		break;

	default: 
		AGL_status.errstat=ILLCODWNG; 
		break;
	}
	break;

default: 
	AGL_status.errstat=ILLCODWNG; 
	break;
}
return 0;
} 



/*****************************************************************************/
/*                            aglsetS (Internal use)                         */

/* Set status parameters for keywords: S...                                  */

static int aglsetS(pntr)
char *pntr;			/* Keyword pointer (after the S)             */
{
float dd[2];

if(*pntr == 't') { 				/* keywd="ST...."            */
	pntr += 2;
	switch(*pntr++) {
	case 'e':				/* keywd="STpE.."            */
		AGL_status.sevterm=2; 
		break;
	case 's':				/* keywd="STpS.."            */
		AGL_status.sevterm=3;
		break;
	case 'w':				/* keywd="STpW.."            */
		AGL_status.sevterm=1;
		break;
	case 'n':				/* keywd="STpN.."            */
		AGL_status.sevterm=99;
		break;
	default: 
		AGL_status.errstat=ILLCODWNG; 
		break;
	}
} else {
	if(AGL_status.curvwp == VWPEMPTY)      /* test if viewport active    */
		AGL_status.errstat=NOVWPERR; 
	else {
		switch(*pntr++) {
		case 'c':				/* keywd="SC...."    */
			if(AG_RVAL(pntr,(int)1,dd)==1)
				DY(scale)=dd[0];
			else
				AGL_status.errstat=SETSYNWNG; 
			break;
		case 'p':				/* keywd="SP...."    */
			DY(modeflag) = SPECIAL;
			AGL_sstat();
			break;
		case 'y':				/* keywd="SY...."    */
			switch(*pntr++) {
			case 'd':			/*  keywd="SYD..."   */
				AG_RVAL(pntr,(int)1,dd);
				DY(symbsize)=dd[0];
				break;
			case 's':			/*  keywd="SYS..."   */
				DY(symbsize)=0.5;
				break;
			case 'b':			/*  keywd="SYB..."   */
				DY(symbsize)=1.0;
				break;
			case 'm':			/*  keywd="SYM..."   */
				DY(symbsize)=2.0;
				break;
			case 'l':			/*  keywd="SYL..."   */
				DY(symbsize)=3.0;
				break;
			default: 
				AGL_status.errstat=ILLCODWNG; 
				break;
			}
			if(AGL_status.errstat==AGLNOERR)
				break;
		default: 
			AGL_status.errstat=ILLCODWNG; 
			break;
		}
	}
}
return 0;
}



/*****************************************************************************/
/*                            aglsetT (Internal use)                         */

/* Set status parameters for keywords: T...                                  */

static int aglsetT(pntr)
char *pntr;                      /* Keyword pointer (after the T)            */
{
int intpar;
AGL_status.errstat=AGLNOERR;
switch(*pntr++) {
case 'w':				/* keywd="TW...."                    */
	AG_IVAL(pntr,(int)1,&intpar);
	if(intpar<0) intpar=0;
	DY(textlw)=intpar;
	break;

default: 
	AGL_status.errstat=ILLCODWNG; 
	break;
}
return 0;
}



/*****************************************************************************/
/*                            aglsetU (Internal use)                         */

/* Set status parameters for keywords: U...                                  */

static int aglsetU(pntr)
char *pntr;                      /* Keyword pointer (after the U)            */
{
switch(*pntr++) {
case 's':					/* keywd="US..."             */
	if(DY(window)) {
		DY(modeflag) = USER;
		AGL_sstat();
	}
	break;

case 'p':					/* keywd="UP..."             */
	DY(changle)=4.712389;
	break;

default: 
	AGL_status.errstat=ILLCODWNG; 
	break;
}
return 0;
}





/*****************************************************************************/
/*                            aglills (Internal use)                         */

/* Set AGL error code ILLCODWNG                                              */


static int aglills()
{
AGL_status.errstat=ILLCODWNG; 
return 0;
}





/*****************************************************************************/
/*                           AGL_set  (Internal use)                         */

/* Set status parameters getting input from a keyword                        */


int AGL_set(keywd)
char *keywd;                   /* Status specifier                          */
{
#define LASTSETKEY   21
static int (*kwtable[LASTSETKEY+1])() = {
					aglills,	/*      0            */
					aglills,	/*  a - 1            */
					aglsetB,	/*  b - 2            */
					aglsetC,	/*  c - 3            */
					aglsetD,	/*  d - 4            */
					aglsetE,	/*  e - 5            */
					aglsetF,	/*  f - 6            */
					aglsetG,	/*  g - 7            */
					aglills,	/*  h - 8            */
					aglsetI,	/*  i - 9            */
					aglills,	/*  j - 10           */
					aglills,	/*  k - 11           */
					aglsetL,	/*  l - 12           */
					aglsetM,	/*  m - 13           */
					aglsetN,	/*  n - 14           */
					aglills,	/*  o - 15           */
					aglills,	/*  p - 16           */
					aglills,	/*  q - 17           */
					aglsetR,	/*  r - 18           */
					aglsetS,	/*  s - 19           */
					aglsetT,	/*  t - 20           */
					aglsetU };	/*  u - 21           */
int jump;
AGL_status.errstat=AGLNOERR;

jump= *keywd-'a'+1;

jump= (jump<0)?0:jump;
jump = (jump>LASTSETKEY)?0:jump;

switch(jump) {
case 4:			/* cases where viewport check is done below  */
case 5:
case 9:
case 13:
case 19:
	break;
default:
	if(AGL_status.curvwp == VWPEMPTY) {   /* test if viewport active    */
		AGL_status.errstat=NOVWPERR; 
		return 0;
	}
}
return (*kwtable[jump])(keywd+1);
}
