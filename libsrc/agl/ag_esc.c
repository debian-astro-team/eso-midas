/* @(#)ag_esc.c	19.1 (ES0-DMD) 02/25/03 13:52:57 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_esc.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:57   */
/*
 * HEADER : ag_esc.c       - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                           AG_ESC (C callable)                           */

/* AG_ESC     Device escape                                                  */


/* Escape standard AGL commands by sending a strictly device dependent       */
/* command sequence to the device driver.                                    */

/* Note: this function is provided for very specialized applications only    */
/*       and its use is HIGHLY NOT RECOMMENDED. The content of the string is */
/*       only dependent on the device driver and AGL only provides for pass- */
/*       ing it to the driver. Most driver will not support this feature and */
/*       will return from this service with the UNSFEATINF return code       */


void AG_ESC (cmd,cmdlen)

 char *cmd;                   /* Command string (Max 132 bytes)              */
 int cmdlen;                  /* Command string length (Max 132)             */
                                                                         /*--*/
{
static char *modnam="ESC";
int i,is;
DEFINE_AGLDVCOM;
extern void AG_DMSG();

AG_DMSG(modnam,(char *)0);

#ifndef NO_METAFILE_SUPPORT
if ( DY(filact) != NOMETAFILE ) {	/* store data into metafile     */
	enum METACODE cod = MFESC;
	is = fwrite((void *)&cod,sizeof(int),1,DY(metafile));
	is = fwrite((void *)&cmdlen,sizeof(int),1,DY(metafile));
	is = fwrite((void *)cmd,sizeof(char),cmdlen,DY(metafile));
	if(is != cmdlen) 
		AGL_puterr(MFWRITERR,modnam); 
}
#endif

if(cmdlen>DRVCBUFMAX) { 
	AGL_puterr(BUFOVFERR,modnam); 
	return; 
}

for (i=0; i<cmdlen; i++) 
	CHARBUF[i]=cmd[i];

CHANNEL = DY(curchan);
IBUFFR(0) = cmdlen;
ESCPFUNCT(AGLDVCOM);
if(ERRCODE != AGLNOERR) 
	AGL_puterr(ERRCODE,modnam);
}

