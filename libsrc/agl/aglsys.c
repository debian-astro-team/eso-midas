/* @(#)aglsys.c	19.1 (ES0-DMD) 02/25/03 13:53:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)aglsys.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:05   */
/*
 * HEADER : aglsys.c       - Vers 3.6.001  - May 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 * Includes system dependent code according to the currently defined 
 * system symbol. The symbol itself is defined into the file aglos.h
 * (osdefos.h, for Midas installation)
 */

#ifndef SYSDEPC_DEF
#define SYSDEPC_DEF


#ifdef MIDAS
#include <osdefos.h>
#else
#include <aglos.h>
#endif

#include <aglsys.h>

#if MID_OS_MSDOS
#include "agldos.c"
#endif

#if MID_OS_VMS
#include "aglvms.c"
#endif

/*
 * Now include some common system dependencies
 */

#ifdef UNIX
#include "aglunix.c"
#endif

#if MID_OS_BSD
#include "aglbsd.c"
#endif

#if MID_OS_SYSV
#include "aglsysv.c"
#endif

#if MID_OS_SYSV_V2
#include "aglsysv.c"
#endif

#if MID_OS_ULTRIX
#include "aglsysv.c"
#endif

#if MID_OS_POSIX
#include "aglposix.c"
#endif

#endif
