/* @(#)ag_vn2u.c	19.1 (ES0-DMD) 02/25/03 13:53:02 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_vn2u.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:02   */
/*
 * HEADER : ag_vn2u.c      - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                            AG_VN2U (C callable)                         */

/* AG_VN2U    Normalized to user coord. conversion                           */


/* Convert an (x,y) couple of NORMALized coordinates into the corresponding  */
/* USER coordinates.                                                         */

/* An information status is set if coordinates are outside the current       */
/* clipping area. A warning status is set if coordinates are outside the     */
/* current viewport. An error status is set if no window is currently defi-  */
/* ned.                                                                      */


void AG_VN2U (xpn,ypn,xu,yu)

double  xpn,ypn;             /* Normalized input coordinates                */
float  *xu,*yu;              /* User output coordinates                     */
                                                                         /*--*/
{
int AGL_n2u();
double xx,yy;
static char *modnam = "VN2U";

AGL_status.errstat=AGL_n2u(xpn,ypn,&xx,&yy);

if(AGL_status.errstat != AGLNOERR) 
	AGL_siger(modnam);

*xu = xx;
*yu = yy;
}

