/* @(#)ag_iget.c	19.1 (ES0-DMD) 02/25/03 13:52:59 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_iget.c	19.1  (OAA-ASTRONET) 02/25/03 13:52:59   */
/*
 * HEADER : ag_iget.c      - Vers 3.6.001  - Oct 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                            AG_IGET (C callable)                         */

/* AG_IGET    Get integer information                                        */


/* Get integer pieces of information. Required information is specified      */
/* by a command string.                                                      */


int AG_IGET (item,ival)
                              /* Returns number of values in ival.           */

 char *item;                  /* Information select string (see below).      */
 int  ival[];                 /* Return value(s). The array must be long     */
                              /* enough to contain the required number of    */
                              /* values specified in the list below.         */

/* The select string contains a keyword which specifies the piece of informa-*/
/* tion required. Keywords can be truncated at the minimum number of charac- */
/* ters needed to recognize them (shown uppercase in the list below) but can */
/* be typed both in upper and lower case                                     */

/*        Number of                                                          */
/* Name     items    Description                                             */

/* BACKGround 1      Default background color of current device              */
/* BLACk      1      Color code to get black color                           */
/* BLUE       1      Color code to get blue  color                           */
/* COLOr      1      Current color code                                      */
/* CLR1       1      Main color code                                         */
/* CLR2       1      Secondary color code                                    */
/* CLR3       1      Third choice color code                                 */
/* CYAN       1      Color code to get cyan  color                           */
/* DEVType    1      Device type (0: passive, 1:interactive)                 */
/* ERROr      1      Last error code                                         */
/* FLG00      1      =1 if device is Interactive  (else 0)                   */
/* FLG01      1      =1 if device locator can return pixel value (else 0)    */
/* FLG02      1      =1 if device allows partial erasing (else 0)            */
/* FLG03      1      =1 if alpha plane is separated from graphic planes      */
/* FLG04      1      =1 if device must be explicitly erased at initializ.    */
/* FLG05      1      =1 if command defined in AGLDEVS.DAT will be executed   */
/* FLG06      1      =1 if device accept RGB color definition (else 0)       */
/* FLG07      1      =1 if device can set RGB color background (else 0)      */
/* GREEn      1      Color code to get green color                           */
/* LINX       1      =1 if linear transformation on X axis (else 0)          */
/* LINY       1      =1 if linear transformation on Y axis (else 0)          */
/* LOGX       1      =1 if linear transformation on X axis (else 0)          */
/* LOGY       1      =1 if linear transformation on Y axis (else 0)          */
/* LSTYLe     1      Current line style                                      */
/* LWIDTh     1      Current line width                                      */
/* MAGEnta    1      Color code to get magenta color                         */
/* MFMODE     1      Current metafile mode (0:no metafile; 1: SOFT; 2: HARD) */
/* MODE       1      =0 if current mode is NORMALIZED, =1 if USER, =2 if     */
/*                   current mode is SPECIAL                                 */
/* NCOLors    1      Number of colors (pens, gray tones) allowed by device   */
/* NSYMbols   1      Number of symbols (markers)                             */
/* NSTYLes    1      Number of line styles supported (the default 0 not      */
/*                   included)                                               */
/* NWIDThs    1      Number of line widths supported (the default 0 not      */
/*                   included)                                               */
/* RED        1      Color code to get red color                             */
/* TWIDTh     1      Current text line width                                 */
/* USER       10     Get up to 10 user specified data items. See AG_ISET.    */
/* WHITe      1      Color code to get white color                           */
/* YELLow     1      Color code to get yellow color                          */
/*                                                                           */
                                                                         /*--*/
{
extern char* AG_SCAN();
static char *modnam = "IGET";
char keywd[MAXKEYLNG];
extern void AG_DMSG();
int nvalues=0;

AG_DMSG(modnam,(char *)0);

AG_SCAN(item,';',MAXKEYLNG,keywd);
if(*keywd!='\0')  {
	nvalues=AGL_gint(keywd,ival);
	if ( AGL_status.errstat != AGLNOERR ) 
		AGL_siger(modnam);
}
return nvalues;
}
