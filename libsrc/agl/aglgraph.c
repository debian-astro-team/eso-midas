/* @(#)aglgraph.c	19.1 (ESO-DMD) 02/25/03 13:53:09 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
 * HEADER : aglgraph.c     - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA

 021204		last modif (KB)

*/





/*                       INTERNAL GRAPHIC ROUTINES                           */




#include <aglenvr.h>
#include <aglstat.h>




/*****************************************************************************/
/*                      AGL_gpll (Internal AGL use)                           */

/* This routine draws a generic polyline                                     */

void AGL_gpll (poly)
struct polyline *poly;        /* Polyline to draw                           */

{
struct polyline *work;
struct polyline polyaux;
struct polyline *AGL_padj();
DEFINE_AGLDVCOM;



if ( poly->nvects <= 0 ) 
   { 
   AGL_status.errstat=VECLNGWNG; 
   return; 
   }

if(AGL_status.curvwp == VWPEMPTY) 
   { 
   AGL_status.errstat=NOVWPERR; 
   return; 
   }


polyaux.maxvects=poly->maxvects;
polyaux.nvects=poly->nvects;
polyaux.XV=poly->XV;
polyaux.YV=poly->YV;
polyaux.grmode=poly->grmode;
polyaux.clipmode=poly->clipmode;
polyaux.join=poly->join;


while((polyaux.nvects > 1) && ((work=AGL_padj(&polyaux))!=NULL))
   { 

#ifndef NO_METAFILE_SUPPORT

   if ( DY(filact) == HARDMETAFILE ) 
      {
      enum METACODE cod = MFGPLL;
      (void)fwrite((void *)&cod,sizeof(int),1,DY(metafile));
      AGL_mput(DY(metafile),work,FALSE);
      if(AGL_status.errstat >= ERRTRSH) 
      return;
      }

#endif

   NPOINTS = work->nvects;
   VECTX = work->XV;
   VECTY = work->YV;
   CHANNEL = DY(curchan);
   POLYFUNCT(AGLDVCOM);
   AGL_status.errstat=ERRCODE;
   }
}

