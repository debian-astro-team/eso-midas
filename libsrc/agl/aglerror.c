/* @(#)aglerror.c	19.1 (ES0-DMD) 02/25/03 13:53:04 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)aglerror.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:04   */
/*
 * HEADER : aglerror.c     - Vers 3.6.001  - Sep 1993 -  L. Fini, OAA
 *                         - Vers 3.6.000  - Nov 1991 -  L. Fini, OAA
 */





/*                       ERROR MANAGEMENT ROUTINES                           */




#include <aglenvr.h>
#include <aglstat.h>




/*****************************************************************************/
/*                        aglmsg (Local use)                                 */

/* Error message displaying                                                  */

static void aglmsg (code)
int code;
{
char *rtname;
extern char *AGL_scan();

fprintf ( AGL_err, "AGL Status code %4d in:", code );
while ( (rtname=AGL_scan()) != NULL ) {
	fprintf ( AGL_err, " AG_%s\n                   from:", rtname);
}
fprintf ( AGL_err, " User prog.\n\n");
}




/*****************************************************************************/
/*                           trap (Internal AGL use)                         */

/* Standard error management routine                                         */

static void trap()
{
extern char *AGL_top();
int sever;

AGL_status.lasterr = AGL_status.errstat;	/* Store last error code     */

AGL_status.lastname=AGL_top();			/* Store last routine name   */

sever = AGL_status.errstat/100;			/* Test error severity       */

if ( sever >= AGL_status.sevmsg ) 
	aglmsg(AGL_status.errstat);
if ( sever >= AGL_status.sevterm ) 
	AGL_stop();
}



/*****************************************************************************/
/*                        AGL_siger  (Internal AGL use)                      */

/* Error signal utility (error code is into AGL_status.errstat)              */

/* This module is to be called to signal an error generated elsewhere        */

void AGL_siger(modnam)
char *modnam;                 /* Module name to push into stack              */
{
AGL_push(modnam);
trap();
AGL_pop();
}



/*****************************************************************************/
/*                      AGL_puterr  (Internal AGL use)                       */

/* Error signal utility (also put error code into AGL_status.errstat)        */

/* This module is to be called to generate an error with given code          */

void AGL_puterr(errcode,modnam)
int errcode;			/* Error code                                */
char *modnam;			/* Module name to push into stack            */
{
AGL_status.errstat=errcode;
AGL_siger(modnam);
}
