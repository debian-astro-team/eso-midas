/* @(#)ag_tget.c	19.1 (ES0-DMD) 02/25/03 13:53:01 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)ag_tget.c	19.1  (OAA-ASTRONET) 02/25/03 13:53:01   */
/*
 * HEADER : ag_tget.c      - Vers 3.6.000  - Oct 1991 -  L. Fini, OAA
 *
 *
 *                           C INTERFACE MODULE
 */


#include <aglenvr.h>
#include <aglstat.h>


/*****************************************************************************/
/*++                             AG_TGET (C callable)                        */

/* AG_TGET    Get text dimensions                                            */


/* Returns the normalized displacements of a box enclosing the given string, */
/* as shown in the following figure:                                         */
/* The displacements are computed with respect to the box origin shown as    */
/* point (0,0) in the figure.                                                */

/*                   |                     |                                 */
/*  (xd[2],yd[2]) --- ***  ****        **** --- (xd[1],yd[1])                */
/*                   *   * *   *       *   *                                 */
/*                   *   * *   *       *   *                                 */
/*                   ***** ****  ****  *   *                                 */
/*                   *   * *   * *   * *   *                                 */
/*                   *   * *   * *   * *   *                                 */
/*     (0,0) ------- *   * ****  ****  **** --- (xd[0],yd[0])                */
/*                   |           *         |                                 */
/*                   |           *         |                                 */
/*                   |                     |                                 */


/* N.b.: in the example there are couples of coordinates which are equal     */
/*       (xd[0]=xd[1], etc.) but this is in general not true for different   */
/*       orientations of the string                                          */

/* Return values are always referred to the NORMALIZED mode coordinates      */
/* irrespective of the current graphic mode.                                 */


void AG_TGET (text,xd,yd)

 char *text;                  /* Input string                                */
 float  xd[3];                /* String x displacements (Normalized)         */
 float  yd[3];                /* String y displacements (Normalized)         */
                                                                         /*--*/
{
static char *modnam = "TGET";
char *chstrg=text;
double hdim, vdim;
double rescalx,rescaly;
int font=DY(curfont);

AG_DMSG(modnam,(char *)0);

AGL_strd(chstrg,font,&hdim,&vdim);      /* Compute string dimensions     */

rescalx = rescaly = DY(scale) * CHARMULT(AGL_status.devid);

rescalx *= DY(chrhsize) * RSLX(AGL_status.devid);
rescaly *= DY(chrvsize) * RSLY(AGL_status.devid);

hdim *= rescalx;                         /* Scale and normalize        */
vdim *= rescaly;

AG_TSET(0.0,0.0,DY(changle),3);

xd[0] = hdim;
yd[0] = 0.0;
xd[1] = hdim;
yd[1] = vdim;
xd[2] = 0.0;
yd[2] = vdim;

AG_TROT(xd,yd,3);
}

