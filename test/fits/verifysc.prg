! @(#)verifysc.prg	19.1 (ESO-IPG) 02/25/03 14:32:43
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT  (c) 1993  European Southern Observatory
!.TYPE       Procedure
!.IDENT      verifysc.prg
!.AUTHOR     P.Grosbol,   ESO/IPG
!.PURPOSE    Simple test on strings
!.USAGE      @@ fcomp  str1  str2  label
!.COMMENTS   If a difference is found the global keyword FTEST is set to 1
!.VERSION    1.0  1993-Sep-17 : Creation,  PJG
! ------------------------------------------------------------------
CROSSREF  STR1  STR2  LABEL
DEFINE/PAR  P1  ? ? ?
DEFINE/PAR  P2  ? ? ?
DEFINE/PAR  P3  ? ? ?
!
!  Compare as strings
!
IF  P1 .ne. P2  THEN
 WRITE/OUT  Error: {P3}  {p1} <-> {p2}
 FTEST(1) = 1
ENDIF
