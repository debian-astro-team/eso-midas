! @(#)glueFITS.prg	19.1 (ESO-DMD) 02/25/03 14:32:38 
! ++++++++++++++++++++++++++++++++++++
! 
! procedure glueFITS.prg creates a FITS file with many extensions 
! from the usual FITS test files we already have
! 
! K. Banse	ESO - DMD	020627
! 
! ++++++++++++++++++++++++++++++++++++
! 
define/param p1 0 n "Enter Alpha_flag:"
! 
indisk/mfits tst0001.mt a
indisk/mfits tst0002.mt b
indisk/mfits tst0003.mt c
indisk/mfits tst0004.mt d
indisk/mfits tst0005.mt e
indisk/mfits tst0006.mt f
if {p1} .eq. 0 then
   indisk/mfits tst0007.mt g
   indisk/mfits tst0008.mt h
else
   indisk/mfits tst0006.mt g		!tst0007/8.mt give trouble on Alpha
   indisk/mfits tst0006.mt h
endif
indisk/mfits tst0011.mt tt
! 
outdisk/sfits tt0000.bdf,tt0001.bdf,tt0002.tbl,a0001.bdf,b0001.bdf,c0001.bdf,d0001.bdf,tt0000.bdf,e0001.bdf,f0001.bdf,g0001.bdf,h0001.bdf tst99.mt
! 
if aux_mode(1) .eq. 1 then		!VMS
   $copy tst0011.mt tst0020.mt
   $append tst99.mt tst0020.mt
else
   $cat tst0011.mt tst99.mt > tst0020.mt
endif
! 
write/out 
write/out > info/frame tst0020.mt ext
info/frame tst0020.mt ext

