! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT  (c) 1997-2009  European Southern Observatory
!.TYPE       Procedure
!.IDENT      verifytape.prg
!.AUTHOR     P.Grosbol,   ESO/IPG
!.PURPOSE    Test and verification of FITS tape I/O commands
!.ALGORITHM  Create a set of image and tables, write/read them to
!            tape, check some data values in them, re-do it after a
!            INTAPE/OUTTAPE cycle.
!.USAGE      @@ verifytape tape-device
!.VERSION    1.0  1997-Mar-06 : Creation,  PJG
! 091029	last modif
! ------------------------------------------------------------------
CROSSREF     TAPE
DEFINE/PARM  P1  ? ? ?
!
! Define local keywords for tests
!
DEFINE/LOCAL  REF/R/1/13  0.0 all
DEFINE/LOCAL  VAL/R/1/13  0.0 all
WRITE/KEYW    FTEST/I/1/1 0
SET/FORMAT    ,E15.5			! set format for double data
!
! Reference values
!
ref(1)  = 1.273543E+2
val(1)  = 7.394488E+1
ref(2)  = 3.600000E+1
val(2)  = 2.107224E+1
ref(3)  = 1.856400E+4
val(3)  = 1.071779E+4
ref(4)  = 3.532000E+1
val(4)  = 1.129088E-1
ref(5)  = 0.000000
val(5)  = 9.560513E+1
ref(6)  = -3.560797E-8
val(6)  = 2.262827E+1
ref(7)  = 4.8446182E+02
val(7)  = -2.4352182E-02
ref(8)  = 2.10188154006588375E+19
val(8)  = -6.52064009369660060E-16
ref(9)  = -12300.1204232321033
val(9)  = 234567.89
ref(10) = 6.52064009369660060E-16
val(10) = -2.4352182E-02
ref(11) = 0.0
val(11) = 0.0
ref(12) = 0.0
val(12) = 0.0
ref(13) = 7.50000E+0
val(13) = 4.60991E+0
!
! Create a few images and tables
!
create/image  zzi0001 2,512,512
create/table  zzt0001 10 1234 NULL
!
! Create catalogues and write them to tape
!
create/icat  zzimg  zzi*.bdf
create/tcat  zztbl  zzt*.tbl
outtape/fits zzimg  {p1}  RNN
outtape/fits zztbl  {p1}  NNN
!
! Read them back and test they are the same
!
intape/fits *  zzz  {p1}  N
!
! Check frame  1:
!
stat/ima  zzz0001 ? ? ? SN
@@ verifysc  {outputr(3)}  {ref(1)}  "Frame 1 mean"
@@ verifysc  {outputr(4)}  {val(1)}  "Frame 1 rms."
!
! Check frame  2:
!
stat/ima  zzz0002 ? ? ? SN
@@ verifysc  {outputr(3)}  {ref(2)}  "Frame 2 mean"
@@ verifysc  {outputr(4)}  {val(2)}  "Frame 2 rms."
!
! Write frames out again
!
create/icat zzzi zzz*.bdf
create/tcat zzzt zzz*.tbl
!
! Write frames out with different options
!
!  Catalog option
!
outtape  zzzi  zzi NNN
outtape  zzzt  zzt NNN
!
! Report status
!
IF FTEST(1) .eq. 0 THEN
 WRITE/OUT FITS tape I/O test completed and o.k.
 IF AUX_MODE(1) .EQ. 1 THEN		!VMS
   $ DELETE/NOCONF zz*.*.*
 ELSE					!UNIX
   $ rm -f zz*
 ENDIF
ELSE
 WRITE/OUT FITS tape I/O test completed with ERROR - check LIST!!!
 return 1
ENDIF
return 0
