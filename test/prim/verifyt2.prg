!+++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure verifyt2.prg to verify MIDAS table commands
!
! use as @@ verifyt2
!
! 110906        last modif
!
!++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local seconds/i/1/2 0,0 ? +lower
define/local rcorr/r/1/10 0. all +lower
define/local errsum/i/1/1 0 ? +lower
!
write/out +------------------------------------------+
write/out Start of procedure verifyt2.prg
write/out +------------------------------------------+
!
seconds(1) = m$secs()
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
!
define/local loop/i/1/1 0
define/local diff/d/1/1 0
define/local val/r/1/1 0
define/local res1/d/1/6 -
44817.7898855113,-
-26.77814753558,-
0.00746710798533
write/key res1/d/4/3 -
-1.0405397609741e-06,-
7.1308821103016e-11,-
-1.912565194384e-15
define/local res2/r/1/2 1.0,0.0
create/table veritb 2 38 veritb
sort/table veritb #2
regression/polynomial veritb #1 #2 5
do loop = 1 6
    diff = outputd('loop')-res1('loop')
    if m$abs(diff) .gt. 1.e-7 goto regre_error
enddo
save/regression veritb  regre                                        
create/column veritb :FIT 
compute/regression veritb :FIT = REGRE                               
project/table veritb veritc #2
create/column veritc :SPLINE E15.6 
interpolate/tt veritc :LAB002,:SPLINE veritb :LAB002,:LAB001 0.001
create/ima ref 1,190 2410,50 nodata
interpolate/ti veritb veritb #2,#1 ref 0.001
convert/tab conv = veritb #2 #1 ref SPLINE 0.001,3
compute/ima convint = conv/veritb
stat/ima convint
do loop = 1 2
   diff = outputr('loop'+2)-res2('loop')
   if m$abs(diff) .gt. 1.e-7 goto inter_error
enddo
sort/table veritb #1
val = 'veritb,#1,@1'
if val .ne. 4710.0 goto sort_error
sort/tab veritb #1(-)
val = 'veritb,#1,@1'
if val .ne. 11570.0 goto sort_error
project/tab  veritb veritd #3 
copy/tt veritb.{tbltype} #2 veritd.{tbltype} :lab002
copy/tab veritd.{tbltype} verite.{tbltype}
join/tab veritd.{tbltype} #1,#2 verite.{tbltype} #1,#2 veritout.{tbltype} 0.,0.
show/tab veritout
if outputi(2) .ne. 38 goto join_error 
compute/table veritout :diff = #3-#1
stat/table veritout :diff
do loop = 1 2
   diff = outputr('loop'+2)
   if m$abs(diff) .gt. 1.e-7 goto join_error
enddo
! 
! test 32bit unsigned integer data (data from the NOT)
write/out > indisk/fits NOT.fits not_telescope
indisk/fits NOT.fits not_telescope
info/frame not_telescope >Null
if mid$info(1) .ne. 18 goto bit32_error
write/out > statistics/image not_telescope
statistics/image not_telescope
write/keyw rcorr  9899,124172,101197,21225.4,-3.9457,16.935
@@ kcompare rcorr outputr 1,3 0.25 4,4 0.05 5,5 0.00005
@@ kcompare rcorr outputr 6,6 0.0001
!
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verifyt2.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
!
return 0
! 
regre_error:
write/out ############Problems with REGRESSION/POLY#############
return 1
inter_error:
write/out ############Problems with one of the INTERPOLATE commands ###
return 1
sort_error:
write/out ############Problems with the SORT command ###
return 1
join_error:
write/out ############Problems with the JOIN command ###
return 1
bit32_error:
write/out ###########Problems reading 32bit unsigned int FITS file #########
return 1
 
