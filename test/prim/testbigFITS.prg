! @(#)testbigFITS.prg	19.1 (ESO-DMD) 02/25/03 14:33:44
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure testbigFITS.prg  to measure the effects of descr.
!  allocation (control) options set via the SET/MIDAS command
!  for very large FITS headers, VIMOS FITS files are good examples...
! 
!  K. Banse     000710
!
!  use as @@ testbigFITS FITSfile_name
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 vimos.fits ima "Enter FITS file:"
! 
write/out this procedure measures the performance of INTAPE/FITS 
write/out using the commands: SET/MIDAS dscalloc=... and  SET/MIDAS dschelp=...
write/out for the file {p1} (in FITS format) 
write/out
! 
set/format i1
delete/temp
define/local sc/i/1/5 0 all +low
create/image &a 2,100,100
!
write/out use default descr. allocations and save the FITS keyword comments
write/out set/midas dscalloc=0,0
write/out this forces the predefined values, which are currently: 100,6000
write/out set/midas dschelp=yes
write/out the FITS keyword comments are written as descriptor help text-
          to the file
write/out
! 
set/midas dscalloc=0,0
set/midas dschelp=yes
@@ testbigFITS,doit {p1}
sc(5) = sc(3)		!save 1. time
! 
write/out
write/out same test but we now ignore the descr help text
write/out set/midas dschelp=no
write/out
set/midas dschelp=no
@@ testbigFITS,doit {p1}
! 
write/out
write/out reserve right away space for a large no. of descriptors (3300)
write/out and descr-data (30000 bytes), also use descr help text again
write/out set/midas dscalloc=3300,30000
write/out set/midas dschelp=yes
write/out
set/midas dscalloc=3300,30000
set/midas dschelp=yes
@@ testbigFITS,doit {p1}
! 
write/out
write/out same test but we now ignore the descr help text
write/out set/midas dschelp=no
write/out
set/midas dschelp=no
@@ testbigFITS,doit {p1}
! 
sc(4) = sc(5) - sc(3)
sc(1) = sc(4)*100/sc(5)
write/out 
write/out so, we have an improvement in processing speed of {sc(1)}%
write/out 
!
entry doit
set/format i1
find/min &a >Null			!to use a different executable
! 
sc(1) = m$secs()
write/out ... INTAPE/FITS 1 lola {p1} ...
intape/fits 1 lola {p1}
sc(2) = m$secs()
sc(3) = sc(2)-sc(1)
write/out ... executed in {sc(3)} secs ...  
! 
