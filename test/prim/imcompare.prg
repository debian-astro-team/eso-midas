! @(#)imcompare.prg	19.1 (ES0-DMD) 02/25/03 14:33:40
! @(#)imcompare.prg	4.2 (ESO-IPG) 3/23/93 16:09:46
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure imcompare.prg
!  K. Banse     920910
!
!  use as @@ imcompare imref imnew eps
!  with   imref = name of reference image
!         imnew = name of new image
!	  eps = epsilon for minimum of difference
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ? ? "Enter refima: "
define/par p2 ? ? "Enter newima: "
define/par p3 0. n "Enter epsilon for difference: "
! 
define/local sum/d/1/1 0 ? +lower
define/local ddif/d/1/1 0. ? +lower
! 
compute/pixel &diff = {p1}-{p2}
find/min &diff >Null
outputr(1) = m$abs(outputr(1))
if outputr(1) .gt. {p3} goto bad
! 
outputr(2) = m$abs(outputr(2))
if outputr(2) .le. {p3} return
! 
bad:
write/out ref-image ({p1}) - new-image ({p2}):
find/min &diff 
errsum = errsum+1
