#!/bin/bash
#
# shell script testsuper.sh to test Midas
#
# variables MIDASHOME and MIDVERS must be set already!
#
# execute as: $ $MIDASHOME/$MIDVERS/test/prim/testsuper.sh 
# 
# if it's the very first time you execute Midas (e.g. after an installation)
# execute as: $ $MIDASHOME/$MIDVERS/test/prim/testsuper.sh 1
# 
# K. Banse      041129
#
#.VERSION
# 041223        last modif
#


if [ -d $MIDASHOME/$MIDVERS ] ; then
   echo "MIDASHOME/MIDVERS = $MIDASHOME/$MIDVERS"
else
   echo env. var. MIDASHOME and/or MIDVERS not set... we abort
   exit 1
fi


cd ~/tmp
echo "we work in `pwd`"
echo "first we clean this dir. completely...!"
echo "you have 2 seconds to abort this...!"
sleep 2

rm -rf *
ls 

echo we execute Midas in job-mode:
inmidas -j "@ vericopy; @ superverify; bye"

$MIDASHOME/$MIDVERS/system/unix/inmidas -j "@ vericopy; @ superverify; bye"

echo "now we check the Midas logfile for errors in any tutorial"
grep \(ERR\) ~/midwork/F*00.LOG

if [ $? != 0 ] ; then
   echo "-------------------------------------------------------------"
   echo procedure superverify terminated without errors
   echo "-------------------------------------------------------------"
else
   echo "-------------------------------------------------------------"
   echo procedure superverify had problems - check the Midas logfile!
   echo "-------------------------------------------------------------"
   exit 1
fi


cd $MIDASHOME

echo 
echo "as the final step, we also install the calibration data base"
echo "if we have a calib tarfile (compressed or uncompressed) in $MIDASHOME"
echo 
ls
sleep 2

if [ ! -f calib.tar.Z ] ; then
   if [ ! -f calib.tar ] ; then
      echo "neither file calib.tar.Z nor calib.tar found in $MIDASHOME"
      echo "skip calibration data base creation..."
      exit 
   fi
fi

$MIDASHOME/$MIDVERS/monit/calib_build

 
