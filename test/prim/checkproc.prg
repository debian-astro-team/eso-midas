! @(#)checkproc.prg	19.1 (ES0-DMD) 02/25/03 14:33:38
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure checkproc.prg  to check all MIDAS procedures in a
!				  given directory
!  K. Banse     960715
!
!  use as @@ checkproc direc
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 + ? "Enter directory name:"
define/param p2 + ? "Enter file specifiers:"
! 
-delete middumm.dat
-delete middumm.datt
! 
if p2(1:1) .eq. "+" then
   define/local filespec/c/1/10 *.prg
else
   define/local filespec/c/1/40 "{p2}"
endif
! 
if p1(1:1) .eq. "+" then
   -dir {filespec} >middumm.dat
   define/local direc/c/1/80 "current directory"
else
   -dir {p1}/{filespec} >middumm.dat
   define/local direc/c/1/80  "directory {p1}"
endif
! 
define/local ifct/i/1/2 0,0
define/local ofct/i/1/2 -1,-1
define/local count/i/1/2 0,0
define/local record/c/1/200 " " all
set/format i1
! 
open/file middumm.dat read ifct
if ifct(1) .lt. 0 then
   write/out "No procedures found in {direc}"
   return/exit
endif
!
read_loop:                                      !read until EOF
write/keyw record/c/1/200 " " all
read/file {ifct(1)} record
!
if ifct(2) .ne. -1 then
   count(2) = count(2)+1
   translate/show {record} x,silent
   if mid$info .gt. 0 then
      count = count+1
      if ofct(1) .eq. -1 then
         open/file middumm.datt write ofct  
         if ofct(1) .lt. 0 then
            write/out Could not open file: middumm.datt
            return/exit
         endif
         write/file {ofct(1)} "Directory: {direc}"
         close/file {ofct(1)}
      endif
      open/file middumm.datt append ofct  
      write/file {ofct(1)} " "
      write/file {ofct(1)} "translate/show {record} x"
      close/file {ofct(1)}
      translate/show {record} x >>middumm.datt
   endif
   goto read_loop
endif
!
close/file {ifct(1)}
! 
if count(1) .gt. 0 then
   write/out {count} (out of {count(2)}) procedures have -
             "problems + need be corrected"
   write/out see the file `middumm.datt' "for detailed listing"
else
   write/out all {count(2)} procedures are o.k.
endif

