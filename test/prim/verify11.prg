! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify11.prg  to verify MIDAS commands
!  K. Banse     991013, 011211, 040826, 091029, 111130
!
!  use as @@ verify11 ffffffffffff             with f = 1 or 0 (on/off)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 111111111111 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/50 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/12 "0" all
define/local scale/i/1/1 1 ? +lower
define/local seconds/i/1/2 0,0? +lower
!
delete/temp				!get rid of old temporary files
seconds(1) = m$secs()
write/key sizez/i/1/5 600,600,0,0,0
write/key dispyes/i/1/2 0,0
! 
write/out +------------------------------------------+
write/out Start of procedure verify11.prg
write/out +------------------------------------------+
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
!
write/key ccc {p1}
set/format i1
do loop = 1 9
   if ccc({loop}:{loop}) .eq. "1" @@ verify11,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify11.prg!"
      return 1
   endif
enddo
do loop = 10 10
   if ccc({loop}:{loop}) .eq. "1" @@ verify11,00{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 00{loop} in verify11.prg!"
      stop = 1
      return
   endif
enddo
! 
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verify11.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
!
!  here the different sub-procedures
!
entry 0001
!
write/out test of CREATE/DISPLAY 
write/out "----------------------"
!
reset/display  >Null
create/display 7 512,512,616,300
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
load/lut rainbow
!
entry 0002
!
write/out test of LOAD/TABLE
write/out "------------------"
!
write/out > build artificial table
! 
create/table middumma
create/colu middumma :x "Frame Pixels" i4 I*4
create/colu middumma :y "Frame Pixels" i4 I*4
create/colu middumma :id "None" a4 C*4
middumma,:x,@1 = 333
middumma,:y,@1 = 456
middumma,:id,@1 = "M"
middumma,:id,@1 = "M"
middumma,:x,@2 = 333
middumma,:y,@2 = 565
middumma,:id,@2 = "M"
middumma,:x,@3 = 368
middumma,:y,@3 = 511
middumma,:id,@3 = "M"
middumma,:x,@4 = 399
middumma,:y,@4 = 565
middumma,:id,@4 = "M"
middumma,:x,@5 = 399
middumma,:y,@5 = 456
middumma,:id,@5 = "M"
! 
middumma,:x,@6 = 459
middumma,:y,@6 = 456
middumma,:id,@6 = "I"
middumma,:x,@7 = 459
middumma,:y,@7 = 565
middumma,:id,@7 = "I"
! 
middumma,:x,@8 = 523
middumma,:y,@8 = 456
middumma,:id,@8 = "D"
middumma,:x,@9 = 523
middumma,:y,@9 = 565
middumma,:id,@9 = "D"
middumma,:x,@10 = 560
middumma,:y,@10 = 549
middumma,:id,@10 = "D"
middumma,:x,@11 = 580
middumma,:y,@11 = 524
middumma,:id,@11 = "D"
middumma,:x,@12 = 580
middumma,:y,@12 = 494
middumma,:id,@12 = "D"
middumma,:x,@13 = 560
middumma,:y,@13 = 470
middumma,:id,@13 = "D"
middumma,:x,@14 = 523
middumma,:y,@14 = 456
middumma,:id,@14 = "D"

middumma,:x,@15 = 620
middumma,:y,@15 = 456
middumma,:id,@15 = "A"
middumma,:x,@16 = 655
middumma,:y,@16 = 565
middumma,:id,@16 = "A"
middumma,:x,@17 = 690
middumma,:y,@17 = 456
middumma,:id,@17 = "A"
 
middumma,:x,@18 = 635
middumma,:y,@18 = 505
middumma,:id,@18 = "AX"
middumma,:x,@19 = 674
middumma,:y,@19 = 505
middumma,:id,@19 = "AX"

middumma,:x,@20 = 734
middumma,:y,@20 = 456
middumma,:id,@20 = "S"
middumma,:x,@21 = 767
middumma,:y,@21 = 456
middumma,:id,@21 = "S"
middumma,:x,@22 = 780
middumma,:y,@22 = 484
middumma,:id,@22 = "S"
middumma,:x,@23 = 767
middumma,:y,@23 = 511
middumma,:id,@23 = "S"
middumma,:x,@24 = 743
middumma,:y,@24 = 511
middumma,:id,@24 = "S"
middumma,:x,@25 = 731
middumma,:y,@25 = 537
middumma,:id,@25 = "S"
middumma,:x,@26 = 743
middumma,:y,@26 = 565
middumma,:id,@26 = "S"
middumma,:x,@27 = 777
middumma,:y,@27 = 565
middumma,:id,@27 = "S"

if workenv(1:1) .eq. "F" then
   -copy nttexample.mt wuff.fits
else
   indisk/fits nttexample.mt wuff.bdf
endif

if dispyes(1) .eq. 1 then
   inputi(20) = 0			!no debugging display
   load/image wuff cuts=470,540
   write/out > load/tab middumma :x :y :id,no 1 3 white -1
   load/tab middumma :x :y :id,no 1 3 white -1
   wait/secs 1
   clear/chan ov
   load/tab middumma :x :y :id,no 3 5 red -1
   wait/secs 1
   clear/chan ov
   load/tab middumma :x :y ? ? 5 cyan 
   wait/secs 1
   clear/chan ov
   load/tab middumma :x :y :id,no 101 6 yellow 0
   load/tab middumma :x :y ? 1 7 blue 0			!add blue rings around
   clear/chan ov
   load/image wuff cuts=470,540 scale=2
   load/tab middumma :x :y :id,no 101 6 yellow 0	!some rows have to be skipped
   if outputi(11) .ne. 13 then
      write/out "[outputi(11)} rows skipped - should be 13"
      errsum = errsum+1
      return
   endif
else
   write/out No display! You won't see much...
endif
! 
delete/table middumma no
!
entry 0003
!
write/out test of CONVERT/COORDS
write/out "----------------------"
!
if workenv(1:1) .eq. "F" then
   -copy wcstest.mt wcs.fits
else
   in_a = "wcstest.mt"
   indisk/fits {in_a} wcs.bdf
endif
read/descr wcs
write/keyw outputd 0.0 all
write/out > convert/coords wcs @216,@189
convert/coords wcs @216,@189
do inputi = 1 12
   outputr({inputi}) = outputd({inputi})
enddo
write/keyw rcorr 40.7306,18.356,0,2,42,55.34,18,21,21.73,216,189,1
@@ kcompare rcorr outputr 1,2 0.0001
@@ kcompare rcorr outputr 4,11 0.0001
write/keyw outputd 0.0 all
write/out > convert/coords wcs 40.730574,18.356036
convert/coords wcs 40.730574,18.356036
do inputi = 1 12
   outputr({inputi}) = outputd({inputi})
enddo
@@ kcompare rcorr outputr 1,2 0.0001
@@ kcompare rcorr outputr 4,11 0.0001
write/keyw outputd 0.0 all
write/out > convert/coords wcs 2:42:55.34,18:21:21.73
convert/coords wcs 2:42:55.34,18:21:21.73
do inputi = 1 12
   outputr({inputi}) = outputd({inputi})
enddo
@@ kcompare rcorr outputr 1,2 0.0001
@@ kcompare rcorr outputr 4,11 0.0001
!
entry 0004
!
write/out test of 3d tables
write/out "-----------------"
!
if workenv(1:1) .eq. "F" then
   -copy in3d.mt in3d.tfits
else
   write/out > indisk/fits in3d.mt in3d.tbl
   indisk/fits in3d.mt in3d.tbl
endif
! 
write/out > show/table in3d
show/table in3d
! 
write/out > copy/t3tab in3d :lambda @1 out2d :lambda_1
copy/t3tab in3d :lambda @1 out2d :lambda_1
! 
write/out > show/table out2d
show/table out2d
! 
statistics/table out2d :lambda_1
rcorr(1) = outputr(1)
rcorr(2) = outputr(2)
rcorr(3) = outputr(3)
rcorr(4) = outputr(4)
!
copy/t3ima  in3d :lambda 1,1 ? ? outima
statistics/image outima
@@ kcompare rcorr outputr 1,4 0.05
!
! compare 6th element of result column and result image
outputd = m$value(outima[@6])
outputd(2) = m$value(out2d,#1,@6)
outputd(3) = m$abs(outputd(1) - outputd(2))
if outputd(3) .gt. 0.05 then
   write/out problems with COPY/T3IMA or COPY/T3TAB ...
   errsum = errsum+1
   return
endif
! 
if workenv(1:1) .eq. "F" return         !omit in FITS working environment
! 
! another 3dim table
show/table 3Dtable.tfits
write/out > extract :FLUX vector in row nr. 2 via
write/out > copy/teima 3Dtable.tfits :flux 2 p6=flux	
indisk/fits 3Dtable.tfits ana.tbl
copy/teima ana :flux 2 p6=flux		!pull out 3rd dim array
statistics/image flux0002
write/key rcorr -
-18.15,58817.9,21215.9,19087.6,0.639888,2.02827,4.27712e+07,15191.8,97.2148
@@ kcompare rcorr outputr 1,1 0.001
@@ kcompare rcorr outputr 2,4 0.5
@@ kcompare rcorr outputr 5,6 0.00001
@@ kcompare rcorr outputr 8,9 0.1
!
if dispyes(2) .eq. 1 then
   create/graph 1 800,440,0,400
   write/out > plot the table vector
   plot/table ana @2#11[1..2016] @2#12[1..2016]
   write/out > and plot the extracted spectrum
   plot flux0002
endif
! 
entry 0005
!
write/out test of catalog creation from FITS files
write/out "----------------------------------------"
!
inputi(19) = 0
if aux_mode(1) .eq. 1 then		!for VMS
   $ ASSIGN VER11.DAT SYS$OUTPUT
   $ DIR/TOTAL tst*.mt
   $ DEASSIGN SYS$OUTPUT
   define/local vmsfc/i/1/2 0,0
   open/file VER11.DAT read vmsfc
   if vmsfc(1) .lt. 1 then
      write/out "Could not open file VER11.DAT with dir contents..."
      errsum = errsum+1
      return
   endif
  read_loop:
   read/file {vmsfc(1)} inputc
   if vmsfc(2) .gt. -1 then
      if inputc(1:8) .eq. "Total of" then
         inputi(19) = {inputc(9:)}
         write/out total count: {inputi(19)} tst*.mt files
         close/file {vmsfc(1)}
      else
         goto read_loop
      endif
   else
      close/file {vmsfc(1)}
      write/out "Could not find info in file VER11.DAT with dir contents..."
      errsum = errsum+1
      return
   endif
else					!for Unix
   $ls tst*.mt >middumm.dat
   $wc <middumm.dat >indisk.dat	!get the no. of Preben's tst*.mt files
   write/keyw inputi/i/19/1 <indisk.dat
endif
! 
if inputi(19) .gt. 0 then	!we only do it, if verifydio was executed
   create/icat fitsimas tst*.mt
   show/icat fitsimas 
   if outputi .ne. inputi(19) then
      write/out "problem with creating a catalog from FITS files..."
      errsum = errsum+1
      return
   endif
   read/icat fitsimas
endif
! 
entry 0006
!
write/out test of char. keyword arrays
write/out "----------------------------"
! 
define/local charbuf/c*40/1/3 " " all
write/keyw charbuf/c*40/1/1 "This is the first line of keyword: charbuf "
write/keyw charbuf/c*40/2/1 "Here we find the second record... "
write/keyw charbuf/c*40/3/1 "Finally, the last line of `charbuf' "
read/keyw charbuf
write/out ...{charbuf(2)}
inputc = "aaaaaaaaaaaaaa"
inputc = "{charbuf(1)(13:17)}"
if inputc(1:5) .ne. "first" then
   write/out "problem with character keyword arrays..."
   errsum = errsum+1
   return
endif
! 
inputc = "You" // charbuf(2)(9:13) //"the" // charbuf(3)(14:22)
if inputc(1:19) .ne. "Youfindthelast line" then
   write/out "problem with character keyword arrays..."
   errsum = errsum+1
   return
endif
! 
entry 0007
! 
entry 0008
!
write/out  test of MODIFY/DISP parent
write/out "---------------------------"
!
if dispyes(1) .ne. 1 return
! 
reset/display
create/image &u 2,256,256 ? gauss 128,128,128,128
! 
set/graph default
create/disp 0 800,400,0,20 ? no
modify/disp parent
crea/disp 1 400,400,0,0 p4=yes,2                !white background
crea/gra 4 400,400,401,0
set/graph bcol=5
load/lut heat ? d
load/image &u
statist/ima &u
plot/histo &u
!
extract/ima &r = &u [<,@123:>,@123]
find/minmax &r
compute/ima &rr = {outputr(2)}-&r
!
modify/disp parent=root
!
create/gra 0 600,400,401,400
set/graph bcol=0
modify/graph parent
create/gra 1 600,200,0,0
set/gra colo=2
plot/row &r
create/gra 2 600,200,0,201
set/gra colo=4
plot/row &rr
!
modify/disp parent=root
!
create/random &x = &r Uni,f
compute/ima &rr = (&r*500000.) + &x
!
create/disp 9 600,400,0,500 ? no
modify/disp parent
create/gra 3 600,400,0,0
set/graph colo=1
plot/row &rr
modify/gra parent
create/disp 5 210,180,180,40 ? no
modify/disp parent
load/lut rainbow3
load/image &u
create/gra 5 190,80,5,5
set/graph colo=4
plot/row &r
display/lut off
!
delete/graph 0
set/graph bcol=5
plot/row &r
delete/graph 5
!
label/disp "End Subwindow demo" 100,8 O blue 1
show/display all
set/graph
!
entry 0009
!
write/out  test of heavy memory usage...
write/out "------------------------------"
!
write/out > rebin/linear wuff waff 0.25,0.25
rebin/linear wuff waff 0.25,0.25
write/out > compute/image &a = waff*(1.0-sin(waff))
compute/image &a = waff*(1.0-sin(waff))
write/out > replace/image waff &b <,800=middumma.{imatype}
replace/image waff &b <,800=middumma.{imatype}
write/out > write/descr wuff npix 1024,1024
write/descr wuff npix 1024,1024
write/out > fft/frequ wuff ? &a &b
fft/frequ wuff ? &a &b
!
entry 0010
!
write/out  chck of BSCALE, BZERO in FITS files
write/out "-----------------------------------"
!
write/key rcorr -
   8528.801,8528.801,8528.801,8528.801,8528.801,8528.801,8528.801,8528.801,8528.801,8528.801
if workenv(1:1) .eq. "F" then
   write/out > read/image f43test @2,@2,20
   read/image f43test @2,@2,20
else
   write/out > indisk/fits f43test.fits middumma
   indisk/fits f43test.fits middumma
   write/out > read/image &a @2,@2,20
   read/image &a @2,@2,20
endif
@@ kcompare rcorr outputr 1,10 0.005
!
