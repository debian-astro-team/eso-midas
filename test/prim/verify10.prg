! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify10.prg  to verify MIDAS commands
!  K. Banse	ESO - Garching	000329
!
!  use as @@ verify10 ffffffffff             with f = 1 or 0 (on/off)
!
! 090129	last modif
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 111111111111 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/50 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/12 "0" all
define/local scale/i/1/1 1 ? +lower
define/local seconds/i/1/2 0,0? +lower
!
delete/temp				!get rid of old temporary files
seconds(1) = m$secs()
write/key sizez/i/1/5 600,600,0,0,0
write/key dispyes/i/1/2 0,0
! 
write/out +------------------------------------------+
write/out Start of procedure verify10.prg
write/out +------------------------------------------+
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
!
write/key ccc {p1}
set/format i1
do loop = 1 10
   if ccc({loop}:{loop}) .eq. "1" @@ verify10,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify10.prg!"
      return 1
   endif
enddo
! 
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verify10.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
!
!  here the different sub-procedures
!
entry 0001
!
write/out test of CREATE/DISPLAY 
write/out "----------------------"
!
reset/display  >Null
create/display 7 512,512,616,300
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
load/lut rainbow
!
entry 0002
!
write/out "-------------------------------"
write/out test of some more key functions
write/out "-------------------------------"
!
in_a = "was ist hier los?"
inputc = m$repla(in_a,"i","xyz")
read/key in_a,inputc
out_a = "ESO " // m$repla(in_a,"i","xyz") // "mucki"
read/key out_a
out_b = "ESO "//m$repla(in_a,"i","xyz") //"mucki"
if out_a .ne. out_b then
   write/out "we have a problem (1) with m$repla..."
   errsum = errsum+1
   return
endif
if m$repla(in_a,"i","xyz") .ne. "was xyzst hxyzer los?" then
   write/out "we have a problem (2) with m$repla..."
   errsum = errsum+1
   return
endif
outputc = "as"
in_a = "was ist hier las?"
inputc = m$repla(in_a,outputc,"7")
read/key inputc
in_b = "     " //in_a
inputc = m$trim(in_b)
read/keyw in_b,inputc
if inputc .ne. in_a then   
   write/out "we have a problem (1) with m$trim..."
   errsum = errsum+1
   return
endif
! 
entry 0003
!
write/out "------------------------"
write/out test of subframe options
write/out "------------------------"
!
if workenv(1:1) .eq. "F" then
   -copy nttexample.mt middummfitsa.{imatype}
else
   indisk/fits nttexample.mt &fitsa
endif
statist/image &fitsa [@100,@100:@200,@200]
copy/kk outputr rcorr/r/1/12
statist/image &fitsa[@100,@100:@200,@200]
@@ kcompare rcorr outputr 1,12 0.01
-copy middummfitsa.{imatype} middummb.{imatype}
compute/image &b[@100,@100:@200,@200] = 333.
extract/image &c = &b [@100,@100:@200,@200]
write/keyw rcorr 333.0 all
find/minmax &c
@@ kcompare rcorr outputr 1,2 0.001
! 
entry 0004
!
write/out "------------------------------------------"
write/out test of copying of descr to primary header
write/out "------------------------------------------"
!
if workenv(1:1) .eq. "F" return
! 
indisk/fits PWDL_990510A_G300V_S25_1x1s.mt -
            PWDL_990510A_G300V_S25_1x1s.{tbltype}
read/descr PWDL_990510A_G300V_S25_1x1s.{tbltype} *
icorr(1) = outputi(1)
! 
define/local inpa/c/1/100 "PWDL_990510A_G300V_S25_1x1s.{tbltype} "
define/local fitsname/c/1/100 "PWDL_990510A_G300V_S25_1x1s.tfits "
! 
write/out > outdisk/sfits PWDL_990510A_G300V_S25_1x1s.{tbltype} ? ? copy
outdisk/sfits PWDL_990510A_G300V_S25_1x1s.{tbltype} ? ? copy
! 
read/descr {fitsname} *
if outputi(1) .ne. icorr(1) then
   write/out "we have a problem with FITS tables (orig.no. {icorr(1)}) ..."
   errsum = errsum+1
   return
endif
! 
rename/table {inpa} middumma.{tbltype}
outdisk/sfits middumma.{tbltype} ? ? copy
read/descr middumma.tfits * >Null
if outputi(1) .ne. icorr(1) then
   write/out "we have a problem with outdisk/sfits (a) (orig.no. {icorr(1)})"
   errsum = errsum+1
   return
endif
outdisk/sfits middumma.{tbltype} middummx.mt ? copy
read/descr middummx.mt * >Null
if outputi(1) .ne. icorr(1) then
   write/out "we have a problem with outdisk/sfits (b) (orig.no. {icorr(1)})"
   errsum = errsum+1
   return
endif
! 
write/out > outdisk/sfits middumma.{tbltype},middummb,middummc middummx.fits
outdisk/sfits middumma.{tbltype},middummb,middummc middummx.fits
! 
write/out > indisk/mfits {fitsname}[0] empty.bdf
indisk/mfits {fitsname}[0] empty.bdf
! 
-delete PWDL_990510A_G300V_S25_1x1s.{tbltype}
-delete PWDL_990510A_G300V_S25_1x1s.tfits
! 
entry 0005
!
write/out "---------------------------------"
write/out test of access to FITS extensions
write/out "---------------------------------"
!
if workenv(1:1) .eq. "F" return
! 
write/out > list all extensions of FITS file `tst0012.mt'
write/out > info/image tst0012.mt ext
info/image tst0012.mt ext
if outputi(19) .ne. 5 then			!we should have 4 extensions
   write/out "we have a problem with info/image... "-
             "no. of extensions = {outputi(19)}"
   errsum = errsum+1
   return
endif
! 
intape/fits 1 klaus tst0012.mt
write/out > "pull out all valid extensions of FITS file" `tst0012.mt'
write/out > intape/fits 1 klaus tst0012.mt
intape/fits 1 klaus tst0012.mt
write/out > statist/image klaus0001a
statist/image klaus0001a
write/keyw rcorr  0.0,72.0,36.0,21.0722,0.0,1.79923,407340.0,36.0,0.141176 
write/keyw icorr 11315,1,1,1,73,1,1 
@@ kcompare rcorr outputr 1,6 0.001
@@ kcompare rcorr outputr 7,7 0.1
@@ kcompare rcorr outputr 8,9 0.001
@@ kcompare icorr outputi 1,7
write/out > statist/image tst0012.mt[3]
statist/image tst0012.mt[3]
@@ kcompare rcorr outputr 1,6 0.001
@@ kcompare rcorr outputr 7,7 0.1
@@ kcompare rcorr outputr 8,9 0.001
@@ kcompare icorr outputi 1,7
write/out > get/fextens tst0012.mt quality 
get/fextens tst0012.mt quality 
if outputi .ne. 3 then
   write/out "we have a problem with: "get/fextens newbie.fits quality"
   errsum = errsum+1
   return
endif
write/out > statist/image tst0012.mt[{outputi}]
statist/image tst0012.mt[{outputi}]
@@ kcompare rcorr outputr 1,6 0.001
@@ kcompare rcorr outputr 7,7 0.1
@@ kcompare rcorr outputr 8,9 0.001
@@ kcompare icorr outputi 1,7
! 
write/out > "rebuild FITS file without unkown extension"
write/out > outdisk/sfits -
klaus0001.bdf,klaus0001.tbl,klaus0001a.bdf,klaus0001a.tbl newbie.fits
outdisk/sfits klaus0001.bdf,klaus0001.tbl,klaus0001a.bdf,klaus0001a.tbl -
              newbie.fits
write/out > info/image newbie.fits ext
info/image newbie.fits ext
! 
write/out > get/fextens tst0012.mt Asciitable 
get/fextens tst0012.mt Asciitable 
write/out > read/desc tst0012.mt[{outputi}] *
read/desc tst0012.mt[{outputi}] *
icorr(1) = 19
@@ kcompare icorr outputi 1,1
write/out > read/desc klaus0001a.tbl *
read/desc klaus0001a.tbl *
@@ kcompare icorr outputi 1,1
icorr(1) = icorr(1) + 1					! descr FILENAME added 
write/out > get/fextens newbie.fits Asciitable 
get/fextens newbie.fits Asciitable 
write/out > read/desc newbie.fits[{outputi}] *
read/desc newbie.fits[{outputi}] *
@@ kcompare icorr outputi 1,1
write/out > compute/image &a = newbie.fits[2] + 22.2 
compute/image &a = newbie.fits[2] + 22.2 
write/out > compute/image &b = klaus0001a + 22.2 
compute/image &b = klaus0001a + 22.2 
write/out > compute/image &d = &a - &b
compute/image &d = &a - &b
write/out > find/minmax &d
find/minmax &d
if m$abs(outputr(1)) .gt. 0.0001 .or. m$abs(outputr(2)) .gt. 0.0001 then 
   write/out "we have a problem with: "compute/image &a = newbie.fits[1] + 22.2"
   errsum = errsum+1
   return
endif
! 
write/keyw rcorr -934.322,234568,11308.7,47742.8 
write/out > get/fextens newbie.fits Asciitable 
get/fextens newbie.fits Asciitable 
if outputi .ne. 3 then
   write/out "we have a problem with: "get/fextens newbie.fits Asciitable"
   errsum = errsum+1
   return
endif
write/out > statist/tab newbie.fits[{outputi}] :dist
statist/tab newbie.fits[{outputi}] :dist
@@ kcompare rcorr outputr 1,1 0.01
@@ kcompare rcorr outputr 2,2 0.5
@@ kcompare rcorr outputr 3,4 0.1
! 
if dispyes(1) .eq. 1 then
   write/out > load/image klaus0001a 0 scale=3,1 p7=36,15,256,356
   load/image klaus0001a 0 scale=3,1 p7=36,15,256,356
   wait/secs 1
   write/out > get/fextens newbie.fits quality 
   get/fextens newbie.fits quality 
   if outputi .ne. 2 then
      write/out "we have a problem with: "get/fextens newbie.fits quality"
      errsum = errsum+1
      return
   endif
   write/out > load/image "newbie.fits[{outputi}] 0 scale=3,1 p6=Up,Over" -
               p7=36,15,256,156
   load/image newbie.fits[{outputi}] 0 scale=3,1 p6=Up,Over p7=36,15,256,156
endif
! 
if aux_mode(1) .lt. 2 then
   -delete  klaus*.*.*
else
   -delete  klaus*.*
endif
! 
entry 0006
!
write/out "-----------------------------------------"
write/out test of access to several FITS extensions
write/out "-----------------------------------------"
!
if workenv(1:1) .eq. "F" return
! 
indisk/fits order.tfits midd1.tbl >Null
write/descr midd1.tbl extname/c/1/16 order-table
indisk/fits xcentera.tfits midd2.tbl >Null
write/descr midd2.tbl extname/c/1/16 xcenter-table
indisk/fits wcstest.mt galax.bdf 
write/descr galax extname/c/1/16 galax-image
write/out > compute/image galax2 = galax + galax
compute/image galax2 = galax + galax
! 
outdisk/sfits midd1.tbl,galax.bdf,midd2.tbl,galax.bdf goodie.fits >Null
write/out > info/frame goodie.fits ext
info/frame goodie.fits ext
write/out > compute/image &a = goodie.fits[2] + goodie.fits[4]
compute/image &a = goodie.fits[2] + goodie.fits[4]
write/out > compute/imag &d = &a - galax2
compute/imag &d = &a - galax2
find/min &d >Null
if m$abs(outputr(1)) .gt. 10.e-20 .or. m$abs(outputr(2)) .gt. 10.e-20 then
   write/out -
   "we have a problem with: compute/image &a = goodie.fits[2] + goodie.fits[4]"
   errsum = errsum+1
   return
   write/out error!
endif
! 
outdisk/sfits galax.bdf,midd2.tbl,galax.bdf baddie.fits >Null
write/out > info/frame baddie.fits ext
info/frame baddie.fits ext
write/out > compute/image &a = baddie.fits[0] + baddie.fits[2]
compute/image &a = baddie.fits[0] + baddie.fits[2]
write/out > compute/imag &d = &a - galax2
compute/imag &d = &a - galax2
find/min &d >Null
if m$abs(outputr(1)) .gt. 10.e-20 .or. m$abs(outputr(2)) .gt. 10.e-20 then
   write/out -
   "we have a problem with: compute/image &a = goodie.fits[2] + goodie.fits[4]"
   errsum = errsum+1
   return
endif
! 
write/out > compute/image &a = goodie.fits[2] + baddie.fits[2]
compute/image &a = goodie.fits[2] + baddie.fits[2]
write/out > compute/imag &d = &a - galax2
compute/imag &d = &a - galax2
find/min &d >Null
if m$abs(outputr(1)) .gt. 10.e-20 .or. m$abs(outputr(2)) .gt. 10.e-20 then
   write/out -
   "we have a problem with: compute/image &a = goodie.fits[2] + goodie.fits[4]"
   errsum = errsum+1
   return
endif
!
statistic/tabl midd1 :yfit
rcorr(1) = outputr(1)
rcorr(2) = outputr(2)
rcorr(3) = outputr(3)
rcorr(4) = outputr(4)
icorr(1) = outputi(1)
icorr(2) = outputi(2)
!
statistic/tabl goodie.fits[1] :yfit
@@ kcompare rcorr outputr 1,4 0.01
@@ kcompare icorr outputi 1,2
!
entry 0007
!
write/out "---------------------------------"
write/out more tests of descriptor handling
write/out "---------------------------------"
!
if workenv(1:1) .eq. "F" return
! 
indisk/fits nttexample.mt &fitsa
define/local nodsc/i/1/1 80
! 
outputi = -99
show/descr &fitsa * full >Null
if outputi .ne. nodsc then
   write/out nodsc = {nodsc}, no. of descriptors = {outputi}
   if outputi .lt. nodsc then
      errsum = errsum+1
      return
   endif
endif
! 
-copy middummfitsa.{imatype} middummx.{imatype}
delete/descr &x *
do ival = 1 2
   copy/dd &fitsa * &x
   outputi = -99
   show/descr &x * full >Null
   !
   if outputi .lt. nodsc then
      errsum = errsum+1
      return
   endif
enddo
! 
! compare the FITS headers of &fitsa and &x (after their conversion to FITS)
if aux_mode(1) .ne. 1 then		!not implemented for VMS yet
   @a compFITS image &fitsa &x
   write/out 
   if q1 .ne. "0" then
      errsum = errsum+1
      return
   endif
endif
! 
delete/image &x no
compute/image &x = 4.0 + &fitsa
outputi = -99
show/descr &x * full >Null
if outputi .lt. nodsc then
   errsum = errsum+1
   return
endif
! 
read/descr &fitsa i* full
delete/descr &x instrume
read/descr &x i* full
copy/dd &fitsa *,5 &x
outputi = -99
read/descr &x i* full
if outputi .ne. 2 then
   errsum = errsum+1
   return
endif
! 
!					test copying of help text
delete/descr &x ESO.DET.OUT1.CF
copy/dd &fitsa ESO.DET.OUT1.CF &x
outputi(5) = -99
read/descr &x ESO.DET.OUT1.CF full
if outputi(5) .ne. 33 then		!that's the length of the help text
   errsum = errsum+1
   return
endif
! 
write/descr &x zell/c/1/20 "am Harmersbach "
read/descr &x zell
outputi = -99
copy/dd &x zell &x biberach
show/descr &x biberach
if outputi .ne. 1 then
   errsum = errsum+1
   return
endif
! 
show/descr &x * full >Null
nodsc = outputi			!update no. of descrs.
wr/desc &x datamin/r/1/1  479.0000
wr/desc &x datamax/r/1/1  1271.000
wr/desc &x eso.test.descr/i/1/1 9999
outdisk/fits &x middummz.fits
indisk/fits middummz.fits middumma.bdf
inputi = -999
inputi = m$existd("middumma","eso.test.descr")
if inputi .ne. 1 then
   errsum = errsum+1
   return
endif
nodsc = nodsc + 2			!also orig FILENAME is saved
show/descr &a * full >Null
if outputi .lt. nodsc then
   errsum = errsum+1
   return
endif
! 
if m$exist("wcs.bdf") .ne. 1 then
   indisk/fits wcstest.mt wcs
endif
write/descr wcs xrota/r/1/2 0.0,0.0
-copy wcs.bdf middummw.bdf
write/descr middummw xrota/r/1/5 1.,2.,3.,4.,5.
write/out > copy/dd wcs *,3 middummw ? 2	
copy/dd wcs *,3 middummw ? 2		!copy, but no overwrite
read/descr middummw xrota
if m$value("middummw.bdf",xrota(2)) .ne. 2.0 then
   errsum = errsum+1
   return
endif
copy/dd wcs *,1 middummw 		!just copy
if m$value("middummw.bdf",xrota(2)) .ne. 0.0 .or. -
   m$value("middummw.bdf",xrota(3)) .ne. 3.0 then
   errsum = errsum+1
   return
endif
middummw,xrota(2) = 99.9
copy/dd wcs *,3 middummw ? 1		!copy and clean
copy/dk middummw xrota outputi
if outputi(2) .ne. 0.0 .or. outputi(5) .ne. 0.0 then
   errsum = errsum+1
   return
endif
! 
entry 0008
!
write/out "----------------------------------------------------"
write/out test of STATISTICS/IMAGE with different buffer sizes
write/out "----------------------------------------------------"
! 
extract/image &a = nttexample.mt [<,<:>,@512]
extract/image &b = nttexample.mt [<,@513:>,>]
create/image &x 2,1130,1600 ? poly 0.0
insert/image &a &x <,@10
insert/image &b &x <,@701
!
if dispyes(1) .eq. 1 then
   load &x scale=-3 cuts=-300,700
endif
define/local savmp/i/1/2 {monitpar(20)},0
monitpar(20) = 1600
statist/image &x
!
do inputi = 1 14
   rcorr({inputi}) = outputr({inputi})
enddo
!
monitpar(20) = 100
statist/image &x
!
@@ kcompare rcorr outputr 1,12 0.01
! 
monitpar(20) = savmp(1)
statist/image &x
!
@@ kcompare rcorr outputr 1,12 0.01
! 
entry 0009
!
write/out "--------------------------------------------"
write/out test the ALIGN/IMAGE, AVERAGE/IMAGE commands
write/out "--------------------------------------------"
!
if dispyes(1) .eq. 0 then
   write/out for this test we need a display...
   return
endif
! 
reset/display
create/display
load/lut rainbow ? d
! 
define/local l/i/1/5 0,0,0,0,0
define/local filea/c/1/60 " " ALL
define/local fileb/c/1/60 " " ALL
define/local filec/c/1/60 " " ALL
define/local comtab/c/1/60 " " ALL
!
if workenv(1:1) .eq. "F" then
   -copy dss_test1.fits align_in.fits
   -copy dss_test2.fits align_ref.fits
else
   indisk/fits dss_test1.fits align_in.bdf >Null
   indisk/fits dss_test2.fits align_ref.bdf >Null
   indisk/fits xcentera.tfits xcentera.tbl >Null
   indisk/fits xcenterb.tfits xcenterb.tbl >Null
endif
cuts align_ref align_in				!use same cuts
write/keyw filea align_in
write/keyw fileb align_ref
write/keyw filec align_res
write/keyw comtab xcentera
! 
write/out load/image {filea}
load/image {filea} scale=2
draw/rect xcentera f
write/out center/gauss {filea},{comtab} centera
center/gauss {filea},{comtab} centera
wait/secs 0.5
clear/chan ov
! 
write/keyw comtab xcenterb
write/out load/image {fileb}
load/image {fileb} scale=2
draw/rect xcentera f
write/out center/gauss {fileb},{comtab} centerb
center/gauss {fileb},{comtab} centerb
wait/secs 0.5
clear/chan ov
!
write/out "Now we can align the two images via:"
write/out align/image centera centerb free
write/out 
align/image centera centerb free
!
write/out
write/out "Finally we rotate the first image `{filea}` according to"
write/out  the results of the ALIGN command:
write/out rebin/rotate {filea} {filec} keyword {fileb}
rebin/rotate {filea} {filec} keyword {fileb}
! 
write/out "and load the output image `{filec}` in order to see the result:"
write/out load/image {filec}
load/image {filec} scale=2
! 
write/out "Now we average the two images `align_ref' and" `align_res' with 
write/out and without Merge option
write/out average/image &av =  align_ref,align_res Merge
average/image &av =  align_ref,align_res Merge
cuts &av align_res
write/out load/image &av
load/image &av scale=2
wait/secs 0.5
! 
write/out average/image &av =  align_ref,align_res 
average/image &av =  align_ref,align_res 
write/out load/image &av
load/image &av scale=3
! 
entry 00010
!
write/out "--------------------"
write/out test of DELETE/DESCR
write/out "--------------------"
!
if workenv(1:1) .eq. "F" then
   -copy nttexample.mt wuff.fits
else
   indisk/fits nttexample.mt wuff.bdf
endif
set/format i1
!
read/descr wuff eso.* >Null
icorr = outputi
write/out > "delete/descr wuff eso.*"
outputi = -99
delete/descr wuff eso.*
if outputi .ne. icorr then
   write/out we have a problem!
   write/out before: {icorr} - after: {outputi}
   errsum = errsum+1
   return
endif
!
if workenv(1:1) .eq. "F" then
   -copy nttexample.mt wuff.fits
else
   indisk/fits nttexample.mt wuff.bdf
endif
read/descr wuff *gen* >Null
icorr = outputi
write/out > "delete/descr wuff *gen*"
outputi = -99
delete/descr wuff *gen*
if outputi .ne. icorr then
   write/out we have a problem!
   write/out before: {icorr} - after: {outputi}
   errsum = errsum+1
   return
endif
!
if workenv(1:1) .eq. "F" then
   -copy nttexample.mt wuff.fits
else
   indisk/fits nttexample.mt wuff.bdf
endif
read/descr wuff ESO.*t >Null
icorr = outputi
write/out > "delete/descr wuff ESO.*t"
outputi = -99
delete/descr wuff ESO.*t
if outputi .ne. icorr then
   write/out we have a problem!
   write/out before: {icorr} - after: {outputi}
   errsum = errsum+1
   return
endif
!
if workenv(1:1) .eq. "F" then
   -copy nttexample.mt wuff.fits
else
   indisk/fits nttexample.mt wuff.bdf
endif
read/descr wuff *.det.* >Null
icorr = outputi+2
write/out > "delete/descr wuff cunit,*.det.*,o_pos"
outputi = -99
delete/descr wuff cunit,*.det.*,o_pos
if outputi .ne. icorr then
   write/out we have a problem!
   write/out before: {icorr} - after: {outputi}
   errsum = errsum+1
   return
endif
!
if workenv(1:1) .eq. "F" then
   -copy nttexample.mt wuff.fits
else
   indisk/fits nttexample.mt wuff.bdf
endif
write/out > "delete/descr wuff LST,utc,instrume,o_POS"
outputi = -99
delete/descr wuff LST,utc,instrume,o_POS
if outputi .ne. 4 then
   write/out we have a problem!
   write/out OUTPUTI(1): {outputi} (should be 4!)
   errsum = errsum+1
   return
endif
!
write/descr wuff longy/i/1/6000 123 all
write/descr wuff longy/i/5999/2 -3,-33
compute/image &a = wuff+1.0
inputi = m$value(middumma.{imatype},longy(6000))
if inputi .ne. -33 then
   write/out we have a problem!
   write/out descr. longy(6000): {inputi}
   errsum = errsum+1
   return
endif
!


