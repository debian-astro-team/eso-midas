! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify8.prg  to verify MIDAS commands
!  K. Banse     950906	creation
!
!  use as @@ verify8 ffffffff             with f = 1 or 0 (on/off)
!
! 130503		last modif
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 111111111 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/9 000000000
define/local scale/i/1/1 1 ? +lower
define/local seconds/i/1/2 0,0? +lower
!
delete/temp				!get rid of old temporary files
seconds(1) = m$secs()
write/key sizez/i/1/5 600,600,0,0,0
write/key dispyes/i/1/2 0,0
! 
write/out +------------------------------------------+
write/out Start of procedure verify8.prg
write/out +------------------------------------------+
!
set/midas f_update=??
define/local save_out/c/1/20 {outputc}
set/midas f_update=yes
! 
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
! 
write/key ccc {p1}
set/format i1
do loop = 1 9
   if ccc({loop}:{loop}) .eq. "1" @@ verify8,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify8.prg!"
      return 1
   endif
enddo
! 
set/midas f_update={save_out}
! 
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verify8.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
!
!  here the different sub-procedures
!
entry 0001
!
write/out test of CREATE/DISPLAY 
write/out "----------------------"
!
reset/display  >Null
create/display 7 700,512,400,300  >Null
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
display/lut off
load/lut rainbow3
!
entry 0002
!
write/out test of constructing an image
write/out "----------------------------"
!
@@ creamask			!create image arti.bdf
! 
rebin/linear arti &z .1,.25
filter/smooth &z &b 2,2,0. >Null
!
if dispyes(2) .eq. 1 then
   load/ima &b 
endif
!
create/ima &a 2,{&b,npix(1)},{&b,npix(2)} ? poly 0.,0.7
replace/ima &b veria 0.09,>=&a,0. >Null
! 
find/min veria >Null
find/pixel veria 0.0,0.1 out >Null
rval = outputr(1)-0.2
!
if dispyes(2) .eq. 1 then
   load/ima veria cuts={rval},{veria,lhcuts(4)}
endif
!
apply/edge veria &e 0.1 >Null
! 
if dispyes(2) .eq. 1 then
   load/ima &e cuts=0.,1.
endif
!
inputr = 0.2+{veria,lhcuts(4)}
replace/ima veria &b &e/0.09,>={inputr} >Null
! 
find/min &b >Null
if dispyes(2) .eq. 1 then
   load/ima &b cuts={rval},{&b,lhcuts(4)}
endif
!
crea/ima &z 2,2047,172 
!
write/descr &b step 1.,1.
inputi(1) = {&z,npix(1)}/2-{&b,npix(1)}/2
insert/ima &b &z @{inputi}
! 
write/image &z <,<,2047 {&b,lhcuts(4)} all
write/image &z <,>,2047 {&b,lhcuts(4)} all
create/image &c 2,1,172 ? ? {&b,lhcuts(4)}
insert/image &c &z <,<
insert/image &c &z >,<
!
if dispyes(1) .eq. 1 then
   load/ima &z cuts={rval},{&b,lhcuts(4)}
endif
! 
read/descr &z *
!
entry 0003
write/out test of LOAD/IMAGE with scaling
write/out "-------------------------------"
!
if dispyes(1) .ne. 1 return
! 
define/local loop/i/1/2 0,0
set/format i1
! 
do loop = 1 4
   write/out > load/image &z.{imatype} scale=-{loop},1 center=c
   load/image &z.{imatype} scale=-{loop},1 center=c
enddo
do loop = 2 5
   write/out > load/image &z.{imatype} scale=-{loop},{loop} center=c
   load/image &z.{imatype} scale=-{loop},{loop} center=c
enddo
do loop = 4 7
   write/out > load/image &z.{imatype} scale={loop},-{loop} center=c
   load/image &z.{imatype} scale={loop},-{loop} center=c
enddo
do loop = 3 6
   write/out > load/image &z.{imatype} scale=-{loop},-{loop} center=c
   load/image &z.{imatype} scale=-{loop},-{loop} center=c
enddo
!
write/out > load/image &z.{imatype} scale=-4,1 center=c
load/image &z.{imatype} scale=-4,1 center=c
wait/sec 1
write/out > load/image &z.{imatype} scale=-4,1,a center=c
load/image &z.{imatype} scale=-4,1,a center=c
wait/sec 1
write/out > load/image &z.{imatype} scale=-4,1,min center=c
load/image &z.{imatype} scale=-4,1,min center=c
wait/sec 1
write/out > load/image &z.{imatype} scale=-4,1,max center=c
load/image &z.{imatype} scale=-4,1,max center=c
! 
write/image &z <,<,2047 0 all
write/image &z <,>,2047 0 all
! 
write/out > load/image &z.{imatype} scale=-2,2 center=c
load/image &z.{imatype} scale=-2,2 center=c
write/out > load/image &z.{imatype} scale=-6,-2 dirs=up,ov center=c
load/image &z.{imatype}  scale=-6,-2 dirs=up,ov center=c
! 
write/out finally let's display a vertical LUT with labels
write/out @a vertlut
@a vertlut
! 
rename/image &z veria.bdf
!
entry 0004
write/out test of LOAD/IMAGE with different planes (video)
write/out "------------------------------------------------"
!
if dispyes(1) .ne. 1 .or. workenv(1:1) .eq. "F" return
!
write/out > build a cube with 4 planes from a Rosat image
write/out > create/image &cub 3,256,256,4
create/image &cub 3,256,256,4
! 
compute/image &p = rosat.mt
write/descr &p step 1.,1.
write/descr &p start 100.,100.
compute/image &r = &p * 1.5
compute/image &s = &p * 2.
insert/image &p &cub <,<,@1
insert/image &p &cub <,<,@2
insert/image &r &cub <,<,@3
insert/image &s &cub <,<,@4
clear/chan overlay
write/out > load/image &cub,2..4,1 scale=2 cuts=3.,8.
load/image &cub,2..4,1 scale=2 cuts=3.,8.
! 
if applic(1:1) .ne. "t" then		!test, if load/image command went o.k.
   errsum = errsum+1
   return
endif 
! 
entry 0005
write/out some more table tests
write/out "---------------------"
! 
define/local mm/i/1/1 0
! 
create/table test 15  1000
write/descr  test.{tbltype}  PIXEL/R/1/1 1.
create/column test :X F10.2  R*4
create/column test :Y F10.2  R*4
create/column test :PEAK E12.3  R*4
create/column test :IDENT F15.7  R*8
DELETE/DESCR test.{tbltype} PIXEL
create/column test :R1 F10.6 R*8
create/column test :R2 F10.6 R*8
create/column test :R3 F10.6 R*8
create/column test :R4 F10.6 R*8
create/column test :R5 F10.6 R*8
mm = m$existd("test.{tbltype}","tlabl009")
if mm .ne. 1 then
   errsum = errsum+1
   return
endif 
create/column test :R6 F10.6 R*8
create/column test :R7 F10.6 R*8
create/column test :R8 F10.6 R*8
create/column test :R9 F10.6 R*8
create/column test :R10 F10.6 R*8
create/column test :R11 F10.6 R*8
mm = m$existd("test.{tbltype}","tlabl006")
if mm .ne. 1 then
   errsum = errsum+1
   return
endif 
! 
test.{tbltype},x,@1 = 1.234
test.{tbltype},y,@1 = 2.345
read/table test
! 
if workenv(1:1) .eq. "F" then
   -copy test.{tbltype} testtab.mt
else
   outdisk/fits test.{tbltype} testtab.mt
endif
! 
entry 0006
!
if aux_mode(1) .eq. 1 return                    !not implemented yet for VMS
! 
write/out test of handling FITS images
write/out "----------------------------"
!
set/midas newfile=midas			!ensure Midas format for new files
$rm -f flatf.fits
$cp nttexample.mt flatf.fits		!get the data file
$ chmod +w flatf.fits
if workenv(1:1) .eq. "F" then
  set/midas newfile=fits
  write/descr testtab.mt ident/c/1/72 "this is the identification... "
  return		!we already work with FITS files...
endif
! 
indisk/fits flatf.fits middummz.bdf	!the Midas reference frame
! 
read/descr flatf.fits
write/out > write/descr flatf.fits start/d/2/1 1.12345
write/descr flatf.fits start/d/2/1 1.12345
write/out > and now start(2) of flatf.fits:
write/out {flatf.fits,start(2)}
outputr(1) = 1.12345
rcorr(1) = m$value(flatf.fits,start(2))
@@ kcompare rcorr outputr 1,1 0.00005
! 
write/image flatf.fits @100,@200,2 11.,12.
write/out > write/image flatf.fits @100,@200,2 11.,12.
write/image &z @100,@200,2 11.,12.
@a diffima flatf.fits &z 0.0001
if outputi(10) .ne. 0 then
   errsum = errsum+1
   write/out ######## problem (a) with entry 0006
   return
endif
write/out > compute/image &r = flatf.fits+21.22
compute/image &r = flatf.fits+21.22
compute/image &b = &z+21.22
@a diffima &r &b 0.0001
if outputi(10) .ne. 0 then
   errsum = errsum+1
   write/out ######## problem (b) with entry 0006
   return
endif
!
if dispyes(2) .eq. 1 then
   clear/chan ov
   load/ima flatf.fits cuts=f,3sigma
endif
!
-copy flatf.fits a_VLT_instrument_like_very_long_name_ident1.fits
-copy flatf.fits a_VLT_instrument_like_very_long_name_ident2.fits
-copy flatf.fits a_VLT_instrument_like_very_long_name_ident3.fits
write/out > copy FITS file flatf.fits to the files:
write/out "     " -
 a_VLT_instrument_like_very_long_name_ident1.fits  and
write/out "     " -
 a_VLT_instrument_like_very_long_name_ident2.fits  and
write/out "     " -
 a_VLT_instrument_like_very_long_name_ident3.fits 
write/out > -
compute/image &m = (a_VLT_instrument_like_very_long_name_ident1.fits + 
write/out "     " -
       (a_VLT_instrument_like_very_long_name_ident2.fits*1.03)) + 
write/out "     " -
       (a_VLT_instrument_like_very_long_name_ident3.fits/4.4)
compute/image &m = (a_VLT_instrument_like_very_long_name_ident1.fits + -
      (a_VLT_instrument_like_very_long_name_ident2.fits*1.03)) + -
      (a_VLT_instrument_like_very_long_name_ident3.fits/4.4)
compute/image &n = &z + (&z*1.03) + (&z/4.4)
@a diffima &m &n 0.0001
if outputi(10) .ne. 0 then
   errsum = errsum+1
   write/out ######## problem (c) with entry 0006
   return
endif
compute/image &m = (a_VLT_instrument_like_very_long_name_ident1.fits + -
      (a_VLT_instrument_like_very_long_name_ident2.fits*1.03)) + -
      (a_VLT_instrument_like_very_long_name_ident3.fits/4.4)
! 
entry 0007
!
if aux_mode(1) .eq. 1 return                    !not implemented yet for VMS
! 
if workenv(1:1) .eq. "F" return		!we already work with FITS files...
! 
write/out test of creating FITS images automatically
write/out "------------------------------------------"
!
replace/image flatf.fits &b <,300=0.0 >Null
compute/image &y = (444.4+&b)/12.
! 
write/out > -rename flatf.fits flatf.fitz
-rename flatf.fits flatf.fitz
write/out > set/midas newfiles=fits,fitz
set/midas newfiles=fits,fitz
! 
write/out > replace/image flatf.fitz &a <,300=0.0
replace/image flatf.fitz &a <,300=0.0 >Null
@a diffima middumma.fitz &b.bdf 0.0001
if outputi(10) .ne. 0 then
   errsum = errsum+1
   write/out ######## problem (a) with entry 0007
   goto setback
endif
! 
write/out > compute/image &t = (444.4+middumma.fitz)/12.
compute/image &t = (444.4+middumma.fitz)/12.
@a diffima middummt.fitz &y.bdf 0.0001
if outputi(10) .ne. 0 then
   errsum = errsum+1
   write/out ######## problem (b) with entry 0007
   goto setback
endif
! 
rename/ima middumma.fitz lola.fitz
rename/ima middummt.fitz kuki.fitz
write/out > create/icat kukiwu *.fitz ESO.DET.EXP.TYPE
create/icat kukiwu *.fitz ESO.DET.EXP.TYPE
write/out > show/icat kukiwu
show/icat kukiwu
write/out > read/icat kukiwu
read/icat kukiwu
write/out > read/descr #3_kukiwu
read/descr #3_kukiwu
setback:
set/midas newfile=midas			!go back to Midas format for new files
!
-delete flatf.fitz lola.fitz kuki.fitz kukiwu.cat 
-delete a_VLT_instrument_like_very_long_name_ident1.fits
-delete a_VLT_instrument_like_very_long_name_ident2.fits
-delete a_VLT_instrument_like_very_long_name_ident3.fits
! 
entry 0008
!
if aux_mode(1) .eq. 1 return                    !not implemented yet for VMS
! 
write/out some tests of handling FITS tables
write/out "----------------------------------"
! 
write/out > write/table testtab.mt :R6 @1 66.66
write/table testtab.mt :R6 @1 66.66
write/out > testtab.mt,:r7,@1 = 77.77
testtab.mt,:r7,@1 = 77.77
write/out > read/table testtab.mt
read/table testtab.mt
write/out > show/table testtab.mt
show/table testtab.mt
inputr = m$value(testtab.mt,:r7,1)
write/key rcorr 77.77
@@ kcompare rcorr inputr 1,1 0.001
read/descr testtab.mt ident
write/descr testtab.mt ident kukiwuki
copy/dk testtab.mt ident out_a
if out_a .ne. "kukiwuki" then   errsum = errsum+1
   write/out ######## problem with entry 0008
   return
endif
!
delete/table test noconf
!
entry 0009
!
if mid$sys(1:5) .eq. "PC/Cy" then
   define/local im_a/c/1/96 FORS.1999-01-27T05c43c50.495.fits
else
   define/local im_a/c/1/96 FORS.1999-01-27T05:43:50.495.fits
endif

inputi = m$exist("MID_TEST:{im_a}")
if inputi .ne. 1 then
   nodemo = nodemo + 1
   $ echo "missing file: {im_a} in verify8.prg" >> ./missing-files
   return
endif
! 
write/out  some tests with a FORS1 image
write/out "-----------------------------"
! 
-copy MID_TEST:{im_a} {im_a}
! 
define/local cv/r/1/2 280,8300
if dispyes(1) .eq. 1 then
   load/lut rainbow4
   load/ima {im_a} cuts={cv(1)},{cv(2)}
endif
write/out > extract/image &ll = {im_a} [<,<:@1040,@1024]	!lower left
extract/image &ll = {im_a} [<,<:@1040,@1024]	!lower left
write/out > extract/image &lr = {im_a} [@1041,<:>,@1024]
extract/image &lr = {im_a} [@1041,<:>,@1024]
write/out > extract/image &ul = {im_a} [<,@1025:@1040,>]	!upper left
extract/image &ul = {im_a} [<,@1025:@1040,>]	!upper left
write/out > extract/image &ur = {im_a} [@1041,@1025:>,>]
extract/image &ur = {im_a} [@1041,@1025:>,>]
! 
define/local minim/r/1/4 0. all
define/local fact/r/1/1 0. 
! 
statist/image &ll [151.034,-27.3490:151.029,-27.3458]
minim(1) = outputr(3)
statist/image &lr [151.011,-27.3500:151.007,-27.3468]
minim(2) = outputr(3)
statist/image &ul [151.035,-27.3347:151.030,-27.3315]
minim(3) = outputr(3)
statist/image &ur [151.009,-27.3307:151.005,-27.3274]
minim(4) = outputr(3)
! 
fact = minim(1) / minim(2)
write/out > compute/imag &lr1 = &lr * {fact}
compute/imag &lr1 = &lr * {fact}
fact = minim(1) / minim(3)
write/out > compute/imag &ul1 = &ul * {fact}
compute/imag &ul1 = &ul * {fact}
fact = minim(1) / minim(4)
write/out > compute/imag &ur1 = &ur * {fact}
compute/imag &ur1 = &ur * {fact}
! 
write/out > compute/image &new = "{im_a}" * 0.0
compute/image &new = "{im_a}" * 0.0
write/out > insert/image &ll &new
insert/image &ll &new
write/out > insert/image &lr1 &new
insert/image &lr1 &new
write/out > insert/image &ul1 &new
insert/image &ul1 &new
write/out > insert/image &ur1 &new
insert/image &ur1 &new
if dispyes(1) .eq. 1 then
   load/ima &new cuts={cv(1)},{cv(2)}
endif
statist/image &new
write/keyw rcorr  194,76675.3,2839.6,1019.6,51.3063,3266.62
@@ kcompare rcorr outputr 1,6 0.1
write/keyw icorr  4259840,11,363,1723,47
@@ kcompare icorr outputi 1,5


