! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure all_tutos.prg to execute ALL TUTORIAL commands
! with no interaction. all_itutos.prg  execute TUTORIAL with interaction.
! C. Guirao  941104
! KB  981001, 990804, 030812, 040408
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 PseudoColor ? "Enter PseudoColor or RGB:"
!
write/keyw total_time/i/1/3 0,0,0
total_time(1) = m$secs()
! 
write/keyword alltutos/i/1/1 1
! 
WRITE/OUT CLEAR/CONTEXT -ALL
CLEAR/CONTEXT -ALL
!
WRITE/OUT "@@ veriall"
@@ veriall
if p1 .eq. "RGB" then
   @@ verify55
   reset/display
endif
if aux_mode .ne. 1 then			!Unix
   define/local aux/c/1/1 " "
else
   define/local aux/c/1/2 ".*"
endif
-delete veri*.*{aux}
-delete tst*.mt{aux}
-delete creamask.prg{aux}
-delete *compare.prg{aux}
-delete works.prg{aux}
! -delete all_*.prg{aux}
!
write/out
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/FILTER AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORI/FILT AUTO
RESET/DISPLAY
!
write/out
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/GRAPHIC AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORI/GRAP AUTO
RESET/DISPLAY
!
write/out
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/ITT AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORI/ITT AUTO
RESET/DISPLAY
!
write/out
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/TABLE AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORI/TABL AUTO
RESET/DISPLAY
!
write/out
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/FIT AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORI/FIT AUTO
RESET/DISPLAY
!
write/out
WRITE/OUT SET/CONTEXT CLOUD
SET/CONTEXT CLOUD
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/CLOUD AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORIAL/CLOUD AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
write/out
WRITE/OUT SET/CONTEXT ECHELLE
SET/CONTEXT ECHELLE
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/ECHELLE AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORIAL/ECHELLE AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
write/out
WRITE/OUT SET/CONTEXT INVENT
SET/CONTEXT INVENT
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/INVENTORY AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORIAL/INVENTORY AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
write/out
WRITE/OUT SET/CONTEXT IRSPEC
SET/CONTEXT IRSPEC
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/IRSPEC AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORIAL/IRSPEC AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
delete/table irsbadpix no
delete/table irstab no
!
write/out
WRITE/OUT SET/CONTEXT LONG
SET/CONTEXT LONG
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/LONG AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORIAL/LONG AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
write/out
WRITE/OUT SET/CONTEXT MOS
SET/CONTEXT MOS
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/MOS AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORIAL/MOS AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
write/out
WRITE/OUT SET/CONTEXT WAVELET
SET/CONTEXT WAVELET
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/WAVELET AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORIAL/WAVELET AUTO 1
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
write/out
WRITE/OUT SET/CONTEXT DO
SET/CONTEXT DO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
WRITE/OUT TUTORIAL/DO AUTO
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out 
TUTORIAL/DO AUTO
CLEAR/CONTEXT -ALL
RESET/DISPLAY
!
-DELETE FIT*.bdf{aux}
-DELETE TEST.bdf{aux}
-DELETE REF.bdf{aux}
-DELETE ref.bdf{aux}
-DELETE psft.bdf{aux}
-DELETE PROFILE.bdf{aux}
-DELETE NOISE.bdf{aux}
-DELETE FITTED.bdf{aux}
-DELETE FUNCTION.fit{aux}
-DELETE ORIGINAL.fit{aux}
-DELETE *.mt{aux}
-DELETE bias*.*{aux}
-DELETE cas*.bdf{aux}
-DELETE cloud*.bdf{aux}
-DELETE conv*.bdf{aux}
-DELETE cosmics.bdf{aux}
delete/table cloudt no
delete/table order no
delete/table texample no
delete/table tatest no
delete/table tstest no
delete/table thar no
delete/table ff_rule no
delete/table absp no
delete/table back no
delete/table line no
delete/table ltt1020 no
delete/image lnbias no
delete/image ffcor no
delete/image arti no
delete/image blaze no
delete/image invtest no
delete/image spectrum no
-delete tugc.*{aux}
-DELETE flat.cat{aux}
-DELETE ses*.tbl{aux}
-DELETE demo_mos*.bdf{aux}
-DELETE demos00*.bdf{aux}
-DELETE ext00*.bdf{aux}
-DELETE ext00*.tbl{aux}
-DELETE ext*.bdf{aux}
-DELETE susi_*.tbl{aux}
-DELETE std*.bdf{aux}
-DELETE syn*.bdf{aux}
-DELETE resp*.*{aux}
-DELETE mos.tbl{aux}
-DELETE mostab.tbl{aux}
-DELETE wlc*.bdf{aux}
-DELETE wlct.tbl{aux}
-DELETE lnf*.bdf{aux}
-DELETE lndemo*.*{aux}
-DELETE linpos.tbl{aux}
-DELETE coerbr.tbl{aux}
-DELETE demos_hear.tbl{aux}
-DELETE window.tbl{aux}
-DELETE windows.tbl{aux}
-DELETE klaus.fit{aux}
-DELETE middumm*{aux}
-DELETE dirfile.*{aux}
-DELETE d*.dat{aux}
-DELETE temp*.*{aux}
-DELETE *.plt{aux}
-DELETE screen*.*{aux}
-DELETE mysess*.*{aux}
-DELETE proctest.ctx{aux}
-DELETE Mid_Pipe{aux}
! 
delete/keyword alltutos
! 
total_time(2) = m$secs() - total_time(1)
total_time(3) = total_time(2)/60
total_time(1) = total_time(2) - total_time(3)*60
set/format i1
write/out
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out Procedure `all_tutos' took {total_time(3)} min {total_time(1)} sec
write/out ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
write/out
! 
BYE
