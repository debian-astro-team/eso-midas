! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify15.prg  to verify MIDAS commands
!  for long filenames
! 
!  K. Banse     060222, 071019, 080731, 091029, 101027
!
!  use as @@ verify15 ffffffffffffffffffff         with f = 1 or 0 (on/off)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 111111111111111 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local seconds/i/1/2 0,0 ? +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/15 000000000000000
define/local secs/i/1/2 0,0 ? +lower
define/local myvals/i/1/2 0,0 ? +lower
define/local longdir1/c/1/60 verylongdirectoryforfilestotestmidas ? +lower
define/local longdir2/c/1/60 anotherverylongdirectoryforfilestotestmidas ? +lower
!
delete/temp                             !get rid of old temporary files
! 
write/key sizez/i/1/5 600,600,0,0,0
write/key dispyes/i/1/2 0,0
!
write/out +------------------------------------------+
write/out Start of procedure verify15.prg
write/out +------------------------------------------+
!
seconds(1) = m$secs()
write/key ccc {p1}
set/format i1
do loop = 1 15
   if ccc({loop}:{loop}) .eq. "1" @@ verify15,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify15.prg!"
      return 1
   endif
enddo
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
! 
! we have to execute this entry after all others, because all local
! keywords disappear after SET/MIDAS keyw=...
! 
@@ verify15,last
!
write/out +------------------------------------------+
write/out procedure verify15.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
!
!  here the different sub-procedures
!
entry 0001
!
write/out test of LOAD/TABLE
write/out "------------------"
! 
reset/display
create/display 7 512,512,616,300 p5=40000	!lots of drawing...
dispyes(1) = 1					!mark that we have display 
load/lut heat
! 
write/out > load/image order.fits scale=-3
load/image order.fits scale=-3
write/out > load/table order.tfits :x :y :order 0 0 4
load/table order.tfits :x :y :order 0 0 4
! 
entry 0002
!
if aux_mode(1) .lt. 2 return
! 
write/out create subdirs with long names
write/out "------------------------------"
! 
open/file makelongdir.sh write inputi
write/file {inputi} "#!/bin/sh "
write/file {inputi} "mkdir {longdir1}"
write/file {inputi} "cd ./{longdir1}"
write/file {inputi} "mkdir {longdir2}"
write/file {inputi} "cd ./{longdir2}"
write/file {inputi} "echo now in `pwd`"
write/file {inputi} "cp ../../wcstest.bdf ."
close/file {inputi}
!
indisk/fits wcstest.mt wcstest.bdf
!
$ cat ./makelongdir.sh
$ chmod +x ./makelongdir.sh
$ ./makelongdir.sh
!
entry 0003
!
write/out test of loooooooong filenames
write/out "-----------------------------"
! 
$ rm -f middummz*
write/out > copy/ii LONG_FILE_NAME &z
copy/ii ./{longdir1}/{longdir2}/wcstest &z
if m$exist("middummz.bdf") .ne. 1 then
   write/out problems with copy/ii
   errsum = errsum + 1
   return
endif
! 
write/out > copy/dk LONG_FILE_NAME  npix outputi
copy/dk ./{longdir1}/{longdir2}/wcstest npix outputi
if outputi(1) .ne. 353 .or. outputi(2) .ne. 353 then
   write/out problems with copy/dk
   errsum = errsum + 1
   return
endif
! 
write/out > read/descr LONG_FILE_NAME
read/descr ./{longdir1}/{longdir2}/wcstest 
! 
write/out > fft/image LONG_FILE_NAME
fft/image ./{longdir1}/{longdir2}/wcstest 
! 
write/keyw rcorr  2.981000e+03,1.395000e+04
write/out > statistics/image LONG_FILE_NAME 
statist/image ./{longdir1}/{longdir2}/wcstest 
@@ kcompare rcorr outputr 1,2 0.3
!
entry 0004
!
write/out test of erroneous ESO descriptors in FITS file
write/out "----------------------------------------------"
! 
indisk/fits F1_PZPI_050103A_VBE_fhis.tfits eso-descr
if m$exist("eso-descr.tbl") .ne. 1 then
   write/out problems with reading faulty ESO descriptors from FITS header"
   errsum = errsum + 1
   return
endif
! 
entry 0005
!
write/out test of statistics like STATIST/IMA on table column
write/out "---------------------------------------------------"
! 
write/keyw rcorr/r/3/2 5863.042,1644.981
write/keyw rcorr/r/8/2 5650.704,3653.93 
write/out > statistics/image hear.tbl,:wave
statistics/image hear.tbl,:wave
@@ kcompare rcorr outputr 3,2 0.05
@@ kcompare rcorr outputr 8,2 0.05
statistics/image tcol2ima
@@ kcompare rcorr outputr 3,2 0.05
@@ kcompare rcorr outputr 8,2 0.05
! 
write/keyw rcorr/r/8/2 5641.884,3653.93 
write/out > statistics/image hear.tbl,:wave p5=ff
statistics/image hear.tbl,:wave p5=ff
@@ kcompare rcorr outputr 3,2 0.05
@@ kcompare rcorr outputr 8,2 0.05
statistics/image tcol2ima p5=ff
@@ kcompare rcorr outputr 3,2 0.05
@@ kcompare rcorr outputr 8,2 0.05
! 
entry 0006
!
write/out test of reading wrong syntax FITS header 
write/out "----------------------------------------"
! 
read/descr badfitskeys.mt
!
entry 0007
!
write/out test of adaptive filter
write/out "-----------------------"
! 
write/out > filter/adaptive hbo.fits &new ? L P 15 4 a
filter/adaptive hbo.fits &new ? L P 15 4 a
statistics/image &new
write/keyw rcorr/r/5/2  -37.7547,2228.83
@@ kcompare rcorr outputr 5,2 0.06
!
entry 0008
!
write/out test of creating lightcurve for objects
write/out "---------------------------------------"
!
if dispyes(1) .ne. 1 then
   return
endif
!
indisk/fits image_M12c.fits M12
write/out > use object at 282,214 in f.p. (7.5593835,4.8141313 in w.c.)
write/out > @a gcurve M12 7.5593835,4.8141313 obj_curve 30 g
@a gcurve M12 7.5593835,4.8141313 obj_curve 30 g
copy/tk obj_curve :npix,:flux,:flux_norm @19 outputr/r/1/3
write/keyw rcorr/r/1/3 1014.19,987.877,0.97405
@@ kcompare rcorr outputr 1,3 0.05
!
entry 0009
!
write/out test of plot/selected
write/out "---------------------"
!
if dispyes(1) .ne. 1 then
   return
endif
!
show/table coerbr
create/graph 1 1000,500,0,600
write/out > sele/tab coerbr.tbl :coef_2.le.3.2375
sele/tab coerbr.tbl :coef_2.le.3.2375
set/graph stype=5
write/out > plot/sele coerbr.tbl :coef_2 :rms
plot/sele coerbr.tbl :coef_2 :rms
set/graph stype=0 ltype=1
write/out > plot/sele coerbr.tbl seq :rms
plot/sele coerbr.tbl seq :rms p7=blue
return				!spare
!
entry 00010
!
write/out test of creating tables from ASCII files 
write/out "----------------------------------------"
! 
$sed s/"3.025817         54319.500205"/"3.025817 "/ R_UL.asc >  R_new.asc
! 
write/out > create/table R_table 3 1000 R_new.asc R_UL.fmt
create/table R_table 3 1000 R_new.asc R_UL.fmt
outputr = m$value(R_table.tbl,#3,@71)
read/key outputr
write/keyw rcorr/r/1/1 54363.28505
@@ kcompare rcorr outputr 1,1 0.1
inputi = m$tnull(R_table.tbl,#3,@21)
if inputi .ne. 1 then
   write/out problems with create/table and NULL value
   errsum = errsum + 1
   return
endif
write/out > read/tab R_table
read/tab R_table
!
entry 00011
!
write/out test of CENTER/1DGAUSS
write/out "----------------------"
! 
create/image tg 1,512 ? gauss 255.0,10.
! 
create/image biggi 1,2000 0.,1. poly + R
insert/image tg biggi <
insert/image tg biggi @400
insert/image tg biggi @800
insert/image tg biggi @1500
! 
write/out > center/1dgauss biggi,cnttable koko
center/1dgauss biggi,cnttable.fits koko
! 
outputr(1) =  m$value(koko,:xcen,@2)
outputr(2) =  m$value(koko,:xsig,@2)
outputr(3) =  m$value(koko,:xfwhm,@2)
write/keyw rcorr  654.0,10.0,23.548
@@ kcompare rcorr outputr 1,3 0.01
read/table koko
! 
entry 00012
!
write/out test of extract/reference_frame
write/out "-------------------------------"
! 
indisk/fits wcstest.mt wcs.bdf
write/out > copy/ii wcs &i i4
copy/ii wcs &i i4
statistics/image &i
write/out > find/pixel &i 3811.5,3812.5 in all
find/pixel &i 3811.5,3812.5 in all
if outputi .ne. 159 then
   write/out problems with find/pixel
   errsum = errsum + 1
   return
endif
write/out > extra/reference &i &i &o 3811.5,3812.5 in
extract/reference &i &i &o 3811.5,3812.5 in
if outputi(15) .ne. 159 then
   write/out problems with extract/reference
   errsum = errsum + 1
   return
endif
write/out > extra/reference &i &i &o <,28005 in
extract/reference &i &i &o <,2800 in
if outputi(15) .ne. 0 then
   write/out problems with extract/reference
   errsum = errsum + 1
   return
endif
! 
replace/ima wcstest.mt &test <,3200.=14000.
@a clean &test &good 13900.,>
read/key outputr
write/keyw rcorr/r/1/4  3201,14000,3808.51,597.479
@@ kcompare rcorr outputr 1,4 0.5
! 
if dispyes(1) .eq. 1 then
   clear/chan ov
   load/lut heat
   load &test
   wait/secs 1
   load &good
endif
! 
entry 00013
!
write/out test of INTEGRATE/ELLIPS
write/out "------------------------"
!
inputi = m$exist("MID_TEST:ngc2997.fits")
if inputi .ne. 1 then
   write/out "we need ngc2997.fits from the Midas demo dir. for this test"
   write/out "but no Midas demo directory found - we skip"
   return
endif
!
write/out > set/context surfphot
set/context surfphot
indisk/fits MID_TEST:ngc2997.fits NGC2997
if dispyes(1) .eq. 1 then
   clear/chan ov
   load NGC2997
endif
write/out > integrate/ellips NGC2997 129.,127.,13.,3.,0.
integrate/ellips NGC2997 129.,127.,13.,3.,0.
copy/dk NGC2997 ellpar/r/6/1 outputd
write/keyw rcorr/r/1/1   339129.1
@@ kcompare rcorr {outputd} 1,1 0.9
! 
entry 00014
!
write/out test of extending FITS tables
write/out "-----------------------------"
! 
set/midas newfil=?? >Null
if outputc(1:1) .eq. "M" then  			!we're in Midas env.
   define/local envi/i/1/1 1
else
   define/local envi/i/1/1 0
endif
! 
set/midas workenv=fits				!switch to FITS env.
set/midas newfile=fits,fits,tfits
crea/tabl tabbi 2 3
show/tab tabbi
create/row tabbi @0 2
create/colu tabbi :aa float
create/colu tabbi :bb int
show/tab tabbi
tabbi.tfits,:aa,@1 = 123.456
write/table tabbi :bb @2 -456
read/tab tabbi
outputi = {tabbi,:bb,@2}
if envi .eq. 1 then			!if needed reset the environment
   set/midas workenv=midas
   set/midas newfile=bdf,tbl,fit
endif
! 
if outputi(1) .ne. -456 then
   write/out problems with FITS tables
   errsum = errsum + 1
   return
endif
! 
entry 00015
!
write/out test of astrometry command
write/out "--------------------------"
! 
write/out > set/context astromet
set/context astromet
write/out > astro/comp xxopp.fits 2 xxast.fits
astro/comp xxopp.fits 2 xxast.fits >astrom.out
define/local idi/i/1/2
open/file ./astrom.out read idi
if idi(1) .lt. 1 then
   write/out could not open file astrom.out
   errsum = errsum + 1
   close/file {idi}
   return
endif
do outputi = 1 16
   read/file {idi} inputc
   if idi .lt. 1 then
      write/out could not read line {outputi} of astrom.out
      errsum = errsum + 1
      close/file {idi}
      return
   endif
   if outputi .eq. 2 then		!line 2 holds no. of objects
   outputi(9) = {inputc(1:18)}
   write/keyw icorr/i/9/1  6240
   @@ kcompare icorr outputi 9,9
   endif
enddo
close/file {idi}
! 
write/out "> for object no. 5 we get:"
write/out " " {inputc}
outputi = 1
do inputi = 1 42		!replace (multiple) blanks by single ","
   if inputc({inputi}:{inputi}) .ne. " " then
      outputc({outputi}:{outputi}) = inputc({inputi}:{inputi})
      outputi = outputi + 1
   else if inputi .ne. 1 then
      outputi(2) = outputi - 1
      if outputc({outputi(2)}:{outputi(2)}) .ne. "," then
         outputc({outputi}:{outputi}) = ","
         outputi = outputi + 1
      endif
   endif
enddo
outputi = m$parse(outputc(2:),"papa")	!omit leading ","
write/keyw icorr 5,17,48,-29,4
outputi(1) = {papa01}
outputi(2) = {papa02}
outputi(3) = {papa03}
outputi(4) = {papa05}
outputi(5) = {papa06}
@@ kcompare icorr outputi 1,5
write/keyw rcorr 47.717,36.52
outputr(1) = {papa04}
outputr(2) = {papa07}
@@ kcompare rcorr outputr 1,2 0.001
! 
entry last
!
write/out test of extending keyword area
write/out "------------------------------"
! 
set/midas keywords=??
outputi(1) = outputi(1) + 100
outputi(2) = outputi(2) + 200
set/midas keywords={outputi},{outputi(2)}
set/midas keywords=?
! 
 


