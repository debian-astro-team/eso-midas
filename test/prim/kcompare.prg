! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure kcompare.prg
!  K. Banse     920910, 980526, 090717, 100319, 110427
!
!  use as @@ kcompare labref:keyref keynew indexa,indexb eps
!      or             keyref keynew indexa,indexb eps
!  with   labref: = optional reference label displayed for failed checks
!         keyref = name of reference keyword
!         keynew = name of new keyword
!         indexa,indexb = start + end index for comparison
!  and following params only for real/double keywords
!         eps = epsilon for difference from indexa-b
!         indexc,indexd = start + end index for comparison
!         eps = epsilon for difference from indexc-d
!         indexe,indexf = start + end index for comparison
!         eps = epsilon for difference from indexe-f
! 
!  we use he global integer keyword errsum to pass error info 
!  back to the calling procedure, i.e. keyw. errsum must exist!
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ? ? "Enter labref:refkey "
define/par p2 ? ? "Enter newkey: "
define/par p3 1,1 n "Enter indexa,indexb: "
define/par p4 0. n "Enter epsilon for difference check: "
!
define/local myp5/c/1/20 {p5}		!optional 2nd interval
define/local myp7/c/1/20 {p7}		!optional 3rd interval
define/local mypara/c/1/20 {p3}
define/local myparb/c/1/20 {p4}
define/local loopi/i/1/3 0 all +lower
define/local failindx/i/1/1 0 ? +lower		!index of 1st difference
define/local labref/c/1/20 "? " ? +lower
! 
define/local ktype/i/1/1 0 
define/local idif/i/1/1 0 
define/local rdif/d/1/1 0.
define/local reps/r/1/1 0.
define/local ddif/d/1/1 0.
define/local deps/d/1/1 0.
define/local idx/i/1/1 0 
! 
idx = m$index(p1,":") 
if idx .gt. 1 then
   idx = idx - 1
   labref(1:) = "["//p1(1:{idx})//"]  "
   idx = idx + 2
   p1(1:) = p1({idx}:)
endif
! 
info/keyw {p1}
ktype = mid$info(1)			!save keytype
if ktype .eq. 0 .or. ktype .eq. 3 then
   write/out invalid keyword {p1}, or invalid type ...
   errsum = errsum+1
   return
endif
! 
again:
write/keyw loopi/i/1/3 0,{mypara} 
! 
if ktype .eq. 1 then
   ! 
   do loopi = {loopi(2)} {loopi(3)}
      idif = m$abs({p1}({loopi}) - {p2}({loopi}))
      if idif .gt. 0 then
         if failindx .eq. 0 failindx = loopi
      endif
   enddo
   !
! 
else if ktype .eq. 2 then
   reps = {myparb}
   ! 
   do loopi = {loopi(2)} {loopi(3)}
      rdif = m$abs({p1}({loopi}) - {p2}({loopi}))
      if rdif .gt. reps then
         if failindx .eq. 0 failindx = loopi
      endif
   enddo
   ! 
else 
   deps = {myparb}
   ! 
   do loopi = {loopi(2)} {loopi(3)}
      ddif = m$abs({p1}({loopi}) - {p2}({loopi}))
      if ddif .gt. deps then
         if failindx .eq. 0 failindx = loopi
      endif
   enddo
   ! 
endif
if failindx .ne. 0 then
   @@ kcompare,out {p1} {p2} 
   return
endif
if myp5(1:1) .ne. "?" then
   write/keyw mypara/c/1/20 {p5}
   write/keyw myparb/c/1/20 {p6}
   failindx = 0
   myp5(1:2) = "? "
   goto again
endif
if myp7(1:1) .ne. "?" then
   write/keyw mypara/c/1/20 {p7}
   write/keyw myparb/c/1/20 {p8}
   failindx = 0
   myp7(1:2) = "? "
   goto again
endif
! 
entry out
set/format j2 g15.10
write/out
if labref(1:1) .eq. "?" then
   write/out "     reference:         new:     "
else
   write/out "     reference:         new:         label: " {labref}
endif
do loopi = {loopi(2)} {loopi(3)}
   if loopi .eq. failindx then
      write/out ({loopi}) {{p1}({loopi})} " *** " {{p2}({loopi})}
   else
      write/out ({loopi}) {{p1}({loopi})} "     " {{p2}({loopi})}
   endif
enddo
errsum = errsum+1
  
