# @(#)clean.sed	19.1 (ESO-DMD) 02/25/03 14:34:02
# 
# "clean.sed" - script for sed
# to clean Midas logfile before comparing the logs from
# verifications with different Midas versions
# use via: sed -f clean.sed Midas_logfile >cleaned_logfile
#  
# K. Banse	last modif:	030121
# 
# delete all lines with ""
//d

# delete all lines with "Page" and the following (empty) line
/Page/{
N
d
}

# delete all lines with specific patterns

/home\/esomidas/d

/elapsed time/d

/time elapsed/d

/DATE    =/d
/DATE:/d

/yields:/d

/expanding frame -/d

/created on:/d

/vimos.fits took/d

# the end

