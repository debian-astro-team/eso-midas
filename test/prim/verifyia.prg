! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verifyia.prg  to verify interactive MIDAS commands
!  K. Banse     030325
!
!  use as @@ verifyia ffffffffff             with f = 1 or 0 (on/off)
!
! 030701	last modif
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 111111111100 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local seconds/i/1/2 0,0 ? +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/50 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local secs/i/1/2 0,0 ? +lower
define/local myvals/i/1/6 0 all +lower
!
delete/temp                             !get rid of old temporary files
! 
write/key sizez/i/1/5 400,400,0,0,0
write/key dispyes/i/1/2 0,0
write/key stop/i/1/1 0
!
write/out +------------------------------------------+
write/out Start of procedure verifyia.prg
write/out +------------------------------------------+
!
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
! 
seconds(1) = m$secs()
set/format i1
do loop = 1 9
   if p1({loop}:{loop}) .eq. "1" @@ verifyia,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verifyia.prg!"
      stop = 1
      return
   endif
enddo
! 
seconds(2) = m$secs()
ival = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verifyia.prg:
write/out Total time elapsed after first 9 tests = {ival} seconds.
!
! because of the clear/contetx command we loose all local keywords,
! therefore entry 10, 11, ... execution comes here...
! 
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
! 
!  here the different sub-procedures
!
entry 0001
!
write/out test of CREATE/DISPLAY + CREATE/GRAPHICS
write/out "----------------------------------------"
!
reset/display
create/display 0 4,4,0,0 ? no
mid$info(4) = dazdevr(12)-525
mid$info(5) = dazdevr(13)-560
modify/display icon
create/display 7 512,512,{mid$info(4)},{mid$info(5)}
mid$info(4) = mid$info(4) - 608
mid$info(5) = dazdevr(13)-420
create/gra 3 600,400,{mid$info(4)},{mid$info(5)}
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
display/lut off
load/lut rainbow4
!
entry 0002
!
write/out  test of GET/CURSOR
write/out "-------------------"
!
if dispyes(1) .eq. 0 return
! 
load/image wcstest.mt
draw/circle 251,311,10 s n cyan
draw/circle 329,244,10 s n cyan
draw/circle 104,95,10 s n cyan
draw/circle 417,337,10 s n cyan
label/display 1 326,236 ? cyan
label/display 2 259,314 ? cyan
label/display 3 110,89 ? cyan
label/display 4 352,402 ? cyan
! 
write/out > now, use the cursor and click on the center of the circles
write/out > in increasing order of the labels
get/cursor curs_tab ? NN	!no markers
! 
read/table curs_tab
! 
inputi = m$value(curs_tab.tbl,:X_coordpix,@1)
if inputi .lt. 170 .or. inputi .gt. 174 then
   write/out cursor position 1 is out of range...
endif
inputi = m$value(curs_tab.tbl,:Y_coordpix,@1)
if inputi .lt. 231 .or. inputi .gt. 235 then
   write/out cursor position 1 is out of range...
endif
inputi = m$value(curs_tab.tbl,:X_coordpix,@2)
if inputi .lt. 248 .or. inputi .gt. 252 then
   write/out cursor position 2 is out of range...
endif
inputi = m$value(curs_tab.tbl,:Y_coordpix,@2)
if inputi .lt. 164 .or. inputi .gt. 168 then
   write/out cursor position 2 is out of range...
endif
inputi = m$value(curs_tab.tbl,:X_coordpix,@3)
if inputi .lt. 23 .or. inputi .gt. 27 then
   write/out cursor position 3 is out of range...
endif
inputi = m$value(curs_tab.tbl,:Y_coordpix,@3)
if inputi .lt. 14 .or. inputi .gt. 18 then
   write/out cursor position 3 is out of range...
endif
inputi = m$value(curs_tab.tbl,:X_coordpix,@4)
if inputi .lt. 336 .or. inputi .gt. 340 then
   write/out cursor position 4 is out of range...
endif
inputi = m$value(curs_tab.tbl,:Y_coordpix,@4)
if inputi .lt. 256 .or. inputi .gt. 260 then
   write/out cursor position 4 is out of range...
endif
!
entry 0003
!
if dispyes(1) .ne. 1 return
!
write/out test of zoom window
write/out "-----------------"
!
write/out "... in the following GET/CURSOR command a zoom window will pop up"
write/out -
"... click once in the main window, then click once in the zoom window"
write/out "... after getting the output, exit from the command by"
write/out "... clicking the right mouse button in the zoom window and "
write/out "... clicking the right mouse button in the main window"
write/out "... an additional terminal window will display explanatory help text"
write/out
!
write/out > get/cursor p5=w
get/cursor p5=w
wait/secs 1.0
!
entry 0004
!
if dispyes(1) .ne. 1 return
!
write/out  test of MODIFY/DISP, DELETE/GRAPH, ASSIGN/DISP
write/out "----------------------------------------------"
!
define/local wini/i/1/20 0 all +lower
!
reset/display
write/out > create 4 graphic windows
create/grap 1 250,100,50,500
create/grap 2 250,150,250,400
create/grap 3 300,200,500,300
create/grap 4 400,300,300,200
write/key icorr/i/32/2 400,300
@@ kcompare icorr ididev 32,34
write/key icorr/i/39/4 100000,0,400,300
@@ kcompare icorr ididev 39,42
!
write/out > create 2 display windows
create/disp
create/disp 1 250,250,600,300
write/key icorr/i/2/2 250,250
@@ kcompare icorr ididev 2,3
write/key icorr/i/9/4 10000,99,250,250
@@ kcompare icorr ididev 9,12
write/key icorr/i/14/3 1,0,1
@@ kcompare icorr ididev 14,16
!
wait/secs 0.5
write/out > move active graphics (4) + display window (1) to icons
modify/gra icon
modify/disp icon
@@ kcompare icorr ididev 2,3
@@ kcompare icorr ididev 9,12
@@ kcompare icorr ididev 32,34
@@ kcompare icorr ididev 39,42
write/keyw wini/i/1/2 1,2
write/keyw wini/i/12/4 1,1,1,2
@@ kcompare wini winopen 1,20
!
wait/secs 0.2
write/out > make graphics window 2 the active one
assign/gra g,2
write/keyw icorr/i/32/2 250,150
@@ kcompare icorr ididev 32,34
write/key icorr/i/39/4 100000,0,250,150
@@ kcompare icorr ididev 39,42
!
wait/secs 0.2
write/out >  make display window 0 the active one
assign/disp d,0
display/chan 1
write/key icorr/i/2/2 512,512
@@ kcompare icorr ididev 2,3
write/key icorr/i/9/4 30000,99,512,512
@@ kcompare icorr ididev 9,12
write/key icorr/i/14/3 2,1,2
@@ kcompare icorr ididev 14,16
!
wait/secs 0.5
write/out > delete graphics w. 2
dele/graph 2
write/keyw icorr/i/32/2 250,100
@@ kcompare icorr ididev 32,34
write/key icorr/i/39/4 100000,0,250,100
@@ kcompare icorr ididev 39,42
!
wait/secs 0.5
write/out > delete graphics w. 4 and make graphics w. 3 the active one
dele/graph 4
assign/gra g,3
write/keyw icorr/i/32/2 300,200
@@ kcompare icorr ididev 32,34
write/key icorr/i/39/4 100000,0,300,200
@@ kcompare icorr ididev 39,42
!
wait/secs 0.5
write/out > delete display window 0
dele/disp 0
write/key icorr/i/2/2 250,250
@@ kcompare icorr ididev 2,3
write/key icorr/i/9/4 10000,99,250,250
@@ kcompare icorr ididev 9,12
write/key icorr/i/14/3 1,0,1
@@ kcompare icorr ididev 14,16
!
wait/secs 0.5
write/out > move active display window back to screen
modify/disp win
write/keyw wini/i/1/2 0,1
write/keyw wini/i/12/4 1,0,1,0
@@ kcompare wini winopen 1,20
!
wait/secs 0.5
write/out > delete graphics w. 3
dele/graph 3
write/keyw icorr/i/32/2 250,100
@@ kcompare icorr ididev 32,34
write/key icorr/i/39/4 100000,0,250,100
@@ kcompare icorr ididev 39,42
@@ kcompare icorr ididev 2,3
@@ kcompare icorr ididev 9,12
@@ kcompare icorr ididev 14,16
!
write/keyw wini/i/12/4 1,0,0,0
@@ kcompare wini winopen 1,20
write/out > show/display
show/display
!
wait/secs 0.5
write/out > reset/display
reset/disp
! 
entry 0005
!
write/out  test of `drs' command (Midas command from Unix command line)
write/out "------------------------------------------------------------"
!
if aux_mode(1) .eq. 1 then
   write/out this test only on Unix systems...
   return
endif
!
! use keywords for safety
in_a = m$symbol("MIDASHOME")
in_b = m$symbol("MIDVERS")
!
! copy shell script + make it executable
$ cp $MIDASHOME/$MIDVERS/test/prim/testdrs.sh testdrs.sh
$ chmod +x testdrs.sh
if dispyes(1) .ne. 1 then
   $ ./testdrs.sh {in_a} {in_b}
else
   $ xterm -geometry 80x25+10+10 -e ./testdrs.sh {in_a} {in_b}
endif
!
copy/dk kukiwuki.bdf statistic outputr
write/key rcorr/r/1/4  0.0,418100.2,122077.9,98424.14
@@ kcompare rcorr outputr 1,4 0.1
write/key rcorr/r/5/2  0.6793876,2.514770
@@ kcompare rcorr outputr 5,6 0.0001
write/key rcorr/r/8/5   99092.72,819.804,256.0,1639.609,819.8043
@@ kcompare rcorr outputr 8,12 0.01
$ rm -f kukiwuki..bdf testdrs.sh
!
entry 0008
!
if dispyes(1) .ne. 1 return
! 
write/out test of COPY/DISPLAY
write/out "-------------------"
!
define/local lutsize/i/1/1 {ididev(17)}
if lutsize .gt. 150 lutsize = 150
reset/disp
write/out force to exactly {lutsize} colors:
write/out > "init/disp 1,{lutsize},{lutsize}"
init/disp 1,{lutsize},{lutsize}			!we want exactly 150 colors
mid$info(4) = dazdevr(12)-530
mid$info(5) = dazdevr(13)-570
create/display 7 512,512,{mid$info(4)},{mid$info(5)}
write/out > load ia.{imatype}
load ia.{imatype}
write/out > load/lut rainbow4
load/lut rainbow4
write/out > draw/circle 300,300,400,400 s ? red
draw/circle 300,300,400,400 s ? red
write/out > label/display "where are the stars?" 100,150 p5=1
label/display "where are the stars?" 100,150 p5=1
write/out > copy/display ? stop
copy/display ? stop
write/out save the screen.ima file:
-rename screen{mid$sess(11:12)}.ima screen{mid$sess(11:12)}.imaold
write/out make an Icon of the display window:
write/out > modify/display icon				!make an icon
modify/display icon				!make an icon
if mid$sys(1:3) .eq. "MAC" then
   wait/sec 1
   modify/display window			!redisplay the window
   goto post_check
endif
! 
write/out > copy/display ? stop
copy/display ? stop
write/out "check difference of the two screen copies:"
write/out > "compute/pixel &d = screen{mid$sess(11:12)}.ima-screen{mid$sess(11:12)}.imaold"
compute/pixel &d = screen{mid$sess(11:12)}.ima-screen{mid$sess(11:12)}.imaold
find/minmax &d >Null
if outputr(1) .gt. 0.00001 .or. outputr(1) .lt. -0.00001 then
   write/out difference of the two screen dumps should be 0
endif
if outputr(2) .gt. 0.00001 .or. outputr(2) .lt. -0.00001 then
   write/out difference of the two screen dumps should be 0
endif
write/out redisplay window on screen:
write/out > modify/display window	
modify/display window				!redisplay the window
! 
! check the Postscript file
post_check:
if aux_mode(1) .eq. 2 then
   display/lut
   copy/display colour p5=noprint
   reset/display
   ! 
   define/local progname/c/1/20 "ghostview  "
   inputi(10) = 1
   $rm -f kkkkkk.test
   $which {progname} > kkkkkk.test
   open/file kkkkkk.test read inputi
   if inputi(1) .gt. 0 then		!so the program does exist...
      read/file {inputi(1)} inputc
      if inputi(2) .gt. 0 then
         if inputc(1:1) .eq. "/" then
            write/out -
              "Please, check the Postscript file and then exit from {progname}"
            $ {progname} screen{mid$sess(11:12)}.ps 
            inputi(10) = 20
         endif
      endif
      close/file {inputi(1)}
   endif
   if inputi(10) .lt. 20 then
      write/out -
      "We could not find `ghostview' to show the Postscript file ..."
      write/keyw progname/c/1/20 " " all
      inquire/keyw progname -
      "Enter name of program to view the file, else Return only:"
      if aux_mode(7) .gt. 0 then
         $ {progname} screen{mid$sess(11:12)}.ps 
      endif
   endif
endif
! 
entry 0006
! 
if dispyes(1) .ne. 1 return
! 
write/out  test of extract/trace
write/out "---------------------"
!
resamp/image  dss_test2.fits iaa
load iaa cuts=2000,5000.
load/lut heat ? d
load iaa cuts=2000,5000.
draw/circ 192,135,10 s n white
draw/circ 412,391,10 s n white
write/out > extract a trace going through the two objects
extract/trace ? ? plot
cuts trace 2000,10000
write/out > now use the graphics cursor to mark the peaks
plot/row trace
get/gcursor klaus,table
read/table klaus
if m$value(klaus.tbl,:value,@1) .lt. 8000.0 then
   write/out first peak seems to be not o.k. ...
endif
if m$value(klaus.tbl,:value,@2) .lt. 5000.0 then
   write/out last peak seems to be not o.k. ...
endif
! 
clear/chan over
draw/arrow 173,281,189,281 s ?  
draw/arrow 230,417,230,389 s
draw/arrow 300,310,274,310 s
! 
write/out extract cursor window going through the 3 marked objects
extract/cursor
! 
load/image subframe cuts=1999,4999
! 
statistics/image subframe
inputr(1) = 12280.0 - 0.3
inputr(2) = 12280.0 + 0.3
if outputr(2) .lt. inputr(1) .or. outputr(2) .gt. inputr(2) then
   write/out the extracted cursor window is not what we expected ...
endif
wait/secs 0.5
! 
entry 0007
!
entry 0009
!
if dispyes(1) .ne. 1 return
! 
write/out  test of communicating with another Midas
write/out "----------------------------------------"
!
if aux_mode(1) .eq. 1 then
   write/out this test only on Unix systems...
   return
endif
!
$ cp $MID_MONIT/communi.test communi.test
$ chmod +w communi.test
reset/display >Null
$ xterm -geometry 80x25+10+10 -e sh  communi.test $MIDASHOME $MIDVERS 47
!

