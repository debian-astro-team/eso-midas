! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify12.prg  to verify MIDAS commands
!  K. Banse     020107, 020910, 031017, 080521, 091029
!
!  use as @@ verify12 ffffffffff             with f = 1 or 0 (on/off)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 11111111111111 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local seconds/i/1/2 0,0 ? +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local secs/i/1/2 0,0 ? +lower
define/local myvals/i/1/6 0 all +lower
!
delete/temp                             !get rid of old temporary files
! 
write/key sizez/i/1/5 600,600,0,0,0
write/key dispyes/i/1/2 0,0
!
write/out +------------------------------------------+
write/out Start of procedure verify12.prg
write/out +------------------------------------------+
!
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
! 
seconds(1) = m$secs()
set/format i1
do loop = 1 9
   if p1({loop}:{loop}) .eq. "1" @@ verify12,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify12.prg!"
      return 1
   endif
enddo
! 
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verify12.prg:
write/out Total time elapsed after first 9 tests = {mid$info(8)} seconds.
return 0
!
! because of the clear/contetx command we loose all local keywords,
! therefore entry 10, 11, ... execution comes here...
! 
if p1(10:10) .eq. "1" @@ verify12,00010
! 
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
! 
!  here the different sub-procedures
!
entry 0001
!
write/out test of CREATE/DISPLAY + CREATE/GRAPHICS
write/out "----------------------------------------"
!
reset/display
create/display 7 512,512,616,300
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
display/lut off
wait/secs 0.1
modify/disp ico
!
entry 0002
!
write/out  test of MODIFY/AREA
write/out "-------------------"
!
if workenv(1:1) .eq. "F" then
   -copy ccd.fits ccdin.fits
else
   indisk/fits ccd.fits ccdin.bdf
endif
create/table ccdintab * * ccdintab ccdintab
if dispyes(1) .eq. 1 then
   create/display 0 550,800,10,55
   clear/channel overlay
   load/image ccdin scale=2
   load/lut rainbow
   draw/rect ccdintab >Null
endif
modify/area ccdin,ccdintab ccdout
if dispyes(1) .eq. 1 then
   create/display 1 550,800,565,55
   load/image ccdout scale=2
   draw/rect ccdintab >Null
   draw/arrow 191.,312.,173.,321. f ? yellow >Null
   label/display "cleaned area" 302.,178.,f ov yellow 1
endif
modify/column ccdout ccdout v @53
if dispyes(1) .eq. 1 then
   load/ima ccdout
   draw/arrow 86.,250.,67.,231. f ? yellow >Null
   label/display "cleaned column" 258.,81.,f ov yellow 1 
endif
!
if aux_mode(1) .eq. 1 then
   -delete ccdi*.{imatype}.*
   -delete ccdint*.{tbltype}.*
else
   -delete ccdi*.{imatype}
   -delete ccdint*.{tbltype}
endif
!
entry 0003
!
write/out  test of VIMOS data files
write/out "-------------------------"
!
delete/temp
define/param p1 vimos.fits c "Enter name of FITS file (with type):"
if dispyes(1) .eq. 1 then
   delete/display 0
   delete/display 1
   modify/display window
endif
! 
set/format i1
!
write/out > get an overview of FITS file {p1}:
write/out > via: info/frame {p1} extens
! 
info/frame {p1} extens
define/local auxi/i/1/1 0
! 
set/midas dscalloc=0,0
secs(1) = m$secs()
display/long
READ/DESCR {p1} 
secs(2) = m$secs()
auxi = auxi+1
myvals({auxi}) = secs(2)-secs(1)
!
write/out > with the default settings for the descr. directory
write/out > READ/DESCR {p1} took {myvals({auxi})} seconds
!
write/out > 
write/out > extension[1] of FITS file {p1} has more than 3000 descriptors
outputi(2) = 1600
outputi(3) = 6000
set/midas dscalloc={outputi(2)},{outputi(3)}
write/out > so let's already allocate space for a big descr. directory
write/out > SET/MIDAS dscalloc={outputi(2)},{outputi(3)}
secs(1) = m$secs()
display/long
READ/DESCR {p1} 
secs(2) = m$secs()
auxi = auxi+1
myvals({auxi}) = secs(2)-secs(1)
write/out > now READ/DESCR {p1} took {myvals({auxi})} seconds
if myvals(1) .gt. 0 then
   outputr = (myvals(1)-myvals({auxi}))/myvals(1)
   outputi = outputr*100
   write/out > which is a {outputi} % saving
endif
write/out
write/out > actually for this command we don't need any ESO.xyz descriptors
write/out > so, let's ignore them via
write/out > SET/MIDAS eso-desc=no
SET/MIDAS eso-desc=no
secs(1) = m$secs()
READ/DESCR {p1} 
secs(2) = m$secs()
auxi = auxi+1
myvals({auxi}) = secs(2)-secs(1)
write/out > now READ/DESCR {p1} took {myvals({auxi})} seconds
SET/MIDAS eso-desc=yes
! 
if workenv(1:1) .eq. "F" return
! 
write/out 
write/out > Convert all extensions of {p1} to internal Midas format
write/out > via: indisk/mfits vimos.fits mvim
indisk/mfits vimos.fits mvim
!
if dispyes(2) .eq. 1 then
   load/image mvim0001 scale=-4 cuts=185,900
endif
! 
write/out
read/descr mvim0001 eso.ins.slit400.x f >middu.dat+term
open/file ./middu.dat read inputi
if inputi(1) .lt. 1 then
   write/out > could not open ./middu.dat ...
   errsum = errsum + 1
   return
endif
read/file {inputi} inputc
write/keyw inputc " " all
read/file {inputi} inputc
close/file {inputi}
inputi = m$index(inputc,"(x co-ordinate")
if inputi .lt. 10 then
   write/out > could not read the help text for descr ESO.INS.SLIT400.X ...
   errsum = errsum + 1
   return
endif
! 
write/out 
write/out > STATISTICS/IMAGE mvim0001
statistics/image mvim0001
write/key rcorr  1.93596E+02,6.09617E+03,2.29753E+02,2.17852E+02
write/key rcorr/r/5/3 1.54960E+01,2.74223E+02,1.93892784E+08
write/key icorr  843920,2,955,533,484
@@ kcompare rcorr outputr 1,4 0.01
@@ kcompare rcorr outputr 5,6 0.3
@@ kcompare rcorr outputr 7,7 0.5
@@ kcompare icorr outputi 1,5
write/out > 
write/out > STATISTICS/IMAGE mvim0002
statistics/image mvim0002
write/key rcorr 209.233,11597.9,3.03998E+02,1.13098E+02
write/key rcorr/r/5/3 1.95013E+01,1.26267E+03,2.56549968E+08
write/key icorr  843920,879,953,521,329
@@ kcompare rcorr outputr 1,4 0.02
@@ kcompare rcorr outputr 5,6 0.3
@@ kcompare rcorr outputr 7,7 0.5
@@ kcompare icorr outputi 1,5
! 
write/out >  
write/out > one can access FITS extensions directly in a Midas command, e.g.:
write/out > STATISTICS/IMAGE {p1}[2]
write/out > should be the same as:
write/out > STATISTICS/IMAGE mvim0002
statistics/image {p1}[2]
write/key rcorr 209.233,11597.9,3.03998E+02,1.13098E+02
write/key rcorr/r/5/3 1.95013E+01,1.26267E+03,2.56549968E+08
write/key icorr  843920,879,953,521,329
@@ kcompare rcorr outputr 1,4 0.02
@@ kcompare rcorr outputr 5,6 0.3
@@ kcompare rcorr outputr 7,7 0.5
@@ kcompare icorr outputi 1,5
! 
write/out >  
write/out > now some number crunching: "COMPUTE/IMAGE vv = mvim0001 + 123.456"
secs(1) = m$secs()
compute/image vv = mvim0001 + 123.456
secs(2) = m$secs()
myvals(1) = secs(2)-secs(1)
write/out > COMPUTE/IMAGE took {myvals(1)} seconds
write/out > "but most of the time was spent copying the more than" -
            3000 descriptors...
write/out > let's turn off the descriptor copying, via: SET/MIDAS dsccopy=no
write/out > COMPUTE/IMAGE vv = mvim0001 + 123.456
set/midas dsccopy=no
secs(1) = m$secs()
compute/image vv = mvim0001 + 123.456
secs(2) = m$secs()
myvals(2) = secs(2)-secs(1)
write/out > now the COMPUTE/IMAGE took {myvals(2)} seconds
set/midas dsccopy=yes			!needed for other verifications
! 
write/out >  
write/out > "now rebuild the Vimos FITS file and add image `vv.bdf' as" -
            third extension 
write/out > OUTDISK/SFITS mvim0000.bdf,mvim0001.bdf,mvim0002.bdf,vv.bdf -
            newVIMOS.fits
! 
-delete newVIMOS.fits
OUTDISK/SFITS mvim0000.bdf,mvim0001.bdf,mvim0002.bdf,vv.bdf newVIMOS.fits
if m$exist("newVIMOS.fits") .ne. 1 then
   write/out > problems with building new FITS file (OUTDISK/SFITS)"
   errsum = errsum + 1
   return
endif
! 
set/midas dscalloc=0,0
write/out
read/key myvals
!
entry 0004
!
write/out  test of CONVERT/DESCR_MATRIX
write/out "-----------------------------"
! 
if workenv(1:1) .eq. "F" return
! 
set/format i1
show/descr mvim0001 >Null
myvals(1) = outputi				!save total
! 
write/out >
write/out > get rid of that ESO.INS.SLIT descriptor matrix via
write/out > -
CONVERT/DESCR_MATRIX mvim0001 slittab ds-t eso.ins.slit type,id,x,y,dimx,dimy 1
CONVERT/DESCR mvim0001 slittab ds-t eso.ins.slit type,id,x,y,dimx,dimy 1
! 
write/out > now, let's time again the COMPUTE statement
secs(1) = m$secs()
compute/image vv = mvim0001 + 123.456
secs(2) = m$secs()
myvals(2) = secs(2)-secs(1)
write/out > COMPUTE/IMAGE took {myvals(2)} seconds
! 
write/out > CONVERT/DESCR mvim0001 slittab t-ds 
CONVERT/DESCR mvim0001 slittab t-ds 
show/descr mvim0001 >Null
if outputi .ne. myvals(1) then
   write/out descr total was {myvals(1)}, is now {outputi}
endif
! 
delete/temp
if aux_mode(1) .eq. 1 then
   -delete mvim0*.{imatype}.*
   -delete vv.bdf.*
else
   -delete mvim0*.{imatype}
   -delete vv.bdf
endif
!
entry 0005
! 
if workenv(1:1) .eq. "F" return
!
select/table slittab seq.lt.30
copy/table slittab middumm
! 
execute/table middumm compute/image [:x] -2 >blabla.dat
execute/table middumm compute/image [:dimy] + 0.1 >blublu.dat
execute/table middumm compute/image [:id] + 0 >bloblo.dat
! 
write/keyword in_a <blabla.dat
write/keyword in_b <blublu.dat
if in_a .eq. in_b then
   write/out > in_a = {in_a}
   write/out > in_b = {in_b}
   write/out > they should be different...
   errsum = errsum + 1
   return
endif
! 
define/local fca/i/1/2 0,0
define/local fcb/i/1/2 0,0
open/file blabla.dat read fca
open/file blublu.dat read fcb 
if fca(1) .lt. 0 .or. fcb(1) .lt. 0 then
   errsum = errsum + 1
   return
endif
read/file {fca} inputc
read/file {fcb} outputc
if fca(2) .lt. 0 .or. fcb(2) .lt. 0 then
   errsum = errsum + 1
   return
endif
if inputc(1:{fca(2)}) .eq. outputc(1:{fca(2)}) then
   errsum = errsum + 1
   return
endif
close/file {fca(1)}
close/file {fcb(1)}
! 
open/file blublu.dat read fca
open/file bloblo.dat read fcb 
if fca(1) .lt. 0 .or. fcb(1) .lt. 0 then
   errsum = errsum + 1
   return
endif
read/file {fca} inputc
read/file {fcb} outputc
if fca(2) .lt. 0 .or. fcb(2) .lt. 0 then
   errsum = errsum + 1
   return
endif
if inputc(1:{fca(2)}) .eq. outputc(1:{fca(2)}) then
   errsum = errsum + 1
   return
endif
close/file {fca(1)}
close/file {fcb(1)}
! 
if aux_mode(1) .eq. 1 then
   -delete slittab.tbl.*
   -delete middumm.tbl.*
else
   -delete slittab.tbl
   -delete middumm.tbl
endif
! 
entry 0006
! 
entry dscfill
define/local klus/i/1/1 0
! 
do klus = 1 10
   write/descr {p1} eso.ins.desc.txt{klus}/c/1/20 "Na bitte, Brigitte "
   write/dhelp {p1} eso.ins.desc.txt{klus} "Just a text string..."
   write/descr {p1} eso.ins.desc.ix{klus}/i/1/2 {klus},{klus}
   write/dhelp {p1} eso.ins.desc.ix{klus} "Two integer values..."
   write/descr {p1} eso.ins.desc.rx{klus}/r/1/2 {klus}.1,{klus}.2
   write/dhelp {p1} eso.ins.desc.rx{klus} "Two real values..."
   write/descr {p1} eso.ins.desc.dx{klus}/d/1/3 {klus}.7,{klus}.8,{klus}.9
   write/dhelp {p1} eso.ins.desc.dx{klus} "Three double values..."
enddo
! 
entry 0007
!
if dispyes(1) .eq. 1 then
   crea/disp 0
   wait/secs 0.2
   dele/disp all
   wait/secs 1			!for slow machines...
   crea/disp 1
endif
! 
entry 0008
!
entry 0009
!
entry 00010
!
write/out  test of creating/deleting commands (via context)
write/out "------------------------------------------------"
!
set/context rbs
start/pipeline
write/keyw klaussi/i/1/1 0
define/local maindir/c/1/120 " " all
maindir = m$symbol("MID_HOME")
if aux_mode .eq. 1 then
   write/keyw veridir/c/1/120 {maindir}.test.prim]
else
   write/keyw veridir/c/1/120 {maindir}/test/prim/
endif
! 
do klaussi = 1 5
   write/out
   write/out >>>>>>> loop no. {klaussi} <<<<<<<<
   write/out
   clear/context -total
   set/context rbs
   set/context verify {veridir}			!removes all local keywords
   !
   set/context cloud 
   write/out "show currently enabled contexts"
   write/out
   show/context
   wait/secs 1
   !
   clear/cont verify 
   clear/CONTEXT cloud
   clear/CONTEXT rbs
enddo
! 
ididev(18) = 11
mid$disp = "I_ImageDisplay "
! 
if dispyes(1) .eq. 1 then
   load tst0010.mt[2] scale=3,2
   load/lut rainbow3 ? d
   load tst0010.mt[2]
   clear/chan 
   load/lut aips0 ? d
   load tst0010.mt[2]
endif
!

