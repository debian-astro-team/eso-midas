! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify7.prg  to verify MIDAS commands
!  K. Banse     930628	creation
!
!  use as @@ verify7 fffffff             with f = 1 or 0 (on/off)
!
!  130503		last modif
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 111111111111 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/12 0000000000
define/local scale/i/1/1 1 ? +lower
define/local seconds/i/1/2 0,0? +lower
!
delete/temp				!get rid of old temporary files
write/key sizez/i/1/5 600,600,0,0,0
write/key dispyes/i/1/2 0,0
! 
write/out +------------------------------------------+
write/out Start of procedure verify7.prg
write/out +------------------------------------------+
!
seconds(1) = m$secs()
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
!
set/midas f_update=??
define/local save_out/c/1/20 {outputc}
set/midas f_update=yes
!
write/key ccc {p1}
set/format i1
do loop = 1 11		
   if ccc({loop}:{loop}) .eq. "1" @@ verify7,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify7.prg!"
      return 1
   endif
enddo
! 
set/midas f_update={save_out}
! 
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verify7.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
!
!  here the different sub-procedures
!
entry 0001
!
write/out test of CREATE/DISPLAY + CREATE/GRAPHICS
write/out "----------------------------------------"
!
reset/display
create/display 0 4,4,0,0 ? no
mid$info(4) = dazdevr(12)-525
mid$info(5) = dazdevr(13)-560
modify/display icon
create/display 7 512,512,{mid$info(4)},{mid$info(5)}
mid$info(4) = mid$info(4) - 608
mid$info(5) = dazdevr(13)-420
create/gra 3 600,400,{mid$info(4)},{mid$info(5)}
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
display/lut off
load/lut rainbow3
!
entry 0002
!
write/out test of CREATE/IMA
write/out "------------------"
!
@@ creamask			!create image arti.bdf
if {arti,npix(1)} .gt. 50 then
   scale = 1
else
   scale = 2
endif
! 
rebin/linear arti &z .25,.25
! 
write/out > resample/image &z &zz half_step
resample/image &z &zz half_step
statist/image &zz p5=wf >Null
write/keyw rcorr 0.0,1.0,2.087141e-01,3.674280e-01,1.40988,3.21806
@@ kcompare rcorr outputr 1,6 0.001
write/keyw rcorr/r/13/14 3.000105e-01,2.087141e-01
@@ kcompare rcorr outputr 13,14 0.001
write/out > filter/smooth &zz &zzz 3,3,0.
filter/smooth &zz &zzz 3,3,0.
write/out > resample/image &zzz &b double_step
resample/image &zzz &x double_step
! 
! filter again on small image
write/out > filter/smooth &z &zz 2,2,0.
filter/smooth &z &zz 2,2,0.
write/out > compute/image &d = &zz-&x 
compute/image &d = &zz-&x 
write/out > statist/image &d >Null
statist/image &d >Null
write/keyw rcorr -9.562504e-02,9.562498e-02,6.882602e-06,1.633776e-02,-1.56138E-02,6.4243
@@ kcompare rcorr outputr 1,6 0.00001
rename/image &zz &b			!that's smoother
compute/image &b = &b * 0.999999	!to avoid rounding errors for different
					!machine architectures
!
if dispyes(1) .eq. 1 then
   load/ima &b.{imatype} scale={scale}
endif
!
entry 0003
!
write/out test of REPLACE/IMA
write/out "-------------------"
!
create/ima &a 2,{&b,npix(1)},{&b,npix(2)} ? poly 0.,0.7
write/out > replace/ima &b veria 0.09,>=&a,0.
replace/ima &b veria 0.09,>=&a,0.
write/keyw icorr/i/15/1  20566
@@ kcompare icorr outputi 15,15
! 
write/out > find/min veria
find/min veria
write/keyw rcorr 0.0,259.0
@@ kcompare rcorr outputr 1,2 0.001
write/keyw icorr 1,1,371,64
@@ kcompare icorr outputi 1,4
! 
write/out > find/pixel veria 0.0,5.9 out all werio 221
find/pixel veria 0.0,5.9 out all werio 221
write/keyw icorr 20236
@@ kcompare icorr outputi 1,1
show/table werio
write/out > copy/dk werio.{tbltype} tblcontr inputi
copy/dk werio.{tbltype} tblcontr inputi
write/key icorr 9,248,3,221,0,0,0,1,15,221
@@ kcompare icorr inputi 1,10
! 
write/out > find/pixel veria 0.0,0.1 out
find/pixel veria 0.0,0.1 out 
rcorr(1) = 4.9
@@ kcompare rcorr outputr 1,1 0.001
write/keyw icorr 8,30
@@ kcompare icorr outputi 1,2
rval = outputr(1)-0.2
!
if dispyes(1) .eq. 1 then
   load/ima veria.{imatype} scale={scale} cuts={rval},{veria,lhcuts(4)}
endif
-delete werio.tbl
!
entry 0004
!
write/out test of APPLY/EDGE
write/out "------------------"
!
write/out > apply/edge veria &e 0.1
apply/edge veria &e 0.1
! 
if dispyes(1) .eq. 1 then
   load/ima &e.{imatype} scale={scale} cuts=0.,1.
endif
!
inputr = 0.2+{veria,lhcuts(4)}
write/out > replace/ima veria &b &e/0.09,>={inputr}
replace/ima veria &b &e/0.09,>={inputr}
! 
find/min &b
if dispyes(1) .eq. 1 then
   load/ima &b.{imatype} scale={scale} cuts={rval},{&b,lhcuts(4)}
endif
!
! save this image for interactive usage
compute/image ia.{imatype} = &b.{imatype}
read/descr ia.{imatype}
! 
entry 0005
!
write/out test of EXTRACT, INSERT, FLIP/IMAGE
write/out "-----------------------------------"
!
write/out > extra/image &f = &b[<,@26:>,@150]
extra/image &f = &b[<,@26:>,@150]
!
crea/ima &t 2,512,256 ? poly 50.,-0.05,-0.1
crea/ima &u 2,512,256 
write/descr &f step 1.,1.		!avoid a warning mesage in `insert'
insert/ima &f &u @67,<
replace/ima &u &s <,0=&t
create/ima &x 2,512,512
create/ima &y 2,512,512
insert/ima &s &x <,@257
insert/ima &s &y <,@257
flip/image &x y
compute/pix veria = &x+&y
write/ima veria <,@256,1024 250. all
!
write/descr veria -
            this_is_a_very_long_long_descriptor_name0001/c/1/8 pipeline
write/dhelp veria this_is_a_very_long_long_descriptor_name0001 -
            "help text for this horrible descriptor"
!
if dispyes(2) .eq. 1 then
   load/ima veria.{imatype} scale={scale} cuts={rval},{&b,lhcuts(4)}
endif
!
entry 0006
write/out test of COPY/...
write/out "----------------"
!
!
write/out "create imageZ via: extra/ima &z = veria[<,@100:@100,@100]"
extract/ima &z = veria[<,@100:@100,@100]
write/out copy: -
"imageZ -> descrZ -> keyZ -> descrY -> imageY -> keyY"
copy/id &z &z zzz
write/key testz/i/1/100 -1 all
copy/dk &z zzz testz
copy/kd testz &z yyy 
copy/dima &z yyy &y
write/key testy/i/1/100 -1 all
copy/ik &y testy
write/out -
"finally, compare the contents of keyZ and keyY (no message, if o.k.)"
@@ kcompare testz testy 1,100
! 
define/local longer_test/c/1/10 x all
copy/dk &z this_is_a_very_long_long_descriptor_name0001/c/2/4 longer_test
if longer_test .ne. "ipelxxxxxx" then
   write/out key `longer_test': "{longer_test} - should be: ipelxxxxxx"
   errsum = errsum+1
endif
!
entry 0007
! 
entry 0008
write/out test of LOAD/IMAGE cube
write/out "-------------------"
!
if dispyes(1) .ne. 1 return
!
define/local xs/i/1/2 0,190
define/local ys/i/1/2 0,190
define/local loop/i/1/1 0
define/local angle/r/1/1 45.0
! 
create/image &a 3,380,380,9
! 
write/out "rotate image in steps of 45 degrees" and store as planes of a cube
xs = -(m$value(&b,npix(1))/2-xs(2))
ys = -(m$value(&b,npix(2))/2-ys(2))
insert/image &b &a @{xs},@{ys},< >Null
insert/image &b &a @{xs},@{ys},> >Null
! 
set/format i1 f4.1
do loop = 2 8
   write/out > rebin/rotate &b &u {angle}
   rebin/rotate &b &u {angle}
   xs = -(m$value(&u,npix(1))/2-xs(2))
   ys = -(m$value(&u,npix(2))/2-ys(2))
   if xs .eq. 0 xs = 1
   write/out > insert/image &u &a @{xs},@{ys},@{loop}
   insert/image &u &a @{xs},@{ys},@{loop} >Null
   angle = angle+45.0
enddo
! 
create/display 7 512,512,616,300
load/lut rainbow4
write/out > load/image &a,all
load/image &a.{imatype},all
! 
entry 0009
write/out test of very long command lines
write/out "-------------------------------"
!
write/out aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb cccccccccccccccccccccccccccccccccccccccccccccccccc ddddddddddddddddddddddddddddddddd eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff ggggggggggggggggggggggggggggggggggggggg 99.9
@@ verify7,testlong aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaazzzzzzzzzz bbbbbbbbbbbbbbbbbbbbbbbbbbbzzzzzzzzzzzzzzzzz ccccccccccccccccccccccccccccccccccccccccczz dddddddddddddddddddddddddddddddddddddddddddddddddddzz eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeezzzzzzzzzzzzzzz fffffffffffffffffffffffffffffffffffffffffffffffzz ggggggggggggggggggggggggggggggggggggggggggzz
write/out mid$errmess(42:43) should be = `gz'
write/out {mid$errmess(42:43)}
! 
entry testlong
define/param p1 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
define/param p2 bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
define/param p3 ccccccccccccccccccccccccccccccccccccccccccc
define/param p4 ddddddddddddddddddddddddddddddddddddddddddddd
define/param p5 eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
define/param p6 ffffffffffffffffffffffffffffffffffffffffffffffffffffff
define/param p7 gggggggggggggggggggggggggggggggggggggggggggggggggggggggggg
define/param p8 zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
write/keyw in_a {p1}
write/keyw in_b {p2}
write/keyw out_a {p3}
write/keyw out_b {p4}
write/keyw inputc {p5}
write/keyw outputc {p6}
write/keyw mid$errmess {p7}
read/keyw in_a,in_b,out_a,out_b,inputc,outputc,mid$errmess
write/out 
!
entry 00010
write/out test of WCS handling via CD matrix
write/out "---------------------------------"
!
if mid$sys(1:5) .ne. "PC/Cy" then
   define/local tmpname/c/1/48 "r.VIMOS.2004-07-13T08:27:31.790_0000.fits "
else
   define/local tmpname/c/1/48 "r.VIMOS.2004-07-13T08c27c31.790_0000.fits "
endif
inputi = m$exist("MID_TEST:{tmpname}")
if inputi .ne. 1 then
  write/out > we need the FITS file `{tmpname}' 
  write/out > from the $MID_TEST directory for this verification
  write/out > please, get the latest demo_data tar file from the ESO ftp server
  write/out > to bring your MID_TEST dir up to date ...
  write/out > test skipped.
  nodemo = nodemo + 1
  $ echo "missing file: {tmpname} in verify7.prg" >> ./missing-files
  return
endif
! 
indisk/fits MID_TEST:{tmpname} vimos2004
read/desc vimos2004
inputd(1) = m$value(vimos2004.bdf,step(2))
read/desc vimos2004 rotang_from_cd-matrix
inputd(2) = m$value(vimos2004.bdf,rotang_from_cd-matrix(1))
write/keyw outputd 0.5694400E-04,1.5707963267500 
@@ kcompare outputd inputd 1,2 0.00001
!
write/out
write/out > wcstest.mt -> middummk.bdf 
write/out > modify descr. step of middummk.bdf 
write/out > middummk.bdf -> lola.fits -> middummz.bdf
write/out > compare step of middummk.bdf and middummz.bdf
define/local mymode/i/1/1 {mid$mode(6)}
define/local dwork/d/1/2 0.,0. ? +lower
! 
mid$mode(6) = 0
@@ verify7,do_it
@@ kcompare rcorr outputd 1,2 0.000001
mid$mode(6) = 1
@@ verify7,do_it			!should give WRONG results
@@ kcompare rcorr dwork 1,2 0.000001
mid$mode(6) = 2
@@ verify7,do_it
@@ kcompare rcorr outputd 1,2 0.000001
! 
mid$mode(6) = mymode			!reset to value before 
! 
entry do_it
set/format i1
write/out 
indisk/fits wcstest.mt middummk
dwork(1) = m$value(middummk,step(1))	!save original step size
dwork(2) = m$value(middummk,step(2))
write/keyw outputd/d/1/2 -0.55250E-03,0.55250E-03
copy/kd outputd/d/1/2 middummk step/d/1/2
write/out modify step from: {dwork(1)},{dwork(2)} to: {outputd(1)},{outputd(2)}
outdisk/fits middummk lola.fits >Null
indisk/fits lola.fits middummz >Null
write/out for MID$MODE(6) = {MID$MODE(6)} we get:
read/descr middummk step
read/descr middummz step
rcorr(1) = m$value(middummz,step(1))
rcorr(2) = m$value(middummz,step(2))
!
entry 00011
write/out test of FIND/PXPOS
write/out "------------------"
!
indisk/fits ccd.fits pxpos
find/pxpos pxpos postab med,5 y		!get 11 pixel positions
if outputi .ne. 11 then
   write/out we should have got 11 pixel positions
   errsum = errsum+1
endif
! sort table in ascending order of pixel positions
sort/table postab :y_pix,:x_pix
copy/ti postab oldx :x_pix
copy/ti postab oldy :y_pix
do inputi = 1 11			!set column :value to 99999.0
   postab,:value,@{inputi} = 99999.0
enddo
copy/ii pxpos ccd
write/image ccd postab,table		!store 99999.0 into the pixel pos.
!
! find back the positions of these 11 max values
find/pxpos ccd newtab max,10 y
if outputi .ne. 11 then
   write/out we should have got 11 pixel positions
   errsum = errsum+1
endif
! sort table in ascending order of pixel positions
sort/table newtab :y_pix,:x_pix
copy/ti newtab newx :x_pix
copy/ti newtab newy :y_pix
! 
! check result positions 
! for images to be equal, we had to sort the tables...
rcorr(1) = 0.0
rcorr(2) = 0.0
compute/image &d = newx - oldx
find/minmax &d
@@ kcompare rcorr outputr 1,2 0.00001
compute/image &d = newy - oldy
find/minmax &d
@@ kcompare rcorr outputr 1,2 0.00001


