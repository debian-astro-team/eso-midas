! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify13.prg  to verify MIDAS commands
!  K. Banse	ESO - Garching		030122
!
!  use as @@ verify13 fffffffffff             with f = 1 or 0 (on/off)
!
! 091029	last modif
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 11111111111111 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local seconds/i/1/2 0,0 ? +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local secs/i/1/2 0,0 ? +lower
define/local myvals/i/1/6 0 all +lower
!
delete/temp                             !get rid of old temporary files
! 
write/key sizez/i/1/5 400,400,0,0,0
write/key dispyes/i/1/2 0,0
!
write/out +------------------------------------------+
write/out Start of procedure verify13.prg
write/out +------------------------------------------+
!
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
! 
seconds(1) = m$secs()
set/format i1
do loop = 1 13
   if p1({loop}:{loop}) .eq. "1" @@ verify13,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify13.prg!"
      return 1
   endif
enddo
! 
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verify13.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
! 
!  here the different sub-procedures
!
entry 0001
!
write/out test of CREATE/GRAPHICS
write/out "-----------------------"
!
reset/display
create/graph 3 950,440,10,480
dispyes(2) = 1
display/lut off
!
entry 0002
!
write/out  test of missing CDELT in FITS header
write/out "-------------------------------------"
!
if workenv(1:1) .eq. "F" then
   -copy nocdelt.fits ctest.fits
else
   indisk/fits nocdelt.fits ctest.bdf >Null
endif
write/out > use 3-dim image ctest.{imatype} which has no CDELTi FITS keywords
read/descr ctest.{imatype}
if m$value(ctest.{imatype},step(1)) .gt. 0.99 then
   write/out > could not compute stepsize correctly...
   errsum = errsum + 1
   return
endif
! 
if dispyes(1) .eq. 1 then
   load/lut rainbow4 ? d
   load/image ctest.{imatype},2 scale=2
   draw/arrow @29,@59,@16,@70 f ? yellow >Null
endif
!
write/out 
write/out > convert/coord ctest.{imatype} @14,@72,@2
convert/coord ctest.{imatype} @14,@72,@2
write/key rcorr 3.5783085,-32.265692,2,0,14,18.79,-32,15,56.49,14,72,2
@@ kcompare rcorr outputd 1,12 0.0001
! 
write/out 
write/out > read/ima  ctest.bdf @14,@72,@2,4
read/ima  ctest.bdf @14,@72,@2,4
write/keyw rcorr 7464.000,6177.000,3283.000,3283.000
@@ kcompare rcorr outputr 1,4 0.001
write/out 
write/out > read/ima  ctest.bdf 0:14:18.79,-32:15:56.49,@2,4
read/ima  ctest.bdf 0:14:18.79,-32:15:56.49,@2,4
@@ kcompare rcorr outputr 1,4 0.0001
write/out 
write/out > data value at pixel ctest[0:14:18.81,-32:15:57.46,@2]:
write/out {ctest[0:14:18.79,-32:15:56.49,@2]}
inputr = {ctest[0:14:18.79,-32:15:56.49,@2]}
@@ kcompare rcorr inputr 1,1 0.0001
!
entry 0003
!
write/out test of precision of written FITS keywords
write/out "------------------------------------------"
!
inputi = m$exist("NACO.fits")
if inputi .eq. 0 return                 !no NACO.fits copied so far
! 
get/fheader NACO.fits
! 
define/local fc/i/1/2 0,0
write/keyw in_b NACO.fhead
open/file {in_b} read fc
if fc(1) .lt. 1 then
   write/out "Could not open file {in_b} ..."
   errsum = errsum+1
   return
endif
!
read_loop:
read/file {fc(1)} inputc
if fc(2) .gt. -1 then			!test for EOF
   if inputc(1:9) .eq. "MJD-OBS =" then
      write/out {inputc(1:35)}
      if inputc(16:31) .ne. " 52680.02879437 " then
         write/out "wrong value for MJD-OBS of NACO.fits ..."
         errsum = errsum + 1
      endif
      close/file {fc(1)}
   else
      goto read_loop
   endif
else
   close/file {fc(1)}
   write/out "Could not find MJD-OBS keyword in {in_b} ..."
   errsum = errsum+1
endif
! 
entry 0004
!
write/out test of pipeline messaging
write/out "--------------------------"
!
inputi(12) = 123
mess/out 3 klaus "this is a test of: \t\t {inputi(12)}"
append/out ulli  
mess/out 2 momo

mess/out 1 noman  "nochmal \x999  \n\tund weiter {inputi(12)}"

mess/out 3 lola "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz"


append/out 
append/out xxxxxxxxxxx
append/out \t
append/out cccccc
mess/out 1 koko " \t klaus"
!
entry 0005
!
write/out test of create/virtual
write/out "----------------------"
!
write/out > create/table bias 20 20 bias.dat bias_dec.fmt
create/table bias 20 20 bias.dat bias_dec.fmt
write/out > create/virtual bvirtual.tbl bias.tbl
create/virtual bvirtual.tbl bias.tbl
write/out > select/table -
            bvirtual.tbl :BINX.eq.1.AND.:BINY.eq.1.AND.:CONAD.LT.1.0
select/table bvirtual.tbl :BINX.eq.1.AND.:BINY.eq.1.AND.:CONAD.LT.1.0
write/out > copy/table bvirtual biassel
copy/table bvirtual biassel
write/out > read/table biassel
read/table biassel
write/out > show/table biassel
show/table biassel
if  outputi(1) .ne. 20 .or. outputi(2) .ne. 7 then
    errsum = errsum + 1
endif
!
entry 0006
!
write/out test of plot/row with different colours
write/out "---------------------------------------"
!
define/local mypix/i/1/1 {wcstest.mt,npix(1)}
indisk/fits wcstest.mt &w >Null
! 
if dispyes(2) .eq. 1 then
   set/gra				!default all graphics settings
   plot/row &w @244 ? ? <,> magenta
   inputi = 2 * mypix
   write/descr &w npix/i/1/1 {inputi}
   plot/row &w @122 ? ? <,> magenta
   inputi = 4 * mypix
   write/descr &w npix/i/1/1 {inputi}
   plot/row &w @61 ? ? <,> magenta
endif
! 
write/out > extract/image &s = wcstest.mt[<,@220:>,@221]	
extra/ima &s = wcstest.mt[<,@220:>,@221]	!emulate spectrum
write/out > extract/image &q = wcstest.mt[<,@121:>,@122]
extra/ima &q = wcstest.mt[<,@121:>,@122]	!emulate quality map
mypix = 2 * mypix
write/descr &s naxis 1
write/descr &s npix/i/1/1 {mypix}
write/descr &s start/d/1/1 17456.23
write/descr &s step/d/1/1 1.234
write/descr &s cunit " " all
write/descr &s ident "simulated spectrum "
read/descr &s
write/descr &q naxis 1
write/descr &q npix/i/1/1 {mypix}
write/descr &q start/d/1/1 0.0
write/descr &q step/d/1/1 1.0
write/descr &q cunit " " all
write/descr &q ident "simulated quality map  "
read/descr &q
! 
if dispyes(2) .eq. 1 then
   plot/row &s ? ? ? 4000,>(&q,<) red,blue
   label/graph "Midas> plot/row &s ? ? ? 4000,>(&q,<) red,blue" -
               0.2,0.2,no ? 1.2 1
   label/graph "plot pixels of &s in red/blue according to row 1 of &q" -
               0.2,0.15,no ? 1. 1
endif
! 
write/out > create/image &x 1,353 ? sequence 1,4
create/imag &x 1,353 ? sequence 1,4
write/out > "plot pixels of &s with colors specified in row 1 of &x" 
! 
if dispyes(2) .eq. 1 then
   plot/row &s ? ? ? c=&x,@1
   label/graph "plot pixels of &s with colors specified in row 1 of &x" -
               0.2,0.15,no ? 1. 1
endif
! 
entry 0007
!
write/out test of scaling images
write/out "----------------------"
! 
write/out > create/image test 2,11,3 ? poly 10.0
create/image test 2,11,3 ? poly 10.0
write/out > write/image test [<,<:@2,>] 0.0 all
write/image test [<,<:@2,>] 0.0 all
write/out > write/image test [>,<:>,>] 4.0 all
write/image test [>,<:>,>] 4.0 all
read/image test <,<,33
write/out > @a scaler test &scal -3,1 a
@a scaler test &scal -3,1 a
read/image &scal
rcorr(1) = m$value(&scal[<,@2])
rcorr(2) = m$value(&scal[>,@3])
write/keyw inputr 3.3333,6.0
@@ kcompare rcorr inputr 1,2 0.0001
! 
write/out > @a scaler test &scal 1,-2
read/image &scal <,<,22
@a scaler test &scal 1,-2
if  {&scal,npix(2)} .ne. 2 .or. {&scal[@11,@2]} .ne. 4.0 then
    errsum = errsum + 1
endif
! 
entry 0008
!
write/out test of division of images with many zeroes
write/out "-------------------------------------------"
!
write/out > comp/ima &a = image_M12c.fits / expo_map_M12c.fits
comp/ima &a = image_M12c.fits / expo_map_M12c.fits
! 
if dispyes(2) .eq. 1 then
   dispyes(1) = 1 
   create/display
   load/lut heat ? d
   load/image middumma.{imatype} cuts=0.0,0.0005
endif
!
write/out > statistics/image &a 
statistics/image &a 
write/keyw rcorr 0,0.00536191,1.22346e-05,4.20911e-05,22.4801,1787.58,3.29553 
@@ kcompare rcorr outputr 1,7 0.001
! 
entry 0009
!
write/out test of working on non-existing files..."
write/out "----------------------------------------"
!
if aux_mode .eq. 1 then
   -delete lola.bdf.*
   -delete lola.tbl.*
else
   -delete lola.bdf
   -delete lola.tbl
endif
!
error(3) = -1                   !do not stop on errors
error(4) = 0
error(7) = 1
echo/on
write/descr lola npix/i/1/2 200,200
read/descr lola npix/i/1/2 200,200
info/desc lola npix
inputr = {lola[<,<]}
write/out {lola.tbl,#1,@1}
write/out {lola.bdf,naxis}
write/descr &a wait 21		!should set error code 85
echo/off
if progstat(1) .ne. 85 then
   errsum = errsum + 1
endif
error(4) = 1
error(7) = 0
progstat(1) = 0		
! 
! execute a successful command to move back up normally 
read/desc wcstest.mt >Null
error(3) = 0			!otherwise we'd stop here...
! 
entry 00010
!
write/out test of fitting images
write/out "----------------------"
!
create/image toto 1,512 0,1 gauss 256,40
! 
! the FIT file was created via: edit/fit test
! 
!    1| poly(X;p0)      RET  
!     p0=0              RET
!    2| gauss(X;a,b,c)  RET
!     a=.01 b=256 c=40  RET
! Ctrl Z
! => enter command: exit
! 
indisk/fits test.ffits test.fit
fit/image 99,.000001,.5 toto test
compute/fit fito
! 
entry 00011
!
write/out  test of context LONG commands
write/out "-----------------------------"
!
if workenv(1:1) .eq. "F" return		!context LONG not ready for FITS yet...
! 
out_a = "Lola"
indisk/fits HeAr_gr2_extr.fits HeAr_gr2_extr
if m$strcmp(out_a(1:4),"HeAr") .ne. 0 then
   errsum = errsum + 1
endif
indisk/fits grism2_test2D.tfits grism2_test2D
indisk/fits grism2_test2DCOE.tfits grism2_test2DCOE
indisk/fits hear.tfits hear
! 
set/cont long
write/out > indisk/fits xspectr.fits spectrum
indisk/fits xspectr.fits spectrum
! 
if dispyes(1) .eq. 1 then
   delete/graph *
   set/graph pmode=0 xaxis=3950,4050 yaxis=200,1000
   plot/row spectrum.bdf
endif
write/out > create/table -
   normtable_smcx1 * * normtable_smcx1_phase004.dat formatnormalisation T
create/table -
   normtable_smcx1 * * normtable_smcx1_phase004.dat formatnormalisation T
if dispyes(1) .eq. 1 then
   write/out > normalize/spec spectrum continuum TABLE normtable_smcx1
   normalize/spec spectrum continuum TABLE normtable_smcx1
   set/graph color=3
   overplot/row continuum
else
   write/out > normalize/spec spectrum continuum TABLE normtable_smcx1 yes
   normalize/spec spectrum continuum TABLE normtable_smcx1 yes
endif
!
write/out > init/long grism2_test2D
init/long grism2_test2D
write/out > set/long npix=1685,286 start=1,1 step=1,1
set/long npix=1685,286 start=1,1 step=1,1
save/long mine
if dispyes(1) .eq. 1 plot/search
write/out > set/long thres=60 ywidth=5 wlcmtd=guess guess=mine
set/long thres=60 ywidth=5 wlcmtd=guess guess=mine
write/out > search/long
search/long
if dispyes(1) .eq. 1 plot/search
write/out > calibrate/long
calibrate/long
write/out > rectify/long HeAr_gr2_extr.bdf longo
rectify/long HeAr_gr2_extr.bdf longo
statist/image longo
if dispyes(1) .eq. 1 load/image longo
write/keyw rcorr 0,66953.9,2939.41,8880.98,5.10382,29.9187
@@ kcompare rcorr outputr 1,4 3.0
@@ kcompare rcorr outputr 5,2 0.001
! 
entry 00012
!
write/out  test of updating FITS files
write/out "---------------------------"
! 
if dispyes(1) .ne. 1 then
   write/out for this test we need display capabilities...
   write/out so we have to skip this test
   write/out and wait instead for 3 seconds...
   wait/secs 3
   return
endif
! 
set/midas f_update=yes
if mid$sys(1:5) .eq. "PC/Cy" then
   define/local tmpname/c/1/40 "VISIR.2004-09-30T03c17c49.095.fits " 
else
   define/local tmpname/c/1/40 "VISIR.2004-09-30T03:17:49.095.fits " 
endif
load/image {tmpname}
! 
statistics/image {tmpname}
write/keyw rcorr -31947.3,32768,-223.398,4021.04  
@@ kcompare rcorr outputr 1,4 0.1
read/ima {tmpname} @20,@256,>,10
write/keyw rcorr/r/1/6 19.92830,21.07043,21.34581,17.28046,19.06599,20.43909
write/keyw rcorr/r/7/4 15.77728,17.36294,17.94353,20.65419
@@ kcompare rcorr outputr 1,10 0.01
set/midas f_update=no
! 
entry 00013
!
write/out  test of copying table to image"
write/out "------------------------------"
! 
set/midas f_update=yes
write/out > compute/table  hear.tfits :new_wave = :wave/2.1
compute/table  hear.tfits :new_wave = :wave/2.1
write/out > copy/ti hear.tfits hear_ima :new_wave
copy/ti hear.tfits hear_ima :new_wave
outputr(1) = m$value("hear.tfits",:new_wave,@150)
rcorr(1) = m$value(hear_ima[@150]) 
write/out > show/table hear.tfits
show/table hear.tfits
outputr(2) = outputi(8)
rcorr(2) = m$value(hear_ima,npix(1))
@@ kcompare rcorr outputr 1,2 0.01
! 
copy/table bias newbie
delete/column newbie #1,#20
write/out > show/table newbie
show/table newbie
write/out > copy/ti newbie newbie_ima
copy/ti newbie newbie_ima
write/out > copy/ti newbie test_ima #11
copy/ti newbie test_ima #11
write/out > read/image test_ima
read/image test_ima
write/out > read/image newbie_ima <,@11
read/image newbie_ima <,@11
write/out > comp/ima &d = newbie_ima@11 - test_ima
comp/ima &d = newbie_ima@11 - test_ima
write/out > find/minmax &d
find/minmax &d
write/keyw rcorr/r/1/2 0.,0.
@@ kcompare rcorr outputr 1,2 0.0001


