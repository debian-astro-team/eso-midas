!+++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure verifyt1.prg to verify MIDAS command 
! SELECT/TABLE
!
! use as @@ verifyt1
! 
! 091029	last modif
!
!++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local seconds/i/1/2 0,0? +lower
! 
write/out +------------------------------------------+
write/out Start of procedure verifyt1.prg
write/out +------------------------------------------+
!
seconds(1) = m$secs()
!
define/local loop/i/1/1 0
define/local res/r/1/1 0
define/local const1/r/1/1 0
define/local diff/r/1/1 0
define/local res1/r/1/8 -
6.93147e-01,9.86123e-02,-2.79192e-02,-1.22613e-01  
write/key res1/r/5/4 -
-2.08241e-01,-2.90158e-01,-3.70048e-01,-4.48527e-01
define/local res2/r/1/8 -
1.48776,1.35090,1.88303,2.43095 
write/key res2/r/5/4 -
2.95778,3.45725,3.92978,4.37742
define/local res3/r/1/8 -
8.0,27.0,64.0,125.0
write/key res3/r/5/4 -
216.0,343.0,512.0,729.0
define/local res4/r/1/8 -
-2.49881e-02,1.36334e+00,1.26375e+00,1.35044e+00
write/key res4/r/5/4 -
2.20412,2.70848,2.31505,2.84867
define/local res5/r/1/8 -
1.12535e-07,1.38879e-11,2.31952e-16,5.24289e-22
write/key res5/r/5/4 -
1.60381e-28,6.63968e-36,3.78351e-44,0.0
define/local res6/r/1/4 -
0.0,0.11254E-06,0.14069E-07, 0.39786E-07
write/out test of CREATE/TAB
write/out "----------------"
create/table verita 3 8 null
create/column verita :x 
create/column verita :y
create/column verita :z 
create/column verita :w C*4
write/out test of COMPUTE/TAB
write/out "-----------------"
compute/table verita :x = seq+1
compute/table verita :y = ln(:x)-sqrt(seq-1.)
do loop = 1 8
    const1 = 'verita,:y,@{loop}'
    diff = const1-res1('loop')
    if m$abs(diff) .gt. 0.00001 goto comp_error
enddo
compute/tab verita :z = (ln(:x+:y*2))**2
do loop = 1 8
    const1 = 'verita,:z,@{loop}'
    diff = const1-res2('loop')
    if m$abs(diff) .gt. 0.00001 goto comp_error
enddo
compute/table verita :y = :x**(2.+1)
do loop = 1 8
    const1 = 'verita,:y,@{loop}'
    diff = const1-res3('loop')
    if m$abs(diff) .gt. 0.00001 goto comp_error
enddo
compute/table verita :z = sin(:x+:y*4*:y)/sqrt(:x)+int(:x)/3.
do loop = 1 8
    const1 = 'verita,:z,@{loop}'
    diff = const1-res4('loop')
    if m$abs(diff) .gt. 0.00001 goto comp_error
enddo
compute/table verita :y = exp(-(:x+2)**2)
do loop = 1 8
    const1 = 'verita,:y,@{loop}'
    diff = const1-res5('loop')
    if m$abs(diff) .gt. 0.00001 goto comp_error
enddo
write/tab verita :w @1 "abcd"
write/tab verita :w @2 "ebcf"
write/tab verita :w @3 "a 78"
write/tab verita :w @4 "AbCd"
write/tab verita :w @5 "ebjk"
write/tab verita :w @6  NULL
write/tab verita :w @7 "ebJk"
write/tab verita :w @8 "  a"
write/out test of SELECT/TAB
write/out "-----------------"
select/tab verita sqrt(:z).le.1.5
if outputi(1) .ne. 4 goto sele_error
select/tab verita :w.eq."a*"
if outputi(1) .ne. 2 goto sele_error
select/tab verita :w.eq."~a[b ]*"
if outputi(1) .ne. 3 goto sele_error
select/tab verita :w.ne."".and.ln(:z).gt.0.5
if outputi(1) .ne. 3 goto sele_error
select/tab verita :w.ne."".and.min(:z,sqrt(:x)).le.2.3
if outputi(1) .ne. 5 goto sele_error
select/table verita mod(:x,:z).le.0.1.or.(:x.ge.6.and.:x.le.8)
if outputi(1) .ne. 4 goto sele_error
select/tab verita all
 write/out test of the NULL values
 write/out "-----------------"
 compute/tab verita :z = ln(:z)
 select/tab verita :z.ne.NULL
 if outputi(1) .ne. 7 goto null_error
 write/tab verita :z @8 0.
 compute/table verita :z = 1/:z
 select/tab verita :z.ne.NULL
 if outputi(1) .ne. 6 goto null_error
write/out test of STAT/TAB
write/out "-----------------"
stat/tab verita :y
do loop = 1 4
    diff = res6('loop') -outputr('loop')
    if m$abs(diff) .gt. 0.00001 goto stat_error
enddo
!
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verifyt1.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
!
return 0
! 
comp_error:
write/out ############Problems with compute/tab############
return 1
sele_error:
write/out ############Problems with select/tab#############
return 1
stat_error:
write/out ############Problems with stat/tab#############
return 1
null_error:
write/out ############Problems with the NULL values######
return 1
