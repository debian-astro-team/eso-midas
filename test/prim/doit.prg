! 
! Midas procedure doit.prg to simply create a graph window and plot
! it is used by the shell script midasparallel via "inmidas -j "
! 
! K. Banse	ESO - Garching
! 070718	created
! 070726	last modif
! 
set/format i1
inputi = {p2}+1
write/out loop no. {inputi} for midasparallel {p1}, Midas unit = -
     {mid$sess(11:12)} >>midasparallel{p1}.log
!

inputi = ({p1}-1)*110
create/graph 0 250,100,0,{inputi(1)}
plot image_M12c.fits @200

inputr = 0.2 * {p1}
wait/secs {inputr}
write/out terminating with Midas unit = -
          {mid$sess(11:12)} >>midasparallel{p1}.log
