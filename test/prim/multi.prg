! 
write/out "time out = {p1} minutes"
! 
define/local mysess/i/1/1 0
define/local myline/i/1/1 100
define/local myscale/i/1/1 1
define/local end/i/1/3 0,0,0
define/local mylut/c/1/20 rainbow
define/local size/i/1/3 200,400,250
define/local colo/c/1/5 red
define/local bcolor/i/1/1 0
define/local odd/i/1/1 0
define/local goffy/i/1/1 0
mysess = {mid$sess(11:12)}
! 
end(2) = {p1}*60
end(1) = m$secs()		!current time in seconds
end(3) = end(1) + end(2)	!real end time
! 
branch mysess 1,2,3,4,5,6,7 m1,m2,m3,m4,m5,m6,m7
! 
mysess = 0			!default for all other values
goto doit
! 
m1:
colo = "green"
mylut = "heat"
bcolor = 0
goto doit
! 
m2:
colo = "white"
mylut = "random"
bcolor = 5
goto doit
! 
m3:
colo = "cyan"
mylut = "staircase"
bcolor = 0
goto doit
! 
m4:
colo = "red"
mylut = "rainbow"
bcolor = 4
goto doit
! 
m5:
colo = "green"
mylut = "heat"
bcolor = 0
goto doit
! 
m6:
colo = "white"
mylut = "random"
bcolor = 6
goto doit
! 
m7:
colo = "cyan"
mylut = "staircase"
bcolor = 0
goto doit
! 
doit:
inputi = mysess/2
odd = mysess - inputi*2
myline = 100 + mysess*10
myscale = mysess + 1
inputi = mysess*size(1) + 40
outputi = mysess*30 + 50
create/display {mysess}  {size(1)},{size(1)},{inputi},{outputi}
! 
if odd .eq. 0 then
   goffy = mysess/2
   inputi = goffy*size(2) + 40
   outputi = 500
   create/graph {mysess} {size(2)},{size(3)},{inputi},{outputi}
   inputi = mysess*5
   set/graph bcolo={bcolor}
endif
! 
repeat:
load/lut {mylut}
load/image wcs scale={mysess}
! 
inputc = m$time()		!random wait 
inputi(2) = {inputc(25:26)}	!using current no. of seconds
inputi(3) = {inputc(25:25)} + 1
inputi = inputi(2)/inputi(3)
if inputi .lt. 1 inputi = 1
if inputi .gt. 11 inputi = 11
write/out Midas-unit {mid$sess(11:12)}, waiting for {inputi} sec
wait/secs {inputi}
! 
if odd .eq. 1 then
   load/itt neg
endif
! 
disp/lut
load/image wcs scale={myscale}
draw/line 0,{size(1)},{size(1)},0 s ? {colo}
wait/sec 1
! 
inputi = mysess 
if inputi .gt. 2 inputi = 1
inputi = inputi + odd
label/display "scale = {myscale}" 128,10 ? {colo} {inputi}
wait/sec 1
!
if odd .eq. 0 then
   plot wcs@{myline}
   wait/secs 2
endif
clear/display 
! 
if mysess .eq. 3 then
   reset/display
   wait/secs 1
   inputi = mysess*size(1) + 40
   outputi = mysess*30 + 50
   create/display {mysess} {size(1)},{size(1)},{inputi},{outputi}
   wait/secs 2
endif

! check, if end time reached
end(1) = m$secs()
if end(1) .lt. end(3) goto repeat
 
