! @(#)works.prg	19.1 (ES0-DMD) 02/25/03 14:34:01
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure works.prg  to provide the tools for the verify procedures
!  K. Banse     920910
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
entry aaa
! 
outputr(1) = {{p1}[@64,@64]}
outputr(2) = {{p1}[@64,@128]}
outputr(3) = {{p1}[@64,@192]}
outputr(4) = {{p1}[@128,@64]}
outputr(5) = {{p1}[@128,@128]}
outputr(6) = {{p1}[@128,@192]}
outputr(7) = {{p1}[@192,@64]}
outputr(8) = {{p1}[@192,@128]}
outputr(9) = {{p1}[@192,@192]}
! 
entry bbb
! 
define/par p1 ? ima "Enter image: "
! 
define/local indx/i/1/1 1
read {p1}    @64,@64,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @64,@128,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @64,@192,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @128,@64,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @128,@128,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @128,@192,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @192,@64,1  
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @192,@128,1 
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @192,@192,1 
inputr({indx}) = outputr(1)
! 
read/key inputr
! 
entry ccc
!
outputr(1) = {{p1}[<]}
outputr(2) = {{p1}[@500]}
outputr(3) = {{p1}[@1000]}
outputr(4) = {{p1}[@1500]}
outputr(5) = {{p1}[@2000]}
outputr(6) = {{p1}[@2500]}
outputr(7) = {{p1}[@3000]}
outputr(8) = {{p1}[@3500]}
outputr(9) = {{p1}[@4000]}
!
entry ddd
!
define/par p1 ? ima "Enter image: "
!
define/local indx/i/1/1 1
read {p1}    <,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @500,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @1000,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @1500,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @2000,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @2500,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @3000,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @3500,1
inputr({indx}) = outputr(1)
indx = indx+1
read {p1}    @4000,1
inputr({indx}) = outputr(1)
!
read/key inputr

