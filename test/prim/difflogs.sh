#! /bin/bash
# @(#)difflogs.sh	19.1 (ESO-DMD) 02/25/03 14:34:02 
# 
# bash shell script `difflogs.sh' to compare the logfiles
# of the verifications of two Midas releases
# ojo: the width of the terminal has to be 80 columns (LOG(10))
# 
# K. Banse		last modif:	030122
# 
# use via $ sh difflogs.sh old logfile new logfile
# 
echo via sed convert $1 to old.log
sed -f $MIDASHOME/$MIDVERS/test/prim/clean.sed $1 >old.log
echo via sed convert $2 to new.log
sed -f $MIDASHOME/$MIDVERS/test/prim/clean.sed $2 >new.log
echo "'diff old.log new.log' yields: " `diff old.log new.log | wc -l` differences
#
diff old.log new.log >./difflogs.txt
#
echo "result of diff command => difflogs.txt"
#
