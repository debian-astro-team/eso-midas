#!/bin/sh
# 
# shell script for running multiple Midas sessions in parallel
# 
# arg1 = root dir (/home/esomidas/tmp)
# arg2 = no. of Midas sessions
# arg3 end time in minutes	(default = 2 minutes)
# 


if [ $# -lt 3 ] ; then
   stop=2			#default to 2 minutes
else
   stop=$3
fi
echo "timeout = $stop minutes"
if [ $# -lt 2 ] ; then
   nsess=1			#default to 1 Midas session
else
   nsess=$2
fi
echo "working with $nsess Midas sessions"
if [ $# -lt 1 ] ; then
   rootdir="/home/esomidas/tmp"
else
   rootdir=$1
fi
echo "working in $rootdir"

cd $rootdir
pwd

loop=0

while [ $loop -lt $nsess ] 
do
   if [ ! -d testdir$loop ] ; then
      mkdir testdir$loop
      echo making dir: testdir$loop
   fi

   cd testdir$loop
   rm -rf *
   cp ../wcs.bdf .

   ~/test/system/unix/inmidas -P 0$loop -j "@@ multi $stop" &

   cd $1
   loop=$(( $loop + 1 ))
done





