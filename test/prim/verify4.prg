! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify4.prg  to verify MIDAS commands
!  K. Banse     ESO - Garching  	920910
! 
!  091029	last modif
!
!  use as @@ verify4 ffffffff		with f = 1 or 0 (on/off)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 1111111111 n "Enter control flags for entries: "
! 
define/local loop/i/1/1 0
define/local test/c/1/1 ? ? +lower
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local seconds/i/1/2 0,0? +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/10 0000000000
! 
delete/temp                             !get rid of old temporary files
! 
write/key sizex/i/1/5 256,0,0,0,0
write/key sizey/i/1/5 256,0,0,0,0
write/key dispyes/i/1/2 0,0
!
write/out +------------------------------------------+
write/out Start of procedure verify4.prg
write/out +------------------------------------------+
!
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
   define/local dcounter/i/1/2 100,23 ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
   define/local dcounter/i/1/2 300,223 ? +lower
endif
! 
seconds(1) = m$secs()
write/key ccc {p1}
set/format i1
do loop = 1 10
   if ccc({loop}:{loop}) .eq. "1" @@ verify4,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify4.prg!"
      return 1
   endif
enddo
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
! 
write/out +------------------------------------------+
write/out procedure verify4.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
! 
!  here the different sub-procedures
! 
entry 0001
! 
write/out test of CREATE/GRAPHICS + CREATE/DISPLAY 
write/out "----------------------------------------"
! 
reset/display
create/gra 3 600,400,0,380
create/display 7 512,512,616,300
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1 
load/lut heat
display/lut
clear/chan overlay
! 
entry 0002
! 
write/out test of CREATE/IMA + INSERT/IMA
write/out "-------------------------------"
! 
create/ima veria 2,{sizex(1)},{sizey(1)}  ? poly 0.0,0.01
! 
sizex(2) = sizex(1)
sizey(2) = sizey(1)/3
rval = {veria[>,<]}			!value of last x-pixel
create/ima &a 2,{sizex(2)},{sizey(2)} ?  poly {rval},-0.01
insert/ima &a veria 0.,{sizey(2)}
! 
sizex(3) = sizex(1)
sizey(3) = sizey(1) - sizey(2) - sizey(2)
create/ima &b 2,{sizex(3)},{sizey(3)} ?  poly 0.,0.,0.01
ival = sizey(2)+sizey(2)
insert/ima &b veria 0.,{ival}
! 
if dispyes(1) .eq. 1 load/ima veria.{imatype}
! 
! put a border around
write/ima veria <,<,{sizex(1)} {rval} all
write/ima veria <,>,{sizex(1)} {rval} all
! 
create/ima &c 2,{sizex(1)},1 ? ? {rval}
write/desc &c npix 1,{sizex(1)}
insert/ima &c veria <,<
insert/ima &c veria >,<
! 
sizex(4) = sizex(1)/4
sizey(4) = sizey(1)/4
write/ima veria @{sizex(4)},@{sizey(4)},1 1.1
sizey(5) = sizey(1)/2
write/ima veria @{sizex(4)},@{sizey(5)},1 1.2
sizey(5) = sizey(4)+sizey(5)
write/ima veria @{sizex(4)},@{sizey(5)},1 1.3
sizex(4) = sizex(1)/2
sizey(4) = sizey(1)/4
write/ima veria @{sizex(4)},@{sizey(4)},1 1.4
sizey(5) = sizey(1)/2
write/ima veria @{sizex(4)},@{sizey(5)},1 1.5
sizey(5) = sizey(4)+sizey(5)
write/ima veria @{sizex(4)},@{sizey(5)},1 1.6
sizex(4) = sizex(1)/2+sizex(1)/4
sizey(4) = sizey(1)/4
write/ima veria @{sizex(4)},@{sizey(4)},1 1.7
sizey(5) = sizey(1)/2
write/ima veria @{sizex(4)},@{sizey(5)},1 1.8
sizey(5) = sizey(4)+sizey(5)
write/ima veria @{sizex(4)},@{sizey(5)},1 1.9
read/descr veria
! 
if dispyes(1) .eq. 1 load/ima veria.{imatype}
! 
entry 0003
! 
write/out test of STATISTICS/IMA
write/out "----------------------"
! 
write/out > stat/ima veria ? #1024 p5=vf
statist/ima veria ? #1024 p5=vf
write/key rcorr 0.,2.55,1.013255,0.754680,5.59989E-01,2.03534,66404.7
! median,1.mode,nobins,binsize,mode,meanabsdev,median
write/key rcorr/r/8/7 -
  7.674355e-01,1.246334e-03,1024.0,2.492669e-03,2.5512,6.529316e-01,6.269e-01
write/key icorr 65536,2,171,1,1
@@ kcompare rcorr outputr 1,6 0.01
@@ kcompare rcorr outputr 7,7 0.5
@@ kcompare rcorr outputr 8,14 0.001
@@ kcompare icorr outputi 1,5
if dispyes(2) .eq. 1 plot/histogram veria 
! 
write/out > stat/ima veria ? #1024 p5=wf
statist/ima veria ? #1024 p5=wf
write/key rcorr 0.,2.55,1.013255,0.754680,5.59989E-01,2.03534,66404.7
write/key rcorr/r/8/7 -
  7.70e-01,1.246334e-03,1024.0,2.492669e-03,2.5512,6.529316e-01,6.268794e-01
write/key icorr 65536,2,171,1,1
@@ kcompare rcorr outputr 1,6 0.01
@@ kcompare rcorr outputr 7,7 0.5
@@ kcompare rcorr outputr 8,14 0.001
@@ kcompare icorr outputi 1,5
! 
write/out > stat/ima veria p5=xf
statist/ima veria p5=xf
@@ kcompare rcorr outputr 8,8 0.0001
! 
ival = sizex(1)/4
ival(2) = 3*ival
write/out > extra/ima &d = veria[@{ival},@{ival}:@{ival(2)},@{ival(2)}]
extra/ima &d = veria[@{ival},@{ival}:@{ival(2)},@{ival(2)}]
! 
write/out > stat/ima &d
statist/ima &d
write/key rcorr 0.,1.92,1.078221,0.556990,-4.83418E-01,2.22721,17942.7
write/key icorr 16641,1,108,1,23
@@ kcompare rcorr outputr 1,6 0.001
@@ kcompare rcorr outputr 7,7 0.5
@@ kcompare icorr outputi 1,5 
!
entry 0004
! 
write/out test of COMPUTE/IMA
write/out "-------------------"
!
write/out > compute/ima &z = (veria + ln(veria) + sin(veria) ) * 0.2
compute/ima &z = (veria + ln(veria) + sin(veria) ) * 0.2
if null .ne. 254 then
   write/out -
   "we should have 254 undefined pixels, but got {null} bad pixels ..."
   write/out
   errsum = errsum+1
endif
! 
write/out > stat/ima &z
statist/ima &z
write/key rcorr -0.918999,0.706117,0.128105,0.356583,-3.01513E-01,2.57453,8395.46
write/key icorr 65536,2,2,1,1
@@ kcompare rcorr outputr 1,6 0.001
@@ kcompare rcorr outputr 7,7 0.5
@@ kcompare icorr outputi 1,5 
! 
if dispyes(1) .eq. 1 load/ima &z.{imatype}
!
entry 0005
!
write/out test of REBIN/ROTATE
write/out "--------------------"
!
write/out > rotate/count &z &y 1
rotate/coun &z &y 1
if dispyes(1) .eq. 1 load/ima &y.{imatype}
write/out > rotate/count &y &x 1
rotate/coun &y &x 1
if dispyes(1) .eq. 1 load/ima &x.{imatype}
write/out > rotate/count &x &w 1
rotate/coun &x &w 1
if dispyes(1) .eq. 1 load/ima &w.{imatype}
write/out > rotate/count &w &v 1
rotate/coun &w &v 1
if dispyes(1) .eq. 1 load/ima &v.{imatype}
@@ imcompare &z &v 0.00001
!
write/out > rebin/rota veria &k 33.3
rebin/rota veria &k 33.3
read/descr &k
if dispyes(1) .eq. 1 load/ima &k.{imatype}
write/key icorr 355,355
copy/dkey &k npix/i/1/2 outputi
@@ kcompare icorr outputi 1,2
write/key rcorr -50.0,-49.0
copy/dkey &k start/d/1/2 outputr
@@ kcompare rcorr outputr 1,2 0.0001
! 
@@ works,aaa &k
write/key rcorr -
0.0,2.5018,0.32126,0.2317,1.9669,1.61555,0.76662,1.11799,1.0806
@@ kcompare rcorr outputr 1,9 0.0001
! 
if dispyes(2) .eq. 1 plot &k @180
! 
entry 0006
! 
write/out test of FILTER/...
write/out "------------------"
!
write/out > filter/smooth veria &f
filter/smooth veria &f
if dispyes(1) .eq. 1 load/ima &f.{imatype}
@@ works,aaa &f
write/key rcorr 0.68222,1.83999,0.331109,1.28444,1.30444,0.3644409
write/key rcorr/r/7/3 1.88666,0.76888,0.39777
@@ kcompare rcorr outputr 1,9 0.0001
write/out > filter/median veria &g
filter/median veria &g
if dispyes(1) .eq. 1 load/ima &g.{imatype}
@@ works,aaa &g
write/key rcorr 0.630,1.920,0.210,1.270,1.280,0.210,1.910,0.640,0.210
@@ kcompare rcorr outputr 1,9 0.0001
write/out > filter/gauss veria &h
filter/gauss veria &h
if dispyes(1) .eq. 1 load/ima &h.{imatype}
@@ works,aaa &h
write/key rcorr -
0.63833,1.90723,0.22933,1.2723,1.28390,0.23465,1.90628,0.66057,0.23997
@@ kcompare rcorr outputr 1,9 0.0001
write/out > filter/digit veria &i laplace
filter/digit veria &i laplace
if dispyes(1) .eq. 1 load/ima &i.{imatype}
@@ works,aaa &i
write/key rcorr 2.98,-1.68,5.66,1.92,2.38,7.16,0.86,6.44,8.66
@@ kcompare rcorr outputr 1,9 0.001
! 
entry 0007
! 
write/out exhaustive test of descriptors
write/out "------------------------------"
!
define/local mm/i/1/2 0,0
define/local tt/i/1/3 0,0,0
define/local fc/i/1/2 0,0
define/local cbuf/c/1/88 " " all
if workenv(1:1) .ne. "F" then
   write/out > intape/fits 1 x nttexample.mt
   intape/fits 1 x nttexample.mt
   outputi(10) = 80
   outputi(11) = 300
else
   set/midas newfile=midas
   intape/fits 1 xx nttexample.mt
   set/midas newfile=fits
   compute/image x0001 = xx0001.bdf
   delete/image xx0001.bdf no
   outputi(10) = 81
   outputi(11) = 100
endif
! 
show/descr x0001 >Null
if outputi(1) .lt. {outputi(10)} then
   write/out "we should have {outputi(10)} descriptors, but got {outputi} ..."
   write/out
   errsum = errsum+1
   return
endif
!
write/keyw cbuf "hypothetical_descriptor_name_ " 
write/out > "we add {dcounter(1)} more descriptors" with help text "-
            to x0001.{imatype}"
! 
tt(1) = m$secs()
open/file middumm.dat WRITE fc
do mm = 1 {outputi(11)}
   mm(2) = -mm(1)
   write/keyw cbuf/c/30/4 {mm}
   write/file {fc(1)} {cbuf}/i/1/2 {mm},{mm(2)}
   write/file {fc(1)} {cbuf}/h/1/52 "this is the help text for {cbuf}"
enddo
close/file {fc(1)}
write/descr x0001 <middumm.dat
! 
tt(2) = m$secs()
tt(3) = tt(2)-tt(1)
set/format i1
write/out > that took {tt(3)} seconds ...
set/format i4
wait/secs 1.0
write/out > read one of those descriptors
write/out > read/descr x0001 hypothetical_descriptor_name_{dcounter(2)} full
read/descr x0001 hypothetical_descriptor_name_{dcounter(2)} full
outputi(2) = m$value(x0001,hypothetical_descriptor_name_{dcounter(2)}(2))
if outputi(2) .ne. -{dcounter(2)} then 
   write/out -
   "this descriptor should have a value of -{dcounter(2)}, but we got {dcounter(2)} ..."
   write/out
   errsum = errsum+1
   return
endif
!
entry 0008
!
write/out more tests of STATISTICS/IMA
write/out "----------------------------"
!
write/out > build frame &c1 "from row #100 and row #200 of x0001.bdf"
define/local npx/i/1/2 0,0
npx(1) = m$value(x0001,npix(1))
npx(2) = m$value(x0001,npix(2))
extract/image &a1 = x0001[<,@100:>,@100]
extract/image &a2 = x0001[<,@200:>,@200]
create/image &c1 2,{npx(1)},2
insert/image &a1 &c1 <,<  >Null
insert/image &a2 &c1 <,@2 >Null
write/out > "and compare its statistics with output from"
statist/image &c1 >Null
copy/kk outputr rcorr
write/out > statist/image x0001 row,100,200 ? ? gfnm 
statist/image x0001 row,100,200 ? ? gfnm 
@@ kcompare outputr rcorr 1,12 0.0001
!
write/out > "now do the same for the sub-windows [@50,@50:@100,@100]," -
[@150,@150:@200,@200]
extract/image &a1 = x0001[@50,@50:@100,@100]
extract/image &a2 = x0001[@150,@150:@200,@200]
create/image &c1 3,51,51,2
insert/image &a1 &c1 <,<,< >Null
insert/image &a2 &c1 <,<,@2 >Null
statist/image &c1 >Null
copy/kk outputr rcorr
write/out > "statist/image x0001 [@50,@50:@100,@100],[@150,@150:@200,@200]" ? ? gfnm 
statist/image x0001 [@50,@50:@100,@100],[@150,@150:@200,@200] ? ? gfnm 
@@ kcompare outputr rcorr 1,12 0.0001
!
transpose/image x0001 &c1 minor
write/out > and finally "the same with columns 10,100,200 and 220"
write/out > statist/image x0001 column,10,100,200,220 ? ? gfnm 
statist/image x0001 column,10,100,200,220 ? ? gfnm 
copy/kk outputr rcorr
statist/image &c1 row,10,100,200,220 ? ? gfnm >Null
@@ kcompare outputr rcorr 1,12 0.0001
! 
entry 0009
!
write/out > compute/image &a = x0001*(0.2*{outputi(2)})
write/out > "and check the descriptors of image &a"
compute/image &a = x0001*(0.2*{outputi(2)})
outputi(2) = m$value(&a,hypothetical_descriptor_name_{dcounter(2)})
if outputi(2) .ne. {dcounter(2)} then 
   read/descr &a hypothetical_descriptor_name_{dcounter(2)} full
   errsum = errsum+1
   return
endif
! 
if workenv(1:1) .eq. "M" then
   write/out > "and test the loop outdisk/fits -> indisk/fits"
   write/descr &a short223/i/1/1 0
   copy/dd &a hypothetical_descriptor_name_{dcounter(2)}/i/2/1 &a short223
   outdisk/fits middumma x0001.fits
   indisk/fits x0001.fits x0002.bdf
   outputi(2) = m$value(x0002,short223(1))
   if outputi(2) .ne. -{dcounter(2)} then 
      write/out -
      "this descriptor should have a value of -{dcounter(2)}," -
      "but we got {outputi(2)} ..."
      write/out
      errsum = errsum+1
      return
   endif
endif
! 
write/out > "delete all the x000* files"
if aux_mode(1) .lt. 2 then			!VMS
   -delete x0001.bdf.*
   -delete x0001.fits.*
   -delete x0002.bdf.*
else						!Unix
   -delete x0001.bdf
   -delete x0001.fits
   -delete x0002.bdf
endif
! 
entry 00010
! 
write/out > Create table from ascii file "foil.asc"
create/table foiltab 3 1000 foil.asc
write/out > Compute 3rd column with wavelength in `mu' from 1st column
write/out > compute/table foiltab :LAB003 = (1.0 / :LAB001) * 1.e+04
compute/table foiltab :LAB003 = (1.0 / :LAB001) * 1.e+04
! 
if dispyes(1) .eq. 1 then
   delete/graph
   create/graph 0 1000,400
   set/graph stype=1 ltype=1 yaxis=-20,100
   plot/table foiltab :LAB003 :LAB002
endif
! 
define/local mystep/r/1/1 0.005
define/local n_entries/i/1/1  0
write/out statistics/table foiltab #3
statistics/table foiltab #3
n_entries = outputi(2)
! 
inputr(1) = m$value(foiltab,:LAB003,@1})   !get start value in 'mu'
inputr(2) = m$value(foiltab,:LAB003,@{n_entries}}) !get last value in 'mu'
! 
write/out > start, endpoint of spectrum = {inputr(1)},{inputr(2)}
! 
inputr(3) = (inputr(2)-inputr(1)) / mystep + 1  !get nopix for refima
inputi = inputr(3)
! 
write/out > create reference image for command INTERPOLATE/TI
write/out > create/image refi 1,{inputi(1)} {inputr(1)},{mystep}
create/image refi 1,{inputi(1)} {inputr(1)},{mystep}
! 
write/out > Rebin data by 5.10-3 mu steps
write/out > interpolate/ti rfoilspec foiltab :LAB003,:LAB002 refi
interpolate/ti rfoilspec foiltab :LAB003,:LAB002 refi
! 
if dispyes(1) .eq. 1 plot/row rfoilspec 


