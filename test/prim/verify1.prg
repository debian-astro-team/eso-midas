! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify1.prg  to verify MIDAS commands
!  K. Banse     931104, 970521, 981008, 990729
!
! 091029		last modif
! 
!  use as @@ verify1 ffffffff		with f = 1 or 0 (on/off)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 11111111 n "Enter control flags for entries: "
! 
define/local loop/i/1/1 0
define/local test/c/1/1 ? ? +lower
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local cfunc/c/1/16 " " all +lower
define/local seconds/i/1/2 0,0? +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/8 00000000
define/local aname/c/1/80 " " all +lower
define/local bname/c/1/80 " " all +lower
define/local cname/c/1/80 " " all +lower
! 
delete/temp                             !get rid of old temporary files
! 
write/out +------------------------------------------+
write/out start of procedure verify1.prg
write/out +------------------------------------------+
! 
write/key dispyes/i/1/2 0,0
!
seconds(1) = m$secs()
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
!
if aux_mode(1) .eq. 1 then				!VMS
   write/keyw aname veria_s
   write/keyw bname veria_v
   write/keyw cname for
else							!Unix
   write/keyw aname veria_somewhat_longer_file
   write/keyw bname veria_very_very_much_longer_than_usual_file
   write/keyw cname for_all_the_weri_images
endif
! 
write/key ccc {p1}
set/format i1
do loop = 1 8
   if ccc({loop}:{loop}) .eq. "1" @@ verify1,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify1.prg!"
      return 1
   endif
enddo
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
! 
write/out +------------------------------------------+
write/out procedure verify1.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
! 
!  here the different sub-procedures
! 
entry 0001
! 
write/out test of CREATE/ICAT
write/out "-------------------"
! 
define/local ojo/i/1/1 0
in_a = m$upper(mid$sys(1:20))
inputi = m$index(in_a,"VMS")
if aux_mode(1) .eq. 1 then				!VMS
   if inputi .lt. 1 then
      ojo = 1
      in_a = "AUX_MODE(1) = 1 indicates a VMS system"
   endif
else
   if inputi .ge. 1 then
      ojo = 1
      in_a = "AUX_MODE(1) != 1 indicates a Unix/Linux system"
   endif
endif
if ojo .eq. 1 then
   write/out Ojo: "Mismatch between keywords AUX_MODE and MID$SYS ...!"
   write/out "    " {in_a(1:>)}
   write/out "    " however, MID$SYS says: {mid$sys(1:20)}
   write/out "    " to fix that problem
   write/out "    " "execute the `setup' option in the Midas configuration"-
             script
   errsum = errsum + 1
   return
endif
! 
clear/icat
delete/image veria* noconf
! 
write/out First, we create 4 images:
write/out {aname}_name_1, {aname}_name_2
write/out {aname}_name_3,
write/out {bname}_name_4
create/ima {aname}_name_1 2,256,256 ? radius_law 200,40,15,70
write/descr {aname}_name_1.{imatype} -
            this_is_a_very_long_long_descriptor_name0001/c/1/8 pipeline
write/dhelp {aname}_name_1.{imatype} -
            this_is_a_very_long_long_descriptor_name0001 -
            "help text for this horrible descriptor"
create/ima {aname}_name_2 2,256,256 ? radius_law 200,40,25,70
write/descr {aname}_name_2.{imatype} -
            this_is_a_very_long_long_descriptor_name0002/c/1/8 pipeline
write/dhelp {aname}_name_2.{imatype} -
            this_is_a_very_long_long_descriptor_name0002 -
            "help text for this horrible descriptor"
create/ima {aname}_name_3 2,256,256 ? radius_law 200,40,35,70
write/descr {aname}_name_3.{imatype} -
            this_is_a_very_long_long_descriptor_name0003/c/1/8 pipeline
write/dhelp {aname}_name_3.{imatype} -
            this_is_a_very_long_long_descriptor_name0003 -
            "help text for this horrible descriptor"
create/ima {bname}_name_4 -
           2,256,256 ? radius_law 200,40,45,70
write/descr {bname}_name_4.{imatype} -
            this_is_a_very_long_long_descriptor_name0004/c/1/8 pipeline
write/dhelp {bname}_name_4.{imatype} -
            this_is_a_very_long_long_descriptor_name0004 -
            "help text for this horrible descriptor"
write/out "Now we create an image catalog `vericat.cat' for them via:"
write/out create/icat vericat veria*.{imatype}
create/icat vericat veria*.{imatype}
write/out And display the contents fo the catalog via:
write/out read/icat vericat
read/icat vericat
! 
delete/ima weria*.{imatype} noconf
write/out "Now we create 4 more images weria1,2,3,4"
modify/column {aname}_name_1.{imatype} weria1 v @101 >Null
modify/column {aname}_name_2.{imatype} weria2 v @101 >Null
modify/column {aname}_name_3.{imatype} weria3 v @101 >Null
modify/column {bname}_name_4.{imatype} weria4 v @101 >Null
write/out And a catalog `{cname}_catalog.cat' for them
create/icat {cname}_catalog weria*.{imatype}
read/icat {cname}_catalog
-copy {cname}_catalog.cat weriax.cat
cfunc = "use entry #"
read/descr #3_weriax.cat

! 
entry 0002
! 
write/out test of ADD, SUBTRACT, SEARCH and SORT/ICAT
write/out "-------------------------------------------"
! 
cfunc = "ADD/ICAT "
write/out Add all entries in {cname}_catalog.cat to vericat.cat via:
write/out add/icat vericat {cname}_catalog.cat
add/icat vericat {cname}_catalog.cat
show/icat vericat >Null
if outputi .ne. 8 @@ verify1,error
! 
read/icat vericat
cfunc = "SUBTR/ICAT "
write/out "Subtract the entry for image `{aname}_name_2.{imatype} from vericat.cat via:"
write/out subtract/icat vericat {aname}_name_2.{imatype}
subtract/icat vericat {aname}_name_2.{imatype}
show/icat vericat >Null
if outputi .ne. 7 @@ verify1,error
! 
read/icat vericat
write/out "And subtract all entries for images with a `3' in their names via:"
write/out subtract/icat vericat  *3*
subtract/icat vericat  *3*
show/icat vericat >Null
if outputi .ne. 5 @@ verify1,error
! 
read/icat vericat
write/out Finally, subtract entry no. 8 via
write/out subtract/icat vericat  #8
subtract/icat vericat  #8
show/icat vericat >Null
if outputi .ne. 4 @@ verify1,error
! 
read/icat vericat
write/out -
"We change the IDENT descr. of image weria2.{imatype} and update the catalog."
write/desc weria2.{imatype} ident " " all
write/desc weria2.{imatype} ident "Test of search"
add/icat vericat weria2.{imatype} >Null
! 
read/icat vericat
write/out And search for the image via the command:
write/out search/icat vericat Test
search/icat vericat Test
if out_a .ne. "weria2.{imatype}" @@ verify1,error
write/out "Now we sort the catalog according to the IDENT field via:"
write/out sort/icat vericat
sort/icat vericat
! 
read/icat vericat
write/out "Now we make the catalog the `active' image catalog via:"
write/out set/icat vericat
set/icat vericat
write/out Now we create a new image via:
write/out create/image straightx 1,9000 ? sequence -360,360
create/image straightx 1,9000 ? sequence -360,360
write/out and another one via:
write/out compute/image diff = -
{bname}_name_4.{imatype} - {aname}_name_2.{imatype}
compute/image diff = -
{bname}_name_4.{imatype} - {aname}_name_2.{imatype}
write/out "These frames should be added automatically to the catalog"
cfunc = "SET/ICAT "
show/icat vericat >Null
if outputi .ne. 6 @@ verify1,error
! 
write/out 
read/icat vericat
write/out "We will compute a new image by subtracting two existing images"
write/out "these images will be accessed by sequence numbers and by their names"
set/icat vericat
write/out The new image middumma is given by 
write/out compute/image &a = #4 - #3
write/out and will be compared to middummb
write/out compute/image &b = weria2.{imatype} - weria1.{imatype}
compute/image &a = #4 - #3
compute/image &b = weria2.{imatype} - weria1.{imatype}
write/out Now subtract these images
write/out compute/image &c = middumma.{imatype} - middummb.{imatype}
compute/image &c = middumma.{imatype} - middummb.{imatype}
statistic/image &c
if outputr(4) .ne. 0 @@ verify1,error
write/out 
!
read/icat vericat
write/out Finally, we `deactivate' the catalog via:
write/out clear/icat vericat
clear/icat vericat
! 
! finally loop many times to catch open files
define/local count/i/1/2 150,0
create/image klaus 1,100
! 
write/out test of add/icat  in a long ({count(1)}) loop ...
do count(2) = 1 count(1)
   ! write/out "loop no." {count(2)}
   crea/icat aaa NULL >Null
   crea/icat bbb kla*.{imatype} >Null
   open/file TEST w inputi
   close/file {inputi(1)}
   add/icat aaa bbb.cat >Null
   open/file TEST w inputi
   close/file {inputi(1)}
enddo
if count(2) .ne. count(1) then
   @@ verify1,error
endif
! 
entry 0003
! 
write/out test of AVERAGE/IMAGE
write/out "----------------------"
! 
write/out avera/ima weriares1 = {cname}_catalog.cat
avera/ima weriares1 = {cname}_catalog.cat
statist/ima weriares1 >Null
write/key rcorr 3.24023E-03,2.07653E+01,1.00440E-01,3.17890E-01 
write/key rcorr/r/5/3 2.13603E+01,8.71274E+02,6.58243E+03 
write/key icorr 65536,0256,0001,0128,0128 
@@ kcompare rcorr outputr 1,6 0.01
@@ kcompare rcorr outputr 7,7 0.1
@@ kcompare icorr outputi 1,5
avera/ima weriares2 = weria1,weria2,weria3,weria4
compute/pix &a = weriares1-weriares2
statist/ima &a >Null
write/key rcorr 0.,0.
@@ kcompare rcorr outputr 1,2 0.0001
write/out avera/ima weriares2 = {cname}_catalog.cat ? ? min
avera/ima weriares2 = {cname}_catalog.cat ? ? min
statist/ima weriares2 >Null
write/key rcorr 2.81103E-03,1.74128E+01,7.06025E-02,2.57701E-01 
write/key rcorr/r/5/3 2.41263E+01,1.05318E+03,4.62701E+03 
write/key icorr 65536,0256,0001,0128,0128 
@@ kcompare rcorr outputr 1,6 0.01
@@ kcompare rcorr outputr 7,7 0.1
@@ kcompare icorr outputi 1,5
write/out avera/ima weriares3 = {cname}_catalog.cat ? ? max
avera/ima weriares3 = {cname}_catalog.cat ? ? max
statist/ima weriares3 >Null
write/key rcorr 3.96110E-03,2.35756E+01,1.33625E-01,3.81238E-01 
write/key rcorr/r/5/3 1.88921E+01,7.02512E+02,8.75722E+03 
write/key icorr 65536,0256,0001,0128,0128 
@@ kcompare rcorr outputr 1,6 0.01
@@ kcompare rcorr outputr 7,7 0.1
@@ kcompare icorr outputi 1,5
write/out avera/ima weriares4 = {cname}_catalog.cat ? ? median
avera/ima weriares4 = {cname}_catalog.cat ? ? median
stat/ima weriares4 >Null
write/key rcorr 2.91881E-03,1.97706E+01,8.78078E-02,2.96087E-01 
write/key rcorr/r/5/3 2.25164E+01,9.52958E+02,5.75457E+03 
write/key icorr 65536,0256,0001,0128,0128 
@@ kcompare rcorr outputr 1,6 0.01
@@ kcompare rcorr outputr 7,7 0.1
@@ kcompare icorr outputi 1,5
! 
entry 0004
! 
write/out test of COMPUTE/WEIGHTS
write/out "-----------------------"
!
write/out compute/weights {cname}_catalog.cat @140,@100,@178,@155
compute/weights {cname}_catalog.cat @140,@100,@178,@155
write/key outputr {weria2,weight}
write/key rcorr 0.9491076
@@ kcompare rcorr outputr 1,1 0.0001
rcorr(1) = {weria2,weight}
compute/weights weria1,weria2,weria3,weria4 @140,@100,@178,@155 >Null
outputr(1) = {weria2,weight}
@@ kcompare rcorr outputr 1,1 0.0001
! 
entry 0005
!
write/out test of file access within the monitor
write/out "--------------------------------------"
! 
cfunc = "M$EXISTD()"
write/descr weria1 remarks/c*68/1/3 KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK all
copy/ii weria4 weria5
write/descr weria5 kuki/c/1/4 LOLE
set/format i1
! 
do inputi(10) = 1 4
   inputi = m$existd("weria{inputi(10)}","kuki")
   if inputi .ne. 0 then
      write/out "we got {inputi} for" m$existd("weria{inputi(10)}","kuki")
      @@ verify1,error
   endif
enddo
inputi = m$existd("weria5","kuki")
if inputi .ne. 1 then
   write/out "test1 for" m$existd("weria5,"kuki")
   @@ verify1,error
endif
! 
cfunc = "use of FRAMACC"
write/descr weria1 kuki/c/1/4 LOLA
write/descr weria2 kuki/c/1/4 LOLB
write/descr weria3 kuki/c/1/4 LOLC
write/descr weria4 kuki/c/1/4 LOLD
define/local tchar/c/1/5 ABCDE
inputi = 99
do inputi(10) = 1 5
   inputi = m$existd("weria{inputi(10)}","kuki")
   if inputi .ne. 1 then
      write/out test2 for m$existd("weria{inputi(10)}","kuki")
      @@ verify1,error
   endif
   inputc(1:1) = "{weria{inputi(10)},kuki(4:4)}"
   if inputc(1:1) .ne. tchar({inputi(10)}:{inputi(10)}) then
      write/out "test3 for weria{inputi(10)}
      @@ verify1,error
   endif
enddo
!
do inputi(10) = 1 5
   if workenv(1:1) .eq. "M" then
      weria{inputi(10)},naxis = 1
      copy/it weria{inputi(10)} veria{inputi(10)}.{tbltype}
      weria{inputi(10)},naxis = 2
   else
      extract/image wweria = weria{inputi(10)}[<,<:>,<]
      copy/it wweria veria{inputi(10)}.{tbltype}
   endif
   !
   inputi = m$existd("veria{inputi(10)}.{tbltype}","kuki")
   if inputi .ne. 0 then
      write/out test4 for m$existd("veria{inputi(10)}.{tbltype}","kuki")
      @@ verify1,error
   endif
enddo
! 
write/descr veria1.{tbltype} kuki/c/1/4 LOLA
write/descr veria2.{tbltype} kuki/c/1/5 abcBd
write/descr veria3.{tbltype} kuki/c/1/6 LOLCxx
write/descr veria4.{tbltype} kuki/c/1/4 LOLD
write/descr veria5.{tbltype} kuki/c/1/10 "LOLE "
! 
do inputi(10) = 1 5
   inputc(1:1) = "{veria{inputi(10)}.{tbltype},kuki(4:4)}"
   if inputc(1:1) .ne. tchar({inputi(10)}:{inputi(10)}) then
      write/out "test5 for veria{inputi(10)}.{tbltype}
      @@ verify1,error
   endif
enddo
!
write/descr weria1 kiki/i/1/4 1,2,3,4
write/descr weria2 kiki/i/1/4 1,2,3,4
write/descr weria3 kiki/i/1/4 1,2,3,4
write/descr weria4 kiki/i/1/4 1,2,3,4
write/descr weria5 kiki/i/1/4 1,2,3,4
write/descr veria1.{tbltype} kiki/i/1/4 11,12,13,14
write/descr veria2.{tbltype} kiki/i/1/5 11,12,13,14,15
write/descr veria3.{tbltype} kiki/i/1/6 11,12,13,14,15,16
write/descr veria4.{tbltype} kiki/i/1/4 11,12,13,14
write/descr veria5.{tbltype} kiki/i/1/10 11,12,13,14,0,0,0,0,0,0
! 
do inputi(10) = 1 5
   inputi(1) = m$value(weria{inputi(10)},kiki(4))
   if inputi(1) .ne. 4 then
      write/out "test6 for weria{inputi(10)}
      @@ verify1,error
   endif
   inputi(1) = m$value(veria{inputi(10)}.{tbltype},kiki(4))
   if inputi(1) .ne. 14 then
      write/out "test7 for veria{inputi(10)}.{tbltype}
      @@ verify1,error
   endif
   weria{inputi(10)}[@10,@10] = 99.9
   veria{inputi(10)}.{tbltype},#1,@256 = 88.8
enddo
! 
   read/image weria{inputi(10)} @10,@10,1 
do inputi(10) = 1 5
   rcorr = 99.9
   read/image weria{inputi(10)} @10,@10,1 h | write/keyw outputr
   @@ kcompare rcorr outputr 1,1 0.0001
   rcorr = 88.8
   copy/tk veria{inputi(10)} #1 @256 outputr
   @@ kcompare rcorr outputr 1,1 0.0001
enddo
! 
entry 0006
!
write/out test of DELETE/ICAT
write/out "-------------------"
!
write/out "Delete all frames with entries in catalog vericat.cat via:"
write/out delete/icat vericat noconf
delete/icat vericat noconf
if aux_mode(1) .lt. 2 then			!VMS
   -delete vericat.cat.*
   -delete {cname}_catalog.cat.*
else						!Unix
   -delete vericat.cat
   -delete {cname}_catalog.cat
endif
delete/ima weria* noconf
delete/ima veria* noconf
delete/tab veria* noconf
! 
entry 0007
!
write/out 
write/out ********* redo catalog creation + FRAMACC tests *********
write/out 
!
@@ verify1,0001
@@ verify1,0005
@@ verify1,0006
@@ verify1,0001
@@ verify1,0005
!
! 
entry 0008
!
write/out do @@ verify1,0006
write/out "-------------------"
!
@@ verify1,0006
!
entry error
! ---------
! 
write/out ######## problems with {cfunc} ########
errsum = errsum+1
return/exit
