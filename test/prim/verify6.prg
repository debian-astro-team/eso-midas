! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify6.prg  to verify MIDAS commands
!  K. Banse     930331, 931206, 971015, 011211, 031017, 091029
!
!  use as @@ verify6 ffffffffff             with f = 1 or 0 (on/off)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 1111111111 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local seconds/i/1/2 0,0 ? +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/10 0000000000
!
delete/temp                             !get rid of old temporary files
! 
write/key sizez/i/1/5 600,600,0,0,0
write/key dispyes/i/1/2 0,0
!
write/out +------------------------------------------+
write/out Start of procedure verify6.prg
write/out +------------------------------------------+
!
seconds(1) = m$secs()
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
! 
write/key ccc {p1}
set/format i1
do loop = 1 10
   if ccc({loop}:{loop}) .eq. "1" @@ verify6,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify6.prg!"
      return 1
   endif
enddo
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verify6.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
!
!  here the different sub-procedures
!
entry 0001
!
write/out test of CREATE/DISPLAY + CREATE/GRAPHICS
write/out "----------------------------------------"
!
create/display 7 512,512,616,300
create/gra 3 600,400,0,380
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
display/lut off
load/lut smooth
!
entry 0002
!
write/out test of CREATE/IMA
write/out "------------------"
!
crea/ima &a 2,7,7
&a[<,<] = 1.
&a[@7,<] = 1.
&a[<,@2] = 1.
&a[@7,@2] = 1.
&a[<,@3] = 1.
&a[@7,@3] = 1.
&a[<,@4] = 1.
&a[@4,@4] = 1.
&a[@7,@4] = 1.
&a[<,@5] = 1.
&a[@3,@5] = 1.
&a[@5,@5] = 1.
&a[@7,@5] = 1.
&a[<,@6] = 1.
&a[@2,@6] = 1.
&a[@6,@6] = 1.
&a[@7,@6] = 1.
&a[<,@7] = 1.
&a[@7,@7] = 1.
crea/ima &b 2,9,9
insert/ima &a &b @2,@2
rebin/linear &b &z .22,.22
@a scaler &z &a 10
filter/smooth &a veria 2,2,0.
!
if dispyes(1) .eq. 1 then
   load/ima veria.{imatype}
endif
!
entry 0003
!
write/out test of REPLACE/IMAGE
write/out "---------------------"
!
write/out > replace/ima veria &r 0.5,>=veria+0.5
replace/ima veria &r 0.5,>=veria+0.5
inputi = 35269
if outputi(15) .ne. inputi then
   set/format i1
   write/out -
   "we should have {inputi} pixels replaced, but got {outputi(15)} pixels ..."
   write/out
   errsum = errsum+1
endif
!
statistics/image &r
write/key rcorr 0.0,1.4987,0.337582,0.489107
write/key rcorr/r/5/3 1.24434,2.91109,56747.4
write/key icorr 168100,1,1,333,293
@@ kcompare rcorr outputr 1,4 0.01
@@ kcompare rcorr outputr 5,6 0.3
@@ kcompare rcorr outputr 7,7 0.3
@@ kcompare icorr outputi 1,5
!
if dispyes(1) .eq. 1 then
   set/graph colo=4
   load/ima &r.{imatype}
   plot/histogram &r
endif
!
write/out > create/image &d 2,410,410 -205,-205,1,1 ? 0.,0.,0.,0.,0.01,0.01
create/image &d 2,410,410 -205,-205,1,1 ? 0.,0.,0.,0.,0.1,0.1
write/out > replace/image &d &p &r/0.2,1.01=veria
replace/image &d &p &r/0.2,1.01=veria
if dispyes(1) .eq. 1 load/image &p.{imatype}
write/out > replace/image &d &q veria/0.2,1.01=&p*1.3
replace/image &d &q veria/0.2,1.01=&p*1.3
if dispyes(1) .eq. 1 load/image &q
write/out > replace/image &d &s 2000,3800=&r*8000
replace/image &d &s 2000,3800=&r*8000
if dispyes(1) .eq. 1 load/image &s.{imatype}
! 
entry 0004
!
if dispyes(1) .ne. 1 return
! 
write/out test of DRAW/xyz
write/out "----------------"
!
clear/channel overlay
!
define/local fct/i/1/2 0,0
open/file draw.dat write fct
if fct(1) .lt. 0 then
   write/out could not create file draw.dat
   return
endif
! 
write/out draw lines around the image + diagonals
!
write/file {fct(1)} -
draw/line {idimemr(1)},{idimemr(2)},{idimemr(3)},{idimemr(2)} F
write/file {fct(1)} -
draw/line {idimemr(1)},{idimemr(2)},{idimemr(1)},{idimemr(4)} F
write/file {fct(1)} -
draw/line {idimemr(1)},{idimemr(4)},{idimemr(3)},{idimemr(4)} F
write/file {fct(1)} -
draw/line {idimemr(3)},{idimemr(4)},{idimemr(3)},{idimemr(2)} F
write/file {fct(1)} -
draw/line {idimemr(1)},{idimemr(2)},{idimemr(3)},{idimemr(4)} F
write/file {fct(1)} -
draw/line {idimemr(1)},{idimemr(4)},{idimemr(3)},{idimemr(2)} F
! 
write/out draw circles 
!
rcorr(1) = (idimemr(1)+idimemr(3))*0.5
rcorr(2) = (idimemr(2)+idimemr(4))*0.5
write/file {fct(1)} -
draw/circle {rcorr(1)},{rcorr(2)},25 F 
write/file {fct(1)} -
draw/circle {rcorr(1)},{rcorr(2)},35 F 
write/file {fct(1)} -
draw/circle {rcorr(1)},{rcorr(2)},45 F 
write/file {fct(1)} -
draw/circle {rcorr(1)},{rcorr(2)},55 F 
write/file {fct(1)} -
draw/circle {rcorr(1)},{rcorr(2)},65 F 
! 
write/out draw rectangles 
! 
rcorr(3) = idimemr(3)-50.
rcorr(4) = idimemr(4)-50
rcorr(5) = idimemr(1)+50.
rcorr(6) = idimemr(2)+50.
write/file {fct(1)} -
draw/rect {rcorr(1)},{rcorr(2)},{rcorr(3)},{rcorr(4)} F ? white,45. 
write/file {fct(1)} -
draw/rect {rcorr(1)},{rcorr(6)},{rcorr(3)},{rcorr(2)} F ? white,45.
! 
write/file {fct(1)} -
draw/rect {rcorr(5)},{rcorr(6)},{rcorr(1)},{rcorr(2)} F ? white,45. 
write/file {fct(1)} -
draw/rect {rcorr(5)},{rcorr(1)},{rcorr(1)},{rcorr(3)} F ? white,45. 
! 
close/file {fct(1)}
set/midas output=no
draw/any draw.dat
set/midas output=yes
! 
@a getcoord &s {rcorr(1)},{rcorr(2)} 
icorr(1) = outputi(1)
icorr(2) = outputi(2)
@a getcoord &s {rcorr(3)},{rcorr(4)} 
icorr(3) = outputi(1)
icorr(4) = outputi(2)
@a getcoord &s {rcorr(5)},{rcorr(6)} 
icorr(5) = outputi(1)
icorr(6) = outputi(2)
clear/channel 0
! 
open/file draw.dat write fct
if fct(1) .lt. 0 then
   write/out could not create file draw.dat
   return
endif
! 
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},55 S fill white 
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},50 S fill red 
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},45 S fill yellow
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},40 S fill blue 
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},35 S fill cyan 
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},30 S fill black 
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},25 S fill magenta 
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},20 S fill green
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},15 S fill white
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},10 S fill blue 
! 
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},100 S nofill yellow ? n 330.,390.
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},100 S nofill yellow ? n 60.,120.
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},100 S nofill yellow ? n 150.,210.
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},100 S nofill yellow ? n 240.,300.
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},120 S nofill yellow ? n 30.,60.
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},120 S nofill yellow ? n 120.,150.
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},120 S nofill yellow ? n 210.,240.
write/file {fct(1)} -
draw/circle {icorr(1)},{icorr(2)},120 S nofill yellow ? n 300.,330.
! 
close/file {fct(1)}
! 
set/midas output=no
draw/any draw.dat
set/midas output=yes
! 
clear/chan ov
! 
write/out > some more drawing
define/local kk/i/1/10 0 all
define/local rk/r/1/10 0.0 all
! 
rk(10) = 0.4                           !outer radius
rk(9) = 0.1                             !inner radius
!  
kk(2) = 0
! 
draw_loop:
rk(1) = 0.5 + (rk(9) * m$cos(kk(2)))
rk(2) = 0.5 + (rk(9) * m$sin(kk(2)))
rk(3) = 0.5 + (rk(10) * m$cos(kk(2)))
rk(4) = 0.5 + (rk(10) * m$sin(kk(2)))
draw/line {rk(1)},{rk(2)},{rk(3)},{rk(4)} ? ? white
kk(2) = kk(2) + 3
if kk(2) .le. 360 goto draw_loop
!
entry 0005
!
write/out test of INSERT/IMAGE
write/out "--------------------"
!
create/image &a 2,512,512 ? ? 0.,0.01
!
rotate/clock &z &y 2
&z,step(1) = 1.
&z,step(2) = 1.
&y,step(1) = 1.
&y,step(2) = 1.
insert/image &z &a @100,@100
insert/image &y &a @100,@325
insert/image &y &a @325,@325
insert/image &z &a @325,@100
extract/image &k = &a[@129,@129:@384,@384]
!
if dispyes(1) .eq. 1 then
   load/lut smooth
   load/image &k.{imatype}
   wait/secs 3
   clear/chan overlay
endif
!
entry 0006
!
write/out test of FFT/FREQU
write/out "-----------------"
!
write/out > fft/freq &k ? &fr &fi
fft/freq &k ? &fr &fi
if dispyes(1) .eq. 1 load/ima &fr.{imatype} cuts=-0.0005,0.0004
!
write/out > replace/image &fr &gr -0.000002,0.0002=0.
replace/image &fr &gr -0.000002,0.0002=0.
inputi = 28798
if outputi(15) .ne. inputi then
   set/format i1
   write/out -
   "we should have {inputi} pixels replaced, but got {outputi(15)} pixels ..."
   write/out
   errsum = errsum+1
endif
write/out > fft/finv &gr.{imatype} &fi.{imatype} zztr.{imatype} zzti.{imatype}
fft/finv &gr &fi zztr zzti
if dispyes(1) .eq. 1 then
   load zztr.{imatype}
   clear/chan overlay
endif
!
entry 0007
!
write/out  test of using FITS images directly
write/out "----------------------------------"
!
-delete flatf.fits			!test OS commands
-copy nttexample.mt flatf.fits
! 
if workenv(1:1) .eq. "F" return
! 
write/out > show/descr flatf.fits
show/descr flatf.fits
write/out
! 
write/out > statist/image flatf.fits
statist/image flatf.fits
write/key rcorr 479.0,1271.0,498.09,2.626
write/key rcorr/r/8/2 498.0,499.188
@@ kcompare rcorr outputr 1,4 0.01
@@ kcompare rcorr outputr 8,9 0.01
write/key icorr 1150976,1124,550,900,107
@@ kcompare icorr outputi 1,5
! 
write/out > compute/image &a = flatf.fits+0.1
compute/image &a = flatf.fits+0.1
!
write/out > compute/image &b = &a-0.1
compute/image &b = &a-0.1
write/out > and compare &b with flatf.fits
@a diffima &b flatf.fits
if inputi(10) .eq. -1 then
   errsum = errsum+1
   return
endif
! 
write/out
if  workenv(1:1) .eq. "F" then
   -copy middummb.fits klaus.fit
else
   write/out > outdisk/fits middummb klaus.fit
   outdisk/fits middummb klaus.fit
   write/out > and compare klaus.fit with flatf.fits
   @a diffima flatf.fits klaus.fit
   if inputi(10) .eq. -1 then
      errsum = errsum+1
      return
   endif
endif
! 
write/out > replace/image &b &z <,700=700.0
replace/ima &b &z <,700=700.0 >Null
if outputi(15) .ne. 1150968 then
   no. of replace pixels is {outputi(15)} - should be 1150968 ...
   errsum = errsum+1
   return
endif
! 
write/out > replace/image klaus.fit &y <,700=700.0
replace/ima klaus.fit &y <,700=700.0 >Null
if outputi(15) .ne. 1150968 then
   no. of replace pixels is {outputi(15)} - should be 1150968 ...
   errsum = errsum+1
   return
endif
! 
write/out > and compare the result frame &z with &y
@a diffima &z &y
if inputi(10) .eq. -1 then
   errsum = errsum+1
   return
endif
! 
write/out > compute/image &a = (flatf.fits*1.3)-klaus.fit
compute/image &a = (flatf.fits*1.3)-klaus.fit
!
if dispyes(2) .eq. 1 then
   load/ima flatf.fits cuts=f,2sig
endif
!
entry 0008
!
write/out  test of COMPUTE/DIMAGE
write/out "-----------------------"
!
write/out > compute/dimage dimagy = flatf.fits+0.1234567
compute/dimage dimagy = flatf.fits+0.1234567
write/out > compute/dimage &d = flatf.fits-dimagy
compute/dimage &d = flatf.fits-dimagy
write/out > statist/image &d
statist/image &d
write/key rcorr/r/1/2 -1.234567e-01,-1.234567e-01
@@ kcompare rcorr outputr 1,2 0.00001
write/out > compute/dimage tan(45.0)
compute/dimage tan(45.0)
! 
inputd = outputd - 0.999999999955103
if m$abs(inputd) .gt.  0.0000001 then
   errsum = errsum+1
   return
endif
!
entry 0009
!
write/out  descr tests
write/out "------------"
!
delete/temp
!
define/local nodsc/i/1/1 83		
outputi = -99
show/descr dimagy * full >Null
if outputi .lt. nodsc then
   errsum = errsum+1
   return
endif
!
-copy dimagy.{imatype} middummx.{imatype}
delete/descr &x *
do ival = 1 2
   copy/dd dimagy * &x
   outputi = -99
   show/descr &x * full >Null
   if outputi .lt. nodsc then
      errsum = errsum+1
      return
   endif
enddo
! 
entry 00010
!
write/out  test of AVERAGE/IMAGE with FITS files
write/out "--------------------------------------"
! 
do inputi = 1 26
   -copy tst0003.mt avim{inputi}.fits
enddo
write/out > create/icat klaus avim*.fits
create/icat klaus avim*.fits
write/out > read/icat klaus
read/icat klaus
write/out > average/image &av = klaus.cat
average/image &av = klaus.cat
!
if dispyes(2) .eq. 1 then
   load/image &av.{imatype}
endif
if aux_mode(1) .eq. 1 then
   -delete avim*.fits.*
   -delete dimagy.{imatype}.*
else
   -delete avim*.fits
   -delete dimagy.{imatype}
endif

