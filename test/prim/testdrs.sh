#! /bin/sh
# 
# Bourne shell script `testdrs.sh' for testing the `drs' command
# K. Banse	ESO - Garching creation: 000607
# 090827	last modif
# 
# execute via    % testdrs.sh MIDAShomedir MIDASversion
pwd
MIDASHOME=$1; export MIDASHOME
MIDVERS=$2; export MIDVERS
# 
rm -f FORGRdrs.KEY
rm -f kukiwuki.bdf
# 
DAZUNIT=DAZ;export DAZUNIT		#inside Midas DAZUNIT is set!!!
# 
# for the verification we use the explicit path, however, for a correct
# installation, the `drs' script should have been copied to /usr/local/bin
# 
testdrs=${MIDASHOME}/${MIDVERS}/system/unix/drs; export testdrs
echo
echo "test of the drs command"
echo
echo "drs  create/image kukiwuki 2,512,512 \? poly 0.,0.3,0.3,0.8,0.8"
$testdrs  create/image kukiwuki 2,512,512 \? poly 0.,0.3,0.3,0.8,0.8
echo "drs  statist/image kukiwuki p5=FFYN"
$testdrs  statist/image kukiwuki p5=FFYN
$testdrs wait/secs 3
