! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify55.prg  to verify MIDAS commands
!  K. Banse     980928, 031017, 091029, 110428
!
!  use as @@ verify55 ffffffff             with f = 1 or 0 (on/off)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 1111111 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/8 00000000
define/local scale/i/1/1 1 ? +lower
define/local seconds/i/1/2 0,0? +lower
!
delete/temp				!get rid of old temporary files
seconds(1) = m$secs()
write/key sizez/i/1/5 600,600,0,0,0
write/key dispyes/i/1/2 0,0
! 
write/key ccc {p1}
set/format i1
do loop = 1 4				!currently only 4 entries
   if ccc({loop}:{loop}) .eq. "1" @@ verify55,000{loop}
enddo
! 
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
!
write/out +------------------------------------------+
write/out procedure verify55.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
if errsum .gt. 0 then
   write/out We got problems - check the MIDAS logfile !!!
   return 1
else
   write/out All tests o.k. - you deserve a coffee now...
   return 0
endif
write/out +------------------------------------------+
!
!  here the different sub-procedures
!
entry 0001
!
write/out test of CREATE/GRAPHICS + CREATE/DISPLAY
write/out "----------------------------------------"
!
reset/display >Null
create/gra 3 600,400,0,380
create/display 7 512,512,616,300
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
load/lut heat
display/lut
clear/chan overlay
!
entry 0002
!
write/out test of FITS long string handling (CONTINUE keyword)
write/out "---------------------------------------------------"
!
define/local loong/c/1/160 " " all
 
write/descr zperspec bigdsc/c/1/330 "a" all
write/descr zperspec bigdsc/c/81/80 "b" all
write/descr zperspec bigdsc/c/161/80 "y" all
write/descr zperspec bigdsc/c/241/80 "x" all
write/descr zperspec bigdsc/c/321/10 0123456789
! read/descr zperspec bigdsc f
write/descr zperspec testlast/c/1/68 "A" all
zperspec,testlast(67:68) = "X&"

do aux_mode(11) = 0 1
   if aux_mode(11) .eq. 0 then
      write/out "--- using default Midas scheme for long char. descriptors"
   else
      write/out "--- using LONGSTRN scheme of FITS for long char. descriptors"
   endif

   write/keyw loong "D[0~3] & E[0~63] & T[0;1;16] & C[(S[msLimit1]~S[msLimit2]),((S[msLimit2]+1)~S[ms "
   write/keyw loong/c/81/80 "Limit3]),((S[msLimit3]+1)~S[msLimit4]),((S[msLimit4]+1)~S[msLimit5])]"
   ! read/key loong f
   write/out > indisk/fits longstrn.fits lola.tbl
   indisk/fits longstrn.fits lola.tbl
   write/out > outdisk/fits lola.tbl xlstrn.fits
   outdisk/fits lola.tbl xlstrn.fits
   write/out > indisk/fits xlstrn.fits pachita.tbl
   indisk/fits xlstrn.fits pachita.tbl
   read/descr lola.tbl tddes12 f
   if "{lola.tbl,TDDES12}" .ne. loong then
      ival = (aux_mode(11) * 10) + 1
      goto CONTI_error
   endif
   write/keyw loong "(S[msLimit1]~S[msLimit2]),((S[msLimit2]+1)~S[msLimit3]),((S[msLimit3]+1)~S[msLimit4]),((S[msLimit4]+1)~S[msLimit5]) "
   read/descr lola.tbl 1cpix12 f
   if "{lola.tbl,1cpix12}" .ne. loong then
      ival = (aux_mode(11) * 10) + 2
      goto CONTI_error
   endif

   outdisk/fits zperspec xlstrn.fits
   intape/fits 1 koko xlstrn.fits fnn >koko.data
   $cat koko.data | $ grep TESTLAST > fhead.data
   $cat koko.data | $ grep BIGDSC >> fhead.data
   $cat koko.data | $ grep CONTINUE >> fhead.data
   $cat fhead.data
   write/out > indisk/fits xlstrn.fits lupita
   indisk/fits xlstrn.fits lupita
   info/descr lupita testlast
   if mid$info(1) .ne. 3 .or. mid$info(2) .ne. 68 then
      ival = 7
      goto CONTI_error
   endif
   if "{zperspec,testlast(67:68)}" .ne. "X&" then
      ival = 8
      goto CONTI_error
   endif
enddo
! 
!reset to default (needed for subsequent verifications
set/midas longstrn=no
return
! 
CONTI_error:
write/out problems with LONGSTRN option in FITS (flag = {ival})
errsum = errsum+1
!
entry 0003
!
write/out test of RGB mode
write/out "----------------------"
!
if dispyes(1) .ne. 1 return
! 
reset/display  >Null
write/out create a display window in RGB mode
write/out initialize/display  p5=rgb
initialize/display  p5=rgb
create/display 7 600,600,40,300 
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
display/lut off
!
entry 0004
!
write/out test of LOAD/IMAGE
write/out "------------------"
!
if m$exist("zperspec.bdf") .eq. 1 then
   write/out "copy last image as red, green and blue component of an RGB image"
   $cp zperspec.bdf veria.bdf
else
   write/out "create image as red, green and blue component of an RGB image"
   @@ creamask                     !create image arti.bdf
   !
   rebin/linear arti &z .1,.25
   filter/smooth &z &b 2,2,0. >Null
   !
   create/ima &a 2,{&b,npix(1)},{&b,npix(2)} ? poly 0.,0.7
   replace/ima &b veria 0.09,>=&a,0. >Null
endif
! 
compute/image &red = veria
compute/image &green = veria
compute/image &blue = veria
write/out and use them for the different planes
write/out
! 
if dispyes(1) .ne. 1 return
! 
write/out "load only into channel 0 (red)"
load/ima &red 0 scale=1,1
write/out "load only into channel 1 (green)"
clear/channel 0
load/ima &green 1 scale=1,1
write/out "load only into channel 2 (blue)"
clear/channel 1
load/ima &blue 2 scale=1,1
!
write/out "now load red + green"
clear/channel 2
load/ima &red 0 
load/ima &green 1
write/out "now load red + blue"
clear/channel 1
load/ima &blue 2
write/out "now load green + blue"
clear/channel 0
load/ima &green 1 
load/ima &blue 2
write/out "now load all colour planes, so final image is grayscale..."
load/ima &red 0 
! 
! get back to normal pseudo colour mode
write/out get back to normal pseudo colour mode
wait/sec 1
write/out  delete/display all
delete/display all
write/out  initialize/display p5=RGBQ
initialize/display p5=RGBQ
create/display 0 600,600,400,300 
load/lut heat ? d
load zperspec 
wait/sec 1
 
