! @(#)verifyautocurs.prg	19.1 (ESO-DMD) 02/25/03 14:33:48
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify99.prg  to verify MIDAS commands
!  K. Banse     970224, 970604
!
!  use as @@ verify99 fff             with f = 1 or 0 (on/off)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 11 n "Enter control flags for entries: "
!
define/local loop/i/1/1 0
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/8 00000000
define/local scale/i/1/1 1 ? +lower
define/local seconds/i/1/2 0 all +lower
define/local fcontr/i/1/2 0,0 ? +lower
!
seconds(1) = m$secs()
write/key sizez/i/1/5 600,600,0,0,0
write/key dispyes/i/1/2 0,0
write/key stop/i/1/1 0
!
write/key ccc {p1}
set/format i1
do loop = 1 2				!currently only 4 entries
   if ccc({loop}:{loop}) .eq. "1" @@ verify99,000{loop}
enddo
! 
seconds(2) = m$secs()
ival = seconds(2)-seconds(1)
!
delete/temp				!get rid of old temporary files
! 
write/out +------------------------------------------+
write/out procedure verify99.prg:
write/out Total time elapsed = {ival} seconds.
if errsum .gt. 0 then
   write/out We got problems - check the MIDAS logfile !!!
   stop = 1
else
   write/out All tests o.k. - you deserve a coffee now...
endif
write/out +------------------------------------------+
!
!  here the different sub-procedures
!
entry 0001
!
reset/display >Null
initialize/display p5=auto
! 
! prepare file MID_WORK/cursor.automatic
! 
open/file  MID_WORK:cursor.automatic write fcontr
write/file {fcontr} "1,10,22,0"		!ENTER cursor 1
write/file {fcontr} "2,5"			!use bigger increments
write/file {fcontr} "2,-3"			!arrow right (cursor 2)
write/file {fcontr} "2,-5"			!RETURN (for drawing)
write/file {fcontr} "2,5"
write/file {fcontr} "2,-3"
write/file {fcontr} "2,-5"
write/file {fcontr} "2,5"
write/file {fcontr} "2,-2"			!arrow down (cursor 2)
write/file {fcontr} "2,-5"
write/file {fcontr} "2,5"
write/file {fcontr} "2,-2"
write/file {fcontr} "2,-2"
write/file {fcontr} "2,-2"
write/file {fcontr} "1,10,122"			!ENTER cursor 1
write/file {fcontr} "0 "			!EXIT
close/file {fcontr}
! 
write/out 
write/out 
write/out -
"The following interactive commands will be executed in AutoCursor mode"
write/out -
"therefore do not move the mouse around, since that would change the results"
write/out -
"and thus might lead to errors..."
wait/secs 2
! 
create/display 0 20,20,550,550  >Null
create/graphics 3 600,480,0,450
set/graphics
create/display 7 700,512,550,550  >Null	!so keyboard focus is in display w.
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
load/image veria scale=-2,2 cuts=-70,660
load/lut rainbow2
load/itt log
! 
write/out we do an EXTRACT/TRACE using AutoCursor mode
extract/trace ? ? plot
!
write/out statist/image trace
statist/image trace
write/key rcorr 0.0,6.42654E+02,1.13325E+02,1.90601E+02
write/key rcorr/r/7/3 1.430160E+05,1.95079E+00,1.25518E+00
write/key icorr 1262,1,1099
@@ kcompare rcorr outputr 1,4 0.01
@@ kcompare rcorr outputr 7,7 0.9
@@ kcompare rcorr outputr 8,9 0.01
@@ kcompare icorr outputi 1,3
wait/secs 3
! 
entry 0002
!
reset/display >Null
initialize/display p5=auto
! 
open/file  MID_WORK:cursor.automatic write fcontr
write/file {fcontr} "1,226,290,100 "
write/file {fcontr} "0 "
write/file {fcontr} "1,422,170 "
write/file {fcontr} "0 "
write/file {fcontr} "1,559,340 "
write/file {fcontr} "0 "
close/file {fcontr}
! 
create/graphics 3 800,480,0,450
plot/row trace
set/graph color=2
!
write/out we do 3 times GET/GCURSOR using AutoCursor mode
get/gcursor
write/key rcorr 554.051,373.554
@@ kcompare rcorr outputr 1,2 0.01
get/gcursor
write/key rcorr 1067.84,128.869
@@ kcompare rcorr outputr 1,2 0.01
get/gcursor
write/key rcorr 1426.97,475.506
@@ kcompare rcorr outputr 1,2 0.01
set/graph 
! 
