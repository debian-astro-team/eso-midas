! @(#)compFITS.prg	19.1 (ESO-DMD) 02/25/03 14:33:39
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! Midas procedure compFITS.prg 
! K. Banse	ESO - GArching		000726
! 
! to compare FITS primary headers of FITS image files
! execute as: @@ compFITS flag file1 file2
!         flag = IMAGE, TABLE or DIFFERENCES
!         file1, file2 = image or table names for flag = I, T
!         file1 = name of ASCII file holding already the differences of the
!                 FITS headers of two FITS images/tables
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/local diffname/c/1/80 middif.dat
! 
define/param p1 image c "Enter flag (IMAGE/TABLE):"
if p1(1:1) .eq. "I" then
   define/param p2 ? image "Enter first Midas image:"
   define/param p3 ? image "Enter second Midas image:"
else if p1(1:1) .eq. "T" then
   define/param p2 ? table "Enter first Midas table:"
   define/param p3 ? table "Enter second Midas table:"
else if p1(1:1) .eq. "D" then
   define/param p2 middif.dat c "Enter ASCII file name with differences:"
   write/keyw diffname {p2}
   goto check_diff_file
else
   write/out invalid input flag...
   return -2
endif
! 
write/out converting {p2} and {p3} to FITS format
write/out and comparing the primary header of these FITS files
outdisk/fits {p2} middf1.fits >Null
intape/fits 1 midd middf1.fits fnn >middat1.dat
outdisk/fits {p3} middf2.fits >Null
intape/fits 1 midd middf2.fits fnn >middat2.dat
if aux_mode .lt. 2 then
   write/out Sorry, this procedure has not been updated for VMS yet...
   return -1
else
   $diff middat1.dat middat2.dat >{diffname}
endif
! 
check_diff_file:
define/local fc/i/1/2 0,0
open/file {diffname} read fc
if fc(1) .lt. 0 then
   write/out Problems in opening ASCII file "{diffname}"
   return -1
endif
! 
read_loop:
write/key inputc/c/1/20 " " all
read/file {fc(1)} inputc 20
if fc(2) .lt. 0 then
   close/file {fc(1)}
   write/out FITS headers are `equal'...
   return 0
endif
! 
! only DATE and FILENAME should be different, nothing else...
! 
if inputc(1:1) .ne. "<" .and. inputc(1:1) .ne. ">" then
   goto read_loop
endif
! 
if inputc(3:7) .eq. "DATE " goto read_loop
if inputc(3:11) .eq. "FILENAME=" goto read_loop
! 
! some other differences found...
write/out line: \"{inputc(1:20)}\" is bad...
close/file {fc(1)}
return 1
 
