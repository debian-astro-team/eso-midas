! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure veriall.prg  to execute all the "verify" procedures
!  K. Banse     921202	creation
!
!  use as:	@@ veriall display flags format loop 
! 
!  130611		last modif
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 Display c "Enter NoDisplay or Display (the default):"
define/param p2 10 c "Enter 2-digit flag for choice of verifications:"
! p2(1:1) = 1 for non-interactive tests
! p2(2:2) = 1 for interactive tests
define/param p3 1 n -
   "Enter flag (1/2/3/4) for using data files in Midas, FITS, M+F, F+M format):"
define/param p4 1 n "Enter no. of verification runs:"
! 
if p1(1:1) .eq. "D" then
   write/keyw dispflg/i/1/1 1
else
   write/keyw dispflg/i/1/1 0
endif
! 
write/keyw verities/i/1/30 0 all
write/keyw nodemo/i/1/1 0		!to check MID_TEST dir.
! 
write/keyw loopflag/i/1/2 1,1
write/keyw datformat/c/1/6 " " all
write/keyw times/i/1/5 0 all
times(1) = m$secs()
set/format i1
! 
! check for basic run
if p2(1:1) .eq. "0" goto last_procs
! 
! check, if we have the default width (80 chars)
if log(10) .ne. 80 then
   write/out "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
   write/out Warning: terminal width not good for logfile comparison...
   write/out "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
endif
! 
do loopflag(1) = 1 {p4} 1
   loopflag(2) = {p3}			!init loopflag(2) again
   if loopflag(2) .eq. 1 .or. loopflag(2) .eq. 3 then
      datformat = "Midas "
   else
      datformat = "FITS  "
   endif
   set/midas workenv={datformat}
   ! 
  FITSloop:
   ! 
   write/out
   write/out *********************************************************
   write/out
   set/format i3
   write/out    Verification run ({loopflag(1)}) using data files in {datformat} format
   set/format i1
   write/out
   write/out *********************************************************
   write/out
   !
   q1 = "0"
   @@ verify1 
   if q1 .ne. "0" return 99
   verities(1) = mid$info(8)
   ! 
   @@ verify2 {dispflg}1111111111 111111111111
   if q1 .ne. "0" return 99
   verities(2) = mid$info(8)
   ! 
   @@ verify3 
   if q1 .ne. "0" return 99
   verities(3) = mid$info(8)
   ! 
   @@ verify4 {dispflg}1111111111 111111111111
   if q1 .ne. "0" return 99
   verities(4) = mid$info(8)
   ! 
   @@ verify5 {dispflg}111111 1111111111111
   if q1 .ne. "0" return 99
   verities(5) = mid$info(8)
   ! 
   @@ verify55 {dispflg}111111 1111111111111
   if q1 .ne. "0" return 99
   verities(23) = mid$info(8)
   ! 
   @@ verify6 {dispflg}1111111111 111111111111
   if q1 .ne. "0" return 99
   verities(6) = mid$info(8)
   ! 
   @@ verify7  {dispflg}1111111111 111111111111
   if q1 .ne. "0" return 99
   verities(7) = mid$info(8)
   ! 
   @@ verify8  {dispflg}1111111111 111111111111
   if q1 .ne. "0" return 99
   verities(8) = mid$info(8)
   ! 
   @@ verifyt1 
   if q1 .ne. "0" return 99
   verities(21) = mid$info(8)
   ! 
   @@ verifyt2 
   if q1 .ne. "0" return 99
   verities(22) = mid$info(8)
   ! 
   @@ verifydio
   verities(20) = mid$info(8)
   if q1 .ne. "0" then
      write/out ******** verifydio.prg ******** ... failed ...
      return 99
   endif
   ! 
   @@ verify99  {dispflg}1111111111 
   if q1 .ne. "0" return 99
   verities(24) = mid$info(8)
   ! 
   @@ verify9  1111111111 111111111111
   if q1 .ne. "0" return 99
   verities(9) = mid$info(8)
   ! 
   @@ verify10  {dispflg}11111111
   if q1 .ne. "0" return 99
   verities(10) = mid$info(8)
   ! 
   @@ verify11  {dispflg}1111111111 
   if q1 .ne. "0" return 99
   verities(11) = mid$info(8)
   ! 
   @@ verify12  {dispflg}111111111111 
   if q1 .ne. "0" return 99
   verities(12) = mid$info(8)
   ! 
   @@ verify13  {dispflg}111111111111 
   if q1 .ne. "0" return 99
   verities(13) = mid$info(8)
   ! 
   @@ verify14  {dispflg}111111111111		!Marc's table stuff...
   if q1 .ne. "0" return 99
   verities(14) = mid$info(8)
   ! 
   @@ verify15  {dispflg}111111111111111
   if q1 .ne. "0" return 99
   verities(15) = mid$info(8)
   ! 
   @@ verify16  {dispflg}111111111111111
   if q1 .ne. "0" return 99
   verities(16) = mid$info(8)
   ! 
   ! +++ special test procedure for copy/table ...
   @@ verify17  {dispflg}11111
   if q1 .ne. "0" then
      write/out "  "
      write/out "*** these problems are caused by the copy/table command..."
      write/out "*** please, notify ESO/Midas via the midas-users mailing list"
      return 99
   endif
   ! +++
   ! 
   if loopflag(2) .gt. 2 then
      @ vericopy  >Null
      if loopflag(2) .eq. 3 then
         datformat = "FITS  "
      else
         datformat = "Midas "
      endif
      set/midas workenv={datformat}
      loopflag(2) = -1			!loop only once
      goto FITSloop
   endif
   ! 
   if {p3} .gt. 1 @ vericopy  >Null
   !
enddo
! 
set/midas workenv=midas
! 					get execution time
times(2) = m$secs()-times(1)
set/format i1
times(3) = times(2)/60
times(4) = times(2)-times(3)*60
! 					get system info from host
if aux_mode(1) .ne. 1 then
   $ uname -mnrs | write/keyw out_b
else
   write/keyw out_b VAX/VMS system
endif
inputc = m$time(1)
! 					open logfile for verification times
define/local fc/i/1/3 0,0,0
inputi = m$index(out_b," ") - 1
if inputi .lt. 1 then
   write/keyw in_b  MID_WORK:veriall_X.{inputc} 
else
   if inputi .gt. 8 inputi = 8
   if out_b(1:5) .eq. "Linux" then		!different Linux systems
      $ uname -m | write/keyw outputc
      write/keyw in_b MID_WORK:veriall_{out_b(1:{inputi})}_{outputc}.{inputc}
   else
      write/keyw in_b MID_WORK:veriall_{out_b(1:{inputi})}.{inputc}
   endif
endif
write/keyw in_b  MID_WORK:veriall_debian.{inputc}
open/file {in_b} write  fc
if fc(1) .lt. 0 then
   write/out Could not create file: "MID_WORK:veriall_{out_b}.{inputc} ..."
   fc(3) = -1
endif
! 
write/out  
write/out Midas version {mid$sess(16:27)} ... {inputc(1:40)}
if fc(3) .eq. 0 then
   write/file {fc(1)} "Midas version {mid$sess(16:27)} ... {inputc(1:40)}"
endif
write/out 
if loopflag(1) .eq. 1 then
   write/out -
   "successful end of verification run (with option {p3})"
   if fc(3) .eq. 0 then
      write/file {fc(1)} -
         "successful end of verification run (with option {p3})"
   endif
else
   write/out -
   "successful end of {p4} verification runs (with option {p3})"
   if fc(3) .eq. 0 then
      write/file {fc(1)} -
         "successful end of {p4} verification runs (with option {p3})"
   endif
endif
if nodemo .gt. 0 then
   write/out " "
   write/out {nodemo} files were missing from Midas demo-data dir. (MID_TEST)
   write/out check the ASCII file ./missing-files for details
   write/out " "
else
   write/out Midas demo-data dir. (MID_TEST) complete
endif
write/out "elapsed time: {times(3)}m {times(4)}s ({times(2)} secs)"
   write/out
if fc(3) .eq. 0 then
   write/file {fc(1)} "elapsed time: {times(3)}m {times(4)}s ({times(2)} secs)"
   write/file {fc(1)} "   "
endif
! 
write/out individual times (in secs) for the verify(x) procedures:
write/out (1) {verities(1)}, (2) {verities(2)}, (3) {verities(3)}, (4) {verities(4)}, (5) {verities(5)},
write/out (6) {verities(6)}, (7) {verities(7)}, (8) {verities(8)}, (9) {verities(9)}, (10) {verities(10)},
write/out (11) {verities(11)}, (12) {verities(12)}, (13) {verities(13)}, (14) {verities(14)}, (15) {verities(15)}, (16) {verities(16)}
write/out (dio) {verities(20)}, (t1) {verities(21)}, (t2) {verities(22)}, (99) {verities(29)}
if fc(3) .eq. 0 then
   write/file {fc(1)} -
      individual times (in secs) for the verify(x) procedures:
   write/file {fc(1)} -
      "(1) {verities(1)}, (2) {verities(2)}, (3) {verities(3)}, (4) {verities(4)}, (5) {verities(5)},"
   write/file {fc(1)} -
      "(6) {verities(6)}, (7) {verities(7)}, (8) {verities(8)}, (9) {verities(9)}, (10) {verities(10)},"
   write/file {fc(1)} -
      "(11) {verities(11)}, (12) {verities(12)}, (13) {verities(13)}, (14) {verities(14)},(15) {verities(15)}"
   write/file {fc(1)} -
      "(dio) {verities(20)}, (t1) {verities(21)}, (t2) {verities(22)}, (99) {verities(29)}"
endif
! 
write/out 
write/out system used: {out_b(1:40)}
! 
if fc(3) .eq. 0 then
   write/file {fc(1)} "   "
   write/file {fc(1)} "system used: {out_b(1:40)}"
   close/file {fc(1)}
endif
!
! if required, execute the interactive verification procedures 
! 
last_procs:
if p2(2:2) .eq. "1" then
   write/out executing interactive verifications
   write/out
   @@ verifyia
endif
return 0
 
