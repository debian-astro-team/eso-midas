! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure verify5.prg  to verify MIDAS commands
!  K. Banse     921222, 930917, 031017, 050531, 091029, 110215
!
!  use as @@ verify5 ffffff ffffff            with f = 1 or 0 (on/off)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 11111111 n "Enter control flags for entries: "
define/par p2 11111111 n "Enter control flags for entries: "
! 
define/local loop/i/1/1 0 
define/local test/c/1/1 ? ? +lower
define/local rval/r/1/1 0. ? +lower
define/local ival/i/1/5 0 all +lower
define/local seconds/i/1/2 0,0 ? +lower
define/local rcorr/r/1/20 0. all +lower
define/local icorr/i/1/20 0 all +lower
define/local errsum/i/1/1 0 ? +lower
define/local ccc/c/1/8 00000000
define/local error_entry/i/1/1 0
! 
delete/temp                             !get rid of old temporary files
! 
write/key sizez/i/1/5 4000,256,512,496,1000
write/key dispyes/i/1/2 0,0
! 
write/out +------------------------------------------+
write/out Start of procedure verify5.prg
write/out +------------------------------------------+
!
! if enabled, handle FITS working environment
!
set/midas newfil=?? >Null
if outputc(1:1) .eq. "F" then           !we're in true FITS environment
   inputi = m$len(mid$types)
   define/local imatype/c/1/{inputi} {mid$types(1:8)} ? +lower
   inputi = m$len(mid$types(9:))
   define/local tbltype/c/1/{inputi} {mid$types(9:)} ? +lower
   define/local workenv/c/1/4 FITS ? +lower
else
   define/local imatype/c/1/3 bdf ? +lower
   define/local tbltype/c/1/3 tbl ? +lower
   define/local workenv/c/1/5 Midas ? +lower
endif
! 
seconds(1) = m$secs()
write/key ccc/c/1/8 {p1}
set/format i1
!
do loop = 1 6
   if ccc({loop}:{loop}) .eq. "1" @@ verify5,000{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 000{loop} in verify5.prg!"
      return 1
   endif
enddo
! 
write/key sizez/i/1/5 4000,256,512,496,1000
write/key ccc/c/1/8 {p2}
do loop = 1 8
   if ccc({loop}:{loop}) .eq. "1" @@ verify5,001{loop}
   if errsum .gt. 0 then
      write/out "We got problems with entry 001{loop} in verify5.prg!"
      return 1
   endif
enddo
seconds(2) = m$secs()
mid$info(8) = seconds(2)-seconds(1)
! 
write/out +------------------------------------------+
write/out procedure verify5.prg:
write/out Total time elapsed = {mid$info(8)} seconds.
write/out All tests o.k. - you deserve a coffee now...
write/out +------------------------------------------+
return 0
! 
!  here the different sub-procedures
! 
entry 0001
! 
write/out test of CREATE/DISPLAY + CREATE/GRAPHICS
write/out "----------------------------------------"
! 
reset/display
create/display 7 512,512,616,300
create/gra 3 600,400,0,380
dispyes(1) = 1                          !mark that we have display + graphic
dispyes(2) = 1
load/lut random3
display/lut
! 
entry 0002
! 
write/out test of CREATE/IMA + INSERT/IMA
write/out "-------------------------------"
! 
crea/ima veria 1,{sizez(1)} ? poly 0.0,0.01
! 
if dispyes(2) .eq. 1 then
   set/graph colo=1
   plot/row veria
endif
! 
sizez(2) = sizez(1)/8
rval = {veria[>]}			!value of last x-pixel
! 
write/ima veria @{sizez(2)},1 {rval}
sizez(3) = 2*sizez(2)
write/ima veria @{sizez(3)},1 {rval}
sizez(3) = 3*sizez(2)
write/ima veria @{sizez(3)},1 {rval}
sizez(3) = 4*sizez(2)
write/ima veria @{sizez(3)},1 {rval}
sizez(3) = 5*sizez(2)
write/ima veria @{sizez(3)},1 {rval}
sizez(3) = 6*sizez(2)
write/ima veria @{sizez(3)},1 {rval}
sizez(3) = 7*sizez(2)
read/descr veria
! 
if dispyes(2) .eq. 1 then
   set/graph colo=2
   plot/row veria
endif
! 
entry 0003
! 
write/out test of STATISTICS/IMA
write/out "----------------------"
! 
write/out > stat/ima veria
stat/ima veria
write/key rcorr 0.,39.990,20.02875,11.5692,-4.59851E-04,1.79856,80115
write/key icorr 4000,1,500
@@ kcompare rcorr outputr 1,4 0.01
@@ kcompare rcorr outputr 5,7 0.3
@@ kcompare icorr outputi 1,3
! 
write/out > stat/ima veria p5=gf
stat/ima veria p5=gf
inputr(8) = 20.03
if m$abs(outputr(8)-inputr(8)) .gt. 0.0001 goto bad_median
! 
write/out > stat/ima veria [@100:@2080] ? 1.099,29.99 p5=gf
stat/ima veria [@100:@2080] ? 1.099,29.99 p5=gf
inputr(8) = 1.094000e+01
if m$abs(outputr(8)-inputr(8)) .gt. 0.0001 goto bad_median
! 
write/out > stat/ima veria ? ? 1.099,39.99 p5=gf
stat/ima veria ? ? 1.099,39.99 p5=gf
inputr(8) = 2.058000e+01
bad_median:
if m$abs(outputr(8)-inputr(8)) .gt. 0.0001 then
   write/out "#### problem with exact median..."
   write/out
   errsum = errsum+1
endif
!
if dispyes(2) .eq. 1 then
   set/graph colo=4
   plot/histogram veria 
endif
! 
entry 0004
! 
write/out test of COMPUTE/IMAGE
write/out "---------------------"
!
write/out > compute/image &z = log10(veria) + sin(veria*20.) 
compute/image &z = log10(veria) + sin(veria*20.) 
if null .ne. 1 then
   set/format f3.0
   write/out -
   "we should have 1 undefined pixels, but got {null} bad pixels ..."
   write/out
   errsum = errsum+1
endif
! 
write/out > statist/image &z
statist/image &z
write/keyw rcorr -1.99651,2.58615,1.22904,0.73147,-5.65490E-02,2.30671,4916.17
write/keyw icorr 4000,2,500
@@ kcompare rcorr outputr 1,6 0.001
@@ kcompare rcorr outputr 7,7 0.5
@@ kcompare icorr outputi 1,3 
! 
if dispyes(2) .eq. 1 then
   set/graph colo=6
   plot/row &z
endif
!
entry 0005
!
write/out test of GROW/IMAGE
write/out "------------------"
!
write/out > extract/image &b = &z[@201:@400]
extract/image &b = &z[@201:@400]
grow/image &x = &b ? ? col
! 
if dispyes(1) .eq. 1 load/ima &x
! 
write/out > extra/ima &a = &z[@201:@600]
extract/image &a = &z[@201:@600]
write/out > grow/image &y = &a 0.,1.,300
grow/image &y = &a 0.,1.,300
! 
if dispyes(1) .eq. 1 load/ima &y
! 
write/out > insert/image &x &y @50,@50
insert/image &x &y @50,@50
! 
if dispyes(1) .eq. 1 load/ima &y
! 
write/out > stat/image &y
statist/image &y
write/key rcorr 0.943818,2.58615,1.4441,0.250707,-3.55569E-01,2.81676,173292
write/key icorr 120000,1,1,300,1
@@ kcompare rcorr outputr 1,6 0.001
@@ kcompare rcorr outputr 7,7 0.9
@@ kcompare icorr outputi 1,5
! 
entry 0006
! 
write/out test of FILTER/SMOOTH
write/out "---------------------"
!
write/out > filter/median &z &f 4,0
filter/median &z &f 4,0
if dispyes(2) .eq. 1 then
   set/graph colo=1
   plot/row &z
   set/graph colo=2
   overplot/row &f
endif
@@ works,ccc &f
write/key rcorr -1.51241,1.68378,0.663678,0.310067,1.94382,2.04571,
write/key rcorr/r/7/3 0.614316,1.19865,2.58467
@@ kcompare rcorr outputr 1,9 0.0001
!
entry 0011
!
write/out test of CREATE/IMAGE 
write/out "--------------------"
!
set/format i1
write/out > size of frame veria.{imatype} = {sizez(2)} * {sizez(2)}
create/image veria 2,{sizez(2)},{sizez(2)} ? radius_law 200.,40.,0.,0.
write/key rcorr 0.0161776,0.0353602,0.0164754,0.0353602,12.2124,0.0365716
write/key rcorr/r/7/3 0.0164754,0.0365716,0.0167829
@@ works,aaa veria
!
if dispyes(1) .eq. 1 load/ima veria.{imatype}
! 
entry 0012
!
write/out test of COMPUTE/IMAGE
write/out "---------------------"
!
write/out > compute/ima &z = (veria*2.)-(veria+(veria*sin(90.)))
compute/ima &z = (veria*2.)-(veria+(veria*sin(90.)))
find/minmax &z >Null
write/key rcorr 0.,0.
@@ kcompare rcorr outputr 1,2 0.0001
write/out > compute/ima &y = -
"veria+veria+veria+1.234-3.*veria*(2.0-exp(0.0))"
compute/ima &y = -
veria+veria+veria+1.234-3.*veria*(2.0-exp(0.0))
find/minmax &y >Null
write/key rcorr 1.234,1.234
@@ kcompare rcorr outputr 1,2 0.0001
!
entry 0013
!
write/out "test of COMPUTE/IMAGE with large images"
write/out "---------------------------------------"
!
set/format i1
define/local savemapsize/i/1/1 {monitpar(20)}
write/out > size of frame veria.{imatype} = {sizez(5)} * {sizez(5)}
crea/ima veria 2,{sizez(5)},{sizez(5)} ? radius_law 200.,40.,0.,0.
!
if dispyes(1) .eq. 1 load/ima veria.{imatype}
write/out > compute/ima &z = (veria*2.+4.56)-(veria+(veria*sin(90.)))
monitpar(20) = 100
write/out > with monitpar(20) = 100
compute/ima &z = (veria*2.+4.56)-(veria+(veria*sin(90.)))
find/minmax &z >Null
write/key rcorr 4.56,4.56
@@ kcompare rcorr outputr 1,2 0.0001
monitpar(20) = 900
write/out > with monitpar(20) = 900
compute/ima &z = (veria*2.+4.56)-(veria+(veria*sin(90.)))
find/minmax &z >Null
write/key rcorr 4.56,4.56
@@ kcompare rcorr outputr 1,2 0.0001
! 
write/out > "compute/ima &y = -"
write/out > min(exp(veria*0.01),atan(log10(veria*sin(veria*100.0)))+0.1234/veria)
monitpar(20) = 100
write/out > with monitpar(20) = {monitpar(20)}
compute/ima &y = -
min(exp(veria*0.01),atan(log10(veria*sin(veria*100.0)))+0.1234/veria)
statist/imag &y >Null
write/key rcorr  -67.5703,1.1299,-5.686,17.5448
@@ kcompare rcorr outputr 1,4 0.001
outputr(1) = outputr(5)
rcorr(1) = -2.52167
@@ kcompare rcorr outputr 1,1 0.1
rcorr(1) = 72.0
@@ kcompare rcorr null 1,1 0.001
monitpar(20) = 900
write/out > with monitpar(20) = {monitpar(20)}
compute/ima &y = -
min(exp(veria*0.01),atan(log10(veria*sin(veria*100.0)))+0.1234/veria)
statist/imag &y >Null
write/key rcorr  -67.5703,1.1299,-5.686,17.5448
@@ kcompare rcorr outputr 1,4 0.001
outputr(1) = outputr(5)
rcorr(1) = -2.52167
@@ kcompare rcorr outputr 1,1 0.1
rcorr(1) = 72.0
@@ kcompare rcorr null 1,1 0.001
! 
write/out > get a "perspective view of &y" via: "@a zperspec &y zperspec ? ? no"
@a zperspec &y zperspec ? ? no
if dispyes(1) .eq. 1 then
   write/out > load/ima zperspec.{imatype}
   load/ima zperspec
endif
! 
monitpar(20) = 100
write/out > compute/ima &t = -
"veria+veria+veria+1.234-3.*veria*(2.0-exp(0.0))"
compute/ima &t = -
veria+veria+veria+1.234-3.*veria*(2.0-exp(0.0))
find/minmax &t >Null
write/key rcorr 1.234,1.234
@@ kcompare rcorr outputr 1,2 0.0001
monitpar(20) = savemapsize
!
entry 0014
!
write/out "test of COMPUTE/IMA with different image size"
write/out "---------------------------------------------"
!
crea/ima &x 2,{sizez(3)},{sizez(4)} ? poly 0.5
crea/ima veria 2,{sizez(2)},{sizez(2)} ? radius_law 200.,40.,0.,0.
write/descr veria start 200.,196.
insert/ima veria &x
!
if dispyes(1) .eq. 1 load/ima &x
! 
write/out > compute/ima &w = veria + &x
compute/ima &w = veria + &x
compute/ima &d = &w-2*veria
find/minmax &d
write/key rcorr 0.,0.
@@ kcompare rcorr outputr 1,2 0.0001
!
entry 0015
!
write/out "test of COMPUTE/ROW"
write/out "-------------------"
!
crea/ima veria 2,{sizez(2)},{sizez(2)} ? radius_law 200.,40.,0.,0.
extra/ima &a = veria[<,@123:>,@123]
extra/ima &b = veria[<,@213:>,@213]
comp/ima &c = &a+&b - (&b*cos(0.))
write/out > compute/row veria.r100 = r123+r213-(r213*cos(0.))
compute/row veria.r100 = r123+r213-(r213*cos(0.))
extra/ima &q = veria[<,@100:>,@100]
compute/ima &w = &q-&c
find/minmax &w
write/key rcorr 0.,0.
@@ kcompare rcorr outputr 1,2 0.0001
! 
write/out "test of COMPUTE/COLUMN"
write/out "----------------------"
!
crea/ima veria 2,{sizez(2)},{sizez(2)} ? radius_law 200.,40.,0.,0.
extra/ima &a = veria[@123,<:@123,>]
extra/ima &b = veria[@213,<:@213,>]
comp/ima &c = &a+&b - (&b*cos(0.))
write/out > compute/col veria.c100 = c123+c213-(c213*cos(0.))
compute/col veria.c100 = c123+c213-(c213*cos(0.))
extra/ima &q = veria[@100,<:@100,>]
compute/ima &w = &q-&c
find/minmax &w
write/key rcorr 0.,0.
@@ kcompare rcorr outputr 1,2 0.0001
! 
entry 0016
!
write/out "test of COMPUTE/XYPLANE"
write/out "-----------------------"
!
set/format i1
write/out > create/ima &cube 3,{sizez(2)},{sizez(2)},8
create/ima &cube 3,{sizez(2)},{sizez(2)},8
do inputi = 1 8
   compute/ima &v = sin(veria*33.3*{inputi})
   insert/ima &v &cube <,<,@{inputi}
enddo
! 
write/out > compute/xyplane &c = &cube - veria
compute/xyplane &c = &cube - veria
! 
if dispyes(1) .eq. 1 load/ima &c.{imatype},all,1
! 
write/out > statist/ima &c [<,<,@4:>,>,@4] ? ? ff
statist/ima &c [<,<,@4:>,>,@4] ? ? ff
write/key rcorr  -12.3289,0.41834,0.02985,0.1963,-2.73969E+01,1.23591E+03
@@ kcompare rcorr outputr 1,4 0.0005
@@ kcompare rcorr outputr 5,6 0.005
! 
entry 0017
!
write/out "test of COMPUTE/PLANE"
write/out "-----------------------"
!
write/out > compute/plane &c.p2 = P1+P4
compute/plane &c.p2 = P1+P4
compute/image &a = &c@1+&c@4
extract/image &b1 = &c [<,<,@2:>,>,@2]
@@ imcompare &a &b1 0.001
! 
write/out > compute/plane &c.p7 = 99.9*sin(&cube.p5)
compute/plane &c.p7 = 99.9*sin(&cube.p5)
compute/image &a = 99.9*sin(&cube@5)
extract/image &b2 = &c [<,<,@7:>,>,@7]
@@ imcompare &a &b2 0.001
! 
write/out > compute/plane &c.p1 = (1.1+veria)/p7
compute/plane &c.p1 = (1.1+veria)/p7
compute/image &a = (1.1+veria)/&c@7
extract/image &b3 = &c [<,<,<:>,>,<]
@@ imcompare &a &b3 0.001
! 
write/out > compute/plane &c.p8 = tan(23.45)
compute/plane &c.p8 = tan(23.45)
compute/image &a = tan(23.45)
extract/image &b4 = &c [<,<,>:>,>,>]
@@ imcompare &a &b4 0.001
! 
entry 0018
! 
write/out "test of COMPUTE/ZYPLANE"
write/out "-----------------------"
!
write/out > compute/zyplane &z = &cube *2.22
write/out > "this will take quite some time -" " be patient ..."
compute/zyplane &z = &cube * 2.22
statist/ima &z [@100,<,<:@100,>,>] ? ? ff >Null
copy/key outputr/r/1/12 rcorr
write/out > extract/image &a =  &z [@100,<,<:@100,>,>]
extract/image &a =  &z [@100,<,<:@100,>,>]
statist/image &a ? ? ? ff >Null
@@ kcompare rcorr outputr 1,12 0.0001
write/out > extract/image &b =  &cube [@100,<,<:@100,>,>]
extract/image &b =  &cube [@100,<,<:@100,>,>]
write/out > compute/image &d = &a/&b
compute/image &d = &a/&b
write/out read/image &d <,@8,20
read/image &d <,@8,20
find/minmax &d >Null
copy/key rcorr/r/1/12 inputr            !save rcorr
rcorr(1) = 2.22
rcorr(2) = 2.22
@@ kcompare rcorr outputr 1,2 0.0001
copy/key inputr/r/1/12 rcorr
!
write/out "test of TRANSPOSE/CUBE"
write/out "----------------------"
!
if mid$sys(1:5) .eq. "PC/Cy" return		!avoid for Cygwin ...
! 
write/out transpose/cube &z &v ZY
transpose/cube &z &v ZY
if dispyes(1) .eq. 1 then
   load/ima &v.{imatype},100 scale=8,1
   load/ima &v.{imatype},101..160
endif
!
statist/image &v [<,<,@100:>,>,@100] ? ? ff
@@ kcompare rcorr outputr 1,2 0.0001

