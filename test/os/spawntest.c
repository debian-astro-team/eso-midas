/* @(#)spawntest.c	19.1 (ES0-DMD) 02/25/03 14:33:06 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include <stdio.h>
#include <signal.h>


#ifndef SIGCLD
#define SIGCLD SIGCHLD		/* Death of child signal has different name */
#endif				/* in BSD and SYSV */
#ifndef BADSIG
#define BADSIG	(int (*)())-1
#endif

int errno;

main(argc,argv)
int argc;
char **argv;
{
	int pid;	
	int i;
	void  (*parint)(), (*parquit)();
	int chstat;

	parint = signal(SIGINT,SIG_IGN);	/* save parent sig.-s */
	parquit = signal(SIGQUIT,SIG_IGN); 
	signal(SIGCLD,SIG_DFL);
	printf("BEGIN TESTOSP\n");
	switch (pid = fork()) {
	case -1:
		return(-1);
	case 0: 		/* In child process */
		if(signal(SIGINT, SIG_DFL) == BADSIG || 
		   signal(SIGQUIT, SIG_DFL) == BADSIG ||
		   signal(SIGCLD, SIG_DFL) == BADSIG) 
			ospexit(1);
		printf("I'm the child: %s\n",argv[1]);
		/* sleep(10);*/
		execl("/bin/sh","sh","-c",argv[1],(char *)0);
		ospexit(-1);  /* suppress the fork	*/;
	
	default: 	/* In father process */
		while((i = wait(&chstat)) != pid && i != -1) {
			printf("Not my child: %x %x\n",i,pid);
			}
		printf("My child: i=%x pid=%x chstat=%x\n",i,pid,chstat);
		if (i==-1) {
			printf("Errno=%x\n",errno);
			}
		printf("END TESTOSP\n");
	}
	ospexit(0);
}
