/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*

.VERSION
051021		last modif

*/

/* practically the same as for osa functions	*/
#include  <stdlib.h>
#include  <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/file.h>
#include <filedef.h>
#define errprint(x) { printf(x); ospexit(1); }
char fname[80];

main(argc,argv)
int argc;
char *argv[];
{
	char *pbuf, *pbufn, *pbl, *buf1, *buf2;
	int in,out,size,newsize;
	long nchar;
	int osdopen(),osdclose();
	long osdseek(),osdread(),osdwrite();

	strcpy(fname,argv[0]); strcat(fname,".c");
	if((in=osdopen(fname,READ))== -1)  	/* open prog. text */
		errprint("Osdopen BAD on read\n");
	if((size=(int)osdseek(in,0L,FILE_END))== -1) /* get file length */
		errprint("Osdseek to end BAD on read\n");
	if((buf1=malloc((size_t)size))==NULL)	/* allocate first membuf */
		errprint("Something wrong with malloc()\n");
	if(osdseek(in,0L,FILE_START)== -1)		/* rewind file */
		errprint("Osdseek to start BAD on read\n");
	/*
	** rewind the file
	*/
	if((int)(nchar=osdread(in,buf1,(long)size))!=size) 
		printf("Osdread BAD - got %ld bytes less\n",size-nchar);
	if(osdclose(in)== -1)						/* close the file */
		printf("Osdclose BAD on read\n");

	strcpy(fname,argv[0]); strcat(fname,".b");
	/* 
	** 	create copy file manually	
	*/
	if((out=open(fname,O_WRONLY | O_CREAT,0644))== -1
			|| close(out)== -1)		 
		errprint("Cannot create copy file\n");
	if((in=out=osdopen(fname,READ_WRITE))== -1)	/* open copy file	*/
		errprint("Osdopen BAD on read_write\n");
	/* 
	** write membuf out	
	*/
	if((int)osdwrite(out,buf1,(long)size)!= size)
		errprint("Osdwrite BAD\n");
	/*
	** compare sizes
	*/
	if((newsize=(int)osdseek(out,0L,FILE_END))!=size)
		printf("Osdseek to end BAD after write, %d for %d\n",
			newsize,size);
	if(osdseek(out,0L,FILE_START)!=0L)
		printf("Osdseek to start BAD after write\n");
	if((buf2=malloc((size_t)newsize))==NULL)   /* alloc second membuf */
		errprint("Something wrong with malloc()\n");
	/* 
	** read copy back	
	*/
	if((int)(nchar=osdread(in,buf2,newsize))!=newsize)
		printf("Osdread BAD - got %ld bytes less\n",newsize-nchar);
	if(osdclose(in)== -1)						/* close the file	*/
		printf("Osdclose BAD on read-write file\n");
	/* 
	** compare memory buffers	
	*/
	for(pbuf=buf1,pbufn=buf2,pbl=buf1+size;pbl-pbuf;)
		if (*pbuf++ != *pbufn++) {
			printf("All BAD -buffers differ at char %d (tot.size %d)\n",
						pbuf-buf1,size);
			break;
			}
	ospexit(0);
}
