/* @(#)osmtest1.c	19.1 (ES0-DMD) 02/25/03 14:33:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include <stdio.h>
#include <osparms.h>
#define errmess(x) { fprintf(stderr,x); }
#define errexit(x) { fprintf(stderr,x); ospexit(1); }
#define PIECE 500000L

main()
{
    int osmshget(),osmshfree();
	int testmem;
	char *map;

/*	if((testmem=osmshget(getppid(),PIECE,&map,IPC_CREAT))==-1) */
	if((testmem=osmshget(getppid(),PIECE,&map))==-1) 
		errexit("Osmshget BAD in child\n");
	*(map+PIECE/2)== 'a';
/*	if(osmshfree(testmem,map)== -1)			*/
/*	  errmess("Osmshfree BAD in child\n"); */
    ospexit(0);
}
	
