/* @(#)osetest1.c	19.1 (ES0-DMD) 02/25/03 14:33:04 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include <stdio.h>
#include <osparms.h>
#define errmess(x) fprintf(stderr,x)
#define errexit(x) { fprintf(stderr,x); ospexit(1); }

main ()
{
	int oseprep(),oseset(),oseread(),oseclear(),osewait();
     
/*	printf("a\n"); */
	if(oseprep()== -1) 
		errmess("Oseprep BAD\n");
	if(oseread(0)!= 1)
		errexit("Oseread in child BAD\n");
/*	printf("b\n"); */
	/* CGS000: Flag 0 doens not exit in this version */
	if(oseclear(0)== -1)
		errmess("Oseclear in child BAD\n");
	sleep(5);
/*	printf("c\n"); */
	if(oseset(0)== -1)
		errmess("Oseset in child BAD\n");
/*	printf("d\n"); */
}
