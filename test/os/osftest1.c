/* @(#)osftest1.c	19.1 (ES0-DMD) 02/25/03 14:33:04 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++   
.TYPE           Program
.LANGUAGE       C
.IDENTIFICATION	osfTEST.c
.VERSION 1.0	23-Oct-1987: Creation
.AUTHOR         Francois Ochsenbein [ESO-IPG]
.KEYWORDS       Test of osd routines
.ENVIRONMENT    
.COMMENTS       

-------------------------------------------------------------------------*/

#include   <osfile.h>	
#include   <osdefos.h>	/* Needs to know which Operating System */

static char msg[80];
static char c1[80], c2[80];

#if 0					/* NOT IMPLEMENTED *** */
static struct filestatus info;
#endif

main()
{ 	int i, n, n1, l, n2;
	long int ll;
	int recsize;
	char addctrl = 0;

  puts("Test of osf (File Management) Routines");

  while (1)
  {	printf("Operation: Creat / Del / Rename / Mode / Info / Parse / Supply / Unix: ");
	if (gets(msg) == 0)	break;
	switch(msg[0])
	{ case 'c': case 'C': 		/* Create a File */
		printf("File to Create: "); 	gets(c1);
		printf("VMS options F n or V n: "); gets(msg);
		if (msg[0])	osfop(msg[0], atoi(msg+1));
		printf("Number of bytes: "); 	
		gets(msg); ll = atol(msg);
		printf("Protection (3xoctal): "); 	
		gets(msg); 
		i = (msg[0]-'0') *64 + (msg[1]-'0') *8 + (msg[2]-'0');
		printf("Statut create=%d\n", osfcreate(c1, ll, i));
		puts(osmsg());
		break;

	  case 'd': case 'D':		/* Delete File	*/
		printf("File to Delete: "); 	gets(c1);
		printf("Statut Delete=%d\n", osfdelete(c1));
		puts(osmsg());
		break;

	  case 'r': case 'R':		/* Rename File	*/
		printf("Old Name: "); 	gets(c1);
		printf("New Name: "); 	gets(c2);
		printf("Statut Rename=%d\n", osfrename(c1, c2));
		puts(osmsg());
		break;

	  case 'p': case 'P':		/* Find Elements	*/
		printf("File to parse: ");  gets(c1);
		printf("Path: %s\n", osfparse(c1,_PATH_)); 
		printf("File: %s\n", osfparse(c1,_FILE_)); 
		printf("Type: %s\n", osfparse(c1,_TYPE_)); 
		printf("Vers: %s\n", osfparse(c1,_VERSION_)); 
		break;

	  case 's': case 'S':		/* Change Protection	*/
		printf("File Name with missing items: ");  gets(c1);
		printf("Give File Name Template     : ");  gets(c2);
		printf("Resulting Name: %s\n", osfsupply(c1, c2));
		break;

	  case 'm': case 'M':		/* Change Protection	*/
		printf("File to Change: "); 	gets(c1);
		printf("Protection (3xoctal): "); 	
		gets(msg); 
		i = (msg[0]-'0') *64 + (msg[1]-'0') *8 + (msg[2]-'0');
		printf("Statut chg prot=%d\n", osfcontrol(c1, CHMOD, i, 0));
		puts(osmsg());
		break;

	  case 'u': case 'U':		/* Check Unix compatible	*/
		printf("File to Check: "); 	gets(c1);
		printf("Unix code: %d\n", osfunix(c1));
		puts(osmsg());
		break;

#if 0
	  case 'i': case 'I':		/* Retrieve Info	*/
		printf("File to get Info: "); 	gets(c1);
		i = osfinfo(c1, &info);
		printf("Statut Info=%d\n", i);
		if (i < 0) { puts(osmsg()); break; }
		printf("Info for: %s\n\tsize=%d", 
			info.filename,              /* file name */
			info.filesize);		/* file size in BYTES */
		printf(" date=%d", 
			info.date);           	/* creation date */
		printf(" uid=%X,prot=%3o, ss=%d\n",
			info.owner,			/* owner (UID) id */
			info.protection,		/* protection */
			info.blocksize);		/* sector_size */
		printf("Seconds elapsed since creation: %d\n",
			oshtime(0) - info.date);
		break;
#endif
	  case 'i': case 'I':		/* Retrieve Info	*/
		printf("File to get Info: "); 	gets(c1);
		printf("Date: %d, Size: %d bytes\n",
			osfdate(c1), osfsize(c1));
		break;

	  default: puts("***BAD***"); 
	}
  }
}
