/* @(#)osptest.c	19.1 (ES0-DMD) 02/25/03 14:33:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include  <stdio.h>
#include  <string.h>
#include  <fcntl.h>
#include <sys/types.h>
#include <sys/file.h>
#include  <osparms.h>

#define CHILD "oschild.exe 1 2 3 4 5 6 7"
int in,out;

main()
{
	int i,pid;
	int ospcreate(),ospwait(),ospdelete() /*CGS000:,osprename()*/,ospexit();
	int prep();
/*	char *getname(); */	/* CGS000: not in current version */
	int status;

	if(prep()) {
		printf("Trouble with files; no check\n");
		ospexit(1);
		}
	if((pid=ospcreate(CHILD,CHILD,1,in,out))== -1)  
		printf("Ospcreate for background BAD\n");
	wait(&status);		/* CGS000: needed in this current */
/*	CGS000: Not osprename nor ospinfo in current version.
/*	if(strcmp("proc1",getname(pid)))
/*		printf("Ospinfo BAD...\n");
/*	if(osprename(pid,"proc2")== -1 || strcmp("proc2",getname(pid)))
/*		printf("Osprename BAD...\n");
/*	if(ospdelete(pid)== -1 || getname(pid)!=NULL) 
/*
/*	if(ospdelete(pid)== -1)		/* Doesn't whork very well */
/*		printf("Ospdelete BAD...\n");
/*
/*	CGS000: Only create in background in current version
/*	if((pid=ospcreate(CHILD,"proc3",0,in,out))== -1)
/*		printf("Ospcreate for foreground BAD\n");
*/
	ospexit(0);
	printf("Ospexit BAD\n");
}


/*	CGS000: No osprename, ospinfo in current version.
/*char *getname(pid)
/*int pid;
/*{
/*	static struct prstatus statpnt;
/*	int ospinfo();
/*
/*	if(ospinfo(pid,&statpnt)== -1)
/*		return(NULL);
/*	return(statpnt.procname);
/*}
*/
static char one[]="1";


int prep()
{

	if((out=open("in1",O_WRONLY | O_CREAT,0644))== -1)
		return(1);
	write(out,one,strlen(one));
	close(out);
	if((out=open("out1",O_WRONLY | O_CREAT,0644))== -1 || 
							(in=open("in1",O_RDONLY))== -1)
		return(1);
	return(0);
}
