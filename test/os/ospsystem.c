/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*--------------------------ospsystem--------------------------------------
.TYPE        testdriver
.NAME        ospsystem
.MODULE      osp (operating system independant interface, processes)
.LANGUAGE    C
.AUTHOR      Trond Melen (IPG-ESO Garching)
.PURPOSE
  Execute a command in a shell (The Borne shell under UNIX) or to a 
  command line interpreter (DCL under VMS).
.IMPLEMENTATION
  Loop that reads a commandline and pass it to ospsystem. Type 'logout'
  to exit, 'debug' to turn on debug printouts.
.BUGS
  Typing ^C or ^Y terminates the testdriver. The characters should be
  passed on to the process started by ospsystem().
.RETURNS
  Error status of the command (UNIX only).
----------------------------------------------------------------------*/

#include <stdio.h>

int ospdebug;      /* flag controlling debug printings */

/* A simple test driver */

static int getline(s, lim)  /* get line into s, return length */
char s[];
int lim;
{
  int c, i;

  i = 0;
  while (--lim > 0 && (c = getchar()) != EOF && c != '\n') s[i++] = c;
  /* if (c == '\n') s[i++] = c; */
  s[i] = '\0';
  return(i);
}


main(argc, argv)
int argc;
char *argv[];
{
  char command[80];

  for (;;)
    {
      printf("osp> ");
      (void) getline(command, sizeof(command));
      printf("\n");
      if (strcmp(command, "logout") == 0) break;
      if (strcmp(command, "debug") == 0)
        ospdebug = 1 - ospdebug;
      else
        (void) ospsystem(command);
    }

  ospexit(0);
}
