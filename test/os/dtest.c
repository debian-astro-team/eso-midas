/* @(#)dtest.c	19.1 (ES0-DMD) 02/25/03 14:33:03 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Program
.NAME        dtest.c
.LANGUAGE    C
.AUTHOR      IPG-ESO Garching
.CATEGORY    Stand-alone program to test Disk Access in Raw Mode
.COMMENTS    Tape management. 
		The program writes 2 10k files, separated by a tape-mark.
		The device is given as $DISK
.VERSION 1.0 19-Dec-1989: Creation.
.ENVIRONMENT UNIX
------------------------------------------------------------*/

#define BLKSIZE	1024	
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

char *getenv();

static int iostat(fd)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE Get status.
.RETURNS Not-zero when error
------------------------------------------------------------*/
int fd;                 /* IN:  Tape device file descriptor */
{
        struct stat buf;

        if ( fstat(fd,&buf) == -1) {
                perror("****Can't get Status ! ");
                return(-1);
                }
	printf("....Current position as returned by lseek: %d\n",
		lseek(fd, 0L, 1));

        return(0);
}


main()
{
	int i, n, fd, st;
	static char buffer[10*BLKSIZE] = "Test buffer";
	char	*dname;

    dname = getenv("DISK");
    if (!dname) { printf("DISK is not defined.... Do it !\n"); return; }
    printf("Testing raw device %s\n", dname);

	/* 1) Open the Disk in READ/WRITE mode */
    fd = open(dname, 2);
    if (fd < 0)	{ perror("****Can't open in Read/Write mode"); return; }

	/* 2) Rewind */
    st = lseek(fd, 0L, 0); 
    if (st < 0)	perror("****Failed to Rewind\n");

	/* 3) Write 2 files made of 10 blocks followed by a TM */
    for (n=0; n<2; n++)
    {	for (i=0; i<10; i++)
	{    sprintf(buffer, "Block %d/%d====", n, i);
	     st = write(fd, buffer, BLKSIZE);
	     printf("Written file %d, block %d - Status=%d\n", 
		n, i, st);
	     if (st < 0) perror("****Bad Write: ");
	}
	iostat(fd);
    }

    close(fd);

	/* 4) Reopen the Tape in Read Mode, rewind */
    fd = open(dname, 0);	/* Open in READ mode */
    if (fd < 0)	{ perror("****Can't open in Read mode"); return; }
    st = lseek(fd, 0L, 0); 
    if (st < 0)	perror("****Failed to Rewind\n");

	/* 5) Read Back . Check that 10 are read... */
    for (n=0; n<2; n++)
    {	for (i=0; i < 10; i++)
	{   buffer[0] = '\0';
	    st = read(fd, buffer, BLKSIZE);
	    printf("Read #%d/%d ? %s\n", n, i, buffer);
	    if (st != BLKSIZE) 
		printf("****Found %d-byte block instead of %d-byte\n", 
		st, BLKSIZE);
	}
    }
    printf("\n====Test Program Terminated====\n");
}

