/* @(#)osftest.c	19.1 (ES0-DMD) 02/25/03 14:33:04 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include  <stdio.h>
#include  <string.h>
#include  <osfile.h>
#include  <osparms.h>

#define errprint(x) { fprintf(stderr,x); ospexit(1); }
#define LARGE 250000L
#define SMALL 250L
#define MODEA 0666
#define MODEB 0222
#define MODEC 0444
#define LARGEFILE "osf_lg_file"
#define SMALLFILE "osf_sm_file"
struct filestatus osfstat;
struct lname logn;
/* char *logvar; */
char logvar[] = "VARIABLE";

main()
{
	long osfcreate();
	int osfdelete(),osfcontrol(),osfinfo(),osfrename();
	int osfphname(), osflgname();

/*	printf("A\n"); */
	if(osfcreate(LARGEFILE,LARGE,MODEA)!=LARGE ||
				osfcreate(SMALLFILE,SMALL,MODEB)!=SMALL)
		errprint("Osfcreate BAD on a new file\n");
/*	printf("B\n"); */
	if(osfcreate(LARGEFILE,SMALL,MODEB)!=SMALL)
		errprint("Osfcreate BAD on an old file\n");
/*	printf("C\n"); */
	if(osfdelete(SMALLFILE)== -1)
		errprint("Osfdelete BAD\n");
/*	printf("D\n"); */
	if(osfrename(LARGEFILE,SMALLFILE)== -1)
		errprint("Osfrename BAD\n");
/*	printf("E\n"); */
	if(osfcontrol(SMALLFILE,CHMOD,MODEA)== -1)
		errprint("Osfcontrol BAD\n");
/*	printf("F\n"); */
	if(osfinfo(SMALLFILE,&osfstat)== -1)
		errprint("Osfinfo BAD\n");
/*	printf("G\n"); */
	if(osfstat.filesize!=SMALL ||
		(osfstat.protection & 0777) !=MODEA)
		errprint("Something BAD with osf\n");
/*	printf("H\n"); */
	if(!osfphname(logvar,SMALLFILE))
		errprint("Osfphname BAD\n");
/*	printf("I\n"); */
	if(osflgname("/pub/user1/osf.dat",&logn)== -1 /* ||
		strcmp("/pub/user1",logn.lpath) || strcmp("osf",logn.lfile) ||
		strcmp("dat",logn.ltype )*/)
		errprint("Osflgname BAD\n");
/*	printf("J\n"); */
	ospexit(0);
}
