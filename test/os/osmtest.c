/* @(#)osmtest.c	19.1 (ES0-DMD) 02/25/03 14:33:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include <stdio.h>
#include <osparms.h>

#define errmess(x) { fprintf(stderr,x); }
#define errexit(x) { fprintf(stderr,x); ospexit(1); }
#define LARGE 1000000L
#define PIECE 500000L
/*#define SMALL 200L*/
#define SMALL 200000L
#define LARGEFILE "osm_lg_file"
#define MODEA 0666

main()
{
	long osfcreate();
	int osmopen(),osmclose(),osmunmap(); long osmmap();
	int testfile; long numbytes; char *map;
		int osmalloc(),osmcontrol(),osmfree();
	int ospcreate();
	int osmshget(),osmshrel();
	int testmem;

					/* mapping	*/
/*	printf("A\n"); */
	if(osfcreate(LARGEFILE,LARGE,MODEA)!=LARGE )
		errexit("Osfcreate BAD on a new file\n");
/*	printf("B\n"); */
	if((testfile=osmopen(LARGEFILE,READ_WRITE))== -1)
		errexit("Osmopen BAD on read/write\n");
/*	printf("C\n"); */
/*	if((numbytes=osmmap(testfile,&map,PIECE,0L,IPC_CREAT))!=PIECE) */
	if((numbytes=osmmap(testfile,&map,PIECE,0L,PRIVATE))!=PIECE) 
		errexit("Osmmap BAD\n");
	*(map+PIECE/2)== 'a';
/*	printf("D\n"); */
	if(osmunmap(testfile,map)== -1)
		errmess("Osmunmap BAD\n");
/*	printf("E\n"); */
	if(osmclose(testfile)== -1)
		errmess("Osmclose BAD\n");

					/* normal memory allocation	*/
	printf("F\n"); 
	if(osmalloc(&map,SMALL)== -1)
		errexit("Osmalloc BAD\n");
	*(map+SMALL/2)== 'a';
	printf("G\n");
	if(osmcontrol(map,SMALL,0)== -1)
		errmess("Osmcontrol BAD\n");
/*	printf("H\n"); */
	if(osmfree(map)== -1)
		errmess("Osmfree BAD\n");

					/* shared memory ops	*/
/*	printf("I\n"); */
	if(ospcreate("osmtest1","proc1",1,-1,-1)== -1)
		printf("Ospcreate for background BAD\n");
	sleep(3);
/*	printf("J\n"); */
/*	if((testmem=osmshget(getpid(),PIECE,&map,IPC_CREAT))==-1) */
	if((testmem=osmshget(getpid(),PIECE,&map))==-1)
		errexit("Osmshget BAD in main (osmtest)\n");
	*(map+PIECE/2)== 'a';
/*	printf("K\n"); */
	if(osmshrel(testmem,map)== -1)
	  errmess("Osmshrel BAD\n");
	ospexit(0);
}
