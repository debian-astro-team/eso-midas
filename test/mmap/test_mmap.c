/* @(#)test_mmap.c	19.1 (ES0-DMD) 02/25/03 14:32:58 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>

main()
{
     caddr_t paddr;
     caddr_t addr;
     int len, prot, flags, fd;
     off_t off;

     addr = NULL;
     len = getpagesize();
     prot = PROT_READ;
     flags = MAP_SHARED;
     off = 0;
     if ( (fd = open("./makefile",O_RDWR,0)) == -1 ) {
	perror("TEST_MMAP/OPEN");
	exit(1);
	}
     if ( (paddr = mmap(addr, len, prot, flags, fd, off)) == (caddr_t)-1) {
	perror("TEST_MMAP/MAPPING");
	exit(1);
	}
     exit (0);
}
