#!/bin/sh
# 
# @(#)lutasc.sh	19.1 (ESO-IPG) 02/25/03 14:30:09
# Bourne shell script
# to delete lines 1-3 and last line of ascii table file
# and to remove 1. field
# K. Banse	980928
# 
sed '1,3d
     $d' $1 >temp.temp
# 
# now we use awk to throw out the sequence field
# 
awk '{ printf "                   %s      %s      %s\n", $2, $3, $4 }' temp.temp >temp.final
# 
mv temp.final $2
rm temp.temp
