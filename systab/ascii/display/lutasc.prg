! @(#)lutasc.prg	19.1 (ESO-DMD) 02/25/03 14:30:09
! ++++++++++++++
!  
!  Midas procedure lutasc.prg 
!  to convert Midas LUT to ASCII file which can later be used to regenerate
!  that LUT via CREATE/TABLE
! 
!  xyz.lut  ->  xyz.lasc
! 
!  KB  910315, 980928
! 
! ++++++++++++++
! 
read/table {p1}.lut :RED :GREEN :BLUE >lut.lis
! 
$ sh $MIDASHOME/$MIDVERS/systab/ascii/display/lutasc.sh lut.lis {p1}.lasc
! 
write/out table {p1}.lut converted to {p1}.lasc
