! @(#)ittasc.prg	19.1 (ESO-DMD) 02/25/03 14:30:08
! ++++++++++++++
! 
!  Midas procedure ittasc.prg
!  to convert Midas ITT to ASCII file which can later be used to regenerate
!  that ITT via CREATE/TABLE
!
!  xyz.itt  ->  xyz.iasc
!
!  KB  910315, 980928
!
! ++++++++++++++
!
read/table {p1}.itt :ITT >itt.lis
!
$ sh $MIDASHOME/$MIDVERS/systab/ascii/display/ittasc.sh itt.lis {p1}.iasc
!
write/out table {p1}.itt converted to {p1}.iasc

