#!/bin/sh
# @(#)ittasc.sh	19.1 (ESO-IPG) 02/25/03 14:30:08
# 
# Bourne shell script  ittasc.sh
# to delete lines 1-3 and last line of ascii table file
# and to remove 1. field
# 
sed '1,3d
     $d' $1 >temp.temp
# 
# now we use awk to throw out the sequence field
# 
awk '{ printf "                   %s\n", $2 }' temp.temp >temp.final
# 
mv temp.final $2
rm temp.temp
