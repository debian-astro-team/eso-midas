#!/usr/bin/make -f
# -*- makefile -*-
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include  /usr/share/dpkg/pkg-info.mk

export MIDVERS := $(shell echo '$(DEB_VERSION_UPSTREAM)' | sed -e "s/\([[:digit:]]*\)\.\([[:digit:]]*\).*/\1-\2/;s/-01/JAN/;s/-02/FEB/;s/-03/MAR/;s/-04/APR/;s/-05/MAY/;s/-06/JUN/;s/-07/JUL/;s/-08/AUG/;s/-09/SEP/;s/-10/OCT/;s/-11/NOV/;s/-12/DEC/")
export MIDASHOME = $(shell pwd)
export MID_HOME = $(MIDASHOME)/$(MIDVERS)
export MID_HOME0 = /usr/lib/eso-midas/$(MIDVERS)
export MID_INSTALL = $(MIDASHOME)/$(MIDVERS)/install/unix
export MID_SYS = $(MIDASHOME)/$(MIDVERS)/system/unix/
export MID_WORK = $(MIDASHOME)/midwork
TEST_DIR = $(MIDASHOME)/test_tmp
export DEB_FFLAGS_MAINT_APPEND = -std=legacy -fallow-argument-mismatch
export DEB_CFLAGS_MAINT_APPEND = -fcommon -fno-strict-aliasing -Wno-error=implicit-function-declaration
export DEB_BUILD_MAINT_OPTIONS = optimize=-lto hardening=-fortify

%:
	dh  $@

override_dh_auto_configure:
	mkdir -p $(MIDVERS)
	for i in `ls -d [a-z]* | fgrep -v debian` ; do \
	  cp -alr $$i $(MIDVERS) ; \
	done
	(cd $(MID_HOME); $(MID_INSTALL)/select all)
	(cd $(MID_HOME); $(MID_INSTALL)/preinstall -a)

override_dh_auto_build-arch:
	chmod 755 $(MID_HOME)/local/make_shared
	(cd $(MID_HOME); \
	 $(MID_INSTALL)/install2; \
	 CMND_YES=2 $(MID_INSTALL)/install3 -a)
	test -x $(MID_HOME)/monit/midasgo.exe
	(cd $(MID_HOME)/monit ; make syskeys.unix )
	$(MID_SYS)/inmidas -m $(MID_WORK) -j "@ compile.all"
	$(MID_SYS)/inmidas -m $(MID_WORK) -j "@ ascii_bin no ; bye"

override_dh_install-arch:
	(cd $(MID_HOME) ; yes | $(MID_SYS)/cleanmidas)
	find $(MID_HOME) \( \
		-name "*.a" -o \
		-name "makefile" -o \
	        -name "default.mk" -o \
		-name "*.h" -o \
		-name "*.inc" \) -delete
	rm -rf $(MID_HOME)/libsrc/ftoc*
	find $(MID_HOME) -type d -empty -delete
	sed -e "s:^MIDVERS0=.*:MIDVERS0=$(MIDVERS):" \
	    -e "s:^MIDASHOME0=.*:MIDASHOME0=/usr/lib/eso-midas:" \
	    -i $(MID_HOME)/system/unix/inmidas \
	       $(MID_HOME)/system/unix/helpmidas \
	       $(MID_HOME)/system/unix/drs
	dh_install -X.mod -XCOPYING -Xsetup~ \
	           -X$(MIDVERS)/test -X$(MIDVERS)/install \
	           -X$(MIDVERS)/system/unix/man -X$(MIDVERS)/libsrc/readline

override_dh_fixperms-arch:
	dh_fixperms
	chmod 0644 debian/eso-midas/$(MID_HOME0)/contrib/baches/*/*.fit \
	           debian/eso-midas/$(MID_HOME0)/contrib/baches/*/*.fmt \
	           debian/eso-midas/$(MID_HOME0)/contrib/baches/*/*.datorg \
	           debian/eso-midas/$(MID_HOME0)/contrib/baches/*/*.prg \
	           debian/eso-midas/$(MID_HOME0)/contrib/baches/*/*.README
	find debian/eso-midas/$(MID_HOME0)/ -name \*.sh | xargs chmod 0755 
	chmod 0755 debian/eso-midas/$(MID_HOME0)/util/bench/brun

override_dh_fixperms-indep:
	dh_fixperms
	chmod 0644 debian/eso-midas-testdata/$(MID_HOME0)/test/*/*.prg \
	           debian/eso-midas-testdata/$(MID_HOME0)/test/*/*.mt
	chmod 0755 debian/eso-midas-testdata/$(MID_HOME0)/test/*/*.sh \
	           debian/eso-midas-testdata/$(MID_HOME0)/test/prim/midasparallel \
	           debian/eso-midas-testdata/$(MID_HOME0)/test/prim/midcheck

override_dh_auto_test-arch:
	mkdir -p $(TEST_DIR)
	( cd $(TEST_DIR) ; $(MID_SYS)/inmidas -m $(MID_WORK) -j "@ vericopy ; @@ veriall -nodisplay ; bye" )
	test -f $(MID_WORK)/veriall_*
	rm -rf $(TEST_DIR)

override_dh_makeshlibs:
	# Avoid Lintian complaint: we don't have public shared libs

override_dh_clean:
	dh_clean
	rm -rf $(MIDVERS) $(MID_WORK) $(TEST_DIR)
