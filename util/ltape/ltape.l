.\" @(#)ltape.l	19.1 (ESO-IPG) 02/25/03 14:36:12
.TH LTAPE 1 "26 August 1998"
.SH NAME
ltape \- list headers of FITS and IHAP format files on tape or disk
with optional conversion to ESO-MIDAS format
.SH SYNOPSIS
.B ltape
[
.B \-hnv
]
[
.B \-i
.I input-file
]
[
.B \-d
.I file-list
]
[
.B \-o
.I log-file
]
.if n .ti +.5i
[
.B \-c
.I file-prefix
]
[
.B \-l
.I file-no
]
.IX  "ltape command"  ""  "\fLltape\fP \(em list tape content"
.IX  "tape directory"  ""  "tape directory \(em \fLltape\fP"
.IX  "FITS and IHAP conversion" "" "FITS and IHAP conversion \(em \fLltape\fP"
.IX  "FITS and IHAP headers" "" "FITS and IHAP headers \(em \fLltape\fP"
.SH DESCRIPTION
.LP
The command
.B ltape
reads files in either FITS and IHAP format from disk or tape
and list their headers. Both a one line summary and a full listing
are available. A number of comment fields can be provided and will
be printed in the list header. The file may also be converted to
MIDAS format.
.SH "FUNCTION LETTERS"
.TP
.B h
when given adds the full listing of the FITS header.
.TP
.B n
when given no questions are asked when parameters are missing.
The default values are used in this case. Further, a number of
questions to provide comments is avoided.
.TP
.B v
will list a one line summary of each file on the standard output even
if output list-file or create options are used.
.TP
.B i
specifies that the input should be taken from
.I input-file
which either can be a tape device, a file name or a file prefix.
Tape devices are specified by giving directly a physical device name.
If the device name is of the form 
.I host:device 
or
.I host!device
, ltape reads from a tape device located on a remote host.
A single file name can be
given by including its file extension. Otherwise the name is taken
as a prefix to which a four digit sequence number is added with the
the extension '.mt'.
.TP
.B d
defines that the list of files should giving in the  file
.I file-list
should be read instead of single file or device specified by the -i option.
The file must contain a list of file names separated by new-line chars
e.g. produced by the ls command.
.TP
.B o
defines that the list output will be written to the file
.I log-file
instead of the default which is standard output.
.TP
.B c
defines that the file should be converted to ESO-MIDAS format.
The
.I file-prefix
gives the file prefix used for the ESO-MIDAS file. If a single file
is converted, the prefix will be used as the full name. By default
the 'n' option will be used and no listing be given. If a listing
is required the verbose option may be used.
.TP
.B l
defines that only the files given in the
.I file-no
should be listed. File numbers can be given as a list separated 
with ','. The first file is 1. Ranges are specified with the
first and last separated with either a '\-' or '..'.
The default is all files for tape devices. If 
.I input-file
is the name of an existing file this parameter will be ignored.
.LP
The short one line listing has the following format:
.IP
.B
 No   Identifier     Format   Mb  Axes    Nx    Ny
.br
.B
 -------------------------------------------------
.br
.B
 20 >NO.2201 MZ 3  < F(-32)  1.3,   2:   501   701
.br
.B
 21 >NO.2164 MZ 3  < F(-32)  6.4,   2:  1201  1401
.LP
where No is the relative file number and Identifier its title.
The format field first shows if it is a FITS or IHAP file
by the letter 'F' or 'I'. The data type is given in parenthesis
as the FITS BITPIX value for images (i.e. 8, 16, 32, -32 or -64),
and (ATx)/(BTx) for FITS table extensions. Unknown FITS extensions are
indicated by (--x) while not recognized formats are shown with 'UNKNOWN
FILE FORMAT' in the Identifier field. The approximate size
in Mbytes is given in the Mb column. For images the number of axes
and the size of the two first are listed where as row and column
size is given for tables. A Remark field is provided only to
give space for personal remarks and comments and will not be filled
by the list program.
.br
.ne 5
.SH EXAMPLES
.LP
To list headers of all FITS or IHAP format files on the tape
'tape8mm', one can just issue the command:
.IP
.B
example> ltape -i tape8mm
.br
.B
Name         : Dr. No. Body
.br
.B
Volume/format: NB00123, DSS/DAT
.br
.B
Comments     : Observations of NGC 488
.br
.B
example>
.LP
where several comment information can be given. They will be regarded
as strings and just printed in the header.
.LP
If only the files 1 to 100, 234 and 400 to 600 should be listed
from the tape device /dev/nrst1 on host 'server' and no questions
asked, the following command will do the job:
.IP
.B
example> ltape -i server:/dev/nrst1 -l 1-100,234,400-600 -n
.LP
The FITS file 'my.fits' is converted to ESO-MIDAS format with
the following command:
.IP
.B
example> ltape -i my.fits -c myfile
.LP
The prime data matrix of 'my.fits' file will get the name 'myfile.bdf'
while the first table extension will be named 'myfile.tbl'. If more
than one image or table exist in the FITS-file letters will be appended
to the base name of the file.
.SH FILES
.SB /midas/rels/incl/decompress_dat 
.SH ENVIRONMENT
.SB TAPExxxx
.SB DEVCAPFILE
.SB DECOMPRESS_TABLE
.LP
The physical device name of tape devices may be defined in
environment variables starting with the four letters 'TAPE'.
The properties of tape devices may be defined in the file given
by the environment variable 'DEVCAPFILE'. The environment variable
'DECOMPRESS_TABLE' may point to a file which defines the naming convention
for compressed files.
.SH SEE ALSO
.BR midas,
.BR tapeserver,
.BR intape
.SH BUGS
.LP
