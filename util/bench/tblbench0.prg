! @(#)tblbench0.prg	19.1 (ES0-DMD) 02/25/03 14:35:24
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  MIDAS procedure tblbench0.prg 
!  J.D.Ponz        15 Feb 1988 
! 
!  use as @@ tblbench0 table ncol nrow
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
DEFINE/PAR P1 test   TABLE "Enter Table: "
DEFINE/PAR P2 20     N     "Enter no. of columns: "
DEFINE/PAR P3 10000  N     "Enter no. of rows: "
!
DEFINE/LOCAL FILE/C/1/60  'P1'
DEFINE/LOCAL TIMEA/C/1/40 " " ALL
DEFINE/LOCAL TIMEB/C/1/40 " " ALL
DEFINE/LOCAL LOOP/I/1/1   0
!
WRITE/KEY TIMER/R/1/3     0.,0.,0.
! 
WRITE/OUT 
WRITE/OUT ++++++++++++++++++++++++++++++++++++++++++
WRITE/OUT procedure tblbench0 on 'MID$SYS(1:16)'
WRITE/OUT ++++++++++++++++++++++++++++++++++++++++++
WRITE/OUT 
! 
! ***** table creation *****
! **************************
! 
WRITE/OUT 
! 
COMP/KEY TIMEA = M$TIME()
! 
CREATE/TABLE 'FILE' 'P2' 'P3' NULL 
DO LOOP = 1 'P2'
   CREATE/COL 'FILE' :A'LOOP' 
ENDDO
COMPUTE/TABLE 'FILE' :A0001 = SEQUENCE 
! 
COMP/KEY TIMEB = M$TIME()
@ timediff "'TIMEA(1:40)'" "'TIMEB(1:40)'"
WRITE/OUT ==========================================
WRITE/OUT table creation time = 'TIMER(1)' seconds
WRITE/OUT ==========================================
