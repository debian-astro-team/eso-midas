! @(#)invdemo.prg	19.1 (ES0-DMD) 02/25/03 14:35:23
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: INVDEMO.PRG
!.PURPOSE:        Demo. procedure for INVENTOTY reduction package
!.USE:            Execute as TUTORIAL/INVENT
!                 The following files are copied into your directory :
!                 INFTEST.BDF     --  Test frame
!                 Output files are
! --------------------------------------------------------------------
WRITE/KEY INV_INTG/I/1/12 1,0,1,1,0,0,0,7,1,1,0,0
WRITE/KEY INV_INTG/I/13/11 0,0,0,1,0,20,3,0,0,3,16
WRITE/KEY INV_REAL/R/1/10 1290.,130000.,22.,4.5,.05,-0.05,1.0,18.5,50.,5400.
WRITE/KEY INV_REAL/R/11/10 21.11,3.0,5.0,0.26,0.33,0.39,0.43,0.46,0.36,0.24
WRITE/KEY INV_REAL/R/21/10 0.19,0.15,0.14,0.14,0.14,0.14,0.14,0.14,0.14,0.14
WRITE/KEY INV_REAL/R/31/10 0.14,0.14,0.14,0.14,0.14,0.14,0.14,0.14,3.0,5.0
WRITE/KEY INV_REAL/R/41/10 2.0,4.0,4.5,0.25,0.6,0.02,0.6,1.0,0.0,2.5
WRITE/KEY INV_REAL/R/51/10 1.2,0.0,1.39,0.0,0.0,0.0,0.0
SEARCH/INV invtest2 tstest2
ANALYSE/INV invtest2 tstest2 tatest2 ver debug
CLASSIFY/INV tatest2

