/* @(#)ben_rread.c	19.1 (ES0-DMD) 02/25/03 14:35:21 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        ben_rread
.LANGUAGE    C
.AUTHOR      CARLOS GUIRAO- IPG-ESO Garching
.CATEGORY    OS benchmarks. Random disk read benchmark.
.COMMENTS    This benchmark checks the disk read time through
             the os interface using random access.

.REMARKS     The ben_osdwrite benchmark must run before running 
             this benchmark.
.REMARKS     This benchmarck is included in the shell script: "brun".
.REMARKS     Use the "make" command to update the benchmark. 
.VERSION     1.0 10-Feb-1988   Implementation     CGS
------------------------------------------------------------*/
#include <osparms.h>

#ifdef ERRNO
extern int oserror;
#else
#define ERRNO
int oserror;
#endif

static long randx = 1;

/* #define BLOCKS 256 */
#define BLOCKS 10000		
#define BLKSIZE 2048
#define TOTAL (BLOCKS * BLKSIZE)
/* Size File: BLOCKS*BLKSIZE 10000*2048 = 20MBytes */ 

main(argc,argv)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE     Random read from disc.
.RETURNS     Upon succesful execution of the command 0 status is
             returned, or 1 in case of error.
.REMARKS     The ben_osdwrite benchmark must run before running 
             this benchmark.
.REMARKS     System dependencies:
             OS: osfdelate(), osdopen(), osdseek(), osdread();
------------------------------------------------------------------*/
int argc;
char **argv;
{
	int osfdelate(), osdopen();
	long osdseek(), osdread();
	long random();

	char buffer[BLKSIZE];
	register int i;
	int fildes;
	long int offset;

	if (argc < 2) {
		printf("Usage: %s filename\n",argv[0]);
		ospexit(1);
		}

	if ((fildes = osdopen(argv[1], READ)) < 0) {
		printf("Cannot find '%s'. Run 'osdwrite' first.\n",argv[1]);
		ospexit(1);
		}

	for (i=0 ; i<BLOCKS; i++) {
		offset = random() * (long)(TOTAL/(1024*32));
		if (osdseek(fildes, offset, FILE_START) != offset) {
			printf("OSDESEEK to %ld FAILED i=%d",offset,i);
			ospexit(1);
			}

		if (osdread(fildes, buffer, BLKSIZE) < 0L) {
			printf("OSDREAD ERROR reading block at byte %ld\n",offset);
			ospexit(1);
			}
		}	

	if (osfdelete(argv[1]) <0 ) {
		printf ("OSFDELATE FAILED\n");
		ospexit(1);
		}
	ospexit(0);
}

/*
*/
long random()
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE     Random generator.
.RETURNS     A random number .
------------------------------------------------------------------*/
{	
	return(((randx = randx * 1103515245L + 12345) >> 16) & 077777);
}
