#! /bin/sh
# @(#)multi.sh	19.1 (ESO-IPG) 02/25/03 14:35:24
loop=$1
i=0
while [ $i -lt $loop ]
do
	/bin/sh ./tst.sh $i &
	i=`expr $i + 1`
done;
wait
