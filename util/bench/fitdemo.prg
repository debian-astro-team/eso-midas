! @(#)fitdemo.prg	19.1 (ES0-DMD) 02/25/03 14:35:22
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!      Demo. procedure for FITTING procedure. Name : FITDEMO.PRG
!      Execute as TUTORIAL/FIT
!      The following files are copied into your directory :
!           init.FIT          -- Original function
!           FUNCTOIN.FIT      -- function to be fitted
!      The procedure creates the working files
!           REF.BDF, test.BDF, NOISE.BDF, profile.BDF, fitted.BDF
!
!    -  Files will be deleted after demo.
!
!      J.D.Ponz 		ESO - Munich    version 1.0  April. 16 , 1985
!                		ESO - Munich            2.0  January 13, 1986
!      Ph.Defert                ESO-ST/ECF              3.0  February 20,1986
!
! ---------------------------------------------------------------
SET/PLOT  BIN=ON
!
WRITE/OUT **************************************************
WRITE/OUT This\demo\will\show\how\to\handle\functions
WRITE/OUT in\the\context\of\non\linear\fitting.
WRITE/OUT The\procedure\will\create\1-D\frame\consisting
WRITE/OUT of\2\gaussians\on\top\of\a\non-linear\background,
WRITE/OUT with\some\additional\noise
WRITE/OUT The\fitting\is\done\using\different\methods
WRITE/OUT **************************************************
!
WRITE/OUT This\is\the\original\function:
WRITE/OUT READ/FIT\init
READ/FIT  init
!
WRITE/OUT Now\create\a\reference\image
WRITE/OUT CREATE/IMAGE\ref\1,200\1.,1.
CREATE/IMAGE ref 1,200 1.,1.
WRITE/OUT Create\the\function\values\using\reference\image
WRITE/OUT COMPUTE/FUNCTION\test\=\init(ref)
COMPUTE/FUNCTION test = init(ref)
WRITE/OUT PLOT\test
PLOT         test
!
WRITE/OUT COMPUTE/IMAGE\profile\=\test+0
COMPUTE/IMAGE profile = test+0.
!
WRITE/OUT The\plotted\values\are\used\as\input\for\the\fit
WRITE/OUT PLOT\profile
PLOT         profile
WRITE/OUT The\function\to\be\fitted\is
WRITE/OUT READ/FIT\function
READ/FIT     function
WRITE/OUT Note\that\the\guesses\include\constrains
!
SET/FIT METHOD=NR
WRITE/OUT SHOW/FIT\\\\\(to\see\qualifiers\of\the\FIT\command)
SHOW/FIT
!
WRITE/OUT Fit\the\function
WRITE/OUT Method:NR\(Newton-Raphson,\default)
WRITE/OUT Print=1\(default)\:\impresions\at\each\iteration
WRITE/OUT FIT/IMAGE\11,1.,0.5\profile\function
FIT/IMAGE 11,1.,0.5 profile function
!
!
WRITE/OUT Finally,\compute\the\fitted\values
WRITE/OUT COMPUTE/FIT\fitted\=\function
COMPUTE/FIT fitted = function
WRITE/OUT OVER\fitted
OVER      fitted
!
WRITE/OUT   Show\the\two\components\plus\background
WRITE/OUT   SELECT/function function 1,3
SELECT/FUN  function 1,3
WRITE/OUT   COMPUTE/FIT fit1 = function
COMPUTE/FIT fit1 = function
WRITE/OUT   SELECT/FUNCTION function 2,3
SELECT/FUN  function 2,3
WRITE/OUT   COMPUTE/FIT fit2 = function
COMPUTE/FIT fit2 = function
WRITE/OUT   PLOT        profile
PLOT        profile
WRITE/OUT   OVER        fit1
OVER        fit1
WRITE/OUT   OVER        fit2
OVER        fit2
WRITE/OUT   Select\all\components...
WRITE/OUT   SELECT/FUNCTION function ALL
SELECT/FUN  function ALL
!
