! @(#)mmfbench0.prg	19.1 (ES0-DMD) 02/25/03 14:35:23
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  MIDAS procedure mmfbench0.prg 
!  J.D.Ponz        15 Feb 1988 
! 
!  use as @@ mmfbench0 frame ny nx
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
DEFINE/PAR P1 test   IMAGE "Enter Image: "
DEFINE/PAR P2 64     N     "Enter no. of lines: "
DEFINE/PAR P3 30000  N     "Enter no. of samples: "
!
DEFINE/LOCAL TIMEA/C/1/40 " " ALL
DEFINE/LOCAL TIMEB/C/1/40 " " ALL
!
WRITE/KEY TIMER/R/1/3     0.,0.,0.
! 
WRITE/OUT 
WRITE/OUT ++++++++++++++++++++++++++++++++++++++++++
WRITE/OUT procedure mmfbench0 on 'MID$SYS(1:16)'
WRITE/OUT ++++++++++++++++++++++++++++++++++++++++++
WRITE/OUT 
! 
! ***** frame creation *****
! **************************
! 
WRITE/OUT 
! 
COMP/KEY TIMEA = M$TIME()
! 
@@@ fdummy 'P1' 2,'P3','P2' 0,1,0,1 10
! 
COMP/KEY TIMEB = M$TIME()
@ timediff "'TIMEA(1:40)'" "'TIMEB(1:40)'"
WRITE/OUT ==========================================
WRITE/OUT Frame creation time = 'TIMER(1)' seconds
WRITE/OUT ==========================================
! 
! ***** Add noise to frame *****
! **************************
! 
WRITE/OUT 
! 
COMP/KEY TIMEA = M$TIME()
! 
@@@ surface 'P1' 0,0 NO 5,0,0,0
! 
COMP/KEY TIMEB = M$TIME()
@ timediff "'TIMEA(1:40)'" "'TIMEB(1:40)'"
WRITE/OUT ==========================================
WRITE/OUT Noise add time = 'TIMER(1)' seconds
WRITE/OUT ==========================================
! 
! ***** Add objects to frame *****
! **************************
! 
WRITE/OUT 
! 
COMP/KEY TIMEA = M$TIME()
! 
DEFINE/LOCAL  ILOOP/I/1/2 0,0
DO ILOOP(2) = 10 'P2' 20
   DO ILOOP(1) = 100 'P3' 1000
      @@@ surface 'P1' 'ILOOP(1)','ILOOP(2)' GA 1000,3
   ENDDO
ENDDO
! 
COMP/KEY TIMEB = M$TIME()
@ timediff "'TIMEA(1:40)'" "'TIMEB(1:40)'"
WRITE/OUT ==========================================
WRITE/OUT Object add time = 'TIMER(1)' seconds
WRITE/OUT ==========================================
