! @(#)casdemo.prg	19.1 (ES0-DMD) 02/25/03 14:35:22
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!      Demo. procedure for CASPEC reduction. Name : CASDEMO.PRG
!      Execute as TUTORIAL/CASPEC
!      The following files are copied into your directory :
!           CASFF.BDF     -- Flat Field
!           CASTH.BDF     -- Comparison
!           CASSTD.BDF    -- Standard Star
!           CASOBJ.BDF    -- Object
!      The procedure creates the working tables
!           ORDER.TBL  LINE.TBL
!      Output files are
!           Work space   Response    Flatfield corr.  Red. spectrum
!           MID*.BDF      -- Work space
!        Standard Reduction :
!           RESPONSE.BDF  -- Instrument response
!           FLATCORR.BDF  -- Flat Field correction
!           CASPEC.BDF    -- Reduced spectrum
!           CAS0001.BDF to CAS0016.BDF   -- Individual Orders
!           CASPEC3.BDF   -- Reduced spectrum multiplied by a suitable factor
!        Alternate reduction
!           GROSS.BDF     -- Gross spectrum
!           BACK.BDF      -- Background
!           NET.BDF       -- Net spectrum
!           RNET.BDF      -- Ripple corrected using SINC method
!           CASPEC1.BDF   -- Reduced spectrum using SINC method
!           CASPEC2.BDF   -- Reduced spectrum using OVERLAY method
!
!    -  The reduction requires cursor interaction to identify
!       at least two lines located to the right and to the left
!       of the comparison 2D spectrum on the image display.
!       Use Wavelengths:  5760.55, in order 98 and 6583.91, in order 87
!
!    -  Files will be deleted after demo.
!
!      J.D.Ponz + E.Brinks		ESO - Munich     Feb. 12 , 1986
!
! ---------------------------------------------------------------
!
! CREA/COMM CALIBR/CASPEC @@ CASCALI.PRG
assign/plot term nofile
SET/PLOT XAXIS=AUTO
SET/PLOT YAXIS=AUTO
! SET/CURSOR
WRITE/OUT Initialize\context
WRITE/OUT SET/CONTEXT ECHELLE
SET/CONTEXT ECHELLE
WRITE/OUT SHOW/ECHELLE
SHOW/ECHELLE
!
WRITE/OUT Define\relevant\parameters
WRITE/OUT SET/ECHELLE\FLAT=CASFF\WLC=CASTH\WIDTH1=6\THRES1=5\THRES2=400
SET/ECHELLE  FLAT=CASFF WLC=CASTH WIDTH1=6 THRES1=5 THRES2=400
SET/ECHELLE  GUESS=TEST
!
WRITE/OUT And\execute\wavelength\calibration\procedure
WRITE/OUT CALIBRATE/CASPEC
CALIBRATE/CASPEC
!
! Compute Flat-Background
CLEAR/CHAN OVER
SHOW/ECHELLE
WRITE/OUT Compute\flat\field\correction\file
WRITE/OUT SET/ECHELLE\SAMPLE=0.15\SLIT=5
SET/ECHELLE   SAMPLE=0.15 SLIT=5
WRITE/OUT FLAT/CASPEC
FLAT/CASPEC
!
WRITE/OUT Compute\Response
WRITE/OUT SET/ECHELLE\FLUX=LTT1020\STD=CASSTD\BACOBJ=0,0\RESPOL=2
WRITE/OUT RESPONSE/CASPEC
SET/ECHELLE   FLUX=LTT1020  STD=CASSTD BACOBJ=0,0 RESPOL=2
RESPONSE/CASPEC
!
WRITE/OUT Check\standard\star
WRITE/OUT MERGE/ECHELLE\&\STDSPEC\3.0\AVE
MERGE/ECHELLE & STDSPEC 3.0 AVE
CUTS STDSPEC 0,200
WRITE/OUT PLOT\STDSPEC
PLOT STDSPEC
WRITE/OUT OVER/TABLE\LTT1020\:WAVE\:FLUX
OVER/TABLE LTT1020 :WAVE :FLUX
!
! If there is different FF+WLC for object the commands
! CALIBRATE/CASPEC and FLAT/CASPEC have to be used here
!
WRITE/OUT Finally\Reduce\the\Object
WRITE/OUT LOAD/IMAGE\CASOBJ\0
WRITE/OUT     SAMPLE=RESPONSE
SET/ECHELLE   SAMPLE=RESPONSE
WRITE/OUT REDUCE/CASPEC\CASOBJ\CASPEC
LOAD/IMAGE    CASOBJ 0
REDUCE/CASPEC CASOBJ CASPEC
!
WRITE/OUT and\get\individual\orders
WRITE/OUT MERGE/ECHELLE\&\CAS\7,8\NOAVERAGE
MERGE/ECHELLE & CAS 7,8 NOAVERAGE
WRITE/OUT MERGE/ECHELLE\&\CAS\14,15\NOAVERAGE
MERGE/ECHELLE & CAS 14,15 NOAVERAGE
!
WRITE/OUT Display\some\results\on\graphics\terminal\and\VERSATEC
WRITE/OUT Plot\of\merged\spectrum
SET/PLOT  BIN=ON
SET/PLOT  XAXIS=5700,6700,100
SET/PLOT  YAXIS=0,20,10
PLOT      CASPEC
SEND/PLOT LASERA
!
WRITE/OUT Plot\orders\7\and\8\to\show\the\overlapping\region
SET/PLOT  XAXIS=6050,6150,10
SET/PLOT  YAXIS=0,20,10
PLOT      CAS0007
OVER      CAS0008
SEND/PLOT LASERA
!
WRITE/OUT Plot\orders\14\and\15\to\show\the\overlapping\region
SET/PLOT  XAXIS=6500,6600,10
SET/PLOT  YAXIS=0,20,10
PLOT      CAS0014
OVER      CAS0015
SEND/PLOT LASERA
!
