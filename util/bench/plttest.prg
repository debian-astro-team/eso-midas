! @(#)plttest.prg	19.1 (ES0-DMD) 02/25/03 14:35:24
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!      Name : PLTDEMO.PRG
!      Demonstartion procedure for the plot package. 
!      Execute as TUTORIAL/PLOT
!      The following files should exist UGC.TBL in MID_TEST
!                                        
!      Copy the file UGC.TBL into your directory
!
!      Rein Warmels  ESO - Garching April. 25 , 1988
!-----------------------------------------------------------------------
! -copy mid_test:ugc.tbl tugc.tbl
set/plot default
!
assign/plot term nofile
set/plot pmode=1
set/plot xaxis=0,1.0,0.5,0.5 yaxis=0,1.5,0.5,0.5
plot/tab ugc #1 #2
!
write/out There\are\a\number\line\types\and\fonts\available:
echo/on
over/lin 1 0.1,0.1 0.6,0.1
label/graphic "Line type 1" 0.8,0.1 1
over/line 2 0.1,0.2 0.6,0.2
label/graphic "Line type 2" 0.8,0.2 1
over/line 3 0.1,0.3 0.6,0.3 
