/* @(#)cetime.c	19.1 (ES0-DMD) 02/25/03 14:35:59 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

cetime(pa)   float *pa; { c_etime(pa); }
_cetime(pa)  float *pa; { c_etime(pa); }
cetime_(pa)  float *pa; { c_etime(pa); }
_cetime_(pa) float *pa; { c_etime(pa); }
CETIME(pa)   float *pa; { c_etime(pa); }
_CETIME(pa)  float *pa; { c_etime(pa); }
CETIME_(pa)  float *pa; { c_etime(pa); }
_CETIME_(pa) float *pa; { c_etime(pa); }

#ifdef SYSV_V2 		/* For pure SYSTEM_V systems */
#include <sys/types.h>
#include <sys/times.h>
static struct tms buff;
#ifndef CLK_TCK
#define CLK_TCK 60
#endif
c_etime(pa) 
float *pa;
{
	times(&buff);
	pa[0] = (float)buff.tms_utime/(float)CLK_TCK;
	pa[1] = (float)buff.tms_stime/(float)CLK_TCK;
}
#else  			/* For BSD systems */
#include <sys/time.h>
#include <sys/resource.h>
static struct rusage rusage;
c_etime(pa) 
float *pa;
{
	getrusage(RUSAGE_SELF, &rusage);
	pa[0] = (float)rusage.ru_utime.tv_sec + (float)rusage.ru_utime.tv_usec/(float)1000000;
	pa[1] = (float)rusage.ru_stime.tv_sec + (float)rusage.ru_stime.tv_usec/(float)1000000;
}
#endif
