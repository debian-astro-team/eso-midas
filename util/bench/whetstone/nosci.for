C @(#)nosci.for	19.1 (ES0-DMD) 02/25/03 14:36:00
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
c	"nosci" and "noscf" are nosc routines which
c	write run timing results to file "{directory_name}results"
c	in the current directory.  The environment
c	file "../environ",  containing the compiler
c	names and options, is copied to "*results",
c	along with any local "environ" file.  This local
c	"environ" defines info unique to the individual
c	benchmark , e.g. hspice.  The current date and 
c	wall clock time is also written to "*results".
c	The Timings are then referenced to our "baseline" machine.
c
c	The "*results" file is always appended to which
c	gives a benchmark history for this benchmark
c	using different compiler options.
c
c
c	"iu" is unit to use when writing to "*results".
c	Unit "iu+1" is also used to read in "environ" and "baseline" 
c	files.
c
c	Usage: call nosci(iu)		Initalize "results" file.
c	       call noscf()		Finalize "results".
c


	subroutine nosci(iu)
c	  This should be the 1st thing the program does!
c	  set up file "results" and initialize timing

	character*24 fdate
	character*10 envir
	character line*80,path*80
	character *16 group,name
	character *25 results
	real t1(2),t2(2)
	external fdate
	save iout,t1,group
	iout=iu
c
c	get directory name
	call getcwd(path)
	i=1
c	strip path name from directory
	last=index(path,' ')-1
	do 40 j=1,6
	  ii=index(path(i:last),'/')
	  if(ii.lt.1)go to 45
	  i=i+ii
40	continue
45	continue
	results=path(i:last)//'results'
c
c	open the 'results' file and position to end of file.
c	if file not there, create it.
c
	
	open(iout,file=results,status='old',err=20)
c	file exists - position to end of file
5	read(iout,*,end=30,err=5) line(1:1)
	go to 5
c	'results' doesn't exist - open new file
20	open(iout,file=results,status='new',err=300)
	call chmod(results,'ugo+rw')
30	continue
	write(iout,*)'*********************************'
	write(iout,*)path(i:last),'  ',fdate()
	group=path(i:last)
	in=iout + 1
c	copy "../environ" and "./environ" (if one exists)
c	files to "./*results" file
	envir = 'environ'
	ie = 1
	do 55 i=1,2
	  open(in,file=envir(ie:10),status='old',err=55)
47	  read(in,49,end=50)line
49	  format(a80)
	  write(iout,*)line
	  go to 47
50	  close(in,err=65)
55	  ie=4
60	call etime(t1)
65	return
c
c
c	****************** NOSCF ***********************	
	entry noscf()
c	This should be the last thing the program does!
c	get elapsed time for run and write to "*results"
	call etime(t2)
	u=t2(1)-t1(1)
	s=t2(2)-t1(2)
	t=u+s
	write(iout,*)'Total elapsed time (sec) =',t,' system  ='
     *  ,s,' user =',u
c
c	***********************************************
c	reference to baseline   '../baseline'
	in=iout+1
	open (in,file='../baseline',status='old',err=90)
80	read(in,*,end=90,err=90)name,bt,bs,bu
	if (name.ne.group)go to 80
	bt1=0.
	bs1=0.
	bu1=0.
	if(t.ne.0.) bt1=bt/t
	if(s.ne.0.) bs1=bs/s
	if(u.ne.0.) bu1=bu/u
	write(iout,*)'BASELINED   total =',bt1,'  system =',bs1,
     1	             '  user =',bu1
90	return

c	  Come here on errors!
300	continue
c	call perror('nosci error')
	end
