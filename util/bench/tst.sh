#! /bin/sh
# @(#)tst.sh	19.1 (ESO-IPG) 02/25/03 14:35:25
func1()
{
#echo -n "a"
sort >sort.$$ <</*EOF
And
now
let's
see
how 
fast
is
this
text
treated
in
between
their
colleges
running
at
the
same
time
/*EOF
od sort.$$ | sort -n +1 > od.$$
grep the sort.$$ | tee grep.$$ | wc > wc.$$
cat sort.$$ | sed 's/e/E/' | awk -FA '{ print $1 }' >awk.$$
rm sort.$$ grep.$$ od.$$ wc.$$ awk.$$
#echo -n "A"
}

func2()
{
#echo -n "b"
cp ben_fpu.c fpu$$.c
cc -c fpu$$.c
cc fpu$$.o  -lm -o fpu$$
strip fpu$$
rm fpu$$.c fpu$$.o fpu$$
#echo -n "B"
}

func3()
{
#echo -n "c"
cp ben_fpu.exe fpu$$
fpu$$ 1 >/dev/null
rm fpu$$
#echo -n "C"
}

order=`expr $1 % 4`
case $order in
0)
   func1
   func2
   func3
   ;;
1)
   func2
   func1
   func3
   ;;
2)
   func3
   func1
   func2
   ;;
3)
   func3
   func2
   func1
   ;;
*)
   echo "ERROR. This line should not be printed"
   ;;
esac
