! @(#)mmfbench.prg	19.1 (ES0-DMD) 02/25/03 14:35:23
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  MIDAS procedure mmfbench.prg 
!  P.Grosbol        1988 Oct 08
! 
!  use as @@ mmfbench frame ny nx
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
DEFINE/PAR P1 test  IMAGE "Enter Image: "
DEFINE/PAR P2 128   N     "Enter no. of lines: "
DEFINE/PAR P3 30000 N     "Enter no. of samples: "
!
WRITE/OUT  MMF search benchmark
!
!  Create test frame
!
@@ mmfbench0 'P1' 'P2' 'P3'
!
!  Perform search of objects in frame
!
@@ mmfbench1 'P1' 
