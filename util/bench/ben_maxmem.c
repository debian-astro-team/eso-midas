/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        ben_maxmem
.LANGUAGE    C
.AUTHOR      CARLOS GUIRAO- IPG-ESO Garching
.CATEGORY    OS benchmarks. Check Maximum Memory Allocation.
.COMMENTS    This benchmark checks the maximum virtual memory 
             allocation of the system per procces.

.REMARKS     This benchmarck is included in the shell script: "brun".
.REMARKS     Use the "make" command to update the benchmark. 
.VERSION     1.0 25-Mar-1988   Implementation     CGS
051021		last modif

------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <sys/signal.h>

int i;
int interr();

main()
{
	char *pbuf;

	i=1;
	signal(SIGINT,interr);
	signal(SIGQUIT,interr);

	while(i++) {
		if (!(i%0x400))
			printf("%d Kbytes\n",i); 
		if ((pbuf = (char *)malloc((size_t)(0x400*i))) == (char *)0 ) { 
			printf("Max. memory allocation: %d Kbytes\n",i);
			exit(0);
			}
		free(pbuf);
		}
	exit(0);
}

interr()
{
		printf("Max. memory allocated: %d Kbytes\n",i);
		exit(0);
}	
