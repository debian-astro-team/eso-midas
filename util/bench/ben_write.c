/* @(#)ben_write.c	19.1 (ES0-DMD) 02/25/03 14:35:21 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        ben_write
.LANGUAGE    C
.AUTHOR      CARLOS GUIRAO- IPG-ESO Garching
.CATEGORY    OS benchmarks. Disk write benchmark.
.COMMENTS    This benchmark checks the disk write time through
             the os interface.

.REMARKS     This benchmark must run before running the ben_dread
             benchmark.
.REMARKS     This benchmarck is included in the shell script: "brun".
.REMARKS     Use the "make" command to update the benchmark. 
.VERSION     1.0 10-Feb-1988   Implementation     CGS
------------------------------------------------------------*/
#include <osparms.h>

#ifdef ERRNO
extern int oserror;
#else
#define ERRNO
int oserror;
#endif

/* #define BLOCKS 256 */
#define BLOCKS 10000		
#define BLKSIZE 2048
/* Size File: BLOCKS*BLKSIZE 10000*2048 = 20MBytes */ 

main(argc,argv)
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE     Write data on disc.
.RETURNS     Upon succesful execution of the command 0 status is
             returned, or 1 in case of error.
.REMARKS     This benchmark must run before running the ben_rread
             o ben_sread benchmark.
.REMARKS     System dependencies:
             OS: osfcreate(), osdopen(), osdclose(), osdwrite();
------------------------------------------------------------------*/
int argc;
char **argv;
{
	int osfcreate(), osdopen(), osdclose();
	long osdwrite();

	char buffer[BLKSIZE];
	register int i;
	int fildes;

	if (argc < 2) {
		printf("Usage: %s filename\n",argv[0]);
		ospexit(1);
		}

	if (osfcreate(argv[1], sizeof(buffer)*BLOCKS, 0640) < 0) {
		printf("OSFCREATE FAILED.\n");
		ospexit(1);
		}
	else {
		if ((fildes = osdopen(argv[1],READ_WRITE)) < 0) {
			printf("OSDOPEN FAILED.\n");
			ospexit(1);
			}
		}

	for (i=0 ; i<BLOCKS; i++) {
		if (osdwrite(fildes, buffer, sizeof(buffer)) < 0) {
			printf("OSDWRITE FAILED BLOCK %d\n",i);
			ospexit(1);
			}
		}

	if (fsync(fildes) < 0) {
		printf("FSYNC FAILED.\n");
		ospexit(1);
		}

	if (osdclose(fildes) < 0) {
		printf("OSDCLOSE FAILED.\n");
		ospexit(1);
		}

	ospexit(0);
}
