! @(#)benchmarki.prg	19.1 (ES0-DMD) 02/25/03 14:35:22
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  benchmarki.prg      procedure to execute the different benchmarks
!                      for images
!  K. Banse	880329, 930716
!
!  use as @@ benchmarki Size
!  then all benchmarks will be done with an image `bench1' of SizexSize pixels
!  Size is defaulted to 1000
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/par p1 1000 C "Enter size of benchmark image: "
! 
@@ bench1.time {p1}		!create the image
!
@@ bench2.time 			!do compute/image stuff
! 
@@ bench4.time 			!filtering
! 
@@ bench3.time 			!FFT test
