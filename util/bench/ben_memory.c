/* @(#)ben_memory.c	19.1 (ES0-DMD) 02/25/03 14:35:21 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE        Module
.NAME        ben_memory
.LANGUAGE    C
.AUTHOR      CARLOS GUIRAO- IPG-ESO Garching
.CATEGORY    OS benchmarks. Memory benchmark.
.COMMENTS    This benchmark read and write main memory in order
             to point RAM acces.

.REMARKS     This benchmarck is included in the shell script: "brun".
.REMARKS     Use the "make" command to update the benchmark. 
.VERSION     1.0 10-Feb-1988   Implementation     CGS
------------------------------------------------------------*/

#define ITERATION 4000
#define BUFFER 1000

struct ram {
              int feld1 [BUFFER];
              int feld2 [BUFFER];
              int feld3 [BUFFER];
              int feld4 [BUFFER];
           };
struct ram feld;
int i,j;

/*
*/
main()
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE     Ram test; read and write main memory.
.RETURNS     Always 0 status is returned.
------------------------------------------------------------------*/
{
    fill ();   comp ();
    exit(0);
}

fill ()
{  
   for (j=0;j<ITERATION;j++)
      for (i=0;i<BUFFER;i++)
        {
          feld.feld1[i] = i;
          feld.feld2[i] = i;
          feld.feld3[i] = i;
          feld.feld4[i] = i;
        }
}

comp ()
{
   long int k;
   k = 0;
   for (j=0;j<ITERATION;j++)
      for (i=0;i<BUFFER;i++)
         {
           k = k + feld.feld1[i];
           k = k + feld.feld2[i];
           k = k + feld.feld3[i];
           k = k + feld.feld4[i];
         }
}
