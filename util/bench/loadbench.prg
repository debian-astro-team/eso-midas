! @(#)loadbench.prg	19.1 (ES0-DMD) 02/25/03 14:35:23
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! ++++++++++++++++++++++++++++++++++++++++++++
! 
! benchmark procedure 'loadbench.prg'
! K. Banse      891023
! 
! use via @@ loadbench loop_count 1_image 2_image
!     loop_count defaulted to 1
!     1_image defaulted to itest
!     2_image defaulted to sombrero
! 
! +++++++++++++++++++++++++++++++++++++++++++
! 
DEFINE/PAR P1 1 N "Enter number of iterations: "
DEFINE/PAR P2 itest I "Enter name of 1. image: "
DEFINE/PAR P3 sombrero I "Enter name of 2. image: "
! 
DEFINE/LOCAL LOOP/I/1/1  'P1'
DEFINE/LOCAL N/I/1/1  0
DEFINE/LOCAL RESULT/R/1/2  0.,0.
DEFINE/LOCAL TIMEA/C/1/40 " " ALL
DEFINE/LOCAL TIMEB/C/1/40 " " ALL
! 
WRITE/KEY TIMER/R/1/3 0.,0.,0.
! 
CLEAR/DISP
DISP/LUT OFF
!
! here the loops:
! --------------  
! 
DO N = 1 'LOOP'
   COMP/KEY TIMEA = M$TIME()
   LOAD/IMA 'P2' 
   COMP/KEY TIMEB = M$TIME()
   @ timediff "'TIMEA(1:40)'" "'TIMEB(1:40)'"
   RESULT(1) = TIMER(1) + RESULT(1)
!
   COMP/KEY TIMEA = M$TIME()
   LOAD/IMA 'P3' 
   COMP/KEY TIMEB = M$TIME()
   @ timediff "'TIMEA(1:40)'" "'TIMEB(1:40)'"
   RESULT(2) = TIMER(1) + RESULT(2)
ENDDO
! 
WRITE/OUT loadbench.prg with 'LOOP' iterations:
WRITE/OUT LOAD/IMAGE 'P2' ...
TIMER(1) = RESULT(1) / LOOP
WRITE/OUT time = 'TIMER(1)' seconds
WRITE/OUT -------------------------  
! 
WRITE/OUT LOAD/IMAGE 'P3' ...
TIMER(2) = RESULT(2) / LOOP
WRITE/OUT time = 'TIMER(2)' seconds
WRITE/OUT -------------------------  
