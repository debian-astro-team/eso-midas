! @(#)tblbench1.prg	19.1 (ES0-DMD) 02/25/03 14:35:24
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  MIDAS procedure tblbench1.prg 
!  J.D.Ponz        15 Feb 1988 
! 
!  use as @@ tblbench table niter 
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
DEFINE/PAR P1 test TABLE "Enter Table: "
DEFINE/PAR P2 5    N     "Enter no. of iterations: "
! 
DEFINE/LOCAL FILE/C/1/60 'P1'
DEFINE/LOCAL TIMEA/C/1/40 " " ALL
DEFINE/LOCAL TIMEB/C/1/40 " " ALL
DEFINE/LOCAL LOOP/I/1/1 0
!
WRITE/KEY    TIMER/R/1/3      0.,0.,0.
! 
WRITE/OUT 
WRITE/OUT ++++++++++++++++++++++++++++++++++++++++++
WRITE/OUT procedure tblbench1 on 'MID$SYS(1:16)'
WRITE/OUT ++++++++++++++++++++++++++++++++++++++++++
WRITE/OUT 
! 
! ***** compute *****
! *******************
! 
WRITE/OUT 
! 
COMP/KEY TIMEA = M$TIME()
! 
DO LOOP = 1 'P2'
   COMPUTE/TABLE   'FILE' #2 = #1+10.
   COMPUTE/TABLE   'FILE' #3 = 20000.-#1
   COMPUTE/TABLE   'FILE' #4 = #1*0.018
   COMPUTE/TABLE   'FILE' #5 = SIN(#4)
   COMPUTE/TABLE   'FILE' #6 = COS(#4)
   COMPUTE/TABLE   'FILE' #7 = #6*#6+#5*#5
ENDDO
! 
COMP/KEY     TIMEB = M$TIME()
@ timediff "'TIMEA(1:40)'" "'TIMEB(1:40)'"
TIMER(2) = TIMER(1)/'P2'			!average over all iterations
!
STATIS/TABLE 'FILE' #7 0.1 0. 2.
WRITE/OUT =========================================
WRITE/OUT compute/table time = 'TIMER(2)' seconds
WRITE/OUT =========================================
