! @(#)tbldemo.prg	19.1 (ES0-DMD) 02/25/03 14:35:25
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
!
!      Demo. procedure for table files. Name : TBLDEMO.PRG
!      Execute as TUTORIAL/TABLE
!      The following files should exist UGC.DAT and UGC.FMT
!      Copy the files UGC.* into your directory
!
!      ESO - Munich     Sept. 16 , 1982
!
! $ COPY MID_TEST:UGC.DAT TUGC.DAT
! $ COPY MID_TEST:UGC.FMT TUGC.FMT
ECHO/ON
WRITE/OUT Create table file
CREATE/TABLE TUGC 10 700 TUGC TUGC   ! create the table file
NAME/COL TUGC #1 F11.3         ! change format
NAME/COL TUGC #2 G12.6         ! change format
SHOW/TABLE   TUGC                ! display structure
READ/TABLE   TUGC  @1 @30        ! display a few entries
!
! WRITE/OUT Plot blue and red diameters
! PLOT/TABLE  TUGC :DR :DB       ! plot diameters in red and blue bands
!
WRITE/OUT Compute regression coefficients
REGR/LINEAR TUGC :DB :DR       ! linear regression on these variables
READ/KEY    OUTPUTD           ! and display stored coefficients
!
WRITE/OUT Compute distribution of apparent magnitudes
READ/HIST   TUGC :BT           ! display results on terminal
! PLOT/HIST   TUGC :BT           ! and plot device
!
WRITE/OUT Select galaxies brighter than 13.5 magnitude
SELECT/TAB TUGC :BT.LT.13.5    ! select brightest objects
!
WRITE/OUT Compute distribution of red diameters on the subset
STAT/TAB   TUGC :DR            ! do statistics on the subset,
READ/HIST  TUGC :DR            ! display the result
!
! WRITE/OUT and display the subset
! PLOT/TAB   TUGC :BT :RV        ! and plot the selected set
!
WRITE/OUT Select new subset with rad.vel. greather than 4000 Km/s
SELECT/TAB TUGC :RV.GT.4000.0  ! select a new subset with largest rad.vel.
!
WRITE/OUT Print subset
PRINT/TAB  TUGC                ! print them
!
WRITE/OUT    Compute absolute blue magnitude
COMPUTE/TAB TUGC :MBT = :BT-25.-5.*LOG10(:RV/50)       ! compute abs.magnitude
NAME/COL    TUGC :MBT "ABS.B.MAG."                     ! include units
!
WRITE/OUT    Compute linear diameter in Kpc
COMPUTE/TAB TUGC :SIZE = :RV*SIN(0.000291*:DB)*20      ! diameter
NAME/COL    TUGC :SIZE "KPC"                           ! include units
!
WRITE/OUT    Plot linear size as function of absolute magnitude
! PLOT/TAB   TUGC :MBT :SIZE                            ! display result
ECHO/OFF
! $ DELETE TUGC.DAT.
! $ DELETE TUGC.FMT.
! $ DELETE TUGC.TBL.
