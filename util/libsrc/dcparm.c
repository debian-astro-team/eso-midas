/*===========================================================================
  Copyright (C) 1994-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.LANGUAGE    C
.IDENT       dcparm.c
.AUTHOR      Preben J. Grosbol [ESO/IPG]
.KEYWORDS    parameter decoding, command line
.ENVIRON     UNIX
.VERSION     1.0   1990-Jul-12 : Creation,     PJG
.VERSION     1.1   1990-Aug-24 : Add gvparm function,     PJG
.VERSION     1.2   1991-Aug-30 : Check for agrc<=0, PJG
.VERSION     1.4   1994-Ovt-09 : Ckeck for wrong options, PJG
------------------------------------------------------------------------*/
static  char  *cnull = "";            /* pointer to NULL string         */

int dcparm(argc,argv,plist,pval)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE     Decode options and parameters on the command line using
             standard UNIX syntax.
.COMMENT     Each option/flag and parameter is defined in a string
             where the first character is the letter for the flag and
             the second indicates the type '-' for flag and '+' for
             addition string expected. After the parameter definition
             field as comment can be given starting with the delimeter ':'.
             If a parameter is not given it's value in a NULL popinter.
             When a flag without string is specified the pointer  points
             to a NULL string.
             Example:    command  -x -i name -o file
             A parameter list    plist[] = {"x-:Execute flag",
                                            "v-:Verbose flag",
                                            "i+:Input file",
                                            "o+:Output file",(char *) 0}
             would give an output pval[] = {"",(char *) 0,"name","file"}
.RETURN      Status where 0:OK, -1:Error
------------------------------------------------------------------------*/
int        argc;          /* parameter count                            */
char     **argv;          /* pointers to parameters                     */
char   *plist[];          /* pointers to parameter definitions          */
char    *pval[];          /* pointers to parameters strings             */

{
  char  c, *pc, *pl;
  int   n, err;

  pl = cnull;                                     /* initiate variables */
  err = 0;
  for (n=0; plist[n]; n++) pval[n] = (char *) 0;
  if (argc<=0) return err;

  argv++;                                  /* skip over command name    */
  if (--argc != 0)                         /* decode parameters         */
    while (argc--) {
       pc = *argv++;
       if (*pl=='+') { pval[n] = pc; pl = cnull; continue; }	    
       if (*pc++ == '-')
          while (c = *pc++) {
	     n = 0;
	     while ((pl=plist[n]) && (*pl!=c)) n++;
             if (pl++) { 
                if (*pl!='+') pval[n] = cnull;
                else { 
                  if (*pc) { pval[n] = pc; pl = cnull; }
                  break;
                }
	      }
	     else { pl = cnull; err = -1; }
	   }
     }

  return err;
}

char *gvparm(opt,plist,pval)
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.PURPOSE     Retrieve parameter value for option specified
.COMMENT     The arrays 'plist' and 'pval' are the same as in
             routine cdparm().
.RETURN      Pointer to value string, NULL pointer if not available
------------------------------------------------------------------------*/
char        opt;          /* option for parameter                       */
char   *plist[];          /* pointers to parameter definitions          */
char    *pval[];          /* pointers to parameter strings              */

{
  while (*plist && **plist!=opt) plist++, pval++;

  return ((*plist) ? *pval : (char *) 0);
}
