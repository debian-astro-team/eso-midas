.\" @(#)bdf2ps.l	19.1 (ESO-IPG) 02/25/03 14:35:14
.TH BDF2PS l "09 November 1994"
.SH NAME
bdf2ps \- convert MIDAS-bdf files to PostScript format
.SH SYNOPSIS
.B bdf2ps
[
.B \-wbneavfNE
]
[
.B \-i
.I bdf-file
]
[
.B \-o
.I ps-file
]
.if n .ti +.7i
[
.B \-l
.I spec-file
]
[
.B \-p
.I page-format
]
[
.B \-r
.I resolution
]
.if n .ti +.7i
[
.B \-c
.I lut
]
[
.B \-s
.I xscale,yscale
]
[
.B \-z
.I lcut,hcut
]
.IX  "bdf2ps command"  ""  "\fLbdf2ps\fP \(em MIDAS-bdf to PostScript"
.IX  "Postscript"  ""  "Hardcopy \(em \fLbdf2ps\fP"
.IX  "Hardcopy" "" "Postscript file \(em \fLbdf2ps\fP"
.SH DESCRIPTION
.LP
The command
.B bdf2ps
read a single MIDAS-bdf file or a specification file with a set of
images and create a PostScript file depending on the options.
.SH "FUNCTION LETTERS"
.TP
.B w
add an intensity wedge to the right of each frame.
.TP
.B b
use a blue background color instead of white.
.TP
.B v
will list the origin and scale parametrs for each frame.
.TP
.B n
use a negative ITT instead of a positive ramp.
.TP
.B e
use an exponential ITT instead of a linear.
.TP
.B a
autoscale bdf-file to fit into display area.
.TP
.B N
no frame identifier will be included.
.TP
.B f
draw a frame box around the image.
.TP
.B E
create a single Encapsulated PostScript file of a MIDAS bdf-file.
This option makes a one-to-one mapping from image to PostScript
pixels. Thus, the i option must be used while the options 'plsbaNf'
are ignored.
.TP
.B i
specifies that the input should be taken from
.I bdf\-input
which must be in the standard MIDAS-bdf format.
.TP
.B o
defines the name of the output file
.I ps-file
which is in Encapsulated PostScript format. The default name is 'psimage.ps'.
.TP
.B l
defines the name of an input specification file
.I spec-file
which must contain a set of lines each with display information for 
a single bdf-file or text. The format of a line is: "I: <bdf-file>
<xorigin,yorigin> <xscale,yscale> <xsize,ysize> <xcent,ycent>" for images 
or "T: <"text"> <xorigin,yorigin,size>" for text strings
(see note for example). The 'origin' and 'size' are given in cm while
scale in world coordinates per cm. The 'cent' is in world coordinates
and defines the image point which should be displayed in the middle
of the rectangular of 'size' and with lower left corner at 'origin' on
the page.
.TP
.B p
defines the 
.I page-format
which can be one of the following: A4P, A4L, A3P, A3L, S4P and S4L.
The first two letters define the page size as A3/A4 or S4 where
the latter is for Slide format being A4 with wider margins. The last
letter is P or L for portrait or landscape, respectively. The default
is A4P.
.TP
.B r
specifies the
.I resolution
of the printer in pixels per cm. If this parameter is 0 or not given
the printer default is used.
.TP
.B c
defines that a color PostScript file should be produced with the given
.I lut
which either can be a number 0,1,2,3 for a set of build-in LUT's or a
name of a MIDAS table containing a LUT.
.TP
.B s
specifies the scaling of the image as
.I xscale,yscale
in world coordinates per cm. By default the image will be scaled to
fill the full available area with xscale=yscale. If zero or negative
scales are given a value is taken depending on the printer resolution
as to display individual pixels of the input frame.
.TP
.B z
defines the scaling in intensities by
.I lcut,hcut
being the low and high cut values. If not specified, the values from the
LHCUTS descriptor are used.
.LP
The PostScript file can be viewed with a viewer (e.g. Gostscript) or
printed out on a PostScript printer with the 'lpr' command. A color
PostScript file requires a color printer to come out correct.
.LP
.SH EXAMPLES
.LP
A single image in 'galaxy.bdf' can be converted in to the
color PostScript file 'galaxy.ps' using the internal LUT no. 3.
by the connand:
.IP
.B
example> bdf2ps -i galaxy -o galaxy.ps -c3 -wn
.LP
A negative ITT is also used and a color wedge is added to the right
of the image.
.LP
The MIDAS bdf-file 'n2997.bdf' is converted in an Encapsulated
PostScript file using a negative ITT by the connand:
.IP
.B
example> bdf2ps -i n2997 -o n2997.eps -E -n
.LP
The output file 'n2997.eps' can then be enbedded in other documents.
.LP
Several bdf-files can be converted into a single PostScript file
using the specification option.
.IP
.B
example> bdf2ps -l viewg.vg -o viewg.ps -e
.LP
The viewg.vg may contain the following lines:
.IP
.B
I: ngc3223 8.0,9.0  50.0,50.0  12.0,9.0  0.0,0.0
.br
.B
I: ngc5247 16,20.0  22,22  4,4 8,9
.br
.B
T: "This is a test text"  10.3,2.0,10
.br
.B
I: m81   2.3,3.2  12.0,12.0  6.0,6.0  0.0,0.0
.LP
which will convert the three images and add a text. An exponential
ITT will be used.
.LP

.SH FILES
.LP

.SH ENVIRONMENT
.LP

.SH SEE ALSO
.BR midas,
.BR copy/display
.SH BUGS
.LP
The command is very unforgiving! It wants all the values in a specification
file and with reasonable values. You better check that the scale you
give is really in world coordinates per cm.
