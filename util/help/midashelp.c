/* @(#)midashelp.c	19.1 (ES0-DMD) 02/25/03 14:36:06 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)midashelp.c	19.1 (ES0-DMD) 02/25/03 14:36:06 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT     (c)  1988   European Southern Observatory
.IDENT         midashelp.c
.LAUGUAGE      C
.AUTHOR        P.Grosbol   ESO/IPG
.KEYWORDS      MIDAS help file, TeX
.PURPOSE       Routine reads directory list of MIDAS help-files and
               generates a file with TeX commands for printing.
.VERSION       1.0  1988-Dec-04 : Creation,   PJG 
.VERSION       1.1  1990-Apr-23 : Correct allowed characters,   PJG 
---------------------------------------------------------------------*/
#include  <stdio.h>

main(argc,argv)
int       argc;
char    **argv;
{
  int   c,cpf;
  FILE  *inf,*outf;

  if ((inf=fopen("midashelp.dat","r")) == NULL) {
     fprintf(stderr,"Error: Cannot open input file"); exit(1);
  }
  if ((outf=fopen("midashelp.tex","w")) == NULL) {
     fprintf(stderr,"Error: Cannot open output file"); exit(1);
  }

  fprintf(outf,"%%+\n%% midashelp.tex\n%%-\n"); /* write file header */
  fprintf(outf,"\n\\input midashelp.mac\n\n");  /* include TeX macro */

  cpf = 0;                                  /* copy flag false       */
  while ((c=getc(inf)) != EOF) {            /* go through whole file */
     if (cpf) {                             /* copy file name        */
        if (c<=' ' || c==';' || 'z'<c) {    /* end of file name      */
           cpf = 0; fprintf(outf,"}\n");
        }
        else putc(c,outf);
     }
     else if (c=='[' || c=='/' || c=='.') { /* start of file name    */
        cpf = 1;
        fprintf(outf,"\\typesethelp{%c",c);
     }
  }

  fprintf(outf,"\n\\bye");                    /* terminate TeX input */
  fclose(inf); fclose(outf);
  exit(0);
}
