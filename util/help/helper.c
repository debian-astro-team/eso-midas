/* @(#)helper.c	19.1 (ES0-DMD) 02/25/03 14:36:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)helper.c	19.1 (ES0-DMD) 02/25/03 14:36:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.TYPE		Program
.NAME		thtest.c
.LANGUAGE	C
.AUTHOR		Francois Ochsenbein [ESO-IPG]
.CATEGORY	Help 
.COMMENTS	Test of th (hierarchical help) routines
.ENVIRONMENT	TermWindows
.VERSION  2.1	(Version '2' of TermWindows)
.VERSION  2.2	02-Jul-1987: Merging with TermDisplay.
.VERSION  2.3	02-Dec-1987: Usage of WhileInput
------------------------------------------------------------*/

#define	PM_LEVEL	2

#include <twhelp.h>
#include <trace.h>
#include <macrogen.h>
#include <pgm.h>

static TWHELP *help;

static char zero = 0;
static char one  = 1;

static int ReadKey(name, index, items, result)
/*+++++++
.PURPOSE Simplified interface for SCKRD
		are found in the text.
.RETURNS 
--------*/
	char *str;	/* IN: Math. symbol */
	int  len;	/* IN: Length */
{
  return(tx_out(str, len));
}

static int mymath(str, len)
/*+++++++
.PURPOSE Specify what to do when the special LaTeX characters $ ^ _
		are found in the text.
.RETURNS 
--------*/
	char *str;	/* IN: Math. symbol */
	int  len;	/* IN: Length */
{
  return(tx_out(str, len));
}

/*===========================================================================*/
main()
/*+++++++
.PURPOSE Just test the Help until the Interrupt
.RETURNS To Midas
--------*/
{
	int	ef[3];	/* Error flags */
	int	status;
	char	ch, *logfile, *hfile;
	static  char file[40];

		/* Start Midas Prologue, and set Error Flags  */
  SCSPRO("Helper");
  SCECNT("GET", ef, ef+1, ef+2);
  SCECNT("PUT", &one, &zero, &zero);
  
		/* Don't log */
  
  if_not (InitWindows((char *)0, (char *)0, -1)) goto FIN;

  hfile = GetNextParm();
  if (!hfile)	hfile = "midas.ind";	/* Head file with indexes */
  help = InitHelp(hfile, NULL_WINDOW, NULL_WINDOW, NULL_WINDOW) ;
  hfile = "echelle/midas.ind";		/* Special Context	*/
  AddHelp(help, hfile);

  SetHelpTitleOption(help, 1);		/* Use title from Help File	*/
  
		/* Tell that / are used as topic separators */
        SetHelpSeparators(" /");
		/* Define what to do in case of Math. Symbols */
  	tx_math(mymath);

  	Help(help);
  
  EndWindows();
		/* Reset Error Status, and return to Midas */
  FIN:
  SCECNT("PUT", ef, ef+1, ef+2);
  SCSEPI();
  ospexit(0);
}
