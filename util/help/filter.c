/* @(#)filter.c	19.1 (ES0-DMD) 02/25/03 14:36:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)filter.c	19.1 (ES0-DMD) 02/25/03 14:36:05 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*++++++++++++++
.IDENTIFICATION filter.c
.LANGUAGE       C
.AUTHOR         ESO-IPG, Garching
.ENVIRONMENT    Help Files
.KEYWORDS      	Help Files
.VERSION  1.0   22-Nov-1990
.COMMENTS       Parameter = 1st word
---------------*/

#include <str.h>

main(argc, argv)
/*++++++++++++++++
.PURPOSE  Process a .hlp
.RETURNS  to OS
.REMARKS  stdout output
-----------------*/
    int argc;	/* Count */
    char **argv; /* IN: Parameters = Context */
{
    char *co1;	/* First word of Command */
    char *context, *ct1, *ct2;
    char line[512], topic[80], command[80], *version;
    char *b, buffer[8000];
    int  i, lco;

    ct1 = ct2 = context = "";
    if (argc > 1) {
	context = *++argv, argc--;
	ct1 = " \\tab [";	ct2 = "]";
	*context = toupper(*context);
    }
    topic[0] = 0;

    b = buffer; *b = ' ';
    while (gets(line)) {
	i = strindex(line, "\\co");
	if (line[i] && ((line[i+3] == '\0') || isspace(line[i+3]))) {
		line[i] = '\0'; 
		if (buffer[0])	b += strcopy(b, line), *(b++) = '\n';
		else		puts(line);
		if (topic[0])	printf("\\eos{%s}\n\n", topic);
		gets(line);	/* Line with Title + Version */
		lco = strspan(line,_GRAPH_);
		version = line + lco;
		version += strspan(version, _SPACE_);
		line[lco] = '\0';	/* Line is now just full command */
		i = strloc(line, '/');
		oscopy(command, line, i); command[i] = 0; co1 = command;
    		if (!topic[0]){	/* Initialize the current command */
			printf("\\Help{0}{%s}{%s ======== %s%s%s\\hfill %s}{}\n", 
				co1, co1, ct1, context, ct2, version);
			*b = 0;
			printf("%s", buffer);
			buffer[0] = 0;
		}
		if (line[i]) {	/* There is a Qualifier */
			strcopy(topic, line+i+1);
			printf("\\Help{1}{%s}{%s%s%s%s\\hfill %s}{}\n", 
				topic,line, ct1,context,ct2,version);
		}
		puts("%++++++");
		printf("%%.IDENTIFICATION %s\n", line);
		printf("%%.VERSION  1.0   %s\n", version);
		puts("%------");
		continue;
	}
	if (buffer[0])	b += strcopy(b, line), *(b++) = '\n';
	else	puts(line);
    }
    if(topic[0]) printf("\\eos{%s}\n\n", topic);
}

