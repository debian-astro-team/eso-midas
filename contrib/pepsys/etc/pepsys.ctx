! @(#)pepsys.ctx	19.1 (ES0-DMD) 02/25/03 13:28:30
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION     pepsys.ctx
!.MODULE             midas context procedure
!.LANGUAGE           Midas Command language
!.AUTHOR             A.T.Young
!.PURPOSE            setup and start pepsys context. 
!.COMMENT            use:  set/context pepsys 
!.VERSION            1.0.0, 31-Mar-92
!--------------------------------------------------------
!
!HELP = contrib
!
!SET/MIDAS USER=EXPERT
!
!	Create PEPSYS commands:
!
CREATE/COMMAND MAKE/HORFORM                @c horform
CREATE/COMMAND MAKE/STARTABLE              @c starfile
CREATE/COMMAND MAKE/PHOTOMETER             @c instr
CREATE/COMMAND MAKE/PLAN                   @c plan
CREATE/COMMAND CONVERT/PHOT                @c dcon
CREATE/COMMAND REDUCE/PHOT                 @c reduce
!
!
WRITE/OUT "**************** Context PEPSYS enabled *****************"
!
WRITE/OUT
WRITE/OUT
WRITE/OUT "        The following commands are now available:"
WRITE/OUT
WRITE/OUT
WRITE/OUT "-----------------------  setup commands  -----------------------"
WRITE/OUT
WRITE/OUT "MAKE/PLAN       makes a detailed photometric observing schedule"
WRITE/OUT
WRITE/OUT "MAKE/STARTABLE  makes a new star table from an ascii file"
WRITE/OUT
WRITE/OUT "MAKE/PHOTOMETER makes a new instrument description table"
WRITE/OUT
WRITE/OUT "MAKE/HORFORM    makes a form to fill in for horizon table"
WRITE/OUT
WRITE/OUT "CONVERT/PHOT    converts ESO or Danish raw data to table format"
WRITE/OUT
WRITE/OUT "----------------------  reduction command  ---------------------"
WRITE/OUT
WRITE/OUT "REDUCE/PHOT     reduces photometric observations"
WRITE/OUT
WRITE/OUT "----------------------------------------------------------------"
WRITE/OUT
