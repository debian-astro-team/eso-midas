C @(#)driver.for	19.1 (ES0-DMD) 02/25/03 13:29:07
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)driver.for	19.1  (ESO-IPG)  02/25/03  13:29:07
         PROGRAM DRIVER
C
C       tests subroutines converted to MIDAS style
C
C
      IMPLICIT REAL (A-H,O-Z)
      IMPLICIT INTEGER (I-N)
C
      COMMON /FILNOS/ KB, KTV, K2, K3, K4, K7, K8, K9
C
        CHARACTER*40 STRING
C
C               in MIDAS:  RUN DRIVER   (to test subroutines)
C
        CALL STSPRO ('DRIVER')
        KB=5
        KTV=6
C
        CALL ASKFIL('Enter a lower-case filename:',STRING)
        CALL TV('Filename was read as:')
        CALL TVN(STRING)
        CALL ASK('Enter a string:',STRING)
        CALL TV('String was read as:')
        CALL TVN(STRING)
        CALL QF('Enter a real number:',VALUE)
        WRITE(STRING,'(G10.4)') VALUE
        CALL TV('value was read as: '//STRING)
        CALL STSEPI
        END
