/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT       eso50.c
.LANGUAGE    C
.AUTHOR      P. Grosbol ESO/IPG
.KEYWORDS    ESO 50cm, photometry, data file
.ENVIRON     MIDAS
.PURPOSE     Read data file from the ESO 50cm and convert it into a
             table conforming to the MIDAS PEPSYS context
.VERSION     1.0  1995-Jul-27 : Creation, PJG

 090804		last modif
------------------------------------------------------------------------*/
#include   <stdlib.h>                /* Standard C library              */
#include   <stdio.h>                 /* Standard I/O definitions        */
#include   <math.h>                  /* Mathematical macros             */
#include   <string.h>                /* String macros                   */
#include   <midas_def.h>             /* MIDAS definitions               */

#define    MXBUF                128  /* Max size of line buffer         */



int main()
{
  char     *pc, rflag, sign, acs[3];
  char     star[5], stat[8], dia[5], obj[10], band[9];
  char     fname[61], tname[61], text[MXBUF], buf[MXBUF];
  int      idt, err, na, iva;
  int      nerr, nrow, ncom, nint, nlim, nband;
  int      ncobj, nccoun, ncra, ncdec, ncexp, ncmjd, ncoffr, ncoffd;
  int      nccom, ncsigm, nctemp, ncdtemp, ncslim, ncband, ncstar;
  int      nclst, ncdia, ncstat, ncnlim, ncac, nczd, ncftemp, ncnband;
  int      rah, ram, ras, ded, dem, des, sth, stm, sts;
  float    exp, zd, ac, lst, ra, dec, dtemp, temp, ftemp;
  float    slim, count, sigm, offr, offd;
  double   mjd;
  FILE     *fpi;

  SCSPRO("ESO50");                     /* initiate MIDAS environment    */

/*  read FRAME name and open it  */

  err = SCKGETC("IN_A", 1, 60, &iva, fname);
  if (!(fpi=fopen(fname,"r")))
    SCETER(1, "Error: cannot input data file");

/*  count no. of photometric data readings file contains   */

  rflag = '\0';
  nrow = nerr = ncom = nint = 0;
  while ((pc=fgets(text, MXBUF, fpi)))
    switch (*pc) {
       case 's' : rflag = 's'; nint++; break;
       case 'f' : rflag = 'f'; nerr++; break;
       case 'c' : rflag = 'c'; ncom++; break;
       case ' ' : if (rflag=='s') nrow++; break;
       default  : break;
     }
  sprintf(text,"Input file >%s<: %4d intg, %4d meas, %4d com, %4d err",
	  fname, nint, nrow, ncom, nerr);
  SCTPUT(text);

/* get output table name and create it  */

  err = SCKGETC("OUT_A", 1, 60, &iva, tname);

  err = TCTINI(tname, F_TRANS, F_O_MODE, 48, nrow, &idt);
  err = TCCINI(idt, D_C_FORMAT, 32, "A10",  "", "OBJECT",      &ncobj);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F11.6","", "RA",          &ncra);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F11.5","", "DEC",         &ncdec);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F11.1","", "SIGNAL",      &nccoun);
  err = TCCINI(idt, D_C_FORMAT,  4, "A4",   "", "STARSKY",     &ncstar);
  err = TCCINI(idt, D_C_FORMAT,  8, "A8",   "", "BAND",        &ncband);
  err = TCCINI(idt, D_R8_FORMAT, 1, "F12.5","", "MJD_OBS",     &ncmjd);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F8.1", "", "EXPTIME",     &ncexp);
  err = TCCINI(idt, D_C_FORMAT, 32, "A16",  "", "COMMENT",     &nccom);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F5.2", "", "SKYOFFSETRA", &ncoffr);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F5.2", "", "SKYOFFSETDEC",&ncoffd);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F6.2", "", "ERROR",       &ncsigm);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F6.1", "", "DETTEMP",     &ncdtemp);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F6.1", "", "FILTTEMP",    &ncftemp);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F6.1", "", "DOMETEMP",    &nctemp);
  err = TCCINI(idt, D_C_FORMAT,  4, "A4",   "", "DIAPHRAGM",   &ncdia);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F11.5","", "LST",         &nclst);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F5.1", "", "AC",          &ncac);
  err = TCCINI(idt, D_C_FORMAT,  7, "A7",   "", "STATUS",      &ncstat);
  err = TCCINI(idt, D_I4_FORMAT, 1, "I3",   "", "NLIM",        &ncnlim);
  err = TCCINI(idt, D_I4_FORMAT, 1, "I3",   "", "NBAND",       &ncnband);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F5.2", "", "SIGLIM",      &ncslim);
  err = TCCINI(idt, D_R4_FORMAT, 1, "F5.2", "", "ZDIST",       &nczd);

/*  read input file line by line   */

  fseek(fpi, 0, 0);                        /* rewind file to start      */

  rflag = '\0';
  nrow = 1;
  while ((pc=fgets(text, MXBUF, fpi)))
    switch (*pc) {
       case 's' : rflag = 's';
                  strncpy(buf, &text[6], 11); buf[11] = '\0';
                  mjd = atof(buf) + 40000.0 - 0.5;
                  strncpy(buf, &text[18], 7); buf[7] = '\0';
                  sscanf(buf,"%2d%2d%3d",&sth,&stm,&sts);
                  lst = sth + stm/60.0 + sts/36000.0;
                  strncpy(stat, &text[26], 7); stat[7] = '\0';
                  dia[0] = stat[1]; dia[1] = '\0';
                  nband = ('0'<=stat[0] && stat[0]<='9') ?
		    stat[0]-'0' : stat[0]-'A'+10;
                  strncpy(acs, &text[33], 2); acs[2] = '\0';
                  if (*acs!='-') ac = strtod(acs, (char **)NULL);
                  strncpy(buf, &text[36], 3); buf[3] = '\0';
                  slim = atof(buf);
                  strncpy(buf, &text[40], 3); buf[3] = '\0';
                  exp = atof(buf);
                  strncpy(obj, &text[44], 9); obj[9] = '\0';
                  star[0] = text[54];
                  if (star[0]=='*') strcpy(star, "STAR");
                   else strcpy(star, "SKY");
                  strncpy(buf, &text[57], 8); buf[8] = '\0';
                  sscanf(buf,"%2d%2d%4d",&rah,&ram,&ras);
                  ra  = rah + ram/60.0 + ras/360000.0;
                  sign = text[65];
                  strncpy(buf, &text[66], 7); buf[8] = '\0';
                  sscanf(buf,"%2d%2d%3d",&ded,&dem,&des);
                  dec = ded + dem/60.0 + des/36000.0;
                  if (sign=='-') dec = -dec;
                  strncpy(buf, &text[74], 4); buf[4] = '\0';
                  zd = 0.01*atof(buf);
                  break;
       case ' ' : if (rflag!='s') break;
                  na = 1;
                  while(text[na]==' ' && na<9) na++;
                  strncpy(band, &text[na], 9-na); band[9-na] = '\0';
                  if (strstr(band,"328")) strcpy(band,"u");
                  else if (strstr(band,"329")) strcpy(band,"v");
                  else if (strstr(band,"330")) strcpy(band,"b");
                  else if (strstr(band,"331")) strcpy(band,"y");
                  else if (strstr(band,"332")) strcpy(band,"betaN");
                  else if (strstr(band,"339")) strcpy(band,"betaW");
                  else if (strstr(band,"DARK")) strcpy(band,"DARK");
                  strncpy(buf, &text[9], 9); buf[9] = '\0';
                  count = atof(buf);
                  strncpy(buf, &text[19], 4); buf[4] = '\0';
                  sigm = 0.01 * atof(buf);
                  strncpy(buf, &text[23], 3); buf[3] = '\0';
                  nlim = atol(buf);

                  if (40 < (int) strlen(text)) {
		     strncpy(buf, &text[27], 4); buf[4] = '\0';
		     ftemp = dtemp = atof(buf);
		     strncpy(buf, &text[32], 4); buf[4] = '\0';
		     temp = atof(buf);
		     TCEWRR(idt, nrow, nctemp, &temp);
		     TCEWRR(idt, nrow, ncdtemp, &dtemp);
		     TCEWRR(idt, nrow, ncftemp, &ftemp);
		  }

                  TCEWRC(idt, nrow, ncobj,   obj);
		  TCEWRC(idt, nrow, ncstar,  star);
		  TCEWRR(idt, nrow, nccoun,  &count);
		  TCEWRR(idt, nrow, ncsigm,  &sigm);
		  TCEWRC(idt, nrow, ncband,  band);
		  TCEWRR(idt, nrow, ncoffr,  &offr);
		  TCEWRR(idt, nrow, ncoffd,  &offd);
		  TCEWRC(idt, nrow, ncstat,  stat);
		  TCEWRC(idt, nrow, ncdia,   dia);
		  TCEWRR(idt, nrow, ncexp,   &exp);
		  TCEWRR(idt, nrow, ncra,    &ra);
		  TCEWRR(idt, nrow, ncdec,   &dec);
		  TCEWRD(idt, nrow, ncmjd,   &mjd);
		  TCEWRR(idt, nrow, nclst,   &lst);
		  TCEWRR(idt, nrow, ncslim,  &slim);
		  TCEWRR(idt, nrow, ncsigm,  &sigm);
		  TCEWRR(idt, nrow, nczd,    &zd);
		  TCEWRI(idt, nrow, ncnlim,  &nlim);
		  TCEWRI(idt, nrow, ncnband, &nband);
		  if (*acs!='-') TCEWRR(idt, nrow, ncac,   &ac);
                  nrow++;
                  break;
       case 'f' : rflag = 'f'; break;
       case 'c' : rflag = 'c'; break;
       default  : break;
     }

/*  close files and exit    */

  fclose(fpi);
  TCSINI(idt); TCTCLO(idt);
  SCSEPI();
return 0;
}
