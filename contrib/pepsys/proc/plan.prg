! @(#)plan.prg	19.1 (ES0-DMD) 02/25/03 13:29:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLAN.PRG
!.PURPOSE:        Help user make up a photometric observing schedule
!.USE:            MAKE/PLAN
!.AUTHOR:         Andrew T. Young
! ----------------------------------------------------------------------
!
! ********** DEFINE LOCAL KEYWORDS **********
!
DEFINE/LOCAL reply/C/1/10 "?" A
DEFINE/LOCAL obstbl/C/1/80 "esotel.tbl"
!
! ********* BEGIN EXECUTION  **********
!
!
WRITE/OUT " "
WRITE/OUT "The MAKE/PLAN command makes a photometric observing schedule."
WRITE/OUT " "
WRITE/OUT "     (You can reply HELP or QUIT to most questions)"
Start:
WRITE/OUT " "
WRITE/OUT " "
WRITE/OUT "You will need the following files:"
WRITE/OUT " "
WRITE/OUT "       Observatory table file (esotel.tbl)"
!WRITE/OUT " "
!WRITE/OUT "       Horizon-obstruction table"	! (not yet)
WRITE/OUT " "
WRITE/OUT "       Standard-star file(s)"
WRITE/OUT " "
WRITE/OUT "       Program-star file(s)  (optional)"
WRITE/OUT " "
WRITE/OUT "       Instrument-description file"
WRITE/OUT " "
WRITE/OUT " "
INQUIRE/KEYW reply "Do you have all the required files in your working directory?"
!
IF reply(:1) .eq. "y" THEN	! user claims all files exist
!
    WRITE/OUT " "
!
ELSEIF reply(:1) .eq. "n" THEN	! offer some help...
!
    GetFile:
    WRITE/OUT " "
    WRITE/OUT " "
    WRITE/OUT "Which file do you need?"
    WRITE/OUT " "
    WRITE/OUT "       OBSERVATORY table file"
    !WRITE/OUT " "
    !WRITE/OUT "       HORIZON-obstruction table"	! (not yet)
    WRITE/OUT " "
    WRITE/OUT "       STAR file"
    WRITE/OUT " "
    WRITE/OUT "       INSTRUMENT-description file"
    WRITE/OUT " "
    WRITE/OUT " "
    INQUIRE/KEYW reply "(Please enter one of the capitalized words in this list)"
    WRITE/OUT " "
    !
    IF reply(:1) .eq. "o" THEN	! needs obs.file
	WRITE/OUT "The default file esotel.tbl will be supplied automatically."
	WRITE/OUT " "
	WRITE/OUT "If that is not what you want, you will probably have to"
	WRITE/OUT "use EDIT/TABLE on a copy of the esotel.tbl file."

    ELSEIF reply(:1) .eq. "h" THEN	! needs horizon file
	WRITE/OUT "OK, we will run MAKE/HORFORM for you:""
	MAKE/HORFORM
	GOTO Start

    ELSEIF reply(:1) .eq. "s" THEN	! needs star file
	WRITE/OUT "There are some standard-star tables in"
	WRITE/OUT "$MID_PEPSYS"
	WRITE/OUT " "
	INQUIRE/KEYW reply "Are you ready to make a new star table?"
	IF reply(:1) .eq. "y" THEN
	    WRITE/OUT "OK, we will run MAKE/STARTABLE for you:""
	    MAKE/STARTABLE
	    GOTO Start

	ELSE
	    WRITE/OUT " "
	    INQUIRE/KEYW reply "QUIT, or RE-START?"
	    IF reply(:1) .eq. "q" THEN
	        WRITE/OUT "OK, you are returning to MIDAS."
	        RETURN/EXIT
	    ELSE
		WRITE/OUT "Starting over..."
		GOTO Start
	    ENDIF
	ENDIF

    ELSEIF reply(:1) .eq. "i" THEN	! needs instrument file
	WRITE/OUT "OK, we will run MAKE/PHOTOMETER for you:""
	MAKE/PHOTOM
	GOTO Start

    ELSEIF reply(:1) .eq. "q" THEN	! quit
	RETURN/EXIT

    ELSE
	WRITE/OUT " "
	WRITE/OUT "Enter a word in CAPS from the list, or Q to quit:"
	GOTO GetFile
    ENDIF
    !
    Check:
    WRITE/OUT " "
    INQUIRE/KEYW reply "Now, are you all set to go?"
    IF reply(:1) .eq. "y" THEN	! OK
        WRITE/OUT " "
    ELSEIF reply(:1) .eq. "n" THEN	! needs another file
	GOTO GetFile
    ELSEIF reply(:1) .eq. "h" .or. reply(:1) .eq. "?" THEN	! needs help
	GOTO Start
    ELSEIF reply(:1) .eq. "q" THEN	! quit
	RETURN/EXIT
    ELSE
        WRITE/OUT " "
        WRITE/OUT "Please reply YES, NO, QUIT, or HELP."
	GOTO Check
    ENDIF
!
ELSEIF reply(:1) .eq. "h" THEN	! offer some help...
!
    WRITE/OUT " "
    WRITE/OUT "Here is what you have available:"
    WRITE/OUT " "
    !
    IF AUX_MODE(1) .LE. 1 THEN              ! VMS
       $ DIR
    ELSE                                    ! UNIX
       $ ls
    ENDIF
    !
    WRITE/OUT " "
    INQUIRE/KEYW reply "(press <RETURN> to continue)"
    GOTO Start
!
ELSEIF reply(:1) .eq. "q" THEN	! quit
!
    RETURN/EXIT
!
ENDIF
!
!
!
!	Supposed to be ready to run now...
!
WRITE/OUT " "
    !
    AskAscii:
    WRITE/OUT " "
    INQUIRE/KEYW reply "Is the telescope in the esotel.tbl table?"
    IF reply(:1) .eq. "y" THEN
    !
    ! See if esotel.tbl is available locally...
    !
        IF M$EXIST("esotel.tbl") .eq. 0 THEN	! not available
            IF AUX_MODE(1) .LE. 1 THEN              ! VMS
               define/local peplib/c/1/60 -
                 "MID_DISK:[&MID_PEPSYS] "
               $ COPY {peplib}esotel.tbl []
               $ SET PROT=(O:RWED) esotel.tbl
            ELSE                                    ! UNIX
               define/local peplib/c/1/60 "$MID_PEPSYS/ "
               $ cp {peplib}esotel.tbl `pwd`
               $ chmod 644 esotel.tbl
            ENDIF
        ENDIF
    !
    ELSEIF reply(:1) .eq. "n" THEN		! Find name of obstbl
    !
        INQUIRE/KEYW obstbl "Enter the name of the observatory table file:"
        IF M$EXIST(obstbl) .eq. 0 THEN
    	    WRITE/OUT {obstbl} " is not available."
    	    WRITE/OUT "Please copy it to this directory; then try again."
    	    RETURN/EXIT
        ELSE
    	   IF AUX_MODE .LE. 1 THEN		! VMS
    	      $ COPY {obstbl} esotel.tbl
              $ SET PROT=(O:RWED) esotel.tbl
    	   ELSE					! UNIX
    	      $ cp {obstbl} esotel.tbl
              $ chmod 644 esotel.tbl
    	   ENDIF
        ENDIF
    !
    ELSEIF reply(:1) .eq. "h" THEN	! Show file
    !
        IF M$EXIST("esotel.tbl") .eq. 0 THEN	! not available
            IF AUX_MODE(1) .LE. 1 THEN              ! VMS
               define/local peplib/c/1/60 -
                 "MID_DISK:[&MID_PEPSYS] "
               $ COPY {peplib}esotel.tbl []
               $ SET PROT=(O:RWED) esotel.tbl
            ELSE                                    ! UNIX
               define/local peplib/c/1/60 "$MID_PEPSYS/ "
               $ cp {peplib}esotel.tbl `pwd`
               $ chmod 644 esotel.tbl
            ENDIF
        ENDIF
	!		file now available for sure.
	!
	WRITE/OUT " "
	WRITE/OUT " "
	WRITE/OUT "Here is what esotel.tbl contains:"
	WRITE/OUT " "
	READ/TABLE esotel.tbl
	GOTO AskAscii
    !
    ELSEIF reply(:1) .eq. "q" THEN	! quit
    !
	RETURN/EXIT
    !
    ELSE
    !
        WRITE/OUT "Please reply YES, NO, QUIT, or HELP."
        GOTO AskAscii
    !
    ENDIF
!
!
IF M$EXIST("PROUT") .ne. 0 THEN	! check to allow over-writing it
!
    WRITE/OUT " "
    WRITE/OUT "The standard output file for printing is named PROUT."
    WRITE/OUT " "
    WRITE/OUT "You already have a file named PROUT in this directory."
    PROUTcheck:
    INQUIRE/KEYW reply "OK to write over it?"
    !
    IF reply(:1) .eq. "Y" THEN
	WRITE/OUT "PROUT will be re-written."
    ELSEIF reply(:1) .eq. "n" .or. reply(:1) .eq. "q" THEN
	WRITE/OUT "Go save your PROUT file; then try again."
	RETURN/EXIT
    ELSEIF reply(:1) .eq. "h" THEN	! Show file
        IF AUX_MODE(1) .LE. 1 THEN              ! VMS
	    $ TYPE PROUT
	ELSE					! UNIX
	    $ more PROUT
	ENDIF
	GOTO PROUTcheck
    ELSE
	WRITE/OUT "Please reply YES, NO, or HELP."
	WRITE/OUT "NO will return you to MIDAS."
	WRITE/OUT " "
	GOTO PROUTcheck
    ENDIF
!
ENDIF
!
!
!
! files now available.  Run program:
!
RUN CON_EXE:PLAN
!
! Make sure it terminated normally:
!
IF APPLIC(1:1) .EQ. "X" THEN
   RETURN/EXIT
ENDIF
!
!
IF M$EXIST("PROUT") .ne. 0 THEN	! check to be sure it exists
    INQUIRE/KEYW reply "Do you want to print the PROUT file now?"
    !
    IF reply(:1) .eq. "n" THEN
        RETURN/EXIT
    ENDIF
    WRITE/OUT "We will now try to print the PROUT file for you:"
    !
    IF AUX_MODE .LE. 1 THEN		! VMS
        $ PRINT PROUT
!    ELSEIF AUX_MODE .EQ. 2 THEN	! SYS V
!        $ lp -d{MID$PRNT(3:52)} PROUT
!    ELSE				! BSD
!        $ lpr -P{MID$PRNT(3:52)} PROUT
    ELSE
         $ {syscoms(1:16)}{MID$PRNT(3:52>)} PROUT
    ENDIF
    !
    WRITE/OUT "If that produced no error message, PROUT will print."
    WRITE/OUT "...otherwise, you will have to print it yourself."
    WRITE/OUT
    !WRITE/OUT "(note that it contains FORTRAN carriage control)"
    WRITE/OUT
ENDIF
