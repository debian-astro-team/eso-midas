! @(#)starfile.prg	19.1 (ES0-DMD) 02/25/03 13:29:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: STARFILE.PRG
!.PURPOSE:        Help user set up star files
!.USE:            MAKE/STARFILE
!.AUTHOR:         Andrew T. Young
! ----------------------------------------------------------------------
!
! ********* DEFINE LOCAL KEYWORDS  **********
!
DEFINE/LOCAL reply/C/1/12 "?" A
DEFINE/LOCAL tblfil/C/1/80 " " A
DEFINE/LOCAL datfil/C/1/80 " " A
DEFINE/LOCAL fmtfil/C/1/80 " " A
DEFINE/LOCAL nrows/I/1/1 1
DEFINE/LOCAL system/C/1/32 " " A
!
! ********* BEGIN EXECUTION  **********
!
WRITE/OUT " "
WRITE/OUT " "
WRITE/OUT "The MAKE/STARFILE command will make a new program-star file for you."
WRITE/OUT " "
WRITE/OUT " "
WRITE/OUT "EITHER :"
WRITE/OUT " "
WRITE/OUT "	A) you have an ascii data file, AND a *.fmt file to describe it;"
WRITE/OUT "	   (a dummy *.fmt file will be provided, if you need one)"
WRITE/OUT " "
WRITE/OUT "OR:"
WRITE/OUT " "
WRITE/OUT "	B) you will have to enter data by hand."
WRITE/OUT " "
WRITE/OUT " "
WRITE/OUT "(Enter <Ctrl-C> to abort if you are not ready.)"
WRITE/OUT " "
!
INQUIRE/KEYW tblfil "What name do you want to give the new TABLE file?"
IF M$INDEX(tblfil,".tbl") .ne. 0 THEN
    ! name already has .tbl ext.
    WRITE/OUT "(new file will be named" {tblfil} ")"
ELSE
    WRITE/OUT "(new file will be named" {tblfil}.tbl ")"
    WRITE/KEYW tblfil {tblfil}.tbl
ENDIF
WRITE/OUT " "

AskAscii:
INQUIRE/KEYW reply "Do you have an ASCII file ready to convert?"

IF reply(:1) .eq. "y" THEN
!
    AskDatfil:
    INQUIRE/KEYW datfil "Please enter the name of the ASCII file:"
    IF M$EXIST(datfil) .eq. 0 THEN
       WRITE/OUT "SORRY -- No such file exists."
       GOTO AskDatfil
    ELSE
       WRITE/OUT "OK," {datfil} "is found."
    ENDIF
!
ELSEIF reply(:1) .eq. "n" THEN		! create a dummy file & edit it
!
    WRITE/OUT "Then we assume you want to edit a star table by hand."
    IF M$EXIST(tblfil) .eq. 0 THEN
       INQUIRE/KEYW nrows "How many stars do you want to put in the table?"
       WRITE/OUT "Creating a new star table..."
       CREATE/TABLE {tblfil} 1 {nrows} NULL
       CREATE/COLUMN {tblfil} :OBJECT	" "	A32 C*32
       CREATE/COLUMN {tblfil} :RA	" "	R11.6 R*4
       CREATE/COLUMN {tblfil} :DEC "degrees"	s12.6 R*4
       CREATE/COLUMN {tblfil} :EQUINOX "years"	F7.2 R*4
       !
       INQUIRE/KEYW reply "Do you want to include proper-motion data?"
       IF reply(:1) .eq. "y" THEN
          CREATE/COLUMN {tblfil} :MUALPHA "sec/y" F6.4 R*4
          CREATE/COLUMN {tblfil} :MUDELTA "arcsec/y" F6.3 R*4
          CREATE/COLUMN {tblfil} :EPOCH "years" F7.2 R*4
       ENDIF
       !
       INQUIRE/KEYW reply "Do you want to include spectral types?"
       IF reply(:1) .eq. "y" THEN
          CREATE/COLUMN {tblfil} :SPTYPE	" "	A12 C*12
       ENDIF
       !
       INQUIRE/KEYW reply "Do you want to include rough magnitudes?"
       IF reply(:1) .eq. "y" THEN
          CREATE/COLUMN {tblfil} :MAG	" "	A16 C*16
       ENDIF
       !	add comment column last in any case...
       CREATE/COLUMN {tblfil} :COMMENT	" "	A32 C*32
    ENDIF
    EDIT/TABLE {tblfil}
    WRITE/OUT "You can add more rows later with the WRITE/TABLE command."
    RETURN/EXIT
!
ELSE
    WRITE/OUT "Please answer yes or no."
    GOTO AskAscii
ENDIF
WRITE/OUT " "

AskFMT:
INQUIRE/KEYW reply "Do you have a *.fmt file prepared?"
!
IF reply(:1) .eq. "y" THEN		! DOIT!
!
    AskFMTfil:
    INQUIRE/KEYW fmtfil "Please enter the name of the *.fmt file:"
    IF M$EXIST(fmtfil) .eq. 0 THEN
       WRITE/OUT "Could not find " {fmtfil}
       WRITE/OUT "SORRY -- No such file exists."
       WRITE/OUT
       WRITE/OUT "Try one of these:"
       WRITE/OUT
       IF AUX_MODE(1) .LE. 1 THEN ! VMS
	   $ DIR
       ELSE			! UNIX
	   $ ls
       ENDIF
       GOTO AskFMTfil
    ELSE
       WRITE/OUT "OK," {fmtfil} "is found."
    ENDIF
    WRITE/OUT 
    WRITE/OUT "   ... making a new table; may take some time."
    WRITE/OUT 
    CREATE/TABLE {tblfil} 9 99 {datfil} {fmtfil}
    WRITE/OUT "Table file" {tblfil} "has been created."
    WRITE/OUT "Here are the first 5 lines:"
    WRITE/OUT " "
    READ/TABLE {tblfil} @1..5
!
ELSEIF reply(:1) .eq. "n" THEN		! make a new *.fmt file
!
    IF AUX_MODE(1) .LE. 1 THEN              ! VMS
       define/local peplib/c/1/60 -
         "MID_DISK:[&MIDASHOME.&MIDVERS.CONTRIB.PEPSYS.LIB] "
       IF M$EXIST("starfile.fmt") .EQ. 0 THEN
          $ COPY {peplib}starfile.fmt []
          $ SET PROT=(O:RWED) starfile.fmt
       ELSE
	  WRITE/OUT "You already have a copy of starfile.fmt"
       ENDIF
    ELSE                                    ! UNIX
       define/local peplib/c/1/60 "$MIDASHOME/$MIDVERS/contrib/pepsys/lib/ "
       IF M$EXIST("starfile.fmt") .EQ. 0 THEN
          $ cp {peplib}starfile.fmt `pwd`
          $ chmod 644 starfile.fmt
       ELSE
	  WRITE/OUT "You already have a copy of starfile.fmt"
       ENDIF
    ENDIF
    !
    WRITE/OUT " "
    WRITE/OUT "starfile.fmt  copied to current directory."
    WRITE/OUT " "
    WRITE/OUT "	You can now edit the dummy format file  starfile.fmt"
    WRITE/OUT "	with your favorite editor."
    WRITE/OUT " "
    WRITE/OUT "Invoke MAKE/STARFILE again when the format file is ready."
    RETURN/EXIT
!
ELSEIF M$EXIST(reply) .ne. 0 THEN
    WRITE/OUT "OK, " {reply} " is found."
    WRITE/KEYW fmtfil {reply}
    WRITE/OUT " "
    WRITE/OUT "   ... making a new table; may take some time."
    WRITE/OUT " "
    CREATE/TABLE {tblfil} 9 99 {datfil} {fmtfil}
    WRITE/OUT " "
    WRITE/OUT "Table file" {tblfil} "has been created."
    WRITE/OUT "Here are the first 5 lines:"
    WRITE/OUT " "
    READ/TABLE {tblfil} @1..5
!
ELSE
    WRITE/OUT "Please answer yes or no."
    GOTO AskFMT
ENDIF
!
!
!
INQUIRE/KEYW reply "Does this file contain only STANDARD stars?"
!
AskSTD:
IF reply(:1) .eq. "y" THEN
    WRITE/OUT "Please enter a 32-character identification string that"
    WRITE/OUT "contains the name of the system:"
    WRITE/OUT "  ................................"
    INQUIRE/KEYW system " "
    WRITE/DESC {tblfil} SYSTEM/C/1/32 "{system}"
ELSEIF reply(:1) .eq. "n" THEN
    WRITE/OUT "Thanks.  We are done."
ELSE
    WRITE/OUT "Please answer yes or no."
    GOTO AskSTD
ENDIF
