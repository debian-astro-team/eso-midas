! @(#)horform.prg	19.1 (ES0-DMD) 02/25/03 13:29:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: HORFORM.PRG
!.PURPOSE:        Help user set up horizon-limit forms
!.USE:            MAKE/HORFORM
!.AUTHOR:         Andrew T. Young
! ----------------------------------------------------------------------
!
! ********* DEFINE LOCAL KEYWORDS  **********
!
DEFINE/LOCAL reply/C/1/10 "?" A
DEFINE/LOCAL obstbl/C/1/80 "esotel.tbl"
!
! ********* BEGIN EXECUTION  **********
!
WRITE/OUT " "
WRITE/OUT "The MAKE/HORFORM command will make horizon-limit forms for you."
WRITE/OUT " "
!
AskAscii:
INQUIRE/KEYW reply "Is the telescope in the esotel.tbl table?"
IF reply(:1) .eq. "y" THEN
!
! See if esotel.tbl is available locally...
!
    IF M$EXIST("esotel.tbl") .eq. 0 THEN	! not available
        IF AUX_MODE(1) .LE. 1 THEN              ! VMS
           define/local peplib/c/1/60 -
             "MID_DISK:[&MID_PEPSYS] "
           $ COPY {peplib}esotel.tbl []
           $ SET PROT=(O:RWED) esotel.tbl
        ELSE                                    ! UNIX
           define/local peplib/c/1/60 "$MID_PEPSYS/"
           $ cp {peplib}esotel.tbl `pwd`
           $ chmod 644 esotel.tbl
        ENDIF
    ENDIF
!
ELSEIF reply(:1) .eq. "n" THEN		! Find name of obstbl
!
    INQUIRE/KEYW obstbl "Enter the name of the observatory table file:"
    IF M$EXIST(obstbl) .eq. 0 THEN
	WRITE/OUT {obstbl} " is not available."
	WRITE/OUT "Please copy it to this directory; then try again."
	RETURN/EXIT
    ELSE
	IF AUX_MODE .LE. 1 THEN		! VMS
	   $ COPY {obstbl} esotel.tbl
           $ SET PROT=(O:RWED) esotel.tbl
	ELSE				! unix
	   $ cp {obstbl} esotel.tbl
           $ chmod 644 esotel.tbl
	ENDIF
    ENDIF
!
ELSE
    WRITE/OUT "Please answer yes or no."
    GOTO AskAscii
ENDIF

! esotel.tbl now available.  Run program:
!
RUN CON_EXE:HORFORM
!
! Make sure it terminated normally:
!
IF APPLIC(1:1) .EQ. "X" THEN
   RETURN/EXIT
ENDIF
!
!
WRITE/OUT "We will now try to print the FORM file for you:"
!
IF AUX_MODE .LE. 1 THEN		! VMS
    $ PRINT FORM
ELSE				! UNIX
    $ cat FORM | ( fpr || asa ) 2>/dev/null | {SYSCOMS(1:16)}
ENDIF
!
WRITE/OUT "If that produced no error message, FORM will print."
WRITE/OUT "...otherwise, you will have to print it yourself."
WRITE/OUT
WRITE/OUT "(note that it contains FORTRAN carriage control)"
WRITE/OUT
WRITE/OUT "      (HELP [PRINTERS] names local printers)"
