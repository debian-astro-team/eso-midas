! @(#)instr.prg	19.1 (ES0-DMD) 02/25/03 13:29:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: INSTR.PRG
!.PURPOSE:        Help user set up instrument table for photometer
!.USE:            MAKE/PHOTOM
!.AUTHOR:         Andrew T. Young
! ----------------------------------------------------------------------
!
! ********* BEGIN EXECUTION  **********
!
WRITE/OUT " "
WRITE/OUT "The MAKE/PHOTOM command makes or checks the instrumental table file."
WRITE/OUT " "
!
! run instr.exe
RUN CON_EXE:INSTR
