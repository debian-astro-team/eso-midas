! @(#)dcon.prg	19.1 (ES0-DMD) 02/25/03 13:28:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: DCON.PRG
!.PURPOSE:        Convert data from raw to MIDAS *.tbl form.
!.USE:            CONVERT/PHOT
!.AUTHOR:         Andrew T. Young
! ----------------------------------------------------------------------
!
! ********* DEFINE LOCAL KEYWORDS  **********
!
DEFINE/LOCAL reply/C/1/12 "?" A
DEFINE/LOCAL type/C/1/6 "?" A
DEFINE/LOCAL rawdat/C/1/80 " " A
DEFINE/LOCAL tblfil/C/1/80 " " A
DEFINE/LOCAL datfil/C/1/80 "data.dat"
DEFINE/LOCAL fmtfil/C/1/80 "data.fmt"
DEFINE/LOCAL obstbl/C/1/80 "esotel.tbl"
DEFINE/LOCAL nrows/I/1/1 1
!
! ********* BEGIN EXECUTION  **********
!
WRITE/OUT " "
WRITE/OUT " "
WRITE/OUT "The CONVERT/PHOT command will make a new data-table file for you."
WRITE/OUT " "
WRITE/OUT "You should have a file of raw data from the telescope to convert."
WRITE/OUT " "
WRITE/OUT " "
WRITE/OUT "   It will be useful to embed date information in file names."
WRITE/OUT " "
WRITE/OUT " "
WRITE/OUT " "
!
AskTbl:
INQUIRE/KEYW tblfil "What name do you want to give the new DATA-TABLE file?"
IF M$INDEX(tblfil,".tbl") .ne. 0 THEN
    ! name already has .tbl ext.
    WRITE/OUT "(new file will be named" {tblfil} ")"
ELSE
    WRITE/OUT "(new file will be named" {tblfil}.tbl ")"
    WRITE/KEYW tblfil {tblfil}.tbl
ENDIF
!
WRITE/OUT " "
!
IF M$EXIST(tblfil) .eq. 0 THEN
   ! OK, no such file.
ELSE
   AskWrite:
   WRITE/OUT {tblfil} "already exists!"
   INQUIRE/KEYW reply "OK to write over it?"
   !
   IF  reply(:1) .eq. "y" THEN
      WRITE/OUT " "
      WRITE/OUT {tblfil} "will be re-written."
   ELSEIF reply(:1) .eq. "n" THEN
      WRITE/OUT " "
      WRITE/OUT "Here are the files in your current directory:"
      WRITE/OUT " "
      IF AUX_MODE(1) .LE. 1 THEN ! VMS
         $ DIR
      ELSE			! UNIX
         $ ls
      ENDIF
      WRITE/OUT " "
      WRITE/OUT "Please select a new name."
      GOTO AskTbl
   ELSE
      WRITE/OUT "Please reply YES or NO."
      GOTO AskWrite
   ENDIF
ENDIF

AskRaw:
WRITE/OUT " "
WRITE/OUT " "
rawdat = "                                                    "
INQUIRE/KEYW rawdat "What is the name of the RAW data file?"
!
IF M$EXIST(rawdat) .eq. 0 THEN
   WRITE/OUT "(Cannot find {rawdat})"
   WRITE/OUT " "
   WRITE/OUT "Sorry, no such file exists.  Be sure the name is right."
   WRITE/OUT " "
   WRITE/OUT "Here are the files in your current directory:"
   WRITE/OUT " "
   IF AUX_MODE(1) .LE. 1 THEN ! VMS
      $ DIR
   ELSE			! UNIX
      $ ls
   ENDIF
   GOTO AskRaw
ELSEIF rawdat(:4) .eq. "quit" THEN
   RETURN/EXIT
ELSE
   WRITE/OUT "OK," {rawdat} "is found."
   WRITE/OUT " "
ENDIF

! Make sure esotel.tbl is available...
    IF M$EXIST("esotel.tbl") .eq. 0 THEN	! not available
        IF AUX_MODE(1) .LE. 1 THEN              ! VMS
           define/local peplib/c/1/60 -
             "MID_DISK:[&MID_PEPSYS] "
           $ COPY {peplib}esotel.tbl []
           $ SET PROT=(O:RWED) esotel.tbl
        ELSE                                    ! UNIX
           define/local peplib/c/1/60 "$MID_PEPSYS/"
           $ cp {peplib}esotel.tbl `pwd`
           $ chmod 644 esotel.tbl
        ENDIF
	WRITE/OUT " "
	WRITE/OUT "The standard Observatory file    esotel.tbl"
	WRITE/OUT "has been copied into your working directory."
	WRITE/OUT " "
    ENDIF

AskType:
WRITE/OUT " "
WRITE/OUT " "
WRITE/OUT "The following formats are supported and cna be converted:"
WRITE/OUT "      DANISH (D)"
WRITE/OUT "      ESO 50 cm (ESO50)"
WRITE/OUT "      ESO standard (E)"
WRITE/OUT "and    OTHER (O) format"
WRITE/OUT " "
INQUIRE/KEYW reply "In which format is the raw data:"
IF reply(:1) .eq. "D" THEN		! Danish format
!
    RUN CON_EXE:DANISH
    !
    ! Make sure it terminated normally:
    !
    IF APPLIC(1:1) .EQ. "X" THEN
       RETURN/EXIT
    ENDIF
    !
    WRITE/KEYW type "Danish"
!
ELSEIF reply(1:5) .eq. "ESO50" THEN	! ESO 50cm photometer format
!
    WRITE/KEYW  IN_A/C/1/60 {rawdat}
    WRITE/KEYW  OUT_A/C/1/60 {tblfil}
    RUN CON_EXE:ESO50
    !
    ! Make sure it terminated normally:
    !
    IF APPLIC(1:1) .EQ. "X" THEN
       RETURN/EXIT
    ENDIF
    !
    WRITE/KEYW type "ESO50"
    GOTO READ
!
ELSEIF reply(:1) .eq. "E" THEN		! ESO format
!
    RUN CON_EXE:ESODCON
    !
    ! Make sure it terminated normally:
    !
    IF APPLIC(1:1) .EQ. "X" THEN
       RETURN/EXIT
    ENDIF
    !
    WRITE/KEYW type "ESO"
!
ELSEIF reply(:1) .eq. "O" THEN		! other format
!
    WRITE/OUT "Sorry -- you have to convert those data yourself."
    RETURN/EXIT
!
ELSE
    WRITE/OUT "Please answer DANISH, ESO50, ESO, or OTHER."
    GOTO AskType
ENDIF
!
! *** Here check for the data and format file and create the table
!     Only for the D and E format; the ESO50 table are created directly
WRITE/OUT " "
WRITE/OUT " "
!	(*dcon programs create files "data.dat" and "data.fmt")
IF M$EXIST(datfil) .eq. 0 THEN
   WRITE/OUT "ERROR -- Could not find " {datfil}
   WRITE/OUT " "
   WRITE/OUT "Probably the data could not be converted."
   WRITE/OUT "Please make sure your raw data are clean."
   RETURN/EXIT
ELSE
   WRITE/OUT {datfil} "has been created."
ENDIF

IF M$EXIST(fmtfil) .eq. 0 THEN
   WRITE/OUT " "
   WRITE/OUT "ERROR -- Could not find " {fmtfil}
   WRITE/OUT " "
   WRITE/OUT "Probably the data could not be converted."
   WRITE/OUT "Please make sure your raw data are clean."
   RETURN/EXIT
ELSE
   WRITE/OUT {fmtfil} "has been created."
ENDIF
!
WRITE/OUT " "
WRITE/OUT "Creating the table (may take a while) ..."
CREATE/TABLE {tblfil} 1 1 {datfil} {fmtfil}
WRITE/OUT " "
WRITE/OUT "Observational-data table file" {tblfil} "has been created."
WRITE/OUT "Here are the first 5 lines:"
WRITE/OUT " "
!
READ:
! *** Table created; read it
READ/TABLE {tblfil} @1..5
!

WRITE/OUT " "
WRITE/OUT " "
INQUIRE/KEYW reply "Do you have a star-table file for your program stars?"
!
WRITE/OUT " "
IF reply(:1) .eq. "y" THEN
   WRITE/OUT "Good.  You can now reduce these data with REDUCE/PHOT."

ELSEIF reply(:1) .eq. "n" THEN
   WRITE/OUT " "
   WRITE/OUT " "
   WRITE/OUT "If you can find accurate coordinates for them, create"
   WRITE/OUT "an ASCII table of the coordinate data, and use the"
   WRITE/OUT "MAKE/STARTABLE command to convert it to MIDAS *.tbl format."
   WRITE/OUT " "
   WRITE/OUT "You should be able to get good coordinates from STARCAT."
   WRITE/OUT " "
   WRITE/OUT " "
   WRITE/OUT "It is possible to extract rough coordinates from the" {type}
   WRITE/OUT "data themselves.  However, these values may be in error by an"
   WRITE/OUT "unknown amount, and this procedure is NOT recommended."
   WRITE/OUT " "
   WRITE/OUT "Do you want to try to extract coordinates from" {rawdat} "?"
   INQUIRE/KEYW reply "?"
   !
   IF reply(:1) .eq. "y" THEN
      WRITE/OUT " "
      WRITE/OUT " "
      WRITE/OUT " "
      WRITE/OUT "                      W A R N I N G"
      WRITE/OUT " "
      WRITE/OUT " "
      WRITE/OUT "You are at the mercy of whoever last adjusted the coordinate"
      WRITE/OUT "zero-points of the telescope control system!  The results may"
      WRITE/OUT "be in ERROR by an UNKNOWN amount.  This is the last warning"
      WRITE/OUT "you will have that there may be serious systematic errors."
      WRITE/OUT " "
      WRITE/OUT " "
      INQUIRE/KEYW reply "            Do you REALLY want to do this ???"
      IF reply(:1) .eq. "y" THEN
         WRITE/OUT " "
         WRITE/OUT " "
         WRITE/OUT " "
         WRITE/OUT "                    YOU HAVE BEEN WARNED."
         WRITE/OUT " "
         WRITE/OUT " "
         WRITE/OUT " "
	 WRITE/KEYW tblfil/C/1/80 " " ALL	! clear the damn keyword
	 INQUIRE/KEYW tblfil "What do you want to call the program-star table?"
	 !
         IF M$EXIST(tblfil) .eq. 0 THEN
            ! OK, no such file.
         ELSE
            BadName:
            WRITE/OUT {tblfil} "already exists!"
            INQUIRE/KEYW reply "OK to write over it?"
            !
            IF  reply(:1) .eq. "y" THEN
               WRITE/OUT {tblfil} "will be re-written."
            ELSEIF reply(:1) .eq. "n" THEN
               WRITE/OUT " "
               WRITE/OUT "Here are the files in your current directory:"
               WRITE/OUT " "
               IF AUX_MODE(1) .LE. 1 THEN ! VMS
                  $ DIR
               ELSE			! UNIX
                  $ ls
               ENDIF
               WRITE/OUT " "
               WRITE/OUT "Please select a new name."
               GOTO AskTbl
            ELSE
               WRITE/OUT "Please reply YES or NO."
               GOTO BadName
            ENDIF
         ENDIF
	 !
	 WRITE/OUT "Extracting stars..."
	 !
	 IF type(:1) .eq. "D" THEN
	    WRITE/OUT " "
	    ! run danstarx
            RUN CON_EXE:DANSTARX
            !
            ! Make sure it terminated normally:
            !
            IF APPLIC(1:1) .EQ. "X" THEN
               RETURN/EXIT
            ENDIF
            !
	 ELSEIF type(:1) .eq. "E" THEN
	    ! run esodstarx
            RUN CON_EXE:ESODSTARX
            !
            ! Make sure it terminated normally:
            !
            IF APPLIC(1:1) .EQ. "X" THEN
               RETURN/EXIT
            ENDIF
            !
	 ENDIF
	 !
	 WRITE/OUT "Creating sdata.tbl (may take some time)..."
         CREATE/TABLE sdata.tbl 1 1 sdata.dat sdata.fmt
	 !
	 WRITE/OUT "Combining multiple positions..."
	 ! run meanstar
         RUN CON_EXE:MEANSTAR
         !
         ! Make sure it terminated normally:
         !
         IF APPLIC(1:1) .EQ. "X" THEN
            RETURN/EXIT
         ENDIF
         !
	 WRITE/OUT " "
	 WRITE/OUT "Program star positions are in " {tblfil}
	 WRITE/OUT " "
	 WRITE/OUT "Here are the first 10 lines:"
	 WRITE/OUT " "
         READ/TABLE {tblfil} @1..10
	 WRITE/OUT " "
      ELSE
         WRITE/OUT " "
         WRITE/OUT "Please use Starcat and MAKE/STARTABLE before REDUCE/PHOT."
      ENDIF
   ELSEIF reply(:1) .eq. "n" THEN
      WRITE/OUT " "
      WRITE/OUT "Please use Starcat and MAKE/STARTABLE before REDUCE/PHOT."
!   ELSE
   ENDIF
   !
!ELSE
ENDIF
