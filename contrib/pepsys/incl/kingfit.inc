C  @(#)kingfit.inc	19.1 (ESO-IPG) 02/25/03 13:28:38
C
C	Copyright (C) Andrew T. Young, 1990
C
C     Sets dimensions for DLSQ arrays (see dlsq.inc).
C
      INTEGER MXP,MXPT,MXX,MKX
      PARAMETER (MXPT=16960, MXX=4, MKX=6, MXP=600)
