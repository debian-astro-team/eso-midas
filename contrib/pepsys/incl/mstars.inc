C  @(#)mstars.inc	19.1 (ESO-IPG) 02/25/03 13:28:38
C
C     'Include' file for PEPSYS programs that use star catalogs
C
      INTEGER MSTARS
      PARAMETER (MSTARS=1650)
C
C       MSTARS is max.number of star-catalog entries.
C     CAUTION: modify Subroutine NBIN if this exceeds 2048!
C
