C @(#)terror.for	19.1 (ES0-DMD) 02/25/03 13:28:48
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)terror.for	19.1  (ESO-IPG)  02/25/03  13:28:48
      SUBROUTINE TERROR(ITBL,N,MSG)
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT (c) European Southern Observatory 1992
C.IDENT		terror.for
C.MODULE
C.AUTHOR	Andrew T. Young
C.KEYWORD
C.LANGUAGE	FORTRAN 77
C.PURPOSE       closes table file and writes error message, with rtn.value
C.COMMENTS
C.VERSION	0.0
C.RETURNS
C.ENVIRONMENT
C.
C-----------------------------------------------------------------------------
C
C
      IMPLICIT NONE
C
      INTEGER ITBL, N, JCONT, JLOG, JDISP, ISTAT
C
      CHARACTER*(*) MSG
C
      COMMON/FLAGS/JCONT,JLOG,JDISP
C
      CALL TBTCLO(ITBL,ISTAT)
      CALL STECNT('PUT', JCONT,JLOG,JDISP)
      CALL STETER(N,MSG)
      RETURN
      END
