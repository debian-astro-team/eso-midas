C @(#)lword.for	19.1 (ES0-DMD) 02/25/03 13:28:46
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)lword.for	19.1  (ESO-IPG)  02/25/03  13:28:46
      FUNCTION LWORD(STRING)
C
C       Copyright (C) Andrew T. Young, 1990
C
C  DETERMINE LENGTH OF NON-BLANK PART OF STRING.     28 NOV. 1986
C               Modified 8 Aug. 1990 to do full scan.
C
C
      IMPLICIT NONE
C
      INTEGER LWORD,MLEN,NCHAR
C
      CHARACTER STRING*(*), WORD*80
C
      WORD=STRING
      IF (WORD(24:).EQ.' ') THEN
          MLEN=23
      ELSE
          MLEN=80
      END IF
      DO 7 NCHAR=MLEN,1,-1
      IF (WORD(NCHAR:NCHAR) .NE. ' ') GOTO 9
    7 CONTINUE
      NCHAR=0
    9 LWORD=NCHAR
      RETURN
      END
