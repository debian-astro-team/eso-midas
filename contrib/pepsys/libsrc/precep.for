C @(#)precep.for	19.1 (ES0-DMD) 02/25/03 13:28:47
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)precep.for	19.1  (ESO-IPG)  02/25/03  13:28:47
      SUBROUTINE PRECEP(EF,ED,RA,DEC)
c
c	Copyright (C) Andrew T. Young, 1990
C
C  PRECESS FROM EPOCH EF TO EPOCH ED.
C
C    SEE LIESKE, A.& AP. 73,282(1979).
C
C
      IMPLICIT NONE
      REAL EF, ED, RA, DEC, TWOPI, BIGT, T, TSQ, A11, A22, A33, A12,A21, 
     1         A13, A31, A23, A32, COSDEC, X, Y, Z, X1, Y1, Z1
C
      SAVE A11,A12,A13,A21,A22,A23,A31,A32,A33
C
      DATA TWOPI/6.2831853071796/
C
C   BIGT=(EFJD-2451545.0)/36525.
C   T=(EDJD-EFJD)/36525.
C
      BIGT=(EF-2000.)/100.
      T=(ED-EF)/100.
C  JULIAN CENTURIES.
      TSQ=T*T
C   THIS VERSION TRUNCATED TO 0.1 ARCSEC/CENTURY.
      A11=1.-TSQ*29724.E-8
      A22=1.-TSQ*(25002.E-8+BIGT*3.E-7)
      A33=1.-TSQ*4721.E-8
      A12=-T*(2236172.E-8+BIGT*1354.E-8 + T*(677.E-8 - T*222.E-8))
      A21=-A12
      A13=T*(-971717.E-8+BIGT*414.E-8 + T*(207.E-8 + T*96.E-8))
      A31=-A13
      A23=-TSQ*10865.E-8
      A32=A23
C
C  NOW USE MATRIX TO PRECESS RA,DEC.
C
      ENTRY PRECST(RA,DEC)
C
      COSDEC=COS(DEC)
      X=COS(RA)*COSDEC
      Y=SIN(RA)*COSDEC
      Z=SIN(DEC)
      X1=A11*X + A12*Y + A13*Z
      Y1=A21*X + A22*Y + A23*Z
      Z1=A31*X + A32*Y + A33*Z
      DEC=ATAN(Z1/SQRT(X1*X1+Y1*Y1))
      RA=MOD(ATAN2(Y1,X1)+TWOPI,TWOPI)
      RETURN
      END
