$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.PEPSYS.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:50 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   addstr.for
$ FORTRAN   asker.for
$ FORTRAN   askfil.for
$ FORTRAN   cathed.for
$ FORTRAN   help.for
$ FORTRAN   lword.for
$ FORTRAN   match.for
$ FORTRAN   plansubs.for
$ FORTRAN   plot.for
$ FORTRAN   precep.for
$ FORTRAN   round.for
$ FORTRAN   subs1.for
$ FORTRAN   filhlp.for
$ FORTRAN   terror.for
$ FORTRAN   redsubs.for
$ FORTRAN   redsubs2.for
$ FORTRAN   space.for
$ FORTRAN   dlsq.for
$ FORTRAN   partit.for
$ FORTRAN   fmt.for
$ LIB/REPLACE libpep addstr.obj,asker.obj,askfil.obj,cathed.obj,help.obj,lword.obj,match.obj,plansubs.obj,plot.obj,precep.obj,round.obj,subs1.obj,filhlp.obj,terror.obj,redsubs.obj,redsubs2.obj,space.obj,dlsq.obj,partit.obj,fmt.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
