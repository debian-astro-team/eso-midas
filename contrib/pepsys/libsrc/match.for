C @(#)match.for	19.1 (ES0-DMD) 02/25/03 13:28:46
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)match.for	19.1  (ESO-IPG)  02/25/03  13:28:46
      FUNCTION MATCH(WORD,KEY)
C
C       Copyright (C) Andrew T. Young, 1990
C
C   MATCH IS .TRUE. IF WORD MATCHES KEY.    Aug.10,1989
C
C
      IMPLICIT NONE
C
      INTEGER L1,L2,LWORD
C
      LOGICAL MATCH
      CHARACTER*(*) WORD, KEY
      CHARACTER*10 W1,W2
C
      W1=' '//WORD
      L1=LWORD(W1)
      IF(L1.EQ.0) GO TO 9
      W2=' '//KEY
      L2=LWORD(W2)
      IF (L1.LT.L2) THEN
        IF(INDEX(W2(:L2),W1(:L1)).GE.1)THEN
       MATCH=.TRUE.
       RETURN
        END IF
      ELSE
        IF(INDEX(W1(:L1),W2(:L2)).GE.1)THEN
       MATCH=.TRUE.
       RETURN
        END IF
      END IF
C  NO MATCH.
    9 MATCH=.FALSE.
      RETURN
      END
