C @(#)help.for	19.1 (ES0-DMD) 02/25/03 13:28:46
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)help.for	19.1  (ESO-IPG)  02/25/03  13:28:46
      FUNCTION HELP(WORD) 
c
c	Copyright (C) Andrew T. Young, 1990
C 
C  .TRUE. IF WORD IS '?' OR 'HELP'.     14 JAN.1987 
C 
C
      IMPLICIT NONE
C 
      INTEGER NEEDH, NASSMP
C 
      COMMON /HELPS/ NEEDH,NASSMP
      SAVE /HELPS/
c
      LOGICAL HELP,MATCH
      EXTERNAL MATCH
      CHARACTER*(*) WORD
C 
      HELP=MATCH(WORD,'?') .OR. MATCH(WORD,'HELP')
      IF(HELP) NEEDH=NEEDH+1
      RETURN
      END 
