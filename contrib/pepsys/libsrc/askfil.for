C @(#)askfil.for	19.1 (ES0-DMD) 02/25/03 13:28:45
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)askfil.for	19.1  (ESO-IPG)  02/25/03  13:28:45
      SUBROUTINE ASKFIL(QUERY,REPLY)
C
C       Copyright (C) Andrew T. Young, 1990
C       Copyright (C) European Southern Observatory, 1992
C
C  ASKS THE QUERY ON THE TERMINAL SCREEN AND    28 Sep.,'88
C  ACCEPTS REPLY FROM KEYBOARD.
C       Asks for file names; accept lower case.
C
C  QUITS IF REPLY IS 'Q' OR 'QUIT'.
C
      IMPLICIT NONE
C
      INTEGER ISTAT, NACTEL, IUNIT, NULLS, LWORD
C
      CHARACTER *(*) QUERY,REPLY, A*1
      CHARACTER*80 BUFIN, BUFOUT
C
C
      LOGICAL MATCH
      EXTERNAL MATCH
C
      BUFIN=QUERY
    1 CALL STTPUT(' ',ISTAT)
      CALL STKPRC(BUFIN, 'INPUTC', 1, 1, 80,
     1           NACTEL, BUFOUT, IUNIT, NULLS, ISTAT)
      REPLY=BUFOUT
      IF (ISTAT.NE.0) THEN
                CALL STTPUT('Failed to get string; try again:',ISTAT)
                GO TO 1
      END IF
C   3 FORMAT(/1X,A/)
      IF(LWORD(REPLY).EQ.0) GOTO 1
       IF(MATCH(REPLY,'QUIT').OR.MATCH(REPLY,'quit'))THEN
      CALL ASK('DO YOU WANT TO QUIT?',A)
      IF(A.EQ.'Y'.OR.A.EQ.'Q'.OR.A.EQ.'y'.OR.A.EQ.'q') THEN
        CALL TV('ABANDONED.')
        CALL STSEPI
      END IF
      GO TO 1
       END IF
      RETURN
C
      END
