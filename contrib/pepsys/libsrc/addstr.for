C @(#)addstr.for	19.1 (ES0-DMD) 02/25/03 13:28:45
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)addstr.for	19.1  (ESO-IPG)  02/25/03  13:28:45
      SUBROUTINE ADDSTR(EPOCH,RA,DEC) 
c
c       Copyright (C) Andrew T. Young, 1990
c       Copyright (C) European Southern Observatory, 1992
C 
C  ASKS FOR NEW STAR POSITION FROM KEYBOARD.     2 JAN.'87
C 
C 
      IMPLICIT NONE
C 
C 
      REAL EPOCH, RA, DEC, DEG10, DEGRAD, PI
C 
      INTEGER N1,N2
C 
      EXTERNAL DEG10, DEG2MS
      EXTERNAL HELP,ASKN,TV
C 
      LOGICAL HELP
      CHARACTER STRING*32, DEG2MS*13, A*1 
      CHARACTER*38 BIGSTR
      DATA DEGRAD/0.017453292519943/, PI/3.1415926535898/ 
      DATA BIGSTR/'                                      '/
C 
C     *****  BEGIN  *****
C 
    1 CALL TV('Enter coordinates:') 
      CALL TV('    h  m  s      o  ''  "  Epoch')
C                11 22 34.56 -12 34 56.7  1985.25 
      CALL ASKN('?',STRING)
       IF(HELP(STRING))THEN 
          CALL TV('   Omit Epoch if current date.')
          GO TO 1 
       END IF 
      RA=DEG10(STRING(:12))*PI/12.
      N1=INDEX(STRING,'(')
      IF (N1.NE.0) STRING(N1:N1)=' '
      DEC=DEG10(STRING(13:24))*DEGRAD 
      IF (N1.EQ.0) THEN
         N1=24
         N2=33
      ELSE
         N2=INDEX(STRING,')')
         IF (N2.EQ.0) N2=33
      END IF
      READ(STRING(N1+1:N2-1),'(BN,F7.0)',ERR=1)EPOCH
C   Verify. 
      CALL TV('Please verify:')
      CALL TVN('    h  m  s      o  ''  "   EPOCH')
      BIGSTR(2:15)=DEG2MS(RA*12./PI) 
      BIGSTR(15:27)=DEG2MS(DEC/DEGRAD)
      IF(EPOCH.NE.0.) WRITE(BIGSTR(27:),'(F7.2)')EPOCH
      BIGSTR(36:)='OK?'
      CALL ASKN(BIGSTR,A)
      IF(A.EQ.'N') GO TO 1
      RETURN
      END 
