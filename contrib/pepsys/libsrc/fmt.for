C @(#)fmt.for	19.1 (ES0-DMD) 02/25/03 13:28:46
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
       SUBROUTINE SKYEQN(PG, SP, CARD)
C
C   Now display fitted equation:
C
C
      DOUBLE PRECISION SP(12)
      REAL PG(12)
C
      CHARACTER FMT*93, CARD*80
C
C
C
      CALL SPACE2
      CALL NEED(12)
      CALL CENTER('Representation of sky brightness:')
      CALL SPACE
C
C     Dark-sky term:
C
      CALL TV('      dark-sky term:')
C     Numerator exponent:
      CARD=' '
      CARD(70:)='2'
      CALL TV(CARD)
C     Numerator:
      FMT(:44)='(''('',1PE8.2,'' +/-'',E9.2,'') * M  +  ('')'
      FMT(36:)='('',1PE8.2,'' +/-'',E9.2,'') * M'')'
      WRITE(CARD(11:),FMT) PG(2),SP(2), PG(3),SP(3)
      CALL TVN(CARD)
C     Center line:
      CARD='SKY ='
      WRITE (CARD(7:),'(64(''-''),''   +'')')
      CALL TVN(CARD)
C     Denominator exponent:
      CARD=' '
      CARD(70:)='2'
      CALL TVN(CARD)
C     Denominator:
      CARD(7:11)='1 +'
      FMT(:44)='(''('',  F8.4,'' +/-'',F9.4,'') * M  +  ('')'
      FMT(36:)='('',  F8.4,'' +/-'',F9.4,'') * M'')'
      WRITE(CARD(11:),FMT) PG(4),SP(4), PG(5),SP(5)
      CALL TVN(CARD)
      CALL SPACE2
C
C     Moonlight term:
C
      CALL NEED(7)
      CALL TV('      moonlight term:')
      CALL SPACE2
C     First factor:
      FMT(:44)='(''M * [('',1PE8.2,''+/-'',E8.2,'')/E + ('')'
      FMT(36:)='('',1PE8.2,''+/-'',E8.2,'') + ('')'
      FMT(62:)='('',1PE8.2,''+/-'',E8.2,'')*E]'')'
      IF (PG(7).LT.0.) FMT(34:34)='-'
      WRITE(CARD,FMT) PG(6),SP(6), ABS(PG(7)),SP(7), PG(8),SP(8)
      CALL TVN(CARD)
C     Second factor:
      FMT(:44)='(2X,''* {exp[-('',  F7.4,'' +/-'',F8.4,'')*S] + ('')'
      FMT(44:)='('',F7.4,'' +/-'',F8.4,'')/P}   +'')'
      WRITE(CARD(1:),FMT) PG(9),SP(9), PG(10),SP(10)
      CALL TV(CARD)
      CALL SPACE2
C
C     Instrumental term:
C
      CALL NEED(4)
      CALL TV('      instrumental term:')
      FMT=
     1'(2X,''('',1PE8.2,'' +/-'',E9.2,'') * (signal of nearest star)'')'
      WRITE(CARD(1:),FMT) PG(11),SP(11)
      CALL TV(CARD)
C
C     Explanation:
C
      CALL SPACE
      CALL NEED(10)
      CALL TV('where')
      CALL TV('      M  is the airmass at the piece of sky observed')
      CALL TV(
     1   '      E  is its angular elongation (radians) from the Moon')
      CALL TV('      S  is the Sum of lunar and sky airmasses')
      CALL TV('      P  is the Product of lunar and sky airmasses')
      CALL SPACE
      CALL TV(
     1 'Note:  Moonlight term applies only when Moon is above horizon.')
      CALL SPACE
C
      END
