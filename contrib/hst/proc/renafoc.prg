! @(#)renafoc.prg	19.1 (ES0-DMD) 02/25/03 13:24:55
! @(#)renafoc.prg	19.1 02/25/03 13:24:55
!****************************************************************************
!                        Procedure renafoc.prg
!
!
! M. Almudena Prieto, 
!  June 1992
!
! It renames bdf and tbl FOC files, previously read with INTAPE/FITS,
! to their original  names.
! 
! Files created with INTAPE/FITS should be identified with a initial(s)
! letter(s). Parameter P3: Initial(s) of the MIDAS/FOC images
!
!****************************************************************************


! Read the bdf and tbl files and  put them in respective catalogues
define/para  p3 ? C   "Give the initial/s of your FOC files:"
creat/icat ifoc {p3}*.bdf
creat/tcat tfoc {p3}*.tbl

!Loop on the catalogues
!Rename the tables files

DEFINE/LOCAL CATAL/I/1/2 0,0
DEFINE/LOCAl II/I/1/1 0
WRITE/KEY NAME/C/1/20 " "
First_LOOP:
STORE/FRAME IN_B tfoc.cat 1 First
READ/KEY IN_B
STORE/FRAME IN_A ifoc.cat 2 First
READ/KEY IN_A
II = M$INDEX(NAME,".")
COPY/DK 'IN_A' FILENAME NAME
II = M$INDEX(NAME,".")-1
RENAME/table 'IN_B' 'NAME(1:{II})'
GOTO First_LOOP
First:

! Next with the image files
dele/key CATAL
DEFINE/LOCAL CATAL/I/1/2 0
CAT_LOOP:
STORE/FRAME IN_A ifoc.cat 1 END
II = M$INDEX(NAME,".")
COPY/DK 'IN_A' FILENAME NAME
II = M$INDEX(NAME,".")-1
RENAME/IMA 'IN_A' 'NAME(1:{II})'
READ/KEY CATAL
GOTO CAT_LOOP
END:

!clean area
!del/icat ifoc nc
!del/tcat tfoc nc
del tfoc.cat nc
del ifoc.cat nc
del dirfile.dat nc
!exit


