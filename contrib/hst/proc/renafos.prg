! @(#)renafos.prg	19.1 (ES0-DMD) 02/25/03 13:24:55
! @(#)renafos.prg	19.1 02/25/03 13:24:55
!****************************************************************************
!                        Procedure renafos.prg
!
! M. Almudena Prieto, 
! December 1991
! June 1992
!
! It renames bdf and tbl FOS files, previously read with INTAPE/FITS,
! to their original names. Files created with INTAPE/FITS should be
! identified with an initial(s) letter(s).
! 
! Parameter P3 : Inicitial(s) of the FOS/MIDAS files
!****************************************************************************

! Read the bdf and tbl files and  put them in respective catalogues
define/para  p3 ? C "Give the inicial/s of your FOS  files:"
creat/icat ifos {p3}*.bdf
creat/tcat tfos {p3}*.tbl

!Loop on the catalogues
!Rename the tables files

DEFINE/LOCAL CATAL/I/1/2 0,0
DEFINE/LOCAl II/I/1/1 0
WRITE/KEY NAME/C/1/20 " "
First_LOOP:
STORE/FRAME IN_B tfos.cat 1 First
READ/KEY IN_B
STORE/FRAME IN_A ifos.cat 2 First
READ/KEY IN_A
II = M$INDEX(NAME,".")
COPY/DK 'IN_B' FILENAME NAME
II = M$INDEX(NAME,".")-1
RENAME/table 'IN_B' 'NAME(1:{II})'
GOTO First_LOOP
First:

! Next with the image files
dele/key CATAL
DEFINE/LOCAL CATAL/I/1/2 0
CAT_LOOP:
STORE/FRAME IN_A ifos.cat 1 TOTO
II = M$INDEX(NAME,".")
COPY/DK 'IN_A' FILENAME NAME
II = M$INDEX(NAME,".")-1
RENAME/IMA 'IN_A' 'NAME(1:{II})'
READ/KEY CATAL
GOTO CAT_LOOP
TOTO:

! clean up area
dele/icat ifos nc
dele/tcat tfos nc
dele tfos.cat nc
dele ifos.cat nc
del dirfile.dat nc

!exit

























