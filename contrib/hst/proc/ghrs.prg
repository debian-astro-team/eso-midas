! @(#)ghrs.prg	19.1 (ES0-DMD) 02/25/03 13:24:55
! @(#)ghrs.prg	19.1 02/25/03 13:24:55
!****************************************************************************
!                        Procedure ghrs.prg
!! M. Almudena Prieto, 
! December 1991
! Revised March 1993
!  
!                      ** IMPORTANT **
!This procedure should run in a separate subdirectory where
!only the  *c0*, *c1* and *cq* GHRS  files are included.
!                       ************
!
! The procedure re-groups the *c0*, *c1* and *cq* GHRS files into a single
! MIDAS table. The new table  is renamed with the root-name of the
! GHRS dataset.
!The table  contains  wavelengths:   column #1 or   :WAV
!                     fluxes:        column #2 or   :FLUX
!                     quality flags: column #3 or   :QUAL
! 
! Column 1 and 2 should be used by the user to create  an 1-D spectrum.
!****************************************************************************
!MIDAS/GHRS files - images and associated tables- are renamed with the
!ROOTNAME  appearing in the FITS header:

DEFINE/PAR P1 ? C   "Give the inicial of your GHRS files, e.g., z:  "

creat/icat ghrs {p1}*.bdf

DEFINE/LOCAL CATAL/I/1/1 0
DEFINE/LOCAL I/I/1/1 0
WRITE/KEY NAME/C/1/20 " "
write/key VAR/r/1/1 1
LOOP_ghrs:
DO I = 1 3
STORE/FRAME IN_B ghrs.cat 1 FINITO
READ/KEY IN_B
copy/it 'IN_B' temp{I}
enddo
IF {{IN_B},NAXIS} .NE. 0 THEN
VAR = {{IN_B},NPIX(2)}
endif

COPY/DK 'IN_B' ROOTNAME NAME
!
! Each 2-D GHRS/MIDAS  image is converted to a MIDAS table
!
CREA/TABLE TEMP 3 1000 NULL
COPY/TT temp0001 #{VAR} TEMP :WAV 
COPY/TT temp0002 #{VAR}  TEMP :FLUX 
COPY/TT temp0003 #{VAR}  TEMP :QUAL
RENAME/table TEMP 'NAME'
GOTO LOOP_ghrs
FINITO:
! clean up area
! Delete temporary and catalog files
do I = 1 3
dele/table temp{I} no
enddo
dele ghrs.cat nc
del dirfile.dat nc

!exit




