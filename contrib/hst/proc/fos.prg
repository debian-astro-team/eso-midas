! @(#)fos.prg	19.1 (ES0-DMD) 02/25/03 13:24:55
! @(#)fos.prg	19.1 02/25/03 13:24:55
!****************************************************************************
!                        Procedure fos.prg
!! M. Almudena Prieto, 
! December 1991
! Revised June 1992
!  
!                      ** IMPORTANT **
!This procedure should run in a separate sub-directory where
!only the  *c0*, *c1* and *cq* FOS files are included.
!                        ************
!The procedure re-groups the *c0*, *c1* and *cq* FOS files into a single
! MIDAS table. The new table  is renamed with the root-name of the FOS dataset.
!The table  contains  wavelengths:   column #1 or   :WAV
!                     fluxes:        column #2 or   :FLUX
!                     quality flags: column #3 or   :QUAL
! 
! Column 1 and 2 should be used by the user to create an 1-D spectrum.
!****************************************************************************
!MIDAS/FOS files - images and associated tables- are renamed with the
!ROOTNAME  appearing in the FITS header:

creat/icat fos *.bdf
DEFINE/LOCAL CATAL/I/1/1 0
DEFINE/LOCAL I/I/1/1 0
WRITE/KEY NAME/C/1/20 " "
write/key VAR/r/1/1 1
LOOP_fos:
DO I = 1 3
STORE/FRAME IN_B fos.cat 1 FINITO
READ/KEY IN_B
copy/it 'IN_B' temp{I}
enddo
IF {{IN_B},NAXIS} .NE. 0 THEN
VAR = {{IN_B},NPIX(2)}
endif

COPY/DK 'IN_B' ROOTNAME NAME
!
! Each 2-D FOS/MIDAS  image is converted to a MIDAS table
!
CREA/TABLE TEMP 3 2064 NULL
COPY/TT temp0001 #{VAR} TEMP :WAV 
COPY/TT temp0002 #{VAR}  TEMP :FLUX 
COPY/TT temp0003 #{VAR}  TEMP :QUAL
RENAME/table TEMP 'NAME'
GOTO LOOP_fos
FINITO:
! clean up area
! Delete temporary and catalog files
do I = 1 3
dele/table temp{I} no
enddo
del dirfile.dat nc
dele fos.cat nc
!exit





