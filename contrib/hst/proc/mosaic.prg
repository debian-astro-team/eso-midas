! @(#)mosaic.prg	19.1 (ES0-DMD) 02/25/03 13:24:55
! @(#)mosaic.prg	19.1 02/25/03 13:24:55
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!                               MOSAIC.PRG
! 
!  Mosaic set of 4 WF/PC frames into single image
!  Extract and  save separately each individual WF/PC frame
!
! Parameters: 
! P1 : 4-D (800x800x4) Midas BDF containing WF/PC data
! P2 : output image (1532x1532)
! P3 : WF or PC image
!
! M. Almudena Prieto
! 
! Original      FP910306 
! Modifications AP920405
! Revised:      AP920601
! ---------------------------------------------------------------------------
!
! Define parameters
DEFINE/PAR P1 ? IMA "Enter name of input  (800x800x4) file: "
DEFINE/PAR P2 ? IMA "Enter name of output (1532x1532) file: "
DEFINE/LOCAL OFFSET/R/1/1 0.

DEFINE/PAR P3 ? C "WF or PC images?:    "
WRITE/OUT This will take a while...

!BRANCH 'P3(1:2)' WF,PC WIDE_FIELD, PLANETARY_CAMERA
! fall through if no match
!WRITE/OUT Invalid option - please try again
!RETURN

Write/key IN_B {P3}

ECHO/off
!
! Create output image
CREATE/IMA 'P2' 2,1535,1535 1,1,1,1
! Loop on WF/PC frames:
!
! 1. Extract individual 2-D frames from 3-D BDF file
!    ("magic" numbers refer to optically inactive areas)
! 2. Rotate properly the frames: 90 by 90 degrees! 
!    (amount of rotation refers to chip readout direction)
!
!! 3. Compute statistics on the frame
!! 4. Subtract lowest mode STATISTIC(9) from frame (as in IRAF subroutine)
!     Steps 3 and 4 are omitted to speed the procedure
!
! 5. Insert frame in output image
! 6. Delete temporary files
!
! WF1 frame:
! PC5 frame:
EXTRACT/IMA TEMPA = 'P1'[@41,@34,@1:@800,@800,@1]
ROTATE/COUNTER TEMPA TEMPR 1
!STAT/IMA FRAME=TEMPR BINSIZE=1. EXCESS=-200.,300.
!OFFSET = {TEMPR,STATISTIC(9)}            
!COMPUTE/IMA TEMPR = TEMPR - {OFFSET}
WRITE/OUT "        Don't worry about the warning messages"
INSERT/IMA TEMPR 'P2' @1,@766
DELETE/IMA TEMPA NO 

IF P3 .EQ. "PC" THEN
write/key in_a {p1}pc5
ELSE
write/key in_a {p1}wf1
ENDIF
rename/ima TEMPR 'in_a'
!
! WF2 frame or PC6
!
EXTRACT/IMA TEMPB = 'P1'[@29,@34,@2:@800,@800,@2]
ROTATE/COUNTER TEMPB TEMPRR 1
ROTATE/COUNTER TEMPRR TEMPR 1
!STAT/IMA FRAME=TEMPR BINSIZE=1. EXCESS=-200.,300.
!OFFSET = {TEMPR,STATISTIC(9)}
!COMPUTE/IMA TEMPR = TEMPR - {OFFSET}
INSERT/IMA TEMPR 'P2' @1,@1
DELETE/IMA TEMPB NO
DELETE/IMA TEMPRR NO

IF P3 .EQ. "PC" THEN
write/key in_a {p1}pc6
ELSE
write/key in_a {p1}wf2
ENDIF
rename/ima TEMPR 'in_a'

!
! WF3 frame or PC7:
!
EXTRACT/IMA TEMPC = 'P1'[@35,@36,@3:@800,@800,@3]
ROTATE/CLOCK TEMPC TEMPR 1
!STAT/IMA FRAME=TEMPR BINSIZE=1. EXCESS=-200.,300.
!OFFSET = {TEMPR,STATISTIC(9)}
!COMPUTE/IMA TEMPR = TEMPR - {OFFSET}
INSERT/IMA TEMPR 'P2' @771,@1
DELETE/IMA TEMPC NO

IF P3 .EQ. "PC" THEN
write/key in_a {p1}pc7
ELSE
write/key in_a {p1}wf3
ENDIF
rename/ima TEMPR 'in_a'
!
! WF4 frame or PC8:
!
EXTRACT/IMA TEMPD = 'P1'[@25,@33,@4:@800,@800,@4]
!STAT/IMA FRAME=TEMPD BINSIZE=1. EXCESS=-200.,300.
!OFFSET = {TEMPD,STATISTIC(9)}
!COMPUTE/IMA TEMPD = TEMPD - {OFFSET}
INSERT/IMA TEMPD 'P2' @771,@764

IF P3 .EQ. "PC" then
write/key in_a {p1}pc8
ELSE
write/key in_a {p1}wf4
ENDIF
rename/ima TEMPD 'in_a'

!clean area
delet iwfpc.cat no
RETURN
 
!exit
