C @(#)dophot.for	19.1 (ES0-DMD) 02/25/03 13:23:48
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      character*35 logfile
      character*30 direc, frames, table
      character*30 pict, switch, jobfile
      real*4 radii(2), psfopt(2)
c
c Get input relevant to all frames
c
      call getchr ('Directory:', direc, ndi)
      call getdat ('Find threshold (in sigmas):', thresh, 1)
      call getchr ('Aperture photometry table:', table, nta)
      call getdat ('Fitting radius, psf radius:', radii, 2)
      call getdat ('Number of PSF stars, analytic model:',
     .     psfopt, 2)
c
c-------------------------------------------------------------
c
c Loop over frames
c
  900 call tblank
c
c Get information relevant to this frame only
c
 1000 call getnam ('Input image:', pict)
      if ((pict .eq. 'END OF FILE') .or. 
     .     (pict .eq. 'EXIT')) call byebye
      call getchr ('Frames averaged, added:', frames, nfr)
c
c Open jobfile and write initial stuff
c
      jobfile = switch(pict, '.dop')
      call outfil (3, jobfile, istat)
      call startup (direc, ndi, pict, jobfile, logfile)
c
c Set up daophot/allstar commands
c
      call remove (switch(pict, '.coo'))
      call remove (switch(pict, '.ap'))
      call remove (switch(pict, '.lst'))
      call remove (switch(pict, '.psf'))
      call remove (switch(pict, '.nei'))
      call run ('daophot', logfile)
      write (3,*) 'no'
      write (3,*) 'no'
      write (3,*) 'op'
      write (3,*) ' '
      write (3,*) 'threshold =', thresh+2.
      write (3,*) 'wa = -2'
      write (3,*) 'va = -1'
      write (3,*) 'fi =', radii(1)
      write (3,*) 'ps =', radii(2)
      write (3,*) 'an =', psfopt(2)
      write (3,*) ' '
      write (3,*) 'attach ', pict
      write (3,*) 'find'
      write (3,*) frames(1:nfr)
      write (3,*) ' '
      write (3,*) 'y'
      write (3,*) 'photometry'
      write (3,*) table(1:nta)
      write (3,*) 'A2 = 0'
      write (3,*) ' '
      write (3,*) ' '
      write (3,*) ' '
      write (3,*) 'pickpsf'
      write (3,*) ' '
      write (3,*) psfopt(1)
      write (3,*) ' '
      write (3,*) 'psf'
      write (3,*) ' '
      write (3,*) ' '
      write (3,*) ' '
      write (3,*) 'exit'
      call confrm (ndi, logfile, jobfile)
      call remove (switch(pict, '.coo'))
c
      call remove (switch(pict, '.als'))
      call run ('allstar', logfile)
      write (3,*) 'wa = 0'
      write (3,*) 'fi =', radii(1)
      write (3,*) ' '
      write (3,*) pict
      write (3,*) switch(pict, '.psf')
      write (3,*) switch(pict, '.nei')
      write (3,*) ' '
      write (3,*) ' '
      call confrm (ndi, logfile, jobfile)
      call remove (switch(pict, '.nei'))
c
      call remove (switch(pict, '.jnk'))
      call run ('daophot', logfile)
      write (3,*) 'no'
      write (3,*) 'no'
      write (3,*) 'attach ', pict
      write (3,*) 'sub'
      write (3,*) ' '
      write (3,*) '.als'
      write (3,*) 'y'
      write (3,*) ' '
      write (3,*) ' '
      write (3,*) 'attach ', switch(pict, 's')
      write (3,*) 'op'
      write (3,*) ' '
      write (3,*) 'wa = -2'
      write (3,*) 'va = 0'
      write (3,*) 'fi =', radii(1)
      write (3,*) 'ps =', radii(2)
      write (3,*) 'an =', psfopt(2)
      write (3,*) ' '
      write (3,*) 'psf'
      write (3,*) switch(pict, '.als')
      write (3,*) switch(pict, '.lst')
      write (3,*) switch(pict, '.jnk')
      write (3,*) 'exit'
      call confrm (ndi, logfile, jobfile)
      call rename (switch(pict, '.jnk'), switch(pict, '.psf'))
c
      call run ('allstar', logfile)
      write (3,*) 'wa = 0'
      write (3,*) 'fi =', radii(1)
      write (3,*) ' '
      write (3,*) pict
      write (3,*) switch(pict, '.psf')
      write (3,*) switch(pict, '.ap')
      write (3,*) switch(pict, '.jnk')
      write (3,*) ' '
      call confrm (ndi, logfile, jobfile)
      call rename (switch(pict, '.jnk'), switch(pict, '.als'))
      call remove (switch(pict, '.ap'))
c
      call remove (switch(pict, 's.coo'))
      call remove (switch(pict, 's.off'))
      call remove (switch(pict, 's.ap'))
      call run ('daophot', logfile)
      write (3,*) 'no'
      write (3,*) 'no'
      write (3,*) 'op'
      write (3,*) ' '
      write (3,*) 'threshold = ', thresh
      write (3,*) ' '
      write (3,*) 'attach ', switch(pict, 's')
      write (3,*) 'find'
      write (3,*) frames(1:nfr)
      write (3,*) ' '
      write (3,*) 'y'
      write (3,*) 'offset'
      write (3,*) switch(pict, 's.coo')
      write (3,*) '50000 0 0 0'
      write (3,*)
      write (3,*) 'photometry'
      write (3,*) table(1:nta)
      write (3,*) 'A2 = 0'
      write (3,*) ' '
      write (3,*) switch(pict, 's.off')
      write (3,*) ' '
      write (3,*) 'ex'
      call confrm (ndi, logfile, jobfile)
c
      call remove (switch(pict, 's.als'))
      call run ('allstar', logfile)
      write (3,*) 'wa = 0'
      write (3,*) 'fi =', radii(1)
      write (3,*) ' '
      write (3,*) switch(pict, 's')
      write (3,*) switch(pict, '.psf')
      write (3,*) switch(pict, 's.ap')
      write (3,*) ' '
      write (3,*) 'END OF FILE'
      call confrm (ndi, logfile, jobfile)
c
      call remove (switch(pict, '.cmb'))
      call remove (switch(pict, '.jnk'))
      call remove (switch(pict, '.nei'))
      call run ('daophot', logfile)
      write (3,*) 'no'
      write (3,*) 'no'
      write (3,*) 'append'
      write (3,*) switch(pict, 's.als')
      write (3,*) switch(pict, '.als')
      write (3,*) switch(pict, '.cmb')
      write (3,*) 'attach ', pict
      write (3,*) 'sub'
      write (3,*) switch(pict, '.psf')
      write (3,*) '.cmb'
      write (3,*) 'y'
      write (3,*) ' '
      write (3,*) ' '
      write (3,*) 'attach ', switch(pict, 's')
      write (3,*) 'op'
      write (3,*) ' '
      write (3,*) 'wa = -2'
      write (3,*) 'va = 1'
      write (3,*) 'fi =', radii(1)
      write (3,*) 'ps =', radii(2)
      write (3,*) 'an =', psfopt(2)
      write (3,*) ' '
      write (3,*) 'psf'
      write (3,*) switch(pict, '.als')
      write (3,*) switch(pict, '.lst')
      write (3,*) switch(pict, '.jnk')
      write (3,*) 'exit'
      call confrm (ndi, logfile, jobfile)
      call rename (switch(pict, '.jnk'), switch(pict, '.psf'))
      call remove (switch(pict, '.nei'))
      call remove (switch(pict, 's.coo'))
      call remove (switch(pict, 's.off'))
      call remove (switch(pict, 's.ap'))
      call remove (switch(pict, 's.grp'))
      call remove (switch(pict, 's.nst'))
c
      call run ('allstar', logfile)
      write (3,*) 'wa = 0'
      write (3,*) 'fi =', radii(1)
      write (3,*) ' '
      write (3,*) pict
      write (3,*) switch(pict, '.psf')
      write (3,*) switch(pict, '.cmb')
      write (3,*) switch(pict, '.jnk')
      write (3,*) ' '
      call confrm (ndi, logfile, jobfile)
      call rename (switch(pict, '.jnk'), switch(pict, '.als'))
      call remove (switch(pict, '.cmb'))
c
      call shutdwn (ndi, jobfile)
      call clfile (3)
      pict = 'EXIT'
      go to 900
      end!
c
c
c
      subroutine remove (file)
      character*30 file
      write (3,*) 'if (-f ', file, ') rm ', file
      return
      end!
c
c
c
      subroutine confrm (ndi, logfile, jobfile)
      character*30 logfile, jobfile
      if (ndi .gt. 0) then
         write (3,1) logfile, jobfile, logfile
    1    format ('DONE'/'set temp=`tail -2 ', a, ' | head -1`'/
     .       'if ("$temp" != "Good bye.") then'/
     .       'cat << DONE >>! ~/running'//
     .       'cpu *BOMBED* ', a/
     .       'DONE'/
     .       'date >>! ~/running'/
     .       'exit'/'endif'/
     .       'cat << DONE >>! ', a/
     .       'Continuing...'//'DONE')
      else
         write (3,2)
    2    format ('DONE')
      end if
      return
      end!
c
c
c
      subroutine rename (file1, file2)
      character*(*) file1, file2
      write (3,*) 'mv ', file1, ' ', file2
      return
      end
c
c
c
      subroutine delete (file)
      character*(*) file
      write (3,*) 'rm ', file
      return
      end
c
c
c
      subroutine startup (direc, ndi, pict, jobfile, logfile)
      character*30 switch
      character*(*) direc, pict, jobfile, logfile
      write (3,*) 'source ~pstetson/.daophot'
      if (ndi .gt. 0) then
         write (3,*) 'set cpu = `hostname`'
         write (3,*) 'set job = ', pict
         write (3,*) 'cat >>! ~/running << DONE'
         write (3,*) ' '
         write (3,*) '$cpu is starting ', jobfile
         write (3,*) 'DONE'
         write (3,*) 'date >>! ~/running'
         write (3,*) 'cd ', direc(1:ndi)
      end if
      logfile = '$scr/'//switch(pict, '.log')
      return
      end!
c
c
c
      subroutine run (progrm, logfile)
      character*(*) progrm, logfile
      write (3,*) 'daophot << DONE >>! ', logfile
      return
      end!
c
c
c
      subroutine shutdwn (ndi, jobfile)
      character*(*) jobfile
      if (ndi .gt. 0) then
         write (3,*) 'cat >>! ~/running << DONE'
         write (3,*) ' '
         write (3,*) '$cpu finished ', jobfile
         write (3,*) 'DONE'
         write (3,*) 'date >>! ~/running'
      end if
      return
      end!
