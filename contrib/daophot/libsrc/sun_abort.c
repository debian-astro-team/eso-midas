/* @(#)sun_abort.c	19.1 (ES0-DMD) 02/25/03 13:23:51 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

#include <floatingpoint.h>
#include <signal.h>
#include <stdio.h>
extern catch_div_by_zero(), catch_overflow(), catch_invalid();
/* extern catch_inexact(), catch_underflow(); */

sun_abort_()
/*
  This routine exists to force a program to abort when it encounters
  a floating point error such as a divide by zero, an overflow, or
  an invalid operation.  This used to be standard behavior until IEEE
  came along.  Unfortunately, this code is entirely Sun specific!
  It used to flush away inexact and underflow errors.  We have disabled
  the feature of flushing away inexact and underflow both because it
  slows the program down tremendously and because of philosophical
  questions about whether that is a good idea!  As far as I can tell,
  SIGFPE_IGNORE is the default action on underflow and inexact and is
  included here only for completeness' sake.
*/

{
/*  if (ieee_handler ("set", "inexact", catch_inexact))
    printf ("Can't set inexact.\n"); */
  if (ieee_handler ("set", "inexact", SIGFPE_IGNORE))
    printf ("Can't set inexact.\n");
  if (ieee_handler ("set", "division", catch_div_by_zero))
    printf ("Can't set division.\n");
/*  if (ieee_handler ("set", "underflow", catch_underflow))
    printf ("Can't set underflow.\n"); */
  if (ieee_handler ("set", "underflow", SIGFPE_IGNORE))
    printf ("Can't set underflow.\n");
  if (ieee_handler ("set", "overflow", catch_overflow))
    printf ("Can't set overflow.\n");
  if (ieee_handler ("set", "invalid", catch_invalid))
    printf ("Can't set invalid.\n");
}

catch_div_by_zero (sig, code, scp)
int sig, code;
struct sigcontext *scp;
{
  printf ("Divide by zero occurred at pc %X\n", scp->sc_pc);
  abort();
}

catch_overflow (sig, code, scp)
int sig, code;
struct sigcontext *scp;
{
  printf ("Floating-point overflow occurred at pc %X\n", scp->sc_pc);
  abort();
}

catch_invalid (sig, code, scp)
int sig, code;
struct sigcontext *scp;
{
  printf ("Invalid floating-point operation at pc %X\n", scp->sc_pc);
  abort();
}

catch_inexact (sig, code, scp)
int sig, code;
struct sigcontext *scp;
{
  char *to;
  ieee_flags ("clear", "exception", "inexact", &to);
}

catch_underflow (sig, code, scp)
int sig, code;
struct sigcontext *scp;
{
  char *to;
  ieee_flags ("clear", "exception", "underflow", &to);
}
