$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.DAOPHOT.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:52 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   addstar.for
$ FORTRAN   allstsubs.for
$ COPY vmsdaosubs.for daosubs.for
$ FORTRAN   daosubs.for
$ FORTRAN   find.for
$ FORTRAN   fotometry.for
$ FORTRAN   fudge.for
$ FORTRAN   group.for
$ FORTRAN   iosubs.for
$ FORTRAN   mathsubs.for
$ FORTRAN   midsubs.for
$ FORTRAN   nstar.for
$ FORTRAN   pckpsf.for
$ FORTRAN   peak.for
$ FORTRAN   psf.for
$ FORTRAN   sort.for
$ FORTRAN   substar.for
$ LIB/REPLACE libdaophot addstar.obj,allstsubs.obj,daosubs.obj
$ LIB/REPLACE libdaophot find.obj,fotometry.obj,fudge.obj,group.obj,iosubs.obj
$ LIB/REPLACE libdaophot mathsubs.obj,midsubs.obj,nstar.obj,pckpsf.obj
$ LIB/REPLACE libdaophot peak.obj,psf.obj,sort.obj,substar.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
