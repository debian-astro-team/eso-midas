# @(#)DESCRIPTION	19.1 (ESO) 02/25/03 13:23:32
DAOPHOT is a package to obtain precise photometry and astrometric positions
for stellar objects in two-dimensional digital images.

DAOPHOT is intended to run as non-interactive as possible. DAOPHOT performs 
no operations related to display or manipulation of the digital image on the 
image-display system, even though at some stages in the data reduction it is 
important to be able to examine the picture visually. 
DAOPHOT performs the following tasks:
 - find stellar objects above a certain threshold;
 - derive concentric aperture photometry for the selected objects;
 - determine a point spread function for the frame from one of more stars;
 - compute precise positions and magnitudes of the program stars.

It is assumed that before running DAOPHOT, (1) the user will have performed 
all necessary preparations of the images, such as flat fielding, bias 
subtraction and trimming worthless rows and columns, and (2) that the 
brightness data in the image are linearly related to the true intensities.

DAOPHOT was written by Peter Stetson of the Dominion Astrophysical Observatory.

Latest update: May 1991.
