! @(#)daophot.prg	19.1 (ES0-DMD) 02/25/03 13:24:04
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: DAOPHOT.PRG
!.PURPOSE:        Do stellar photometry
!.USE:            DAOPHOT/DAOPHOT
!.AUTHOR:         Rein H. Warmels, ESO-IPG
!.VERSION:        910430 RHW Documentation added
!.VERSION:        910821 RHW Conditional copy of option files
! ----------------------------------------------------------------------
IF AUX_MODE(1) .LE. 1 THEN              ! VMS
   define/local daodir/c/1/60 -
     "MID_DISK:[&MIDASHOME.&MIDVERS.CONTRIB.DAOPHOT.INCL] "
   IF M$EXIST("daophot.opt") .EQ. 0 THEN
      $ COPY 'daodir'daophot.opt []
      $ SET PROT=(O:RWED) daophot.opt
   ENDIF
   IF M$EXIST("photo.opt") .EQ. 0 THEN
      $ COPY 'daodir'photo.opt []
      $ SET PROT=(O:RWED) photo.opt
   ENDIF
ELSE                                    ! UNIX
   define/local daodir/c/1/60 "$MIDASHOME/$MIDVERS/contrib/daophot/incl/ "
   IF M$EXIST("daophot.opt") .EQ. 0 THEN
      $ cp 'daodir'daophot.opt `pwd`
      $ chmod 644 daophot.opt
   ENDIF
   IF M$EXIST("photo.opt") .EQ. 0 THEN
      $ cp 'daodir'photo.opt `pwd`
      $ chmod 644 photo.opt
   ENDIF
ENDIF
!
RUN CON_EXE:DAOPHOT
