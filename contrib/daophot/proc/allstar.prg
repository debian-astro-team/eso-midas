! @(#)allstar.prg	19.1 (ES0-DMD) 02/25/03 13:24:03
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ALLSTAR.PRG
!.PURPOSE:        Do simultaneous-multiple-fiiting of the psf
!.USE:            ALLSTAR/DAOPHOT
!.AUTHOR:         Rein H. Warmels, ESO-IPG
!.VERSION:        910430 RHW Documentation added
!.VERSION:        910821 RHW Conditional copy of option file
! ----------------------------------------------------------------------
IF AUX_MODE(1) .LE. 1 THEN              ! VMS
   define/local daodir/c/1/60 -
     "MID_DISK:[&MIDASHOME.&MIDVERS.CONTRIB.DAOPHOT.INCL] "
   IF M$EXIST("allstar.opt") .EQ. 0 THEN
      $ COPY 'daodir'allstar.opt []
      $ SET PROT=(O:RWED) allstar.opt
   ENDIF
ELSE                                    ! UNIX
   define/local daodir/c/1/60 "$MIDASHOME/$MIDVERS/contrib/daophot/incl/ "
   IF M$EXIST("allstar.opt") .EQ. 0 THEN
      $ cp 'daodir'allstar.opt `pwd`
      $ chmod 644 allstar.opt
   ENDIF
ENDIF
!
RUN CON_EXE:ALLSTAR
