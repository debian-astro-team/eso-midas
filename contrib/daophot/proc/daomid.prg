! @(#)daomid.prg	19.1 (ES0-DMD) 02/25/03 13:24:04
!Puprpose: Conversion of daophot tables to MIDAS tables
!          FILE.COO ====> FILECOO.TBL
!          FILE.PK  ====> FILEPK.TBL
!          FILE.NST ====> FILENSP.TBL
!
DEFINE/PARAM P1 ? TABLE  "Enter input (DAOPHOT ASCII) table name"
WRITE/KEYW IN_A 'P1'
!
RUN CON_EXE:TBLDAOMID.EXE
