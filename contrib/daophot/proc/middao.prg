! @(#)middao.prg	19.1 (ES0-DMD) 02/25/03 13:24:04
!+++++++++
!.Purpose: Convert MIDAS tables into DAOPHOT tables
!------------
DEFINE/PARAM P1 ? TABLE "Enter MIDAS table name (without .TBL extension)"
WRITE/KEYW  IN_A 'P1'
!
RUN CON_EXE:TBLMIDDAO.EXE
