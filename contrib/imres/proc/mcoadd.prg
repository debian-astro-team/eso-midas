! @(#)mcoadd.prg	19.1 (ES0-DMD) 02/25/03 13:25:10
! MCOADD.PRG
!
! Coaddition with acceleration and sub-sampling
!
! Richard Hook, ST-ECF, September 1992
!
! Procedure for control within MIDAS
!
define/param p1 ? ima "List of input data images: "
define/local Images/c/1/80 {p1}
!
define/param p2 ? ima "List of matching PSF images: "
define/local PSFs/c/1/80 {p2}
!
define/param p3 ? n "Number of Iterations: "
define/local Niter/i/1/1 {p3}
!
define/param p4 ? ima "Deconvolved output image: "
define/local Decon/c/1/80 {p4}
!
define/param p5 ? ima "Root name for coadded output images: "
define/local Coaddr/c/1/80 {p5}
!
define/param p6 YYNN ? "Flags (verbose|accel|initial_image|subsamp default YYNN): "
define/local Verbose/c/1/1 {p6(1:1)}
define/local Accel/c/1/1 {p6(2:2)}
define/local First/c/1/1 {p6(3:3)}
define/local Subsamp/c/1/1 {p6(4:4)}
!
if Subsamp .eq. "Y" then
   define/param p7 ? n "Sub-sampling in X,Y: "
   write/keyw inputr {p7}
   define/local Xsubsam/i/1/1 {inputr(1)}
   define/local Ysubsam/i/1/1 {inputr(2)}
else
   define/local Xsubsam/i/1/1 1
   define/local Ysubsam/i/1/1 1
endif
!
if First .eq. "Y" then
   define/param p8 ? cha "Name of first estimate image: "
   define/local FirstIm/c/1/80 {p8}
endif
!
run CON_EXE:mcoadd.exe
