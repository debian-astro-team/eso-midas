$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.IMRES.SRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:51 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   mcoadd.for
$ LINK/NOMAP/EXE=[--.exec] mcoadd.obj, libimres/L, libmidas/L, -Wl,-rpath .home.midasmgr.midas.release.lib
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
