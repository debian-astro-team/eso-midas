!++++++++++++++++++++++++++++++++++++++++++++++++
!
!    MIDAS procedure  geotest.prg
!    with entries for RAMP, SPEC1, SPEC2, SPEC3, WAVE
!    for commands CREATE/RAMP, /WAVE, /SPC1, /SPC2, /SPC3
!
!.PURPOSE
!    Generate test images
!
!.SYNTAX
!
!    @c geotest,ramp image slope angle dimension
!
!    where
!
!    image  -  the output image name,
!    slope  -  real slope (units/pixel along gradient),
!              default = 1.0,
!    angle  -  integer position angle (degrees),
!              default = 0,
!    dimension - integer dimensions of axes of image,
!                default = 128.
!
!
!    @c geotest,spec1 image slope amplitude period phase dimension
!
!    where
!
!    image     -  the    output image name,
!    slope     -  real wave slope,
!                 default = 0.0,
!    amplitude -  real, wave amplitude,
!                 default = 10.0,
!    period    -  real period of wave,
!                 default = 8.5,
!    phase     -  real wave phase,
!                 default = 0.0
!    dimension -  integer dimensions of axes of image,
!                 default = 128.
!
!
!    @c geotest,spec2 image period slope amplitude phase dimension
!
!    where
!
!    image     -  the    output image name,
!    period    -  integer period (1 to 20 are valid values),
!    slope     -  real wave slope,
!                 default = 0.0,
!    phase     -  integer wave phase,
!                 default = 1
!    dimension -  integer dimensions of axes of image,
!                 default = 128.
!
!
!    @c geotest,spec3 image psf-option centring table boxwidth-or-fwhm
!
!    where
!
!    image      -  the output image name.
!    psf-option -  a box of specified width (B, default); or
!                  a gaussian of specified full width half max. (G).
!    centring   -  positions of lines vis-a-vis pixel centres.
!                  Default in 0.645.
!    table      -  input table, giving positions of lines.
!                  Default is LINES.TBL.
!    boxwidth   -  width of "box" PSF; if .LE. 0.001, then a delta
!                  function is used instead.
!    (or)
!    fwhm       -  full width half maximum (greater than 0.2).
!                  Default of boxwidth or of fwhm is 1.0.
!
!
!    @c geotest,wave image amplitude period dimension
!
!    where
!
!    image     -  the output image name,
!    amplitude -  real wave amplitude,
!                 default = 10.0,
!    period    -  real period of wave,
!                 default = 8.5,
!    dimension -  integer dimensions of axes of image,
!                 default = 128.
!
!
!.AUTHOR
!
!    D. Ponz, ESO - IPG, Garching               860622
!    F. Murtagh, ST-ECF, Garching               860707
!    K. Banse, ESO - IPG, Garching              901107, 910108, 070522
!
!-----------------------------------------------------------
!
! **********************
!     ENTRY RAMP
! **********************
!
ENTRY RAMP
DEFINE/PARAM P1 ? IMA "Enter output image name:"
DEFINE/PARAM P2 1.0 NUMBER
DEFINE/PARAM P3 0 NUMBER
DEFINE/PARAM P4 128 NUMBER
!
WRITE/KEYW OUT_A {P1}
WRITE/KEYW INPUTR/R/1/1 {P2}
WRITE/KEYW INPUTI/I/1/1 {P3}
WRITE/KEYW INPUTI/I/2/1 {P4}
RUN CON_EXE:RAMP
!
! **********************
!     ENTRY SPEC1
! **********************
!
ENTRY SPEC1
DEFINE/PARAM  P1  ?    IMA  "Enter output image name:"
DEFINE/PARAM  P2  0.0  NUMBER
DEFINE/PARAM  P3  10.0 NUMBER
DEFINE/PARAM  P4  8.5  NUMBER
DEFINE/PARAM  P5  0.0  NUMBER
DEFINE/PARAM  P6  128  NUMBER
!
WRITE/KEYW OUT_A        {P1}
WRITE/KEYW INPUTR/R/1/1 {P2}
WRITE/KEYW INPUTR/R/2/1 {P3}
WRITE/KEYW INPUTR/R/3/1 {P4}
WRITE/KEYW INPUTR/R/4/1 {P5}
WRITE/KEYW INPUTI/I/1/1 {P6}
RUN CON_EXE:SPEC1
!
! **********************
!     ENTRY SPEC2
! **********************
!
ENTRY SPEC2
DEFINE/PARAM  P1  ?   IMA  "Enter output image name:"
DEFINE/PARAM  P2  10  NUMBER
DEFINE/PARAM  P3  0.0 NUMBER
DEFINE/PARAM  P4  1   NUMBER
DEFINE/PARAM  P5  128 NUMBER
!
WRITE/KEYW OUT_A           {P1}
WRITE/KEYW INPUTR/R/1/1    {P3}
WRITE/KEYW INPUTI/I/1/1    {P4}
WRITE/KEYW INPUTI/I/2/1    {P5}
WRITE/KEYW INPUTI/I/3/1    {P2}
RUN CON_EXE:SPEC2
!
! **********************
!     ENTRY SPEC3
! **********************
!
ENTRY SPEC3
DEFINE/PARAM  P1  ?     IMA  "Enter output image name:"
DEFINE/PARAM  P2  B     CHARACTER
DEFINE/PARAM  P3  0.645 NUMBER
DEFINE/PARAM  P4  LINES TABLE
DEFINE/PARAM  P5  1.0   NUMBER
!
WRITE/KEYW OUT_A        {P1}
WRITE/KEYW INPUTC/C/1/1 {P2}
WRITE/KEYW INPUTR/R/1/1 {P3}
IF INPUTR .LT. 0.0 THEN
  WRITE/OUT "Offset vis a vis pixel centre must be positive"
  RETURN
ENDIF
IF INPUTR .GT. 1.0 THEN
  WRITE/OUT "Offset vis a vis pixel centre must be less than 1.0"
  RETURN
ENDIF
!
WRITE/KEYW IN_TAB/C/1/8 {P4}
WRITE/KEYW INPUTR/R/2/1 {P5}
IF P2(1:1) .EQ. "G" THEN
   RUN CON_EXE:SPEC3G                  ! Gaussian psf used.
ELSE
   RUN CON_EXE:SPEC3B                  ! "Box" psf used.
ENDIF
!
! **********************
!     ENTRY WAVE
! **********************
!
ENTRY WAVE
DEFINE/PARAM P1 ? IMA "Enter output image name:"
DEFINE/PARAM P2 10.0 NUMBER
DEFINE/PARAM P3 8.5  NUMBER
DEFINE/PARAM P4 128  NUMBER
!
WRITE/KEYW OUT_A {P1}
WRITE/KEYW INPUTR/R/1/1 {P2}
WRITE/KEYW INPUTR/R/2/1 {P3}
WRITE/KEYW INPUTI/I/1/1 {P4}
RUN CON_EXE:WAVES
