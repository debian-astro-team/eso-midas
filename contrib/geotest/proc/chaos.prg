! @(#)chaos.prg	19.1 (ES0-DMD) 02/25/03 13:24:43
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure chaos.prg 
! K. Banse	901107, 901212, 951010
!
! execute via   @c chaos  name p2 p3 func_type coefs
! or            @c chaos  name = ref_frame func_type coefs
! 
! with
!       p2 = NAXIS,NPIX(1),NPIX(2),NPIX(3),...
!       p3 = START(1),...,STEP(3),...
!       func_type = MANDEL, JULIA, COLWHEEL, SYMMETRIC, LORENZ or ELFLY
!	coefs = coefficients for functions
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!
! 	here the reference file option
!
!
define/param p1 ? ima "Enter image name:"
!
define/param p4 LORENZ  C "Enter function type: "
!
if p4(1:1) .eq. "E" then
   define/param p5 2.24,.43,-.65,-2.43,1.,300000 C "Enter coefficients: "
elseif p4(1:1) .eq. "L" then
   define/param p5 1,1,1,.01,28,0.1,10000 C "Enter coefficients: "
elseif p4(1:1) .eq. "M" then
   define/param p5 .31,.04,100. C "Enter coefficients: "
elseif p4(1:1) .eq. "J" then
   define/param p5 .31,.04,100. C "Enter coefficients: "
elseif p4(1:1) .eq. "S" then
   define/param p5 100.,100.,1.89,-1.1,0.17,-0.79,100000. C -
   "Enter coefficients: "
endif
!
if p2(1:1) .eq. "=" then		!check for reference frame
!
! real image stuff...
   define/param p3 ? ima "Enter reference image name:"
   default(1:1) = "N"
else
!
! here the NAXIS,NPIX(i) START(i),STEP(i) option
   define/param p2 2,512,512 N "Enter Naxis,Npix(1),Npix(2),..,Npix(Naxis): "
   default(1:1) = "Y"
   write/keyw inputi {p2}		!NAXIS + NPIX
   branch inputi(1) 1,2,3 NAX1,NAX2,NAX3
!
   NAX1:
   define/param p3 -1.3,0.005 N "Enter Start(1),..,Step(1),..: "
   goto out
! 
   NAX2:
   define/param p3 -1.3,-1.3,0.005,0.005 N "Enter Start(1),..,Step(1),..: "
   goto out
! 
   NAX3:
   define/param -
   p3 -1.3,-1.3,-1.3,0.005,0.005,0.005  N "Enter Start(1),..,Step(1),..: "
! 
OUT:
   write/keyw inputd {p3}				!START + STEP
!
endif
!
RUN_IT:
write/keyw in_a {p1}				!name of new frame
run CON_EXE:CHAOS

