! @(#)sdp.prg	19.1 (ES0-DMD) 02/25/03 13:24:44
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure sdp.prg 
! K. Banse	901107, 901212
! 
!  to create a 2-dim symmetric dot pattern from a 1-dim frame
!
! execute via   @c sdp 1dim_in 2dim_out ntop,lag,colora,colorb,incflag
! 
!   where   ntop = max. of integer scaling interval
!                  that also determines the size of the result frame:
!                  NPIX(1),NPIX(2) = (ntop*2) + 1
!           lag = distance between two pixels compared
!           colora,colorb = the values of the result pixels depending
!                           upon if input(n+lag) >= input(n)
!           if incflag = 1, result pixels are summed up (colora only!)
!           else,           result pixels are just colora
! 
! 
!   defaulted to 50,1,1,1,1
! 
!   the spectrum will be first scaled into the interval [0,ntop]
!   using the cut values of it
!   then a symmetric dot pattern will be created
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ima "Enter input image: "
define/param p2 sdp ima "Enter output image: "
define/param p3 50,1,1,1,1 n "Enter halfsize,lag,col1,col2,inc_flag: "
write/keyw in_a 'p1'
write/keyw out_a 'p2'
write/keyw inputi 'p3'
run CON_EXE:sdp
