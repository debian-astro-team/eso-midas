! @(#)wfocus.prg	19.1 (ES0-DMD) 02/25/03 13:28:19
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! @(#)wfocus.prg	19.1 (ESO-ASD) 02/25/03 13:28:19
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT      wfocus.prg
!.PURPOSE    Wedge focus in the displayed image
!.USE        @c wfocus
!.AUTHOR     Cristian Levin
!.VERSION    1.0       01/06/92  Creation
!.VERSION    2.0       17/05/94  Cleaned up - K.Banse
!.VERSION    2.1       10/01/95  implementation of parameters file (CL)
!.VERSION    2.2       14/09/95  remove CLEAR/CHAN OVER - KB
!.KEYWORDS
! --------------------------------------------------------------
!
define/param p8 + ? "Enter input_spec for center/gauss:"
if p8(1:1) .eq. "+" then
   define/local centin/c/1/6 CURSOR
else
   define/local centin/c/1/60 {p8}
endif
!
! EXTERNAL VARIABLES:
! teloffs1, teloffs2, camoffs1, camoffs2, signwedg, distwedg
!
define/local dx/r/1/1 0.0
define/local dy/r/1/1 0.0
define/local dtelf/r/1/1 0.0
define/local yleft/r/1/1 0.0
define/local yright/r/1/1 0.0
define/local dcamf/r/1/1 0.0
! 
center/gauss {centin} focleft
if outputi(1) .eq. 0 then
   write/out
   write/out Invalid number of coordinates.
   return
endif
! 
dx = distwedg
! 
compute/table focleft :xstart = :xstart + {dx}
compute/table focleft :xend = :xend + {dx}
! 
define/local ioff/i/1/2 0,0
if mid$sess(16:20) .eq. "94NOV" then
   ioff = ididev(6)*ididev(15)+1           !offset into keyword IDIMEMC
else
   ioff = 1			           !offset into keyword IDIMEMC
endif
ioff(2) = ioff(1)+19
define/local cmc/c/1/20 {idimemc({ioff}:{ioff(2)})}
center/gauss {cmc},focleft focright
! 
set/midas output=no ! we don't want output on the screen...
! 
statist/table focleft :ycen
yleft = outputr(3)     ! minimum value
statist/table focright :ycen
yright = outputr(3)    ! minimum value
! 
set/midas output=yes
! 
! Y-shift
dy = signwedg * (yright-yleft)
!
! TELESCOPE focus offset
dtelf = teloffs1 * dy * teloffs2
!
! RILD camera focus offset
dcamf = camoffs1 * dy * camoffs2
! 
write/out
write/out
set/format F9.3
write/out Y-shift = {dy}
write/out RILD camera focus offset = {dcamf}
write/out
set/format F9.3
write/out TELESCOPE focus offset = {dtelf}
! 
outputr(1) = dy
outputr(2) = dcamf
outputr(3) = dtelf
