! @(#)shslit.prg	19.1 (ES0-DMD) 02/25/03 13:28:19
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! @(#)shslit.prg	19.1 (ESO-ASD) 02/25/03 13:28:19
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT      shslit.prg
!.PURPOSE    draw an horizontal or vertical slit, depending
!            on the value of the image descriptor _EI_MODE:
!                       RILD         --> vertical line
!                       REMD or BLMD --> horizontal line
!.USE        @c shslit
!.AUTHOR     Cristian Levin
!.VERSION    1.0       01/06/92  Creation
!.VERSION    2.0       17/05/94  Cleaned up - K.Banse
!.VERSION    2.1       10/01/95  implementation of parameters file (CL)
!.VERSION    2.2       10/01/95  addition of parameter p3
!.KEYWORDS
! -----------------------------------------------------------
!
! PARAMETERS:
! p1 --> x-position
! p2 --> y-position
! p3 --> exposure type (RILD, REMD, BLMD)
!
define/param p1 ? N "Enter X position : "
define/param p2 ? N "Enter Y position : "
define/param p3 ? C "Enter exposure type (RILD, REMD, BLMD) : "
! 
define/local xend/r/1/1 0
define/local yend/r/1/1 0
define/local lowlex/i/1/1 0
define/local lowley/i/1/1 0
define/local uprigx/i/1/1 0
define/local uprigy/i/1/1 0
define/local cmode/c/1/8 " " all
define/local ioff/i/1/2 0,0
!
define/local exmode/i/1/1 0
ioff = ididev(6)*ididev(15)+1           !offset into keyword IDIMEMC
ioff(2) = ioff(1)+19
define/local cmc/c/1/20 {idimemc({ioff}:{ioff(2)})}
! 
write/key cmode {p3}
!
set/midas output=no  ! we don't want output on the screen
! 
lowlex = {p1} - radslit
uprigx = {p1} + radslit
lowley = {p2} - radslit
uprigy = {p2} + radslit
! 
xend = {{idimemc},START(1)} + ({{idimemc},NPIX(1)}-1) * {{idimemc},STEP(1)}
yend = {{idimemc},START(2)} + ({{idimemc},NPIX(2)}-1) * {{idimemc},STEP(2)}
! 
set/overlay
! 
if cmode(1:4) .eq. "RILD" then				! RILD
   draw/line {p1},{{cmc},START(2)},{p1},{yend} F
   draw/circle {lowlex},{lowley},{uprigx},{uprigy} F
else							! REMD or BLMD
   draw/line {{cmc},START(1)},{p2},{xend},{p2} F
   draw/circle {lowlex},{lowley},{uprigx},{uprigy} F
endif
! 
set/midas output=yes
