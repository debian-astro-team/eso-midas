! @(#)obsrscat.prg	19.1 (ES0-DMD) 02/25/03 13:28:18
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! @(#)obsrscat.prg	19.1 (ESO-ASD) 02/25/03 13:28:18
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT      obsrscat.prg
!.PURPOSE    Reset a catalog deleting its entries. It is an internal
!            procedure called by the observing batches graphical 
!            interface.
!.USE        @c obsrscat
!.AUTHOR     Cristian Levin
!.VERSION    2.0       12/08/94
!.KEYWORDS
!-------------------------------------------------------------------
!
! SYSTEM DEPENDENCIES:
! It calls the Unix command "rm"
!
def/par P1 ? C "Enter catalog name: "

$ /bin/rm -f 'p1'.cat
create/icat 'p1' 'p1'*.bdf
