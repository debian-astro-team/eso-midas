! @(#)obsinit.prg	19.1 (ES0-DMD) 02/25/03 13:28:18
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! @(#)obsinit.prg	19.1 (ESO-ASD) 02/25/03 13:28:18
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT      obsinit.prg
!.PURPOSE    Initialize the parameters for the observing
!            batches.
!.USE        @c obsinit
!.AUTHOR     Cristian Levin
!.VERSION    1.0       10/01/95  Creation
!.KEYWORDS
! ------------------------------------------------------
! 
! Scale values for the display of the image
write/key ldscal_r/i/1/2 	1,1
write/key ldscal_b/i/1/2 	1,1
write/key ldscal_s/i/1/2 	1,1
!
! Instrument scales
write/key imscal_r/r/1/1	0.268
write/key imscal_b/r/1/1	0.37
write/key imscal_s/r/1/1	0.127
!
! Sign for the rotator offset computation
write/key signril/i/1/1	1
write/key signsus/i/1/1	1
write/key signlos/i/1/1	-1
!
! Angle for the rotator offset computation (acquisition frame)
write/key anglered/i/1/1	90
write/key anglesus/i/1/1	90
write/key angleblu/i/1/1	-90
!
! Sign for the rotator offset computation (acquisition frame)
write/key signred/i/1/1		1
write/key signsus/i/1/1		1
write/key signblu/i/1/1		-1
!
! Angle for the rotator offset computation (incoming frame)
write/key angleril/i/1/1	0
write/key anglelos/i/1/1	-90
!
! Max. number of counts in a pixel
write/key maxcts/i/1/1		60000
!
! distance between the peers for the wedge focus computation
write/key distwedg/r/1/1	34.0
!
! Y-shift sign for the wedge focus computation
write/key signwedg/i/1/1	-1
!
! Telescope focus offset constants
write/key teloffs1/r/1/1	-0.07715
write/key teloffs2/r/1/1	0.63334
!
! RILD camera focus offset constants
write/key camoffs1/r/1/1	30.3
write/key camoffs2/r/1/1	3.466
!
! Radius (in pixels) for the circle drawn on "show slit"
write/key radslit/i/1/1	10
!
! Rotator angle offset for pointing
write/key ptoffs_r/r/1/1	-0.88
! write/key ptoffs_r/r/1/1	-0.65
write/key ptoffs_b/r/1/1	0.0
write/key ptoffs_s/r/1/1	0.0
