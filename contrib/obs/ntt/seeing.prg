! @(#)seeing.prg	19.1 (ES0-DMD) 02/25/03 13:28:19
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! @(#)seeing.prg	19.1 (ESO-ASD) 02/25/03 13:28:19
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT      seeing.prg
!.PURPOSE    Computes the seeing in the displayed image.
!.USE        @c seeing
!.AUTHOR     Cristian Levin
!.VERSION    1.0       01/06/92  Creation
!.VERSION    2.0       17/05/94  Cleaned up - K.Banse
!.VERSION    2.1       10/01/95  implementation of parameters file (CL)
!.KEYWORDS
! ------------------------------------------------------
!
define/param p8 + ? "Enter input_spec for center/gauss:"
if p8(1:1) .eq. "+" then
   define/local centin/c/1/6 CURSOR
else
   define/local centin/c/1/60 {p8}
endif
! 
! ASSUMPTIONS:
! - Input image name begins with: "red", "blu" or "sus"
!
! EXTERNAL VARIABLES:
! From obsinit.prg: imscal_r, imscal_b, imscal_s, maxcts
! From get_mode.prg: obsmode
!
!
define/local fwx/r/1/1 0.0
define/local fwy/r/1/1 0.0
define/local sigfwx/r/1/1 0.0
define/local sigfwy/r/1/1 0.0
define/local ioff/i/1/2 0,0
! 
create/table seeing 15 0 null
! 
! get the image name
if mid$sess(16:20) .eq. "94NOV" then
   ioff = ididev(6)*ididev(15)+1           !offset into keyword IDIMEMC
else
   ioff = 1			           !offset into keyword IDIMEMC
endif
ioff(2) = ioff(1)+19
define/local cmc/c/1/20 {idimemc({ioff}:{ioff(2)})}
!
@c get_mode {cmc}       ! get the observing mode
!
if obsmode(1:4) .eq. "RILD" then
    define/local scale/r/1/1 {imscal_r}
else if obsmode(1:4) .eq. "BIMG" then
    define/local scale/r/1/1 {imscal_b}
else if obsmode(1:4) .eq. "SUSI" then
    define/local scale/r/1/1 {imscal_s}
else
    return
endif
! 
center/gauss {centin} seeing
if outputi(1) .eq. 0 then
    write/out 
    write/out Invalid number of coordinates.
    return
endif
! 
set/midas output=no			!suppress output
! 
select/table seeing :icent .le. {maxcts}
! 
statist/table seeing :xsig 
fwx = outputr(3) * 2.35482 * scale
sigfwx = outputr(4) * 2.35482 * scale
! 
statist/table seeing :ysig
fwy = outputr(3) * 2.35482 * scale
sigfwy = outputr(4) * 2.35482 * scale
! 
set/midas output=yes			!reenable output
write/out
write/out
set/format F6.2
write/out "FWHMx = {fwx} arcsecs         FWHMy = {fwy} arcsecs"
write/out "sigma-x = +/-{sigfwx} arcsecs    sigma-y = +/-{sigfwy} arcsecs"
! 
outputr(1) = fwx
outputr(2) = fwy
outputr(3) = sigfwx
outputr(4) = sigfwy

