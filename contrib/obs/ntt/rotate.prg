! @(#)rotate.prg	19.1 (ES0-DMD) 02/25/03 13:28:19
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! @(#)rotate.prg	19.1 (ESO-ASD) 02/25/03 13:28:19
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT      rotate.prg
!.PURPOSE    Computes the distance and angle between two
!            points entered using the display cursor.
!.USE        @c rotate
!.AUTHOR     Cristian Levin
!.VERSION    1.0       01/06/92  Creation
!.VERSION    2.0       17/05/94  Cleaned up - K.Banse
!.VERSION    2.2       10/01/95  implementation of parameters file (CL)
!.VERSION    2.3       12/01/95  addition of p2
!.KEYWORDS
! ------------------------------------------------------
! 
! PARAMETERS:
! p1 --> C[URSOR] or G[AUSS] (default)
! p2 --> RILD, REMD, BLMD, DIMD
!
! EXTERNAL VARIABLES:
! From obsinit.prg: anglelos, angleril, anglered, angleblu, anglesus, 
!                   signred, signblu, signsus
! From get_mode.prg: obsmode
!
define/param p1 G C
define/param p2 ? C "Enter observing mode : "
!
define/param p8 + ? "Enter input_spec for center/gauss:"
if p8(1:1) .eq. "+" then
   define/local centin/c/1/6 CURSOR
else
   define/local centin/c/1/60 {p8}
endif
! 
define/local angle/r/1/1 0.
define/local dist/r/1/1 0.
define/local x1/r/1/1 0.
define/local x2/r/1/1 0.
define/local y1/r/1/1 0.
define/local y2/r/1/1 0.
define/local x/r/1/1 0.
define/local y/r/1/1 0.
define/local x0/r/1/1 0.
define/local y0/r/1/1 0.
define/local xn/r/1/1 0.
define/local yn/r/1/1 0.
define/local dx/r/1/1 0.
define/local dy/r/1/1 0.
define/local xstart/r/1/1 0.
define/local ystart/r/1/1 0.
define/local xend/r/1/1 0.
define/local yend/r/1/1 0.
define/local ioff/i/1/2 0,0
define/local obsexp/c/1/10 " " all
! 
set/overlay
! 
write/key obsexp {p2}
!
create/table angle 15 0 null
! 
! get the image name
ioff = ididev(6)*ididev(15)+1           !offset into keyword IDIMEMC
ioff(2) = ioff(1)+19
define/local cmc/c/1/20 {idimemc({ioff}:{ioff(2)})}
!
@c get_mode {cmc} 	! get the observing mode
if obsmode(1:4) .eq. "NULL" then
    return
endif
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! get two points from the screen
!
if p1(1:1) .eq. "G" then
    center/gauss {centin} angle
    if outputi(1) .lt. 2 then
        write/out 
        write/out Invalid number of coordinates.
        return
    endif
    x1 = {angle,:xcen,1}
    x2 = {angle,:xcen,2}
    y1 = {angle,:ycen,1}
    y2 = {angle,:ycen,2}
else
    get/cursor angle
    if outputi(1) .lt. 2 then
        write/out 
        write/out Invalid number of coordinates.
        return
    endif
    x1 = {angle,:x_coord,1}
    x2 = {angle,:x_coord,2}
    y1 = {angle,:y_coord,1}
    y2 = {angle,:y_coord,2}
endif
if x1 .gt. x2 then
    x = x1
    y = y1
    x1 = x2
    y1 = y2
    x2 = x
    y2 = y
endif
! 
dx = x2 - x1
dy = y2 - y1
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! draw a line across the points
!
x0 = {{cmc},start(1)}
y0 = {{cmc},start(2)}
xn = {{cmc},start(1)} + {{cmc},step(1)}*({{cmc},npix(1)}-1)
yn = {{cmc},start(2)} + {{cmc},step(2)}*({{cmc},npix(2)}-1)
! 
if dx .eq. 0.0 then
    xstart = x1
    xend = x1
    ystart = y0
    yend = yn
    goto DRAW
else if dy .eq. 0.0 then
    xstart = x0
    xend = xn
    ystart = y1
    yend = y1
    goto DRAW
endif
!
y = y1 + (x0-x1)*dy/dx
x = x1 + (y0-y1)*dx/dy
! 
if y .lt. y0 then
    xstart = x
    ystart = y0
elseif y .gt. yn then
    x = x1 + (yn-y1)*dx/dy
    xstart = x
    ystart = yn
else
    xstart = x0
    ystart = y
endif
! 
y = y1 + (xn-x1)*dy/dx
x = x1 + (yn-y1)*dx/dy
! 
if y .gt. yn then
    xend = x
    yend = yn
elseif y .le. y0 then
    x = x1 + (y0-y1)*dx/dy
    xend = x
    yend = y0
else
    xend = xn
    yend = y
endif
! 
DRAW:
!
draw/line {xstart},{ystart},{xend},{yend} F
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! compute the angle and distance
!
dist = m$sqrt(dx*dx+dy*dy)
if dx .eq. 0 then
    angle = 90
else
    angle = m$atan(dy/dx) 
endif
! 
if obsmode(1:4) .eq. "RILD" then
   angle = signred * (angle + anglered)
else if obsmode(1:4) .eq. "SUSI" then
   angle = signblu * (angle + anglesus)
else if obsmode(1:4) .eq. "BIMG" then
   angle = signsus * (angle + angleblu)
endif
! 
if obsexp(1:4) .eq. "RILD" then
   angle = angle + angleril
else ! REMD, BLMD, DIMD
   angle = angle + anglelos
endif
!
write/out 
set/format F8.2
write/out "Distance between points (pixels) : {dist}"
write/out "Offset angle to be added to the current rotator offset : {angle}"
! 
outputr(1) = dist
outputr(2) = angle
