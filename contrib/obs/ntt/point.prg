! @(#)point.prg	19.1 (ES0-DMD) 02/25/03 13:28:18
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! @(#)point.prg	19.1 (ESO-ASD) 02/25/03 13:28:18
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT      point.prg
!.PURPOSE    pointing procedure
!.USE        @c point
!.AUTHOR     Cristian Levin
!.VERSION    1.0       01/06/92  Creation
!.VERSION    2.0       17/05/94  Cleaned up - K.Banse
!.VERSION    2.1       17/08/94  sleep stuff added (CL)
!.VERSION    2.2       10/01/95  implementation of parameters file (CL)
!.VERSION    2.3       13/09/95  Cosmetics - K.Banse
!.KEYWORDS
! ------------------------------------------------------
!
! PARAMETERS:
! p1 --> x-position of the slit
! p2 --> y-position of the slit
! p3 --> Rotator angle (default=0).
! p4 --> C[URSOR] or G[AUSS] (default=G)
!
! SYSTEM DEPENDENCIES:
! It calls the Unix command "sleep"
!
! EXTERNAL VARIABLES:
! From obsinit.prg: imscal_r, imscal_b, imscal_s, ptoffs_r, ptoffs_b, ptoffs_s
! From get_mode.prg: obsmode
!
! ASSUMPTIONS:
! Input image name begins with: "red", "blu" or "sus"
!
! ------------------------------------------------------
!
define/param p1 ? N "Enter X position of slit: "
define/param p2 ? N "Enter Y position of slit: "
define/param p3 0. N 
define/param p4 G C 
!
define/param p8 + ? "Enter input_spec for center/gauss:"
if p8(1:1) .eq. "+" then
   define/local centin/c/1/6 CURSOR
else
   define/local centin/c/1/60 {p8}
endif
! 
define/local ycen/r/1/1 0.0	! target y coordinate
define/local xcen/r/1/1 0.0	! target x coordinate
define/local dx/r/1/1 0.0	! target x offset
define/local dy/r/1/1 0.0	! target y offset
define/local alfa/r/1/1 0.0
define/local ra/r/1/1 0.0
define/local dec/r/1/1 0.0
define/local sen/r/1/1 0.0
define/local cos/r/1/1 0.0
define/local c1/r/1/1 0.0
define/local c2/r/1/1 0.0
define/local ioff/i/1/2 0,0
!
! sleep for a while because a lack of synchronization
! between graphical interface and center/gauss
! 
$sleep 1
! 
if p4(1:1) .eq. "G" then
    center/gauss {centin}
    if outputi(1) .eq. 0 then
        write/out
        write/out Invalid number of coordinates.
        return
    endif
    xcen = outputr(5)
    ycen = outputr(6)
else
    get/cursor
    if outputi(1) .eq. 0 then
        write/out
        write/out Invalid number of coordinates.
        return
    endif
    xcen = outputr(12)
    ycen = outputr(13)
endif
! 
dx = xcen - {p1}
dy = ycen - {p2}
! 
! get the image name
ioff = ididev(6)*ididev(15)+1           !offset into keyword IDIMEMC
ioff(2) = ioff(1)+19
define/local cmc/c/1/20 {idimemc({ioff}:{ioff(2)})}
!
@c get_mode {cmc}	! get the observing mode
!
if obsmode(1:4) .eq. "RILD" then
    alfa  = {p3} + ptoffs_r
    sen = m$sin(alfa)
    cos = m$cos(alfa)
    ra  = imscal_r*(dy*sen - dx*cos)
    dec = imscal_r*(dx*sen + dy*cos) 
else if obsmode(1:4) .eq. "BIMG" then
    alfa  = {p3} + ptoffs_b
    sen = m$sin(alfa)
    cos = m$cos(alfa)
    ra  =  imscal_b*(dx*cos - dy*sen)
    dec = -imscal_b*(dx*sen + dy*cos)
else if obsmode(1:4) .eq. "SUSI" then
    alfa  = {p3} + ptoffs_s
    sen = m$sin(alfa)
    cos = m$cos(alfa)
    ra  = imscal_s*(dy*sen - dx*cos)
    dec = imscal_s*(dx*sen + dy*cos) 
else
    return
endif
!
write/out   Rotator offset: {p3} degrees
set/format  F7.2
write/out   "Offsets:  RA = {ra} arcsecs     DEC = {dec} arcsecs"
! 
outputr(1) = ra
outputr(2) = dec
