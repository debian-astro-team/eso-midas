! @(#)get_mode.prg	19.1 (ES0-DMD) 02/25/03 13:28:18
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! @(#)get_mode.prg	19.1 (ESO-ASD) 02/25/03 13:28:18
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT      get_mode.prg
!.PURPOSE    Gets the observing mode corresponding to
!            the image coming as the parameter.
!.USE        @c get_mode image_name
!.AUTHOR     Cristian Levin
!.VERSION    1.0       18/01/95  Creation
!.VERSION    1.1       20/01/95  solution for missing _EI descriptors
!.RETURN     obsmode/c/1/10   -- possible values: RILD, BIMG, SUSI, NULL
! ----------------------------------------------------------------------
!
write/key obsmode/c/1/10 "NULL"
!
define/par p1 ? C "Enter image name : "
! 
define/local fram/c/1/80 {p1}
!
define/local exmode/i/1/1 0
define/local exid/i/1/1 0
!
exmode = m$existd(fram,"_EI_MODE")
exid = m$existd(fram,"_EI_ID")
if exmode .eq. 0 .or. exid .eq. 0 then
    goto NODESC
endif

define/local cid/c/1/8 {{fram},_EI_ID}
define/local cmode/c/1/8 {{fram},_EI_MODE}
!
if cid(1:4) .eq. "EMMI" then
   goto EMMI
else if cid(1:4) .eq. "SUSI" then
   goto SUSI
else
    write/out "Invalid value for the following descriptor:"
    write/out "   _EI_ID = {cid}   (valid values = (EMMI, SUSI) )"
    return
endif
!
EMMI:
if cmode(1:4) .eq. "RILD" then
    write/out "This is a red EMMI image..."
    write/key obsmode "RILD"
else if cmode(1:4) .eq. "BIMG" then
    write/out "This is a blue EMMI image..."
    write/key obsmode "BIMG"
else
    write/out "Invalid value for the following descriptor:"
    write/out "   _EI_MODE = {cmode} (valid values = (RILD,BIMG) )"
    return
endif
goto THE_END
!
SUSI:
write/out "This is a SUSI image..."
write/key obsmode "SUSI"
! 
THE_END:
write/out
return

NODESC:
write/out "_EI descriptors not present..."
!
if fram(1:3) .eq. "RED" then
    write/out "This is a red EMMI image..."
    write/key obsmode "RILD"
!
else if fram(1:3) .eq. "SUS" then
    write/out "This is a SUSI image..."
    write/key obsmode "SUSI"
!
else if fram(1:3) .eq. "BLU" then
    write/out "This is a blue EMMI image..."
    write/key obsmode "BIMG"
else
    write/out "Invalid filename. The observing mode cannot be determined."
    return
endif
write/out
return
