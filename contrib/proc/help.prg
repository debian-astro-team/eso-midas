! @(#)help.prg	19.1 (ES0-DMD) 02/25/03 13:29:19
! @(#)help.prg	8.3 (ESO-IPG) 10/12/94 10:42:38 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! The following contributed procedures exist :
!
! intiff            convert image in TIFF format to MIDAS format
!
! lutedit           execute an X11/Motif based LUT Editor
!
! xcols             display the current X11 color map 
!
!
! Use "HELP/CONTRIB procedure" to get more detailed info about a procedure
!                                                        950308  KB
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
