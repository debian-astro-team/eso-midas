%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993-2010 European Southern Observatory
%.IDENT      fit_lyma.hlq
%.AUTHOR     PB, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: FIT/LYMAN
%.VERSION    1.0  25-AUG-94 : Creation, PB
%----------------------------------------------------------------
\se
SECTION./LYMAN
\es\co
FIT/LYMAN					      25-AUG-94  PB
\oc\su
FIT/LYMAN  spec [outtab] [log] [session] [graph(Y/N)]
	     Interactive procedure for absorption line fitting.
\us\pu
Purpose:    
	    FIT/LYMAN is an interactive program for the fitting of absorption
	    lines in high resolution spectra. 
    	    It uses a menu-driven interface to allow the user to specify the 
	    lines to be fitted, and displays the data on the default graphic 
	    window.
	    It performs a global chi-square minimization using the MINUIT
	    package developed at CERN.
	    A complete trace of all the fits performed is stored in 
   	    suited tables, and can be used to re-start minimization. 
	    In contrast to most MIDAS packages, FITLYMAN  doesn't
	    use keywords. All parameters are permanently stored in 3 tables:
	    LYPAR.tbl, LYLIM.tbl and LYMIN.tbl.
	    For a full explanation of the menu functions, please refer to the 
	    manual.
\up\sy
Syntax:     
            FIT/LYMAN spec [outtab] [log] [session] [graph(Y/N)]
\ys\pa
	    spec = spectrum in table format.
		Table must contain 4 columns: :WAVE, :STDEV, :NORMFLUX  and
		:FWHM. An optional :PIXSIZE column is recommended.
		They can be created using the CONVERT/LYMAN command.
		This parameter MUST be specified only on the first
		call to the program. Later call uses  the SPECTRUMTABLE 
		descriptor of LYPAR.tbl
\ap\pa
	    outtab = table to store the results.
		On the first call of the program, default is  "spec"FIT.
		Otherwise it is the last name used,
		as stored in the OUTTAB descriptor of LYPAR.tbl
\ap\pa      
 	    log = prefix of the names of the tables that keeps track of the 
		minimization performed. They are "log"LYPAR (line 
		configuration),  "log"LYLIM (limits in wavelength over 
		which minimization is  done) and "log"LYMIN (instructions
		to MINUIT). Each fit performed is labeled with a unique ID
		number  which is stored  both in these tables and in the 
		outtab.
		On the first call of the program, default is the 
		"spec" parameter. Otherwise it is the last name used,
		as stored in the LOGNAME descriptor of LYPAR.tbl
\ap\pa			
	    session = name of a previously defined FIT/LYMAN set-up to be
		loaded at start-up.
\ap\pa			
	    graph = flag to enable automatic graphic output;
		defaulted to Y(es)
\ap\sa
See also: 
	    BATCH/LYMAN, COMPUTE/LYMAN, CONVERT/LYMAN,
	    FIT/DIRECT, PLOT/LYMAN
\as \exs
Examples:
\ex
            FIT/LYMAN  myspec 
              FITLYMAN is started reading and displaying myspec.tbl file.
              The resulting line list is stored in the table myspecFIT, and the 
	      log of the operations is saved in the tables myspecLYPAR, 
              myspecLYMIN and myspecLYLIM.
\ex
            FIT/LYMAN  myspec mylist mylog 
             FITLYMAN is started reading and displaying myspec.tbl file.
             The resulting line list is stored in the table mylist, and the 
	     log of the operations is saved in the tables mylogLYPAR, 
             mylogLYMIN and mylogLYLIM.
\ex
            FIT/LYMAN p5=NO 
             FITLYMAN is started using all the files used in the last call,
	     but graphics do not show up automatically.
\ex
	    FIT/LYMAN ? ? ? myold no
	     FITLYMAN is started using the files and the configuration stored
	     in the myoldLYPAR, myoldLYMIN and myoldLYLIM tables.
	     For graphic output you need the w (window) command.
\xe
\sxe


