C @(#)curslyman.for	19.1 (ESO-IPG) 02/25/03 13:28:03
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C       Internet e-mail: midas@eso.org
C       Postal address: European Southern Observatory
C                       Data Management Division 
C                       Karl-Schwarzschild-Strasse 2
C                       D 85748 Garching bei Muenchen 
C                       GERMANY
C===========================================================================
C

      program plotlyman
      
      implicit none
      include 'MID_REL_INCL:fit_var.inc'
      
                                ! copies of keywords coming from line
      character*80 SPESYS,SESSYS,sjunk


      integer ist,i,ijk,icur,iok
      real xdum,ydum,z,vel

        INTEGER MADRID(1)
        INCLUDE 'MID_INCLUDE:ST_DEF.INC'

        COMMON/VMR/MADRID

        INCLUDE 'MID_INCLUDE:ST_DAT.INC'

C!!!!!!!
C     MIDAS SET-UP
C!!!!!!!
      call STSPRO('fitlyman')
      
      call STKRDC('SPECTAB',1,1,80,i,SPESYS,ijk,ijk,iok)
      if (iok.ne.0) then
         call ErrMsg('Error in reading keywords: check context..')
         call STSEPI() 
         stop
      end if   
      call STKRDC('SESSNAM',1,1,80,i,SESSYS,ijk,ijk,iok)
      if (iok.ne.0) then
         call ErrMsg('Error in reading keywords: check context..')
         call STSEPI() 
         stop
      end if   



      
C!!!!!!!
C     Inizializzazione programma
C!!!!!!!
      if (SESSYS.ne.'NULL') then
         i=index(SESSYS,' ')
         sjunk=SESSYS(1:i-1)//PARTBL
      else
         sjunk=PARTBL
      end if
      call rsetup(sjunk,ist)  ! read set-up 

      if (ist.ne.0) then        ! on error, default values
         call ErrMsg('Error in reading set-up: run FIT/LYMAN again')
                                ! copy syskeys into variables
      end if

      i_grap = .true.
      call GetXCr(xdum,ydum,z,vel,icur)

      call STSEPI()

      end
      









