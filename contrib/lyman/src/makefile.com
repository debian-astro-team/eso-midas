$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.LYMAN.SRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:51 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN     backly.for
$ FORTRAN     fit_gen.for
$ FORTRAN     fit_min.for
$ FORTRAN     fit_user.for
$ FORTRAN     minuit.for
$ FORTRAN     setup.for
$ FORTRAN     gra.for
$ FORTRAN     rea.for
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
