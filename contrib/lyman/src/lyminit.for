C @(#)fitlyman.for	10.4 (ESO-IPG) 2/9/96 12:10:18
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C       Internet e-mail: midas@eso.org
C       Postal address: European Southern Observatory
C                       Data Management Division 
C                       Karl-Schwarzschild-Strasse 2
C                       D 85748 Garching bei Muenchen 
C                       GERMANY
C===========================================================================
C


      program lyminit
      
      implicit none
      include 'MID_REL_INCL:fit_var.inc'
      
                                ! copies of keywords coming from line
      character*80 SESSYS


c      character*30 choice
      character*70 sjunk


      integer ist,i,ijk,iok
      real c
      parameter (c=2.997e5)

C      include 'MID_REL_INCL:fit_mid.inc'
        INTEGER MADRID(1)
        INCLUDE 'MID_INCLUDE:ST_DEF.INC'

        COMMON/VMR/MADRID

        INCLUDE 'MID_INCLUDE:ST_DAT.INC'

C!!!!!!!
C     MIDAS SET-UP
C!!!!!!!
      call STSPRO('lyminit')
      
      call STKRDC('SESSNAM',1,1,80,i,SESSYS,ijk,ijk,iok)
      if (iok.ne.0) then
         call ErrMsg('Error in reading keywords: check context..')
         call STSEPI() 
         stop
      end if   


C!!!!!!!
C     Videata iniziale
C!!!!!!!
      call DisMsg('Initializing FITLYMAN...')
      call DisMsg('...Wait please...')
      
C!!!!!!!
C     Inizializzazione programma
C!!!!!!!
      if (SESSYS.ne.'NULL') then
         i=index(SESSYS,' ')
         sjunk=SESSYS(1:i-1)//PARTBL
      else
         sjunk=PARTBL
      end if
      call rsetup(sjunk,ist)  ! read set-up 

      if (ist.ne.0) then        ! on error, default values
         call WrnMsg('Error in reading set-up: using defaults')
         TURBLN=0
         i_grap = .false.
         call F_TBLR('SCRATCH',1,ist) !initializes lypar (in memory!)

      else

         call f_tblr(sjunk,1,ist) !read init. param.

      end if

      call f_tblw(PARTBL,1,ist)
c     call AskStp('PROGRAM',ist)
      call ssetup(PARTBL,ist)
      if (ist.ne.0) then
         call ErrMsg('Error in writing set-up: check disk space')
         call STSEPI() 
         stop
      end if   

c      call AtmRD(ist)            !read atompar.dat
c      NPUNTI=NMXSPE
c      call reaspe(FILSPE,NPUNTI)    !read spectrum
c      if (NPUNTI.lt.1) then
c         call STSEPI() 
c         stop
c      endif
c      i=index(FILLOG,' ')
c      filnam=fillog(1:i-1)//PARTBL
c      call GETIDN(filnam,IDnum,ist) 
c      if (ist.ne.0) then
c         call WrnMsg('Log files not found: starting from scratch')
c         IDnum=0
c      end if
                                ! read fit limits
      if (SESSYS.ne.'NULL') then
         i=index(SESSYS,' ')
         sjunk=SESSYS(1:i-1)//LIMTBL
      else
         sjunk=LIMTBL
      end if   
      call rintvl(sjunk,1,ist)  
      call sintvl(sjunk,1,ist) 

                                ! read minuit instructions
      if (SESSYS.ne.'NULL') then
         i=index(SESSYS,' ')
         sjunk=SESSYS(1:i-1)//MINTBL
      else
         sjunk=MINTBL
      end if   
      call rminui(sjunk,1,ist) 
      call sminui(sjunk,1,ist)

c      call ReaRes
c      call GraMai(ist)          ! plot 


c     call DisMsg('        ..done')
c     call sttdis(' ',0,ist)


      call STSEPI() 

      end
      


