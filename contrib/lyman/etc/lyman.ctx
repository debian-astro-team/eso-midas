!===========================================================================
! Copyright (C) 1995-2010 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
! MA 02139, USA.
!
! Correspondence concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Software Development Division
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! Procedure lyman.ctx       P. Ballester    910111
! 100907	last modif
!                             
! Purpose  :
!            SET/CONTEXT lyman
!            Define standard lyman commands
!
! -------------------------------------------------------------- 
!
! indicate origin of context
!HELP = contrib
! 
DEFINE/LOCAL LERR/I/1/1 {ERROR(2)}		!save current user level
SET/MIDAS USER=EXPERT
!
CREATE/COMMAND  BATCH/LYMAN        @c backly
CREATE/COMMAND  COMPUTE/LYMAN      @c comply
CREATE/COMMAND  CONVERT/LYMAN      @c convlyman
CREATE/COMMAND  CURSOR/LYMAN       @c cursly
CREATE/COMMAND  INITIAL/LYMAN      @c lyminit
CREATE/COMMAND  FIT/DIRECT         @c fitdir
CREATE/COMMAND  FIT/LYMAN          @c fitly
CREATE/COMMAND  PLOT/LYMAN         @c plotly
CREATE/COMMAND  PLOT/REDSHIFT      @c plotredsh

!CREATE/COMMAND  SET/LYMAN          @c setly

!
AUX_MODE(4) = 1            ! Copy all descriptors
ERROR(2) = LERR
!
WRITE/OUT  "*** Context Lyman enabled ***"
WRITE/OUT  " "
WRITE/OUT  "Commands:  BATCH/LYMAN [spe] [inphis] [outtab] [outhis] [gra Y/N]"
WRITE/OUT  "           COMPUTE/LYMAN linetab outsp [refspe] [option] [noise]"
WRITE/OUT  "           CONVERT/LYMAN image table stdev Resolution"
WRITE/OUT  "           CURSOR/LYMAN "
WRITE/OUT  "           INIT/LYMAN "
WRITE/OUT  "           FIT/DIRECT [spec] [outtab] [history] [session] [grap(Y/N)]"
WRITE/OUT  "           FIT/LYMAN  [spec] [outtab] [history] [session] [grap(Y/N)]"
WRITE/OUT  "           PLOT/LYMAN [spec] [lamcen1,..] [size1,..] [label1..] [session]"
WRITE/OUT  "           PLOT/REDSHIFT [spec] [z/lamcen] [size] [label1,..] [session]"
!WRITE/OUT  "           SET/LYMAN   "
!
INIT/LYMAN
!
