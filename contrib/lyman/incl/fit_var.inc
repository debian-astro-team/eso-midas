C  @(#)fit_var.inc	19.1 (ESO-DMD) 02/25/03 13:27:51
C ++++++++++++++++++++++++++++++
C 
C changed (increased) parameters NMXFRE, NMXSPE, NMXWRK 
C on a hint of Cedric Ledoux <cledoux@eso.org>
C K. Banse  ESO -DMD, GArching
C 
C 020614	last modif
C 
C ++++++++++++++++++++++++++++++
C!!!!!!!
C       Parameters for New version
C!!!!!!!
        character*60 PARTBL
        PARAMETER (PARTBL = 'LYPAR')
        character*60 LIMTBL
        PARAMETER (LIMTBL = 'LYLIM')
        character*60 MINTBL
        PARAMETER (MINTBL = 'LYMIN')


        integer NMXFRE,NMXLIN
        PARAMETER (NMXFRE=99)	
CKB: was (NMXFRE=50) before
        PARAMETER (NMXLIN=100)

        integer TURBLN,IMINOS
        COMMON/SYVARB/TURBLN,IMINOS

C!!!!!!!  lypar.tbl  variables
        DOUBLE PRECISION LamIni(NMXLIN),BIni(NMXLIN),NIni(NMXLIN)
        DOUBLE PRECISION LamMin(NMXLIN),BMin(NMXLIN),NMin(NMXLIN)
        DOUBLE PRECISION LamMax(NMXLIN),BMax(NMXLIN),NMax(NMXLIN)
        DOUBLE PRECISION LamSte(NMXLIN),BSte(NMXLIN),NSte(NMXLIN)
        DOUBLE PRECISION CoefL(NMXLIN),CoefB(NMXLIN),CoefN(NMXLIN)
        DOUBLE PRECISION BtuIni(NMXLIN),BtuMin(NMXLIN),BtuMax(NMXLIN)
        DOUBLE PRECISION BtuSte(NMXLIN),CoefBt(NMXLIN)
        DOUBLE PRECISION AtmLam(NMXLIN),AtmFos(NMXLIN),AtmGam(NMXLIN)
        DOUBLE PRECISION AtmMas(NMXLIN)
        integer grID(NMXLIN),nrows,NTOTPA
        character*14 ElmtNm(NMXLIN)
        character*4 CPar(4,NMXLIN)
        character Class(NMXLIN)
        
        COMMON/PARTAB/LamIni,BIni,NIni,LamMin,BMin,NMin,BtuSte,
     1     LamMax,BMax,NMax,LamSte,BSte,NSte,BtuIni,BtuMin,
     2     CoefL,CoefB,CoefN,AtmLam,AtmFos,AtmGam,AtmMas,
     3     BtuMax,CoefBt,grID,nrows,NTOTPA,ElmtNm,CPar,Class

        integer COLTBL
        PARAMETER (COLTBL=32)


C!!!!!!!
C       Variabili globali Parametri atomici
C!!!!!!!

        INTEGER NATPAR
        PARAMETER(NATPAR=1000)                ! Elementi atomici

        character*14 AT_NAM(NATPAR)
        DOUBLE PRECISION AT_LAM(NATPAR),AT_FOS(NATPAR)
        DOUBLE PRECISION AT_GAM(NATPAR), AT_MAS(NATPAR)
        INTEGER AT_N

        COMMON/ATOMPA/AT_LAM,AT_FOS,AT_GAM,AT_MAS,
     1  AT_NAM,AT_N

C!!!!!!!
C        Variabili globali Generali
C!!!!!!!

        INTEGER NMXSPE,NMXWRK,NPTFFT

        PARAMETER(NMXSPE=400000,NMXWRK=40000)   !Dim. max spectrum
CKB: was  (NMXSPE=100000,NMXWRK=5000) before
        PARAMETER (NPTFFT=256)
C!!!!!!!
C        Graphics
C!!!!!!!
        LOGICAL I_GRAP,I_RESD,I_VAR,I_Z
        INTEGER NREGIO,GCOLOR(8),NGRALI
        DOUBLE PRECISION GR_YMN,GR_YMX,GR_DEL,GR_BOT,GR_OFF
        DOUBLE PRECISION GR_TOP,GR_LAB,GR_TIC,REDSH,VELRAN
        DOUBLE PRECISION REGMIN(NMXLIN),REGMAX(NMXLIN),WINSTE(NMXLIN)
        REAL GRAWAV(NMXSPE),GRASPE(NMXSPE),GRASIG(NMXSPE)
        REAL CONTIN(NMXSPE),SKYLEV(NMXSPE)       
        REAL YLIN(NMXLIN)
        CHARACTER*60 LLABEL(NMXLIN)
        COMMON/GRAPHIC/LLABEL,WINSTE,REGMIN,REGMAX,GR_LAB,GR_TIC,
     1  GR_YMN,GR_YMX,GR_DEL,GR_BOT,GR_OFF,GR_TOP,GCOLOR,
     2  GRAWAV,GRASPE,GRASIG,CONTIN,SKYLEV,YLIN,REDSH,VELRAN,
     3  NREGIO,NGRALI,I_GRAP,I_RESD,I_VAR,I_Z
 
C!!!!!!!
C        Limiti e step del fit
C!!!!!!!
                                        ! limiti max nel fit
                                        ! 1,2 min and max, 3 step 
        DOUBLE PRECISION LamLim(3),NLim(3),BLim(3),BtuLim(3)
        COMMON/LIMITINI/LamLim,NLim,BLim,BtuLim


C!!!!!!!
C        Spettro
C!!!!!!!
        DOUBLE PRECISION XLAMBD(NMXSPE),SPECTR(NMXSPE)
        DOUBLE PRECISION SIGNOI(NMXSPE),FWHM(NMXSPE)
        DOUBLE PRECISION PIXSIZ(NMXSPE)
        INTEGER NPUNTI

        COMMON/GEN/XLAMBD,SPECTR,SIGNOI,FWHM,PIXSIZ,NPUNTI
c        COMMON/GEN/XLAMBD,SPECTR,SIGNOI,FWHM,NPUNTI


C!!!!!!!
C        Variabili globali Preferenze
C!!!!!!!

        CHARACTER*60 FILSPE,FILOUT,FILLOG

        COMMON/PREF/FILSPE,FILOUT,FILLOG

        DOUBLE PRECISION FITMIN(NMXLIN),FITMAX(NMXLIN)
        INTEGER NINTFT
        CHARACTER*50 MINCOM(NMXLIN)
        INTEGER NMINCM
        COMMON/INTERV/FITMIN,FITMAX,MINCOM,NMINCM,NINTFT


C!!!!!!!
C        Variabili globali of results
C!!!!!!!
        REAL LamCen(NMXLIN),NHfit(NMXLIN),BDopp(NMXLIN),BTur(NMXLIN)
        REAL  w(NMXLIN),ErLaPa(NMXLIN),ErNnPa(NMXLIN),ErBbPa(NMXLIN)
        REAL ErBtPa(NMXLIN),ErLaPo(NMXLIN),ErLaNe(NMXLIN)
        REAL ErNnPo(NMXLIN),ErNnNe(NMXLIN),ErBbPo(NMXLIN)
        REAL ErBbNe(NMXLIN),ErBtPo(NMXLIN),ErBtNe(NMXLIN)
        REAL Temper(NMXLIN),Redshi(NMXLIN)
        REAL chi,prob
        INTEGER IDnum

        COMMON/RESLTS/ LamCen,NHfit,BDopp,BTur,w,ErLaPa ,ErNnPa ,
     1      ErBbPa ,ErBtPa ,ErLaPo ,ErLaNe ,ErNnPo ,ErNnNe ,
     2      ErBbPo ,ErBbNe ,ErBtPo ,ErBtNe,
     3      Temper,Redshi,chi,prob,IDnum


C!!!!!!!
C	 Spettro fittato
C!!!!!!!
C	REAL FITWAV(NMXSPE),
