! @(#)plotredsh.prg	10.2 (ESO-IPG) 2/16/96 15:00:33

!write/out "PLOT/REDSHIFT [spec] [z] [vel. width] [label1,..] [session] "

! 1 version, 22-nov-1995
!
define/parameter P1 NULL T/C 
define/parameter P2 -1. N/A
define/parameter P3 -1 N/A
define/parameter P4 NULL I/C
define/parameter P5 NULL T/C

write/key SPECTAB {P1}
write/key SESSNAM {P5}

define/local grcen/d/1/1 -1.
write/key grcen {p2}
define/local grran/d/1/1 -1.
write/key grran {P3}

define/local i/i/1/1 1
define/local n/i/1/1 1

if {grcen} .ge. 0 write/desc LYPAR.tbl REDSHIFT/d/1/1 {grcen}
if {grran} .ge. 0 write/desc LYPAR.tbl VELRANGE/d/1/1 {grran}
if M$EXISTD("LYPAR.tbl","GRAPH_MIN") .eq. 0 then
  write/desc LYPAR.tbl GRAPH_MIN/d/1/1 0.
endif
if M$EXISTD("LYPAR.tbl","GRAPH_MAX") .eq. 0 then
  write/desc LYPAR.tbl GRAPH_MAX/d/1/1 00.
endif
if M$EXISTD("LYPAR.tbl","GRAPH_STEP") .eq. 0 then
  write/desc LYPAR.tbl GRAPH_STEP/d/1/1 0
endif

if "{p4}" .ne. "NULL" then
    define/local ilen/i/1/1 0
    define/local idum/i/1/1 1
    define/local idum2/i/1/1 0
    ilen = M$LEN("{P4}")
    define/local labels/c/1/{ilen} {P4}

CALC:
    idum2 = M$INDEX("{labels({idum}:{ilen})}",",") 
    if {idum2} .eq. 0  then  !ultima, o solo una..
      set/format i2
      write/desc LYPAR.tbl  GRALAB{i}/c/1/60  " " A
      write/desc LYPAR.tbl GRALAB{i}/c/1/60 {labels({idum}:{ilen})}
      set/format i4      
    else
      idum2 = {idum2} - 1 + {idum} - 1
      set/format i2
      write/desc LYPAR.tbl  GRALAB{i}/c/1/60  " " A
      write/desc LYPAR.tbl GRALAB{i}/c/1/60 {labels({idum}:{idum2})}
      set/format i4
      idum = {idum2} + 2
      i = {i} + 1.
      if {i} .lt. 5 goto CALC
    endif


  write/desc LYPAR.tbl GREGION/I/1/1 {i}
  n = {i} + 1
  set/format i2
CLEARD:
  if M$EXISTD("LYPAR.tbl","GRALAB{n}") .eq. 1 then
	dele/desc LYPAR.tbl GRALAB{n}
 	n = {n} + 1
	goto CLEARD
  endif
  set/format i4

endif

write/desc LYPAR.tbl ZPLOT/c/1/5 "TRUE "

run CON_EXE:plotlyman.exe
