! @(#)plotly.prg	10.4 (ESO-IPG) 2/16/96 15:00:30

!write/out "PLOT/LYMAN [spec] [lamcen1,..] [size1,..] [label1..] [session] "

! New version, 22-nov-1995
!
define/parameter P1 NULL T/C 
define/parameter P2 -1. N/A
define/parameter P3 10. N/A
define/parameter P4 NULL I/C
define/parameter P5 NULL T/C

write/key SPECTAB {P1}
write/key SESSNAM {P5}

if {P2} .gt. -1 then 
  define/local grcen/d/1/10 -1. A
  write/key grcen {p2}
  define/local grran/d/1/10 -1. A
  write/key grran {P3}
  define/local grmin/d/1/1 0.
  define/local grmax/d/1/1 0.
  define/local grstep/d/1/1 {P3}
  define/local i/i/1/1 1
  define/local n/i/1/1 1

  define/local ilen/i/1/1 0
  define/local idum/i/1/1 1
  define/local idum2/i/1/1 0
  ilen = M$LEN("{P4}")
  define/local labels/c/1/{ilen} {P4}

  if M$EXISTD("LYPAR.tbl","GRAPH_MIN") .eq. 1 then
    dele/desc LYPAR.tbl GRAPH_MIN
  endif
  if M$EXISTD("LYPAR.tbl","GRAPH_MAX") .eq. 1 then
    dele/desc LYPAR.tbl GRAPH_MAX
  endif
  if M$EXISTD("LYPAR.tbl","GRAPH_STEP") .eq. 1 then
    dele/desc LYPAR.tbl GRAPH_STEP
  endif

CALC:
  if {grran({i})} .lt. 0 write/key grran/d/{i}/1 {grstep}
  grmin  =  {grcen({i})} - {grran({i})}/2.
  grmax  =  {grcen({i})} + {grran({i})}/2.
  write/desc LYPAR.tbl GRAPH_MIN/d/{i}/1 {grmin}
  write/desc LYPAR.tbl GRAPH_MAX/d/{i}/1 {grmax}
  write/desc LYPAR.tbl GRAPH_STEP/d/{i}/1 {grran({i})}

  if "{p4}" .ne. "NULL" then
    idum2 = M$INDEX("{labels({idum}:{ilen})}",",")
    if {idum2} .eq. 0  then  !ultima, o solo una..
      set/format i2
      write/desc LYPAR.tbl  GRALAB{i}/c/1/60  " " A
      write/desc LYPAR.tbl GRALAB{i}/c/1/60 {labels({idum}:{ilen})}
      set/format i4      
    endif
    if {idum2} .gt. {idum} .and. {idum2} .lt. {ilen} then
      idum2 = {idum2} - 1
      set/format i2
      write/desc LYPAR.tbl  GRALAB{i}/c/1/60  " " A
      write/desc LYPAR.tbl GRALAB{i}/c/1/60 {labels({idum}:{idum2})}
      set/format i4
      idum = {idum2} + 2
    endif
  else
    set/format i2
    write/desc LYPAR.tbl  GRALAB{i}/c/1/60  " " A
    set/format i4
  endif

  i = {i} + 1.
  if {grcen({i})} .gt. 0 goto CALC

  n = {i} - 1
  write/desc LYPAR.tbl GREGION/i/1/1 {n}
  set/format i2
CLEARD:
  if M$EXISTD("LYPAR.tbl","GRALAB{i}") .eq. 1 then
	dele/desc LYPAR.tbl GRALAB{i}
 	i = {i} + 1
 	goto CLEARD
  endif
  set/format i4

endif

write/desc LYPAR.tbl ZPLOT/c/1/5 "FALSE"

run CON_EXE:plotlyman.exe
