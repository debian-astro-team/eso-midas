! @(#)comply.prg	19.1 (ES0-DMD) 02/25/03 13:27:54
! @(#)comply.prg 7.1 (ESO-IPG) 2/24/94 20:30:26

!write/out "COMP/LYMAN linetab outtab [refspe] [opt(L,M,A)] [noi1,noi2,noi3] 

define/parameter P1 ? T/A "Input line table"
define/parameter P2 ? T/C " Output spectrum"
define/parameter P3 NULL T/C 
define/parameter P4 A T/C
define/parameter P5 0 N

write/key OUTTAB {P1}
write/key SINTTAB {P2}
write/key SPECTAB {P3}
write/key INPUTC {P4}
write/key INPUTR 0,0,-1
write/key INPUTR {P5}

if SPECTAB .eq. "NULL" then
	write/key SPECTAB  {LYPAR.tbl,SPECTRUMTABLE}
end if

run CON_EXE:complyman.exe


