! @(#)convlyman.prg	19.1 (ES0-DMD) 02/25/03 13:27:54
! @(#)convlyman.prg	7.1 (ESO-IPG) 2/24/94 20:30:26

define/parameter P1 ?  IMA  "Input Image:"
define/parameter P2 ?  TAB  "Output Table:"
define/parameter P3 ?  ?    "Variance Image or Value:"
define/parameter P4 ?  NUM  "Resolution:"

copy/it {P1} {P2}  :WAVE  

name/column  {P2}  #2  :NORMFLUX

IF M$TSTNO(P3) .EQ. 1 THEN
   compute/tab  {P2}  :STDEV = {P3}
ELSE
   copy/it {P3} &u 
   copy/tt &u  #1 {P2} :STDEV
ENDIF

COMPUTE/TABLE {P2} :FWHM = :WAVE/{P4}



