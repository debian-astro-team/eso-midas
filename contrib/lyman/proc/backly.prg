! 
!write/out "BACK/LYMAN [spec] [inphis] [outtab] [outhis] [gra Y/N]"
! 
define/parameter P1 {SPECTAB} T/A "Input spectrum table"
define/parameter P2 {HISTAB} T/A "Input HISTORY table"
define/parameter P3 {OUTTAB} T/C " Output table"
define/parameter P4 {HISTAB} T/C " Output history table"
define/parameter P5 A I

write/key SPECTAB " " 80
write/key INPUTC " " 80
write/key OUTTAB " " 80
write/key HISTAB " " 80
write/key SPECTAB {P1}
write/key INPUTC {P2}
write/key OUTTAB {P3}
write/key HISTAB {P4}
write/key GRAWIN -1
if P5 .eq. "Y" .or. P5 .eq. "y" then
	write/key GRAWIN 1
endif
if P5 .eq. "N" .or. P5 .eq. "n" then
	write/key GRAWIN 0
endif

run CON_EXE:backly.exe


