! @(#)fitdir.prg	19.1 (ES0-DMD) 02/25/03 13:27:54
! @(#)fitdir.prg 7.1 (ESO-IPG) 2/24/94 20:30:26

!write/out "FIT/DIRECT [spec] [outtab] [history] [session] [grap(Y/N)]"

define/parameter P1 NULL T/C 
define/parameter P2 NULL T/C
define/parameter P3 NULL T/C
define/parameter P4 NULL T/C
define/parameter P5 A I

write/key SPECTAB {P1}
write/key OUTTAB {P2}
write/key HISTAB {P3}
write/key SESSNAM {P4}
write/key GRAWIN -1
if P5 .eq. "Y" .or. P5 .eq. "y" then
	write/key GRAWIN 1
endif
if P5 .eq. "N" .or. P5 .eq. "n" then
	write/key GRAWIN 0
endif

run CON_EXE:fitdirect.exe

