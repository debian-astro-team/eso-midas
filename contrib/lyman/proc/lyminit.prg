! @(#)lyminit.prg	10.3 (ES0-DMD) 2/16/96 15:00:27
define/parameter P1 NULL C "Session name"

write/key SESSNAM/c/1/80 " " ALL  ! Name of the current session
write/key SPECTAB/c/1/80 " " ALL  ! Name of the input spectrum table
write/key OUTTAB/c/1/80 " " ALL   ! Name of the output spectrum table
write/key HISTAB/c/1/80 " " ALL   ! Name of 
write/key SINTTAB/c/1/80 " " ALL
write/key GRAWIN/I/1/1 1

write/key SESSNAM/c/1/80  {P1}

RUN CON_EXE:lyminit.exe

