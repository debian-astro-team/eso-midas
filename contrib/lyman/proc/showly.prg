define/parameter P1 NULL C "Session"

define/local Sestab/c/1/80 " "
if "{P1}" .ne. "NULL" then
  SesTab = "{P1}LYPAR"
else 
  SesTab =  "LYPAR"
endif
write/key sessdisp/c/1/1 "Y"
write/key sessnlin/i/1/1 0
write/key sessline/i/1/1 0

show/sess " ***********   FITLYMAN Parameters   ***********"
show/sess "    "
show/sess "   INPUT FILES  :  	{{SesTab}.tbl,SPECTRUMTABLE}"
show/sess "   OUTPUT TABLE :	{{SesTab}.tbl,OUTTAB}"
show/sess "   LOG TABLES   :    {{SesTab}.tbl,LOGNAME}"
show/sess "    "
show/sess "----- Fitting Options -----"
if {{SesTab}.tbl,TURBOLENCE} .eq. 0 then
  show/sess "  Turbolence broadening : NOT ACTIVE"
else
  show/sess "  Turbolence broadening : ACTIVE"
endif
if {{SesTab}.tbl,I_MINOS} .eq. 0 then
  show/sess "  Asimmetric Errors     : NOT COMPUTED"
else
  show/sess "  Asimmetric Errors     : COMPUTED"
endif
show/sess "    "
show/sess "----- Allowed range for parameters -----"
define/local c1/c/1/15 " "
define/local c2/c/1/15 " "
define/local c3/c/1/15 " "
show/sess "                        Lower Limit    Upper Limit   Scan Step"
if {{SesTab}.tbl,LAMBDA_LIM(1)} .eq. 0 then
  c1 = "        FREE"
else
  c1 = "{{SesTab}.tbl,LAMBDA_LIM(1)}"
endif 
if {{SesTab}.tbl,LAMBDA_LIM(2)} .eq. 0 then
  c2 = "        FREE"
else
  c2 = "{{SesTab}.tbl,LAMBDA_LIM(2)}"
endif
  c3 = "{{SesTab}.tbl,LAMBDA_LIM(3)}"
show/sess " Wavelength       :   {c1}   {c2}   {c3}"

if {{SesTab}.tbl,COLDEN_LIM(1)} .eq. 0 then
  c1 = "        FREE"
else
  c1 = "{{SesTab}.tbl,COLDEN_LIM(1)}"
endif 
if {{SesTab}.tbl,COLDEN_LIM(2)} .eq. 0 then
  c2 = "        FREE"
else
  c2 = "{{SesTab}.tbl,COLDEN_LIM(2)}"
endif
  c3 = "{{SesTab}.tbl,COLDEN_LIM(3)}"
show/sess " Column density   :   {c1}   {c2}   {c3}"

if {{SesTab}.tbl,BTEMP_LIM(1)} .eq. 0 then
  c1 = "        FREE"
else
  c1 = "{{SesTab}.tbl,BTEMP_LIM(1)}"
endif 
if {{SesTab}.tbl,BTEMP_LIM(2)} .eq. 0 then
  c2 = "        FREE"
else
  c2 = "{{SesTab}.tbl,BTEMP_LIM(2)}"
endif
  c3 = "{{SesTab}.tbl,BTEMP_LIM(3)}"
show/sess " Thermal Doppler b:   {c1}   {c2}   {c3}"

if {{SesTab}.tbl,BTURB_LIM(1)} .eq. 0 then
  c1 = "        FREE"
else
  c1 = "{{SesTab}.tbl,BTURB_LIM(1)}"
endif 
if {{SesTab}.tbl,BTURB_LIM(2)} .eq. 0 then
  c2 = "        FREE"
else
  c2 = "{{SesTab}.tbl,BTURB_LIM(2)}"
endif
  c3 = "{{SesTab}.tbl,BTEMP_LIM(3)}"
show/sess " Turbol. Doppler b:   {c1}   {c2}   {c3}"

show/sess "    "
show/sess "----- Graphics -----"
if {{SesTab}.tbl,GRAPHWIN} .eq. 0 then
  show/sess "  GRAPHICS NOT ACTIVE"
  goto GRSETUP
endif
set/format F11.5
if "{{SesTab}.tbl,ZPLOT}" .eq. "FALSE" then
  show/sess "  Plots are displayed in the LAMBDA space"
else
  show/sess "  Plots are displayed in the VELOCITY space"
  show/sess "  Central Redshift   : {{SesTab}.tbl,REDSHIFT}"
  show/sess "  Velocity Range     : {{SesTab}.tbl,VELRANGE}"
endif
define/local i/i/1/1 0
define/local c4/c/1/20 " "
if "{{SesTab}.tbl,ZPLOT}" .eq. "FALSE" then
  show/sess "  Windows:    From          To          Label"
else 
  show/sess "  Windows:    From          To          Element"
endif
set/format i2
set/format F8.2
do i = 1 {{SesTab}.tbl,GREGION}
  set/format i1
  c1 = "{i}"
  set/format i2
  c2 = "{{SesTab}.tbl,GRAPH_MIN({i})}"
  c3 = "{{SesTab}.tbl,GRAPH_MAX({i})}"
  c4 = "{{SesTab}.tbl,GRALAB{i}}"
  show/sess "     #{c1}     {c2}      {c3}       {c4} "
enddo
set/format i4
set/format E15.5

GRSETUP:
show/sess "    "
show/sess "----- Graphics Set-Up -----"
if {{SesTab}.tbl,I_RESIDUALS} .eq. 0 then
  show/sess "  Residuals    : NOT PLOTTED"
else 
  show/sess "  Residuals    : PLOTTED"
endif
if {{SesTab}.tbl,I_STDEV} .eq. 0 then
  show/sess "  Std. deviat. : NOT PLOTTED"
else 
  show/sess "  Std. deviat. : PLOTTED"
endif
set/format F5.3
show/sess "  Lower empty space  : {{SesTab}.tbl,GRAPH_BOTTOM}"
show/sess "  Upper empty space  : {{SesTab}.tbl,GRAPH_TOP}"
show/sess "  Label position     : {{SesTab}.tbl,GRAPH_LABEL} (respect to continuum level)"
show/sess "  Line tick position : {{SesTab}.tbl,GRAPH_TICK} (respect to zero level)"
set/format i1
show/sess "  Colors:"
show/sess "       - Spectrum           :  {{SesTab}.tbl,GCOLOR(1)}"
show/sess "       - Continuum and sky  :  {{SesTab}.tbl,GCOLOR(2)}"
show/sess "       - Fitted profile     :  {{SesTab}.tbl,GCOLOR(3)}"
show/sess "       - Residuals          :  {{SesTab}.tbl,GCOLOR(4)}"
show/sess "       - Standard deviation :  {{SesTab}.tbl,GCOLOR(5)}"
show/sess "       - Labels             :  {{SesTab}.tbl,GCOLOR(6)}"
show/sess "       - Line ticks         :  {{SesTab}.tbl,GCOLOR(7)}"
show/sess "    "
show/sess "    "

set/format E15.5
set/format i4

return

