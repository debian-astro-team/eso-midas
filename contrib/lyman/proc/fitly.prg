! 
!execute as
!   FIT/LYMAN spec outtab [history] [session] [grap(Y/N)]
! 
define/parameter P1 NULL T
define/parameter P2 NULL T
define/parameter P3 NULL T
define/parameter P4 NULL T
define/parameter P5 Y ?			!en/disable automatic graphics output

write/key SPECTAB " " 80
write/key OUTTAB " " 80
write/key HISTAB " " 80
write/key SESSNAM " " 80
write/key SPECTAB {P1}
write/key OUTTAB {P2}
write/key HISTAB {P3}
write/key SESSNAM {P4}
write/key GRAWIN -1
if P5 .eq. "Y" .or. P5 .eq. "y" then
   GRAWIN = 1
else if P5 .eq. "N" .or. P5 .eq. "n" then
   GRAWIN = 0
endif

run CON_EXE:fitlyman.exe
 
