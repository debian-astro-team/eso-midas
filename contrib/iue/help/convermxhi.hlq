%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.IDENT      convmxhi.hlq
%.AUTHOR     JDP, ESA/Vilspa.
%.KEYWORDS   MIDAS, help files, Ontext, IUEFA Context,CONVERT/MXHI
%.PURPOSE    On-line help file for the command: CONVERT/MXHI
%.VERSION    1.0  3-JUN-1997 : Creation, JDP
%----------------------------------------------------------------
\se
SECTION./MXHI
\es\co
CONVERT/MXHI                                                 3-JUN-1997 JDP
\oc\su
CONVERT/MXHI mxhi_file
	Convert an IUEFA MXHI FITS file into a Midas Table.

\us\pu
Purpose: 
        Converts IUEFA NewSIPS MXHI FITS file into a Midas Table.
\up\sy
Syntax:  
        CONVERT/MXHI mxhi_file 
\ys\pa
        mxhi_file = Name of the MXHI FITS file (Assumes MXHI as extension).
\\
        The output table has the same file name than the input table
        with H as suffix, to indicate the dispersion.
\\
        For example, if the MXHI file is SWP21805, ouput table is SWP21805H.
\ap\sa
See also:
        CONVERT/MXLO, CONCATENATE/IUE, IUEFA Context.
\as\no
Note:  
        IUEFA MXHI FITS files are produced by the NewSIPS pipeline.
\\
        The converted table has a format simiilar to:
\\
   Table : SWP21805H                       [Transposed format]
\\
 No.Columns :       8   No.Rows :   37757 
\\ 
 All.Columns:      11   All.Rows:   37760         Sel.Rows:   37757
\\
 Sorted  by :Sequence  Reference:Sequence     
\\
 Col.#   1:ORDER            Unit:UNITLESS         Format:I4     I*4
\\
 Col.#   2:WAVELENGTH       Unit:ANGSTROM         Format:F10.3  R*4
\\
 Col.#   3:NET              Unit:FN               Format:E15.5  R*4
\\
 Col.#   4:BACKGROUND       Unit:FN               Format:E15.5  R*4
\\
 Col.#   5:NOISE            Unit:FN               Format:E15.5  R*4
\\
 Col.#   6:QUALITY          Unit:UNITLESS         Format:I11    I*4
\\
 Col.#   7:RIPPLE           Unit:FN               Format:E15.5  R*4
\\
 Col.#   8:FLUX             Unit:ERG/CM2/S/A      Format:E15.5  R*4
\\
           
\on\exs
Examples:
\ex
           CONVERT/MXHI SWP21805
\xe\sxe

