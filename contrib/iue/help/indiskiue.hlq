%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.IDENT      indiskiue.hlq
%.AUTHOR     JDP - ESA, VILSPA
%.KEYWORDS   MIDAS, help files
%.PURPOSE    On-line help file for the command: INDISK/IUE
%.VERSION    1.0  28-JUN-1994 : Update: JDP
%----------------------------------------------------------------
\se
SECTION./IUE  
\es\co
INDISK/IUE                                                       28-JUN-1994 JDP
\oc\su
INDISK/IUE infile [outfile] [flags]
	read IUE data in GO format
\us\pu
Purpose: 
          Read IUE data in GO format from disk.:-)
\up\sub
Subject:
          IUE, Spectroscopy
\bus\sy
Syntax: 
          INDISK/IUE infile [outfile] [flags]
\ys\pa
          infile = input filename with IUE data in GO format.
\ap\pa
          outfile =  output filename or the keyword IUE.
\\
               Default value is IUE to indicate the standard name
\\
               convention by camera and image number (See Note below). 
\ap\pa
          flags = Two letter code;
\\
               The first letter defines the internal format on disk as follows:
\\
               I - integer data for RAW (I*1) and PHOT (I*2) 
\\
               R - floating point for RAW and PHOT (R*4) 
\\
               X - data is not written on disk, this option is used to display
               the headers.
\\
               The second letter controls the header display as follows: 
\\
               N - no header display 
\\
               S - Short header display (first 5 lines) 
\\
               F - Full display of the header (except for the binary part) ;
\\
               defaulted to IS
\ap\see
See also:
          INDISK/FITS, INTAPE/IUE
\ees\no
Note:
          Output files are images for RAW, PHOT, (E)LBL, and FES data types, 
          and tables for MELO and MEHI extracted spectra. 
          Output filenames are assigned by default as follows:
\\
           File name
\\
           fesnnnnn.bdf    FES image
\\
           cccnnnnn.raw    Raw data
\\
           cccnnnnn.pho    Photom. corrected image
\\
           cccnnnnnL.lbl   Line-by-line spectrum, large aperture
\\
           cccnnnnnS.lbl   Line-by-line spectrum, small aperture
\\
           cccnnnnnL.tbl   Extracted spectra, large aperture
\\
           cccnnnnnS.tbl   Extracted spectra, small aperture
\\
          Spectra are stored in tables with the following columns:
\\
          :WAVELENGTH  Wavelength in Angstrom (F8.3) 
\\
          :EPSILON     Quality flag (I5) 
\\
          :GROSS       Gross spectrum (E12.4)
\\
          :BACKGROUND  Interorder background (E12.4)
\\
          :NET         Net (i.e.,Gross - smoothed background) (E12.4)
\\
          :RNET        Ripple corrected net (high disp only) (E12.4)
\\
          :FLUX        Absolute flux in Erg/cm2/A (E12.4) 
\\
          :ORDER       Order number (high disp only) (I4)
\on\exs
Examples:
\ex
          INDISK/IUE  myfile
          Read the file 'myrawimage' output file is named by camera, image
          number and file type (swp12345.raw) according with the default naming
          convention. 
\xe\sxe
