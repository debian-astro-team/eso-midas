$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.IUE.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:54 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   istfhd.for
$ FORTRAN   isthed.for
$ FORTRAN   istlow.for
$ FORTRAN   istupc.for
$ FORTRAN   istdes.for
$ FORTRAN   istfn1.for
$ FORTRAN   isthig.for
$ FORTRAN   istpho.for
$ FORTRAN   istfes.for
$ FORTRAN   istfn2.for
$ FORTRAN   istlbl.for
$ FORTRAN   istraw.for
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
