        SUBROUTINE ISTUPC(SA,SB)
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C.VERSION: 1.0  
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C.IDENTIFICATION        ISTUPC.FOR
C.KEYWORDS              FORTRAN FUNCTION
C.ENVIRONMENT  VMS
C.PURPOSE
C       Converts characters in the input string into uppercase.
C
C------------------------------------------------------------------
        IMPLICIT  NONE
        CHARACTER*(*)   SA      ! IN  : Input string 
        CHARACTER*(*)   SB      ! OUT : Destination string
C
        INTEGER   N,KLEN,LEN,M
        CHARACTER*26    ALPHU,ALPHL
C 
        DATA   ALPHU  /'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/
        DATA   ALPHL  /'abcdefghijklmnopqrstuvwxyz'/
C 
        KLEN = LEN(SA)
C 
C  compare each input character with lowercase alphabet
        DO 100 N=1,KLEN
C 
           SB(N:N) = SA(N:N)
           DO 50 M=1,26
              IF (SA(N:N).EQ.ALPHL(M:M)) SB(N:N) = ALPHU(M:M)
50         CONTINUE
C 
100     CONTINUE
C 
        RETURN
        END

