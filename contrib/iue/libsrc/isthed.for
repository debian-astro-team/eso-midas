C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION        ISTHED.FOR
C.AUTHOR:               J.D.PONZ  ESA-VILSPA        
C.KEYWORDS              IUE, GO FORMAT, FILE HEADER
C.ENVIRONMENT           VMS
C.PURPOSE
C \begin{TeX}
C  Reads the first record of the IUE GO file header
C  to find file characteristics.
C  The record is converted into ASCII and stored in a buffer for later
C  usage.
C
C  The routine performs the following functions (routine names in brackets)
C \begin{enumerate}
C  \item Reads first record (ISTREA)
C  \item Translates EBCDIC into ASCII 
C  \item Decodes Camera number, Dispersion, Number of bytes per record in the
C     data matrix and Number of records in the data matrix
C  \item Computes the file type as:
C       \begin{itemize}
C        \item 0 = FES, 
C        \item 1 = RAW, 
C        \item 2 = PHOTOM, 
C        \item 3 = (E)LBL, 
C        \item 4 = MELO, 
C        \item 5 = MEHI
C       \end{itemize}
C  \end{enumerate}
C  \end{TeX}
C.LANGUAGE              F77 
C.COMMENTS
C \begin{TeX}
C The following extensions are used:
C   \begin{itemize}
C    \item IMPLICIT NONE
C    \item INCLUDE statement
C    \item long variable names
C    \item underscore character
C   \end{itemize}
C \end{TeX}
C.VERSION: 1.0  INITIAL CODING  09 JUL 1990
C.VERSION: 1.1  INITIAL CODING  24 SEP 1990 Handle different FES sizes.
C                                           Add more STATUS return codes.
C.VERSION: 1.2  REMOVE VMS EXTENSIONS. 14 APR 1992
C------------------------------------------------------------------
      SUBROUTINE ISTHED(CHANL,BUFF,CAMERA,IMANUM,APERTU,DISPER,
     .                  FTYPE,NRECO,NBYTE,STATUS)
C
      IMPLICIT NONE
      INTEGER       CHANL             ! IN: tape channel number
      CHARACTER*(*) BUFF              ! OUT: buffer with the ascii header
      INTEGER       CAMERA            ! OUT: camera code number
      INTEGER       IMANUM            ! OUT: image number
      INTEGER       APERTU            ! OUT: LAP-SAP code
      INTEGER       DISPER            ! OUT: dispersion code
      INTEGER       FTYPE             ! OUT: file type
      INTEGER       NRECO             ! OUT: number of lines
      INTEGER       NBYTE             ! OUT: number of bytes per line
      INTEGER       STATUS            ! OUT: status
C
      INTEGER ISTAT, LEN, LABLEN
      CHARACTER*78   LINE
      CHARACTER*360 CBUFF
C
      STATUS = 0
      LABLEN = 360
      APERTU = 0
      CALL ISTREC(CHANL,CBUFF,LABLEN,LEN,STATUS)
      IF (STATUS.NE.0) RETURN
      IF (LEN.NE.LABLEN) THEN
          CALL STTPUT('*** Not an IUE GO format ***',ISTAT)
          STATUS = 1
          RETURN
      ENDIF
      BUFF = CBUFF
      READ(BUFF(33:36),1000,ERR=999) NRECO       ! decode number of lines
      READ(BUFF(37:40),1000,ERR=999) NBYTE       ! decode number of bytes/line
      READ(BUFF(50:50),1001,ERR=999) CAMERA      ! decode camera number
      READ(BUFF(51:51),1001,ERR=999) DISPER      ! decode dispersion
      READ(BUFF(52:56),1002,ERR=999) IMANUM      ! decode image number
      IF ((NRECO.EQ.113 .AND. NBYTE.EQ.113) .OR.      ! ERES (VILSPA std)     
     .    (NRECO.EQ. 81 .AND. NBYTE.EQ. 81) .OR.      ! DRES (Default)
     .    (NRECO.EQ.127 .AND. NBYTE.EQ.127) .OR.      ! FRES (Full field)
     .    (NRECO.EQ.  7 .AND. NBYTE.EQ.  7))  THEN    ! PRES (Postage)
           FTYPE = 0                               ! FES 
      ELSEIF (NRECO.EQ.768 .AND. NBYTE.EQ.768) THEN
           FTYPE = 1                               ! RAW 
      ELSEIF (NRECO.EQ.768 .AND. NBYTE.EQ.1536) THEN
           FTYPE = 2                               ! PHOTOM
      ELSEIF ( (NRECO.EQ.166 .OR. NRECO.EQ.331) .AND. 
     .        (NBYTE.EQ.2048 .OR. NBYTE.EQ.1204) ) THEN
           FTYPE = 3                               ! (E)LBL 
      ELSEIF ( NRECO.EQ.7 ) THEN
           FTYPE = 4                               ! LOW 
      ELSEIF ( NRECO.EQ.325 .OR. NRECO.EQ.361 .OR.
     .         NRECO.EQ.379 .OR. NRECO.EQ.421 ) THEN
           FTYPE = 5                               ! HIGH
      ELSE
           WRITE(LINE,2000) NRECO,NBYTE,CAMERA,IMANUM
           CALL STTPUT(LINE,ISTAT)
           CALL STTPUT('*** Error in file type ***',ISTAT)
           STATUS = 2
C
C ... print header to give a hint !!!!!!!!!!!!!!!
C
      ENDIF 
      RETURN
 999  CONTINUE
      CALL STTPUT('*** Error decoding the header info ***',ISTAT)
      CALL STTPUT('*** File is not in GO format ***',ISTAT)
      STATUS = 1
      RETURN
 1000 FORMAT(I4)
 1001 FORMAT(I1)
 1002 FORMAT(I5)
 2000 FORMAT(' NREC:',I5,' NBYTE:',I5,' CAMERA:',I1,'IMAGE:',I6)
      END

