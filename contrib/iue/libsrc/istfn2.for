C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION        ISTFN2.FOR
C.AUTHOR:               J.D.PONZ  ESA-VILSPA        
C.KEYWORDS              IUE, GO FORMAT, FILE HEADER
C.ENVIRONMENT           VMS
C.PURPOSE
C \begin{TeX}
C    Defines the file name as <{\tt root}><{\tt number}>
C    where 
C   \begin{description}
C    \item[{\tt root}] is a character string and
C    \item[{\tt number}] is a four digit number, position of the file on tape 
C   \end{description}
C \end{TeX}
C.LANGUAGE              F77
C.VERSION: 1.0  INITIAL CODING  09 JUL 1990
C------------------------------------------------------------------
      SUBROUTINE ISTFN2(FID,NUMBER,FILE)
C
      IMPLICIT NONE
      CHARACTER*(*) FID               ! IN: root filename
      INTEGER       NUMBER            ! IN: sequential number
      CHARACTER*(*) FILE              ! OUT: filename
C
      INTEGER       I, INDEX
C
      FILE = FID
      I    = INDEX(FILE,' ') 
      WRITE(FILE(I:I+4),1000) NUMBER
      RETURN
 1000 FORMAT(I4.4)
      END
