C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION        ISTDES.FOR
C.AUTHOR:               J.D.PONZ  ESA-VILSPA        
C.KEYWORDS              IUE, GO FORMAT, FILE HEADER
C.ENVIRONMENT           VMS
C.PURPOSE
C  Writes the first five lines of the header into a descriptor
C  -- not yet implemented
C         
C.LANGUAGE              F77
C.COMMENTS
C  \begin{TeX}
C    The following extensions are used:
C   \begin{itemize}
C    \item IMPLICIT NONE
C    \item INCLUDE statement
C    \item long variable names
C    \item underscore character
C   \end{itemize}
C  \end{TeX}
C.VERSION: 1.0  INITIAL CODING  09 JUL 1990
C------------------------------------------------------------------
      SUBROUTINE ISTDES(NO,BUFF,STATUS)
C
      IMPLICIT NONE
      INTEGER       NO                ! IN: file id
      CHARACTER*(*) BUFF              ! IN: first header record
      INTEGER       STATUS            ! OUT: status return
C
      INTEGER   DUM(1), CAMERA, DISPER, IMANUM, ISTAT
      CHARACTER*3  TELESCOP, CAM(5)
      CHARACTER*8  DISP(3)
C
      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'
      DATA TELESCOP/'IUE'/
      DATA DISP(1)/'HIGH    '/
      DATA DISP(2)/'LOW     '/
      DATA DISP(3)/'N.A.    '/
      DATA CAM(1)/'LWP'/
      DATA CAM(2)/'LWR'/
      DATA CAM(3)/'SWP'/
      DATA CAM(4)/'SWR'/
      DATA CAM(5)/'FES'/
C
C      CALL STDWRC(NO,'IDENT',1,IDENT,1, 72,DUM,STATUS)
C      CALL STDWRI(NO,'NPIX',NPIX,1,2,DUM,STATUS)
C      CALL STDWRD(NO,'START',START,1,2,DUM,STATUS)
C      CALL STDWRR(NO,'LHCUTS',CUTS,1,4,DUM,STATUS)
      CALL STDWRC(NO,'TELESCOP',1,TELESCOP,1, 3,DUM,STATUS)
      READ(BUFF(50:50),1001,ERR=999) CAMERA      ! decode camera number
      READ(BUFF(51:51),1001,ERR=999) DISPER      ! decode dispersion
      READ(BUFF(52:56),1002,ERR=999) IMANUM      ! decode image number
      IF (CAMERA.EQ.9) THEN                  !  FES
         CAMERA = 5
         DISPER = 2
      ENDIF
      CALL STDWRC(NO,'CAMERA',1,CAM(CAMERA),1, 3,DUM,STATUS)
      CALL STDWRC(NO,'DISPERSN',1,DISP(DISPER+1),1, 8,DUM,STATUS)
      CALL STDWRI(NO,'IMAGE',IMANUM,1,1,DUM,STATUS)
      RETURN
 999  CONTINUE
      CALL STTPUT('*** Error decoding the header info ***',ISTAT)
      CALL STTPUT('*** File is not in GO format ***',ISTAT)
      STATUS = 1
      RETURN
 1001 FORMAT(I1)
 1002 FORMAT(I5)
      END
