C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION        ISTFN1.FOR
C.AUTHOR:               J.D.PONZ  ESA-VILSPA        
C.KEYWORDS              IUE, GO FORMAT
C.ENVIRONMENT           VMS
C.PURPOSE
C   \begin{TeX}
C    Generates file name as a function of the
C    Camera, image number, file type and aperture as follows:
C    \begin{itemize}
C \item {\tt filename} = <{\tt camera}><{\tt imano}> 
C                         for {\tt FES,RAW,PHOTOM} and {\tt MEHI}
C \item {\tt filename} = <{\tt camera}><{\tt imano}><{\tt aperture}> 
C                         for {\tt (E)LBL} and {\tt MELO}
C    \end{itemize}
C    where: 
C   \begin{description}
C    \item[{\tt camera}] can be {\tt LWP, LWR, SWP, SWR}
C    \item[{\tt imano}] is a 5 digit integer number
C    \item[{\tt aperture}]  can be  {\tt L, S}
C   \end{description}
C   \end{TeX}
C         
C.LANGUAGE              F77
C.VERSION: 1.0  INITIAL CODING  09 JUL 1990
C------------------------------------------------------------------
      SUBROUTINE ISTFN1(CAMERA,IMANUM,APERTU,FTYPE,FILE)
C
      IMPLICIT NONE
      INTEGER       CAMERA            ! IN: camera code number
      INTEGER       IMANUM            ! IN: image number
      INTEGER       APERTU            ! IN: LAP-SAP code
      INTEGER       FTYPE             ! IN: file type
      CHARACTER*(*) FILE              ! OUT: filename
C
      CHARACTER*4 CAM(4), EXT(6)
      CHARACTER*1 AP(2)
      DATA CAM(1)/'lwp'/, CAM(2)/'lwr'/
      DATA CAM(3)/'swp'/, CAM(4)/'swr'/
      DATA AP(1) /'l'/,   AP(2) /'s'/
      DATA EXT(1)/'.bdf'/
      DATA EXT(2)/'.raw'/
      DATA EXT(3)/'.pho'/
      DATA EXT(4)/'.lbl'/
      DATA EXT(5)/'.tbl'/
      DATA EXT(6)/'.tbl'/
C
C      IF (FTYPE.LE.2) THEN
         WRITE(FILE,100) CAM(CAMERA),IMANUM,EXT(FTYPE+1)
C      ELSE
C         WRITE(FILE,200) CAM(CAMERA),IMANUM,AP(APERTU+1)
C      ENDIF   
 100  FORMAT(A3,I5.5,A4)
C 200  FORMAT(A3,I5.5,A1)
      RETURN
      END
