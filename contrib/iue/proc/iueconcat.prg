!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!     iueconcat.prg      A.Fuente, May 21, 97.
!
!.PURPOSE
!     Rebins IUE High Disp spectra  & connects overlapping orders
!
!.KEYWORDS
!     MIDAS TABLES, ORDERS, IUE HIGH DISPERSION
!
!.CALLING SEQUENCE
!
! CONCATENATE/IUE in out [start,end,step] [con_flag] [cam] [:flux] [:eps]
!
!--------------------------------------------------------------------------
!
  DEF/PAR P1 ? 		T "Input table name: "
  DEF/PAR P2 ?          C "Output table name: "
  DEF/PAR P3 0,0,0      C "Start,end,stepsize of output table"
  DEF/PAR P4 O          C "Concatenation flag: (O/L/R/C/E) def. [O] :
  DEF/PAR P5 XXX        C "Camera (1/2/3) or (LWP/LWR/SWP): "
  DEF/PAR P6 :FLUX      C "Flux column [:FLUX] "
  DEF/PAR P7 :EPSILON   C "Quality column [:EPSILON/:QUALITY] "
!
!
  DEF/LOC SWSTART/R/1/1 1100.0
  DEF/LOC SWEND/R/1/1   2000.0
  DEF/LOC SWSTEP/R/1/1      0.1
  DEF/LOC LWSTART/R/1/1 1900.0
  DEF/LOC LWEND/R/1/1   3200.0
  DEF/LOC LWSTEP/R/1/1      0.1
  WRITE/KEY INPUTR/R/1/3   0.,0.,0.
!
  WRITE/KEY IN_A         {P1}
  WRITE/KEY OUT_A        {P2}
  WRITE/KEY INPUTR/R/1/3 {P3}
  WRITE/KEY FLAG/C/1/1   {P4(1:1)}
  WRITE/KEY COLF/C/1/60  {P6}
  WRITE/KEY COLW/C/1/60  ":WAVELENGTH"
  WRITE/KEY COLO/C/1/60  ":ORDER"
  WRITE/KEY COLE/C/1/60  {P7}
!
  BRANCH P5(1:1) 3,2,1          SWP,LWR,LWP
  BRANCH P5(1:3) SWP,LWR,LWP    SWP,LWR,LWP
  BRANCH P1(1:3) SWP,LWR,LWP    SWP,LWR,LWP
!
  WRITE/OUT "Sorry Sir ..., Which camera?"
  RETURN

SWP:
  WRITE/KEY CAME/C/1/1 "3"
  IF INPUTR(1) .EQ. 0 INPUTR(1) = {SWSTART}
  IF INPUTR(2) .EQ. 0 INPUTR(2) = {SWEND}
  IF INPUTR(3) .EQ. 0 INPUTR(3) = {SWSTEP} 
  GOTO RUN
!
LWR:
  WRITE/KEY CAME/C/1/1 "2"
  IF INPUTR(1) .EQ. 0 INPUTR(1) = {LWSTART}
  IF INPUTR(2) .EQ. 0 INPUTR(2) = {LWEND}
  IF INPUTR(3) .EQ. 0 INPUTR(3) = {LWSTEP} 
  GOTO RUN
!
LWP:
  WRITE/KEY CAME/C/1/1 "1"
  IF INPUTR(1) .EQ. 0 INPUTR(1) = {LWSTART}
  IF INPUTR(2) .EQ. 0 INPUTR(2) = {LWEND}
  IF INPUTR(3) .EQ. 0 INPUTR(3) = {LWSTEP} 
  GOTO RUN
!
RUN:
  RUN CON_EXE:iueconcat






