! 
!  Procedure: INIUE
!
!
CROSSREF FILES NAME DEVICE SELECT FLAGS
!
DEFINE/PAR P1 ? NUMBER "File spec. (e.g. 17,22-34,109):"
DEFINE/PAR P2 ? CHAR   "File identification(Max 3 chars):"
DEFINE/PAR P3 ? CHAR   "Tape unit:"
DEFINE/PAR P4 ALL ?    "Selection (F,R,P,E,L,H):"
DEFINE/PAR P5 IS CHAR  "Flags (Integer/Real,Short/Full):"
!   
IF AUX_MODE(1) .LE. 1 THEN              ! VMS
	$ MOUNT/FOREIGN/BLOCK=2048/RECO=2048 'P3'
	RUN CON_EXE:IUEIN
	$ DISMOUNT/NOUNLOAD 'P3'
ELSE					! UNIX
	RUN CON_EXE:IUEIN
ENDIF
