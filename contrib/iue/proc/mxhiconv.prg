!.IDENTIFICATION
! procedure mxhiconv.prg
! 
!.PURPOSE
!  converts MXHI FITS file into standard table. 
!
!.SYNTAX
!   convert/mxhi camimage
!
define/par  p1 ? char "Enter CAMimage for MXHI file:"
intape/fits  1  a  'p1'.MXHI
IF AUX_MODE(1) .LE. 1 THEN              ! VMS
	$ rename a0001.tbl 'p1'.tbl
	$ tblconv := $CON_EXE:tblconv.exe
        $ tblconv 'p1'
ELSE                                    ! UNIX
	$ mv a0001.tbl 'p1'.tbl
        $ $CON_EXE/tblconv.exe 'p1'
ENDIF 
