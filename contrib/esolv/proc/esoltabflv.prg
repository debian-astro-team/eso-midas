! @(#)esoltabflv.prg	19.1 (ES0-DMD) 02/25/03 13:24:23
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ESOLTABFLV.PRG
!.USE:		  @s esoltabflv p1 p2 p3
!		  P1 = name of intermediate table file used for ESOLV
!		  P2 = name of output ascii file (program appends .DAT)
!		  P3 = flag: 0 = print all, 1 = print all but octants
!.PURPOSE:	  Lists contents of special table file
! NOTE:		  The special table file was used for the construction of
!		  the ESOLV database;
!		  The example table file, E001.TBL, resides in directory
!		  PMID$DISK:[MIDAS.CALIB.DATA.ESOLV]
!-----------------------------------------------------------------------
DEFINE/PARAM  P1 	E001 	T
DEFINE/PARAM  P2 	E001	
DEFINE/PARAM  P3	1	N
WRITE/KEYW IN_A		'P1'
WRITE/KEYW OUT_A		'P2'
WRITE/KEYW INPUTI	'P3'
RUN CON_EXE:TABFLV
