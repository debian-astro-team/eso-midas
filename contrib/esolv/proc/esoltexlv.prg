! @(#)esoltexlv.prg	19.1 (ES0-DMD) 02/25/03 13:24:23
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ESOLTEXLV.PRG
!.USE:		  @s esoltextlv p1 p2
!		  P1 = name of ESOLV input table file (default PCAT)
! 		  P2 = name of Tex include file
!.PURPOSE:	  prepare Tex file with selected parameters from ESOLV
! NOTE:		  New page and header after 35 objects
!		  Remember to sort ESOLV in RA order!
!------------------------------------------------------------------------
DEFINE/PARAM  P1 	PCAT 	T
DEFINE/PARAM  P2 	TABNEW  
WRITE/KEYW IN_A  'P1'
WRITE/KEYW IN_B  'P2'
RUN CON_EXE:TEXLV
