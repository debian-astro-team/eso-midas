! @(#)esolfromod.prg	19.1 (ES0-DMD) 02/25/03 13:24:23
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ESOLFROMOD.PRG 
!.PURPOSE: get ESO-LV images from optical disks.
!.USE:     @s esolfromod 
!.AUTHOR:  J.E.Huizinga ESO - Garching 7 JUL 1989
!.NOTE:    calls SETSELECT.EXE, FROMOD.EXE, LOGINMC2.COM, INTA.PRG
!          This procedure can be used in two modes, one in which
!          the user has selected galaxies in a input table, before calling
!          FROMOD, and one in which the user can give the file-names
!          after calling FROMOD.
!--------------------------------------------------------------------- 
SET/FORMAT I8
DEFINE/PARAM P1 P CHAR
DEFINE/PARAM P2 B CHAR
DEFINE/LOCAL N/I/1/1 1
DEFINE/LOCAL FATAL/I/1/1 0
!
! if selected entries in pcat are used, skip the file-number part:
! else ask wanted file-numbers:
IF "'P1'" .EQ. "A" GOTO RUNFROMOD
IF "'P1'" .EQ. "a" GOTO RUNFROMOD
IF "'P1'" .EQ. "P" GOTO MORE
IF "'P1'" .EQ. "p" GOTO MORE
WRITE/OUT  *** FATAL: Not a valid mode: 'P1'
RETURN
!
! loop to read in galaxy identifiers:
MORE:   
INPUTI('N') =  99999999
INQUIRE/KEYW INPUTI/I/'N'/1 Give ESO-identifier:
!
! line below: true if only <cr> is given.
IF INPUTI('N') .EQ. 99999999 GOTO RUNFROMOD
N = N+1
GOTO MORE
!
RUNFROMOD:                                   ! from here on two modes are equal:
!
! create listings on microvax:
N = N-1
RUN CON_EXE:FROMOD
IF FATAL .EQ. 1 RETURN
!
! login on microvax and execute FROMOD.COM:
$ @MID_DISK:[MIDAS.LOCAL]LOGINMC2
!
! copy all *.MFITS and INTA.PRG file from microvax to working space:
$ COPY MC2::ARCHIVE:[ESOLV]*.MFITS  MID_WORK:*.MT
$ COPY MC2::ARCHIVE:[ESOLV]INTA.PRG MID_WORK:*
!
@@ MID_WORK:INTA                                            ! run INTA.PRG
$ DEL MID_WORK:INTA.PRG;                                 ! delete inta.prg
