! @(#)esolmtablv.prg	19.1 (ES0-DMD) 02/25/03 13:24:23
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ESOLMTABLV.PRG
!.USE:		  @s esolmtablv p1 p2 p3 p4 p5
!		  P1 = name of ESOLV input table file (default PCAT)
! 		  P2 = col no of 1st mag value in array of max 13 elems
! 		  P3 = col no of reference mag (normally total mag)
!		  P4 = fractional (total) light wanted
!		  P5 = col no of output semidiameter corresp to FRAC
!.PURPOSE:	  find semidiameter of ellipse at given fraction of light
! NOTE:		  undefined semidiameters are set to NULL
!------------------------------------------------------------------------
DEFINE/PARAM  P1 	PCAT 	T
DEFINE/PARAM  P2 	47   	N
DEFINE/PARAM  P3 	15   	N
DEFINE/PARAM  P4 	0.2  	N
DEFINE/PARAM  P5 	183  	N
WRITE/KEYW IN_A/C/1/12  'P1'
WRITE/KEYW INPUTI/I/1/1 'P2'
WRITE/KEYW INPUTI/I/2/1 'P3'
WRITE/KEYW INPUTI/I/3/1 'P5'
WRITE/KEYW INPUTR       'P4'
RUN CON_EXE:MTABLV
