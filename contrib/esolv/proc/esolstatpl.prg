! @(#)esolstatpl.prg	19.1 (ES0-DMD) 02/25/03 13:24:23
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: ESOLSTATPL.PRG
!.USE:            @s esolstatpl p1 p2 p3 
!                 P1 = input table
!                 P2 = column number of the wanted column 
!		       for mean and standard deviation;
!                      also the name of the output table T'P2'
!                 P3 = logical expression select
!		  P4 = 1: display statistics
!.AUTHOR:         Edwin Valentijn   881011
! 		  Andris Lauberts   890721  converted with SX and TX 
!					    suppressed SEND/PLOT
!.EXAMPLE:        STATPL  PCAT 14 #182.GT.0.5
! NOTE		  Intended for statistics on ESOLV catalogue
!		  using Type = col #7 as reference
!------------------------------------------------------------------
! DEFAULTS PCAT ?
WRITE/KEYW INPUTI/I/1/1 'P2'
WRITE/KEYW IN_A/C/1/1 T
WRITE/KEYW IN_A/C/2/8   'P2'
SELECT/TABLE             'P1' 'P3'
!
RUN CON_EXE:STATPL
!
$TYPE FOR007.DAT
$RENAME FOR007.DAT 'IN_A'.DAT
ASSI/PRINT FILE PRINT.DAT
PRINT/TABLE 'IN_A' #1 #2 #3 #4
$copy/concat 'IN_A'.DAT,PRINT.DAT PRINT1.DAT
$PRINT PRINT1.DAT
!
!IF 'P4' .NE. 1 THEN EXIT
!
STATISTICS/TABLE 'IN_A' #5
COMP/KEYW OUTPUTR(20) = OUTPUTR(3)*1.   ! miniumum
STATISTICS/TABLE 'IN_A' #6
COMP/KEYW OUTPUTR(10) = OUTPUTR(4)-OUTPUTR(20)
COMP/KEYW OUTPUTR(19) = M$ABS(OUTPUTR(10))  ! long way to step
COMP/KEYW OUTPUTR(18) = M$LOG(OUTPUTR(19))
COMP/KEYW OUTPUTR(17) = M$NINT(OUTPUTR(18))
COMP/KEYW OUTPUTR(16) = 2.30258509*OUTPUTR(17)
COMP/KEYW OUTPUTR(15) = M$EXP(OUTPUTR(16))
COMP/KEYW OUTPUTR(14) = OUTPUTR(15)/5.       ! step
COMP/KEYW OUTPUTR(13) = OUTPUTR(20)/OUTPUTR(14)
COMP/KEYW OUTPUTR(12) = M$NINT(OUTPUTR(13))
COMP/KEYW OUTPUTR(11) = OUTPUTR(12)*OUTPUTR(14) ! minimum in step units
SET/GRAPH YAXIS='OUTPUTR(11)','OUTPUTR(4)','OUTPUTR(14)'
SET/GRAPH XAXIS=AUTO SSIZE=3 STYPE=5
PLOT/TABLE 'IN_A' #1 #3
SET/GRAPH LTYPE=1 STYPE=0
OVERPLOT/TABLE 'IN_A' #1 #3
!SET/GRAPH LTYPE=2
OVERPLOT/TABLE 'IN_A' #1 #5
OVERPLOT/TABLE 'IN_A' #1 #6
!
!COPY/GRAPH LASER
