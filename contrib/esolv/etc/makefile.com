$ ! @(#)makefile.com	19.1 (ESO-IPG) 02/25/03 13:24:13
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.TEST.*.*.ETC]
$ ! .REMARKS     Generated by hand
$ ! .DATE        30.12.92
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ COPY *.ctx [---.context]
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
