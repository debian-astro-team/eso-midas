/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  wa_entr.c
**
.VERSION
 090810		last modif

*******************************************************************************
**
**    DESCRIPTION  Extraction of a plane from a wavelet transform
**    ----------- 
**                 
******************************************************************************
**
**    DESCRIPTION   program which allows to replace a plane of a wavelet 
**    -----------   transform by an image
**
**    PARAMETER
**    ---------
**   
**     File_Name_Transform (keyword: IN_A) = 
**          input file name of the wavelet transform
**     File_Name_Imag (keyword: IN_B) = input file name of the image
**     File_Name_Transform (keyword: OUT_A) = 
**          output file name of the wavelet transform
**     Num_Plan (keyword: INPUTI) =
**          plane number to extract (Num_Plan = 1 .. Number_of_Planes)
**
******************************************************************************/


#include <stdio.h>
#include <math.h>
#include <string.h>

#include <midas_def.h>

#include "Def_Math.h"
#include "Def_Mem.h"
#include "Def_Wavelet.h"

#define VISU_PARAM FALSE

extern void wavelet_enter_plan_file();


/*****************************************************************************/

int main()
{
    string File_Name_Transform, File_Name_Imag, File_Trans_Out;
    int Unit; 
    int Num_Plan;
    int Stat, Null, Actvals, Buffer_Int, Maxvals, Felem;
 
    /* Initialisation */  
    SCSPRO("enter_plan");

    /* read the image file name */
    Felem = 1;
    Maxvals = 60;
    Stat = SCKGETC("IN_A", Felem, Maxvals, &Actvals, File_Name_Transform);

    /* read the  output image file name */
    Stat = SCKGETC("IN_B", Felem, Maxvals, &Actvals, File_Name_Imag);

    Felem = 1;
    Maxvals = 1;
    Stat= SCKRDI("INPUTI", Felem, Maxvals, &Actvals, &Buffer_Int, &Unit, &Null);
    Num_Plan = Buffer_Int;

    /* read the  output image file name */
    Felem = 1;
    Maxvals = 60;
    Stat = SCKGETC("OUT_A", Felem, Maxvals, &Actvals, File_Trans_Out);

#if VISU_PARAM
    { char Send[100];
    sprintf (Send,"Nom du fichier de la transformee   =%s\n",File_Name_Transform);
    SCTPUT(Send);
    sprintf (Send,"Nom de fichier image               =%s\n",File_Name_Imag);
    SCTPUT(Send);
    sprintf (Send,"Nom du fichier ondelette en sortie =%s\n",File_Trans_Out);
    SCTPUT(Send);
    sprintf(Send,"Numero du plan=%d\n",Num_Plan); 
    SCTPUT(Send);
    }
#endif

    wavelet_enter_plan_file (File_Name_Imag, File_Name_Transform, 
                             Num_Plan, File_Trans_Out);


   /* End */
   return SCSEPI();
}
