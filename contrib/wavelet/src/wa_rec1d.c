/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck 
**
**    Date:  03/02/25
**    
**    File: wa_rec1d.c
**
.VERSION
 090810		last modif
 
*******************************************************************************
**
**    DESCRIPTION 1D Wavelet reconstruction program.
**    ----------- 
**
**      File_Name_Imag (keyword: IN_A):
**             File name of the wavelet transform (input image)
**
**      File_Name_Transform (keyword: OUT_A):
**             File name of the reconstruct image
**
**      Type_transform (keyword: INPUTI[1]): 
**                   1: TO1_FRENCH : french hat
**                   2: TO1_MEX : mexican hat
**                   3: TO1_LINEAR:a trous algorithm with linear 
**                      scaling function
**                   4: TO1_LINEAR:a trous algorithm with B1-spline 
**                      scaling function
**                   5: TO1_LINEAR:a trous algorithm with B3-spline 
**                      scaling function
**                   6: TO1_MORLET: Morlet's transform
**
**      Number of channels per octove  (keyword: INPUTI[2]):
**                   only use with trasform 1,2,6
**
**      Nu_0   (keyword: INPUTR[1]):   Morlet's parameter
**                   only use with trasform 6        
** 
******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <midas_def.h>

#include "Def_Math.h"
#include "Def_Mem.h"
#include "Def_Wavelet.h"

#define VISU_PARAM FALSE

extern void io_read_file_to_pict_f(), io_write_pict_f_to_file();
extern void  wave1d_recons();

/*****************************************************************************/

int main()
{
    string File_Name_Imag, File_Name_Transform;
    int Unit;
    int Type_Transform;
    int Null, Actvals, Buffer_Int, Maxvals, Felem;
    int Stat;
    float *Imag, *Signal, **W_1D, Nu_0;
    int i,j,Nl, Nc, Nbr_Voie;
 
    /* Initialisation */  
    SCSPRO("transform");

    /* read the wavelet transform file name */
    Felem = 1;
    Maxvals = 60;
    Stat = SCKGETC("IN_A", Felem, Maxvals, &Actvals, File_Name_Transform);

    /* read the image output file name */
    Stat = SCKGETC("OUT_A", Felem, Maxvals, &Actvals, File_Name_Imag);

    /* read the transform type */
    Felem = 1;
    Maxvals = 1;
    Stat = SCKRDI("INPUTI",Felem, Maxvals, &Actvals, &Buffer_Int, &Unit, &Null);
    Type_Transform = Buffer_Int;

    /* read the number of channels  */
    Felem = 2;
    Maxvals = 1;
    Stat = SCKRDI("INPUTI",Felem,Maxvals,&Actvals,&Buffer_Int,&Unit,&Null);
    Nbr_Voie = Buffer_Int;

    /* read Morlet's parameter  */
    Felem = 1;
    Maxvals = 1;
    Stat = SCKRDR("INPUTR", Felem, Maxvals, &Actvals, &Nu_0, &Unit, &Null);

    io_read_file_to_pict_f (File_Name_Transform, &Imag, &Nl, &Nc);
    W_1D = f_matrix_alloc(Nl,Nc);

    for(i = 0; i < Nl; i++)
    for(j = 0; j < Nc; j++)
       W_1D[i][j] = Imag[i*Nc+j];
    free ((char *) Imag);
    if (Type_Transform == TO1_MORLET) Nl /= 2;

    /* Wavelet reconstruction */
    wave1d_recons (W_1D, Nc, Nl, Type_Transform, Nbr_Voie, &Signal, Nu_0);
    io_write_pict_f_to_file (File_Name_Imag, Signal, 1, Nc);

   /* End */
 return  SCSEPI();
}





