/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  wa_info.c
**
.VERSION
 090904		last modif

*******************************************************************************
**
**    DESCRIPTION Gives information about a wavelet file 
**    -----------  
**                 
**
**    PARAMETRES    
**    ----------    
**
**         File_Name_Transform (keyword: IN_A):
**             File name of the input wavelet transform
**
**    RESULTS  Writes in Output_File information about the wavelet    
**    -------  wavelet transform
**                          
******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <midas_def.h>

#include "Def_Math.h"
#include "Def_Mem.h"
#include "Def_Wavelet.h"

#define VISU_PARAM FALSE

extern void wavelet_extract_plan(), lib_mat_moy_ecart_type();
extern void wave_io_read();


/*****************************************************************************/

int main()
{
    wave_transf_des Wavelet;
    struct mallat_plan_des *Ptr_Mallat;

    int i, Nl, Nc, Nbr_Plan;
    float *Plan, Moy, Sigma;
    int Stat, Actvals, Maxvals, Felem;
    string File_Name_Transform, Send;

    /* Initialisation */
    SCSPRO("info");

    /* read the image file name */
    Felem = 1;
    Maxvals = 60;
    Stat = SCKGETC("IN_A", Felem, Maxvals, &Actvals, File_Name_Transform);

    /* read the wavelet */
    wave_io_read (File_Name_Transform, &Wavelet);
    Nbr_Plan = Wavelet.Nbr_Plan;
    Nl = Wavelet.Nbr_Ligne;
    Nc = Wavelet.Nbr_Col;

    sprintf (Send,"\n            Wavelet file : %s\n", File_Name_Transform);
    SCTPUT(Send);
    sprintf (Send,"\n   Original name image : %s", Wavelet.Name_Imag);
    SCTPUT(Send);
    sprintf(Send,"   Number of lines : %d", Wavelet.Nbr_Ligne);
    SCTPUT(Send);
    sprintf (Send,"   Number of columns : %d", Wavelet.Nbr_Col);
    SCTPUT(Send);
    sprintf (Send,"   Number of scales : %d", Wavelet.Nbr_Plan);
    SCTPUT(Send);

    switch (Wavelet.Type_Wave_Transform)
    {
        case TO_PAVE_LINEAR:
          sprintf (Send,"   Type : %d = a trou algorithm",
                                          Wavelet.Type_Wave_Transform);
          SCTPUT(Send);
          sprintf (Send,"               phi =  linear scaling function");
          SCTPUT(Send);
          sprintf (Send,"               psi =   ");
          SCTPUT(Send); 
          break;
        case TO_PAVE_BSPLINE:
          sprintf (Send,"   Type : %d = a trou algorithm",
                                          Wavelet.Type_Wave_Transform);
          SCTPUT(Send);
          sprintf (Send,"               phi =  B3-spline scaling function ");
          SCTPUT(Send);
          sprintf (Send,"               psi =  difference between two interpolations ");
          SCTPUT(Send);
          break;
        case TO_PAVE_BSPLINE_FFT:
          sprintf (Send,"   Type : %d = algorithm using the FFT without reducing the sampling",
                                           Wavelet.Type_Wave_Transform);
          SCTPUT(Send);
          sprintf (Send,"               phi =  B3-bspline in the Fourier space");
          SCTPUT(Send);
          sprintf (Send,"               psi =  difference between two interpolations");
          SCTPUT(Send);
          sprintf (Send,"               phi's cut-off frequency : %6.3f",
                                           Wavelet.Pyramid.Freq_Coup);
          SCTPUT(Send);
          break;
        case TO_PYR_LINEAR:
          sprintf (Send,"   Pyramide size: %d", Wavelet.Pyramid.Size);
          SCTPUT(Send);
          sprintf (Send,"   Type : %d = pyramidal transform in the direct space",
                                          Wavelet.Type_Wave_Transform);
          SCTPUT(Send);
          sprintf (Send,"               phi =  linear scaling function");
          SCTPUT(Send);
          sprintf (Send,"               psi =  difference between two interpolations ");
          SCTPUT(Send);
          break;
        case TO_PYR_BSPLINE:
          sprintf (Send,"   Pyramide size: %d", Wavelet.Pyramid.Size);
          SCTPUT(Send);
          sprintf (Send,"   Type : %d = pyramidal transform in the direct space",
                                          Wavelet.Type_Wave_Transform);
          SCTPUT(Send);
          sprintf (Send,"               phi =  B3-spline scaling function");
          SCTPUT(Send);
          sprintf (Send,"               psi =  difference between two interpolations");
          SCTPUT(Send);
          break;
        case TO_PYR_FFT_DIFF_RESOL:
          sprintf (Send,"   Pyramide size: %d", Wavelet.Pyramid.Size);
          SCTPUT(Send);
          sprintf (Send,"   Type : %d = pyramidale transform using FFT",
                                           Wavelet.Type_Wave_Transform);
          SCTPUT(Send);
          sprintf (Send,"               phi =  B3-bspline in the Fourier space");
          SCTPUT(Send);
          sprintf (Send,"               psi =  difference between two interpolations");
          SCTPUT(Send);
          sprintf (Send,"               phi's cut-off frequency : %6.3f",
                                           Wavelet.Pyramid.Freq_Coup);
          SCTPUT(Send);
          break;
        case TO_PYR_FFT_DIFF_SQUARE_RESOL:
          sprintf (Send,"   Pyramide size: %d", Wavelet.Pyramid.Size);
          SCTPUT(Send);
          sprintf (Send,"   Type : %d = pyramidale transform using FFT",
                                           Wavelet.Type_Wave_Transform);
          SCTPUT(Send);
          sprintf (Send,"               phi =  B3-bspline in the Fourier space");
          SCTPUT(Send);
          sprintf (Send,"               psi =  difference between  the square of two interpolations");
          SCTPUT(Send);
          sprintf (Send,"               phi's cut-off frequency : %6.3f",
                                           Wavelet.Pyramid.Freq_Coup);
          SCTPUT(Send);
          break;
        case TO_MALLAT_BARLAUD:
          sprintf (Send,"   Mallat's transform with bi-orthogonal filters");
          SCTPUT(Send);
                   break;
        default:
                    fprintf (stderr, "Unknow transform");
                    break;

    } 

    if (Wavelet.Type_Wave_Transform != TO_MALLAT_BARLAUD)
    for (i = 1; i <= Nbr_Plan; i++)
    {
        float Min, Max;

        /* plane extraction */
        wavelet_extract_plan (&Wavelet, &Plan, &Nl, &Nc, i);

        sprintf (Send,"\n Scale number: %d", i);

        lib_mat_moy_ecart_type (Plan, Nl, Nc, &Sigma, &Moy);

        MIN_MAX_IMA(Plan,Nl*Nc,Min,Max);

        sprintf (Send,"\n   Scale Number = %d", i);
        SCTPUT(Send);
        sprintf (Send,"   Number of lines = %d", Nl);
        SCTPUT(Send);
        sprintf (Send,"   Number of columns = %d",Nc);
        SCTPUT(Send);
        sprintf (Send,"   Average = %f",Moy);
        SCTPUT(Send);
        sprintf (Send,"   Standard deviation = %f",Sigma);
        SCTPUT(Send);
        sprintf (Send,"   Minimum = %f",Min);
        SCTPUT(Send);
        sprintf (Send,"   Maximum = %f",Max);
        SCTPUT(Send);

        free ((char *) Plan);
    }
    else 
    {
        Ptr_Mallat = &(Wavelet.Mallat);
        for (i = 1; i < Nbr_Plan; i++)
	{
           float Min, Max;

           sprintf (Send,"\n Scale number: %d", i);
           SCTPUT(Send);
           Nl = Ptr_Mallat -> Nl;
           Nc = Ptr_Mallat -> Nc;
           sprintf (Send,"   Number of lines = %d", Nl);
           SCTPUT(Send);
           sprintf (Send,"   Number of columns = %d\n",Nc);
           SCTPUT(Send);

           lib_mat_moy_ecart_type (Ptr_Mallat -> Coef_Horiz, 
                                Nl, Nc, &Sigma, &Moy);
           MIN_MAX_IMA(Ptr_Mallat -> Coef_Horiz,Nl*Nc,Min,Max);
           sprintf (Send,"   Horizontal Details");
           SCTPUT(Send);
           sprintf (Send,"   Average = %f",Moy);
           SCTPUT(Send);
           sprintf (Send,"   Standard deviation = %f",Sigma);
           SCTPUT(Send);
           sprintf (Send,"   Minimum = %f",Min);
           SCTPUT(Send);
           sprintf (Send,"   Maximum = %f\n",Max);
           SCTPUT(Send);

           lib_mat_moy_ecart_type (Ptr_Mallat -> Coef_Vert, 
                                Nl, Nc, &Sigma, &Moy);
           MIN_MAX_IMA(Ptr_Mallat -> Coef_Vert,Nl*Nc,Min,Max);
           sprintf (Send,"   Vertical Details");
           SCTPUT(Send);
           sprintf (Send,"   Average = %f",Moy);
           SCTPUT(Send);
           sprintf (Send,"   Standard deviation = %f",Sigma);
           SCTPUT(Send);
           sprintf (Send,"   Minimum = %f",Min);
           SCTPUT(Send);
           sprintf (Send,"   Maximum = %f\n",Max);
           SCTPUT(Send);

           lib_mat_moy_ecart_type (Ptr_Mallat -> Coef_Diag, 
                                Nl, Nc, &Sigma, &Moy);
           MIN_MAX_IMA(Ptr_Mallat -> Coef_Diag,Nl*Nc,Min,Max);
           sprintf (Send,"   Diagonal Details");
           SCTPUT(Send);
           sprintf (Send,"   Average = %f",Moy);
           SCTPUT(Send);
           sprintf (Send,"   Standard deviation = %f",Sigma);
           SCTPUT(Send);
           sprintf (Send,"   Minimum = %f",Min);
           SCTPUT(Send);
           sprintf (Send,"   Maximum = %f\n",Max);
           SCTPUT(Send);

           if (i == Nbr_Plan-1)           
	   {

              sprintf (Send,"\n Scale number: %d", i+1);
              SCTPUT(Send);
              sprintf (Send,"   Number of lines = %d", Nl);
              SCTPUT(Send);
              sprintf (Send,"   Number of columns = %d",Nc);
              SCTPUT(Send);

              lib_mat_moy_ecart_type (Ptr_Mallat -> Low_Resol, 
                                Nl, Nc, &Sigma, &Moy);
              MIN_MAX_IMA(Ptr_Mallat -> Low_Resol,Nl*Nc,Min,Max);
              sprintf (Send,"   Lowest Resolution");
              SCTPUT(Send);
              sprintf (Send,"   Average = %f",Moy);
              SCTPUT(Send);
              sprintf (Send,"   Standard deviation = %f",Sigma);
              SCTPUT(Send);
              sprintf (Send,"   Minimum = %f",Min);
              SCTPUT(Send);
              sprintf (Send,"   Maximum = %f\n",Max);
              SCTPUT(Send);
	   }
           else  Ptr_Mallat = Ptr_Mallat -> Smooth_Imag;
        }

   }

   /* End */
 return  SCSEPI();
}
 
