/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**                   Copyright (C) by European Southern Observatory
*******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck 
**
**    Date:  03/02/25
**    
**    File: wa_rec.c
**
.VERSION
 090810		last modif

*******************************************************************************
**
**    DESCRIPTION Wavelet reconstruction program.
**    ----------- 
**
**    PARAMETRES    
**    ----------   
** 
**      File_Name_Wavelet (keyword: IN_A):
**             File name of the input wavelet transform
**
**      File_Name_Imag (keyword: OUT_A):
**             File name of the output image
**
**        Build_Direct_Ok (keyword: INPUTI[0]): 
**             if the wavelet transform algorithm used the FFT and is pyramidal
**               (Type_Wave_Transform =  TO_PYR_FFT_DIFF_RESOL
**                                    or TO_PYR_FFT_DIFF_SQUARE_RESOL)
**             then if Build_Direct_Ok = 1 (TRUE) then
**                       the reconstruction is done by addition
**                       of the wavelet coefficient in the Fourier space
**                  else (Build_Direct_Ok = 0 (FALSE))  
**                       the reconstruction is done from a least mean square
**                       estimation
** 
**    RESULTS : reconstructs an image of name  File_Name_Imag
**    -------   from its wavelet coefficients   
**                 
**    LIMITS : This program has been tested for image size <= 512
**    ------
**
******************************************************************************/


#include <stdio.h>
#include <math.h>
#include <string.h>

#include <midas_def.h>

#include "Def_Math.h"
#include "Def_Mem.h"
#include "Def_Wavelet.h"

#define VISU_PARAM FALSE

extern void wavelet_reconstruct_file();


/*****************************************************************************/

int main()
{
    string File_Name_Imag, File_Name_Transform;
    int Unit;
    int Build_Direct_Ok;
    int Null, Actvals, Buffer_Int, Maxvals, Felem;
    int Stat;
 
    /* Initialisation */  
    SCSPRO("transform");

    /* read the wavelet transform file name */
    Felem = 1;
    Maxvals = 60;
    Stat = SCKGETC("IN_A", Felem, Maxvals, &Actvals, File_Name_Transform);

    /* read the image output file name */
    Stat = SCKGETC("OUT_A", Felem, Maxvals, &Actvals, File_Name_Imag);

    /* read the Build_Direct_Ok parameter*/
    Felem = 1;
    Maxvals = 1;
    Stat = SCKRDI("INPUTI",Felem,Maxvals,&Actvals,&Buffer_Int,&Unit,&Null);
    Build_Direct_Ok = Buffer_Int;

#if VISU_PARAM
    {
    char Send[100];
    sprintf(Send," File_Name_Imag = %s\n", File_Name_Imag);
    SCTPUT(Send);
    sprintf(Send," File_Name_Transform = %s\n", File_Name_Transform);
    SCTPUT(Send);
    sprintf(Send," Build_Direct_Ok = %d\n", Build_Direct_Ok);
    SCTPUT(Send);
  }
#endif

    /* Wavelet reconstruction */
    wavelet_reconstruct_file (File_Name_Imag, 
                              File_Name_Transform, Build_Direct_Ok);

   /* End */
 return  SCSEPI();
}





