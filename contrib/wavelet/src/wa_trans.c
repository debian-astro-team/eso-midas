/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck 
**
**    Date:  03/02/25
**    
**    File: wa_trans.c
**
.VERSION
 090810		last modif

*******************************************************************************
**
**    DESCRIPTION Wavelet Transform program.
**    ----------- 
**                 
**
**    PARAMETRES    
**    ----------   
** 
**      File_Name_Imag (keyword: IN_A):
**             File name of the input image
**
**      File_Name_Transform (keyword: OUT_A):
**             File name of the output wavelet transform
**
**       Type_transform (keyword: INPUTI[0]): 
**             1 : a trous algorithm with linear scaling function
**                  The wavelet function is the difference between two resolutions
**             2 : a trous algorithm with B3-spline scaling function
**                  The wavelet function is the difference between two resolutions
**             3 : algorithm using the FFT without reducing the sampling
**                  The Fourier transform of the scaling function is a B3-spline
**                  The wavelet function is the difference between two resolutions
**             4 : pyramidal algorithm with a linear scaling function
**                  The wavelet function is the difference between two resolutions
**             5 : pyramidal algorithm with a B3-spline scaling function
**                  The wavelet function is the difference between two resolutions
**             6 : pyramidal algorithm using the FFT:
**                  The Fourier transform of the scaling function is a B3-spline
**                  The wavelet function is the difference between two resolutions
**             7 : pyramidal algorithm using the FFT:
**                  The Fourier transform of the scaling function is a B3-spline
**                  The wavelet function is the difference between the square of
**                   two resolutions
**             8 : Mallat's Algorithm with biorthogonal filters
**
**      Nbr_Plan (keyword: INPUTI[1]): 
**             Number of scales                             
** 
**    RESULTS : Computes the wavelet transform of an image and put it   
**    -------   in a file.    
**                 
**    LIMITS : This program has been tested for image size <= 512
**    ------
**
**    CONSTRAINTS : For algorithms using the FFT, images size must be a
**    -----------   power of two.
**                  The number of scales must be less than 7
**                  The frequency cut-off must verify: 0 < Fc <= 0.5
**
******************************************************************************/


#include <stdio.h>
#include <math.h>
#include <string.h>

#include <midas_def.h>

#include "Def_Math.h"
#include "Def_Mem.h"
#include "Def_Wavelet.h"

#define VISU_PARAM FALSE

extern void wavelet_transform_file();
               
/*****************************************************************************/

int main()
{
    string File_Name_Imag, File_Name_Transform;
    int Unit;
    int Type_Transform, Nbr_Plan;
    int Stat, Null, Actvals, Buffer_Int, Maxvals, Felem;
    float Fc;
 
    /* Initialisation */  
    SCSPRO("transform");

    /* read the image file name */
    Felem = 1;
    Maxvals = 60;
    Stat = SCKGETC("IN_A", Felem, Maxvals, &Actvals, File_Name_Imag);

    /* read the wavelet transform output file name */
    Stat = SCKGETC("OUT_A", Felem, Maxvals, &Actvals, File_Name_Transform);

    /* read the transform type */
    Felem = 1;
    Maxvals = 1;
    Stat = SCKRDI("INPUTI",Felem, Maxvals, &Actvals, &Buffer_Int, &Unit, &Null);
    Type_Transform = Buffer_Int;

    /* read the  number of scales */
    Felem = 2;
    Maxvals = 1;
    Stat = SCKRDI("INPUTI",Felem, Maxvals, &Actvals, &Buffer_Int, &Unit, &Null);
    Nbr_Plan = Buffer_Int;

    /* read the frequency cut-off */
    Felem = 1;
    Maxvals = 1;
    Stat = SCKRDR("INPUTR", Felem, Maxvals, &Actvals, &Fc, &Unit, &Null);

#if VISU_PARAM
    { char Send[100];
    sprintf(Send," File_Name_Imag = %s\n", File_Name_Imag);
    SCTPUT(Send);
    sprintf(Send," File_Name_Transform = %s\n", File_Name_Transform);
    SCTPUT(Send);
    sprintf(Send," Type_Transform = %d\n", Type_Transform);
    SCTPUT(Send);
    sprintf(Send," Nbr_Plan = %d\n", Nbr_Plan);
    SCTPUT(Send);
    sprintf(Send," Fc = %f\n", Fc);
    SCTPUT(Send);
    }
#endif

    /* Wavelet transform */
    wavelet_transform_file (File_Name_Imag, File_Name_Transform, 
                            Type_Transform, Fc, Nbr_Plan);
 
   /* End */
return SCSEPI();
}


