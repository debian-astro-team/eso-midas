/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  wa_head.c
**
.VERSION
 090810		last modif

*******************************************************************************
**
**    DESCRIPTION Gives information about a wavelet file 
**    -----------  
**                 
**
**    PARAMETRES    
**    ----------    
**         OUTPUTI[1] = number of lines
**         OUTPUTI[2] = number of columns
**         OUTPUTI[3] = number of scales
**         OUTPUTI[4] = algorithm number
**         OUTPUTR[1] = Frequency cut-off
**         OUT_A      = PYR, CUB or IMA
**         OUT_B      = 
**         File_Name_Transform (keyword: OU_A):
**             File name of the input wavelet transform
**
**    RESULTS      
**    -------  
**                          
******************************************************************************/


#include <stdio.h>
#include <math.h>
#include <string.h>

#include <midas_def.h>

#include "Def_Math.h"
#include "Def_Mem.h"
#include "Def_Wavelet.h"

extern void wave_io_read();



/*****************************************************************************/

int main()
{
    wave_transf_des Wavelet;
    int Stat, Actvals, Maxvals, Felem, Noelm;
    string File_Name_Transform, Send;
    int  Unit;
    int Buffer_Int;
    float Buffer_Float;


    /* Initialisation */
    SCSPRO("info_header");

    /* read the image file name */
    Felem = 1;
    Maxvals = 60;
    Stat = SCKGETC("IN_A", Felem, Maxvals, &Actvals, File_Name_Transform);

    /* read the wavelet */
    wave_io_read (File_Name_Transform, &Wavelet);

    sprintf (Send,"\n            Wavelet file : %s\n", File_Name_Transform);
    SCTPUT(Send);
    sprintf (Send,"\n   Original name image : %s", Wavelet.Name_Imag);
    SCTPUT(Send);
    sprintf(Send,"   Number of lines : %d", Wavelet.Nbr_Ligne);
    SCTPUT(Send);
    sprintf (Send,"   Number of columns : %d", Wavelet.Nbr_Col);
    SCTPUT(Send);
    sprintf (Send,"   Number of scales : %d", Wavelet.Nbr_Plan);
    SCTPUT(Send);

    Felem = 1;
    Maxvals = 1;
    Buffer_Int = Wavelet.Nbr_Ligne;
    Stat = SCKWRI("OUTPUTI", &Buffer_Int, Felem, Maxvals, &Unit);

    Felem = 2;
    Buffer_Int = Wavelet.Nbr_Col;
    Stat = SCKWRI("OUTPUTI", &Buffer_Int, Felem, Maxvals, &Unit);

    Felem = 3;
    Buffer_Int = Wavelet.Nbr_Plan;
    Stat = SCKWRI("OUTPUTI", &Buffer_Int, Felem, Maxvals, &Unit);

    Felem = 4;
    Buffer_Int = Wavelet.Type_Wave_Transform;
    Stat = SCKWRI("OUTPUTI", &Buffer_Int, Felem, Maxvals, &Unit);
 
    Buffer_Float = 0.;
    Felem = Noelm = 1;
    Maxvals = 60;
    switch (Wavelet.Type_Wave_Transform)
    {
        case TO_PAVE_BSPLINE_FFT:
               Buffer_Float = Wavelet.Pave.Freq_Coup;
        case TO_PAVE_LINEAR:
        case TO_PAVE_BSPLINE:
               sprintf (Send,"   cube : %d", Wavelet.Type_Wave_Transform);
               SCTPUT(Send);
               strcpy (Send,"CUB");
               SCKWRC("OUT_A", Noelm, "CUB", Felem, Maxvals, &Unit);
               break;
        case TO_PYR_FFT_DIFF_RESOL:
        case TO_PYR_FFT_DIFF_SQUARE_RESOL:
               Buffer_Float = Wavelet.Pyramid.Freq_Coup;
        case TO_PYR_LINEAR:
        case TO_PYR_BSPLINE:
             sprintf (Send,"   pyramid : %d", Wavelet.Type_Wave_Transform);
             SCTPUT(Send);
             SCKWRC("OUT_A", Noelm, "PYR", Felem, Maxvals, &Unit);
             break;
        case TO_MALLAT_BARLAUD:
             sprintf (Send,"   image : %d", Wavelet.Type_Wave_Transform);
             SCTPUT(Send);
             SCKWRC("OUT_A", Noelm, "IMA", Felem, Maxvals, &Unit);
             break;
    }
    
    Felem = 1;
    Maxvals = 1;
    Stat = SCKWRR("OUTPUTR", &Buffer_Float, Felem, Maxvals, &Unit);

   /* End */
 return  SCSEPI();
}
 
