/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  wa_extr.c
**
.VERSION
 090810		last modif

*******************************************************************************
**
**    DESCRIPTION  Extraction of a plane from a wavelet transform
**    ----------- 
**                 
******************************************************************************
**
**    DESCRIPTION   program which allows to extract a plane from a 
**    -----------   wavelet transform
**
**    PARAMETER
**    ---------
**
**     File_Name_Transform (keyword: IN_A) = 
**          input file name of the wavelet transform
**     File_Name_Imag (keyword: OUT_A) = output file name of the image
**     Num_Plan (keyword: INPUTI) =
**          plane number to extract (Num_Plan = 1 .. Number_of_Planes)
**
******************************************************************************/


#include <stdio.h>
#include <math.h>
#include <string.h>

#include <midas_def.h>

#include "Def_Math.h"
#include "Def_Mem.h"
#include "Def_Wavelet.h"

#define VISU_PARAM FALSE


extern void wavelet_extract_plan_file();

/*****************************************************************************/

int main()
{
    string File_Name_Transform, File_Name_Extract;
    int Unit; 
    int Num_Plan;
    int Stat, Null, Actvals, Buffer_Int, Maxvals, Felem;
 
    /* Initialisation */  
    SCSPRO("extract_plan");

    /* read the image file name */
    Felem = 1;
    Maxvals = 60;
    Stat = SCKGETC("IN_A", Felem, Maxvals, &Actvals, File_Name_Transform);

    /* read the  output image file name */
    Stat = SCKGETC("OUT_A", Felem, Maxvals, &Actvals, File_Name_Extract);

    /* read the plane number */
    Felem = 1;
    Maxvals = 1;
    Stat = SCKRDI("INPUTI",Felem,Maxvals,&Actvals,&Buffer_Int,&Unit,&Null);
    Num_Plan = Buffer_Int;

#if VISU_PARAM
    { char Send[100];
    sprintf (Send,"Nom du fichier de la transformee=%s\n",File_Name_Transform);
    SCTPUT(Send);
    sprintf (Send,"Nom de fichier extrait=%s\n",File_Name_Extract);
    SCTPUT(Send);
    sprintf(Send,"Numero du plans=%d\n", Num_Plan); 
    SCTPUT(Send);
    }
#endif

    wavelet_extract_plan_file (File_Name_Extract, File_Name_Transform,Num_Plan); 

   /* End */
 return  SCSEPI();
}


