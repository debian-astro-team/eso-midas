/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck 
**
**    Date:  03/02/25
**    
**    File: wa_visu.c
**
.VERSION
 090810		last modif

*******************************************************************************
**
**    DESCRIPTION creation of an image from a wavelet transform
**    ----------- 
**                 
**    PARAMETRES    
**    ----------   
** 
**      File_Name_Transform (keyword: IN_A):
**             File name of the input wavelet transform
**
**      File_Name_Imag (keyword: OUT_A):
**             File name of the output image
**
**    RESULTS : create an image from a wavelet file   
**    -------    
******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <midas_def.h>

#include "Def_Math.h"
#include "Def_Mem.h"
#include "Def_Wavelet.h"

extern float lib_mat_ecart_type();

extern void wave_io_read(), io_write_pict_f_to_file();
extern void mallat_2d_visu(), lib_mat_detect_snr();
extern void wavelet_extract_plan();


/*****************************************************************************/

int main()
{
    int i,j;
    int Nl,Nc;
    wave_transf_des Wavelet;
    float *ImaSynt, *Plan, Sigma, Moy;
    string File_Name_Imag, File_Name_Transform;
    int Stat, Actvals, Maxvals, Felem;

    /* Initialisation */
    SCSPRO("visu");

    /* read the wavelet transform file name */
    Felem = 1;
    Maxvals = 60;
    Stat = SCKGETC("IN_A", Felem, Maxvals, &Actvals, File_Name_Transform);

    /* read the image output file name */
    Stat = SCKGETC("OUT_A", Felem, Maxvals, &Actvals, File_Name_Imag);

   /* read the wavelet */
    wave_io_read (File_Name_Transform, &Wavelet);

#if VISU_PARAM
    { char Send[100];
    sprintf(Send," File_Name_Imag = %s\n", File_Name_Imag);
    SCTPUT(Send);
    sprintf(Send," File_Name_Transform = %s\n", File_Name_Transform);
    SCTPUT(Send);
    }
#endif

    Nl = Wavelet.Nbr_Ligne;
    Nc = Wavelet.Nbr_Col;

    switch (Wavelet.Type_Wave_Transform)
    {
        case TO_MALLAT_BARLAUD:
                 mallat_2d_visu (&Wavelet, &ImaSynt, &Nl, &Nc);
                 break;
        case TO_PAVE_LINEAR:
        case TO_PAVE_BSPLINE:
        case TO_PAVE_BSPLINE_FFT:
              ImaSynt = f_vector_alloc(Nl*Nc);
              for (j = 0; j < Nl*Nc; j++) ImaSynt[j] = 0.;   
              wavelet_extract_plan (&Wavelet, &Plan, &Nl, &Nc, 1);
              /* Sigma = lib_mat_ecart_type(Plan,Nl,Nc);*/
              lib_mat_detect_snr (Nc,Nl,Plan,0,3,&Moy,&Sigma);
              Sigma *= 3.;
              for (i=1; i < Wavelet.Nbr_Plan; i++)
              {
               if (i != 1) wavelet_extract_plan (&Wavelet, &Plan, &Nl, &Nc, i);
               
               for (j = 0; j < Nl*Nc; j++)
               if ((Plan[j] > Sigma) && (ImaSynt[j] < FLOAT_EPSILON))  
                      ImaSynt[j] = 5.+(float) (Wavelet.Nbr_Plan-i)*10.;
               
               Sigma /= 4.;
               free ((char *) Plan);
             }
             break;
    }

   io_write_pict_f_to_file (File_Name_Imag, ImaSynt, Nl, Nc);

   /* End */
 return  SCSEPI();
}

