
/*************************************************************************
**           Copyright (C) 1993 by European Southern Observatory
**************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  Def_Wavelet.h
**
*******************************************************************************
**
**    DESCRIPTION  Data structures definitions for the wavelet package
**    -----------   
******************************************************************************/ 

/* Wavelet transform algorithm number  */
#define TO_PAVE_LINEAR   1
#define TO_PAVE_BSPLINE  2
#define TO_PAVE_BSPLINE_FFT 3
#define TO_PYR_LINEAR 4
#define TO_PYR_BSPLINE 5
#define TO_PYR_FFT_DIFF_RESOL 6
#define TO_PYR_FFT_DIFF_SQUARE_RESOL 7
#define TO_MALLAT_BARLAUD 8

#define MAX_SIZE_NAME_IMAG 100
#define MAX_PLAN_WAVELET 20

/* Function definitions for the algorithms using FFT  */
#define SCALING_FUNCTION 1
#define FILTER_H 2
#define FILTER_H_TILDE 3
#define FILTER_G 4
#define FILTER_G_TILDE 5
#define WAVELET 6

/* Pyramid data structure */
typedef struct 	{
	int Tab_Nl[MAX_PLAN_WAVELET];
	int Tab_Col[MAX_PLAN_WAVELET];
	int Tab_Pos[MAX_PLAN_WAVELET];
	int Size;
	float Freq_Coup;
	float *Data;
	        } pyramid_f_des;

/* Complex pyramid data structure */
typedef struct 	{
	int Tab_Nl[MAX_PLAN_WAVELET];
	int Tab_Col[MAX_PLAN_WAVELET];
	int Tab_Pos[MAX_PLAN_WAVELET];
	int Size;
	float Freq_Coup; /* Frequency cutt-off */
	complex_float *Data;
	        } pyramid_cf_des;

/* Data  structure for an algorithm without reduction of sampling */
typedef struct 	{
	float *Data;
	float Freq_Coup;  /* Frequency cutt-off */
	        } pave_f_des;


/* Data structure for Mallat's algorithm */
struct mallat_plan_des	{
	int Nl,Nc;
        float *Coef_Horiz;
        float *Coef_Diag;
        float *Coef_Vert;
        float *Low_Resol;
        struct mallat_plan_des *Smooth_Imag;
        } mallat_plan_des;

/* Data structure for a wavelet transform */
typedef struct 	{
	/* Image name */
	char Name_Imag [MAX_SIZE_NAME_IMAG];
	/* Line and column number */
	int Nbr_Ligne, Nbr_Col;
	/* Scale Number */
	int Nbr_Plan;
	/* Transform algorithm choosen */
	int Type_Wave_Transform;
	/* Buffer for the data */
	pyramid_f_des Pyramid;
	pave_f_des Pave;
	struct mallat_plan_des Mallat;
	         } wave_transf_des;

/* Data structure for image information */
typedef struct {
        float Sigma;
        float Mean;
        float Min, Max;
        float Energ, Entrop;
        float Correl_Plan[MAX_PLAN_WAVELET];
	         } plan_info_des;

/* Filtering */

    /* Thresholding */
#define FILTER_TRESHOLD 1     

    /* Adaptative thresholding */
#define FILTER_HIERARCHICAL_TRESHOLD 2

    /* Hierarchical Wiener filtering */
#define FILTER_HIERARCHICAL 3

    /* Multiresolution Wiener filtering */
#define FILTER_MULTI_RES_WIENER 4

#define TO1_FRENCH 1
#define TO1_MEX 2
#define TO1_LINEAR 3
#define TO1_B1SPLINE 4
#define TO1_B3SPLINE 5
#define TO1_MORLET 6
#define TO1_ROBUST 7
#define TO1_D1GAUS 8
