% @(#)tran1dwave.hlq	19.1 (ESO-IPG) 02/25/03 13:34:20
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1993 European Southern Observatory
%.IDENT      tran1dwave.hlq
%.AUTHOR     18-MAY-93, IPG/ESO
%.KEYWORDS   MIDAS, help files, 
%.PURPOSE    On-line help file for the command: TRAN1D/WAVE
%.VERSION    1.0  JS : Creation, 18-MAY-93
%----------------------------------------------------------------
\se
SECTION./WAVE
\es\co
TRAN1D/WAVE					   JS  18-MAY-93
\oc\su
TRAN1D/WAVE Im_In Wave_Out [Num_Trans] [Num_Line] [Channel] [Nu]
\us\pu
Purpose:    
            one dimensional wavelet transform. Computess the wavelet
            transform of an image line. The wavelet transform is a two
            dimensional array. Each line of the output image represents
            a scale of the transform.
\up\sy
Syntax:     
            TRAN1D/WAVE Im_In Wave_Out [Num_Trans] [Num_Line] [Channel] [Nu]
\ys\pa
            Im_In =   image input
\ap\pa
            Wave_Out = wavelet transform. The wavelet transform of 
                      a 1D signal is an image. 

\ap\pa
            Num_Trans = wavelet transform chosen. 6 transform are possible:
                      1: french hat\\
                      2: mexican hat\\
                      3: a trous algorithm with a linear scaling function\\
                      4: a trous algorithm with a B1-spline scaling function\\
                      5: a trous algorithm with a B3-spline scaling function\\
                      6: Morlet's transform\\
\\
                      In the case of Morlet's transform, the wavelet transform
                      is complex. The modulus of the transform is stored in the
                      first part of the output image, and the phase in the 
                      second part. Default value is 2.
\ap\pa
            Num_Line= line number in the input image which will be used.
                      1 is the default value
\ap\pa
            Channel = number of channel per octave; default value is 12.
                      This parameter is not used in the a-trous algorithm
                      because a-trous algorithm is a diadic algorithm (1 
                      channel per octave)
\ap\pa
            Nu =      Morlet's parameter; only used with Morlet's transform
                
\ap\see
See also: 
            REC1D/WAVE
\ees\no
Note:       
            The number of scales of the tranform is automatically 
            calculated. The number of lines of the output file 
            gives the number of scales. A particular case is  
            Morlet's transform where the number of scales is equal
            to the half of number of the output file lines number.
\\
            Since the result is an image, MIDAS visualisation tools
            can be used for the visualisatio, plot, ...
\on\exs
Examples:
\ex
            TRAN1D/WAVE  data transform
            computes the one-dimensional wavelet transform of the 
            first line of the image "data" and stores the result in
            image "transform". The wavelet function is the mexican hat.
\xe\sxe


