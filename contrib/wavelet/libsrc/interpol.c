/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  interpol.c
**
.VERSION
 090804		last modif

*******************************************************************************
**
**    DESCRIPTION  plane interpolation routines
**    -----------  
**                 
******************************************************************************
**
** wavelet_interpol_plan_file (File_Name_Imag, File_Name_Transform, 
**                            Num_Plan, Num_Plan_Up)
**
** Interpolates from a wavelet transform, which is in the file of name
** File_Name_Transform, a plane of resolution number Num_Plan to a plane 
** of resolution number Num_Plan_Up . The interpolated plane is saved in  
** a image of name File_Name_Imag
** we need  0 < Num_Plan_Up < Num_Plan <= Nbr_Plan
** 
** File_Name_Imag = output file name of the image
** File_Name_Transform = input file name of the wavelet transform
** Num_Plan = plane number to interpolates (Num_Plan = 1 .. Number_of_Planes)
** Num_Plan_up = interpolated plane number (Num_Plan = 1 .. Num_Plan)
**
******************************************************************************
**
** wavelet_interpol_plan (Wavelet, Imag, Nl, Nc, Num_Plan, Num_Plan_Up)
** float **Imag;
** wave_transf_des *Wavelet;
** int *Nl, *Nc, Num_Plan, Num_Plan_Up;
** 
** Interpolates from a wavelet transform a plane of resolution number
**  Num_Plan to a plane of resolution number Num_Plan_Up 
** 
** we need  0 < Num_Plan_Up < Num_Plan <= Nbr_Plan
** output: Imag = Wavelet.Plan[Num_Plan] interpolated to Num_Plan_Up 
** 
** Wavelet = INPUT: wavelet transform
** Imag = OUTPUT: interpolated plane
** Nl = OUTPUT: ptr to the number of lines of Imag
** Nc = OUTPUT: ptr to the number of columns of Imag
** Num_Plan = plane number to interpolated (Num_Plan = 1 .. Number_of_Planes)
** Num_Plan_up = interpolated plane number (Num_Plan = 1 .. Num_Plan)
** 
** The Mallat's wavelet transform is not treated by this routine 
** 
******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "Def_Math.h"
#include "Def_Mem.h"
#include "Def_Wavelet.h"

extern void wave_io_read(), wave_io_free(), wavelet_interpol_plan();
extern void io_err_message_exit(), io_write_pict_f_to_file();
extern void pyr_2d_interp_plan(), pyr_2d_cf_interp_plan();



/*****************************************************************************/

void wavelet_interpol_plan_file (File_Name_Imag, File_Name_Transform, 
                            Num_Plan, Num_Plan_Up)
char *File_Name_Imag, *File_Name_Transform;
int Num_Plan, Num_Plan_Up;
{
    float *Imag;
    wave_transf_des Wavelet;
    int Nl, Nc;

    /* read the wavelet file */
    wave_io_read (File_Name_Transform, &Wavelet);

    if (Wavelet.Type_Wave_Transform == TO_MALLAT_BARLAUD)
    {
        io_err_message_exit (ERR_NOT_IMPLEMENTED, " ");
    }
    
    /* interpolation */
    wavelet_interpol_plan (&Wavelet,&Imag,&Nl, &Nc, Num_Plan, Num_Plan_Up);

    /* we save the result */
    io_write_pict_f_to_file (File_Name_Imag, Imag, Nl, Nc);

    wave_io_free (&Wavelet);
    free ((char *) Imag);
}

/*****************************************************************************/

void wavelet_interpol_plan (Wavelet, Imag, Nl, Nc, Num_Plan, Num_Plan_Up)
float **Imag;
wave_transf_des *Wavelet;
int *Nl, *Nc, Num_Plan, Num_Plan_Up;
{
    float *Ptr;
    int i, Nbr_Plan;
    string Mes_Memory;

    /* number of planes */
    Nbr_Plan = Wavelet->Nbr_Plan;

    /* Test the plane number Num_Plan */
    if ((Num_Plan <= 0) || (Num_Plan > Nbr_Plan))
    {
        /* The plane number Num_Plan is false */
        sprintf (Mes_Memory,", Number of planes = %d\n", Nbr_Plan);
        io_err_message_exit (ERR_PLANE_NUMBER, Mes_Memory);
    }

    /* Test the plane number Num_Plan_Up */
    if ((Num_Plan_Up >= Num_Plan) || (Num_Plan_Up <= 0))
    {
        /* The plane number Num_Plan_Up is false */
        sprintf (Mes_Memory,"interpolated plane number must be in 1 .. %d\n",
                 Num_Plan-1);
        io_err_message_exit (ERR_INTERP_PLANE_NUMBER, Mes_Memory);
    }
    
    switch (Wavelet->Type_Wave_Transform)
    {
        case TO_PAVE_LINEAR:
        case TO_PAVE_BSPLINE:
        case TO_PAVE_BSPLINE_FFT:
                   *Nl = Wavelet->Nbr_Ligne;
                   *Nc = Wavelet->Nbr_Col;
                   Ptr = Wavelet->Pave.Data + (Num_Plan - 1) * *Nl * *Nc;
                   *Imag = f_vector_alloc (*Nl * *Nc);
                   for (i = 0; i < *Nl * *Nc; i++) (*Imag)[i] = Ptr[i];
                   break;
        case TO_PYR_LINEAR:
        case TO_PYR_BSPLINE:
                   *Nl = (Wavelet->Pyramid.Tab_Nl) [Num_Plan_Up - 1];
                   *Nc = (Wavelet->Pyramid.Tab_Col)[Num_Plan_Up - 1];
                   *Imag = f_vector_alloc (*Nl * *Nc);
                    pyr_2d_interp_plan (Wavelet->Pyramid.Data, 
                                        *Imag, 
                                        Wavelet->Pyramid.Tab_Nl, 
                                        Wavelet->Pyramid.Tab_Col, 
                                        Wavelet->Pyramid.Tab_Pos, 
                                        Num_Plan-1, Num_Plan_Up-1);
                   break;
        case TO_PYR_FFT_DIFF_RESOL:
        case TO_PYR_FFT_DIFF_SQUARE_RESOL:
                   *Nl = (Wavelet->Pyramid.Tab_Nl) [Num_Plan_Up - 1];
                   *Nc = (Wavelet->Pyramid.Tab_Col)[Num_Plan_Up - 1];
                   *Imag = f_vector_alloc (*Nl * *Nc);
                   pyr_2d_cf_interp_plan (Wavelet->Pyramid.Data, 
                                          *Imag, 
                                          Wavelet->Pyramid.Tab_Nl, 
                                          Wavelet->Pyramid.Tab_Col, 
                                          Wavelet->Pyramid.Tab_Pos, 
                                          Num_Plan-1, Num_Plan_Up-1);
                   break;
        case TO_MALLAT_BARLAUD:
                   io_err_message_exit (ERR_NOT_IMPLEMENTED, " ");
                   break;
        default:
                   io_err_message_exit (ERR_TRANSF, " ");
                   break;
    }   
}



    
