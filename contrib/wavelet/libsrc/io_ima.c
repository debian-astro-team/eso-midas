/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  io_ima.c
**
.VERSION
 090804		last modif

*******************************************************************************
**
**    DECRIPTION This module contains input-output routines
**    ---------- 
******************************************************************************
**
** io_read_file_to_pict_f (File_Name, Imag, Nl, Nc)
** float **Imag
** char *File_Step_Name
** int *Nl, *Nc
**
** Reads in a file of name File_Name an image of floats
** Allocates the memory 
** INPUT File_Name
** OUTPUT *Imag is a pointer to a an image of float
**        *Nc is the number of column
**        *Nc is the number of lines
**
******************************************************************************
**
** io_read_file_to_pict_cf (File_Name, Imag, Nl, Nc)
** complex_float **Imag
** char *File_Step_Name
** Int *Nl, *Nc
**
** Reads in a file of name File_Name an image  of complex_float
** INPUT File_Name
** OUTPUT *Imag is a pointer to a an image of complex_float
**        *Nc is the number of column
**        *Nc is the number of lines
**
******************************************************************************
**
** io_write_pict_f_to_file (File_Step_Name, Imag, Nl, Nc)
** float *Imag
** char *File_Step_Name
** Int Nl, Nc
**
** Write an image of float on the disk whith a file name File_Name_Step
** INPUT Image = pointer to an image of float
**       Nl = number of lines
**       Nc = Number of columns
** OUTPUT a file is created
** 
******************************************************************************
**
** io_write_pict_cf_to_file (File_Step_Name, Imag, Nl, Nc)
** char *File_Step_Name
** complex_float *Imag
** int Nl, Nc
** 
** Write an image of complex_float on the disk whith a file name File_Name_Step
** INPUT Image = pointer to an image of complex_float
**       Nl = number of lines
**       Nc = Number of columns
** OUTPUT a file is created
**
****************************************************************************** 
**
** io_write_real_part_to_file (File_Name_Out, Imag, Nl, Nc)
** complex_float *Imag;
** int Nl,Nc;
** char *File_Name_Out;
**
** Write the real part of an image of complex_float on the disk whith a
** file name File_Name_Step
** INPUT Image = pointer to an image of complex_float
**       Nl = number of lines
**       Nc = Number of columns
** OUTPUT a file is created
**
******************************************************************************
** 
** io_write_imag_part_to_file (File_Name_Out, Imag, Nl, Nc)
** complex_float *Imag;
** int Nl,Nc;
** char *File_Name_Out;
** 
** Write the imaginary part of an image of complex_float float on the 
** disk whith a file name File_Name_Step
** INPUT Image = pointer to an image of complex_float
**       Nl = number of lines
**       Nc = Number of columns
** OUTPUT a file is created
**
******************************************************************************/


/* error definitions */
#define NUMBER_ERROR 13
#define MAX_TAB NUMBER_ERROR+1

static char *Tab_Error_Mes[MAX_TAB] = {"not used",
                            "Error, unknown transform ",
                            "array size is not a power of two ",
                            "cannot read the data ",
                            "cannot write the data ",
                            "cannot open the file ",
                            "cannot close the file ",
                            "cannot allocate memory ",
                            "bad scale number ",
                            "the image must be square ",
                            "bad image size ",
                            "not implemented ",
                            "bad number of scales (too high) ",
                            "cannot interpolate plane "};


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <string.h>

#include<midas_def.h>

#include "Def_Math.h"
#include "Def_Mem.h"


#define DEBUG TRUE

/***** Definition of the Midas image structure  */

#define SIZE_PIC_CUNIT 63      /* Max of the comment size on the axes */
#define SIZE_PIC_INDENT 71     /* Max of the comment size on the image */
#define SIZE_PIC_TAB_NAXIS 1   /* Dimensions array size */
#define SIZE_PIC_TAB_NPIX 2    /* Array size for the dimensions on each axis */
#define SIZE_PIC_START 2       /* Origine coordinates array size */
#define SIZE_PIC_STEP 2        /* Step array size  */
#define SIZE_PIC_TAB_CUTS 4    /* Cuts array size  */

typedef struct  {
        float *Rbuf;
           /* Buffer where the data are ranged */
        float Cuts[SIZE_PIC_TAB_CUTS];
           /* Cuts [0] ... Cuts [1] for the visualisation 
              Cuts [2] ... Cuts [3] = min ... max of the image
           */
        char Cunit[SIZE_PIC_CUNIT];
           /* Axis comments  */
        char Ident[SIZE_PIC_INDENT];
           /* Image comments */
        int Naxis [SIZE_PIC_TAB_NAXIS];
           /* Dimension Number */
        int Npix [SIZE_PIC_TAB_NPIX];
           /* Npix [0] = pixels number on x
              Npix [1] = pixels number on y  */
        double Start [SIZE_PIC_START];
           /* Origine coordinates x = Start [0], y = Start */
        double Step [SIZE_PIC_STEP];
           /* Step on the axis */
                 } midas_pic_des;


void io_write_pict_f_to_file(),io_write_real_part_to_file();
void io_write_imag_part_to_file();


/***************************************************************/

void io_err_message_exit (Num_Err, Mes)
int Num_Err;
char *Mes;
{
    int Errno = Num_Err;
    string Mes_Send;

    strcpy(Mes_Send, Tab_Error_Mes[Num_Err]);
    strcat(Mes_Send, Mes);
  
    SCTPUT("\nERROR");
#if DEBUG
    printf ("Num_Err = %d, Tab_Err = %s, Mes = %s\n",Num_Err,
             Tab_Error_Mes[Num_Err], Mes);  
    printf ("Mes_Send = %s\n", Mes_Send); 
#endif
    SCETER(Errno, Mes_Send);
}

/****************************************************************************/

void io_read_file_to_pict_f (File_Name, Imag, Nl, Nc)
float **Imag;
char *File_Name;
int *Nl, *Nc;
{
    int Stat,n;
    int Size;
    char *mypntr;
    int Imnoa;
    float *Ptr_Bis, *Ptr;
    midas_pic_des Des_Pic;

    Stat = SCIGET(File_Name,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,2,
                  Des_Pic.Naxis,Des_Pic.Npix,Des_Pic.Start,
/*                  Des_Pic.Step,Des_Pic.Ident,Des_Pic.Cunit, */
/* CG. 03.11.94: Des_Pic.Ident and Des_Pic.Cunit not initiated */
                  Des_Pic.Step,"","",
                  &mypntr, &Imnoa);

    if (Stat != 0) io_err_message_exit (ERR_OPEN_FILE, File_Name);

    Size = Des_Pic.Npix [0] * Des_Pic.Npix [1];
    *Imag = f_vector_alloc (Size);

    Ptr_Bis = (float *) mypntr;
    Ptr = *Imag;
    for (n = 0; n < Size; n++) Ptr[n]= Ptr_Bis [n];
    *Nl = Des_Pic.Npix[1];
    *Nc = Des_Pic.Npix[0];
    Stat = SCFCLO(Imnoa);
    if (Stat != 0) io_err_message_exit (ERR_CLOSE_FILE, File_Name);
}

/****************************************************************/

void io_read_file_to_pict_cf (File_Step_Name, Imag, Nl, Nc)
complex_float **Imag;
char *File_Step_Name;
int *Nl, *Nc;
{
    int Size,i;
    string File_Name;
    float *Pict;
    complex_float *Ptr;

    /* read the real part  of the image  */
    strcpy (File_Name, File_Step_Name);
    strcat (File_Name, "_re");
    io_read_file_to_pict_f (File_Name, &Pict, Nl, Nc);

    Size = *Nl * *Nc;
    *Imag = cf_vector_alloc (Size);
    Ptr = *Imag;
    for (i = 0; i < Size; i++)
    {
        Ptr[i].re = Pict[i];
    }
    free ((char *) Pict);

    /* read the imaginary part of the image  */
    strcpy (File_Name, File_Step_Name);
    strcat (File_Name, "_im");
    io_read_file_to_pict_f (File_Name, &Pict, Nl, Nc);
    for (i = 0; i < Size; i++)
    {
        Ptr[i].im = Pict[i];
    }
    free ((char *) Pict);
}
/****************************************************************/

static void io_init_descriptor (Des_Pic, Nl, Nc)
midas_pic_des *Des_Pic;
int Nl, Nc;
{
    sprintf (Des_Pic->Ident, " image  %d * %d",Nl,Nc);
    strcpy (Des_Pic->Cunit, " x virtual        y virtual");
    Des_Pic->Cuts [0] = 0.;
    Des_Pic->Cuts [1] = 0.;
    Des_Pic->Cuts [2] = 0.;
    Des_Pic->Cuts [3] = 0.;
    Des_Pic->Naxis [0] = 2;
    Des_Pic->Npix [1] = Nl;
    Des_Pic->Npix [0] = Nc;
    Des_Pic->Step [0] = 1;
    Des_Pic->Step [1] = 1;
    Des_Pic->Start [0] = 1.;
    Des_Pic->Start [1] = 1.;
}

/****************************************************************************/

void io_write_pict_cf_to_file (File_Step_Name, Imag, Nl, Nc)
char *File_Step_Name;
complex_float *Imag;
int Nl, Nc;
{
    char File_Name [STRING_SIZE];
    int i,Length;
    float *Pict;

    strcpy (File_Name, File_Step_Name);
    strcat (File_Name, "_re");
    Length = Nl * Nc;
    Pict = f_vector_alloc (Length);
    for (i = 0; i < Length; i++)
    {
        Pict[i] = Imag [i].re;
    }
    io_write_pict_f_to_file (File_Name, Pict, Nl, Nc);
    strcpy (File_Name, File_Step_Name);
    strcat (File_Name, "_im");
    for (i = 0; i < Length; i++)
    {
        Pict[i] = Imag [i].im;
    }
    io_write_pict_f_to_file (File_Name, Pict, Nl, Nc);
    free ((char *) Pict);
}

/****************************************************************/

void io_write_real_part_to_file (File_Name_Out, Imag, Nl, Nc)
complex_float *Imag;
int Nl,Nc;
char *File_Name_Out;
{
    float *Pict_f;
    int i;

    Pict_f = f_vector_alloc (Nl*Nc);
    for (i = 0; i < Nl*Nc; i++)  Pict_f [i] = Imag [i].re;
    io_write_pict_f_to_file (File_Name_Out, Pict_f, Nl, Nc);
    free ((char *) Pict_f);
}

/****************************************************************/

void io_write_imag_part_to_file (File_Name_Out, Imag, Nl, Nc)
complex_float *Imag;
int Nl,Nc;
char *File_Name_Out;
{
    float *Pict_f;
    int i;

    Pict_f = f_vector_alloc (Nl*Nc);
    for (i = 0; i < Nl*Nc; i++)  Pict_f [i] = Imag [i].im;
    io_write_pict_f_to_file (File_Name_Out, Pict_f, Nl, Nc);
    free ((char *) Pict_f);
}

/****************************************************************************/

void io_write_pict_f_to_file (File_Name, Imag, Nl, Nc)
float *Imag;
char *File_Name;
int Nl, Nc;
{
    int Imnob, Size;
    int Stat;
    float *Ptr;
    int i;
    midas_pic_des Des_Pic;
    float min,max;
    int unit;

    max = -WAVELET_INFINITY;
    min = WAVELET_INFINITY;

    io_init_descriptor (&Des_Pic, Nl, Nc);
    Des_Pic.Rbuf = Imag;

    Stat = SCIPUT (File_Name, D_R4_FORMAT, F_O_MODE, F_IMA_TYPE,
            Des_Pic.Naxis[0], Des_Pic.Npix, Des_Pic.Start, Des_Pic.Step,
            Des_Pic.Ident, Des_Pic.Cunit, (char **)&Ptr, &Imnob);

    if (Stat != 0) io_err_message_exit (ERR_OPEN_FILE, File_Name);

    Size = Des_Pic.Npix [0] * Des_Pic.Npix [1];
    for (i = 0; i < Size; i++)
    {
        Ptr [i] = Des_Pic.Rbuf [i];
        max = MAX(Des_Pic.Rbuf[i],max);
        min = MIN(Des_Pic.Rbuf[i],min);
    }
    Des_Pic.Cuts [0] = 0.;
    Des_Pic.Cuts [1] = 0.;
    Des_Pic.Cuts [2] = min;
    Des_Pic.Cuts [3] = max;

    Stat = SCDWRR(Imnob,"LHCUTS", Des_Pic.Cuts,1,4,&unit);
    if (Stat != 0) io_err_message_exit (ERR_CLOSE_FILE, File_Name);

    Stat = SCFCLO(Imnob);
    if (Stat != 0) io_err_message_exit (ERR_CLOSE_FILE, File_Name);
}

/****************************************************************************/

