/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
.VERSION
 090904		last modif
===========================================================================*/

/******************************************************************************
**           Copyright (C) 1993 by European Southern Observatory
*******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  b_spline.c
**
*****************************************************************************
**
** double lib_math_b3_spline (x)
** double x;
** 
** Computes the value on a b3-spline 
** we have b3_spline (0) = 2 / 3 
** and if |x| >= 2 => b3_spline (x) = 0
** 
*****************************************************************************/
 

#include <stdio.h>
#include <math.h>

/***************************************************************************/

double lib_math_b3_spline (x)
double x;
{
    double A1,A2,A3,A4,A5,Val;
    
    A1 = fabs ((x - 2) * (x - 2) * (x - 2));
    A2 = fabs ((x - 1) * (x - 1) * (x - 1));
    A3 = fabs (x * x * x);
    A4 = fabs ((x + 1) * (x + 1) * (x + 1));
    A5 = fabs ((x + 2) * (x + 2) * (x + 2));
    Val = 1./12. * (A1 - 4. * A2 + 6. * A3 - 4. * A4 + A5);
    return (Val);
}
    
/***************************************************************************/
