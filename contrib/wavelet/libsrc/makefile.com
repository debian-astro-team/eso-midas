$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.WAVELET.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:51 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libwave2d fft_pow2.obj,b_spline.obj,oper_ima.obj,io_ima.obj,mem_alloc.obj,mem_free.obj,mallat.obj,pave.obj,pave_cf.obj,pyr.obj,pyr_cf.obj
$ LIB/REPLACE libwave2d filter_cf.obj,entr_plan.obj,extr_plan.obj,io_wave.obj,interpol.obj,reconstr.obj,transform.obj,filtering.obj,wa_deconv.obj
$ LIB/REPLACE libwave2d fft1d.obj,wave1d.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
