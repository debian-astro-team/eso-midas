/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  mem_alloc.c
.VERSION
 090804		last modif

**
*******************************************************************************
**
**    DECRIPTION  This module contains allocation procedures
**    ---------- 
**
******************************************************************************
**
** double *d_vector_alloc (Nbr_Elem)
** int  Nbr_Elem
**
** Allocates an array of Nbr_Elem doubles
** ex: double *Tab;
**     Tab = d_vector_alloc(10);
**     
**
******************************************************************************
**
** float *f_vector_alloc (Nbr_Elem) 
** int  Nbr_Elem
**
** Allocates an array of Nbr_Elem floats
** ex: float *Tab;
**     Tab = f_vector_alloc(10);
**
******************************************************************************
**
** int *i_vector_alloc (Nbr_Elem) 
** int  Nbr_Elem
**
** Allocates an array of Nbr_Elem integers
** ex: int *Tab;
**     Tab = i_vector_alloc(10);
**
******************************************************************************
**
** int  **i_matrix_alloc(nbr_lin,nbr_col)
** int  nbr_lin, nbr_col;
**
** Allocate a matrix of nbr_lin lines and nbr_col columns of integer
** ex: int **Tab;
**     Tab = i_matrix_alloc(10,20);
**
******************************************************************************
**
** complex_float *cf_vector_alloc(Nbr_Elem)
** int  Nbr_Elem
** 
** Allocates an array of Nbr_Elem complex floats
** ex: complex_float *Tab;
**     Tab = cf_vector_alloc(10);
**
******************************************************************************
**
** float  **f_matrix_alloc(nbr_lin,nbr_col)
** int    nbr_lin, nbr_col;
**
** Allocate a matrix of nbr_lin lines and nbr_col columns of floats
** ex: float **Tab;
**     Tab = f_matrix_alloc(10,20);
**
******************************************************************************
**
** float  **cf_matrix_alloc(nbr_lin,nbr_col)
** int    nbr_lin, nbr_col;
**
** Allocate a matrix of nbr_lin lines and nbr_col columns of complex floats
** ex: complex_float **Tab;
**     Tab = cf_matrix_alloc(10,20);
**
*****************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "Def_Math.h"
#include "Def_Mem.h"

extern void io_err_message_exit();


/****************************************************************************/

static void memory_abort ()
{
    io_err_message_exit (ERR_ALLOC_MEMO, " ");
}

/****************************************************************************/

double *d_vector_alloc(Nbr_Elem)
/* allocates a vector of doubles  */
int  Nbr_Elem;
{
    double *Vector;

    Vector = (double*) calloc ((unsigned) Nbr_Elem * sizeof (double),1);
    if (Vector == NULL) memory_abort();
    return(Vector);
}

/****************************************************************************/

float *f_vector_alloc(Nbr_Elem)
/* allocates a vector of float */
int  Nbr_Elem;
{
    float *Vector;

    Vector = (float*) calloc ((unsigned)Nbr_Elem * sizeof (float),1);
    if (Vector == NULL) memory_abort();
    return(Vector);
}

/****************************************************************************/

int *i_vector_alloc(Nbr_Elem)
/* allocates a vector of integer */
int  Nbr_Elem;
{
    int *Vector;

    Vector = (int*) calloc ((unsigned) Nbr_Elem * sizeof (int),1);
    if (Vector == NULL) memory_abort();
    return(Vector);
}

/****************************************************************************/

complex_float *cf_vector_alloc(Nbr_Elem)
/* allocates a vector of complex float */
int  Nbr_Elem;
{
    complex_float *Vector;

    Vector= (complex_float*) calloc((unsigned)Nbr_Elem * sizeof(complex_float),1);
    if (Vector == NULL) memory_abort();
    return(Vector);
}

/****************************************************************************/

int **i_matrix_alloc(nbr_lin,nbr_col)
int nbr_lin, nbr_col;
/* allocates a matrix of integer */
{
   auto int  **matrix;
   register int i, j;

    matrix = (int **) calloc((unsigned) nbr_lin*sizeof(int *),1);
    if (matrix == NULL) memory_abort();

    for (i=0; i<nbr_lin; i++)
    {
       matrix[i] = (int *) calloc((unsigned) nbr_col*sizeof(int),1);
       if (matrix[i] == NULL) memory_abort();
    }
 
    for (i=0; i<nbr_lin; i++)
    for (j=0; j<nbr_col; j++)  matrix[i][j] = 0;

    return(matrix);
}

/****************************************************************************/

float  **f_matrix_alloc(nbr_lin,nbr_col)
int    nbr_lin, nbr_col;
/* allocates a matrix of float */
{
   auto float **matrix;
   register int i;

   matrix = (float **) calloc((unsigned) nbr_lin*sizeof(float *),1);
   if (matrix == NULL) memory_abort();

   for (i=0; i<nbr_lin; i++)
   {
      matrix[i] = (float *) calloc((unsigned) nbr_col*sizeof(float),1);
      if (matrix[i] == NULL) memory_abort();
   }

   return(matrix);
}

/****************************************************************************/

complex_float  **cf_matrix_alloc(nbr_lin,nbr_col)
int    nbr_lin, nbr_col;
{
    complex_float  **matrix;
    register int i;

    matrix = (complex_float **)
                  calloc((unsigned) nbr_lin*sizeof(complex_float *),1);
    if (matrix == NULL) memory_abort();

    for (i=0; i< nbr_lin; i++)
    {
        matrix[i] = (complex_float *)
                         calloc((unsigned) nbr_col*sizeof(complex_float),1);
        if (matrix[i] == NULL) memory_abort();

    }
    return(matrix);
}

/****************************************************************************/
