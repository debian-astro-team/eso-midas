/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/******************************************************************************
**
**    UNIT
**
**    Version: 19.1
**
**    Author: Jean-Luc Starck
**
**    Date:  03/02/25
**    
**    File:  mem_free.c
**
.VERSION
 090804

*******************************************************************************
**
**    DECRIPTION This module contains the procedures which deallocate
**    ---------- the matrix
**
******************************************************************************
**
** void   i_matrix_free(matrix,nbr_lin)
** int    **matrix;
** int    nbr_lin;
**
** deallocates a matrix of integers
** 
******************************************************************************
**
** void   f_matrix_free(matrix,nbr_lin)
** float  **matrix;
** int    nbr_lin;
**
** deallocates a matrix of float
** 
******************************************************************************
**
** void   cf_matrix_free(matrix,nbr_lin)
** complex_float  **matrix;
** int    nbr_lin;
**
** deallocates a matrix of complex_float
** 
*****************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Def_Math.h"


/**********************************************************/

void   i_matrix_free(matrix,nbr_lin)
int    **matrix;
int    nbr_lin;
{
    register  int    i;

    for (i=0; i<nbr_lin; i++)  free((char *) matrix[i]);
    free ((char *) matrix);
}

/**********************************************************/

void   f_matrix_free(matrix,nbr_lin)
float  **matrix;
int    nbr_lin;
{
    register  int    i;

    for (i=0; i<nbr_lin; i++)  free((char *) matrix[i]);
    free ((char *) matrix);
}

/**********************************************************/

void   cf_matrix_free(matrix,nbr_lin)
complex_float  **matrix;
int    nbr_lin;
{
   register  int    i;

   for (i=0; i<nbr_lin; i++)  free((char *) matrix[i]);
   free ((char *) matrix);
}

/**********************************************************/

