! @(#)wa_synt.prg	19.1 (ES0-DMD) 02/25/03 13:34:57
! @(#) @(#)wa_synt.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_synt.prg
!.PURPOSE:    visualisation of a wavelet file in a cube
!.USE:        VISUAL/SYNT Wavelet [imag_out] [Disp] [Disp_Number]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? C "Enter wavelet file name:  "
DEFINE/PARAM P2 file_visu C "Enter the output file name:  "
DEFINE/PARAM P3 Y C "Display the result (Y or N)"
DEFINE/PARAM P4 1 N "Display number:"

set/format I1
!set/format f16.9

! parameter definition
define/local I/i/1/1 0
define/local Nl/i/1/1 0
define/local Nc/i/1/1 0
define/local Nscale/i/1/1 0
define/local Typ_Wave/C/1/60
define/local n_plan/C/1/60 
define/local n_tem/C/1/60 "xxxyyy"
define/local posx/i/1/1 100
define/local posy/i/1/1 300
define/local sizex/i/1/1 0
define/local sizey/i/1/1 0
define/local disp/i/1/1 0

@c wa_head {P1}
WRITE/KEYW Nl {OUTPUTI(1)}
WRITE/KEYW Nc {OUTPUTI(2)}
WRITE/KEYW Nscale {OUTPUTI(3)}
WRITE/KEYW Typ_Wave "{OUT_A(1:3)}"

IF Typ_Wave(1:3) .EQ. "CUB" THEN
      WRITE/KEYW IN_A/C/1/60 {P1}
      WRITE/KEYW OUT_A/C/1/60 {P2}
      RUN CON_EXE:wa_visu.exe
      IF P3(1:1) .EQ. "Y" THEN
         compute/keyw sizex = {{P2},npix(1)}
         compute/keyw sizey = {{P2},npix(2)}
         Nc = sizex
         Nl = sizey
         IF sizex .GT. 512 sizex = 512
         IF sizey .GT. 512 sizey = 512
         create/display {P4} {sizex},{sizey},{posx},{posy} 1,{NC},{NL}
         load/image {P2}
         load/lut isophot
      ENDIF
ELSEIF  Typ_Wave(1:3) .EQ. "PYR" THEN
      sizex = 2*NL
      sizey = 2*NL
      create/image {P2} 2,{sizex},{sizey}
      posx = 1
      posy = 1
      DO I = 1 {nscale}
        write/keyw n_plan "scale_{I}"
        @c wa_extr {P1} {n_plan} {I}
        stat/image {n_plan}
        write/out "Max = {outputr(2)}"
        compute/image {n_tem} = {n_plan}/{outputr(2)}
        compute/keyw Nc = {{n_plan},npix(1)}
        compute/keyw Nl = {{n_plan},npix(2)}
        insert/image {n_tem} {P2} @{posx},@{posy}
        posx = posx + NC
        posy = posy + NL
        delete/image {n_plan} n
        delete/image {n_tem} n
      ENDDO
      IF P3(1:1) .EQ. "Y" THEN
        NL = sizey
        NC = sizex
        IF sizex .GT. 512 sizex = 512
        IF sizey .GT. 512 sizey = 512
        create/display {P4} {sizex},{sizey},{posx},{posy} 1,{NC},{NL}
        load/image {P2} cuts=0,0.8
        load/lut backgr
      ENDIF
ELSEIF Typ_Wave(1:3) .EQ. "IMA" THEN
      WRITE/KEYW IN_A/C/1/60 {P1}
      WRITE/KEYW OUT_A/C/1/60 {P2}
      RUN CON_EXE:wa_visu.exe
      IF P3(1:1) .EQ. "Y" THEN
         compute/keyw sizex = {{P2},npix(1)}
         compute/keyw sizey = {{P2},npix(2)}
         Nc = sizex
         Nl = sizey
         IF sizex .GT. 512 sizex = 512
         IF sizey .GT. 512 sizey = 512
         create/display {P4} {sizex},{sizey},{posx},{posy} 1,{NC},{NL}
         load/image {P2}
      ENDIF
ELSE 
       write/out "Error: bad wavelet transform"
ENDIF

