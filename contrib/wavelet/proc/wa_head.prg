! @(#)wa_head.prg	19.1 (ES0-DMD) 02/25/03 13:34:56
! @(#) @(#)wa_head.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_head.prg
!.PURPOSE:    gives information about a wavelet file
!.USE:        HEADER/WAVE Wavelet
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? C  "Enter wavelet file name:  "

WRITE/KEYW IN_A/C/1/60 {P1}

RUN CON_EXE:wa_head.exe
