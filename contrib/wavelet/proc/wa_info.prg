! @(#)wa_info.prg	19.1 (ES0-DMD) 02/25/03 13:34:56
! @(#) @(#)wa_info.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_info.prg
!.PURPOSE:    gives information about a wavelet file
!.USE:        INFO/WAVE Wavelet
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? IMAGE "Enter wavelet file name:  "

WRITE/KEYW IN_A/C/1/60 {P1}

RUN CON_EXE:wa_info.exe
