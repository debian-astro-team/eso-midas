! @(#)wa_cittert.prg	19.1 (ES0-DMD) 02/25/03 13:34:55
! @(#) @(#)wa_cittert.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_cittert.prg
!.PURPOSE:    deconvolve  an image with a regularization in the wavelet space
!             by the van-cittert method
!.USE:        CITTERT/WAVE Image_In Psf Imag_Out [residual] 
!              [Nb_Scales, Max_Iter] [N_Sigma, Noise] [Epsilon] [Fwmh]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis, restoration
!----------------------------------------------------------------------

DEFINE/PARAM P1 ? IMAGE "Enter Image:  "
DEFINE/PARAM P2 ? IMAGE "Enter Psf:  "
DEFINE/PARAM P3 ? IMAGE "Enter Result:  " 
DEFINE/PARAM P4 "residual" IMAGE "Enter Residual:  " 
DEFINE/PARAM P5 4,100 N "Number of scales, maximun iteration number:" 
DEFINE/PARAM P6 4.,0 N "Enter N_sigma, Noise  " 
DEFINE/PARAM P7 0.001 N "Convergence parameter:"
DEFINE/PARAM P8 0. N "Full Width at Half Maximum:  " 

WRITE/KEYW IN_A/C/1/60 {P1}
WRITE/KEYW IN_B/C/1/60 {P2}
WRITE/KEYW OUT_A/C/1/60  {P3}
WRITE/KEYW OUT_B/C/1/60  {P4}

WRITE/KEYW INPUTI/I/1/2  {P5}


WRITE/KEYW INPUTR/R/1/2  {P6}
WRITE/KEYW INPUTR/R/3/1  {P7}
WRITE/KEYW INPUTR/R/4/1  {P8}

RUN CON_EXE:wa_cittert.exe













