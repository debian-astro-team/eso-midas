! @(#)wa_pers.prg	19.1 (ES0-DMD) 02/25/03 13:34:56
! @(#) @(#)wa_pers.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_pers.prg
!.PURPOSE:    visualisation of a wavelet file in perspective
!.USE:        VISUAL/PERS Wavelet
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? C "Enter wavelet file name:  "
DEFINE/PARAM P2 file_visu C "Output file name  "
DEFINE/PARAM P3 1 N "Display number:"
DEFINE/PARAM P4 CO C "Visu. mode (CO = colour or BW = Black and White)"
DEFINE/PARAM P5 1 N "Increment Value"
DEFINE/PARAM P6 5 N "Threshold Value"
DEFINE/PARAM P7 Y C "Display the result (Y or N)"

set/format I1
!set/format f16.9

! parameter definition
define/local I/i/1/1 0
define/local Nl/i/1/1 0
define/local Nc/i/1/1 0
define/local Nscale/i/1/1 0
define/local Num_Tran/i/1/1 0
define/local Typ_Wave/C/1/60 
define/local posx/i/1/1 100
define/local posy/i/1/1 300
define/local sizex/i/1/1 0
define/local sizey/i/1/1 0
define/local Wavelet/C/1/60

@c wa_head {P1}
WRITE/KEYW Nl {OUTPUTI(1)}
WRITE/KEYW Nc {OUTPUTI(2)}
WRITE/KEYW Nscale {OUTPUTI(3)}
WRITE/KEYW Num_Tran {OUTPUTI(4)}
WRITE/KEYW Wavelet {P1}
WRITE/KEYW Typ_Wave "{OUT_A(1:3)}"

IF Typ_Wave(1:3) .EQ. "CUB" .OR. Typ_Wave(1:3) .EQ. "PYR"  THEN

!        Visualisation of all the plane with a perspective view
      WRITE/KEYW IN_A/C/1/60 {P1}
      WRITE/KEYW OUT_A/C/1/60 {P2}
      WRITE/KEYW IN_B/C/1/60 {P4}
      WRITE/KEYW INPUTI/I/1/1 {P5}
      WRITE/KEYW INPUTR/R/1/1 {P6}
      RUN CON_EXE:wa_pers.exe
      
      IF P7(1:1) .EQ. "Y" THEN
         compute/keyw sizex = {{P2},npix(1)}
         compute/keyw sizey = {{P2},npix(2)}
         compute/keyw NC = {{P2},npix(1)}
         compute/keyw NL = {{P2},npix(2)}
         IF sizex .GT. 512 sizex = 512
         IF sizey .GT. 512 sizey = 512
         create/display {P3} {sizex},{sizey},{posx},{posy} 1,{NC},{NL}
         load/image {P2}
         IF P4(1:2) .EQ. "CO" load/lut heat
      ENDIF
ELSE
         write/out "Error: bad wavelet transform"
ENDIF

