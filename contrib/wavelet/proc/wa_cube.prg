! @(#)wa_cube.prg	19.1 (ES0-DMD) 02/25/03 13:34:55
! @(#) @(#)wa_cube.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_cube.prg
!.PURPOSE:    visualisation of a wavelet file in a cube
!.USE:        VISUAL/CUBE Wavelet
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? C "Enter wavelet file name:  "
DEFINE/PARAM P2 file_visu C "Output file name  "
DEFINE/PARAM P3 1 N "Display number:"
DEFINE/PARAM P4 CO C "Visu. mode (CO = Colour or BW = Black and White)"
DEFINE/PARAM P5 Y C "Display the result (Y or N)"

set/format I1
!set/format f16.9

! parameter definition
define/local I/i/1/1 0
define/local Nl/i/1/1 0
define/local Nc/i/1/1 0
define/local Nscale/i/1/1 0
define/local Typ_Wave/C/1/60
define/local n_plan/C/1/60 
define/local name_tem/C/1/60 "auxfile"

define/local sizex/i/1/1 0
define/local sizey/i/1/1 0

@c wa_head {P1}
WRITE/KEYW Nl {OUTPUTI(1)}
WRITE/KEYW Nc {OUTPUTI(2)}
WRITE/KEYW Nscale {OUTPUTI(3)}
WRITE/KEYW Typ_Wave "{OUT_A(1:3)}"

IF Typ_Wave(1:3) .EQ. "CUB" .OR. Typ_Wave(1:3) .EQ. "PYR"  THEN

!        Visualisation of all the planes in a cube

!        Creation of 3-Dimension images
      create/image {name_tem} 3,{Nl},{Nc},{Nscale}
!        Extracts all the planes of the wavelet transform
!        and inserts them in the cube 
     DO I = 1 {nscale}
        write/keyw n_plan "scale_{I}"
        @c wa_extr {P1} {n_plan} {I}
        insert/image {n_plan} {name_tem} <,<,@{I}
        delete/image {n_plan} n
     ENDDO
!       Creation of the perspective image
     @a perspec {name_tem} {P2} ? ? n
     delete/image {name_tem} n

      IF P5(1:1) .EQ. "Y" THEN
!         Create a display and load the image
        compute/keyw sizex = {{P2},npix(1)}
        compute/keyw sizey = {{P2},npix(2)}
        compute/keyw NC = {{P2},npix(1)}
        compute/keyw NL = {{P2},npix(2)}
        IF sizex .GT. 1000 sizex = 1000
        IF sizey .GT. 800 sizey = 800
        create/display {P3} {sizex},{sizey},630,630 1,{NC},{NL}
        load/image {P2}
        IF P4(1:2) .EQ. "CO" THEN
              load/lut pastel
              load/itt jigsaw
        ENDIF
      ENDIF
ELSE
         write/out "Error: bad wavelet transform"
ENDIF

