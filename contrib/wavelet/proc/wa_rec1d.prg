! @(#)wa_rec1d.prg	19.1 (ES0-DMD) 02/25/03 13:34:57
! @(#) @(#)wa_rec1d.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_rec1d.prg
!.PURPOSE:    reconstruct the signal from the wavelet transform
!.USE:        REC1D/WAVE Wave_in Im_out [Num_Trans] [Channel] [Nu0]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? IMAGE "Wavelet_in:  "
DEFINE/PARAM P2 ? IMAGE "Image_out:  " 
DEFINE/PARAM P3 2 N "Enter the algorithm number:  " 1,6
DEFINE/PARAM P4 12 N "Enter the number of channels:  "
DEFINE/PARAM P5 0.8 N "Enter Morlet's parameter:  "

WRITE/KEYW IN_A/C/1/60 {P1}
WRITE/KEYW OUT_A/C/1/60  {P2}
WRITE/KEYW INPUTI/I/1/1  {P3}
WRITE/KEYW INPUTI/I/3/1  {P4}
WRITE/KEYW INPUTR/R/1/1  {P5}

RUN CON_EXE:wa_rec1d.exe

