! @(#)wa_contour.prg	19.1 (ES0-DMD) 02/25/03 13:34:55
! @(#) @(#)wa_contour.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_contour.prg
!.PURPOSE:    visualisation of the contours of a wavelet file 
!.USE:        VISUAL/CONT Wavelet [Display] [Visu_Mode] [Cont_Level]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------


DEFINE/PARAM P1 ? C "Enter wavelet file name:  "
DEFINE/PARAM P2 0 N "Graphic number:"
DEFINE/PARAM P3 CO C "Visu. mode (CO = colour or BW = Black and White)"
DEFINE/PARAM P4 3 N "Contours level:"

set/format I1
!set/format f16.9

! parameter definition
define/local I/i/1/1 0
define/local Nl/i/1/1 0
define/local Nc/i/1/1 0
define/local Nscale/i/1/1 0
define/local Num_Tran/i/1/1 0
define/local Typ_Wave/C/1/60 
define/local Wavelet/C/1/60

define/local name_tem/C/1/60

define/local rxlab/r/1/1 0
define/local rylab/r/1/1 0
define/local sizegr/r/1/1 0
define/local rxlins/r/1/1 0
define/local rxline/r/1/1 0
define/local linesize/r/1/1 0 
define/local minm/r/1/1 0
define/local maxm/r/1/1 0
define/local stepy/r/1/1 0

define/local colour/i/1/1 1
define/local ltype/i/1/1 1
define/local level/r/1/1 0
define/local ksig/r/1/1 0

define/local sizex/r/1/1 640
define/local sizey/r/1/1 480

@c wa_head {P1}
WRITE/KEYW Nl {OUTPUTI(1)}
WRITE/KEYW Nc {OUTPUTI(2)}
WRITE/KEYW Nscale {OUTPUTI(3)}
WRITE/KEYW Num_Tran {OUTPUTI(4)}
WRITE/KEYW Wavelet {P1}
WRITE/KEYW Typ_Wave "{OUT_A(1:3)}"

COMPUTE/KEYW sizegr = {NC}
COMPUTE/KEYW linesize = {sizegr}/15
COMPUTE/KEYW rxlab = {NC}+ {sizegr}/5
COMPUTE/KEYW rxline = {rxlab}-{sizegr}/20
COMPUTE/KEYW rxlins = {rxline}-{linesize}
COMPUTE/KEYW stepy = {NL}*0.05

IF Typ_Wave(1:3) .EQ. "CUB" .OR. Typ_Wave(1:3) .EQ. "PYR"  THEN

!      Plot one contour per plane on a graphic window

        set/format f3.2
        set/format I1
        
        write/keyw ksig {P4}
        create/graph {P2} 800,600,100,400

        set/graph XFORMAT=d YFORMAT=d colour=1 pmode=1
        plot/axes 1,{Nc} 1,{Nl} -150,-150,20,20 "{Wavelet}: Contours at {ksig}*sigma" " " 
        Nscale = Nscale - 1
        maxm = 1
        DO I = 1 {nscale}
          write/keyw name_tem "scale_{I}"
          @c wa_extr {Wavelet} {name_tem} {I}
          IF Typ_Wave(1:3) .EQ. "PYR"  THEN
              write/descr {name_tem} start/d/1/2 1,1
              write/descr {name_tem} step/d/1/2 {maxm},{maxm}
          ENDIF
          compute/keyw NC = {{name_tem},npix(1)}
          compute/keyw NL = {{name_tem},npix(2)}
          IF I .EQ. 1 THEN
             stat/image {name_tem}
             compute/keyw level = outputr(4)*ksig
          ELSE
             level = level/4.
          ENDIF

          IF P3(1:2) .EQ. "CO" .AND. I .LT. 9 THEN 
               colour = I
          ELSE 
               colour = 1
          ENDIF 
          ltype = I
          IF ltype .GT. 6 ltype = 1
          set/graph colour={colour} ltype={ltype} 
          overplot/contour {name_tem} ? {level} LTYPE
          delete/image {name_tem} n 
          maxm = maxm * 2.
       ENDDO  
!         print legend
       set/graph pmode=0
       overplot/axes 1,10 1,10 -50,-150,175,20
       DO I = 1 {nscale}
          IF P3(1:2) .EQ. "CO" .AND. I .LT. 9 THEN 
               colour = I
          ELSE 
               colour = 1
          ENDIF 
          ltype = I
          IF ltype .GT. 6 ltype = 1
          
          set/graph colour={colour} ltype={ltype} 

          COMPUTE/KEYW rylab = 2+I*0.5
          OVERPLOT/LINE {I} 2,{rylab} 5,{rylab}
          LABEL/GRAPH "Scale {I}" 6,{rylab} ? 1.5 1
       ENDDO     
ELSE
         write/out "Error: bad wavelet transform"
ENDIF
set/graph 




