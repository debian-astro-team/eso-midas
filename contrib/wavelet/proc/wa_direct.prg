! @(#)wa_direct.prg	19.1 (ES0-DMD) 02/25/03 13:34:56
! @(#) @(#)wa_direct.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_direct.prg
!.PURPOSE:    deconvolve  an image with a regularization in the wavelet space
!             by the van-cittert method
!.USE:        DIRECT/WAVE Image_In Psf Imag_Out [Nb_Scales] [g1,g2,g3,...]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis, restoration
!----------------------------------------------------------------------

DEFINE/PARAM P1 ? IMAGE "Enter Image:  "
DEFINE/PARAM P2 ? IMAGE "Enter Psf:  "
DEFINE/PARAM P3 ? IMAGE "Enter Result:  " 
DEFINE/PARAM P4 4 N "Number of scales:" 
DEFINE/PARAM P5 ? N "Regularization parameters:  " 

WRITE/KEYW IN_A/C/1/60 {P1}
WRITE/KEYW IN_B/C/1/60 {P2}
WRITE/KEYW OUT_A/C/1/60  {P3}
WRITE/KEYW INPUTI/I/1/2 {P4} 

WRITE/KEYW INPUTR/R/1/10 0 ALL
WRITE/KEYW INPUTR/R/1/10  {P5}

RUN CON_EXE:wa_direct.exe













