! @(#)wa_cmpima.prg	19.1 (ES0-DMD) 02/25/03 13:34:55
! @(#) @(#)wa_cmpima.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_cmpima.prg
!.PURPOSE:  compare two images in the wavelet space
!.USE:      COMPAR/WAVE frame1 frame2 [Nbr_of_Scales] [N-Sigma]
!            [Correl_Tab] [Snr_Tab] [Display] [Init]
!.AUTHOR:   Jean-Luc Starck
!.VERSION:  03/02/25
! KEYWORDS: Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? IMAGE "frame 1:  "
DEFINE/PARAM P2 ? IMAGE "frame 2:  "
DEFINE/PARAM P3 3 N "Enter the number of scales: " 1,10
DEFINE/PARAM P4 0 N "N_Sigma :  " 0,10
DEFINE/PARAM P5 cmp_correl C "correlation table :  "
DEFINE/PARAM P6 cmp_snr C "signal to noise ratio table :  "
DEFINE/PARAM P7 Y C "Display the result (Y or N): "
DEFINE/PARAM P8 Y C "Init the files (Y or N): "

define/local exist/i/1/1 1
define/local nfile/c/1/60 

WRITE/KEYW IN_A/C/1/60 {P1}
WRITE/KEYW IN_B/C/1/60 {P2}
WRITE/KEYW INPUTI/I/1/1  {P3}
WRITE/KEYW INPUTR/R/1/1  {P4}
WRITE/KEYW OUT_A/C/1/60  {P5}
WRITE/KEYW OUT_B/C/1/60  {P6}

IF P8(1:1) .EQ. "Y" THEN
   compute/keyw nfile = "{P5}.tbl"
   compute/keyw exist = m$exist(nfile)
   IF exist .EQ. 1 delete/table {P5} no
   compute/keyw nfile = "{P6}.tbl"
   compute/keyw exist = m$exist(nfile)
   IF exist .EQ. 1 delete/table {P6} no
ENDIF

RUN CON_EXE:wa_comp.exe

IF P7(1:1) .EQ. "Y" THEN
    @c wa_pltcorr {P5}
    @c wa_pltsnr {P6}
ENDIF

