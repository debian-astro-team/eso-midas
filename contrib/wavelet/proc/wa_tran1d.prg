! @(#)wa_tran1d.prg	19.1 (ES0-DMD) 02/25/03 13:34:57
! @(#) @(#)wa_tran1d.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_tran1d.prg
!.PURPOSE:    computes a wavelet transform
!.USE:        TRAN1D/WAVE Im_In Wave_Out [Num_Trans] [Num_Line] [Channel] [Nu0]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? IMAGE "Enter source frame:  "
DEFINE/PARAM P2 ? IMAGE "Enter destination wavelet:  " 
DEFINE/PARAM P3 2 N "Enter the algorithm number:  " 1,8
DEFINE/PARAM P4 1  N "Enter the line number:  "
DEFINE/PARAM P5 12 N "Enter the number of channels:  "
DEFINE/PARAM P6 0.8 N "Enter Morlet's parameter:  "

WRITE/KEYW IN_A/C/1/60 {P1}
WRITE/KEYW OUT_A/C/1/60  {P2}
WRITE/KEYW INPUTI/I/1/1  {P3}
WRITE/KEYW INPUTI/I/2/1  {P4}
WRITE/KEYW INPUTI/I/3/1  {P5}
WRITE/KEYW INPUTR/R/1/1  {P6}

RUN CON_EXE:wa_tran1d.exe

copy/dd {P1} start/D/1/1 {P2}
copy/dd {P1} step/D/1/1 {P2}
