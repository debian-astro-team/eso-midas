! @(#)wa_pltsnr.prg	19.1 (ES0-DMD) 02/25/03 13:34:57
! @(#) @(#)wa_pltsnr.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_pltsnr.prg
!.PURPOSE:  Display a snr table
!.USE:      PLOT/SNR name_snr_table 
!.AUTHOR:   Jean-Luc Starck
!.VERSION:  03/02/25
! KEYWORDS: signal to noise ratio, wavelet
! ----------------------------------------------------------------------

! name of the table
DEFINE/PARAM P1 cmp_snr C "signal to noise ratio table:  "
WRITE/KEYW IN_A/C/1/60 {P1}


! parameter definition
define/local I/i/1/1 0
define/local J/i/1/1 0
define/local rxlab/r/1/1 0
define/local rylab/r/1/1 0
define/local sizegr/r/1/1 0
define/local rxlins/r/1/1 0
define/local rxline/r/1/1 0
define/local linesize/r/1/1 0
define/local minm/r/1/1 0
define/local maxm/r/1/1 0
define/local lastcol/r/1/1 0
define/local stepy/r/1/1 0

set/format I3
write/keyw namecol/C/1/16 " "

! unselects the fisrt line which are not wavelet
! information.
select/table {P1} seq.ne.1

! number of columns
COMPUTE/KEYW lastcol = {{P1}.tbl,TBLCONTR(3)}


! computes the min and the max 
stat/table {P1} #2
COMPUTE/KEYW MINM = OUTPUTR(1)
COMPUTE/KEYW MAXM = OUTPUTR(2)
IF {lastcol} .GT. 2 THEN
  DO I = 3 {lastcol}
    write/keyw namecol {{P1}.tbl,TLABL{I}}
    stat/table {P1} :{namecol}
    IF  {OUTPUTR(1)} .LT. {MINM} MINM = OUTPUTR(1)
    IF  {OUTPUTR(2)} .GT. {MAXM} MAXM = OUTPUTR(2)
  ENDDO
ENDIF
IF  {MINM} .GT. 0. MINM = 0
COMPUTE/KEYW MAXM = {MAXM}+1

! y-step for the text
COMPUTE/KEYW stepy = ({MAXM}-{MINM})*0.05

stat/table {P1} #1
COMPUTE/KEYW sizegr = OUTPUTR(2)
COMPUTE/KEYW rxlab = 3*{sizegr}/4
COMPUTE/KEYW linesize = {sizegr}/15
COMPUTE/KEYW rxline = {rxlab}-{sizegr}/30
COMPUTE/KEYW rxlins = {rxline}-{linesize}

! create the graphic
create/graph 2 600,350,100,100


!plot the axes
set/graph XFORMAT=d colour=1
plot/axes {outputr(1)},{outputr(2)} {MINM},{MAXM} ? "Scale" "SNR"

!plot the curves
DO I = 2 {lastcol}
    J = I-1
    write/keyw namecol {{P1}.tbl,TLABL{I}}
!     ltype = line,dots,... parameter
    set/graph ltype={J} stype=0 colour={J}
    OVERPLOT/TABLE {P1} #1 :{namecol}
    COMPUTE/KEYW rylab = {MINM}+J*{stepy}
    OVERPLOT/LINE {J} {rxlins},{rylab} {rxline},{rylab}
    LABEL/GRAPH {namecol} {rxlab},{rylab} ? ? 1
ENDDO

set/graph
select/table {P1} ALL




