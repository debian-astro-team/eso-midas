! @(#)wa_test1.prg	19.1 (ES0-DMD) 02/25/03 13:34:57
! @(#) @(#)wa_test1.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_test1.prg
!.PURPOSE:    test procedure 
!.USE:        VISUAL/PERS Wavelet
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 + IMA "Enter imag file name:  " 
DEFINE/PARAM P2 4. N "Enter Coefficients:" 

if p1(1:1) .eq. "+" then
   indisk/fits MID_TEST:ngc2997.fits ngc2997.bdf >Null
   define/local imag/c/1/60 "ngc2997 "
else
   define/local imag/c/1/60 {p1}
endif
define/local nwave/c/1/60 "zzwaxx"

define/local pri_data/i/1/1 0
define/local nsig/r/1/1 0.1
define/local visu/i/1/1 1

create/display 1 512,512,10,630
assign/display d,1
load/image {imag} scale=2 cuts=0,3200
load/lut rainbow

write/out "Filtering"
FILTER/WAVE {imag} &a  5 1 1 4 2.

write/out "Comparison"
COMPAR/WAVE {imag} &a 4 ? ? ? N Y

write/out "SNR"
PLOT/SNR

write/out "Correlation"
PLOT/COR
delete/table cmp_correl no
delete/table cmp_snr no

write/out "Wavelet transform 
TRANSF/WAVE &a {nwave} 1 3

write/out "information on the wavelet transform"
HEADER/WAVE {nwave}
INFO/WAVE {nwave}

write/out "first wavelet plane"
EXTRAC/WAVE {nwave} &c 1

write/out "multiply"
compute/image &c = &c*{P2}

write/out "first resolution plan"
ENTER/WAVE {nwave} &c 1 {nwave}

write/out "reconstruction"
RECONS/WAVE {nwave} &d

create/display 2 512,512,630,630
assign/display d,2
load/image &d scale=2 cuts=0,3200
-delete {nwave}.wave
delete/image &a n
delete/image &c n
delete/image &d n
