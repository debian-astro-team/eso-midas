! @(#)wa_extr.prg	19.1 (ES0-DMD) 02/25/03 13:34:56
! @(#) @(#)wa_extr.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_extr.prg
!.PURPOSE:    Extracts a plane from a wavelet transform
!.USE:        EXTRAC/WAVE Wavelet Imag_Out  Scale_Number
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? IMAGE "Enter wavelet file name:  "
DEFINE/PARAM P2 ? IMAGE "Enter destination frame:  " 
DEFINE/PARAM P3 ? N "Enter the scale number (< number of scales): " 1,10

WRITE/KEYW IN_A/C/1/60 {P1}
WRITE/KEYW OUT_A/C/1/60  {P2}
WRITE/KEYW INPUTI/I/1/1  {P3}

RUN CON_EXE:wa_extr.exe
