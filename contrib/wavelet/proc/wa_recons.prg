! @(#)wa_recons.prg	19.1 (ES0-DMD) 02/25/03 13:34:57
! @(#) @(#)wa_recons.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_recons.prg
!.PURPOSE:    reconstruct an image from a wavelet transform
!.USE:        RECONS/WAVE Wavelet_In Imag_Out 
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? C "Enter input wavelet:  " 
DEFINE/PARAM P2 ? IMAGE "Enter destination frame:  "

WRITE/KEYW IN_A/C/1/60 {P1}
WRITE/KEYW OUT_A/C/1/60  {P2}
WRITE/KEYW INPUTI/I/1/1  0

RUN CON_EXE:wa_rec.exe
