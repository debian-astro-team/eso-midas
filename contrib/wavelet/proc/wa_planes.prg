! @(#)wa_planes.prg	19.1 (ES0-DMD) 02/25/03 13:34:57
! @(#) @(#)wa_planes.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_planes.prg
!.PURPOSE:    visualisation of a wavelet file in a cube
!.USE:        VISUAL/PLAN Wavelet [Display]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? C "Enter wavelet file name:  "
DEFINE/PARAM P2 Y C "Display the result (Y or N)"

set/format I1
set/format f16.9

! parameter definition
define/local I/i/1/1 0
define/local Nl/i/1/1 0
define/local Nc/i/1/1 0
define/local Nscale/i/1/1 0
define/local Typ_Wave/C/1/60
define/local n_plan/C/1/60 
define/local name_tem/C/1/60 "auxfile"
define/local posx/i/1/1 100
define/local posy/i/1/1 300
define/local sizex/i/1/1 0
define/local sizey/i/1/1 0
define/local maxm/r/1/1 1
define/local disp/i/1/1 0
define/local scalx/r/1/1 1
define/local scaly/r/1/1 1

@c wa_head {P1}
WRITE/KEYW Nl {OUTPUTI(1)}
WRITE/KEYW Nc {OUTPUTI(2)}
WRITE/KEYW Nscale {OUTPUTI(3)}
WRITE/KEYW Typ_Wave "{OUT_A(1:3)}"

!      Load each plane in a window
IF Typ_Wave(1:3) .EQ. "CUB" .OR. Typ_Wave(1:3) .EQ. "PYR" THEN
     sizex = Nl
     sizey = Nc
     IF Sizex .GT. 512 sizex = 512
     IF Sizey .GT. 512 sizey = 512
     DO I = 1 {Nscale}
        maxm = 1.
        disp = Nscale-I+1
        IF disp .GT. 9 disp = 9
        write/keyw n_plan "scale_{disp}"
        @c wa_extr {P1} {n_plan} {disp}
        IF Typ_Wave(1:3) .EQ. "PYR" THEN
           write/descr {n_plan} start/d/1/2 1,1
           write/descr {n_plan} step/d/1/2 {maxm},{maxm}
        ENDIF
        IF P2(1:1) .EQ. "Y" THEN
            compute/keyw NC = {{n_plan},npix(1)}
            compute/keyw NL = {{n_plan},npix(2)}

            scalx = sizex / Nc
            scaly = sizey / Nl 
            IF NC .LT. sizex NC = sizex
            IF NL .LT. sizey NL = sizey
            create/display {disp} {sizex},{sizey},{posx},{posy} 1,{NC},{NL}
            compute/keyw posy = {posy}-20
            compute/keyw posx = {posx}+50
            load/image {n_plan} scale={scalx},{scaly}
            maxm = maxm * 2
        ENDIF
     ENDDO
ELSEIF Typ_Wave(1:3) .EQ. "IMA" THEN
       write/keyw n_plan "scale_1"
       @c wa_extr {P1} {n_plan} 1
       IF P2(1:1) .EQ. "Y" THEN
            compute/keyw NC = {{n_plan},npix(1)}
            compute/keyw NL = {{n_plan},npix(2)}
            Sizex = Nc
            IF Sizex .GT. 512 sizex = 512
            Sizey = Nl
            IF Sizey .GT. 512 sizey = 512         
            create/display 1 {sizex},{sizey},{posx},{posy} 1,{NC},{NL}
            compute/keyw posy = {posy}-20
            compute/keyw posx = {posx}+50
            load/image {n_plan}
       ENDIF
ELSE  
       write/out "Error: bad wavelet transform"
ENDIF

