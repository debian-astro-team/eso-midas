! @(#)wa_transf.prg	19.1 (ES0-DMD) 02/25/03 13:34:58
! @(#) @(#)wa_transf.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_transf.prg
!.PURPOSE:    computes a wavelet transform
!.USE:        TRANSF/WAVE Image_In Wavelet_Out [Num_Trans] [Nb_plan] [Fc]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? IMAGE "Enter source frame:  "
DEFINE/PARAM P2 ? C "Enter destination wavelet:  " 
DEFINE/PARAM P3 2 N "Enter the algorithm number:  " 1,8
DEFINE/PARAM P4 3 N "Enter the number of scales:  " 2,10
DEFINE/PARAM P5 0.5 N "Enter the  cut-off frequency:  " 0.1,0.5

WRITE/KEYW IN_A/C/1/60 {P1}
WRITE/KEYW OUT_A/C/1/60  {P2}
WRITE/KEYW INPUTI/I/1/1  {P3}
WRITE/KEYW INPUTI/I/2/1  {P4}
WRITE/KEYW INPUTR/R/1/1  {P5}

RUN CON_EXE:wa_trans.exe

