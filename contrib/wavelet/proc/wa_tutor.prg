! @(#)wa_tutor.prg	19.1 (ESO-DMD) 02/25/03 13:34:58
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  wa_tutor.prg  to implement TUTORIAL/WAVE
! J.L. Starck	930531
! K. Banse	930707, 940104, 980930
!
! use via TUTORIAL/WAVE [image] [delay] 
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 MID_TEST:ngc2997 IMA "Enter imag file name:  " 
define/param p2 5 n "Enter no. of sec. to wait/secs between demos: "
define/param p3 NO c "Enter YES or NO for deleting temporary frames: "
! 
! get test image
indisk/fits MID_TEST:ngc2997.fits ngc2997.bdf >Null
!
define/local imag/c/1/60 "ngc2997"
define/local wavel/c/1/60 "wngc"
! 
define/local pri_data/i/1/1 0
define/local sleep/i/1/1 {p2}
define/local visu/i/1/1 1
! 
if aux_mode(1) .lt. 2 then		!VMS
   define/local auxfil/c/1/2 ".*"
else
   define/local auxfil/c/1/2 "  "
endif
! 
write/out "wavelet transform: algo=1, number of scales=5"
write/out "Wavelet transform: a trous algorithm" 
TRANSF/WAVE {imag} {wavel} 1 5
! 
reset/display
write/out "visual/plan"
write/out "each scale is displayed in a window"
VISUAL/PLAN {wavel}
wait/secs {sleep}
! 
reset/display
write/out "visual/cube"
write/out "visualization of all the scales in a cube"
VISUAL/CUBE {wavel}
wait/secs {sleep}
! 
reset/display
write/out "visual/cont"
write/out "one contour per scale is plotted"
VISUAL/CONT {wavel}
wait/secs {sleep}
! 
reset/display
write/out "visual/synt"
write/out "synthesis image: one colour per scale"
VISUAL/SYNT {wavel}
wait/secs {sleep}
! 
reset/display
write/out "visual/pers"
write/out "superposition of all scales"
write/out "each scale is plotted in a 3 dim. representation"
VISUAL/PERS {wavel}
! 
write/out "wavelet transform: algo=6, number of scales=5"
write/out "Wavelet transform: pyramidal algorithm"
TRANSF/WAVE {imag} {wavel} 6 5
! 
reset/display
write/out "visual/plan"
write/out "each scale is displayed in a window"
VISUAL/PLAN {wavel}
wait/secs {sleep}
! 
reset/display
write/out "visual/cube"
write/out "visualization of all the scales in a cube"
VISUAL/CUBE {wavel}
wait/secs {sleep}
! 
reset/display
write/out "visual/synt"
write/out "synthesis image: all the scale in one window"
VISUAL/SYNT {wavel}
wait/secs {sleep}
! 
reset/display
write/out "visual/pers"
write/out "superposition of all scales"
write/out "each scale is plotted in a 3 dim. representation"
VISUAL/PERS {wavel}
wait/secs {sleep}
! 
write/out "wavelet transform: algo=8, number of scales=5"
write/out "Mallat's algorithm"
TRANSF/WAVE {imag} {wavel} 8 5
! 
reset/display
write/out "visual/synt"
VISUAL/SYNT {wavel}
load/lut rainbow1
! 
-delete {wavel}.wave{auxfil}
-delete file_visu.bdf{auxfil}

