! @(#)wa_filtr.prg	19.1 (ES0-DMD) 02/25/03 13:34:56
! @(#) @(#)wa_filtr.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_filtr.prg
!.PURPOSE:    filter an image in the wavelet space
!.USE:        FILTER/WAVE Image_In Imag_Out [Num_Trans] [Filtr_Num] 
!                 {Iter_Number] [Nb_Scales] [N_Sigma] [Fc] [Noise]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis, restoration
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? IMAGE "Enter source frame:  "
DEFINE/PARAM P2 ? IMAGE "Enter destination frame:  " 
DEFINE/PARAM P3 2 N "Enter the algorithm number:  " 1,8
DEFINE/PARAM P4 1 N "Enter the filtering number:  " 1,4
DEFINE/PARAM P5 1 N "Enter the number of iterations:  " 1,20
DEFINE/PARAM P6 4 N "Enter the number of scales:  " 1,20
DEFINE/PARAM P7 3. N "Statistical parameter (n_sigma):  " 1,20
DEFINE/PARAM P8 0. N "Enter the standard deviation of the noise:  " 

WRITE/KEYW IN_A/C/1/60 {P1}
WRITE/KEYW OUT_A/C/1/60  {P2}
WRITE/KEYW INPUTI/I/1/1  {P3}
WRITE/KEYW INPUTI/I/2/1  {P4}
WRITE/KEYW INPUTI/I/3/1  {P5}
WRITE/KEYW INPUTI/I/4/1  {P6}

WRITE/KEYW INPUTR/R/1/1  {P7}
WRITE/KEYW INPUTR/R/2/1  {P8}

RUN CON_EXE:wa_filt.exe


