! @(#)wa_entr.prg	19.1 (ES0-DMD) 02/25/03 13:34:56
! @(#) @(#)wa_entr.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_entr.prg
!.PURPOSE:  replace a plane of the wavelet transform by an image
!.USE:      ENTER/WAVE Wavelet_In Image [Scale_Number] Wavelet_Out
!.AUTHOR:   Jean-Luc Starck
!.VERSION:  03/02/25
! KEYWORDS: Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? C "Enter wavelet input file name:  "
DEFINE/PARAM P2 ? IMAGE "Enter input image:  " 
DEFINE/PARAM P3 1 N "Enter the scale number (< number of scales): " 1,10
DEFINE/PARAM P4 ? C "Enter wavelet output file name: "

WRITE/KEYW IN_A/C/1/60  {P1}
WRITE/KEYW IN_B/C/1/60  {P2}
WRITE/KEYW INPUTI/I/1/1 {P3}
WRITE/KEYW OUT_A/C/1/60 {P4}

RUN CON_EXE:wa_entr.exe
