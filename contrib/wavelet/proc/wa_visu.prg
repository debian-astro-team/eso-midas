! @(#)wa_visu.prg	19.1 (ES0-DMD) 02/25/03 13:34:58
! @(#) @(#)wa_visu.prg 19.1 03/02/25 ESO @(#)
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: wa_visu.prg
!.PURPOSE:    visualisation of a wavelet file
!.USE:        VISUAL/WAVE Wavelet [Type_Visu]
!.AUTHOR:     Jean-Luc Starck
!.VERSION:    03/02/25
! KEYWORDS:   Wavelet transform, Multiresolution analysis
! ----------------------------------------------------------------------

DEFINE/PARAM P1 ? C "Enter wavelet file name:  "
DEFINE/PARAM P2 PLAN C "Visualisation parameter (CUB,SYN,PER,PLAN,CONT):  "

@c wa_head {P1}

IF OUT_A(1:3) .EQ. "CUB" THEN

!        Creation of a synthesis image from the wavelet
!        coefficient.
   IF P2(1:3) .EQ. "SYN"  @c wa_synt {P1}

!        Visualisation of all the plane with a perspective view
   IF P2(1:3) .EQ. "PER"  @ {P1}

!        Visualisation of all the planes in a cube
   IF P2(1:3) .EQ. "CUB"  @c wa_cube {P1}

!      Load each plane in a window
   IF P2(1:3) .EQ. "PLA" @c wa_planes {P1} 

!      Plot one contour per plane on a graphic window
   IF P2(1:3) .EQ. "CON"  @c wa_contour {P1}


!           Pyramidal visualisation
ELSEIF OUT_A(1:3) .EQ. "PYR"  THEN

   IF P2(1:3) .EQ. "SYN"  @c wa_synt {P1}

!        Visualisation of all the plane with a perspective view
   IF P2(1:3) .EQ. "PER"   @c wa_pers {P1} 

!      Load each plane in a window
   IF P2(1:3) .EQ. "PLA"  @c wa_planes {P1}

   IF P2(1:3) .EQ. "CONT" @c wa_contour {P1}

!        Visualisation of all the planes in a cube
   IF P2(1:3) .EQ. "CUB"  @c wa_cube {P1}

ELSEIF OUT_A(1:3) .EQ. "IMA"  THEN
   IF P2(1:3) .EQ. "SYN"   @c wa_synt {P1}
   IF P2(1:3) .EQ. "PLA"   @c wa_planes {P1}
ENDIF







