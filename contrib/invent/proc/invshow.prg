! @(#)invshow.prg	19.1 (ES0-DMD) 02/25/03 13:26:16
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: INVCLASS.PRG
!.PURPOSE:        executes the command show/inventory
!.USE:            @c invclass
!.AUTHOR:         Rein H. Warmels, ESO-IPG
!.VERSION:        9102011 change due to new directory structure
! ----------------------------------------------------------------------
DEFINE/PARAM P1 " "
DEFINE/PARAM P2 " "
!
WRITE/KEYW INPUTC/C/1/1 'P1'
WRITE/KEYW INPUTC/C/2/1 'P2'
RUN CON_EXE:INVSET
