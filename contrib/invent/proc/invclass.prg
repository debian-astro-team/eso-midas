! @(#)invclass.prg	19.1 (ES0-DMD) 02/25/03 13:26:15
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: INVCLASS.PRG
!.PURPOSE:        executes the command classify/inventory
!.USE:            @c invclass	
!.AUTHOR:         Rein H. Warmels, ESO-IPG
!.VERSION:        9102011 change due to new directory structure
! ----------------------------------------------------------------------
DEFINE/PARAM P1 " "
WRITE/KEYW IN_B 'P1'
!
RUN CON_EXE:INVCLASS
