! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: invdemo.prg
!.PURPOSE:        Demo. procedure for INVENTOTY reduction package
!.USE:            Execute as TUTORIAL/INVENT
!                 The following files are copied into your directory :
!                 INFTEST.BDF     --  Test frame
!                 Output files are
! 
! 070824	last modif
! --------------------------------------------------------------------
!
reset/displ >Null
! 
write/out Tutorial for the INVENTORY package
write/out This show creates the following files:
write/out One test frame: INVTEST.BDF
write/out Three output tables: TSTEST.TBL, TATEST.TBL, TCTEST.TBL
write/out First we copy the test frame
!
indisk/fits MID_TEST:invtest.fits invtest 
!
set/context invent
! 
write/keyw INV_INTG/I/1/12 1,0,1,1,0,0,0,7,1,1,0,0
write/keyw INV_INTG/I/13/11 0,0,0,1,0,20,3,0,0,3,32
write/keyw INV_REAL/R/1/10 1290.,130000.,22.,4.5,.05,-.05,1.,18.5,50.,5400.
write/keyw INV_REAL/R/11/10 21.11,3.0,5.0,0.26,0.33,0.39,0.43,0.46,0.36,0.24
write/keyw INV_REAL/R/21/10 0.19,0.15,0.14,0.14,0.14,0.14,0.14,0.14,0.14,0.14
write/keyw INV_REAL/R/31/10 0.14,0.14,0.14,0.14,0.14,0.14,0.14,0.14,3.0,5.0
write/keyw INV_REAL/R/41/10 2.0,4.0,4.5,0.25,0.6,0.02,0.6,1.0,0.0,2.5
write/keyw INV_REAL/R/51/10 1.2,0.0,1.39,0.0,0.0,0.0,0.0
!
create/display 0 700,880,50,55
load/lut heat
load/image invtest 0 2,2 cuts=870,1500
!
write/out 
write/out First, INVENTORY searches for objects in this frame
write/out The output table tstest.tbl will contain the results
echo/on
search/inv invtest tstest
echo/off
write/out The objects found are indicated by square small crosses
echo/on
load/table tstest :X :Y ? 5 5 1
echo/off
wait/sec 1
!
write/out
write/out Next, INVENTORY analyses the objects found:
echo/on
analyse/inv invtest tstest tatest VER
echo/off
write/out The ANALYSE command comes with somewhat less objects 
write/out the new list of objects is stored in table TATEST
echo/on
clear/channel overlay
load/table tatest :X :Y ? 5 5 1
echo/off
wait/sec 1
!
write/out
write/out Finally, the CLASSIFY command will classify the objects
echo/on
classify/inv tatest
echo/off
clear/channel overlay
write/out The objects in the squares are classified as galaxies
write/out The objects in the circles are classified as stars
echo/on
select/table tatest :CLASS.EQ.2.0
load/table tatest :X :Y ? 2 5 3
wait/sec 1
select/table tatest :CLASS.EQ.1.0
load/table  tatest :X :Y ? 1 5 5
echo/off
