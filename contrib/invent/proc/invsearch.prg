! @(#)invsearch.prg	19.1 (ES0-DMD) 02/25/03 13:26:15
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: INVSEARCH.PRG
!.PURPOSE:        execure the command search/inventory
!.USE:            @c invsearch
!.AUTHOR:         Rein H. Warmels, ESO-IPG
!.VERSION:        910211 RHW change due to new direftory structure
! ----------------------------------------------------------------------
DEFINE/PARAMETER P1 ?  IMA   "Enter input frame:"
DEFINE/PARAMETER P2 ?  TABLE "Enter output table:"
!
WRITE/KEYW IN_A 'P1'
WRITE/KEYW OUT_A 'P2'
RUN CON_EXE:INVSEARCH
