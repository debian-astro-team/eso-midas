! @(#)invanalys.prg	19.1 (ES0-DMD) 02/25/03 13:26:15
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: INVANALYS.PRG
!.PURPOSE:        execute the command analyse/inventory
!.USE:            @c invanalys
!.AUTHOR:         Rein H. Warmels
!.VERSION:        910211 RHW change due to new directory structure
! ----------------------------------------------------------------------
DEFINE/PARAM P1 ?
DEFINE/PARAM P2 ? 
DEFINE/PARAM P3 ?
DEFINE/PARAM P4 VERIF C
DEFINE/PARAM P5 NODEB C
DEFINE/PARAM P6 NONE  C
!
WRITE/KEYW IN_A  'P1'
WRITE/KEYW IN_B  'P2'
WRITE/KEYW OUT_A 'P3'
WRITE/KEYW INPUTC/C/1/10 'P5'
WRITE/KEYW OUT_B 'P6'
!
BRANCH P4(1:3) VER,NOV VERIF,NOVER                         ! branch on verify
WRITE/OUT "Warning: Unknown option: VERIFY option used"
WRITE/KEYW INV_INTG/I/4/2 1,0
GOTO NEXT
!
VERIF:
WRITE/KEYW INV_INTG/I/4/2 1,0
GOTO NEXT
!
NOVER:
WRITE/KEYW INV_INTG/I/4/1 0
GOTO NEXT
!
NEXT:
IF P5(1:3) .EQ. "DEB" THEN
   WRITE/KEYW INV_INTG/I/1/1 3
ENDIF
!
RUN CON_EXE:INVANALYS
WRITE/KEYW INV_INTG/I/1/1 1
WRITE/KEYW INV_INTG/I/3/3 1,1,0
