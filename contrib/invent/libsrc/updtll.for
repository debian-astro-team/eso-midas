C @(#)updtll.for	19.1 (ES0-DMD) 02/25/03 13:25:40
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE UPDTLL ( MCAT , NL , IDET , NXS , ILIM ,
     &                                          IDA , IP , M )
C
      IMPLICIT NONE
      INTEGER  ILIM , NL , NXS
      INTEGER  IDA(2,0:ILIM)
      INTEGER  IDET(NXS,0:ILIM)
      INTEGER  IP
      INTEGER  K , L , L1 , L2
      INTEGER  M , MCAT(4,NL) , NN
C
C ******      Look for other close detections.
C
      DO 10 K = 0 , ILIM
          L1 = MAX( 1 , IP - IDA(1,K) )
          L2 = MIN( NXS , IP + IDA(2,K) )
          DO 20 L = L1 , L2
            NN = IDET(L,K)
            IF ( NN .NE. 0 ) THEN
                CALL UPDTL( MCAT , NL , NN , M )
            ENDIF
   20          CONTINUE
   10      CONTINUE
C
C ******      Write detection entry in array IDET.
C
      IDET(IP,0) = M
C
      RETURN
C
      END
