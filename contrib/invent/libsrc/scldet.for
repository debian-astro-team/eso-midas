C @(#)scldet.for	19.1 (ES0-DMD) 02/25/03 13:25:39
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE SCLDET(LZ, NCAT, PMTR, PRCT, NCT,
     &                  PMT, PRC, APSF, NPAS, SCA,
     &                  SCC)
C
      IMPLICIT  NONE
      INCLUDE   'MID_REL_INCL:INVENT.INC/NOLIST'
C
      INTEGER   LZ                          ! IN: Object number
      INTEGER   NCAT(NIPAR,MAXCNT)           ! IN: Object integer parameters
      REAL      PMTR(NRPAR,MAXCNT)           ! IN: Object real parameters
      REAL      PRCT(0:MAXSUB,MAXCNT)        ! IN: Object profile
      INTEGER   NCT(NIPAR)                   ! IN: Component integer parameters 
      REAL      PMT(NRPAR)                   ! IN: Component real parameters
      REAL      PRC(0:MAXSUB)                ! IN: Component profile 
      REAL      APSF(0:MAXSUB)               ! IN: One dimensional p.s.f.
      INTEGER   NPAS                         ! IN: Iteration number
      REAL      SCA                          ! OUT: Object intensity
      REAL      SCC                          ! OUT: Component intensity
C
      INTEGER   KSAT
      REAL      ATMP , SCALE
C
      SCALE = 9.0 / ( 1.0 + 8.0*APSF(1) )
      IF ( NPAS .GT. 2 ) THEN
          SCA = PMTR(12,LZ)
          SCC = PMT(12)
      ELSE
          KSAT = NCAT(6,LZ)
          IF ( KSAT .EQ. -1 ) THEN
              SCA = SCALE * PMTR(2,LZ)
          ELSE
              ATMP = APSF(KSAT+2)
              IF ( ATMP .GT. 0.0 ) THEN
                  SCA = PRCT(KSAT+2,LZ) / ATMP
              ELSE
                  SCA = 0.0
              ENDIF
          ENDIF
          KSAT = NCT(6)
          IF ( KSAT .EQ. -1 ) THEN
              SCC = SCALE * PMT(2)
          ELSE
              ATMP = APSF(KSAT+2)
              IF ( ATMP .GT. 0.0 ) THEN
                  SCC = PRC(KSAT+2) / ATMP
              ELSE
                  SCC = 0.0
              ENDIF
          ENDIF
      ENDIF
      SCC = 0.0
C
      RETURN
C
      END
