C @(#)sbgnet.for	19.1 (ES0-DMD) 02/25/03 13:25:39
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine SBGNET          version 1.0          880503
C  A. Kruszewski              Warsaw U. Obs.
C                                     1.1          881114 
C.INPUT/OUTPUT
C  input arguments
C  A          real*4 array           image buffer
C  JAPY       integer*4 array        pointers to image lines
C  IBUF       integer*4 array        limits of frame buffer
C  IXYU       integer*4 array        limits of used area
C  JS         integer*4              segment number
C  JSTART     integer*4              first searched line
C  JEND       integer*4              last searched line
C  INET       integer*4 array        net X-coordinates
C  SKYNET     real*4 array           sky background net
C  NHSEG      integer*4              number of horizontal segments
C  CRMD       real*4 array           controls subroutine mode
C  IHED       integer*4              subarray halfedge
C  output arguments
C  SKYNET     real*4 array           sky background net
C-----------------------------------------------------------------------
      SUBROUTINE SBGNET(A, JAPY, IBUF, IXYU, JS, JSTART,
     &                 JEND, inet0, INET, 
     +                 skynet0, SKYNET, NHSEG, CRMD, IHED)
C
      IMPLICIT  NONE
C
      INTEGER   NHSEG
      INTEGER   IBUF(4) , IFULL , IHED , II
      INTEGER   IXYU(4) , JAPY(1) , JEND
      INTEGER   JS , JSTART , INET0, INET(NHSEG)
C
      REAL   A(1) , CRMD(3) , SKYNET0(2),skynet(2,NHSEG)
C
C ******      Fill first line of SKYNET.
C
      write(*,12345) jstart,jend,nhseg,ihed,crmd(1),crmd(2),crmd(3)
12345 format('jstart, jend, nhseg, ihed = ',4i4,' crmd = ',3f8.3)

      IFULL = 1
      IF ( JS .GT. 1 ) THEN
            SKYNET0(1) = SKYNET0(2)
          DO 10 II = 1 , NHSEG
            SKYNET(1,II) = SKYNET(2,II)
   10          CONTINUE

      ELSE
          CALL SKYMOD( A , JAPY , IBUF , INET0 ,
CKB     &                  JSTART , CRMD , IHED , IFULL , SKYNET(1,0) )
     &                  JSTART , CRMD , IHED , IFULL , SKYNET0(1) )
          DO 20 II = 1 , NHSEG
            CALL SKYMOD( A , JAPY , IBUF , INET(II) ,
     &                  JSTART , CRMD , IHED , IFULL , SKYNET(1,II) )
   20          CONTINUE
        ENDIF
C
C ******      Fill second line of SKYNET.
C
      CALL SKYMOD( A , JAPY , IBUF , INET0 , JEND ,
     &                        CRMD , IHED , IFULL , SKYNET0(2) )
      DO 30 II = 1 , NHSEG
          CALL SKYMOD( A , JAPY , IBUF , INET(II) , JEND ,
     &                        CRMD , IHED , IFULL , SKYNET(2,II) )
   30      CONTINUE
C
      RETURN
C
      END
