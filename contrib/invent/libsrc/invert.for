C @(#)invert.for	19.1 (ES0-DMD) 02/25/03 13:25:35
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine INVERT         version 1.0       830818
C  A. Kruszewski             ESO Garching
C.PURPOSE
C  inverts a symmetrical positively defined array SMTR
C  and keeps the result in the same array SMTR
C  it was written and tested in the case when array SMTR represents
C  a covariance matrix of a multimensional random variable
C  it may fail in other applications
C.INPUT/OUTPUT
C  input arguments
C  SMTR        real*4 array      symmetrical array
C  N           integer*4         dimension of array SMTR
C  output arguments
C  SMTR        real*4 array      inverted input array
C-----------------------------------------------------------------------
      SUBROUTINE INVERT(SMTR,N)
C
      IMPLICIT NONE
      INTEGER  N
      REAL     SMTR(N,N)
C
      INTEGER  I, I1
      INTEGER  J, J1
      INTEGER  K
      INTEGER  N1
      REAL     TEMP
C
C calculates triangular square root of the input array
C
      SMTR(1,1) = SQRT(SMTR(1,1))
      DO 40 I = 2,N
          I1     = I - 1
          DO 20 J = 1,I1
              TEMP   = SMTR(I,J)
              J1     = J - 1
              DO 10 K = 1,J1
                  TEMP   = TEMP - SMTR(I,K)*SMTR(J,K)
   10         CONTINUE
              SMTR(I,J) = TEMP/SMTR(J,J)
   20     CONTINUE
          TEMP   = SMTR(I,I)
          DO 30 K = 1,I1
              TEMP   = TEMP - SMTR(I,K)*SMTR(I,K)
   30     CONTINUE
          SMTR(I,I) = SQRT(TEMP)
   40 CONTINUE
C
C inverts the triangular square root matrix
C
      DO 50 I = 1,N
          SMTR(I,I) = 1.0/SMTR(I,I)
   50 CONTINUE
      N1     = N - 1
      DO 80 I = 1,N1
          I1     = I + 1
          DO 70 J = I1,N
              J1     = J - 1
              TEMP   = 0.0
              DO 60 K = I,J1
                  TEMP   = TEMP + SMTR(I,K)*SMTR(J,K)
   60         CONTINUE
              SMTR(I,J) = -TEMP*SMTR(J,J)
   70     CONTINUE
   80 CONTINUE
C
C squares inverted triangular matrix
C
      DO 110 I = 1,N
          DO 100 J = 1,I
              TEMP   = 0.0
              DO 90 K = I,N
                  TEMP   = TEMP + SMTR(I,K)*SMTR(J,K)
   90         CONTINUE
              SMTR(J,I) = TEMP
              SMTR(I,J) = TEMP
  100     CONTINUE
  110 CONTINUE
      RETURN

      END
