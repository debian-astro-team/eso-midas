C @(#)fltrbp.for	19.1 (ES0-DMD) 02/25/03 13:25:33
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine FLTRBP         version 1.0         1987.01.26
C  A. Kruszewski             Obs. de Geneve
C.PURPOSE
C  Identifies bright pixels and replaces them with median value
C  of eight nearest pixels. Average value of all nine pixels
C  is updated.
C.INPUT/OUTPUT
C  Input parameters
C  A          real*4 array        input image array
C  JAPY       integer*4 array     pointers to image lines
C  IBUF       integer*4 array     limits of image buffer
C  I          integer*4           pixel X coordinate
C  J          integer*4           pixel Y coordinate
C  BGRD       real*4              local background
C  FLTR       real*4              filter parameter
C  AV         real*4              average of nine central pixels
C  Output parameters
C  AV         real*4              may be updated
C-----------------------------------------------------------------------
      SUBROUTINE FLTRBP(A, JAPY, IBUF, I, J, BGRD, FLTR, AV)
C
      IMPLICIT  NONE
C
      REAL      A(1)
      INTEGER   JAPY(1)
      INTEGER   IBUF(4)
      INTEGER   I
      INTEGER   J
      REAL      BGRD 
      REAL      FLTR
      REAL      AV
C
      INTEGER   IADR , IADRM , IADRP
      INTEGER   JOFF
      INTEGER   K
      INTEGER   L, L1
      REAL      AT(8), AV5 , AV8
      REAL      CPIX 
      REAL      TEMP
C
      JOFF = IBUF(2) - 1
      IADR = JAPY(J-JOFF) + I
      CPIX = A(IADR)
      AV8 = ( AV * 9.0 - CPIX ) / 8.0
      IF ( (CPIX-AV8) .GT. (0.5*FLTR*(AV8-BGRD)) ) THEN
          IADRM = JAPY(J-JOFF-1) + I
          IADRP = JAPY(J-JOFF+1) + I
          AT(1) = A(IADRM-1)
          AT(2) = A(IADRM)
          AT(3) = A(IADRM+1)
          AT(4) = A(IADR-1)
          AT(5) = A(IADR+1)
          AT(6) = A(IADRP-1)
          AT(7) = A(IADRP)
          AT(8) = A(IADRP+1)
          DO 10 K = 1 , 7
            L1 = K + 1
            DO 20 L = 8 , L1 , -1
                IF ( AT(L) .LT. AT(L-1) ) THEN
                  TEMP = AT(L)
                  AT(L) = AT(L-1)
                  AT(L-1) = TEMP
                ENDIF
   20            CONTINUE
   10          CONTINUE
             AV5 = 0.0
          DO 30 K = 2 , 6
            AV5 = AV5 + AT(K)
   30          CONTINUE
             AV5 = AV5 / 5.0
          IF ( (CPIX-AV5) .GT. (FLTR*(AV5-BGRD)) ) THEN
            AV = AV5
          ENDIF
      ENDIF
C
      RETURN
C
      END
