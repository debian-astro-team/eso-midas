C @(#)objshp.for	19.1 (ES0-DMD) 02/25/03 13:25:36
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine OBJSHP         version 1         820521
C  A. Kruszewski             ESO Garching
C.KEYWORDS
C  search, shape
C.PURPOSE
C  calculates elongation of a geometrical figure formed by
C  multiple detections
C  positions of detections are used only
C.INPUT/OUTPUT
C  input arguments
C  ICAT        integer*4 array   pixel coordinates of detections
C  M           integer*$         number of independent detections
C                                of the same object
C  AVI         real*4            average x-coordinate in pixels
C  AVJ         real*4            average y-coordinate in pixels
C  output arguments
C  ELON        real*4            elongation of detection points
C  THETA       real*4            position angle of major axis
C  ISIZ        integer*4         maximal distance between detection
C                                points in pixels
C-----------------------------------------------------------------------
      SUBROUTINE OBJSHP(AVI, AVJ, AVII, AVIJ, AVJJ, ELON, THETA)
C
      IMPLICIT NONE
      REAL     ARG1 , ARG2
      REAL     AVI , AVII , AVIJ , AVJ , AVJJ
      REAL     ELON , THETA
C
C ******      Calculate elongation ELON and
C ******      position angle of major axis THETA.
C
      ELON = (AVII-AVJJ) * (AVII-AVJJ) + 4 * AVIJ * AVIJ
      ELON = SQRT( ELON )
      IF ( AVII+AVJJ .GT. 0.0 ) THEN
          ELON = ELON / (AVII+AVJJ)
          ARG1 = 2.0 * AVIJ
          ARG2 = AVII - AVJJ
          IF ( ARG1 .NE. 0.0 .OR. ARG2 .NE. 0.0 ) THEN
            THETA = 0.5 * ATAN2( ARG1 , ARG2 )
          ELSE
            ELON = 0.0
            THETA = 0.0
          ENDIF
      ELSE
          ELON = 0.0
          THETA = 0.0
      ENDIF
C
      RETURN
C
      END  

