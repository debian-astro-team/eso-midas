C @(#)clbrgt.for	19.1 (ES0-DMD) 02/25/03 13:25:32
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENT:   subroutine CLBRGT         version 1.1       870729
C          A. Kruszewski             ESO Garching
C.PURPOSE: classifies saturated objects into stars and galaxies
C.INPUT/OUTPUT: 
C. input arguments
C  PMTR        real*4 array      array holding classifiers
C  BRGT        logical array     array indicating saturated objects
C  M           integer*4         number of objects
C  output arguments
C  ICLS        integer*4 array   array holding the classification
C                                coded: 0-defect or not classified,
C                                1-star, 2-galaxy. for each object 
C 				 the first element gives a seed value,
C				 the second gives a current value.
C-----------------------------------------------------------------------
      SUBROUTINE CLBRGT(PMTR, BRGT, ICLS, M)
C
      IMPLICIT NONE
      INCLUDE'MID_REL_INCL:INVENT.INC/NOLIST'
C
      INTEGER I
      INTEGER IAC
      INTEGER ICLS(2,MAXCNT)
      INTEGER ISTAT
      INTEGER KUN, KNUL
      INTEGER MADRID(1)
      INTEGER M
C
      REAL    ALIM
      REAL    CVAR(3)
      REAL    PMTR(30,MAXCNT)
C
      LOGICAL BRGT(MAXCNT)
C
      COMMON /VMR/MADRID
C
      CALL STKRDR('INV_REAL', 5, 3, IAC, CVAR, KUN, KNUL, ISTAT )
C
      ALIM   = (CVAR(2)-CVAR(1))/2.0
      DO 10 I = 1,M
          IF (BRGT(I)) THEN
              IF (PMTR(3,I).GT.ALIM) THEN
                  ICLS(2,I) = 1

              ELSE
                  ICLS(2,I) = 2
              END IF

          END IF

   10 CONTINUE
      RETURN

      END
