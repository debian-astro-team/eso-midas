C @(#)flstbd.for	19.1 (ES0-DMD) 02/25/03 13:25:33
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C  subroutine FLSTBD         version 1.0         880901
C  A. Kruszewski             Warsaw U. Obs.
C.PURPOSE
C.INPUT/OUTPUT
C  Input arguments
C  NREG      integer*4         number of regional linked lists
C  ITOB      integer*4 array   limits of objects area
C  Output arguments
C  LSTP      integer*4 array   borders of regions
C-----------------------------------------------------------------------
      SUBROUTINE FLSTBD ( NREG , LSTP , ITOB )
C
      IMPLICIT  NONE
C
      INTEGER   NREG
      INTEGER   LSTP(0:4,0:NREG)
      INTEGER   ITOB(4)
C
      INTEGER   ISIDE
      INTEGER   KX, KY
      INTEGER   ICNT
      INTEGER   L, K
      INTEGER   K1, L1
C
C ******      Initialize pointers and find regions' borders.
      ISIDE = LSTP(0,0)
      KX = LSTP(1,0)
      KY = LSTP(2,0)
      ICNT = 0
      DO 10 L = 1 , KY
            L1 = L * ISIDE
            DO 20 K = 1 , KX
                  K1 = K * ISIDE
                  ICNT = ICNT + 1
                  LSTP(0,ICNT) = 0
                  LSTP(1,ICNT) = ITOB(1) + K1 - ISIDE
                  LSTP(2,ICNT) = ITOB(2) + L1 - ISIDE
                  LSTP(3,ICNT) = MIN( ITOB(1) + K1-1 , ITOB(3) )
                  LSTP(4,ICNT) = MIN( ITOB(2) + L1-1 , ITOB(4) )
   20            CONTINUE
   10      CONTINUE
C
      RETURN
      END

