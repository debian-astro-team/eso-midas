C @(#)rlbias.for	19.1 (ES0-DMD) 02/25/03 13:25:38
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE RLBIAS(PRFL, ICNT, BARG)
C
      IMPLICIT  NONE
      REAL      PRFL(8,0:1)
      INTEGER   ICNT(8,0:1)
      REAL      BARG
C
      INTEGER   ITEMP
      INTEGER   K
C
      REAL      AV, SIG
      REAL      TEMP(8)
C
      ITEMP = 0
      DO 10 K = 1 , 8
          IF ( ICNT(K,1) .GT. 0 ) THEN
              ITEMP = ITEMP + 1
              TEMP(ITEMP) = PRFL(K,1)
          ENDIF
   10 CONTINUE
      IF ( ITEMP .GE. 2 ) THEN
          CALL MEAN ( TEMP , ITEMP , AV , SIG )
          IF ( SIG .GT. 0.1*AV ) THEN
              BARG = AV / SIG
          ELSE
              BARG = 10.0
          ENDIF
      ELSE
          BARG = 10.0
      ENDIF
C
      RETURN
C
      END
C
