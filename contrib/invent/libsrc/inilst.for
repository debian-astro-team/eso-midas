C @(#)inilst.for	19.1 (ES0-DMD) 02/25/03 13:25:34
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.INPUT/OUTPUT
C  Input arguments
C  LSTP      integer*4 array    limits of regions
C  ITOB      integer*4 array    limits of object area
C  NREG      integer*4          number of regional linked lists
C  NCAT      integer*4 array    integer catalog
C  PMTR      real*4 array       real catalog
C  PRCT      real*4 array       catalog of profiles
C  M         integer*4          number of objects
C  MS        integer*4          maximal subarray half edge
C  Output arguments
C  LSTP      integer*4 array    limits of regions
C  NCAT      integer*4 array    integer catalog
C-----------------------------------------------------------------------
      SUBROUTINE INILST ( LSTP , ITOB , NREG , NCAT , PMTR , PRCT , M )
C
      IMPLICIT  NONE
      INCLUDE   'MID_REL_INCL:INVENT.INC/NOLIST'
C
      INTEGER   NREG
      INTEGER   LSTP(0:4,0:NREG)
      INTEGER   ITOB(4)
      INTEGER   NCAT(NIPAR,MAXCNT)
      REAL      PMTR(NRPAR,MAXCNT)
      REAL      PRCT(0:MAXSUB,MAXCNT)
      INTEGER   M 
C
      INTEGER   I, II
      INTEGER   J, JJ
      INTEGER   K
C
      DO 10 II = 1 , NREG
          LSTP(0,II) = 0
   10 CONTINUE
      DO 20 II = 1 , M
          NCAT(7,II) = 0
          NCAT(8,II) = 0
   20 CONTINUE
      JJ = 0
      DO 30 II = 1 , M
          I = NCAT(1,II)
          J = NCAT(2,II)
          IF ( I .GE. ITOB(1) .AND. J .GE. ITOB(2) .AND. I .LE. ITOB(3)
     &                                      .AND. J .LE. ITOB(4) ) THEN
              JJ = JJ + 1
              DO 40 K = 1 , NIPAR
                  NCAT(K,JJ) = NCAT(K,II)
   40         CONTINUE
              DO 50 K = 1 , NRPAR
                  PMTR(K,JJ) = PMTR(K,II)
   50         CONTINUE
              CALL PUTLSS( JJ , NREG , LSTP , NCAT , PMTR , PRCT )
          ENDIF
   30 CONTINUE
C
      M = JJ
C
      RETURN
C
      END
