C @(#)ifgood.for	19.1 (ES0-DMD) 02/25/03 13:25:34
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE IFGOOD(A, JAPY, IBUF, MAS, I,
     &                    J, IK, JK, BGRD,
     &                    FLTR, TRSH, GOOD)
C
      IMPLICIT    NONE	
      INCLUDE     'MID_REL_INCL:INVENT.INC/NOLIST'
C
      REAL        A(1)
      INTEGER     JAPY(1)
      INTEGER     IBUF(4)
      INTEGER     MAS((-MAXSUB):MAXSUB,(-MAXSUB):MAXSUB)
      INTEGER     I
      INTEGER     J
      INTEGER     IK
      INTEGER     JK
      REAL        BGRD
      REAL        FLTR
      REAL        TRSH
      LOGICAL     GOOD
C
      INTEGER     IK1, IK2
      INTEGER     JK1, JK2
      INTEGER     ITMP
      INTEGER     JOF, JOFF
      INTEGER     L
      INTEGER     K
      REAL        AVER
      REAL        TEMP
C
      GOOD = .TRUE.
      IK1 = MAX( IBUF(1) , IK-1 , I-MAXSUB )
      JK1 = MAX( IBUF(2) , JK-1 , J-MAXSUB )
      IK2 = MIN( IBUF(3) , IK+1 , I+MAXSUB )
      JK2 = MIN( IBUF(4) , JK+1 , J+MAXSUB )
      TEMP = 0.0
      ITMP = 0
      JOFF = IBUF(2) - 1
      DO 10 L = JK1 , JK2
          JOF = JAPY(L-JOFF)
          DO 20 K = IK1 , IK2
              IF ( MAS(K-I,L-J) .EQ. -1 ) THEN
                  GOOD = .FALSE.
                  RETURN
              ELSE
                  TEMP = TEMP + A(JOF+K)
                  ITMP = ITMP + 1
              ENDIF
   20     CONTINUE
   10 CONTINUE
      IF ( ITMP .GT. 0 ) THEN
          AVER = TEMP / ITMP
      ELSE
          GOOD = .FALSE.
          RETURN
      ENDIF
C
      IF ( IK .GT. IBUF(1) .AND. JK .GT. IBUF(2) .AND.
     &                   IK .LT. IBUF(3) .AND. JK .LT. IBUF(4) ) THEN
          CALL FLTRBP( A , JAPY , IBUF , IK , JK , BGRD , FLTR , AVER )
      ELSE
          GOOD = .FALSE.
          RETURN
      ENDIF
C
      IF ( ( A(JAPY(JK-JOFF)+IK) - AVER ) .GT. FLTR*(AVER-BGRD) ) THEN
          GOOD = .FALSE.
          MAS(IK-I,JK-J) = 0
      ELSE IF ( AVER .LT. BGRD+TRSH ) THEN
          GOOD = .FALSE.
      ENDIF
C
      RETURN
C
      END
