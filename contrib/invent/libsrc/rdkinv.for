C===========================================================================
C Copyright (C) 1982-2010 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine RDKEYINV       version 4         831012
C  Ch. Ounnas                ESO - GARCHING
C  from subroutine RDKWRD    version 2         820618
C  A. Kruszewski             ESO Garching
C  renamed to RDKINV         version 5.0       870803
C  A. Kruszewski             ESO Garching
C.PURPOSE
C  reads keywords INV_INTG and INV_REAL and stores them
C  in arrays "IARR" and "RARR"
C.INPUT/OUTPUT
C  output arguments
C  IARR        integer*4 array     values of integer keywords
C  RARR        real*4 array        values of real keywors
C  ISTAT       integer*4           status
C
C
C  IARR(1)  - DEBUG is used to invoke debuging outputs.
C  IARR(2)  - ANALITER sets number of additional iterations in ANALYSE.
C  IARR(3)  - TBLWRITE controls writing an output table.
C  IARR(4)  - VERIFY switches between VERIFY and NOVERIFY options.
C  IARR(5)  - ISOPHOTR activates the use of fixed isophotal radii.
C  IARR(6)  - OUPROFIL gives number of profile points in output table.
C  IARR(7)  - PHYSICAL tells if searched area is defined by
C                     physical coordinates.
C  IARR(8)  - HALFEDGE sets the size of local subarray.
C  IARR(9)  - FIELDVAR 0 for additive and 1 for multipl. field 
C                     variations.
C  IARR(10) - UNITS switches between background 0 and instrument
C                   units 1.
C  IARR(11) - PRFLCTRL number of standard profile points updated.
C  IARR(12) - IARR(15) - IJBORDER sets limits of investigated area.
C  IARR(16) - SKYDETER sets accuracy of sky background
C                      determination.
C  IARR(17) - IARR(18) - ITERATE controls iterations in CLASSIFY.
C  IARR(19) - MARGINS size in pixels of unused margins.
C  IARR(20) - FULLPSF extend in pixels of 2-dim p.s.f.
C  IARR(21) - UNDRSMPL undersampling factor.
C  IARR(22) - BRGTCTRL controls removal of spurious detections.
C  IARR(23) - NETHEDGE half edge of regions used to determine preliminary
C                      sky background in a net of regions.
C
C  RARR(1)  - RARR(2) - LHCUTS
C  RARR(3)  - TRESHOLD
C  RARR(4)  - FILTER sets level of bright pixel filtering.
C  RARR(5)  - RARR(7) - CLASSPAR controls preliminary classification.
C  RARR(8)  - BRGHTLMT divides objects onto bright and faint ones
C                     for classification purposes.
C  RARR(9)  - DISTANCE limiting distance in the parameters space.
C  RARR(10) - EXPRTIME exposure time in seconds.
C  RARR(11) - ZEROMAGN calibrates magnitudes.
C  RARR(12) - RARR(13) - STMETRIC radii of two fixed apertures in pixels.
C  RARR(14) - RARR(18) - SPROFIL1 differential logarithmic p.s.f.
C  RARR(19) - RARR(23) - SPROFIL2      "            "        "
C  RARR(24) - RARR(28) - SPROFIL3      "            "        "
C  RARR(29) - RARR(33) - SPROFIL4      "            "        "
C  RARR(34) - RARR(38) - SPROFIL5      "            "        "
C  RARR(39) - CTRLMODE controls mode determination.
C  RARR(40) - CVFACTOR controls termination of mode determination.
C  RARR(41) - SGFACTOR sets clipping factor at mode determination.
C  RARR(42) - PAIRSPRT minimum allowable pair separation.
C  RARR(43) - MULTDTCT maximum separation of detections 
C                      of a single object.
C  RARR(44) - ELONGLMT minimum elongation of double object.
C  RARR(45) - YFACTOR ratio of dimensions of scanning rectangle.
C  RARR(46) - DFVALLEY minimal depth of separating two 
C                      components valley.
C  RARR(47) - BRIGHTOB controls cleaning of faint components.
C  RARR(48) - SKYSAMPL controls sampling of sky for background
C                      determination.
C  RARR(49) - RARR(51) - PRFLCLNG controls approximate cleaning.
C  RARR(52) - NEBLIMIT controls detection of extended objects.
C  RARR(53) - ETAFUNCT parameter at Petrosian quantities determination.
C  RARR(54) - RARR(57) - XYBORDER limits of investigated area
C                               in physical units.
C 
C.VERSION
C 100511		last modif
C 
C-----------------------------------------------------------------------
      SUBROUTINE RDKINV(IARR,RARR,ISTAT)
C
      IMPLICIT  NONE
      INTEGER   IARR(32)
      REAL      RARR(64) 
      INTEGER   ISTAT
C
      INTEGER   IACT
      INTEGER   KUN , KNUL
C
      DO 100 IACT=1,32 
         IARR(IACT) = 0
100   CONTINUE
      DO 200 IACT=1,64 
         RARR(IACT) = 0.0
200   CONTINUE
C 
      CALL STKRDI( 'INV_INTG' , 1 , 23 , IACT , IARR , KUN ,
     &                                          KNUL , ISTAT )
      CALL STKRDR( 'INV_REAL' , 1 , 57 , IACT , RARR , KUN ,
     &                                          KNUL , ISTAT )
C
      RETURN
C
      END
C
