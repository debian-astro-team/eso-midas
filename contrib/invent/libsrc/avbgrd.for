C @(#)avbgrd.for	19.1 (ES0-DMD) 02/25/03 13:25:31
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE AVBGRD(L0, L2, PMTR, CRMD, ABGRD)
C
      IMPLICIT  NONE
      INCLUDE   'MID_REL_INCL:INVENT.INC/NOLIST'
C
      INTEGER   L0
      INTEGER   L2
      REAL      PMTR(NRPAR,MAXCNT)
      REAL      CRMD(3)
      REAL      ABGRD
C
      INTEGER   L
      INTEGER   NOBJ
      REAL      SIGMA , TEMP(MAXCNT)
C
      NOBJ = L2 - L0
      DO 10 L = 1 , NOBJ
          TEMP(L) = PMTR(1,L)
   10 CONTINUE
C
      CALL MODE( TEMP , NOBJ , CRMD , ABGRD , SIGMA )
C
      RETURN
C
      END
C
