C @(#)ifstar.for	19.1 (ES0-DMD) 02/25/03 13:25:34
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE IFSTAR(IBUFS, SCA, LPXL, LSBP, IS ,
     &                  JS, MPRF, IPSF, FPSF, IOFF0,
     &                  IOFF, STAR, IOFFI)
      INTEGER   IBUFS(4)
      REAL      SCA
      INTEGER   LPXL
      INTEGER   LSBP
      INTEGER   IS
      INTEGER   JS
      INTEGER   MPRF
      INTEGER   IPSF(0:MPRF)
      REAL      FPSF(1)
      INTEGER   IOFF0
      INTEGER   IOFF
      LOGICAL   STAR
      INTEGER   IOFFI                    ! OUT: Corrections offset
C
      INTEGER   I , ICSP , IOF0 , ITMP , NADR , NOSP
      REAL      TEMP , TEMP1
      REAL      MSBP
C
C *** Check if standard star.
      IF ( (-LPXL) .GE. IBUFS(1)
     &                  .AND. (-LPXL) .GE. IBUFS(2)
     &                  .AND. LPXL .LE. IBUFS(3)
     &                  .AND. LPXL .LE. IBUFS(4) ) THEN
          STAR = .TRUE.
      ELSE
          STAR = .FALSE.
      ENDIF
C
      IF ( STAR ) THEN
          MSBP = 2 * LSBP + 1
C
C ***     NOSP - maximum number of standard stars.
C
          NOSP = IPSF(0)
C
C ***     Find offset to adresses of standard stars intensities.
C
          IOF0 = (5+NOSP) * IOFF +
     &           ( (LSBP+JS) * MSBP + LSBP + IS ) * NOSP
C
C ***     Calculate current number of this standard object.
C
          NADR = (LSBP+JS) * MSBP + LSBP + IS + 1 + NOSP
          ICSP = IPSF(NADR) + 1
          IF ( ICSP .GT. NOSP ) THEN
C
C ***         Look if present object is brighter than any
C ***         of previously recorded standard objects.
C
              TEMP = FPSF(IOF0+1) + 1.0
              ITMP = 0
              DO 40 I = 1 , NOSP
                  TEMP1 = FPSF(IOF0+I)
                  IF ( SCA .GT. TEMP1 ) THEN
                      IF ( TEMP1 .LT. TEMP ) THEN
                          TEMP = TEMP1
                          ITMP = I
                      ENDIF
                  ENDIF
   40         CONTINUE
              IF ( ITMP .GT. 0 .AND. ITMP .LE. NOSP ) THEN
C
C ***             Replace fainter object.
C
                  ICSP = ITMP
                  FPSF(IOF0+ICSP) = SCA
              ELSE
C
C ***             Is too faint.
C
                  STAR = .FALSE.
                  ICSP = ICSP - 1
              ENDIF
          ELSE
C
C ***         Add new standard star.
C
              IPSF(NADR) = ICSP
              FPSF(IOF0+ICSP) = SCA
          ENDIF
C
          IOFFI = IOFF0 + (4+ICSP) * IOFF
      ENDIF
C
      RETURN
C
      END
C
