C @(#)subpxl.for	19.1 (ES0-DMD) 02/25/03 13:25:40
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE SUBPXL ( NCT , PMT , LSBP , IACT , JACT , IS , JS )
C
      IMPLICIT  NONE
      INCLUDE   'MID_REL_INCL:INVENT.INC/NOLIST'
C
      INTEGER   NCT(NIPAR)                   ! IN: Integer parameters
      REAL      PMT(NRPAR)                   ! IN: Real parameters
      INTEGER   LSBP                         ! IN: Subpixel extend
      INTEGER   IACT                         ! OUT: X pixel number
      INTEGER   JACT                         ! OUT: Y pixel number
      INTEGER   IS                           ! OUT: X subpixel number
      INTEGER   JS                           ! OUT: Y subpixel number
C
      INTEGER   MSBP
C
      IACT = NCT(1)
      JACT = NCT(2)
C
      MSBP = 2 * LSBP + 1
      IS = NINT( ( PMT(10) - FLOAT(IACT) ) * FLOAT(MSBP) )
      IF ( IS .GT. LSBP ) THEN
          IS = LSBP
      ELSE IF ( IS .LT. (-LSBP) ) THEN
          IS = -LSBP
      ENDIF
      JS = NINT( ( PMT(11) - FLOAT(JACT) ) * FLOAT(MSBP) )
      IF ( JS .GT. LSBP ) THEN
          JS = LSBP
      ELSE IF ( JS .LT. (-LSBP) ) THEN
          JS = -LSBP
      ENDIF
C
      RETURN
C
      END
C
