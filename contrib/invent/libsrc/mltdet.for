C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine MLTDET         version 1         820519
C  A. Kruszewski             ESO Garching
C  modified by               version 1.1
C  A. Kruszewski             Obs. de Geneve    870224
C.KEYWORDS
C  search, detections
C.PURPOSE
C  identifies detections which are closer than "DLIM" pixels apart
C  multiple detections are marked by assigning the same
C  value of "MCAT(3,*)"
C.INPUT/OUTPUT
C  input arguments
C  MCAT        integer*4 array   integer parameters of detections
C  BCAT        real*4 array      real parameters of detections
C  NL          integer*4         number of entries in detections buffer
C  M           integer*4         number of detections
C  MOB         integer*4         detections buffer offset
C  AVI         real*4            average x-coordinate
C  AVJ         real*4            average y-coordinate
C  AVIJ        real*4            variance in x-coordinate
C  AVIJ        real*4            covariance of x and y coordinates
C  AVJJ        real*4            variance in y-coordinate
C  AVBG        real*4            average background
C  IM          integer*4         x-coordinate of maximal detection
C  JM          integer*4         y-coordinate of maximal detection
C  AVM         real*4            average of 9 central pixels
C                                for maximal detection
C  ISIZ        integer*4         maximal distance between detections
C  output arguments
C  MCAT        integer*4 array   integer parameters of detections
C  OBJECT      logical           object detection flag
C-----------------------------------------------------------------------
      SUBROUTINE MLTDET(MCAT, BCAT, NL, M, MOB, AVI, AVJ,
     &            AVII, AVIJ, AVJJ, AVBG, IM, JM, AVM, ISIZ, OBJECT)
C
      IMPLICIT  NONE 
      INTEGER   NL
      INTEGER   MCAT(4,NL)
      REAL      BCAT(2,NL)
      INTEGER   M
      INTEGER   MOB
      REAL      AVI
      REAL      AVJ
      REAL      AVII
      REAL      AVIJ
      REAL      AVJJ
      REAL      AVBG
      INTEGER   IM
      INTEGER   JM
      REAL      AVM
      INTEGER   ISIZ
      LOGICAL   OBJECT
C
      INTEGER   M1, M2, M3, M4
      INTEGER   MB, MMT, MOT, MS
C
      REAL      AV
      REAL      BG
      REAL      SBG, SI, SII, SIJ, SJ, SJJ, SUM
      REAL      TMP
      REAL      X1, X11, X12, X2, X21, X22, XSIZ
C
      M1 = 0
      M2 = 0
      M3 = 0
      BG = 0.0
      AV = 0.0
C 
      MB = MOD( M-1 , NL ) + 1
      MS = M - MB
      IF ( MOB .GT. MS ) THEN
          MOT = MOB - MS
          M1 = MCAT(1,MOT)
          M2 = MCAT(2,MOT)
          M3 = MCAT(3,MOT)
          BG = BCAT(1,MOT)
          AV = BCAT(2,MOT)
c      ELSE
c          READ ( ISF , REC=MOB ) M1 , M2 , M3 , M4 , BG , AV
      ENDIF
      X1 = FLOAT(M1)
      X2 = FLOAT(M2)
      X11 = X1
      X12 = X1
      X21 = X2
      X22 = X2
      IM = M1
      JM = M2
      AVM = 0.0
      SBG = 0.0
      SUM = 0.0
      SI = 0.0
      SJ = 0.0
      SII = 0.0
      SIJ = 0.0
      SJJ = 0.0
      M4 = M3
  10      CONTINUE
c      IF ( M4 .GT. MS ) THEN
          MMT = M4 - MS
          M1 = MCAT(1,MMT)
          M2 = MCAT(2,MMT)
          X1 = FLOAT( M1 )
          X2 = FLOAT( M2 )
          M4 = MCAT(4,MMT)
          BG = BCAT(1,MMT)
          AV = BCAT(2,MMT)
c      ELSE
c          READ ( ISF , REC=M4 ) M1 , M2 , M3 , M4 , BG , AV
c          X1 = FLOAT( M1 )
c          X2 = FLOAT( M2 )
c      ENDIF
      TMP = AV - BG
      SUM = SUM + TMP
      SI = SI + X1 * TMP
      SJ = SJ + X2 * TMP
      SII = SII + X1*X1 * TMP
      SIJ = SIJ + X1*X2 * TMP
      SJJ = SJJ + X2*X2 * TMP
      SBG = SBG + BG * TMP
      IF ( AV .GT. AVM ) THEN
          AVM = AV
          IM = M1
          JM = M2
      ENDIF
      IF ( X1 .LT. X11 ) THEN
          X11 = X1
      ENDIF
      IF ( X1 .GT. X12 ) THEN
          X12 = X1
      ENDIF
      IF ( X2 .LT. X21 ) THEN
          X21 = X2
      ENDIF
      IF ( X2 .GT. X22 ) THEN
          X22 = X2
      ENDIF 
      IF ( M4 .NE. 0 ) THEN
          GOTO 10
      ENDIF
      IF ( SUM .GT. 0.0 ) THEN
          AVI = SI / SUM
          AVJ = SJ / SUM
          AVII = SII / SUM
          AVII = AVII - AVI * AVI
          IF ( AVII .LT. 0.0 ) THEN
            AVII = 0.0
          ENDIF
          AVIJ = SIJ / SUM
          AVIJ = AVIJ - AVI * AVJ
          AVJJ = SJJ / SUM
          AVJJ = AVJJ - AVJ * AVJ
          IF ( AVJJ .LT. 0.0 ) THEN
            AVJJ = 0.0
          ENDIF
          AVBG = SBG / SUM
          XSIZ = ( (X12-X11)*(X12-X11) + (X22-X21)*(X22-X21) )
          IF ( XSIZ .GT. 0.0 ) THEN
            XSIZ = SQRT( XSIZ )
          ELSE
            XSIZ = 0.0
          ENDIF
          ISIZ = NINT( XSIZ )
          OBJECT = .TRUE.
      ELSE
          OBJECT = .FALSE.
      ENDIF
C
      RETURN
C
      END

