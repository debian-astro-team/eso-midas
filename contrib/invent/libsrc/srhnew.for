C @(#)srhnew.for	19.1 (ES0-DMD) 02/25/03 13:25:39
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE SRHNEW(AS, MAS, JAPYS, IBUFS, LHED,
     &                  II, JJ, TRSH, FLTR, BGRD,
     &                  SIGMA, MCM)
C
      IMPLICIT   NONE
      INCLUDE    'MID_REL_INCL:INVENT.INC/NOLIST'
C
      REAL        AS(1)                 ! IN: Subarray with subtracted objects
      INTEGER     MAS((-MAXSUB):MAXSUB,(-MAXSUB):MAXSUB) ! IN: Mask subarray
      INTEGER     JAPYS(1)              ! IN: Array with pointers to lines
      INTEGER     IBUFS(4)              ! IN: Limits of usable data region
      INTEGER     LHED                  ! IN: Half-size of data subarray
      INTEGER     II                    ! IN: X position of subarray center
      INTEGER     JJ                    ! IN: Y position of subarray center
      REAL        TRSH                  ! IN: Detection threshold
      REAL        FLTR                  ! IN: Filtering threshold
      REAL        BGRD                  ! IN: Local sky background
      REAL        SIGMA                 ! IN: Sigma of pixel data
      INTEGER     MCM(21)               ! MOD: New objects coordinates
C
      REAL        ALIM, AOLD, AVER
      INTEGER     IARG
      INTEGER     I, I1, I2, III
      INTEGER     J, J1, J2, JJJ
      INTEGER     JOFF, JOF
      INTEGER     KM
      LOGICAL     DETECT , GOOD
C
C *** Set local detection limit.
C
      ALIM = MAX( TRSH , 2.0*SIGMA ) + BGRD 
C
C *** Determine region to be searched for new objects.
C
      I1 = MAX( (-LHED) , IBUFS(1) ) + 1
      I2 = MIN( LHED , IBUFS(3) ) - 1
      J1 = MAX( (-LHED) , IBUFS(2) ) + 1
      J2 = MIN( LHED , IBUFS(4) ) - 1
      JOFF = IBUFS(2) - 1
      AOLD = 0.0
C
C *** Start search.
C
      DO 10 J = J1 , J2
          JOF = JAPYS(J-JOFF)
          DO 20 I = I1 , I2
              DETECT = .FALSE.
              IARG = JOF + I
              AVER = AS(IARG)
C
C ***         Pixels lower than limit ALIM is no longer considered.
C
              IF ( AVER .LT. ALIM ) GOTO 20
C
C ***         Compare with eight neighbours.
C
              CALL SRHOBJ( AS , JAPYS , JOFF , I , J , DETECT , AVER )
              IF ( ( .NOT. DETECT ) .OR. AVER .LT. ALIM ) GOTO 20
              IF ( AVER .GT. AOLD ) THEN
                  CALL FLTRBP( AS , JAPYS , IBUFS , I , J ,
     &                         BGRD , FLTR , AVER )
                  IF ( AVER .GT. AOLD ) THEN
                      AOLD = AVER
                      III = I
                      JJJ = J
                  ENDIF
              ENDIF
   20     CONTINUE
   10 CONTINUE
C
      IF ( AOLD .GT. ALIM ) THEN
C
C ***     Check if good object.
C
          III = III + II
          JJJ = JJJ + JJ
          CALL IFGOOD( AS , JAPYS , IBUFS , MAS , II ,
     &                 JJ , III , JJJ , BGRD , FLTR ,
     &                 TRSH , GOOD )
          IF ( GOOD ) THEN
C
C ***         Write a new object into array MCM.
C
              KM = MCM(1)
              IF ( KM .GE. 10 ) THEN
                  KM = 9
              ENDIF
              MCM(2*(KM+1)) = III
              MCM(2*(KM+1)+1) = JJJ
              MCM(1) = KM + 1
          ENDIF
      ENDIF
C
      RETURN
C
      END
