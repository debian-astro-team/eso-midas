C @(#)trsfrm.for	19.1 (ES0-DMD) 02/25/03 13:25:40
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C.IDENTIFICATION
C  subroutine TRSFRM         version 1.2        870729
C  A. Kruszewski             ESO Garching
C.AUTHOR: A.KRUSZEWSKI
C.PURPOSE
C.LANGUAGE: F77+ESOext
C.INPUT/OUTPUT
C  input arguments
C  PMTR        real*4 array      array holding classifiers
C  M           integer*4         number of objects
C                                of stallar objects
C  output arguments
C  PCLA        real*4 array      array holding corrected classifiers
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  10:07 - 22 NOV 1987
C
C-----------------------------------------------------------------------
      SUBROUTINE TRSFRM(PMTR,PCLA,M)
C
      IMPLICIT NONE
      INCLUDE 'MID_REL_INCL:INVENT.INC/NOLIST' 
C
      REAL     PMTR(30,MAXCNT)
      REAL     PCLA(0:13,MAXCNT)
      INTEGER  M
C
      INTEGER  I
      REAL     TEMP
C
C  ****    Transforms some parameters in order to make them
C	   more independent.
C
      DO 10 I = 1,M
          TEMP   = PMTR(2,I)
          IF (TEMP.LT.0.0001) THEN
              TEMP   = 0.0001
          END IF

          PCLA(0,I) = 19 - 2.5*LOG10(TEMP)
          PCLA(1,I) = 10.0*PMTR(3,I)
          PCLA(2,I) = PMTR(4,I)
          PCLA(3,I) = PCLA(3,I) - PCLA(0,I)
          TEMP   = PCLA(4,I)
          IF (TEMP.GT.1.0) THEN
              PCLA(4,I) = LOG(TEMP)

          ELSE
              PCLA(4,I) = 0.0
          END IF

          TEMP   = PCLA(5,I)
          IF (TEMP.GT.1.0) THEN
              PCLA(5,I) = LOG(TEMP)

          ELSE
              PCLA(5,I) = 0.0
          END IF

          PCLA(6,I) = PMTR(8,I)
          PCLA(7,I) = PMTR(12,I) - PCLA(0,I)
          PCLA(8,I) = PMTR(13,I) - PCLA(0,I)
          TEMP   = PCLA(9,I)
          IF (TEMP.GT.1.0) THEN
              PCLA(9,I) = LOG(TEMP)

          ELSE
              PCLA(9,I) = 0.0
          END IF

          PCLA(10,I) = PMTR(16,I) - PCLA(0,I)
          TEMP   = PMTR(17,I)
          IF (TEMP.GT.1.0) THEN
              PCLA(11,I) = LOG(TEMP)

          ELSE
              PCLA(11,I) = 0.0
          END IF

          TEMP   = PCLA(12,I)
          IF (TEMP.GT.0.1) THEN
              PCLA(12,I) = LOG(TEMP)

          ELSE
              PCLA(12,I) = -1.0
          END IF

          PCLA(13,I) = PMTR(19,I)
   10 CONTINUE
      RETURN

      END

