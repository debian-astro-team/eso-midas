C @(#)objmng.for	19.1 (ES0-DMD) 02/25/03 13:25:36
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine OBJMNG         version 1         820520
C  A. Kruszewski             ESO Garching
C  modified by               version 2         830905
C  Ch. Ounnas                ESO - Garching
C  modified to FX            version 2.1       870219
C  A. Kruszewski             Obs. de Geneve
C.PURPOSE
C  writes data into arrays NCAT and ACAT
C.INPUT/OUTPUT
C  input arguments
C  A           real*4 array      image frame
C  NPIX        integer*4 array   dimensions of an array "A"
C  ACAT        real*4 array      real parameters of detected objects
C  ICAT        integer*4 array   array holding positions of independent
C                                detections of the same object
C  M           integer*4         identification number of considered
C                                object
C  INN         integer*4         number of independent detections
C                                of the same object
C  IAV         integer*4         average X-coordinate in pixels
C  JAV         integer*4         average Y-coordinate in pixels
C  BGRD        real*4            average background
C  output arguments
C  ACAT        real*4 array      real parameters of detected objects
C  MM          integer*4         new identification number of an object
C-----------------------------------------------------------------------
C
      SUBROUTINE OBJMNG ( A , JAPY , IBUF , ACAT , NC ,
     &                    MM , MMB , IAV , JAV , BGRD ,
     &                    TRSH , GCUT , MINCR )
C
      IMPLICIT     NONE
      INTEGER      NC, ISTAT
      INTEGER      IARG , IAV , IBUF(4)
C     INTEGER      ITF
      INTEGER      JAPY(1) , JAV , JOF , JOFF
      INTEGER      K , L , MINCR , MM , MMB
      REAL         A(1) , ACAT(5,NC) , BGRD , TMP , TRLM , TRSH , GCUT
      CHARACTER*80 TEXT
C
      JOFF = IBUF(2) - 1
      IF ( MMB .EQ. NC ) THEN
          WRITE (TEXT,'(A)') 'Too many objects'
          CALL STTPUT(TEXT,ISTAT)
c          CALL CTLG( ITF , START , STEP , ACAT , NC , MM , MMB )
c          MMB = 0
      ENDIF
      MMB = MMB + 1
      MM = MM + 1
      ACAT(1,MMB) = FLOAT( IAV )
      ACAT(2,MMB) = FLOAT( JAV )
      ACAT(3,MMB) = BGRD
      JOF = JAV - JOFF
      IF ( JOF .LT. 2 ) THEN
          JOF = 2
      ENDIF
      TMP = 0.0
      DO 10 L = -1 , 1
          IARG = JAPY(JOF+L) + IAV
          DO 20 K = -1 , 1
            TMP = TMP + A(IARG+K)
   20          CONTINUE
   10      CONTINUE
        ACAT(4,MMB) = TMP / 9.0
      TRLM = BGRD + TRSH
      IF ( ACAT(4,MMB) .LE. TRLM ) THEN
          MMB = MMB - 1
          MM = MM - 1
      ELSE
          CALL RADDET( A , JAPY , IBUF , IAV , JAV ,
     &                 MINCR , TRLM , ACAT(4,MMB) , TRSH , ACAT(5,MMB) )
          IF ( ACAT(5,MMB) .LE. 0.0 .AND. ACAT(4,MMB) .LT. GCUT ) THEN
            MMB = MMB - 1
            MM = MM - 1
          ENDIF
      ENDIF
C
      RETURN
C
      END
C
