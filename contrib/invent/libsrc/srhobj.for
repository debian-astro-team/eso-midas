C @(#)srhobj.for	19.1 (ES0-DMD) 02/25/03 13:25:39
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine SRHOBJ         version 1         820506
C  A. Kruszewski             ESO Garching
C  A. Lauberts                                 840511
C  modified to FX            version 1.1       870219
C  A. Kruszewski             Obs. de Geneve
C  modified to MSDOS         version 1.2       880522
C  A. Kruszewski             Warsaw U. Obs.
C.KEYWORDS
C  search
C.PURPOSE
C  Compares pixel of coordinates I,J with its eight closest neighbours.
C  Sets DETECT to .TRUE. when pixel I,J is equal or higher than
C  any of its eight neighbours.
C  Calculates average value of all nine pixels if DETECT is .TRUE.
C.INPUT/OUTPUT
C  input arguments:
C  A           real*4 array        image buffer
C  JAPY        integer*4 array     array of line pointers
C  JOFF        integer*4           line number offset
C  I           integer*4           x-pixel coordinate
C  J           integer*4           y-pixel coordinate
C  AV          real*4              value of I,J pixel
C  output arguments:
C  DETECT      logical*4           object detection flag
C  AV          real*4              average of 9 pixels centered on I,J
C-----------------------------------------------------------------------
      SUBROUTINE SRHOBJ(A, JAPY, JOFF, I, J, DETECT, AV)
C
      IMPLICIT  NONE
      INTEGER   I , II
      INTEGER   J , JAPY(1) , JOF , JOFF
      REAL      A(1) , AV , TEMP , SUM
      LOGICAL    DETECT
C
C ******      Compare pixel I,J with 8 neighbours.
C
      SUM = AV
      DETECT = .FALSE.
      JOF = JAPY(J-JOFF-1) + I
      DO 10 II = -1 , 1
          TEMP = A(JOF+II)
          IF ( TEMP .GT. AV ) RETURN
          SUM = SUM + TEMP
   10      CONTINUE
      JOF = JAPY(J-JOFF) + I
      DO 20 II = -1 , 1 , 2
          TEMP = A(JOF+II)
          IF ( TEMP .GT. AV ) RETURN
          SUM = SUM + TEMP
   20      CONTINUE
      JOF = JAPY(J-JOFF+1) + I
      DO 30 II = -1 , 1
          TEMP = A(JOF+II)
          IF ( TEMP .GT. AV ) RETURN
          SUM = SUM + TEMP
   30      CONTINUE
C
C ******      Central pixel is the maximum value.
C
      DETECT = .TRUE.
C
C ******      Calculates average AV of pixels on a 3x3
C ******      matrix which is centered on pixel I,J.
C
      AV = SUM / 9.0
C
      RETURN
C
      END 
