$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.INVENT.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:53 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   adjpsf.for
$ FORTRAN   appmtr.for
$ FORTRAN   aprbgr.for
$ FORTRAN   avbgrd.for
$ FORTRAN   averpr.for
$ FORTRAN   calcul.for
$ FORTRAN   cfaint.for
$ FORTRAN   clasfy.for
$ FORTRAN   clbrgt.for
$ FORTRAN   clpmtr.for
$ FORTRAN   cmpsub.for
$ FORTRAN   corpsf.for
$ FORTRAN   cpcntr.for
$ FORTRAN   ctable.for
$ FORTRAN   deflst.for
$ FORTRAN   defpsf.for
$ FORTRAN   double.for
$ FORTRAN   extrem.for
$ FORTRAN   fdtotd.for
$ FORTRAN   filbuf.for
$ FORTRAN   flstbd.for
$ FORTRAN   fltrbp.for
$ FORTRAN   fppmtr.for
$ FORTRAN   fsgrnt.for
$ FORTRAN   getlst.for
$ FORTRAN   gradet.for
$ FORTRAN   ifgood.for
$ FORTRAN   ifnear.for
$ FORTRAN   ifstar.for
$ FORTRAN   inapsf.for
$ FORTRAN   inilst.for
$ FORTRAN   intdet.for
$ FORTRAN   invert.for
$ FORTRAN   joinmd.for
$ FORTRAN   lmtdet.for
$ FORTRAN   lsqsol.for
$ FORTRAN   mean.for
$ FORTRAN   median.for
$ FORTRAN   mltdet.for
$ FORTRAN   mltobj.for
$ FORTRAN   mode.for
$ FORTRAN   modpsf.for
$ FORTRAN   newobj.for
$ FORTRAN   nrdist.for
$ FORTRAN   nrmleq.for
$ FORTRAN   objmng.for
$ FORTRAN   objshp.for
$ FORTRAN   octfrr.for
$ FORTRAN   pntspf.for
$ FORTRAN   pranlz.for
$ FORTRAN   prfcnv.for
$ FORTRAN   prflch.for
$ FORTRAN   prflcl.for
$ FORTRAN   profil.for
$ FORTRAN   putlss.for
$ FORTRAN   radcor.for
$ FORTRAN   raddet.for
$ FORTRAN   rdcrin.for
$ FORTRAN   rdkinv.for
$ FORTRAN   renmbr.for
$ FORTRAN   rlbias.for
$ FORTRAN   rlgrnt.for
$ FORTRAN   rmflst.for
$ FORTRAN   satbad.for
$ FORTRAN   satobj.for
$ FORTRAN   sbgnet.for
$ FORTRAN   scldet.for
$ FORTRAN   search.for
$ FORTRAN   skymod.for
$ FORTRAN   smtpsf.for
$ FORTRAN   srhnew.for
$ FORTRAN   srhobj.for
$ FORTRAN   starsa.for
$ FORTRAN   stgrnt.for
$ FORTRAN   stseed.for
$ FORTRAN   subpxl.for
$ FORTRAN   trsfrm.for
$ FORTRAN   twodim.for
$ FORTRAN   updtl.for
$ FORTRAN   updtll.for
$ LIB/REPLACE libinvent adjpsf.obj,appmtr.obj,aprbgr.obj,avbgrd.obj,averpr.obj,calcul.obj,cfaint.obj,clasfy.obj,clbrgt.obj,clpmtr.obj,cmpsub.obj,corpsf.obj,cpcntr.obj
$ LIB/REPLACE libinvent ctable.obj,deflst.obj,defpsf.obj,double.obj,extrem.obj,fdtotd.obj,filbuf.obj,flstbd.obj,fltrbp.obj,fppmtr.obj,fsgrnt.obj,getlst.obj,gradet.obj,ifgood.obj
$ LIB/REPLACE libinvent ifnear.obj,ifstar.obj,inapsf.obj,inilst.obj,intdet.obj,invert.obj,joinmd.obj,lmtdet.obj,lsqsol.obj,mean.obj,median.obj,mltdet.obj,mltobj.obj,mode.obj
$ LIB/REPLACE libinvent modpsf.obj,newobj.obj,nrdist.obj,nrmleq.obj,objmng.obj,objshp.obj,octfrr.obj,pntspf.obj,pranlz.obj,prfcnv.obj,prflch.obj,prflcl.obj,profil.obj,putlss.obj
$ LIB/REPLACE libinvent radcor.obj,raddet.obj,rdcrin.obj,rdkinv.obj,renmbr.obj,rlbias.obj,rlgrnt.obj,rmflst.obj,satbad.obj,satobj.obj,sbgnet.obj,scldet.obj,search.obj,skymod.obj
$ LIB/REPLACE libinvent smtpsf.obj,srhnew.obj,srhobj.obj,starsa.obj,stgrnt.obj,stseed.obj,subpxl.obj,trsfrm.obj,twodim.obj,updtl.obj,updtll.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
