C @(#)corpsf.for	19.1 (ES0-DMD) 02/25/03 13:25:32
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE CORPSF(LPXL, LSBP, LL, CPSF, IPSF,
     &                  CRMD, BPSF, DPSF)
C
      IMPLICIT  NONE
      INCLUDE   'MID_REL_INCL:INVENT.INC/NOLIST'
C
      INTEGER   LPXL
      INTEGER   LSBP
      INTEGER   LL
      REAL      CPSF((-LPXL):LPXL,(-LPXL):LPXL,(-LSBP):LSBP,
     &               (-LSBP):LSBP,NOSP)
      INTEGER   IPSF((-LSBP):LSBP,(-LSBP):LSBP)
      REAL      CRMD(3)
      REAL      BPSF((-LL):LL,(-LL):LL)
      REAL      DPSF((-LL):LL,(-LL):LL)
C
      INTEGER   I, II
      INTEGER   IT, IK
      INTEGER   ISTAT
      INTEGER   ITMP
      INTEGER   J
      INTEGER   JL
      INTEGER   K
      INTEGER   L
C
      REAL      AIT
C     REAL      SIGMA
C
      REAL   TEMP(NOSP)
C
      CHARACTER*80   OUTPUT
C
      IT = 0
      DO 10 L = -LSBP , LSBP
          DO 20 K = -LSBP , LSBP
              ITMP = IPSF(K,L)
              IT = IT + ITMP
              DO 30 J = -LPXL , LPXL
                  JL = J * (2*LSBP+1) - L
                  DO 40 I = -LPXL , LPXL
                      IK = I * (2*LSBP+1) - K
                      DO 50 II = 1 , ITMP
                          TEMP(II) = CPSF(I,J,K,L,II)
   50                 CONTINUE
                      IF ( ITMP .GT. 2 ) THEN
                          CALL MODE( TEMP , ITMP , CRMD , BPSF(IK,JL) ,
     &                               DPSF(IK,JL) )
                          IF ( ITMP .EQ. 3 ) THEN
                              DPSF(IK,JL) = 1.5 * DPSF(IK,JL)
                          ENDIF
                      ELSE IF ( ITMP .EQ. 2 ) THEN
                          BPSF(IK,JL) = ( TEMP(1) + TEMP(2) ) / 2.0
                          DPSF(IK,JL) = MAX(0.1,ABS(TEMP(1)-TEMP(2)))
                      ELSE IF ( ITMP .EQ. 1 ) THEN
                          BPSF(IK,JL) = TEMP(1)
                          DPSF(IK,JL) = 1.0
                      ELSE
                          BPSF(IK,JL) = 0.0
                          DPSF(IK,JL) = 10.0
                      ENDIF
   40             CONTINUE
   30         CONTINUE
   20     CONTINUE
   10 CONTINUE
      AIT = FLOAT(IT) / FLOAT((2*LSBP+1)**2)
      WRITE( OUTPUT , '(A,A,F7.2)' ) 'Average number of standard stars '
     &               ,'per subpixel =',AIT
      CALL STTPUT( OUTPUT , ISTAT )
C
      RETURN
C
      END
