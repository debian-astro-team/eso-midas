C @(#)intdet.for	19.1 (ES0-DMD) 02/25/03 13:25:35
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.INPUT/OUTPUT
C  Input parameters
C  AVPR     real*4 array        average object profile
C  IAVR     integer*4 array     valid points in AVPR
C  APSF     real*4 array        one dimensional p.s.f.
C  LIM      integer*4           object size
C  Output parameters
C  AVER     real*4              average intensity of 9 central pixels
C-----------------------------------------------------------------------
      SUBROUTINE INTDET(AVPR, IAVR, APSF, LIM, AVER)
C
      IMPLICIT  NONE
C
      INTEGER   LIM
      REAL      AVPR(0:LIM)
      INTEGER   IAVR(0:LIM)
      REAL      APSF(0:LIM)
      REAL      AVER
C
      INTEGER   K
      REAL      TEMP
C
      IF ( IAVR(0) .GT. 0 .AND. IAVR(1) .GT. 0 ) THEN
            AVER = ( AVPR(0) + 8.0 * AVPR(1) ) / 9.0
      ELSE
            AVER = 0.0
            TEMP = ( APSF(0) + 8.0 * APSF(1) ) / 9.0
            K = 0
   10            CONTINUE
               IF ( IAVR(K) .GT. 0 .AND. APSF(K) .GT. 0.0 ) THEN
                  AVER = AVPR(K) * TEMP / APSF(K)
            ELSE IF ( K .LT. LIM ) THEN
                  K = K + 1
                  GOTO 10
            ENDIF
      ENDIF
C
      RETURN
      END

