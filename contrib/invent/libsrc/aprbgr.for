C @(#)aprbgr.for	19.1 (ES0-DMD) 02/25/03 13:25:31
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine APRBGR         version 1         820507
C  A. Kruszewski             ESO Garching
C  modified for FX           version 1.1       870219
C  A. Kruszewski             Obs. de Geneve
C  modified for MSDOS        version 1.2       880522
C.KEYWORDS
C  background
C.PURPOSE
C  calculates approximate value of local sky background
C.ALGORITHM
C  averages of 9 pixels are calculated in four corners
C  of a detection square
C  second smallest of these averages is accepted as background
C.INPUT/OUTPUT
C  input arguments
C  A           real*4 array         image buffer
C  JAPY        integer*4 array      array of line pointers
C  JOFF        integer*4            line number offset
C  JXY         integer*4 array      limits of subarray
C  output arguments
C  SB2         real*4            approximate background
C-----------------------------------------------------------------------
      SUBROUTINE APRBGR(A, JAPY, JOFF, JXY, SB2)
C
      IMPLICIT NONE
C
      REAL      A(1)
      INTEGER   JAPY(1)
      INTEGER   JOFF
      INTEGER   JXY(4)
      REAL      SB2
C
      INTEGER   J1, J2, JJ, JJJ, JOF
      REAL      AA , AVER(4) 
      REAL      SUM
      INTEGER   I1 , I2 , II , III , IJ(2,4) , ITMP
      INTEGER   K
C
C   
C ******      Averages of four 3x3 arrays of pixels are obtained.
C ******      These arrays are situated in corners of a bigger
C ******      array defined by its borders stored in array JXY.
C
      IJ(1,1) = JXY(1) + 1
      IJ(2,1) = JXY(2) + 1
      IJ(1,2) = JXY(3) - 1
      IJ(2,2) = JXY(2) + 1
      IJ(1,3) = JXY(1) + 1
      IJ(2,3) = JXY(4) - 1
      IJ(1,4) = JXY(3) - 1
      IJ(2,4) = JXY(4) - 1
      DO 10 K = 1 , 4
          SUM = 0.0
          ITMP = IJ(2,K)
          J1 = ITMP - 1
          J2 = ITMP + 1
          DO 20 JJ = J1 , J2
            JOF = JAPY(JJ-JOFF)
            ITMP = IJ(1,K)
            I1 = ITMP - 1
            I2 = ITMP + 1
            DO 30 II = I1 , I2
                SUM = SUM + A(JOF+II)
   30            CONTINUE
   20          CONTINUE
            AVER(K) = SUM / 9.0
   10      CONTINUE
C
C ******      Find second smallest value of array AVER.
C
      DO 11 JJJ = 2 , 3
          DO 12 III = JJJ , 4
            IF ( AVER(III-1) .LT. AVER(III) ) THEN
                AA = AVER(III-1)
                AVER(III-1) = AVER(III)
                AVER(III) = AA
            ENDIF
   12          CONTINUE
   11      CONTINUE
C
      SB2 = AVER(3)
C
      RETURN
C
      END
