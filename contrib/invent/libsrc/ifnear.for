C @(#)ifnear.for	19.1 (ES0-DMD) 02/25/03 13:25:34
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C
C
C-----------------------------------------------------------------------
      SUBROUTINE IFNEAR(NREG, LSTP, IREG, I, J, LDIS, NEAR)
C
      IMPLICIT  NONE
      INTEGER   NREG
      INTEGER   LSTP(0:4,0:NREG)
      INTEGER   IREG
      INTEGER   I
      INTEGER   J
      INTEGER   LDIS
      LOGICAL   NEAR
C
      INTEGER   IDIS2
      INTEGER   LDIS2
C
      INTEGER   IR1, IR2, IR3, IR4
C
      NEAR = .FALSE.
      IR1 = LSTP(1,IREG)
      IF ( IR1 .GT. I + LDIS ) RETURN
      IR2 = LSTP(2,IREG)
      IF ( IR2 .GT. J + LDIS ) RETURN
      IR3 = LSTP(3,IREG)
      IF ( IR3 .LT. I - LDIS ) RETURN
      IR4 = LSTP(4,IREG)
      IF ( IR4 .LT. J - LDIS ) RETURN
c      IF ( IR1 .LE. I .AND. IR3 .GE. I .AND.
c     &                      IR2 .LE. J .AND. IR4 .GE. J ) THEN
c          NEAR = .TRUE.
c          RETURN
c      ENDIF
      IF ( IR1 .LE. I .AND. IR3 .GE. I ) THEN
          IF ( IR2 .GT. J+LDIS .OR. IR4 .LT. J-LDIS ) THEN
              NEAR = .FALSE.
              RETURN
          ELSE
              NEAR = .TRUE.
              RETURN
          ENDIF
      ENDIF
      IF ( IR2 .LE. J .AND. IR4 .GE. J ) THEN
          IF ( IR1 .GT. I+LDIS .OR. IR3 .LT. I-LDIS ) THEN
              NEAR = .FALSE.
              RETURN
          ELSE
              NEAR = .TRUE.
              RETURN
          ENDIF
      ENDIF
      LDIS2 = LDIS * LDIS
      IF ( IR1 .GT. I .AND. IR2 .GT. J ) THEN
          IDIS2 = (IR1-I)*(IR1-I) + (IR2-J)*(IR2-J)
          IF ( IDIS2 .LE. LDIS2 ) THEN
              NEAR = .TRUE.
          ENDIF
          RETURN
      ELSE IF ( IR1 .GT. I .AND. IR4 .LT. J ) THEN
          IDIS2 = (IR1-I)*(IR1-I) + (J-IR4)*(J-IR4)
          IF ( IDIS2 .LE. LDIS2 ) THEN
              NEAR = .TRUE.
          ENDIF
          RETURN
      ELSE IF ( IR3 .LT. I .AND. IR2 .GT. J ) THEN
          IDIS2 = (I-IR3)*(I-IR3) + (IR2-J)*(IR2-J)
          IF ( IDIS2 .LE. LDIS2 ) THEN
              NEAR = .TRUE.
          ENDIF
          RETURN
      ELSE IF ( IR3 .LT. I .AND. IR4 .LT. J ) THEN
          IDIS2 = (I-IR3)*(I-IR3) + (J-IR4)*(J-IR4)
          IF ( IDIS2 .LE. LDIS2 ) THEN
              NEAR = .TRUE.
          ENDIF
          RETURN
      ENDIF
C
      RETURN
C
      END 
