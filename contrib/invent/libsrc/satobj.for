C @(#)satobj.for	19.1 (ES0-DMD) 02/25/03 13:25:39
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE SATOBJ(A, JAPY, JOFF, I, J, HCUT, AV)
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C-----------------------------------------------------------------------
      IMPLICIT NONE
C
      REAL       A(1)
      INTEGER    JAPY(1)
      INTEGER    JOFF
      INTEGER    I
      INTEGER    J
      REAL       HCUT
      REAL       AV
C
      INTEGER    II, JOF
      REAL       AA(9)
C
      JOF = JAPY(J-JOFF-1) + I
      AA(1) = MIN( A(JOF-1) , HCUT )
      AA(2) = MIN( A(JOF) , HCUT )
      AA(3) = MIN( A(JOF+1) , HCUT )
      JOF = JAPY(J-JOFF) + I
      AA(4) = MIN( A(JOF-1) , HCUT )
      AA(5) = MIN( A(JOF) , HCUT )
      AA(6) = MIN( A(JOF+1) , HCUT )
      JOF = JAPY(J-JOFF+1) + I
      AA(7) = MIN( A(JOF-1) , HCUT )
      AA(8) = MIN( A(JOF) , HCUT )
      AA(9) = MIN( A(JOF+1) , HCUT )
C
      AV = 0.0
      DO 10 II = 1 , 9
            AV = AV + AA(II)
  10      CONTINUE
C
        AV = AV / 9.0
C
      RETURN
      END
