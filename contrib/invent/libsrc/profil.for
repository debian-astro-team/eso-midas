C @(#)profil.for	19.1 (ES0-DMD) 02/25/03 13:25:37
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine PROFIL         version 2.1       830907
C  A. Kruszewski             ESO Garching
C  modified for FX           version 2.2       870303
C  A. Kruszewski             Obs. de Geneve
C.KEYWORDS
C  profiles
C.PURPOSE
C  calculates profile in eight octants for an object found in
C  an image frame
C.INPUT/OUTPUT
C  input arguments
C  A           real*4 array      image frame
C  N1          integer*4         x-dimansion of array A
C  N2          integer*4         y-dimension of array A
C  I           integer*4         x-coordinate of an object
C  J           integer*4         y-coordinate of an object
C  LIM         integer*4         radial extent of calculated profile
C                                expressed in pixels
C  LDBG        integer*4         output control
C  BGDR        real*4            local background
C  output arguments
C  PRFL        real*4 array      calculated profile in eight octants
C  ICNT        integer*4 array   number of pixels used in profile points
C-----------------------------------------------------------------------
      SUBROUTINE PROFIL(A, MA, JAPY, IBUF, I, J, LIM,
     &                  LDBG, BGRD, HCUT, PRFL, ICNT)
C
      IMPLICIT    NONE
      INCLUDE    'MID_REL_INCL:INVENT.INC/NOLIST'
C
      REAL        A(1)
      INTEGER     MA((-MAXSUB):MAXSUB,(-MAXSUB):MAXSUB)
      INTEGER     JAPY(1)
      INTEGER     IBUF(4)
      INTEGER     I
      INTEGER     J
      INTEGER     LIM
      INTEGER     LDBG
      REAL        BGRD
      REAL        HCUT
      REAL        PRFL(8,0:LIM)
      INTEGER     ICNT(8,0:LIM)
C
      INTEGER     IDD
      INTEGER     JOF, JOFF
      INTEGER     I1 , I2 , IADR , II , IVAL
      INTEGER     J1 , J2 , JJ , JM , K , L
      INTEGER     IMASK
      INTEGER     L1
C
      REAL        VAL
      REAL        X, XX
C
      INTEGER     IPRF(8)
C
C *** Set to zero data in arrays PRFL and ICNT.
C
      DO 10 L = 0 , LIM
          DO 11 K = 1 , 8
              PRFL(K,L) = 0.0
              ICNT(K,L) = 0
   11     CONTINUE
   10 CONTINUE
C
C *** Assign value to the central point of profile.
C
      JOFF = IBUF(2) - 1
      IADR = JAPY(J-JOFF) + I
      IMASK = MA(0,0)
      IF ( IMASK .GE. 1 ) THEN
          VAL = A(IADR) - BGRD
          IVAL = 1
      ELSE IF ( IMASK .EQ. -1 ) THEN
          VAL = HCUT - BGRD
          IVAL = -1
      ELSE
          VAL = 0.0
          IVAL = 0
      ENDIF
      DO 20 K = 1 , 8
          PRFL(K,0) = VAL
          ICNT(K,0) = IVAL
   20 CONTINUE
C
C *** Define subarray to be used.
C
      I1 = MAX( IBUF(1) , I-LIM )
      I2 = MIN( IBUF(3) , I+LIM )
      J1 = MAX( IBUF(2) , J-LIM )
      J2 = MIN( IBUF(4) , J+LIM )
C
C *** Start calculating profile.
C
      DO 30 JJ = J1 , J2
          JOF = JAPY(JJ-JOFF)
          JM = JJ - J
          DO 31 II = I1 , I2
C
C ***         Check if good pixel.
C
              IMASK = MA(II-I,JM)
              IF ( IMASK .EQ. 0 ) GOTO 31
              VAL = A(JOF+II)
C
C ***         Calculate ring number L.
C
              IDD = (J-JJ) * (J-JJ) + (I-II) * (I-II)
              XX = SQRT( FLOAT( IDD ) )
              X = 0.382683433 * XX
              L = NINT( XX )
              IF ( L .GT. 0 .AND. L .LE. LIM ) THEN
C
C ***             Searches for octant.
C
                  IF ( ABS(FLOAT(J-JJ)) .LT. X ) THEN
                      IF ( II .GT. I ) THEN
                          K = 1
                      ELSE
                          K = 5
                      ENDIF
                  ELSE IF ( ABS(FLOAT(I-II)) .LT. X ) THEN
                      IF ( JJ .GT. J ) THEN
                          K = 3
                      ELSE
                          K = 7
                      ENDIF
                  ELSE IF ( II-I .GT. 0 ) THEN
                      IF ( JJ-J .GT. 0 ) THEN
                          K = 2
                      ELSE
                          K = 8
                      ENDIF
                  ELSE
                      IF ( JJ-J .GT. 0 ) THEN
                          K = 4
                      ELSE
                          K = 6
                      ENDIF
                  ENDIF
                  IF ( IMASK .GE. 1 ) THEN
                      IF ( ICNT(K,L) .GT. 0 ) THEN
                          PRFL(K,L) = PRFL(K,L) + VAL
                          ICNT(K,L) = ICNT(K,L) + 1
                      ELSE IF ( ICNT(K,L) .EQ. 0 ) THEN
                          PRFL(K,L) = VAL
                          ICNT(K,L) = 1
                      ENDIF
                  ELSE IF ( IMASK .EQ. -1 ) THEN
                      ICNT(K,L) = -1
                  ENDIF
              ENDIF
   31     CONTINUE
   30 CONTINUE
C
C *** Calculate profile.
C
      DO 40 L = 1 , LIM
          DO 41 K = 1 , 8
              IF ( ICNT(K,L) .GT. 0 ) THEN
                  PRFL(K,L) = PRFL(K,L) / ICNT(K,L) - BGRD
              ELSE IF ( ICNT(K,L) .EQ. -1 ) THEN
                  PRFL(K,L) = HCUT - BGRD
              ELSE
                  PRFL(K,L) = 0.0
              ENDIF
   41     CONTINUE
   40 CONTINUE
C
      IF ( LDBG .GE. 3 ) THEN
C
C ***     Write profile into screen.
C
          L1 = MIN( LIM , 10 )
          WRITE (*,*) 'Eight octants pixel spaced profile'
          DO 50 L = 0 , L1
              DO 60 K = 1 , 8
                  IPRF(K) = INT( 1000.*PRFL(K,L) / BGRD )
   60         CONTINUE
              WRITE (*,'(8I9)') IPRF 
   50     CONTINUE
      ENDIF
C
      RETURN
C
      END
