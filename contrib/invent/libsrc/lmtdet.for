C @(#)lmtdet.for	19.1 (ES0-DMD) 02/25/03 13:25:35
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  Subroutine LMTDET          version 1.0          870615
C  A. Kruszewski              ESO Garching
C.PURPOSE
C  Sets a region around current detection to be searched for other
C  detections
C.INPUT/OUTPUT
C  Input parameters
C  DLIM        real*4                  maximum separation between
C                                      different detections to belong
C                                      to the same object
C  ILIM        integer*4               the same but integer
C  Output parameters
C  LMAR        integer*4 array         array with region limits
C-----------------------------------------------------------------------
      SUBROUTINE LMTDET(ILIM, DLIM, LMAR)
C 
      IMPLICIT  NONE
      INTEGER   ILIM
      REAL      DLIM
      INTEGER   LMAR(2,0:ILIM)
C
      INTEGER   ITMP
      INTEGER   L
      REAL      TEMP
C
      LMAR(1,0) = ILIM
      LMAR(2,0) = -1
      DO 10 L = 1 , ILIM
          TEMP   = DLIM*DLIM - FLOAT(L*L)
          IF ( TEMP .GE. 1.0 ) THEN
            ITMP = INT(SQRT(TEMP))
            LMAR(1,L) = ITMP
            LMAR(2,L) = ITMP
          ELSE
            LMAR(1,L) = 0
            LMAR(2,L) = 0
          ENDIF
   10      CONTINUE
C
      RETURN
C
      END
