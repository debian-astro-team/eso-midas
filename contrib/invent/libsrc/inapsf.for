C @(#)inapsf.for	19.1 (ES0-DMD) 02/25/03 13:25:34
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.INPUT/OUTPUT
C  Input parameters
C  RARR       real*4 array    real keywords
C  MAXSUB     integer*4       maximal subarray half edge
C  Output parameters
C  APSF       real*4 array    one dimensional p.s.f.
C-----------------------------------------------------------------------
      SUBROUTINE INAPSF ( RARR , MAXSUB , APSF )
C
      IMPLICIT  NONE
      REAL      RARR(64)
      INTEGER   MAXSUB
      REAL      APSF(0:MAXSUB)
C
      INTEGER   K , K2
C
      APSF(0) = 1.0
      K2 = MIN( 25 , MAXSUB )
      DO 30 K = 1 , K2
            APSF(K) = APSF(K-1) * 10**(-RARR(13+K))
  30      CONTINUE
        DO 31 K = 26 , MAXSUB
            APSF(K) = APSF(K-1) * 10**(-RARR(38))
  31      CONTINUE
C
      RETURN
C
      END
