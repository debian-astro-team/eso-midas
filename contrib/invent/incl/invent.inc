C @(#)invent.inc	19.1 (ESO-IPG) 02/25/03 13:25:22
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION: INV_INC
C.LANGUAGE:       ESO-FOR
C.AUTHOR:         A. Kruszewski
C.KEYWORDS:
C.ENVIRONMENT:    MIDAS
C.COMMENTS:
C.VERSION:        1.0      02-DEC-1988:      Creation
C.VERSION:        1.1      13-Oct-1995:      Increate MAXCNT and MAXDET - RHW
C
C-----------------------------------------------------------------------
C
C      LPBUF  Maximum size of array holding informations about how the
C             frame is divided into regions in ANALYSE. Number and size
C             of regions will be adjusted at run time to agree with LPBUF.
C
C      MAXCNT Maximum number of objects in SEARCH and ANALYSE. Program
C             will abort if that limit is exceeded.
C
C      MAXDET Maximum number of detections in SEARCH. Program will abort
C             if that limit is exceeded.
C
C      MAXNET Maximum number of horizontal subregions in SEARCH. Number
C             and size of regions will be adjusted at run time to agree
C             with MAXNET. 
C
C      MAXSCN Maximum length of a scanning rectangle applied in SEARCH.
C             Only minor impairment of treatment of very large objects
C             if exceeded.
C
C      MAXSUB Maximum half edge of subarrays used for individual objects.
C             Only very large objects can be affected if too small.
C
C      MPSF   Maximum number of stars used determination of one
C             dimensional point spread function.
C
C      NOSP   Maximum number of standard stars used to determine two
C             dimensional p.s.f. for a single subpixel.
C

      INTEGER         LPBUF
      PARAMETER      (LPBUF = 1024)
      INTEGER         MAXCNT
      PARAMETER      (MAXCNT = 16384)
      INTEGER         MAXDET
      PARAMETER      (MAXDET = 32768)
      INTEGER         MAXNET
      PARAMETER      (MAXNET = 32)
      INTEGER         MAXSCN
      PARAMETER      (MAXSCN = 1024)
      INTEGER         MAXSUB
      PARAMETER      (MAXSUB = 50)
      INTEGER         MAXTBL
      PARAMETER      (MAXTBL = 128)
      INTEGER         MPSF
      PARAMETER      (MPSF = 1024)
      INTEGER         NIPAR
      PARAMETER      (NIPAR = 10)
      INTEGER         NOSP
      PARAMETER      (NOSP = 16)
      INTEGER         NRPAR
      PARAMETER      (NRPAR = 32)
