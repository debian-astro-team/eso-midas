! @(#)cloudabs.prg	19.1 (ES0-DMD) 02/25/03 13:23:25
! POCEDURE ABS
! CALCUL D'UN SPECTRE D'ABSORPTION
!
! COMPUTE/ABS P1 P2 P3 P4 P5
! P1 : image en emission
! P2 : RESULTAT image P1 + absorption
! P3 : table modele de nuages
! P4 : tables des parametres physiques
! P5 : image PSF
! 
DEFINE/PARAM P1 ? IMA "Input Image"
DEFINE/PARAM P2 ? IMA "Ouput Synthetic Spectrum"
DEFINE/PARAM P3 absc TAB
DEFINE/PARAM P4 absp TAB
DEFINE/PARAM P5 psf IMA
!
! DEFINE/LOCAL CLDZ/R/1/1 3.5
! DEFINE/LOCAL CLDOP/I/1/1 1
!
WRITE/KEYW IN_A/C/1/60 'P1'
WRITE/KEYW IN_B/C/1/60 'P2'
WRITE/KEYW IN_C/C/1/60 'P3'
WRITE/KEYW IN_D/C/1/60 'P4'
WRITE/KEYW IN_E/C/1/60 'P5'
!
RUN CON_EXE:CLOUDABS
