! @(#)cloudemi.prg	19.1 (ES0-DMD) 02/25/03 13:23:25
!PROCEDURE EMI
!CALCUL D'UN SPECTRE D'EMISSION : 
!   continu polynomial + raies gaussiennes
!
! COMPUTE/EMI P1 P2 
!       
! P1 : nom de l'image resultat
! P2 : table contenant les parametres des raies
!
! DEFINE/LOCAL CLDCT/R/1/6 1.,0.,0.,0.,0.,0.
! DEFINE/LOCAL CLDDIM/R/1/3 4000.,.5,1000.
DEFINE/PARAM P1 ?    IMA "Output spectrum ?:"
DEFINE/PARAM P2 NULL TAB "Table with line parameters"
WRITE/KEYW IN_A/C/1/60 'P1'
WRITE/KEYW IN_B/C/1/60 'P2'
RUN CON_EXE:CLOUDEMI
