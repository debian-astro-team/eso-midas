! @(#)cloudemo.prg	19.1 (ES0-DMD) 02/25/03 13:23:25
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT (c) European Southern Observatory
!.IDENT     cloudemo.prg
!.AUTHOR    M. Pierre, J.D.Ponz  - ESO
!.KEYWORDS  tutorial, demo, test, cloud context
!.PURPOSE
!      Demo. procedure for cloud context.
!      Absorption cloud model. 
!      The following files are copied into your directory :
!      cloudt.tbl   -  table defining the cloud model
!      cloudsn1.bdf -  SN 1986g Ca II H
!      cloudsn2.bdf -  SN 1986g Ca II K
!      The tutorial computes the instrumetal PSF.
!      The continuum of the spectrum is then computed, as
!      a constant in our example, and then an image with the 
!      absorption model is created and convolved with the PSF.  
!      Finally, the observed and the computed spectra are compared. 
!
!    -  Files will be deleted after demo.
!.USAGE
!      Execute as TUTORIAL/CLOUD
!
!.VERSION   2.0     22-Sep-1988
!.VERSION   2.1     30-Apr-1991 M. Peron
!.VERSION   2.2     02-Dec-1991 M. Peron
!------------------------------------------------------------

ECHO/OFF
SET/CONTEXT SPEC
@ creifnot 1
WRITE/OUT " "
WRITE/OUT "Copy observations into your directory:"
WRITE/OUT "cloudsn1.bdf  - SN 1986g CaII H "
WRITE/OUT "cloudsn2.bdf  - SN 1986g CaII K "
WRITE/OUT "cloudt.tbl    - Cloud model "
WRITE/OUT "absp.tbl      - Atomic parameters "
WRITE/OUT " "
intape/fits 1 toto MID_TEST:cloudsn1.fits NOC
-rename toto0001.bdf  cloudsn1.bdf
intape/fits 1 toto MID_TEST:cloudsn2.fits NOC
-rename toto0001.bdf  cloudsn2.bdf
intape/fits 1 toto MID_TEST:cloudt.tfits NOC
-rename toto0001.tbl  cloudt.tbl
intape/fits 1 toto MID_TEST:absp.tfits NOC
-rename toto0001.tbl  absp.tbl
ECHO/ON
select/table absp all
select/table cloudt all
ASSIGN/GRAPHIC T NOFILE
!
SET/GRAPHIC    PMODE=1
!
!  Prepare control variables
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Define sampling domain of the modelled image. "
WRITE/OUT " "
ECHO/ON
CLDDIM(1) = 3925.0      ! Starting wavelength
CLDDIM(2) = 0.050       ! Sampling step
CLDDIM(3) = 1200        ! Number of pixels 
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Define the redshift of the source. "
WRITE/OUT " "
ECHO/ON
CLDZ(1) = 0.           
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Define the continuum. "
WRITE/OUT " A constant in our case. "
WRITE/OUT " "
ECHO/ON
CLDCT(1) = 1.
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Define the shape of the absorption profile. "
WRITE/OUT " It can be either Maxwellian or Voigtian."
WRITE/OUT " Voigtian is selected in our example. "
WRITE/OUT " "
ECHO/ON
CLDOP(1) = 1              ! Voigtian option
! 
! PSF
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Model the instrumental PSF as a gaussian profile with "
WRITE/OUT " 0.22 as FWHM. "
WRITE/OUT " "
ECHO/ON
CREATE/PSF psft 0.22
! 
! CONTINUUM
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Model the continuum as a constant (see CLDCT)"
WRITE/OUT " No emission lines are present. "
WRITE/OUT " "
ECHO/ON
COMPUTE/EMISSION synemi 
! 
! ABS CLOUD
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " The table with the cloud model contains 12 absorption"
WRITE/OUT " components as follows: "
WRITE/OUT " "
NAME/COLUMN        cloudt :ID F4.0
ECHO/ON
SHOW/TABLE      cloudt
READ/TABLE      cloudt
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Compute the cloud model using this table"
WRITE/OUT " "
ECHO/ON
COMPUTE/ABSORPTION synemi synabs cloudt absp psft
!
! DISPLAY RESULTS
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Compare convolved spectrum (synabs1c) with "
WRITE/OUT " observations "
WRITE/OUT " "
FIND/MINMAX synabs1c
FIND/MINMAX synabs1
CUTS synabs1c 0.0,0.0
CUTS synabs1 0.0,0.0 
ECHO/ON
SET/GRAPHIC XAXIS=3925.,3985.,10.,2. YAXIS=0.,1.2,0.5,0.1
SET/GRAPHIC BINMODE=OFF
PLOT     synabs1c 
SET/GRAPHIC BINMODE=ON
OVER     cloudsn1
OVER     cloudsn2
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Show the CaII H components "
WRITE/OUT " "
ECHO/ON
! SET/GRAPHIC PMODE=1
SET/GRAPHIC XAXIS=3965.,3978.,2.,0.5
SET/GRAPHIC BINMODE=OFF
PLOT     synabs1c 
SET/GRAPHIC BINMODE=ON
OVER     cloudsn2
OVERPLOT/IDENT cloudt :POS :ID BOTTOM
LABEL/GRAP "CaII H" 3965.5,1.07 0 2 1
!
ECHO/OFF
WRITE/OUT " "
WRITE/OUT " Show the CaII K components "
WRITE/OUT " "
ECHO/ON
COMPUTE/TABLE cloudt :POS1 = :POS-34.81
SET/GRAPHIC XAXIS=3930.,3942.,2.,0.5
SET/GRAPHIC BINMODE=OFF
PLOT     synabs1c 
SET/GRAPHIC BINMODE=ON
OVER     cloudsn1
OVERPLOT/IDENT cloudt :POS1 :ID BOTTOM
LABEL/GRAP "CaII K" 3930.5,1.07 0 2 1
ECHO/OFF
