C @(#)cloudpsf.for	19.1 (ESO-DMD) 02/25/03 13:23:29
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
       PROGRAM PSF
C**************************************************
C       M.PIERRE 1-88                                
C                                                   
C       CALCULATION OF  A 1D_GAUSSIAN NORMALIZED   
C                   AND CENTERED AT 0              
C       Modified: START, STEP double precision i/o  
C 
C 990805
C**************************************************
C
       IMPLICIT NONE
C
       INTEGER MADRID(1),IMNO
       INTEGER NCARA,KUN,KNUL,IST,NPIX
       INTEGER*8  PNTR
C
       REAL START,FNORM
       REAL FWHM,STEP,DIM(3)
C
       DOUBLE PRECISION DSTA, DSTP
C
       CHARACTER NOMP*60,IDENT*72,CUNIT*80
C
       COMMON/VMR/MADRID
C
       CALL STSPRO('PSF')
C
       CALL STKRDC('IN_A',1,1,60,NCARA,NOMP,KUN,KNUL,IST)
       CALL STKRDR('INR',1,1,NCARA,FWHM,KUN,KNUL,IST)
       CALL STKRDR('CLDDIM',1,3,NCARA,DIM,KUN,KNUL,IST)
C
       STEP =DIM(2)
       START=-4.*FWHM
       NPIX =-2.*START/STEP
       FNORM=.9394373/FWHM*STEP
C
       IDENT='PSF '
       CUNIT = ' '
       DSTA = START
       DSTP = STEP
       CALL STIPUT(NOMP,10,1,1,1,NPIX,DSTA,DSTP,IDENT,
     :                   CUNIT,PNTR,IMNO,IST)       
       CALL IMAGE(MADRID(PNTR),NPIX,STEP,START,FWHM,FNORM)
       CALL STSEPI
       END
C
       SUBROUTINE IMAGE(Y,NPIX,STEP,START,FWHM,FNORM)
       INTEGER NPIX,I
       REAL  Y(1),START,STEP,FWHM,FNORM,WL
       DO 10 I=1,NPIX
         WL=START+FLOAT(I-1)*STEP
         Y(I)=FNORM*EXP(-0.69315*((2.*WL/FWHM)**2))
10     CONTINUE
       RETURN
       END
C
