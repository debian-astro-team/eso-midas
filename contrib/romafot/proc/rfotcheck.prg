! @(#)rfotcheck.prg	19.1 (ES0-DMD) 02/25/03 13:29:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTCHECK.PRG
!.PURPOSE:        Procedure check the photometry accuracy using the command
!.USE:            CHECK/ROMAFOT [cat_tab] [out_tab] err_mag
!.AUTHOR:         Rein H. Warmels
!.VERSION:        890912 RHW Implemented 
! ----------------------------------------------------------------------
DEFINE/PARAM    P1  ?  TAB "Enter catalogue table: "
DEFINE/PARAM    P2  ?  TAB "Enter the registration table: "
DEFINE/PARAM    P3  ?  NUM "Enter limiting instrumental magn. error: "
!
WRITE/KEYW IN_A             'P1'
WRITE/KEYW IN_B             'P2'
WRITE/KEYW INPUTR/R/1/1     'P3'
!
RUN CON_EXE:RFOTCHECK
