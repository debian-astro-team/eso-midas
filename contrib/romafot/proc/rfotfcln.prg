! @(#)rfotfcln.prg	19.1 (ES0-DMD) 02/25/03 13:30:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTFCLN.PRG
!.PURPOSE:        Select window in artifical frame
!.USE:            FCLEAN/ROMAFOT cat_tab int_tab [out_tab]
!.AUTHOR:         Rein H. Warmels , ESO Garching
!.VERSION:        890809  Implementation in pMIDAS
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?            TAB "Enter input catalogue table: "
DEFINE/PARAM   P2  ?            TAB "Enter input intermediate table: "
DEFINE/PARAM   P3  NONE         CHA "Enter output intermediate table: "
!
WRITE/KEYW IN_A           'P1'
WRITE/KEYW IN_B           'P2'
WRITE/KEYW OUT_A          'P3'
!
SELECT/TABLE 'P2' :GROUP.LT.1000
RUN CON_EXE:RFOTFCLEAN
