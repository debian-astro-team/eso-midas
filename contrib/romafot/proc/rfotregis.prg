! @(#)rfotregis.prg	19.1 (ES0-DMD) 02/25/03 13:30:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTREGIS.PRG
!.PURPOSE:        Register command for the ROMAFOT package
!.USE:            REGISTER/ROMAFOT int_tab reg_tab [wnd_opt] [obj_opt]
!.AUTHOR:         Rein H. Warmels, ESO Garching
!.VERSION:        890614 RHW  Creation 
!.VERSION:        890803 RHW  Implementation of table file system
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?             TAB  "Enter input intermediate table: "
DEFINE/PARAM   P2  ?             TAB  "Enter output registration table: "
DEFINE/PARAM   P3  A             C    "Enter window option: "
DEFINE/PARAM   P4  N             C    "Enter object option: "
!
WRITE/KEYW IN_A             'P1'
WRITE/KEYW OUT_A            'P2'
WRITE/KEYW INPUTC/C/1/1     'P3'
WRITE/KEYW INPUTC/C/2/1     'P4'
!
RUN CON_EXE:RFOTREGIST
