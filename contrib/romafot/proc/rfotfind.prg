! @(#)rfotfind.prg	19.1 (ESO-DMD) 02/25/03 13:30:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTFIND.PRG
!.PURPOSE:        To select object from the displayed frame and to store
!.                their position into an catalogue table
!.USE:            FIND/ROMAFOT frame [int_tab}
!.AUTHOR:         Rein H. Warmels
!.VERSION:        890228 RHW loading of frame done by LOAD/IMA
! 990607  KB
! ----------------------------------------------------------------------
define/param   p1  *        IMA   "Input frame "
define/param   p2  {p1}cat  TAB   "Output catalogue table "
!
write/keyw in_a            {p1}
write/keyw out_a           {p2}
!
set/cursor 0 CROSS,WHITE
! 
if p1 .ne. "*"  load/image {p1}
run CON_EXE:RFOTFIND
