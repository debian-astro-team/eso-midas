! @(#)rfotrfit.prg	19.1 (ES0-DMD) 02/25/03 13:30:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTRFIT.PRG
!.PURPOSE:        To determine the characyterristics of stellar images by
!.                non-linear fitting
!.USE:            FIT/ROMAFOT frame [int_tab] [thers,sky] [sig,sat,tol,iter]
!                 [meth[,beta]] [fit_opt] [mean_opt]
!.AUTHOR:         Rein H. Warmels
!.VERSION:        890903 RHW implementation of table file system
!.                           restructering the code
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?                 C     "Enter frame: "
DEFINE/PARAM   P2  'P1'INT           TAB   "Enter output intermediate table: "
DEFINE/PARAM   P3  0.0,0.0           NUM   "Enter threshold and sky background: " 
DEFINE/PARAM   P4  0.0,0.0,2.0,50.0  NUM   "Enter sigma, sat. tol. and # iter.: "
DEFINE/PARAM   P5  MU,4.0            C     "Enter the fit method and beta: "
DEFINE/PARAM   P6  YNNN              C     "Enter the fit options: "
DEFINE/PARAM   P7  N                 C     "Enter the averaging option: "
DEFINE/PARAM   P8  Y                 C     "Enter the display option: "
!
WRITE/KEYW IN_A           'P1'
WRITE/KEYW IN_B           'P2'
WRITE/KEYW INPUTR/R/1/2   'P3'
WRITE/KEYW INPUTR/R/3/4   'P4'
WRITE/KEYW INPUTC/C/1/20  'P5'
WRITE/KEYW INPUTC/C/21/4  'P6'
WRITE/KEYW INPUTC/C/41/1  'P7'
WRITE/KEYW INPUTC/C/61/1  'P8'
!
RUN CON_EXE:RFOTRFIT
