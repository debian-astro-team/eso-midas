! @(#)rfotsky.prg	19.1 (ES0-DMD) 02/25/03 13:30:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTSKY.PRG
!.PURPOSE:        Sky determination in the ROMAFOT package
!.USE:            SKY/ROMAFOT frame [sky_tab] [area] [nrx,nry]
!.AUTHOR:         Rein H. Warmels, ESO Garching
!.VERSION:        890803  RHW  Inclusion of MIDAS table file system
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?           IMA  "Enter frame: "
DEFINE/PARAM   P2  'P1'SKY     TAB  "Enter output table for background values: "
DEFINE/PARAM   P3  [<,<:>,>]   C    "Enter area for operation: "
DEFINE/PARAM   P4  5,8         N    "Enter number of boxes in x and y: "
DEFINE/PARAM   P5  0.0,0.0     N    "Enter minimum and maximum for histogram:"
!
WRITE/KEYW IN_A             'P1'
WRITE/KEYW OUT_A            'P2'
WRITE/KEYW INPUTC/C/1/40    'P3'
WRITE/KEYW INPUTI/I/1/2     'P4'
WRITE/KEYW INPUTR/R/1/2     'P5'
!
RUN CON_EXE:RFOTSKY
