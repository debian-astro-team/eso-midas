! @(#)rfotadapt.prg	19.1 (ES0-DMD) 02/25/03 13:29:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTADAPT.PRG
!.PURPOSE:        Adapt trial values to a new image frame
!.USE:            ADAPT/ROMAFOT int_tab [thres] [i_factor] [s_factor] [h_factor]
!                                [x_size,y_size]
!.AUTHOR:         Rein H. Warmels, ESO-IPG
!.VERSION:        18-Jan-1990   Documentation added
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?            TAB "Enter inter_file "
DEFINE/PARAM   P2  0.0          NUM "Enter threshold "
DEFINE/PARAM   P3  1.0          NUM "Enter factor for height "
DEFINE/PARAM   P4  1.0          NUM "Enter factor for background "
DEFINE/PARAM   P5  1.0          NUM "Enter factor for holes "
DEFINE/PARAM   P6  0,0          NUM "Enter x_size,y_size "
!
WRITE/KEYW IN_A             'P1'
WRITE/KEYW INPUTR/R/1/1     'P2'
WRITE/KEYW INPUTR/R/2/1     'P3'
WRITE/KEYW INPUTR/R/3/1     'P4'
WRITE/KEYW INPUTR/R/4/1     'P5'
WRITE/KEYW INPUTI/I/1/2     'P6'
!
RUN CON_EXE:RFOTADAPT
