! @(#)rfotsearc.prg	19.1 (ES0-DMD) 02/25/03 13:30:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTSEARC.PRG
!.PURPOSE:        MIDAS procedure to select objects in the ROMAFOT package
!.USE:            SEARCH/ROMAFOT frame [sky_tab] [cat_tab] [area] [psf_par] 
!                                thresh] [height]
!.AUTHOR:         Rein H. Warmels  
!.NOTE:           In case the sky background file is created with the 
!                 rectangular mode (using the program SKY/ROMAFOT the user 
!                 will be asked to specify the area for the search
!                 interactively. Default is then the area on which SKY/ROMAFOT
!                 has operated.
!.VERSION:        890802  RHW  Implementation of MIDAS tables
!-----------------------------------------------------------------------
DEFINE/PARAM P1 ?           IMA     "Enter input IMA image file: "
DEFINE/PARAM P2 'P1'SKY     TAB     "Enter input table with sky background: "
DEFINE/PARAM P3 'P1'CAT     TAB     "Enter output table name for catalogue: "
DEFINE/PARAM P4 SKY         C       "Enter the area for operation: "
DEFINE/PARAM P5 3.,4.,15000 NUM     "Enter parameters for point spread function: "
DEFINE/PARAM P6 PROMPT      C       "Enter photometric option,threshold: "
DEFINE/PARAM P7 0.0         NUM     "Enter mimimum height for uninfluenced: "
!
WRITE/KEYW IN_A            'P1'    !input image
WRITE/KEYW IN_B            'P2'    !input sky background table
WRITE/KEYW OUTPUTC         'P3'    !output catalogue table
WRITE/KEYW INPUTC/C/1/40   'P4'    !area
WRITE/KEYW INPUTR/R/1/3    'P5'    !point spread function parameters
WRITE/KEYW INPUTC/C/41/40  'P6'    !photometric option, threshold
WRITE/KEYW INPUTR/R/4/1    'P7'    !mimimum height
!
RUN CON_EXE:RFOTSEARCH
