! @(#)rfotexam.prg	19.1 (ES0-DMD) 02/25/03 13:30:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION RFOTEXAM.PRG
!.PURPOSE:       Examine quality of fitted objects and flag badly fitted ones
!.USE:           EXAMIN/ROMAFOT in_tab [hmin,hmax]
! AUTHOR:        Rein H. Warmels  ESO, Garching
!.VERSION:       890802  RHW New version for portable MIDAS
!----------------------------------------------------------------------
DEFINE/PARAM P1 ?           TAB    "Enter name of the intermediate name: "
DEFINE/PARAM P2 0.0,0.0     NUM    "Enter intensity range for objects [whole]: "
!
WRITE/KEYW INPUTR/R/1/2 'P2'
!
RUN CON_EXE:RFOTEXAMIN
