! @(#)rfotgroup.prg	19.1 (ES0-DMD) 02/25/03 13:30:00
! @(#)rfotgroup.prg	6.1.1.2 (ESO-IPG) 10/27/93 15:35:38
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTGROUP.PRG
!.PURPOSE:        Grouping for the ROMAFOT pacakge
!.USE:            GROUP/ROMAFOT frame [area] [cat_tab] [int_tab] [thres] 
!                               [wnd_max] [end_rad,sta_rad] [wnd_perc]
!.AUTHOR:         Rein H. Warmels, ESO Garching
!.VERSION:        890803  RHW  Inclusion of table file system
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?               C   "Enter frame name: "
DEFINE/PARAM   P2  [<,<:>,>]       C   "Enter area: "
DEFINE/PARAM   P3  {P1}CAT         C   "Enter catalogue table: "
DEFINE/PARAM   P4  {P1}INT         C   "Enter intermediate table: "
DEFINE/PARAM   P5  0.0             N   "Enter photometric threshold: "
DEFINE/PARAM   P6  10              N   "Enter max. objects/group "
DEFINE/PARAM   P7  90.0,100.0      N   "Enter %_act_rad "
DEFINE/PARAM   P8  100.0           N   "Enter %_window "
!
DEFINE/LOCAL K/I/1/1 0
DEFINE/LOCAL FILE/C/1/60 {P4}
K = M$INDEX("{P4}","/")
IF K .EQ. 0 THEN
   WRITE/OUT "{P4}"
   IF M$EXIST("{FILE}.tbl") .EQ. 0 THEN
      WRITE/OUT "*** INFO: Intermediate table not present;"
      WRITE/OUT "          will create a new one"
   ELSE
      WRITE/OUT "*** INFO: Intermediate table already present;"
      DEFINE/LOCAL DEL/C/1/1 "Y"
      INQUIRE/KEYW DEL/C/1/1 "     DO YOU WANT TO OVERWRITE IT [Y]: "
      IF DEL .EQ. "Y" THEN
         DELETE/TABLE {FILE} NO
      ELSE
         WRITE/OUT "***       Intermediate table will be appended"
      ENDIF
   ENDIF

ELSE
   K = K - 1
   WRITE/KEYW FILE/C/1/60 "{P4(1:{K})}"
   K = K + 2
   DEFINE/LOCAL OPT/C/1/1 "{P4({K}:{K})}"
   IF OPT .EQ. "O" THEN
      WRITE/OUT "*** INFO: Intermediate table will be overwritten"
      DELETE/TABLE {FILE} NO
   ELSE IF OPT .EQ. "A" THEN
      WRITE/OUT "*** INFO: Intermediate table will be appended"
   ENDIF
ENDIF
!
WRITE/KEYW IN_A             'P1'
WRITE/KEYW INPUTC/C/1/80    'P2'
WRITE/KEYW IN_B             'P3'
WRITE/KEYW OUT_A/C/1/60 "{FILE}"
WRITE/KEYW INPUTR/R/1/1     'P5'
WRITE/KEYW INPUTI/I/1/1     'P6'
WRITE/KEYW INPUTR/R/2/2     'P7'
WRITE/KEYW INPUTR/R/4/1     'P8'
!
!
RUN CON_EXE:RFOTGROUP

