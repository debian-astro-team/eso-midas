! @(#)rfotmodel.prg	19.1 (ES0-DMD) 02/25/03 13:30:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTMODEL.PRG
!.PURPOSE:        Compute subpixel values model observation
!.USE:            MODEL/ROMAFOT [out_file]
!.AUTHOR:         Rein H. Warmels (ESO-IPG)
!.NOTE:           The command reads the keyword INPUTR for the input
!.VERSION:        890926  RHW Implementation for Pmidas
!.VERSION:        920413  KB
! ----------------------------------------------------------------------
DEFINE/PARAMETER P1 MODEL C  "Enter name of the output file: "
WRITE/KEYW INPUTC/C/1/60 {P1}
!
WRITE/OUT "*** INFO: You can change the input parameters by rewriting the "
WRITE/OUT "          keyword INPUTR with the command WRITE/KEYW (see the help)."
!
WRITE/KEYW INPUTR/R/1/13 1,3,2.2,800,.3,10,1000,.35,35,23,18,.5,.1
RUN CON_EXE:RFOTMODEL

