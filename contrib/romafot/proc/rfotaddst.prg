! @(#)rfotaddst.prg	19.1 (ES0-DMD) 02/25/03 13:29:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTADDST.PRG
!.PURPOSE:        MIDAS procedure to put artifical components in a frame
!.AUTHOR:         Rein H. Warmels
!.USE:            ADDST/ROMAFOT in_frame out_frame [reg_tab] [cat_tab] 
!                               [x_size,y_size] [n_sub]
!.VERSION:        890209 RHW new version for Portable MIDAS
!.VERSION:        890808 RHW split size and number of subframes
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?            IMA "Enter input frame: "
DEFINE/PARAM   P2  ?            IMA "Enter artificial output frame: "
DEFINE/PARAM   P3  'P1'REG      TAB "Enter the input registration table: "
DEFINE/PARAM   P4  'P2'CAT      TAB "Enter the output catalogue table: "
DEFINE/PARAM   P5  21,21        NUM "Enter the dimension in x and y: "
DEFINE/PARAM   P6  1            NUM "Enter the number of subframes: "
!
WRITE/KEYW OUT_A          'P2'
WRITE/KEYW IN_B           'P3'
WRITE/KEYW OUT_B          'P4'
WRITE/KEYW INPUTI/I/1/2   'P5'
WRITE/KEYW INPUTI/I/3/1   'P6'
!
-COPY 'P1'.bdf 'P2'.bdf
RUN CON_EXE:RFOTADDST
