! @(#)rfotresid.prg	19.1 (ES0-DMD) 02/25/03 13:30:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTRESID.PRG
!.PURPOSE:        Compute reconstructed frame and difference with original
!.USE:            RESIDUAL/ROMAFOT IN_FRAME OUT_FRAME diff_frame [reg_tab]
!.AUTHOR:         Rein H. Warmels, ESO Garching
!.VERSION:        890808  RHW  New version for pMIDAS
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?            IMA  "Enter original frame "
DEFINE/PARAM   P2  ?            IMA  "Enter reconstruction frame"
DEFINE/PARAM   P3  ?            IMA  "Enter difference output frame "
DEFINE/PARAM   P4  'P1'REG      TAB  "Enter input registration table: "
!
!
WRITE/KEYW IN_A   'P1'
WRITE/KEYW OUT_A  'P2'
WRITE/KEYW OUT_B  'P3'
WRITE/KEYW IN_B   'P4'
!
RUN CON_EXE:RFOTRESID
STAT/IMAGE 'P2'
COMPUTE/IMAGE 'P3' = 'P1'-'P2'
STAT/IMAGE 'P3'
