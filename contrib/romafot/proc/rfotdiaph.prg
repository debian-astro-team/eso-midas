! @(#)rfotdiaph.prg	19.1 (ES0-DMD) 02/25/03 13:30:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTDIAPH.PRG
!.PURPOSE:        Compute aparture limited magnitudes
!.USE:            DIAPHRAGM/ROMAFOT frame option [cat_tab] [reg_tab] ap_rad
!.AUTHOR:         Rein H. Warmels, ESO-Garching
!.VERSION:        881208 RHW First version
!.VERSION:        890918 RHW Integration in Pmidas
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?          IMA  "Enter input frame: "
DEFINE/PARAM   P2  'P1'REG    TAB  "Enter input catalogue table: "
DEFINE/PARAM   P3  'P1'MAG    TAB  "Enter output registration table: "
DEFINE/PARAM   P4  ?          NUM  "Enter aperature radius for integration: "
!
WRITE/KEYW IN_A           'P1'
WRITE/KEYW IN_B           'P2'
WRITE/KEYW OUT_A          'P3'
WRITE/KEYW INPUTI/I/1/1   'P4'
!
RUN CON_EXE:RFOTDIAPHR
