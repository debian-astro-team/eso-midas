! @(#)rfotcbase.prg	19.1 (ES0-DMD) 02/25/03 13:29:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTCBASE.PRG
!.PURPOSE:        MIDAS procedure for the command CBASE/ROMAFOT
!.USE:            CBASE/ROMAFOT frame_1 frame_2 [out_tab1] [out_tab2]
!.AUTHOR:         Rein H. Warmels, ESO-Garching
!.VERSION:        890912 RHW New version for portable MIDAS; now most of the
!                            works is done on command level
!.VERSION         920413  KB
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?         C   "Enter the first frame: "
DEFINE/PARAM   P2  ?         C   "Enter the second frame: "
DEFINE/PARAM   P3  TRACOO1   TAB "Enter the output table for the first frame: "
DEFINE/PARAM   P4  TRACOO2   TAB "Enter the output table for the second frame: "
WRITE/KEYW    INPUTC/C/1/60 CBASE
!
IF IDIDEV(18) .LT. 10 THEN
   CLEAR/DISPLAY
   SET/SPLIT 1,0
   LOAD/IMAGE 'P1' 0
   LOAD/IMAGE 'P2' 1
   WRITE/OUT "*** INFO: Select the objects in the first image first;"
   WRITE/OUT "***       thereafter select the objects in the second image"
   GET/CURSOR 'INPUTC' NO,1 YY
   WRITE/OUT
   WRITE/OUT ***       You can clear the screen with the command CLEAR/DISPLAY"
ELSE
   IF DAZDEVR(10) .GE. 0 DELETE/DISPLAY
   CREATE/DISPLAY 0 512,512,0,0
   LOAD/IMAGE 'P1'            ! load first image in channel 0
   CREATE/DISPLAY 1 512,512,512,0
   LOAD/IMAGE 'P2'            ! load first image in channel 1
   ASSIGN/DISPLAY D,0
   WRITE/OUT "*** INFO: Select the objects in the first image"
   GET/CURSOR CBASE NO,1 YY
   ASSIGN/DISPLAY D,1
   WRITE/OUT "*** INFO: Select the objects in the second image"
   GET/CURSOR CBASE A YY
   WRITE/OUT
   WRITE/OUT -
      "***       You can clear the screen with the command DELETE/DISPLAY"
   WRITE/OUT "***       or with CLEAR/DISPLAY"
ENDIF
!
WRITE/KEYW IN_A          'P1'
WRITE/KEYW IN_B          'P2'
RUN CON_EXE:RFOTCBASE
DELETE/TABLE 'INPUTC'
