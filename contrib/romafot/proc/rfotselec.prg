! @(#)rfotselec.prg	19.1 (ESO-DMD) 02/25/03 13:30:01
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTSELECT.PRG
!.PURPOSE:        To select object from the displayed frame and to store
!.                their position into a intermediate file
!.USE:            SELECT/ROMAFOT frame [int_tab] [wnd_size]
!.AUTHOR:         Rein H. Warmels
!.VERSION:        270189 RHW  loading of frame done by LOAD/IMA
!.VERSION:        890803 RHW  implementation of table file system
! 990607  KB
! ----------------------------------------------------------------------
define/param   p1  *        IMA   "Input frame "
define/param   p2  {p1}int  TAB   "Input intermediate table "
define/param   p3  21,21    NUM   "Input wind_size  "
!
write/keyw in_a            {p1}
write/keyw out_a           {p2}
write/keyw inputi/i/1/2/   {p3}
!
set/cursor 0 CROSS,WHITE
! 
if p1 .ne. "*"  load/image {p1}
run CON_EXE:RFOTSELECT
