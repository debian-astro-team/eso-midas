! @(#)rfotanal.prg	19.1 (ES0-DMD) 02/25/03 13:29:59
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTANAL.PRG
!.PURPOSE:        Procedure for the Romafot command ANALYSE/ROMAFOT
!.USE:            ANALYSE/ROMAFOT frame [cat_tab] [int_tab] [sigma,sat]
!.AUTHOR:         Rein Warmels
!.VERSION:        07/08/89  RHW  new version for portable MIDAS
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?            I   "Enter image frame: "
DEFINE/PARAM   P2  'P1'CAT      T   "Enter catalogue table: "
DEFINE/PARAM   P3  'P1'INT      T   "Enter intermediate table: "
DEFINE/PARAM   P4  0.0,0.0      N   "Enter sigma and saturation [old values]: "
!
SET/CUR 0 C_HAIR
!
WRITE/KEYW IN_A             'P1'
WRITE/KEYW IN_B             'P2'
WRITE/KEYW INPUTC/C/1/60    'P3'
WRITE/KEYW INPUTR/R/1/2     'P4'
!
RUN CON_EXE:RFOTANALYS
!
!SELECT/TABLE 'P3' :IDENT.LT.1000
CLEAR/CHANNEL 0
CLEAR/CHANNEL OVERLAY
