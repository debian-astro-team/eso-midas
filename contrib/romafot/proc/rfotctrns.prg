! @(#)rfotctrns.prg	19.1 (ES0-DMD) 02/25/03 13:30:00
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: RFOTCTRNS.PRG
!.PURPOSE:        Coordiate transformation from one to a second frame
!.USE:            CTRANS/ROMAFOT int_tab [base_1] [base_2] [pol_deg]
!.AUTHOR:         Roberto Buonanno (Roma) , Rein H. Warmels (ESO-IPG)
!.VERSION:        880119 RB  Implementation
!.VERSION:        890914 RHW New version for Portable MIDAS
! ----------------------------------------------------------------------
DEFINE/PARAM   P1  ?            TAB   "Enter intermediate table name: "
DEFINE/PARAM   P2  TRACOO1      TAB   "Enter input coordinate for frame 1: "
DEFINE/PARAM   P3  TRACOO2      TAB   "Enter input coordinate for frame 1: "
DEFINE/PARAM   P4  0            NUM   "Enter degree of polynomial fit: "
!
WRITE/KEYW IN_A             'P1'
WRITE/KEYW INPUTC/C/1/30    'P2'
WRITE/KEYW INPUTC/C/31/30   'P3'
WRITE/KEYW INPUTI/I/1/1     'P4'
!
RUN CON_EXE:RFOTCTRANS
