% @(#)model_roma.hlq	19.1 (ESO-IPG) 02/25/03 13:29:28 
\se
SECTION./ROMAFOT
\es\co
MODEL/ROMAFOT                                                   25-SEP-1989 RHW
\oc\su
MODEL/ROMAFOT [mod_file]
	compute (sub)pixel values for a model observation
\us\pu
Purpose:      Compute (sub)pixel values for a model observation
\up\sy
Syntax:       MODEL/ROMAFOT [mod_file]
\ys\pa
              out_file = output file containing the results of the model
                         calculation. The file extension is ".dat"; default
                         file name is `MODEL'.
\ap\no
Note:         The command prepares a file in which the (sub)pixel values are 
              computed according the various observing parmeters (aperature,
              exposure time, seeing, accuracy, magnitude, distance of the pixel
              from the center of the object, ...).
              The input paremeters are taken from the keyword INPUTR in which
              they should be stored as follows (using WRITE/KEY):\\
              INPUTR(1) = seeing in arcsec\\
              INPUTR(2) = beta\\
              INPUTR(3) = diameter of the telescope in meters\\
              INPUTR(4) = bandwidth in Angstrom\\
              INPUTR(5) = total efficiency\\
              INPUTR(6) = photons-to-ADU conversion factor\\
              INPUTR(7) = exposure time on seconds\\
              INPUTR(8) = pixel size in arcsec\\
              INPUTR(9) = readout noise of the detector\\
              INPUTR(10)= faint magnitude limit\\
              INPUTR(11)= bright magnitude limit\\
              INPUTR(12)= magnitude increment\\
              INPUTR(13)= fraction of intrinsic error\\
\on\exs
Examples:
\ex
              WRITE/KEY INPUTR/R/1/13 1,3,2.2,800,.3,10,1000,.35,35,23,18,.5,.1
              MODEL/ROMAFOT MODEL2
\xe \sxe
