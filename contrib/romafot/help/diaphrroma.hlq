% @(#)diaphrroma.hlq	19.1 (ESO-IPG) 02/25/03 13:29:27 
\se
SECTION./ROMAFOT
\es\co
DIAPHRAGM/ROMAFOT                                                21-DEC-1989 RHW
\oc\su
DIAPHRAGM/ROMAFOT frame [regi_tab] [rego_tab] ap_rad
	do aperture photometry with fixed diaphragm
\us\pu
Purpose:      Make aperture photometry with fixed aperture
\up\sy
Syntax:       DIAPHRAGM/ROMAFOT frame [regi_tab] [rego_tab] ap_rad
\ys\pa
              frame    = input image frame.
\ap\pa
              regi_tab = input registration table. The table can be created by
                         the ROMAFOT command REGIST/ROMAFOT; default name is the
                         frame name extended with `REG'. 
\ap\pa
              rego_tab = output registration table to receive the final results;
                         default name is the name of the input frame extended
                         with `MAG'. This table is fully compatible with that 
                         created by REGISTER/ROMAFOT and contains, in particular
                         the magnitude parameter defined as -2.5 * LOG(SUM),
                         where SUM is the summation over all pixel values (sky 
                         subtracted). 
\ap\pa
              ap_rad =   radius (in pixel units) of the aperture over which the
                         integration is performed; no default.
\ap\no
Note:         none
\on\exs
Examples:
\ex
              DIAPHRAGM/ROMAFOT GCLUST GCLTAB DIA 5
              Perform aperture photometry on image frame GCLUST. Read positions 
              of the objects in table GCLTAB and store the results in table 
              DIA. The radius of circular area for integration is 5 pixels.
\xe \sxe
