% @(#)examinroma.hlq	19.1 (ESO-IPG) 02/25/03 13:29:27 
\se                                                                             
SECTION./ROMAFOT
\es\co                                                                          
EXAMINE/ROMAFOT                                                  07-AUG-1989 RHW
\oc\su               
EXAMINE/ROMAFOT int_tab [hmin,hmax]
	examine quality of fitted objects and flag badly fitted ones
\us\pu
Purpose:      Examine the quality of the fitted objects in the intermediate 
              table and flag badly fitted ones
\up\sy
Syntax:       EXAMINE/ROMAFOT int_tab [hmin,hmax]
\ys\pa                                                                          
              int_tab =    intermediate table. The table is created by the 
                           command FIT/ROMAFOT; no default.
\ap\pa
              hmin,hmax  = interval in intensity for the objects to examine. 
                           Default is `all'.
\ap\pa
              min,max,step = Interactive inquiry.\\
                           Enter <RETURN> if you are satisfied with the plotted
                           histogram and the fit. Enter new minimum,maximum,step
                           if you want to have it redrawn with different 
                           binning. Default values is continue.
\ap\no                                                                          
Note:         The command produces two histogram distibutions from the 
              reduced chi**2 and the semi-interquartile interval (SIQ).
              These data are computed for each object by the command FIT/ROMAFOT
              and stored in the intermediate table.\\
              Both histograms are fitted by a gaussian function from which
              the sigma and the mode are computed. The user can enter the range
              over which these fit are made.\\
              Hereafter a plot of chi**2 versus SIQ is produced. In the 
              plot, on both axes, the mode in indicated with a arrow mark; 
              the scales of the axes are in units of sigma.\\

              By cursor control the user can indicate which objects in the 
              plot should be considered as badly fitted and flagged. The 
              following input given to the prompt are possible:\\
              X: flag all stars at the right of the vertical cursor;\\
              Y: flag all stars above the horizontal cursor;\\
              A: flag all stars to the right of the vertical OR above the 
                 horizontal cursor;\\
              R: restore flags;\\
              E: EXIT;\\
              H: display the menu.\\

              As an example the user can issue the command `A' to flag
              the objects to the right or above the cursor. Thereafter a new
              plot of chi**2 versus SIQ is created, excluding the 
              flagged objects. These flagged objects can then be examined
              with ANALYSE/ROMAFOT to correct the input or simply ignored
              in the registration.
\on\exs                                                                         
Examples:                                                                       
\ex
              EXAMINE/ROMAFOT GCLUSINT
              Examine the quality of the fitted objects in the intermediate
              table GCLUSINT and flag badly fitted ones in that file.
\xe \sxe                                                                        
