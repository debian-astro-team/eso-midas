% @(#)adapt_roma.hlq	19.1 (ESO-IPG) 02/25/03 13:29:26 
\se
SECTION./ROMAFOT
\es\co
ADAPT/ROMAFOT                                           08-DEC-1988 RHW
\oc\su
ADAPT/ROMAFOT int_tab [thres] [fac_int] [fac_sky] [fac_hol] [x_siz,y_siz]
	derive trial values for fitting a new frame
\us\pu
Purpose:      Use the fit on the template image frame to derive trial values
              to fit a new frame
\up\sy
Syntax:       ADAPT/ROMAFOT int_tab [thres] [fac_int] [fac_sky] [fac_hol] 
                            [x_size,y_size]
\ys\pa
              int_tab =  the intermediate table with transformed coordinates by
                         CTRANS/ROMAFOT; no default.
\ap\pa
              thresh =   photometric threshold in intensity. Objects with
                         max. intensity under this threshold are flagged
                         and will not be considered in the fit. Nevertheless
                         they are kept in the intermediate table (with flag
                         set to 0) to keep the correspondence with previous
                         registration tables. Default value is 0.0.
\ap\pa
              fac_int =  factor to multiply the intensities of the objects
                         in the intermediate table; default 1.0.
\ap\pa
              fac_sky =  factor to multiply the intensity of the sky bakground
                         in the intermediate tablep; default 1.0.
\ap\pa
              fac_hol =  factor to multiply the radius of holes in the
                         intermediate table; default 1.0.
\ap\pa
              x_size,y_size = dimensions of the new frame to fit; 
                         default same size(s) as before.
\ap\no
Note:         None
\on\exs
Examples:
\ex
              ADAPT/ROMAFOT GCLINT 200 3.2 0.5 1.4 320,520
              Flag the objects with central intensity lower than 200 units in
              the intermediate table GCLINT; for the remainders multiply the
              fitted central intensity by 3.2, the sky background by 0.5 and,
              finally, the holes radii by 1.4. Flag the objects which,
              after the transformation of coordinates, exceed 320 in x and 520
              in y.
\xe \sxe
