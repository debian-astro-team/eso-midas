% @(#)searchroma.hlq	19.1 (ESO-IPG) 02/25/03 13:29:29 
\se                                                                             SECTION./ROMAFOT
\es\co                                                                          
SEARCH/ROMAFOT                                                 07-AUG-1989 RHW
\oc\su
SEARCH/ROMAFOT  frame [sky_tab] [cat_tab] [area] [psf_par] [thres] [height]
	do the actual search for objects above a certain threshold
\us\pu
Purpose:      Do the actual search for objects above a certain threshold
\up\sy 
Syntax:       SEARCH/ROMAFOT frame [sky_tab] [cat_tab] [area] [psf_par] [thres]
                             [height]
\ys\pa                                                                          
              frame    = input frame.
\ap\pa                                                                          
              sky_tab  = table containing sky values (from SKY/ROMAFOT); default
                         is the frame name extended with `SKY'.
\ap\pa                                                                          
              cat_tab  = output table which will contain the object found; 
                         default is the frame name extended with `CAT'.
\ap\pa                                                                          
              area     = area to perform the actual search. Input as can be
                         done using the standard MIDAS notation.
\ap\pa                                                                          
              psf_par  = sigma, beta of the PSF and saturation. If the PSF is
                         described by a gaussian, beta should be set at 0.
                         Default values are 3.,4.,15000. The default can be 
                         used if the user is not interested in automatic 
                         grouping.
\ap\pa                                                 
              thres    = option and value for the photometric threshold (above
                         the sky). Two inputs may be given:\\
                         C,val: the threshold will be constant with value val;\\
                         R,val: the treshold will be set at val times the sky 
                                standard deviation.
\ap\pa                                                 
              height   = height at which the star is considered not to 
                         influence the results photometrically. The 
                         action radius associated to each object, AR, and, 
                         consequently, the size of holes is strongly dependent
                         on this figure. An interactive check with
                         ANALYSE/ROMAFOT is recommended after GROUP/ROMAFOT
                         and before FIT/ROMAFOT.
\ap\no                             
Note:         The command produces a list of (sometimes) thousands of objects.
              Inspection of these using the command ANALYSE/ROMAFOT might be 
              instructive. To proceed it is recommended to use the command 
              GROUP/ROMAFOT. Interactive inspection of the catalogue table is 
              possible using the command ANALYSE/ROMAFOT.
\on\exs                                                                        Examples:                                                                       
\ex
              SEARCH/ROMAFOT GCLUST
              Find objects above a constant threshold over the whole area 
              of the image.
\xe\ex
              SEARCH/ROMAFOT GCLUST ? ? ? ? ? R,3
              Find objects above a relative threshold of 3 standard deviations
              of the sky background (S is asked interactively); these sky 
              values are supposed to be stored in the table GCLUSTSKY.
              The object found will be put into the table GCLUSTCAT.
\xe \sxe                                                                        
