C @(#)rfotdecl.inc	19.1 (ESO-IPG) 02/25/03 13:29:38
C++++
C.Purpose: Include file for the Romafot package
C.Author:  Rein H. Warmels
C.Version: Jun, 06, 1989  Implementation
C.Version: Aug, 12, 1989  identification column taken out
C------
      INTEGER       NINTO
      INTEGER       NINTP
      INTEGER       NINTC
      INTEGER       NINTH
C
      PARAMETER     (NINTO=10000)
      PARAMETER     (NINTP=16)
      PARAMETER     (NINTC=6)
      PARAMETER     (NINTH=3)
C
      INTEGER       IDNGRP                       ! group identification
C
      INTEGER       IDNCMP(NINTO)                ! component type identification
      INTEGER       IDNHOL(NINTO)                ! hole type identification
      REAL          PARINT(NINTP)                ! array containing gen. par.
C
      INTEGER       FLGCMP(NINTO)                ! component parameters array
      INTEGER       FLGHOL(NINTO)                ! hole parameters array
      REAL          FITCMP(NINTO*NINTC)          ! fit par.s array components
      REAL          FITHOL(NINTO*NINTH)          ! fit par.s array holes
C
      COMMON        /INTPAR/IDNGRP,IDNCMP,IDNHOL
      COMMON        /INTFIT/PARINT,FITCMP,FITHOL
      COMMON        /INTFLG/FLGCMP,FLGHOL
