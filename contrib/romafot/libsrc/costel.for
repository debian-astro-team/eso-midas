C @(#)costel.for	19.1 (ES0-DMD) 02/25/03 13:29:42
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE COSTEL(K,IFL,NC,J,SF,NN)
C +++
C ---
      IMPLICIT NONE
      INTEGER  NN
      INTEGER  K
      INTEGER  IFL
      INTEGER  NC
      INTEGER  J
      REAL     SF(NN,7)
C
      IFL = 0
      K   = 0
C 
   20 CONTINUE
      IF (IFL.NE.0.OR.K.GE.NC-1) GO TO 10
      K = K+1
      IF (SF(K,1).EQ.J) IFL=1
      GO TO 20
10    CONTINUE
C
      RETURN
      END
