C @(#)wrilin.for	19.1 (ES0-DMD) 02/25/03 13:29:45
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE WRILIN(NPL,NL,IY,IX,NV,DAT,RV)
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION:  WRILIN
C.PURPOSE:         Routine che scrive una riga, o parte di essa,
C                  di una immagine formato midas (.bdf)
C.USE:             DATI INPUT:
C                  NPL - NL :  dimensioni x - y del file immagine
C                  IY       :  coordinata y della riga da scrivere
C                  IX  - NV :  coordinata x inizio scrittura e 
C                              nr. pixels scritti
C                  DAT      :  indirizzo file immagine - da chiamare con 
C                              %val(punt) con madrid(punt) che e' standard
C                              con le nuove rout. midas
C                  DATI OUTPUT:
C                  RV       :  vettore dati
C------------------------------------------------------------------------
      IMPLICIT  NONE
      INTEGER   NPL
      INTEGER   NL
      INTEGER   IX
      INTEGER   IY
      INTEGER   NV
      REAL      DAT(NPL,NL)
      REAL      RV(1)
C
      INTEGER   I,K
C ***
      K=0
      DO 10 I = IX,IX+NV-1
         K = K+1
         DAT(I,IY) = RV(K)
   10 CONTINUE
      RETURN
      END
