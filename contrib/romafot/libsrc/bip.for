C @(#)bip.for	19.1 (ES0-DMD) 02/25/03 13:29:42
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      REAL FUNCTION BIP(X1,Y1,C,IGP)
C+++
C---
      IMPLICIT  NONE
      DOUBLE PRECISION X1
      DOUBLE PRECISION Y1
      DOUBLE PRECISION C(1)
C     
      DOUBLE PRECISION AA,BB
      INTEGER IGP, IL, L, K
      REAL    VZ
C
      IL = 0
      VZ = 0
      DO 10 L = 0,IGP
         DO 11 K = 0,IGP-L
            IL = IL+1
            IF (K.EQ.0 .AND. X1.EQ.0.) THEN
               AA = 1.
            ELSE
               AA = X1**K
            END IF
            IF (L.EQ.0 .AND. Y1.EQ.0.) THEN
               BB = 1.
            ELSE
              BB = Y1**L
            END IF
            VZ = VZ+AA*BB*C(IL)
  11     CONTINUE
  10  CONTINUE
C
      BIP = VZ
      RETURN
      END
