C @(#)limitx.for	19.1 (ES0-DMD) 02/25/03 13:29:44
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE LIMITX(V,IS,NN,KC,IX,IY,LX,LY)
C +++
C ---
      IMPLICIT  NONE
      INTEGER   NN
      REAL      V(7)
      REAL      IS(NN,4)
      INTEGER   KC
      INTEGER   IX
      INTEGER   IY
      INTEGER   LX
      INTEGER   LY
C
      IF (V(2)-V(6).LE.IX+LX-1.AND.V(2)+V(6).GE.IX) THEN
        IF (V(3)-V(6).LE.IY+LY-1.AND.V(3)+V(6).GE.IY) THEN
           KC       = KC+1
           IS(KC,1) = V(2)
           IS(KC,2) = V(3)
           IS(KC,3) = V(6)
           IS(KC,4) = V(7)
        END IF
      END IF
C
      RETURN
      END
