C @(#)fist.for	19.1 (ES0-DMD) 02/25/03 13:29:43
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FIST(IVZ,NP,VP)
C+++
C.Purpose: Fit con gaussiana monodimensionale a 3 parametri
C         (altezza - larghezza - posizione)
C---
      IMPLICIT NONE
      INTEGER  IVZ(1)
      INTEGER  NP
      REAL     VP(1)
C
      REAL     VPR(3),SIGP(3)
      REAL     RMT(3,3),VTN(3),VFZ(3),VFF(3),VC(3)
      REAL     FS
      REAL     CO
      INTEGER  I, J , K
      INTEGER  TWO, THREE
      INTEGER  NC
      REAL     FK, EP, EPES, FNO

      DATA     FS/.7/,VPR/1.,1.,1./
C
      TWO = 2
      THREE = 3

      DO 10 I=1,3
         VFF(I)=0
         VFZ(I)=0
         VC(I)=0
         DO 11 J=1,3
            RMT(I,J)=0
   11    CONTINUE
   10 CONTINUE

      FK=-4.*ALOG(2.)/(VP(THREE)**2)
C
      DO 20 K=1,NP
         EPES   = (K-VP(TWO))**2
         EP     = EXP(EPES*FK)
         CO     = -VP(1)*EP*2.*FK
         VTN(1) = EP
         VTN(2) = CO*(K-VP(TWO))
         VTN(3) = CO*(K-VP(TWO))**2/VP(THREE)
         FNO    = VP(1)*EP
         DO 21 I=1,3
            VFZ(I) = VFZ(I)+IVZ(K)*VTN(I)
            VFF(I) = VFF(I)+FNO*VTN(I)
            DO 22 J = 1,3
               RMT(I,J) = RMT(I,J)+VTN(I)*VTN(J)
   22       CONTINUE
   21    CONTINUE
   20 CONTINUE
C
      DO 30 I=1,3
         RMT(I,I) = RMT(I,I)*(1+FS**2)
         VC(I)    = VC(I)+VFZ(I)-VFF(I)
   30 CONTINUE
C
      NC = 3
      CALL LISIB(RMT,VC,3,NC,SIGP)
      DO 40 I=1,3
        VP(I) = VC(I)*VPR(I)+VP(I)
   40 CONTINUE
C
      RETURN
      END
