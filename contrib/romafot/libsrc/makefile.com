$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.ROMAFOT.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:51 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   cattab.for
$ FORTRAN   conts3.for
$ FORTRAN   costel.for
$ FORTRAN   elmor.for
$ FORTRAN   elmoi.for
$ FORTRAN   fist.for
$ FORTRAN   bip.for
$ FORTRAN   iperd.for
$ FORTRAN   inttab.for
$ FORTRAN   lisid.for
$ FORTRAN   inters.for
$ FORTRAN   intsat.for
$ FORTRAN   limitx.for
$ FORTRAN   lisib.for
$ FORTRAN   mono4.for
$ FORTRAN   piant5.for
$ FORTRAN   profi5.for
$ FORTRAN   proone.for
$ FORTRAN   realin.for
$ FORTRAN   smain.for
$ FORTRAN   smoot.for
$ FORTRAN   tmask.for
$ FORTRAN   wrilin.for
$ FORTRAN   ran3.for
$ FORTRAN   mima.for
$ LIB/REPLACE librfot cattab.obj,conts3.obj,costel.obj,elmor.obj,elmoi.obj,fist.obj,bip.obj,iperd.obj,inttab.obj,lisid.obj,inters.obj,intsat.obj,limitx.obj,lisib.obj
$ LIB/REPLACE librfot mono4.obj,piant5.obj,profi5.obj,proone.obj,realin.obj,smain.obj,smoot.obj,tmask.obj,wrilin.obj,ran3.obj,mima.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
