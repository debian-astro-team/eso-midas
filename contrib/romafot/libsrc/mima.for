C @(#)mima.for	19.1 (ES0-DMD) 02/25/03 13:29:44
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE MIMA(IMF,FMES,ISIZE,DMI,DMA)                                  
C+++                                                                            
C.PURPOSE:  Determine the minimum and maximum and write into descriptor
C.AUTHOR:   Rein H. Warmels
C.COMMENTS: none
C.VERSION:  890117 RHW documented
C.VERSION:  910115 RHW IMPLICIT NONE added
C---                                                                            
      IMPLICIT      NONE
      INTEGER       IMF
      REAL          FMES(1)
      INTEGER       ISIZE
      REAL          DMA
      REAL          DMI
C
      INTEGER       ISTAT	
      INTEGER       KNUL, KUN
      INTEGER       MADRID(1)
      REAL          VAL(4)                                           
C
      COMMON        /VMR/MADRID                                              
C                                                                               
      CALL MINMAX(FMES,ISIZE,DMI,DMA)
      VAL(1) = DMI                                                              
      VAL(2) = DMA                                                              
      VAL(3) = DMI                                                              
      VAL(4) = DMA                                                              
      CALL STDRDR(IMF,'LHCUTS',VAL,1,4,KUN,KNUL,ISTAT)
C                                                                               
      RETURN                                                                    
      END                                                                       


      SUBROUTINE MINMAX(ARRAY,NDIM,FMIN,FMAX)                                   
C+++                                                                            
C.PURPOSE: Determine the minnimum and maximum in an array
C.AUTHOR:  Ch. Ounnas, ESO - Garching                                    
C.VERSION: 810820 Creation                                                
C.VERSION: 910115 RHW IMPLICIT NONE added
C---                                                                            
      IMPLICIT  NONE
      INTEGER   NDIM                                                          
      REAL      FMIN
      REAL      FMAX
C
      INTEGER   MADRID(1)
      INTEGER   N 
      REAL      ARRAY(NDIM)
      REAL      PIX
C
      COMMON   /VMR/MADRID
C
C ***
      FMIN   = ARRAY(1)                                                         
      FMAX   = ARRAY(1)                                                         
C                                                                               
      DO 10 N = 1,NDIM                                                          
         PIX = ARRAY(N)                                                         
         IF (PIX.LT.FMIN) THEN                                                  
            FMIN = PIX                                                          
         ELSE                                                                   
            IF (PIX.GT.FMAX) THEN                                               
              FMAX = PIX                                                        
            ENDIF                                                               
        ENDIF                                                                   
   10 CONTINUE                                                                  
C                                                                               
      RETURN                                                                    
      END                                                                       
