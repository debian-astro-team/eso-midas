C @(#)intsat.for	19.1 (ES0-DMD) 02/25/03 13:29:43
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE INTSAT(MTS,NR,NC,NI,IST,NLI,KSAT)
C+++
C---
      IMPLICIT NONE
      INTEGER  NR
      INTEGER  NC
      INTEGER  MTS(NR,NC)
      INTEGER  NI
      INTEGER  IST(1)
      INTEGER  NLI
      INTEGER  KSAT
C
      INTEGER  I, K
C
      I = IST(NI)
      K = I-1
   20 CONTINUE
      IF (K.LT.1.OR.IABS(MTS(I,2)-MTS(K,2)).GE.2) THEN
         GO TO 10
      ENDIF
C
      IF (MTS(K,4).EQ.0) THEN
         IF (MTS(K,2).EQ.MTS(I,2)-1) THEN
           IF (MTS(K,3).GE.MTS(I,1).AND.MTS(I,3).GE.MTS(K,1)) THEN
             NLI      = NLI+1
             MTS(K,4) = 1
             IST(NLI) = K
           END IF
         END IF
      END IF
      K=K-1
      GO TO 20
C
   10 CONTINUE
      K = I+1
C
   30 CONTINUE
      IF (K.GT.KSAT.OR.IABS(MTS(K,2)-MTS(I,2)).GE.2) THEN
         GO TO 40
      ENDIF
C
      IF (MTS(K,4).EQ.0) THEN
         IF (MTS(K,2).EQ.MTS(I,2)+1) THEN
           IF (MTS(K,3).GE.MTS(I,1).AND.MTS(I,3).GE.MTS(K,1)) THEN
             NLI      = NLI+1
             MTS(K,4) = 1
             IST(NLI) = K
           END IF
         END IF
      END IF
      K=K+1
      GO TO 30

   40 CONTINUE
      RETURN
      END
