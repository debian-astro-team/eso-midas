! @(#)plotree.prg	19.1 (ES0-DMD) 02/25/03 13:27:25
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PLOTTREE.PRG
!.PURPOSE:  MIDAS procedute to plot a table tree
!.USE:      execute as @@ PLOTREE par1 par2 par3 par4 par5, where:
!           par1 = input table
!           par2 = column x1
!           par3 = column y1
!           par4 = column x2
!           par5 = column y2
!.AUTHOR:   J.D.Ponz  ESO - Garching
!.VERSION:  820405 creation
!.VERSION:  870625 Rein Warmels
! ----------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter input table:"
DEFINE/PARAM P2 #1 ?
DEFINE/PARAM P3 #2 ?
DEFINE/PARAM P4 #3 ?
DEFINE/PARAM P5 #4 ?
DEFINE/PARAM P6 0.,0. NUM
!
WRITE/KEYW INPUTR/R/1/2 'P6'
COMPUTE/KEYW DATTIM = M$TIME()
!
!
WRITE/KEYW PLCURSOR/C/1/20  'P1'
WRITE/KEYW PLCURSOR/C/21/20 "TABLE"
WRITE/KEYW PLCURSOR/C/41/20 'P1'
!
RUN MID_EXE:PLOTREE
!
@ purgeplt 'p1'
!
IF MID$PLOT(26:30) .EQ. "SPOOL" THEN               !make the plot if spool is on
   @ sendplot
ENDIF
