! @(#)knn.prg	19.1 (ES0-DMD) 02/25/03 13:27:23
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!
!   KNN.PRG
!
!.AUTHOR
!
!   F. Murtagh, ST-ECF, Garching.      Version 1.0; 16 June 1986
!
!.PURPOSE
!
!   Execute KNNIF.EXE
!
!.KEYWORDS
!
!   K-Nearest Neighbours Discriminant Analysis,
!   Multivariate statistics, Pattern recognition.
!
!.CALLING SEQUENCE
!
! @@ KNN Training_table  No._of_gp.1_members Test_table No._of_NNs
!
!.INPUT/OUTPUT
!
!   Input training set table name.
!   Number - first "number" rows of the input training set belong to
!            group 1; the remainder belong to group 2.
!   Test set table name.
!   Number of nearest neighbours (default: 3)
!
!-------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter input table"
DEFINE/PARAM P2 ? NUMBER "Enter no of rows used as training set"
DEFINE/PARAM P3 ? TABLE "Enter set table name"
DEFINE/PARAM P4 3 NUMBER "no of nearest neighbours""
WRITE/KEYW INPUTI/I/1/1 'P2'
WRITE/KEYW INPUTI/I/2/1 'P4'
RUN CON_EXE:KNNIF
