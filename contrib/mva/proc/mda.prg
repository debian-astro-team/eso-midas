! @(#)mda.prg	19.1 (ES0-DMD) 02/25/03 13:27:24
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!
!   MDA.PRG
!
!.AUTHOR
!
!   F. Murtagh, ST-ECF, Garching.      Version 1.0; 16 June 1986
!
!.PURPOSE
!
!   Execute MDAIF.EXE
!
!.KEYWORDS
!
!   Multiple Discriminant Analysis, Discriminant Factor Analysis,
!   Canonical Discriminant Analysis,
!   Multivariate statistics, Pattern recognition.
!
!.CALLING SEQUENCE
!
! @@ MDA  Input_table  Output_table  Outable_for_eigenvectors.
!
!.INPUT/OUTPUT
!
!   Input table name.
!   Output table name.
!   Eigenvectors output table name (defaults to none).
!
!-------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter input table"
DEFINE/PARAM P2 ? TABLE "Enter output table"
DEFINE/PARAM P3 NO CHAR "Eigenvectors output table"
!
WRITE/KEYW INPUTC/C/1/8 'P3'
!
RUN CON_EXE:MDAIF
