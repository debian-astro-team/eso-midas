! @(#)pca.prg	19.1 (ES0-DMD) 02/25/03 13:27:24
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!
!   PCA.PRG
!
!.AUTHOR
!
!   F. Murtagh, ST-ECF, Garching.      Version 1.0; 9 May 1986
!
!.PURPOSE
!
!   Execute PCARIF.EXE and PCACIF.EXE
!
!.KEYWORDS
!
!   Principal Components Analysis, Karhunen-Loeve expansion,
!   Multivariate statistics, Pattern recognition.
!
!.CALLING SEQUENCE
!
! @@ PCA  Input_table  Output_table  Analysis_option
!     Row_or_col._analysis  Ncols._Output_table  Outable_for_eigenvectors.
!
!.INPUT/OUTPUT
!
!   Input table name.
!   Output table name.
!   Analysis option (3 = PCA of correlation matrix, default; 2 = PCA of
!                  covariance matrix; 1 = PCA of sums of squares and
!                  cross-products matrix).
!   Row/column analysis (note that all results are detd., but that
!                  output is produced for either rows or columns
!                  (cf. so-called "Q-" or "R-mode" factor analysis).
!   Number of columns in output table (no. of principal components)
!                  Defaults to 3.
!   Eigenvectors output table name (defaults to none).
!
!-------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter input table"
DEFINE/PARAM P2 ? TABLE "Enter output table"
DEFINE/PARAM P3 3 NUMBER "analysis option" 1,3
DEFINE/PARAM P4 R CHAR   "analysis mode (R/C)"
DEFINE/PARAM P5 3 NUMBER "no. of PC (output table columns)"
DEFINE/PARAM P6 NO CHAR "output table with eigenvectors"
!
WRITE/KEYW INPUTI/I/1/1 'P3'
WRITE/KEYW RC/C/1/1     'P4'
WRITE/KEYW INPUTI/I/2/1 'P5'
WRITE/KEYW INPUTC/C/1/8 'P6'
!
IF P4(1:1) .EQ. "C" THEN
   RUN CON_EXE:PCACIF
ELSE
   RUN CON_EXE:PCARIF
ENDIF
