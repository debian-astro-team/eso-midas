! @(#)lda.prg	19.1 (ES0-DMD) 02/25/03 13:27:24
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!
!   LDA.PRG
!
!.AUTHOR
!
!   F. Murtagh, ST-ECF, Garching.      Version 1.0; 14 June 1986
!
!.PURPOSE
!
!   Execute LDAIF.EXE
!
!.KEYWORDS
!
!   Fisher's Linear Discriminant Analysis,
!   Multivariate statistics, Pattern recognition.
!
!.CALLING SEQUENCE
!
! @@ LDA Input_table  Output_table
!
!.INPUT/OUTPUT
!
!   Input table name.
!   Output table name.
!
!-------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Input table"
DEFINE/PARAM P2 ? TABLE "Output table"
RUN CON_EXE:LDAIF
