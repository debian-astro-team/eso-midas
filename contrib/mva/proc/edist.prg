! @(#)edist.prg	19.1 (ES0-DMD) 02/25/03 13:27:23
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!
!   EDIST.PRG
!
!.AUTHOR
!
!   F. Murtagh, ST-ECF, Garching.      Version 1.0; 9 May 1986
!
!.PURPOSE
!
!   Execute EDIST.EXE
!
!.KEYWORDS
!
!   Distances, standardization,
!   Multivariate statistics, Pattern recognition.
!
!.CALLING SEQUENCE
!
! @@ EDIST Input_table  Output_table
!
!.INPUT/OUTPUT
!
!   Input table name.
!   Output table name.
!
!-------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter input table"
DEFINE/PARAM P2 ? TABLE "Enter output table"
!
RUN CON_EXE:EDISTIF
