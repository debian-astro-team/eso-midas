! @(#)cmds.prg	19.1 (ES0-DMD) 02/25/03 13:27:23
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!
!   CMDSIF.PRG
!
!.AUTHOR
!
!   F. Murtagh, ST-ECF, Garching.      Version 1.0; 9 May 1986
!
!.PURPOSE
!
!   Execute CMDSIF
!
!.KEYWORDS
!
!   Principal Coordinates Analysis (Gower's),
!   Classical (Torgerson's) Multidimensional Scaling,
!   Multivariate statistics, Pattern recognition.
!
!.CALLING SEQUENCE
!
! @@ CMDSIF Input_table  Output_table  Ncols._Output_table
!
!.INPUT/OUTPUT
!
!   Input table name.
!   Output table name.
!   Number of columns in output table (no. of principal coordinates)
!                  Defaults to 3.
!
!-------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter input table"
DEFINE/PARAM P2 ? TABLE "Enter output table"
DEFINE/PARAM P3 3 NUMBER "no. of PC (output table columns)"
WRITE/KEYW INPUTI/I/1/1 'P3'
RUN CON_EXE:CMDSIF
