! @(#)ca.prg	19.1 (ES0-DMD) 02/25/03 13:27:23
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!
!   CA.PRG
!
!.AUTHOR
!
!   F. Murtagh, ST-ECF, Garching.      Version 1.0; 9 May 1986
!
!.PURPOSE
!
!   Execute CARIF.EXE and CACIF.EXE
!
!.KEYWORDS
!
!   Correspondence analysis, reciprocal averageing, dual scaling,
!   Multivariate statistics, Pattern recognition.
!
!.CALLING SEQUENCE
!
! @@ CA    Input_table  Output_table  Row/Column_analysis
!          Ncols._Output_table  Outable_for_eigenvectors.
!
!.INPUT/OUTPUT
!
!   Input table name.
!   Output table name.
!   Row/column_analysis (defaults to R; note that all results are
!                  detd., but results are output according to choice).
!   Number of columns in output table (no. of factors)
!                  Defaults to 3.
!   Eigenvectors output table name (defaults to none).
!
!-------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter input table"
DEFINE/PARAM P2 ? TABLE "Enter output table"
DEFINE/PARAM P3 R CHAR   "analysis mode (R/C)"
DEFINE/PARAM P4 3 NUMBER "no. of factors (output table columns)"
DEFINE/PARAM P5 NO CHAR "output table with eigenvectors"
WRITE/KEYW RC/C/1/1     'P3'
WRITE/KEYW INPUTI/I/1/1 'P4'
WRITE/KEYW INPUTC/C/1/8 'P5'
IF P3(1:1) .EQ. "C" THEN
   RUN CON_EXE:CACIF
ELSE
   RUN CON_EXE:CARIF
ENDIF
