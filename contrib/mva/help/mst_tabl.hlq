% @(#)mst_tabl.hlq	19.1 (ESO-IPG) 02/25/03 13:27:06
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      mst_tabl.hlq
%.AUTHOR     BP/JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, MST/TABLE
%.PURPOSE    On-line help file for the command: MST/TABLE
%.VERSION    1.0  OCT-1985 : Creation, BP/JDP
%----------------------------------------------------------------
\se
SECTION./TABLE
\es\co
MST/TABLE						OCT-1985  BP/JDP
\oc\su
MST/TABLE  intable outtable grid_size
	create minimal spanning tree for position table
\us\pu
Purpose:      Create a minimal spanning tree from a table containing
              points positions.

\up\sy
Syntax:       MST/TABLE intable outtable grid_size
\ys\pa
              intable   = input table (with two columns called :X and :Y)
\ap\pa
              outtable  = table with the structure :
                          :X1 :Y1 :X2 :Y2 :DIST :POINT_NB1 :POINT_NB2
                          (one entry per link created)
\ap\pa
              grid_size = use 50.

\ap\no
Note:         none

\on\exs
Examples:
\ex
              MST/TABLE MYTAB MST 50.
              Compute the minimal spanning tree from the table MYTAB
              giving  table MST as result
\xe \sxe
