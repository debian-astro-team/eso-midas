% @(#)mda_tabl.hlq	19.1 (ESO-IPG) 02/25/03 13:27:05
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      mda_tab.hlq
%.AUTHOR     FM, IPG/ESO
%.KEYWORDS   MIDAS, help files, MDA/TAB
%.PURPOSE    On-line help file for the command: MDA/TAB
%.VERSION    1.0  28-AUG-1986 : Creation, FM
%----------------------------------------------------------------
\se
SECTION./TABL
\es\co
MDA/TAB							28-AUG-1986  FM
\oc\su
MDA/TAB  input_table  output_table  eigenvectors
	discriminant analysis
\us\pu
Purpose:   Multiple Discriminant Analysis, Discriminant Factor
           Analysis, Canonical Discriminant Analysis.
\up\sy
Syntax:    MDA/TAB  input_table  output_table  eigenvectors
\ys\pa
           input_table = input table name.
\ap\pa
           output_table = output table name.
\ap\pa
           eigenvectors = output table name (defaults to none).
\ap\no
Note:      Singularities, caused by linear dependance, can be circumvented
           by first carrying out a PCA, and using a small number of principal
           components as input to LDA.
           Singularity problems may arise in matrix inversion, due to
           very small values: if you get an IERR=2 message, multiplying
           the input data by an arbitrary constant may painlessly get
           around the difficulty.

\on\exs
Examples:
\ex
           MDA/TAB INTAB OUTAB
\xe \sxe
