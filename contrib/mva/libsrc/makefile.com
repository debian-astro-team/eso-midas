$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.MVA.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:55 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   ca.for
$ FORTRAN   clst.for
$ FORTRAN   clussbr.for
$ FORTRAN   cmds.for
$ FORTRAN   densbr.for
$ FORTRAN   edist.for
$ FORTRAN   knn.for
$ FORTRAN   lda.for
$ FORTRAN   mda.for
$ FORTRAN   mvautil.for
$ FORTRAN   part.for
$ FORTRAN   pca.for
$ FORTRAN   seesbr.for
$ LIB/REPLACE libmva ca.obj,clst.obj,clussbr.obj,cmds.obj,densbr.obj,edist.obj
$ LIB/REPLACE libmva knn.obj,lda.obj,mda.obj,mvautil.obj,part.obj,pca.obj,seesbr.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
