! @(#)pos1a.prg	19.1 (ES0-DMD) 02/25/03 13:23:10
! @(#)pos1a.prg	19.1 (ESO-SDAG) 02/25/03 13:23:10
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT:      pos1a.prg
!.AUTHOR:     
!.KEYWORDS:   POS1, astrometry
!.PURPOSE:    
!.USE:        @c pos1 standard mes plate_epoch cat_epoch sch/lsb tol
!                        1      2      3          4         5     6            
!                     x_terms,yterms  std
!                        7         8
!             refer to pos1.f for the details
!--------------------------------------------------------
defi/par p1 ppm ? "Standard file "
if p1 .eq. "??" then
  writ/out "@c pos1a std mes center pla,cat  schmidt,blink tol xterm,yterm std"
  writ/out "         ppm xy  MEAN 1985,2000 YN 5 111110000,111110000 A"
  writ/out "         compute the astrometric transformation parameters" 
  return
endif

defi/par p2 cernis ? "Measurement file "
defi/par p3 MEAN C "Center of Plate (h,m,s,d,m,s), MEAN for center of the field"
defi/par p4 2000,2000. n "Plate epoch, Catalogue epoch?"
defi/par p5 SN ? "Schmidt[S/N] & La_Silla_blink[B/N] flags "
defi/par p6 5 n "tolerance in mu-m "
defi/par p7 111000000,111000000 ? "X/Y terms  "
defi/par p8 U ? "All standards (A)/ Undeleted std only (U)"

defi/loc IN_STD/c/1/16 {p1}
defi/loc IN_MES/c/1/16 {p2}
defi/loc CENTCOOR/c/1/72 {p3}
defi/loc W_EPOCH/d/1/2 {p4}
defi/loc EPO_PLA/d/1/1 {W_EPOCH(1)}
defi/loc EPO_CAT/d/1/1 {W_EPOCH(2)}
defi/loc FLAGS/c/1/2 {p5}
defi/loc SIG/d/1/1 {p6}
defi/loc TERMS/c/1/19 {p7}
defi/loc STDSEL/C/1/1 {p8}

!- output transformation matrix
writ/key BX/D/1/9 0. all
writ/key BY/D/1/9 0. all
writ/key CX/D/1/1 0.
writ/key CY/D/1/1 0.
writ/key nx/i/1/1 0
writ/key ny/i/1/1 0
writ/key AL_DE0/D/1/4 0. ALL

if stdsel .eq. "A" comp/tab {p1} :std = 0
selec/tab {IN_STD} :std .eq. 1

run CON_EXE:pos1a
writ/out "*** INFO: Updating descriptors"
copy/KD EPO_PLA/d/1/1 {IN_MES}.tbl EPO_PLA/d/1/1
copy/KD EPO_CAT/d/1/1 {IN_MES}.tbl EPO_CAT/d/1/1
copy/KD FLAGS/c/1/2 {IN_MES}.tbl FLAGS/c/1/2 
copy/kd SIG/d/1/1 {IN_MES}.tbl SIG/d/1/1
copy/kd TERMS/c/1/19 {IN_MES}.tbl TERMS/c/1/19 

copy/KD  BX/D/1/9 {IN_MES}.tbl BX/D/1/9 
copy/KD  BY/D/1/9 {IN_MES}.tbl BY/D/1/9 
copy/KD  CX/D/1/1 {IN_MES}.tbl CX/D/1/1 
copy/KD  CY/D/1/1 {IN_MES}.tbl CY/D/1/1
copy/KD  nx/i/1/1 {IN_MES}.tbl nx/i/1/1
copy/KD  ny/i/1/1 {IN_MES}.tbl ny/i/1/1
COPY/KD  AL_DE0/D/1/4 {IN_MES}.tbl AL_DE0/D/1/4
