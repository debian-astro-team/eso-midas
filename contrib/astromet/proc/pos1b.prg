! @(#)pos1b.prg	19.1 (ES0-DMD) 02/25/03 13:23:10
! @(#)pos1b.prg	19.1 (ESO-SDAG) 02/25/03 13:23:10
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT:      pos1b.prg
!.AUTHOR:     
!.KEYWORDS:   POS1, astrometry
!.PURPOSE:    
!.USE:        @c pos1 standard mes plate_epoch cat_epoch sch/lsb tol
!                        1      2      3          4         5     6            
!                     x_terms,yterms  std
!                        7         8
!
! (1) Calculate (A,D) for plate center and several (X,Y)-values'/ 
! (2) Calculate (A,D) for several (X,Y)-values only'/ 
! (3) Calculate (A,D) for one (X,Y)-value'/ 
! (4) Calculate (X,Y) for one (A,D)-position'/
! (5) Calculate (X,Y) for several (A,D)s in disc file'/
!-----------------------------------------------------------------

defi/par p1 cernis ? "Measurement file "
if p1 .eq. "??" then
 write/out "@c pos1b mes option output trail"
 write/out "(1) Calculate (A,D) for plate center and several (X,Y)-values"
 write/out "(2) Calculate (A,D) for several (X,Y)-values only"
 write/out "(3) Calculate (A,D) for one (X,Y)-value"
 write/out "(4) Calculate (X,Y) for one (A,D)-position"
 write/out "(5) Calculate (X,Y) for several (A,D)s in disc file"
 return
 endif
defi/par p2 2 n "Option?"
defi/par p3 pos1 ? "Output table?"
defi/par p4 N ? "Flag"


defi/loc IN_MES/c/1/16 {p1}
defi/loc OPT/i/1/1 {p2}
defi/loc out_mes/c/1/16 {p3}.tbl
defi/loc flag/c/1/1 {p4}

RUN CON_EXE:pos1b






