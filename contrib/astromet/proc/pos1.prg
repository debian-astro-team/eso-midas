! @(#)pos1.prg	19.1 (ES0-DMD) 02/25/03 13:23:10
! @(#)pos1.prg	19.1 (ESO-SDAG) 02/25/03 13:23:10
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       pos1.prg
!.AUTHOR      
!.KEYWORDS    
!.PURPOSE     
!--------------------------------------------------------
writ/out
writ/out "POS1 - MIDAS version"
writ/out


defi/loc F1/c/1/1 Y
defi/loc F2/c/1/1 N
defi/loc IN_STD/c/1/16 " "
defi/loc IN_MES/c/1/16 " "
defi/loc SIG/d/1/1 5
defi/loc EPO_PLA/d/1/1 0.
defi/loc EPO_CAT/d/1/1 0.
defi/loc STDSEL/C/1/1 A
defi/loc tx/c/1/9 "111000000"
defi/loc ty/c/1/9 "111000000"
defi/loc yon/c/1/1 Y
defi/loc opt/i/1/1 0
defi/loc out_mes/c/1/16 " "
defi/loc flag/c/1/1 " "
defi/loc CENTCOOR/c/1/72 {p3}

inqu/key F1 "Is this a Schmidt Plate (Y or N)?              > "
inqu/key F2 "Was the Plate measured on the La Silla Blink?  > "
inqu/key IN_STD "Give name of standard stars table:             > "
inqu/key IN_MES "and name of (X,Y)-table:                       > "
inqu/key SIG "Give permitted r.m.s for (X,Y) in microns (e.g. 5.0): >"
inqu/key EPO_PLA "Epoch of the plate?                            > "
inqu/key EPO_CAT "Epoch of the standard catalogue?               > "
inqu/key CENTCOOR  "Center of Plate (h,m,s,d,m,s), MEAN for center of the field"

again:
writ/out By typing (1) or (0),
writ/out decide whether to use(1) or not to use(0)
writ/out "        X Y XY X**2 Y**2 X**3 Y**3 XY**2 YX**2"
inqu/key tx "IN X ([ret]={tx})?                           > " 
inqu/key ty "IN Y ([ret]={ty})?                           > " 

writ/out "* calling POS1A..."
echo/ful
@c pos1a {IN_STD} {IN_MES} "{CENTCOOR}" {EPO_PLA},{EPO_CAT} {F1}{F2} {SIG} {TX},{TY} {STDSEL}
echo/off

inqu/key yon "ACCURACY OK? "
if yon .ne. "y" .and. yon .ne. "Y" then
!  cre/grap   
  @c residual {IN_STD} {YON}
  stdsel = "U"
  goto again
  endif



writ/out   ACCURACY ASSUMED SATISFACTORY
writ/out  
menu:
writ/out  
writ/out   CHOOSE MODE:
writ/out 
writ/out  (1) CALCULATE (A,D) FOR PLATE CENTER AND SEVERAL (X,Y)-VALUES
writ/out  (2) CALCULATE (A,D) FOR SEVERAL (X,Y)-VALUES ONLY
writ/out  (3) CALCULATE (A,D) FOR ONE (X,Y)-VALUE
writ/out  (4) CALCULATE (X,Y) FOR ONE (A,D)-POSITION
writ/out  (5) CALCULATE (X,Y) FOR SEVERAL (A,D)S IN DISC FILE
writ/out  (6) NEW RUN OF POS1
writ/out  (7) EXIT PROGRAMME

inqu/key opt  "Mode No? > "

if opt .ge. 6 return

if opt .le. 2 then
  inqu/key flag    "MEAN POSITIONS (EG, MOVING OBJECT TRAIL)? y/n > "
  inqu/key out_mes "GIVE OUTPUT TABLE NAME                   > "
  endif

@c pos1b {IN_MES} {OPT} {OUT_MES} {FLAG}

goto menu

