C @(#)qkdin.for	19.1 (ES0-DMD) 02/25/03 13:23:05
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C @(#)qkdin.for	19.1 (ESO-SDAG) 02/25/03 13:23:05
c----------------------------------------------------------------------
      SUBROUTINE QKDIN(A,N) 
c----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
      DIMENSION A(9,9)

      DO 1 I=1,N
         X=A(I,I)
         A(I,I) = 1.0D0
         DO 2 J=1,N
 2          A(I,J)=A(I,J)/X 
         DO 1 K=1,N
            IF (K-I) 3,1,3

 3          X=A(K,I)
            A(K,I)=0.0D0
            DO 4 J=1,N
 4             A(K,J)=A(K,J)-X*A(I,J)

 1    CONTINUE         
      RETURN
      END 

