C @(#)xyad.for	19.1 (ES0-DMD) 02/25/03 13:23:05
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C @(#)xyad.for	19.1 (ESO-SDAG) 02/25/03 13:23:05
c----------------------------------------------------------------------
      SUBROUTINE XYAD(AKSI,ANU,IA1,ID1,DA1S,DD1S,ITEGN1,ALFAS,DELTS,
     *COSD0,SIND0,ALFA0)
c----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
      DIMENSION ALFA(3),DELTA(3),IA1(3),ID1(3)
      CHARACTER*2 ITEGN1

      PI = 4.0D0*DATAN(1.0D0)
      DOPI18 = PI/1.8D2
      DO1 = 1.0D0
      RH = DSQRT(AKSI**2 + ANU**2)
      SINPH = AKSI/RH 
      COSPH = ANU/RH
      RH = RH*DOPI18
      SINRH= DSIN(RH) 
      COSRH = DCOS(RH)
      DELTS = COSRH*SIND0+SINRH*COSD0*COSPH 
      DELTS = DATAN(DELTS/DSQRT(DO1-DELTS**2))
      ALFAS = SINRH*SINPH/DCOS(DELTS) 
      ALFAS = ALFA0 + DATAN(ALFAS/DSQRT(DO1-ALFAS**2))

      CALL ADCON(ALFAS,DELTS,ALFA,DELTA)

      DO 670 J=1,2
        IA1(J) = IDINT(ALFA(J)+1.0D-3)
  670   ID1(J) = IDINT(DELTA(J)+DSIGN(1.0D-3,DELTA(J))) 

      DA1S = ALFA(3)
      DD1S = DELTA(3) 
      ID1(1) = IABS(ID1(1)) 
      ITEGN1 = ' +' 
C      IF (DELTS.LT.DO0) ITEGN1 = ' -' 
      IF (DELTS.LT. 0.0D0) ITEGN1 = ' -' 
      RETURN
      END 

