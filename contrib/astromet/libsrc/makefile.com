$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.ASTROMET.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:53 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   adcon.for
$ FORTRAN   adxy.for
$ FORTRAN   blinkxy.for
$ FORTRAN   mean.for
$ FORTRAN   qkdin.for
$ FORTRAN   rar.for
$ FORTRAN   xyad.for
$ LIB/REPLACE libpos1 adcon.obj,adxy.obj,blinkxy.obj,mean.obj,qkdin.obj,rar.obj,xyad.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
