C @(#)adcon.for	19.1 (ES0-DMD) 02/25/03 13:23:04
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C @(#)adcon.for	19.1 (ESO-SDAG) 02/25/03 13:23:04
cc----------------------------------------------------------------------
      SUBROUTINE ADCON(ALFAM,DELTM,ALFA,DELTA)
c----------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
      DIMENSION ALFA(3),DELTA(3)

      DO0 = 0.0D0
      DO1 = 1.0D0
      DO12 = 1.2D1
      DO24 = 2.4D1
      DO60 = 6.0D1
      PI = 4.0D0*DATAN(1.0D0)
      DOPI18 = PI/1.8D2
      ALFAS = ALFAM*DO12/PI + 2.8D-9

      IF (ALFAS.GE.DO24) ALFAS = ALFAS -DO24
      IF (ALFAS.LT.DO0) ALFAS = ALFAS + DO24
      ALFA(1) = DINT(ALFAS)
      ALFAS = (ALFAS-ALFA(1))*DO60
      ALFA(2) = DINT(ALFAS)
      ALFA(3) = (ALFAS-ALFA(2))*DO60

      DELTS = DELTM/DOPI18 + DSIGN(1.0D0,DELTM)*2.8D-8
      DELTA(1) = DINT(DELTS) 
      DELTS = (DELTS-DELTA(1))*DO60 
      DELTA(2) = DINT(DELTS) 
      DELTA(3) = (DELTS-DELTA(2))*DO60
      DELTA(2) = DABS(DELTA(2)) 
      DELTA(3) = DABS(DELTA(3)) 

      IF(DELTA(3).NE.DO60) return
      DELTA(3) = DO0
      DELTA(2) = DELTA(2) + DO1 

      IF (DELTA(2).NE.DO60) return
      DELTA(2) = DO0
      DELTA(1) = DELTA(1) + DSIGN(DO1,DELTA(1)) 

      RETURN
      END 

