c                                   
c                                   
c                                                                               
      real function gamlog(x0) 
c implementation of Lanczos approximation, 
c based on ideas from Numerical Recipes (Press et. al., 1986)
      implicit none
      integer i
      real x0
      double precision lancz(6),one,half,x,scal,sav 
      parameter (one=1d0,half=0.5d0)
      data lancz/
     $     76.18009173d+0,-86.50532033d+0,24.01409822d0, 
     $    -1.231739516d+0, 0.120858003d-2,-0.536382d-5/
      x=x0
      scal=x+4.5d0
      sav=one
      do 1 i=1,6
        sav=sav+lancz(i)/x
        x=x+one
 1    continue
      gamlog=log(scal)*(x0-half)-scal
     $  +log(sav*2.50662827465d0)
      end
c
c
c
