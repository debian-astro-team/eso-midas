C @(#)acf.for	19.1 (ES0-DMD) 02/25/03 13:32:57
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)acf.for	5.1 (ESO-IPG) 4/5/93 15:58:26
C
C
C
      DOUBLE PRECISION FUNCTION ACFLIN(TLAG,PARM)
      DOUBLE PRECISION TLAG,PARM(12)
      ACFLIN=PARM(1)+PARM(2)*ABS(TLAG)
      END
C
C
C
      DOUBLE PRECISION FUNCTION ACFPOL(TLAG,PARM)
      DOUBLE PRECISION TLAG,PARM(12),SAV
      INTEGER I
      SAV=0.
      DO 1 I=12,1,-1
        SAV=SAV*ABS(TLAG)+PARM(I)
 1    CONTINUE
      ACFPOL=SAV
      END
C
C
C
      DOUBLE PRECISION FUNCTION ACFPOW(TLAG,PARM)
      DOUBLE PRECISION TLAG,PARM(12)
      ACFPOW=PARM(1)+PARM(2)*ABS(TLAG)**PARM(3)
      END
C
C
C
      DOUBLE PRECISION FUNCTION ACFEXP(TLAG,PARM)
      DOUBLE PRECISION TLAG,PARM(12)
      ACFEXP=PARM(1)+PARM(2)*EXP(PARM(3)*ABS(TLAG))
      END
C
C
C
      DOUBLE PRECISION FUNCTION ACFLOG(TLAG,PARM)
      DOUBLE PRECISION TLAG,PARM(12)
      ACFLOG=PARM(1)+PARM(2)*LOG(PARM(3)*ABS(TLAG))
      END
C
C
C
      DOUBLE PRECISION FUNCTION ACFIPO(TLAG,PARM)
      DOUBLE PRECISION TLAG,PARM(12),SAV,X
      INTEGER I
      X=1./(ABS(TLAG)-PARM(1))
      SAV=0.
      DO 1 I=12,2,-1
        SAV=SAV*X+PARM(I)
 1    CONTINUE
      ACFIPO=SAV
      END
C
C
C

