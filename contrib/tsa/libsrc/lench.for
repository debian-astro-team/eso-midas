C @(#)lench.for	19.1 (ES0-DMD) 02/25/03 13:32:58
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C  @(#)lench.for	5.1 (ESO-IPG) 4/5/93 15:58:34
      INTEGER FUNCTION LENCH(STRING)
C
C    Find length of non-trivial string, i.e.
C    of string with trailing nonprintable characters truncated
C
      IMPLICIT NONE
      CHARACTER*(*) STRING
C
      CHARACTER*256 CHSET
      INTEGER       LEN,LENGTH,NC,NSET
C
      CHSET=
     $   '~!@#$%^&*()_+|`1234567890-=\\{}[]:";''<>?,./'//
     $   'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm'
C2345678901234567890123456789012345678901234567890123456789012345678901234567890
C        1         2         3         4         5         6         7
      NSET=42+52
      LENCH=LEN(STRING)
      DO 1 LENGTH=LEN(STRING),1,-1
        DO 2 NC=1,NSET
          IF(STRING(LENGTH:LENGTH).EQ.CHSET(NC:NC)) THEN
            GOTO 9
          ENDIF
 2      CONTINUE
        LENCH=LENGTH-1
 1    CONTINUE
 9    END
