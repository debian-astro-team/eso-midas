$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.TSA.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:52 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   acf.for
$ FORTRAN   cracow.for
$ FORTRAN   inpcpy.for
$ FORTRAN   interpol.for
$ FORTRAN   lench.for
$ FORTRAN   sort.for
$ FORTRAN   timdft.for
$ FORTRAN   riembi.for
$ FORTRAN   rbfrac.for
$ FORTRAN   gamlog.for
$ LIB/REPLACE libtsa acf.obj,cracow.obj,inpcpy.obj,interpol.obj,lench.obj,sort.obj,timdft.obj,riembi.obj,rbfrac.obj,gamlog.obj
$ FORTRAN   tsadelur0.for
$ FORTRAN   tsadelur1.for
$ FORTRAN   tsadelur2.for
$ FORTRAN   tsadelur3.for
$ FORTRAN   tsadelur4.for
$ FORTRAN   tsadelur5.for
$ FORTRAN   tsadelur6.for
$ FORTRAN   tsadelur7.for
$ FORTRAN   tsadelur8.for
$ FORTRAN   tsadelur9.for
$ LIB/REPLACE libtsausr tsadelur0.obj,tsadelur1.obj,tsadelur2.obj,tsadelur3.obj,tsadelur4.obj,tsadelur5.obj,tsadelur6.obj,tsadelur7.obj,tsadelur8.obj,tsadelur9.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
