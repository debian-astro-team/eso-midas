% @(#)band_tsa.hlq	19.1 (ESO-IPG) 02/25/03 13:32:44
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
%.COPYRIGHT (c) 1992 European Southern Observatory & Copernicus Astron. Center%.IDENT     band_tsa.hlq
%.AUTHOR    Alex Schwarzenberg-Czerny, ESO & Copernicus Astronomical Center 
%.KEYWORD   MIDAS, help files, BAND/TSA
%.LANGUAGE  LATEX
%.PURPOSE   Evaluate frequency band for time series analysis
%.VERSION   0.0               June 1992
%.RETURNS   None  
%.ENVIRON   TSA context
%------------------------------------------------------------------------------
\se
SECTION./TSA
\es\co
BAND/TSA                                 15-SEPT-1992 A. Schwarzenberg-Czerny
\oc\su
BAND/TSA intab [maxobs]      Evaluate frequency band for time series analysis
\us\pu
Purpose: 
           Sampling times of observations are analysed and a suitable frequency
           band and resolution are derived. The keywords defining the 
           frequency grid STARTTSA, STEPTSA and NSTEPS are set. 
	   This command requires observations sorted in ascending order in time.
\up\sy
Syntax: 
           BAND/TSA intab [maxobs]
\ys\pa
           intab  = name of input table, it must contain columns :TIME and 
	            :VALUE in DOUBLE PRECISION.
\ap\pa
           maxobs = maximum number of observations analysed for separation.
                    The default value maxobs=0 causes use of all observations.
                    This causes unnecessary waste of time for uniform or 
                    randomly  distributed observations with only few large gaps.
                    In such a case supply a number of initial observations 
                    whose spacing is representative for the whole data.
\ap\sa
See also:
           SHOW/TSA, POWER/TSA, AOV/TSA, SCARGLE/TSA
\as\no
Note: 
           none
\on\exs
Examples:
\ex
           BAND/TSA LCURVE 
\xe\ex
           BAND/TSA LCURVE 100
\xe\sxe
