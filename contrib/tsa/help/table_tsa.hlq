% @(#)table_tsa.hlq	19.1 (ESO-IPG) 02/25/03 13:32:45
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
%.COPYRIGHT (c) 1992 European Southern Observatory & Copernicus Astron. Center%.IDENT     table_time.prg
%.AUTHOR    Alex Schwarzenberg-Czerny, ESO & Copernicus Astronomical Center 
%.KEYWORD   MIDAS, help files, TABLE/TSA
%.LANGUAGE  LATEX
%.PURPOSE   Convert ASCI table into MIDAS table
%           in a more flexible way as CREATE/TABLE
%.VERSION   0.0               June 1992
%.RETURNS   None  
%.ENVIRON   TSA context
%-----------------------------------------------------------------------------
\se
SECTION./TSA
\es\co
TABLE/TSA                                15-SEPT-1992 A.Schwarzenberg-Czerny
\oc\su
TABLE/TSA inascii [outtab] [type] [mxcol]      convert ASCI table into MIDAS table
\us\pu
Purpose: 
        Convert ASCII table into MIDAS table
\up\sy

Syntax: 
        TABLE/TSA inascii outtab type mxcol
\ys\pa
        inascii = name of input ASCII file with extension .dat. The file should
                  contain data written in columns of FORTRAN free format 
                  consistent with the conversion type. Text lines in this file
                  if any are displayed and otherwise ignored.
\ap\pa
        outtab  = name of MIDAS output table (default - 'inascii')
\ap\pa
        type    = conversion type for numerical data: R,D or I (default - D).
\ap\pa
        maxcol  = maximum number of columns scanned in 'inascii', need not be 
                  the number of actually present columns (default - 10).
\ap\sa
See also:
        CREATE/TABLE, COMPUTE/TABLE
\as\no
Note:   
        The default column labels :RVALi (DVALi or IVALi), (i=1,...) can
        be changed with NAME/COLUMN to those requested by a particular
        command.
\on\exs
Examples:
\ex
TABLE/TSA table                      produces table.tbl in D(ouble) format
\xe\ex
TABLE/TSA ascii data D 20            produces table with up to 20 D columns
\xe\sxe
