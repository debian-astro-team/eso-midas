C @(#)tsa_def.inc	19.1 (ES0-DMD) 02/25/03 13:32:53
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
C.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
C.IDENT     my_def.inc
C.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsawe Observatory 
C.KEYWORD   ????????????
C.LANGUAGE  FORTRAN 77
C.PURPOSE   Standard definitions
C.VERSION   0.0               June 1992
C.RETURNS   None 
C.ENVIRON   TIME/ANAL context
C-----------------------------------------------------------------------------
C
      INTEGER  ISTAT
      INTEGER  ISTORE
      DOUBLE PRECISION   ZERO,ONE,HALF,TWO
C
