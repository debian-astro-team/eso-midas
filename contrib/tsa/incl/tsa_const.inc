C @(#)tsa_const.inc	19.1 (ES0-DMD) 02/25/03 13:32:53
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
C.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
C.IDENT     MY_const.INC
C.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsawe Observatory 
C.KEYWORD   MIDAS, time analysis 
C.LANGUAGE  FORTRAN 77
C.PURPOSE   Standard definitions
C.VERSION   0.0               June 1992
C.RETURNS   None 
C.ENVIRON   TSA context
C-----------------------------------------------------------------------------
C
      DATA         ISTORE,ISTAT /1,0/
      DATA         ZERO,ONE,HALF,TWO/0D0,1D0,0.5D0,2D0/
C
