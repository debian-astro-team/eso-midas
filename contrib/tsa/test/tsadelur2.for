C @(#)tsadelur2.for	10.2 (ES0-DMD) 1/16/96 00:53:01
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C                                                                               
C                                                                               
C                                                                               
C    TSADELUR2                                                                        
C                                                                               
C   Autocorrelation function of data in analytic form, for FITCOR
C   An example from Ap.J. 385,404 (case B)               
C                                                                               
C   Input:                                                                      
C   TLAG                          - time lag                                      
C                                                                               
C   Output:                                                                     
C   TSADELUR2                     - ACF function value                            
C                                                                               
      DOUBLE PRECISION FUNCTION TSADELUR2(TLAG,PARM)                                                   
      DOUBLE PRECISION TLAG, PARM(12)
      TSADELUR2=0.2**2-(0.011*ABS(TLAG)**0.34)**2                                     
      END                                                                       
