! @(#)list.prg	19.1 (ESO-IPG) 02/25/03 13:33:34
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
!
!        Test procedure for TSA
!
WRITE/OUT "SET/CONTEXT TSA"
SET/CONTEXT TSA
!
! INITIALIZE/TSA �Initialize global keywords
! SET/TSA�        Update global keywords
! SHOW/TSA�       Show values of global keywords
!
!   Get periodic data
!
WRITE/OUT " Get periodic data"
CRE/TAB pd * * pd.dat pd
!
! NORMALIZE/TSA  �Mean and Variance to 0, 1
WRITE/OUT " NORMALIZE/TSA  �Mean and Variance to 0, 1"
NORMALIZE/TSA pd ? :TIME
NORMALIZE/TSA pd tt :VALUE V
!
! BAND/TSA       �Evaluate suitable frequency band
WRITE/OUT " BAND/TSA       �Evaluate suitable frequency band"
BAND/TSA pd 15
BAND/TSA pd 
!
! SET/TSA�        Update global keywords
WRITE/OUT " SET/TSA�        Update global keywords"
SET/TSA STEPTSA=0.1 NSTEPS=601
!
! AOV/TSA         Analysis of Variance periodogramme
WRITE/OUT " AOV/TSA         Analysis of Variance periodogramme"
AOV/TSA pd ti 
!
! SCARGLE/TSA    �Scargle periodogramme
WRITE/OUT " SCARGLE/TSA    �Scargle periodogramme"
SCARGLE/TSA  
!
! POWER/TSA       Power spectrum
WRITE/OUT " POWER/TSA       Power spectrum"
POWER/TSA pd ti 
!
! WIDTH/TSA      �Find width of a line
WRITE/OUT " WIDTH/TSA      �Find width of a line"
WIDTH/TSA ti 5 22
!
! SINEFIT/TSA�    Fit data with a sine curve
WRITE/OUT " SINEFIT/TSA�    Fit data with a sine curve"
SINEFIT/TSA pd tt 22.5 2 1
SINEFIT/TSA pd tt 22.5 2 -3
!
!   Get aperiodic data
!
WRITE/OUT " Get aperiodic data"
CRE/TAB ad1 * * ad1.dat ad
CRE/TAB ad2 * * ad2.dat ad
!
! COVAR/TSA�      Compute discrete covariance function
WRITE/OUT " COVAR/TSA�      Compute discrete covariance function"
COVAR/TSA ad1 ad1 tt 1 0.1 24 LOG
!
! DELAY/TSA�      Compute delay graph of Chi2
WRITE/OUT " DELAY/TSA�      Compute delay graph of Chi2"
DELAY/TSA ad1 ad2 tt 0 530 2 POW 0.02,-0.000121,0.68
DELAY/TSA ad1 ad2 tt 0 530 2 UR2 
!
! INTERPOL/TSA�   Interpolate a series
!
! TABLE/TSA       Flexible Create/Table
!
