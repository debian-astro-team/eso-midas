!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992-2011     ESO and Warsaw Observatory
!.IDENT     test.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   MIDAS, time analysis, testing
!.LANGUAGE  MIDAS
!.PURPOSE   Test the TSA package
!.VERSION   0.0          Oct. 1992
!	    0.5		 Oct. 2011
!.RETURNS   Communique
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!        Test procedure for TSA
!
!     Usage:
!     Copy this file, imtst.prg, tbtst.prg, data files pd.dat, ad.dat
!     and format files pd.fmt, ad1.fmt, ad2.fmt into your work directory.
!     Start MIDAS by inmidas or equivalent procedure
!     Start test routine by @@ test. The test result is printed
!     at its completion. Any errors are indicated by *** in the test
!     output.
!
!
WRITE/OUT "SET/CONTEXT TSA"
SET/CONTEXT TSA
!
INITIALIZE/TSA Initialize global keywords
WRITE/KEY TOLER/R/1/1 0.0001
WRITE/KEY TSA_RESULT/C/1/10 "PASSED"
! SET/TSA        Update global keywords
! SHOW/TSA       Show values of global keywords
!
!   Get periodic data
!
WRITE/OUT " Get periodic data"
CRE/TAB pd * * pd.dat pd
! test mean and variance
@@ tbtst pd :VALUE 0.0029474 0.082190
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
!
! NORMALIZE/TSA  Mean and Variance to 0, 1
WRITE/OUT " NORMALIZE/TSA  Mean and Variance to 0, 1"
NORMALIZE/TSA pd pd :TIME
NORMALIZE/TSA pd pd :VALUE
NORMALIZE/TSA pd tt :VALUE V
@@ tbtst tt :VALUE 0.0 1.0
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
! test mean,var
!
! BAND/TSA       Evaluate suitable frequency band
WRITE/OUT " BAND/TSA       Evaluate suitable frequency band"
BAND/TSA pd 15
BAND/TSA pd 
! test keywords
COMPUTE/KEY INPUTR(1) = M$ABS(STEPTSA-0.23919)+M$ABS(181.-NSTEPS)/100.
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
!
! SET/TSA        Update global keywords
WRITE/OUT " SET/TSA        Update global keywords"
SET/TSA STEPTSA=0.1 NSTEPS=601
! test keywords
COMPUTE/KEY INPUTR(1) = M$ABS(STEPTSA-0.1)+M$ABS(601.-NSTEPS)/100.
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF
!
! AOV/TSA         Analysis of Variance periodogramme
WRITE/OUT " AOV/TSA         Analysis of Variance periodogramme"
AOV/TSA pd ti
! test mean and variance 
@@ imtst ti 1 1.21838 0.85369
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
!
! ORT/TSA         Multiharmonic Fourier spectrum
WRITE/OUT " ORT/TSA         Multiharmonic Fourier spectrum"
WRITE/KEY ORDERTSA 2
ORT/TSA pd ti
! test mean and std.var. 
!@@ imtst ti 1 0. 1.
@@ imtst ti 1 1.173272 1.334589
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF
!
! SCARGLE/TSA    Scargle periodogramme
WRITE/OUT " SCARGLE/TSA    Scargle periodogramme"
SCARGLE/TSA  
! test mean and variance 
@@ imtst ti 1 0.97348 0.86260
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
!
! POWER/TSA       Power spectrum
WRITE/OUT " POWER/TSA       Power spectrum"
POWER/TSA pd ti 
! test mean and variance 
@@ imtst ti 1 3.42758E-4 3.07303E-4
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
!
! WIDTH/TSA      Find width of a line
WRITE/OUT " WIDTH/TSA      Find width of a line"
WIDTH/TSA ti 5 22
! test keywords
COMPUTE/KEY INPUTR(1) = M$ABS(STARTTSA-23.011)/23.+M$ABS(OUTPUTD(1)-0.115804)
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
!
! SINEFIT/TSA    Fit data with a sine curve
WRITE/OUT " SINEFIT/TSA    Fit data with a sine curve"
SINEFIT/TSA pd tt 22.5 2 1
SINEFIT/TSA pd tt 22.5 2 -3
! test STARTTSA and mean and variance in residuals
COMPUTE/KEY INPUTR(1) = M$ABS(STARTTSA-23.0369)/23.
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
@@ tbtst tt :VALUE 0.0049254 0.057986
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF
!
!   Get aperiodic data
!
WRITE/OUT " Get aperiodic data"
CRE/TAB ad1 * * ad1.dat ad
CRE/TAB ad2 * * ad2.dat ad
! test mean and variance 
@@ tbtst ad1 :VALUE 0.45168 0.087514
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
@@ tbtst ad2 :VALUE 0.53305 0.12147
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
!
! COVAR/TSA      Compute discrete covariance function
WRITE/OUT " COVAR/TSA      Compute discrete covariance function"
COVAR/TSA ad1 ad1 tt 1 0.1 24 LOG S
! test mean and variance 
@@ tbtst tt :COVAR 0.010650 0.0090499
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
!
! DELAY/TSA      Compute delay graph of Chi2
WRITE/OUT " DELAY/TSA      Compute delay graph of Chi2"
DELAY/TSA ad1 ad2 tt 0 530 2 POW 0.02,-0.000121,0.68
! test mean and variance 
@@ tbtst tt :CHI2 580.98 448.85
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF
! 
!DELAY/TSA ad1 ad2 tt 0 530 2 UR2 
! test mean and variance 
!@@ tbtst tt :CHI2 580.98 448.85
IF INPUTR(1) .GT. TOLER THEN
   WRITE/OUT "*** ERROR: output incorrect"
   WRITE/KEY TSA_RESULT/C/1/10 "FAILED"
ENDIF 
!
! INTERPOL/TSA   Interpolate a series
!
! TABLE/TSA       Flexible Create/Table
!
WRITE/OUT "The TSA package test {TSA_RESULT}"
!
!   End TSA test
!
