! @(#)tbtst.prg	19.1 (ESO-IPG) 02/25/03 13:33:35
!===========================================================================
! Copyright (C) 1995 European Southern Observatory (ESO)
!
! This program is free software; you can redistribute it and/or 
! modify it under the terms of the GNU General Public License as 
! published by the Free Software Foundation; either version 2 of 
! the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public 
! License along with this program; if not, write to the Free 
! Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
! MA 02139, USA.
!
! Corresponding concerning ESO-MIDAS should be addressed as follows:
!	Internet e-mail: midas@eso.org
!	Postal address: European Southern Observatory
!			Data Management Division 
!			Karl-Schwarzschild-Strasse 2
!			D 85748 Garching bei Muenchen 
!			GERMANY
!===========================================================================
!
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tbtst.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   MIDAS, time analysis, testing
!.LANGUAGE  MIDAS
!.PURPOSE   Test column mean and variance
!.VERSION   0.0               October 1992
!.RETURNS   INPUTR(1)
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!
!  Test table column
!
!   Parameters
!   intab - table name
!   col - column name
!   mean - required mean value
!   var - required variance
!   Result:
!   Error - stored in INPUTR(1)
!
STAT/TAB {P1} {P2}
COMPUTE/KEY OUTPUTR(3) = (OUTPUTR(3)-{P3})*(OUTPUTR(3)-{P3})
COMPUTE/KEY OUTPUTR(4) = (OUTPUTR(4)-{P4})*(OUTPUTR(4)-{P4})
COMPUTE/KEY INPUTR(1) = M$SQRT(OUTPUTR(3)+OUTPUTR(4)+1E-25)/{P4}
!
!
!
