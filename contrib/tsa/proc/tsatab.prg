! @(#)tsatab.prg	19.1 (ES0-DMD) 02/25/03 13:33:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsatab.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   ????????????
!.LANGUAGE  MIDAS
!.PURPOSE   Convert ASCI table into MIDAS table
!           in a more flexible way as CREATE/TABLE
!.VERSION   0.0               June 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!   Get parameters
!
CROSSREF INTAB OUTTAB TYPE MXCOL
DEFINE/PARAM P1 {INROOT1} CHARACTER "Enter input table: "
WRITE/KEYW INROOT1/C/1/60  'P1'
DEFINE/PARAM P2 {INROOT1} TABLE     "Enter output table: "
DEFINE/PARAM P3 D       CHARACTER "Enter conversion type (R,D or I): "
DEFINE/PARAM P4 10      NUMBER    "Maximum number of columns: "
!
WRITE/KEYW OUTROOT/C/1/60  'P2'
WRITE/KEYW TYPE/C/1/1    'P3'
WRITE/KEYW MXCOL/I/1/1   'P4'
!
WRITE/KEYW IN_A/C/1/60  {INROOT1}
WRITE/KEYW OUT_A/C/1/60 {OUTROOT}
!
!   Call routine
!
WRITE/OUT "Command not yet released"
!RUN CON_EXE:TSATAB
WRITE/KEYW INROOT1/C/1/60  {OUT_A}
WRITE/KEYW HISTORY  "TAB/TSA"
!
!   End of tsasin
!
