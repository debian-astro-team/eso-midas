! @(#)otsasin.prg	19.1 (ES0-DMD) 02/25/03 13:33:12
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsasin.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   MIDAS, time analysis, SINEFIT/TSA
!.LANGUAGE  MIDAS
!.PURPOSE   Fit sine (Fourier) series, return residuals 
!.VERSION   0.0               June 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!   Get parameters
!
CROSSREF INTAB OUTTAB FREQUE ORDERTSA
DEFINE/PARAM P1 {INROOT1} TABLE  "Enter input table: "
DEFINE/PARAM P2 {OUTROOT} TABLE  "Enter output table: "
DEFINE/PARAM P3 {STARTTSA}   NUMBER "Enter approximate frequency: "
DEFINE/PARAM P4 {ORDERTSA}   NUMBER "Enter order (1 for pure sine): "
DEFINE/PARAM P5 1         NUMBER "Enter No. of iterations (<0 to continue): "
!
WRITE/KEYW  INROOT1/C/1/60 'P1'
WRITE/KEYW  OUTROOT/C/1/60 'P2'
WRITE/KEYW  FREQUE/D/1/1   'P3'
WRITE/KEYW  ORDERTSA/I/1/1    'P4'
WRITE/KEYW  ITER/I/1/1     'P5'
!
COPY/TABLE   {INROOT1}    ZZMID1WORK
WRITE/KEYW  IN_A/C/1/60  ZZMID1WORK
WRITE/KEYW  OUT_A/C/1/60 {OUTROOT}
WRITE/KEYW  INPUTC/C/1/1 Y
COPY/TABLE {IN_A}       {OUT_A}
WRITE/KEYW HISTORY "{MID$CMND(1:4)}/{MID$CMND(11:14)}"
!
!   Call routine
!
RUN CON_EXE:TSASIN
!
!   Note: STARTTSA is reset to the fitted frequency
!
DELETE/TABLE ZZMID1WORK   NO
!
!   End of tsasin
!
