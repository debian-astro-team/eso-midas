! @(#)tsabnd.prg	19.1 (ES0-DMD) 02/25/03 13:33:12
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsabnd.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   MIDAS, time series, BAND/TSA
!.LANGUAGE  MIDAS
!.PURPOSE   Evaluate frequency band for period analysis
!.VERSION   0.0               June 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!   Get parameters
!
CROSSREF INTAB MAXOBS
DEFINE/PARAM P1  {INROOT1} TABLE  "Enter input table: "
DEFINE/PARAM P2  100       NUMBER "Enter number of observations to be scanned: "
!
WRITE/KEYW  INROOT1/C/1/60 'P1'
WRITE/KEYW  MAXOBS/I/1/60  'P2'
!
SELECT/TABLE {INROOT1}       SELECTED.AND.:TIME.NE.NULL
IF OUTPUTI(1) .LE. 4 THEN
  WRITE/OUT *** Too few selected/correct data
  RETURN/EXIT
ENDIF
COPY/TABLE   {INROOT1}       ZZMID1WORK
!
!   Call routine
!
WRITE/KEYW  IN_A/C/1/60     ZZMID1WORK 
WRITE/KEYW  HISTORY    "BAND/TSA"
RUN CON_EXE:TSABND
DELETE/TABLE ZZMID1WORK NO
!
!   End of tsasin
!
