#!/bin/sh
# @(#)tsadelcrf.sh	5.1 (ESO-IPG) 4/5/93 15:59:31
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
#.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
#.IDENT     timdelcrf.sh
#.AUTHOR    Written by Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory,
#           using example of REBIN/II command by M.Rosa et al.
#           
#.KEYWORD   MIDAS, time series, DELAY/TSA
#.LANGUAGE  UNIX Shell
#.PURPOSE   Compute chi2-time lag function/Compile and link ACF routine
#.VERSION   0.0               Sept 1992
#.VERSION   1.1               Nov 1992 CG. Uses a makefile
#.RETURNS   None  
#.ENVIRON   TSA context
#------------------------------------------------------------------------------
#
cp $MIDASHOME/$MIDVERS/contrib/tsa/proc/makefile.tsa .
/bin/sh $MID_HOME/system/unix/makemidas -n -f makefile.tsa ARG=$1 MOD=tsadel
