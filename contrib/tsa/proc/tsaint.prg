! @(#)tsaint.prg	19.1 (ES0-DMD) 02/25/03 13:33:13
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsaint.prg
!.AUTHOR    Written by Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory,
!           using example of REBIN/II command by M.Rosa et al.
!           
!.KEYWORD   MIDAS, time series, INTERPOL/TSA
!.LANGUAGE  MIDAS
!.PURPOSE  Interpolate an uneven sampled series using its correlation function
!.VERSION   0.0               Sept 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!.CALLING SEQUENCE
!
!       INTERPOL/TSA iname oname func parm
!
!             funct   function code (see below) for the mapping of
!                     OUTPUT indep to INPUT indep
!
!             parm    parameters for the function used
!                  1) from command line: a,b,c,d,....(max.12)
!                  2) from keyword: "K(name)", e.g. KINPUTR
!
!             FUNCTIONS    (parameters A, B, C, ..... )
!
!             LIN     x(old) = A + Bx
!             POL     x      = A + Bx +Cxx +  .... (up to 12 coeff)
!             POW     x      = A + B*x**C
!	      EXP     x      = A + B*exp(Cx)
!	      LOG     x      = A + B*LOG(Cx)
!             IPO     x      = A + B/(x-x0) + C/(x-x0)**2 + ...
!	      USR     x      = user defined function.
!
!
!.DEFAULTS
!       INTERPOL/TSA #1 #2 LIN 0.,1. 
!
!------------------------------------------------------------------------------
!
!
!
!   Get parameters
!
CROSSREF INTAB OUTTAB FUNCT PARM
!
DEFINE/PARAM P1 {INROOT1}  TABLE  "Enter 1st input table: "
DEFINE/PARAM P2 {OUTROOT}  TABLE  "Enter output table: "
DEFINE/PARAM P3  LIN            
DEFINE/PARAM P4  0.,1.  
!
WRITE/KEYW INROOT1/C/1/60  'P1'
WRITE/KEYW OUTROOT/C/1/60  'P2'
WRITE/KEYW CFUNC/C/1/8     'P3'
WRITE/KEYW INPUTD/D/1/12 0. ALL
  IF P8(1:1) .NE. "K" THEN
     WRITE/KEYW INPUTD 'P4'
  ELSE
     COPY/KK 'P4(2:)' INPUTD/D/1/12
  ENDIF
!
SEL/TABLE {INROOT1} SELECT.AND.:TIME.NE.NULL.AND.:VALUE.NE.NULL.AND.:VAR.NE.NULL
IF OUTPUTI(1) .LE. 4 THEN
  WRITE/OUT *** Too few selected/correct data
  RETURN/EXIT
ENDIF
COPY/TABLE  {INROOT1}       ZZMID1WORK
!
!
!   Call routine
!
WRITE/OUT " Command not yet released"
! 
!IF CFUNC(1:3) .EQ. "USR" THEN
!
!   Compile and link, if necessary
!
!   IF AUX_MODE(1) .LE. 1 THEN
!       $ @ MID_DISK:['MID_HOME'.CONTRIB.TSA.PROC]tsaintcrf.com
!   ELSE
!       $ sh $MID_HOME/contrib/tsa/proc/tsaintcrf.sh
!   ENDIF
!   WRITE/KEYW IN_A            ZZMID1WORK
!   WRITE/KEYW OUT_A           {OUTROOT}
!   WRITE/KEYW  HISTORY   "INTERP/TSA"
!   RUN tsaint 
!ELSE
!   WRITE/KEYW IN_A            ZZMID1WORK
!   WRITE/KEYW OUT_A           {OUTROOT}
!   WRITE/KEYW  HISTORY   "INTERP/TSA"
!   RUN CON_EXE:tsaint
!ENDIF
DELETE/TABLE ZZMID1WORK NO
!
!   End of tsaint
!
