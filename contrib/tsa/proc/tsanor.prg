! @(#)tsanor.prg	19.1 (ES0-DMD) 02/25/03 13:33:13
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsanor.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   MIDAS, time analysis, NORMALIZE/TSA
!.LANGUAGE  MIDAS
!.PURPOSE   Normalize mean and, optionaly variance in a column to 0 and 1
!.VERSION   0.0               June 1992
!.RETURNS   None  
!.ENVIRON   CON_EXE:TSA context
!-----------------------------------------------------------------------------
!
!   Get parameters
!
CROSSREF INTAB OUTTAB COLUMN VAR
DEFINE/PARAM P1 {INROOT1} TABLE     "Enter input table: "
WRITE/KEYW INROOT1/C/1/60 'P1'
!
DEFINE/PARAM P2 {INROOT1} TABLE     "Enter output table: "
DEFINE/PARAM P3 ?       CHARACTER "Enter column name: "
DEFINE/PARAM P4 "M"     CHARACTER "Enter M-for mean only, V-otherwise: "
!
WRITE/KEYW OUTROOT/C/1/60  'P2'
WRITE/KEYW COLUMN/C/1/60 'P3'
WRITE/KEYW VAR/C/1/60    'P4'
!
WRITE/KEYW IN_A/C/1/60  {INROOT1}
WRITE/KEYW OUT_A/C/1/60 {OUTROOT}
IF IN_A .NE. OUT_A THEN
   COPY/TABLE {IN_A} {OUT_A}
ENDIF
!
!   Call routine
!
STATISTICS/TABLE {OUT_A} {COLUMN}
COMPUTE/TABLE    {OUT_A} {COLUMN}={COLUMN}-{OUTPUTR(3)}
!
IF VAR .EQ. "V" THEN
  COMPUTE/TABLE  {OUT_A} {COLUMN}={COLUMN}/{OUTPUTR(4)}
ENDIF 
WRITE/KEYW HISTORY "NORM/TSA"
!
!   End of tsasin
!
