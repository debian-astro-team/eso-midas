! @(#)tsacov.prg	19.1 (ES0-DMD) 02/25/03 13:33:12
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsacov.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   ????????????
!.LANGUAGE  MIDAS
!.PURPOSE   Compute discrete covariance function for uneven sampled data
!.VERSION   0.0               June 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!   Get parameters
!
CROSSREF INTAB1 INTAB2 OUTTAB STARTTSA STEPTSA NSTEPS
DEFINE/PARAM P1 {INROOT1}  TABLE  "Enter 1st input table: "
DEFINE/PARAM P2 {INROOT2} TABLE  "Enter 2nd input table: "
DEFINE/PARAM P3 {OUTROOT}  TABLE  "Enter output table: "
DEFINE/PARAM P4 {STARTTSA}  NUMBER "Enter first time lag: "
DEFINE/PARAM P5 {STEPTSA}   NUMBER "Enter time lag increment: "
DEFINE/PARAM P6 {NSTEPS} NUMBER "Enter number of lags: "
DEFINE/PARAM P7 LIN      CHARACTER "Enter type of lag scale: "
DEFINE/PARAM P8 LIN      CHARACTER "Enter type of function: "
!
WRITE/KEYW INROOT1/C/1/60  'P1'
WRITE/KEYW INROOT2/C/1/60  'P2'
WRITE/KEYW OUTROOT/C/1/60  'P3'
WRITE/KEYW STARTTSA/D/1/1  'P4'
WRITE/KEYW STEPTSA/D/1/1   'P5'
WRITE/KEYW NSTEPS/I/1/1    'P6'
WRITE/KEYW CSCALE/C/1/3    'P7'
WRITE/KEYW CFUNCT/C/1/1    'P8'
!
SEL/TABLE {INROOT1} SELECT.AND.:TIME.NE.NULL.AND.:VALUE.NE.NULL.AND.:VAR.NE.NULL
IF OUTPUTI(1) .LE. 4 THEN
  WRITE/OUT *** Too few selected/correct data 1
  RETURN/EXIT
ENDIF
COPY/TABLE  {INROOT1}    ZZMID1WORK
!
SEL/TABLE {INROOT2} SELECT.AND.:TIME.NE.NULL.AND.:VALUE.NE.NULL.AND.:VAR.NE.NULL
IF OUTPUTI(1) .LE. 4 THEN
  WRITE/OUT *** Too few selected/correct data 2
  RETURN/EXIT
ENDIF
COPY/TABLE  {INROOT2}    ZZMID2WORK
!
!
!   Call routine
!
WRITE/KEYW IN_A/C/1/60  ZZMID1WORK
WRITE/KEYW IN_B/C/1/60  ZZMID2WORK
WRITE/KEYW OUT_A/C/1/60 {OUTROOT}
WRITE/KEYW HISTORY "COVAR/TSA"
RUN CON_EXE:TSACOV
DELETE/TABLE  ZZMID1WORK NO
DELETE/TABLE  ZZMID2WORK NO
!
!   End of tsasin
!
