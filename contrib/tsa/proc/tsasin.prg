! @(#)tsasin.prg	19.1 (ES0-DMD) 02/25/03 13:33:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsasin.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   MIDAS, time analysis, SINEFIT/TSA
!.LANGUAGE  MIDAS
!.PURPOSE   Fit sine (Fourier) series, return residuals 
!.VERSION   0.0               June 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!   Get parameters
!
CROSSREF INTAB OUTTAB FREQUE ORDERTSA ITER
DEFINE/PARAM P1 {INROOT1} TABLE  "Enter input table: "
DEFINE/PARAM P2 {OUTROOT} TABLE  "Enter output table: "
DEFINE/PARAM P3 {STARTTSA}   NUMBER "Enter approximate frequency: "
DEFINE/PARAM P4 {ORDERTSA}   NUMBER "Enter order (1 for pure sine): "
DEFINE/PARAM P5 1         NUMBER "Enter No. of iterations (<0 to continue): "
!
WRITE/KEYW  ITER/I/1/1     'P5'
!
!   Ignore 'P1' to 'P4' by continuation
!
IF ITER .GT. 0 THEN
  WRITE/KEYW  INROOT1/C/1/60   'P1'
  WRITE/KEYW  OUTROOT/C/1/60   'P2'
  WRITE/KEYW  STARTTSA/D/1/1  'P3'
  WRITE/KEYW  ORDERTSA/I/1/1  'P4'
!
  IF ORDERTSA .LT. 0 THEN
    WRITE/KEYW ORDERTSA/I/1/1 0
    WRITE/OUT *** Wrong ORDERTSA, set to 0
  ELSEIF ORDERTSA .GT. 9 THEN
    WRITE/KEYW ORDERTSA/I/1/1 9
    WRITE/OUT *** Wrong ORDERTSA, set to 9
  ENDIF
!
  SELECT/TABLE {INROOT1}    SELECT.AND.:TIME.NE.NULL.AND.:VALUE.NE.NULL
  OUTPUTI(2) = ORDERTSA*2+1
  IF OUTPUTI(1) .LE. OUTPUTI(2) THEN
    WRITE/OUT *** Too few selected/correct data or too large ORDERTSA
    RETURN/EXIT
  ENDIF
  COPY/TABLE   {INROOT1}        {OUTROOT}
  COMPUTE/TABLE {OUTROOT} :ORIG_VALUE = :VALUE
ELSE
  COMPUTE/TABLE {OUTROOT} :VALUE = :ORIG_VALUE
ENDIF
!
!   Call routine
!
WRITE/KEYW  IN_A/C/1/60  {OUTROOT}
WRITE/KEYW  OUT_A/C/1/60 {OUTROOT}
WRITE/KEYW  HISTORY      "SINEFIT/TSA"
RUN CON_EXE:TSASIN
COMPUTE/TABLE {OUT_A} :FIT = :ORIG_VALUE-:VALUE
!
!   Fit results are stored in STARTTSA and OUTPUTD
!
WRITE/OUT *** Call no other TSA commands and preserve OUTPUTD to continue 
WRITE/OUT *** by repeating SINEFIT/TSA with negative number of iterations
!
!
!   End of tsasin
!
