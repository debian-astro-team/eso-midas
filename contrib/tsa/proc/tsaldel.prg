! @(#)tsaldel.prg	19.1 (ES0-DMD) 02/25/03 13:33:13
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsadel.prg
!.AUTHOR    Written by Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory,
!           using example of REBIN/II command by M.Rosa et al.
!           
!.KEYWORD   MIDAS, time series, DELAY/TSA
!.LANGUAGE  MIDAS
!.PURPOSE   Compute chi2-time lag function
!.VERSION   0.0               Sept 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!.CALLING SEQUENCE
!
!       DELAY/TSA iname inameb oname start step nsteps func,mode parm
!
!             funct   function code (see below) for the mapping of
!                     OUTPUT indep to INPUT indep
!
!             parm    parameters for the function used
!                  1) from command line: a,b,c,d,....(max.12)
!                  2) from keyword: "K(name)", e.g. KINPUTR
!
!             intop   interpolation option (see below)
!
!             FUNCTIONS    (parameters A, B, C, ..... )
!
!             LIN     x(old) = A + Bx
!             POL     x      = A + Bx +Cxx +  .... (up to 12 coeff)
!             POW     x      = A + B*x**C
!	      EXP     x      = A + B*exp(Cx)
!	      LOG     x      = A + B*LOG(Cx)
!             IPO     x      = A + B/(x-x0) + C/(x-x0)**2 + ...
!	      USR     x      = user defined function.
!
!
!.DEFAULTS
!       DELAY/TSA #1 #2 #3 STARTTSA STEPTSA NSTEPS LIN 0.,1. 
!
!------------------------------------------------------------------------------
!
!WRITE/KEYW REBIN/C/1/12 "'MID$CMND(1:12)'"
!
!
!   Get parameters
!
CROSSREF INTAB1 INTAB2 OUTTAB STARTTSA STEPTSA NSTEPS FUNCT PARM
!
DEFINE/PARAM P1 {INROOT1}  TABLE  "Enter 1st input table: "
DEFINE/PARAM P2 {INROOT2} TABLE  "Enter 2nd input table: "
DEFINE/PARAM P3 {OUTROOT}  TABLE  "Enter output table: "
DEFINE/PARAM P4 {STARTTSA}  NUMBER "Enter first time lag: "
DEFINE/PARAM P5 {STEPTSA}   NUMBER "Enter time lag increment: "
DEFINE/PARAM P6 {NSTEPS} NUMBER "Enter number of lags: "
DEFINE/PARAM P7  LIN,NOR            
DEFINE/PARAM P8  0.,1.  
DEFINE/LOCAL FUNCLOW/C/1/3 " " ALL
!
WRITE/KEYW INROOT1/C/1/60  'P1'
WRITE/KEYW INROOT2/C/1/60  'P2'
WRITE/KEYW OUTROOT/C/1/60  'P3'
WRITE/KEYW STARTTSA/D/1/1     'P4'
WRITE/KEYW STEPTSA/D/1/1      'P5'
WRITE/KEYW NSTEPS/I/1/1    'P6'
WRITE/KEYW CFUNC/C/1/3     'P7(1:3)'
IF M$INDEX(P7,",") .EQ. 0 THEN
     WRITE/KEYW CMODE/C/1/3 "NOR"
ELSE
     WRITE/KEYW CMODE/C/1/3  'P7(5:7)'
ENDIF
WRITE/KEYW INPUTD/D/1/12 0. ALL
  IF P8(1:1) .NE. "K" THEN
     WRITE/KEYW INPUTD   'P8'
  ELSE
     COPY/KK 'P8(2:)' INPUTD/D/1/12
  ENDIF
!  Note: ORDERTSA is used to set mode of operation
!
SEL/TABLE {INROOT1} SELECT.AND.:TIME.NE.NULL.AND.:VALUE.NE.NULL.AND.:VAR.NE.NULL
IF OUTPUTI(1) .LE. 4 THEN
  WRITE/OUT *** Too few selected/correct data 1
  RETURN/EXIT
ENDIF
COPY/TABLE  {INROOT1} ZZMID1WORK
WRITE/KEYW IN_A      ZZMID1WORK
SEL/TABLE {INROOT2} SELECT.AND.:TIME.NE.NULL.AND.:VALUE.NE.NULL.AND.:VAR.NE.NULL
IF OUTPUTI(1) .LE. 4 THEN
  WRITE/OUT *** Too few selected/correct data 2
  RETURN/EXIT
ENDIF
COPY/TABLE  {INROOT2} ZZMID2WORK
WRITE/KEYW IN_B      ZZMID2WORK
WRITE/KEYW OUT_A {OUTROOT}
!
!   Call routine
!
! 
IF CFUNC(1:2) .EQ. "UR" THEN
!
!   Compile and link, if necessary
!
FUNCLOW = M$LOWER(CFUNC)
   IF AUX_MODE(1) .LE. 1 THEN
       $ @ MID_DISK:['MID_HOME'.CONTRIB.TSA.PROC]tsadelcrf.com 'FUNCLOW'
   ELSE
       $ sh $MID_HOME/contrib/tsa/proc/tsadelcrf.sh 'FUNCLOW' 
   ENDIF
   RUN tsadel 
ELSE
   RUN CON_EXE:tsadel
ENDIF
DELETE/TABLE  ZZMID1WORK NO
DELETE/TABLE  ZZMID2WORK NO
WRITE/KEYW   HISTORY    "DELAYL/TSA"
!
!   End of tsadel
!
