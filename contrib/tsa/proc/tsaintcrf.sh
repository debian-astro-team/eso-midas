#!/bin/sh
# @(#)tsaintcrf.sh	5.1 (ESO-IPG) 4/5/93 15:59:21
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
#.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
#.IDENT     tsadintcrf.sh
#.AUTHOR    Written by Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory,
#           using example of REBIN/II command by M.Rosa et al.
#           
#.KEYWORD   MIDAS, time series, INTERP/TSA
#.LANGUAGE  UNIX Shell
#.PURPOSE   Compute chi2-time lag function/Compile and link ACF routine
#.VERSION   0.0               Sept 1992
#.RETURNS   None  
#.ENVIRON   TSA context
#------------------------------------------------------------------------------
#
cp $MIDASHOME/$MIDVERS/contrib/tsa/src/makefile.tsa .
sh $MIDASHOME/$MIDVERS/system/unix/makemidas -f makefile.tsa MOD=tsaint OBJ_ARG=tsaintusr.o 
