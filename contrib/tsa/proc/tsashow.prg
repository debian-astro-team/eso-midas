! @(#)tsashow.prg	19.1 (ES0-DMD) 02/25/03 13:33:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsashow.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   ????????????
!.LANGUAGE  MIDAS
!.PURPOSE   Show context global keywords 
!.VERSION   0.0               June 1992
!.RETURNS   None  (Stack created)
!.ENVIRON   CON_EXE:TSA context
!-----------------------------------------------------------------------------
!
SET/FORMAT I1
!
WRITE/OUT "  "
WRITE/OUT "Time Series Analysis: Values of context keywords"
WRITE/OUT "  "
IF INROOT1(1:1) .EQ. "?" THEN
   WRITE/OUT "Input file:     INROOT1  = "
ELSE
   WRITE/OUT "Input file:     INROOT1  = {INROOT1}"
ENDIF
IF INROOT2(1:1) .EQ. "?" THEN
   WRITE/OUT "2nd input file: INROOT2  = "
ELSE
   WRITE/OUT "2nd input file: INROOT2  = {INROOT2}"
ENDIF
IF OUTROOT(1:1) .EQ. "?" THEN
   WRITE/OUT "Output file:    OUTROOT  ="
ELSE
   WRITE/OUT "Output file:    OUTROOT  = {OUTROOT}"
ENDIF
WRITE/OUT "Grid start:     STARTTSA= {STARTTSA}"
WRITE/OUT "Grid step:      STEPTSA = {STEPTSA}"
WRITE/OUT "Grid size:      NSTEPS   = {NSTEPS}"
WRITE/OUT "Order:          ORDERTSA= {ORDERTSA}"
!
!   End of tsashow file
!
