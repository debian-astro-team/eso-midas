!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992-2012     European Southern Observatory and Warsaw Observatory
!.IDENT     tsawdt.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   MIDAS, time series, WIDTH/TSA
!.LANGUAGE  MIDAS
!.PURPOSE   Evaluate width of a spectral line
!.VERSION   0.0               June 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!   Get parameters
!
CROSSREF INTAB WIDTH CENTRE
DEFINE/PARAM P1  {INROOT1}  IMAGE  "Enter input table: "
COMPUTE/KEYW INPUTD = {STEPTSA}*{NSTEPS}
DEFINE/PARAM P2  {INPUTD}   NUMBER "Enter band width:"
COMPUTE/KEYW INPUTD = {INPUTD}+{STARTTSA}
DEFINE/PARAM P3  {INPUTD}   NUMBER "Enter band centre:"
!
define/local INROOT1/C/1/60  {P1}  ? +lower     !KB 120126
define/local WIDTH/D/1/1   {P2} ? +lower
define/local CENTRE/D/1/1  {P3} ? +lower
!
!   Call routine
!
WRITE/KEYW IN_A/C/1/60   {INROOT1}
WRITE/KEYW HISTORY "WIDTH/TSA"
RUN CON_EXE:TSAWDT
!
!   End of tsawdt
!
