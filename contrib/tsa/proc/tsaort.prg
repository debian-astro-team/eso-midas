! @(#)tsaort.prg	19.1 (ESO-IPG) 02/25/03 13:33:13
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsaort.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   TSA, MIDAS, Time Series Analysis
!.LANGUAGE  MIDAS
!.PURPOSE   Compute multiharmonic Fourier spectrum by orthogonal projections
!.VERSION   0.0               June 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!   Get parameters
!
CROSSREF INTAB OUTIMA STARTTSA STEPTSA NSTEPS ORDERTSA  
! 
DEFINE/PARAM P1 {INROOT1}  TABLE  "Enter input table: "
DEFINE/PARAM P2 {OUTROOT}  IMAGE  "Enter output image: "
DEFINE/PARAM P3 {STARTTSA}    NUMBER "Enter frequency start: "
DEFINE/PARAM P4 {STEPTSA}     NUMBER "Enter frequency step: "
DEFINE/PARAM P5 {NSTEPS}   NUMBER "Enter number of frequencies: "
DEFINE/PARAM P6 {ORDERTSA}    NUMBER "Enter order (2*no of harmonics): "
!
WRITE/KEYW INROOT1/C/1/60 {P1}
WRITE/KEYW OUTROOT/C/1/60 {P2}
WRITE/KEYW STARTTSA/D/1/1    {P3}
WRITE/KEYW STEPTSA/D/1/1     {P4}
WRITE/KEYW NSTEPS/I/1/1   {P5}
WRITE/KEYW ORDERTSA/I/1/1    {P6}
IF ORDERTSA .LT. 1 THEN
  WRITE/KEYW ORDERTSA/I/1/1 2
  WRITE/OUT *** Wrong ORDERTSA, set to 2
ELSEIF ORDERTSA .GT. 100 THEN
  WRITE/KEYW ORDERTSA/I/1/1 100
  WRITE/OUT *** Wrong ORDERTSA, set to 100
ENDIF
!
SELECT/TABLE {INROOT1}     SELECT.AND.:TIME.NE.NULL.AND.:VALUE.NE.NULL
OUTPUTI(2) = ORDERTSA
IF OUTPUTI(1) .LE. OUTPUTI(2) THEN
  WRITE/OUT *** Too few selected/correct data or too many bins
  RETURN/EXIT
ENDIF
COPY/TABLE  {INROOT1}      ZZMID1WORK
!
!   Call routine
!
WRITE/KEYW IN_A/C/1/60    ZZMID1WORK
WRITE/KEYW OUT_A/C/1/60   {OUTROOT}
WRITE/KEYW   HISTORY    "ORT/TSA"
RUN CON_EXE:TSAORT
DELETE/TABLE  ZZMID1WORK NO
