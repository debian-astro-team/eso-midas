! @(#)tsasca.prg	19.1 (ES0-DMD) 02/25/03 13:33:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsasca.prg
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   ????????????
!.LANGUAGE  MIDAS
!.PURPOSE   Compute discrete and Scargle spectrum 
!.VERSION   0.0               June 1992
!.RETURNS   None  
!.ENVIRON   TSA context
!-----------------------------------------------------------------------------
!
!   Get parameters
!
CROSSREF INTAB OUTIMA STARTTSA STEPTSA NSTEPS 
DEFINE/PARAM P1 {INROOT1}    TABLE "Enter input table: "
DEFINE/PARAM P2 {OUTROOT}    IMAGE "Enter output image: "
DEFINE/PARAM P3 {STARTTSA}      NUMBER "Enter frequency start: "
DEFINE/PARAM P4 {STEPTSA}       NUMBER "Enter frequency step: "
DEFINE/PARAM P5 {NSTEPS}     NUMBER "Enter number of frequencies: "
!
WRITE/KEYW  INROOT1/C/1/60 'P1'
WRITE/KEYW  OUTROOT/C/1/60 'P2'
WRITE/KEYW  STARTTSA/D/1/1    'P3'
WRITE/KEYW  STEPTSA/D/1/1     'P4'
WRITE/KEYW  NSTEPS/I/1/1   'P5'
!
SELECT/TABLE {INROOT1}      SELECT.AND.:TIME.NE.NULL.AND.:VALUE.NE.NULL
IF OUTPUTI(1) .LE. 2 THEN
  WRITE/OUT *** Too few selected/correct data
  RETURN/EXIT
ENDIF
COPY/TABLE   {INROOT1}      ZZMID1WORK
!
!
!   Call routine
!
WRITE/KEYW  IN_A/C/1/60    ZZMID1WORK
WRITE/KEYW  OUT_A/C/1/60   {OUTROOT}
WRITE/KEYW  HISTORY    "SCARGLE/TSA"
RUN CON_EXE:TSASCA
DELETE/TABLE ZZMID1WORK NO
!
!   End of tsasca
!
