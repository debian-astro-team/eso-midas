! @(#)tsainit.prg	19.1 (ES0-DMD) 02/25/03 13:33:13
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
!.COPYRIGHT (c)  1992     European Southern Observatory and Warsaw Observatory
!.IDENT     tsaini.ctx
!.AUTHOR    Alex Schwarzenberg-Czerny, ESO and Warsaw Observatory 
!.KEYWORD   ????????????
!.LANGUAGE  MIDAS
!.PURPOSE   Define context TIME/ANAL 
!.VERSION   0.0               June 1992
!.RETURNS   None  (Stack created)
!.ENVIRON   CON_EXE:TSA context
!-----------------------------------------------------------------------------
!
!
!   Initialize global keywords
!
register/session tsa CON_PROC tsainit.prg  tsatab.tbl ! Initialisation
!Begin Session List
WRITE/KEYW INROOT1/C/1/60  "?" ! Last input file name
WRITE/KEYW INROOT2/C/1/60 "?" ! Last 2nd input file name
WRITE/KEYW OUTROOT/C/1/60  "?" ! Last output file name
WRITE/KEYW STARTTSA/D/1/1   0.  ! Output grid start (Frequency or Time lag)
WRITE/KEYW STEPTSA/D/1/1    0.01! Output grid step
WRITE/KEYW NSTEPS/I/1/1  101 ! Number of grid steps
WRITE/KEYW ORDERTSA/I/1/1   5   ! Order, required by some methods
!End Session List
!   Other global keys used: IN_A, OUT__A
!
!   End of initialization file
!
