$ ! @(#)preinstall.com	19.1 (ESO-IPG) 02/25/03 13:26:59 
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1987 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       PREINSTALL.COM
$ ! .COMMENTS    Preinstallation procedure ['MIDASHOME'.'MIDVERS'.CONTRIB.LIB]
$ !              Create Midas libraries.
$ ! .REMARKS     Genereted by hand.
$ ! .DATE        Thu Dec  1 12:24:17 MET 1988
$ ! .DATE        900316  KB
$ ! .DATE        910101  CG
$ ! .DATE        921001  MP
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ SET VERIFY
$ !
$ LIB/CREATE LIBDAOPHOT
$ LIB/CREATE LIBESOLV
$ LIB/CREATE LIBIMRES
$ LIB/CREATE LIBINVENT
$ LIB/CREATE LIBIST
$ LIB/CREATE LIBMVA
$ LIB/CREATE LIBPEP
$ LIB/CREATE LIBRFOT
$ LIB/CREATE LIBSURFPHOT
$ LIB/CREATE LIBTSA
$ LIB/CREATE LIBTSAUSR
$ LIB/CREATE LIBWAVE2D
$ LIB/CREATE LIBPOS1
$ LIB/CREATE LIBECH
$ !
$ SET NOVER
$ PURGE
