!
! Template procdure to call the C program
! my_c_template.c
!
! Call with:
! @@ my_c {image}
! 
! Main purpose of this program:
! show how programming in MIDAS environment works
!
! P. Nass 11/19/1999
!
DEFINE/PARAM P1 myimage.bdf IMAGE
!
DEFINE/LOCAL IMAGE/C/1/20 
DEFINE/LOCAL TYPE/C/1/20 
!
CREATE/IMAGE {P1} 2,4,4
WRITE/DESCRIPTOR FORMTYPE/C/1/20 a all
!
IMAGE = {P1}
!
!
run ../my_c_template.exe 
!
write/out Back in the MIDAS procedure my_c.prg!
!
write/out End of my_c.prg!
