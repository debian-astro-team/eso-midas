!
! This is a template procedure
!
! Call this MIDAS procedure with:  
!  @@ my_for {in} {out}
!
! the procedure will then create a dummy table {in},
! then it'll execute the FORTRAN program  my_for_template.exe
! which will read the names from keywords, then rename the frame 
! and return to MIDAS
!
! P.Nass 11/19/1999
!
DEFINE/PARAM P1 input.tbl TABLE 
DEFINE/PARAM P2 output.tbl TABLE  
!
DEFINE/LOCAL INPUT/C/1/10
DEFINE/LOCAL OUTPUT/C/1/10
!
CREATE/TABLE {P1} 2 2 
WRITE/OUT Dummy table {P1} has been created
! put content of P1 and P2 in local keywords: 
INPUT = P1
OUTPUT = P2 
! now execute your FORTRAN program in MIDAS environment:
run ../my_for_template.exe 
!
WRITE/OUT Back in the MIDAS procedure my_for.prg again!
!
WRITE/OUT This is the end of the FORTRAN example!
