! 
! example procedure for using general ASCII files
! in a procedure
! 
! K. Banse	ESO - Garching
! creation	061008
! 
! 061010	last modif
! 
$ls *.bdf > mydir.dat
! 
write/key fcnt/i/1/2 0,0
open/file mydir.dat read fcnt
if fcnt(1) .lt. 0 then
   write/out error opening file mydir.dat
   return/exit
endif
! 
read_file:
read/file {fcnt(1)} outputc
if fcnt(2) .lt. 0 then
   close/file {fcnt(1)}
   return
endif
! 
! do whatever you want with the file contained in keyword outputc
! 
$echo "we have here file:" {outputc}
! 
goto read_file

