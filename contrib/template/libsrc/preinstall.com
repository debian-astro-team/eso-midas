$ ! @(#)preinstall.com	19.1 (ESO-IPG) 02/25/03 13:31:54 
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1987 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Jean-Pierre De Cuyper - [KSB/ORB]
$ ! .IDENT       PREINSTALL.COM
$ ! .COMMENTS    Preinstallation procedure in ['MIDASHOME'.'MIDVERS'.CONTRIB.TEMPLATE.LIBSRC]
$ !              Create Midas library.
$ ! .REMARKS     Genereted by hand.
$ ! .DATE        Thu 05 May 2000
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ !
$ LIB/CREATE/NOLOG LIBTEMPLATE	
$ !
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
$ PURGE/NOLOG/NOCONFIRM LIBTEMPLATE.OLB
