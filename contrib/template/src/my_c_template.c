/* --------------------------------------

Very simple template file for 
C program which runs in MIDAS environment

Use procedure my_c.prg in ../proc to call 
this program


P.Nass 11/19/1999

090804		last modif
---------------------------------------- */

#include <midas_def.h>
#include <string.h>

int main()
{
  int iInfo;
  int imfid;
  int stat; 
  int kunit;
  int knull;
  int actvals;
 
  char filenam[10];
  char chInfo[20];
 

  /* get into the MIDAS environment: */
  stat = SCSPRO("MY_C");
  
  /* read keyword:*/
  (void) SCKRDC("IMAGE",1,1,14,&actvals,filenam,&kunit,&knull);

  /* open image: */
  iInfo = F_IMA_TYPE;
  (void) SCFOPN(filenam,D_OLD_FORMAT,0,iInfo, &imfid);

  /* write F_IMA_TYPE into keyword TYPE*/
  (void) strcpy(chInfo,"F_IMA_TYPE");
  (void) SCKWRC("TYPE",1,chInfo,1,20,&kunit);
  
  /* and write it also into descriptor FORMTYPE: */
  (void) SCDWRC(imfid, "FORMTYPE",1,"F_IMA_TYPE", 1, 20, &kunit);
  
  /* close the image */
  (void) SCFCLO(imfid);


  SCTPUT("Reached the end of the C program!");
  return SCSEPI();
} 
