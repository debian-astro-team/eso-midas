CC  
CC   This is a template for a short
CC   FORTRAN program which runs in 
CC   the MIDAS enviroment
CC
CC   Use the procedure my_for.prg to
CC   call this program
CC
CC    
CC
CC   P.Nass 11/19/1999
CC 
CC   090806		last modif
CC 
CC
      PROGRAM FTEMPL
      IMPLICIT NONE

      CHARACTER OLDNAME*60, NEWNAME*60
      CHARACTER CBUF*80

      INTEGER STAT
      INTEGER ACTS
      INTEGER KUN
      INTEGER KNUL

      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      COMMON /VMR/  MADRID
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'

      CALL STSPRO('FTEMPL')          ! start communication with MIDAS     
      
CC    read character keyword:
      CALL STKRDC('INPUT',1,1,60,ACTS,OLDNAME,KUN,KNUL,STAT)

CC    again read character keyword:
      CALL STKRDC('OUTPUT',1,1,60,ACTS,NEWNAME,KUN,KNUL,STAT)

CC    rename a frame on disk:
      CALL STFRNM(OLDNAME, NEWNAME,STAT)

CC    produce some output:
      WRITE (CBUF,10000) OLDNAME
      CALL STTPUT(CBUF)
      WRITE (CBUF,10000)
      CALL  STTPUT (CBUF)
C
      CALL STSEPI                     ! stop communication with MIDAS
C
10000 FORMAT('Rename frame: ',A)
10001 FORMAT('to: ',A)
      END

