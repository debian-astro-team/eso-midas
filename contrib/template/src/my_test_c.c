/* --------------------------------------

Very simple template file for 
C program which runs in MIDAS environment

Use procedure my_c.prg in ../proc to call 
this program


P.Nass 11/19/1999
---------------------------------------- */

#include <midas_def.h>
#include <string.h>

main()
{
  char filenam[10];
  int iInfo;
  char chInfo[20];
  int imfid;
  int nval;
  char instr[10];
  int un;
  int nulval;
  int ret;
  int stat; 
  char name[20];
  int npix;
  int imno;

  int kunit;
  int knull;
  int actvals;
  int values[1];
  
  /* get into the MIDAS environment: */
  stat = SCSPRO("ALICE_TEST");


  stat = SCFCRE("test", D_R4_FORMAT,F_O_MODE,F_IMA_TYPE, 1, &imno); 
 
  /* and write 1 also into descriptor NPIX : */
  values[0] = 1;
  SCDWRD(imno, "NPIX",values,1,1, &kunit);

  SCTPUT("Reached the end of the C program!");
  return SCSEPI();
} 
