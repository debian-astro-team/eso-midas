!+ 
! Example 18, MIDAS procedure exa18.prg 
!+ 
DEFINE/PARAM P1 11 NUMBER "Enter input number: " 
DEFINE/MAXPAR 1                                  ! only one parameter expected
WRITE/OUT "Parameter 1 = {P1}"
!
ENTRY 2 
DEFINE/PARAM P1 new C "Enter input: " 
DEFINE/MAXPAR 1                                  ! only one parameter expected
WRITE/OUT "Parameter 1 = {P1}"
! 
ENTRY third 
DEFINE/PARAM P1 spiral IMA "Enter input image: "
DEFINE/MAXPAR 1                                  ! only one parameter expected
WRITE/OUT "Parameter 1 = {P1}"
