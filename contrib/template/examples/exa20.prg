!+
! Example 20, MIDAS procedure exa20.prg
!+
DEFINE/LOCAL CATAL/I/1/1 0	 ! define local key CATAL
! 
LOOP:
STORE/FRAME IN_A {P1}            ! fill key IN_A with parameter 1
RUN pearl		         ! run our application
GOTO LOOP 
