!+ 
! Example 2, MIDAS procedure exa2.prg 
!+ 
DEFINE/PARAM P1 999 NUMBER "Enter first input number:" 22,1024
DEFINE/MAXPAR 1 ! only 1 parameter expected
WRITE/KEYWORD INPUTI/I/7/1 {P1}! store contents of P1 in INPUTI(7)
