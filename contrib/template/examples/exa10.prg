!+
! Example 10, MIDAS procedure exa10.prg 
!+
DEFINE/PARAM P1 ? IMA "Enter root_name for input frames: " 
DEFINE/PARAM P2 ? IMA "Enter root_name for output frames: " 
DEFINE/MAXPAR 2 ! max. 2 parameters expected
SET/FORMAT I4 		! we need 4 digits
WRITE/KEYWORD N/I/1/1 0
WRITE/KEYWORD NN/I/1/2 0,0 ! 
DO N = 1 50			! default increment is 1
    NN(1) = 2*N 
    NN(2) = NN(1)-1 
    COMPUTE/IMAGE {P2}{N} = {P1}{NN(2)}+{P1}{NN(1)}
				 ! sum up
    LOAD/IMAGE {P2}{N} 
				 ! display the result frame
ENDDO
