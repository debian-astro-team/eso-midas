!+
! Example 7a, MIDAS procedure exa7a.prg
!+
inputd = 1234.55667701
write/keyword outputd/d/1/1 {inputd}
set/format ,f20.10
write/out {outputd(1)}
