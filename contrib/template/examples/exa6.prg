!+
! Example 6, MIDAS procedure exa6.prg 
!+
DEFINE/PARAM P1 ? N "Enter alpha value: " -88.5,912.4 
DEFINE/PARAM P2 ? N "Enter loop_count: " 1,999 
DEFINE/MAXPAR 2                                 ! max. 2 parameters expected
WRITE/KEYWORD VAR/R/1/1 0.                      ! init key VAR
VAR = {P1} * 3.3	                        ! see remark a
  WRITE/DESCR myframe rval/r/1/2 0.0,0.0        !  
  LOOP:                      		        ! declare label LOOP
  VAR = 1.+VAR	        	                ! see remark b
  myframe,rval(2) = var+12.99
  WRITE/OUT {$myframe,rval(2)}
  myframe[@10,@20] = 20.0-{myframe,rval(2)}
  WRITE/OUT {myframe[@10,@20]}
  mytable,:DEC,@7 = {myframe[@10,@20]}*2.0
  WRITE/OUT {mytable,:DEC,@7}
  WRITE/OUT " "
  IF VAR .LE. {P2} GOTO LOOP ! go to label LOOP, if VAR <= contents of P2
