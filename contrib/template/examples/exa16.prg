!+ 
! Example 16, MIDAS procedure exa16.prg 
!+ 
DEFINE/PARAM P1 ? ? "Enter FITS file name:" 
!  
DEFINE/LOCAL INA/C/1/80 " " all ! that fills all elements of INA with blanks
DEFINE/LOCAL K/I/1/2 0,0
!  
K = M$INDEX(P1,".mt")           ! test, if type of FITS file entered
IF K .LT. 2 THEN 
  WRITE/KEYW INA {P1}.mt        ! if not, append type
ELSE 
  WRITE/KEYW INA {P1}           ! if yes, no need to append type
ENDIF
INTAPE/FITS 1 midd {ina} fnn | $more
                  !   but if we want to save the no. of axes of the FITS file
                  !   we do not use the keyword name INA but it's contents
K = M$VALUE({ina},naxis) 
WRITE/OUT we have {K} axes
