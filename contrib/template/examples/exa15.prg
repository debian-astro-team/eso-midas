!+ 
! Example 15, MIDAS procedure exa15.prg 
!+ 
DEFINE/PARAMETER P1 ? C "Enter method: "  
DEFINE/MAXPAR 1 ! only one parameter expected
! 
! Use the first 2 characters of parameter P1 to distinguish the methods 
BRANCH P1(1:2) AN,DI,HY ANALOG,DIGIT,HYBRID 
! 
! fall through if no match ... 
WRITE/OUT Invalid option - please try again 
RETURN
! 
ANALOG:  
RUN ANALO 
RETURN  
! 
DIGIT:  
RUN DIGI 
RETURN  
!
HYBRID:  
RUN HYBRI 
