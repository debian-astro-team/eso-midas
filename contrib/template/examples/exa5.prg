!+
! Example 5, MIDAS procedure exa5.prg
!+
CROSSREF IN_FILE OUT_FILE
DEFINE/PARAM P1 ? IMA "Enter name of input file: " 
DEFINE/PARAM P2 ? IMA "Enter name of result file: "
DEFINE/MAXPAR 2 ! max. 2 parameters expected}
WRITE/KEYWORD IN_B " " all                     ! fill keyword IN_B with blanks
INQUIRE/KEYWORD IN_B "Which filter, enter LOW or HIGH: "
IF AUX_MODE(7) .EQ. 0 IN_B = "LOW"           ! LOW is the default
