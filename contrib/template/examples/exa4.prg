!+ 
! Example 4, MIDAS procedure exa4.prg
!+
CROSSREF IN_FILE OUT_FILE METHOD ALPHA
DEFINE/PARAM P1 ? IMA "Enter name of input file: "
DEFINE/PARAM P2 ? IMA "Enter name of result file: "
DEFINE/PARAM P3 ? C   "Enter method: "
DEFINE/PARAM P4 999 NUM "Enter alpha value: " 22,1024
DEFINE/MAXPAR 4                          ! max. 4 parameters expected
WRITE/KEYWORD INPUTI/I/7/1 {P4}
