!+ 
! Example 13a, MIDAS procedure exa13a.prg 
!+ 
DEFINE/PARAM P1 ? N "Enter number: "
DEFINE/MAXPAR 1                        ! only one parameter expected
IF {P1} .EQ. 1  THEN 
     WRITE/OUT P1 = 1
ELSE  
     WRITE/OUT P1 is not = 1
ENDIF
