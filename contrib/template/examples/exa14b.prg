!+ 
! Example 14b, MIDAS procedure exa14b.prg 
!+ 
DEFINE/PARAM P6 + NUMBER "Enter first input number: " 
IF P6(1:1) .EQ. "+" THEN 
   WRITE/KEYWORD INPUTC NONE ! no P6 entered, set INPUTC accordingly
ELSE 
   DEFINE/PARAM P6 + NUMBER "Enter first input number: " 22,1024
   WRITE/KEYWORD INPUTI/I/7/1 {P6} ! store contents of P6 in INPUTI(7)
   WRITE/KEYWORD INPUTC YES !indicate, that INPUTI holds a valid number
ENDIF
