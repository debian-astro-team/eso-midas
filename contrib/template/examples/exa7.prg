!+ 
! Example 7, MIDAS procedure exa7.prg
!+ 
WRITE/KEYWORD INPUTI 12		! set INPUTI(1) to 12
WRITE/KEYWORD INPUTR 12.345	! set INPUTR(1) to 12.345
                                ! and set INPUTD(1) to 123456.98765432
WRITE/KEYWORD INPUTD 123456.98765432
WRITE/OUT {inputi(1)} {inputr(1)} {inputd(1)}
SET/FORMAT I2		        ! use format I2.2 for integer symbols}
                 ! and use format E12.8 and G22.8 for real and double symbols}
SET/FORMAT E12.8,G22.8 
WRITE/OUT {inputi(1)} {inputr(1)} {inputd(1)} 
SET/FORMAT i5			! use format I5.5 for integer symbols
                  ! and use format F12.4 and E22.13 for real and double symbols
SET/FORMAT f12.4,e22.13
WRITE/OUT {inputi(1)} {inputr(1)} {inputd(1)}  
