!+ 
! Example 3, MIDAS procedure exa3.prg 
!+ 
DEFINE/PARAM P1 999 N/CONT "Enter first input number:"
DEFINE/MAXPAR 1                   ! only 1 parameter expected
IF PARSTAT(1) .EQ. 1 - 
  WRITE/KEYWORD INPUTI/I/7/1 {P1} !store contents of P1 in INPUTI(7) 
