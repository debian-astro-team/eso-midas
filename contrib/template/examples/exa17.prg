!+
! Example 17, MIDAS procedure exa17.prg
!+
DEFINE/PARAM P1 ? IMA "Enter input frame: "
DEFINE/PARAM P2 ? IMA "Enter output frame: "
DEFINE/MAXPAR 2                         ! max. 2 parameters expected
WRITE/KEYWORD IN_A {P1} 
DEFINE/LOCAL MYRESULT/C/1/80 {P2}
RUN tricky.exe
PAUSE
!
INQUIRE/KEYWORD INPUTC "Result frame o.k.? Enter YES or NO: "
IF INPUTC(1:1) .EQ. "Y" THEN 
     ASSIGN/DISPLAY LASER 
     LOAD/IMAGE {MYRESULT} 
ENDIF 
