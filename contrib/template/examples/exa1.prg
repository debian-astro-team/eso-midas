!+ 
! Example 1, MIDAS procedure exa1.prg 
!+ 
READ/KEYWORD {P1}         ! read keyword the name of which is given as par1
@@ test {P2}              ! execute test.prg and pass par2 as first parameter
WRITE/KEYWORD INPUTC {P2} ! write contents of par2 into keyword INPUTC
