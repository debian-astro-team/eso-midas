!+ \\
! Example 19, MIDAS procedure exa19.prg \\
!+ \\
DEFINE/PARAM P1 000 C "Enter control flags for entries: " \\
DEFINE/PARAM P2 sombrero IMA "Enter image to work with: " 
DEFINE/MAXPAR 2                             ! max 2 parameters expected
! 
DEFINE/LOCAL LOOP/I/1/1 0 
DEFINE/LOCAL CCC/C/1/3 {P1(1:3)}
SET/FORMAT I1
DO LOOP = 1 3 
     IF CCC({LOOP}:{LOOP}) .EQ. "1" @@ exa19,000{LOOP} {P2}
ENDDO
!
!  here the different sub-procedures
!
ENTRY 0001
!
CREATE/IMAGE {P1} 2,256,256 ? gauss 128.5,128,128.5,128
!
ENTRY 0002
!
READ/DESCR {P1}
!
ENTRY 0003
!
STATIST/IMAGE {P1}
