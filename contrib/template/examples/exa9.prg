!+ \\
! Example 9, MIDAS procedure exa9.prg \\
!+ \\
WRITE/KEYWORD N/I/1/1 0      ! keywords serve as loop variables
DO N = 1 6 2		     ! loop from N=1 until N<=6 in steps of 2 
   WRITE/OUT N = {N}
ENDDO
