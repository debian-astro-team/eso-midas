! $Id: baches_resolving_power.prg,v 1.3 2011-04-06 14:02:41 midasmgr Exp $
!
! AUTHOR: A.Modigliani 15.10.07
!         Modified by C.Guirao 20.10.07
!
! NAME: baches_resolving_power.prg
!    Compute the resolving power based on the spectrum of a calibrated 
!    thorium lamp
!
! INPUT:
!   - 1-dimensional FITS/BDF image, wavelength calibrated, with the spectrum 
!     a of thorium lamp
!   - MIDAS table FITS/TBL with the XSTART and XEND columns of well 
!     defined 1000 single lines of thorium (default thar_1000lines.fit)
!
! REMARKS: 
! How was the file "thar_300.fits" generated? It was initiated with
! the spectrum of a very good thorium lamp, e..g thorium_wrm, by ploting 
! small intervals and identifying with the Thorium Atlas single, strong and
! isolated lines. The following MIDAS commands were used:
!   INDISK/FITS thorium_wrm.fits thar_lines
!   CREA/GRAPH
!   PLOT/AXES 3900,4000 0,1000
!   OVERPLOT thorium_wrm
!   CENTER/GAUS GCURSOR thar_lines EMISSION A
!
! And select with the cursor, left and right of well defined single lines 
! (no bend) and rather isoloted from other lines.
!
! This generates a thar_lines with the following columns
!   SHOW/TABL thar_lines
!   Col.#   1:START            Unit:WAVELENGTH       Format:F10.3  R*4
!   Col.#   2:END              Unit:WAVELENGTH       Format:F10.3  R*4
!   Col.#   3:CENTER           Unit:WAVELENGTH       Format:F10.3  R*4
!   Col.#   4:INT_START        Unit:FLUX             Format:G15.5  R*4
!   Col.#   5:INT_END          Unit:FLUX             Format:G15.5  R*4
!   Col.#   6:INT_PEAK         Unit:FLUX             Format:G15.5  R*4
!   Col.#   7:INT_FIT          Unit:FLUX             Format:G15.5  R*4
!   Col.#   8:FWHM             Unit:WAVELENGTH       Format:F10.3  R*4
!
! By hand, repeat this sequence increasing 100 Angstroms interval, i.e:
!   PLOT/AXES 4000,4100 0,1000
!   OVERPLOT thorium_wrm
!   CENTER/GAUS GCURSOR thar_lines EMISSION A
!
! After completing the whole spectral range, rename START and END columns
! into XSTART and XEND with MIDAS commands:
!
!   NAME/COLUMN thar_lines :START :XSTART
!   NAME/COLUMN thar_lines :END :XEND
!
! And convert it to FITS:
! 
!   OUTDISK/FITS thar_lines.tbl thorium.fit
!
! OUTPUT:
!   - MIDAS table (basename of input)R.fit: e.g.
!     th1_wrm.fit generates th1_wrmR.fit
!   - If DISPLAY set, it will plot the Resolving Power of all lines, and 
!     generates a PostScript file, e.g. th1_wrmR.ps
!
! SUFIX CONVENTION:
!   b with bias subtraction
!   d with dark-current subtraction
!   f with flat-field division
!   1 averaged to one-dimensional image
!   w calibrated in wavelength
!   r rebined in wavelength
!   s calibrated in flux with a standard star
!   m all orders merged
!   R MIDAS table with computed R (resolving power)
!
! USAGE:
!   RESOLV/BACHES th1_wrm.fit
!
! EXAMPLES:
!   Typically after the calibration of a thorium lamp (th1.fit):
!   Midas 001> CALIBRATE/BACHES ff1.fit th1.fit
!   Midas 002> PIPELINE/BACHES th1.fit
!   Midas 003> RESOLV/BACHES th1_wrm.fit 
!
!*******************************************************************************
! Set ECHO/FULL for debugging purposes
! ECHO/FULL   
! 
! INPUT PARAMS:
! p1 REQUIRED: FITS or BDF 1-dimension calibrated spectrum of a thorium lamp
! p2 OPTIONAL: FITS or TBL MIDAS table with thorium lines identified with 
!              XSTART and XEND columns. (See REMARKS)
 DEFINE/PARAM p1 ? I "Enter 1-dimensional image already calibrated of a thorium lamp? " 
 DEFINE/PARAM p2 thar_1000lines tbl "Enter a table of identified thorium lines with XSTART XEND"
 DEFINE/PARAM p3 2 n            "Enter input kappa (kappa-sigma clip)"
 DEFINE/PARAM p4 5 n            "Enter input number of iterations"

 DEFINE/LOCA lamp/c/1/80 {p1}
 DEFINE/LOCA thar_lines/c/1/80 {p2}
 DEFINE/LOCA table/c/1/80 
 DEFINE/LOCA mean/d/1/1 0
 DEFINE/LOCA std/d/1/1 0
 DEFINE/LOCA kappa/i/1/1 {p3}
 DEFINE/LOCA iter/i/1/1 {p4}
 DEFINE/LOCA it/i/1/1 0

!
! Other local definitions
!
 DEFINE/LOCA frame/c/1/40
 DEFINE/LOCA basename/c/1/40    "" ? +lower_levels
 DEFINE/LOCA ext/c/1/10         "" ? +lower_levels

!
! Gets basename and extension of 1dim file
!
 FILE/BACHES {lamp}
 frame = basename
!
! Convert input 1dim FITS format into MIDAS BDF format
!
! ECHO/FULL
 IF ext .ne. ".bdf" THEN
   INDISK/FITS {lamp} {frame}
 ENDIF
 lamp = frame

!
! Convert input FITS format into MIDAS TBL format
!
 INDISK/FITS MID_HOME:contrib/baches/demo/{thar_lines}.fit {thar_lines}

 table = lamp + "R"
 CENTER/GAUS {lamp},{thar_lines} {table}
 COMPUTE/TABL {table} resol_power=:XCEN/:XFWHM
 NAME/COLU   {table} :XCEN "Angstroms"
 OUTDISK/FITS {table}.tbl {table}.fit

 DO it = 1 {iter}
  STATIS/TABL  {table} :resol_power >Null
  mean = outputr(3)
  std = outputr(4)
  SELECT/TABL {table} :resol_power.gt.({mean}-{kappa}*{std}).and.:resol_power.lt.({mean}+{kappa}*{std})
 ENDDO

 STATIS/TABL  {table} :resol_power >Null
 mean = outputr(3)
 std = outputr(4)
 CREATE/GRAP
 SET/GRAP pmode=0
! SET/GRAP pmode=2
 PLOT/TABL {table} :XCEN :resol_power
 LABEL/GRAP "{table}" 5,5,mm 0 1 1

 COPY/GRAP postscript
 $ mv postscript.ps {table}.ps

! READ/TABL {table} :XCEN,resol_power >{table}.txt

 WRITE/OUT "Resolving power computed in MIDAS table: {table}.fit"
 WRITE/OUT "Postscript file generated in {table}.ps"
 WRITE/OUT "Average (kappa-sigma-cleaned) resolution power: {mean} +/- {std}"
!
! Clean all temporary files.
!
! CLEAN/BACHES
!
 ECHO/OFF
