! $Id: baches_recalib.prg,v 1.2 2009-09-09 08:22:50 midasmgr Exp $
! $Name: not supported by cvs2svn $
!
! AUTHOR: C. Guirao
!
! NAME: baches_recalib.prg
!   Semi-automatic wavelength procedure for echelle spectra BACHES.
!   The calibration is based in the ECHELLE package of MIDAS which assumes
!   raw-images oriented so that orders appears along the rows, withe low orders
!   up and wavelength increases from left to right, i.e. red is up and left.
!   START and STEP descriptors of the image must be positive.
!
! INPUT:
!   - 2-dimensional FITS/BDF image with the spectrum of a calibration lamp.
!   - MIDAS table of the calibration lamp in FITS format. Default "thar.fit"
! OUTPUT:
!   - ECHELLE session files for BACHES. Result of the "SAVE/ECHE baches"
!     generates: bachesLINE.tbl and bachesORDE.tbl
!
!
! SUFIX CONVENTION:
!   b with bias subtraction
!   d with dark-current subtraction
!   f with flat-field division
!   1 averaged to one-dimensional image
!   w calibrated in wavelength
!   r rebined in wavelength
!   s calibrated in flux with a standard star
!   m all orders merged
!
! USAGE: RECAL/BACHES <lamp> 
!*******************************************************************************
! ECHO/FULL
!
! INPUT PARAMS:
! p1 REQUIRED: FITS or BDF 2-dimension of image of spectrum of a calibration lamp
! p2 OPTIONAL: Table containing lines of calib. lamp.
! p3 OPTIONAL: Final tolerance on RMS error
! p4 OPTIONAL: Final degree of polynomial defining dispersion
!
! Edit here to change defaults:
!
 DEFINE/PARA p1 ? I "Enter 2-dimensional image with the spectrum of a calibration lamp? "
 DEFINE/PARA p2 thar.fit  TAB
 DEFINE/PARA p3 0.4 NUM
 DEFINE/PARA p4 3   NUM
!
! Convert paramaters into variables:
!
 DEFINE/LOCA cmd/c/1/20         "baches_recalib"
 DEFINE/LOCA lamp_frame/c/1/40  {p1}
!
! Other local definitions
!
 DEFINE/LOCA frame/c/1/40
 DEFINE/LOCA basename/c/1/40    "" ? +lower_levels
 DEFINE/LOCA ext/c/1/10         "" ? +lower_levels
 DEFINE/LOCA session/c/1/40     "baches"
!
! MAIN, main:
!
 WRITE/OUT
 WRITE/OUT "PARAMETERS FOR THIS CALIBRATION:"
 WRITE/OUT "==============================="
 WRITE/OUT "Calibration lamp = {lamp_frame}"
 WRITE/OUT
!
!
#WRITE/KEYW reply/c/1/1 y
#INQUIRE/KEYW reply "{cmd}: Do you want to continue [yn] (y)?"
#IF reply(:1) .eq. "y" THEN
#  GOTO continue
#ELSE
#  RETURN/EXIT
#ENDIF
!
!
! CONTINUE
continue:
!
! Set context ECHELLE, if not yet already
!
! SHOW/CONT echelle >NULL
! IF  OUTPUTI(5) .eq. 0 THEN
!   SET/CONTEXT echelle >NULL
! ENDIF
! RESET/DIS >NULL
! INITIA/ECHE {session}
!
! Start creating a display
!
 CREATE/DISP 0 590,390,0,25
 LOAD/LUT heat
!
! Gets basename and extension of input file
!
 FILE/BACHES {lamp_frame}
 frame = basename
!
! Convert FITS format into MIDAS BDF format
!
 IF ext .ne. ".bdf" THEN
   COMPUTE/BACHES {lamp_frame} {frame}
 ENDIF
 CUTS/IMAG {frame} =sigma
 DISPLA/ECHE {frame}

 SET/ECHE wlc = {frame}
 EXTRAC/ECHE {wlc} extwlc
 SEARCH/ECHE extwlc
 LOAD/SEAR
! SAVE/ECHE {session} 

! SET/ECH guess=baches
 SET/ECH wlcmtd=restart
 IDENTI/ECH 
 SAVE/ECHE {session} 
! SET/ECH wlcmtd=guess
! IDENTI/ECH 
! SAVE/ECHE {session} 
!
! Save session in fits
!
 WRITE/OUT "{cmd}: Identification completed"
 OUTDISK/FITS bachesORDE.tbl bachesORDE.fit
 OUTDISK/FITS bachesLINE.tbl bachesLINE.fit
 WRITE/KEYW reply/c/1/1 y
 INQUIRE/KEYW reply "{cmd}: Do you want to clean temporary files [yn] (y)?"
 IF reply(:1) .eq. "y" THEN
   GOTO clean
 ELSE
   GOTO noclean
 ENDIF
!
! Clean/NoClean
!
clean:
 CLEAN/BACHES
!
noclean:
 RESET/DISP
 ECHO/OFF
!
 WRITE/KEYW reply/c/1/1 y
 INQUIRE/KEYW reply "{cmd}: Do you want to calculate R for {lamp_frame} [yn] (y)?"
 IF reply(:1) .eq. "y" THEN
   GOTO calculateR
 ELSE
   GOTO nocalculateR
 ENDIF
!
! calculateR/nocalculateR
!
 WRITE/OUT "RESOLV/BACHES {frame}_wrm.fit"
 RESOLV/BACHES {frame}_wrm.fit
!
 ECHO/OFF
 RETURN/EXIT

nocalculateR:
 RESET/DISP
 ECHO/OFF
 RETURN/EXIT

# WRITE/KEYW reply/c/1/1 y
# INQUIRE/KEYW reply "{cmd}: Do you want to calculate R for {lamp_frame} [yn] (y)?"
# IF reply(:1) .eq. "y" THEN
#   GOTO calculateR
# ELSE
#   GOTO nocalculateR
# ENDIF
!
! calculateR/nocalculateR
!
calculateR:
 WRITE/OUT "RESOLV/BACHES {lamp_frame}"
 RESOLV/BACHES {lamp_frame}
!
! Gets basename and extension of calibration lamp frame
!
 FILE/BACHES {lamp_frame}
 frame = basename
!
 WRITE/OUT "RESOLV/BACHES {frame}_wrm.fit"
 RESOLV/BACHES {frame}_wrm.fit
!
 ECHO/OFF
 RETURN/EXIT

nocalculateR:
 RESET/DISP
 ECHO/OFF
 RETURN/EXIT
