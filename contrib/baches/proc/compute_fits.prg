! $Id: compute_fits.prg,v 1.2 2011-05-04 08:03:26 midasmgr Exp $
!
! AUTHOR: C.Guirao
! NAME: compute_fits.prg: 
!  Convert FITS to BDF and compute any other need in the BDF image.
! 
! USAGE: @@ compute_fits.prg inframe [frame]
! EXAMPLES:
!  @@ compute_fits.prg ff01.fit flat01
!  @@ compute_fits.prg ff01.mt
!*******************************************************************************
! ECHO/FULL
!
! INPUT PARAMS:
! p1 REQUIRED: FITS image to be converted
! p2 OPTIONAL: BDF image
!
! Edit here to change defaults:
!
 DEFINE/PARA p1 ? I "Enter input FITS frame ? "
 DEFINE/PARA p2 + ? "Enter output BDF frame ? "
!
! Convert paramaters into variables:
!
 DEFINE/LOCA cmd/c/1/30  "compute_fits.prg"
 DEFINE/LOCA inframe/c/1/40 	{p1}
 DEFINE/LOCA frame/c/1/40	{p2}
 DEFINE/LOCA fitsframe/c/1/40
 DEFINE/LOCA indx/i/1/1         0
 DEFINE/LOCA basename/c/1/40    "" ? +lower_levels
 DEFINE/LOCA ext/c/1/10         "" ? +lower_levels
!
! Gets basename and extension of input file
!
 FILE/BACHES {inframe}
 IF ext .eq. ".bdf" THEN
    WRITE/OUT "{cmd}: ERROR. There is already a file {basename}{ext}"
    RETURN/EXIT   
 ENDIF
 fitsframe = basename + ext
!
! If BDF frame name {p2} is missing, use fitsframe without extension.
!
 IF p2 .eq. "+" THEN
   frame = basename
 ENDIF
!
! Convert the FITS file into a MIDAS .bdf
!
 INDISK/FITS {fitsframe} {frame} >NULL
 WRITE/OUT "{cmd}: {fitsframe} converted to {frame}.bdf"
!==========================================================
! Typical operations in the raw images before proceding with 
! calibration or pipeline. Uncoment what could apply to your
! CCD:
!
! Flip image up/down right/left
!-------------------------------
!
! WRITE/OUT "{cmd}: {frame} flipped XY"
! flip/ima {frame} xy
! write/desc {frame} start/d/1/2 1,1
! write/desc {frame} step/d/1/2 1,1
!
! Convert values to positive range (part1)
!-------------------------------------------
! Other controllers (like Hale) generates pixel values in the range
! -32768 to +32768. This can be fixed with the COMPUTE command
! by adding 32768 to any value.
!
! COMPUTE/IMAGE P_{frame} = ({frame} + 32768)
! RENAME P_{frame} {frame}
! WRITE/OUT "{cmd}: {frame}.bdf added +32768"
!
!
! Convert values to positive range (part2)
!-------------------------------------------
! Some controllers generates negative numbers for pixel value above half
! the scale (e.g. in a 16bit controller, the scale goes from 0 to 65536,
! however any value above 32768 is converted in a negative value so the
! scale goes from 0 to 32768 and -32768 to 0. This error can be fixed
! with MIDAS with a REPLACE operation that adds 65536 to any negative value.
!
! REPLACE/IMAGE {frame} P_{frame} <,-0.00001={frame}+65536
! RENAME P_{frame} {frame}
! WRITE/DESC {frame} LHCUTS/D/1/2 0 all
! WRITE/OUT "{cmd}: {frame}.bdf negative numbers +65536"
!
! Rotation
!---------
!
! ROTATE/CLOCK {frame} R_{frame}
! ROTATE/COUNTER_CLOCK {frame} R_{frame}
! RENAME R_{frame} {frame}
!
! Flipping images (x, y or both)
!-------------------------------
! Other controllers generates images from RB (red to blue) instead of
! BR (blue to red). They need to be flipped and their START & STEP
! descriptors to be rewriten.
!
! FLIP/IMAG {frame}
! WRITE/DESC {frame} START/d/1/2 1.,1.
! WRITE/DESC {frame} STEP/d/1/2 1.,1.
! WRITE/OUT "{cmd}: flipping {frame} to put blue to left"
!
! Correcting hot columns
!------------------------ 
! The column 381 is a hot column, it has signal plu an additional constant 
! offset. It can be corrected with the two columns to the left and two 
! columns to the right using MODIFY/COLUM
!
! WRITE/OUT "{cmd}: Correcting hot column 381"
! MODIFY/COLU {frame} P_{frame} C @381 >NULL
! RENAME P_{frame} {frame}
!
! Other corrections in columns
!------------------------------
! Columns 1,511 and 512 in HaleRed are defect. They are substituted with command
! COMPUTE/COLU with adjacents columns.
!
! WRITE/OUT "{cmd}: Correcting bad columns 1,511 and 512"
! COMPUTE/COLU {frame}.C1 = C2
! COMPUTE/COLU {frame}.C511 = C510
! COMPUTE/COLU {frame}.C512 = C510
!
! Columns 1,2 and 512 in HaleBlue are defect. They are substituted with command
! COMPUTE/COLU with adjacents columns.
!
! WRITE/OUT "{cmd}: Correcting bad columns 1,2 and 512"
! COMPUTE/COLU {frame}.C1 = C3
! COMPUTE/COLU {frame}.C2 = C3
! COMPUTE/COLU {frame}.C512 = C511
!
! Hot pixel corrections
!-------------------------
! Fix HOT (saturated) pixels in HALE-Blue
!
! WRITE/OUT "{cmd}: Correcting bad pixels at 149,250 and 149,251"
! FIXP/BACHES {frame} 149,250
! FIXP/BACHES {frame} 149,251
!
! Rotation
!-----------
! Rotating the images from the central pixel (765,65) 0.15 grad in order
! align the spectra.
! WRITE/OUT "{cmd}: Rotating image .15,765,65
! REBIN/ROTA {frame} R_{frame} .15,765,65
! RENAME R_{frame} {frame}
!
! Cleaning tempoary files
!
! WRITE/OUT {cmd}: Removing temporary files
 $rm -f NULL
 $rm -f midlis*.prg
!
! ECHO/OFF
