! $Id
!
! AUTHOR: C. Guirao
!
! NAME: baches_dark_subtraction.prg
!   Procedure to subtract dark current from a FITS or BDF frame
!   If the input file is a FITS it is converted to BDF with COMPUTE/BACHES
!   and bias_subtracted if master_bias exists.
!   It requires that a table master_dark.tbl exists and the 
!   master dark frames refered in master_dark.tbl. This master_dark.tbl is
!   generated with the command 
!     "DARK/BACHES master_dark10,master_dark20"
!
! INPUT: 
!   - Frame in FITS or BDF format with a spectrum in rows
!   - Frame in BDF format with result
!
! OUTPUT: 
!   A new frame_dark.bdf = frame.bdf - master_dark.bdf
!
! USAGE: DARK/BACHES frame.bdf frame_dark.bdf
!    or: DARK/BACHES frame.fit frame_dark.bdf
!
!*******************************************************************************
! ECHO/FULL
!
! INPUT PARAMS:
! p1 REQUIRED: BDF or FITS frame containing an spectrum in rows
! p2 OPTIONAL: BDF frame with result of subtraction (default: frame_dark)
!
! Edit here to change defaults:
!
 DEFINE/PARA p1 ?	IMA "ENTER frame to be dark subtracted: "
 DEFINE/PARA p2 ?	IMA "ENTER name for result frame:"
!
! Convert paramaters into variables:
!
 DEFINE/LOCA cmd/c/1/20  	"dark_subtraction"
 DEFINE/LOCA input/c/1/40   	{p1}
 DEFINE/LOCA output/c/1/40      {p2}
 DEFINE/LOCA frame/c/1/40
 DEFINE/LOCA master_table/c/1/40 "master_dark.tbl"
 DEFINE/LOCA bias/c/1/40      	"master_bias.bdf"
 DEFINE/LOCA dark/c/1/40   	
 DEFINE/LOCA exposure/i/1/1 0
 DEFINE/LOCA frame_exposure/i/1/1 0
 DEFINE/LOCA dark_exposure/i/1/1  0
 DEFINE/LOCA coeficient/r/1/1 0.
 DEFINE/LOCA best_coef/r/1/1 0.
 DEFINE/LOCA current_coef/r/1/1 0.
 DEFINE/LOCA nrow/i/1/1       0
 DEFINE/LOCA loop/i/1/1       0
 DEFINE/LOCA best_row/i/1/1   0
 DEFINE/LOCA basename/c/1/40    "" ? +lower_levels
 DEFINE/LOCA ext/c/1/10         "" ? +lower_levels
!
! Check if master dark_current table 
!
IF m$exist(master_table) .eq. 0 THEN
  WRITE/OUT "{cmd}: ERROR: {master_table} does not exist!!"
  WRITE/OUT "{cmd}: recreate it with MDARK/BACHES dark*.bdf"
  ECHO/OFF
  RETURN/EXIT
ENDIF
!
! Gets basename and extension of input file
!
 FILE/BACHES {input}
 frame = basename
!
! Convert FITS format into MIDAS BDF format
!
 IF ext .ne. ".bdf" THEN
   WRITE/OUT "{cmd}: converting {input} into {frame}"
   COMPUTE/BACHESfits {input} {frame}
   !
   ! Bias subtraction
   !
   IF m$exist(bias) .eq. 1 THEN
     COMPUTE/IMA {frame}b = {frame} - {bias}
     RENAME {frame}b {frame}
     WRITE/OUT "{cmd}: creating {frame} with {bias} subtracted"
   ELSE
     WRITE/OUT "{cmd}: {bias} not found. No bias substructed"
   ENDIF
 ENDIF
!
! Check number of entries in  master dark_current table 
!
 SHOW/TAB {master_table} >NULL
 nrow = OUTPUTI(2)
 WRITE/OUT "{cmd}: number of entries in {master_table} = {nrow}" 
!
! Get the exposure time
!
 frame_exposure = m$value({frame},O_TIME(7))
 WRITE/OUT "{cmd}: exposure time for {frame}: {frame_exposure}"
!
! Find the closest dark_current to our frame 
!
current_coef = 0.
DO loop = 1 {nrow}
  exposure = {{master_table},:EXPOSURE,@{loop}}
  IF exposure .gt. frame_exposure THEN 
   current_coef = exposure / frame_exposure
 ELSE
   current_coef = frame_exposure / exposure 
 ENDIF
  dark = "{{master_table},:FRAME,@{loop}}"
  WRITE/OUT "{cmd}: file {dark}, exposure {exposure}, coeficient {current_coef}"
  IF {loop} .eq. 1 THEN
    best_coef = current_coef
    best_row = loop
  ENDIF
  IF best_coef .gt. current_coef THEN
    best_coef = current_coef
    best_row = loop
  ENDIF
ENDDO
!
! Best dark 
!
dark = "{{master_table},:FRAME,@{best_row}}"
WRITE/OUT "{cmd}: best dark in file {dark} with coeficient {best_coef}"
!
 dark_exposure = m$value({dark},O_TIME(7))
 coeficient = frame_exposure / dark_exposure
 COMPUTE/IMAG my_master_dark = ({dark} * {coeficient})
 COMPUTE/IMAG {output} = ({frame} - my_master_dark)
 DELETE/IMA my_master_dark NO
!
WRITE/OUT "{cmd}: Exposure value in {frame} = {frame_exposure}"
WRITE/OUT "{cmd}: Exposure value in {dark} = {dark_exposure}"
WRITE/OUT "{cmd}: Coeficient for my_master_dark = {coeficient}"
!
 ECHO/OFF
