! $Id: baches_compute_fits.prg,v 1.5 2011-05-04 08:03:26 midasmgr Exp $
!
! AUTHOR: C.Guirao
! NAME: baches_compute_fits.prg: 
!  Convert FITS to BDF and compute any other need in the BDF image.
!  It copies the real script compute_fits.prg to the local directory, so that
!  it can be modified to adapt it to local settings.
! 
! USAGE: COMPUTE/BACHES inframe [frame]
! EXAMPLES:
!  COMPUTE/BACHES ff01.fit flat01
!  COMPUTE/BACHES ff01.mt
!*******************************************************************************
! ECHO/FULL
!
! INPUT PARAMS:
! p1 REQUIRED: FITS image to be converted
! p2 OPTIONAL: BDF image
!
! Edit here to change defaults:
!
 DEFINE/PARA p1 ? I "Enter input FITS frame ? "
 DEFINE/PARA p2 + ? "Enter output BDF frame ? "
!
! Convert paramaters into variables:
!
 DEFINE/LOCA cmd/c/1/30  "compute_fits.prg"
 DEFINE/LOCA inframe/c/1/40 	{p1}
 DEFINE/LOCA frame/c/1/40	{p2}
!
 DEFINE/LOCAL testdir/c/1/60 "$MID_HOME/contrib/proc/ "
 DEFINE/LOCAL defdir/c/1/2 "./"
 IF m$exist(cmd) .eq. 0 THEN
   $ cp {testdir}{cmd} {defdir}
   WRITE/OUT "File {cmd} first copied to the current directory."
   WRITE/OUT "Edit it if necessary to adapt it to your needs."
   WRITE/OUT "By default just converts FITS file into BDF format"
 ENDIF
 @@ compute_fits.prg {inframe} {frame}
! ECHO/OFF
