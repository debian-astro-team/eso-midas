! $Id: baches_clean.prg,v 1.1 2009-06-11 15:30:29 midasmgr Exp $
! $Name: not supported by cvs2svn $
!
! AUTHOR: C. Guirao
!
! NAME: clean.prg
!   Remove the garbage left by MIDAS
!
!*******************************************************************************
! ECHO/FULL

 DEFINE/LOCA cmd/c/1/20         "clean"

!
! Clean
!
 WRITE/OUT "{cmd}: cleaning temporary files"
 $ rm -f *.dat *.bdf *.tbl *.cat *.KEY *.plt 
 $ rm -f NULL
 $ rm -rf core*
 $ rm -f IDENTI.res Mid*Pipe
! Do not remove bachesIORDER.fit to avoid re-identification of orders
! $ rm -f bachesIORDE.*
!
 ECHO/OFF
