! $Id: baches_file_ext.prg,v 1.3 2011-05-04 08:00:50 midasmgr Exp $
!
! AUTHOR: C.Guirao
! NAME: file_ext
!  Test if filename exits and return
!  return basename and extension
!  If extension is not given, it looks first for .bdf, otherwise
!  try to find <file>.fit
!
! INPUT: 
!  - filename to be search 
!
! REMARK:
!        This procedure can only be used within other procedures that define:
!	 DEFINE/LOCA basename/c/1/40    "" ? +lower_levels
!	 DEFINE/LOCA ext/c/1/10         "" ? +lower_levels
!
! OUTPUT:
!  - Two local definitions (DEFINE/LOCAL)  are set: basename & extension 
!
! USAGE: FILE/BACHES <filename>
! 
! EXAMPLES:
!    Example: FILE/BACHES andromeda.fit
!             basename = andromeda
!             ext = .fit
!
!    Example: FILE/BACHES andromeda
!             basename = andromeda
!             ext = .bdf	(if andromeda.bdf exist)
!    or
!             ext = .fit	(if andromeda.fit exist but no andromeda.bdf)
!*******************************************************************************
! ECHO/FULL
!
! INPUT PARAMS:
! p1 REQUIRED: filename to be checked
!
 DEFINE/PARA p1 ? I "Enter FITS/BDF image to be checked "
!
! Convert paramaters into variables:
!
 DEFINE/LOCA name/c/1/80 	{p1}
! Next two local variables are defined in the calling procedure
! DEFINE/LOCA basename/c/1/80
! DEFINE/LOCA ext/c/1/10
 DEFINE/LOCA name_bdf/c/1/80
 DEFINE/LOCA name_fit/c/1/80
 DEFINE/LOCA indx/i/1/1 	0
!
! Strip the extension to input file (anything after ".")
!
 indx = m$index(name,".")
 IF indx .ne. 0 THEN
   IF m$exist(name) .eq. 0 THEN
     WRITE/OUT "ERROR: File {name} NOT FOUND"
     RETURN/EXIT
   ENDIF
   ext = name({indx}:)
   indx = indx-1
   basename = name(1:{indx})
 ELSE
   basename = name
   ext = ".bdf"
   name_bdf = basename + ".bdf"
   IF m$exist(name_bdf) .eq. 0 THEN
     ext = ".fit"
     name_fit = basename + ".fit"
     IF m$exist(name_fit) .eq. 0 THEN
       WRITE/OUT "ERROR: Neither file {name}.fit nor {name}.bdf FOUND"
       RETURN/EXIT
     ENDIF
   ENDIF
 ENDIF
!
! WRITE/OUT "  Name: {name}"
! WRITE/OUT "  Basename: {basename}"
! WRITE/OUT "  Extension: {ext}"
!
 ECHO/OFF
