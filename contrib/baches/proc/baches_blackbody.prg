! $Id: baches_blackbody.prg,v 1.2 1999/08/13 14:53:53 cguirao Exp $
! $Name: FIASCO-1_1 $
!
! AUTHOR: C. Guirao
!
! NAME: blackbody.prg
!   Creates a table with FLUX vs WAVELENGTH for given input TEMPERATURE
!   It plots a graphic in a given  wavelength range
!
! INPUT:
!   P1: (required) Input temperature in Kelvin
!   P2: (optional) WAVE range in Angstroms to be ploted
!
! OUTPUT:
!   Table blackbody.mt (FITS) and a plot in the given wavelength range
!
! USAGE: @@ blackbody T [Wawelength_range]
!
! EXAMPLES: @@ blackbody 3200 3000,8000
!  
! REMARKS:
!   Physical Constants (c.g.s units)
!   c -> Speed of light
!   h -> Planck's constant
!   k -> Bolzmann's constant
!
!*******************************************************************************
! ECHO/FULL
!
! INPUT PARAM:
! p1 REQUIRED: Temperature in Kelvin degrees 
! p2 OPTIONAL: Wavelength range in angstroms
!
! Edit here to change defaults:
!
 DEFINE/PARA p1 ? NUM "Enter temperature (K) to calculate blackbody radiation: "
 DEFINE/PARA p2 3000,8000 NUM
!
! Convert parameters into variables:
!
 DEFINE/LOCA temperature/d/1/1 {p1}
 DEFINE/LOCA temp/i/1/1        {p1}
 DEFINE/LOCA wave_range/i/1/2  {p2}  
!
 DEFINE/LOCA reply/c/1/1  y
 DEFINE/LOCA printer/c/1/40      "postscript"
!
!   Equation for black-body emission:
!
 DEFINE/LOCA c/d/1/1 3.0e10
 DEFINE/LOCA h/d/1/1 6.63e-27
 DEFINE/LOCA k/d/1/1 1.38e-16
!
! CREATE/TABL &t 60 60
 CREATE/TABL &t 1200 1200
 CREATE/COLU &t :WAVE   R*8 F6.1 "ANGSTROM"
 CREATE/COLU &t :FLUX_W R*8 E12.4 "Bv(T)e2"
 COMPUT/TABL &t :W = 2e-6 + (seq+1)*1e-7
 COMPUT/TABL &t :WAVE = 200 + (seq+1)*10
 COMPUT/TABL &t :F = (2*{h}*{c}**2)/(:W**5)
 COMPUT/TABL &t :FLUX_W = 300000*:F/(EXP(({h}*{c})/(:W*{k}*{temperature})) - 1)*1e-14
!
 COPY/TABL &t blackbody_{temp}
!
! PLOT in graphical display
!
!
! SET/GRAP DEFAULT=
! SET/GRAP pm=1 font=1 stype=0
! DELETE/GRAP 0 > NULL
! CREATE/GRAP 0
!
! PLOT/TABL &t :WAVE :FLUX_W
! SET/GRAP colour=2
! OVERPLOT/TABL &t :WAVE :FLUX_W
!
! SET/GRAP colour=1
! OVERPL/LINE 2 77,0,mm 77,108,mm
! OVERPLOT/LINE 2 112,0,mm 112,108,mm
! LABEL/GRAPH "Blackbody Radiation at {temp} K" 80,40,mm 0 1 1 
! LABEL/GRAPH "U-V" 50,100,mm 0 1 1
! LABEL/GRAPH "Visible" 85,100,mm 0 1 1
! LABEL/GRAPH "I-R" 130,100,mm 0 1 1
!
! askToContinue:
! WRITE/KEYW reply/c/1/1 y
! INQUIRE/KEYW reply "Creating a postscript file [yn] (y)?"
! IF reply(:1) .eq. "y" THEN
!   COPY/GRAPH {printer}
!   WRITE/OUT "Converting plot to postscript file blackbody_{temp}.ps"
!   $mv {printer}.ps blackbody_{temp}.ps
!   GOTO continue
! ELSEIF reply(:1) .eq. "n" THEN
!   GOTO continue
! ELSE
!   GOTO askToContinue
! ENDIF
! continue:
!
! askRemove:
! WRITE/KEYW reply/c/1/1 y
! INQUIRE/KEYW reply "Remove all temporary files [yn] (y)?"
! IF reply(:1) .eq. "y" THEN
!   GOTO remove
! ELSEIF reply(:1) .eq. "n" THEN
!   ECHO/OFF
!   RETURN/EXIT
! ELSE
!   GOTO askRemove
! ENDIF
! remove:
!
! Clean all temporary files
!
! WRITE/OUT "Cleaning temporary files and finishing"
 $rm -f graph_wn*.plt
 $rm -f lntab.tbl
 $rm -f middummt.tbl
 ECHO/OFF
