%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  2009 European Southern Observatory
%.IDENT      pipelibach.hlq
%.AUTHOR     CG, SDD/SED/ESO
%.KEYWORDS   MIDAS, help files,
%.PURPOSE    On-line help file for the command: PIPELINE/BACHES
%.VERSION    1.0  08-SEP-09 : Creation, CG
%----------------------------------------------------------------
\se
SECTION./BACHES
\es\co
PIPELINE/BACHES                                             09-SEP-09  CG
\oc\su
PIPELINE/BACHES  image
          Pipeline for BACHES instrument
\us\pu
Purpose:
          Procedure to reduce a 2-dimensional echelle spectra
          image.  The procedure calibreate in wavelenth and extract the
          signal from the science spectrum. The result is 1-dimensional 
          image calibrated in wavelenth.
\up\sub
Subject:
          BACHES, Spectroscopy
\up\sy
Syntax:
          PIPELINE/BACHES  image
\ys\pa
          image =  CCD image in FITS format with an echelle spectrum
\ap\see
See also:
          CALIB/BACHES
\ees\no
Note:    
          The pipeline requires the two calibration tables:
             bachesLINE.fit 
             bachesORDE.fit 
\\
          created with the interactive procedure CALIB/BACHES

\on\exs
Examples:
\ex
          PIPELINE/BACHES sirius.fit
\xe\sxe
