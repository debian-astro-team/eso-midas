C @(#)cpfram.for	19.1 (ES0-DMD) 02/25/03 13:30:45
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++       
C.IDENTIFICATION: CPFRAM                                                  
C.AUTHOR:         R.H. Warmels     ESO Garching                        
C.PURPOSE:        Copy an image frame into a non-existing output frame
C.INPUT/OUTPUT:   INAR    real  array       input image frame               
C                 OUTAR   real  array       output image frame
C.VERSION:        880831  RHW  Creation
C -----------------------------------------------------------------------       
      SUBROUTINE CPFRAM(INAR,OUTAR,NCOL,NROW)
C
      IMPLICIT NONE
      INTEGER  NCOL
      INTEGER  NROW
      REAL     INAR(NCOL,NROW)
      REAL     OUTAR(NCOL,NROW)
C
      INTEGER  I
      INTEGER  J
C                                                                               
C                       
      DO 20 J = 1,NROW
         DO 10 I = 1,NCOL
            OUTAR(I,J) = INAR(I,J)
   10    CONTINUE
   20 CONTINUE
C
      RETURN
      END

