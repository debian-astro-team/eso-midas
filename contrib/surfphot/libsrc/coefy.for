C @(#)coefy.for	19.1 (ES0-DMD) 02/25/03 13:30:45
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE COEFY(Y,C,CC,IDEG)                                             
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
C.PURPOSE: Evaluates coefficients depending on x coordinate only for 2-d.pol.   
C --------------------------------------------------------------------------    
      IMPLICIT         NONE
      DOUBLE PRECISION C(21)                                            
      DOUBLE PRECISION CC(6)                                            
      DOUBLE PRECISION Y                                            
      INTEGER          IDEG
                                                                                
      GO TO (50,40,30,20,10),IDEG                                               
                                                                                
   10 CC(1)  = ((((C(21)*Y+C(15))*Y+C(10))*Y+C(6))*Y+C(3))*Y + C(1)             
      CC(2)  = (((C(20)*Y+C(14))*Y+C(9))*Y+C(5))*Y + C(2)                       
      CC(3)  = ((C(19)*Y+C(13))*Y+C(8))*Y + C(4)                                
      CC(4)  = (C(18)*Y+C(12))*Y + C(7)                                         
      CC(5)  = C(17)*Y + C(11)                                                  
      CC(6)  = C(16)                                                            
      RETURN                                                                    
                                                                                
   20 CC(1)  = (((C(15)*Y+C(10))*Y+C(6))*Y+C(3))*Y + C(1)                       
      CC(2)  = ((C(14)*Y+C(9))*Y+C(5))*Y + C(2)                                 
      CC(3)  = (C(13)*Y+C(8))*Y + C(4)                                          
      CC(4)  = C(12)*Y + C(7)                                                   
      CC(5)  = C(11)                                                            
      RETURN                                                                    
                                                                                
   30 CC(1)  = ((C(10)*Y+C(6))*Y+C(3))*Y + C(1)                                 
      CC(2)  = (C(9)*Y+C(5))*Y + C(2)                                           
      CC(3)  = C(8)*Y + C(4)                                                    
      CC(4)  = C(7)                                                             
      RETURN                                                                    
                                                                                
   40 CC(1)  = (C(6)*Y+C(3))*Y + C(1)                                           
      CC(2)  = C(5)*Y + C(2)                                                    
      CC(3)  = C(4)                                                             
      RETURN                                                                    
                                                                                
   50 CC(1)  = C(3)*Y + C(1)                                                    
      CC(2)  = C(2)                                                             
      RETURN                                                                    
                                                                                
      END                                                                       
