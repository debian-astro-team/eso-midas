C @(#)fllgrd.for	19.1 (ES0-DMD) 02/25/03 13:30:45
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FLLGRD(DATA,ALPHA0)                                            
C +++                                                                           
C --- 
      IMPLICIT    NONE	
      INTEGER     NDIM
      INTEGER     NSEP
      PARAMETER  (NDIM=510) 
      PARAMETER  (NSEP= 60)                                                                          
      REAL       DATA(NDIM*NDIM)
      REAL       ALPHA0                                           
C
      INTEGER    I,J
      INTEGER    IXD,IYD                                                    
      REAL       ALPHA
      REAL       X,Y
      REAL       RSQUAR                                              
      REAL       XORIG,YORIG                                                    
C                                                                               
      DATA       XORIG/100./
      DATA       YORIG/255./                                        
C                                                                               
      DO 10 I = 1,NDIM*NDIM            ! first fill the entire frame with zero's
         DATA(I) = 0.                                                           
   10 CONTINUE                                                                  
C                                                                               
C *** then define the grid with NSEP pixels spacing
C     begin with the vertical lines                                             
      DO 30 I = 1,9                                                             
         X      = (I-1)*NSEP + 15 - XORIG 
         DO 20 J = 1,NDIM     
            Y      = J - YORIG                              !   rotate pixel by
            RSQUAR = X**2 + Y**2                                ! alpha radians
            ALPHA  = RSQUAR*ALPHA0                              ! about xorig 
            IXD    = NINT(X*COS(ALPHA)-Y*SIN(ALPHA)+XORIG)          ! and yorig
            IYD    = NINT(X*SIN(ALPHA)+Y*COS(ALPHA)+YORIG)                     
            IF ((IXD.GE.1) .AND. (IXD.LE.NDIM) .AND. (IYD.GE.1) .AND.
     +          (IYD.LE.NDIM)) THEN 
               DATA((IYD-1)*NDIM+IXD) = 100. 
            ENDIF                                                               
   20    CONTINUE                                                               
   30 CONTINUE                                                                  
C                                                                               
C *** and now for the horizontal lines                                          
      DO 50 J = 1,9                                                             
         Y = (J-1)*NSEP + 15 - YORIG 
         DO 40 I = 1,NDIM
            X      = I - XORIG                                                 
            RSQUAR = X**2 + Y**2                                               
            ALPHA  = RSQUAR*ALPHA0                                            
            IXD    = NINT(X*COS(ALPHA)-Y*SIN(ALPHA)+XORIG)                     
            IYD    = NINT(X*SIN(ALPHA)+Y*COS(ALPHA)+YORIG)                     
            IF ((IXD.GE.1) .AND. (IXD.LE.NDIM) .AND. (IYD.GE.1) .AND.
     +          (IYD.LE.NDIM)) THEN 
               DATA((IYD-1)*NDIM+IXD) = 100. 
            ENDIF                                                               
   40    CONTINUE                                                               
   50 CONTINUE                                                                  
C                                                                               
C *** job done                                                                  
      RETURN                                                                    
      END                                                                       
