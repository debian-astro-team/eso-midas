C @(#)outdim.for	19.1 (ES0-DMD) 02/25/03 13:30:47
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE OUTDIM(START,STEP,NPIX,CX,CY,OUTSTA,OUTPIX)
C +++
C.PURPOSE: Determine starting coordinates and stepsizes of output frame
C ---
      DOUBLE PRECISION START(2)
      DOUBLE PRECISION STEP(2)
      INTEGER  NPIX(2)
      DOUBLE PRECISION CX(20)
      DOUBLE PRECISION CY(20)
      REAL     OUTSTA(2)
      INTEGER  OUTPIX(2)
C
      INTEGER  I
      REAL     END(2)
      REAL     OUTEND(2)
      REAL     CORNER(4,2)
      REAL     OUTXMN,OUTXMX,OUTYMN,OUTYMX
      DOUBLE PRECISION X,Y
      DOUBLE PRECISION OUTX,OUTY
C
      DATA     OUTXMN/1.E8/
      DATA     OUTXMX/-1.E8/
      DATA     OUTYMN/1.E8/
      DATA     OUTYMX/-1.E8/

C
      DO 10 I = 1,2
         END(I) = START(I) + (NPIX(I)-1)*STEP(I)
   10 CONTINUE
C
C *** For the type of distortions caused by most image tubes, the space
C     needed for the output frame can be rather safely determined
C     from the four corners of the input frame. these are:
C
      CORNER(1,1) = START(1)
      CORNER(1,2) = START(2)
      CORNER(2,1) = START(1)
      CORNER(2,2) = END(2)
      CORNER(3,1) = END(1)
      CORNER(3,2) = START(2)
      CORNER(4,1) = END(1)
      CORNER(4,2) = END(2)
C
C *** image corners to output space
C
      DO 20 I = 1,4
          X      = CORNER(I,1)
          Y      = CORNER(I,2)
          OUTX   = CX(1) + X * (CX(2)+X  * (CX(3)+X*CX(4))) +
     +             (CX(5)+X  * (CX(6)+X  * (CX(7)+X*CX(8))))*Y +
     +             (CX(9)+X  * (CX(10)+X * (CX(11)+X*CX(12))))*Y*Y +
     +             (CX(13)+X * (CX(14)+X * (CX(15)+X*CX(16))))*Y*Y*Y
          OUTY   = CY(1) + X * (CY(2)+X  * (CY(3)+X*CY(4))) +
     +             (CY(5)+X  * (CY(6)+X  * (CY(7)+X*CY(8))))*Y +
     +             (CY(9)+X  * (CY(10)+X * (CY(11)+X*CY(12))))*Y*Y +
     +             (CY(13)+X * (CY(14)+X * (CY(15)+X*CY(16))))*Y*Y*Y
          IF (OUTX.GT.OUTXMX) OUTXMX = OUTX
          IF (OUTX.LT.OUTXMN) OUTXMN = OUTX
          IF (OUTY.GT.OUTYMX) OUTYMX = OUTY
          IF (OUTY.LT.OUTYMN) OUTYMN = OUTY
   20 CONTINUE
C
C *** Deduce maximal dimensions of output frame
C
      OUTSTA(1) = OUTXMN - 5*STEP(1)                             ! safety margin
      OUTSTA(2) = OUTYMN - 5*STEP(2)                             ! safety margin
      OUTEND(1) = OUTXMX + 5*STEP(1)                             ! safety margin
      OUTEND(2) = OUTYMX + 5*STEP(1)
C
      DO 30 I = 1,2
         OUTPIX(I) = (OUTEND(I)-OUTSTA(I))/STEP(I) + 1
   30 CONTINUE
C
      RETURN
      END
