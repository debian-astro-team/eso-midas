C @(#)lire.for	19.1 (ES0-DMD) 02/25/03 13:30:46
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE LIRE(NL,NPL,NPL1,NPL2,NPL3,FMES,RMES)                          
C+++                                                                            
C.PURPOSE:  Write part of a frame into an array
C.AUTHOR:   ???
C.VERSION:  ?????? ??? created
C.VERSION:  890117 RHW documented
C.COMMENTS: none
C---
      IMPLICIT   NONE
      INTEGER    NL
      INTEGER    NPL
      INTEGER    NPL1
      INTEGER    NPL2
      INTEGER    NPL3
      REAL       FMES(1)
      REAL       RMES(1)                                                

      INTEGER    MADRID(1)
      INTEGER    NPD, NPF, K, I 
C
      COMMON     /VMR/MADRID                                                 
C
C ***
      NPD = NPL* (NL-1) + NPL1                                                  
      NPF = NPD + NPL2 - NPL1                                                   
      K   = 0                                                                   
C                                                                               
      DO 10 I = NPD,NPF,NPL3                                                    
         K       = K + 1                                                        
         RMES(K) = FMES(I)                                                      
   10 CONTINUE                                                                  
C                                                                               
      RETURN                                                                    
      END                                                                       
