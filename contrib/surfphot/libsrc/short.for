C @(#)short.for	19.1 (ES0-DMD) 02/25/03 13:30:48
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE SHORT(DR1,DR2,DR3,DR4,I,J,Z,A)                                 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++      
C.PURPOSE: Calculates system matrix elements in the case of full rectangle      
C -------------------------------------------------------------------------     
      IMPLICIT         NONE
      REAL             DR1, DR2, DR3, DR4
      INTEGER          I, J
      DOUBLE PRECISION Z(64,10),A(21,22)
C
      DOUBLE PRECISION D1,D2,D3,D4                            
C                                                                               
      D1       = (DR1+DR2+DR3+DR4)/4.0                                          
      D2       = (DR2+DR4-DR1-DR3)/4.0                                          
      D3       = (DR1+DR2-DR3-DR4)/4.0                                          
      D4       = (DR2+DR3-DR1-DR4)/4.0                                          
      A(1,1)   = A(1,1)   + 1.D0                                                
      A(1,4)   = A(1,4)   + Z(I,2)                                              
      A(1,6)   = A(1,6)   + Z(J,2)                                              
      A(1,11)  = A(1,11)  + Z(I,4)                                              
      A(1,13)  = A(1,13)  + Z(I,2)*Z(J,2)                                       
      A(1,15)  = A(1,15)  + Z(J,4)                                              
      A(2,16)  = A(2,16)  + Z(I,6)                                              
      A(2,18)  = A(2,18)  + Z(I,4)*Z(J,2)                                       
      A(2,20)  = A(2,20)  + Z(I,2)*Z(J,4)                                       
      A(3,21)  = A(3,21)  + Z(J,6)                                              
      A(7,16)  = A(7,16)  + Z(I,8)                                              
      A(7,18)  = A(7,18)  + Z(I,6)*Z(J,2)                                       
      A(7,20)  = A(7,20)  + Z(I,4)*Z(J,4)                                       
      A(8,21)   = A(8,21) + Z(I,2)*Z(J,6)                                       
      A(10,21) = A(10,21) + Z(J,8)                                              
      A(16,16) = A(16,16) + Z(I,10)                                             
      A(16,18) = A(16,18) + Z(I,8)*Z(J,2)                                       
      A(16,20) = A(16,20) + Z(I,6)*Z(J,4)                                       
      A(17,21) = A(17,21) + Z(I,4)*Z(J,6)                                       
      A(19,21) = A(19,21) + Z(I,2)*Z(J,8)                                       
      A(21,21) = A(21,21) + Z(J,10)                                             
      A(1,22)  = A(1,22)  + D1                                                  
      A(2,22)  = A(2,22)  + D2*Z(I,1)                                           
      A(3,22)  = A(3,22)  + D3*Z(J,1)                                           
      A(4,22)  = A(4,22)  + D1*Z(I,2)                                           
      A(5,22)  = A(5,22)  + D4*Z(I,1)*Z(J,1)                                    
      A(6,22)  = A(6,22)  + D1*Z(J,2)                                           
      A(7,22)  = A(7,22)  + D2*Z(I,3)                                           
      A(8,22)  = A(8,22)  + D3*Z(I,2)*Z(J,1)                                    
      A(9,22)  = A(9,22)  + D2*Z(I,1)*Z(J,2)                                    
      A(10,22) = A(10,22) + D3*Z(J,3)                                           
      A(11,22) = A(11,22) + D1*Z(I,4)                                           
      A(12,22) = A(12,22) + D4*Z(I,3)*Z(J,1)                                    
      A(13,22) = A(13,22) + D1*Z(I,2)*Z(J,2)                                    
      A(14,22) = A(14,22) + D4*Z(I,1)*Z(J,3)                                    
      A(15,22) = A(15,22) + D1*Z(J,4)                                           
      A(16,22) = A(16,22) + D2*Z(I,5)                                           
      A(17,22) = A(17,22) + D3*Z(I,4)*Z(J,1)                                    
      A(18,22) = A(18,22) + D2*Z(I,3)*Z(J,2)                                    
      A(19,22) = A(19,22) + D3*Z(I,2)*Z(J,3)                                    
      A(20,22) = A(20,22) + D2*Z(I,1)*Z(J,4)                                    
      A(21,22) = A(21,22) + D3*Z(J,5)                                           
                                                                                
      RETURN                                                                    
      END                                                                       
