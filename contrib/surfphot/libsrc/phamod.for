C @(#)phamod.for	19.1 (ES0-DMD) 02/25/03 13:30:47
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE PHAMOD(PNEW,POLD,NP)
C
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C.IDENTIFICATION
C   SUBROUTINE  PHAMOD              VER. 1.1                MAY  13, 1987
C   P.GROSBOL                       ESO/GARCHING
C
C.PURPOSE
C   ADDS A NUMBER OF PI/M TO PHASES IN ORDER TO REMOVE PHASE JUMPS
C   I.E. MINUMIZE THE DIFFERENCE BETWEEN PHASES IN PNEW AND POLD.
C   IT IS ASSUMED THAT THE PHASES COME FROM A FOURIER TRANSFORM
C   MODULO 2*PI AND THE IN RADIANS. THE TWO PHASE ARRAYS CONTAIN
C   THE PHASES OF M=0,1,2,3,4,5,.. IN INDEX N=1,2,3,4,5,6,..
C
C.INPUT/OUTPUT
C   THE PARAMETERS ARE
C       PNEW(NP) :  ARRAY WITH NEW PHASES PNEW(1):M=0, PNEW(2):M=1 ETC.
C       POLD(NP) :  ARRAY WITH OLD PHASES
C       NP       :  NO. OF PHASES IN ARRAY
C
C-------------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER  NP
      REAL     PNEW(NP)
      REAL     POLD(NP)
C
      INTEGER  I,IDF
      REAL     PI2
      REAL     DIF
      REAL     RFQ
C
C     SETUP CONSTANTS ETC.
C
      PI2 = 8.0 * ATAN(1.0)
C
C     CORRECT PHASE JUMPS BETWEEN OLD AND NEW
C
      DO 1000, I = 2, NP
         RFQ = 1.0 / FLOAT(I-1)
         DIF = RFQ * PI2
         PNEW(I) = RFQ * PNEW(I)
         IDF = INT( (PNEW(I)-POLD(I))/DIF + 10000.5) - 10000
         PNEW(I) = PNEW(I) - FLOAT(IDF) * DIF
 1000 CONTINUE
C
C     COPY PHASES FROM NEW TO OLD AND RETURN
C
      DO 3000, I = 1,NP
         POLD(I) = PNEW(I)
 3000 CONTINUE
      RETURN
      END
