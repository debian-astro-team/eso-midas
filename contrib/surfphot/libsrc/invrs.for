C @(#)invrs.for	19.1 (ES0-DMD) 02/25/03 13:30:46
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE INVRS(A,N)                                                     
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++              
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C.IDENTIFICATION: INVRS                                                         
C.PURPOSE:        Determines the inverse of the matrix A, of dimension N.       
C                 the inverse is returned in A                                  
C.ALGORITHM:      chinese to me - see clever books                              
C.INPUT/OUTPUT:   <-> A      input/output  matrix                               
C                  -> N      dimension of A                                     
C.AUTHOR:         P.Stetsan                                                     
C.LANGUAGE:       F77+ESOext                                                    
C.KEYWORDS:       mathematics                                                   
C.VERSION:        8212??  A.P. Stetson     Creation                             
C.VERSION:        871123  Rein H. Warmels  ESO-FORTRAN Conversion               
C.VERSION:        H.Waldthausen            VAX-implementation                   
C-----------------------------------------------------------------------        
      IMPLICIT   NONE
      INTEGER    N
      DOUBLE PRECISION A(5,5)                                                   
C
      INTEGER    I
      INTEGER    J
      INTEGER    K
C                                                                               
      I      = 0                                                                
   10 I      = I + 1                                                            
      IF (I.GT.N) GO TO 60                                                      
      A(I,I) = 1.0/A(I,I)                                                       
      J      = 0                                                                
   20 J      = J + 1                                                            
      IF (J.GT.N) GO TO 40                                                      
      IF (J.EQ.I) GO TO 20                                                      
      A(J,I) = -A(J,I)*A(I,I)                                                   
      K      = 0                                                                
   30 K      = K + 1                                                            
      IF (K.GT.N) GO TO 20                                                      
      IF (K.EQ.I) GO TO 30                                                      
      A(J,K) = A(J,K) + A(J,I)*A(I,K)                                           
      GO TO 30                                                                  
                                                                                
   40 K      = 0                                                                
   50 K      = K + 1                                                            
      IF (K.GT.N) GO TO 10                                                      
      IF (K.EQ.I) GO TO 50                                                      
      A(I,K) = A(I,K)*A(I,I)                                                    
      GO TO 50                                                                  
C                                                                               
   60 RETURN                                                                    
      END                                                                       
