$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.CONTRIB.SURFPHOT.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:24:54 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   apfftc.for
$ FORTRAN   lfit.for
$ FORTRAN   mnag.for
$ FORTRAN   phamod.for
$ FORTRAN   sfft.for
$ FORTRAN   histo.for
$ FORTRAN   coefy.for
$ FORTRAN   cpfram.for
$ FORTRAN   extell.for
$ FORTRAN   feet.for
$ FORTRAN   fllgrd.for
$ FORTRAN   flmtx.for
$ FORTRAN   intnul.for
$ FORTRAN   invrs.for
$ FORTRAN   lire.for
$ FORTRAN   long.for
$ FORTRAN   mean.for
$ FORTRAN   median.for
$ FORTRAN   mode.for
$ FORTRAN   normal.for
$ FORTRAN   outdim.for
$ FORTRAN   polin.for
$ FORTRAN   rctint.for
$ FORTRAN   rctmap.for
$ FORTRAN   row.for
$ FORTRAN   sbtrct.for
$ FORTRAN   serch.for
$ FORTRAN   sqztmp.for
$ FORTRAN   short.for
$ FORTRAN   syst.for
$ FORTRAN   truncy.for
$ FORTRAN   updat.for
$ FORTRAN   vfl.for
$ FORTRAN   rctscl.for
$ FORTRAN   simul.for
$ FORTRAN   sort1.for
$ LIB/REPLACE libsurfphot apfftc.obj,lfit.obj,mnag.obj,phamod.obj,sfft.obj,histo.obj,coefy.obj,cpfram.obj,extell.obj,feet.obj,fllgrd.obj,flmtx.obj
$ LIB/REPLACE libsurfphot intnul.obj,invrs.obj,lire.obj,long.obj,mean.obj,median.obj,mode.obj,normal.obj,outdim.obj,polin.obj,rctint.obj,rctmap.obj
$ LIB/REPLACE libsurfphot row.obj,sbtrct.obj,serch.obj,sqztmp.obj,short.obj,syst.obj,truncy.obj,updat.obj,vfl.obj,rctscl.obj,simul.obj,sort1.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
