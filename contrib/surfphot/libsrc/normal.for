C @(#)normal.for	19.1 (ES0-DMD) 02/25/03 13:30:47
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE NORMAL(A,NPIX,CRMD,BGRD)                                       
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++       
C.IDENTIFICATION: NORMAL                                                        
C.AUTHOR:         A. Kruszewski             ESO Garching                        
C.PURPOSE:        Divides data contained in array "A" by its mode "BGRD"        
C                 and stores results in the same array "A"                      
C.INPUT/OUTPUT:   input arguments                                               
C                 A       real  array       image frame                         
C                 NPIX    integer array     dimensions of array "A"             
C                 CRMD    real array        control parameters                  
C                 output arguments                                              
C                 A       real array        image frame divided by mode "BGRD"  
C                 BGRD        real*4        mode of array "A"                   
C.VERSION:        830531  AK   Creation                                         
C.VERSION:        ??????  KB                                                    
C.VERSION:        880716  RHW  Fortran conversion                               
C -----------------------------------------------------------------------      
      IMPLICIT NONE 
      REAL     A(1)
      INTEGER  NPIX(2)
      REAL     BGRD(1)
      REAL     CRMD(1)                                             
C
      INTEGER  J
      INTEGER  N1, N2, N
      REAL     FACT
      REAL     SIGMA
C                                                                               
C *** get size                                                                  
      N1     = NPIX(1)                                                          
      N2     = NPIX(2)                                                          
      N      = N1*N2                                                            
C                                                                               
C *** calculates mode "BGRD"                                                    
      CALL MODE(A,N,CRMD,BGRD,SIGMA)                                            
C                                                                               
C *** divide data from array "A" by mode "BGRD"                                 
C     and store results in the same array "A"                                   
      FACT   = 1./BGRD(1)                                                       
      DO 10 J = 1,N                                                             
         A(J)   = A(J)*FACT                                                     
   10 CONTINUE                                                                  
C                                                                               
      RETURN                                                                    
      END                                                                       
