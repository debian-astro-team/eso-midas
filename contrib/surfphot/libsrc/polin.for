C @(#)polin.for	19.1 (ES0-DMD) 02/25/03 13:30:47
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      REAL FUNCTION POLIN(X,CC,I)
C +++
C.PURPOSE: Evaluates polynomial at given y coord. and variable x coord.
C ---
      IMPLICIT         NONE
      DOUBLE PRECISION X
      DOUBLE PRECISION CC(6)
      INTEGER          I
C
      GO TO (50,40,30,20,10),I
C
   10 POLIN  = ((((CC(6)*X+CC(5))*X+CC(4))*X+CC(3))*X+CC(2))*X + CC(1)
      RETURN

   20 POLIN  = (((CC(5)*X+CC(4))*X+CC(3))*X+CC(2))*X + CC(1)
      RETURN

   30 POLIN  = ((CC(4)*X+CC(3))*X+CC(2))*X + CC(1)
      RETURN

   40 POLIN  = (CC(3)*X+CC(2))*X + CC(1)
      RETURN

   50 POLIN  = CC(2)*X + CC(1)
      RETURN

      END



