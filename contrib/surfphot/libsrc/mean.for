C @(#)mean.for	19.1 (ES0-DMD) 02/25/03 13:30:46
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.IDENTIFICATION
C  subroutine MEAN           version 2         810727
C  A. Kruszewski             ESO - Garching
C.PURPOSE
C  calculates mean value "AMEAN" and sigma "SIGMA" of random
C  variable basing on a sample presented in form of an array
C.INPUT/OUTPUT
C  input arguments
C  A           real*4 array        data array
C  N           integer*4           number of values in "A"
C  output arguments
C  AMEAN       real*4              mean of data in "A"
C  SIGMA       real*4              sigma of data in "A"
C-----------------------------------------------------------------------
      SUBROUTINE MEAN(A,N,AMEAN,SIGMA)
C
      IMPLICIT  NONE
      INTEGER   N
      REAL      A(N)
      REAL      AMEAN
      REAL      SIGMA
C
      INTEGER   K
      REAL      SUM
      REAL      SSUM
      REAL      X
C
C
      SUM=0.0
      SSUM=0.0
C
C *** Calculates mean and sigma.
C
      DO 10 K=1,N
         SUM=SUM+A(K)
         SSUM=SSUM+A(K)*A(K)
10    CONTINUE
      AMEAN=SUM/N
      SSUM=SSUM/N
      X=AMEAN
         SSUM=SSUM-X*X
C
C *** Checks if "SSUM" is positive.
C
      IF (SSUM.GT.0.00001) THEN
         SIGMA=SQRT(SSUM*(N/FLOAT(N-1)))
      ELSE
         SIGMA=0.0
      END IF
      RETURN
      END 
