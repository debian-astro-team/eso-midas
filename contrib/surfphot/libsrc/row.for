C @(#)row.for	19.1 (ES0-DMD) 02/25/03 13:30:48
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
C.IDENT:   ROW                                                                  
C.PURPOSE: Extracts a vector V from the array A ,starting from the element IF.  
C.NOTE   : Be careful with the out-of-bounds in A !                             
C          Must be IF+N-1 .LE. 'lastaddress'                                    
C          V has N elements.                                                    
C ------------------------------------------------------------------------------
      SUBROUTINE ROW(A,V,IF,N)                                                  
      IMPLICIT NONE
C
      REAL     A(1)
      REAL     V(1)                                                       
      INTEGER  IF
      INTEGER  N
C
      INTEGER  I
C                                                                               
      DO 10 I = 1,N                                                             
         V(I)   = A(I+IF-1)                                                     
   10 CONTINUE                                                                  
C                                                                               
      RETURN                                                                    
      END                                                                       
