C @(#)vfl.for	19.1 (ES0-DMD) 02/25/03 13:30:49
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE VFL(VAL,STRING)                                                
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
C.PURPOSE:  Internal file formatted write of VAL into STRING                    
C           VAL is checked for over- and underflow before writing               
C ------------------------------------------------------------------------------
      IMPLICIT      NONE
C
      REAL          VAL
      CHARACTER*(*) STRING                                                      
C                                                                               
C *** begin code                                                                
C                                                                               
      IF ((VAL.GE..001.AND.VAL.LE.9999.999) .OR.                                
     +   (VAL.LE.-.001.AND.VAL.GE.-999.999)) THEN                               
         WRITE (STRING,'(F8.3)') VAL                                            
      ELSE                                                                      
         WRITE (STRING,'('' ...... '')')                                        
      END IF                                                                    
C                                                                               
      RETURN                                                                    
      END                                                                       
