\se 
SECTION./SURFPHOT
\es\co 
INTEGRATE/ELLIPS                                        22-MAY-2007 RHW,KB
\oc\su
INTEGRATE/ELLIPS frame [ellips_par] [flag]
	integrate pixel intensities within ellipse in 2-dim. image
\us\pu
Purpose:      Integrate pixel intensities within ellipse in 2-D image
\up\sy                                                                         
Syntax:       INTEGRATE/ELLIPS frame [ellips_param] [flag]
\ys\pa 
              frame =      input image frame 
\ap\pa  
              ellips_par = ellipse parameters: center X, center Y, semi-major 
                     diameter, semi-minor diameter, position angle;
                     the pos. angle is in degrees, 0 on x-axis, incr towards 
		     the y-axis,
		     all other values are given in world coordinates (NOT as
		     frame pixels!)
                     Default are the values used in the previous run.
\ap\pa
              flag = 0 or 1. With a 1 the ellipse parameters are obtained
                     from the descriptor ELLPAR of the input frame;
                     default is 0.
\ap\no
Note:         The program does a simultaneous search for points within a 
              slightly larger ellipse, determines the differential increase 
              in pixel area, ellipse area and flux. A linear interpolation 
              is applied to get flux from pixels covering same total area 
              (approx same region) as the nominal ellipse.
 
              The result is stored in the descriptor ELLPAR(1) to ELLPAR(6).
              The flux (integral over all pixel values is stored in the 
              descriptor ELLPAR(6)
 
	      The algorithm assumes a linear world coord system.
\on\exs 
Examples:                                                                      
\ex
              INTEGRATE/ELLIPS myframe 250.,312.,50.,30.,45.
                Assuming, that image `myframe.bdf' has start values 100.,200.
                and stepsize 2.,2., then an ellipse around the center 250.,312.
                with semi-major axis of 25 pixels and semi-minor axis of 
		15 pixels, and an angle of 45 degrees versus the x-axis
		is used. 
\xe \sxe 
