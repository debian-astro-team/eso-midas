% @(#)fit_ell2.hlq	19.1 (ESO-IPG) 02/25/03 13:30:32
\se
SECTION./ELL2
\es\co
FIT/ELL2                                                       17-Feb-1992 RHW
\oc\su
FIT/ELL2 inframe pol_opt iso_tol iso_levels [center[ [radius] [sky_level]
	fit an ellips with respect to predefined center
\us\pu
Purpose:     An image is searched for intensity points within a certain
             intensity range and within a radius to the center.

\up\sy
Syntax:      FIT/ELL2 inframe pol_opt iso_tol iso_levels [center[ [radius]
                      [sky_level]
\ys\pa
             inframe       = input frame
\ap\pa
             polar_opt     = 0, if polar frames should be made (first run)
                             1, polar frames already exist (repeated runs)
\ap\pa
             iso_tol       = isophotal tolerance in promille of iso_levels
\ap\pa
             iso_levels    = isophotal levels you want to fit (max = 10)
                             the format can be low:high:incr or lev1,lev2,..
                             or a combination of the two separated by a comma
\ap\pa
             center        = x,y-coordinate of center of object; default is the
                             value used in the previous run
\ap\pa
             radius        = radius around center to be surveyed; default is
                             value used in the previous run
\ap\pa
             sky_level     = value to be added to the iso_levels before a fit
                             is made to the data points; default is the value
                             in the previous run
\ap\no
Note:        The input image should have been cleaned from disturbing objects
             (stars) in the region of interest. The center of the object can
             be calculated using the command CENTER/GAUSS.\\
             The ellipses are fitted with respect to the predefined center.
             At high isophotal levels the fit will be affected by the seeing
             and it will become round, the position angle being undefined.
             At low isophotal levels (< 5 - 2 %) the affect of noise is that
             of a round object. With these natural restrictions the code
             allows to trace isophotal twisting.\\
             Points that were found and lay exactly on the ellips can not
             be discriminated from the ellips.\\
             The parameters of the ellips are communicated to the user.

             The command creates two polar coordinate frames containing the
             polar coordinates radii and angles of every pixel of the input
             frame. The polar coordinates of the points found are used to fit
             an ellips with a predefined center. Every rerun of the program
             (with polar_opt=0) will delete the preexisting scratch files of
             the polar coordinates.

             The command also produces an image called ELLIPS with the points
             used for the fit and with the actual ellips determined. This image
             can be used for display. Different isophotal levels will displayed
             in different colours. In the examples below the use of this file
             together with the input data file is demonstrated.

\on\exs
Examples:
\ex
             FIT/ELL2 INFRAME 0 10 10:50:10 -356.,-456 1000 125
             and run again bad solution for the 50_isophote changing the
             tolerance
\xe\ex
             FIT/ELL2 INFRAME 1 30 50
             or run again changing the radius of the search area
\xe\ex
             FIT/ELL2 INFRAME 1 10 50 300
             etc. In this way one can interactively steer the fit
             towards optimal solutions.
\xe\ex
{\tt         FIT/ELL2 INFRAME 0 10 10:50:10,70,90 -356.,-456. 1000 0\\
             LOAD/IMAGE INFRAME 0\\
             LOAD/IMAGE ELLIPS OVERLAY\\
             SET/OVERLAY
}
\xe\ex
{\tt         FIT/ELL2 INFRAME 1 10 10:50:10,70,90 -356.,-456. 1000 0\\
             LOAD/IMAGE INFRAME 0\\
             LOAD/IMAGE ELLIPS OVERLAY\\
             BLINK 0 OVERLAY\\
}
\xe \sxe
