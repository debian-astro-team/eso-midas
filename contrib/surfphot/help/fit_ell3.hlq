% @(#)fit_ell3.hlq	19.1 (ESO-IPG) 02/25/03 13:30:32
\se
SECTION./ELL3
\es\co
FIT/ELL3                                                     10-Dec-1990 MS+RHW
\oc\su
FIT/ELL3 inframe outframe [step] [x,y] [low,high] [min,max] [opt]
	fit ellipses to the isophotes of an object in a 2-dim. frame
\us\pu
Purpose:     
             fit ellipses to the isophotes of an object in a 2-dim. frame
\up\sy
Syntax:      
             FIT/ELL3 inframe outframe [step] [x,y] [low,high] [min,max] [opt]
\ys\pa
             inframe  = input frame
\ap\pa
             outframe = output frame containing the model reconstructed 
                        from the fit
\ap\pa
             step =     spacing of the isophot levels. The levels C(n) are 
                        logarithmically equidistant: 
                        C(n+1)=C(n).10**(-step). Default is 0.01.
\ap\pa
             x,y =      estimated x and y centre of the innermost ellipse.
                        If not given, the command will read elements 5 and
                        6 of the real descriptor CENTER.
\ap\pa
             low,high = lowest and highest isophote level. Default display
                        cuts.
\ap\pa
             min,max =  min, max position angle (degrees, from x-axis counter
                        clockwise) of the sector to be excluded from the fit.
                        Default is 0,0.
\ap\pa
             opt =      option to enable (Y) or not enable cleaning (N). See
                        below; default is `N'.
\ap\no
Note:        
             The last two parameters min,max and opt are aids in case that the
             isophotes are disturbed: a single major disturbation can be 
             excluded by excluding the sector which contains it; multiple minor 
             perturbations are removed by comparing the values of pixels at 
             opposite position angles and selecting the minimum (opt = Y). 
\\
             An estimate of the x,y center of the innermost isophote may be 
             obtained with the CENTER/GAUSS command which optionally also
             produces the CENTER descriptor.
\\
             In addition a table `outframe'.TBL is produced which contains 
             the fitted parameters: the isophote levels in column :Z; the 
             major and minor axes in columns :A and :B, respectively; the 
             position angle of the major axis in :PA; the center of the 
             ellipses in :X and :Y; and the noise in :NOISE. 
\\
             The algorithm is based on the formulas of R. Bender and 
             C. Moellenhof published in Astron. and Astrophys. 177,71 (1987).
\on\see
See also:    
             COMPUTE/TABLE, LOAD/IMAGE
\ees\exs
Examples:
\ex	
             FIT/ELL3 m87 model 0.05 ? ? 20.,30. Y
             This will fit ellipses to the isophotes of an object located at 
             CENTER coordinates in frame M87 and write result into frame MODEL.
             The isophote levels are logarithmically spaced with 0.05. The 
             lowest and highest levels are defined by display cuts. An angle 
             section between 20 and 30 degree will be excluded from fit. The
             cleaning option is enabled. 
\xe \sxe
