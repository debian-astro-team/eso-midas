% @(#)fit_ell1.hlq	19.1 (ESO-IPG) 02/25/03 13:30:32 
\se
SECTION./ELL1
\es\co
FIT/ELL1                                                        14-Mar-1991 RHW
\oc\su
FIT/ELL1 inframe outframe l_iso,h_iso x_cen,y_cen max_rad
	fit an ellips with respect to predefined center
\us\pu
Purpose:     An image is searched for intensity points within a certain
             intensity range and within a radius to the center.
\up\sy
Syntax:      FIT/ELL1 inframe outframe l_iso,h_iso x_cen,y_cen max_rad
\ys\pa
             inframe       = input frame
\ap\pa
             outframe      = output frame with isophotal points and fitted
                             ellipse
\ap\pa
             l_iso,h_iso   = low and high isophotal levels
\ap\pa
             x_cen,y_cen   = x and y centre in world coordinates
\ap\pa
             max_rad       = radius around center to be surveyed; default is
                             value used in the previous run
\ap\no
Note:        The ellipse centre (x and y), the semi-major diameter, the 
             semi-minor diameter and the position angle (degrees, o on 
             positive x axis increasing to positive y axis) will be saved in
             the descriptor ELLPAR attached to the output frame.
\\
             The maximum survey radius should be slightly larger than the 
             semi-major diameter of the fitted ellipse - a too large survey
             diameter of the fitted ellipse includes to many spurious pixels
             at large distances, producing a too large (and round) fitted 
             ellipse.
\on\see
See also:    LOAD/IMAGE
\ees\exs
Examples:
\ex
             FIT/ELL1 INFRAME FITFRAM 1,5 98.5,101.1 20
\xe\sxe
