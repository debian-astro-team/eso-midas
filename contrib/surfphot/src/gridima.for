C @(#)gridima.for	19.1 (ESO-DMD) 02/25/03 13:31:22
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C.IDENTIFICATION: GRIDIMA
C.LANGUAGE:       F77+ESOext
C.AUTHOR:         D.Baade
C.KEYWORDS:       Ingeneering frames
C.PURPOSE:        Produce 2-D frame with rectangular grid subjected to
C                 "S-type" distortion
C.ALGORITHM:      trivial
C.PECULARITIES:   The size of the frame is fixed to 510 by 510 pixels as is the
C                 separation of the lines of the undistorted grid to 60 pixels
C.INPUT/OUTPUT:   The following key words are used:
C                 ALPHA/R/1/1  angle ALPHA_0 of the distortion in the following 
C                 parametrization:
C                                 ALPHA = ALPHA0 * R**2
C                 with R being the distance from the point about which
C                 the rotation takes place. ALPHA_0 to be given in degrees/pixel.
C.VERSION:        850322 DB  Creation
C.VERSION:        871123 RHW ESO-FORTRAN Conversion
C
C 021031	last modif
C 
C ---------------------------------------------------------------------
      PROGRAM GRDIMA

      IMPLICIT     NONE
C
      INTEGER      MADRID,IMF
      INTEGER      IAV,ISTAT
      INTEGER      NPIX(2)
      INTEGER      NAXIS
      INTEGER      KUN,KNUL
      INTEGER*8    IPNTR
C
      CHARACTER    IDENT*72,CUNIT*48,IMAG*64
C
      REAL         ALPHA0
      REAL         LHCUTS(4)
      DOUBLE PRECISION START(2),STEP(2)
C
      INCLUDE      'MID_INCLUDE:ST_DEF.INC/NOLIST'
      COMMON       /VMR/MADRID(1)
      INCLUDE      'MID_INCLUDE:ST_DAT.INC/NOLIST'
C
      DATA         START/1.,1./
      DATA         STEP/1.,1./
      DATA         NAXIS/2/
      DATA         NPIX/510,510/
      DATA         LHCUTS/0.,100.,0.,100./
      DATA         IMAG/'GRID'/
      DATA         IDENT/'GRID'/
      DATA         CUNIT/'FLUX'/
C
C *** begin code
      CALL STSPRO('GRIDIMA')                        ! get into midas environment
      CALL STKRDR('ALPHA',1,1,IAV,ALPHA0,KUN,KNUL,ISTAT)  ! get distortion angle
      ALPHA0 = ALPHA0*3.14159/180.                        !   convert to radians
      CALL STIPUT(IMAG,D_R4_FORMAT,F_O_MODE,F_IMA_TYPE,NAXIS,
     2            NPIX,START,STEP,IDENT,CUNIT,IPNTR,IMF,ISTAT)
C
C *** we already know (by definition) the cuts. let's set them now:
      CALL FLLGRD(MADRID(IPNTR),ALPHA0)            ! call fillgrid to do the job
      CALL STDWRR(IMF,'LHCUTS',LHCUTS,1,4,KUN,ISTAT)
C
      CALL STSEPI                            ! release files and update keywords
      END
