! @(#)comcc.prg	19.1 (ES0-DMD) 02/25/03 13:31:07
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: comcc.prg
!.PURPOSE:        Compute values on characteristic curve
!.USE:            COMPUTE/CC P1 P2
! 		  P1 name of input calibration table with coefficients of 
!		     the characteristic curve according to the Llebaria 
!		     formula in 1st row
! 		  P2 name of output 1-D image file with characteristic curve, 
!		     D = D(log I)
!.NOTE:           Derived from old routine CTAB
!-----------------------------------------------------------------------
DEFI/PARAM    P1      A   T
DEFI/PARAM    P2      A   I
WRITE/KEYW   IN_A   'P1'
WRITE/KEYW   OUT_A  'P2'
RUN CON_EXE:COMCC
