! @(#)subsky.prg	19.1 (ES0-DMD) 02/25/03 13:31:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure SUBSKY to implement SUBTRACT/SKY
! D. Baade	881017
! KB		890523
! D. Baade      900220      (added size of neighbourhood box and inclusion 
!                            flag for central pixel as parameters)
! 	
! use via SUBTRACT/SKY inframe outframe dimx,dimy incl_flag 
! 	
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter input frame: "
DEFINE/PARAM P2 ? IMA "Enter result frame: "
DEFINE/PARAM P3 1,1 N "Enter dimensions of neighbourhood area: "
DEFINE/PARAM P4 N ? "Enter flag for inclusion of central pixel: "
!
WRITE/KEYW HISTORY "'MID$CMND(1:6)'/'MID$CMND(11:14)' 'P1' 'P2' 'P3' 'P4'"
!
WRITE/KEYW IN_A 'P1'
WRITE/KEYW OUT_A 'P2'
WRITE/KEYW INPUTI/I/1/2 'P3'
WRITE/KEYW INPUTC/C/1/1 'P4'
!
RUN CON_EXE:SUBSKY

