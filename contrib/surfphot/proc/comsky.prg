! @(#)comsky.prg	19.1 (ES0-DMD) 02/25/03 13:31:07
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: comsky.prg
!.PURPOSE:        compute sky background, restitute image (option)
!.USE:            COMPUTE/SKY P1 P2 P3 P4 P5, where
!                 P1 = first input frame
!	          P2 = second input frame (if P4=3 then for any P2 later P2=P1)
!	          P3 = calibration table
!	          P4 = method 0,1(new), 2(old), 3(single frame) = default
!                 P5 = sky factor, default=1.4
! NOTE:           Descriptor O_TIME/D must exist; value<1984. 
!                 For old PDS config. The value of O_TIME is vital to the
!		  successful execution of the procedure!
!		  WARNING: after 13.1.1984 the PDS scans generally do NOT 
!		  contain the actual O_TIME (in most cases still value<1984 !)
!                 Normally, P1 is updated with a number of new descriptors.
!---------------------------------------------------------------------------
DEFINE/PARAM  P1 A   I
DEFINE/PARAM  P2 B   I
DEFINE/PARAM  P3 C   T
DEFINE/PARAM  P4 3   N
DEFINE/PARAM  P5 1.4 N
!
WRITE/KEYW IN_A/C/1/40   'P1'
WRITE/KEYW IN_B/C/1/40   'P2'
WRITE/KEYW INPUTC/C/1/60 'P3'
WRITE/KEYW INPUTI/I/1/2  'P4'
WRITE/KEYW INPUTR/R/1/1  'P5'
!
RUN CON_EXE:COMSKY
