! @(#)dtom.prg	19.1 (ES0-DMD) 02/25/03 13:31:08
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: DTOM.PRG
!.PURPOSE:        Find characteristic curve of a photographic plate
!                 from sensitometer marks and/or galaxy aperture photometry
!.USE:            CONVERT/DTOM with input parameters prompted 
!.NOTE            This routine should follow the application of COMSKY
!                 (COMPUTE/SKY ) - except when only sensitometer marks are
!		  needed.
!                 The aperture photometry for ESO galaxies is in the
!                 table file BVRLA.TBL residing in the directory
!		  PMID$DISK:[MIDAS.CALIB.DATA.MORPH] with the format
!                 col     Contents (the argument is log Aperture (arcsec))
!                  1      ESO.OBJ
!                  2-7    V  : 0-3rd order coeffs + 1st.last log A limits
!                  8-13   U-B: 0-3rd order coeffs + 1st.last log A limits
!                 14-19   B-V: 0-3rd order coeffs + 1st.last log A limits
!                 20-25   V-R: 0-3rd order coeffs + 1st.last log A limits
!                 26-31   V-I: 0-3rd order coeffs + 1st.last log A limits
!
!		  Default (promted) names should be completely overwritten
!		  (add blanks if nec) in order to avoid trailing characters
!------------------------------------------------------------------
IF INPUTI(7) .NE. -999 THEN
   WRITE/KEYW    IN_A/C/1/60 " " ALL
   WRITE/KEYW    IN_A/C/1/1 I
   WRITE/KEYW    INPUTC/C/1/60 " " ALL
   WRITE/KEYW    INPUTC/C/1/1 B
   WRITE/KEYW    SCALE/R/1/1 67.52
   WRITE/KEYW    INPUTI/I/1/7 0,0,0,0,0,0,-999
   WRITE/KEYW    IN_B/C/1/60 " " ALL
   WRITE/KEYW    IN_B/C/1/4 CBLU
   WRITE/KEYW    NAPE/I/1/1 0
   WRITE/KEYW    TABLEA/C/1/8 "BVRLA   "
   WRITE/KEYW    TABLEC/C/1/8 "BCAL    "
   WRITE/KEYW    ESOFLD/I/1/1 999
   WRITE/KEYW    SKYMAG/R/1/1 23.
   WRITE/KEYW    INPUTR/R/1/4 0.,5.12,1.3,2.
   WRITE/KEYW    ESOOBJ/R/1/1 999.
   WRITE/KEYW    WEIGHT/R/1/9 0.15,0.7,1.,1.,1.,1.,1.,1.,1.
ENDIF
! 
INQU IN_A    name of image frame (='IN_A'): 
INQU INPUTC  colour U,B,V or R (='INPUTC'): 
INQU SCALE   plate scale in arcs/mm (='SCALE'): 
WRITE/OUT    'INPUTI(1)','INPUTI(2)','INPUTI(3)','INPUTI(4)'
INQU INPUTI  1/0 for fix nofix calibr parameters:  
INQU IN_B    name of table with logI, D values (='IN_B'): 
INQU NAPE    no of apertures (='NAPE', 0=only sky&spots): 
INQU TABLEA  name of table with UBVR coeffs (='TABLEA'): 
INQU TABLEC  name of table with calibr parameters (='TABLEC'): 
INQU ESOFLD  ESO field no (='ESOFLD'): 
INQU SKYMAG  sky mag per square arcsec (='SKYMAG'): 
IF NAPE .NE. 0 THEN 
   INQU ESOOBJ ESO.OBJ (='ESOOBJ'): 
   READ/KEYW WEIGHT
   INQU WEIGHT relative weights of first,last,betw ap.rings : 
ENDIF
WR/OUT 'INPUTR(1)','INPUTR(2)','INPUTR(3)','INPUTR(4)'
INQU INPUTR calibr coeffs DF,DS,RG,DI: 
! 
RUN CON_EXE:DTOM
