! @(#)sgfpi.prg	19.1 (ES0-DMD) 02/25/03 13:31:09
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.PURPOSE: Fits position angle and inclination to 2-nd and 4-th harmonic
!.USE:     FIT/PI image xc,yc,pa,i  ri,ro,dr  region
!          where the parameters are:
!          INPUTR(1:4): x center, y center, position angle, inclination
!          INPUTR(5:7): inner radius, outer radius, radial step
!          INPUTR(8:9): pa region, inclination region
!.VERSION: 880209 PJG creation
!.VERSION: 910307 RHW included in ESO-MIDAS
!----------------------------------------------------------
DEFINE/PARAM P1 ? ? "Enter name of input image :"
DEFINE/PARAM P2 ? ? "Enter xc,yc,pa,i          :"
DEFINE/PARAM P3 ? ? "Enter ri,ro,dr            :"
DEFINE/PARAM P4 ? ? "Enter region in p.a. and incl.:"
!
WRITE/KEYW  IN_A/C/1/40  'P1'
WRITE/KEYW  INPUTR/R/1/4 'P2'
WRITE/KEYW  INPUTR/R/5/3 'P3'
WRITE/KEYW  INPUTR/R/8/2 'P4'
!
RUN CON_EXE:SGFPI
