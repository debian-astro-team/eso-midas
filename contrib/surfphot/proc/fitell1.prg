! @(#)fitell1.prg	19.1 (ES0-DMD) 02/25/03 13:31:08
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: FITELL
!.PURPOSE:	  Fit ellipse to 2-D image isophote
!.USE		  FIT/ELLIPS P1 P2 P3 P4 P5
!		  P1 = inframe (recommend smoothing by FILTER/MEDIAN )
!		  P2 = outframe with isophotal points and fitted ellipse
!		  P3 = isophotal lower, upper levels
!		  P4 = center X,Y in world coordinates
!		  P5 = max radius around center to be surveyed
!.NOTE:           Ellipse center X, center Y, semi-major diameter, semi-minor 
!		  diameter and position angle (degrees, 0 on positive X-axis, 
!		  increasing towards pos Y-axis) will be saved in descriptor 
!		  ELLPAR attached to outframe.
!
!		  Advice: max survey radius should be slightly larger than
!		  the semi-major diameter of the fitted ellipse - a too
!		  large survey radius includes too many spurious pixels
!		  at large distances, producing a too large (and round) 
!		  fitted ellipse.
!--------------------------------------------------------------------------
DEFINE/PARAM P1 ?  IMA               "Enter input image"
DEFINE/PARAM P2 ?  IMA               "Enter output image"
DEFINE/PARAM P3 'MORPH(41:60)' N     "Enter lower, upper isophotal levels:"
DEFINE/PARAM P4 'MORPH(1:20)'  N     "Enter center x,y:"
DEFINE/PARAM P5 'MORPH(21:40)' N     "Enter radius around center:"
!
WRITE/KEYW IN_A  	'P1'       !write inframe into IN_A
WRITE/KEYW OUT_A 	'P2'       !write outframe into OUT_A
COPY/II   'P1' 'P2'                !copy inframe to outframe
!
WRITE/KEYW INPUTR/R/4/2  'P3'       !write lower, upper isophotal levels
WRITE/KEYW MORPH/C/41/20 'P3'
!
WRITE/KEYW INPUTR/R/1/2  'P4'       !write the centre
WRITE/KEYW MORPH/C/1/20  'P4'
!
WRITE/KEYW INPUTR/R/3/1  'P5'       !write the limiting radius
WRITE/KEYW MORPH/C/21/20 'P5'
!
RUN CON_EXE:FITELL1
