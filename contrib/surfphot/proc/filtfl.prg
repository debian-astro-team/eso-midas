! @(#)filtfl.prg	19.1 (ES0-DMD) 02/25/03 13:31:08
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT:   filtfl.prg
!.PURPOSE: implements FILTER/FILL
!.AUTHOR:  D. Baade
!.USE:     FILTER/FILL inframe outframe radx,rady threshold
!.VERSION: 881010 DB Creation
!.VERSION: 890530 KB ???
!---------------------------------------------------------------------------
DEFINE/PARAM P1 ? IMA "Enter input frame:"
DEFINE/PARAM P2 ? IMA "Enter result frame:"
DEFINE/PARAM P3 1,1 N "Enter radx,rady:"
DEFINE/PARAM P4 0 N "Enter threshold:"
!
WRITE/KEYW HISTORY "'MID$CMND(1:6)'/'MID$CMND(11:14)' 'P1' 'P2' 'P3' 'P4'"
!
WRITE/KEYW IN_A 'P1'
WRITE/KEYW OUT_A 'P2'
WRITE/KEYW INPUTI/I/1/2 'P3'
WRITE/KEYW INPUTR/R/1/1 'P4'
!
DISPLAY/LONG
RUN CON_EXE:FILTFL
