!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: intell.prg
!.PURPOSE:	  Integrate pixel intensities within ellipse in 2-D image
!.USE:            INTEGR/ELLIPS P1 P2 P3
!		  P1 = inframe 
!		  P2 = ellipse parameters: center X, center Y, 
!		       semi-major diameter, semi-minor diameter, 
!		       position angle (degrees, 0 on X-axis, incr tow Y-axis)
!		  P3 = flag: 1 = ellipse parameters read from
!		       descriptor ELLPAR according to P2
!.NOTE:           Descriptor ELLPAR updated with flux in 6th element
! 080320	last modif
!--------------------------------------------------------------------------
define/param p1 ?  IMA               "Enter input image"
define/param p2 {morph(1:50)}  N     "Ellipse center X,Y, semidiams, posangle:"
define/param p3 0 N                  "Flag (1=ellipse params in descr ELLPAR):"
!
write/keyw in_a  	{p1}       !write inframe into IN_A
!
write/keyw inputr/r/1/5  {p2}       !write ellipse parameters
write/keyw morph/c/1/50  {p2}
!
write/keyw inputi/i/1/1  {p3}       !write flag
!
run CON_EXE:INTELL
