! @(#)sgepi.prg	19.1 (ES0-DMD) 02/25/03 13:31:09
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.PURPOSE:  Estimate of position and inclination angle of galaxies.
!.USE:      FIND/PI image  xc,yc,pa,i  ri,ro,dr
!           where the parameters are:
!.VERSION:  Sep 8, 1988 P.Grosbol
!-------------------------------------------------------------
DEFINE/PARAM P1 ? ? "Enter input image : "
DEFINE/PARAM P2 ? ? "Enter xc,yc       : "
DEFINE/PARAM P3 ? ? "Enter ri,ro,dr    : "
!
WRITE/KEYW  IN_A/C/1/40  'P1'
WRITE/KEYW  INPUTR/R/4/2 'P2'
WRITE/KEYW  INPUTR/R/1/3 'P3'
!
RUN CON_EXE:SGEPI
