! @(#)grid.prg	19.1 (ES0-DMD) 02/25/03 13:31:08
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT:   grid.prg
!.PURPOSE: Create image with distorted but otherwise rectangular and
!          evenly spaced grid plus a table with the points defining the
!          grid. The output is suitable to test the performance of
!          MIDAS command RECTIFY and other operations requiring rebinning
!          of 2-D images.
!.AUTHOR:  D. Baade  ST-ECF
!.USE:     COMPUTE/GRID angle
!          angle = distortion angle of the grid (in degrees),
!                  defaulted to 1.E-5, which  gives a slight exaggeration
!                  of the distortion typically found in PCD and ST-FOC image
!.NOTE:    Size of both image (510 x 510 pixels) and table (81 rows) are
!          fixed. For most tests it may be advisable to EXTRACT a
!          subframe of the image. In order to simulate point spread
!          functions with a larger FWHM, the image may be FILTERed.
!.EXAMPLE: COMPUTE/GRID 0.00002
!.VERSION: 850820 DB creation
!.VERSION: 860915 KB ???
!--------------------------------------------------------------------------
DEFINE/PARAM P1 0.00001 NUM	!default distortion angle to 1.E-5 degrees
DEFINE/LOCAL ALPHA/R/1/1 {P1}
!
! create image with distorted grid and display it
! 
RUN CON_EXE:GRIDIMA	
LOAD/IMAGE GRID
!
! create also table with distorted grid of reseau marks
RUN CON_EXE:GRIDTAB
!
! finally plot reseau marks on graphics terminal
! 
SET/GRAPH XAXIS=-100,700,200,100 YAXIS=-100,700,200,100 FRAME=SQUARE
PLOT/TABLE GRID #3 #4
