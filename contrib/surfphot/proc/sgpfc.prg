! @(#)sgpfc.prg	19.1 (ES0-DMD) 02/25/03 13:31:10
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.PURPOSE: Fourier coefficients of azimuthal profiles of spiral galaxis
!.USE:     COMPUTE/FCOEFF image  xc,yc,pa,i  ri,ro,dr  table
!          where the parameters are:
!.VERSION: Sep 8, 1988 P.Grosbol
!----------------------------------------------------------
DEFINE/PARAM P1 ? ? "Enter name of input image :"
DEFINE/PARAM P2 ? ? "Enter xc,yc,pa,i          :"
DEFINE/PARAM P3 ? ? "Enter ri,ro,dr            :"
DEFINE/PARAM P4 ? ? "Enter name of output table:"
!
WRITE/KEYW  IN_A/C/1/40  'P1'
WRITE/KEYW  INPUTR/R/1/4 'P2'
WRITE/KEYW  INPUTR/R/5/3 'P3'
WRITE/KEYW  OUT_A/C/1/40 'P4'
!
RUN CON_EXE:SGPFC
