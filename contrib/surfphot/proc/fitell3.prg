! @(#)fitell3.prg	19.1 (ES0-DMD) 02/25/03 13:31:08
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT:    fitell3.prg
!.PURPOSE:  fit ellipses to isophotes of an object
!.AUTHOR:   Martin Schloetelburg       11-December-1990
!.SEE ALSO: the procedure is based on the analytical expressions of
!           R. Bender & C. Moellenhof published in
!           Astron. Astrophys. 177,71 (1987)
!.USE:      @c ELLIPSES in out [x,y] [step] [low,high] [min,max] [opt]
!.OUTPUT:   In addition to the model frame 'P2'.BDF a table 'P2'.TBL
!           is produced containing the isophote level (:Z), the major
!           (:A) and minor axes (:B), the position angle (:PHI), the
!           center coordinates (:X :Y) and the noise (:SIG).
!.NOTE:     The isophotes levels are defined as
!           level(n) = (high-low)*10.**(n*step)  n < 500
!           The min and max are either given (as P4) or taken
!           from the display cuts (default).
!           With the last parameter (P7) a cleaning process can
!           be enabled, which compares the values of pixels opposite
!           to each other with respect to the isophote center and
!           takes the minimum for fitting. This throughs out 
!           minor disturbations effectively.
!.VERSION:  910621 RHW Implemented in MIDAS
!---------------------------------------------------------------------------
DEFINE/PARAM P1 ? ? "input  frame "
DEFINE/PARAM P2 ? ? "output frame "
DEFINE/PARAM P3 .01 ? "logarithmic spacing of isophotes "
DEFINE/PARAM P4 C ? "estimate of x,y center of innermost isophote "
DEFINE/PARAM P5 0.,0. ? "lowest,highest isophote level "
DEFINE/PARAM P6 0.,0. ? "angle sector to be excluded from fit "
DEFINE/PARAM P7 N ? "cleaning option [Y/N] ? "
!
WRITE/KEYW INPUTR/R/1/1 'P3'
WRITE/KEYW INPUTR/R/2/2 'P5'
WRITE/KEYW INPUTR/R/4/2 'P6'
!
IF P4(1:1) .EQ. "C" THEN
   COMPUTE/KEYW INPUTI(1) = M$EXISTD(P1,"CENTER")
   IF INPUTI(1) .EQ. 1 THEN
      COMPUTE/KEYW INPUTR(6) = {{P1},CENTER(5)}
      COMPUTE/KEYW INPUTR(7) = {{P1},CENTER(6)}
   ELSE
     WRITE/OUT "*** INFO: CENTER descriptor does not exist !"
     RETURN
   ENDIF
ELSE
   WRITE/KEYW INPUTR/R/6/2 'P4'
ENDIF
!
RUN CON_EXE:FITELL3
