! @(#)normalize.prg	19.1 (ES0-DMD) 02/25/03 13:31:09
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT:   normalize.prg
!.PURPOSE: to implement NORMALIZE/IMAGE
!.AUTHOR:  860917 KB 
!.USE:     NORMALIZE/IMAGE in_frame out_frame trunc_vals control_vals
!--------------------------------------------------------------------------
DEFINE/PARAM P1 ? IMA "Enter input frame:"
DEFINE/PARAM P2 ? IMA "Enter output frame:"
DEFINE/PARAM P3 0.,0. N
DEFINE/PARAM P4 3.,5.,2.,0. N
!
WRITE/KEYW IN_A 'P1'
WRITE/KEYW OUT_A 'P2'
WRITE/KEYW INPUTR 'P3'
WRITE/KEYW INPUTR/R/3/4 'P4'
DISPLAY/LONG
!
RUN CON_EXE:NORMALIZE
