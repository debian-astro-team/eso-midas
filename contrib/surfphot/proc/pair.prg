! @(#)pair.prg	19.1 (ES0-DMD) 02/25/03 13:31:09
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: PAIR
!.PURPOSE:	  Match two point sets with coordinates in table files
!.USE:		  FIND/PAIR P1 P2 P3 P4 P5 P6
!		  P1 = name of first table 
!		  P2 = name of second table 
!		  P3 = name of output table
!		  P4 = column nos of coordinates:
!		       1st table (X,Y), 2nd table (X,Y)
!		  P5 = estimated separations in X, Y between 1st and 2nd tables,
!		       max residual distance after coordinate translation, 
!		       max residual distance after coordinate rotation 
!		  P6 = 0: coordinates in their own system
!		       1: 2nd system transformed in 1st system
!
!------------------------------------------------------------------------------
DEFI/PARA P1  ?           T "Enter first input table:"
DEFI/PARA P2  ?           T "Enter second input table:"
DEFI/PARA P3  ?           T "Enter output table:"
DEFI/PARA P4  ?           N "Enter the coordinate columns in input tables:"
DEFI/PARA P5  0.,0.,2.,2. N "Enter the esitmated error separations:" 
DEFI/PARA P6  0           N "Enter the coordinate system:"
!
WRITE/KEYW IN_A           'P1'
WRITE/KEYW IN_B	         'P2'
WRITE/KEYW OUT_A          'P3'
WRITE/KEYW INPUTI         'P4'
WRITE/KEYW INPUTR         'P5'
WRITE/KEYW INPUTI/I/5/1   'P6'
!
RUN CON_EXE:PAIR
