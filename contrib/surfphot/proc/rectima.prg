! @(#)rectima.prg	19.1 (ES0-DMD) 02/25/03 13:31:09
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: rectimag.prg
!.PURPOSE:        MIDAS command procedure RECTIFY to implement  RECTIFY/IMAGE
!.AUTHOR:         D. Baade
!.VERSION:        850813  D. Baade, ST-ECF: creation
!.VERSION:        870520  K. Banse, ESO   : ????????
!.USE:            RECTIFY/IMAGE
!.NOTE:           Drive MIDAS modules REGRESSION/POLYNOMIAL, pass coefficients
!                 to MIDAS module RECTIMAG which does the rebinning to 
!                 geometrically rectify the input frame
! -----------------------------------------------------------------------------
DEFINE/PARAM P1 ? IMA "Enter first image:"
DEFINE/PARAM P2 ? IMA "Enter second image:"
DEFINE/PARAM P3 ? TABLE "Enter table:"
DEFINE/PARAM P4 1 N
DEFINE/PARAM P5 N C
!
DEFINE/LOCAL MYACT/C/1/2 {MID$CMND(11:12)}	!save qualifier...
DEFINE/LOCAL ERRSAV/I/1/1 {ERROR(3)}		!disable errors in regression
ERROR(3) = -1			
!
WRITE/KEYW COEFFX/D/1/20 0.D0 ALL
WRITE/KEYW COEFFY/D/1/20 0.D0 ALL
! 
REGRESSION/POLY {P3} #1 #3,#4 3,3	!do the regression analysis in x
COPY/KK OUTPUTD COEFFX			!copy key OUTPUTD to hold coefficients
REGRESSION/POLY {P3} #2 #3,#4 3,3	!and now same thing for y
COPY/KK OUTPUTD COEFFY			!copy key OUTPUTD to hold coefficients
ERROR(3) = ERRSAV			!reset key ERROR
!
! fill relevant keywords
! 
WRITE/KEYW IN_A {P1}
WRITE/KEYW OUT_A {P2}
inputi(1) = {P4}
IF P5(1:1) .EQ. "Y" THEN
   INPUTR(1) = 3.			!SUBFACTOR in RECTIMAG/RECTSPEC
ELSE
   INPUTR(1) = 1.
ENDIF
!
RUN CON_EXE:RECTIMAG
