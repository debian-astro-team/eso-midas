! @(#)bgfit.prg	19.1 (ES0-DMD) 02/25/03 13:31:07
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT:    bgfit.prg
!.PURPOSE:  implement FIT/BACKGROUND
!.AUTHORS:  E. Held
!.USE:      FIT/BACK result_frame = input_spec deg,max_iter clip_1,clip_next
!                    up_lim surface
!           FIT/BACK input_spec deg,max_iter clip_1,clip_next up_lim surface
!           FIT/BACK result_frame = in_frame COEF surface
!           FIT/BACK in_frame COEF surface
!
!           with input_spec either in_frame,in_mask (a) or in_frame (b)
!.VERSION:  840518 rev. 1.2
!.VERSION:  840614 KB ???
!.VERSION:  901108 KB ???
!----------------------------------------------------------------------------
DEFINE/LOCAL L/I/1/5 0,0,0,0,0
!
IF P2(1:1) .EQ. "=" THEN		!handle option result = ...
   DEFINE/PARAM P1 ? IMA "Enter result frame:"
   DEFINE/PARAM P4 3,10 N
   DEFINE/PARAM P5 1.,2.5 N
   DEFINE/PARAM P6 0.05 N
   DEFINE/PARAM P7 + IMA
!
   WRITE/KEYW OUT_A 'P1'
   L = M$INDEX(P3,",")
   IF L .GT. 0 THEN
      L(2) = L(1) - 1
      WRITE/KEYW IN_A 'P3(1:{L(2)})'
      L(2) = L(1) + 1
      WRITE/KEYW IN_B 'P3({L(2)}:>)'
   ELSE
      WRITE/KEYW IN_A 'P3'
      WRITE/KEYW IN_B +
   ENDIF
!
   IF P4(1:4) .EQ. "COEF" THEN
      WRITE/KEYW INPUTC BG_COEF		!coefficients are already calculated
      WRITE/KEYW OUT_B 'P5'
   ELSE
      WRITE/KEYW INPUTC ?
      WRITE/KEYW INPUTI/I/1/2 'P4'
      WRITE/KEYW INPUTR/R/1/2 'P5'
      WRITE/KEYW INPUTR/R/3/1 'P6'
      WRITE/KEYW OUT_B 'P7'
      WRITE/KEYW BG_COEF/D/1/28 0.0 ALL
   ENDIF
!
ELSE					!handle option input_spec ...
   WRITE/KEYW OUT_A +
   L = M$INDEX(P1,",")
   IF L .GT. 0 THEN
      L(2) = L(1) - 1
      WRITE/KEYW IN_A 'P1(1:{L(2)})'
      L(2) = L(1) + 1
      WRITE/KEYW IN_B 'P1({L(2)}:>)'
   ELSE
      WRITE/KEYW IN_A 'P1'
      WRITE/KEYW IN_B +
   ENDIF
   IF P2(1:4) .EQ. "COEF" THEN
      DEFINE/PARAM P3 + IMA
      WRITE/KEYW INPUTC BG_COEF		!coefficients are already calculated
      WRITE/KEYW OUT_B 'P3'
   ELSE
      DEFINE/PARAM P2 3,10
      DEFINE/PARAM P3 1.,2.5
      DEFINE/PARAM P4 .05
      DEFINE/PARAM P5 + IMA
!
      WRITE/KEYW INPUTC ?
      WRITE/KEYW INPUTI/I/1/2 'P2'
      WRITE/KEYW INPUTR/R/1/2 'P3'
      WRITE/KEYW INPUTR/R/3/1 'P4'
      WRITE/KEYW OUT_B 'P5'
      WRITE/KEYW BG_COEF/D/1/28 0.0 ALL
   ENDIF
!
ENDIF
!
RUN CON_EXE:BACKGRND                     !now execute the program
