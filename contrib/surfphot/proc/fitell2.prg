! @(#)fitell2.prg	19.1 (ES0-DMD) 02/25/03 13:31:08
! ++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT:  ellips.prg
!.AUTHOR: Edwin Valentijn
!.USE:    FIT/ELLIPS par1 par2 par3 par4 par5 par6 par7
!         par1    = inframe
!         par2    = 0 when polar frames should be made
!                 = 1  "     "     "    are already made - enables quick rerun
!         par3    = isophotal tolerance in pro mille of the iso levels
!         par4    = isophotal levels, in char. string of maximum size 10 values
!         par5    = center X,Y
!         par6    = radius around center to be surveyed
!         par7    = sky_level
!.VERSION: 870713 KB  ???
!.VERSION: 920216 RHW implementation in MIDAS (with help for HS, Basel)
! --------------------------------------------------------------------------
DEFINE/PARAM P1 ?  IMA               "Enter input image"
DEFINE/PARAM P2 0  N                 "Enter polar option, 0 or 1:"
DEFINE/PARAM P3 20 N                 "Enter isophotal window in promille:"
DEFINE/PARAM P4 "1" C                "Enter isophotal levels separated by comma:"
DEFINE/PARAM P5 'MORPH(1:20)' N      "Enter center x,y:"
DEFINE/PARAM P6 'MORPH(21:40)' N     "Enter radius around center:"
DEFINE/PARAM P7 'MORPH(41:60)' N     "Enter skyflag,background_level:"
!
!IF P2 .EQ. "0" THEN
!   $ ASSI/USER NL: SYS$OUTPUT
!   $ DELETE/NOCONF SCRRADII.BDF;*
!   $ ASSI/USER NL: SYS$OUTPUT
!   $ DELETE/NOCONF SCRANGLI.BDF;*
!ENDIF
!
WRITE/KEYW IN_A 'P1'                                !write frame into IN_A
WRITE/KEYW INPUTI/I/1/1  'P2'                       !write polar option
WRITE/KEYW INPUTI/I/2/1  'P3'                       !write isophotal window
WRITE/KEYW INPUTC/C/1/72 'P4'                       !write contours
!
WRITE/KEYW INPUTR/R/1/2  'P5'                       !write the centre
WRITE/KEYW MORPH/C/1/20  'P5'
!
WRITE/KEYW INPUTR/R/3/1  'P6'                       !write the radius
WRITE/KEYW MORPH/C/21/20 'P6'
!
WRITE/KEYW INPUTR/R/4/1  'P7'                       !write the background
WRITE/KEYW MORPH/C/41/20 'P7'
!
DISPLAY/LONG
RUN CON_EXE:FITELL2
