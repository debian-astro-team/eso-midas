! @(#)dtoi.prg	19.1 (ES0-DMD) 02/25/03 13:31:08
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: DTOI.PRG
!.PURPOSE:        Transform density to intensity
!.USE:            CONVERT/DTOI P1 P2 
!                 P1 = name of input frame in PDS density units
!	          P2 = name of associated calibration table, one row only
!                 P1 = name of output frame in intensity
! NOTE:           Descriptor O_TIME/D must exist; value<1984. 
!                 For old PDS config. The value of O_TIME is vital to the
!		  successful execution of the procedure!
!		  WARNING: after 13.1.1984 the PDS scans generally do NOT 
!		  contain the actual O_TIME (in most cases still value<1984 !)
!
!                 Descriptor SKYBGR/R (4 values) must exist in PDS density
!		  units
!		  Descriptor HISTORY/C will be checked for and appended with 
!		  'INTE' (to avoid D to I conversion a second time)
!		  Another warning is that SKYBGR = 1 implies intensity.
!---------------------------------------------------------------------------
DEFI/PARAM     P1    A   I
DEFI/PARAM     P2    C   T
DEFI/PARAM     P3    C   I
WRITE/KEYW    IN_A   'P1'
WRITE/KEYW    IN_B   'P2'
WRITE/KEYW    OUT_A  'P3'
COPY/II      'P1'   'P3'
!
RUN CON_EXE:DTOI
WRITE/DESCR  'P3' HISTORY/C/-1/21 "DENSITY TO INTENSITY"
