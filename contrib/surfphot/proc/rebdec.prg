! @(#)rebdec.prg	19.1 (ES0-DMD) 02/25/03 13:31:09
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENT:  
!.PURPOSE: to implement REBIN/DECONVOLVE
!.AUTHOR:  D. Baade
!.VERSION: 880928 DB creation
!.VERSION: 900220 KB ???
!.USE:     REBIN?DECONV inframe psf outframe zoom_x,zoom_y n_iter 
!.NOTE:    Rebin an image to smaller stepsize. Reconstruct the information 
!          contents by deconvolving the image with the user-supplied 
!          point spread function (PSF). The deconvolution algorithm is 
!          the one developed by LUCY. Most of the error and inconsistency 
!          checks of the input data are performed in rebdec.for  
!
!--------------------------------------------------------------------------- 
DEFINE/PARAM P1 ? IMA "Enter name of input frame:"
DEFINE/PARAM P2 psf IMA "Enter name of PSF:"
DEFINE/PARAM P3 ? IMA "Enter name of output frame:"
DEFINE/PARAM P4 ? N "Enter zoom factors (X,Y):"
DEFINE/PARAM P5 1 N "Enter number of iterations to be performed:"
!
WRITE/KEYW IN_A 'P1'
WRITE/KEYW IN_B 'P2'
WRITE/KEYW OUT_A 'P3'
WRITE/KEYW INPUTI/I/1/2 'P4'
WRITE/KEYW INPUTI/I/3/1 'P5'
!
! INPUTI(4) tells the main program whether a frame with the name 
! of the output frame already exists in which case it will be taken 
! as an initial guess of the solution.
!
INPUTI(4) = M$EXIST("'OUT_A'.bdf")
!
! Prepare key HISTORY
!
WRITE/KEYW HISTORY "'MID$CMND(1:6)'/'MID$CMND(11:14)' 'P1' 'P2' 'P3' 'P4' 'P5'"
!
DISPLAY/LONG
RUN CON_EXE:REBDEC
