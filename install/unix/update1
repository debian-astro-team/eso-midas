#! /bin/bash
# .COPYRIGHT: Copyright (c) 1988-2011 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           update1.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 scratch.
#                 Usage: install1 system [options]
#
# .AUTHOR         Carlos Guirao
# .VERSION 3.1    910724:		New implementation.
# 111206	last modif

echo=echo
if [ "`echo -n`" = "-n" ] ; then
        SV_NONL="\c"
else
        echo="echo -n"
fi

#
# First of all, goto the config directory adn
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

cd $MID_INSTALL

clear
if [ ! -f "$MID_HOME/local/default.mk" ] ; then
  echo "*** ERROR: File <$MID_HOME/local/default.mk> does not exist."
  echo "*** You need to run <preinstall> before continuing with <update>"
  exit 1
fi

if [ ! -f "$MID_HOME/local/make_options" ] ; then
  touch $MID_HOME/local/make_options
fi

echo ""
echo "MIDAS UPDATE PROCEDURE"
echo "======================"
echo "MIDASHOME: $MIDASHOME"
echo "MIDVERS:   $MIDVERS"
echo "MAKE_OPTIONS:"
awk -F# '{ if ($1 != "") {printf "\t  %s\n",$1} }' $MID_HOME/local/make_options
echo ""
echo "NOTE: Update will only compile and link those files and"
echo "      executables not updated in previous installation" 
echo "      It will run in background and the output will be sent to"
echo "      the file <$MID_HOME/tmp/update.$$>"
echo ""
$echo "Do you want to continue [yn]? (y): " $SV_NONL
read answ
if [ -z "$answ" ]; then
    answ=y
fi
if [ "$answ" != "y" -a "$answ" != "Y" ]
then
    echo "Bye"
    exit 0
fi

#
# If $MID_HOME/tmp does not exists then creates it.
#
if [ ! -d $MID_HOME/tmp ]; then
    mkdir $MID_HOME/tmp
fi

time /bin/bash $MID_INSTALL/update -i > $MID_HOME/tmp/update.$$ 2>&1 &
echo "Running update in background"
echo "Results are coming in $MID_HOME/tmp/update.$$"
