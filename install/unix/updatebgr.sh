#! /bin/bash
# .TYPE           command
# .NAME           updatebgr.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 the scratch.
#                 
# .REMARKS        The external variable $MIDASHOME and $MIDASVERS must be 
#		  set before run 
#                 this script. They can be defined in the ".profile" or 
#                 ".cshrc" and executed when you login in the MIDAS account.
#
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    24-Jun-1988:		Implementation
# .VERSION 1.2    910219:		Removing System name
# .VERSION 2.1    910724:		Output to $MIDASHOME/$MIDVERS/local
# 111206	last modif

if [ $# -gt 1 ]
then
	echo "Use: update.bgr [MAKE_OPTIONS]"	
	exit
fi

#
# If $MID_HOME/tmp does not exists then creates it.
#
if [ ! -d $MID_HOME/tmp ]; then
    mkdir $MID_HOME/tmp
fi

time /bin/bash ./update $1 > $MIDASHOME/$MIDVERS/tmp/update.$$ 2>&1 &
echo "Running update in background"
echo "Results are coming in $MIDASHOME/$MIDVERS/tmp/update.$$"
