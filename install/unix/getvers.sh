#! /bin/bash
#.COPYRIGHT: Copyright (c) 2005-2006 European Southern Observatory,
#                           
#.TYPE           command
#.NAME           getvers.sh
#.LANGUAGE       shell script
#.ENVIRONMENT    Unix Systems. Executable under bash shell !
# 
# 
#.COMMENTS       check the version of the GNU C compiler
#.AUTHOR         K. Banse	ESO - Garching
# 
#.VERSION
# 051026	creation
# 070615	last modif
# 


gcc -v > gcc.version 2>&1			# save output in file ./gcc.version

# read the file ./gcc.version line by line, process only the last one

while read line  
do
   begi=${line:0:12}
   if [ "$begi" = "gcc version " ] ; then
      longversion=${line#"gcc version "}
      version=${longversion:0:1}
      if [ "$version" = "4" ] ; then
         echo gfortran
      else
         echo g77
      fi
   fi
done < gcc.version

rm gcc.version

exit 0
