#! /bin/sh
# @(#)cleanm.sh	19.1 (ESO-IPG) 02/25/03 13:51:09
# .COPYRIGHT: Copyright (c) 1988 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           cleanm.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 scratch.
#                 Usage: install system [options]
#
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    30-Oct-1989:		Installation.
# .VERSION 1.2    05-Dec-1989:		Adding "echo" definition
# .VERSION 1.3    30-Jan-1992:		Adding factor of blocks 
# .VERSION 1.4    15-Oct-1993:		Docomuntation does not exist.

echo=echo
if [ "`echo -n`" = "-n" ] ; then
        SV_NONL="\c"
else
        echo="echo -n"
fi

#
# First of all, goto the config directory MID_INSTALL
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

#
# START
#
#clear

# 
# Check weather "ls -s" command returns blocks of 1/2 or 1 kbytes.
# The file test_file_1k is 1kbyte big.
factor_ls=`ls -s test_file_1k | awk '{print $1}'`

task="unknown"
while :
do
echo ""
echo "	       MIDAS DELETE MENU:"
echo "	===================================="
echo "	1 - Delete only object files."
echo "	2 - Delete object and source files."
echo "	3 - Delete object, source files and libraries."
echo "	4 - Delete executable files."
echo "	q - Quit."
echo ""
$echo "Select: " $SV_NONL

read task
task=`echo $task | tr A-Z a-z`

echo ""
case $task in
1) 
  echo "1. This option will delete all *.o files under $MID_HOME"
  $echo "   Computing first number of kbytes to be deleted [yn]? (y): " $SV_NONL
  read answ
  if [ -z "$answ" ]; then
    answ=y
  fi
  if [ "$answ" = "y" -o "$answ" = "Y" ]; then
    $echo "   Number of kbytes to be deleted: (wait) " $SV_NONL
    expr `find $MID_HOME -name "*.o" -exec ls -s {} \; \
      | awk '{size += $1} END {printf "%d\n",size}'` / $factor_ls
  fi
  ;;
2) 
  echo "2. This option will delete all *.o *.for *.c files under $MID_HOME"
  $echo "   Computing first number of kbytes to be deleted [yn]? (y): " $SV_NONL
  read answ
  if [ -z "$answ" ]; then
    answ=y
  fi
  if [ "$answ" = "y" -o "$answ" = "Y" ]; then
    $echo "   Number of kbytes to be deleted: (wait) " $SV_NONL
    expr `find $MID_HOME \( -name "*.o" -o -name "*.for" -o -name "*.c" \) \
      -exec ls -s {} \; \
      | awk '{size += $1} END {printf "%d\n",size}'` / $factor_ls
  fi
  ;;
3) 
  echo "3. This option will delete all *.o *.for *.c *.a files under $MID_HOME"
  $echo "   Computing first number of kbytes to be deleted [yn]? (y): " $SV_NONL
  read answ
  if [ -z "$answ" ]; then
    answ=y
  fi
  if [ "$answ" = "y" -o "$answ" = "Y" ]; then
    $echo "   Number of kbytes to be deleted: (wait) " $SV_NONL
    expr `find $MID_HOME \( -name "*.o" -o -name "*.for" -o -name "*.a" -o \
      -name "*.c" \) -exec ls -s {} \; \
      | awk '{size += $1} END {printf "%d\n",size}'` / $factor_ls
  fi
  ;;
4)
  /bin/sh $MID_INSTALL/delete
  continue
  ;;
q) 
  exit
  ;;
*) 
  echo "Selection unknown"
  continue
  ;;
esac

if [ $? != 0 ]; then
    continue
fi

$echo "   Do you want to continue [yn]? (y): " $SV_NONL
read answ
if [ -z "$answ" ]; then
    answ=y
fi
if [ "$answ" != "y" -a "$answ" != "Y" ]
then
    continue
fi
#exit

echo "   Start removing."
case $task in
1)
  echo '   find $MID_HOME -name "*.o" -exec rm -f {} \;'
  find $MID_HOME -name "*.o" -exec rm -f {} \;
  ;;
2)
  echo '   find $MID_HOME \( \
     	-name "*.o" -o \
  	-name "*.for" -o  \
  	-name "*.c" \) -exec rm -f {} \;'
  find $MID_HOME \( \
  	-name "*.o" -o \
  	-name "*.for" -o  \
  	-name "*.c" \) -exec rm -f {} \;
  ;;
3)
  echo '   find $MID_HOME \( \
  	-name "*.o" -o \
  	-name "*.for" -o  \
  	-name "*.c" -o  \
  	-name "*.a" \) -exec rm -f {} \;'
  find $MID_HOME \( \
  	-name "*.o" -o \
  	-name "*.for" -o  \
  	-name "*.c" -o  \
  	-name "*.a" \) -exec rm -f {} \;
  ;;
esac

echo "   Done."
done
