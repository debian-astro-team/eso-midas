#! /bin/sh
# .COPYRIGHT: Copyright (c) 1988 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           delete.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       
#
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    30-Oct-1989:		Implementation.
# .VERSION 1.2    05-Dec-1989:		Adding "echo" definition
# 070615	last modif
# 
echo=echo
if [ "`echo -n`" = "-n" ] ; then
        SV_NONL="\c"
else
        echo="echo -n"
fi

#
# First of all, goto the config directory MID_INSTALL
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

#
# START
#

# Check weather "ls -s" command returns blocks of 1/2 or 1 kbytes.
# The file test_file_1k is 1kbyte big.
factor_ls=`ls -s test_file_1k | awk '{print $1}'`

while : ; do
(echo ""
echo "	DELETE EXECUTABLES FILES FROM:"
echo ""
echo "	NAME		CATEGORY"
echo "	========================"
for categ in applic stdred contrib gui
do
    if [ ! -d $MID_HOME/$categ ]; then
	continue
    fi
    cd $MID_HOME/$categ
    for pack in `ls | egrep -v 'lib|exec|proc'`
    do
        if [ -f $pack/src/makefile ] ; then
            echo  "	$pack      	$categ	"
	fi
   done
done
echo "" ) | more
cd $MID_INSTALL
echo "Type <Q> to quit or <L> to list again."

while : ; do
    $echo "Select NAME to DELETE: " $SV_NONL
    read name
    if [ "$name" = "q" -o "$name" = "Q" ] ; then
	exit
    fi
    if [ "$name" = "l" -o "$name" = "L" ] ; then
	break
    fi
    if [ "$name" = "" ] ; then
	continue
    fi
    if [ -f $MID_HOME/applic/$name/src/makefile ] ; then
	categ=applic
    elif [ -f $MID_HOME/stdred/$name/src/makefile ] ; then
	categ=stdred
    elif [ -f $MID_HOME/contrib/$name/src/makefile ] ; then
	categ=contrib
    elif [ -f $MID_HOME/gui/$name/src/makefile ] ; then
	categ=gui
    else
	echo "$name: No such package with that name"
	continue
    fi

    $echo "Number of kbytes to be deleted: (wait) " $SV_NONL
    cd $MID_HOME/$categ/$name/src
    ls -s `make -n clean_exec | sed -e 's/^rm//' -e 's/-f//'` 2>/dev/null | \
    awk -f $MID_INSTALL/size.awk factor=$factor_ls
    if [ $? != 0 ] ; then
	continue
    fi
    $echo "Do you want to delete $name executables [yn]? (y): " $SV_NONL
    read answ
    if [ -z "$answ" ]; then
        answ=y
    fi
    if [ "$answ" != "y" -a "$answ" != "Y" ]; then
        cd $MID_INSTALL
	echo ""
        continue
    fi

    make clean_exec
    cd $MID_INSTALL
    echo ""
done
done

exit 
