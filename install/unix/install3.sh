#! /bin/bash
# .COPYRIGHT: Copyright (c) 2011 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           install3.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 scratch.
#                 Usage: install system [options]
#
# .AUTHOR         Klaus Banse
# .VERSION 
# 110825	creation
# 190322	last modif	take out continue statements,
#				and fix for being called from config


#*************** building dummy NAG library *********************

# the install3 script is called from install1 with one parameter
# par1   = manual (to use an edited make_options file),
# or     = -a (via  autoconfig)
# or no parameter at all
 
cd $MID_HOME/system/ext
make 
if [ $? !=  0 ]; then
   echo "Error building esoext."
   exit 1
fi
#
cd $MID_HOME/libsrc/nag
rm -f ../../lib/libnag.a

make all
if [ $? !=  0 ]; then
   echo "Error creating dummy NAG library."
   exit 1
fi

#**************************************************************


cd $MID_INSTALL

echo ""
echo "MIDAS INSTALLATION PROCEDURE"
echo "============================"
echo "MIDASHOME: $MIDASHOME"
echo "MIDVERS:   $MIDVERS"

if [ $1 = "manual" ]; then
   echo "Your own MAKE_OPTIONS after checks and/or editing:"
else
   echo "Your final MAKE_OPTIONS after all checks:"
fi

echo "<$MID_HOME/local/make_options>"

awk -F# '{if ($1 != "") {printf "\t  %s\n",$1} }' $MID_HOME/local/make_options
echo ""

echo "Do you want to check the OS library [yn]? (y): " $SV_NONL
unset answ
if [ "$1" != "-a" ]; then    
  read answ
fi
if [ -z "$answ" ]; then
    answ=y
fi
if [ "$answ" = "y" -o "$answ" = "Y" ]
then
    cd $MID_HOME/libsrc/os/unix
    rm -f $MID_HOME/lib/libos.a
    make clean_exec
    make clean
    make all
    make testos.exe
    if [ $? != 0 ]; then
	echo ""
	echo " **********************************************"
	echo " *               ATTENTION                    *"
	echo " * There was a severe error in your <oslib.a> *"
	echo " * that should be corrected before continuing *"
	echo " * with the MIDAS installation.               *"
	echo " *                                            *"
	echo " * Go to $MID_HOME/libsrc/os/unix "
	echo " * and type by hand:                          *"
	echo " *                                            *"
        echo " *     make clean_exec                        *"
        echo " *     make clean                             *"
        echo " *     make all                               *"
        echo " *     make testos.exe                        *"
	echo " *                                            *"
        echo " * The sequence above should be executed      *"
        echo " * without any error (Warnings are tolerated) *"
        echo " * MIDAS installation will be aborted !!      *"
        echo " *                                            *"
        echo " * For help contact ESO's Midas support:      *"
        echo " * midas@eso.org                              *"
	echo " **********************************************"
        echo ""
	$echo "Type return to exit: " $SV_NONL
	read return
	exit 1
    fi
fi

echo ""
echo "WARNING: MIDAS installation will delete all dependent files."
echo "         It will run in background and the output will be sent to"
echo "         the file <$MID_HOME/tmp/install.$$>"
echo ""
echo "Do you want to continue [yn]? (y): " $SV_NONL
unset answ
if [ "$1" != "-a" ]; then 
  read answ
fi
if [ -z "$answ" ]; then
    answ=y
fi
if [ "$answ" != "y" -a "$answ" != "Y" ]
then
    echo "Bye"
    exit 0
fi

#
# if $MID_HOME/tmp does not exist then create it
#
if [ ! -d $MID_HOME/tmp ]; then
    mkdir $MID_HOME/tmp
fi

# do we have the 'time' command?
if [ $CMND_YES = 2 ] ; then
   /bin/bash $MID_INSTALL/install -i > $MID_HOME/tmp/install.$$ 2>&1 &
else
   time /bin/bash $MID_INSTALL/install -i > $MID_HOME/tmp/install.$$ 2>&1 &
fi

echo "Running install in background"
echo "Results are coming in $MID_HOME/tmp/install.$$"

if [ "$1" = "-a" -o "$1" = "auto" ]; then 
  echo "Waiting for MIDAS installation to finish..."  $SV_NONL
  wait
  echo "Finished."
  echo ""
fi
exit 0
