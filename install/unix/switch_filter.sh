#! /bin/sh
while read input
do
	command=`echo $input | sed 's/^\([^ 	]*\).*/\1/g'`
#	echo $command
	case $command in
	cp)
	# Filter, copying files:
	#	cp	-->	$copy
	#	*	-->	*;*
	#	./	-->	(null)
	#	..	-->	-
	#       -.-.dir/ -->    [-.-.dir]
	#	.o	-->	.obj
		echo $input | sed \
		-e 's/[ 	][ 	]*/ /g' \
		-e 's/^cp/\$ COPY/' \
		-e 's/ \.\// /g' \
		-e 's/\.\./-/g' \
		-e 's/-\//-\./g' \
		-e 's/\.-/-/g' \
		-e 's/ \([-\.][-\.]*[^\/]*\)\// \[\1\]/g' \
		-e 's/\.o/.obj/g'
		;;

	# rm) entry remove because it is not needed any more. CG 301089
	#rm)
	## Filter removing files in directories:
	##
	##	tabs	-->	single blank
	##	rm	-->	$ DELETE
	##	-f	-->	(null)
	##	*.f	-->	(null)
	##	file.o	-->	file.obj;*
	##	file.exe-->	file.exe;*
	##	file.a	-->	file.obl;*
	#	echo $input | sed \
	#	-e 's/[ 	][ 	]*/ /g' \
	#	-e 's/ -f//g' \
	#	-e 's/\.f$/& /' \
	#	-e 's/[^ ]*\.f //g' \
	#	-e 's/rm *$//' \
	#	-e 's/^rm/\$ DELETE/' \
	#	-e 's/\.o[ 	]*/.obj;*,/g' \
	#	-e 's/\.exe[ 	]*/.exe;*,/g' \
	#	-e 's/\.a[ 	]*/.olb;*,/g' \
	#	-e 's/\.c[ 	]*/.c;*,/g' \
	#	-e 's/\.for[ 	]*/.for;*,/g' \
	#	-e 's/\.h[ 	]*/.h;*,/g' \
	#	-e 's/,$//'
	#	;;

	cc)
	# Filter compiling "C" files in directories:
	#
	#	tabs		-->	single blank
	#	cc		-->	$ CC
	#	-I/*    	-->	-I*
	#	-I./*   	-->	-I.*
	#	..		-->	-
	#	pl*.*		-->	pl*_* (eg. pl2.1 in pl2_1)
	#	/		-->	.
	#	-Ddef		-->	/DEFINE=(def
	#	-Ddef		-->	,def
	#	/DEFINE*	-->	/DEFINE=(*)
	#	-Iinclude_R4	-->	(null)
	#	-I.		-->	(null)
	#	-Idir		-->	/INCLUDE=([dir]
	#	-Idir		-->	,[dir]
	#	/DEFINE*	-->	/DEFINE=(*)
	#	-g		-->	(null)
	#	-O		-->	(null)
	#	.-		-->	-
	#	-c		-->	(null)
	#	# .c		-->	(null) 
	#	[*midas.<rels>   -->	['MIDASHOME'.'MIDVERS'
	# 	-e 's/\.c$//g'
	#	-e 's/\.c[ 	]*/ /g' 
		echo $input | sed \
		-e 's/[ 	][ 	]*/ /g' \
		-e 's/^cc/\$ CC/' \
		-e 's/ -I\// -I/g' \
		-e 's/ -I\.\// -I\./g' \
		-e 's/\.\./-/g' \
		-e 's/pl\(.\)\./pl\1_/' \
		-e 's/\//\./g' \
		-e 's/-D/\/DEFINE=\(/' \
		-e 's/ -D/,/g' \
		-e 's/DEFINE[^ ]*/&\)/' \
		-e 's/ -I[^ ]*include_R4 / /g' \
		-e 's/ -I\. / /g' \
		-e 's/ -I\([^ ]*\)/ \/INCLUDE=\(\[\1\]/' \
		-e 's/ -I\([^ ]*\)/,\[\1\]/g' \
		-e 's/INCLUDE[^ ]*/&\)/' \
		-e 's/ -g/ /g' \
		-e 's/ -O/ /g' \
		-e 's/ -c/ /g' \
		-e 's/\.-/-/g' \
		-e 's/ -[^ ]*/ /g' \
		-e 's/\[[^,]*midas\.[^\.]*/\['\''MIDASHOME'\''.'\''MIDVERS'\''/g' 
		;;

	f77)	
	# Filter compiling fortran files in directories:
	#
	#	f77	-->	$ FORTRAN
	#	-O	-->	(null)
	#	-u	-->	(null)
	#	-d*	-->	(null)
	#	-g	-->	(null)
	#	-c	-->	(null)
	#	#.f	-->	(null)
	#	.f	-->	.for
		echo $input | sed -e 's/^f77/\$ FORTRAN/' \
			-e 's/-[cgOu]//g' \
			-e 's/-d.* //g' \
			-e 's/ -[^ ]*/ /g' \
			-e 's/\.f/\.for/g'
		;;
	ar)
        # Filter generating libraries files in directories:
	#
	#	ar		-->	$ LIB/REPLACE
	#	[./]*    	-->	[null]
	#	dirs/lib.a	-->	lib.a
	#	lib.a		-->	lib
	#	file1.o file2.o	-->	file1,file2
		echo $input | sed -e 's/^ar[ 	]*ruv/\$ LIB\/REPLACE/' \
			-e 's/[ 	][\.\/]*/ /g' \
			-e 's/REPLACE .*\/\(.*\)/REPLACE \1/' \
			-e 's/\.a//' \
			-e 's/\.o[ 	]*/\.obj,/g' \
			-e 's/,$//'
		;;
	ldvms)	
	# Filter linking files in directories:
	# 
	#	tabs			-->	single blank
	# 	..			-->	-
	# 	/			-->	.
	#	file.opt		-->	file/OPT,
	#	file.o			-->	file.obj,
	#	lib1.a lib2.a		-->	lib1/L, lib2/L
	#	ldvms * -o dir.file.exe	-->	$ LINK/NOMAP/EXE=[dir]
	#	-o *exec/file.exe	-->	(null)
	#	-l*			-->	lib*/L
	#	libm/L			-->	(null) 
	#	libtermcap/L		-->	(null) 
	#	libreadline/L		-->	(null) 
	#	libsocket/L		-->	(null) 
	#	libnsl/L		-->	(null) 
	#	-L*			-->	(null)
	#	-.dir.file.obj		-->	[-.dir.]file.obj
	#	-R*			-->	(null)
		echo $input | sed  -e 's/[ 	][	 ]*/ /g' \
			-e 's/\.\./-/g' \
			-e 's/\//./g' \
			-e 's/\.opt /\/OPT, /g' \
			-e 's/\.o /\.obj, /g' \
			-e 's/\.a /\/L, /g' \
			-e 's/^ldvms \(.*\) -o [^\.-]*\.exe/$ LINK\/NOMAP \1/' \
-e 's/^ldvms \(.*\) -o \(.*\)\.[^\.]*\.exe/$ LINK\/NOMAP\/EXE=\[\2\] \1/' \
			-e 's/\(.*\)NOMAP\(.*OPT.*\)/\1NOTRACE\2/' \
			-e 's/[-\.][-\.]*lib\.//g' \
			-e 's/\.-/-/g' \
			-e 's/ -l\([^ 	]*\)/ lib\1\/L,/g' \
			-e 's/libm\/L,//g' \
			-e 's/libtermcap\/L,//g' \
			-e 's/libreadline\/L,//g' \
			-e 's/libsocket\/L,//g' \
			-e 's/libnsl\/L,//g' \
			-e 's/ -L[^ 	]*//g' \
			-e 's/ -R[^ 	]*//g' \
			-e 's/ \(-.*\)\.\([^\.-]*\.obj\)/ \[\1\]\2/g' \
			-e 's/ *$//' \
			-e 's/,$//'
		;;
	macro)
	# Filter compiling macros files in directories:
	#
	#	macro		-->	$ MACRO
		echo $input | sed -e 's/^macro/\$ MACRO/'
		;;
	*esoext.exe)
	# Filter for UNIX filter esoext.exe
		;;
	*.exe)
	# Filter for other executables:
	# It removes ftoc_names.exe` argument for FORIF command
	# It removes ftoc_vmr.exe` argument for FORIF command
	#
	#	tabs			-->	single blank
	#	`*ftoc_names.exe`	-->	blank
	#	`*ftoc_vmr`		-->	blank
	#	(blank)/		-->	blank
	#	(blank)./		-->	blank/
	#	..	 		-->	-
	#	/	 		-->	.
	#	.-	 		-->	-
	# 	rel_dir/file.exe	-->	[.rel_dir]file
	# 	abs_dir/file.exe	-->	[abs_dir]file
	# 	file.exe		-->	file
	# 	[.*]file		-->	$ file
	# 	>	 	 	-->	/OUTPUT=
		echo $input | sed \
		-e 's/[ 	][ 	]*/ /g' \
		-e 's/ `[^ ]*ftoc_names.exe` / /'g \
		-e 's/ `[^ ]*ftoc_vmr` / /'g \
		-e 's/ \// /g' \
		-e 's/ \.\// \//g' \
		-e 's/\.\./-/g' \
		-e 's/\//\./g' \
		-e 's/\.-/-/g' \
		-e 's/\([^ ]*\)\.\([^ ]*\).exe/[\1]\2/g' \
		-e 's/.exe / /' \
		-e 's/.exe//' \
		-e 's/^\[[^ ]*\]//' \
		-e 's/^/\$ /' \
		-e 's/>/\/OUTPUT=/g' 
		;;

	*)	
	# Filter "ranlib" and "esoext" files in directories:
	#
	#	*	-->	 (null)
	#	echo $input | sed  -e 's/^.*$//'
		;;
	esac
done
exit 0
