/*===========================================================================
  Copyright (C) 1995-2007 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++  testxm  +++++++++++++++++++++++++++++++

..LANGUAGE     C

..IDENTIFICATION
use some definitions and functions from Motif
check existence of Motif include files

..VERSION
 140815         last modif
------------------------------------------------------------------------*/


/* Include files extracted from all the C files in  dir.
   "gui/GraphLib/libsrc/vms_uimx/src/" */
 
#define __COMPILE_MOTIF_LIBS__
#include <Xm/Xm.h>
#include <Xm/Text.h>
#include <Xm/ArrowB.h>
#include <Xm/ArrowBG.h>
#include <Xm/BulletinB.h>
#include <Xm/CascadeB.h>
#include <Xm/CascadeBG.h>
#include <Xm/Command.h>
#include <Xm/DialogS.h>
#include <Xm/DrawingA.h>
#include <Xm/DrawnB.h>
#include <Xm/FileSB.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/List.h>
#include <Xm/MainW.h>
#include <Xm/MenuShell.h>
#include <Xm/MessageB.h>
#include <Xm/PanedW.h>
#include <Xm/PushB.h>
#include <Xm/PushBG.h>
#include <Xm/RowColumn.h>
#include <Xm/Scale.h>
#include <Xm/ScrollBar.h>
#include <Xm/ScrolledW.h>
#include <Xm/SelectioB.h>
#include <Xm/SeparatoG.h>
#include <Xm/Separator.h>
#include <Xm/Text.h>
#include <Xm/ToggleB.h>
#include <Xm/ToggleBG.h>

#include <stdio.h>
#include <stdlib.h>

/*

for Kubuntu 7.10 we also had to install the package 	(071114)
x11proto-print-dev	for the printing extension protocol
because in Xm/Xm.h reference was made to X11/extension/Print.h

*/


main()
{
printf("%d\n",XmVersion);
exit(0);
}

#ifndef ONLY_INCLUDES
routine()
{
/* 
* Some X11 fuctions extracted from:
* gui/GraphLib/libsrc/vms_uimx/src/*.c
*/

XmRegisterConverters();
XmMenuPosition ((Widget)0, (XButtonEvent*)0);
XmScrolledWindowSetAreas((Widget)0, (Widget)0, (Widget)0, (Widget)0);
XmMainWindowSetAreas((Widget)0, (Widget)0, (Widget)0, 
                     (Widget)0, (Widget)0, (Widget)0);
XmAddTabGroup((Widget)0);
XmRemoveTabGroup((Widget)0);
XmConvertUnits ((Widget)0, (unsigned char)0, 
                (unsigned char)0, (int)0, (unsigned char)0);
XmStringCreateLtoR((char*)0, "");
 
exit(0);
}
#endif /* ONLY_INCLUDES */
