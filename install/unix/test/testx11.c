/*===========================================================================
  Copyright (C) 1995-2008 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

# include <stdlib.h>
# include <stdio.h>

# include <X11/Xlib.h>
# include <X11/Xutil.h>
# include <X11/cursorfont.h>
# include <X11/keysym.h>
/* this is for the Xt toolkit... not needed here
   # include <X11/Intrinsic.h>  */

main()
{
	/* see above: printf("%d\n",XtSpecificationRelease); */
	printf("%d\n",XlibSpecificationRelease);
	exit(0);
}

#ifndef ONLY_INCLUDES
int routine()
{
	/* 
	 * Some X11 functions extracted from module:
	 * $MIDASHOME/$MIDVERS/system/idiserv/x11/Xtest.c
	 */
	XOpenDisplay((char*)0);
	XDefaultVisual((Display*)0,(int)0);
	XMatchVisualInfo( (Display*)0, (int)0, (int)0, (int)0, (XVisualInfo*)0);
	XCreateColormap((Display*)0,(Window)0,(Visual*)0,(int)0);
	XAllocNamedColor((Display*)0,(Colormap)0,(char*)0,(XColor*)0,(XColor*)0);
	XAllocColorCells( (Display*)0, (Colormap)0, (Bool)0, (unsigned long*)0, (unsigned int)0, (unsigned long*)0, (unsigned int)0 );
	XQueryColors((Display*)0, (Colormap)0, (XColor*)0, (int)0 );

	XStoreColors((Display*)0,(Colormap)0,(XColor*)0,(int)0 );
	XFlush((Display*)0);
	XSetStandardProperties( (Display*)0, (Window)0, (char*)0, (char*)0, (Pixmap)0, (char**)0, (int)0, (XSizeHints*)0);
	XSetBackground((Display*)0,(GC)0,(unsigned long)0);
	XSetForeground((Display*)0,(GC)0,(unsigned long)0);
	XSetPlaneMask((Display*)0,(GC)0,(unsigned long)0);
	XSetFunction((Display*)0,(GC)0,(int)0);
	XSync((Display*)0,(Bool)0);
	XSelectInput((Display*)0,(Window)0,(long)0);
	XMapRaised ((Display*)0,(Window)0);
	XWindowEvent((Display*)0, (Window)0, (long)0, (XEvent*)0);
	XFlush ((Display*)0);
	XCreateImage( (Display*)0, (Visual*)0, (unsigned int)0, (int)0, (int)0, (char*)0, (unsigned int)0, (unsigned int)0, (int)0, (int)0);
	XPutImage( (Display*)0, (Drawable)0, (GC)0, (XImage*)0, (int)0, (int)0, (int)0, (int)0, (unsigned int)0, (unsigned int)0 );
	XCreateGC( (Display*)0, (Drawable)0, (unsigned long)0, (XGCValues*)0 );
	XInstallColormap( (Display*)0,(Colormap)0);
	XRecolorCursor( (Display*)0, (Cursor)0, (XColor*)0, (XColor*)0 );
	XDefineCursor( (Display*)0, (Window)0, (Cursor)0 );
	XSelectInput( (Display*)0, (Window)0, (long)0 );
	return 0;
}
#endif /* ONLY_INCLUDES */
