C @(#)testf.for	19.1 (ES0-DMD) 02/25/03 13:52:16
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C @(#)testf.for	19.1 (ESO) 02/25/03 13:52:16
      PROGRAM TESTF
C
C.IDENT: TESTF
C.PURPOSE: To test if variables in subroutines are kept or
C          reinitialized
C.USE    : testf.exe
C
      INTEGER IIN
      INTEGER IOUT
C
      IIN = 0
      CALL INIT(IIN,IOUT)
      IIN = 1      
      CALL INIT(IIN,IOUT)
C      STOP
      END
C
C
C
C
      SUBROUTINE INIT(IN,KINIT)
C 
      INTEGER      IN
      INTEGER      IINIT
      INTEGER      KINIT
      INTEGER      JINIT
      DATA         JINIT/12345/
 210  FORMAT('SAVE')
 220  FORMAT('NOSAVE')
C
      IF (IN.EQ.0) THEN
C        WRITE (6,100) 
        IINIT = JINIT
        KINIT = -1

      ELSE
        IF (IINIT.EQ.JINIT) THEN
           WRITE (6,210)
           KINIT = 0
        ELSE
           WRITE (6,220) 
           KINIT = 1
        ENDIF
      ENDIF

      RETURN
      END
