#! /bin/bash
# .COPYRIGHT: Copyright (c) 2002-2011 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           autoinstall 
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Automatic installation procedure of MIDAS
#                 Usage: autoconfig
#
# .REMARKS        The external variable $MIDASHOME and $MIDVERS must be set 
#
# .AUTHOR         Carlos Guirao
# .VERSION    
# 021028	implementation
# 111206	last modif 
#
# Checks echo command which option to use to get NO NEW LINE
#
echo=echo
if [ "`echo -n `" = "-n" ] ; then
	SV_NONL="\c"
else	
	echo="echo -n"
fi

clear

cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
MID_INSTALL=`pwd`
VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

export MIDASHOME MIDVERS

#
# CG: The DISPLAY variable is not needed anymore. I'm using 'ascii_bin no'
#if [ -z "$DISPLAY" ];  then
#  echo "DISPLAY environment variable not set - exiting"
#  exit 1
#fi

#
# START
#
echo "**********************************************************************"
echo "********** MIDAS AUTO INSTALLATION/CONFIGURATION SCRIPT **************"
echo "**********************************************************************"
echo "********** MIDASHOME: $MIDASHOME"
echo "********** MIDVERS:   $MIDVERS"
echo ""

$echo "Do you want to continue [yn]? (y): " $SV_NONL
read answ
if [ -z "$answ" ]; then
  answ="y"
fi
if [ "$answ" = "n" -o "$answ" = "N" ]; then
  exit 1
fi

/bin/bash $MID_INSTALL/select all
/bin/bash $MID_INSTALL/preinstall -a
/bin/bash $MID_INSTALL/install1 -a
/bin/bash $MID_INSTALL/setup -a
/bin/bash ../../system/unix/inmidas -j "@ compile.all"
/bin/bash ../../system/unix/inmidas -j "@ ascii_bin no ; bye"

echo  " "
echo "**********************************************************************"
echo "Midas installation successfully terminated."
echo "on:  "`date`
echo  " "
echo "Enter:"
echo "  inmidas         (if you want to run MIDAS defaults)"
echo "  gomidas         (to restart the previous MIDAS session)"
echo "  helpmidas       (standalone GUI help for MIDAS)"
echo "  drs <midas_command> (execute MIDAS command from Unix command line)"
echo "**********************************************************************"

exit 0
