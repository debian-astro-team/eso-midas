#! /bin/bash
# .COPYRIGHT: Copyright (c) 1988-2011 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           install1.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 scratch.
#                 Usage: install system [options]
#
# .AUTHOR         K. Banse
# .VERSION 1.0  
# 060328	Creation
# 111116	last modif


echo=echo
if [ "`echo -n`" = "-n" ] ; then
  SV_NONL="\c"
else
  echo="echo -n"
fi


# link correct ftoc dir to good-ftoc
# and copy relevant .fc files (+ correct y123.for files)

rm -rf $MID_HOME/system/good-ftoc

if [ -f "$MID_HOME/libsrc/ftoc/ftocc" ] ; then
   ln -s $MID_HOME/system/ftoc $MID_HOME/system/good-ftoc
   echo "*** Use (default) $MID_HOME/system/ftoc"
   MYNO=1
else
   ln -s $MID_HOME/system/ftoc-new $MID_HOME/system/good-ftoc
   echo "*** Use (new) $MID_HOME/system/ftoc-new"
   MYNO=2
fi

echo "*** and copy ...-$MYNO files for f2c work"


cp $MID_HOME/prim/plot/libsrc/yplot-$MYNO.for $MID_HOME/prim/plot/libsrc/yplot.for
cp $MID_HOME/prim/plot/libsrc/getinp-$MYNO.fc $MID_HOME/prim/plot/libsrc/getinp.fc

cp $MID_HOME/libsrc/plot/getaxs-$MYNO.fc $MID_HOME/libsrc/plot/getaxs.fc
cp $MID_HOME/libsrc/plot/pta-$MYNO.fc $MID_HOME/libsrc/plot/pta.fc
cp $MID_HOME/libsrc/plot/ptb-$MYNO.fc $MID_HOME/libsrc/plot/ptb.fc
cp $MID_HOME/libsrc/plot/ptg-$MYNO.fc $MID_HOME/libsrc/plot/ptg.fc
cp $MID_HOME/libsrc/plot/ptk-$MYNO.fc $MID_HOME/libsrc/plot/ptk.fc
cp $MID_HOME/libsrc/plot/pto-$MYNO.fc $MID_HOME/libsrc/plot/pto.fc
cp $MID_HOME/libsrc/plot/ypt-$MYNO.for $MID_HOME/libsrc/plot/ypt.for

cp $MID_HOME/libsrc/agl/yagl-$MYNO.for $MID_HOME/libsrc/agl/yagl.for
cp $MID_HOME/libsrc/agl/fintf-$MYNO.fc $MID_HOME/libsrc/agl/fintf.fc 

cp $MID_HOME/libsrc/idi/cidi/iic-$MYNO.fc $MID_HOME/libsrc/idi/cidi/iic.fc 
cp $MID_HOME/libsrc/idi/cidi/iid-$MYNO.fc $MID_HOME/libsrc/idi/cidi/iid.fc 
cp $MID_HOME/libsrc/idi/cidi/iig-$MYNO.fc $MID_HOME/libsrc/idi/cidi/iig.fc 
cp $MID_HOME/libsrc/idi/cidi/iii-$MYNO.fc $MID_HOME/libsrc/idi/cidi/iii.fc 
cp $MID_HOME/libsrc/idi/cidi/iil-$MYNO.fc $MID_HOME/libsrc/idi/cidi/iil.fc 
cp $MID_HOME/libsrc/idi/cidi/iim-$MYNO.fc $MID_HOME/libsrc/idi/cidi/iim.fc 
cp $MID_HOME/libsrc/idi/cidi/iir-$MYNO.fc $MID_HOME/libsrc/idi/cidi/iir.fc 
cp $MID_HOME/libsrc/idi/cidi/yiic-$MYNO.for $MID_HOME/libsrc/idi/cidi/yiic.for 
cp $MID_HOME/libsrc/idi/cidi/yiid-$MYNO.for $MID_HOME/libsrc/idi/cidi/yiid.for 
cp $MID_HOME/libsrc/idi/cidi/yiig-$MYNO.for $MID_HOME/libsrc/idi/cidi/yiig.for 
cp $MID_HOME/libsrc/idi/cidi/yiii-$MYNO.for $MID_HOME/libsrc/idi/cidi/yiii.for 
cp $MID_HOME/libsrc/idi/cidi/yiim-$MYNO.for $MID_HOME/libsrc/idi/cidi/yiim.for 
cp $MID_HOME/libsrc/idi/cidi/yiir-$MYNO.for $MID_HOME/libsrc/idi/cidi/yiir.for 


cp $MID_HOME/prim/display/libsrc/f2cdsp-$MYNO.fc $MID_HOME/prim/display/libsrc/f2cdsp.fc 
cp $MID_HOME/prim/display/libsrc/yf2cdsp-$MYNO.for $MID_HOME/prim/display/libsrc/yf2cdsp.for 

cp $MID_HOME/prim/general/libsrc/f2cgen-$MYNO.fc $MID_HOME/prim/general/libsrc/f2cgen.fc 
cp $MID_HOME/prim/general/libsrc/f2cgena-$MYNO.fc $MID_HOME/prim/general/libsrc/f2cgena.fc 
cp $MID_HOME/prim/general/libsrc/extrco-$MYNO.fc $MID_HOME/prim/general/libsrc/extrco.fc 
cp $MID_HOME/prim/general/libsrc/clefra-$MYNO.fc $MID_HOME/prim/general/libsrc/clefra.fc 
cp $MID_HOME/prim/general/libsrc/vecsubs-$MYNO.fc $MID_HOME/prim/general/libsrc/vecsubs.fc 
cp $MID_HOME/prim/general/libsrc/stvals-$MYNO.fc $MID_HOME/prim/general/libsrc/stvals.fc 
cp $MID_HOME/prim/general/libsrc/zmstat-$MYNO.fc $MID_HOME/prim/general/libsrc/zmstat.fc 
cp $MID_HOME/prim/general/libsrc/yf2cgen-$MYNO.for $MID_HOME/prim/general/libsrc/yf2cgen.for 
cp $MID_HOME/prim/general/libsrc/yextrco-$MYNO.for $MID_HOME/prim/general/libsrc/yextrco.for 
cp $MID_HOME/prim/general/libsrc/yvecsubs-$MYNO.for $MID_HOME/prim/general/libsrc/yvecsubs.for 
cp $MID_HOME/prim/general/libsrc/ystvals-$MYNO.for $MID_HOME/prim/general/libsrc/ystvals.for 

cp $MID_HOME/contrib/iue/libsrc/istiue-$MYNO.fc  $MID_HOME/contrib/iue/libsrc/istiue.fc 
 
exit 0
