		MIDAS release 94NOV for Linux 1.0.8
		===================================

Contents of this directory:

README.linux		# This file
Hardware.HOWTO.Z	# Linux Hardware Compatiblity List.  01.Aug.93
linux.faq.Z		# Frequently Asked Questions about Linux. 03.Jan.94
gui			# directory with GUIs for MIDAS, already compiled and
			# linked with the static Motif library. 
verify.tar.z		# MIDAS verification procedures (optional)
94NOVpl0.tar.z		# MIDAS 94NOV patch level 0, for Linux 1.0.8
			# already installed and without sources (7.6 Mb).
[00-05].94NOVpl0.tar.z	# Six 1.4-Mb. chunks from the previous file.
tiff.tar.Z 		# TIFF package to convert an image file in TIFF format 
			# into a MIDAS (.bdf) file.
================================================================================

Installing MIDAS on PC/Linux:


- Minimum Hardware required to run MIDAS:

	- 486 CPU, but probably also works on 386.
	- Minimun of 8 Mbytes of RAM, but 16 Mbytes is recommended.
	- 30 Mbytes of disk for MIDAS.
	- VGA, or Super-VGA monitor.
	- Linux 1.0.8 or higher.

- Retrieve the binary files of the compressed-tar release of MIDAS:

    - Create the HOME directory for MIDAS code <midas_dir>. 

    - Go to the HOME directory, <midas_dir>

	% cd <midas_dir>

    - From ftp:

	ftp> bin
	ftp> get 94NOVpl_.tar.z

  	Or if this file is to big for a single transfer, 
	try the splitted ones:

	ftp> prompt
	ftp> mget ??.94NOVpl_.tar.z

    - From disquettes:
	
	% tar xvf /dev/fd0		

- Re-assemble the original 94NOVpl_.tar.z file from the chunks (which
  conventiently fit on one 1.44 Mybte floppy each):

	% cat ??.94NOVpl_.tar.z > 94NOVpl_.tar.z
	% rm ??.94NOVpl_.tar.z

- Rename or remove a previous release of MIDAS previously installed with
  the same name 94NOV.

- Decompress and untar MIDAS (~27 Mbytes):

	% zcat 94NOVpl_.tar.z | tar xf -
	% rm 94NOVpl_.tar.z

  Do the same with all other .tar.z files that you wish to use.

- Make MIDAS front-end scripts available:

   As a normal user, you can make the following  aliases, and you can include
   them in your .profile or equivalent:
	% alias inmidas=<midas_dir>/94NOV/system/unix/inmidas
	% alias gomidas=<midas_dir>/94NOV/system/unix/gomidas
	% alias helpmidas=<midas_dir>/94NOV/system/unix/helpmidas
    
   Or as superuser:
	% cp <midas_dir>/94NOV/system/unix/inmidas /usr/local/bin
	% cp <midas_dir>/94NOV/system/unix/gomidas /usr/local/bin
	% cp <midas_dir>/94NOV/system/unix/helpmidas /usr/local/bin

- As superuser:
 
        # ln -s <midas_dir> /midas

  Edit the file /etc/ld.so.conf and add the line "/midas/94NOV/lib" at the
  end of the file:
	
	# cd /etc
	# vi ld.so.conf
	...	
	# /sbin/ldconfig

- Make MIDAS man-pages available:
  As superuser:

	# cp <midas_dir>/system/unix/*.l /usr/man/manl

- To run MIDAS, simply type:

	% inmidas		(to start a MIDAS session)
	% gomidas    		(to resume a previous stopped MIDAS session)

- Demo and tutorial data can be obtained in the "/midaspub/demo" subdirectory.
  For on-line help, simply type HELP in response to the MIDAS prompt.

- Midas documentation in dvi and PostScript format can be obtained in
  the "/midaspub/94NOV/doc" directory.

- Verification procedures for MIDAS are available in the file 
  "verify94NOVpl_.tar.z",
  To run the verification procedures, type:

	% cd <midas_dir>
	% zcat verify.tar.z | tar xf -
        % mkdir tmp
	% cd tmp
	% inmidas 
	Midas 001> @ vericopy
   to execute all of them:
	Midas 002> @@ veriall
   to execute one bye one:
	Midas 003> @@ verify1
	Midas 003> @@ verify3
	Midas 003> @@ verify4
	Midas 004> @@ verify5
	Midas 005> @@ verify6
	Midas 006> @@ verify7
	Midas 007> @@ verifydio
	Midas 008> @@ verifyt1
	Midas 009> @@ verifyt2
	% cd ..
	% rm -rf tmp

- To remove unnecessary MIDAS packages:

	% cd <midas_dir>/94NOV/install/unix
	% cleanm 
	               MIDAS DELETE MENU:
        ====================================
        1 - Delete only object files.
        2 - Delete object and source files.
        3 - Delete object, source files and libraries.
        4 - Delete executable files.
        q - Quit.

	Select: 4
	        DELETE EXECUTABLES FILES FROM:

	<here a list of MIDAS packages>
	Type <Q> to quit or <L> to list again.
	Select NAME to DELETE: <packagename>

- Optional: Graphical User Interfaces for MIDAS with static Motif library.

  The binaries contains already the GUIs using Motif shared library. If
  your system does not have the Motif shared library (libXm.so) the MIDAS
  GUIs will not work. You can however retrieve the MIDAS GUIs with the
  Motif library linked statically (thus, you do not need the shared library,
  but their size is rather big):

	% cd <midas_dir>
	% zcat <gui>.tar.z | tar xmvf -
	% rm <gui>.tar.z

- Send your comments and/or verification form and/or problem reports to 
  "midas@eso.org".

- Best of success an enjoy MIDAS!.

- PS. The command "finger midas@ftphost.hq.eso.org" will give you the latest
      news about MIDAS and its development.
