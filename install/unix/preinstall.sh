#! /bin/bash 
# .TYPE           command
# .NAME           preinstall.sh 
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under Bourne shell 
# .COMMENTS      
#                 Usage: preinstall 
#
#	The preinstall command will :
#
#	a) Copy files from:
#
#	      $MID_INSTALL/install/unix/systems/<system>
#			to
#	      $MID_HOME/local
#                 
#
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    881023:	Implementation
# .VERSION 1.3    910424:	Removing first argument
# .VERSION 2.1    910725:	Much easier.
# .VERSION 2.2    910917:	Much easier yet.
# .VERSION 3.2    930727:	Using output of uname command.
# .VERSION 3.3    021028:	Using -a argument for all packages
# 
# 120420	last modif

echo=echo
if [ "$1" != "-a" ]; then
  if [ "`echo -n`" = "-n" ] ; then
    SV_NONL="\c"
  else
    echo="echo -n"
  fi
fi

#
# First of all, goto the config directory 
# <dirname> & <basename> commands emulated with <sed>
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

if [ ! -d $MID_HOME/local ]; then
  mkdir $MID_HOME/local
fi

if [ ! -d $MID_HOME/pipeline ]; then
  mkdir $MID_HOME/pipeline
  chmod 775 $MID_HOME/pipeline
fi

if [ -f $MID_HOME/local/default.mk ]; then
  rm -f $MID_HOME/local/default.mk~
  mv $MID_HOME/local/default.mk  $MID_HOME/local/default.mk~
fi

if [ -f $MID_HOME/local/preinstall ]; then
  mv $MID_HOME/local/preinstall $MID_HOME/local/preinstall~
fi

while :
do
  echo ""
  echo "Your current definitions:"
  echo "------------------------"
  echo "MIDASHOME=$MIDASHOME"
  echo "MIDVERS=$MIDVERS"
  echo ""
  $echo "Do you want to modify these definitions [yn]? (n): " $SV_NONL
  if [ "$1" != "-a" ]; then
    read answ
  fi
  if [ -z "$answ" ]; then
    break
  fi
  if [ "$answ" = "n" -o "$answ" = "N" ]; then
    break
  fi
  while :
  do
    $echo "MIDASHOME: " $SV_NONL
    read MIDASHOME
    if [ ! -d $MIDASHOME ]; then
      echo "No such directory: $MIDASHOME"
      continue
    else 
      break
    fi
  done
  while :
  do
    $echo "MIDVERS: " $SV_NONL
    read MIDVERS
    if [ ! -d $MIDASHOME/$MIDVERS ]; then
    echo "No such directory: $MIDASHOME/$MIDVERS"
      continue
    else
      break
    fi
  done
done

(echo "MIDASHOME=$MIDASHOME"; echo "MIDVERS=$MIDVERS" ) | cat - default_mk \
	> $MIDASHOME/$MIDVERS/local/default.mk
echo "File $MIDASHOME/$MIDVERS/local/default.mk created."

cd systems
uname=`(uname) | sed 's/\//-/g' 2>/dev/null`
unode=`(uname -n) 2>/dev/null`
urels=`(uname -r) 2>/dev/null`
machine=`(uname -m) 2>/dev/null`

if [ -d "Debian" -a -f "/etc/debian_version" ];  then
    system="Debian"

elif [ "$uname" = "Darwin" ]; then
    system="Darwin"

elif [ "$machine" = "x86_64" ]; then
    system="Linux_AMD64"

elif [ -d "${uname}_$urels" ]; then
  system="${uname}_$urels"

elif [ -d "${uname}_$machine" ]; then
  system="${uname}_$machine"

elif [ -d "$uname" ]; then
    system="$uname"

elif [ -d "$unode" ]; then
    system="$unode"

else
   uname -o  2>/dev/null		#not all systems support -o option
   exitstat=$?
   if [ $exitstat = 0 ]; then
      temp=`uname -o`
      if [ -d "$temp" ]; then
         system="$temp"
      fi
   fi
fi

if [ -z "$system" ]; then
  echo ""
  while :
  do
    echo "Select your system (or q to QUIT) :"
    echo ""
    for i in `ls`
    do
	if [ -d $i ]
	then
	     echo "		$i"
	fi
    done	
    $echo "System: " $SV_NONL
    read system
    if [ "$system" = "q" -o "$system" = "Q" ]; then
	exit 0
    fi
    if [ ! -d "$system" ]; then
	echo "No such directory: $system"
    else
	break
    fi
  done
else
   echo system chosen: $system
fi

echo ""
echo "All files from the <systems/$system> directory will be copied"
echo "into your <$MID_HOME/local> directory"
$echo "Do you want to continue [yn]? (y): " $SV_NONL
if [ "$1" != "-a" ]; then
  read answ
fi
if [ -z "$answ" ]; then
    answ=y
fi
if [ "$answ" = "n" -o "$answ" = "N" ]; then
    exit 0
fi

# 
# Choose libsrc/ftoc according to $system.
# 
rm -rf $MIDASHOME/$MIDVERS/libsrc/ftoc
ln -s $MIDASHOME/$MIDVERS/libsrc/ftoc-new $MIDASHOME/$MIDVERS/libsrc/ftoc

# 
# Using the script copy instead of cpio, which not always exists.

cd $system

# Option prune does not exist in Ultrix. Anaway SCCS is never included in tape
#find . ! \( -name "SCCS" -prune \) -print -exec ../../copy {} \;
#find . -print -exec ../../copy {} \;
# CG. For some obscure reasons "-print" has to be at the end on IBM6000

find .  -exec ../../copy {} \; -print

if [ -f "$MID_HOME/local/README" ] ; then
    cat $MID_HOME/local/README
    $echo "Type <RETURN> to continue...." $SV_NONL
    if [ "$1" != "-a" ]; then
      read key
    fi
fi

#
# CG 15.02.2017 Edit make_options with g77 only if g77 exists
#
which g77 >/dev/null 2>&1
if [ $? -eq 0 ] ; then
  if [ ! -f $MID_HOME/local/make_options ]; then
    touch $MID_HOME/local/make_options
  else
    G77_F2C_PATH=`g77 -print-libgcc-file-name`
    G77_F2C_PATH=`dirname $G77_F2C_PATH`
    echo "F2C_LIBS=-L${G77_F2C_PATH} -lg2c" >> $MID_HOME/local/make_options
  fi
fi

#
# CG 02.05.2011 Edit make_options with gfortran only if gfortran exists
#
which gfortran >/dev/null 2>&1
if [ $? -eq 0 ] ; then
  if [ ! -f $MID_HOME/local/make_options ]; then
    touch $MID_HOME/local/make_options
  else
    gccversion=`$MID_INSTALL/getvers`
    if [ "$gccversion" = "gfortran" ] ; then
       sed -e 's/g77/gfortran/' $MID_HOME/local/make_options  > $MID_INSTALL/maky
       mv $MID_INSTALL/maky $MID_HOME/local/make_options
    fi
  fi
fi

echo ""
echo "Your MAKE_OPTIONS after pre-install:"
echo " <$MID_HOME/local/make_options>"
awk -F# '{if ($1 != "") {printf "\t  %s\n",$1} }' $MID_HOME/local/make_options

echo ""
echo "Preinstallation of MIDAS completed."
$echo "Type <RETURN> to continue...." $SV_NONL
if [ "$1" != "-a" ]; then
  read key
fi
#
# Check if there is a preinstall in the local directory
#
if [ -f "$MID_HOME/local/preinstall" ] ; then
    /bin/bash $MID_HOME/local/preinstall
fi

exit
