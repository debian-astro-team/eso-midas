
#!/usr/bin/awk
#
# getMacInfo.awk
#
# K. Banse      ESO - Garching  110714
# 110714        last modif
# 
# 

BEGIN {
newfile = "mac_system"
IGNORECASE = 0;
}


# look for the system software section

/^Software:/ {
getline;                #skip to next line		#first blank line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line
print  $0 >> newfile
getline;                #skip to next line		#last blank line
print  $0 >> newfile
}

END {
# printf "system info extracted to file mac_system\n";
}

