#! /bin/bash
# .COPYRIGHT: Copyright (c) 1988-2011 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           setup.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       
#                 Usage: setup
# .REMARKS        The external variable $MIDASHOME and $MIDVERS must be set 
#
# .AUTHOR         Carlos Guirao
# .VERSION 3.2	  910213        Implementation
# .VERSION 3.3	  910929        Now also mail address for MIDAS manager.
# .VERSION 3.4	  911005        IDIDEV is changed without using s/RE/re/number
#				This feature does not exist for ed-Ultrix
# .VERSION 4.1    920119	MID$SYS (31:40) name of editor to use.
# .VERSION 4.2    920119	MID$SYS (31:40) by default "vi".
# .VERSION 4.3    920121	Using "ex" instead of "ed" (for PC/Linux)
# .VERSION 4.4    990324	Checking EDITOR (default ex, otherwise ed)
# 
# 111116	last modif KB
#
# Check how echo command removes new-line.
#
cmd=`basename $0`
echo=echo
if [ "$1" != "-a" ]; then 
  if [ "`echo -n`" = "-n" ] ; then
    SV_NONL="\c"
  else
    echo="echo -n"
  fi
fi

#
# Check if 'ex' editor exists otherwise use 'ed'
# "ed" is substituted by "ex" for PC/Linux except in SuSE
#
EDITOR=ex
which ex
if [ $? != 0 ]; then
   EDITOR=ed
fi

#
# First of all, goto the config directory adn
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
  cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
  MID_INSTALL=`pwd`
  VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
  MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
  MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
  cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

#
# Reading local variables from local/setup
#
if [ -f $MIDASHOME/$MIDVERS/local/setup ]; then
  echo "Reading your local setup $MIDASHOME/$MIDVERS/local/setup..."
  echo ""
  . $MIDASHOME/$MIDVERS/local/setup		#setting host_os, debugger, ...
fi

#
# Checking existence of syskeys.unix
#
if [ ! -f $MIDASHOME/$MIDVERS/monit/syskeys.unix ]; then
  cd $MIDASHOME/$MIDVERS/monit
  make syskeys.unix >/dev/null 2>&1
  cd $MIDASHOME/$MIDVERS/install/unix
fi

while :
do
  if [ -z "$host_os" ]; then
    $echo "Name of the computer/system. (Max 20 char., e.g. IBM/AIX): " $SV_NONL
    read host_os
    if [ -z "$host_os" ] ; then
	continue
    fi
  fi
  host_os=`echo $host_os | \
    sed -e 's/$/                    /' \
	-e 's/^\(....................\).*$/\1/'`
  echo "Name of the computer/system: $host_os"
  $echo "OK? [yn] (y): " $SV_NONL
  if [ "$1" != "-a" ]; then   
    read answ
  fi
  if [ -z "$answ" ]; then
    answ=y
  fi
  if [ "$answ" = "y" -o "$answ" = "Y" ]; then
    break
  fi
  unset host_os
done


# Editor to be used, set by default to vi
editor="vi"
editor=`echo $editor | sed -e 's/$/         /' -e 's/^\(.........\).*$/\1/'`

while :
do
  if [ -z "$ps_print" ]; then
    echo "Command to print a PostScript file in a PostScript printer."
    $echo "(Max 20 char., e.g. lpr -h -P): " $SV_NONL
    read ps_print
    if [ -z "$ps_print" ] ; then
	continue
    fi
  fi
  ps_print=`echo $ps_print | \
    sed -e 's/$/                    /' \
	-e 's/^\(....................\).*$/\1/'`
  echo "Command to print a PostScript file: $ps_print"
  $echo "OK? [yn] (y): " $SV_NONL
  if [ "$1" != "-a" ]; then   
    read answ
  fi
  if [ -z "$answ" ]; then
    answ=y
  fi
  if [ "$answ" = "y" -o "$answ" = "Y" ]; then
    break
  fi
  unset ps_print
done

while :
do
  if [ -z "$ascii_print" ]; then
    echo "Command to print an ASCII file in a PostScript printer."
    $echo "(Max 20 char., e.g. lpr -h -P): " $SV_NONL
    read ascii_print
    if [ -z "$ascii_print" ] ; then
	continue
    fi
  fi
  ascii_print=`echo $ascii_print | \
    sed -e 's/$/                    /' \
	-e 's/^\(....................\).*$/\1/'`
  echo "Command to print an ASCII file: $ascii_print"
  $echo "OK? [yn] (y): " $SV_NONL
  if [ "$1" != "-a" ]; then   
    read answ
  fi
  if [ -z "$answ" ]; then
    answ=y
  fi
  if [ "$answ" = "y" -o "$answ" = "Y" ]; then
    break
  fi
  unset ascii_print
done

while :
do
  echo "Email of the MIDAS administrator at your site (problem-report service)"
  $echo "(Max 40 char., default: midas@eso.org): " $SV_NONL 
  if [ "$1" != "-a" ]; then 
    read mmail
  fi
  if [ -z "$mmail" ] ; then
    mmail="midas@eso.org"
  fi
  mmail=`echo $mmail | \
    sed -e 's/$/                                        /' \
        -e 's/^\(........................................\).*$/\1/'`
  echo "Email address: $mmail"
  $echo "OK? [yn] (y): " $SV_NONL
  if [ "$1" != "-a" ]; then 
    read answ
  fi
  if [ -z "$answ" ]; then
    answ=y
  fi
  if [ "$answ" = "y" -o "$answ" = "Y" ]; then
    break
  fi
  unset mmail
done

#echo host_os=$host_os, debugger=$debugger, editor=$editor, mail=$mmail

$echo "Editing $MIDASHOME/$MIDVERS/monit/syskeys.unix... " $SV_NONL

$EDITOR $MIDASHOME/$MIDVERS/monit/syskeys.unix <<EOF >/dev/null 2>&1
1
/^AUX_MODE/

s/^[^,]*,/2,/

1
/^MID\$SYS/

i
$host_os\$$editor
.

s/^/C /p
1
/^SYSCOMS/

s/........................................\(.*\)$/${ps_print}$ascii_print\1/

1
/^MID\$DEBUG/

s/\$dbx/\$$debugger/

1
/^MID\$MAIL/

i
$mmail
.

s/^/C /p
w
q
EOF
echo "Done."

#
# Set default MIDASHOME0 and MIDVERS0 to current values
#
MIDASHOME0=$MIDASHOME
MIDVERS0=$MIDVERS

#

FILE0=$MIDASHOME/$MIDVERS/system/unix/inmidas
FILE1=$MIDASHOME/$MIDVERS/system/unix/helpmidas
FILE2=$MIDASHOME/$MIDVERS/system/unix/drs

if [ ! -f $FILE0 ]; then
  cd $MIDASHOME/$MIDVERS/system/unix
  make >/dev/null 2>&1
  cd $MIDASHOME/$MIDVERS/install/unix
fi

unset answ
MIDASHOME0=$MIDASHOME
MIDVERS0=$MIDVERS
while :
do
  echo ""
  echo "Default definitions for <inmidas> script:"
  echo "-------------------------------------------"
  echo "MIDASHOME=$MIDASHOME0"
  echo "MIDVERS=$MIDVERS0"
  echo ""
  $echo "Do you want to modify these values [yn]? (n): " $SV_NONL
  if [ "$1" != "-a" ]; then   
    read answ
  fi
  if [ -z "$answ" ]; then
    break
  fi
  echo answ="$answ"
  if [ "$answ" = "n" -o "$answ" = "N" ]; then
    break
  fi
  while :
  do
    $echo "MIDASHOME: " $SV_NONL
    read MIDASHOME0
    if [ ! -d $MIDASHOME ]; then
      echo "No such directory: $MIDASHOME"
      continue
    else
      break
    fi
  done
  while :
  do
    $echo "MIDVERS: " $SV_NONL
    read MIDVERS0
    if [ ! -d $MIDASHOME/$MIDVERS ]; then
      echo "No such directory: $MIDASHOME/$MIDVERS"
      continue
    else
      break
    fi
  done
done

#
# NOTE: This could be done easier with ed, but in UNIX SYSTEM V
# ed exits when a pattern is not found.
#
MIDASHOME_0=`echo MIDASHOME0=$MIDASHOME0`
MIDVERS_0=`echo MIDVERS0=$MIDVERS0`
MIDASHOME_0_set="set $MIDASHOME_0"
MIDVERS_0_set="set $MIDVERS_0"

#
# IFS: To cancel space as argument separator
#
IFS=''

for file in $FILE0 $FILE1 $FILE2
do
echo "Editing $file ..."
cat $file | sed  \
        -e 's!^MIDVERS0=.*!'$MIDVERS_0'!' \
        -e 's!^set MIDVERS0=.*!'$MIDVERS_0_set'!' \
        -e 's!^MIDASHOME0=.*!'$MIDASHOME_0'!' \
        -e 's!^set MIDASHOME0=.*!'$MIDASHOME_0_set'!' \
> $file.t
chmod +xr $file.t
mv -f $file.t $file
done
echo "Done."
echo ""
echo "**********************************************************************"
echo "Copy the following files to a public directory included in \$PATH"
echo "(e.g. /usr/local/bin)"
echo "  cp $MIDASHOME/$MIDVERS/system/unix/inmidas      /usr/local/bin"
echo "  cp $MIDASHOME/$MIDVERS/system/unix/gomidas      /usr/local/bin"
echo "  cp $MIDASHOME/$MIDVERS/system/unix/helpmidas    /usr/local/bin"
echo "  cp $MIDASHOME/$MIDVERS/system/unix/drs          /usr/local/bin"
echo ""
echo "Copy the following man-pages to a directory included in \$MANPATH"
echo "(e.g. /usr/man/manl)"
echo "  cp $MIDASHOME/$MIDVERS/system/unix/manl/inmidas.l    /usr/man/manl"
echo "  cp $MIDASHOME/$MIDVERS/system/unix/manl/gomidas.l    /usr/man/manl"
echo "  cp $MIDASHOME/$MIDVERS/system/unix/manl/helpmidas.l  /usr/man/manl"
echo "  cp $MIDASHOME/$MIDVERS/system/unix/manl/readline.l   /usr/man/manl"
echo " or modify \$MANPATH to include $MIDASHOME/$MIDVERS/system/unix/manl"
echo ""
echo "To run MIDAS, type:"
echo "  inmidas         (if you want to run MIDAS defaults)"
echo "  gomidas         (to restart the previous MIDAS session)"
echo "  helpmidas       (standalone GUI help for MIDAS)"
echo "  drs <midas_command> (execute MIDAS command from Unix command line)"
echo "**********************************************************************"
echo "Type any character to continue." 
if [ "$1" != "-a" ]; then   
  read answ
fi

# 
# Send mail to esomidas@eso.org
#
#cd $MIDASHOME/$MIDVERS/local
#uname=`(uname -a) 2>/dev/null`
#(echo "MIDAS installed on $uname" | cat - setup make_options | mail esomidas@eso.org) >/dev/null 2>&1 &
#
exit
