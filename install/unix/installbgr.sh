#! /bin/bash
# .TYPE           command
# .NAME           installbgr.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 the scratch.
#                 
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    880624:		Implementation
# .VERSION 1.2    910219:		Removing NAME option
# .VERSION 2.1    910724:		Output to $MIDASHOME/$MIDVERS/tmp
# 111206	last modif
# 
if [ $# -gt 1 ]
then
	echo "Use: install.bgr [MAKE_OPTIONS]"	
	exit
fi

#
# If $MID_HOME/tmp does not exists then creates it.
#
if [ ! -d $MID_HOME/tmp ]; then
    mkdir $MID_HOME/tmp
fi

time /bin/bash ./install $1 > $MIDASHOME/$MIDVERS/tmp/install.$$ 2>&1 &
echo "Running install in background"
echo "Results are coming in $MIDASHOME/$MIDVERS/tmp/install.$$"

exit 0
