#! /bin/bash
# .COPYRIGHT: Copyright (c) 1988-2011 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           install.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 scratch.
#                 
#                 Usage: install [options]
#                 
#                    options: are options for the "make" command. This option
#                             will be exported to all the directories of the
#                             installation procedure.
#			      Multiple definitions for keywords must be:
#				e.g.	CFLAGS=\"-O -g -I/usr/include\"
#			      to avoid quotes to be interpreted by the shell.
#
#                 The command will execute the following steps:
#
#                    - $MID_HOME/local/make_options is read if exists.
#		       It contains local options for "make" commands.
#                        
#		     - MIDAS is structured in "core" (all directories 
#			in file "core.cnf" are included in the installation).
#			and "packages" (optional, they are clasified in 
#			"applic", "stdred" and "contrib".) A package is 
#			selected for installation if it is marked with file
#			"SELECTED".
#
#                    - All the directories involved in the installation are
#                      cleaned by running "make options clean" command. 
#
#                    - Run "make options" in all the directories.
#
# .AUTHOR         Carlos Guirao
# .VERSION 5.1    910725:	New implementation.
# .VERSION 5.2    910827:	Check for file SELECTED when installing package 
# 
# 111116	last modif

umask 002
#
# First of all, goto the config directory MID_INSTALL
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS


echo "*********************************************************************"
echo "******************** MIDAS INSTALLATION PROCEDURE *******************"
echo "*********************************************************************"
echo "******************** MIDASHOME: $MIDASHOME  VERSION: $MIDVERS"
echo "******************** DATE:  `date`"
echo "******************** HOSTNAME: `hostname`"
echo "******************** COMMAND: $0 $*"
echo ""

export OURHOST=$(uname)
			   #for Mac we save internal system info in a file
if [ "$OURHOST" = "Darwin" ]; then
   echo "******************** System used (output from system_profiler):"
   system_profiler > $MID_HOME/tmp/mac_info
   cd $MID_HOME/tmp
   awk -f $MID_INSTALL/getMacInfo.awk ./mac_info
   cat ./mac_system

else
   echo "******************** System used (output from uname -a):"
   uname -a
fi

echo ""
echo "******************** C compiler used (output from cc -v):"
cc -v
echo ""
if [ -f /proc/cpuinfo ]; then
   cat /proc/cpuinfo
fi

#
# Local options for making commands will be read if they exist in the
# $MID_HOME/local/make_options file.
# Other options can be set as arguments in the command line of this script 
#
# These options will replace in $MID_HOME/local/make_options file 
# those definitions with the same name:
# Multiple definitions must be between \" e.g. CFLAGS=\"-O -g -I/local/incl\"
#
if [ -r $MID_HOME/local/make_options ]
then
    echo ""
    echo "*************************************************"
    echo "********* READING YOUR LOCAL OPTIONS ************"
    echo "*************************************************"
    echo "********* Date:  `date`"
    echo ""
    cat $MID_HOME/local/make_options | awk -F# '{print $1}'
    echo ""
fi

echo ""
echo "*************************************************"
echo "********* REMOVING DEPENDENT FILES **************"
echo "*************************************************"
echo "********* DATE:  `date`"
for dir in `awk -F# '{ print $1 }' $MID_INSTALL/core.cnf`
do
    echo ""
    echo "**********************************************"
    echo "****** REMOVING FILES IN $dir"
    echo "**********************************************"
    if [ ! -d $MID_HOME/$dir ]; then
	echo "Warning. $dir .No such directory."
	continue
    fi
    cd $MID_HOME/$dir
    eval make "$@" clean
done

for categ in applic stdred contrib gui
do
    if [ ! -d $MID_HOME/$categ ]; then
        continue
    fi
    cd $MID_HOME/$categ

    for dir in lib exec proc 
    do
	if [ -f $dir/makefile ]; then
	    echo ""
	    echo "**********************************************"
	    echo "****** REMOVING FILES IN  ./$categ/$dir"
	    echo "**********************************************"
	    cd $dir
	    eval make "$@" clean
	    cd $MID_HOME/$categ
	fi
    done

    for pack in `(ls */SELECTED* | awk -F/ '{print $1}' | sort -u) 2>/dev/null`
    do
        for dir in etc libsrc src proc help
        do
          if [ -f $pack/$dir/makefile ]; then
            echo ""
            echo "**********************************************"
            echo "****** REMOVING FILES IN ./$categ/$pack/$dir"
            echo "**********************************************"
            cd $pack/$dir
            eval make "$@" clean
            cd $MID_HOME/$categ
          fi
        done
    done
done
		
echo ""
echo "*************************************************"
echo "********** INSTALLING........ *******************"
echo "*************************************************"
echo "********** DATE:  `date`"
for dir in `awk -F# '{ print $1 }' $MID_INSTALL/core.cnf`
do
    echo ""
    echo "**********************************************"
    echo "****** INSTALLING IN $dir"
    echo "**********************************************"
    echo "****** DATE:  `date`"
    if [ ! -d $MID_HOME/$dir ]; then
	echo "Warning. $dir .No such directory."
	continue
    fi
    cd $MID_HOME/$dir
    eval make "$@" all
done

for categ in applic stdred contrib gui
do
    if [ ! -d $MID_HOME/$categ ]; then
        continue
    fi
    cd $MID_HOME/$categ
    first_packages=`(ls */SELECTED_BY* | awk -F/ '{print $1}' | sort -u) 2>/dev/null`
    if [ -n "$first_packages" ]; then
      excluded=`echo $first_packages | sed -e 's/^/\^/' -e 's/$/\$/' -e 's/ /\$|\^/g'`
      packages=`(ls */SELECTED | awk -F/ '{print $1}' | egrep -v "$excluded") 2>/dev/null`
    else
      packages=`(ls */SELECTED | awk -F/ '{print $1}') 2>/dev/null`
    fi
    for pack in $first_packages $packages
    do
	for dir in etc libsrc src proc help
	do
	  if [ -f $pack/$dir/makefile ]; then
    	    echo ""
    	    echo "**********************************************"
    	    echo "****** INSTALLING IN ./$categ/$pack/$dir"
    	    echo "**********************************************"
	    cd $pack/$dir
	    eval make "$@" all
	    cd $MID_HOME/$categ
	  fi
	done
    done
done
cd $MID_HOME

echo ""
echo "*********************************************************************"
echo "******************** END INSTALLATION PROCEDURE *********************"
echo "*********************************************************************"
echo "********** END DATE:  `date`"
echo "********** SYSTEM: $SYSTEM"

exit
