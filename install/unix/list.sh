#! /bin/sh
# @(#)list.sh	19.1 (ESO-IPG) 02/25/03 13:51:11
# .COPYRIGHT: Copyright (c) 1988 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           list.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       
#                 Usage: 
#
# .AUTHOR         Carlos Guirao
# .VERSION 2.2    14-Oct-1988:		Cosmetic changes
# .VERSION 2.3    05-Dec-1989:		Adding "echo" definition

echo=echo
if [ "`echo -n`" = "-n" ] ; then
    SV_NONL="\c"
else
    echo="echo -n"
fi

#
# First of all, goto the config directory MID_INSTALL
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

clear
while : ; do
(echo ""
echo "	LIST OF AVAILABLE PACKAGES FOR MIDAS:"
echo ""
echo "	Name		Category   Status"
echo "	========================================"
for categ in applic stdred contrib gui
do
    if [ ! -d $MID_HOME/$categ ]; then
	continue
    fi
    cd $MID_HOME/$categ
    for pack in `ls | egrep -v 'lib|exec|proc'`
    do
        if [ -f $pack/src/makefile -o \
	     -f $pack/libsrc/makefile -o \
	     -f $pack/etc/makefile -o \
	     -f $pack/proc/makefile ] ; then
            $echo  "	$pack      	$categ	"  $SV_NONL
	    if ( ls $pack/SELECTED* | egrep SELECTED ) >/dev/null 2>&1
	    then
		$echo "(selected)	" $SV_NONL
	    else
		$echo "(not selected)	" $SV_NONL
	    fi
            if [ ! -f $pack/DESCRIPTION ] ; then
		echo "(DESCRIPTION not available)"
	    else
		echo ""
	    fi
	fi
   done
done
echo "" ) | more
cd $MID_INSTALL
echo "Type <Q> to quit or <L> to list again."

while : ; do
    $echo "Select NAME: " $SV_NONL
    read name
    if [ "$name" = "q" -o "$name" = "Q" ] ; then
	clear
	exit
    fi
    if [ "$name" = "l" -o "$name" = "L" ] ; then
	break
    fi
    if [ "$name" = "" ] ; then
	continue
    fi
    if [ -d $MID_HOME/applic/$name ] ; then
	categ=applic
    elif [ -d $MID_HOME/stdred/$name ] ; then
	categ=stdred
    elif [ -d $MID_HOME/contrib/$name ] ; then
	categ=contrib
    elif [ -d $MID_HOME/gui/$name ] ; then
	categ=gui
    else
	echo "$name: No such package with that name"
	continue
    fi

    if [ -f $MID_HOME/$categ/$name/DESCRIPTION ] ; then
	cat $MID_HOME/$categ/$name/DESCRIPTION | more
	echo ""
	continue
    else
	echo "DESCRIPTION file for package <$name> not available."
    fi
done
done
clear
exit
