#! /bin/bash
# .COPYRIGHT: Copyright (c) 1988-2011 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           config 
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 scratch.
#                 Usage: config
#
# .REMARKS        The external variable $MIDASHOME and $MIDVERS must be set 
#
# .AUTHOR         Carlos Guirao
# .VERSION 2.2    881014:		Cosmetic changes
# 
# 111206	last modif
#
# Checks echo command which option uses to get NO NEW LINE
#

echo=echo
if [ "`echo -n `" = "-n" ] ; then
	SV_NONL="\c"
else	
	echo="echo -n"
fi

clear

#
# First of all, goto the config directory MID_INSTALL
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
#if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
#else
#    cd $MIDASHOME/$MIDVERS/install/unix
#fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

export MIDASHOME MIDVERS

#
# START
#
if [ ! -f test_file_1k ]; then
  dd if=/dev/zero bs=1024 count=1 of=test_file_1k
fi
echo "**********************************************************************"
echo "******************** MIDAS CONFIGURATION SCRIPT **********************"
echo "**********************************************************************"
echo "******************** MIDVERS: $MIDVERS"
echo "******************** DATE:  `date`"
echo ""

task="unknown"
while true
do
echo ""
echo "		MIDAS CONFIG MENU:"
echo "	========================================"
echo "	1 - list of MIDAS packages available"
echo "	2 - select only MIDAS core"
echo "	3 - select all MIDAS packages"
echo "	4 - select MIDAS core & packages of your choice"
echo "	5 - preinstall MIDAS"
echo "	6 - install MIDAS"
echo "	66 - install MIDAS - prepare own MAKE_OPTIONS"
echo "	67 - install MIDAS - use the MAKE_OPTIONS above"
echo "	7 - update MIDAS"
echo "	8 - setup MIDAS"
echo "	9 - clean MIDAS"
echo "	10- help"
echo "	q - quit"
echo ""

while :
do
    $echo "Select: " $SV_NONL
    read task
    if [ -n "$task" ]; then
	break
    fi
done

task=`echo $task | tr A-Z a-z`

# trap "" 2			# ignore interrupts 
case $task in
	1) /bin/bash $MID_INSTALL/list
	   ;;
	2) /bin/bash $MID_INSTALL/select core
	   ;;
	3) /bin/bash $MID_INSTALL/select all
	   ;;
	4) /bin/bash $MID_INSTALL/select own
	   ;;
	5) /bin/bash $MID_INSTALL/preinstall
	   ;;
	6) /bin/bash $MID_INSTALL/install1 auto
	   ;;
	66) /bin/bash $MID_INSTALL/install1 own
	   ;;
	67) /bin/bash $MID_INSTALL/install1 67
	   ;;
	7) /bin/bash $MID_INSTALL/update1
	   ;;
	8) /bin/bash $MID_INSTALL/setup
	   ;;
	9) /bin/bash $MID_INSTALL/cleanm
	   ;;
	10) /bin/bash $MID_INSTALL/help
	   ;;
	q) exit
	   ;;
	*) echo "Selection unknown"
	   ;;
esac

# trap 2			# obey interrupts again
done
