#! /bin/sh
# .COPYRIGHT: Copyright (c) 1988-2005 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           select.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       
#                 Usage: 
#
# .AUTHOR         Carlos Guirao
# .VERSION 4.1	  910724	New implementation.
# 051129	last modif
# 

cmd=`basename $0`
echo=echo
if [ "`echo -n`" = "-n" ] ; then
    SV_NONL="\c"
else
    echo="echo -n"
fi

#
# First of all, goto the config directory adn
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

#
# Check for first argument. If none select `all' packages.
#
if [ -n "$1" ] ; then
   option=$1
else
   option=all
fi

echo ""
echo ""
echo "	LIST OF AVAILABLE PACKAGES IN MIDAS:"
echo ""

case $option in 
all|core)
   echo "	Category 	Name    	 Status"    
   echo "	========================================"
   ;;
own)
   echo " 	Type [S]elect, [N]o_select, C[hange status], Q[uit] or CR"
   echo "	Category        Name   		 Status		[SNCQ]?" 
   echo "	======================================================="
   ;;
*)
   echo "$option bad argument"
   echo "Usage: $cmd [all|core|own]   (default=all)"
   exit 1
   ;;
esac

trap "stty echo; exit" 2

stty -echo

for categ in applic stdred contrib gui
do
    if [ ! -d $MID_HOME/$categ ]; then
	continue
    fi

    cd $MID_HOME/$categ
    for pack in `ls | egrep -v 'lib|exec|proc|help|resource|Xstuff'`
    do
        if [ "$pack" = "mva" ]; then		#omit the mva package
	    rm -f $pack/SELECTED 2>/dev/null
	    continue
        fi

	if [ -f $pack/src/makefile -o \
	     -f $pack/libsrc/makefile -o \
	     -f $pack/etc/makefile -o \
	     -f $pack/proc/makefile ] ; then
	    $echo  "	$categ     	$pack       	"  $SV_NONL
	    case $option in 
	    all)
		echo "(selected)"
		rm -f $pack/SELECTED 2>/dev/null
                if [ -f $pack/DEPENDENCIES ]; then
                   for depend in `cat $pack/DEPENDENCIES`
                   do
                      rm -f $depend/SELECTED_BY_$pack 2>/dev/null
                      touch $depend/SELECTED_BY_$pack
                   done
                fi
		touch $pack/SELECTED
		continue
		;;
	    core)
		echo "(not selected)"
		rm -f $pack/SELECTED* 2>/dev/null
		continue
		;;
	    esac
	    while :
	    do
	    if [ -f $pack/SELECTED ]; then
		$echo "(selected)	" $SV_NONL
	    else
		$echo "(not selected)	" $SV_NONL
	    fi
	    read answ
	    case $answ in
	    s|S)
		echo "SELECTED"
		rm -f $pack/SELECTED 2>/dev/null
		touch $pack/SELECTED
		break
		;;
	    n|N)
		echo "NOT SELECTED"
		rm -f $pack/SELECTED 2>/dev/null
                if [ -f $pack/DEPENDENCIES ]; then
                   for depend in `cat $pack/DEPENDENCIES`
                   do
                      rm -f $depend/SELECTED_BY_$pack 2>/dev/null
                   done
                fi
		break
		;;
	    c|C)
	        if [ -f $pack/SELECTED ]; then
		    echo "NOT SELECTED"
		    rm -f $pack/SELECTED 2>/dev/null
		else
		    echo "SELECTED"
		    rm -f $pack/SELECTED 2>/dev/null
		    touch $pack/SELECTED
		fi
		break
		;;
	    q|Q)
		echo QUIT
		stty echo
		exit 0
		;;
	    '')
	        if [ -f $pack/SELECTED ]; then
		    echo "SELECTED"
                else
		    echo "NOT SELECTED"
		fi
		break
		;;
	    *)
		echo "[SNCQ]?"
	        $echo  "	$categ     	$pack       	"  $SV_NONL
		;;
	    esac
	    done
		
            if [ -f $pack/DEPENDENCIES ]; then 
		for depend in `cat $pack/DEPENDENCIES`
                do
		  rm -f $depend/SELECTED_BY_$pack 2>/dev/null
                  if [ -f $pack/SELECTED ]; then 
		    touch $depend/SELECTED_BY_$pack
                  fi
                done
            fi
	fi
    done
done
cd $MID_INSTALL
stty echo
exit 0
