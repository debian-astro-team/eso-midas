#!/bin/bash
MIDASHOME=/midas
MIDVERS=$1
LM_LICENSE_FILE=/usr/server/license/license_idl:1726@opus1:1726@opus2:1726@opus3
PATH=$MIDASHOME/bin:/opt/SUNWspro/bin:/usr/bin:/usr/ccs/bin:$MIDASHOME/$MIDVERS/install:$MIDASHOME/$MIDVERS/install/unix:$MIDASHOME/$MIDVERS/install/vms
LD_LIBRARY_PATH=/usr/dt/lib:/usr/openwin/lib:/opt/SUNWspro/lib:/usr/ccs/lib
export MIDASHOME MIDVERS PATH LD_LIBRARY_PATH
export LM_LICENSE_FILE
cd $MIDASHOME/$MIDVERS/install/unix
umask 002
/bin/bash updatebgr -i
