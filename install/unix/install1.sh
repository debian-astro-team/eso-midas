#! /bin/bash
# .COPYRIGHT: Copyright (c) 1988-2011 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           install1.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 scratch.
#                 Usage: install system [options]
#
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    25-Oct-1989:		Implementation
# .VERSION 1.2    05-Dec-1989:		Adding "echo" definition
# .VERSION 1.3    910219:               Removing System name argument
# .VERSION 1.4    911004:               Removing "set -x +x" ignored by some
# .VERSION 1.5    911118:               Checking Fortran and C compiler.
# .VERSION 2.1    920324:               Creating makemidas first.
# .VERSION 3.1    930325:		Using default.mk. CG.
# .VERSION 4.1    940124:		OS library does not require computer.h
# .VERSION 4.2    021028:       	Using -a argument for automatic
# 
# 111116	last modif
  
# the install1 script is called with one parameter 
# from script config (a) or autoconfig (b)
#
# par1 = auto - use automatic (default) compiler checks for setting
#		up /midas/version/local/make_options 
#		and start Midas installation (a)
#      = manual - use make_options file as it is, no further checks,
#		and start Midas installation (a)
#
# or     = -a  - as 'auto' but with no interaction (b)
# 
# this parameter is saved as option INSTALL_FLAG in 
# the file /midas/version/local/make_options

INSTALL_FLAG=$1

echo=echo
if [ "`echo -n`" = "-n" ] ; then
  SV_NONL="\c"
else
  echo="echo -n"
fi

#
# First of all, goto the config directory MID_INSTALL
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS
PATH=$PATH:$MID_INSTALL
export MIDASHOME MIDVERS MID_HOME MID_INSTALL

clear

# ************** make_options file already final *************
if [ $INSTALL_FLAG = "67" ]; then
   export SV_NONL CMND_YES
   $MID_INSTALL/install3 manual
   exit 0
fi
# ************************************************************

if [ ! -f "$MID_HOME/local/default.mk" ] ; then
  echo "*** ERROR: File <$MID_HOME/local/default.mk> does not exist."
  echo "*** You need to run <preinstall> before continuing with <install>"
  exit 1
fi

if [ -f "$MID_HOME/local/make_options" ] ; then
echo "Your current MAKE_OPTIONS:"
  awk -F# '{if ($1 != "") {printf "\t  %s\n",$1} }' $MID_HOME/local/make_options
  echo ""
else
  touch $MID_HOME/local/make_options
fi

chmod +x $MID_HOME/local/make_shared

echo "*** MIDAS INSTALLATION TESTS ***"

#*************** START: Checking existence of make, time commands *********

CMND_YES=1

which time > /dev/null 2>&1
if [  $? = 1 ] ; then
   echo "*** WARNING: Unix command 'time' not found"
   CMND_YES=2
fi

which make > /dev/null 2>&1
if [  $? = 1 ] ; then
   echo "*** ERROR: Unix command 'make' not found"
   CMND_YES=0
fi

if [ $CMND_YES = 0 ] ; then
   echo ">>> Please install the missing command(s), and redo the installation." 
   exit 1
fi
#*************** END: Checking existence of make, time commands *********


# All test modules are in the test directory

cd test
make clean >/dev/null 2>&1


#*************** START: Checking the C compiler ********************
$echo "*** Checking the C compiler...." $SV_NONL
rm -f testc.o 2>/dev/null
if (make testc.o) >/dev/null 2>&1
then
  echo "OK."
else
  CC=`ed_moptions get CC`
  while :
  do 
    echo "NOT FOUND."
    echo ""
    echo "ERROR using CC=$CC (or no 'make' command installed)"
    echo "Type the name of your C compiler,"
    echo "or CR to exit."
    $echo "C compiler: " $SV_NONL
    unset answ
    read answ
    if [ -z "$answ" ]; then
      echo "No C compiler -> No MIDAS. Exit."
      $echo "Type return to exit: " $SV_NONL
      read return
      exit 1
    fi
    CC="$answ"
    ed_moptions delete CC >/dev/null
    $echo "*** Checking the C compiler...." $SV_NONL
    rm -f testc.o 2>/dev/null
    if (make CC="$CC" testc.o)
    then
      echo "OK."
      ed_moptions add CC="$CC" >/dev/null
      break
    fi
  done 
fi

# test, if we include <varargs.h> or <stdarg.h>

rm -f testvar.o 2>/dev/null
if (make testvar.o) >/dev/null 2>&1
then
   varargs=VARARGS
   echo "*** Use <varargs.h>"
else
   varargs=NOVARARGS
   echo "*** Use <stdarg.h>"
fi

# adapt all potential ftoc_xyz files accordingly

rm -f ./temp.out

for  myfile in amd64 alfa alliant hp hpn hpr pc pc3 sun solaris 
do
   incfile=$MID_HOME/incl/ftoc_${myfile}.h
   chmod +w $incfile
   if [ "$varargs" = "VARARGS" ]; then
      sed 's/stdarg.h/varargs.h/' $incfile > ./temp.out
   else
      sed 's/varargs.h/stdarg.h/' $incfile > ./temp.out
   fi
   mv ./temp.out $incfile
done

ed_moptions add "VARARGS=-D$varargs" >/dev/null

# link correct ftoc dir to good-ftoc
# and copy relevant .fc files (+ correct y123.for files)

$MID_INSTALL/install2


#*************** END: Checking the C compiler ********************

#*************** START: Checking the C linker ********************
$echo "*** Checking the C linker...." $SV_NONL
rm -f testc.exe 2>/dev/null
if (make testc.exe) >/dev/null 2>&1
then 
  echo "OK."
else
  LDCC=`ed_moptions get LDCC`
  while :
  do
    echo "NOT FOUND."
    echo ""
    echo "ERROR using LDCC=$LDCC"
    echo "Type the name of your C linker,"
    echo "or CR to exit."
    $echo "C linker: " $SV_NONL
    unset answ
    read answ
    if [ -z "$answ" ]; then
      echo "No C linker no MIDAS. Exit."
      $echo "Type return to exit: " $SV_NONL
      read return
      exit 1
    fi
    LDCC="$answ"
    ed_moptions delete LDCC >/dev/null
    $echo "*** Checking the C linker...." $SV_NONL
    rm -f testc.exe 2>/dev/null
    if (make LDCC="$LDCC" testc.exe) 
    then
       echo "OK."
       ed_moptions add LDCC="$LDCC" >/dev/null
       break
     fi
  done
fi
#*************** END: Checking the C linker *******************

#*************** START: Checking oserror definition *****************
LSYS=""
while :
do
   $echo "*** Checking the MIDAS oserror definition...." $SV_NONL
   rm -f oserror.o oserror.exe 2>/dev/null
   if (make LSYS="$LSYS" oserror.exe) >/dev/null 2>&1
   then
      echo "OK."
      if [ -n "$LSYS" ] ; then
	ed_moptions add SYS="$LSYS" >/dev/null
      fi
      break
   else
      if [ -z "$LSYS" ]; then
        echo "NOT FOUND."
        echo "*** Trying now with LSYS=-DOSERROR_D option:"
        LSYS="-DOSERROR_D"
      else
	echo "NOT FOUND." 
        echo "###############################################################" 
        echo "#                     WARNING                                 #"
        echo "# I could not run the following command:                      #"
        echo "#       make LSYS=-DOSERROR_D oserror.exe                     #"
        echo "#                                                             #"
        echo "# For help contact ESO's Midas support: midas@eso.org         #"
        echo "###############################################################"
	$echo "Type return to exit: " $SV_NONL
	read return
	exit 1
      fi
   fi
done
#*************** END: Checking oserror definition *******************

#************* START: Checking for 32/64bit C compiler *************

cd $MIDASHOME/$MIDVERS/install/unix/test

echo "  "

X_OPT=`ed_moptions get C_OPT`
#echo X_OPT = [$X_OPT]

# if C_OPT=-O ...				skip the "-O" substring
ksize=${#X_OPT}					#length of X_OPT
c2=${X_OPT:0:2}					#extract first 2 chars
#echo c2 = [$c2]
#echo ksize = $ksize

if [ "$c2" = "-O" ]; then			#extract chars from 3rd char. on
   if [ $ksize = 2 ]; then
      X_OPT=" "
   else
      cafter=${X_OPT:2:$ksize}
      X_OPT=$cafter				#shift left over the first 2 chars...
   fi
fi
#echo now X_OPT = [$X_OPT]

ed_moptions delete F77 >/dev/null
ed_moptions delete LD77_CMD >/dev/null
ed_moptions delete E_OPT >/dev/null
ed_moptions delete C_OPT >/dev/null
ed_moptions delete INSTALL_FLAG >/dev/null

ed_moptions add "C_OPT=$X_OPT"  >/dev/null
ed_moptions add "K_OPT= $X_OPT"  >/dev/null
ed_moptions add "INSTALL_FLAG=auto" >/dev/null


rm -f testsiz.exe 2>/dev/null

# here we use already the compiler chosen for Midas
if ! (make testsiz.exe) >/dev/null 2>&1
then
     echo "      "
     echo "could not build testsiz.exe"
     echo "check directory $MID_HOME/install/unix/test !!"
     exit 7
fi

./testsiz.exe
 
#
# 32 bit compiler
if [ $? = 4 ]; then
   echo installing Midas on a 32 bit system
   MACH=" "
   EO=" "
#
# 64 bit compiler
else
   echo installing Midas on a 64 bit system
   MACH=" "
   EO="-Z"
fi
 
ed_moptions add "F77=gfortran $MACH" >/dev/null
ed_moptions add "LD77_CMD=gfortran $MACH $LDFLAGS"  >/dev/null
#ed_moptions add "LDCC=gcc $LDFLAGS"  >/dev/null
ed_moptions add "E_OPT= $EO"  >/dev/null


#if [ $? = 4 ]; then
#   echo installing Midas on a 32 bit system
#   ed_moptions add "F77=gfortran" >/dev/null
#   ed_moptions add "LD77_CMD=gfortran"  >/dev/null
#   ed_moptions add "C_OPT=-O -fPIC $X_OPT"  >/dev/null
#   ed_moptions add "K_OPT=-fPIC $X_OPT"  >/dev/null
#   ed_moptions add "E_OPT= "  >/dev/null
#   ed_moptions add "INSTALL_FLAG=auto" >/dev/null
#else
#   echo installing Midas on a 64 bit system
#   ed_moptions add "F77=gfortran -m64" >/dev/null
#   ed_moptions add "LD77_CMD=gfortran -m64" >/dev/null
#   ed_moptions add 'C_OPT=-m64 -O -fPIC $X_OPT'  >/dev/null
#   ed_moptions add "K_OPT=-m64 -fPIC $X_OPT"  >/dev/null
#   ed_moptions add "E_OPT=-Z"  >/dev/null
#   ed_moptions add "INSTALL_FLAG=auto" >/dev/null
#fi

echo "  "

#*************** END: Checking 32/64bit C compiler ****************


#*************** START: Checking the Fortran compiler ********************
$echo "*** Checking the Fortran compiler...." $SV_NONL
rm -f testf.o 2>/dev/null
if (make testf.o) >/dev/null 2>&1
then
  echo "OK."
else
  F77=`ed_moptions get F77`
  while :
  do 
    echo "NOT FOUND."
    echo ""
    echo "ERROR using F77=$F77"
    echo "Type the name of your Fortran compiler,"
    echo "or CR to exit."
    $echo "Fortran compiler: " $SV_NONL
    unset answ
    read answ
    if [ -z "$answ" ]; then
      echo "No Fortran compiler -> No MIDAS. Exit."
      $echo "Type return to exit: " $SV_NONL
      read return
      exit 1
    fi
    F77="$answ"
    ed_moptions delete F77 >/dev/null
    $echo "*** Checking the Fortran compiler...." $SV_NONL
    rm -f testf.o 2>/dev/null
    if (make F77="$F77" testf.o)
    then
      echo "OK."
      ed_moptions add F77="$F77" >/dev/null
      break
    fi
  done 
fi
#*************** END: Checking the Fortran compiler ********************

#*************** START: Checking the Fortran linker ********************
$echo "*** Checking the Fortran linker...." $SV_NONL
rm -f testf.exe 2>/dev/null
if (make testf.exe) >/dev/null 2>&1
then 
  echo "OK."
else
  LD77_CMD=`ed_moptions get LD77_CMD`
  while :
  do
    echo "NOT FOUND."
    echo ""
    echo "ERROR using LD77_CMD=$LD77_CMD"
    echo "Type the name of your Fortran linker,"
    echo "or CR to exit."
    $echo "Fortran linker: " $SV_NONL
    unset answ
    read answ
    if [ -z "$answ" ]; then
      echo "No Fortran linker no MIDAS. Exit."
      $echo "Type return to exit: " $SV_NONL
      read return
      exit 1
    fi
    LD77_CMD="$answ"
    ed_moptions delete LD77_CMD >/dev/null
    $echo "*** Checking the Fortran linker...." $SV_NONL
    rm -f testf.exe 2>/dev/null
    if (make LD77_CMD="$LD77_CMD" testf.exe) 
    then
       echo "OK."
       ed_moptions add LD77_CMD="$LD77_CMD" >/dev/null
       break
     fi
  done
fi
#*************** END: Checking the Fortran linker *******************

#*************** START: Checking math routines *****************
$echo "*** Checking some standard math routines...." $SV_NONL
rm -f mathc.o mathf.o math.exe 2>/dev/null
## if (make math.exe) >/dev/null 2>&1
if (make math.exe) 
then
  echo "OK."
else
  echo "mathlib NOT FOUND." 
  echo "*** Trying now with -lm option:"
  SLIB_ORG=`ed_moptions delete SLIB`
  SLIB=`eval echo $SLIB_ORG -lm`
  $echo "*** Checking some standard math routines...." $SV_NONL
  rm -f math.exe 2>/dev/null
  if (make SLIB="$SLIB" math.exe) >/dev/null 2>&1
  then
    ed_moptions add SLIB="$SLIB" >/dev/null
    echo "OK."
  else
    ed_moptions add SLIB="$SLIB_ORG" >/dev/null
    echo "mathlib NOT FOUND." 
    echo "###############################################################" 
    echo "#                     WARNING                                 #"
    echo "# I could not run the following commands:                     #"
    echo "#       make math.exe                                         #"
    echo "#       make SLIB=$SLIB math.exe                              #"
    echo "# a) probably the mathematical library libm.a is not found -  #"
    echo "#    edit the file $MID_HOME/local/make_options 	        #"
    echo "#    and set SLIB=<new libm.a path>.                          #" 
    echo "# b) or if this happens on a Mac -                            #"
    echo "#    maybe you have non-matching C compiler (gcc) and         #"
    echo "#    Fortran frontend (gfortran).				#"
    echo "#    E.g. 64bit C compiler and 32bit Fortran, in that case	#"
    echo "#    set in /midas/version/local/make_options:                #"
    echo "#    F77=gfortran -m64  					#"
    echo "#    LD77_CMD77=gfortran -m64  				#"
    echo "#                     					#"
    echo "# For help contact ESO's Midas support: midas@eso.org         #"
    echo "###############################################################"
    $echo "Type return to exit: " $SV_NONL
    read return
    exit 1
  fi
fi
#*************** END: Checking math routines *******************

#*************** START: Checking the ln command *************
ed_moptions delete LN >/dev/null
rm -f aaa bbb 2>/dev/null 
touch aaa
$echo "*** Checking the ln command...." $SV_NONL
if (ln -s aaa bbb ) >/dev/null 2>&1
then
    echo "(soft) OK."
else
    if (ln aaa bbb ) >/dev/null 2>&1
    then
	echo "(hard) OK."
	echo "*** LN set to ln."
        ed_moptions add LN="ln" >/dev/null
    else
	echo "NOT FOUND."
        echo "###############################################################"
        echo "#                     WARNING                                 #"
        echo "# I could neither set the hard nor symbolic link between 	    #" 
        echo "#  files:     ln -s aaa bbb                                   #"
        echo "#      	    ln aaa bbb                                      #"
        echo "# Probably the command is not found or its path not included  #"
        echo "# in the PATH variable. Without this command makefiles in     #" 
        echo "# proc and help directories will fail                         #"
        echo "#                                                             #"
        echo "# For help contact ESO's Midas support: midas@eso.org         #"
        echo "###############################################################"
        $echo "Type return to exit: " $SV_NONL
        read return
	exit 1
    fi
fi
#*************** END: Checking the ln command **************

#*************** START: Checking the strip command *************
ed_moptions delete STRIP >/dev/null
$echo "*** Checking the strip command...." $SV_NONL
before=`du -s testc.exe | awk '{print $1}'`
if (strip  testc.exe) >/dev/null 2>&1
then
   now=`du -s testc.exe | awk '{print $1}'`
   if [ "$before" != "$now" ]; then
      echo "OK."
   else
      echo "NOT STRIPPED."
      echo "*** STRIP set to echo."
      ed_moptions add STRIP=echo >/dev/null
   fi
else
   echo "NOT FOUND."
fi
echo "*** STRIP set to echo."
ed_moptions add STRIP=echo >/dev/null
#*************** END: Checking the strip command **************

#*************** START: Checking the ranlib command ***********
ed_moptions delete RANLIB >/dev/null
$echo "*** Checking the ranlib command...." $SV_NONL
rm -f testlib.a 2>/dev/null
if (make testlib.a) >/dev/null 2>&1
then
   before=`sum testlib.a | awk '{print $1}'`
   sleep 1
   if (ranlib  testlib.a) >/dev/null 2>&1
   then
      now=`sum testlib.a | awk '{print $1}'`
      if [ "$before" != "$now" ]; then
          echo "OK."
      else
          echo "NO EFFECT."
          echo "*** RANLIB set to echo."
   	  ed_moptions replace RANLIB=echo >/dev/null
      fi
   else
      echo "NOT FOUND."
      echo "*** RANLIB set to echo"
      ed_moptions replace RANLIB=echo >/dev/null
   fi
fi
#*************** END: Checking the ranlib command **************

#*************** START: Checking the ncurses development library *********
x11incl=""
$echo "*** Checking ncurses development library...." $SV_NONL

rm -f testncurs.o 2>/dev/null
if (make testncurs.exe) >/dev/null 2>&1
then
  echo "OK."
  ed_moptions replace "EDITSWITCH=-DEdit_dummy" >/dev/null
  ed_moptions replace "EDITLIBS=-lreadline -lncurses" >/dev/null
else
  echo "Not found."
  echo "*** WARNING: You'll not be able to edit Midas commands in the monitor."
  
  unset answ
  if [ "$1" != "-a" ]; then	#interactive mode
    echo "*** If that's o.k., just continue."
    echo "*** Otherwise, quit and install first this development library."
    echo "*** Type 'c' or q' (without ') to continue or quit: " $SV_NONL
    read answ
    if [ -z "$answ" ]; then	#by default continue
       answ="c"
    fi
  else				#always continue for autoconfigure
    answ="c"
  fi

  if [ $answ = "c" -o  $answ = "C" ]; then
    ed_moptions replace "EDITSWITCH=-DNO_READLINE" >/dev/null
    ed_moptions replace "EDITLIBS=-lreadline" >/dev/null
  else
    exit 1
  fi
fi
#
#*************** END: Checking the ncurses development library ***********

#*************** START: Checking the X11 include directory *********
x11incl=""
$echo "*** Checking X11 include directory...." $SV_NONL
#
rm -f testx11.o 2>/dev/null
if (make LSYS=-DONLY_INCLUDES testx11.o) >/dev/null 2>&1
then
  echo "OK."
  x11incl="found"
else
  ed_moptions delete X11INC >/dev/null
  x11inclpth='/usr/include/X11 /usr/include/X11R6 /usr/include/X11R5 /usr/X11R6/include /usr/X11R6/include/X11 /usr/X11R5/include /usr/local/include /include /usr/X/include /opt/X-local/include'
  for dir in $x11inclpth
  do
    if [ ! -d $dir ]; then
      continue
    fi
    X11INC="X11INC=-I$dir"
    rm -f testx11.o 2>/dev/null
    if (make "$X11INC" LSYS=-DONLY_INCLUDES testx11.o) >/dev/null 2>&1
    then 
      echo "OK."
      ed_moptions add "$X11INC" >/dev/null
      x11incl="found"
      break;
    fi
  done
fi
# 
# The X11 include file was not found in $x11inclpth, so ask the operator
# where it could be. 
#
if [ "$x11incl" != "found" ]; then
  echo "NOT FOUND."
  echo ""
  echo "*** WARNING:  X11 include directory not found."
  echo "*** I checked in paths:"
  echo "$x11inclpth"
  while :
  do
    echo "":
    echo "Type the full pathname for the X11 include directory,"
    echo "or CR if you do not have any."
    while :
    do
      $echo "Pathname: " $SV_NONL
      unset answ
      read answ
      if [ -z "$answ" -o -d "$answ" ]; then
	break
      else
	echo "$answ no such directory."
	continue
      fi
    done
    if [ -z "$answ" ]; then
      echo "*** WARNING: MIDAS will be installed without X11."
      break
    fi
    $echo "*** Checking X11 include files...." $SV_NONL
    rm -f testx11.o 2>/dev/null
    answ=`echo $answ | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    X11INC="X11INC=-I$answ"
    if (make "$X11INC" LSYS=-DONLY_INCLUDES testx11.o) 
    then 
      echo "OK."
      ed_moptions add "$X11INC" >/dev/null
      x11incl="found"
      break
    else
      echo "NOT FOUND."
    fi
  done
fi
#*************** END: Checking the X11 include directory ***********

#*************** START: Checking the X11 libraries ***************
# Run this check only if include directory was found previosly
#
if [ "$x11incl" = "found" ]; then
#
  x11lib=""
  $echo "*** Checking X11 libraries...." $SV_NONL
  rm -f testx11.exe 2>/dev/null 
  if (make testx11.exe) >/dev/null 2>&1
  then 
    X11RELEASE=`./testx11.exe`
    echo "OK."
    echo "*** X11 release $X11RELEASE found."
    x11lib="found"
  else
    ed_moptions delete X11_LIBPATH >/dev/null
    x11libpth='/lib /usr/lib /usr/local/lib /usr/openwin/lib /usr/lib/X11R6 /usr/lib/X11R5 /usr/X11R6/lib64 /usr/X11R6/lib /usr/X11R5/lib /usr/lib/Motif1.2 /usr/X/lib /usr/X11/lib /usr/lpp/X11/lib /usr/lib/Motif1.1 /opt/X-local/lib'
    for dir in $x11libpth
    do
      if [ ! -d $dir ]; then
        continue
      fi
      X11_LIBPATH="X11_LIBPATH=-L$dir"
      rm -f testx11.exe 2>/dev/null 
      if (make "$X11_LIBPATH" testx11.exe) >/dev/null 2>&1
      then
	X11RELEASE=`./testx11.exe`
        echo "OK."
        echo "*** X11 release $X11RELEASE found."
        ed_moptions add "$X11_LIBPATH" >/dev/null
        x11lib="found"
        break
      fi
    done
  fi
# 
# The X11/Xt libraries not found in $x11libpth, so ask the operator
# where it could be. 
#
  if [ "$x11lib" != "found" ]; then
    echo "NOT FOUND."
    echo "*** WARNING: X11 library not found."
    echo "*** I checked in paths:"
    echo "$x11libpth"
    while :
    do
      echo ""
      echo "Type the full pathname for the directory containing the X11 library,"
      echo "or CR if you do not have any."
      while :
      do
	$echo "Pathname: " $SV_NONL
        unset answ
	read answ
	if [ -z "$answ" -o -d "$answ" ]; then
	   break
	else
	   echo "$answ no such directory."
	   continue
	fi
      done
      if [ -z "$answ" ]; then
        echo "*** WARNING: MIDAS will be installed without X11."
        break
      fi
      X11_LIBPATH="X11_LIBPATH=-L$answ"
      $echo "*** Checking X11 libraries...." $SV_NONL
      rm -f testx11.exe 2>/dev/null
      if (make "$X11_LIBPATH" testx11.exe) 
      then
	X11RELEASE=`./testx11.exe`
        echo "OK."
        echo "*** X11 release $X11RELEASE found."
	ed_moptions add "$X11_LIBPATH" >/dev/null
	break
      else
	echo "NOT FOUND."
      fi
    done
  fi
fi
#*************** END: Checking the X11 library *******************

if [ "$X11RELEASE" = "4" ]; then  
  MOTIF_RELEASE1="1001"
  MOTIF_RELEASE2="1001"
  MOTIF_RELEASE3="1001"
  MOTIF_RELEASE4="1001"
  MOTIF_RELEASE5="1001"		
  echo "*** Motif release 1.1 ($MOTIF_RELEASE1) expected."
elif [ "$X11RELEASE" = "5" ]; then
  MOTIF_RELEASE1="1002"
  MOTIF_RELEASE2="1002"
  MOTIF_RELEASE3="1002"
  MOTIF_RELEASE4="1002"
  MOTIF_RELEASE5="1002"		
  echo "*** Motif release 1.2 ($MOTIF_RELEASE1) expected."
elif [ "$X11RELEASE" = "6" ]; then  
  MOTIF_RELEASE1="2000"
  MOTIF_RELEASE2="2001"
  MOTIF_RELEASE3="2002"
  MOTIF_RELEASE4="2003"
# apparently used in Alpha/OSF4 in combination with X11 release 6
  MOTIF_RELEASE5="1002"		
  echo "*** Motif release 2.0 ($MOTIF_RELEASE1) expected."
else
  X11RELEASE="UNKNOWN"
  MOTIF_RELEASE1="UNKNOWN"
  echo "*** WARNING: X11 release 4, 5 or 6 expected"
fi

#*************** START: Checking the Motif include directory *********
if [ "$x11incl" = "found" -a "$x11lib" = "found" ]; then
xmincl=""
$echo "*** Checking Motif Xm include directory...." $SV_NONL
#
rm -f testxm.o motif_rel.exe 2>/dev/null
if (make LSYS=-DONLY_INCLUDES motif_rel.exe) >/dev/null 2>&1
then
  xmincl="found"
  MOTIFRELEASE=`./motif_rel.exe`
fi
if [ "$xmincl" = "found" -a \( "$MOTIFRELEASE" = "$MOTIF_RELEASE1" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE2" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE3" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE4" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE5" \) ]; then
  echo "OK."
  echo "*** Motif release $MOTIFRELEASE found."
else
  xmincl=""
  ed_moptions delete XMINC >/dev/null
  xminclpth='/usr/include/X11R6 /usr/include/X11R5 /usr/include/X11R4 /usr/include/X11 /usr/include/Xm /usr/include/Motif1.2 usr/include/Motif1.1 /usr/local/include /include /usr/X/include /usr/X11/include /usr/X11R5/include /usr/X11R6/include /usr/dt/include /sw/include /sw/lib'
  for dir in $xminclpth
  do
    if [ ! -d $dir ]; then
      continue
    fi
    XMINC="XMINC=-I$dir"
    rm -f testxm.o motif_rel.exe 2>/dev/null
    if (make "$XMINC" LSYS=-DONLY_INCLUDES motif_rel.exe) >/dev/null 2>&1
    then 
      xmincl="found"
      MOTIFRELEASE=`./motif_rel.exe`
    fi
    if [ "$xmincl" = "found" -a \( "$MOTIFRELEASE" = "$MOTIF_RELEASE1" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE2" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE3" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE4" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE5" \) ]; then
      echo "OK."
      echo "*** Motif release $MOTIFRELEASE found."
      ed_moptions add "$XMINC" >/dev/null
      xmincl="found"
      break;
    else
      xmincl=""
    fi
  done
fi
# 
# The Motif include file not found in $xminclpth, so ask the operator
# where it could be. 
#
if [ "$xmincl" != "found" ]; then
  echo "NOT FOUND."
  echo ""
  echo "*** WARNING:  Motif release $MOTIF_RELEASE1 include directory Xm not found."
  echo "*** I checked in paths:"
  echo "$xminclpth"
  while :
  do
    echo ""
    echo "Type the full pathname for the Motif release $MOTIF_RELEASE1 include directory, e.g. /sw/include/"
    echo "or CR if you do not have any."
    while :
    do
      $echo "Pathname: " $SV_NONL
      unset answ
      read answ
      if [ -z "$answ" -o -d "$answ" ]; then
	break
      else
	echo "$answ no such directory."
	continue
      fi
    done
    if [ -z "$answ" ]; then
      echo "*** WARNING: MIDAS will be installed without Motif applications."
      break
    fi
    $echo "*** Checking Motif include files...." $SV_NONL
    rm -f testxm.o motif_rel.exe 2>/dev/null
    answ=`echo $answ | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    XMINC="XMINC=-I$answ"
    if (make "$XMINC" LSYS=-DONLY_INCLUDES motif_rel.exe) 
    then 
      xmincl="found"
      MOTIFRELEASE=`./motif_rel.exe`
    fi
    if [ "$xmincl" = "found" -a \( "$MOTIFRELEASE" = "$MOTIF_RELEASE1" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE2" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE3" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE4" -o "$MOTIFRELEASE" = "$MOTIF_RELEASE5" \) ]; then
      echo "OK."
      echo "*** Motif release $MOTIFRELEASE found."
      ed_moptions add "$XMINC" >/dev/null
      xmincl="found"
      break
    else
      xmincl=""
      echo "NOT FOUND."
    fi
  done
fi
fi
#*************** END: Checking the Motif include directory ***********

#*************** START: Checking the Motif library ***************
# Run this check only if include directory was found previosly
#
if [ "$xmincl" = "found" ]; then
#
  xmlib=""
  $echo "*** Checking Motif library...." $SV_NONL
  rm -f testxm.o testxm.exe 2>/dev/null 
  if (make testxm.exe) >/dev/null 2>&1
  then 
    echo "OK."
    xmlib="found"
  else
    ed_moptions delete MOTIF_LIBPATH >/dev/null
    xmlibpth='/lib /usr/lib /usr/local/lib /usr/dt/lib /usr/lib/X11R6 /usr/lib/X11/R5 /usr/X11R6/lib /usr/X11R5/lib /usr/lib/X11/R4 /usr/lib/Motif1.2 /usr/X/lib /usr/X11/lib /usr/lpp/X11/lib /usr/lib/Motif1.1'
    for dir in $xmlibpth
    do
      if [ ! -d $dir ]; then
        continue
      fi
      MOTIF_LIBPATH="MOTIF_LIBPATH=-L$dir"
      rm -f testxm.o testxm.exe 2>/dev/null 
      if (make "$MOTIF_LIBPATH" testxm.exe) >/dev/null 2>&1
      then
        echo "OK."
        ed_moptions add "$MOTIF_LIBPATH" >/dev/null
        xmlib="found"
        break
      fi
    done
  fi
# 
# The Xm library not found in $x11libpth, so ask the operator
# where it could be. 
#
  if [ "$xmlib" != "found" ]; then
    echo "NOT FOUND."
    echo "*** WARNING: Motif library release $MOTIF_RELEASE not found."
    echo "*** I checked in paths:"
    echo "$xmlibpth"
    while :
    do
      echo "Type the full pathname for the directory containing the Motif library, e.g. /sw/lib/"
      echo "or CR if you do not have any."
      while :
      do
	$echo "Pathname: " $SV_NONL
        unset answ
	read answ
	if [ -z "$answ" -o -d "$answ" ]; then
	   break
	else
	   echo "$answ no such directory."
	   continue
	fi
      done
      if [ -z "$answ" ]; then
        echo "*** WARNING: MIDAS will be installed without Motif applications."
        break
      fi
      MOTIF_LIBPATH="MOTIF_LIBPATH=-L$answ"
      $echo "*** Checking Motif library...." $SV_NONL
      rm -f testxm.exe 2>/dev/null
      if (make "$MOTIF_LIBPATH" testxm.exe)
      then
        echo "OK."
	ed_moptions add "$MOTIF_LIBPATH" >/dev/null
	break
      else
	echo "NOT FOUND."
      fi
    done
  fi
fi
#*************** END: Checking the Motif library *******************

#
# Default UIMX=uimxR4 in default.mk
# Set UIMX=uimxR5 in make_options if X11RELEASE=5
#
if [ "$X11RELEASE" = "5" -o "$X11RELEASE" = "6" ]; then
  ed_moptions delete UIMX >/dev/null
  ed_moptions add "UIMX=uimxR5" >/dev/null
fi

#*************** for manually edited MAKE_OPTIONS ***************
 
if [ $INSTALL_FLAG = "own" ] ; then
   ed_moptions delete INSTALL_FLAG >/dev/null
   ed_moptions add "INSTALL_FLAG=user-choice" >/dev/null
   echo " "
   echo "------------------------------------------------------------"
   echo "=== now, you can edit $MID_HOME/local/make_options"
   echo "=== and adapt it to your special needs"
   echo "=== to continue, use step 67 of the config script"
   echo "------------------------------------------------------------"
   echo " "
   exit 0
fi   

# ******** now the actual installation (in background) *********

export SV_NONL CMND_YES
$MID_INSTALL/install3 $1
exit 0


