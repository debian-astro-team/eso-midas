#! /bin/sh
#% @(#)update.sh	19.1 (ESO-IPG) 02/25/03 13:51:12
# .COPYRIGHT: Copyright (c) 1988 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           update.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Updating procedure of the MIDAS system. 
#                 The MIDAS system is installed with the "install"
#                 command and is executed just once. Henceforth, the
#                 system will be updated for any modification with the
#                 "update" command.
#                 
#                 Usage: update [options]
#                 
#                    options: are options for the "make" command. This option
#                             will be exported to all the directories of the
#                             update procedure.
#			      Multiple definitions for keywords must be:
#				e.g.	CFLAGS=\"-O -g -I/usr/include\"
#			      to avoid quotes to be interpreted by the shell.
#
#                 The command executes the following steps:
#
#                    - $MIDASHOME/$MIDVERS/local/make_options is read if exists.#		       It contains local options for "make" commands.a
#                        
#                        
#		     - MIDAS is structured in "core" (all directories 
#			in file "core.cnf" are included in the installation).
#			and "packages" (optional, they are clasified in 
#			"applic", "stdred" and "contrib".) A package is 
#			selected for installation if it is marked with file
#			"SELECTED".
#
#                    - Running "make options" in all the directories updating
#                      only those files involved in the update.
#
# .AUTHOR         Carlos Guirao
# .VERSION 3.1    910725:		New implementation.

umask 002
#
# First of all, goto the config directory adn
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

#
# Local options for makes commands will be read if they exits in the
# make_options file, from the local directory.
# Other options can be set in the command line of this script as the
# second parameter: $2. These option will replace those local option with 
# the same name
#
if [ -r $MID_HOME/local/make_options ]
then
    echo ""
    echo "*************************************************"
    echo "********* READING YOUR LOCAL OPTIONS ************"
    echo "*************************************************"
    echo ""
    cat $MID_HOME/local/make_options | awk -F# '{print $1}'
    echo ""
fi

echo "*********************************************************************"
echo "******************** MIDAS UPDATING PROCEDURE ***********************"
echo "*********************************************************************"
echo "******************** MIDASHOME: $MIDASHOME  VERSION: $MIDVERS"
echo "******************** DATE:  `date`"
echo "******************** COMMAND: $0 $*"

for dir in `awk -F# '{ print $1 }' $MID_INSTALL/core.cnf`
do
    echo ""
    echo "**********************************************"
    echo "****** UPDATING IN $dir"
    echo "**********************************************"
    echo "****** DATE:  `date`"
    if [ ! -d $MID_HOME/$dir ]; then
        echo "Warning. $dir .No such directory."
        continue
    fi
    cd $MID_HOME/$dir
    eval make "$@" all
done

for categ in applic stdred contrib gui
do
    if [ ! -d $MID_HOME/$categ ]; then
        continue
    fi
    cd $MID_HOME/$categ
    first_packages=`(ls */SELECTED_BY* | awk -F/ '{print $1}' | sort -u) 2>/dev/null`
    if [ -n "$first_packages" ]; then
      excluded=`echo $first_packages | sed -e 's/^/\^/' -e 's/$/\$/' -e 's/ /\$|\^/g'`
      packages=`(ls */SELECTED | awk -F/ '{print $1}' | egrep -v "$excluded") 2>/dev/null`
    else
      packages=`(ls */SELECTED | awk -F/ '{print $1}') 2>/dev/null`
    fi
    for pack in $first_packages $packages
    do
        for dir in etc libsrc src proc help
        do
          if [ -f $pack/$dir/makefile ]; then
            echo ""
            echo "**********************************************"
            echo "****** UPDATING IN ./$categ/$pack/$dir"
            echo "**********************************************"
            cd $pack/$dir
            eval make "$@" all
            cd $MID_HOME/$categ
          fi
        done
    done
done
cd $MID_HOME

echo ""
echo "*********************************************************************"
echo "************************ END UPDATING PROCEDURE *********************"
echo "*********************************************************************"
echo "********** END DATE:  `date`"
