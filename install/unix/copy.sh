#! /bin/sh
# @(#)copy.sh	19.1 (ESO-IPG) 02/25/03 13:51:10
# .TYPE           command
# .NAME           copy.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Copy file in first argument ($1) to the local directory.
#		  If the file exists previously, then it is renamed.
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    27-May-1988:		Implementation

if [ -d $1 ]; then
    if [ ! -d $MIDASHOME/$MIDVERS/local/$1 ]; then
	mkdir $MIDASHOME/$MIDVERS/local/$1
    fi
else
    if [ -f $MIDASHOME/$MIDVERS/local/$1 ]; then
	mv $MIDASHOME/$MIDVERS/local/$1  $MIDASHOME/$MIDVERS/local/${1}~
    fi
    cp $1  $MIDASHOME/$MIDVERS/local/$1
    chmod ug+rw $MIDASHOME/$MIDVERS/local/$1
fi
