#! /bin/sh
# @(#)help.sh	19.1 (ESO-IPG) 02/25/03 13:51:10
# .COPYRIGHT: Copyright (c) 1988 European Southern Observatory,
#                                         all rights reserved
# .TYPE           command
# .NAME           help.sh 
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Installation procedure of the MIDAS system starting from
#                 scratch.
#                 Usage: help 
#
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    14-Oct-1988:		Implementation
# .VERSION 1.2    05-Dec-1989:		Adding "echo" definition
echo=echo
if [ "`echo -n`" = "-n" ] ; then
        SV_NONL="\c"
else
        echo="echo -n"
fi

#
# First of all, goto the config directory MID_INSTALL
# <dirname> & <basename> commands emulated with <sed>
#   cd `dirname $0`
#   MIDVERS=`basename $VERSDIR`
#   MIDASHOME=`dirname $VERSDIR`
#
if [ -z "$MIDASHOME" -o -z "$MIDVERS" ] ; then
    cd `echo $0 | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
    MID_INSTALL=`pwd`
    VERSDIR=`echo $MID_INSTALL | sed 's/\/install\/unix$//'`
    MIDVERS=`echo $VERSDIR | sed -e 's/^.*\///'`
    MIDASHOME=`echo $VERSDIR | sed -e 's/[^\/]*$//' -e 's/^$/./' -e 's/\/$//'`
else
    cd $MIDASHOME/$MIDVERS/install/unix
fi

MID_INSTALL=$MIDASHOME/$MIDVERS/install/unix
MID_HOME=$MIDASHOME/$MIDVERS

#
# START
#
clear

task="unknown"
while true
do
echo ""
echo "		MIDAS HELP MENU:"
echo "	========================================"
echo "	0 Config MIDAS."
echo "	1 List and description of MIDAS packages."
echo "	2 Select only required packages."
echo "	3 Select all packages."
echo "	4 Select MIDAS core & packages of your choice."
echo "	5 Preinstall MIDAS."
echo "	6 Install MIDAS."
echo "	7 Update MIDAS."
echo "	8 Setup MIDAS."
echo "	9 Clean MIDAS."
echo "	q QUIT"
echo ""
$echo "	Select: " $SV_NONL

read task
task=`echo $task | tr A-Z a-z`

case $task in
	0) 
	   clear
	   cat $MID_HOME/doc/install/config.doc | more
	   ;;
	1) 
	   clear
	   cat $MID_HOME/doc/install/list.doc | more
	   ;;
	2) 
	   clear
	   cat $MID_HOME/doc/install/select.doc | more
	   ;;
	3) 
	   clear
	   cat $MID_HOME/doc/install/select.doc | more
	   ;;
	4) 
	   clear
	   cat $MID_HOME/doc/install/select.doc | more
	   ;;
	5) 
	   clear
	   cat $MID_HOME/doc/install/preinstall.doc | more
	   ;;
	6) 
	   clear
	   cat $MID_HOME/doc/install/install.doc | more
	   ;;
	7) 
	   clear
	   cat $MID_HOME/doc/install/update.doc | more
	   ;;
	8) 
	   clear
	   cat $MID_HOME/doc/install/setup.doc | more
	   ;;
	9) 
	   clear
	   cat $MID_HOME/doc/install/cleanm.doc | more
	   ;;
	q) 
	   clear 
	   exit
	   ;;
	*) echo "	Selection unknown"
	   ;;
esac
echo ""
$echo "Type <RETURN> to continue...." $SV_NONL
read key
clear
done
