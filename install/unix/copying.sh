#! /bin/sh
# @(#)copying.sh	19.1 (ESO-IPG) 02/25/03 13:51:10
# .TYPE           command
# .NAME           copying.sh
# .LANGUAGE       shell script
# .ENVIRONMENT    Unix Systems. Executable under SHELL and C-SHELL
# .COMMENTS       Copy file in first argument ($1) to the local directory.
#		  If the file exists previously, then it is renamed.
# .AUTHOR         Carlos Guirao
# .VERSION 1.1    27-May-1988:		Implementation

if [ -f $MIDASHOME/local/$1 ]
then
	mv $MIDASHOME/local/$1  $MIDASHOME/local/$1.orig
fi
cp $1  $MIDASHOME/local/$1
