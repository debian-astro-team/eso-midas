$ ! @(#)installmidas.com	19.1 (ESO-IPG) 02/25/03 13:52:26
$ set noverify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]INSTALLMIDAS.COM
$ ! .COMMENTS    Midas installation procedure for VMS systems.
$ !              The command will:
$ !                 -Read the PREINSTALLMIDAS.CNF file containing all the
$ !                  directories involved in the preinstallation. Then 
$ !                  execute the PREINSTALL.COM command in each directory.
$ !
$ !                 -Read the INSTALLMIDAS.CNF file containing all the
$ !                  directories involved in the installation. Then execute 
$ !                  the MAKEFILE.COM command in each directory.
$ ! 
$ ! .REMARKS     A exclamation mark (!) as the first non-white character 
$ !              in the PREINSTALL.CNF indicates a comment 
$ !              line which is ignored by this procedure.
$ ! 
$ ! .DATE	 881201   C.G.	Creation
$ ! .DATE	 890426	  C.G.	Including 'MIDASHOME'
$ ! .DATE	 900313	  K.B.	make sure we go through LIBDEF.COM
$ ! .DATE	 910117   C.G.  New directory structure.
$ !
$ ! .DATE	 960715   J-P. De Cuyper  Update
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set noon
$ set protection=(s:rewd,o:rewd,g:rewd,w:r)/default
$ !
$ ! Definitions:
$ !
$ echo == "WRITE SYS$OUTPUT"
$ !
$ ! get logical assignments for MIDAS stuff
$ !
$ if MIDVERS .nes. "" then goto CHECK_HOME
$ echo "Error: MIDVERS must be defined previously"
$ exit
$ !
$ CHECK_HOME:
$ if MIDASHOME .nes. "" then goto CHECK_ALPHA
$ echo "Error: MIDASHOME must be defined previously"
$ exit
$ !
$ ! Checking whether this is a VAX or an ALPHA machine.
$ !
$ CHECK_ALPHA:
$ !OPEN/READ LOG_FILE test.log
$ !READ/END_OF_FILE=ERROR LOG_FILE RECORD
$ !NN = F$LOCATE("ALPHA_VMS",RECORD)
$ !IF NN .EQ. F$LENGTH(RECORD) THEN GOTO NO_ALPHA
$ !VMS == "ALPHA"
$ !CLOSE LOG_FILE
$ !GOTO START
$ !
$ !ERROR:
$ !CLOSE LOG_FILE
$ !echo "ERROR: TEST.LOG IS EMPTY"
$ !EXIT
$ !
$ !NO_ALPHA:
$ !CLOSE LOG_FILE
$ !VMS == "NOALPHA"
$ !GOTO START
$ !
$ HWNAME:='F$GETSYI("HW_NAME")'
$ VERSION:='F$GETSYI("VERSION")'
$ NODENAME:='F$GETSYI("NODENAME")'
$ !
$ if F$GETSYI("HW_MODEL") .lt. 1024
$ then
$ echo "This is VAX machine ",NODENAME
$ VMS == "NOALPHA"
$ else
$ echo "This is ALPHA machine  ",NODENAME
$ VMS == "ALPHA"
$ endif     
$ echo "Type ", HWNAME," running VMS ", VERSION 
$ echo "Checking DECC compiler"
$ CC/DECC/VERSION NL:
$ echo "Checking FORTRAN compiler"
$ product show product FORTRAN
$ GOTO START
$ !
$ !
$ INTERRUPT:
$ set verify
$ set def ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]
$ close input_file
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ deassign sys$error
$ deassign sys$output
$ exit
$ !
$ ! Start here:
$ !
$ START:
$ on control_y then exit
$ on control_c then goto INTERRUPT
$ set message/nofacility -
        /noidentification -
        /noseverity -
        /notext
$ !
$ if P1 .nes. "RECONFIRM" then goto NOCONFIRM
$ echo "Installmidas could take several hours."
$ inquire ANSWER "Do you want to continue [yn]? (y)"
$ if ANSWER .eqs. "" then goto NOCONFIRM
$ if ANSWER .nes. "Y" then goto FIN
$ !
$ NOCONFIRM:
$ @ ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]LIBDEF.COM
$ !
$ goto CHECK_IP8LIB
$ !CHECK_VAXCRTL:
$ !if VMS .eqs. "ALPHA" then goto CHECK_IP8LIB
$ !show logical/output=SYSTEMLIB.DAT lnk$library*
$ !open/read input_file SYSTEMLIB.DAT
$ !READMORE:
$ !read/end_of_file=VMS_NOTFOUND input_file RECORD
$ !nn = f$locate("VAXCRTL.OLB",RECORD)
$ !if nn .eq. f$length(RECORD) then goto READMORE
$ !close input_file
$ !goto CHECK_IP8LIB
$ !
$ !VMS_NOTFOUND:
$ !close input_file
$ !echo "ERROR: C runtime library VAXCRTL.OLB not found !!!"
$ !echo "       It must be defined between the logical names LNK$LIBRARY*"
$ !echo "       Ask your system manager for the location of this library in the"
$ !echo "       system and type: "
$ !echo "          $ DEFINE LNK$LIBRARY <location>:VAXCRTL.OLB"
$ !echo "       Include this in file LIBDEF.COM. EXIT."
$ !goto FIN
$ !
$ CHECK_IP8LIB:
$ lib/nolist IP8LIB
$ if $STATUS then goto CHECK_LIBNAG
$ echo ""
$ echo "ERROR: IP8LIB library not found !!!"
$ echo "       Whether you have a DEANZA display or not, library IP8LIB"
$ echo "       must exist, otherwise the MIDAS installation will fail."
$ echo ""
$ echo "If you do not have a DEANZA display,"
$ inquire ANSWER "do you want me to create an IP8LIB empty library [yn]? (y)"
$ if ANSWER .eqs. "" then goto CREA_IP8LIB
$ if ANSWER .eqs. "Y" then goto CREA_IP8LIB
$ echo ""
$ echo "OK. Then install your DEANZA library and make it available for MIDAS."
$ echo "   Definition for this library can be changed in file:"
$ echo "   ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]LIBDEF.COM"
$ echo "   Current definition for this library is:"
$ show logical IP8LIB
$ echo ""
$ echo "Rerun INSTALLMIDAS again when ready. EXIT."
$ goto FIN
$ !
$ CREA_IP8LIB:
$ lib/create IP8LIB
$ goto CHECK_IP8LIB
$ !
$ CHECK_LIBNAG:
$ lib/nolist LIBNAG
$ if $STATUS then goto CHECK_DISPLAY
$ echo ""
$ echo "ERROR: NAG library LIBNAG.OLB not found !!!"
$ echo "       Whether you have a NAG library or not, this library LIBNAG"
$ echo "       must exist, otherwise the MIDAS installation will fail."
$ echo ""
$ echo "If you do not have the mathematical NAG library (NAG$LIBRARY.OLB),"
$ inquire ANSWER "do you want to use a dummy NAG library [yn]? (y)"
$ if ANSWER .eqs. "" then goto CREA_LIBNAG
$ if ANSWER .eqs. "Y" then goto CREA_LIBNAG
$ echo ""
$ echo "OK. Then install your NAG library and make it available for MIDAS."
$ echo "   Definition for this library can be changed in file:"
$ echo "   ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]LIBDEF.COM"
$ echo "   Current definition for this library is:"
$ show logical LIBNAG
$ echo ""
$ echo "Rerun INSTALLMIDAS again when ready. EXIT."
$ goto FIN
$ !
$ CREA_LIBNAG:
$ lib/create LIBNAG
$ set def ['MIDASHOME'.'MIDVERS'.LIBSRC.NAG]
$ make 
$ set def ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]
$ goto CHECK_LIBNAG
$ !
$ CHECK_DISPLAY:
$ directory/nooutput ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]DISPLAY.CNF
$ if $STATUS then goto CHECK_SELECTED
$ echo ""
$ echo "ERROR: file ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]DISPLAY.CNF not found"
$ echo "       Use option <selec display> from CONFIGMIDAS command.
$ echo ""
$ echo "Rerun INSTALLMIDAS again when ready. EXIT."
$ goto FIN
$ !
$ CHECK_SELECTED:
$ directory/nooutput ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]SELECTED.CNF
$ if $STATUS then goto MERGE_CNF
$ echo ""
$ echo "ERROR: file ['MIDASHOME'.MIDVERS'.INSTALL.VMS]SELECTED.CNF not found"
$ echo "       Use option <selec only/own/all MIDAS> from CONFIGMIDAS command.
$ echo ""
$ echo "Rerun INSTALLMIDAS again when ready. EXIT."
$ goto FIN
$ !
$ !
$ MERGE_CNF:
$ copy DISPLAY.CNF,SELECTED.CNF INSTALLMIDAS.CNF
$ if $STATUS then goto CHECK_LOGFILE
$ echo ""
$ echo "ERROR: I tried the following command, and it failed:"
$ echo "       $ COPY DISPLAY.CNF,SELECTED.CNF INSTALLMIDAS.CNF"
$ echo ""
$ echo "Rerun INSTALLMIDAS again when ready. EXIT."
$ goto FIN
$ !
$ CHECK_LOGFILE:
$ inquire ANSWER "Do you want to log the output into INSTALLMIDAS.LOG file [yn]? (y)"
$ if ANSWER .eqs. "" then goto CREA_LOGFILE
$ if ANSWER .nes. "Y" then goto INSTALL
$ CREA_LOGFILE:
$ echo "**********************************************************************"
$ echo "******************** MIDAS INSTALLATION PROCEDURE ********************"
$ echo "**********************************************************************"
$ echo "****** OUTPUT IS NOW BEING WRITTEN INTO THE INSTALLMIDAS.LOG FILE*****"
$ DEFINE SYS$OUTPUT INSTALLMIDAS.LOG 
$ DEFINE SYS$ERROR INSTALLMIDAS.LOG 
$ INSTALL:
$ set message/facility -
	/identification -
	/severity -
	/text
$ echo ""
$ echo "**********************************************************************"
$ echo "******************** MIDAS INSTALLATION PROCEDURE ********************"
$ echo "**********************************************************************"
$ !
$ if F$GETSYI("HW_MODEL") .lt. 1024
$ then
$ echo "******************* On VAX machine ",NODENAME
$ else
$ echo "******************* On ALPHA machine ",NODENAME
$ endif
$ echo "************* Type ", HWNAME," running VMS ", VERSION 
$ CC/decc/version nl:
$ product show product FORTRAN
$ echo "**********************************************************************"
$ echo "******************** MIDVERS: ",MIDVERS
$ echo "******************** DATE: ",F$TIME()
$ !
$ !
$ !
$ ! preinstallation directory list in PREINSTALL.LIS
$ ! and read that file now
$ !
$ open/read input_file PREINSTALLMIDAS.CNF
$ !
$ READ_PREINSTALL:
$ read/end_of_file=EOF_PREINS input_file RECORD
$ ! 
$ ! skip all those lines starting with the comment character "!"
$ ! 
$ nn = 'f$locate("!",RECORD)'
$ if nn .eq. 0 then goto READ_PREINSTALL
$ source := 'f$extract(0,nn,RECORD)'
$ ! 
$ ! now change to that directory
$ ! 
$ set def 'SOURCE'
$ !
$ t = f$time()
$ echo ""
$ echo "***********************************************************"
$ echo "****** PREINSTALLATION IN ",SOURCE
$ echo "***********************************************************"
$ echo "**** DATE: ",t
$ ! 
$ ! now execute the preinstallation in that directory 
$ ! 
$ make preinstall.com
$ set noverify
$ purge/nolog/noconfirm
$ goto READ_PREINSTALL
$ !
$ EOF_PREINS:
$ set def ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]
$ close input_file
$ !
$ open/read input_file INSTALLMIDAS.CNF
$ READ_INSTALL:
$ read/end_of_file=EOF_INSTALL input_file RECORD
$ ! 
$ ! skip all those lines starting with the comment character "!"
$ !
$ nn = 'f$locate("!",RECORD)'
$ if nn .eq. 0 then goto READ_INSTALL
$ source := 'f$extract(0,nn,RECORD)'
$ ! 
$ ! now change to that directory 
$ ! 
$ set def 'SOURCE'
$ !
$ t = f$time()
$ echo ""
$ echo "***********************************************************"
$ echo "****** INSTALLATION IN ",SOURCE
$ echo "***********************************************************"
$ echo "**** DATE: ",t
$ ! 
$ ! now execute the makefile in that directory 
$ !
$ make
$ set noverify
$ set message/nofacility -
	/noidentification -
	/noseverity -
	/notext
$ delete/nolog/noconfirm *.OBJ;*
$ set message/facility -
	/identification -
	/severity -
	/text
$ goto READ_INSTALL
$ !
$ ! That's it folks...
$ !
$ EOF_INSTALL:
$ set def ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]
$ close input_file
$ !
$ t = f$time()
$ echo ""
$ echo "***********************************************************"
$ echo "*************** END INSTALLATION PROCEDURE ****************"
$ echo "***********************************************************"
$ echo "*************** DATE: ",t
$ !
$ FIN:
$ !
$ set message/facility -
        /identification -
        /severity -
        /text
$ set on
$ purge/nolog/noconfirm
$ deassign sys$error
$ deassign sys$output
$ exit
