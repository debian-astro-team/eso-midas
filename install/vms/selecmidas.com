$ ! @(#)selecmidas.com	19.1 (ESO-IPG) 02/25/03 13:52:28 
$ set noverify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]SELECMIDAS.COM
$ ! .COMMENTS    Midas configuration procedure for VMS systems.
$ ! 
$ ! .REMARKS
$ !
$ ! .DATE	 990111   C.Guirao	Creation
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set noon
$ set protection=(S:REWD,O:REWD,G:REWD,W:R)/DEFAULT
$ !
$ ! Definitions:
$ !
$ echo == "WRITE SYS$OUTPUT"
$ clear = "[2J[H"
$ !
$ ! Get logical assignments for MIDAS stuff
$ !
$ if MIDVERS .nes. "" then goto CHECK_HOME
$ echo "Error: MIDVERS must be defined previously"
$ exit
$ !
$ CHECK_HOME:
$ if MIDASHOME .nes. "" then goto CHECK_OPTION
$ echo "Error: MIDASHOME must be defined previously"
$ exit
$ !
$ ! Check option P1:
$ !
$ CHECK_OPTION:
$ OPTION := 'P1'
$ if P1 .eqs. "" then OPTION := "ALL"
$ if OPTION .eqs. "CORE" then goto START
$ if OPTION .eqs. "ALL" then goto START
$ if OPTION .eqs. "OWN" then goto START
$ ERRMSG0 = OPTION + " bad argument"
$ echo ERRMSG0
$ echo "Usage: SELECMIDAS [ALL|CORE|OWN]  (default=ALL)"
$ exit
$ !
$ ! Interrupt:
$ !
$ !
$ INTERRUPT:
$ close tmp_file
$ close dir_file
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ exit
$ !
$ !
$ ! Start here:
$ !
$ START:
$ on control_y then goto exit
$ on control_c then goto INTERRUPT
$ set message/nofacility -
	/noidentification -
	/noseverity -
	/notext
$ SPEC := "N"
$ convert CORE.CNF CORE_TMP.CNF
$ open/append tmp_file CORE_TMP.CNF
$ echo clear
$ if OPTION .eqs. "CORE" then echo "NOTE: Only CORE MIDAS will be included."
$ if OPTION .eqs. "CORE" then goto CHECK_CNF
$ echo "	LIST OF AVAILABLE PACKAGES FOR MIDAS:"
$ echo ""
$ echo "	NAME		CATEGORY	Y/N"
$ echo "	========================================"
$
$ GROUP := "APPLIC"
$ gosub LIST_DIRS
$ GROUP := "STDRED"
$ !
$ ! SPEC is first package of STDRED, because might be used by other packages.
$ !
$ PCKG:= SPEC
$ gosub READ_YN
$ gosub LIST_DIRS
$ GROUP := "CONTRIB"
$ gosub LIST_DIRS
$ echo ""
$ !
$ CHECK_CNF:
$ close tmp_file
$ inquire ANSWER "Do you want this configuration [yn]? (y)"
$ echo clear
$ if ANSWER .eqs. "" then goto UPDATE_CNF
$ if ANSWER .eqs. "Y" then goto UPDATE_CNF
$ echo "Configuration not selected."
$ echo ""
$ goto FIN 
$ !
$ UPDATE_CNF:
$ rename/nolog/noconfirm CORE_TMP.CNF SELECTED.CNF
$ echo "Configuration selected."
$ echo ""
$ !
$ FIN:
$ purge/nolog/noconfirm GROUP.LIS,CORE_TMP.CNF,SELECTED.CNF
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ exit
$ !
$ !--------------------------------------------------------------------------
$ ! .NAME        LIST_DIRS
$ ! .TYPE        DCL subroutine
$ ! .COMMENTS    Identifies MIDAS packages under ['MIDASHOME'.'MIDVERS'.'GROUP']
$ !              That means, those directories containing a [.LIBSRC], [.SRC] 
$ !              [.PROC] or [.ETC] subdirectory.
$ ! .REMARKS
$ ! .DATE	 990111   C.Guirao	Creation
$ !--------------------------------------------------------------------------
$ LIST_DIRS:
$ directory/size/exclude=SPEC.DIR /output=group ['MIDASHOME'.'MIDVERS'.'GROUP']*.dir
$ open/read dir_file GROUP.LIS
$ !
$ BEGIN:
$ read/end_of_file=EOF_GROUP dir_file RECORD
$ !
$ ! skip in GROUP.LIS all those lines with no ";" character
$ !
$ nn = 'f$locate(";",RECORD)'
$ if nn .eq. f$length(RECORD) then goto BEGIN
$ nn = 'f$locate(".",RECORD)'
$ PCKG := 'f$extract(0,nn,RECORD)'
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''PCKG'.*]MAKEFILE.COM") -
     .nes. "" then gosub READ_YN
$ goto BEGIN
$ EOF_GROUP:
$ close dir_file
$ return 
$ !
$ !
$ !--------------------------------------------------------------------------
$ ! .NAME        READ_YN
$ ! .TYPE        DCL subroutine
$ ! .COMMENTS    Identifies MIDAS packages under ['MIDASHOME'.'MIDVERS'.'GROUP']
$ !              That means, those directories containing a [.LIBSRC], [.SRC] 
$ !              [.PROC] or [.ETC] subdirectory.
$ ! .REMARKS
$ ! .DATE	 990111   C.Guirao	Creation
$ !--------------------------------------------------------------------------
$ READ_YN:
$ !
$ ! If SPEC package has been already included then return
$ !
$ if GROUP .eqs. "STDRED" then -
     if PCKG .eqs. "SPEC" then -
         if SPEC .eqs "Y" then return
$ !
$ ASK_OUT = "	" + PCKG + "     	" + GROUP  + "		(y/n)?"
$ DIR_OUT = "	" + PCKG + "     	" + GROUP
$ if OPTION .eqs. "ALL" then goto ANSWER_YES
$ echo ASK_OUT
$ inquire ANSWER "Do you want to include this package Y[ES], N[O] or Q[UIT]?, (Y)"
$ if ANSWER .eqs. "" then goto ANSWER_YES
$ if ANSWER .eqs. "Y" then goto ANSWER_YES
$ if ANSWER .eqs. "Q" then close dir_file
$ if ANSWER .eqs. "Q" then goto CHECK_CNF
$ !
$ ! If answer is no, then adds entries in temporary configuration
$ ! file with a comment mark
$ !
$ ENTRY = "!['MIDASHOME'.'MIDVERS'."'GROUP'"."'PCKG'".LIBSRC]"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''PCKG'.LIBSRC]MAKEFILE.COM") -
       .nes. "" then write/symbol tmp_file ENTRY 
$ ENTRY = "!['MIDASHOME'.'MIDVERS'."'GROUP'"."'PCKG'".SRC]"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''PCKG'.SRC]MAKEFILE.COM") -
       .nes. "" then write/symbol tmp_file ENTRY 
$ ENTRY = "!['MIDASHOME'.'MIDVERS'."'GROUP'"."'PCKG'".PROC]"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''PCKG'.PROC]MAKEFILE.COM") -
       .nes. "" then write/symbol tmp_file ENTRY 
$ ENTRY = "!['MIDASHOME'.'MIDVERS'."'GROUP'"."'PCKG'".ETC]"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''PCKG'.ETC]MAKEFILE.COM") -
       .nes. "" then write/symbol tmp_file ENTRY 
$ DIR_OUT = DIR_OUT + "		NOT INCLUDED"
$ echo DIR_OUT
$ return
$ !
$ ! If answer is yes, then adds entries in temporary configuration
$ ! file without comment mark
$ !
$ ANSWER_YES:
$ ! 
$ ! SPEC package have to be included before "LONG" or "ECHELLE"
$ !
$ if GROUP .eqs. "STDRED" then if PCKG .eqs. "SPEC" then SPEC := "Y"
$ !
$ if GROUP .nes. "STDRED" then goto ANSW_YES
$ if SPEC .nes. "N"       then goto ANSW_YES
$ if PCKG .eqs. "LONG"    then goto INCLUDE_SPEC
$ if PCKG .eqs. "ECHELLE" then goto INCLUDE_SPEC
$ goto ANSW_YES:
$ INCLUDE_SPEC:
$ SPEC := "Y"
$ ENTRY = "['MIDASHOME'.'MIDVERS'.STDRED.SPEC.LIBSRC]"
$ write/symbol tmp_file ENTRY
$ ENTRY = "['MIDASHOME'.'MIDVERS'.STDRED.SPEC.SRC]"
$ write/symbol tmp_file ENTRY
$ ENTRY = "['MIDASHOME'.'MIDVERS'.STDRED.SPEC.PROC]"
$ write/symbol tmp_file ENTRY
$ DIR_SPEC = "	SPEC		STDRED		INCLUDED FIRST"
$ echo DIR_SPEC
$ !
$ ANSW_YES:
$ ENTRY = "['MIDASHOME'.'MIDVERS'."'GROUP'"."'PCKG'".LIBSRC]"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''PCKG'.LIBSRC]MAKEFILE.COM") -
       .nes. "" then write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'."'GROUP'"."'PCKG'".SRC]"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''PCKG'.SRC]MAKEFILE.COM") -
       .nes. "" then write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'."'GROUP'"."'PCKG'".PROC]"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''PCKG'.PROC]MAKEFILE.COM") -
       .nes. "" then write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'."'GROUP'"."'PCKG'".ETC]"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''PCKG'.ETC]MAKEFILE.COM") -
       .nes. "" then write/symbol tmp_file ENTRY 
$ DIR_OUT = DIR_OUT + "		INCLUDED"
$ echo DIR_OUT
$ return
$ !--------------------------------------------------------------------------
