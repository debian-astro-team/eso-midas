% @(#)installmidas.doc	19.1 (ESO-IPG) 02/25/03 13:52:26

INSTALLMIDAS

    MIDAS installation DCL procedure.

    INSTALLMIDAS is the MIDAS installation procedure. It can be executed
    from the CONFIGMIDAS procedure when selected from menu, or directly by 
    typing  "@ INSTALLMIDAS <option>"

    INSTALLMIDAS has only one option:

    o     RECONFIRM. When given, INSTALLMIDAS will required the operator
                     for acceptance before starting. This option is 
                     used when invoked by CONFIGMIDAS.

    The official MIDAS distribution from ESO provides by default
    the following configuration:

    o     Only CORE MIDAS packages.
    o     machine default internal data representation 
          (DEC default is d_float on VAX and g_float on ALPHA). 
    o     Only X11 graphic devices.

    INSTALLMIDAS executes the following steps:

    o     Checks if 'MIDASHOME' and 'MIDVERS' symbols are defined. Exit 
          otherwise, (e.g. when invoked from CONFIGMIDAS, these symbols are
          always defined).

    o     Executes the procedure "@ LIBDEF". In this procedure is defined
          all definitions for MIDAS libraries.

    o     Checks if mathematical NAG library and Deanza library exist. 
          These libraries are defined by the symbols LIBNAG and IP8LIB 
          in file: ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]LIBDEF.COM

          If these libraries exist in the system, the operator should first 
          define LIBNAG and IP8LIB symbols to wherever in the system the 
          libraries are. He could also copy them to where LIBNAG and IP8LIB
          are defined, i.e. ['MIDASHOME'.'MIDVERS'.LIB]. 

          Otherwise INSTALLMIDAS will inquire the operator for creating empty 
          LIBNAG and IP8LIB libraries. INSTALLMIDAS will not continue whitout
          these libraries.

    o     INSTALLMIDAS checks also for the existence of two configuration 
          files: DISPLAY.CNF and SELECTED.CNF. If they do not exist, they
          can be generated from SELECDISPLAY and SELECMIDAS procedures. (Also
          they are invoked from CONFIGMIDAS).
          INSTALLMIDAS concatenates this two files in a unique configuration
          file INSTALLMIDAS.CNF.

    o     INSTALLMIDAS initiates first the MIDAS installation by reading the
          file PREINSTALLMIDAS.CNF. For each valid directory listed here, 
          INSTALLMIDAS executes the procedure "@ MAKE PREINSTALL". This
	  procedure requires a file PREINSTALL.COM in the directory where it
          is executed.
          The procedure MAKE.COM defines the compilers and can be configured 
          by the SELECMAKE procedure. 
	  
    o     After preinstallation, INSTALLMIDAS reads the file CONFIGMIDAS.CNF
          For each directory listed here INSTALLMIDAS executes the procedure 
          "@ MAKE". This procedure requires a file MAKEFILE.COM in the 
           directory where it is executed.

    NOTE: In the files PREINSTALLMIDAS.CNF and INSTALLMIDAS.CNF an exclamation 
    mark (!) as the first non-white character indicates a comment line which 
    is ignored by INSTALLMIDAS.

  Additional information available:

    In DCL files: ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]INSTALLMIDAS.COM
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]CONFIGMIDAS.COM
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]SELECMIDAS.COM
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]LIBDEF.COM
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]MAKE.COM
    In configuration files:
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]PREINSTALLMIDAS.CNF
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]INSTALLMIDAS.CNF
    In documentation files:   
                  [MIDAS.95NOV.INSTALL.VMS]CLEANMIDAS.DOC
                  [MIDAS.95NOV.INSTALL.VMS]CONFIGMIDAS.DOC
                  [MIDAS.95NOV.INSTALL.VMS]LISTMIDAS.DOC
                  [MIDAS.95NOV.INSTALL.VMS]SELECDISPLAY.DOC
                  [MIDAS.95NOV.INSTALL.VMS]SELECMAKE.DOC
                  [MIDAS.95NOV.INSTALL.VMS]SELECMIDAS.DOC

  Examples: 

    1. If your system is ready for the default configuration, you can 
       proceed with INSTALLMIDAS:

       $ SET DEF  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]
       $ @ INSTALLMIDAS           or (See NOTE below)
       $ @ INSTALLMIDAS/OUTPUT=INSTALLMIDAS.LOG

    NOTE: It is recommended to execute the first time: "@ INSTALLMIDAS"
    since the output will be displayed on the screen, and simple and 
    corrigible errors can be easily detected. The installation can be then
    interrupted with several CTR-C keystrokes.

    When errors are then corrected, a complete installation can be executed 
    redirecting the output to a log-file.
