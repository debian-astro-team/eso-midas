$!	Proc�dure : SUBSTITE.COM
$!	Auteur    : N_DIAKONOFF
$!	Cr�ation  : 28 Mars 89
$!      Modified  : H. Van Diest 08-SEP-1999
$!      Updated   : J.-P. De Cuyper 30-MAY-2000
$!	Purpose	  : Substitutes a character chain by an other one
$!		    in one or more files (Wildcards authorized).
$!
$	On Control_y Then Goto END
$	On Error     Then Goto END
$!
$	If P1.eqs."" Then Goto HELP
$ START:
$	If P1.eqs."" Then Inquire P1 "_Name of file(s) "
$	If P1.eqs."" Then Exit
$	If P2.eqs."" Then Inquire P2 "_Old text  "
$	If P2.eqs."" Then Exit
$	If P3.eqs."" Then Inquire P3 "_New text  "
$	Goto TODO
$ HELP:
$ Write Sys$output ""
$ Write Sys$output "[2J[1;1H"
$ Write Sys$output ""
$ Write Sys$output "	Procedure SUBSTITE.COM"
$ Write Sys$output "		  ============"
$ Write Sys$output ""
$ Write Sys$output "	Substitution of agiven character chain by another one."
$ Write Sys$output "	Wildcards are accepted in the name of the file."
$ Write Sys$output ""
$ Write Sys$output ""
$ Write Sys$output "	$@SUBSTITE *.DBL OLD_STRING NEW_STRING"
$ Write Sys$output ""
$ Write Sys$output "	P1 = Name of the file(s)."
$ Write Sys$output "	P2 = Old text"
$ Write Sys$output "	P3 = New text"
$ Write Sys$output ""
$ Write Sys$output ""
$ Write Sys$output "	If P1 or P2 ne are not specified  --> end the procedure."
$ Write Sys$output ""
$	Goto START
$ TODO:  
$	Search/Window=0/Output=SYS$SCRATCH:SUBSTITE.TM1 'P1' "''P2'"
$	Open/Read  TMP SYS$SCRATCH:SUBSTITE.TM1
$	Open/Write CMD SYS$SCRATCH:SUBSTITE.TM2
$	On Control_y Then Goto CLEANUP
$	On Error     Then Goto CLEANUP
$ LOOP:
$	Read/End=C_FINI TMP FILE
$	Write CMD "$Edite/Edt/Nocommand ",FILE
$	Write CMD "Substitute\","''P2'","\","''P3'","\Whole"
$	Write CMD "Exit"

$   NN = 'F$LOCATE(";",file)'
$   SOURCE := 'F$EXTRACT(0,NN,file)'
$       Write CMD "$pu ''source'"

$   filea = source+"_orig"
$       copy 'File' 'filea' 
$	Goto LOOP
$!
$ C_FINI:
$	Close TMP
$	Close CMD
$       Define sys$output _nl:
$	@SYS$SCRATCH:SUBSTITE.TM2
$       Deassign sys$output 
$ END:
$	Delete/NoConfirm/NoLog SYS$SCRATCH:SUBSTITE.TM%;*
$ Exit
$!
$ CLEANUP:
$	Close TMP
$	Close CMD
$	Goto END
$!---------------------------------------------- Fin de la proc�dure ----------
