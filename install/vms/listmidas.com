$ ! @(#)listmidas.com	19.1 (ESO-IPG) 02/25/03 13:52:27 
$ set noverify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]LISTMIDAS.COM
$ ! .COMMENTS    Midas configuration procedure for VMS systems.
$ ! 
$ ! .REMARKS
$ !
$ ! .DATE	 990111   C.Guirao	Creation
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set noon
$ set protection=(S:REWD,O:REWD,G:REWD,W:R)/DEFAULT
$ !
$ ! Definitions:
$ !
$ clear = "[2J[H"
$ echo == "WRITE SYS$OUTPUT"
$ !
$ ! Get logical assignments for MIDAS stuff
$ !
$ if MIDVERS .nes. "" then goto CHECK_HOME
$ echo "Error: MIDVERS must be defined previously"
$ exit
$ !
$ CHECK_HOME:
$ if MIDASHOME .nes. "" then goto START
$ echo "Error: MIDASHOME must be defined previously"
$ exit
$ !
$ INTERRUPT:
$ close dir_file
$ goto CHECK_SELECT
$ !
$ ! Start here:
$ !
$ START:
$ on control_y then goto exit
$ on control_c then goto INTERRUPT
$ set message/nofacility -
	/noidentification -
	/noseverity -
	/notext
$ SHOW_LIST:
$ echo clear
$ echo "	LIST OF AVAILABLE PACKAGES FOR MIDAS:"
$ echo ""
$ echo "	NAME            CATEGORY"
$ echo "	=========================="
$
$ GROUP := "APPLIC"
$ gosub LIST_DIRS
$ GROUP := "STDRED"
$ gosub LIST_DIRS
$ GROUP := "CONTRIB"
$ gosub LIST_DIRS
$ echo ""
$ !
$ CHECK_SELECT:
$ echo "Type <Q> to quit or <L> to list again."
$ REPEAT:
$ inquire SELECT "Select NAME"
$ if SELECT .eqs. "Q" then goto FIN
$ if SELECT .eqs. "L" then goto SHOW_LIST
$ !
$ GROUP := "APPLIC"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP']''SELECT'.DIR") -
    .nes. "" then got CHECK_DESCRIPTOR
$ GROUP := "STDRED"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP']''SELECT'.DIR") -
    .nes. "" then got CHECK_DESCRIPTOR
$ GROUP := "CONTRIB"
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP']''SELECT'.DIR") -
    .nes. "" then got CHECK_DESCRIPTOR
$ ERROR0 = SELECT +  ": No such package with that name."
$ echo ERROR0
$ goto REPEAT
$ !
$ CHECK_DESCRIPTOR:
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''SELECT']DESCRIPTION") -
    .nes. "" then goto CAT_DESCRIPTOR
$ ERROR1 = "DESCRIPTION file from <" + SELECT + "> not available."
$ echo ERROR1
$ goto REPEAT
$ CAT_DESCRIPTOR:
$ type/page ['MIDASHOME'.'MIDVERS'.'GROUP'.'SELECT']DESCRIPTION.
$ goto CHECK_SELECT
$ !
$ FIN:
$ purge GROUP.LIS
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ exit
$ !
$ !--------------------------------------------------------------------------
$ ! .NAME        LIST_DIRS
$ ! .TYPE        DCL subroutine
$ ! .COMMENTS    Identifies MIDAS packages under ['MIDASHOME'.'MIDVERS'.'GROUP']
$ !              That means, those directories containing a [.LIBSRC], [.SRC] 
$ !              or [.PROC] subdirectory.
$ ! .REMARKS
$ ! .DATE	 990111   C.Guirao	Creation
$ !--------------------------------------------------------------------------
$ LIST_DIRS:
$ directory/size/output=GROUP ['MIDASHOME'.'MIDVERS'.'GROUP']*.DIR
$ open/read dir_file GROUP.LIS
$ !
$ BEGIN:
$ read/end_of_file=EOF_GROUP dir_file RECORD
$ !
$ ! skip in GROUP.LIS all those lines with no ";" character
$ !
$ nn = 'f$locate(";",RECORD)'
$ if nn .eq. f$length(RECORD) then goto BEGIN
$ nn = 'f$locate(".",RECORD)'
$ DIR_INPUT := 'f$extract(0,nn,RECORD)'
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''DIR_INPUT'.*]MAKEFILE.COM") -
    .nes. "" then goto ECHO_DIR
$ goto BEGIN
$ ECHO_DIR:
$ DIR_OUT = "	" + DIR_INPUT + "     	" + GROUP
$ if f$search("[''MIDASHOME'.''MIDVERS'.''GROUP'.''DIR_INPUT']DESCRIPTION") -
    .eqs. "" then DIR_OUT = DIR_OUT + "	(DESCRIPTION file not available)"
$ echo DIR_OUT
$ goto BEGIN
$ !
$ EOF_GROUP:
$ close dir_file
$ return 
$ !--------------------------------------------------------------------------
