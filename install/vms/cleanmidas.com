$ ! @(#)cleanmidas.com	19.1 (ESO-IPG) 02/25/03 13:52:25 
$ set noverify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]CLEANMIDAS.COM
$ ! .COMMENTS    Midas configuration procedure for VMS systems.
$ ! 
$ ! .REMARKS
$ !
$ ! .DATE	 990111   C.Guirao	Creation
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set noon
$ set protection=(S:REWD,O:REWD,G:REWD,W:R)/DEFAULT
$ !
$ ! Definitions:
$ !
$ echo == "WRITE SYS$OUTPUT"
$ clear = "[2J[H"
$ !
$ ! Get logical assignments for MIDAS stuff
$ !
$ if MIDVERS .nes. "" then goto CHECK_HOME
$ echo "Error: MIDVERS must be defined previously"
$ exit
$ !
$ CHECK_HOME:
$ if MIDASHOME .nes. "" then goto START
$ echo "Error: MIDASHOME must be defined previously"
$ exit
$ !
$ !
$ ! Start here:
$ !
$ START:
$ echo clear
$ SHOW_MENU:
$ echo ""
$ echo "	    MIDAS DELETE MENU:
$ echo "        ==============================
$ echo "	1 - Delete source files."
$ echo "	2 - Delete source files and libraries."
$ echo "	3 - Delete documentation."
$ echo "	4 - Delete executable files."
$ echo "	5 - Purge all files.
$ echo "	q - Quit."
$ echo ""
$ inquire SELECT "Select"
$ !
$ if SELECT .eqs. "1" then goto SRC
$ if SELECT .eqs. "2" then goto SRC_OLB
$ if SELECT .eqs. "3" then goto DOC
$ if SELECT .eqs. "4" then goto EXE
$ if SELECT .eqs. "5" then goto PURGE
$ if SELECT .eqs. "Q" then goto FIN
$ goto SHOW_MENU
$ !
$ SRC:
$ echo "DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.FOR.*"
$ echo "DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.C.*"
$ GOTO COMMON
$ SRC_OLB:
$ echo "DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.FOR.*"
$ echo "DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.C.*"
$ echo "DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.OLB.*"
$ GOTO COMMON
$ DOC:
$ echo "DELETE/NOLOG ['MIDASHOME'.'MIDVERS'.DOC...]*.*.*"
$ GOTO COMMON
$ EXE:
$ echo "DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.EXE.*"
$ GOTO COMMON
$ PURGE:
$ echo "PURGE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.*"
$ GOTO COMMON
$ !
$ COMMON:
$ echo ""
$ inquiry ANSWER "Do you want to continue? (y/n)" 
$ if ANSWER .eqs. "Y" then goto REMOVE
$ goto SHOW_MENU
$ !
$ REMOVE:
$ if SELECT .eqs. "1" then goto DEL_SRC
$ if SELECT .eqs. "2" then goto DEL_SRC_OLB
$ if SELECT .eqs. "3" then goto DEL_DOC
$ if SELECT .eqs. "4" then goto DEL_EXE
$ if SELECT .eqs. "5" then goto DEL_PURGE
$ !
$ DEL_SRC:
$ set verify
$ DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.FOR.*
$ DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.C.*
$ set noverify
$ goto FIN
$ !
$ DEL_SRC_OLB:
$ set verify
$ DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.FOR.*
$ DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.C.*
$ DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.OLB.*
$ set noverify
$ goto FIN
$ !
$ DEL_DOC:
$ set verify
$ DELETE/NOLOG ['MIDASHOME'.'MIDVERS'.DOC...]*.*.*
$ set noverify
$ goto FIN
$ !
$ DEL_EXE:
$ set verify
$ DELETE/NOLOG ['MIDASHOME'.'MIDVERS'...]*.EXE.*
$ set noverify
$ goto FIN
$ !
$ DEL_PURGE:
$ set verify
$ PURGE/LOG ['MIDASHOME'.'MIDVERS'...]*.*
$ set noverify
$ goto FIN
$ !
$ !
$ FIN:
$ set on
$ exit
