$ ! @(#)helpmidas.com	19.1 (ESO-IPG) 02/25/03 13:52:26 
$ set noverify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]HELPMIDAS.COM
$ ! .COMMENTS    Midas configuration procedure for VMS systems.
$ ! 
$ ! .REMARKS
$ !
$ ! .DATE	 990111   C.Guirao	Creation
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set protection=(S:REWD,O:REWD,G:REWD,W:R)/DEFAULT
$ set noon
$ !
$ ! Definitions:
$ !
$ echo == "WRITE SYS$OUTPUT"
$ clear = "[2J[H"
$ !
$ ! Get logical assignments for MIDAS stuff
$ !
$ if MIDVERS .nes. "" then goto CHECK_HOME
$ echo "Error: MIDVERS must be defined previously"
$ exit
$ !
$ CHECK_HOME:
$ if MIDASHOME .nes. "" then goto START
$ echo "Error: MIDASHOME must be defined previously"
$ exit
$ !
$ ! Interrupt:
$ !
$ !
$ INTERRUPT:
$ close input_file
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ exit
$ !
$ ! Start here:
$ !
$ START:
$ on control_y then goto INTERRUPT
$ on control_c then goto INTERRUPT
$ echo clear
$ SHOW_MENU:
$ echo ""
$ echo "	    MIDAS HELP MENU:"
$ echo "        =======================================
$ echo "	0 - Configmidas."
$ echo "	1 - List of MIDAS packages available."
$ echo "	2 - Select make."
$ echo "	3 - Select graphic display."
$ echo "	4 - Select only MIDAS core."
$ echo "	5 - Select all MIDAS packages."
$ echo "	6 - Select MIDAS core & packages of your choice."
$ echo "	7 - Install MIDAS."
$ echo "	8 - Clean MIDAS."
$ echo "	q - Quit."
$ echo ""
$ inquire SELECT "Select"
$ !
$ echo clear
$ if SELECT .eqs. "0" then goto TYPE_DOC
$ if SELECT .eqs. "1" then goto TYPE_DOC
$ if SELECT .eqs. "2" then goto TYPE_DOC
$ if SELECT .eqs. "3" then goto TYPE_DOC
$ if SELECT .eqs. "4" then goto TYPE_DOC
$ if SELECT .eqs. "5" then goto TYPE_DOC
$ if SELECT .eqs. "6" then goto TYPE_DOC
$ if SELECT .eqs. "7" then goto TYPE_DOC
$ if SELECT .eqs. "8" then goto TYPE_DOC
$ if SELECT .eqs. "Q" then goto FIN
$ goto SHOW_MENU
$ !
$ TYPE_DOC:
$ if SELECT .eqs. "0" then DOC := "configmidas.doc"
$ if SELECT .eqs. "1" then DOC := "listmidas.doc"
$ if SELECT .eqs. "2" then DOC := "selecmake.doc"
$ if SELECT .eqs. "3" then DOC := "selecdisplay.doc"
$ if SELECT .eqs. "4" then DOC := "selecmidas.doc"
$ if SELECT .eqs. "5" then DOC := "selecmidas.doc"
$ if SELECT .eqs. "6" then DOC := "selecmidas.doc"
$ if SELECT .eqs. "7" then DOC := "installmidas.doc"
$ if SELECT .eqs. "8" then DOC := "cleanmidas.doc"
$ !
$ type/page 'DOC'
$ inquire/nopunctuation ANSWER "END OF FILE: Press any key to return... "
$ echo clear
$ goto SHOW_MENU
$ !
$ FIN:
$ set on
$ exit
