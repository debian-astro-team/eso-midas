$ ! @(#)totalinstall.com	19.1 (ESO-IPG) 02/25/03 13:52:29 
$ set verify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]TOTALINSTALL.COM
$ ! .COMMENTS    Midas installation procedure for VMS systems.
$ !              The command executes:
$ !                 -The INSTALLMIDAS.COM
$ ! 
$ ! .DATE       881201   C.Guirao	Creation
$ ! .DATE       890426	 C.Guirao	Including 'MIDASHOME'
$ ! .DATE       910110	 C.Guirao	New directory structure.
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set protection=(s:rewd,o:rewd,g:rewd,w:re)/default
$ set noon
$ !
$ ASSIGN DKA400: MID_DISK:
$ MIDASHOME :== MIDAS
$ MIDVERS :== 95NOV
$ !
$ ! Definitions of C runtime library, and GNU C library.
$ !
$ !@ MISC$DISK:[GNU]PREPARE_GCC.COM
$ delete/nolog/noconfirm MID_DISK:['MIDASHOME'.'MIDVERS'...]*.exe.*
$ set def MID_DISK:['MIDASHOME'.'MIDVERS']
$ purge/nolog/noconfirm/exclude=(*.FOR,*.C,*.C1,*.C2) [...]*.*
$ !
$ ! Export for tomorrow
$ !
$ set def MID_DISK:['MIDASHOME'.'MIDVERS'.INSTALL.VMS]
$ ! submit/queue=pmidas/noprint/after="tomorrow+3:00" TOTALINSTALL.COM
$ submit/noprint/after="tomorrow+1:00" TOTALINSTALL.COM
$ !
$ set noverify
$ @ INSTALLMIDAS/OUTPUT=INSTALLMIDAS.LOG 
$ !
$ set verify
$ set def ['MIDASHOME'.'MIDVERS']
$ purge/nolog/noconfirm/exclude=(*.FOR,*.C,*.C1,*.C2) [...]*.*
$ exit
