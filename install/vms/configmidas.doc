% @(#)configmidas.doc	19.1 (ESO-IPG) 02/25/03 13:52:25

CONFIGMIDAS

    MIDAS configuration DCL procedure.

    CONFIGMIDAS provides help and access to other MIDAS procedures through 
    a menu.

    From CONFIGMIDAS 5 main tasks can be invoked:

    o     MIDAS display configuration.
    o     MIDAS internal data format
    o     MIDAS software configuration.
    o     MIDAS installation.
    o     MIDAS remove procedure.
    o     MIDAS help.

    The official MIDAS distribution from ESO provides by default
    the following configuration:

    o     Only CORE MIDAS packages.
    o     DECC compiler
    o     machine default internal data representation 
          (DEC default is d_float on VAX and g_float on ALPHA). 
    o     Only X11 devices.

  Additional information available:

    In DCL file:  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]CONFIGMIDAS.COM
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]INSTALLMIDAS.COM
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]LIBDEF.COM
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]MAKE.COM
    In configuration files:
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]PREINSTALLMIDAS.CNF
                  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]INSTALLMIDAS.CNF
    In documentation files:   
                  [MIDAS.95NOV.INSTALL.VMS]CLEANMIDAS.DOC
                  [MIDAS.95NOV.INSTALL.VMS]INSTALLMIDAS.DOC
                  [MIDAS.95NOV.INSTALL.VMS]LISTMIDAS.DOC
                  [MIDAS.95NOV.INSTALL.VMS]SELECDISPLAY.DOC
                  [MIDAS.95NOV.INSTALL.VMS]SELECMAKE.DOC
                  [MIDAS.95NOV.INSTALL.VMS]SELECMIDAS.DOC

   
  Examples: 

    1. If your system is ready for the default configuration, you can 
       proceed with INSTALLMIDAS:

       $ SET DEF  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]
       $ @ INSTALLMIDAS           or (See NOTE below)
       $ @ INSTALLMIDAS/OUTPUT=INSTALLMIDAS.LOG

    2. If you want to modify the default MIDAS configuration:

       $ SET DEF  ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]
       $ @ CONFIGMIDAS
       select: 1 	List of MIDAS packages available:
               		All optional packages will be listed, 
			and a brief description can be displayed.
       select: 2 	Select internal data format
       select: 3 	Select display: Deanza, X11 etc...
       select: 4,5,6 	Select only CORE, ALL or at your own selection 
			packages of MIDAS.
       select: 7	Run INSTALLMIDAS sending the output to the screen. 
       select: 8	Clean MIDAS: To remove unnecessary files.
       select: q	QUIT.
       $
       $ @ INSTALLMIDAS/OUTPUT=INSTALLMIDAS.LOG

    NOTE: It is recommended to execute the first time: "@ INSTALLMIDAS"
    since the output will be display on the screen, and simple and 
    corrigible errors can be easily detected. The installation can be then
    interrupted with several CTR-C keystrokes.

    When errors have being corrected, a complete installation can be executed 
    redirecting the output to a log-file.
