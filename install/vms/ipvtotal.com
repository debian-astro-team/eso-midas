$ ! @(#)ipvtotal.com	19.1 (ESO-IPG) 02/25/03 13:52:26 
$ set verify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]TOTALINSTALL.COM
$ ! .COMMENTS    Midas installation procedure for VMS systems.
$ !              The command executes:
$ !                 -The INSTALLMIDAS.COM
$ ! 
$ ! .DATE       881201   C.Guirao	Creation
$ ! .DATE       890426	 C.Guirao	Including 'MIDASHOME'
$ ! .DATE       910110	 C.Guirao	New directory structure.
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set protection=(s:rewd,o:rewd,g:rewd,w:re)/default
$ set noon
$ !
$ ASSIGN DKA300: MID_DISK:
$ MIDASHOME :== MIDAS
$ MIDVERS :== TEST
$ !
$ ! Definitions of C runtime library, and GNU C library.
$ !
$ @ dka100:[GNU]PREPARE_GCC.COM
$ delete MID_DISK:['MIDASHOME'.'MIDVERS'...]*.exe.*
$ set def MID_DISK:['MIDASHOME'.'MIDVERS']
$ purge/nolog/noconfirm/exclude=(*.FOR,*.C,*.C1,*.C2) [...]*.*
$ !
$ ! Export for tomorrow
$ !
$ set def MID_DISK:['MIDASHOME'.'MIDVERS'.INSTALL.VMS]
$ submit/queue=sys$batch/noprint/after="tomorrow+3:00" IPVTOTAL.COM
$ !
$ @ INSTALLMIDAS/OUTPUT=INSTALLMIDAS.LOG 
$ !
$ set verify
$ set def ['MIDASHOME'.'MIDVERS']
$ purge/nolog/noconfirm/exclude=(*.FOR,*.C,*.C1,*.C2) [...]*.*
$ exit
