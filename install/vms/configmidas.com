$ ! @(#)configmidas.com	19.1 (ESO-IPG) 02/25/03 13:52:25
$ set noverify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]CONFIGMIDAS.COM
$ ! .COMMENTS    Midas configuration procedure for VMS systems.
$ ! 
$ ! .REMARKS
$ !
$ ! .DATE	 990111   C.Guirao	Creation
$ ! .DATE	 990716   J-P De Cuyper Update
$ ! .DATE      20000529   J-P De Cuyper Update 64bit pointers
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set noon
$ set protection=(S:REWD,O:REWD,G:REWD,W:R)/DEFAULT
$ !
$ !
$ ! Definitions:
$ !
$ clear = "[2J[H"
$ echo == "WRITE SYS$OUTPUT"
$ !
$ ! Get logical assignments for MIDAS stuff:
$ !
$ if MIDVERS .nes. "" then goto CHECK_HOME
$ echo "Error: MIDVERS must be defined previously"
$ exit
$ !
$ CHECK_HOME:
$ if MIDASHOME .nes. "" then goto START
$ echo "Error: MIDASHOME must be defined previously"
$ exit
$ !
$ ! Start here:
$ !                                                
$ START:
$ on control_y then goto FIN
$ on control_c then goto FIN
$ !
$ ! The original extracted tar files: DISPLAY.CNF and SELECTED.CNF have a
$ ! different format than those edited with procedures. 
$ ! So here their format is changed.
$ !
$ convert DISPLAY.CNF DISPLAY_TMP.CNF
$ rename/nolog/noconfirm DISPLAY_TMP.CNF DISPLAY.CNF
$ convert SELECTED.CNF SELECTED_TMP.CNF
$ rename/nolog/noconfirm SELECTED_TMP.CNF SELECTED.CNF
$ convert MAKE_HEADER.COM MAKE_HEADER_TMP.COM
$ rename/nolog/noconfirm MAKE_HEADER_TMP.COM MAKE_HEADER.COM
$ convert MAKE_POST.COM MAKE_POST_TMP.COM
$ rename/nolog/noconfirm MAKE_POST_TMP.COM MAKE_POST.COM
$ !
$ ! Checking whether this is a VAX or an ALPHA machine.
$ !
$ echo "Checking if this is a VAX or an ALPHA machine...."
$ !CC testvms
$ !if .not. $STATUS then goto NO_ALPHA
$ !LINK testvms
$ !RUN testvms
$ !NO_ALPHA:
$ !
$ HWNAME:='F$GETSYI("HW_NAME")'
$ VERSION:='F$GETSYI("VERSION")'
$ NODENAME:='F$GETSYI("NODENAME")'
$ if F$GETSYI("HW_MODEL") .lt. 1024
$ then
$ echo "This is VAX machine ",NODENAME
$ goto P32
$ else
$ echo "This is ALPHA machine  ",NODENAME
$  if F$GETSYI("VERSION") .ges. "V7" 
$  then 
$  echo "Type ", HWNAME," running VMS ", VERSION  
$  echo "Using int64 flong2c" 
$ goto CHECK
$  else 
$  goto P32
$  endif
$ endif
$!
$ P32:
$ echo "Type ", HWNAME," running VMS ", VERSION  
$ echo "Using int flong2c" 
$ echo "Editing Fortran routines to replace INTEGER*8 by INTEGER*4" 
$ echo "Purging all Fortran routines first"
$ echo "Keeping original Fortran routines as *.for_orig files" 
$ echo "Be patient this may take some time."
$ purge mid_disk:['midashome'.'midvers'...]*.for
$ @substite.com mid_disk:['midashome'.'midvers'...]*.for INTEGER*8  INTEGER*4
$!
$ CHECK:
$ echo "Checking DECC compiler"
$ CC/DECC/VERSION NL:
$ echo "Checking FORTRAN compiler"
$ product show product FORTRAN
$ @ selecmake 
$ !
$ ! Read MIDAS environment:
$ !
$ !echo clear
$ echo "**********************************************************************"
$ echo "******************** MIDAS CONFIGURATION SCRIPT **********************"
$ echo "**********************************************************************"
$ echo "*********************************************************"
$ echo "******************** MIDVERS: ",MIDVERS
$ echo "******************** DATE: ",F$TIME()
$ echo ""
$ SHOW_MENU:
$ echo ""
$ echo "		MIDAS CONFIG MENU:"
$ echo "	========================================"
$ echo "	1 - List of MIDAS packages available."
$ echo "	2 - Select internal data format."
$ echo "	3 - Select display."
$ echo "	4 - Select only MIDAS core."
$ echo "	5 - Select all MIDAS packages."
$ echo "	6 - Select MIDAS core & packages of your choice."
$ echo "	7 - Install MIDAS."
$ echo "	8 - Clean MIDAS."
$ echo "	9 - Help."
$ echo "	q - Quit."
$ echo ""
$ inquire SELECT "Select"
$ !
$ echo SELECT 
$ if SELECT .eqs. "1" then @ listmidas
$ if SELECT .eqs. "2" then @ selecmake
$ if SELECT .eqs. "3" then @ selecdisplay
$ if SELECT .eqs. "4" then @ selecmidas CORE
$ if SELECT .eqs. "5" then @ selecmidas ALL
$ if SELECT .eqs. "6" then @ selecmidas OWN
$ if SELECT .eqs. "7" then @ installmidas RECONFIRM
$ if SELECT .eqs. "8" then @ cleanmidas
$ if SELECT .eqs. "9" then @ helpmidas
$ if SELECT .eqs. "Q" then goto FIN
$ set noverify
$ goto SHOW_MENU
$ !
$ FIN:
$ set on
$ exit
