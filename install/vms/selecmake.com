$ ! @(#)selecmake.com	19.1 (ESO-IPG) 02/25/03 13:52:28
$ set noverify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]SELECMAKE.COM
$ ! .COMMENTS    Midas configuration procedure for VMS systems.
$ ! 
$ ! .REMARKS
$ !
$ ! .DATE	 990111   C.Guirao	Creation
$ ! .DATE	 960715   J-P De Cuyper Update
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set noon
$ set protection=(S:REWD,O:REWD,G:REWD,W:R)/DEFAULT
$ !
$ ! Definitions:
$ !
$ echo == "WRITE SYS$OUTPUT"
$ clear = "[2J[H"
$ !
$ ! Get logical assignments for MIDAS stuff
$ !
$ if MIDVERS .nes. "" then goto CHECK_HOME
$ echo "Error: MIDVERS must be defined previously"
$ exit
$ !
$ CHECK_HOME:
$ !if MIDASHOME .nes. "" then goto START
$ if MIDASHOME .nes. "" then goto CHECK_ALPHA
$ echo "Error: MIDASHOME must be defined previously"
$ exit
$ !
$ ! Checking whether this is a VAX or an ALPHA machine
$ ! 
$ !CHECK_ALPHA: 
$ !OPEN/READ LOG_FILE test.log
$ !READ/END_OF_FILE=ERROR LOG_FILE RECORD
$ !NN = F$LOCATE("ALPHA_VMS",RECORD)
$ !IF NN .EQ. F$LENGTH(RECORD) THEN GOTO NO_ALPHA
$ !VMS == "ALPHA"
$ !CLOSE LOG_FILE
$ !GOTO START
$ !
$ ERROR:
$ !CLOSE LOG_FILE
$ !echo "ERROR: TEST.LOG IS EMPTY"
$ EXIT
$ !
$ !NO_ALPHA:
$ !CLOSE LOG_FILE
$ !VMS == "NOALPHA"
$ !GOTO START 
$ !
$ CHECK_ALPHA:
$ if F$GETSYI("HW_MODEL") .lt. 1024
$ then
$ echo "This is VAX machine ",NODENAME
$ VMS == "NOALPHA"
$ else
$ echo "This is ALPHA machine  ",NODENAME
$ VMS == "ALPHA"
$ endif   
$ !
$ GOTO START 
$ !
$ INTERRUPT:
$ close tmp_file
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ exit
$ !
$ !
$ ! Start here:
$ !
$ START:
$ on control_y then exit
$ on control_c then goto INTERRUPT
$ set message/nofacility - 
        /noidentification - 
        /noseverity - 
        /notext
$ echo clear
$ if VMS .eqs. "ALPHA" then goto SHOW_MENUAXP
$ SHOW_MENUVAX:
$ echo ""
$ echo "	INTERNAL DATA REPRESENTATION FOR MIDAS ON VAX:"
$ echo "	=============================================="
$ echo "	1 - d_float" 
$ echo "	2 - g_float"
$ echo "	q - Quit."
$ echo ""   
$ !
$ inquire SELECT "Select"
$ !
$ if SELECT .eqs. ""  then goto VAX_D
$ if SELECT .eqs. "1" then goto VAX_D
$ if SELECT .eqs. "2" then goto VAX_G
$ if SELECT .eqs. "Q" then goto FIN
$ goto SHOW_MENUVAX
$ ! 
$ SHOW_MENUAXP:
$ echo ""
$ echo "	INTERNAL DATA REPRESENTATION FOR MIDAS ON ALPHA:"
$ echo "	================================================"
$ echo "	1 - g_float" 
$ echo "	2 - ieee_float"
$ echo "	q - Quit."
$ echo ""   
$ !
$ inquire SELECT "Select"
$ !
$ if SELECT .eqs. ""  then goto AXP_G
$ if SELECT .eqs. "1" then goto AXP_G
$ if SELECT .eqs. "2" then goto IEEE
$ if SELECT .eqs. "Q" then goto FIN
$ goto SHOW_MENUAXP
$ !
$ VAX_D:
$ DCC := CC/decc/STANDARD=VAXC/EXTERN=COMMON/NODEBUG/NOLIST/float=d_float/WARNING=DISABLE=MISSINGRETURN"
$ DECFOR := fortran/align=al/nodeb/nolist/d_float/warn=(align,alp,nodecla,gen,uncall,unin,nounus,usa)
$ goto TEST
$!
$ VAX_G:
$ DCC := CC/decc/STANDARD=VAXC/EXTERN=COMMON/NODEBUG/NOLIST/g_float/WARNING=DISABLE=MISSINGRETURN"
$ DECFOR := fortran/align=al/nodeb/nolist/g_float/warn=(align,alp,nodecla,gen,uncall,unin,nounus,usa)
$ goto TEST
$ !
$ AXP_G:
$  if F$GETSYI("VERSION") .ges. "V7" 
$  then 
$  DCC := CC/decc/STANDARD=VAXC/EXTERN=COMMON/NODEBUG/NOLIST/g_float/WARNING=DISABLE=(MISSINGRETURN,INTRINSICINT)
$  else 
$  DCC := CC/decc/STANDARD=VAXC/EXTERN=COMMON/NODEBUG/NOLIST/g_float/WARNING=DISABLE=(MISSINGRETURN)
$  endif
$ DECFOR := fortran/align=al/nodeb/nolist/g_float/warn=(align,noarg,nodecla,gen,uncall,unin,nounus,usa)
$ goto TEST
$ !
$ IEEE:
$  if F$GETSYI("VERSION") .ges. "V7" 
$  then 
$  DCC := CC/decc/STANDARD=VAXC/EXTERN=COMMON/NODEBUG/NOLIST/float=ieee/WARNING=DISABLE=(MISSINGRETURN,INTRINSICINT)
$  else 
$  DCC := CC/decc/STANDARD=VAXC/EXTERN=COMMON/NODEBUG/NOLIST/float=ieee/WARNING=DISABLE=(MISSINGRETURN)
$  endif
$ DECFOR := fortran/align=al/nodeb/nolist/float=ieee/warn=(align,noarg,nodecla,gen,uncall,unin,nounus,usa)
$ goto TEST
$ !
$ TEST:
$ !
$ DCC MYTEST.C
$ if .not. $STATUS then goto CERR
$ goto MAKE
$ CERR:
$ echo "Command: CC MYTEST.C did not work. EXIT."
$ echo $status
$ goto END
$ !
$ MAKE:
$ echo "Internal data representation selected."
$ echo "$ CC := ",DCC
$ echo "$ FORTRAN := ",DECFOR
$ copy MAKE_HEADER.COM MAKE_TMP.COM
$ open/append tmp_file MAKE_TMP.COM
$ write/symbol tmp_file "$ CC := ",DCC
$ write/symbol tmp_file "$ FORTRAN := ",DECFOR
$ if VMS .eqs. "NOALPHA" then goto MAKE1
$ DECMA := "MACRO/MIGRATION"
$ echo "$ MACRO := ",DECMA 
$ write/symbol tmp_file "$ MACRO := ",DECMA 
$ MAKE1:
$ close tmp_file
$ copy MAKE_TMP.COM,MAKE_POST.COM MAKE.COM
$ echo ""
$ !
$ END:
$ purge/nolog/noconfirm MAKE*.COM,MYTEST.OBJ,SYSTEMLIB.DAT
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ !
$ exit
$ !!!
$ !
$ ! The following is not used anymore !!!
$ !
$ !echo clear
$ !SHOW_MENU:
$ !echo ""
$ !echo "	    C COMPILERS FOR MIDAS:"
$ !echo "	=========================="
$ !echo "	1 - VAX/VMS C Compiler."
$ !echo "	2 - GNU C Compiler."
$ !echo "	q - Quit."
$ !echo ""
$ !
$ inquire SELECT "Select"
$ !
$ if SELECT .eqs. "1" then goto COMMON
$ if SELECT .eqs. "2" then goto COMMON
$ if SELECT .eqs. "Q" then goto FIN
$ goto SHOW_MENU
$ !
$ COMMON:
$ show logical/output=SYSTEMLIB.DAT lnk$library*
$ if SELECT .eqs. "1" then goto VMS_C
$ if SELECT .eqs. "2" then goto GNU_C
$ echo "This message should never appear. EXIT."
$ goto FIN
$ ! 
$ GNU_C:
$ ! 
$ ! Checks for the existance of a library GCCLIB.OLB
$ ! 
$ open/read tmp_file SYSTEMLIB.DAT
$ READMORE:
$ read/end_of_file=GNU_NOTFOUND tmp_file RECORD
$ nn = f$locate("GCCLIB.OLB",RECORD)
$ if nn .eq. f$length(RECORD) then goto READMORE
$ echo "GNU C Library GCCLIB.OLB found."
$ close tmp_file
$ goto VMS_C
$ !
$ GNU_NOTFOUND:
$ close tmp_file
$ echo "GNU C Library GCCLIB.OLB not found. EXIT."
$ goto FIN
$ ! 
$ ! Checks for the existance of a library VAXCRTL.OLB (only in old VMS)
$ ! 
$ VMS_C:
$ if VMS .eqs. "ALPHA" then goto CONTINUE
$ open/read tmp_file SYSTEMLIB.DAT
$ READ_MORE:
$ read/end_of_file=VMS_NOTFOUND tmp_file RECORD
$ nn = f$locate("VAXCRTL.OLB",RECORD)
$ if nn .eq. f$length(RECORD) then goto READ_MORE
$ echo "VAX/VMS C Library VAXCRTL.OLB found"
$ close tmp_file
$ goto CONTINUE
$ !
$ VMS_NOTFOUND:
$ close tmp_file
$ echo "VAX/VMS C Library VAXCRTL.OLB NOT found. EXIT."
$ goto FIN
$ !
$ ! Creates file MAKE.COM
$ !
$ CONTINUE:
$ if SELECT .eqs. "1" then goto CVMS_C
$ if SELECT .eqs. "2" then goto CGNU_C
$ echo "This message should never appear. EXIT."
$ goto FIN
$ !
$ CVMS_C:
$ echo "Testing VAX/VMS C compiler as <CC>..."
$ if VMS .eqs. "ALPHA" then goto ALPHA_CC
$ ! ENTRY = "$ CC:= CC/OPTIMIZE/NODEBUG/NOLIST"
$ ENTRY = "$ CC:= CC/decc/STANDARD=VAXC/EXTERN=COMMON/NODEBUG/NOLIST/g_float/WARNING=DISABLE=MISSINGRETURN"
$ cc/optimize/nodebug/nolist MYTEST.C
$ if .not. $STATUS then goto NO_CC
$ goto CONTINUE1
$ !fortran:=fortran/align=al/nodeb/nolist/warn=(align,argu,nodecla,gen,uncall,unin,nounus,usa)
$ ! fortran:=fortran/nodeb/nolist/warn=(align,argu,decla,gen,uncall,unin,nounus,usa)/float=ieee
$ !
$ ALPHA_CC:
$ ! 
$ ENTRY = "$ CC := CC/decc/STAND=VAXC/EXT=COM/NODEB/NOLIS/WAR=DISAB=MISSINGRETURN"
$ ! ENTRY = "$ CC := CC/decc/STAND=VAXC/EXT=COM/NODEB/NOLIS/WAR=DISAB=MISSINGRETURN/float=ieee"
$ ! ENTRY = "$ CC:= CC/decc/STANDARD=VAXC/EXTERN=COMMON/NODEBUG/NOLIST/g_float/WARNING=DISABLE=MISSINGRETURN"
$ ! ENTRY = "$ CC:= CC/decc/STANDARD=VAXC/EXTERN=COMMON/OPTIMIZE/NODEBUG/NOLIST/WARNING=DISABLE=MISSINGRETURN"
$ ! ENTRY = "$ CC:= CC/decc/STANDARD=VAXC/EXTERN=COMMON/NOOPTIMIZE/NODEBUG/NOLIST/WARNING=DISABLE=MISSINGRETURN"
$ cc/standard=vaxc/extern=common MYTEST.C
$ if .not. $STATUS then goto NO_CC
$ goto CONTINUE1
$ !
$ CGNU_C:
$ ENTRY = "$ CC:= GCC/OPTIMIZE/NODEBUG/NOLIST"
$ echo "Testing GNU C compiler as <GCC>..."
$ gcc/optimize/nodebug/nolist mytest.c
$ if .not. $STATUS then goto NO_GCC
$ goto CONTINUE1
$ !
$ NO_CC:
$ echo "Command: CC TEST.C did not work. EXIT."
$ goto FIN
$ NO_GCC:
$ echo "Command: GCC TEST.C did not work. EXIT."
$ goto FIN
$ !
$ !
$ CONTINUE1:
$ echo "Compiler is OK."
$ Inquire ANSWER "Do you want to use this C compiler [yn]? (y)"
$ if ANSWER .eqs. "" then goto CREA_MAKE
$ if ANSWER .eqs. "Y" then goto CREA_MAKE
$ echo "Compiler not selected."
$ GOTO FIN
$ ! 
$ ! now create the new make.com file
$ !
$ CREA_MAKE:
$ copy MAKE_HEADER.COM MAKE_TMP.COM
$ open/append tmp_file MAKE_TMP.COM
$ write/symbol tmp_file ENTRY
$ if VMS .eqs. "NOALPHA" then goto CREA_MAKE1
$ ENTRY = "$ MACRO:= MACRO/MIGRATION"
$ write/symbol tmp_file ENTRY
$ CREA_MAKE1:
$ close tmp_file
$ copy MAKE_TMP.COM,MAKE_POST.COM MAKE.COM
$ echo "Compiler selected."
$ echo ""
$ !
$ FIN:
$ purge/nolog/noconfirm MAKE*.COM,MYTEST.OBJ,SYSTEMLIB.DAT
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ exit
