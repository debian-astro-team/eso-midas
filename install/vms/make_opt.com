$ ! @(#)make_opt.com	14.1.1.1 (ESO-IPG) 09/16/99 10:00:58
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! Command file MAKE_OPTIONS.COM
$ !
$ ! Defines the Fortran and CC symbols specifying the floating point
$ ! representation and other compilation options.
$ !
$ ! J-P De Cuyper 20000608
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! Here comes the definition of CC and FORTRAN done by SELECMAKE.COM
