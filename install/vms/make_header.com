$ ! @(#)make_header.com	19.1 (ESO-IPG) 02/25/03 13:52:27
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! Command file MAKE.COM
$ !
$ ! Executes a makefile (makefile.com by default) in working directory
$ ! after defining some symbols.
$ !
$ ! T. Melen    891128
$ ! C. Guirao   910130	The real MAKE.COM file is now created by command
$ !                     SELECMAKE.COM.
$ ! J-P De Cuyper 960716 Update, new compiler options
$ !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! Here comes the definition of CC and FORTRAN done by SELECMAKE.COM
