$ ! @(#)selecdisplay.com	19.1 (ESO-IPG) 02/25/03 13:52:28 
$ set noverify
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       ['MIDASHOME'.'MIDVERS'.INSTALL.VMS]SELECDISPLAY.COM
$ ! .COMMENTS    Midas configuration procedure for VMS systems.
$ ! 
$ ! .REMARKS
$ !
$ ! .DATE	 990111   C.Guirao	Creation
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ set noon
$ set protection=(S:REWD,O:REWD,G:REWD,W:R)/DEFAULT
$ !
$ ! Definitions:
$ !
$ echo == "WRITE SYS$OUTPUT"
$ clear = "[2J[H"
$ !
$ ! Get logical assignments for MIDAS stuff
$ !
$ if MIDVERS .nes. "" then goto CHECK_HOME
$ echo "Error: MIDVERS must be defined previously"
$ exit
$ !
$ CHECK_HOME:
$ if MIDASHOME .nes. "" then goto START
$ echo "Error: MIDASHOME must be defined previously"
$ exit
$ !
$ ! Interrupt:
$ !
$ !
$ INTERRUPT:
$ close tmp_file
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ exit
$ !
$ !
$ ! Start here:
$ !
$ START:
$ on control_y then goto INTERRUPT
$ on control_c then goto INTERRUPT
$ set message/nofacility -
	/noidentification -
	/noseverity -
	/notext
$ echo clear
$ SHOW_MENU:
$ echo ""
$ echo "	    MIDAS GRAPHIC DISPLAY MENU:"
$ echo "	===================================="
$ echo "	1 - No display at all."
$ echo ""
$ echo "	    WITH C INTERFACE:"
$ echo "	    -----------------"
$ echo "	2 - X11 display."
$ echo "	3 - Other display."
$ echo ""
$ echo "	    WITH FORTRAN INTERFACE:"
$ echo "	    -----------------------"
$ echo "	4 - A Deanza display."
$ echo "	5 - A Deanza display & Other display."
$ echo "	6 - Other display."
$ echo "	q - Quit."
$ echo ""
$ inquire SELECT "Select"
$ !
$ if SELECT .eqs. "1" then goto COMMON
$ if SELECT .eqs. "2" then goto COMMON
$ if SELECT .eqs. "3" then goto COMMON
$ if SELECT .eqs. "4" then goto COMMON
$ if SELECT .eqs. "5" then goto COMMON
$ if SELECT .eqs. "6" then goto COMMON
$ if SELECT .eqs. "Q" then goto FIN
$ echo "Option Unknown"
$ goto SHOW_MENU
$ 
$ COMMON:
$ convert HEADER.CNF DISPLAY_TMP.CNF
$ open/append TMP_FILE DISPLAY_TMP.CNF
$ if SELECT .eqs. "1" then goto NO_DISPLAY
$ if SELECT .eqs. "2" then goto C_X11
$ if SELECT .eqs. "3" then goto C_OTHER
$ if SELECT .eqs. "4" then goto F_DEANZA
$ if SELECT .eqs. "5" then goto F_DEANZA_OTHER
$ if SELECT .eqs. "6" then goto F_OTHER
$ !
$ NO_DISPLAY:
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FNIDI]"
$ write/symbol tmp_file ENTRY 
$ goto CHECK_CNF
$ !
$ C_X11:
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.CIDI]"
$ write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.CIDI.X11]"
$ write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'.SYSTEM.IDISERV.SRC]"
$ write/symbol tmp_file ENTRY 
$ goto CHECK_CNF
$ !
$ C_OTHER:
$ echo ""
$ inquire ANSWER "Type name of display"
$ if ANSWER .eqs. "" then ANSWER := "NEWDEV"
$ FILE=f$search("[''MIDASHOME'.''MIDVERS'.LIBSRC.IDI.CIDI]''ANSWER'.DIR")
$ if FILE .nes. "" then goto C_OTHER_OK
$ echo "ERROR: Cannot find in ['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.CIDI]"
$ echo "       a subdirectory with that name."
$ goto SHOW_MENU
$ !
$ C_OTHER_OK:
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.CIDI]"
$ write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.CIDI."'ANSWER'"]"
$ write/symbol tmp_file ENTRY 
$ goto CHECK_CNF
$ !
$ F_DEANZA:
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI]"
$ write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI.DEANZA]"
$ write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI.NEWDEV]"
$ write/symbol tmp_file ENTRY 
$ goto CHECK_CNF
$ !
$ F_DEANZA_OTHER:
$ echo ""
$ inquire ANSWER "Type name of the other display (default=newdev)"
$ if ANSWER .eqs. "" then ANSWER := "NEWDEV"
$ FILE=f$search("[''MIDASHOME'.''MIDVERS'.LIBSRC.IDI.FIDI]''ANSWER'.DIR")
$ if FILE .nes. "" then goto F_DEANZA_OK
$ echo "ERROR: Cannot find in ['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI]"
$ echo "       a subdirectory with that name."
$ goto SHOW_MENU
$ !
$ F_DEANZA_OK:
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI]"
$ write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI.DEANZA]"
$ write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI."'ANSWER'"]"
$ write/symbol tmp_file ENTRY 
$ goto CHECK_CNF
$ !
$ F_OTHER:
$ echo ""
$ inquire ANSWER "Type name of display (default=newdev)"
$ if ANSWER .eqs. "" then ANSWER := "NEWDEV"
$ FILE=f$search("[''MIDASHOME'.''MIDVERS'.LIBSRC.IDI.FIDI]''ANSWER'.DIR")
$ if FILE .nes. "" then goto F_DEANZA_OK
$ echo "ERROR: Cannot find in ['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI]"
$ echo "       a subdirectory with that name."
$ inquire/nopunctuation ANSWER "Type any key to continue..."
$ goto SHOW_MENU
$ !
$ F_OTHER_OK:
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI]"
$ write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI.NODEANZA]"
$ write/symbol tmp_file ENTRY 
$ ENTRY = "['MIDASHOME'.'MIDVERS'.LIBSRC.IDI.FIDI."'ANSWER'"]"
$ write/symbol tmp_file ENTRY 
$ goto CHECK_CNF
$
$ CHECK_CNF:
$ close tmp_file
$ echo ""
$ inquire ANSWER "Do you want the selected configuration [yn])? (y)"
$ if ANSWER .eqs. "" then goto UPDATE_CNF
$ if ANSWER .eqs. "Y" then goto UPDATE_CNF
$ echo "Configuration not selected."
$ echo ""
$ goto FIN
$ !
$ UPDATE_CNF:
$ rename/nolog/noconfirm DISPLAY_TMP.CNF DISPLAY.CNF
$ echo "Configuration selected."
$ echo ""
$ goto FIN
$ !
$ FIN:
$ purge DISPLAY_TMP.CNF
$ set on
$ set message/facility -
	/identification -
	/severity -
	/text
$ exit
