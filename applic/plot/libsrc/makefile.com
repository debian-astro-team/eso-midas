$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.APPLIC.PLOT.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:25:03 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   calfon.for
$ FORTRAN   intfr.for
$ FORTRAN   plflux.for
$ FORTRAN   spflx.for
$ FORTRAN   getpix.for
$ FORTRAN   coord.for
$ FORTRAN   alint.for
$ FORTRAN   sort2.for
$ LIB/REPLACE libaplot calfon.obj,intfr.obj,plflux.obj,spflx.obj,getpix.obj,coord.obj,alint.obj,sort2.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
