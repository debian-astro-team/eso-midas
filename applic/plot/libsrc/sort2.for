C @(#)sort2.for	19.1 (ES0-DMD) 02/25/03 13:19:39
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE SORT2(RA,RB,N)                                                 
C+++                                                                            
C.IDENTIFICATION                                                                
C.AUTHOR:   M.Rosa, ESO-Garching, 831105                                        
C.PURPOSE:  Sort string of values into ascending sequence of values             
C.ALGORITM: The art of scientific computing                                     
C           Press et al., Cambridge university Press, 1986                      
C.USE:      CALL SORT2(XC,YC,N)                                                 
C.VERSION:  910115 RHW IMPLICIT NONE added
C---                                                                            
C     
      IMPLICIT   NONE
      INTEGER    N
      REAL       RA(N)
      REAL       RB(N)                                                         
C
      INTEGER    I, IR
      INTEGER    J
      INTEGER    L
      REAL       RRA, RRB
                                                                                
      L  = N/2 + 1                                                              
      IR = N                                                                    
                                                                                
10    CONTINUE                                                                  
      IF (L.GT.1) THEN                                                          
         L      = L - 1                                                         
         RRA    = RA(L)                                                         
         RRB    = RB(L)                                                         
      ELSE                                                                      
         RRA    = RA(IR)                                                        
         RRB    = RB(IR)                                                        
         RA(IR) = RA(1)                                                         
         RB(IR) = RB(1)                                                         
         IR     = IR - 1                                                        
                                                                                
         IF (IR.EQ.1) THEN                                                      
            RA(1) = RRA                                                         
            RB(1) = RRB                                                         
            RETURN                                                              
         ENDIF                                                                  
      ENDIF                                                                     
                                                                                
      I = L                                                                     
      J = L+L                                                                   
                                                                                
20    CONTINUE                                                                  
      IF (J.LE.IR) THEN                                                         
         IF (J.LT.IR) THEN                                                      
            IF (RA(J).LT.RA(J+1)) THEN                                          
               J = J + 1                                                        
            ENDIF                                                               
         ENDIF                                                                  
                                                                                
         IF (RRA.LT.RA(J)) THEN                                                 
            RA(I) = RA(J)                                                       
            RB(I) = RB(J)                                                       
            I     = J                                                           
            J     = J + J                                                       
         ELSE                                                                   
            J     = IR + 1                                                      
         ENDIF                                                                  
         GO TO 20                                                               
      ENDIF                                                                     
                                                                                
      RA(I) = RRA                                                               
      RB(I) = RRB                                                               
      GO TO 10                                                                  
      END                                                                       
