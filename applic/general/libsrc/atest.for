C @(#)atest.for	19.1 (ES0-DMD) 02/25/03 13:18:59
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE ATEST(NX,NY,IN,HG,HL,OUT,M,OG,UL,OL,
     >                 NSG,NSL,ORD,TYP)
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C  compares actual gradient and Laplaces with noise statistics and decides
C  if a pixel is significant on the actual order
C  OG,UL,OL : upper limit for gradients and lower and upper limits for 
C  Laplace-terms respectively; pixel values exceeding these
C  limits make the pixel significant. (in)
C  NSG,NSL  : number of pixels wich became significant at the actual 
C  order by gradient or Laplace respectively. (out)
C
C----------------------------------------------------------------------------
      IMPLICIT NONE
C
      CHARACTER*1 S,TYP
      INTEGER     NX,NY,ORD,NSG,NSL,I
      REAL       IN(1),HG(1),HL(1),OUT(1),OG,UL,OL,HHL,ORDR,M(1)
C
      ORDR=ORD
      S='L'
      NSG=0
      NSL=0
C
      DO 10 I=1,NX*NY
      IF (M(I).NE.0) GOTO 10          ! pixel already significant on lower order
      IF (TYP.NE.'S') GOTO 5
C
C *** test for filter type SMOOTH
      IF (HG(I).GE.OG) GOTO 2                     ! gradient significant
      HHL=HL(I)
      IF ((HHL.GE.OL).OR.(HHL.LE.UL)) GOTO 3      ! Laplace significant
      GOTO 10
C
    2 S = 'G'
    3 IF (S.EQ.'G') NSG=NSG+1
      IF (S.EQ.'L') NSL=NSL+1
      S='L'
      OUT(I)=IN(I)
      GOTO 9
C
    5 IF (TYP.NE.'G') GOTO 7

C *** test for filter type GRADIENT
      HHL=HG(I)
      IF(HHL.LT.OG) GOTO 10                       ! gradient not significant
      OUT(I)=HHL
      NSG=NSG+1
      GOTO 9
C
C *** test for filter type LAPLACE
    7 HHL=HL(I)
      IF ((HHL.LT.OL).AND.(HHL.GT.UL)) GOTO 10    ! Laplace not significant
      OUT(I)=HHL
      NSL=NSL+1
    9 M(I)=ORDR! mark significant pixel
   10 CONTINUE
C
      RETURN
      END
