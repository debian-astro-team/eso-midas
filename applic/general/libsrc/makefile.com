$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.APPLIC.GENERAL.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:25:02 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   abox.for
$ FORTRAN   afido.for
$ FORTRAN   ainfo.for
$ FORTRAN   aini.for
$ FORTRAN   atest.for
$ LIB/REPLACE libagen abox.obj,afido.obj,ainfo.obj,aini.obj,atest.obj,filtlib.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
