C @(#)ainfo.for	19.1 (ES0-DMD) 02/25/03 13:18:59
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
       SUBROUTINE AINFO(ORD)
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C  writes information on the noise statistics and filter performance
C  to the terminal:
C      ORD   : order
C      EG    : exspectation value by noise of gradients at the given order
C      SG    : standard deviation of gradients
C      ES    : exspectation value of Laplace-terms
C      EL    : standard deviation of Laplace-terms
C      NSG   : number of pixels wich became significant by the gradient
C      NSL   : number of pixels wich became significant by Laplace
C      NREST : number of pixels wich did not become significant at
C      any order. They are set by the maximal filter size.
C---------------------------------------------------------------------
       IMPLICIT NONE
C
       INTEGER       NSG,NSL,NREST,ORD,I,ST
       REAL          EG,SG,EL,SL
       CHARACTER*80  A
C
       COMMON        /TEST/EG(16),SG(16),EL(16),SL(16),NSG(16),
     >                     NSL(16),NREST
 100   FORMAT(' ',1X,'ORD',7X,'EG',11X,'SG',11X,'EL',11X,
     >        'SL',11X,'NSG',7X,'NSL')
 200   FORMAT (' ',1X,I3,4(1X,G12.3),2(I10))
 300   FORMAT (' ',60X,'NREST:',I10)
C
       WRITE(A,100)
       CALL STTPUT(A,ST)
C
       DO I=2,ORD
          WRITE (A,200) I,EG(I),SG(I),EL(I),SL(I),NSG(I),NSL(I)
          CALL STTPUT(A,ST)
       ENDDO
C
       WRITE (A,300) NREST
       CALL STTPUT(A,ST)
C
       RETURN
       END
