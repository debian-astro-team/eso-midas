/* @(#)filtlib.c	19.1 (ESO-DMD) 02/25/03 13:18:59 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program; if not, write to the Free
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
  MA 02139, USA.

  Correspondence concerning ESO-MIDAS should be addressed as follows:
        Internet e-mail: midas@eso.org
        Postal address: European Southern Observatory
                        Data Management Division
                        Karl-Schwarzschild-Strasse 2
                        D 85748 Garching bei Muenchen
                        GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.COPYRIGHT:  Copyright (c) 1987 European Southern Observatory,
                                         all rights reserved
.IDENTIFIER  module filtlib
.LANGUAGE    C
.AUTHOR      K. Banse                   IPG-ESO Garching
.KEYWORDS
.COMMENTS    holds fits interfaces (based on Midas std. intf.)
	     needed for the different modules of T.B.Georgiev


.ENVIRONment MIDAS
             #include <midas_def.h>      Prototypes for MIDAS interfaces

.VERSIONS    1.00       980708  implementation
--------------------------------------------------------------*/

#include <stdio.h>
#include <midas_def.h>


/*

*/

void fits_put_data(imno,nrow,nstart,jbuf,nend)
int  imno, nrow, nstart, nend;
short int *jbuf;

{
char  *pntra;

int  npix[2], nullo, uni, mapsize, felem, iav;


(void) SCDRDI(imno,"NPIX",1,2,&iav,npix,&uni,&nullo);

felem = (nrow * npix[0]) + nstart + 1;
mapsize = nend - nstart;
pntra = (char *)jbuf;
(void) SCFPUT(imno,felem,mapsize,pntra);
}

/*

*/

void fits_get_data(imno,nrow,nstart,jbuf,nend)
int  imno, nrow, nstart, nend;
short int *jbuf;
{
char  *pntra;

int  npix[2], nullo, uni, mapsize, felem, iav;


(void) SCDRDI(imno,"NPIX",1,2,&iav,npix,&uni,&nullo);

felem = (nrow * npix[0]) + nstart + 1;
mapsize = nend - nstart;
pntra = (char *)jbuf;
(void) SCFGET(imno,felem,mapsize,&iav,pntra);
}

/*

*/

void fits_close(imno)
int  imno;

{
(void) SCFCLO(imno);
}

/*

*/

int fits_open(name,dummy,nr,nc)
char  *name, *dummy;
int   *nr, *nc;

{
int  imno, iav, uni, nullo, npix[2];

imno = 0;
if (*dummy == 'r')
   {
   *nr = *nc = 1;
   (void) SCFOPN(name,D_I2_FORMAT,0,F_IMA_TYPE,&imno);
   (void) SCDRDI(imno,"NPIX",1,2,&iav,npix,&uni,&nullo);
   *nr = npix[1];
   *nc = npix[0];
   }

else
   {
   int  size;

   npix[0] = *nc;
   npix[1] = *nr;
   size = npix[0] * npix[1];
   (void) SCFCRE(name,D_I2_FORMAT,F_O_MODE,F_IMA_TYPE,size,&imno);
   size = 2;
   (void) SCDWRI(imno,"NAXIS",&size,1,1,&uni);
   (void) SCDWRI(imno,"NPIX",npix,1,2,&uni);
   }

return (imno);
}

/*

*/

void find(pattrn,strng,value)
char  *strng, *pattrn;
float *value;

{
char  realpat[4], tmp[80];

int  nn, ibuf;

double  dbuf;



realpat[0] = *pattrn;
realpat[1] = '=';
realpat[2] = '\0';

nn = CGN_INDEXS(strng,realpat);
if (nn < 0)
   {
   realpat[0] = CGN_UPPER(*pattrn);
   nn = CGN_INDEXS(strng,realpat);
   }

if (nn > -1)
   {
   nn = CGN_CNVT(&strng[nn+2],2,1,&ibuf,value,&dbuf);
   if (nn == 1) return;
   }
   
sprintf(tmp,"invalid input: %s, pattern = %s\n",strng,realpat);
SCTPUT(tmp);

*value = -9999.999;
}

