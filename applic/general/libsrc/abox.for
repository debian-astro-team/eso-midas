C @(#)abox.for	19.1 (ES0-DMD) 02/25/03 13:18:58
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE abox(NX,NY,MT,IN,H0,HG,HL,EG,EL,SG,SL,V,YMT)
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C     executes one order of the algorithm
C     V : shift parametre for H-transform; minimum=2 (in)
C     other parameters as in afido
C
C--------------------------------------------------------------------------
      IMPLICIT    NONE
      LOGICAL     L
      CHARACTER*1 YMT
      INTEGER     NX,NY,V,V1,V2,V21,NXV,NYV,NXV1,NYV1,JV,IV,
     >            J2,I2,I,J,T
      REAL        IN(NX,NY),H0(NX,NY),HG(NX,NY),HL(NX,NY),MT(NX,NY),
     >            EG,EL,SG,SL,A,B,C,D,HX,HY,H,QG,QL,NP,NPP
C
      V1=V
      V2=V1/2
      L=.FALSE.
      NP=0.
      EL=NP
      EG=NP
      QL=NP
      QG=NP

      DO J=1,NY-V1
         JV=J+V1
         J2=J+V2
         DO I=1,NX-V1
            IV=I+V1
            I2=I+V2
            A=IN(I,J)
            B=IN(IV,J)
            C=IN(I,JV)
            D=IN(IV,JV)
            H=(A+B+C+D)/4.
            H0(I2,J2)=H                      ! smooth
            H=IN(I2,J2)-H
            HL(I2,J2)=H                      ! Laplace
            IF (YMT.EQ.'N') GOTO 5           ! no mask for statistics
            T=MT(I2,J2)
            IF (T.NE.0) L=.TRUE.
            IF (L) GOTO 6                    ! pixel not used for statistics
    5       NPP=NP
            NP=NP+1
            NPP=NPP/NP
            H=H-EL
            EL=EL+H/NP                       ! estimate statistics
            QL=QL+H*H*NPP                    ! for Laplace
    6       HX=(A-B+C-D)/4.
            HY=(A+B-C-D)/4.
            H=SQRT(HX*HX+HY*HY)
            HG(I2,J2)=H                      ! gradient
            IF (YMT.EQ.'N') GOTO 7           ! no mask for statistics
            IF (L) GOTO 10                   ! pixel not used for statistics
    7       H=H-EG
            EG=EG+H/NP                       ! estimate statistics
            QG=QG+H*H*NPP                    ! for gradient
   10       L = .FALSE.
         ENDDO
      ENDDO
C
C *** correct statistics
      NP=NP-1
      SL=SQRT(QL/NP)
      SG=SQRT(QG/NP)
C
C *** set edge lines and corners
      V21=V2+1
      NXV=NX-V2
      NYV=NY-V2
      NXV1=NXV+1
      NYV1=NYV+1

      DO I=V21,NXV                           ! edge rows
         A=H0(I,V21)
         B=HG(I,V21)
         C=HL(I,V21)
         DO J=1,V2                           ! up
            H0(I,J)=A
            HG(I,J)=B
            HL(I,J)=C
         ENDDO
         A=H0(I,NYV)
         B=HG(I,NYV)
         C=HL(I,NYV)
         DO J=NYV1,NY                        ! down
            H0(I,J)=A
            HG(I,J)=B
            HL(I,J)=C
        ENDDO
      ENDDO

      DO J=V21,NYV                            ! edge columns
         A=H0(V21,J)
         B=HG(V21,J)
         C=HL(V21,J)
         DO I=1,V2                            ! left
            H0(I,J)=A
            HG(I,J)=B
            HL(I,J)=C
         ENDDO
         A=H0(NXV,J)
         B=HG(NXV,J)
         C=HL(NXV,J)
         DO I=NXV1,NX                         ! right
            H0(I,J)=A
            HG(I,J)=B
            HL(I,J)=C
         ENDDO
      ENDDO

      A=H0(V21,V21)
      B=HG(V21,V21)
      C=HL(V21,V21)
      DO I=1,V2                              ! upper left corner
         DO J=1,V2
            H0(I,J)=A
            HG(I,J)=B
            HL(I,J)=C
         ENDDO
      ENDDO

      A=H0(NXV,V21)
      B=HG(NXV,V21)
      C=HL(NXV,V21)
      DO I=NXV1,NX                           ! upper right corner
         DO J=1,V2
            H0(I,J)=A
            HG(I,J)=B
            HL(I,J)=C
         ENDDO
      ENDDO

      A=H0(V21,NYV)
      B=HG(V21,NYV)
      C=HL(V21,NYV)
      DO I=1,V2                              ! lower left corner
         DO J=NYV1,NY
            H0(I,J)=A
            HG(I,J)=B
            HL(I,J)=C
         ENDDO
      ENDDO

      A=H0(NXV,NYV)
      B=HG(NXV,NYV)
      C=HL(NXV,NYV)
      DO I=NXV1,NX                           ! lower right corner
         DO J=NYV1,NY
            H0(I,J)=A
            HG(I,J)=B
            HL(I,J)=C
         ENDDO
      ENDDO

      RETURN
      END
