% @(#)start_pipe.hlq	19.1 (ESO-IPG) 02/25/03 13:18:54 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1992 European Southern Observatory
%.IDENT      start_pipe.hlq
%.AUTHOR     KB, IPG/ESO
%.KEYWORDS   MIDAS, help files, START/PIPELINE
%.PURPOSE    On-line help file for the command: START/PIPELINE
%.VERSION    1.0  000525 : Creation, KB
%----------------------------------------------------------------
\se
SECTION./PIPE
\es\co
START/PIPELINE					25-MAY-2000 KB
\oc\su
START/PIPELINE  display_flag
	start the VLT instrument pipeline 
\us\pu
Purpose:    
          Start the VLT instrument pipeline with or without display (graphics)
          capabilities, currently such a Midas based pipeline exists for the 
          following VLT instruments:
          FORS1, FOPRS2, UVES
\up\sy
Syntax:   
          START/PIPELINE  display_flag
\ys\pa
          display_flag = Display/NoDisplay to start the pipeline with/without
                 display (graphics) capabilities.
\ap\no
Note:   
          OJO: since the pipelines create a lot of new commands, the number 
          of commands and qualifiers is increased significantly when starting
          the pipeline.
          As a consequence, all installed contexts (except RBS) and
          also all user commands created so far will be removed!
          Thus, it would be a good idea to set the context RBS and execute
          START/PIPELINE in the very beginning of your Midas session before
          creating any commands (e.g. put the RBS context stuff into the 
          beginning of your login.prg).
\on\exs
Examples:
\ex
          START/PIPELINE Display
            Start the pipeline (increase keywords, create all relevant 
            keywords) and enable image display and graphics.
\xe \sxe

