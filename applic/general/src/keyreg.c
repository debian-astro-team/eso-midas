/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

.IDENT       keyreg.c                                       
.AUTHOR      Pascal Ballester,  ESO - Garching             
.KEYWORDS    Session Manager                              
.PURPOSE     Parse initialisation file and initialize    
             session table                              

.VERSION     1.0    Creation    18-FEB-1994  PB        

 090710		last modif
------------------------------------------------------------- */

#include <tbldef.h>
#include <midas_def.h>
#include <filedef.h>
#include <str.h>

#include <stdio.h>


#define  MAXCHAR 200

int main()

{
char direct[80], file[80], table[80], phfile[300],
           line[MAXCHAR], segment[MAXCHAR];
char keyname[13], type[13], defval[51], descr[101];
char indspace, indslash, indmark, nbcar;

int  actvals, fid, tid;
int  colk, colt, cold[4], colf;
int  bounds[2], cnt=0, row=0, discount;
int  dbg = 0, lgth, status;


bounds[0] = bounds[1] = discount = 0;


   (void) SCSPRO("keyreg");

   (void) SCKGETC("IN_A", 1, 80, &actvals, direct);
   (void) SCKGETC("IN_B", 1, 80, &actvals, file);
   (void) SCKGETC("OUT_A",1, 80, &actvals, table);

  (void) TCTINI(table, F_TRANS, F_O_MODE, 100, 10, &tid);

  TCCINI(tid, D_C_FORMAT, 32, "A10",  "Keyword",     "KEY",          &colk);
  TCCINI(tid, D_C_FORMAT, 12, "A8",   "Type",        "TYPE",         &colt);
  TCCINI(tid, D_C_FORMAT, 50, "A10",  "Default",     "DEFAULT",      &colf);
  TCCINI(tid, D_C_FORMAT, 70, "A15",  "Description", "D1",  &cold[0]);
  TCCINI(tid, D_C_FORMAT, 70, "A5",   "Description", "D2",  &cold[1]);
  TCCINI(tid, D_C_FORMAT, 70, "A5",   "Description", "D3",  &cold[2]);
  TCCINI(tid, D_C_FORMAT, 70, "A5",   "Description", "D4",  &cold[3]);

  status = osfphname(direct,phfile);
  if (status == -1) (void) strcpy(phfile,direct);

#ifndef vms
  if (phfile[(int)strlen(phfile)-1] != '/') (void) strcat(phfile,"/");
#endif

  (void) strcat(phfile,file);

  if ((fid = osaopen(phfile,READ)) == -1) {
       (void) sprintf(line,"Could not open file %s",phfile);
       SCETER(11,line);
     }

  line[0] = '\0';
  while (osaread(fid,  line, MAXCHAR) >= 0) 
     {
        cnt++;

        strred(line);
        strlower(line);

        if (line[0] == '!') {
        if (strindex(line,"begin session list") < (int)strlen(line)) 
                   bounds[0] = cnt + 1;
        if (strindex(line,"end session list") < (int)strlen(line))   
                   bounds[1] = cnt - 1;
        }
     }

  if (bounds[0] == 0) bounds[0] = 1;
  if (bounds[1] == 0) bounds[1] = cnt - 1;

  if (dbg == 1) printf("Bounds from %d to %d\n",bounds[0],bounds[1]);

  osaseek(fid, 0L, FILE_START); /* Returns to beggining of file */

  if (bounds[0] > 1) {
     for(cnt=1; cnt<bounds[0]; cnt++)  
           osaread(fid,  line, MAXCHAR);
     }

  for(cnt=bounds[0]; cnt<=bounds[1]; cnt++) {

     if ((lgth = osaread(fid,  line, MAXCHAR)) > 0) {

     (void) strcpy (segment,line);
     (void) strred (segment);
     (void) strupper(segment);

     if (dbg == 1) 
        {
        printf("       :          1         2         3         4         5\n");
        printf("       :012345678901234567890123456789012345678901234567890\n");
        printf("Line   :%s\n",line);
        printf("Segment:%s\n",segment);
        printf("Position: %d\n",strindex(segment,"WRITE/KEY"));
        }

     if (strindex(segment,"WRITE/KEY") == 0) {

        discount = 0;

        indspace = strindex(segment," ");
        indslash = strindex(segment+8,"/")+8;
        nbcar = indslash - indspace;

        keyname[0] = '\0';

        (void) strncopy(keyname,nbcar,segment+indspace+1);

        /* if keyword already exists, it should not be deleted ... (KB) 
      
        if (MID_FNDKEY(keyname,type,&actvals,&actvals,&actvals) != -1)
           {
           (void) sprintf(table,"keyword: %s already exists -> no table entry",
                          keyname);
           SCTPUT(table);
           continue;		
           }			*/


        if ((keyname[0] != '\0') && (keyname[0] != ' ')) 
           {
           TCEWRC(tid, ++row, colk, keyname);

           indspace = strindex(segment+indslash," ");
           nbcar = indspace;

           strncopy(type,nbcar,segment+indslash+1);

           TCEWRC(tid, row, colt, type);

           indslash = strindex(line,"/");
           indslash += strindex(line+indslash+1,"/"); /* Second Slash */
           indspace = strindex(line+indslash," ")+indslash;
           indmark  = strindex(line,"!");

           if (indmark < (int)strlen(line))  {

              nbcar = indmark - indspace;

              strncopy (defval,nbcar,line+indspace+1);
              strred   (defval);
              TCEWRC   (tid, row, colf, defval);

              strcpy (descr,line+indmark+1);
              TCEWRC (tid, row, cold[discount], descr);
              }   
	   else {
              (void)strcpy (defval,line+indspace+1);
              strred (defval);
              TCEWRC   (tid, row, colf, defval);
	      }

           while(discount < 3)  (void) TCEWRC (tid, row, cold[++discount], "~");
           discount = 0;
           }
      } /* Closes if (segment... */

     if (strindex(segment,"!$") == 0) {
           discount++;
           strcpy (descr,line+2);
           (void) TCEWRC (tid, row, cold[discount], descr);
     }     

  
  }} /* Closes if ((lgth=... and for (cnt=bounds[0] ... */


  (void) TCTCLO(tid);
  (void) osaclose(fid);
  (void) SCSEPI();

return 0;
}


