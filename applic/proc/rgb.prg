! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  rgb.prg  
! to create a jpeg file from 3 Midas images which serve as
!                                    red, green, and blue components
! H.J. Roeser      950616
!
!  use as: @a rgb out flag red green blue  red_cuts  green_cuts  blue_cuts
!  with out = name of resulting .jpg file (without extension!)
!       flag = RAMP (default) or LOG for using linear or log10 scaling
!       red = image serving as red colour component
!       green = image serving as green colour component
!       blue = image serving as blue colour component
!       red_cuts = low,high cuts for red colour
!                  if set to 0,0 the descr. LHCUTS of red image will be used
!       green_cuts = low,high cuts for green colour
!                  if set to 0,0 the descr. LHCUTS of green image will be used
!       blue_cuts = low,high cuts for blue colour
!                  if set to 0,0 the descr. LHCUTS of blue image will be used
! 
! to obtain a hardcopy including e.g. drawings in the overlay channel:
! $xwd -out test.shot -name "MIDAS_03 display_0"
! $/usr/bin/convert test.shot test.jpg
! 
! $xprop returns the name (WM_NAME) of the Midas display window.
! 
! 
! 090728	last modif, KB
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ? ? "Output frame : "
define/par p2 ramp ? "scaling (ramp or log) :"
define/par p3 NULL ? "Red frame : "
define/par p4 NULL ? "Green frame : "
define/par p5 NULL ? "Blue frame : "
define/par p6 0,0 ? "Red cuts : "
define/par p7 0,0 ? "Green cuts : "
define/par p8 0,0 ? "Blue cuts : "

if m$exist("{P1}.ppm"} .eq. 1  $rm {P1}.ppm

define/local cutr/r/1/2 {p6}
define/local cutg/r/1/2 {p7}
define/local cutb/r/1/2 {p8}
define/local ctrl_key/c/1/4 {p2}

if p3(1:4) .ne. "NULL"  then
   if p6(1:3) .eq. "0,0" then
      copy/dk {p3} lhcuts/r/1/2 cutr/r/1/2
   endif
endif
if p4(1:4) .ne. "NULL"  then
   if p7(1:3) .eq. "0,0" then
      copy/dk {p4} lhcuts/r/1/2 cutg/r/1/2
   endif
endif
if p5(1:4) .ne. "NULL"  then
   if p8(1:3) .eq. "0,0" then
      copy/dk {p5} lhcuts/r/1/2 cutb/r/1/2
   endif
endif


run CON_EXE:rgbhjr

! if successful execution convert to JPG via Unix command
if outputi(1) .eq. 0  then
   $cjpeg {P1}.ppm > {P1}.jpg
   $rm {P1}.ppm
endif


