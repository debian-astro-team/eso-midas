! @(#)edges.prg	19.1 (ES0-DMD) 02/25/03 13:20:09
! ++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  MIDAS procedure edges.prg
!        to find edges using Sobel or Prewitt filter
!  K. Banse		ESO - IPG
!  920327
! 
!  use via @a edges in_ima out_ima [filter_name]
!  with    filter_name = Sobel or Prewitt, defaulted to Sobel
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ima "Enter input image: "
define/param p2 ? ima "Enter output image: "
define/param p3 sobel ? "Enter filter, Sobel or Prewitt: "
! 
if aux_mode(1) .eq. 1 then			!only needed for VMS
   inputi = m$existd("midtempx","naxis")
   if inputi .eq. 1 delete/image midtempx no
   inputi = m$existd("midtempy","naxis")
   if inputi .eq. 1 delete/image midtempy no
endif
! 
if p3(1:1) .eq. "S" then
   filter/digital {p1} midtempx 1,0,-1,2,0,-2,1,0,-1
   filter/digital {p1} midtempy -1,-2,-1,0,0,0,1,2,1
else
   filter/digital {p1} midtempx 1,0,-1,1,0,-1,1,0,-1
   filter/digital {p1} midtempy -1,-1,-1,0,0,0,1,1,1
endif
! 
compute/image {p2} = sqrt(midtempx*midtempx+midtempy*midtempy)
