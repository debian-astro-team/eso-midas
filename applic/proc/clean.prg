! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure clean.prg  
! K. Banse	ESO - Garching
! 080124
! 
! clean bad pixels of an image by replacing them with the min, max, mean 
! or median value of the good pixels
! 
! execute via @a clean in out bad_intval [statist_value]
! where
!	in, out = input, output image
!	bad_intval = lo,hi - the borders of a closed interval [lo,hi]
!		     <, > can be used for lowest, highest pixel value 
!		     of input image
!	statist_value = min, max, mean or median (the default)	
!
! Examples:  @a clean in out 22.2345,> mean
!		replace all pixels of in.bdf with a value >= 22.345
!		with the mean value of all "good" pixels
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ima "Enter input image: "
define/param p2 ? c "Enter result image: "
define/param p3 ? c "Enter (closed) interval of bad pixel values: "
define/param p4 median c "Enter statist. quantity for replacement: "
! 
define/local in_frame/c/1/80 {p1}
define/local out_frame/c/1/80 {p2}
define/local bad_intval/c/1/80 {p3}
! default is median
define/local jdx/i/1/1 8		!median is in outputr(8)
define/local flags/c/1/4 XFYN		!use exact median
! 
! find/pixel {in_frame} {bad_intval} in all
extract/ref {in_frame} ? &g {bad_intval} out
! define/local no/i/1/1 {outputi(15)}
if p4(1:3) .eq. "MIN" then
   Flags = "RFYN"
   jdx = 1				!min is in outputr(1)
else if p4(1:3) .eq. "MAX" then
   Flags = "RFYN"
   jdx = 2				!max is in outputr(2)
else if p4(1:3) .eq. "MEA" then
   Flags = "RFYN"
   jdx = 3				!mean is in outputr(3)
endif
statistics/image &g p5={flags}
! 
set/format f14.6
replace/image {in_frame} {out_frame} {bad_intval}={outputr({jdx})}
