! @(#)histogram.prg	19.1 (ESO-DMD) 02/25/03 13:20:12
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS Procedure: histogram.prg   P.Ballester   ESO-DMD  940315
!  KB 980708
!
! Purpose: Image-to-image or image-to-table histogram transformation
!
! Syntax:  @a histogram  input  output  [bin]  [low,high]
!
! Parameters:
!     input    = Name of the input image
!     output   = Name of the output histogram image. A table will
!                be generated if the output name is followed by the
!                string ",TABLE"
!     bin      = Bin size
!     low,high = Lower,Upper limit of the histogram
!
! Note:   By default the lower an upper limits correspond to the 
!         min and max of the image and the bin size is chosen so
!         that the histogram is formed of 256 bins.
!
! See Also: COMPUTE/HISTOGRAM, PLOT/HISTOGRAM, EQUALIZE/HISTOGRAM
!
! Examples: @a histogram  ccd0023  hist23  10.  0.,2000.
!           @a histogram  ccd0023  hist23,TABLE 
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Input Image:"
define/param p2 ? ima "Output Image:"
! 
define/param p3 + number "Bin Size:"
define/param p4 + number "low,high val:"
define/maxpar 4
! 
define/local outname/c/1/60  {p2}
define/local index/i/1/1     0
define/local tabflag/i/1/1   0
! 
index = m$index(outname,",T")
! 
if index .ne. 0 then
   tabflag = 1
   index = index - 1
   write/keyword outname {p2(1:{index})}
endif
! 
if p3(1:1) .eq. "+" p3(1:1) = "?"
if p4(1:1) .eq. "+" p4(1:1) = "?"
! 
statist/image {p1} ? {p3} {p4}  >Null
! 
define/local nn/i/1/3 {{p1},hist_bins(1)},{{p1},hist_bins(5)},1
if nn(2) .ne. 0 then            !Yes, we have excess bins
   nn(1) = nn(1)-nn(2)          !so subtract ecess bins
   nn(3) = 2                    !skip low excess bin
endif
copy/dima  {p1} histogram/i/{nn(3)}/{nn(1)} {outname}
!     next change thanks to Jean-Pierre De Cuyper
! write/descr {outname}  start {{p1},hist_bins(3)}
define/local dd/d/1/1 0.
dd = m$value({p1},hist_bins(3))+(m$value({p1},hist_bins(2))*.5)
write/descr {outname}  start {dd}
write/descr {outname}  step {{p1},hist_bins(2)}
write/descr {outname}  ident/c/1/72  "Histogram of the image: {p1} " 
! 
if tabflag .eq. 1 then
   copy/it {outname} &t :PIXEL  
   name/column &t #2 :FREQUENCY
   copy/table &t {outname}  >Null
endif
