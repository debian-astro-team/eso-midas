! @(#)copyfile.prg	19.1 (ESO-DMD) 02/25/03 13:20:07
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure copyfile.prg to copy an ASCII file omitting certain records
!                             of input ASCII file
! K. Banse      980710
!
! execute as 
! @a copyfile infile outfile mins,maxs comment_str pat1 pat2 pat3 pat4
!
! with infile = name of input ASCII file
!      outfile = name of output ASCII file
!      mins,maxs = minimum, maximum size of record to be copied
!                  defaulted to 1,80
!      comment_str = char.string at beginning of records indicating
!                    that it is a comment line and thus ignored 
!      pat1 = char.string to look for in records which should NOT be copied
!      pat2 = char.string to look for in records which should NOT be copied
!      pat3 = char.string to look for in records which should NOT be copied
!      pat4 = char.string to look for in records which should NOT be copied
!
! so up to 4 different patterns can be given to specify the records which 
! should not be copied
! 
! e.g. @a copyfile kukiwu.ascii lola.dat ? "!" ? alpha
! copies all lines except the ones containing the char. '?' or 
! the string 'alpha' 
! and also all comment lines, i.e. the ones beginning with `!' are not copied
! 
! keyword OUTPUTI(1) holds no. of records copied
! keyword OUTPUTI(2) holds no. of records not copied
! so total no. of records (lines) of infile = OUTPUTI(1) + OUTPUTI(2)
! 
! if no output file name is given, the procedure just reports how many times
! any of the given pattern is found in the input file (ignoring comment lines)
! e.g. @a copyfile kukiwu.ascii ? ? "!" alpha beta delta gamma
! 
! keyword OUTPUTI(1) holds no. of records without
! keyword OUTPUTI(2) holds no. of records with any of the given patterns
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? c "Enter infile name:"
define/param p2 + c "Enter outfile name:"
! 
define/param p3 1,80 no "Enter minsize,maxsize of record to copy:"
define/param p4 + c "Enter comment_string at start of record:"
define/param p5 + c "Enter pattern1 for exclusion of record:"
define/param p6 + c "Enter pattern2 for exclusion of record:"
define/param p7 + c "Enter pattern3 for exclusion of record:"
define/param p8 + c "Enter pattern4 for exclusion of record:"
! 
define/local fc/i/1/2 0,0
define/local fd/i/1/2 -1,-1
define/local ibad/i/1/2 0,0
define/local mima/i/1/2 0,0
write/keyw mima {p3}
define/local record/c/1/200 " " all
define/local comstr/c/1/20 {p4}
define/local comlen/i/1/1 0
set/format i1
! 
if comstr(1:1) .ne. "+" then
   comlen = m$len(p4)
   if p4(1:1) .eq. """ then
      if p4({comlen}:{comlen}) .eq. """ comlen = comlen-2
   endif
endif
! 
open/file {p1} read fc
if fc .lt. 1 then
   write/out Could not open file: {p1} ...
   return/exit
endif
! 
if p2(1:1) .ne. "+" then
   open/file {p2} write fd
   if fd .lt. 1 then
      write/out Could not open file: {p2} ...
      return/exit
   endif
endif
! 
ibad(1) = 0
ibad(2) = 0
! 
read_loop:
write/keyw record " " all
read/file {fc} record
if fc(2) .lt. 0 goto end_of_loop
if fc(2) .eq. 0 goto read_loop
! 
if log(12) .eq. 123 then
   write/out {fc(2)} chars read
   write/out {record(1:{fc(2)})}
endif
! 
if comstr(1:1) .ne. "+" then
   if record(1:{comlen}) .eq. comstr(1:{comlen}) then
      if fd .ge. 0 ibad(2) = ibad(2)+1
      goto read_loop
   endif
endif
! 
if p5(1:1) .ne. "+" then
   if p5(1:1) .ne. """ then
      inputi = m$index(record,"{p5}")
   else
      inputi = m$index(record,{p5})
   endif
   if inputi .gt. 0 then
      ibad(2) = ibad(2)+1
      goto read_loop
   endif
else
   goto ok_rec
endif
! 
if p6(1:1) .ne. "+" then
   if p6(1:1) .ne. """ then
      inputi = m$index(record,"{p6}")
   else
      inputi = m$index(record,{p6})
   endif
   if inputi .gt. 0 then
      ibad(2) = ibad(2)+1
      goto read_loop
   endif
else
   goto ok_rec
endif
! 
if p7(1:1) .ne. "+" then
   if p7(1:1) .ne. """ then
      inputi = m$index(record,"{p7}")
   else
      inputi = m$index(record,{p7})
   endif
   if inputi .gt. 0 then
      ibad(2) = ibad(2)+1
      goto read_loop
   endif
else
   goto ok_rec
endif
! 
if p8(1:1) .ne. "+" then
   if p8(1:1) .ne. """ then
      inputi = m$index(record,"{p8}")
   else
      inputi = m$index(record,{p8})
   endif
   if inputi .gt. 0 then
      ibad(2) = ibad(2)+1
      goto read_loop
   endif
else
   goto ok_rec
endif
! 
! we found a `good' record ...
ok_rec:
if fc(2) .lt. mima(1) then
   goto read_loop
else if fc(2) .gt. mima(2) then
   fc(2) = mima(2) + 1
   inputi = 200-fc(2)+1
   write/keyw record/c/{fc(2)}/{inputi} " " all
endif
ibad(1) = ibad(1)+1
if fd .gt. -1 write/file {fd},key record
goto read_loop
! 
end_of_loop:
close/file {fc}
if fd .gt. -1  then
   write/out {ibad(1)} records copied, {ibad(2)} records omitted
   close/file {fd}
else
   write/out {ibad(1)} records without, "{ibad(2)} records with given pattern(s)"
endif
! 
outputi(1) = ibad(1)
outputi(2) = ibad(2)
