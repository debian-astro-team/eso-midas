! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure outima.prg  to convert Midas images to other image formats
! K. Banse      941020, 941107, 040929
! 
! execute via   @a outima frame format [LUTname] [ITTname] 
!   with frame = name of input Midas image
!        format = AVS   AVS X image file.
!                 BMP   Microsoft Windows bitmap image file.
!                 EPS   Adobe Encapsulated PostScript file.
!                 GIF   Compuserve Graphics image file.
!                 JPEG
!                 MIFF  Magick image file format.
!                 PCX   ZSoft IBM PC Paintbrush file.
!                 PICT  Apple Macintosh QuickDraw/PICT file.
!                 PNM   Portable bitmap.
!                 PS    Adobe PostScript
!                 TGA   Truevision Targa image file.
!                 TIFF  Tagged Image File Format.
!                 VIFF  Khoros Visualization image file.
!                 XBM   X11 bitmap file.
!                 XPM   X11 pixmap file.
!                 XWD   X Window System window dump image file.
! 
!        LUT name = name of MIDAS LUT, e.g. heat; defaulted to ramp
!        ITT name = name of MIDAS ITT, e.g. log; defaulted to ramp
! 
!  The conversion is done via transforming the MIDAS image to PostScript 
!  format and then applying the public domain package `ImageMagick'.
!  I.e. this software must be installed at your site - otherwise this
!  procedure will not work...	
!  (the PostScript conversion is always possible)
! 
!  The output name will be:
!      frame name + format (in lowercase) as file type.
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/para p1 ? ima "Enter image name: "
define/para p2 GIF c "Enter output format: "
define/para p3 ramp.lut c "Enter LUT: "
define/para p4 + c "Enter ITT: "
define/maxpar 4
define/local lutname/c/1/60 {p3}
! 
define/local nn/i/1/2 0,0
nn(1) = m$indexb(p1,".")
if aux_mode(1) .eq. 1 then
   nn(2) = m$indexb(p1,"]")
else
   nn(2) = m$indexb(p1,"/")
endif
if nn(1) .gt. nn(2) then		!define root of output frame
   nn = nn-1
   define/local outa/c/1/60 {p1(1:{nn})}
else
   define/local outa/c/1/60 {p1}
endif
! 
! if ITT required, we have to map LUT through it
if p4(1:1) .ne. "+" then
   @a mapitt {p4} {p3} x_y_outima
   if inputi .ne. -4321 then
      delete/display {inputi(17)}
   else
      lutname = "x_y_outima.lut"
   endif
endif
! 
! build up the Postscript file
! 
if lutname(1:8) .eq. "ramp.lut" then
   copy/pscr {p1} {outa}.ps 
else
   copy/pscr {p1} {outa}.ps p6={lutname}
endif
! 
! now convert if necessary
! 
if p2(1:3) .eq. "AVS" then
   define/local outfile/c/1/60 {outa}.avs
else if p2(1:3) .eq. "BMP" then
   define/local outfile/c/1/60 {outa}.bmp
else if p2(1:1) .eq. "E" then
   define/local outfile/c/1/60 {outa}.eps
else if p2(1:1) .eq. "G" then
   define/local outfile/c/1/60 {outa}.gif
else if p2(1:1) .eq. "J" then
   define/local outfile/c/1/60 {outa}.jpeg
else if p2(1:1) .eq. "M" then
   define/local outfile/c/1/60 {outa}.miff
else if p2(1:3) .eq. "PCX" then
   define/local outfile/c/1/60 {outa}.pcx
else if p2(1:3) .eq. "PIC" then
   define/local outfile/c/1/60 {outa}.pict
else if p2(1:3) .eq. "PNM" then
   define/local outfile/c/1/60 {outa}.pnm
else if p2(1:2) .eq. "PS" then
   define/local outfile/c/1/60 {outa}.ps
   goto end
else if p2(1:2) .eq. "TG" then
   define/local outfile/c/1/60 {outa}.tga
else if p2(1:2) .eq. "TI" then
   define/local outfile/c/1/60 {outa}.tiff
else if p2(1:1) .eq. "V" then
   define/local outfile/c/1/60 {outa}.viff
else if p2(1:3) .eq. "XBM" then
   define/local outfile/c/1/60 {outa}.xbm
else if p2(1:3) .eq. "XPM" then
   define/local outfile/c/1/60 {outa}.xpm
else if p2(1:3) .eq. "XWD" then
   define/local outfile/c/1/60 {outa}.xwd
else
   write/out {p2} is an unknown output format
   return
endif
! 
! use ImageMagick for conversion Postscript -> {p2} format
! 
$convert {outa}.ps {outfile}
! 
end:
define/local format/c/1/8 " " all
format = m$upper(p2)
write/out {format} format file `{outfile}' created...
