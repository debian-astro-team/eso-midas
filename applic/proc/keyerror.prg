! @(#)keyerror.prg	19.1 (ES0-DMD) 02/25/03 13:20:13
! @(#)keyerror.prg	19.1  (ESO)  02/25/03  13:20:13
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       keyerror.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Session, Manager
!.PURPOSE     Command ERROR/...
!.VERSION     1.0    Creation    4-SEP-1991  PB
!
!-------------------------------------------------------
!
DEFINE/PARAM P1  ?  C   "Command name:"
DEFINE/PARAM P2  ?  C   "Parameter name:"

DEFINE/LOCAL KEYNAM/C/1/8  {P2}
DEFINE/LOCAL PARENT/I/1/1  0

PARENT = M$INDEX(P2,"(") - 1
IF PARENT .GT. 0   WRITE/KEYW KEYNAM/C/1/8  {P2(1:{PARENT})}

WRITE/OUT "{P1}: wrong parameter {P2} = {{P2}}"
HELP/{MID$CMND(11:14)} {KEYNAM}
RETURN/EXIT




