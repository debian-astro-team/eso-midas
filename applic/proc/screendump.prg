! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! Midas procedure screendump.prg
! U. Kaeufl	ESO - Garching
! 050527	creation
! 
! make a screen dump of displayed image and save in inmage-file
! using xwininfo, xwd (from X11) and convert (from ImageMagick)
! 
! use as @a screendump image-file
! where the type of image-file determines the format chosen,
! e.g. abc.gif, def.tiff or xyz.eps
! default of image-file is: midas_graph.jpg
!
! 050530	last modif
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

defi/param   P1 midas_graph.jpg ? "Name of output file"

defi/local   sessnr/i/1/1      00      ! obvious
defi/local   dispunit/c/1/100   "MIDAS_xx display_y"     
defi/local   disp_id/c/1/20     "xx"  			!to hold display id

set/format  i2
write/keyw   dispunit/c/7/2 {mid$sess(9:14)}
set/format  i1
write/keyw   dispunit/c/18/1 {dazdevr(10)}
set/format  i4

$ xwininfo -root -tree   |  grep "{dispunit}" >  atmund

create/image    am_null 1,1
copy/ad     atmund  am_null dummy

! note, write/key disp_id <atmund  does not work due to
! "special" character ` " '

copy/dkey   am_null dummy disp_id

$ rm am_null.bdf atmund 

if mid$sys(1:3) .eq. "Lin" then
   $ xwd  -id {disp_id} -silent > gra2jpg_dummy
else
   $ xwd  -id {disp_id}  > gra2jpg_dummy
endif

$ convert gra2jpg_dummy {P1}

$ rm gra2jpg_dummy
