! @(#)fastcomp.prg	19.1 (ESO-DMD) 02/25/03 13:20:09
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! MIDAS procedure fastcomp.prg
! K. Banse	970908
! 
! interpret real numbers as integer (4 bytes) and vice versa
! this is no conversion from real <-> integer but a different interpretation
! of the data type by the compiler (we assume IEEE floating point format)
! in order to calculate fast approximations of LOG10, LN, SQRT
! 
! Note, that these are NOT exact functions but approximations...
!
! use via @a fastcomp in_ima out_ima function
! 
!  with function = LOG10 or LN or SQRT or INVERSE 
!                  out_ima = function(in_ima)
!                  for INVERSE, out_ima = 1/in_ima
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter source image:"
define/param p2 ? IMA "Enter destination image:"
define/param p3 I c "Enter function:"
! 
write/keyw in_a {p1}
write/keyw out_a {p2}
action(1:3) = "FAS"
run MID_EXE:GENXY1
