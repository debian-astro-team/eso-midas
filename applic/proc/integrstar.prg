! @(#)integrstar.prg	19.1 (ES0-DMD) 02/25/03 13:20:13
!.IDENTIFICATION: INTESTAR.PRG
!.PURPOSE:        MIDAS command procedure FLUXST for INTEGRATE/STAR
!.NOTES:          Integrate area enclosed by a line (the input image) 
!                 interactively via cursor input, by interpolating 
!                 polynomially between cursor positions (currently up to 
!                 100 cursor-positions and 9th degree); or alternatively 
!                 by giving independent variable values as parameters, for 
!                 batch execution.
!.KEYWORDS:       Integration, flux
!.LANGUAGE:       MIDAS command language.
!.SYNTAX:         INTEGRATE/STAR frame intable outtable parameters mode
!                 curs-posns = cursor positions, followed by range (for 
!                 batch usage)
!.AUTHOR:         A. Llebariat, Ch. Ounnas	?
!.VERSION:        860917 KB  ???
!.VERSION:        8606?? RHW ???
!.VERSION:        900731 RHW combining the interactive and manual code
!.VERSION:        901127 RHW changes to obtain same command sequence as in
!                            INTEGRATE/APERTURE
!.VERSION:        931108 RHW {} stuff
!-------------------------------------------------------------------------
DEFINE/PARAM P1 CURSOR
DEFINE/PARAM P3 20,1,0 NUM 
DEFINE/PARAM P4 AUTO CHA
!
DEFINE/LOCAL L/I/1/1 0
L = M$INDEX(P1,",")
IF L .GT. 0 THEN                                           !inframe,intable
   L = L - 1
   WRITE/KEYW IN_A {P1(1:{L})}
   L = L + 2
   WRITE/KEYW IN_B {P1({L}:>)}
!
ELSE
   SET/CURSOR 0
   WRITE/KEYW IN_A *
   WRITE/KEYW IN_B *
ENDIF
!
WRITE/KEYW OUT_A        {P2}                              !fill output_specs
WRITE/KEYW INPUTR/R/1/3 {P3}
WRITE/KEYW INPUTC/C/1/4 {P4}
!
RUN APP_EXE:INTESTAR
