!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1992-2005 European Southern Observatory
!.IDENT       creategui.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Graphical, User, Interfaces
!.PURPOSE     Generates Midas Graphical User Interfaces
!.VERSION     1.0    Creation    31.03.1992
! 051116	last modif
!-------------------------------------------------------
!
!
DEFINE/PARAM  P1   ?    CHAR    !  Interface Name 
!
WRITE/KEYW   LASTIME/I/1/1  0
define/local found/I/1/1   0
! 
IF AUX_MODE(1) .LE. 1 THEN
  WRITE/OUT "Sorry. Graphical Interfaces are currently unavailable under VMS"
  RETURN
ENDIF
! 
IF P1(1:1) .EQ. "A" THEN        !  Alice
   found = 1
   WRITE/OUT "Preparing the XAlice user interface..."
   set/context alice
   $ $GUI_EXE/alice.exe {AUX_MODE(3)} {MID$SESS(11:12)}  &
! 
ELSEIF P1(1:2) .EQ. "EC" THEN        !  Context echelle
   found = 1
   set/context echelle
   synchron/echelle
   WRITE/OUT "Preparing the XEchelle user interface..."
   $ $GUI_EXE/echelle.exe {AUX_MODE(3)} {MID$SESS(11:12)}  &
! 
ELSEIF P1(1:2) .eq. "EM" then        ! context emos
   found = 1
   WRITE/OUT "Preparing the EMOS user interface..."
   set/context emos
   $ $CON_EXE/emos.exe  {AUX_MODE(3)} &
!
ELSEIF P1(1:1) .EQ. "I" THEN        !  Context irspec
   found = 1
   set/context irspec
   WRITE/OUT "Preparing the XIrspec user interface..."
   $ $GUI_EXE/irspec.exe {AUX_MODE(3)} {MID$SESS(11:12)}  &
!
ELSEIF P1(1:1) .EQ. "L" THEN        !  Context XLong
   found = 1
   set/context long
   WRITE/OUT "Preparing the XLong user interface..."
   $ $GUI_EXE/long.exe {AUX_MODE(3)} {MID$SESS(11:12)}  &
! 
ELSEIF P1(1:2) .EQ. "ST" THEN        ! Context Xstella
   found = 1
   set/context stella
   write/out "preparing the Xstella Graphical interface..."
   $ $CON_EXE/stella.exe  {AUX_MODE(3)} &
! 
ELSEIF P1(1:1) .eq. "F" then        ! context filters
   found = 1
   WRITE/OUT "Preparing the XFilter user interface..."
   $ $GUI_EXE/filter.exe  {AUX_MODE(3)} &
! 
ELSEIF P1(1:2) .eq. "DO" then        ! DO 
   found = 1
   set/context do
   WRITE/OUT "Preparing the XDO user interface..."
   $ $GUI_EXE/do.exe {AUX_MODE(3)} {MID$SESS(11:12)} &
ELSEIF P1(1:1) .eq. "D" then        ! Display
   found = 1
   WRITE/OUT "Preparing the Graphical User Interface for Display stuff..."
   $ $GUI_EXE/display.exe {AUX_MODE(3)} {MID$SESS(11:12)}  &
! 
ELSEIF P1(1:1) .eq. "H" then        ! Helper 
   found = 1
   WRITE/OUT "Preparing the XHelp user interface..."
   define/local context/c/1/160 " " all
   help -1			!get contexts stored into INPUTC, OUTPUTC
   if inputc(1:1) .ne. " " then
      write/out "Loading contexts : {inputc(1:80)}"
      if outputc(1:1) .ne. " " then
         write/out "and contexts : {outputc(1:80)}"
      endif
   endif
   copy/keyw inputc/c/1/80 context/c/1/80
   copy/keyw outputc/c/1/80 context/c/81/80

   define/local mimi/c/1/12 " " all
   mimi = M$SYMBOL("MIDVERS")			!get current Midas version
   if mimi(1:3) .eq. "MID" then
      write/out "env. var. MIDVERS must be set - we abort."
      return/exit
   endif

   $ $GUI_EXE/help.exe  {MID$SESS(11:12)}{mimi} {MID$MAIL} -
           "{SYSCOMS(21:40)}" $MID_CONTEXT $MID_WORK "{CONTEXT(1:160)}" &
ENDIF

IF FOUND .EQ. 0 THEN
   WRITE/OUT "Could not understand interface name: {P1}. 
   WRITE/OUT "    Read the help for the list of names"
ENDIF


ENTRY init
DEFINE/PARAM P1  ?   CHAR       ! Output file name
DEFINE/PARAM P2  *   CHAR       ! Keyword list

DEFINE/LOCAL SAVPR/C/1/102 {MID$PRNT(1:102)}

ASSIGN/PRINT file  {P1}
PRINT/KEYW    {P2} {LASTIME}
MID$PRNT(1:102) = SAVPR(1:102)
LASTIME = M$SECS()

