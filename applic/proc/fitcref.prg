! @(#)fitcref.prg	19.1 (ES0-DMD) 02/25/03 13:20:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : FITCREF.PRG
!  FIT Subsystem
!
!  J.D.Ponz                ESO - Garching   1 APR 84
!                                          26 JUN 85
!  K. Banse				      871214
!  M Peron                 ESO                900215 portability to UNIX!
! .PURPOSE
!
!    implements
!
! CREATE/FUNCTION USER0,... par2
! to define the user functions in the fit package
!
!
!  par2 = additional library needed for linking    KB
!
! ----------------------------------------------------------------------
!
IF  AUX_MODE(1) .LE. 1 THEN
       $ @ APP_PROC:fitcref.com 'P1' 'P2'       !VMS
ELSE
       $ sh $APP_PROC/fitcref.sh 'P1'       !UNIX
ENDIF
!
SET/FIT FCTDEF=US
