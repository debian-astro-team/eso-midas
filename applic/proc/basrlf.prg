! @(#)basrlf.prg	19.1 (ES0-DMD) 02/25/03 13:20:05
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure BASRLF to implement command /BASRLF
! K. Banse	830114
! execute via /BASRLF threshold (threshold in [0,255])
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DEFINE/PARAM P1 127
WR/KEYW INPUTI 'P1'
DISP/CHAN 'DEANZA(4)'
RUN APP_EXE:BASRLF
SET/OVERLAY
