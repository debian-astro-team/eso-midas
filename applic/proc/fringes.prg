! @(#)fringes.prg	19.1 (ES0-DMD) 02/25/03 13:20:11
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! ECHELLE PROCEDURE : FRINGES.PRG
! J.D.Ponz			version 1.0 131083
!
! .PURPOSE
!
! execute the command :
!  SEARCH/FRINGE image window,threshold DIR table
!
!  POSITION DEFINED VIA CURSOR
!
DEFINE/PARAM P4 LINE TABLE
WRITE/KEYW IN_A 'P1'
WRITE/KEYW INPUTR/R/1/2 'P2'
WRITE/KEYW INPUTI/I/3/1 'P3'
WRITE/KEYW OUT_A  'P4'
WRITE/OUT  enter one cursor position
GET/CURSOR
INPUTI(1) = OUTPUTR(12)
INPUTI(2) = OUTPUTR(13)
RUN APP_EXE:FRINGES
LOAD/TABLE 'OUT_A' :X :Y :ORDER -1
