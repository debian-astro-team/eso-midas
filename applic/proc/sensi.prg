! @(#)sensi.prg	19.1 (ES0-DMD) 02/25/03 13:20:18
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure SENSI to implement COMP/SENSITIVITY
! K. Banse	860917
!
! use as COMP/SENS COEF = input_spec
!        input_spec : catal.CAT or frame1,frame2,...,framen
!
! still in testing phase...
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
WRITE/KEYW OUT_A 'P1'
RUN APP_EXE:SENSI
