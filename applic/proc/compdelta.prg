! @(#)compdelta.prg	19.1 (ESO-DMD) 02/25/03 13:20:07
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure   compdelta.prg	to compute "difference images"
!  K. Banse     010227
!
!  use as:  @a compdelta in out distpix operation
!  with:  in        - input image (1- or 2-dim)
!         out       - result image
!         distpix   - distance in pixels between operators
!         operation - "+", "-", "*", "/"	(has to be in double quotes...)
! 
!  to do arithmetic with "adjacent" pixels of an image
!  the resulting image will have `distpix' less pixel than the input image
!  but same start (and naturally same step)
! 
!  @a compdelta inni outi 2,0 "-"
!  results in:
!  outi(m,n) = inni(m+2,n)-inni(m,n) for (m,n) in [1,1:npix(1),npix(2)] 
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? i "Enter input image:"
define/param p2 delta i "Enter result image:"
define/param p3 1 n "Enter the x,y-pixel distance to operand:"
define/local shift/i/1/2 {p3},0
define/param p4 "-" c "Enter operator ("+","-","/","*"):"
define/local op/c/1/1 {p4(2:2)}
! 
! create a shifted copy of input frame
! 
shift/image {p1} &dum -{shift(1)},-{shift(2)}
!
! now do a compute combining input frame and shifted copy
! 
define/local koko/i/1/2 1 all
if {{p1},naxis} .gt. 1 then
   koko(1) = {{p1},npix(1)} - shift(1)		!new x-end
   koko(2) = {{p1},npix(2)} - shift(2)		!new y-end
   compute/image {p2} = -
    &dum[<,<:@{koko(1)},@{koko(2)}] {op} {p1}[<,<:@{koko(1)},@{koko(2)}] 
else
   koko(1) = {{p1},npix(1)} - shift(1)		!new x-end
   compute/image {p2} = &dum[<:@{koko(1)}] {op} {p1}[<:@{koko(1)}]
endif
! 

