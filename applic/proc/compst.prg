! @(#)compst.prg	19.1 (ESO-DMD) 02/25/03 13:20:07
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!  MIDAS procedure  compst.prg              version 1.0   860201
!  D. Baade                   ESO - Garching
!  KB		890214, 901108, 921209, 930323, 980713
!  PJG          920302
!
!.KEYWORDS
!  Julian date, conversion from universal time to local mean sidereal time
!
!.PURPOSE
!  convert from UT to local mean ST, calculate Julian date
!
!.COMMENT
!  Input has been changed so that eastern longitude is required
!  following IAU standards.          PJG 920902
!
!.ALGORITHM
!  all done in FORTRAN program COMPXY which is executed by this MIDAS procedure
!
!.INPUT/OUTPUT
!  the following keywords are used:
!
!  a) if input is only via keywords
!  P1                          date: year,month,day
!  P2                          universal time: hour,min,sec
!  P3                          eastern longitude of observatory: degree,min,sec
!  OUTPUTR/R/1/3               local mean sidereal time : hour,min,sec
!  OUTPUTD/D/1/1               Julian date
!
!  b) if dates and times are read from table
!  P1                          name of table
!  P2                          eastern longitude of observatory: degree,min,sec
!  table :YEAR :MONTH :DAY     date
!  table :UTHR :UTMIN :UTSEC   universal time (UT)
!  table :STHR :STMIN :STSEC   will receive local mean ST in real hours
!  table :JD                   will receive Julian date
!
!  c) if date and time are read from descriptors of image
!  P1                          name of image
!  P2                          eastern longitude of observatory: degree,min,sec
!  image,O_TIME(1)             year
!  image,O_TIME(2)             month
!  image,O_TIME(3)             day
!  image,O_TIME(5)             universal time in real hours
!  image,O_TIME(4)             will receive Julian date
!  image,O_TIME(6)             will receive local mean ST in real hours
!
!------------------------------------------------------------------------------
!
crossref DATE UT LONGITUDE
!
inputc(1:3) = "+++"			!for sign of longitude etc.
define/local rneg/r/1/1 1.0
set/format ,g24.12			!for double data
!
define/param p1 ? ? "Enter date (year,month,day) or name of image or table: "
!
if m$tstno(p1) .ne. 0 goto keys         !check, if frame or number(s)
! 
define/param p1 + ima
write/keyw in_a {p1}
if m$filtyp(in_a,".bdf") .eq. 2 goto table
!
!  input is read from descriptors of image {p1}
!
define/param p2 -70,43,55.35 N -
"Enter (eastern) longitude of observatory (degree,min,sec): "
!
write/keyw inputr/r/1/10 0,0,0,0,0,0,{p2},0,0  ! partly reset keyword
rneg = (inputr(7)*3600.)+(inputr(8)*60.)+inputr(9)
inputc(1:1) = p2(1:1)
if rneg .lt. 0.0 inputc(1:1) = "-"
!
inputr(1) = m$value({in_a},o_time(1))  			!year
if inputr(1) .le. 1.0 .or. inputr(1) .lt. 1901.0 then
   write/out " Warning: year of the observation: {inputr} really correct? ..."
endif
inputr(2) = m$value({in_a},o_time(2))  			!month
inputr(3) = m$value({in_a},o_time(3))	  		!day
inputr(4) = m$value({in_a},o_time(5))			!UT in real hours
!
action(1:2) = "US"			!indicate conversion UT -> ST
run MID_EXE:COMPXY
!
outputd(20) = outputd(1)-2400000.5000000		 !JD - 2,400,000.5
outputd(19) = outputr(1)+(outputr(2)+outputr(3)/60.)/60. !ST in real hour
!
{in_a},o_time(4) = outputd(20)		!JD - 2,400,000.5
{in_a},o_time(6) = outputd(19)		!local mean ST in real hours
!
return					!finished with image option
!
KEYS:
!
! ... input only via keywords
!
define/param p2 ? N "Enter UT of observation (hour,min,sec): "
define/param p3 -70,43,55.35 N -
"Enter (eastern) longitude of observatory (degree,min,sec): "
!
write/keyw inputr/r/1/9 {p1},0,0			!date
write/keyw inputr/r/4/3 {p2},0,0			!UT
write/keyw inputr/r/7/3 {p3},0,0			!longitude
rneg = (inputr(7)*3600.)+(inputr(8)*60.)+inputr(9)
inputc(1:1) = p2(1:1)
if rneg .lt. 0.0 inputc(1:1) = "-"
!
action(1:2) = "US"			!indicate conversion UT -> ST
run MID_EXE:COMPXY
!
outputd(19) = outputr(1)+(outputr(2)+outputr(3)/60.)/60. !ST in real hour
set/format i2 f10.3,g24.12
write/out "Julian date:                 {outputd(1)}"
outputi(1) = outputr(1)
outputi(2) = outputr(2)
write/out "Local mean sidereal time:    {outputi(1)} : -
{outputi(2)} : {outputr(3)}"
!
return						!finished with keyword option
!
TABLE:
!
! ... input mainly from table
!
define/param p2 -70,43,55.35 N -
"Enter (eastern) longitude of observatory (degree,min,sec): "
!
define/local nline/i/1/1 0			!number of lines in table
define/local lp/i/1/1 0				!loop variable
!	
write/keyw inputr/r/7/3 {p2},0,0       		!longitude
rneg = (inputr(7)*3600.)+(inputr(8)*60.)+inputr(9)
inputc(1:1) = p2(1:1)
if rneg .lt. 0.0 inputc(1:1) = "-"
!
! ... create table columns which shall receive results
!
set/midas output=loginly	!no error messages if columns exist already
create/column {in_a} :STHR 		!ST hours
create/column {in_a} :STMIN 		!ST minutes
create/column {in_a} :STSEC 		!ST sec
if m$existc("{in_a}","JD") .ne. -1 delete/column {in_a} :JD 
create/column {in_a} :JD G24.12 R*8	!Julian date
set/midas output=yes			!reset to default
!
nline = m$value({in_a},tblcontr(4))  !get number of entries in table
!
do lp = 1 {nline}                 	!loop over all lines in table
   copy/tk {in_a} :YEAR :MONTH :DAY @{lp} inputr/r/1/3     !date
   copy/tk {in_a} :UTHR :UTMIN :UTSEC @{lp} inputr/r/4/3   !UT
!
   action(1:2) = "US"			!indicate conversion UT -> ST
   run MID_EXE:COMPXY
!
   copy/kt outputr/r/1/3 {in_a} :STHR :STMIN :STSEC @{lp}    !ST
   {in_a},:JD,{lp} = outputd(1)                    	     !Julian date
   outputd(19) = outputr(1)+(outputr(2)+outputr(3)/60.)/60.  !ST in real
enddo
