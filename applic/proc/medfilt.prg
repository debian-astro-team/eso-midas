! @(#)medfilt.prg	19.1 (ESO-DMD) 02/25/03 13:20:15
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! MIDAS procedure medfilt.prg
! T.B.Georgiev, K.Banse      971129
!
! use via IFILTER/MEDIAN infr outfr window-diam thresh_coef rsdfr 
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input image:"
define/param p2 + IMA "Enter result image:"
define/param p3 25 N "Enter window diameter of circle (2/3/4..99):"
define/param p4 0. N "Enter coeff. C for threshold T=C*sigma:"
define/param p5 + IMA "Enter residual image:"
! 
define/local fw/i/1/1 25
define/local csig/d/1/1 0.0
! 
write/keyw in_a {p1}
define/local num_out/i/1/1 2
if p2(1:1) .eq. "+" then
   write/keyw num_out 1
else
   write/keyw out_a {p2}
endif
!
write/keyw fw {p3}
write/keyw csig {p4}
! 
if p5(1:1) .eq. "+" then
   write/keyw num_out 0
else
   write/keyw out_b {p5}
endif
! 
run APP_EXE:medfilt.exe

