! @(#)intalign.prg	19.1 (ESO-DMD) 02/25/03 13:20:13 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  intalign.prg  
!       to interactively align two 2-dim images 
! 
!	the goal is to align `input_ima' to `ref_ima'
!	i.e. our `result_ima' should be aligned with `ref_ima'
! 
! K. Banse	020103
!
! use via:
! ALIGN/INTER input_ima ref_ima result_ima align_opt scale_fact cuts_opt
! 
! with	input_ima  = name of image to be aligned
!		     (in FITS or Midas format)
!	ref_ima    = name of reference image
!		     (in FITS or Midas format)
!	result_ima = name of output image (aligned image)
!		     default = "align_res.bdf"
!	align_opt  = option of the ALIGN/IMAGE command
!		     FREE, EQUAL, UNIT, SHIFT 
!		     default = EQUAL
!	scale_fact = optional scaling factor when displaying images
!		     default = 1
!	cuts_opt   = optional flag for cut values,
!		     Y, display ref_image with same cuts as input image
!		     else, no
!		     default = No
! 
! Note: read the help of ALIGN/IMAGE for an explanation of the options!
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
clear/display
!
define/param p1 ? ima "Enter input image:"  
define/param p2 ? ima "Enter reference image:"  
define/param p3 align_res ima "Enter resulting frame name:"
define/param p4 EQUAL c"Enter ALIGN/IMAGE option:"
define/param p5 0 n "Enter scaling factor:"
define/param p6 No c "Enter cuts option (Yes/No):"

!
define/local filea/c/1/60 " " ALL
define/local fileb/c/1/60 " " ALL
define/local filec/c/1/60 " " ALL
define/local comtab/c/1/60 "middummx.tbl "
! 
write/keyw filea {p1}
write/keyw fileb {p2}
write/keyw filec {p3}
if p5(1:1) .eq. "0" then
   define/local scaling/c/1/4 "? "
else
   define/local scaling/c/1/20 "SCALE={p5} "
endif
!
! -----------------------------------------------------------------
!
! we load the input and reference image into the display and
! use GET/CURSOR to get two tables with coords of the same objects
! in both images
!
! -----------------------------------------------------------------
!
write/out 
write/out LOAD/IMAGE {filea} {scaling}
load/image {filea} {scaling}
write/out "We create table" `{comtab}' "and use it to store the cursor" 
write/out "define rectangles of the image to be aligned:"
!
write/out GET/CURSOR {comtab} ? yn 999,2
get/cursor {comtab} ? yn 999,2
!
write/out CENTER/GAUSS {filea},{comtab} centera
center/gauss {filea},{comtab} centera
clear/chan ov
! 
! now we work on the second frame
! 
if p6(1:1) .eq. "Y" then
   cuts {fileb} {filea}
endif
! 
write/out LOAD/IMAGE {fileb} {scaling}
load/image {fileb} {scaling}
write/keyw comtab middummy.tbl
write/out 
write/out "Now we create table" `{comtab}' "and use it to store the cursor" 
write/out "defined rectangles of the reference image:"
!
write/out GET/CURSOR {comtab} ? yn 999,2
get/cursor {comtab} ? yn 999,2
!
write/out CENTER/GAUSS {fileb},{comtab} centerb
center/gauss {fileb},{comtab} centerb
clear/chan ov
!
! -----------------------------------------------------------------
!
! we use the two tables to align the images
! and display the aligned (result) image
!
! -----------------------------------------------------------------
!
write/out 
write/out "Now we can align the two images via:"
write/out ALIGN/IMAGE centera centerb {p4} 
write/out 
align/image centera centerb {p4} 
!
write/out "Finally we rotate the input image `{filea}' according to"
write/out  the results of the ALIGN/IMAGE command:
write/out REBIN/ROTATE {filea} {filec} keyword {fileb}
rebin/rotate {filea} {filec} keyword {fileb}
write/out 
write/out LOAD/IMAGE {filec} {scaling}
load/image {filec} {scaling}
 
