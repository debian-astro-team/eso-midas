! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  thumbs.prg  to provide a thumbsnail view of 2-dim images
! K. Banse      ESO - Garching	040210
!
! use via @a thumbs file_list [thumbsize] [label_color]
! where   file_list = ASCII file with names of images (one per line)
!                     e.g. created through: $ ls *.bdf > mydir.list
!         thumbsize = no. of pixels in x-dim for (square) thumbnail image
!		        defaulted to: 80 (i.e. thumbnails = 80x80 pixels)
!         label_color = the color for writing the filenames (in ASCII)
!		        defaulted to: white
! then: Midas> @a thumbs mydir.lis	will create the required view
! 
! it is assumed there is already a display window of at least 512 x 512
!
! 040210	last modif
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? c "Enter filename containing images to be viewed:"
define/param p2 80 n "Enter nopix for one side of square thumbnail:"
define/param p3 white c "Enter color for writing the filenames:"
! 
define/local loop/i/1/2 0,0
define/local looplim/i/1/2 0,0
define/local ximoff/i/1/1 1
define/local yimoff/i/1/1 21
define/local xlabel/i/1/1 1
define/local ylabel/i/1/1 1
define/local xdist/i/1/1 1
define/local ydist/i/1/1 1
define/local npmax/i/1/1 0
define/local scc/i/1/1 0
define/local thsize/r/1/1 {p2}
define/local fc/i/1/2 0,0
! 
xdist = {p2} + 20
ydist = {p2} + 40		!extra space for label
looplim(1) = ididev(2) / xdist
looplim(2) = ididev(3) / ydist
! 
clear/chan			!a display must already exist
clear/chan over
! 
open/file {p1} read fc
if fc(1) .lt. 0 then
   write/out could not open file {p1}
   return/exit
endif
! 
set/format i1
do loop(2) = 1 looplim(2)
   ximoff = 1				!reset for loop along x-axis
   xlabel = 1
   loop = 1
   !
  read_data:
   read/file {fc(1)} in_a 
   if fc(2) .lt. 0 goto endy		!EOF reached
   ! 
   outputi = m$filtyp(in_a," ")
   if outputi .eq. 1 then		!only use images
      inputi = {{in_a},naxis}
      if inputi .gt. 1 then		!omit 1-dim frames
         npmax = {{in_a},npix(1)}
         inputi = {{in_a},npix(2)}
         if npmax .lt. inputi npmax = inputi
         inputr = npmax/thsize
         scc = m$nint(inputr)
         load/image {in_a} scale=-{scc} p6=up,over p7=1,1,{ximoff},{yimoff}
         label/display {in_a} {ylabel},{xlabel} ? {p3}
         ximoff = ximoff + xdist
         xlabel = xlabel + xdist
         loop = loop + 1
         if loop .gt. looplim goto y_loop
      endif
   endif
   goto read_data			!we fill first a full x-line
   !
  y_loop:
   yimoff = yimoff + ydist
   ylabel = ylabel + ydist
enddo   
! 
endy:
close/file {fc(1)}

