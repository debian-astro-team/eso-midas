! @(#)scanima.prg	19.1 (ES0-DMD) 02/25/03 13:20:18
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure scanima.prg to convert a graph represented as a 2-dim image
! back into a 1-dim image or a table
! K. Banse	ESO - Garching		940203
!
! use as:  @a scanima in out threshold option x-ref y-ref sub_specs win_specs
! 
! where    in = name of image frame containing the graphics
!          out = name of 1-dim result image or table
!		 to indicate a table enter `name,t' instead of just `name'
!          threshold = single_val or low,hi to specify either the single
!                      detection value for graph or an interval;
!                      that is either a pixel belongs to the graph
!		       if its intensity is >= single_val
!		       or if it is in the interval [low,hi]
!		       defaulted to (MIN+MAX)/2 of input frame
!          option = 3 char-string
!		   (1) = B(ottom) or T(op), for each column search from bottom 
!			 or top of graph for first pixel >= threshold
!		   (2) = G(lobal) or F(ixed Window) or D(ynamic Window),
!			 search globally, i.e. beginning at lowest/highest
!			 pixel in each column of graph;
!			 or search in sliding window of fixed size centered
!			 at last y-pixel beginning at bottom or top of window;
!			 or search in sliding window (again centered at last
!			 y-pixel) oscillating down + up or up + down depending
!			 on option(1) up to given size of window
!		   (3) = M(aximum) or V(alue), search for maximum pixel, which
!			 is above the (single) threshold, in each column
!			 or use value(s) given in `threshold' parameter above;
!			 M only applicable for option(2) = 'G' or 'F';
!		   defaulted to BGV
!          x-ref = xg1,xr1,xg2,xr2 
!                  2 * x-frame-pixels of graphics image,ref_value for output
!          y-ref = yg1,yr1,yg2,yr2 
!                  2 * y-frame-pixels of graphics image,ref_value for output
!	     (these values are usually obtained via the GET/CURSOR command)
!            if x-ref, y-ref is omitted output will have start=0.0 and step=1.0
!	   sub_specs = [xa,ya:xb,yb] to indicate if we should only work on a
!		       subframe of the image, e.g to avoid the frame/axes
!		       drawn around a graph;
! 		   if omitted, the complete input image will be scanned
!	   win_specs = wsize or wsize,xpix,ypix - window size in pixels,
!		                      and start x,ypixel (in frame pixels)
!		       if xpix,ypix is omitted, we search for "first" pixel
!		       according to `option(3)'
!		       this parameter only used for `option(2)' = 'F' or 'D'
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter input image:
define/param p2 ? ima "Enter output image:
define/param p3 + ?  "Enter threshold: "
define/param p4 bgv ? "Enter option:"
define/param p5 + ?  "Enter xreference as xg1,xr1,xg2,xr2: "
define/param p6 + ?  "Enter yreference as yg1,yr1,yg2,yr2: "
define/param p7 + ?  "Enter [xa,ya:xb,yb] if work only on subframe: "
if p4(2:2) .ne. "G" then
   define/param p8 + ?  "Enter wsize,start-xpix,ypix: "
endif
!
write/keyw in_a {p1}
write/keyw out_a {p2}
! 
write/keyw history "@a scanima "
run APP_EXE:scanima.exe
