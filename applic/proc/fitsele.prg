! @(#)fitsele.prg	19.1 (ES0-DMD) 02/25/03 13:20:11
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : FITSELE.PRG
!  FIT Subsystem
!
!  J.D.Ponz                ESO - Garching    5 APR 85
! .PURPOSE
!
!    implements
!
!  SELECT/FIT name number[,...]
!
!
! ----------------------------------------------------------------------
!
IF P2(1:1) .EQ. "A" THEN
  WRITE/KEYW INPUTI/I/1/17 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,0
ELSE
  WRITE/KEYW INPUTI/I/1/17 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  WRITE/KEYW INPUTI/I/1/16 'P2'
ENDIF
RUN APP_EXE:FITSELE       ! SELECT COMMAND
