! @(#)tbltool.prg	19.1 (ES0-DMD) 02/25/03 13:20:20
! Procedure : tbltool.prg
! Author    : Pascal Ballester   ESO - Garching
! Date      : 910207
! Purpose   : Toolbox for table applications
!

!******************* Sum a column of a table in a keyword  ***************
!
! Syntax : @a tbltool,addup <table_name.tbl> <column> <keyword/descriptor>
!
! Note   : keyw/descr must be declared and initialized already
!          for big columns, see MAP/TABLE and UNMAP/TAB
!
ENTRY ADDUP

DEFINE/LOCAL  LOOP/I/1/1  0
DEFINE/LOCAL  LAST/I/1/1  {{P1},TBLCONTR(4)}

DO LOOP = 1  {LAST}
      {P3} = {P3} + {{P1},{P2},@{LOOP}}
ENDDO

WRITE/OUT "{P3} = {{P3}}"

RETURN


