! @(#)defcirc.prg	19.1 (ES0-DMD) 02/25/03 13:20:08
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  defcirc.prg
!
! B. Pirenne  vers. 1.0  JAN. 86
! KB  900712, 920413
! PURPOSE: Compute radius and center coordinates of a circle defined by
!          4 points
!
!--------------------------------------------------------------------
!
WRITE/OUT "  ===> ENTER CURSOR 4 TIMES (L_R_U_D):"
!
DEFINE/LOCAL   I/I/1/1        0
DEFINE/LOCAL   J/I/1/1        0
DEFINE/LOCAL   C/R/1/8        0.,0.,0.,0.,0.,0.,0.,0.
WRITE/KEYW   CIRCLE/R/1/3   0.,0.,0.
!
DO I = 1 8 2
   GET/CURSOR ? ? ? 1,1
   C({I}) = OUTPUTR(10)
   J = I + 1
   C({J}) = OUTPUTR(11)
ENDDO
!
CIRCLE(3) = ((C(3)-C(1))+(C(6)-C(8)))/4
CIRCLE(1) = (C(1)+C(3)+C(5)+C(7))/4
CIRCLE(2) = (C(2)+C(4)+C(6)+C(8))/4
!
CLEAR/CHANNEL OVER
CLEAR/CURSOR
