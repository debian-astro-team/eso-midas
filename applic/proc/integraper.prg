! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! MIDAS procedure  integraper.prg  for INTEGRATE/APERTURE
! Ch. Ounnas    840803
!.USE:          use as 
!	        INTEGRATE/APERTURE in_specs out_specs [radius[,pix_flag]]
!               in_specs = CURSOR, or GCURSOR,
!		           or inframe,xc[,yc]  or  inframe,intable
!               out_specs = ?, just display data
!	                  = table for results
!               radius = no. of pixels, can be ? for CURSOR/GCURSOR input 
!               pix_flag = F if we use frame pixels, else world coords
!.VERSION:      931108 RHW {} stuff
! 070629	last modif
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 CURSOR
! 
write/keyw inputc {p2}				!fill output_specs
!
define/local k/i/1/3 0 all
k(1) = m$index(p1,",")				!for inframe,table
if k .gt. 0 then				!we need radius,pixeltype
   k(2) = k
   define/param p3 ? C "Enter radius[,pixflag] of aperture:"
endif
! 
! work on radius[,pixflag]
! 
inputr(1) = -1.0
k(3) = m$index(p3,",")
if k(3) .gt. 0 then				!radius,pixeltype
   k = k(3) - 1
   inputr(1) = {p3(1:{k})}
   k = k + 2
   in_b(1:1) = p3({k}:{k})
   if in_b(1:1) .eq. "F" then
      inputr(2) = 1.0				!inputr(2) > 0: frame pixels
   else
      inputr(2) = -1.0				!inputr(2) < 0: worldcoords
   endif
else
   inputr(2) = -1.0				!default to worldcoords
   if p3 .ne. "?" inputr(1) = {p3}	
endif
!
! either inframe,intable (inframe,xc[,yc])
! 
if k(2) .gt. 0 then			
   k = k(2) - 1
   write/keyw in_a {p1(1:{k})}
   k = k + 2
   write/keyw in_b {p1({k}:>)}
   if m$tstno(in_b) .eq. 1 then			!we got fixed coords
      write/keyw inputr/r/11/2 {in_b}
      in_b(1:8) = "*       "
   endif
!
! or display/graphics cursor input
!
else
   if p1(1:5) .eq. "GCURS" then			!1-dim frame
      write/keyw in_a **			!use plotted image
   else
      if p3 .eq. "?" then		
         set/cursor 0 circle 100,100,20 s
      else
         set/cursor 0 open_cross,white
      endif
      write/keyw in_a *				!use displayed image
   endif
endif
! 
run APP_EXE:INTEAPER

