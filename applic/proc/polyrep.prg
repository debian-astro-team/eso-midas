! @(#)polyrep.prg	19.1 (ES0-DMD) 02/25/03 13:20:17
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure polyrep.prg to use REPLACE/POLYGON with Cursor or CursorTable
! K. Banse      950209
!
! use via @a polyrep in_spec out_frame value in_out scaling_fact load_flag 
! 
! with in_spec  = input_image or in_image,in_table
!      out_frame = result image
!      value = constant for replacing (defaulted to 0.0)
!      in_out = IN or OUT to replace inside or outside the polygon
!               (defaulted to IN)
!      scaling_fact = scale-x,scale-y as in the LOAD/IMAGE command
!               (defaulted to 1,1)
!      load_flag = YES or NO if the result image should be displayed or not
!                  (defaulted to YES)
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/par p1 ? c "Enter input image or in_image,in_table: "
define/par p2 ? image "Enter output image: "
define/par p3 0.0 n "Enter constant for replacing: "
define/par p4 in c "Enter I/O for inside/outside replacement:"
define/par p5 1,1 n "Enter scaling factors: "
define/par p6 yes c "Enter YES/NO for loading result frame: "
! 
define/local optio/i/1/1 0
define/local myframe/c/1/60 " " all
define/local scale/i/1/4 1,1,0,0
write/keyw scale {p5},{p5}
if scale(1) .eq. 1 .and. scale(2) .eq. 1 then
   define/local scflag/i/1/1 0
else
   define/local scflag/i/1/1 1
endif
define/local rlim/r/1/2 -0.01,0.01
! 
optio = m$index(p1,",")
if optio .gt. 1 then
   inputi = optio+1
   define/local intab/c/1/60 {p1({inputi}:)}
   inputi = inputi-2
   write/keyw myframe {p1(1:{inputi})}
else
   define/local intab/c/1/60 midtest
   write/keyw myframe {p1}
endif
! 
load/image {myframe} scale={scale(1)},{scale(2)}
! 
! get first + last screen pixel of diplayed image
define/local kx/i/1/2 0,0
kx(1) = idimemi(2)+1
kx(2) = kx(1)+idimemi(4)-1
define/local ky/i/1/2 0,0
ky(1) = idimemi(3)+1
ky(2) = ky(1)+idimemi(5)-1
! 
if optio .lt. 1 then
   write/out "use cursor to mark the nodes of the polygon"
   write/out "last node will be connected to first node automatically"
   get/cursor {intab}
endif
! 
clear/channel overlay
show/table {intab} >null		!get no. of rows, output -> file `null'
define/local tablim/i/1/1 {outputi(2)}
! 
define/local loop/i/1/1 1
define/local x1/r/1/1 0.0
define/local y1/r/1/1 0.0
define/local x2/r/1/1 0.0
define/local y2/r/1/1 0.0
! 
do_loop:
x1 = {{intab},:x_coord,@{loop}}
y1 = {{intab},:y_coord,@{loop}}
loop = loop+1
if loop .gt. tablim then
   x2 = {{intab},:x_coord,@1}
   y2 = {{intab},:y_coord,@1}
else
   x2 = {{intab},:x_coord,@{loop}}
   y2 = {{intab},:y_coord,@{loop}}
endif
draw/line {x1},{y1},{x2},{y2} f >null
if loop .le. tablim goto do_loop
! 
! now get the image with the polygon
copy/display ? stop >null
! 
write/out "building up intermediate mask from displayed image"
if scflag .eq. 0 then
   if {{myframe},npix(1)} .gt. ididev(2) then
      compute/ima midtest = {myframe}*0
      insert/ima screen{mid$sess(11:12)}.ima midtest @{idimemi(6)},@{idimemi(7)}
   else
      extract/ima midtest = -
      screen{mid$sess(11:12)}.ima[@{kx(1)},@{ky(1)}:@{kx(2)},@{ky(2)}]
   endif
   compute/ima midtest2 = {myframe}
! 
else
   extract/ima midtest = -
   screen{mid$sess(11:12)}.ima[@{kx(1)},@{ky(1)}:@{kx(2)},@{ky(2)}]
   @a scaler {myframe} midtest2 {scale(1)},{scale(2)}
endif
! 
write/out "doing REPLACE/POLYGON"
define/local plocol/r/1/2 {ididev(17)},0.0	!get interval of plot colors
plocol(2) = plocol(1)+20.
if p4(1:1) .eq. "I" then
   replace/poly midtest2 midtest3 midtest/{plocol(1)},{plocol(2)}=0.0
else
   replace/poly midtest2 midtest1 midtest/{plocol(1)},{plocol(2)}=0.0
   compute/image midtest3 = midtest2-midtest1
endif
! 
if scflag .eq. 0 then
   if p3 .ne. "0.0" then
      replace/image {myframe} {p2} midtest3/{rlim(1)},{rlim(2)}={p3}
   else
      compute/image {p2} = midtest3
   endif
else
   define/local mscale/i/1/2 0,0
   mscale(1) = -scale(1)
   mscale(2) = -scale(2)
   @a scaler midtest3 polygon.mask {mscale(1)},{mscale(2)}
   replace/image {myframe} {p2} polygon.mask/{rlim(1)},{rlim(2)}={p3}
endif
! 
if p6(1:1) .eq. "Y" then
   write/out "displaying the result frame"
   load/image {p2} scale={scale(1)},{scale(2)}
endif
! 
if aux_mode .lt. 2 then			!remove temporary frames
   -delete midtest*.*.*
else
   -delete midtest*.*
endif
