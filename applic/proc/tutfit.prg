!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!      Demo. procedure for FITTING procedure
!      Execute as TUTORIAL/FIT
!      The following files are copied into your directory :
!           ORIGINAL.FIT      -- Original function
!           FUNCTOIN.FIT      -- function to be fitted
!      The procedure creates the working files
!           REF.BDF, TEST.BDF, NOISE.BDF, PROFILE.BDF, FITTED.BDF
!
!    -  Files will be deleted after demo.
!
!      J.D.Ponz 		ESO - Munich    version 1.0  April. 16 , 1985
!                		ESO - Munich            2.0  January 13, 1986
!
! 080328	last modif
!----------------------------------------------------------------
! 
write/out Functions are copied into your directory
if aux_mode(1) .le. 1 then              ! VMS
   define/local testdir/c/1/80 "MID_DISK:[&MIDASHOME.DEMO.DATA] "
   define/local defdir/c/1/2 "[]"
   $COPY {testdir}original.ffits {defdir}
   $COPY {testdir}function.ffits {defdir}
else
   define/local testdir/c/1/80 "$MID_TEST/ "
endif
-copy {testdir}original.ffits  ./original.ffits
-copy {testdir}function.ffits  ./function.ffits
indisk/mfits original.ffits ? ? no >Null
-rename toto0001.bdf ORIGINAL.fit
indisk/mfits function.ffits  ? ? no >Null
-rename toto0001.bdf FUNCTION.fit
!
create/graph
SET/GRAPHICS  BIN=ON
!
WRITE/OUT **************************************************
WRITE/OUT This demo will show how to handle functions
WRITE/OUT in the context of non linear fitting.
WRITE/OUT The procedure will create 1-D frame consisting
WRITE/OUT of 2 gaussians and non-linear background,
WRITE/OUT with some additional noise
WRITE/OUT The fitting is done using different methods
WRITE/OUT **************************************************
!
ECHO/OFF
WRITE/OUT This is the original function:
ECHO/ON
READ/FIT  ORIGINAL
!
ECHO/OFF
WRITE/OUT Now create a reference image
ECHO/ON
CREATE/IMAGE REF 1,200 1.,1.
!
ECHO/OFF
WRITE/OUT Create the function values using reference image
ECHO/ON
COMPUTE/FUNCTION TEST = ORIGINAL(REF)
SET/GRAPHIC  BIN=OFF
PLOT             TEST
!
ECHO/OFF
WRITE/OUT Add noise to the test image
ECHO/ON
! CREATE/IMAGE NOISE = REF R/GAUSS 0.,1.
CREATE/RANDOM NOISE = REF GAUSS 0.,1.
COMPUTE/IMAGE PROFILE = TEST+20.*NOISE
SET/GRAPHIC  BIN=ON
PLOT          PROFILE
!
ECHO/OFF
WRITE/OUT The function to be fitted is
ECHO/ON
READ/FIT     FUNCTION
!
SET/FIT      METHOD=NR
SHOW/FIT
!
FIT/IMAGE 11,1.,0.5 PROFILE FUNCTION
!
!! WRITE/OUT Fit the function
!! WRITE/OUT Method:CGNND (Corrected Gauss-Newton no derivatives)
!! WRITE/OUT SET/FIT METHOD=CGNND
!! SET/FIT   METHOD=CGNND
!! WRITE/OUT SET/FIT PRINT=0  (Print=0 : only final results are printed)
!! SET/FIT   PRINT=0
!! WRITE/OUT FIT/IMAGE 1 PROFILE FUNCTION
!! FIT/IMAGE 100,.5 PROFILE FUNCTION
!
!! WRITE/OUT Fit the function
!! WRITE/OUT Method:QN (Quasi-Newton derivatives required)
!! WRITE/OUT SET/FIT METHOD=QN
!! SET/FIT   METHOD=QN
!! WRITE/OUT SET/FIT PRINT=-4  (Print=-4 : impresions every 4 iterations)
!! WRITE/OUT                   (Print <0 : with the correlations matrix)
!! SET/FIT   PRINT=-4
!! WRITE/OUT FIT/IMAGE 30,0.5 PROFILE FUNCTION
!! FIT/IMAGE 30,.5 PROFILE FUNCTION
!
echo/off
WRITE/OUT SHOW/FIT     (to see qualifiers)
SHOW/FIT
!
!! WRITE/OUT Fit the function
!! WRITE/OUT Method:MGN (Modified Gauss-Newton derivatives required)
!! WRITE/OUT SET/FIT METHOD=MGN
!! SET/FIT   METHOD=MGN
!! WRITE/OUT SET/FIT PRINT=-0.1 : only final results are printed as 0
!! WRITE/OUT                      as <0 correlations matrix printed
!! WRITE/OUT FIT/IMAGE 30,.5 PROFILE FUNCTION
!! FIT/IMAGE 30,.5 PROFILE FUNCTION
!
WRITE/OUT Compute fitted values
ECHO/ON
COMPUTE/FIT FITTED = FUNCTION
SET/GRAPHIC    BIN=OFF
OVER        FITTED
!
ECHO/OFF
WRITE/OUT   Show the two components plus background
ECHO/ON
SELECT/FUN  FUNCTION 1,3
COMPUTE/FIT FIT1 = FUNCTION
SELECT/FUN  FUNCTION 2,3
COMPUTE/FIT FIT2 = FUNCTION
SET/GRAPHIC    BIN=ON
PLOT        PROFILE
SET/GRAPHIC BIN=OFF
OVER        FIT1
OVER        FIT2
SELECT/FUN  FUNCTION ALL
!
echo/off
write/out Automatic tutorial for fitting functions terminated o.k.
write/out 

