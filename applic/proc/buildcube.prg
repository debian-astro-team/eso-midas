! @(#)buildcube.prg	19.1 (ESO-DMD) 02/25/03 13:20:06
! +++++++++++++++++++++++++++++++++++++++++++++++
! 
!  Midas procedure  buildcube.prg
!  K. Banse	ESO - Garching	980114
! 
!  build a 3-dim frame from a set of 2-dim frames (stored in a Midas catalog)
!  these frames can be either in Midas or in FITS format
!  result cube will be in Midas format (i.e. a bdf file)
! 
!  it is assumed that all 2-dim frames are of the same size...
! 
!  use via: @a buildcube cat_name cube_name 
!  
!  with cat_name = name of image catalog holding the frame names
!       cube_name = name of result cube
! +++++++++++++++++++++++++++++++++++++++++++++++
! 
define/par p1 ? c "Enter catalog of input images: "
define/par p2 rescube Ima "Enter name of result cube:"
! 
set/format i1
show/icat {p1} no			!get no. of entries in catalog
define/local framecount/i/1/1 {outputi(1)}
define/local loop/i/1/1 0
! 
! get size, start + step values of first frame in catalog
define/local nx/i/1/1 0
define/local ny/i/1/1 0
define/local xsta/d/1/1 0.
define/local ysta/d/1/1 0.
define/local xstp/d/1/1 0.
define/local ystp/d/1/1 0.
! 
define/local frame/c/1/60 #1_{p1}
nx = m$value({frame},npix(1))
ny = m$value({frame},npix(2))
xsta = m$value({frame},start(1))
ysta = m$value({frame},start(2))
xstp = m$value({frame},step(1))
ystp = m$value({frame},step(2))
! 
! now create the output cube
! 
create/image {p2} 3,{nx},{ny},{framecount} {xsta},{ysta},0.,{xstp},{ystp},1.
insert/image {frame} {p2} <,<,<
! 
! insert all other frames
! 
do loop = 2 {framecount}
   insert/image #{loop}_{p1} {p2} <,<,@{loop}
enddo
! 
write/out cube {p2} with {framecount} planes created
