! @(#)slicube.prg	19.1 (ES0-DMD) 02/25/03 13:20:19
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure slicube.prg to extract 2-D frames from a 3-D cube
! K. Banse	901212, 950905
!
! use as   @a slicube 3-D_frame 2-D_result step_axis
! where 3-D_frame = three dimensional input frame
!       2-D_result = max. 4 char. root, a 4-digit number will be appended
!       step_axis = coordinate in which to step through 3-D frame (X, Y or Z)
!
! Example:  @a slicube mycube myimage z
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input image:"
define/param p2 out ima
define/param p3 Z
!
define/local k/i/1/1 0			!loop variable
!
info/descr {p1} npix
if mid$info(1) .ne. 1 then
   write/out {p1} not a Midas image...
   return
endif
if mid$info(2) .lt. 3 then
   write/out {p1} not a 3-dim image...
   return
endif
! 
if p3(1:1) .eq. "X" then
   define/local pix/i/1/1 {{p1},npix(1)}
   do k = 1 {pix}
      extract/image {p2(1:>)}{k} = {p1}[@{k},<,<:@{k},>,>]
      write/out frame {p2(1:>)}{k} extracted
   enddo
! 
elseif p3(1:1) .eq. "Y" then
   define/local pix/i/1/1 {{p1},npix(2)}
   do k = 1 {pix}
      extract/image {p2(1:>)}{k} = {p1}[<,@{k},<:>,@{k},>]
      write/out frame {p2(1:>)}{k} extracted
   enddo
! 
else
   define/local pix/i/1/1 {{p1},npix(3)}
   do k = 1 {pix}
      extract/image {p2(1:>)}{k} = {p1}[<,<,@{k}:>,>,@{k}]
      write/out frame {p2(1:>)}{k} extracted
   enddo
endif
