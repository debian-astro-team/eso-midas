! @(#)fitrepf.prg	19.1 (ES0-DMD) 02/25/03 13:20:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : FITREPF.PRG
!  FIT Subsystem
!
!  P.DEFERT                ESO - Garching   1 APR 84
!                                          26 JUN 85
!  K. Banse	871214
!  M. Peron     920212    Unix implementation
!
! .PURPOSE
!
!    implements
!
! REPLACE/FUNCTION USER0,...
! to define the user functions in the fit package
!
!
!
! ----------------------------------------------------------------------
!
IF FITCHAR(1:2) .EQ. "SY" THEN
  WRITE/OUT No\user\defined\functions\:\\\see\CREATE/FUNCTION\or\\\SET/FIT.
ELSE
    IF AUX_MODE(1) .LE. 1 THEN
       $ @ APP_PROC:fitrepf.com 'P1'  !VMS
    ELSE
       $ sh $APP_PROC/fitrepf.sh 'P1' !UNIX
    ENDIF
ENDIF
