! @(#)sprebi.prg	19.1 (ES0-DMD) 02/25/03 13:20:20
! +++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  SPECTRAL PROCEDURE : SPREBI.PRG
!  J.D.Ponz			version 1.0 131083
!
! .PURPOSE
!
!  execute the command :
!  REBIN/IPCS INPUT OUTPUT DW
! ---------------------------------------
! WRITE/KEYW INPUTR/R/1/1 'P3'
DEFINE/PARAM P1 ? IMAGE " Enter input image:"
DEFINE/PARAM P2 ? IMAGE " Enter output image:"
WRITE/KEYW IN_A/C/1/8   LINE
RUN APP_EXE:ECHREBINB
