! @(#)fitgues.prg	19.1 (ES0-DMD) 02/25/03 13:20:10
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : FITGUES.PRG
!  FIT Subsystem
!
!  J.D.Ponz                ESO - Garching   1 APR 84
!  KB			860911
!
! .PURPOSE
!
!    implements
!
!    MODIFY/FIT table seq [name]
!    SAVE/FIT  table seq [name]
!
! ----------------------------------------------------------------------
!
DEFINE/PARAM P1 ? TABLE "Enter table name:"
DEFINE/PARAM P2 ? ? "Enter seq:"
!
WRITE/KEYW INPUTI/I/1/1 'P2(2:>)'
RUN APP_EXE:FITCOPY
