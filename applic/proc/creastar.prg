! @(#)creastar.prg	19.1 (ES0-DMD) 02/25/03 13:20:08
!+++++
!.IDENT:   Procedure file CREASTAR.PRG
!.PURPOSE: procedure to create stellar image
!.USE:     CREATE/STAR in_frame in_table out_frame ? ?
!.AUTHOR:  Ch. Ounnas, ESO - Garching
!.VERSION: 840524     creation
!.VERSION: 931122 RHW new installation
!--------
DEFINE/PARAM P1 ?   IMA "Enter input frame:"
DEFINE/PARAM P2 ?   TAB "Enter input table:"
DEFINE/PARAM P3 ?   IMA "Enter output frame:"
DEFINE/PARAM P4 64
DEFINE/PARAM P5 {{P1},START(1)},{{P1},START(2)},{{P1},STEP(1)},{{P1},STEP(2)}
DEFINE/PARAM P6 0,0 N
DEFINE/PARAM P7 20  N
!
WRITE/KEYW IN_A/C/1/60  {P1}
WRITE/KEYW IN_B/C/1/60  {P2}
WRITE/KEYW OUT_A/C/1/60 {P3}
WRITE/KEYW INPUTI/I/1/1 {P4}
WRITE/KEYW INPUTR/R/1/4 {P5}
WRITE/KEYW INPUTR/R/5/2 {P6}
WRITE/KEYW INPUTR/R/7/1 {P7}
RUN APP_EXE:creastar
 
