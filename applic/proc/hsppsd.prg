! @(#)hsppsd.prg	19.1 (ES0-DMD) 02/25/03 13:20:12
!     Fourier coefficients of azimuthal profiles of spiral galaxis
!
!       @@ HSPPSD  psh-data  np2  imag
!
!     where the parameters are:
!
!     Date: Feb 20, 1989                            P.Grosbol
!
DEFINE/PARAM P1 ?    ? "Enter name of hsp-data file :"
DEFINE/PARAM P2 10,0 ? "Enter samples 2**n,method   :"
DEFINE/PARAM P3 ?    ? "Enter name of output image  :"
!
WRITE/KEYW  IN_A/C/1/40  'P1'
WRITE/KEYW  INPUTI/I/1/2 'P2'
WRITE/KEYW  OUT_A/C/1/40 'P3'
RUN APP_EXE:HSPPSD
