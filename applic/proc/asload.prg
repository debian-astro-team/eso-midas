! @(#)asload.prg	19.1 (ES0-DMD) 02/25/03 13:20:05
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  asload.prg  to assign a display + load an image
! K. Banse	901212
!
! use as   @a asload display_id image channel ...
! where      display_id = 0,1,... the display no used in CREATE/DISPLAY
!            image channel ... = the same parameters as in LOAD/IMAGE
! 
!            the new display will remain the active display
!
! Example:   if you put into your login.prg "CREA/COMM ASLOAD/IMAGE @a asload"
!            then
!                   @a asload 1 myimage scale=2,2
!            or  
!                   asload/image 1 myimage scale=2,2
!            will switch to display window 1 and load the image there
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 0 N "Enter display_id: "
! 
ASSIGN/DISPLAY D,'P1'
! 
LOAD/IMAGE 'P2' 'P3' 'P4' 'P5' 'P6' 'P7' 'P8'
