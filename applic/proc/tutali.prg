! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  tutali.prg  to implement TUTORIAL/ALIGN
! K. Banse	901108, 920413, 940503, 990116, 030926
!
! use via TUTORIAL/ALIGN ina,inb out ali_par com_table
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref INPUT OUTPUT PAR COM_TAB
!
clear/display
!
define/param p1 AUTO ? "Auto/Enter input frames:"
define/local auto_flag/i/1/1 0
!
if p1(1:4) .eq. "AUTO" .or. p1(1:6) .eq. "NOPLOT" then
   auto_flag = 1
endif
! 
if auto_flag .eq. 1 then
   define/param p2 "align_res" ima "Enter resulting frame name:"
   define/param p3 UNIT
else
   define/param p2 ? ima "Enter resulting frame name:"
   define/param p3 UNIT
!
   if p4(1:1) .eq. "?" then
      write/out "If you can use a common table for star centers"
      write/out "i.e. the sources in the two images are rather close"
      inquire/keyw p4 "enter its name otherwise hit RETURN"
   endif
endif
!
define/local l/i/1/5 0,0,0,0,0
define/local filea/c/1/60 " " ALL
define/local fileb/c/1/60 " " ALL
define/local filec/c/1/60 " " ALL
define/local comtab/c/1/60 " " ALL
!
if auto_flag .eq. 1 then
   write/out "we get the images" `spiral.fits` and `align_sp.fits` -
             "from directory MID_TEST"
   write/out "and convert them to Midas format"
   write/out "our goal is to align image" "`spiral.bdf` to image `align_sp.bdf`"
   write/out "i.e. our result `align_res.bdf`" should be -
             "aligned with `align_sp.bdf`"
   indisk/fits MID_TEST:spiral.fits spiral.bdf >Null
   indisk/fits MID_TEST:align_sp.fits align_sp.bdf >Null
   cuts align_sp spiral				!use same cuts
   write/keyw filea spiral
   write/keyw fileb align_sp
else
   l = m$index(p1,",")
   if l .le. 1 then
      write/out Invalid input ...
      return
   endif
!
   l(2) = l-1
   write/keyw filea {p1(1:{l(2)})}
   l(2) = l+1
   write/keyw fileb {p1({l(2)}:>)}
endif
!
!
write/keyw filec {p2}
write/keyw comtab {p4}
if comtab(1:1) .eq. "?" goto separate
!
! -----------------------------------------------------------------
!
! the input images are rather close, we can use the same table to calculate
! the star centers
!
! -----------------------------------------------------------------
!
write/out 
write/out we load the first image via:
write/out LOAD/IMAGE {filea}
load/image {filea}
write/out "We will create table" {comtab} -
          and use it to store the cursor defined rectangles:
!
write/out GET/CURSOR {comtab} ? yy 999,2
get/cursor {comtab} ? yy 999,2
clear/chan over
!
write/out "We now use table {comtab} as input for finding star centers"
write/out in {filea} and {fileb}
write/out CENTER/GAUSS {filea},{comtab} centera
center/gauss {filea},{comtab} centera
write/out CENTER/GAUSS {fileb},{comtab} centerb
center/gauss {fileb},{comtab} centerb
!
write/out "Let us load image {fileb} via LOAD/IMAGE {fileb}"
load/image {fileb}
goto common
!
! -----------------------------------------------------------------
!
! the input images are quite different, we use different tables to calculate
! the star centers
!
! -----------------------------------------------------------------
!
SEPARATE:
write/keyw comtab middummo
write/out 
write/out we load the first image via:
write/out LOAD/IMAGE {filea}
load/image {filea}
write/out "We will create table" `{comtab}` "and use it to store the cursor" -
           defined
write/out "rectangles of the first image:"
!
write/out GET/CURSOR {comtab} ? yn 999,2
get/cursor {comtab} ? yn 999,2
!
write/out "We now use table that" `{comtab}` as input for finding star centers
write/out in the first image `{filea}` :
write/out CENTER/GAUSS {filea},{comtab} centera
center/gauss {filea},{comtab} centera
inquire/keyw inputc "Hit RETURN when you're ready to continue"
! 
! now we work on the second frame
! 
write/out 
write/out "Now we do the same for the second frame."
write/keyw comtab middummp
write/out we load the second image via:
write/out LOAD/IMAGE {fileb}
load/image {fileb}
write/out "We will create table" `{comtab}` -
          and use it to store the cursor defined
write/out  rectangles of the second image:
!
write/out GET/CURSOR {comtab} ? yn 999,2
get/cursor {comtab} ? yn 999,2
!
write/out "We now use that table `{comtab}` as input for finding star centers:"
write/out CENTER/GAUSS {fileb},{comtab} centerb
center/gauss {fileb},{comtab} centerb
!
clear/chan overlay
!
! -----------------------------------------------------------------
!
! here the two options join
!
! -----------------------------------------------------------------
!
COMMON:
inquire/keyw inputc "Hit RETURN when you're ready to continue"
write/out 
write/out "Now we can align the two images via:"
write/out ALIGN/IMAGE centera centerb {p3} ov
write/out 
align/image centera centerb {p3} ov,200
!
write/out "Finally we rotate the first image `{filea}` according to"
write/out  the results of the ALIGN command:
write/out REBIN/ROTATE {filea} {filec} keyword {fileb}
rebin/rotate {filea} {filec} keyword {fileb}
write/out 
write/out "and load the output image `{filec}` in order to see the result:"
write/out LOAD/IMAGE {filec}
load/image {filec}
! 
if auto_flag .eq. 1 then
   if inputc(1:1) .eq. "Y" then
      -delete spiral.bdf
      -delete align_*.bdf
      -delete middumm*.tbl
      -delete center*.tbl
   endif
endif
