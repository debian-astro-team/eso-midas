! @(#)fonttxt.prg	19.1 (ESO-DMD) 02/25/03 13:20:11
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure fonttxt.prg  to display a message with given font
! K. Banse     990119
!
! use as @a fonttxt string font_no disp_font_no_flag
! with string = text, has to be enclosed in `"' if it contains spaces
!      font_no = a font no. (see file /midas/`version'/systab/bin/x11defs.dat
!                            for the corrsponding font names
!      disp_font_no_flag = Yes (default) or No, if Yes also the font_no is
!                 displayed
! 
!  see also the application procedure showfonts.prg
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 + ? "Enter string:"
define/param p2 + ? "Enter font no.:"
define/param p3 Y ? "Enter font_show_flag (Yes/No):"
! 
define/local kk/i/1/5 1,1,1,1,0
define/local fontno/i/1/1 140
define/local ifont/i/1/1 1
define/local str/c/1/80 " " all
! 
if p1(1:1) .eq. "+" then
 get_str:
   inquire/keyw str "Enter char. string (without surrounding quotes):"
   if aux_mode(7) .lt. 1 then
      if kk(5) .gt. 0 then
         return/exit
      else
         goto get_str
      endif
   endif
else
   write/keyw str {p1}
endif
! 
if p2(1:1) .ne. "+" then
   fontno = {p2}
   ifont = 1
   goto next
endif
write/out  "Enter font no. (`quit' + RETURN to stop)"
write/out "or + and RETURN to increment font no. (next RETURNs keep increasing)"
write/out "or - and RETURN to decrement font no. (next RETURNs keep decreasing)"
! 
loop:
write/keyw inputc " " all
inquire/keyw inputc "Enter font no. (`quit' + RETURN to stop)"
if inputc(1:1) .eq. "Q" return/exit
! 
if aux_mode(7) .lt. 1 then		!just RETURN
   if  ifont .eq. -1 then
      fontno = fontno - 1
      if fontno .eq. 0 fontno = 1
      goto next
   else if  ifont .eq. 1 then
      fontno = fontno + 1
      goto next
   endif
endif
! 
if inputc(1:1) .eq. "+" then
   ifont = 1
   fontno = fontno + 1
   goto next
else if inputc(1:1) .eq. "-" then
   ifont = -1
   fontno = fontno - 1
   if fontno .eq. 0 fontno = 1
   goto next
endif
! 
if m$tstno(inputca .ne. 1 then
   write/out "Please, enter valid no."
   goto loop
endif
fontno = {inputc}			!remains only a real font no.
! 
next:
if p3(1:1) .ne. "N" then
   label/display "({fontno}) {str}" {kk},10 ? ? -{fontno}
else
   label/display "{str}" {kk},10 ? ? -{fontno}
endif
! 
kk = kk + 30
inputi = ididev(3)-30
if kk .gt. inputi then
   clear/chan over
   kk = 2
endif
goto loop

