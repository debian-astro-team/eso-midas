! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure indisk.prg for INDISK/FITS 
!       use module infile.exe to convert all files in one go
! K. Banse	040108
! execute via INDISK/FITS in_files out_files option INTAPE_flags table_compress
! where
!        option = OC or ON to keep the original name of the files,
!		  i.e if the result files should be named according to the 
!		  FITS keyword FILENAME, so we also execute RESTORE/NAME 
!		  OC - with overwrite confirmation, ON - without confirmation
!		  root=abcd for result names abcd0001, abcd0002, ...
! 		  name=input for result names = input names with relevant Midas type
!               = NO for not keeping the original name
!	 INTAPE-FLAGS = like the 3char. flag of INTAPE/FITS 
!        tbl_compress = YES or NO => like INTAPE/FITS or INDISK/MFITS
! 
!     the total no. of input and result files created is stored in 
!     keyword MID$INFO(6,7)
! 
!.VERSION
!
! 100617	last modif
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref IN OUT OPTION FLAG TBLCOMP
!
!
define/param p1 ? c -
"Enter input FITS name(s) or ASCII file with list of names: "
define/param p2 root=toto c -
"Enter output MIDAS name(s) or ASCII file with list of names: "
define/param p3 NO ? "Enter option: "
define/param p4 NOY ? "Enter print,format,history option (for INTAPE):"
define/param p5 YES ? "Enter flag for dropping empty 1st header for tables:"
! 
define/local inspec/c/1/160 {p1} ? +lower
define/local intape_options/c/1/4 {p4} 
define/local drop_empty/c/1/4 {p5} 
! 
define/local kindx/i/1/2 0 ? +lower
define/local fin/i/1/2 -1,-1 ? +lower
define/local fout/i/1/2 -1,-1 ? +lower
define/local names/c/1/160 " " all +lower
define/local single/c/1/160 " " all +lower
!
write/keyw F$OUTNAM/c/1/40 " " all	!like in INDISK/MFITS
! 
! look for different input options
! 
kindx = m$index(inspec,"*")
if kindx .gt. 0 then			!wildcard stuff
   @a indisk,wildcard i__i
! 
else
   kindx = m$index(inspec,".cat")
   if kindx .gt. 1 then			!Midas/ASCII catalog of FITS files
      @a indisk,catalog i__i
   else
      @a indisk,single i__i		!single line of file names
   endif
endif
! 
! look for different result options
! 
define/local root/c/1/20 "*toto " ? +lower
! 
inputi = m$index(p2,"=")
if inputi .gt. 1 then
   inputi = inputi+1              !move to following char.
   if p2(1:1) .eq. "r" .or. p2(1:1) .eq. "R" then
      write/keyw root "*{p2({inputi}:>)}  "
      goto do_it
   else if p2(1:1) .eq. "n" .or. p2(1:1) .eq. "N" then
      write/keyw root "$$ "
      goto do_it
   endif
else
   inputi = m$index(p2,"*")
   if inputi .gt. 0 then
      inputi = inputi- 1
      write/keyw root "*{p2(1:{inputi})} "
      goto do_it
   endif
endif
! 
write/keyw inspec {p2} 
! 
kindx = m$index(inspec,".cat")
if kindx .gt. 1 then			!Midas/ASCII catalog of FITS files
   @a indisk,catalog r__r
else
   @a indisk,single r__r			!single line of file names
endif
write/keyw root "r__r.cat "
! 
do_it:
run MID_EXE:infile.exe
! 
if p3(1:1) .eq. "O" then
   open/file outnames.cat read fout
   if fout(1) .lt. 0 then
      write/out could not open outnames.cat, file with output names ...
      return/exit
   endif
   write/out restoring original filenames ...
   !
 reading:
   read/file {fout} names
   if fout(2) .le. 0 then		!check for EOF
      close/file {fout}
      return
   endif
   restore/name {names} no no {p3(2:2)}
   goto reading
endif
! 
! 
! entry for wildcard handling
! +++++++++++++++++++++++++++
!
entry wildcard
define/param p1 ? c "Enter name of result catalog:"
! 
kindx = m$index(inspec,",")
if kindx .gt. 0 then
   write/out currently "," and "*" cannot be used together...
   return/exit
endif
!				different code for VMS / Unix
if aux_mode .lt. 2 then
   define/local diri/c/1/1 ]
   open/file {p1}.cat write fout
   $dir/out=z__z.cat/column=1/version=1 {inspec}
   open/file z__z.cat read fin
   read/file {fin} names		!skip 1. line
   if fin(2) .le. 0 then
      write/error 30
      return/exit
   endif
   read/file {fin} names		!skip 2. line
   read/file {fin} names		!skip 3. line
   vms_read:
   read/file {fin} names
   if fin(2) .le. 0 .or. names(1:1) .eq. " " then
      close/file {fout}
   else
      write/file {fout} {names(1:160)}
      goto vms_read
   endif
   close/file {fin}
!
else
   define/local diri/c/1/1 /
   $ls {inspec} > {p1}.cat
endif
! 
! entry for catalog handling
! +++++++++++++++++++++++++++
! 
entry catalog
define/param p1 ? c "Enter name of result catalog:"
!
open/file {p1}.cat write fout
if fout .lt. 0 then
   write/out could not create file {p1}.cat for input names ...
   return/exit
endif
! 
open/file {inspec} read fin
if fin .lt. 0 then
   write/out could not open file {inspec} ...
   return/exit
endif
read/file {fin} names
if names(1:2) .eq. " =" read/file {fin} names
if fin(2) .lt. 1 then
   close/file {fin}
   close/file {fout}
   write/out empty catalog {inspec} ...
   return/exit
else
   goto copy_loop
endif
!
read_loop:
read/file {fin} names
if fin(2) .lt. 1 then
   close/file {fin}
   close/file {fout}
   return
endif
!
copy_loop:
if names(1:1) .eq. "!" goto read_loop
write/keyw single {names}			!to get rid of trailing stuff
write/file {fout} {single}
goto read_loop
! 
! entry for single line of names
! +++++++++++++++++++++++++++
!
entry single
define/param p1 ? c "Enter name of result catalog:"
!
open/file {p1}.cat write fout
if fout .lt. 0 then
   write/out could not create file {p1}.cat for input names ...
   return/exit
endif
! 
write/keyw names/c/1/160 {inspec}
! 
in_loop:
if names(1:1) .eq. " " then
   close/file {fout}
   return
endif
! 
kindx = m$index(names,",")
if kindx .gt. 1 then
   kindx = kindx-1
   write/keyw single "{names(1:{kindx})} "
   kindx = kindx+2
   write/keyw names "{names({kindx}:)} "
else
   write/keyw single "{names(1:)} "
   names(1:1) = " "
endif
write/file {fout},key single
goto in_loop



