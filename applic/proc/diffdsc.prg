! @(#)diffdsc.prg	19.1 (ESO-DMD) 02/25/03 13:20:08
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure diffdsc.prg to compare the descriptors (FITS header)
!                             of two Midas images
! K. Banse	970718
!
! execute as @a diffdsc ima_a ima_b
!
! with ima_a, ima_b - Midas images
! if the descriptors are not equal, keyword OUTPUTI(10) = 1
!                                   else    OUTPUTI(10) = 0
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter frame1: "
define/param p2 ? ima "Enter frame2: "
!
write/keyw in_a {p1}
read/descr {in_a} *,history full >temp.data
write/keyw in_b {p2}
read/descr {in_b} *,history full >temp.datb
! 
$sed '1 d' temp.data > diffdsc.data
$sed '1 d' temp.datb > diffdsc.datb
$rm -f temp.data
$diff diffdsc.data diffdsc.datb > temp.data
! 
outputi(10) = 0
define/local fc/i/1/2 0,0
open/file temp.data read fc
if fc(1) .ne. -1 then
   read/file {fc(1)} inputc 10
   if fc(2) .gt. 0 outputi(10) = 1
   close/file {fc(1)}
endif
! 
if outputi(10) .eq. 1 $more temp.data

