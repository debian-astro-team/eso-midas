! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure outdated.prg to handle obsolete commands
! K. Banse	020612, 071005
!
! use as   @a outdated command/keyword stop-flag
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ? "What's up?"
define/param p2 1 n "Enter stop flag:"
! 
write/out "since 07SEP command {p1} is not supported anymore!"
! 
if p2 .eq. 0 then
   write/out "Command ignored..."
else
   write/out "we terminate..."
   bye
endif
 
