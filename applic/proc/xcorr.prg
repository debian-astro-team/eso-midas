! @(#)xcorr.prg	19.1 (ESO-DMD) 02/25/03 13:20:21
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure xcorr.prg
!
! Correlate 2 1D frames in pixel coordinates (as in IHAP).
!
! use as XCORRELATE/IMAGE temp spec result shift
! with   temp = template
!        spec = spectrum
!        result = result frame
!        shift = max shif in pixels
!
!  M. Rosa	ST-ECF  870601
!  K. Banse	ESO - Garching	890508, 950120, 020108
!
!----------------------------------------------------------------------
!
define/param p1 ? I "Enter name of template frame: "
define/param p2 ? I "Enter name of spectral frame: "
define/param p3 xcorr I "Enter name of output frame: "
define/param p4 10 N "Enter 1/2 bandwith = shift of frames: "
define/param p5 N C -
"Enter YES or NO for pixelwise normalization of output frame:"
define/maxpar 5
!
write/keyw in_a {p1}
write/keyw in_b {p2}
write/keyw out_a {p3}
write/keyw inputi/i/1/1 {p4}
write/keyw action/c/1/2 C{p5}		!indicate correlation
!
write/keyw history "XCORRELATE/IMAGE "
run MID_EXE:FLIP
