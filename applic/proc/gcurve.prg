! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! MIDAS procedure gcurve.prg to emulate the IHAP GCURVE command
! K. Banse	ESO - Garching		070629
!
! Given an input image and coords of the center of an object as
! e.g. obtained from the CENTER/GAUSS command, this procedure
! will produce a table with radius, no of pixels within this radius,
! total flux, and normalized flux of the image within the given
! radius from the center.
! 
! use as   @a gcurve in_imag coord_input out_table max_radius [disp_flag]
! with     in_imag = input image (max 2-dim)
! 	   coord_input = xcent[,ycent] or in_table
!                xcent,ycent= center coords (as world coords)
!		 in_table must contain columns :x_coord,:y_coord with coords
! 	   out_table = table for results
!	   maximum radius (in pixels)
! 	   disp_flag = YES or NO or GRAPH to show the results
!
! Example:  @a gcurve sombrero 321.44,567.22 res_tab 32
!
! 070704	last modif
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input image:"
define/param p2 ? C "Enter center coords  or  in_table:"
define/param p3 ? T "Enter result table:"
define/param p4 ? N "Enter max. radius in pixels:"
define/param p5 Y C "show results (Y/N):"
!
define/local loopi/i/1/2 0,0		!loop counter, 1 -> max_radius
!
! 				create output table
create/table {p3} 3 {p4} null	
create/column {p3} :radius "Pixel" F6.1
create/column {p3} :npix "" F10.2
create/column {p3} :flux "10-18erg/cm2/s" G13.5
create/column {p3} :flux_norm "10-18erg/cm2/s" G13.5
! 
{p3},:radius,1 = 0.			!start with radius = 0
{p3},:npix,1 = 0.
{p3},:flux,1 = 0.
{p3},:flux_norm,1 = 0.
! 
do loopi = 1 {p4}
   loopi(2) = loopi + 1
   integrate/aper {p1},{p2} ? {loopi},f  >Null
   {p3},:radius,{loopi(2)} = {outputr(3)}
   {p3},:npix,{loopi(2)} = {outputr(4)}
   {p3},:flux,{loopi(2)} = {outputr(5)}
      outputr(6) = outputr(5)/outputr(4)
   {p3},:flux_norm,{loopi(2)} = {outputr(6)}
enddo
! 
if p5(1:1) .eq. "N" return
! 
if m$tstno(p2) .eq. 1 then
   write/keyw outputc {p2}
else
   write/keyw outputc {{p2},:x_coord,@1},{{p2},:y_coord,@1}
endif
write/out with
write/out image frame: {p1}, object at: {outputc}
! 
if p5(1:1) .eq. "G" then
   write/out yields plot
   if dazdevr(11) .lt. 0 create/graph
   plot/table {p3} :radius :flux_norm ? 1 1
! 
else if p5(1:1) .eq. "Y" then
   write/out yields
   read/table {p3}
endif  
