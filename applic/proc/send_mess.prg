! @(#)send_mess.prg	19.1 (ESO-DMD) 02/25/03 13:20:18
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure send_mess.prg  to display a message on an empty screen
! K. Banse     990119
! 
! use as @a send_mess string1 string2 ... string7 string8
! this procedure clears the screen and displays a max. of 8 centered messages
!                  `string1'
!                    ... 
!                  `string8'
! depending on the size of the display window a large or normal font is used
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
set/format i1
if ididev(2) .le. 512 then
   define/local font/i/1/1 238 
   define/local fact/r/1/1 14.0
else
   define/local font/i/1/1 585 
   define/local fact/r/1/1 16.2
endif
define/local m/i/1/5 0 all
! 
clear/disp 
! 
!  center string in display
! 
if pcount .eq. 1 then
   m = ididev(3)/2 - 30
   inputr = m$strlen(p1)*fact			!length of string in pixels
   m(3) = inputr
   m(2) = ididev(2)/2 - m(3)/2
   if m(2) .lt. 0 m(2) = 0
   !
   label/display {p1} {m},{m(2)} ? white -{font}
   inputi(1) = m(2)-14
   if inputi(1) .lt. 0 inputi(1) = 0
   inputi(2) = m(1)-20
   inputi(3) = m(2)+m(3)-14
   if inputi(3) .gt. ididev(2) inputi(3) = ididev(2)
   inputi(4) = m(1)+45
   draw/rect {inputi(1)},{inputi(2)},{inputi(3)},{inputi(4)}
! 
else
   define/local count/i/1/1 {pcount}
   ! 
   m = ididev(3)/2 - (count*15)	!offset for messages below center line
   ! 
   m(4) = count
   do m(5) = 1 count
      inputr = m$strlen(p{m(4)})*fact		!length of string in pixels
      m(3) = inputr
      m(2) = ididev(2)/2 - m(3)/2
      if m(2) .lt. 0 m(2) = 0
      label/display {p{m(4)}} {m},{m(2)} ? white -{font}
      m = m + 30
      m(4) = m(4) - 1
   enddo
endif 

