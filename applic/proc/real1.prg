! @(#)real1.prg	19.1 (ES0-DMD) 02/25/03 13:20:17
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : REAL1.PRG
!  TABLE Subsystem
!
!  M.C. Maccarone	IFCAI/CNR - Palermo	880509	  Vers.4.3
!  O.Di Rosa    	IFCAI/CNR - Palermo	880509	  Vers.4.3
!
! .PURPOSE
!  
!  DIFFERENCE/TABLE intab1 intab2 outtab flag_ext
!  PROJECTION/TABLE intab outtab column-ref1 [column-ref2 ... ] 
!  UNION/TABLE intab1 intab2 outtab flag_ext
!  COMSS/TABLE table 				(Compression)
!  PRODUCT/TABLE intab1 intab2 outtab flag_ext
!  NJOIN/TABLE intab1 intab2 outtab 
!  QUOTIENT/TABLE intab1 intab2 outtab flag_ext
!  CSORT/TABLE table flag_sort
!
!  DELETE/ROW table start_row no_row
!   or
!  DELETE/ROW table start_row end_row
!
! ----------------------------------------------------------------------    
! 
BRANCH MID$CMND(1:4) COMS,UNIO,PROJ,PROD,CSOR,DIFF,NJOI,QUOT,DELE-
 		     	COM,UNI,PRJ,PRD,CSO,DIF,NJO,QUO,DEL
!  
WRITE/ERROR 5
RETURN
!
! Compression command COMSS/TABLE
!
COM:
DEFINE/PARAM P1 ? TABLE
WRITE/KEYW IN_A/C/1/60 'P1'
RUN APP_EXE:TCOMPRTBL
RETURN
!
! Union command UNION/TABLE
!
UNI:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 ? TABLE
DEFINE/PARAM P3 ? TABLE
DEFINE/PARAM P4 YES CHAR
IF "'P1(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Input tables with same name !
 WRITE/OUT UNION command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P1(1:>)'" THEN
 WRITE/OUT Input and output tables with same name !
 WRITE/OUT UNION command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Input and output tables with same name !
 WRITE/OUT UNION command not performed !
 RETURN
ENDIF
WRITE/KEYW IN_A/C/1/60 'P1'
WRITE/KEYW IN_B/C/1/60 'P2'
WRITE/KEYW OUT_A/C/1/60 'P3'
WRITE/KEYW ACTION/C/1/1 'P4(1:1)'
! RUN APP_EXE:TUNIONTBL
RUN APP_EXE:TUNIONEXT
COMS/TABLE 'P3'
RETURN
!
! Projection command PROJECT/TABLE
!  
PRJ:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 ? TABLE
DEFINE/PARAM P3 NONE CHAR
IF "'P1(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Input and output tables with same name !
 WRITE/OUT PROJECTION command not performed !
 RETURN
ENDIF
IF "'P3(1:4)'" .EQ. "NONE'" THEN
 WRITE/OUT Undefined columns !
 WRITE/OUT PROJECTION command not performed !
 RETURN
ENDIF
RUN APP_EXE:TPROJETBL
RETURN
!
! Cartesian product command PRODUCT/TABLE
!  
PRD:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 ? TABLE
DEFINE/PARAM P3 ? TABLE
DEFINE/PARAM P4 NO CHAR
IF "'P1(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Input tables with same name !
 WRITE/OUT PRODUCT command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P1(1:>)'" THEN 
 WRITE/OUT Please change name of the output table !
 WRITE/OUT PRODUCT command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Please change name of the output table !
 WRITE/OUT PRODUCT command not performed !
 RETURN
ENDIF
WRITE/KEYW IN_A/C/1/60 'P1'
WRITE/KEYW IN_B/C/1/60 'P2'
WRITE/KEYW OUT_A/C/1/60 'P3'
WRITE/KEYW ACTION/C/1/1 'P4(1:1)'
RUN APP_EXE:TPRCARTBL
RETURN
!  
! Complete sorting command CSORT/TABLE
!
CSO:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 A CHAR
WRITE/KEYW IN_A 'P1'
WRITE/KEYW ACTION/C/1/1 'P2(1:1)'
RUN APP_EXE:TSORTTOT
RETURN
!
! Difference command DIFFERENCE/TABLE
!  
DIF:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 ? TABLE
DEFINE/PARAM P3 ? TABLE
DEFINE/PARAM P4 YES CHAR
IF "'P1(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Input tables with same name !
 WRITE/OUT DIFFE command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P1(1:>)'" THEN
 WRITE/OUT Input and output tables with same name !
 WRITE/OUT DIFFE command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Input and output tables with same name !
 WRITE/OUT DIFFE command not performed !
 RETURN
ENDIF
WRITE/KEYW IN_A/C/1/60 'P1'
WRITE/KEYW IN_B/C/1/60 'P2'
WRITE/KEYW OUT_A/C/1/60 'P3'
WRITE/KEYW ACTION/C/1/1 'P4'
RUN APP_EXE:TDIFFETBL
RETURN
!
! Natural join command  NJOIN/TABLE
!
NJO:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 ? TABLE
DEFINE/PARAM P3 ? TABLE
IF "'P1(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Natural join on the same table !
 WRITE/OUT NJOIN/TABLE command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P1(1:>)'" THEN
 WRITE/OUT Please change name of the output table !
 WRITE/OUT NJOIN/TABLE command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Please change name of the output table !
 WRITE/OUT NJOIN/TABLE command not performed !
 RETURN
ENDIF
WRITE/KEYW IN_A 'P1'
WRITE/KEYW IN_B 'P2'
WRITE/KEYW OUT_A 'P3'
RUN APP_EXE:NATUJOIN
RETURN
!
! Quotient command QUOTIENT/TABLE
!
QUO:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 ? TABLE
DEFINE/PARAM P3 ? TABLE
DEFINE/PARAM P4 Y CHAR
COPY/TABLE 'P1' DUMMY1
COMS/TABLE DUMMY1
COPY/TABLE 'P2' DUMMY2
COMS/TABLE DUMMY2
WRITE/KEYW IN_A/C/1/60 DUMMY1
WRITE/KEYW IN_B/C/1/60 DUMMY2
WRITE/KEYW OUT_A/C/1/60 'P3'
WRITE/KEYW ACTION/C/1/1 'P4'
RUN APP_EXE:TQUOTBL
DELETE/TABLE DUMMY1 N
DELETE/TABLE DUMMY2 N
RETURN
!
! Delete row command DELETE/ROW
!
DEL:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 ? ?
DEFINE/PARAM P3 1 N
RUN APP_EXE:TDELEROW
RETURN
!
