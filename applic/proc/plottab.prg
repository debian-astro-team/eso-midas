! @(#)plottab.prg	19.1 (ES0-DMD) 02/25/03 13:20:16
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure plottab.prg
! to plot the red, green and blue parts of a LUT or the ITT
! K. Banse	900703, 901212, 910425, 920423
!
! execute as   @a plottab,lut LUT
! LUT - name of LUT table, in own workspace or a system LUT 
! or as   @a plottab,itt ITT
! ITT - name of ITT table, in own workspace or a system ITT 
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! -----------------------------
ENTRY ITT
! -----------------------------
! 
DEFINE/PARAM P1 log C "Enter ITT: "
! 
DEFINE/LOCAL KOKO/C/1/60 " " all
KOKO = M$SYMBOL("MID_WORK")
DEFINE/LOCAL ITTTAB/C/1/80 {KOKO}{P1}.itt
INPUTI = M$EXIST(ITTTAB)
IF INPUTI .EQ. 0 WRITE/KEYW ITTTAB MID_SYSTAB:{P1}.itt
! 
SET/GRAPH COLOUR=1 LTYPE=1 STYPE=0 LWIDTH=1
! 
PLOT/AXES 0,255 -0.1,1.1 ? ITT:{P1} Intensity
SET/GRAPH COLOUR=4
OVERPLOT/TABLE {ITTTAB} ? :ITT 0
! 
! -----------------------------
ENTRY LUT
! -----------------------------
! 
DEFINE/PARAM P1 heat C "Enter LUT: "
! 
DEFINE/LOCAL KOKO/C/1/60 " " all
KOKO = M$SYMBOL("MID_WORK")
DEFINE/LOCAL LUTTAB/C/1/80 {KOKO}{P1}.lut
INPUTI = M$EXIST(LUTTAB)
IF INPUTI .EQ. 0 WRITE/KEYW LUTTAB MID_SYSTAB:{P1}.lut
! 
SET/GRAPH COLOUR=1 LTYPE=1 STYPE=0 LWIDTH=1
! 
PLOT/AXES 0,255 -0.1,1.1 ? LUT:{P1} Intensity
IF IDIDEV(18) .EQ. 11 THEN
   SET/GRAPH COLOUR=2
   OVERPLOT/TABLE {LUTTAB} ? :RED 0
   SET/GRAPH COLOUR=3
   OVERPLOT/TABLE {LUTTAB} ? :GREEN 0
   SET/GRAPH COLOUR=4
   OVERPLOT/TABLE {LUTTAB} ? :BLUE 0
ELSE
   OVERPLOT/TABLE {LUTTAB} ? :RED 5
   OVERPLOT/TABLE {LUTTAB} ? :GREEN 2
   OVERPLOT/TABLE {LUTTAB} ? :BLUE 3
ENDIF
