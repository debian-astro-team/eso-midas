! @(#)fibrand.prg	19.1 (ESO-DMD) 02/25/03 13:20:10
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure fibrand.prg
! K. Banse	961112
! execute via @a fibrand out naxis,npix1,npix2
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!
define/param p1 ? c "Enter result file name: "
define/param p2 2,256,256 n "Enter naxis,npix,...:"
! 
write/keyw out_a {p1}
write/keyw inputi {p2}
! 
outputi = m$secs()		!get seed of random generator
! 
if inputi .eq. 1 then
   inputi(3) = outputi(1)
else
   inputi(4) = outputi(1)
endif
! 
run APP_EXE:fibrand.exe
