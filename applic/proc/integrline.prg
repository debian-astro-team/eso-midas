! @(#)integrline.prg	19.1 (ES0-DMD) 02/25/03 13:20:13
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: integrline.prg
!.PURPOSE:        MIDAS command procedure INTEGR for INTEGRATE/LINE
!.NOTES:          Integrate area enclosed by a line (the input image) 
!                 interactively via cursor input, by interpolating 
!                 polynomially between cursor positions (currently up to 
!                 100 cursor-positions and 9th degree); or alternatively 
!                 by giving independent variable values as parameters, for 
!                 batch execution.
!.KEYWORDS:       Integration, 
!.LANGUAGE:       MIDAS command language.
!.SYNTAX:         INTEGRATE/LINE image line [pstart:pend] [cursors,degree]
!                                [B] [curs-posns]
!                 where: image = file name
!                        line  = line number
!                        pstart,pend = first and last point on the line to be
!           	      	         displayed (input as pixel numbers, prefixed 
!                                by @; or as real world coordinates); note 
!                                that the square brackets are obligatory.
!                        cursors = no. of cursor positions.
!                        degree  = degree of polynomial fit for the interpolation.
!                        B if batch rather than interactive use.
!                        curs-posns = cursor positions, followed by range 
!                                (for batch usage)
!.USAGE:         Interactive:
!                To implement, type any key except BAR or RETURN to validate a
!                cursor position.  When the specified number of cursor positions
!                are available, the interpolated line is drawn in the selected
!                line type. To visualize the modified line, strike BAR. At 
!                this step, you can continue to send new positions or you can 
!                exit by striking BAR again.
!                Batch:
!                Just give the required data for one feature on the 
!                line/spectrum.
!.AUTHOR:        F. Murtagh, ST-ECF, Garching.          14.03.1986
!.VERSION:       860616  RHW 
!.VERSION:       900731  RHW  Change name to integrline.prg
!.VERSION:       920413  KB
!.VERSION:       931109  RHW  Chane needed for new graphics system 
!----------------------------------------------------------------------------
DEFINE/PARAM P1 ? IMA "Enter image:"                      ! frame 
DEFINE/PARAM P2 @1                                        ! line number
DEFINE/PARAM P4 2,1 N
DEFINE/PARAM P5 I
DEFINE/PARAM P6 0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.
!
IF {PLRSTAT(1)} .NE. 0.0 .AND. {PLRSTAT(2)} .NE. 0.0 THEN
   DEFINE/PARAM P3 MANUAL C
ELSE
   DEFINE/PARAM P3 <,> C
   IF P3(1:1) .EQ. "M" THEN
      WRITE/OUT "*** FATAL: X axis not manually set; use SET/GRAPHICS"
      RETURN
   ENDIF
ENDIF
!
WRITE/KEYW  IN_A   {P1}                               ! frame
WRITE/KEYW  INPUTC {P3}                               ! start and end coord.
WRITE/KEYW  INPUTI/I/1/2 {P4}
!
IF INPUTI(1) .LE. INPUTI(2) THEN
   WRITE/OUT -
         "Degree must be strictly less than the number of cursor positions."
   RETURN
ENDIF
!
IF P5 .EQ. "B" THEN
   WRITE/KEYW BATCH/C/1/1  {P5}
   WRITE/KEYW CURS/R/1/12  {P6}
ELSE
   @ plscoff 0.,0.,-999,-999                         ! set defautl scaling
   WRITE/KEYW PLCDATA/C/1/60  {P1}                   ! name of data structure
   WRITE/KEYW PLCDATA/C/61/20 "FRAME       "         ! type of data structure
ENDIF
!
RUN APP_EXE:INTELINE

