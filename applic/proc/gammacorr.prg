! @(#)gammacorr.prg	19.1 (ESO-DMD) 02/25/03 13:20:11
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
!  MIDAS procedure gammacorr.prg to produce ITTs for gamma correction
!  K. Banse  970623
! 
!  use via @@ gammacorr itt_name [gamma]
!  with  itt_name = name of ITT, defaulted to gammacorr
!        gamma = gamma value of display
!  
!  an ITT is created according to the formula
!  itt = k ** (1.0/gamma)     (k=0,1,...,256)
!
!  If gamma is omitted a template file is loaded and the gamma value
!  can be determined interactively by clicking in sub-window where the 
!  right and left background match most closely. The gamma value printed
!  in that sub-window is then used as input to the procedure.
! 
!  if a graphics window does exist, the resulting ITT is plotted there
!  if the template file is loaded, the ITT is applied
!  
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/par p1 gammacorr ? "Enter name of resulting ITT: "
define/par p2 + n "Enter gamma of display:"
! 
define/local rr/r/1/1 0.0
! 
if p2(1:1) .eq. "+" then
    if mid$sess(6:6) .ne. "3" then
       create/disp 3 300,832,400,40 ? no
    endif
    load/image MID_TEST:gammacorr.fits
    load/lut ramp
    write/out "choose (with cursor) sub-window where right/left" -
               background match most closely
    get/cursor ? ? ? 1,1 >Null
    inputi = (outputr(11)-1)/64
    rr = 0.6 + 0.2*inputi
    set/format f3.1
    write/out selected gamma = {rr}
else
   rr = {p2}
endif
rr = 1./rr
! 
crea/ima &s 1,256 ? sequence 0.,1
comp/ima &b = &s**{rr}
! 
if mid$sess(7:7) .ne. " " .and. mid$sess(7:7) .ne. "-" then
   set/graph colo=1 ltype=1 stype=0 lwidth=1
   plot/axes 0,255 -0.1,1.1 ? ITT:{p1} Intensity
   set/graph colo=4                                   !use blue colour
   overplot &b  
endif
! 
copy/it &b middumma 
write/descr middumma.tbl history/c/1/80 "y = x**{rr}"
name/col middumma #1 :ITT
! 
$cp middumma.tbl {p1}.itt
if p2(1:1) .eq. "+" load/itt {p1}
