! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure scaler.prg
!  to scale images like when we load images into the image display
!  K. Banse  ESO	890502 - creation
!
!  use via   @a scaler inframe outframe scale_values [option]
!
!  inframe	  input frame
!  outframe	  output frame
!  scale_values	  integer scaling factors for x and y
!		  > 1    increase the size (by replication)
!		  < -1   decrease the size (by omission)
!  option         if set to A, averaging is done instead of omission for
!                 decreasing the size (i.e. if scale_values < 1);
!    defaulted to N (NoAveraging)
! 
!.VERSION
! 030728	last modif
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter input image:"
define/param p2 ? ima "Enter output image:"
define/param p3 1,1 n "Enter x,y scaling factors:"
define/param p4 N C "Enter average option (AV/No_AV): "
!
write/keyw in_a {p1}
write/keyw out_a {p2}
write/keyw inputi/i/1/2 {p3},{p3}
write/keyw history "@a scaler "
write/keyw action SC{p4}
! 
run MID_EXE:FLIP
