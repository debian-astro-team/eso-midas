! @(#)editfit.prg	19.1 (ES0-DMD) 02/25/03 13:20:09
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : TEDITTBL.PRG
!  TABLE Subsystem
!  J.D.Ponz                ESO - Garching   14 mar 84
!					    01 Aug 85  KB
!					    30 Oct 86  JDP
!
! .PURPOSE
!
!    implements
!
!  EDIT/TABLE  table-name column row
!
! ----------------------------------------------------------------------
!
!                             check if input file exists
DEFINE/PARAM P1 ? FIT "Enter name of fit file:"
WRITE/KEYW IN_B " " ALL
INPUTI = M$INDEX(P1,".")
IF INPUTI(1) .EQ. 0 THEN
  COMPUTE/KEYW IN_B = "'P1(1:>)'" // ".fit"
ELSE
  IN_B = "'P1'"
ENDIF
INPUTI = M$EXIST(IN_B)
!
WRITE/KEYW IN_A/C/1/60 " " ALL
WRITE/KEYW OUT_A/C/1/60 " " ALL
IN_A = "'P1'"
RUN APP_EXE:fittable
EDIT/TABLE 'OUT_A'
RUN APP_EXE:tablefit
