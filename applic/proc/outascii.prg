! @(#)outascii.prg	19.1 (ES0-DMD) 02/25/03 13:20:15
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure outascii.prg  to convert 1-dim Midas bdf file to ASCII file
! K. Banse	970623, 001020
! execute via OUTDISK/ASCII in_file out_file 
! where
!        in_file = name of input 1-dim Midas image 
!        out_file = name of output ASCII file 
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter name of input 1-dim MIDAS image: "
define/param p2 ? c "Enter name of output ASCII file; "
! 
define/local naxpix/i/1/1 1
define/local nn/i/1/4 0,0,0,0
define/local image/c/1/60 " " all
write/keyw image {p1}					!get cleaned image name
! 
nn(4) = m$value({image},NAXIS)
if nn(4) .gt. 1 then
   write/out 2-dim input image ... will be converted to 1-column ASCII file
   if nn(4) .gt. 3 nn(4) = 3				!cutoff at 3-dim
   define/local loop/i/1/1 0
   do loop = 1 nn(4)
      nn({loop}) = m$value({image},NPIX({loop}))
      naxpix = naxpix*nn({loop})
   enddo
   write/descr {image} npix/i/1/{nn(4)} 1,1,1
! 
else
   nn(4) = 1
   nn(1) = m$value({image},NPIX(1))
   naxpix = nn(1)
   {image},start(2) = m$value({image},START(1))		!duplicate START, STEP
   {image},step(2) = m$value({image},STEP(1))
endif
! 
! change 1-dim row to 1-dim column image
! 
{image},NAXIS = 2 
{image},NPIX(1) = 1 
{image},NPIX(2) = {naxpix} 
! 
! "print" data to ASCII file
! 
assign/print file {p2}
print/image {image} <,<,{naxpix} h
! 
! reset descriptors of Midas image
! 
{image},NAXIS = {nn(4)}
if nn(4) .gt. 1 then
   write/descr {image} npix/i/1/{nn(4)} {nn(1)},{nn(2)},{nn(3)}
else
   {image},NPIX(1) = {naxpix} 
   {image},NPIX(2) = 1 
endif
!
! reset to default = LPRINT:
assign/print LPRINT
