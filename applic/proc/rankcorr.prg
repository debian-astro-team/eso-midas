! @(#)rankcorr.prg	19.1 (ES0-DMD) 02/25/03 13:20:17


! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! PROCEDURE : RANKCORR.PRG
! M Peron                                   891111
!
! .PURPOSE
!
! execute the command :
! 
! ASSOCIATE/RANK table col1 col2 [method]
!
! ------------------------------------------------------------
!
DEFINE/PARAM P4 K C " Enter method K (Kendall) or S (Spearmann)"
WRITE/KEYW ACTION/C/1/1 'P4'
RUN APP_EXE:RANKCORR
