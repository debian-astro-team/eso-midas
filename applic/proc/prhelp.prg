! @(#)prhelp.prg	19.1 (ES0-DMD) 02/25/03 13:20:17
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure prhelp.prg to print the output of HELP/APPLIC
! K. Banse	911010
!
! use as @a prhelp param
! 
! where param is the same parameter as the one used for HELP/APPLIC
! 
! the output will be printed on the device which has been specified
! in the ASSIGN/PRINT command before 
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
LOG(3) = 1
@ print assign
HELP/APPL 'P1'
LOG(3) = 0
@ print out                          !send file to chosen printer
