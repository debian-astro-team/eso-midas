! @(#)catalsync.prg	19.1 (ES0-DMD) 02/25/03 13:20:06
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  Midas procedure  catalsync.prg  to synchronize catalogs
!
!  Klaus Banse		ESO - Garching
!  960223
! 
!  use as @a catalsync catalog_name
!
!  The descriptors of the images (tables) which are displayed with a
!  "read/icat catalog" or "read/tcat catalog" command are shown as they were
!  at creation time of the catalog.
!  Any change of such a descriptor after the catalog creation is not
!  reflected in the display of the catalog entries.
!  With `catalsync.prg' the catalog display is synchronized again with the
!  actual content of the relevant descriptors.
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ? "Enter name of catalog:"
!
define/local kfid/i/1/2 0,0
define/local frec/c/1/20
kfid = M$INDEX(p1,".cat")		!check for extension ".cat"
if kfid .gt. 0 then
   define/local catalnam/c/1/80 {p1}
else
   define/local catalnam/c/1/80 {p1}.cat
endif
!
open/file {catalnam} read kfid		!open catalog as ASCII file
if kfid(1) .eq. -1 then
   write/out problems opening catalog {catalnam} ...
   return
endif
read/file {kfid(1)} frec 10		!read 1. record to get type
close/file {kfid(1)}
if kfid(2) .lt. 3 then
   write/out problems reading 1. record of catalog {catalnam} ...
   return
endif
! 
if frec(3:3) .eq. "T" then				!table catalog
   create/tcat middummz Null >Null
   execute/catalog add/tcat middummz {catalnam} >Null
elseif frec(3:3) .eq. "F" then				!fitfile catalog
   create/fcat middummz Null >Null
   execute/catalog add/fcat middummz {catalnam} >Null
else							!all else image catalog
   create/icat middummz Null >Null
   execute/catalog add/icat middummz {catalnam} >Null
endif
! 
-rename middummz.cat {catalnam}			!overwrite original catalog
