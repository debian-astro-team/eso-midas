! @(#)keyout.prg	19.1 (ES0-DMD) 02/25/03 13:20:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       keyout.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Session Manager, Output Control
!.PURPOSE     Command OUTPUT/SESSION
!.VERSION     1.0    Creation    15-MAR-1994   PB
!
!-------------------------------------------------------
!
DEFINE/PARAM P1 Normal CHAR  "Mode (Silent, Normal, Verbose):"
!
SET/MIDAS OUTPUT=YES
!
   WRITE/KEYW  OUTMODE/C/1/10  {P1}

   WRITE/KEYW  OUTFILEN/C/1/20 " "  ALL
   WRITE/KEYW  OUTFILEV/C/1/20 Null
   WRITE/KEYW  OUTSYMBN/C/1/3  " "  ALL
   WRITE/KEYW  OUTSYMBV/C/1/3  ">>"

   IF P1(1:1) .EQ. "V"  THEN !  Level: Verbose
       WRITE/KEYW  OUTFILEV/C/1/20 " "  ALL
       WRITE/KEYW  OUTSYMBV/C/1/3  " "  ALL
   ENDIF

   IF P1(1:1) .EQ. "S" THEN !  Level: Silent
      WRITE/KEYW  OUTFILEN/C/1/20 "Null"
      WRITE/KEYW  OUTSYMBN/C/1/3  ">>"
   ENDIF

   WRITE/KEYW SESSOUTN/C/1/24 "{OUTSYMBN}{OUTFILEN} "
   WRITE/KEYW SESSOUTV/C/1/24 "{OUTSYMBV}{OUTFILEV} "

RETURN




