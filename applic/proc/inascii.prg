! @(#)inascii.prg	19.1 (ES0-DMD) 02/25/03 13:20:12
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure inascii.prg  to convert ASCII image to Midas bdf file
! K. Banse	950301
! execute via INDISK/ASCII in_file out_file [naxis,npix(1),npix(2),...]
! where
!        in_file = name of input ASCII file 
!        out_file = name of Midas image (max. 3 dimensions),
!                   defaulted to: toto.bdf
!        naxis,npix(1),npix(2) = optional naxis,npix string if number of
!                                pixels in x-, y-, ... direction is known
! 
!                    else a 1-dim image with the no. of pixels read from
!                    the input file is created; that is the default
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? c "Enter input name of ASCII data file; "
define/param p2 toto.bdf c "Enter name of output MIDAS image: "
define/param p3 + ? "Enter naxis,npix(1),npix(2),... if known: "
define/maxpar 3
! 
define/local naxpix/i/1/4 1 all
set/format i1
! 
if p3(1:1) .ne. "+" then	
   write/keyw naxpix {p3}
! 
   if naxpix(1) .eq. 3 then
      create/image {p2} 3,{naxpix(2)},{naxpix(3)},{naxpix(4)} ? ascii {p1}
      write/out "image frame:" {p2} -
      "created with NAXIS = 3 and NPIX = {naxpix(2)},{naxpix(3)},{naxpix(4)}"
   elseif naxpix(1) .eq. 2 then
      create/image {p2} 2,{naxpix(2)},{naxpix(3)} ? ascii {p1}
      write/out "image frame:" {p2} -
      "created with NAXIS = 2 and NPIX = {naxpix(2)},{naxpix(3)}"
   else
      create/image {p2} 1,{naxpix(2)} ? ascii {p1}
      write/out "image frame:" {p2} -
      "created with NAXIS = 1 and NPIX = {naxpix(2)}"
   endif
! 
else
   naxpix = 10
   create/image {p2} 1,{naxpix(1)} ? ascii {p1} >Null
   naxpix(2) = naxpix(1)-null			!difference in nopix in NULL(1)
   if naxpix(2) .ne. naxpix(1) then
      naxpix(1) = naxpix(2)
      create/image {p2} 1,{naxpix(1)} ? ascii {p1}
   endif
   write/out -
   "image frame:" {p2} "created with NAXIS = 1 and NPIX = {naxpix(1)}"
endif
