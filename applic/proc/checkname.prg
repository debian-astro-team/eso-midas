! @(#)checkname.prg	19.1 (ESO-DMD) 02/25/03 13:20:06
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure checkname.prg  to check a Midas file name for bad chars
! K. Banse	980707
! execute via @a checkname file_name disp_flag
! where
!        file_name is the name to be checked
!        disp_flag = Y(es) or N(o) for displaying message if name is bad
! 
!  keyword INPUTC is used internally, so previous contents of INPUTC will
!  be lost after a call of checkname.prg...!
!  
!  outputi(19) will be set on return:
!  outputi(19) = -1, if bad chars in name
!		  1, if name o.k.
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? c "Enter name:"
define/param p2 Y c "Enter disp_flag:"
! 
inputc = p1
@a indisk,checkname {p2(1:1)}c
if {q1} .eq. 0 then
   outputi(19) = 1
else
   outputi(19) = -1
endif 
