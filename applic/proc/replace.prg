! @(#)replace.prg	19.1 (ES0-DMD) 02/25/03 13:20:17
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure replace.prg  to use more elaborate expressions 
!                              for replacing image pixels than is possible
!                              with the REPLACE/IMAGE command
! K. Banse	931110
! execute via @a replace in out test/low,hi express1 [express2]
! where
!        `in', `out' `test/low,hi' have the same meaning as in REPLACE/IMAGE
! 
!        `express1' and `express2' are algebraic expressions like the ones
!        used in COMPUTE/IMAGE, e.g.
!            image1*15.3+(image2-sqrt(image3))*log10(image4)
!        is such an expression (No Blanks in the expression, please!)
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref IN OUT INTVAL EXP1 EXP2
!
!
define/param p1 ? c "Enter input file name: "
define/param p2 ? c "Enter output file name: "
define/param p3 N ? "Enter replacement interval: "
define/param p4 N ? "Enter 1. expression: "
define/param p5 + ? "Enter 2. expression: "
define/maxpar 5
! 
compute/image &r1 = {p4}
if p5(1:1) .ne. "+" then
   compute/image &r2 = {p5}
   replace/image {p1} {p2} {p3}=&r1,&r2
   delete/image &r2 no
else
   replace/image {p1} {p2} {p3}=&r1
endif
delete/image &r1 no

