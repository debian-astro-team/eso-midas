! @(#)snail.prg	19.1 (ES0-DMD) 02/25/03 13:20:19
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure snail.prg
!  K. Banse     920413
!
!  use as @a snail
!  to display a snail - indicating a command which takes quite some time
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
WRITE/OUT "                                        \/  _"
WRITE/OUT "                                         \_(_)__ ..."
