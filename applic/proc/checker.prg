! @(#)checker.prg	19.1 (ESO-DMD) 02/25/03 13:20:06
! +++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  checker.prg  to create a checkerboard pattern
! K. Banse      980107
!
! execute as: @a checker resima size
!
! with resima = name of created image
!      size = NAXIS,NPIX(1),NPIX(2),...
! the pixels of the result image will be set alternatively to -1.0 and +1.0
!
! +++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter name of image:"
define/param p2 2,32,32 no "Enter NAXIS,NPIX(1),.. :"
! 
create/image &w {p2} ? poly 0,180
compute/image &x = cos(&w)
transpose/image &x &w 
compute/pix {p1} = &w*&x
