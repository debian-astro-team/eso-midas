! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure mapitt.prg to map a LUT through a given ITT
! K. Banse      040929
!
! execute as @a mapitt ITT inputLUT mappedLUT
! 
! the mapped LUT is the input LUT convolved with the given ITT
! e.g. Midas> @a mapitt MID_SYSTAB:ramp.itt  MID_SYSTAB:heat.lut mine.lut
!
! 040929	last modif
!
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 MID_SYSTAB:ramp.itt C "Enter ITT:"
define/param p2 MID_SYSTAB:heat.lut C "Enter LUT:"
define/param p3 ? C "Enter result LUT (.lut will be appended automatically):"
define/maxpar 3
! 
define/local mydispi/i/1/1 0
do inputi = 1 10
   if winopen({inputi}) .eq. 0 then
      mydispi = inputi - 1
      goto id_ok 
   endif
enddo
write/out command failed - all possible display ids (0 - 9) are curently ued...
return/exit
! 
id_ok:
create/display {mydispi} 100,20,0,0 ? n,1
load/lut {p2}
load/itt {p1}
get/lut {p3} ? itt
delete/display {mydispi}
inputi = -4321
