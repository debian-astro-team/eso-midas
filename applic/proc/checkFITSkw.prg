! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure checkFITSkw.prg to check FITS header keywords
! K. Banse      091229
!
! use as   @a checkFITSkw FITSfile option
!    if option = 1
!       values of DATAMIN,DATAMAX are stored in keyword outputr(1), (2)
!    else if option = 2
!       value of BITPIX is stored in keyword outputr(3)
!    else nada...
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? c "Enter name of FITS file:" 
define/param p2 0 n "Enter option (1 for DATAMIN,DATAMAX):"

define/local fca/i/1/2 0 all 
define/local count/i/1/1 0 
define/local option/i/1/1 {p2}
 
outputr(1) = 99999.9
outputr(2) = -99999.9
outputr(3) = -99999.9
if option .lt. 1 .or. option .gt. 2 return

intape/fits 1 midd {p1} fnn >middumm.dat
open/file middumm.dat read fca
if fca(1) .lt. 0 return

count = 1

loop:
inputc = "   "
read/file {fca(1)} inputc 32
if fca(2) .eq. -1 goto eof
 
! count = count + 1
! if inputc(1:1) .eq. " " then
!    write/out {count}:
! else
!    write/out {count}: {inputc(1:8)}
! endif

if option .eq. 2 goto bitpix

if inputc(1:8) .eq. "DATAMIN " then
   outputr(1) = {inputc(10:32)}
else
   if inputc(1:8) .eq. "DATAMAX " outputr(2) = {inputc(10:32)}
endif
goto loop

bitpix:
if inputc(1:8) .eq. "BITPIX  " then
   outputr(3) = {inputc(10:32)}
endif
goto loop

eof:
close/file {fca(1)}

