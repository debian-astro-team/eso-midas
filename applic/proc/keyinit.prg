! @(#)keyinit.prg	19.1 (ES0-DMD) 02/25/03 13:20:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       keyinit.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Session, Manager, Initialisation
!.PURPOSE     Command INIT/SESSION 
!.VERSION     1.0    Creation    05-MAY-1992   PB
!
!-------------------------------------------------------
!
!
DEFINE/PARAM  P1   ?      CHAR    "Session name (CCD, SPEC, LONG, ECH) : "
DEFINE/PARAM  P2   OUT_A  CHAR    "Initialisation file : "
DEFINE/PARAM  P3   IN_A   CHAR    "Keywords definition file : "
DEFINE/PARAM  P4   IN_B   CHAR    "Keyword names file : "
!
! Default directories for Unix and VMS
DEFINE/LOCAL  DIRUNIX/C/1/40  "$STD_PROC"  ?   +lower 
DEFINE/LOCAL  DIRVMS/C/1/40   "STD_PROC"   ?   +lower
!
IF P1(1:1) .EQ. "C"   THEN   !  Session CCD
   WRITE/KEYW {P2}  ccdinit.prg
   WRITE/KEYW {P3}  ccdkey.all
   WRITE/KEYW {P4}  ccdkey.cat
ENDIF
!
IF P1(1:1) .EQ. "E"   THEN   !  Session  Echelle
   WRITE/KEYW {P2}  echinit.prg
   WRITE/KEYW {P3}  echkey.all
   WRITE/KEYW {P4}  echkey.cat
ENDIF
!
IF P1(1:1) .EQ. "L"   THEN   !  Session  Long
   WRITE/KEYW {P2}  lninit.prg
   WRITE/KEYW {P3}  longkey.all
   WRITE/KEYW {P4}  longkey.cat
ENDIF
!
IF P1(1:1) .EQ. "F"   THEN   !  Session  M.O.S. / FORS
   WRITE/KEYW {P2}  mosinit.prg
   WRITE/KEYW {P3}  moskey.all
   WRITE/KEYW {P4}  moskey.cat
ENDIF
!
IF P1(1:1) .EQ. "I"   THEN   !  Session  Irspec
   WRITE/KEYW {P2}  irsinit.prg
   WRITE/KEYW {P3}  irskey.all
   WRITE/KEYW {P4}  irskey.cat
ENDIF
!
IF P1(1:1) .EQ. "T" THEN     ! Session Time Analysis
   WRITE/KEYW {P2}  tsainit.prg
   WRITE/KEYW {P3}  tsakey.all
   WRITE/KEYW {P4}  tsakey.cat
   WRITE/KEYW DIRUNIX "$CON_PROC"
   WRITE/KEYW DIRVMS  "CON_PROC"
ENDIF
!
DEFINE/LOCAL   INIT/I/1/1  0
IF M$EXIST("{{P3}}") .EQ. 0  INIT = 1
IF M$EXIST("{{P4}}") .EQ. 0  INIT = 1
IF P8(1:1) .EQ. "I"          INIT = 2
!
IF INIT .GT. 0   @a keyinit,keyfile {{P2}}  {{P3}}  {{P4}}
IF INIT .EQ. 2   THEN
!
! Keywords initialisations
!
   WRITE/KEYW   SESSLINE/I/1/1          0        ! Current number of lines
   WRITE/KEYW   SESSNLIN/I/1/1          0        ! Number of lines on screen
   WRITE/KEYW   SESSDISP/C/1/3        "YES"      ! Display option: Stop at SESSNLIN or not
ENDIF
!
RETURN
!
ENTRY KEYFILE
!
! Reads file    P1  to :
! Create  file  P2 containing the definition of all keywords
! and     file  P3 containing the list of names of all keywords
!
IF AUX_MODE(1) .GT. 1 THEN
$grep -i "WRITE/KEY" {DIRUNIX}/{P1} | awk '{print $2}' > {P2}
$cat {P2} | awk '{FS = "/"} {print $1}' > {P3}
ELSE
$@APP_PROC:keyvms.prg  keyset {P2}  {DIRVMS}:{P1} {P3}
ENDIF
!
WRITE/OUT "Created file {P2} (list of session keywords)"
!
RETURN
