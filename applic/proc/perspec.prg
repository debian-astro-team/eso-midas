! @(#)perspec.prg	19.1 (ES0-DMD) 02/25/03 13:20:16
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure perspec.prg to provide perspective view
! K. Banse	920103, 920109, 940318
!
! execute as
! @a perspec inframe outframe rot_ang,tilt_ang plane_nos load_flag
!
! with rot_ang in [0,360] degrees and tilt_ang in [0,90] degrees:
!      0 degrees tilt means no tilt, i.e. normal view 
!      90 degrees tilt shows side of plane, i.e. single line closest to viewer
!      defaulted to 30.,60.
!
!      plane_nos = plane1,plane2,...,planez, e.g. 1,2,5
!      with each plane_no in [1,NPIX(3) of inframe]
!      or ALL, if all planes should be shown; defaulted to ALL
!
!      load_flag = Y(es) or N(o) for loading or not loading the resframe
!      defaulted to Yes
! 
!  the result frame does not contain the plotted lines, to redraw those
!  lines on top of the displayed result frame, use  @a perspec,draw
!
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ?     ima "Enter input frame: "
define/param p2 ?     ima "Enter output frame: "
define/param p3 30.,60. n "Enter rot_angle,tilt_angle in degrees: "
define/param p4 all   c   "Enter plane no's.: "
define/param p5 y   c   "Enter load_flag: "
!
write/keyw in_a 'p1'
write/keyw out_a 'p2'
write/keyw inputr 'p3'
write/keyw inputc 'p4'
!
run APP_EXE:perspec
write/descr {out_a} history/c/1/80 -
"created via: @a perspec {p1} {p2} {p3} {p4} "
! 
if p5(1:1) .eq. "Y" then
   load/image {out_a}
   @a perspec,draw
endif
!
entry draw				!the drawing is done here
!---------
! 
clear/chan over
!
draw/line {outputr(1)},{outputr(2)},{outputr(3)},{outputr(4)} f
draw/line {outputr(3)},{outputr(4)},{outputr(5)},{outputr(6)} f
draw/line {outputr(1)},{outputr(2)},{outputr(7)},{outputr(8)} f
draw/line {outputr(5)},{outputr(6)},{outputr(7)},{outputr(8)} f
!
define/local noplan/i/1/1 0
noplan = outputr(13)                    !get no. of planes
if noplan .lt. 2 return
! 
draw/line {outputr(1)},{outputr(2)},{outputr(1)},{outputr(9)} f
draw/line {outputr(3)},{outputr(4)},{outputr(3)},{outputr(10)} f
draw/line {outputr(5)},{outputr(6)},{outputr(5)},{outputr(11)} f
define/local loop/i/1/2 0,0
loop(2) = noplan-2
!
define/local rr/r/1/5 0.,0.,0.,0.,0.
define/local kl/i/1/2 0,0
do kl = 1 4
   kl(2) = kl(1)*2
   rr({kl}) = outputr({kl(2)})
enddo
rr(5) = rr(4) + outputr(15)
draw/line {outputr(7)},{rr(4)},{outputr(7)},{rr(5)} f
if loop(2) .le. 0 goto last_plane
!
do loop = 1 {loop(2)}
   do kl = 1 4
      rr({kl}) = rr({kl})+outputr(16)
   enddo
   draw/line {outputr(1)},{rr(1)},{outputr(3)},{rr(2)} f
   draw/line {outputr(3)},{rr(2)},{outputr(5)},{rr(3)} f
   draw/line {outputr(1)},{rr(1)},{outputr(7)},{rr(4)} f
   draw/line {outputr(5)},{rr(3)},{outputr(7)},{rr(4)} f
   rr(5) = rr(4) + outputr(15)
   draw/line {outputr(7)},{rr(4)},{outputr(7)},{rr(5)} f
enddo
!
last_plane:
draw/line {outputr(1)},{outputr(9)},{outputr(3)},{outputr(10)} f
draw/line {outputr(3)},{outputr(10)},{outputr(5)},{outputr(11)} f
draw/line {outputr(1)},{outputr(9)},{outputr(7)},{outputr(12)} f
draw/line {outputr(5)},{outputr(11)},{outputr(7)},{outputr(12)} f
