! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT  (c) 1996  European Southern Observatory
!.TYPE       Procedure
!.IDENT      tbl2bdf.prg
!.AUTHOR     P.Grosbol,   ESO/IPG
!.PURPOSE    Extract image file from a 3Dtable column
!.USAGE      TBL2BDF table column [row] [npix] [start,step] [prefix]
!.VERSION    1.0  1996-Apr-11 : Creation,  PJG
! ------------------------------------------------------------------
!
CROSSREF TABLE  COL  ROW  DIM  PFIX
DEFINE/PARA P1  ?               T  "Table name:"
DEFINE/PARA P2  ?               ?  "Column name:"
DEFINE/PARA P3  *               ?  "Row no.   :"
DEFINE/PARA P4  *               ?  "Npix :"
DEFINE/PARA P5  *               ?  "Start,step :"
DEFINE/PARA P6  image           ?  "Image prefix:"
!
WRITE/KEYW  IN_A/C/1/60     {P1}
WRITE/KEYW  INPUTC/C/1/60   {P2}
DEFINE/LOCA ROW/C/1/60      {P3}
DEFINE/LOCA DIM/C/1/60      {P4}
DEFINE/LOCA COOR/C/1/60     {P5}
DEFINE/LOCA PREFIX/C/1/60   {P6}
!
WRITE/KEYW  HISTORY/C/1/80  "TBL2BDF {p1} {p2} {p3} {p4} {p5} {p5}"
!
RUN MID_EXE:tbl2bdf

