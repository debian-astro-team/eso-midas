! @(#)convgrap.prg	19.1 (ES0-DMD) 02/25/03 13:20:07
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! . IDENTIFICATION
!
! MIDAS COMMAND PROCEDURE : CONVGRAP.PRG
!
! D. Baade, ST-ECF, Garching           version 1.0      851028
! K. Banse  860915, 920413
!
! .PURPOSE
!
! execute the command :
!
! CONVERT/GRAP outimage [mode]
!
! outimage:  output frame
! mode:      GCURSOR - create new table CONV1D, input from graphics cursor
!            ADD     - add new points to table CONV1D
!            DELETE  - delete points from table CONV1D
!
!-------------------------------------------------------------------------------
!
DEFINE/PARAM P1 ? IMA "Enter result image:"
DEFINE/PARAM P2 G 					! provide defaults
!
DEFINE/LOCAL FILE/C/1/8 REFIMA
!
IF P2(1:1) .EQ. "A"  GOTO ADD
IF P2(1:1) .EQ. "D"  GOTO DELETE
!
GCURSOR: 			! 1st case: start from scratch
!
! COPY/KK HPCURSOR/C/1/8 FILE/C/1/8
COPY/KK PLCURSOR/C/1/8 FILE/C/1/8
IF FILE(1:3) .EQ. "   " THEN
   WRITE/OUT "We need an image on the graphics screen ..."
   RETURN/EXIT
ENDIF
!
WRITE/OUT "                       >>> Use graphics cursor to enter data <<<"
GET/GCURS CONV1D
GOTO FIT
!
ADD: 				! 2nd case: add points to previoulsy created table FIT1D
!
CREATE/IMAGE REFIMA = {P1} NODATA
PLOT/ROW {P1}
OVERPLOT/TABLE CONV1D :X_AXIS :Y_AXIS
GET/GCURS CONV1D ADD
GOTO FIT
!
DELETE: 			! 3rd case: delete points from previously created table FIT1D
!
CREATE/IMAGE REFIMA = {P1} NODATA
PLOT/ROW {P1}
OVERPLOT/TABLE CONV1D :X_AXIS :Y_AXIS
IDENTIFY/GCURS CONV1D :X_AXIS :X_AXIS :Y_AXIS 3
!
FIT: 				! do the spline fit
!
SORT/TABLE CONV1D :X_AXIS
CONVERT/TABLE {P1} = CONV1D :X_AXIS :Y_AXIS {FILE} SPLINE
OVERPLOT/ROW {P1}
