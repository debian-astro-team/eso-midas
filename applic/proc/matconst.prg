! @(#)matconst.prg	19.1 (ES0-DMD) 02/25/03 13:20:15
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
! MIDAS procedure matconst.prg to define the following constants in
! double prec. keyword MATCONST/D/1/5
!
! Pi = 3.1415926535
! 2Pi = 6.2831853071
! Pi/180. =  0.0174532925
! e = 2.7182818284
! sqroot2 = 1.4142135623
!
! K. Banse	910620
! 
! use via:  @a matconst
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
WRITE/KEYW MATCONST/D/1/5 -
  3.1415926535,6.2831853071,0.0174532925,2.7182818284,1.4142135623
