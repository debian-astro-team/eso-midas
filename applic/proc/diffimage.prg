! @(#)diffimage.prg	19.1 (ESO-DMD) 02/25/03 13:20:08
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure diffimage.prg to compare images in the current directory
!                               with images (same name) in another directory
! K. Banse      020124
!
! use as   @a diffimage in_string other_dir
! where    in_string = single image file or image catalog (.cat)
!          other_dir = path of the other directory
!
!   if differences are found, Q1 = -1 is returned, else Q1 = 00
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? c "Enter image file or catalog name (with .cat):"
define/param p2 ? c "Enter path of other file list:"
! 
define/local catal/i/1/1 0
define/local kk/i/1/5 0 all
define/local dir/c/1/80 " " all
define/local retstat/c/1/2 "00"
define/local eps/r/1/1 0.00001
if aux_mode .ne. 1 then                 !not for VMS ...
   write/keyw dir {p2}/
endif
! 
kk(5) = m$filtyp(p1," ")
if kk(5) .ne. 9 then			!single image
   if m$ftset(p1) .eq. 0 then		!test, if filetype given
      write/keyw out_b {p1}.bdf
   else
      write/keyw out_b {p1}
   endif
   write/out image: {out_b}
   compute/image &d = {out_b} - "{dir}{out_b}"
   find/minmax &d >Null
   if m$abs(outputr(1)) .gt. eps .or. m$abs(outputr(2)) .gt. eps then
      write/out {out_b} and {dir}{out_b} differ ...
      retstat = "-1"
   endif
   return {retstat}
endif
! 
cat_loop:
store/frame out_b {p1} 1 finito
write/out image: {out_b}
compute/image &d = {out_b} - "{dir}{out_b}"
kk = kk + 1
! 
find/minmax &d >Null
if m$abs(outputr(1)) .gt. eps .or. m$abs(outputr(2)) .gt. eps then
   write/out {out_b} and {dir}{out_b} differ ...
   retstat = "-1"
endif
goto cat_loop
! 
finito:
set/format i1
write/out {kk} images processed
return {retstat}

