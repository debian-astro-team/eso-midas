! @(#)dids.prg	19.1 (ES0-DMD) 02/25/03 13:20:06
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
! DIDS.PRG
! J.D.PONZ
!
! .PURPOSE
!
! SIMILAR TO DIDS IN IHAP -- TEST VERSION
! IT WORKS ON ONE OF THE APERTURES ONLY
!
! @@ DIDS input output airmass flat flux
!
! +++++++++++++++++++++++++++++++++++++++++++++++++
COMPUTE    G51   = 'P1'_1-'P1'_3
PLOT G51
COMPUTE    G52   = G51/'P4'
PLOT G52
REBIN/WAVE G52 G53 'P5'
PLOT G53
COMPUTE    'P2'  = G53/'P5'
PLOT 'P2'
! COMPUTE    G54 = G53/'P5'
! EXTINCTION G54 'P2' 'P3' EXTAB
