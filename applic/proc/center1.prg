! @(#)center1.prg	19.1 (ES0-DMD) 02/25/03 13:20:06
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure CENTER1 for CENTER/INTERACTIV
! K. Banse	880727
!
! use via CENTER/INTER in_ima in_table out_table
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter input image: "
DEFINE/PARAM P2 ? TAB "Enter input table: "
DEFINE/PARAM P3 ? TAB "Enter output table: "
! 
WRITE/KEYW IN_A 'P1'			! in_frame
WRITE/KEYW IN_B 'P2'			! in_table
WRITE/KEYW OUT_A 'P3'			! out_table
RUN APP_EXE:POINFIN
