! @(#)keyshow.prg	19.1 (ES0-DMD) 02/25/03 13:20:14
! @(#)keyshow.prg	19.1  (ESO)  02/25/03  13:20:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       keyshow.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Session, Manager 
!.PURPOSE     Command SHOW/SESSION 
!.VERSION     1.0    Creation    10-SEP-1991 PB
!
!-------------------------------------------------------
!
define/local line/C/1/132  " "
line(1:132)  = MID$LINE(1:132)

define/local tlen/I/1/1   0
define/local tsta/I/1/1   0
define/local tend/I/1/1   0
define/local I/I/1/1      0

tsta = M$INDEX(line," ") + 1

define/local text/C/1/132  " "  ALL
if tsta .gt. 1   text(1:132) = line({tsta}:132)

I = 1
DOTS:
if text({I}:{I}) .eq. "."  then  
   text({I}:{I}) = " "
   I = I + 1
   GOTO DOTS
endif

define/local QUOT/C/1/1 "

IF text(1:1) .EQ. QUOT THEN
    write/out  {text}
ELSE
    write/out  "{text}"
ENDIF

IF SESSDISP(1:1) .NE. "Y"  RETURN

SESSNLIN = LOG(11) - 1
SESSLINE = SESSLINE + 1
IF SESSLINE .GE. SESSNLIN THEN
   SESSLINE = 0
   INQUIRE/KEYW  ANSW/C/1/3   "Hit return to continue, q + return to quit "
   IF AUX_MODE(7) .NE. 0 THEN
      IF ANSW(1:1) .EQ. "Q"    RETURN/EXIT
   ENDIF
ENDIF

RETURN

