! @(#)sharpen.prg	19.1 (ESO-DMD) 02/25/03 13:20:19
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure sharpen.prg  
!  to sharpen an image using Gauss filtered images as unsharp masks
! 
!  K. Banse     920401
!
!
!  use via  @@ sharpen in_ima out_ima [enh_fact]
!    in_ima	input image
!    out_ima	output image
!    enh_fact	enhancement factor applied to the difference of original
!		image and Gaussian mask, defaulted to 3.0
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter input image:"
define/param p2 ? ima "Enter result image:"
define/param p3 3.0 num "Enter enhancement factor:"
!
filter/gauss {p1} &sh1			!build unsharp mask
!
compute/image &sh2 = ({p1}-&sh1)*{p3}	!get (enhanced) difference
filter/gauss &sh2 &sh3 1,1
compute/image {p2} = &sh3+&sh1
cuts/image {p2} {p1}			!use original cuts

