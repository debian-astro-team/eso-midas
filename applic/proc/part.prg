! @(#)part.prg	19.1 (ES0-DMD) 02/25/03 13:20:16
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!
!   PART.PRG
!
!.AUTHOR
!
!   F. Murtagh, ST-ECF, Garching.      Version 1.0; 16 June 1986
!   J.D.Ponz, ESO< Garching                    1.1; 19 Sept 1986
!
!.PURPOSE
!
!   Execute PARTIF.EXE and PCACIF.EXE
!
!.KEYWORDS
!
!   Cluster Analysis, Partitioning, Iterative Optimisation,
!   Multivariate statistics, Pattern recognition.
!
!.CALLING SEQUENCE
!
! @@ PART  Input_table  Output_table  No._of_classes Analysis_option
!          Min._cardinality  Seed_value
!
!.INPUT/OUTPUT
!
!   Input table name.
!   Output table name.
!   Number of classes or groups.
!   Analysis option (1 = minimum distaces optimisation, 2 = exchange).
!   Minimum number of objects per class.
!   Seed value for random assignments to groups (used at beginning).
!
!-------------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter input table"
DEFINE/PARAM P2 ? TABLE "Enter output table"
DEFINE/PARAM P3 3 NUMBER "number of classes"
DEFINE/PARAM P4 1 NUMBER "analysis option"
DEFINE/PARAM P5 1 NUMBER "min no of objects per class"
DEFINE/PARAM P6 37519 NUMBER "seed value"
!
WRITE/KEYW INPUTI/I/1/1 'P3'
WRITE/KEYW INPUTI/I/2/1 'P4'
WRITE/KEYW INPUTI/I/3/1 'P5'
WRITE/KEYW INPUTI/I/4/1 'P6'
!
RUN APP_EXE:PARTIF
