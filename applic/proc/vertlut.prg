! @(#)vertlut.prg	19.1 (ESO-DMD) 02/25/03 13:20:21
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  vertlut.prg  to provide a vertical LUT at the left side
!				of the loaded image with tick marks + labels
! K. Banse	960531
!
! this procedure should be executed once an image is ready for hardcopy, 
! i.e. the image is loaded and labels and graphics in the overlay are done
! the next step is then the COPY/DISPLAY command
!
! execute via  @a vertlut LUTspecs tick_color no_ticks label_format lside
!  with LUTspecs = xsize,ysize,xoff,yoff of LUTbar;
! 		   defaulted to 20,ydisp-10,4,4
!	tick_color = string with the color for tick marks
!		     any of the Plot Colors (defaulted to white)
!	no_ticks = no. of tickmarks on LUT (defaulted to 6)
!	label_format = string with format used for writing intensities at 
!		       tick marks (defaulted to F4.2)
!	lside = right or left, if intensities are written to the right or
!		left of LUTbar (defaulted to right)
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local lutsiz/i/1/4 20,{ididev(3)},4,4
lutsiz(2) = lutsiz(2)-10
! 
define/param p1 20,{lutsiz(2)},4,4 N "Enter size+offset of LUTbar: "
define/param p2 white C "Enter color for drawing:"
define/param p3 6 N "Enter no. of tick-marks:" 
define/param p4 f4.2 C "Enter format of intensities at tick-marks:"
define/param p5 right C -
"Enter right or left, to indicate where tick-marks are drawn:"
define/maxpar 5
! 
define/local xypos/i/1/7 0 all
define/local ticki/i/1/3 0 all
define/local pix/r/1/5 0. all
pix(3) = {*,lhcuts(1)}			!low cut of loaded image
pix(4) = {*,lhcuts(2)}-pix(3)		!data interval of loaded image
pix(2) = pix(4)/(ididev(17)-1.)		!data_interval/LUTsize
! 
!  build vertical LUTbar
! 
write/keyw lutsiz {p1}
define/local fac/r/1/3 0.,{ididev(17)},{lutsiz(2)}
fac(1) = (fac(2)-1.)/(fac(3)-1.)
create/image MidVertLUT 2,{lutsiz(1)},{lutsiz(2)} ? poly 0.,0.,1
compute/image MidVertLUT = MidVertLUT*{fac}
! 
set/midas clover=no			!keep all drawings
!
!  load LUTbar at left size of display in OVERWRITE mode
! 
xypos(1) = lutsiz(3)
xypos(2) = lutsiz(4)
load/image MidVertLUT dirs=up,over fix=1,1,{xypos(1)},{xypos(2)}
xypos(3) = xypos(1)+lutsiz(1)
xypos(4) = xypos(2)+lutsiz(2)
draw/rect {xypos(1)},{xypos(2)},{xypos(3)},{xypos(4)} ? ? {p2} 
! 
! calculate offsets for tick marks
! 
if {p3} .lt. 2 then
   if {p3} .eq. 1 then
      set/format {p4}
      if p5(1:1) .eq. "R" then
         xypos(5) = xypos(3)+3
      else
         xypos(5) = xypos(1)-26
      endif
      label/disp {pix(3)} {xypos(2)},{xypos(5)} ? {p2}
   endif
   goto clean_up	
endif
! 
inputr = lutsiz(1)*0.25
ticki(1) = m$nint(inputr)
inputr = {p3}-1
! 
inputr(2) = lutsiz(2)/inputr
ticki(2) = m$nint(inputr(2))
ticki(3) = {p3}-2
! 
! draw tick marks (using keyword mode for speed)
! 
if p5(1:1) .eq. "R" then
   xypos(5) = xypos(3)-ticki(1)
   xypos(7) = xypos(3)
else
   xypos(5) = xypos(1)
   xypos(7) = xypos(1)+ticki(1)
endif
if ticki(3) .gt. 0 then
   xypos(6) = xypos(2)
   do inputi = 1 ticki(3)
      xypos(6) = xypos(6)+ticki(2)
      draw/line {xypos(5)},{xypos(6)},{xypos(7)},{xypos(6)} ? ? {p2}
   enddo
endif
! 
xypos(6) = xypos(2)
xypos(7) = xypos(6)
if p5(1:1) .eq. "R" then		!set x-offset for labels
   xypos(5) = xypos(3)+3
else
   xypos(5) = xypos(1)-26
endif
! 
! draw intensities at tick marks
! 
set/format {p4}
do inputi = 0 ticki(3)
   pix = pix(5)*pix(2)+pix(3)
   label/disp {pix} {xypos(7)},{xypos(5)} ? {p2}
   xypos(6) = xypos(6)+ticki(2)
   xypos(7) = xypos(6)-3
   pix(5) = ticki(2)*(inputi+1.)*fac
enddo
xypos(7) = xypos(4)-4
pix = (ididev(17)-1.)*pix(2)+pix(3)			!last label
label/disp {pix} {xypos(7)},{xypos(5)} ? {p2}
! 
clean_up:
if aux_mode .lt. 2 then				!VMS
   -delete MidVertLUT.bdf.*
else						!Unix
   -delete MidVertLUT.bdf
endif
