! @(#)modipix.prg	19.1 (ESO-DMD) 02/25/03 13:20:15
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure modipix.prg  to implement MODIFY/PIXEL, /AREA
! K. Banse	900619, 921008, 930405, 970408
! R. Hook       890622 - Noise flag option added to MODI/PIX
! S. Wolf	980923 - Add image section on which fits may be performed
!
! use via
! MODIFY/PIXEL CURSOR [result_frame] area_factors x_deg,y_deg,no_iter drawflag
!              noise
! MODIFY/PIXEL in_frame,table [result_frame] area_factors x_deg,y_deg,no_iter
! MODIFY/PIXEL disp_frame [result_frame] area_factors x_deg,y_deg,no_iter
!
! MODIFY/AREA CURSOR [result_frame] degree constant drawflag
! MODIFY/AREA in_frame,table [result_frame] degree constant drawflag
! MODIFY/AREA disp_frame [result_frame] degree constant drawflag
!
!  draw_flag = 1,2,3  (1,3 draw rectangle)
!                     (2,3 load cleaned area in ImageDisplay)
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/local kk/i/1/1 0
define/local qual/c/1/2 {mid$cmnd(11:12)}	!save qualifier
define/local imasec/c/1/100 " " ALL		!image section
! 
define/param p1 CURSOR C "Enter input_spec: "
define/param p2 + ima "Enter optional result frame: "
!
kk = m$index(p1,",")			!check for cursor option
if kk .le. 0 then
   if p1(1:6) .eq. "CURSOR" then	!currently displayed  frame
      define/param p8 * IMA
      write/keyw in_a {p8}
   else
      load {p1}				!load disp_frame first
   endif
   define/param p5 3 N "Enter drawflag: "
else
   define/param p5 0 N "Enter drawflag: "
endif
! 
if p2(1:1) .ne. "+" then		!check if result_frame is a number
   if m$tstno(p2) .eq. 1 then
      write/out Warning: it looks as if result_frame is missing...
   endif
endif
! 
write/keyw out_a {p2}
write/keyw history "{mid$cmnd(1:4)}/{mid$cmnd(11:13)} "
! 
if qual(1:1) .ne. "A" then			 !here for MODIFY/PIXEL
   define/param p3 2,2 N "Enter area_factors ax,ay: "
   define/param p4 2,2,5 N "Enter x_deg,y_deg,no_iter: "
   define/param p6 Y C "Add noise to replaced region (y/n)? "
   define/param p7 + C "Allowed image section to perform fits: "
!
   write/keyw inputr/r/1/2   {p3}
   write/keyw inputi/i/3/3   {p4}
   write/keyw inputi/i/6/1   {p5}
   write/keyw noise/c/1/1    {p6}
   write/keyw imasec {p7}
   action = "MP"
! 
else						!here for MODIFY/AREA
   define/param p3 2 N "Enter degree for fitting: "
   define/param p4 0. N "Enter constant (in case of degree 0): "
!
   write/keyw inputi/i/1/2  {p3},{p5}
   write/keyw inputr/r/1/1 {p4}
   action = "MA"
endif
!
run MID_EXE:MODIF
