! @(#)difftable.prg	19.1 (ESO-DMD) 02/25/03 13:20:08 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure difftable.prg to compare tables in the current directory
!				with tables (same name) in another directory
! K. Banse	020124
!
! use as   @a difftable in_string other_dir
! where    in_string = single table file or table catalog (.cat)
!          other_dir = path of the other directory
! 
!   if differences are found, Q1 = -1 is returned, else Q1 = 00
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? c "Enter table file or catalog name (with .cat):"
define/param p2 ? c "Enter path of other file list:"
! 
define/local catal/i/1/1 0
define/local dir/c/1/80 " " all
define/local kk/i/1/5 0 all +lower
define/local fca/i/1/2 0 all +lower
define/local fcb/i/1/2 0 all +lower
define/local retstat/c/1/2 "00"
if aux_mode .ne. 1 then			!not for VMS ...
   write/keyw dir {p2}/
endif
! 
kk(2) = m$filtyp(p1," ")
if kk(2) .ne. 9 then			!single table
   write/keyw in_a {p1}
   write/out table: {in_a}
   log/off				!avoid excessive logs
   read/table {in_a} >ta.dat
   read/table {dir}{in_a} >tb.dat
   log/on
   @a difftable,check ta.dat tb.dat
   if kk(5) .ne. 0 then
      set/format i1
      if kk(5) .gt. 0 then
         write/out {in_a}  and  {dir}{in_a} differ in line {kk(5)} ...
      else
         write/out {in_a}  and  {dir}{in_a} have not the same length ...
      endif
      retstat = "-1"
   endif
   return {retstat}
endif
! 
cat_loop:
store/frame in_a {p1} 1 finito
write/out table: {in_a}
kk = kk + 1
! 
log/off
read/table {in_a} >ta.dat
read/table {dir}{in_a} >tb.dat
log/on
@a difftable,check ta.dat tb.dat
if kk(5) .ne. 0 then
   set/format i1
   if kk(5) .gt. 0 then
      write/out {in_a}  and  {dir}{in_a} differ in line {kk(5)} ...
   else
      write/out {in_a}  and  {dir}{in_a} have not the same length ...
   endif
   retstat = "-1"
endif
goto cat_loop
! 
finito:
write/out {kk} tables processed
return {retstat}
! 
entry check
! 
! return 0  if files are equal
!        -n if files have different no. of records (one ends at <n> lines)
!         n if files differ in line <n>
! 
define/local count/i/1/1 0
! 
open/file {p1} read fca
open/file {p2} read fcb
! 
! skip the first line with table name
read/file {fca(1)} inputc
read/file {fcb(1)} outputc
count = count + 1
! 
loop:
write/keyw inputc " " all
write/keyw outputc " " all
read/file {fca(1)} inputc 80
read/file {fcb(1)} outputc 80
if fca(2) .eq. -1 then
   if fcb(2) .eq. -1 then
      kk(5) = 0
      goto eof
   else
      kk(5) = -count
      goto eof
   endif
endif
if fcb(2) .eq. -1 then
      kk(5) = -count
      goto eof
endif
! 
count = count + 1
if inputc(1:80) .eq. outputc(1:80) goto loop
kk(5) = count			!indicate difference in line {count}
! 
! close the files
! 
eof:
close/file {fca(1)}
close/file {fcb(1)}

