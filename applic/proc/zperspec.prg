! @(#)zperspec.prg	19.1 (ES0-DMD) 02/25/03 13:20:21
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure zperspec.prg to provide perspective view
! K. Banse	940103, 940318
!
! execute as
! @a zperspec inframe outframe rot_ang,tilt_ang scalsize,scallim load_flag
!
! with rot_ang in [0,360] degrees and tilt_ang in [0,90] degrees:
!      0 degrees tilt means no tilt, i.e. normal view 
!      90 degrees tilt shows side of plane, i.e. single line closest to viewer
!      defaulted to 30.,60.
!
!      scalsize,scallim  = no. of pixels foreseen for height, high cut for scale
!      defaulted to 50,Max  where Max is max. intensity of inframe
!
!      load_flag = Y(es) or N(o) for loading or not loading the resframe
!      defaulted to Yes
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ?     ima "Enter input frame: "
define/param p2 ?     ima "Enter output frame: "
define/param p3 30.,60. n "Enter rot_angle,tilt_angle in degrees: "
define/param p4 +   c   "Enter scaling size (pixels),scale-limit (intensity): "
define/param p5 y   c   "Enter load_flag: "
!
write/keyw in_a {p1}	
write/keyw out_a {p2}
write/keyw inputr 30.,60.,50,{{in_a},lhcuts(4)}
write/keyw inputr {p3}
if p4(1:1) .ne. "+" then
   write/keyw inputr/r/3/2 {p4}
endif
write/keyw inputc zperspec		!indicate where we come from...
!
run APP_EXE:perspec
write/descr {out_a} history/c/1/80 -
"created via: @a zperspec {p1} {p2} {p3} {p4} "
! 
if p5(1:1) .eq. "Y" then
   load/image {out_a}
endif
