! @(#)keydel.prg	19.1 (ESO-DMD) 02/25/03 13:20:13
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       keydel.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Applications, Keywords. 
!.PURPOSE     Delete Session Keywords
!.VERSION     1.0    Creation    23-DEC-1993  PB
! 
! 011030	last modif	KB
! 
!-------------------------------------------------------
!
define/local    table/C/1/80  " "  ?  +lower
! 
if p2(1:1) .ne. "?" then
   inquire/session {p2}   table   INPUTI
else
   inquire/session {mid$cmnd(11:14)}   table   INPUTI
endif
!
define/local cnt/i/1/1 0
!
do cnt = 1 inputi(1)
   if p1(1:1) .eq. "?" then
      delete/keyword  {{table},:key,@{cnt}}
   else
      if m$ftype(p1) .eq. 0 then
         delete/descr   {p1}.tbl  {{table},:key,@{cnt}}
      else
         delete/descr   {p1}  {{table},:key,@{cnt}}
      endif
   endif
enddo
!
delete/table {table} NO







