! @(#)deconv.prg	19.1 (ES0-DMD) 02/25/03 13:20:08
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  deconv.prg  to implement DECONVOLVE/IMAGE + CONVOLVE/IMA
! P. Grosbol, K. Banse	820722,851105
! KB			880916, 901212, 920401
!
! Use via   @a deconv inframe psf outframe no_iter cont_flag
!
! Deconvolution of an image with a Point Spread Function.
! The LUCY method is used. The iteration can be continued
! by setting cont_flag = C.
! For no_iter = 0, a convolution is done.
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input frame: "
define/param p2 ? IMA "Enter psf: "
define/param p3 ? IMA "Enter output frame: "
if mid$cmnd(1:1) .eq. "C" then
   inputi(1) = 0				!Iteration no = 0 ...
   write/keyw history "CONVOLVE/IMAGE "
else
   define/param p4 3 N "Enter no. of iterations: "
   write/keyw inputi/i/1/1 {p4}
   write/keyw history "DECONVOLVE/IMAGE "
endif
define/param p5 no ? "Enter continue_flag: "
define/maxpar 5
!
write/keyw in_a {p1}
write/keyw in_b {p2}
write/keyw out_a {p3}
!
if p5(1:1) .ne. "C" then
   inputi(2) = 0
else
   inputi(2) = 1
endif
!
run MID_EXE:DECONV
