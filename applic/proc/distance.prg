! @(#)distance.prg	19.1 (ES0-DMD) 02/25/03 13:20:09
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure DISTANCE to implement GET/DISTANCE
! K. Banse	880927
!
! use via GET/DISTANCE descr/D app_flag  [mark_flag] [max_no_inputs]
! or      GET/DISTANCE table   labl_flag [mark_flag] [max_no_inputs]
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 + C "Enter output option: "
DEFINE/PARAM P3 YY
DEFINE/PARAM P4 99999 N
!
WRITE/KEYW CURSOR 'P4'				!init CURSOR(1) to max...
RUN MID_EXE:DISTA
