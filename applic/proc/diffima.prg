! @(#)diffima.prg	19.1 (ESO-DMD) 02/25/03 13:20:08
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure diffima.prg to compare the pixels of two Midas images
! K. Banse	970718
!
! execute as @a diffima ima_a ima_b epsilon
!
! with ima_a, ima_b - Midas images
!      epsilon = max. allowd diff-value (defaulted to 0.0004)
! 
! if the image data are not equal, keyword OUTPUTI(10) = -1
!                                  else    OUTPUTI(10) = 0
! 
! the difference image is stored in middummd.bdf 
! even if we work in the FITS work environment!
! 
! 011113	last modif
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter frame1: "
define/param p2 ? ima "Enter frame2: "
define/param p3 0.0004 N "Enter epsilon: "
define/local rk/r/1/3 0.,0.,{p3}
!
! force creation of Midas format image file
compute/pixel &d.bdf = {p1}-{p2}
! 
statist/image &d.bdf >temp.data
rk(1) = m$abs(outputr(1))               !abs(minimum)
rk(2) = m$abs(outputr(2))               !abs(maximum)
if rk(1) .gt. rk(3) .or. rk(2) .gt. rk(3) then
   outputi(10) = -1
   $more temp.data
else
   outputi(10) = 0
endif

