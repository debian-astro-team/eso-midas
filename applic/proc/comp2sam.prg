! @(#)comp2sam.prg	19.1 (ES0-DMD) 02/25/03 13:20:07
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! PROCEDURE : COMP2SAM.PRG
! M Peron                                   891111
!
! .PURPOSE
!
! execute the command :
! 
! COMP/2SAM table col1 col2 [test]
!
! ------------------------------------------------------------
!
RUN APP_EXE:COMP2SAM.EXE
