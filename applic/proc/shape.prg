! @(#)shape.prg	19.1 (ES0-DMD) 02/25/03 13:20:19
! ++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! PROCEDURE : SHAPE.PRG
! B.Pirenne			version 1.0 861201
!
! .PURPOSE
!
! execute the command :
!  @@ SHAPE input_ref_frame  in_tab  out_frame
!
!
!
WRITE/OUT
WRITE/OUT SHAPE  build the image corresponding to the mirror interferogram
WRITE/OUT
WRITE/KEYW IN_A 'P1'
WRITE/KEYW IN_B 'P2'
WRITE/KEYW OUT_A  'P3'
RUN [BENOIT.INTERF.PGM]SHAPE
