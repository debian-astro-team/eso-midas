! @(#)showfit.prg	19.1 (ES0-DMD) 02/25/03 13:20:19
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : SHOWFIT.PRG
!  FIT commands
!
!  P.H.Defert, J.D.Ponz                ESO - Garching    17 FEB 86
!  920413  KB
!  920728  MP
! .PURPOSE
!   implements    SHOW/FIT     to display fitting keywords
! 
! ----------------------------------------------------------------------
!
WRITE/OUT 
IF FITCHAR(1:2) .EQ. "US" THEN
   WRITE/OUT "Current function with user definitions is"
ELSE
   WRITE/OUT "Current function with system definitions is"
ENDIF
WRITE/OUT {FITNAME}
! 
WRITE/OUT  
WRITE/OUT "METHOD   =   {FITCHAR(15:20)}"
WRITE/OUT "PRINT    =   {FITCHAR(21:24)}"
WRITE/OUT "WEIGHT   =   {FITCHAR(13:13)}"
WRITE/OUT "BOUNDS   =   {FITCHAR(14:14)}"
IF BNDTAB(1:1) .EQ. "+" THEN
   WRITE/OUT "BNDTAB   = "
ELSE
   WRITE/OUT "BNDTAB   =   {BNDTAB}"
ENDIF
