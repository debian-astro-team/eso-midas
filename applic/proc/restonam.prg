! @(#)restonam.prg	19.1 (ES0-DMD) 02/25/03 13:20:18
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  restonam.prg   for RESTORE/NAME
!
! K.Banse        920924, 931001, 950905, 990323
!
! use as RESTORE/NAME [file_spec] [verbose] [history] [overwrite] [descr]
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
crossref file_spec verbose history overwrite descr
!
define/param p1 + c "Enter catalog (file) name: "
define/param p2 YES c "Enter verbose flag, YES/NO: "
define/param p3 NO c "Enter history_update flag, YES/NO: "
define/param p4 NO c "Enter overwrite flag, CONFIRM/NO_CONFIRM: "
define/param p5 IDENT c "Enter descriptor used for name creation: "
define/maxpar 5
!
define/local filtyp/c/1/2 FX ? +lower		!catalog + file type
define/local kfid/i/1/3 0,0,0
define/local burro/c/1/60 " " all
define/local newname/c/1/60 " " all +lower
define/local counter/i/1/1 1 ? +lower
if aux_mode .lt. 2 then
   define/local dirend/c/1/1 "]" ? +lower
else
   define/local dirend/c/1/1 "/" ? +lower
endif
define/local done/c/1/1 N
if p2(1:1) .eq. "Y" then
   define/local wout/c/1/12 "write/out "
else
   define/local wout/c/1/12 "wait/secs 0 "
endif
! 
if p1(1:1) .eq. "+" then			!use active image catalog as
   if catalinf(6) .eq. 1 then			!default P1
      write/keyw p1 "{catalogs(1:{catalinf(11)})}  "
      write/keyw filtyp CI
      goto catalog_start
   else
      write/out "invalid default for parameter P1:" no active image catalog ...
      return
   endif
endif
! 
kfid(1) = m$index(p1,".cat")
if kfid(1) .gt. 1 then
   define/local frec/c/1/20
   open/file {p1} read kfid
   if kfid(1) .eq. -1 then
      write/out problems opening file {p1} ...
      return
   endif
   read/file {kfid(1)} frec 10
   close/file {kfid(1)}
   if frec(3:3) .eq. "I" then
      filtyp(1:2) = "CI"
      goto catalog_start
   elseif frec(3:3) .eq. "T" then
      filtyp(1:2) = "CT"
      goto catalog_start
   elseif frec(3:3) .eq. "F" then
      filtyp(1:2) = "CF"
      goto catalog_start
   endif
! 
else
   kfid(1) = m$index(p1,".")
   if kfid(1) .lt. 1 write/keyw p1 {p1}.bdf
! 
   kfid(1) = m$exist(p1)
   if kfid(1) .lt. 1 then
      {wout(1:12)} input file {p1} does not exist ...
      return
   endif
! 
   kfid(1) = m$existd(p1,"NAXIS")		!with type, test for image
   if kfid(1) .eq. 1 then
      write/keyw burro {p1}
      filtyp(1:2) = "FI"
      goto next_step
   endif   
   kfid(1) = m$existd(p1,"TBLENGTH")
   if kfid(1) .eq. 1 then			!or table
      write/keyw burro {p1}
      filtyp(1:2) = "FT"
      goto next_step
   endif   
   kfid(1) = m$existd(p1,"FITCHAR")		!or fit file
   if kfid(1) .eq. 1 then
      write/keyw burro {p1}
      filtyp(1:2) = "FF"
      goto next_step
   endif   
endif
! 
{wout(1:12)} unsupported file type ...
return
! 
! ----------------------------
! 
! handle catalogs
! 
! ----------------------------
! 
CATALOG_START:
define/local catal/i/1/1 0			!needed for catalog loop
define/local catnam/c/1/20 {p1}
! 
define/local same/i/1/1 0 
define/local ioff/i/1/3 0,0,0
if filtyp(2:2) .eq. "I" then
   {wout(1:12)} Image Catalog {catnam} used:
   if catalinf(6) .ne. 1 goto cat_loop
   ioff(1) = catalinf(1)
elseif filtyp(2:2) .eq. "T" then
   {wout(1:12)} Table Catalog {catnam} used:
   if catalinf(8) .ne. 1 goto cat_loop
   ioff(1) = catalinf(3)
else
   {wout(1:12)} Fit file Catalog {catnam} used:
   if catalinf(9) .ne. 1 goto cat_loop
   ioff(1) = catalinf(4)
endif
ioff(2) = ioff(1)+catalinf(11)-1		!point to end of catalog name
define/local actcat/c/1/{catalinf(11)} {catalogs({ioff(1)}:{ioff(2)})}
! 
if actcat(1:>) .eq. p1(1:>) then
   -copy {p1} middummz.cat
   write/keyw catnam "middummz.cat  "
   same = 1					!input is the active catalog
endif
! 
CAT_LOOP:
done = "N"
store/frame burro {catnam} ? end_cat
!
! here we handle a single file
!
NEXT_STEP:
info/descr {burro} filename
if mid$info(1) .lt. 0 then
   {wout(1:12)} "input file {burro} does not exist ... "
   goto go_on
else if mid$info(1) .eq. 0 then
   {wout(1:12)} "descriptor FILENAME missing - we use descr. {p5} instead ..."
   info/descr {burro} {p5} 
   if mid$info(1) .eq. 0 then
      {wout(1:12)} also descriptor {p5} missing ...
      goto go_on
   else if mid$info(1) .ne. 3 then
      {wout(1:12)} "descr. {p5} is not a character descriptor ..."
      goto go_on
   endif
   kfid(3) = 2
else
   kfid(3) = 1
endif
! 
if kfid(3) .eq. 1 then
   write/keyw newname "{{burro},FILENAME} "
else
   write/keyw newname "{{burro},{p5}(1:24)} "
   if newname(1:4) .eq. "    " then
      {wout(1:12)} descriptor {p5} empty ...
      goto go_on
   endif
endif
@a restonam,namconv 
if newname .eq. burro then
   {wout(1:12)} input + output names are the same ...
   goto go_on
endif
!
! now do the renaming, if successful, Q1 is set to YES
! 
if filtyp(2:2) .eq. "I" then
   rename/image {burro} {newname} {p3} {p4}
elseif filtyp(2:2) .eq. "T" then
   rename/table {burro} {newname} {p3} {p4}
elseif filtyp(2:2) .eq. "F" then
   rename/fit {burro} {newname} {p3} {p4}
endif
!
done = q1(1:1)
if done .eq. "Y" then
   {wout(1:12)} file {burro} renamed to {newname}
   out_a = newname
else
   out_a = "  "
endif
! 
GO_ON:
if done .eq. "N" then
   {wout(1:12)} "file {burro} skipped ..."
endif
if filtyp(1:1) .eq. "C" then
   goto cat_loop
else
   return
endif
! 
END_CAT:
if same .eq. 1 then
   if aux_mode(1) .lt. 2 then
      -delete middummz.cat.*
   else
      -delete middummz.cat
   endif
endif
! 
! ----------------------------------------------------
! 
! "subroutine" to clean up the names
! 
! ----------------------------------------------------
! 
entry namconv
! 
define/local klaus/i/1/2 0,0
! 
check_blanks:
klaus = m$index(newname," ")
if klaus .gt. 1 then
   newname({klaus}:{klaus}) = "_"
   klaus(2) = klaus(2)+1
   if klaus(2) .lt. 6 goto check_blanks		!max. 6 blanks replaced
endif
!
klaus = m$indexb(newname,dirend)+1
klaus(2) = m$index(newname({klaus}:>),".")
if klaus(2) .gt. 0 return
! 
if filtyp(2:2) .eq. "I" then
   write/keyw newname {newname}.bdf
elseif filtyp(2:2) .eq. "T" then
   write/keyw newname {newname}.tbl
elseif filtyp(2:2) .eq. "F" then
   write/keyw newname {newname}.fit
endif
