! @(#)keyreg.prg	19.1 (ESO-DMD) 02/25/03 13:20:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       keyreg.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Session Manager, Registration
!.PURPOSE     Command INIT/SESSION 
!.VERSION     1.0    Creation    15-JAN-1994   PB
! 
! 010215	last modif
!
!-------------------------------------------------------
!

DEFINE/PARAM P1 ? CHAR  "Session Name:"
DEFINE/PARAM P2 ? CHAR  "MIDAS Directory Name:"
DEFINE/PARAM P3 ? CHAR  "Initialisation File:"
DEFINE/PARAM P4 ? CHAR  "Output Table:"

write/keyw in_a  {P2}
write/keyw in_b  {P3}
write/keyw out_a {P4}

RUN APP_EXE:keyreg.exe

! Initialize visualisation keywords
WRITE/KEYW SESSLINE/I/1/1  0
WRITE/KEYW SESSNLIN/I/1/1  0
WRITE/KEYW SESSDISP/C/1/3  "YES"

!Initialise output control keywords
ECHO/SESSION

!Initialize session documentation keywords
WRITE/KEYW SESDIR{P1(1:2)}/C/1/80  {P2}
WRITE/KEYW SESFIL{P1(1:2)}/C/1/80  {P3}
WRITE/KEYW SESKEY{P1(1:2)}/C/1/80  {P4}





