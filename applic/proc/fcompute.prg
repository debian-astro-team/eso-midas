! @(#)fcompute.prg	19.1 (ES0-DMD) 02/25/03 13:20:09
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure fcompute.prg
!	to implement COMPUTE/FIT, /FUNC
!
! J.D.Ponz	850316
! K. Banse	870127, 900704, 920401
!
! execute via @@@ fcompute
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
IF MID$CMND(11:12) .EQ. "FI" THEN	!compute fit
   WRITE/KEYW HISTORY "{MID$CMND(1:6)}/{MID$CMND(11:14)} "
   IF FITCHAR(1:1) .EQ. "S" THEN
     RUN APP_EXE:FITCREA		! compute fitted image/table
   ELSE
     RUN FITCREA		
   ENDIF
ELSE 					!compute function
   WRITE/KEYW HISTORY "{MID$CMND(1:6)}/{MID$CMND(11:14)} "
   IF FITCHAR(1:1) .EQ. "S" THEN
     RUN APP_EXE:FUNCREA		! compute function as image/table
   ELSE
     RUN FUNCREA		
   ENDIF
ENDIF
