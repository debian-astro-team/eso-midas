! @(#)kstest.prg	19.1 (ES0-DMD) 02/25/03 13:20:14


! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! PROCEDURE : KSTEST.PRG
! M Peron                                   891111
!
! .PURPOSE
!
! execute the command :
! 
! KSTEST/1SAM table col1 distri parameters
!
! ------------------------------------------------------------
!
DEFINE/PARAM P3 UNIFORM C "Enter function type: "
DEFINE/PARAM P4 0.,1. N "Enter coefficients: "
WRITE/KEYW INPUTC/C/1/1 'P3'
WRITE/KEYW INPUTR/R/1/2 'P4'
RUN APP_EXE:TKSTEST.EXE
