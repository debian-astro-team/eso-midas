! @(#)tcluster.prg	19.1 (ES0-DMD) 02/25/03 13:20:20
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! STATISTICAL PACKAGE : TCLUSTER.PRG
! F.Murtagh, J.D.Ponz			version 1.0  22 JAN 85
!
! .PURPOSE
!
! execute the command :
!
! CLUSTER/TABLE input output [method]
!
! Default method  is minimum variance.
!
! -----------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter input table"
DEFINE/PARAM P2 ? TABLE "Enter output table"
DEFINE/PARAM P3 MVAR CHAR "Enter method"
RUN APP_EXE:TCLUSTER
