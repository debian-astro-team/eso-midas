! @(#)statlong.prg	19.1 (ES0-DMD) 02/25/03 13:20:20
!---------------------------------------------
! PROCEDURE STATLONG
! M. Pierre Dec. 1989
! Statistics on the residuals of table LINE
!----------------------------------------------
SET/GRAPH XAXIS YAXIS PMODE=1
!
DEFINE/PARAM P1 5. NUMBER "sigma level for line selection"
!
WRITE/KEYW WWW/R/1/1 0.
WRITE/OUT Histogramme of the residuals
WRITE/OUT
PLOT/HIST LINE :RESIDUAL
WRITE/OUT Statistics on residual absolute values
WRITE/OUT
COMP/TABLE  LINE :AR = ABS(:RESIDUAL)
STAT/TABLE LINE :AR
WRITE/OUT
COMP/KEYW WWW = OUTPUTR(2)*'P1'
SELECT/TABLE LINE :AR.GT.'WWW'
WRITE/OUT Plots lines with residual greater than 'P1'*sigma='WWW'
WRITE/OUT
PLOT/TABLE LINE :WAVEC :Y
WRITE/OUT  *************************************************
WRITE/OUT WARNING: Selection flags on table LINE are reset
SELECT/TABLE LINE ALL
