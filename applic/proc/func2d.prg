! @(#)func2d.prg	19.1 (ES0-DMD) 02/25/03 13:20:11
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  Midas procedure  func2d.prg  to build a 2-d function 
!  K. Banse  ESO-IPG	931108, 940517
!
!  use via   @a func2d name p2 p3 function
!  with
!       name = name of output frame
!       p2 = NAXIS,NPIX(1),NPIX(2),NPIX(3)
!       p3 = START(1),...,STEP(3)
!       function = formula of function, e.g. sin(x)*log10(y)
!
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter name of output frame: "
define/param p2 2,256,256 N "Enter Naxis,Npix(1),Npix(2),..,Npix(Naxis): "
define/param p3 + N "Enter Start(1),..,Step(1),..: "
define/param p4 ? c "Enter formula of function: "
! 
define/local kindx/i/1/3 0,0,0
kindx = m$exist("x.bdf")
if kindx .ne. 0 then
   kindx(2) = 1
   rename/image x x__func2d
endif
kindx = m$exist("y.bdf")
if kindx .ne. 0 then
   kindx(3) = 1
   rename/image y y__func2d
endif
if p3(1:1) .eq. "+" then
   create/image x {p2} ? poly 0.,1.0
   create/image y {p2} ? poly 0.,0.,1.0
else
   create/image x {p2} {p3} poly 0.,1.0
   create/image y {p2} {p3} poly 0.,0.,1.0
endif
! 
write/keyw p4 "{p4}{p5}{p6}{p7}{p8}"
kindx = m$index(p4,"?")
if kindx .gt. 0 then
   p4({kindx}:{kindx}) = " "
   compute/image {p1} = {p4(1:{kindx})}
else
   compute/image {p1} = {p4}
endif
! 
if kindx(2) .gt. 0 then
   rename/image x__func2d x
else
   if aux_mode .lt. 2 then
      $delete/noconf/nolog x.bdf.*
   else
      *rm -f x.bdf
   endif
endif
if kindx(3) .gt. 0 then
   rename/image y__func2d y
else
   if aux_mode .lt. 2 then
      $delete/noconf/nolog y.bdf.*
   else
      *rm -f y.bdf
   endif
endif
