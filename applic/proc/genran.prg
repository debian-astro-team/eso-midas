!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure genran.prg to implement CREATE/RANDOM_IMAGE
!  K. Banse	ESO - Garching	030707
!
!  execute via @a genran outfr ndim wcoo func_type coeffs seed
!  or          @a genran outfr = refima func_type coeffs seed
!
!  ndim = NAXIS,NPIX(1),NPIX(2),NPIX(3)
!  wcoo = START(1),...,STEP(3)
!  func_type = U(niform),U_opt       coeffs = lower,upper limits
!       U_opt = F(ibonnacci) for the lagged Fibonacci random generator
!       U_opt = S(tandard) for a standard normalized random generator
!                    G(aussian)      coeffs = mean,sigma
!  n.i.y.            E(xponential)
!  n.i.y.            L(ognormal)
!  n.i.y.            B(inomial)
!  n.i.y.            P(oisson)       coeffs = mean
!  n.i.y.            C(auchy)
!
!  seed = seed for the random generators
!  same seed yields same result in successive runs
!
!.VERSION
! 030714        last modif
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter result frame: "
write/keyw out_a {p1}
!
if p2(1:1) .ne. "=" then                ! dimensions given explicitly
   define/param p2 2,64,64 N  "Enter Naxis,Npix(1),Npix(2),... : "
   if {p2} .eq. 1 then
      define/param p3 0.,1. N "Enter Start(1),Step(1): "
   elseif {p2} .eq. 2 then
      define/param p3 0.,0.,1.,1. N "Enter Start(1),Start(2),Step(1),Step(2): "
   else
      define/param p3 0.,0.,0.,1.,1.,1. N -
      "Enter Start(1),Start(2),Start(3),Step(1),Step(2),Step(3): "
   endif
   define/local id/i/1/4 {p2}
   define/local rd/d/1/6 {p3}
   in_a = "+ "                          ! make sure, IN_A is set to "+"
else                                    ! REFIMA option
   define/param p3 ? ima "Enter reference frame: "
   write/keyw in_a {p3}
endif
!
outputi(1) = m$secs()/1237
!
define/param p4 UNIFORM C "Enter function type: "
define/param p5 0.,1. N "Enter coefficients: "
define/param p6 {outputi(1)} N "Enter seed value: "
!
write/keyw HISTORY "create/random "
define/local rcoeff/r/1/2 {p5}
define/local seed/i/1/1 {p6}
run MID_EXE:genran.exe

