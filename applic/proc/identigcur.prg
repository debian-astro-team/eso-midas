! @(#)identigcur.prg	19.1 (ES0-DMD) 02/25/03 13:20:12
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  identify.prg
!    IDENTIFY/CURSOR  table :ident :x [:y] [error]
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
DEFINE/PARAM P1 ? TABLE "Enter input table:"
DEFINE/PARAM P2 ? C     "Enter input for the identification column:"
DEFINE/PARAM P3 ? C     "Enter input for the abscissa column:"
!
RUN APP_EXE:IDENTIGCUR
