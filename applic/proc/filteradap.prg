! @(#)filteradap.prg	19.1 (ES0-DMD) 02/25/03 13:20:10
!.COPYRIGHT  (c)  1991 European Southern Observatory
!.IDENTIFICATION: filteradap.prg
!.PURPOSE:        MIDAS command procedure for FILTER/ADAPTIVE
!.SYNTAX:         FILTER/ADAPTIV frame outframe maskframe type shape 
!                                size k noise
!.AUTHOR:         G. Richter
!.VERSION:        910425 RHW implemented
!.VERSION:        910809 PJG correct name of executable, remove display
!-------------------------------------------------------------------------
DEFINE/PARAM P1 ? IMA " Enter input image: "
DEFINE/PARAM P2 ? IMA " Enter output image: "
DEFINE/PARAM P3 NULL IMA " Enter masking image [NULL to skip]: "
DEFINE/PARAM P4 S C   " Enter filter-type [S(moothing)/G(radient)/L(aplace)]: "
DEFINE/PARAM P5 P C   " Enter impulse-response shape [P(yramide)/B(ox)]: "
!
IF P5 .EQ. "P" THEN
   DEFINE/PARAM P6 ? N   " Max. filter-size [3/5/7/11/15/23/31/47/63/95/127]: "
!
ELSEIF P5 .EQ. "B" THEN
   DEFINE/PARAM P6 ? N   " Max. filter-size [3/5/9/17/33/65/129]: "
!
ELSE
   WRITE/OUT "*** INVALID SHAPE ***"
   RETURN
ENDIF
!
DEFINE/PARAM P7 ? N   " Enter threshold K [real]: "
DEFINE/PARAM P8 ? C   " Enter noise type [A(dditive)/P(oisson)]: "
!
WRITE/KEYW IN_A 'P1'
WRITE/KEYW OUT_A 'P2'
WRITE/KEYW IN_B 'P3'
WRITE/KEYW INPUTC/C/1/1 'P4'
WRITE/KEYW INPUTC/C/2/1 'P5'
WRITE/KEYW INPUTI/I/1/1 'P6'
WRITE/KEYW INPUTR/R/1/1 'P7'
WRITE/KEYW INPUTC/C/3/1 'P8'
!
WRITE/KEYW HISTORY "'MID$CMND(1:6)'/'MID$CMND(11:14)' "
RUN APP_EXE:filteradap
