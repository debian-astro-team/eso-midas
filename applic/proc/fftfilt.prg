! @(#)fftfilt.prg	19.1 (ES0-DMD) 02/25/03 13:20:09
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure fftfilt.prg
! K. Banse	860915,900703, 901212
!
! use via   @a fftfilt,a filter input fftr fftim imreal imimag
!           @a fftfilt,b filter       fftr fftim imreal imimag
!
! for mode = a, the FFT of the real input image 'input' is computed and
!     stored into 'fftr' and 'fftim'
! for mode b, we already start with 'fftr' and 'fftim'
!
! both modes will then multiply the real and imaginary part of the
! Fourier transform with the given filter and yield the final
! result via an inverse FFT
!
! filter is defaulted to blpf.bdf,
! fftr to fftr.bdf, fftim to ffti.bdf, imreal to nfftr.bdf, imimag to nffti.bdf,
! no default exists for input
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
CROSSREF FILTER MODE
!
ENTRY A
!
DEFINE/PARAM P1 blpf IMA
DEFINE/PARAM P2 ? IMA "Enter input frame:"
DEFINE/PARAM P3 fftr IMA
DEFINE/PARAM P4 ffti IMA
DEFINE/PARAM P5 nfftr IMA
DEFINE/PARAM P6 nffti IMA
!
FFT/FREQU 'P2' ? 'P3' 'P4'
COMP/PIX &a = 'P3'*'P1'
COMP/PIX &b = 'P4'*'P1'
!
FFT/FINV &a &b 'P5' 'P6'
!
ENTRY B
!
DEFINE/PARAM P1 blpf IMA
DEFINE/PARAM P2 fftr IMA
DEFINE/PARAM P3 ffti IMA
DEFINE/PARAM P4 nfftr IMA
DEFINE/PARAM P5 nffti IMA
!
COMP/PIX &a = 'P2'*'P1'
COMP/PIX &b = 'P3'*'P1'
!
FFT/FINV &a &b 'P4' 'P5'
