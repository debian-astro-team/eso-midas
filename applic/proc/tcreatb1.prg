! @(#)tcreatb1.prg	19.1 (ES0-DMD) 02/25/03 13:20:21
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!  MIDAS Command : tcreatb1.prg
!
!  J.D.Ponz 820405               ESO - Garching 
!  KB  910104
!
! .PURPOSE
!    implements
!  FIT/IMA	 [iter[,chisq[,relax]]] [image[,error] [fit_name]
!  FIT/TAB	 [iter[,chisq[,relax]]] [table] [depvar] [indvar] [fit_name]
!  READ/FIT	 [fit_name]
!  PRINT/FIT	 [fit_name]
!
! ----------------------------------------------------------------------
!
WRITE/KEYW HISTORY "'MID$CMND(1:4)'/'MID$CMND(11:14)' "
! 
IF MID$CMND(1:1) .EQ. "F" THEN
  IF FITCHAR(15:15) .EQ. "N" THEN
    DEFINE/PARAM P1 100,0.,-0.1 NUMBER
    WRITE/KEYW INPUTR/R/1/3 100,0.,-0.1
    WRITE/KEYW INPUTR/R/1/3 'P1'
  ELSE
    DEFINE/PARAM P1 999999.,999999.,999999.,999999.,999999.  NUMBER
    WRITE/KEYW INPUTR/R/1/5 999999.,999999.,999999.,999999.,999999.
    WRITE/KEYW INPUTR/R/1/5 'P1'
  ENDIF
  IF FITCHAR(1:1) .EQ. "S" THEN
    RUN APP_EXE:FITIMAG
  ELSE
    RUN FITIMAG
  ENDIF
! 
ELSE
  IF MID$CMND(1:1) .EQ. "P" @ print assign
  RUN APP_EXE:FITREAD
  IF MID$CMND(1:1) .EQ. "P" @ print out
ENDIF
