! @(#)autocuts.prg	19.1 (ESO-DMD) 02/25/03 13:20:05
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure autocuts.prg
!  K. Banse     920401
!
!  use as @a autocuts image [method]
!  with   image = name of image frame
!         method = MEDIAN or SIGMA, defaulted to SIGMA
!         SIGMA: use mean-3sigma,mean+3sigma as cuts
!         MEDIAN: do a 3x3 median filter on the image and use min,max of 
!                 filtered image as cuts
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter input image: "
define/param p2 SIGMA C "Enter method: "
! 
define/local loopi/i/1/1 0
define/local qq/r/1/8 0.0 all
! 
loopi = m$existd(p1,"STATISTIC")
if loopi .eq. 0 statist/image {p1}
! 
qq(7) = m$value({p1},statistic(1))			!copy min + max
qq(8) = m$value({p1},statistic(2))
! 
if p2(1:1) .eq. "S" then
   qq(5) = m$value({p1},statistic(3))		!get also mean + sigma
   qq(6) = m$value({p1},statistic(4))
   qq(3) = 3*qq(6)				!3 * sigma
   qq(1) = qq(5)-qq(3)
   qq(2) = qq(5)+qq(3)
else
   filter/median {p1} 9x9
   find/minmax 9x9
   qq(1) = outputr(1)
   qq(2) = outputr(2)
endif
! 
if qq(1) .lt. qq(7) qq(1) = qq(7)
if qq(2) .gt. qq(8) qq(2) = qq(8)
load/image {p1} cuts={qq(1)},{qq(2)}
delete/image 9x9 no
