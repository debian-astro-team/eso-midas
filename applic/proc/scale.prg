! @(#)scale.prg	19.1 (ES0-DMD) 02/25/03 13:20:18
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure scale.prg
! K. Banse	890705, 901212, 910205
!
! map pixel intensities of given frame into given interval
! like when loading an image
!
! use via   @a scale inframe outframe [lo_scale,hi_scale]
!
! where   inframe = name of input frame
! 	  outframe = name of output frame
!	  lo_scale,hi_scale = real values defining the mapping interval
!	                      defaulted to 0.0,255.0
!
! Example:
!	  @a scale centaur test 0.,350.5
!         map data of centaur.bdf into interval [0.,350.5] 
!         and store it in test.bdf
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? I "Enter input image: "
DEFINE/PARAM P2 ? I "Enter output image: "
DEFINE/PARAM P3 0.,255. N "Enter scaling interval: "
!
WRITE/KEYW IN_A 'P1'
WRITE/KEYW OUT_A 'P2'
WRITE/KEYW INPUTR 'P3'
!
ACTION(1:2) = "SC"
RUN MID_EXE:GENYY1
