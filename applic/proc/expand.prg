! @(#)expand.prg	19.1 (ES0-DMD) 02/25/03 13:20:09
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure expand.prg to expand a frame by folding over lines + columns
! K. Banse	920115
!
! execute as @a expand inframe outframe [xleft,xright] [ylow,yup]
!
! with xleft,xright - no. of expansion columns to left and right of image
!   or just xexpa   - if xexpa = xleft = xright
!   defaulted to 4    i.e. 4 pixels left and right
! and  ylow,yup     - no. of expansion lines below and above the image
!   or just yexpa   - if yexpa = ylow = yup
!   defaulted to 4    i.e. 4 lines below and above
!
! The frame is expanded by folding over the rows and columns at the border
! of the frame except the border rows + columns themselves;
! i.e. xleft + xright columns and ylow + yup rows are reflected at the border
! columns and rows.
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter input frame: "
define/param p2 ? ima "Enter output frame: "
define/param p3 4 n   "Enter xleft,xright: "
define/param p4 4 n   "Enter ylow,yup: "
!
write/keyw history "@a expand {p1} {p2} {p3} {p4} "
!
write/keyw in_a {p1}
write/keyw out_a {p2}
write/keyw inputi {p3},{p3}
write/keyw inputi/i/3/2 {p4},{p4}
!
action(1:1) = "E"
run MID_EXE:smooth
