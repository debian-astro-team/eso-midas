! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure showmidas.prg to display the current settings in MIDAS
! K. Banse      920102, 921007, 930107, 931029, 950331
! 
! 060508	last modif
!
! use as   @a showmidas
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
if mid$sess(15:15) .eq. "P" then
   write/out -
   "MIDAS version {mid$sess(16:20)} in parallel mode on" {mid$sys(1:20)}
else
   write/out -
   "MIDAS version {mid$sess(16:20)} in single user mode on" {mid$sys(1:20)}
endif
! 
inputc = m$symbol("MID_WORK")
write/out MIDAS root directory (MID_WORK) is: {inputc}
! 
write/out 
if mid$sess(11:11) .lt. "1" goto step_a
if mid$sess(12:12) .gt. "9" goto step_a
!
write/out "current display device for MIDAS:" {mid$disp(3:)}
! 
step_a:
!------
if aux_mode .lt. 2 then
   write/out -
   "current VMS procedure to print in MIDAS:" {mid$prnt(3:)}
   write/out -
   "current VMS procedure to plot in MIDAS:"  {mid$plot(3:19)}
else
   write/out -
   "current print device for MIDAS:" {mid$prnt(3:)}
   write/out -
   "current plot device for MIDAS:" {mid$plot(3:19)}
endif
write/out 
! 
if error(1) .eq. 0 then
   define/local messa/c/1/5 short
else
   define/local messa/c/1/5 full 
endif
if log(1) .eq. 0 then
   write/out logging disabled, {messa} error display enabled
else
   write/out logging enabled, {messa} error display enabled
endif
! 
! show all the set/midas_sys stuff
write/out " "
set/midas comm=? key=?
write/out " "
set/midas wcs-flag=?
if aux_mode .lt. 2 then
   set/midas  dq=? dsc=? clov=? outp=? user=? edit=? 
else
   set/midas  dq=? dsc=? clov=? outp=? user=? edit=? debug=?
endif
! 
show/context
