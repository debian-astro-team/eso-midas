! +++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure inoutFITS.prg to convert Midas images/tables in the
!					current directory from/to FITS
! K. Banse      040826
!
! execute as 
! @a inoutFITS direction type intype outtype
!
! with direction = IN or OUT (FITS -> Midas  or  Midas -> FITS)
!      type = IMAGE or TABLE or FITFILE
!      intype = file type of input files - with leading '.' (dot)
!      outtype = file type of output files - with leading '.' (dot)
!
! e.g. @a inoutFITS out t .tbl .tfits
! to convert all Midas tables *.tbl to corresponding FITS file *.tfits
! 
! 040826	last modif
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 IN c "Enter conversion direction (IN/OUT):"
define/param p2 IMA c "Enter frame type (IMA/TABLE):"
define/param p3 .fits c "Enter file type of input files:"
define/param p4 .bdf c "Enter file type of output files:"
define/maxpar 4
! 
if aux_mode(1) .eq. 1 then
   write/out Midas procedure inoutFITS not supported for VMS - bye, bye
   return/exit
endif
! 
$ ls *{p3} > in_in.cat
$ sed s/{p3}$/{p4}/ in_in.cat > out_out.cat	!exchange {p3} at end of lines
! 
if p1(1:1) .eq. "I" then
   indisk/fits in_in.cat out_out.cat
else
   outdisk/fits in_in.cat out_out.cat {p2}
endif

