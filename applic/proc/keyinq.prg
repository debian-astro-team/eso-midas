! @(#)keyinq.prg	19.1 (ESO-DMD) 02/25/03 13:20:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1994 European Southern Observatory
!.IDENT       keyinq.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Session Manager, Registration
!.PURPOSE     Command INIT/SESSION 
!.VERSION     1.0    Creation    15-JAN-1994   PB
!
! 020225		last modif (KB)
! 
!-------------------------------------------------------
!
!
define/param p1 ? char  "Session Name:"
define/param p2 ? char  "Char Keyword Name (for table name):"
define/param p3 ? char  "Integer Keyword (for length):"
!
define/local file/c/1/50  {seskey{p1(1:2)}}
!
if m$exist(file) .eq. 0 then
   register/session {p1(1:4)} {sesdir{p1(1:2)}} {sesfil{p1(1:2)}} {file}
endif
!
write/keyw {p2} {file}
{p3} = {{file},TBLCONTR(4)}	!before: COPY/DK {file} TBLCONTR/I/4/1 {P3}

