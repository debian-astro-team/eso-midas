! @(#)mapper.prg	19.1 (ES0-DMD) 02/25/03 13:20:15
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure mapper.prg to implement APPLY/MAP
! K. Banse	890704
!
! execute via APPLY/MAP outfile = infile mapfile flags
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter result frame:"
DEFINE/PARAM P3 ? IMA "Enter input frame:"
DEFINE/PARAM P4 ? IMA "Enter map frame:"
DEFINE/PARAM P5 0,0 N -
"Enter control flags for outside + equidistant  pixels:"
!
WRITE/KEYW IN_A 'P3' 
WRITE/KEYW IN_B 'P4' 
WRITE/KEYW OUT_A 'P1' 
WRITE/KEYW INPUTI 'P5'
!
ACTION(1:2) = "MP"
RUN MID_EXE:GENXX1
