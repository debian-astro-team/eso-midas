! @(#)keyhelp.prg	19.1 (ES0-DMD) 02/25/03 13:20:13
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       keyhelp.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Session, Manager, Help
!.PURPOSE     Provide help message on parameters
!.VERSION     1.0    Creation    21-AUG-1991   PB
!             2.0    New Session Manager 18-JAN-1994 PB
!
!-------------------------------------------------------
!
! define/param  P1  ?   CHAR   "Selection String:"
define/param  P2  KEY CHAR   "Mode:"
!
DEFINE/LOCAL    TYPE/I/1/1    0
define/local    sess/C/1/10   {MID$CMND(11:14)}
define/local    DES/I/1/1     0
define/local    line/C/1/80   " "

IF P1(1:1) .EQ. "?" THEN 
   SESSLINE = 0
   SHOW/{MID$CMND(11:14)} P8=HELP
   RETURN
ENDIF

inquire/session {sess} in_a  inputi	

if P2(1:1) .EQ. "K" THEN
  select/table {in_a} :KEY.EQ."~{P1}" {SESSOUTV}
else
  select/table {in_a}-
 :D1.EQ."~*{P1}*".OR.:D2.EQ."~*{P1}*".OR.:D3.EQ."~*{P1}*"-
 .OR.:D4.EQ."~*{P1}*" {SESSOUTV}
endif

if outputi(1) .eq. 0 .and. P2(1:1) .EQ. "K" then

 write/out "Warning: Could not match exactly keyword {P1}. Trying *{P1}*"
 select/table {in_a} :KEY.EQ."~*{P1}*"  {SESSOUTV}

endif

if outputi(1) .gt. 0 then

  copy/table {in_a} &a
  define/local cnt/I/1/1 0
  set/format I1 F20.10

  do cnt = 1 {middumma.tbl,TBLCONTR(4)}

   write/out "Keyword:        {&a,:KEY,@{CNT}}"
   write/out "Default Value:  " {&a,:DEFAULT,@{CNT}} " ; first element: " {{&a,:KEY,@{CNT}}}
   DO  DES = 1 4
     write/keyw line "{&a,:D{DES},@{CNT}}"
     if line(1:1) .ne. "~" then
        if DES .EQ. 1  then 
           write/out Description: {line}
        else
           write/out "             "{line}
        endif
     endif
   ENDDO
   write/out         "Type:         {&a,:TYPE,@{CNT}}"
   write/out "All elements:"
   read/keyw  {&a,:KEY,@{CNT}} h
   write/out "    "

  enddo	

else

  write/out "Help: Could not find string *{P1}*"

endif

select/table {in_a} all



RETURN


