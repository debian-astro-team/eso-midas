! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  magni.prg  for MAGNITUDE/CENTER, /RECTANGLE, /CIRCLE
! K. Banse	900730, 920721, 940224, 971112, 101216
!
! use via MAGNITUDE/method
! 	  in_specs out_specs pix_specs out_option CENT_FLAG curs_opt zw_option
!
!  in_specs    = CURSOR  or  inframe  or   inframe,xpix,ypix
!		         or  inframe,intable
!  out_specs   = ?      just display data
!	       = table  if output to table (may be same as intable or new one)
!	       = descr,D   descriptor name (of inframe)
!  pix_spec    = size of magnitude area (x)
!		 size of magnitude area (y)	<- optional for magni/rect
!                size of no_mans_land (one half)
!                size of background area (one half)
!  out_option  = A, append flag for table or descriptor
!                I, write :IDENT column for table
!                AI, if append + ident for tables
!  cent_params   = centflag,kappa
!		   centflag: 1 - use centered window, 0 - use unchanged window
!  		   kappa: kappa for sky kappa-sigma clipping (default =2.0)
!  curs_option = no_curs,drawflg1,drawflg2,max_input
!		 no_curs = 1 or 2 for 1 or 2 cursors
!		 drawflg1 = 0: do not draw the circles/rectangles in overlay
! 		            1: yes, do it
!		 drawflg2 = 0: no label with number in image channel
! 		            1: yes, do it
! 		 max_input = max. no. of cursor inputs
!  zw_option = zoom_window_flag,zoom;
!                zoom_window_flag = W for zoom_window, N for none,
!                zoom = initial zoom factor;
! 
! OUTPUTR(13) < 0.0 if something went wrong in the calculations
!             > 0.0 if all o.k.
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/para p1 CURSOR C "Enter input specs: "
define/para p2 + C "Enter output specs: "
define/para p3 + C "Enter flux, no_mans_land, background size: "
define/para p4 + C "Enter out_options: "
!
if p3(1:2) .eq. "+ " then
   define/local area/c/1/80 "@13,@2,@2 "
else
   define/local area/c/1/80 "{p3} "
endif
if mid$cmnd(11:12) .eq. "CE" then
   action(1:5) = "CENXX"
elseif mid$cmnd(11:12) .eq. "CI" then
   action(1:5) = "CIRXX"
else
   action(1:5) = "RECXX"		!the default
endif
!
if p1(1:6) .eq. "CURSOR" then          !interactive input via cursor
   define/local coption/i/1/4 1,1,0,9999
   define/para p5 0,2.0 N "Enter center_flag, 1 or 0, and kappa: "
   define/para p6 1,1,0,9999 N "Enter no_curs,drawflg1,drawflg2,max_input: "
   define/para p7 + ? "Enter zoom_window_flag,zoom: "
! 
   write/keyw coption/i/1/4 {p6}		!so we can only update no_curs
   write/keyw in_a * 
!
   dazin(1) = -1
   if p7(1:1) .eq. "W" then
      if p7(2:2) .eq. "," then
         dazin(1) = {p7(3:)}
      else
         dazin(1) = 4                              !default to 4*zoom
      endif
      in_b(1:1) = "?"                              !force to default Xstation
   endif
   define/maxpar 7
!
else					!inframe,table or just inframe
   define/para p5 +,2.0 C "Enter center_flag, 1 or 0, and kappa: "
   define/maxpar 5
endif
!
run MID_EXE:MAGNI
! if outputr(13) .lt. 0. then
!    write/out control flag => problems with MAGNITUDE/{action(1:3)} command...
! endif
action = "XXXXXX"                       !clean up ...
