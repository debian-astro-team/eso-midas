! @(#)normalize.prg	19.1 (ES0-DMD) 02/25/03 13:20:15
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure normalize.prg
! to normalize an image frame (i.e. map it to interval [0,1])
! K. Banse	920305, 940517
!
! execute as   @a normalize input [result]
! where
! input - name of input image frame
! result - optional name of normalized output image
!
! Note: If the name of an output frame is not given, normalization is done
!       in place, i.e. the input frame itself is changed...
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ima "Enter name of input image: "
define/param p2 + ima "Enter name of result image: "
! 
! make sure, frame exists
inputi = m$existd(p1,"naxis")
if inputi .ne. 1 then
   write/out "frame {p1} not found..."
   return/exit
endif
! 
define/local myname/c/1/60 {p1}
define/local oldi/i/1/1 {log(4)}
! 
log(4) = 1			!no display
if p2(1:1) .ne. "+" then
   copy/ii {p1} {p2}
   write/keyw myname {p2}
endif
! 
find/minmax {myname}
compute/image {myname} = {myname} - {outputr(1)}
find/minmax {myname}
compute/image {myname} = {myname}/{outputr(2)}
log(4) = {oldi}
