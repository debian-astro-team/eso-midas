! @(#)sortrow.prg	19.1 (ES0-DMD) 02/25/03 13:20:20
! Procedure: sortrow.prg
! Author   : P. Ballester   -   ESO Garching
! Date     : 910506
!
! Syntax   :
!
!    SORT/ROW  input_frame   output_frame
!
! Purpose  :
!
!    Row by row sorting of pixels of a frame 
!
define/param P1  ?   I "Input frame :"
define/param P2  ?   I "Output frame:"

write/keyw in_a/C/1/60   {P1}
write/keyw in_b/C/1/60   {P2}

RUN  APP_EXE:sortrow

COPY/DD  {P1}   LHCUTS    {P2}   LHCUTS

RETURN

