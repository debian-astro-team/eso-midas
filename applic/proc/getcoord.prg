! @(#)getcoord.prg	19.2 (ES0-DMD) 03/31/03 16:56:50
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure getcoord.prg to convert frame pixels to world coords
!                              and RA, DEC to world coords
! K. Banse	960124, 030318
!
! use as   @a getcoord name coord_string
!     or   @a getcoord name table in_columns from-to out_columns
! where    name = name of image frame
!          coord_string = string with frame pixels: @xpix,@ypix,..
!			  => world coords. are calculated and displayed
!                         or string with world coords: x,y,...
!			  => frame pixels are calculated and displayed
!	   table = name of table file
!	   in_columns = labels of input columns (separated by a comma)
!	   from-to = pix/world/radec for from or to, e.g.:
!		     wo-ra  means conversion from dec. world coords (degrees)
!			    to RA and DEC
!	   out_columns = labels of output columns (separated by a comma)
! 
!   world coords in dec. form are stored in OUTPUTD(1-3), if non-linear WCS
!   the RA, DEC wc in hr:min:sec are stored in OUTPUTD(4-9) as hr,min,sec,...
!   frame pixels are stored in OUTPUTD(10-12)
!   if image `name' is currently loaded into the display window, also the
!   corresponding screen coords are displayed and stored in OUTPUTI(1,...)
! 
! examples:	@a getcoord wcstest.mt 02:42:55.41,018:22:07.66
!		@a getcoord wcstest.mt coords.tbl :ra,:dec ra-wo :xdeg,:ydeg
! 
! if you don't need the screen pixels, 
! use            CONVERT/COORDS image coord_string
! instead of     @a getcoord image coord_string
! it's much faster (executed inside the monitor)
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter image name:"
define/param p2 ?  c  "Enter coordinate pair in usual MIDAS syntax:"
!
write/keyw in_a {p1}
write/keyw inputc {p2}
write/keyw action/c/1/3 WCO
! 
run MID_EXE:idauxx.exe
