! @(#)addpars.prg	19.1 (ESO-DMD) 02/25/03 13:20:04
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  MIDAS procedure addpars.prg 
!  Klaus Banse			960712
! 
!  use as   @a addpars file_name par1 ... par7
!  where      file_name = name of ASCII file
!             par1 = 1. parameter to append
!             ...
!             par7 = last param to append
! 
!  so up to a maximum of 7 parameters can be added to the contents of
!  each record of `file_name'
! 
!  this procedure is useful if you want to execute a Midas command on a whole
!  list of frames
! 
!  Example:  Midas > $ls *.bdf >lola.dat
!            Midas > @a addpars lola.dat BINSIZE=512
!            Midas > statist/image <lola.dat
! 
!  will do the statistics on each frame with a 512 bin histogram
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? ? "Enter name of ASCII file:"
! 
define/local ifct/i/1/2 0,0
define/local ofct/i/1/2 0,0
open/file {p1} read ifct			!open input file
if ifct(1) .lt. 0 then
   write/out Could not open file: {p1}
   return/exit
endif
open/file add_#_{p1} write ofct			!open temp. output file
if ofct(1) .lt. 0 then
   write/out Could not open file: add_#_{p1}
   return/exit
endif
! 
if pcount .lt. 2 then
   write/out Parameters missing...
   return/exit
endif
! 
define/local params/c/1/200 {p2}		!merge par1 ... par7 -> params
if pcount .gt. 2 then
   define/local loo/i/1/1 0
   set/format i1
   do loo = 3 pcount
      write/keyw params "{params} {p{loo}}"
   enddo
endif
! 
define/local record/c/1/200 " " all
! 
read_loop:					!read until EOF
write/keyw record/c/1/200 " " all
read/file {ifct(1)} record
! 
if ifct(2) .gt. 0 then
   ifct(2) = ifct(2)+2
   write/keyw record/c/{ifct(2)}/100 "{params}"	!append par1 ... par7
   write/file {ofct(1)},key record
   goto read_loop
endif
! 
close/file {ifct(1)}
close/file {ofct(1)}
-rename add_#_{p1} {p1}				!rename temp. file to input f.
