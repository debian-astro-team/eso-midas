! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! The following application procedures exist :
!
! addpars           add parameters to each record of ASCII file
!
! asload            assign display window + load an image into it
!
! areatable         build a table with areas of equal size covering an image
!
! autocuts          determine `good' cuts for loading of an image
!
! buildcube         build a cube from a set of 2-dim frames
!
! catalsync         synchronize catalog entries
!
! checker           create a checkerboard pattern
!
! checkname         check name of Midas file for illegal characters
!
! clean             clean bad pixels by replacing them with min, max, mean
!		    or median of good pixels
!
! compFITS          compare primary headers of FITS files
! 
! compdelta         compute difference image
! 
! convolfft         convolve/correlate two images via FFT
!
! copyfile          copy an ASCII files omitting specific records (lines)
!
! diffdsc           compare the descriptors (FITS header) of two Midas images
! 
! diffima           compare the pixels of two Midas images
! 
! diffimage         compare images in different directories
! 
! difftable         compare tables in different directories
! 
! dscedit           edit a descriptor
!
! edges             find edges using Sobel or Prewitt filter
!
! expand            expand an image by folding over of rows and columns at the
!                   borders
! 
! fonttxt           show text in any chosen font and browse font data base
! 
! fastcomp          compute fast ln, log10 or sqrt functions
!
! func2d            create image according to any 2-dim function
!
! fftfilt           filter an image in the Fourier domain
!
! gammacorr         create ITT to apply gamma correction for "good" hardcopy
!
! getcoord          translate frame pixels to world coords and vice versa
!
! gcurve            create light curve of object
!
! histogram         image to image histogram transformation
!
! inoutFITS         tool to convert all files of given type from/to FITS 
!		    keeping same name 
!
! mapitt            map a LUT via a given ITT to get the convolved LUT
!
! matconst          define some often used mathematical constants
!
! normalize         map pixel values into [0.,1.]
!
! outima            convert Midas images to other formats (e.g. GIF)
!
! packdsc           pack descriptors of a Midas file (reclaim space from
!                   previously deleted descriptors)
!
! perspec           build + display 3-dim perspective view of cube
!
! polyrep           mark polygon in overlay channel and replace image
!                   according to edges of that polygon
!
! plottab           plot contents of a colour LookUpTable or ITT
!
! prhelp            print this help
!
! replace           use more elaborate expressions for replacing pixels than 
!                   is possible in REPLACE/IMAGE
! 
! resetback         reset the connection with a background Midas
! 
! rgb               create a jpeg file from 3 Midas images which serve as 
!                   red, green, and blue components
! 
! sancheck          check the sanity of an image frame, i.e. test if there are
!                   any NaN or Inf in the data
! 
! scale             scale image data into given range
!
! scaler            scale images (pixels) the same way as in
!                   LOAD/IMAGE but on disk
!
! scanima           reconstruct a 1-dim image from a 2-dim frame representing
!                   the plot of that image
!
! screendump        screen dump of displayed image, saved in an image-file
!
! send_mess         display message(s) (centered) on display
!
! sharpen           sharpen an image via Gaussian unsharp masking
!
! showfonts         show the fonts which can be used in Midas (LABEL/DISPLAY)
!
! showmidas         display current setup of MIDAS
!
! slicube           extract a 2-D slice from a 3-D cube
!
! swap4             swap the 4 quadrants of an image diagonally
!
! thumbs            get a thunmbnail view of images
!
! vertlut           integrate vertical LUT with tick marks into display
!
! zperspec          build + display 3-dim perspective view of 2-dim frame
!
! Click on its name or use "HELP/APPLIC procedure" to get more detailed info
! about a procedure.
!                                                        090210  KB
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
