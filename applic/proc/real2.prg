! @(#)real2.prg	19.1 (ES0-DMD) 02/25/03 13:20:17
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : REAL2.PRG
!  TABLE Subsystem
!
!  M.C. Maccarone	IFCAI/CNR - Palermo	May 1988
!  updated for Portable Midas   900703  K. Banse, ESO - IPG
!
! .IMPLEMENTED COMMANDS
!
!	INTSECT/TABLE tab1 tab2 outtab flag_ext
!	TJOIN/TABLE tab1 tab2 outtab condition
!
! ----------------------------------------------------------------------    
! 
BRANCH MID$CMND(1:4) INTS,TJOI INT,TJO
!  
WRITE/ERROR 5
RETURN
!
!Intersection command INTSECT/TABLE
!
INT:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 ? TABLE
DEFINE/PARAM P3 ? TABLE
DEFINE/PARAM P4 YES CHAR
IF "'P1(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Intersection on the same table !
 WRITE/OUT INTSECT/TABLE command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P1(1:>)'" THEN
 WRITE/OUT Please change name of the output table !
 WRITE/OUT INTSECT/TABLE command not performed !
 RETURN
ENDIF
IF "'P3(1:>)'" .EQ. "'P2(1:>)'" THEN
 WRITE/OUT Please change name of the output table !
 WRITE/OUT INTSECT/TABLE command not performed !
 RETURN
ENDIF
! 
DEFINE/LOCAL FIRST/C/1/60 'P1'
DEFINE/LOCAL SECOND/C/1/60 'P2'
DEFINE/LOCAL MYOUT/C/1/60 'P3'
DEFINE/LOCAL FLEXT/C/1/3 'P4'
DIFFE/TABLE 'FIRST' 'SECOND' dummy 'FLEXT'
DEFINE/LOCAL IR/I/1/1 0
IR = M$EXIST("dummy.tbl")
IF IR .EQ. 0	RETURN
COPY/DK dummy.tbl TBLCONTR/I/4/1 IR
IF IR .EQ. 0 THEN
 WRITE/OUT No problems - INTSECT command is yet running
 MERGE/TABLE dummy 'FIRST' 'MYOUT'
 COMSS/TABLE 'MYOUT'
ELSE
 DIFFE/TABLE 'FIRST' dummy 'MYOUT' 'FLEXT'
ENDIF
DELETE/TABLE dummy NO
RETURN
!
! Teta-join  command  TJOIN/TABLE
!
TJO:
DEFINE/PARAM P1 ? TABLE
DEFINE/PARAM P2 ? TABLE
DEFINE/PARAM P3 ? TABLE
DEFINE/PARAM P4 #1.EQ.#2 ?
DEFINE/LOCAL FIRST/C/1/60 'P1'
DEFINE/LOCAL SECOND/C/1/60 'P2'
DEFINE/LOCAL MYOUT/C/1/60 'P3'
DEFINE/LOCAL CONDIT/C/1/60 'P4'
IF AUX_MODE .LT. 2 THEN
   -DELETE dummy.tbl.*		!VAX/VMS
ELSE
   -DELETE dummy.tbl		!Unix
ENDIF
PRODUCT/TABLE 'FIRST' 'SECOND' dummy NO
DEFINE/LOCAL IR/I/1/1 0
IR = M$EXIST("dummy.tbl")
IF IR .EQ. 0	RETURN
SELECT/TABLE dummy 'CONDIT'
COPY/TABLE dummy 'MYOUT'
IF AUX_MODE .LT. 2 THEN
   -DELETE dummy.tbl.*		!VAX/VMS
ELSE
   -DELETE dummy.tbl		!Unix
ENDIF
RETURN
!
