! @(#)comput.prg	19.1 (ESO-DMD) 02/25/03 13:20:07
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!  MIDAS command procedure    comput.prg 
!  version 1.0   860201  D. Baade             ESO Garching
!  KB	890214, 901009, 901108, 920319, 930323, 980713
!  PJG  920302
!
!.KEYWORDS
!  Julian date, conversion from local mean sidereal time to universal time
!
!.PURPOSE
!  convert from local mean ST to UT, calculate Julian date
!
!.COMMENT
!  Input has been changed so that eastern longitude is required
!  following IAU standards.          PJG 920902
!
!.ALGORITHM
!  all done in FORTRAN program COMPXY which is executed by this MIDAS procedure
!
!.INPUT/OUTPUT
!  the following keywords are used:
!
!  a) if input is only via keywords
!  P1                          date: year,month,day
!  P2                          sidereal time: hour,min,sec
!  P3                          eastern longitude of observatory: degree,min,sec
!  OUTPUTR/R/1/3               universal time: hour,min,sec
!  OUTPUTD/D/1/1               Julian date
!
!  b) if dates and times are read from table
!  P1                          name of table
!  P2                          eastern longitude of observatory: degree,min,sec
!  table :YEAR :MONTH :DAY     date
!  table :STHR :STMIN :STSEC   sidereal time
!  table :UTHR :UTMIN :UTSEC   will receive universal time in real hours
!  table :JD                   will receive Julian date
!
!  c) if dates and times are read from descriptors of image
!  P1                          name of image
!  P2                          eastern longitude of observatory: degree,min,sec
!  image,O_TIME(1)             year
!  image,O_TIME(2)             month
!  image,O_TIME(3)             day
!  image,O_TIME(6)             sidereal time in real hours
!  image,O_TIME(4)             will receive Julian date
!  image,O_TIME(5)             will receive universal time in real hours
!
!------------------------------------------------------------------------------
!
crossref DATE ST LONGITUDE
!
inputc(1:3) = "+++"			!for sign of longitude etc.
define/local rneg/r/1/1 1.0
!
define/param p1 ? ? "Enter date (year,month,day) or name of image or table:"
!
if m$tstno(p1) .ne. 0 goto keys		!check, if frame or number(s)
! 
define/param p1 + ima
write/keyw in_a {p1}
if m$filtyp(in_a,".bdf") .eq. 2 goto table
!
!  input is read from descriptors of image {p1}
!
define/param p2 -70,43,55.35 N -
"Enter (eastern) longitude of observatory (degree,min,sec): "
!
write/keyw inputr/r/1/10 0,0,0,0,0,0,{p2},0,0    ! partly reset keyword
rneg = (inputr(7)*3600.)+(inputr(8)*60.)+inputr(9)
inputc(1:1) = p2(1:1)
if rneg .lt. 0.0 inputc(1:1) = "-"
!
inputr(1) = m$value({in_a},o_time(1))	  	! year
if inputr(1) .le. 1.0 .or. inputr(1) .lt. 1901.0 then
   write/out " Warning: year of the observation: {inputr} really correct? ..."
endif
inputr(2) = m$value({in_a},o_time(2))		! month
inputr(3) = m$value({in_a},o_time(3))		! day
inputr(4) = m$value({in_a},o_time(6))		! ST in real hours
!
action(1:2) = "SU"			!indicate ST -> UT
run MID_EXE:COMPXY
!
outputd(20) = outputd(1)-2400000.500000		! JD - 2,400,000.5
outputd(19) = outputr(1)+(outputr(2)+outputr(3)/60.)/60. !UT in real hour
!
{in_a},o_time(4) = outputd(20)		! JD - 2,400,000.5
{in_a},o_time(5) = outputd(19)		! UT in real hours
!
return					! finished with image option
!
KEYS:
!
! ... input only via keywords
!
define/param p2 ? N "Enter ST of observation (hour,min,sec): "
define/param p3 -70,43,55.35 N -
"Enter (eastern) longitude of observatory (degree,min,sec): "
!
write/keyw inputr/r/1/3 {p1},0,0
write/keyw inputr/r/4/3 {p2},0,0
write/keyw inputr/r/7/3 {p3},0,0
rneg = (inputr(7)*3600.)+(inputr(8)*60.)+inputr(9)
inputc(1:1) = p2(1:1)
if rneg .lt. 0.0 inputc(1:1) = "-"
!
action(1:2) = "SU"			!indicate ST -> UT
run MID_EXE:COMPXY
!
set/format i2 f10.3,g24.12
write/out "        Julian date:       {outputd(1)}"
outputi(1) = outputr(1)
outputi(2) = outputr(2)
write/out "        Universal time:    {outputi(1)} : {outputi(2)} :-
 {outputr(3)}"
!
return 					! finished with keyword option
!
TABLE:
!
! ... input mainly from table
!
define/param p2 -70,43,55.35 N -
"Enter (eastern) longitude of observatory (degree,min,sec): "
!
define/local nline/I/1/1 0		! number of lines in table
define/local lp/i/1/2 0,0
!
write/keyw inputr/r/7/3 {p2},0,0       	! eastern longitude
rneg = (inputr(7)*3600.)+(inputr(8)*60.)+inputr(9)
inputc(1:1) = p2(1:1)
if rneg .lt. 0.0 inputc(1:1) = "-"
!
! ... create table columns which shall receive results
!
set/midas output=loginly	!no error messages if columns exist already
create/column {in_a} :UTHR 			!UT hours
create/column {in_a} :UTMIN 			!UT minutes
create/column {in_a} :UTSEC 			!UT seconds
if m$existc("{in_a}","JD") .ne. -1 delete/column {in_a} :JD
create/column {in_a} :JD G24.12 R*8 		!Julian date
set/midas output=yes				!reset to default
!
nline = m$value({in_a},tblcontr(4))  	!get number of entries in table
!
do lp = 1 {nline}                 	!loop over all lines in table
   copy/tk {in_a} :YEAR :MONTH :DAY @{lp} inputr/r/1/3     !date
   copy/tk {in_a} :STHR :STMIN :STSEC @{lp} inputr/r/4/3   !ST
!
   action(1:2) = "SU"			!indicate ST -> UT
   run MID_EXE:COMPXY
!
   copy/kt outputr/r/1/3 {in_a} :UTHR :UTMIN :UTSEC @{lp}  !UT
   {in_a},:jd,{lp} = outputd(1)                          !Julian date
enddo					!table done
! 
write/out {nline} rows of table {in_a}.tbl processed.
