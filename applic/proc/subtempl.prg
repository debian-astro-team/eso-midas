! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure subtempl.prg to implement SUBTRACT/TEMPLATE
! D. Baade	900220
!
! use as   SUBTRACT/TEMPLATE in out template coords scale
! 
! shift and resample a 2-dim template pattern, scale it in flux, and
! subtract it from the input frame
!
! 070629	last modif
! 
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? IMA "Enter input image:"
define/param p2 ? ima "Enter result image:"
define/param p3 ? ima "Enter template image:"
define/param p4 ? N "Coordinates in x,y:"
define/param p5 + N "Scaling factor:"
!
define/local inimag/c/1/80 {p1}		!input ima
define/local outimag/c/1/80 {p2}	!output ima
define/local psf/c/1/80 {p3}		!template ima
define/local spara/r/1/7 {p4},0,0,0,0,0	!parameters
define/local newsta/r/1/2 0.,0.
newsta(1) = m$value({psf},start(1))
newsta(2) = m$value({psf},start(2))
define/local newstp/r/1/2 1.,1.
newstp(1) = m$value({psf},step(1))
newstp(2) = m$value({psf},step(2))
!
spara(4) = spara(1)-0.5			!subtract 0.5 because of nearest int
spara(5) = spara(2)-0.5			!dito
spara(4) = m$nint(spara(4))		!int part of x-coord
spara(5) = m$nint(spara(5))		!int part of y-coord
spara(6) = 1.0-(spara(1)-spara(4))	!fractional part of shift in x
spara(7) = 1.0-(spara(2)-spara(5))	!fractional part of shift in y
spara(4) = spara(4)+newsta(1)+1.	!x-start of shifted template
spara(5) = spara(5)+newsta(2)+1.	!y-start of shifted template
!
if p5 .eq. "+" then
   copy/ii {psf} &a 
else
   compute/image &a = {psf} * {p5}	!scale template in flux
endif
!
! use REBIN/LINEAR to accomplish shift by fraction of pixel, the values
! in spara(6,7). 
! the integer part of the shift is contained in (but not equal to) spara(4,5)
! which are the new start coords of the template copy
! 
rebin/linear &a &b -
  {newstp(1)},{newstp(2)} {spara(6)},{spara(7)} {spara(4)},{spara(5)}
! 
! assuming that the template will usually be much smaller than the
! input image to be decontaminated, we compute the difference in the
! supposedly small overlapping region and insert the result into the
! result frame
! 
compute/image &c = {inimag} - &b
copy/ii {inimag} {outimag}
insert/image &c {outimag}
! 
find/minmax {outimag} >Null

