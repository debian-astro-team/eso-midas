! @(#)tuttilut.prg	19.1 (ES0-DMD) 02/25/03 13:20:21
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  tutlut.prg 
! this is an example of how to use a menu in MIDAS
! 
! use as @ tutlut display_id action_flag
! where
!        display_id = id of display used for menu 
!                     has to exist already unless we use action_flag = C !!
!        action_flag = C, for creating also the menu
! 
!  K. Banse     910306
!  ESO/IPG - Garching
!
!
!  for this example we assume, that there is already a display 0 created !
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
define/param p1 9 n "Enter display_id for menu: "
define/param p2 c ? "Enter action_flag: "
! 
define/local myframe/c/1/60 "?" all
define/local koko/i/1/32 0 all
define/local logo/i/1/5 0,0,0,0,0
define/local getstr/c/1/80 " " all	!needed to read from Midas comm. window
define/local prompt/c/1/1 Y
define/local mydisp/i/1/1 'dazdevr(10)'
define/local lolo/i/1/3 0,0,0
! 
if mydisp .lt. 0 crea/disp 
logo(1) = log(1)
logo(4) = log(4)
! 
! ------------------------------------------------------------- 
!  to create a menu we first define the menu labels
!  and then create the menu itself 
!  finally create also the menu_display
! ------------------------------------------------------------- 
if p2(1:1) .eq. "C" then 	!first we have to set up the labels...
   crea/image MID_TEST:tutlut 2,512,100
!
   write/descr MID_TEST:tutlut labels1/c/1/80 -
   "staircase~color~random~pseudo1~rainbow~light~heat~backgr~~ "
   write/descr MID_TEST:tutlut labels2/c/1/80 -
   "standard~ikon~smooth~idl2~idl4~idl5~idl6~Exit~~ "
! 
   lolo(3) = 1
   do lolo = 0 448 64
      lolo(2) = lolo(1)+10
      write/keyw koko/i/'lolo(3)'/2 20,'lolo(2)'
      lolo(3) = lolo(3)+2
   enddo
   copy/kd koko MID_TEST:tutlut offset1/i/1/16
!
   lolo(3) = 1
   do lolo = 0 448 64
      lolo(2) = lolo(1)+10
      write/keyw koko/i/'lolo(3)'/2 70,'lolo(2)'
      lolo(3) = lolo(3)+2
   enddo
   copy/kd koko MID_TEST:tutlut offset2/i/1/16
else
   assign/display d,'p1'
endif
! 
crea/disp 'p1' 512,100,300,0 1,512,100 N
disp/lut off
clear/chan over
load MID_TEST:tutlut cuts=0.,2.
draw/line 0,49,511,49 s ? white
do lolo = 0 448 64
   draw/line 'lolo',0,'lolo',99 s ? white
enddo
!
copy/dk MID_TEST:tutlut offset1 koko
copy/dk MID_TEST:tutlut labels1 inputc
label/disp inputc koko o ? ? key
copy/dk MID_TEST:tutlut offset2 koko
copy/dk MID_TEST:tutlut labels2 inputc
label/disp inputc koko o ? ? key
! 
! ------------------------------------------------------------- 
!  here is the loop where we point with the mouse into a field 
!  of the menu and choose a function
! ------------------------------------------------------------- 
menuloop:
assign/display d,'p1'
! 
once_more:
if prompt(1:1) .eq. "Y" then
   prompt = "N"
   write/out "Choose desired LUT with the cursor."
   write/out "Press enter for loading the LUT, press exit for plotting it."
endif
log(1) = 0			!avoid logging + display
log(4) = 2
get/cursor ? ? nn -1,1		!we only want keyword CURSOR filled...
log(1) = logo(1)
log(4) = logo(4)
! 
assign/disp d,'mydisp'
   if cursor(1) .lt. 64 then
      @ tuttilut,loadlut staircase standard 
   elseif cursor(1) .lt. 128 then
      @ tuttilut,loadlut  color  ikon
   elseif cursor(1) .lt. 192 then
      @ tuttilut,loadlut random smooth
   elseif cursor(1) .lt. 256 then
      @ tuttilut,loadlut pseudo idl2
   elseif cursor(1) .lt. 320 then
      @ tuttilut,loadlut rainbow idl4
   elseif cursor(1) .lt. 384 then
      @ tuttilut,loadlut light idl5
   elseif cursor(1) .lt. 448 then
      @ tuttilut,loadlut heat idl6
   else
      if cursor(2) .lt. 50 then
         @ tuttilut,loadlut backgr 
      else
         assign/disp d,'mydisp'			!Exit
         write/out We exit...
         return
      endif
   endif
! 
goto menuloop
! 
entry loadlut
!
if cursor(2) .lt. 50 then
   write/keyw out_a 'p1'
else
   write/keyw out_a 'p2'
endif
! 
if cursor(3) .eq. 2 then
   @a plotlut 'out_a'
else
   load/lut 'out_a'
endif



