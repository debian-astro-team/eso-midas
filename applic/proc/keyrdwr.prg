! @(#)keyrdwr.prg	19.1 (ES0-DMD) 02/25/03 13:20:14
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       keyrdwr.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Applications, Keywords. 
!.PURPOSE     Delete Session Keywords
!.VERSION     1.0    Creation    23-DEC-1993  PB
!             2.0 New Manager    18-JAN-1994  PB
!-------------------------------------------------------
!
IF M$INDEX(P1,".tbl") .LT. 1  WRITE/KEYW P1/C/1/80   "{P1}.tbl"
DEFINE/PARAM  P2  WRITE CHAR  "Mode (READ/WRITE):"
!
! Prepares  files
!
INQUIRE/SESSION {MID$CMND(11:14)}   IN_A   INPUTI
!
define/local CNT/I/1/1 0
define/local FID/I/1/2 0
!
OPEN/FILE middumma.tbl  WRITE  FID
DO CNT = 1 {INPUTI(1)}
   WRITE/FILE {FID(1)} "{{IN_A},:KEY,@{CNT}} {{IN_A},:KEY,@{CNT}}"
ENDDO
CLOSE/FILE {FID(1)}
!
IF P2(1:1) .EQ. "W" THEN
       COPY/LSKD  middumma.tbl {P1}
ELSE
       COPY/LSDK  middumma.tbl {P1}
ENDIF
!
RETURN







