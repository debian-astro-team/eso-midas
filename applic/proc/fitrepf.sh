#!/bin/sh
# @(#)fitrepf.sh	19.1 (ESO-IPG) 02/25/03 13:20:10
# Shell procedure fitcref.sh 
# called from fitcref.prg as $fitcref.sh
# Michele Peron    900215
# Carlos Guirao	   921224
# Carlos Guirao	   000609

rm -f fitcrea.o fitimag.o funcrea.o makefile.fit
cp $MID_LIB/libfituser.a .
cp $MID_FIT/fitcrea.mod .           
cp $MID_FIT/fitimag.mod .
cp $MID_FIT/funcrea.mod funcrea.o
cp $MID_FIT/makefile.fit makefile.fit

IFS="$IFS,"
set - $1
while [ $# -gt 0 ]
do
  OBJ="$OBJ $1.o"
  shift
done
rm -f $OBJ

make -i -f makefile.fit OBJ="$OBJ" OUT="fitcrea.exe fitimag.exe funcrea.exe"
