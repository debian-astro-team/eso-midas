! @(#)sortcol.prg	19.1 (ES0-DMD) 02/25/03 13:20:20
! Procedure: sortcol.prg
! Author   : P. Ballester   -   ESO Garching
! Date     : 910506
!
! Syntax   :
!
!    SORT/COLUMN  input_frame   output_frame
!
! Purpose  :
!
!    Column by column sorting of pixels of a frame 
!
define/param P1  ?   I "Input frame :"
define/param P2  ?   I "Output frame:"

ROTATE/COUNTER  {P1}  middumma

write/keyw in_a/C/1/60   middumma
write/keyw in_b/C/1/60   middummb

RUN  APP_EXE:sortrow

ROTATE/CLOCK    middummb  {P2}

COPY/DD  {P1}   LHCUTS    {P2}   LHCUTS

RETURN
