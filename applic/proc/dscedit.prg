! @(#)dscedit.prg	19.1 (ESO-DMD) 02/25/03 13:20:09
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure dscedit.prg to provide editing of descriptors
! of images or tables, e.g. the descriptor HISTORY
! K. Banse	870303, 900703, 921021, 931103, 940131, 980121, 021217
!
! use as   @a dscedit name descr [dsctype] [clean_flag]
! where    name = name of image frame
!	          for tables you must enter `name.tbl'
!          descr = name of descriptor which you want to edit
!          dsctype = type of descr., only needed if new descr.
!          clean_flag = Y(es) or N(o), if you want to clean up the descriptors
!	                after the editing
! 
! the editor will be the one defined via the SET/MIDAS_SYSTEM EDITOR=...
! command
! 
! note, that numerical data has to be separated by a blank, NOT by a comma...
! 
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
define/param p1 ? ima "Enter file name:"
define/param p2 ?  c  "Enter name of descr:"
define/param p3 c  c  "Enter type of new descr:"
define/param p4 n  c  "Enter clean_flag: "
define/maxpar 4
!
define/local filnam/c/1/60 {p1}
! 
! get contents of descriptor into file middummz.txt
read/descr {filnam} {p2} H >middummz.txt
define/local dexi/i/1/1 {outputi(1)}
define/local dtype/c/1/1 d
if dexi .eq. 0 then
   write/keyw dtype/c/1/1 {p3(1:1)}
else
   if outputi(2) .eq. 1 then
      write/keyw dtype/c/1/1 i
   elseif outputi(2) .eq. 2 then
      write/keyw dtype/c/1/1 r
   elseif outputi(2) .eq. 3 then
      write/keyw dtype/c/1/1 c
   endif
endif
!  edit that file with a normal text editor
if mid$sys(21:21) .eq. "$" then
   {mid$sys(21:30)} middummz.txt
else
   $ {mid$sys(21:30)} middummz.txt
endif
!
! now delete the descriptor (if it exists)
if dexi .eq. 1 delete/descr {filnam} {p2}
!
! and store contents of edited file back into descriptor
assign/in file middummz.txt
write/descr {filnam} {p2}/{dtype}/1/30		!the 30 will be overwritten...
assign/in terminal
!
! finally reclaim the space lost due to the DELETE/DESCR command above
if p4(1:1) .eq. "Y" .and. dexi .eq. 1 then
   define/local tb/i/1/1 0
   tb = m$index(filnam,".tbl")
   if tb .lt. 2 then
      copy/ii {filnam} &x ? d,no		!no update of HISTORY descr.
      rename/image &x {filnam} NO
   else
      copy/table {filnam} middummx
      rename/table middummx {filnam} NO
   endif
endif
