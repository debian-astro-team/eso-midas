! @(#)barycorr.prg	19.1 (ESO-DMD) 02/25/03 13:20:05
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.IDENTIFICATION
!  MIDAS command procedure barycorr.prg 
!  860203   D. Baade                             ESO - Garching
!  KB	870227, 890519, 900213, 920226, 971212
!  PJG  920302
!
!  correct times and radial velocities to center of sun or center of gravity
!  of solar system
!
!.COMMENT
!  Input has been changed so that eastern longitude is required
!  following IAU standards.          PJG 920902
!
!.INPUT/OUTPUT
!
!  a) if input is from keywords only
!  P1              date: year,month,day
!  P2              universal time: hour,min,sec
!  P3              right ascension: hour,min,sec
!  P4              declination: degree,min,sec
!  P5              eastern longitude of observatory: degree,min,sec 
!  P6              latitude of observatory: degree,min,sec
!  OUTPUTD/D/1/1   barycentric correction to time (days)
!  OUTPUTD/D/2/1   heliocentric correction to time (dayss)
!  OUTPUTR/R/1/1   barycentric correction to radial velocity (km/s)
!  OUTPUTR/R/2/1   heliocentric correction to radial velocity (km/s)
!
!  b) if coordinates, date and UT are input from table
!  P1              name of table
!  P2              longitude of observatory: degree,min,sec
!  P3              latitude of observatory: degree,min,sec
!  (all output is appended to table)
!
!  c) if date and UT are input from descriptor O_TIME of image
!  P1              name of image 
!  P2              right ascension: hour,min,sec
!  P3              declination: degree,min,sec
!  P4              longitude of observatory: degree,min,sec
!  P5              latitude of observatory: degree,min,sec
!
! Note:  To distinguish tables and images enter also the file type,
!        e.g. name.tbl or name.bdf;
!        if no explicit file type is given, we try to use a table with 
!        the given name (i.e. we try `name.tbl')
!------------------------------------------------------------------------------
!
crossref DATE UT ALPHA DELTA LONGITUDE LATITUDE
!
inputc(1:3) = "+++"
define/local rneg/r/1/1 1.0
!
define/param p1 ? ? -
"Enter date (year,month,day) or name of table or name of image:    "
!
if m$tstno(p1) .ne. 0 then
!
! all input from keywords
!
   define/param p2 ? N "Enter universal time (hour,min,sec):             "
   define/param p3 ? N "Enter right ascension (hour,min,sec):            "
   define/param p4 ? N "Enter declination (degree,min,sec):              "
   define/param p5 -70,43,55.35 N "Enter (eastern) longitude of observatory -
(degree,min,sec): "
   define/param p6 -29,15,25.8 N "Enter latitude of observatory -
(degree,min,sec): "
!
   write/keyw inputr/r/1/3 {p1},0,0
   write/keyw inputr/r/4/3 {p2},0,0
   write/keyw inputr/r/7/3 {p5},0,0
   rneg = (inputr(7)*3600.)+(inputr(8)*60.)+inputr(9)
   inputc(1:1) = p5(1:1)
   if rneg .lt. 0.0 inputc(1:1) = "-"
   write/keyw inputr/r/10/3 {p6},0,0
   rneg = (inputr(10)*3600.)+(inputr(11)*60.)+inputr(12)
   inputc(2:2) = p6(1:1)
   if rneg .lt. 0.0 inputc(2:2) = "-"
   write/keyw inputr/r/13/3 {p3},0,0
   write/keyw inputr/r/16/3 {p4},0,0
   inputc(3:3) = p4(1:1)
   rneg = (inputr(16)*3600.)+(inputr(17)*60.)+inputr(18)
   if rneg .lt. 0.0 inputc(3:3) = "-"
!
   write/keyw action BA				!indicate barycorr stuff
   run MID_EXE:COMPXY				!compute the corrections
!
   set/format f14.6,g24.12
   write/out "        Barycentric correction time:      {outputd(1)} day"
   write/out "        Heliocentric correction time:     {outputd(2)} day"
   write/out
   write/out "        Total barycentric RV correction:  {outputr(1)} km/s"
   write/out "        Total heliocentric RV correction: {outputr(2)} km/s"
   write/out "        (incl. diurnal RV correction of {outputr(3)} km/s)"
   return
endif
!
if m$filtyp(p1,".tbl") .eq. 1 then		!p1 = image
!
! dates, and times input from descr. o_time of image
!
   define/param p2 ? N "Enter right ascension (hour,min,sec):            "
   define/param p3 ? N "Enter declination (degree,min,sec):              "
   define/param p4 -70,43,55.35 N "Enter (eastern) longitude of observatory -
(degree,min,sec): "
   define/param p5 -29,15,25.8 N "Enter latitude of observatory -
(degree,min,sec): "
!
   write/keyw inputr/r/1/18 0.0 all
   inputr(1) = m$value({p1},o_time(1))
   inputr(2) = m$value({p1},o_time(2))
   inputr(3) = m$value({p1},o_time(3))
   inputr(4) = m$value({p1},o_time(5))			!UT in real hours
   write/keyw inputr/r/7/3 {p4}
   rneg = (inputr(7)*3600.)+(inputr(8)*60.)+inputr(9)
   inputc(1:1) = p4(1:1)
   if rneg .lt. 0.0 inputc(1:1) = "-"
   write/keyw inputr/r/10/3 {p5},0,0
   rneg = (inputr(10)*3600.)+(inputr(11)*60.)+inputr(12)
   inputc(2:2) = p5(1:1)
   if rneg .lt. 0.0 inputc(2:2) = "-"
   write/keyw inputr/r/13/3 {p2},0,0
   write/keyw inputr/r/16/3 {p3},0,0
   inputc(3:3) = p3(1:1)
   rneg = (inputr(16)*3600.)+(inputr(17)*60.)+inputr(18)
   if rneg .lt. 0.0 inputc(3:3) = "-"
!
   write/keyw action BA                         !indicate barycorr stuff
   run MID_EXE:COMPXY                           !compute the corrections
!
   set/format f14.6,g24.12
   write/out "        Barycentric correction time:      {outputd(1)} day"
   write/out "        Heliocentric correction time:     {outputd(2)} day"
   write/out
   write/out "        Total barycentric RV correction:  {outputr(1)} km/s"
   write/out "        Total heliocentric RV correction: {outputr(2)} km/s"
   write/out "        (incl. diurnal RV correction of {outputr(3)} km/s)"
   write/out " "
   write/out "Descriptor O_TIME of image {p1} used for date and UT."
!
else
!
! coordinates, dates, and times input from table
!
   define/param p2 -70,43,55.35 N "Enter (eastern) longitude of observatory -
(degree,min,sec): "
   define/param p3 -29,15,25.8 N "Enter latitude of observatory -
(degree,min,sec):  "
!
   define/local nline/i/1/1 0			!number of lines in table
   define/local lp/i/1/1 0			!loop variable
   define/local table/c/1/20			!name of table
!
   inputi(20) = m$index(p1,".tbl")-1
   table = "{p1(1:{inputi(20)})}       "
!
   write/keyw inputr/r/7/3 {p2},0,0		!east longitude
   inputc(1:1) = p2(1:1)
   rneg = (inputr(7)*3600.)+(inputr(8)*60.)+inputr(9)
   if rneg .lt. 0.0 inputc(1:1) = "-"
   write/keyw inputr/r/10/3 {p3},0,0		!latitude
   inputc(2:2) = p3(1:1)
   rneg = (inputr(10)*3600.)+(inputr(11)*60.)+inputr(12)
   if rneg .lt. 0.0 inputc(2:2) = "-"
!
! ... create table columns which shall accept the results
!
   set/midas output=logonly	!no error messages if columns exist already
   create/column {table} :BCT
   create/column {table} :HCT
   create/column {table} :EBRV
   create/column {table} :EHRV
   create/column {table} :EDV
   set/midas output=yes				!default
!
   nline = m$value({table}.tbl,tblcontr(4))       !get number of lines in table
!
   do lp = 1 nline				!loop over all lines in table
      inputc(3:3) = "+"
      copy/tk {table} :AHR :AMIN :ASEC @{lp} inputr/r/13/3   !right ascension
      copy/tk {table} :DDEG :DMIN :DSEC @{lp} inputr/r/16/3  !declination
      copy/tk {table} :YEAR :MONTH :DAY @{lp} inputr/r/1/3   !date
      copy/tk {table} :UTHR :UTMIN :UTSEC @{lp} inputr/r/4/3 !UT
!
      rneg = (inputr(16)*3600.)+(inputr(17)*60.)+inputr(18)
      if rneg .lt. 0.0 inputc(3:3) = "-"
!
      write/keyw action BA			!indicate barycorr stuff
      run MID_EXE:COMPXY			!compute the corrections
!
      outputr(4) = outputd(1)
      outputr(5) = outputd(2)
      copy/kt outputr/r/1/5 {table} :EBRV :EHRV :EDV :BCT :HCT @{lp}  ! save re
   enddo
!
   write/out {nline} rows of table {table}.tbl processed.
endif
