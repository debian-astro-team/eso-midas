! @(#)setfit.prg	19.1 (ES0-DMD) 02/25/03 13:20:18
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : SETFIT.PRG
!  FIT Subsystem
!
!  P.Defert 			ESO - Garching   26 FEB 86
!  M.Peron                      ESO - Garching   28 JUL 92
! .PURPOSE
!
!    implements
!
! SET/FIT
! to define auxiliary parameters in the fit package
!
! ----------------------------------------------------------------------
!
CROSSREF METHOD PRINT WEIGHT BOUNDS FUNCT FCTDEF BNDTAB
DEFINE/PARAM P1 'FITCHAR(15:20)' ?
DEFINE/PARAM P2 'FITCHAR(21:24)' ?
DEFINE/PARAM P3 'FITCHAR(13:13)' ?
DEFINE/PARAM P4 'FITCHAR(14:14)' ?
DEFINE/PARAM P5 'FITNAME(1:80)'  ?
DEFINE/PARAM P6 'FITCHAR(1:2)'   ?
DEFINE/PARAM P7 + TBL 
WRITE/KEYW FITCHAR/C/15/6 'P1'
WRITE/KEYW FITCHAR/C/21/4 'P2'
WRITE/KEYW FITCHAR/C/13/1 'P3'
WRITE/KEYW FITCHAR/C/14/1 'P4'
WRITE/KEYW FITNAME/C/1/80 'P5'
WRITE/KEYW FITCHAR/C/1/2  'P6'
IF P7(1:1) .NE. "+" THEN
    WRITE/KEYW BNDTAB/C/1/60 'P7'
ENDIF
