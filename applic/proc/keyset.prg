! @(#)keyset.prg	19.2 (ES0-DMD) 03/04/03 09:19:28
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!.COPYRIGHT   (C) 1991 European Southern Observatory
!.IDENT       keyset.prg
!.AUTHOR      Pascal Ballester,  ESO - Garching
!.KEYWORDS    Session, Manager, Set
!.PURPOSE     Command SET/...  PAR=VALUE ... 
!.VERSION     1.0    Creation    02-SEP-1991   PB
!
! 020225	last modif (KB)
! 
!-------------------------------------------------------
!
define/local npar/i/1/1    {pcount(1)}
define/local line/c/1/132  "{mid$line(1:132)}"
! 
if npar .le. 0  then
   write/out "Error: Command SET requires at least 1 argument"
   return/exit
endif
! 
! insert code of: inquire/session {mid$cmnd(11:14)} in_a outputi
! ..............................................................
define/local file/c/1/50  {seskey{mid$cmnd(11:12)}}
!
if m$exist(file) .eq. 0 then
   register/session {mid$cmnd(11:14)} {sesdir{mid$cmnd(11:12)}} -
                    {sesfil{mid$cmnd(11:12)}} {file}
endif
!
write/keyw in_a {file}
outputi =  {{file},TBLCONTR(4)}     !before: COPY/DK {file} TBLCONTR/I/4/1 {P3}
! 
! ..............................................................
! end of insertion
! 
inputi(1) = npar
run APP_EXE:keyset

