! @(#)settsa.prg	19.1 (ES0-DMD) 02/25/03 13:20:19
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
!  MIDAS Command : SETTSA.PRG
!  TSA Subsystem
!
!  A. Sprimont 			ESO - Garching   08 DEC 86
!  P.Defert 			ESO - Garching   08 DEC 86
!
! .PURPOSE
!
!    implements
!
! SET/TSA
! to define auxiliary parameters in the TSA package
!
! ----------------------------------------------------------------------
!
CROSSREF METHOD PRINT WIDTH OUTPUT
DEFINE/PARAM P1 'TSAKEY(1:5)' ?
DEFINE/PARAM P2 'TSAKEY(6:8)' ?
DEFINE/PARAM P3 'TSAKEY(9:11)' ?
DEFINE/PARAM P4 'TSAKEY(12:14)' ?
WRITE/KEYW TSAKEY/C/1/5 'P1'
WRITE/KEYW TSAKEY/C/6/3 'P2'
WRITE/KEYW TSAKEY/C/9/3 'P3'
WRITE/KEYW TSAKEY/C/12/3 'P4'
