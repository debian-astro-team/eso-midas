! @(#)resetback.prg	19.1 (ES0-DMD) 02/25/03 13:20:18
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure  resetback.prg  
!       to reset the broken connection with a background Midas
! 
! K. Banse	931125  La Silla
!
! execute as  @a resetback BMidas_unit [restart-command]
! 
! where  BMidas-unit = the unit of the background Midas we were connected to
!        resetart-command = optional command(script) which, when executed, will
!                           restart the background Midas
!                           you have then to reconnect via CONNECT/BACKGROUND
!
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! 
define/param p1 ? c "Enter unit of Background Midas: "
define/param p2 + c "Enter optional restart-command/script for Background Midas: "
define/maxpar 2
! 
if p1(2:2) .eq. " " then
   write/error 30
   return/exit
endif
! 
define/local fcn/i/1/2 0,0
define/local stri/c/1/4 {p1(1:2)}{MID$SESS(11:12)}
! 
! --- 
! try to delete any screwed up "receive" box
! ---
! 
if aux_mode(1) .eq. 1 then				!VAX/VMS
   -DELETE MID_WORK:FORGR{stri}.RBOX.*
else							!Unix
   -DELETE MID_WORK:FORGR{stri}.RBOX
endif
! 
! ---
! build a clean receive box
! ---
! 
-DELETE {stri}_reset			!delete any trailing one...
! 
! check, if background Midas is still around
fcn = m$exist("MID_WORK:RUNNING{p1(1:2)}")
if fcn .ne. 1 then
   write/out Midas with unit {p1(1:2)} has died!
   if p2(1:1) .eq. "+" then
      write/out So we cannot reset much... restart the Background Midas.
      return/exit
   else
      $ {p2}
      write/out Midas Unit {p1} restarted ...
      return
   endif
endif
!
open/file {stri}_reset write fcn
if fcn .lt. 0 then
   write/out "Could not open file in MID_WORK - directory full?"
   write/out "We abort..."
   return/exit
endif
! 
write/file {fcn} Status = 0,0		!emulate a succesfull return...
close/file {fcn}
-RENAME {stri}_reset MID_WORK:FORGR{stri}.RBOX
! 
write/out Connection with Midas Unit {p1} reset ...
