! @(#)pcluster.prg	19.1 (ES0-DMD) 02/25/03 13:20:16
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! .IDENTIFICATION
!
! STATISTICAL PACKAGE : PCLUSTER.PRG
! F.Murtagh, J.D.Ponz			version 1.0  22 JAN 85
!
! .PURPOSE
!
! execute the command :
!
! PARTITION/TABLE input output nclass [iter]
!
! Default nclass = 3 (i.e. 3 classes required)
! Default iter = 0 (i.e. no iteration)
!
! -----------------------------------------------------
DEFINE/PARAM P3 3 N
DEFINE/PARAM P4 0 N
WRITE/KEYW INPUTI/I/1/2 'P3','P4'
RUN APP_EXE:PCLUSTER
