! @(#)plotcol.prg	19.1 (ES0-DMD) 02/25/03 13:20:16
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
! MIDAS procedure plotcol.prg
! K. Banse	860917, 901212
!
! use via   @a plotcol frame column_coord
!         plot the column of a 2-dim image
!
! where   frame = name of image file
! 	  column_coord = the coordinate of the required column
! 			 in the ususal MIDAS coordinate format
! Example:
!	  @a plotcol ngc1234 @17       plot column no. 17 of image ngc1234.bdf
!
! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
DEFINE/PARAM P1 ? IMA "Enter frame name:"
!
EXTRACT/LINE &c = 'P1'['P2',<:'P2',>]
WRITE/DESCR &c IDENT "column at 'P2' of frame 'P1'"
PLOT/ROW &c
