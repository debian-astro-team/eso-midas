! @(#)bintabl.prg	19.1 (ES0-DMD) 02/25/03 13:20:05
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.IDENTIFICATION: BINTBL.PRG
!.PURPOSE:        Create table (bin.tbl) with binned averages
!                 of table column
!                 BIN/TABLE table col1 col2 [bin] [min] [max] [sigma]
!.AUTHOR:         J. Edwin Huizinga
!.VERSION:        910110 JEH private program converted to MIDAS standards
!.VERSION:        910621 RHW implemented in MIDAS
!------------------------------------------------------------
DEFINE/PARAM P1 ? TABLE "Enter table:"
DEFINE/PARAM P2 ? C "Enter X-column:"
DEFINE/PARAM P3 ? C "Enter Y-column:"
DEFINE/PARAM P4 + ?
DEFINE/PARAM P5 + ?
DEFINE/PARAM P6 + ?
DEFINE/PARAM P7 + N
WRITE/KEYW DEFAULT/C/1/4 YYYY
!
IF P4 .NE. "+" THEN
   WRITE/KEYW DEFAULT/C/1/1 N
   WRITE/KEYW INPUTR/R/1/1 'P4'
ENDIF
IF P5 .NE. "+" THEN
   WRITE/KEYW DEFAULT/C/2/1 N
   WRITE/KEYW INPUTR/R/2/1 'P5'
ENDIF
IF P6 .NE. "+" THEN
   WRITE/KEYW DEFAULT/C/3/1 N
   WRITE/KEYW INPUTR/R/3/1 'P6'
ENDIF
IF P7 .NE. "+" THEN
   WRITE/KEYW DEFAULT/C/4/1 N
   WRITE/KEYW INPUTR/R/4/1 'P7'
ENDIF
!
RUN APP_EXE:bintabl
