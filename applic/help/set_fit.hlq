% @(#)set_fit.hlq	19.1 (ESO-IPG) 02/25/03 13:19:19 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      set_fit.hlq
%.AUTHOR     PD+JDP, IPG/ESO
%.KEYWORDS   MIDAS, help files, SET/FIT
%.PURPOSE    On-line help file for the command: SET/FIT
%.VERSION    1.0  20-FEB-1986 : Creation, PD+JDP
%.VERSION    2.0  19-NOV-1992 : add BNDTAB and BNDTYP, MP
%----------------------------------------------------------------
\se
SECTION./FIT
\es\co
SET/FIT							02-SEP-1992   PD+JDP+MP
\oc\su
SET/FIT par=value [par=value ...]
	set parameters for the FITTING\ package
\us\pu
Purpose:      
           Modify qualifiers of the FIT command
\up\sy
Syntax:       
           SET/FIT par=value [par=value ...]
\ys\pa
           par   = parameter name
\ap\pa
           value = parameter value
\\
           Allowed parameters and values are:
\\
           METHOD  - fitting method as
\\
               NR     : Newton-Raphson - derivatives required  (default)
\\
               CGNND  : Corrected Gauss-Newton method - no derivatives
\\
               QN     : Quasi Newton  - derivatives required
\\
               MGN    : Modified Gauss-Newton - derivatives required
\\
           PRINT   - integer number to control the printing of the
                     intermediate fitted values. If negative, the
                     correlations matrix will be computed and displayed.
                     Default is 1.
\\
           WEIGHT  - weighting scheme as
\\
               C      : Constant (=no) weighting (default)
\\
               W      : Weight is given in the FIT command
                         (consult also HELP on FIT)
\\
               I      : Instrumental weighting w(i)=1/sigma(i)**2
                         (consult also HELP on FIT)
\\
               S      : Statistical weighting w(i)=1/indep_var(i)

\\
           FUNCT   - FIT function to be used.
               any function created by EDIT/FIT.
               Default name is FIT.
\\
           FCTDEF  - function definition type
               SY[ST] : Only MIDAS defined functions are used (Default).
               US[ER] : User functions (USER_) are defined.
\\
           BOUNDS  - parameters bounds type
               N : No bounds (default)
               P : Parameters are forced to be positive
               I : Bounds are provided by the user in a table . The name
                   of the table can be defined by the parameter BNDTAB. The
                   table should contain two columns, the first one for the
                   lower bound, the second one for the upper bound and as
                   many rows as the number of parameters.
               G : The same bounds are used for all parameters. There will
                   be read from the first row of the table defined by the 
                   parameter BNDTAB.

           BNDTAB  - parameter bounds table
\ap\no
Note:   
          none

\on\exs
Examples:
\ex
           SET/FIT METHOD=QN PRINT=2
           or SET/FIT QN 2
           select QN method and print intermediate results every second
           iteration. No correlations computed.
\xe \sxe
