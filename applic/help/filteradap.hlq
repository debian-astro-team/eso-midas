% @(#)filteradap.hlq	19.1 (ESO-IPG) 02/25/03 13:19:16 
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      filter.adap.hlq
%.AUTHOR     G.M. Richter
%.KEYWORDS   MIDAS, help files, FILTER/ADAPTIV
%.PURPOSE    On-line help file for the command: FILTER/ADAPTIV
%.VERSION    1.0  10-APR-1991 : Creation, Richter 
%----------------------------------------------------------------
\se
SECTION./ADAP
\es\co
FILTER/ADAPTIV                                                 10-APR-1991, GMR
\oc\su
FILTER/ADAPTIV frame outframe [maskframe] [type] [shape] size k noise   
	adaptive filtering of an image
\us\pu
Purpose: 
         Adaptive filtering of an image
\up\sub
Subject: 
         Smoothing, gradient-filter, Laplace-filter
\bus\sy
Syntax:  
         FILTER/ADAPTIV frame outframe [maskframe] [type] [shape] size k noise
\ys\pa
         frame =     input image
\ap\pa
         outframe =  result image
\ap\pa
         maskframe = input image for noise statistics: Only unmasked 
                     (mask-value = 0.) pixels of the input image are used to 
                     estimate the noise statistics. If 'NULL' is put in, no 
                     mask is used (default).
\ap\pa
         type =      type of filter:\\
                     'S'= smoothing (default)
                     'G'= gradient-filter
                     'L'= Laplace-filter
\ap\pa
         shape =     shape of impulse response:\\
                     'B'= box
                     'P'= pyramide (P is recommented and default)
\ap\pa
         size =      maximal size of impulse response. In regions of high 
                     resolution the actual size is smaller. Possible sizes are:
                     for box: 3,5,9,17,33,65,129 pixels,\\
                     for pyr.:3,5,7,11,15,23,31,47,63,95,127.
\ap\pa
         k =         threshold for significance (see note).
\ap\pa
         noise =     noise model:\\
                     'A'= additive noise assumed
                     'P'= Poisson-noise assumed
\ap\no
Note:    
         Algorithm: The local-signal-to noise ratio as a
         function of decreasing resolution is evaluated via the
         H-transform: mean gradients and curvatures over different
	 scale lengths (obtained from the H-coefficients of
	 different order) are compared to the corresponding
	 exspectation values of the noise. The order for which this
	 signal-to-noise ratio exceedes a given parameter k
	 indicates the local resolution scale length of the signal
	 (dubbed: the point becomes significant at this order), and
	 determines the size of the impulse response of the filter
	 at this point.
\\
	 When the algorithm is finished some information on the
	 noise statistics is printed out on the terminal: the
	 standard deviation and the exspectation values of the
	 gradients and the Laplace-termes at every order involved,
	 and the the number of pixels which became significant on
	 every order by the gradient and by the Laplace-term
	 respectively. The rest pixels are set to the maximal size
	 response.
\on\exs
Examples:
\ex
            FILTER/ADAPTIV frame result null s p 31 3 a
            The image 'frame' is adaptively smoothed with a maximal
	    filter sizs of 31x31 pixels. The noise statistics is
	    estimated from the whole image (also signal is included!
	    be careful when strong signal is in the image!).
            The image 'result' is displayed on channel 1.
\xe \sxe
