C @(#)tkstest.for	19.1 (ESO-DMD) 02/25/03 13:21:26
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  1 JUL 1989
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: M.PERON
C
C.IDENTIFICATION
C
C  Program TKSTEST
C
C.KEYWORDS
C
C KOLMOGOROV TEST
C
C.PURPOSE
C
C Comparaison of one data set to a known distribution using Kolmogorov TEST
C 
C The distribution available are :
C  --Uniform Distribution (UNI)
C  --Gaussian Distribution (GAUS)
C --
C
C
C The application calculates the KS statistic D as well as the probability
C of exceeding D under the Null hypothesis which is :
C Data follow the selectedn distribution
C References: Numerical Recipes p 472
C 
C.VERSION
C 
C 010621		last modif
C 
C-----------------------------------------------------------
C
       PROGRAM TKSTEST
C 
       IMPLICIT NONE
C 
       REAL*4 D,PROB,PARA(2)
C 
       INTEGER*8   ADDRIN(1)
C 
       INTEGER MADRID,IACT,KUN,KNUL,STAT,NSC,NBYTES
       INTEGER PARVAL,NPAR,ISTAT,TID,NCOL,NROW,NACOL,NAROW,COL
       INTEGER NREAL
C 
       CHARACTER*80 INTAB
       CHARACTER*1 DISTRI
       CHARACTER*17 COLIND
       CHARACTER*80 OUTPUT1,OUTPUT2,OUT,OUTPUT0,OUTPUT3
C 
       INCLUDE 'MID_INCLUDE:TABLES.INC'
       COMMON/VMR/MADRID(1)
C 
       INCLUDE 'MID_INCLUDE:TABLED.INC'
       DATA PARVAL/8/, TID /-1/
       DATA OUTPUT0 
     1    /' Null Hypothesis : Data follow the given distribution'/
       DATA OUTPUT1
     1    /'Kolmogorov-Smirnov D Statistic  '/
       DATA OUTPUT2
     2    /'-----------------------------------------------------  '/
       DATA OUTPUT3
     3   /'Probability of exceeding D under Null Hypothesis'/
C
C      get into MIDAS
       CALL STSPRO('TKSTEST')

C      read parameters of the command
       CALL TDPGET(PARVAL,NPAR,ISTAT)
       IF  (ISTAT.NE.0) GOTO 80
       INTAB = TPARBF(1)
       COLIND = TPARBF(2)

c      read name of the distribution used

       CALL STKRDC('INPUTC',1,1,1,IACT,DISTRI,KUN,KNUL,STAT)
       CALL UPCAS(DISTRI,DISTRI)
C
c      read parameters of this distribution
       CALL STKRDR('INPUTR',1,2,IACT,PARA,KUN,KNUL,STAT)       

c      open the input table

       CALL TBTOPN(INTAB,F_I_MODE,TID,ISTAT)       
       IF(ISTAT.NE.0) GOTO 80
       CALL TBIGET(TID,NCOL,NROW,NSC,NACOL,NAROW,ISTAT)
        CALL TBCSER(TID,COLIND,COL,ISTAT)
         IF (COL.LT.0) THEN
         CALL STTPUT('Column not found...',ISTAT)
          GOTO 80
        ENDIF


c      prepare work space

       NBYTES = 4*NROW
       CALL TDMGET(NBYTES,ADDRIN(1),ISTAT)
       CALL TMAP(TID,NROW,COL,MADRID(ADDRIN(1)),NREAL)
        CALL SORT (MADRID(ADDRIN(1)),NREAL)

c      calculate the KS Statistic

       CALL KOLM1D(MADRID(ADDRIN(1)),NREAL,DISTRI,PARA,D,PROB)
       CALL STTPUT(OUTPUT0,ISTAT)
       CALL STTPUT(OUTPUT2,ISTAT)
       CALL STTPUT(OUTPUT1,ISTAT)
        WRITE(OUT,100)D
       CALL STTPUT(OUT,ISTAT)
       CALL STTPUT(OUTPUT2,ISTAT)
       CALL STTPUT(OUTPUT3,ISTAT)
        WRITE(OUT,100)PROB
       CALL STTPUT(OUT,ISTAT)
       CALL STTPUT(OUTPUT2,ISTAT)
       CALL STKWRR('OUTPUTR',D,1,1,KUN,STAT)
       CALL STKWRR('OUTPUTR',PROB,2,1,KUN,ISTAT)
        CALL TBTCLO(TID,ISTAT)

C      that's folks

       CALL STSEPI
80      CONTINUE
100     FORMAT(1X,G15.6,15X,G15.6)
        END

