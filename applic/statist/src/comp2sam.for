C @(#)comp2sam.for	19.1 (ES0-DMD) 02/25/03 13:21:26
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  5 JUL 1989
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: M.PERON
C
C.IDENTIFICATION
C
C  Program COMP2SAM
C
C.KEYWORDS
C
C KOLMOGOROV, SIGN, WILCOXON
C
C.PURPOSE
C
C Comparaison of two data sets using 
C   - Kolmogorov Test 
C   - Sign Test  - NOT IMPLEMENTED YET-
C   - Wilcoxon Test - NOT IMPLEMENTED YET-
C   - Mann-Whitney U test -NOT IMPLEMENTED YET-
C-----------------------------------------------------------
C
       PROGRAM   COMP2SAM
       REAL      D,PROB
       INTEGER   MADRID
       INTEGER*8 ADDRIN(4)
       INTEGER   NSC,KUN
       INTEGER*4 PARVAL,NPAR,ISTAT,TID,NCOL,NROW,NACOL,NAROW,COL1,COL2
       INTEGER*4 NBYTES,NREAL
       CHARACTER*80 INTAB,OUTPUT1,OUTPUT2,OUTPUT3,OUT,OUTPUT0
       CHARACTER*17 COLIND,COLDEP
       INCLUDE 'MID_INCLUDE:TABLES.INC'
       COMMON/VMR/MADRID(1)
       INCLUDE 'MID_INCLUDE:TABLED.INC'
       DATA PARVAL/8/
       DATA OUTPUT0
     1 /' Null Hypothesis: the two data sets come 
     2   from the same distribution'/
       DATA OUTPUT1
     3    /'Kolmogorov-Smirnov D Statistic '/
       DATA OUTPUT2
     4   /'Probability of exceeding D under Null Hypothesis'/
       DATA OUTPUT3
     5 /'----------------------------------------------------------'/
       CALL STSPRO('COMP2SAM')
       CALL TDPGET(PARVAL,NPAR,ISTAT)
       IF  (ISTAT.NE.0) GOTO 80
       INTAB = TPARBF(1)
       COLIND = TPARBF(2)
       COLDEP = TPARBF(3)
       CALL TBTOPN(INTAB,F_U_MODE,TID,ISTAT)       
       IF(ISTAT.NE.0) GOTO 80
       CALL TBIGET(TID,NCOL,NROW,NSC,NACOL,NAROW,ISTAT)
        CALL TBCSER(TID,COLIND,COL1,ISTAT)
         IF (COL1.LT.0) THEN
         CALL STTPUT('Column not found...',ISTAT)
          GOTO 80
        ENDIF
        CALL TBCSER(TID,COLDEP,COL2,ISTAT)
        IF (COL2.LT.0) THEN
          CALL STTPUT('Column not found...',ISTAT)
          GOTO 80
        ENDIF
       NBYTES = 4*NROW
       CALL TDMGET(NBYTES,ADDRIN(1),ISTAT)
       CALL TDMGET(NBYTES,ADDRIN(2),ISTAT)
       CALL TMAP2(TID,NROW,COL1,COL2,MADRID(ADDRIN(1)),
     1        MADRID(ADDRIN(2)),NREAL)
           CALL SORT (MADRID(ADDRIN(1)),NREAL)
           CALL SORT (MADRID(ADDRIN(2)),NREAL)
                CALL KOLM2DIS(MADRID(ADDRIN(1)),NROW,MADRID(ADDRIN(2)),
     1              NREAL,D,PROB)
          CALL STKWRR('OUTPUTR',D,1,1,KUN,ISTAT)
          CALL STKWRR('OUTPUTR',PROB,2,1,KUN,ISTAT)
          CALL STTPUT(OUTPUT0,ISTAT)
          CALL STTPUT(OUTPUT3,ISTAT)
          WRITE(OUT,100)D
          CALL STTPUT(OUTPUT1,ISTAT)
          CALL STTPUT(OUT,ISTAT)
          CALL STTPUT(OUTPUT3,ISTAT)
          CALL STTPUT(OUTPUT2,ISTAT)
          WRITE(OUT,100)PROB
          CALL STTPUT(OUT,ISTAT)
          CALL STTPUT(OUTPUT3,ISTAT)

C       IF (ACTION.EQ.'SIGN') THEN
C          CALL SIGN(MADRID(ADDRIN(1)),MADRID(ADDRIN(2)),NROW)
C       ENDIF
C       IF (ACTION.EQ.'WILC') THEN
C          CALL TDMGET(NBYTES,ADDRIN(3),ISTAT)
C          CALL TDMGET(NBYTES,ADDRIN(4),ISTAT)
C          CALL WILC(MADRID(ADDRIN(1)),MADRID(ADDRIN(2)),MADRID(ADDRIN(3)),
C     1          MADRID(ADDRIN(4)),NROW,S)
C          MEAN = NROW*(NROW+1)/4
C          SIGMA = NROW*(NROW+1)*(2*NROW+1)/24
C           PROB = ERFCC((S-MEAN)/SQRT(2.*SIGMA))
C          CALL STKWRR('OUTPUTR',S,1,1,KUN,ISTAT)
C          CALL STKWRR('OUTPUTR',PROB,2,1,KUN,ISTAT)
C          WRITE(OUT,100)S,PROB
C          CALL STTPUT(OUTPUT2,ISTAT)
C          CALL STTPUT(OUTPUT3,ISTAT)
C          CALL STTPUT(OUT,ISTAT)
C       ENDIF
        CALL TBTCLO(TID,ISTAT)
       CALL STSEPI
80      CONTINUE
100     FORMAT(1X,G15.6,15X,G15.6)
        END

