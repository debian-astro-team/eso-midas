% @(#)kstest1sam.hlq	19.1 (ESO-IPG) 02/25/03 13:19:18
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%.COPYRIGHT  (c)  1990 European Southern Observatory
%.IDENT      kstest1sam.hlq
%.AUTHOR     MP, IPG/ESO
%.KEYWORDS   MIDAS, help files, KSTEST/1SAM
%.PURPOSE    On-line help file for the command: KSTEST/1SAM
%.VERSION    1.0  03-NOV-1989 : Creation, MP
%----------------------------------------------------------------
\se
SECTION./1SAM
\es\co
KSTEST/1SAM    			                    03-NOV-1989 MP
\oc\su
KSTEST/1SAM table col distri coeffs 
	kolmogorov one-sample test
\us\pu
Purpose:      
              Performs a Kolmogorov-Smirnov One-sample Test .
\up\sy
Syntax:       
             KSTEST/1SAM table col [distri] [coeffs] 
\ys\pa
             table = name of the table
\ap\pa
             col  = column containing the set of sample values.
\ap\pa
             distri = theoritical distribution\\
                         U(NIFORM) for uniform probability function\\
                         G(AUSS) for Gaussian p.f.\\
                         E(XPONENTIAL) for exponential p.f.\\
                         P(OISSON) for Poisson p.f.\\
                         distri is defaulted to UNIFORM
\ap\pa
             coefs = coefficients for the distribution above,
                     number and meaning depends on the parameter distri,
                     defaulted to 0.,1.
\ap\no
Note:        
             The command provides a test of the Null Hypothesis:\\
             "The data set comes from a distribution having the theoretical
             distribution" against the hypothesis:\\
             "The data set cannot be considered to be a random sample from the
             specified theoritical distribution"
             The command will return the Kolmogorov-Smirnov D statistic 
             (largest absolute deviation between the sample cumulative 
             distribution and the theoritical distribution) as well as the 
             probability of exceeding D under Null Hypothesis,
\on\exs
Examples:
\ex
             none
\xe \sxe
