C @(#)fitnag.for	19.1 (ES0-DMD) 02/25/03 13:17:31
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FITNAG(ALGOR,ISTAT)                                           
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  21:29 - 20 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: ECF                                                                   
C                                                                               
C.PURPOSE                                                                       
C       Interfaces the old MIDAS FIT module with yhe new one using NAG          
C       library.                                                                
C                                                                               
C.KEYWORDS                                                                      
C       NAG library, Non-linear Least Squares.                                  
C                                                                               
C.DESCRIPTION                                                                   
C       Detects if the problem is constrained and passes the control            
C       to the right module.                                                    
C                                                                               
C-----------------------------------------------------------------------        
C     ..                                                                        
C      IMPLICIT NONE                                                            
C     .. Scalar Arguments ..                                                    
      INTEGER ISTAT,STATUS,KUN,KNUL,K
      CHARACTER ALGOR* (*)                                                      
C     ..                                                                        
C     .. Local Scalars ..                                                       
      CHARACTER BNDKEY*10,BNDTYP*1                                    

C     ..                                                                        
      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:FITNAGI.INC'
      INCLUDE 'MID_INCLUDE:FITNAGC.INC'
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'
      DATA BNDKEY(1:10)/'FITCHAR   '/                                           
C                                                                               
C   Get the boundary type in FITCHAR 14:14                                      
C                                                                               
      CALL STKRDC(BNDKEY,1,FZIBND,1,K,BNDTYP,KUN,KNUL,STATUS)     
C                                                                               
C   Chain to the right module                                                   
C                                                                               
      CALL UPCAS(BNDTYP,BNDTYP)
      IF (BNDTYP(1:1).EQ.'N') THEN                                              
          CALL FITUNC(ALGOR,ISTAT)                                             
                                                                                
      ELSE IF (BNDTYP(1:1).EQ.'I' .OR. BNDTYP(1:1).EQ.'P' .OR.                  
     +         BNDTYP(1:1).EQ.'G') THEN                                         
          CALL FITCON(ALGOR,BNDTYP,ISTAT)                                      
                                                                                
      ELSE                                                                      
          CALL STETER(12,'FIT/TABLE : Bounds type mismatch')                    
      END IF                                                                    
                                                                                
      RETURN                                                                    
                                                                                
      END                                                                       
