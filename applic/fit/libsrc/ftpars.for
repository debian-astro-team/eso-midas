C @(#)ftpars.for	19.1 (ES0-DMD) 02/25/03 13:17:35
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTPARS(PAR1,NIND,NPAR,ISTAT)                                   
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C PARSE FUNCTION DEFINITION AND SET UP TABLE WITH PARAMETER NAMES               
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  USE MIDAS I/O INTERFACES TO FRAMES AND TABLES                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  NON LINEAR FITTING                                                           
C                                                                               
C                                                                               
C----------------------------------------------------------------               
C                                                                               
C      IMPLICIT NONE                                                            
C                                                                               
C INPUT ARGUMENTS                                                               
C PAR1 CHAR FUNCTION ARGS (x1,...;p1,...)                                       
C                                                                               
C OUTPUT ARGUMENTS                                                              
C NIND INTG NUMBER OF INDEPENDENT VARIABLES                                     
C NPAR INTG NUMBER OF FUNCTION PARAMETERS                                       
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      INTEGER NIND,NPAR,ISTAT,I,I1 
      INTEGER II,KZ6967,LENGTH,NP1,II1
      CHARACTER*(*) PAR1                                                        
      CHARACTER LINE1*80,LINE2*80,C*1,EOFMARK*1,DELIM*1                         
      CHARACTER*20 TOKEN                                                        
      CHARACTER*20 TEST1, TEST2
      LOGICAL EOF                                                               
       INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'                                     
       INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'                                     
      LINE1  = PAR1//',?'                                                       
C                                                                               
C ... SEARCH FOR INDEP VARS                                                     
C                                                                               
      II     = INDEX(LINE1,'?') - 2                                             
      NIND   = 0                                                                
      DO 10 I = 1,II                                                            
          C      = LINE1(I:I)                                                   
          IF (C.EQ.',') NIND   = NIND + 1                                       
          IF (C.EQ.';') THEN                                                    
              NIND   = NIND + 1                                                 
              GO TO 20                                                          
          END IF                                                                
   10 CONTINUE                                                                  
      ISTAT  = FERPFU                                                       
C                                                                               
C ... GET TOKENS CORRESPONDING TO PARAMS                                        
C                                                                               
   20 I1     = I + 1                                                            
      NPAR   = 0                                                                
      LINE2  = LINE1(I1:)                                                       
      EOF    = .FALSE.                                                          
      EOFMARK = '?'                                                             
      DELIM  = ','                                                              
      KZ6967 = 0
99    KZ6967 = KZ6967 + 1
          IF ( .NOT. (.NOT.EOF)) GO TO 50                                       
          CALL FTTOKN(LINE2,EOFMARK,DELIM,TOKEN,LENGTH,EOF)                     
          IF ( .NOT. EOF) THEN                                                  
              NPAR   = NPAR + 1                                                 
              NP1    = NPAR + FZNPTOT                                         
              FZPTOKEN(NP1) = TOKEN                                           
              FZPLEN(NP1) = LENGTH                                            
C                                                                               
C     CHECK THAT PARAMETER NAME IS NOT DUPLICATED                               
C                                                                               
              TEST1 = TOKEN
              CALL FORUPC(TEST1, TEST1)
              DO 30 I = 1,NP1 - 1                                               
                  II1    = FZPLEN(I)                                          
                  TEST2 = FZPTOKEN(I)
                  CALL FORUPC(TEST2, TEST2)
C                  IF (TEST1(1:II1).EQ.TEST2(1:II1)) THEN              
C                      ISTAT  = FERDUP                                       
C                      RETURN                     
C                  END IF                      
   30         CONTINUE                                                          
          END IF                                                                
          I1     = LENGTH + 2                                                   
          LINE1  = LINE2                                                        
          LINE2  = LINE1(I1:)                                                   
      GOTO 99
   50 CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
