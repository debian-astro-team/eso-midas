$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.APPLIC.FIT.LIBSRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:25:02 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ FORTRAN   brand.for
$ FORTRAN   cauchy.for
$ FORTRAN   cauchy1d.for
$ FORTRAN   cauchy2d.for
$ FORTRAN   chorec.for
$ FORTRAN   dpsi.for
$ FORTRAN   dcprin.for
$ FORTRAN   dgamma.for
$ FORTRAN   dipole.for
$ FORTRAN   dmatin.for
$ FORTRAN   erf.for
$ FORTRAN   erfunc.for
$ FORTRAN   fexp.for
$ FORTRAN   fitcon.for
$ FORTRAN   fitnag.for
$ FORTRAN   fitunc.for
$ FORTRAN   flog.for
$ FORTRAN   franz.for
$ FORTRAN   franz1d.for
$ FORTRAN   franz2d.for
$ FORTRAN   fsin.for
$ FORTRAN   fsinh.for
$ FORTRAN   ftan.for
$ FORTRAN   ftanh.for
$ FORTRAN   ftcomp.for
$ FORTRAN   ftcopy.for
$ FORTRAN   ftcpgv.for
$ FORTRAN   ftcva1.for
$ FORTRAN   ftcval.for
$ FORTRAN   ftdcfn.for
$ FORTRAN   ftddvi.for
$ FORTRAN   ftddvt.for
$ FORTRAN   ftdfpr.for
$ FORTRAN   ftdfun.for
$ FORTRAN   ftdivi.for
$ FORTRAN   ftdivt.for
$ FORTRAN   ftdodo.for
$ FORTRAN   ftexit.for
$ FORTRAN   ftext1.for
$ FORTRAN   ftfunc.for
$ FORTRAN   ftimag.for
$ FORTRAN   ftinfo.for
$ FORTRAN   ftini1.for
$ FORTRAN   ftinif.for
$ FORTRAN   ftinit.for
$ FORTRAN   ftintr.for
$ FORTRAN   ftpars.for
$ FORTRAN   ftparv.for
$ FORTRAN   ftrdfn.for
$ FORTRAN   ftrdin.for
$ FORTRAN   ftrdpr.for
$ FORTRAN   ftspar.for
$ FORTRAN   ftstat.for
$ FORTRAN   ftsval.for
$ FORTRAN   fttabl.for
$ FORTRAN   fttntr.for
$ FORTRAN   fttokn.for
$ FORTRAN   funct1.for
$ FORTRAN   funct1i.for
$ FORTRAN   funct1t.for
$ FORTRAN   funct2.for
$ FORTRAN   funct2i.for
$ FORTRAN   funct2t.for
$ FORTRAN   funct3.for
$ FORTRAN   funct4.for
$ FORTRAN   fitbl.for
$ FORTRAN   gammdis.for
$ FORTRAN   gauss.for
$ FORTRAN   gauss1d.for
$ FORTRAN   gauss2d.for
$ FORTRAN   moffat.for
$ FORTRAN   gauss3d.for
$ FORTRAN   gaussa.for
$ FORTRAN   igauss.for
$ FORTRAN   igaussa.for
$ FORTRAN   laplace.for
$ FORTRAN   logistic.for
$ FORTRAN   lognorm.for
$ FORTRAN   lorentz.for
$ FORTRAN   lsfun1.for
$ FORTRAN   lsfun1i.for
$ FORTRAN   lsfun1t.for
$ FORTRAN   lsfun2.for
$ FORTRAN   lsfun2i.for
$ FORTRAN   lsfun2t.for
$ FORTRAN   lsfun3.for
$ FORTRAN   lsfun4.for
$ FORTRAN   lsqgrd.for
$ FORTRAN   lsqmon.for
$ FORTRAN   lsqmsg.for
$ FORTRAN   matdiag.for
$ FORTRAN   minmon.for
$ FORTRAN   minmsg.for
$ FORTRAN   mnmx.for
$ FORTRAN   nullf.for
$ FORTRAN   pareto.for
$ FORTRAN   poisson.for
$ FORTRAN   poly.for
$ FORTRAN   poly1d.for
$ FORTRAN   poly2d.for
$ FORTRAN   poly3d.for
$ FORTRAN   semilog.for
$ FORTRAN   setone.for
$ FORTRAN   setzero.for
$ FORTRAN   sinc.for
$ FORTRAN   sinc2.for
$ FORTRAN   triang.for
$ LIB/REPLACE libfit brand.obj,cauchy.obj,cauchy1d.obj,cauchy2d.obj,chorec.obj,dpsi.obj,dcprin.obj,dgamma.obj,dipole.obj,dmatin.obj,erf.obj,erfunc.obj,fexp.obj
$ LIB/REPLACE libfit fitcon.obj,fitnag.obj,fitunc.obj,flog.obj,franz.obj,franz1d.obj,franz2d.obj,fsin.obj,fsinh.obj,ftan.obj,ftanh.obj,ftcomp.obj,ftcopy.obj,ftcpgv.obj
$ LIB/REPLACE libfit ftcva1.obj,ftcval.obj,ftdcfn.obj,ftddvi.obj,ftddvt.obj,ftdfpr.obj,ftdfun.obj,ftdivi.obj,ftdivt.obj,ftdodo.obj,ftexit.obj,ftext1.obj,ftfunc.obj,ftimag.obj
$ LIB/REPLACE libfit ftinfo.obj,ftini1.obj,ftinif.obj,ftinit.obj,ftintr.obj,ftpars.obj,ftparv.obj,ftrdfn.obj,ftrdin.obj,ftrdpr.obj,ftspar.obj,ftstat.obj,ftsval.obj,fttabl.obj
$ LIB/REPLACE libfit fttntr.obj,fttokn.obj,funct1.obj,funct1i.obj,funct1t.obj,funct2.obj,funct2i.obj,funct2t.obj,funct3.obj,funct4.obj,fitbl.obj
$ LIB/REPLACE libfit gammdis.obj,gauss.obj,gauss1d.obj,gauss2d.obj,moffat.obj,gauss3d.obj,gaussa.obj,igauss.obj,igaussa.obj,laplace.obj,logistic.obj
$ LIB/REPLACE libfit lognorm.obj,lorentz.obj,lsfun1.obj,lsfun1i.obj,lsfun1t.obj,lsfun2.obj,lsfun2i.obj,lsfun2t.obj,lsfun3.obj,lsfun4.obj,lsqgrd.obj,lsqmon.obj,lsqmsg.obj
$ LIB/REPLACE libfit matdiag.obj,minmon.obj,minmsg.obj,mnmx.obj,nullf.obj,pareto.obj,poisson.obj,poly.obj,poly1d.obj,poly2d.obj,poly3d.obj,semilog.obj,setone.obj,setzero.obj
$ LIB/REPLACE libfit sinc.obj,sinc2.obj,triang.obj
$ FORTRAN   user00.for
$ FORTRAN   user01.for
$ FORTRAN   user02.for
$ FORTRAN   user03.for
$ FORTRAN   user04.for
$ FORTRAN   user05.for
$ FORTRAN   user06.for
$ FORTRAN   user07.for
$ FORTRAN   user08.for
$ FORTRAN   user09.for
$ LIB/REPLACE libfituser user00.obj,user01.obj,user02.obj,user03.obj,user04.obj,user05.obj,user06.obj,user07.obj,user08.obj,user09.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
