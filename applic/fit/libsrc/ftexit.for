C===========================================================================
C Copyright (C) 1995,2004 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTEXIT(NAME,ISTAT)                                             
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987,2004 European Southern Observatory,
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C FINISH FIT SESSION                                                            
C CLOSE  DATA FILE                                                              
C CREATE FIT FILE NAME.FIT                                                      
C                                                                               
C.ALGORITHM                                                                     
C  use MIDAS I/O interfaces to frames and tables                                
C                                                                               
C.KEYWORDS                                                                      
C  non linear fitting                                                           
C 
C.VERSION:
C 040524	last modif
C                                                                               
C----------------------------------------------------------------               
C                                                                               
      IMPLICIT NONE                                                            
C                                                                               
C INPUT PARAMETERS                                                              
C NAME CHAR FIT DATA FILE NAME                                                  
C                                                                               
C OUTPUT PARAMETERS                                                             
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      INTEGER DUN,IP,ISTAT,II,I,N1
C 
      CHARACTER*(*) NAME                                                        
      CHARACTER*60 FITNAME                                                      
      CHARACTER WS*5,FITSPEC*7,HTXT*44
C 
      INCLUDE 'MID_INCLUDE:ST_DEF.INC/NOLIST' 
      INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'   
C 
      DOUBLE PRECISION STASTEP(2*FZINDMAX)
C 
      INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'  
      INCLUDE 'MID_INCLUDE:ST_DAT.INC/NOLIST'  
C 
      DATA FITSPEC /'FIT....'/
      DATA HTXT /'start (1-3), step (4-6) of related image '/
C                                                                               
C CLOSE DATA FILE                                                               
C                                                                               
      IF (FZMAPPED.NE.0) THEN                                                 
          IF (FZTYPE.EQ.'BDF ') THEN                                          
              CALL STFCLO(FZIDEN,ISTAT)                                       
          ELSE                                                                  
              CALL TBTCLO(FZIDEN,ISTAT)                                       
          END IF                                                                
          FZMAPPED = 0                                                        
      END IF                                                                    
C                                                                               
C EXIT DATA                                                                     
C                                                                               
      II = INDEX(NAME,' ') - 1                                         
      FITNAME = NAME(1:II)//'.fit'                                              
      CALL STFOPN(FITNAME,D_R4_FORMAT,0,F_FIT_TYPE,IP,ISTAT)
C 
      II = 0                         !just in case write NAXIS = 0
      CALL STDWRI(IP,'NAXIS',II,1,1,DUN,ISTAT)             
      CALL STDWRC(IP,'FITCHAR',1,FZCHAR,1,FZNCHAR,DUN,ISTAT)             
      CALL STDWRI(IP,'FITINTG',FZINTG,1,FZNINTG,DUN,ISTAT)             
      CALL STDWRR(IP,'FITREAL',FZREAL,1,FZNREAL,DUN,ISTAT)           
C 
      DO 100,II=1,FZINDMAX                  !save start, step in FITDOUBLE
         STASTEP(II) = FZSTART(II)
         STASTEP(II+FZINDMAX) = FZSTEP(II)
100   CONTINUE
      N1 = 2 * FZINDMAX
      CALL STDWRD(IP,'FITDOUBLE',STASTEP,1,N1,DUN,ISTAT)         
      CALL STDWRH(IP,'FITDOUBLE',HTXT,1,43,ISTAT)         
C 
      IF (FZNPTOT.EQ.0) THEN
         N1 = 128
      ELSE
         N1 = FZNPTOT
      ENDIF
      CALL STDWRD(IP,'FITPARAM',FZVALUE,1,N1,DUN,ISTAT)         
      CALL STDWRD(IP,'FITERROR',FZERROR,1,N1,DUN,ISTAT)         
      DO 200,I = 1,FZNFUN                                                      
          WRITE (WS,9000) 10000 + I                                             
          FITSPEC(4:7) = WS(2:5)                                                
          CALL STDWRC(IP,FITSPEC,1,FZSPEC(I),1,80,DUN,ISTAT)               
200   CONTINUE  
C 
      CALL STFCLO(IP,ISTAT)                                                
      RETURN                                                                    
C 
9000  FORMAT (I5)  
      END                                                                       
