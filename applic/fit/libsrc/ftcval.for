C  @(#)ftcval.for	19.1 (ESO-DMD) 02/25/03 13:17:33 
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTCVAL(NIN,XVAL,XOUT)                                          
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C COMPUTE FITTED VALUE FOR THE INPUT VARIABLES XVAL                             
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  USE MIDAS I/O INTERFACES TO FRAMES AND TABLES                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  NON LINEAR FITTING                                                           
C                                                                               
C.VERSION
C 021122	last modif
C                                                                               
C----------------------------------------------------------------               
C                                                                               
C      IMPLICIT NONE                                                            
C                                                                               
C INPUT PARAMETERS                                                              
C NIN INTG NO.OF INDEPENDENT VARIABLES                                          
C XVAL REAL INDEPENDENT VARIABLES                                               
C                                                                               
C OUTPUT PARAMETERS                                                             
C XOUT DBLE COMPUTED VALUE, ERROR                                               
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      INTEGER NIN,I,NOFFSET,limi
      REAL XVAL(NIN)                                                            
      DOUBLE PRECISION Y1,XOUT(2)                                               
       INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'                                     
      DOUBLE PRECISION DY(FZPARMAX)                                           
       INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'                                     
                                                                                
      XOUT(1) = 0.D0                                                            
      XOUT(2) = 0.D0                                                            
      NOFFSET = 1                                                               
      DO 10, I = 1,FZNFUN                                                      
          DY(I) = 0.D0
          IF (FZSELE(I).EQ.1) THEN                                            
              CALL FTFUNC(FZFCODE(I),FZNIND,XVAL,FZACTPAR(I),             
     +                    FZVALUE(NOFFSET),Y1,DY(NOFFSET))                    
              XOUT(1) = XOUT(1) + Y1                                            
          END IF                               
          NOFFSET = NOFFSET + FZACTPAR(I)                                     
   10 CONTINUE                                                                  
C 
      IF (FZNPTOT.GT.FZNFUN) THEN		!take the samller value
         LIMI = FZNFUN
      ELSE
         LIMI = FZNPTOT
      ENDIF
      DO 20, I = 1,LIMI  
          XOUT(2) = XOUT(2) + (DY(I)*FZERROR(I))**2                           
   20 CONTINUE                                                                  
      XOUT(2) = DSQRT(XOUT(2))                                                  
      RETURN                                                                    
                                                                                
      END                                                                       
