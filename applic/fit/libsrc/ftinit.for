C===========================================================================
C Copyright (C) 1995-2005 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTINIT(NAME,ISTAT)                                             
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C INIT FIT SESSION                                                              
C READ FIT FILE NAME.FIT                                                        
C                                                                               
C.ALGORITHM                                                                     
C  use MIDAS I/O interfaces to frames and tables                                
C                                                                               
C.KEYWORDS                                                                      
C  non linear fitting                                                           
C                                                                               
C.VERSION
C 051109	last modif
C 
C----------------------------------------------------------------               
C                                                                               
C                                                                               
C INPUT PARAMETERS                                                              
C NAME CHAR FIT DATA FILE NAME                                                  
C                                                                               
C OUTPUT PARAMETERS                                                             
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      IMPLICIT NONE
C 
      INTEGER ISTAT,II,IP,NACT,NI,NR,N1,I1
      INTEGER EUC,EUL,EUD,I,DNUL,DUN,ACT
C 
      CHARACTER*(*) NAME                                                        
      CHARACTER WS*5,FITSPEC*7
      CHARACTER*60 FITNAME                                                      
C 
      INCLUDE 'MID_INCLUDE:ST_DEF.INC/NOLIST'
      INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'                                     
C
      DOUBLE PRECISION STASTEP(2*FZINDMAX)
C
      INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'                                     
      INCLUDE 'MID_INCLUDE:ST_DAT.INC/NOLIST'
      DATA FITSPEC/'FIT....'/                                       
C                                                                               
C READ DATA                                                                     
C                                                                               
      II     = INDEX(NAME,' ') - 1                                         
      FITNAME = NAME(1:II)//'.fit'                                              
      CALL STFOPN(FITNAME,D_R4_FORMAT,0,F_FIT_TYPE,IP,ISTAT)
      IF (FZTYPE(1:1).EQ.' ') THEN                                            
          CALL STDRDC(IP,'FITCHAR',1,1, 
     +         FZNCHAR,NACT,FZCHAR,DNUL,DUN,ISTAT) 
          NI     = FZNINTG                                                    
          NR     = FZNREAL                                                    
      ELSE                                                                      
          NI     = FZCINTG                                                    
          NR     = FZCREAL                                                    
      END IF                                                                    
      CALL STDRDI(IP,'FITINTG',1,NI,ACT,FZINTG,DUN,DNUL,ISTAT)                  
      CALL STDRDR(IP,'FITREAL',1,NR,ACT,FZREAL,DUN,DNUL,ISTAT)                
C 
      IF (FZNPTOT.EQ.0) THEN
         N1 = 128
      ELSE
         N1 = FZNPTOT
      ENDIF
      CALL STDRDD(IP,'FITPARAM',1,N1,ACT,FZVALUE,DUN,DNUL,ISTAT)
      CALL STDRDD(IP,'FITERROR',1,N1,ACT,FZERROR,DUN,DNUL,ISTAT)
C 
      CALL STECNT('GET',EUC,EUL,EUD)                                            
      CALL STECNT('PUT',1,0,0)                                                  
      N1 = 2 * FZINDMAX
      CALL STDRDD(IP,'FITDOUBLE',1,N1,ACT,STASTEP,DUN,DNUL,ISTAT)
      IF (ISTAT.NE.0) THEN                                                
         DO 90,II=1,FZINDMAX 
            FZSTART(II) = 0.D0
            FZSTEP(II) = 1.D0
90       CONTINUE                                                              
      ELSE
         DO 100,II=1,FZINDMAX                  !get start, step from FITDOUBLE
            FZSTART(II) = STASTEP(II)
            FZSTEP(II) = STASTEP(II+FZINDMAX)
100      CONTINUE
      END IF                                                                    
C 
      CALL STDRDI(IP,'FITSELE',1,FZFUNMAX,ACT,FZSELE,DUN,DNUL,ISTAT) 
      CALL STECNT('PUT',EUC,EUL,EUD)                                            
      IF (ISTAT.NE.0) THEN                                                
         DO 1000,I = 1,FZFUNMAX 
            FZSELE(I) = 1                                                   
1000     CONTINUE                                                              
      END IF                                                                    
C                                                                               
C ASSIGN FUNCTION NAMES                                                         
C                                                                               
      DO 2000,I = FZNFUN + 1,FZFUNMAX                                         
         FZSPEC(I) = ' '                                                     
2000  CONTINUE                                                                  
      FZNPTOT = 0                                                             
      I1     = FZNFUN                                                         
      FZNFUN = 0                                                              
      DO 3000,I = 1,I1  
         WRITE (WS,9000) 10000 + I                                             
         FITSPEC(4:7) = WS(2:5)                                                
         CALL STDRDC(IP,FITSPEC,1,1,80,ACT,FZSPEC(I),DNUL,
     +               DUN,ISTAT)            
         CALL FTDFUN(I,FZSPEC(I),ISTAT)                                      
3000  CONTINUE                                                                  
      CALL FTPARV(ISTAT)                                                        
      RETURN                                                                    
C 
9000  FORMAT (I5)                                                               
      END                                                                       
