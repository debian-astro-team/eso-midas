C===========================================================================
C Copyright (C) 1995,2004 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTEXT1(NAME,ISTAT)                                             
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987,2004 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C FINISH FIT SESSION                                                            
C CLOSE  DATA FILE                                                              
C CREATE FIT FILE NAME.FIT                                                      
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  use MIDAS I/O interfaces to frames and tables                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  non linear fitting                                                           
C                                                                               
C.VERSION
C 040210	last modif
C 
C----------------------------------------------------------------               
C                                                                               
C                                                                               
C INPUT PARAMETERS                                                              
C NAME CHAR FIT DATA FILE NAME                                                  
C                                                                               
C OUTPUT PARAMETERS                                                             
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      INTEGER IP,DUN, N1,ISTAT,I,II
      CHARACTER*(*) NAME                                                        
      CHARACTER*60 FITNAME                                                      
      CHARACTER WS*5,FITSPEC*7
      CHARACTER*5 TYPE                                                          
       INCLUDE 'MID_INCLUDE:ST_DEF.INC/NOLIST'                    
       INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'                    
       INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'                          
       INCLUDE 'MID_INCLUDE:ST_DAT.INC/NOLIST'                    
      DATA FITSPEC/'FIT....'/                                       
      DATA TYPE/'R*4 '/                                                         
C                                                                               
C CLOSE DATA FILE                                                               
C                                                                               
      IF (FZMAPPED.NE.0) THEN                                                 
          IF (FZTYPE.EQ.'BDF ') THEN                                          
              CALL STFCLO(FZIDEN,ISTAT)                                       
                                                                                
          ELSE                                                                  
              CALL TBTCLO(FZIDEN,ISTAT)                                       
          END IF                                                                
                                                                                
          FZMAPPED = 0                                                        
      END IF                                                                    
C                                                                               
C EXIT DATA                                                                     
C                                                                               
      II     = INDEX(NAME,' ') - 1                                         
      FITNAME = NAME(1:II)//'.fit'                                              
      CALL STFCRE(FITNAME,D_R4_FORMAT,F_O_MODE,F_FIT_TYPE,1,IP,ISTAT)
C
      II = 0                         !NAXIS = 0 needed for FITS output
      CALL STDWRI(IP,'NAXIS',II,1,1,DUN,ISTAT)
      CALL STDWRC(IP,'FITCHAR',1,FZCHAR,1,FZNCHAR,DUN,ISTAT)             
      CALL STDWRI(IP,'FITINTG',FZINTG,1,FZNINTG,DUN,ISTAT)             
      CALL STDWRR(IP,'FITREAL',FZREAL,1,FZNREAL,DUN,ISTAT)           
      IF (FZNPTOT .EQ. 0) THEN
          N1 = 128
      ELSE
          N1 = FZNPTOT
      ENDIF
      CALL STDWRD(IP,'FITPARAM',FZVALUE,1,N1,DUN,ISTAT)         
      CALL STDWRD(IP,'FITERROR',FZERROR,1,N1,DUN,ISTAT)         
      DO 10 I = 1,FZNFUN                                                      
          WRITE (WS,9000) 10000 + I                                             
          FITSPEC(4:7) = WS(2:5)                                                
          CALL STDWRC(IP,FITSPEC,1,FZSPEC(I),1,80,DUN,ISTAT)               
   10 CONTINUE                                                                  
      CALL STFCLO(IP,ISTAT)                                                
      RETURN                                                                    
                                                                                
 9000 FORMAT (I5)                                                               
      END                                                                       
