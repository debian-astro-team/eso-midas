C @(#)dipole.for	19.1 (ES0-DMD) 02/25/03 13:17:29
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE DIPOLE(NIND,X,NPAR,PARAM,Y,DERIV)                              
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                  
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C--------------------------------------------------------------                 
C***************************************************                            
C                                                                               
C      IMPLICIT NONE                                                            
C FIT WITH A LINEAR INFALL MODEL                                                
C                                                                               
C Fit the observed radial velocities (in any                                    
C reference system) with a dipole velocity                                      
C field with respect to this reference                                          
C                                                                               
C * * * * * * * * * * * * * * * * * * * * * * * * *                             
C                                                                               
C Independent variables:                                                        
C                                                                               
C  X(1): Galactic longitude                                                     
C  X(2): Galactic latitude                                                      
C                                                                               
C * * * * * * * * * * * * * * * * * * * * * * * * *                             
C                                                                               
C Free parameters:                                                              
C                                                                               
C  PARAM(1): Solar motion (in km/s)                                             
C  PARAM(2): Galactic longitude of Apex                                         
C  PARAM(3): Galactic latitude of Apex                                          
C                                                                               
C (All angles are in degrees !)                                                 
C                                                                               
C * * * * * * * * * * * * * * * * * * * * * * * * *                             
C                                                                               
C Author: O.-G. Richter, ESO Garching                                           
C                                                                               
      INTEGER NIND,NPAR
      REAL X(NIND),RAD,D1,A1,A2,D2,BC,BS,A                                      
      DOUBLE PRECISION Y,PARAM(NPAR),DERIV(NPAR)                                
      DATA RAD/1.74532925E-2/                                                   
C  !   Galactic latitude of object                                              
      D1     = X(2)*RAD  !   Galactic latitude of apex                          
      D2     = PARAM(3)*RAD                                                     
      A1     = COS(D1)                                                          
      A2     = COS(D2)                                                          
      D1     = SIN(D1)                                                          
      D2     = SIN(D2)                                                          
      A      = A1*A2  !   Difference in galactic longitude                      
      BS     = (PARAM(2)-X(1))*RAD                                              
      BC     = COS(BS)                                                          
      DERIV(1) = -D1*D2 - A*BC  !   Predicted radial velocity                   
      Y      = DERIV(1)*PARAM(1)                                                
      DERIV(2) = PARAM(1)*A*SIN(BS)*RAD                                         
      DERIV(3) = PARAM(1)* (D2*A1*BC-A2*D1)*RAD                                 
C                                                                               
      RETURN                                                                    
                                                                                
      END                                                                       
