C @(#)moffat.for	19.1 (ES0-DMD) 02/25/03 13:17:42
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE MOFFAT(X,NP,PARAM,Y,DERIV)
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,
C                                         all rights reserved
C
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: P.Magain, M.Remy, M.Peron 
C
C--------------------------------------------------------------
C**********************************************
C
C      IMPLICIT NONE
C 2-D Moffat 
C
C**********************************************
C
C
      INTEGER NP
      DOUBLE PRECISION PARAM(NP),DERIV(NP)
      DOUBLE PRECISION XX(2),F,Y
      REAL X(2)


      XX(1)=(X(1)-PARAM(2))/PARAM(4)
      XX(2)=(X(2)-PARAM(3))/PARAM(5)
      F=(XX(1)*XX(1)+XX(2)*XX(2)-2.*PARAM(6)*XX(1)*XX(2))+1.
      DERIV(1)=EXP(-PARAM(7)*LOG(F))
      Y=PARAM(1)*DERIV(1)
      DERIV(2)=Y*2./F*PARAM(7)*(XX(1)-PARAM(6)*XX(2))/PARAM(4)
      DERIV(3)=Y*2./F*PARAM(7)*(XX(2)-PARAM(6)*XX(1))/PARAM(5)
      DERIV(4)=DERIV(2)*XX(1)
      DERIV(5)=DERIV(3)*XX(2)
      DERIV(6)=2.*Y/F*PARAM(7)*XX(1)*XX(2)
      DERIV(7)=-Y*LOG(F)
      RETURN
      END

