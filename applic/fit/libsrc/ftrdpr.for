C @(#)ftrdpr.for	19.1 (ES0-DMD) 02/25/03 13:17:36
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTRDPR(ISEQ,NAME,NPAR,PAR,ERR,FIX,ISTAT)                       
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C READ FUNCTION PARAMETERS, ACCESS BY SEQ.NO.                                   
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  USE MIDAS I/O INTERFACES TO FRAMES AND TABLES                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  NON LINEAR FITTING                                                           
C                                                                               
C                                                                               
C----------------------------------------------------------------               
C                                                                               
C                                                                               
C INPUT PARAMETERS                                                              
C ISEQ INTG SEQ.NO. OF THE FUNCTION                                             
C                                                                               
C OUTPUT PARAMETERS                                                             
C NAME CHAR FUNCTION NAME                                                       
C NPAR INTG NUMBER OF FUNCTION PARAMETERS                                       
C PAR DBLE ARRAY WITH PARAMETER VALUES                                          
C ERR DBLE ARRAY WITH ASSOCIATED ERRORS                                         
C FIX INTG ARRAY WITH FIXED FLAG (0    - CONSTANT)                              
C                         (>0   - CONSTR.POINTER)                               
C                  (<0   - VARIABLE)                                            
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      CHARACTER*(*) NAME                                                        
      INTEGER FIX(1),I,ISTAT,NPAR,NOFFSET,I1,ISEQ                                                            
      DOUBLE PRECISION PAR(1)                                                   
      DOUBLE PRECISION ERR(1)                                                   
       INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'                                     
       INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'                                     
      NAME   = FZFNAM(FZFCODE(ISEQ))                                        
      CALL FORUPC(NAME,NAME)
      NPAR   = FZACTPAR(ISEQ)                                                 
      NOFFSET = 0                                                               
      DO 10 I = 1,ISEQ - 1                                                      
          NOFFSET = NOFFSET + FZACTPAR(I)                                     
   10 CONTINUE                                                                  
      DO 20 I = 1,NPAR                                                          
          I1     = I + NOFFSET                                                  
          PAR(I) = FZVALUE(I1)                                                
          FIX(I) = FZFIXED(I1)                                                
          ERR(I) = FZERROR(I1)                                                
   20 CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
