C @(#)gauss2d.for	19.1 (ES0-DMD) 02/25/03 13:17:39
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE GAUSS2(X,NP,PARAM,Y1,DERIV)                                   
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                  
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C--------------------------------------------------------------                 
C**********************************************                                 
C                                                                               
C      IMPLICIT NONE                                                            
C 2-D GAUSSIAN                                                                  
C                                                                               
C**********************************************                                 
C                                                                               
C Author: O.-G. Richter, ESO Garching                                           
C                                                                               
      INTEGER NP
      DOUBLE PRECISION Y1,PARAM(NP),DERIV(NP),A,A1,B,C,D,E,F                    
      REAL X(2)                                                            
                                                                                
      A1     = 1.0D0 - PARAM(6)*PARAM(6)                                        
      A      = 0.631471806D0/A1                                                 
      B      = (X(1)-PARAM(2))/PARAM(4)                                         
      C      = (X(2)-PARAM(3))/PARAM(5)                                         
      D      = B*C                                                              
      E      = B*B + C*C - 2.0D0*PARAM(6)*D                                     
      F      = DEXP(-A*E)                                                       
      DERIV(1) = F                                                              
      F      = PARAM(1)*F                                                       
      Y1     = F                                                                
      F      = 2.D0*A*F                                                         
      DERIV(2) = F* (B-PARAM(6)*C)/PARAM(4)                                     
      DERIV(3) = F* (C-PARAM(6)*B)/PARAM(5)                                     
      DERIV(4) = DERIV(2)*B                                                     
      DERIV(5) = DERIV(3)*C                                                     
      DERIV(6) = F* (D*A1-PARAM(6)*E)/A1                                        
      RETURN                                                                    
                                                                                
      END                                                                       
