C @(#)lsfun3.for	19.1 (ES0-DMD) 02/25/03 13:17:41
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE LSFUN3(IFLAG,NRRES,NRPRM,PRM,FCTVEC,IW,LIW,W,LW)               
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
C                                                                               
C.MODULE                                                                        
C       FIT                                                                     
C                                                                               
C.NAME                                                                          
C       LSFUN3                                                                  
C                                                                               
C.PURPOSE                                                                       
C   Function to compute the approximating function at each point.               
C   Pass the control to different routines according to IMAGE or TABLE          
C                                                                               
C.KEYWORDS                                                                      
C       Approximating Function.                                                 
C                                                                               
C.DESCRIPTION                                                                   
C       Test the type of FIT and go to respective routines                      
C                                                                               
C.LANGUAGE                                                                      
C       FORTRAN                                                                 
C                                                                               
C.CALLING SEQUENCE                                                              
C      CALL LSFUN3(IFLAG,NRRES,NRPRM,PRM,FCTVEC,IW,LIW,W,LW)                    
C                                                                               
C.INPUT PARAMETERS                                                              
C       IFLAG               INTEGER  unused                                     
C       NRRES               INTEGER  Number of residuals                        
C       NRPRM             INTEGER  Number of parameters                         
C       PRM (NRPRM)       DOUBLE   Parameters                                   
C       IW (LIW)            INTEGER  unused                                     
C       LIW                 INTEGER                                             
C       W (LW)              DOUBLE                                              
C       LW                  INTEGER                                             
C                                                                               
C.MODIFIED PARAMETERS                                                           
C       none                                                                    
C                                                                               
C.OUTPUT PARAMETERS                                                             
C       FCTVEC (NRRES)      DOUBLE   Approximating values                       
C                                                                               
C.FILES                                                                         
C       FIT_NAG.INC/NOLIST                                                      
C                                                                               
C.MODULES CALLED                                                                
C       LSFUN1_T                                                                
C       LSFUN1_I                                                                
C                                                                               
C.AUTHOR                                                                        
C       Ph. DEFERT,      Feb 1986                                               
C                                                                               
C.MODIFICATIONS                                                                 
C                                                                               
C                                                                               
C-----------------------------------------------------------------------        
C                                                                               
C      IMPLICIT NONE                                                            
C     .. Scalar Arguments ..                                                    
      INTEGER IFLAG,LIW,LW,NRPRM,NRRES,IW(LIW),MADRID(1) 
C     ..                                                                        
C     .. Array Arguments ..                                                     
      DOUBLE PRECISION FCTVEC(NRRES),PRM(NRPRM),W(LW)                   
C     ..                                                                        
C     .. External Files ..                                                      
       INCLUDE 'MID_INCLUDE:FITNAGI.INC/NOLIST'                                 
      COMMON /VMR/MADRID                                 
       INCLUDE 'MID_INCLUDE:FITNAGC.INC/NOLIST'                                 

C     ..                                                                        
C     .. Executable Statements ..                                               
                                                                                
      IF (FILTYP(1:1).EQ.'T') THEN                                              
          CALL LSFU1T(NRRES,NRPRM,PRM,FCTVEC)                                 
                                                                                
      ELSE                                                                      
          CALL LSFU1I(NRRES,NRPRM,PRM,FCTVEC,MADRID(PTRI),                    
     +                  MADRID(PTRM),NRPIX(1),NRPIX(2),NRPIX(3))                
      END IF                                                                    
                                                                                
      RETURN                                                                    
                                                                                
      END                                                                       
