C @(#)user03.for	19.1 (ES0-DMD) 02/25/03 13:17:45
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE USER03(IND,X,NP,PARAM,Y1,DERIV)                                
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                 
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  16:03 - 20 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.Ponz                                                              
C                                                                               
C--------------------------------------------------------------                 
C                                                                               
C NULL ROUTINE                                                                  
C      IMPLICIT NONE                                                            
C                                                                               
      INTEGER NP,I,IND,ISTAT
      DOUBLE PRECISION Y1,PARAM(NP),DERIV(NP)                                   
      REAL X(1)                                                                 
                                                                                
      Y1     = 0.D0                                                             
      DO 10 I = 1,NP                                                           
          DERIV(I) = 0.D0                                                      
   10 CONTINUE                                                                 
      CALL STTPUT('Sorry but I am a dummy user03 routine...',ISTAT)
      CALL STSEPI

      RETURN                                                                    
                                                                                
      END                                                                       
