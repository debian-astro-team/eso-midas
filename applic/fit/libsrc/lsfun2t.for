C @(#)lsfun2t.for	19.1 (ES0-DMD) 02/25/03 13:17:41
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C @(#)lsfun2t.for	2.1.1.1 (ESO-IPG) 9/27/91 18:10:08
      SUBROUTINE LSFU2T(NRRES,NRPRM,PRM,FCTVEC,FCTJAC,LJAC)                   
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
C                                                                               
C.MODULE                                                                        
C       FIT                                                                     
C                                                                               
C.NAME                                                                          
C       LSFUN2_T                                                                
C                                                                               
C.PURPOSE                                                                       
C   Function to compute the approximating function and the derivatives          
C   at each point for FIT/TABLE                                                 
C                                                                               
C.KEYWORDS                                                                      
C       Approximating Function.                                                 
C                                                                               
C.DESCRIPTION                                                                   
C       Test the type of FIT and go to respective routines                      
C                                                                               
C.LANGUAGE                                                                      
C       FORTRAN                                                                 
C                                                                               
C.CALLING SEQUENCE                                                              
C      CALL LSFUN2_T(NRRES,NRPRM,PRM,FCTVEC,FCTJAC,LJAC)                        
C                                                                               
C.INPUT PARAMETERS                                                              
C       NRRES               INTEGER  Number of residuals                        
C       NRPRM             INTEGER  Number of parameters                         
C       PRM (NRPRM)       DOUBLE   Parameters                                   
C       LJAC                INTEGER  1st dimension of jacobian                  
C                                                                               
C.MODIFIED PARAMETERS                                                           
C       none                                                                    
C                                                                               
C.OUTPUT PARAMETERS                                                             
C       FCTVEC (NRRES)      DOUBLE   Approximating values                       
C       FCTJAC (LJAC,NRRES) DOUBLE   values of the jacobian                     
C                                                                               
C.FILES                                                                         
C       FIT_NAG.INC/NOLIST                                                      
C                                                                               
C.MODULES CALLED                                                                
C       FTFUNC                                                                  
C                                                                               
C.AUTHOR                                                                        
C       Ph. DEFERT,      Feb 1986                                               
C                                                                               
C.MODIFICATIONS                                                                 
C       M.Peron apr92 fit on tables for Apollo and VAX                     
C                                                                               
C-----------------------------------------------------------------------        
C                                                                               
C      IMPLICIT NONE                                                            
C     .. Scalar Arguments ..                                                    
      INTEGER LJAC,NRPRM,NRRES                                                  
C     ..                                                                        
C     .. Array Arguments ..                                                     
      DOUBLE PRECISION FCTJAC(LJAC,NRPRM),FCTVEC(NRRES),PRM(NRPRM)              
C     ..                                                                        
C     .. Scalars in Common ..                                                   
      INTEGER NRCOL,ISTAR                                                       
      CHARACTER WGTTYP*1                                                        
C     ..                                                                        
C     .. Arrays in Common ..                                                    
      INTEGER ICOL(10)                                                          
C     ..                                                                        
C     .. Local Scalars ..                                                       
      DOUBLE PRECISION W,Y,Y1,YOUT,YY                                           
      INTEGER IDAT,IFUN,IPNT,ISTAT,K,NC,NFJAC,NROW,NS,BEGFCT,IP
      INTEGER ACOL,AROW                 
      LOGICAL ISEL,VALID                                                        
C     ..                                                                        
C     .. Local Arrays ..                                                        
      DOUBLE PRECISION DUMMY(10)                                                
      REAL XVAL(10),X(10)                                                       
      LOGICAL NULL(10)                                                          
C     ..                                                                        
C     .. Common blocks ..                                                       
C     ..                                                                        
C     .. External Files ..                                                      
       INCLUDE 'MID_INCLUDE:FITNAGI.INC/NOLIST'                                 
       INCLUDE 'MID_INCLUDE:FITNAGC.INC/NOLIST'                                 
      COMMON /LSQFUN/ICOL,NRCOL,ISTAR,WGTTYP                                    
C     ..                                                                        
C     .. Executable Statements ..                                               
C                                                                               
C   deal with simple linear constraints                                         
C                                                                               
      DO 10 K = 1,NRPRM                                                         
          IP     = FIXPAR(K)                                                    
          IF (IP.EQ.0) PRM(K) = PARINI(K)                                       
          IF (IP.GT.0) THEN                                                     
              PRM(K) = PRM(IP)*PRPFAC(K)                                        
              PARAM(K) = PRM(K)                                                 
          END IF                                                                
                                                                                
   10 CONTINUE                                                                  
C                                                                               
C   Go through the table                                                        
C                                                                               
      CALL TBIGET(FZIDEN,NC,NROW,NS,ACOL,AROW,ISTAT)                        
      IPNT   = 0                                                                
      DO 60 IDAT = 1,NROW                                                       
          CALL TBSGET(FZIDEN,IDAT,ISEL,ISTAT)                                   
          IF ( .NOT. ISEL) GO TO 60                                             
          CALL TBRRDR(FZIDEN,IDAT,NRCOL,ICOL(ISTAR),XVAL(ISTAR),    
     +                NULL(ISTAR),ISTAT)                                      
          VALID  = ISEL                                                         
          DO 20 K = 1,NRCOL                                                     
              VALID  = VALID .AND. ( .NOT. NULL(K+ISTAR-1))                             
   20     CONTINUE                                                              
          IF ( .NOT. VALID) GO TO 60                                            
          Y      = XVAL(2)                                                      
          W      = XVAL(1)                                                      
          DO 30 K = 1,NRCOL                                                     
              X(K)   = XVAL(K+2)                                                
   30     CONTINUE                                                              
          IPNT   = IPNT + 1                                                     
C                                                                               
C  compute weights                                                              
C                                                                               
          IF (WGTTYP(1:1).NE.'W') THEN                                          
              IF (WGTTYP(1:1).EQ.'C') THEN                                      
                  W      = 1.                                                   
                                                                                
              ELSE IF (WGTTYP(1:1).EQ.'S') THEN                                 
                  YY     = ABS(Y)                                               
                  IF (YY.LT.1.D-12) THEN                                        
                      W      = 1.                                               
                                                                                
                  ELSE                                                          
                      W      = 1./YY                                            
                  END IF                                                        
                                                                                
              ELSE IF (WGTTYP(1:1).EQ.'I') THEN                                 
                  W      = 1./W**2                                                 
              END IF                                                            
                                                                                
          END IF                                                                
C                                                                               
C   Compute fitting values                                                      
C                                                                               
          Y1     = 0.D0                                                         
          BEGFCT = 1                                                            
          DO 50 IFUN = 1,NRFUN                                                  
              CALL FTFUNC(FCTCOD(IFUN),NRIND,X,ACTPAR(IFUN),PRM(BEGFCT),        
     +                    YOUT,DUMMY)                                           
              Y1     = Y1 + YOUT                                                
              DO 40 NFJAC = 1,ACTPAR(IFUN)                                      
                  K      = BEGFCT + NFJAC - 1                                   
                  FCTJAC(IPNT,K) = SQRT(W)*DUMMY(NFJAC)                         
   40         CONTINUE                                                          
                                                                                
              BEGFCT = BEGFCT + ACTPAR(IFUN)                                    
   50     CONTINUE                                                              
                                                                                
          FCTVEC(IPNT) = SQRT(W)* (Y1-Y)                                        
   60 CONTINUE                                                                  
C                                                                               
C   correct derivatives in case of linear simple constraints                    
C                                                                               
      DO 90 K = 1,NRPRM                                                         
          IP     = FIXPAR(K)                                                    
          IF (IP.EQ.0) THEN                                                     
              DO 70 IPNT = 1,NRRES                                              
                  FCTJAC(IPNT,K) = 0.D0                                         
   70         CONTINUE                                                          
                                                                                
          ELSE IF (IP.GT.0) THEN                                                
              DO 80 IPNT = 1,NRRES                                              
                  FCTJAC(IPNT,IP) = FCTJAC(IPNT,IP) +                           
     +                              FCTJAC(IPNT,K)/PRPFAC(K)                    
                  FCTJAC(IPNT,K) = 0.D0                                         
   80         CONTINUE                                                          
          END IF                                                                
                                                                                
   90 CONTINUE                                                                  
      RETURN                                                                    
                                                                                
      END                                                                       

