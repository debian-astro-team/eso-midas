C @(#)fitbl.for	19.1 (ES0-DMD) 02/25/03 13:17:30
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
       SUBROUTINE FITBL
       INCLUDE 'MID_INCLUDE:FITI.INC'
       INCLUDE 'MID_INCLUDE:FITC.INC'
      
       FZFNAM(1) = 'POLY'
       FZFNAM(2) = 'LOG'
       FZFNAM(3) = 'EXP'
       FZFNAM(4) = 'SIN'
       FZFNAM(5) = 'TAN'
       FZFNAM(6) = 'SINH'
       FZFNAM(7) = 'TANH'
       FZFNAM(8) = 'ASIN'
       FZFNAM(9) = 'ATAN'
       FZFNAM(10) = 'ASINH'
       FZFNAM(11) = 'ATANH'
       FZFNAM(12) = 'BOX'
       FZFNAM(13) = 'TRIANG'
       FZFNAM(14) = 'POISSON'
       FZFNAM(15) = 'GAUSS'
       FZFNAM(16) = 'CAUCHY'
       FZFNAM(17) = 'LORENTZ'
       FZFNAM(18) = 'VOIGT'
       FZFNAM(19) = 'LOGPROF'
       FZFNAM(20) = 'EXPPROF' 
       FZFNAM(21) = 'FEXPINT'
       FZFNAM(22) = 'ERF'
       FZFNAM(23) = 'SINC'
       FZFNAM(24) = 'SINCS'
       FZFNAM(25) = 'FRANZ'
       FZFNAM(26) = 'HUBBLE'
       FZFNAM(27) = 'KING'
       FZFNAM(28) = 'RQUART'
       FZFNAM(29) = 'BRAND'
       FZFNAM(30) = 'USER00'
       FZFNAM(31) = 'USER01'
       FZFNAM(32) = 'USER02'
       FZFNAM(33) = 'USER03'
       FZFNAM(34) = 'USER04'
       FZFNAM(35) = 'USER05'
       FZFNAM(36) = 'USER06'
       FZFNAM(37) = 'USER07'
       FZFNAM(38) = 'USER08'
       FZFNAM(39) = 'USER09'
       FZFNAM(40) = 'LAPLACE'
       FZFNAM(41) = 'LOGISTIC'
       FZFNAM(42) = 'SEMILOG'
       FZFNAM(43) = 'LOGNORM'
       FZFNAM(44) = 'PARETO'
       FZFNAM(45) = 'GAMMA'
       FZFNAM(46) = 'DIPOLE'
       FZFNAM(47) = 'IGAUSS'
       FZFNAM(48) = 'GAUSSA'
       FZFNAM(49) = 'IGAUSSA'
       FZFNAM(50) = 'MOFFAT'
       FZFPAR(1) = 0
       FZFPAR(2) = 3
       FZFPAR(3) = 3
       FZFPAR(4) = 3
       FZFPAR(5) = 3
       FZFPAR(6) = 3
       FZFPAR(7) = 3
       FZFPAR(8) = 3
       FZFPAR(9) = 3
       FZFPAR(10) = 3
       FZFPAR(11) = 3
       FZFPAR(12) = 3
       FZFPAR(13) = 3
       FZFPAR(14) = 2
       FZFPAR(15) = 0
       FZFPAR(16) = 3
       FZFPAR(17) = 4
       FZFPAR(18) = 3
       FZFPAR(19) = 3
       FZFPAR(20) = 3
       FZFPAR(21) = 3
       FZFPAR(22) = 2
       FZFPAR(23) = 3
       FZFPAR(24) = 3
       FZFPAR(25) = 0
       FZFPAR(26) = 3
       FZFPAR(27) = 3
       FZFPAR(28) = 3
       FZFPAR(29) = 3
       FZFPAR(30) = 0
       FZFPAR(31) = 0
       FZFPAR(32) = 0
       FZFPAR(33) = 0
       FZFPAR(34) = 0
       FZFPAR(35) = 0
       FZFPAR(36) = 0
       FZFPAR(37) = 0
       FZFPAR(38) = 0
       FZFPAR(39) = 0
       FZFPAR(40) = 3
       FZFPAR(41) = 3
       FZFPAR(42) = 3
       FZFPAR(43) = 4
       FZFPAR(44) = 4
       FZFPAR(45) = 4
       FZFPAR(46) = 3
       FZFPAR(47) = 3
       FZFPAR(48) = 0
       FZFPAR(49) = 3
       FZFPAR(50) = 7
       FERPAR = -600 
       FERFUN = -601       ! non defined function name
       FERTAB = -602       ! inp/out on different tables
       FERDAT = -603       ! data not available
       FERPFU = -604       ! error in function params.
       FERDUP = -605       ! duplicated param name
       FERCON = -606
       FZPPRN = 21
       FZIWGT = 13
       FZIBND = 14
       FZIMET = 15
       FZNAME =' '
       FZTYPE = ' '

       END
