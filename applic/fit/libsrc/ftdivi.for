C  @(#)ftdivi.for	19.1 (ESO-DMD) 02/25/03 13:17:33 
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTDIVI(REFIMA,ISTAT)                                           
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C DEFINE NEW INDEPENDENT VARIABLES BASED ON THE REFERENCE IMAGE                 
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  USE MIDAS I/O INTERFACES TO FRAMES AND TABLES                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  NON LINEAR FITTING                                                           
C                                                                               
C.VERSION
C 901025	M PeroN                                                 
C 021122	last modif
C 
C----------------------------------------------------------------               
C                                                                               
C                                                                               
C INPUT PARAMETERS                                                              
C REFIMA CHAR REFERENCE IMAGE                                                   
C                                                                               
C OUTPUT PARAMETERS                                                             
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      IMPLICIT NONE                                                            

      CHARACTER*(*) REFIMA                                                      
      CHARACTER*72 IDENT,UNIT                                                   

      INTEGER NPIX(3)  
      INTEGER INO,ISTAT,I,NAXIS
      INTEGER*8 IPTR
C 
       INCLUDE 'MID_INCLUDE:ST_DEF.INC/NOLIST'  
       INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'   
       INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'  
       INCLUDE 'MID_INCLUDE:ST_DAT.INC/NOLIST' 
C                                                                               
C READ REFERENCE IMAGE                                                          
C                                                                               
      CALL STIGET(REFIMA,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,
     .     3,NAXIS,NPIX,FZSTART,FZSTEP,IDENT,UNIT,IPTR,INO,ISTAT)        
      IF (NAXIS.NE.FZNIND .AND. FZNIND.NE.0) THEN                           
          ISTAT  = FERPAR                                                   
          RETURN                                                                
      END IF                                                                    
      FZRIMA = REFIMA                                                         
      FZNIND = NAXIS                                                          
      DO 10, I = 1,3 
          FZIVAR(I) = I                                                       
          FZNPIX(I) = MAX(1,NPIX(I))                                          
   10 CONTINUE                                                                  
      FZTYPE = 'BDF '                                                         
      RETURN                                                                    
      END                                                                       
