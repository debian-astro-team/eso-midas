C @(#)laplace.for	19.1 (ES0-DMD) 02/25/03 13:17:39
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE LAPLAC(IND,X,NP,P,Y1,D)                                       
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                  
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C--------------------------------------------------------------                 
C                                                                               
C LAPLACE DISTRIBUTION                                                          
C      IMPLICIT NONE                                                            
C                                                                               
C Y1 = P(1)*EXP(-LN(2)*ABS(X-P(2))/P(3))                                        
C                                                                               
      INTEGER NP,IND
      DOUBLE PRECISION Y1,P(NP),D(NP),A,B,C                                     
      REAL X(1)                                                                 
C                                                                               
      A      = X(1) - P(2)                                                      
      B      = 0.6931471806/P(3)                                                
      C      = B*DABS(A)                                                        
      D(1)   = DEXP(-C)                                                         
      Y1     = D(1)*P(1)                                                        
      D(2)   = DSIGN(Y1*B,A)                                                    
      D(3)   = Y1*C/P(3)                                                        
      RETURN                                                                    
                                                                                
      END                                                                       
