C @(#)ftdfpr.for	19.1 (ES0-DMD) 02/25/03 13:17:33
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTDFPR(PARAM,LEN,VAL,LENV,ISTAT)                               
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C DEFINES NEW PARAMETER GUESS                                                   
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  USE MIDAS I/O INTERFACES TO FRAMES AND TABLES                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  NON LINEAR FITTING                                                           
C                                                                               
C                                                                               
C----------------------------------------------------------------               
C                                                                               
C      IMPLICIT NONE                                                            
C                                                                               
C INPUT ARGUMENTS                                                               
C PARAM CHAR PARAMETER NAME                                                     
C LEN INTG PARAMETER LENGTH                                                     
C VAL CHAR PARAMETER GUESS                                                      
C LENV INTG VALUE LENGTH                                                        
C                                                                               
C OUTPUT ARGUMENTS                                                              
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      INTEGER LEN,LENV,ISTAT,I,J,II,II1,L,IFUN,IOFF
      CHARACTER*(*) PARAM,VAL                                                   
      CHARACTER NAME*12,FSPEC*80,CVAL*30                                        
       INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'                                     
       INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'                                     
C                                                                               
C SEARCH FOR PARAMETER (I IS THE PARAMETER NUMBER)                              
C                                                                               
      DO 10 I = 1,FZNPTOT                                                     
          NAME   = FZPTOKEN(I)                                                
          IF (NAME(1:FZPLEN(I)).EQ.PARAM(1:LEN)) GO TO 20                     
   10 CONTINUE                                                                  
      ISTAT  = FERPFU                                                       
      RETURN                                                                    
C                                                                               
C FIND FUNCTION  (J IS THE FUNCTION NUMBER)                                     
C                                                                               
   20 CONTINUE                                                                  
      IF (FZFIXED(I).GT.0) THEN                                               
          ISTAT  = FERPFU                                                   
          RETURN                                                                
                                                                                
      END IF                                                                    
                                                                                
      IOFF   = 0                                                                
      DO 30 J = 1,FZNFUN                                                      
          IF (I.LE.IOFF+FZACTPAR(J)) THEN                                     
              IFUN   = FZFCODE(J)                                             
              GO TO 40                                                          
                                                                                
          END IF                                                                
                                                                                
          IOFF   = IOFF + FZACTPAR(J)                                         
   30 CONTINUE                                                                  
      ISTAT  = FERFUN                                                       
      RETURN                                                                    
C                                                                               
C SEARCH FOR INITIAL VALUE IN FSPEC                                             
C                                                                               
   40 FSPEC  = FZSPEC(J)                                                      
      NAME   = PARAM(1:LEN)//'='                                                
      II     = INDEX(FSPEC,NAME(1:LEN+1))                                       
      IF (II.EQ.0) THEN                                                         
          II     = INDEX(FSPEC,')') + 2                                         
                                                                                
      ELSE                                                                      
          II1    = INDEX(FSPEC(II:),' ')                                        
          FSPEC(II:) = FZSPEC(J) (II1:)                                       
      END IF                                                                    
C                                                                               
C INSERT VAL IN POSITION II                                                     
C                                                                               
      IF (FZFIXED(I).EQ.0) THEN                                               
          CVAL   = VAL(1:LENV)//'@'                                             
          L      = LENV + 1                                                     
                                                                                
      ELSE                                                                      
          CVAL   = VAL                                                          
          L      = LENV                                                         
      END IF                                                                    
                                                                                
      FZSPEC(J) = FSPEC(1:II-1)//CVAL(1:L)//FSPEC(II:)                        
      RETURN                                                                    
                                                                                
      END                                                                       
