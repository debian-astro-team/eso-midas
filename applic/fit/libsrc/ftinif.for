C @(#)ftinif.for	19.1 (ES0-DMD) 02/25/03 13:17:35
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTINIF(NAME,ISTAT)                                             
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C INIT FUNCTION                                                                 
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  USE MIDAS I/O INTERFACES TO FRAMES AND TABLES                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  NON LINEAR FITTING                                                           
C                                                                               
C                                                                               
C----------------------------------------------------------------               
C                                                                               
C      IMPLICIT NONE                                                            
C                                                                               
C INPUT PARAMETERS                                                              
C NAME CHAR FUNCTION SPECIFICATION                                              
C                                                                               
C OUTPUT PARAMETERS                                                             
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      INTEGER I,I1,IAC,ISTAT,IB
      REAL RB
      CHARACTER*(*) NAME                                                        
      CHARACTER FITSPEC*7
       INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'                                     
       INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'                                     
      DATA FITSPEC/'FIT....'/                                       
C                                                                               
C       INITALIZE VARIABLES                                                     
C                                                                               
      FZNFUN = 1                                                              
C                                                                               
C ASSIGN FUNCTION NAMES                                                         
C                                                                               
      DO 10 I = FZNFUN + 1,FZFUNMAX                                         
          FZSPEC(I) = ' '                                                     
   10 CONTINUE                                                                  
      FZNPTOT = 0                                                             
      I1     = FZNFUN                                                         
      FZNFUN = 0                                                              
      CALL FTDFUN(1,NAME,ISTAT)                                                 
      DO 20 I = 1,FZNPTOT                                                     
          CALL GENCNV(FZPTOKEN(I),4,1,IB,RB,FZGUESS(I),IAC)                 
C   WRITE(*,*) FZPTOKEN(I)                                                    
   20 CONTINUE                                                                  
      RETURN                                                                    
                                                                                
      END                                                                       
