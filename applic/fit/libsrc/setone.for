C @(#)setone.for	19.1 (ES0-DMD) 02/25/03 13:17:44
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE SETONE(VECTOR,DIM)                                             
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
C                                                                               
C.MODULE                                                                        
C       FIT                                                                     
C                                                                               
C.NAME                                                                          
C       SETONE                                                                  
C                                                                               
C.PURPOSE                                                                       
C       Set ones in a vector                                                    
C                                                                               
C.KEYWORDS                                                                      
C       Linear algebra.                                                         
C                                                                               
C.DESCRIPTION                                                                   
C       trivial                                                                 
C                                                                               
C.RESTRICTIONS                                                                  
C       ...                                                                     
C                                                                               
C.LANGUAGE                                                                      
C       FORTRAN                                                                 
C                                                                               
C.CALLING SEQUENCE                                                              
C       CALL SETONE (VECTOR,DIM)                                                
C                                                                               
C.INPUT PARAMETERS                                                              
C       DIM                           INTEGER Dimension of the vector           
C                                                                               
C.MODIFIED PARAMETERS                                                           
C       none                                                                    
C                                                                               
C.OUTPUT PARAMETERS                                                             
C       VECTOR    (DIM)               DOUBLE  Input vector                      
C                                                                               
C.FILES                                                                         
C       none                                                                    
C                                                                               
C.MODULES CALLED                                                                
C       none                                                                    
C                                                                               
C.AUTHOR                                                                        
C       Ph. DEFERT,      Feb 1986                                               
C                                                                               
C.MODIFICATIONS                                                                 
C                                                                               
C                                                                               
C-----------------------------------------------------------------------        
C      IMPLICIT NONE                                                            
C     ..                                                                        
C     .. Scalar Arguments ..                                                    
      INTEGER DIM                                                               
C     ..                                                                        
C     .. Array Arguments ..                                                     
      DOUBLE PRECISION VECTOR(DIM)                                              
C     ..                                                                        
C     .. Local Scalars ..                                                       
      INTEGER I                                                                 
                                                                                
      DO 10 I = 1,DIM                                                           
          VECTOR(I) = 1.D0                                                      
   10 CONTINUE                                                                  
      RETURN                                                                    
                                                                                
      END                                                                       
