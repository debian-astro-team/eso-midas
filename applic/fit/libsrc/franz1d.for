C @(#)franz1d.for	19.1 (ES0-DMD) 02/25/03 13:17:31
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FRANZ1(X,NP,PARAM,Y1,DERIV)                                   
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                  
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C--------------------------------------------------------------                 
C**********************************************                                 
C                                                                               
C      IMPLICIT NONE                                                            
C 1-DIM FRANZ-FUNCTION  18 MAR 1986                                             
C                                                                               
C**********************************************                                 
C                                                                               
C Author: O.-G. Richter, ESO Garching                                           
C                                                                               
      INTEGER NP
      REAL X
      DOUBLE PRECISION Y1,PARAM(NP),DERIV(NP)                                   
C                                                                               
      DOUBLE PRECISION A,A1,B,B1,B2,C,C1,D                                      
C                                                                               
      A      = X - PARAM(2)                                                     
      A1     = DABS(A)                                                          
      B      = A1/PARAM(3)                                                      
      B1     = 1.0D0 + B                                                        
      B2     = DLOG(B1)                                                         
      C      = A1/PARAM(5)                                                      
      C1     = 1.0D0 + C                                                        
      D      = (PARAM(5)+A1)/ (PARAM(3)+A1)                                     
C                                                                               
      DERIV(1) = B1** (-PARAM(4)*C1)                                            
      Y1     = PARAM(1)*DERIV(1)                                                
C                                                                               
      A1     = Y1*PARAM(4)/PARAM(5)                                             
      DERIV(2) = DSIGN(A1* (B2+D),A)                                            
      DERIV(3) = A1*B*D                                                         
      DERIV(4) = -Y1*C1*B2                                                      
      DERIV(5) = A1*C*B2                                                        
C                                                                               
      RETURN                                                                    
                                                                                
      END                                                                       
