C @(#)lognorm.for	19.1 (ES0-DMD) 02/25/03 13:17:40
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE LOGNRM(IND,X,NP,PARAM,Y,DERIV)                                
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                  
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C--------------------------------------------------------------                 
C                                                                               
C   Logarithmic normal distribution                                             
C      IMPLICIT NONE                                                            
C                                                                               
C   Y = P1*EXP(-0.5*((LN(X-P4)-P2)/P3)**2)/((X-P4)*P3*SQRT(2*PI))               
C                                                                               
      INTEGER NP,IND,I
      DOUBLE PRECISION Y,PARAM(NP),DERIV(NP)                                    
      REAL X(IND)                                                          
      DOUBLE PRECISION A,B,C,D,E                                                
C                                                                               
      Y      = 0.0D0                                                            
      DO 10 I = 1,4                                                             
          DERIV(I) = 0.0D0                                                      
   10 CONTINUE                                                                  
      A      = X(1) - PARAM(4)                                                  
      IF (A.LE.0.0D0) RETURN                                                    
C                                                                               
      B      = DLOG(A) - PARAM(2)                                               
      C      = 1.0D0/PARAM(3)                                                   
      D      = B*C*C                                                            
      E      = B*D                                                              
      DERIV(1) = 3.989422803D-1*C*DEXP(-0.5D0*E)/A                              
      Y      = PARAM(1)*DERIV(1)                                                
      DERIV(2) = Y*D                                                            
      DERIV(3) = Y*C* (E-1.0D0)                                                 
      DERIV(4) = Y* (D+1.0D0)/A                                                 
      RETURN                                                                    
                                                                                
      END                                                                       
