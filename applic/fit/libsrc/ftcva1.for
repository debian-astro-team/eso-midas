C  @(#)ftcva1.for	19.1 (ESO-DMD) 02/25/03 13:17:32 
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTCVA1(X,W,NAXIS,NPIX1,NPIX2,NPIX3,STR,STP,CUT)                             
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C INTERMEDIATE ROUTINE TO COMPUTE FITTED VALUES                                 
C LIMITED TO THREE DIMS IN THIS VERSION-SPEED                                   
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  USE MIDAS I/O INTERFACES TO FRAMES AND TABLES                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  NON LINEAR FITTING                                                           
C                                                                               
C.VERSION
C 021122	last modif
C                                                                               
C----------------------------------------------------------------               
C                                                                               
C      IMPLICIT NONE                                                            
C                                                                               
      INTEGER NAXIS,NPIX1,NPIX2,NPIX3,I1,I2,I3

      REAL X(NPIX1,NPIX2,NPIX3)                                      
      REAL W(NPIX1,NPIX2,NPIX3)                                      
      REAL XIND(8),RESULT(2),CUT(4)                                        

      DOUBLE PRECISION RES(2)                                                   
      DOUBLE PRECISION STR(3),STP(3)                                           
C 
      INCLUDE   'MID_INCLUDE:FITI.INC'
      INCLUDE   'MID_INCLUDE:FITC.INC'
C                                                                               
C ITERATE ON PIXEL VALUES                                                       
C                                                                               
      CUT(1) = 0.                                                               
      CUT(2) = 0.                                                               
      CUT(3) = 1.E20                                                            
      CUT(4) = -CUT(3)                                                          
      DO 30 I3 = 1,NPIX3                                                      
          XIND(3) = STR(3) + (I3-1)*STP(3)                                      
          DO 20 I2 = 1,NPIX2                                                  
              XIND(2) = STR(2) + (I2-1)*STP(2)                                  
              DO 10 I1 = 1,NPIX1                                              
                  XIND(1) = STR(1) + (I1-1)*STP(1)                              
                  CALL FTCVAL(NAXIS,XIND,RES)                                   
                  RESULT(1) = RES(1)                                            
                  CUT(3) = AMIN1(CUT(3),RESULT(1))                              
                  CUT(4) = AMAX1(CUT(4),RESULT(1))                              
                  X(I1,I2,I3) = RESULT(1)                                       
                  IF (FZPTRM.NE.0) W(I1,I2,I3) = RES(2)                       
   10         CONTINUE                                                          
   20     CONTINUE                                                              
   30 CONTINUE                                                                  
      RETURN                                                                    
                                                                                
      END                                                                       
