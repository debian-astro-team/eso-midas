C  @(#)ftimag.for	19.1 (ESO-DMD) 02/25/03 13:17:34 
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTIMAG(NAME,MASK,ISTAT)                                        
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C INITIALIZE DATA STRUCTURE TO FIT IMAGE DATA                                   
C DEPENDENT VARIABLE IS THE IMAGE DATA                                          
C INDEPENDENT VARIABLES ARE IMAGE AXES                                          
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  USE MIDAS I/O INTERFACES TO FRAMES AND TABLES                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  NON LINEAR FITTING                                                           
C                                                                               
C.VERSION
C 021122	last modif
C                                                                               
C----------------------------------------------------------------               
C                                                                               
C                                                                               
C INPUT PARAMETERS                                                              
C NAME CHAR IMAGE NAME                                                          
C MASK CHAR OPTIONAL WEIGHTING MASK                                             
C   BLANKS IF NOT USED                                                          
C                                                                               
C OUTPUT PARAMETERS                                                             
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      IMPLICIT NONE

      CHARACTER*(*) NAME,MASK                                                   
      CHARACTER UNIT*72,IDENT*72                                                

      INTEGER NPIX(8), I,ISTAT,NAXIS,IMASK
      INTEGER*8  IPTR

      DOUBLE PRECISION STR(3), STP(3)
C 
      INCLUDE 'MID_INCLUDE:ST_DEF.INC'
      INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'  
      INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'  
      INCLUDE 'MID_INCLUDE:ST_DAT.INC'
C                                                                               
C READ OPTIONAL MASK                                                            
C                                                                               
      IF (MASK(1:1).EQ.' ') THEN                                                
          FZPTRM = 0                                                          
          FZWEIGHT = 0                                                        
      ELSE                                                                      
          CALL STIGET(MASK,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,
     +    3,NAXIS,NPIX,STR,STP,IDENT,UNIT,IPTR,IMASK,
     +                ISTAT)                                                    
          FZPTRM = IPTR                                                       
          FZWEIGHT = 1                                                        
      END IF                                                                    
C                                                                               
C READ IMAGE                                                                    
C                                                                               
      CALL STIGET(NAME,D_R4_FORMAT,F_I_MODE,F_IMA_TYPE,3,NAXIS,
     +            NPIX,FZSTART,FZSTEP,IDENT,
     +            UNIT,IPTR,FZIDEN,ISTAT)          
      FZPTRI = IPTR                                                           
      FZNAXIS = NAXIS                                                         
      FZDVAR = 0                                                              
      DO 10, I = 1,3 
          FZIVAR(I) = I                                                       
          FZNPIX(I) = MAX(1,NPIX(I))                                          
   10 CONTINUE                                                                  
C                                                                               
C INITIALIZE VARIABLES IN THE COMMON AREA                                       
C                                                                               
      FZNAME = NAME                                                           
      FZTYPE = 'BDF '                                                         
      FZNFUN = 0                                                              
      FZNDAT = 0                                                              
      FZNITER = 0                                                             
      FZNPTOT = 0                                                             
      FZRELAX = 0.                                                            
      FZCHISQ = 0.                                                            
      FZCCHIS = 0.                                                            
      DO 20, I = 1,FZFUNMAX                                                    
          FZFCODE(I) = 0                                                      
          FZACTPAR(I) = 0                                                     
          FZSPEC(I) = ' '                                                     
   20 CONTINUE                                                                  
      DO 30, I = 1,FZPARMAX                                                    
          FZERROR(I) = 0.D0                                                   
          FZVALUE(I) = 0.D0                                                   
          FZGUESS(I) = 0.D0                                                   
          FZUNCER(I) = 0.                                                     
          FZFIXED(I) = -1                                                     
   30 CONTINUE                                                                  
      FZMAPPED = 1                                                            
      RETURN                                                                    
      END                                                                       
