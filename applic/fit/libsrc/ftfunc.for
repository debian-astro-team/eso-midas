C @(#)ftfunc.for	19.1 (ES0-DMD) 02/25/03 13:17:34
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE FTFUNC(NF,IND,X,NP,PARAM,Y1,DERIV)                             
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                  
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C. feb1992 add the MOFFAT function. M.Peron                                                                               
C--------------------------------------------------------------                 
C                                                                               
C FUNCTION DEFINITION                                                           
C                                                                               
C INPUT PARAMS                                                                  
C NF INTG FUNCTION CODE                                                         
C IND INTG NO. OF IND. VARS                                                     
C X REAL IND.VARS                                                               
C NP INTG NO. OF PARAMS                                                         
C PARAM DBLE FUNCTION PARAMETERS                                                
C                                                                               
C OUTPUT PARAMS                                                                 
C Y1 DBLE COMPUTED FUNCTION VALUE                                               
C DERIV DBLE COMPUTED DERIVATIVE VALUE                                          
C
C      IMPLICIT NONE                                                            
C                                                                               
      INTEGER NP,IND,NF
      DOUBLE PRECISION Y1,PARAM(NP),DERIV(NP)                                   
      REAL X(IND)                                                          
                                                                                
      GO TO (10,20,30,40,50,60,70,390,390,                                      
     +       390,390,390,80,90,100,110,120,390,                                 
     +       390,390,390,130,140,150,160,390,390,                               
     +       390,170,180,190,200,210,220,230,240,                               
     +       250,260,270,280,290,300,310,320,330,                               
     +       340,350,360,370,380),NF                                                
                                                                                
      GO TO 390                                                                 
                                                                                
   10 CALL POLY(IND,X,NP,PARAM,Y1,DERIV)                                        
      RETURN                                                                    
                                                                                
   20 CALL FLOG(X,NP,PARAM,Y1,DERIV)                                            
      RETURN                                                                    
                                                                                
   30 CALL FEXP(X,NP,PARAM,Y1,DERIV)                                            
      RETURN                                                                    
                                                                                
   40 CALL FSIN(X,NP,PARAM,Y1,DERIV)                                            
      RETURN                                                                    
                                                                                
   50 CALL FTAN(X,NP,PARAM,Y1,DERIV)                                            
      RETURN                                                                    
                                                                                
   60 CALL FSINH(X,NP,PARAM,Y1,DERIV)                                           
      RETURN                                                                    
                                                                                
   70 CALL FTANH(X,NP,PARAM,Y1,DERIV)                                           
      RETURN                                                                    
                                                                                
   80 CALL TRIANG(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
   90 CALL POISSN(IND,X,NP,PARAM,Y1,DERIV)                                     
      RETURN                                                                    
                                                                                
  100 CALL GAUSS(IND,X,NP,PARAM,Y1,DERIV)                                       
      RETURN                                                                    
                                                                                
  110 CALL CAUCHY(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  120 CALL LORENZ(IND,X,NP,PARAM,Y1,DERIV)                                     
      RETURN                                                                    
                                                                                
  130 CALL ERFUNC(X,NP,PARAM,Y1,DERIV)                                          
      RETURN                                                                    
                                                                                
  140 CALL SINC(X,NP,PARAM,Y1,DERIV)                                            
      RETURN                                                                    
                                                                                
  150 CALL SINC2(X,NP,PARAM,Y1,DERIV)                                           
      RETURN                                                                    
                                                                                
  160 CALL FRANZ(IND,X,NP,PARAM,Y1,DERIV)                                       
      RETURN                                                                    
                                                                                
  170 CALL BRAND(IND,X,NP,PARAM,Y1,DERIV)                                       
      RETURN                                                                    
                                                                                
  180 CALL USER00(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  190 CALL USER01(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  200 CALL USER02(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  210 CALL USER03(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  220 CALL USER04(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  230 CALL USER05(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  240 CALL USER06(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  250 CALL USER07(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  260 CALL USER08(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  270 CALL USER09(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  280 CALL LAPLAC(IND,X,NP,PARAM,Y1,DERIV)                                     
      RETURN                                                                    
                                                                                
  290 CALL LOGIST(IND,X,NP,PARAM,Y1,DERIV)                                    
      RETURN                                                                    
                                                                                
  300 CALL SEMILG(IND,X,NP,PARAM,Y1,DERIV)                                     
      RETURN                                                                    
                                                                                
  310 CALL LOGNRM(IND,X,NP,PARAM,Y1,DERIV)                                     
      RETURN                                                                    
                                                                                
  320 CALL PARETO(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  330 CALL GAMMDS(IND,X,NP,PARAM,Y1,DERIV)                                     
      RETURN                                                                    
                                                                                
  340 CALL DIPOLE(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  350 CALL IGAUSS(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  360 CALL GAUSSA(IND,X,NP,PARAM,Y1,DERIV)                                      
      RETURN                                                                    
                                                                                
  370 CALL IGAUSA(IND,X,NP,PARAM,Y1,DERIV)                                     
      RETURN                                                                    
                                                                                
  380 CALL MOFFAT(X,NP,PARAM,Y1,DERIV)
      RETURN

  390 CALL NULLF(IND,X,NP,PARAM,Y1,DERIV)                                       
      RETURN                                                                    
                                                                                
      END                                                                       
