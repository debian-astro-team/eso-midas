C @(#)gauss1d.for	19.1 (ES0-DMD) 02/25/03 13:17:38
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE GAUSS1(X,NP,PARAM,Y1,DERIV)                                   
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                  
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C--------------------------------------------------------------                 
C**********************************************                                 
C                                                                               
C      IMPLICIT NONE                                                            
C 1-D GAUSSIAN                                                                  
C                                                                               
C PARAMETERS                                                                    
C Y1 = P(1)*EXP(-LOG(2)*(2*(X-P(2))/P(3))**2))                                  
C                                                                               
C**********************************************                                 
C                                                                               
C Authors: O.-G. Richter and D. Ponz, ESO Garching                              
C                                                                               
    
      INTEGER NP
      REAL X
      DOUBLE PRECISION Y1,PARAM(NP),DERIV(NP)                                   
      DOUBLE PRECISION W1,W2,W3                                                 
C                                                                               
      W1     = X - PARAM(2)                                                     
      W2     = 2.D0*W1/PARAM(3)                                                 
      W3     = DEXP(-0.6931471805599D0*W2*W2)                                   
      Y1     = PARAM(1)*W3                                                      
      DERIV(1) = W3                                                             
      W2     = 2.7725887222397D0*PARAM(1)*W2*W3/PARAM(3)                        
      DERIV(2) = W2                                                             
      DERIV(3) = W1*W2/PARAM(3)                                                 
      RETURN                                                                    
                                                                                
      END                                                                       
