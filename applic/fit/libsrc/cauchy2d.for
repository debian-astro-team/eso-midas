C @(#)cauchy2d.for	19.1 (ES0-DMD) 02/25/03 13:17:28
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE CAUC2D(X,NP,PARAM,Y,DERIV)                                   
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                  
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C--------------------------------------------------------------                 
C**********************************************                                 
C                                                                               
C      IMPLICIT NONE                                                            
C      COMMON /VMR/MADRID(1)                                                   
C  CAUCHY DISTRIBUTION                                                          
C                                                                               
C  Y(X;P1,P2,P3) = P1/(1.+(2(X-P2)/P3)**2)                                      
C                                                                               
C**********************************************                                 
C  Author: O.-G. Richter, ESO Garching                                          
C                                                                               
      INTEGER NP
      REAL X(2)                                                            
      DOUBLE PRECISION Y,PARAM(NP),DERIV(NP)                                    
C                                                                               
      DOUBLE PRECISION A,B                                                      
C                                                                               
      A      = 2.0D0* (X(1)-PARAM(2))/PARAM(4)                                  
      B      = 2.0D0* (X(2)-PARAM(3))/PARAM(5)                                  
      DERIV(1) = 1.0D0/ (1.D0+A*A+B*B)                                          
      Y      = PARAM(1)*DERIV(1)                                                
      DERIV(2) = 4.0D0*Y*DERIV(1)*A/PARAM(4)                                    
      DERIV(3) = 4.0D0*Y*DERIV(1)*B/PARAM(5)                                    
      DERIV(4) = 0.5D0*A*DERIV(2)                                               
      DERIV(5) = 0.5D0*B*DERIV(3)                                               
C                                                                               
      RETURN                                                                    
                                                                                
      END                                                                       
