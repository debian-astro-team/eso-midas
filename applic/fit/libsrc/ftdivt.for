C @(#)ftdivt.for	19.1 (ES0-DMD) 02/25/03 13:17:34
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C @(#)ftdivt.for	2.1.1.1 (ESO-IPG) 9/27/91 18:08:44
      SUBROUTINE FTDIVT(NAME,IVAR,ISTAT)                                        
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                   
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  17:25 - 13 JAN 1988                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C.IDENTIFICATION                                                                
C                                                                               
C  FITLIB.FOR   VERSION 1.0  27 MAR 1984                                        
C                                                                               
C.PURPOSE                                                                       
C                                                                               
C  INTERFACE ROUTINES FOR THE FITTING STRUCTURES                                
C                                                                               
C.ALGORITHM                                                                     
C                                                                               
C  USE MIDAS I/O INTERFACES TO FRAMES AND TABLES                                
C                                                                               
C.KEYWORDS                                                                      
C                                                                               
C  NON LINEAR FITTING                                                           
C                                                                               
C MODIF MP apr 92: make compute/fit table funct(:x,:y) working   
C----------------------------------------------------------------               
C                                                                               
C DEFINE NEW INDEPENDENT VARIABLES AS COLUMNS IN A TABLE                        
C      IMPLICIT NONE                                                            
C                                                                               
C INPUT PARAMETERS                                                              
C NAME CHAR TABLE NAME                                                          
C IVAR CHAR COLUMNS ACTING AS IND.VARS                                          
C                                                                               
C OUTPUT PARAMETERS                                                             
C ISTAT INTG STATUS RETURN                                                      
C                                                                               
      INTEGER ISTAT,IND2,KZ6952
      CHARACTER*(*) NAME,IVAR                                                   
      CHARACTER*80 LINE,LINE1                                                   
      CHARACTER*8 NAME1                                                         
      CHARACTER*17 LAB1
      LOGICAL NEXT                                                              
       INCLUDE 'MID_INCLUDE:FITI.INC/NOLIST'                                     
       INCLUDE 'MID_INCLUDE:FITC.INC/NOLIST'                                     
C                                                                               
C READ TABLE                                                                    
C                                                                               
      NAME1  = NAME                                                             
      IF (FZMAPPED.EQ.1 .AND. FZTYPE.EQ.'TBL ') THEN                        
          IF (FZNAME(1:8).NE.NAME1) THEN  !   TABLE MUST BE THE SAME          
              ISTAT  = FERTAB                                               
              RETURN                                                            
                                                                                
          END IF                                                                
                                                                                
      ELSE                                                                      
          IF (FZMAPPED.EQ.0) THEN                                             
              CALL TBTOPN(NAME,2,FZIDEN,ISTAT)                                           
              FZMAPPED = 1                                                    
              FZNAME = NAME                                                   
              FZTYPE = 'TBL '                                                 
          END IF                                                                
                                                                                
      END IF                                                                    
C                                                                               
C DECODE INDEPENDENT VARIABLES                                                  
C                                                                               
      IND2   = INDEX(IVAR,' ') - 1                                              
      LINE   = IVAR(1:IND2)//','                                             
C      LINE = IVAR(1:IND2)
      IND2   = INDEX(LINE,',')                                               
      NEXT   = .TRUE.                                                           
      FZNIND = 0                                                              
      KZ6952 = 0
10    KZ6952 = KZ6952 + 1
      IF ( .NOT. (NEXT)) GO TO 20                                           
C                                                                               
C   LAB1 CONTAINS NEXT LABEL                                                    
C                                                                               
      LAB1   = LINE(1:IND2-1)                                            
      LINE1  = LINE(IND2+1:)                                                
      LINE   = LINE1                                                        
      FZNIND = FZNIND + 1                                               
      CALL TBCSER(FZIDEN,LAB1,FZIVAR(FZNIND),ISTAT)                       
      IF (FZIVAR(FZNIND).EQ.-1) THEN                                    
         ISTAT  = FERPAR                                               
         RETURN                                                            
      END IF                                                                
C
      IND2   = INDEX(LINE,',')                                           
      NEXT   = IND2 .NE. 0                                                  
      GOTO 10
C 
   20 CONTINUE                                                                  
C                                                                               
C INITIALIZE VARIABLES IN THE COMMON AREA                                       
C                                                                               
      FZNAME = NAME                                                           
      FZTYPE = 'TBL '                                                         
      FZMAPPED = 1                                                            
      RETURN                                                                    
                                                                                
      END                                                                       

