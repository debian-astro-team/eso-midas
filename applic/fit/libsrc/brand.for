C @(#)brand.for	19.1 (ES0-DMD) 02/25/03 13:17:28
C===========================================================================
C Copyright (C) 1995 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
C MA 02139, USA.
C
C Corresponding concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
      SUBROUTINE BRAND(INDEP,X,NP,PARAM,Y1,DERIV)                               
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                  
C.COPYRIGHT: Copyright (c) 1987 European Southern Observatory,                  
C                                         all rights reserved                   
C                                                                               
C.VERSION: 1.0  ESO-FORTRAN Conversion, AA  18:01 - 21 DEC 1987                 
C                                                                               
C.LANGUAGE: F77+ESOext                                                          
C                                                                               
C.AUTHOR: J.D.PONZ                                                              
C                                                                               
C--------------------------------------------------------------                 
C**************************************************                             
C                                                                               
C      IMPLICIT NONE                                                            
C      FIT FOR ROTATIONCURVES WITH A BRAND-MODEL USING 3 PARAMS                 
C                                                                               
C**************************************************                             
C                                                                               
C Author: O.-G. Richter, ESO Garching                                           
C                                                                               
      INTEGER INDEP,NP
      DOUBLE PRECISION Y1,PARAM(NP),DERIV(NP),XX,SS,ST,SU                       
      REAL X(INDEP)                                                        
C                                                                               
      XX     = X(1)/PARAM(1)                                                    
      SS     = 0.0D0                                                            
      IF (XX.GT.0.D0) SS     = XX**PARAM(3)                                     
      ST     = 0.33333333333D0 + 0.66666666667D0*SS                             
      SU     = 1.5D0/PARAM(3)                                                   
      DERIV(2) = XX/ (ST**SU)                                                   
      Y1     = DERIV(2)*PARAM(2)                                                
      DERIV(1) = Y1* (SS/ST-1.0D0)/PARAM(1)                                     
      DERIV(3) = Y1/PARAM(3)* (SU*DLOG(ST)-SS*DLOG(XX)/ST)                      
C                                                                               
      RETURN                                                                    
                                                                                
      END                                                                       
