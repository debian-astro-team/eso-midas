C===========================================================================
C Copyright (C) 1995-2010 European Southern Observatory (ESO)
C
C This program is free software; you can redistribute it and/or 
C modify it under the terms of the GNU General Public License as 
C published by the Free Software Foundation; either version 2 of 
C the License, or (at your option) any later version.
C
C This program is distributed in the hope that it will be useful,
C but WITHOUT ANY WARRANTY; without even the implied warranty of
C MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C GNU General Public License for more details.
C
C You should have received a copy of the GNU General Public 
C License along with this program; if not, write to the Free 
C Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
C MA 02139, USA.
C
C Correspondence concerning ESO-MIDAS should be addressed as follows:
C	Internet e-mail: midas@eso.org
C	Postal address: European Southern Observatory
C			Data Management Division 
C			Karl-Schwarzschild-Strasse 2
C			D 85748 Garching bei Muenchen 
C			GERMANY
C===========================================================================
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C.VERSION: 1.2  ESO-FORTRAN Conversion, AA  14:17 - 19 NOV 1989
C
C.LANGUAGE: F77+ESOext
C
C.AUTHOR: J.D.PONZ
C
C.IDENTIFICATION:
C         PROGRAM tablefit
C
C.PURPOSE
C
C  Copy TABLE file into fit file after editing
C
C.KEYWORDS
C
C ARITHMETIC OPERATS, TABLES.
C
C.ALGORITHM
C
C USE TABLE INTERFACE ROUTINES
C
C.VERSION
C 080391	add FZMAPPED=0  M.Peron
C 100615	last modif
C-----------------------------------------------------------
C
      PROGRAM TBLFIT
      IMPLICIT NONE
C
      CHARACTER*72 LINE
      CHARACTER*80  FILENA, TABLE
      CHARACTER*80  SPEC
      CHARACTER*60  FUNC, PARM, BLANK, CTEST
      CHARACTER*16  LABEL1, LABEL2, UNIT
      CHARACTER*10  FORM
      INTEGER       KUN, KNUL, STAT, TID, NROW, I, II, IJ
      INTEGER       INDEX, IQUIT
      INTEGER       MADRID(1)
      LOGICAL       NULL1, NULL2
C
      INCLUDE      'MID_INCLUDE:ST_DEF.INC'
      INCLUDE      'MID_INCLUDE:FITI.INC'
      INCLUDE      'MID_INCLUDE:FITC.INC'
      COMMON/VMR/MADRID
      INCLUDE      'MID_INCLUDE:ST_DAT.INC'
      DATA          LABEL1/'FUNCTIONS   '/
      DATA          LABEL2/'PARAMETERS  '/
      DATA          FILENA/'           '/
      DATA          UNIT/'             '/
      DATA          FORM/'A40    '/
C
C ... GET INTO MIDAS
C
      CALL STSPRO('TBLFIT')
      CALL FITBL
C
C ... GET COMMAND FORM ENVIRONMENT
C

      CALL STKRDC('IN_A',1,1,80,I,FILENA,KUN,KNUL,STAT)
      CALL STKRDI('OUTPUTI',1,1,I,IQUIT,KUN,KNUL,STAT)
      I     = INDEX(FILENA,'.')
      IF (I .EQ. 0) I = INDEX(FILENA,' ')
      I     = I - 1
      TABLE = FILENA(1:I)//'_fit'
      IF (IQUIT.EQ.1) GOTO 100
C
C ... INITIALIZE FIT FILE
C

      CALL FTINIT(FILENA, STAT)

C
C ... INITIALIZE TABLE FILE
C
      CALL TBTOPN(TABLE, F_I_MODE, TID, STAT)
C
C ... INITIALIZE TABLE DATA
C
      BLANK  = ' '
      NROW   = 20
      FZNFUN = 0
      DO 10 I = 1, NROW
         CALL TBERDC(TID, I, 1, CTEST, NULL1, STAT)
         CALL FT_EOS(CTEST,60,FUNC,STAT)
         CALL TBERDC(TID, I, 2, CTEST, NULL2, STAT)
         CALL FT_EOS(CTEST,60,PARM,STAT)
         IF (NULL2) PARM = BLANK
         IF (INDEX(FUNC,'(').NE.0) THEN
C
C         WRITE(LINE,9999) I,FUNC
C9999     FORMAT(1X,I4,' Function :',A40)
C         CALL STTPUT(LINE,STAT)
C         WRITE(LINE,9998) I,PARM
C9998     FORMAT(1X,I4,' Parameter:',A40)
C         CALL STTPUT(LINE,STAT)
C
             FZNFUN = FZNFUN + 1
             IJ     = INDEX(FUNC,')')
             SPEC   = FUNC(1:IJ)//PARM
             II     = FZNFUN
             CALL FTDFUN(II, SPEC, STAT)
         ENDIF
10    CONTINUE
      CALL TBTCLO(TID, STAT)
      FZMAPPED = 0
      CALL FTEXIT(FILENA, STAT)
100   CONTINUE
      I = INDEX(TABLE,' ')
      I = I - 1
      FILENA = TABLE(1:I)//'.tbl'
      CALL STFDEL(FILENA, STAT)
      WRITE(6,*)STAT
      WRITE(LINE,9997) FILENA
9997  FORMAT(' Infor: delete ',A20)
      CALL STTPUT(LINE,STAT)
C
C ... EXIT FROM MIDAS
C
      CALL STSEPI
      END
