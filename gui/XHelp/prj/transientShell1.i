! UIMX ascii 2.0 key: 1793                                                      
*action.WriteHelp: {\
WriteHelpAW(UxWidget);\
helpPR(UxWidget);\
\
}

*translation.table: transTable1
*translation.parent: transientShell1
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.Ctrl<Key>a: beginning-of-line()
*translation.Ctrl<Key>b: backward-character()
*translation.Ctrl<Key>d: kill-next-character()
*translation.Ctrl<Key>e: end-of-line()
*translation.Ctrl<Key>f: forward-character()
*translation.Ctrl<Key>i: insert-string("    ")
*translation.Ctrl<Key>k : kill-to-end-of-line()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation.<Key>osfDelete: delete-previous-character()
*translation.Meta<Key>b: backward-word()
*translation.Meta<Key>d: delete-next-word()
*translation.Meta<Key>f: forward-word()
*translation.Ctrl<Key>j: newline-and-indent()
*translation.Ctrl<Key>l: redraw-display()
*translation.Ctrl<Key>m: newline()
*translation.Ctrl<Key>n: next-line()
*translation.Ctrl<Key>o: newline-and-backup()
*translation.Ctrl<Key>p: previous-line()
*translation.Ctrl<Key>u: kill-to-start-of-line()
*translation.Ctrl<Key>v: next-page()
*translation.Ctrl<Key>w: kill-selection()
*translation.Ctrl<Key>y: unkill()
*translation.Ctrl<Key>z: scroll-one-line-down()
*translation.Meta<Key>v: previous-page()
*translation.Meta<Key>y: stuff()
*translation.Meta<Key>z: scroll-one-line-down()
*translation.<EnterWindow>: WriteHelp() Enter()

*translation.table: transTable6
*translation.parent: transientShell1
*translation.policy: override
*translation.<EnterNotify>: WriteHelp() Enter()

*transientShell1.class: transientShell
*transientShell1.parent: NO_PARENT
*transientShell1.static: true
*transientShell1.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
\
extern char midvers[];\
#include <stdlib.h>\
#include <UxSubproc.h>\

*transientShell1.ispecdecl:
*transientShell1.funcdecl: swidget create_transientShell1()\

*transientShell1.funcname: create_transientShell1
*transientShell1.funcdef: "swidget", "<create_transientShell1>(%)"
*transientShell1.icode:
*transientShell1.fcode: return(rtrn);\

*transientShell1.auxdecl: helpPR(uw)\
Widget uw;\
{\
char s[100];\
extern char mid_mail[];\
\
strcpy(s,"");\
\
/* Class */\
if (uw == UxGetWidget(toggleButton2))\
    strcpy(s,"Problem class: software bug");\
\
if (uw == UxGetWidget(toggleButton3))\
    strcpy(s,"Problem class: documentation bug");\
\
if (uw == UxGetWidget(toggleButton4))\
    strcpy(s,"Problem class: no bug, but modification or new feature requested");\
\
if (uw == UxGetWidget(toggleButton1))\
    strcpy(s,"Problem class: Midas support needed");\
\
/* Category */\
\
if (uw == UxGetWidget(toggleButton5))\
    strcpy(s,"Problem Category: Midas installation");\
\
if (uw == UxGetWidget(toggleButton6))\
    strcpy(s,"Problem Category: Midas distribution");\
\
if (uw == UxGetWidget(toggleButton7))\
    strcpy(s,"Problem Category: System, Monitor, Command Language");\
\
if (uw == UxGetWidget(toggleButton8))\
    strcpy(s,"Problem Category: Display Window and related commands");\
\
if (uw == UxGetWidget(toggleButton9))\
    strcpy(s,"Problem Category: Graphic window and related commands");\
\
if (uw == UxGetWidget(toggleButton10))\
    strcpy(s,"Problem Category: Midas Tables");\
\
if (uw == UxGetWidget(toggleButton11))\
    strcpy(s,"Problem Category: On-line help and documentation");\
\
if (uw == UxGetWidget(toggleButton12))\
    strcpy(s,"Problem Category: Image arithmetic, filtering, transforms");\
\
if (uw == UxGetWidget(toggleButton13))\
    strcpy(s,"Problem Category: Graphical User Interfaces");\
\
if (uw == UxGetWidget(toggleButton14))\
    strcpy(s,"Problem Category: Midas context (select the appropriate one)");\
\
/* Confidentiality, Severity, Priority */\
\
if (uw == UxGetWidget(menu4) || uw == UxGetWidget(menu1_p4))\
    strcpy(s,"Midas Context");\
\
if (uw == UxGetWidget(menu5) || uw == UxGetWidget(menu1_p5))\
    strcpy(s,"Priority level");\
\
/* Originator, Release */\
\
if (uw == UxGetWidget(textField6))\
   strcpy(s,"E-mail address or identification of the mail originator");\
\
if (uw == UxGetWidget(textField7))\
   strcpy(s,"Midas release");\
\
/* Environment, Command , Synopsis */\
\
if (uw == UxGetWidget(text3))\
   strcpy(s,"Hardware and software environment (OS, compilers, platform, ...)");\
\
if (uw == UxGetWidget(textField9))\
   strcpy(s,"Midas command involved (if relevant)");\
\
if (uw == UxGetWidget(textField8))\
   strcpy(s,"Short description of the problem");\
\
/* Description, How-To-Repeat */\
\
if (uw == UxGetWidget(scrolledWindow6) || uw == UxGetWidget(scrolledText6))\
    strcpy(s,"Precise description of the problem");\
\
/* Push Buttons: OK, Save As, Clear, Cancel */\
\
if (uw == UxGetWidget(NewsButton1))\
   sprintf(s,"Send a mail to the address: %s and close the window",\
           mid_mail);\
\
if (uw == UxGetWidget(PrintButton1))\
   strcpy(s,"Save the mail in an ASCII file");\
\
if (uw == UxGetWidget(ReportButton1))\
   strcpy(s,"Clear entry fields: Command, Synopsis, Description, How-To-Repeat");\
\
if (uw == UxGetWidget(SendButton1))\
   strcpy(s,"Close the window");\
\
/* Short Help */\
\
if (uw == UxGetWidget(SHelp1))\
   strcpy(s,"Short help area");\
\
/* Now display the help message */\
\
XmTextSetString(UxGetWidget(SHelp1),s);\
\
}\
\
makeReport(file)\
char file[]; \
{\
 extern swidget pb;\
 extern char mid_mail[];\
 char   full_synopsis[200], category[30], logname[100], command[100];\
 char   synopsis[100], ctx[30], gnats_cat[30];\
 int    len, lenf, i;\
 char    linux_string[80];\
 swidget linux_sw;\
 int     dbx=0;\
 \
 FILE *fp;\
\
 strcpy(ctx,UxGetLabelString(UxFindSwidget(UxGetMenuHistory(menu4))));\
 strcpy(full_synopsis,XmTextGetString(UxGetWidget(textField8)));\
 strcpy(synopsis,full_synopsis);\
 strcpy(command,XmTextGetString(UxGetWidget(textField9)));\
 len = strlen(command);\
\
 if (len > 0) {\
     strcat(full_synopsis," - Command: ");\
     lenf = strlen(full_synopsis);\
     for (i=0; i<len; i++) full_synopsis[lenf+i] = toupper(command[i]);\
     full_synopsis[lenf+len] = '\0';\
     }\
\
 strcpy(logname,getenv("LOGNAME"));\
\
\
  if ((fp = fopen(file,"w")) == NULL) {\
       sprintf(command,"Could not open file: %s with write permission\n",file);\
       XmTextSetString(UxGetWidget(SHelp1),command);\
       return(0); \
     }\
\
 fprintf(fp,"To: %s\n",mid_mail);\
 fprintf(fp,"Cc: %s\n",logname);\
 fprintf(fp,"Subject: %s\n",synopsis);\
 fprintf(fp,"From: %s\n",logname);\
 fprintf(fp,"Reply-To: %s\n",logname);\
 fprintf(fp,">X-send-pr-version: XHelp Problem Report Form\n");\
\
 fprintf(fp,">Submitter-Id:\n");\
\
 fprintf(fp,">Originator:      %s\n",\
      XmTextGetString(UxGetWidget(textField6)));\
\
 fprintf(fp,">Organization:\n");\
 fprintf(fp,">Confidential: no\n");\
 \
 fprintf(fp,">Synopsis:        %s\n",full_synopsis);\
\
 fprintf(fp,">Severity:        serious\n");\
\
 fprintf(fp,">Priority:        %s\n",\
    UxGetLabelString(UxFindSwidget(UxGetMenuHistory(menu5))) );\
\
 strcpy(linux_string, UxGetMenuHistory(rowColumn2));\
 if (dbx == 1) printf("String %s\n",linux_string);\
 if (linux_string[0] != 't') strcpy(linux_string,"toggleButton7");\
 linux_sw = UxFindSwidget(linux_string);\
 if (dbx == 1) printf("Content %s\n",linux_sw);\
 strcpy(category,UxGetLabelString(linux_sw));\
\
 /* Associating Category */\
 strcpy(gnats_cat,"midas-system");\
 if (category[0] == 'I' && category[1] == 'n') \
                         strcpy(gnats_cat,"midas-install");\
 if (category[0] == 'D' && category[3] == 't') \
                         strcpy(gnats_cat,"midas-dist");\
 if (category[0] == 'S') strcpy(gnats_cat,"midas-system");\
 if (category[0] == 'D' && category[3] == 'p') \
                         strcpy(gnats_cat,"midas-display");\
 if (category[0] == 'G' && category[7] == 's') \
                         strcpy(gnats_cat,"midas-graphic");\
 if (category[0] == 'T') strcpy(gnats_cat,"midas-table");\
 if (category[0] == 'H') strcpy(gnats_cat,"midas-doc");\
 if (category[0] == 'I' && category[1] == 'm') \
                         strcpy(gnats_cat,"midas-image");\
 if (category[0] == 'G' && category[7] == 'a') \
                         strcpy(gnats_cat,"midas-gui");\
 if (category[0] == 'C') {\
        strcpy(gnats_cat,"ctx-");\
        strcat(gnats_cat,\
          UxGetLabelString(UxFindSwidget(UxGetMenuHistory(menu4))) );\
        }\
\
 if (dbx == 1) printf ("Associated GNATS category: %s\n",gnats_cat);\
\
 fprintf(fp,">Category:        "); \
 fprintf(fp,"%s\n",gnats_cat);\
\
 strcpy(linux_string,UxGetMenuHistory(rowColumn1));\
 if (dbx == 1) printf("String %s\n",linux_string);\
 if (linux_string[0] != 't') strcpy(linux_string,"toggleButton1");\
 linux_sw = UxFindSwidget(linux_string);\
 fprintf(fp,">Class:           %s\n",UxGetLabelString(linux_sw));\
\
 fprintf(fp,">Release:         %s\n",\
      XmTextGetString(UxGetWidget(textField7)));\
\
 fprintf(fp,">Environment:     \n");\
 fprintf(fp,"%s",XmTextGetString(UxGetWidget(text3)) );\
\
 fprintf(fp,">Description:\n"); \
 fprintf(fp,"%s\n",\
      XmTextGetString(UxGetWidget(scrolledText6)));\
\
 fprintf(fp,">How-To-Repeat:\n\n"); \
 fprintf(fp,">Fix:\n");\
\
 fclose(fp);\
}\
\
sendReport(file)\
char file[];\
{\
char command[100];\
\
 sprintf(command,\
  "cat %s | /usr/lib/sendmail -oi -t ; rm %s",\
  file,file);\
\
 system(command);\
}\
\
get_subproc(UxWidget, mod)\
\
Widget UxWidget;\
int    mod;\
\
{\
char command[1000], buffer[1000];\
FILE *fpin;\
int  input, pos=0;\
\
strcpy( command, getenv("MIDASHOME")); \
strcat( command, "/");\
strcat( command, getenv("MIDVERS")); \
strcat( command, "/system/unix/");\
\
if (mod == 2 ) strcat(command,"patchlevel");\
if (mod == 1 ) strcat(command,"environment");\
if (mod == 0 ) strcat(command,"originator");\
\
if ((fpin = popen(command,"r")) == NULL) {\
    printf("Could not execute command: %s\n",command);\
    return(-1);\
  }\
\
\
while ((input =  fgetc(fpin)) != EOF) buffer[pos++] = (char)input;\
buffer[pos] = '\0';\
\
if (pclose(fpin) == -1)\
   printf("Could not close stream for command: %s\n",command);\
\
XmTextSetString(UxWidget,buffer);\
return(0);\
}\

*transientShell1.name: transientShell1
*transientShell1.x: 654
*transientShell1.y: 28
*transientShell1.width: 572
*transientShell1.height: 750
*transientShell1.title: "Problem Report Form"
*transientShell1.allowShellResize: "false"
*transientShell1.popupCallback: {\
extern char mid_mail[];\
\
char text[100];\
\
sprintf(text,"Edit problem report. Button OK sends a mail to: %s\n",\
         mid_mail);\
\
UxPutText(SHelp1,text);\
\
get_subproc(UxGetWidget(textField6),0); \
get_subproc(UxGetWidget(textField7),2); \
\
XmTextSetTopCharacter(UxGetWidget(text3),(XmTextPosition) 0);\
XmTextSetInsertionPosition(UxGetWidget(text3),(XmTextPosition) 0);\
\
}

*workArea1.class: form
*workArea1.parent: transientShell1
*workArea1.static: true
*workArea1.name: workArea1
*workArea1.x: 20
*workArea1.y: 2
*workArea1.width: 572
*workArea1.height: 4
*workArea1.borderWidth: 0
*workArea1.createCallback: {\
\
}
*workArea1.mapCallback: {\
\
}
*workArea1.background: WindowBackground
*workArea1.unitType: "pixels"
*workArea1.resizePolicy: "resize_none"
*workArea1.fractionBase: 100
*workArea1.focusCallback: {\
/* UxPutBottomOffset(scrolledWindow6,GetBottom()); */\
}

*scrolledWindow6.class: scrolledWindow
*scrolledWindow6.parent: workArea1
*scrolledWindow6.static: true
*scrolledWindow6.name: scrolledWindow6
*scrolledWindow6.scrollingPolicy: "application_defined"
*scrolledWindow6.x: 5
*scrolledWindow6.y: 400
*scrolledWindow6.visualPolicy: "variable"
*scrolledWindow6.scrollBarDisplayPolicy: "static"
*scrolledWindow6.shadowThickness: 0
*scrolledWindow6.height: 160
*scrolledWindow6.width: 480
*scrolledWindow6.background: ApplicBackground
*scrolledWindow6.translations: ""
*scrolledWindow6.bottomAttachment: "attach_form"
*scrolledWindow6.bottomWidget: ""
*scrolledWindow6.leftAttachment: "attach_form"
*scrolledWindow6.leftOffset: 5
*scrolledWindow6.rightAttachment: "attach_form"
*scrolledWindow6.rightOffset: 5
*scrolledWindow6.topAttachment: "attach_form"
*scrolledWindow6.bottomPosition: 68
*scrolledWindow6.topOffset: 440
*scrolledWindow6.bottomOffset: 100

*scrolledText6.class: scrolledText
*scrolledText6.parent: scrolledWindow6
*scrolledText6.static: true
*scrolledText6.name: scrolledText6
*scrolledText6.width: 569
*scrolledText6.height: 100
*scrolledText6.background: "WhiteSmoke"
*scrolledText6.x: 0
*scrolledText6.y: 0
*scrolledText6.editable: "true"
*scrolledText6.scrollHorizontal: "true"
*scrolledText6.scrollVertical: "true"
*scrolledText6.editMode: "multi_line_edit"
*scrolledText6.foreground: TextForeground
*scrolledText6.highlightColor: "black"
*scrolledText6.fontList: TextFont
*scrolledText6.translations: ""
*scrolledText6.createCallback: {\
\
}
*scrolledText6.highlightOnEnter: "true"

*SHelp1.class: text
*SHelp1.parent: workArea1
*SHelp1.static: true
*SHelp1.name: SHelp1
*SHelp1.x: 30
*SHelp1.y: 380
*SHelp1.width: 325
*SHelp1.height: 40
*SHelp1.background: SHelpBackground
*SHelp1.foreground: TextForeground
*SHelp1.highlightColor: ApplicBackground
*SHelp1.fontList: TextFont
*SHelp1.cursorPositionVisible: "true"
*SHelp1.translations: ""
*SHelp1.activateCallback: {\
\
}
*SHelp1.pendingDelete: "true"
*SHelp1.blinkRate: 500
*SHelp1.editMode: "single_line_edit"
*SHelp1.highlightOnEnter: "true"
*SHelp1.wordWrap: "false"
*SHelp1.accelerators: ""
*SHelp1.leftAttachment: "attach_form"
*SHelp1.leftOffset: 5
*SHelp1.rightAttachment: "attach_form"
*SHelp1.rightOffset: 5
*SHelp1.bottomAttachment: "attach_none"
*SHelp1.bottomOffset: 0
*SHelp1.text: "Edit your message. Button OK sends a mail to the midas account"
*SHelp1.topAttachment: "attach_widget"
*SHelp1.topOffset: 10
*SHelp1.topWidget: "scrolledWindow6"

*NewsButton1.class: pushButton
*NewsButton1.parent: workArea1
*NewsButton1.static: true
*NewsButton1.name: NewsButton1
*NewsButton1.x: 180
*NewsButton1.y: 330
*NewsButton1.width: 95
*NewsButton1.height: 30
*NewsButton1.background: ButtonBackground
*NewsButton1.labelString: "OK"
*NewsButton1.foreground: ButtonForeground
*NewsButton1.fontList: BoldTextFont
*NewsButton1.activateCallback: {\
 extern char filename[];\
 extern swidget pb;\
\
 makeReport(filename);\
 sendReport(filename);\
 SensMenu(1);\
 UxPopdownInterface(pb);\
\
}
*NewsButton1.recomputeSize: "false"
*NewsButton1.topAttachment: "attach_widget"
*NewsButton1.topOffset: 5
*NewsButton1.topWidget: "SHelp1"
*NewsButton1.highlightColor: "Black"
*NewsButton1.leftAttachment: "attach_form"
*NewsButton1.leftOffset: 10
*NewsButton1.translations: ""
*NewsButton1.highlightOnEnter: "true"

*PrintButton1.class: pushButton
*PrintButton1.parent: workArea1
*PrintButton1.static: true
*PrintButton1.name: PrintButton1
*PrintButton1.x: 280
*PrintButton1.y: 330
*PrintButton1.width: 95
*PrintButton1.height: 30
*PrintButton1.background: ButtonBackground
*PrintButton1.labelString: "Save As..."
*PrintButton1.foreground: ButtonForeground
*PrintButton1.fontList: BoldTextFont
*PrintButton1.activateCallback: {\
extern swidget save;\
UxPopupInterface(save,exclusive_grab);\
MapRaised(save);\
\
}
*PrintButton1.recomputeSize: "false"
*PrintButton1.topAttachment: "attach_widget"
*PrintButton1.topOffset: 5
*PrintButton1.topWidget: "SHelp1"
*PrintButton1.leftAttachment: "attach_widget"
*PrintButton1.leftOffset: 10
*PrintButton1.leftWidget: "NewsButton1"
*PrintButton1.highlightColor: "Black"
*PrintButton1.translations: ""
*PrintButton1.highlightOnEnter: "true"

*ReportButton1.class: pushButton
*ReportButton1.parent: workArea1
*ReportButton1.static: true
*ReportButton1.name: ReportButton1
*ReportButton1.x: 430
*ReportButton1.y: 330
*ReportButton1.width: 95
*ReportButton1.height: 30
*ReportButton1.background: ButtonBackground
*ReportButton1.labelString: "Clear"
*ReportButton1.foreground: ButtonForeground
*ReportButton1.fontList: BoldTextFont
*ReportButton1.activateCallback: {\
\
UxPutText(scrolledText6,""); \
UxPutText(textField8,""); \
UxPutText(textField9,""); \
\
}
*ReportButton1.recomputeSize: "false"
*ReportButton1.highlightColor: "Black"
*ReportButton1.leftAttachment: "attach_widget"
*ReportButton1.leftOffset: 10
*ReportButton1.leftWidget: "PrintButton1"
*ReportButton1.topAttachment: "attach_widget"
*ReportButton1.topOffset: 5
*ReportButton1.topWidget: "SHelp1"
*ReportButton1.translations: ""
*ReportButton1.highlightOnEnter: "true"

*SendButton1.class: pushButton
*SendButton1.parent: workArea1
*SendButton1.static: true
*SendButton1.name: SendButton1
*SendButton1.x: 550
*SendButton1.y: 320
*SendButton1.width: 95
*SendButton1.height: 30
*SendButton1.background: ButtonBackground
*SendButton1.labelString: "Cancel"
*SendButton1.foreground: ButtonForeground
*SendButton1.fontList: BoldTextFont
*SendButton1.activateCallback: {\
extern swidget pb; \
SensMenu(1);\
UxPopdownInterface(pb);\
}
*SendButton1.recomputeSize: "false"
*SendButton1.highlightColor: "Black"
*SendButton1.leftAttachment: "attach_widget"
*SendButton1.leftOffset: 10
*SendButton1.leftWidget: "ReportButton1"
*SendButton1.topAttachment: "attach_widget"
*SendButton1.topOffset: 5
*SendButton1.topWidget: "SHelp1"
*SendButton1.translations: ""
*SendButton1.highlightOnEnter: "true"

*label8.class: label
*label8.parent: workArea1
*label8.static: true
*label8.name: label8
*label8.x: 0
*label8.y: 310
*label8.width: 100
*label8.height: 20
*label8.background: LabelBackground
*label8.fontList: BoldTextFont
*label8.foreground: TextForeground
*label8.labelString: "Describe here the problem, indicate if relevant how to repeat and how to fix it:"
*label8.alignment: "alignment_beginning"
*label8.bottomAttachment: "attach_widget"
*label8.bottomOffset: 2
*label8.bottomWidget: "scrolledWindow6"
*label8.leftAttachment: "attach_form"
*label8.leftOffset: 10
*label8.translations: ""
*label8.rightAttachment: "attach_form"
*label8.rightOffset: 20

*label9.class: label
*label9.parent: workArea1
*label9.static: true
*label9.name: label9
*label9.x: 0
*label9.y: 100
*label9.width: 100
*label9.height: 20
*label9.background: LabelBackground
*label9.fontList: BoldTextFont
*label9.foreground: TextForeground
*label9.labelString: "Environment:"
*label9.alignment: "alignment_beginning"
*label9.leftAttachment: "attach_form"
*label9.leftOffset: 10
*label9.topAttachment: "attach_form"
*label9.topOffset: 265
*label9.translations: ""

*label10.class: label
*label10.parent: workArea1
*label10.static: true
*label10.name: label10
*label10.x: 370
*label10.y: 50
*label10.width: 100
*label10.height: 20
*label10.background: LabelBackground
*label10.fontList: BoldTextFont
*label10.foreground: TextForeground
*label10.labelString: "Category"
*label10.alignment: "alignment_center"
*label10.leftAttachment: "attach_none"
*label10.leftOffset: 10
*label10.topAttachment: "attach_form"
*label10.topOffset: 5
*label10.translations: ""
*label10.rightAttachment: "attach_form"
*label10.rightOffset: 150

*label11.class: label
*label11.parent: workArea1
*label11.static: true
*label11.name: label11
*label11.x: 10
*label11.y: 110
*label11.width: 100
*label11.height: 20
*label11.background: LabelBackground
*label11.fontList: BoldTextFont
*label11.foreground: TextForeground
*label11.labelString: "Class"
*label11.alignment: "alignment_center"
*label11.leftAttachment: "attach_form"
*label11.leftOffset: 50
*label11.topAttachment: "attach_form"
*label11.topOffset: 5
*label11.translations: ""

*label12.class: label
*label12.parent: workArea1
*label12.static: true
*label12.name: label12
*label12.x: 20
*label12.y: 80
*label12.width: 70
*label12.height: 20
*label12.background: LabelBackground
*label12.fontList: BoldTextFont
*label12.foreground: TextForeground
*label12.labelString: "Priority:"
*label12.alignment: "alignment_beginning"
*label12.leftAttachment: "attach_form"
*label12.leftOffset: 20
*label12.topAttachment: "attach_form"
*label12.topOffset: 177
*label12.translations: ""

*label15.class: label
*label15.parent: workArea1
*label15.static: true
*label15.name: label15
*label15.x: 010
*label15.y: 190
*label15.width: 100
*label15.height: 20
*label15.background: LabelBackground
*label15.fontList: BoldTextFont
*label15.foreground: TextForeground
*label15.labelString: "Originator:"
*label15.alignment: "alignment_beginning"
*label15.leftAttachment: "attach_form"
*label15.leftOffset: 10
*label15.topAttachment: "attach_form"
*label15.topOffset: 225
*label15.translations: ""

*label17.class: label
*label17.parent: workArea1
*label17.static: true
*label17.name: label17
*label17.x: 10
*label17.y: 240
*label17.width: 100
*label17.height: 20
*label17.background: LabelBackground
*label17.fontList: BoldTextFont
*label17.foreground: TextForeground
*label17.labelString: "Synopsis:"
*label17.alignment: "alignment_beginning"
*label17.leftAttachment: "attach_form"
*label17.leftOffset: 10
*label17.topAttachment: "attach_form"
*label17.topOffset: 370
*label17.translations: ""

*textField6.class: text
*textField6.parent: workArea1
*textField6.static: true
*textField6.name: textField6
*textField6.x: 110
*textField6.y: 170
*textField6.width: 250
*textField6.height: 35
*textField6.background: TextBackground
*textField6.fontList: TextFont
*textField6.foreground: TextForeground
*textField6.text: ""
*textField6.leftAttachment: "attach_form"
*textField6.rightAttachment: "attach_form"
*textField6.rightOffset: 220
*textField6.leftOffset: 120
*textField6.topAttachment: "attach_form"
*textField6.topOffset: 220
*textField6.editable: "true"
*textField6.translations: ""
*textField6.highlightOnEnter: "true"
*textField6.createCallback: {\
\
}

*textField7.class: text
*textField7.parent: workArea1
*textField7.static: true
*textField7.name: textField7
*textField7.x: 110
*textField7.y: 210
*textField7.width: 250
*textField7.height: 35
*textField7.background: TextBackground
*textField7.fontList: TextFont
*textField7.foreground: TextForeground
*textField7.text: ""
*textField7.leftAttachment: "attach_widget"
*textField7.rightAttachment: "attach_form"
*textField7.rightOffset: 20
*textField7.leftOffset: 80
*textField7.topAttachment: "attach_form"
*textField7.topOffset: 220
*textField7.editable: "true"
*textField7.leftWidget: "textField6"
*textField7.translations: ""
*textField7.highlightOnEnter: "true"

*textField8.class: text
*textField8.parent: workArea1
*textField8.static: true
*textField8.name: textField8
*textField8.x: 110
*textField8.y: 240
*textField8.width: 250
*textField8.height: 35
*textField8.background: TextBackground
*textField8.fontList: TextFont
*textField8.foreground: TextForeground
*textField8.text: ""
*textField8.leftAttachment: "attach_form"
*textField8.rightAttachment: "attach_form"
*textField8.rightOffset: 20
*textField8.leftOffset: 120
*textField8.topAttachment: "attach_form"
*textField8.topOffset: 365
*textField8.editable: "true"
*textField8.translations: ""
*textField8.highlightOnEnter: "true"

*rowColumn1.class: rowColumn
*rowColumn1.parent: workArea1
*rowColumn1.static: true
*rowColumn1.name: rowColumn1
*rowColumn1.x: 110
*rowColumn1.y: 20
*rowColumn1.width: 150
*rowColumn1.height: 130
*rowColumn1.background: WindowBackground
*rowColumn1.orientation: "vertical"
*rowColumn1.radioBehavior: "true"
*rowColumn1.packing: "pack_tight"
*rowColumn1.navigationType: "tab_group"
*rowColumn1.entryAlignment: "alignment_beginning"
*rowColumn1.leftOffset: 10
*rowColumn1.rightAttachment: "attach_none"
*rowColumn1.rightOffset: 20
*rowColumn1.topOffset: 30
*rowColumn1.leftAttachment: "attach_form"
*rowColumn1.topAttachment: "attach_form"
*rowColumn1.resizeHeight: "false"
*rowColumn1.resizeWidth: "false"
*rowColumn1.whichButton: 2

*toggleButton1.class: toggleButton
*toggleButton1.parent: rowColumn1
*toggleButton1.static: true
*toggleButton1.name: toggleButton1
*toggleButton1.x: 3
*toggleButton1.y: 3
*toggleButton1.width: 97
*toggleButton1.height: 24
*toggleButton1.background: WindowBackground
*toggleButton1.fontList: BoldTextFont
*toggleButton1.labelString: "support"
*toggleButton1.selectColor: "Yellow"
*toggleButton1.set: "true"
*toggleButton1.translations: ""
*toggleButton1.highlightOnEnter: "true"

*toggleButton2.class: toggleButton
*toggleButton2.parent: rowColumn1
*toggleButton2.static: true
*toggleButton2.name: toggleButton2
*toggleButton2.x: 100
*toggleButton2.y: 10
*toggleButton2.width: 50
*toggleButton2.height: 100
*toggleButton2.background: WindowBackground
*toggleButton2.fontList: BoldTextFont
*toggleButton2.labelString: "sw-bug"
*toggleButton2.selectColor: "Yellow"
*toggleButton2.translations: ""
*toggleButton2.highlightOnEnter: "true"

*toggleButton3.class: toggleButton
*toggleButton3.parent: rowColumn1
*toggleButton3.static: true
*toggleButton3.name: toggleButton3
*toggleButton3.x: 180
*toggleButton3.y: 3
*toggleButton3.width: 142
*toggleButton3.height: 77
*toggleButton3.background: WindowBackground
*toggleButton3.fontList: BoldTextFont
*toggleButton3.labelString: "doc-bug"
*toggleButton3.selectColor: "Yellow"
*toggleButton3.translations: ""
*toggleButton3.highlightOnEnter: "true"

*toggleButton4.class: toggleButton
*toggleButton4.parent: rowColumn1
*toggleButton4.static: true
*toggleButton4.name: toggleButton4
*toggleButton4.x: 20
*toggleButton4.y: 60
*toggleButton4.width: 50
*toggleButton4.height: 100
*toggleButton4.background: WindowBackground
*toggleButton4.fontList: BoldTextFont
*toggleButton4.labelString: "change-request"
*toggleButton4.selectColor: "Yellow"
*toggleButton4.translations: ""
*toggleButton4.highlightOnEnter: "true"

*menu4.class: rowColumn
*menu4.parent: workArea1
*menu4.static: true
*menu4.name: menu4
*menu4.rowColumnType: "menu_option"
*menu4.subMenuId: "menu1_p4"
*menu4.background: ApplicBackground
*menu4.foreground: TextForeground
*menu4.width: 200
*menu4.translations: transTable6
*menu4.entryCallback: {\
printf("Hello\n");\
}
*menu4.height: 8
*menu4.x: 290
*menu4.y: 0
*menu4.leftAttachment: "attach_none"
*menu4.leftOffset: 260
*menu4.topAttachment: "attach_form"
*menu4.topOffset: 155
*menu4.whichButton: 1
*menu4.menuHistory: "menu1_p1_b14"
*menu4.rightAttachment: "attach_form"
*menu4.rightPosition: 50
*menu4.rightOffset: 40
*menu4.marginHeight: 0
*menu4.marginWidth: 0
*menu4.labelString: ""

*menu1_p4.class: rowColumn
*menu1_p4.parent: menu4
*menu1_p4.static: true
*menu1_p4.name: menu1_p4
*menu1_p4.rowColumnType: "menu_pulldown"
*menu1_p4.background: TextBackground
*menu1_p4.foreground: TextForeground
*menu1_p4.topShadowColor: "White"
*menu1_p4.translations: transTable6
*menu1_p4.entryCallback: {\
\
}

*menu1_p1_b13.class: pushButtonGadget
*menu1_p1_b13.parent: menu1_p4
*menu1_p1_b13.static: true
*menu1_p1_b13.name: menu1_p1_b13
*menu1_p1_b13.labelString: "alice"
*menu1_p1_b13.fontList: TextFont

*menu1_p1_b14.class: pushButtonGadget
*menu1_p1_b14.parent: menu1_p4
*menu1_p1_b14.static: true
*menu1_p1_b14.name: menu1_p1_b14
*menu1_p1_b14.labelString: "astromet"
*menu1_p1_b14.fontList: TextFont

*menu1_p4_b3.class: pushButtonGadget
*menu1_p4_b3.parent: menu1_p4
*menu1_p4_b3.static: true
*menu1_p4_b3.name: menu1_p4_b3
*menu1_p4_b3.labelString: "ccd"
*menu1_p4_b3.fontList: TextFont

*menu1_p4_b4.class: pushButtonGadget
*menu1_p4_b4.parent: menu1_p4
*menu1_p4_b4.static: true
*menu1_p4_b4.name: menu1_p4_b4
*menu1_p4_b4.labelString: "cloud"
*menu1_p4_b4.fontList: TextFont

*menu1_p4_b5.class: pushButtonGadget
*menu1_p4_b5.parent: menu1_p4
*menu1_p4_b5.static: true
*menu1_p4_b5.name: menu1_p4_b5
*menu1_p4_b5.labelString: "daophot"
*menu1_p4_b5.fontList: TextFont

*menu1_p4_b6.class: pushButtonGadget
*menu1_p4_b6.parent: menu1_p4
*menu1_p4_b6.static: true
*menu1_p4_b6.name: menu1_p4_b6
*menu1_p4_b6.labelString: "do"
*menu1_p4_b6.fontList: TextFont

*menu1_p4_b7.class: pushButtonGadget
*menu1_p4_b7.parent: menu1_p4
*menu1_p4_b7.static: true
*menu1_p4_b7.name: menu1_p4_b7
*menu1_p4_b7.labelString: "echelle"
*menu1_p4_b7.fontList: TextFont

*menu1_p4_b8.class: pushButtonGadget
*menu1_p4_b8.parent: menu1_p4
*menu1_p4_b8.static: true
*menu1_p4_b8.name: menu1_p4_b8
*menu1_p4_b8.labelString: "esolv"
*menu1_p4_b8.fontList: TextFont

*menu1_p4_b9.class: pushButtonGadget
*menu1_p4_b9.parent: menu1_p4
*menu1_p4_b9.static: true
*menu1_p4_b9.name: menu1_p4_b9
*menu1_p4_b9.labelString: "geotest"
*menu1_p4_b9.fontList: TextFont

*menu1_p4_b10.class: pushButtonGadget
*menu1_p4_b10.parent: menu1_p4
*menu1_p4_b10.static: true
*menu1_p4_b10.name: menu1_p4_b10
*menu1_p4_b10.labelString: "imres"
*menu1_p4_b10.fontList: TextFont

*menu1_p4_b11.class: pushButtonGadget
*menu1_p4_b11.parent: menu1_p4
*menu1_p4_b11.static: true
*menu1_p4_b11.name: menu1_p4_b11
*menu1_p4_b11.labelString: "invent"
*menu1_p4_b11.fontList: TextFont

*menu1_p4_b12.class: pushButtonGadget
*menu1_p4_b12.parent: menu1_p4
*menu1_p4_b12.static: true
*menu1_p4_b12.name: menu1_p4_b12
*menu1_p4_b12.labelString: "irspec"
*menu1_p4_b12.fontList: TextFont

*menu1_p4_b13.class: pushButtonGadget
*menu1_p4_b13.parent: menu1_p4
*menu1_p4_b13.static: true
*menu1_p4_b13.name: menu1_p4_b13
*menu1_p4_b13.labelString: "iue"
*menu1_p4_b13.fontList: TextFont

*menu1_p4_b14.class: pushButtonGadget
*menu1_p4_b14.parent: menu1_p4
*menu1_p4_b14.static: true
*menu1_p4_b14.name: menu1_p4_b14
*menu1_p4_b14.labelString: "long"
*menu1_p4_b14.fontList: TextFont

*menu1_p4_b15.class: pushButtonGadget
*menu1_p4_b15.parent: menu1_p4
*menu1_p4_b15.static: true
*menu1_p4_b15.name: menu1_p4_b15
*menu1_p4_b15.labelString: "lyman"
*menu1_p4_b15.fontList: TextFont

*menu1_p4_b16.class: pushButtonGadget
*menu1_p4_b16.parent: menu1_p4
*menu1_p4_b16.static: true
*menu1_p4_b16.name: menu1_p4_b16
*menu1_p4_b16.labelString: "mos"
*menu1_p4_b16.fontList: TextFont

*menu1_p4_b17.class: pushButtonGadget
*menu1_p4_b17.parent: menu1_p4
*menu1_p4_b17.static: true
*menu1_p4_b17.name: menu1_p4_b17
*menu1_p4_b17.labelString: "mva"
*menu1_p4_b17.fontList: TextFont

*menu1_p4_b18.class: pushButtonGadget
*menu1_p4_b18.parent: menu1_p4
*menu1_p4_b18.static: true
*menu1_p4_b18.name: menu1_p4_b18
*menu1_p4_b18.labelString: "optopus"
*menu1_p4_b18.fontList: TextFont

*menu1_p4_b19.class: pushButtonGadget
*menu1_p4_b19.parent: menu1_p4
*menu1_p4_b19.static: true
*menu1_p4_b19.name: menu1_p4_b19
*menu1_p4_b19.labelString: "pepsys"
*menu1_p4_b19.fontList: TextFont

*menu1_p4_b20.class: pushButtonGadget
*menu1_p4_b20.parent: menu1_p4
*menu1_p4_b20.static: true
*menu1_p4_b20.name: menu1_p4_b20
*menu1_p4_b20.labelString: "pisco"
*menu1_p4_b20.fontList: TextFont

*menu1_p4_b21.class: pushButtonGadget
*menu1_p4_b21.parent: menu1_p4
*menu1_p4_b21.static: true
*menu1_p4_b21.name: menu1_p4_b21
*menu1_p4_b21.labelString: "romafot"
*menu1_p4_b21.fontList: TextFont

*menu1_p4_b22.class: pushButtonGadget
*menu1_p4_b22.parent: menu1_p4
*menu1_p4_b22.static: true
*menu1_p4_b22.name: menu1_p4_b22
*menu1_p4_b22.labelString: "spec"
*menu1_p4_b22.fontList: TextFont

*menu1_p4_b23.class: pushButtonGadget
*menu1_p4_b23.parent: menu1_p4
*menu1_p4_b23.static: true
*menu1_p4_b23.name: menu1_p4_b23
*menu1_p4_b23.labelString: "statist"
*menu1_p4_b23.fontList: TextFont

*menu1_p4_b24.class: pushButtonGadget
*menu1_p4_b24.parent: menu1_p4
*menu1_p4_b24.static: true
*menu1_p4_b24.name: menu1_p4_b24
*menu1_p4_b24.labelString: "surfphot"
*menu1_p4_b24.fontList: TextFont

*menu1_p4_b25.class: pushButtonGadget
*menu1_p4_b25.parent: menu1_p4
*menu1_p4_b25.static: true
*menu1_p4_b25.name: menu1_p4_b25
*menu1_p4_b25.labelString: "tsa"
*menu1_p4_b25.fontList: TextFont

*menu1_p4_b26.class: pushButtonGadget
*menu1_p4_b26.parent: menu1_p4
*menu1_p4_b26.static: true
*menu1_p4_b26.name: menu1_p4_b26
*menu1_p4_b26.labelString: "wavelet"
*menu1_p4_b26.fontList: TextFont

*menu5.class: rowColumn
*menu5.parent: workArea1
*menu5.static: true
*menu5.name: menu5
*menu5.rowColumnType: "menu_option"
*menu5.subMenuId: "menu1_p5"
*menu5.background: ApplicBackground
*menu5.foreground: TextForeground
*menu5.width: 200
*menu5.translations: transTable6
*menu5.entryCallback: {\
printf("Hello\n");\
}
*menu5.height: 10
*menu5.x: 470
*menu5.y: 10
*menu5.leftAttachment: "attach_form"
*menu5.leftOffset: 80
*menu5.topAttachment: "attach_form"
*menu5.topOffset: 167
*menu5.menuHistory: "menu1_p1_b16"
*menu5.labelString: ""

*menu1_p5.class: rowColumn
*menu1_p5.parent: menu5
*menu1_p5.static: true
*menu1_p5.name: menu1_p5
*menu1_p5.rowColumnType: "menu_pulldown"
*menu1_p5.background: TextBackground
*menu1_p5.foreground: TextForeground
*menu1_p5.topShadowColor: "White"
*menu1_p5.translations: transTable6
*menu1_p5.entryCallback: {\
\
}

*menu1_p1_b15.class: pushButtonGadget
*menu1_p1_b15.parent: menu1_p5
*menu1_p1_b15.static: true
*menu1_p1_b15.name: menu1_p1_b15
*menu1_p1_b15.labelString: "low"
*menu1_p1_b15.accelerator: "m"
*menu1_p1_b15.fontList: TextFont

*menu1_p1_b16.class: pushButtonGadget
*menu1_p1_b16.parent: menu1_p5
*menu1_p1_b16.static: true
*menu1_p1_b16.name: menu1_p1_b16
*menu1_p1_b16.labelString: "medium"
*menu1_p1_b16.accelerator: "d"
*menu1_p1_b16.fontList: TextFont

*menu1_p4_b1.class: pushButtonGadget
*menu1_p4_b1.parent: menu1_p5
*menu1_p4_b1.static: true
*menu1_p4_b1.name: menu1_p4_b1
*menu1_p4_b1.labelString: "high"
*menu1_p4_b1.fontList: TextFont

*rowColumn2.class: rowColumn
*rowColumn2.parent: workArea1
*rowColumn2.static: true
*rowColumn2.name: rowColumn2
*rowColumn2.x: 112
*rowColumn2.y: 77
*rowColumn2.width: 350
*rowColumn2.height: 160
*rowColumn2.background: WindowBackground
*rowColumn2.orientation: "vertical"
*rowColumn2.radioBehavior: "true"
*rowColumn2.packing: "pack_tight"
*rowColumn2.navigationType: "tab_group"
*rowColumn2.entryAlignment: "alignment_beginning"
*rowColumn2.resizeHeight: "false"
*rowColumn2.resizeWidth: "false"
*rowColumn2.leftAttachment: "attach_none"
*rowColumn2.leftOffset: 120
*rowColumn2.rightAttachment: "attach_form"
*rowColumn2.rightOffset: 20
*rowColumn2.topAttachment: "attach_form"
*rowColumn2.topOffset: 30
*rowColumn2.numColumns: 2
*rowColumn2.shadowThickness: 3
*rowColumn2.whichButton: 0
*rowColumn2.menuHistory: ""

*toggleButton5.class: toggleButton
*toggleButton5.parent: rowColumn2
*toggleButton5.static: true
*toggleButton5.name: toggleButton5
*toggleButton5.x: 3
*toggleButton5.y: 3
*toggleButton5.width: 97
*toggleButton5.height: 24
*toggleButton5.background: WindowBackground
*toggleButton5.fontList: BoldTextFont
*toggleButton5.labelString: "Installation"
*toggleButton5.selectColor: "Yellow"
*toggleButton5.set: "false"
*toggleButton5.translations: ""
*toggleButton5.highlightOnEnter: "true"
*toggleButton5.indicatorOn: "true"

*toggleButton6.class: toggleButton
*toggleButton6.parent: rowColumn2
*toggleButton6.static: true
*toggleButton6.name: toggleButton6
*toggleButton6.x: 100
*toggleButton6.y: 10
*toggleButton6.width: 50
*toggleButton6.height: 100
*toggleButton6.background: WindowBackground
*toggleButton6.fontList: BoldTextFont
*toggleButton6.labelString: "Distribution"
*toggleButton6.selectColor: "Yellow"
*toggleButton6.translations: ""
*toggleButton6.highlightOnEnter: "true"

*toggleButton7.class: toggleButton
*toggleButton7.parent: rowColumn2
*toggleButton7.static: true
*toggleButton7.name: toggleButton7
*toggleButton7.x: 60
*toggleButton7.y: 50
*toggleButton7.width: 50
*toggleButton7.height: 100
*toggleButton7.background: WindowBackground
*toggleButton7.fontList: BoldTextFont
*toggleButton7.labelString: "System"
*toggleButton7.selectColor: "Yellow"
*toggleButton7.translations: ""
*toggleButton7.highlightOnEnter: "true"
*toggleButton7.set: "true"

*toggleButton8.class: toggleButton
*toggleButton8.parent: rowColumn2
*toggleButton8.static: true
*toggleButton8.name: toggleButton8
*toggleButton8.x: 20
*toggleButton8.y: 60
*toggleButton8.width: 50
*toggleButton8.height: 100
*toggleButton8.background: WindowBackground
*toggleButton8.fontList: BoldTextFont
*toggleButton8.labelString: "Display"
*toggleButton8.selectColor: "Yellow"
*toggleButton8.translations: ""
*toggleButton8.highlightOnEnter: "true"

*toggleButton9.class: toggleButton
*toggleButton9.parent: rowColumn2
*toggleButton9.static: true
*toggleButton9.name: toggleButton9
*toggleButton9.x: 188
*toggleButton9.y: -35
*toggleButton9.width: 50
*toggleButton9.height: 100
*toggleButton9.background: WindowBackground
*toggleButton9.fontList: BoldTextFont
*toggleButton9.labelString: "Graphics"
*toggleButton9.selectColor: "Yellow"
*toggleButton9.translations: ""
*toggleButton9.highlightOnEnter: "true"

*toggleButton10.class: toggleButton
*toggleButton10.parent: rowColumn2
*toggleButton10.static: true
*toggleButton10.name: toggleButton10
*toggleButton10.x: 188
*toggleButton10.y: -35
*toggleButton10.width: 50
*toggleButton10.height: 100
*toggleButton10.background: WindowBackground
*toggleButton10.fontList: BoldTextFont
*toggleButton10.labelString: "Tables"
*toggleButton10.selectColor: "Yellow"
*toggleButton10.translations: ""
*toggleButton10.highlightOnEnter: "true"

*toggleButton11.class: toggleButton
*toggleButton11.parent: rowColumn2
*toggleButton11.static: true
*toggleButton11.name: toggleButton11
*toggleButton11.x: 188
*toggleButton11.y: -35
*toggleButton11.width: 50
*toggleButton11.height: 100
*toggleButton11.background: WindowBackground
*toggleButton11.fontList: BoldTextFont
*toggleButton11.labelString: "Help & Documentation"
*toggleButton11.selectColor: "Yellow"
*toggleButton11.translations: ""
*toggleButton11.highlightOnEnter: "true"

*toggleButton12.class: toggleButton
*toggleButton12.parent: rowColumn2
*toggleButton12.static: true
*toggleButton12.name: toggleButton12
*toggleButton12.x: 188
*toggleButton12.y: -35
*toggleButton12.width: 50
*toggleButton12.height: 100
*toggleButton12.background: WindowBackground
*toggleButton12.fontList: BoldTextFont
*toggleButton12.labelString: "Image Operations"
*toggleButton12.selectColor: "Yellow"
*toggleButton12.translations: ""
*toggleButton12.highlightOnEnter: "true"

*toggleButton13.class: toggleButton
*toggleButton13.parent: rowColumn2
*toggleButton13.static: true
*toggleButton13.name: toggleButton13
*toggleButton13.x: 20
*toggleButton13.y: 60
*toggleButton13.width: 50
*toggleButton13.height: 100
*toggleButton13.background: WindowBackground
*toggleButton13.fontList: BoldTextFont
*toggleButton13.labelString: "Graphical User Interfaces"
*toggleButton13.selectColor: "Yellow"
*toggleButton13.translations: ""
*toggleButton13.highlightOnEnter: "true"

*toggleButton14.class: toggleButton
*toggleButton14.parent: rowColumn2
*toggleButton14.static: true
*toggleButton14.name: toggleButton14
*toggleButton14.x: 20
*toggleButton14.y: 60
*toggleButton14.width: 50
*toggleButton14.height: 100
*toggleButton14.background: WindowBackground
*toggleButton14.fontList: BoldTextFont
*toggleButton14.labelString: "Context: "
*toggleButton14.selectColor: "Yellow"
*toggleButton14.translations: ""
*toggleButton14.highlightOnEnter: "true"

*text3.class: text
*text3.parent: workArea1
*text3.static: true
*text3.name: text3
*text3.x: 130
*text3.y: 250
*text3.width: 440
*text3.height: 60
*text3.leftAttachment: "attach_form"
*text3.leftOffset: 120
*text3.rightAttachment: "attach_form"
*text3.rightOffset: 20
*text3.topAttachment: "attach_form"
*text3.topOffset: 260
*text3.background: TextBackground
*text3.editMode: "multi_line_edit"
*text3.fontList: TextFont
*text3.foreground: TextForeground
*text3.highlightOnEnter: "true"
*text3.translations: ""
*text3.createCallback: {\
get_subproc(UxWidget,1);\
\
}
*text3.autoShowCursorPosition: "true"
*text3.focusCallback: {\
\
}

*label18.class: label
*label18.parent: workArea1
*label18.static: true
*label18.name: label18
*label18.x: 10
*label18.y: 280
*label18.width: 100
*label18.height: 20
*label18.background: LabelBackground
*label18.fontList: BoldTextFont
*label18.foreground: TextForeground
*label18.labelString: "Command:"
*label18.alignment: "alignment_beginning"
*label18.leftAttachment: "attach_form"
*label18.topAttachment: "attach_form"
*label18.topOffset: 330
*label18.leftOffset: 10
*label18.translations: ""

*textField9.class: text
*textField9.parent: workArea1
*textField9.static: true
*textField9.name: textField9
*textField9.x: 110
*textField9.y: 270
*textField9.width: 250
*textField9.height: 35
*textField9.background: TextBackground
*textField9.fontList: TextFont
*textField9.foreground: TextForeground
*textField9.text: ""
*textField9.editable: "true"
*textField9.leftAttachment: "attach_form"
*textField9.leftOffset: 120
*textField9.rightAttachment: "attach_form"
*textField9.rightOffset: 20
*textField9.topAttachment: "attach_form"
*textField9.topOffset: 325
*textField9.translations: ""
*textField9.highlightOnEnter: "true"

*label16.class: label
*label16.parent: workArea1
*label16.static: true
*label16.name: label16
*label16.x: 10
*label16.y: 220
*label16.width: 70
*label16.height: 20
*label16.background: LabelBackground
*label16.fontList: BoldTextFont
*label16.foreground: TextForeground
*label16.labelString: "Release:"
*label16.alignment: "alignment_beginning"
*label16.leftAttachment: "attach_widget"
*label16.leftOffset: 5
*label16.topAttachment: "attach_form"
*label16.topOffset: 225
*label16.leftWidget: "textField6"
*label16.translations: ""

*separator1.class: separator
*separator1.parent: workArea1
*separator1.static: true
*separator1.name: separator1
*separator1.x: 200
*separator1.y: 210
*separator1.width: 350
*separator1.height: 4
*separator1.separatorType: "single_line"
*separator1.background: WindowBackground
*separator1.topAttachment: "attach_widget"
*separator1.topOffset: 15
*separator1.topWidget: "rowColumn2"
*separator1.rightAttachment: "attach_form"
*separator1.rightOffset: 20
*separator1.rightWidget: "rowColumn2"
*separator1.leftAttachment: "attach_form"
*separator1.leftOffset: 10
*separator1.leftWidget: "rowColumn2"

*separator2.class: separator
*separator2.parent: workArea1
*separator2.static: true
*separator2.name: separator2
*separator2.x: 200
*separator2.y: 210
*separator2.width: 4
*separator2.height: 200
*separator2.separatorType: "single_line"
*separator2.background: WindowBackground
*separator2.orientation: "vertical"
*separator2.leftAttachment: "attach_none"
*separator2.leftOffset: 0
*separator2.leftWidget: "rowColumn2"
*separator2.bottomAttachment: "attach_widget"
*separator2.bottomWidget: "separator1"
*separator2.topAttachment: "attach_form"
*separator2.topOffset: 15
*separator2.topWidget: "rowColumn2"
*separator2.rightAttachment: "attach_widget"
*separator2.rightOffset: 0
*separator2.rightWidget: "rowColumn2"
*separator2.bottomOffset: 0

*separator3.class: separator
*separator3.parent: workArea1
*separator3.static: true
*separator3.name: separator3
*separator3.x: 200
*separator3.y: 210
*separator3.width: 4
*separator3.height: 200
*separator3.separatorType: "single_line"
*separator3.background: WindowBackground
*separator3.orientation: "vertical"
*separator3.leftAttachment: "attach_widget"
*separator3.leftOffset: 0
*separator3.leftWidget: "rowColumn2"
*separator3.rightAttachment: "attach_none"
*separator3.rightOffset: 0
*separator3.rightWidget: "rowColumn2"
*separator3.topAttachment: "attach_form"
*separator3.topOffset: 15
*separator3.topWidget: "rowColumn2"
*separator3.bottomAttachment: "attach_opposite_widget"
*separator3.bottomOffset: 0
*separator3.bottomWidget: "separator1"

*separator4.class: separator
*separator4.parent: workArea1
*separator4.static: true
*separator4.name: separator4
*separator4.x: 200
*separator4.y: 210
*separator4.width: 350
*separator4.height: 4
*separator4.separatorType: "single_line"
*separator4.background: WindowBackground
*separator4.topAttachment: "attach_widget"
*separator4.topOffset: 0
*separator4.topWidget: "rowColumn1"
*separator4.rightAttachment: "attach_widget"
*separator4.rightOffset: 0
*separator4.rightWidget: "separator2"
*separator4.leftAttachment: "attach_opposite_widget"
*separator4.leftOffset: 0
*separator4.leftWidget: "rowColumn1"

*separator5.class: separator
*separator5.parent: workArea1
*separator5.static: true
*separator5.name: separator5
*separator5.x: 200
*separator5.y: 210
*separator5.width: 4
*separator5.height: 200
*separator5.separatorType: "single_line"
*separator5.background: WindowBackground
*separator5.orientation: "vertical"
*separator5.bottomAttachment: "attach_widget"
*separator5.bottomOffset: 0
*separator5.bottomWidget: "separator1"
*separator5.rightAttachment: "attach_widget"
*separator5.rightOffset: 0
*separator5.rightWidget: "rowColumn1"
*separator5.topAttachment: "attach_form"
*separator5.topOffset: 15

*separator6.class: separator
*separator6.parent: workArea1
*separator6.static: true
*separator6.name: separator6
*separator6.x: 200
*separator6.y: 210
*separator6.width: 350
*separator6.height: 4
*separator6.separatorType: "single_line"
*separator6.background: WindowBackground
*separator6.leftAttachment: "attach_form"
*separator6.rightAttachment: "attach_form"
*separator6.rightOffset: 20
*separator6.topAttachment: "attach_form"
*separator6.topOffset: 15
*separator6.leftOffset: 10

