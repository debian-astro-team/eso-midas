! UIMX ascii 2.0 key: 4185                                                      
*action.HelpACT: {\
 char *s;\
\
 if ((s = XmTextGetSelection(UxWidget)) == NULL) return; \
\
 if (s[0] == '/') GetHelp (s," ",1L); \
 else             GetHelp (s,"?",1L); \
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
}
*action.WriteHelp: {\
WriteHelpAW(UxWidget);\
helpPR(UxWidget);\
\
}
*action.SelectCommand: {\
\
 extern SelHelp();\
 char *s;\
\
 if ((s = XmTextGetSelection(UxWidget)) == NULL) return; \
\
 SelHelp(s);\
\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
\
}
*action.HInit: {\
InitHelp();\
}

*translation.table: HelpTable
*translation.parent: ApplicWindow
*translation.policy: replace
*translation.<Btn1Up>: HelpACT()
*translation.<Btn1Down>: grab-focus()
*translation.<EnterWindow>: WriteHelp()

*translation.table: TextTable
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Up> : SelectCommand()

*translation.table: transTable10
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<EnterWindow>: WriteHelp() Enter()

*ApplicWindow.class: applicationShell
*ApplicWindow.parent: NO_PARENT
*ApplicWindow.static: false
*ApplicWindow.gbldecl: #include <ExternResources.h>\
#include <stdio.h>\
#include <Xm/Protocols.h>\
#include <UxFsBox.h>\
#include <Xm/Text.h>\
\
/* Definitions for History mechanism */\
\
#define SIZHIST 50\
#define SIZTEXT 40\
int  ItemNumber = 0, ItemPosition = 0;\
int  DispItem = 0; \
int  Overview[2] = {1,0};\
\
char ItemHistory[SIZHIST][SIZTEXT];\
char HistList[SIZHIST*SIZTEXT];\
\
/* Variables for SelectionArray */\
\
XmTextScanType Sarray[60];\
int            SarI = 0;\
\
\
/* Include for UxPut calls on the fileSelectionBox */\
\
void CreateWindowManagerProtocols();\
void ExitCB();\
void CreateSessionManagerProtocols();\
void SaveSessionCB();\
\
extern Boolean saving;  /* declared in file selection box declarations */\
extern swidget fileSelectionDialog;\
extern swidget fileSelectionBox;\
extern swidget exitDialog;\
extern swidget create_fileSelectionDialog();\
extern swidget create_exitDialog();
*ApplicWindow.ispecdecl:
*ApplicWindow.funcdecl: swidget popup_ApplicWindow()\

*ApplicWindow.funcname: popup_ApplicWindow
*ApplicWindow.funcdef: "swidget", "<popup_ApplicWindow>(%)"
*ApplicWindow.icode:
*ApplicWindow.fcode: PutSelection(text1,0);\
PutSelection(text2,2);\
PutSelection(scrolledText,1);\
\
return(rtrn);\

*ApplicWindow.auxdecl: GetHelp(s,type,inc)\
\
char *s,*type;\
long int inc;\
\
{\
\
char  *s2;\
char  MenuItem[40], command[100], item[100];\
extern char contxt[], midvers[];\
XmString GetList();\
void     DispList(); \
\
int stat, pos, i=0;\
\
/* Store Overview Panel position */\
if (s[0] != '?'&& Overview[0] == 1) StoreOverviewPosition(0);\
\
if (type[0] == '?')\
  strcpy(MenuItem,\
     UxGetLabelString(UxFindSwidget(UxGetMenuHistory(menu1))) );\
else\
  strcpy(MenuItem,type);\
\
  if (MenuItem[0] == 'C' && MenuItem[2] == 'm' )  strcpy(command,"");\
  if (MenuItem[0] == 'A')  strcpy(command,"/APPLIC ");\
  if (MenuItem[0] == 'P')  strcpy(command,"/CL ");\
  if (MenuItem[0] == 'K')  strcpy(command,"/KEYWORD ");\
  if (MenuItem[0] == 'S')  strcpy(command,"/SUBJECT ");\
  if (MenuItem[0] == 'C' && MenuItem[2] == 'n' )  strcpy(command,"/CONTRIB ");\
\
  if (MenuItem[0] == ' ') \
               strcpy(command,"");\
\
/* \
   Filter out unallowed characters in string s \
   Allowed characters are a-z, A-Z, and _ @ $ / [] ? * and Space\
*/\
\
\
for (pos=0; pos<(int)strlen(s); pos++) {\
   if (s[pos] >= 'a' && s[pos] <= 'z')  item[i++] = s[pos];\
   if (s[pos] >= 'A' && s[pos] <= 'Z')  item[i++] = s[pos];\
   if (s[pos] == '$' || s[pos] == '_' || s[pos] == '/')\
         item[i++] = s[pos];\
   if (s[pos] == '[' || s[pos] == ']') item[i++] = s[pos];\
   if (s[pos] == '?' || s[pos] == '*') item[i++] = s[pos];\
   if (s[pos] == ' ') item[i++] = s[pos];\
   if (s[pos] == '@') item[i++] = s[pos];\
   if (s[pos] >= '0' && s[pos] <= '9') item[i++] = s[pos];\
}   \
\
item[i] = '\0';\
strcat (command,item); \
\
\
   stat = helpme(2L, midvers, command, contxt, &s2);\
\
   if (stat == 0) {\
\
    /* Display Help message */\
    XmTextSetString(UxGetWidget(scrolledText),s2); \
    XmTextSetString(UxGetWidget(SHelp),"");\
\
    /* Positions Overview Panel */\
    if (s[0] == '?') SetOverviewPosition(1);\
\
    /* Clears all highlights */\
    XmTextSetHighlight(UxGetWidget(scrolledText), 0, \
           strlen(s2), XmHIGHLIGHT_NORMAL);\
\
    /* Update History List */\
     if (inc != 0) {\
      if (item[0] != '?') \
          strcpy(ItemHistory[ItemPosition++],command);\
      if (ItemPosition == SIZHIST) ItemPosition = 0;\
      ItemNumber++;\
      DispItem = ItemNumber - 2;\
      UxPutText(text2,GetList());\
      DispItem = ItemNumber - 1; \
\
      /* Builds History List and Display it */\
      {\
      int LastItem, FirstItem=0, Item, ListPosition=0, stat;\
      char s[SIZTEXT*SIZHIST];\
\
      LastItem = ItemPosition - 1;\
      if (LastItem < 0) LastItem += SIZHIST;\
      if (ItemNumber >= SIZHIST) FirstItem = LastItem + 1; \
\
      strcpy(s,"");\
\
      for (Item = LastItem; Item>=0; Item--) {\
        strcat(s,ItemHistory[Item]);\
        strcat(&s[ListPosition],"\n");\
        } \
  \
      if (ItemNumber >= SIZHIST) {\
      for (Item = SIZHIST-1; Item >= FirstItem; Item--) {\
          strcat(s,ItemHistory[Item]);\
          strcat(&s[ListPosition],"\n");\
          } \
       }      \
\
       DispList(s);\
       }\
 \
     }\
   }\
   else\
     XmTextSetString(UxGetWidget(SHelp),s2); \
\
/*{ End of if (s != 0) { */\
\
}\
\
\
InitHelp()\
\
{\
\
 GetHelp("?","?",0L);\
 XmTextSetString(UxGetWidget(text1),"");\
\
}\
\
SearchHelp(s,dir)\
\
char *s;\
long int dir;\
\
{\
\
/* SearchHelp should strip leading and trailing blanks */\
\
char *HText;\
Widget SText;\
\
XmTextPosition PosText = 0;\
\
int LText;\
int relpos; /* Relative position */\
\
HText = UxGetText(scrolledText);\
SText = UxGetWidget(scrolledText);\
\
/* Initialisations */\
\
LText    = strlen(HText);\
XmTextSetHighlight(SText, 0, LText, XmHIGHLIGHT_NORMAL);\
\
/* Set all characters to upper case for case insensitivity */\
\
for (PosText=0; PosText<LText; PosText++) \
          HText[PosText] = toupper(HText[PosText]); \
\
for (PosText=0; PosText<(int)strlen(s); PosText++) \
          s[PosText] = toupper(s[PosText]); \
\
PosText = 0;\
\
/* If Show, shows all occurences */\
\
if (dir > 0) {\
\
   relpos  = strindex(HText,s);\
\
   if (relpos == LText) \
       XmTextSetString(UxGetWidget(SHelp),"Pattern not found");\
   else\
       XmTextShowPosition(SText,(XmTextPosition) relpos);  \
 \
   while (relpos != LText) {\
\
     PosText += relpos;\
\
     XmTextSetHighlight(SText,PosText,\
             PosText+strlen(s),XmHIGHLIGHT_SELECTED);\
\
     PosText++;\
\
     LText    = strlen(HText+PosText);\
     relpos   = strindex(HText+PosText,s);\
\
    }\
  }\
}\
\
XmString GetList()\
\
{\
\
\
int n;\
int quotient, module, minitem;\
\
n = DispItem;\
\
if (n >= ItemNumber)       n = ItemNumber-1;\
\
if (ItemNumber < SIZHIST)  minitem = 0;\
else                       minitem = ItemNumber - SIZHIST;\
\
if (n <  minitem)  n = minitem;\
\
DispItem = n;\
\
quotient = (int) n/SIZHIST;\
module = n - quotient*SIZHIST;\
\
return( (XmString) (ItemHistory+module) );\
\
}\
\
SelHelp(s)\
\
char *s;\
\
{\
\
if (s[0] == '/') {\
\
   if (toupper(s[1]) == 'A') \
         UxPutMenuHistory(menu1,"menu1_p1_b2"); /* APPLIC */\
\
\
   if (toupper(s[1]) == 'C') \
         UxPutMenuHistory(menu1,"menu1_p1_b3");  /*  CL */\
\
   if (toupper(s[1]) == 'K') \
         UxPutMenuHistory(menu1,"menu1_p1_b4");  /*  KEYWORD */\
\
   if (toupper(s[1]) == 'S') \
         UxPutMenuHistory(menu1,"menu1_p1_b6");  /*  SUBJECT */\
\
   GetHelp(s," ",0L);\
   }\
else  {\
   UxPutMenuHistory(menu1,"menu1_p1_b1");\
   GetHelp(s,"?",0L);\
   }\
}\
\
PutSelection(sw, mode)\
\
swidget sw;\
int     mode;\
\
{\
        XmTextScanType *array;\
        Arg            args[2];\
        int            n = 0, i;\
        Widget         wid;\
\
        wid = UxGetWidget(sw);\
\
        XtSetArg(args[n], XmNselectionArray,  &array), n++;\
        XtGetValues(wid , args, n);\
 \
        /* printf("SarI, Widget ID: %i , %i \n",SarI,wid); */\
\
          for (i=0; i<=3; i++) \
            Sarray[SarI+i] = array[i];\
\
/*      for (i=0; i<=3; i++) {\
         if (array[i] == XmSELECT_POSITION) printf("%d : Position\n",i);\
         if (array[i] == XmSELECT_WORD)     printf("%d : Word\n",i);\
         if (array[i] == XmSELECT_LINE)     printf("%d : Line\n",i);\
         if (array[i] == XmSELECT_ALL)      printf("%d : All\n",i);\
       }\
*/\
\
        for (i=0; i<=3; i++)\
           Sarray[SarI+i] = Sarray[SarI+mode];\
\
        n = 0;\
        XtSetArg(args[n], XmNselectionArray,  (Sarray+SarI)), n++;\
        XtSetValues(wid , args, n);\
        SarI += 4;\
}\
\
SensMenu(mode)\
\
int  mode;\
{\
if (mode == 0) XtSetSensitive(UxGetWidget(SysEntry),FALSE);\
if (mode == 1) XtSetSensitive(UxGetWidget(SysEntry),TRUE);\
}\
\
WriteHelpAW(UxWidget)\
\
Widget UxWidget;\
{\
char s[100];\
extern char print_com[];\
\
strcpy(s,"");\
\
if (UxWidget == UxGetWidget(CommandButton))\
   strcpy(s,"Display an overview of the commands");\
\
if (UxWidget == UxGetWidget(HistoButton))\
   strcpy(s,"Popup the history list of previously queried commands");\
\
if (UxWidget == UxGetWidget(SearchButton))\
   strcpy(s,"Search strings in help file");\
\
if (UxWidget == UxGetWidget(NewsButton))\
   strcpy(s,"Display Release News");\
\
if (UxWidget == UxGetWidget(PrintButton))\
   sprintf(s,"Send help message to the printer. (Command: cat <file> | %s)\n",\
           print_com);\
\
if (UxWidget == UxGetWidget(ReportButton))\
   strcpy(s,"Popup the problem report form");\
\
if (UxWidget == UxGetWidget(CtxButton))\
   strcpy(s,"Popup the context selection form");\
\
if (UxWidget == UxGetWidget(text1))\
   strcpy(s,"Commands names can be typed in this area. (Get help by Return)");\
\
if (UxWidget == UxGetWidget(text2))\
   strcpy(s,"Non editable area. Displays previous command. Click to select.");\
\
if (UxWidget == UxGetWidget(scrolledWindow))\
   strcpy(s,"Help files display area");\
\
if (UxWidget == UxGetWidget(arrowButton2))\
   strcpy(s,"Moves forward in the history list of commands");\
\
if (UxWidget == UxGetWidget(arrowButton1))\
   strcpy(s,"Moves backward in the history list of commands");\
\
if (UxWidget == UxGetWidget(scrolledText))\
   strcpy(s,"Point and Click in this area to access help files");\
\
\
if (UxWidget == UxGetWidget(menu1))\
   strcpy(s,"Information mode");\
\
if (UxWidget == UxGetWidget(commandsCascade))\
   strcpy(s,"Setup options and Exit");\
\
XmTextSetString(UxGetWidget(SHelp),s);\
\
}\
\
MapRaised(sw)\
\
swidget sw;\
{\
Window xw;\
xw = XtWindow(UxGetWidget(sw));\
XMapRaised(UxDisplay, xw);\
}\
\
StoreOverviewPosition(mode)\
int mode;\
{\
/* Store Overview Panel position */\
    Overview[0] = mode;\
    Overview[1] = (int) XmTextGetTopCharacter(UxGetWidget(scrolledText));\
}\
\
SetOverviewPosition(mode)\
int mode;\
{\
/* Positions Overview Panel */\
     Overview[0] = mode;\
     XmTextSetTopCharacter(UxGetWidget(scrolledText),\
                        (XmTextPosition) Overview[1]);\
}\

*ApplicWindow.name: ApplicWindow
*ApplicWindow.x: 550
*ApplicWindow.y: 577
*ApplicWindow.width: 700
*ApplicWindow.height: 600
*ApplicWindow.title: "XHelp"
*ApplicWindow.deleteResponse: "do_nothing"
*ApplicWindow.keyboardFocusPolicy.source: public
*ApplicWindow.keyboardFocusPolicy: "pointer"
*ApplicWindow.iconName: "XHelp"
*ApplicWindow.popupCallback: {\
\
}
*ApplicWindow.background: WindowBackground
*ApplicWindow.geometry: "700x480+30+30"

*mainWindow.class: mainWindow
*mainWindow.parent: ApplicWindow
*mainWindow.static: true
*mainWindow.name: mainWindow
*mainWindow.x: 200
*mainWindow.y: 180
*mainWindow.width: 700
*mainWindow.height: 470
*mainWindow.background: WindowBackground

*pullDownMenu.class: rowColumn
*pullDownMenu.parent: mainWindow
*pullDownMenu.static: true
*pullDownMenu.name: pullDownMenu
*pullDownMenu.borderWidth: 0
*pullDownMenu.menuHelpWidget: ""
*pullDownMenu.rowColumnType: "menu_bar"
*pullDownMenu.menuAccelerator: "<KeyUp>F10"
*pullDownMenu.background: MenuBackground
*pullDownMenu.foreground: MenuForeground

*UtilsPane.class: rowColumn
*UtilsPane.parent: pullDownMenu
*UtilsPane.static: true
*UtilsPane.name: UtilsPane
*UtilsPane.rowColumnType: "menu_pulldown"
*UtilsPane.background: MenuBackground
*UtilsPane.foreground: MenuForeground

*SysEntry.class: pushButtonGadget
*SysEntry.parent: UtilsPane
*SysEntry.static: true
*SysEntry.name: SysEntry
*SysEntry.labelString: "Print & Mail..."
*SysEntry.mnemonic: "S"
*SysEntry.fontList: BoldTextFont
*SysEntry.accelerator: "S"
*SysEntry.activateCallback: {\
extern swidget sys; \
UxPopupInterface(sys,no_grab);\
MapRaised(sys);\
}

*UtilsPane_b2.class: pushButtonGadget
*UtilsPane_b2.parent: UtilsPane
*UtilsPane_b2.static: true
*UtilsPane_b2.name: UtilsPane_b2
*UtilsPane_b2.labelString: "Exit"
*UtilsPane_b2.activateCallback: {\
close_monit_connection();\
exit(0);\
}
*UtilsPane_b2.fontList: BoldTextFont
*UtilsPane_b2.mnemonic: "E"

*commandsCascade.class: cascadeButton
*commandsCascade.parent: pullDownMenu
*commandsCascade.static: true
*commandsCascade.name: commandsCascade
*commandsCascade.labelString: "File"
*commandsCascade.subMenuId: "UtilsPane"
*commandsCascade.background: MenuBackground
*commandsCascade.fontList: BoldTextFont
*commandsCascade.foreground: MenuForeground
*commandsCascade.mnemonic: "F"
*commandsCascade.x: 101
*commandsCascade.translations: transTable10

*workAreaForm.class: form
*workAreaForm.parent: mainWindow
*workAreaForm.static: true
*workAreaForm.name: workAreaForm
*workAreaForm.width: 490
*workAreaForm.height: 440
*workAreaForm.borderWidth: 0
*workAreaForm.background: WindowBackground

*workAreaFrame.class: frame
*workAreaFrame.parent: workAreaForm
*workAreaFrame.static: true
*workAreaFrame.name: workAreaFrame
*workAreaFrame.x: 50
*workAreaFrame.y: 30
*workAreaFrame.width: 390
*workAreaFrame.height: 230
*workAreaFrame.bottomAttachment: "attach_form"
*workAreaFrame.bottomOffset.source: public
*workAreaFrame.bottomOffset: 0
*workAreaFrame.leftAttachment: "attach_form"
*workAreaFrame.leftOffset.source: public
*workAreaFrame.leftOffset: 0
*workAreaFrame.rightAttachment: "attach_form"
*workAreaFrame.rightOffset.source: public
*workAreaFrame.rightOffset: 0
*workAreaFrame.topAttachment: "attach_form"
*workAreaFrame.topOffset: 0
*workAreaFrame.background: WindowBackground

*workArea.class: form
*workArea.parent: workAreaFrame
*workArea.static: true
*workArea.name: workArea
*workArea.x: 20
*workArea.y: 2
*workArea.width: 408
*workArea.height: 498
*workArea.borderWidth: 0
*workArea.createCallback: {\
\
}
*workArea.mapCallback: {\
\
}
*workArea.background: WindowBackground
*workArea.borderColor: WindowBackground
*workArea.marginHeight: 5
*workArea.marginWidth: 5

*scrolledWindow.class: scrolledWindow
*scrolledWindow.parent: workArea
*scrolledWindow.static: true
*scrolledWindow.name: scrolledWindow
*scrolledWindow.scrollingPolicy: "application_defined"
*scrolledWindow.x: 8
*scrolledWindow.y: 20
*scrolledWindow.visualPolicy: "variable"
*scrolledWindow.scrollBarDisplayPolicy: "static"
*scrolledWindow.shadowThickness: 0
*scrolledWindow.height: 270
*scrolledWindow.width: 490
*scrolledWindow.background: ApplicBackground
*scrolledWindow.bottomAttachment: "attach_form"
*scrolledWindow.bottomOffset: 135
*scrolledWindow.leftAttachment: "attach_form"
*scrolledWindow.leftOffset: 5
*scrolledWindow.rightAttachment: "attach_form"
*scrolledWindow.rightOffset: 5
*scrolledWindow.topAttachment: "attach_form"
*scrolledWindow.topOffset: 5

*scrolledText.class: scrolledText
*scrolledText.parent: scrolledWindow
*scrolledText.static: true
*scrolledText.name: scrolledText
*scrolledText.width: 670
*scrolledText.height: 220
*scrolledText.background: "WhiteSmoke"
*scrolledText.x: 0
*scrolledText.y: 0
*scrolledText.editable: "false"
*scrolledText.scrollHorizontal: "true"
*scrolledText.scrollVertical: "true"
*scrolledText.editMode: "multi_line_edit"
*scrolledText.foreground: TextForeground
*scrolledText.highlightColor: "black"
*scrolledText.fontList: SmallFont
*scrolledText.translations: HelpTable
*scrolledText.createCallback: {\
char *s;\
extern char contxt[],midvers[];\
\
helpme (2L, midvers, "?" , contxt, &s);\
XmTextSetString(UxWidget,s);\
\
}
*scrolledText.text: "This is my help, \nhelp, \nhelp...\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n End of Text"
*scrolledText.motionVerifyCallback: {\
\
}
*scrolledText.valueChangedCallback: {\
\
}

*menu1.class: rowColumn
*menu1.parent: workArea
*menu1.static: true
*menu1.name: menu1
*menu1.rowColumnType: "menu_option"
*menu1.subMenuId: "menu1_p1"
*menu1.background: ApplicBackground
*menu1.foreground: TextForeground
*menu1.leftAttachment: "attach_form"
*menu1.leftOffset: 75
*menu1.topAttachment: "attach_widget"
*menu1.topOffset: 10
*menu1.topWidget: "scrolledWindow"
*menu1.width: 200
*menu1.translations: transTable10
*menu1.entryCallback: {\
printf("Hello\n");\
}
*menu1.height: 10
*menu1.labelString: ""

*menu1_p1.class: rowColumn
*menu1_p1.parent: menu1
*menu1_p1.static: true
*menu1_p1.name: menu1_p1
*menu1_p1.rowColumnType: "menu_pulldown"
*menu1_p1.background: TextBackground
*menu1_p1.foreground: TextForeground
*menu1_p1.topShadowColor: "White"
*menu1_p1.translations: ""
*menu1_p1.entryCallback: {\
Overview[1] = 0;\
\
}

*menu1_p1_b1.class: pushButtonGadget
*menu1_p1_b1.parent: menu1_p1
*menu1_p1_b1.static: true
*menu1_p1_b1.name: menu1_p1_b1
*menu1_p1_b1.labelString: "Commands"
*menu1_p1_b1.fontList: TextFont
*menu1_p1_b1.activateCallback: {\
InitHelp();\
}
*menu1_p1_b1.mnemonic: "C"

*menu1_p1_b2.class: pushButtonGadget
*menu1_p1_b2.parent: menu1_p1
*menu1_p1_b2.static: true
*menu1_p1_b2.name: menu1_p1_b2
*menu1_p1_b2.labelString: "Applications"
*menu1_p1_b2.fontList: TextFont
*menu1_p1_b2.activateCallback: InitHelp();
*menu1_p1_b2.mnemonic: "A"

*menu1_p1_b3.class: pushButtonGadget
*menu1_p1_b3.parent: menu1_p1
*menu1_p1_b3.static: true
*menu1_p1_b3.name: menu1_p1_b3
*menu1_p1_b3.labelString: "Procedure Lang."
*menu1_p1_b3.fontList: TextFont
*menu1_p1_b3.activateCallback: {\
InitHelp();\
\
}
*menu1_p1_b3.mnemonic: "P"

*menu1_p1_b4.class: pushButtonGadget
*menu1_p1_b4.parent: menu1_p1
*menu1_p1_b4.static: true
*menu1_p1_b4.name: menu1_p1_b4
*menu1_p1_b4.labelString: "Keywords"
*menu1_p1_b4.fontList: TextFont
*menu1_p1_b4.activateCallback: InitHelp();
*menu1_p1_b4.mnemonic: "K"

*menu1_p1_b6.class: pushButtonGadget
*menu1_p1_b6.parent: menu1_p1
*menu1_p1_b6.static: true
*menu1_p1_b6.name: menu1_p1_b6
*menu1_p1_b6.labelString: "Subjects"
*menu1_p1_b6.fontList: TextFont
*menu1_p1_b6.activateCallback: InitHelp();
*menu1_p1_b6.mnemonic: "S"

*menu1_p1_b7.class: pushButtonGadget
*menu1_p1_b7.parent: menu1_p1
*menu1_p1_b7.static: true
*menu1_p1_b7.name: menu1_p1_b7
*menu1_p1_b7.labelString: "Contributions"
*menu1_p1_b7.activateCallback: {InitHelp();}
*menu1_p1_b7.mnemonic: "t"
*menu1_p1_b7.fontList: TextFont

*text1.class: text
*text1.parent: workArea
*text1.static: true
*text1.name: text1
*text1.x: 20
*text1.y: 320
*text1.width: 325
*text1.height: 40
*text1.background: TextBackground
*text1.foreground: TextForeground
*text1.highlightColor: "Black"
*text1.fontList: BoldTextFont
*text1.cursorPositionVisible: "true"
*text1.translations: ""
*text1.activateCallback: {\
char *s;\
extern SelHelp();\
\
s = XmTextGetString(UxWidget);\
\
GetHelp(s,"?",1L);\
\
}
*text1.pendingDelete: "true"
*text1.blinkRate: 500
*text1.editMode: "single_line_edit"
*text1.highlightOnEnter: "true"
*text1.wordWrap: "false"
*text1.accelerators: ""
*text1.bottomOffset: 0
*text1.leftAttachment: "attach_widget"
*text1.leftOffset: 5
*text1.rightAttachment: "attach_form"
*text1.rightOffset: 270
*text1.topAttachment: "attach_widget"
*text1.topOffset: 5
*text1.topWidget: "scrolledWindow"
*text1.leftWidget: "menu1"

*SHelp.class: text
*SHelp.parent: workArea
*SHelp.static: true
*SHelp.name: SHelp
*SHelp.x: 30
*SHelp.y: 380
*SHelp.width: 325
*SHelp.height: 40
*SHelp.background: SHelpBackground
*SHelp.foreground: TextForeground
*SHelp.highlightColor: ApplicBackground
*SHelp.fontList: TextFont
*SHelp.cursorPositionVisible: "true"
*SHelp.translations: ""
*SHelp.activateCallback: {\
char  *s1;\
char  *s2;\
extern char contxt[], midvers[];\
\
s1 = XmTextGetString(UxWidget);\
helpme(2L, midvers, s1, contxt, &s2); \
XmTextSetString(UxGetWidget(scrolledText),s2);\
\
}
*SHelp.pendingDelete: "true"
*SHelp.blinkRate: 500
*SHelp.editMode: "single_line_edit"
*SHelp.highlightOnEnter: "true"
*SHelp.wordWrap: "false"
*SHelp.accelerators: ""
*SHelp.leftAttachment: "attach_form"
*SHelp.leftOffset: 5
*SHelp.rightAttachment: "attach_form"
*SHelp.rightOffset: 5
*SHelp.topAttachment: "attach_widget"
*SHelp.topOffset: 5
*SHelp.topWidget: "text1"
*SHelp.bottomAttachment: "attach_form"
*SHelp.bottomOffset: 45

*CommandButton.class: pushButton
*CommandButton.parent: workArea
*CommandButton.static: true
*CommandButton.name: CommandButton
*CommandButton.x: 20
*CommandButton.y: 380
*CommandButton.width: 90
*CommandButton.height: 30
*CommandButton.background: ButtonBackground
*CommandButton.labelString: "Overview"
*CommandButton.foreground: ButtonForeground
*CommandButton.fontList: BoldTextFont
*CommandButton.activateCallback: {\
InitHelp();\
}
*CommandButton.recomputeSize: "false"
*CommandButton.leftAttachment: "attach_form"
*CommandButton.leftOffset: 10
*CommandButton.topAttachment: "attach_widget"
*CommandButton.topOffset: 5
*CommandButton.topWidget: "SHelp"
*CommandButton.highlightColor: "Black"
*CommandButton.bottomAttachment: "attach_form"
*CommandButton.bottomOffset: 5
*CommandButton.mnemonic: ""

*HistoButton.class: pushButton
*HistoButton.parent: workArea
*HistoButton.static: true
*HistoButton.name: HistoButton
*HistoButton.x: 110
*HistoButton.y: 380
*HistoButton.width: 90
*HistoButton.height: 30
*HistoButton.background: ButtonBackground
*HistoButton.labelString: "History.."
*HistoButton.fontList: BoldTextFont
*HistoButton.foreground: ButtonForeground
*HistoButton.activateCallback: {\
extern swidget hist;\
\
\
UxPopupInterface(hist,no_grab);\
MapRaised(hist);\
\
}
*HistoButton.recomputeSize: "false"
*HistoButton.highlightColor: "Black"
*HistoButton.bottomAttachment: "attach_form"
*HistoButton.bottomOffset: 5
*HistoButton.leftAttachment: "attach_widget"
*HistoButton.leftOffset: 10
*HistoButton.leftWidget: "CommandButton"
*HistoButton.topAttachment: "attach_widget"
*HistoButton.topOffset: 5
*HistoButton.topWidget: "SHelp"
*HistoButton.mnemonic: ""

*SearchButton.class: pushButton
*SearchButton.parent: workArea
*SearchButton.static: true
*SearchButton.name: SearchButton
*SearchButton.x: 110
*SearchButton.y: 380
*SearchButton.width: 90
*SearchButton.height: 30
*SearchButton.background: ButtonBackground
*SearchButton.labelString: "Search.."
*SearchButton.fontList: BoldTextFont
*SearchButton.foreground: ButtonForeground
*SearchButton.activateCallback: {\
extern swidget srch;\
\
UxPopupInterface(srch,no_grab);\
MapRaised(srch);\
\
}
*SearchButton.recomputeSize: "false"
*SearchButton.highlightColor: "Black"
*SearchButton.bottomAttachment: "attach_form"
*SearchButton.bottomOffset: 5
*SearchButton.topAttachment: "attach_widget"
*SearchButton.topOffset: 5
*SearchButton.topWidget: "SHelp"
*SearchButton.leftAttachment: "attach_widget"
*SearchButton.leftOffset: 10
*SearchButton.leftWidget: "HistoButton"
*SearchButton.mnemonic: ""

*NewsButton.class: pushButton
*NewsButton.parent: workArea
*NewsButton.static: true
*NewsButton.name: NewsButton
*NewsButton.x: 180
*NewsButton.y: 330
*NewsButton.width: 90
*NewsButton.height: 30
*NewsButton.background: ButtonBackground
*NewsButton.labelString: "News"
*NewsButton.foreground: ButtonForeground
*NewsButton.fontList: BoldTextFont
*NewsButton.activateCallback: {\
GetHelp("[News]","C",0L);\
}
*NewsButton.recomputeSize: "false"
*NewsButton.topAttachment: "attach_widget"
*NewsButton.topOffset: 5
*NewsButton.topWidget: "SHelp"
*NewsButton.leftAttachment: "attach_widget"
*NewsButton.leftOffset: 5
*NewsButton.leftWidget: "SearchButton"
*NewsButton.highlightColor: "Black"
*NewsButton.bottomAttachment: "attach_form"
*NewsButton.bottomOffset: 5
*NewsButton.mnemonic: ""

*PrintButton.class: pushButton
*PrintButton.parent: workArea
*PrintButton.static: true
*PrintButton.name: PrintButton
*PrintButton.x: 280
*PrintButton.y: 330
*PrintButton.width: 90
*PrintButton.height: 30
*PrintButton.background: ButtonBackground
*PrintButton.labelString: "Print"
*PrintButton.foreground: ButtonForeground
*PrintButton.fontList: BoldTextFont
*PrintButton.activateCallback: {\
 extern char filename[], print_com[];\
 char command[100];\
 FILE *fp;\
\
 fp = fopen(filename,"w"); \
\
 fprintf(fp,"%s\n",XmTextGetString(UxGetWidget(scrolledText)));\
\
 fclose(fp);\
\
 sprintf(command,"cat %s | %s ; rm %s\n",\
       filename,print_com,filename);\
\
 system(command);\
\
 UxPutText(SHelp,"Help file sent to printer.");\
\
\
}
*PrintButton.recomputeSize: "false"
*PrintButton.topAttachment: "attach_widget"
*PrintButton.topOffset: 5
*PrintButton.topWidget: "SHelp"
*PrintButton.leftAttachment: "attach_widget"
*PrintButton.leftOffset: 5
*PrintButton.leftWidget: "NewsButton"
*PrintButton.highlightColor: "Black"
*PrintButton.bottomAttachment: "attach_form"
*PrintButton.bottomOffset: 5
*PrintButton.mnemonic: ""

*ReportButton.class: pushButton
*ReportButton.parent: workArea
*ReportButton.static: true
*ReportButton.name: ReportButton
*ReportButton.x: 430
*ReportButton.y: 330
*ReportButton.width: 90
*ReportButton.height: 30
*ReportButton.background: ButtonBackground
*ReportButton.labelString: "Feedback.."
*ReportButton.foreground: ButtonForeground
*ReportButton.fontList: BoldTextFont
*ReportButton.activateCallback: {\
extern swidget pb;\
\
UxPopupInterface(pb,no_grab);\
MapRaised(pb);\
SensMenu(0);\
\
}
*ReportButton.recomputeSize: "false"
*ReportButton.highlightColor: "Black"
*ReportButton.leftAttachment: "attach_widget"
*ReportButton.leftOffset: 5
*ReportButton.leftWidget: "PrintButton"
*ReportButton.topAttachment: "attach_widget"
*ReportButton.topOffset: 5
*ReportButton.topWidget: "SHelp"
*ReportButton.bottomAttachment: "attach_form"
*ReportButton.bottomOffset: 5
*ReportButton.mnemonic: ""

*CtxButton.class: pushButton
*CtxButton.parent: workArea
*CtxButton.static: true
*CtxButton.name: CtxButton
*CtxButton.x: 430
*CtxButton.y: 330
*CtxButton.width: 90
*CtxButton.height: 30
*CtxButton.background: ButtonBackground
*CtxButton.labelString: "Contexts.."
*CtxButton.foreground: ButtonForeground
*CtxButton.fontList: BoldTextFont
*CtxButton.activateCallback: {\
extern swidget ctx;\
extern char contxt[], nctx[];\
\
strred(contxt);\
strtrs(nctx,contxt," ","\n");\
convert(contxt,nctx);\
\
UxPopupInterface(ctx,no_grab);\
MapRaised(ctx);\
\
}
*CtxButton.recomputeSize: "false"
*CtxButton.highlightColor: "Black"
*CtxButton.bottomAttachment: "attach_form"
*CtxButton.bottomOffset: 5
*CtxButton.leftAttachment: "attach_widget"
*CtxButton.leftOffset: 5
*CtxButton.leftWidget: "ReportButton"
*CtxButton.topAttachment: "attach_widget"
*CtxButton.topOffset: 5
*CtxButton.topWidget: "SHelp"
*CtxButton.mnemonic: ""

*textField5.class: textField
*textField5.parent: workArea
*textField5.static: true
*textField5.name: textField5
*textField5.x: 30
*textField5.y: 310
*textField5.width: 70
*textField5.height: 35
*textField5.topAttachment: "attach_widget"
*textField5.topOffset: 12
*textField5.topWidget: "scrolledWindow"
*textField5.leftAttachment: "attach_form"
*textField5.leftOffset: 10
*textField5.background: WindowBackground
*textField5.fontList: BoldTextFont
*textField5.text: "Info on:"
*textField5.bottomShadowColor: WindowBackground
*textField5.topShadowColor: WindowBackground
*textField5.cursorPositionVisible: "false"
*textField5.editable: "false"
*textField5.highlightColor: WindowBackground
*textField5.highlightThickness: 0
*textField5.marginHeight: 5
*textField5.marginWidth: 5
*textField5.shadowThickness: 0

*text2.class: text
*text2.parent: workArea
*text2.static: true
*text2.name: text2
*text2.x: 20
*text2.y: 320
*text2.width: 325
*text2.height: 40
*text2.background: TextBackground
*text2.foreground: TextForeground
*text2.highlightColor: "Black"
*text2.fontList: BoldTextFont
*text2.cursorPositionVisible: "false"
*text2.translations: TextTable
*text2.activateCallback: {\
char *s;\
extern SelHelp();\
\
s = XmTextGetString(UxWidget);\
\
GetHelp(s,"?",1L);\
\
}
*text2.pendingDelete: "true"
*text2.blinkRate: 500
*text2.editMode: "single_line_edit"
*text2.highlightOnEnter: "false"
*text2.wordWrap: "false"
*text2.accelerators: ""
*text2.leftAttachment: "attach_widget"
*text2.leftWidget: "text1"
*text2.leftOffset: 10
*text2.rightAttachment: "attach_form"
*text2.rightOffset: 35
*text2.topAttachment: "attach_widget"
*text2.topOffset: 5
*text2.topWidget: "scrolledWindow"
*text2.autoShowCursorPosition: "false"
*text2.editable: "false"

*arrowButton2.class: arrowButton
*arrowButton2.parent: workArea
*arrowButton2.static: true
*arrowButton2.name: arrowButton2
*arrowButton2.x: 671
*arrowButton2.y: 120
*arrowButton2.width: 20
*arrowButton2.height: 20
*arrowButton2.leftAttachment: "attach_widget"
*arrowButton2.leftOffset: 5
*arrowButton2.leftWidget: "text2"
*arrowButton2.rightAttachment: "attach_form"
*arrowButton2.rightOffset: 5
*arrowButton2.foreground: "PaleGreen"
*arrowButton2.bottomShadowColor: WindowBackground
*arrowButton2.topShadowColor: ButtonForeground
*arrowButton2.background: ButtonBackground
*arrowButton2.borderColor: WindowBackground
*arrowButton2.activateCallback: {\
char s[100];\
\
++DispItem;\
\
strcpy(s,(char *)GetList());\
\
UxPutText(text2,s);\
\
SelHelp(s);\
\
}
*arrowButton2.topAttachment: "attach_widget"
*arrowButton2.topOffset: 3
*arrowButton2.topWidget: "scrolledWindow"
*arrowButton2.translations: ""

*arrowButton1.class: arrowButton
*arrowButton1.parent: workArea
*arrowButton1.static: true
*arrowButton1.name: arrowButton1
*arrowButton1.x: 671
*arrowButton1.y: 310
*arrowButton1.width: 20
*arrowButton1.height: 20
*arrowButton1.leftAttachment: "attach_widget"
*arrowButton1.leftOffset: 5
*arrowButton1.leftWidget: "text2"
*arrowButton1.rightAttachment: "attach_form"
*arrowButton1.rightOffset: 5
*arrowButton1.arrowDirection: "arrow_down"
*arrowButton1.foreground: "PaleGreen"
*arrowButton1.bottomShadowColor: WindowBackground
*arrowButton1.topShadowColor: ButtonForeground
*arrowButton1.background: ButtonBackground
*arrowButton1.borderColor: WindowBackground
*arrowButton1.activateCallback: {\
char s[100];\
\
--DispItem;\
\
strcpy(s,(char *)GetList());\
\
UxPutText(text2,s);\
\
SelHelp(s);\
\
}
*arrowButton1.topAttachment: "attach_widget"
*arrowButton1.topOffset: 0
*arrowButton1.topWidget: "arrowButton2"
*arrowButton1.translations: ""

