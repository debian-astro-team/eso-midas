! UIMX ascii 2.0 key: 2024                                                      
*action.WriteHelp: {\
WriteHelpAW(UxWidget);\
helpPR(UxWidget);\
\
}

*translation.table: transTable2
*translation.parent: transientShell3
*translation.policy: override
*translation.<Btn1Up>: SelectContext1()

*translation.table: transTable8
*translation.parent: transientShell3
*translation.policy: override
*translation.<EnterWindow>: WriteHelp()

*transientShell3.class: transientShell
*transientShell3.parent: NO_PARENT
*transientShell3.static: true
*transientShell3.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
\
extern char print_com[], mid_mail[];
*transientShell3.ispecdecl:
*transientShell3.funcdecl: swidget create_transientShell3()\

*transientShell3.funcname: create_transientShell3
*transientShell3.funcdef: "swidget", "<create_transientShell3>(%)"
*transientShell3.icode:
*transientShell3.fcode: return(rtrn);\

*transientShell3.auxdecl:
*transientShell3.name: transientShell3
*transientShell3.x: 80
*transientShell3.y: 490
*transientShell3.width: 350
*transientShell3.height: 190
*transientShell3.popupCallback: {\
extern char print_com[], mid_mail[];\
\
UxPutText(textField1,print_com);\
UxPutText(textField2,mid_mail);\
\
}
*transientShell3.title: "System Definitions"

*workArea3.class: form
*workArea3.parent: transientShell3
*workArea3.static: true
*workArea3.name: workArea3
*workArea3.x: 20
*workArea3.y: 2
*workArea3.width: 600
*workArea3.height: 498
*workArea3.borderWidth: 0
*workArea3.createCallback: {\
\
}
*workArea3.mapCallback: {\
\
}
*workArea3.background: WindowBackground
*workArea3.unitType: "pixels"
*workArea3.noResize: "true"
*workArea3.resizePolicy: "resize_none"

*SHelp3.class: text
*SHelp3.parent: workArea3
*SHelp3.static: true
*SHelp3.name: SHelp3
*SHelp3.x: 30
*SHelp3.y: 380
*SHelp3.width: 325
*SHelp3.height: 40
*SHelp3.background: SHelpBackground
*SHelp3.foreground: TextForeground
*SHelp3.highlightColor: ApplicBackground
*SHelp3.fontList: TextFont
*SHelp3.cursorPositionVisible: "true"
*SHelp3.translations: ""
*SHelp3.activateCallback: {\
\
}
*SHelp3.pendingDelete: "true"
*SHelp3.blinkRate: 500
*SHelp3.editMode: "single_line_edit"
*SHelp3.highlightOnEnter: "true"
*SHelp3.wordWrap: "false"
*SHelp3.accelerators: ""
*SHelp3.leftAttachment: "attach_form"
*SHelp3.leftOffset: 5
*SHelp3.rightAttachment: "attach_form"
*SHelp3.rightOffset: 5
*SHelp3.bottomAttachment: "attach_form"
*SHelp3.bottomOffset: 50
*SHelp3.editable: "false"
*SHelp3.text: "Push button OK to update the system definitions"

*NewsButton3.class: pushButton
*NewsButton3.parent: workArea3
*NewsButton3.static: true
*NewsButton3.name: NewsButton3
*NewsButton3.x: 20
*NewsButton3.y: 630
*NewsButton3.width: 95
*NewsButton3.height: 30
*NewsButton3.background: ButtonBackground
*NewsButton3.labelString: "OK"
*NewsButton3.foreground: ButtonForeground
*NewsButton3.fontList: BoldTextFont
*NewsButton3.activateCallback: {\
extern char mid_mail[], print_com[];\
extern swidget sys;\
\
strcpy(print_com,UxGetText(textField1));\
strcpy(mid_mail,UxGetText(textField2));\
UxPopdownInterface(sys);\
\
}
*NewsButton3.recomputeSize: "false"
*NewsButton3.topAttachment: "attach_widget"
*NewsButton3.topOffset: 10
*NewsButton3.topWidget: "SHelp3"
*NewsButton3.highlightColor: "Black"
*NewsButton3.leftAttachment: "attach_form"
*NewsButton3.leftOffset: 50

*PrintButton3.class: pushButton
*PrintButton3.parent: workArea3
*PrintButton3.static: true
*PrintButton3.name: PrintButton3
*PrintButton3.x: 280
*PrintButton3.y: 330
*PrintButton3.width: 95
*PrintButton3.height: 30
*PrintButton3.background: ButtonBackground
*PrintButton3.labelString: "Cancel"
*PrintButton3.foreground: ButtonForeground
*PrintButton3.fontList: BoldTextFont
*PrintButton3.activateCallback: {\
extern swidget sys;\
\
UxPopdownInterface(sys);\
\
}
*PrintButton3.recomputeSize: "false"
*PrintButton3.topAttachment: "attach_widget"
*PrintButton3.topOffset: 10
*PrintButton3.topWidget: "SHelp3"
*PrintButton3.leftAttachment: "attach_widget"
*PrintButton3.leftOffset: 40
*PrintButton3.leftWidget: "NewsButton3"
*PrintButton3.highlightColor: "Black"

*label3.class: label
*label3.parent: workArea3
*label3.static: true
*label3.name: label3
*label3.x: 10
*label3.y: 10
*label3.width: 130
*label3.height: 20
*label3.background: LabelBackground
*label3.fontList: BoldTextFont
*label3.foreground: TextForeground
*label3.labelString: "MIDAS e-mail"
*label3.leftAttachment: "attach_form"
*label3.leftOffset: 5
*label3.topAttachment: "attach_form"
*label3.topOffset: 65

*label4.class: label
*label4.parent: workArea3
*label4.static: true
*label4.name: label4
*label4.x: 10
*label4.y: 10
*label4.width: 130
*label4.height: 20
*label4.background: LabelBackground
*label4.fontList: BoldTextFont
*label4.foreground: TextForeground
*label4.labelString: "Print Command"
*label4.leftAttachment: "attach_form"
*label4.leftOffset: 5
*label4.topOffset: 15
*label4.topAttachment: "attach_form"

*textField1.class: text
*textField1.parent: workArea3
*textField1.static: true
*textField1.name: textField1
*textField1.x: 140
*textField1.y: 10
*textField1.width: 250
*textField1.height: 35
*textField1.background: TextBackground
*textField1.leftAttachment: "attach_form"
*textField1.rightAttachment: "attach_form"
*textField1.rightOffset: 5
*textField1.topAttachment: "attach_form"
*textField1.leftOffset: 140
*textField1.topOffset: 10
*textField1.fontList: TextFont
*textField1.foreground: TextForeground
*textField1.text: print_com

*textField2.class: text
*textField2.parent: workArea3
*textField2.static: true
*textField2.name: textField2
*textField2.x: 140
*textField2.y: 60
*textField2.width: 250
*textField2.height: 35
*textField2.background: TextBackground
*textField2.leftAttachment: "attach_form"
*textField2.leftOffset: 140
*textField2.rightAttachment: "attach_form"
*textField2.rightOffset: 5
*textField2.topAttachment: "attach_form"
*textField2.topOffset: 60
*textField2.fontList: TextFont
*textField2.foreground: TextForeground
*textField2.text: mid_mail

