! UIMX ascii 2.0 key: 3516                                                      
*action.WriteHelp: {\
WriteHelpAW(UxWidget);\
helpPR(UxWidget);\
\
}

*translation.table: transTable5
*translation.parent: transientShell6
*translation.policy: override
*translation.<EnterWindow>: WriteHelp()

*transientShell6.class: transientShell
*transientShell6.parent: NO_PARENT
*transientShell6.static: true
*transientShell6.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*transientShell6.ispecdecl:
*transientShell6.funcdecl: swidget create_transientShell6()\

*transientShell6.funcname: create_transientShell6
*transientShell6.funcdef: "swidget", "<create_transientShell6>(%)"
*transientShell6.icode:
*transientShell6.fcode: return(rtrn);\

*transientShell6.auxdecl:
*transientShell6.name: transientShell6
*transientShell6.x: 910
*transientShell6.y: 330
*transientShell6.width: 300
*transientShell6.height: 160
*transientShell6.title: "Search Form"

*workArea6.class: form
*workArea6.parent: transientShell6
*workArea6.static: true
*workArea6.name: workArea6
*workArea6.x: 20
*workArea6.y: 2
*workArea6.width: 600
*workArea6.height: 498
*workArea6.borderWidth: 0
*workArea6.createCallback: {\
\
}
*workArea6.mapCallback: {\
\
}
*workArea6.background: WindowBackground
*workArea6.unitType: "pixels"
*workArea6.noResize: "true"
*workArea6.resizePolicy: "resize_none"

*SHelp5.class: text
*SHelp5.parent: workArea6
*SHelp5.static: true
*SHelp5.name: SHelp5
*SHelp5.x: 30
*SHelp5.y: 380
*SHelp5.width: 325
*SHelp5.height: 40
*SHelp5.background: SHelpBackground
*SHelp5.foreground: TextForeground
*SHelp5.highlightColor: ApplicBackground
*SHelp5.fontList: TextFont
*SHelp5.cursorPositionVisible: "true"
*SHelp5.translations: ""
*SHelp5.activateCallback: {\
\
}
*SHelp5.pendingDelete: "true"
*SHelp5.blinkRate: 500
*SHelp5.editMode: "multi_line_edit"
*SHelp5.highlightOnEnter: "true"
*SHelp5.wordWrap: "false"
*SHelp5.accelerators: ""
*SHelp5.leftAttachment: "attach_form"
*SHelp5.leftOffset: 5
*SHelp5.rightAttachment: "attach_form"
*SHelp5.rightOffset: 5
*SHelp5.bottomAttachment: "attach_form"
*SHelp5.bottomOffset: 50
*SHelp5.editable: "false"
*SHelp5.text: "Case insensitive search.\nApply or Return displays all occurences."
*SHelp5.topAttachment: "attach_form"
*SHelp5.topOffset: 55

*NewsButton5.class: pushButton
*NewsButton5.parent: workArea6
*NewsButton5.static: true
*NewsButton5.name: NewsButton5
*NewsButton5.x: 20
*NewsButton5.y: 630
*NewsButton5.width: 85
*NewsButton5.height: 30
*NewsButton5.background: ButtonBackground
*NewsButton5.labelString: "Apply"
*NewsButton5.foreground: ButtonForeground
*NewsButton5.fontList: BoldTextFont
*NewsButton5.activateCallback: {\
extern SearchHelp();\
\
SearchHelp(UxGetText(textField3),1L);\
\
}
*NewsButton5.recomputeSize: "false"
*NewsButton5.topAttachment: "attach_widget"
*NewsButton5.topOffset: 10
*NewsButton5.topWidget: "SHelp5"
*NewsButton5.highlightColor: "Black"
*NewsButton5.leftAttachment: "attach_form"
*NewsButton5.leftOffset: 15
*NewsButton5.bottomAttachment: "attach_form"
*NewsButton5.bottomOffset: 10

*label6.class: label
*label6.parent: workArea6
*label6.static: true
*label6.name: label6
*label6.x: 10
*label6.y: 10
*label6.width: 80
*label6.height: 20
*label6.background: LabelBackground
*label6.fontList: BoldTextFont
*label6.foreground: TextForeground
*label6.labelString: "String"
*label6.leftAttachment: "attach_form"
*label6.leftOffset: 5
*label6.topAttachment: "attach_form"
*label6.topOffset: 15

*textField3.class: text
*textField3.parent: workArea6
*textField3.static: true
*textField3.name: textField3
*textField3.x: 140
*textField3.y: 60
*textField3.width: 250
*textField3.height: 35
*textField3.background: TextBackground
*textField3.leftAttachment: "attach_form"
*textField3.leftOffset: 90
*textField3.rightAttachment: "attach_form"
*textField3.rightOffset: 5
*textField3.topAttachment: "attach_form"
*textField3.topOffset: 10
*textField3.fontList: TextFont
*textField3.foreground: TextForeground
*textField3.text: ""
*textField3.activateCallback: {\
SearchHelp(UxGetText(UxThisWidget),1L);\
}

*NewsButton6.class: pushButton
*NewsButton6.parent: workArea6
*NewsButton6.static: true
*NewsButton6.name: NewsButton6
*NewsButton6.x: 102
*NewsButton6.y: 65
*NewsButton6.width: 85
*NewsButton6.height: 30
*NewsButton6.background: ButtonBackground
*NewsButton6.labelString: "Clear"
*NewsButton6.foreground: ButtonForeground
*NewsButton6.fontList: BoldTextFont
*NewsButton6.activateCallback: {\
extern SearchHelp();\
\
SearchHelp(UxGetText(textField3),0L);\
UxPutText(textField3,"");\
}
*NewsButton6.recomputeSize: "false"
*NewsButton6.highlightColor: "Black"
*NewsButton6.leftAttachment: "attach_widget"
*NewsButton6.leftOffset: 10
*NewsButton6.leftWidget: "NewsButton5"
*NewsButton6.bottomAttachment: "attach_form"
*NewsButton6.bottomOffset: 10
*NewsButton6.topAttachment: "attach_widget"
*NewsButton6.topOffset: 10
*NewsButton6.topWidget: "SHelp5"

*PrintButton5.class: pushButton
*PrintButton5.parent: workArea6
*PrintButton5.static: true
*PrintButton5.name: PrintButton5
*PrintButton5.x: 280
*PrintButton5.y: 330
*PrintButton5.width: 85
*PrintButton5.height: 30
*PrintButton5.background: ButtonBackground
*PrintButton5.labelString: "Cancel"
*PrintButton5.foreground: ButtonForeground
*PrintButton5.fontList: BoldTextFont
*PrintButton5.activateCallback: {\
extern swidget srch;\
\
UxPopdownInterface(srch);\
\
}
*PrintButton5.recomputeSize: "false"
*PrintButton5.topAttachment: "attach_widget"
*PrintButton5.topOffset: 10
*PrintButton5.topWidget: "SHelp5"
*PrintButton5.leftAttachment: "attach_widget"
*PrintButton5.leftOffset: 10
*PrintButton5.leftWidget: "NewsButton6"
*PrintButton5.highlightColor: "Black"

