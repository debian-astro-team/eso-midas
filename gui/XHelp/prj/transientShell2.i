! UIMX ascii 2.0 key: 5562                                                      
*action.SelectContext: {\
 extern char contxt[], nctx[];\
\
 strcpy(nctx,UxGetText(scrolledText2)); \
\
 if ((int)strlen(nctx) > 0) strcat(nctx,"\n");\
\
 if (XmTextGetSelection(UxWidget) == NULL) return;\
\
 strcat(nctx,\
     XmTextGetSelection(UxWidget));\
\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
 UxPutText(scrolledText2, nctx);\
\
 \
}
*action.WriteHelp: {\
WriteHelpAW(UxWidget);\
helpPR(UxWidget);\
\
}

*translation.table: CtxTable
*translation.parent: transientShell2
*translation.policy: override
*translation.<Btn1Up>: SelectContext()

*translation.table: transTable7
*translation.parent: transientShell2
*translation.policy: override
*translation.<EnterWindow>: WriteHelp()

*transientShell2.class: transientShell
*transientShell2.parent: NO_PARENT
*transientShell2.static: true
*transientShell2.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <dirent.h>\
#include <monitdef.h>         /* Define MAX_CONTXT */\
#include <midback.h>          /* Define MAX_CONTXT */\

*transientShell2.ispecdecl:
*transientShell2.funcdecl: swidget create_transientShell2()\

*transientShell2.funcname: create_transientShell2
*transientShell2.funcdef: "swidget", "<create_transientShell2>(%)"
*transientShell2.icode:
*transientShell2.fcode: PutSelection(scrolledText2,1);\
PutSelection(scrolledText3,1);\
\
return(rtrn);\

*transientShell2.auxdecl:
*transientShell2.name: transientShell2
*transientShell2.x: 570
*transientShell2.y: 250
*transientShell2.width: 250
*transientShell2.height: 500
*transientShell2.popupCallback: {\
extern char nctx[];\
\
UxPutText(scrolledText2,nctx);\
\
}
*transientShell2.title: "Context Selection"

*workArea2.class: form
*workArea2.parent: transientShell2
*workArea2.static: true
*workArea2.name: workArea2
*workArea2.x: 20
*workArea2.y: 2
*workArea2.width: 600
*workArea2.height: 498
*workArea2.borderWidth: 0
*workArea2.createCallback: {\
\
}
*workArea2.mapCallback: {\
\
}
*workArea2.background: WindowBackground
*workArea2.unitType: "pixels"
*workArea2.noResize: "true"
*workArea2.resizePolicy: "resize_none"

*scrolledWindow2.class: scrolledWindow
*scrolledWindow2.parent: workArea2
*scrolledWindow2.static: true
*scrolledWindow2.name: scrolledWindow2
*scrolledWindow2.scrollingPolicy: "application_defined"
*scrolledWindow2.x: 8
*scrolledWindow2.y: 20
*scrolledWindow2.visualPolicy: "variable"
*scrolledWindow2.scrollBarDisplayPolicy: "static"
*scrolledWindow2.shadowThickness: 0
*scrolledWindow2.height: 270
*scrolledWindow2.width: 490
*scrolledWindow2.background: ApplicBackground
*scrolledWindow2.bottomAttachment: "attach_form"
*scrolledWindow2.bottomOffset: 130
*scrolledWindow2.leftAttachment: "attach_form"
*scrolledWindow2.leftOffset: 5
*scrolledWindow2.rightAttachment: "attach_form"
*scrolledWindow2.rightOffset: 5
*scrolledWindow2.topAttachment: "attach_form"
*scrolledWindow2.topOffset: 250
*scrolledWindow2.translations: ""

*scrolledText2.class: scrolledText
*scrolledText2.parent: scrolledWindow2
*scrolledText2.static: true
*scrolledText2.name: scrolledText2
*scrolledText2.width: 670
*scrolledText2.height: 220
*scrolledText2.background: "WhiteSmoke"
*scrolledText2.x: 0
*scrolledText2.y: 0
*scrolledText2.editable: "false"
*scrolledText2.scrollHorizontal: "true"
*scrolledText2.scrollVertical: "true"
*scrolledText2.editMode: "multi_line_edit"
*scrolledText2.foreground: TextForeground
*scrolledText2.highlightColor: "grey"
*scrolledText2.fontList: SmallFont
*scrolledText2.translations: ""
*scrolledText2.createCallback: {\
\
}

*SHelp2.class: text
*SHelp2.parent: workArea2
*SHelp2.static: true
*SHelp2.name: SHelp2
*SHelp2.x: 30
*SHelp2.y: 380
*SHelp2.width: 325
*SHelp2.height: 40
*SHelp2.background: SHelpBackground
*SHelp2.foreground: TextForeground
*SHelp2.highlightColor: ApplicBackground
*SHelp2.fontList: TextFont
*SHelp2.cursorPositionVisible: "true"
*SHelp2.translations: ""
*SHelp2.activateCallback: {\
\
}
*SHelp2.pendingDelete: "true"
*SHelp2.blinkRate: 500
*SHelp2.editMode: "single_line_edit"
*SHelp2.highlightOnEnter: "true"
*SHelp2.wordWrap: "false"
*SHelp2.accelerators: ""
*SHelp2.leftAttachment: "attach_form"
*SHelp2.leftOffset: 5
*SHelp2.rightAttachment: "attach_form"
*SHelp2.rightOffset: 5
*SHelp2.topAttachment: "attach_widget"
*SHelp2.topWidget: "scrolledWindow2"
*SHelp2.topOffset: 10
*SHelp2.topPosition: 0
*SHelp2.bottomAttachment: "attach_none"
*SHelp2.bottomOffset: 0
*SHelp2.text: "Select contexts by clicking"

*NewsButton2.class: pushButton
*NewsButton2.parent: workArea2
*NewsButton2.static: true
*NewsButton2.name: NewsButton2
*NewsButton2.x: 20
*NewsButton2.y: 630
*NewsButton2.width: 95
*NewsButton2.height: 30
*NewsButton2.background: ButtonBackground
*NewsButton2.labelString: "OK"
*NewsButton2.foreground: ButtonForeground
*NewsButton2.fontList: BoldTextFont
*NewsButton2.activateCallback: {\
 extern char contxt[], nctx[];\
 \
 strcpy(nctx,UxGetText(scrolledText2)); \
\
 convert(contxt,nctx);\
\
 InitHelp(); \
\
 UxPopdownInterface(transientShell2);\
\
}
*NewsButton2.recomputeSize: "false"
*NewsButton2.topAttachment: "attach_widget"
*NewsButton2.topOffset: 5
*NewsButton2.topWidget: "SHelp2"
*NewsButton2.highlightColor: "Black"
*NewsButton2.leftAttachment: "attach_form"
*NewsButton2.leftOffset: 20

*PrintButton2.class: pushButton
*PrintButton2.parent: workArea2
*PrintButton2.static: true
*PrintButton2.name: PrintButton2
*PrintButton2.x: 280
*PrintButton2.y: 330
*PrintButton2.width: 95
*PrintButton2.height: 30
*PrintButton2.background: ButtonBackground
*PrintButton2.labelString: "Clear"
*PrintButton2.foreground: ButtonForeground
*PrintButton2.fontList: BoldTextFont
*PrintButton2.activateCallback: {\
UxPutText(scrolledText2,"");\
}
*PrintButton2.recomputeSize: "false"
*PrintButton2.topAttachment: "attach_widget"
*PrintButton2.topOffset: 5
*PrintButton2.topWidget: "SHelp2"
*PrintButton2.leftAttachment: "attach_widget"
*PrintButton2.leftOffset: 20
*PrintButton2.leftWidget: "NewsButton2"
*PrintButton2.highlightColor: "Black"

*ReportButton3.class: pushButton
*ReportButton3.parent: workArea2
*ReportButton3.static: true
*ReportButton3.name: ReportButton3
*ReportButton3.x: 430
*ReportButton3.y: 330
*ReportButton3.width: 95
*ReportButton3.height: 30
*ReportButton3.background: ButtonBackground
*ReportButton3.labelString: "Cancel"
*ReportButton3.foreground: ButtonForeground
*ReportButton3.fontList: BoldTextFont
*ReportButton3.activateCallback: {\
extern swidget ctx;\
\
UxPopdownInterface(ctx);\
\
}
*ReportButton3.recomputeSize: "false"
*ReportButton3.highlightColor: "Black"
*ReportButton3.leftAttachment: "attach_form"
*ReportButton3.leftOffset: 20
*ReportButton3.leftWidget: ""
*ReportButton3.topAttachment: "attach_widget"
*ReportButton3.topWidget: "NewsButton2"
*ReportButton3.topOffset: 5

*scrolledWindow3.class: scrolledWindow
*scrolledWindow3.parent: workArea2
*scrolledWindow3.static: true
*scrolledWindow3.name: scrolledWindow3
*scrolledWindow3.scrollingPolicy: "application_defined"
*scrolledWindow3.x: 8
*scrolledWindow3.y: 20
*scrolledWindow3.visualPolicy: "variable"
*scrolledWindow3.scrollBarDisplayPolicy: "static"
*scrolledWindow3.shadowThickness: 0
*scrolledWindow3.height: 270
*scrolledWindow3.width: 490
*scrolledWindow3.background: ApplicBackground
*scrolledWindow3.translations: ""
*scrolledWindow3.leftAttachment: "attach_form"
*scrolledWindow3.leftOffset: 5
*scrolledWindow3.rightAttachment: "attach_form"
*scrolledWindow3.rightOffset: 5
*scrolledWindow3.topAttachment: "attach_form"
*scrolledWindow3.topOffset: 35
*scrolledWindow3.bottomAttachment: "attach_form"
*scrolledWindow3.bottomOffset: 280

*scrolledText3.class: scrolledText
*scrolledText3.parent: scrolledWindow3
*scrolledText3.static: true
*scrolledText3.name: scrolledText3
*scrolledText3.width: 670
*scrolledText3.height: 220
*scrolledText3.background: "WhiteSmoke"
*scrolledText3.x: 0
*scrolledText3.y: 0
*scrolledText3.editable: "false"
*scrolledText3.scrollHorizontal: "true"
*scrolledText3.scrollVertical: "true"
*scrolledText3.editMode: "multi_line_edit"
*scrolledText3.foreground: TextForeground
*scrolledText3.highlightColor: "grey"
*scrolledText3.fontList: SmallFont
*scrolledText3.translations: CtxTable
*scrolledText3.createCallback: {\
#ifdef  DESIGN_TIME\
#include <dirent.h>\
#endif\
\
extern char mid_ctx[];\
char list[20000], name[20];\
char   command[100];\
FILE *fpin;\
int    input, pos=0;\
\
sprintf(command,"ls %s | sort ",mid_ctx);\
\
if ((fpin = popen(command,"r")) == NULL)\
    printf("Could not execute command: %s\n",command);\
\
strcpy(list,"");\
while (fgets(name,19,fpin) != NULL) {\
   pos = strindex(name,".ctx");\
   if (pos < (int) strlen(name)) {\
      name[pos] = '\0';\
      strcat(list, name);\
      strcat(list,"\n");\
      strcpy(name,"");\
}}\
\
if (pclose(fpin) == -1)\
   printf("Could not close stream for command: %s\n",command);\
\
UxPutText(UxThisWidget,list);\
\
\
}

*label1.class: label
*label1.parent: workArea2
*label1.static: true
*label1.name: label1
*label1.x: 10
*label1.y: 10
*label1.width: 160
*label1.height: 20
*label1.background: LabelBackground
*label1.fontList: BoldTextFont
*label1.foreground: TextForeground
*label1.labelString: "List of Contexts:"
*label1.leftAttachment: "attach_form"
*label1.leftOffset: 5
*label1.topAttachment: "attach_form"
*label1.topOffset: 10
*label1.alignment: "alignment_beginning"

*label2.class: label
*label2.parent: workArea2
*label2.static: true
*label2.name: label2
*label2.x: 10
*label2.y: 10
*label2.width: 160
*label2.height: 20
*label2.background: LabelBackground
*label2.fontList: BoldTextFont
*label2.foreground: TextForeground
*label2.labelString: "Selection:"
*label2.leftAttachment: "attach_form"
*label2.topAttachment: "attach_widget"
*label2.topWidget: "scrolledWindow3"
*label2.topOffset: 5
*label2.leftOffset: 5
*label2.alignment: "alignment_beginning"

