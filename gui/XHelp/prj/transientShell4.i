! UIMX ascii 2.0 key: 4602                                                      
*action.WriteHelp: {\
WriteHelpAW(UxWidget);\
helpPR(UxWidget);\
\
}

*translation.table: transTable3
*translation.parent: transientShell4
*translation.policy: override
*translation.<EnterWindow>: WriteHelp()

*transientShell4.class: transientShell
*transientShell4.parent: NO_PARENT
*transientShell4.static: true
*transientShell4.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*transientShell4.ispecdecl:
*transientShell4.funcdecl: swidget create_transientShell4()\

*transientShell4.funcname: create_transientShell4
*transientShell4.funcdef: "swidget", "<create_transientShell4>(%)"
*transientShell4.icode:
*transientShell4.fcode: return(rtrn);\

*transientShell4.auxdecl:
*transientShell4.name: transientShell4
*transientShell4.x: 630
*transientShell4.y: 600
*transientShell4.width: 300
*transientShell4.height: 140
*transientShell4.title: "Save Report"

*workArea4.class: form
*workArea4.parent: transientShell4
*workArea4.static: true
*workArea4.name: workArea4
*workArea4.x: 20
*workArea4.y: 2
*workArea4.width: 600
*workArea4.height: 498
*workArea4.borderWidth: 0
*workArea4.createCallback: {\
\
}
*workArea4.mapCallback: {\
\
}
*workArea4.background: WindowBackground
*workArea4.unitType: "pixels"
*workArea4.noResize: "true"
*workArea4.resizePolicy: "resize_none"

*SHelp4.class: text
*SHelp4.parent: workArea4
*SHelp4.static: true
*SHelp4.name: SHelp4
*SHelp4.x: 30
*SHelp4.y: 380
*SHelp4.width: 325
*SHelp4.height: 40
*SHelp4.background: SHelpBackground
*SHelp4.foreground: TextForeground
*SHelp4.highlightColor: ApplicBackground
*SHelp4.fontList: TextFont
*SHelp4.cursorPositionVisible: "true"
*SHelp4.translations: ""
*SHelp4.activateCallback: {\
\
}
*SHelp4.pendingDelete: "true"
*SHelp4.blinkRate: 500
*SHelp4.editMode: "single_line_edit"
*SHelp4.highlightOnEnter: "true"
*SHelp4.wordWrap: "false"
*SHelp4.accelerators: ""
*SHelp4.leftAttachment: "attach_form"
*SHelp4.leftOffset: 5
*SHelp4.rightAttachment: "attach_form"
*SHelp4.rightOffset: 5
*SHelp4.bottomAttachment: "attach_form"
*SHelp4.bottomOffset: 50
*SHelp4.editable: "false"
*SHelp4.text: "Enter file name"

*NewsButton4.class: pushButton
*NewsButton4.parent: workArea4
*NewsButton4.static: true
*NewsButton4.name: NewsButton4
*NewsButton4.x: 20
*NewsButton4.y: 630
*NewsButton4.width: 95
*NewsButton4.height: 30
*NewsButton4.background: ButtonBackground
*NewsButton4.labelString: "OK"
*NewsButton4.foreground: ButtonForeground
*NewsButton4.fontList: BoldTextFont
*NewsButton4.activateCallback: {\
 extern swidget save;\
 char addr[100];\
\
 FILE *fp;\
\
 strcpy(addr,UxGetText(textField4));\
\
 makeReport(addr);\
\
 UxPopdownInterface(save);\
\
}
*NewsButton4.recomputeSize: "false"
*NewsButton4.topAttachment: "attach_widget"
*NewsButton4.topOffset: 10
*NewsButton4.topWidget: "SHelp4"
*NewsButton4.highlightColor: "Black"
*NewsButton4.leftAttachment: "attach_form"
*NewsButton4.leftOffset: 50

*PrintButton4.class: pushButton
*PrintButton4.parent: workArea4
*PrintButton4.static: true
*PrintButton4.name: PrintButton4
*PrintButton4.x: 280
*PrintButton4.y: 330
*PrintButton4.width: 95
*PrintButton4.height: 30
*PrintButton4.background: ButtonBackground
*PrintButton4.labelString: "Cancel"
*PrintButton4.foreground: ButtonForeground
*PrintButton4.fontList: BoldTextFont
*PrintButton4.activateCallback: {\
extern swidget save;\
\
UxPopdownInterface(save);\
\
}
*PrintButton4.recomputeSize: "false"
*PrintButton4.topAttachment: "attach_widget"
*PrintButton4.topOffset: 10
*PrintButton4.topWidget: "SHelp4"
*PrintButton4.leftAttachment: "attach_widget"
*PrintButton4.leftOffset: 40
*PrintButton4.leftWidget: "NewsButton4"
*PrintButton4.highlightColor: "Black"

*label5.class: label
*label5.parent: workArea4
*label5.static: true
*label5.name: label5
*label5.x: 10
*label5.y: 10
*label5.width: 80
*label5.height: 20
*label5.background: LabelBackground
*label5.fontList: BoldTextFont
*label5.foreground: TextForeground
*label5.labelString: "File Name"
*label5.leftAttachment: "attach_form"
*label5.leftOffset: 5
*label5.topAttachment: "attach_form"
*label5.topOffset: 15

*textField4.class: text
*textField4.parent: workArea4
*textField4.static: true
*textField4.name: textField4
*textField4.x: 140
*textField4.y: 60
*textField4.width: 250
*textField4.height: 35
*textField4.background: TextBackground
*textField4.leftAttachment: "attach_form"
*textField4.leftOffset: 90
*textField4.rightAttachment: "attach_form"
*textField4.rightOffset: 5
*textField4.topAttachment: "attach_form"
*textField4.topOffset: 10
*textField4.fontList: TextFont
*textField4.foreground: TextForeground
*textField4.text: "midas.report"

