! UIMX ascii 2.0 key: 3334                                                      
*action.SelectCommand: {\
\
 extern SelHelp();\
 char *s;\
\
 if ((s = XmTextGetSelection(UxWidget)) == NULL) return; \
\
 SelHelp(s);\
\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
\
}
*action.WriteHelp: {\
WriteHelpAW(UxWidget);\
helpPR(UxWidget);\
\
}

*translation.table: transTable4
*translation.parent: transientShell5
*translation.policy: override
*translation.<Btn1Up>: SelectCommand()

*translation.table: transTable9
*translation.parent: transientShell5
*translation.policy: override
*translation.<EnterWindow> : WriteHelp()

*transientShell5.class: transientShell
*transientShell5.parent: NO_PARENT
*transientShell5.static: true
*transientShell5.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*transientShell5.ispecdecl:
*transientShell5.funcdecl: swidget create_transientShell5()\

*transientShell5.funcname: create_transientShell5
*transientShell5.funcdef: "swidget", "<create_transientShell5>(%)"
*transientShell5.icode:
*transientShell5.fcode: PutSelection(scrolledText5,2);\
\
return(rtrn);\

*transientShell5.auxdecl: void DispList(s)\
\
char *s;\
\
{\
UxPutText(scrolledText5,s);\
}\
\

*transientShell5.name: transientShell5
*transientShell5.x: 470
*transientShell5.y: 280
*transientShell5.width: 240
*transientShell5.height: 300
*transientShell5.popupCallback: {\
\
}
*transientShell5.title: "History List"
*transientShell5.geometry: ""
*transientShell5.keyboardFocusPolicy: "pointer"
*transientShell5.waitForWm: "false"

*workArea5.class: form
*workArea5.parent: transientShell5
*workArea5.static: true
*workArea5.name: workArea5
*workArea5.x: 20
*workArea5.y: 2
*workArea5.width: 600
*workArea5.height: 498
*workArea5.borderWidth: 0
*workArea5.createCallback: {\
\
}
*workArea5.mapCallback: {\
\
}
*workArea5.background: WindowBackground
*workArea5.unitType: "pixels"
*workArea5.noResize: "true"
*workArea5.resizePolicy: "resize_none"

*scrolledWindow5.class: scrolledWindow
*scrolledWindow5.parent: workArea5
*scrolledWindow5.static: true
*scrolledWindow5.name: scrolledWindow5
*scrolledWindow5.scrollingPolicy: "application_defined"
*scrolledWindow5.x: 8
*scrolledWindow5.y: 20
*scrolledWindow5.visualPolicy: "variable"
*scrolledWindow5.scrollBarDisplayPolicy: "static"
*scrolledWindow5.shadowThickness: 0
*scrolledWindow5.height: 270
*scrolledWindow5.width: 490
*scrolledWindow5.background: ApplicBackground
*scrolledWindow5.translations: ""
*scrolledWindow5.leftAttachment: "attach_form"
*scrolledWindow5.leftOffset: 5
*scrolledWindow5.rightAttachment: "attach_form"
*scrolledWindow5.rightOffset: 5
*scrolledWindow5.topAttachment: "attach_form"
*scrolledWindow5.topOffset: 5
*scrolledWindow5.bottomAttachment: "attach_form"
*scrolledWindow5.bottomOffset: 40

*scrolledText5.class: scrolledText
*scrolledText5.parent: scrolledWindow5
*scrolledText5.static: true
*scrolledText5.name: scrolledText5
*scrolledText5.width: 670
*scrolledText5.height: 220
*scrolledText5.background: "WhiteSmoke"
*scrolledText5.x: 0
*scrolledText5.y: 0
*scrolledText5.editable: "false"
*scrolledText5.scrollHorizontal: "true"
*scrolledText5.scrollVertical: "true"
*scrolledText5.editMode: "multi_line_edit"
*scrolledText5.foreground: TextForeground
*scrolledText5.highlightColor: "grey"
*scrolledText5.fontList: SmallFont
*scrolledText5.translations: transTable4
*scrolledText5.createCallback: {\
\
}

*ReportButton2.class: pushButton
*ReportButton2.parent: workArea5
*ReportButton2.static: true
*ReportButton2.name: ReportButton2
*ReportButton2.x: 430
*ReportButton2.y: 330
*ReportButton2.width: 95
*ReportButton2.height: 30
*ReportButton2.background: ButtonBackground
*ReportButton2.labelString: "Cancel"
*ReportButton2.foreground: ButtonForeground
*ReportButton2.fontList: BoldTextFont
*ReportButton2.activateCallback: {\
extern swidget hist;\
\
UxPopdownInterface(hist);\
\
}
*ReportButton2.recomputeSize: "false"
*ReportButton2.highlightColor: "Black"
*ReportButton2.leftAttachment: "attach_form"
*ReportButton2.leftOffset: 75
*ReportButton2.leftWidget: ""
*ReportButton2.bottomAttachment: "attach_form"
*ReportButton2.bottomOffset: 5

