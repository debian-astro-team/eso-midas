/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*------------------------------------------------------------
 * This is the project main program file for Ux generated  
 * code. You may add application dependent source code
 * at the appropriate places. 
 * 			     
 * Do not modify the statements preceded by the dollar
 * sign ($), these statements will be replaced with
 * the appropriate source code when the main program is
 * automatically generated. 
 *			
 * $Date: 2009-08-21 08:02:33 $  		$Revision: 1.3 $ 

.VERSION
 090818		last modif

 *-----------------------------------------------------------*/

#ifdef XOPEN_CATALOG
#include <locale.h>
#endif

#include <UxLib.h>


/*----------------------------------------------
 * Insert application global declarations here
 *---------------------------------------------*/

#include <StdResources.h>


/* MAX_CONTXT*8+2 is 15*80 + 2 = 122 < len(argv[6]) = 160     (070823) */

char contxt[164], nctx[164], midvers[20], filename[200];
char mid_ctx[100], mid_mail[100], print_com[100], mid_work[100];

swidget pb, save, sys, ctx, hist, srch;
extern swidget create_transientShell1(), 
               create_transientShell2(), 
               create_transientShell3(),
               create_transientShell4(),
               create_transientShell5(),
               create_transientShell6();

extern int init_monit_connection();

int main(argc,argv)
	int 	argc;
	char 	*argv[];
{
	/*-----------------------------------------------------------
	 * Declarations.
	 * The default identifier - mainIface will only be declared  
	 * if the interface function is global and of type swidget. 
	 * To change the identifier to a different name, modify the
	 * string mainIface in the file "main.dat".  If "mainIface"
	 * is declared, it will be used below where the return value 
	 * of  PJ_INTERFACE_FUNCTION_CALL will be assigned to it.
         *-----------------------------------------------------------*/ 

  	swidget mainIface;
        int ncar;

 	/*--------------------------------
	 * Interface function declaration
	 *-------------------------------*/	

 	swidget popup_ApplicWindow();

	

        /*------------------------------------------------
         * Initialize interrupt routine for MIDAS monitor
         *----------------------------------------------*/

        (void) init_monit_connection();

	/*---------------------
	 * Initialize Program
	 *--------------------*/

#ifdef XOPEN_CATALOG
	setlocale(LC_ALL, "");
#endif

  	UxTopLevel = UxInitialize("XHelp", &argc, argv);

	/*------------------------------------------------------
	 * Insert initialization code for your application here
	 *-----------------------------------------------------*/

	strcpy(midvers,  argv[1]);
        strcpy(mid_mail, argv[2]);
        strcpy(print_com,argv[3]);
        strcpy(mid_ctx,  argv[4]);
        strcpy(mid_work, argv[5]);
        strcpy(contxt,   argv[6]);

        /* printf("Version: %s, Mail: %s, Print: %s\nContext: %s\n
               Midwork: %s\nContexts: %s\n",
               midvers, mid_mail, print_com, mid_ctx, mid_work, contxt); */

        /* Process print command to remove final -P or -d option */
        ncar = (int) strlen(print_com)-1;
        while (print_com[ncar] == ' ') ncar--;
        if (print_com[ncar-1] == '-' && (print_com[ncar] == 'P' || 
                print_com[ncar] == 'd'))  ncar = ncar-1;
        else
            printf("Warning: Unexpected print option. Check Print in Setup");
        print_com[ncar] = '\0';

         strcpy(filename,"/tmp/hprint.txt");

         pb    = create_transientShell1(); 
         ctx   = create_transientShell2(); 
         sys   = create_transientShell3(); 
         save  = create_transientShell4(); 
         hist  = create_transientShell5(); 
         srch  = create_transientShell6(); 

	/*----------------------------------------------------------------
	 * Create and popup the first window of the interface.  The 	 
	 * return value can be used in the popdown or destroy functions.
	 * The swidget return value of  PJ_INTERFACE_FUNCTION_CALL will
	 * be assigned to "mainIface" from  PJ_INTERFACE_RETVAL_TYPE. 
	 *---------------------------------------------------------------*/

	mainIface = popup_ApplicWindow();

	UxPopupInterface(mainIface, no_grab);

	/*-------------------------
	 * Enter the event loop 
	 *------------------------*/

  	UxMainLoop();

return 0;
}

int convert(s1,s2)

char *s1,*s2;

{

int i=0, k=0, kmax;
char car;

while((car = s2[i++]) != '\0') {
  if (car != '\n') s1[k++] = car;
  else  {
    kmax = (int) (k/8);
    kmax = (kmax+1)*8 - 1;
    while (k <= kmax) s1[k++] = ' ';
    }
}

s1[k] = '\0';
strcat(s1,"        ");

return(k);

}
