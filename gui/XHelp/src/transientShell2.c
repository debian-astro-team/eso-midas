/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	transientShell2.c

*******************************************************************************/

#include <stdio.h>
#include <str.h>

#include "UxLib.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxScText.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <dirent.h>
#include <monitdef.h>         /* Define MAX_CONTXT */
#include <midback.h>          /* Define MAX_CONTXT */

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxtransientShell2;
	swidget	UxworkArea2;
	swidget	UxscrolledWindow2;
	swidget	UxscrolledText2;
	swidget	UxSHelp2;
	swidget	UxNewsButton2;
	swidget	UxPrintButton2;
	swidget	UxReportButton3;
	swidget	UxscrolledWindow3;
	swidget	UxscrolledText3;
	swidget	Uxlabel1;
	swidget	Uxlabel2;
} _UxCtransientShell2;

#define transientShell2         UxTransientShell2Context->UxtransientShell2
#define workArea2               UxTransientShell2Context->UxworkArea2
#define scrolledWindow2         UxTransientShell2Context->UxscrolledWindow2
#define scrolledText2           UxTransientShell2Context->UxscrolledText2
#define SHelp2                  UxTransientShell2Context->UxSHelp2
#define NewsButton2             UxTransientShell2Context->UxNewsButton2
#define PrintButton2            UxTransientShell2Context->UxPrintButton2
#define ReportButton3           UxTransientShell2Context->UxReportButton3
#define scrolledWindow3         UxTransientShell2Context->UxscrolledWindow3
#define scrolledText3           UxTransientShell2Context->UxscrolledText3
#define label1                  UxTransientShell2Context->Uxlabel1
#define label2                  UxTransientShell2Context->Uxlabel2

static _UxCtransientShell2	*UxTransientShell2Context;

extern void helpPR(), InitHelp(), WriteHelpAW(), MapRaised(), SensMenu();
extern void PutSelection();
extern int convert();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*CtxTable = "#override\n\
<Btn1Up>:SelectContext()\n";

/*
static char	*transTable7 = "#override\n\
<EnterWindow>:WriteHelp()\n";
*/

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_transientShell2();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_HInit( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	InitHelp();
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	WriteHelpAW(UxWidget);
	helpPR(UxWidget);
	
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	action_SelectContext( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	 extern char nctx[];
	
	 strcpy(nctx,UxGetText(scrolledText2)); 
	
	 if ((int)strlen(nctx) > 0) strcat(nctx,"\n");
	
	 if (XmTextGetSelection(UxWidget) == NULL) return;
	
	 strcat(nctx,
	     XmTextGetSelection(UxWidget));
	
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	
	 UxPutText(scrolledText2, nctx);
	
	 
	}
	UxTransientShell2Context = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	popupCB_transientShell2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	extern char nctx[];
	
	UxPutText(scrolledText2,nctx);
	
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	mapCB_workArea2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	createCB_workArea2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	createCB_scrolledText2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	activateCB_SHelp2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	activateCB_NewsButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	 extern char contxt[], nctx[];
	 
	 strcpy(nctx,UxGetText(scrolledText2)); 
	
	 convert(contxt,nctx);
	
	 InitHelp(); 
	
	 UxPopdownInterface(transientShell2);
	
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	activateCB_PrintButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	UxPutText(scrolledText2,"");
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	activateCB_ReportButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
	extern swidget ctx;
	
	UxPopdownInterface(ctx);
	
	}
	UxTransientShell2Context = UxSaveCtx;
}

static void	createCB_scrolledText3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell2     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell2Context;
	UxTransientShell2Context = UxContext =
			(_UxCtransientShell2 *) UxGetContext( UxThisWidget );
	{
#ifdef  DESIGN_TIME
#include <dirent.h>
#endif
	
	extern char mid_ctx[];
	char list[20000], name[20];
	char   command[100];
	FILE *fpin;
	int   pos=0;
	
	sprintf(command,"ls %s | sort ",mid_ctx);
	
	if ((fpin = (FILE*) popen(command,"r")) == NULL)
	    printf("Could not execute command: %s\n",command);
	
	strcpy(list,"");
	while (fgets(name,19,fpin) != NULL) {
	   pos = strindex(name,".ctx");
	   if (pos < (int) strlen(name)) {
	      name[pos] = '\0';
	      strcat(list, name);
	      strcat(list,"\n");
	      strcpy(name,"");
	}}
	
	if (pclose(fpin) == -1)
	   printf("Could not close stream for command: %s\n",command);
	
	UxPutText(UxThisWidget,list);
	
	
	}
	UxTransientShell2Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_transientShell2()
{
	UxPutTitle( transientShell2, "Context Selection" );
	UxPutHeight( transientShell2, 500 );
	UxPutWidth( transientShell2, 250 );
	UxPutY( transientShell2, 250 );
	UxPutX( transientShell2, 570 );

	UxPutResizePolicy( workArea2, "resize_none" );
	UxPutNoResize( workArea2, "true" );
	UxPutUnitType( workArea2, "pixels" );
	UxPutBackground( workArea2, WindowBackground );
	UxPutBorderWidth( workArea2, 0 );
	UxPutHeight( workArea2, 498 );
	UxPutWidth( workArea2, 600 );
	UxPutY( workArea2, 2 );
	UxPutX( workArea2, 20 );

	UxPutTranslations( scrolledWindow2, "" );
	UxPutBackground( scrolledWindow2, ApplicBackground );
	UxPutWidth( scrolledWindow2, 490 );
	UxPutHeight( scrolledWindow2, 270 );
	UxPutShadowThickness( scrolledWindow2, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow2, "static" );
	UxPutVisualPolicy( scrolledWindow2, "variable" );
	UxPutY( scrolledWindow2, 20 );
	UxPutX( scrolledWindow2, 8 );
	UxPutScrollingPolicy( scrolledWindow2, "application_defined" );

	UxPutTranslations( scrolledText2, "" );
	UxPutFontList( scrolledText2, SmallFont );
	UxPutHighlightColor( scrolledText2, "grey" );
	UxPutForeground( scrolledText2, TextForeground );
	UxPutEditMode( scrolledText2, "multi_line_edit" );
	UxPutScrollVertical( scrolledText2, "true" );
	UxPutScrollHorizontal( scrolledText2, "true" );
	UxPutEditable( scrolledText2, "false" );
	UxPutY( scrolledText2, 0 );
	UxPutX( scrolledText2, 0 );
	UxPutBackground( scrolledText2, "WhiteSmoke" );
	UxPutHeight( scrolledText2, 220 );
	UxPutWidth( scrolledText2, 670 );

	UxPutText( SHelp2, "Select contexts by clicking" );
	UxPutAccelerators( SHelp2, "" );
	UxPutWordWrap( SHelp2, "false" );
	UxPutHighlightOnEnter( SHelp2, "true" );
	UxPutEditMode( SHelp2, "single_line_edit" );
	UxPutBlinkRate( SHelp2, 500 );
	UxPutPendingDelete( SHelp2, "true" );
	UxPutTranslations( SHelp2, "" );
	UxPutCursorPositionVisible( SHelp2, "true" );
	UxPutFontList( SHelp2, TextFont );
	UxPutHighlightColor( SHelp2, ApplicBackground );
	UxPutForeground( SHelp2, TextForeground );
	UxPutBackground( SHelp2, SHelpBackground );
	UxPutHeight( SHelp2, 40 );
	UxPutWidth( SHelp2, 325 );
	UxPutY( SHelp2, 380 );
	UxPutX( SHelp2, 30 );

	UxPutHighlightColor( NewsButton2, "Black" );
	UxPutRecomputeSize( NewsButton2, "false" );
	UxPutFontList( NewsButton2, BoldTextFont );
	UxPutForeground( NewsButton2, ButtonForeground );
	UxPutLabelString( NewsButton2, "OK" );
	UxPutBackground( NewsButton2, ButtonBackground );
	UxPutHeight( NewsButton2, 30 );
	UxPutWidth( NewsButton2, 95 );
	UxPutY( NewsButton2, 630 );
	UxPutX( NewsButton2, 20 );

	UxPutHighlightColor( PrintButton2, "Black" );
	UxPutRecomputeSize( PrintButton2, "false" );
	UxPutFontList( PrintButton2, BoldTextFont );
	UxPutForeground( PrintButton2, ButtonForeground );
	UxPutLabelString( PrintButton2, "Clear" );
	UxPutBackground( PrintButton2, ButtonBackground );
	UxPutHeight( PrintButton2, 30 );
	UxPutWidth( PrintButton2, 95 );
	UxPutY( PrintButton2, 330 );
	UxPutX( PrintButton2, 280 );

	UxPutHighlightColor( ReportButton3, "Black" );
	UxPutRecomputeSize( ReportButton3, "false" );
	UxPutFontList( ReportButton3, BoldTextFont );
	UxPutForeground( ReportButton3, ButtonForeground );
	UxPutLabelString( ReportButton3, "Cancel" );
	UxPutBackground( ReportButton3, ButtonBackground );
	UxPutHeight( ReportButton3, 30 );
	UxPutWidth( ReportButton3, 95 );
	UxPutY( ReportButton3, 330 );
	UxPutX( ReportButton3, 430 );

	UxPutTranslations( scrolledWindow3, "" );
	UxPutBackground( scrolledWindow3, ApplicBackground );
	UxPutWidth( scrolledWindow3, 490 );
	UxPutHeight( scrolledWindow3, 270 );
	UxPutShadowThickness( scrolledWindow3, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow3, "static" );
	UxPutVisualPolicy( scrolledWindow3, "variable" );
	UxPutY( scrolledWindow3, 20 );
	UxPutX( scrolledWindow3, 8 );
	UxPutScrollingPolicy( scrolledWindow3, "application_defined" );

	UxPutTranslations( scrolledText3, CtxTable );
	UxPutFontList( scrolledText3, SmallFont );
	UxPutHighlightColor( scrolledText3, "grey" );
	UxPutForeground( scrolledText3, TextForeground );
	UxPutEditMode( scrolledText3, "multi_line_edit" );
	UxPutScrollVertical( scrolledText3, "true" );
	UxPutScrollHorizontal( scrolledText3, "true" );
	UxPutEditable( scrolledText3, "false" );
	UxPutY( scrolledText3, 0 );
	UxPutX( scrolledText3, 0 );
	UxPutBackground( scrolledText3, "WhiteSmoke" );
	UxPutHeight( scrolledText3, 220 );
	UxPutWidth( scrolledText3, 670 );

	UxPutAlignment( label1, "alignment_beginning" );
	UxPutLabelString( label1, "List of Contexts:" );
	UxPutForeground( label1, TextForeground );
	UxPutFontList( label1, BoldTextFont );
	UxPutBackground( label1, LabelBackground );
	UxPutHeight( label1, 20 );
	UxPutWidth( label1, 160 );
	UxPutY( label1, 10 );
	UxPutX( label1, 10 );

	UxPutAlignment( label2, "alignment_beginning" );
	UxPutLabelString( label2, "Selection:" );
	UxPutForeground( label2, TextForeground );
	UxPutFontList( label2, BoldTextFont );
	UxPutBackground( label2, LabelBackground );
	UxPutHeight( label2, 20 );
	UxPutWidth( label2, 160 );
	UxPutY( label2, 10 );
	UxPutX( label2, 10 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_transientShell2()
{
	/* Create the swidgets */

	transientShell2 = UxCreateTransientShell( "transientShell2", NO_PARENT );
	UxPutContext( transientShell2, UxTransientShell2Context );

	workArea2 = UxCreateForm( "workArea2", transientShell2 );
	scrolledWindow2 = UxCreateScrolledWindow( "scrolledWindow2", workArea2 );
	scrolledText2 = UxCreateScrolledText( "scrolledText2", scrolledWindow2 );
	SHelp2 = UxCreateText( "SHelp2", workArea2 );
	NewsButton2 = UxCreatePushButton( "NewsButton2", workArea2 );
	PrintButton2 = UxCreatePushButton( "PrintButton2", workArea2 );
	ReportButton3 = UxCreatePushButton( "ReportButton3", workArea2 );
	scrolledWindow3 = UxCreateScrolledWindow( "scrolledWindow3", workArea2 );
	scrolledText3 = UxCreateScrolledText( "scrolledText3", scrolledWindow3 );
	label1 = UxCreateLabel( "label1", workArea2 );
	label2 = UxCreateLabel( "label2", workArea2 );

	_Uxinit_transientShell2();

	/* Create the X widgets */

	UxCreateWidget( transientShell2 );
	UxCreateWidget( workArea2 );
	createCB_workArea2( UxGetWidget( workArea2 ),
			(XtPointer) UxTransientShell2Context, (XtPointer) NULL );

	UxPutTopOffset( scrolledWindow2, 250 );
	UxPutTopAttachment( scrolledWindow2, "attach_form" );
	UxPutRightOffset( scrolledWindow2, 5 );
	UxPutRightAttachment( scrolledWindow2, "attach_form" );
	UxPutLeftOffset( scrolledWindow2, 5 );
	UxPutLeftAttachment( scrolledWindow2, "attach_form" );
	UxPutBottomOffset( scrolledWindow2, 130 );
	UxPutBottomAttachment( scrolledWindow2, "attach_form" );
	UxCreateWidget( scrolledWindow2 );

	UxCreateWidget( scrolledText2 );
	createCB_scrolledText2( UxGetWidget( scrolledText2 ),
			(XtPointer) UxTransientShell2Context, (XtPointer) NULL );

	UxPutBottomOffset( SHelp2, 0 );
	UxPutBottomAttachment( SHelp2, "attach_none" );
	UxPutTopPosition( SHelp2, 0 );
	UxPutTopOffset( SHelp2, 10 );
	UxPutTopWidget( SHelp2, "scrolledWindow2" );
	UxPutTopAttachment( SHelp2, "attach_widget" );
	UxPutRightOffset( SHelp2, 5 );
	UxPutRightAttachment( SHelp2, "attach_form" );
	UxPutLeftOffset( SHelp2, 5 );
	UxPutLeftAttachment( SHelp2, "attach_form" );
	UxCreateWidget( SHelp2 );

	UxPutLeftOffset( NewsButton2, 20 );
	UxPutLeftAttachment( NewsButton2, "attach_form" );
	UxPutTopWidget( NewsButton2, "SHelp2" );
	UxPutTopOffset( NewsButton2, 5 );
	UxPutTopAttachment( NewsButton2, "attach_widget" );
	UxCreateWidget( NewsButton2 );

	UxPutLeftWidget( PrintButton2, "NewsButton2" );
	UxPutLeftOffset( PrintButton2, 20 );
	UxPutLeftAttachment( PrintButton2, "attach_widget" );
	UxPutTopWidget( PrintButton2, "SHelp2" );
	UxPutTopOffset( PrintButton2, 5 );
	UxPutTopAttachment( PrintButton2, "attach_widget" );
	UxCreateWidget( PrintButton2 );

	UxPutTopOffset( ReportButton3, 5 );
	UxPutTopWidget( ReportButton3, "NewsButton2" );
	UxPutTopAttachment( ReportButton3, "attach_widget" );
	UxPutLeftWidget( ReportButton3, "" );
	UxPutLeftOffset( ReportButton3, 20 );
	UxPutLeftAttachment( ReportButton3, "attach_form" );
	UxCreateWidget( ReportButton3 );

	UxPutBottomOffset( scrolledWindow3, 280 );
	UxPutBottomAttachment( scrolledWindow3, "attach_form" );
	UxPutTopOffset( scrolledWindow3, 35 );
	UxPutTopAttachment( scrolledWindow3, "attach_form" );
	UxPutRightOffset( scrolledWindow3, 5 );
	UxPutRightAttachment( scrolledWindow3, "attach_form" );
	UxPutLeftOffset( scrolledWindow3, 5 );
	UxPutLeftAttachment( scrolledWindow3, "attach_form" );
	UxCreateWidget( scrolledWindow3 );

	UxCreateWidget( scrolledText3 );
	createCB_scrolledText3( UxGetWidget( scrolledText3 ),
			(XtPointer) UxTransientShell2Context, (XtPointer) NULL );

	UxPutTopOffset( label1, 10 );
	UxPutTopAttachment( label1, "attach_form" );
	UxPutLeftOffset( label1, 5 );
	UxPutLeftAttachment( label1, "attach_form" );
	UxCreateWidget( label1 );

	UxPutLeftOffset( label2, 5 );
	UxPutTopOffset( label2, 5 );
	UxPutTopWidget( label2, "scrolledWindow3" );
	UxPutTopAttachment( label2, "attach_widget" );
	UxPutLeftAttachment( label2, "attach_form" );
	UxCreateWidget( label2 );


	UxAddCallback( transientShell2, XmNpopupCallback,
			popupCB_transientShell2,
			(XtPointer) UxTransientShell2Context );

	UxAddCallback( workArea2, XmNmapCallback,
			mapCB_workArea2,
			(XtPointer) UxTransientShell2Context );

	UxAddCallback( SHelp2, XmNactivateCallback,
			activateCB_SHelp2,
			(XtPointer) UxTransientShell2Context );

	UxAddCallback( NewsButton2, XmNactivateCallback,
			activateCB_NewsButton2,
			(XtPointer) UxTransientShell2Context );

	UxAddCallback( PrintButton2, XmNactivateCallback,
			activateCB_PrintButton2,
			(XtPointer) UxTransientShell2Context );

	UxAddCallback( ReportButton3, XmNactivateCallback,
			activateCB_ReportButton3,
			(XtPointer) UxTransientShell2Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( transientShell2 );

	return ( transientShell2 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_transientShell2()
{
	swidget                 rtrn;
	_UxCtransientShell2     *UxContext;

	UxTransientShell2Context = UxContext =
		(_UxCtransientShell2 *) UxMalloc( sizeof(_UxCtransientShell2) );

	rtrn = _Uxbuild_transientShell2();

	PutSelection(scrolledText2,1);
	PutSelection(scrolledText3,1);
	
	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_transientShell2()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "HInit", action_HInit },
				{ "WriteHelp", action_WriteHelp },
				{ "SelectContext", action_SelectContext }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_transientShell2();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

