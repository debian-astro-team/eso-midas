/*===========================================================================
  Copyright (C) 1995-2010 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
VERSION
 100922		last modif
*/



/*******************************************************************************
	transientShell1.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxPushBG.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxScText.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTranSh.h"



/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

extern char midvers[];

extern void InitHelp(), WriteHelpAW(), MapRaised(), SensMenu();

#include <stdio.h>
#include <stdlib.h>
#include <str.h>

#include <UxSubproc.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxtransientShell1;
	swidget	UxworkArea1;
	swidget	UxscrolledWindow6;
	swidget	UxscrolledText6;
	swidget	UxSHelp1;
	swidget	UxNewsButton1;
	swidget	UxPrintButton1;
	swidget	UxReportButton1;
	swidget	UxSendButton1;
	swidget	Uxlabel8;
	swidget	Uxlabel9;
	swidget	Uxlabel10;
	swidget	Uxlabel11;
	swidget	Uxlabel12;
	swidget	Uxlabel15;
	swidget	Uxlabel17;
	swidget	UxtextField6;
	swidget	UxtextField7;
	swidget	UxtextField8;
	swidget	UxrowColumn1;
	swidget	UxtoggleButton1;
	swidget	UxtoggleButton2;
	swidget	UxtoggleButton3;
	swidget	UxtoggleButton4;
	swidget	Uxmenu1_p4;
	swidget	Uxmenu1_p1_b13;
	swidget	Uxmenu1_p1_b14;
	swidget	Uxmenu1_p4_b3;
	swidget	Uxmenu1_p4_b4;
	swidget	Uxmenu1_p4_b5;
	swidget	Uxmenu1_p4_b6;
	swidget	Uxmenu1_p4_b7;
	swidget	Uxmenu1_p4_b8;
	swidget	Uxmenu1_p4_b9;
	swidget	Uxmenu1_p4_b10;
	swidget	Uxmenu1_p4_b11;
	swidget	Uxmenu1_p4_b12;
	swidget	Uxmenu1_p4_b13;
	swidget	Uxmenu1_p4_b14;
	swidget	Uxmenu1_p4_b15;
	swidget	Uxmenu1_p4_b16;
	swidget	Uxmenu1_p4_b17;
	swidget	Uxmenu1_p4_b18;
	swidget	Uxmenu1_p4_b19;
	swidget	Uxmenu1_p4_b20;
	swidget	Uxmenu1_p4_b21;
	swidget	Uxmenu1_p4_b22;
	swidget	Uxmenu1_p4_b23;
	swidget	Uxmenu1_p4_b24;
	swidget	Uxmenu1_p4_b25;
	swidget	Uxmenu1_p4_b26;
	swidget	Uxmenu4;
	swidget	Uxmenu1_p5;
	swidget	Uxmenu1_p1_b15;
	swidget	Uxmenu1_p1_b16;
	swidget	Uxmenu1_p4_b1;
	swidget	Uxmenu5;
	swidget	UxrowColumn2;
	swidget	UxtoggleButton5;
	swidget	UxtoggleButton6;
	swidget	UxtoggleButton7;
	swidget	UxtoggleButton8;
	swidget	UxtoggleButton9;
	swidget	UxtoggleButton10;
	swidget	UxtoggleButton11;
	swidget	UxtoggleButton12;
	swidget	UxtoggleButton13;
	swidget	UxtoggleButton14;
	swidget	Uxtext3;
	swidget	Uxlabel18;
	swidget	UxtextField9;
	swidget	Uxlabel16;
	swidget	Uxseparator1;
	swidget	Uxseparator2;
	swidget	Uxseparator3;
	swidget	Uxseparator4;
	swidget	Uxseparator5;
	swidget	Uxseparator6;
} _UxCtransientShell1;

#define transientShell1         UxTransientShell1Context->UxtransientShell1
#define workArea1               UxTransientShell1Context->UxworkArea1
#define scrolledWindow6         UxTransientShell1Context->UxscrolledWindow6
#define scrolledText6           UxTransientShell1Context->UxscrolledText6
#define SHelp1                  UxTransientShell1Context->UxSHelp1
#define NewsButton1             UxTransientShell1Context->UxNewsButton1
#define PrintButton1            UxTransientShell1Context->UxPrintButton1
#define ReportButton1           UxTransientShell1Context->UxReportButton1
#define SendButton1             UxTransientShell1Context->UxSendButton1
#define label8                  UxTransientShell1Context->Uxlabel8
#define label9                  UxTransientShell1Context->Uxlabel9
#define label10                 UxTransientShell1Context->Uxlabel10
#define label11                 UxTransientShell1Context->Uxlabel11
#define label12                 UxTransientShell1Context->Uxlabel12
#define label15                 UxTransientShell1Context->Uxlabel15
#define label17                 UxTransientShell1Context->Uxlabel17
#define textField6              UxTransientShell1Context->UxtextField6
#define textField7              UxTransientShell1Context->UxtextField7
#define textField8              UxTransientShell1Context->UxtextField8
#define rowColumn1              UxTransientShell1Context->UxrowColumn1
#define toggleButton1           UxTransientShell1Context->UxtoggleButton1
#define toggleButton2           UxTransientShell1Context->UxtoggleButton2
#define toggleButton3           UxTransientShell1Context->UxtoggleButton3
#define toggleButton4           UxTransientShell1Context->UxtoggleButton4
#define menu1_p4                UxTransientShell1Context->Uxmenu1_p4
#define menu1_p1_b13            UxTransientShell1Context->Uxmenu1_p1_b13
#define menu1_p1_b14            UxTransientShell1Context->Uxmenu1_p1_b14
#define menu1_p4_b3             UxTransientShell1Context->Uxmenu1_p4_b3
#define menu1_p4_b4             UxTransientShell1Context->Uxmenu1_p4_b4
#define menu1_p4_b5             UxTransientShell1Context->Uxmenu1_p4_b5
#define menu1_p4_b6             UxTransientShell1Context->Uxmenu1_p4_b6
#define menu1_p4_b7             UxTransientShell1Context->Uxmenu1_p4_b7
#define menu1_p4_b8             UxTransientShell1Context->Uxmenu1_p4_b8
#define menu1_p4_b9             UxTransientShell1Context->Uxmenu1_p4_b9
#define menu1_p4_b10            UxTransientShell1Context->Uxmenu1_p4_b10
#define menu1_p4_b11            UxTransientShell1Context->Uxmenu1_p4_b11
#define menu1_p4_b12            UxTransientShell1Context->Uxmenu1_p4_b12
#define menu1_p4_b13            UxTransientShell1Context->Uxmenu1_p4_b13
#define menu1_p4_b14            UxTransientShell1Context->Uxmenu1_p4_b14
#define menu1_p4_b15            UxTransientShell1Context->Uxmenu1_p4_b15
#define menu1_p4_b16            UxTransientShell1Context->Uxmenu1_p4_b16
#define menu1_p4_b17            UxTransientShell1Context->Uxmenu1_p4_b17
#define menu1_p4_b18            UxTransientShell1Context->Uxmenu1_p4_b18
#define menu1_p4_b19            UxTransientShell1Context->Uxmenu1_p4_b19
#define menu1_p4_b20            UxTransientShell1Context->Uxmenu1_p4_b20
#define menu1_p4_b21            UxTransientShell1Context->Uxmenu1_p4_b21
#define menu1_p4_b22            UxTransientShell1Context->Uxmenu1_p4_b22
#define menu1_p4_b23            UxTransientShell1Context->Uxmenu1_p4_b23
#define menu1_p4_b24            UxTransientShell1Context->Uxmenu1_p4_b24
#define menu1_p4_b25            UxTransientShell1Context->Uxmenu1_p4_b25
#define menu1_p4_b26            UxTransientShell1Context->Uxmenu1_p4_b26
#define menu4                   UxTransientShell1Context->Uxmenu4
#define menu1_p5                UxTransientShell1Context->Uxmenu1_p5
#define menu1_p1_b15            UxTransientShell1Context->Uxmenu1_p1_b15
#define menu1_p1_b16            UxTransientShell1Context->Uxmenu1_p1_b16
#define menu1_p4_b1             UxTransientShell1Context->Uxmenu1_p4_b1
#define menu5                   UxTransientShell1Context->Uxmenu5
#define rowColumn2              UxTransientShell1Context->UxrowColumn2
#define toggleButton5           UxTransientShell1Context->UxtoggleButton5
#define toggleButton6           UxTransientShell1Context->UxtoggleButton6
#define toggleButton7           UxTransientShell1Context->UxtoggleButton7
#define toggleButton8           UxTransientShell1Context->UxtoggleButton8
#define toggleButton9           UxTransientShell1Context->UxtoggleButton9
#define toggleButton10          UxTransientShell1Context->UxtoggleButton10
#define toggleButton11          UxTransientShell1Context->UxtoggleButton11
#define toggleButton12          UxTransientShell1Context->UxtoggleButton12
#define toggleButton13          UxTransientShell1Context->UxtoggleButton13
#define toggleButton14          UxTransientShell1Context->UxtoggleButton14
#define text3                   UxTransientShell1Context->Uxtext3
#define label18                 UxTransientShell1Context->Uxlabel18
#define textField9              UxTransientShell1Context->UxtextField9
#define label16                 UxTransientShell1Context->Uxlabel16
#define separator1              UxTransientShell1Context->Uxseparator1
#define separator2              UxTransientShell1Context->Uxseparator2
#define separator3              UxTransientShell1Context->Uxseparator3
#define separator4              UxTransientShell1Context->Uxseparator4
#define separator5              UxTransientShell1Context->Uxseparator5
#define separator6              UxTransientShell1Context->Uxseparator6

static _UxCtransientShell1	*UxTransientShell1Context;


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

/*
static char	*transTable1 = "#override\n\
<Key>Delete:delete-previous-character()\n\
Ctrl<Key>a:beginning-of-line()\n\
Ctrl<Key>b:backward-character()\n\
Ctrl<Key>d:kill-next-character()\n\
Ctrl<Key>e:end-of-line()\n\
Ctrl<Key>f:forward-character()\n\
Ctrl<Key>i:insert-string(\"    \")\n\
Ctrl<Key>k:kill-to-end-of-line()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n\
Meta<Key>b:backward-word()\n\
Meta<Key>d:delete-next-word()\n\
Meta<Key>f:forward-word()\n\
Ctrl<Key>j:newline-and-indent()\n\
Ctrl<Key>l:redraw-display()\n\
Ctrl<Key>m:newline()\n\
Ctrl<Key>n:next-line()\n\
Ctrl<Key>o:newline-and-backup()\n\
Ctrl<Key>p:previous-line()\n\
Ctrl<Key>u:kill-to-start-of-line()\n\
Ctrl<Key>v:next-page()\n\
Ctrl<Key>w:kill-selection()\n\
Ctrl<Key>y:unkill()\n\
Ctrl<Key>z:scroll-one-line-down()\n\
Meta<Key>v:previous-page()\n\
Meta<Key>y:stuff()\n\
Meta<Key>z:scroll-one-line-down()\n\
<EnterWindow>:WriteHelp() Enter()\n";
*/


static char	*transTable6 = "#override\n\
<EnterNotify>:WriteHelp() Enter()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_transientShell1();

/*******************************************************************************
	Auxiliary code from the Declarations Editor:
*******************************************************************************/

void helpPR(uw)
Widget uw;

{
char s[100];
extern char mid_mail[];

strcpy(s,"");

/* Class */
if (uw == UxGetWidget(toggleButton2))
    strcpy(s,"Problem class: software bug");

if (uw == UxGetWidget(toggleButton3))
    strcpy(s,"Problem class: documentation bug");

if (uw == UxGetWidget(toggleButton4))
    strcpy(s,"Problem class: no bug, but modification or new feature requested");

if (uw == UxGetWidget(toggleButton1))
    strcpy(s,"Problem class: Midas support needed");

/* Category */

if (uw == UxGetWidget(toggleButton5))
    strcpy(s,"Problem Category: Midas installation");

if (uw == UxGetWidget(toggleButton6))
    strcpy(s,"Problem Category: Midas distribution");

if (uw == UxGetWidget(toggleButton7))
    strcpy(s,"Problem Category: System, Monitor, Command Language");

if (uw == UxGetWidget(toggleButton8))
    strcpy(s,"Problem Category: Display Window and related commands");

if (uw == UxGetWidget(toggleButton9))
    strcpy(s,"Problem Category: Graphic window and related commands");

if (uw == UxGetWidget(toggleButton10))
    strcpy(s,"Problem Category: Midas Tables");

if (uw == UxGetWidget(toggleButton11))
    strcpy(s,"Problem Category: On-line help and documentation");

if (uw == UxGetWidget(toggleButton12))
    strcpy(s,"Problem Category: Image arithmetic, filtering, transforms");

if (uw == UxGetWidget(toggleButton13))
    strcpy(s,"Problem Category: Graphical User Interfaces");

if (uw == UxGetWidget(toggleButton14))
    strcpy(s,"Problem Category: Midas context (select the appropriate one)");

/* Confidentiality, Severity, Priority */

if (uw == UxGetWidget(menu4) || uw == UxGetWidget(menu1_p4))
    strcpy(s,"Midas Context");

if (uw == UxGetWidget(menu5) || uw == UxGetWidget(menu1_p5))
    strcpy(s,"Priority level");

/* Originator, Release */

if (uw == UxGetWidget(textField6))
   strcpy(s,"E-mail address or identification of the mail originator");

if (uw == UxGetWidget(textField7))
   strcpy(s,"Midas release");

/* Environment, Command , Synopsis */

if (uw == UxGetWidget(text3))
   strcpy(s,"Hardware and software environment (OS, compilers, platform, ...)");

if (uw == UxGetWidget(textField9))
   strcpy(s,"Midas command involved (if relevant)");

if (uw == UxGetWidget(textField8))
   strcpy(s,"Short description of the problem");

/* Description, How-To-Repeat */

if (uw == UxGetWidget(scrolledWindow6) || uw == UxGetWidget(scrolledText6))
    strcpy(s,"Precise description of the problem");

/* Push Buttons: OK, Save As, Clear, Cancel */

if (uw == UxGetWidget(NewsButton1))
   sprintf(s,"Send a mail to the address: %s and close the window",
           mid_mail);

if (uw == UxGetWidget(PrintButton1))
   strcpy(s,"Save the mail in an ASCII file");

if (uw == UxGetWidget(ReportButton1))
   strcpy(s,"Clear entry fields: Command, Synopsis, Description, How-To-Repeat");

if (uw == UxGetWidget(SendButton1))
   strcpy(s,"Close the window");

/* Short Help */

if (uw == UxGetWidget(SHelp1))
   strcpy(s,"Short help area");

/* Now display the help message */

XmTextSetString(UxGetWidget(SHelp1),s);

}

void makeReport(file)
char file[]; 
{
 extern char mid_mail[];
 char   full_synopsis[200], category[30], logname[100], command[100];
 char   synopsis[100], ctx[30], gnats_cat[30];
 int    len, lenf, i;
 char    linux_string[80];
 swidget linux_sw;
 int     dbx=0;
 
 FILE *fp;

 strcpy(ctx,UxGetLabelString(UxFindSwidget(UxGetMenuHistory(menu4))));
 strcpy(full_synopsis,XmTextGetString(UxGetWidget(textField8)));
 strcpy(synopsis,full_synopsis);
 strcpy(command,XmTextGetString(UxGetWidget(textField9)));
 len = strlen(command);

 if (len > 0) {
     strcat(full_synopsis," - Command: ");
     lenf = strlen(full_synopsis);
     for (i=0; i<len; i++) full_synopsis[lenf+i] = toupper(command[i]);
     full_synopsis[lenf+len] = '\0';
     }

 strcpy(logname,getenv("LOGNAME"));


  if ((fp = fopen(file,"w")) == NULL) {
       sprintf(command,"Could not open file: %s with write permission\n",file);
       XmTextSetString(UxGetWidget(SHelp1),command);
     }

 fprintf(fp,"To: %s\n",mid_mail);
 fprintf(fp,"Cc: %s\n",logname);
 fprintf(fp,"Subject: %s\n",synopsis);
 fprintf(fp,"From: %s\n",logname);
 fprintf(fp,"Reply-To: %s\n",logname);
 fprintf(fp,">X-send-pr-version: XHelp Problem Report Form\n");

 fprintf(fp,">Submitter-Id:\n");

 fprintf(fp,">Originator:      %s\n",
      XmTextGetString(UxGetWidget(textField6)));

 fprintf(fp,">Organization:\n");
 fprintf(fp,">Confidential: no\n");
 
 fprintf(fp,">Synopsis:        %s\n",full_synopsis);

 fprintf(fp,">Severity:        serious\n");

 fprintf(fp,">Priority:        %s\n",
    UxGetLabelString(UxFindSwidget(UxGetMenuHistory(menu5))) );

 strcpy(linux_string, UxGetMenuHistory(rowColumn2));
 if (dbx == 1) printf("String %s\n",linux_string);
 if (linux_string[0] != 't') strcpy(linux_string,"toggleButton7");
 linux_sw = UxFindSwidget(linux_string);
 if (dbx == 1) printf("Content %s\n",(char *)linux_sw);
 strcpy(category,UxGetLabelString(linux_sw));

 /* Associating Category */
 strcpy(gnats_cat,"midas-system");
 if (category[0] == 'I' && category[1] == 'n') 
                         strcpy(gnats_cat,"midas-install");
 if (category[0] == 'D' && category[3] == 't') 
                         strcpy(gnats_cat,"midas-dist");
 if (category[0] == 'S') strcpy(gnats_cat,"midas-system");
 if (category[0] == 'D' && category[3] == 'p') 
                         strcpy(gnats_cat,"midas-display");
 if (category[0] == 'G' && category[7] == 's') 
                         strcpy(gnats_cat,"midas-graphic");
 if (category[0] == 'T') strcpy(gnats_cat,"midas-table");
 if (category[0] == 'H') strcpy(gnats_cat,"midas-doc");
 if (category[0] == 'I' && category[1] == 'm') 
                         strcpy(gnats_cat,"midas-image");
 if (category[0] == 'G' && category[7] == 'a') 
                         strcpy(gnats_cat,"midas-gui");
 if (category[0] == 'C') {
        strcpy(gnats_cat,"ctx-");
        strcat(gnats_cat,
          UxGetLabelString(UxFindSwidget(UxGetMenuHistory(menu4))) );
        }

 if (dbx == 1) printf ("Associated GNATS category: %s\n",gnats_cat);

 fprintf(fp,">Category:        "); 
 fprintf(fp,"%s\n",gnats_cat);

 strcpy(linux_string,UxGetMenuHistory(rowColumn1));
 if (dbx == 1) printf("String %s\n",linux_string);
 if (linux_string[0] != 't') strcpy(linux_string,"toggleButton1");
 linux_sw = UxFindSwidget(linux_string);
 fprintf(fp,">Class:           %s\n",UxGetLabelString(linux_sw));

 fprintf(fp,">Release:         %s\n",
      XmTextGetString(UxGetWidget(textField7)));

 fprintf(fp,">Environment:     \n");
 fprintf(fp,"%s",XmTextGetString(UxGetWidget(text3)) );

 fprintf(fp,">Description:\n"); 
 fprintf(fp,"%s\n",
      XmTextGetString(UxGetWidget(scrolledText6)));

 fprintf(fp,">How-To-Repeat:\n\n"); 
 fprintf(fp,">Fix:\n");

 fclose(fp);
}

void sendReport(file)
char file[];
{
char command[100];

 sprintf(command,
  "cat %s | /usr/lib/sendmail -oi -t ; rm %s",
  file,file);

 system(command);
}

int get_subproc(UxWidget, mod)

Widget UxWidget;
int    mod;

{
char command[1000], buffer[1000];
FILE *fpin;
int  input, pos=0;

if (mod == 2 ) 
   strcpy(buffer, getenv("MIDVERS")); 

else
   {
   strcpy( command, getenv("MIDASHOME")); 
   strcat( command, "/");
   strcat( command, getenv("MIDVERS")); 
   strcat( command, "/system/unix/");

   if (mod == 1 ) strcat(command,"environment");
   if (mod == 0 ) strcat(command,"originator");


   if ((fpin = (FILE*) popen(command,"r")) == NULL) 
      {
      printf("Could not execute command: %s\n",command);
      return(-1);
      }


   while ((input =  fgetc(fpin)) != EOF) buffer[pos++] = (char)input;
   buffer[pos] = '\0';

   if (pclose(fpin) == -1)
      printf("Could not close stream for command: %s\n",command);
   }

XmTextSetString(UxWidget,buffer);
return(0);
}

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_HInit( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	InitHelp();
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	WriteHelpAW(UxWidget);
	helpPR(UxWidget);
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	popupCB_transientShell1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	extern char mid_mail[];
	
	char text[100];
	
	sprintf(text,"Edit problem report. Button OK sends a mail to: %s\n",
	         mid_mail);
	
	UxPutText(SHelp1,text);
	
	get_subproc(UxGetWidget(textField6),0); 
	get_subproc(UxGetWidget(textField7),2); 
	
	XmTextSetTopCharacter(UxGetWidget(text3),(XmTextPosition) 0);
	XmTextSetInsertionPosition(UxGetWidget(text3),(XmTextPosition) 0);
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	focusCB_workArea1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	/* UxPutBottomOffset(scrolledWindow6,GetBottom()); */
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	mapCB_workArea1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	createCB_workArea1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	createCB_scrolledText6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	activateCB_SHelp1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	activateCB_NewsButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	 extern char filename[];
	 extern swidget pb;
	
	 makeReport(filename);
	 sendReport(filename);
	 SensMenu(1);
	 UxPopdownInterface(pb);
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	activateCB_PrintButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	extern swidget save;
	UxPopupInterface(save,exclusive_grab);
	MapRaised(save);
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	activateCB_ReportButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	UxPutText(scrolledText6,""); 
	UxPutText(textField8,""); 
	UxPutText(textField9,""); 
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	activateCB_SendButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	extern swidget pb; 
	SensMenu(1);
	UxPopdownInterface(pb);
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	createCB_textField6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	entryCB_menu1_p4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	entryCB_menu4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	printf("Hello\n");
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	entryCB_menu1_p5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	entryCB_menu5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	printf("Hello\n");
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	focusCB_text3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

static void	createCB_text3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell1     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell1Context;
	UxTransientShell1Context = UxContext =
			(_UxCtransientShell1 *) UxGetContext( UxThisWidget );
	{
	get_subproc(UxWidget,1);
	
	}
	UxTransientShell1Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_transientShell1()
{
	UxPutAllowShellResize( transientShell1, "false" );
	UxPutTitle( transientShell1, "Problem Report Form" );
	UxPutHeight( transientShell1, 750 );
	UxPutWidth( transientShell1, 572 );
	UxPutY( transientShell1, 28 );
	UxPutX( transientShell1, 654 );

	UxPutFractionBase( workArea1, 100 );
	UxPutResizePolicy( workArea1, "resize_none" );
	UxPutUnitType( workArea1, "pixels" );
	UxPutBackground( workArea1, WindowBackground );
	UxPutBorderWidth( workArea1, 0 );
	UxPutHeight( workArea1, 4 );
	UxPutWidth( workArea1, 572 );
	UxPutY( workArea1, 2 );
	UxPutX( workArea1, 20 );

	UxPutTranslations( scrolledWindow6, "" );
	UxPutBackground( scrolledWindow6, ApplicBackground );
	UxPutWidth( scrolledWindow6, 480 );
	UxPutHeight( scrolledWindow6, 160 );
	UxPutShadowThickness( scrolledWindow6, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow6, "static" );
	UxPutVisualPolicy( scrolledWindow6, "variable" );
	UxPutY( scrolledWindow6, 400 );
	UxPutX( scrolledWindow6, 5 );
	UxPutScrollingPolicy( scrolledWindow6, "application_defined" );

	UxPutHighlightOnEnter( scrolledText6, "true" );
        UxPutMaxLength(scrolledText6, 100000);
	UxPutTranslations( scrolledText6, "" );
	UxPutFontList( scrolledText6, TextFont );
	UxPutHighlightColor( scrolledText6, "black" );
	UxPutForeground( scrolledText6, TextForeground );
	UxPutEditMode( scrolledText6, "multi_line_edit" );
	UxPutScrollVertical( scrolledText6, "true" );
	UxPutScrollHorizontal( scrolledText6, "true" );
	UxPutEditable( scrolledText6, "true" );
	UxPutY( scrolledText6, 0 );
	UxPutX( scrolledText6, 0 );
	UxPutBackground( scrolledText6, "WhiteSmoke" );
	UxPutHeight( scrolledText6, 100 );
	UxPutWidth( scrolledText6, 569 );

	UxPutText( SHelp1, "Edit your message. Button OK sends a mail to the midas account" );
	UxPutAccelerators( SHelp1, "" );
	UxPutWordWrap( SHelp1, "false" );
	UxPutHighlightOnEnter( SHelp1, "true" );
	UxPutEditMode( SHelp1, "single_line_edit" );
	UxPutBlinkRate( SHelp1, 500 );
	UxPutPendingDelete( SHelp1, "true" );
	UxPutTranslations( SHelp1, "" );
	UxPutCursorPositionVisible( SHelp1, "true" );
	UxPutFontList( SHelp1, TextFont );
	UxPutHighlightColor( SHelp1, ApplicBackground );
	UxPutForeground( SHelp1, TextForeground );
	UxPutBackground( SHelp1, SHelpBackground );
	UxPutHeight( SHelp1, 40 );
	UxPutWidth( SHelp1, 325 );
	UxPutY( SHelp1, 380 );
	UxPutX( SHelp1, 30 );

	UxPutHighlightOnEnter( NewsButton1, "true" );
	UxPutTranslations( NewsButton1, "" );
	UxPutHighlightColor( NewsButton1, "Black" );
	UxPutRecomputeSize( NewsButton1, "false" );
	UxPutFontList( NewsButton1, BoldTextFont );
	UxPutForeground( NewsButton1, ButtonForeground );
	UxPutLabelString( NewsButton1, "OK" );
	UxPutBackground( NewsButton1, ButtonBackground );
	UxPutHeight( NewsButton1, 30 );
	UxPutWidth( NewsButton1, 95 );
	UxPutY( NewsButton1, 330 );
	UxPutX( NewsButton1, 180 );

	UxPutHighlightOnEnter( PrintButton1, "true" );
	UxPutTranslations( PrintButton1, "" );
	UxPutHighlightColor( PrintButton1, "Black" );
	UxPutRecomputeSize( PrintButton1, "false" );
	UxPutFontList( PrintButton1, BoldTextFont );
	UxPutForeground( PrintButton1, ButtonForeground );
	UxPutLabelString( PrintButton1, "Save As..." );
	UxPutBackground( PrintButton1, ButtonBackground );
	UxPutHeight( PrintButton1, 30 );
	UxPutWidth( PrintButton1, 95 );
	UxPutY( PrintButton1, 330 );
	UxPutX( PrintButton1, 280 );

	UxPutHighlightOnEnter( ReportButton1, "true" );
	UxPutTranslations( ReportButton1, "" );
	UxPutHighlightColor( ReportButton1, "Black" );
	UxPutRecomputeSize( ReportButton1, "false" );
	UxPutFontList( ReportButton1, BoldTextFont );
	UxPutForeground( ReportButton1, ButtonForeground );
	UxPutLabelString( ReportButton1, "Clear" );
	UxPutBackground( ReportButton1, ButtonBackground );
	UxPutHeight( ReportButton1, 30 );
	UxPutWidth( ReportButton1, 95 );
	UxPutY( ReportButton1, 330 );
	UxPutX( ReportButton1, 430 );

	UxPutHighlightOnEnter( SendButton1, "true" );
	UxPutTranslations( SendButton1, "" );
	UxPutHighlightColor( SendButton1, "Black" );
	UxPutRecomputeSize( SendButton1, "false" );
	UxPutFontList( SendButton1, BoldTextFont );
	UxPutForeground( SendButton1, ButtonForeground );
	UxPutLabelString( SendButton1, "Cancel" );
	UxPutBackground( SendButton1, ButtonBackground );
	UxPutHeight( SendButton1, 30 );
	UxPutWidth( SendButton1, 95 );
	UxPutY( SendButton1, 320 );
	UxPutX( SendButton1, 550 );

	UxPutTranslations( label8, "" );
	UxPutAlignment( label8, "alignment_beginning" );
	UxPutLabelString( label8, "Describe here the problem, indicate if relevant how to repeat and how to fix it:" );
	UxPutForeground( label8, TextForeground );
	UxPutFontList( label8, BoldTextFont );
	UxPutBackground( label8, LabelBackground );
	UxPutHeight( label8, 20 );
	UxPutWidth( label8, 100 );
	UxPutY( label8, 310 );
	UxPutX( label8, 0 );

	UxPutTranslations( label9, "" );
	UxPutAlignment( label9, "alignment_beginning" );
	UxPutLabelString( label9, "Environment:" );
	UxPutForeground( label9, TextForeground );
	UxPutFontList( label9, BoldTextFont );
	UxPutBackground( label9, LabelBackground );
	UxPutHeight( label9, 20 );
	UxPutWidth( label9, 100 );
	UxPutY( label9, 100 );
	UxPutX( label9, 0 );

	UxPutTranslations( label10, "" );
	UxPutAlignment( label10, "alignment_center" );
	UxPutLabelString( label10, "Category" );
	UxPutForeground( label10, TextForeground );
	UxPutFontList( label10, BoldTextFont );
	UxPutBackground( label10, LabelBackground );
	UxPutHeight( label10, 20 );
	UxPutWidth( label10, 100 );
	UxPutY( label10, 50 );
	UxPutX( label10, 370 );

	UxPutTranslations( label11, "" );
	UxPutAlignment( label11, "alignment_center" );
	UxPutLabelString( label11, "Class" );
	UxPutForeground( label11, TextForeground );
	UxPutFontList( label11, BoldTextFont );
	UxPutBackground( label11, LabelBackground );
	UxPutHeight( label11, 20 );
	UxPutWidth( label11, 100 );
	UxPutY( label11, 110 );
	UxPutX( label11, 10 );

	UxPutTranslations( label12, "" );
	UxPutAlignment( label12, "alignment_beginning" );
	UxPutLabelString( label12, "Priority:" );
	UxPutForeground( label12, TextForeground );
	UxPutFontList( label12, BoldTextFont );
	UxPutBackground( label12, LabelBackground );
	UxPutHeight( label12, 20 );
	UxPutWidth( label12, 70 );
	UxPutY( label12, 80 );
	UxPutX( label12, 20 );

	UxPutTranslations( label15, "" );
	UxPutAlignment( label15, "alignment_beginning" );
	UxPutLabelString( label15, "Originator:" );
	UxPutForeground( label15, TextForeground );
	UxPutFontList( label15, BoldTextFont );
	UxPutBackground( label15, LabelBackground );
	UxPutHeight( label15, 20 );
	UxPutWidth( label15, 100 );
	UxPutY( label15, 190 );
	UxPutX( label15, 010 );

	UxPutTranslations( label17, "" );
	UxPutAlignment( label17, "alignment_beginning" );
	UxPutLabelString( label17, "Synopsis:" );
	UxPutForeground( label17, TextForeground );
	UxPutFontList( label17, BoldTextFont );
	UxPutBackground( label17, LabelBackground );
	UxPutHeight( label17, 20 );
	UxPutWidth( label17, 100 );
	UxPutY( label17, 240 );
	UxPutX( label17, 10 );

	UxPutHighlightOnEnter( textField6, "true" );
	UxPutTranslations( textField6, "" );
	UxPutEditable( textField6, "true" );
	UxPutText( textField6, "" );
	UxPutForeground( textField6, TextForeground );
	UxPutFontList( textField6, TextFont );
	UxPutBackground( textField6, TextBackground );
	UxPutHeight( textField6, 35 );
	UxPutWidth( textField6, 250 );
	UxPutY( textField6, 170 );
	UxPutX( textField6, 110 );

	UxPutHighlightOnEnter( textField7, "true" );
	UxPutTranslations( textField7, "" );
	UxPutEditable( textField7, "true" );
	UxPutText( textField7, "" );
	UxPutForeground( textField7, TextForeground );
	UxPutFontList( textField7, TextFont );
	UxPutBackground( textField7, TextBackground );
	UxPutHeight( textField7, 35 );
	UxPutWidth( textField7, 250 );
	UxPutY( textField7, 210 );
	UxPutX( textField7, 110 );

	UxPutHighlightOnEnter( textField8, "true" );
	UxPutTranslations( textField8, "" );
	UxPutEditable( textField8, "true" );
	UxPutText( textField8, "" );
	UxPutForeground( textField8, TextForeground );
	UxPutFontList( textField8, TextFont );
	UxPutBackground( textField8, TextBackground );
	UxPutHeight( textField8, 35 );
	UxPutWidth( textField8, 250 );
	UxPutY( textField8, 240 );
	UxPutX( textField8, 110 );

	UxPutWhichButton( rowColumn1, 2 );
	UxPutResizeWidth( rowColumn1, "false" );
	UxPutResizeHeight( rowColumn1, "false" );
	UxPutEntryAlignment( rowColumn1, "alignment_beginning" );
	UxPutNavigationType( rowColumn1, "tab_group" );
	UxPutPacking( rowColumn1, "pack_tight" );
	UxPutRadioBehavior( rowColumn1, "true" );
	UxPutOrientation( rowColumn1, "vertical" );
	UxPutBackground( rowColumn1, WindowBackground );
	UxPutHeight( rowColumn1, 130 );
	UxPutWidth( rowColumn1, 150 );
	UxPutY( rowColumn1, 20 );
	UxPutX( rowColumn1, 110 );

	UxPutHighlightOnEnter( toggleButton1, "true" );
	UxPutTranslations( toggleButton1, "" );
	UxPutSet( toggleButton1, "true" );
	UxPutSelectColor( toggleButton1, "Yellow" );
	UxPutLabelString( toggleButton1, "support" );
	UxPutFontList( toggleButton1, BoldTextFont );
	UxPutBackground( toggleButton1, WindowBackground );
	UxPutHeight( toggleButton1, 24 );
	UxPutWidth( toggleButton1, 97 );
	UxPutY( toggleButton1, 3 );
	UxPutX( toggleButton1, 3 );

	UxPutHighlightOnEnter( toggleButton2, "true" );
	UxPutTranslations( toggleButton2, "" );
	UxPutSelectColor( toggleButton2, "Yellow" );
	UxPutLabelString( toggleButton2, "sw-bug" );
	UxPutFontList( toggleButton2, BoldTextFont );
	UxPutBackground( toggleButton2, WindowBackground );
	UxPutHeight( toggleButton2, 100 );
	UxPutWidth( toggleButton2, 50 );
	UxPutY( toggleButton2, 10 );
	UxPutX( toggleButton2, 100 );

	UxPutHighlightOnEnter( toggleButton3, "true" );
	UxPutTranslations( toggleButton3, "" );
	UxPutSelectColor( toggleButton3, "Yellow" );
	UxPutLabelString( toggleButton3, "doc-bug" );
	UxPutFontList( toggleButton3, BoldTextFont );
	UxPutBackground( toggleButton3, WindowBackground );
	UxPutHeight( toggleButton3, 77 );
	UxPutWidth( toggleButton3, 142 );
	UxPutY( toggleButton3, 3 );
	UxPutX( toggleButton3, 180 );

	UxPutHighlightOnEnter( toggleButton4, "true" );
	UxPutTranslations( toggleButton4, "" );
	UxPutSelectColor( toggleButton4, "Yellow" );
	UxPutLabelString( toggleButton4, "change-request" );
	UxPutFontList( toggleButton4, BoldTextFont );
	UxPutBackground( toggleButton4, WindowBackground );
	UxPutHeight( toggleButton4, 100 );
	UxPutWidth( toggleButton4, 50 );
	UxPutY( toggleButton4, 60 );
	UxPutX( toggleButton4, 20 );

	UxPutTranslations( menu1_p4, transTable6 );
	UxPutTopShadowColor( menu1_p4, "White" );
	UxPutForeground( menu1_p4, TextForeground );
	UxPutBackground( menu1_p4, TextBackground );
	UxPutRowColumnType( menu1_p4, "menu_pulldown" );

	UxPutFontList( menu1_p1_b13, TextFont );
	UxPutLabelString( menu1_p1_b13, "alice" );

	UxPutFontList( menu1_p1_b14, TextFont );
	UxPutLabelString( menu1_p1_b14, "astromet" );

	UxPutFontList( menu1_p4_b3, TextFont );
	UxPutLabelString( menu1_p4_b3, "ccd" );

	UxPutFontList( menu1_p4_b4, TextFont );
	UxPutLabelString( menu1_p4_b4, "cloud" );

	UxPutFontList( menu1_p4_b5, TextFont );
	UxPutLabelString( menu1_p4_b5, "daophot" );

	UxPutFontList( menu1_p4_b6, TextFont );
	UxPutLabelString( menu1_p4_b6, "do" );

	UxPutFontList( menu1_p4_b7, TextFont );
	UxPutLabelString( menu1_p4_b7, "echelle" );

	UxPutFontList( menu1_p4_b8, TextFont );
	UxPutLabelString( menu1_p4_b8, "esolv" );

	UxPutFontList( menu1_p4_b9, TextFont );
	UxPutLabelString( menu1_p4_b9, "geotest" );

	UxPutFontList( menu1_p4_b10, TextFont );
	UxPutLabelString( menu1_p4_b10, "imres" );

	UxPutFontList( menu1_p4_b11, TextFont );
	UxPutLabelString( menu1_p4_b11, "invent" );

	UxPutFontList( menu1_p4_b12, TextFont );
	UxPutLabelString( menu1_p4_b12, "irspec" );

	UxPutFontList( menu1_p4_b13, TextFont );
	UxPutLabelString( menu1_p4_b13, "iue" );

	UxPutFontList( menu1_p4_b14, TextFont );
	UxPutLabelString( menu1_p4_b14, "long" );

	UxPutFontList( menu1_p4_b15, TextFont );
	UxPutLabelString( menu1_p4_b15, "lyman" );

	UxPutFontList( menu1_p4_b16, TextFont );
	UxPutLabelString( menu1_p4_b16, "mos" );

	UxPutFontList( menu1_p4_b17, TextFont );
	UxPutLabelString( menu1_p4_b17, "mva" );

	UxPutFontList( menu1_p4_b18, TextFont );
	UxPutLabelString( menu1_p4_b18, "optopus" );

	UxPutFontList( menu1_p4_b19, TextFont );
	UxPutLabelString( menu1_p4_b19, "pepsys" );

	UxPutFontList( menu1_p4_b20, TextFont );
	UxPutLabelString( menu1_p4_b20, "pisco" );

	UxPutFontList( menu1_p4_b21, TextFont );
	UxPutLabelString( menu1_p4_b21, "romafot" );

	UxPutFontList( menu1_p4_b22, TextFont );
	UxPutLabelString( menu1_p4_b22, "spec" );

	UxPutFontList( menu1_p4_b23, TextFont );
	UxPutLabelString( menu1_p4_b23, "statist" );

	UxPutFontList( menu1_p4_b24, TextFont );
	UxPutLabelString( menu1_p4_b24, "surfphot" );

	UxPutFontList( menu1_p4_b25, TextFont );
	UxPutLabelString( menu1_p4_b25, "tsa" );

	UxPutFontList( menu1_p4_b26, TextFont );
	UxPutLabelString( menu1_p4_b26, "wavelet" );

	UxPutLabelString( menu4, "" );
	UxPutMarginWidth( menu4, 0 );
	UxPutMarginHeight( menu4, 0 );
	UxPutWhichButton( menu4, 1 );
	UxPutY( menu4, 0 );
	UxPutX( menu4, 290 );
	UxPutHeight( menu4, 8 );
	UxPutTranslations( menu4, transTable6 );
	UxPutWidth( menu4, 200 );
	UxPutForeground( menu4, TextForeground );
	UxPutBackground( menu4, ApplicBackground );
	UxPutRowColumnType( menu4, "menu_option" );

	UxPutTranslations( menu1_p5, transTable6 );
	UxPutTopShadowColor( menu1_p5, "White" );
	UxPutForeground( menu1_p5, TextForeground );
	UxPutBackground( menu1_p5, TextBackground );
	UxPutRowColumnType( menu1_p5, "menu_pulldown" );

	UxPutFontList( menu1_p1_b15, TextFont );
	UxPutAccelerator( menu1_p1_b15, "m" );
	UxPutLabelString( menu1_p1_b15, "low" );

	UxPutFontList( menu1_p1_b16, TextFont );
	UxPutAccelerator( menu1_p1_b16, "d" );
	UxPutLabelString( menu1_p1_b16, "medium" );

	UxPutFontList( menu1_p4_b1, TextFont );
	UxPutLabelString( menu1_p4_b1, "high" );

	UxPutLabelString( menu5, "" );
	UxPutY( menu5, 10 );
	UxPutX( menu5, 470 );
	UxPutHeight( menu5, 10 );
	UxPutTranslations( menu5, transTable6 );
	UxPutWidth( menu5, 200 );
	UxPutForeground( menu5, TextForeground );
	UxPutBackground( menu5, ApplicBackground );
	UxPutRowColumnType( menu5, "menu_option" );

	UxPutWhichButton( rowColumn2, 0 );
	UxPutShadowThickness( rowColumn2, 3 );
	UxPutNumColumns( rowColumn2, 2 );
	UxPutResizeWidth( rowColumn2, "false" );
	UxPutResizeHeight( rowColumn2, "false" );
	UxPutEntryAlignment( rowColumn2, "alignment_beginning" );
	UxPutNavigationType( rowColumn2, "tab_group" );
	UxPutPacking( rowColumn2, "pack_tight" );
	UxPutRadioBehavior( rowColumn2, "true" );
	UxPutOrientation( rowColumn2, "vertical" );
	UxPutBackground( rowColumn2, WindowBackground );
	UxPutHeight( rowColumn2, 160 );
	UxPutWidth( rowColumn2, 350 );
	UxPutY( rowColumn2, 77 );
	UxPutX( rowColumn2, 112 );

	UxPutIndicatorOn( toggleButton5, "true" );
	UxPutHighlightOnEnter( toggleButton5, "true" );
	UxPutTranslations( toggleButton5, "" );
	UxPutSet( toggleButton5, "false" );
	UxPutSelectColor( toggleButton5, "Yellow" );
	UxPutLabelString( toggleButton5, "Installation" );
	UxPutFontList( toggleButton5, BoldTextFont );
	UxPutBackground( toggleButton5, WindowBackground );
	UxPutHeight( toggleButton5, 24 );
	UxPutWidth( toggleButton5, 97 );
	UxPutY( toggleButton5, 3 );
	UxPutX( toggleButton5, 3 );

	UxPutHighlightOnEnter( toggleButton6, "true" );
	UxPutTranslations( toggleButton6, "" );
	UxPutSelectColor( toggleButton6, "Yellow" );
	UxPutLabelString( toggleButton6, "Distribution" );
	UxPutFontList( toggleButton6, BoldTextFont );
	UxPutBackground( toggleButton6, WindowBackground );
	UxPutHeight( toggleButton6, 100 );
	UxPutWidth( toggleButton6, 50 );
	UxPutY( toggleButton6, 10 );
	UxPutX( toggleButton6, 100 );

	UxPutSet( toggleButton7, "true" );
	UxPutHighlightOnEnter( toggleButton7, "true" );
	UxPutTranslations( toggleButton7, "" );
	UxPutSelectColor( toggleButton7, "Yellow" );
	UxPutLabelString( toggleButton7, "System" );
	UxPutFontList( toggleButton7, BoldTextFont );
	UxPutBackground( toggleButton7, WindowBackground );
	UxPutHeight( toggleButton7, 100 );
	UxPutWidth( toggleButton7, 50 );
	UxPutY( toggleButton7, 50 );
	UxPutX( toggleButton7, 60 );

	UxPutHighlightOnEnter( toggleButton8, "true" );
	UxPutTranslations( toggleButton8, "" );
	UxPutSelectColor( toggleButton8, "Yellow" );
	UxPutLabelString( toggleButton8, "Display" );
	UxPutFontList( toggleButton8, BoldTextFont );
	UxPutBackground( toggleButton8, WindowBackground );
	UxPutHeight( toggleButton8, 100 );
	UxPutWidth( toggleButton8, 50 );
	UxPutY( toggleButton8, 60 );
	UxPutX( toggleButton8, 20 );

	UxPutHighlightOnEnter( toggleButton9, "true" );
	UxPutTranslations( toggleButton9, "" );
	UxPutSelectColor( toggleButton9, "Yellow" );
	UxPutLabelString( toggleButton9, "Graphics" );
	UxPutFontList( toggleButton9, BoldTextFont );
	UxPutBackground( toggleButton9, WindowBackground );
	UxPutHeight( toggleButton9, 100 );
	UxPutWidth( toggleButton9, 50 );
	UxPutY( toggleButton9, -35 );
	UxPutX( toggleButton9, 188 );

	UxPutHighlightOnEnter( toggleButton10, "true" );
	UxPutTranslations( toggleButton10, "" );
	UxPutSelectColor( toggleButton10, "Yellow" );
	UxPutLabelString( toggleButton10, "Tables" );
	UxPutFontList( toggleButton10, BoldTextFont );
	UxPutBackground( toggleButton10, WindowBackground );
	UxPutHeight( toggleButton10, 100 );
	UxPutWidth( toggleButton10, 50 );
	UxPutY( toggleButton10, -35 );
	UxPutX( toggleButton10, 188 );

	UxPutHighlightOnEnter( toggleButton11, "true" );
	UxPutTranslations( toggleButton11, "" );
	UxPutSelectColor( toggleButton11, "Yellow" );
	UxPutLabelString( toggleButton11, "Help & Documentation" );
	UxPutFontList( toggleButton11, BoldTextFont );
	UxPutBackground( toggleButton11, WindowBackground );
	UxPutHeight( toggleButton11, 100 );
	UxPutWidth( toggleButton11, 50 );
	UxPutY( toggleButton11, -35 );
	UxPutX( toggleButton11, 188 );

	UxPutHighlightOnEnter( toggleButton12, "true" );
	UxPutTranslations( toggleButton12, "" );
	UxPutSelectColor( toggleButton12, "Yellow" );
	UxPutLabelString( toggleButton12, "Image Operations" );
	UxPutFontList( toggleButton12, BoldTextFont );
	UxPutBackground( toggleButton12, WindowBackground );
	UxPutHeight( toggleButton12, 100 );
	UxPutWidth( toggleButton12, 50 );
	UxPutY( toggleButton12, -35 );
	UxPutX( toggleButton12, 188 );

	UxPutHighlightOnEnter( toggleButton13, "true" );
	UxPutTranslations( toggleButton13, "" );
	UxPutSelectColor( toggleButton13, "Yellow" );
	UxPutLabelString( toggleButton13, "Graphical User Interfaces" );
	UxPutFontList( toggleButton13, BoldTextFont );
	UxPutBackground( toggleButton13, WindowBackground );
	UxPutHeight( toggleButton13, 100 );
	UxPutWidth( toggleButton13, 50 );
	UxPutY( toggleButton13, 60 );
	UxPutX( toggleButton13, 20 );

	UxPutHighlightOnEnter( toggleButton14, "true" );
	UxPutTranslations( toggleButton14, "" );
	UxPutSelectColor( toggleButton14, "Yellow" );
	UxPutLabelString( toggleButton14, "Context: " );
	UxPutFontList( toggleButton14, BoldTextFont );
	UxPutBackground( toggleButton14, WindowBackground );
	UxPutHeight( toggleButton14, 100 );
	UxPutWidth( toggleButton14, 50 );
	UxPutY( toggleButton14, 60 );
	UxPutX( toggleButton14, 20 );

	UxPutAutoShowCursorPosition( text3, "true" );
	UxPutTranslations( text3, "" );
	UxPutHighlightOnEnter( text3, "true" );
	UxPutForeground( text3, TextForeground );
	UxPutFontList( text3, TextFont );
	UxPutEditMode( text3, "multi_line_edit" );
	UxPutBackground( text3, TextBackground );
	UxPutHeight( text3, 60 );
	UxPutWidth( text3, 440 );
	UxPutY( text3, 250 );
	UxPutX( text3, 130 );

	UxPutTranslations( label18, "" );
	UxPutAlignment( label18, "alignment_beginning" );
	UxPutLabelString( label18, "Command:" );
	UxPutForeground( label18, TextForeground );
	UxPutFontList( label18, BoldTextFont );
	UxPutBackground( label18, LabelBackground );
	UxPutHeight( label18, 20 );
	UxPutWidth( label18, 100 );
	UxPutY( label18, 280 );
	UxPutX( label18, 10 );

	UxPutHighlightOnEnter( textField9, "true" );
	UxPutTranslations( textField9, "" );
	UxPutEditable( textField9, "true" );
	UxPutText( textField9, "" );
	UxPutForeground( textField9, TextForeground );
	UxPutFontList( textField9, TextFont );
	UxPutBackground( textField9, TextBackground );
	UxPutHeight( textField9, 35 );
	UxPutWidth( textField9, 250 );
	UxPutY( textField9, 270 );
	UxPutX( textField9, 110 );

	UxPutTranslations( label16, "" );
	UxPutAlignment( label16, "alignment_beginning" );
	UxPutLabelString( label16, "Release:" );
	UxPutForeground( label16, TextForeground );
	UxPutFontList( label16, BoldTextFont );
	UxPutBackground( label16, LabelBackground );
	UxPutHeight( label16, 20 );
	UxPutWidth( label16, 70 );
	UxPutY( label16, 220 );
	UxPutX( label16, 10 );

	UxPutBackground( separator1, WindowBackground );
	UxPutSeparatorType( separator1, "single_line" );
	UxPutHeight( separator1, 4 );
	UxPutWidth( separator1, 350 );
	UxPutY( separator1, 210 );
	UxPutX( separator1, 200 );

	UxPutOrientation( separator2, "vertical" );
	UxPutBackground( separator2, WindowBackground );
	UxPutSeparatorType( separator2, "single_line" );
	UxPutHeight( separator2, 200 );
	UxPutWidth( separator2, 4 );
	UxPutY( separator2, 210 );
	UxPutX( separator2, 200 );

	UxPutOrientation( separator3, "vertical" );
	UxPutBackground( separator3, WindowBackground );
	UxPutSeparatorType( separator3, "single_line" );
	UxPutHeight( separator3, 200 );
	UxPutWidth( separator3, 4 );
	UxPutY( separator3, 210 );
	UxPutX( separator3, 200 );

	UxPutBackground( separator4, WindowBackground );
	UxPutSeparatorType( separator4, "single_line" );
	UxPutHeight( separator4, 4 );
	UxPutWidth( separator4, 350 );
	UxPutY( separator4, 210 );
	UxPutX( separator4, 200 );

	UxPutOrientation( separator5, "vertical" );
	UxPutBackground( separator5, WindowBackground );
	UxPutSeparatorType( separator5, "single_line" );
	UxPutHeight( separator5, 200 );
	UxPutWidth( separator5, 4 );
	UxPutY( separator5, 210 );
	UxPutX( separator5, 200 );

	UxPutBackground( separator6, WindowBackground );
	UxPutSeparatorType( separator6, "single_line" );
	UxPutHeight( separator6, 4 );
	UxPutWidth( separator6, 350 );
	UxPutY( separator6, 210 );
	UxPutX( separator6, 200 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_transientShell1()
{
	/* Create the swidgets */

	transientShell1 = UxCreateTransientShell( "transientShell1", NO_PARENT );
	UxPutContext( transientShell1, UxTransientShell1Context );

	workArea1 = UxCreateForm( "workArea1", transientShell1 );
	scrolledWindow6 = UxCreateScrolledWindow( "scrolledWindow6", workArea1 );
	scrolledText6 = UxCreateScrolledText( "scrolledText6", scrolledWindow6 );
	SHelp1 = UxCreateText( "SHelp1", workArea1 );
	NewsButton1 = UxCreatePushButton( "NewsButton1", workArea1 );
	PrintButton1 = UxCreatePushButton( "PrintButton1", workArea1 );
	ReportButton1 = UxCreatePushButton( "ReportButton1", workArea1 );
	SendButton1 = UxCreatePushButton( "SendButton1", workArea1 );
	label8 = UxCreateLabel( "label8", workArea1 );
	label9 = UxCreateLabel( "label9", workArea1 );
	label10 = UxCreateLabel( "label10", workArea1 );
	label11 = UxCreateLabel( "label11", workArea1 );
	label12 = UxCreateLabel( "label12", workArea1 );
	label15 = UxCreateLabel( "label15", workArea1 );
	label17 = UxCreateLabel( "label17", workArea1 );
	textField6 = UxCreateText( "textField6", workArea1 );
	textField7 = UxCreateText( "textField7", workArea1 );
	textField8 = UxCreateText( "textField8", workArea1 );
	rowColumn1 = UxCreateRowColumn( "rowColumn1", workArea1 );
	toggleButton1 = UxCreateToggleButton( "toggleButton1", rowColumn1 );
	toggleButton2 = UxCreateToggleButton( "toggleButton2", rowColumn1 );
	toggleButton3 = UxCreateToggleButton( "toggleButton3", rowColumn1 );
	toggleButton4 = UxCreateToggleButton( "toggleButton4", rowColumn1 );
	menu1_p4 = UxCreateRowColumn( "menu1_p4", workArea1 );
	menu1_p1_b13 = UxCreatePushButtonGadget( "menu1_p1_b13", menu1_p4 );
	menu1_p1_b14 = UxCreatePushButtonGadget( "menu1_p1_b14", menu1_p4 );
	menu1_p4_b3 = UxCreatePushButtonGadget( "menu1_p4_b3", menu1_p4 );
	menu1_p4_b4 = UxCreatePushButtonGadget( "menu1_p4_b4", menu1_p4 );
	menu1_p4_b5 = UxCreatePushButtonGadget( "menu1_p4_b5", menu1_p4 );
	menu1_p4_b6 = UxCreatePushButtonGadget( "menu1_p4_b6", menu1_p4 );
	menu1_p4_b7 = UxCreatePushButtonGadget( "menu1_p4_b7", menu1_p4 );
	menu1_p4_b8 = UxCreatePushButtonGadget( "menu1_p4_b8", menu1_p4 );
	menu1_p4_b9 = UxCreatePushButtonGadget( "menu1_p4_b9", menu1_p4 );
	menu1_p4_b10 = UxCreatePushButtonGadget( "menu1_p4_b10", menu1_p4 );
	menu1_p4_b11 = UxCreatePushButtonGadget( "menu1_p4_b11", menu1_p4 );
	menu1_p4_b12 = UxCreatePushButtonGadget( "menu1_p4_b12", menu1_p4 );
	menu1_p4_b13 = UxCreatePushButtonGadget( "menu1_p4_b13", menu1_p4 );
	menu1_p4_b14 = UxCreatePushButtonGadget( "menu1_p4_b14", menu1_p4 );
	menu1_p4_b15 = UxCreatePushButtonGadget( "menu1_p4_b15", menu1_p4 );
	menu1_p4_b16 = UxCreatePushButtonGadget( "menu1_p4_b16", menu1_p4 );
	menu1_p4_b17 = UxCreatePushButtonGadget( "menu1_p4_b17", menu1_p4 );
	menu1_p4_b18 = UxCreatePushButtonGadget( "menu1_p4_b18", menu1_p4 );
	menu1_p4_b19 = UxCreatePushButtonGadget( "menu1_p4_b19", menu1_p4 );
	menu1_p4_b20 = UxCreatePushButtonGadget( "menu1_p4_b20", menu1_p4 );
	menu1_p4_b21 = UxCreatePushButtonGadget( "menu1_p4_b21", menu1_p4 );
	menu1_p4_b22 = UxCreatePushButtonGadget( "menu1_p4_b22", menu1_p4 );
	menu1_p4_b23 = UxCreatePushButtonGadget( "menu1_p4_b23", menu1_p4 );
	menu1_p4_b24 = UxCreatePushButtonGadget( "menu1_p4_b24", menu1_p4 );
	menu1_p4_b25 = UxCreatePushButtonGadget( "menu1_p4_b25", menu1_p4 );
	menu1_p4_b26 = UxCreatePushButtonGadget( "menu1_p4_b26", menu1_p4 );
	menu4 = UxCreateRowColumn( "menu4", workArea1 );
	menu1_p5 = UxCreateRowColumn( "menu1_p5", workArea1 );
	menu1_p1_b15 = UxCreatePushButtonGadget( "menu1_p1_b15", menu1_p5 );
	menu1_p1_b16 = UxCreatePushButtonGadget( "menu1_p1_b16", menu1_p5 );
	menu1_p4_b1 = UxCreatePushButtonGadget( "menu1_p4_b1", menu1_p5 );
	menu5 = UxCreateRowColumn( "menu5", workArea1 );
	rowColumn2 = UxCreateRowColumn( "rowColumn2", workArea1 );
	toggleButton5 = UxCreateToggleButton( "toggleButton5", rowColumn2 );
	toggleButton6 = UxCreateToggleButton( "toggleButton6", rowColumn2 );
	toggleButton7 = UxCreateToggleButton( "toggleButton7", rowColumn2 );
	toggleButton8 = UxCreateToggleButton( "toggleButton8", rowColumn2 );
	toggleButton9 = UxCreateToggleButton( "toggleButton9", rowColumn2 );
	toggleButton10 = UxCreateToggleButton( "toggleButton10", rowColumn2 );
	toggleButton11 = UxCreateToggleButton( "toggleButton11", rowColumn2 );
	toggleButton12 = UxCreateToggleButton( "toggleButton12", rowColumn2 );
	toggleButton13 = UxCreateToggleButton( "toggleButton13", rowColumn2 );
	toggleButton14 = UxCreateToggleButton( "toggleButton14", rowColumn2 );
	text3 = UxCreateText( "text3", workArea1 );
	label18 = UxCreateLabel( "label18", workArea1 );
	textField9 = UxCreateText( "textField9", workArea1 );
	label16 = UxCreateLabel( "label16", workArea1 );
	separator1 = UxCreateSeparator( "separator1", workArea1 );
	separator2 = UxCreateSeparator( "separator2", workArea1 );
	separator3 = UxCreateSeparator( "separator3", workArea1 );
	separator4 = UxCreateSeparator( "separator4", workArea1 );
	separator5 = UxCreateSeparator( "separator5", workArea1 );
	separator6 = UxCreateSeparator( "separator6", workArea1 );

	_Uxinit_transientShell1();

	/* Create the X widgets */

	UxCreateWidget( transientShell1 );
	UxCreateWidget( workArea1 );
	createCB_workArea1( UxGetWidget( workArea1 ),
			(XtPointer) UxTransientShell1Context, (XtPointer) NULL );

	UxPutBottomOffset( scrolledWindow6, 100 );
	UxPutTopOffset( scrolledWindow6, 440 );
	UxPutBottomPosition( scrolledWindow6, 68 );
	UxPutTopAttachment( scrolledWindow6, "attach_form" );
	UxPutRightOffset( scrolledWindow6, 5 );
	UxPutRightAttachment( scrolledWindow6, "attach_form" );
	UxPutLeftOffset( scrolledWindow6, 5 );
	UxPutLeftAttachment( scrolledWindow6, "attach_form" );
	UxPutBottomWidget( scrolledWindow6, "" );
	UxPutBottomAttachment( scrolledWindow6, "attach_form" );
	UxCreateWidget( scrolledWindow6 );

	UxCreateWidget( scrolledText6 );
	createCB_scrolledText6( UxGetWidget( scrolledText6 ),
			(XtPointer) UxTransientShell1Context, (XtPointer) NULL );

	UxPutTopWidget( SHelp1, "scrolledWindow6" );
	UxPutTopOffset( SHelp1, 10 );
	UxPutTopAttachment( SHelp1, "attach_widget" );
	UxPutBottomOffset( SHelp1, 0 );
	UxPutBottomAttachment( SHelp1, "attach_none" );
	UxPutRightOffset( SHelp1, 5 );
	UxPutRightAttachment( SHelp1, "attach_form" );
	UxPutLeftOffset( SHelp1, 5 );
	UxPutLeftAttachment( SHelp1, "attach_form" );
	UxCreateWidget( SHelp1 );

	UxPutLeftOffset( NewsButton1, 10 );
	UxPutLeftAttachment( NewsButton1, "attach_form" );
	UxPutTopWidget( NewsButton1, "SHelp1" );
	UxPutTopOffset( NewsButton1, 5 );
	UxPutTopAttachment( NewsButton1, "attach_widget" );
	UxCreateWidget( NewsButton1 );

	UxPutLeftWidget( PrintButton1, "NewsButton1" );
	UxPutLeftOffset( PrintButton1, 10 );
	UxPutLeftAttachment( PrintButton1, "attach_widget" );
	UxPutTopWidget( PrintButton1, "SHelp1" );
	UxPutTopOffset( PrintButton1, 5 );
	UxPutTopAttachment( PrintButton1, "attach_widget" );
	UxCreateWidget( PrintButton1 );

	UxPutTopWidget( ReportButton1, "SHelp1" );
	UxPutTopOffset( ReportButton1, 5 );
	UxPutTopAttachment( ReportButton1, "attach_widget" );
	UxPutLeftWidget( ReportButton1, "PrintButton1" );
	UxPutLeftOffset( ReportButton1, 10 );
	UxPutLeftAttachment( ReportButton1, "attach_widget" );
	UxCreateWidget( ReportButton1 );

	UxPutTopWidget( SendButton1, "SHelp1" );
	UxPutTopOffset( SendButton1, 5 );
	UxPutTopAttachment( SendButton1, "attach_widget" );
	UxPutLeftWidget( SendButton1, "ReportButton1" );
	UxPutLeftOffset( SendButton1, 10 );
	UxPutLeftAttachment( SendButton1, "attach_widget" );
	UxCreateWidget( SendButton1 );

	UxPutRightOffset( label8, 20 );
	UxPutRightAttachment( label8, "attach_form" );
	UxPutLeftOffset( label8, 10 );
	UxPutLeftAttachment( label8, "attach_form" );
	UxPutBottomWidget( label8, "scrolledWindow6" );
	UxPutBottomOffset( label8, 2 );
	UxPutBottomAttachment( label8, "attach_widget" );
	UxCreateWidget( label8 );

	UxPutTopOffset( label9, 265 );
	UxPutTopAttachment( label9, "attach_form" );
	UxPutLeftOffset( label9, 10 );
	UxPutLeftAttachment( label9, "attach_form" );
	UxCreateWidget( label9 );

	UxPutRightOffset( label10, 150 );
	UxPutRightAttachment( label10, "attach_form" );
	UxPutTopOffset( label10, 5 );
	UxPutTopAttachment( label10, "attach_form" );
	UxPutLeftOffset( label10, 10 );
	UxPutLeftAttachment( label10, "attach_none" );
	UxCreateWidget( label10 );

	UxPutTopOffset( label11, 5 );
	UxPutTopAttachment( label11, "attach_form" );
	UxPutLeftOffset( label11, 50 );
	UxPutLeftAttachment( label11, "attach_form" );
	UxCreateWidget( label11 );

	UxPutTopOffset( label12, 177 );
	UxPutTopAttachment( label12, "attach_form" );
	UxPutLeftOffset( label12, 20 );
	UxPutLeftAttachment( label12, "attach_form" );
	UxCreateWidget( label12 );

	UxPutTopOffset( label15, 225 );
	UxPutTopAttachment( label15, "attach_form" );
	UxPutLeftOffset( label15, 10 );
	UxPutLeftAttachment( label15, "attach_form" );
	UxCreateWidget( label15 );

	UxPutTopOffset( label17, 370 );
	UxPutTopAttachment( label17, "attach_form" );
	UxPutLeftOffset( label17, 10 );
	UxPutLeftAttachment( label17, "attach_form" );
	UxCreateWidget( label17 );

	UxPutTopOffset( textField6, 220 );
	UxPutTopAttachment( textField6, "attach_form" );
	UxPutLeftOffset( textField6, 120 );
	UxPutRightOffset( textField6, 220 );
	UxPutRightAttachment( textField6, "attach_form" );
	UxPutLeftAttachment( textField6, "attach_form" );
	UxCreateWidget( textField6 );
	createCB_textField6( UxGetWidget( textField6 ),
			(XtPointer) UxTransientShell1Context, (XtPointer) NULL );

	UxPutLeftWidget( textField7, "textField6" );
	UxPutTopOffset( textField7, 220 );
	UxPutTopAttachment( textField7, "attach_form" );
	UxPutLeftOffset( textField7, 80 );
	UxPutRightOffset( textField7, 20 );
	UxPutRightAttachment( textField7, "attach_form" );
	UxPutLeftAttachment( textField7, "attach_widget" );
	UxCreateWidget( textField7 );

	UxPutTopOffset( textField8, 365 );
	UxPutTopAttachment( textField8, "attach_form" );
	UxPutLeftOffset( textField8, 120 );
	UxPutRightOffset( textField8, 20 );
	UxPutRightAttachment( textField8, "attach_form" );
	UxPutLeftAttachment( textField8, "attach_form" );
	UxCreateWidget( textField8 );

	UxPutTopAttachment( rowColumn1, "attach_form" );
	UxPutLeftAttachment( rowColumn1, "attach_form" );
	UxPutTopOffset( rowColumn1, 30 );
	UxPutRightOffset( rowColumn1, 20 );
	UxPutRightAttachment( rowColumn1, "attach_none" );
	UxPutLeftOffset( rowColumn1, 10 );
	UxCreateWidget( rowColumn1 );

	UxCreateWidget( toggleButton1 );
	UxCreateWidget( toggleButton2 );
	UxCreateWidget( toggleButton3 );
	UxCreateWidget( toggleButton4 );
	UxCreateWidget( menu1_p4 );
	UxCreateWidget( menu1_p1_b13 );
	UxCreateWidget( menu1_p1_b14 );
	UxCreateWidget( menu1_p4_b3 );
	UxCreateWidget( menu1_p4_b4 );
	UxCreateWidget( menu1_p4_b5 );
	UxCreateWidget( menu1_p4_b6 );
	UxCreateWidget( menu1_p4_b7 );
	UxCreateWidget( menu1_p4_b8 );
	UxCreateWidget( menu1_p4_b9 );
	UxCreateWidget( menu1_p4_b10 );
	UxCreateWidget( menu1_p4_b11 );
	UxCreateWidget( menu1_p4_b12 );
	UxCreateWidget( menu1_p4_b13 );
	UxCreateWidget( menu1_p4_b14 );
	UxCreateWidget( menu1_p4_b15 );
	UxCreateWidget( menu1_p4_b16 );
	UxCreateWidget( menu1_p4_b17 );
	UxCreateWidget( menu1_p4_b18 );
	UxCreateWidget( menu1_p4_b19 );
	UxCreateWidget( menu1_p4_b20 );
	UxCreateWidget( menu1_p4_b21 );
	UxCreateWidget( menu1_p4_b22 );
	UxCreateWidget( menu1_p4_b23 );
	UxCreateWidget( menu1_p4_b24 );
	UxCreateWidget( menu1_p4_b25 );
	UxCreateWidget( menu1_p4_b26 );
	UxPutRightOffset( menu4, 40 );
	UxPutRightPosition( menu4, 50 );
	UxPutRightAttachment( menu4, "attach_form" );
	UxPutTopOffset( menu4, 155 );
	UxPutTopAttachment( menu4, "attach_form" );
	UxPutLeftOffset( menu4, 260 );
	UxPutLeftAttachment( menu4, "attach_none" );
	UxPutSubMenuId( menu4, "menu1_p4" );
	UxCreateWidget( menu4 );

	UxCreateWidget( menu1_p5 );
	UxCreateWidget( menu1_p1_b15 );
	UxCreateWidget( menu1_p1_b16 );
	UxCreateWidget( menu1_p4_b1 );
	UxPutTopOffset( menu5, 167 );
	UxPutTopAttachment( menu5, "attach_form" );
	UxPutLeftOffset( menu5, 80 );
	UxPutLeftAttachment( menu5, "attach_form" );
	UxPutSubMenuId( menu5, "menu1_p5" );
	UxCreateWidget( menu5 );

	UxPutTopOffset( rowColumn2, 30 );
	UxPutTopAttachment( rowColumn2, "attach_form" );
	UxPutRightOffset( rowColumn2, 20 );
	UxPutRightAttachment( rowColumn2, "attach_form" );
	UxPutLeftOffset( rowColumn2, 120 );
	UxPutLeftAttachment( rowColumn2, "attach_none" );
	UxCreateWidget( rowColumn2 );

	UxCreateWidget( toggleButton5 );
	UxCreateWidget( toggleButton6 );
	UxCreateWidget( toggleButton7 );
	UxCreateWidget( toggleButton8 );
	UxCreateWidget( toggleButton9 );
	UxCreateWidget( toggleButton10 );
	UxCreateWidget( toggleButton11 );
	UxCreateWidget( toggleButton12 );
	UxCreateWidget( toggleButton13 );
	UxCreateWidget( toggleButton14 );
	UxPutTopOffset( text3, 260 );
	UxPutTopAttachment( text3, "attach_form" );
	UxPutRightOffset( text3, 20 );
	UxPutRightAttachment( text3, "attach_form" );
	UxPutLeftOffset( text3, 120 );
	UxPutLeftAttachment( text3, "attach_form" );
	UxCreateWidget( text3 );
	createCB_text3( UxGetWidget( text3 ),
			(XtPointer) UxTransientShell1Context, (XtPointer) NULL );

	UxPutLeftOffset( label18, 10 );
	UxPutTopOffset( label18, 330 );
	UxPutTopAttachment( label18, "attach_form" );
	UxPutLeftAttachment( label18, "attach_form" );
	UxCreateWidget( label18 );

	UxPutTopOffset( textField9, 325 );
	UxPutTopAttachment( textField9, "attach_form" );
	UxPutRightOffset( textField9, 20 );
	UxPutRightAttachment( textField9, "attach_form" );
	UxPutLeftOffset( textField9, 120 );
	UxPutLeftAttachment( textField9, "attach_form" );
	UxCreateWidget( textField9 );

	UxPutLeftWidget( label16, "textField6" );
	UxPutTopOffset( label16, 225 );
	UxPutTopAttachment( label16, "attach_form" );
	UxPutLeftOffset( label16, 5 );
	UxPutLeftAttachment( label16, "attach_widget" );
	UxCreateWidget( label16 );

	UxPutLeftWidget( separator1, "rowColumn2" );
	UxPutLeftOffset( separator1, 10 );
	UxPutLeftAttachment( separator1, "attach_form" );
	UxPutRightWidget( separator1, "rowColumn2" );
	UxPutRightOffset( separator1, 20 );
	UxPutRightAttachment( separator1, "attach_form" );
	UxPutTopWidget( separator1, "rowColumn2" );
	UxPutTopOffset( separator1, 15 );
	UxPutTopAttachment( separator1, "attach_widget" );
	UxCreateWidget( separator1 );

	UxPutBottomOffset( separator2, 0 );
	UxPutRightWidget( separator2, "rowColumn2" );
	UxPutRightOffset( separator2, 0 );
	UxPutRightAttachment( separator2, "attach_widget" );
	UxPutTopWidget( separator2, "rowColumn2" );
	UxPutTopOffset( separator2, 15 );
	UxPutTopAttachment( separator2, "attach_form" );
	UxPutBottomWidget( separator2, "separator1" );
	UxPutBottomAttachment( separator2, "attach_widget" );
	UxPutLeftWidget( separator2, "rowColumn2" );
	UxPutLeftOffset( separator2, 0 );
	UxPutLeftAttachment( separator2, "attach_none" );
	UxCreateWidget( separator2 );

	UxPutBottomWidget( separator3, "separator1" );
	UxPutBottomOffset( separator3, 0 );
	UxPutBottomAttachment( separator3, "attach_opposite_widget" );
	UxPutTopWidget( separator3, "rowColumn2" );
	UxPutTopOffset( separator3, 15 );
	UxPutTopAttachment( separator3, "attach_form" );
	UxPutRightWidget( separator3, "rowColumn2" );
	UxPutRightOffset( separator3, 0 );
	UxPutRightAttachment( separator3, "attach_none" );
	UxPutLeftWidget( separator3, "rowColumn2" );
	UxPutLeftOffset( separator3, 0 );
	UxPutLeftAttachment( separator3, "attach_widget" );
	UxCreateWidget( separator3 );

	UxPutLeftWidget( separator4, "rowColumn1" );
	UxPutLeftOffset( separator4, 0 );
	UxPutLeftAttachment( separator4, "attach_opposite_widget" );
	UxPutRightWidget( separator4, "separator2" );
	UxPutRightOffset( separator4, 0 );
	UxPutRightAttachment( separator4, "attach_widget" );
	UxPutTopWidget( separator4, "rowColumn1" );
	UxPutTopOffset( separator4, 0 );
	UxPutTopAttachment( separator4, "attach_widget" );
	UxCreateWidget( separator4 );

	UxPutTopOffset( separator5, 15 );
	UxPutTopAttachment( separator5, "attach_form" );
	UxPutRightWidget( separator5, "rowColumn1" );
	UxPutRightOffset( separator5, 0 );
	UxPutRightAttachment( separator5, "attach_widget" );
	UxPutBottomWidget( separator5, "separator1" );
	UxPutBottomOffset( separator5, 0 );
	UxPutBottomAttachment( separator5, "attach_widget" );
	UxCreateWidget( separator5 );

	UxPutLeftOffset( separator6, 10 );
	UxPutTopOffset( separator6, 15 );
	UxPutTopAttachment( separator6, "attach_form" );
	UxPutRightOffset( separator6, 20 );
	UxPutRightAttachment( separator6, "attach_form" );
	UxPutLeftAttachment( separator6, "attach_form" );
	UxCreateWidget( separator6 );


	UxAddCallback( transientShell1, XmNpopupCallback,
			popupCB_transientShell1,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( workArea1, XmNfocusCallback,
			focusCB_workArea1,
			(XtPointer) UxTransientShell1Context );
	UxAddCallback( workArea1, XmNmapCallback,
			mapCB_workArea1,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( SHelp1, XmNactivateCallback,
			activateCB_SHelp1,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( NewsButton1, XmNactivateCallback,
			activateCB_NewsButton1,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( PrintButton1, XmNactivateCallback,
			activateCB_PrintButton1,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( ReportButton1, XmNactivateCallback,
			activateCB_ReportButton1,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( SendButton1, XmNactivateCallback,
			activateCB_SendButton1,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( menu1_p4, XmNentryCallback,
			entryCB_menu1_p4,
			(XtPointer) UxTransientShell1Context );

	UxPutMenuHistory( menu4, "menu1_p1_b14" );
	UxAddCallback( menu4, XmNentryCallback,
			entryCB_menu4,
			(XtPointer) UxTransientShell1Context );

	UxAddCallback( menu1_p5, XmNentryCallback,
			entryCB_menu1_p5,
			(XtPointer) UxTransientShell1Context );

	UxPutMenuHistory( menu5, "menu1_p1_b16" );
	UxAddCallback( menu5, XmNentryCallback,
			entryCB_menu5,
			(XtPointer) UxTransientShell1Context );

	UxPutMenuHistory( rowColumn2, "" );

	UxAddCallback( text3, XmNfocusCallback,
			focusCB_text3,
			(XtPointer) UxTransientShell1Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( transientShell1 );

	return ( transientShell1 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_transientShell1()
{
	swidget                 rtrn;
	_UxCtransientShell1     *UxContext;

	UxTransientShell1Context = UxContext =
		(_UxCtransientShell1 *) UxMalloc( sizeof(_UxCtransientShell1) );

	rtrn = _Uxbuild_transientShell1();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_transientShell1()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "HInit", action_HInit },
				{ "WriteHelp", action_WriteHelp }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_transientShell1();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

