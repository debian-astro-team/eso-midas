/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
.VERSION
 090818		last modif
*/

/*******************************************************************************
	transientShell4.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxtransientShell4;
	swidget	UxworkArea4;
	swidget	UxSHelp4;
	swidget	UxNewsButton4;
	swidget	UxPrintButton4;
	swidget	Uxlabel5;
	swidget	UxtextField4;
} _UxCtransientShell4;

#define transientShell4         UxTransientShell4Context->UxtransientShell4
#define workArea4               UxTransientShell4Context->UxworkArea4
#define SHelp4                  UxTransientShell4Context->UxSHelp4
#define NewsButton4             UxTransientShell4Context->UxNewsButton4
#define PrintButton4            UxTransientShell4Context->UxPrintButton4
#define label5                  UxTransientShell4Context->Uxlabel5
#define textField4              UxTransientShell4Context->UxtextField4

static _UxCtransientShell4	*UxTransientShell4Context;

extern void helpPR(), InitHelp(), WriteHelpAW(), makeReport(), SensMenu();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_transientShell4();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell4     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell4Context;
	UxTransientShell4Context = UxContext =
			(_UxCtransientShell4 *) UxGetContext( UxThisWidget );
	{
	WriteHelpAW(UxWidget);
	helpPR(UxWidget);
	
	}
	UxTransientShell4Context = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	mapCB_workArea4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell4     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell4Context;
	UxTransientShell4Context = UxContext =
			(_UxCtransientShell4 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell4Context = UxSaveCtx;
}

static void	createCB_workArea4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell4     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell4Context;
	UxTransientShell4Context = UxContext =
			(_UxCtransientShell4 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell4Context = UxSaveCtx;
}

static void	activateCB_SHelp4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell4     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell4Context;
	UxTransientShell4Context = UxContext =
			(_UxCtransientShell4 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell4Context = UxSaveCtx;
}

static void	activateCB_NewsButton4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell4     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell4Context;
	UxTransientShell4Context = UxContext =
			(_UxCtransientShell4 *) UxGetContext( UxThisWidget );
	{
	 extern swidget save;
	 char addr[100];
	
	
	 strcpy(addr,UxGetText(textField4));
	
	 makeReport(addr);
	
	 UxPopdownInterface(save);
	
	}
	UxTransientShell4Context = UxSaveCtx;
}

static void	activateCB_PrintButton4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell4     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell4Context;
	UxTransientShell4Context = UxContext =
			(_UxCtransientShell4 *) UxGetContext( UxThisWidget );
	{
	extern swidget save;
	
	UxPopdownInterface(save);
	
	}
	UxTransientShell4Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_transientShell4()
{
	UxPutTitle( transientShell4, "Save Report" );
	UxPutHeight( transientShell4, 140 );
	UxPutWidth( transientShell4, 300 );
	UxPutY( transientShell4, 600 );
	UxPutX( transientShell4, 630 );

	UxPutResizePolicy( workArea4, "resize_none" );
	UxPutNoResize( workArea4, "true" );
	UxPutUnitType( workArea4, "pixels" );
	UxPutBackground( workArea4, WindowBackground );
	UxPutBorderWidth( workArea4, 0 );
	UxPutHeight( workArea4, 498 );
	UxPutWidth( workArea4, 600 );
	UxPutY( workArea4, 2 );
	UxPutX( workArea4, 20 );

	UxPutText( SHelp4, "Enter file name" );
	UxPutEditable( SHelp4, "false" );
	UxPutAccelerators( SHelp4, "" );
	UxPutWordWrap( SHelp4, "false" );
	UxPutHighlightOnEnter( SHelp4, "true" );
	UxPutEditMode( SHelp4, "single_line_edit" );
	UxPutBlinkRate( SHelp4, 500 );
	UxPutPendingDelete( SHelp4, "true" );
	UxPutTranslations( SHelp4, "" );
	UxPutCursorPositionVisible( SHelp4, "true" );
	UxPutFontList( SHelp4, TextFont );
	UxPutHighlightColor( SHelp4, ApplicBackground );
	UxPutForeground( SHelp4, TextForeground );
	UxPutBackground( SHelp4, SHelpBackground );
	UxPutHeight( SHelp4, 40 );
	UxPutWidth( SHelp4, 325 );
	UxPutY( SHelp4, 380 );
	UxPutX( SHelp4, 30 );

	UxPutHighlightColor( NewsButton4, "Black" );
	UxPutRecomputeSize( NewsButton4, "false" );
	UxPutFontList( NewsButton4, BoldTextFont );
	UxPutForeground( NewsButton4, ButtonForeground );
	UxPutLabelString( NewsButton4, "OK" );
	UxPutBackground( NewsButton4, ButtonBackground );
	UxPutHeight( NewsButton4, 30 );
	UxPutWidth( NewsButton4, 95 );
	UxPutY( NewsButton4, 630 );
	UxPutX( NewsButton4, 20 );

	UxPutHighlightColor( PrintButton4, "Black" );
	UxPutRecomputeSize( PrintButton4, "false" );
	UxPutFontList( PrintButton4, BoldTextFont );
	UxPutForeground( PrintButton4, ButtonForeground );
	UxPutLabelString( PrintButton4, "Cancel" );
	UxPutBackground( PrintButton4, ButtonBackground );
	UxPutHeight( PrintButton4, 30 );
	UxPutWidth( PrintButton4, 95 );
	UxPutY( PrintButton4, 330 );
	UxPutX( PrintButton4, 280 );

	UxPutLabelString( label5, "File Name" );
	UxPutForeground( label5, TextForeground );
	UxPutFontList( label5, BoldTextFont );
	UxPutBackground( label5, LabelBackground );
	UxPutHeight( label5, 20 );
	UxPutWidth( label5, 80 );
	UxPutY( label5, 10 );
	UxPutX( label5, 10 );

	UxPutText( textField4, "midas.report" );
	UxPutForeground( textField4, TextForeground );
	UxPutFontList( textField4, TextFont );
	UxPutBackground( textField4, TextBackground );
	UxPutHeight( textField4, 35 );
	UxPutWidth( textField4, 250 );
	UxPutY( textField4, 60 );
	UxPutX( textField4, 140 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_transientShell4()
{
	/* Create the swidgets */

	transientShell4 = UxCreateTransientShell( "transientShell4", NO_PARENT );
	UxPutContext( transientShell4, UxTransientShell4Context );

	workArea4 = UxCreateForm( "workArea4", transientShell4 );
	SHelp4 = UxCreateText( "SHelp4", workArea4 );
	NewsButton4 = UxCreatePushButton( "NewsButton4", workArea4 );
	PrintButton4 = UxCreatePushButton( "PrintButton4", workArea4 );
	label5 = UxCreateLabel( "label5", workArea4 );
	textField4 = UxCreateText( "textField4", workArea4 );

	_Uxinit_transientShell4();

	/* Create the X widgets */

	UxCreateWidget( transientShell4 );
	UxCreateWidget( workArea4 );
	createCB_workArea4( UxGetWidget( workArea4 ),
			(XtPointer) UxTransientShell4Context, (XtPointer) NULL );

	UxPutBottomOffset( SHelp4, 50 );
	UxPutBottomAttachment( SHelp4, "attach_form" );
	UxPutRightOffset( SHelp4, 5 );
	UxPutRightAttachment( SHelp4, "attach_form" );
	UxPutLeftOffset( SHelp4, 5 );
	UxPutLeftAttachment( SHelp4, "attach_form" );
	UxCreateWidget( SHelp4 );

	UxPutLeftOffset( NewsButton4, 50 );
	UxPutLeftAttachment( NewsButton4, "attach_form" );
	UxPutTopWidget( NewsButton4, "SHelp4" );
	UxPutTopOffset( NewsButton4, 10 );
	UxPutTopAttachment( NewsButton4, "attach_widget" );
	UxCreateWidget( NewsButton4 );

	UxPutLeftWidget( PrintButton4, "NewsButton4" );
	UxPutLeftOffset( PrintButton4, 40 );
	UxPutLeftAttachment( PrintButton4, "attach_widget" );
	UxPutTopWidget( PrintButton4, "SHelp4" );
	UxPutTopOffset( PrintButton4, 10 );
	UxPutTopAttachment( PrintButton4, "attach_widget" );
	UxCreateWidget( PrintButton4 );

	UxPutTopOffset( label5, 15 );
	UxPutTopAttachment( label5, "attach_form" );
	UxPutLeftOffset( label5, 5 );
	UxPutLeftAttachment( label5, "attach_form" );
	UxCreateWidget( label5 );

	UxPutTopOffset( textField4, 10 );
	UxPutTopAttachment( textField4, "attach_form" );
	UxPutRightOffset( textField4, 5 );
	UxPutRightAttachment( textField4, "attach_form" );
	UxPutLeftOffset( textField4, 90 );
	UxPutLeftAttachment( textField4, "attach_form" );
	UxCreateWidget( textField4 );


	UxAddCallback( workArea4, XmNmapCallback,
			mapCB_workArea4,
			(XtPointer) UxTransientShell4Context );

	UxAddCallback( SHelp4, XmNactivateCallback,
			activateCB_SHelp4,
			(XtPointer) UxTransientShell4Context );

	UxAddCallback( NewsButton4, XmNactivateCallback,
			activateCB_NewsButton4,
			(XtPointer) UxTransientShell4Context );

	UxAddCallback( PrintButton4, XmNactivateCallback,
			activateCB_PrintButton4,
			(XtPointer) UxTransientShell4Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( transientShell4 );

	return ( transientShell4 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_transientShell4()
{
	swidget                 rtrn;
	_UxCtransientShell4     *UxContext;

	UxTransientShell4Context = UxContext =
		(_UxCtransientShell4 *) UxMalloc( sizeof(_UxCtransientShell4) );

	rtrn = _Uxbuild_transientShell4();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_transientShell4()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "WriteHelp", action_WriteHelp }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_transientShell4();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

