/*===========================================================================
  Copyright (C) 1995-2010 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	ApplicWindow.c

	Associated Resource file: ApplicWindow.rf

.VERSION
 100921		last modif

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <str.h>

#include "UxLib.h"
#include "UxArrB.h"
#include "UxTextF.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxScText.h"
#include "UxScrW.h"
#include "UxFrame.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <Xm/Protocols.h>
#include <UxFsBox.h>
#include <Xm/Text.h>

/* Definitions for History mechanism */

#define SIZHIST 50
#define SIZTEXT 40
int  ItemNumber = 0, ItemPosition = 0;
int  DispItem = 0; 
int  Overview[2] = {1,0};

char ItemHistory[SIZHIST][SIZTEXT];
char HistList[SIZHIST*SIZTEXT];

/* Variables for SelectionArray */

XmTextScanType Sarray[60];
int            SarI = 0;



/* Include for UxPut calls on the fileSelectionBox */

void CreateWindowManagerProtocols();
void ExitCB();
void CreateSessionManagerProtocols();
void SaveSessionCB();

extern Boolean saving;  /* declared in file selection box declarations */
extern swidget fileSelectionDialog;
extern swidget fileSelectionBox;
extern swidget exitDialog;
extern swidget create_fileSelectionDialog();
extern swidget create_exitDialog();

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxmainWindow;
	swidget	UxpullDownMenu;
	swidget	UxUtilsPane;
	swidget	UxSysEntry;
	swidget	UxUtilsPane_b2;
	swidget	UxcommandsCascade;
	swidget	UxworkAreaForm;
	swidget	UxworkAreaFrame;
	swidget	UxworkArea;
	swidget	UxscrolledWindow;
	swidget	UxscrolledText;
	swidget	Uxmenu1_p1;
	swidget	Uxmenu1_p1_b1;
	swidget	Uxmenu1_p1_b2;
	swidget	Uxmenu1_p1_b3;
	swidget	Uxmenu1_p1_b4;
	swidget	Uxmenu1_p1_b6;
	swidget	Uxmenu1_p1_b7;
	swidget	Uxmenu1;
	swidget	Uxtext1;
	swidget	UxSHelp;
	swidget	UxCommandButton;
	swidget	UxHistoButton;
	swidget	UxSearchButton;
	swidget	UxNewsButton;
	swidget	UxPrintButton;
	swidget	UxReportButton;
	swidget	UxCtxButton;
	swidget	UxtextField5;
	swidget	Uxtext2;
	swidget	UxarrowButton2;
	swidget	UxarrowButton1;
} _UxCApplicWindow;

#define mainWindow              UxApplicWindowContext->UxmainWindow
#define pullDownMenu            UxApplicWindowContext->UxpullDownMenu
#define UtilsPane               UxApplicWindowContext->UxUtilsPane
#define SysEntry                UxApplicWindowContext->UxSysEntry
#define UtilsPane_b2            UxApplicWindowContext->UxUtilsPane_b2
#define commandsCascade         UxApplicWindowContext->UxcommandsCascade
#define workAreaForm            UxApplicWindowContext->UxworkAreaForm
#define workAreaFrame           UxApplicWindowContext->UxworkAreaFrame
#define workArea                UxApplicWindowContext->UxworkArea
#define scrolledWindow          UxApplicWindowContext->UxscrolledWindow
#define scrolledText            UxApplicWindowContext->UxscrolledText
#define menu1_p1                UxApplicWindowContext->Uxmenu1_p1
#define menu1_p1_b1             UxApplicWindowContext->Uxmenu1_p1_b1
#define menu1_p1_b2             UxApplicWindowContext->Uxmenu1_p1_b2
#define menu1_p1_b3             UxApplicWindowContext->Uxmenu1_p1_b3
#define menu1_p1_b4             UxApplicWindowContext->Uxmenu1_p1_b4
#define menu1_p1_b6             UxApplicWindowContext->Uxmenu1_p1_b6
#define menu1_p1_b7             UxApplicWindowContext->Uxmenu1_p1_b7
#define menu1                   UxApplicWindowContext->Uxmenu1
#define text1                   UxApplicWindowContext->Uxtext1
#define SHelp                   UxApplicWindowContext->UxSHelp
#define CommandButton           UxApplicWindowContext->UxCommandButton
#define HistoButton             UxApplicWindowContext->UxHistoButton
#define SearchButton            UxApplicWindowContext->UxSearchButton
#define NewsButton              UxApplicWindowContext->UxNewsButton
#define PrintButton             UxApplicWindowContext->UxPrintButton
#define ReportButton            UxApplicWindowContext->UxReportButton
#define CtxButton               UxApplicWindowContext->UxCtxButton
#define textField5              UxApplicWindowContext->UxtextField5
#define text2                   UxApplicWindowContext->Uxtext2
#define arrowButton2            UxApplicWindowContext->UxarrowButton2
#define arrowButton1            UxApplicWindowContext->UxarrowButton1

static _UxCApplicWindow	*UxApplicWindowContext;

swidget	ApplicWindow;

void StoreOverviewPosition(), SetOverviewPosition();

extern int helpme();
extern int close_monit_connection(), convert();
extern void helpPR();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*HelpTable = "#replace\n\
<Btn1Up>:HelpACT()\n\
<Btn1Down>:grab-focus()\n\
<EnterWindow>:WriteHelp()\n";

static char	*TextTable = "#override\n\
<Btn1Up>:SelectCommand()\n";

static char	*transTable10 = "#override\n\
<EnterWindow>:WriteHelp() Enter()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	popup_ApplicWindow();

/*******************************************************************************
	Auxiliary code from the Declarations Editor:
*******************************************************************************/

void GetHelp(s,type,inc)

char *s,*type;
long int inc;

{

char  *s2;
char  MenuItem[40], command[100], item[100];
extern char contxt[], midvers[];
XmString GetList();
void     DispList(); 

int   found, stat, pos, i=0;

/* Store Overview Panel position */
if (s[0] != '?'&& Overview[0] == 1) StoreOverviewPosition(0);

if (type[0] == '?')
  strcpy(MenuItem,
     UxGetLabelString(UxFindSwidget(UxGetMenuHistory(menu1))) );
else
  strcpy(MenuItem,type);

  if (MenuItem[0] == 'C' && MenuItem[2] == 'm' )  strcpy(command,"");
  if (MenuItem[0] == 'A')  strcpy(command,"/APPLIC ");
  if (MenuItem[0] == 'P')  strcpy(command,"/CL ");
  if (MenuItem[0] == 'K')  strcpy(command,"/KEYWORD ");
  if (MenuItem[0] == 'S')  strcpy(command,"/SUBJECT ");
  if (MenuItem[0] == 'C' && MenuItem[2] == 'n' )  strcpy(command,"/CONTRIB ");

  if (MenuItem[0] == ' ') strcpy(command,"");

/* 
   Filter out unallowed characters in string s 
   Allowed characters are a-z, A-Z, and _ @ $ / [] ? * and Space
*/


for (pos=0; pos<(int)strlen(s); pos++) {
   if (s[pos] >= 'a' && s[pos] <= 'z')  item[i++] = s[pos];
   if (s[pos] >= 'A' && s[pos] <= 'Z')  item[i++] = s[pos];
   if (s[pos] == '$' || s[pos] == '_' || s[pos] == '/')
         item[i++] = s[pos];
   if (s[pos] == '[' || s[pos] == ']') item[i++] = s[pos];
   if (s[pos] == '?' || s[pos] == '*') item[i++] = s[pos];
   if (s[pos] == ' ') item[i++] = s[pos];
   if (s[pos] == '@') item[i++] = s[pos];
   if (s[pos] >= '0' && s[pos] <= '9') item[i++] = s[pos];
}   

item[i] = '\0';
strcat (command,item); 


   stat = helpme(2, midvers, command, contxt, &s2);

   if (stat == 99)
      {
      found = 1;
      stat = 0;
      }
   else if (stat == -99)
      {
      found = 0;
      stat = 0;
      }


   if (stat == 0) {

    /* Display Help message */
    XmTextSetString(UxGetWidget(scrolledText),s2); 
    XmTextSetString(UxGetWidget(SHelp),"");

    /* Positions Overview Panel */
    if (s[0] == '?') SetOverviewPosition(1);

    /* Clears all highlights */
    XmTextSetHighlight(UxGetWidget(scrolledText), 0, 
           strlen(s2), XmHIGHLIGHT_NORMAL);

    /* Update History List */
     if (inc != 0) {
      if (item[0] != '?') 
          strcpy(ItemHistory[ItemPosition++],command);
      if (ItemPosition == SIZHIST) ItemPosition = 0;
      ItemNumber++;
      DispItem = ItemNumber - 2;
      UxPutText(text2,GetList());
      DispItem = ItemNumber - 1; 

      /* Builds History List and Display it */
      {
      int LastItem, FirstItem=0, Item, ListPosition=0;
      char s[SIZTEXT*SIZHIST];

      LastItem = ItemPosition - 1;
      if (LastItem < 0) LastItem += SIZHIST;
      if (ItemNumber >= SIZHIST) FirstItem = LastItem + 1; 

      strcpy(s,"");

      for (Item = LastItem; Item>=0; Item--) {
        strcat(s,ItemHistory[Item]);
        strcat(&s[ListPosition],"\n");
        } 
  
      if (ItemNumber >= SIZHIST) {
      for (Item = SIZHIST-1; Item >= FirstItem; Item--) {
          strcat(s,ItemHistory[Item]);
          strcat(&s[ListPosition],"\n");
          } 
       }      

       DispList(s);
       }
 
     }
   }
   else
     XmTextSetString(UxGetWidget(SHelp),s2); 

   if (found == 1)
      {
      UxPutMenuHistory(menu1,"menu1_p1_b1");
      }

/*{ End of if (s != 0) { */

}


void InitHelp()

{

 GetHelp("?","?",0L);
 XmTextSetString(UxGetWidget(text1),"");

}

void SearchHelp(s,dir)

char *s;
long int dir;

{

/* SearchHelp should strip leading and trailing blanks */

char *HText;
Widget SText;

XmTextPosition PosText = 0;

int LText;
int relpos; /* Relative position */

HText = UxGetText(scrolledText);
SText = UxGetWidget(scrolledText);

/* Initialisations */

LText    = strlen(HText);
XmTextSetHighlight(SText, 0, LText, XmHIGHLIGHT_NORMAL);

/* Set all characters to upper case for case insensitivity */

for (PosText=0; PosText<LText; PosText++) 
          HText[PosText] = toupper(HText[PosText]); 

for (PosText=0; PosText<(int)strlen(s); PosText++) 
          s[PosText] = toupper(s[PosText]); 

PosText = 0;

/* If Show, shows all occurences */

if (dir > 0) {

   relpos  = strindex(HText,s);

   if (relpos == LText) 
       XmTextSetString(UxGetWidget(SHelp),"Pattern not found");
   else
       XmTextShowPosition(SText,(XmTextPosition) relpos);  
 
   while (relpos != LText) {

     PosText += relpos;

     XmTextSetHighlight(SText,PosText,
             PosText+strlen(s),XmHIGHLIGHT_SELECTED);

     PosText++;

     LText    = strlen(HText+PosText);
     relpos   = strindex(HText+PosText,s);

    }
  }
}

XmString GetList()

{


int n;
int quotient, module, minitem;

n = DispItem;

if (n >= ItemNumber)       n = ItemNumber-1;

if (ItemNumber < SIZHIST)  minitem = 0;
else                       minitem = ItemNumber - SIZHIST;

if (n <  minitem)  n = minitem;

DispItem = n;

quotient = (int) n/SIZHIST;
module = n - quotient*SIZHIST;

return( (XmString) (ItemHistory+module) );

}

void SelHelp(s)

char *s;

{

if (s[0] == '/') {

   if (toupper(s[1]) == 'A') 
         UxPutMenuHistory(menu1,"menu1_p1_b2"); /* APPLIC */


   if (toupper(s[1]) == 'C') 
         UxPutMenuHistory(menu1,"menu1_p1_b3");  /*  CL */

   if (toupper(s[1]) == 'K') 
         UxPutMenuHistory(menu1,"menu1_p1_b4");  /*  KEYWORD */

   if (toupper(s[1]) == 'S') 
         UxPutMenuHistory(menu1,"menu1_p1_b6");  /*  SUBJECT */

   GetHelp(s," ",0L);
   }
else  {
   UxPutMenuHistory(menu1,"menu1_p1_b1");
   GetHelp(s,"?",0L);
   }
}

void PutSelection(sw, mode)

swidget sw;
int     mode;

{
        XmTextScanType *array;
        Arg            args[2];
        int            n = 0, i;
        Widget         wid;

        wid = UxGetWidget(sw);

        XtSetArg(args[n], XmNselectionArray,  &array), n++;
        XtGetValues(wid , args, n);
 
        /* printf("SarI, Widget ID: %i , %i \n",SarI,wid); */

          for (i=0; i<=3; i++) 
            Sarray[SarI+i] = array[i];

/*      for (i=0; i<=3; i++) {
         if (array[i] == XmSELECT_POSITION) printf("%d : Position\n",i);
         if (array[i] == XmSELECT_WORD)     printf("%d : Word\n",i);
         if (array[i] == XmSELECT_LINE)     printf("%d : Line\n",i);
         if (array[i] == XmSELECT_ALL)      printf("%d : All\n",i);
       }
*/

        for (i=0; i<=3; i++)
           Sarray[SarI+i] = Sarray[SarI+mode];

        n = 0;
        XtSetArg(args[n], XmNselectionArray,  (Sarray+SarI)), n++;
        XtSetValues(wid , args, n);
        SarI += 4;
}

void SensMenu(mode)

int  mode;
{
if (mode == 0) XtSetSensitive(UxGetWidget(SysEntry),FALSE);
if (mode == 1) XtSetSensitive(UxGetWidget(SysEntry),TRUE);
}

void WriteHelpAW(UxWidget)

Widget UxWidget;
{
char s[100];
extern char print_com[];

strcpy(s,"");

if (UxWidget == UxGetWidget(CommandButton))
   strcpy(s,"Display an overview of the commands");

if (UxWidget == UxGetWidget(HistoButton))
   strcpy(s,"Popup the history list of previously queried commands");

if (UxWidget == UxGetWidget(SearchButton))
   strcpy(s,"Search strings in help file");

if (UxWidget == UxGetWidget(NewsButton))
   strcpy(s,"Display Release News");

if (UxWidget == UxGetWidget(PrintButton))
   sprintf(s,"Send help message to the printer. (Command: cat <file> | %s)\n",
           print_com);

if (UxWidget == UxGetWidget(ReportButton))
   strcpy(s,"Popup the problem report form");

if (UxWidget == UxGetWidget(CtxButton))
   strcpy(s,"Popup the context selection form");

if (UxWidget == UxGetWidget(text1))
   strcpy(s,"Commands names can be typed in this area. (Get help by Return)");

if (UxWidget == UxGetWidget(text2))
   strcpy(s,"Non editable area. Displays previous command. Click to select.");

if (UxWidget == UxGetWidget(scrolledWindow))
   strcpy(s,"Help files display area");

if (UxWidget == UxGetWidget(arrowButton2))
   strcpy(s,"Moves forward in the history list of commands");

if (UxWidget == UxGetWidget(arrowButton1))
   strcpy(s,"Moves backward in the history list of commands");

if (UxWidget == UxGetWidget(scrolledText))
   strcpy(s,"Point and Click in this area to access help files");


if (UxWidget == UxGetWidget(menu1))
   strcpy(s,"Information mode");

if (UxWidget == UxGetWidget(commandsCascade))
   strcpy(s,"Setup options and Exit");

XmTextSetString(UxGetWidget(SHelp),s);

}

void MapRaised(sw)

swidget sw;
{
Window xw;
xw = XtWindow(UxGetWidget(sw));
XMapRaised(UxDisplay, xw);
}

void StoreOverviewPosition(mode)
int mode;
{
/* Store Overview Panel position */
    Overview[0] = mode;
    Overview[1] = (int) XmTextGetTopCharacter(UxGetWidget(scrolledText));
}

void SetOverviewPosition(mode)
int mode;
{
/* Positions Overview Panel */
     Overview[0] = mode;
     XmTextSetTopCharacter(UxGetWidget(scrolledText),
                        (XmTextPosition) Overview[1]);
}

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_HInit( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	InitHelp();
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_SelectCommand( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	 char *s;
	
	 if ((s = XmTextGetSelection(UxWidget)) == NULL) return; 
	
	 SelHelp(s);
	
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	WriteHelpAW(UxWidget);
	helpPR(UxWidget);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_HelpACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 char *s;
	
	 if ((s = XmTextGetSelection(UxWidget)) == NULL) return; 
	
	 if (s[0] == '/') GetHelp (s," ",1L); 
	 else             GetHelp (s,"?",1L); 
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	popupCB_ApplicWindow( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_SysEntry( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget sys; 
	UxPopupInterface(sys,no_grab);
	MapRaised(sys);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_UtilsPane_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	close_monit_connection();
	exit(0);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	mapCB_workArea( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_workArea( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_scrolledText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	motionVerifyCB_scrolledText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_scrolledText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *s;
	extern char contxt[],midvers[];
        int  stat;
	
	stat = helpme (2, midvers, "?" , contxt, &s);

	XmTextSetString(UxWidget,s);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	entryCB_menu1_p1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	Overview[1] = 0;
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	InitHelp();
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	InitHelp();
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	InitHelp();
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	InitHelp();
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	InitHelp();
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{InitHelp();}
	UxApplicWindowContext = UxSaveCtx;
}

static void	entryCB_menu1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	printf("Hello\n");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *s;
	
	s = XmTextGetString(UxWidget);
	
	GetHelp(s,"?",1L);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_SHelp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char  *s1;
	char  *s2;
	extern char contxt[], midvers[];
        int stat;
	
	s1 = XmTextGetString(UxWidget);
	stat = helpme(2, midvers, s1, contxt, &s2); 

	XmTextSetString(UxGetWidget(scrolledText),s2);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_CommandButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	InitHelp();
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_HistoButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget hist;
	
	
	UxPopupInterface(hist,no_grab);
	MapRaised(hist);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_SearchButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget srch;
	
	UxPopupInterface(srch,no_grab);
	MapRaised(srch);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_NewsButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	GetHelp("[News]"," ",0L);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_PrintButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	 extern char filename[], print_com[];
	 char command[100];
	 FILE *fp;
	
	 fp = fopen(filename,"w"); 
	
	 fprintf(fp,"%s\n",XmTextGetString(UxGetWidget(scrolledText)));
	
	 fclose(fp);
	
	 sprintf(command,"cat %s | %s ; rm %s\n",
	       filename,print_com,filename);
	
	 system(command);
	
	 UxPutText(SHelp,"Help file sent to printer.");
	
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_ReportButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget pb;
	
	UxPopupInterface(pb,no_grab);
	MapRaised(pb);
	SensMenu(0);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_CtxButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	extern swidget ctx;
	extern char contxt[], nctx[];
	
	strred(contxt);
	strtrs(nctx,contxt," ","\n");
	convert(contxt,nctx);
	
	UxPopupInterface(ctx,no_grab);
	MapRaised(ctx);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_text2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *s;
	
	s = XmTextGetString(UxWidget);
	
	GetHelp(s,"?",1L);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_arrowButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char s[100];
	
	++DispItem;
	
	strcpy(s,(char *)GetList());
	
	UxPutText(text2,s);
	
	SelHelp(s);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_arrowButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char s[100];
	
	--DispItem;
	
	strcpy(s,(char *)GetList());
	
	UxPutText(text2,s);
	
	SelHelp(s);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ApplicWindow()
{
	UxPutGeometry( ApplicWindow, "700x480+30+30" );
	UxPutBackground( ApplicWindow, WindowBackground );
	UxPutIconName( ApplicWindow, "XHelp" );
	UxPutDeleteResponse( ApplicWindow, "do_nothing" );
	UxPutTitle( ApplicWindow, "XHelp" );
	UxPutHeight( ApplicWindow, 600 );
	UxPutWidth( ApplicWindow, 700 );
	UxPutY( ApplicWindow, 577 );
	UxPutX( ApplicWindow, 550 );

	UxPutBackground( mainWindow, WindowBackground );
	UxPutHeight( mainWindow, 470 );
	UxPutWidth( mainWindow, 700 );
	UxPutY( mainWindow, 180 );
	UxPutX( mainWindow, 200 );

	UxPutForeground( pullDownMenu, MenuForeground );
	UxPutBackground( pullDownMenu, MenuBackground );
	UxPutMenuAccelerator( pullDownMenu, "<KeyUp>F10" );
	UxPutRowColumnType( pullDownMenu, "menu_bar" );
	UxPutBorderWidth( pullDownMenu, 0 );

	UxPutForeground( UtilsPane, MenuForeground );
	UxPutBackground( UtilsPane, MenuBackground );
	UxPutRowColumnType( UtilsPane, "menu_pulldown" );

/* 
on some Motif installations the MenuForeground color came out black... 070521 
*/
	UxPutForeground( SysEntry, "white" );
	UxPutFontList( SysEntry, BoldTextFont );
	UxPutMnemonic( SysEntry, "S" );
	UxPutLabelString( SysEntry, "Print & Mail..." );

	UxPutForeground( UtilsPane_b2, "white" );
	UxPutMnemonic( UtilsPane_b2, "E" );
	UxPutFontList( UtilsPane_b2, BoldTextFont );
	UxPutLabelString( UtilsPane_b2, "Exit" );

	UxPutTranslations( commandsCascade, transTable10 );
	UxPutX( commandsCascade, 101 );
	UxPutMnemonic( commandsCascade, "F" );
	UxPutForeground( commandsCascade, MenuForeground );
	UxPutFontList( commandsCascade, BoldTextFont );
	UxPutBackground( commandsCascade, MenuBackground );
	UxPutLabelString( commandsCascade, "File" );

	UxPutBackground( workAreaForm, WindowBackground );
	UxPutBorderWidth( workAreaForm, 0 );
	UxPutHeight( workAreaForm, 440 );
	UxPutWidth( workAreaForm, 490 );

	UxPutBackground( workAreaFrame, WindowBackground );
	UxPutHeight( workAreaFrame, 230 );
	UxPutWidth( workAreaFrame, 390 );
	UxPutY( workAreaFrame, 30 );
	UxPutX( workAreaFrame, 50 );

	UxPutMarginWidth( workArea, 5 );
	UxPutMarginHeight( workArea, 5 );
	UxPutBorderColor( workArea, WindowBackground );
	UxPutBackground( workArea, WindowBackground );
	UxPutBorderWidth( workArea, 0 );
	UxPutHeight( workArea, 498 );
	UxPutWidth( workArea, 408 );
	UxPutY( workArea, 2 );
	UxPutX( workArea, 20 );

	UxPutBackground( scrolledWindow, ApplicBackground );
	UxPutWidth( scrolledWindow, 490 );
	UxPutHeight( scrolledWindow, 270 );
	UxPutShadowThickness( scrolledWindow, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow, "static" );
	UxPutVisualPolicy( scrolledWindow, "variable" );
	UxPutY( scrolledWindow, 20 );
	UxPutX( scrolledWindow, 8 );
	UxPutScrollingPolicy( scrolledWindow, "application_defined" );

	UxPutText( scrolledText, "This is my help, \nhelp, \nhelp...\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n End of Text" );
	UxPutTranslations( scrolledText, HelpTable );
	UxPutFontList( scrolledText, SmallFont );
	UxPutHighlightColor( scrolledText, "black" );
	UxPutForeground( scrolledText, TextForeground );
	UxPutEditMode( scrolledText, "multi_line_edit" );
	UxPutScrollVertical( scrolledText, "true" );
	UxPutScrollHorizontal( scrolledText, "true" );
	UxPutEditable( scrolledText, "false" );
	UxPutY( scrolledText, 0 );
	UxPutX( scrolledText, 0 );
	UxPutBackground( scrolledText, "WhiteSmoke" );
	UxPutHeight( scrolledText, 220 );
	UxPutWidth( scrolledText, 670 );

	UxPutTranslations( menu1_p1, "" );
	UxPutTopShadowColor( menu1_p1, "White" );
	UxPutForeground( menu1_p1, TextForeground );
	UxPutBackground( menu1_p1, TextBackground );
	UxPutRowColumnType( menu1_p1, "menu_pulldown" );

	UxPutMnemonic( menu1_p1_b1, "C" );
	UxPutFontList( menu1_p1_b1, TextFont );
	UxPutLabelString( menu1_p1_b1, "Commands" );

	UxPutMnemonic( menu1_p1_b2, "A" );
	UxPutFontList( menu1_p1_b2, TextFont );
	UxPutLabelString( menu1_p1_b2, "Applications" );

	UxPutMnemonic( menu1_p1_b3, "P" );
	UxPutFontList( menu1_p1_b3, TextFont );
	UxPutLabelString( menu1_p1_b3, "Procedure Lang." );

	UxPutMnemonic( menu1_p1_b4, "K" );
	UxPutFontList( menu1_p1_b4, TextFont );
	UxPutLabelString( menu1_p1_b4, "Keywords" );

	UxPutMnemonic( menu1_p1_b6, "S" );
	UxPutFontList( menu1_p1_b6, TextFont );
	UxPutLabelString( menu1_p1_b6, "Subjects" );

	UxPutFontList( menu1_p1_b7, TextFont );
	UxPutMnemonic( menu1_p1_b7, "t" );
	UxPutLabelString( menu1_p1_b7, "Contributions" );

	UxPutLabelString( menu1, "" );
	UxPutHeight( menu1, 10 );
	UxPutTranslations( menu1, transTable10 );
	UxPutWidth( menu1, 200 );
	UxPutForeground( menu1, TextForeground );
	UxPutBackground( menu1, ApplicBackground );
	UxPutRowColumnType( menu1, "menu_option" );

	UxPutAccelerators( text1, "" );
	UxPutWordWrap( text1, "false" );
	UxPutHighlightOnEnter( text1, "true" );
	UxPutEditMode( text1, "single_line_edit" );
	UxPutBlinkRate( text1, 500 );
	UxPutPendingDelete( text1, "true" );
	UxPutTranslations( text1, "" );
	UxPutCursorPositionVisible( text1, "true" );
	UxPutFontList( text1, BoldTextFont );
	UxPutHighlightColor( text1, "Black" );
	UxPutForeground( text1, TextForeground );
	UxPutBackground( text1, TextBackground );
	UxPutHeight( text1, 40 );
	UxPutWidth( text1, 325 );
	UxPutY( text1, 320 );
	UxPutX( text1, 20 );

	UxPutAccelerators( SHelp, "" );
	UxPutWordWrap( SHelp, "false" );
	UxPutHighlightOnEnter( SHelp, "true" );
	UxPutEditMode( SHelp, "single_line_edit" );
	UxPutBlinkRate( SHelp, 500 );
	UxPutPendingDelete( SHelp, "true" );
	UxPutTranslations( SHelp, "" );
	UxPutCursorPositionVisible( SHelp, "true" );
	UxPutFontList( SHelp, TextFont );
	UxPutHighlightColor( SHelp, ApplicBackground );
	UxPutForeground( SHelp, TextForeground );
	UxPutBackground( SHelp, SHelpBackground );
	UxPutHeight( SHelp, 40 );
	UxPutWidth( SHelp, 325 );
	UxPutY( SHelp, 380 );
	UxPutX( SHelp, 30 );

	UxPutMnemonic( CommandButton, "" );
	UxPutHighlightColor( CommandButton, "Black" );
	UxPutRecomputeSize( CommandButton, "false" );
	UxPutFontList( CommandButton, BoldTextFont );
	UxPutForeground( CommandButton, ButtonForeground );
	UxPutLabelString( CommandButton, "Overview" );
	UxPutBackground( CommandButton, ButtonBackground );
	UxPutHeight( CommandButton, 30 );
	UxPutWidth( CommandButton, 90 );
	UxPutY( CommandButton, 380 );
	UxPutX( CommandButton, 20 );

	UxPutMnemonic( HistoButton, "" );
	UxPutHighlightColor( HistoButton, "Black" );
	UxPutRecomputeSize( HistoButton, "false" );
	UxPutForeground( HistoButton, ButtonForeground );
	UxPutFontList( HistoButton, BoldTextFont );
	UxPutLabelString( HistoButton, "History.." );
	UxPutBackground( HistoButton, ButtonBackground );
	UxPutHeight( HistoButton, 30 );
	UxPutWidth( HistoButton, 90 );
	UxPutY( HistoButton, 380 );
	UxPutX( HistoButton, 110 );

	UxPutMnemonic( SearchButton, "" );
	UxPutHighlightColor( SearchButton, "Black" );
	UxPutRecomputeSize( SearchButton, "false" );
	UxPutForeground( SearchButton, ButtonForeground );
	UxPutFontList( SearchButton, BoldTextFont );
	UxPutLabelString( SearchButton, "Search.." );
	UxPutBackground( SearchButton, ButtonBackground );
	UxPutHeight( SearchButton, 30 );
	UxPutWidth( SearchButton, 90 );
	UxPutY( SearchButton, 380 );
	UxPutX( SearchButton, 110 );

	UxPutMnemonic( NewsButton, "" );
	UxPutHighlightColor( NewsButton, "Black" );
	UxPutRecomputeSize( NewsButton, "false" );
	UxPutFontList( NewsButton, BoldTextFont );
	UxPutForeground( NewsButton, ButtonForeground );
	UxPutLabelString( NewsButton, "News" );
	UxPutBackground( NewsButton, ButtonBackground );
	UxPutHeight( NewsButton, 30 );
	UxPutWidth( NewsButton, 90 );
	UxPutY( NewsButton, 330 );
	UxPutX( NewsButton, 180 );

	UxPutMnemonic( PrintButton, "" );
	UxPutHighlightColor( PrintButton, "Black" );
	UxPutRecomputeSize( PrintButton, "false" );
	UxPutFontList( PrintButton, BoldTextFont );
	UxPutForeground( PrintButton, ButtonForeground );
	UxPutLabelString( PrintButton, "Print" );
	UxPutBackground( PrintButton, ButtonBackground );
	UxPutHeight( PrintButton, 30 );
	UxPutWidth( PrintButton, 90 );
	UxPutY( PrintButton, 330 );
	UxPutX( PrintButton, 280 );

	UxPutMnemonic( ReportButton, "" );
	UxPutHighlightColor( ReportButton, "Black" );
	UxPutRecomputeSize( ReportButton, "false" );
	UxPutFontList( ReportButton, BoldTextFont );
	UxPutForeground( ReportButton, ButtonForeground );
	UxPutLabelString( ReportButton, "Feedback.." );
	UxPutBackground( ReportButton, ButtonBackground );
	UxPutHeight( ReportButton, 30 );
	UxPutWidth( ReportButton, 90 );
	UxPutY( ReportButton, 330 );
	UxPutX( ReportButton, 430 );

	UxPutMnemonic( CtxButton, "" );
	UxPutHighlightColor( CtxButton, "Black" );
	UxPutRecomputeSize( CtxButton, "false" );
	UxPutFontList( CtxButton, BoldTextFont );
	UxPutForeground( CtxButton, ButtonForeground );
	UxPutLabelString( CtxButton, "Contexts.." );
	UxPutBackground( CtxButton, ButtonBackground );
	UxPutHeight( CtxButton, 30 );
	UxPutWidth( CtxButton, 90 );
	UxPutY( CtxButton, 330 );
	UxPutX( CtxButton, 430 );

	UxPutShadowThickness( textField5, 0 );
	UxPutMarginWidth( textField5, 5 );
	UxPutMarginHeight( textField5, 5 );
	UxPutHighlightThickness( textField5, 0 );
	UxPutHighlightColor( textField5, WindowBackground );
	UxPutEditable( textField5, "false" );
	UxPutCursorPositionVisible( textField5, "false" );
	UxPutTopShadowColor( textField5, WindowBackground );
	UxPutBottomShadowColor( textField5, WindowBackground );
	UxPutText( textField5, "Info on:" );
	UxPutFontList( textField5, BoldTextFont );
	UxPutBackground( textField5, WindowBackground );
	UxPutHeight( textField5, 35 );
	UxPutWidth( textField5, 70 );
	UxPutY( textField5, 310 );
	UxPutX( textField5, 30 );

	UxPutEditable( text2, "false" );
	UxPutAutoShowCursorPosition( text2, "false" );
	UxPutAccelerators( text2, "" );
	UxPutWordWrap( text2, "false" );
	UxPutHighlightOnEnter( text2, "false" );
	UxPutEditMode( text2, "single_line_edit" );
	UxPutBlinkRate( text2, 500 );
	UxPutPendingDelete( text2, "true" );
	UxPutTranslations( text2, TextTable );
	UxPutCursorPositionVisible( text2, "false" );
	UxPutFontList( text2, BoldTextFont );
	UxPutHighlightColor( text2, "Black" );
	UxPutForeground( text2, TextForeground );
	UxPutBackground( text2, TextBackground );
	UxPutHeight( text2, 40 );
	UxPutWidth( text2, 325 );
	UxPutY( text2, 320 );
	UxPutX( text2, 20 );

	UxPutTranslations( arrowButton2, "" );
	UxPutBorderColor( arrowButton2, WindowBackground );
	UxPutBackground( arrowButton2, ButtonBackground );
	UxPutTopShadowColor( arrowButton2, ButtonForeground );
	UxPutBottomShadowColor( arrowButton2, WindowBackground );
	UxPutForeground( arrowButton2, "PaleGreen" );
	UxPutHeight( arrowButton2, 20 );
	UxPutWidth( arrowButton2, 20 );
	UxPutY( arrowButton2, 120 );
	UxPutX( arrowButton2, 671 );

	UxPutTranslations( arrowButton1, "" );
	UxPutBorderColor( arrowButton1, WindowBackground );
	UxPutBackground( arrowButton1, ButtonBackground );
	UxPutTopShadowColor( arrowButton1, ButtonForeground );
	UxPutBottomShadowColor( arrowButton1, WindowBackground );
	UxPutForeground( arrowButton1, "PaleGreen" );
	UxPutArrowDirection( arrowButton1, "arrow_down" );
	UxPutHeight( arrowButton1, 20 );
	UxPutWidth( arrowButton1, 20 );
	UxPutY( arrowButton1, 310 );
	UxPutX( arrowButton1, 671 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ApplicWindow()
{
	/* Create the swidgets */

	ApplicWindow = UxCreateApplicationShell( "ApplicWindow", NO_PARENT );
	UxPutContext( ApplicWindow, UxApplicWindowContext );

	mainWindow = UxCreateMainWindow( "mainWindow", ApplicWindow );
	pullDownMenu = UxCreateRowColumn( "pullDownMenu", mainWindow );
	UtilsPane = UxCreateRowColumn( "UtilsPane", pullDownMenu );
	SysEntry = UxCreatePushButtonGadget( "SysEntry", UtilsPane );
	UtilsPane_b2 = UxCreatePushButtonGadget( "UtilsPane_b2", UtilsPane );
	commandsCascade = UxCreateCascadeButton( "commandsCascade", pullDownMenu );
	workAreaForm = UxCreateForm( "workAreaForm", mainWindow );
	workAreaFrame = UxCreateFrame( "workAreaFrame", workAreaForm );
	workArea = UxCreateForm( "workArea", workAreaFrame );
	scrolledWindow = UxCreateScrolledWindow( "scrolledWindow", workArea );
	scrolledText = UxCreateScrolledText( "scrolledText", scrolledWindow );
	menu1_p1 = UxCreateRowColumn( "menu1_p1", workArea );
	menu1_p1_b1 = UxCreatePushButtonGadget( "menu1_p1_b1", menu1_p1 );
	menu1_p1_b2 = UxCreatePushButtonGadget( "menu1_p1_b2", menu1_p1 );
	menu1_p1_b3 = UxCreatePushButtonGadget( "menu1_p1_b3", menu1_p1 );
	menu1_p1_b4 = UxCreatePushButtonGadget( "menu1_p1_b4", menu1_p1 );
	menu1_p1_b6 = UxCreatePushButtonGadget( "menu1_p1_b6", menu1_p1 );
	menu1_p1_b7 = UxCreatePushButtonGadget( "menu1_p1_b7", menu1_p1 );
	menu1 = UxCreateRowColumn( "menu1", workArea );
	text1 = UxCreateText( "text1", workArea );
	SHelp = UxCreateText( "SHelp", workArea );
	CommandButton = UxCreatePushButton( "CommandButton", workArea );
	HistoButton = UxCreatePushButton( "HistoButton", workArea );
	SearchButton = UxCreatePushButton( "SearchButton", workArea );
	NewsButton = UxCreatePushButton( "NewsButton", workArea );
	PrintButton = UxCreatePushButton( "PrintButton", workArea );
	ReportButton = UxCreatePushButton( "ReportButton", workArea );
	CtxButton = UxCreatePushButton( "CtxButton", workArea );
	textField5 = UxCreateTextField( "textField5", workArea );
	text2 = UxCreateText( "text2", workArea );
	arrowButton2 = UxCreateArrowButton( "arrowButton2", workArea );
	arrowButton1 = UxCreateArrowButton( "arrowButton1", workArea );

	_Uxinit_ApplicWindow();

	/* Create the X widgets */

	UxCreateWidget( ApplicWindow );
	UxCreateWidget( mainWindow );
	UxCreateWidget( pullDownMenu );
	UxCreateWidget( UtilsPane );
	UxCreateWidget( SysEntry );
	UxCreateWidget( UtilsPane_b2 );
	UxPutSubMenuId( commandsCascade, "UtilsPane" );
	UxCreateWidget( commandsCascade );

	UxCreateWidget( workAreaForm );
	UxPutTopOffset( workAreaFrame, 0 );
	UxPutTopAttachment( workAreaFrame, "attach_form" );
	UxPutRightAttachment( workAreaFrame, "attach_form" );
	UxPutLeftAttachment( workAreaFrame, "attach_form" );
	UxPutBottomAttachment( workAreaFrame, "attach_form" );
	UxCreateWidget( workAreaFrame );

	UxCreateWidget( workArea );
	createCB_workArea( UxGetWidget( workArea ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutTopOffset( scrolledWindow, 5 );
	UxPutTopAttachment( scrolledWindow, "attach_form" );
	UxPutRightOffset( scrolledWindow, 5 );
	UxPutRightAttachment( scrolledWindow, "attach_form" );
	UxPutLeftOffset( scrolledWindow, 5 );
	UxPutLeftAttachment( scrolledWindow, "attach_form" );
	UxPutBottomOffset( scrolledWindow, 135 );
	UxPutBottomAttachment( scrolledWindow, "attach_form" );
	UxCreateWidget( scrolledWindow );

	UxCreateWidget( scrolledText );
	createCB_scrolledText( UxGetWidget( scrolledText ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxCreateWidget( menu1_p1 );
	UxCreateWidget( menu1_p1_b1 );
	UxCreateWidget( menu1_p1_b2 );
	UxCreateWidget( menu1_p1_b3 );
	UxCreateWidget( menu1_p1_b4 );
	UxCreateWidget( menu1_p1_b6 );
	UxCreateWidget( menu1_p1_b7 );
	UxPutTopWidget( menu1, "scrolledWindow" );
	UxPutTopOffset( menu1, 10 );
	UxPutTopAttachment( menu1, "attach_widget" );
	UxPutLeftOffset( menu1, 75 );
	UxPutLeftAttachment( menu1, "attach_form" );
	UxPutSubMenuId( menu1, "menu1_p1" );
	UxCreateWidget( menu1 );

	UxPutLeftWidget( text1, "menu1" );
	UxPutTopWidget( text1, "scrolledWindow" );
	UxPutTopOffset( text1, 5 );
	UxPutTopAttachment( text1, "attach_widget" );
	UxPutRightOffset( text1, 270 );
	UxPutRightAttachment( text1, "attach_form" );
	UxPutLeftOffset( text1, 5 );
	UxPutLeftAttachment( text1, "attach_widget" );
	UxPutBottomOffset( text1, 0 );
	UxCreateWidget( text1 );

	UxPutBottomOffset( SHelp, 45 );
	UxPutBottomAttachment( SHelp, "attach_form" );
	UxPutTopWidget( SHelp, "text1" );
	UxPutTopOffset( SHelp, 5 );
	UxPutTopAttachment( SHelp, "attach_widget" );
	UxPutRightOffset( SHelp, 5 );
	UxPutRightAttachment( SHelp, "attach_form" );
	UxPutLeftOffset( SHelp, 5 );
	UxPutLeftAttachment( SHelp, "attach_form" );
	UxCreateWidget( SHelp );

	UxPutBottomOffset( CommandButton, 5 );
	UxPutBottomAttachment( CommandButton, "attach_form" );
	UxPutTopWidget( CommandButton, "SHelp" );
	UxPutTopOffset( CommandButton, 5 );
	UxPutTopAttachment( CommandButton, "attach_widget" );
	UxPutLeftOffset( CommandButton, 10 );
	UxPutLeftAttachment( CommandButton, "attach_form" );
	UxCreateWidget( CommandButton );

	UxPutTopWidget( HistoButton, "SHelp" );
	UxPutTopOffset( HistoButton, 5 );
	UxPutTopAttachment( HistoButton, "attach_widget" );
	UxPutLeftWidget( HistoButton, "CommandButton" );
	UxPutLeftOffset( HistoButton, 10 );
	UxPutLeftAttachment( HistoButton, "attach_widget" );
	UxPutBottomOffset( HistoButton, 5 );
	UxPutBottomAttachment( HistoButton, "attach_form" );
	UxCreateWidget( HistoButton );

	UxPutLeftWidget( SearchButton, "HistoButton" );
	UxPutLeftOffset( SearchButton, 10 );
	UxPutLeftAttachment( SearchButton, "attach_widget" );
	UxPutTopWidget( SearchButton, "SHelp" );
	UxPutTopOffset( SearchButton, 5 );
	UxPutTopAttachment( SearchButton, "attach_widget" );
	UxPutBottomOffset( SearchButton, 5 );
	UxPutBottomAttachment( SearchButton, "attach_form" );
	UxCreateWidget( SearchButton );

	UxPutBottomOffset( NewsButton, 5 );
	UxPutBottomAttachment( NewsButton, "attach_form" );
	UxPutLeftWidget( NewsButton, "SearchButton" );
	UxPutLeftOffset( NewsButton, 5 );
	UxPutLeftAttachment( NewsButton, "attach_widget" );
	UxPutTopWidget( NewsButton, "SHelp" );
	UxPutTopOffset( NewsButton, 5 );
	UxPutTopAttachment( NewsButton, "attach_widget" );
	UxCreateWidget( NewsButton );

	UxPutBottomOffset( PrintButton, 5 );
	UxPutBottomAttachment( PrintButton, "attach_form" );
	UxPutLeftWidget( PrintButton, "NewsButton" );
	UxPutLeftOffset( PrintButton, 5 );
	UxPutLeftAttachment( PrintButton, "attach_widget" );
	UxPutTopWidget( PrintButton, "SHelp" );
	UxPutTopOffset( PrintButton, 5 );
	UxPutTopAttachment( PrintButton, "attach_widget" );
	UxCreateWidget( PrintButton );

	UxPutBottomOffset( ReportButton, 5 );
	UxPutBottomAttachment( ReportButton, "attach_form" );
	UxPutTopWidget( ReportButton, "SHelp" );
	UxPutTopOffset( ReportButton, 5 );
	UxPutTopAttachment( ReportButton, "attach_widget" );
	UxPutLeftWidget( ReportButton, "PrintButton" );
	UxPutLeftOffset( ReportButton, 5 );
	UxPutLeftAttachment( ReportButton, "attach_widget" );
	UxCreateWidget( ReportButton );

	UxPutTopWidget( CtxButton, "SHelp" );
	UxPutTopOffset( CtxButton, 5 );
	UxPutTopAttachment( CtxButton, "attach_widget" );
	UxPutLeftWidget( CtxButton, "ReportButton" );
	UxPutLeftOffset( CtxButton, 5 );
	UxPutLeftAttachment( CtxButton, "attach_widget" );
	UxPutBottomOffset( CtxButton, 5 );
	UxPutBottomAttachment( CtxButton, "attach_form" );
	UxCreateWidget( CtxButton );

	UxPutLeftOffset( textField5, 10 );
	UxPutLeftAttachment( textField5, "attach_form" );
	UxPutTopWidget( textField5, "scrolledWindow" );
	UxPutTopOffset( textField5, 12 );
	UxPutTopAttachment( textField5, "attach_widget" );
	UxCreateWidget( textField5 );

	UxPutTopWidget( text2, "scrolledWindow" );
	UxPutTopOffset( text2, 5 );
	UxPutTopAttachment( text2, "attach_widget" );
	UxPutRightOffset( text2, 35 );
	UxPutRightAttachment( text2, "attach_form" );
	UxPutLeftOffset( text2, 10 );
	UxPutLeftWidget( text2, "text1" );
	UxPutLeftAttachment( text2, "attach_widget" );
	UxCreateWidget( text2 );

	UxPutTopWidget( arrowButton2, "scrolledWindow" );
	UxPutTopOffset( arrowButton2, 3 );
	UxPutTopAttachment( arrowButton2, "attach_widget" );
	UxPutRightOffset( arrowButton2, 5 );
	UxPutRightAttachment( arrowButton2, "attach_form" );
	UxPutLeftWidget( arrowButton2, "text2" );
	UxPutLeftOffset( arrowButton2, 5 );
	UxPutLeftAttachment( arrowButton2, "attach_widget" );
	UxCreateWidget( arrowButton2 );

	UxPutTopWidget( arrowButton1, "arrowButton2" );
	UxPutTopOffset( arrowButton1, 0 );
	UxPutTopAttachment( arrowButton1, "attach_widget" );
	UxPutRightOffset( arrowButton1, 5 );
	UxPutRightAttachment( arrowButton1, "attach_form" );
	UxPutLeftWidget( arrowButton1, "text2" );
	UxPutLeftOffset( arrowButton1, 5 );
	UxPutLeftAttachment( arrowButton1, "attach_widget" );
	UxCreateWidget( arrowButton1 );


	UxAddCallback( ApplicWindow, XmNpopupCallback,
			popupCB_ApplicWindow,
			(XtPointer) UxApplicWindowContext );

	UxPutMenuHelpWidget( pullDownMenu, "" );

	UxAddCallback( SysEntry, XmNactivateCallback,
			activateCB_SysEntry,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( UtilsPane_b2, XmNactivateCallback,
			activateCB_UtilsPane_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( workArea, XmNmapCallback,
			mapCB_workArea,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( scrolledText, XmNvalueChangedCallback,
			valueChangedCB_scrolledText,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( scrolledText, XmNmotionVerifyCallback,
			motionVerifyCB_scrolledText,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1, XmNentryCallback,
			entryCB_menu1_p1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b1, XmNactivateCallback,
			activateCB_menu1_p1_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b2, XmNactivateCallback,
			activateCB_menu1_p1_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b3, XmNactivateCallback,
			activateCB_menu1_p1_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b4, XmNactivateCallback,
			activateCB_menu1_p1_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b6, XmNactivateCallback,
			activateCB_menu1_p1_b6,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b7, XmNactivateCallback,
			activateCB_menu1_p1_b7,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1, XmNentryCallback,
			entryCB_menu1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text1, XmNactivateCallback,
			activateCB_text1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( SHelp, XmNactivateCallback,
			activateCB_SHelp,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( CommandButton, XmNactivateCallback,
			activateCB_CommandButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( HistoButton, XmNactivateCallback,
			activateCB_HistoButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( SearchButton, XmNactivateCallback,
			activateCB_SearchButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( NewsButton, XmNactivateCallback,
			activateCB_NewsButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( PrintButton, XmNactivateCallback,
			activateCB_PrintButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( ReportButton, XmNactivateCallback,
			activateCB_ReportButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( CtxButton, XmNactivateCallback,
			activateCB_CtxButton,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( text2, XmNactivateCallback,
			activateCB_text2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( arrowButton2, XmNactivateCallback,
			activateCB_arrowButton2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( arrowButton1, XmNactivateCallback,
			activateCB_arrowButton1,
			(XtPointer) UxApplicWindowContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ApplicWindow );

	UxMainWindowSetAreas( mainWindow, pullDownMenu, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, workAreaForm );
	return ( ApplicWindow );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_popup_ApplicWindow()
{
	swidget                 rtrn;
	_UxCApplicWindow        *UxContext;

	UxApplicWindowContext = UxContext =
		(_UxCApplicWindow *) UxMalloc( sizeof(_UxCApplicWindow) );

	rtrn = _Uxbuild_ApplicWindow();

	PutSelection(text1,0);
	PutSelection(text2,2);
	PutSelection(scrolledText,1);
	
	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	popup_ApplicWindow()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "HInit", action_HInit },
				{ "SelectCommand", action_SelectCommand },
				{ "WriteHelp", action_WriteHelp },
				{ "HelpACT", action_HelpACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		UxLoadResources( "ApplicWindow.rf" );
		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_popup_ApplicWindow();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

