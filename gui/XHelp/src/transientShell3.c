/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
.VERSIOn
 090818		last modif
*/

/*******************************************************************************
	transientShell3.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

extern char print_com[], mid_mail[];

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxtransientShell3;
	swidget	UxworkArea3;
	swidget	UxSHelp3;
	swidget	UxNewsButton3;
	swidget	UxPrintButton3;
	swidget	Uxlabel3;
	swidget	Uxlabel4;
	swidget	UxtextField1;
	swidget	UxtextField2;
} _UxCtransientShell3;

#define transientShell3         UxTransientShell3Context->UxtransientShell3
#define workArea3               UxTransientShell3Context->UxworkArea3
#define SHelp3                  UxTransientShell3Context->UxSHelp3
#define NewsButton3             UxTransientShell3Context->UxNewsButton3
#define PrintButton3            UxTransientShell3Context->UxPrintButton3
#define label3                  UxTransientShell3Context->Uxlabel3
#define label4                  UxTransientShell3Context->Uxlabel4
#define textField1              UxTransientShell3Context->UxtextField1
#define textField2              UxTransientShell3Context->UxtextField2

static _UxCtransientShell3	*UxTransientShell3Context;


extern void helpPR(), InitHelp(), WriteHelpAW(), MapRaised(), SensMenu();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_transientShell3();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	WriteHelpAW(UxWidget);
	helpPR(UxWidget);
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	popupCB_transientShell3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	extern char print_com[], mid_mail[];
	
	UxPutText(textField1,print_com);
	UxPutText(textField2,mid_mail);
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	mapCB_workArea3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	createCB_workArea3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	activateCB_SHelp3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	activateCB_NewsButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	extern char mid_mail[], print_com[];
	extern swidget sys;
	
	strcpy(print_com,UxGetText(textField1));
	strcpy(mid_mail,UxGetText(textField2));
	UxPopdownInterface(sys);
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

static void	activateCB_PrintButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell3     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell3Context;
	UxTransientShell3Context = UxContext =
			(_UxCtransientShell3 *) UxGetContext( UxThisWidget );
	{
	extern swidget sys;
	
	UxPopdownInterface(sys);
	
	}
	UxTransientShell3Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_transientShell3()
{
	UxPutTitle( transientShell3, "System Definitions" );
	UxPutHeight( transientShell3, 190 );
	UxPutWidth( transientShell3, 350 );
	UxPutY( transientShell3, 490 );
	UxPutX( transientShell3, 80 );

	UxPutResizePolicy( workArea3, "resize_none" );
	UxPutNoResize( workArea3, "true" );
	UxPutUnitType( workArea3, "pixels" );
	UxPutBackground( workArea3, WindowBackground );
	UxPutBorderWidth( workArea3, 0 );
	UxPutHeight( workArea3, 498 );
	UxPutWidth( workArea3, 600 );
	UxPutY( workArea3, 2 );
	UxPutX( workArea3, 20 );

	UxPutText( SHelp3, "Push button OK to update the system definitions" );
	UxPutEditable( SHelp3, "false" );
	UxPutAccelerators( SHelp3, "" );
	UxPutWordWrap( SHelp3, "false" );
	UxPutHighlightOnEnter( SHelp3, "true" );
	UxPutEditMode( SHelp3, "single_line_edit" );
	UxPutBlinkRate( SHelp3, 500 );
	UxPutPendingDelete( SHelp3, "true" );
	UxPutTranslations( SHelp3, "" );
	UxPutCursorPositionVisible( SHelp3, "true" );
	UxPutFontList( SHelp3, TextFont );
	UxPutHighlightColor( SHelp3, ApplicBackground );
	UxPutForeground( SHelp3, TextForeground );
	UxPutBackground( SHelp3, SHelpBackground );
	UxPutHeight( SHelp3, 40 );
	UxPutWidth( SHelp3, 325 );
	UxPutY( SHelp3, 380 );
	UxPutX( SHelp3, 30 );

	UxPutHighlightColor( NewsButton3, "Black" );
	UxPutRecomputeSize( NewsButton3, "false" );
	UxPutFontList( NewsButton3, BoldTextFont );
	UxPutForeground( NewsButton3, ButtonForeground );
	UxPutLabelString( NewsButton3, "OK" );
	UxPutBackground( NewsButton3, ButtonBackground );
	UxPutHeight( NewsButton3, 30 );
	UxPutWidth( NewsButton3, 95 );
	UxPutY( NewsButton3, 630 );
	UxPutX( NewsButton3, 20 );

	UxPutHighlightColor( PrintButton3, "Black" );
	UxPutRecomputeSize( PrintButton3, "false" );
	UxPutFontList( PrintButton3, BoldTextFont );
	UxPutForeground( PrintButton3, ButtonForeground );
	UxPutLabelString( PrintButton3, "Cancel" );
	UxPutBackground( PrintButton3, ButtonBackground );
	UxPutHeight( PrintButton3, 30 );
	UxPutWidth( PrintButton3, 95 );
	UxPutY( PrintButton3, 330 );
	UxPutX( PrintButton3, 280 );

	UxPutLabelString( label3, "MIDAS e-mail" );
	UxPutForeground( label3, TextForeground );
	UxPutFontList( label3, BoldTextFont );
	UxPutBackground( label3, LabelBackground );
	UxPutHeight( label3, 20 );
	UxPutWidth( label3, 130 );
	UxPutY( label3, 10 );
	UxPutX( label3, 10 );

	UxPutLabelString( label4, "Print Command" );
	UxPutForeground( label4, TextForeground );
	UxPutFontList( label4, BoldTextFont );
	UxPutBackground( label4, LabelBackground );
	UxPutHeight( label4, 20 );
	UxPutWidth( label4, 130 );
	UxPutY( label4, 10 );
	UxPutX( label4, 10 );

	UxPutText( textField1, print_com );
	UxPutForeground( textField1, TextForeground );
	UxPutFontList( textField1, TextFont );
	UxPutBackground( textField1, TextBackground );
	UxPutHeight( textField1, 35 );
	UxPutWidth( textField1, 250 );
	UxPutY( textField1, 10 );
	UxPutX( textField1, 140 );

	UxPutText( textField2, mid_mail );
	UxPutForeground( textField2, TextForeground );
	UxPutFontList( textField2, TextFont );
	UxPutBackground( textField2, TextBackground );
	UxPutHeight( textField2, 35 );
	UxPutWidth( textField2, 250 );
	UxPutY( textField2, 60 );
	UxPutX( textField2, 140 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_transientShell3()
{
	/* Create the swidgets */

	transientShell3 = UxCreateTransientShell( "transientShell3", NO_PARENT );
	UxPutContext( transientShell3, UxTransientShell3Context );

	workArea3 = UxCreateForm( "workArea3", transientShell3 );
	SHelp3 = UxCreateText( "SHelp3", workArea3 );
	NewsButton3 = UxCreatePushButton( "NewsButton3", workArea3 );
	PrintButton3 = UxCreatePushButton( "PrintButton3", workArea3 );
	label3 = UxCreateLabel( "label3", workArea3 );
	label4 = UxCreateLabel( "label4", workArea3 );
	textField1 = UxCreateText( "textField1", workArea3 );
	textField2 = UxCreateText( "textField2", workArea3 );

	_Uxinit_transientShell3();

	/* Create the X widgets */

	UxCreateWidget( transientShell3 );
	UxCreateWidget( workArea3 );
	createCB_workArea3( UxGetWidget( workArea3 ),
			(XtPointer) UxTransientShell3Context, (XtPointer) NULL );

	UxPutBottomOffset( SHelp3, 50 );
	UxPutBottomAttachment( SHelp3, "attach_form" );
	UxPutRightOffset( SHelp3, 5 );
	UxPutRightAttachment( SHelp3, "attach_form" );
	UxPutLeftOffset( SHelp3, 5 );
	UxPutLeftAttachment( SHelp3, "attach_form" );
	UxCreateWidget( SHelp3 );

	UxPutLeftOffset( NewsButton3, 50 );
	UxPutLeftAttachment( NewsButton3, "attach_form" );
	UxPutTopWidget( NewsButton3, "SHelp3" );
	UxPutTopOffset( NewsButton3, 10 );
	UxPutTopAttachment( NewsButton3, "attach_widget" );
	UxCreateWidget( NewsButton3 );

	UxPutLeftWidget( PrintButton3, "NewsButton3" );
	UxPutLeftOffset( PrintButton3, 40 );
	UxPutLeftAttachment( PrintButton3, "attach_widget" );
	UxPutTopWidget( PrintButton3, "SHelp3" );
	UxPutTopOffset( PrintButton3, 10 );
	UxPutTopAttachment( PrintButton3, "attach_widget" );
	UxCreateWidget( PrintButton3 );

	UxPutTopOffset( label3, 65 );
	UxPutTopAttachment( label3, "attach_form" );
	UxPutLeftOffset( label3, 5 );
	UxPutLeftAttachment( label3, "attach_form" );
	UxCreateWidget( label3 );

	UxPutTopAttachment( label4, "attach_form" );
	UxPutTopOffset( label4, 15 );
	UxPutLeftOffset( label4, 5 );
	UxPutLeftAttachment( label4, "attach_form" );
	UxCreateWidget( label4 );

	UxPutTopOffset( textField1, 10 );
	UxPutLeftOffset( textField1, 140 );
	UxPutTopAttachment( textField1, "attach_form" );
	UxPutRightOffset( textField1, 5 );
	UxPutRightAttachment( textField1, "attach_form" );
	UxPutLeftAttachment( textField1, "attach_form" );
	UxCreateWidget( textField1 );

	UxPutTopOffset( textField2, 60 );
	UxPutTopAttachment( textField2, "attach_form" );
	UxPutRightOffset( textField2, 5 );
	UxPutRightAttachment( textField2, "attach_form" );
	UxPutLeftOffset( textField2, 140 );
	UxPutLeftAttachment( textField2, "attach_form" );
	UxCreateWidget( textField2 );


	UxAddCallback( transientShell3, XmNpopupCallback,
			popupCB_transientShell3,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( workArea3, XmNmapCallback,
			mapCB_workArea3,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( SHelp3, XmNactivateCallback,
			activateCB_SHelp3,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( NewsButton3, XmNactivateCallback,
			activateCB_NewsButton3,
			(XtPointer) UxTransientShell3Context );

	UxAddCallback( PrintButton3, XmNactivateCallback,
			activateCB_PrintButton3,
			(XtPointer) UxTransientShell3Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( transientShell3 );

	return ( transientShell3 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_transientShell3()
{
	swidget                 rtrn;
	_UxCtransientShell3     *UxContext;

	UxTransientShell3Context = UxContext =
		(_UxCtransientShell3 *) UxMalloc( sizeof(_UxCtransientShell3) );

	rtrn = _Uxbuild_transientShell3();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_transientShell3()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "WriteHelp", action_WriteHelp }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_transientShell3();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

