/*===========================================================================
  Copyright (C) 1995-2010 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	transientShell6.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxtransientShell6;
	swidget	UxworkArea6;
	swidget	UxSHelp5;
	swidget	UxNewsButton5;
	swidget	Uxlabel6;
	swidget	UxtextField3;
	swidget	UxNewsButton6;
	swidget	UxPrintButton5;
} _UxCtransientShell6;

#define transientShell6         UxTransientShell6Context->UxtransientShell6
#define workArea6               UxTransientShell6Context->UxworkArea6
#define SHelp5                  UxTransientShell6Context->UxSHelp5
#define NewsButton5             UxTransientShell6Context->UxNewsButton5
#define label6                  UxTransientShell6Context->Uxlabel6
#define textField3              UxTransientShell6Context->UxtextField3
#define NewsButton6             UxTransientShell6Context->UxNewsButton6
#define PrintButton5            UxTransientShell6Context->UxPrintButton5

static _UxCtransientShell6	*UxTransientShell6Context;

extern void helpPR(), WriteHelpAW(), SearchHelp();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_transientShell6();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell6     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell6Context;
	UxTransientShell6Context = UxContext =
			(_UxCtransientShell6 *) UxGetContext( UxThisWidget );
	{
	WriteHelpAW(UxWidget);
	helpPR(UxWidget);
	
	}
	UxTransientShell6Context = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	mapCB_workArea6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell6     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell6Context;
	UxTransientShell6Context = UxContext =
			(_UxCtransientShell6 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell6Context = UxSaveCtx;
}

static void	createCB_workArea6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell6     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell6Context;
	UxTransientShell6Context = UxContext =
			(_UxCtransientShell6 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell6Context = UxSaveCtx;
}

static void	activateCB_SHelp5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell6     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell6Context;
	UxTransientShell6Context = UxContext =
			(_UxCtransientShell6 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell6Context = UxSaveCtx;
}

static void	activateCB_NewsButton5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell6     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell6Context;
	UxTransientShell6Context = UxContext =
			(_UxCtransientShell6 *) UxGetContext( UxThisWidget );
	{
	
	SearchHelp(UxGetText(textField3),1L);
	
	}
	UxTransientShell6Context = UxSaveCtx;
}

static void	activateCB_textField3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell6     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell6Context;
	UxTransientShell6Context = UxContext =
			(_UxCtransientShell6 *) UxGetContext( UxThisWidget );
	{
	SearchHelp(UxGetText(UxThisWidget),1L);
	}
	UxTransientShell6Context = UxSaveCtx;
}

static void	activateCB_NewsButton6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell6     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell6Context;
	UxTransientShell6Context = UxContext =
			(_UxCtransientShell6 *) UxGetContext( UxThisWidget );
	{
	
	SearchHelp(UxGetText(textField3),0L);
	UxPutText(textField3,"");
	}
	UxTransientShell6Context = UxSaveCtx;
}

static void	activateCB_PrintButton5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell6     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell6Context;
	UxTransientShell6Context = UxContext =
			(_UxCtransientShell6 *) UxGetContext( UxThisWidget );
	{
	extern swidget srch;
	
	UxPopdownInterface(srch);
	
	}
	UxTransientShell6Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_transientShell6()
{
	UxPutTitle( transientShell6, "Search Form" );
	UxPutHeight( transientShell6, 160 );
	UxPutWidth( transientShell6, 300 );
	UxPutY( transientShell6, 330 );
	UxPutX( transientShell6, 910 );

	UxPutResizePolicy( workArea6, "resize_none" );
	UxPutNoResize( workArea6, "true" );
	UxPutUnitType( workArea6, "pixels" );
	UxPutBackground( workArea6, WindowBackground );
	UxPutBorderWidth( workArea6, 0 );
	UxPutHeight( workArea6, 498 );
	UxPutWidth( workArea6, 600 );
	UxPutY( workArea6, 2 );
	UxPutX( workArea6, 20 );

	UxPutText( SHelp5, "Case insensitive search in help window.\nApply/Return highlights all occurences." );
	UxPutEditable( SHelp5, "false" );
	UxPutAccelerators( SHelp5, "" );
	UxPutWordWrap( SHelp5, "false" );
	UxPutHighlightOnEnter( SHelp5, "true" );
	UxPutEditMode( SHelp5, "multi_line_edit" );
	UxPutBlinkRate( SHelp5, 500 );
	UxPutPendingDelete( SHelp5, "true" );
	UxPutTranslations( SHelp5, "" );
	UxPutCursorPositionVisible( SHelp5, "true" );
	UxPutFontList( SHelp5, TextFont );
	UxPutHighlightColor( SHelp5, ApplicBackground );
	UxPutForeground( SHelp5, TextForeground );
	UxPutBackground( SHelp5, SHelpBackground );
	UxPutHeight( SHelp5, 40 );
	UxPutWidth( SHelp5, 325 );
	UxPutY( SHelp5, 380 );
	UxPutX( SHelp5, 30 );

	UxPutHighlightColor( NewsButton5, "Black" );
	UxPutRecomputeSize( NewsButton5, "false" );
	UxPutFontList( NewsButton5, BoldTextFont );
	UxPutForeground( NewsButton5, ButtonForeground );
	UxPutLabelString( NewsButton5, "Apply" );
	UxPutBackground( NewsButton5, ButtonBackground );
	UxPutHeight( NewsButton5, 30 );
	UxPutWidth( NewsButton5, 85 );
	UxPutY( NewsButton5, 630 );
	UxPutX( NewsButton5, 20 );

	UxPutLabelString( label6, "String" );
	UxPutForeground( label6, TextForeground );
	UxPutFontList( label6, BoldTextFont );
	UxPutBackground( label6, LabelBackground );
	UxPutHeight( label6, 20 );
	UxPutWidth( label6, 80 );
	UxPutY( label6, 10 );
	UxPutX( label6, 10 );

	UxPutText( textField3, "" );
	UxPutForeground( textField3, TextForeground );
	UxPutFontList( textField3, TextFont );
	UxPutBackground( textField3, TextBackground );
	UxPutHeight( textField3, 35 );
	UxPutWidth( textField3, 250 );
	UxPutY( textField3, 60 );
	UxPutX( textField3, 140 );

	UxPutHighlightColor( NewsButton6, "Black" );
	UxPutRecomputeSize( NewsButton6, "false" );
	UxPutFontList( NewsButton6, BoldTextFont );
	UxPutForeground( NewsButton6, ButtonForeground );
	UxPutLabelString( NewsButton6, "Clear" );
	UxPutBackground( NewsButton6, ButtonBackground );
	UxPutHeight( NewsButton6, 30 );
	UxPutWidth( NewsButton6, 85 );
	UxPutY( NewsButton6, 65 );
	UxPutX( NewsButton6, 102 );

	UxPutHighlightColor( PrintButton5, "Black" );
	UxPutRecomputeSize( PrintButton5, "false" );
	UxPutFontList( PrintButton5, BoldTextFont );
	UxPutForeground( PrintButton5, ButtonForeground );
	UxPutLabelString( PrintButton5, "Cancel" );
	UxPutBackground( PrintButton5, ButtonBackground );
	UxPutHeight( PrintButton5, 30 );
	UxPutWidth( PrintButton5, 85 );
	UxPutY( PrintButton5, 330 );
	UxPutX( PrintButton5, 280 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_transientShell6()
{
	/* Create the swidgets */

	transientShell6 = UxCreateTransientShell( "transientShell6", NO_PARENT );
	UxPutContext( transientShell6, UxTransientShell6Context );

	workArea6 = UxCreateForm( "workArea6", transientShell6 );
	SHelp5 = UxCreateText( "SHelp5", workArea6 );
	NewsButton5 = UxCreatePushButton( "NewsButton5", workArea6 );
	label6 = UxCreateLabel( "label6", workArea6 );
	textField3 = UxCreateText( "textField3", workArea6 );
	NewsButton6 = UxCreatePushButton( "NewsButton6", workArea6 );
	PrintButton5 = UxCreatePushButton( "PrintButton5", workArea6 );

	_Uxinit_transientShell6();

	/* Create the X widgets */

	UxCreateWidget( transientShell6 );
	UxCreateWidget( workArea6 );
	createCB_workArea6( UxGetWidget( workArea6 ),
			(XtPointer) UxTransientShell6Context, (XtPointer) NULL );

	UxPutTopOffset( SHelp5, 55 );
	UxPutTopAttachment( SHelp5, "attach_form" );
	UxPutBottomOffset( SHelp5, 50 );
	UxPutBottomAttachment( SHelp5, "attach_form" );
	UxPutRightOffset( SHelp5, 5 );
	UxPutRightAttachment( SHelp5, "attach_form" );
	UxPutLeftOffset( SHelp5, 5 );
	UxPutLeftAttachment( SHelp5, "attach_form" );
	UxCreateWidget( SHelp5 );

	UxPutBottomOffset( NewsButton5, 10 );
	UxPutBottomAttachment( NewsButton5, "attach_form" );
	UxPutLeftOffset( NewsButton5, 15 );
	UxPutLeftAttachment( NewsButton5, "attach_form" );
	UxPutTopWidget( NewsButton5, "SHelp5" );
	UxPutTopOffset( NewsButton5, 10 );
	UxPutTopAttachment( NewsButton5, "attach_widget" );
	UxCreateWidget( NewsButton5 );

	UxPutTopOffset( label6, 15 );
	UxPutTopAttachment( label6, "attach_form" );
	UxPutLeftOffset( label6, 5 );
	UxPutLeftAttachment( label6, "attach_form" );
	UxCreateWidget( label6 );

	UxPutTopOffset( textField3, 10 );
	UxPutTopAttachment( textField3, "attach_form" );
	UxPutRightOffset( textField3, 5 );
	UxPutRightAttachment( textField3, "attach_form" );
	UxPutLeftOffset( textField3, 90 );
	UxPutLeftAttachment( textField3, "attach_form" );
	UxCreateWidget( textField3 );

	UxPutTopWidget( NewsButton6, "SHelp5" );
	UxPutTopOffset( NewsButton6, 10 );
	UxPutTopAttachment( NewsButton6, "attach_widget" );
	UxPutBottomOffset( NewsButton6, 10 );
	UxPutBottomAttachment( NewsButton6, "attach_form" );
	UxPutLeftWidget( NewsButton6, "NewsButton5" );
	UxPutLeftOffset( NewsButton6, 10 );
	UxPutLeftAttachment( NewsButton6, "attach_widget" );
	UxCreateWidget( NewsButton6 );

	UxPutLeftWidget( PrintButton5, "NewsButton6" );
	UxPutLeftOffset( PrintButton5, 10 );
	UxPutLeftAttachment( PrintButton5, "attach_widget" );
	UxPutTopWidget( PrintButton5, "SHelp5" );
	UxPutTopOffset( PrintButton5, 10 );
	UxPutTopAttachment( PrintButton5, "attach_widget" );
	UxCreateWidget( PrintButton5 );


	UxAddCallback( workArea6, XmNmapCallback,
			mapCB_workArea6,
			(XtPointer) UxTransientShell6Context );

	UxAddCallback( SHelp5, XmNactivateCallback,
			activateCB_SHelp5,
			(XtPointer) UxTransientShell6Context );

	UxAddCallback( NewsButton5, XmNactivateCallback,
			activateCB_NewsButton5,
			(XtPointer) UxTransientShell6Context );

	UxAddCallback( textField3, XmNactivateCallback,
			activateCB_textField3,
			(XtPointer) UxTransientShell6Context );

	UxAddCallback( NewsButton6, XmNactivateCallback,
			activateCB_NewsButton6,
			(XtPointer) UxTransientShell6Context );

	UxAddCallback( PrintButton5, XmNactivateCallback,
			activateCB_PrintButton5,
			(XtPointer) UxTransientShell6Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( transientShell6 );

	return ( transientShell6 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_transientShell6()
{
	swidget                 rtrn;
	_UxCtransientShell6     *UxContext;

	UxTransientShell6Context = UxContext =
		(_UxCtransientShell6 *) UxMalloc( sizeof(_UxCtransientShell6) );

	rtrn = _Uxbuild_transientShell6();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_transientShell6()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "WriteHelp", action_WriteHelp }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_transientShell6();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

