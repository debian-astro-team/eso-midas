/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*
.VERSION
 090818		last modif
*/

/*******************************************************************************
	transientShell5.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxScText.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxtransientShell5;
	swidget	UxworkArea5;
	swidget	UxscrolledWindow5;
	swidget	UxscrolledText5;
	swidget	UxReportButton2;
} _UxCtransientShell5;

#define transientShell5         UxTransientShell5Context->UxtransientShell5
#define workArea5               UxTransientShell5Context->UxworkArea5
#define scrolledWindow5         UxTransientShell5Context->UxscrolledWindow5
#define scrolledText5           UxTransientShell5Context->UxscrolledText5
#define ReportButton2           UxTransientShell5Context->UxReportButton2

static _UxCtransientShell5	*UxTransientShell5Context;

extern void helpPR(), WriteHelpAW(), PutSelection(), SelHelp();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable4 = "#override\n\
<Btn1Up>:SelectCommand()\n";


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_transientShell5();

/*******************************************************************************
	Auxiliary code from the Declarations Editor:
*******************************************************************************/

void DispList(s)

char *s;

{
UxPutText(scrolledText5,s);
}

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell5     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell5Context;
	UxTransientShell5Context = UxContext =
			(_UxCtransientShell5 *) UxGetContext( UxThisWidget );
	{
	WriteHelpAW(UxWidget);
	helpPR(UxWidget);
	
	}
	UxTransientShell5Context = UxSaveCtx;
}

static void	action_SelectCommand( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCtransientShell5     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell5Context;
	UxTransientShell5Context = UxContext =
			(_UxCtransientShell5 *) UxGetContext( UxThisWidget );
	{
	
	 char *s;
	
	 if ((s = XmTextGetSelection(UxWidget)) == NULL) return; 
	
	 SelHelp(s);
	
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	
	
	}
	UxTransientShell5Context = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	popupCB_transientShell5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell5     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell5Context;
	UxTransientShell5Context = UxContext =
			(_UxCtransientShell5 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell5Context = UxSaveCtx;
}

static void	mapCB_workArea5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell5     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell5Context;
	UxTransientShell5Context = UxContext =
			(_UxCtransientShell5 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell5Context = UxSaveCtx;
}

static void	createCB_workArea5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell5     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell5Context;
	UxTransientShell5Context = UxContext =
			(_UxCtransientShell5 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell5Context = UxSaveCtx;
}

static void	createCB_scrolledText5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell5     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell5Context;
	UxTransientShell5Context = UxContext =
			(_UxCtransientShell5 *) UxGetContext( UxThisWidget );
	{
	
	}
	UxTransientShell5Context = UxSaveCtx;
}

static void	activateCB_ReportButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCtransientShell5     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxTransientShell5Context;
	UxTransientShell5Context = UxContext =
			(_UxCtransientShell5 *) UxGetContext( UxThisWidget );
	{
	extern swidget hist;
	
	UxPopdownInterface(hist);
	
	}
	UxTransientShell5Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_transientShell5()
{
	UxPutWaitForWm( transientShell5, "false" );
	UxPutKeyboardFocusPolicy( transientShell5, "pointer" );
	UxPutGeometry( transientShell5, "" );
	UxPutTitle( transientShell5, "History List" );
	UxPutHeight( transientShell5, 300 );
	UxPutWidth( transientShell5, 240 );
	UxPutY( transientShell5, 280 );
	UxPutX( transientShell5, 470 );

	UxPutResizePolicy( workArea5, "resize_none" );
	UxPutNoResize( workArea5, "true" );
	UxPutUnitType( workArea5, "pixels" );
	UxPutBackground( workArea5, WindowBackground );
	UxPutBorderWidth( workArea5, 0 );
	UxPutHeight( workArea5, 498 );
	UxPutWidth( workArea5, 600 );
	UxPutY( workArea5, 2 );
	UxPutX( workArea5, 20 );

	UxPutTranslations( scrolledWindow5, "" );
	UxPutBackground( scrolledWindow5, ApplicBackground );
	UxPutWidth( scrolledWindow5, 490 );
	UxPutHeight( scrolledWindow5, 270 );
	UxPutShadowThickness( scrolledWindow5, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow5, "static" );
	UxPutVisualPolicy( scrolledWindow5, "variable" );
	UxPutY( scrolledWindow5, 20 );
	UxPutX( scrolledWindow5, 8 );
	UxPutScrollingPolicy( scrolledWindow5, "application_defined" );

	UxPutTranslations( scrolledText5, transTable4 );
	UxPutFontList( scrolledText5, SmallFont );
	UxPutHighlightColor( scrolledText5, "grey" );
	UxPutForeground( scrolledText5, TextForeground );
	UxPutEditMode( scrolledText5, "multi_line_edit" );
	UxPutScrollVertical( scrolledText5, "true" );
	UxPutScrollHorizontal( scrolledText5, "true" );
	UxPutEditable( scrolledText5, "false" );
	UxPutY( scrolledText5, 0 );
	UxPutX( scrolledText5, 0 );
	UxPutBackground( scrolledText5, "WhiteSmoke" );
	UxPutHeight( scrolledText5, 220 );
	UxPutWidth( scrolledText5, 670 );

	UxPutHighlightColor( ReportButton2, "Black" );
	UxPutRecomputeSize( ReportButton2, "false" );
	UxPutFontList( ReportButton2, BoldTextFont );
	UxPutForeground( ReportButton2, ButtonForeground );
	UxPutLabelString( ReportButton2, "Cancel" );
	UxPutBackground( ReportButton2, ButtonBackground );
	UxPutHeight( ReportButton2, 30 );
	UxPutWidth( ReportButton2, 95 );
	UxPutY( ReportButton2, 330 );
	UxPutX( ReportButton2, 430 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_transientShell5()
{
	/* Create the swidgets */

	transientShell5 = UxCreateTransientShell( "transientShell5", NO_PARENT );
	UxPutContext( transientShell5, UxTransientShell5Context );

	workArea5 = UxCreateForm( "workArea5", transientShell5 );
	scrolledWindow5 = UxCreateScrolledWindow( "scrolledWindow5", workArea5 );
	scrolledText5 = UxCreateScrolledText( "scrolledText5", scrolledWindow5 );
	ReportButton2 = UxCreatePushButton( "ReportButton2", workArea5 );

	_Uxinit_transientShell5();

	/* Create the X widgets */

	UxCreateWidget( transientShell5 );
	UxCreateWidget( workArea5 );
	createCB_workArea5( UxGetWidget( workArea5 ),
			(XtPointer) UxTransientShell5Context, (XtPointer) NULL );

	UxPutBottomOffset( scrolledWindow5, 40 );
	UxPutBottomAttachment( scrolledWindow5, "attach_form" );
	UxPutTopOffset( scrolledWindow5, 5 );
	UxPutTopAttachment( scrolledWindow5, "attach_form" );
	UxPutRightOffset( scrolledWindow5, 5 );
	UxPutRightAttachment( scrolledWindow5, "attach_form" );
	UxPutLeftOffset( scrolledWindow5, 5 );
	UxPutLeftAttachment( scrolledWindow5, "attach_form" );
	UxCreateWidget( scrolledWindow5 );

	UxCreateWidget( scrolledText5 );
	createCB_scrolledText5( UxGetWidget( scrolledText5 ),
			(XtPointer) UxTransientShell5Context, (XtPointer) NULL );

	UxPutBottomOffset( ReportButton2, 5 );
	UxPutBottomAttachment( ReportButton2, "attach_form" );
	UxPutLeftWidget( ReportButton2, "" );
	UxPutLeftOffset( ReportButton2, 75 );
	UxPutLeftAttachment( ReportButton2, "attach_form" );
	UxCreateWidget( ReportButton2 );


	UxAddCallback( transientShell5, XmNpopupCallback,
			popupCB_transientShell5,
			(XtPointer) UxTransientShell5Context );

	UxAddCallback( workArea5, XmNmapCallback,
			mapCB_workArea5,
			(XtPointer) UxTransientShell5Context );

	UxAddCallback( ReportButton2, XmNactivateCallback,
			activateCB_ReportButton2,
			(XtPointer) UxTransientShell5Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( transientShell5 );

	return ( transientShell5 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_transientShell5()
{
	swidget                 rtrn;
	_UxCtransientShell5     *UxContext;

	UxTransientShell5Context = UxContext =
		(_UxCtransientShell5 *) UxMalloc( sizeof(_UxCtransientShell5) );

	rtrn = _Uxbuild_transientShell5();

	PutSelection(scrolledText5,2);
	
	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_transientShell5()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "WriteHelp", action_WriteHelp },
				{ "SelectCommand", action_SelectCommand }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_transientShell5();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

