/* @(#)main_decl.h	1.2 (ESO-IPG) 6/16/93 09:48:38 */
/* @(#)gm_decl.h    1.0.0.0 (ESO-La Silla) 10/08/91 12:00:00 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        gm_decl.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Declarations of global variables for the
              graphical user interface of Spectra.
.KEYWORDS     global variables, X11
.COMMENTS     Athena widgets implementation.
.VERSION 1.0  1-Mar-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

int	MonitorPid;	/* Pid of the Midas monitor process */

float 	Rnull;		/* null values for Midas */
int 	Inull;
double 	Dnull;

/************************
 * Application variables 
 ************************/
float 	Thres,			/* Threshold */
	Tol,			/* Tolerance (Angstrom) */
	Avdisp,
        Wcenter,
	Shift,
	Alpha,
	Imin,			/* Strength intensity limit */
	Maxdev;
char 	Instrume[MAXLINE],	/* Instrument */
	Wlc[MAXLINE],		/* WLC name */
	Lincat[MAXLINE],	/* Line catalog name */
	Coerbr[MAXLINE],
	Guess[MAXLINE],
    	Seamtd[MAXLINE], 
	Wlcmtd[MAXLINE], 
	Rebmtd[MAXLINE], 
	Coropt[MAXLINE],
	Corvisu[MAXLINE],
	Twodopt[MAXLINE],
	Coorfil[MAXLINE],
	Session[MAXLINE];
int	Ystart,			/* starting line */
	Wrang[2], 		/* Wavelength range */
	Ywidth,			/* smooth factor */
	Width,			/* window size */
	Dcx[2],			/* fit degree */
	Wlcniter[2],
	Ystep;
double	Rebstrt,
	Rebend,
	Rebstp;

int Npix[2];

/* Plot residual variables */
int RowResidual;
int AllResidual; /* boolean */

int TolPixels; /* boolean -- option menu for tolerance units */

/* flux calibration variables */
char 	Extab[MAXLINE],
	Fluxtab[MAXLINE],
	Resptab[MAXLINE],
	Plotyp[MAXLINE],
	Fityp[MAXLINE],
	Response[MAXLINE];
int	Fitd;
float	Smooth;

/* extraction variables */
char	Extmtd[MAXLINE];
int	Skyord,
	Uppsky[2],
	Lowsky[2],
	Order,
	Radius,
	Niter,
	Objlim[2],
	Skymod;
float	Sigma,
	Ron,
	Gain;

/* Batch reduction variables */
char	Inputf[MAXLINE],
	Outputf[MAXLINE],
	Inpnumb[MAXLINE],
	Bias[MAXLINE],
	Dark[MAXLINE],
	Flat[MAXLINE],
	Redextab[MAXLINE],
	Redresp[MAXLINE],
	Redrbmtd[MAXLINE],
	Longsess[MAXLINE],
	Redsess[MAXLINE],
	Biasopt[4],
	Darkopt[4],
	Flatopt[4],
	Rotopt[4],
	Trimopt[4],
	Rebopt[4],
	Extopt[4],
	Respopt[4];
double	Rotstart,
	Rotstep;
int	Trim[4],
	Outnumb;

char Plotmode[MAXLINE] = "";

int UpdateToggle; /* boolean -- to avoid the set/long of toggles */

int ListType;	  /* type of list to display in the List Selection Box */
int DialogType;	  /* type of dialog to display in the Dialog Box */

#define MAXTEXT		256	/* max. lines interactively identified */

float 	LincatPos[MAXTEXT];	/* :WAVE in 'Lincat' pointed by the user */
float 	WlcPos[MAXTEXT];	/* :X values in LINE matching LincatPos[] */
int 	NumLincatPos;		/* cardinality of LincatPos[] */
int 	NumWlcPos;		/* cardinality of WlcPos[] */
int     IdentBegin = FALSE;     /* Toggle for the beginning of identification */

LCTAB *	Lc;		/* Line catalog table */
float *	LinePos;	/* array of positions of LINE.tbl */
float *	LinePeak;	/* array of intensities of LINE.tbl */
int *	LineRow;	/* row numbers. of each element of LinePos */
int	NumLinePos;	/* LinePos cardinality */

/*************************************************************
  Definitions related to the addition of spectral lines
 *************************************************************/
#define MAXADD	100

int	NToAdd; 		/* current no. of spectral lines to add */
float	LinesToAdd[MAXADD]; 	/* x-coords of lines to add */

