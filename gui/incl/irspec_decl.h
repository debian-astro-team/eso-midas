/* @(#)astro_decl.h    1.0.0.0 (ESO-La Silla) 10/08/91 12:00:00 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        irspec_decl.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Declarations of global variables for the
              graphical user interface of Irspec.
.KEYWORDS     global variables, X11
.COMMENTS     
.VERSION 1.0  1-Nov-1993   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

int	MonitorPid;	/* Pid of the Midas monitor process */

float 	Rnull;		/* null values for Midas */
int 	Inull;
double 	Dnull;

/************************
 * Application variables 
 ************************/

char 	BadpixTable[60];

int ListType;     /* type of list to display in the List Selection Box */
int DialogType;   /* type of dialog to display in the Dialog Box */


/** Bad pixel form variables **/
int BadpixMode = 'r';  		/* 'r'elative, 'a'bsolute */
int BadpixMethod = 'a'; 	/* 'a'utomatic, 'x', 'y', 'b' (x+y) */

/* Input files for the calibration */
char InObj[MAXLINE] = "";
char InSky[MAXLINE] = "";
char InStstar[MAXLINE] = "";
char InStsky[MAXLINE] = "";

char Flat[MAXLINE] = "";
char Dark[MAXLINE] = "";

char ParametersFile[MAXLINE] = "params.irs";
