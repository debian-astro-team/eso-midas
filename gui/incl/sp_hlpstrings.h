/* @(#)sp_hlpstrings.h	19.1 (ESO-IPG) 02/25/03 13:48:38 */
#define HLP_PARS_FILE	"Name of the parameters file, assigned by pressing the button \"init\"."
#define HLP_INSTRUME	"Instrument associated to the calibration image."
#define HLP_CALIB_IMA	"Name of the calibration image, assigned in the search line process."
#define HLP_LINCAT	"Line Catalogue table name."
#define HLP_WRANG	"Wavelength limits of the Line Catalogue."
#define HLP_IMIN	"Minimum strength (intensity) limit of the Line Catalogue."
#define HLP_YSTART	"Starting line of the calibration image."


#define HLP_SMOOTH	"The size of the window in which the rows are averaged. It is used to \nincrease the signal to noise ratio of the lines before detection."
#define HLP_YRBR	"The step in row number used for lines searching. This step can be    \nincreased in order to fasten the calibration."
#define HLP_WIDTH	"Defines the full width (in pixels) of the comparison lines along the \ndispersion direction."
#define HLP_THRES	"Defines the threshold used for the automatic selection of lines in the\ncomparison spectrum."


#define HLP_REBIN_IN	"Image to rebin. You can select it from the set of images in the current\ndirectory, pressing the right button of the mouse."
#define HLP_REBIN_OUT	"Name of the output rebinned image."
#define HLP_REBIN_START	"Start value in wavelength units for the rebinned image."
#define HLP_REBIN_STEP	"Step value in wavelength units for the rebinned image."


#define HLP_CALIB_TOL	"Identification tolerance in wavelength units."
#define HLP_CALIB_FIT	"Fit degree for the calibration."


#define HLP_EXTIN_IN	"Input image for extinction. You can select it pressing the right mouse \nbutton."
#define HLP_EXTIN_AIRM	"Airmass of the extinction input image."
#define HLP_EXTIN_OUT	"Name of the output image of the extinction process."
#define HLP_EXTIN_TABLE	"Name of the extinction table with the columns WAVE (Angstrom) and LA_SILLA\n (magnitude/airmass). You can select it pressing the right mouse button."
#define HLP_INTEGR_IN	"Input image for integration. You can select pressing the right mouse   \nbutton."
#define HLP_FLUX_TABLE	"Name of the flux table with the columns: X_AXIS, FLUX and BIN_WIDTH.   \nYou can select it pressing the right mouse button."
#define HLP_RESP_OUT	"Name of the output image containing the response curve."
#define HLP_RESP_FIT	"Degree for the fitting of the response curve." 
#define HLP_RESP_SMO	"Smoothing factor in case of spline fitting."


#define HLP_EXTR_SKLIMS	"Starting and ending Y coordinates of the sky windows."
#define HLP_EXTR_OBLIMS	"Starting and ending Y coordinates of the object window."
#define HLP_EXTR_FIT	"Degree of the fit to be used in the sky computation."
#define HLP_EXTR_ITER	"Number of iterations in the extraction process."
#define HLP_EXTR_RON	"Read out noise of the CCD in electrons."
#define HLP_EXTR_GAIN	"Conversion factor from ADU to electrons (e-/ADU)."
#define HLP_EXTR_THRES	"Threshold used for the rejection of cosmic ray hits."
#define HLP_EXTR_RADIUS	"Radius used for cosmic rays removal."
#define HLP_EXTR_IN	"Input image for extraction. You can select it pressing the right mouse button."
#define HLP_EXTR_SKY	"name of the sky image (output of \"fit sky\", input of \"ext spec\")."
#define HLP_EXTR_OUT	"Name of the output image containing the extracted spectrum."
