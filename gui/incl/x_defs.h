/* @(#)x_defs.h	19.1 (ESO-IPG) 02/25/03 13:48:39 */
/* @(#)x_defs.h    1.0.0.0 (ESO-La Silla) 10/08/91 12:00:00 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        x_defs.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Contain extern declarations of variables and 
              functions, #includes, #defines and typedefs 
              related to X.
.KEYWORDS     X11 definitions.
.COMMENTS     Athena widgets implementation.
.VERSION 1.0  1-Mar-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

#include <stdio.h>
#include <sys/param.h>
#include <X11/Xos.h>
#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/Cardinals.h>
#include <X11/Xatom.h>
#include <X11/Shell.h>
#include <X11/Xaw/Paned.h>
#include <X11/Xaw/Grip.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/MenuButton.h>
#include <X11/Xaw/SimpleMenu.h>
#include <X11/Xaw/SmeBSB.h>
#include <X11/Xaw/AsciiText.h>
#include <X11/Xaw/TextP.h>
#include <X11/Xaw/TextSrc.h>
#include <X11/Xaw/Box.h>
#include <X11/Xaw/Dialog.h>
#include <X11/Xaw/List.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Viewport.h>
#include <X11/cursorfont.h>

/* incompatibility of header file with X11R4 documentation */
#define XawChainTop	XtChainTop
#define XawChainBottom	XtChainBottom
#define XawChainLeft	XtChainLeft
#define XawChainRight	XtChainRight
#define WidgetOf   	XtNameToWidget

#ifndef AssignMax
#define AssignMax(x, y) 	if ((y) > (x)) x = (y)
#endif
#ifndef AssignMin
#define AssignMin(x, y) 	if ((y) < (x)) x = (y)
#endif

#define	LASTCH(s)	(s[strlen(s)-1])
#define	SECLASTCH(s)	(s[strlen(s)-2])

#define LINESIZ         512		/* input line length */
#define MAXARGS 	20		/* max number of args */
#define MAXTEXT		10240		/* Max. lines in text window */

#ifdef SUNOS4
typedef struct dirent 	Directory;
#else
typedef struct direct 	Directory;
#endif

