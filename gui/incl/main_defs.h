/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        gm_defs.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Contain extern declarations of variables and 
              functions, #includes, #defines and typedefs 
              related to the graphical interface of Spectra.
.KEYWORDS     global variables, X11
.COMMENTS     Athena widgets implementation.
.VERSION 1.0  1-Mar-1991   Implementation
 
090423		last modif
------------------------------------------------------------*/

#ifndef GM_DEFS       /* === Just to avoid multiple includes === */
#define GM_DEFS


/* types of plots for response curve generation */
#define PLOT_RATIO_WAVE	0
#define PLOT_MAGN_WAVE	1
#define PLOT_MAGN_FREQ	2

/* label or no label the spectrum representing the starting line */
#define LABEL_SPEC	0
#define NO_LABEL	1

/* options for rebinning */
#define LINEAR		0
#define QUADRATIC	1
#define SPLINE		2

/* parameters for identification */
#define IDBEGIN		0
#define IDCONT		1

/* parameters of update_line_table() */
#define ADDMARK		0
#define DELMARK		1

/* for values in the :ERASED column in the line table */
#define VAL_ERASED	'-'
#define VAL_INVALID	'x'
#define VAL_OK		' '

extern float 	Rnull;		/* null values for Midas */
extern int 	Inull;
extern double 	Dnull;

/*
 * Application variables
 */
extern float   	Thres,   
        	Tol,      
        	Avdisp,
        	Wcenter,
        	Shift,
        	Alpha,
        	Imin,  
        	Maxdev;
extern char    	Instrume[],
        	Wlc[],   
        	Lincat[], 
        	Coerbr[], 
        	Guess[],
        	Seamtd[],
        	Wlcmtd[],
        	Rebmtd[],
        	Coropt[],
        	Corvisu[],
        	Twodopt[],
		Coorfil[],
		Session[];
extern int     	Ystart, 
        	Wrang[],
        	Ywidth, 
        	Width,   
        	Dcx[],    
        	Wlcniter[],
        	Ystep;
extern double  	Rebstrt,
        	Rebend,
        	Rebstp;

extern int Npix[];

/* Plot residual variables */
extern int RowResidual;
extern int AllResidual; /* boolean */

extern int TolPixels; /* boolean -- option menu for tolerance units */

extern int UpdateToggle;

/* flux calibration variables */
extern char	Extab[],
        	Fluxtab[],
        	Resptab[],
        	Plotyp[],
        	Fityp[],
        	Response[];
extern int	Fitd;
extern float	Smooth;

/* extraction variables */
extern char	Extmtd[];
extern int     	Skyord,
        	Uppsky[],
        	Lowsky[],
        	Order,
        	Radius,
        	Niter,
        	Objlim[],
		Skymod;
extern float   	Sigma,
        	Ron,
        	Gain;

extern int 	ListType;
extern int 	DialogType;

/* Batch reduction variables */
extern char    	Inputf[],
        	Outputf[],
        	Inpnumb[],
        	Bias[],
        	Dark[],
        	Flat[],
        	Redextab[],
        	Redresp[],
        	Redrbmtd[],
        	Longsess[],
        	Redsess[],
        	Biasopt[],
        	Darkopt[],
        	Flatopt[],
        	Rotopt[],
        	Trimopt[],
        	Rebopt[],
        	Extopt[],
        	Respopt[];
extern double	Rotstart,
		Rotstep;
extern int     	Trim[],
        	Outnumb;

extern char	Plotmode[];

/*************************************************************
  Definitions related to the addition of spectral lines
 *************************************************************/

extern int	NToAdd; 	/* current no. of spectral lines to add */
extern float	LinesToAdd[]; 	/* x-coords of lines to add */

#endif
