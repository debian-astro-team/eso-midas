/* @(#)StdResources.h	19.1 (ESO-IPG) 02/25/03 13:48:37 */

#ifdef DESIGN_TIME
#include <UimxResources.h>
#endif

#ifndef DESIGN_TIME

#define ShortLen  40
#define LongLen   120

char    TextFont[LongLen]      = {"-Adobe-helvetica-medium-r-normal--14*"};
char    BoldTextFont[LongLen]  = {"-Adobe-helvetica-bold-r-normal--14*"};
char    BigFont[LongLen]       = {"12x24"};
char    BoldBigFont[LongLen]   = {"12x24"};
char    SmallFont[LongLen]     = {"8x13"};
char    BoldSmallFont[LongLen] = {"8x13bold"};
char    SmallPropFont[LongLen] = {"-Adobe-helvetica-medium-r-normal--12*"};

char    TextForeground[ShortLen]   = {"Black"};
char    TextBackground[ShortLen]   = {"White"};
char    LabelBackground[ShortLen]  = {"Gray80"};
char    WindowBackground[ShortLen] = {"Gray80"};
char    ApplicBackground[ShortLen] = {"Gray80"};
char    SHelpBackground[ShortLen]  = {"FloralWhite"};
/* When running out of colors DeepSkyBlue3 is substituted by white, as for
   ForegGround, so the menu becomes invisible. I believe is better to use
   NavyBlue which always exist. CG
   char    MenuBackground[ShortLen]   = {"DeepSkyBlue3"};
*/
char    MenuBackground[ShortLen]   = {"NavyBlue"};
char    MenuForeground[ShortLen]   = {"White"};
char    ListBackground[ShortLen]   = {"Grey95"};
char    ListForeground[ShortLen]   = {"Black"};

char    ButtonBackground[ShortLen] = {"Gray70"};
char    ButtonForeground[ShortLen] = {"NavyBlue"};
char    CancelForeground[ShortLen] = {"yellow"};
char    ApplyForeground[ShortLen]  = {"OrangeRed2"};
char    SelectColor[ShortLen]      = {"green"};

char    ApplicGeometry[ShortLen]   = {"+20+340"};
char    ExtHelpGeometry[ShortLen]  = {"+20+340"};
char    SelBoxGeometry[ShortLen]   = {"+20+340"};

#endif
