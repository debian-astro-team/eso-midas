/*
 * IRSPEC GUI general definitions
 */

#define TOGGLE_STATE(x, y, z)	XmToggleButtonSetState(UxGetWidget(UxFindSwidget(x)), y, z)
#define SET_SENSITIVE(x, y)  	XtSetSensitive(UxGetWidget(UxFindSwidget(x)), y)
#define BADPIX_TABLE		"irsbadpix.tbl"

/*
 * IRSPEC GUI commands
 */
#define C_COPY_IMA 	"copy/ii "
#define C_LOAD_IMA 	"load/ima "
#define C_DISPLAY 	"make/display "
#define C_DEFINE 	"define/irspec "
#define C_BADPIX 	"badpix/irspec "
#define C_SKYSUB 	"skysub/irspec "
#define C_RECTIFY 	"rectify/irspec "
#define C_CALIB 	"calib/irspec "
#define C_STANDARD 	"standard/irspec "
#define C_RESPONSE 	"response/irspec "
#define C_MERGE 	"merge/irspec " 
#define C_SKYSUB 	"skysub/irspec "
#define C_FLAT	 	"flat/irspec "
#define C_FLUX	 	"flux/irspec "
#define C_EXTR_FIT_SKY  "@g irsskyfit "
#define C_EXTR_GCOOR	"@g irsgcoord "
#define C_EXTR_AVERAGE	"@g irsaver "
#define C_EXTR_WEIGHTED	"@g irsextr "
#define C_CLEAN		"@g irsclean "


/*
 * Commands for updating the ASTROMETRY keywords
 */
#define K_BADPIX	"set/irspec BADPIX = "
#define K_FLAT		"set/irspec FLAT = "
#define K_DARK		"set/irspec DARK = "

#define LIST_OBJECT		0
#define LIST_SKY		1
#define LIST_STSTAR		2
#define LIST_STSKY		3
#define LIST_DARK		4
#define LIST_FLAT		5
#define LIST_LOAD_IMA		6
#define LIST_EXT_WEIGHT		7
#define LIST_EXT_AVER		8
#define LIST_FLUX		9
#define LIST_RESP		10
#define LIST_OPEN		11
#define LIST_STD_REF		12
#define LIST_OBJ_REF		13
#define LIST_OBJ_FINPUT		14
#define LIST_FIT_SKY		15

#define DIALOG_FLAT		0
#define DIALOG_EXT_AVER		1
#define DIALOG_EXT_WEIGHT	2
#define DIALOG_SAVE_AS		3
