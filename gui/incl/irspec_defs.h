/* @(#)astro_defs.h    1.0.0.0 (ESO-La Silla) 10/08/91 12:00:00 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        irspec_defs.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Contain extern declarations of variables and 
              functions, #includes, #defines and typedefs 
              related to the graphical interface of Irspec.
.KEYWORDS     global variables, X11
.COMMENTS     
.VERSION 1.0  1-Nov-1993   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

extern float 	Rnull;		/* null values for Midas */
extern int 	Inull;
extern double 	Dnull;

/************************
 * Application variables
 ************************/

extern char	BadpixTable[];

extern int ListType;
extern int DialogType;

/** Bad pixel form variables **/
extern int BadpixMode;
extern int BadpixMethod;

/* Input files for the calibration */
extern char InObj[];
extern char InSky[];
extern char InStstar[];
extern char InStsky[];

extern char Flat[];
extern char Dark[];

extern char ParametersFile[];
