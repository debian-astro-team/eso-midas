/* @(#)do.h	19.1 (ESO-IPG) 02/25/03 13:48:37 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        do.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Michele Peron
.PURPOSE      definitions for the  DO
.KEYWORDS     application definitons.
.COMMENTS     
.VERSION 1.0  1-Mar-1994   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

#define MAXION	        72	/* max. length of a string in :IDENT column */
#define MAXVAL          1000

#define MAXCDESC	20
#define MAXIDENT	20
#define MAXNAME         60


typedef struct lc_table {
	int id; 		/* identifier of the table */
	char name[MAXNAME]; 	/* filename */
        int *row;               /* effective row number */
	char **ident;		/* values of the IDENT column */
	int nrows;		/* number of rows */
} LCTAB;

typedef struct lc_column {
	char **value;		/* row value */
	int nrows;		/* number of rows */
} LCCOL;

typedef struct lc_label {
	char **name;		/* row value */
	int ncol;		/* number of rows */
} LCLAB;

