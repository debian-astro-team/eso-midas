/* @(#)sp_inivals.h	19.1 (ESO-IPG) 02/25/03 13:48:38 */
/*
 * Initial values for the parameters of the forms 
 */

#define D_LINCAT	"hear"
#define D_WRANG		"0,9999"
#define D_IMIN		"90"
#define D_YSTART	"1"
#define D_YBIN		"0"
#define D_YRBR		"1"
#define D_WIDTH		"8"
#define D_THRES		"300.0"
#define D_TOL		"1.0"
#define D_FITD		"3"
