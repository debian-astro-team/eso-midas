/* @(#)spec_defs.h	19.1 (ESO-IPG) 02/25/03 13:48:38 */
/* @(#)sp_defs.h    1.0.0.0 (ESO-La Silla) 10/08/91 12:00:00 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        sp_defs.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      definitions for the application side of Spectra.
.KEYWORDS     application definitons.
.COMMENTS     
.VERSION 1.0  1-Mar-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

#define MAXION		20	/* max. length of a string in :ION column */
#define COL_WAVE	":WAVE"
#define COL_ION		":ION"
#define COL_INTENSITY	":INTENSITY"
#define COL_STRENGTH	":STRENGTH"

#define MAXDELTA	1000000.0 /* max. diff. between :X values */

#define MAXSTACK	30	/* max.size of stack of spectra */

#define MAXCDESC	20
#define MAXIDENT	20
#define LEFT_BUT	1
#define MID_BUT		32

/* for calibration processes */
#define CALLIN		0
#define CALFIT		1
#define CALRBR		2

typedef struct spec {
	char name[MAXNAME]; 	/* name of file without suffix */
	char ident[MAXIDENT]; 	/* identification of image */
	int id;		    	/* id of file */
	float x[MAXDATA], y[MAXDATA];
	float xmin, ymin, xmax, ymax;
	int row;		/* row of image associated to this spectrum */
	int ahead;		/* rows ahead to be averaged with 'row' */
	int ncols, nrows;
	float start, step;
} SPEC;

typedef struct sp_table {
	int id; 		/* identifier of the table */
	char name[MAXNAME]; 	/* filename */
	float **data;		/* values of the table (matrix form) */
	char **label;		/* labels of columns */
	int ncols;		/* number of columns */
	int nrows;		/* number of rows */
	int naw;		/* number of allocated words by row */
	int nar;		/* number of allocated rows */
} SPTAB;

typedef struct lc_table {
	int id; 		/* identifier of the table */
	char name[MAXNAME]; 	/* filename */
        int *row;               /* effective row number */
	float *wave;		/* values of the WAVE column */
	float *intens;		/* values of the INTENSITY column */
	char **ion;		/* values of the ION column */
	int nrows;		/* number of rows */
} LCTAB;

