/* @(#)ExternResources.h	19.1 (ESO-IPG) 02/25/03 13:48:37 */

#ifdef DESIGN_TIME
#include <UimxResources.h>
#endif

#ifndef DESIGN_TIME

#define ShortLen  40
#define LongLen   120

extern char    TextFont[];
extern char    BoldTextFont[];
extern char    BigFont[];
extern char    BoldBigFont[];
extern char    SmallFont[];
extern char    BoldSmallFont[];
extern char    SmallPropFont[];

extern char    TextForeground[];
extern char    TextBackground[];
extern char    LabelBackground[];
extern char    WindowBackground[];
extern char    ApplicBackground[];
extern char    SHelpBackground[];
extern char    MenuBackground[];
extern char    MenuForeground[];
extern char    ListBackground[];
extern char    ListForeground[];

extern char    ButtonBackground[];
extern char    ButtonForeground[];
extern char    CancelForeground[];
extern char    ApplyForeground[];
extern char    SelectColor[];

extern char    ApplicGeometry[];
extern char    ExtHelpGeometry[];
extern char    SelBoxGeometry[];

#endif
