/*
 * LONG context commands
 */
#define C_INIT 		"init/long "
#define C_SAVE 		"save/long "
#define C_CLEAN 	"clean/long "
#define C_SEARCH 	"search/long "
#define C_PLOT_SEARCH 	"plot/search "
#define C_IDENT 	"xident/long "

#define C_SELINE 	"@s lnident,seline "
#define C_SELLIN_ALL 	"sel/tab 'lintab' all"
#define C_SELCAT_ALL 	"sel/tab 'lincat' all"
#define C_NODEL 	"@s lnident,nodel "
#define C_CALIB_RBR 	"calib/long "
#define C_CALIB_RBR2 	"calib/twice "
#define C_CALIB_EDIT_D 	"plot/delta edit "
#define C_CALIB_EDIT_C 	"plot/calib edit "
#define C_CALIB_ERAS 	"erase/long "
#define C_CALIB_CURS 	"plot/calib cursor "
#define C_PLOT_DELTA 	"plot/delta "
#define C_PLOT_CALIB 	"plot/calib "
#define C_PLOT_IDENT 	"plot/ident "
#define C_PLOT_DISTOR 	"plot/distort "
#define C_PLOT_RESID 	"plot/residual "

#define C_REBIN 	"rebin/long "
#define C_APP_DISP 	"apply/disp "
#define C_RECTIFY 	"rectify/long "
#define C_PLOT_SPEC 	"plot/spec "

#define C_EXTR_GCOOR 	"gcoord/long "
#define C_EXTR_FIT_SKY	"skyfit/long "
#define C_EXTR_AVERAGE 	"extract/average "
#define C_EXTR_WEIGHTED "extract/long "

#define C_EXTIN 	"extin/long "
#define C_INTEGR 	"integr/long "
#define C_PLOT_RESP 	"plot/response "
#define C_FLUX_FIT 	"response/long "
#define C_CORRECT 	"calib/flux "
#define C_FLUX_FILT 	"response/filter "
#define C_PLOT_FLUX 	"plot/flux "
#define C_EDIT_FLUX 	"edit/flux "

#define C_GRAPHICS	"graph/long "
#define C_DISPLAY	"make/display "
#define C_LOAD_IMA	"load/long "

#define C_BATCHRED	"batch/long "
#define C_REDUCE	"reduce/long "
#define C_REDINIT	"reduce/init "
#define C_REDSAVE	"reduce/save "

/*
 * Commands for updating the LONG keywords
 */
#define K_INSTRUME	"set/long INSTRUME = "
#define K_WLC		"set/long WLC = "
#define K_WIDTH		"set/long WIDTH = "
#define K_THRES		"set/long THRES = "
#define K_SEAMTD	"set/long SEAMTD = "
#define K_YSTART	"set/long YSTART = "
#define K_YWIDTH	"set/long YWIDTH = "
#define K_YSTEP		"set/long YSTEP = "
#define K_WLCMTD	"set/long WLCMTD = "
#define K_TOL		"set/long TOL = "
#define K_FITD		"set/long FITD = "
#define K_DCX1		"set/long DCX = "
#define K_DCX2		"set/long DCX = ,"
#define K_LINTAB	"set/long LINTAB = "
#define K_COERBR	"set/long COERBR = "
#define K_SESSION	"set/long SESSION = "
#define K_LINCAT	"set/long LINCAT = "
#define K_WRANG1	"set/long WRANG = "
#define K_WRANG2	"set/long WRANG = ,"
#define K_IMIN		"set/long IMIN = "
#define K_ALPHA		"set/long ALPHA = "
#define K_WLCNITER1	"set/long WLCNITER = "
#define K_WLCNITER2	"set/long WLCNITER = ,"
#define K_MAXDEV	"set/long MAXDEV = "
#define K_GUESS		"set/long GUESS = "
#define K_COROPT	"set/long COROPT = "
#define K_CORVISU	"set/long CORVISU = "
#define K_SHIFT		"set/long SHIFT = "
#define K_WCENTER	"set/long WCENTER = "
#define K_AVDISP	"set/long AVDISP = "
#define K_TWODOPT	"set/long TWODOPT = "

#define K_REBMTD	"set/long REBMTD = "
#define K_REBSTRT	"set/long REBSTRT = "
#define K_REBEND	"set/long REBEND = "
#define K_REBSTP	"set/long REBSTP = "

#define K_EXTMTD	"set/long EXTMTD = "
#define K_OBJECT1	"set/long OBJECT = "
#define K_OBJECT2	"set/long OBJECT = ,"
#define K_LOWSKY1	"set/long LOWSKY = "
#define K_LOWSKY2	"set/long LOWSKY = ,"
#define K_UPPSKY1	"set/long UPPSKY = "
#define K_UPPSKY2	"set/long UPPSKY = ,"

#define K_RON		"set/long RON = "
#define K_GAIN		"set/long GAIN = "
#define K_SIGMA		"set/long SIGMA = "
#define K_RADIUS	"set/long RADIUS = "
#define K_SKYORD	"set/long SKYORD = "
#define K_SKYMOD	"set/long SKYMOD = "
#define K_ORDER		"set/long ORDER = "
#define K_NITER		"set/long NITER = "

#define K_COMET		"set/long COMET = "
#define K_FFIT		"set/long FFIT = "
#define K_FDEG		"set/long FDEG = "
#define K_FVISU		"set/long FVISU = "

#define K_RESPONSE	"set/long RESPONSE = "
#define K_RESPTAB	"set/long RESPTAB = "
#define K_EXTAB		"set/long EXTAB = "
#define K_STD		"set/long STD = "
#define K_FLUXTAB	"set/long FLUXTAB = "
#define K_FILTMED	"set/long FILTMED = "
#define K_FILTSMO	"set/long FILTSMO = "
#define K_FITD		"set/long FITD = "
#define K_PLOTYP	"set/long PLOTYP = "
#define K_FITYP		"set/long FITYP = "
#define K_RESPLOT	"set/long RESPLOT = "
#define K_SMOOTH	"set/long SMOOTH = "

#define K_NPIX		"set/long NPIX = "
#define K_START		"set/long START = "
#define K_STEP		"set/long STEP = "

#define LIST_WLC		0
#define LIST_REBIN_RBR		1
#define LIST_REBIN_2D		2
#define LIST_REBIN_TBL		3
#define LIST_EXTIN		4
#define LIST_INTEGR		5
#define LIST_FLUX_FILT		6
#define LIST_FIT_SKY		7
#define LIST_EXT_AVER		8
#define LIST_EXT_WEIGHT		9
#define LIST_LINCAT		10
#define LIST_GUESS		11
#define LIST_FLUX_TBL		12
#define LIST_EXTIN_TBL		13
#define LIST_SESSION		14
#define LIST_LOAD_IMA		15
#define LIST_BROWSER		16
#define LIST_CORRECT		17

#define DIALOG_REBIN_RBR	0
#define DIALOG_REBIN_2D		1
#define DIALOG_REBIN_TBL	2
#define DIALOG_SESSION		3
#define DIALOG_EXTIN		4
#define DIALOG_EXT_AVER		5
#define DIALOG_EXT_WEIGHT	6
#define DIALOG_CORRECT		7

/* Reduction form definitions */
#define K_INPUTF	"set/long INPUTF = "
#define K_INPNUMB	"set/long INPNUMB = "
#define K_OUTPUTF	"set/long OUTPUTF = "
#define K_OUTNUMB	"set/long OUTNUMB = "
#define K_ROTOPT	"set/long ROTOPT = "
#define K_ROTSTART	"set/long ROTSTART = "
#define K_ROTSTEP	"set/long ROTSTEP = "
#define K_TRIMOPT	"set/long TRIMOPT = "
#define K_TRIM1		"set/long TRIM = "
#define K_TRIM2		"set/long TRIM = ,"
#define K_TRIM3		"set/long TRIM = ,,"
#define K_TRIM4		"set/long TRIM = ,,,"
#define K_REBOPT	"set/long REBOPT = "
#define K_LONGSESS	"set/long SESSION = "
#define K_REDRBMTD	"set/long REBMTD = "
#define K_EXTOPT	"set/long EXTOPT = "
#define K_REDEXTAB	"set/long EXTAB = "
#define K_RESPOPT	"set/long RESPOPT = "
#define K_REDRESP	"set/long RESPONSE = "
#define K_BIASOPT	"set/long BIASOPT = "
#define K_DARKOPT	"set/long DARKOPT = "
#define K_FLATOPT	"set/long FLATOPT = "
#define K_DARK		"set/long DARK = "
#define K_FLAT		"set/long FLAT = "
#define K_BIAS		"set/long BIAS = "

#define LIST_REDSESS	0
#define LIST_INPUTF	1
#define LIST_BIAS	2
#define LIST_DARK	3
#define LIST_FLAT	4
#define LIST_LONGSESS	5
#define LIST_REDEXTAB	6
#define LIST_REDRESP	7

#define DIALOG_REDSESS	0
