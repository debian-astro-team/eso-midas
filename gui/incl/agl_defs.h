/* @(#)agl_defs.h	19.1 (ESO-IPG) 02/25/03 13:48:37 */
/* @(#)agl_defs.h    1.0.0.0 (ESO-La Silla) 10/08/91 12:00:00 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        agl_defs.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Definitions for applications using AGL routines.
.KEYWORDS     agl, plotting
.COMMENTS     
.VERSION 1.0  1-Mar-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

/* Devices for AG_VDEF */
#define DEV_ERASE	0
#define DEV_NO_ERASE	1

/* definitions for AG_GPLM (Draw polymarker) AGL routine */
#define BOX_MARKER	2
#define FBOX_MARKER	18
#define X_MARKER	5
#define VBAR_MARKER	12

/* definitions for AG_RGET (real information) AGL routine */
#define XMIN		0
#define XMAX		1
#define YMIN		2
#define YMAX		3

/* definitions for AG_SSET (Set plot) AGL routine */
#define LARGE_SYM	"SYLA"
#define DEF_SYM		"SYDI=1.0"
#define DEF_COLOR	"COLO=1"
#define RED_COLOR	"COLO=2"
#define GREEN_COLOR	"COLO=3"
#define BLUE_COLOR	"COLO=4"
#define DASH_LINE	"LSTYL=2"
#define SOLID_LINE	"LSTYL=0"
#define DU_TEXT		"BOTT"
#define UD_TEXT		"UPDO"
#define LR_TEXT		"LFRG"
#define RL_TEXT		"RGLF"
#define DEF_CHAR	"CHBA"
#define SMALL_CHAR	"CHSM"
#define MEDIUM_CHAR	"CHME"
#define LARGE_CHAR	"CHLA"
#define DEF_FONT	"FONT=0"
#define HIGH_FONT	"FONT=1"

#define MAXOPTS		512	/* max. length of options string */
