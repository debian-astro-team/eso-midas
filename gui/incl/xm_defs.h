/* @(#)xm_defs.h	19.1 (ESO-IPG) 02/25/03 13:48:39 */
/* @(#)x_defs.h    1.0.0.0 (ESO-La Silla) 10/08/91 12:00:00 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        xm_defs.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Contain extern declarations of variables and 
              functions, #includes, #defines and typedefs 
              related to X.
.KEYWORDS     X11 definitions.
.COMMENTS     Motif widgets implementation.
.VERSION 1.0  1-Mar-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

#include <stdio.h>
#include <X11/StringDefs.h>
#include <X11/Intrinsic.h>

#include <Xm/Frame.h>
#include <Xm/PushB.h>
#include <Xm/PushBG.h>
#include <Xm/Text.h>
#include <Xm/CascadeB.h>
#include <Xm/BulletinB.h>
#include <Xm/SelectioB.h>
#include <Xm/MessageB.h>
#include <Xm/RowColumn.h>
#include <Xm/Separator.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/List.h>
#include <Xm/Form.h>

#define LINESIZ         512		/* input line length */
#define MAXARGS 	20		/* max number of args */
#define MAXTEXT		10240		/* Max. lines in text window */

#define SET_LIST_TITLE(title)   \
        XtVaSetValues(UxGetWidget(FileListInterface), XmNtitle, title, NULL)

#define SET_DIALOG_TITLE(title)         \
        XtVaSetValues(UxGetWidget(UxFindSwidget("file_dialog")), XmNtitle, title, NULL)

#define SET_DIALOG_PROMPT(title)         {\
	XmString s = XmStringCreateSimple(title);\
        XtVaSetValues(UxGetWidget(UxFindSwidget("lb_file_dialog")), XmNlabelString, s, NULL);\
	XmStringFree(s); }

