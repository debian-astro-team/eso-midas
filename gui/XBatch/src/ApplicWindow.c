/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	ApplicWindow.c

.VERSION
 090819         last modif


*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxTextF.h"
#include "UxTogB.h"
#include "UxLabel.h"
#include "UxText.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxSep.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxApplicWindow;
	swidget	UxMainWindow;
	swidget	Uxmenu1;
	swidget	Uxmenu1_p1;
	swidget	Uxmenu1_p1_b1;
	swidget	Uxmenu1_p1_b2;
	swidget	Uxmenu1_p1_b3;
	swidget	Uxmenu1_p1_b4;
	swidget	Uxmenu1_p1_b5;
	swidget	Uxmenu1_top_b1;
	swidget	Uxmenu1_p4;
	swidget	Uxmenu_help_window;
	swidget	Uxmenu1_top_b4;
	swidget	Uxform6;
	swidget	Uxseparator1;
	swidget	Uxshort_help;
	swidget	Uxseparator2;
	swidget	Uxlabel2;
	swidget	Uxlabel3;
	swidget	Uxtg_biasopt;
	swidget	Uxtg_darkopt;
	swidget	Uxtg_flatopt;
	swidget	Uxtg_rebopt;
	swidget	Uxlabel8;
	swidget	UxrowColumn1;
	swidget	Uxtg_linear;
	swidget	Uxtg_quadratic;
	swidget	Uxtg_spline;
	swidget	Uxtg_respopt;
	swidget	Uxtg_extopt;
	swidget	Uxlabel9;
	swidget	Uxlabel10;
	swidget	Uxlabel11;
	swidget	Uxlabel12;
	swidget	Uxlabel13;
	swidget	Uxtg_rotopt;
	swidget	Uxtg_trimopt;
	swidget	Uxtf_inputf;
	swidget	Uxtf_inpnumb;
	swidget	Uxtf_bias;
	swidget	Uxtf_dark;
	swidget	Uxtf_flat;
	swidget	Uxtf_trim4;
	swidget	Uxtf_longsess;
	swidget	Uxtf_redextab;
	swidget	Uxtf_redresp;
	swidget	Uxtf_rotstart;
	swidget	Uxtf_outputf;
	swidget	Uxtf_outnumb;
	swidget	Uxlabel5;
	swidget	Uxtf_trim1;
	swidget	Uxtf_trim2;
	swidget	Uxtf_trim3;
	swidget	Uxlabel6;
	swidget	Uxlabel7;
	swidget	Uxlabel1;
	swidget	Uxform2;
	swidget	Uxpb_execute;
	swidget	Uxpb_airmass;
	swidget	Uxtf_rotstep;
} _UxCApplicWindow;

#define ApplicWindow            UxApplicWindowContext->UxApplicWindow
#define MainWindow              UxApplicWindowContext->UxMainWindow
#define menu1                   UxApplicWindowContext->Uxmenu1
#define menu1_p1                UxApplicWindowContext->Uxmenu1_p1
#define menu1_p1_b1             UxApplicWindowContext->Uxmenu1_p1_b1
#define menu1_p1_b2             UxApplicWindowContext->Uxmenu1_p1_b2
#define menu1_p1_b3             UxApplicWindowContext->Uxmenu1_p1_b3
#define menu1_p1_b4             UxApplicWindowContext->Uxmenu1_p1_b4
#define menu1_p1_b5             UxApplicWindowContext->Uxmenu1_p1_b5
#define menu1_top_b1            UxApplicWindowContext->Uxmenu1_top_b1
#define menu1_p4                UxApplicWindowContext->Uxmenu1_p4
#define menu_help_window        UxApplicWindowContext->Uxmenu_help_window
#define menu1_top_b4            UxApplicWindowContext->Uxmenu1_top_b4
#define form6                   UxApplicWindowContext->Uxform6
#define separator1              UxApplicWindowContext->Uxseparator1
#define short_help              UxApplicWindowContext->Uxshort_help
#define separator2              UxApplicWindowContext->Uxseparator2
#define label2                  UxApplicWindowContext->Uxlabel2
#define label3                  UxApplicWindowContext->Uxlabel3
#define tg_biasopt              UxApplicWindowContext->Uxtg_biasopt
#define tg_darkopt              UxApplicWindowContext->Uxtg_darkopt
#define tg_flatopt              UxApplicWindowContext->Uxtg_flatopt
#define tg_rebopt               UxApplicWindowContext->Uxtg_rebopt
#define label8                  UxApplicWindowContext->Uxlabel8
#define rowColumn1              UxApplicWindowContext->UxrowColumn1
#define tg_linear               UxApplicWindowContext->Uxtg_linear
#define tg_quadratic            UxApplicWindowContext->Uxtg_quadratic
#define tg_spline               UxApplicWindowContext->Uxtg_spline
#define tg_respopt              UxApplicWindowContext->Uxtg_respopt
#define tg_extopt               UxApplicWindowContext->Uxtg_extopt
#define label9                  UxApplicWindowContext->Uxlabel9
#define label10                 UxApplicWindowContext->Uxlabel10
#define label11                 UxApplicWindowContext->Uxlabel11
#define label12                 UxApplicWindowContext->Uxlabel12
#define label13                 UxApplicWindowContext->Uxlabel13
#define tg_rotopt               UxApplicWindowContext->Uxtg_rotopt
#define tg_trimopt              UxApplicWindowContext->Uxtg_trimopt
#define tf_inputf               UxApplicWindowContext->Uxtf_inputf
#define tf_inpnumb              UxApplicWindowContext->Uxtf_inpnumb
#define tf_bias                 UxApplicWindowContext->Uxtf_bias
#define tf_dark                 UxApplicWindowContext->Uxtf_dark
#define tf_flat                 UxApplicWindowContext->Uxtf_flat
#define tf_trim4                UxApplicWindowContext->Uxtf_trim4
#define tf_longsess             UxApplicWindowContext->Uxtf_longsess
#define tf_redextab             UxApplicWindowContext->Uxtf_redextab
#define tf_redresp              UxApplicWindowContext->Uxtf_redresp
#define tf_rotstart             UxApplicWindowContext->Uxtf_rotstart
#define tf_outputf              UxApplicWindowContext->Uxtf_outputf
#define tf_outnumb              UxApplicWindowContext->Uxtf_outnumb
#define label5                  UxApplicWindowContext->Uxlabel5
#define tf_trim1                UxApplicWindowContext->Uxtf_trim1
#define tf_trim2                UxApplicWindowContext->Uxtf_trim2
#define tf_trim3                UxApplicWindowContext->Uxtf_trim3
#define label6                  UxApplicWindowContext->Uxlabel6
#define label7                  UxApplicWindowContext->Uxlabel7
#define label1                  UxApplicWindowContext->Uxlabel1
#define form2                   UxApplicWindowContext->Uxform2
#define pb_execute              UxApplicWindowContext->Uxpb_execute
#define pb_airmass              UxApplicWindowContext->Uxpb_airmass
#define tf_rotstep              UxApplicWindowContext->Uxtf_rotstep

static _UxCApplicWindow	*UxApplicWindowContext;

extern void DisplayExtendedHelp(), DisplayShortHelp(), ExecuteReduce();
extern void WriteKeyword(), CreateInputCatalog();

extern int PopupList(), AppendDialogText(), MissingAirmassInCatalog();
extern int FillAirmassForm();

extern int SCSEPI();



/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*FileSelectMain = "#override\n\
<Btn3Down>:FileSelectACT()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ApplicWindow();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("short_help"), "");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayShortHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
		
	extern swidget TextFieldSwidget;
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_inputf")) )
	    PopupList(LIST_INPUTF);
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_bias")) )
	    PopupList(LIST_BIAS);
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_dark")) )
	    PopupList(LIST_DARK);
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flat")) )
	    PopupList(LIST_FLAT);
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_longsess")) )
	    PopupList(LIST_LONGSESS);
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_redextab")) )
	    PopupList(LIST_REDEXTAB);
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_redresp")) )
	    PopupList(LIST_REDRESP);
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_menu1_p1_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_REDSESS);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern char Redsess[];
	char command[256];
	
	sprintf(command, "%s%s", C_REDSAVE, Redsess);
	AppendDialogText(command);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern int DialogType;
	extern char Redsess[];
	
	SET_DIALOG_PROMPT("Output parameters table :");
	
	XmTextSetString(UxGetWidget(UxFindSwidget("tf_file_dialog")), Redsess);
	DialogType = DIALOG_REDSESS;
	UxPopupInterface(UxFindSwidget("file_dialog"), exclusive_grab);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	SCSEPI();
	exit(0);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_window( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_biasopt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("YES", K_BIASOPT);
	else
	    WriteKeyword("NO ", K_BIASOPT);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_darkopt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("YES", K_DARKOPT);
	else
	    WriteKeyword("NO ", K_DARKOPT);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_flatopt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("YES", K_FLATOPT);
	else
	    WriteKeyword("NO ", K_FLATOPT);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_rebopt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("YES", K_REBOPT);
	else
	    WriteKeyword("NO ", K_REBOPT);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_linear( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("LINEAR", K_REDRBMTD);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_quadratic( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	      WriteKeyword("QUADRATIC", K_REDRBMTD);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_spline( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	      WriteKeyword("SPLINE", K_REDRBMTD);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_respopt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("YES", K_RESPOPT);
	else
	    WriteKeyword("NO ", K_RESPOPT);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_extopt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("YES", K_EXTOPT);
	else
	    WriteKeyword("NO ", K_EXTOPT);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_rotopt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("YES", K_ROTOPT);
	else
	    WriteKeyword("NO ", K_ROTOPT);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_trimopt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	if ( XmToggleButtonGetState(UxWidget) )
	    WriteKeyword("YES", K_TRIMOPT);
	else
	    WriteKeyword("NO ", K_TRIMOPT);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_inputf( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Inputf[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Inputf) ) {
	    strcpy(Inputf, text);
	    WriteKeyword(text, K_INPUTF);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_inpnumb( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text, valok[256];
	int i, j = 0;
	extern char Inpnumb[];
	
	text = XmTextGetString(UxWidget);
	
	for ( i = 0; text[i] != '\0'; i++ ) 
	    if ( text[i] != ' ' && (text[i] == ',' || text[i] == '-' || 
	        (text[i] <= '9' && text[i] >= '0')) )
	        valok[j++] = text[i];
	valok[j] = '\0';
	
	if ( strcmp(valok, Inpnumb) ) {
	    strcpy(Inpnumb, valok);
	    WriteKeyword(valok, K_INPNUMB);
	    UxPutText(UxThisWidget, valok);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_bias( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Bias[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Bias) ) {
	    strcpy(Bias, text);
	    WriteKeyword(text, K_BIAS);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_dark( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Dark[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Dark) ) {
	    strcpy(Dark, text);
	    WriteKeyword(text, K_DARK);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_flat( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Flat[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Flat) ) {
	    strcpy(Flat, text);
	    WriteKeyword(text, K_FLAT);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_trim4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Trim[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Trim[3] ) {
	    Trim[3] = val;
	    WriteKeyword(text, K_TRIM4);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_longsess( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Longsess[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Longsess) ) {
	    strcpy(Longsess, text);
	    WriteKeyword(text, K_LONGSESS);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_redextab( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Redextab[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Redextab) ) {
	    strcpy(Redextab, text);
	    WriteKeyword(text, K_REDEXTAB);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_redresp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Redresp[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Redresp) ) {
	    strcpy(Redresp, text);
	    WriteKeyword(text, K_REDRESP);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_rotstart( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	double val;
	extern double Rotstart;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%lf", &val);
	
	if ( val != Rotstart ) {
	    Rotstart = val;
	    WriteKeyword(text, K_ROTSTART);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_outputf( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	extern char Outputf[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Outputf) ) {
	    strcpy(Outputf, text);
	    WriteKeyword(text, K_OUTPUTF);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_outnumb( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Outnumb;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Outnumb ) {
	    Outnumb = val;
	    WriteKeyword(text, K_OUTNUMB);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_trim1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Trim[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Trim[0] ) {
	    Trim[0] = val;
	    WriteKeyword(text, K_TRIM1);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_trim2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Trim[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Trim[1] ) {
	    Trim[1] = val;
	    WriteKeyword(text, K_TRIM2);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_trim3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Trim[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Trim[2] ) {
	    Trim[2] = val;
	    WriteKeyword(text, K_TRIM3);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_execute( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *prefix, *numbers;
	extern int ExecuteCaller;
		   
	prefix = XmTextFieldGetString(UxGetWidget(tf_inputf));
	numbers = XmTextFieldGetString(UxGetWidget(tf_inpnumb));
	CreateInputCatalog(prefix, numbers);
	
	if ( MissingAirmassInCatalog() ) {
	    if ( FillAirmassForm(UxGetWidget(UxFindSwidget("t_airmass_name")),
	                         UxGetWidget(UxFindSwidget("t_airmass_value")))) {
	        UxPopupInterface(UxFindSwidget("AirmassShell"), exclusive_grab);
	        ExecuteCaller = TRUE;
	    }
	}
	else
	    ExecuteReduce();
	
	XtFree(prefix);
	XtFree(numbers);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_airmass( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *prefix, *numbers;
	extern int ExecuteCaller;
	
	prefix = XmTextFieldGetString(UxGetWidget(tf_inputf));
	numbers = XmTextFieldGetString(UxGetWidget(tf_inpnumb));
	CreateInputCatalog(prefix, numbers);
	
	if ( FillAirmassForm(UxGetWidget(UxFindSwidget("t_airmass_name")),
	                     UxGetWidget(UxFindSwidget("t_airmass_value")))) {
	    ExecuteCaller = FALSE;
	    UxPopupInterface(UxFindSwidget("AirmassShell"), exclusive_grab);
	}
	
	XtFree(prefix);
	XtFree(numbers);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_rotstep( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	double val;
	extern double Rotstep;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%lf", &val);
	
	if ( val != Rotstep ) {
	    Rotstep = val;
	    WriteKeyword(text, K_ROTSTEP);
	}
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ApplicWindow()
{
	UxPutGeometry( ApplicWindow, "+0+0" );
	UxPutTitle( ApplicWindow, "Batch reduction" );
	UxPutKeyboardFocusPolicy( ApplicWindow, "pointer" );
	UxPutIconName( ApplicWindow, "Batch reduction" );
	UxPutHeight( ApplicWindow, 643 );
	UxPutWidth( ApplicWindow, 447 );
	UxPutY( ApplicWindow, 21 );
	UxPutX( ApplicWindow, 2 );

	UxPutBackground( MainWindow, WindowBackground );
	UxPutHeight( MainWindow, 460 );
	UxPutWidth( MainWindow, 440 );
	UxPutY( MainWindow, 0 );
	UxPutX( MainWindow, 10 );
	UxPutUnitType( MainWindow, "pixels" );

	UxPutBackground( menu1, MenuBackground );
	UxPutMenuAccelerator( menu1, "<KeyUp>F10" );
	UxPutRowColumnType( menu1, "menu_bar" );

	UxPutForeground( menu1_p1, MenuForeground );
	UxPutBackground( menu1_p1, MenuBackground );
	UxPutRowColumnType( menu1_p1, "menu_pulldown" );

	UxPutFontList( menu1_p1_b1, BoldTextFont );
	UxPutMnemonic( menu1_p1_b1, "O" );
	UxPutLabelString( menu1_p1_b1, "Open" );

	UxPutFontList( menu1_p1_b2, BoldTextFont );
	UxPutMnemonic( menu1_p1_b2, "S" );
	UxPutLabelString( menu1_p1_b2, "Save" );

	UxPutFontList( menu1_p1_b3, BoldTextFont );
	UxPutMnemonic( menu1_p1_b3, "S" );
	UxPutLabelString( menu1_p1_b3, "Save As ..." );

	UxPutFontList( menu1_p1_b5, BoldTextFont );
	UxPutAccelerator( menu1_p1_b5, "E" );
	UxPutLabelString( menu1_p1_b5, "Exit" );

	UxPutForeground( menu1_top_b1, MenuForeground );
	UxPutFontList( menu1_top_b1, BoldTextFont );
	UxPutBackground( menu1_top_b1, MenuBackground );
	UxPutMnemonic( menu1_top_b1, "F" );
	UxPutLabelString( menu1_top_b1, "File" );

	UxPutForeground( menu1_p4, MenuForeground );
	UxPutBackground( menu1_p4, MenuBackground );
	UxPutRowColumnType( menu1_p4, "menu_pulldown" );

	UxPutFontList( menu_help_window, BoldTextFont );
	UxPutMnemonic( menu_help_window, "W" );
	UxPutLabelString( menu_help_window, "On Window ..." );

	UxPutForeground( menu1_top_b4, MenuForeground );
	UxPutFontList( menu1_top_b4, BoldTextFont );
	UxPutBackground( menu1_top_b4, MenuBackground );
	UxPutMnemonic( menu1_top_b4, "H" );
	UxPutLabelString( menu1_top_b4, "Help" );

	UxPutBackground( form6, WindowBackground );

	UxPutBackground( separator1, LabelBackground );
	UxPutHeight( separator1, 6 );
	UxPutWidth( separator1, 450 );
	UxPutY( separator1, 496 );
	UxPutX( separator1, -4 );

	UxPutFontList( short_help, TextFont );
	UxPutEditable( short_help, "false" );
	UxPutCursorPositionVisible( short_help, "false" );
	UxPutBackground( short_help, SHelpBackground );
	UxPutHeight( short_help, 50 );
	UxPutWidth( short_help, 448 );
	UxPutY( short_help, 502 );
	UxPutX( short_help, -2 );

	UxPutBackground( separator2, LabelBackground );
	UxPutHeight( separator2, 6 );
	UxPutWidth( separator2, 450 );
	UxPutY( separator2, 552 );
	UxPutX( separator2, -4 );

	UxPutForeground( label2, TextForeground );
	UxPutFontList( label2, TextFont );
	UxPutBackground( label2, ApplicBackground );
	UxPutLabelString( label2, "Prefix  /" );
	UxPutHeight( label2, 20 );
	UxPutWidth( label2, 54 );
	UxPutY( label2, 38 );
	UxPutX( label2, 10 );

	UxPutForeground( label3, TextForeground );
	UxPutFontList( label3, TextFont );
	UxPutBackground( label3, ApplicBackground );
	UxPutLabelString( label3, "Numbers :" );
	UxPutHeight( label3, 25 );
	UxPutWidth( label3, 70 );
	UxPutY( label3, 36 );
	UxPutX( label3, 238 );

	UxPutForeground( tg_biasopt, TextForeground );
	UxPutHighlightOnEnter( tg_biasopt, "true" );
	UxPutIndicatorSize( tg_biasopt, 18 );
	UxPutSelectColor( tg_biasopt, "green" );
	UxPutHighlightColor( tg_biasopt, "black" );
	UxPutSet( tg_biasopt, "false" );
	UxPutFontList( tg_biasopt, TextFont );
	UxPutAlignment( tg_biasopt, "alignment_beginning" );
	UxPutBackground( tg_biasopt, ApplicBackground );
	UxPutLabelString( tg_biasopt, "Bias" );
	UxPutHeight( tg_biasopt, 30 );
	UxPutWidth( tg_biasopt, 68 );
	UxPutY( tg_biasopt, 72 );
	UxPutX( tg_biasopt, 16 );

	UxPutForeground( tg_darkopt, TextForeground );
	UxPutHighlightOnEnter( tg_darkopt, "true" );
	UxPutIndicatorSize( tg_darkopt, 18 );
	UxPutSelectColor( tg_darkopt, "green" );
	UxPutHighlightColor( tg_darkopt, "black" );
	UxPutSet( tg_darkopt, "false" );
	UxPutFontList( tg_darkopt, TextFont );
	UxPutAlignment( tg_darkopt, "alignment_beginning" );
	UxPutBackground( tg_darkopt, ApplicBackground );
	UxPutLabelString( tg_darkopt, "Dark" );
	UxPutHeight( tg_darkopt, 30 );
	UxPutWidth( tg_darkopt, 68 );
	UxPutY( tg_darkopt, 112 );
	UxPutX( tg_darkopt, 16 );

	UxPutForeground( tg_flatopt, TextForeground );
	UxPutHighlightOnEnter( tg_flatopt, "true" );
	UxPutIndicatorSize( tg_flatopt, 18 );
	UxPutSelectColor( tg_flatopt, "green" );
	UxPutHighlightColor( tg_flatopt, "black" );
	UxPutSet( tg_flatopt, "false" );
	UxPutFontList( tg_flatopt, TextFont );
	UxPutAlignment( tg_flatopt, "alignment_beginning" );
	UxPutBackground( tg_flatopt, ApplicBackground );
	UxPutLabelString( tg_flatopt, "Flat field" );
	UxPutHeight( tg_flatopt, 30 );
	UxPutWidth( tg_flatopt, 86 );
	UxPutY( tg_flatopt, 152 );
	UxPutX( tg_flatopt, 16 );

	UxPutForeground( tg_rebopt, TextForeground );
	UxPutHighlightOnEnter( tg_rebopt, "true" );
	UxPutIndicatorSize( tg_rebopt, 18 );
	UxPutSelectColor( tg_rebopt, "green" );
	UxPutHighlightColor( tg_rebopt, "black" );
	UxPutFontList( tg_rebopt, TextFont );
	UxPutAlignment( tg_rebopt, "alignment_beginning" );
	UxPutBackground( tg_rebopt, ApplicBackground );
	UxPutLabelString( tg_rebopt, "Rebin" );
	UxPutSet( tg_rebopt, "false" );
	UxPutHeight( tg_rebopt, 30 );
	UxPutWidth( tg_rebopt, 70 );
	UxPutY( tg_rebopt, 287 );
	UxPutX( tg_rebopt, 16 );

	UxPutForeground( label8, TextForeground );
	UxPutFontList( label8, TextFont );
	UxPutBackground( label8, ApplicBackground );
	UxPutLabelString( label8, "Parameters table :" );
	UxPutHeight( label8, 25 );
	UxPutWidth( label8, 128 );
	UxPutY( label8, 280 );
	UxPutX( label8, 104 );

	UxPutPacking( rowColumn1, "pack_tight" );
	UxPutBackground( rowColumn1, ApplicBackground );
	UxPutRadioBehavior( rowColumn1, "true" );
	UxPutOrientation( rowColumn1, "horizontal" );
	UxPutHeight( rowColumn1, 34 );
	UxPutWidth( rowColumn1, 318 );
	UxPutY( rowColumn1, 310 );
	UxPutX( rowColumn1, 106 );

	UxPutForeground( tg_linear, TextForeground );
	UxPutHighlightOnEnter( tg_linear, "true" );
	UxPutSelectColor( tg_linear, "green" );
	UxPutFontList( tg_linear, TextFont );
	UxPutBackground( tg_linear, ApplicBackground );
	UxPutSet( tg_linear, "true" );
	UxPutLabelString( tg_linear, "LINEAR" );
	UxPutIndicatorType( tg_linear, "one_of_many" );
	UxPutHeight( tg_linear, 21 );
	UxPutWidth( tg_linear, 67 );
	UxPutY( tg_linear, 3 );
	UxPutX( tg_linear, 3 );

	UxPutForeground( tg_quadratic, TextForeground );
	UxPutHighlightOnEnter( tg_quadratic, "true" );
	UxPutSelectColor( tg_quadratic, "green" );
	UxPutFontList( tg_quadratic, TextFont );
	UxPutBackground( tg_quadratic, ApplicBackground );
	UxPutLabelString( tg_quadratic, "CUADRATIC" );
	UxPutIndicatorType( tg_quadratic, "one_of_many" );
	UxPutHeight( tg_quadratic, 25 );
	UxPutWidth( tg_quadratic, 89 );
	UxPutY( tg_quadratic, 0 );
	UxPutX( tg_quadratic, 75 );

	UxPutForeground( tg_spline, TextForeground );
	UxPutHighlightOnEnter( tg_spline, "true" );
	UxPutSelectColor( tg_spline, "green" );
	UxPutFontList( tg_spline, TextFont );
	UxPutBackground( tg_spline, ApplicBackground );
	UxPutLabelString( tg_spline, "SPLINE" );
	UxPutIndicatorType( tg_spline, "one_of_many" );
	UxPutHeight( tg_spline, 28 );
	UxPutWidth( tg_spline, 89 );
	UxPutY( tg_spline, 3 );
	UxPutX( tg_spline, 235 );

	UxPutForeground( tg_respopt, TextForeground );
	UxPutHighlightOnEnter( tg_respopt, "true" );
	UxPutIndicatorSize( tg_respopt, 18 );
	UxPutSelectColor( tg_respopt, "green" );
	UxPutHighlightColor( tg_respopt, "black" );
	UxPutSet( tg_respopt, "false" );
	UxPutFontList( tg_respopt, TextFont );
	UxPutAlignment( tg_respopt, "alignment_beginning" );
	UxPutBackground( tg_respopt, ApplicBackground );
	UxPutLabelString( tg_respopt, "Flux" );
	UxPutHeight( tg_respopt, 28 );
	UxPutWidth( tg_respopt, 65 );
	UxPutY( tg_respopt, 391 );
	UxPutX( tg_respopt, 16 );

	UxPutForeground( tg_extopt, TextForeground );
	UxPutHighlightOnEnter( tg_extopt, "true" );
	UxPutIndicatorSize( tg_extopt, 18 );
	UxPutSelectColor( tg_extopt, "green" );
	UxPutHighlightColor( tg_extopt, "black" );
	UxPutSet( tg_extopt, "false" );
	UxPutFontList( tg_extopt, TextFont );
	UxPutAlignment( tg_extopt, "alignment_beginning" );
	UxPutBackground( tg_extopt, ApplicBackground );
	UxPutLabelString( tg_extopt, "Extinct" );
	UxPutHeight( tg_extopt, 31 );
	UxPutWidth( tg_extopt, 85 );
	UxPutY( tg_extopt, 350 );
	UxPutX( tg_extopt, 16 );

	UxPutForeground( label9, TextForeground );
	UxPutFontList( label9, TextFont );
	UxPutBackground( label9, ApplicBackground );
	UxPutLabelString( label9, "Response curve :" );
	UxPutHeight( label9, 20 );
	UxPutWidth( label9, 115 );
	UxPutY( label9, 397 );
	UxPutX( label9, 108 );

	UxPutForeground( label10, TextForeground );
	UxPutFontList( label10, TextFont );
	UxPutBackground( label10, ApplicBackground );
	UxPutLabelString( label10, "Extinct table :" );
	UxPutHeight( label10, 20 );
	UxPutWidth( label10, 98 );
	UxPutY( label10, 356 );
	UxPutX( label10, 104 );

	UxPutForeground( label11, TextForeground );
	UxPutFontList( label11, TextFont );
	UxPutBackground( label11, ApplicBackground );
	UxPutLabelString( label11, "OUTPUT FILES" );
	UxPutHeight( label11, 20 );
	UxPutWidth( label11, 123 );
	UxPutY( label11, 430 );
	UxPutX( label11, 0 );

	UxPutForeground( label12, TextForeground );
	UxPutFontList( label12, TextFont );
	UxPutBackground( label12, ApplicBackground );
	UxPutLabelString( label12, "Prefix :" );
	UxPutHeight( label12, 20 );
	UxPutWidth( label12, 59 );
	UxPutY( label12, 460 );
	UxPutX( label12, 10 );

	UxPutForeground( label13, TextForeground );
	UxPutFontList( label13, TextFont );
	UxPutBackground( label13, ApplicBackground );
	UxPutLabelString( label13, "Initial file # :" );
	UxPutHeight( label13, 20 );
	UxPutWidth( label13, 81 );
	UxPutY( label13, 460 );
	UxPutX( label13, 191 );

	UxPutForeground( tg_rotopt, TextForeground );
	UxPutHighlightOnEnter( tg_rotopt, "true" );
	UxPutIndicatorSize( tg_rotopt, 18 );
	UxPutSelectColor( tg_rotopt, "green" );
	UxPutHighlightColor( tg_rotopt, "black" );
	UxPutSet( tg_rotopt, "false" );
	UxPutFontList( tg_rotopt, TextFont );
	UxPutAlignment( tg_rotopt, "alignment_beginning" );
	UxPutBackground( tg_rotopt, ApplicBackground );
	UxPutLabelString( tg_rotopt, "Rotate" );
	UxPutHeight( tg_rotopt, 30 );
	UxPutWidth( tg_rotopt, 72 );
	UxPutY( tg_rotopt, 194 );
	UxPutX( tg_rotopt, 16 );

	UxPutForeground( tg_trimopt, TextForeground );
	UxPutHighlightOnEnter( tg_trimopt, "true" );
	UxPutIndicatorSize( tg_trimopt, 18 );
	UxPutSelectColor( tg_trimopt, "green" );
	UxPutHighlightColor( tg_trimopt, "black" );
	UxPutSet( tg_trimopt, "false" );
	UxPutFontList( tg_trimopt, TextFont );
	UxPutAlignment( tg_trimopt, "alignment_beginning" );
	UxPutBackground( tg_trimopt, ApplicBackground );
	UxPutLabelString( tg_trimopt, "Trim" );
	UxPutHeight( tg_trimopt, 26 );
	UxPutWidth( tg_trimopt, 62 );
	UxPutY( tg_trimopt, 234 );
	UxPutX( tg_trimopt, 16 );

	UxPutForeground( tf_inputf, TextForeground );
	UxPutHighlightOnEnter( tf_inputf, "true" );
	UxPutColumns( tf_inputf, 128 );
	UxPutMaxLength( tf_inputf, 15 );
	UxPutTranslations( tf_inputf, FileSelectMain );
	UxPutFontList( tf_inputf, TextFont );
	UxPutBackground( tf_inputf, TextBackground );
	UxPutHeight( tf_inputf, 34 );
	UxPutWidth( tf_inputf, 104 );
	UxPutY( tf_inputf, 32 );
	UxPutX( tf_inputf, 134 );

	UxPutForeground( tf_inpnumb, TextForeground );
	UxPutHighlightOnEnter( tf_inpnumb, "true" );
	UxPutColumns( tf_inpnumb, 128 );
	UxPutMaxLength( tf_inpnumb, 18 );
	UxPutFontList( tf_inpnumb, TextFont );
	UxPutBackground( tf_inpnumb, TextBackground );
	UxPutHeight( tf_inpnumb, 34 );
	UxPutWidth( tf_inpnumb, 128 );
	UxPutY( tf_inpnumb, 32 );
	UxPutX( tf_inpnumb, 308 );

	UxPutColumns( tf_bias, 128 );
	UxPutForeground( tf_bias, TextForeground );
	UxPutHighlightOnEnter( tf_bias, "true" );
	UxPutMaxLength( tf_bias, 20 );
	UxPutTranslations( tf_bias, FileSelectMain );
	UxPutFontList( tf_bias, TextFont );
	UxPutBackground( tf_bias, TextBackground );
	UxPutHeight( tf_bias, 34 );
	UxPutWidth( tf_bias, 178 );
	UxPutY( tf_bias, 74 );
	UxPutX( tf_bias, 104 );

	UxPutColumns( tf_dark, 128 );
	UxPutForeground( tf_dark, TextForeground );
	UxPutHighlightOnEnter( tf_dark, "true" );
	UxPutMaxLength( tf_dark, 20 );
	UxPutTranslations( tf_dark, FileSelectMain );
	UxPutFontList( tf_dark, TextFont );
	UxPutBackground( tf_dark, TextBackground );
	UxPutHeight( tf_dark, 34 );
	UxPutWidth( tf_dark, 178 );
	UxPutY( tf_dark, 110 );
	UxPutX( tf_dark, 104 );

	UxPutColumns( tf_flat, 128 );
	UxPutForeground( tf_flat, TextForeground );
	UxPutHighlightOnEnter( tf_flat, "true" );
	UxPutMaxLength( tf_flat, 20 );
	UxPutTranslations( tf_flat, FileSelectMain );
	UxPutFontList( tf_flat, TextFont );
	UxPutBackground( tf_flat, TextBackground );
	UxPutHeight( tf_flat, 34 );
	UxPutWidth( tf_flat, 178 );
	UxPutY( tf_flat, 148 );
	UxPutX( tf_flat, 104 );

	UxPutForeground( tf_trim4, TextForeground );
	UxPutHighlightOnEnter( tf_trim4, "true" );
	UxPutMaxLength( tf_trim4, 20 );
	UxPutFontList( tf_trim4, TextFont );
	UxPutBackground( tf_trim4, TextBackground );
	UxPutHeight( tf_trim4, 34 );
	UxPutWidth( tf_trim4, 60 );
	UxPutY( tf_trim4, 230 );
	UxPutX( tf_trim4, 374 );

	UxPutColumns( tf_longsess, 128 );
	UxPutForeground( tf_longsess, TextForeground );
	UxPutHighlightOnEnter( tf_longsess, "true" );
	UxPutMaxLength( tf_longsess, 20 );
	UxPutTranslations( tf_longsess, FileSelectMain );
	UxPutFontList( tf_longsess, TextFont );
	UxPutBackground( tf_longsess, TextBackground );
	UxPutHeight( tf_longsess, 34 );
	UxPutWidth( tf_longsess, 178 );
	UxPutY( tf_longsess, 276 );
	UxPutX( tf_longsess, 232 );

	UxPutColumns( tf_redextab, 128 );
	UxPutForeground( tf_redextab, TextForeground );
	UxPutHighlightOnEnter( tf_redextab, "true" );
	UxPutMaxLength( tf_redextab, 20 );
	UxPutText( tf_redextab, "" );
	UxPutTranslations( tf_redextab, FileSelectMain );
	UxPutFontList( tf_redextab, TextFont );
	UxPutBackground( tf_redextab, TextBackground );
	UxPutHeight( tf_redextab, 34 );
	UxPutWidth( tf_redextab, 178 );
	UxPutY( tf_redextab, 350 );
	UxPutX( tf_redextab, 232 );

	UxPutColumns( tf_redresp, 128 );
	UxPutForeground( tf_redresp, TextForeground );
	UxPutHighlightOnEnter( tf_redresp, "true" );
	UxPutMaxLength( tf_redresp, 20 );
	UxPutTranslations( tf_redresp, FileSelectMain );
	UxPutFontList( tf_redresp, TextFont );
	UxPutBackground( tf_redresp, TextBackground );
	UxPutHeight( tf_redresp, 34 );
	UxPutWidth( tf_redresp, 178 );
	UxPutY( tf_redresp, 390 );
	UxPutX( tf_redresp, 232 );

	UxPutForeground( tf_rotstart, TextForeground );
	UxPutHighlightOnEnter( tf_rotstart, "true" );
	UxPutMaxLength( tf_rotstart, 15 );
	UxPutColumns( tf_rotstart, 15 );
	UxPutFontList( tf_rotstart, TextFont );
	UxPutBackground( tf_rotstart, TextBackground );
	UxPutHeight( tf_rotstart, 34 );
	UxPutWidth( tf_rotstart, 72 );
	UxPutY( tf_rotstart, 190 );
	UxPutX( tf_rotstart, 218 );

	UxPutForeground( tf_outputf, TextForeground );
	UxPutHighlightOnEnter( tf_outputf, "true" );
	UxPutMaxLength( tf_outputf, 10 );
	UxPutColumns( tf_outputf, 128 );
	UxPutFontList( tf_outputf, TextFont );
	UxPutBackground( tf_outputf, TextBackground );
	UxPutHeight( tf_outputf, 34 );
	UxPutWidth( tf_outputf, 100 );
	UxPutY( tf_outputf, 452 );
	UxPutX( tf_outputf, 74 );

	UxPutForeground( tf_outnumb, TextForeground );
	UxPutHighlightOnEnter( tf_outnumb, "true" );
	UxPutMaxLength( tf_outnumb, 6 );
	UxPutColumns( tf_outnumb, 6 );
	UxPutText( tf_outnumb, "" );
	UxPutFontList( tf_outnumb, TextFont );
	UxPutBackground( tf_outnumb, TextBackground );
	UxPutHeight( tf_outnumb, 34 );
	UxPutWidth( tf_outnumb, 67 );
	UxPutY( tf_outnumb, 452 );
	UxPutX( tf_outnumb, 276 );

	UxPutForeground( label5, TextForeground );
	UxPutLabelString( label5, "Catalog :" );
	UxPutFontList( label5, TextFont );
	UxPutBackground( label5, ApplicBackground );
	UxPutHeight( label5, 20 );
	UxPutWidth( label5, 68 );
	UxPutY( label5, 38 );
	UxPutX( label5, 64 );

	UxPutForeground( tf_trim1, TextForeground );
	UxPutHighlightOnEnter( tf_trim1, "true" );
	UxPutMaxLength( tf_trim1, 20 );
	UxPutFontList( tf_trim1, TextFont );
	UxPutBackground( tf_trim1, TextBackground );
	UxPutHeight( tf_trim1, 34 );
	UxPutWidth( tf_trim1, 60 );
	UxPutY( tf_trim1, 230 );
	UxPutX( tf_trim1, 194 );

	UxPutForeground( tf_trim2, TextForeground );
	UxPutHighlightOnEnter( tf_trim2, "true" );
	UxPutMaxLength( tf_trim2, 20 );
	UxPutFontList( tf_trim2, TextFont );
	UxPutBackground( tf_trim2, TextBackground );
	UxPutHeight( tf_trim2, 34 );
	UxPutWidth( tf_trim2, 60 );
	UxPutY( tf_trim2, 230 );
	UxPutX( tf_trim2, 254 );

	UxPutForeground( tf_trim3, TextForeground );
	UxPutHighlightOnEnter( tf_trim3, "true" );
	UxPutMaxLength( tf_trim3, 20 );
	UxPutFontList( tf_trim3, TextFont );
	UxPutBackground( tf_trim3, TextBackground );
	UxPutHeight( tf_trim3, 34 );
	UxPutWidth( tf_trim3, 60 );
	UxPutY( tf_trim3, 230 );
	UxPutX( tf_trim3, 314 );

	UxPutForeground( label6, TextForeground );
	UxPutFontList( label6, TextFont );
	UxPutBackground( label6, ApplicBackground );
	UxPutLabelString( label6, "x1,y1,x2,y2 :" );
	UxPutHeight( label6, 25 );
	UxPutWidth( label6, 84 );
	UxPutY( label6, 236 );
	UxPutX( label6, 106 );

	UxPutForeground( label7, TextForeground );
	UxPutFontList( label7, TextFont );
	UxPutBackground( label7, ApplicBackground );
	UxPutLabelString( label7, "start-y,step-y :" );
	UxPutHeight( label7, 25 );
	UxPutWidth( label7, 109 );
	UxPutY( label7, 198 );
	UxPutX( label7, 104 );

	UxPutForeground( label1, TextForeground );
	UxPutFontList( label1, TextFont );
	UxPutBackground( label1, ApplicBackground );
	UxPutLabelString( label1, "INPUT FILES" );
	UxPutHeight( label1, 20 );
	UxPutWidth( label1, 105 );
	UxPutY( label1, 10 );
	UxPutX( label1, 0 );

	UxPutBackground( form2, ButtonBackground );
	UxPutHeight( form2, 48 );
	UxPutWidth( form2, 448 );
	UxPutY( form2, 559 );
	UxPutX( form2, 0 );
	UxPutResizePolicy( form2, "resize_none" );

	UxPutHighlightColor( pb_execute, ButtonForeground );
	UxPutHighlightOnEnter( pb_execute, "true" );
	UxPutLabelString( pb_execute, "Apply" );
	UxPutBackground( pb_execute, ButtonBackground );
	UxPutForeground( pb_execute, ApplyForeground );
	UxPutFontList( pb_execute, BoldTextFont );
	UxPutHeight( pb_execute, 35 );
	UxPutWidth( pb_execute, 85 );
	UxPutY( pb_execute, 6 );
	UxPutX( pb_execute, 107 );

	UxPutHighlightColor( pb_airmass, ButtonForeground );
	UxPutHighlightOnEnter( pb_airmass, "true" );
	UxPutLabelString( pb_airmass, "Airmass ..." );
	UxPutBackground( pb_airmass, ButtonBackground );
	UxPutForeground( pb_airmass, ButtonForeground );
	UxPutFontList( pb_airmass, BoldTextFont );
	UxPutHeight( pb_airmass, 35 );
	UxPutWidth( pb_airmass, 85 );
	UxPutY( pb_airmass, 6 );
	UxPutX( pb_airmass, 8 );

	UxPutForeground( tf_rotstep, TextForeground );
	UxPutHighlightOnEnter( tf_rotstep, "true" );
	UxPutMaxLength( tf_rotstep, 15 );
	UxPutColumns( tf_rotstep, 15 );
	UxPutFontList( tf_rotstep, TextFont );
	UxPutBackground( tf_rotstep, TextBackground );
	UxPutHeight( tf_rotstep, 34 );
	UxPutWidth( tf_rotstep, 72 );
	UxPutY( tf_rotstep, 190 );
	UxPutX( tf_rotstep, 290 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ApplicWindow()
{
	/* Create the swidgets */

	ApplicWindow = UxCreateApplicationShell( "ApplicWindow", NO_PARENT );
	UxPutContext( ApplicWindow, UxApplicWindowContext );

	MainWindow = UxCreateMainWindow( "MainWindow", ApplicWindow );
	menu1 = UxCreateRowColumn( "menu1", MainWindow );
	menu1_p1 = UxCreateRowColumn( "menu1_p1", menu1 );
	menu1_p1_b1 = UxCreatePushButtonGadget( "menu1_p1_b1", menu1_p1 );
	menu1_p1_b2 = UxCreatePushButtonGadget( "menu1_p1_b2", menu1_p1 );
	menu1_p1_b3 = UxCreatePushButtonGadget( "menu1_p1_b3", menu1_p1 );
	menu1_p1_b4 = UxCreateSeparator( "menu1_p1_b4", menu1_p1 );
	menu1_p1_b5 = UxCreatePushButtonGadget( "menu1_p1_b5", menu1_p1 );
	menu1_top_b1 = UxCreateCascadeButton( "menu1_top_b1", menu1 );
	menu1_p4 = UxCreateRowColumn( "menu1_p4", menu1 );
	menu_help_window = UxCreatePushButtonGadget( "menu_help_window", menu1_p4 );
	menu1_top_b4 = UxCreateCascadeButton( "menu1_top_b4", menu1 );
	form6 = UxCreateForm( "form6", MainWindow );
	separator1 = UxCreateSeparator( "separator1", form6 );
	short_help = UxCreateText( "short_help", form6 );
	separator2 = UxCreateSeparator( "separator2", form6 );
	label2 = UxCreateLabel( "label2", form6 );
	label3 = UxCreateLabel( "label3", form6 );
	tg_biasopt = UxCreateToggleButton( "tg_biasopt", form6 );
	tg_darkopt = UxCreateToggleButton( "tg_darkopt", form6 );
	tg_flatopt = UxCreateToggleButton( "tg_flatopt", form6 );
	tg_rebopt = UxCreateToggleButton( "tg_rebopt", form6 );
	label8 = UxCreateLabel( "label8", form6 );
	rowColumn1 = UxCreateRowColumn( "rowColumn1", form6 );
	tg_linear = UxCreateToggleButton( "tg_linear", rowColumn1 );
	tg_quadratic = UxCreateToggleButton( "tg_quadratic", rowColumn1 );
	tg_spline = UxCreateToggleButton( "tg_spline", rowColumn1 );
	tg_respopt = UxCreateToggleButton( "tg_respopt", form6 );
	tg_extopt = UxCreateToggleButton( "tg_extopt", form6 );
	label9 = UxCreateLabel( "label9", form6 );
	label10 = UxCreateLabel( "label10", form6 );
	label11 = UxCreateLabel( "label11", form6 );
	label12 = UxCreateLabel( "label12", form6 );
	label13 = UxCreateLabel( "label13", form6 );
	tg_rotopt = UxCreateToggleButton( "tg_rotopt", form6 );
	tg_trimopt = UxCreateToggleButton( "tg_trimopt", form6 );
	tf_inputf = UxCreateTextField( "tf_inputf", form6 );
	tf_inpnumb = UxCreateTextField( "tf_inpnumb", form6 );
	tf_bias = UxCreateTextField( "tf_bias", form6 );
	tf_dark = UxCreateTextField( "tf_dark", form6 );
	tf_flat = UxCreateTextField( "tf_flat", form6 );
	tf_trim4 = UxCreateTextField( "tf_trim4", form6 );
	tf_longsess = UxCreateTextField( "tf_longsess", form6 );
	tf_redextab = UxCreateTextField( "tf_redextab", form6 );
	tf_redresp = UxCreateTextField( "tf_redresp", form6 );
	tf_rotstart = UxCreateTextField( "tf_rotstart", form6 );
	tf_outputf = UxCreateTextField( "tf_outputf", form6 );
	tf_outnumb = UxCreateTextField( "tf_outnumb", form6 );
	label5 = UxCreateLabel( "label5", form6 );
	tf_trim1 = UxCreateTextField( "tf_trim1", form6 );
	tf_trim2 = UxCreateTextField( "tf_trim2", form6 );
	tf_trim3 = UxCreateTextField( "tf_trim3", form6 );
	label6 = UxCreateLabel( "label6", form6 );
	label7 = UxCreateLabel( "label7", form6 );
	label1 = UxCreateLabel( "label1", form6 );
	form2 = UxCreateForm( "form2", form6 );
	pb_execute = UxCreatePushButton( "pb_execute", form2 );
	pb_airmass = UxCreatePushButton( "pb_airmass", form2 );
	tf_rotstep = UxCreateTextField( "tf_rotstep", form6 );

	_Uxinit_ApplicWindow();

	/* Create the X widgets */

	UxCreateWidget( ApplicWindow );
	UxCreateWidget( MainWindow );
	UxCreateWidget( menu1 );
	UxCreateWidget( menu1_p1 );
	UxCreateWidget( menu1_p1_b1 );
	UxCreateWidget( menu1_p1_b2 );
	UxCreateWidget( menu1_p1_b3 );
	UxCreateWidget( menu1_p1_b4 );
	UxCreateWidget( menu1_p1_b5 );
	UxPutSubMenuId( menu1_top_b1, "menu1_p1" );
	UxCreateWidget( menu1_top_b1 );

	UxCreateWidget( menu1_p4 );
	UxCreateWidget( menu_help_window );
	UxPutSubMenuId( menu1_top_b4, "menu1_p4" );
	UxCreateWidget( menu1_top_b4 );

	UxCreateWidget( form6 );
	UxCreateWidget( separator1 );
	UxCreateWidget( short_help );
	UxCreateWidget( separator2 );
	UxCreateWidget( label2 );
	UxCreateWidget( label3 );
	UxCreateWidget( tg_biasopt );
	UxCreateWidget( tg_darkopt );
	UxCreateWidget( tg_flatopt );
	UxCreateWidget( tg_rebopt );
	UxCreateWidget( label8 );
	UxCreateWidget( rowColumn1 );
	UxCreateWidget( tg_linear );
	UxCreateWidget( tg_quadratic );
	UxCreateWidget( tg_spline );
	UxCreateWidget( tg_respopt );
	UxCreateWidget( tg_extopt );
	UxCreateWidget( label9 );
	UxCreateWidget( label10 );
	UxCreateWidget( label11 );
	UxCreateWidget( label12 );
	UxCreateWidget( label13 );
	UxCreateWidget( tg_rotopt );
	UxCreateWidget( tg_trimopt );
	UxCreateWidget( tf_inputf );
	UxCreateWidget( tf_inpnumb );
	UxCreateWidget( tf_bias );
	UxCreateWidget( tf_dark );
	UxCreateWidget( tf_flat );
	UxCreateWidget( tf_trim4 );
	UxCreateWidget( tf_longsess );
	UxCreateWidget( tf_redextab );
	UxCreateWidget( tf_redresp );
	UxCreateWidget( tf_rotstart );
	UxCreateWidget( tf_outputf );
	UxCreateWidget( tf_outnumb );
	UxCreateWidget( label5 );
	UxCreateWidget( tf_trim1 );
	UxCreateWidget( tf_trim2 );
	UxCreateWidget( tf_trim3 );
	UxCreateWidget( label6 );
	UxCreateWidget( label7 );
	UxCreateWidget( label1 );
	UxCreateWidget( form2 );
	UxCreateWidget( pb_execute );
	UxCreateWidget( pb_airmass );
	UxCreateWidget( tf_rotstep );

	UxPutMenuHelpWidget( menu1, "menu1_top_b4" );

	UxAddCallback( menu1_p1_b1, XmNactivateCallback,
			activateCB_menu1_p1_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b2, XmNactivateCallback,
			activateCB_menu1_p1_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b3, XmNactivateCallback,
			activateCB_menu1_p1_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b5, XmNactivateCallback,
			activateCB_menu1_p1_b5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_window, XmNactivateCallback,
			activateCB_menu_help_window,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_biasopt, XmNvalueChangedCallback,
			valueChangedCB_tg_biasopt,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_darkopt, XmNvalueChangedCallback,
			valueChangedCB_tg_darkopt,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_flatopt, XmNvalueChangedCallback,
			valueChangedCB_tg_flatopt,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_rebopt, XmNvalueChangedCallback,
			valueChangedCB_tg_rebopt,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_linear, XmNvalueChangedCallback,
			valueChangedCB_tg_linear,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_quadratic, XmNvalueChangedCallback,
			valueChangedCB_tg_quadratic,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_spline, XmNvalueChangedCallback,
			valueChangedCB_tg_spline,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_respopt, XmNvalueChangedCallback,
			valueChangedCB_tg_respopt,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_extopt, XmNvalueChangedCallback,
			valueChangedCB_tg_extopt,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_rotopt, XmNvalueChangedCallback,
			valueChangedCB_tg_rotopt,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_trimopt, XmNvalueChangedCallback,
			valueChangedCB_tg_trimopt,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_inputf, XmNlosingFocusCallback,
			losingFocusCB_tf_inputf,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_inpnumb, XmNlosingFocusCallback,
			losingFocusCB_tf_inpnumb,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_bias, XmNlosingFocusCallback,
			losingFocusCB_tf_bias,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_dark, XmNlosingFocusCallback,
			losingFocusCB_tf_dark,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_flat, XmNlosingFocusCallback,
			losingFocusCB_tf_flat,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_trim4, XmNlosingFocusCallback,
			losingFocusCB_tf_trim4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_longsess, XmNlosingFocusCallback,
			losingFocusCB_tf_longsess,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_redextab, XmNlosingFocusCallback,
			losingFocusCB_tf_redextab,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_redresp, XmNlosingFocusCallback,
			losingFocusCB_tf_redresp,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_rotstart, XmNlosingFocusCallback,
			losingFocusCB_tf_rotstart,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_outputf, XmNlosingFocusCallback,
			losingFocusCB_tf_outputf,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_outnumb, XmNlosingFocusCallback,
			losingFocusCB_tf_outnumb,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_trim1, XmNlosingFocusCallback,
			losingFocusCB_tf_trim1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_trim2, XmNlosingFocusCallback,
			losingFocusCB_tf_trim2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_trim3, XmNlosingFocusCallback,
			losingFocusCB_tf_trim3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_execute, XmNactivateCallback,
			activateCB_pb_execute,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_airmass, XmNactivateCallback,
			activateCB_pb_airmass,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_rotstep, XmNlosingFocusCallback,
			losingFocusCB_tf_rotstep,
			(XtPointer) UxApplicWindowContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ApplicWindow );

	UxMainWindowSetAreas( MainWindow, menu1, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, form6 );
	return ( ApplicWindow );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ApplicWindow()
{
	swidget                 rtrn;
	_UxCApplicWindow        *UxContext;

	UxApplicWindowContext = UxContext =
		(_UxCApplicWindow *) UxMalloc( sizeof(_UxCApplicWindow) );

	{
		extern swidget create_file_dialog();
		extern swidget create_AirmassShell();
		extern swidget create_HelpShell();
		rtrn = _Uxbuild_ApplicWindow();

		create_file_dialog();
		create_HelpShell();
		create_AirmassShell();
		
		return(rtrn);
	}
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ApplicWindow()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearHelp", action_ClearHelp },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "WriteHelp", action_WriteHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ApplicWindow();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

