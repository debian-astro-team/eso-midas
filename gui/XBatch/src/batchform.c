/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       batchform.c                                */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    XLong, Spectroscopy, Long-Slit             */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Creation  17-MAR-1993         

    090819	last modif				    */
/* ------------------------------------------------------- */

#include <gl_defs.h>
#include <xm_defs.h>
#include <midas_def.h>
#include <main_defs.h>
#include <spec_defs.h>
#include <spec_comm.h>

#include <UxLib.h>

#define MAXCDESC	20

#define TOGGLE_STATE(x, y, z)   XmToggleButtonSetState(UxGetWidget(UxFindSwidget(x)), y, z);
#define SET_SENSITIVE(x, y)  XtSetSensitive(UxGetWidget(UxFindSwidget(x)), y);


extern void WriteKeyword(), XmToggleButtonSetState();
 
extern int AppendDialogText();


void ReadDefaultsReduce();
void DisplayParamsReduce();
void WriteKeyword();
char *GetCdesc();

void InitReduce()
{
    strcpy(Redsess, "default.brf");

    ReadDefaultsReduce();
    DisplayParamsReduce();
}

void ReadDefaultsReduce()
{
    int unit;		/* useless */
    int actval, nulval;	/* useless */

    SCKGETC("INPUTF", 1, 60, &actval, Inputf);
    SCKGETC("INPNUMB", 1, 60, &actval, Inpnumb);
    SCKGETC("OUTPUTF", 1, 60, &actval, Outputf);
    SCKRDI("OUTNUMB", 1, 1, &actval, &Outnumb, &unit, &nulval);
    SCKGETC("ROTOPT", 1, 3, &actval, Rotopt);
    SCKRDD("ROTSTART", 1, 1, &actval, &Rotstart, &unit, &nulval);
    SCKRDD("ROTSTEP", 1, 1, &actval, &Rotstep, &unit, &nulval);
    SCKGETC("TRIMOPT", 1, 3, &actval, Trimopt);
    SCKRDI("TRIM", 1, 4, &actval, Trim, &unit, &nulval);
    SCKGETC("REBOPT", 1, 3, &actval, Rebopt);
    SCKGETC("SESSION", 1, 60, &actval, Longsess);
    SCKGETC("REBMTD", 1, 12, &actval, Redrbmtd);
    SCKGETC("EXTOPT", 1, 3, &actval, Extopt);
    SCKGETC("EXTAB", 1, 60, &actval, Redextab);
    SCKGETC("RESPOPT", 1, 3, &actval, Respopt);
    SCKGETC("RESPONSE", 1, 60, &actval, Redresp);
    SCKGETC("BIASOPT", 1, 3, &actval, Biasopt);
    SCKGETC("DARKOPT", 1, 3, &actval, Darkopt);
    SCKGETC("FLATOPT", 1, 3, &actval, Flatopt);
    SCKGETC("BIAS", 1, 60, &actval, Bias);
    SCKGETC("DARK", 1, 60, &actval, Dark);
    SCKGETC("FLAT", 1, 60, &actval, Flat);
}

void ReadParamsReduce( redsess )
char redsess[];
{
    int unit;		/* useless */
    int actval, nulval;	/* useless */
    int id;

    TCTOPN(redsess, F_I_MODE, &id);
    strcpy(Redsess, redsess);

    SCDGETC(id, "INPUTF", 1, 60, &actval, Inputf);
    SCDGETC(id, "INPNUMB", 1, 60, &actval, Inpnumb);
    SCDGETC(id, "OUTPUTF", 1, 60, &actval, Outputf);
    SCDRDI(id, "OUTNUMB", 1, 1, &actval, &Outnumb, &unit, &nulval);
    SCDGETC(id, "ROTOPT", 1, 3, &actval, Rotopt);
    SCDRDD(id, "ROTSTART", 1, 1, &actval, &Rotstart, &unit, &nulval);
    SCDRDD(id, "ROTSTEP", 1, 1, &actval, &Rotstep, &unit, &nulval);
    SCDGETC(id, "TRIMOPT", 1, 3, &actval, Trimopt);
    SCDRDI(id, "TRIM", 1, 4, &actval, Trim, &unit, &nulval);
    SCDGETC(id, "REBOPT", 1, 3, &actval, Rebopt);
    SCDGETC(id, "SESSION", 1, 60, &actval, Longsess);
    SCDGETC(id, "REBMTD", 1, 12, &actval, Redrbmtd);
    SCDGETC(id, "EXTOPT", 1, 3, &actval, Extopt);
    SCDGETC(id, "EXTAB", 1, 60, &actval, Redextab);
    SCDGETC(id, "RESPOPT", 1, 3, &actval, Respopt);
    SCDGETC(id, "RESPONSE", 1, 60, &actval, Redresp);
    SCDGETC(id, "BIASOPT", 1, 3, &actval, Biasopt);
    SCDGETC(id, "DARKOPT", 1, 3, &actval, Darkopt);
    SCDGETC(id, "FLATOPT", 1, 3, &actval, Flatopt);
    SCDGETC(id, "BIAS", 1, 60, &actval, Bias);
    SCDGETC(id, "DARK", 1, 60, &actval, Dark);
    SCDGETC(id, "FLAT", 1, 60, &actval, Flat);

    TCTCLO(id);
}

void DisplayParamsReduce()
{
    char str[MAXLINE];

    XmTextSetString(UxGetWidget(UxFindSwidget("tf_inputf")), Inputf);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_inpnumb")), Inpnumb);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_bias")), Bias);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_dark")), Dark);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_flat")), Flat);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_longsess")), Longsess);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_redextab")), Redextab);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_redresp")), Redresp);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_outputf")), Outputf);

    TOGGLE_STATE("tg_biasopt", !strncmp(Biasopt, "YE", 2), False);
    TOGGLE_STATE("tg_darkopt", !strncmp(Darkopt, "YE", 2), False);
    TOGGLE_STATE("tg_flatopt", !strncmp(Flatopt, "YE", 2), False);
    TOGGLE_STATE("tg_rotopt", !strncmp(Rotopt, "YE", 2), False);
    TOGGLE_STATE("tg_trimopt", !strncmp(Trimopt, "YE", 2), False);
    TOGGLE_STATE("tg_rebopt", !strncmp(Rebopt, "YE", 2), False);
    TOGGLE_STATE("tg_extopt", !strncmp(Extopt, "YE", 2), False);
    TOGGLE_STATE("tg_respopt", !strncmp(Respopt, "YE", 2), False);

    TOGGLE_STATE("tg_linear", !strncmp(Redrbmtd, "LINE", 4), False);
    TOGGLE_STATE("tg_quadratic", !strncmp(Redrbmtd, "QUAD", 4), False);
    TOGGLE_STATE("tg_spline", !strncmp(Redrbmtd, "SPLI", 4), False);

    sprintf(str, "%.5g", Rotstart);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_rotstart")), str);
    sprintf(str, "%.5g", Rotstep);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_rotstep")), str);
    sprintf(str, "%d", Trim[0]);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_trim1")), str);
    sprintf(str, "%d", Trim[1]);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_trim2")), str);
    sprintf(str, "%d", Trim[2]);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_trim3")), str);
    sprintf(str, "%d", Trim[3]);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_trim4")), str);
    sprintf(str, "%d", Outnumb);
    XmTextSetString(UxGetWidget(UxFindSwidget("tf_outnumb")), str);

}

void WriteKeyword( text, keyword_cmd )
char *text;
char *keyword_cmd;
{
    char command[MAXLINE];

    sprintf(command, "%s%s", keyword_cmd, text);
    AppendDialogText(command);
}
