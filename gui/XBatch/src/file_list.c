/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	file_list.c

.VERSION
 090819         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxfile_list;
	swidget	Uxform1;
	swidget	UxpushButton1;
	swidget	UxscrolledWindow1;
	swidget	Uxsl_file_list;
} _UxCfile_list;

#define file_list               UxFile_listContext->Uxfile_list
#define form1                   UxFile_listContext->Uxform1
#define pushButton1             UxFile_listContext->UxpushButton1
#define scrolledWindow1         UxFile_listContext->UxscrolledWindow1
#define sl_file_list            UxFile_listContext->Uxsl_file_list

static _UxCfile_list	*UxFile_listContext;

extern void CallbackList();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_file_list();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_list           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_listContext;
	UxFile_listContext = UxContext =
			(_UxCfile_list *) UxGetContext( UxThisWidget );
	{
	extern swidget FileListInterface;
	
	UxPopdownInterface(FileListInterface);
	}
	UxFile_listContext = UxSaveCtx;
}

static void	createCB_sl_file_list( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_list           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_listContext;
	UxFile_listContext = UxContext =
			(_UxCfile_list *) UxGetContext( UxThisWidget );
	{
	extern Widget FileListWidget;
	
	FileListWidget = UxWidget;
	}
	UxFile_listContext = UxSaveCtx;
}

static void	browseSelectionCB_sl_file_list( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_list           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_listContext;
	UxFile_listContext = UxContext =
			(_UxCfile_list *) UxGetContext( UxThisWidget );
	{
	char *choice;
	XmListCallbackStruct *cbs;
	
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);
	
	CallbackList(choice);
	
	}
	UxFile_listContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_file_list()
{
	UxPutTitle( file_list, "List selection" );
	UxPutGeometry( file_list, "+100+100" );
	UxPutBackground( file_list, WindowBackground );
	UxPutKeyboardFocusPolicy( file_list, "pointer" );
	UxPutIconName( file_list, "List selection" );
	UxPutHeight( file_list, 353 );
	UxPutWidth( file_list, 260 );
	UxPutY( file_list, 306 );
	UxPutX( file_list, 638 );

	UxPutBackground( form1, ApplicBackground );
	UxPutHeight( form1, 352 );
	UxPutWidth( form1, 214 );
	UxPutY( form1, 0 );
	UxPutX( form1, 0 );
	UxPutUnitType( form1, "pixels" );
	UxPutResizePolicy( form1, "resize_none" );

	UxPutForeground( pushButton1, ButtonForeground );
	UxPutLabelString( pushButton1, "Cancel" );
	UxPutFontList( pushButton1, BoldTextFont );
	UxPutBackground( pushButton1, ButtonBackground );
	UxPutHeight( pushButton1, 35 );
	UxPutWidth( pushButton1, 260 );
	UxPutY( pushButton1, 318 );
	UxPutX( pushButton1, 0 );

	UxPutWidth( scrolledWindow1, 270 );
	UxPutHeight( scrolledWindow1, 316 );
	UxPutBackground( scrolledWindow1, ListBackground );
	UxPutScrollBarPlacement( scrolledWindow1, "bottom_left" );
	UxPutShadowThickness( scrolledWindow1, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow1, "static" );
	UxPutVisualPolicy( scrolledWindow1, "variable" );
	UxPutY( scrolledWindow1, 2 );
	UxPutX( scrolledWindow1, 21 );
	UxPutScrollingPolicy( scrolledWindow1, "application_defined" );

	UxPutForeground( sl_file_list, ListForeground );
	UxPutVisibleItemCount( sl_file_list, 19 );
	UxPutFontList( sl_file_list, SmallFont );
	UxPutBackground( sl_file_list, TextBackground );
	UxPutListSizePolicy( sl_file_list, "variable" );
	UxPutScrollBarDisplayPolicy( sl_file_list, "static" );
	UxPutHeight( sl_file_list, 316 );
	UxPutWidth( sl_file_list, 191 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_file_list()
{
	/* Create the swidgets */

	file_list = UxCreateTopLevelShell( "file_list", NO_PARENT );
	UxPutContext( file_list, UxFile_listContext );

	form1 = UxCreateForm( "form1", file_list );
	pushButton1 = UxCreatePushButton( "pushButton1", form1 );
	scrolledWindow1 = UxCreateScrolledWindow( "scrolledWindow1", form1 );
	sl_file_list = UxCreateScrolledList( "sl_file_list", scrolledWindow1 );

	_Uxinit_file_list();

	/* Create the X widgets */

	UxCreateWidget( file_list );
	UxCreateWidget( form1 );
	UxCreateWidget( pushButton1 );
	UxPutLeftOffset( scrolledWindow1, 2 );
	UxPutLeftAttachment( scrolledWindow1, "attach_form" );
	UxPutRightOffset( scrolledWindow1, 2 );
	UxPutRightAttachment( scrolledWindow1, "attach_form" );
	UxCreateWidget( scrolledWindow1 );

	UxCreateWidget( sl_file_list );
	createCB_sl_file_list( UxGetWidget( sl_file_list ),
			(XtPointer) UxFile_listContext, (XtPointer) NULL );


	UxAddCallback( pushButton1, XmNactivateCallback,
			activateCB_pushButton1,
			(XtPointer) UxFile_listContext );

	UxAddCallback( sl_file_list, XmNbrowseSelectionCallback,
			browseSelectionCB_sl_file_list,
			(XtPointer) UxFile_listContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( file_list );

	return ( file_list );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_file_list()
{
	swidget                 rtrn;
	_UxCfile_list           *UxContext;

	UxFile_listContext = UxContext =
		(_UxCfile_list *) UxMalloc( sizeof(_UxCfile_list) );

	rtrn = _Uxbuild_file_list();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_file_list()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_file_list();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

