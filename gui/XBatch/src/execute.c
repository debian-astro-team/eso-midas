/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*  

.VERSION
 090819         last modif

*/

#include <stdlib.h>
#include <stdio.h>
#include <midas_def.h>
#include <osfile.h>
#include <gl_defs.h>
#include <spec_comm.h>

#define MAXFILES        100

#define CREA_IN_ICAT_PRG	"tmp_crea_in_icat.prg"
#define CREA_IN_ICAT		"@@ tmp_crea_in_icat"
#define TMP_IN_CAT		"tmp_in.cat"

#define CREA_IN_ICAT_INST 	"crea/icat tmp_in null\n"

extern int AppendDialogText();

extern void DropTrailingBlanks();


void GetNameInputCatalog();

void ExecuteReduce()
{
    char command[MAXLINE];
    char in_cat[MAXLINE];

    GetNameInputCatalog(in_cat);
    sprintf(command, "%s%s", C_REDUCE, in_cat);
    AppendDialogText(command);
}


/*********************************************************************

  Input catalog management 

 *********************************************************************/

static char InputCatalog[MAXLINE];

void GetNameInputCatalog( catalog )
char *catalog;
{
    strcpy(catalog, InputCatalog);
}

void CreateInputCatalog( tx_in_prefix, tx_in_numbers )
char *tx_in_prefix, *tx_in_numbers;
{
    char *delim = " ,";
    char *token;
    char buf[MAXLINE], files[MAXLINE];
    int inlist[MAXFILES];
    int i, n_inlist = 0;
    int begin, end;
    int inlist_fp;
    char *strtok();

    token = (char *)malloc((size_t)(sizeof(char) * MAXLINE));

    DropTrailingBlanks(tx_in_prefix);
    DropTrailingBlanks(tx_in_numbers);

    /* if field = null ==> prefix field is considered to be the input catalog */
    if ( tx_in_numbers[0] == '\0' )
	strcpy(InputCatalog, tx_in_prefix);
    else { 
        /* generate the file numbers list */
    	strcpy(files, tx_in_numbers);
    	token = strtok(files, delim);
    	while ( token != NULL ) {
            if ( sscanf(token, "%d-%d", &begin, &end) == 2 )
            	for ( i = begin; i <= end && n_inlist < MAXFILES; i++ )
                    inlist[n_inlist++] = i;
            else if ( sscanf(token, "%d", &begin) == 1 )
            	inlist[n_inlist++] = begin;
            token = strtok((char *)NULL, delim);
    	}
        /* generate the catalog */
	strcpy(InputCatalog, TMP_IN_CAT);
	inlist_fp = osaopen(CREA_IN_ICAT_PRG, WRITE);
	sprintf(buf, "%s", CREA_IN_ICAT_INST);
	osawrite(inlist_fp, buf, strlen(buf));
    	for ( i = 0; i < n_inlist; i++ ) {
            sprintf(buf, "add/icat %s %s%04d\n", InputCatalog, 
	            tx_in_prefix, inlist[i]);
	    osawrite(inlist_fp, buf, strlen(buf));
        }
	osaclose(inlist_fp);
	AppendDialogText(CREA_IN_ICAT);
	unlink(CREA_IN_ICAT_PRG);
    }
}
