/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       midasutil.c                                */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    XLong, Spectroscopy, Long-Slit             */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Creation  17-MAR-1993         
   
   090819	last modif				   */
/* ------------------------------------------------------- */

#include <gl_defs.h>
#include <xm_defs.h>
#include <main_defs.h>
#include <spec_comm.h>
#include <UxLib.h>

#include <midas_def.h>

#define HELP_FILE	"gui/XBatch/help/batchred.hlp"
#define HELP_DELIMITER	'~'

static char help_text[4000]; /* Max of 50 lines */

void DisplayShortHelp( sw )
Widget sw; 	/* current widget */
{
    char s[256];
 
    s[0] = '\0';

    if ( sw == UxGetWidget(UxFindSwidget("tf_inputf")) )
      	strcpy(s,"Input catalog or prefix of file numbers entered in the \"Numbers\" field");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_inpnumb")) )
      	strcpy(s,"File numbers associated to the prefix entered, separated by\ncommas and/or dashes (range)");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_bias")) )
      	strcpy(s,"Bias image or value");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_dark")) )
      	strcpy(s,"Dark image");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_flat")) )
      	strcpy(s,"Flat image or value");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_rotstart")) )
      	strcpy(s,"New start value in Y direction if image is rotated");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_rotstep")) )
      	strcpy(s,"New step value in Y direction if image is rotated");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_trim1")) )
      	strcpy(s,"X-start of subimage to be extracted");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_trim2")) )
      	strcpy(s,"Y-start of subimage to be extracted");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_trim3")) )
      	strcpy(s,"X-end of subimage to be extracted");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_trim4")) )
      	strcpy(s,"Y-end of subimage to be extracted");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_longsess")) )
      	strcpy(s,"Parameters table saved in a previous LONG reduction session");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_redextab")) )
      	strcpy(s,"Extinction table");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_redresp")) )
      	strcpy(s,"Response curve (1-D or 2-D image)");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_outputf")) )
      	strcpy(s,"Prefix of the output images. If it is not filled, the output names will be\nthe input names plus the \"_red\" suffix");
    else if ( sw == UxGetWidget(UxFindSwidget("tf_outnumb")) )
      	strcpy(s,"Initial output file number");
    else if ( sw == UxGetWidget(UxFindSwidget("pb_airmass")) )
      	strcpy(s,"Open the form for edition of airmass values of the input images");
    else if ( sw == UxGetWidget(UxFindSwidget("pb_execute")) )
      	strcpy(s,"Execute the batch reduction");

    if ( s[0] != '\0' ) 
    	UxPutText(UxFindSwidget("short_help"), s);
}

void DisplayExtendedHelp( sw )
Widget sw; 	/* current widget */
{
    char s[4000];
    char *find_extended_help();
 
    s[0] = '\0';

    if ( sw == UxGetWidget(UxFindSwidget("menu_help_window")) )
      	strcpy(s, find_extended_help("HELP_WINDOW"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_airmass")) )
      	strcpy(s, find_extended_help("AIRMASS"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_execute")) )
      	strcpy(s, find_extended_help("EXECUTE"));

    if ( s[0] != '\0' ) {
    	UxPopupInterface(UxFindSwidget("HelpShell"), no_grab);
    	UxPutText(UxFindSwidget("tx_extended_help"), s);
    }
}

char *find_extended_help( key )
char *key;
{
    char midashome[512], midvers[80], help_pathname[512];
    char c, h, s[MAXLINE];
    FILE *fp;
    int idx = 0;

    osfphname("MIDVERS", midvers);
    osfphname("MIDASHOME", midashome);
    sprintf( help_pathname, "%s/%s/%s", midashome, midvers, HELP_FILE );
    if ( (fp = fopen(help_pathname, "r")) == NULL ) {
        sprintf(s, "XBatch Help file [%s] not found.\r\n", help_pathname);
	SCTPUT(s);
        return("");
    }

    while ( (c = getc(fp)) != EOF )
	if ( c == HELP_DELIMITER && 
             !strncmp(key, fgets(s, MAXLINE, fp), strlen(key)) ) {
    	    while ( (h = getc(fp)) != EOF && h != HELP_DELIMITER )
		help_text[idx++] = h;
	    help_text[idx] = '\0';
            fclose(fp);
	    return(help_text);
	}
    fclose(fp);
    return("");

}
