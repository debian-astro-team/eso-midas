/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       cb_list.c                                  */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    XLong, Spectroscopy, Long-Slit             */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Creation  17-MAR-1993         

    090819		last modif			   */
/* ------------------------------------------------------- */

#include <gl_defs.h>
#include <xm_defs.h>
#include <main_defs.h>
#include <spec_comm.h>
#include <UxLib.h>



extern void ReadParamsReduce(), DisplayParamsReduce(), WriteKeyword();
extern void SetFileList();

extern int AppendDialogText();



int PopupList( list_type )
int list_type;
{
    int strip = 1;  /* strip off the directories */

    extern swidget FileListInterface;
    extern Widget FileListWidget;
    extern char  DirSpecs[];

    ListType = list_type;

    switch ( list_type ) {
	case LIST_REDSESS:
	    SET_LIST_TITLE("Enter parameters table");
       	    strcpy(DirSpecs, "*.brf");
	    break;
	case LIST_INPUTF:
	    SET_LIST_TITLE("Enter catalog name");
       	    strcpy(DirSpecs, "*.cat");
	    break;
	case LIST_BIAS:
	    SET_LIST_TITLE("Enter bias image");
       	    strcpy(DirSpecs, "*.bdf");
	    break;
	case LIST_DARK:
	    SET_LIST_TITLE("Enter dark image");
       	    strcpy(DirSpecs, "*.bdf");
	    break;
	case LIST_FLAT:
	    SET_LIST_TITLE("Enter flat-field image");
       	    strcpy(DirSpecs, "*.bdf");
	    break;
	case LIST_LONGSESS:
	    SET_LIST_TITLE("Enter parameters table");
       	    strcpy(DirSpecs, "*.tbl");
	    break;
	case LIST_REDEXTAB:
	    SET_LIST_TITLE("Enter extinction table");
       	    strcpy(DirSpecs, "*.tbl");
	    break;
	case LIST_REDRESP:
	    SET_LIST_TITLE("Enter response curve");
       	    strcpy(DirSpecs, "*.bdf");
	    break;
	default:
	    break;
    }
    SetFileList(FileListWidget, strip, DirSpecs);
    UxPopupInterface(FileListInterface, exclusive_grab);
return 0;
}

void CallbackList( choice )
char *choice;
{
    char command[512];
    
    extern swidget FileListInterface;
    extern swidget TextFieldSwidget;
    
    command[0] = '\0';

    switch ( ListType ) {
        case LIST_REDSESS:
            ReadParamsReduce(choice);
            DisplayParamsReduce();
            sprintf(command, "%s%s", C_REDINIT, choice);
    	    break;
        case LIST_INPUTF:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Inputf, choice);
            WriteKeyword(Inputf, K_INPUTF);
    	    break;
        case LIST_LONGSESS:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Longsess, choice);
            WriteKeyword(Longsess, K_LONGSESS);
    	    break;
        case LIST_REDRESP:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Redresp, choice);
            WriteKeyword(Redresp, K_REDRESP);
    	    break;
        case LIST_BIAS:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Bias, choice);
            WriteKeyword(Bias, K_BIAS);
    	    break;
        case LIST_DARK:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Dark, choice);
            WriteKeyword(Dark, K_DARK);
    	    break;
        case LIST_FLAT:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Flat, choice);
            WriteKeyword(Flat, K_FLAT);
    	    break;
        case LIST_REDEXTAB:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Redextab, choice);
            WriteKeyword(Redextab, K_REDEXTAB);
    	    break;
    }
    XtFree(choice);

    UxPopdownInterface(FileListInterface);
    
    if ( command[0] != '\0' )
    	AppendDialogText(command);
}

void CallbackDialog()
{
    char command[128];
    char *out;
    extern int DialogType;

    out = XmTextGetString(UxGetWidget(UxFindSwidget("tf_file_dialog")));
    switch ( DialogType ) {
	case DIALOG_REDSESS:
    	    strcpy(Redsess, out);
     	    sprintf(command, "%s%s", C_REDSAVE, Redsess);
	    break;
    }
    AppendDialogText(command);
    XtFree(out);
    UxPopdownInterface(UxFindSwidget("file_dialog"));
}
