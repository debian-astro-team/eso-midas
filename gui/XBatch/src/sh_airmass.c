/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	sh_airmass.c

.VERSION
 090819         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxRowCol.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxAirmassShell;
	swidget	Uxform4;
	swidget	UxscrolledWindow2;
	swidget	UxrowColumn2;
	swidget	Uxt_airmass_name;
	swidget	Uxt_airmass_value;
	swidget	Uxpb_airmass_ok;
	swidget	Uxpb_airmass_cancel;
} _UxCAirmassShell;

#define AirmassShell            UxAirmassShellContext->UxAirmassShell
#define form4                   UxAirmassShellContext->Uxform4
#define scrolledWindow2         UxAirmassShellContext->UxscrolledWindow2
#define rowColumn2              UxAirmassShellContext->UxrowColumn2
#define t_airmass_name          UxAirmassShellContext->Uxt_airmass_name
#define t_airmass_value         UxAirmassShellContext->Uxt_airmass_value
#define pb_airmass_ok           UxAirmassShellContext->Uxpb_airmass_ok
#define pb_airmass_cancel       UxAirmassShellContext->Uxpb_airmass_cancel

static _UxCAirmassShell	*UxAirmassShellContext;

extern void  UpdateAirmassValues(), ExecuteReduce();

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_AirmassShell();

/*******************************************************************************
	Auxiliary code from the Declarations Editor:
*******************************************************************************/

swidget AirmassNameSwidget()
{
    return(t_airmass_name);
}

swidget AirmassValueSwidget()
{
    return(t_airmass_value);
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_airmass_ok( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAirmassShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAirmassShellContext;
	UxAirmassShellContext = UxContext =
			(_UxCAirmassShell *) UxGetContext( UxThisWidget );
	{
	extern int ExecuteCaller;
	
	UxPopdownInterface(UxFindSwidget("AirmassShell"));
	UpdateAirmassValues();
	if ( ExecuteCaller )
	    ExecuteReduce();
	}
	UxAirmassShellContext = UxSaveCtx;
}

static void	activateCB_pb_airmass_cancel( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAirmassShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAirmassShellContext;
	UxAirmassShellContext = UxContext =
			(_UxCAirmassShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("AirmassShell"));
	
	}
	UxAirmassShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_AirmassShell()
{
	UxPutGeometry( AirmassShell, "+100+100" );
	UxPutBackground( AirmassShell, WindowBackground );
	UxPutKeyboardFocusPolicy( AirmassShell, "pointer" );
	UxPutTitle( AirmassShell, "Airmass form" );
	UxPutHeight( AirmassShell, 252 );
	UxPutWidth( AirmassShell, 428 );
	UxPutY( AirmassShell, 381 );
	UxPutX( AirmassShell, 684 );

	UxPutBackground( form4, WindowBackground );
	UxPutHeight( form4, 248 );
	UxPutWidth( form4, 420 );
	UxPutY( form4, 2 );
	UxPutX( form4, 2 );
	UxPutUnitType( form4, "pixels" );
	UxPutResizePolicy( form4, "resize_none" );

	UxPutBackground( scrolledWindow2, WindowBackground );
	UxPutScrollBarPlacement( scrolledWindow2, "bottom_left" );
	UxPutHeight( scrolledWindow2, 211 );
	UxPutWidth( scrolledWindow2, 424 );
	UxPutY( scrolledWindow2, 2 );
	UxPutX( scrolledWindow2, 0 );
	UxPutScrollingPolicy( scrolledWindow2, "automatic" );

	UxPutEntryAlignment( rowColumn2, "alignment_beginning" );
	UxPutBackground( rowColumn2, WindowBackground );
	UxPutOrientation( rowColumn2, "horizontal" );
	UxPutHeight( rowColumn2, 202 );
	UxPutWidth( rowColumn2, 394 );
	UxPutY( rowColumn2, 53 );
	UxPutX( rowColumn2, 59 );

	UxPutEditable( t_airmass_name, "false" );
	UxPutForeground( t_airmass_name, TextForeground );
	UxPutCursorPositionVisible( t_airmass_name, "false" );
	UxPutEditMode( t_airmass_name, "multi_line_edit" );
	UxPutFontList( t_airmass_name, SmallFont );
	UxPutBackground( t_airmass_name, TextBackground );
	UxPutHeight( t_airmass_name, 781 );
	UxPutWidth( t_airmass_name, 298 );
	UxPutY( t_airmass_name, 7 );
	UxPutX( t_airmass_name, 3 );

	UxPutForeground( t_airmass_value, TextForeground );
	UxPutEditMode( t_airmass_value, "multi_line_edit" );
	UxPutBackground( t_airmass_value, TextBackground );
	UxPutFontList( t_airmass_value, SmallFont );
	UxPutHeight( t_airmass_value, 524 );
	UxPutWidth( t_airmass_value, 90 );
	UxPutY( t_airmass_value, 4 );
	UxPutX( t_airmass_value, 304 );

	UxPutForeground( pb_airmass_ok, ApplyForeground );
	UxPutHighlightOnEnter( pb_airmass_ok, "true" );
	UxPutHighlightColor( pb_airmass_ok, "red" );
	UxPutFontList( pb_airmass_ok, BoldTextFont );
	UxPutLabelString( pb_airmass_ok, "Ok" );
	UxPutBackground( pb_airmass_ok, ButtonBackground );
	UxPutHeight( pb_airmass_ok, 32 );
	UxPutWidth( pb_airmass_ok, 82 );
	UxPutY( pb_airmass_ok, 216 );
	UxPutX( pb_airmass_ok, 19 );

	UxPutForeground( pb_airmass_cancel, CancelForeground );
	UxPutHighlightOnEnter( pb_airmass_cancel, "true" );
	UxPutHighlightColor( pb_airmass_cancel, ButtonForeground );
	UxPutLabelString( pb_airmass_cancel, "Cancel" );
	UxPutFontList( pb_airmass_cancel, BoldTextFont );
	UxPutBackground( pb_airmass_cancel, ButtonBackground );
	UxPutHeight( pb_airmass_cancel, 32 );
	UxPutWidth( pb_airmass_cancel, 82 );
	UxPutY( pb_airmass_cancel, 216 );
	UxPutX( pb_airmass_cancel, 108 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_AirmassShell()
{
	/* Create the swidgets */

	AirmassShell = UxCreateTopLevelShell( "AirmassShell", NO_PARENT );
	UxPutContext( AirmassShell, UxAirmassShellContext );

	form4 = UxCreateForm( "form4", AirmassShell );
	scrolledWindow2 = UxCreateScrolledWindow( "scrolledWindow2", form4 );
	rowColumn2 = UxCreateRowColumn( "rowColumn2", scrolledWindow2 );
	t_airmass_name = UxCreateText( "t_airmass_name", rowColumn2 );
	t_airmass_value = UxCreateText( "t_airmass_value", rowColumn2 );
	pb_airmass_ok = UxCreatePushButton( "pb_airmass_ok", form4 );
	pb_airmass_cancel = UxCreatePushButton( "pb_airmass_cancel", form4 );

	_Uxinit_AirmassShell();

	/* Create the X widgets */

	UxCreateWidget( AirmassShell );
	UxCreateWidget( form4 );
	UxCreateWidget( scrolledWindow2 );
	UxCreateWidget( rowColumn2 );
	UxCreateWidget( t_airmass_name );
	UxCreateWidget( t_airmass_value );
	UxCreateWidget( pb_airmass_ok );
	UxCreateWidget( pb_airmass_cancel );

	UxAddCallback( pb_airmass_ok, XmNactivateCallback,
			activateCB_pb_airmass_ok,
			(XtPointer) UxAirmassShellContext );

	UxAddCallback( pb_airmass_cancel, XmNactivateCallback,
			activateCB_pb_airmass_cancel,
			(XtPointer) UxAirmassShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( AirmassShell );

	return ( AirmassShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_AirmassShell()
{
	swidget                 rtrn;
	_UxCAirmassShell        *UxContext;

	UxAirmassShellContext = UxContext =
		(_UxCAirmassShell *) UxMalloc( sizeof(_UxCAirmassShell) );

	rtrn = _Uxbuild_AirmassShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_AirmassShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_AirmassShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

