! UIMX ascii 2.0 key: 3413                                                      

*file_list.class: topLevelShell
*file_list.parent: NO_PARENT
*file_list.static: true
*file_list.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*file_list.ispecdecl:
*file_list.funcdecl: swidget create_file_list()\

*file_list.funcname: create_file_list
*file_list.funcdef: "swidget", "<create_file_list>(%)"
*file_list.icode:
*file_list.fcode: return(rtrn);\

*file_list.auxdecl:
*file_list.name: file_list
*file_list.x: 638
*file_list.y: 306
*file_list.width: 260
*file_list.height: 353
*file_list.iconName: "List selection"
*file_list.keyboardFocusPolicy: "pointer"
*file_list.background: WindowBackground
*file_list.geometry: "+100+100"
*file_list.title: "List selection"

*form1.class: form
*form1.parent: file_list
*form1.static: true
*form1.name: form1
*form1.resizePolicy: "resize_none"
*form1.unitType: "pixels"
*form1.x: 0
*form1.y: 0
*form1.width: 214
*form1.height: 352
*form1.background: ApplicBackground

*pushButton1.class: pushButton
*pushButton1.parent: form1
*pushButton1.static: true
*pushButton1.name: pushButton1
*pushButton1.x: 0
*pushButton1.y: 318
*pushButton1.width: 260
*pushButton1.height: 35
*pushButton1.background: ButtonBackground
*pushButton1.fontList: BoldTextFont
*pushButton1.labelString: "Cancel"
*pushButton1.activateCallback: {\
extern swidget FileListInterface;\
\
UxPopdownInterface(FileListInterface);\
}
*pushButton1.foreground: ButtonForeground

*scrolledWindow1.class: scrolledWindow
*scrolledWindow1.parent: form1
*scrolledWindow1.static: true
*scrolledWindow1.name: scrolledWindow1
*scrolledWindow1.scrollingPolicy: "application_defined"
*scrolledWindow1.x: 21
*scrolledWindow1.y: 2
*scrolledWindow1.visualPolicy: "variable"
*scrolledWindow1.scrollBarDisplayPolicy: "static"
*scrolledWindow1.shadowThickness: 0
*scrolledWindow1.scrollBarPlacement: "bottom_left"
*scrolledWindow1.background: ListBackground
*scrolledWindow1.height: 316
*scrolledWindow1.rightAttachment: "attach_form"
*scrolledWindow1.rightOffset: 2
*scrolledWindow1.leftAttachment: "attach_form"
*scrolledWindow1.leftOffset: 2
*scrolledWindow1.width: 270

*sl_file_list.class: scrolledList
*sl_file_list.parent: scrolledWindow1
*sl_file_list.static: true
*sl_file_list.name: sl_file_list
*sl_file_list.width: 191
*sl_file_list.height: 316
*sl_file_list.scrollBarDisplayPolicy: "static"
*sl_file_list.listSizePolicy: "variable"
*sl_file_list.background: TextBackground
*sl_file_list.fontList: SmallFont
*sl_file_list.visibleItemCount: 19
*sl_file_list.browseSelectionCallback: {\
char *choice;\
XmListCallbackStruct *cbs;\
\
cbs = (XmListCallbackStruct *)UxCallbackArg;\
XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);\
\
CallbackList(choice);\
\
}
*sl_file_list.createCallback: {\
extern Widget FileListWidget;\
\
FileListWidget = UxWidget;\
}
*sl_file_list.foreground: ListForeground

