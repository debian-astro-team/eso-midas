! UIMX ascii 2.0 key: 6648                                                      

*AirmassShell.class: topLevelShell
*AirmassShell.parent: NO_PARENT
*AirmassShell.static: true
*AirmassShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*AirmassShell.ispecdecl:
*AirmassShell.funcdecl: swidget create_AirmassShell()\

*AirmassShell.funcname: create_AirmassShell
*AirmassShell.funcdef: "swidget", "<create_AirmassShell>(%)"
*AirmassShell.icode:
*AirmassShell.fcode: return(rtrn);\

*AirmassShell.auxdecl: swidget AirmassNameSwidget()\
{\
    return(t_airmass_name);\
}\
\
swidget AirmassValueSwidget()\
{\
    return(t_airmass_value);\
}\

*AirmassShell.name: AirmassShell
*AirmassShell.x: 684
*AirmassShell.y: 381
*AirmassShell.width: 428
*AirmassShell.height: 252
*AirmassShell.title: "Airmass form"
*AirmassShell.keyboardFocusPolicy: "pointer"
*AirmassShell.background: WindowBackground
*AirmassShell.geometry: "+100+100"

*form4.class: form
*form4.parent: AirmassShell
*form4.static: true
*form4.name: form4
*form4.resizePolicy: "resize_none"
*form4.unitType: "pixels"
*form4.x: 2
*form4.y: 2
*form4.width: 420
*form4.height: 248
*form4.background: WindowBackground

*scrolledWindow2.class: scrolledWindow
*scrolledWindow2.parent: form4
*scrolledWindow2.static: true
*scrolledWindow2.name: scrolledWindow2
*scrolledWindow2.scrollingPolicy: "automatic"
*scrolledWindow2.x: 0
*scrolledWindow2.y: 2
*scrolledWindow2.width: 424
*scrolledWindow2.height: 211
*scrolledWindow2.scrollBarPlacement: "bottom_left"
*scrolledWindow2.background: WindowBackground

*rowColumn2.class: rowColumn
*rowColumn2.parent: scrolledWindow2
*rowColumn2.static: true
*rowColumn2.name: rowColumn2
*rowColumn2.x: 59
*rowColumn2.y: 53
*rowColumn2.width: 394
*rowColumn2.height: 202
*rowColumn2.orientation: "horizontal"
*rowColumn2.background: WindowBackground
*rowColumn2.entryAlignment: "alignment_beginning"

*t_airmass_name.class: text
*t_airmass_name.parent: rowColumn2
*t_airmass_name.static: true
*t_airmass_name.name: t_airmass_name
*t_airmass_name.x: 3
*t_airmass_name.y: 7
*t_airmass_name.width: 298
*t_airmass_name.height: 781
*t_airmass_name.background: TextBackground
*t_airmass_name.fontList: SmallFont
*t_airmass_name.editMode: "multi_line_edit"
*t_airmass_name.cursorPositionVisible: "false"
*t_airmass_name.foreground: TextForeground
*t_airmass_name.editable: "false"

*t_airmass_value.class: text
*t_airmass_value.parent: rowColumn2
*t_airmass_value.static: true
*t_airmass_value.name: t_airmass_value
*t_airmass_value.x: 304
*t_airmass_value.y: 4
*t_airmass_value.width: 90
*t_airmass_value.height: 524
*t_airmass_value.fontList: SmallFont
*t_airmass_value.background: TextBackground
*t_airmass_value.editMode: "multi_line_edit"
*t_airmass_value.foreground: TextForeground

*pb_airmass_ok.class: pushButton
*pb_airmass_ok.parent: form4
*pb_airmass_ok.static: true
*pb_airmass_ok.name: pb_airmass_ok
*pb_airmass_ok.x: 19
*pb_airmass_ok.y: 216
*pb_airmass_ok.width: 82
*pb_airmass_ok.height: 32
*pb_airmass_ok.background: ButtonBackground
*pb_airmass_ok.labelString: "Ok"
*pb_airmass_ok.fontList: BoldTextFont
*pb_airmass_ok.activateCallback: {\
extern int ExecuteCaller;\
\
UxPopdownInterface(UxFindSwidget("AirmassShell"));\
UpdateAirmassValues();\
if ( ExecuteCaller )\
    ExecuteReduce();\
}
*pb_airmass_ok.highlightColor: "red"
*pb_airmass_ok.highlightOnEnter: "true"
*pb_airmass_ok.foreground: ApplyForeground

*pb_airmass_cancel.class: pushButton
*pb_airmass_cancel.parent: form4
*pb_airmass_cancel.static: true
*pb_airmass_cancel.name: pb_airmass_cancel
*pb_airmass_cancel.x: 108
*pb_airmass_cancel.y: 216
*pb_airmass_cancel.width: 82
*pb_airmass_cancel.height: 32
*pb_airmass_cancel.background: ButtonBackground
*pb_airmass_cancel.fontList: BoldTextFont
*pb_airmass_cancel.labelString: "Cancel"
*pb_airmass_cancel.activateCallback: {\
UxPopdownInterface(UxFindSwidget("AirmassShell"));\
\
}
*pb_airmass_cancel.highlightColor: ButtonForeground
*pb_airmass_cancel.highlightOnEnter: "true"
*pb_airmass_cancel.foreground: CancelForeground

