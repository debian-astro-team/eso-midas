! UIMX ascii 2.0 key: 1637                                                      

*file_dialog.class: applicationShell
*file_dialog.parent: NO_PARENT
*file_dialog.static: true
*file_dialog.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*file_dialog.ispecdecl:
*file_dialog.funcdecl: swidget create_file_dialog()\

*file_dialog.funcname: create_file_dialog
*file_dialog.funcdef: "swidget", "<create_file_dialog>(%)"
*file_dialog.icode:
*file_dialog.fcode: return(rtrn);\

*file_dialog.auxdecl:
*file_dialog.name: file_dialog
*file_dialog.x: 678
*file_dialog.y: 312
*file_dialog.width: 408
*file_dialog.height: 105
*file_dialog.title: ""
*file_dialog.keyboardFocusPolicy: "pointer"
*file_dialog.geometry: "+100+100"
*file_dialog.background: WindowBackground

*form3.class: form
*form3.parent: file_dialog
*form3.static: true
*form3.name: form3
*form3.resizePolicy: "resize_none"
*form3.unitType: "pixels"
*form3.x: 0
*form3.y: 0
*form3.width: 408
*form3.height: 348
*form3.background: WindowBackground

*form5.class: form
*form5.parent: form3
*form5.static: true
*form5.name: form5
*form5.resizePolicy: "resize_none"
*form5.x: 0
*form5.y: 62
*form5.width: 452
*form5.height: 40
*form5.background: ButtonBackground

*pushButton2.class: pushButton
*pushButton2.parent: form5
*pushButton2.static: true
*pushButton2.name: pushButton2
*pushButton2.x: 8
*pushButton2.y: 4
*pushButton2.width: 80
*pushButton2.height: 30
*pushButton2.background: ButtonBackground
*pushButton2.fontList: BoldTextFont
*pushButton2.foreground: ApplyForeground
*pushButton2.labelString: "Ok"
*pushButton2.activateCallback: {\
CallbackDialog();\
}

*pushButton3.class: pushButton
*pushButton3.parent: form5
*pushButton3.static: true
*pushButton3.name: pushButton3
*pushButton3.x: 100
*pushButton3.y: 4
*pushButton3.width: 80
*pushButton3.height: 30
*pushButton3.background: ButtonBackground
*pushButton3.fontList: BoldTextFont
*pushButton3.foreground: CancelForeground
*pushButton3.labelString: "Cancel"
*pushButton3.activateCallback: {\
UxPopdownInterface(UxFindSwidget("file_dialog"));\
}

*lb_file_dialog.class: label
*lb_file_dialog.parent: form3
*lb_file_dialog.static: true
*lb_file_dialog.name: lb_file_dialog
*lb_file_dialog.x: 10
*lb_file_dialog.y: 12
*lb_file_dialog.width: 176
*lb_file_dialog.height: 30
*lb_file_dialog.background: LabelBackground
*lb_file_dialog.fontList: TextFont
*lb_file_dialog.labelString: "Output parameters file :"
*lb_file_dialog.alignment: "alignment_beginning"
*lb_file_dialog.foreground: TextForeground

*tf_file_dialog.class: textField
*tf_file_dialog.parent: form3
*tf_file_dialog.static: true
*tf_file_dialog.name: tf_file_dialog
*tf_file_dialog.x: 190
*tf_file_dialog.y: 10
*tf_file_dialog.width: 210
*tf_file_dialog.height: 34
*tf_file_dialog.background: TextBackground
*tf_file_dialog.fontList: TextFont
*tf_file_dialog.highlightOnEnter: "true"
*tf_file_dialog.foreground: TextForeground
*tf_file_dialog.losingFocusCallback: {\
char *text;\
extern char Session[];\
\
text = XmTextGetString(UxWidget);\
strcpy(Session, text);\
\
XtFree(text);\
}
*tf_file_dialog.activateCallback: {\
CallbackDialog();\
}

