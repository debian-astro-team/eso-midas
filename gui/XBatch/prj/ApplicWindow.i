! UIMX ascii 2.0 key: 999                                                       
*action.FileSelectACT: {\
#include <spec_comm.h>\
	\
extern swidget TextFieldSwidget;\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_inputf")) )\
    PopupList(LIST_INPUTF);\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_bias")) )\
    PopupList(LIST_BIAS);\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_dark")) )\
    PopupList(LIST_DARK);\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flat")) )\
    PopupList(LIST_FLAT);\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_longsess")) )\
    PopupList(LIST_LONGSESS);\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_redextab")) )\
    PopupList(LIST_REDEXTAB);\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_redresp")) )\
    PopupList(LIST_REDRESP);\
}
*action.WriteHelp: {\
DisplayShortHelp(UxWidget);\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.ClearHelp: {\
UxPutText(UxFindSwidget("short_help"), "");\
}

*translation.table: FileSelectMain
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*ApplicWindow.class: applicationShell
*ApplicWindow.parent: NO_PARENT
*ApplicWindow.static: true
*ApplicWindow.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*ApplicWindow.ispecdecl:
*ApplicWindow.funcdecl: swidget create_ApplicWindow()\

*ApplicWindow.funcname: create_ApplicWindow
*ApplicWindow.funcdef: "swidget", "<create_ApplicWindow>(%)"
*ApplicWindow.icode: extern swidget create_file_dialog();\
extern swidget create_AirmassShell();\
extern swidget create_HelpShell();
*ApplicWindow.fcode: create_file_dialog();\
create_HelpShell();\
create_AirmassShell();\
\
return(rtrn);\

*ApplicWindow.auxdecl:
*ApplicWindow.name: ApplicWindow
*ApplicWindow.x: 2
*ApplicWindow.y: 21
*ApplicWindow.width: 447
*ApplicWindow.height: 643
*ApplicWindow.iconName: "Batch reduction"
*ApplicWindow.keyboardFocusPolicy: "pointer"
*ApplicWindow.title: "Batch reduction"
*ApplicWindow.geometry: "+0+0"

*MainWindow.class: mainWindow
*MainWindow.parent: ApplicWindow
*MainWindow.static: true
*MainWindow.name: MainWindow
*MainWindow.unitType: "pixels"
*MainWindow.x: 10
*MainWindow.y: 0
*MainWindow.width: 440
*MainWindow.height: 460
*MainWindow.background: WindowBackground

*menu1.class: rowColumn
*menu1.parent: MainWindow
*menu1.static: true
*menu1.name: menu1
*menu1.rowColumnType: "menu_bar"
*menu1.menuAccelerator: "<KeyUp>F10"
*menu1.menuHelpWidget: "menu1_top_b4"
*menu1.background: MenuBackground

*menu1_p1.class: rowColumn
*menu1_p1.parent: menu1
*menu1_p1.static: true
*menu1_p1.name: menu1_p1
*menu1_p1.rowColumnType: "menu_pulldown"
*menu1_p1.background: MenuBackground
*menu1_p1.foreground: MenuForeground

*menu1_p1_b1.class: pushButtonGadget
*menu1_p1_b1.parent: menu1_p1
*menu1_p1_b1.static: true
*menu1_p1_b1.name: menu1_p1_b1
*menu1_p1_b1.labelString: "Open"
*menu1_p1_b1.mnemonic: "O"
*menu1_p1_b1.fontList: BoldTextFont
*menu1_p1_b1.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_REDSESS);\
}

*menu1_p1_b2.class: pushButtonGadget
*menu1_p1_b2.parent: menu1_p1
*menu1_p1_b2.static: true
*menu1_p1_b2.name: menu1_p1_b2
*menu1_p1_b2.labelString: "Save"
*menu1_p1_b2.mnemonic: "S"
*menu1_p1_b2.fontList: BoldTextFont
*menu1_p1_b2.activateCallback: {\
#include <spec_comm.h>\
\
extern char Redsess[];\
char command[256];\
\
sprintf(command, "%s%s", C_REDSAVE, Redsess);\
AppendDialogText(command);\
}

*menu1_p1_b3.class: pushButtonGadget
*menu1_p1_b3.parent: menu1_p1
*menu1_p1_b3.static: true
*menu1_p1_b3.name: menu1_p1_b3
*menu1_p1_b3.labelString: "Save As ..."
*menu1_p1_b3.mnemonic: "S"
*menu1_p1_b3.fontList: BoldTextFont
*menu1_p1_b3.activateCallback: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern int DialogType;\
extern char Redsess[];\
\
SET_DIALOG_PROMPT("Output parameters table :");\
\
XmTextSetString(UxGetWidget(UxFindSwidget("tf_file_dialog")), Redsess);\
DialogType = DIALOG_REDSESS;\
UxPopupInterface(UxFindSwidget("file_dialog"), exclusive_grab);\
}

*menu1_p1_b4.class: separator
*menu1_p1_b4.parent: menu1_p1
*menu1_p1_b4.static: true
*menu1_p1_b4.name: menu1_p1_b4

*menu1_p1_b5.class: pushButtonGadget
*menu1_p1_b5.parent: menu1_p1
*menu1_p1_b5.static: true
*menu1_p1_b5.name: menu1_p1_b5
*menu1_p1_b5.labelString: "Exit"
*menu1_p1_b5.accelerator: "E"
*menu1_p1_b5.activateCallback: {\
SCSEPI();\
exit(0);\
}
*menu1_p1_b5.fontList: BoldTextFont

*menu1_p4.class: rowColumn
*menu1_p4.parent: menu1
*menu1_p4.static: true
*menu1_p4.name: menu1_p4
*menu1_p4.rowColumnType: "menu_pulldown"
*menu1_p4.background: MenuBackground
*menu1_p4.foreground: MenuForeground

*menu_help_window.class: pushButtonGadget
*menu_help_window.parent: menu1_p4
*menu_help_window.static: true
*menu_help_window.name: menu_help_window
*menu_help_window.labelString: "On Window ..."
*menu_help_window.mnemonic: "W"
*menu_help_window.fontList: BoldTextFont
*menu_help_window.activateCallback: DisplayExtendedHelp(UxWidget);

*menu1_top_b1.class: cascadeButton
*menu1_top_b1.parent: menu1
*menu1_top_b1.static: true
*menu1_top_b1.name: menu1_top_b1
*menu1_top_b1.labelString: "File"
*menu1_top_b1.mnemonic: "F"
*menu1_top_b1.subMenuId: "menu1_p1"
*menu1_top_b1.background: MenuBackground
*menu1_top_b1.fontList: BoldTextFont
*menu1_top_b1.foreground: MenuForeground

*menu1_top_b4.class: cascadeButton
*menu1_top_b4.parent: menu1
*menu1_top_b4.static: true
*menu1_top_b4.name: menu1_top_b4
*menu1_top_b4.labelString: "Help"
*menu1_top_b4.mnemonic: "H"
*menu1_top_b4.subMenuId: "menu1_p4"
*menu1_top_b4.background: MenuBackground
*menu1_top_b4.fontList: BoldTextFont
*menu1_top_b4.foreground: MenuForeground

*form6.class: form
*form6.parent: MainWindow
*form6.static: true
*form6.name: form6
*form6.background: WindowBackground

*separator1.class: separator
*separator1.parent: form6
*separator1.static: true
*separator1.name: separator1
*separator1.x: -4
*separator1.y: 496
*separator1.width: 450
*separator1.height: 6
*separator1.background: LabelBackground

*short_help.class: text
*short_help.parent: form6
*short_help.static: true
*short_help.name: short_help
*short_help.x: -2
*short_help.y: 502
*short_help.width: 448
*short_help.height: 50
*short_help.background: SHelpBackground
*short_help.cursorPositionVisible: "false"
*short_help.editable: "false"
*short_help.fontList: TextFont

*separator2.class: separator
*separator2.parent: form6
*separator2.static: true
*separator2.name: separator2
*separator2.x: -4
*separator2.y: 552
*separator2.width: 450
*separator2.height: 6
*separator2.background: LabelBackground

*label2.class: label
*label2.parent: form6
*label2.static: true
*label2.name: label2
*label2.x: 10
*label2.y: 38
*label2.width: 54
*label2.height: 20
*label2.labelString: "Prefix  /"
*label2.background: ApplicBackground
*label2.fontList: TextFont
*label2.foreground: TextForeground

*label3.class: label
*label3.parent: form6
*label3.static: true
*label3.name: label3
*label3.x: 238
*label3.y: 36
*label3.width: 70
*label3.height: 25
*label3.labelString: "Numbers :"
*label3.background: ApplicBackground
*label3.fontList: TextFont
*label3.foreground: TextForeground

*tg_biasopt.class: toggleButton
*tg_biasopt.parent: form6
*tg_biasopt.static: true
*tg_biasopt.name: tg_biasopt
*tg_biasopt.x: 16
*tg_biasopt.y: 72
*tg_biasopt.width: 68
*tg_biasopt.height: 30
*tg_biasopt.labelString: "Bias"
*tg_biasopt.background: ApplicBackground
*tg_biasopt.alignment: "alignment_beginning"
*tg_biasopt.fontList: TextFont
*tg_biasopt.set: "false"
*tg_biasopt.highlightColor: "black"
*tg_biasopt.selectColor: "green"
*tg_biasopt.indicatorSize: 18
*tg_biasopt.highlightOnEnter: "true"
*tg_biasopt.foreground: TextForeground
*tg_biasopt.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("YES", K_BIASOPT);\
else\
    WriteKeyword("NO ", K_BIASOPT);\
\
}

*tg_darkopt.class: toggleButton
*tg_darkopt.parent: form6
*tg_darkopt.static: true
*tg_darkopt.name: tg_darkopt
*tg_darkopt.x: 16
*tg_darkopt.y: 112
*tg_darkopt.width: 68
*tg_darkopt.height: 30
*tg_darkopt.labelString: "Dark"
*tg_darkopt.background: ApplicBackground
*tg_darkopt.alignment: "alignment_beginning"
*tg_darkopt.fontList: TextFont
*tg_darkopt.set: "false"
*tg_darkopt.highlightColor: "black"
*tg_darkopt.selectColor: "green"
*tg_darkopt.indicatorSize: 18
*tg_darkopt.highlightOnEnter: "true"
*tg_darkopt.foreground: TextForeground
*tg_darkopt.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("YES", K_DARKOPT);\
else\
    WriteKeyword("NO ", K_DARKOPT);\
\
}

*tg_flatopt.class: toggleButton
*tg_flatopt.parent: form6
*tg_flatopt.static: true
*tg_flatopt.name: tg_flatopt
*tg_flatopt.x: 16
*tg_flatopt.y: 152
*tg_flatopt.width: 86
*tg_flatopt.height: 30
*tg_flatopt.labelString: "Flat field"
*tg_flatopt.background: ApplicBackground
*tg_flatopt.alignment: "alignment_beginning"
*tg_flatopt.fontList: TextFont
*tg_flatopt.set: "false"
*tg_flatopt.highlightColor: "black"
*tg_flatopt.selectColor: "green"
*tg_flatopt.indicatorSize: 18
*tg_flatopt.highlightOnEnter: "true"
*tg_flatopt.foreground: TextForeground
*tg_flatopt.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("YES", K_FLATOPT);\
else\
    WriteKeyword("NO ", K_FLATOPT);\
\
}

*tg_rebopt.class: toggleButton
*tg_rebopt.parent: form6
*tg_rebopt.static: true
*tg_rebopt.name: tg_rebopt
*tg_rebopt.x: 16
*tg_rebopt.y: 287
*tg_rebopt.width: 70
*tg_rebopt.height: 30
*tg_rebopt.set: "false"
*tg_rebopt.labelString: "Rebin"
*tg_rebopt.background: ApplicBackground
*tg_rebopt.alignment: "alignment_beginning"
*tg_rebopt.fontList: TextFont
*tg_rebopt.highlightColor: "black"
*tg_rebopt.selectColor: "green"
*tg_rebopt.indicatorSize: 18
*tg_rebopt.highlightOnEnter: "true"
*tg_rebopt.foreground: TextForeground
*tg_rebopt.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("YES", K_REBOPT);\
else\
    WriteKeyword("NO ", K_REBOPT);\
\
}

*label8.class: label
*label8.parent: form6
*label8.static: true
*label8.name: label8
*label8.x: 104
*label8.y: 280
*label8.width: 128
*label8.height: 25
*label8.labelString: "Parameters table :"
*label8.background: ApplicBackground
*label8.fontList: TextFont
*label8.foreground: TextForeground

*rowColumn1.class: rowColumn
*rowColumn1.parent: form6
*rowColumn1.static: true
*rowColumn1.name: rowColumn1
*rowColumn1.x: 106
*rowColumn1.y: 310
*rowColumn1.width: 318
*rowColumn1.height: 34
*rowColumn1.orientation: "horizontal"
*rowColumn1.radioBehavior: "true"
*rowColumn1.background: ApplicBackground
*rowColumn1.packing: "pack_tight"

*tg_linear.class: toggleButton
*tg_linear.parent: rowColumn1
*tg_linear.static: true
*tg_linear.name: tg_linear
*tg_linear.x: 3
*tg_linear.y: 3
*tg_linear.width: 67
*tg_linear.height: 21
*tg_linear.indicatorType: "one_of_many"
*tg_linear.labelString: "LINEAR"
*tg_linear.set: "true"
*tg_linear.background: ApplicBackground
*tg_linear.fontList: TextFont
*tg_linear.selectColor: "green"
*tg_linear.highlightOnEnter: "true"
*tg_linear.foreground: TextForeground
*tg_linear.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("LINEAR", K_REDRBMTD);\
\
}

*tg_quadratic.class: toggleButton
*tg_quadratic.parent: rowColumn1
*tg_quadratic.static: true
*tg_quadratic.name: tg_quadratic
*tg_quadratic.x: 75
*tg_quadratic.y: 0
*tg_quadratic.width: 89
*tg_quadratic.height: 25
*tg_quadratic.indicatorType: "one_of_many"
*tg_quadratic.labelString: "CUADRATIC"
*tg_quadratic.background: ApplicBackground
*tg_quadratic.fontList: TextFont
*tg_quadratic.selectColor: "green"
*tg_quadratic.highlightOnEnter: "true"
*tg_quadratic.foreground: TextForeground
*tg_quadratic.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
      WriteKeyword("QUADRATIC", K_REDRBMTD);\
\
}

*tg_spline.class: toggleButton
*tg_spline.parent: rowColumn1
*tg_spline.static: true
*tg_spline.name: tg_spline
*tg_spline.x: 235
*tg_spline.y: 3
*tg_spline.width: 89
*tg_spline.height: 28
*tg_spline.indicatorType: "one_of_many"
*tg_spline.labelString: "SPLINE"
*tg_spline.background: ApplicBackground
*tg_spline.fontList: TextFont
*tg_spline.selectColor: "green"
*tg_spline.highlightOnEnter: "true"
*tg_spline.foreground: TextForeground
*tg_spline.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
      WriteKeyword("SPLINE", K_REDRBMTD);\
\
}

*tg_respopt.class: toggleButton
*tg_respopt.parent: form6
*tg_respopt.static: true
*tg_respopt.name: tg_respopt
*tg_respopt.x: 16
*tg_respopt.y: 391
*tg_respopt.width: 65
*tg_respopt.height: 28
*tg_respopt.labelString: "Flux"
*tg_respopt.background: ApplicBackground
*tg_respopt.alignment: "alignment_beginning"
*tg_respopt.fontList: TextFont
*tg_respopt.set: "false"
*tg_respopt.highlightColor: "black"
*tg_respopt.selectColor: "green"
*tg_respopt.indicatorSize: 18
*tg_respopt.highlightOnEnter: "true"
*tg_respopt.foreground: TextForeground
*tg_respopt.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("YES", K_RESPOPT);\
else\
    WriteKeyword("NO ", K_RESPOPT);\
\
}

*tg_extopt.class: toggleButton
*tg_extopt.parent: form6
*tg_extopt.static: true
*tg_extopt.name: tg_extopt
*tg_extopt.x: 16
*tg_extopt.y: 350
*tg_extopt.width: 85
*tg_extopt.height: 31
*tg_extopt.labelString: "Extinct"
*tg_extopt.background: ApplicBackground
*tg_extopt.alignment: "alignment_beginning"
*tg_extopt.fontList: TextFont
*tg_extopt.set: "false"
*tg_extopt.highlightColor: "black"
*tg_extopt.selectColor: "green"
*tg_extopt.indicatorSize: 18
*tg_extopt.highlightOnEnter: "true"
*tg_extopt.foreground: TextForeground
*tg_extopt.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("YES", K_EXTOPT);\
else\
    WriteKeyword("NO ", K_EXTOPT);\
\
}

*label9.class: label
*label9.parent: form6
*label9.static: true
*label9.name: label9
*label9.x: 108
*label9.y: 397
*label9.width: 115
*label9.height: 20
*label9.labelString: "Response curve :"
*label9.background: ApplicBackground
*label9.fontList: TextFont
*label9.foreground: TextForeground

*label10.class: label
*label10.parent: form6
*label10.static: true
*label10.name: label10
*label10.x: 104
*label10.y: 356
*label10.width: 98
*label10.height: 20
*label10.labelString: "Extinct table :"
*label10.background: ApplicBackground
*label10.fontList: TextFont
*label10.foreground: TextForeground

*label11.class: label
*label11.parent: form6
*label11.static: true
*label11.name: label11
*label11.x: 0
*label11.y: 430
*label11.width: 123
*label11.height: 20
*label11.labelString: "OUTPUT FILES"
*label11.background: ApplicBackground
*label11.fontList: TextFont
*label11.foreground: TextForeground

*label12.class: label
*label12.parent: form6
*label12.static: true
*label12.name: label12
*label12.x: 10
*label12.y: 460
*label12.width: 59
*label12.height: 20
*label12.labelString: "Prefix :"
*label12.background: ApplicBackground
*label12.fontList: TextFont
*label12.foreground: TextForeground

*label13.class: label
*label13.parent: form6
*label13.static: true
*label13.name: label13
*label13.x: 191
*label13.y: 460
*label13.width: 81
*label13.height: 20
*label13.labelString: "Initial file # :"
*label13.background: ApplicBackground
*label13.fontList: TextFont
*label13.foreground: TextForeground

*tg_rotopt.class: toggleButton
*tg_rotopt.parent: form6
*tg_rotopt.static: true
*tg_rotopt.name: tg_rotopt
*tg_rotopt.x: 16
*tg_rotopt.y: 194
*tg_rotopt.width: 72
*tg_rotopt.height: 30
*tg_rotopt.labelString: "Rotate"
*tg_rotopt.background: ApplicBackground
*tg_rotopt.alignment: "alignment_beginning"
*tg_rotopt.fontList: TextFont
*tg_rotopt.set: "false"
*tg_rotopt.highlightColor: "black"
*tg_rotopt.selectColor: "green"
*tg_rotopt.indicatorSize: 18
*tg_rotopt.highlightOnEnter: "true"
*tg_rotopt.foreground: TextForeground
*tg_rotopt.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("YES", K_ROTOPT);\
else\
    WriteKeyword("NO ", K_ROTOPT);\
\
}

*tg_trimopt.class: toggleButton
*tg_trimopt.parent: form6
*tg_trimopt.static: true
*tg_trimopt.name: tg_trimopt
*tg_trimopt.x: 16
*tg_trimopt.y: 234
*tg_trimopt.width: 62
*tg_trimopt.height: 26
*tg_trimopt.labelString: "Trim"
*tg_trimopt.background: ApplicBackground
*tg_trimopt.alignment: "alignment_beginning"
*tg_trimopt.fontList: TextFont
*tg_trimopt.set: "false"
*tg_trimopt.highlightColor: "black"
*tg_trimopt.selectColor: "green"
*tg_trimopt.indicatorSize: 18
*tg_trimopt.highlightOnEnter: "true"
*tg_trimopt.foreground: TextForeground
*tg_trimopt.valueChangedCallback: {\
#include <spec_comm.h>\
\
if ( XmToggleButtonGetState(UxWidget) )\
    WriteKeyword("YES", K_TRIMOPT);\
else\
    WriteKeyword("NO ", K_TRIMOPT);\
\
}

*tf_inputf.class: textField
*tf_inputf.parent: form6
*tf_inputf.static: true
*tf_inputf.name: tf_inputf
*tf_inputf.x: 134
*tf_inputf.y: 32
*tf_inputf.width: 104
*tf_inputf.height: 34
*tf_inputf.background: TextBackground
*tf_inputf.fontList: TextFont
*tf_inputf.translations: FileSelectMain
*tf_inputf.maxLength: 15
*tf_inputf.columns: 128
*tf_inputf.highlightOnEnter: "true"
*tf_inputf.foreground: TextForeground
*tf_inputf.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Inputf[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Inputf) ) {\
    strcpy(Inputf, text);\
    WriteKeyword(text, K_INPUTF);\
}\
\
XtFree(text);\
}

*tf_inpnumb.class: textField
*tf_inpnumb.parent: form6
*tf_inpnumb.static: true
*tf_inpnumb.name: tf_inpnumb
*tf_inpnumb.x: 308
*tf_inpnumb.y: 32
*tf_inpnumb.width: 128
*tf_inpnumb.height: 34
*tf_inpnumb.background: TextBackground
*tf_inpnumb.fontList: TextFont
*tf_inpnumb.maxLength: 18
*tf_inpnumb.columns: 128
*tf_inpnumb.highlightOnEnter: "true"
*tf_inpnumb.foreground: TextForeground
*tf_inpnumb.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text, valok[256];\
int i, j = 0;\
extern char Inpnumb[];\
\
text = XmTextGetString(UxWidget);\
\
for ( i = 0; text[i] != '\0'; i++ ) \
    if ( text[i] != ' ' && (text[i] == ',' || text[i] == '-' || \
        (text[i] <= '9' && text[i] >= '0')) )\
        valok[j++] = text[i];\
valok[j] = '\0';\
\
if ( strcmp(valok, Inpnumb) ) {\
    strcpy(Inpnumb, valok);\
    WriteKeyword(valok, K_INPNUMB);\
    UxPutText(UxThisWidget, valok);\
}\
\
XtFree(text);\
}

*tf_bias.class: textField
*tf_bias.parent: form6
*tf_bias.static: true
*tf_bias.name: tf_bias
*tf_bias.x: 104
*tf_bias.y: 74
*tf_bias.width: 178
*tf_bias.height: 34
*tf_bias.background: TextBackground
*tf_bias.fontList: TextFont
*tf_bias.translations: FileSelectMain
*tf_bias.maxLength: 20
*tf_bias.highlightOnEnter: "true"
*tf_bias.foreground: TextForeground
*tf_bias.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Bias[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Bias) ) {\
    strcpy(Bias, text);\
    WriteKeyword(text, K_BIAS);\
}\
\
XtFree(text);\
}
*tf_bias.columns: 128

*tf_dark.class: textField
*tf_dark.parent: form6
*tf_dark.static: true
*tf_dark.name: tf_dark
*tf_dark.x: 104
*tf_dark.y: 110
*tf_dark.width: 178
*tf_dark.height: 34
*tf_dark.background: TextBackground
*tf_dark.fontList: TextFont
*tf_dark.translations: FileSelectMain
*tf_dark.maxLength: 20
*tf_dark.highlightOnEnter: "true"
*tf_dark.foreground: TextForeground
*tf_dark.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Dark[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Dark) ) {\
    strcpy(Dark, text);\
    WriteKeyword(text, K_DARK);\
}\
\
XtFree(text);\
}
*tf_dark.columns: 128

*tf_flat.class: textField
*tf_flat.parent: form6
*tf_flat.static: true
*tf_flat.name: tf_flat
*tf_flat.x: 104
*tf_flat.y: 148
*tf_flat.width: 178
*tf_flat.height: 34
*tf_flat.background: TextBackground
*tf_flat.fontList: TextFont
*tf_flat.translations: FileSelectMain
*tf_flat.maxLength: 20
*tf_flat.highlightOnEnter: "true"
*tf_flat.foreground: TextForeground
*tf_flat.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Flat[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Flat) ) {\
    strcpy(Flat, text);\
    WriteKeyword(text, K_FLAT);\
}\
\
XtFree(text);\
}
*tf_flat.columns: 128

*tf_trim4.class: textField
*tf_trim4.parent: form6
*tf_trim4.static: true
*tf_trim4.name: tf_trim4
*tf_trim4.x: 374
*tf_trim4.y: 230
*tf_trim4.width: 60
*tf_trim4.height: 34
*tf_trim4.background: TextBackground
*tf_trim4.fontList: TextFont
*tf_trim4.maxLength: 20
*tf_trim4.highlightOnEnter: "true"
*tf_trim4.foreground: TextForeground
*tf_trim4.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Trim[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Trim[3] ) {\
    Trim[3] = val;\
    WriteKeyword(text, K_TRIM4);\
}\
\
XtFree(text);\
}

*tf_longsess.class: textField
*tf_longsess.parent: form6
*tf_longsess.static: true
*tf_longsess.name: tf_longsess
*tf_longsess.x: 232
*tf_longsess.y: 276
*tf_longsess.width: 178
*tf_longsess.height: 34
*tf_longsess.background: TextBackground
*tf_longsess.fontList: TextFont
*tf_longsess.translations: FileSelectMain
*tf_longsess.maxLength: 20
*tf_longsess.highlightOnEnter: "true"
*tf_longsess.foreground: TextForeground
*tf_longsess.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Longsess[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Longsess) ) {\
    strcpy(Longsess, text);\
    WriteKeyword(text, K_LONGSESS);\
}\
\
XtFree(text);\
}
*tf_longsess.columns: 128

*tf_redextab.class: textField
*tf_redextab.parent: form6
*tf_redextab.static: true
*tf_redextab.name: tf_redextab
*tf_redextab.x: 232
*tf_redextab.y: 350
*tf_redextab.width: 178
*tf_redextab.height: 34
*tf_redextab.background: TextBackground
*tf_redextab.fontList: TextFont
*tf_redextab.translations: FileSelectMain
*tf_redextab.text: ""
*tf_redextab.maxLength: 20
*tf_redextab.highlightOnEnter: "true"
*tf_redextab.foreground: TextForeground
*tf_redextab.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Redextab[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Redextab) ) {\
    strcpy(Redextab, text);\
    WriteKeyword(text, K_REDEXTAB);\
}\
\
XtFree(text);\
}
*tf_redextab.columns: 128

*tf_redresp.class: textField
*tf_redresp.parent: form6
*tf_redresp.static: true
*tf_redresp.name: tf_redresp
*tf_redresp.x: 232
*tf_redresp.y: 390
*tf_redresp.width: 178
*tf_redresp.height: 34
*tf_redresp.background: TextBackground
*tf_redresp.fontList: TextFont
*tf_redresp.translations: FileSelectMain
*tf_redresp.maxLength: 20
*tf_redresp.highlightOnEnter: "true"
*tf_redresp.foreground: TextForeground
*tf_redresp.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Redresp[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Redresp) ) {\
    strcpy(Redresp, text);\
    WriteKeyword(text, K_REDRESP);\
}\
\
XtFree(text);\
}
*tf_redresp.columns: 128

*tf_rotstart.class: textField
*tf_rotstart.parent: form6
*tf_rotstart.static: true
*tf_rotstart.name: tf_rotstart
*tf_rotstart.x: 218
*tf_rotstart.y: 190
*tf_rotstart.width: 72
*tf_rotstart.height: 34
*tf_rotstart.background: TextBackground
*tf_rotstart.fontList: TextFont
*tf_rotstart.columns: 15
*tf_rotstart.maxLength: 15
*tf_rotstart.highlightOnEnter: "true"
*tf_rotstart.foreground: TextForeground
*tf_rotstart.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
double val;\
extern double Rotstart;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%lf", &val);\
\
if ( val != Rotstart ) {\
    Rotstart = val;\
    WriteKeyword(text, K_ROTSTART);\
}\
\
XtFree(text);\
}

*tf_outputf.class: textField
*tf_outputf.parent: form6
*tf_outputf.static: true
*tf_outputf.name: tf_outputf
*tf_outputf.x: 74
*tf_outputf.y: 452
*tf_outputf.width: 100
*tf_outputf.height: 34
*tf_outputf.background: TextBackground
*tf_outputf.fontList: TextFont
*tf_outputf.columns: 128
*tf_outputf.maxLength: 10
*tf_outputf.highlightOnEnter: "true"
*tf_outputf.foreground: TextForeground
*tf_outputf.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
extern char Outputf[];\
\
text = XmTextGetString(UxWidget);\
\
if ( strcmp(text, Outputf) ) {\
    strcpy(Outputf, text);\
    WriteKeyword(text, K_OUTPUTF);\
}\
\
XtFree(text);\
}

*tf_outnumb.class: textField
*tf_outnumb.parent: form6
*tf_outnumb.static: true
*tf_outnumb.name: tf_outnumb
*tf_outnumb.x: 276
*tf_outnumb.y: 452
*tf_outnumb.width: 67
*tf_outnumb.height: 34
*tf_outnumb.background: TextBackground
*tf_outnumb.fontList: TextFont
*tf_outnumb.text: ""
*tf_outnumb.columns: 6
*tf_outnumb.maxLength: 6
*tf_outnumb.highlightOnEnter: "true"
*tf_outnumb.foreground: TextForeground
*tf_outnumb.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Outnumb;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Outnumb ) {\
    Outnumb = val;\
    WriteKeyword(text, K_OUTNUMB);\
}\
\
XtFree(text);\
}

*label5.class: label
*label5.parent: form6
*label5.static: true
*label5.name: label5
*label5.x: 64
*label5.y: 38
*label5.width: 68
*label5.height: 20
*label5.background: ApplicBackground
*label5.fontList: TextFont
*label5.labelString: "Catalog :"
*label5.foreground: TextForeground

*tf_trim1.class: textField
*tf_trim1.parent: form6
*tf_trim1.static: true
*tf_trim1.name: tf_trim1
*tf_trim1.x: 194
*tf_trim1.y: 230
*tf_trim1.width: 60
*tf_trim1.height: 34
*tf_trim1.background: TextBackground
*tf_trim1.fontList: TextFont
*tf_trim1.maxLength: 20
*tf_trim1.highlightOnEnter: "true"
*tf_trim1.foreground: TextForeground
*tf_trim1.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Trim[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Trim[0] ) {\
    Trim[0] = val;\
    WriteKeyword(text, K_TRIM1);\
}\
\
XtFree(text);\
}

*tf_trim2.class: textField
*tf_trim2.parent: form6
*tf_trim2.static: true
*tf_trim2.name: tf_trim2
*tf_trim2.x: 254
*tf_trim2.y: 230
*tf_trim2.width: 60
*tf_trim2.height: 34
*tf_trim2.background: TextBackground
*tf_trim2.fontList: TextFont
*tf_trim2.maxLength: 20
*tf_trim2.highlightOnEnter: "true"
*tf_trim2.foreground: TextForeground
*tf_trim2.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Trim[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Trim[1] ) {\
    Trim[1] = val;\
    WriteKeyword(text, K_TRIM2);\
}\
\
XtFree(text);\
}

*tf_trim3.class: textField
*tf_trim3.parent: form6
*tf_trim3.static: true
*tf_trim3.name: tf_trim3
*tf_trim3.x: 314
*tf_trim3.y: 230
*tf_trim3.width: 60
*tf_trim3.height: 34
*tf_trim3.background: TextBackground
*tf_trim3.fontList: TextFont
*tf_trim3.maxLength: 20
*tf_trim3.highlightOnEnter: "true"
*tf_trim3.foreground: TextForeground
*tf_trim3.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Trim[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Trim[2] ) {\
    Trim[2] = val;\
    WriteKeyword(text, K_TRIM3);\
}\
\
XtFree(text);\
}

*label6.class: label
*label6.parent: form6
*label6.static: true
*label6.name: label6
*label6.x: 106
*label6.y: 236
*label6.width: 84
*label6.height: 25
*label6.labelString: "x1,y1,x2,y2 :"
*label6.background: ApplicBackground
*label6.fontList: TextFont
*label6.foreground: TextForeground

*label7.class: label
*label7.parent: form6
*label7.static: true
*label7.name: label7
*label7.x: 104
*label7.y: 198
*label7.width: 109
*label7.height: 25
*label7.labelString: "start-y,step-y :"
*label7.background: ApplicBackground
*label7.fontList: TextFont
*label7.foreground: TextForeground

*label1.class: label
*label1.parent: form6
*label1.static: true
*label1.name: label1
*label1.x: 0
*label1.y: 10
*label1.width: 105
*label1.height: 20
*label1.labelString: "INPUT FILES"
*label1.background: ApplicBackground
*label1.fontList: TextFont
*label1.foreground: TextForeground

*form2.class: form
*form2.parent: form6
*form2.static: true
*form2.name: form2
*form2.resizePolicy: "resize_none"
*form2.x: 0
*form2.y: 559
*form2.width: 448
*form2.height: 48
*form2.background: ButtonBackground

*pb_execute.class: pushButton
*pb_execute.parent: form2
*pb_execute.static: true
*pb_execute.name: pb_execute
*pb_execute.x: 107
*pb_execute.y: 6
*pb_execute.width: 85
*pb_execute.height: 35
*pb_execute.fontList: BoldTextFont
*pb_execute.foreground: ApplyForeground
*pb_execute.background: ButtonBackground
*pb_execute.labelString: "Apply"
*pb_execute.activateCallback: {\
char *prefix, *numbers;\
extern int ExecuteCaller;\
	   \
prefix = XmTextFieldGetString(UxGetWidget(tf_inputf));\
numbers = XmTextFieldGetString(UxGetWidget(tf_inpnumb));\
CreateInputCatalog(prefix, numbers);\
\
if ( MissingAirmassInCatalog() ) {\
    if ( FillAirmassForm(UxGetWidget(UxFindSwidget("t_airmass_name")),\
                         UxGetWidget(UxFindSwidget("t_airmass_value")))) {\
        UxPopupInterface(UxFindSwidget("AirmassShell"), exclusive_grab);\
        ExecuteCaller = TRUE;\
    }\
}\
else\
    ExecuteReduce();\
\
XtFree(prefix);\
XtFree(numbers);\
\
}
*pb_execute.highlightOnEnter: "true"
*pb_execute.highlightColor: ButtonForeground

*pb_airmass.class: pushButton
*pb_airmass.parent: form2
*pb_airmass.static: true
*pb_airmass.name: pb_airmass
*pb_airmass.x: 8
*pb_airmass.y: 6
*pb_airmass.width: 85
*pb_airmass.height: 35
*pb_airmass.fontList: BoldTextFont
*pb_airmass.foreground: ButtonForeground
*pb_airmass.background: ButtonBackground
*pb_airmass.labelString: "Airmass ..."
*pb_airmass.activateCallback: {\
char *prefix, *numbers;\
extern int ExecuteCaller;\
\
prefix = XmTextFieldGetString(UxGetWidget(tf_inputf));\
numbers = XmTextFieldGetString(UxGetWidget(tf_inpnumb));\
CreateInputCatalog(prefix, numbers);\
\
if ( FillAirmassForm(UxGetWidget(UxFindSwidget("t_airmass_name")),\
                     UxGetWidget(UxFindSwidget("t_airmass_value")))) {\
    ExecuteCaller = FALSE;\
    UxPopupInterface(UxFindSwidget("AirmassShell"), exclusive_grab);\
}\
\
XtFree(prefix);\
XtFree(numbers);\
\
}
*pb_airmass.highlightOnEnter: "true"
*pb_airmass.highlightColor: ButtonForeground

*tf_rotstep.class: textField
*tf_rotstep.parent: form6
*tf_rotstep.static: true
*tf_rotstep.name: tf_rotstep
*tf_rotstep.x: 290
*tf_rotstep.y: 190
*tf_rotstep.width: 72
*tf_rotstep.height: 34
*tf_rotstep.background: TextBackground
*tf_rotstep.fontList: TextFont
*tf_rotstep.columns: 15
*tf_rotstep.maxLength: 15
*tf_rotstep.highlightOnEnter: "true"
*tf_rotstep.foreground: TextForeground
*tf_rotstep.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
double val;\
extern double Rotstep;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%lf", &val);\
\
if ( val != Rotstep ) {\
    Rotstep = val;\
    WriteKeyword(text, K_ROTSTEP);\
}\
\
XtFree(text);\
}

