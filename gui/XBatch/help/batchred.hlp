# @(#)batchred.hlp	19.1  (ESO)  02/25/03  13:43:54
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++
#.COPYRIGHT   (C) 1993 European Southern Observatory
#.IDENT       batch.hlp
#.AUTHORS     Pascal Ballester (ESO/Garching)
#             Cristian Levin   (ESO/La Silla)
#.KEYWORDS    Spectroscopy, Long-Slit
#.PURPOSE
#             This is the help file for the extended help
#             feature of XBatch.
#
#.VERSION     1.0  Package Creation  17-MAR-1993
#-------------------------------------------------------
#

~HELP_WINDOW
This interface allows to do batch reductions. You have to fill the form
with the necessary parameters. These parameters can be saved in a 
parameters file in order to be restored later; for that, you have to use
the "File" pulldown menu.

The fields associated to images, tables or catalogs allow you to access a
name list using the right mouse button. After clicking over the desired name,
it is displayed in the field.

The input files can be given in two ways:
 - catalog name : you have to fill the field "prefix/catalog" with the catalog
                  name.
 - image numbers: you have to put the prefix of the images in the field
                  "prefix/catalog" and then put the numbers of the images
                  in the field "Numbers", using dashes and/or commas. e.g.:
                  Prefix  : red   
                  Numbers : 3-5,8
		  The image names taken are: red0003, red0004, red0005, red0008

The dialog form accessed by pressing "Airmass ..." gives you the possibility
of modification of the airmass values of the input images.

If any of the input images don't have the corresponding airmass descriptor,
the airmass dialog form is displayed automatically.

~AIRMASS
It gives you the possibility of modification of the airmass values of the 
input images.


~EXECUTE
Execute the batch reduction.
