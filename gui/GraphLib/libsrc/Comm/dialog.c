/*===========================================================================
  Copyright (C) 1991-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        dialog.c
.MODULE       subroutines -- graphical user interface
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      The function of this module perform the communication
              between an external process and Midas.
.KEYWORDS     signals, communication.
.COMMENTS     - Communication through a file in MID_WORK, using
                SIGUSR1 for interrupting Midas and waiting for
                SIGUSR2 from Midas.
.ENVIRONMENT  UNIX
                
.VERSION 1.0   1-Mar-1991   Implementation
.VERSION 2.0  20-May-1992   Addition of SIGUSR2 stuff

 090812		last modif
------------------------------------------------------------*/

#include <midas_def.h>
#include <osfile.h>
#include <ok.h>

#include <signal.h>

#include <stdio.h>



#define MYSESSION	"QQ"
#define FOREVER		0
#define MAXLINE         160
 
#define OKSTAT           0
#define NOEXSTAT        -999
#define SENDSTAT        -1000
#define BUSYSTAT        -1001
#define RETSTAT         -1002

#define UNDEFINED       -1009


static int CommStat, MidasStat, MidasStata, keylength;
static int startup = -1;

static char keyfile[1000][MAXLINE];

extern int strred(), strindex();



/*

*/

int AppendDialogText(s)
char   *s;

{

int  stat, secs, retstat[2];

static int  mid;

char  unit[4];



MidasStat = UNDEFINED;

if (startup == -1)
   {
   startup = 0;
   osfphname("DAZUNIT", unit);
   unit[3] = '\0';
   stat = XCZOPN(unit,MYSESSION,"",&mid);
   if (stat != 0)
      {
      CommStat = NOEXSTAT;
      return (CommStat);
      }
   }

secs = -1;			/* wait forever */
stat = XCCSND(mid,s,secs,retstat);
if (stat == 1)
   CommStat = BUSYSTAT;
else if (stat == 4)
   CommStat = SENDSTAT;
else
   {
   CommStat = OKSTAT;
   MidasStat = retstat[0];
   MidasStata = retstat[1];
   }


return (CommStat);
}

/*

*/

int AppendDialogTextNoWait(s)
char   *s;

{

int  stat, secs, retstat[2];

static int  mid;

char  unit[4];



MidasStat = UNDEFINED;

if (startup == -1)
   {
   startup = 0;
   osfphname("DAZUNIT", unit);
   unit[3] = '\0';
   stat = XCZOPN(unit,MYSESSION,"",&mid);
   if (stat != 0)
      {
      CommStat = NOEXSTAT;
      return (CommStat);
      }
   }

secs = 0;                      /* don't wait */
stat = XCCSND(mid,s,secs,retstat);
if (stat == 1)
   CommStat = BUSYSTAT;
else if (stat == 4)
   CommStat = SENDSTAT;
else
   {
   CommStat = OKSTAT;
   MidasStat = retstat[0];
   MidasStata = retstat[1];
   }


return (CommStat);
}

/*

*/

void StatMidas(actio,mstat,mstring)
char  *actio, *mstring;
int   *mstat;

{

/*  currently, MidasStata is not used */


if ((*actio == 'g') || (*actio == 'G'))
   {						/* get errors out */
   mstat[0] = MidasStat;
   if (MidasStat != UNDEFINED)
      sprintf(mstring,"Midas Error: %d",MidasStat);
   else
      mstring[0] = '\0';
   }
else
   {
   MidasStat = mstat[0];
   }

}

/*

*/

int KeyValue(key,value)

char *key, *value;

{

    int n, i, found;
    char msg[MAXLINE], line[MAXLINE];

    strcpy ( line,  "keyword: ");
    strcat ( line,  key);
    strcat ( line,  " ");

    found = 0;
    for (n=0; n<keylength; n++) {

          strcpy(msg, keyfile[n]);

          if (strindex(msg, line) == 0) {
           found = 1;
           strcpy(msg, keyfile[++n]);
           for (i=0; i<strred(msg); i++)     /* Removes redundant spaces */
               if (msg[i] == ' ')  msg[i] = ','; /* Replace by comas */
           strcpy(value, msg);
           break;
	   }        /* Matches if (strindex .. */
    }  /* Matches for n=0... */

    return(found);

}
/*

*/

int LoadFile(fsend)

char *fsend;

{

    int  fn, len=MAXLINE, n=0;
    char msg[MAXLINE];

    if ( (fn = osaopen(fsend, READ)) != -1 ) {
           while (osaread(fn,msg,len) != -1)
                    strcpy(keyfile[n++], msg);
           keylength = n;
           osaclose(fn);
	 }
    else
       printf("Error while opening %s\n",fsend);

    if (n == 0) return(0); /* File empty */
    else        return(1);
  }


