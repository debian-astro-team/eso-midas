/*===========================================================================
  Copyright (C) 1995-2005 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/* Attributes groups of colors */
/*
    Group 0 : Background       (Wheat)       -bkg
    Group 1 : Input Fields     (PapayaWhip)  -inpf
    Group 2 : Action Buttons   (PapayaWhip)  -act
    Group 3 : Short Help       (PaleGreen2)  -sh
    Group 4 : Quit Buttons     (SeaGreen3)   -quit
    Group 5 : Low level steps  (Yellow)      -lstp
    Group 6 : High level steps (SeaGreen3)   -hstp      
*/


/*

.VERSION
051014		last modif

*/

#include <ExternResources.h>
#include <string.h>
#include <stdio.h>



#define  NOPT   20

void SetResources(count, argpntrs)

int     count;
char    *argpntrs[];

{

int  opt,narg,vbmode;
char Option[NOPT][ShortLen], Resource[NOPT][LongLen];

strcpy(Option[0],"-tf");    /* Text Font */
strcpy(Option[1],"-btf");   /* Bold Text Font */
strcpy(Option[2],"-bf");    /* Big Font */
strcpy(Option[3],"-bbf");   /* Bold Big Font */
strcpy(Option[4],"-tfg");   /* Text Foreground */
strcpy(Option[5],"-tbg");   /* Text Background */
strcpy(Option[6],"-wbg");   /* Window Background */
strcpy(Option[7],"-shbg");  /* Short Help Background */
strcpy(Option[8],"-bbg");   /* Button Background */
strcpy(Option[9],"-bfg");   /* Button Foreground */
strcpy(Option[10],"-ag");   /* Window Geometry */
strcpy(Option[11],"-hg");   /* Extended Help Geometry */
strcpy(Option[12],"-sbg");  /* Selection Box Geometry */
strcpy(Option[13],"NULL");   /* Must terminate the list of options */

strcpy(Resource[0],  TextFont);         
strcpy(Resource[1],  BoldTextFont);     
strcpy(Resource[2],  BigFont);          
strcpy(Resource[3],  BoldBigFont);      
strcpy(Resource[4],  TextForeground);   
strcpy(Resource[5],  TextBackground);   
strcpy(Resource[6],  WindowBackground); 
strcpy(Resource[7],  SHelpBackground);  
strcpy(Resource[8],  ButtonBackground); 
strcpy(Resource[9],  ButtonForeground); 
strcpy(Resource[10], ApplicGeometry);   
strcpy(Resource[11], ExtHelpGeometry);  
strcpy(Resource[12], SelBoxGeometry);   

vbmode = 0;

if ( count > 1 )
   {

/* Examine once all arguments to detect the verbose mode */

   for (narg = 1; narg < count; narg++)
      {
      if (strcmp(argpntrs[narg],"-v") == 0)   vbmode = 1;
      }

/* Examine all arguments to detect the FileSelection mode
   (option -fs) or modify the default of parametrable
   resources 9list provided by Option[].
   The value of the resource is assumed to be provided as the
   next argument (e.g. -bkg Blue, and not -bkgBlue )               */


   for (narg = 1; narg < count; narg++)
      {
      if (argpntrs[narg][0] == '-')
         {
         opt = -1;			/* Initialization */
         while (strcmp(Option[++opt],"NULL") != 0)
            {
            if (strcmp(Option[opt],argpntrs[narg]) == 0)
               {
               if (vbmode == 1)
                  printf("\n Option %s : \n  Default = %s \n  New value = %s\n",
                         Option[opt], Resource[opt], argpntrs[narg+1]);
               strcpy(Resource[opt],argpntrs[narg+1]);
               }
            }
         }
      }
   }

strcpy(TextFont,         Resource[0]);
strcpy(BoldTextFont,     Resource[1]);
strcpy(BigFont,          Resource[2]);
strcpy(BoldBigFont,      Resource[3]);
strcpy(TextForeground,   Resource[4]);
strcpy(TextBackground,   Resource[5]);
strcpy(WindowBackground, Resource[6]);
strcpy(SHelpBackground,  Resource[7]);
strcpy(ButtonBackground, Resource[8]);
strcpy(ButtonForeground, Resource[9]);
strcpy(ApplicGeometry,   Resource[10]);
strcpy(ExtHelpGeometry,  Resource[11]);
strcpy(SelBoxGeometry,   Resource[12]);

}






