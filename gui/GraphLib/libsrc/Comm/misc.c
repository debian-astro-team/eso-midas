/* @(#)misc.c	19.1 (ES0-DMD) 02/25/03 13:39:46 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)misc.c    1.0.0.0 (ESO-La Silla) 10/08/91 12:00:00 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        misc.c
.MODULE       subroutines
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Miscelaneous functions:
		- Checking of the existence of a Unix file.
		- Checking of the Midas graphics window.
.KEYWORDS     file checking, graphics window
.VERSION 1.0  1-Apr-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <gl_defs.h>
#include <proto_os.h>

/* check for the existence of 'file' with the extension 'ext' (incl. dot) */
int file_exists( file, ext )
char *file, *ext;
{
    char fileext[MAXLINE];
    int i;
    struct stat statbuf;

    if ( file[0] == '\0' )
	return(FALSE);

    for ( i = 0; file[i] != '\0'; i++ )
	if ( file[i] == ' ' ) {
	    file[i] = '\0';
	    break;
	}

    if ( strstrs(file, ext) == (char *)NULL )
    	sprintf( fileext, "%s%s", file, ext );
    else
	strcpy( fileext, file );

    if ( stat(fileext, &statbuf) == -1 )
	return(FALSE);

    return(TRUE);
}

int graphwin_exists()
{
    char unit[10];
    char file_old[MAXLINE], file[MAXLINE];
    char midwork[MAXLINE];

    osfphname("MID_WORK", midwork);
    osfphname("DAZUNIT", unit);
    unit[3] = '\0';

    sprintf(file_old, "%s%sXW", midwork, unit);
    sprintf(file, "%smidas_xw%s", midwork, unit);
    return(file_exists(file_old, ".soc") || file_exists(file_old, ".soc=") ||
           file_exists(file, "") || file_exists(file, "=") );
}

void DropTrailingBlanks(str)
char *str;
{
    int i;

    for ( i = 0; str[i] != '\0'; i++ )
        if ( str[i] == ' ' ) {
            str[i] = '\0';
            break;
        }
}
