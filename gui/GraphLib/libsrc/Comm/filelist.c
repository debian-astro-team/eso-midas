/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


#include <gl_defs.h>
#include <xm_defs.h>
#include <ctype.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <osparms.h>

#define MAXLEN_FILENAME	32		/* max lenght of a filename */

static char	Suffix[MAXLINE];	/* suffix of files to select */
static String  	filelist; 		/* list of file names */
static int	nfiles = 0;		/* number of files in filelist */

static void ScanDir();

extern int oshcmd();



void SetFileList( wlist,strip,dirspecs )
Widget wlist;
char *dirspecs;
int  strip;

{
    int i,j,k;
    char    liste[500];
    XmStringTable str_list;

    strcpy(Suffix,dirspecs);

    ScanDir(strip,dirspecs);

    str_list = (XmStringTable)XtMalloc(nfiles * sizeof(XmString *));

    k = 0 ;
    for ( i = 0; i < nfiles; i++ ) {
        j = 0;
        while (filelist[k] != '\n')  { liste[j++] = filelist[k++]; }
        liste[j] = '\0';
        k++;
	str_list[i] = XmStringCreateSimple(liste);
      }

    XmListSetPos(wlist, 1);
    XmListDeleteAllItems(wlist);
    XmListAddItems(wlist, str_list, nfiles, 1);

    for ( i = 0; i < nfiles; i++ )
	XmStringFree(str_list[i]);
    XtFree((char *)str_list);
}

/*  Scans the working directory for files selected by InList()???, sorted
 *  alphabetically, and stored in an array of pointers to directory
 *  entries called namelist.
 *  The names of the selected files are stored in filelist.
 */

static void ScanDir(strip,dir)

int  strip;
char *dir;

{
char    Command[256];
char    filemark;

FILE    *list, *number;

int     jj, nword, nchar, row, k=0, k0=0;
register int  nr;

#if vms
filemark = ']';

#else
filemark = '/';
strcpy (Command, "ls -aF ");
strcat (Command, dir);
strcat (Command, " > tmpscan.dir; cat tmpscan.dir | wc > tmpscan.nb");
#endif


oshcmd(Command,NULL,NULL,NULL);

number = fopen("tmpscan.nb", "r");
list   = fopen("tmpscan.dir","r");

fscanf(number, "%d %d %d",&nfiles, &nword, &nchar);
filelist = (char *) malloc((size_t)((nchar+1)*sizeof(char)));

if (strip == 1) 			/* Yes. Strip off directory specs */
   {
   for (row = 0; row < nfiles; row++)
      {
      fscanf (list,"%s",&filelist[k]);
      while (filelist[k] != '\0')
         {
         if (filelist[k] == '*') filelist[k] = ' ';
         k++;
         }

      for (nr=k-1; nr>k0; nr--)		/* check for directory specs */
         {
         if (filelist[nr] == filemark)
            {
            jj = nr + 1;
            strcpy(&filelist[k0],&filelist[jj]);
            k -= (jj-k0);
            break;
            }
         }
      filelist[k++] = '\n';
      k0 = k;			/* follow with start index */
      }
   }

else					/* leave as it is */
   {
   for (row = 0; row < nfiles; row++)
      {
      fscanf (list,"%s",&filelist[k]);
      while (filelist[k] != '\0')
         {
         if (filelist[k] == '*') filelist[k] = ' ';
         k++;
         }
      filelist[k++] = '\n';
      }
   }
filelist[k] = '\0';
     
fclose(number);
fclose(list);     
}










