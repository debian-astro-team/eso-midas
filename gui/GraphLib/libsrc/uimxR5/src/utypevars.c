/* @(#)utypevars.c	17.1 (ESO-IPG) 01/25/02 17:26:36 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:19 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1991, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * UIMX type indices
 *------------------------------------------------------------------------*/

int 	UxUT_float			= -1;
int 	UxUT_int			= -1;
int 	UxUT_short			= -1;
int 	UxUT_string			= -1;
int 	UxUT_vhandle			= -1;
int 	UxUT_char			= -1;
int 	UxUT_long			= -1;
int 	UxUT_stringTable		= -1;
int 	UxUT_cardFunction		= -1;
int 	UxUT_voidFunction		= -1;
int 	UxUT_visualPointer		= -1;
int 	UxUT_XmTextSource		= -1;


/*----------------------------------------------------------------------------
 * Non-enumerated X types (in alphabetic order, ignoring case). 
 *---------------------------------------------------------------------------*/

int 	UxXT_Accelerators		= -1;
int	UxXT_Atom			= -1;
int 	UxXT_bitmap			= -1;
int 	UxXT_BorderPixmap		= -1;
int 	UxXT_BottomShadowPixmap		= -1;
int	UxXT_char			= -1;
int	UxXT_Colormap			= -1;
int 	UxXT_CreatePopupChildProc	= -1;
int	UxXT_Dimension			= -1;
int	UxXT_DirListItems		= -1;
int 	UxXT_DirSearchProc		= -1;
int	UxXT_FileListItems		= -1;
int 	UxXT_FileSearchProc		= -1;
int 	UxXT_FontStruct			= -1;
int 	UxXT_HighlightPixmap		= -1;
int	UxXT_HistoryItems		= -1;
int 	UxXT_InsertPosition		= -1;
int 	UxXT_int			= -1;
int	UxXT_Items			= -1;
int	UxXT_KeySym			= -1;
int	UxXT_ListItems			= -1;
int	UxXT_StringOrNull		= -1;
int 	UxXT_Pixel			= -1;
int 	UxXT_Pixmap			= -1;
int 	UxXT_Pointer			= -1;
int	UxXT_Position			= -1;
int 	UxXT_QualifySearchDataProc	= -1;
int	UxXT_SelectedItems		= -1;
int 	UxXT_short			= -1;
int 	UxXT_String			= -1;
int	UxXT_StringTable		= -1;
int 	UxXT_TopShadowPixmap		= -1;
int 	UxXT_Translations		= -1;
int	UxXT_ValueWcs			= -1;
int	UxXT_VisualPointer		= -1;
int 	UxXT_Widget			= -1;
int	UxXT_WidgetClass		= -1;
int 	UxXT_WidgetList			= -1;
int 	UxXT_Window			= -1;
int 	UxXT_XID			= -1;
int	UxXT_XmFontList			= -1;
int	UxXT_XmString			= -1;
int	UxXT_XmTextSource		= -1;


/*----------------------------------------------------------------------------
 * Enumerated X types (in alphabetic order) 
 *---------------------------------------------------------------------------*/

int	UxXT_Alignment			= -1;
int	UxXT_ArrowDirection		= -1;
int	UxXT_AttachmentType		= -1;
int	UxXT_AudibleWarning		= -1;
int 	UxXT_Bool			= -1;
int 	UxXT_Boolean			= -1;
int	UxXT_ChildPlacement		= -1;
int	UxXT_ChildType			= -1;
int	UxXT_ChildVerticalAlignment	= -1;
int     UxXT_CommandWindowLocation	= -1;
int	UxXT_DefaultButtonType		= -1;
int	UxXT_DeleteResponse		= -1;
int	UxXT_DialogStyle		= -1;
int	UxXT_DialogType			= -1;
int	UxXT_EditMode			= -1;
int	UxXT_EntryVerticalAlignment	= -1;
int	UxXT_FileTypeMask		= -1;
int	UxXT_IndicatorType		= -1;
int 	UxXT_InitialWindowState		= -1;
int	UxXT_KeyboardFocusPolicy	= -1;
int	UxXT_LabelType			= -1;
int	UxXT_ListSizePolicy		= -1;
int	UxXT_MsgDialogType		= -1;
int	UxXT_MultiClick			= -1;
int	UxXT_MwmInputMode		= -1;
int	UxXT_NavigationType		= -1;
int	UxXT_Orientation		= -1;
int	UxXT_Packing			= -1;
int	UxXT_ProcessingDirection	= -1;
int	UxXT_ResizePolicy		= -1;
int	UxXT_RowColumnType		= -1;
int	UxXT_ScrollBarDisplayPolicy	= -1;
int	UxXT_ScrollBarPlacement		= -1;
int	UxXT_ScrollingPolicy		= -1;
int	UxXT_SelectionPolicy		= -1;
int	UxXT_SelectionArray		= -1;
int	UxXT_SeparatorType		= -1;
int	UxXT_ShadowType			= -1;
int	UxXT_StringDirection		= -1;
int	UxXT_TearOffModel		= -1;
int	UxXT_UnitType			= -1;
int	UxXT_UnpostBehavior		= -1;
int	UxXT_VisualPolicy		= -1;
int	UxXT_WinGravity			= -1;

/*-----------------------------------------------------
 * End of file (there are no procedures)
 *-----------------------------------------------------*/

