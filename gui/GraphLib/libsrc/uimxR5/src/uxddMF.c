/*
.VERSION
 090818		last modif
*/

#include "UxLib.h"
#include "utype.h"

extern void UxDDInstall();


extern WidgetClass xmToggleButtonGadgetClass;
extern WidgetClass xmTextFieldWidgetClass;
extern WidgetClass xmSeparatorGadgetClass;
extern WidgetClass xmScrollBarWidgetClass;
extern WidgetClass xmScrolledWindowWidgetClass;
extern WidgetClass xmTextWidgetClass;
extern WidgetClass xmSeparatorWidgetClass;
extern WidgetClass xmScaleWidgetClass;
extern WidgetClass xmPushButtonGadgetClass;
extern WidgetClass xmRowColumnWidgetClass;
extern WidgetClass xmToggleButtonWidgetClass;
extern WidgetClass xmSelectionBoxWidgetClass;
extern WidgetClass xmPrimitiveWidgetClass;
extern WidgetClass shellWidgetClass;
extern WidgetClass vendorShellWidgetClass;
extern WidgetClass xmManagerWidgetClass;
extern WidgetClass xmListWidgetClass;
extern WidgetClass xmLabelGadgetClass;
extern WidgetClass xmCascadeButtonGadgetClass;
extern WidgetClass xmFrameWidgetClass;
extern WidgetClass xmDrawnButtonWidgetClass;
extern WidgetClass xmDrawingAreaWidgetClass;
extern WidgetClass xmPanedWindowWidgetClass;
extern WidgetClass xmLabelWidgetClass;
extern WidgetClass xmMenuShellWidgetClass;
extern WidgetClass xmPushButtonWidgetClass;
extern WidgetClass xmArrowButtonGadgetClass;

void UxDDInit()
{
  UxDDInstall (XmNwinGravity, UxUT_string, UxXT_WinGravity);
  UxDDInstall (XmNwindowGroup, UxUT_string, UxXT_XID);
  UxDDInstall (XmNwaitForWm, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNtransient, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNtitleEncoding, UxUT_string, UxXT_Atom);
  UxDDInstall (XmNinput, UxUT_string, UxXT_Bool);
  UxDDInstall (XmNinitialState, UxUT_string, UxXT_InitialWindowState);
  UxDDInstall (XmNiconWindow, UxUT_string, UxXT_Window);
  UxDDInstall (XmNiconPixmap, UxUT_string, UxXT_bitmap);
  UxDDInstall (XmNiconMask, UxUT_string, UxXT_bitmap);
  UxDDInstall (XmNuseAsyncGeometry, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNshellUnitType, UxUT_string, UxXT_UnitType);
  UxDDInstall (XmNmwmInputMode, UxUT_string, UxXT_MwmInputMode);
  UxDDInstall (XmNkeyboardFocusPolicy, UxUT_string, UxXT_KeyboardFocusPolicy);
  UxDDInstall (XmNdeleteResponse, UxUT_string, UxXT_DeleteResponse);
  UxDDInstall (XmNaudibleWarning, UxUT_string, UxXT_AudibleWarning);
  UxDDInstall (XmNtransientFor, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNiconNameEncoding, UxUT_string, UxXT_Atom);
  UxDDInstall (XmNiconic, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNvisibleWhenOff, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNset, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNselectPixmap, UxUT_string, UxXT_Pixmap);
  UxDDInstall (XmNselectInsensitivePixmap, UxUT_string, UxXT_Pixmap);
  UxDDInstall (XmNselectColor, UxUT_string, UxXT_Pixel);
  UxDDInstall (XmNindicatorType, UxUT_string, UxXT_IndicatorType);
  UxDDInstall (XmNindicatorSize, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNindicatorOn, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNfillOnSelect, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNwordWrap, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNcursorPositionVisible, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNselectionArray, UxUT_string, UxXT_SelectionArray);
  UxDDInstall (XmNpendingDelete, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNverifyBell, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNvalueWcs, UxUT_string, UxXT_ValueWcs);
  UxDDInstall (XmNeditMode, UxUT_string, UxXT_EditMode);
  UxDDInstall (XmNeditable, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNautoShowCursorPosition, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNsaveUnder, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNoverrideRedirect, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNgeometry, UxUT_string, UxXT_StringOrNull);
  UxDDInstall (XmNseparatorType, UxUT_string, UxXT_SeparatorType);
  UxDDInstall (XmNmargin, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNtextString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNtextAccelerators, UxUT_string, UxXT_Accelerators);
  UxDDInstall (XmNselectionLabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNmustMatch, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNlistLabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNlistItems, UxUT_string, UxXT_ListItems);
  UxDDInstall (XmNdialogType, UxUT_string, UxXT_DialogType);
  UxDDInstall (XmNchildPlacement, UxUT_string, UxXT_ChildPlacement);
  UxDDInstall (XmNapplyLabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNworkWindow, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNvisualPolicy, UxUT_string, UxXT_VisualPolicy);
  UxDDInstall (XmNscrollingPolicy, UxUT_string, UxXT_ScrollingPolicy);
  UxDDInstall (XmNscrolledWindowMarginWidth, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNscrolledWindowMarginHeight, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNscrollBarPlacement, UxUT_string, UxXT_ScrollBarPlacement);
  UxDDInstall (XmNclipWindow, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNtroughColor, UxUT_string, UxXT_Pixel);
  UxDDInstall (XmNshowArrows, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNtitleString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNshowValue, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNscaleWidth, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNscaleHeight, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNprocessingDirection, UxUT_string, UxXT_ProcessingDirection);
  UxDDInstall (XmNscrollVertical, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNscrollTopSide, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNscrollLeftSide, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNscrollHorizontal, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNverticalScrollBar, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNhorizontalScrollBar, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNtearOffModel, UxUT_string, UxXT_TearOffModel);
  UxDDInstall (XmNrowColumnType, UxUT_string, UxXT_RowColumnType);
  UxDDInstall (XmNresizeWidth, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNresizeHeight, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNradioBehavior, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNradioAlwaysOne, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNpopupEnabled, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNpacking, UxUT_string, UxXT_Packing);
  UxDDInstall (XmNorientation, UxUT_string, UxXT_Orientation);
  UxDDInstall (XmNmenuPost, UxUT_string, UxXT_StringOrNull);
  UxDDInstall (XmNmenuHistory, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNmenuHelpWidget, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNisHomogeneous, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNisAligned, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNentryVerticalAlignment, UxUT_string, UxXT_EntryVerticalAlignment);
  UxDDInstall (XmNentryClass, UxUT_string, UxXT_WidgetClass);
  UxDDInstall (XmNentryBorder, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNentryAlignment, UxUT_string, UxXT_Alignment);
  UxDDInstall (XmNadjustMargin, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNadjustLast, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNshowAsDefault, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNfillOnArm, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNdefaultButtonShadowThickness, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNarmPixmap, UxUT_string, UxXT_Pixmap);
  UxDDInstall (XmNarmColor, UxUT_string, UxXT_Pixel);
  UxDDInstall (XmNskipAdjust, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNpaneMinimum, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNpaneMaximum, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNallowResize, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNspacing, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNseparatorOn, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNsashWidth, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNsashShadowThickness, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNsashIndent, UxUT_int, UxXT_Position);
  UxDDInstall (XmNsashHeight, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNrefigureMode, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNsymbolPixmap, UxUT_string, UxXT_Pixmap);
  UxDDInstall (XmNokLabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNminimizeButtons, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNmessageString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNmessageAlignment, UxUT_string, UxXT_Alignment);
  UxDDInstall (XmNhelpLabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNdefaultButtonType, UxUT_string, UxXT_DefaultButtonType);
  UxDDInstall (XmNcancelLabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNtopShadowPixmap, UxUT_string, UxXT_TopShadowPixmap);
  UxDDInstall (XmNtopShadowColor, UxUT_string, UxXT_Pixel);
  UxDDInstall (XmNinitialFocus, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNhighlightPixmap, UxUT_string, UxXT_HighlightPixmap);
  UxDDInstall (XmNhighlightColor, UxUT_string, UxXT_Pixel);
  UxDDInstall (XmNforeground, UxUT_string, UxXT_Pixel);
  UxDDInstall (XmNbottomShadowPixmap, UxUT_string, UxXT_BottomShadowPixmap);
  UxDDInstall (XmNbottomShadowColor, UxUT_string, UxXT_Pixel);
  UxDDInstall (XmNallowShellResize, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNdefaultFontList, UxUT_string, UxXT_XmFontList);
  UxDDInstall (XmNshowSeparator, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNmessageWindow, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNmenuBar, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNmainWindowMarginWidth, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNmainWindowMarginHeight, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNcommandWindowLocation, UxUT_string, UxXT_CommandWindowLocation);
  UxDDInstall (XmNcommandWindow, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNselectionPolicy, UxUT_string, UxXT_SelectionPolicy);
  UxDDInstall (XmNselectedItems, UxUT_string, UxXT_SelectedItems);
  UxDDInstall (XmNscrollBarDisplayPolicy, UxUT_string, UxXT_ScrollBarDisplayPolicy);
  UxDDInstall (XmNlistSpacing, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNlistSizePolicy, UxUT_string, UxXT_ListSizePolicy);
  UxDDInstall (XmNlistMarginWidth, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNlistMarginHeight, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNitems, UxUT_string, UxXT_Items);
  UxDDInstall (XmNautomaticSelection, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNstringDirection, UxUT_string, UxXT_StringDirection);
  UxDDInstall (XmNrecomputeSize, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNmnemonic, UxUT_string, UxXT_KeySym);
  UxDDInstall (XmNmarginTop, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNmarginRight, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNmarginLeft, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNmarginBottom, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNlabelType, UxUT_string, UxXT_LabelType);
  UxDDInstall (XmNlabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNlabelPixmap, UxUT_string, UxXT_Pixmap);
  UxDDInstall (XmNlabelInsensitivePixmap, UxUT_string, UxXT_Pixmap);
  UxDDInstall (XmNfontList, UxUT_string, UxXT_XmFontList);
  UxDDInstall (XmNalignment, UxUT_string, UxXT_Alignment);
  UxDDInstall (XmNacceleratorText, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNuserData, UxUT_string, UxXT_Pointer);
  UxDDInstall (XmNunitType, UxUT_string, UxXT_UnitType);
  UxDDInstall (XmNtraversalOn, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNshadowThickness, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNnavigationType, UxUT_string, UxXT_NavigationType);
  UxDDInstall (XmNhighlightThickness, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNhighlightOnEnter, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNpattern, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNnoMatchString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNlistUpdated, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNfilterLabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNfileTypeMask, UxUT_string, UxXT_FileTypeMask);
  UxDDInstall (XmNfileListLabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNfileListItems, UxUT_string, UxXT_FileListItems);
  UxDDInstall (XmNdirSpec, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNdirMask, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNdirListLabelString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNdirListItems, UxUT_string, UxXT_DirListItems);
  UxDDInstall (XmNdirectoryValid, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNdirectory, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNchildVerticalAlignment, UxUT_string, UxXT_ChildVerticalAlignment);
  UxDDInstall (XmNchildHorizontalSpacing, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNchildHorizontalAlignment, UxUT_string, UxXT_Alignment);
  UxDDInstall (XmNchildType, UxUT_string, UxXT_ChildType);
  UxDDInstall (XmNtopWidget, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNtopAttachment, UxUT_string, UxXT_AttachmentType);
  UxDDInstall (XmNrightWidget, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNrightAttachment, UxUT_string, UxXT_AttachmentType);
  UxDDInstall (XmNresizable, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNleftWidget, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNleftAttachment, UxUT_string, UxXT_AttachmentType);
  UxDDInstall (XmNbottomWidget, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNbottomAttachment, UxUT_string, UxXT_AttachmentType);
  UxDDInstall (XmNverticalSpacing, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNrubberPositioning, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNhorizontalSpacing, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNpushButtonEnabled, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNchildren, UxUT_stringTable, UxXT_WidgetList);
  UxDDInstall (XmNpromptString, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNhistoryItems, UxUT_string, UxXT_HistoryItems);
  UxDDInstall (XmNcommand, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNsubMenuId, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNcascadePixmap, UxUT_string, UxXT_Pixmap);
  UxDDInstall (XmNtextTranslations, UxUT_string, UxXT_Translations);
  UxDDInstall (XmNtextFontList, UxUT_string, UxXT_XmFontList);
  UxDDInstall (XmNshadowType, UxUT_string, UxXT_ShadowType);
  UxDDInstall (XmNresizePolicy, UxUT_string, UxXT_ResizePolicy);
  UxDDInstall (XmNnoResize, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNmarginWidth, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNmarginHeight, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNlabelFontList, UxUT_string, UxXT_XmFontList);
  UxDDInstall (XmNdialogTitle, UxUT_string, UxXT_XmString);
  UxDDInstall (XmNdialogStyle, UxUT_string, UxXT_DialogStyle);
  UxDDInstall (XmNdefaultPosition, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNdefaultButton, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNcancelButton, UxUT_string, UxXT_Widget);
  UxDDInstall (XmNbuttonFontList, UxUT_string, UxXT_XmFontList);
  UxDDInstall (XmNautoUnmanage, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNallowOverlap, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNmultiClick, UxUT_string, UxXT_MultiClick);
  UxDDInstall (XmNarrowDirection, UxUT_string, UxXT_ArrowDirection);
  UxDDInstall (XmNy, UxUT_int, UxXT_Position);
  UxDDInstall (XmNx, UxUT_int, UxXT_Position);
  UxDDInstall (XmNwidth, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNsensitive, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNheight, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNborderWidth, UxUT_int, UxXT_Dimension);
  UxDDInstall (XmNancestorSensitive, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNmappedWhenManaged, UxUT_string, UxXT_Boolean);
  UxDDInstall (XmNcolormap, UxUT_long, UxXT_Colormap);
  UxDDInstall (XmNborderPixmap, UxUT_string, UxXT_BorderPixmap);
  UxDDInstall (XmNborderColor, UxUT_string, UxXT_Pixel);
  UxDDInstall (XmNbackgroundPixmap, UxUT_string, UxXT_Pixmap);
  UxDDInstall (XmNbackground, UxUT_string, UxXT_Pixel);
}
