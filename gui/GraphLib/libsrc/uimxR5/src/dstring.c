/* @(#)dstring.c	17.1 (ESO-IPG) 01/25/02 17:26:33 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1990, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 * Dynamic strings.
 *------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "valloc.h"
#include "dstring.h"

#define BLOCKSIZE	64
#define round(len, sz) ( ((len)/(sz)+1)*(sz) )

dstring UxDefaultDstring = {NULL, 0, 0};


/*****************************************************************************
NAME:		void		UxFree_dstring(ds)

INPUT:		dstring		*ds		- dstring to free

DESCRIPTION:	free the contents of the dstring
CREATION:	Jan 14/1989
REVISIONS:	--
-----------------------------------------------------------------------------*/

void UxFree_dstring(ds)
	dstring		*ds;
{
	UxFree(ds->str);

	ds->str = NULL;
	ds->size = 0;
	ds->len = 0;
}

/*------------------------------------------------------------------------
 * NAME:	DstringAppend
 * INPUT:	dstring *s;	-- target dstring to be appended to
 *		char	*dat;	-- non-NULL string to append
 *		int	datlen	-- strlen of dat.
 * DESCRIPTION:
 *	Appends datlen characters of 'dat' to the dstring 's',
 *	resizing the storage of 's' as needed.
 *
 * LAST REV:	May 92	fix3545		extracted from UxAppend_dstring.
 *------------------------------------------------------------------------*/


static void DstringAppend(s, dat, datlen)
	dstring *s;
	char	*dat;
	int	datlen;
{
	if (s->str == NULL) {

		s->size = round(datlen + 1, BLOCKSIZE);
		s->str = UxRealloc(s->str, s->size);
		memcpy(s->str, dat, datlen);
		s->len = datlen;

	} else {

		if (s->len + datlen + 1 > s->size)
		{
			s->size = round(s->len + datlen + 1, BLOCKSIZE);
			s->str = UxRealloc(s->str, s->size);
		}

		memcpy(&(s->str[s->len]), dat, datlen);  
		s->len += datlen;
	}
	s->str[s->len] = 0;
}


/*****************************************************************************
NAME:		void		UxAppend_to_dstring(s, dat)

INPUT:		dstring		*s		- string to append to
				*dat		- string to append

DESCRIPTION:	
	Append 'dat' to 's' resizing it if needed.

CREATION:	Dec 16, 1988
REVISIONS:	May 92	fix3545		memcpy rather than strcat.
-----------------------------------------------------------------------------*/

void UxAppend_to_dstring(s, dat)
	dstring		*s;
	char		*dat;
{
	if (dat) {
		DstringAppend(s, dat, strlen(dat));
	}
}

/*****************************************************************************
NAME:		void		UxAppend_dstring(s, dat)

INPUT:		dstring		*s		- string to append to
				*dat		- string to append

DESCRIPTION:	
	Append 'dat' to 's' resizing it if needed

CREATION:	Dec 16, 1988
REVISIONS:	May 92	fix3545		memcpy rather than strcat.
-----------------------------------------------------------------------------*/

void UxAppend_dstring(s, dat)
	dstring		*s, dat;
{
	if (dat.str) {
		DstringAppend(s, dat.str, dat.len);
	}
}


/*****************************************************************************
NAME:		dstring		UxDcreate(str)

INPUT:		char		*str		- initial string

RETURN:		created dstring

DESCRIPTION:	create a dstring given the initial string
CREATION:	Jan 14/1989
REVISIONS:	May 92	fix3545		DstringAppend added.
-----------------------------------------------------------------------------*/

dstring UxDcreate(str)
	char	*str;
{
	dstring	ds;

	ds.str = NULL;
	ds.size = 0;
	ds.len = 0;

	if (str) {
		DstringAppend(&ds, str, strlen(str));
	}

	return(ds);
}
