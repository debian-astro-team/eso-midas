/*---------------------------------------------------------------------
 * $Date: 2009-08-18 15:23:20 $             $Revision: 1.2 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1988, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

/*
.VERSION
 090818		last modif
*/

/* ---------------------------------------------------------------------------
  DESCRIPTION:
	Routines to handle user messages from the message catalog

----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#include "uimx_cat.h"

extern	char	*UxCopyString();
extern void UxInitErrorHandler();


#ifdef	XOPEN_CATALOG
#if defined(sun4) || defined(SOLARIS)
char	*catgets();

#endif
#include <nl_types.h>
static nl_catd	catd = -1;


/* ---------------------------------------------------------------------------
NAME:           int UxCatOpen
INPUT:		None
RETURNS:	-1 if Open failed, > 0 otherwise
DESCRIPTION:    Opens the message catalog file which contains all the 
		language dependant messages.
CREATION:       15 Aug 1990
---------------------------------------------------------------------------*/

int UxCatOpen()
{
	catd = catopen(CAT_FILE, 0);
	return (catd);
}

/* ---------------------------------------------------------------------------
NAME:           void UxCatClose
INPUT:		None
RETURNS:	Nothing
DESCRIPTION:    Closes the message catalog file
CREATION:       15 Aug 1990
---------------------------------------------------------------------------*/

void UxCatClose()
{
    catclose(catd);
}

/* ---------------------------------------------------------------------------
NAME:           char *UxCatGets(set_num, msg_num, default_str)
INPUT:		set_num		-- Message set number
		msg_num		-- Message number
		default_str	-- default string to return if call fails
RETURNS:	A pointer to a message string
DESCRIPTION:    Retrieves a message from an open message catalog.
CREATION:       15 Aug 1990
---------------------------------------------------------------------------*/

char	*UxCatGets(set_num, msg_num, default_str)
int	set_num;
int	msg_num;
char	*default_str;
{
    char *result;

    if (catd == -1)
	return(default_str);
    result = catgets(catd, set_num, msg_num, default_str);
    if ( *result == '\0' )
	return(default_str);
    return( result);
}

/* ---------------------------------------------------------------------------
NAME:           void UxMsg(type, msg, arg)
INPUT:		type   		-- Message display type
		msg		-- Message to display (should come from the
				   message catalog
		arg 		-- additional argument - optional
RETURNS:	
DESCRIPTION:    Calls UxMsg2 with the display type argument and with 
		with any additional argument passed.
CREATION:	Aout 1990
---------------------------------------------------------------------------*/

#endif
#ifdef	RUNTIME

char	*UxInitCat()
{	
int n;

	char	*AppName;
#if !(defined(sun4) || defined(SOLARIS))
	if (!getenv("LANG"))
		putenv("LANG=C");
#endif
	n = UxCatOpen();
	AppName = UxCopyString(UxCatGets(MC_SYSTEM_NAME,MS_SYS_RUNTIME_RSRC_NAME,
					DS_MS_SYS_RUNTIME_RSRC_NAME));
	UxInitErrorHandler();
	return(AppName);
}
#endif


