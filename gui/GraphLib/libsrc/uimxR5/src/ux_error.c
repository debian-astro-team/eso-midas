/*---------------------------------------------------------------------
 * $Date: 2009-08-18 15:23:20 $             $Revision: 1.2 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

/*
.VERSION
 090818		last modif
*/


#include <stdio.h>
#include <stdarg.h>
#include "uimx_cat.h"
#include "misc1_ds.h"
#define	CGETS(x,y)	UxCatGets(MC_MISC1,(x),(y))
#define CGETS_SYS(x,y)	UxCatGets(MC_SYSTEM_NAME,(x),(y))

extern	char	*UxCopyString();
extern void UxFree();


static  char    *msg_prefix = "";

/***************************************************************************
NAME:		UxInternalError (char *file_name,char *format_str, ...)
INPUT:		see description
OUTPUT:		---
RETURN:		void
DESCRIPTION:	This function is to be used to print error messages to stderr.
		It should be called with at least 3 arguments.
		The first argument should be the name of the file where the
		error occurred. The second argument should be the line number
		within that file where the error occurred. Usually these would
		be __FILE__ and __LINE__ which are supplied by the preprocessor.
		The third argument should be a format string of the form that
		would be passed to printf. The remaining arguments (if any)
		should be the values to be printed.
		For example:
		UxInternalError("my_file", 23, "hello\n")
		would print "ERROR: my_file(23) hello"
		UxInternalError("my_file", 23, "Arg %d is wrong\n", cnt)
		would print something like "ERROR: my_file(23) Arg 13 is wrong"
EXT REFERENCES: ---
EXT EFFECTS:	---
CREATION:	Apr 10/90
REVISIONS:	Sep  6/90
			Added NLS support. This function can be called
			with a string which was received directly from
			the message catalog. It is copied before accessing
			the catalog again. But any argument to the format
			has to be preserved (through UxCopyString) by the
			caller.

---------------------------------------------------------------------------*/
void	UxInternalError (char *file_name, int line_num, ...)
{
	va_list	args;

	char *format_str;

	/* The first two args are the file name and the line number */
	va_start (args,line_num);

	format_str = UxCopyString(va_arg (args, char*));
	
	(void) fprintf (stderr, "%s", msg_prefix);
	(void) fprintf (stderr, CGETS(MS_MISC_INTERNALERRORFORMAT,
			DS_MS_MISC_INTERNALERRORFORMAT), file_name, line_num);

	/* The remaining arguments are to be passed to vfprintf() */
	(void) vfprintf (stderr, format_str, args);
	UxFree(format_str);

	va_end (args);
}

/***************************************************************************
NAME:           UxStandardError (va_alist)
INPUT:          see description
OUTPUT:         ---
RETURN:         void
CREATION:       Sep 20/90
DESCRIPTION:    This function is to be used to print error messages to stderr.
		It imitates fprintf(stderr,...) except it prefixes the string
		to be printed with the product name.
****************************************************************************/
void	UxStandardError (char *format_str, ...)
{
	va_list	args;

	va_start (args,format_str);

	(void) fprintf (stderr, "%s", msg_prefix);
	(void) vfprintf (stderr, format_str, args);

	va_end (args);
}

/*****************************************************************************
NAME:           void UxInitInternalError()
INPUT:          None

CREATION:       15/09/90
REVISIONS:      --
-----------------------------------------------------------------------------*/
void    UxInitInternalError()
{
        msg_prefix = UxCopyString(CGETS_SYS(MS_SYS_PREFIX,DS_MS_SYS_PREFIX));
}
