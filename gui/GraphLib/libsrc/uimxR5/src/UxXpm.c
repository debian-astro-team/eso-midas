/* Copyright 1990,91 GROUPE BULL -- See license conditions in file COPYRIGHT */
/*****************************************************************************\
* XpmCrIFData.c:                                                              *
*                                                                             *
*  XPM library                                                                *
*  Parse an Xpm array and create the image and possibly its mask              *
*                                                                             *
*  Developed by Arnaud Le Hors                                                *

.VERSION
 090818		last modif
\*****************************************************************************/

#include "xpmP.h"

/* UIM/X Modification:  To support transparent colors,
**                      we need to pass the widget on which
**                      the pixmap will be drawn.
*/
#include <Xm/Xm.h>

extern int UxxpmCreateImage();


int
UxXpmCreateImageFromData(display, w, data, image_return,
		       shapeimage_return, attributes)
    Display *display;
    Widget  w;
    char **data;
    XImage **image_return;
    XImage **shapeimage_return;
    XpmAttributes *attributes;
{
    xpmData mdata;
    int ErrorStatus;
    xpmInternAttrib attrib;

    /*
     * initialize return values 
     */
    if (image_return)
	*image_return = NULL;
    if (shapeimage_return)
	*shapeimage_return = NULL;

    if ((ErrorStatus = xpmOpenArray(data, &mdata)) != XpmSuccess)
	return (ErrorStatus);

    xpmInitInternAttrib(&attrib);

    ErrorStatus = xpmParseData(&mdata, &attrib, attributes);

    if (ErrorStatus == XpmSuccess) {
       /* UIM/X Modification:  To support transparent colors,
       **                      we need to pass the widget on which
       **                      the pixmap will be drawn.
       */
	ErrorStatus = UxxpmCreateImage(display, w, &attrib, image_return,
				     shapeimage_return, attributes);
    }
    if (ErrorStatus >= 0)
	xpmSetAttributes(&attrib, attributes);
    else if (attributes)
	XpmFreeAttributes(attributes);

    xpmFreeInternAttrib(&attrib);
    XpmDataClose(&mdata);

    return (ErrorStatus);
}

int
UxXpmCreatePixmapFromData(display, d, w, data, pixmap_return,
			shapemask_return, attributes)
    Display *display;
    Drawable d;
    Widget w;
    char **data;
    Pixmap *pixmap_return;
    Pixmap *shapemask_return;
    XpmAttributes *attributes;
{
    XImage *image, **imageptr = NULL;
    XImage *shapeimage, **shapeimageptr = NULL;
    int ErrorStatus;
    XGCValues gcv;
    GC gc;

    /*
     * initialize return values 
     */
    if (pixmap_return) {
	*pixmap_return = 0;		/* was: *pixmap_return = NULL; */
	imageptr = &image;
    }
    if (shapemask_return) {
	*shapemask_return = 0;		/* = NULL; */
	shapeimageptr = &shapeimage;
    }

    /*
     * create the images 
     */
     
    /* UIM/X Modification:  To support transparent colors,
    **                      we need to pass the widget on which
    **                      the pixmap will be drawn.
    */ 
    ErrorStatus = UxXpmCreateImageFromData(display, w, data, imageptr,
					 shapeimageptr, attributes);
    if (ErrorStatus < 0)
	return (ErrorStatus);

    /*
     * create the pixmaps 
     */
    if (imageptr && image) {
	*pixmap_return = XCreatePixmap(display, d, image->width,
				       image->height, image->depth);
	gcv.function = GXcopy;
	gc = XCreateGC(display, *pixmap_return, GCFunction, &gcv);

	XPutImage(display, *pixmap_return, gc, image, 0, 0, 0, 0,
		  image->width, image->height);

#ifdef Debug
	/*
	 * XDestroyImage free the image data but mnemosyne don't know about it
	 * so I free them by hand to avoid mnemalyse report it as lost data.
	 */
	free(image->data);
#endif
	XDestroyImage(image);
	XFreeGC(display, gc);
    }
    if (shapeimageptr && shapeimage) {
	*shapemask_return = XCreatePixmap(display, d, shapeimage->width,
					  shapeimage->height,
					  shapeimage->depth);
	gcv.function = GXcopy;
	gc = XCreateGC(display, *shapemask_return, GCFunction, &gcv);

	XPutImage(display, *shapemask_return, gc, shapeimage, 0, 0, 0, 0,
		  shapeimage->width, shapeimage->height);

#ifdef Debug
	/*
	 * XDestroyImage free the image data but mnemosyne don't know about it
	 * so I free them by hand to avoid mnemalyse report it as lost data.
	 */
	free(shapeimage->data);
#endif
	XDestroyImage(shapeimage);
	XFreeGC(display, gc);
    }
    return (ErrorStatus);
}
/*---------------------------------------------------------------------
 * $Date: 2009-08-18 15:23:20 $             $Revision: 1.3 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1990-1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
/*
 * Copyright 1990, 1991 GROUPE BULL
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of GROUPE BULL not be used in advertising
 * or publicity pertaining to distribution of the software without specific,
 * written prior permission.  GROUPE BULL makes no representations about the
 * suitability of this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 *
 * GROUPE BULL disclaims all warranties with regard to this software,
 * including all implied warranties of merchantability and fitness,
 * in no event shall GROUPE BULL be liable for any special,
 * indirect or consequential damages or any damages
 * whatsoever resulting from loss of use, data or profits,
 * whether in an action of contract, negligence or other tortious
 * action, arising out of or in connection with the use 
 * or performance of this software.
 *
 */

/*****************************************************************************\
* XpmRdFToI.c:                                                                *
*                                                                             *
*  XPM library                                                                *
*  Parse an XPM file and create the image and possibly its mask               *
*                                                                             *
*  Developed by Arnaud Le Hors                                                *
\*****************************************************************************/

extern xpmDataType xpmDataTypes[];

/* UIM/X Modification: 	To support transparent colors,
**			we need to pass the widget on which
** 			the pixmap will be drawn.
*/			

int
UxXpmReadFileToImage(display, w, filename, image_return,
		   shapeimage_return, attributes)
    Display *display;
    Widget  w;
    char *filename;
    XImage **image_return;
    XImage **shapeimage_return;
    XpmAttributes *attributes;
{
    xpmData mdata;
    char buf[BUFSIZ];
    int l, n = 0;
    int ErrorStatus;
    xpmInternAttrib attrib;

    /*
     * initialize return values 
     */
    if (image_return)
	*image_return = NULL;
    if (shapeimage_return)
	*shapeimage_return = NULL;

    if ((ErrorStatus = xpmReadFile(filename, &mdata)) != XpmSuccess)
	return (ErrorStatus);

    xpmInitInternAttrib(&attrib);

    /*
     * parse the header file 
     */
    mdata.Bos = '\0';
    mdata.Eos = '\n';
    mdata.Bcmt = mdata.Ecmt = NULL;
    xpmNextWord(&mdata, buf);		/* skip the first word */
    l = xpmNextWord(&mdata, buf);	/* then get the second word */
    if ((l == 3 && !strncmp("XPM", buf, 3)) ||
	(l == 4 && !strncmp("XPM2", buf, 4))) {
	if (l == 3)
	    n = 1;			/* handle XPM as XPM2 C */
	else {
	    l = xpmNextWord(&mdata, buf); /* get the type key word */

	    /*
	     * get infos about this type 
	     */
	    while (xpmDataTypes[n].type
		   && strncmp(xpmDataTypes[n].type, buf, l))
		n++;
	}
	if (xpmDataTypes[n].type) {
	    if (n == 0) {		/* natural type */
		mdata.Bcmt = xpmDataTypes[n].Bcmt;
		mdata.Ecmt = xpmDataTypes[n].Ecmt;
		xpmNextString(&mdata);	/* skip the end of headerline */
		mdata.Bos = xpmDataTypes[n].Bos;
	    } else {
		xpmNextString(&mdata);	/* skip the end of headerline */
		mdata.Bcmt = xpmDataTypes[n].Bcmt;
		mdata.Ecmt = xpmDataTypes[n].Ecmt;
		mdata.Bos = xpmDataTypes[n].Bos;
		mdata.Eos = '\0';
		xpmNextString(&mdata);	/* skip the assignment line */
	    }
	    mdata.Eos = xpmDataTypes[n].Eos;

	    ErrorStatus = xpmParseData(&mdata, &attrib, attributes);

	    if (ErrorStatus == XpmSuccess)
		ErrorStatus = UxxpmCreateImage(display, w, &attrib, image_return,
                                             shapeimage_return, attributes);
	} else
	    ErrorStatus = XpmFileInvalid;
    } else
	ErrorStatus = XpmFileInvalid;

    if (ErrorStatus >= 0)
	xpmSetAttributes(&attrib, attributes);
    else if (attributes)
	XpmFreeAttributes(attributes);

    xpmFreeInternAttrib(&attrib);
    XpmDataClose(&mdata);

    return (ErrorStatus);
}
/*---------------------------------------------------------------------
 * $Date: 2009-08-18 15:23:20 $             $Revision: 1.3 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
/*
 * Copyright 1990, 1991 GROUPE BULL
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of GROUPE BULL not be used in advertising
 * or publicity pertaining to distribution of the software without specific,
 * written prior permission.  GROUPE BULL makes no representations about the
 * suitability of this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 *
 * GROUPE BULL disclaims all warranties with regard to this software,
 * including all implied warranties of merchantability and fitness,
 * in no event shall GROUPE BULL be liable for any special,
 * indirect or consequential damages or any damages
 * whatsoever resulting from loss of use, data or profits,
 * whether in an action of contract, negligence or other tortious
 * action, arising out of or in connection with the use 
 * or performance of this software.
 *
 */

/*****************************************************************************\
* XpmRdFToP.c:                                                                *
*                                                                             *
*  XPM library                                                                *
*  Parse an XPM file and create the pixmap and possibly its mask              *
*                                                                             *
*  Developed by Arnaud Le Hors                                                *
\*****************************************************************************/

/* UIM/X Modification:  To support transparent colors,
**                      we need to pass the widget on which
**                      the pixmap will be drawn.
*/

int
UxXpmReadFileToPixmap(display, d, w, filename, pixmap_return,
		    shapemask_return, attributes)
    Display *display;
    Drawable d;
    Widget   w;
    char *filename;
    Pixmap *pixmap_return;
    Pixmap *shapemask_return;
    XpmAttributes *attributes;
{
    XImage *image, **imageptr = NULL;
    XImage *shapeimage, **shapeimageptr = NULL;
    int ErrorStatus;
    XGCValues gcv;
    GC gc;

    /*
     * initialize return values 
     */
    if (pixmap_return) {
	*pixmap_return = 0;			/* was = NULL; */
	imageptr = &image;
    }
    if (shapemask_return) {
	*shapemask_return = 0;			/* was = NULL; */
	shapeimageptr = &shapeimage;
    }

    /*
     * create the images 
     */

    /* UIM/X Modification:  To support transparent colors,
    **                      we need to pass the widget on which
    **                      the pixmap will be drawn.
    */
    ErrorStatus = UxXpmReadFileToImage(display, w, filename, imageptr,
				     shapeimageptr, attributes);
    if (ErrorStatus < 0)
	return (ErrorStatus);

    /*
     * create the pixmaps 
     */
    if (imageptr && image) {
	*pixmap_return = XCreatePixmap(display, d, image->width,
				       image->height, image->depth);
	gcv.function = GXcopy;
	gc = XCreateGC(display, *pixmap_return, GCFunction, &gcv);

	XPutImage(display, *pixmap_return, gc, image, 0, 0, 0, 0,
		  image->width, image->height);

#ifdef Debug
	/*
	 * XDestroyImage free the image data but mnemosyne don't know about it
	 * so I free them by hand to avoid mnemalyse report it as lost data.
	 */
	free(image->data);
#endif
	XDestroyImage(image);
	XFreeGC(display, gc);
    }
    if (shapeimageptr && shapeimage) {
	*shapemask_return = XCreatePixmap(display, d, shapeimage->width,
					  shapeimage->height,
					  shapeimage->depth);
	gcv.function = GXcopy;
	gc = XCreateGC(display, *shapemask_return, GCFunction, &gcv);

	XPutImage(display, *shapemask_return, gc, shapeimage, 0, 0, 0, 0,
		  shapeimage->width, shapeimage->height);

#ifdef Debug
	/*
	 * XDestroyImage free the image data but mnemosyne don't know about it
	 * so I free them by hand to avoid mnemalyse report it as lost data.
	 */
	free(shapeimage->data);
#endif
	XDestroyImage(shapeimage);
	XFreeGC(display, gc);
    }
    return (ErrorStatus);
}
