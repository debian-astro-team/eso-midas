/* @(#)unit.c	17.1 (ESO-IPG) 01/25/02 17:26:36 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:19 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
  NAME:		unit.c
  DESCRIPTION:	Utilities for Motif unit type handling.
		
  CREATION:       Nov 21 1990  Visual Edge Software
  REVISIONS:	Nov 30 1990
		-- Added ifdef statements for MOTIF_WIDGETS and added 
		   "else" cases. 
---------------------------------------------------------------------------*/

#include <Xm/Xm.h>

/******************************************************************************
NAME:		UxFindUnitType
RETURN:		unsigned char				-- unitType value
INPUTS:		Widget		wid;			-- widget in question

DESCRIPTION:	Determines the widget's unitType.
CREATION:	21 Nov 1990	(bug1154)
-----------------------------------------------------------------------------*/

unsigned char 	UxFindUnitType (wid)
	Widget		wid;
{
	unsigned char	unittype;

#ifdef MOTIF_WIDGETS
	if (XmIsVendorShell(wid)) 
	{
		XtVaGetValues (wid,
			       XmNshellUnitType, &unittype,
			       NULL);
	}
	else if (XmIsManager(wid) || XmIsPrimitive(wid) || XmIsGadget(wid)) 
	{
		XtVaGetValues (wid,
			       XmNunitType, &unittype,
			       NULL);
	}
	else
	{
		unittype = XmPIXELS;
	}
#else
	unittype = 0;
#endif /* MOTIF_WIDGETS */

    return (unittype);
}

/******************************************************************************
NAME:		UxConvertPosition
INPUTS:		Widget		wid;		-- widget in question
		unsigned char   from;		-- unitType to convert from
		Position	xval;		-- x value to convert
		Position	yval;		-- y value to convert
		unsigned char   to;		-- unitType to convert to
		Position	*x;		-- x storage
		Position	*y;		-- y storage

DESCRIPTION:	Converts x and y resources to a new unit type.
CREATION:	21 Nov 1990	(bug1154)
-----------------------------------------------------------------------------*/

void 	UxConvertPosition (wid, from, xval, yval, to, x, y)
	Widget		wid;
	unsigned char	from;
	Position	xval;
	Position	yval;
	unsigned char	to;
	Position	*x;
	Position	*y;
{
        int xInt, yInt;
#ifdef MOTIF_WIDGETS
	*x = xInt = XmConvertUnits (wid, XmHORIZONTAL, from, (int) xval, to);
	*y = yInt = XmConvertUnits (wid, XmVERTICAL,   from, (int) yval, to);
	/* if converted value is invalid, set to ZERO. */
	if (xInt != (int)(*x)) {
	   *x = 0;
	}
	if (yInt != (int)(*y)) {
	   *y = 0;
	}
#else
	*x = xval;
	*y = yval;
#endif /* MOTIF_WIDGETS */
}

/******************************************************************************
NAME:		UxConvertDimension
INPUTS:		Widget		wid;		-- widget in question
		unsigned char   from;		-- unitType to convert from
		Dimension	wval;		-- width value to convert
		Dimension	hval;		-- height value to convert
		unsigned char   to;		-- unitType to convert to
		Dimension	*w;		-- width storage
		Dimension	*h;		-- height storage

DESCRIPTION:	Converts width and height resources to a new unit type.
CREATION:	21 Nov 1990	(bug1154)
-----------------------------------------------------------------------------*/

void 	UxConvertDimension (wid, from, wval, hval, to, w, h)
	Widget		wid;
	unsigned char	from;
	Dimension	wval;
	Dimension	hval;
	unsigned char	to;
	Dimension	*w;
	Dimension	*h;
{
        int wInt, hInt;
#ifdef MOTIF_WIDGETS
	*w = wInt = XmConvertUnits (wid, XmHORIZONTAL, from, (int) wval, to);
	*h = hInt = XmConvertUnits (wid, XmVERTICAL,   from, (int) hval, to);
	/* if converted value is invalid, set to 100. */
	if (wInt != (int)(*w)) {
	   *w = XmConvertUnits (wid, XmHORIZONTAL, XmPIXELS, (int) 100, to);
	}
	if (hInt != (int)(*h)) {
	   *h = XmConvertUnits (wid, XmVERTICAL, XmPIXELS, (int) 100, to);
	}
#else
	*w = wval;
	*h = hval;
#endif /* MOTIF_WIDGETS */
}

