/*---------------------------------------------------------------------
 * $Date: 2012-05-07 09:34:30 $             $Revision: 1.3 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1989-1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

/*

.VERSION
 090818		last modif

*/



#include <string.h>
#include <stdio.h>

#ifdef MOTIF_WIDGETS
#include <X11/Intrinsic.h>
#include <X11/IntrinsicP.h>
#include <X11/RectObjP.h>
#include <Xm/DialogS.h>
#endif  /* MOTIF_WIDGETS */

#include "UxLib.h"
#include "UxLibP.h"
#include "resload.h"

#include "dstring.h"
#include <X11/ShellP.h>
#include "uimx_cat.h"
#include "lib_ds.h"
#include "valloc.h"
#include "utype.h"

#define CGETS(x,y) UxCatGets(MC_LIBUIMX,(x),(y))
  
extern void XmRegisterConverters();
extern void UxDDInit(), UxDDInstall();
extern void UxAddRuntimeResources();
extern void UxInit_colors(), UxSwidgetFreeArgs(), UxSwidgetAddNamedValue();

extern int UxIsInterface(), UxSwidgetGetWinGroupFlag();

int UxStrEqual(), UxQueryTree();
 


/*-----------------------------------------------------
 *  Standard system globals.
 *-----------------------------------------------------*/

Widget		UxTopLevel;	
XEvent   	*UxCurrentEvent;
XtAppContext 	UxAppContext;
Display		*UxDisplay;
int		UxScreen;
Window		UxRootWindow;
char		*UxDefaultShellType;
char		*UxUserShell;
char    	*UxThisWidget;
GC      	UxDrawGC;
GC  		UxUimxBlackGC;

int     UxNotifyFlag = 0;

static   void   RecordWidget UXPROTO((Widget, swidget));
static   void   RemoveWidgets UXPROTO((Widget, Widget));
static   void   GC_init UXPROTO((void));
static	 void	DestroyTree UXPROTO((swidget));
static   void   DestroyWidgetTree UXPROTO((Widget));

/*-----------------------------------------------------------------------------
NAME:		void		UxDelayUpdate (sw)
INPUT:		swidget		sw		
DESCRIPTION:	Set swidget update flag to false so that when resources
		are set update of the widget in swidget is delayed until
		UxUpdate is executed
CREATION:	14 Mar 89	run-time version
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
-----------------------------------------------------------------------------*/

void    UxDelayUpdate (sw)
	swidget sw;
{
	sw->UpdateFlag = 0;
}

/*-----------------------------------------------------------------------------
NAME:		void		UxUpdate (sw)
INPUT:		swidget		sw		
DESCRIPTION:	Update the widget in swidget and set the swidget update flag
		to true.
LAST REV:	March 93    fix3963	UxIsSwidget now called.
-----------------------------------------------------------------------------*/

void    UxUpdate (sw)
	swidget sw;
{
	if (UxIsSwidget(sw) && sw->ItsWidget) {
		if (sw->NumValues> 0) {
			XtSetValues(sw->ItsWidget, sw->Values, sw->NumValues);
		}
		sw->UpdateFlag = 1;
		UxSwidgetFreeArgs (sw);
	}
}

/*-----------------------------------------------------------------------------
NAME:		int		UxPutProp (sw, name, value)
INPUT:		swidget		sw		
		char            *name
		XtArgVal	value

DESCRIPTION:	
	General put for a value that does not require conversion.
	If the swidget is under delay, save the value in its Values list;
	otherwise, just send it to the Widget.

LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
		Aug 93	fix4472		Free proper value.
-----------------------------------------------------------------------------*/

int  	UxPutProp (sw, prop, val)
	swidget sw;
	String  prop;
	XtArgVal val;
{
	Arg 	arg[2];
	Widget  w = UxGetWidget(sw);

	/* Rename msgDialogType to dialogType so that the widget understands!
	   This is done for setting a resource value when updating or creating
	   a widget */
	if (UxStrEqual(prop, "msgDialogType")) {
		prop = "dialogType";
	}

	if (w && sw->UpdateFlag) {

	      	XtSetArg(arg[0], prop, val);
	      	XtSetValues(w, arg, 1);
		UxFreeXValue(sw, prop, val); 

	} else {

		UxSwidgetAddNamedValue(sw, prop, val);

	}
	return NO_ERROR;
}

/*-----------------------------------------------------------------------------
NAME:		void		UxGetProp (sw, name)
INPUT:		swidget		sw		
		char            *name
DESCRIPTION:	Get the value of the named resource associated with the 
		specified swidget.
		Should be used only for resources whose size is == sizeof(XtArgVal).

LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
-----------------------------------------------------------------------------*/

XtArgVal UxGetProp (sw, prop)
	swidget  sw;
	String   prop;
{
	Arg 	arg[2];
	XtArgVal  val = 0;
	Widget  w = UxGetWidget(sw);

	if (w)
	{
		XtSetArg(arg[0], prop, &val);	
	      	XtGetValues(w, arg, 1);
	      	return ((XtArgVal) val);
	} else {
	      	return (XtArgVal) UxSwidgetGetProp(sw, prop);
	}
}

/*-----------------------------------------------------------------------------
NAME:		void		UxShortGetProp (sw, name)
INPUT:		swidget		sw		
		char            *name
DESCRIPTION:	Get the value of the named resource associated with the 
		specified swidget.
CREATION:	14 Feb 90	run-time version
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
-----------------------------------------------------------------------------*/

short 	UxShortGetProp (sw, prop)
	swidget  sw;
	String   prop;
{
	Arg 	arg[2];
	short  val =0;
	Widget  w = UxGetWidget(sw);

	if (w)
	{
		XtSetArg(arg[0], prop, &val);	
	      	XtGetValues(w, arg, 1);
	      	return val;
	} else {
	      	return (short) UxSwidgetGetProp(sw, prop);
	}
}

/*-----------------------------------------------------------------------------
NAME:		void		UxCharGetProp (sw, name)
INPUT:		swidget		sw		
		char            *name
DESCRIPTION:	Get the value of the named resource associated with the 
		specified swidget.
CREATION:	14 Feb 90	run-time version
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
-----------------------------------------------------------------------------*/

char 	UxCharGetProp (sw, prop)
	swidget  sw;
	String   prop;
{
	Arg 	arg[2];
	char    val =0;
	Widget  w = UxGetWidget(sw);

	if (w)
	{
		XtSetArg(arg[0], prop, &val);	
	      	XtGetValues(w, arg, 1);
	      	return val;
	} else {
	      	return (char) UxSwidgetGetProp(sw, prop);
	}
}

/*-----------------------------------------------------------------------------
NAME:		int UxIsOverrideShell (sw)
INPUT:		swidget sw
RETURN:		integer (1 or 0)
DESCRIPTION:	check if the given swidget is an override shell.
CREATION:
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
-----------------------------------------------------------------------------*/

int  	UxIsOverrideShell (sw)
	swidget sw;
{
	return (UxGetClass(sw) == overrideShellWidgetClass);
}

/*-----------------------------------------------------------------------------
NAME:		int UxIsShellClass (cls)
INPUT:		WidgetClass cls
RETURN:		integer (1 or 0)
DESCRIPTION:	check if the given widgetclass is shellwidgetclass
CREATION:
REVISIONS:	14 Mar 89	run-time version
                29 Aug 90	OLE MERGE
		-- separator motif and ol  
-----------------------------------------------------------------------------*/

int     UxIsShellClass (cls)
	WidgetClass    cls;
{
#ifdef MOTIF_WIDGETS

	return ((cls == topLevelShellWidgetClass) ||
	        (cls == transientShellWidgetClass) ||
	        (cls == overrideShellWidgetClass) ||
	        (cls == applicationShellWidgetClass) ||
	        (cls == xmDialogShellWidgetClass));

#endif /* MOTIF_WIDGETS */
}

#ifdef MOTIF_WIDGETS
 
#include "uimxMF.c"
#endif	/* MOTIF_WIDGETS */

/*-----------------------------------------------------------------------------
NAME:		void UxAddCallback (sw, name, callback, client_data)
INPUT:		swidget sw, char *name, XtCallbackProc callback, XtPointer client_data
RETURN:
DESCRIPTION:	given swidget add callback client_data to it.
REVISIONS:	14 Mar 89	run-time version
-----------------------------------------------------------------------------*/

void UxAddCallback (sw, name, callback, client_data)
	swidget sw;
	char    *name;
	XtCallbackProc callback;
	XtPointer    client_data;
{
	if (UxGetWidget(sw)) {
		XtAddCallback(UxGetWidget(sw), name, callback, client_data);
	}
}

/*-----------------------------------------------------------------------------
NAME:		void UxMainLoop ()
INPUT:
RETURN:
DESCRIPTION:	call XtmainLoop
REVISIONS:	14 Mar 89	run-time version
		25 Aug 89       call Events routines
-----------------------------------------------------------------------------*/

void UxMainLoop ()
{
	XEvent		event;

	for (;;) {
		UxNextEvent (&event);
		UxDispatchEvent (&event);
	}
}


/******************************************************************************
NAME:		void		destroySwidgetsCallback (wid)

INPUT:		Widget		wid		- widget to destroy
		XtPointer	client_data	- not used
		XtPointer	call_data	- not used

DESCRIPTION:	Find the swidget for 'wid' and destroy all the libuimx data
		structures.  This is called as a callback from the top
		widget to be deleted in a tree.

CREATION:	Jul 29/1990
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
-----------------------------------------------------------------------------*/

static void destroySwidgetsCallback (wid, client_data, call_data)
	Widget		wid;
	XtPointer 	client_data;
	XtPointer	call_data;
{
	swidget		curr;

	curr = UxWidgetToSwidget (wid);

	DestroyTree (curr);
}
	
/******************************************************************************
NAME:		DelayedFreeData ( client_data, id )

INPUT:		XtPointer       client_data	- pointer to be freed
		XtIntervalId 	*id;

RETURN:		void

DESCRIPTION:	This  XtTimerCallbackProc function simply frees the client data.

CREATION:	Visual Edge Software		April 6 1991
-----------------------------------------------------------------------------*/
static	void    DelayedFreeData( client_data, _id )
	XtPointer       client_data;
	XtIntervalId    *_id;
{
	/* XtIntervalId    *id = _id; */

	if (client_data != NULL) {
		UxFree(client_data);
	}
}
/******************************************************************************
NAME:		void		UxDestroyContextCB (wid, con)

INPUT:		Widget		wid		- widget for callback
		XtPointer	con		- context pointer
		XtPointer 	call_data 	- not used

DESCRIPTION:	This callback function registers a timeout to free the
                context structure. This mechanism is used to ensure that
                user's destroyCallbacks are executed before we free the
                context structure.

CREATION:	Jul 29/1990
REVISIONS:	Visual Edge Software	April 30 1993 
-----------------------------------------------------------------------------*/

void UxDestroyContextCB (wid, con, call_data)
	Widget		wid;
	XtPointer	con;
	XtPointer	call_data;
{
	XtAppAddTimeOut(UxAppContext, 0,
                                DelayedFreeData, con);
}

/*-----------------------------------------------------------------------------
NAME:		void		UxDestroyInterface (sw)
INPUT:		swidget		sw		
DESCRIPTION:	Destroys the interface.
CREATION:	20 Dec 88
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
-----------------------------------------------------------------------------*/

int 	UxDestroyInterface (sw)
	swidget		sw;
{
	Widget	w = UxGetWidget(sw);

	if (! w) return (ERROR);

#ifdef MOTIF_WIDGETS
	XtAddCallback(w, XmNdestroyCallback, destroySwidgetsCallback, NULL);
#endif /* MOTIF_WIDGETS */

	XtDestroyWidget(UxRealWidget(sw));

	return NO_ERROR;
}

/* ---------------------------------------------------------------------------
NAME:		UxDestroySwidget (sw)
INPUT:		swidget 	sw;
RETURN:
DESCRIPTION:	Destroys an swidget.
CREATION:	14 Mar 89	
                16 Nov 89       added XtDestroyWidget (at top level)
		Jul 29/1990
		      - register destroy handler to destroy the swidgets then
			destroy the widgets.
---------------------------------------------------------------------------*/
void	UxDestroySwidget (sw)
	swidget 	sw;
{
	Widget	w = UxGetWidget(sw);

	if (! w) return;

#ifdef MOTIF_WIDGETS
	XtAddCallback(w, XmNdestroyCallback, destroySwidgetsCallback, NULL);
#endif /* MOTIF_WIDGETS */

	XtDestroyWidget(UxRealWidget(sw));
}

/* ---------------------------------------------------------------------------
NAME:		void		UxRealizeInterface (sw)
INPUT:		swidget		sw		
RETURN:
DESCRIPTION:	Realizes the swidget tree, so long as its parent is realized.
CREATION:	Dec 20/1988
REVISIONS:	14 Mar 89	run-time version
-----------------------------------------------------------------------------*/
int UxRealizeInterface (sw)
	swidget	sw;
{
	Widget	w =  UxRealWidget(sw);

	if (w)
	{
	    if (XtIsRealized(XtParent(w)) || XtParent(w) == UxTopLevel) {
            	XtRealizeWidget(w);
	    }
	    return NO_ERROR; 
	}
	return ERROR;
}

#ifdef MOTIF_WIDGETS
/*-----------------------------------------------------------------------------
NAME:		handle_dialog_child(wgt, manage_func)
INPUT:		Widget		wgt		- the dialogShell widget
		void	(*manage_func)()	- either XtManageChild
						  or XtUnmanageChild
RETURN:		int				- NO_ERROR if successful
						  ERROR otherwise
DESCRIPTION:	Handles the popping up or popping down of dialog shells
		by managing or unmanaging their children.
REVISIONS:	Oct 8/91	fix3035
-----------------------------------------------------------------------------*/

static	int	handle_dialog_child(wgt, manage_func)
	Widget	wgt;
	void	(*manage_func)();
{
	int	i, num_children;
	Widget	*children;
	int	error_flag = ERROR;

	XtVaGetValues (wgt,
		       XmNnumChildren, &num_children,
		       XmNchildren, &children,
		       0,NULL);

	/* We manage/unmanage the first rectObj child in the list.
	 * Note that the check for rectObjClass is necessary since some
	 * implementations of Motif add protocol children to the dialogShell.
	 * Additionally, when the LANG is set to Japanese, a widget of class
	 * Core is created. This widget should not be managed. We'll skip it.
	 */

	for (i = 0; i < num_children; i++)
	{
		if (XtIsSubclass(children[i], rectObjClass) &&
			(XtClass (children[i]) != coreWidgetClass))
		{
			(*manage_func)(children[i]);
			error_flag = NO_ERROR;
			break;
		}
	}

	return (error_flag);
}
#endif /* MOTIF_WIDGETS */

#ifdef MOTIF_WIDGETS
/*-----------------------------------------------------------------------------
NAME:		popup_dialog (wgt, grab_flag)
INPUT:		Widget		wgt		- dialogShell to pop up
		XtGrabKind	grab_flag	- the grab flag
RETURN:		void
DESCRIPTION:	Pops up the dialogShell.
-----------------------------------------------------------------------------*/

static	void	popup_dialog (wgt, grab_flag)
	Widget		wgt;
	XtGrabKind	grab_flag;
{
	if (handle_dialog_child(wgt, XtManageChild) == ERROR)
		XtPopup(wgt, grab_flag);
}
#endif /* MOTIF_WIDGETS */

/*-----------------------------------------------------------------------------
NAME:		static void		DealWithWindowGroup (sw)
INPUT:		swidget	  	sw		- swidget to popup
RETURN:		void
DESCRIPTION:	Set the window group if the window group flag is set.
CREATION:	Aug/1 /1990    (see bug866)
REVISIONS:	
-----------------------------------------------------------------------------*/
static void DealWithWindowGroup (sw)
swidget sw;
{
	Widget	w = UxGetWidget(sw);

	if (w && 
	    UxIsShellClass(UxGetClass(sw)) &&   /* if explicit shell */
	    !UxIsOverrideShell(sw) &&		/* and not override shell */
	    UxSwidgetGetWinGroupFlag(sw)) 	/* and window group flag set*/
	{
		if (!XtIsRealized(w))
			XtRealizeWidget(w);
		UxPutWindowGroup(sw, UxGetName(sw));
	}
}

/*-----------------------------------------------------------------------------
NAME:		int		UxPopupInterface (sw, type)
INPUT:		swidget	  	sw		- swidget to popup
		grabtype	type		- grab type
RETURN:		ERROR/NO_ERROR
DESCRIPTION:	If the swidget is a shell then call a Popup function using 
		the proper grab type. 
CREATION:	Dec 19/1988
REVISIONS:	March 14/1989
		Aug 1/90  (see bug866)
		-- deal with window groups
		Oct 8/91	fix3035 - use of popup_dialog()
-----------------------------------------------------------------------------*/

int 	UxPopupInterface (sw, type)
	swidget		sw;
	grabtype	type;
{
	Widget	w = UxRealWidget(sw);

	if (w && XtIsSubclass(w, shellWidgetClass)) 
	{
		XtGrabKind	xt_grab;

		/* deal with window group */
		DealWithWindowGroup(sw);

		switch (type) {
		    case nonexclusive_grab:
			xt_grab = XtGrabNonexclusive;
			break;
		    case exclusive_grab:
			xt_grab = XtGrabExclusive;
			break;
		    default:
			xt_grab = XtGrabNone;
			break;
		}

#ifdef MOTIF_WIDGETS
		if (XtIsSubclass(w, xmDialogShellWidgetClass))
			popup_dialog(w, xt_grab);
		else
#endif /* MOTIF_WIDGETS */
			XtPopup(w, xt_grab);

		return NO_ERROR;
	}
	return (ERROR);
}

#ifdef MOTIF_WIDGETS
/*-----------------------------------------------------------------------------
NAME:		popdown_dialog (wgt)
INPUT:		Widget		wgt		- dialogShell to pop down
RETURN:		void
DESCRIPTION:	Pops down the dialogShell.
-----------------------------------------------------------------------------*/

static	void	popdown_dialog (wgt)
	Widget		wgt;
{
	if (handle_dialog_child(wgt, XtUnmanageChild) == ERROR)
		XtPopdown(wgt);
}
#endif /* MOTIF_WIDGETS */

/*-----------------------------------------------------------------------------
NAME:		int		UxPopdownInterface (sw)
INPUT:		swidget	  	sw		- swidget to popdown
RETURN:		ERROR/NO_ERROR
DESCRIPTION:	If the swidget is a shell then call a popdown function.
CREATION:	Dec 19/1988
REVISIONS:	Mar 14/1989
		Oct 8/91	fix3035	- use of popdown_dialog ()
-----------------------------------------------------------------------------*/

int UxPopdownInterface (sw)
	swidget	sw;
{
	Widget	w = UxRealWidget(sw);

	if (w && XtIsSubclass(w, shellWidgetClass)) {
#ifdef MOTIF_WIDGETS
		if (XtIsSubclass(w, xmDialogShellWidgetClass))
			popdown_dialog(w);
		else
#endif /* MOTIF_WIDGETS */
			XtPopdown(w);
		return NO_ERROR;
	}
	return(ERROR);
}

/*-----------------------------------------------------------------------------
NAME:		void UxUnmap (sw)
INPUT:		swidget sw
RETURN:
DESCRIPTION:	Unmap the swidget
CREATION:
REVISIONS:	14 Mar 89	run-time version
		24 July 90   (see bug802)
		-- unmap widgets that are subclass of windowObjClass
-----------------------------------------------------------------------------*/

void 	UxUnmap (sw)
     	swidget sw;
{
	Widget	w = UxGetWidget(sw);

  	if (w)
	{
		if (UxIsInterface (sw)) {
			Widget rw = UxRealWidget (sw);
#ifdef MOTIF_WIDGETS
                	if (XtIsSubclass(rw, xmDialogShellWidgetClass))
                        	popdown_dialog(rw);
			else
#endif /* MOTIF_WIDGETS */
      			XtPopdown(rw);
		}
		/* unmap if widget is subclass of windowObjClass */
		else if (XtIsSubclass(w, widgetClass)){
			XtUnmapWidget(w);
		}
    		else {
      			XtUnmanageChild(w);
		}
  	}
}
/*--------------------------------------------------------------------------
NAME:		int UxIsInterface (sw)
INPUT:		swidget sw
RETURN:		True or False.
DESCRIPTION:	Determines if the swidget is a top level interface.
CREATION:	Nov/4/91	fix3112
REVISIONS:	March 93    fix3963	UxIsSwidget now called.
--------------------------------------------------------------------------*/
int  UxIsInterface (sw)
swidget sw;
{
	if (UxIsSwidget (sw)) 
	{
		Widget	w = UxGetWidget (sw);
		swidget parent;
		
		if (w)  {
			if (XtIsSubclass(w, shellWidgetClass))
				return 1;
		}
		else {
			WidgetClass cl = UxGetClass (sw);
			if (UxIsShellClass (cl))
				return 1;
		}
		parent = UxGetParent(sw);
		if (! UxGetWidget (parent))
			return 1;
	}
	return 0;
}
		
/*--------------------------------------------------------------------------
NAME:		void UxMap (sw)
INPUT:		swidget sw
RETURN:
DESCRIPTION:	map swidget
CREATION:	25 May 1989
REVISIONS:	24 July 90   (see bug802)
		-- map widgets that are subclass of windowObjClass
		4  Nov 91    fix3112
--------------------------------------------------------------------------*/
void 	UxMap (sw)
     	swidget sw;
{
	Widget	w = UxGetWidget(sw);

  	if (w)
	{
		if (UxIsInterface (sw))
		{
			Widget rw = UxRealWidget (sw);
#ifdef MOTIF_WIDGETS
                 	if (XtIsSubclass(rw, xmDialogShellWidgetClass))
                        	popup_dialog(rw, XtGrabNone);
			else
#endif /* MOTIF_WIDGETS */
      				XtPopup(rw, XtGrabNone);
		}
    		else if (XtIsSubclass(w, widgetClass)){
      			XtMapWidget(w);
		}
		else {
			XtManageChild(w);
		}
	}
}

/*-----------------------------------------------------------------------------
NAME:		void UxUnmanage (sw)
INPUT:		swidget sw
RETURN:
DESCRIPTION:	Unmanage the swidget
LAST REV:	Nov 92, fix3728		Always unmanage even when popping down.
-----------------------------------------------------------------------------*/

void 	UxUnmanage (sw)
     	swidget sw;
{
	Widget  w = UxGetWidget(sw);
	Widget	rw = UxRealWidget(sw);

	if (rw && XtIsSubclass(rw, shellWidgetClass)){
      		XtPopdown(rw);
	} 
  	if (w && XtIsComposite(XtParent(w))) {
      		XtUnmanageChild(w);
	}
}

/*--------------------------------------------------------------------------
NAME:		void UxManage (sw)
INPUT:		swidget sw
RETURN:
DESCRIPTION:	manage swidget
LAST REV:	Nov 92, fix3728		Always manage even when popping up.
--------------------------------------------------------------------------*/
void 	UxManage (sw)
     	swidget sw;
{
	Widget  w = UxGetWidget(sw);
	Widget	rw = UxRealWidget(sw);

  	if (w && XtIsComposite(XtParent(w))) {
		XtManageChild(w);
	}
	if (rw && XtIsSubclass(rw, shellWidgetClass)) {
      		XtPopup(rw, XtGrabNone);
	}
}

/*-----------------------------------------------------------------------------
NAME:		void		UxWaitForNotify ()
INPUT:		--
RETURN:		--
DESCRIPTION:	Do an event loop until UxNotify_flag is set
CREATION:	Jan 21/1989
REVISIONS:	--
-----------------------------------------------------------------------------*/
void UxWaitForNotify ()
{
	XEvent		*oldev, event;

	UxNotifyFlag = 0;
	oldev = UxCurrentEvent;
	UxCurrentEvent = &event;

	while (!UxNotifyFlag) {
		if (XtAppPeekEvent (UxAppContext, &event)) {
                        XtAppNextEvent(UxAppContext, &event);
                        XtDispatchEvent (&event);
                } else {
                        XtAppProcessEvent (UxAppContext,
                                           XtIMTimer | XtIMAlternateInput);
                }
	}

	UxCurrentEvent = oldev;
	UxFlushQueue ();
}

/*-----------------------------------------------------------------------------
NAME:		void		UxNotify ()
INPUT:		--
RETURN:		--
DESCRIPTION:	set UxNotify_flag to break from event loop in UxWait_for_notify
CREATION:	Jan 21/1989
REVISIONS:	Apr 29/1993 	fix4208	push back event on event Q
-----------------------------------------------------------------------------*/
void UxNotify ()
{
	UxNotifyFlag = 1;
}

/*-----------------------------------------------------------------------------
NAME:		void		UxFlushQueue ()
INPUT:		--
RETURN:		--
DESCRIPTION:	set UxNotify_flag to break from event loop in UxWait_for_notify
CREATION:	Jan 21/1989
REVISIONS:	--
-----------------------------------------------------------------------------*/
void UxFlushQueue ()
{
	XEvent	*oldev, event;

	oldev = UxCurrentEvent;

	UxCurrentEvent = &event;

	while (XtAppPending(UxAppContext))
	{
		XtAppNextEvent(UxAppContext, &event);
		XtDispatchEvent(&event);
	}

	UxCurrentEvent = oldev;
}

/*------------------------------------------------------------------------
 * NAME:	IgnoreErrors
 *
 * DESCRIPTION:	Default XErrorHandler for runtime, does nothing.
 *------------------------------------------------------------------------*/

static int IgnoreErrors (display, event)
Display *display;
XErrorEvent *event;
{
return 0;
}

/*----------------------------------------------------------------------
 * NAME: UxXtInitialize
 * DESCRIPTION: Starts the toolkit session.
 * PARAMETERS: 
 *	char *rfile  		-- The resource file of the user's 
 *			  	   application.
 *	int *arc;		-- Args from the mainline
 *	char **argc
 * RETURN: The applications top level widget
 * REVISIONS:	09/11/92	fix3711
 *--------------------------------------------------------------------*/
char		*UxProgramName = (char *) 0;
char		*UxProgramClassName = (char *) 0;

void  UxXtInitialize (rfile, argc, argv)
     	char 			*rfile;
     	int	 		*argc;
     	char 			**argv;
{
	dstring		uxapp;

	UxProgramName = strrchr(argv[0], '/');
	if (UxProgramName) 
		++UxProgramName;
	else 
		UxProgramName = argv[0];

	uxapp = dcreate("UXAPP=");
	dappend(uxapp, rfile);
	putenv(dnstr(uxapp));
	if ((UxProgramClassName = strchr(dnstr(uxapp), '=')))
		UxProgramClassName++;
	else
		UxProgramClassName = dnstr(uxapp);

	/* do not free uxapp -- memory used by putenv permanently */

#ifdef  MOTIF_WIDGETS
	XmRegisterConverters();
 	UxAddActions(action_list, XtNumber(action_list));
#endif	/* MOTIF_WIDGETS */
		
  	XSetErrorHandler(IgnoreErrors); 

  	UxDisplay = XtDisplay(UxTopLevel);
  	UxScreen  = XDefaultScreen(UxDisplay);
  	UxRootWindow  = RootWindow(UxDisplay, UxScreen);

	/* We set the geometry of UxTopLevel so that dialogShells
	   that are parented on it will get centered on the screen
	   (if defaultPosition is true). */

	XtVaSetValues(UxTopLevel,
			XtNx, 0,
			XtNy, 0,
			XtNwidth, DisplayWidth(UxDisplay, UxScreen),
			XtNheight, DisplayHeight(UxDisplay, UxScreen),
			/* 0,NULL); */ 
        /* P.Ballester 2012-04-30. The above line must be NULL terminated, not 0,NULL */ 
                        NULL); 

	UxInit_colors();
	UxInitializeResourcePath();
	UxInitializeBitmapPath();
	UxSetDefaultShell(0);
	UxUtype_init();
	UxDDInit();
	/*
	  Since MsgDialogType is only an alias to the resource DialogType,
	  it was added to the SpecialCaseCheck in tools/getputapp.c. This
	  is done to avoid generating bad code (XmNmsgDialogType) in UxGetsMF.c
	  and UxPutsMF.c. Unfortunately, the following call was never generated
	  within UxDDInit() (in runtime/uxddMF.c). So it is added here...
	 */
	UxDDInstall ("msgDialogType", UxUT_string, UxXT_MsgDialogType);
/*
 *
 *	At this point we are initializing the User defined resources.
 *
 */
	UxAddRuntimeResources();
	
	GC_init();
        return;
}

#ifdef MOTIF_WIDGETS
/*----------------------------------------------------------------------
 * NAME: UxOptionInitialize
 * DESCRIPTION: Starts off our prototyping mode and allows command line
 *		options to be set.
 * PARAMETERS: 
 *	char *rfile  	-- The resource file of the user's application.
 *	int *arc	-- Args from the mainline
 *	char **argc
 *	XrmOptionDescRec	*options -- command line arguments
 *	Cardinal	num_options      -- the number of options
 * RETURN: The applications top level widget
 * REVISIONS:	09/11/92	fix3711
 *--------------------------------------------------------------------*/
Widget  UxOptionInitialize (rfile, argc, argv, options, num_options)
     	char 		*rfile;
     	int	 	*argc;
     	char 		**argv;
	XrmOptionDescRec	*options;
	Cardinal	num_options;
{
	UxTopLevel = XtAppInitialize(&UxAppContext, rfile,
		     options, num_options, argc, argv, NULL, NULL, 0);
	UxAppInitialize(rfile, argc, argv);

	return(UxTopLevel);
}

/*----------------------------------------------------------------------
 * NAME: UxInitialize
 * DESCRIPTION: Starts off our prototyping mode
 * PARAMETERS: 
 *	char *rfile  	-- The resource file of the user's application.
 *	int *arc;	-- Args from the mainline
 *	char **argc
 * RETURN: The applications top level widget
 * REVISIONS:	09/11/92	fix3711
 *--------------------------------------------------------------------*/
Widget  UxInitialize (rfile, argc, argv)
     	char 		*rfile;
     	int	 	*argc;
     	char 		**argv;
{
	UxTopLevel = XtAppInitialize(&UxAppContext, rfile,
			         NULL, 0, argc, argv, NULL, NULL, 0);
	UxAppInitialize(rfile, argc, argv);

	return(UxTopLevel);
}

/*----------------------------------------------------------------------
 * NAME: UxAppInitialize
 * DESCRIPTION: Starts off our prototyping mode
 * PARAMETERS: 
 *	char *rfile  	-- The resource file of the user's application.
 *	int *arc;	-- Args from the mainline
 *	char **argc
 * RETURN: The applications top level widget
 * REVISIONS:	09/11/92	fix3711
 *--------------------------------------------------------------------*/
void    UxAppInitialize (rfile, argc, argv)
     	char 		*rfile;
     	int	 	*argc;
     	char 		**argv;
{
    	UxXtInitialize(rfile, argc, argv);
    	return;
}
#endif /* MOTIF_WIDGETS */

/* ---------------------------------------------------------------------------
NAME:		void UxSetDefaultShell ()
INPUT:		--
RETURN 		--
DESCRIPTION:	Initializes the shell class used depending on what the
		user places int the sub-resource:
CREATION:       Sep 24 1988  Visual Edge Software
		-- changed to allow setting of UxDefaultShellType
LAST REV:	May 92	fix3571		Removed some antique code.
---------------------------------------------------------------------------*/
void UxSetDefaultShell (shelltype)
	char	*shelltype;
{
#ifdef MOTIF_WIDGETS
#define	DEF_SHELL	"toplevel"
#endif /* MOTIF_WIDGETS */

	dstring	ds;

	if (shelltype == 0)
	    UxDefaultShellType = UxGetUimxDefault("defaultShell", DEF_SHELL);
	else
	    UxDefaultShellType = shelltype;

	/* store these in permanent storage */

	ds = dcreate(UxDefaultShellType);
	UxDefaultShellType = dnstr(ds);

#ifdef MOTIF_WIDGETS
	if (UxStrEqual(UxDefaultShellType, DEF_SHELL))
		UxUserShell = (char*) topLevelShellWidgetClass;
	else if (UxStrEqual(UxDefaultShellType, "transient"))
		UxUserShell = (char*) transientShellWidgetClass;
	else
		UxUserShell = (char*) overrideShellWidgetClass;

#endif	/* MOTIF_WIDGETS */
}


/*---------------------------------------------------------------------------
NAME:		Widget		UxRealWidget (rs)
INPUT:		swidget	        s	
RETURN:		real widget (if shell get parent widget)
DESCRIPTION:	get the real widget for UIMX.
CREATION:	Feb 27/1989
REVISIONS:	March 93    fix3963	UxIsSwidget now called.
-----------------------------------------------------------------------------*/
Widget UxRealWidget (s)
	swidget	s;
{
	Widget		wp;
	swidget         sp;
	Widget		wid = UxGetWidget(s);

	if (wid)
	{
	   if (XtIsSubclass(wid, shellWidgetClass))
	      return(wid);

	   sp = UxGetParent(s);
	   wp = XtParent(wid);

	   if (!UxIsSwidget(sp))
	      return(wp ? wp : wid);
	   else if (wp && XtIsSubclass(wp, shellWidgetClass))
	      return wp;
	   else
	      return wid;
	}
	return (Widget)0;
}
/*--------------------------------------------------------------------------
NAME:		UxAddTranslations (s, t)
INPUT:		swidget s, String t
RETURN:		--
DESCRIPTION:	add translation to swidget
CREATION:	25 May 1989
REVISIONS:      Sept 1989
                add ol virtual convertion.
		04 1990
		-- Handles policy: the switch statement is just a work around
		   a Motif bug.
--------------------------------------------------------------------------*/
void	UxAddTranslations (s, t)
	swidget	s;
	String  t;
{
	XtTranslations tr;
	Arg             a[1];
	char	*policy;
	Widget	w = UxGetWidget(s);

	if (w && t)
	{
#ifdef MOTIF_WIDGETS
           tr = XtParseTranslationTable (t);
#endif	/* MOTIF_WIDGETS */

		policy = t;
		if (*policy == '#') {
			policy++;
			switch (*policy) {
				case 'r':
	    				XtSetArg(a[0], XtNtranslations, tr);
	    				XtSetValues(w, a, 1);
					break;
				case 'a':
	    				XtAugmentTranslations(w, tr);
					break;
				case 'o':
				default:
	    				XtOverrideTranslations(w, tr);
					break;
			}
		} else {
	    		XtOverrideTranslations(w, tr);
		}
	}
}	

/*--------------------------------------------------------------------------
NAME:		void GC_init ()
INPUT:		--
RETURN:		--
DESCRIPTION:	create graphic context
CREATION:	25 May 1989
REVISIONS:	--
--------------------------------------------------------------------------*/
static void GC_init ()
{
	Window	root;
	XGCValues xgc;

  if (!UxDrawGC) {

	root = XRootWindow(UxDisplay, UxScreen);
    	xgc.foreground = 0xfd;
    	xgc.function = GXxor;
    	xgc.subwindow_mode = IncludeInferiors;
    	UxDrawGC = XCreateGC(UxDisplay, DefaultRootWindow(UxDisplay),
			   GCForeground | GCFunction | GCSubwindowMode, &xgc);

	xgc.foreground = BlackPixel(UxDisplay, UxScreen);
	xgc.background = WhitePixel(UxDisplay, UxScreen);
	UxUimxBlackGC = XCreateGC(UxDisplay, root, GCForeground | GCBackground,
			&xgc);
  }
}

/*--------------------------------------------------------------------------
NAME:		NameToSwidget (sw, name)	
INPUT:	        swidget    sw
		char	   *name
RETURN:
DESCRIPTION:      
CREATION:       1. March.90 
REVISIONS:      16, March, 90  
                -- gadget does not have name and has no children, 
                   add XrmQuarkToString to get gadget name
--------------------------------------------------------------------------*/
static  Widget NameToWidget (wid, name)
	Widget	wid;
	char	*name;
{	
	WidgetList	w_children;
	unsigned int	nc, i;
	Widget		w = (Widget)0;

	if (wid){
		if (!XtIsSubclass(wid, widgetClass)) {
                    if (UxStrEqual(XrmQuarkToString(wid->core.xrm_name), name)) 
                      return wid;
                }
		else if ((w = XtNameToWidget(wid, name)))
			return w;
		else if ((UxQueryTree(wid, &w_children, &nc))){
	     		for (i =0; i < nc; i++)
	            		if ((w = NameToWidget(w_children[i], name)))
					break;
	        	free(w_children);
		}
        }
	return w;
}

/*--------------------------------------------------------------------------
NAME:		swidget UxNameToSwidget (ref, name)
INPUT:		char *name
		swidget ref 
RETURN:		swidget
DESCRIPTION:	given a widget name and a reference swidget 
		return it's swidget object if that object exist as 
		a child or as a sibling.
CREATION:	25 May 1989
LAST REV:	May 7, 1991			See bugfix 2311.
		-- Modified to start search at parent rather than grand-parent.
		   If search fails then do search over again starting at grand-
		   parent if one exists.
--------------------------------------------------------------------------*/

swidget	UxNameToSwidget (ref, name)
swidget ref;
char	*name;
{
	Widget	  w;
	swidget	  parent = UxGetParent(ref);
	Widget    wid = UxGetWidget(parent);

	/*
	 * Search for widget starting at parent.  If not found and there
	 * is a grand-parent then restart search at grand-parent.  If still
	 * not found then do global search.
	 */

	if (wid) 
		w = NameToWidget(wid, name);
	else 
		w = NameToWidget(UxGetWidget(ref), name);

	if (wid && ! w) {
		Widget pwid = XtParent(wid);

		if (pwid)
			w = NameToWidget(pwid, name);
	}

	if (w) {
		return UxWidgetToSwidget(w);
	} else {

		/* named swidget not in "ref" widget tree so 		*/
		/* find the first occurence of the named swidget in the */
		/* swidget table. Note that for multiple instances	*/
		/* interface this may not get the appropriate swidget   */

		return UxFindSwidget(name);
	}
}

/*--------------------------------------------------------------------------
NAME:		Widget UxShellWidget (sw)
INPUT:	        swidget sw	
RETURN:	        Widget	
DESCRIPTION:    gaven the swidget return it's most top Widget	
CREATION:       13, Sept. 1989	
REVISIONS:	19, Sept. 1989
                XtParent get extra parent, So add parentwp to keep
                Shell Widget
--------------------------------------------------------------------------*/

Widget UxShellWidget (sw)
swidget sw;
{
    Widget wp, parentwp = (Widget) NULL;
    Widget w = UxGetWidget (sw);

    if (w)
    {
        while ((wp = XtParent(w)))
        {
            parentwp = w;
            w = wp;
        }
    }
    return parentwp? parentwp: w;
}
    
  
/*--------------------------------------------------------------------------
NAME:		void UxTextAppend (w, string)
INPUT:          Widget	w
		char	*string
DESCRIPTION:	appends 'string' to the end of the buffer
CREATION:	Jun 30 1988, Visual Edge Software
REVISION:	July 11, 1989
                -- added support for Motif widgets
		July 15
		-- added OpenLook support
		July 16, 1990
		-- changed the second parameter from `string` to `str`
			because `string` is a typedef for some compiliers.
		Nov 19/91	fix3189
---------------------------------------------------------------------------*/
void	UxTextAppend (w, str)
     Widget w;
     char *str;
{
#ifdef	MOTIF_WIDGETS
	int	last_position;

	last_position = XmTextGetLastPosition(w);
	XmTextReplace(w, last_position, last_position, str);
       /*
        * get last position again so that last part of
        * message is visible in message window
        */
        last_position = XmTextGetLastPosition(w);
        XmTextShowPosition(w, last_position);
#endif	/* MOTIF_WIDGETS */
}



/*--------------------------------------------------------------------------
NAME:		UxQueryTree (widget, wid_children, nc)	
INPUT:		Widget widget, WidgetList *wid_children, unsigned int *nc	
RETURN:         if given widget has children 1 otherwise return 0  
DESCRIPTION:    given widget copy its children widget list to  *wid_children 
                and copy the total number of children to *nc.  	
CREATION:	12, Oct. 89
REVISIONS:      15. Nov. 89
                fixed  popup childdren list index error. 
--------------------------------------------------------------------------*/

int	UxQueryTree (widget, wid_children, nc)
Widget		widget;
WidgetList	*wid_children;
unsigned int	*nc;
{
	WidgetList	wlist, plist;
	int		i, np, n, size;
	CompositeWidget w;


	wlist = plist = (WidgetList) 0;

        if (widget && XtIsSubclass(widget, widgetClass))
	{
		if (XtIsComposite(widget))
		{
	   		w = (CompositeWidget) widget;
	   		if ((*nc = tree_num_children(w)))
	      		wlist = tree_children(w);
		} else {
			*nc = 0;
		}

	   	if ((np = tree_num_popups(widget)))
	      		plist = tree_popups(widget);

           	if ((n = *nc + np))
		{
	      		size = n*sizeof(Widget);
              		if ((*wid_children = (WidgetList) UxMalloc(size)))
			{
	         		for (i = 0; i < *nc; i++)
	            			(*wid_children)[i]= wlist[i];
		 		for (i = *nc; i < n; i++)
		    			(*wid_children)[i]= plist[i - *nc];
		 		*nc = n;
                 		return 1;
              		}
	   	}
	}
	return 0;
}

/*--------------------------------------------------------------------------
NAME:		DestroyTree (sw)	
INPUT:	        swidget sw.
RETURN:
DESCRIPTION:    Delete given swidget and its children. 
CREATION:       12. Oct. 89
REVISIONS:      
--------------------------------------------------------------------------*/
static void DestroyTree (sw)
	swidget	sw;
{
	Widget	w = UxRealWidget(sw);

	if (w)
	   DestroyWidgetTree(w);
}

/******************************************************************************
NAME:		int		IsImplicitShell (sw)

INPUT:		swidget		sw		- swidget to check

RETURN:		1 or 0

DESCRIPTION:	Return whether this swidget has an implicit shell or not.
		It will only work correctly after the widget is created.

		It is an implicit shell if 
			there is no realized swidget parent, and
			the widget itself is not a shell.

CREATION:	Aug 02/1990
REVISIONS:	--
-----------------------------------------------------------------------------*/

static int IsImplicitShell (sw)
	swidget		sw;
{
	Widget 	wid = UxGetWidget(sw);
	Widget	pwid = UxGetWidget(UxGetParent(sw));

	if (!pwid && !XtIsSubclass(wid, shellWidgetClass))
		return(1);

	return (0);
}

/*--------------------------------------------------------------------------
NAME:		DestroyWidgetTree (widget)	
INPUT:	        widget
RETURN:
DESCRIPTION:    given widget find all its children and  delete its corresponding                Swidget.  
CREATION:       12. Oct. 89
REVISIONS:      March 93    fix3963	UxIsSwidget now called.
--------------------------------------------------------------------------*/

static  void DestroyWidgetTree (widget)
Widget	widget;
{	
	WidgetList	w_children;
	unsigned int	nc, i;	
	swidget		sw;

	if (widget)
        {  
	   if (UxQueryTree(widget, &w_children, &nc))
	   {
		for (i =0; i < nc; i++)
	            DestroyWidgetTree(w_children[i]);

	        free (w_children);
	   }

	   sw = UxWidgetToSwidget(widget);

	   if (UxIsSwidget(sw))
	   {
		if (IsImplicitShell(sw))
			RemoveWidgets(widget, UxRealWidget(sw));
		else	
			RemoveWidgets(widget, 0);

		UxFreeSwidget(sw);
	   }
	}
}


/******************************************************************************
NAME:		int	UxStrEqual (str1, str2)
INPUTS:			char *str1, *str2;
RETURN:		1 if the strings are equal, 0 otherwise

DESCRIPTION:	Compares two strings and returns 1 if they are equal, 0
                otherwise. This routine checks for NULL pointers. If both
		pointers are NULL, the strings are equal. If only one
		pointer is NULL, the strings are not equal.

CREATION:	july/29/91  fix2823
REVISIONS:	--
-----------------------------------------------------------------------------*/

int	UxStrEqual (str1, str2)
	char *str1, *str2;
{
    if (str1 == str2)
	return 1;			/* both same ptr (maybe NULL) */

    else if (!str1 || !str2)
	return 0;			/* one is NULL and one is not */

    else if (*str1 == *str2)		/* This often saves a fn call */
	return !strcmp(str1, str2);

    else
	return 0;
}		

/*------------------------------------------------------------------------
 *  The table of all widgets.
 *------------------------------------------------------------------------*/

typedef struct WidRec 
{
	Widget 	TheWidget;
	swidget TheSwidget;
} WidRec_t;

static WidRec_t * AllWidgets  = 0;
static int    	  NumWidgets  = 0;
static int	  MaxWidgets = 0;
#define TABLEBLOCK	100

/*-----------------------------------------------------------------------------
NAME:		void RecordWidget (w, m)
INPUT:		Widget w, swidget m
RETURN:
DESCRIPTION:	record widget swidget pair to widget table
CREATION:
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
-----------------------------------------------------------------------------*/

static void    RecordWidget (w, m)
	Widget   w;
	swidget  m;
{
	if (MaxWidgets <= NumWidgets)
	{
		AllWidgets = (WidRec_t*)  
			UxRealloc(AllWidgets, 
			     (MaxWidgets += TABLEBLOCK) * sizeof(WidRec_t));
	}

	AllWidgets[NumWidgets].TheWidget = w;
	AllWidgets[NumWidgets].TheSwidget = m;

	NumWidgets ++;
}

/*-----------------------------------------------------------------------------
NAME:		swidget UxWidgetToSwidget (w)
INPUT:		Widget w
RETURN:		swidget
DESCRIPTION:	given object Widget return it's corresponding swidget
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
-----------------------------------------------------------------------------*/

swidget UxWidgetToSwidget (w)
Widget  w;
{
	int i;
	for (i = 0; i < NumWidgets; i++)
	{
		if (AllWidgets[i].TheWidget == w)
		{
			return AllWidgets[i].TheSwidget;
		}
	}

	return NULL;
}

/*--------------------------------------------------------------------------
NAME:		swidget UxFindSwidget (name)
INPUT:		char *name
RETURN:		swidget
DESCRIPTION:	given a widgetname return it's swidget object
CREATION:	25 May 1989
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
--------------------------------------------------------------------------*/

swidget	UxFindSwidget (name)
	char	*name;
{
	int i;
	for (i = NumWidgets-1; i >= 0; i--)
	{
		if (UxStrEqual(name, AllWidgets[i].TheSwidget->Name))
			return AllWidgets[i].TheSwidget;
	}
	return NULL_SWIDGET;
}	

/*------------------------------------------------------------------------
 * NAME:	RemoveWidgets  
 * INPUT:
 *	Widget wid1;		-- to be removed from global table
 *	Widget wid2;		-- to be removed from global table
 *
 * DESCRIPTION:
 *	Removes one or two widgets from the AllWidgets table,
 *	shifting down all higher entries so the table is contiguous.
 *
 *	We do two, rather than one, to speed a bit the case
 *	where we destroy an implicit shell; the manager and shell 
 *	Widgets can both be removed at one pass.
 *
 * LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
 *------------------------------------------------------------------------*/

static void RemoveWidgets (wid1, wid2)
	Widget wid1;
	Widget wid2;
{
	int i;
	int Shifting = 0;	 /* # of positions we're shifting down */

	for (i = 0; i < NumWidgets; i++)
	{
		if (Shifting) {
			AllWidgets[i-Shifting] = AllWidgets[i];
		} 

		/*-----------------------------------------------------
		 * As we find the entries we want, we just increment
		 * Shifting, so the scan will slide down the rest.
		 *-----------------------------------------------------*/

		if (wid1 && AllWidgets[i].TheWidget == wid1)
		{
			wid1 = 0;
			Shifting++;
		}
		else if (wid2 && AllWidgets[i].TheWidget == wid2)
		{
			wid2 = 0;
			Shifting++;
		}
	}
	NumWidgets -= Shifting;
}

/*--------------------------------------------------------------------------
NAME:		Widget UxWindowToWidget (win)	
INPUT:	        Window win. 
RETURN:         given Window win return its Widget.
DESCRIPTION:    
CREATION:       12. Oct. 89 
LAST REV:	May 92	fix3571		Runtime swidgets are pointers.
--------------------------------------------------------------------------*/

Widget UxWindowToWidget (win)
	Window win;
{
	int i;
	for (i = NumWidgets-1; i>= 0; i--)
	{
		if (XtWindow(AllWidgets[i].TheWidget) == win)
			return AllWidgets[i].TheWidget;
	}
return (Widget) 0;
}
