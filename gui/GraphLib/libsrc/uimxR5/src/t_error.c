/*---------------------------------------------------------------------
 * $Date: 2009-08-18 15:23:20 $             $Revision: 1.3 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.

.VERSION
 090818		last modif

 *-------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>

#include "t_error.h"
#include "uimx_cat.h"
#include "lib_ds.h"
#define	CGETS(x,y)	UxCatGets(MC_LIBUIMX,(x),(y))
extern	char	*UxCopyString();

static	char	*Instances,*MallocFailed,*ReallocFailed,*Undefined;

void    UxErrorHandler(error,type,where)
int     error,type;
char    *where;
{
	switch(error){
	    case INSTANCES_EXCEEDED:
		 printf(Instances,where);
		 break;
	    case MALLOC_FAILED:
		 printf(MallocFailed,where);
		 break;
	    case REALLOC_FAILED:
		 printf(ReallocFailed,where);
		 break;
	    default:
		 printf(Undefined,where);
		 break;
	}
	if(type == FATAL)
	   exit(error);
}


void UxInitErrorHandler()
{
	Instances = UxCopyString(CGETS(MS_LU_INSTEXCEEDED,DS_MS_LU_INSTEXCEEDED));
	MallocFailed = UxCopyString(CGETS(MS_LU_MALLOCFAILED,DS_MS_LU_MALLOCFAILED));
	ReallocFailed = UxCopyString(CGETS(MS_LU_REALLOCFAILED,DS_MS_LU_REALLOCFAILED));
	Undefined = UxCopyString(CGETS(MS_LU_UNDEFERROR,DS_MS_LU_UNDEFERROR));
}





