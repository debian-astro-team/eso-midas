/*---------------------------------------------------------------------
 * $Date: 2009-08-18 15:23:20 $             $Revision: 1.3 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1990, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.

.VERSION
cw 090818051111		last modif

 *-------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <valloc.h>
#include "uimx_cat.h"
#include "misc1_ds.h"
#define GetMSG_MISC1(ms)	UXCATGETS(MC_MISC1,MS_MISC1_,ms)

/* ---------------------------------------------------------------------------
NAME:           char *UxCopyString(string)
INPUT:		string		-- String to be duplicated
RETURNS:	A pointer to a copy of the input string
DESCRIPTION:   	Copies a string and returns a copy of it.
CREATION:       7 Sept 1990
---------------------------------------------------------------------------*/
char * UxCopyString(string)
     char * string;
{
  char * new_str = NULL;
  if ( string != NULL )
    {
      new_str = UxMalloc(strlen(string) + 1);
      strcpy(new_str, string);
    }
  return(new_str);
}

/*-----------------------------------------------------------------------------
 * NAME:	UxToLowerString()
 * INPUT:	pointer to a character string
 * RETURNS:	nothing
 *
 * DESCRIPTION:	
 *   This procedure takes the incoming string and puts it in lower case. 
 *   Notice: The string being passed is directly modified by this procedure.
 *   
 * CREATION:	20 Novembre 1990
 *---------------------------------------------------------------------------*/

char	*
UxToLowerString(string_ptr)
char *string_ptr;
{
    int i=0;
    while ( string_ptr[i] != '\0') {
 	if (isupper(string_ptr[i])) {
           string_ptr[i] = tolower(string_ptr[i]);
        }
        i++;
    }
    return(string_ptr);
}

/*-----------------------------------------------------------------------------
 * NAME:	Ux_wcslen()
 * INPUT:	pointer to a wide character string
 * RETURNS:	number of wide characters is string
 *		(not including terminating null wide character, WNULL)
 *
 * DESCRIPTION:	
 *   Wide character string equivalent of strlen()
 *   
 * CREATION:	February 15, 1993
 *---------------------------------------------------------------------------*/

int Ux_wcslen(pwchar)
wchar_t *pwchar;
{
        wchar_t null_wchar;
        int count = 0;

        if (!pwchar)
                return 0;

	/* The following is done to obtain the null wide character. Using
         * WNULL is not done because it is not defined in standard header
         * files (provided in X11R5 distribution: contrib/Xwchar/wchar.h).
         */
        mbtowc(&null_wchar, "", MB_CUR_MAX);
        for (; *pwchar != null_wchar; pwchar++, count++);
        return count;
}

/******************************************************************************
NAME:           Ux_strpbrk (s1, s2)
INPUT:          char *s1        - multibyte character string to be searched
                char *s2        - multibyte search characters
OUTPUT:         ---
RETURN:         char *          - pointer to matching multibyte character
DESCRIPTION:
   This function is similar to the standard C library function, strpbrk().
   The difference is that it supports multibyte character strings.
   The Ux_strpbrk is functionally equivalent to the IBM specific function,
   mbspbrk().
   It returns a pointer to the first occurrence in string s1
   of any multibyte character from string s2, or a null pointer if
   no character from s2 exists in s1.
   A null pointer is also returned if either string is a null pointer.
   The terminating null character of s2 is not considered.
   The mechanism used to compare multibyte characters involves a conversion
   to wide characters. The argument multibyte character strings are converted
   to wide character strings one character at a time. If a multibyte character
   is encountered which cannot be converted to a wide character string, then
   the function resturns NULL. This applies to both string arguments.
   The function makes use of the ANSI C wide character string function mbtowc().
   The source for a possible implementation of mbtowc() is available from the
   X11R5 distribution in contrib/lib/Xwchar.

EXT REFERENCES: ---
EXT EFFECTS:    ---

CREATED: March 10, 1993
LAST REV:

******************************************************************************/

char *Ux_strpbrk(s1, s2)
char *s1, *s2;
{
        typedef struct {
                wchar_t wchar;
                char *str;
        } WcStr;

        wchar_t *wset;
        WcStr *wcstr;
        int len, i;
        char *ps1 = s1;
        int s1_count = 0, s2_count = 0;
        int ws1_count = 0;
        register wchar_t *wp;
	int s1_error = 0, s2_error = 0;
        char *ret_val;

	if (s1)
		s1_count = strlen(s1);
	if (s2)
		s2_count = strlen(s2);

	/* If either s1 or s2 is a null pointer or a pointer to a null character
	 * then return NULL.
	 */
	if (!s1_count || !s2_count)
		return NULL;

        /* The s1 characters are converted to wide characters and stored in
         * the array of WcStr structure. The pointer to the s1 character which
         * yielded the wide character is also stored.
         * The terminating null character is not converted.
         * The ws1_count is the number of valid converted wide characters.
         */

	wcstr = (WcStr *)UxMalloc(s1_count * sizeof(WcStr));
	while (ps1 < (s1+s1_count)) {
                len = mbtowc(&(wcstr[ws1_count].wchar), ps1, MB_CUR_MAX);
                if (len > 0) {
                        wcstr[ws1_count].str = ps1;
                        ps1 += len;
                        ws1_count++;
                }
                else {
			/* If we got here, then len must be -1 because ps1 is
			 * not the null character.
			 */
			s1_error = 1;
                        break;
                }
        }

        /* The s2 characters are converted to wide characters and stored in
         * the allocated wchar_t memory. The call to mbstowcs is sure to append
	 * a terminating null wide character to wset (if there are no errors).
         */

	wset = (wchar_t *)UxMalloc((s2_count+1) * sizeof(wchar_t));
	s2_error = ((size_t)-1 == mbstowcs(wset, s2, (s2_count+1)));
		
	if (s1_error || s2_error) {
		UxFree(wset);
		UxFree(wcstr);
		return NULL;
	}

	/* Now that both arguments have been converted, do the work
	 */
	i = 0;
	ret_val = NULL;
        do {
                for (wp = wset; *wp; wp++) {
                        if (wcstr[i].wchar == *wp) {
                                ret_val = wcstr[i].str;
				break;
			}
		}
		i++;
        }
        while ((i < ws1_count) && !ret_val);

	UxFree(wset);
	UxFree(wcstr);
        return ret_val;
}

/******************************************************************************
NAME:           char            *UxStripLeadingAndTrailingSpaces(str)

INPUT:          char            *str            - NULL terminateINPUT:          char            *str            - NULL terminated string

RETURN:         stripped string;

DESCRIPTION:    strips leading and trailing spaces.

CREATION:       July 25/90 (see bug798)
REVISIONS:      --
-----------------------------------------------------------------------------*/
char    *UxStripLeadingAndTrailingSpaces(str)
char    *str;
{
        char    *st;

        while( isspace( *str)) str++;
        for (st = str + strlen(str) - 1; (st > str) &&
                isspace((unsigned char) *st); st--) {
            *st = '\0';
        }
        return str;
}

