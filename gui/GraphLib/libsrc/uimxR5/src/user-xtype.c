/* @(#)user-xtype.c	17.1 (ESO-IPG) 01/25/02 17:26:36 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:19 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1991, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#include "UxLib.h"
#include "vtypes.h"
#include "utype.h"

void	UxAddUserDefEnumTypes()
{
	/* This stub function provides a place for the user to add
	   calls to UxAddEnumType() in order to add enumerated-resource types */
}

void	UxAddUserDefXtypes()
{
	/* This stub function provides a place for the user to add
	   calls to UxAddXType() and UxAddConv() in order to add resource types
	   other than enumerated ones */
}

