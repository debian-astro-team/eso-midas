$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ ! .COPYRIGHT   (c) 1991 European Southern Observatory
$ ! .ENVIRONMENT VMS
$ ! .AUTHOR      Carlos Guirao - [ESO/IPG]
$ ! .IDENT       MAKEFILE.COM
$ ! .COMMENTS    Compile/link procedure in [MIDAS.RELEASE.GUI.GRAPHLIB.LIBSRC.UIMXR5.SRC]
$ ! .REMARKS     Automatically generated in Unix with make.vms command
$ ! .DATE        Thu Jul 20 16:25:01 UTC 2023
$ !
$ !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$ !
$ SET NOON
$ VERIFY = F$VERIFY(1)
$ LIB/REPLACE libuimx center_vis.obj,colors.obj,dstring.obj,enum_type.obj,filepath.obj,UxMethod.obj,resload.obj,types.obj,utype.obj,valloc.obj,subproc.obj,sp_utils.obj,sp_spmd.obj,swidget.obj,t_error.obj,uimx.obj,user-rtime.obj,user-xtype.obj,user-misc.obj,uxdd.obj,uxddMF.obj,cat_utils.obj,str_utils.obj,unit.obj,ux_error.obj,utypevars.obj,XpmRdFToP.obj,XpmRdFToI.obj,hashtable.obj,create.obj,parse.obj,data.obj,misc.obj,uxpm.obj,UxXpm.obj,uxcreate.obj
$ IF VERIFY .EQ. 0 THEN SET NOVERIFY
