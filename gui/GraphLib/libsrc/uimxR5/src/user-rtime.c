/* @(#)user-rtime.c	17.1 (ESO-IPG) 01/25/02 17:26:36 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:19 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1991, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *---------------------------------------------------------------------
 * This file contains all the runtime resources conversion registration.
 * The format is to call UxrDDInstall to add a new converter.
 *-------------------------------------------------------------------*/

/*--- include files ---*/

#include <X11/Intrinsic.h>
#include "UxLib.h"
#include "utype.h"

/*--- macro symbolic constants ---*/

/*--- macro functions ---*/

/*--- types ---*/

/*--- external functions ---*/

/*--- external variables ---*/

/*--- global variables ---*/

/*--- file global variables ---*/

/*--- forward declaration of static functions ---*/

void UxAddRuntimeResources()
{

 /*
  * UxDDInstall( SquareNmajorDimension, UxUT_string, UxXT_MajorDimension );
  * UxDDInstall( SquareNmakeSquare, UxUT_string, UxXt_Boolean );
  */
}
/*--- end of file ---*/
