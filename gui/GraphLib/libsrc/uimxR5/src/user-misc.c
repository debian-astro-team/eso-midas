/*---------------------------------------------------------------------
 * $Date: 2009-08-18 15:23:20 $             $Revision: 1.2 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *---------------------------------------------------------------------
 *   File_Description_Section
 *--------------------------------------------------------------------*/

/*
.VERSION
 090818		last modif
*/


/*--- include files ---*/
#include "uxproto.h"
#include "UxLib.h"
#include "vtypes.h"
#include "utype.h"

#include <Xm/RowColumn.h>
#include <Xm/SelectioB.h>

#ifdef RUNTIME
#include "uxdd.h"
#else
#include "resource.h"
#endif /* RUNTIME */

/*--- macro symbolic constants ---*/

/*--- macro functions ---*/

/*--- types ---*/

/*--- external functions ---*/

/*--- external variables ---*/

/*--- global variables ---*/

/*--- file global variables ---*/

/*--- forward declaration of static functions ---*/

/*----------------------------------------------------------------------
 * NAME:	<UxFreeXValue(sw, xt_name, xt_value)>
 * DESCRIPTION:	<The 'xt_value' argument is assumed to be the result
 *               of a conversion from a UIM/X value.
 *               This function, which is intended for use after the value
 *               has been passed into the widget, does any cleanup necessary.
 *               Whether there is anything to do is dependent on the
 *               implementation of the 'TO_X' side of the convertor function.>
 * PARAMETERS: 
 *	widget         sw              - swidget
 *      char           *xt_name        - Xt name of resource
 *      XtArgVal       xt_value        - Xt resource value
 * RETURN:	void	
 * EXT REFS:	none	
 * EXT EFFECTS: none
 * ASSUMPTIONS:
 * REVISIONS:	23/10/92	created
 *		01/12/92	fix3749	added case for selectionarray
 * 		05/07/93	fix4234	_NO_PROTO
 * 		31/08/93	fix4472	free XmStrings on dg88
 *--------------------------------------------------------------------*/
#ifdef _NO_PROTO
void    UxFreeXValue(sw, xt_name, xt_value)
        swidget         sw;
        char            *xt_name;
        XtArgVal        xt_value;
#else
void    UxFreeXValue(swidget sw, char *xt_name, XtArgVal xt_value)
#endif /* _NO_PROTO */
{
        int     xtype, utype;

#ifdef RUNTIME
        CL_ENTRY        *dd = UxDDEntry(xt_name, UxGetClass(sw));

        if (dd == NULL)
                return;

        xtype = dd->xtype;
        utype = dd->utype;
#else
        Resource_t      *res = UxFindXtResource(sw, xt_name);

        if (res == NULL)
                return;

        xtype = UxGetXType(res);
        utype = UxGetUType(res);
#endif /* RUNTIME */


        if (xtype == UxXT_ValueWcs && utype == UxUT_string) {
                /* the convertor string_ValueWcs() allocated an array */

                UxFree((char *)xt_value);
        }

        if (xtype == UxXT_XmString && utype == UxUT_string) {

		/*-----------------------------------------------------
                 * The convertor string_XmString() allocated an XmString. 
                 * In general, when an XmString is set on a widget via 
		 * a call to XtSetValues, Motif makes a copy of the 
		 * XmString. However, some inconsistencies in Motif force
		 * us to leave some memory unfreed because the widget does
		 * not make a copy of the passed XmString but just a pointer
		 * to it. 
		 *
 		 * Widget		Resource		Platform
                 * --------------------------------------------------------
                 * XmRowColumn		XmNlabelString		All
		 *
		 * For these resources, we cannot reliably free the allocated
		 * memory after a call to XtSetValues otherwise the widget
		 * would still have a pointer to freed memory.
		 * For all these resources, we should actually register
		 * destroyCallbacks to free the allocated memory.
		 *-----------------------------------------------------*/

		Widget wid = UxGetWidget(sw);
		if (!XtIsSubclass (wid, xmRowColumnWidgetClass))
		{
			XmStringFree((XmString)xt_value);
		}
        } else if (xtype == UxXT_SelectionArray && utype == UxUT_string) {
                /* the convertor string_Selection_array() allocated an array */

                XtFree((char *) xt_value);
        }
}

/*----------------------------------------------------------------------
 * NAME:	<UxClearText>
 * DESCRIPTION:	<Clears the specified text swidget.>
 * PARAMETERS: 
 *	swidget       textsw   I text swidget to be cleared
 * RETURN:	none
 * EXT REFS:	
 * EXT EFFECTS:	
 * ASSUMPTIONS:
 * REVISIONS:	10/03/89		Creation 
 *		dd/04/93	fix4162	Use more explicit Xm calls.
 *              05/05/93	fix4225	renamed function.
 * 		05/07/93	fix4234	_NO_PROTO
 *--------------------------------------------------------------------*/
#ifdef _NO_PROTO
void    UxClearText(textsw)
        swidget textsw;
#else
void    UxClearText(swidget textsw)
#endif /* _NO_PROTO */
{
        Widget w = UxGetWidget(textsw);

        /*---------------------------------------------------------------
         * This function used to say simply XmTextSetString(w, "")
         * but that led to mysterious crashes after a lot of text
         * was shown in and then cleared from the projwin message window.
         * The following sequence is more stable;
         * the final XmTextShowPosition(w, 1) is essential to
         * avoiding those undiagnosable crashes.

         *---------------------------------------------------------------*/

        if (w) {
                int last = XmTextGetLastPosition(w);
                XmTextReplace(w, 0, last, "");
                XmTextShowPosition(w, 1);
        }
}
/*----------------------------------------------------------------------
 * NAME:	<UxFreeClientDataCB>
 * DESCRIPTION:	<Callback function to free client data.>
 * PARAMETERS: 
 *	Widget		wgt		I widget
 *	XtPointer       client_data	I pointer to be freed
 *	XtPointer	call_data	I not used
 * RETURN:	none
 * EXT REFS:	
 * EXT EFFECTS:	
 * ASSUMPTIONS:
 * REVISIONS:	05/07/93	fix4234	created (copied from UxXt.c)
 *--------------------------------------------------------------------*/
#ifdef _NO_PROTO
void    UxFreeClientDataCB( _wgt, client_data, _call_data )
	Widget          _wgt;
	XtPointer       client_data, _call_data;
#else
void    UxFreeClientDataCB( Widget _wgt, XtPointer client_data,
				XtPointer _call_data )
#endif /* _NO_PROTO */
{
	/* Widget		wgt = _wgt;
	XtPointer	call_data = _call_data; */

	if (client_data != NULL)
		XtFree((char *) client_data);
}

/*--- end of file ---*/
