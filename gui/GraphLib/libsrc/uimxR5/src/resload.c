/*---------------------------------------------------------------------
 * $Date: 2009-08-18 15:23:20 $             $Revision: 1.2 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1988, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

/*
.VERSION
 090813		last modif KB
*/

/*--- include files ---*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#ifdef USE_STDLIB
#include <stdlib.h>
#endif /* USE_STDLIB */

#ifndef _IBMR2
#include <math.h>
#endif /* _IBMR2 */

#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <X11/Xutil.h>
#include <X11/StringDefs.h>
#include "uxproto.h"
#include "version.h"
#include "global.h"
#include "resload.h"
#include "uimx_cat.h"
#include "valloc.h"
#include "vtypes.h"
#include "getsysmsg.h"
#ifndef	RUNTIME
#include "misc_ds.h"
#endif

/*--- macro symbolic constants ---*/

#ifdef MOTIF_WIDGETS
#define APPDEF "/usr/lib/X11/app-defaults"
#define APPDEF2 "/usr/lib/X11/%L/app-defaults"
#define DEFAULT_BPATH ". ~ /usr/include/X11/bitmaps/$UXAPP /usr/include/X11/bitmaps"
#endif /* MOTIF_WIDGETS */

/*--- macro functions ---*/

/* X/OPEN message catalog macros. These will make the code more compact. */
#define CGETS(ms,ds_ms)                 UxCatGets(MC_SYSTEM_NAME,ms,ds_ms)
#define MCGETS(ms,ds_ms)                UxCatGets(MC_MISC,ms,ds_ms)

/*--- types ---*/

/*--- external functions ---*/

/*--- external variables ---*/

/*--- global variables ---*/

/*--- file global variables ---*/
M_FILE_VERSION("$Header")

pathlist		UxResourcePath= NULL, UxBitmapPath= NULL;

char	*UxShellNameList[] = {
				    "applicationShell",
				    "dialogShell",
				    "overrideShell",
				    "topLevelShell",
				    "transientShell",
				    "menuShell" };
char	*UxBrowserType[] = {
	"outline",
	"tree" };

extern void UxAppend_dstring();


/*--- forward declaration of static functions ---*/


/*****************************************************************************
NAME:		void	replace_char_in_string (str, inch, outch)
INPUT:		char *str;		- string to be modified
		char inch;		- character to replace
		char outch;		- replacement char
RETURN:		modified string
DESCRIPTION:	This routine takes a string as input and changes all
		occurences of inch in it with outch.
CREATION	Jan 7 1991	(bug1199)
-----------------------------------------------------------------------------*/

void	replace_char_in_str(str, inch, outch)
	char *str;
	char inch;
	char outch;
{
	char *pntr;
	char previous = 0;

	pntr=str;
	do {	

	   if (*pntr == inch && previous != '\\')
		*pntr = outch;
	   previous = *pntr;
	} while (*pntr++ != '\0');
}

#ifndef	RUNTIME
/*****************************************************************************
NAME:		void UxUpdateScreenSpecificResources()
		void UpdateResource(qualifier, rname)
		void UpdateFontResource(qualifier, rname, font1, font2)

		char	*qualifier	- "HighRes", "MediumRes", "LowRes"
					  "Color" or "Mono"
		char	*rname		- resource name
		char	*font1		- first default font name
		char	*font2		- second default font name

DESCRIPTION:	These functions together get from the resource database
		certain UIM/X screen specific resources for the current type
		of screen and then put them back in the database without the
		screen specification if they are not already defined in the
		database.  For example on a high res monitor
			Uimx*HighRes*UxSystem*FontList:
		gets written back to the resource database as
			Uimx*UxSystem*FontList:

		For fonts, if the requested ones aren't available,
		we attempt to substitute "variable" or "fixed".

LAST REV:	March 15, 1990		See bugfix 2111
		July 29, 1991		See fix2830
		September 10, 1991	See fix295
		January 92		fix3331, fix3364
		03/04/93	fix4050	removed HP specific code
-----------------------------------------------------------------------------*/

void UxUpdateScreenSpecificResources()
{
	register char	*res;
	register int	width, height, pixels;
	extern void	UpdateResource();
	extern void	UpdateFontResource();

	res = UxGetAppResource("screenResolution");
	if (res == NULL) {
		width = DisplayWidth(UxDisplay, UxScreen);
		height = DisplayHeight(UxDisplay, UxScreen);
		pixels = width * height;
		if (pixels >= 1000 * 1000)
			res = "HighRes";
		else if (pixels >= 800 * 600)
			res = "MediumRes";
		else
			res = "LowRes";
	}

	UpdateFontResource(res, "UxSystem*SysFont1",
				"variable", "fixed");
	UpdateFontResource(res, "UxSystem*FontList",
				"variable", "fixed");
	UpdateFontResource(res, "UxSystem*XmText.fontList",
				"fixed", "variable");
	UpdateFontResource(res, "UxSystem*XmTextField.fontList",
				"fixed", "variable");
	UpdateResource(res, "uimxScale");
	UpdateResource(res, "uimxXScale");
	UpdateResource(res, "uimxYScale");

	{
		char 	*type = UxGetAppResource("screenType");

		if (type == NULL) {
			Visual *vis = DefaultVisual(UxDisplay, UxScreen);	

#if defined(__cplusplus) || defined(c_plusplus)
			if( vis->c_class == GrayScale
			    || vis->c_class == StaticGray ) {
#else
			if( vis->class == GrayScale
			    || vis->class == StaticGray ) {
#endif
				type = "Mono";
			} else {
				type = "Color";
			}
		}

		UpdateResource(type, "UxSystem*foreground");
		UpdateResource(type, "UxSystem*background");
	}
}
#endif /* ! RUNTIME */

#ifndef RUNTIME
static void UpdateResource(qualifier, rname)
	char *qualifier, *rname;
{
	dstring		res;
	XrmValue	value, value2;
	char		*type, *type2;
	int		found;
	extern char	*UxProgramName, *UxProgramClassName;
	extern void	UxPutAnyResource();

	res = dcreate(qualifier);
	dappend(res, ".");
	dappend(res, rname);

	found = UxGetAnyResource(UxProgramName, dgetstr(res), &type, &value);
	if (found) {
		found = UxGetAnyResource(UxProgramName, rname, &type2, &value2);
		if (!found) {
			/*-------------------------------------------
	 		 * Install the qualified resource 
			 * under the unqualified name.
	 		 *-------------------------------------------*/

			UxPutAnyResource(UxProgramClassName, rname, type, &value);
		}
	}

	dfree(res);
}
#endif /* ! RUNTIME */

#ifndef	RUNTIME
static void UpdateFontResource (qualifier, rname, font1, font2)
	char *qualifier, *rname, *font1, *font2;
{
    int		isGeneral;
    int		isSpecific = 0;
    char	*type;
    XrmValue	value, newvalue;
    extern char	*UxProgramName;

    /*-----------------------------------------------------
     * Get the resource value.
     *-----------------------------------------------------*/
    isGeneral = UxGetAnyResource (UxProgramName, rname, &type, &value);
    if (!isGeneral)
    {
	dstring	res;

	res = dcreate (qualifier);
	dappend (res, ".");
	dappend (res, rname);
	isSpecific = UxGetAnyResource(UxProgramName,dgetstr(res),&type,&value);
	dfree (res);
    }

    if (isGeneral || isSpecific) 
    {
    	int	isNewFont;
	/*-----------------------------------------------------
         * Validate the resource value.
	 *-----------------------------------------------------*/
	if (NO_ERROR == UxLoadOneFont ((char *)value.addr)) {
	    newvalue.addr = value.addr;
	    newvalue.size = strlen((char *)value.addr)+1;
	    isNewFont = 0;
	} else {
	    /*-----------------------------------------------------
	     * No luck.  Try the alternates....
	     *-----------------------------------------------------*/
	    UxStandardError (MCGETS (MS_MISC_FIND_FONT_CHANGE,
				    DS_MS_MISC_FIND_FONT_CHANGE), 
			     font1);

	    if (NO_ERROR == UxLoadOneFont (font1)) {
	        newvalue.addr = (caddr_t) font1;
	        newvalue.size = strlen(font1)+1;
	    } else {
	        UxStandardError (MCGETS (MS_MISC_FIND_FONT_CHANGE,
				        DS_MS_MISC_FIND_FONT_CHANGE), 
				 font2);
	        newvalue.addr = (caddr_t) font2;
	        newvalue.size = strlen(font2)+1;
	    }
	    isNewFont = 1;
	}

	/*-----------------------------------------------------
 	 * Install the validated font resource 
	 * under its unqualified name.
 	 *-----------------------------------------------------*/
	if (isSpecific || isNewFont) {

    	    extern char *UxProgramClassName;
    	    extern void	UxPutAnyResource();

	    UxPutAnyResource(UxProgramClassName, rname, type, &newvalue);
	}
    }
}
#endif


/*****************************************************************************
NAME:		void		UxInitializeResourcePath()

DESCRIPTION:	initialize the resource file load path

LAST REV:	March 13, 1991		See bugfix 2111.
		Modified to conform to X resource path.
-----------------------------------------------------------------------------*/

void UxInitializeResourcePath()
{
	static Boolean	initialized = FALSE;
	dstring		path, rdir, screen;
	char		res[32];
	char		*getenv();
	char		*appResDir;

	if( initialized == TRUE ) {
	   return;
	} else {
	   initialized = TRUE;
	}

	if(UxResourcePath != NULL)
		return;

	screen= dcreate(UxGetAppResource("screenType"));

	if(dlen(screen) == 0)
	{	
		Visual *vis = DefaultVisual(UxDisplay, UxScreen);	

#if defined(__cplusplus) || defined(c_plusplus)
		if( vis->c_class == GrayScale || vis->c_class == StaticGray ) {
#else
		if( vis->class == GrayScale || vis->class == StaticGray ) {
#endif
			screen= dcreate("mono/");
		} else {
			screen= dcreate("color/");
		}
	} else {
		if (dnstr(screen)[0] == 'C')
			dnstr(screen)[0] = 'c';
		if (dnstr(screen)[0] == 'M')
			dnstr(screen)[0] = 'm';
		if( dnstr(screen)[dlen(screen)-1] != '/' )
			dappend(screen, "/");
	}

	sprintf(res, "%dx%d", DisplayWidth(UxDisplay, UxScreen),
		DisplayHeight(UxDisplay, UxScreen));

	path= dcreate(" ");

	/*
	 * The path is ordered such that if a file is present in more than
	 * one directory, the second copy of the file override the previously
	 * loaded copy.  The following order is used:
	 *
	 *	/usr/lib/X11/app-defaluts/
	 *	/usr/lib/X11/app-defaults/$UXAPP/...
	 *	/usr/lib/X11/LANG/app-defaluts/			(for MOTIF only)
	 *	/usr/lib/X11/LANG/app-defaults/$UXAPP/...	(for MOTIF only)
	 * if XAPPLRESDIR exists
	 *		XAPPLRESDIR/$UXAPP/...
	 * else
	 *		~/$UXAPP/...
	 * for compatibility with old versions of UIM/X add current directory
	 *	./$UXAPP/...
	 */

	/* /usr/lib/X11/app-defaults/ */
	rdir = dcreate(" ");
	dappend(rdir, APPDEF);
	dappend(rdir, "/");
	dconcat(path, rdir);

	/* /usr/lib/X11/app-defaults/<ClassName> */
	dappend(rdir, "$UXAPP/");
	dconcat(path, rdir);

	/* /usr/lib/X11/app-defaults/<ClassName>/color */
	dconcat(path, rdir);
	dconcat(path, screen);

	/* /usr/lib/X11/app-defaults/<ClassName>/color/1280x1024 */
	dconcat(path, rdir);
	dconcat(path, screen);
	dappend(path, res);

	dfree(rdir);

#ifdef MOTIF_WIDGETS
	/* /usr/lib/X11/%L/app-defaults/ */
	rdir = dcreate(" ");
	dappend(rdir, APPDEF2);
	dappend(rdir, "/");
	dconcat(path, rdir);

	/* /usr/lib/X11/%L/app-defaults/<ClassName> */
	dappend(rdir, "$UXAPP/");
	dconcat(path, rdir);

	/* /usr/lib/X11/%L/app-defaults/<ClassName>/color */
	dconcat(path, rdir);
	dconcat(path, screen);

	/* /usr/lib/X11/%L/app-defaults/<ClassName>/color/1280x1024 */
	dconcat(path, rdir);
	dconcat(path, screen);
	dappend(path, res);

	dfree(rdir);
#endif /* MOTIF_WIDGETS */	

	rdir = dcreate(" ");
	if ( (appResDir = getenv( "XAPPLRESDIR" )) != NULL ) {
		dappend(rdir, appResDir);
	} else {
		dappend(rdir, "~/");
	}

	dappend(rdir, "$UXAPP/");

	/* XAPPLRESDIR/<ClassName> or ~/<ClassName> */
	dconcat(path, rdir);

	/* XAPPLRESDIR/<ClassName>/color or ~/<ClassName>/color */
	dconcat(path, rdir);
	dconcat(path, screen);

	/* XAPPLRESDIR/<ClassName>/color/res or ~/<ClassName>/color/res */
	dconcat(path, rdir);
	dconcat(path, screen);
	dappend(path, res);

	dfree(rdir);

	/*
	 * Current directory for compatibility with old versions of UIM/X.
	 */ 
	rdir = dcreate(" ./$UXAPP/");

	/* ./<ClassName> */
	dconcat(path, rdir);

	/* ./<ClassName>/color */
	dconcat(path, rdir);
	dconcat(path, screen);

	/* ./<ClassName>/color/res */
	dconcat(path, rdir);
	dconcat(path, screen);
	dappend(path, res);

	UxResourcePath= UxInitPath(dnstr(path));

	dfree(rdir);
	dfree(screen);
	dfree(path);
}

/*****************************************************************************
NAME:		char		*UxExpandResourceFilename(fname)

INPUT:		char		*fname		- resource file name

RETURN:		expanded name in a static buffer

DESCRIPTION:	find the full path to the resource file using the current
		resource file path.

LAST REV:	Jan 3/1989
-----------------------------------------------------------------------------*/

char *UxExpandResourceFilename(fname)
	char		*fname;
{
	UxInitializeResourcePath();

	return(UxExpandFilename(UxResourcePath, fname));
}


/*****************************************************************************
NAME:		void		UxInitializeBitmapPath()

DESCRIPTION:	initialize the bitmap search path

LAST REV:	Feb 25/1989
-----------------------------------------------------------------------------*/

void UxInitializeBitmapPath()
{
	if(UxBitmapPath == NULL) {
#if     defined(MOTIF_WIDGETS) & ! defined(RUNTIME)
		char *str, *fmt= "$%s/icons %s", *buf;
		str = UxGetSysMessage( SM_U_TASKDIR,
		    CGETS(MS_SYS_U_TASKDIR,DS_MS_SYS_U_TASKDIR));
		buf = UxMalloc(strlen(fmt) + strlen(str) + strlen(DEFAULT_BPATH) + 1);
		sprintf(buf,fmt,str,DEFAULT_BPATH);
		UxBitmapPath= UxInitPath(buf);
		UxFree(buf);
#else 
		UxBitmapPath= UxInitPath(DEFAULT_BPATH);
#endif
	}
}

/*****************************************************************************
NAME:		char		*UxExpandBitmapFilename(fname)

INPUT:		char		*fname		- filename

RETURN:		full path to bitmap file

DESCRIPTION:	search for the bitmap file 'fname' in the dirs given in
		UxBitmapPath

LAST REV:	Feb 21/1989
-----------------------------------------------------------------------------*/

char *UxExpandBitmapFilename(fname)
	char		*fname;
{
	UxInitializeBitmapPath();

	return(UxExpandFilename(UxBitmapPath, fname));
}

/*****************************************************************************
NAME:		void		UxOverrideResources(fname)

INPUT:		char		*fname		- resource file name

DESCRIPTION:	Load the resource file 'fname' and don't override its values
		by loading .Xdefaults again.

LAST REV:	September 24, 1992	See bugfix 3652.
		Removed XTDATABASE macro and put in new code for X11R5 support
-----------------------------------------------------------------------------*/

void UxOverrideResources(fname)
	char		*fname;
{
	XrmDatabase     database = XtDatabase( UxDisplay);
	char		*name;
	pathlist	cur_dir;

	if(fname == NULL)
		return;

	cur_dir = UxInitPath(".");
	name = UxExpandFilename(cur_dir, fname);
	dfree(*cur_dir);

	if (name == NULL)
	{	
		return;
	}

	XrmCombineFileDatabase( name, &database, True);
}

/*****************************************************************************
NAME:		void		UxLoadResources(fname)

INPUT:		char		*fname		- name of resource file

DESCRIPTION:	Load 'fname' into the current toolkit resource database
		nondestructively.

LAST REV:	September 24, 1992	See bugfix 3652.
		Removed XTDATABASE macro and put in new code for X11R5 support
-----------------------------------------------------------------------------*/

void UxLoadResources(fname)
	char		*fname;
{
	XrmDatabase     database = XtDatabase( UxDisplay);
	char		*name;
	pathlist	cur_dir;

	if(fname == NULL)
		return;

	cur_dir = UxInitPath(".");
	name = UxExpandFilename(cur_dir, fname);
	dfree(*cur_dir);

	if (name == NULL)
	{	
		return;
	}

	XrmCombineFileDatabase( name, &database, False);
}

/*****************************************************************************
NAME:		char		*UxGetResource(prog, rname)

INPUT:		char		*prog		- program name
				*rname		- resource name

RETURN:		value of the requested resource in string form 
		when successful.
		NULL is returned if the resource is not found or
		if the type of the resource is not an XtRString.

DESCRIPTION:	given a program 'name' and a resource name, get the 
		associated resource from the resource database.

LAST REV:	Jan 10/91 - bug2006
-----------------------------------------------------------------------------*/

char *UxGetResource(prog, rname)
	char		*prog, *rname;
{
	dstring		ds;
	XrmString	type;
	XrmValue	rtrn;
	extern char	*UxProgramClassName;
	dstring		ds2;
				
	ds= dcreate(prog);
	dappend(ds, ".");
	dappend(ds, rname);

	ds2 = dcreate(UxProgramClassName);
	dappend(ds2, ".");
	dappend(ds2, rname);

	if( False == XrmGetResource( XtDatabase( UxDisplay),
				     dnstr(ds), 
				     dnstr(ds2),
				     &type, 
				     &rtrn) ||
				     strcmp (type, XtRString) != 0 )
	{
	    rtrn.addr = (caddr_t) NULL;
	}

	dfree(ds);
	dfree(ds2);

	return(rtrn.addr);
}

/*****************************************************************************
NAME:		char		*UxGetAppResource(rname)
INPUT:		char		*rname		- resource name
RETURN:		resource value
DESCRIPTION:	get a resource for the program with name given by the
		environment variable UXAPP
LAST REV:	5 Sept 1990
		-- removed the ifdef's and both now use the MOTIF one;
-----------------------------------------------------------------------------*/

char *UxGetAppResource(rname)
	char		*rname;
{
	char		*rtrn;
	extern		char *UxProgramName;
	rtrn = UxGetResource(UxProgramName, rname);
	return(rtrn);
}

char *UxGetUimxResource(rname)
	char		*rname;
{
	return(UxGetAppResource(rname));
}

#ifndef RUNTIME
/***************************************************************************
NAME:		UxGetUimxWgtResource(rname)
INPUT:		char	*rname			: name of resource
OUTPUT:		---
RETURN:		char *				: string value of the resource
DESCRIPTION:	Prepends "UxSystem." to the name of the resource
		and then uses this qualified name to retrieve the value of
		the resource from the database.

		This function should be used for obtaining resources that
		apply to the widgets in UIM/X's own interfaces.
		(Such resources should be specified with "UxSystem."
		 in the app-defaults file.)
EXT REFERENCES: ---
EXT EFFECTS:	---
CREATION:	Jan 13/91

---------------------------------------------------------------------------*/
char *UxGetUimxWgtResource(rname)
	char		*rname;
{
	char		*value;
	dstring		ds;

	/* Prepend "UxSystem." to the resource name */
	ds = dcreate("UxSystem.");
	dappend(ds, rname);

	value = UxGetAppResource(dnstr(ds));

	dfree(ds);
	return (value);
}
#endif /* ! RUNTIME */

/*****************************************************************************
NAME:		char		*UxGetDefault(prog, rname, def)

INPUT:		char		*prog		- program name
				*rname		- resource name
				*def		- default value

RETURN:		resource value

DESCRIPTION:	return the resource 'rname'. If not found return 'def'

LAST REV:	Feb 25/1989
-----------------------------------------------------------------------------*/

char *UxGetDefault(prog, rname, def)
	char		*prog, *rname, *def;
{
	char		*rtrn;

	rtrn= UxGetResource(prog, rname);

	if(rtrn == NULL)
		return(def);

	return(rtrn);
}

/*****************************************************************************
NAME:		char		*UxGetUimxDefault(rname, def)

INPUT:		char		*rname		- resource name
				*def		- default value

RETURN:		resource value

DESCRIPTION:	return the Uimx resource 'rname'. If not found return 'def'

LAST REV:	Feb 25/1989
-----------------------------------------------------------------------------*/

char *UxGetAppDefault(rname, def)
	char		*rname, *def;
{
	char		*rtrn;

	rtrn= UxGetUimxResource(rname);

	if(rtrn == NULL)
		return(def);

	return(rtrn);
}

char *UxGetUimxDefault(rname, def)
	char		*rname, *def;
{
	return(UxGetAppDefault(rname, def));
}

#ifndef RUNTIME
/***************************************************************************
NAME:		UxGetUimxWgtDefault(rname, def)
INPUT:		char	*rname			: name of resource
		char	*def			: fallback value
OUTPUT:		---
RETURN:		char *				: string value of the resource
DESCRIPTION:	Prepends "UxSystem." to the name of the resource
		and then uses this qualified name to retrieve the value of
		the resource from the database. If no such resource exists
		in the database, the fallback value is returned.

		This function should be used for obtaining resources that
		apply to the widgets in UIM/X's own interfaces.
		(Such resources should be specified with "UxSystem."
		 in the app-defaults file.)
EXT REFERENCES: ---
EXT EFFECTS:	---
CREATION:	Jan 13/91

---------------------------------------------------------------------------*/
char *UxGetUimxWgtDefault(rname, def)
	char		*rname, *def;
{
	char		*value;
	dstring		ds;

	/* Prepend "UxSystem." to the resource name */
	ds = dcreate("UxSystem.");
	dappend(ds, rname);

	value = UxGetAppDefault(dnstr(ds), def);

	dfree(ds);
	return (value);
}
#endif /* ! RUNTIME */

static	int	scale_factors_uninitialized = 1;
static	float	uimxXScale = 1.0;
static	float	uimxYScale = 1.0;

static	int	InitScaleFactors()
{
	float	temp;

#ifdef MOTIF_WIDGETS
	temp = atof(UxGetAppDefault("uimxScale", "1.0"));
	uimxXScale = temp * atof(UxGetAppDefault("uimxXScale", "1.0"));
	uimxYScale = temp * atof(UxGetAppDefault("uimxYScale", "1.0"));
#endif /* MOTIF_WIDGETS */
	scale_factors_uninitialized = 0;
	return NO_ERROR;
}



int UxScaleUimxX(width)
	int		width;
{
	if (scale_factors_uninitialized)
		InitScaleFactors();
	return (width*uimxXScale + 0.5);
}

int UxScaleUimxY(height)
	int		height;
{
	if (scale_factors_uninitialized)
		InitScaleFactors();
	return (height*uimxYScale + 0.5);
}

/*****************************************************************************
NAME:		int		*UxGetAnyResource(prog, rname, type, value)

INPUT:		char		*prog		- program name
				*rname		- resource name

OUTPUT:		char		**type		- type of resource value
		XrmValue	*value		- pointer to resource value

RETURN:		True if resource is found, False otherwise.

DESCRIPTION:	Given a program 'name' and a resource name, get the 
		associated resource from the resource database.
		Handles any type of resource.

LAST REV:	March 15, 1991		See bugfix 2111.
-----------------------------------------------------------------------------*/

int	UxGetAnyResource(prog, rname, type, value)
	char		*prog, *rname, **type;
	XrmValue	*value;
{
	dstring		ds, ds2;
	int		rc;
	extern char	*UxProgramClassName;
				
	ds= dcreate(prog);
	dappend(ds, ".");
	dappend(ds, rname);

	ds2 = dcreate(UxProgramClassName);
	dappend(ds2, ".");
	dappend(ds2, rname);

	replace_char_in_str( dnstr(ds),'*','.');
	replace_char_in_str( dnstr(ds2),'*','.');

	rc = XrmGetResource(XtDatabase( UxDisplay),
			    dnstr(ds), dnstr(ds2), type, value);

	dfree(ds);
	dfree(ds2);

	return(rc);
}

/*****************************************************************************
NAME:		void		UxPutAnyResource(prog, rname, type, value)

INPUT:		char		*prog		- program name or class name
				*rname		- resource name
				*type		- type of value
		XrmValue	*value		- value for resource

DESCRIPTION:	Given a program 'name' and a resource name, put the 
		associated resource into the resource database.
		Handles any type of resource.

LAST REV:	March 15, 1991		See bugfix 2111.
-----------------------------------------------------------------------------*/

void UxPutAnyResource(prog, rname, type, value)
	char		*prog, *rname, *type;
	XrmValue	*value;
{
	XrmDatabase     database = XtDatabase( UxDisplay);

	dstring		ds;
				
	ds = dcreate(prog);
	dappend(ds, "*");	/* Use '*' because rname may not be */
	dappend(ds, rname);	/* fully specified */

	XrmPutResource(&database, dnstr(ds), type, value);

	dfree(ds);
}
/*--- end of file ---*/
