/*---------------------------------------------------------------------
 * $Date: 2009-08-18 15:23:20 $             $Revision: 1.2 $
 *---------------------------------------------------------------------
 *
 *             Copyright (c) 1990, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
/*
.VERSION
 090812		last modif KB
*/


#include <stdio.h>
#include <string.h>
#include "ctype.h"

#include <X11/StringDefs.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <X11/X.h> /* included for the win gravity resource */

#include <Xm/Xm.h>
#include <Xm/MwmUtil.h>

#ifndef	RUNTIME

#include "veos.h"
#include "valuesOf.h"	/* included just so the compiler will cross-check */
#include "swidget.h"

#ifdef MOTIF_WIDGETS
#include "scrBar.cl.h"
#endif /* MOTIF_WIDGETS */

#else
#include "UxLib.h"
#endif /* RUNTIME */

#include "utype.h"
#include "types.h"
#include "vtypes.h"
#include "valloc.h"
#include "global.h"
#include "tkdefaults.h"
#include "uimx_cat.h"
#include "misc1_ds.h"

#include "UxDialSh.h"

/* X/OPEN message catalog macros. These will make the code more compact. */
#define CGETS(ms,ds_ms)                 UxCatGets(MC_MISC1,ms,ds_ms)


extern int UxStrEqual();
extern void UxAddXValues(), UxAddUserDefEnumTypes();



/* string conversions in alphabetical order */
/* ------------------------------------------------------------------------*/

/*---------------------------------------------------------------
 * Boolean and Bool have same Uimx strings, different X type
 *---------------------------------------------------------------*/

static char *uBoolean[] = {
	"true", "false"
};
static unsigned char xBoolean[] = {
	1, 0
};
static int xBool[] = {
	1, 0
};

#ifndef	RUNTIME
int UxValuesOfBoolean(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uBoolean;
	*n = XtNumber(uBoolean);
	return *n;
}

int UxValuesOfBool(ulist, n)
char ***ulist;
int *n;
{
	return UxValuesOfBoolean (ulist, n);
}

int UxValidateBoolean(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uBoolean);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uBoolean[i]))
			return NO_ERROR;
	return ERROR;
}

int UxValidateBool(rs, s)
swidget rs;
char *s;
{
	return UxValidateBoolean(rs, s);
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static char *uInitialWindowState[] = {
	"DontCareState", "NormalState", "ZoomState", 
	"IconicState", "InactiveState"
};
static int  xInitialWindowState[] = {
	DontCareState, NormalState, ZoomState,
	IconicState, InactiveState
};

#ifndef	RUNTIME
int UxValuesOfInitialWindowState(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uInitialWindowState;
	*n = XtNumber(uInitialWindowState);
	return *n;
}

int UxValidateInitialWindowState(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uInitialWindowState);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uInitialWindowState[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

#ifdef VE_INTERNAL
static char *uWhichButton[] = {
	"button1", "button2", "button3", 
	"button4", "button5"
};
static unsigned char xWhichButton[] = {
	Button1, Button2, Button3,
	Button4, Button5
};

#ifndef	RUNTIME
int UxValuesOfWhichButton(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uWhichButton;
	*n = XtNumber(uWhichButton);
	return *n;
}

int UxValidateWhichButton(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uWhichButton);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uWhichButton[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */
#endif /* VE_INTERNAL */

/* ------------------------------------------------------------------------*/

#ifndef	RUNTIME

/** Resize recursion is a Compound resource;
 ** the X Values never actually put to use.
 **/

static	char *uResizeRecursion[] = {
	"none",
	"up",
	"down"
};

static	int  xResizeRecursion[] = {
	-1, 0, 1
};

int	UxValuesOfResizeRecursion(ulist, n)
char	***ulist;
int	*n;
{
	*ulist = uResizeRecursion;
	*n = XtNumber(uResizeRecursion);
	return( *n );
}

int	UxValidateResizeRecursion(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uResizeRecursion);

	for (i=0; i<n; i++)
		if (UxStrEqual(s, uResizeRecursion[i]))
			return NO_ERROR;
	return ERROR;
}

static	char *uDragRecursion[] = {
	"none",
	"up",
};

static	int  xDragRecursion[] = {
	0, 1
};

int	UxValuesOfDragRecursion(ulist, n)
char	***ulist;
int	*n;
{
	*ulist = uDragRecursion;
	*n = XtNumber(uDragRecursion);
	return( *n );
}

int	UxValidateDragRecursion(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uDragRecursion);

	for (i=0; i<n; i++)
		if (UxStrEqual(s, uDragRecursion[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uAlignment[] = {
	"alignment_center", "alignment_end", 
	"alignment_beginning"
	
};
static	unsigned char xAlignment[] = {
	XmALIGNMENT_CENTER, XmALIGNMENT_END,
	XmALIGNMENT_BEGINNING
};

#ifndef	RUNTIME
int UxValuesOfAlignment(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uAlignment;
	*n = XtNumber(uAlignment);
	return *n;
}

int UxValidateAlignment(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uAlignment);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uAlignment[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static  char *uEntryVerticalAlignment[] = {
        "alignment_baseline_bottom", "alignment_baseline_top",
        "alignment_contents_bottom", "alignment_center",
        "alignment_contents_top"

};
static  unsigned char xEntryVerticalAlignment[] = {
        XmALIGNMENT_BASELINE_BOTTOM, XmALIGNMENT_BASELINE_TOP,
        XmALIGNMENT_CONTENTS_BOTTOM, XmALIGNMENT_CENTER,
        XmALIGNMENT_CONTENTS_TOP
};

#ifndef RUNTIME

int UxValuesOfEntryVerticalAlignment(ulist, n)
char ***ulist;
int *n;
{
        *ulist = uEntryVerticalAlignment;
        *n = XtNumber(uEntryVerticalAlignment);
        return *n;
}

int UxValidateEntryVerticalAlignment(rs, s)
swidget rs;
char *s;
{
        int i, n=XtNumber(uEntryVerticalAlignment);

        if (s == NULL)
                return ERROR;
        for (i=0; i<n; i++)
                if (UxStrEqual(s, uEntryVerticalAlignment[i]))
                        return NO_ERROR;
        return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uChildVerticalAlignment[] = {
	"alignment_baseline_bottom", "alignment_baseline_top", 
	"alignment_widget_top", "alignment_center", 
	"alignment_widget_bottom"
	
};
static	unsigned char xChildVerticalAlignment[] = {
	XmALIGNMENT_BASELINE_BOTTOM, XmALIGNMENT_BASELINE_TOP,
	XmALIGNMENT_WIDGET_TOP, XmALIGNMENT_CENTER,
	XmALIGNMENT_WIDGET_BOTTOM
};

#ifndef	RUNTIME

int UxValuesOfChildVerticalAlignment(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uChildVerticalAlignment;
	*n = XtNumber(uChildVerticalAlignment);
	return *n;
}

int UxValidateChildVerticalAlignment(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uChildVerticalAlignment);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uChildVerticalAlignment[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uArrowDirection[] = {
	"arrow_up", "arrow_down", "arrow_left", 
	"arrow_right"
};

static	unsigned char xArrowDirection[] = {
	XmARROW_UP, XmARROW_DOWN, XmARROW_LEFT,
	XmARROW_RIGHT
};

#ifndef	RUNTIME
int UxValuesOfArrowDirection(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uArrowDirection;
	*n = XtNumber(uArrowDirection);
	return *n;
}

int UxValidateArrowDirection(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uArrowDirection);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uArrowDirection[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uAttachmentType[] = {
	"attach_none", "attach_form", 
	"attach_opposite_form", "attach_widget", 
	"attach_opposite_widget", "attach_position", 
	"attach_self"
};
static	unsigned char xAttachmentType[] = {
	XmATTACH_NONE, XmATTACH_FORM, 
	XmATTACH_OPPOSITE_FORM, XmATTACH_WIDGET, 
	XmATTACH_OPPOSITE_WIDGET, XmATTACH_POSITION,
	XmATTACH_SELF
};

#ifndef	RUNTIME
int UxValuesOfAttachmentType(ulist, n)
char ***ulist;
int *n;
{
	static char *values[1];
	static char **val;
	static Boolean first_time = TRUE;
	int nu = XtNumber(uAttachmentType);
	int nv = XtNumber(values);

	
	if (first_time)
	{
		int 		i;
		extern char     *UxCopyString();

		values[0] = UxCopyString(CGETS(MS_MISC_ATTACHMENT_MSG1, 
				DS_MS_MISC_ATTACHMENT_MSG1));
		val = (char **) UxCalloc(nu+nv, sizeof(char *));
		for (i=0; i< nu; i++)
			val[i] = uAttachmentType[i];
		for (i=nu; i< (nu+nv); i++)
			val[i] = values[i-nu];
		first_time = FALSE;
	}

	*ulist = val;
	*n = nu+nv;
	return nu;
}

static int ValidateAttachmentType(rs, s, side)
swidget rs;
char *s;
char *side;
{
	int i, n=XtNumber(uAttachmentType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uAttachmentType[i]))
		{
			if (UxCheckForAttachmentReferences (rs, side) != ERROR)
				return NO_ERROR;
		}
	return ERROR;
}

int UxValidateBottomAttachment(rs, s)
swidget rs;
char *s;
{
	return ValidateAttachmentType(rs, s, XmNbottomAttachment);
}

int UxValidateTopAttachment(rs, s)
swidget rs;
char *s;
{
	return ValidateAttachmentType(rs, s, XmNtopAttachment);
}

int UxValidateRightAttachment(rs, s)
swidget rs;
char *s;
{
	return ValidateAttachmentType(rs, s, XmNrightAttachment);
}

int UxValidateLeftAttachment(rs, s)
swidget rs;
char *s;
{
	return ValidateAttachmentType(rs, s, XmNleftAttachment);
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uAudibleWarning[] = {
	"bell", "none"
};
static	unsigned char xAudibleWarning[] = {
	XmBELL, XmNONE
};

#ifndef	RUNTIME
int UxValuesOfAudibleWarning(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uAudibleWarning;
	*n = XtNumber(uAudibleWarning);
	return *n;
}

int UxValidateAudibleWarning(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uAudibleWarning);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uAudibleWarning[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uChildType[] = {
	"frame_title_child", "frame_workarea_child", "frame_generic_child"
};
static	unsigned char xChildType[] = {
	XmFRAME_TITLE_CHILD, XmFRAME_WORKAREA_CHILD, XmFRAME_GENERIC_CHILD
};

#ifndef	RUNTIME
int UxValuesOfChildType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uChildType;
	*n = XtNumber(uChildType);
	return *n;
}

int UxValidateChildType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uChildType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uChildType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uCommandWindowLocation[] = {
	"command_above_workspace", "command_below_workspace" 
};
static	unsigned char xCommandWindowLocation[] = {
	XmCOMMAND_ABOVE_WORKSPACE, XmCOMMAND_BELOW_WORKSPACE
};

#ifndef	RUNTIME
/******************************************************************************
NAME:		int	UxValuesOfCommandWindowLocation(lst, n)
		char ***lst;
		int *n;
RETURN:		0
DESCRIPTION:	Displays the allowable values of CommandWindowLocation
CREATION:	February 5 1991		(bug2045)
REVISIONS:	
-----------------------------------------------------------------------------*/
int UxValuesOfCommandWindowLocation(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uCommandWindowLocation;
	*n = XtNumber(uCommandWindowLocation);
	return *n;
}

/******************************************************************************
NAME:		int	UxValidateCommandWindowLocation(rs, s)
		swidget	rs;
		char	 *s;
RETURN:		0
DESCRIPTION:	Validates a CommandWindowLocation
CREATION:	February 5 1991		(bug2045)
REVISIONS:	
-----------------------------------------------------------------------------*/
int UxValidateCommandWindowLocation(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uCommandWindowLocation);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uCommandWindowLocation[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/*----------------------------------------------------------*/

static  char *uChildPlacement[] = {
	 "place_above_selection", "place_below_selection",
	 "place_top"
};

static  unsigned char xChildPlacement[] = {
        XmPLACE_ABOVE_SELECTION, XmPLACE_BELOW_SELECTION,
	XmPLACE_TOP
};


#ifndef RUNTIME

/* -----------------------------------------------------------------------

NAME:           int     UxValuesOfChildPlacement(lst, n)
                char ***lst;
                int *n;
RETURN:
DESCRIPTION:    Specifies the placement of the work area child
CREATION:       October 26 1992
REVISIONS:      added for Motif1_2.2 support
-----------------------------------------------------------------------*/

int     UxValuesOfChildPlacement(ulist, n)
char    ***ulist;
int     *n;
{
        *ulist = uChildPlacement;
        *n = XtNumber(uChildPlacement);
        return  *n;
}


int UxValidateChildPlacement(rs, s)
swidget rs;
char *s;
{
        int i, n=XtNumber(uChildPlacement);

        if (s == NULL)
                return ERROR;
        for (i=0; i<n; i++)
                if (UxStrEqual(s, uChildPlacement[i]))
                        return NO_ERROR;
        return ERROR;
}
#endif /* ! RUNTIME */


/* ------------------------------------------------------------------------*/

static	char *uDefaultButtonType[] = {
	"dialog_cancel_button", "dialog_ok_button", 
	"dialog_help_button", "dialog_none"
};
static	unsigned char xDefaultButtonType[] = {
	XmDIALOG_CANCEL_BUTTON, XmDIALOG_OK_BUTTON,
	XmDIALOG_HELP_BUTTON, XmDIALOG_NONE
};

#ifndef	RUNTIME
int UxValuesOfDefaultButtonType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uDefaultButtonType;
	*n = XtNumber(uDefaultButtonType);
	return *n;
}

int UxValidateDefaultButtonType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uDefaultButtonType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uDefaultButtonType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uDeleteResponse[] = {
	"destroy", "unmap", "do_nothing"
};
static	unsigned char xDeleteResponse[] = {
	XmDESTROY, XmUNMAP, XmDO_NOTHING
};

#ifndef	RUNTIME
int UxValuesOfDeleteResponse(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uDeleteResponse;
	*n = XtNumber(uDeleteResponse);
	return *n;
}

int UxValidateDeleteResponse(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uDeleteResponse);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uDeleteResponse[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/


static	char *uDialogStyle[] = {
	"dialog_modeless", 
	"dialog_primary_application_modal", "dialog_full_application_modal",
	"dialog_system_modal", "dialog_work_area"
};

#ifdef RUNTIME
static	unsigned char xDialogStyle[] = {
	XmDIALOG_MODELESS, 
	XmDIALOG_PRIMARY_APPLICATION_MODAL, XmDIALOG_FULL_APPLICATION_MODAL,
	XmDIALOG_SYSTEM_MODAL, XmDIALOG_WORK_AREA
};
#else
     /*
      * For user widgets, dialog_system_modal, dialog_full_application_modal
      * mapped to XmDIALOG_PRIMARY_APPLICATION_MODAL .
      * UIM/X widgets use the true values.
      */
static	unsigned char xDialogStyle[] = {
	XmDIALOG_MODELESS, 
	XmDIALOG_PRIMARY_APPLICATION_MODAL, XmDIALOG_PRIMARY_APPLICATION_MODAL,
	XmDIALOG_PRIMARY_APPLICATION_MODAL, XmDIALOG_WORK_AREA
};
static	unsigned char xTrueDialogStyle[] = {
	XmDIALOG_MODELESS, 
	XmDIALOG_PRIMARY_APPLICATION_MODAL, XmDIALOG_FULL_APPLICATION_MODAL,
	XmDIALOG_SYSTEM_MODAL, XmDIALOG_WORK_AREA
};
#endif /*  RUNTIME */


#ifndef	RUNTIME
int UxValuesOfDialogStyle(ulist, n)
char ***ulist;
int *n;
{
	static char *values[1];
	static char **val;
	static Boolean first_time = TRUE;
	int nu = XtNumber(uDialogStyle);
	int nv = XtNumber(values);

	if (first_time)
	{
		int 		i;
		extern char     *UxCopyString();

		values[0] = UxCopyString(CGETS(MS_MISC1_DIAG_STYLE_MSG,
				DS_MS_MISC1_DIAG_STYLE_MSG));
		val = (char **) UxCalloc(nu+nv, sizeof(char *));
		for (i=0; i< nu; i++)
			val[i] = uDialogStyle[i];
		for (i=nu; i< (nu+nv); i++)
			val[i] = values[i-nu];
		first_time = FALSE;
	}

	*ulist = val;
	*n = nu+nv;
	return nu;
}

int UxValidateDialogStyle(rs, s)
swidget rs;
char *s;
{
	int i, dialog_shell, n=XtNumber(uDialogStyle);

	if (s == NULL)
		return ERROR;

	/*
	 * If dialog shell then the only valid choice is "dialog_work_area"
	 * otherwise "dialog_work_area" is not a valid choice.
	 */
	dialog_shell = False;

	if (UxIsDialogWidget( rs)) {
			dialog_shell = True;
	} else
	if (is_shell(rs)) {
		if (UxGetDefaultShellClass(rs) == xmDialogShellWidgetClass)
			dialog_shell = True;
	} else {
		if (UxIsSubclass(UxGetParent(rs), UxC_dialogShell))
			dialog_shell = True;
	}

	if (dialog_shell) {
		if (UxStrEqual(s, "dialog_work_area"))
			return ERROR;
	} else {
		if (! UxStrEqual(s, "dialog_work_area"))
			return ERROR;
	}

	for (i=0; i<n; i++)
		if (UxStrEqual(s, uDialogStyle[i]))
			return NO_ERROR;

	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uDialogType[] = {

        "dialog_prompt",
        "dialog_selection", "dialog_file_selection",
        "dialog_work_area", "dialog_command"
};

static	unsigned char xDialogType[] = {

        XmDIALOG_PROMPT,
        XmDIALOG_SELECTION, XmDIALOG_FILE_SELECTION,
        XmDIALOG_WORK_AREA, XmDIALOG_COMMAND
};

#ifndef	RUNTIME
int UxValuesOfDialogType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uDialogType;
	*n = XtNumber(uDialogType);
	return *n;
}

int UxValidateDialogType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uDialogType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uDialogType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uEditMode[] = {
	"single_line_edit", "multi_line_edit" 
};
static	int xEditMode[] = {
	(int) XmSINGLE_LINE_EDIT, (int) XmMULTI_LINE_EDIT 
};

#ifndef	RUNTIME
int UxValuesOfEditMode(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uEditMode;
	*n = XtNumber(uEditMode);
	return *n;
}

int UxValidateEditMode(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uEditMode);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uEditMode[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/*  ------------------------------------------------------- */
#ifdef MOTIF_WIDGETS
static char * uFileTypeMask[] = {
	"file_regular", "file_directory", "file_any_type"
};
static unsigned char xFileTypeMask[] = {
	XmFILE_REGULAR, XmFILE_DIRECTORY, XmFILE_ANY_TYPE
};

#ifndef	RUNTIME
/******************************************************************************
NAME:		int	UxValuesOfFileTypeMask(lst, n)
		char ***lst;
		int *n;
RETURN:		0
DESCRIPTION:	Displays the allowable values of FileTypeMask
CREATION:	January 29 1991		(bug2032)
REVISIONS:	
-----------------------------------------------------------------------------*/
int UxValuesOfFileTypeMask(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uFileTypeMask;
	*n = XtNumber(uFileTypeMask);
	return *n;
}

/******************************************************************************
NAME:		int	UxValidateFileTypeMask(rs, s)
		rswidget	*rs;	- rswidget
		char	*s;		- name.
RETURN:		ERROR or NO_ERROR
DESCRIPTION:	Validates FileTypeMask
CREATION:	January 29 1991		(bug2032)
REVISIONS:	
-----------------------------------------------------------------------------*/
int UxValidateFileTypeMask(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uFileTypeMask);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uFileTypeMask[i]))
			return NO_ERROR;
	return ERROR;
}
#endif
#endif
/* ------------------------------------------------------------------------*/

static	char *uIndicatorType[] = {
	"one_of_many", "n_of_many"
};
static	unsigned char xIndicatorType[] = {
	XmONE_OF_MANY, XmN_OF_MANY
};

#ifndef	RUNTIME
int UxValuesOfIndicatorType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uIndicatorType;
	*n = XtNumber(uIndicatorType);
	return *n;
}

int UxValidateIndicatorType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uIndicatorType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uIndicatorType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uKeyboardFocusPolicy[] = {
	"explicit", "pointer"
};
static	unsigned char xKeyboardFocusPolicy[] = {
	XmEXPLICIT, XmPOINTER
};

#ifndef	RUNTIME
int UxValuesOfKeyboardFocusPolicy(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uKeyboardFocusPolicy;
	*n = XtNumber(uKeyboardFocusPolicy);
	return *n;
}

int UxValidateKeyboardFocusPolicy(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uKeyboardFocusPolicy);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uKeyboardFocusPolicy[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uLabelType[] = {
	"string", "pixmap"
};
static	unsigned char xLabelType[] = {
	XmSTRING, XmPIXMAP
};

#ifndef	RUNTIME
int UxValuesOfLabelType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uLabelType;
	*n = XtNumber(uLabelType);
	return *n;
}

int UxValidateLabelType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uLabelType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uLabelType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uListSizePolicy[] = {
	"constant", "variable", "resize_if_possible"
};
static	unsigned char xListSizePolicy[] = {
	XmCONSTANT, XmVARIABLE, XmRESIZE_IF_POSSIBLE
};

#ifndef	RUNTIME
int UxValuesOfListSizePolicy(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uListSizePolicy;
	*n = XtNumber(uListSizePolicy);
	return *n;
}

int UxValidateListSizePolicy(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uListSizePolicy);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uListSizePolicy[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uMsgDialogType[] = {

	"dialog_error", "dialog_information",
        "dialog_message", "dialog_question",
        "dialog_template", "dialog_warning",
        "dialog_working"
};

static	unsigned char xMsgDialogType[] = {

	XmDIALOG_ERROR, XmDIALOG_INFORMATION,
        XmDIALOG_MESSAGE, XmDIALOG_QUESTION,
        XmDIALOG_TEMPLATE, XmDIALOG_WARNING,
        XmDIALOG_WORKING
};

#ifndef	RUNTIME
int UxValuesOfMsgDialogType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uMsgDialogType;
	*n = XtNumber(uMsgDialogType);
	return *n;
}

int UxValidateMsgDialogType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uMsgDialogType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uMsgDialogType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

#ifdef MOTIF_WIDGETS
static	char * uMwmInputMode[] = {
	"-1", "mwm_input_modeless",
	"mwm_input_primary_application_modal",
	"mwm_input_system_modal", "mwm_input_full_application_modal"
};

#ifdef RUNTIME
static	int xMwmInputMode[] = {
	-1, MWM_INPUT_MODELESS, MWM_INPUT_PRIMARY_APPLICATION_MODAL,
	MWM_INPUT_SYSTEM_MODAL, MWM_INPUT_FULL_APPLICATION_MODAL
};
#else
static	int xMwmInputMode[] = {
	-1, MWM_INPUT_MODELESS, MWM_INPUT_PRIMARY_APPLICATION_MODAL,
	MWM_INPUT_PRIMARY_APPLICATION_MODAL, MWM_INPUT_PRIMARY_APPLICATION_MODAL
};
static	int xTrueMwmInputMode[] = {
	-1, MWM_INPUT_MODELESS, MWM_INPUT_PRIMARY_APPLICATION_MODAL,
	MWM_INPUT_SYSTEM_MODAL, MWM_INPUT_FULL_APPLICATION_MODAL
};
#endif /* RUNTIME */

/******************************************************************************
NAME:		int	UxValuesOfMwmInputMode(lst, n)
		char ***lst;
		int *n;
RETURN:		0
DESCRIPTION:	Displays the allowable values of MwmInputMode
CREATION:	February 4 1993		(bug1950)
REVISIONS:	
-----------------------------------------------------------------------------*/
#ifndef	RUNTIME
int UxValuesOfMwmInputMode(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uMwmInputMode;
	*n = XtNumber(uMwmInputMode);
	return *n;
}

/******************************************************************************
NAME:		int	UxValidateMwmInputMode(rs, s)
		rswidget	*rs;	- rswidget
		char	*s;		- name.
RETURN:		ERROR or NO_ERROR
DESCRIPTION:	Validates MwmInputMode
CREATION:	February 4 1993		(bug1950)
REVISIONS:	
-----------------------------------------------------------------------------*/
int UxValidateMwmInputMode(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uMwmInputMode);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uMwmInputMode[i]))
			return NO_ERROR;
	return ERROR;
}
#endif
#endif

/* ------------------------------------------------------------------------*/

#ifdef MOTIF_WIDGETS
static	char * uMultiClick[] = {
	"multiclick_keep", "multiclick_discard"
};
static	unsigned char xMultiClick[] = {
	XmMULTICLICK_KEEP, XmMULTICLICK_DISCARD
};

/******************************************************************************
NAME:		int	UxValuesOfMultiClick(lst, n)
		char ***lst;
		int *n;
RETURN:		0
DESCRIPTION:	Displays the allowable values of MultiClick
CREATION:	January 29 1991		(bug2035)
REVISIONS:	
-----------------------------------------------------------------------------*/
#ifndef	RUNTIME
int UxValuesOfMultiClick(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uMultiClick;
	*n = XtNumber(uMultiClick);
	return *n;
}

/******************************************************************************
NAME:		int	UxValidateMultiClick(rs, s)
		rswidget	*rs;	- rswidget
		char	*s;		- name.
RETURN:		ERROR or NO_ERROR
DESCRIPTION:	Validates MultiClick's
CREATION:	January 29 1991		(bug2035)
REVISIONS:	
-----------------------------------------------------------------------------*/
int UxValidateMultiClick(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uMultiClick);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uMultiClick[i]))
			return NO_ERROR;
	return ERROR;
}
#endif
#endif

/* ------------------------------------------------------------------------*/

#ifdef MOTIF_WIDGETS
static	char * uNavigationType[] = {
	"none", "tab_group", "sticky_tab_group", "exclusive_tab_group"
};
static	unsigned char xNavigationType[] = {
	XmNONE, XmTAB_GROUP, XmSTICKY_TAB_GROUP, XmEXCLUSIVE_TAB_GROUP
};

/******************************************************************************
NAME:		int	UxValuesOfNavigationType(lst, n)
		char ***lst;
		int *n;
RETURN:		0
DESCRIPTION:	Displays the allowable values of NavigationType
CREATION:	January 29 1991		(bug2035)
REVISIONS:	
-----------------------------------------------------------------------------*/
#ifndef	RUNTIME
int UxValuesOfNavigationType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uNavigationType;
	*n = XtNumber(uNavigationType);
	return *n;
}

/******************************************************************************
NAME:		int	UxValidateNavigationType(rs, s)
		rswidget	*rs;	- rswidget
		char	*s;		- name.
RETURN:		ERROR or NO_ERROR
DESCRIPTION:	Validates NavigationType's
CREATION:	January 29 1991		(bug2035)
REVISIONS:	
-----------------------------------------------------------------------------*/
int UxValidateNavigationType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uNavigationType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uNavigationType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif
#endif

/* ------------------------------------------------------------------------*/

static	char *uOrientation[] = {
	"vertical", "horizontal"
};
static	unsigned char xOrientation[] = {
	XmVERTICAL, XmHORIZONTAL
};

#ifndef	RUNTIME
int UxValuesOfOrientation(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uOrientation;
	*n = XtNumber(uOrientation);
	return *n;
}

int UxValidateOrientation(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uOrientation);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uOrientation[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uPacking[] = {
	"pack_tight", "pack_column", "pack_none"
};
static	unsigned char xPacking[] = {
	XmPACK_TIGHT, XmPACK_COLUMN, XmPACK_NONE
};

#ifndef	RUNTIME
int UxValuesOfPacking(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uPacking;
	*n = XtNumber(uPacking);
	return *n;
}

int UxValidatePacking(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uPacking);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uPacking[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uProcessingDirection[] = {
	"max_on_top", "max_on_bottom", 
	"max_on_left", "max_on_right"
};
static	unsigned char xProcessingDirection[] = {
	XmMAX_ON_TOP, XmMAX_ON_BOTTOM,
	XmMAX_ON_LEFT, XmMAX_ON_RIGHT
};

#ifndef	RUNTIME
int UxValuesOfProcessingDirection(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uProcessingDirection;
	*n = XtNumber(uProcessingDirection);
	return *n;
}

int UxValidateProcessingDirection(rs, s)
swidget rs;
char *s;
{
	int i;
	char *hv = UX_DEFAULT_SB_ORIENTATION;

	(void) UxFindValue(rs, UxGetRD_orientation(rs), &hv);
	if (s == NULL)
		return ERROR;
	if (UxStrEqual(hv, "vertical") &&
	   (UxStrEqual(s, "max_on_top") || UxStrEqual(s, "max_on_bottom")))
		return NO_ERROR;
	if (UxStrEqual(hv, "horizontal") &&
	   (UxStrEqual(s, "max_on_left") || UxStrEqual(s, "max_on_right")))
		return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uResizePolicy[] = {
	"resize_none", "resize_any", 
	"resize_grow"
};
static	unsigned char xResizePolicy[] = {
	XmRESIZE_NONE, XmRESIZE_ANY,
	XmRESIZE_GROW
};

#ifndef	RUNTIME
int UxValuesOfResizePolicy(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uResizePolicy;
	*n = XtNumber(uResizePolicy);
	return *n;
}

int UxValidateResizePolicy(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uResizePolicy);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uResizePolicy[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uRowColumnType[] = {
	"work_area", "menu_bar", "menu_pulldown", 
	"menu_popup", "menu_option"
};
static	unsigned char xRowColumnType[] = {
	XmWORK_AREA, XmMENU_BAR, XmMENU_PULLDOWN,
	XmMENU_POPUP, XmMENU_OPTION
};

#ifndef	RUNTIME
int UxValuesOfRowColumnType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uRowColumnType;
	*n = XtNumber(uRowColumnType);
	return *n;
}

int UxValidateRowColumnType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uRowColumnType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uRowColumnType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uScrollBarDisplayPolicy[] = {
	"as_needed", "static"
};
static	unsigned char xScrollBarDisplayPolicy[] = {
	XmAS_NEEDED, XmSTATIC
};

#ifndef	RUNTIME
int UxValuesOfScrollBarDisplayPolicy(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uScrollBarDisplayPolicy;
	*n = XtNumber(uScrollBarDisplayPolicy);
	return *n;
}

int UxValidateScrollBarDisplayPolicy(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uScrollBarDisplayPolicy);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uScrollBarDisplayPolicy[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uScrollBarPlacement[] = {
	"top_left", "bottom_left", "top_right", 
	"bottom_right"
};
static	unsigned char xScrollBarPlacement[] = {
	XmTOP_LEFT, XmBOTTOM_LEFT, XmTOP_RIGHT,
	XmBOTTOM_RIGHT
};

#ifndef	RUNTIME
int UxValuesOfScrollBarPlacement(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uScrollBarPlacement;
	*n = XtNumber(uScrollBarPlacement);
	return *n;
}

int UxValidateScrollBarPlacement(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uScrollBarPlacement);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uScrollBarPlacement[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uScrollingPolicy[] = {
	"automatic", "application_defined"
};
static	unsigned char xScrollingPolicy[] = {
	XmAUTOMATIC, XmAPPLICATION_DEFINED
};

#ifndef	RUNTIME
int UxValuesOfScrollingPolicy(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uScrollingPolicy;
	*n = XtNumber(uScrollingPolicy);
	return *n;
}

int UxValidateScrollingPolicy(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uScrollingPolicy);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uScrollingPolicy[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uSelectionPolicy[] = {
	"single_select", "multiple_select", 
	"extended_select", "browse_select"
};
static	unsigned char xSelectionPolicy[] = {
	XmSINGLE_SELECT, XmMULTIPLE_SELECT,
	XmEXTENDED_SELECT, XmBROWSE_SELECT
};

#ifndef	RUNTIME
int UxValuesOfSelectionPolicy(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uSelectionPolicy;
	*n = XtNumber(uSelectionPolicy);
	return *n;
}

int UxValidateSelectionPolicy(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uSelectionPolicy);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uSelectionPolicy[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uSeparatorType[] = {
	"single_line", "double_line", 
	"single_dashed_line", "double_dashed_line", 
	"no_line", "shadow_etched_in", 
	"shadow_etched_out"
};
static	unsigned char xSeparatorType[] = {
	XmSINGLE_LINE, XmDOUBLE_LINE,
	XmSINGLE_DASHED_LINE, XmDOUBLE_DASHED_LINE,
	XmNO_LINE, XmSHADOW_ETCHED_IN,
	XmSHADOW_ETCHED_OUT
};

#ifndef	RUNTIME
int UxValuesOfSeparatorType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uSeparatorType;
	*n = XtNumber(uSeparatorType);
	return *n;
}

int UxValidateSeparatorType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uSeparatorType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uSeparatorType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uShadowType[] = {
	"shadow_in", "shadow_out", 
	"shadow_etched_in", "shadow_etched_out"
};
static	unsigned char xShadowType[] = {
	XmSHADOW_IN, XmSHADOW_OUT,
	XmSHADOW_ETCHED_IN, XmSHADOW_ETCHED_OUT
};

#ifndef	RUNTIME
int UxValuesOfShadowType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uShadowType;
	*n = XtNumber(uShadowType);
	return *n;
}

int UxValidateShadowType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uShadowType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uShadowType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uStringDirection[] = {
	"string_direction_l_to_r", "string_direction_r_to_l"
};
static	unsigned char xStringDirection[] = {
	XmSTRING_DIRECTION_L_TO_R, XmSTRING_DIRECTION_R_TO_L
};

#ifndef	RUNTIME
int UxValuesOfStringDirection(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uStringDirection;
	*n = XtNumber(uStringDirection);
	return *n;
}

int UxValidateStringDirection(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uStringDirection);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uStringDirection[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/


static  char *uTearOffModel[] = {
        "tear_off_enabled", "tear_off_disabled"
};
static  unsigned char xTearOffModel[] = {
        XmTEAR_OFF_ENABLED, XmTEAR_OFF_DISABLED
};

#ifndef RUNTIME
int UxValuesOfTearOffModel(ulist, n)
char ***ulist;
int *n;
{
        *ulist = uTearOffModel;
        *n = XtNumber(uTearOffModel);
        return *n;
}

int UxValidateTearOffModel(rs, s)
swidget rs;
char *s;
{
        int i, n=XtNumber(uTearOffModel);

        if (s == NULL)
                return ERROR;
        for (i=0; i<n; i++)
                if (UxStrEqual(s, uTearOffModel[i]))
                        return NO_ERROR;
        return ERROR;
}
#endif /* ! RUNTIME */


/* ------------------------------------------------------------------------*/

static	char *uUnitType[] = {
	"pixels", "100th_millimeters", 
	"1000th_inches", "100th_points", 
	"100th_font_units"
};
static	unsigned char xUnitType[] = {
	XmPIXELS, Xm100TH_MILLIMETERS,
	Xm1000TH_INCHES, Xm100TH_POINTS,
	Xm100TH_FONT_UNITS
};

#ifndef	RUNTIME
int UxValuesOfUnitType(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uUnitType;
	*n = XtNumber(uUnitType);
	return *n;
}

int UxValidateUnitType(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uUnitType);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uUnitType[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uVisualPolicy[] = {
	"constant", "variable"
};
static	unsigned char xVisualPolicy[] = {
	XmCONSTANT, XmVARIABLE
};

#ifndef	RUNTIME
int UxValuesOfVisualPolicy(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uVisualPolicy;
	*n = XtNumber(uVisualPolicy);
	return *n;
}

int UxValidateVisualPolicy(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uVisualPolicy);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uVisualPolicy[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */

/* ------------------------------------------------------------------------*/

static	char *uWinGravity[] = {
	"forget_gravity",	"north_west_gravity", 	"north_gravity",
	"north_east_gravity", 	"west_gravity", 	"center_gravity",
	"east_gravity", 	"south_west_gravity", 	"south_gravity",
	"south_east_gravity", 	"static_gravity"

};
static	int xWinGravity[] = {
	ForgetGravity, 		NorthWestGravity,       NorthGravity,
	NorthEastGravity,       WestGravity,            CenterGravity,
	EastGravity,            SouthWestGravity,       SouthGravity,
	SouthEastGravity,       StaticGravity           

};

#ifndef	RUNTIME
int UxValuesOfWinGravity(ulist, n)
char ***ulist;
int *n;
{
	*ulist = uWinGravity;
	*n = XtNumber(uWinGravity);
	return *n;
}

int UxValidateWinGravity(rs, s)
swidget rs;
char *s;
{
	int i, n=XtNumber(uWinGravity);

	if (s == NULL)
		return ERROR;
	for (i=0; i<n; i++)
		if (UxStrEqual(s, uWinGravity[i]))
			return NO_ERROR;
	return ERROR;
}
#endif /* ! RUNTIME */


/******************************************************************************
NAME:		UxStringToCharEnum (sw, udata, xdata, flag, XT_type)
INPUT:		swidget		sw		- swidget
		char		**udata		- UIMX string representation
		char		*xdata		- Motif enumerated value
		int		flag		- TO_UIMX or TO_X
		int		XT_type		- the type identifier
RETURN:		int				- ERROR/NO_ERROR
DESCRIPTION:	Performs the conversion DIRECTLY using the 'conversion' records
		defined above. This function is for Widget properties that do
		not have resource converters.
CREATION:	Jul 4/1989
REVISIONS:	--
-----------------------------------------------------------------------------*/

int	UxStringToCharEnum (sw, udata, xdata, flag, XT_type)
	swidget 	sw;
	char		**udata;
	unsigned char	*xdata;
	int		flag;
	int		XT_type;
{
	int		i, match, err = 0;
	char 		**uconv = UxStr_conv[XT_type].strings;
	unsigned char	*xconv = UxStr_conv[XT_type].values;
	int		n = UxStr_conv[XT_type].size;

	if (flag == TO_UIMX)
	{
	    for (i=0, match=0; i<n; i++)
		if (*xdata == xconv[i])
		{
		    match = 1;
		    break;
		}
	    if (match)
		*udata = uconv[i];
	    else
		err = 1;
	}
	else if (flag == TO_X)
	{
	    for (i=0, match=0; i<n; i++)
	    {
		if (UxStrEqual(*udata, uconv[i]))
		{
		    match = 1;
		    break;
		}
	    }
	    if (match)
		*xdata = xconv[i];
	    else
		err = 1;
	}
	else
	{
	    UxStandardError(CGETS( MS_MISC_WRONG_FLAG, DS_MS_MISC_WRONG_FLAG));
	    return ERROR;
	}
	if (!err)
	    return NO_ERROR;
	else
	{
	    UxStandardError(CGETS( MS_MISC_NO_MATCH, DS_MS_MISC_NO_MATCH));
	    return ERROR;
	}
}

/******************************************************************************
NAME:		int		string_dialogStyle
INPUT:		swidget		sw		- swidget
		char		**udata		- UIMX string representation
		char		*xdata		- Motif enumerated value
		int		flag		- TO_UIMX or TO_X
		int		XT_type		- the type identifier
RETURN:		ERROR/NO_ERROR
DESCRIPTION:	This routine converts between strings and dialogStyle values.
		If UIMX is in design mode the conversion to X always uses
		"dialog_modeless", Otherwise the normal conversion occurs.
LAST REV:	Apr 93	fix 4119 pass true values to UIM/X widgets.
-----------------------------------------------------------------------------*/

static int string_dialogStyle(sw, udata, xdata, flag, XT_type)
	swidget 	sw;
	char		**udata;
	unsigned char	*xdata;
	int		flag;
	int		XT_type;
{
#ifndef RUNTIME

	/*-------------------------------------------------------------
	 * USER widgets are NEVER modal in Design Mode, only Test Mode.
	 *-------------------------------------------------------------*/
	if ((flag == TO_X) 
	    && UxDesignModeP() 
	    && (sw == NULL || UxGetClassification(sw) != UIMX_WIDGET))
	{	
		char		*newdata= "dialog_modeless";

		if(UxStrEqual(*udata, "dialog_work_area"))
			return(UxStringToCharEnum(sw, udata, xdata,
				flag, XT_type));

		return(UxStringToCharEnum(sw, &newdata, xdata,
			flag, XT_type));
	}

	/*-----------------------------------------------------
	 * UIM/X widgets get real values, not the safe subset.
	 *-----------------------------------------------------*/
	if (flag == TO_X &&
	    sw &&
	    UxGetClassification(sw) == UIMX_WIDGET)
	{
	    int i; 
	    int n = XtNumber(uDialogStyle);

	    for (i=0; i<n; i++) {
		if (UxStrEqual(*udata, uDialogStyle[i])) {
			*xdata = xTrueDialogStyle[i];
			return NO_ERROR;
		}
	    }
	    *xdata =  XmDIALOG_MODELESS;
	    return ERROR;
	}

#endif /* RUNTIME */
	/*----------------------------------------------------------
	 * Correct name for dialog_modeless depends on the widget.
	 *----------------------------------------------------------*/
	if (flag == TO_UIMX) 
	{
		int st = UxStringToCharEnum(sw, udata, xdata, flag, XT_type);

		if (st == NO_ERROR && UxStrEqual(*udata, "dialog_modeless")) 
		{
			Widget	w, wp;

			w = UxGetWidget(sw);
			if (   w == NULL 
			    || (wp = XtParent(w)) == NULL
			    || ! XmIsDialogShell(wp))
			{
				*udata = "dialog_work_area";
			}
			return NO_ERROR;
		}
		return st;
	}

	return(UxStringToCharEnum(sw, udata, xdata, flag, XT_type));
}

/******************************************************************************
NAME:		int		string_inputMode(sw, udata, xdata, flag,
								XT_type)
INPUT:		swidget		sw		- swidget
		char		**udata		- UIMX string representation
		int		*xdata		- Motif enumerated value
		int		flag		- TO_UIMX or TO_X
		int		XT_type		- the type identifier
RETURN:		ERROR/NO_ERROR
DESCRIPTION:	Converts between strings and inputMode values.

In design time, the values for user widgets are mapped to the
harmless subset of values, so that USER widgets don't interfere with
interactions.    For UIM/X widgets, we override that.

LAST REV:	fix4119		Created.
-----------------------------------------------------------------------------*/

static int string_mwmInputMode(sw, udata, xdata, flag, XT_type)
	swidget 	sw;
	char		**udata;
	int 		*xdata;
	int		flag;
	int		XT_type;
{
#ifndef RUNTIME
	/*--------------------------------------------------------
	 * UIM/X widgets get the true values, not the safe subset.
	 *--------------------------------------------------------*/
	if (flag == TO_X &&
	    sw &&
	    UxGetClassification(sw) == UIMX_WIDGET)
	{
	    int i; 
	    int n = XtNumber(uMwmInputMode);

	    for (i=0; i < n; i++) 
	    {
		if (UxStrEqual(*udata, uMwmInputMode[i])) {
			*xdata = xTrueMwmInputMode[i];
			return NO_ERROR;
		}
	    }
	    *xdata =  MWM_INPUT_MODELESS;
	    return ERROR;
	}

#endif /* RUNTIME */

	return UxStringToIntEnum(sw, udata, xdata, flag, XT_type);
}

#ifdef RUNTIME
/******************************************************************************
NAME:		int	string_UIMXconversion (sw, udata, xdata, flag, XT_type)
INPUT:		swidget		sw		- swidget
		char		**udata		- UIMX string representation
		int		*xdata		- Motif enumerated value
		int		flag		- TO_UIMX or TO_X
		int		XT_type		- the type identifier
RETURN:		ERROR/NO_ERROR
DESCRIPTION:	Performs the conversion DIRECTLY using the 'conversion' records
		defined above. This function is for Widget properties that do
		not have resource converters.
CREATION:	Jul 4/1989
REVISIONS:	--
-----------------------------------------------------------------------------*/

static	int	string_UIMXconversion (sw, udata, xdata, flag, XT_type)
	swidget sw;
	char	**udata;
	int	*xdata;
	int	flag;
	int	XT_type;
{
	int		i, match, err = 0;
	char		**uconv = UxStr_conv[XT_type].strings;
	int 		*xconv = (int *) UxStr_conv[XT_type].values;
	int		n = UxStr_conv[XT_type].size;
	int		real_xdata;

	if (flag == TO_UIMX)
	{

	    real_xdata = * xdata;

	    for (i = 0, match = 0; i<n; i++)
		if (real_xdata == (int) xconv[i])
		{
		    match = 1;
		    break;
		}
	    if (match)
		*udata = uconv[i];
	    else
		err = 1;
	}
	else if (flag == TO_X)
	{
	    for (i=0, match=0; i<n; i++)
		if (UxStrEqual (*udata, uconv[i]))
		{
		    match = 1;
		    break;
		}
	    if (match)
		*xdata = (int)xconv[i];
	    else
		err = 1;
	}
	else
	{
	    UxStandardError (CGETS( MS_MISC_WRONG_FLAG, DS_MS_MISC_WRONG_FLAG));
	    return ERROR;
	}
	if (! err)
	    return NO_ERROR;
	else
	{
	    UxStandardError (CGETS( MS_MISC_NO_MATCH, DS_MS_MISC_NO_MATCH));
	    return ERROR;
	}
}
#endif /* RUNTIME */

/******************************************************************************
NAME:		UxStringToIntEnum (sw, udata, xdata, flag, XT_type)
INPUT:		swidget		sw		- swidget
		char		**udata		- UIMX string representation
		char		*xdata		- Motif enumerated value
		int		flag		- TO_UIMX or TO_X
		int		XT_type		- the type identifier
RETURN:		int				- ERROR/NO_ERROR
DESCRIPTION:	Performs the conversion DIRECTLY using the 'conversion' records
		defined above.
CREATION:	Jul 4/1989
REVISIONS:	--
-----------------------------------------------------------------------------*/

int	UxStringToIntEnum (sw, udata, xdata, flag, XT_type)
	swidget 	sw;
	char		**udata;
	int		*xdata;
	int		flag;
	int		XT_type;
{

#ifdef RUNTIME
	return string_UIMXconversion(sw, udata, xdata, flag, XT_type);
#else
	if (flag == TO_UIMX)
	{
	    for (i=0, match=0; i<n; i++)
		if (*xdata == (int) xconv[i])
		{
		    match = 1;
		    break;
		}
	    if (match)
		*udata = uconv[i];
	    else
		err = 1;
	}
	else if (flag == TO_X)
	{
	    for (i=0, match=0; i<n; i++)
	    {
		if (UxStrEqual(*udata, uconv[i]))
		{
		    match = 1;
		    break;
		}
	    }
	    if (match)
		*xdata = (int) xconv[i];
	    else
		err = 1;
	}
	else
	{
	    UxStandardError(CGETS( MS_MISC_WRONG_FLAG, DS_MS_MISC_WRONG_FLAG));
	    return ERROR;
	}
	if (!err)
	    return NO_ERROR;
	else
	{
	    UxStandardError(CGETS( MS_MISC_NO_MATCH, DS_MS_MISC_NO_MATCH));
	    return ERROR;
	}
#endif /* RUNTIME */
}

/***************************************************************************
NAME:		add_enum_type( name, xt_size, xt_vals, uimx_vals, num_vals,
				convertor, xt_variable )
INPUT:		char		*name		: name of resource type
		int		xt_size		: sizeof(Xt value)
		unsigned char	*xt_vals	: array of Xt values
		char		**uimx_vals	: array of uimx values
		int		num_vals	: number of array elements
		UIMX_conv	convertor	: convertor function to be used
OUTPUT:		int		*xt_variable	: the UxXT_ variable to be set
RETURN:		void
DESCRIPTION:	Installs the arrays of possible values and the convertor for an
		Xt resource type that takes on one of a small set of values.
		Note that most resources are of Xt type 'unsigned char'
		and use the convertor 'UxStringToCharEnum()',
		but there are a few exceptions.
EXT REFERENCES: Relies on the UxUT_string variable already being initialized.
EXT EFFECTS:	---
CREATION:	Jan 16/90
REVISIONS:	21 Mars 1991	bug 2118
		-- Changed name of function to be compliant to VE style
		   for user delivery. UxAddXtype and UxAddConv

---------------------------------------------------------------------------*/
static	void	add_enum_type( name, xt_size, xt_vals, uimx_vals, num_vals,
				convertor, xt_variable )
	char		*name;
	int		xt_size;
	unsigned char	*xt_vals;
	char		**uimx_vals;
	int		num_vals;
	UIMX_conv	convertor;
	int		*xt_variable;
{
	*xt_variable = UxAddXtype( name, xt_size );
	UxAddXValues( *xt_variable, uimx_vals, xt_vals, num_vals );
	UxAddConv( UxUT_string, *xt_variable, convertor );
}

/***************************************************************************
NAME:		UxAddEnumType( name, xt_size, xt_vals, uimx_vals, xdef_vals,
							num_vals, convertor )
INPUT:		char		*name		: name of resource type
		int		xt_size		: sizeof(Xt value)
		unsigned char	*xt_vals	: array of Xt values
		char		**uimx_vals	: array of uimx values
		char		**xdef_vals	: array of Xt defined constants
		int		num_vals	: number of array elements
		UIMX_conv	convertor	: convertor function to be used
OUTPUT:		---
RETURN:		int				: the id of the new xtype
DESCRIPTION:	Installs the arrays of possible values and the convertor for an
		Xt resource type that takes on one of a small set of values.
		The return value is the id of the new xtype and should be stored
		in a UxXT_ global variable.
		Note that most resources are of Xt type 'unsigned char'
		and use the convertor 'UxStringToCharEnum()',
		but there are a few exceptions.

		This function is presently not used in the standard UIM/X
		- it is provided for use by the users who are extending UIM/X.
		Note that the parameter 'xdef_vals' is not used in this
		version of the function but there is another version of this
		function in cgen/enum.c where 'xdef_vals' is used and so we
		retain it here so that both versions have the same parameters.

EXT REFERENCES: Relies on the UxUT_string variable already being initialized.
EXT EFFECTS:	---
CREATION:	Jan 16/90	Visual Edge Software

---------------------------------------------------------------------------*/
int	UxAddEnumType( name, xt_size, xt_vals, uimx_vals, xdef_vals,
							num_vals, convertor )
	char		*name;
	int		xt_size;
	unsigned char	*xt_vals;
	char		**uimx_vals;
	char		**xdef_vals;
	int		num_vals;
	UIMX_conv	convertor;
{
	int		xtype;

	add_enum_type( name, xt_size, xt_vals, uimx_vals, num_vals, convertor,
			&xtype );

	return ( xtype );
}

/***************************************************************************
NAME:		UxInitEnumTypes()
INPUT:		---
OUTPUT:		---
RETURN:		void
DESCRIPTION:	Adds the Xt types and registers the convertors and arrays of
		values for the enumerated-type resources.
EXT REFERENCES: Relies on the UxUT_string variable already being initialized.
EXT EFFECTS:	Initializes the UxXT_ variables for the enumerated types.
CREATION:	Jan 11/91

---------------------------------------------------------------------------*/
void	UxInitEnumTypes()
{
	/* in alphabetical order */

	add_enum_type( "Alignment",  sizeof(unsigned char),
		xAlignment, uAlignment, XtNumber(uAlignment),
		UxStringToCharEnum, &UxXT_Alignment );

        add_enum_type( "ChildPlacement",  sizeof(unsigned char),
                xChildPlacement, uChildPlacement,
                XtNumber(uChildPlacement),
                UxStringToCharEnum, &UxXT_ChildPlacement );

	add_enum_type( "ChildVerticalAlignment",  sizeof(unsigned char), 
		xChildVerticalAlignment, uChildVerticalAlignment,
 		XtNumber(uChildVerticalAlignment),
		UxStringToCharEnum, &UxXT_ChildVerticalAlignment);
        add_enum_type( "EntryVerticalAlignment",  sizeof(unsigned
 char),
                xEntryVerticalAlignment, uEntryVerticalAlignment,
                XtNumber(uEntryVerticalAlignment),
                UxStringToCharEnum, &UxXT_EntryVerticalAlignment);


	add_enum_type( "ArrowDirection",  sizeof(unsigned char),
		xArrowDirection, uArrowDirection, XtNumber(uArrowDirection),
		UxStringToCharEnum, &UxXT_ArrowDirection );

	add_enum_type( "AttachmentType",  sizeof(unsigned char),
		xAttachmentType, uAttachmentType, XtNumber(uAttachmentType),
		UxStringToCharEnum, &UxXT_AttachmentType );

	add_enum_type( "AudibleWarning",  sizeof(unsigned char),
		xAudibleWarning, uAudibleWarning, XtNumber(uAudibleWarning),
		UxStringToCharEnum, &UxXT_AudibleWarning );

	add_enum_type( "Bool",  sizeof(Bool),
		xBool, uBoolean, XtNumber(uBoolean),
		UxStringToIntEnum, &UxXT_Bool );

	add_enum_type( "Boolean",  sizeof(Boolean),
		xBoolean, uBoolean, XtNumber(uBoolean),
		UxStringToCharEnum, &UxXT_Boolean );

	add_enum_type( "ChildType",  sizeof(unsigned char),
		xChildType, uChildType,
 		XtNumber(uChildType),
		UxStringToCharEnum, &UxXT_ChildType );

	add_enum_type( "CommandWindowLocation",  sizeof(unsigned char),
		xCommandWindowLocation, uCommandWindowLocation, 
		XtNumber(uCommandWindowLocation),
		UxStringToCharEnum, &UxXT_CommandWindowLocation );

	add_enum_type( "DefaultButtonType",  sizeof(unsigned char),
		xDefaultButtonType, uDefaultButtonType,
		XtNumber(uDefaultButtonType),
		UxStringToCharEnum, &UxXT_DefaultButtonType );

	add_enum_type( "DeleteResponse",  sizeof(unsigned char),
		xDeleteResponse, uDeleteResponse, XtNumber(uDeleteResponse),
		UxStringToCharEnum, &UxXT_DeleteResponse );

	add_enum_type( "DialogStyle",  sizeof(unsigned char),
		xDialogStyle, uDialogStyle, XtNumber(uDialogStyle),
		string_dialogStyle, &UxXT_DialogStyle );

	add_enum_type( "DialogType",  sizeof(unsigned char),
		xDialogType, uDialogType, XtNumber(uDialogType),
		UxStringToCharEnum, &UxXT_DialogType );

	add_enum_type( "EditMode",  sizeof(int),
		(unsigned char *)xEditMode, uEditMode, XtNumber(uEditMode),
		UxStringToIntEnum, &UxXT_EditMode );

	add_enum_type( "FileTypeMask",  sizeof(unsigned char),
		xFileTypeMask, uFileTypeMask, XtNumber(uFileTypeMask),
		UxStringToCharEnum, &UxXT_FileTypeMask );

	add_enum_type( "IndicatorType",  sizeof(unsigned char),
		xIndicatorType, uIndicatorType, XtNumber(uIndicatorType),
		UxStringToCharEnum, &UxXT_IndicatorType );

	add_enum_type( "InitialWindowState",  sizeof(int),
		(unsigned char *)xInitialWindowState, uInitialWindowState,
		XtNumber(uInitialWindowState),
		UxStringToIntEnum, &UxXT_InitialWindowState );

	add_enum_type( "KeyboardFocusPolicy",  sizeof(unsigned char),
		xKeyboardFocusPolicy, uKeyboardFocusPolicy,
		XtNumber(uKeyboardFocusPolicy),
		UxStringToCharEnum, &UxXT_KeyboardFocusPolicy );

	add_enum_type( "LabelType",  sizeof(unsigned char),
		xLabelType, uLabelType, XtNumber(uLabelType),
		UxStringToCharEnum, &UxXT_LabelType );

	add_enum_type( "ListSizePolicy",  sizeof(unsigned char),
		xListSizePolicy, uListSizePolicy, XtNumber(uListSizePolicy),
		UxStringToCharEnum, &UxXT_ListSizePolicy );

	add_enum_type( "MsgDialogType",  sizeof(unsigned char),
		xMsgDialogType, uMsgDialogType, XtNumber(uMsgDialogType),
		UxStringToCharEnum, &UxXT_MsgDialogType );

	add_enum_type( "MwmInputMode",  sizeof(int),
		xMwmInputMode, uMwmInputMode, XtNumber(uMwmInputMode),
		string_mwmInputMode, &UxXT_MwmInputMode );

	add_enum_type( "MultiClick",  sizeof(unsigned char),
		xMultiClick, uMultiClick, XtNumber(uMultiClick),
		UxStringToCharEnum, &UxXT_MultiClick );

	add_enum_type( "NavigationType",  sizeof(unsigned char),
		xNavigationType, uNavigationType, XtNumber(uNavigationType),
		UxStringToCharEnum, &UxXT_NavigationType );

	add_enum_type( "Orientation",  sizeof(unsigned char),
		xOrientation, uOrientation, XtNumber(uOrientation),
		UxStringToCharEnum, &UxXT_Orientation );

	add_enum_type( "Packing",  sizeof(unsigned char),
		xPacking, uPacking, XtNumber(uPacking),
		UxStringToCharEnum, &UxXT_Packing );

	add_enum_type( "ProcessingDirection",  sizeof(unsigned char),
		xProcessingDirection, uProcessingDirection,
		XtNumber(uProcessingDirection),
		UxStringToCharEnum, &UxXT_ProcessingDirection );

	add_enum_type( "ResizePolicy",  sizeof(unsigned char),
		xResizePolicy, uResizePolicy, XtNumber(uResizePolicy),
		UxStringToCharEnum, &UxXT_ResizePolicy );

	add_enum_type( "RowColumnType",  sizeof(unsigned char),
		xRowColumnType, uRowColumnType, XtNumber(uRowColumnType),
		UxStringToCharEnum, &UxXT_RowColumnType );

	add_enum_type( "ScrollBarDisplayPolicy",  sizeof(unsigned char),
		xScrollBarDisplayPolicy, uScrollBarDisplayPolicy,
		XtNumber(uScrollBarDisplayPolicy),
		UxStringToCharEnum, &UxXT_ScrollBarDisplayPolicy );

	add_enum_type( "ScrollBarPlacement",  sizeof(unsigned char),
		xScrollBarPlacement, uScrollBarPlacement,
		XtNumber(uScrollBarPlacement),
		UxStringToCharEnum, &UxXT_ScrollBarPlacement );

	add_enum_type( "ScrollingPolicy",  sizeof(unsigned char),
		xScrollingPolicy, uScrollingPolicy, XtNumber(uScrollingPolicy),
		UxStringToCharEnum, &UxXT_ScrollingPolicy );

	add_enum_type( "SelectionPolicy",  sizeof(unsigned char),
		xSelectionPolicy, uSelectionPolicy, XtNumber(uSelectionPolicy),
		UxStringToCharEnum, &UxXT_SelectionPolicy );

	add_enum_type( "SeparatorType",  sizeof(unsigned char),
		xSeparatorType, uSeparatorType, XtNumber(uSeparatorType),
		UxStringToCharEnum, &UxXT_SeparatorType );

	add_enum_type( "ShadowType",  sizeof(unsigned char),
		xShadowType, uShadowType, XtNumber(uShadowType),
		UxStringToCharEnum, &UxXT_ShadowType );

	add_enum_type( "StringDirection",  sizeof(unsigned char),
		xStringDirection, uStringDirection, XtNumber(uStringDirection),
		UxStringToCharEnum, &UxXT_StringDirection );

	add_enum_type( "TearOffModel",  sizeof(unsigned char),
                xTearOffModel, uTearOffModel, XtNumber(uTearOffModel),
                UxStringToCharEnum, &UxXT_TearOffModel );

	add_enum_type( "UnitType",  sizeof(unsigned char),
		xUnitType, uUnitType, XtNumber(uUnitType),
		UxStringToCharEnum, &UxXT_UnitType );

	add_enum_type( "VisualPolicy",  sizeof(unsigned char),
		xVisualPolicy, uVisualPolicy, XtNumber(uVisualPolicy),
		UxStringToCharEnum, &UxXT_VisualPolicy );

	add_enum_type( "WinGravity",  sizeof(int),
		xWinGravity, uWinGravity, XtNumber(uWinGravity),
		UxStringToIntEnum, &UxXT_WinGravity );

	/* Now add any enumerated-resource types that the user has defined */
	UxAddUserDefEnumTypes();
}

