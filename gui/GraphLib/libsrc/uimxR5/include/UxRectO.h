/* @(#)UxRectO.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXRectObject_INCLUDED
#define UXRectObject_INCLUDED

#include "Xm/Xm.h"

#ifdef DESIGN_TIME
#include "UxSwidget.h"

#ifndef UxGetY
extern binptr UxP_RectObjectRD_y;
#define UxGetY(sw) \
        UxGET_int(sw,UxP_RectObjectRD_y,"y")
#define UxPutY(sw,val) \
        UxPUT_int(sw,UxP_RectObjectRD_y,"y",val)
#endif

#ifndef UxGetX
extern binptr UxP_RectObjectRD_x;
#define UxGetX(sw) \
        UxGET_int(sw,UxP_RectObjectRD_x,"x")
#define UxPutX(sw,val) \
        UxPUT_int(sw,UxP_RectObjectRD_x,"x",val)
#endif

#ifndef UxGetWidth
extern binptr UxP_RectObjectRD_width;
#define UxGetWidth(sw) \
        UxGET_int(sw,UxP_RectObjectRD_width,"width")
#define UxPutWidth(sw,val) \
        UxPUT_int(sw,UxP_RectObjectRD_width,"width",val)
#endif

#ifndef UxGetSensitive
extern binptr UxP_RectObjectRD_sensitive;
#define UxGetSensitive(sw) \
        UxGET_string(sw,UxP_RectObjectRD_sensitive,"sensitive")
#define UxPutSensitive(sw,val) \
        UxPUT_string(sw,UxP_RectObjectRD_sensitive,"sensitive",val)
#endif

#ifndef UxGetHeight
extern binptr UxP_RectObjectRD_height;
#define UxGetHeight(sw) \
        UxGET_int(sw,UxP_RectObjectRD_height,"height")
#define UxPutHeight(sw,val) \
        UxPUT_int(sw,UxP_RectObjectRD_height,"height",val)
#endif

#ifndef UxGetBorderWidth
extern binptr UxP_RectObjectRD_borderWidth;
#define UxGetBorderWidth(sw) \
        UxGET_int(sw,UxP_RectObjectRD_borderWidth,"borderWidth")
#define UxPutBorderWidth(sw,val) \
        UxPUT_int(sw,UxP_RectObjectRD_borderWidth,"borderWidth",val)
#endif

#ifndef UxGetAncestorSensitive
extern binptr UxP_RectObjectRD_ancestorSensitive;
#define UxGetAncestorSensitive(sw) \
        UxGET_string(sw,UxP_RectObjectRD_ancestorSensitive,"ancestorSensitive")
#define UxPutAncestorSensitive(sw,val) \
        UxPUT_string(sw,UxP_RectObjectRD_ancestorSensitive,"ancestorSensitive",val)
#endif

extern Class_t UxC_RectObject;
#define UxCreateRectObject(name,parent) \
        UxCreateSwidget(UxC_RectObject,name,parent)
#else
#define UxCreateRectObject(name,parent) \
        UxCreateSwidget(name,rectObjClass,parent)
#endif
#endif
