/* @(#)UxScale.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXScale_INCLUDED
#define UXScale_INCLUDED

#include "Xm/Scale.h"

#ifdef DESIGN_TIME
#include "UxMgr.h"

#ifndef UxGetValue
extern binptr UxP_ScaleRD_value;
#define UxGetValue(sw) \
        UxGET_int(sw,UxP_ScaleRD_value,"value")
#define UxPutValue(sw,val) \
        UxPUT_int(sw,UxP_ScaleRD_value,"value",val)
#endif

#ifndef UxGetTitleString
extern binptr UxP_ScaleRD_titleString;
#define UxGetTitleString(sw) \
        UxGET_string(sw,UxP_ScaleRD_titleString,"titleString")
#define UxPutTitleString(sw,val) \
        UxPUT_string(sw,UxP_ScaleRD_titleString,"titleString",val)
#endif

#ifndef UxGetShowValue
extern binptr UxP_ScaleRD_showValue;
#define UxGetShowValue(sw) \
        UxGET_string(sw,UxP_ScaleRD_showValue,"showValue")
#define UxPutShowValue(sw,val) \
        UxPUT_string(sw,UxP_ScaleRD_showValue,"showValue",val)
#endif

#ifndef UxGetScaleWidth
extern binptr UxP_ScaleRD_scaleWidth;
#define UxGetScaleWidth(sw) \
        UxGET_int(sw,UxP_ScaleRD_scaleWidth,"scaleWidth")
#define UxPutScaleWidth(sw,val) \
        UxPUT_int(sw,UxP_ScaleRD_scaleWidth,"scaleWidth",val)
#endif

#ifndef UxGetScaleMultiple
extern binptr UxP_ScaleRD_scaleMultiple;
#define UxGetScaleMultiple(sw) \
        UxGET_int(sw,UxP_ScaleRD_scaleMultiple,"scaleMultiple")
#define UxPutScaleMultiple(sw,val) \
        UxPUT_int(sw,UxP_ScaleRD_scaleMultiple,"scaleMultiple",val)
#endif

#ifndef UxGetScaleHeight
extern binptr UxP_ScaleRD_scaleHeight;
#define UxGetScaleHeight(sw) \
        UxGET_int(sw,UxP_ScaleRD_scaleHeight,"scaleHeight")
#define UxPutScaleHeight(sw,val) \
        UxPUT_int(sw,UxP_ScaleRD_scaleHeight,"scaleHeight",val)
#endif

#ifndef UxGetProcessingDirection
extern binptr UxP_ScaleRD_processingDirection;
#define UxGetProcessingDirection(sw) \
        UxGET_string(sw,UxP_ScaleRD_processingDirection,"processingDirection")
#define UxPutProcessingDirection(sw,val) \
        UxPUT_string(sw,UxP_ScaleRD_processingDirection,"processingDirection",val)
#endif

#ifndef UxGetOrientation
extern binptr UxP_ScaleRD_orientation;
#define UxGetOrientation(sw) \
        UxGET_string(sw,UxP_ScaleRD_orientation,"orientation")
#define UxPutOrientation(sw,val) \
        UxPUT_string(sw,UxP_ScaleRD_orientation,"orientation",val)
#endif

#ifndef UxGetMinimum
extern binptr UxP_ScaleRD_minimum;
#define UxGetMinimum(sw) \
        UxGET_int(sw,UxP_ScaleRD_minimum,"minimum")
#define UxPutMinimum(sw,val) \
        UxPUT_int(sw,UxP_ScaleRD_minimum,"minimum",val)
#endif

#ifndef UxGetMaximum
extern binptr UxP_ScaleRD_maximum;
#define UxGetMaximum(sw) \
        UxGET_int(sw,UxP_ScaleRD_maximum,"maximum")
#define UxPutMaximum(sw,val) \
        UxPUT_int(sw,UxP_ScaleRD_maximum,"maximum",val)
#endif

#ifndef UxGetHighlightThickness
extern binptr UxP_ScaleRD_highlightThickness;
#define UxGetHighlightThickness(sw) \
        UxGET_int(sw,UxP_ScaleRD_highlightThickness,"highlightThickness")
#define UxPutHighlightThickness(sw,val) \
        UxPUT_int(sw,UxP_ScaleRD_highlightThickness,"highlightThickness",val)
#endif

#ifndef UxGetHighlightOnEnter
extern binptr UxP_ScaleRD_highlightOnEnter;
#define UxGetHighlightOnEnter(sw) \
        UxGET_string(sw,UxP_ScaleRD_highlightOnEnter,"highlightOnEnter")
#define UxPutHighlightOnEnter(sw,val) \
        UxPUT_string(sw,UxP_ScaleRD_highlightOnEnter,"highlightOnEnter",val)
#endif

#ifndef UxGetFontList
extern binptr UxP_ScaleRD_fontList;
#define UxGetFontList(sw) \
        UxGET_string(sw,UxP_ScaleRD_fontList,"fontList")
#define UxPutFontList(sw,val) \
        UxPUT_string(sw,UxP_ScaleRD_fontList,"fontList",val)
#endif

#ifndef UxGetDecimalPoints
extern binptr UxP_ScaleRD_decimalPoints;
#define UxGetDecimalPoints(sw) \
        UxGET_short(sw,UxP_ScaleRD_decimalPoints,"decimalPoints")
#define UxPutDecimalPoints(sw,val) \
        UxPUT_short(sw,UxP_ScaleRD_decimalPoints,"decimalPoints",val)
#endif

extern Class_t UxC_scale;
#define UxCreateScale(name,parent) \
        UxCreateSwidget(UxC_scale,name,parent)
#else
#define UxCreateScale(name,parent) \
        UxCreateSwidget(name,xmScaleWidgetClass,parent)
#endif
#endif
