/* @(#)UxScText.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXScrolledText_INCLUDED
#define UXScrolledText_INCLUDED

#include "Xm/Text.h"

#ifdef DESIGN_TIME
#include "UxText.h"

#ifndef UxGetScrollVertical
extern binptr UxP_ScrolledTextRD_scrollVertical;
#define UxGetScrollVertical(sw) \
        UxGET_string(sw,UxP_ScrolledTextRD_scrollVertical,"scrollVertical")
#define UxPutScrollVertical(sw,val) \
        UxPUT_string(sw,UxP_ScrolledTextRD_scrollVertical,"scrollVertical",val)
#endif

#ifndef UxGetScrollTopSide
extern binptr UxP_ScrolledTextRD_scrollTopSide;
#define UxGetScrollTopSide(sw) \
        UxGET_string(sw,UxP_ScrolledTextRD_scrollTopSide,"scrollTopSide")
#define UxPutScrollTopSide(sw,val) \
        UxPUT_string(sw,UxP_ScrolledTextRD_scrollTopSide,"scrollTopSide",val)
#endif

#ifndef UxGetScrollLeftSide
extern binptr UxP_ScrolledTextRD_scrollLeftSide;
#define UxGetScrollLeftSide(sw) \
        UxGET_string(sw,UxP_ScrolledTextRD_scrollLeftSide,"scrollLeftSide")
#define UxPutScrollLeftSide(sw,val) \
        UxPUT_string(sw,UxP_ScrolledTextRD_scrollLeftSide,"scrollLeftSide",val)
#endif

#ifndef UxGetScrollHorizontal
extern binptr UxP_ScrolledTextRD_scrollHorizontal;
#define UxGetScrollHorizontal(sw) \
        UxGET_string(sw,UxP_ScrolledTextRD_scrollHorizontal,"scrollHorizontal")
#define UxPutScrollHorizontal(sw,val) \
        UxPUT_string(sw,UxP_ScrolledTextRD_scrollHorizontal,"scrollHorizontal",val)
#endif

extern Class_t UxC_scrolledText;
#define UxCreateScrolledText(name,parent) \
        UxCreateSwidget(UxC_scrolledText,name,parent)
#else
#define UxCreateScrolledText(name,parent) \
        UxCreateSwidget(name,xmTextWidgetClass,parent)
#endif
#endif
