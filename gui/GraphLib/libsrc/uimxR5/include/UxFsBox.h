/* @(#)UxFsBox.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXFileSelectionBox_INCLUDED
#define UXFileSelectionBox_INCLUDED

#include "Xm/FileSB.h"

#ifdef DESIGN_TIME
#include "UxSelBox.h"

#ifndef UxGetQualifySearchDataProc
extern binptr UxP_FileSelectionBoxRD_qualifySearchDataProc;
#define UxGetQualifySearchDataProc(sw) \
        UxGET_voidFunction(sw,UxP_FileSelectionBoxRD_qualifySearchDataProc,"qualifySearchDataProc")
#define UxPutQualifySearchDataProc(sw,val) \
        UxPUT_voidFunction(sw,UxP_FileSelectionBoxRD_qualifySearchDataProc,"qualifySearchDataProc",val)
#endif

#ifndef UxGetPattern
extern binptr UxP_FileSelectionBoxRD_pattern;
#define UxGetPattern(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_pattern,"pattern")
#define UxPutPattern(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_pattern,"pattern",val)
#endif

#ifndef UxGetNoMatchString
extern binptr UxP_FileSelectionBoxRD_noMatchString;
#define UxGetNoMatchString(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_noMatchString,"noMatchString")
#define UxPutNoMatchString(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_noMatchString,"noMatchString",val)
#endif

#ifndef UxGetListUpdated
extern binptr UxP_FileSelectionBoxRD_listUpdated;
#define UxGetListUpdated(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_listUpdated,"listUpdated")
#define UxPutListUpdated(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_listUpdated,"listUpdated",val)
#endif

#ifndef UxGetFilterLabelString
extern binptr UxP_FileSelectionBoxRD_filterLabelString;
#define UxGetFilterLabelString(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_filterLabelString,"filterLabelString")
#define UxPutFilterLabelString(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_filterLabelString,"filterLabelString",val)
#endif

#ifndef UxGetFileTypeMask
extern binptr UxP_FileSelectionBoxRD_fileTypeMask;
#define UxGetFileTypeMask(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_fileTypeMask,"fileTypeMask")
#define UxPutFileTypeMask(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_fileTypeMask,"fileTypeMask",val)
#endif

#ifndef UxGetFileSearchProc
extern binptr UxP_FileSelectionBoxRD_fileSearchProc;
#define UxGetFileSearchProc(sw) \
        UxGET_voidFunction(sw,UxP_FileSelectionBoxRD_fileSearchProc,"fileSearchProc")
#define UxPutFileSearchProc(sw,val) \
        UxPUT_voidFunction(sw,UxP_FileSelectionBoxRD_fileSearchProc,"fileSearchProc",val)
#endif

#ifndef UxGetFileListLabelString
extern binptr UxP_FileSelectionBoxRD_fileListLabelString;
#define UxGetFileListLabelString(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_fileListLabelString,"fileListLabelString")
#define UxPutFileListLabelString(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_fileListLabelString,"fileListLabelString",val)
#endif

#ifndef UxGetFileListItemCount
extern binptr UxP_FileSelectionBoxRD_fileListItemCount;
#define UxGetFileListItemCount(sw) \
        UxGET_int(sw,UxP_FileSelectionBoxRD_fileListItemCount,"fileListItemCount")
#define UxPutFileListItemCount(sw,val) \
        UxPUT_int(sw,UxP_FileSelectionBoxRD_fileListItemCount,"fileListItemCount",val)
#endif

#ifndef UxGetFileListItems
extern binptr UxP_FileSelectionBoxRD_fileListItems;
#define UxGetFileListItems(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_fileListItems,"fileListItems")
#define UxPutFileListItems(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_fileListItems,"fileListItems",val)
#endif

#ifndef UxGetDirSpec
extern binptr UxP_FileSelectionBoxRD_dirSpec;
#define UxGetDirSpec(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_dirSpec,"dirSpec")
#define UxPutDirSpec(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_dirSpec,"dirSpec",val)
#endif

#ifndef UxGetDirSearchProc
extern binptr UxP_FileSelectionBoxRD_dirSearchProc;
#define UxGetDirSearchProc(sw) \
        UxGET_voidFunction(sw,UxP_FileSelectionBoxRD_dirSearchProc,"dirSearchProc")
#define UxPutDirSearchProc(sw,val) \
        UxPUT_voidFunction(sw,UxP_FileSelectionBoxRD_dirSearchProc,"dirSearchProc",val)
#endif

#ifndef UxGetDirMask
extern binptr UxP_FileSelectionBoxRD_dirMask;
#define UxGetDirMask(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_dirMask,"dirMask")
#define UxPutDirMask(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_dirMask,"dirMask",val)
#endif

#ifndef UxGetDirListLabelString
extern binptr UxP_FileSelectionBoxRD_dirListLabelString;
#define UxGetDirListLabelString(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_dirListLabelString,"dirListLabelString")
#define UxPutDirListLabelString(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_dirListLabelString,"dirListLabelString",val)
#endif

#ifndef UxGetDirListItemCount
extern binptr UxP_FileSelectionBoxRD_dirListItemCount;
#define UxGetDirListItemCount(sw) \
        UxGET_int(sw,UxP_FileSelectionBoxRD_dirListItemCount,"dirListItemCount")
#define UxPutDirListItemCount(sw,val) \
        UxPUT_int(sw,UxP_FileSelectionBoxRD_dirListItemCount,"dirListItemCount",val)
#endif

#ifndef UxGetDirListItems
extern binptr UxP_FileSelectionBoxRD_dirListItems;
#define UxGetDirListItems(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_dirListItems,"dirListItems")
#define UxPutDirListItems(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_dirListItems,"dirListItems",val)
#endif

#ifndef UxGetDirectoryValid
extern binptr UxP_FileSelectionBoxRD_directoryValid;
#define UxGetDirectoryValid(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_directoryValid,"directoryValid")
#define UxPutDirectoryValid(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_directoryValid,"directoryValid",val)
#endif

#ifndef UxGetDirectory
extern binptr UxP_FileSelectionBoxRD_directory;
#define UxGetDirectory(sw) \
        UxGET_string(sw,UxP_FileSelectionBoxRD_directory,"directory")
#define UxPutDirectory(sw,val) \
        UxPUT_string(sw,UxP_FileSelectionBoxRD_directory,"directory",val)
#endif

extern Class_t UxC_fileSelectionBox;
#define UxCreateFileSelectionBox(name,parent) \
        UxCreateSwidget(UxC_fileSelectionBox,name,parent)
#else
#define UxCreateFileSelectionBox(name,parent) \
        UxCreateSwidget(name,xmFileSelectionBoxWidgetClass,parent)
#endif
#endif
