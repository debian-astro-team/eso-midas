/* @(#)UxQuestD.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXQuestionDialog_INCLUDED
#define UXQuestionDialog_INCLUDED

#include "Xm/MessageB.h"

#ifdef DESIGN_TIME
#include "UxMsgBox.h"

extern Class_t UxC_questionDialog;
#define UxCreateQuestionDialog(name,parent) \
        UxCreateSwidget(UxC_questionDialog,name,parent)
#else
#define UxCreateQuestionDialog(name,parent) \
        UxCreateDialogSwidget(name,xmMessageBoxWidgetClass,parent)
#endif
#endif
