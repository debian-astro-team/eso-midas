/* @(#)UxApplSh.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
#ifndef UXApplicationShell_INCLUDED
#define UXApplicationShell_INCLUDED

#include "X11/Shell.h"

#ifdef DESIGN_TIME
#include "UxTopSh.h"

#ifndef UxGetArgv
extern binptr UxP_ApplicationShellRD_argv;
#define UxGetArgv(sw) \
        UxGET_stringTable(sw,UxP_ApplicationShellRD_argv,"argv")
#define UxPutArgv(sw,val) \
        UxPUT_stringTable(sw,UxP_ApplicationShellRD_argv,"argv",val)
#endif

#ifndef UxGetArgc
extern binptr UxP_ApplicationShellRD_argc;
#define UxGetArgc(sw) \
        UxGET_int(sw,UxP_ApplicationShellRD_argc,"argc")
#define UxPutArgc(sw,val) \
        UxPUT_int(sw,UxP_ApplicationShellRD_argc,"argc",val)
#endif

extern Class_t UxC_applicationShell;
#define UxCreateApplicationShell(name,parent) \
        UxCreateSwidget(UxC_applicationShell,name,parent)
#else
#define UxCreateApplicationShell(name,parent) \
        UxCreateSwidget(name,applicationShellWidgetClass,parent)
#endif
#endif
