/* @(#)UxPushBG.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXPushButtonGadget_INCLUDED
#define UXPushButtonGadget_INCLUDED

#include "Xm/PushBG.h"

#ifdef DESIGN_TIME
#include "UxLabelG.h"

#ifndef UxGetShowAsDefault
extern binptr UxP_PushButtonGadgetRD_showAsDefault;
#define UxGetShowAsDefault(sw) \
        UxGET_int(sw,UxP_PushButtonGadgetRD_showAsDefault,"showAsDefault")
#define UxPutShowAsDefault(sw,val) \
        UxPUT_int(sw,UxP_PushButtonGadgetRD_showAsDefault,"showAsDefault",val)
#endif

#ifndef UxGetMultiClick
extern binptr UxP_PushButtonGadgetRD_multiClick;
#define UxGetMultiClick(sw) \
        UxGET_string(sw,UxP_PushButtonGadgetRD_multiClick,"multiClick")
#define UxPutMultiClick(sw,val) \
        UxPUT_string(sw,UxP_PushButtonGadgetRD_multiClick,"multiClick",val)
#endif

#ifndef UxGetFillOnArm
extern binptr UxP_PushButtonGadgetRD_fillOnArm;
#define UxGetFillOnArm(sw) \
        UxGET_string(sw,UxP_PushButtonGadgetRD_fillOnArm,"fillOnArm")
#define UxPutFillOnArm(sw,val) \
        UxPUT_string(sw,UxP_PushButtonGadgetRD_fillOnArm,"fillOnArm",val)
#endif

#ifndef UxGetDefaultButtonShadowThickness
extern binptr UxP_PushButtonGadgetRD_defaultButtonShadowThickness;
#define UxGetDefaultButtonShadowThickness(sw) \
        UxGET_int(sw,UxP_PushButtonGadgetRD_defaultButtonShadowThickness,"defaultButtonShadowThickness")
#define UxPutDefaultButtonShadowThickness(sw,val) \
        UxPUT_int(sw,UxP_PushButtonGadgetRD_defaultButtonShadowThickness,"defaultButtonShadowThickness",val)
#endif

#ifndef UxGetArmPixmap
extern binptr UxP_PushButtonGadgetRD_armPixmap;
#define UxGetArmPixmap(sw) \
        UxGET_string(sw,UxP_PushButtonGadgetRD_armPixmap,"armPixmap")
#define UxPutArmPixmap(sw,val) \
        UxPUT_string(sw,UxP_PushButtonGadgetRD_armPixmap,"armPixmap",val)
#endif

#ifndef UxGetArmColor
extern binptr UxP_PushButtonGadgetRD_armColor;
#define UxGetArmColor(sw) \
        UxGET_string(sw,UxP_PushButtonGadgetRD_armColor,"armColor")
#define UxPutArmColor(sw,val) \
        UxPUT_string(sw,UxP_PushButtonGadgetRD_armColor,"armColor",val)
#endif

extern Class_t UxC_pushButtonGadget;
#define UxCreatePushButtonGadget(name,parent) \
        UxCreateSwidget(UxC_pushButtonGadget,name,parent)
#else
#define UxCreatePushButtonGadget(name,parent) \
        UxCreateSwidget(name,xmPushButtonGadgetClass,parent)
#endif
#endif
