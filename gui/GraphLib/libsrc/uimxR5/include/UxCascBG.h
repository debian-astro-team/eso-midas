/* @(#)UxCascBG.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
#ifndef UXCascadeButtonGadget_INCLUDED
#define UXCascadeButtonGadget_INCLUDED

#include "Xm/CascadeBG.h"

#ifdef DESIGN_TIME
#include "UxLabelG.h"

#ifndef UxGetSubMenuId
extern binptr UxP_CascadeButtonGadgetRD_subMenuId;
#define UxGetSubMenuId(sw) \
        UxGET_string(sw,UxP_CascadeButtonGadgetRD_subMenuId,"subMenuId")
#define UxPutSubMenuId(sw,val) \
        UxPUT_string(sw,UxP_CascadeButtonGadgetRD_subMenuId,"subMenuId",val)
#endif

#ifndef UxGetMappingDelay
extern binptr UxP_CascadeButtonGadgetRD_mappingDelay;
#define UxGetMappingDelay(sw) \
        UxGET_int(sw,UxP_CascadeButtonGadgetRD_mappingDelay,"mappingDelay")
#define UxPutMappingDelay(sw,val) \
        UxPUT_int(sw,UxP_CascadeButtonGadgetRD_mappingDelay,"mappingDelay",val)
#endif

#ifndef UxGetCascadePixmap
extern binptr UxP_CascadeButtonGadgetRD_cascadePixmap;
#define UxGetCascadePixmap(sw) \
        UxGET_string(sw,UxP_CascadeButtonGadgetRD_cascadePixmap,"cascadePixmap")
#define UxPutCascadePixmap(sw,val) \
        UxPUT_string(sw,UxP_CascadeButtonGadgetRD_cascadePixmap,"cascadePixmap",val)
#endif

extern Class_t UxC_cascadeButtonGadget;
#define UxCreateCascadeButtonGadget(name,parent) \
        UxCreateSwidget(UxC_cascadeButtonGadget,name,parent)
#else
#define UxCreateCascadeButtonGadget(name,parent) \
        UxCreateSwidget(name,xmCascadeButtonGadgetClass,parent)
#endif
#endif
