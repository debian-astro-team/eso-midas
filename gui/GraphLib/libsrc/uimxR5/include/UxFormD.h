/* @(#)UxFormD.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXFormDialog_INCLUDED
#define UXFormDialog_INCLUDED

#include "Xm/Form.h"

#ifdef DESIGN_TIME
#include "UxForm.h"

extern Class_t UxC_formDialog;
#define UxCreateFormDialog(name,parent) \
        UxCreateSwidget(UxC_formDialog,name,parent)
#else
#define UxCreateFormDialog(name,parent) \
        UxCreateDialogSwidget(name,xmFormWidgetClass,parent)
#endif
#endif
