/* @(#)UxDialSh.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXDialogShell_INCLUDED
#define UXDialogShell_INCLUDED

#include "Xm/DialogS.h"

#ifdef DESIGN_TIME
#include "UxTranSh.h"

extern Class_t UxC_dialogShell;
#define UxCreateDialogShell(name,parent) \
        UxCreateSwidget(UxC_dialogShell,name,parent)
#else
#define UxCreateDialogShell(name,parent) \
        UxCreateSwidget(name,xmDialogShellWidgetClass,parent)
#endif
#endif
