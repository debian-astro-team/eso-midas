/* @(#)UxBboard.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
#ifndef UXBulletinBoard_INCLUDED
#define UXBulletinBoard_INCLUDED

#include "Xm/BulletinB.h"

#ifdef DESIGN_TIME
#include "UxMgr.h"

#ifndef UxPutTextTranslations
extern binptr UxP_BulletinBoardRD_textTranslations;
#define UxPutTextTranslations(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_textTranslations,"textTranslations",val)
#endif

#ifndef UxGetTextFontList
extern binptr UxP_BulletinBoardRD_textFontList;
#define UxGetTextFontList(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_textFontList,"textFontList")
#define UxPutTextFontList(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_textFontList,"textFontList",val)
#endif

#ifndef UxGetShadowType
extern binptr UxP_BulletinBoardRD_shadowType;
#define UxGetShadowType(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_shadowType,"shadowType")
#define UxPutShadowType(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_shadowType,"shadowType",val)
#endif

#ifndef UxGetResizePolicy
extern binptr UxP_BulletinBoardRD_resizePolicy;
#define UxGetResizePolicy(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_resizePolicy,"resizePolicy")
#define UxPutResizePolicy(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_resizePolicy,"resizePolicy",val)
#endif

#ifndef UxGetNoResize
extern binptr UxP_BulletinBoardRD_noResize;
#define UxGetNoResize(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_noResize,"noResize")
#define UxPutNoResize(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_noResize,"noResize",val)
#endif

#ifndef UxGetMarginWidth
extern binptr UxP_BulletinBoardRD_marginWidth;
#define UxGetMarginWidth(sw) \
        UxGET_int(sw,UxP_BulletinBoardRD_marginWidth,"marginWidth")
#define UxPutMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_BulletinBoardRD_marginWidth,"marginWidth",val)
#endif

#ifndef UxGetMarginHeight
extern binptr UxP_BulletinBoardRD_marginHeight;
#define UxGetMarginHeight(sw) \
        UxGET_int(sw,UxP_BulletinBoardRD_marginHeight,"marginHeight")
#define UxPutMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_BulletinBoardRD_marginHeight,"marginHeight",val)
#endif

#ifndef UxGetLabelFontList
extern binptr UxP_BulletinBoardRD_labelFontList;
#define UxGetLabelFontList(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_labelFontList,"labelFontList")
#define UxPutLabelFontList(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_labelFontList,"labelFontList",val)
#endif

#ifndef UxGetDialogTitle
extern binptr UxP_BulletinBoardRD_dialogTitle;
#define UxGetDialogTitle(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_dialogTitle,"dialogTitle")
#define UxPutDialogTitle(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_dialogTitle,"dialogTitle",val)
#endif

#ifndef UxGetDialogStyle
extern binptr UxP_BulletinBoardRD_dialogStyle;
#define UxGetDialogStyle(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_dialogStyle,"dialogStyle")
#define UxPutDialogStyle(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_dialogStyle,"dialogStyle",val)
#endif

#ifndef UxGetDefaultPosition
extern binptr UxP_BulletinBoardRD_defaultPosition;
#define UxGetDefaultPosition(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_defaultPosition,"defaultPosition")
#define UxPutDefaultPosition(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_defaultPosition,"defaultPosition",val)
#endif

#ifndef UxGetDefaultButton
extern binptr UxP_BulletinBoardRD_defaultButton;
#define UxGetDefaultButton(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_defaultButton,"defaultButton")
#define UxPutDefaultButton(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_defaultButton,"defaultButton",val)
#endif

#ifndef UxGetCancelButton
extern binptr UxP_BulletinBoardRD_cancelButton;
#define UxGetCancelButton(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_cancelButton,"cancelButton")
#define UxPutCancelButton(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_cancelButton,"cancelButton",val)
#endif

#ifndef UxGetButtonFontList
extern binptr UxP_BulletinBoardRD_buttonFontList;
#define UxGetButtonFontList(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_buttonFontList,"buttonFontList")
#define UxPutButtonFontList(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_buttonFontList,"buttonFontList",val)
#endif

#ifndef UxGetAutoUnmanage
extern binptr UxP_BulletinBoardRD_autoUnmanage;
#define UxGetAutoUnmanage(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_autoUnmanage,"autoUnmanage")
#define UxPutAutoUnmanage(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_autoUnmanage,"autoUnmanage",val)
#endif

#ifndef UxGetAllowOverlap
extern binptr UxP_BulletinBoardRD_allowOverlap;
#define UxGetAllowOverlap(sw) \
        UxGET_string(sw,UxP_BulletinBoardRD_allowOverlap,"allowOverlap")
#define UxPutAllowOverlap(sw,val) \
        UxPUT_string(sw,UxP_BulletinBoardRD_allowOverlap,"allowOverlap",val)
#endif

extern Class_t UxC_bulletinBoard;
#define UxCreateBulletinBoard(name,parent) \
        UxCreateSwidget(UxC_bulletinBoard,name,parent)
#else
#define UxCreateBulletinBoard(name,parent) \
        UxCreateSwidget(name,xmBulletinBoardWidgetClass,parent)
#endif
#endif
