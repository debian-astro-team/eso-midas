/* @(#)UxComp.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
#ifndef UXComposite_INCLUDED
#define UXComposite_INCLUDED

#include "Xm/Xm.h"

#ifdef DESIGN_TIME
#include "UxCore.h"

#ifndef UxGetNumChildren
extern binptr UxP_CompositeRD_numChildren;
#define UxGetNumChildren(sw) \
        UxGET_int(sw,UxP_CompositeRD_numChildren,"numChildren")
#endif

#ifndef UxGetInsertPosition
extern binptr UxP_CompositeRD_insertPosition;
#define UxGetInsertPosition(sw) \
        UxGET_cardFunction(sw,UxP_CompositeRD_insertPosition,"insertPosition")
#define UxPutInsertPosition(sw,val) \
        UxPUT_cardFunction(sw,UxP_CompositeRD_insertPosition,"insertPosition",val)
#endif

#ifndef UxGetChildren
extern binptr UxP_CompositeRD_children;
#define UxGetChildren(sw) \
        UxGET_stringTable(sw,UxP_CompositeRD_children,"children")
#endif

extern Class_t UxC_composite;
#define UxCreateComposite(name,parent) \
        UxCreateSwidget(UxC_composite,name,parent)
#else
#define UxCreateComposite(name,parent) \
        UxCreateSwidget(name,compositeWidgetClass,parent)
#endif
#endif
