/* @(#)UxWorkD.h	17.1 (ESO-IPG) 01/25/02 17:26:04 */
#ifndef UXWorkingDialog_INCLUDED
#define UXWorkingDialog_INCLUDED

#include "Xm/MessageB.h"

#ifdef DESIGN_TIME
#include "UxMsgBox.h"

extern Class_t UxC_workingDialog;
#define UxCreateWorkingDialog(name,parent) \
        UxCreateSwidget(UxC_workingDialog,name,parent)
#else
#define UxCreateWorkingDialog(name,parent) \
        UxCreateDialogSwidget(name,xmMessageBoxWidgetClass,parent)
#endif
#endif
