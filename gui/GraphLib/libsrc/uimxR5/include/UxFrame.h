/* @(#)UxFrame.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXFrame_INCLUDED
#define UXFrame_INCLUDED

#include "Xm/Frame.h"

#ifdef DESIGN_TIME
#include "UxMgr.h"

#ifndef UxGetChildVerticalAlignment
extern binptr UxP_FrameRD_childVerticalAlignment;
#define UxGetChildVerticalAlignment(sw) \
        UxGET_string(sw,UxP_FrameRD_childVerticalAlignment,"childVerticalAlignment")
#define UxPutChildVerticalAlignment(sw,val) \
        UxPUT_string(sw,UxP_FrameRD_childVerticalAlignment,"childVerticalAlignment",val)
#endif

#ifndef UxGetChildHorizontalSpacing
extern binptr UxP_FrameRD_childHorizontalSpacing;
#define UxGetChildHorizontalSpacing(sw) \
        UxGET_int(sw,UxP_FrameRD_childHorizontalSpacing,"childHorizontalSpacing")
#define UxPutChildHorizontalSpacing(sw,val) \
        UxPUT_int(sw,UxP_FrameRD_childHorizontalSpacing,"childHorizontalSpacing",val)
#endif

#ifndef UxGetChildHorizontalAlignment
extern binptr UxP_FrameRD_childHorizontalAlignment;
#define UxGetChildHorizontalAlignment(sw) \
        UxGET_string(sw,UxP_FrameRD_childHorizontalAlignment,"childHorizontalAlignment")
#define UxPutChildHorizontalAlignment(sw,val) \
        UxPUT_string(sw,UxP_FrameRD_childHorizontalAlignment,"childHorizontalAlignment",val)
#endif

#ifndef UxGetChildType
extern binptr UxP_FrameRD_childType;
#define UxGetChildType(sw) \
        UxGET_string(sw,UxP_FrameRD_childType,"childType")
#define UxPutChildType(sw,val) \
        UxPUT_string(sw,UxP_FrameRD_childType,"childType",val)
#endif

#ifndef UxGetShadowType
extern binptr UxP_FrameRD_shadowType;
#define UxGetShadowType(sw) \
        UxGET_string(sw,UxP_FrameRD_shadowType,"shadowType")
#define UxPutShadowType(sw,val) \
        UxPUT_string(sw,UxP_FrameRD_shadowType,"shadowType",val)
#endif

#ifndef UxGetMarginWidth
extern binptr UxP_FrameRD_marginWidth;
#define UxGetMarginWidth(sw) \
        UxGET_int(sw,UxP_FrameRD_marginWidth,"marginWidth")
#define UxPutMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_FrameRD_marginWidth,"marginWidth",val)
#endif

#ifndef UxGetMarginHeight
extern binptr UxP_FrameRD_marginHeight;
#define UxGetMarginHeight(sw) \
        UxGET_int(sw,UxP_FrameRD_marginHeight,"marginHeight")
#define UxPutMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_FrameRD_marginHeight,"marginHeight",val)
#endif

extern Class_t UxC_frame;
#define UxCreateFrame(name,parent) \
        UxCreateSwidget(UxC_frame,name,parent)
#else
#define UxCreateFrame(name,parent) \
        UxCreateSwidget(name,xmFrameWidgetClass,parent)
#endif
#endif
