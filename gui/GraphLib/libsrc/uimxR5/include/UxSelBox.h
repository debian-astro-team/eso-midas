/* @(#)UxSelBox.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXSelectionBox_INCLUDED
#define UXSelectionBox_INCLUDED

#include "Xm/SelectioB.h"

#ifdef DESIGN_TIME
#include "UxBboard.h"

#ifndef UxGetTextString
extern binptr UxP_SelectionBoxRD_textString;
#define UxGetTextString(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_textString,"textString")
#define UxPutTextString(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_textString,"textString",val)
#endif

#ifndef UxGetTextColumns
extern binptr UxP_SelectionBoxRD_textColumns;
#define UxGetTextColumns(sw) \
        UxGET_short(sw,UxP_SelectionBoxRD_textColumns,"textColumns")
#define UxPutTextColumns(sw,val) \
        UxPUT_short(sw,UxP_SelectionBoxRD_textColumns,"textColumns",val)
#endif

#ifndef UxPutTextAccelerators
extern binptr UxP_SelectionBoxRD_textAccelerators;
#define UxPutTextAccelerators(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_textAccelerators,"textAccelerators",val)
#endif

#ifndef UxGetSelectionLabelString
extern binptr UxP_SelectionBoxRD_selectionLabelString;
#define UxGetSelectionLabelString(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_selectionLabelString,"selectionLabelString")
#define UxPutSelectionLabelString(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_selectionLabelString,"selectionLabelString",val)
#endif

#ifndef UxGetOkLabelString
extern binptr UxP_SelectionBoxRD_okLabelString;
#define UxGetOkLabelString(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_okLabelString,"okLabelString")
#define UxPutOkLabelString(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_okLabelString,"okLabelString",val)
#endif

#ifndef UxGetMustMatch
extern binptr UxP_SelectionBoxRD_mustMatch;
#define UxGetMustMatch(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_mustMatch,"mustMatch")
#define UxPutMustMatch(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_mustMatch,"mustMatch",val)
#endif

#ifndef UxGetMinimizeButtons
extern binptr UxP_SelectionBoxRD_minimizeButtons;
#define UxGetMinimizeButtons(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_minimizeButtons,"minimizeButtons")
#define UxPutMinimizeButtons(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_minimizeButtons,"minimizeButtons",val)
#endif

#ifndef UxGetListVisibleItemCount
extern binptr UxP_SelectionBoxRD_listVisibleItemCount;
#define UxGetListVisibleItemCount(sw) \
        UxGET_int(sw,UxP_SelectionBoxRD_listVisibleItemCount,"listVisibleItemCount")
#define UxPutListVisibleItemCount(sw,val) \
        UxPUT_int(sw,UxP_SelectionBoxRD_listVisibleItemCount,"listVisibleItemCount",val)
#endif

#ifndef UxGetListLabelString
extern binptr UxP_SelectionBoxRD_listLabelString;
#define UxGetListLabelString(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_listLabelString,"listLabelString")
#define UxPutListLabelString(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_listLabelString,"listLabelString",val)
#endif

#ifndef UxGetListItemCount
extern binptr UxP_SelectionBoxRD_listItemCount;
#define UxGetListItemCount(sw) \
        UxGET_int(sw,UxP_SelectionBoxRD_listItemCount,"listItemCount")
#define UxPutListItemCount(sw,val) \
        UxPUT_int(sw,UxP_SelectionBoxRD_listItemCount,"listItemCount",val)
#endif

#ifndef UxGetListItems
extern binptr UxP_SelectionBoxRD_listItems;
#define UxGetListItems(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_listItems,"listItems")
#define UxPutListItems(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_listItems,"listItems",val)
#endif

#ifndef UxGetHelpLabelString
extern binptr UxP_SelectionBoxRD_helpLabelString;
#define UxGetHelpLabelString(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_helpLabelString,"helpLabelString")
#define UxPutHelpLabelString(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_helpLabelString,"helpLabelString",val)
#endif

#ifndef UxGetDialogType
extern binptr UxP_SelectionBoxRD_dialogType;
#define UxGetDialogType(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_dialogType,"dialogType")
#define UxPutDialogType(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_dialogType,"dialogType",val)
#endif

#ifndef UxGetChildPlacement
extern binptr UxP_SelectionBoxRD_childPlacement;
#define UxGetChildPlacement(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_childPlacement,"childPlacement")
#define UxPutChildPlacement(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_childPlacement,"childPlacement",val)
#endif

#ifndef UxGetCancelLabelString
extern binptr UxP_SelectionBoxRD_cancelLabelString;
#define UxGetCancelLabelString(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_cancelLabelString,"cancelLabelString")
#define UxPutCancelLabelString(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_cancelLabelString,"cancelLabelString",val)
#endif

#ifndef UxGetApplyLabelString
extern binptr UxP_SelectionBoxRD_applyLabelString;
#define UxGetApplyLabelString(sw) \
        UxGET_string(sw,UxP_SelectionBoxRD_applyLabelString,"applyLabelString")
#define UxPutApplyLabelString(sw,val) \
        UxPUT_string(sw,UxP_SelectionBoxRD_applyLabelString,"applyLabelString",val)
#endif

extern Class_t UxC_selectionBox;
#define UxCreateSelectionBox(name,parent) \
        UxCreateSwidget(UxC_selectionBox,name,parent)
#else
#define UxCreateSelectionBox(name,parent) \
        UxCreateSwidget(name,xmSelectionBoxWidgetClass,parent)
#endif
#endif
