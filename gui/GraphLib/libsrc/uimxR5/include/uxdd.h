/* @(#)uxdd.h	17.1 (ESO-IPG) 01/25/02 17:26:06 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _UXDD_INCLUDED
#define _UXDD_INCLUDED

#include "uxproto.h"

#define MAXRESLEN	32

struct cl_entry {
		int	utype;
		int	xtype;
		struct  cl_entry *next_cl;
		char	*xtclass;
		};

struct dd_entry {
		int        utype;
		int        xtype;
		struct cl_entry *next_cl;
		char       name[MAXRESLEN];
		struct	dd_entry *next;
		};

typedef struct dd_entry DD;
typedef struct dd_entry	*DDList;
typedef struct cl_entry CL_ENTRY;

#define  dd_lookup(l)      type_lookup(l,DD)
#define  cl_lookup(l)	   type_lookup(l,CL_ENTRY)

#define  dd_name(p)             (p->name)
#define  dd_put_name(p,n)       (dd_name(p) = n)
#define  dd_utype(p)            ((p)->utype)
#define  dd_put_utype(p,v)      (dd_utype(p) = v)
#define  dd_xtype(p)            ((p)->xtype)
#define  dd_put_xtype(p,v)      (dd_xtype(p) = v)

CL_ENTRY *UxDDEntry UXPROTO((char *name, void *xtclass));
void     UxDDInit UXPROTO((void));
void	 UxDDInstall UXPROTO((char *name, int utype, int xtype));
void	 UxDDAddEntry UXPROTO((char *name, void *xtclass, int utype, int xtype));


#endif  /* _UXDD_INCLUDED */
