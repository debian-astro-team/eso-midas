/* @(#)UxMenuSh.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
#ifndef UXMenuShell_INCLUDED
#define UXMenuShell_INCLUDED

#include "Xm/MenuShell.h"

#ifdef DESIGN_TIME
#include "UxOverSh.h"

#ifndef UxGetLabelFontList
extern binptr UxP_MenuShellRD_labelFontList;
#define UxGetLabelFontList(sw) \
        UxGET_string(sw,UxP_MenuShellRD_labelFontList,"labelFontList")
#define UxPutLabelFontList(sw,val) \
        UxPUT_string(sw,UxP_MenuShellRD_labelFontList,"labelFontList",val)
#endif

#ifndef UxGetDefaultFontList
extern binptr UxP_MenuShellRD_defaultFontList;
#define UxGetDefaultFontList(sw) \
        UxGET_string(sw,UxP_MenuShellRD_defaultFontList,"defaultFontList")
#define UxPutDefaultFontList(sw,val) \
        UxPUT_string(sw,UxP_MenuShellRD_defaultFontList,"defaultFontList",val)
#endif

#ifndef UxGetButtonFontList
extern binptr UxP_MenuShellRD_buttonFontList;
#define UxGetButtonFontList(sw) \
        UxGET_string(sw,UxP_MenuShellRD_buttonFontList,"buttonFontList")
#define UxPutButtonFontList(sw,val) \
        UxPUT_string(sw,UxP_MenuShellRD_buttonFontList,"buttonFontList",val)
#endif

extern Class_t UxC_menuShell;
#define UxCreateMenuShell(name,parent) \
        UxCreateSwidget(UxC_menuShell,name,parent)
#else
#define UxCreateMenuShell(name,parent) \
        UxCreateSwidget(name,xmMenuShellWidgetClass,parent)
#endif
#endif
