/* @(#)UxBbD.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
#ifndef UXBulletinBoardDialog_INCLUDED
#define UXBulletinBoardDialog_INCLUDED

#include "Xm/BulletinB.h"

#ifdef DESIGN_TIME
#include "UxBboard.h"

extern Class_t UxC_bulletinBoardDialog;
#define UxCreateBulletinBoardDialog(name,parent) \
        UxCreateSwidget(UxC_bulletinBoardDialog,name,parent)
#else
#define UxCreateBulletinBoardDialog(name,parent) \
        UxCreateDialogSwidget(name,xmBulletinBoardWidgetClass,parent)
#endif
#endif
