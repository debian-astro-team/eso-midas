/* @(#)lib_msg.h	17.1 (ESO-IPG) 01/25/02 17:26:04 */
/*---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
#define	MC_LIBUIMX	3
#define	MS_LU_OUTMEMORY	1
#define	MS_LU_FORMATDUMP	2
#define	MS_LU_INSTEXCEEDED	3
#define	MS_LU_MALLOCFAILED	4
#define	MS_LU_REALLOCFAILED	5
#define	MS_LU_UNDEFERROR	6
#define	MS_LU_SECONDNOTANCESTOR	7
#define	MS_LU_ALDRYREGISTERED	8
#define	MS_LU_WIDGETMIDGETFMT	9
