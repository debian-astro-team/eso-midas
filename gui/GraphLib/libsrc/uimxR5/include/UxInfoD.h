/* @(#)UxInfoD.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
#ifndef UXInformationDialog_INCLUDED
#define UXInformationDialog_INCLUDED

#include "Xm/MessageB.h"

#ifdef DESIGN_TIME
#include "UxMsgBox.h"

extern Class_t UxC_informationDialog;
#define UxCreateInformationDialog(name,parent) \
        UxCreateSwidget(UxC_informationDialog,name,parent)
#else
#define UxCreateInformationDialog(name,parent) \
        UxCreateDialogSwidget(name,xmMessageBoxWidgetClass,parent)
#endif
#endif
