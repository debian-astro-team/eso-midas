/* @(#)UxShell.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXShell_INCLUDED
#define UXShell_INCLUDED

#include "X11/Shell.h"

#ifdef DESIGN_TIME
#include "UxComp.h"

#ifndef UxGetVisual
extern binptr UxP_ShellRD_visual;
#define UxGetVisual(sw) \
        UxGET_visualPointer(sw,UxP_ShellRD_visual,"visual")
#define UxPutVisual(sw,val) \
        UxPUT_visualPointer(sw,UxP_ShellRD_visual,"visual",val)
#endif

#ifndef UxGetSaveUnder
extern binptr UxP_ShellRD_saveUnder;
#define UxGetSaveUnder(sw) \
        UxGET_string(sw,UxP_ShellRD_saveUnder,"saveUnder")
#define UxPutSaveUnder(sw,val) \
        UxPUT_string(sw,UxP_ShellRD_saveUnder,"saveUnder",val)
#endif

#ifndef UxGetOverrideRedirect
extern binptr UxP_ShellRD_overrideRedirect;
#define UxGetOverrideRedirect(sw) \
        UxGET_string(sw,UxP_ShellRD_overrideRedirect,"overrideRedirect")
#endif

#ifndef UxGetGeometry
extern binptr UxP_ShellRD_geometry;
#define UxGetGeometry(sw) \
        UxGET_string(sw,UxP_ShellRD_geometry,"geometry")
#define UxPutGeometry(sw,val) \
        UxPUT_string(sw,UxP_ShellRD_geometry,"geometry",val)
#endif

#ifndef UxGetCreatePopupChildProc
extern binptr UxP_ShellRD_createPopupChildProc;
#define UxGetCreatePopupChildProc(sw) \
        UxGET_voidFunction(sw,UxP_ShellRD_createPopupChildProc,"createPopupChildProc")
#define UxPutCreatePopupChildProc(sw,val) \
        UxPUT_voidFunction(sw,UxP_ShellRD_createPopupChildProc,"createPopupChildProc",val)
#endif

#ifndef UxGetAllowShellResize
extern binptr UxP_ShellRD_allowShellResize;
#define UxGetAllowShellResize(sw) \
        UxGET_string(sw,UxP_ShellRD_allowShellResize,"allowShellResize")
#define UxPutAllowShellResize(sw,val) \
        UxPUT_string(sw,UxP_ShellRD_allowShellResize,"allowShellResize",val)
#endif

extern Class_t UxC_shell;
#define UxCreateShell(name,parent) \
        UxCreateSwidget(UxC_shell,name,parent)
#else
#define UxCreateShell(name,parent) \
        UxCreateSwidget(name,shellWidgetClass,parent)
#endif
#endif
