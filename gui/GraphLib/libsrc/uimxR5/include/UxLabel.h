/* @(#)UxLabel.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
#ifndef UXLabel_INCLUDED
#define UXLabel_INCLUDED

#include "Xm/Label.h"

#ifdef DESIGN_TIME
#include "UxPrim.h"

#ifndef UxGetStringDirection
extern binptr UxP_LabelRD_stringDirection;
#define UxGetStringDirection(sw) \
        UxGET_string(sw,UxP_LabelRD_stringDirection,"stringDirection")
#define UxPutStringDirection(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_stringDirection,"stringDirection",val)
#endif

#ifndef UxGetRecomputeSize
extern binptr UxP_LabelRD_recomputeSize;
#define UxGetRecomputeSize(sw) \
        UxGET_string(sw,UxP_LabelRD_recomputeSize,"recomputeSize")
#define UxPutRecomputeSize(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_recomputeSize,"recomputeSize",val)
#endif

#ifndef UxGetMnemonicCharSet
extern binptr UxP_LabelRD_mnemonicCharSet;
#define UxGetMnemonicCharSet(sw) \
        UxGET_string(sw,UxP_LabelRD_mnemonicCharSet,"mnemonicCharSet")
#define UxPutMnemonicCharSet(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_mnemonicCharSet,"mnemonicCharSet",val)
#endif

#ifndef UxGetMnemonic
extern binptr UxP_LabelRD_mnemonic;
#define UxGetMnemonic(sw) \
        UxGET_string(sw,UxP_LabelRD_mnemonic,"mnemonic")
#define UxPutMnemonic(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_mnemonic,"mnemonic",val)
#endif

#ifndef UxGetMarginWidth
extern binptr UxP_LabelRD_marginWidth;
#define UxGetMarginWidth(sw) \
        UxGET_int(sw,UxP_LabelRD_marginWidth,"marginWidth")
#define UxPutMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_LabelRD_marginWidth,"marginWidth",val)
#endif

#ifndef UxGetMarginTop
extern binptr UxP_LabelRD_marginTop;
#define UxGetMarginTop(sw) \
        UxGET_int(sw,UxP_LabelRD_marginTop,"marginTop")
#define UxPutMarginTop(sw,val) \
        UxPUT_int(sw,UxP_LabelRD_marginTop,"marginTop",val)
#endif

#ifndef UxGetMarginRight
extern binptr UxP_LabelRD_marginRight;
#define UxGetMarginRight(sw) \
        UxGET_int(sw,UxP_LabelRD_marginRight,"marginRight")
#define UxPutMarginRight(sw,val) \
        UxPUT_int(sw,UxP_LabelRD_marginRight,"marginRight",val)
#endif

#ifndef UxGetMarginLeft
extern binptr UxP_LabelRD_marginLeft;
#define UxGetMarginLeft(sw) \
        UxGET_int(sw,UxP_LabelRD_marginLeft,"marginLeft")
#define UxPutMarginLeft(sw,val) \
        UxPUT_int(sw,UxP_LabelRD_marginLeft,"marginLeft",val)
#endif

#ifndef UxGetMarginHeight
extern binptr UxP_LabelRD_marginHeight;
#define UxGetMarginHeight(sw) \
        UxGET_int(sw,UxP_LabelRD_marginHeight,"marginHeight")
#define UxPutMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_LabelRD_marginHeight,"marginHeight",val)
#endif

#ifndef UxGetMarginBottom
extern binptr UxP_LabelRD_marginBottom;
#define UxGetMarginBottom(sw) \
        UxGET_int(sw,UxP_LabelRD_marginBottom,"marginBottom")
#define UxPutMarginBottom(sw,val) \
        UxPUT_int(sw,UxP_LabelRD_marginBottom,"marginBottom",val)
#endif

#ifndef UxGetLabelType
extern binptr UxP_LabelRD_labelType;
#define UxGetLabelType(sw) \
        UxGET_string(sw,UxP_LabelRD_labelType,"labelType")
#define UxPutLabelType(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_labelType,"labelType",val)
#endif

#ifndef UxGetLabelString
extern binptr UxP_LabelRD_labelString;
#define UxGetLabelString(sw) \
        UxGET_string(sw,UxP_LabelRD_labelString,"labelString")
#define UxPutLabelString(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_labelString,"labelString",val)
#endif

#ifndef UxGetLabelPixmap
extern binptr UxP_LabelRD_labelPixmap;
#define UxGetLabelPixmap(sw) \
        UxGET_string(sw,UxP_LabelRD_labelPixmap,"labelPixmap")
#define UxPutLabelPixmap(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_labelPixmap,"labelPixmap",val)
#endif

#ifndef UxGetLabelInsensitivePixmap
extern binptr UxP_LabelRD_labelInsensitivePixmap;
#define UxGetLabelInsensitivePixmap(sw) \
        UxGET_string(sw,UxP_LabelRD_labelInsensitivePixmap,"labelInsensitivePixmap")
#define UxPutLabelInsensitivePixmap(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_labelInsensitivePixmap,"labelInsensitivePixmap",val)
#endif

#ifndef UxGetFontList
extern binptr UxP_LabelRD_fontList;
#define UxGetFontList(sw) \
        UxGET_string(sw,UxP_LabelRD_fontList,"fontList")
#define UxPutFontList(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_fontList,"fontList",val)
#endif

#ifndef UxGetAlignment
extern binptr UxP_LabelRD_alignment;
#define UxGetAlignment(sw) \
        UxGET_string(sw,UxP_LabelRD_alignment,"alignment")
#define UxPutAlignment(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_alignment,"alignment",val)
#endif

#ifndef UxGetAcceleratorText
extern binptr UxP_LabelRD_acceleratorText;
#define UxGetAcceleratorText(sw) \
        UxGET_string(sw,UxP_LabelRD_acceleratorText,"acceleratorText")
#define UxPutAcceleratorText(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_acceleratorText,"acceleratorText",val)
#endif

#ifndef UxGetAccelerator
extern binptr UxP_LabelRD_accelerator;
#define UxGetAccelerator(sw) \
        UxGET_string(sw,UxP_LabelRD_accelerator,"accelerator")
#define UxPutAccelerator(sw,val) \
        UxPUT_string(sw,UxP_LabelRD_accelerator,"accelerator",val)
#endif

extern Class_t UxC_label;
#define UxCreateLabel(name,parent) \
        UxCreateSwidget(UxC_label,name,parent)
#else
#define UxCreateLabel(name,parent) \
        UxCreateSwidget(name,xmLabelWidgetClass,parent)
#endif
#endif
