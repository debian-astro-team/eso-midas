/* @(#)UxSelBD.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXSelectionBoxDialog_INCLUDED
#define UXSelectionBoxDialog_INCLUDED

#include "Xm/SelectioB.h"

#ifdef DESIGN_TIME
#include "UxSelBox.h"

extern Class_t UxC_selectionBoxDialog;
#define UxCreateSelectionBoxDialog(name,parent) \
        UxCreateSwidget(UxC_selectionBoxDialog,name,parent)
#else
#define UxCreateSelectionBoxDialog(name,parent) \
        UxCreateDialogSwidget(name,xmSelectionBoxWidgetClass,parent)
#endif
#endif
