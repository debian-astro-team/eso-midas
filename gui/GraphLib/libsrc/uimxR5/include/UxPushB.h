/* @(#)UxPushB.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXPushButton_INCLUDED
#define UXPushButton_INCLUDED

#include "Xm/PushB.h"

#ifdef DESIGN_TIME
#include "UxLabel.h"

#ifndef UxGetShowAsDefault
extern binptr UxP_PushButtonRD_showAsDefault;
#define UxGetShowAsDefault(sw) \
        UxGET_int(sw,UxP_PushButtonRD_showAsDefault,"showAsDefault")
#define UxPutShowAsDefault(sw,val) \
        UxPUT_int(sw,UxP_PushButtonRD_showAsDefault,"showAsDefault",val)
#endif

#ifndef UxGetMultiClick
extern binptr UxP_PushButtonRD_multiClick;
#define UxGetMultiClick(sw) \
        UxGET_string(sw,UxP_PushButtonRD_multiClick,"multiClick")
#define UxPutMultiClick(sw,val) \
        UxPUT_string(sw,UxP_PushButtonRD_multiClick,"multiClick",val)
#endif

#ifndef UxGetFillOnArm
extern binptr UxP_PushButtonRD_fillOnArm;
#define UxGetFillOnArm(sw) \
        UxGET_string(sw,UxP_PushButtonRD_fillOnArm,"fillOnArm")
#define UxPutFillOnArm(sw,val) \
        UxPUT_string(sw,UxP_PushButtonRD_fillOnArm,"fillOnArm",val)
#endif

#ifndef UxGetDefaultButtonShadowThickness
extern binptr UxP_PushButtonRD_defaultButtonShadowThickness;
#define UxGetDefaultButtonShadowThickness(sw) \
        UxGET_int(sw,UxP_PushButtonRD_defaultButtonShadowThickness,"defaultButtonShadowThickness")
#define UxPutDefaultButtonShadowThickness(sw,val) \
        UxPUT_int(sw,UxP_PushButtonRD_defaultButtonShadowThickness,"defaultButtonShadowThickness",val)
#endif

#ifndef UxGetArmPixmap
extern binptr UxP_PushButtonRD_armPixmap;
#define UxGetArmPixmap(sw) \
        UxGET_string(sw,UxP_PushButtonRD_armPixmap,"armPixmap")
#define UxPutArmPixmap(sw,val) \
        UxPUT_string(sw,UxP_PushButtonRD_armPixmap,"armPixmap",val)
#endif

#ifndef UxGetArmColor
extern binptr UxP_PushButtonRD_armColor;
#define UxGetArmColor(sw) \
        UxGET_string(sw,UxP_PushButtonRD_armColor,"armColor")
#define UxPutArmColor(sw,val) \
        UxPUT_string(sw,UxP_PushButtonRD_armColor,"armColor",val)
#endif

extern Class_t UxC_pushButton;
#define UxCreatePushButton(name,parent) \
        UxCreateSwidget(UxC_pushButton,name,parent)
#else
#define UxCreatePushButton(name,parent) \
        UxCreateSwidget(name,xmPushButtonWidgetClass,parent)
#endif
#endif
