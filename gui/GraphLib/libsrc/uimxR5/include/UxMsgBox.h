/* @(#)UxMsgBox.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXMessageBox_INCLUDED
#define UXMessageBox_INCLUDED

#include "Xm/MessageB.h"

#ifdef DESIGN_TIME
#include "UxBboard.h"

#ifndef UxGetSymbolPixmap
extern binptr UxP_MessageBoxRD_symbolPixmap;
#define UxGetSymbolPixmap(sw) \
        UxGET_string(sw,UxP_MessageBoxRD_symbolPixmap,"symbolPixmap")
#define UxPutSymbolPixmap(sw,val) \
        UxPUT_string(sw,UxP_MessageBoxRD_symbolPixmap,"symbolPixmap",val)
#endif

#ifndef UxGetOkLabelString
extern binptr UxP_MessageBoxRD_okLabelString;
#define UxGetOkLabelString(sw) \
        UxGET_string(sw,UxP_MessageBoxRD_okLabelString,"okLabelString")
#define UxPutOkLabelString(sw,val) \
        UxPUT_string(sw,UxP_MessageBoxRD_okLabelString,"okLabelString",val)
#endif

#ifndef UxGetMinimizeButtons
extern binptr UxP_MessageBoxRD_minimizeButtons;
#define UxGetMinimizeButtons(sw) \
        UxGET_string(sw,UxP_MessageBoxRD_minimizeButtons,"minimizeButtons")
#define UxPutMinimizeButtons(sw,val) \
        UxPUT_string(sw,UxP_MessageBoxRD_minimizeButtons,"minimizeButtons",val)
#endif

#ifndef UxGetMessageString
extern binptr UxP_MessageBoxRD_messageString;
#define UxGetMessageString(sw) \
        UxGET_string(sw,UxP_MessageBoxRD_messageString,"messageString")
#define UxPutMessageString(sw,val) \
        UxPUT_string(sw,UxP_MessageBoxRD_messageString,"messageString",val)
#endif

#ifndef UxGetMessageAlignment
extern binptr UxP_MessageBoxRD_messageAlignment;
#define UxGetMessageAlignment(sw) \
        UxGET_string(sw,UxP_MessageBoxRD_messageAlignment,"messageAlignment")
#define UxPutMessageAlignment(sw,val) \
        UxPUT_string(sw,UxP_MessageBoxRD_messageAlignment,"messageAlignment",val)
#endif

#ifndef UxGetHelpLabelString
extern binptr UxP_MessageBoxRD_helpLabelString;
#define UxGetHelpLabelString(sw) \
        UxGET_string(sw,UxP_MessageBoxRD_helpLabelString,"helpLabelString")
#define UxPutHelpLabelString(sw,val) \
        UxPUT_string(sw,UxP_MessageBoxRD_helpLabelString,"helpLabelString",val)
#endif

#ifndef UxGetMsgDialogType
extern binptr UxP_MessageBoxRD_msgDialogType;
#define UxGetMsgDialogType(sw) \
        UxGET_string(sw,UxP_MessageBoxRD_msgDialogType,"msgDialogType")
#define UxPutMsgDialogType(sw,val) \
        UxPUT_string(sw,UxP_MessageBoxRD_msgDialogType,"msgDialogType",val)
#endif

#ifndef UxGetDefaultButtonType
extern binptr UxP_MessageBoxRD_defaultButtonType;
#define UxGetDefaultButtonType(sw) \
        UxGET_string(sw,UxP_MessageBoxRD_defaultButtonType,"defaultButtonType")
#define UxPutDefaultButtonType(sw,val) \
        UxPUT_string(sw,UxP_MessageBoxRD_defaultButtonType,"defaultButtonType",val)
#endif

#ifndef UxGetCancelLabelString
extern binptr UxP_MessageBoxRD_cancelLabelString;
#define UxGetCancelLabelString(sw) \
        UxGET_string(sw,UxP_MessageBoxRD_cancelLabelString,"cancelLabelString")
#define UxPutCancelLabelString(sw,val) \
        UxPUT_string(sw,UxP_MessageBoxRD_cancelLabelString,"cancelLabelString",val)
#endif

extern Class_t UxC_messageBox;
#define UxCreateMessageBox(name,parent) \
        UxCreateSwidget(UxC_messageBox,name,parent)
#else
#define UxCreateMessageBox(name,parent) \
        UxCreateSwidget(name,xmMessageBoxWidgetClass,parent)
#endif
#endif
