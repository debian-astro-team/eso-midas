/* @(#)utype.h	17.1 (ESO-IPG) 01/25/02 17:26:05 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *---------------------------------------------------------------------
 *   File_Description_Section
 *-------------------------------------------------------------------*/

#ifndef	_UTYPE_INCLUDED
#define	_UTYPE_INCLUDED

/*--- include files ---*/
#include "uxproto.h"
#include <X11/Intrinsic.h>

/*--- macro symbolic constants ---*/
/* Defined constants used by the convertor functions */
#define TO_UIMX 0
#define TO_X 1

/*--- macro functions ---*/

/*--- types ---*/
typedef void (* func_ptr)();
#ifdef RUNTIME
typedef void (*voidFunction)();
typedef Cardinal (*cardFunction)();
typedef Visual *visualPointer;
#define XmNmsgDialogType        "msgDialogType"
#endif

/*--- external functions ---*/
/* Declarations of functions in utype.c and enum_type.c and */
extern void	UxUtype_init UXPROTO(( void ));

extern int	UxUimx_to_x UXPROTO(( swidget,
				          int,    char **,
				          int,    char *,
				          int ));

extern int	UxCallConverter UXPROTO(( swidget,
				          int,    char **,
				          int,    char *,
				          int ));

extern int	UxStringToIntEnum UXPROTO(( swidget,
					    char **, int *,
					    int,
					    int ));

extern int	UxStringToCharEnum UXPROTO(( swidget,
					     char **, unsigned char *,
					     int,
					     int ));

extern void 	UxUpdateRotatingBuffer UXPROTO((int *BufIndex,
						char ***RingBuffer,
						char *Val,
						func_ptr free_func));
					
/* Declarations of functions in user-misc.c */
extern void 	UxFreeXValue UXPROTO(( swidget,
				       char *,
				       XtArgVal));

extern void	UxClearText UXPROTO(( swidget ));

extern void	UxFreeClientDataCB UXPROTO ((Widget _wgt,
					     XtPointer client_data,
					     XtPointer _call_data));

/*--- external variables ---*/

/**********************************************************************
*		Uimx_to_X type index declarations		      *
* The following variables get storage defined for them in utypevars.c *
**********************************************************************/

/* --------- UIMX types --------- */

extern 	int 	UxUT_float;
extern 	int 	UxUT_int;
extern 	int 	UxUT_short;
extern 	int 	UxUT_string;
extern 	int 	UxUT_vhandle;
extern 	int 	UxUT_char;
extern 	int 	UxUT_long;
extern 	int 	UxUT_stringTable;
extern 	int 	UxUT_cardFunction;
extern 	int 	UxUT_voidFunction;
extern 	int 	UxUT_visualPointer;
extern 	int 	UxUT_XmTextSource;

/* --------- X types --------- */

/* Non-enumerated xtypes (in alphabetic order, ignoring case) */

extern 	int 	UxXT_Accelerators;
extern  int	UxXT_Atom;
extern 	int 	UxXT_bitmap;
extern 	int 	UxXT_BorderPixmap;
extern 	int 	UxXT_BottomShadowPixmap;
extern	int	UxXT_char;
extern	int	UxXT_Colormap;
extern 	int 	UxXT_CreatePopupChildProc;
extern  int	UxXT_Dimension;
extern	int	UxXT_DirListItems;
extern 	int 	UxXT_DirSearchProc;
extern	int	UxXT_FileListItems;
extern 	int 	UxXT_FileSearchProc;
extern 	int 	UxXT_FontStruct;
extern 	int 	UxXT_HighlightPixmap;
extern	int	UxXT_HistoryItems;
extern 	int 	UxXT_InsertPosition;
extern 	int 	UxXT_int;
extern	int	UxXT_Items;
extern	int	UxXT_KeySym;
extern	int	UxXT_ListItems;
extern	int	UxXT_StringOrNull;
extern 	int 	UxXT_Pixel;
extern 	int 	UxXT_Pixmap;
extern 	int 	UxXT_Pointer;
extern  int	UxXT_Position;
extern 	int 	UxXT_QualifySearchDataProc;
extern	int	UxXT_SelectedItems;
extern 	int 	UxXT_short;
extern 	int 	UxXT_String;
extern	int	UxXT_StringTable;
extern 	int 	UxXT_TopShadowPixmap;
extern 	int 	UxXT_Translations;
extern	int	UxXT_ValueWcs;
extern	int	UxXT_VisualPointer;
extern 	int 	UxXT_Widget;
extern	int	UxXT_WidgetClass;
extern 	int 	UxXT_WidgetList;
extern 	int 	UxXT_Window;
extern 	int 	UxXT_XID;
extern	int	UxXT_XmFontList;
extern	int	UxXT_XmString;
extern	int	UxXT_XmTextSource;

/* Enumerated xtypes (in alphabetic order) */

extern	int	UxXT_Alignment;
extern	int	UxXT_ArrowDirection;
extern  int     UxXT_AudibleWarning;
extern	int	UxXT_AttachmentType;
extern 	int 	UxXT_Bool;
extern 	int 	UxXT_Boolean;
extern  int     UxXT_ChildType;
extern	int	UxXT_ChildPlacement;
extern  int     UxXT_ChildVerticalAlignment;
extern  int     UxXT_CommandWindowLocation;
extern	int	UxXT_EntryVerticalAlignment;
extern	int	UxXT_DefaultButtonType;
extern	int	UxXT_DeleteResponse;
extern	int	UxXT_DialogStyle;
extern	int	UxXT_DialogType;
extern	int	UxXT_EditMode;
extern	int	UxXT_FileTypeMask;
extern	int	UxXT_IndicatorType;
extern 	int 	UxXT_InitialWindowState;
extern	int	UxXT_KeyboardFocusPolicy;
extern	int	UxXT_LabelType;
extern	int	UxXT_ListSizePolicy;
extern	int	UxXT_MsgDialogType;
extern	int	UxXT_MultiClick;
extern	int	UxXT_MwmInputMode;
extern	int	UxXT_NavigationType;
extern	int	UxXT_Orientation;
extern	int	UxXT_Packing;
extern	int	UxXT_ProcessingDirection;
extern	int	UxXT_ResizePolicy;
extern	int	UxXT_RowColumnType;
extern	int	UxXT_ScrollBarDisplayPolicy;
extern	int	UxXT_ScrollBarPlacement;
extern	int	UxXT_ScrollingPolicy;
extern	int	UxXT_SelectionPolicy;
extern	int	UxXT_SelectionArray;
extern	int	UxXT_SeparatorType;
extern	int	UxXT_ShadowType;
extern	int	UxXT_StringDirection;
extern	int	UxXT_TearOffModel;
extern	int	UxXT_UnitType;
extern	int	UxXT_VisualPolicy;
extern	int	UxXT_WinGravity;
					
#endif /* _UTYPE_INCLUDED */
