/* @(#)UxScList.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXScrolledList_INCLUDED
#define UXScrolledList_INCLUDED

#include "Xm/List.h"

#ifdef DESIGN_TIME
#include "UxList.h"

#ifndef UxGetVerticalScrollBar
extern binptr UxP_ScrolledListRD_verticalScrollBar;
#define UxGetVerticalScrollBar(sw) \
        UxGET_string(sw,UxP_ScrolledListRD_verticalScrollBar,"verticalScrollBar")
#endif

#ifndef UxGetHorizontalScrollBar
extern binptr UxP_ScrolledListRD_horizontalScrollBar;
#define UxGetHorizontalScrollBar(sw) \
        UxGET_string(sw,UxP_ScrolledListRD_horizontalScrollBar,"horizontalScrollBar")
#endif

extern Class_t UxC_scrolledList;
#define UxCreateScrolledList(name,parent) \
        UxCreateSwidget(UxC_scrolledList,name,parent)
#else
#define UxCreateScrolledList(name,parent) \
        UxCreateSwidget(name,xmListWidgetClass,parent)
#endif
#endif
