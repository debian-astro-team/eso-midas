/* @(#)UxList.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
#ifndef UXList_INCLUDED
#define UXList_INCLUDED

#include "Xm/List.h"

#ifdef DESIGN_TIME
#include "UxPrim.h"

#ifndef UxGetVisibleItemCount
extern binptr UxP_ListRD_visibleItemCount;
#define UxGetVisibleItemCount(sw) \
        UxGET_int(sw,UxP_ListRD_visibleItemCount,"visibleItemCount")
#define UxPutVisibleItemCount(sw,val) \
        UxPUT_int(sw,UxP_ListRD_visibleItemCount,"visibleItemCount",val)
#endif

#ifndef UxGetTopItemPosition
extern binptr UxP_ListRD_topItemPosition;
#define UxGetTopItemPosition(sw) \
        UxGET_int(sw,UxP_ListRD_topItemPosition,"topItemPosition")
#define UxPutTopItemPosition(sw,val) \
        UxPUT_int(sw,UxP_ListRD_topItemPosition,"topItemPosition",val)
#endif

#ifndef UxGetStringDirection
extern binptr UxP_ListRD_stringDirection;
#define UxGetStringDirection(sw) \
        UxGET_string(sw,UxP_ListRD_stringDirection,"stringDirection")
#define UxPutStringDirection(sw,val) \
        UxPUT_string(sw,UxP_ListRD_stringDirection,"stringDirection",val)
#endif

#ifndef UxGetSelectionPolicy
extern binptr UxP_ListRD_selectionPolicy;
#define UxGetSelectionPolicy(sw) \
        UxGET_string(sw,UxP_ListRD_selectionPolicy,"selectionPolicy")
#define UxPutSelectionPolicy(sw,val) \
        UxPUT_string(sw,UxP_ListRD_selectionPolicy,"selectionPolicy",val)
#endif

#ifndef UxGetSelectedItemCount
extern binptr UxP_ListRD_selectedItemCount;
#define UxGetSelectedItemCount(sw) \
        UxGET_int(sw,UxP_ListRD_selectedItemCount,"selectedItemCount")
#define UxPutSelectedItemCount(sw,val) \
        UxPUT_int(sw,UxP_ListRD_selectedItemCount,"selectedItemCount",val)
#endif

#ifndef UxGetSelectedItems
extern binptr UxP_ListRD_selectedItems;
#define UxGetSelectedItems(sw) \
        UxGET_string(sw,UxP_ListRD_selectedItems,"selectedItems")
#define UxPutSelectedItems(sw,val) \
        UxPUT_string(sw,UxP_ListRD_selectedItems,"selectedItems",val)
#endif

#ifndef UxGetScrollBarDisplayPolicy
extern binptr UxP_ListRD_scrollBarDisplayPolicy;
#define UxGetScrollBarDisplayPolicy(sw) \
        UxGET_string(sw,UxP_ListRD_scrollBarDisplayPolicy,"scrollBarDisplayPolicy")
#define UxPutScrollBarDisplayPolicy(sw,val) \
        UxPUT_string(sw,UxP_ListRD_scrollBarDisplayPolicy,"scrollBarDisplayPolicy",val)
#endif

#ifndef UxGetListSpacing
extern binptr UxP_ListRD_listSpacing;
#define UxGetListSpacing(sw) \
        UxGET_int(sw,UxP_ListRD_listSpacing,"listSpacing")
#define UxPutListSpacing(sw,val) \
        UxPUT_int(sw,UxP_ListRD_listSpacing,"listSpacing",val)
#endif

#ifndef UxGetListSizePolicy
extern binptr UxP_ListRD_listSizePolicy;
#define UxGetListSizePolicy(sw) \
        UxGET_string(sw,UxP_ListRD_listSizePolicy,"listSizePolicy")
#define UxPutListSizePolicy(sw,val) \
        UxPUT_string(sw,UxP_ListRD_listSizePolicy,"listSizePolicy",val)
#endif

#ifndef UxGetListMarginWidth
extern binptr UxP_ListRD_listMarginWidth;
#define UxGetListMarginWidth(sw) \
        UxGET_int(sw,UxP_ListRD_listMarginWidth,"listMarginWidth")
#define UxPutListMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_ListRD_listMarginWidth,"listMarginWidth",val)
#endif

#ifndef UxGetListMarginHeight
extern binptr UxP_ListRD_listMarginHeight;
#define UxGetListMarginHeight(sw) \
        UxGET_int(sw,UxP_ListRD_listMarginHeight,"listMarginHeight")
#define UxPutListMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_ListRD_listMarginHeight,"listMarginHeight",val)
#endif

#ifndef UxGetItemCount
extern binptr UxP_ListRD_itemCount;
#define UxGetItemCount(sw) \
        UxGET_int(sw,UxP_ListRD_itemCount,"itemCount")
#define UxPutItemCount(sw,val) \
        UxPUT_int(sw,UxP_ListRD_itemCount,"itemCount",val)
#endif

#ifndef UxGetItems
extern binptr UxP_ListRD_items;
#define UxGetItems(sw) \
        UxGET_string(sw,UxP_ListRD_items,"items")
#define UxPutItems(sw,val) \
        UxPUT_string(sw,UxP_ListRD_items,"items",val)
#endif

#ifndef UxGetFontList
extern binptr UxP_ListRD_fontList;
#define UxGetFontList(sw) \
        UxGET_string(sw,UxP_ListRD_fontList,"fontList")
#define UxPutFontList(sw,val) \
        UxPUT_string(sw,UxP_ListRD_fontList,"fontList",val)
#endif

#ifndef UxGetDoubleClickInterval
extern binptr UxP_ListRD_doubleClickInterval;
#define UxGetDoubleClickInterval(sw) \
        UxGET_int(sw,UxP_ListRD_doubleClickInterval,"doubleClickInterval")
#define UxPutDoubleClickInterval(sw,val) \
        UxPUT_int(sw,UxP_ListRD_doubleClickInterval,"doubleClickInterval",val)
#endif

#ifndef UxGetAutomaticSelection
extern binptr UxP_ListRD_automaticSelection;
#define UxGetAutomaticSelection(sw) \
        UxGET_string(sw,UxP_ListRD_automaticSelection,"automaticSelection")
#define UxPutAutomaticSelection(sw,val) \
        UxPUT_string(sw,UxP_ListRD_automaticSelection,"automaticSelection",val)
#endif

extern Class_t UxC_list;
#define UxCreateList(name,parent) \
        UxCreateSwidget(UxC_list,name,parent)
#else
#define UxCreateList(name,parent) \
        UxCreateSwidget(name,xmListWidgetClass,parent)
#endif
#endif
