/* @(#)UxCorba.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *         Copyright 1989-1993, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *---------------------------------------------------------------------
 * CORBA compliant definitions.
 *-------------------------------------------------------------------*/

#ifndef _UX_CORBA_INCLUDED
#define _UX_CORBA_INCLUDED 

/*-----------------------------------------------------
 * UXORB_HEADER, if defined, is the include form for
 * the header that defines the CORBA Environment type
 * and exception type codes.
 *
 * You can specify a file with a compile option like
 * 	cc -DUXORB_HEADER='<SomeOrb.h>'
 *-----------------------------------------------------*/

#ifdef UXORB_HEADER
#include UXORB_HEADER
#else
	/*
	 * In the absence of an ORB implementation,
	 * these minimal definitions satisfy our method dispatch code.
	 */

	typedef enum {
		NO_EXCEPTION,
		USER_EXCEPTION,
		SYSTEM_EXCEPTION
	} exception_type;

	typedef struct Environment {
		exception_type	_major;
	} Environment;
#endif  /* UXORB_HEADER */

/*
 * UxEnv is provided as a convenience for use in interface methods.
 */
extern	Environment	UxEnv;

#endif  /* _UX_CORBA_INCLUDED */
