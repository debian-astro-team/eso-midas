/* @(#)pathlist.h	17.1 (ESO-IPG) 01/25/02 17:26:04 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _PATHLIST_INCLUDED
#define _PATHLIST_INCLUDED

#include "uxproto.h"
#include "dstring.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef dstring *pathlist;

extern int	UxFileExists UXPROTO(( char* ));
extern pathlist	UxInitPath UXPROTO(( char* ));
extern void	UxAddPath UXPROTO(( pathlist, char* ));
extern void	UxResetPath UXPROTO(( pathlist, char* ));
extern void	UxFreePath UXPROTO(( pathlist ));
extern char	*UxGetPath UXPROTO(( pathlist ));
extern char	*UxExpandFilename UXPROTO(( pathlist, char* ));
extern char	*UxExpandAllFilenames UXPROTO(( pathlist, char* ));
extern char	*UxGetFilename UXPROTO(( char* ));

#ifdef __cplusplus
}  /* Close scope of 'extern "C"' declaration which encloses file. */
#endif /* __cplusplus */


#endif /* _PATHLIST_INCLUDED */
