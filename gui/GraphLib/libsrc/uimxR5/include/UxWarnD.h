/* @(#)UxWarnD.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXWarningDialog_INCLUDED
#define UXWarningDialog_INCLUDED

#include "Xm/MessageB.h"

#ifdef DESIGN_TIME
#include "UxMsgBox.h"

extern Class_t UxC_warningDialog;
#define UxCreateWarningDialog(name,parent) \
        UxCreateSwidget(UxC_warningDialog,name,parent)
#else
#define UxCreateWarningDialog(name,parent) \
        UxCreateDialogSwidget(name,xmMessageBoxWidgetClass,parent)
#endif
#endif
