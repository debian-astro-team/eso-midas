/* @(#)UxScrBar.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXScrollBar_INCLUDED
#define UXScrollBar_INCLUDED

#include "Xm/ScrollBar.h"

#ifdef DESIGN_TIME
#include "UxPrim.h"

#ifndef UxGetValue
extern binptr UxP_ScrollBarRD_value;
#define UxGetValue(sw) \
        UxGET_int(sw,UxP_ScrollBarRD_value,"value")
#define UxPutValue(sw,val) \
        UxPUT_int(sw,UxP_ScrollBarRD_value,"value",val)
#endif

#ifndef UxGetTroughColor
extern binptr UxP_ScrollBarRD_troughColor;
#define UxGetTroughColor(sw) \
        UxGET_string(sw,UxP_ScrollBarRD_troughColor,"troughColor")
#define UxPutTroughColor(sw,val) \
        UxPUT_string(sw,UxP_ScrollBarRD_troughColor,"troughColor",val)
#endif

#ifndef UxGetSliderSize
extern binptr UxP_ScrollBarRD_sliderSize;
#define UxGetSliderSize(sw) \
        UxGET_int(sw,UxP_ScrollBarRD_sliderSize,"sliderSize")
#define UxPutSliderSize(sw,val) \
        UxPUT_int(sw,UxP_ScrollBarRD_sliderSize,"sliderSize",val)
#endif

#ifndef UxGetShowArrows
extern binptr UxP_ScrollBarRD_showArrows;
#define UxGetShowArrows(sw) \
        UxGET_string(sw,UxP_ScrollBarRD_showArrows,"showArrows")
#define UxPutShowArrows(sw,val) \
        UxPUT_string(sw,UxP_ScrollBarRD_showArrows,"showArrows",val)
#endif

#ifndef UxGetRepeatDelay
extern binptr UxP_ScrollBarRD_repeatDelay;
#define UxGetRepeatDelay(sw) \
        UxGET_int(sw,UxP_ScrollBarRD_repeatDelay,"repeatDelay")
#define UxPutRepeatDelay(sw,val) \
        UxPUT_int(sw,UxP_ScrollBarRD_repeatDelay,"repeatDelay",val)
#endif

#ifndef UxGetProcessingDirection
extern binptr UxP_ScrollBarRD_processingDirection;
#define UxGetProcessingDirection(sw) \
        UxGET_string(sw,UxP_ScrollBarRD_processingDirection,"processingDirection")
#define UxPutProcessingDirection(sw,val) \
        UxPUT_string(sw,UxP_ScrollBarRD_processingDirection,"processingDirection",val)
#endif

#ifndef UxGetPageIncrement
extern binptr UxP_ScrollBarRD_pageIncrement;
#define UxGetPageIncrement(sw) \
        UxGET_int(sw,UxP_ScrollBarRD_pageIncrement,"pageIncrement")
#define UxPutPageIncrement(sw,val) \
        UxPUT_int(sw,UxP_ScrollBarRD_pageIncrement,"pageIncrement",val)
#endif

#ifndef UxGetOrientation
extern binptr UxP_ScrollBarRD_orientation;
#define UxGetOrientation(sw) \
        UxGET_string(sw,UxP_ScrollBarRD_orientation,"orientation")
#define UxPutOrientation(sw,val) \
        UxPUT_string(sw,UxP_ScrollBarRD_orientation,"orientation",val)
#endif

#ifndef UxGetMinimum
extern binptr UxP_ScrollBarRD_minimum;
#define UxGetMinimum(sw) \
        UxGET_int(sw,UxP_ScrollBarRD_minimum,"minimum")
#define UxPutMinimum(sw,val) \
        UxPUT_int(sw,UxP_ScrollBarRD_minimum,"minimum",val)
#endif

#ifndef UxGetMaximum
extern binptr UxP_ScrollBarRD_maximum;
#define UxGetMaximum(sw) \
        UxGET_int(sw,UxP_ScrollBarRD_maximum,"maximum")
#define UxPutMaximum(sw,val) \
        UxPUT_int(sw,UxP_ScrollBarRD_maximum,"maximum",val)
#endif

#ifndef UxGetInitialDelay
extern binptr UxP_ScrollBarRD_initialDelay;
#define UxGetInitialDelay(sw) \
        UxGET_int(sw,UxP_ScrollBarRD_initialDelay,"initialDelay")
#define UxPutInitialDelay(sw,val) \
        UxPUT_int(sw,UxP_ScrollBarRD_initialDelay,"initialDelay",val)
#endif

#ifndef UxGetIncrement
extern binptr UxP_ScrollBarRD_increment;
#define UxGetIncrement(sw) \
        UxGET_int(sw,UxP_ScrollBarRD_increment,"increment")
#define UxPutIncrement(sw,val) \
        UxPUT_int(sw,UxP_ScrollBarRD_increment,"increment",val)
#endif

extern Class_t UxC_scrollBar;
#define UxCreateScrollBar(name,parent) \
        UxCreateSwidget(UxC_scrollBar,name,parent)
#else
#define UxCreateScrollBar(name,parent) \
        UxCreateSwidget(name,xmScrollBarWidgetClass,parent)
#endif
#endif
