/* @(#)UxTogBG.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXToggleButtonGadget_INCLUDED
#define UXToggleButtonGadget_INCLUDED

#include "Xm/ToggleBG.h"

#ifdef DESIGN_TIME
#include "UxLabelG.h"

#ifndef UxGetVisibleWhenOff
extern binptr UxP_ToggleButtonGadgetRD_visibleWhenOff;
#define UxGetVisibleWhenOff(sw) \
        UxGET_string(sw,UxP_ToggleButtonGadgetRD_visibleWhenOff,"visibleWhenOff")
#define UxPutVisibleWhenOff(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonGadgetRD_visibleWhenOff,"visibleWhenOff",val)
#endif

#ifndef UxGetSpacing
extern binptr UxP_ToggleButtonGadgetRD_spacing;
#define UxGetSpacing(sw) \
        UxGET_int(sw,UxP_ToggleButtonGadgetRD_spacing,"spacing")
#define UxPutSpacing(sw,val) \
        UxPUT_int(sw,UxP_ToggleButtonGadgetRD_spacing,"spacing",val)
#endif

#ifndef UxGetSet
extern binptr UxP_ToggleButtonGadgetRD_set;
#define UxGetSet(sw) \
        UxGET_string(sw,UxP_ToggleButtonGadgetRD_set,"set")
#define UxPutSet(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonGadgetRD_set,"set",val)
#endif

#ifndef UxGetSelectPixmap
extern binptr UxP_ToggleButtonGadgetRD_selectPixmap;
#define UxGetSelectPixmap(sw) \
        UxGET_string(sw,UxP_ToggleButtonGadgetRD_selectPixmap,"selectPixmap")
#define UxPutSelectPixmap(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonGadgetRD_selectPixmap,"selectPixmap",val)
#endif

#ifndef UxGetSelectInsensitivePixmap
extern binptr UxP_ToggleButtonGadgetRD_selectInsensitivePixmap;
#define UxGetSelectInsensitivePixmap(sw) \
        UxGET_string(sw,UxP_ToggleButtonGadgetRD_selectInsensitivePixmap,"selectInsensitivePixmap")
#define UxPutSelectInsensitivePixmap(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonGadgetRD_selectInsensitivePixmap,"selectInsensitivePixmap",val)
#endif

#ifndef UxGetSelectColor
extern binptr UxP_ToggleButtonGadgetRD_selectColor;
#define UxGetSelectColor(sw) \
        UxGET_string(sw,UxP_ToggleButtonGadgetRD_selectColor,"selectColor")
#define UxPutSelectColor(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonGadgetRD_selectColor,"selectColor",val)
#endif

#ifndef UxGetIndicatorType
extern binptr UxP_ToggleButtonGadgetRD_indicatorType;
#define UxGetIndicatorType(sw) \
        UxGET_string(sw,UxP_ToggleButtonGadgetRD_indicatorType,"indicatorType")
#define UxPutIndicatorType(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonGadgetRD_indicatorType,"indicatorType",val)
#endif

#ifndef UxGetIndicatorSize
extern binptr UxP_ToggleButtonGadgetRD_indicatorSize;
#define UxGetIndicatorSize(sw) \
        UxGET_int(sw,UxP_ToggleButtonGadgetRD_indicatorSize,"indicatorSize")
#define UxPutIndicatorSize(sw,val) \
        UxPUT_int(sw,UxP_ToggleButtonGadgetRD_indicatorSize,"indicatorSize",val)
#endif

#ifndef UxGetIndicatorOn
extern binptr UxP_ToggleButtonGadgetRD_indicatorOn;
#define UxGetIndicatorOn(sw) \
        UxGET_string(sw,UxP_ToggleButtonGadgetRD_indicatorOn,"indicatorOn")
#define UxPutIndicatorOn(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonGadgetRD_indicatorOn,"indicatorOn",val)
#endif

#ifndef UxGetFillOnSelect
extern binptr UxP_ToggleButtonGadgetRD_fillOnSelect;
#define UxGetFillOnSelect(sw) \
        UxGET_string(sw,UxP_ToggleButtonGadgetRD_fillOnSelect,"fillOnSelect")
#define UxPutFillOnSelect(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonGadgetRD_fillOnSelect,"fillOnSelect",val)
#endif

extern Class_t UxC_toggleButtonGadget;
#define UxCreateToggleButtonGadget(name,parent) \
        UxCreateSwidget(UxC_toggleButtonGadget,name,parent)
#else
#define UxCreateToggleButtonGadget(name,parent) \
        UxCreateSwidget(name,xmToggleButtonGadgetClass,parent)
#endif
#endif
