/* @(#)UxCore.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXCore_INCLUDED
#define UXCore_INCLUDED

#include "Xm/Xm.h"

#ifdef DESIGN_TIME
#include "UxRectO.h"

#ifndef UxGetTranslations
extern binptr UxP_CoreRD_translations;
#define UxGetTranslations(sw) \
        UxGET_string(sw,UxP_CoreRD_translations,"translations")
#define UxPutTranslations(sw,val) \
        UxPUT_string(sw,UxP_CoreRD_translations,"translations",val)
#endif

#ifndef UxGetMappedWhenManaged
extern binptr UxP_CoreRD_mappedWhenManaged;
#define UxGetMappedWhenManaged(sw) \
        UxGET_string(sw,UxP_CoreRD_mappedWhenManaged,"mappedWhenManaged")
#define UxPutMappedWhenManaged(sw,val) \
        UxPUT_string(sw,UxP_CoreRD_mappedWhenManaged,"mappedWhenManaged",val)
#endif

#ifndef UxGetColormap
extern binptr UxP_CoreRD_colormap;
#define UxGetColormap(sw) \
        UxGET_long(sw,UxP_CoreRD_colormap,"colormap")
#define UxPutColormap(sw,val) \
        UxPUT_long(sw,UxP_CoreRD_colormap,"colormap",val)
#endif

#ifndef UxGetBorderPixmap
extern binptr UxP_CoreRD_borderPixmap;
#define UxGetBorderPixmap(sw) \
        UxGET_string(sw,UxP_CoreRD_borderPixmap,"borderPixmap")
#define UxPutBorderPixmap(sw,val) \
        UxPUT_string(sw,UxP_CoreRD_borderPixmap,"borderPixmap",val)
#endif

#ifndef UxGetBorderColor
extern binptr UxP_CoreRD_borderColor;
#define UxGetBorderColor(sw) \
        UxGET_string(sw,UxP_CoreRD_borderColor,"borderColor")
#define UxPutBorderColor(sw,val) \
        UxPUT_string(sw,UxP_CoreRD_borderColor,"borderColor",val)
#endif

#ifndef UxGetBackgroundPixmap
extern binptr UxP_CoreRD_backgroundPixmap;
#define UxGetBackgroundPixmap(sw) \
        UxGET_string(sw,UxP_CoreRD_backgroundPixmap,"backgroundPixmap")
#define UxPutBackgroundPixmap(sw,val) \
        UxPUT_string(sw,UxP_CoreRD_backgroundPixmap,"backgroundPixmap",val)
#endif

#ifndef UxGetBackground
extern binptr UxP_CoreRD_background;
#define UxGetBackground(sw) \
        UxGET_string(sw,UxP_CoreRD_background,"background")
#define UxPutBackground(sw,val) \
        UxPUT_string(sw,UxP_CoreRD_background,"background",val)
#endif

#ifndef UxGetAccelerators
extern binptr UxP_CoreRD_accelerators;
#define UxGetAccelerators(sw) \
        UxGET_string(sw,UxP_CoreRD_accelerators,"accelerators")
#define UxPutAccelerators(sw,val) \
        UxPUT_string(sw,UxP_CoreRD_accelerators,"accelerators",val)
#endif

extern Class_t UxC_Core;
#define UxCreateCore(name,parent) \
        UxCreateSwidget(UxC_Core,name,parent)
#else
#define UxCreateCore(name,parent) \
        UxCreateSwidget(name,widgetClass,parent)
#endif
#endif
