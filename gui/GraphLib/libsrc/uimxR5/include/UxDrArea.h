/* @(#)UxDrArea.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXDrawingArea_INCLUDED
#define UXDrawingArea_INCLUDED

#include "Xm/DrawingA.h"

#ifdef DESIGN_TIME
#include "UxMgr.h"

#ifndef UxGetResizePolicy
extern binptr UxP_DrawingAreaRD_resizePolicy;
#define UxGetResizePolicy(sw) \
        UxGET_string(sw,UxP_DrawingAreaRD_resizePolicy,"resizePolicy")
#define UxPutResizePolicy(sw,val) \
        UxPUT_string(sw,UxP_DrawingAreaRD_resizePolicy,"resizePolicy",val)
#endif

#ifndef UxGetMarginWidth
extern binptr UxP_DrawingAreaRD_marginWidth;
#define UxGetMarginWidth(sw) \
        UxGET_int(sw,UxP_DrawingAreaRD_marginWidth,"marginWidth")
#define UxPutMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_DrawingAreaRD_marginWidth,"marginWidth",val)
#endif

#ifndef UxGetMarginHeight
extern binptr UxP_DrawingAreaRD_marginHeight;
#define UxGetMarginHeight(sw) \
        UxGET_int(sw,UxP_DrawingAreaRD_marginHeight,"marginHeight")
#define UxPutMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_DrawingAreaRD_marginHeight,"marginHeight",val)
#endif

extern Class_t UxC_drawingArea;
#define UxCreateDrawingArea(name,parent) \
        UxCreateSwidget(UxC_drawingArea,name,parent)
#else
#define UxCreateDrawingArea(name,parent) \
        UxCreateSwidget(name,xmDrawingAreaWidgetClass,parent)
#endif
#endif
