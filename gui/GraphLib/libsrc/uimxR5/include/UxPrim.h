/* @(#)UxPrim.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXPrimitive_INCLUDED
#define UXPrimitive_INCLUDED

#include "Xm/Xm.h"

#ifdef DESIGN_TIME
#include "UxCore.h"

#ifndef UxGetUserData
extern binptr UxP_PrimitiveRD_userData;
#define UxGetUserData(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_userData,"userData")
#define UxPutUserData(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_userData,"userData",val)
#endif

#ifndef UxGetUnitType
extern binptr UxP_PrimitiveRD_unitType;
#define UxGetUnitType(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_unitType,"unitType")
#define UxPutUnitType(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_unitType,"unitType",val)
#endif

#ifndef UxGetTraversalOn
extern binptr UxP_PrimitiveRD_traversalOn;
#define UxGetTraversalOn(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_traversalOn,"traversalOn")
#define UxPutTraversalOn(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_traversalOn,"traversalOn",val)
#endif

#ifndef UxGetTopShadowPixmap
extern binptr UxP_PrimitiveRD_topShadowPixmap;
#define UxGetTopShadowPixmap(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_topShadowPixmap,"topShadowPixmap")
#define UxPutTopShadowPixmap(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_topShadowPixmap,"topShadowPixmap",val)
#endif

#ifndef UxGetTopShadowColor
extern binptr UxP_PrimitiveRD_topShadowColor;
#define UxGetTopShadowColor(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_topShadowColor,"topShadowColor")
#define UxPutTopShadowColor(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_topShadowColor,"topShadowColor",val)
#endif

#ifndef UxGetShadowThickness
extern binptr UxP_PrimitiveRD_shadowThickness;
#define UxGetShadowThickness(sw) \
        UxGET_int(sw,UxP_PrimitiveRD_shadowThickness,"shadowThickness")
#define UxPutShadowThickness(sw,val) \
        UxPUT_int(sw,UxP_PrimitiveRD_shadowThickness,"shadowThickness",val)
#endif

#ifndef UxGetNavigationType
extern binptr UxP_PrimitiveRD_navigationType;
#define UxGetNavigationType(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_navigationType,"navigationType")
#define UxPutNavigationType(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_navigationType,"navigationType",val)
#endif

#ifndef UxGetHighlightThickness
extern binptr UxP_PrimitiveRD_highlightThickness;
#define UxGetHighlightThickness(sw) \
        UxGET_int(sw,UxP_PrimitiveRD_highlightThickness,"highlightThickness")
#define UxPutHighlightThickness(sw,val) \
        UxPUT_int(sw,UxP_PrimitiveRD_highlightThickness,"highlightThickness",val)
#endif

#ifndef UxGetHighlightPixmap
extern binptr UxP_PrimitiveRD_highlightPixmap;
#define UxGetHighlightPixmap(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_highlightPixmap,"highlightPixmap")
#define UxPutHighlightPixmap(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_highlightPixmap,"highlightPixmap",val)
#endif

#ifndef UxGetHighlightOnEnter
extern binptr UxP_PrimitiveRD_highlightOnEnter;
#define UxGetHighlightOnEnter(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_highlightOnEnter,"highlightOnEnter")
#define UxPutHighlightOnEnter(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_highlightOnEnter,"highlightOnEnter",val)
#endif

#ifndef UxGetHighlightColor
extern binptr UxP_PrimitiveRD_highlightColor;
#define UxGetHighlightColor(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_highlightColor,"highlightColor")
#define UxPutHighlightColor(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_highlightColor,"highlightColor",val)
#endif

#ifndef UxGetForeground
extern binptr UxP_PrimitiveRD_foreground;
#define UxGetForeground(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_foreground,"foreground")
#define UxPutForeground(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_foreground,"foreground",val)
#endif

#ifndef UxGetBottomShadowPixmap
extern binptr UxP_PrimitiveRD_bottomShadowPixmap;
#define UxGetBottomShadowPixmap(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_bottomShadowPixmap,"bottomShadowPixmap")
#define UxPutBottomShadowPixmap(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_bottomShadowPixmap,"bottomShadowPixmap",val)
#endif

#ifndef UxGetBottomShadowColor
extern binptr UxP_PrimitiveRD_bottomShadowColor;
#define UxGetBottomShadowColor(sw) \
        UxGET_string(sw,UxP_PrimitiveRD_bottomShadowColor,"bottomShadowColor")
#define UxPutBottomShadowColor(sw,val) \
        UxPUT_string(sw,UxP_PrimitiveRD_bottomShadowColor,"bottomShadowColor",val)
#endif

extern Class_t UxC_primitive;
#define UxCreatePrimitive(name,parent) \
        UxCreateSwidget(UxC_primitive,name,parent)
#else
#define UxCreatePrimitive(name,parent) \
        UxCreateSwidget(name,xmPrimitiveWidgetClass,parent)
#endif
#endif
