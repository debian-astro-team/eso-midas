/* @(#)UxMgr.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
#ifndef UXManager_INCLUDED
#define UXManager_INCLUDED

#include "Xm/Xm.h"

#ifdef DESIGN_TIME
#include "UxComp.h"

#ifndef UxGetUserData
extern binptr UxP_ManagerRD_userData;
#define UxGetUserData(sw) \
        UxGET_string(sw,UxP_ManagerRD_userData,"userData")
#define UxPutUserData(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_userData,"userData",val)
#endif

#ifndef UxGetUnitType
extern binptr UxP_ManagerRD_unitType;
#define UxGetUnitType(sw) \
        UxGET_string(sw,UxP_ManagerRD_unitType,"unitType")
#define UxPutUnitType(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_unitType,"unitType",val)
#endif

#ifndef UxGetTraversalOn
extern binptr UxP_ManagerRD_traversalOn;
#define UxGetTraversalOn(sw) \
        UxGET_string(sw,UxP_ManagerRD_traversalOn,"traversalOn")
#define UxPutTraversalOn(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_traversalOn,"traversalOn",val)
#endif

#ifndef UxGetTopShadowPixmap
extern binptr UxP_ManagerRD_topShadowPixmap;
#define UxGetTopShadowPixmap(sw) \
        UxGET_string(sw,UxP_ManagerRD_topShadowPixmap,"topShadowPixmap")
#define UxPutTopShadowPixmap(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_topShadowPixmap,"topShadowPixmap",val)
#endif

#ifndef UxGetTopShadowColor
extern binptr UxP_ManagerRD_topShadowColor;
#define UxGetTopShadowColor(sw) \
        UxGET_string(sw,UxP_ManagerRD_topShadowColor,"topShadowColor")
#define UxPutTopShadowColor(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_topShadowColor,"topShadowColor",val)
#endif

#ifndef UxGetStringDirection
extern binptr UxP_ManagerRD_stringDirection;
#define UxGetStringDirection(sw) \
        UxGET_string(sw,UxP_ManagerRD_stringDirection,"stringDirection")
#define UxPutStringDirection(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_stringDirection,"stringDirection",val)
#endif

#ifndef UxGetShadowThickness
extern binptr UxP_ManagerRD_shadowThickness;
#define UxGetShadowThickness(sw) \
        UxGET_int(sw,UxP_ManagerRD_shadowThickness,"shadowThickness")
#define UxPutShadowThickness(sw,val) \
        UxPUT_int(sw,UxP_ManagerRD_shadowThickness,"shadowThickness",val)
#endif

#ifndef UxGetNavigationType
extern binptr UxP_ManagerRD_navigationType;
#define UxGetNavigationType(sw) \
        UxGET_string(sw,UxP_ManagerRD_navigationType,"navigationType")
#define UxPutNavigationType(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_navigationType,"navigationType",val)
#endif

#ifndef UxGetInitialFocus
extern binptr UxP_ManagerRD_initialFocus;
#define UxGetInitialFocus(sw) \
        UxGET_string(sw,UxP_ManagerRD_initialFocus,"initialFocus")
#define UxPutInitialFocus(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_initialFocus,"initialFocus",val)
#endif

#ifndef UxGetHighlightPixmap
extern binptr UxP_ManagerRD_highlightPixmap;
#define UxGetHighlightPixmap(sw) \
        UxGET_string(sw,UxP_ManagerRD_highlightPixmap,"highlightPixmap")
#define UxPutHighlightPixmap(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_highlightPixmap,"highlightPixmap",val)
#endif

#ifndef UxGetHighlightColor
extern binptr UxP_ManagerRD_highlightColor;
#define UxGetHighlightColor(sw) \
        UxGET_string(sw,UxP_ManagerRD_highlightColor,"highlightColor")
#define UxPutHighlightColor(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_highlightColor,"highlightColor",val)
#endif

#ifndef UxGetForeground
extern binptr UxP_ManagerRD_foreground;
#define UxGetForeground(sw) \
        UxGET_string(sw,UxP_ManagerRD_foreground,"foreground")
#define UxPutForeground(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_foreground,"foreground",val)
#endif

#ifndef UxGetBottomShadowPixmap
extern binptr UxP_ManagerRD_bottomShadowPixmap;
#define UxGetBottomShadowPixmap(sw) \
        UxGET_string(sw,UxP_ManagerRD_bottomShadowPixmap,"bottomShadowPixmap")
#define UxPutBottomShadowPixmap(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_bottomShadowPixmap,"bottomShadowPixmap",val)
#endif

#ifndef UxGetBottomShadowColor
extern binptr UxP_ManagerRD_bottomShadowColor;
#define UxGetBottomShadowColor(sw) \
        UxGET_string(sw,UxP_ManagerRD_bottomShadowColor,"bottomShadowColor")
#define UxPutBottomShadowColor(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_bottomShadowColor,"bottomShadowColor",val)
#endif

#ifndef UxGetAllowShellResize
extern binptr UxP_ManagerRD_allowShellResize;
#define UxGetAllowShellResize(sw) \
        UxGET_string(sw,UxP_ManagerRD_allowShellResize,"allowShellResize")
#define UxPutAllowShellResize(sw,val) \
        UxPUT_string(sw,UxP_ManagerRD_allowShellResize,"allowShellResize",val)
#endif

extern Class_t UxC_manager;
#define UxCreateManager(name,parent) \
        UxCreateSwidget(UxC_manager,name,parent)
#else
#define UxCreateManager(name,parent) \
        UxCreateSwidget(name,xmManagerWidgetClass,parent)
#endif
#endif
