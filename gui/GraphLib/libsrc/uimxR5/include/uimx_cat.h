/* @(#)uimx_cat.h	17.1 (ESO-IPG) 01/25/02 17:26:05 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1990, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef	_UIMXCAT_H_
#define	_UIMXCAT_H_

#include "uxproto.h"

#ifdef  XOPEN_CATALOG
/* If XOPEN_CATALOG is defined, we use the message catalog */

#ifdef DESIGN_TIME
#define CAT_FILE	"uimx2_5.cat"
#endif
#ifdef RUNTIME
#define	CAT_FILE	"libuimx2_5.cat"
#endif
#ifdef UTILITIES
#define	CAT_FILE	"uxutils2_5.cat"
#endif


extern int	UxCatOpen UXPROTO(( void ));
extern void	UxCatClose UXPROTO(( void ));
extern char	*UxCatGets UXPROTO(( int, int, char * ));

#define UxCatGetc(setnum,msgnum,default) *UxCatGets(setnum,msgnum,default)

#else /* not using message catalog */

#define UxCatOpen()	0
#define UxCatClose()
#define UxCatGets(setnum,msgnum,default) (default)
#define UxCatGetc(setnum,msgnum,default) *(default)

#endif /* XOPEN_CATALOG */

#if defined(__STDC__) && !defined(apollo)
#define PASTE(token1,token2)		token1##token2
#define PASTE3(token1,token2,token3)	token1##token2##token3
#else
#define PASTE(token1,token2)		token1/**/token2
#define PASTE3(token1,token2,token3)	token1/**/token2/**/token3
#endif

#define UXCATGETS(setnum,msg_prefix,msg) \
	UxCatGets(setnum,PASTE(msg_prefix,msg),PASTE3(DS_,msg_prefix,msg))

#ifdef CD_DEMO
#	include "systemDM_ds.h"
#else /* CD_DEMO */
#	include "system_ds.h"
#endif /* CD_DEMO */


#endif /* _UIMXCAT_H_ */

