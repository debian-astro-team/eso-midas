/* @(#)valloc.h	17.1 (ESO-IPG) 01/25/02 17:26:06 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _VALLOC_INCLUDED
#define _VALLOC_INCLUDED

#include "uxproto.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern void	*UxMalloc UXPROTO(( unsigned int ));
extern void	*UxCalloc UXPROTO(( unsigned int, unsigned int ));
extern void	*UxRealloc UXPROTO(( void *, unsigned int ));
extern void	UxFree UXPROTO(( void * ));
extern char	*UxCopyString UXPROTO(( char * )); 

#define UxNEW(type)	((type*) UxMalloc(sizeof(type)))

#ifdef __cplusplus
}  /* Close scope of 'extern "C"' declaration which encloses file. */
#endif /* __cplusplus */

	
#endif /* _VALLOC_INCLUDED */
