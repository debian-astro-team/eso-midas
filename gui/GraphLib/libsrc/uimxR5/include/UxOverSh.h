/* @(#)UxOverSh.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXOverrideShell_INCLUDED
#define UXOverrideShell_INCLUDED

#include "X11/Shell.h"

#ifdef DESIGN_TIME
#include "UxShell.h"

extern Class_t UxC_overrideShell;
#define UxCreateOverrideShell(name,parent) \
        UxCreateSwidget(UxC_overrideShell,name,parent)
#else
#define UxCreateOverrideShell(name,parent) \
        UxCreateSwidget(name,overrideShellWidgetClass,parent)
#endif
#endif
