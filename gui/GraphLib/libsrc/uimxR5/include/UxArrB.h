/* @(#)UxArrB.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
#ifndef UXArrowButton_INCLUDED
#define UXArrowButton_INCLUDED

#include "Xm/ArrowB.h"

#ifdef DESIGN_TIME
#include "UxPrim.h"

#ifndef UxGetMultiClick
extern binptr UxP_ArrowButtonRD_multiClick;
#define UxGetMultiClick(sw) \
        UxGET_string(sw,UxP_ArrowButtonRD_multiClick,"multiClick")
#define UxPutMultiClick(sw,val) \
        UxPUT_string(sw,UxP_ArrowButtonRD_multiClick,"multiClick",val)
#endif

#ifndef UxGetArrowDirection
extern binptr UxP_ArrowButtonRD_arrowDirection;
#define UxGetArrowDirection(sw) \
        UxGET_string(sw,UxP_ArrowButtonRD_arrowDirection,"arrowDirection")
#define UxPutArrowDirection(sw,val) \
        UxPUT_string(sw,UxP_ArrowButtonRD_arrowDirection,"arrowDirection",val)
#endif

extern Class_t UxC_arrowButton;
#define UxCreateArrowButton(name,parent) \
        UxCreateSwidget(UxC_arrowButton,name,parent)
#else
#define UxCreateArrowButton(name,parent) \
        UxCreateSwidget(name,xmArrowButtonWidgetClass,parent)
#endif
#endif
