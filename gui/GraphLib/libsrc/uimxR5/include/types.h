/* @(#)types.h	17.1 (ESO-IPG) 01/25/02 17:26:05 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _TYPES_INCLUDED
#define _TYPES_INCLUDED

#include "uxproto.h"

#define NO_TYPE 0
#define RANGE 1
#define VALUES 2

typedef struct {
	float min;
	int has_min;
	float max;
	int has_max;
} Range;

typedef struct {
	int num_values;
	char **values;
} Values;

typedef struct {
	char *name;
	unsigned int size;
	int interp_type;
	int domain_type;
	union {
		Range range;
		Values values;
	} domain;
} UIMXType;

typedef struct {
	char *name;
	unsigned int size;
	int domain_type;
	union {
		Range range;
		Values values;
	} domain;
} XType;

typedef struct {
	char **strings;
	unsigned char *values;
	int size;
} conversion;

#define BLOCK_SIZE 100

#define u_name(tp) UxUIMX_types[tp]->name
#define u_size(tp) UxUIMX_types[tp]->size
#define u_interp_type(tp) UxUIMX_types[tp]->interp_type
#define u_domain_type(tp) UxUIMX_types[tp]->domain_type
#define u_num_values(tp) UxUIMX_types[tp]->domain.values.num_values
#define u_values(tp) UxUIMX_types[tp]->domain.values.values
#define u_min(tp) UxUIMX_types[tp]->domain.range.min
#define u_has_min(tp) UxUIMX_types[tp]->domain.range.has_min
#define u_max(tp) UxUIMX_types[tp]->domain.range.max
#define u_has_max(tp) UxUIMX_types[tp]->domain.range.has_max
#define x_name(tp) UxX_types[tp]->name
#define x_size(tp) UxX_types[tp]->size
#define x_domain_type(tp) UxX_types[tp]->domain_type
#define x_num_values(tp) UxX_types[tp]->domain.values.num_values
#define x_values(tp) UxX_types[tp]->domain.values.values
#define x_min(tp) UxX_types[tp]->domain.range.min
#define x_has_min(tp) UxX_types[tp]->domain.range.has_min
#define x_max(tp) UxX_types[tp]->domain.range.max
#define x_has_max(tp) UxX_types[tp]->domain.range.has_max

typedef int (*UIMX_conv)();
extern UIMX_conv **UxUimx_x;
extern UIMXType **UxUIMX_types;
extern XType **UxX_types;
extern conversion *UxStr_conv;

#define UIMX_TYPE 0
#define X_TYPE 1

extern int UxAdd_utype UXPROTO((char *name, int size, int interp_type));
extern int UxAddXtype UXPROTO((char *name, int size));
extern void UxAddConv UXPROTO((int utypei, int xtypei, UIMX_conv conv));
extern void UxAdd_values UXPROTO((int x_or_uimx, int type, int num_values, char **values));
extern void UxAddXvalues UXPROTO((int type, char **uvals, unsigned char *xvals, int size));

#endif /* _TYPES_INCLUDED */
