/* @(#)UxSepG.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXSeparatorGadget_INCLUDED
#define UXSeparatorGadget_INCLUDED

#include "Xm/SeparatoG.h"

#ifdef DESIGN_TIME
#include "UxGadget.h"

#ifndef UxGetSeparatorType
extern binptr UxP_SeparatorGadgetRD_separatorType;
#define UxGetSeparatorType(sw) \
        UxGET_string(sw,UxP_SeparatorGadgetRD_separatorType,"separatorType")
#define UxPutSeparatorType(sw,val) \
        UxPUT_string(sw,UxP_SeparatorGadgetRD_separatorType,"separatorType",val)
#endif

#ifndef UxGetOrientation
extern binptr UxP_SeparatorGadgetRD_orientation;
#define UxGetOrientation(sw) \
        UxGET_string(sw,UxP_SeparatorGadgetRD_orientation,"orientation")
#define UxPutOrientation(sw,val) \
        UxPUT_string(sw,UxP_SeparatorGadgetRD_orientation,"orientation",val)
#endif

#ifndef UxGetMargin
extern binptr UxP_SeparatorGadgetRD_margin;
#define UxGetMargin(sw) \
        UxGET_int(sw,UxP_SeparatorGadgetRD_margin,"margin")
#define UxPutMargin(sw,val) \
        UxPUT_int(sw,UxP_SeparatorGadgetRD_margin,"margin",val)
#endif

extern Class_t UxC_separatorGadget;
#define UxCreateSeparatorGadget(name,parent) \
        UxCreateSwidget(UxC_separatorGadget,name,parent)
#else
#define UxCreateSeparatorGadget(name,parent) \
        UxCreateSwidget(name,xmSeparatorGadgetClass,parent)
#endif
#endif
