/* @(#)UxArrBG.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
#ifndef UXArrowButtonGadget_INCLUDED
#define UXArrowButtonGadget_INCLUDED

#include "Xm/ArrowBG.h"

#ifdef DESIGN_TIME
#include "UxGadget.h"

#ifndef UxGetMultiClick
extern binptr UxP_ArrowButtonGadgetRD_multiClick;
#define UxGetMultiClick(sw) \
        UxGET_string(sw,UxP_ArrowButtonGadgetRD_multiClick,"multiClick")
#define UxPutMultiClick(sw,val) \
        UxPUT_string(sw,UxP_ArrowButtonGadgetRD_multiClick,"multiClick",val)
#endif

#ifndef UxGetArrowDirection
extern binptr UxP_ArrowButtonGadgetRD_arrowDirection;
#define UxGetArrowDirection(sw) \
        UxGET_string(sw,UxP_ArrowButtonGadgetRD_arrowDirection,"arrowDirection")
#define UxPutArrowDirection(sw,val) \
        UxPUT_string(sw,UxP_ArrowButtonGadgetRD_arrowDirection,"arrowDirection",val)
#endif

extern Class_t UxC_arrowButtonGadget;
#define UxCreateArrowButtonGadget(name,parent) \
        UxCreateSwidget(UxC_arrowButtonGadget,name,parent)
#else
#define UxCreateArrowButtonGadget(name,parent) \
        UxCreateSwidget(name,xmArrowButtonGadgetClass,parent)
#endif
#endif
