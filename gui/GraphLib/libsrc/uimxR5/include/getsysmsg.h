/*********************************************************************
 
 $Date: 2009-08-26 13:05:00 $
 $Revision: 1.2 $

.VERSION
 090813		last modif KB

*********************************************************************/

/*****************************************************************************/
/***                                                                       ***/
/***             Copyright (c) 1991, Visual Edge Software Ltd.             ***/
/***             							   ***/
/***                                                                       ***/
/***   All rights reserved.  This notice is  intended  as  a  precaution   ***/
/***   against  inadvertent publication, and shall not be deemed to con-   ***/
/***   stitute an acknowledgment that publication has  occurred  nor  to   ***/
/***   imply  any  waiver  of confidentiality.  The year included in the   ***/
/***   notice is the year of the creation of the work.                     ***/
/***                                                                       ***/
/*****************************************************************************/

/*---------------------------------------------------------------------------
 * Declarations for getting system messages from resources.
 *---------------------------------------------------------------------------*/

#ifndef	_GETSYSMSG_H_
#define	_GETSYSMSG_H_

/* needed include files. */
#include "uxproto.h"

/* Macro Symbols */

/* Resource to look at. */
#define SM_U_TASKNAME		0
#define SM_TASKNAME		1
#define SM_U_TASKDIR		2
#define SM_TASKDIR		3
#define SM_RESOURCE_VAR		4

/* External function declarations */
char * UxGetSysMessage UXPROTO(( int, char* ));

#endif /* _GETSYSMSG_H_ */
