/* @(#)UxPrompD.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXPromptDialog_INCLUDED
#define UXPromptDialog_INCLUDED

#include "Xm/SelectioB.h"

#ifdef DESIGN_TIME
#include "UxSelBox.h"

extern Class_t UxC_promptDialog;
#define UxCreatePromptDialog(name,parent) \
        UxCreateSwidget(UxC_promptDialog,name,parent)
#else
#define UxCreatePromptDialog(name,parent) \
        UxCreateDialogSwidget(name,xmSelectionBoxWidgetClass,parent)
#endif
#endif
