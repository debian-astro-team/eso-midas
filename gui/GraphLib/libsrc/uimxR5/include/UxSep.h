/* @(#)UxSep.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXSeparator_INCLUDED
#define UXSeparator_INCLUDED

#include "Xm/Separator.h"

#ifdef DESIGN_TIME
#include "UxPrim.h"

#ifndef UxGetSeparatorType
extern binptr UxP_SeparatorRD_separatorType;
#define UxGetSeparatorType(sw) \
        UxGET_string(sw,UxP_SeparatorRD_separatorType,"separatorType")
#define UxPutSeparatorType(sw,val) \
        UxPUT_string(sw,UxP_SeparatorRD_separatorType,"separatorType",val)
#endif

#ifndef UxGetOrientation
extern binptr UxP_SeparatorRD_orientation;
#define UxGetOrientation(sw) \
        UxGET_string(sw,UxP_SeparatorRD_orientation,"orientation")
#define UxPutOrientation(sw,val) \
        UxPUT_string(sw,UxP_SeparatorRD_orientation,"orientation",val)
#endif

#ifndef UxGetMargin
extern binptr UxP_SeparatorRD_margin;
#define UxGetMargin(sw) \
        UxGET_int(sw,UxP_SeparatorRD_margin,"margin")
#define UxPutMargin(sw,val) \
        UxPUT_int(sw,UxP_SeparatorRD_margin,"margin",val)
#endif

extern Class_t UxC_separator;
#define UxCreateSeparator(name,parent) \
        UxCreateSwidget(UxC_separator,name,parent)
#else
#define UxCreateSeparator(name,parent) \
        UxCreateSwidget(name,xmSeparatorWidgetClass,parent)
#endif
#endif
