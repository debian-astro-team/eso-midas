/* @(#)uxproto.h	17.1 (ESO-IPG) 01/25/02 17:26:06 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *---------------------------------------------------------------------
 *   Contains definitions of macros and types used by all files.
 *-------------------------------------------------------------------*/

#ifndef UXPROTO_H
#define UXPROTO_H


/*--- include files ---*/

/*--- macro symbolic constants ---*/

#ifdef _NO_PROTO
#define UXPROTO(x) ()
#else
#define UXPROTO(x) x
#endif

/*--- macro functions ---*/



/*--- types ---*/
typedef struct object         *Object_t;
typedef struct UxClass_st     *Class_t;

/*  DESIGN_TIME swidget definition */

#ifdef DESIGN_TIME
typedef struct ShadowWidget *swidget;
#endif

typedef int	UxStatus_t;       /* Valid values: [UxERROR, UxNO_ERROR] */

/*-------------------------------------------------------------------
 * PRIVATE_SWIDGET should be defined if the developer must have 
 * access to the internals of the swidget class.
 *-------------------------------------------------------------------
 * Special Case: these includes must go after the typedefs section
 *               in order to satisfy the interpreter.
 *-------------------------------------------------------------------*/
#ifdef PRIVATE_SWIDGET
#include "veos.h"
#include "swidget.h"
#endif /* PRIVATE_SWIDGET */

/*--- external functions ---*/

/*--- external variables ---*/

#endif /* UXPROTO_H */

/*--- end of file ---*/
