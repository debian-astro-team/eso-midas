/*---------------------------------------------------------------------
 * $Date: 2005-05-12 07:32:35 $		$Revision: 1.2 $
 *---------------------------------------------------------------------
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *---------------------------------------------------------------------
 *   File_Description_Section
 *--------------------------------------------------------------------

 050512		last modif
*/

#ifndef SP_PTY_H
#define SP_PTY_H

/*--- include files ---*/
#include "uxproto.h"
#include <signal.h>
#include <setjmp.h>

#ifdef	Cygwin
#include <termio.h>
#endif

#ifdef	SYSV
#include <sys/ioctl.h>
#include <sys/termio.h>
#if !defined(m88k) && !defined(sgi) && !defined(magnum) && !defined(sco) && !defined(dg88) && !defined(ibmr2)
#include <sys/ptyio.h>
#if defined(SIGTSTP) 
#include <sys/bsdtty.h>
#endif	/* SIGTSTP */
#endif	/* (m88k) && (sgi) && (magnum) && (sco) && (dg88) && (ibmr2) */
#endif	/* SYSV */

#if defined(USES_SGTTY_H)
#	include <sys/sgtty.h>
#	include <sys/termio.h>
#endif	/* USES_SGTTY_H */

#if defined(_TEK88) || defined(_MOTOR88)
#	include <sys/pty.h>
#endif   /* _TEK88 */

/* For Solaris, in design time, use BSD library and in runtime use SYSV  */
#if (defined (SOLARIS))
#	ifdef DESIGN_TIME
#		define BSD_COMP
#	endif
#	include <sys/ttold.h>
#	include <sys/ttychars.h>
#	ifdef DESIGN_TIME
#		include <sys/termios.h>
#	endif
#endif

#if defined(__FreeBSD__) || defined(__Darwin__) 
#include <sys/ioctl_compat.h>
#else
#include <sys/ioctl.h>
#endif
 
#include <sys/types.h>
#include <sys/wait.h>

#if !defined(_TEK88) && !defined(_MOTOR88)
#	include <sys/file.h>
#endif  

#include <errno.h>
#include <fcntl.h>

#if defined(sgi) || defined(ibmr2)
#	include <sys/stat.h>
#	include <sys/sysmacros.h>
#endif /* sgi */

/* for DG, use SYSV pty code */
#if defined(dg88)
#	define USE_SYSV_TERMIO
#	define USE_SYSV_PGRP
#endif /* dg88 */

#ifdef __cplusplus
extern "C" {			/* C++ compatability */
#endif /* __cplusplus */

/*--- macro symbolic constants ---*/

/*--- macro functions ---*/

/*--- types ---*/
struct _pty_dirs {
    char *pty_dir;	/* pty slave directory...			*/
    char *ptym_dir;	/* pty master directory...			*/
    char *char_1;	/* string of first characters in pty name...	*/
    char *char_2;	/* string of second characters in pty name...	*/
    char *char_3;	/* string of third characters in pty name...	*/
    int fast;		/* true->skip all if first does not exist...	*/
};

/* WHAT IS MISSING 
 * either a new pty slave name, or a "sorry, no more ptys -- terminate"
 * message.
 */
typedef enum {
	PTY_BAD,	/* c->p: can't open pty slave for some reason	*/
	PTY_GOOD,	/* c->p: we have a good pty, let's go on	*/
	PTY_NEW,	/* p->c: here is a new pty slave, try this	*/
	PTY_NOMORE,	/* p->c: no more pty's, terminate		*/
	SUBPROC_ERROR,	/* c->p: here is an error msg to display	*/
	SUBPROC_EXEC_ERROR
			/* c->p: unable to exec subprocess		*/
} status_t;

typedef struct {
	status_t status;	/* packet type...			*/
	int error;		/* open error...			*/
	char buffer[1024];	/* name of pty slave...			*/
} handshake_t;


/*--- external functions ---*/
extern void UxPtyExitHandler UXPROTO(( void ));
extern int UxGetPtty UXPROTO(( int *pty, int reset ));
extern int UxSetTty UXPROTO(( void ));
extern void UxPtyChildCommSetup UXPROTO(( int tty, int echo ));
extern int UxPtyStartChild UXPROTO(( int pty, int echo, char **args ));
extern int UxSetPseudo UXPROTO(( int pty ));
extern char ** UxPtyPrepareCommand UXPROTO(( char *command ));
extern int UxPtyExecSubprocess UXPROTO(( char *cmd, int echo, int *t_pty, int
                                     *t_pid ));

/*--- external variables ---*/


#ifdef __cplusplus
}  /* Close scope of 'extern "C"' declaration which encloses file. */
#endif /* __cplusplus */

#endif
