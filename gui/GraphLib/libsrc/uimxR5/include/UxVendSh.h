/* @(#)UxVendSh.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXVendorShell_INCLUDED
#define UXVendorShell_INCLUDED

#include "X11/Shell.h"

#ifdef DESIGN_TIME
#include "UxWmSh.h"

#ifndef UxGetUseAsyncGeometry
extern binptr UxP_VendorShellRD_useAsyncGeometry;
#define UxGetUseAsyncGeometry(sw) \
        UxGET_string(sw,UxP_VendorShellRD_useAsyncGeometry,"useAsyncGeometry")
#define UxPutUseAsyncGeometry(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_useAsyncGeometry,"useAsyncGeometry",val)
#endif

#ifndef UxGetTextFontList
extern binptr UxP_VendorShellRD_textFontList;
#define UxGetTextFontList(sw) \
        UxGET_string(sw,UxP_VendorShellRD_textFontList,"textFontList")
#define UxPutTextFontList(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_textFontList,"textFontList",val)
#endif

#ifndef UxGetShellUnitType
extern binptr UxP_VendorShellRD_shellUnitType;
#define UxGetShellUnitType(sw) \
        UxGET_string(sw,UxP_VendorShellRD_shellUnitType,"shellUnitType")
#define UxPutShellUnitType(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_shellUnitType,"shellUnitType",val)
#endif

#ifndef UxGetPreeditType
extern binptr UxP_VendorShellRD_preeditType;
#define UxGetPreeditType(sw) \
        UxGET_string(sw,UxP_VendorShellRD_preeditType,"preeditType")
#define UxPutPreeditType(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_preeditType,"preeditType",val)
#endif

#ifndef UxGetMwmMenu
extern binptr UxP_VendorShellRD_mwmMenu;
#define UxGetMwmMenu(sw) \
        UxGET_string(sw,UxP_VendorShellRD_mwmMenu,"mwmMenu")
#define UxPutMwmMenu(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_mwmMenu,"mwmMenu",val)
#endif

#ifndef UxGetMwmInputMode
extern binptr UxP_VendorShellRD_mwmInputMode;
#define UxGetMwmInputMode(sw) \
        UxGET_string(sw,UxP_VendorShellRD_mwmInputMode,"mwmInputMode")
#define UxPutMwmInputMode(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_mwmInputMode,"mwmInputMode",val)
#endif

#ifndef UxGetMwmFunctions
extern binptr UxP_VendorShellRD_mwmFunctions;
#define UxGetMwmFunctions(sw) \
        UxGET_int(sw,UxP_VendorShellRD_mwmFunctions,"mwmFunctions")
#define UxPutMwmFunctions(sw,val) \
        UxPUT_int(sw,UxP_VendorShellRD_mwmFunctions,"mwmFunctions",val)
#endif

#ifndef UxGetMwmDecorations
extern binptr UxP_VendorShellRD_mwmDecorations;
#define UxGetMwmDecorations(sw) \
        UxGET_int(sw,UxP_VendorShellRD_mwmDecorations,"mwmDecorations")
#define UxPutMwmDecorations(sw,val) \
        UxPUT_int(sw,UxP_VendorShellRD_mwmDecorations,"mwmDecorations",val)
#endif

#ifndef UxGetLabelFontList
extern binptr UxP_VendorShellRD_labelFontList;
#define UxGetLabelFontList(sw) \
        UxGET_string(sw,UxP_VendorShellRD_labelFontList,"labelFontList")
#define UxPutLabelFontList(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_labelFontList,"labelFontList",val)
#endif

#ifndef UxGetKeyboardFocusPolicy
extern binptr UxP_VendorShellRD_keyboardFocusPolicy;
#define UxGetKeyboardFocusPolicy(sw) \
        UxGET_string(sw,UxP_VendorShellRD_keyboardFocusPolicy,"keyboardFocusPolicy")
#define UxPutKeyboardFocusPolicy(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_keyboardFocusPolicy,"keyboardFocusPolicy",val)
#endif

#ifndef UxGetInputMethod
extern binptr UxP_VendorShellRD_inputMethod;
#define UxGetInputMethod(sw) \
        UxGET_string(sw,UxP_VendorShellRD_inputMethod,"inputMethod")
#define UxPutInputMethod(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_inputMethod,"inputMethod",val)
#endif

#ifndef UxPutDefaultFontList
extern binptr UxP_VendorShellRD_defaultFontList;
#define UxPutDefaultFontList(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_defaultFontList,"defaultFontList",val)
#endif

#ifndef UxGetDeleteResponse
extern binptr UxP_VendorShellRD_deleteResponse;
#define UxGetDeleteResponse(sw) \
        UxGET_string(sw,UxP_VendorShellRD_deleteResponse,"deleteResponse")
#define UxPutDeleteResponse(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_deleteResponse,"deleteResponse",val)
#endif

#ifndef UxGetButtonFontList
extern binptr UxP_VendorShellRD_buttonFontList;
#define UxGetButtonFontList(sw) \
        UxGET_string(sw,UxP_VendorShellRD_buttonFontList,"buttonFontList")
#define UxPutButtonFontList(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_buttonFontList,"buttonFontList",val)
#endif

#ifndef UxGetAudibleWarning
extern binptr UxP_VendorShellRD_audibleWarning;
#define UxGetAudibleWarning(sw) \
        UxGET_string(sw,UxP_VendorShellRD_audibleWarning,"audibleWarning")
#define UxPutAudibleWarning(sw,val) \
        UxPUT_string(sw,UxP_VendorShellRD_audibleWarning,"audibleWarning",val)
#endif

extern Class_t UxC_vendorShell;
#define UxCreateVendorShell(name,parent) \
        UxCreateSwidget(UxC_vendorShell,name,parent)
#else
#define UxCreateVendorShell(name,parent) \
        UxCreateSwidget(name,vendorShellWidgetClass,parent)
#endif
#endif
