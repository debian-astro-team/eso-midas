/* @(#)UxTopSh.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXTopLevelShell_INCLUDED
#define UXTopLevelShell_INCLUDED

#include "X11/Shell.h"

#ifdef DESIGN_TIME
#include "UxVendSh.h"

#ifndef UxGetIconNameEncoding
extern binptr UxP_TopLevelShellRD_iconNameEncoding;
#define UxGetIconNameEncoding(sw) \
        UxGET_string(sw,UxP_TopLevelShellRD_iconNameEncoding,"iconNameEncoding")
#define UxPutIconNameEncoding(sw,val) \
        UxPUT_string(sw,UxP_TopLevelShellRD_iconNameEncoding,"iconNameEncoding",val)
#endif

#ifndef UxGetIconName
extern binptr UxP_TopLevelShellRD_iconName;
#define UxGetIconName(sw) \
        UxGET_string(sw,UxP_TopLevelShellRD_iconName,"iconName")
#define UxPutIconName(sw,val) \
        UxPUT_string(sw,UxP_TopLevelShellRD_iconName,"iconName",val)
#endif

#ifndef UxGetIconic
extern binptr UxP_TopLevelShellRD_iconic;
#define UxGetIconic(sw) \
        UxGET_string(sw,UxP_TopLevelShellRD_iconic,"iconic")
#define UxPutIconic(sw,val) \
        UxPUT_string(sw,UxP_TopLevelShellRD_iconic,"iconic",val)
#endif

extern Class_t UxC_topLevelShell;
#define UxCreateTopLevelShell(name,parent) \
        UxCreateSwidget(UxC_topLevelShell,name,parent)
#else
#define UxCreateTopLevelShell(name,parent) \
        UxCreateSwidget(name,topLevelShellWidgetClass,parent)
#endif
#endif
