/* @(#)subproc.h	19.1 (ESO-IPG) 02/25/03 13:41:47 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $		$Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *---------------------------------------------------------------------
 *   File_Description_Section
 *--------------------------------------------------------------------*/

#ifndef SUBPROC_H
#define SUBPROC_H

/*--- include files ---*/
#include "uxproto.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>
/*
#include <malloc.h>
*/
#include <valloc.h>

#include <X11/Intrinsic.h>
#include <X11/X.h>

#include "UxSubproc.h"
#include "uimx_cat.h"
#include "misc1_ds.h"
#include "global.h"

#ifdef __cplusplus
extern "C" {			/* C++ compatability */
#endif /* __cplusplus */

/*--- macro symbolic constants ---*/

#ifdef RUNTIME
#define UxError(msg) UxStandardError((msg))
#endif /* RUNTIME */

	/* Provide 32 entries which is the maximum number of
         * of tty/pty pairs for one process on most extended 
	 * architectures. Pseudo terminals are only in use if
	 * a subprocess is between a Run - Exit cycle. Double
	 * the number of subprocess structures should suffice.
         * Refered to in the functions UxDeleteSubproc and GetSp.
	 */
#define MAX_SUBPROC	64

	/* These are the possible values for the field subprocStatus
	 * in the structure SubprocInfo_t. 
	 */
#define UNDEFINED	-1 	/* the process state is unknown */
#define RUNNING		 0	/* the process is running */
#define EXITING		 1	/* the process is exiting */
#define DEAD		 2	/* the process is dead */
#define DELAYED_EXIT	 3	/* the process is in a delayed exit state */

#ifdef	SYSV
# 	define	USE_SYSV_TERMIO
#	define	USE_SYSV_PGRP
#endif	/* SYSV */

/*--- macro functions ---*/
/* X/OPEN message catalog macros. These will make the code more compact. */
#define CGETS(ms,ds_ms)                 UxCatGets(MC_MISC1,ms,ds_ms)
#define error_msg(x)  UxStandardError("%s", x);

/*--- types ---*/
typedef struct {
	char		*process;
	char		*defarg;
	char		*output_str;
	void		(*output_fnt)();
	void		(*exit_cb)();
	int		handle;
	int		echo,
			pid;
	int		subprocStatus;	
	char		*user_data;
	XtInputId	input_id;
	XtInputId	except_id;
} SubprocInfo_t;

typedef void (*void_func_ptr)();
typedef int  (*int_func_ptr)();
typedef char **  (*char_func_ptr)();

typedef struct {
	char_func_ptr prepare_command_m;
	int_func_ptr set_parent_side_m;
	int_func_ptr setup_parent_communication_m;
	int_func_ptr start_child_m;
	int_func_ptr set_child_side_m;
	void_func_ptr setup_child_communication_m;
	int_func_ptr exec_subprocess_m;
	void_func_ptr close_communication_m;
} SubprocMgr_t;

/*--- external functions ---*/
extern int UxGetSp UXPROTO((void));
extern int UxCheckIndex UXPROTO((handle index));
extern void UxSyserr UXPROTO((char *msg1, char *msg2));
extern int UxPrintArgs UXPROTO((char **tmp));
extern void UxSpCheck UXPROTO((SubprocInfo_t *sp));
extern void UxOutputHandler UXPROTO((SubprocInfo_t *sp));
extern void UxSpExit UXPROTO((SubprocInfo_t *sp));
extern void UxOnExitRunExitCb UXPROTO((int pid, int status));
extern char **UxBuildArgs UXPROTO((char *process, char *command_line));
extern void  UxKillAllSubprocs UXPROTO((void));

/*--- external variables ---*/
extern SubprocInfo_t *UxSpArray[MAX_SUBPROC];
extern SubprocMgr_t UxSubprocMgr;


#ifdef __cplusplus
}  /* Close scope of 'extern "C"' declaration which encloses file. */
#endif /* __cplusplus */

#endif
