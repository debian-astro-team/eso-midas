/* @(#)unit.h	17.1 (ESO-IPG) 01/25/02 17:26:05 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _UNIT_INCLUDED
#define _UNIT_INCLUDED

#include "uxproto.h"

unsigned char	UxFindUnitType UXPROTO((Widget));
void   UxConvertPosition UXPROTO((Widget, unsigned char, Position, Position, 
				  unsigned char, Position*, Position*));
void   UxConvertDimension UXPROTO((Widget, unsigned char, Dimension, Dimension, 
				  unsigned char, Dimension*, Dimension*));

Boolean	UxConvertPosDimToPUnitType  UXPROTO(( Position, Position,
   						swidget,  swidget,
   						unsigned char, unsigned char));
#endif /* _UNIT_INCLUDED */
