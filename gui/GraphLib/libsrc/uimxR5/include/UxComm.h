/* @(#)UxComm.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
#ifndef UXCommand_INCLUDED
#define UXCommand_INCLUDED

#include "Xm/Command.h"

#ifdef DESIGN_TIME
#include "UxSelBox.h"

#ifndef UxGetPromptString
extern binptr UxP_CommandRD_promptString;
#define UxGetPromptString(sw) \
        UxGET_string(sw,UxP_CommandRD_promptString,"promptString")
#define UxPutPromptString(sw,val) \
        UxPUT_string(sw,UxP_CommandRD_promptString,"promptString",val)
#endif

#ifndef UxGetHistoryVisibleItemCount
extern binptr UxP_CommandRD_historyVisibleItemCount;
#define UxGetHistoryVisibleItemCount(sw) \
        UxGET_int(sw,UxP_CommandRD_historyVisibleItemCount,"historyVisibleItemCount")
#define UxPutHistoryVisibleItemCount(sw,val) \
        UxPUT_int(sw,UxP_CommandRD_historyVisibleItemCount,"historyVisibleItemCount",val)
#endif

#ifndef UxGetHistoryMaxItems
extern binptr UxP_CommandRD_historyMaxItems;
#define UxGetHistoryMaxItems(sw) \
        UxGET_int(sw,UxP_CommandRD_historyMaxItems,"historyMaxItems")
#define UxPutHistoryMaxItems(sw,val) \
        UxPUT_int(sw,UxP_CommandRD_historyMaxItems,"historyMaxItems",val)
#endif

#ifndef UxGetHistoryItemCount
extern binptr UxP_CommandRD_historyItemCount;
#define UxGetHistoryItemCount(sw) \
        UxGET_int(sw,UxP_CommandRD_historyItemCount,"historyItemCount")
#define UxPutHistoryItemCount(sw,val) \
        UxPUT_int(sw,UxP_CommandRD_historyItemCount,"historyItemCount",val)
#endif

#ifndef UxGetHistoryItems
extern binptr UxP_CommandRD_historyItems;
#define UxGetHistoryItems(sw) \
        UxGET_string(sw,UxP_CommandRD_historyItems,"historyItems")
#define UxPutHistoryItems(sw,val) \
        UxPUT_string(sw,UxP_CommandRD_historyItems,"historyItems",val)
#endif

#ifndef UxGetCommand
extern binptr UxP_CommandRD_command;
#define UxGetCommand(sw) \
        UxGET_string(sw,UxP_CommandRD_command,"command")
#define UxPutCommand(sw,val) \
        UxPUT_string(sw,UxP_CommandRD_command,"command",val)
#endif

extern Class_t UxC_command;
#define UxCreateCommand(name,parent) \
        UxCreateSwidget(UxC_command,name,parent)
#else
#define UxCreateCommand(name,parent) \
        UxCreateSwidget(name,xmCommandWidgetClass,parent)
#endif
#endif
