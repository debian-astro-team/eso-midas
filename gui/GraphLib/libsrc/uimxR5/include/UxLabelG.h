/* @(#)UxLabelG.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
#ifndef UXLabelGadget_INCLUDED
#define UXLabelGadget_INCLUDED

#include "Xm/LabelG.h"

#ifdef DESIGN_TIME
#include "UxGadget.h"

#ifndef UxGetStringDirection
extern binptr UxP_LabelGadgetRD_stringDirection;
#define UxGetStringDirection(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_stringDirection,"stringDirection")
#define UxPutStringDirection(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_stringDirection,"stringDirection",val)
#endif

#ifndef UxGetRecomputeSize
extern binptr UxP_LabelGadgetRD_recomputeSize;
#define UxGetRecomputeSize(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_recomputeSize,"recomputeSize")
#define UxPutRecomputeSize(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_recomputeSize,"recomputeSize",val)
#endif

#ifndef UxGetMnemonicCharSet
extern binptr UxP_LabelGadgetRD_mnemonicCharSet;
#define UxGetMnemonicCharSet(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_mnemonicCharSet,"mnemonicCharSet")
#define UxPutMnemonicCharSet(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_mnemonicCharSet,"mnemonicCharSet",val)
#endif

#ifndef UxGetMnemonic
extern binptr UxP_LabelGadgetRD_mnemonic;
#define UxGetMnemonic(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_mnemonic,"mnemonic")
#define UxPutMnemonic(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_mnemonic,"mnemonic",val)
#endif

#ifndef UxGetMarginWidth
extern binptr UxP_LabelGadgetRD_marginWidth;
#define UxGetMarginWidth(sw) \
        UxGET_int(sw,UxP_LabelGadgetRD_marginWidth,"marginWidth")
#define UxPutMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_LabelGadgetRD_marginWidth,"marginWidth",val)
#endif

#ifndef UxGetMarginTop
extern binptr UxP_LabelGadgetRD_marginTop;
#define UxGetMarginTop(sw) \
        UxGET_int(sw,UxP_LabelGadgetRD_marginTop,"marginTop")
#define UxPutMarginTop(sw,val) \
        UxPUT_int(sw,UxP_LabelGadgetRD_marginTop,"marginTop",val)
#endif

#ifndef UxGetMarginRight
extern binptr UxP_LabelGadgetRD_marginRight;
#define UxGetMarginRight(sw) \
        UxGET_int(sw,UxP_LabelGadgetRD_marginRight,"marginRight")
#define UxPutMarginRight(sw,val) \
        UxPUT_int(sw,UxP_LabelGadgetRD_marginRight,"marginRight",val)
#endif

#ifndef UxGetMarginLeft
extern binptr UxP_LabelGadgetRD_marginLeft;
#define UxGetMarginLeft(sw) \
        UxGET_int(sw,UxP_LabelGadgetRD_marginLeft,"marginLeft")
#define UxPutMarginLeft(sw,val) \
        UxPUT_int(sw,UxP_LabelGadgetRD_marginLeft,"marginLeft",val)
#endif

#ifndef UxGetMarginHeight
extern binptr UxP_LabelGadgetRD_marginHeight;
#define UxGetMarginHeight(sw) \
        UxGET_int(sw,UxP_LabelGadgetRD_marginHeight,"marginHeight")
#define UxPutMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_LabelGadgetRD_marginHeight,"marginHeight",val)
#endif

#ifndef UxGetMarginBottom
extern binptr UxP_LabelGadgetRD_marginBottom;
#define UxGetMarginBottom(sw) \
        UxGET_int(sw,UxP_LabelGadgetRD_marginBottom,"marginBottom")
#define UxPutMarginBottom(sw,val) \
        UxPUT_int(sw,UxP_LabelGadgetRD_marginBottom,"marginBottom",val)
#endif

#ifndef UxGetLabelType
extern binptr UxP_LabelGadgetRD_labelType;
#define UxGetLabelType(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_labelType,"labelType")
#define UxPutLabelType(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_labelType,"labelType",val)
#endif

#ifndef UxGetLabelString
extern binptr UxP_LabelGadgetRD_labelString;
#define UxGetLabelString(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_labelString,"labelString")
#define UxPutLabelString(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_labelString,"labelString",val)
#endif

#ifndef UxGetLabelPixmap
extern binptr UxP_LabelGadgetRD_labelPixmap;
#define UxGetLabelPixmap(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_labelPixmap,"labelPixmap")
#define UxPutLabelPixmap(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_labelPixmap,"labelPixmap",val)
#endif

#ifndef UxGetLabelInsensitivePixmap
extern binptr UxP_LabelGadgetRD_labelInsensitivePixmap;
#define UxGetLabelInsensitivePixmap(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_labelInsensitivePixmap,"labelInsensitivePixmap")
#define UxPutLabelInsensitivePixmap(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_labelInsensitivePixmap,"labelInsensitivePixmap",val)
#endif

#ifndef UxGetFontList
extern binptr UxP_LabelGadgetRD_fontList;
#define UxGetFontList(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_fontList,"fontList")
#define UxPutFontList(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_fontList,"fontList",val)
#endif

#ifndef UxGetAlignment
extern binptr UxP_LabelGadgetRD_alignment;
#define UxGetAlignment(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_alignment,"alignment")
#define UxPutAlignment(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_alignment,"alignment",val)
#endif

#ifndef UxGetAcceleratorText
extern binptr UxP_LabelGadgetRD_acceleratorText;
#define UxGetAcceleratorText(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_acceleratorText,"acceleratorText")
#define UxPutAcceleratorText(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_acceleratorText,"acceleratorText",val)
#endif

#ifndef UxGetAccelerator
extern binptr UxP_LabelGadgetRD_accelerator;
#define UxGetAccelerator(sw) \
        UxGET_string(sw,UxP_LabelGadgetRD_accelerator,"accelerator")
#define UxPutAccelerator(sw,val) \
        UxPUT_string(sw,UxP_LabelGadgetRD_accelerator,"accelerator",val)
#endif

extern Class_t UxC_labelGadget;
#define UxCreateLabelGadget(name,parent) \
        UxCreateSwidget(UxC_labelGadget,name,parent)
#else
#define UxCreateLabelGadget(name,parent) \
        UxCreateSwidget(name,xmLabelGadgetClass,parent)
#endif
#endif
