/* @(#)valloc_ds.h	17.1 (ESO-IPG) 01/25/02 17:26:06 */
/*---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
#ifndef	_UX_VALLOC_DS_H_
#define	_UX_VALLOC_DS_H_
#ifdef	XOPEN_CATALOG
#include	"valloc_msg.h"
#endif
#define	DS_MS_VL_ATMPTOALLCBYTES	"011 Internal Error. Cannot allocate %d bytes\n\
of memory. The program is exiting.\n"
#endif
