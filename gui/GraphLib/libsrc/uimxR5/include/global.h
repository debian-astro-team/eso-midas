/* @(#)global.h	17.1 (ESO-IPG) 01/25/02 17:26:04 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
  Globals that are used through out the program
---------------------------------------------------------------------------*/

#ifndef _GLOBAL_INCLUDED
#define _GLOBAL_INCLUDED

#include "uxproto.h"
#include "vhandle.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* The following variables have storage defined for them in init_uimx.c
   by means of the dubious practice of defining 'extern' to be nothing */

extern Window	UxRootWindow;
extern Widget	UxTopLevel;	/* The top level shell */
extern Widget	UxSystemParent;	/* parent of all of UIM/X's own interfaces */
extern Display	*UxDisplay;
extern int	UxScreen;
extern XtAppContext   UxAppContext;
extern GC 	UxDrawGC, UxUimxBlackGC, UxUimxWhiteGC;
extern vhandle	UxVh_shells, UxVh_widgets;
extern XEvent   *UxEvent;
extern char	*UxUserShell;
extern vhandle	UxActionTable;

extern void	UxInternalError UXPROTO((char *, int, ...));
extern void	UxStandardError UXPROTO((char *, ...));


#ifdef OL_WIDGETS
/* these are added back in for the purpose of getting the beta OL version */
extern vhandle	Ux_trsl_list;
extern vhandle	Ux_action_tab;
extern vhandle	UxActionList;
#endif /* OL_WIDGETS */

#ifdef MOTIF_WIDGETS
#define	WINDOW_MANAGER	"mwm"	/*for when reading from resource files*/
#endif /* MOTIF_WIDGETS */

#ifdef OL_WIDGETS
#define WINDOW_MANAGER "olwm"	/* not currently used: just for consistency */
#endif /* OL_WIDGETS */

#ifdef __cplusplus
}  /* Close scope of 'extern "C"' declaration which encloses file. */
#endif /* __cplusplus */


#endif /* _GLOBAL_INCLUDED */
