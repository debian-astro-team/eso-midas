/* @(#)uxpm.h	17.1 (ESO-IPG) 01/25/02 17:26:06 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1990, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _UX_XPM_H_
#define _UX_XPM_H_

#include "uxproto.h"

extern int UxReadPixmapFile UXPROTO((Display *, Drawable, Widget, Colormap, char *, unsigned int*, unsigned int*, int*, int*, unsigned int, Pixmap *));
extern int UxReadPixmapOrBitmapFile UXPROTO((Display *, Drawable, Widget, Colormap, char *, unsigned int *, unsigned int *, unsigned int, Pixmap *, int *, int *, int *));
extern int UxLoadPixmapFromPixmapOrBitmapFile UXPROTO((swidget, char *, Pixmap *, unsigned *, unsigned *, Pixel, Pixel));

#define UX_IS_A_BITMAP		1
#define UX_IS_A_PIXMAP		2

#endif /* _UX_XPM_H_ */
