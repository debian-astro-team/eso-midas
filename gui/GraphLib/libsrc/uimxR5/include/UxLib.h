/* @(#)UxLib.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1989-1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *---------------------------------------------------------------------
 * Runtime library definitions.  
 * Used by UIM/X-generated Ux-format sources.
 *-------------------------------------------------------------------*/

#ifndef _UXLIB_INCLUDED
#define _UXLIB_INCLUDED 

#include "uxproto.h"
#include "valloc.h"
#include "UxCorba.h"
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/Text.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define UxAlloc(s) 	UxMalloc(s)

#ifdef  DESIGN_TIME

#include "UxDesign.h"

#else   /* ! design time */

/*------------------------------------------------
 * The runtime library declarations.
 *------------------------------------------------*/

typedef struct 	ShadowWidget 	*swidget;
typedef char 	**stringTable;

#define NO_PARENT       ((swidget) 0)
#define NULL_SWIDGET 	((swidget) 0)
#define NULL_WIDGET	((Widget) 0)

#ifndef ERROR
#define ERROR           -1
#endif
#ifndef NO_ERROR
#define NO_ERROR        0
#endif

#ifndef _GRAB_INCLUDED
#define _GRAB_INCLUDED
typedef enum
{
	no_grab = XtGrabNone,
	nonexclusive_grab = XtGrabNonexclusive,
	exclusive_grab = XtGrabExclusive
} grabtype;
#endif  /* _GRAB_INCLUDED  */

/*-------------------------------------------------------------------
 * Resource Puts and Gets.  
 *------------------------------------------------------------------*/

XtArgVal 	UxGetProp  UXPROTO(( swidget, char * ));
int     	UxPutProp  UXPROTO(( swidget, char *, XtArgVal ));

XtArgVal 	UxDDGetProp  UXPROTO(( swidget, char * ));
int     	UxDDPutProp  UXPROTO(( swidget, char *, XtArgVal ));

#include "UxPutsMF.h"
#include "UxGetsMF.h"

#define UxPutMsgDialogType(o,v) UxDDPutProp((o), "msgDialogType", ((XtArgVal)(v)))
#define UxGetMsgDialogType(o) (char *)UxDDGetProp((o), "msgDialogType")

#define UxPutText(m,v)   UxPutProp((m), XmNvalue, ((XtArgVal) (v)))	

#define UxIsValidSwidget(sw) UxIsSwidget(sw)

char 	*UxGetText  UXPROTO(( swidget ));

void 	UxPutAccelerators  UXPROTO((swidget, char *));
void 	UxPutTranslations  UXPROTO((swidget, char *));

char*	UxGetAccelerators  UXPROTO((swidget));
char*	UxGetTranslations  UXPROTO((swidget));

char*	UxGetCreateManaged UXPROTO((swidget));
void	UxPutCreateManaged UXPROTO((swidget, char*));

/*-------------------------------------------------------------------
 * Swidget creation.  The UxCreate<ClassName> macros expand
 * into calls to one of these functions.
 *------------------------------------------------------------------*/

swidget		UxCreateSwidget  UXPROTO ((char*, WidgetClass, swidget));
swidget		UxCreateDialogSwidget  UXPROTO ((char*, WidgetClass, swidget));
swidget 	UxCreateComponent UXPROTO((char *, swidget));
void		UxFreeSwidget UXPROTO((swidget));


/*-------------------------------------------------------------------
 * The UIM/X runtime convenience library.
 *------------------------------------------------------------------*/

void    	UxAddCallback  UXPROTO(( swidget, char *, 
					XtCallbackProc, XtPointer));
void		UxAddTabGroup  UXPROTO(( swidget ));
void    	UxCenterVisibly  UXPROTO(( swidget, swidget ));
void		UxCenterWidgetVisibly  UXPROTO(( Widget, Widget ));
Widget  	UxCreateWidget  UXPROTO(( swidget ));

void    	UxDelayUpdate  UXPROTO(( swidget ));
int     	UxDestroyInterface  UXPROTO(( swidget ));
void    	UxDestroySwidget  UXPROTO(( swidget ));

swidget 	UxFindSwidget  UXPROTO(( char * ));
void		UxFreeClientDataCB UXPROTO ((Widget, XtPointer, XtPointer));

WidgetClass     UxGetClass  UXPROTO((swidget));
void*		UxGetContext  UXPROTO((swidget));
char*		UxGetName  UXPROTO((swidget));
swidget 	UxGetParent  UXPROTO((swidget));
Widget		UxGetWidget  UXPROTO((swidget));
void		UxPutWidget UXPROTO((swidget, Widget));

Widget  	UxInitialize  UXPROTO(( char *, int *, char ** ));
void	  	UxAppInitialize  UXPROTO(( char *, int *, char ** ));
void	  	UxPreInitialize  UXPROTO(( void ));
int     	UxIsSwidget  UXPROTO(( swidget ));

int     	UxUsingSb UXPROTO(( void ));
void    	UxStartUnderSoftbench UXPROTO(( Cardinal *, String* ));

void    	UxMainLoop  UXPROTO(( void ));
void    	UxManage  UXPROTO(( swidget ));
void    	UxMap  UXPROTO(( swidget ));
void    	UxNotify  UXPROTO(( void ));
Widget		UxOptionInitialize  UXPROTO(( char *, int *, char **,
					XrmOptionDescRec *, Cardinal ));

swidget 	UxNameToSwidget  UXPROTO(( swidget, char * ));

int     	UxPopupInterface  UXPROTO(( swidget, grabtype ));
int     	UxPopdownInterface  UXPROTO(( swidget ));
void    	UxPostMenu  UXPROTO(( Widget, XEvent *, String *, Cardinal *));
int     	UxRealizeInterface  UXPROTO(( swidget ));
Widget  	UxRealWidget  UXPROTO(( swidget ));
void		UxRemoveTabGroup  UXPROTO(( swidget ));
Widget  	UxShellWidget  UXPROTO(( swidget ));

void		UxTextAppend UXPROTO((Widget, char*));

void    	UxUnmanage  UXPROTO(( swidget ));
void    	UxUnmap  UXPROTO(( swidget ));
void    	UxUpdate  UXPROTO(( swidget ));

void    	UxWaitForNotify  UXPROTO(( void ));
swidget 	UxWidgetToSwidget  UXPROTO(( Widget ));

/*-------------------------------------------------------------------
 * The runtime versions of the following are the toolkit functions.
 * Use the Ux- form so that your code works in UIM/X at test time.
 *-------------------------------------------------------------------*/

#define 	UxAddActions(a,b)    XtAppAddActions(UxAppContext, a,b)
#define 	UxNextEvent(e)	     XtAppNextEvent(UxAppContext, e)
#define 	UxDispatchEvent(e)   XtDispatchEvent(e)

/*-------------------------------------------------------------------
 * The following are used in Ux-form generated code.
 * You will rarely if ever need to call them directly.
 *-------------------------------------------------------------------*/

WidgetClass     UxGetDefaultShell  UXPROTO((swidget));
void     UxPutDefaultShell  UXPROTO((swidget, char *));

int    	UxIsShellClass  UXPROTO(( WidgetClass ));
void	UxMainWindowSetAreas  UXPROTO((swidget,
				      swidget, swidget, swidget,
				      swidget, swidget ));
void	UxPutContext UXPROTO((swidget, void*));
void   	UxRegisterPopupMenuHandler  UXPROTO(( swidget, swidget ));
void	UxScrolledWindowSetAreas  UXPROTO((swidget, 
					   swidget, swidget, swidget));
void   	UxSetDefaultShell  UXPROTO(( char * ));

int	UxGetIfClassCode   UXPROTO((swidget));
void	UxPutClassCode   UXPROTO((swidget, int));

void	UxLoadResources UXPROTO((char *));

char 	*UxInitCat UXPROTO((void));

void 	UxDestroyContextCB UXPROTO((Widget, XtPointer, XtPointer));
swidget UxChildSite UXPROTO((swidget));

/*-------------------------------------------------------------------
 * Global state of Motif and your X session.
 *-------------------------------------------------------------------*/

extern Widget   	UxTopLevel;
extern XtAppContext 	UxAppContext;
extern XEvent   	*UxCurrentEvent;
extern Display  	*UxDisplay;
extern int     		UxScreen;
extern Window 	  	UxRootWindow;
extern GC       	UxDrawGC;

/*-----------------------------------------------------
 * For backwards compatibility of user-rtime.c
 *-----------------------------------------------------*/

#define UxrDDInstall(nm, ut, xt)   UxDDInstall( nm, ut, xt)

#endif /* not DESIGN_TIME */

#ifdef __cplusplus

/*-----------------------------------------------------
 * Base Class of all generated interfaces
 *-----------------------------------------------------*/
class _UxCInterface {

public:
	virtual swidget childSite (Environment * pEnv);
	virtual swidget UxChildSite (swidget sw);

protected:
	swidget	UxThis;
};

/*-----------------------------------------------------
 * Macros
 *-----------------------------------------------------*/
#define CPLUS_ADAPT_CONTEXT(CLASS) \
        static inline \
                CLASS* UxGetContext(CLASS*self) {return self;} \
        static inline\
                void* UxGetContext(swidget any) {return ::UxGetContext(any);}

/*-----------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------*/
swidget UxChildSite (swidget);

#endif /* _cplusplus */

#ifdef __cplusplus
}  /* Close scope of 'extern "C"' declaration which encloses file. */
#endif /* __cplusplus */

#endif  /* _UXLIB_INCLUDED */
