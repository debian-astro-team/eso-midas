/* @(#)misc1_msg.h	17.1 (ESO-IPG) 01/25/02 17:26:04 */
/*---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
#define	MC_MISC1	1
#define	MS_MISC_NO_COLORMAP	1
#define	MS_MISC_NO_COLORNAME	2
#define	MS_MISC_COLORMAP_ALLOC	3
#define	MS_MISC_MAX_PROCS	4
#define	MS_MISC_CALLOC_ERR	5
#define	MS_MISC_INV_HNDL	6
#define	MS_MISC_NOT_HNDL	7
#define	MS_MISC_MALLOC_ERR	8
#define	MS_MISC_SYS_ERR	9
#define	MS_MISC_STREAM_DRV_ERR	10
#define	MS_MISC_PTY_DRV_ERR	11
#define	MS_MISC_NO_SUBPROCS	12
#define	MS_MISC_PIPE_ERR	13
#define	MS_MISC_SUBPROC_RUNNIN	14
#define	MS_MISC_SUBPROC_NOACT	15
#define	MS_MISC_W_SUBPROC_ERR	16
#define	MS_MISC_INV_SPRC_STRUCT	17
#define	MS_MISC_INV_OUT_FNCTN	18
#define	MS_MISC_R_SUBPROC_ERR	19
#define	MS_MISC_STAT_FAIL	20
#define	MS_MISC_OPEN_FAIL	21
#define	MS_MISC_INTERNALERRORFORMAT	22
#define	MS_MISC_WRONG_FLAG	23
#define	MS_MISC_NO_MATCH	24
#define	MS_MISC_ATTACHMENT_MSG1	25
#define	MS_MISC_NUM_ELEM_SEL	26
#define	MS_MISC_NO_CONV_ROUTINE	27
#define	MS_MISC_SELECTARRAY_ERR	28
#define	MS_MISC_WDGT_PROMPT	29
#define	MS_MISC1_PARM_ERR	30
#define	MS_MISC1_NULL_PROC_ERR	31
#define	MS_MISC1_DIAG_STYLE_MSG	32
#define	MS_MISC1_LONG_FNAME_UNUSED	33
#define	MS_MISC1_LONG_FNAME	34
#define	MS_MISC1_INVALID_MB_CHAR	35
#define	MS_MISC1_MSG_NOT_SENT	36
#define	MS_MISC1_PROCESS_EXITING	37
#define	MS_MISC_HNDL_DELAYED_EXIT	38
