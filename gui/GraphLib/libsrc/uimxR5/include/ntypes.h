/* @(#)ntypes.h	17.1 (ESO-IPG) 01/25/02 17:26:04 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _NTYPES_INCLUDED
#define _NTYPES_INCLUDED

/****************************************************************************/
/*****             ntypes.h                         		        *****/
/****************************************************************************/

#ifndef NULL
#define NULL 0
#endif

#ifndef NONE
#define NONE 0
#endif
			/* The typedef definitions of the data types */
#define SHORT short
#define INT int
#define FLOAT float
#define LONG long
#define U_SHORT unsigned short

#endif /* _NTYPES_INCLUDED */
