/* @(#)dstring.h	17.1 (ESO-IPG) 01/25/02 17:26:04 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1990, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _DSTRING_INCLUDED
#define _DSTRING_INCLUDED

#include "uxproto.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef struct
{
	char	*str;		/* Character storage 		*/
	int	size;		/* Total bytes of storage 	*/
	int	len;		/* Strlen of current content    */
} dstring;


/*** private calls ***/

extern dstring	UxDcreate UXPROTO(( char *str ));
extern void	UxFree_dstring UXPROTO(( dstring *ds ));
extern void	UxAppend_to_dstring UXPROTO(( dstring *s, char *dat ));

/*** public declarations ***/

extern dstring	UxDefaultDstring;

#define UxEmptyDstring		{NULL, 0, 0}

#define dappend(s, dat)		(UxAppend_to_dstring(&(s), dat))
#define dconcat(s, ds)		(UxAppend_dstring(&(s), ds))
#define dfree(s)		(UxFree_dstring(&(s)))
#define dgetstr(s)		((s).str)
#define dnstr(s)		((s).str ? (s).str : "")
#define dlen(s)			((s).len)
#define dcopy(ds)		(dcreate(dgetstr(ds)))
#define dcreate(s)		(UxDcreate(s))

#ifdef __cplusplus
}  /* Close scope of 'extern "C"' declaration which encloses file. */
#endif /* __cplusplus */


#endif /* _DSTRING_INCLUDED */
