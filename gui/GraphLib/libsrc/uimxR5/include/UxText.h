/* @(#)UxText.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXText_INCLUDED
#define UXText_INCLUDED

#include "Xm/Text.h"

#ifdef DESIGN_TIME
#include "UxPrim.h"

#ifndef UxGetWordWrap
extern binptr UxP_TextRD_wordWrap;
#define UxGetWordWrap(sw) \
        UxGET_string(sw,UxP_TextRD_wordWrap,"wordWrap")
#define UxPutWordWrap(sw,val) \
        UxPUT_string(sw,UxP_TextRD_wordWrap,"wordWrap",val)
#endif

#ifndef UxGetRows
extern binptr UxP_TextRD_rows;
#define UxGetRows(sw) \
        UxGET_short(sw,UxP_TextRD_rows,"rows")
#define UxPutRows(sw,val) \
        UxPUT_short(sw,UxP_TextRD_rows,"rows",val)
#endif

#ifndef UxGetResizeWidth
extern binptr UxP_TextRD_resizeWidth;
#define UxGetResizeWidth(sw) \
        UxGET_string(sw,UxP_TextRD_resizeWidth,"resizeWidth")
#define UxPutResizeWidth(sw,val) \
        UxPUT_string(sw,UxP_TextRD_resizeWidth,"resizeWidth",val)
#endif

#ifndef UxGetResizeHeight
extern binptr UxP_TextRD_resizeHeight;
#define UxGetResizeHeight(sw) \
        UxGET_string(sw,UxP_TextRD_resizeHeight,"resizeHeight")
#define UxPutResizeHeight(sw,val) \
        UxPUT_string(sw,UxP_TextRD_resizeHeight,"resizeHeight",val)
#endif

#ifndef UxGetCursorPositionVisible
extern binptr UxP_TextRD_cursorPositionVisible;
#define UxGetCursorPositionVisible(sw) \
        UxGET_string(sw,UxP_TextRD_cursorPositionVisible,"cursorPositionVisible")
#define UxPutCursorPositionVisible(sw,val) \
        UxPUT_string(sw,UxP_TextRD_cursorPositionVisible,"cursorPositionVisible",val)
#endif

#ifndef UxGetFontList
extern binptr UxP_TextRD_fontList;
#define UxGetFontList(sw) \
        UxGET_string(sw,UxP_TextRD_fontList,"fontList")
#define UxPutFontList(sw,val) \
        UxPUT_string(sw,UxP_TextRD_fontList,"fontList",val)
#endif

#ifndef UxGetColumns
extern binptr UxP_TextRD_columns;
#define UxGetColumns(sw) \
        UxGET_short(sw,UxP_TextRD_columns,"columns")
#define UxPutColumns(sw,val) \
        UxPUT_short(sw,UxP_TextRD_columns,"columns",val)
#endif

#ifndef UxGetBlinkRate
extern binptr UxP_TextRD_blinkRate;
#define UxGetBlinkRate(sw) \
        UxGET_int(sw,UxP_TextRD_blinkRate,"blinkRate")
#define UxPutBlinkRate(sw,val) \
        UxPUT_int(sw,UxP_TextRD_blinkRate,"blinkRate",val)
#endif

#ifndef UxGetSource
extern binptr UxP_TextRD_source;
#define UxGetSource(sw) \
        UxGET_XmTextSource(sw,UxP_TextRD_source,"source")
#define UxPutSource(sw,val) \
        UxPUT_XmTextSource(sw,UxP_TextRD_source,"source",val)
#endif

#ifndef UxGetSelectThreshold
extern binptr UxP_TextRD_selectThreshold;
#define UxGetSelectThreshold(sw) \
        UxGET_int(sw,UxP_TextRD_selectThreshold,"selectThreshold")
#define UxPutSelectThreshold(sw,val) \
        UxPUT_int(sw,UxP_TextRD_selectThreshold,"selectThreshold",val)
#endif

#ifndef UxGetSelectionArrayCount
extern binptr UxP_TextRD_selectionArrayCount;
#define UxGetSelectionArrayCount(sw) \
        UxGET_int(sw,UxP_TextRD_selectionArrayCount,"selectionArrayCount")
#define UxPutSelectionArrayCount(sw,val) \
        UxPUT_int(sw,UxP_TextRD_selectionArrayCount,"selectionArrayCount",val)
#endif

#ifndef UxGetSelectionArray
extern binptr UxP_TextRD_selectionArray;
#define UxGetSelectionArray(sw) \
        UxGET_string(sw,UxP_TextRD_selectionArray,"selectionArray")
#define UxPutSelectionArray(sw,val) \
        UxPUT_string(sw,UxP_TextRD_selectionArray,"selectionArray",val)
#endif

#ifndef UxGetPendingDelete
extern binptr UxP_TextRD_pendingDelete;
#define UxGetPendingDelete(sw) \
        UxGET_string(sw,UxP_TextRD_pendingDelete,"pendingDelete")
#define UxPutPendingDelete(sw,val) \
        UxPUT_string(sw,UxP_TextRD_pendingDelete,"pendingDelete",val)
#endif

#ifndef UxGetVerifyBell
extern binptr UxP_TextRD_verifyBell;
#define UxGetVerifyBell(sw) \
        UxGET_string(sw,UxP_TextRD_verifyBell,"verifyBell")
#define UxPutVerifyBell(sw,val) \
        UxPUT_string(sw,UxP_TextRD_verifyBell,"verifyBell",val)
#endif

#ifndef UxGetValueWcs
extern binptr UxP_TextRD_valueWcs;
#define UxGetValueWcs(sw) \
        UxGET_string(sw,UxP_TextRD_valueWcs,"valueWcs")
#define UxPutValueWcs(sw,val) \
        UxPUT_string(sw,UxP_TextRD_valueWcs,"valueWcs",val)
#endif

#ifndef UxGetText
extern binptr UxP_TextRD_text;
#define UxGetText(sw) \
        UxGET_string(sw,UxP_TextRD_text,"text")
#define UxPutText(sw,val) \
        UxPUT_string(sw,UxP_TextRD_text,"text",val)
#endif

#ifndef UxGetTopCharacter
extern binptr UxP_TextRD_topCharacter;
#define UxGetTopCharacter(sw) \
        UxGET_int(sw,UxP_TextRD_topCharacter,"topCharacter")
#define UxPutTopCharacter(sw,val) \
        UxPUT_int(sw,UxP_TextRD_topCharacter,"topCharacter",val)
#endif

#ifndef UxGetMaxLength
extern binptr UxP_TextRD_maxLength;
#define UxGetMaxLength(sw) \
        UxGET_int(sw,UxP_TextRD_maxLength,"maxLength")
#define UxPutMaxLength(sw,val) \
        UxPUT_int(sw,UxP_TextRD_maxLength,"maxLength",val)
#endif

#ifndef UxGetMarginWidth
extern binptr UxP_TextRD_marginWidth;
#define UxGetMarginWidth(sw) \
        UxGET_int(sw,UxP_TextRD_marginWidth,"marginWidth")
#define UxPutMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_TextRD_marginWidth,"marginWidth",val)
#endif

#ifndef UxGetMarginHeight
extern binptr UxP_TextRD_marginHeight;
#define UxGetMarginHeight(sw) \
        UxGET_int(sw,UxP_TextRD_marginHeight,"marginHeight")
#define UxPutMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_TextRD_marginHeight,"marginHeight",val)
#endif

#ifndef UxGetEditMode
extern binptr UxP_TextRD_editMode;
#define UxGetEditMode(sw) \
        UxGET_string(sw,UxP_TextRD_editMode,"editMode")
#define UxPutEditMode(sw,val) \
        UxPUT_string(sw,UxP_TextRD_editMode,"editMode",val)
#endif

#ifndef UxGetEditable
extern binptr UxP_TextRD_editable;
#define UxGetEditable(sw) \
        UxGET_string(sw,UxP_TextRD_editable,"editable")
#define UxPutEditable(sw,val) \
        UxPUT_string(sw,UxP_TextRD_editable,"editable",val)
#endif

#ifndef UxGetCursorPosition
extern binptr UxP_TextRD_cursorPosition;
#define UxGetCursorPosition(sw) \
        UxGET_int(sw,UxP_TextRD_cursorPosition,"cursorPosition")
#define UxPutCursorPosition(sw,val) \
        UxPUT_int(sw,UxP_TextRD_cursorPosition,"cursorPosition",val)
#endif

#ifndef UxGetAutoShowCursorPosition
extern binptr UxP_TextRD_autoShowCursorPosition;
#define UxGetAutoShowCursorPosition(sw) \
        UxGET_string(sw,UxP_TextRD_autoShowCursorPosition,"autoShowCursorPosition")
#define UxPutAutoShowCursorPosition(sw,val) \
        UxPUT_string(sw,UxP_TextRD_autoShowCursorPosition,"autoShowCursorPosition",val)
#endif

extern Class_t UxC_text;
#define UxCreateText(name,parent) \
        UxCreateSwidget(UxC_text,name,parent)
#else
#define UxCreateText(name,parent) \
        UxCreateSwidget(name,xmTextWidgetClass,parent)
#endif
#endif
