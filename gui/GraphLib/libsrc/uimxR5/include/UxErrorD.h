/* @(#)UxErrorD.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXErrorDialog_INCLUDED
#define UXErrorDialog_INCLUDED

#include "Xm/MessageB.h"

#ifdef DESIGN_TIME
#include "UxMsgBox.h"

extern Class_t UxC_errorDialog;
#define UxCreateErrorDialog(name,parent) \
        UxCreateSwidget(UxC_errorDialog,name,parent)
#else
#define UxCreateErrorDialog(name,parent) \
        UxCreateDialogSwidget(name,xmMessageBoxWidgetClass,parent)
#endif
#endif
