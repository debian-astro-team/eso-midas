/* @(#)UxCascB.h	17.1 (ESO-IPG) 01/25/02 17:25:58 */
#ifndef UXCascadeButton_INCLUDED
#define UXCascadeButton_INCLUDED

#include "Xm/CascadeB.h"

#ifdef DESIGN_TIME
#include "UxLabel.h"

#ifndef UxGetSubMenuId
extern binptr UxP_CascadeButtonRD_subMenuId;
#define UxGetSubMenuId(sw) \
        UxGET_string(sw,UxP_CascadeButtonRD_subMenuId,"subMenuId")
#define UxPutSubMenuId(sw,val) \
        UxPUT_string(sw,UxP_CascadeButtonRD_subMenuId,"subMenuId",val)
#endif

#ifndef UxGetMappingDelay
extern binptr UxP_CascadeButtonRD_mappingDelay;
#define UxGetMappingDelay(sw) \
        UxGET_int(sw,UxP_CascadeButtonRD_mappingDelay,"mappingDelay")
#define UxPutMappingDelay(sw,val) \
        UxPUT_int(sw,UxP_CascadeButtonRD_mappingDelay,"mappingDelay",val)
#endif

#ifndef UxGetCascadePixmap
extern binptr UxP_CascadeButtonRD_cascadePixmap;
#define UxGetCascadePixmap(sw) \
        UxGET_string(sw,UxP_CascadeButtonRD_cascadePixmap,"cascadePixmap")
#define UxPutCascadePixmap(sw,val) \
        UxPUT_string(sw,UxP_CascadeButtonRD_cascadePixmap,"cascadePixmap",val)
#endif

extern Class_t UxC_cascadeButton;
#define UxCreateCascadeButton(name,parent) \
        UxCreateSwidget(UxC_cascadeButton,name,parent)
#else
#define UxCreateCascadeButton(name,parent) \
        UxCreateSwidget(name,xmCascadeButtonWidgetClass,parent)
#endif
#endif
