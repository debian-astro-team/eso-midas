/* @(#)UxGadget.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
#ifndef UXGadget_INCLUDED
#define UXGadget_INCLUDED

#include "Xm/Xm.h"

#ifdef DESIGN_TIME
#include "UxRectO.h"

#ifndef UxGetUserData
extern binptr UxP_GadgetRD_userData;
#define UxGetUserData(sw) \
        UxGET_string(sw,UxP_GadgetRD_userData,"userData")
#define UxPutUserData(sw,val) \
        UxPUT_string(sw,UxP_GadgetRD_userData,"userData",val)
#endif

#ifndef UxGetUnitType
extern binptr UxP_GadgetRD_unitType;
#define UxGetUnitType(sw) \
        UxGET_string(sw,UxP_GadgetRD_unitType,"unitType")
#define UxPutUnitType(sw,val) \
        UxPUT_string(sw,UxP_GadgetRD_unitType,"unitType",val)
#endif

#ifndef UxGetTraversalOn
extern binptr UxP_GadgetRD_traversalOn;
#define UxGetTraversalOn(sw) \
        UxGET_string(sw,UxP_GadgetRD_traversalOn,"traversalOn")
#define UxPutTraversalOn(sw,val) \
        UxPUT_string(sw,UxP_GadgetRD_traversalOn,"traversalOn",val)
#endif

#ifndef UxGetShadowThickness
extern binptr UxP_GadgetRD_shadowThickness;
#define UxGetShadowThickness(sw) \
        UxGET_int(sw,UxP_GadgetRD_shadowThickness,"shadowThickness")
#define UxPutShadowThickness(sw,val) \
        UxPUT_int(sw,UxP_GadgetRD_shadowThickness,"shadowThickness",val)
#endif

#ifndef UxGetNavigationType
extern binptr UxP_GadgetRD_navigationType;
#define UxGetNavigationType(sw) \
        UxGET_string(sw,UxP_GadgetRD_navigationType,"navigationType")
#define UxPutNavigationType(sw,val) \
        UxPUT_string(sw,UxP_GadgetRD_navigationType,"navigationType",val)
#endif

#ifndef UxGetHighlightThickness
extern binptr UxP_GadgetRD_highlightThickness;
#define UxGetHighlightThickness(sw) \
        UxGET_int(sw,UxP_GadgetRD_highlightThickness,"highlightThickness")
#define UxPutHighlightThickness(sw,val) \
        UxPUT_int(sw,UxP_GadgetRD_highlightThickness,"highlightThickness",val)
#endif

#ifndef UxGetHighlightOnEnter
extern binptr UxP_GadgetRD_highlightOnEnter;
#define UxGetHighlightOnEnter(sw) \
        UxGET_string(sw,UxP_GadgetRD_highlightOnEnter,"highlightOnEnter")
#define UxPutHighlightOnEnter(sw,val) \
        UxPUT_string(sw,UxP_GadgetRD_highlightOnEnter,"highlightOnEnter",val)
#endif

extern Class_t UxC_gadget;
#define UxCreateGadget(name,parent) \
        UxCreateSwidget(UxC_gadget,name,parent)
#else
#define UxCreateGadget(name,parent) \
        UxCreateSwidget(name,xmGadgetClass,parent)
#endif
#endif
