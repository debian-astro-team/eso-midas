/* @(#)version.h	17.1 (ESO-IPG) 01/25/02 17:26:06 */
/*-------------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $		$Revision: 1.1.1.1 $
 *------------------------------------------------------------------------
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL RIGHTS RESERVED.  This notice is  intended  as  a  precaution
 * against  inadvertent publication, and shall not be deemed to con-
 * stitute an acknowledgment that publication has  occurred  nor  to
 * imply  any  waiver  of confidentiality.  The year included in the
 * notice is the year of the creation of the work.
 *------------------------------------------------------------------------
 * This file contains macro definitions used to create SCCS version
 * control strings.
 * Note:  must be compiled with an ANSI C compiler.
 *------------------------------------------------------------------------*/
#ifndef	_UX_VERSION_H_
#define	_UX_VERSION_H_

/*--- include files ---*/

/*--- macro symbolic constants ---*/

#ifndef UXLIBNAME
#	define UXLIBNAME 	""
#endif /* UXLIBNAME */

#ifndef UXEXECNAME
#	define UXEXECNAME 	""
#endif /* UXEXECNAME */

/*--- macro functions ---*/

/*---------------------------------------------------------------
 * Version string for executables.
 *---------------------------------------------------------------*/
#if !(defined(sun))
#define M_EXEC_VERSION(a,b) static char *ux_exec_version="@(#)"UX_PRODNAME" ["UX_VERSION"] "PATCH_ID" - "a" "b;
#else
#define M_EXEC_VERSION(a,b)
#endif

/*---------------------------------------------------------------
 * Version string for libs.
 *---------------------------------------------------------------*/
#if !(defined(sun))
#define M_LIB_VERSION(a,b) static char *ux_lib_version="@(#)"UX_PRODNAME" ["UX_VERSION"] "PATCH_ID" - "a" "b;
#else
#define M_LIB_VERSION(a,b)
#endif

/*---------------------------------------------------------------
 * The libraries and executable created in the overnight makes 
 * will contain the version strings for all source files by
 * defining in the top level Makefile LOCAL_VERSION.
 *---------------------------------------------------------------*/
#if !(defined(sun))
#ifdef LOCAL_VERSION
#  define M_FILE_VERSION(a) static char *ux_file_version="@(#)"UX_PRODNAME" ["UX_VERSION"] "a;
#else
#  define M_FILE_VERSION(a) 
#endif /* LOCAL_VERSION */
#else /* sun */
#  define M_FILE_VERSION(a)
#endif /* sun */

/*--- types ---*/

/*--- external functions ---*/

/*--- external variables ---*/

#endif /* _UX_VERSION_H_ */
/*--- end of file ---*/

