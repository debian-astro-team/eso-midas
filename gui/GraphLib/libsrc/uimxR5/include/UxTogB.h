/* @(#)UxTogB.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXToggleButton_INCLUDED
#define UXToggleButton_INCLUDED

#include "Xm/ToggleB.h"

#ifdef DESIGN_TIME
#include "UxLabel.h"

#ifndef UxGetVisibleWhenOff
extern binptr UxP_ToggleButtonRD_visibleWhenOff;
#define UxGetVisibleWhenOff(sw) \
        UxGET_string(sw,UxP_ToggleButtonRD_visibleWhenOff,"visibleWhenOff")
#define UxPutVisibleWhenOff(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonRD_visibleWhenOff,"visibleWhenOff",val)
#endif

#ifndef UxGetSpacing
extern binptr UxP_ToggleButtonRD_spacing;
#define UxGetSpacing(sw) \
        UxGET_int(sw,UxP_ToggleButtonRD_spacing,"spacing")
#define UxPutSpacing(sw,val) \
        UxPUT_int(sw,UxP_ToggleButtonRD_spacing,"spacing",val)
#endif

#ifndef UxGetSet
extern binptr UxP_ToggleButtonRD_set;
#define UxGetSet(sw) \
        UxGET_string(sw,UxP_ToggleButtonRD_set,"set")
#define UxPutSet(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonRD_set,"set",val)
#endif

#ifndef UxGetSelectPixmap
extern binptr UxP_ToggleButtonRD_selectPixmap;
#define UxGetSelectPixmap(sw) \
        UxGET_string(sw,UxP_ToggleButtonRD_selectPixmap,"selectPixmap")
#define UxPutSelectPixmap(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonRD_selectPixmap,"selectPixmap",val)
#endif

#ifndef UxGetSelectInsensitivePixmap
extern binptr UxP_ToggleButtonRD_selectInsensitivePixmap;
#define UxGetSelectInsensitivePixmap(sw) \
        UxGET_string(sw,UxP_ToggleButtonRD_selectInsensitivePixmap,"selectInsensitivePixmap")
#define UxPutSelectInsensitivePixmap(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonRD_selectInsensitivePixmap,"selectInsensitivePixmap",val)
#endif

#ifndef UxGetSelectColor
extern binptr UxP_ToggleButtonRD_selectColor;
#define UxGetSelectColor(sw) \
        UxGET_string(sw,UxP_ToggleButtonRD_selectColor,"selectColor")
#define UxPutSelectColor(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonRD_selectColor,"selectColor",val)
#endif

#ifndef UxGetIndicatorType
extern binptr UxP_ToggleButtonRD_indicatorType;
#define UxGetIndicatorType(sw) \
        UxGET_string(sw,UxP_ToggleButtonRD_indicatorType,"indicatorType")
#define UxPutIndicatorType(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonRD_indicatorType,"indicatorType",val)
#endif

#ifndef UxGetIndicatorSize
extern binptr UxP_ToggleButtonRD_indicatorSize;
#define UxGetIndicatorSize(sw) \
        UxGET_int(sw,UxP_ToggleButtonRD_indicatorSize,"indicatorSize")
#define UxPutIndicatorSize(sw,val) \
        UxPUT_int(sw,UxP_ToggleButtonRD_indicatorSize,"indicatorSize",val)
#endif

#ifndef UxGetIndicatorOn
extern binptr UxP_ToggleButtonRD_indicatorOn;
#define UxGetIndicatorOn(sw) \
        UxGET_string(sw,UxP_ToggleButtonRD_indicatorOn,"indicatorOn")
#define UxPutIndicatorOn(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonRD_indicatorOn,"indicatorOn",val)
#endif

#ifndef UxGetFillOnSelect
extern binptr UxP_ToggleButtonRD_fillOnSelect;
#define UxGetFillOnSelect(sw) \
        UxGET_string(sw,UxP_ToggleButtonRD_fillOnSelect,"fillOnSelect")
#define UxPutFillOnSelect(sw,val) \
        UxPUT_string(sw,UxP_ToggleButtonRD_fillOnSelect,"fillOnSelect",val)
#endif

extern Class_t UxC_toggleButton;
#define UxCreateToggleButton(name,parent) \
        UxCreateSwidget(UxC_toggleButton,name,parent)
#else
#define UxCreateToggleButton(name,parent) \
        UxCreateSwidget(name,xmToggleButtonWidgetClass,parent)
#endif
#endif
