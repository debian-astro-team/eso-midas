/* @(#)resload.h	17.1 (ESO-IPG) 01/25/02 17:26:05 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _RESLOAD_INCLUDED
#define _RESLOAD_INCLUDED

#include "uxproto.h"
#include "pathlist.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


extern pathlist	UxResourcePath, UxBitmapPath;

extern void	UxInitializeResourcePath UXPROTO(( void ));
extern void	UxInitializeBitmapPath UXPROTO(( void ));
extern void	UxOverrideResources UXPROTO(( char* ));
extern void	UxLoadResources UXPROTO(( char* ));
extern void	UxLoadResourceTree UXPROTO(( void ));
extern char	*UxGetResource UXPROTO(( char*, char* ));
extern char	*UxGetDefault UXPROTO(( char*, char*, char* ));
extern char	*UxGetUimxResource UXPROTO(( char* ));
extern char	*UxGetUimxDefault UXPROTO(( char*, char* ));
extern char	*UxGetUimxWgtResource UXPROTO(( char* ));
extern char	*UxGetUimxWgtDefault UXPROTO(( char*, char* ));
extern char	*UxGetAppResource UXPROTO(( char* ));
extern char	*UxGetAppDefault UXPROTO(( char*, char* ));
extern char	*UxExpandResourceFilename UXPROTO(( char* ));
extern char	*UxExpandBitmapFilename UXPROTO(( char* ));
extern char	*UxExpandEnv UXPROTO(( char* ));
extern int	UxGetAnyResource UXPROTO(( char*, char*, char**, XrmValue* ));
extern void	UxPutAnyResource UXPROTO(( char*, char*, char*, XrmValue* ));

#define	AppShellIndex		0
#define	DialogShellIndex	1
#define	OverrideShellIndex	2
#define	TopLevelShellIndex	3
#define	TransientShellIndex	4
#define	MenuShellIndex		5

extern	char	*UxShellNameList[];

#define	browserOutline	0
#define	browserTree	1

extern	char	*UxBrowserType[];


#define	MAX_IMPLICIT_SHELL_CLASSES	4

#ifdef __cplusplus
}  /* Close scope of 'extern "C"' declaration which encloses file. */
#endif /* __cplusplus */


#endif /* _RESLOAD_INCLUDED */
