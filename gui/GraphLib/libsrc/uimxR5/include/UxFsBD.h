/* @(#)UxFsBD.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXFileSelectionBoxDialog_INCLUDED
#define UXFileSelectionBoxDialog_INCLUDED

#include "Xm/FileSB.h"

#ifdef DESIGN_TIME
#include "UxFsBox.h"

extern Class_t UxC_fileSelectionBoxDialog;
#define UxCreateFileSelectionBoxDialog(name,parent) \
        UxCreateSwidget(UxC_fileSelectionBoxDialog,name,parent)
#else
#define UxCreateFileSelectionBoxDialog(name,parent) \
        UxCreateDialogSwidget(name,xmFileSelectionBoxWidgetClass,parent)
#endif
#endif
