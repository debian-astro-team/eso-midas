/* @(#)UxPaneW.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXPanedWindow_INCLUDED
#define UXPanedWindow_INCLUDED

#include "Xm/PanedW.h"

#ifdef DESIGN_TIME
#include "UxMgr.h"

#ifndef UxGetSkipAdjust
extern binptr UxP_PanedWindowRD_skipAdjust;
#define UxGetSkipAdjust(sw) \
        UxGET_string(sw,UxP_PanedWindowRD_skipAdjust,"skipAdjust")
#define UxPutSkipAdjust(sw,val) \
        UxPUT_string(sw,UxP_PanedWindowRD_skipAdjust,"skipAdjust",val)
#endif

#ifndef UxGetPositionIndex
extern binptr UxP_PanedWindowRD_positionIndex;
#define UxGetPositionIndex(sw) \
        UxGET_short(sw,UxP_PanedWindowRD_positionIndex,"positionIndex")
#define UxPutPositionIndex(sw,val) \
        UxPUT_short(sw,UxP_PanedWindowRD_positionIndex,"positionIndex",val)
#endif

#ifndef UxGetPaneMinimum
extern binptr UxP_PanedWindowRD_paneMinimum;
#define UxGetPaneMinimum(sw) \
        UxGET_int(sw,UxP_PanedWindowRD_paneMinimum,"paneMinimum")
#define UxPutPaneMinimum(sw,val) \
        UxPUT_int(sw,UxP_PanedWindowRD_paneMinimum,"paneMinimum",val)
#endif

#ifndef UxGetPaneMaximum
extern binptr UxP_PanedWindowRD_paneMaximum;
#define UxGetPaneMaximum(sw) \
        UxGET_int(sw,UxP_PanedWindowRD_paneMaximum,"paneMaximum")
#define UxPutPaneMaximum(sw,val) \
        UxPUT_int(sw,UxP_PanedWindowRD_paneMaximum,"paneMaximum",val)
#endif

#ifndef UxGetAllowResize
extern binptr UxP_PanedWindowRD_allowResize;
#define UxGetAllowResize(sw) \
        UxGET_string(sw,UxP_PanedWindowRD_allowResize,"allowResize")
#define UxPutAllowResize(sw,val) \
        UxPUT_string(sw,UxP_PanedWindowRD_allowResize,"allowResize",val)
#endif

#ifndef UxGetSpacing
extern binptr UxP_PanedWindowRD_spacing;
#define UxGetSpacing(sw) \
        UxGET_int(sw,UxP_PanedWindowRD_spacing,"spacing")
#define UxPutSpacing(sw,val) \
        UxPUT_int(sw,UxP_PanedWindowRD_spacing,"spacing",val)
#endif

#ifndef UxGetSeparatorOn
extern binptr UxP_PanedWindowRD_separatorOn;
#define UxGetSeparatorOn(sw) \
        UxGET_string(sw,UxP_PanedWindowRD_separatorOn,"separatorOn")
#define UxPutSeparatorOn(sw,val) \
        UxPUT_string(sw,UxP_PanedWindowRD_separatorOn,"separatorOn",val)
#endif

#ifndef UxGetSashWidth
extern binptr UxP_PanedWindowRD_sashWidth;
#define UxGetSashWidth(sw) \
        UxGET_int(sw,UxP_PanedWindowRD_sashWidth,"sashWidth")
#define UxPutSashWidth(sw,val) \
        UxPUT_int(sw,UxP_PanedWindowRD_sashWidth,"sashWidth",val)
#endif

#ifndef UxGetSashShadowThickness
extern binptr UxP_PanedWindowRD_sashShadowThickness;
#define UxGetSashShadowThickness(sw) \
        UxGET_int(sw,UxP_PanedWindowRD_sashShadowThickness,"sashShadowThickness")
#define UxPutSashShadowThickness(sw,val) \
        UxPUT_int(sw,UxP_PanedWindowRD_sashShadowThickness,"sashShadowThickness",val)
#endif

#ifndef UxGetSashIndent
extern binptr UxP_PanedWindowRD_sashIndent;
#define UxGetSashIndent(sw) \
        UxGET_int(sw,UxP_PanedWindowRD_sashIndent,"sashIndent")
#define UxPutSashIndent(sw,val) \
        UxPUT_int(sw,UxP_PanedWindowRD_sashIndent,"sashIndent",val)
#endif

#ifndef UxGetSashHeight
extern binptr UxP_PanedWindowRD_sashHeight;
#define UxGetSashHeight(sw) \
        UxGET_int(sw,UxP_PanedWindowRD_sashHeight,"sashHeight")
#define UxPutSashHeight(sw,val) \
        UxPUT_int(sw,UxP_PanedWindowRD_sashHeight,"sashHeight",val)
#endif

#ifndef UxGetRefigureMode
extern binptr UxP_PanedWindowRD_refigureMode;
#define UxGetRefigureMode(sw) \
        UxGET_string(sw,UxP_PanedWindowRD_refigureMode,"refigureMode")
#define UxPutRefigureMode(sw,val) \
        UxPUT_string(sw,UxP_PanedWindowRD_refigureMode,"refigureMode",val)
#endif

#ifndef UxGetMarginWidth
extern binptr UxP_PanedWindowRD_marginWidth;
#define UxGetMarginWidth(sw) \
        UxGET_int(sw,UxP_PanedWindowRD_marginWidth,"marginWidth")
#define UxPutMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_PanedWindowRD_marginWidth,"marginWidth",val)
#endif

#ifndef UxGetMarginHeight
extern binptr UxP_PanedWindowRD_marginHeight;
#define UxGetMarginHeight(sw) \
        UxGET_int(sw,UxP_PanedWindowRD_marginHeight,"marginHeight")
#define UxPutMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_PanedWindowRD_marginHeight,"marginHeight",val)
#endif

extern Class_t UxC_panedWindow;
#define UxCreatePanedWindow(name,parent) \
        UxCreateSwidget(UxC_panedWindow,name,parent)
#else
#define UxCreatePanedWindow(name,parent) \
        UxCreateSwidget(name,xmPanedWindowWidgetClass,parent)
#endif
#endif
