/* @(#)UxWmSh.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXWMShell_INCLUDED
#define UXWMShell_INCLUDED

#include "X11/Shell.h"

#ifdef DESIGN_TIME
#include "UxShell.h"

#ifndef UxGetWmTimeout
extern binptr UxP_WMShellRD_wmTimeout;
#define UxGetWmTimeout(sw) \
        UxGET_int(sw,UxP_WMShellRD_wmTimeout,"wmTimeout")
#define UxPutWmTimeout(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_wmTimeout,"wmTimeout",val)
#endif

#ifndef UxGetWinGravity
extern binptr UxP_WMShellRD_winGravity;
#define UxGetWinGravity(sw) \
        UxGET_string(sw,UxP_WMShellRD_winGravity,"winGravity")
#define UxPutWinGravity(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_winGravity,"winGravity",val)
#endif

#ifndef UxGetWindowGroup
extern binptr UxP_WMShellRD_windowGroup;
#define UxGetWindowGroup(sw) \
        UxGET_string(sw,UxP_WMShellRD_windowGroup,"windowGroup")
#define UxPutWindowGroup(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_windowGroup,"windowGroup",val)
#endif

#ifndef UxGetWidthInc
extern binptr UxP_WMShellRD_widthInc;
#define UxGetWidthInc(sw) \
        UxGET_int(sw,UxP_WMShellRD_widthInc,"widthInc")
#define UxPutWidthInc(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_widthInc,"widthInc",val)
#endif

#ifndef UxGetWaitForWm
extern binptr UxP_WMShellRD_waitForWm;
#define UxGetWaitForWm(sw) \
        UxGET_string(sw,UxP_WMShellRD_waitForWm,"waitForWm")
#define UxPutWaitForWm(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_waitForWm,"waitForWm",val)
#endif

#ifndef UxGetTransient
extern binptr UxP_WMShellRD_transient;
#define UxGetTransient(sw) \
        UxGET_string(sw,UxP_WMShellRD_transient,"transient")
#define UxPutTransient(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_transient,"transient",val)
#endif

#ifndef UxGetTitleEncoding
extern binptr UxP_WMShellRD_titleEncoding;
#define UxGetTitleEncoding(sw) \
        UxGET_string(sw,UxP_WMShellRD_titleEncoding,"titleEncoding")
#define UxPutTitleEncoding(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_titleEncoding,"titleEncoding",val)
#endif

#ifndef UxGetTitle
extern binptr UxP_WMShellRD_title;
#define UxGetTitle(sw) \
        UxGET_string(sw,UxP_WMShellRD_title,"title")
#define UxPutTitle(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_title,"title",val)
#endif

#ifndef UxGetMinWidth
extern binptr UxP_WMShellRD_minWidth;
#define UxGetMinWidth(sw) \
        UxGET_int(sw,UxP_WMShellRD_minWidth,"minWidth")
#define UxPutMinWidth(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_minWidth,"minWidth",val)
#endif

#ifndef UxGetMinHeight
extern binptr UxP_WMShellRD_minHeight;
#define UxGetMinHeight(sw) \
        UxGET_int(sw,UxP_WMShellRD_minHeight,"minHeight")
#define UxPutMinHeight(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_minHeight,"minHeight",val)
#endif

#ifndef UxGetMinAspectY
extern binptr UxP_WMShellRD_minAspectY;
#define UxGetMinAspectY(sw) \
        UxGET_int(sw,UxP_WMShellRD_minAspectY,"minAspectY")
#define UxPutMinAspectY(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_minAspectY,"minAspectY",val)
#endif

#ifndef UxGetMinAspectX
extern binptr UxP_WMShellRD_minAspectX;
#define UxGetMinAspectX(sw) \
        UxGET_int(sw,UxP_WMShellRD_minAspectX,"minAspectX")
#define UxPutMinAspectX(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_minAspectX,"minAspectX",val)
#endif

#ifndef UxGetMaxWidth
extern binptr UxP_WMShellRD_maxWidth;
#define UxGetMaxWidth(sw) \
        UxGET_int(sw,UxP_WMShellRD_maxWidth,"maxWidth")
#define UxPutMaxWidth(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_maxWidth,"maxWidth",val)
#endif

#ifndef UxGetMaxHeight
extern binptr UxP_WMShellRD_maxHeight;
#define UxGetMaxHeight(sw) \
        UxGET_int(sw,UxP_WMShellRD_maxHeight,"maxHeight")
#define UxPutMaxHeight(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_maxHeight,"maxHeight",val)
#endif

#ifndef UxGetMaxAspectY
extern binptr UxP_WMShellRD_maxAspectY;
#define UxGetMaxAspectY(sw) \
        UxGET_int(sw,UxP_WMShellRD_maxAspectY,"maxAspectY")
#define UxPutMaxAspectY(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_maxAspectY,"maxAspectY",val)
#endif

#ifndef UxGetMaxAspectX
extern binptr UxP_WMShellRD_maxAspectX;
#define UxGetMaxAspectX(sw) \
        UxGET_int(sw,UxP_WMShellRD_maxAspectX,"maxAspectX")
#define UxPutMaxAspectX(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_maxAspectX,"maxAspectX",val)
#endif

#ifndef UxGetInput
extern binptr UxP_WMShellRD_input;
#define UxGetInput(sw) \
        UxGET_string(sw,UxP_WMShellRD_input,"input")
#define UxPutInput(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_input,"input",val)
#endif

#ifndef UxGetInitialState
extern binptr UxP_WMShellRD_initialState;
#define UxGetInitialState(sw) \
        UxGET_string(sw,UxP_WMShellRD_initialState,"initialState")
#define UxPutInitialState(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_initialState,"initialState",val)
#endif

#ifndef UxGetIconY
extern binptr UxP_WMShellRD_iconY;
#define UxGetIconY(sw) \
        UxGET_int(sw,UxP_WMShellRD_iconY,"iconY")
#define UxPutIconY(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_iconY,"iconY",val)
#endif

#ifndef UxGetIconX
extern binptr UxP_WMShellRD_iconX;
#define UxGetIconX(sw) \
        UxGET_int(sw,UxP_WMShellRD_iconX,"iconX")
#define UxPutIconX(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_iconX,"iconX",val)
#endif

#ifndef UxGetIconWindow
extern binptr UxP_WMShellRD_iconWindow;
#define UxGetIconWindow(sw) \
        UxGET_string(sw,UxP_WMShellRD_iconWindow,"iconWindow")
#define UxPutIconWindow(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_iconWindow,"iconWindow",val)
#endif

#ifndef UxGetIconPixmap
extern binptr UxP_WMShellRD_iconPixmap;
#define UxGetIconPixmap(sw) \
        UxGET_string(sw,UxP_WMShellRD_iconPixmap,"iconPixmap")
#define UxPutIconPixmap(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_iconPixmap,"iconPixmap",val)
#endif

#ifndef UxGetIconMask
extern binptr UxP_WMShellRD_iconMask;
#define UxGetIconMask(sw) \
        UxGET_string(sw,UxP_WMShellRD_iconMask,"iconMask")
#define UxPutIconMask(sw,val) \
        UxPUT_string(sw,UxP_WMShellRD_iconMask,"iconMask",val)
#endif

#ifndef UxGetHeightInc
extern binptr UxP_WMShellRD_heightInc;
#define UxGetHeightInc(sw) \
        UxGET_int(sw,UxP_WMShellRD_heightInc,"heightInc")
#define UxPutHeightInc(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_heightInc,"heightInc",val)
#endif

#ifndef UxGetBaseWidth
extern binptr UxP_WMShellRD_baseWidth;
#define UxGetBaseWidth(sw) \
        UxGET_int(sw,UxP_WMShellRD_baseWidth,"baseWidth")
#define UxPutBaseWidth(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_baseWidth,"baseWidth",val)
#endif

#ifndef UxGetBaseHeight
extern binptr UxP_WMShellRD_baseHeight;
#define UxGetBaseHeight(sw) \
        UxGET_int(sw,UxP_WMShellRD_baseHeight,"baseHeight")
#define UxPutBaseHeight(sw,val) \
        UxPUT_int(sw,UxP_WMShellRD_baseHeight,"baseHeight",val)
#endif

extern Class_t UxC_wMShell;
#define UxCreateWMShell(name,parent) \
        UxCreateSwidget(UxC_wMShell,name,parent)
#else
#define UxCreateWMShell(name,parent) \
        UxCreateSwidget(name,wmShellWidgetClass,parent)
#endif
#endif
