/* @(#)vtypes.h	17.1 (ESO-IPG) 01/25/02 17:26:06 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1990, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#ifndef _VTYPES_INCLUDED
#define _VTYPES_INCLUDED

#ifndef TRUE
#define FALSE 0
#define TRUE 1
#endif

#ifndef NULL
#define NULL 0
#endif

#ifndef ERROR
#define ERROR (-1)
#define NO_ERROR 0
#endif

/************************************************************/
/*****   Type codes used in interpreter parse trees	*****/
/************************************************************/

/**/
/** variable types  0 - SERVECNT are reserved **/
/**/

#define	SERVECNT	16

/* define the T_ types */

/* should be arranged in order of 
 * increasing binary conversion precedence 
 */
#define T_CHAR          0
#define T_UCHAR         1
#define T_SHORT         2
#define T_USHORT        3
#define T_INT           4
#define T_UINT          5
#define T_LONG          6
#define T_ULONG         7
#define T_FLOAT         8
#define T_DOUBLE        9

#define T_STRUCT        10
#define T_ARRAY         11
#define T_FUNC          12
#define T_PNTR          13
#define T_VOID          14
#define T_ENUM		15
#define	T_vhandle	16

#define	T_char		T_CHAR
#define	T_short		T_SHORT
#define	T_int		T_INT
#define	T_long		T_LONG
#define	T_float		T_FLOAT
#define	T_double	T_DOUBLE
#define	T_void		T_VOID

#define MAX_INTERP_TYPE	600

/* other types must begin after end of interpreter types */

#define T_class         MAX_INTERP_TYPE
#define T_binptr	MAX_INTERP_TYPE+1
#define T_lock		MAX_INTERP_TYPE+2
#define T_STRING	MAX_INTERP_TYPE+3
#define T_Widget	MAX_INTERP_TYPE+4
#define T_Boolean	MAX_INTERP_TYPE+5

/* Data kept in the flag field for vhandles that are shared
 * between the interpreter and UIMX.  
 */

#define FREELIST_KEY    1000
#define FREELIST        1001
#define STRING		1009

#endif /* _VTYPES_INCLUDED */
