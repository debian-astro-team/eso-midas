/* @(#)system_msg.h	17.1 (ESO-IPG) 01/25/02 17:26:05 */
/*---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
#define	MC_SYSTEM_NAME	4
#define	MS_SYS_U_TASKNAME	1
#define	MS_SYS_RSRC_NAME_UNUSED	2
#define	MS_SYS_TASKNAME	3
#define	MS_SYS_U_TASKDIR	4
#define	MS_SYS_U_TASKDIR_ENV_UNUSED	5
#define	MS_SYS_TASKDIR	6
#define	MS_SYS_RESOURCE_VAR_UNUSED	7
#define	MS_SYS_BINDIR	8
#define	MS_SYS_CONFIGDIR	9
#define	MS_SYS_RUNTIME_RSRC_NAME_UNUSED	10
#define	MS_SYS_PREFIX	11
#define	MS_SYS_FULL_NAME	12
#define	MS_SYS_SCCS_VERSION_NUM_UNUSED	13
#define	MS_SYS_RSRC_NAME_UNUSED_1	14
#define	MS_SYS_RESOURCE_VAR_UNUSED_1	15
#define	MS_SYS_SCCS_VERSION_NUM_UNUSED_1	16
#define	MS_SYS_CGEN_SCCS_VERSION_NUM_UNUSED	17
#define	MS_SYS_READUIL_SCCS_VERSION_NUM_UNUSED	18
#define	MS_SYS_KEYGEN_SCCS_VERSION_NUM_UNUSED	19
#define	MS_SYS_SERVERD_SCCS_VERSION_NUM_UNUSED	20
#define	MS_SYS_RSRC_NAME	21
#define	MS_SYS_RESOURCE_VAR	22
#define	MS_SYS_RUNTIME_RSRC_NAME	23
