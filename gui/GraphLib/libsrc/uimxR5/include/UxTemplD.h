/* @(#)UxTemplD.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXTemplateDialog_INCLUDED
#define UXTemplateDialog_INCLUDED

#include "Xm/MessageB.h"

#ifdef DESIGN_TIME
#include "UxMsgBox.h"

extern Class_t UxC_templateDialog;
#define UxCreateTemplateDialog(name,parent) \
        UxCreateSwidget(UxC_templateDialog,name,parent)
#else
#define UxCreateTemplateDialog(name,parent) \
        UxCreateDialogSwidget(name,xmMessageBoxWidgetClass,parent)
#endif
#endif
