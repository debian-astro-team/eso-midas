/* @(#)UxTextF.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXTextField_INCLUDED
#define UXTextField_INCLUDED

#include "Xm/TextF.h"

#ifdef DESIGN_TIME
#include "UxPrim.h"

#ifndef UxGetVerifyBell
extern binptr UxP_TextFieldRD_verifyBell;
#define UxGetVerifyBell(sw) \
        UxGET_string(sw,UxP_TextFieldRD_verifyBell,"verifyBell")
#define UxPutVerifyBell(sw,val) \
        UxPUT_string(sw,UxP_TextFieldRD_verifyBell,"verifyBell",val)
#endif

#ifndef UxGetValueWcs
extern binptr UxP_TextFieldRD_valueWcs;
#define UxGetValueWcs(sw) \
        UxGET_string(sw,UxP_TextFieldRD_valueWcs,"valueWcs")
#define UxPutValueWcs(sw,val) \
        UxPUT_string(sw,UxP_TextFieldRD_valueWcs,"valueWcs",val)
#endif

#ifndef UxGetText
extern binptr UxP_TextFieldRD_text;
#define UxGetText(sw) \
        UxGET_string(sw,UxP_TextFieldRD_text,"text")
#define UxPutText(sw,val) \
        UxPUT_string(sw,UxP_TextFieldRD_text,"text",val)
#endif

#ifndef UxGetSelectThreshold
extern binptr UxP_TextFieldRD_selectThreshold;
#define UxGetSelectThreshold(sw) \
        UxGET_int(sw,UxP_TextFieldRD_selectThreshold,"selectThreshold")
#define UxPutSelectThreshold(sw,val) \
        UxPUT_int(sw,UxP_TextFieldRD_selectThreshold,"selectThreshold",val)
#endif

#ifndef UxGetSelectionArrayCount
extern binptr UxP_TextFieldRD_selectionArrayCount;
#define UxGetSelectionArrayCount(sw) \
        UxGET_int(sw,UxP_TextFieldRD_selectionArrayCount,"selectionArrayCount")
#define UxPutSelectionArrayCount(sw,val) \
        UxPUT_int(sw,UxP_TextFieldRD_selectionArrayCount,"selectionArrayCount",val)
#endif

#ifndef UxGetSelectionArray
extern binptr UxP_TextFieldRD_selectionArray;
#define UxGetSelectionArray(sw) \
        UxGET_string(sw,UxP_TextFieldRD_selectionArray,"selectionArray")
#define UxPutSelectionArray(sw,val) \
        UxPUT_string(sw,UxP_TextFieldRD_selectionArray,"selectionArray",val)
#endif

#ifndef UxGetResizeWidth
extern binptr UxP_TextFieldRD_resizeWidth;
#define UxGetResizeWidth(sw) \
        UxGET_string(sw,UxP_TextFieldRD_resizeWidth,"resizeWidth")
#define UxPutResizeWidth(sw,val) \
        UxPUT_string(sw,UxP_TextFieldRD_resizeWidth,"resizeWidth",val)
#endif

#ifndef UxGetPendingDelete
extern binptr UxP_TextFieldRD_pendingDelete;
#define UxGetPendingDelete(sw) \
        UxGET_string(sw,UxP_TextFieldRD_pendingDelete,"pendingDelete")
#define UxPutPendingDelete(sw,val) \
        UxPUT_string(sw,UxP_TextFieldRD_pendingDelete,"pendingDelete",val)
#endif

#ifndef UxGetMaxLength
extern binptr UxP_TextFieldRD_maxLength;
#define UxGetMaxLength(sw) \
        UxGET_int(sw,UxP_TextFieldRD_maxLength,"maxLength")
#define UxPutMaxLength(sw,val) \
        UxPUT_int(sw,UxP_TextFieldRD_maxLength,"maxLength",val)
#endif

#ifndef UxGetMarginWidth
extern binptr UxP_TextFieldRD_marginWidth;
#define UxGetMarginWidth(sw) \
        UxGET_int(sw,UxP_TextFieldRD_marginWidth,"marginWidth")
#define UxPutMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_TextFieldRD_marginWidth,"marginWidth",val)
#endif

#ifndef UxGetMarginHeight
extern binptr UxP_TextFieldRD_marginHeight;
#define UxGetMarginHeight(sw) \
        UxGET_int(sw,UxP_TextFieldRD_marginHeight,"marginHeight")
#define UxPutMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_TextFieldRD_marginHeight,"marginHeight",val)
#endif

#ifndef UxGetFontList
extern binptr UxP_TextFieldRD_fontList;
#define UxGetFontList(sw) \
        UxGET_string(sw,UxP_TextFieldRD_fontList,"fontList")
#define UxPutFontList(sw,val) \
        UxPUT_string(sw,UxP_TextFieldRD_fontList,"fontList",val)
#endif

#ifndef UxGetEditable
extern binptr UxP_TextFieldRD_editable;
#define UxGetEditable(sw) \
        UxGET_string(sw,UxP_TextFieldRD_editable,"editable")
#define UxPutEditable(sw,val) \
        UxPUT_string(sw,UxP_TextFieldRD_editable,"editable",val)
#endif

#ifndef UxGetCursorPositionVisible
extern binptr UxP_TextFieldRD_cursorPositionVisible;
#define UxGetCursorPositionVisible(sw) \
        UxGET_string(sw,UxP_TextFieldRD_cursorPositionVisible,"cursorPositionVisible")
#define UxPutCursorPositionVisible(sw,val) \
        UxPUT_string(sw,UxP_TextFieldRD_cursorPositionVisible,"cursorPositionVisible",val)
#endif

#ifndef UxGetCursorPosition
extern binptr UxP_TextFieldRD_cursorPosition;
#define UxGetCursorPosition(sw) \
        UxGET_int(sw,UxP_TextFieldRD_cursorPosition,"cursorPosition")
#define UxPutCursorPosition(sw,val) \
        UxPUT_int(sw,UxP_TextFieldRD_cursorPosition,"cursorPosition",val)
#endif

#ifndef UxGetColumns
extern binptr UxP_TextFieldRD_columns;
#define UxGetColumns(sw) \
        UxGET_short(sw,UxP_TextFieldRD_columns,"columns")
#define UxPutColumns(sw,val) \
        UxPUT_short(sw,UxP_TextFieldRD_columns,"columns",val)
#endif

#ifndef UxGetBlinkRate
extern binptr UxP_TextFieldRD_blinkRate;
#define UxGetBlinkRate(sw) \
        UxGET_int(sw,UxP_TextFieldRD_blinkRate,"blinkRate")
#define UxPutBlinkRate(sw,val) \
        UxPUT_int(sw,UxP_TextFieldRD_blinkRate,"blinkRate",val)
#endif

extern Class_t UxC_textField;
#define UxCreateTextField(name,parent) \
        UxCreateSwidget(UxC_textField,name,parent)
#else
#define UxCreateTextField(name,parent) \
        UxCreateSwidget(name,xmTextFieldWidgetClass,parent)
#endif
#endif
