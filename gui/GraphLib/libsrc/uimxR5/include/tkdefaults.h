/* @(#)tkdefaults.h	17.1 (ESO-IPG) 01/25/02 17:26:05 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/

#define UX_DEFAULT_SELECTION_ARRAY \
			"select_position select_word select_line select_all"
#define UX_DEFAULT_SELECTION_ARRAY_COUNT	4
#define UX_DEFAULT_PANEMINIMUM			1
#define UX_DEFAULT_PANEMAXIMUM			1000
#define UX_DEFAULT_SASHHEIGHT   		10
#define UX_DEFAULT_SPACING      		8
#define UX_DEFAULT_MARGINHEIGHT 		3
#define UX_DEFAULT_ROWCOLUMNTYPE 		"work_area"
#define UX_DEFAULT_MAX_DECIMALPOINTS		253
#define UX_DEFAULT_MAXIMUM      		100
#define UX_DEFAULT_MINIMUM      		0
#define UX_DEFAULT_VALUE      			0
#define UX_DEFAULT_SCRBSSRATIO			10
#define UX_DEFAULT_LIST_ITEM_COUNT		0
#define UX_DEFAULT_SB_ORIENTATION			"vertical"
