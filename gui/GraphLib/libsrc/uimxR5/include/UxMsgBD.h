/* @(#)UxMsgBD.h	17.1 (ESO-IPG) 01/25/02 17:26:01 */
#ifndef UXMessageBoxDialog_INCLUDED
#define UXMessageBoxDialog_INCLUDED

#include "Xm/MessageB.h"

#ifdef DESIGN_TIME
#include "UxMsgBox.h"

extern Class_t UxC_messageBoxDialog;
#define UxCreateMessageBoxDialog(name,parent) \
        UxCreateSwidget(UxC_messageBoxDialog,name,parent)
#else
#define UxCreateMessageBoxDialog(name,parent) \
        UxCreateDialogSwidget(name,xmMessageBoxWidgetClass,parent)
#endif
#endif
