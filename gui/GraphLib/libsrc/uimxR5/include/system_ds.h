/* @(#)system_ds.h	17.1 (ESO-IPG) 01/25/02 17:26:05 */
/*---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
#ifndef	_UX_SYSTEM_DS_H_
#define	_UX_SYSTEM_DS_H_
#ifdef	XOPEN_CATALOG
#include	"system_msg.h"
#endif
#define	DS_MS_SYS_U_TASKNAME	"UIM/X"
#define	DS_MS_SYS_RSRC_NAME_UNUSED	"Uimx"
#define	DS_MS_SYS_TASKNAME		"uimx"
#define	DS_MS_SYS_U_TASKDIR	"UIMXDIR"
#define	DS_MS_SYS_U_TASKDIR_ENV_UNUSED	"UIMXDIR="
#define	DS_MS_SYS_TASKDIR		"uimxdir"
#define	DS_MS_SYS_RESOURCE_VAR_UNUSED	"Uimx.uimxdir"
#define	DS_MS_SYS_BINDIR		"bindir"
#define	DS_MS_SYS_CONFIGDIR	"configdir"
#define	DS_MS_SYS_RUNTIME_RSRC_NAME_UNUSED	"Uimxdemo"
#define	DS_MS_SYS_PREFIX	"UIM/X: "
#define	DS_MS_SYS_FULL_NAME        "UIM/X"
#define	DS_MS_SYS_SCCS_VERSION_NUM_UNUSED	"@(#) UIM/X 2.1"
#define	DS_MS_SYS_RSRC_NAME_UNUSED_1	"Uimx2_1"
#define	DS_MS_SYS_RESOURCE_VAR_UNUSED_1	"Uimx2_1.uimxdir"
#define	DS_MS_SYS_SCCS_VERSION_NUM_UNUSED_1	"@(#) UIM/X 2.1 : uimx - User Interface Management System for X "
#define	DS_MS_SYS_CGEN_SCCS_VERSION_NUM_UNUSED	 "@(#) UIM/X 2.1 : uxcgen - C Code Generator"
#define	DS_MS_SYS_READUIL_SCCS_VERSION_NUM_UNUSED "@(#) UIM/X 2.1 : uxreaduil - UIL translator" 
#define	DS_MS_SYS_KEYGEN_SCCS_VERSION_NUM_UNUSED	 "@(#) UIM/X 2.1 : keygen - Key Generator"
#define	DS_MS_SYS_SERVERD_SCCS_VERSION_NUM_UNUSED	 "@(#) UIM/X 2.1 : serverd - Server Daemon"
#define	DS_MS_SYS_RSRC_NAME	"Uimx2_5"
#define	DS_MS_SYS_RESOURCE_VAR	"Uimx2_5.uimxdir"
#define	DS_MS_SYS_RUNTIME_RSRC_NAME	"Uimx2_5"
#endif
