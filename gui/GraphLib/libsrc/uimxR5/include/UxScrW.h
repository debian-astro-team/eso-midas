/* @(#)UxScrW.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXScrolledWindow_INCLUDED
#define UXScrolledWindow_INCLUDED

#include "Xm/ScrolledW.h"

#ifdef DESIGN_TIME
#include "UxMgr.h"

#ifndef UxGetWorkWindow
extern binptr UxP_ScrolledWindowRD_workWindow;
#define UxGetWorkWindow(sw) \
        UxGET_string(sw,UxP_ScrolledWindowRD_workWindow,"workWindow")
#endif

#ifndef UxGetVisualPolicy
extern binptr UxP_ScrolledWindowRD_visualPolicy;
#define UxGetVisualPolicy(sw) \
        UxGET_string(sw,UxP_ScrolledWindowRD_visualPolicy,"visualPolicy")
#define UxPutVisualPolicy(sw,val) \
        UxPUT_string(sw,UxP_ScrolledWindowRD_visualPolicy,"visualPolicy",val)
#endif

#ifndef UxGetVerticalScrollBar
extern binptr UxP_ScrolledWindowRD_verticalScrollBar;
#define UxGetVerticalScrollBar(sw) \
        UxGET_string(sw,UxP_ScrolledWindowRD_verticalScrollBar,"verticalScrollBar")
#endif

#ifndef UxGetSpacing
extern binptr UxP_ScrolledWindowRD_spacing;
#define UxGetSpacing(sw) \
        UxGET_int(sw,UxP_ScrolledWindowRD_spacing,"spacing")
#define UxPutSpacing(sw,val) \
        UxPUT_int(sw,UxP_ScrolledWindowRD_spacing,"spacing",val)
#endif

#ifndef UxGetScrollingPolicy
extern binptr UxP_ScrolledWindowRD_scrollingPolicy;
#define UxGetScrollingPolicy(sw) \
        UxGET_string(sw,UxP_ScrolledWindowRD_scrollingPolicy,"scrollingPolicy")
#define UxPutScrollingPolicy(sw,val) \
        UxPUT_string(sw,UxP_ScrolledWindowRD_scrollingPolicy,"scrollingPolicy",val)
#endif

#ifndef UxGetScrolledWindowMarginWidth
extern binptr UxP_ScrolledWindowRD_scrolledWindowMarginWidth;
#define UxGetScrolledWindowMarginWidth(sw) \
        UxGET_int(sw,UxP_ScrolledWindowRD_scrolledWindowMarginWidth,"scrolledWindowMarginWidth")
#define UxPutScrolledWindowMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_ScrolledWindowRD_scrolledWindowMarginWidth,"scrolledWindowMarginWidth",val)
#endif

#ifndef UxGetScrolledWindowMarginHeight
extern binptr UxP_ScrolledWindowRD_scrolledWindowMarginHeight;
#define UxGetScrolledWindowMarginHeight(sw) \
        UxGET_int(sw,UxP_ScrolledWindowRD_scrolledWindowMarginHeight,"scrolledWindowMarginHeight")
#define UxPutScrolledWindowMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_ScrolledWindowRD_scrolledWindowMarginHeight,"scrolledWindowMarginHeight",val)
#endif

#ifndef UxGetScrollBarPlacement
extern binptr UxP_ScrolledWindowRD_scrollBarPlacement;
#define UxGetScrollBarPlacement(sw) \
        UxGET_string(sw,UxP_ScrolledWindowRD_scrollBarPlacement,"scrollBarPlacement")
#define UxPutScrollBarPlacement(sw,val) \
        UxPUT_string(sw,UxP_ScrolledWindowRD_scrollBarPlacement,"scrollBarPlacement",val)
#endif

#ifndef UxGetScrollBarDisplayPolicy
extern binptr UxP_ScrolledWindowRD_scrollBarDisplayPolicy;
#define UxGetScrollBarDisplayPolicy(sw) \
        UxGET_string(sw,UxP_ScrolledWindowRD_scrollBarDisplayPolicy,"scrollBarDisplayPolicy")
#define UxPutScrollBarDisplayPolicy(sw,val) \
        UxPUT_string(sw,UxP_ScrolledWindowRD_scrollBarDisplayPolicy,"scrollBarDisplayPolicy",val)
#endif

#ifndef UxGetHorizontalScrollBar
extern binptr UxP_ScrolledWindowRD_horizontalScrollBar;
#define UxGetHorizontalScrollBar(sw) \
        UxGET_string(sw,UxP_ScrolledWindowRD_horizontalScrollBar,"horizontalScrollBar")
#endif

#ifndef UxGetClipWindow
extern binptr UxP_ScrolledWindowRD_clipWindow;
#define UxGetClipWindow(sw) \
        UxGET_string(sw,UxP_ScrolledWindowRD_clipWindow,"clipWindow")
#endif

extern Class_t UxC_scrolledWindow;
#define UxCreateScrolledWindow(name,parent) \
        UxCreateSwidget(UxC_scrolledWindow,name,parent)
#else
#define UxCreateScrolledWindow(name,parent) \
        UxCreateSwidget(name,xmScrolledWindowWidgetClass,parent)
#endif
#endif
