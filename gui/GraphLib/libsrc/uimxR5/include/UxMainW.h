/* @(#)UxMainW.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
#ifndef UXMainWindow_INCLUDED
#define UXMainWindow_INCLUDED

#include "Xm/MainW.h"

#ifdef DESIGN_TIME
#include "UxScrW.h"

#ifndef UxGetShowSeparator
extern binptr UxP_MainWindowRD_showSeparator;
#define UxGetShowSeparator(sw) \
        UxGET_string(sw,UxP_MainWindowRD_showSeparator,"showSeparator")
#define UxPutShowSeparator(sw,val) \
        UxPUT_string(sw,UxP_MainWindowRD_showSeparator,"showSeparator",val)
#endif

#ifndef UxGetMessageWindow
extern binptr UxP_MainWindowRD_messageWindow;
#define UxGetMessageWindow(sw) \
        UxGET_string(sw,UxP_MainWindowRD_messageWindow,"messageWindow")
#endif

#ifndef UxGetMenuBar
extern binptr UxP_MainWindowRD_menuBar;
#define UxGetMenuBar(sw) \
        UxGET_string(sw,UxP_MainWindowRD_menuBar,"menuBar")
#endif

#ifndef UxGetMainWindowMarginWidth
extern binptr UxP_MainWindowRD_mainWindowMarginWidth;
#define UxGetMainWindowMarginWidth(sw) \
        UxGET_int(sw,UxP_MainWindowRD_mainWindowMarginWidth,"mainWindowMarginWidth")
#define UxPutMainWindowMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_MainWindowRD_mainWindowMarginWidth,"mainWindowMarginWidth",val)
#endif

#ifndef UxGetMainWindowMarginHeight
extern binptr UxP_MainWindowRD_mainWindowMarginHeight;
#define UxGetMainWindowMarginHeight(sw) \
        UxGET_int(sw,UxP_MainWindowRD_mainWindowMarginHeight,"mainWindowMarginHeight")
#define UxPutMainWindowMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_MainWindowRD_mainWindowMarginHeight,"mainWindowMarginHeight",val)
#endif

#ifndef UxGetCommandWindowLocation
extern binptr UxP_MainWindowRD_commandWindowLocation;
#define UxGetCommandWindowLocation(sw) \
        UxGET_string(sw,UxP_MainWindowRD_commandWindowLocation,"commandWindowLocation")
#define UxPutCommandWindowLocation(sw,val) \
        UxPUT_string(sw,UxP_MainWindowRD_commandWindowLocation,"commandWindowLocation",val)
#endif

#ifndef UxGetCommandWindow
extern binptr UxP_MainWindowRD_commandWindow;
#define UxGetCommandWindow(sw) \
        UxGET_string(sw,UxP_MainWindowRD_commandWindow,"commandWindow")
#endif

extern Class_t UxC_mainWindow;
#define UxCreateMainWindow(name,parent) \
        UxCreateSwidget(UxC_mainWindow,name,parent)
#else
#define UxCreateMainWindow(name,parent) \
        UxCreateSwidget(name,xmMainWindowWidgetClass,parent)
#endif
#endif
