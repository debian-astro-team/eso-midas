/* @(#)UxDrawnB.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXDrawnButton_INCLUDED
#define UXDrawnButton_INCLUDED

#include "Xm/DrawnB.h"

#ifdef DESIGN_TIME
#include "UxLabel.h"

#ifndef UxGetShadowType
extern binptr UxP_DrawnButtonRD_shadowType;
#define UxGetShadowType(sw) \
        UxGET_string(sw,UxP_DrawnButtonRD_shadowType,"shadowType")
#define UxPutShadowType(sw,val) \
        UxPUT_string(sw,UxP_DrawnButtonRD_shadowType,"shadowType",val)
#endif

#ifndef UxGetPushButtonEnabled
extern binptr UxP_DrawnButtonRD_pushButtonEnabled;
#define UxGetPushButtonEnabled(sw) \
        UxGET_string(sw,UxP_DrawnButtonRD_pushButtonEnabled,"pushButtonEnabled")
#define UxPutPushButtonEnabled(sw,val) \
        UxPUT_string(sw,UxP_DrawnButtonRD_pushButtonEnabled,"pushButtonEnabled",val)
#endif

#ifndef UxGetMultiClick
extern binptr UxP_DrawnButtonRD_multiClick;
#define UxGetMultiClick(sw) \
        UxGET_string(sw,UxP_DrawnButtonRD_multiClick,"multiClick")
#define UxPutMultiClick(sw,val) \
        UxPUT_string(sw,UxP_DrawnButtonRD_multiClick,"multiClick",val)
#endif

extern Class_t UxC_drawnButton;
#define UxCreateDrawnButton(name,parent) \
        UxCreateSwidget(UxC_drawnButton,name,parent)
#else
#define UxCreateDrawnButton(name,parent) \
        UxCreateSwidget(name,xmDrawnButtonWidgetClass,parent)
#endif
#endif
