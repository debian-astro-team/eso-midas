/* @(#)uxpm_msg.h	17.1 (ESO-IPG) 01/25/02 17:26:06 */
/*---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
#define	MC_UXPM	5
#define	MS_UXPM_FORMAT	1
#define	MS_UXPM_WIDTH	2
#define	MS_UXPM_HEIGHT	3
#define	MS_UXPM_NCOLORS	4
#define	MS_UXPM_CHARS_PER_PIXEL	5
#define	MS_UXPM_COLOR_ARRAY	6
#define	MS_UXPM_PIXEL_ARRAY	7
#define	MS_UXPM_FIELD_REDEF	8
#define	MS_UXPM_EXPECT_CHAR	9
#define	MS_UXPM_SYNTAX_ERROR	10
#define	MS_UXPM_NAME_MISMATCH	11
#define	MS_UXPM_ILLEG_EXT	12
#define	MS_UXPM_EXPECT_IDENT	13
#define	MS_UXPM_EXPECT_INT	14
#define	MS_UXPM_BAD_FORMAT	15
#define	MS_UXPM_EXPECT_DEFINE	16
#define	MS_UXPM_EXPECT_STRING	17
#define	MS_UXPM_COLORS_REDEF	18
#define	MS_UXPM_UNKNOWN_NCOLORS	19
#define	MS_UXPM_PIXELS_REDEF	20
#define	MS_UXPM_UNKNOWN_HEIGHT	21
#define	MS_UXPM_MISSING_DATA	22
#define	MS_UXPM_BAD_COLORNAME	23
#define	MS_UXPM_BAD_COLORSPEC	24
#define	MS_UXPM_BAD_PIXEL_LEN	25
#define	MS_UXPM_BAD_PIXELSPEC	26
