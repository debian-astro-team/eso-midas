/* @(#)UxTranSh.h	17.1 (ESO-IPG) 01/25/02 17:26:03 */
#ifndef UXTransientShell_INCLUDED
#define UXTransientShell_INCLUDED

#include "X11/Shell.h"

#ifdef DESIGN_TIME
#include "UxVendSh.h"

#ifndef UxGetTransientFor
extern binptr UxP_TransientShellRD_transientFor;
#define UxGetTransientFor(sw) \
        UxGET_string(sw,UxP_TransientShellRD_transientFor,"transientFor")
#define UxPutTransientFor(sw,val) \
        UxPUT_string(sw,UxP_TransientShellRD_transientFor,"transientFor",val)
#endif

extern Class_t UxC_transientShell;
#define UxCreateTransientShell(name,parent) \
        UxCreateSwidget(UxC_transientShell,name,parent)
#else
#define UxCreateTransientShell(name,parent) \
        UxCreateSwidget(name,transientShellWidgetClass,parent)
#endif
#endif
