/* @(#)uxpm_ds.h	17.1 (ESO-IPG) 01/25/02 17:26:06 */
/*---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
#ifndef	_UX_UXPM_DS_H_
#define	_UX_UXPM_DS_H_
#ifdef	XOPEN_CATALOG
#include	"uxpm_msg.h"
#endif
#define	DS_MS_UXPM_FORMAT "341 Unknown paxformat.\n"
#define	DS_MS_UXPM_WIDTH "342 Unknown width.\n"
#define	DS_MS_UXPM_HEIGHT "343 Unknown height.\n"
#define	DS_MS_UXPM_NCOLORS "344 Unknown ncolors.\n"
#define	DS_MS_UXPM_CHARS_PER_PIXEL "345 Unknown chars_per_pixel.\n"
#define	DS_MS_UXPM_COLOR_ARRAY "346 The colors array was not specified.\n"
#define	DS_MS_UXPM_PIXEL_ARRAY "347 The pixels array was not specified.\n"
#define	DS_MS_UXPM_FIELD_REDEF "Warning: %.*s redefined.\n"
#define	DS_MS_UXPM_EXPECT_CHAR "348 Expecting `%c' at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_SYNTAX_ERROR "349 Syntax error at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_NAME_MISMATCH "350 Name mismatch at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_ILLEG_EXT "551 Illegal name extension at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_EXPECT_IDENT "552 Expecting identifier at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_EXPECT_INT "553 Expecting integer constant at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_BAD_FORMAT "554 Incorrect format at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_EXPECT_DEFINE "555 Expecting `define' at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_EXPECT_STRING "556 Expecting string constant at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_COLORS_REDEF "557 The colors array was redefined at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_UNKNOWN_NCOLORS "558 Unknown number of colors at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_PIXELS_REDEF "559 The pixels array was redefined at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_UNKNOWN_HEIGHT "560 Unknown height at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_MISSING_DATA "991 Missing data at or near `%.20s' on line %d.\n"
#define	DS_MS_UXPM_BAD_COLORNAME "992 Cannot parse color name `%.*s'.\n"
#define	DS_MS_UXPM_BAD_COLORSPEC "993 Color specifier \"%.*s\" has illegal length.\n"
#define	DS_MS_UXPM_BAD_PIXEL_LEN "994 Invalid pixel string length in row %d.\n"
#define	DS_MS_UXPM_BAD_PIXELSPEC "995 Invalid pixel character \"%.*s\"\n\
in row %d, column %d.\n"
#endif
