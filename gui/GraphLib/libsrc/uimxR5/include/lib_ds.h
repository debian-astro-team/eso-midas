/* @(#)lib_ds.h	17.1 (ESO-IPG) 01/25/02 17:26:04 */
/*---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
#ifndef	_UX_LIB_DS_H_
#define	_UX_LIB_DS_H_
#ifdef	XOPEN_CATALOG
#include	"lib_msg.h"
#endif
#define	DS_MS_LU_OUTMEMORY	"341 No more memory is available.\n"
#define	DS_MS_LU_FORMATDUMP	"%s = %ld\n"
#define	DS_MS_LU_INSTEXCEEDED      "342 The maximum number of instances for a class is\n\
exceeded in: %s.\n"
#define	DS_MS_LU_MALLOCFAILED	"343 malloc() failed in: %s.\n"
#define	DS_MS_LU_REALLOCFAILED	"344 realloc failed in: %s.\n"
#define	DS_MS_LU_UNDEFERROR	"345 An undefined error occurred in: %s.\n"
#define	DS_MS_LU_SECONDNOTANCESTOR	"346 The second parameter in the call to UxCenterVisibly()\n\
is not an ancestor of the first parameter.\n"
#define	DS_MS_LU_ALDRYREGISTERED	"347 The resource %s is already registered.\n"
#define	DS_MS_LU_WIDGETMIDGETFMT	"%ld = %ld\n"
#endif
