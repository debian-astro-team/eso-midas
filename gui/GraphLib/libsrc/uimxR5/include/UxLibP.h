/* @(#)UxLibP.h	17.1 (ESO-IPG) 01/25/02 17:26:00 */
/*---------------------------------------------------------------------
 * $Date: 2003-06-04 14:49:18 $             $Revision: 1.1.1.1 $
 *---------------------------------------------------------------------
 * 
 *
 *             Copyright (c) 1992, Visual Edge Software Ltd.
 *
 * ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 * distribute  this  software  and its documentation for any purpose
 * and  without  fee  is  hereby  granted,  provided  that the above
 * copyright  notice  appear  in  all  copies  and  that  both  that
 * copyright  notice and this permission notice appear in supporting
 * documentation,  and that  the name of Visual Edge Software not be
 * used  in advertising  or publicity  pertaining to distribution of
 * the software without specific, written prior permission. The year
 * included in the notice is the year of the creation of the work.
 *-------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 * Internal definitions for the UIM/X runtime library.
 *------------------------------------------------------------------------*/

#ifndef _UXLIBP_INCLUDED
#define _UXLIBP_INCLUDED 

typedef struct ShadowWidget
{
	struct ShadowWidget *Self;	/* For safety checks */
	struct ShadowWidget *Parent;	

	char		* Name;		
	WidgetClass	WClass;
	Widget		ItsWidget;	/* Set by UxCreateWidget */
	void		* Context;	/* The context structure ptr */

	ArgList 	Values;		/* Converted resources */
	int		NumValues;	/* Number of resources listed */

	int		UpdateFlag;	/* For Delayed Update */
	WidgetClass	DefaultShell;	/* For implicit shells  */
	int		Flags;		/* See masks below.	*/
	int		IfClassCode;	/* For method extensions */

	char*	Accelerators;		/* Can't do reverse conversion, */
	char*	Translations;		/* so we save the original strings */

} UxShadowWidget_t;


int 	 UxSwidgetSetProp UXPROTO((swidget, char*, XtArgVal));
XtArgVal UxSwidgetGetProp UXPROTO((swidget, char*));

/* 
 * The flag masks.  These must be different powers of two.
 */
#define UXWIN_GROUP_FLAG	1
#define UXCREATE_UNMANAGED_FLAG 2
#define UXDIALOG_FLAG 		4

/*
 * sugar
 */
#define tree_num_children(w)	(((CompositeWidget)(w))->composite.num_children)
#define tree_children(w)	(((CompositeWidget)(w))->composite.children)
#define tree_num_popups(w)      ((w)->core.num_popups)
#define tree_popups(w)		((w)->core.popup_list)

#define x_used(o)                UxSwidgetPropSet(o,XtNx)
#define y_used(o)                UxSwidgetPropSet(o,XtNy)
#define allowShellResize_used(o) UxSwidgetPropSet(o,XtNallowShellResize)

/*
 * private functions in the library
 */
void 	UxAddAccelerators UXPROTO((swidget, String ));
void   	UxAddTranslations  UXPROTO(( swidget, String ));
void    UxFlushQueue  UXPROTO(( void ));
void    UxFreeSwidget UXPROTO(( swidget ));
int	UxNeedsParentShell UXPROTO((swidget));
int	UxSwidgetGetFlag UXPROTO((swidget, int));
void 	UxSwidgetSetFlag UXPROTO((swidget, int));
void	UxSwidgetUnsetFlag UXPROTO((swidget, int));
Widget	UxWindowToWidget   UXPROTO((Window));

extern char     	*UxDefaultShellType;
extern char     	*UxUserShell;

#endif /* _UXLIBP_INCLUDED */
