/* @(#)UxRowCol.h	17.1 (ESO-IPG) 01/25/02 17:26:02 */
#ifndef UXRowColumn_INCLUDED
#define UXRowColumn_INCLUDED

#include "Xm/RowColumn.h"

#ifdef DESIGN_TIME
#include "UxMgr.h"

#ifndef UxGetWhichButton
extern binptr UxP_RowColumnRD_whichButton;
#define UxGetWhichButton(sw) \
        UxGET_int(sw,UxP_RowColumnRD_whichButton,"whichButton")
#define UxPutWhichButton(sw,val) \
        UxPUT_int(sw,UxP_RowColumnRD_whichButton,"whichButton",val)
#endif

#ifndef UxGetTearOffModel
extern binptr UxP_RowColumnRD_tearOffModel;
#define UxGetTearOffModel(sw) \
        UxGET_string(sw,UxP_RowColumnRD_tearOffModel,"tearOffModel")
#define UxPutTearOffModel(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_tearOffModel,"tearOffModel",val)
#endif

#ifndef UxGetSubMenuId
extern binptr UxP_RowColumnRD_subMenuId;
#define UxGetSubMenuId(sw) \
        UxGET_string(sw,UxP_RowColumnRD_subMenuId,"subMenuId")
#define UxPutSubMenuId(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_subMenuId,"subMenuId",val)
#endif

#ifndef UxGetSpacing
extern binptr UxP_RowColumnRD_spacing;
#define UxGetSpacing(sw) \
        UxGET_int(sw,UxP_RowColumnRD_spacing,"spacing")
#define UxPutSpacing(sw,val) \
        UxPUT_int(sw,UxP_RowColumnRD_spacing,"spacing",val)
#endif

#ifndef UxGetRowColumnType
extern binptr UxP_RowColumnRD_rowColumnType;
#define UxGetRowColumnType(sw) \
        UxGET_string(sw,UxP_RowColumnRD_rowColumnType,"rowColumnType")
#define UxPutRowColumnType(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_rowColumnType,"rowColumnType",val)
#endif

#ifndef UxGetResizeWidth
extern binptr UxP_RowColumnRD_resizeWidth;
#define UxGetResizeWidth(sw) \
        UxGET_string(sw,UxP_RowColumnRD_resizeWidth,"resizeWidth")
#define UxPutResizeWidth(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_resizeWidth,"resizeWidth",val)
#endif

#ifndef UxGetResizeHeight
extern binptr UxP_RowColumnRD_resizeHeight;
#define UxGetResizeHeight(sw) \
        UxGET_string(sw,UxP_RowColumnRD_resizeHeight,"resizeHeight")
#define UxPutResizeHeight(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_resizeHeight,"resizeHeight",val)
#endif

#ifndef UxGetRadioBehavior
extern binptr UxP_RowColumnRD_radioBehavior;
#define UxGetRadioBehavior(sw) \
        UxGET_string(sw,UxP_RowColumnRD_radioBehavior,"radioBehavior")
#define UxPutRadioBehavior(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_radioBehavior,"radioBehavior",val)
#endif

#ifndef UxGetRadioAlwaysOne
extern binptr UxP_RowColumnRD_radioAlwaysOne;
#define UxGetRadioAlwaysOne(sw) \
        UxGET_string(sw,UxP_RowColumnRD_radioAlwaysOne,"radioAlwaysOne")
#define UxPutRadioAlwaysOne(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_radioAlwaysOne,"radioAlwaysOne",val)
#endif

#ifndef UxGetPositionIndex
extern binptr UxP_RowColumnRD_positionIndex;
#define UxGetPositionIndex(sw) \
        UxGET_short(sw,UxP_RowColumnRD_positionIndex,"positionIndex")
#define UxPutPositionIndex(sw,val) \
        UxPUT_short(sw,UxP_RowColumnRD_positionIndex,"positionIndex",val)
#endif

#ifndef UxGetPopupEnabled
extern binptr UxP_RowColumnRD_popupEnabled;
#define UxGetPopupEnabled(sw) \
        UxGET_string(sw,UxP_RowColumnRD_popupEnabled,"popupEnabled")
#define UxPutPopupEnabled(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_popupEnabled,"popupEnabled",val)
#endif

#ifndef UxGetPacking
extern binptr UxP_RowColumnRD_packing;
#define UxGetPacking(sw) \
        UxGET_string(sw,UxP_RowColumnRD_packing,"packing")
#define UxPutPacking(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_packing,"packing",val)
#endif

#ifndef UxGetOrientation
extern binptr UxP_RowColumnRD_orientation;
#define UxGetOrientation(sw) \
        UxGET_string(sw,UxP_RowColumnRD_orientation,"orientation")
#define UxPutOrientation(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_orientation,"orientation",val)
#endif

#ifndef UxGetNumColumns
extern binptr UxP_RowColumnRD_numColumns;
#define UxGetNumColumns(sw) \
        UxGET_short(sw,UxP_RowColumnRD_numColumns,"numColumns")
#define UxPutNumColumns(sw,val) \
        UxPUT_short(sw,UxP_RowColumnRD_numColumns,"numColumns",val)
#endif

#ifndef UxGetMnemonicCharSet
extern binptr UxP_RowColumnRD_mnemonicCharSet;
#define UxGetMnemonicCharSet(sw) \
        UxGET_string(sw,UxP_RowColumnRD_mnemonicCharSet,"mnemonicCharSet")
#define UxPutMnemonicCharSet(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_mnemonicCharSet,"mnemonicCharSet",val)
#endif

#ifndef UxGetMnemonic
extern binptr UxP_RowColumnRD_mnemonic;
#define UxGetMnemonic(sw) \
        UxGET_string(sw,UxP_RowColumnRD_mnemonic,"mnemonic")
#define UxPutMnemonic(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_mnemonic,"mnemonic",val)
#endif

#ifndef UxGetMenuPost
extern binptr UxP_RowColumnRD_menuPost;
#define UxGetMenuPost(sw) \
        UxGET_string(sw,UxP_RowColumnRD_menuPost,"menuPost")
#define UxPutMenuPost(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_menuPost,"menuPost",val)
#endif

#ifndef UxGetMenuHistory
extern binptr UxP_RowColumnRD_menuHistory;
#define UxGetMenuHistory(sw) \
        UxGET_string(sw,UxP_RowColumnRD_menuHistory,"menuHistory")
#define UxPutMenuHistory(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_menuHistory,"menuHistory",val)
#endif

#ifndef UxGetMenuHelpWidget
extern binptr UxP_RowColumnRD_menuHelpWidget;
#define UxGetMenuHelpWidget(sw) \
        UxGET_string(sw,UxP_RowColumnRD_menuHelpWidget,"menuHelpWidget")
#define UxPutMenuHelpWidget(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_menuHelpWidget,"menuHelpWidget",val)
#endif

#ifndef UxGetMenuAccelerator
extern binptr UxP_RowColumnRD_menuAccelerator;
#define UxGetMenuAccelerator(sw) \
        UxGET_string(sw,UxP_RowColumnRD_menuAccelerator,"menuAccelerator")
#define UxPutMenuAccelerator(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_menuAccelerator,"menuAccelerator",val)
#endif

#ifndef UxGetMarginWidth
extern binptr UxP_RowColumnRD_marginWidth;
#define UxGetMarginWidth(sw) \
        UxGET_int(sw,UxP_RowColumnRD_marginWidth,"marginWidth")
#define UxPutMarginWidth(sw,val) \
        UxPUT_int(sw,UxP_RowColumnRD_marginWidth,"marginWidth",val)
#endif

#ifndef UxGetMarginHeight
extern binptr UxP_RowColumnRD_marginHeight;
#define UxGetMarginHeight(sw) \
        UxGET_int(sw,UxP_RowColumnRD_marginHeight,"marginHeight")
#define UxPutMarginHeight(sw,val) \
        UxPUT_int(sw,UxP_RowColumnRD_marginHeight,"marginHeight",val)
#endif

#ifndef UxGetLabelString
extern binptr UxP_RowColumnRD_labelString;
#define UxGetLabelString(sw) \
        UxGET_string(sw,UxP_RowColumnRD_labelString,"labelString")
#define UxPutLabelString(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_labelString,"labelString",val)
#endif

#ifndef UxGetIsHomogeneous
extern binptr UxP_RowColumnRD_isHomogeneous;
#define UxGetIsHomogeneous(sw) \
        UxGET_string(sw,UxP_RowColumnRD_isHomogeneous,"isHomogeneous")
#define UxPutIsHomogeneous(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_isHomogeneous,"isHomogeneous",val)
#endif

#ifndef UxGetIsAligned
extern binptr UxP_RowColumnRD_isAligned;
#define UxGetIsAligned(sw) \
        UxGET_string(sw,UxP_RowColumnRD_isAligned,"isAligned")
#define UxPutIsAligned(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_isAligned,"isAligned",val)
#endif

#ifndef UxGetEntryVerticalAlignment
extern binptr UxP_RowColumnRD_entryVerticalAlignment;
#define UxGetEntryVerticalAlignment(sw) \
        UxGET_string(sw,UxP_RowColumnRD_entryVerticalAlignment,"entryVerticalAlignment")
#define UxPutEntryVerticalAlignment(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_entryVerticalAlignment,"entryVerticalAlignment",val)
#endif

#ifndef UxGetEntryClass
extern binptr UxP_RowColumnRD_entryClass;
#define UxGetEntryClass(sw) \
        UxGET_string(sw,UxP_RowColumnRD_entryClass,"entryClass")
#define UxPutEntryClass(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_entryClass,"entryClass",val)
#endif

#ifndef UxGetEntryBorder
extern binptr UxP_RowColumnRD_entryBorder;
#define UxGetEntryBorder(sw) \
        UxGET_int(sw,UxP_RowColumnRD_entryBorder,"entryBorder")
#define UxPutEntryBorder(sw,val) \
        UxPUT_int(sw,UxP_RowColumnRD_entryBorder,"entryBorder",val)
#endif

#ifndef UxGetEntryAlignment
extern binptr UxP_RowColumnRD_entryAlignment;
#define UxGetEntryAlignment(sw) \
        UxGET_string(sw,UxP_RowColumnRD_entryAlignment,"entryAlignment")
#define UxPutEntryAlignment(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_entryAlignment,"entryAlignment",val)
#endif

#ifndef UxGetAdjustMargin
extern binptr UxP_RowColumnRD_adjustMargin;
#define UxGetAdjustMargin(sw) \
        UxGET_string(sw,UxP_RowColumnRD_adjustMargin,"adjustMargin")
#define UxPutAdjustMargin(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_adjustMargin,"adjustMargin",val)
#endif

#ifndef UxGetAdjustLast
extern binptr UxP_RowColumnRD_adjustLast;
#define UxGetAdjustLast(sw) \
        UxGET_string(sw,UxP_RowColumnRD_adjustLast,"adjustLast")
#define UxPutAdjustLast(sw,val) \
        UxPUT_string(sw,UxP_RowColumnRD_adjustLast,"adjustLast",val)
#endif

extern Class_t UxC_rowColumn;
#define UxCreateRowColumn(name,parent) \
        UxCreateSwidget(UxC_rowColumn,name,parent)
#else
#define UxCreateRowColumn(name,parent) \
        UxCreateSwidget(name,xmRowColumnWidgetClass,parent)
#endif
#endif
