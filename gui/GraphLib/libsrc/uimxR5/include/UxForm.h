/* @(#)UxForm.h	17.1 (ESO-IPG) 01/25/02 17:25:59 */
#ifndef UXForm_INCLUDED
#define UXForm_INCLUDED

#include "Xm/Form.h"

#ifdef DESIGN_TIME
#include "UxBboard.h"

#ifndef UxGetTopWidget
extern binptr UxP_FormRD_topWidget;
#define UxGetTopWidget(sw) \
        UxGET_string(sw,UxP_FormRD_topWidget,"topWidget")
#define UxPutTopWidget(sw,val) \
        UxPUT_string(sw,UxP_FormRD_topWidget,"topWidget",val)
#endif

#ifndef UxGetTopPosition
extern binptr UxP_FormRD_topPosition;
#define UxGetTopPosition(sw) \
        UxGET_int(sw,UxP_FormRD_topPosition,"topPosition")
#define UxPutTopPosition(sw,val) \
        UxPUT_int(sw,UxP_FormRD_topPosition,"topPosition",val)
#endif

#ifndef UxGetTopOffset
extern binptr UxP_FormRD_topOffset;
#define UxGetTopOffset(sw) \
        UxGET_int(sw,UxP_FormRD_topOffset,"topOffset")
#define UxPutTopOffset(sw,val) \
        UxPUT_int(sw,UxP_FormRD_topOffset,"topOffset",val)
#endif

#ifndef UxGetTopAttachment
extern binptr UxP_FormRD_topAttachment;
#define UxGetTopAttachment(sw) \
        UxGET_string(sw,UxP_FormRD_topAttachment,"topAttachment")
#define UxPutTopAttachment(sw,val) \
        UxPUT_string(sw,UxP_FormRD_topAttachment,"topAttachment",val)
#endif

#ifndef UxGetRightWidget
extern binptr UxP_FormRD_rightWidget;
#define UxGetRightWidget(sw) \
        UxGET_string(sw,UxP_FormRD_rightWidget,"rightWidget")
#define UxPutRightWidget(sw,val) \
        UxPUT_string(sw,UxP_FormRD_rightWidget,"rightWidget",val)
#endif

#ifndef UxGetRightPosition
extern binptr UxP_FormRD_rightPosition;
#define UxGetRightPosition(sw) \
        UxGET_int(sw,UxP_FormRD_rightPosition,"rightPosition")
#define UxPutRightPosition(sw,val) \
        UxPUT_int(sw,UxP_FormRD_rightPosition,"rightPosition",val)
#endif

#ifndef UxGetRightOffset
extern binptr UxP_FormRD_rightOffset;
#define UxGetRightOffset(sw) \
        UxGET_int(sw,UxP_FormRD_rightOffset,"rightOffset")
#define UxPutRightOffset(sw,val) \
        UxPUT_int(sw,UxP_FormRD_rightOffset,"rightOffset",val)
#endif

#ifndef UxGetRightAttachment
extern binptr UxP_FormRD_rightAttachment;
#define UxGetRightAttachment(sw) \
        UxGET_string(sw,UxP_FormRD_rightAttachment,"rightAttachment")
#define UxPutRightAttachment(sw,val) \
        UxPUT_string(sw,UxP_FormRD_rightAttachment,"rightAttachment",val)
#endif

#ifndef UxGetResizable
extern binptr UxP_FormRD_resizable;
#define UxGetResizable(sw) \
        UxGET_string(sw,UxP_FormRD_resizable,"resizable")
#define UxPutResizable(sw,val) \
        UxPUT_string(sw,UxP_FormRD_resizable,"resizable",val)
#endif

#ifndef UxGetLeftWidget
extern binptr UxP_FormRD_leftWidget;
#define UxGetLeftWidget(sw) \
        UxGET_string(sw,UxP_FormRD_leftWidget,"leftWidget")
#define UxPutLeftWidget(sw,val) \
        UxPUT_string(sw,UxP_FormRD_leftWidget,"leftWidget",val)
#endif

#ifndef UxGetLeftPosition
extern binptr UxP_FormRD_leftPosition;
#define UxGetLeftPosition(sw) \
        UxGET_int(sw,UxP_FormRD_leftPosition,"leftPosition")
#define UxPutLeftPosition(sw,val) \
        UxPUT_int(sw,UxP_FormRD_leftPosition,"leftPosition",val)
#endif

#ifndef UxGetLeftOffset
extern binptr UxP_FormRD_leftOffset;
#define UxGetLeftOffset(sw) \
        UxGET_int(sw,UxP_FormRD_leftOffset,"leftOffset")
#define UxPutLeftOffset(sw,val) \
        UxPUT_int(sw,UxP_FormRD_leftOffset,"leftOffset",val)
#endif

#ifndef UxGetLeftAttachment
extern binptr UxP_FormRD_leftAttachment;
#define UxGetLeftAttachment(sw) \
        UxGET_string(sw,UxP_FormRD_leftAttachment,"leftAttachment")
#define UxPutLeftAttachment(sw,val) \
        UxPUT_string(sw,UxP_FormRD_leftAttachment,"leftAttachment",val)
#endif

#ifndef UxGetBottomWidget
extern binptr UxP_FormRD_bottomWidget;
#define UxGetBottomWidget(sw) \
        UxGET_string(sw,UxP_FormRD_bottomWidget,"bottomWidget")
#define UxPutBottomWidget(sw,val) \
        UxPUT_string(sw,UxP_FormRD_bottomWidget,"bottomWidget",val)
#endif

#ifndef UxGetBottomPosition
extern binptr UxP_FormRD_bottomPosition;
#define UxGetBottomPosition(sw) \
        UxGET_int(sw,UxP_FormRD_bottomPosition,"bottomPosition")
#define UxPutBottomPosition(sw,val) \
        UxPUT_int(sw,UxP_FormRD_bottomPosition,"bottomPosition",val)
#endif

#ifndef UxGetBottomOffset
extern binptr UxP_FormRD_bottomOffset;
#define UxGetBottomOffset(sw) \
        UxGET_int(sw,UxP_FormRD_bottomOffset,"bottomOffset")
#define UxPutBottomOffset(sw,val) \
        UxPUT_int(sw,UxP_FormRD_bottomOffset,"bottomOffset",val)
#endif

#ifndef UxGetBottomAttachment
extern binptr UxP_FormRD_bottomAttachment;
#define UxGetBottomAttachment(sw) \
        UxGET_string(sw,UxP_FormRD_bottomAttachment,"bottomAttachment")
#define UxPutBottomAttachment(sw,val) \
        UxPUT_string(sw,UxP_FormRD_bottomAttachment,"bottomAttachment",val)
#endif

#ifndef UxGetVerticalSpacing
extern binptr UxP_FormRD_verticalSpacing;
#define UxGetVerticalSpacing(sw) \
        UxGET_int(sw,UxP_FormRD_verticalSpacing,"verticalSpacing")
#define UxPutVerticalSpacing(sw,val) \
        UxPUT_int(sw,UxP_FormRD_verticalSpacing,"verticalSpacing",val)
#endif

#ifndef UxGetRubberPositioning
extern binptr UxP_FormRD_rubberPositioning;
#define UxGetRubberPositioning(sw) \
        UxGET_string(sw,UxP_FormRD_rubberPositioning,"rubberPositioning")
#define UxPutRubberPositioning(sw,val) \
        UxPUT_string(sw,UxP_FormRD_rubberPositioning,"rubberPositioning",val)
#endif

#ifndef UxGetHorizontalSpacing
extern binptr UxP_FormRD_horizontalSpacing;
#define UxGetHorizontalSpacing(sw) \
        UxGET_int(sw,UxP_FormRD_horizontalSpacing,"horizontalSpacing")
#define UxPutHorizontalSpacing(sw,val) \
        UxPUT_int(sw,UxP_FormRD_horizontalSpacing,"horizontalSpacing",val)
#endif

#ifndef UxGetFractionBase
extern binptr UxP_FormRD_fractionBase;
#define UxGetFractionBase(sw) \
        UxGET_int(sw,UxP_FormRD_fractionBase,"fractionBase")
#define UxPutFractionBase(sw,val) \
        UxPUT_int(sw,UxP_FormRD_fractionBase,"fractionBase",val)
#endif

extern Class_t UxC_form;
#define UxCreateForm(name,parent) \
        UxCreateSwidget(UxC_form,name,parent)
#else
#define UxCreateForm(name,parent) \
        UxCreateSwidget(name,xmFormWidgetClass,parent)
#endif
#endif
