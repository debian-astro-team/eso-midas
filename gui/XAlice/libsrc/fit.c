/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* .VERSION 

   090420	last modif	
*/


#include <stdio.h>
#include <alice_global.h>
#include <alice_util.h>

double avex,avey,adevx,adevy,sdevx,sdevy,svarx,svary,skew,curt,xm,ym;

void poly_fit(xv,yv,npt,n)
double xv[],yv[];
int npt,n;
{
 int *lista;
 int i;
 double chisq,*a,*sig,**covar,*powx;
 void fpoly(), Alfit();


 lista = ivector(1,n);
 a     = dvector(1,n);
 sig   = dvector(1,npt);
 covar = dmatrix(1,n,1,n);
 powx  = dvector(1,n);
 for(i=1;i<=npt;i++)
  sig[i]= 1.0;
 for(i=1;i<=n;i++)
  lista[i]=i;
 Alfit(xv,yv,sig,npt,a,n,lista,n,covar,&chisq,fpoly);
 for(i=1;i<= n ;i++)
  fitPolyValues[i-1] = a[i];
 free_dvector(powx,1,n);
 free_dmatrix(covar,1,n,1,n);
 free_dvector(sig,1,npt);
 free_ivector(lista,1,n);
 free_dvector(a,1,n);
}
