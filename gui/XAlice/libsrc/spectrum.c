/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION

090420		last modif
===========================================================================*/


#include <stdio.h>
#include <math.h>

int mean_filter(ina,len,width,outa)
float *ina, *outa;
int len,width;

{
 double sum;
 int midpt,i,j,k,endpt;
 midpt = width/2;
 endpt = len - midpt;
 sum =0.0;
 for(i=0;i < width ; i ++)
  sum += ina[i]; 
 for(i=0,k=width,j=midpt; j <endpt; j++,i++,k++)
 {
  outa[j] = sum / width;
  sum += ina[k] - ina[i];
 }
 for(i=0;i<midpt ; i++)
 {
  outa[i]           = outa[midpt];
  outa[len - i - 1] = outa[len- midpt - 1];
 }

return 0;
}

int median_filter(ina,len,width,outa)
float *ina, *outa;
int len,width;

{
 float xxx[1000];
 float xmed;
 int midpt,i,j,k,endpt,ii;

 void mdian1();


 midpt = width/2;
 endpt = len - midpt;
 for(i=0,k=width,j=midpt; j <endpt; j++,i++,k++)
 {
  for(ii=0;ii<width;ii++)
  xxx[ii+1] = ina[i+ii];
  /*mdian3(ina,i,k-1,&xmed);*/
  mdian1(xxx,width,&xmed);
  outa[j] = xmed;
 }
 for(i=0;i<midpt ; i++)
 {
  outa[i]           = outa[midpt];
  outa[len - i - 1] = outa[len- midpt - 1];
 }

return 0;
}

int rebin(opt,xin,xout,yin,yout,n,Z,Xmin,Xmax,Ymin,Ymax)
int opt,n;
float *xin,*xout,*yin,*yout,Z,*Xmin,*Xmax,*Ymin,*Ymax;

{
 int i,istart=0;
 float xstep,splint(),auxval;
 switch(opt)
  {
  case 1:
   for(i=0;i<n;i++)
     xin[i] = xin[i]/(1+Z);
    break;
  case 2:
   for(i=0;i<n;i++)
     xin[i] = log(xin[i]);
    break;
  case 3:
   for(i=0;i<n;i++)
     xin[i] = 1/xin[i];
    break;
  } 
  if(xin[0] < xin[n-1])
  {
   xstep = (xin[n-1] -xin[0]) / n;
   xout[0] = xin[0];
  }
  else
  {
   xstep = (xin[0] -xin[n-1]) / n;
   xout[0] = xin[n-1];
   for(i=0;i< n/2 ; i++)
    {
     auxval = xin[i];
     xin[i] = xin[n-i-1];
     xin[n-i-1]=auxval;
     auxval =yin[i];
     yin[i] =yin[n-i-1];
     yin[n-i-1]=auxval;
    }
  }
  for(i=1; i< n ; i++)
   xout[i] = xout[i-1]+xstep;
  *Xmin = xout[0];
  *Xmax = xout[n-1];
  yout[0] = yin[0]; 
  *Ymin = yout[0];
  *Ymax = yout[0];
  for(i=1; i< n-1 ; i++)
   {
    yout[i] = splint(xout[i],xin,yin,n,&istart); 
    if(yout[i] < *Ymin) *Ymin = yout[i];
    if(yout[i] > *Ymax) *Ymax = yout[i];
   }
  yout[n-1] = yin[n-1]; 

return 0;
}
