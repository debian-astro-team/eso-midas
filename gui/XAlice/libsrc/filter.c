/* @(#)filter.c	19.1 (ES0-DMD) 02/25/03 13:43:11 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)filter.c	19.1  (ESO)  02/25/03  13:43:11 */

#include <math.h>
mean_filter(ina,len,width,outa)
float *ina, *outa;
int len,width;
{
 double sum;
 int midpt,i,j,k,endpt;
 midpt = width/2;
 endpt = len - midpt;
 sum =0.0;
 for(i=0;i < width ; i ++)
  sum += ina[i]; 
 for(i=0,k=width,j=midpt; j <endpt; j++,i++,k++)
 {
  outa[j] = sum / width;
  sum += ina[k] - ina[i];
 }
 for(i=0;i<midpt ; i++)
 {
  outa[i]           = outa[midpt];
  outa[len - i - 1] = outa[len- midpt - 1];
 }
}

median_filter(ina,len,width,outa)
float *ina, *outa;
int len,width;
{
 float xxx[1000];
 float xmed;
 int midpt,i,j,k,endpt,ii;
 midpt = width/2;
 endpt = len - midpt;
 for(i=0,k=width,j=midpt; j <endpt; j++,i++,k++)
 {
  for(ii=0;ii<width;ii++)
  xxx[ii+1] = ina[i+ii];
  /*mdian3(ina,i,k-1,&xmed);*/
  mdian1(xxx,width,&xmed);
  outa[j] = xmed;
 }
 for(i=0;i<midpt ; i++)
 {
  outa[i]           = outa[midpt];
  outa[len - i - 1] = outa[len- midpt - 1];
 }
}
