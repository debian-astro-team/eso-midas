/* @(#)moment.c	19.1 (ES0-DMD) 02/25/03 13:43:13 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* @(#)moment.c	19.1  (ESO)  02/25/03  13:43:13 */

#include <math.h>

void moment(data,n,ave,adev,sdev,svar,skew,curt)
int n;
double *ave,*adev,*sdev,*svar,*skew,*curt;
float data[];
{
	int j;
	double s,p;
	void nrerror();

	if (n <= 1) nrerror("n must be at least 2 in MOMENT");
	s=0.0;
	for (j=1;j<=n;j++) s += (double)data[j];
	*ave=s/n;
	*adev=(*svar)=(*skew)=(*curt)=0.0;
	for (j=1;j<=n;j++) {
		*adev += fabs(s=(double)data[j]-(*ave));
		*svar += (p=s*s);
		*skew += (p *= s);
		*curt += (p *= s);
	}
	*adev /= n;
	*svar /= (n-1);
	*sdev=sqrt(*svar);
	if (*svar) {
		*skew /= (n*(*svar)*(*sdev));
		*curt=(*curt)/(n*(*svar)*(*svar))-3.0;
	} else nrerror("No skew/kurtosis when variance = 0 (in MOMENT)");
}
