/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/
 


/*******************************************************************************
	LabelCursor.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxArrB.h"
#include "UxLabel.h"
#include "UxText.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxLabelCursor;
	swidget	Uxform10;
	swidget	Uxform11;
	swidget	UxOkLabelCursor;
	swidget	UxCancelLabel;
	swidget	UxDefaultLabel;
	swidget	Uxseparator2;
	swidget	UxLabelText;
	swidget	Uxlabel13;
	swidget	UxAngleText;
	swidget	UxSizeText;
	swidget	UxFontText;
	swidget	Uxlabel14;
	swidget	Uxlabel15;
	swidget	Uxlabel16;
	swidget	UxarrowButton13;
	swidget	UxarrowButton14;
} _UxCLabelCursor;

#define LabelCursor             UxLabelCursorContext->UxLabelCursor
#define form10                  UxLabelCursorContext->Uxform10
#define form11                  UxLabelCursorContext->Uxform11
#define OkLabelCursor           UxLabelCursorContext->UxOkLabelCursor
#define CancelLabel             UxLabelCursorContext->UxCancelLabel
#define DefaultLabel            UxLabelCursorContext->UxDefaultLabel
#define separator2              UxLabelCursorContext->Uxseparator2
#define LabelText               UxLabelCursorContext->UxLabelText
#define label13                 UxLabelCursorContext->Uxlabel13
#define AngleText               UxLabelCursorContext->UxAngleText
#define SizeText                UxLabelCursorContext->UxSizeText
#define FontText                UxLabelCursorContext->UxFontText
#define label14                 UxLabelCursorContext->Uxlabel14
#define label15                 UxLabelCursorContext->Uxlabel15
#define label16                 UxLabelCursorContext->Uxlabel16
#define arrowButton13           UxLabelCursorContext->UxarrowButton13
#define arrowButton14           UxLabelCursorContext->UxarrowButton14

static _UxCLabelCursor	*UxLabelCursorContext;

extern void  labelCursor(), putFontText(), out_error();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*CursorTab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_LabelCursor();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_OkLabelCursor( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelCursor         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelCursorContext;
	UxLabelCursorContext = UxContext =
			(_UxCLabelCursor *) UxGetContext( UxThisWidget );
	{
	char label[240];
	strcpy(label, UxGetText(UxFindSwidget("LabelText")));
	labelCursor(label,plotAngle,plotSize,plotFont);
	 
	
	 
	 
	}
	UxLabelCursorContext = UxSaveCtx;
}

static void	activateCB_CancelLabel( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelCursor         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelCursorContext;
	UxLabelCursorContext = UxContext =
			(_UxCLabelCursor *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(LabelCursor);
	}
	UxLabelCursorContext = UxSaveCtx;
}

static void	activateCB_DefaultLabel( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelCursor         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelCursorContext;
	UxLabelCursorContext = UxContext =
			(_UxCLabelCursor *) UxGetContext( UxThisWidget );
	{
	plotFont = 0;
	plotAngle = 0.0;
	plotSize = 1.5;
	UxPutText(UxFindSwidget("AngleText"),"0");
	UxPutText(UxFindSwidget("SizeText"),"1.5");
	UxPutText(UxFindSwidget("FontText"),"Standard");
	UxPutText(UxFindSwidget("LabelText"),"");
	}
	UxLabelCursorContext = UxSaveCtx;
}

static void	modifyVerifyCB_AngleText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelCursor         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelCursorContext;
	UxLabelCursorContext = UxContext =
			(_UxCLabelCursor *) UxGetContext( UxThisWidget );
	{
	plotModAngle = TRUE;
	}
	UxLabelCursorContext = UxSaveCtx;
}

static void	losingFocusCB_AngleText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelCursor         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelCursorContext;
	UxLabelCursorContext = UxContext =
			(_UxCLabelCursor *) UxGetContext( UxThisWidget );
	{
	float aux_angle;
	char out[20];
	if( plotModAngle)
	{ plotModAngle = FALSE;
	  if(sscanf(UxGetText( UxFindSwidget("AngleText") ) ,"%f",&aux_angle)==1)
	   plotAngle = aux_angle;
	  else
	  {
	  sprintf(out,"%g",plotAngle);
	  UxPutText( UxFindSwidget("AngleText") ,out); 
	  out_error("Invalid angle input");
	  }  
	}
	}
	UxLabelCursorContext = UxSaveCtx;
}

static void	modifyVerifyCB_SizeText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelCursor         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelCursorContext;
	UxLabelCursorContext = UxContext =
			(_UxCLabelCursor *) UxGetContext( UxThisWidget );
	{
	plotModSize = TRUE;
	}
	UxLabelCursorContext = UxSaveCtx;
}

static void	losingFocusCB_SizeText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelCursor         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelCursorContext;
	UxLabelCursorContext = UxContext =
			(_UxCLabelCursor *) UxGetContext( UxThisWidget );
	{
	float aux_size;
	char out[20];
	if( plotModSize)
	{ plotModSize = FALSE;
	  if(sscanf(UxGetText( UxFindSwidget("SizeText" )) ,"%f",&aux_size)==1)
	   plotSize = aux_size;
	  else
	  {
	  sprintf(out,"%g",plotSize);
	  UxPutText(UxFindSwidget("SizeText") ,out); 
	  out_error("Invalid size input");
	  }  
	}
	}
	UxLabelCursorContext = UxSaveCtx;
}

static void	activateCB_arrowButton13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelCursor         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelCursorContext;
	UxLabelCursorContext = UxContext =
			(_UxCLabelCursor *) UxGetContext( UxThisWidget );
	{
	if(plotFont < 5)
	 plotFont++;
	else 
	 plotFont = 0;
	putFontText(plotFont);
	}
	UxLabelCursorContext = UxSaveCtx;
}

static void	activateCB_arrowButton14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelCursor         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelCursorContext;
	UxLabelCursorContext = UxContext =
			(_UxCLabelCursor *) UxGetContext( UxThisWidget );
	{
	if(plotFont > 1)
	 plotFont--;
	else 
	 plotFont = 5;
	putFontText(plotFont);
	}
	UxLabelCursorContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_LabelCursor()
{
	UxPutIconName( LabelCursor, "Label Cursor" );
	UxPutKeyboardFocusPolicy( LabelCursor, "pointer" );
	UxPutHeight( LabelCursor, 148 );
	UxPutWidth( LabelCursor, 384 );
	UxPutY( LabelCursor, 448 );
	UxPutX( LabelCursor, 533 );

	UxPutBackground( form10, WindowBackground );
	UxPutHeight( form10, 98 );
	UxPutWidth( form10, 182 );
	UxPutY( form10, 16 );
	UxPutX( form10, 52 );
	UxPutUnitType( form10, "pixels" );
	UxPutResizePolicy( form10, "resize_none" );

	UxPutBackground( form11, ButtonBackground );
	UxPutHeight( form11, 52 );
	UxPutWidth( form11, 386 );
	UxPutY( form11, 96 );
	UxPutX( form11, 0 );
	UxPutResizePolicy( form11, "resize_none" );

	UxPutHighlightColor( OkLabelCursor, "Black" );
	UxPutHighlightOnEnter( OkLabelCursor, "true" );
	UxPutLabelString( OkLabelCursor, "Ok" );
	UxPutForeground( OkLabelCursor, ButtonForeground );
	UxPutFontList( OkLabelCursor, BoldTextFont );
	UxPutBackground( OkLabelCursor, ButtonBackground );
	UxPutHeight( OkLabelCursor, 30 );
	UxPutWidth( OkLabelCursor, 90 );
	UxPutY( OkLabelCursor, 12 );
	UxPutX( OkLabelCursor, 132 );

	UxPutHighlightColor( CancelLabel, "Black" );
	UxPutHighlightOnEnter( CancelLabel, "true" );
	UxPutLabelString( CancelLabel, "Cancel" );
	UxPutForeground( CancelLabel, CancelForeground );
	UxPutFontList( CancelLabel, BoldTextFont );
	UxPutBackground( CancelLabel, ButtonBackground );
	UxPutHeight( CancelLabel, 30 );
	UxPutWidth( CancelLabel, 90 );
	UxPutY( CancelLabel, 12 );
	UxPutX( CancelLabel, 240 );

	UxPutHighlightColor( DefaultLabel, "Black" );
	UxPutHighlightOnEnter( DefaultLabel, "true" );
	UxPutLabelString( DefaultLabel, "Default" );
	UxPutForeground( DefaultLabel, ButtonForeground );
	UxPutFontList( DefaultLabel, BoldTextFont );
	UxPutBackground( DefaultLabel, ButtonBackground );
	UxPutHeight( DefaultLabel, 30 );
	UxPutWidth( DefaultLabel, 90 );
	UxPutY( DefaultLabel, 12 );
	UxPutX( DefaultLabel, 24 );

	UxPutBackground( separator2, ButtonBackground );
	UxPutHeight( separator2, 6 );
	UxPutWidth( separator2, 386 );
	UxPutY( separator2, 90 );
	UxPutX( separator2, -2 );

	UxPutMarginHeight( LabelText, 3 );
	UxPutHighlightColor( LabelText, "Black" );
	UxPutForeground( LabelText, TextForeground );
	UxPutTranslations( LabelText, CursorTab );
	UxPutFontList( LabelText, TextFont );
	UxPutHighlightOnEnter( LabelText, "true" );
	UxPutMaxLength( LabelText, 240 );
	UxPutBackground( LabelText, TextBackground );
	UxPutHeight( LabelText, 32 );
	UxPutWidth( LabelText, 316 );
	UxPutY( LabelText, 6 );
	UxPutX( LabelText, 60 );

	UxPutHighlightColor( label13, "Black" );
	UxPutForeground( label13, TextForeground );
	UxPutFontList( label13, TextFont );
	UxPutLabelString( label13, "Text :" );
	UxPutBackground( label13, LabelBackground );
	UxPutHeight( label13, 30 );
	UxPutWidth( label13, 52 );
	UxPutY( label13, 8 );
	UxPutX( label13, 6 );

	UxPutMarginHeight( AngleText, 3 );
	UxPutHighlightColor( AngleText, "Black" );
	UxPutForeground( AngleText, TextForeground );
	UxPutTranslations( AngleText, CursorTab );
	UxPutFontList( AngleText, TextFont );
	UxPutHighlightOnEnter( AngleText, "true" );
	UxPutMaxLength( AngleText, 40 );
	UxPutBackground( AngleText, TextBackground );
	UxPutHeight( AngleText, 32 );
	UxPutWidth( AngleText, 42 );
	UxPutY( AngleText, 50 );
	UxPutX( AngleText, 68 );

	UxPutMarginHeight( SizeText, 3 );
	UxPutHighlightColor( SizeText, "Black" );
	UxPutForeground( SizeText, TextForeground );
	UxPutTranslations( SizeText, CursorTab );
	UxPutFontList( SizeText, TextFont );
	UxPutHighlightOnEnter( SizeText, "true" );
	UxPutMaxLength( SizeText, 40 );
	UxPutBackground( SizeText, TextBackground );
	UxPutHeight( SizeText, 32 );
	UxPutWidth( SizeText, 42 );
	UxPutY( SizeText, 50 );
	UxPutX( SizeText, 170 );

	UxPutMarginWidth( FontText, 3 );
	UxPutMarginHeight( FontText, 3 );
	UxPutHighlightColor( FontText, "Black" );
	UxPutForeground( FontText, TextForeground );
	UxPutCursorPositionVisible( FontText, "false" );
	UxPutFontList( FontText, TextFont );
	UxPutHighlightOnEnter( FontText, "true" );
	UxPutColumns( FontText, 12 );
	UxPutMaxLength( FontText, 20 );
	UxPutEditable( FontText, "false" );
	UxPutBackground( FontText, TextBackground );
	UxPutHeight( FontText, 32 );
	UxPutWidth( FontText, 94 );
	UxPutY( FontText, 50 );
	UxPutX( FontText, 254 );

	UxPutHighlightColor( label14, "Black" );
	UxPutForeground( label14, TextForeground );
	UxPutFontList( label14, TextFont );
	UxPutLabelString( label14, "Angle :" );
	UxPutBackground( label14, LabelBackground );
	UxPutHeight( label14, 30 );
	UxPutWidth( label14, 58 );
	UxPutY( label14, 50 );
	UxPutX( label14, 10 );

	UxPutHighlightColor( label15, "Black" );
	UxPutForeground( label15, TextForeground );
	UxPutFontList( label15, TextFont );
	UxPutLabelString( label15, "Size :" );
	UxPutBackground( label15, LabelBackground );
	UxPutHeight( label15, 30 );
	UxPutWidth( label15, 58 );
	UxPutY( label15, 50 );
	UxPutX( label15, 110 );

	UxPutHighlightColor( label16, "Black" );
	UxPutForeground( label16, TextForeground );
	UxPutFontList( label16, TextFont );
	UxPutLabelString( label16, "Font :" );
	UxPutBackground( label16, LabelBackground );
	UxPutHeight( label16, 30 );
	UxPutWidth( label16, 40 );
	UxPutY( label16, 50 );
	UxPutX( label16, 214 );

	UxPutBackground( arrowButton13, TextBackground );
	UxPutHighlightOnEnter( arrowButton13, "true" );
	UxPutForeground( arrowButton13, MenuBackground );
	UxPutHeight( arrowButton13, 20 );
	UxPutWidth( arrowButton13, 30 );
	UxPutY( arrowButton13, 48 );
	UxPutX( arrowButton13, 346 );

	UxPutBackground( arrowButton14, TextBackground );
	UxPutHighlightOnEnter( arrowButton14, "true" );
	UxPutForeground( arrowButton14, MenuBackground );
	UxPutArrowDirection( arrowButton14, "arrow_down" );
	UxPutHeight( arrowButton14, 20 );
	UxPutWidth( arrowButton14, 30 );
	UxPutY( arrowButton14, 66 );
	UxPutX( arrowButton14, 346 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_LabelCursor()
{
	/* Create the swidgets */

	LabelCursor = UxCreateTopLevelShell( "LabelCursor", NO_PARENT );
	UxPutContext( LabelCursor, UxLabelCursorContext );

	form10 = UxCreateForm( "form10", LabelCursor );
	form11 = UxCreateForm( "form11", form10 );
	OkLabelCursor = UxCreatePushButton( "OkLabelCursor", form11 );
	CancelLabel = UxCreatePushButton( "CancelLabel", form11 );
	DefaultLabel = UxCreatePushButton( "DefaultLabel", form11 );
	separator2 = UxCreateSeparator( "separator2", form10 );
	LabelText = UxCreateText( "LabelText", form10 );
	label13 = UxCreateLabel( "label13", form10 );
	AngleText = UxCreateText( "AngleText", form10 );
	SizeText = UxCreateText( "SizeText", form10 );
	FontText = UxCreateText( "FontText", form10 );
	label14 = UxCreateLabel( "label14", form10 );
	label15 = UxCreateLabel( "label15", form10 );
	label16 = UxCreateLabel( "label16", form10 );
	arrowButton13 = UxCreateArrowButton( "arrowButton13", form10 );
	arrowButton14 = UxCreateArrowButton( "arrowButton14", form10 );

	_Uxinit_LabelCursor();

	/* Create the X widgets */

	UxCreateWidget( LabelCursor );
	UxCreateWidget( form10 );
	UxCreateWidget( form11 );
	UxCreateWidget( OkLabelCursor );
	UxCreateWidget( CancelLabel );
	UxCreateWidget( DefaultLabel );
	UxCreateWidget( separator2 );
	UxCreateWidget( LabelText );
	UxCreateWidget( label13 );
	UxCreateWidget( AngleText );
	UxCreateWidget( SizeText );
	UxCreateWidget( FontText );
	UxCreateWidget( label14 );
	UxCreateWidget( label15 );
	UxCreateWidget( label16 );
	UxCreateWidget( arrowButton13 );
	UxCreateWidget( arrowButton14 );

	UxAddCallback( OkLabelCursor, XmNactivateCallback,
			activateCB_OkLabelCursor,
			(XtPointer) UxLabelCursorContext );

	UxAddCallback( CancelLabel, XmNactivateCallback,
			activateCB_CancelLabel,
			(XtPointer) UxLabelCursorContext );

	UxAddCallback( DefaultLabel, XmNactivateCallback,
			activateCB_DefaultLabel,
			(XtPointer) UxLabelCursorContext );

	UxAddCallback( AngleText, XmNmodifyVerifyCallback,
			modifyVerifyCB_AngleText,
			(XtPointer) UxLabelCursorContext );
	UxAddCallback( AngleText, XmNlosingFocusCallback,
			losingFocusCB_AngleText,
			(XtPointer) UxLabelCursorContext );

	UxAddCallback( SizeText, XmNmodifyVerifyCallback,
			modifyVerifyCB_SizeText,
			(XtPointer) UxLabelCursorContext );
	UxAddCallback( SizeText, XmNlosingFocusCallback,
			losingFocusCB_SizeText,
			(XtPointer) UxLabelCursorContext );

	UxAddCallback( arrowButton13, XmNactivateCallback,
			activateCB_arrowButton13,
			(XtPointer) UxLabelCursorContext );

	UxAddCallback( arrowButton14, XmNactivateCallback,
			activateCB_arrowButton14,
			(XtPointer) UxLabelCursorContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( LabelCursor );

	return ( LabelCursor );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_LabelCursor()
{
	swidget                 rtrn;
	_UxCLabelCursor         *UxContext;

	UxLabelCursorContext = UxContext =
		(_UxCLabelCursor *) UxMalloc( sizeof(_UxCLabelCursor) );

	rtrn = _Uxbuild_LabelCursor();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_LabelCursor()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_LabelCursor();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

