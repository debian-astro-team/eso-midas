/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090825		last modif

===========================================================================*/


/*******************************************************************************
	SaveAsShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>
#include <alice_help.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxSaveAsShell;
	swidget	UxSaveAsTopForm;
	swidget	UxTextLabel;
	swidget	UxSaveAsText;
	swidget	UxSaveAsForm2;
	swidget	UxSaveAsSeparator;
} _UxCSaveAsShell;

#define SaveAsShell             UxSaveAsShellContext->UxSaveAsShell
#define SaveAsTopForm           UxSaveAsShellContext->UxSaveAsTopForm
#define TextLabel               UxSaveAsShellContext->UxTextLabel
#define SaveAsText              UxSaveAsShellContext->UxSaveAsText
#define SaveAsForm2             UxSaveAsShellContext->UxSaveAsForm2
#define SaveAsSeparator         UxSaveAsShellContext->UxSaveAsSeparator

static _UxCSaveAsShell	*UxSaveAsShellContext;

extern void save_file();



swidget	OkSaveAs;
swidget	CancelSaveAs;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*SaveTextTab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_SaveAsShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	createCB_SaveAsText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSaveAsShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSaveAsShellContext;
	UxSaveAsShellContext = UxContext =
			(_UxCSaveAsShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("SaveAsText"),"noname.bdf" );
	}
	UxSaveAsShellContext = UxSaveCtx;
}

static void	activateCB_OkSaveAs( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSaveAsShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSaveAsShellContext;
	UxSaveAsShellContext = UxContext =
			(_UxCSaveAsShell *) UxGetContext( UxThisWidget );
	{
	strcpy(specSaveName,UxGetText(UxFindSwidget("SaveAsText")));
	save_file(specSaveName);
	UxPopdownInterface(UxFindSwidget("SaveAsShell"));
	
	}
	UxSaveAsShellContext = UxSaveCtx;
}

static void	activateCB_CancelSaveAs( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSaveAsShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSaveAsShellContext;
	UxSaveAsShellContext = UxContext =
			(_UxCSaveAsShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("SaveAsShell"));
	}
	UxSaveAsShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_SaveAsShell()
{
	UxPutKeyboardFocusPolicy( SaveAsShell, "pointer" );
	UxPutIconName( SaveAsShell, "Save As" );
	UxPutHeight( SaveAsShell, 133 );
	UxPutWidth( SaveAsShell, 296 );
	UxPutY( SaveAsShell, 426 );
	UxPutX( SaveAsShell, 456 );

	UxPutBackground( SaveAsTopForm, WindowBackground );
	UxPutHeight( SaveAsTopForm, 110 );
	UxPutWidth( SaveAsTopForm, 142 );
	UxPutY( SaveAsTopForm, 62 );
	UxPutX( SaveAsTopForm, 76 );
	UxPutUnitType( SaveAsTopForm, "pixels" );
	UxPutResizePolicy( SaveAsTopForm, "resize_none" );

	UxPutHighlightColor( TextLabel, "Black" );
	UxPutForeground( TextLabel, TextForeground );
	UxPutLabelString( TextLabel, "Output file:" );
	UxPutFontList( TextLabel, TextFont );
	UxPutBackground( TextLabel, LabelBackground );
	UxPutHeight( TextLabel, 34 );
	UxPutWidth( TextLabel, 110 );
	UxPutY( TextLabel, 18 );
	UxPutX( TextLabel, 10 );

	UxPutHighlightColor( SaveAsText, "Black" );
	UxPutForeground( SaveAsText, TextForeground );
	UxPutTranslations( SaveAsText, SaveTextTab );
	UxPutFontList( SaveAsText, TextFont );
	UxPutBackground( SaveAsText, TextBackground );
	UxPutHeight( SaveAsText, 38 );
	UxPutWidth( SaveAsText, 152 );
	UxPutY( SaveAsText, 16 );
	UxPutX( SaveAsText, 126 );

	UxPutBackground( SaveAsForm2, ButtonBackground );
	UxPutHeight( SaveAsForm2, 52 );
	UxPutWidth( SaveAsForm2, 296 );
	UxPutY( SaveAsForm2, 80 );
	UxPutX( SaveAsForm2, 0 );
	UxPutResizePolicy( SaveAsForm2, "resize_none" );

	UxPutHighlightColor( OkSaveAs, "#000000" );
	UxPutHighlightOnEnter( OkSaveAs, "true" );
	UxPutLabelString( OkSaveAs, "Ok" );
	UxPutForeground( OkSaveAs, ButtonForeground );
	UxPutFontList( OkSaveAs, BoldTextFont );
	UxPutBackground( OkSaveAs, ButtonBackground );
	UxPutHeight( OkSaveAs, 30 );
	UxPutWidth( OkSaveAs, 90 );
	UxPutY( OkSaveAs, 10 );
	UxPutX( OkSaveAs, 20 );

	UxPutHighlightColor( CancelSaveAs, "#000000" );
	UxPutHighlightOnEnter( CancelSaveAs, "true" );
	UxPutLabelString( CancelSaveAs, "Cancel" );
	UxPutForeground( CancelSaveAs, CancelForeground );
	UxPutFontList( CancelSaveAs, BoldTextFont );
	UxPutBackground( CancelSaveAs, ButtonBackground );
	UxPutHeight( CancelSaveAs, 30 );
	UxPutWidth( CancelSaveAs, 90 );
	UxPutY( CancelSaveAs, 10 );
	UxPutX( CancelSaveAs, 126 );

	UxPutBackground( SaveAsSeparator, ButtonBackground );
	UxPutHeight( SaveAsSeparator, 10 );
	UxPutWidth( SaveAsSeparator, 300 );
	UxPutY( SaveAsSeparator, 72 );
	UxPutX( SaveAsSeparator, 0 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_SaveAsShell()
{
	/* Create the swidgets */

	SaveAsShell = UxCreateTopLevelShell( "SaveAsShell", NO_PARENT );
	UxPutContext( SaveAsShell, UxSaveAsShellContext );

	SaveAsTopForm = UxCreateForm( "SaveAsTopForm", SaveAsShell );
	TextLabel = UxCreateLabel( "TextLabel", SaveAsTopForm );
	SaveAsText = UxCreateText( "SaveAsText", SaveAsTopForm );
	SaveAsForm2 = UxCreateForm( "SaveAsForm2", SaveAsTopForm );
	OkSaveAs = UxCreatePushButton( "OkSaveAs", SaveAsForm2 );
	CancelSaveAs = UxCreatePushButton( "CancelSaveAs", SaveAsForm2 );
	SaveAsSeparator = UxCreateSeparator( "SaveAsSeparator", SaveAsTopForm );

	_Uxinit_SaveAsShell();

	/* Create the X widgets */

	UxCreateWidget( SaveAsShell );
	UxCreateWidget( SaveAsTopForm );
	UxCreateWidget( TextLabel );
	UxCreateWidget( SaveAsText );
	createCB_SaveAsText( UxGetWidget( SaveAsText ),
			(XtPointer) UxSaveAsShellContext, (XtPointer) NULL );

	UxCreateWidget( SaveAsForm2 );
	UxCreateWidget( OkSaveAs );
	UxCreateWidget( CancelSaveAs );
	UxCreateWidget( SaveAsSeparator );

	UxAddCallback( OkSaveAs, XmNactivateCallback,
			activateCB_OkSaveAs,
			(XtPointer) UxSaveAsShellContext );

	UxAddCallback( CancelSaveAs, XmNactivateCallback,
			activateCB_CancelSaveAs,
			(XtPointer) UxSaveAsShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( SaveAsShell );

	return ( SaveAsShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_SaveAsShell()
{
	swidget                 rtrn;
	_UxCSaveAsShell         *UxContext;

	UxSaveAsShellContext = UxContext =
		(_UxCSaveAsShell *) UxMalloc( sizeof(_UxCSaveAsShell) );

	rtrn = _Uxbuild_SaveAsShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_SaveAsShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_SaveAsShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

