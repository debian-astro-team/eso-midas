/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
.VERSION
 090825		last modif

===========================================================================*/


/*******************************************************************************
	sh_list1.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	int	mumble;
} _UxCsh_list1;


static _UxCsh_list1	*UxSh_list1Context;

extern void read_image(), read_image_over(), read_prg();




swidget	sh_list1;
swidget	form4;
swidget	pushButton2;
swidget	scrolledWindow1;
swidget	scrolledList1;

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_sh_list1();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCsh_list1            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSh_list1Context;
	UxSh_list1Context = UxContext =
			(_UxCsh_list1 *) UxGetContext( UxThisWidget );
	{
	extern swidget ListPopup;
	
	UxPopdownInterface(ListPopup);
	}
	UxSh_list1Context = UxSaveCtx;
}

static void	createCB_scrolledList1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCsh_list1            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSh_list1Context;
	UxSh_list1Context = UxContext =
			(_UxCsh_list1 *) UxGetContext( UxThisWidget );
	{
	
	/*
	FileListWidget = UxWidget;
	*/
	}
	UxSh_list1Context = UxSaveCtx;
}

static void	browseSelectionCB_scrolledList1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCsh_list1            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSh_list1Context;
	UxSh_list1Context = UxContext =
			(_UxCsh_list1 *) UxGetContext( UxThisWidget );
	{
	char *choice,prgname[40];
	XmListCallbackStruct *cbs;
	extern swidget ListPopup;
	/*UxPopdownInterface(ListPopup); */
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	
	XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);
	switch(caseList)
	{
	  case CASEBDF:
	   strcpy(specImageName,choice);
	   read_image(specImageName);
	   UxPopdownInterface(ListPopup);
	   break;
	  case CASEPRG: 
	   strcpy(prgname,choice);
	   read_prg(prgname);
	   UxPopdownInterface(ListPopup);
	   break;
	  case OVERBDF:
	   read_image_over(choice);
	   break;
	}
	XtFree(choice);   
	
	 
	}
	UxSh_list1Context = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_sh_list1()
{
	UxPutDefaultFontList( sh_list1, TextFont );
	UxPutBorderColor( sh_list1, WindowBackground );
	UxPutBackground( sh_list1, SHelpBackground );
	UxPutKeyboardFocusPolicy( sh_list1, "pointer" );
	UxPutIconName( sh_list1, "List selection" );
	UxPutHeight( sh_list1, 350 );
	UxPutWidth( sh_list1, 210 );
	UxPutY( sh_list1, 426 );
	UxPutX( sh_list1, 468 );

	UxPutForeground( form4, TextForeground );
	UxPutBackground( form4, WindowBackground );
	UxPutHeight( form4, 240 );
	UxPutWidth( form4, 305 );
	UxPutY( form4, 0 );
	UxPutX( form4, 0 );
	UxPutUnitType( form4, "pixels" );
	UxPutResizePolicy( form4, "resize_none" );

	UxPutHighlightOnEnter( pushButton2, "true" );
	UxPutForeground( pushButton2, CancelForeground );
	UxPutLabelString( pushButton2, "Cancel" );
	UxPutFontList( pushButton2, BoldTextFont );
	UxPutBackground( pushButton2, ButtonBackground );
	UxPutHeight( pushButton2, 35 );
	UxPutWidth( pushButton2, 210 );
	UxPutY( pushButton2, 314 );
	UxPutX( pushButton2, 0 );

	UxPutTopShadowColor( scrolledWindow1, "Black" );
	UxPutBottomShadowColor( scrolledWindow1, WindowBackground );
	UxPutForeground( scrolledWindow1, TextForeground );
	UxPutHeight( scrolledWindow1, 314 );
	UxPutBackground( scrolledWindow1, WindowBackground );
	UxPutScrollBarPlacement( scrolledWindow1, "bottom_left" );
	UxPutShadowThickness( scrolledWindow1, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow1, "static" );
	UxPutVisualPolicy( scrolledWindow1, "variable" );
	UxPutY( scrolledWindow1, 0 );
	UxPutX( scrolledWindow1, 0 );
	UxPutScrollingPolicy( scrolledWindow1, "application_defined" );

	UxPutHighlightColor( scrolledList1, "Black" );
	UxPutForeground( scrolledList1, TextForeground );
	UxPutVisibleItemCount( scrolledList1, 15 );
	UxPutFontList( scrolledList1, TextFont );
	UxPutBackground( scrolledList1, SHelpBackground );
	UxPutListSizePolicy( scrolledList1, "constant" );
	UxPutScrollBarDisplayPolicy( scrolledList1, "static" );
	UxPutHeight( scrolledList1, 184 );
	UxPutWidth( scrolledList1, 191 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_sh_list1()
{
	/* Create the swidgets */

	sh_list1 = UxCreateTopLevelShell( "sh_list1", NO_PARENT );
	UxPutContext( sh_list1, UxSh_list1Context );

	form4 = UxCreateForm( "form4", sh_list1 );
	pushButton2 = UxCreatePushButton( "pushButton2", form4 );
	scrolledWindow1 = UxCreateScrolledWindow( "scrolledWindow1", form4 );
	scrolledList1 = UxCreateScrolledList( "scrolledList1", scrolledWindow1 );

	_Uxinit_sh_list1();

	/* Create the X widgets */

	UxCreateWidget( sh_list1 );
	UxCreateWidget( form4 );
	UxCreateWidget( pushButton2 );
	UxCreateWidget( scrolledWindow1 );
	UxCreateWidget( scrolledList1 );
	createCB_scrolledList1( UxGetWidget( scrolledList1 ),
			(XtPointer) UxSh_list1Context, (XtPointer) NULL );


	UxAddCallback( pushButton2, XmNactivateCallback,
			activateCB_pushButton2,
			(XtPointer) UxSh_list1Context );

	UxAddCallback( scrolledList1, XmNbrowseSelectionCallback,
			browseSelectionCB_scrolledList1,
			(XtPointer) UxSh_list1Context );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( sh_list1 );

	return ( sh_list1 );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_sh_list1()
{
	swidget                 rtrn;
	_UxCsh_list1            *UxContext;

	UxSh_list1Context = UxContext =
		(_UxCsh_list1 *) UxMalloc( sizeof(_UxCsh_list1) );

	rtrn = _Uxbuild_sh_list1();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_sh_list1()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_sh_list1();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

