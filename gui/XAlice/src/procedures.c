/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* .VERSION
    090825	last modif
*/


#include <stdio.h>
#include <alice_global.h>


extern void putFontText(), calc_fit(), save_cont();
extern void XmToggleButtonSetState();
extern void  draw_number_component(), draw_sgauss();

extern int draw_number_comp(), spec(), load_image(), auto_fit();
extern int XmToggleButtonGetState();


/*
  Include structures for the clock bitmap. It should be in an include
  file (next version of the standards).
*/

static unsigned char clock1_bits[] = {
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0xf8, 0xff, 0x7f, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0xfd, 0x32, 0xfe,
   0x30, 0xfd, 0x32, 0xfe, 0x30, 0xfd, 0x32, 0xfe, 0x30, 0xfa, 0x32, 0xfe,
   0x30, 0x7a, 0x31, 0xfe, 0x30, 0xb4, 0x30, 0xfe, 0x30, 0x48, 0x30, 0xfe,
   0x30, 0x48, 0x30, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x02, 0x31, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0xf8, 0xff, 0x7f, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe};
static unsigned char clock2_bits[] = {
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0xf8, 0xff, 0x7f, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0xfd, 0x32, 0xfe, 0x30, 0xfd, 0x32, 0xfe, 0x30, 0xfa, 0x32, 0xfe,
   0x30, 0x7a, 0x31, 0xfe, 0x30, 0xb4, 0x30, 0xfe, 0x30, 0x78, 0x30, 0xfe,
   0x30, 0x78, 0x30, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x02, 0x31, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x31, 0x32, 0xfe, 0x30, 0x79, 0x32, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0xf8, 0xff, 0x7f, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe};
static unsigned char clock3_bits[] = {
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0xf8, 0xff, 0x7f, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0x85, 0x32, 0xfe, 0x30, 0xfd, 0x32, 0xfe, 0x30, 0xfa, 0x32, 0xfe,
   0x30, 0x7a, 0x31, 0xfe, 0x30, 0xb4, 0x30, 0xfe, 0x30, 0x78, 0x30, 0xfe,
   0x30, 0x78, 0x30, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x02, 0x31, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0x31, 0x32, 0xfe, 0x30, 0x79, 0x32, 0xfe, 0x30, 0xfd, 0x32, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0xf8, 0xff, 0x7f, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe};
static unsigned char clock4_bits[] = {
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0xf8, 0xff, 0x7f, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0xfe, 0x32, 0xfe,
   0x30, 0x7a, 0x31, 0xfe, 0x30, 0xb4, 0x30, 0xfe, 0x30, 0x78, 0x30, 0xfe,
   0x30, 0x78, 0x30, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x02, 0x31, 0xfe,
   0x30, 0x31, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x31, 0x32, 0xfe,
   0x30, 0x79, 0x32, 0xfe, 0x30, 0xfd, 0x32, 0xfe, 0x30, 0xfd, 0x32, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0xf8, 0xff, 0x7f, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe};
static unsigned char clock5_bits[] = {
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0xf8, 0xff, 0x7f, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x32, 0x32, 0xfe,
   0x30, 0x7a, 0x31, 0xfe, 0x30, 0xb4, 0x30, 0xfe, 0x30, 0x78, 0x30, 0xfe,
   0x30, 0x78, 0x30, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x02, 0x31, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x31, 0x32, 0xfe,
   0x30, 0xfd, 0x32, 0xfe, 0x30, 0xff, 0x33, 0xfe, 0x30, 0xff, 0x33, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0xf8, 0xff, 0x7f, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe};
static unsigned char clock6_bits[] = {
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0xf8, 0xff, 0x7f, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x02, 0x32, 0xfe,
   0x30, 0x32, 0x31, 0xfe, 0x30, 0xb4, 0x30, 0xfe, 0x30, 0x78, 0x30, 0xfe,
   0x30, 0x78, 0x30, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x32, 0x31, 0xfe,
   0x30, 0x31, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x31, 0x32, 0xfe,
   0x30, 0xfd, 0x32, 0xfe, 0x30, 0xff, 0x33, 0xfe, 0x30, 0xff, 0x33, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0xf8, 0xff, 0x7f, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe};
static unsigned char clock7_bits[] = {
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0xf8, 0xff, 0x7f, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x02, 0x32, 0xfe,
   0x30, 0x02, 0x31, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x78, 0x30, 0xfe,
   0x30, 0x78, 0x30, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x02, 0x31, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x31, 0x32, 0xfe, 0x30, 0x79, 0x32, 0xfe,
   0x30, 0xfd, 0x32, 0xfe, 0x30, 0xff, 0x33, 0xfe, 0x30, 0xff, 0x33, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0xf8, 0xff, 0x7f, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe};
static unsigned char clock8_bits[] = {
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe, 0xf8, 0xff, 0x7f, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x01, 0x32, 0xfe, 0x30, 0x02, 0x32, 0xfe,
   0x30, 0x02, 0x31, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x48, 0x30, 0xfe,
   0x30, 0x48, 0x30, 0xfe, 0x30, 0x84, 0x30, 0xfe, 0x30, 0x02, 0x31, 0xfe,
   0x30, 0x01, 0x32, 0xfe, 0x30, 0x79, 0x32, 0xfe, 0x30, 0xfd, 0x32, 0xfe,
   0x30, 0xfd, 0x32, 0xfe, 0x30, 0xff, 0x33, 0xfe, 0x30, 0xff, 0x33, 0xfe,
   0xf8, 0xff, 0x7f, 0xfe, 0xf8, 0xff, 0x7f, 0xfe, 0x00, 0x00, 0x00, 0xfe,
   0x00, 0x00, 0x00, 0xfe, 0x00, 0x00, 0x00, 0xfe};


void Yscaling_auto(), Yscaling_active(), Yscaling_manual();
void vloc();


void init_AMatrix()
{
 int i,j;
 for(i=0;i<27;i++)
  for(j=0;j<28;j++)
   gaussAMatrix[i][j] = i==j?1.0:0.0;
}


void init_values()
{
char out_val[80];
sprintf(out_val,"%d",specLineNum);
UxPutText(UxFindSwidget("CurrLine"),out_val);
sprintf(out_val,"%d",specLineStep);
UxPutText(UxFindSwidget("Linestep"),out_val);
sprintf(out_val,"%d",fitDegree);
UxPutText(UxFindSwidget("degree_text"),out_val);
sprintf(out_val,"%d",gaussNumOfComp);
UxPutText(UxFindSwidget("Components"),out_val);
sprintf(out_val,"%d",gaussMaxIterations);
UxPutText(UxFindSwidget("iterations_text"),out_val);
sprintf(out_val,"%d",gaussMinIterations);
UxPutText(UxFindSwidget("iterations_text1"),out_val);
sprintf(out_val,"%d",filterWindWidth);
UxPutText(UxFindSwidget("width_text"),out_val);
sprintf(out_val,"%g",plotAngle);
UxPutText(UxFindSwidget("AngleText"),out_val);
sprintf(out_val,"%d",0);
UxPutText(UxFindSwidget("dyText"),out_val);
UxPutText(UxFindSwidget("nText"),out_val);
sprintf(out_val,"%g",plotSize);
UxPutText(UxFindSwidget("SizeText"),out_val);
XtSetSensitive(UxGetWidget(UxFindSwidget("UndoItem")),0);
putFontText(plotFont);

specModLineStep = FALSE;
specModLineNum = FALSE;
fitModFitDeg = FALSE;
gaussModNumOfComp = FALSE;
filterModWindWidth = FALSE;
plotModSize = FALSE;
plotModAngle = FALSE;
OverPlotMode = FALSE;
ApplyOverPlot = FALSE;

specCurrCursor = 0;

init_AMatrix();
system("rm -f alicel.plt alice.plt TMPalice.tbl pscrplot.0 TMPalice.prg TMPalice.stat");
system("touch TMPalice.tbl TMPalice.prg TMPalice.stat");
}


/* getFitValue(fitvalues,i,na) : set the fitvalues array using the
                       gaussAMatrix matrix.
                     i  : fitvalues[i] will be setted.
                     na : number of fit value.                    */
double getFitValue(fitvalues,i,na)
double fitvalues[];
int i,na;
{
 int j;
 double ret = 0.0;
 i--;
 if(gaussAMatrix[i][i] == 1.0)
  return( fitvalues[i+1]);
 else
 for(j=0;j<na;j++)
  ret += gaussAMatrix[i][j]*fitvalues[j+1] ;
 ret +=  gaussAMatrix[i][27];
 return(ret);
}


/* getStringValues(s,c,f,k) : set the initial guess values.
                    s  : string from "Initial Guess" field.
                    *c : component index.
                    *f : component factor.
                    *k : constant to be added.                  */ 
int getStringValues(s,c,f,k)
char * s;
int *c;
float *f,*k;
{
 char ch[2];
 *c = 0; *f= 0.0; *k = 0.0;
 if(sscanf(s,"%1[AXS]%d*%f+%f",ch,c,f,k)== 4)
  ;
 else if(sscanf(s,"%1[AXS]%d*%f-%f",ch,c,f,k)== 4)
   *k = - *k;
 else if(sscanf(s,"%1[AXS]%d/%f+%f",ch,c,f,k)== 4)
  *f = 1 / *f;
 else if(sscanf(s,"%1[AXS]%d/%f-%f",ch,c,f,k)== 4)
  {
  *f = 1 / *f;
  *k = - *k;
  }
 else if(sscanf(s,"%1[AXS]%d*%f",ch,c,f)== 3)
  *k = 0.0;
 else if(sscanf(s,"%1[AXS]%d/%f",ch,c,f)== 3)
  {
  *f = 1 / *f;
  *k = 0.0;
  }
 else if(sscanf(s,"%1[AXS]%d+%f",ch,c,k)== 3)
  *f = 1.0;
 else if(sscanf(s,"%1[AXS]%d-%f",ch,c,k)== 3)
  {
  *f = 1.0;
  *k = - *k;
  }
 else if(sscanf(s,"%1[AXS]%d",ch,c)== 2)
  {
  *f = 1.0;
  *k = 0.0;
  }
 else if(sscanf(s,"%f",k)== 1)
  {
  *f = 0.0;
  *c = -1;
  }
  else 
    return(0);
 switch(ch[0])
 {
  case 'A':
	 *c = *c==0?0.0:3*(*c-1);
          break;
  case 'X':
	 *c = *c==0?0.0:3*(*c-1)+1;
          break;
  case 'S':
	 *c = *c==0?0.0:3*(*c-1)+2;
          break;
 }
	 return(1);
}

/* read_init_guess() : Read the initial guess field.         
                    Set the gaussFitValues array and gaussAMatrix matrix
                                                                       */
 
void read_init_guess()
{
 char v1[80],v2[80],v3[80];
 float factor,constant;
 int index = 0;
 extern double gaussFitValues[];
 int i,j,ndata;
 char name[30];
 init_AMatrix();
 for(i=0,j=0;(i<9) & (j < gaussNumOfComp);i++)
 {
  sprintf(name,"toggleButton%d",i+1);
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
  {
   sprintf(name,"textField%d",i+11);
   ndata=sscanf(UxGetText(UxFindSwidget(name)),"%s%s%s",v1,v2,v3);
   if(ndata==3)
   {
    getStringValues(v1,&index,&factor,&constant);
    if(index < 0 )
     gaussFitValues[j*3]=constant;
    else
     {
      gaussFitValues[j*3]=0.0;
      gaussAMatrix[j*3][index] = factor;  
      gaussAMatrix[j*3][27] = constant;  
      gaussAMatrix[j*3][j*3]= 0.0;
      sprintf(name,"toggleButton%d",i*3+10);
      XmToggleButtonSetState(UxGetWidget(UxFindSwidget(name)),TRUE,TRUE);
     }
    getStringValues(v2,&index,&factor,&constant);
    if(index < 0)
     gaussFitValues[j*3+1]=constant;
    else
     {
      gaussFitValues[j*3+1]=0.0;
      gaussAMatrix[j*3+1][index] = factor;  
      gaussAMatrix[j*3+1][27] = constant;  
      gaussAMatrix[j*3+1][j*3+1]= 0.0;
      sprintf(name,"toggleButton%d",i*3+11);
      XmToggleButtonSetState(UxGetWidget(UxFindSwidget(name)),TRUE,TRUE);
     }
    getStringValues(v3,&index,&factor,&constant);
    if(index < 0)
     gaussFitValues[j*3+2]=constant;
    else
     {
      gaussFitValues[j*3+2]=0.0;
      gaussAMatrix[j*3+2][index] = factor;  
      gaussAMatrix[j*3+2][27] = constant;  
      gaussAMatrix[j*3+2][j*3+2]= 0.0;
      sprintf(name,"toggleButton%d",i*3+12);
      XmToggleButtonSetState(UxGetWidget(UxFindSwidget(name)),TRUE,TRUE);
     }
   }
   j++;
  }
 }
}

/* copy_fit_values() : copy the selected "Fit Values" fields to 
                       "Initial Guess" fields                    */
void copy_fit_values()
{
 int i,j;
 char name[30],auxname[30];
 for(i=0,j=0;(i<9) & (j < gaussNumOfComp);i++)
 {
  sprintf(name,"toggleButton%d",i+1);
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
  {
   sprintf(name,"textField%d",i+20);
   sprintf(auxname,"textField%d",i+11);
   XmTextSetString(UxGetWidget(UxFindSwidget(auxname)),
                   UxGetText(UxFindSwidget(name)));
   j++;
  }
 }
}


void Read_plotmode()
{
 if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("StraightToggle"))))
  plotMode = LINEMODE;
 else
  plotMode = HISTMODE;
}

void Set_plotmode()
{
 if( plotMode == LINEMODE)
 {
 XmToggleButtonSetState(UxGetWidget(UxFindSwidget("StraightToggle")),
  TRUE,TRUE);
 XmToggleButtonSetState(UxGetWidget(UxFindSwidget("HistoToggle")),
  FALSE,FALSE);
 }
 else
  {
 XmToggleButtonSetState(UxGetWidget(UxFindSwidget("StraightToggle")),
  FALSE,FALSE);
 XmToggleButtonSetState(UxGetWidget(UxFindSwidget("HistoToggle")),
  TRUE,TRUE);
 }
}

/* read_fit_values() : read the "Fit Values" fields and set the
                       gaussFitValues array                        */
void read_fit_values()
{
 float v1,v2,v3;
 extern double gaussFitValues[];
 int i,j,ndata;
 char name[30];
 for(i=0,j=0;(i<9) & (j <gaussNumOfSol);i++)
 {
  sprintf(name,"toggleButton%d",i+1);
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
  {
  sprintf(name,"textField%d",i+20);
  ndata=sscanf(UxGetText(UxFindSwidget(name)),"%f %f %f",&v1,&v2,&v3);
    gaussFitValues[j*3]=v1;
    gaussFitValues[j*3+1]=v2;
    gaussFitValues[j*3+2]=v3;
  j++;
  }
 }  
}

/* draw_init_guess() : plot the "Initial Guess" fields on a Midas
                       graphic window                              */
void draw_init_guess()
{
 float v1,v2,v3;
 extern double gaussFitValues[];
 int i,j,ndata;
 char name[30];
 for(i=0,j=0;(i<9) & (j<gaussNumOfComp);i++)
 {
  sprintf(name,"toggleButton%d",i+1);
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
  {
   sprintf(name,"textField%d",i+11);
   ndata=sscanf(UxGetText(UxFindSwidget(name)),"%f %f %f",&v1,&v2,&v3);
   if(ndata==3)
   draw_sgauss(gaussFitValues[j*3],gaussFitValues[j*3+1],
	       gaussFitValues[j*3+2],3);
   j++;
  }
  }
}

void read_image_over(name)
char *name;
{
 char item[80],text[1024];
 sprintf(item,"%-20s %4d%4d",name,1,1);
 strcpy(text,UxGetText(UxFindSwidget("OverPlotText")));
 if(strcmp(text,"")!= 0)
  sprintf(text,"%s\n%s",text, item);
 else
  strcpy(text,item);
 UxPutText(UxFindSwidget("OverPlotText"),text);
 OverPlotNum++;
 sprintf(text,"%d",OverPlotNum);
 UxPutText(UxFindSwidget("nText"),text);
 sprintf(text,"SelOverTb%d",OverPlotNum);
 UxMap(UxFindSwidget(text));
}

void clear_over()
{
  int i;
  char name[40];
  for(i=1;i<=15;i++)
  {
   specNpix[i] = 0;
   sprintf(name,"SelOverTb%d",i);
   UxUnmap(UxFindSwidget(name));
  XmToggleButtonSetState(UxGetWidget(UxFindSwidget(name)),TRUE,TRUE);
  }
  UxPutText(UxFindSwidget("OverPlotText"),"");
  OverPlotNum = 0;
}

void plot_over()
{
 char text[1024],file_name[40],name[40];
 int line,step,i,pos,plot;
 float deltay,dy,fit_cont();

dy = 0.0;

 if(!OverPlotMode)
 {
 strcpy(text,UxGetText(UxFindSwidget("OverPlotText")));
 pos =1;
 i = 1;
 sprintf(name,"SelOverTb%d",i);
 if(sscanf(text,"%s%d%d",file_name,&line,&step)==3 &&
    XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
  {
   load_image(file_name,OverX[pos-1],OverY[pos-1],line,step,pos,FALSE);
     pos++;
   }
 while(strchr(text,'\n')!= NULL)
  {
   i++;
   sprintf(name,"SelOverTb%d",i);
   strcpy(text,strchr(text,'\n')+1);
   if(sscanf(text,"%s%d%d",file_name,&line,&step)==3  &&
     XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
    {
     if(pos < 15)
     {
      load_image(file_name,OverX[pos-1],OverY[pos-1],line,step,pos,FALSE);
     pos++;
     }
    }
  }

  overPos= pos-1;
  OverPlotNum = i;

  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Scalingtb1"))))
   Yscaling_active();
  else if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Scalingtb2"))))
    Yscaling_auto();
  else if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Scalingtb3"))))
   Yscaling_manual();

  for(pos=1;pos<=overPos;pos++)
  {
   if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("ContAutoFitTb"))))
   {
   auto_fit(pos);
   calc_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1);
   if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Inctb1"))))
    {
     sscanf(UxGetText(UxFindSwidget("dyText")),"%f",&deltay);
     dy = (pos-1)*deltay;
    }
   if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Inctb2"))))
    {
     sscanf(UxGetText(UxFindSwidget("nText")),"%f",&deltay);
     deltay = (overYmax-overYmin)/(deltay+1);
     dy = pos*deltay +overYmin;
    }

   if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Conttb1"))))
    for(i=0;i<specNpix[pos];i++)
     OverY[pos-1][i] = OverY[pos-1][i] - fit_cont(OverX[pos-1][i]) + dy;
   if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Conttb2"))))
    for(i=0;i<specNpix[pos];i++)
     OverY[pos-1][i] = OverY[pos-1][i] / 
                       gaussNumOfFitData==0?1:fit_cont(OverX[pos-1][i]) + dy;
   }
  }

 auto_fit(0);
 }
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Scalingtb1"))))
   Yscaling_active();
  else if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Scalingtb2"))))
    Yscaling_auto();
  else if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("Scalingtb3"))))
   Yscaling_manual();
 plot = FALSE;
  for(pos = 0;pos < overPos ;pos++)
   {
   if( XmToggleButtonGetState(UxGetWidget(UxFindSwidget("RedrawTB"))) &&
       !plot && ApplyOverPlot )
    {
    spec(OverX[pos],OverY[pos],specNpix[pos+1],specXcen-specDx,
         specXcen+specDx, overYmin,overYmax,PLOTMODE);
    plot = TRUE;
    }
   else
     spec(OverX[pos],OverY[pos],specNpix[pos+1], specXcen-specDx,
           specXcen+specDx, overYmin,overYmax, OVERMODE);
   }
}

/* draw_number_component(color) : put the number component over the
                   gaussian fitting line.
   color : AGL color number                                        */
void draw_number_component(color)
int color;
{
 int i,j;
 char name[20];
 for(i=0,j=0;i<9;i++)
 {
  sprintf(name,"toggleButton%d",i+1);
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
   {
   draw_number_comp(j,j,color);
   /*draw_number_comp(j,i,color); numera segun botones*/
   j++;
   }
 }
}

/*  out_fit_values() : put the gaussian fitting values on the
                       "Fit Values" fields.                    */
void out_fit_values()
{
 extern double gaussFitValues[];
 extern int gaussNumOfComp;
 char out[80],name[30];
 int i,j;
 for(i=0,j=0;(i <9) & (j<gaussNumOfComp);i++)
 {
  sprintf(name,"toggleButton%d",i+1);
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
  {
  sprintf(name,"textField%d",i+20);
  sprintf(out,"%9.8g %9.8g %9.8g",gaussFitValues[j*3],
   gaussFitValues[j*3+1],gaussFitValues[j*3+2]);
  XmTextSetString(UxGetWidget(UxFindSwidget(name)),out);
  j++;
  }
   else
    UxPutLabelString(UxFindSwidget(name),"    "); 
 }
 for(;i<9;i++)
 {
  sprintf(name,"toggleButton%d",i+1);
 if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
  {
   XmToggleButtonSetState(UxGetWidget(UxFindSwidget(name)),FALSE,FALSE);
   UxPutLabelString(UxFindSwidget(name),"    ");
  }
 }  
}


/* clear_values() : clear the "Initial Guess" fields.         */
void clear_values()
{
 extern double gaussFitValues[];
 extern int gaussNumOfSol;
 char name[30];
 int i;
 for(i=0;i<18;i++)
 {
  sprintf(name,"textField%d",i+11);
  XmTextSetString(UxGetWidget(UxFindSwidget(name)),"");
 }
 for(i=0;i<27;i++)
  gaussFitValues[i] = 0.0;
  gaussNumOfSol = 0;
init_AMatrix();
}  

/* get_fix_opt() : set the gaussFixOpt array                   */
void get_fix_opt()
{
extern int gaussFixOpt[];
char name[30];
int i,j,k;
 for(i=0,j=0;(i<9) & (j<gaussNumOfComp*3);i++)
 {
  sprintf(name,"toggleButton%d",i+1);
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
  {
   for(k=0;k<3;k++,j++)
   {
    sprintf(name,"toggleButton%d",i*3+k+10);
    gaussFixOpt[j]=XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name)));
   }
  }
 }
}


int is_auto_fit()
{
 return(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("AutoFitTButton"))));
}

void Yscaling_active()
{
 char out_val[40];
 overYmin = specYcen-specDy;
 overYmax = specYcen+specDy;
  sprintf(out_val,"%g",overYmin);
  UxPutText(UxFindSwidget("yminText"),out_val);
  UxPutEditable(UxFindSwidget("yminText"),"false");
  sprintf(out_val,"%g",overYmax);
  UxPutText(UxFindSwidget("ymaxText"),out_val);
  UxPutEditable(UxFindSwidget("ymaxText"),"false");
}

void Yscaling_auto()
{
 int i,pos;
 float min,max;
 char out_val[40];
 min = max = OverY[0][0];
 for(pos = 0;pos<overPos;pos++)
  for(i=0;i<specNpix[pos];i++)
   {
    if(min > OverY[pos][i])
      min = OverY[pos][i];
    if(max < OverY[pos][i])
      max = OverY[pos][i];
   }
  overYmin = min;
  overYmax = max;
  sprintf(out_val,"%g",overYmin);
  UxPutText(UxFindSwidget("yminText"),out_val);
  UxPutEditable(UxFindSwidget("yminText"),"false");
  sprintf(out_val,"%g",overYmax);
  UxPutText(UxFindSwidget("ymaxText"),out_val);
  UxPutEditable(UxFindSwidget("ymaxText"),"false");
}

void Yscaling_manual()
{
   float min,max;
   UxPutEditable(UxFindSwidget("yminText"),"true");
   UxPutEditable(UxFindSwidget("ymaxText"),"true");
   sscanf(UxGetText(UxFindSwidget("yminText")),"%f",&min);
   sscanf(UxGetText(UxFindSwidget("ymaxText")),"%f",&max);
   overYmin = min;
   overYmax = max;
}


/* cursor() : read the "Initial Guess" field and ask for not entered
              ones.                                                   */
void cursor()
{
 int i,j,key,ndata;
 char name[20],out[80],s1[80],s2[80],s3[80],num[4];
 float x1,x2,y1,aux,contin,v1,v2;
 float fit_cont();
 for(i=0,j=0;i<9;i++)
 {
  sprintf(name,"toggleButton%d",i+1);
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
   {
    j++;
    sprintf(num,"%d",j);
    UxPutLabelString(UxFindSwidget(name),num);
    XFlush(UxDisplay);
   }
 }
 for(i=0;(i<9) & (j<gaussNumOfComp);i++)
 {
  sprintf(name,"toggleButton%d",i+1);
  if(!XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
  {
   j++;
   XmToggleButtonSetState(UxGetWidget(UxFindSwidget(name)),TRUE,TRUE);
   sprintf(num,"%d",j);
   UxPutLabelString(UxFindSwidget(name),num);
   XFlush(UxDisplay);
  }
 }
 specAbortCursor = FALSE;
 for(i=0,j=0;(i<9) & (j < gaussNumOfComp) & (!specAbortCursor);i++)
 {
  sprintf(name,"textField%d",i+11);
  ndata=sscanf(UxGetText(UxFindSwidget(name)),"%s %s %s",s1,s2,s3);
  sprintf(name,"toggleButton%d",i+1);
  XFlush(UxDisplay);
  if(ndata == 3)
  {
   ndata = getStringValues(s1,&key,&v1,&v2)+getStringValues(s2,&key,&v1,&v2)+
           getStringValues(s3,&key,&v1,&v2);
  }
  if((XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))) & ndata) != 3)
  {
    sprintf(name,"textField%d",i+11);
    XmTextSetString(UxGetWidget(UxFindSwidget(name))," ");
    XFlush(UxDisplay);
    x1 = specXcen;
    aux = specYcen;
    vloc(&x1,&aux,&key);
    if(key == 1) 
     {
      x2 = x1;
      vloc(&x2,&aux,&key);
      if(key == 1) 
      {
        y1 = aux;
        aux = x2;
        vloc(&aux,&y1,&key);
        if(key == 1) 
        {
          gaussFitValues[j*3+1] = aux;
          if(x1> x2)
          {
           aux  = x2;
           x2   = x1;
           x1   = aux;
          }
          contin = fit_cont(gaussFitValues[j*3+1]);
          gaussFitValues[j*3]   = y1-contin;
          gaussFitValues[j*3+2] = (x2-x1)/2;
          sprintf(out,"%9.8g %9.8g %9.8g",gaussFitValues[j*3],
          gaussFitValues[j*3+1],gaussFitValues[j*3+2]);
          sprintf(name,"textField%d",i+11);
          XmTextSetString(UxGetWidget(UxFindSwidget(name)),out);
          draw_sgauss(gaussFitValues[j*3],gaussFitValues[j*3+1],
	  gaussFitValues[j*3+2],3);
        }
       else
         specAbortCursor = TRUE;
      }
      else 
       specAbortCursor = TRUE;
    }
    else
     specAbortCursor = TRUE;
  j++;
  }
  else 
  {
  sprintf(name,"toggleButton%d",i+1);
  if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget(name))))
   j++;
  }
 }
}
 
/* put_iterations(i) : set the "Current Iteration" field.
                   i : current iteration.                    */
void put_iterations(i)
int i;

{
 char out[20];
 sprintf(out,"%d",i);
 UxPutText(UxFindSwidget("iterations_text2"),out);
}


/* out_errors() : print the gaussian standar deviation          */
void out_errors()
{
 char out[1920];
 char line[80];
 strcpy(out,"\t    Gaussian Standard Deviation \n\n  Comp\t  Amplitude\t  Position\t\t  Width\n");
 for(i=0;i<gaussNumOfSol;i++)
 {
  sprintf(line,"     %d\t  %f\t\t  %f\t\t  %f\n",i+1,gaussErrors[i*3],
          gaussErrors[i*3+1],gaussErrors[i*3+2]);
  strcat(out,line);
 }
sprintf(line,"\n   RMS : %f",fitRms);
  strcat(out,line);
 UxPutText(UxFindSwidget("text1"),out);
}

/* out_error(error) : popup a message shell window.
             error  : message                              */
void out_error(error)
char * error;
{
 char line[256];
 sprintf(line,"%s",error);
 UxPutLabelString(UxFindSwidget("MessageLabel"),line);
 UxPopupInterface(UxFindSwidget("MessageShell"),exclusive_grab);
}

void InitWindows()
{
wnds[0]=XtWindow(UxGetWidget(UxFindSwidget("GaussTopform")));
}


/* ChangeCurs() : set the clock cursor.                      */
void ChangeCurs()
{
 extern Display *UxDisplay;
/*for(i=0;i<6;i++)
 XDefineCursor(UxDisplay, wnds[i], p8[specCurrCursor]);*/
 XDefineCursor(UxDisplay, wnds[0], p8[specCurrCursor]);
specCurrCursor = (specCurrCursor+1) % 8;
XFlush(UxDisplay);
}

void DefaultCurs()
{
 extern Display *UxDisplay;
for(i=0;i<6;i++)
 XDefineCursor(UxDisplay, wnds[i],(Cursor) NULL);
XFlush(UxDisplay);
}

void CreateCurs()
{
 XColor fg, bg;
 extern Display *UxDisplay;
 int scr_num;
 scr_num=DefaultScreen(UxDisplay);
 fg.red = fg.green = fg.blue = 0;
 bg.red = bg.green = bg.blue = 0xffff;
  p16[0] =  XCreatePixmapFromBitmapData(UxDisplay,
            RootWindow(UxDisplay,scr_num),(char *)clock1_bits,25,29,1,0,1);
  p16[1] =  XCreatePixmapFromBitmapData(UxDisplay,
            RootWindow(UxDisplay,scr_num),(char *)clock2_bits,25,29,1,0,1);
  p16[2] =  XCreatePixmapFromBitmapData(UxDisplay,
            RootWindow(UxDisplay,scr_num),(char *)clock3_bits,25,29,1,0,1);
  p16[3] =  XCreatePixmapFromBitmapData(UxDisplay,
            RootWindow(UxDisplay,scr_num),(char *)clock4_bits,25,29,1,0,1);
  p16[4] =  XCreatePixmapFromBitmapData(UxDisplay,
            RootWindow(UxDisplay,scr_num),(char *)clock5_bits,25,29,1,0,1);
  p16[5] =  XCreatePixmapFromBitmapData(UxDisplay,
            RootWindow(UxDisplay,scr_num),(char *)clock6_bits,25,29,1,0,1);
  p16[6] =  XCreatePixmapFromBitmapData(UxDisplay,
            RootWindow(UxDisplay,scr_num),(char *)clock7_bits,25,29,1,0,1);
  p16[7] =  XCreatePixmapFromBitmapData(UxDisplay,
            RootWindow(UxDisplay,scr_num),(char *)clock8_bits,25,29,1,0,1);
 for(i=0;i<8;i++)
  p8[i] = XCreatePixmapCursor(UxDisplay, p16[i], p16[i], &fg, &bg, 16,16);
}


/* newlabels() : set the default plot labels.                   */
void newlabels()
{
 plotDefaultTitle = FALSE;
 strcpy(plotTitle,UxGetText(UxFindSwidget("Titletext")));
 strcpy(plotLabelX,UxGetText(UxFindSwidget("labelxtext")));
 strcpy(plotLabelY,UxGetText(UxFindSwidget("labelytext")));
}


/*  putFontText(fontnum) : write the font name on "FontText" field.
                fontnum  : AGL font number.                        */
void putFontText(fontnum)
int fontnum;

{
 switch(fontnum)
 {
  case STANDARDFONT:
       UxPutText(UxFindSwidget("FontText"),"Standard");    
       break;
  case QUALITYFONT:
       UxPutText(UxFindSwidget("FontText"),"Roman");    
       break;
  case GREEKFONT :
       UxPutText(UxFindSwidget("FontText"),"Greek");    
       break;
  case SCRIPTFONT:
       UxPutText(UxFindSwidget("FontText"),"Script");    
       break;
  case OLDENGFONT:
       UxPutText(UxFindSwidget("FontText"),"Old England");    
       break;
  case TINYFONT:
       UxPutText(UxFindSwidget("FontText"),"Tiny");    
       break;
 }
}


/* save_TMP() : save alice tmp. files                         */
void save_TMP()
{
 char name[40],stm [80];
 strcpy(name,"");
 sscanf(UxGetText(UxFindSwidget("nameprg")),"%s",name);
 if(name[0]!='\0')
  {
   sprintf(stm,"cp TMPalice.prg %s",name);
   system(stm);
  }
 strcpy(name,"");
 sscanf(UxGetText(UxFindSwidget("nametbl")),"%s",name);
 if(name[0]!='\0')
  {
   sprintf(stm,"cp TMPalice.tbl %s",name);
   system(stm);
  }
 strcpy(name,"");
 sscanf(UxGetText(UxFindSwidget("namestat")),"%s",name);
 if(name[0]!='\0')
  {
   sprintf(stm,"cp TMPalice.stat %s",name);
   system(stm);
  }
 strcpy(name,"");
 sscanf(UxGetText(UxFindSwidget("namecont")),"%s",name);
 if(name[0]!='\0')
  {
   save_cont(name);
  }
}

void noframe_error()
{
 out_error("ERROR: No frame loaded");
}


/* box(xl,xu,yl,yu,mode) : draw a box in the Alice DrawingArea.
             xl,xu,yl,yu : box limits.
             mode        : X11 graphic mode.                     */
void box(xl,xu,yl,yu,mode)
float xl,xu,yl,yu;
int mode;
{
int scr_num;
Window window;
Display *display;
GC     gc;
XGCValues values;
XPoint points[5];
display = XtDisplay(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
window = XtWindow(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
scr_num=DefaultScreen(display);
if(mode == GXequiv)
 values.foreground= 244;
else
 values.foreground= 10;
values.line_style = LineOnOffDash;
gc = XCreateGC(display,RootWindow(display,scr_num),
	       (GCForeground |GCLineStyle) ,&values);
XSetFunction(display,gc,mode);
 points[0].x=points[3].x=points[4].x=(int)((xl-specXmin)*specXnorm);
 points[1].x=points[2].x=(int)((xu-specXmin)*specXnorm);
 points[0].y=points[1].y=points[4].y=200-(int)((yl-specYmin)*specYnorm);
 points[2].y=points[3].y=200-(int)((yu-specYmin)*specYnorm);
 XDrawLines(display,window,gc, points,5,CoordModeOrigin);
}


/* draw_zoom() : plot the spectrum on the DrawingArea and plot 
                 the zoom box.                                   */ 
void draw_zoom()
{
int scr_num;
Window window;
Display *display;
GC     gc;
XGCValues values;
display = XtDisplay(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
window = XtWindow(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
scr_num=DefaultScreen(display);
values.foreground= BlackPixel(display,scr_num);
values.background=  WhitePixel(display,scr_num);
gc = XCreateGC(display,RootWindow(display,scr_num),
	       (GCForeground | GCBackground),&values);
 XDrawLines(display,window,gc, specPoints,specNpix[0],CoordModeOrigin);
 box(specXcenw2-specDxw2,specXcenw2+specDxw2,
     specYcenw2-specDyw2,specYcenw2+specDyw2,GXequiv);
}

/* load_points() : read the spectrum values and set the DrawingArea
                   points.                                          */
void load_points()
{
Window window;
Display *display;
int i;
display = XtDisplay(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
window = XtWindow(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
XClearWindow(display,window);
 specXnorm = 400/(specXmax - specXmin);
 specYnorm = 200/(specYmax - specYmin);
 for(i=0;i<specNpix[0] ; i++)
  {
   specPoints[i].x = (int)((specX[i]-specXmin)*specXnorm);
   specPoints[i].y = 200 - (int)((specY[i]-specYmin)*specYnorm);
  }
}


/* get_cursor(xval,yval,keyval) : like AGL get cursor using the 
                     DrawingArea as input                        */
void get_cursor(xval,yval,keyval)
float *xval, *yval;
int *keyval;
{
Window window;
Display *display;
XEvent event;
GC     gc;
XGCValues values;
int xcur,ycur,scr_num;

display = XtDisplay(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
window = XtWindow(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
scr_num=DefaultScreen(display);
values.foreground= 254;
values.background=  WhitePixel(display,scr_num);
gc = XCreateGC(display,RootWindow(display,scr_num),
	       (GCForeground | GCBackground),&values);
XSetFunction(display,gc,GXequiv);
XSelectInput(display,window,
	     (ButtonPressMask|PointerMotionMask|ExposureMask));
xcur = 200;ycur = 100;
 XDrawLine(display,window,gc,0,ycur,400,ycur);
 XDrawLine(display,window,gc,xcur,0,xcur,200);
 UxNextEvent(&event);
while(event.type != ButtonPress /*&& UxWidget != GaussDrawingArea */)
 {
  if(event.type ==  MotionNotify)
  {
  XDrawLine(display,window,gc,0,ycur,400,ycur);
  XDrawLine(display,window,gc,xcur,0,xcur,200);
  xcur = event.xbutton.x;
  ycur = event.xbutton.y;
  XDrawLine(display,window,gc,0,ycur,400,ycur);
  XDrawLine(display,window,gc,xcur,0,xcur,200);
  }
 UxNextEvent(&event);
 }
  XDrawLine(display,window,gc,0,ycur,400,ycur);
  XDrawLine(display,window,gc,xcur,0,xcur,200);
 *keyval = (int)(event.xbutton.button);
 *xval = specXmin + xcur/specXnorm;
 *yval = specYmin + (200 - ycur)/specYnorm;
}


/* move_zoom : move the zoom box                                */
void move_zoom(xval,yval,dxval,dyval,keyval)
float *xval, *yval;
float dxval,dyval;
int *keyval;
{
Window window;
Display *display;
XEvent event;
GC     gc;
XGCValues values;
int scr_num;
float xcur,ycur;

display = XtDisplay(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
window = XtWindow(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
scr_num=DefaultScreen(display);
values.foreground= BlackPixel(display,scr_num);
values.background=  WhitePixel(display,scr_num);
gc = XCreateGC(display,RootWindow(display,scr_num),
	       (GCForeground | GCBackground),&values);
XSelectInput(display,window,
	     (ButtonPressMask|PointerMotionMask|ExposureMask));
xcur = *xval;
ycur = *yval;
 UxNextEvent(&event);
while(event.type != ButtonPress /*&& UxWidget != GaussDrawingArea */)
 {
  if(event.type ==  MotionNotify)
  {
  box(xcur-dxval,xcur+dxval,ycur-dyval,ycur+dyval,GXequiv);
  xcur = specXmin +event.xbutton.x/specXnorm;
  ycur = specYmin +(200-event.xbutton.y)/specYnorm;
  box(xcur-dxval,xcur+dxval,ycur-dyval,ycur+dyval,GXequiv);
  }
 UxNextEvent(&event);
 }
  box(xcur-dxval,xcur+dxval,ycur-dyval,ycur+dyval,GXequiv);
 *keyval = (int)(event.xbutton.button);
 *xval = xcur;
 *yval = ycur;
}
