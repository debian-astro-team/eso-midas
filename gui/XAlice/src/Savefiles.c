/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	Savefiles.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxText.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxSavefiles;
	swidget	Uxform12;
	swidget	Uxnameprg;
	swidget	Uxnametbl;
	swidget	Uxnamestat;
	swidget	Uxlabel17;
	swidget	Uxlabel18;
	swidget	Uxlabel19;
	swidget	Uxlabel20;
	swidget	Uxlabel21;
	swidget	Uxseparator5;
	swidget	Uxseparator6;
	swidget	Uxlabel22;
	swidget	Uxnamecont;
} _UxCSavefiles;

#define Savefiles               UxSavefilesContext->UxSavefiles
#define form12                  UxSavefilesContext->Uxform12
#define nameprg                 UxSavefilesContext->Uxnameprg
#define nametbl                 UxSavefilesContext->Uxnametbl
#define namestat                UxSavefilesContext->Uxnamestat
#define label17                 UxSavefilesContext->Uxlabel17
#define label18                 UxSavefilesContext->Uxlabel18
#define label19                 UxSavefilesContext->Uxlabel19
#define label20                 UxSavefilesContext->Uxlabel20
#define label21                 UxSavefilesContext->Uxlabel21
#define separator5              UxSavefilesContext->Uxseparator5
#define separator6              UxSavefilesContext->Uxseparator6
#define label22                 UxSavefilesContext->Uxlabel22
#define namecont                UxSavefilesContext->Uxnamecont

static _UxCSavefiles	*UxSavefilesContext;

extern void save_TMP();



swidget	OkSaveFiles;
swidget	CancelSaveFiles;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*SaveTab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_Savefiles();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_OkSaveFiles( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSavefiles           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSavefilesContext;
	UxSavefilesContext = UxContext =
			(_UxCSavefiles *) UxGetContext( UxThisWidget );
	{
	save_TMP();
	UxPopdownInterface(Savefiles);
	}
	UxSavefilesContext = UxSaveCtx;
}

static void	activateCB_CancelSaveFiles( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSavefiles           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSavefilesContext;
	UxSavefilesContext = UxContext =
			(_UxCSavefiles *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(Savefiles);
	}
	UxSavefilesContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_Savefiles()
{
	UxPutKeyboardFocusPolicy( Savefiles, "pointer" );
	UxPutTitle( Savefiles, "Save TMP files" );
	UxPutHeight( Savefiles, 210 );
	UxPutWidth( Savefiles, 369 );
	UxPutY( Savefiles, 320 );
	UxPutX( Savefiles, 505 );

	UxPutBackground( form12, ButtonBackground );
	UxPutHeight( form12, 222 );
	UxPutWidth( form12, 369 );
	UxPutY( form12, 22 );
	UxPutX( form12, 66 );
	UxPutUnitType( form12, "pixels" );
	UxPutResizePolicy( form12, "resize_none" );

	UxPutMarginHeight( nameprg, 2 );
	UxPutHighlightColor( nameprg, "Black" );
	UxPutForeground( nameprg, TextForeground );
	UxPutTranslations( nameprg, SaveTab );
	UxPutFontList( nameprg, TextFont );
	UxPutHighlightOnEnter( nameprg, "true" );
	UxPutBackground( nameprg, TextBackground );
	UxPutHeight( nameprg, 30 );
	UxPutWidth( nameprg, 170 );
	UxPutY( nameprg, 32 );
	UxPutX( nameprg, 172 );

	UxPutMarginHeight( nametbl, 2 );
	UxPutHighlightColor( nametbl, "Black" );
	UxPutForeground( nametbl, TextForeground );
	UxPutTranslations( nametbl, SaveTab );
	UxPutFontList( nametbl, TextFont );
	UxPutHighlightOnEnter( nametbl, "true" );
	UxPutBackground( nametbl, TextBackground );
	UxPutHeight( nametbl, 30 );
	UxPutWidth( nametbl, 170 );
	UxPutY( nametbl, 64 );
	UxPutX( nametbl, 172 );

	UxPutMarginHeight( namestat, 2 );
	UxPutHighlightColor( namestat, "Black" );
	UxPutForeground( namestat, TextForeground );
	UxPutTranslations( namestat, SaveTab );
	UxPutFontList( namestat, TextFont );
	UxPutHighlightOnEnter( namestat, "true" );
	UxPutBackground( namestat, TextBackground );
	UxPutHeight( namestat, 30 );
	UxPutWidth( namestat, 170 );
	UxPutY( namestat, 96 );
	UxPutX( namestat, 172 );

	UxPutHighlightColor( label17, "Black" );
	UxPutForeground( label17, TextForeground );
	UxPutFontList( label17, TextFont );
	UxPutLabelString( label17, "TMPalice.prg" );
	UxPutBackground( label17, ButtonBackground );
	UxPutHeight( label17, 30 );
	UxPutWidth( label17, 138 );
	UxPutY( label17, 32 );
	UxPutX( label17, 26 );

	UxPutHighlightColor( label18, "Black" );
	UxPutForeground( label18, TextForeground );
	UxPutFontList( label18, TextFont );
	UxPutLabelString( label18, "TMPalice.tbl" );
	UxPutBackground( label18, ButtonBackground );
	UxPutHeight( label18, 30 );
	UxPutWidth( label18, 138 );
	UxPutY( label18, 64 );
	UxPutX( label18, 26 );

	UxPutHighlightColor( label19, "Black" );
	UxPutForeground( label19, TextForeground );
	UxPutFontList( label19, TextFont );
	UxPutLabelString( label19, "TMPalice.stat" );
	UxPutBackground( label19, ButtonBackground );
	UxPutHeight( label19, 30 );
	UxPutWidth( label19, 138 );
	UxPutY( label19, 96 );
	UxPutX( label19, 25 );

	UxPutHighlightColor( label20, "Black" );
	UxPutForeground( label20, TextForeground );
	UxPutLabelString( label20, "TMP file" );
	UxPutFontList( label20, TextFont );
	UxPutBackground( label20, ButtonBackground );
	UxPutHeight( label20, 20 );
	UxPutWidth( label20, 142 );
	UxPutY( label20, 4 );
	UxPutX( label20, 26 );

	UxPutHighlightColor( label21, "Black" );
	UxPutForeground( label21, TextForeground );
	UxPutLabelString( label21, "New name" );
	UxPutFontList( label21, TextFont );
	UxPutBackground( label21, ButtonBackground );
	UxPutHeight( label21, 20 );
	UxPutWidth( label21, 134 );
	UxPutY( label21, 4 );
	UxPutX( label21, 180 );

	UxPutHighlightColor( OkSaveFiles, "Black" );
	UxPutHighlightOnEnter( OkSaveFiles, "true" );
	UxPutLabelString( OkSaveFiles, "Ok" );
	UxPutForeground( OkSaveFiles, ButtonForeground );
	UxPutFontList( OkSaveFiles, BoldTextFont );
	UxPutBackground( OkSaveFiles, ButtonBackground );
	UxPutHeight( OkSaveFiles, 30 );
	UxPutWidth( OkSaveFiles, 90 );
	UxPutY( OkSaveFiles, 168 );
	UxPutX( OkSaveFiles, 27 );

	UxPutHighlightColor( CancelSaveFiles, "Black" );
	UxPutHighlightOnEnter( CancelSaveFiles, "true" );
	UxPutLabelString( CancelSaveFiles, "Cancel" );
	UxPutForeground( CancelSaveFiles, CancelForeground );
	UxPutFontList( CancelSaveFiles, BoldTextFont );
	UxPutBackground( CancelSaveFiles, ButtonBackground );
	UxPutHeight( CancelSaveFiles, 30 );
	UxPutWidth( CancelSaveFiles, 90 );
	UxPutY( CancelSaveFiles, 168 );
	UxPutX( CancelSaveFiles, 135 );

	UxPutBackground( separator5, ButtonBackground );
	UxPutHeight( separator5, 8 );
	UxPutWidth( separator5, 136 );
	UxPutY( separator5, 22 );
	UxPutX( separator5, 30 );

	UxPutBackground( separator6, ButtonBackground );
	UxPutHeight( separator6, 8 );
	UxPutWidth( separator6, 172 );
	UxPutY( separator6, 22 );
	UxPutX( separator6, 172 );

	UxPutHighlightColor( label22, "Black" );
	UxPutForeground( label22, TextForeground );
	UxPutFontList( label22, TextFont );
	UxPutLabelString( label22, "TMPcont.bdf" );
	UxPutBackground( label22, ButtonBackground );
	UxPutHeight( label22, 30 );
	UxPutWidth( label22, 138 );
	UxPutY( label22, 132 );
	UxPutX( label22, 25 );

	UxPutMarginHeight( namecont, 2 );
	UxPutHighlightColor( namecont, "Black" );
	UxPutForeground( namecont, TextForeground );
	UxPutTranslations( namecont, SaveTab );
	UxPutFontList( namecont, TextFont );
	UxPutHighlightOnEnter( namecont, "true" );
	UxPutBackground( namecont, TextBackground );
	UxPutHeight( namecont, 30 );
	UxPutWidth( namecont, 170 );
	UxPutY( namecont, 132 );
	UxPutX( namecont, 172 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_Savefiles()
{
	/* Create the swidgets */

	Savefiles = UxCreateTopLevelShell( "Savefiles", NO_PARENT );
	UxPutContext( Savefiles, UxSavefilesContext );

	form12 = UxCreateForm( "form12", Savefiles );
	nameprg = UxCreateText( "nameprg", form12 );
	nametbl = UxCreateText( "nametbl", form12 );
	namestat = UxCreateText( "namestat", form12 );
	label17 = UxCreateLabel( "label17", form12 );
	label18 = UxCreateLabel( "label18", form12 );
	label19 = UxCreateLabel( "label19", form12 );
	label20 = UxCreateLabel( "label20", form12 );
	label21 = UxCreateLabel( "label21", form12 );
	OkSaveFiles = UxCreatePushButton( "OkSaveFiles", form12 );
	CancelSaveFiles = UxCreatePushButton( "CancelSaveFiles", form12 );
	separator5 = UxCreateSeparator( "separator5", form12 );
	separator6 = UxCreateSeparator( "separator6", form12 );
	label22 = UxCreateLabel( "label22", form12 );
	namecont = UxCreateText( "namecont", form12 );

	_Uxinit_Savefiles();

	/* Create the X widgets */

	UxCreateWidget( Savefiles );
	UxCreateWidget( form12 );
	UxCreateWidget( nameprg );
	UxCreateWidget( nametbl );
	UxCreateWidget( namestat );
	UxCreateWidget( label17 );
	UxCreateWidget( label18 );
	UxCreateWidget( label19 );
	UxCreateWidget( label20 );
	UxCreateWidget( label21 );
	UxCreateWidget( OkSaveFiles );
	UxCreateWidget( CancelSaveFiles );
	UxCreateWidget( separator5 );
	UxCreateWidget( separator6 );
	UxCreateWidget( label22 );
	UxCreateWidget( namecont );

	UxAddCallback( OkSaveFiles, XmNactivateCallback,
			activateCB_OkSaveFiles,
			(XtPointer) UxSavefilesContext );

	UxAddCallback( CancelSaveFiles, XmNactivateCallback,
			activateCB_CancelSaveFiles,
			(XtPointer) UxSavefilesContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( Savefiles );

	return ( Savefiles );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_Savefiles()
{
	swidget                 rtrn;
	_UxCSavefiles           *UxContext;

	UxSavefilesContext = UxContext =
		(_UxCSavefiles *) UxMalloc( sizeof(_UxCSavefiles) );

	rtrn = _Uxbuild_Savefiles();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_Savefiles()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_Savefiles();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

