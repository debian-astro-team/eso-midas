/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090825		last modif

===========================================================================*/


/*******************************************************************************
	RebinShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxText.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxRebinShell;
	swidget	UxRebinTopForm;
	swidget	UxRebinRowColumn;
	swidget	UxRebintb1;
	swidget	UxRebintb2;
	swidget	UxRebintb3;
	swidget	UxZRebintext;
	swidget	UxZRebinLabel;
	swidget	UxRebinForm2;
	swidget	Uxseparator4;
} _UxCRebinShell;

#define RebinShell              UxRebinShellContext->UxRebinShell
#define RebinTopForm            UxRebinShellContext->UxRebinTopForm
#define RebinRowColumn          UxRebinShellContext->UxRebinRowColumn
#define Rebintb1                UxRebinShellContext->UxRebintb1
#define Rebintb2                UxRebinShellContext->UxRebintb2
#define Rebintb3                UxRebinShellContext->UxRebintb3
#define ZRebintext              UxRebinShellContext->UxZRebintext
#define ZRebinLabel             UxRebinShellContext->UxZRebinLabel
#define RebinForm2              UxRebinShellContext->UxRebinForm2
#define separator4              UxRebinShellContext->Uxseparator4


extern void spec(), load_points(), draw_zoom();

extern int rebin();


static _UxCRebinShell	*UxRebinShellContext;

swidget	OkRebin;
swidget	CancelRebin;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*ZTab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
 <Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_RebinShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	createCB_Rebintb1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
	rebinOption = ZOPT;
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	armCB_Rebintb1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
	rebinOption= ZOPT;
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	armCB_Rebintb2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
	rebinOption=LOGOPT;
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	armCB_Rebintb3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
	rebinOption = INVOPT;
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	activateCB_OkRebin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
	float z,X[MAXVALUES],Y[MAXVALUES];
	int i;
	sscanf(UxGetText(UxFindSwidget("ZRebintext")),"%f",&z);
	for(i=0;i<specNpix[0] ; i++)
	 {
	  X[i] = specX[i];
	  Y[i] = specY[i]; 
	 }
	rebin(rebinOption,X,specX,Y,specY,specNpix[0],z,&specXmin,
	      &specXmax,&specYmin,&specYmax);
	rebinRebin =TRUE;
	UxPopdownInterface(UxFindSwidget("RebinShell"));
	specYcen = (specYmin+specYmax)/2;
	specDy    = specYmax - specYcen;
	specYcenw2 = specYcen;
	specDyw2 = specDy;
	specXcen = (specXmin+specXmax)/2;
	specDx    = specXmax - specXcen;
	specDxw2  = specDx;
	specXcenw2 = specXcen;
	spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	     specYcen-specDy,specYcen+specDy,PLOTMODE);
	load_points();
	draw_zoom(); 
	}
	UxRebinShellContext = UxSaveCtx;
}

static void	activateCB_CancelRebin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRebinShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRebinShellContext;
	UxRebinShellContext = UxContext =
			(_UxCRebinShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(RebinShell);
	}
	UxRebinShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_RebinShell()
{
	UxPutIconName( RebinShell, "Rebin" );
	UxPutKeyboardFocusPolicy( RebinShell, "pointer" );
	UxPutBackground( RebinShell, WindowBackground );
	UxPutHeight( RebinShell, 166 );
	UxPutWidth( RebinShell, 306 );
	UxPutY( RebinShell, 484 );
	UxPutX( RebinShell, 579 );

	UxPutBackground( RebinTopForm, WindowBackground );
	UxPutHeight( RebinTopForm, 170 );
	UxPutWidth( RebinTopForm, 306 );
	UxPutY( RebinTopForm, -3 );
	UxPutX( RebinTopForm, -2 );
	UxPutUnitType( RebinTopForm, "pixels" );
	UxPutResizePolicy( RebinTopForm, "resize_none" );

	UxPutBackground( RebinRowColumn, LabelBackground );
	UxPutRadioBehavior( RebinRowColumn, "true" );
	UxPutHeight( RebinRowColumn, 92 );
	UxPutWidth( RebinRowColumn, 130 );
	UxPutY( RebinRowColumn, 14 );
	UxPutX( RebinRowColumn, 26 );

	UxPutHighlightColor( Rebintb1, "Black" );
	UxPutForeground( Rebintb1, TextForeground );
	UxPutFontList( Rebintb1, TextFont );
	UxPutSet( Rebintb1, "true" );
	UxPutLabelString( Rebintb1, " x ` = x / (1+z)" );
	UxPutIndicatorOn( Rebintb1, "true" );
	UxPutSelectColor( Rebintb1, SelectColor );
	UxPutBackground( Rebintb1, LabelBackground );
	UxPutHeight( Rebintb1, 22 );
	UxPutWidth( Rebintb1, 66 );
	UxPutY( Rebintb1, 32 );
	UxPutX( Rebintb1, 64 );

	UxPutHighlightColor( Rebintb2, "Black" );
	UxPutForeground( Rebintb2, TextForeground );
	UxPutFontList( Rebintb2, TextFont );
	UxPutLabelString( Rebintb2, " x ` = log(x)" );
	UxPutIndicatorOn( Rebintb2, "true" );
	UxPutSelectColor( Rebintb2, SelectColor );
	UxPutBackground( Rebintb2, LabelBackground );
	UxPutHeight( Rebintb2, 22 );
	UxPutWidth( Rebintb2, 86 );
	UxPutY( Rebintb2, 32 );
	UxPutX( Rebintb2, 22 );

	UxPutHighlightColor( Rebintb3, "Black" );
	UxPutForeground( Rebintb3, TextForeground );
	UxPutFontList( Rebintb3, TextFont );
	UxPutLabelString( Rebintb3, " x ` = 1/x" );
	UxPutIndicatorOn( Rebintb3, "true" );
	UxPutSelectColor( Rebintb3, SelectColor );
	UxPutBackground( Rebintb3, LabelBackground );
	UxPutHeight( Rebintb3, 20 );
	UxPutWidth( Rebintb3, 48 );
	UxPutY( Rebintb3, 60 );
	UxPutX( Rebintb3, 26 );

	UxPutMarginHeight( ZRebintext, 3 );
	UxPutTopShadowColor( ZRebintext, LabelBackground );
	UxPutHighlightColor( ZRebintext, "Black" );
	UxPutForeground( ZRebintext, TextForeground );
	UxPutTranslations( ZRebintext, ZTab );
	UxPutFontList( ZRebintext, TextFont );
	UxPutBackground( ZRebintext, TextBackground );
	UxPutHeight( ZRebintext, 32 );
	UxPutWidth( ZRebintext, 96 );
	UxPutY( ZRebintext, 12 );
	UxPutX( ZRebintext, 198 );

	UxPutHighlightColor( ZRebinLabel, "Black" );
	UxPutForeground( ZRebinLabel, TextForeground );
	UxPutLabelType( ZRebinLabel, "string" );
	UxPutFontList( ZRebinLabel, TextFont );
	UxPutLabelString( ZRebinLabel, "z:" );
	UxPutBackground( ZRebinLabel, LabelBackground );
	UxPutHeight( ZRebinLabel, 26 );
	UxPutWidth( ZRebinLabel, 24 );
	UxPutY( ZRebinLabel, 16 );
	UxPutX( ZRebinLabel, 176 );

	UxPutBackground( RebinForm2, ButtonBackground );
	UxPutHeight( RebinForm2, 50 );
	UxPutWidth( RebinForm2, 310 );
	UxPutY( RebinForm2, 116 );
	UxPutX( RebinForm2, -2 );
	UxPutResizePolicy( RebinForm2, "resize_none" );

	UxPutHighlightColor( OkRebin, "#000000" );
	UxPutHighlightOnEnter( OkRebin, "true" );
	UxPutLabelString( OkRebin, "Ok" );
	UxPutForeground( OkRebin, ButtonForeground );
	UxPutFontList( OkRebin, BoldTextFont );
	UxPutBackground( OkRebin, ButtonBackground );
	UxPutHeight( OkRebin, 30 );
	UxPutWidth( OkRebin, 90 );
	UxPutY( OkRebin, 10 );
	UxPutX( OkRebin, 24 );

	UxPutHighlightColor( CancelRebin, "#000000" );
	UxPutHighlightOnEnter( CancelRebin, "true" );
	UxPutLabelString( CancelRebin, "Cancel" );
	UxPutForeground( CancelRebin, CancelForeground );
	UxPutFontList( CancelRebin, BoldTextFont );
	UxPutBackground( CancelRebin, ButtonBackground );
	UxPutHeight( CancelRebin, 30 );
	UxPutWidth( CancelRebin, 90 );
	UxPutY( CancelRebin, 10 );
	UxPutX( CancelRebin, 136 );

	UxPutBackground( separator4, ButtonBackground );
	UxPutHeight( separator4, 8 );
	UxPutWidth( separator4, 306 );
	UxPutY( separator4, 110 );
	UxPutX( separator4, 2 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_RebinShell()
{
	/* Create the swidgets */

	RebinShell = UxCreateTopLevelShell( "RebinShell", NO_PARENT );
	UxPutContext( RebinShell, UxRebinShellContext );

	RebinTopForm = UxCreateForm( "RebinTopForm", RebinShell );
	RebinRowColumn = UxCreateRowColumn( "RebinRowColumn", RebinTopForm );
	Rebintb1 = UxCreateToggleButton( "Rebintb1", RebinRowColumn );
	Rebintb2 = UxCreateToggleButton( "Rebintb2", RebinRowColumn );
	Rebintb3 = UxCreateToggleButton( "Rebintb3", RebinRowColumn );
	ZRebintext = UxCreateText( "ZRebintext", RebinTopForm );
	ZRebinLabel = UxCreateLabel( "ZRebinLabel", RebinTopForm );
	RebinForm2 = UxCreateForm( "RebinForm2", RebinTopForm );
	OkRebin = UxCreatePushButton( "OkRebin", RebinForm2 );
	CancelRebin = UxCreatePushButton( "CancelRebin", RebinForm2 );
	separator4 = UxCreateSeparator( "separator4", RebinTopForm );

	_Uxinit_RebinShell();

	/* Create the X widgets */

	UxCreateWidget( RebinShell );
	UxCreateWidget( RebinTopForm );
	UxCreateWidget( RebinRowColumn );
	UxCreateWidget( Rebintb1 );
	createCB_Rebintb1( UxGetWidget( Rebintb1 ),
			(XtPointer) UxRebinShellContext, (XtPointer) NULL );

	UxCreateWidget( Rebintb2 );
	UxCreateWidget( Rebintb3 );
	UxCreateWidget( ZRebintext );
	UxCreateWidget( ZRebinLabel );
	UxCreateWidget( RebinForm2 );
	UxCreateWidget( OkRebin );
	UxCreateWidget( CancelRebin );
	UxCreateWidget( separator4 );

	UxAddCallback( Rebintb1, XmNarmCallback,
			armCB_Rebintb1,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( Rebintb2, XmNarmCallback,
			armCB_Rebintb2,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( Rebintb3, XmNarmCallback,
			armCB_Rebintb3,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( OkRebin, XmNactivateCallback,
			activateCB_OkRebin,
			(XtPointer) UxRebinShellContext );

	UxAddCallback( CancelRebin, XmNactivateCallback,
			activateCB_CancelRebin,
			(XtPointer) UxRebinShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( RebinShell );

	return ( RebinShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_RebinShell()
{
	swidget                 rtrn;
	_UxCRebinShell          *UxContext;

	UxRebinShellContext = UxContext =
		(_UxCRebinShell *) UxMalloc( sizeof(_UxCRebinShell) );

	rtrn = _Uxbuild_RebinShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_RebinShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_RebinShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

