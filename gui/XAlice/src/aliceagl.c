/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* .VERSION
   090825	last modif
*/


#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include <stdio.h>              /* generic definitions */
#include <stdlib.h>             /* function definitions */
#include <string.h>             /* string definitions */
#include <unistd.h>             /* string definitions */

#include <midas_def.h>
#include <alice_global.h>
#define DRAW "color=1"
#define NODRAW "color=0"
#define LABELPLT "alicel.plt"
#define LABELADD "alicel.plt/a"
#define TRUE 1
#define FALSE 0
#define ANDMODE  GXand
#define EQUIVMODE  GXequiv
#define COPYMODE   GXcopy

double          DspecStart, DspecStep, DspecX;

void  spec(), vdef_wspec(), move_zoom(), box(), plot_over(), get_cursor();
void init_values(), load_image(), load_points(), draw_zoom(), auto_fit();
void plot_fit(), plot_spline();

int  is_auto_fit();



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  init_alice()  : Initial values for XAlice
--------------------------------------------------------------------------*/
void init_alice()
{
  void CreateCurs();

  SCSPRO("ALICE");
  specStepWin = .2;
  fitDegree = 1;
  specLineNum = 1;
  specLineStep = 1;
  plotFont = 0;
  plotSize = 1.5;
  plotAngle = 0.0;
  gaussMaxIterations = 50;
  gaussMinIterations = 1;
  gaussNumOfComp = 1;
  gaussNumOfSol = 0;
  gaussNumOfFitData = 0;
  filterWindWidth = 3;
  fitPairNum = 0;
  caseList = 0;
  plotDefaultTitle = 1;
  rebinRebin = FALSE;
  plotMode = LINEMODE;
  OverPlotNum = 0;
  CreateCurs();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  clearLabels() : clean the TMPalice.prg file.
   OBS:  TMPalice.prg save all the LABEL commands.
--------------------------------------------------------------------------*/
void clearLabels()
{
  system("rm -f TMPalice.prg;touch TMPalice.prg");
  AG_VDEF("null:", 0.0, 1.0, 0.0, 1.0, 0.0, 0.0);
  AG_MOPN(LABELPLT);
  AG_SSET("changl=0.0;chdi=1.0,1.0");
  AG_MCLS();
  AG_CLS();
}

void end_alice()
{
  SCSEPI();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 zoom(opt) :  Change the current spectrum window (AliceDrawingArea) and 
              plot the curren spectrum.
--------------------------------------------------------------------------*/
void zoom(opt)
  int             opt;
{
  float           xcen, ycen;
  float           x0, x1, x2, y0, y1, y2;

  key = 1;
  switch (opt)
  {
  case MOVE_WIN:
  /* move the current window  ussing the cursor (AliceDrawingArea)*/
    while (key == 1)
    {
      xcen = specXcenw2;
      ycen = specYcenw2;
      specDx = specDxw2;
      specDy = specDyw2;
      move_zoom(&xcen, &ycen, specDx, specDy, &key);
      if (key == 1)
      {
        specXcenw2 = xcen;
        specYcenw2 = ycen;
      }
      specXcen = specXcenw2;
      specYcen = specYcenw2;
      box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
          specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
      if (key == 1)
      {
        spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
             specYcen - specDy, specYcen + specDy, PLOTMODE);
        if (OverPlotMode)
          plot_over();
      }
    }
    break;
  case MOVE_X1:
  /* Change the X limits (AliceDrawingArea) using the cursor*/
    while (key == 1)
    {
      get_cursor(&xcen, &ycen, &key);
      if (key == 1)
      {
        x0 = xcen;
        get_cursor(&xcen, &ycen, &key);
        if (key == 1)
        {
          x1 = xcen;
          if (x0 > x1)
          {
            x2 = x1;
            x1 = x0;
            x0 = x2;
          }
          box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
              specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
          specXcen = x0 + (x1 - x0) / 2;
          specXcenw2 = specXcen;
          specDx = specXcen - x0;
          specDxw2 = specDx;
          box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
              specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
          spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
               specYcen - specDy, specYcen + specDy, PLOTMODE);
          if (OverPlotMode)
            plot_over();
        }
      }
    }
    break;
  case MOVE_Y1:
  /* Change the Y limits (AliceDrawingArea) using the cursor*/
    while (key == 1)
    {
      get_cursor(&xcen, &ycen, &key);
      if (key == 1)
      {
        y0 = ycen;
        get_cursor(&xcen, &ycen, &key);
        if (key == 1)
        {
          y1 = ycen;
          if (y0 > y1)
          {
            y2 = y1;
            y1 = y0;
            y0 = y2;
          }
          box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
              specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
          specYcen = y0 + (y1 - y0) / 2;
          specYcenw2 = specYcen;
          specDy = specYcen - y0;
          specDyw2 = specDy;
          box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
              specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
          spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
               specYcen - specDy, specYcen + specDy, PLOTMODE);
          if (OverPlotMode)
            plot_over();
        }
      }
    }
    break;
  case STEPUP_X:
  /* Change the X limits (AliceDrawingArea) using the up_arrow */
    box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
        specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
    specDxw2 += specStepWin * (specXmax - specXmin) / 4;
    box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
        specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
    specXcen = specXcenw2;
    specYcen = specYcenw2;
    specDx = specDxw2;
    specDy = specDyw2;
    spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
         specYcen - specDy, specYcen + specDy, PLOTMODE);
    if (OverPlotMode)
      plot_over();
    break;
  case STEPUP_Y:
  /* Change the Y limits (AliceDrawingArea) using the up_arrow */
    box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
        specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
    specDyw2 += specStepWin * (specYmax - specYmin) / 4;;
    box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
        specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
    specXcen = specXcenw2;
    specYcen = specYcenw2;
    specDx = specDxw2;
    specDy = specDyw2;
    spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
         specYcen - specDy, specYcen + specDy, PLOTMODE);
    if (OverPlotMode)
      plot_over();
    break;
  case STEPDN_X:
  /* Change the X limits (AliceDrawingArea) using the dn_arrow */
    box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
        specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
    specDxw2 -= specStepWin * (specXmax - specXmin) / 4;;
    box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
        specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
    specXcen = specXcenw2;
    specYcen = specYcenw2;
    specDx = specDxw2;
    specDy = specDyw2;
    spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
         specYcen - specDy, specYcen + specDy, PLOTMODE);
    if (OverPlotMode)
      plot_over();
    break;
  case STEPDN_Y:
  /* Change the Y limits (AliceDrawingArea) using the dn_arrow */
    box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
        specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
    specDyw2 -= specStepWin * (specYmax - specYmin) / 4;;
    box(specXcenw2 - specDxw2, specXcenw2 + specDxw2,
        specYcenw2 - specDyw2, specYcenw2 + specDyw2, EQUIVMODE);
    specXcen = specXcenw2;
    specYcen = specYcenw2;
    specDx = specDxw2;
    specDy = specDyw2;
    spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
         specYcen - specDy, specYcen + specDy, PLOTMODE);
    if (OverPlotMode)
      plot_over();
    break;
  case MOVE_X2:
  /* Change the X limits (MidasWindow) using the cursor */
    while (key == 1)
    {
      vdef_wspec();
      AG_VLOC(&xcen, &ycen, &key, &pixel);
      if (key == 1)
      {
        x0 = xcen;
        AG_VLOC(&xcen, &ycen, &key, &pixel);
        if (key == 1)
        {
          x1 = xcen;
          if (x0 > x1)
          {
            x2 = x1;
            x1 = x0;
            x0 = x2;
          }
          specXcen = x0 + (x1 - x0) / 2;
          specDx = specXcen - x0;
          AG_CLS();
          spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
               specYcen - specDy, specYcen + specDy, PLOTMODE);
          if (OverPlotMode)
            plot_over();
        }
      }
    }
    break;
  case MOVE_Y2:
  /* Change the Y limits (MidasWindow) using the cursor */
    while (key == 1)
    {
      vdef_wspec();
      AG_VLOC(&xcen, &ycen, &key, &pixel);
      if (key == 1)
      {
        y0 = ycen;
        AG_VLOC(&xcen, &ycen, &key, &pixel);
        if (key == 1)
        {
          y1 = ycen;
          if (y0 > y1)
          {
            y2 = y1;
            y1 = y0;
            y0 = y2;
          }
          specYcen = y0 + (y1 - y0) / 2;
          specDy = specYcen - y0;
          AG_CLS();
          spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
               specYcen - specDy, specYcen + specDy, PLOTMODE);
          if (OverPlotMode)
            plot_over();
        }
      }
    }
    break;
  }
  AG_CLS();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 spec(xs, ys, n, xsmin, xsmax, ysmin, ysmax, mode)
 Description : plot a spectrum on Midas Graphic window.
  xs,ys      : x and y values to be plotted.
  xsmin,xsmax: x limits.
  ysmin,ysmax: y limits.
  mode       : PLOT or OVERPLOT.
--------------------------------------------------------------------------*/
void spec(xs, ys, n, xsmin, xsmax, ysmin, ysmax, mode)
  float          *xs, *ys, xsmin, xsmax, ysmin, ysmax;
  int             n, mode;
{
  char            titulo[180];
  char            sset[80], outtext[100], text[80], line[160];
  float           values[4];
  float           xpos, ypos, scale, ang;
  int             font;
  FILE           *fp;

  values[0] = xsmin;
  values[1] = xsmax;
  SCKWRR("PLRSTAT", values, 11, 2, &unit);
  values[0] = ysmin;
  values[1] = ysmax;
  SCKWRR("PLRSTAT", values, 15, 2, &unit);
  SCKWRR("PLRSTAT", specClip, 23, 4, &unit);
  if (plotDefaultTitle)
  {
    if (specLineStep != 1)
      sprintf(plotTitle, "File: %s    Line %d to %d", specImageName,
        specLineNum, specLineNum + specLineStep - 1);
    else
      sprintf(plotTitle, "File: %s    Line %d", specImageName, specLineNum);
  }
  sprintf(titulo, "TITLE= %s;LABX=%s;LABY=%s", plotTitle,
    plotLabelX, plotLabelY);
  if (mode == OVERMODE)
  {
    vdef_wspec();
    AG_MOPN(PLOTAPP);
  } else
  {
    AG_VDEF("graph_wnd0:", 0.0, 1.0, 0.0, 1.0, 0.0, 0.0);
    AG_MOPN(PLOTNAME);
    AG_SSET("FONT=1;CHDI=1.2,1.2");
    AG_AXES(xsmin, xsmax, ysmin, ysmax, titulo);
    specAxesXl = xsmin;
    specAxesXu = xsmax;
    specAxesYl = ysmin;
    specAxesYu = ysmax;
    AG_RGET("CLPL", specClip);
  }
  switch (plotMode)
  {
  case LINEMODE:
    AG_GPLL(xs, ys, n);
    break;
  case HISTMODE:
    AG_HIST(xs, ys, n, 0, 0);
    break;
  }
  AG_VUPD();
  AG_MCLS();
  AG_MOPN(LABELPLT);
  if ((fp = fopen("TMPalice.prg", "r")) != NULL)
  {
    while (fgets(line, 160, fp) != NULL)
    {
      if (sscanf(line, "NLABEL/GRAPHIC \"%[^\"]\" %g,%g %d %g %g",
            text, &xpos, &ypos, &font, &scale, &ang) != 0)
      {
        sprintf(outtext, "~%d%s", font, text);
        sprintf(sset, "chang=%f;chdi=%f,%f", ang, scale, scale);
        AG_SSET(sset);
        AG_GTXT(xpos, ypos, outtext, 1);
      }
    }
    fclose(fp);
  }
  AG_MCLS();
  AG_CLS();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 vloc(xvloc, yvloc, kvloc) : return the cursor position from the Midas 
                             Window.
  xvloc,yvloc : x and y values (output).
  kvloc       : mouse button   (output). 
--------------------------------------------------------------------------*/
void vloc(xvloc, yvloc, kvloc)
  float          *xvloc, *yvloc;
  int            *kvloc;
{
  int             pixel;

  vdef_wspec();
  AG_SSET("CURSOR = 2");
  AG_VLOC(xvloc, yvloc, kvloc, &pixel);
  AG_VUPD();
  AG_CLS();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 labelCursor(text, ang, scale, fontnum) : put a text string on the frame.
    text   : string label.
    ang    : angle.
    scale  : scale factor.
    fontnum: AGL font number.
--------------------------------------------------------------------------*/
void labelCursor(text, ang, scale, fontnum)
  char           *text;
  float           ang, scale;
  int             fontnum;
{
  char            outtext[240];
  char            sset[80];
  int             key = 1, pixval, MOVE = FALSE;
  float           xv, yv, xant, yant;
  FILE           *fp;


xant = yant = 0.0;

  vdef_wspec();
  AG_SSET("CURSOR = 2");
  sprintf(sset, "chang=%f;chdi=%f,%f;font=%d", ang, scale, scale, fontnum);
  AG_SSET(sset);
  sprintf(outtext, "%s", text);
  while (key == 1)
  {
    AG_VLOC(&xv, &yv, &key, &pixval);
    if (MOVE)
    {
      AG_SSET(NODRAW);
      AG_GTXT(xant, yant, outtext, 1);
    }
    AG_SSET(DRAW);
    if (key == 1)
    {
      AG_GTXT(xv, yv, outtext, 1);
      xant = xv;
      yant = yv;
    }
    MOVE = TRUE;
  }
  sprintf(sset, "chang=%f;chdi=%f,%f;font=%d", ang, scale, scale, fontnum);
  AG_MOPN(LABELADD);
  AG_SSET(sset);
  AG_GTXT(xant, yant, outtext, 1);
  AG_VUPD();
  AG_MCLS();
  AG_CLS();
  if ((fp = fopen("TMPalice.prg", "a")) != NULL)
  {
    fprintf(fp, "NLABEL/GRAPHIC \"%s\" %g,%g %d %g %g\n", text, xant, yant, fontnum, scale, ang);
    fclose(fp);
  } else
    printf("ERROR : can't open file TMPalice.prg\n");
  spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
       specYcen - specDy, specYcen + specDy, PLOTMODE);
  if (OverPlotMode)
    plot_over();
}

/* read_image()  : load and plot an image (.bdf) */
void read_image(name)
  char           *name;
{
  float           xcen, ycen;

  if (strcmp(name, specLastName) != 0)
  {
    specLineStep = 1;
    specLineNum = 1;
    init_values();
    clearLabels();
  }
  plotDefaultTitle = 1;
  strcpy(plotLabelX, "Position");
  strcpy(plotLabelY, "Pixel value");
  load_image(name, specX, specY, specLineNum, specLineStep, 0, TRUE);
  if (rebinRebin || strcmp(name, specLastName) != 0)
  {
    xcen = specXmin + (specXmax - specXmin) / 2;
    ycen = specYmin + (specYmax - specYmin) / 2;
    specXcen = xcen;
    specYcen = ycen;
    specDx = specXmax - xcen;
    specDy = specYmax - ycen;
    specXcenw2 = specXcen;
    specYcenw2 = specYcen;
    specDxw2 = specDx;
    specDyw2 = specDy;
  }
  rebinRebin = FALSE;
  spec(specX, specY, specNpix[0], specXcen - specDx, specXcen + specDx,
       specYcen - specDy, specYcen + specDy, PLOTMODE);
  if (OverPlotMode)
    plot_over();
  load_points();
  draw_zoom();
  /* if((strcmp(specLastName,name)== 0) && is_auto_fit() && fitMode ==
     POLYFITMODE) */
  if (is_auto_fit() && fitMode == POLYFITMODE)
  {
    auto_fit(0);
    plot_fit(specXaux, specYaux, gaussNumOfFitData - 1, fitDegree + 1, 6);
  } else if (is_auto_fit() && fitMode == SPLINEFITMODE)
  {
    plot_spline(gaussNumOfFitData, 6);
  }
  strcpy(specLastName, name);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 load_image(name, xv, yv, line, line_step, specnum, newlimits)
 Description : load an image in xv and yv.
   name      : file name (.bdf)
   xv,yv     : x and y values (output)
   line      : first line to be read.
   line_step : number of lines to be read. 
   specnum   : 0 = new spectrum; 1-15 = overplot spectum.
   newlimits : TRUE or FALSE ; change the current frame limits.
--------------------------------------------------------------------------*/
void load_image(name, xv, yv, line, line_step, specnum, newlimits)
  char           *name;
  float           xv[], yv[];
  int             line, line_step, specnum, newlimits;

{
  float           aux[MAXVALUES], start, step;
  int             i, j, naxis, numdata, npix[2];

  SCFOPN(name, D_R4_FORMAT, 0, F_IMA_TYPE, &specImno);
  SCDRDI(specImno, "NAXIS", 1, 1, &numdata, &naxis, &unit, &specNilval);
  SCDRDI(specImno, "NPIX", 1, 2, &numdata, npix, &unit, &specNilval);
  if (naxis == 1)
    npix[1] = 1;
  SCDGETC(specImno, "IDENT", 1, 32, &numdata, specFrameIdent);
  SCDRDD(specImno, "START", 1, 1, &numdata, &DspecStart,
         &unit, &specNilval);
  start = (float) DspecStart;
  SCDRDD(specImno, "STEP", 1, 1, &numdata, &DspecStep,
         &unit, &specNilval);
  step = (float) DspecStep;
  SCDRDR(specImno, "LHCUTS", 1, 2, &numdata, specCuts,
         &unit, &specNilval);
  if (npix[0] >= MAXVALUES)
    npix[0] = MAXVALUES - 1;
  for (i = 0; i < npix[0]; i++)
    xv[i] = start + step * i;
  SCFGET(specImno, (line - 1) * npix[0] + 1, npix[0],
         &specNilval, (char *)yv);
  for (i = 0; i < npix[0]; i++)
    yv[i] /= line_step;
  for (i = 1; i < line_step; i++)
  {
    SCFGET(specImno, (line + i - 1) * npix[0] + 1, npix[0],
           &specNilval, (char *)aux);
    for (j = 0; j < npix[0]; j++)
      yv[j] += aux[j] / line_step;
  }
  SCFCLO(specImno);
  specNpix[specnum] = npix[0];
  if (newlimits)
  {
    specXmin = xv[0];
    specXmax = xv[npix[0] - 1];
    specYmin = specYmax = yv[0];
    specDim = npix[1];
    specNumData = numdata;
    specNaxis = naxis;
    specStart = start;
    specStep = step;
    for (i = 0; i < npix[0]; i++)
    {
      if (yv[i] > specYmax)
        specYmax = yv[i];
      if (yv[i] < specYmin)
        specYmin = yv[i];
    }
  }
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 save_file(name) : save the current spectrum in a new file.
           name  : new name.
--------------------------------------------------------------------------*/
void save_file(name)
  char           *name;
{
  int             varaux[1];
  double          auxvalue[2];

  SCFCRE(name, D_R4_FORMAT, F_O_MODE, F_IMA_TYPE, specNpix[0], &specImno);
  varaux[0] = 1;
  SCDWRI(specImno, "NAXIS", varaux, 1, 1, &unit);
  SCDWRI(specImno, "NPIX", specNpix, 1, 2, &unit);
  DspecX = (double) specX[0];
  SCDWRD(specImno, "START", &DspecX, 1, 1, &unit);
  auxvalue[0] = (specX[specNpix[0] - 1] - specX[0]) / (specNpix[0] - 1);
  SCDWRD(specImno, "STEP", auxvalue, 1, 1, &unit);
  SCDWRR(specImno, "LHCUTS", specCuts, 1, 2, &unit);
  SCDWRC(specImno, "IDENT", 1, specFrameIdent, 1, 32, &unit);
  SCDWRC(specImno, "CUNIT", 1, "", 1, 1, &unit);
  SCFPUT(specImno, 1, specNpix[0], (char *)specY);
  SCFCLO(specImno);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void save_cont(name) : save the current continuum in a file.
--------------------------------------------------------------------------*/
void save_cont(name)
  char           *name;

{
  int             varaux[1], i;
  float           auxvalue[2], continuum[MAXVALUES];
  float           fit_cont();

  SCFCRE(name, D_R4_FORMAT, F_O_MODE, F_IMA_TYPE, specNpix[0], &specImno);
  varaux[0] = 1;
  SCDWRI(specImno, "NAXIS", varaux, 1, 1, &unit);
  SCDWRI(specImno, "NPIX", specNpix, 1, 2, &unit);
  DspecX = (double) specX[0];
  SCDWRD(specImno, "START", &DspecX, 1, 1, &unit);
  auxvalue[0] = specStep;
  SCDWRR(specImno, "STEP", auxvalue, 1, 1, &unit);
  SCDWRR(specImno, "LHCUTS", specCuts, 1, 2, &unit);
  SCDWRC(specImno, "IDENT", 1, specFrameIdent, 1, 32, &unit);
  SCDWRC(specImno, "CUNIT", 1, "", 1, 1, &unit);
  for (i = 0; i < specNpix[0]; i++)
    continuum[i] = fit_cont(specX[i]);
  SCFPUT(specImno, 1, specNpix[0], (char *)continuum);
  SCFCLO(specImno);
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 read_prg(filename) : read an execute a LABEL file.
--------------------------------------------------------------------------*/
void read_prg(filename)
  char           *filename;
{
  char            sset[80], outtext[100], text[80], line[160];
  int             font;
  float           xpos, ypos, scale, ang;
  FILE           *fp, *fp2;

  vdef_wspec();
  fp2 = fopen("TMPalice.prg", "a");
  if ((fp = fopen(filename, "r")) != NULL)
  {
    AG_MOPN(LABELADD);
    while (fgets(line, 160, fp) != NULL)
    {
      if (sscanf(line, "NLABEL/GRAPHIC \"%[^\"]\" %g,%g %d %g %g",
            text, &xpos, &ypos, &font, &scale, &ang) != 0)
      {
        sprintf(outtext, "~%d%s", font, text);
        sprintf(sset, "chang=%f;chdi=%f,%f", ang, scale, scale);
        AG_SSET(sset);
        AG_GTXT(xpos, ypos, outtext, 1);
        fprintf(fp2, "NLABEL/GRAPHIC \"%s\" %g,%g %d %g %g\n", text,
          xpos, ypos, font, scale, ang);
      }
    }
    AG_MCLS();
    AG_VUPD();
    AG_CLS();
    fclose(fp);
    fclose(fp2);
  }
}


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  vdef_wspec() : define the Midas window limits.
--------------------------------------------------------------------------*/
void vdef_wspec()
{
  AG_VDEF("graph_wnd0/n:", 0.0, 1.0, 0.0, 1.0, 0.0, 0.0);
  AG_CDEF(specClip[0], specClip[1], specClip[2], specClip[3]);
  AG_WDEF(specAxesXl, specAxesXu, specAxesYl, specAxesYu);
  AG_SSET("FONT=1;CHDI=1.2,1.2");
}
