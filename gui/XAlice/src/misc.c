/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* .VERSION

   090420	last modif
*/


#include <gl_defs.h>
#include <xm_defs.h>
#include <main_defs.h>
#include <spec_comm.h>
#include <UxLib.h>

#include <proto_os.h>

#define HELP_FILE	"gui/XAlice/help/alice.hlp"
#define HELP_DELIMITER	'~'

int  SCTPUT();




static char help_text[4000]; /* Max of 50 lines */

void DisplayExtendedHelp( sw )
Widget sw; 	/* current widget */
{
    char s[4000];
    char *find_extended_help();
 
    if ( sw == UxGetWidget(UxFindSwidget("GaussButton")) )
      	strcpy(s, find_extended_help("GAUSS"));
    else if ( sw == UxGetWidget(UxFindSwidget("RebinButton")) )
      	strcpy(s, find_extended_help("REBIN"));
    else if ( sw == UxGetWidget(UxFindSwidget("IntegrateButton")) )
      	strcpy(s, find_extended_help("INTEGRATE"));
    else if ( sw == UxGetWidget(UxFindSwidget("ClearGauss")) )
      	strcpy(s, find_extended_help("CLEAR"));
    else if ( sw == UxGetWidget(UxFindSwidget("ExecuteGauss")) )
      	strcpy(s, find_extended_help("EXECUTE"));
    else if ( sw == UxGetWidget(UxFindSwidget("Cut_x")) )
      	strcpy(s, find_extended_help("CUTX"));
    else if ( sw == UxGetWidget(UxFindSwidget("Cut_y")) )
      	strcpy(s, find_extended_help("CUTY"));
    else if ( sw == UxGetWidget(UxFindSwidget("Move")) )
      	strcpy(s, find_extended_help("MOVE"));
    else if ( sw == UxGetWidget(UxFindSwidget("Unzoom")) )
      	strcpy(s, find_extended_help("UNZOOM"));

    if ( s[0] != '\0' ) {
    	UxPopupInterface(UxFindSwidget("HelpTopLevel"), no_grab);
    	UxPutText(UxFindSwidget("helpText"), s);
    }
}

char *find_extended_help( key )
char *key;
{
    char midashome[512], midvers[80], help_pathname[512];
    char c, h, s[MAXLINE];
    FILE *fp;
    int idx = 0;

    osfphname("MIDVERS", midvers);
    osfphname("MIDASHOME", midashome);
    sprintf( help_pathname, "%s/%s/%s", midashome, midvers, HELP_FILE );
    if ( (fp = fopen(help_pathname, "r")) == NULL ) {
        sprintf(s, "XLong Help file [%s] not found.\r\n", help_pathname);
	SCTPUT(s);
        return("");
    }

    while ( (c = getc(fp)) != EOF )
	if ( c == HELP_DELIMITER && 
             !strncmp(key, fgets(s, MAXLINE, fp), strlen(key)) ) {
    	    while ( (h = getc(fp)) != EOF && h != HELP_DELIMITER )
		help_text[idx++] = h;
	    help_text[idx] = '\0';
            fclose(fp);
	    return(help_text);
	}
    fclose(fp);
    return("");

}
