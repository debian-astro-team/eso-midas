/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090825		last modif

===========================================================================*/


/*******************************************************************************
	OverPlotShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <alice_global.h>
#include <alice_help.h>
#include <ExternResources.h>

extern void SetFileList();


void  clear_over(), plot_over();
void  Yscaling_auto(), Yscaling_active(), Yscaling_manual();

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxOverPlotShell;
	swidget	UxOverPlotTopForm;
	swidget	UxOverPlotForm2;
	swidget	Uxseparator15;
	swidget	UxOverPlotRowInc;
	swidget	UxInctb1;
	swidget	UxInctb2;
	swidget	UxOverPlotRowCont;
	swidget	UxConttb1;
	swidget	UxConttb2;
	swidget	Uxlabel23;
	swidget	Uxseparator16;
	swidget	Uxseparator17;
	swidget	UxdyText;
	swidget	UxnText;
	swidget	Uxlabel25;
	swidget	UxRedrawTB;
	swidget	UxContAutoFitTb;
	swidget	UxScalingRow;
	swidget	UxScalingtb1;
	swidget	UxScalingtb2;
	swidget	UxScalingtb3;
	swidget	UxyminText;
	swidget	UxymaxText;
	swidget	Uxlabel24;
	swidget	Uxlabel26;
	swidget	Uxseparator18;
	swidget	UxOverPlotText;
	swidget	Uxseparator19;
	swidget	Uxseparator20;
	swidget	UxSelOverTb1;
	swidget	UxSelOverTb2;
	swidget	UxSelOverTb3;
	swidget	UxSelOverTb4;
	swidget	UxSelOverTb5;
	swidget	UxSelOverTb6;
	swidget	UxSelOverTb7;
	swidget	UxSelOverTb8;
	swidget	UxSelOverTb9;
	swidget	UxSelOverTb10;
	swidget	UxSelOverTb11;
	swidget	UxSelOverTb12;
	swidget	UxSelOverTb13;
	swidget	UxSelOverTb14;
	swidget	UxSelOverTb15;
	swidget	Uxseparator21;
	swidget	Uxseparator22;
	swidget	Uxseparator23;
	swidget	Uxseparator24;
	swidget	Uxseparator25;
} _UxCOverPlotShell;

#define OverPlotShell           UxOverPlotShellContext->UxOverPlotShell
#define OverPlotTopForm         UxOverPlotShellContext->UxOverPlotTopForm
#define OverPlotForm2           UxOverPlotShellContext->UxOverPlotForm2
#define separator15             UxOverPlotShellContext->Uxseparator15
#define OverPlotRowInc          UxOverPlotShellContext->UxOverPlotRowInc
#define Inctb1                  UxOverPlotShellContext->UxInctb1
#define Inctb2                  UxOverPlotShellContext->UxInctb2
#define OverPlotRowCont         UxOverPlotShellContext->UxOverPlotRowCont
#define Conttb1                 UxOverPlotShellContext->UxConttb1
#define Conttb2                 UxOverPlotShellContext->UxConttb2
#define label23                 UxOverPlotShellContext->Uxlabel23
#define separator16             UxOverPlotShellContext->Uxseparator16
#define separator17             UxOverPlotShellContext->Uxseparator17
#define dyText                  UxOverPlotShellContext->UxdyText
#define nText                   UxOverPlotShellContext->UxnText
#define label25                 UxOverPlotShellContext->Uxlabel25
#define RedrawTB                UxOverPlotShellContext->UxRedrawTB
#define ContAutoFitTb           UxOverPlotShellContext->UxContAutoFitTb
#define ScalingRow              UxOverPlotShellContext->UxScalingRow
#define Scalingtb1              UxOverPlotShellContext->UxScalingtb1
#define Scalingtb2              UxOverPlotShellContext->UxScalingtb2
#define Scalingtb3              UxOverPlotShellContext->UxScalingtb3
#define yminText                UxOverPlotShellContext->UxyminText
#define ymaxText                UxOverPlotShellContext->UxymaxText
#define label24                 UxOverPlotShellContext->Uxlabel24
#define label26                 UxOverPlotShellContext->Uxlabel26
#define separator18             UxOverPlotShellContext->Uxseparator18
#define OverPlotText            UxOverPlotShellContext->UxOverPlotText
#define separator19             UxOverPlotShellContext->Uxseparator19
#define separator20             UxOverPlotShellContext->Uxseparator20
#define SelOverTb1              UxOverPlotShellContext->UxSelOverTb1
#define SelOverTb2              UxOverPlotShellContext->UxSelOverTb2
#define SelOverTb3              UxOverPlotShellContext->UxSelOverTb3
#define SelOverTb4              UxOverPlotShellContext->UxSelOverTb4
#define SelOverTb5              UxOverPlotShellContext->UxSelOverTb5
#define SelOverTb6              UxOverPlotShellContext->UxSelOverTb6
#define SelOverTb7              UxOverPlotShellContext->UxSelOverTb7
#define SelOverTb8              UxOverPlotShellContext->UxSelOverTb8
#define SelOverTb9              UxOverPlotShellContext->UxSelOverTb9
#define SelOverTb10             UxOverPlotShellContext->UxSelOverTb10
#define SelOverTb11             UxOverPlotShellContext->UxSelOverTb11
#define SelOverTb12             UxOverPlotShellContext->UxSelOverTb12
#define SelOverTb13             UxOverPlotShellContext->UxSelOverTb13
#define SelOverTb14             UxOverPlotShellContext->UxSelOverTb14
#define SelOverTb15             UxOverPlotShellContext->UxSelOverTb15
#define separator21             UxOverPlotShellContext->Uxseparator21
#define separator22             UxOverPlotShellContext->Uxseparator22
#define separator23             UxOverPlotShellContext->Uxseparator23
#define separator24             UxOverPlotShellContext->Uxseparator24
#define separator25             UxOverPlotShellContext->Uxseparator25

static _UxCOverPlotShell	*UxOverPlotShellContext;

swidget	CancelOver;
swidget	ApplayOver;
swidget	AddNewOver;
swidget	ClearOver;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*TextListTab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_OverPlotShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_CancelOver( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(OverPlotShell);
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	activateCB_ApplayOver( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	extern Boolean XmToggleButtonGetState();
	ApplyOverPlot = TRUE;
	OverPlotMode = FALSE;
	plot_over();
	OverPlotMode = TRUE;
	ApplyOverPlot = FALSE;
	/*UxPopdownInterface(UxFindSwidget("OverPlotShell")); */
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	activateCB_AddNewOver( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	{
	extern swidget ListPopup;
	extern swidget scrolledList1;
	UxPopupInterface(ListPopup,no_grab);
	SetFileList( UxGetWidget(scrolledList1),1, "*.bdf" );
	caseList = OVERBDF;
	}
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	activateCB_ClearOver( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	clear_over();
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	valueChangedCB_ContAutoFitTb( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("ContAutoFitTb"))))
	{
	 XtSetSensitive(UxGetWidget(UxFindSwidget("Conttb1")),1);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("Conttb2")),1);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("Inctb1")),1);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("Inctb2")),1);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("dyText")),1);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("nText")),1);
	 UxPutSelectColor(UxFindSwidget("Conttb1"),SelectColor);
	 UxPutSelectColor(UxFindSwidget("Conttb2"),SelectColor);
	 UxPutSelectColor(UxFindSwidget("Inctb1"),SelectColor);
	 UxPutSelectColor(UxFindSwidget("Inctb2"),SelectColor);
	}
	else
	{
	 XtSetSensitive(UxGetWidget(UxFindSwidget("Conttb1")),0);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("Conttb2")),0);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("Inctb1")),0);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("Inctb2")),0);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("dyText")),0);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("nText")),0);
	 UxPutSelectColor(UxFindSwidget("Conttb1"),WindowBackground);
	 UxPutSelectColor(UxFindSwidget("Conttb2"),WindowBackground);
	 UxPutSelectColor(UxFindSwidget("Inctb1"),WindowBackground);
	 UxPutSelectColor(UxFindSwidget("Inctb2"),WindowBackground);
	}
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	armCB_Scalingtb1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	 Yscaling_active();
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	armCB_Scalingtb2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	 Yscaling_auto();
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	armCB_Scalingtb3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	 Yscaling_manual();
	
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	losingFocusCB_yminText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	UxGetText(UxFindSwidget("yminText"));
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	valueChangedCB_yminText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	XFlush(UxDisplay);
	}
	UxOverPlotShellContext = UxSaveCtx;
}

static void	losingFocusCB_ymaxText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOverPlotShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOverPlotShellContext;
	UxOverPlotShellContext = UxContext =
			(_UxCOverPlotShell *) UxGetContext( UxThisWidget );
	{
	UxGetText(UxFindSwidget("ymaxText"));
	}
	UxOverPlotShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_OverPlotShell()
{
	UxPutKeyboardFocusPolicy( OverPlotShell, "pointer" );
	UxPutIconName( OverPlotShell, "Over Plot" );
	UxPutHeight( OverPlotShell, 402 );
	UxPutWidth( OverPlotShell, 596 );
	UxPutY( OverPlotShell, 275 );
	UxPutX( OverPlotShell, 65 );

	UxPutBackground( OverPlotTopForm, WindowBackground );
	UxPutHeight( OverPlotTopForm, 121 );
	UxPutWidth( OverPlotTopForm, 129 );
	UxPutY( OverPlotTopForm, 73 );
	UxPutX( OverPlotTopForm, 76 );
	UxPutUnitType( OverPlotTopForm, "pixels" );
	UxPutResizePolicy( OverPlotTopForm, "resize_none" );

	UxPutBackground( OverPlotForm2, ButtonBackground );
	UxPutHeight( OverPlotForm2, 67 );
	UxPutWidth( OverPlotForm2, 612 );
	UxPutY( OverPlotForm2, 354 );
	UxPutX( OverPlotForm2, 0 );
	UxPutResizePolicy( OverPlotForm2, "resize_none" );

	UxPutHighlightColor( CancelOver, "#000000" );
	UxPutHighlightOnEnter( CancelOver, "true" );
	UxPutLabelString( CancelOver, "Cancel" );
	UxPutForeground( CancelOver, CancelForeground );
	UxPutFontList( CancelOver, BoldTextFont );
	UxPutBackground( CancelOver, ButtonBackground );
	UxPutHeight( CancelOver, 30 );
	UxPutWidth( CancelOver, 90 );
	UxPutY( CancelOver, 6 );
	UxPutX( CancelOver, 362 );

	UxPutHighlightColor( ApplayOver, "#000000" );
	UxPutHighlightOnEnter( ApplayOver, "true" );
	UxPutLabelString( ApplayOver, "Apply" );
	UxPutForeground( ApplayOver, ButtonForeground );
	UxPutFontList( ApplayOver, BoldTextFont );
	UxPutBackground( ApplayOver, ButtonBackground );
	UxPutHeight( ApplayOver, 30 );
	UxPutWidth( ApplayOver, 90 );
	UxPutY( ApplayOver, 6 );
	UxPutX( ApplayOver, 31 );

	UxPutHighlightColor( AddNewOver, "#000000" );
	UxPutHighlightOnEnter( AddNewOver, "true" );
	UxPutLabelString( AddNewOver, "Add new" );
	UxPutForeground( AddNewOver, ButtonForeground );
	UxPutFontList( AddNewOver, BoldTextFont );
	UxPutBackground( AddNewOver, ButtonBackground );
	UxPutHeight( AddNewOver, 30 );
	UxPutWidth( AddNewOver, 90 );
	UxPutY( AddNewOver, 6 );
	UxPutX( AddNewOver, 140 );

	UxPutHighlightColor( ClearOver, "#000000" );
	UxPutHighlightOnEnter( ClearOver, "true" );
	UxPutLabelString( ClearOver, "Clear" );
	UxPutForeground( ClearOver, ApplyForeground );
	UxPutFontList( ClearOver, BoldTextFont );
	UxPutBackground( ClearOver, ButtonBackground );
	UxPutHeight( ClearOver, 30 );
	UxPutWidth( ClearOver, 90 );
	UxPutY( ClearOver, 6 );
	UxPutX( ClearOver, 249 );

	UxPutBackground( separator15, ButtonBackground );
	UxPutHeight( separator15, 8 );
	UxPutWidth( separator15, 617 );
	UxPutY( separator15, 346 );
	UxPutX( separator15, -3 );

	UxPutBackground( OverPlotRowInc, WindowBackground );
	UxPutRadioBehavior( OverPlotRowInc, "true" );
	UxPutHeight( OverPlotRowInc, 60 );
	UxPutWidth( OverPlotRowInc, 126 );
	UxPutY( OverPlotRowInc, 141 );
	UxPutX( OverPlotRowInc, 357 );

	UxPutSensitive( Inctb1, "false" );
	UxPutSet( Inctb1, "false" );
	UxPutLabelString( Inctb1, "Increase   dy :" );
	UxPutSelectColor( Inctb1, LabelBackground );
	UxPutForeground( Inctb1, TextForeground );
	UxPutFontList( Inctb1, TextFont );
	UxPutBackground( Inctb1, WindowBackground );
	UxPutHeight( Inctb1, 28 );
	UxPutWidth( Inctb1, 117 );
	UxPutY( Inctb1, 3 );
	UxPutX( Inctb1, 3 );

	UxPutSet( Inctb2, "true" );
	UxPutSensitive( Inctb2, "false" );
	UxPutLabelString( Inctb2, "Automatic   n :" );
	UxPutSelectColor( Inctb2, LabelBackground );
	UxPutForeground( Inctb2, TextForeground );
	UxPutFontList( Inctb2, TextFont );
	UxPutBackground( Inctb2, WindowBackground );
	UxPutHeight( Inctb2, 28 );
	UxPutWidth( Inctb2, 117 );
	UxPutY( Inctb2, 34 );
	UxPutX( Inctb2, 3 );

	UxPutBackground( OverPlotRowCont, WindowBackground );
	UxPutRadioBehavior( OverPlotRowCont, "true" );
	UxPutHeight( OverPlotRowCont, 57 );
	UxPutWidth( OverPlotRowCont, 96 );
	UxPutY( OverPlotRowCont, 72 );
	UxPutX( OverPlotRowCont, 357 );

	UxPutSet( Conttb1, "true" );
	UxPutSensitive( Conttb1, "false" );
	UxPutLabelString( Conttb1, "Substract" );
	UxPutSelectColor( Conttb1, LabelBackground );
	UxPutForeground( Conttb1, TextForeground );
	UxPutFontList( Conttb1, TextFont );
	UxPutBackground( Conttb1, WindowBackground );
	UxPutHeight( Conttb1, 28 );
	UxPutWidth( Conttb1, 115 );
	UxPutY( Conttb1, 18 );
	UxPutX( Conttb1, 26 );

	UxPutSensitive( Conttb2, "false" );
	UxPutLabelString( Conttb2, "Divide" );
	UxPutSelectColor( Conttb2, LabelBackground );
	UxPutForeground( Conttb2, TextForeground );
	UxPutFontList( Conttb2, TextFont );
	UxPutBackground( Conttb2, WindowBackground );
	UxPutHeight( Conttb2, 28 );
	UxPutWidth( Conttb2, 115 );
	UxPutY( Conttb2, 49 );
	UxPutX( Conttb2, 26 );

	UxPutForeground( label23, TextForeground );
	UxPutFontList( label23, TextFont );
	UxPutLabelString( label23, "Y Scaling" );
	UxPutBackground( label23, LabelBackground );
	UxPutHeight( label23, 22 );
	UxPutWidth( label23, 85 );
	UxPutY( label23, 207 );
	UxPutX( label23, 426 );

	UxPutBackground( separator16, WindowBackground );
	UxPutHeight( separator16, 13 );
	UxPutWidth( separator16, 130 );
	UxPutY( separator16, 132 );
	UxPutX( separator16, 345 );

	UxPutBackground( separator17, WindowBackground );
	UxPutHeight( separator17, 13 );
	UxPutWidth( separator17, 63 );
	UxPutY( separator17, 213 );
	UxPutX( separator17, 514 );

	UxPutForeground( dyText, TextForeground );
	UxPutMarginWidth( dyText, 4 );
	UxPutMarginHeight( dyText, 1 );
	UxPutTranslations( dyText, TextListTab );
	UxPutFontList( dyText, TextFont );
	UxPutBackground( dyText, TextBackground );
	UxPutHeight( dyText, 28 );
	UxPutWidth( dyText, 59 );
	UxPutY( dyText, 142 );
	UxPutX( dyText, 485 );

	UxPutForeground( nText, TextForeground );
	UxPutMarginWidth( nText, 4 );
	UxPutMarginHeight( nText, 1 );
	UxPutTranslations( nText, TextListTab );
	UxPutFontList( nText, TextFont );
	UxPutBackground( nText, TextBackground );
	UxPutHeight( nText, 28 );
	UxPutWidth( nText, 59 );
	UxPutY( nText, 174 );
	UxPutX( nText, 485 );

	UxPutForeground( label25, TextForeground );
	UxPutAlignment( label25, "alignment_end" );
	UxPutFontList( label25, TextFont );
	UxPutLabelString( label25, "File                      Line     n" );
	UxPutBackground( label25, LabelBackground );
	UxPutHeight( label25, 22 );
	UxPutWidth( label25, 202 );
	UxPutY( label25, 26 );
	UxPutX( label25, 102 );

	UxPutSet( RedrawTB, "true" );
	UxPutForeground( RedrawTB, TextForeground );
	UxPutLabelString( RedrawTB, "Redraw" );
	UxPutSelectColor( RedrawTB, SelectColor );
	UxPutFontList( RedrawTB, TextFont );
	UxPutBackground( RedrawTB, WindowBackground );
	UxPutHeight( RedrawTB, 27 );
	UxPutWidth( RedrawTB, 88 );
	UxPutY( RedrawTB, 18 );
	UxPutX( RedrawTB, 388 );

	UxPutSet( ContAutoFitTb, "false" );
	UxPutForeground( ContAutoFitTb, TextForeground );
	UxPutLabelString( ContAutoFitTb, "Continuum auto-fit" );
	UxPutSelectColor( ContAutoFitTb, SelectColor );
	UxPutFontList( ContAutoFitTb, TextFont );
	UxPutBackground( ContAutoFitTb, WindowBackground );
	UxPutHeight( ContAutoFitTb, 22 );
	UxPutWidth( ContAutoFitTb, 152 );
	UxPutY( ContAutoFitTb, 49 );
	UxPutX( ContAutoFitTb, 386 );

	UxPutBackground( ScalingRow, WindowBackground );
	UxPutRadioBehavior( ScalingRow, "true" );
	UxPutHeight( ScalingRow, 112 );
	UxPutWidth( ScalingRow, 115 );
	UxPutY( ScalingRow, 230 );
	UxPutX( ScalingRow, 352 );

	UxPutSet( Scalingtb1, "false" );
	UxPutSensitive( Scalingtb1, "true" );
	UxPutLabelString( Scalingtb1, "Active" );
	UxPutSelectColor( Scalingtb1, SelectColor );
	UxPutForeground( Scalingtb1, TextForeground );
	UxPutFontList( Scalingtb1, TextFont );
	UxPutBackground( Scalingtb1, WindowBackground );
	UxPutHeight( Scalingtb1, 28 );
	UxPutWidth( Scalingtb1, 115 );
	UxPutY( Scalingtb1, 18 );
	UxPutX( Scalingtb1, 26 );

	UxPutSet( Scalingtb2, "true" );
	UxPutSensitive( Scalingtb2, "true" );
	UxPutLabelString( Scalingtb2, "Automatic" );
	UxPutSelectColor( Scalingtb2, SelectColor );
	UxPutForeground( Scalingtb2, TextForeground );
	UxPutFontList( Scalingtb2, TextFont );
	UxPutBackground( Scalingtb2, WindowBackground );
	UxPutHeight( Scalingtb2, 28 );
	UxPutWidth( Scalingtb2, 115 );
	UxPutY( Scalingtb2, 49 );
	UxPutX( Scalingtb2, 26 );

	UxPutSensitive( Scalingtb3, "true" );
	UxPutLabelString( Scalingtb3, "Manual" );
	UxPutSelectColor( Scalingtb3, SelectColor );
	UxPutForeground( Scalingtb3, TextForeground );
	UxPutFontList( Scalingtb3, TextFont );
	UxPutBackground( Scalingtb3, WindowBackground );
	UxPutHeight( Scalingtb3, 28 );
	UxPutWidth( Scalingtb3, 115 );
	UxPutY( Scalingtb3, 42 );
	UxPutX( Scalingtb3, 11 );

	UxPutForeground( yminText, TextForeground );
	UxPutMarginWidth( yminText, 4 );
	UxPutMarginHeight( yminText, 1 );
	UxPutTranslations( yminText, TextListTab );
	UxPutFontList( yminText, TextFont );
	UxPutBackground( yminText, TextBackground );
	UxPutHeight( yminText, 28 );
	UxPutWidth( yminText, 59 );
	UxPutY( yminText, 243 );
	UxPutX( yminText, 512 );

	UxPutForeground( ymaxText, TextForeground );
	UxPutMarginWidth( ymaxText, 4 );
	UxPutMarginHeight( ymaxText, 1 );
	UxPutTranslations( ymaxText, TextListTab );
	UxPutFontList( ymaxText, TextFont );
	UxPutBackground( ymaxText, TextBackground );
	UxPutHeight( ymaxText, 28 );
	UxPutWidth( ymaxText, 59 );
	UxPutY( ymaxText, 274 );
	UxPutX( ymaxText, 513 );

	UxPutForeground( label24, TextForeground );
	UxPutAlignment( label24, "alignment_end" );
	UxPutFontList( label24, TextFont );
	UxPutLabelString( label24, "y min :" );
	UxPutBackground( label24, LabelBackground );
	UxPutHeight( label24, 22 );
	UxPutWidth( label24, 48 );
	UxPutY( label24, 245 );
	UxPutX( label24, 459 );

	UxPutForeground( label26, TextForeground );
	UxPutAlignment( label26, "alignment_end" );
	UxPutFontList( label26, TextFont );
	UxPutLabelString( label26, "y max :" );
	UxPutBackground( label26, LabelBackground );
	UxPutHeight( label26, 22 );
	UxPutWidth( label26, 48 );
	UxPutY( label26, 277 );
	UxPutX( label26, 459 );

	UxPutOrientation( separator18, "vertical" );
	UxPutBackground( separator18, WindowBackground );
	UxPutHeight( separator18, 90 );
	UxPutWidth( separator18, 10 );
	UxPutY( separator18, 233 );
	UxPutX( separator18, 450 );

	UxPutMaxLength( OverPlotText, 1200 );
	UxPutColumns( OverPlotText, 30 );
	UxPutRows( OverPlotText, 15 );
	UxPutForeground( OverPlotText, TextForeground );
	UxPutFontList( OverPlotText, "9x15" );
	UxPutEditMode( OverPlotText, "multi_line_edit" );
	UxPutTranslations( OverPlotText, "" );
	UxPutBackground( OverPlotText, TextBackground );
	UxPutHeight( OverPlotText, 248 );
	UxPutWidth( OverPlotText, 299 );
	UxPutY( OverPlotText, 56 );
	UxPutX( OverPlotText, 30 );

	UxPutBackground( separator19, WindowBackground );
	UxPutHeight( separator19, 13 );
	UxPutWidth( separator19, 40 );
	UxPutY( separator19, 53 );
	UxPutX( separator19, 345 );

	UxPutBackground( separator20, WindowBackground );
	UxPutHeight( separator20, 13 );
	UxPutWidth( separator20, 40 );
	UxPutY( separator20, 53 );
	UxPutX( separator20, 538 );

	UxPutMappedWhenManaged( SelOverTb1, "false" );
	UxPutSet( SelOverTb1, "true" );
	UxPutMarginLeft( SelOverTb1, 2 );
	UxPutIndicatorSize( SelOverTb1, 12 );
	UxPutForeground( SelOverTb1, TextForeground );
	UxPutLabelString( SelOverTb1, "" );
	UxPutSelectColor( SelOverTb1, SelectColor );
	UxPutFontList( SelOverTb1, TextFont );
	UxPutBackground( SelOverTb1, WindowBackground );
	UxPutHeight( SelOverTb1, 14 );
	UxPutWidth( SelOverTb1, 20 );
	UxPutY( SelOverTb1, 67 );
	UxPutX( SelOverTb1, 8 );

	UxPutMappedWhenManaged( SelOverTb2, "false" );
	UxPutSet( SelOverTb2, "true" );
	UxPutMarginLeft( SelOverTb2, 2 );
	UxPutIndicatorSize( SelOverTb2, 12 );
	UxPutForeground( SelOverTb2, TextForeground );
	UxPutLabelString( SelOverTb2, "" );
	UxPutSelectColor( SelOverTb2, SelectColor );
	UxPutFontList( SelOverTb2, TextFont );
	UxPutBackground( SelOverTb2, WindowBackground );
	UxPutHeight( SelOverTb2, 14 );
	UxPutWidth( SelOverTb2, 20 );
	UxPutY( SelOverTb2, 82 );
	UxPutX( SelOverTb2, 8 );

	UxPutMappedWhenManaged( SelOverTb3, "false" );
	UxPutSet( SelOverTb3, "true" );
	UxPutMarginLeft( SelOverTb3, 2 );
	UxPutIndicatorSize( SelOverTb3, 12 );
	UxPutForeground( SelOverTb3, TextForeground );
	UxPutLabelString( SelOverTb3, "" );
	UxPutSelectColor( SelOverTb3, SelectColor );
	UxPutFontList( SelOverTb3, TextFont );
	UxPutBackground( SelOverTb3, WindowBackground );
	UxPutHeight( SelOverTb3, 14 );
	UxPutWidth( SelOverTb3, 20 );
	UxPutY( SelOverTb3, 97 );
	UxPutX( SelOverTb3, 8 );

	UxPutMappedWhenManaged( SelOverTb4, "false" );
	UxPutSet( SelOverTb4, "true" );
	UxPutMarginLeft( SelOverTb4, 2 );
	UxPutIndicatorSize( SelOverTb4, 12 );
	UxPutForeground( SelOverTb4, TextForeground );
	UxPutLabelString( SelOverTb4, "" );
	UxPutSelectColor( SelOverTb4, SelectColor );
	UxPutFontList( SelOverTb4, TextFont );
	UxPutBackground( SelOverTb4, WindowBackground );
	UxPutHeight( SelOverTb4, 14 );
	UxPutWidth( SelOverTb4, 20 );
	UxPutY( SelOverTb4, 112 );
	UxPutX( SelOverTb4, 8 );

	UxPutMappedWhenManaged( SelOverTb5, "false" );
	UxPutSet( SelOverTb5, "true" );
	UxPutMarginLeft( SelOverTb5, 2 );
	UxPutIndicatorSize( SelOverTb5, 12 );
	UxPutForeground( SelOverTb5, TextForeground );
	UxPutLabelString( SelOverTb5, "" );
	UxPutSelectColor( SelOverTb5, SelectColor );
	UxPutFontList( SelOverTb5, TextFont );
	UxPutBackground( SelOverTb5, WindowBackground );
	UxPutHeight( SelOverTb5, 14 );
	UxPutWidth( SelOverTb5, 20 );
	UxPutY( SelOverTb5, 127 );
	UxPutX( SelOverTb5, 8 );

	UxPutMappedWhenManaged( SelOverTb6, "false" );
	UxPutSet( SelOverTb6, "true" );
	UxPutMarginLeft( SelOverTb6, 2 );
	UxPutIndicatorSize( SelOverTb6, 12 );
	UxPutForeground( SelOverTb6, TextForeground );
	UxPutLabelString( SelOverTb6, "" );
	UxPutSelectColor( SelOverTb6, SelectColor );
	UxPutFontList( SelOverTb6, TextFont );
	UxPutBackground( SelOverTb6, WindowBackground );
	UxPutHeight( SelOverTb6, 14 );
	UxPutWidth( SelOverTb6, 20 );
	UxPutY( SelOverTb6, 142 );
	UxPutX( SelOverTb6, 8 );

	UxPutMappedWhenManaged( SelOverTb7, "false" );
	UxPutSet( SelOverTb7, "true" );
	UxPutMarginLeft( SelOverTb7, 2 );
	UxPutIndicatorSize( SelOverTb7, 12 );
	UxPutForeground( SelOverTb7, TextForeground );
	UxPutLabelString( SelOverTb7, "" );
	UxPutSelectColor( SelOverTb7, SelectColor );
	UxPutFontList( SelOverTb7, TextFont );
	UxPutBackground( SelOverTb7, WindowBackground );
	UxPutHeight( SelOverTb7, 14 );
	UxPutWidth( SelOverTb7, 20 );
	UxPutY( SelOverTb7, 157 );
	UxPutX( SelOverTb7, 8 );

	UxPutMappedWhenManaged( SelOverTb8, "false" );
	UxPutSet( SelOverTb8, "true" );
	UxPutMarginLeft( SelOverTb8, 2 );
	UxPutIndicatorSize( SelOverTb8, 12 );
	UxPutForeground( SelOverTb8, TextForeground );
	UxPutLabelString( SelOverTb8, "" );
	UxPutSelectColor( SelOverTb8, SelectColor );
	UxPutFontList( SelOverTb8, TextFont );
	UxPutBackground( SelOverTb8, WindowBackground );
	UxPutHeight( SelOverTb8, 14 );
	UxPutWidth( SelOverTb8, 20 );
	UxPutY( SelOverTb8, 172 );
	UxPutX( SelOverTb8, 8 );

	UxPutMappedWhenManaged( SelOverTb9, "false" );
	UxPutSet( SelOverTb9, "true" );
	UxPutMarginLeft( SelOverTb9, 2 );
	UxPutIndicatorSize( SelOverTb9, 12 );
	UxPutForeground( SelOverTb9, TextForeground );
	UxPutLabelString( SelOverTb9, "" );
	UxPutSelectColor( SelOverTb9, SelectColor );
	UxPutFontList( SelOverTb9, TextFont );
	UxPutBackground( SelOverTb9, WindowBackground );
	UxPutHeight( SelOverTb9, 14 );
	UxPutWidth( SelOverTb9, 20 );
	UxPutY( SelOverTb9, 187 );
	UxPutX( SelOverTb9, 8 );

	UxPutMappedWhenManaged( SelOverTb10, "false" );
	UxPutSet( SelOverTb10, "true" );
	UxPutMarginLeft( SelOverTb10, 2 );
	UxPutIndicatorSize( SelOverTb10, 12 );
	UxPutForeground( SelOverTb10, TextForeground );
	UxPutLabelString( SelOverTb10, "" );
	UxPutSelectColor( SelOverTb10, SelectColor );
	UxPutFontList( SelOverTb10, TextFont );
	UxPutBackground( SelOverTb10, WindowBackground );
	UxPutHeight( SelOverTb10, 14 );
	UxPutWidth( SelOverTb10, 20 );
	UxPutY( SelOverTb10, 202 );
	UxPutX( SelOverTb10, 8 );

	UxPutMappedWhenManaged( SelOverTb11, "false" );
	UxPutSet( SelOverTb11, "true" );
	UxPutMarginLeft( SelOverTb11, 2 );
	UxPutIndicatorSize( SelOverTb11, 12 );
	UxPutForeground( SelOverTb11, TextForeground );
	UxPutLabelString( SelOverTb11, "" );
	UxPutSelectColor( SelOverTb11, SelectColor );
	UxPutFontList( SelOverTb11, TextFont );
	UxPutBackground( SelOverTb11, WindowBackground );
	UxPutHeight( SelOverTb11, 14 );
	UxPutWidth( SelOverTb11, 20 );
	UxPutY( SelOverTb11, 217 );
	UxPutX( SelOverTb11, 8 );

	UxPutMappedWhenManaged( SelOverTb12, "false" );
	UxPutSet( SelOverTb12, "true" );
	UxPutMarginLeft( SelOverTb12, 2 );
	UxPutIndicatorSize( SelOverTb12, 12 );
	UxPutForeground( SelOverTb12, TextForeground );
	UxPutLabelString( SelOverTb12, "" );
	UxPutSelectColor( SelOverTb12, SelectColor );
	UxPutFontList( SelOverTb12, TextFont );
	UxPutBackground( SelOverTb12, WindowBackground );
	UxPutHeight( SelOverTb12, 14 );
	UxPutWidth( SelOverTb12, 20 );
	UxPutY( SelOverTb12, 232 );
	UxPutX( SelOverTb12, 8 );

	UxPutMappedWhenManaged( SelOverTb13, "false" );
	UxPutSet( SelOverTb13, "true" );
	UxPutMarginLeft( SelOverTb13, 2 );
	UxPutIndicatorSize( SelOverTb13, 12 );
	UxPutForeground( SelOverTb13, TextForeground );
	UxPutLabelString( SelOverTb13, "" );
	UxPutSelectColor( SelOverTb13, SelectColor );
	UxPutFontList( SelOverTb13, TextFont );
	UxPutBackground( SelOverTb13, WindowBackground );
	UxPutHeight( SelOverTb13, 14 );
	UxPutWidth( SelOverTb13, 20 );
	UxPutY( SelOverTb13, 247 );
	UxPutX( SelOverTb13, 8 );

	UxPutMappedWhenManaged( SelOverTb14, "false" );
	UxPutSet( SelOverTb14, "true" );
	UxPutMarginLeft( SelOverTb14, 2 );
	UxPutIndicatorSize( SelOverTb14, 12 );
	UxPutForeground( SelOverTb14, TextForeground );
	UxPutLabelString( SelOverTb14, "" );
	UxPutSelectColor( SelOverTb14, SelectColor );
	UxPutFontList( SelOverTb14, TextFont );
	UxPutBackground( SelOverTb14, WindowBackground );
	UxPutHeight( SelOverTb14, 14 );
	UxPutWidth( SelOverTb14, 20 );
	UxPutY( SelOverTb14, 262 );
	UxPutX( SelOverTb14, 8 );

	UxPutMappedWhenManaged( SelOverTb15, "false" );
	UxPutSet( SelOverTb15, "true" );
	UxPutMarginLeft( SelOverTb15, 2 );
	UxPutIndicatorSize( SelOverTb15, 12 );
	UxPutForeground( SelOverTb15, TextForeground );
	UxPutLabelString( SelOverTb15, "" );
	UxPutSelectColor( SelOverTb15, SelectColor );
	UxPutFontList( SelOverTb15, TextFont );
	UxPutBackground( SelOverTb15, WindowBackground );
	UxPutHeight( SelOverTb15, 14 );
	UxPutWidth( SelOverTb15, 20 );
	UxPutY( SelOverTb15, 277 );
	UxPutX( SelOverTb15, 8 );

	UxPutBackground( separator21, WindowBackground );
	UxPutHeight( separator21, 13 );
	UxPutWidth( separator21, 82 );
	UxPutY( separator21, 213 );
	UxPutX( separator21, 345 );

	UxPutOrientation( separator22, "vertical" );
	UxPutBackground( separator22, WindowBackground );
	UxPutHeight( separator22, 322 );
	UxPutWidth( separator22, 10 );
	UxPutY( separator22, 12 );
	UxPutX( separator22, 338 );

	UxPutOrientation( separator23, "vertical" );
	UxPutBackground( separator23, WindowBackground );
	UxPutHeight( separator23, 323 );
	UxPutWidth( separator23, 10 );
	UxPutY( separator23, 11 );
	UxPutX( separator23, 574 );

	UxPutBackground( separator24, WindowBackground );
	UxPutHeight( separator24, 13 );
	UxPutWidth( separator24, 231 );
	UxPutY( separator24, 328 );
	UxPutX( separator24, 345 );

	UxPutBackground( separator25, WindowBackground );
	UxPutHeight( separator25, 13 );
	UxPutWidth( separator25, 231 );
	UxPutY( separator25, 7 );
	UxPutX( separator25, 345 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_OverPlotShell()
{
	/* Create the swidgets */

	OverPlotShell = UxCreateTopLevelShell( "OverPlotShell", NO_PARENT );
	UxPutContext( OverPlotShell, UxOverPlotShellContext );

	OverPlotTopForm = UxCreateForm( "OverPlotTopForm", OverPlotShell );
	OverPlotForm2 = UxCreateForm( "OverPlotForm2", OverPlotTopForm );
	CancelOver = UxCreatePushButton( "CancelOver", OverPlotForm2 );
	ApplayOver = UxCreatePushButton( "ApplayOver", OverPlotForm2 );
	AddNewOver = UxCreatePushButton( "AddNewOver", OverPlotForm2 );
	ClearOver = UxCreatePushButton( "ClearOver", OverPlotForm2 );
	separator15 = UxCreateSeparator( "separator15", OverPlotTopForm );
	OverPlotRowInc = UxCreateRowColumn( "OverPlotRowInc", OverPlotTopForm );
	Inctb1 = UxCreateToggleButton( "Inctb1", OverPlotRowInc );
	Inctb2 = UxCreateToggleButton( "Inctb2", OverPlotRowInc );
	OverPlotRowCont = UxCreateRowColumn( "OverPlotRowCont", OverPlotTopForm );
	Conttb1 = UxCreateToggleButton( "Conttb1", OverPlotRowCont );
	Conttb2 = UxCreateToggleButton( "Conttb2", OverPlotRowCont );
	label23 = UxCreateLabel( "label23", OverPlotTopForm );
	separator16 = UxCreateSeparator( "separator16", OverPlotTopForm );
	separator17 = UxCreateSeparator( "separator17", OverPlotTopForm );
	dyText = UxCreateText( "dyText", OverPlotTopForm );
	nText = UxCreateText( "nText", OverPlotTopForm );
	label25 = UxCreateLabel( "label25", OverPlotTopForm );
	RedrawTB = UxCreateToggleButton( "RedrawTB", OverPlotTopForm );
	ContAutoFitTb = UxCreateToggleButton( "ContAutoFitTb", OverPlotTopForm );
	ScalingRow = UxCreateRowColumn( "ScalingRow", OverPlotTopForm );
	Scalingtb1 = UxCreateToggleButton( "Scalingtb1", ScalingRow );
	Scalingtb2 = UxCreateToggleButton( "Scalingtb2", ScalingRow );
	Scalingtb3 = UxCreateToggleButton( "Scalingtb3", ScalingRow );
	yminText = UxCreateText( "yminText", OverPlotTopForm );
	ymaxText = UxCreateText( "ymaxText", OverPlotTopForm );
	label24 = UxCreateLabel( "label24", OverPlotTopForm );
	label26 = UxCreateLabel( "label26", OverPlotTopForm );
	separator18 = UxCreateSeparator( "separator18", OverPlotTopForm );
	OverPlotText = UxCreateText( "OverPlotText", OverPlotTopForm );
	separator19 = UxCreateSeparator( "separator19", OverPlotTopForm );
	separator20 = UxCreateSeparator( "separator20", OverPlotTopForm );
	SelOverTb1 = UxCreateToggleButton( "SelOverTb1", OverPlotTopForm );
	SelOverTb2 = UxCreateToggleButton( "SelOverTb2", OverPlotTopForm );
	SelOverTb3 = UxCreateToggleButton( "SelOverTb3", OverPlotTopForm );
	SelOverTb4 = UxCreateToggleButton( "SelOverTb4", OverPlotTopForm );
	SelOverTb5 = UxCreateToggleButton( "SelOverTb5", OverPlotTopForm );
	SelOverTb6 = UxCreateToggleButton( "SelOverTb6", OverPlotTopForm );
	SelOverTb7 = UxCreateToggleButton( "SelOverTb7", OverPlotTopForm );
	SelOverTb8 = UxCreateToggleButton( "SelOverTb8", OverPlotTopForm );
	SelOverTb9 = UxCreateToggleButton( "SelOverTb9", OverPlotTopForm );
	SelOverTb10 = UxCreateToggleButton( "SelOverTb10", OverPlotTopForm );
	SelOverTb11 = UxCreateToggleButton( "SelOverTb11", OverPlotTopForm );
	SelOverTb12 = UxCreateToggleButton( "SelOverTb12", OverPlotTopForm );
	SelOverTb13 = UxCreateToggleButton( "SelOverTb13", OverPlotTopForm );
	SelOverTb14 = UxCreateToggleButton( "SelOverTb14", OverPlotTopForm );
	SelOverTb15 = UxCreateToggleButton( "SelOverTb15", OverPlotTopForm );
	separator21 = UxCreateSeparator( "separator21", OverPlotTopForm );
	separator22 = UxCreateSeparator( "separator22", OverPlotTopForm );
	separator23 = UxCreateSeparator( "separator23", OverPlotTopForm );
	separator24 = UxCreateSeparator( "separator24", OverPlotTopForm );
	separator25 = UxCreateSeparator( "separator25", OverPlotTopForm );

	_Uxinit_OverPlotShell();

	/* Create the X widgets */

	UxCreateWidget( OverPlotShell );
	UxCreateWidget( OverPlotTopForm );
	UxCreateWidget( OverPlotForm2 );
	UxCreateWidget( CancelOver );
	UxCreateWidget( ApplayOver );
	UxCreateWidget( AddNewOver );
	UxCreateWidget( ClearOver );
	UxCreateWidget( separator15 );
	UxCreateWidget( OverPlotRowInc );
	UxCreateWidget( Inctb1 );
	UxCreateWidget( Inctb2 );
	UxCreateWidget( OverPlotRowCont );
	UxCreateWidget( Conttb1 );
	UxCreateWidget( Conttb2 );
	UxCreateWidget( label23 );
	UxCreateWidget( separator16 );
	UxCreateWidget( separator17 );
	UxCreateWidget( dyText );
	UxCreateWidget( nText );
	UxCreateWidget( label25 );
	UxCreateWidget( RedrawTB );
	UxCreateWidget( ContAutoFitTb );
	UxCreateWidget( ScalingRow );
	UxCreateWidget( Scalingtb1 );
	UxCreateWidget( Scalingtb2 );
	UxCreateWidget( Scalingtb3 );
	UxCreateWidget( yminText );
	UxCreateWidget( ymaxText );
	UxCreateWidget( label24 );
	UxCreateWidget( label26 );
	UxCreateWidget( separator18 );
	UxCreateWidget( OverPlotText );
	UxCreateWidget( separator19 );
	UxCreateWidget( separator20 );
	UxCreateWidget( SelOverTb1 );
	UxCreateWidget( SelOverTb2 );
	UxCreateWidget( SelOverTb3 );
	UxCreateWidget( SelOverTb4 );
	UxCreateWidget( SelOverTb5 );
	UxCreateWidget( SelOverTb6 );
	UxCreateWidget( SelOverTb7 );
	UxCreateWidget( SelOverTb8 );
	UxCreateWidget( SelOverTb9 );
	UxCreateWidget( SelOverTb10 );
	UxCreateWidget( SelOverTb11 );
	UxCreateWidget( SelOverTb12 );
	UxCreateWidget( SelOverTb13 );
	UxCreateWidget( SelOverTb14 );
	UxCreateWidget( SelOverTb15 );
	UxCreateWidget( separator21 );
	UxCreateWidget( separator22 );
	UxCreateWidget( separator23 );
	UxCreateWidget( separator24 );
	UxCreateWidget( separator25 );

	UxAddCallback( CancelOver, XmNactivateCallback,
			activateCB_CancelOver,
			(XtPointer) UxOverPlotShellContext );

	UxAddCallback( ApplayOver, XmNactivateCallback,
			activateCB_ApplayOver,
			(XtPointer) UxOverPlotShellContext );

	UxAddCallback( AddNewOver, XmNactivateCallback,
			activateCB_AddNewOver,
			(XtPointer) UxOverPlotShellContext );

	UxAddCallback( ClearOver, XmNactivateCallback,
			activateCB_ClearOver,
			(XtPointer) UxOverPlotShellContext );

	UxAddCallback( ContAutoFitTb, XmNvalueChangedCallback,
			valueChangedCB_ContAutoFitTb,
			(XtPointer) UxOverPlotShellContext );

	UxAddCallback( Scalingtb1, XmNarmCallback,
			armCB_Scalingtb1,
			(XtPointer) UxOverPlotShellContext );

	UxAddCallback( Scalingtb2, XmNarmCallback,
			armCB_Scalingtb2,
			(XtPointer) UxOverPlotShellContext );

	UxAddCallback( Scalingtb3, XmNarmCallback,
			armCB_Scalingtb3,
			(XtPointer) UxOverPlotShellContext );

	UxAddCallback( yminText, XmNlosingFocusCallback,
			losingFocusCB_yminText,
			(XtPointer) UxOverPlotShellContext );
	UxAddCallback( yminText, XmNvalueChangedCallback,
			valueChangedCB_yminText,
			(XtPointer) UxOverPlotShellContext );

	UxAddCallback( ymaxText, XmNlosingFocusCallback,
			losingFocusCB_ymaxText,
			(XtPointer) UxOverPlotShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( OverPlotShell );

	return ( OverPlotShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_OverPlotShell()
{
	swidget                 rtrn;
	_UxCOverPlotShell       *UxContext;

	UxOverPlotShellContext = UxContext =
		(_UxCOverPlotShell *) UxMalloc( sizeof(_UxCOverPlotShell) );

	rtrn = _Uxbuild_OverPlotShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_OverPlotShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_OverPlotShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

