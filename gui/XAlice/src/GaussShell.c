/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	GaussShell.c

.VERSION

090420		last modif
*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxArrB.h"
#include "UxTogB.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxPushB.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>
#include <alice_help.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxGaussShell;
	swidget	UxGaussMenu;
	swidget	UxFramePane2;
	swidget	UxCutxGauss;
	swidget	UxCutyGauss;
	swidget	UxUnzoomGauss;
	swidget	UxRedrawGauss;
	swidget	UxPrintGauss;
	swidget	Uxmenu1_top_b1;
	swidget	UxGraphicsPane;
	swidget	UxResidualItem;
	swidget	UxInitialItem;
	swidget	UxComponentsItem;
	swidget	UxSolutionItem;
	swidget	UxContinItem;
	swidget	Uxmenu1_top_b2;
	swidget	UxOtherPane;
	swidget	UxStatItem;
	swidget	UxIntegrItem;
	swidget	UxGaussMenu_top_b1;
	swidget	UxReturnGauss2;
	swidget	UxReturnButton;
	swidget	Uxmenu1_top_b4;
	swidget	UxtoggleButton3;
	swidget	UxtoggleButton4;
	swidget	UxtoggleButton5;
	swidget	UxtoggleButton6;
	swidget	UxtoggleButton7;
	swidget	UxtoggleButton8;
	swidget	UxtoggleButton9;
	swidget	UxtoggleButton10;
	swidget	UxtoggleButton11;
	swidget	UxtoggleButton12;
	swidget	UxtoggleButton13;
	swidget	UxtoggleButton14;
	swidget	UxtoggleButton15;
	swidget	UxtoggleButton16;
	swidget	UxtoggleButton17;
	swidget	UxtoggleButton18;
	swidget	UxtoggleButton19;
	swidget	UxtoggleButton20;
	swidget	UxtoggleButton21;
	swidget	UxtoggleButton22;
	swidget	UxtoggleButton23;
	swidget	UxtoggleButton24;
	swidget	UxtoggleButton28;
	swidget	UxtoggleButton29;
	swidget	UxtoggleButton30;
	swidget	UxtoggleButton31;
	swidget	UxtoggleButton32;
	swidget	UxtoggleButton33;
	swidget	UxGaussUpComponent;
	swidget	UxGaussDownComponent;
	swidget	Uxlabel6;
	swidget	UxtoggleButton25;
	swidget	UxtoggleButton26;
	swidget	UxtoggleButton27;
	swidget	UxtoggleButton34;
	swidget	UxtoggleButton35;
	swidget	UxtoggleButton36;
	swidget	Uxhelp_text_gauss;
	swidget	UxExecuteGauss;
	swidget	UxClearGauss;
	swidget	UxCopyGauss;
	swidget	Uxseparator7;
	swidget	Uxlabel7;
	swidget	Uxlabel8;
	swidget	Uxlabel9;
} _UxCGaussShell;

#define GaussShell              UxGaussShellContext->UxGaussShell
#define GaussMenu               UxGaussShellContext->UxGaussMenu
#define FramePane2              UxGaussShellContext->UxFramePane2
#define CutxGauss               UxGaussShellContext->UxCutxGauss
#define CutyGauss               UxGaussShellContext->UxCutyGauss
#define UnzoomGauss             UxGaussShellContext->UxUnzoomGauss
#define RedrawGauss             UxGaussShellContext->UxRedrawGauss
#define PrintGauss              UxGaussShellContext->UxPrintGauss
#define menu1_top_b1            UxGaussShellContext->Uxmenu1_top_b1
#define GraphicsPane            UxGaussShellContext->UxGraphicsPane
#define ResidualItem            UxGaussShellContext->UxResidualItem
#define InitialItem             UxGaussShellContext->UxInitialItem
#define ComponentsItem          UxGaussShellContext->UxComponentsItem
#define SolutionItem            UxGaussShellContext->UxSolutionItem
#define ContinItem              UxGaussShellContext->UxContinItem
#define menu1_top_b2            UxGaussShellContext->Uxmenu1_top_b2
#define OtherPane               UxGaussShellContext->UxOtherPane
#define StatItem                UxGaussShellContext->UxStatItem
#define IntegrItem              UxGaussShellContext->UxIntegrItem
#define GaussMenu_top_b1        UxGaussShellContext->UxGaussMenu_top_b1
#define ReturnGauss2            UxGaussShellContext->UxReturnGauss2
#define ReturnButton            UxGaussShellContext->UxReturnButton
#define menu1_top_b4            UxGaussShellContext->Uxmenu1_top_b4
#define toggleButton3           UxGaussShellContext->UxtoggleButton3
#define toggleButton4           UxGaussShellContext->UxtoggleButton4
#define toggleButton5           UxGaussShellContext->UxtoggleButton5
#define toggleButton6           UxGaussShellContext->UxtoggleButton6
#define toggleButton7           UxGaussShellContext->UxtoggleButton7
#define toggleButton8           UxGaussShellContext->UxtoggleButton8
#define toggleButton9           UxGaussShellContext->UxtoggleButton9
#define toggleButton10          UxGaussShellContext->UxtoggleButton10
#define toggleButton11          UxGaussShellContext->UxtoggleButton11
#define toggleButton12          UxGaussShellContext->UxtoggleButton12
#define toggleButton13          UxGaussShellContext->UxtoggleButton13
#define toggleButton14          UxGaussShellContext->UxtoggleButton14
#define toggleButton15          UxGaussShellContext->UxtoggleButton15
#define toggleButton16          UxGaussShellContext->UxtoggleButton16
#define toggleButton17          UxGaussShellContext->UxtoggleButton17
#define toggleButton18          UxGaussShellContext->UxtoggleButton18
#define toggleButton19          UxGaussShellContext->UxtoggleButton19
#define toggleButton20          UxGaussShellContext->UxtoggleButton20
#define toggleButton21          UxGaussShellContext->UxtoggleButton21
#define toggleButton22          UxGaussShellContext->UxtoggleButton22
#define toggleButton23          UxGaussShellContext->UxtoggleButton23
#define toggleButton24          UxGaussShellContext->UxtoggleButton24
#define toggleButton28          UxGaussShellContext->UxtoggleButton28
#define toggleButton29          UxGaussShellContext->UxtoggleButton29
#define toggleButton30          UxGaussShellContext->UxtoggleButton30
#define toggleButton31          UxGaussShellContext->UxtoggleButton31
#define toggleButton32          UxGaussShellContext->UxtoggleButton32
#define toggleButton33          UxGaussShellContext->UxtoggleButton33
#define GaussUpComponent        UxGaussShellContext->UxGaussUpComponent
#define GaussDownComponent      UxGaussShellContext->UxGaussDownComponent
#define label6                  UxGaussShellContext->Uxlabel6
#define toggleButton25          UxGaussShellContext->UxtoggleButton25
#define toggleButton26          UxGaussShellContext->UxtoggleButton26
#define toggleButton27          UxGaussShellContext->UxtoggleButton27
#define toggleButton34          UxGaussShellContext->UxtoggleButton34
#define toggleButton35          UxGaussShellContext->UxtoggleButton35
#define toggleButton36          UxGaussShellContext->UxtoggleButton36
#define help_text_gauss         UxGaussShellContext->Uxhelp_text_gauss
#define ExecuteGauss            UxGaussShellContext->UxExecuteGauss
#define ClearGauss              UxGaussShellContext->UxClearGauss
#define CopyGauss               UxGaussShellContext->UxCopyGauss
#define separator7              UxGaussShellContext->Uxseparator7
#define label7                  UxGaussShellContext->Uxlabel7
#define label8                  UxGaussShellContext->Uxlabel8
#define label9                  UxGaussShellContext->Uxlabel9

static _UxCGaussShell	*UxGaussShellContext;

swidget	GaussWindow;
swidget	GaussTopform;
swidget	label1;
swidget	Components;
swidget	label2;
swidget	label3;
swidget	label4;
swidget	label5;
swidget	toggleButton1;
swidget	toggleButton2;
swidget	textField11;
swidget	textField12;
swidget	textField13;
swidget	textField14;
swidget	textField15;
swidget	textField16;
swidget	textField17;
swidget	textField18;
swidget	textField19;
swidget	textField21;
swidget	textField22;
swidget	textField23;
swidget	textField24;
swidget	textField25;
swidget	textField26;
swidget	textField27;
swidget	textField28;
swidget	textField20;
swidget	iterations_text;
swidget	iterations_text1;
swidget	iterations_text2;
swidget	GaussForm2;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*HELPGausstab = "#override\n\
<EnterWindow>:HelpGauss()\n\
<LeaveWindow>:clearHelpGauss()\n";

static char	*TextGaussTab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

static char	*HelpGaussTable = "#override\n\
<Btn3Down>:HelpHelp()\n\
<EnterWindow>:HelpGauss()\n\
<LeaveWindow>:clearHelpGauss()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_GaussShell();

void Amgauss();
void  spec(), vdef_wspec(), move_zoom(), box(), plot_over(), get_cursor();
void init_values(), load_image(), load_points(), draw_zoom(), auto_fit();
void add_fit(), zoom(), print_plot(), out_integration(), read_image();
void out_error(), save_file(), plot_fit(), plot_spline(), cursor();
void read_fit_values(), draw_error(), read_init_guess(), draw_init_guess();
void  draw_number_component(), draw_sgauss(), draw_gauss();
void  ChangeCurs(), out_fit_values(), print_statistics(), clear_values();
void  copy_fit_values(), get_fix_opt(), DisplayExtendedHelp();


/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_HelpHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxGaussShellContext = UxSaveCtx;
}

static void	action_clearHelpGauss( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{UxPutText(UxFindSwidget("help_text_gauss"),"");}
	UxGaussShellContext = UxSaveCtx;
}

static void	action_HelpGauss( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	 if( UxWidget == UxGetWidget(toggleButton1))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);
	 else if( UxWidget == UxGetWidget(toggleButton2))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);
	 else if( UxWidget == UxGetWidget(toggleButton3))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);
	 else if( UxWidget == UxGetWidget(toggleButton4))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);
	 else if( UxWidget == UxGetWidget(toggleButton5))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);
	 else if( UxWidget == UxGetWidget(toggleButton6))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);
	 else if( UxWidget == UxGetWidget(toggleButton7))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);
	 else if( UxWidget == UxGetWidget(toggleButton8))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);
	 else if( UxWidget == UxGetWidget(toggleButton9))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);
	 else if( UxWidget == UxGetWidget(GaussUpComponent))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPUPAR);
	 else if( UxWidget == UxGetWidget(GaussDownComponent))
	  UxPutText(UxFindSwidget("help_text_gauss"),COMPDNAR);
	 if( UxWidget == UxGetWidget(ClearGauss))
	  UxPutText(UxFindSwidget("help_text_gauss"),CLEAR);
	if( UxWidget == UxGetWidget(CopyGauss))
	  UxPutText(UxFindSwidget("help_text_gauss"),COPY);
	 else if( UxWidget == UxGetWidget(ExecuteGauss))
	  UxPutText(UxFindSwidget("help_text_gauss"),EXECUTE);
	 else if( UxWidget == UxGetWidget(toggleButton10))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);
	 else if( UxWidget == UxGetWidget(toggleButton13))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);
	 else if( UxWidget == UxGetWidget(toggleButton16))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);
	 else if( UxWidget == UxGetWidget(toggleButton19))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);
	 else if( UxWidget == UxGetWidget(toggleButton22))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);
	 else if( UxWidget == UxGetWidget(toggleButton25))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);
	 else if( UxWidget == UxGetWidget(toggleButton28))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);
	 else if( UxWidget == UxGetWidget(toggleButton31))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);
	 else if( UxWidget == UxGetWidget(toggleButton34))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);
	 else if( UxWidget == UxGetWidget(toggleButton11))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);
	 else if( UxWidget == UxGetWidget(toggleButton14))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);
	 else if( UxWidget == UxGetWidget(toggleButton17))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);
	 else if( UxWidget == UxGetWidget(toggleButton20))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);
	 else if( UxWidget == UxGetWidget(toggleButton23))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);
	 else if( UxWidget == UxGetWidget(toggleButton26))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);
	 else if( UxWidget == UxGetWidget(toggleButton29))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);
	 else if( UxWidget == UxGetWidget(toggleButton32))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);
	 else if( UxWidget == UxGetWidget(toggleButton35))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);
	 if ( UxWidget == UxGetWidget(toggleButton12))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);
	 else if( UxWidget == UxGetWidget(toggleButton15))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);
	 else if( UxWidget == UxGetWidget(toggleButton18))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);
	 else if( UxWidget == UxGetWidget(toggleButton21))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);
	 else if( UxWidget == UxGetWidget(toggleButton24))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);
	 else if( UxWidget == UxGetWidget(toggleButton27))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);
	 else if( UxWidget == UxGetWidget(toggleButton30))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);
	 else if( UxWidget == UxGetWidget(toggleButton33))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);
	 else if( UxWidget == UxGetWidget(toggleButton36))
	  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);
	}
	UxGaussShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_CutxGauss( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	 zoom(MOVE_X2);
	
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_CutyGauss( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	zoom(MOVE_Y2);
	
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_UnzoomGauss( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	 specXcen = specXcenw2;
	 specYcen = specYcenw2;
	 specDx = specDxw2;
	 specDy = specDyw2;
	 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	      specYcen-specDy,specYcen+specDy,PLOTMODE);
	if(OverPlotMode)
	   plot_over();
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_RedrawGauss( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	  spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	       specYcen-specDy,specYcen+specDy,PLOTMODE);
	  if(OverPlotMode)
	   plot_over();
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_PrintGauss( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("NormalToggle"))))
	   print_plot(NORMALPRINT,PrinterName);
	else
	   print_plot(PORTRAITPRINT,PrinterName);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_ResidualItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if(gaussNumOfSol >0)
	{
	read_fit_values();
	draw_error();
	}
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_InitialItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	read_init_guess();
	draw_init_guess();
	draw_number_component(3);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_ComponentsItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	int i;
	if(gaussNumOfSol>0)
	{
	 read_fit_values();
	 for(i=0;i<gaussNumOfSol;i++)
	  draw_sgauss(gaussFitValues[i*3],gaussFitValues[i*3+1],
	  gaussFitValues[i*3+2],4);
	}
	 draw_number_component(4);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_SolutionItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	/*spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,specYcen-specDy,specYcen+specDy);*/
	if(gaussNumOfSol >0)
	 {
	  read_fit_values();
	  draw_gauss();
	 }
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_ContinItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if(fitAddFit)
	 {
	 if(fitMode == POLYFITMODE)
	  plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);
	 else
	  plot_spline(gaussNumOfFitData,6);
	 }
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_StatItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	extern swidget StatShell;
	UxPopupInterface(StatShell,no_grab);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_IntegrItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	out_integration();
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_ReturnButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("GaussShell"));
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_Components( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),COMPFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_Components( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModNumOfComp = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_Components( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModNumOfComp)
	{ gaussModNumOfComp = FALSE;
	  sscanf(UxGetText(Components),"%d",&gaussNumOfComp);
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModInitGuess)
	{ gaussModInitGuess = FALSE;
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textField11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModInitGuess= TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModInitGuess)
	{ gaussModInitGuess = FALSE;
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textField12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModInitGuess = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModInitGuess)
	{ gaussModInitGuess = FALSE;
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textField13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModInitGuess = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModInitGuess)
	{ gaussModInitGuess = FALSE;
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textField14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModInitGuess= TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModInitGuess)
	{ gaussModInitGuess = FALSE;
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textField15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModInitGuess = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModInitGuess)
	{ gaussModInitGuess = FALSE;
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textField16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModInitGuess = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField17( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField17( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModInitGuess)
	{ gaussModInitGuess = FALSE;
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textField17( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModInitGuess = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModInitGuess)
	{ gaussModInitGuess = FALSE;
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textField18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModInitGuess = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField19( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField19( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModInitGuess)
	{ gaussModInitGuess = FALSE;
	  read_init_guess();
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textField19( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModInitGuess = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField21( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),""); 
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField21( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField22( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),""); 
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField22( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField23( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),""); 
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField23( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField24( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),""); 
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField24( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField25( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),""); 
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField25( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField26( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),""); 
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField26( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField27( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),""); 
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField27( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField28( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),""); 
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField28( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_textField20( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),""); 
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_textField20( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_GaussUpComponent( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	char line[20];
	if(gaussNumOfComp<9)
	 gaussNumOfComp++;
	sprintf(line,"%d",gaussNumOfComp);
	UxPutText(Components,line);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_GaussDownComponent( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	char line[20];
	if(gaussNumOfComp>1)
	gaussNumOfComp--;
	sprintf(line,"%d",gaussNumOfComp);
	UxPutText(Components,line);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_iterations_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),ITERATIONMAX );
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_iterations_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModMaxIterations = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_iterations_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModMaxIterations)
	{ gaussModMaxIterations = FALSE;
	  sscanf(UxGetText(iterations_text),"%d",&gaussMaxIterations);
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_iterations_text1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),ITERATIONMIN );
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_iterations_text1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	gaussModMinIterations = TRUE;
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_iterations_text1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	if( gaussModMinIterations)
	{ gaussModMinIterations = FALSE;
	  sscanf(UxGetText(iterations_text1),"%d",&gaussMinIterations);
	}
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	losingFocusCB_iterations_text2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_gauss"),"");
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	focusCB_iterations_text2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_ExecuteGauss( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	get_fix_opt();
	cursor();
	read_init_guess();
	if(!specAbortCursor)
	{
	 gaussNumOfSol = gaussNumOfComp;
	/* SCTPUT(".... Computing Solution"); */
	 ChangeCurs();
	 Amgauss();
	 out_fit_values();
	 draw_gauss();
	 print_statistics();
	}
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_ClearGauss( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	clear_values();
	
	}
	UxGaussShellContext = UxSaveCtx;
}

static void	activateCB_CopyGauss( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCGaussShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxGaussShellContext;
	UxGaussShellContext = UxContext =
			(_UxCGaussShell *) UxGetContext( UxThisWidget );
	{
	copy_fit_values(); 
	}
	UxGaussShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_GaussShell()
{
	UxPutAncestorSensitive( GaussShell, "true" );
	UxPutSensitive( GaussShell, "true" );
	UxPutKeyboardFocusPolicy( GaussShell, "pointer" );
	UxPutHeight( GaussShell, 455 );
	UxPutWidth( GaussShell, 588 );
	UxPutY( GaussShell, 424 );
	UxPutX( GaussShell, 466 );

	UxPutSensitive( GaussWindow, "true" );
	UxPutAncestorSensitive( GaussWindow, "true" );
	UxPutHeight( GaussWindow, 220 );
	UxPutWidth( GaussWindow, 278 );
	UxPutY( GaussWindow, 86 );
	UxPutX( GaussWindow, 122 );
	UxPutUnitType( GaussWindow, "pixels" );

	UxPutMenuAccelerator( GaussMenu, "<KeyUp>F10" );
	UxPutBackground( GaussMenu, MenuBackground );
	UxPutRowColumnType( GaussMenu, "menu_bar" );

	UxPutHighlightColor( FramePane2, "#000000" );
	UxPutForeground( FramePane2, MenuForeground );
	UxPutBackground( FramePane2, MenuBackground );
	UxPutRowColumnType( FramePane2, "menu_pulldown" );

	UxPutHighlightColor( CutxGauss, "#000000" );
	UxPutForeground( CutxGauss, MenuForeground );
	UxPutFontList( CutxGauss, BoldTextFont );
	UxPutBackground( CutxGauss, MenuBackground );
	UxPutMnemonic( CutxGauss, "X" );
	UxPutLabelString( CutxGauss, "Cut X" );

	UxPutHighlightColor( CutyGauss, "#000000" );
	UxPutForeground( CutyGauss, MenuForeground );
	UxPutFontList( CutyGauss, BoldTextFont );
	UxPutBackground( CutyGauss, MenuBackground );
	UxPutMnemonic( CutyGauss, "Y" );
	UxPutLabelString( CutyGauss, "Cut Y" );

	UxPutHighlightColor( UnzoomGauss, "#000000" );
	UxPutForeground( UnzoomGauss, MenuForeground );
	UxPutFontList( UnzoomGauss, BoldTextFont );
	UxPutBackground( UnzoomGauss, MenuBackground );
	UxPutMnemonic( UnzoomGauss, "U" );
	UxPutLabelString( UnzoomGauss, "Unzoom" );

	UxPutHighlightColor( RedrawGauss, "#000000" );
	UxPutForeground( RedrawGauss, MenuForeground );
	UxPutFontList( RedrawGauss, BoldTextFont );
	UxPutBackground( RedrawGauss, MenuBackground );
	UxPutMnemonic( RedrawGauss, "R" );
	UxPutLabelString( RedrawGauss, "Redraw" );

	UxPutHighlightColor( PrintGauss, "#000000" );
	UxPutForeground( PrintGauss, MenuForeground );
	UxPutFontList( PrintGauss, BoldTextFont );
	UxPutBackground( PrintGauss, MenuBackground );
	UxPutMnemonic( PrintGauss, "P" );
	UxPutLabelString( PrintGauss, "Print" );

	UxPutAncestorSensitive( menu1_top_b1, "true" );
	UxPutHighlightColor( menu1_top_b1, "#000000" );
	UxPutForeground( menu1_top_b1, MenuForeground );
	UxPutFontList( menu1_top_b1, BoldTextFont );
	UxPutBackground( menu1_top_b1, MenuBackground );
	UxPutMnemonic( menu1_top_b1, "F" );
	UxPutLabelString( menu1_top_b1, "Frame" );

	UxPutHighlightColor( GraphicsPane, "#000000" );
	UxPutForeground( GraphicsPane, MenuForeground );
	UxPutBackground( GraphicsPane, MenuBackground );
	UxPutRowColumnType( GraphicsPane, "menu_pulldown" );

	UxPutHighlightColor( ResidualItem, "#000000" );
	UxPutForeground( ResidualItem, MenuForeground );
	UxPutFontList( ResidualItem, BoldTextFont );
	UxPutBackground( ResidualItem, MenuBackground );
	UxPutMnemonic( ResidualItem, "R" );
	UxPutLabelString( ResidualItem, "Draw Residual" );

	UxPutHighlightColor( InitialItem, "#000000" );
	UxPutForeground( InitialItem, MenuForeground );
	UxPutFontList( InitialItem, BoldTextFont );
	UxPutBackground( InitialItem, MenuBackground );
	UxPutMnemonic( InitialItem, "i" );
	UxPutLabelString( InitialItem, "Draw initial guess" );

	UxPutHighlightColor( ComponentsItem, "#000000" );
	UxPutForeground( ComponentsItem, MenuForeground );
	UxPutFontList( ComponentsItem, BoldTextFont );
	UxPutBackground( ComponentsItem, MenuBackground );
	UxPutMnemonic( ComponentsItem, "c" );
	UxPutLabelString( ComponentsItem, "Draw fit components" );

	UxPutHighlightColor( SolutionItem, "#000000" );
	UxPutForeground( SolutionItem, MenuForeground );
	UxPutFontList( SolutionItem, BoldTextFont );
	UxPutBackground( SolutionItem, MenuBackground );
	UxPutMnemonic( SolutionItem, "s" );
	UxPutLabelString( SolutionItem, "Draw solution" );

	UxPutHighlightColor( ContinItem, "#000000" );
	UxPutForeground( ContinItem, MenuForeground );
	UxPutFontList( ContinItem, BoldTextFont );
	UxPutBackground( ContinItem, MenuBackground );
	UxPutMnemonic( ContinItem, "n" );
	UxPutLabelString( ContinItem, "Draw continuum" );

	UxPutAncestorSensitive( menu1_top_b2, "true" );
	UxPutHighlightColor( menu1_top_b2, "#000000" );
	UxPutForeground( menu1_top_b2, MenuForeground );
	UxPutFontList( menu1_top_b2, BoldTextFont );
	UxPutBackground( menu1_top_b2, MenuBackground );
	UxPutMnemonic( menu1_top_b2, "G" );
	UxPutLabelString( menu1_top_b2, "Graphics" );

	UxPutHighlightColor( OtherPane, "#000000" );
	UxPutForeground( OtherPane, MenuForeground );
	UxPutBackground( OtherPane, MenuBackground );
	UxPutRowColumnType( OtherPane, "menu_pulldown" );

	UxPutHighlightColor( StatItem, "#000000" );
	UxPutForeground( StatItem, MenuForeground );
	UxPutFontList( StatItem, BoldTextFont );
	UxPutBackground( StatItem, MenuBackground );
	UxPutMnemonic( StatItem, "s" );
	UxPutLabelString( StatItem, "Show statistics" );

	UxPutHighlightColor( IntegrItem, "#000000" );
	UxPutForeground( IntegrItem, MenuForeground );
	UxPutFontList( IntegrItem, BoldTextFont );
	UxPutBackground( IntegrItem, MenuBackground );
	UxPutMnemonic( IntegrItem, "I" );
	UxPutLabelString( IntegrItem, "Print Integration" );

	UxPutAncestorSensitive( GaussMenu_top_b1, "true" );
	UxPutHighlightColor( GaussMenu_top_b1, "#000000" );
	UxPutForeground( GaussMenu_top_b1, MenuForeground );
	UxPutFontList( GaussMenu_top_b1, BoldTextFont );
	UxPutBackground( GaussMenu_top_b1, MenuBackground );
	UxPutMnemonic( GaussMenu_top_b1, "O" );
	UxPutLabelString( GaussMenu_top_b1, "Other" );

	UxPutHighlightColor( ReturnGauss2, "#000000" );
	UxPutForeground( ReturnGauss2, MenuForeground );
	UxPutBackground( ReturnGauss2, MenuBackground );
	UxPutRowColumnType( ReturnGauss2, "menu_pulldown" );

	UxPutHighlightColor( ReturnButton, "#000000" );
	UxPutForeground( ReturnButton, MenuForeground );
	UxPutFontList( ReturnButton, BoldTextFont );
	UxPutBackground( ReturnButton, MenuBackground );
	UxPutMnemonic( ReturnButton, "R" );
	UxPutLabelString( ReturnButton, "Return" );

	UxPutAncestorSensitive( menu1_top_b4, "true" );
	UxPutHighlightColor( menu1_top_b4, "#000000" );
	UxPutForeground( menu1_top_b4, MenuForeground );
	UxPutFontList( menu1_top_b4, BoldTextFont );
	UxPutBackground( menu1_top_b4, MenuBackground );
	UxPutMnemonic( menu1_top_b4, "Q" );
	UxPutLabelString( menu1_top_b4, "Quit" );

	UxPutSensitive( GaussTopform, "true" );
	UxPutAncestorSensitive( GaussTopform, "true" );
	UxPutBackground( GaussTopform, WindowBackground );

	UxPutAncestorSensitive( label1, "true" );
	UxPutHighlightColor( label1, "Black" );
	UxPutForeground( label1, TextForeground );
	UxPutLabelString( label1, "Components :" );
	UxPutFontList( label1, TextFont );
	UxPutBackground( label1, LabelBackground );
	UxPutHeight( label1, 30 );
	UxPutWidth( label1, 110 );
	UxPutY( label1, 308 );
	UxPutX( label1, 8 );

	UxPutMarginWidth( Components, 5 );
	UxPutMarginHeight( Components, 3 );
	UxPutAncestorSensitive( Components, "true" );
	UxPutMaxLength( Components, 5 );
	UxPutHighlightColor( Components, "Black" );
	UxPutCursorPositionVisible( Components, "false" );
	UxPutForeground( Components, TextForeground );
	UxPutFontList( Components, TextFont );
	UxPutEditable( Components, "false" );
	UxPutBackground( Components, TextBackground );
	UxPutHeight( Components, 34 );
	UxPutWidth( Components, 30 );
	UxPutY( Components, 304 );
	UxPutX( Components, 118 );

	UxPutAncestorSensitive( label2, "true" );
	UxPutHighlightColor( label2, "Black" );
	UxPutForeground( label2, TextForeground );
	UxPutLabelString( label2, "Component" );
	UxPutFontList( label2, TextFont );
	UxPutBackground( label2, LabelBackground );
	UxPutHeight( label2, 20 );
	UxPutWidth( label2, 83 );
	UxPutY( label2, 2 );
	UxPutX( label2, 4 );

	UxPutAncestorSensitive( label3, "true" );
	UxPutHighlightColor( label3, "Black" );
	UxPutForeground( label3, TextForeground );
	UxPutLabelString( label3, "Fix" );
	UxPutFontList( label3, TextFont );
	UxPutBackground( label3, LabelBackground );
	UxPutHeight( label3, 20 );
	UxPutWidth( label3, 50 );
	UxPutY( label3, 2 );
	UxPutX( label3, 90 );

	UxPutAncestorSensitive( label4, "true" );
	UxPutHighlightColor( label4, "Black" );
	UxPutForeground( label4, TextForeground );
	UxPutLabelString( label4, "Initial Guess" );
	UxPutFontList( label4, TextFont );
	UxPutBackground( label4, LabelBackground );
	UxPutHeight( label4, 20 );
	UxPutWidth( label4, 210 );
	UxPutY( label4, 2 );
	UxPutX( label4, 150 );

	UxPutAncestorSensitive( label5, "true" );
	UxPutHighlightColor( label5, "Black" );
	UxPutForeground( label5, TextForeground );
	UxPutLabelString( label5, "Fit Values" );
	UxPutFontList( label5, TextFont );
	UxPutBackground( label5, LabelBackground );
	UxPutHeight( label5, 20 );
	UxPutWidth( label5, 210 );
	UxPutY( label5, 2 );
	UxPutX( label5, 360 );

	UxPutAncestorSensitive( toggleButton1, "true" );
	UxPutIndicatorSize( toggleButton1, 16 );
	UxPutHighlightColor( toggleButton1, "Black" );
	UxPutTranslations( toggleButton1, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton1, "true" );
	UxPutForeground( toggleButton1, TextForeground );
	UxPutSet( toggleButton1, "false" );
	UxPutLabelString( toggleButton1, "     " );
	UxPutSelectColor( toggleButton1, SelectColor );
	UxPutFontList( toggleButton1, BoldTextFont );
	UxPutBackground( toggleButton1, LabelBackground );
	UxPutHeight( toggleButton1, 30 );
	UxPutWidth( toggleButton1, 60 );
	UxPutY( toggleButton1, 21 );
	UxPutX( toggleButton1, 13 );

	UxPutAncestorSensitive( toggleButton2, "true" );
	UxPutIndicatorSize( toggleButton2, 16 );
	UxPutHighlightColor( toggleButton2, "Black" );
	UxPutTranslations( toggleButton2, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton2, "true" );
	UxPutForeground( toggleButton2, TextForeground );
	UxPutSet( toggleButton2, "false" );
	UxPutLabelString( toggleButton2, "     " );
	UxPutSelectColor( toggleButton2, SelectColor );
	UxPutFontList( toggleButton2, BoldTextFont );
	UxPutBackground( toggleButton2, LabelBackground );
	UxPutHeight( toggleButton2, 30 );
	UxPutWidth( toggleButton2, 60 );
	UxPutY( toggleButton2, 51 );
	UxPutX( toggleButton2, 13 );

	UxPutAncestorSensitive( toggleButton3, "true" );
	UxPutIndicatorSize( toggleButton3, 16 );
	UxPutHighlightColor( toggleButton3, "Black" );
	UxPutTranslations( toggleButton3, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton3, "true" );
	UxPutForeground( toggleButton3, TextForeground );
	UxPutSet( toggleButton3, "false" );
	UxPutLabelString( toggleButton3, "     " );
	UxPutSelectColor( toggleButton3, SelectColor );
	UxPutFontList( toggleButton3, BoldTextFont );
	UxPutBackground( toggleButton3, LabelBackground );
	UxPutHeight( toggleButton3, 30 );
	UxPutWidth( toggleButton3, 60 );
	UxPutY( toggleButton3, 81 );
	UxPutX( toggleButton3, 13 );

	UxPutAncestorSensitive( toggleButton4, "true" );
	UxPutIndicatorSize( toggleButton4, 16 );
	UxPutHighlightColor( toggleButton4, "Black" );
	UxPutTranslations( toggleButton4, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton4, "true" );
	UxPutForeground( toggleButton4, TextForeground );
	UxPutSet( toggleButton4, "false" );
	UxPutLabelString( toggleButton4, "     " );
	UxPutSelectColor( toggleButton4, SelectColor );
	UxPutFontList( toggleButton4, BoldTextFont );
	UxPutBackground( toggleButton4, LabelBackground );
	UxPutHeight( toggleButton4, 30 );
	UxPutWidth( toggleButton4, 60 );
	UxPutY( toggleButton4, 111 );
	UxPutX( toggleButton4, 13 );

	UxPutAncestorSensitive( toggleButton5, "true" );
	UxPutIndicatorSize( toggleButton5, 16 );
	UxPutHighlightColor( toggleButton5, "Black" );
	UxPutTranslations( toggleButton5, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton5, "true" );
	UxPutForeground( toggleButton5, TextForeground );
	UxPutSet( toggleButton5, "false" );
	UxPutLabelString( toggleButton5, "     " );
	UxPutSelectColor( toggleButton5, SelectColor );
	UxPutFontList( toggleButton5, BoldTextFont );
	UxPutBackground( toggleButton5, LabelBackground );
	UxPutHeight( toggleButton5, 30 );
	UxPutWidth( toggleButton5, 60 );
	UxPutY( toggleButton5, 141 );
	UxPutX( toggleButton5, 13 );

	UxPutAncestorSensitive( toggleButton6, "true" );
	UxPutIndicatorSize( toggleButton6, 16 );
	UxPutHighlightColor( toggleButton6, "Black" );
	UxPutTranslations( toggleButton6, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton6, "true" );
	UxPutForeground( toggleButton6, TextForeground );
	UxPutSet( toggleButton6, "false" );
	UxPutLabelString( toggleButton6, "     " );
	UxPutSelectColor( toggleButton6, SelectColor );
	UxPutFontList( toggleButton6, BoldTextFont );
	UxPutBackground( toggleButton6, LabelBackground );
	UxPutHeight( toggleButton6, 30 );
	UxPutWidth( toggleButton6, 60 );
	UxPutY( toggleButton6, 171 );
	UxPutX( toggleButton6, 13 );

	UxPutAncestorSensitive( toggleButton7, "true" );
	UxPutIndicatorSize( toggleButton7, 16 );
	UxPutHighlightColor( toggleButton7, "Black" );
	UxPutTranslations( toggleButton7, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton7, "true" );
	UxPutForeground( toggleButton7, TextForeground );
	UxPutSet( toggleButton7, "false" );
	UxPutLabelString( toggleButton7, "     " );
	UxPutSelectColor( toggleButton7, SelectColor );
	UxPutFontList( toggleButton7, BoldTextFont );
	UxPutBackground( toggleButton7, LabelBackground );
	UxPutHeight( toggleButton7, 30 );
	UxPutWidth( toggleButton7, 60 );
	UxPutY( toggleButton7, 201 );
	UxPutX( toggleButton7, 13 );

	UxPutAncestorSensitive( toggleButton8, "true" );
	UxPutIndicatorSize( toggleButton8, 16 );
	UxPutHighlightColor( toggleButton8, "Black" );
	UxPutTranslations( toggleButton8, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton8, "true" );
	UxPutForeground( toggleButton8, TextForeground );
	UxPutSet( toggleButton8, "false" );
	UxPutLabelString( toggleButton8, "     " );
	UxPutSelectColor( toggleButton8, SelectColor );
	UxPutFontList( toggleButton8, BoldTextFont );
	UxPutBackground( toggleButton8, LabelBackground );
	UxPutHeight( toggleButton8, 30 );
	UxPutWidth( toggleButton8, 60 );
	UxPutY( toggleButton8, 231 );
	UxPutX( toggleButton8, 13 );

	UxPutAncestorSensitive( toggleButton9, "true" );
	UxPutIndicatorSize( toggleButton9, 16 );
	UxPutHighlightColor( toggleButton9, "Black" );
	UxPutTranslations( toggleButton9, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton9, "true" );
	UxPutForeground( toggleButton9, TextForeground );
	UxPutSet( toggleButton9, "false" );
	UxPutLabelString( toggleButton9, "     " );
	UxPutSelectColor( toggleButton9, SelectColor );
	UxPutFontList( toggleButton9, BoldTextFont );
	UxPutBackground( toggleButton9, LabelBackground );
	UxPutHeight( toggleButton9, 30 );
	UxPutWidth( toggleButton9, 60 );
	UxPutY( toggleButton9, 261 );
	UxPutX( toggleButton9, 13 );

	UxPutAncestorSensitive( textField11, "true" );
	UxPutHighlightColor( textField11, "Black" );
	UxPutTranslations( textField11, TextGaussTab );
	UxPutFontList( textField11, SmallPropFont );
	UxPutHighlightOnEnter( textField11, "true" );
	UxPutMaxLength( textField11, 320 );
	UxPutForeground( textField11, TextForeground );
	UxPutBackground( textField11, TextBackground );
	UxPutHeight( textField11, 30 );
	UxPutWidth( textField11, 210 );
	UxPutY( textField11, 24 );
	UxPutX( textField11, 151 );

	UxPutAncestorSensitive( textField12, "true" );
	UxPutHighlightColor( textField12, "Black" );
	UxPutTranslations( textField12, TextGaussTab );
	UxPutFontList( textField12, SmallPropFont );
	UxPutHighlightOnEnter( textField12, "true" );
	UxPutMaxLength( textField12, 320 );
	UxPutForeground( textField12, TextForeground );
	UxPutBackground( textField12, TextBackground );
	UxPutHeight( textField12, 30 );
	UxPutWidth( textField12, 210 );
	UxPutY( textField12, 54 );
	UxPutX( textField12, 151 );

	UxPutAncestorSensitive( textField13, "true" );
	UxPutHighlightColor( textField13, "Black" );
	UxPutTranslations( textField13, TextGaussTab );
	UxPutFontList( textField13, SmallPropFont );
	UxPutHighlightOnEnter( textField13, "true" );
	UxPutMaxLength( textField13, 320 );
	UxPutForeground( textField13, TextForeground );
	UxPutBackground( textField13, TextBackground );
	UxPutHeight( textField13, 30 );
	UxPutWidth( textField13, 210 );
	UxPutY( textField13, 84 );
	UxPutX( textField13, 151 );

	UxPutAncestorSensitive( textField14, "true" );
	UxPutHighlightColor( textField14, "Black" );
	UxPutTranslations( textField14, TextGaussTab );
	UxPutFontList( textField14, SmallPropFont );
	UxPutHighlightOnEnter( textField14, "true" );
	UxPutMaxLength( textField14, 320 );
	UxPutForeground( textField14, TextForeground );
	UxPutBackground( textField14, TextBackground );
	UxPutHeight( textField14, 30 );
	UxPutWidth( textField14, 210 );
	UxPutY( textField14, 114 );
	UxPutX( textField14, 151 );

	UxPutAncestorSensitive( textField15, "true" );
	UxPutHighlightColor( textField15, "Black" );
	UxPutTranslations( textField15, TextGaussTab );
	UxPutFontList( textField15, SmallPropFont );
	UxPutHighlightOnEnter( textField15, "true" );
	UxPutMaxLength( textField15, 320 );
	UxPutForeground( textField15, TextForeground );
	UxPutBackground( textField15, TextBackground );
	UxPutHeight( textField15, 30 );
	UxPutWidth( textField15, 210 );
	UxPutY( textField15, 144 );
	UxPutX( textField15, 151 );

	UxPutAncestorSensitive( textField16, "true" );
	UxPutHighlightColor( textField16, "Black" );
	UxPutTranslations( textField16, TextGaussTab );
	UxPutFontList( textField16, SmallPropFont );
	UxPutHighlightOnEnter( textField16, "true" );
	UxPutMaxLength( textField16, 320 );
	UxPutForeground( textField16, TextForeground );
	UxPutBackground( textField16, TextBackground );
	UxPutHeight( textField16, 30 );
	UxPutWidth( textField16, 210 );
	UxPutY( textField16, 174 );
	UxPutX( textField16, 151 );

	UxPutAncestorSensitive( textField17, "true" );
	UxPutHighlightColor( textField17, "Black" );
	UxPutTranslations( textField17, TextGaussTab );
	UxPutFontList( textField17, SmallPropFont );
	UxPutHighlightOnEnter( textField17, "true" );
	UxPutMaxLength( textField17, 320 );
	UxPutForeground( textField17, TextForeground );
	UxPutBackground( textField17, TextBackground );
	UxPutHeight( textField17, 30 );
	UxPutWidth( textField17, 210 );
	UxPutY( textField17, 204 );
	UxPutX( textField17, 151 );

	UxPutAncestorSensitive( textField18, "true" );
	UxPutHighlightColor( textField18, "Black" );
	UxPutTranslations( textField18, TextGaussTab );
	UxPutFontList( textField18, SmallPropFont );
	UxPutHighlightOnEnter( textField18, "true" );
	UxPutMaxLength( textField18, 320 );
	UxPutForeground( textField18, TextForeground );
	UxPutBackground( textField18, TextBackground );
	UxPutHeight( textField18, 30 );
	UxPutWidth( textField18, 210 );
	UxPutY( textField18, 234 );
	UxPutX( textField18, 151 );

	UxPutAncestorSensitive( textField19, "true" );
	UxPutHighlightColor( textField19, "Black" );
	UxPutTranslations( textField19, TextGaussTab );
	UxPutFontList( textField19, SmallPropFont );
	UxPutHighlightOnEnter( textField19, "true" );
	UxPutMaxLength( textField19, 320 );
	UxPutForeground( textField19, TextForeground );
	UxPutBackground( textField19, TextBackground );
	UxPutHeight( textField19, 30 );
	UxPutWidth( textField19, 210 );
	UxPutY( textField19, 264 );
	UxPutX( textField19, 151 );

	UxPutAncestorSensitive( textField21, "true" );
	UxPutHighlightColor( textField21, "Black" );
	UxPutCursorPositionVisible( textField21, "false" );
	UxPutFontList( textField21, SmallPropFont );
	UxPutMaxLength( textField21, 160 );
	UxPutForeground( textField21, TextForeground );
	UxPutEditable( textField21, "false" );
	UxPutBackground( textField21, TextBackground );
	UxPutHeight( textField21, 30 );
	UxPutWidth( textField21, 210 );
	UxPutY( textField21, 54 );
	UxPutX( textField21, 361 );

	UxPutAncestorSensitive( textField22, "true" );
	UxPutHighlightColor( textField22, "Black" );
	UxPutCursorPositionVisible( textField22, "false" );
	UxPutFontList( textField22, SmallPropFont );
	UxPutMaxLength( textField22, 160 );
	UxPutForeground( textField22, TextForeground );
	UxPutEditable( textField22, "false" );
	UxPutBackground( textField22, TextBackground );
	UxPutHeight( textField22, 30 );
	UxPutWidth( textField22, 210 );
	UxPutY( textField22, 84 );
	UxPutX( textField22, 361 );

	UxPutAncestorSensitive( textField23, "true" );
	UxPutHighlightColor( textField23, "Black" );
	UxPutCursorPositionVisible( textField23, "false" );
	UxPutFontList( textField23, SmallPropFont );
	UxPutMaxLength( textField23, 160 );
	UxPutForeground( textField23, TextForeground );
	UxPutEditable( textField23, "false" );
	UxPutBackground( textField23, TextBackground );
	UxPutHeight( textField23, 30 );
	UxPutWidth( textField23, 210 );
	UxPutY( textField23, 114 );
	UxPutX( textField23, 361 );

	UxPutAncestorSensitive( textField24, "true" );
	UxPutHighlightColor( textField24, "Black" );
	UxPutCursorPositionVisible( textField24, "false" );
	UxPutFontList( textField24, SmallPropFont );
	UxPutMaxLength( textField24, 160 );
	UxPutForeground( textField24, TextForeground );
	UxPutEditable( textField24, "false" );
	UxPutBackground( textField24, TextBackground );
	UxPutHeight( textField24, 30 );
	UxPutWidth( textField24, 210 );
	UxPutY( textField24, 144 );
	UxPutX( textField24, 361 );

	UxPutAncestorSensitive( textField25, "true" );
	UxPutHighlightColor( textField25, "Black" );
	UxPutCursorPositionVisible( textField25, "false" );
	UxPutFontList( textField25, SmallPropFont );
	UxPutMaxLength( textField25, 160 );
	UxPutForeground( textField25, TextForeground );
	UxPutEditable( textField25, "false" );
	UxPutBackground( textField25, TextBackground );
	UxPutHeight( textField25, 30 );
	UxPutWidth( textField25, 210 );
	UxPutY( textField25, 174 );
	UxPutX( textField25, 361 );

	UxPutAncestorSensitive( textField26, "true" );
	UxPutHighlightColor( textField26, "Black" );
	UxPutCursorPositionVisible( textField26, "false" );
	UxPutFontList( textField26, SmallPropFont );
	UxPutMaxLength( textField26, 160 );
	UxPutForeground( textField26, TextForeground );
	UxPutEditable( textField26, "false" );
	UxPutBackground( textField26, TextBackground );
	UxPutHeight( textField26, 30 );
	UxPutWidth( textField26, 210 );
	UxPutY( textField26, 204 );
	UxPutX( textField26, 361 );

	UxPutAncestorSensitive( textField27, "true" );
	UxPutHighlightColor( textField27, "Black" );
	UxPutCursorPositionVisible( textField27, "false" );
	UxPutFontList( textField27, SmallPropFont );
	UxPutMaxLength( textField27, 160 );
	UxPutForeground( textField27, TextForeground );
	UxPutEditable( textField27, "false" );
	UxPutBackground( textField27, TextBackground );
	UxPutHeight( textField27, 30 );
	UxPutWidth( textField27, 210 );
	UxPutY( textField27, 234 );
	UxPutX( textField27, 361 );

	UxPutAncestorSensitive( textField28, "true" );
	UxPutHighlightColor( textField28, "Black" );
	UxPutCursorPositionVisible( textField28, "false" );
	UxPutFontList( textField28, SmallPropFont );
	UxPutMaxLength( textField28, 160 );
	UxPutForeground( textField28, TextForeground );
	UxPutEditable( textField28, "false" );
	UxPutBackground( textField28, TextBackground );
	UxPutHeight( textField28, 30 );
	UxPutWidth( textField28, 210 );
	UxPutY( textField28, 264 );
	UxPutX( textField28, 361 );

	UxPutAncestorSensitive( textField20, "true" );
	UxPutHighlightColor( textField20, "Black" );
	UxPutCursorPositionVisible( textField20, "false" );
	UxPutFontList( textField20, SmallPropFont );
	UxPutMaxLength( textField20, 160 );
	UxPutForeground( textField20, TextForeground );
	UxPutEditable( textField20, "false" );
	UxPutBackground( textField20, TextBackground );
	UxPutHeight( textField20, 30 );
	UxPutWidth( textField20, 210 );
	UxPutY( textField20, 25 );
	UxPutX( textField20, 361 );

	UxPutAncestorSensitive( toggleButton10, "true" );
	UxPutIndicatorSize( toggleButton10, 13 );
	UxPutHighlightColor( toggleButton10, "Black" );
	UxPutForeground( toggleButton10, "Black" );
	UxPutTranslations( toggleButton10, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton10, "true" );
	UxPutSelectColor( toggleButton10, SelectColor );
	UxPutBackground( toggleButton10, LabelBackground );
	UxPutHeight( toggleButton10, 30 );
	UxPutWidth( toggleButton10, 20 );
	UxPutY( toggleButton10, 21 );
	UxPutX( toggleButton10, 83 );

	UxPutAncestorSensitive( toggleButton11, "true" );
	UxPutIndicatorSize( toggleButton11, 13 );
	UxPutHighlightColor( toggleButton11, "Black" );
	UxPutForeground( toggleButton11, "Black" );
	UxPutTranslations( toggleButton11, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton11, "true" );
	UxPutSelectColor( toggleButton11, SelectColor );
	UxPutBackground( toggleButton11, LabelBackground );
	UxPutHeight( toggleButton11, 30 );
	UxPutWidth( toggleButton11, 20 );
	UxPutY( toggleButton11, 21 );
	UxPutX( toggleButton11, 103 );

	UxPutAncestorSensitive( toggleButton12, "true" );
	UxPutIndicatorSize( toggleButton12, 13 );
	UxPutHighlightColor( toggleButton12, "Black" );
	UxPutForeground( toggleButton12, "Black" );
	UxPutTranslations( toggleButton12, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton12, "true" );
	UxPutSelectColor( toggleButton12, SelectColor );
	UxPutBackground( toggleButton12, LabelBackground );
	UxPutHeight( toggleButton12, 30 );
	UxPutWidth( toggleButton12, 20 );
	UxPutY( toggleButton12, 21 );
	UxPutX( toggleButton12, 123 );

	UxPutAncestorSensitive( toggleButton13, "true" );
	UxPutIndicatorSize( toggleButton13, 13 );
	UxPutHighlightColor( toggleButton13, "Black" );
	UxPutForeground( toggleButton13, "Black" );
	UxPutTranslations( toggleButton13, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton13, "true" );
	UxPutSelectColor( toggleButton13, SelectColor );
	UxPutBackground( toggleButton13, LabelBackground );
	UxPutHeight( toggleButton13, 30 );
	UxPutWidth( toggleButton13, 20 );
	UxPutY( toggleButton13, 51 );
	UxPutX( toggleButton13, 83 );

	UxPutAncestorSensitive( toggleButton14, "true" );
	UxPutIndicatorSize( toggleButton14, 13 );
	UxPutHighlightColor( toggleButton14, "Black" );
	UxPutForeground( toggleButton14, "Black" );
	UxPutTranslations( toggleButton14, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton14, "true" );
	UxPutSelectColor( toggleButton14, SelectColor );
	UxPutBackground( toggleButton14, LabelBackground );
	UxPutHeight( toggleButton14, 30 );
	UxPutWidth( toggleButton14, 20 );
	UxPutY( toggleButton14, 51 );
	UxPutX( toggleButton14, 103 );

	UxPutAncestorSensitive( toggleButton15, "true" );
	UxPutIndicatorSize( toggleButton15, 13 );
	UxPutHighlightColor( toggleButton15, "Black" );
	UxPutForeground( toggleButton15, "Black" );
	UxPutTranslations( toggleButton15, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton15, "true" );
	UxPutSelectColor( toggleButton15, SelectColor );
	UxPutBackground( toggleButton15, LabelBackground );
	UxPutHeight( toggleButton15, 30 );
	UxPutWidth( toggleButton15, 20 );
	UxPutY( toggleButton15, 51 );
	UxPutX( toggleButton15, 123 );

	UxPutAncestorSensitive( toggleButton16, "true" );
	UxPutIndicatorSize( toggleButton16, 13 );
	UxPutHighlightColor( toggleButton16, "Black" );
	UxPutForeground( toggleButton16, "Black" );
	UxPutTranslations( toggleButton16, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton16, "true" );
	UxPutSelectColor( toggleButton16, SelectColor );
	UxPutBackground( toggleButton16, LabelBackground );
	UxPutHeight( toggleButton16, 30 );
	UxPutWidth( toggleButton16, 20 );
	UxPutY( toggleButton16, 81 );
	UxPutX( toggleButton16, 83 );

	UxPutAncestorSensitive( toggleButton17, "true" );
	UxPutIndicatorSize( toggleButton17, 13 );
	UxPutHighlightColor( toggleButton17, "Black" );
	UxPutForeground( toggleButton17, "Black" );
	UxPutTranslations( toggleButton17, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton17, "true" );
	UxPutSelectColor( toggleButton17, SelectColor );
	UxPutBackground( toggleButton17, LabelBackground );
	UxPutHeight( toggleButton17, 30 );
	UxPutWidth( toggleButton17, 20 );
	UxPutY( toggleButton17, 81 );
	UxPutX( toggleButton17, 103 );

	UxPutAncestorSensitive( toggleButton18, "true" );
	UxPutIndicatorSize( toggleButton18, 13 );
	UxPutHighlightColor( toggleButton18, "Black" );
	UxPutForeground( toggleButton18, "Black" );
	UxPutTranslations( toggleButton18, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton18, "true" );
	UxPutSelectColor( toggleButton18, SelectColor );
	UxPutBackground( toggleButton18, LabelBackground );
	UxPutHeight( toggleButton18, 30 );
	UxPutWidth( toggleButton18, 20 );
	UxPutY( toggleButton18, 81 );
	UxPutX( toggleButton18, 123 );

	UxPutAncestorSensitive( toggleButton19, "true" );
	UxPutIndicatorSize( toggleButton19, 13 );
	UxPutHighlightColor( toggleButton19, "Black" );
	UxPutForeground( toggleButton19, "Black" );
	UxPutTranslations( toggleButton19, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton19, "true" );
	UxPutSelectColor( toggleButton19, SelectColor );
	UxPutBackground( toggleButton19, LabelBackground );
	UxPutHeight( toggleButton19, 30 );
	UxPutWidth( toggleButton19, 20 );
	UxPutY( toggleButton19, 111 );
	UxPutX( toggleButton19, 83 );

	UxPutAncestorSensitive( toggleButton20, "true" );
	UxPutIndicatorSize( toggleButton20, 13 );
	UxPutHighlightColor( toggleButton20, "Black" );
	UxPutForeground( toggleButton20, "Black" );
	UxPutTranslations( toggleButton20, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton20, "true" );
	UxPutSelectColor( toggleButton20, SelectColor );
	UxPutBackground( toggleButton20, LabelBackground );
	UxPutHeight( toggleButton20, 30 );
	UxPutWidth( toggleButton20, 20 );
	UxPutY( toggleButton20, 111 );
	UxPutX( toggleButton20, 103 );

	UxPutAncestorSensitive( toggleButton21, "true" );
	UxPutIndicatorSize( toggleButton21, 13 );
	UxPutHighlightColor( toggleButton21, "Black" );
	UxPutForeground( toggleButton21, "Black" );
	UxPutTranslations( toggleButton21, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton21, "true" );
	UxPutSelectColor( toggleButton21, SelectColor );
	UxPutBackground( toggleButton21, LabelBackground );
	UxPutHeight( toggleButton21, 30 );
	UxPutWidth( toggleButton21, 20 );
	UxPutY( toggleButton21, 111 );
	UxPutX( toggleButton21, 123 );

	UxPutAncestorSensitive( toggleButton22, "true" );
	UxPutIndicatorSize( toggleButton22, 13 );
	UxPutHighlightColor( toggleButton22, "Black" );
	UxPutForeground( toggleButton22, "Black" );
	UxPutTranslations( toggleButton22, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton22, "true" );
	UxPutSelectColor( toggleButton22, SelectColor );
	UxPutBackground( toggleButton22, LabelBackground );
	UxPutHeight( toggleButton22, 30 );
	UxPutWidth( toggleButton22, 20 );
	UxPutY( toggleButton22, 141 );
	UxPutX( toggleButton22, 83 );

	UxPutAncestorSensitive( toggleButton23, "true" );
	UxPutIndicatorSize( toggleButton23, 13 );
	UxPutHighlightColor( toggleButton23, "Black" );
	UxPutForeground( toggleButton23, "Black" );
	UxPutTranslations( toggleButton23, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton23, "true" );
	UxPutSelectColor( toggleButton23, SelectColor );
	UxPutBackground( toggleButton23, LabelBackground );
	UxPutHeight( toggleButton23, 30 );
	UxPutWidth( toggleButton23, 20 );
	UxPutY( toggleButton23, 141 );
	UxPutX( toggleButton23, 103 );

	UxPutAncestorSensitive( toggleButton24, "true" );
	UxPutIndicatorSize( toggleButton24, 13 );
	UxPutHighlightColor( toggleButton24, "Black" );
	UxPutForeground( toggleButton24, "Black" );
	UxPutTranslations( toggleButton24, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton24, "true" );
	UxPutSelectColor( toggleButton24, SelectColor );
	UxPutBackground( toggleButton24, LabelBackground );
	UxPutHeight( toggleButton24, 30 );
	UxPutWidth( toggleButton24, 20 );
	UxPutY( toggleButton24, 141 );
	UxPutX( toggleButton24, 123 );

	UxPutAncestorSensitive( toggleButton28, "true" );
	UxPutIndicatorSize( toggleButton28, 13 );
	UxPutHighlightColor( toggleButton28, "Black" );
	UxPutForeground( toggleButton28, "Black" );
	UxPutTranslations( toggleButton28, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton28, "true" );
	UxPutSelectColor( toggleButton28, SelectColor );
	UxPutBackground( toggleButton28, LabelBackground );
	UxPutHeight( toggleButton28, 30 );
	UxPutWidth( toggleButton28, 20 );
	UxPutY( toggleButton28, 201 );
	UxPutX( toggleButton28, 83 );

	UxPutAncestorSensitive( toggleButton29, "true" );
	UxPutIndicatorSize( toggleButton29, 13 );
	UxPutHighlightColor( toggleButton29, "Black" );
	UxPutForeground( toggleButton29, "Black" );
	UxPutTranslations( toggleButton29, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton29, "true" );
	UxPutSelectColor( toggleButton29, SelectColor );
	UxPutBackground( toggleButton29, LabelBackground );
	UxPutHeight( toggleButton29, 30 );
	UxPutWidth( toggleButton29, 20 );
	UxPutY( toggleButton29, 201 );
	UxPutX( toggleButton29, 103 );

	UxPutAncestorSensitive( toggleButton30, "true" );
	UxPutIndicatorSize( toggleButton30, 13 );
	UxPutHighlightColor( toggleButton30, "Black" );
	UxPutForeground( toggleButton30, "Black" );
	UxPutTranslations( toggleButton30, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton30, "true" );
	UxPutSelectColor( toggleButton30, SelectColor );
	UxPutBackground( toggleButton30, LabelBackground );
	UxPutHeight( toggleButton30, 30 );
	UxPutWidth( toggleButton30, 20 );
	UxPutY( toggleButton30, 201 );
	UxPutX( toggleButton30, 123 );

	UxPutAncestorSensitive( toggleButton31, "true" );
	UxPutIndicatorSize( toggleButton31, 13 );
	UxPutHighlightColor( toggleButton31, "Black" );
	UxPutForeground( toggleButton31, "Black" );
	UxPutTranslations( toggleButton31, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton31, "true" );
	UxPutSelectColor( toggleButton31, SelectColor );
	UxPutBackground( toggleButton31, LabelBackground );
	UxPutHeight( toggleButton31, 30 );
	UxPutWidth( toggleButton31, 20 );
	UxPutY( toggleButton31, 231 );
	UxPutX( toggleButton31, 83 );

	UxPutAncestorSensitive( toggleButton32, "true" );
	UxPutIndicatorSize( toggleButton32, 13 );
	UxPutHighlightColor( toggleButton32, "Black" );
	UxPutForeground( toggleButton32, "Black" );
	UxPutTranslations( toggleButton32, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton32, "true" );
	UxPutSelectColor( toggleButton32, SelectColor );
	UxPutBackground( toggleButton32, LabelBackground );
	UxPutHeight( toggleButton32, 30 );
	UxPutWidth( toggleButton32, 20 );
	UxPutY( toggleButton32, 231 );
	UxPutX( toggleButton32, 103 );

	UxPutAncestorSensitive( toggleButton33, "true" );
	UxPutIndicatorSize( toggleButton33, 13 );
	UxPutHighlightColor( toggleButton33, "Black" );
	UxPutForeground( toggleButton33, "Black" );
	UxPutTranslations( toggleButton33, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton33, "true" );
	UxPutSelectColor( toggleButton33, SelectColor );
	UxPutBackground( toggleButton33, LabelBackground );
	UxPutHeight( toggleButton33, 30 );
	UxPutWidth( toggleButton33, 20 );
	UxPutY( toggleButton33, 231 );
	UxPutX( toggleButton33, 123 );

	UxPutAncestorSensitive( GaussUpComponent, "true" );
	UxPutHighlightColor( GaussUpComponent, "Black" );
	UxPutTranslations( GaussUpComponent, HELPGausstab );
	UxPutHighlightOnEnter( GaussUpComponent, "true" );
	UxPutBorderColor( GaussUpComponent, TextBackground );
	UxPutForeground( GaussUpComponent, MenuBackground );
	UxPutBackground( GaussUpComponent, TextBackground );
	UxPutHeight( GaussUpComponent, 20 );
	UxPutWidth( GaussUpComponent, 30 );
	UxPutY( GaussUpComponent, 302 );
	UxPutX( GaussUpComponent, 145 );

	UxPutAncestorSensitive( GaussDownComponent, "true" );
	UxPutHighlightColor( GaussDownComponent, "#000000" );
	UxPutTranslations( GaussDownComponent, HELPGausstab );
	UxPutHighlightOnEnter( GaussDownComponent, "true" );
	UxPutBorderColor( GaussDownComponent, TextBackground );
	UxPutArrowDirection( GaussDownComponent, "arrow_down" );
	UxPutForeground( GaussDownComponent, MenuBackground );
	UxPutBackground( GaussDownComponent, TextBackground );
	UxPutHeight( GaussDownComponent, 20 );
	UxPutWidth( GaussDownComponent, 30 );
	UxPutY( GaussDownComponent, 318 );
	UxPutX( GaussDownComponent, 145 );

	UxPutAncestorSensitive( label6, "true" );
	UxPutHighlightColor( label6, "Black" );
	UxPutForeground( label6, TextForeground );
	UxPutFontList( label6, TextFont );
	UxPutLabelString( label6, "Iterations :" );
	UxPutBackground( label6, LabelBackground );
	UxPutHeight( label6, 22 );
	UxPutWidth( label6, 90 );
	UxPutY( label6, 312 );
	UxPutX( label6, 276 );

	UxPutMarginWidth( iterations_text, 5 );
	UxPutMarginHeight( iterations_text, 3 );
	UxPutAncestorSensitive( iterations_text, "true" );
	UxPutMaxLength( iterations_text, 5 );
	UxPutHighlightColor( iterations_text, "Black" );
	UxPutTranslations( iterations_text, TextGaussTab );
	UxPutFontList( iterations_text, TextFont );
	UxPutHighlightOnEnter( iterations_text, "true" );
	UxPutForeground( iterations_text, TextForeground );
	UxPutBackground( iterations_text, TextBackground );
	UxPutHeight( iterations_text, 30 );
	UxPutWidth( iterations_text, 40 );
	UxPutY( iterations_text, 310 );
	UxPutX( iterations_text, 436 );

	UxPutMarginWidth( iterations_text1, 5 );
	UxPutMarginHeight( iterations_text1, 3 );
	UxPutAncestorSensitive( iterations_text1, "true" );
	UxPutMaxLength( iterations_text1, 4 );
	UxPutHighlightColor( iterations_text1, "Black" );
	UxPutTranslations( iterations_text1, TextGaussTab );
	UxPutFontList( iterations_text1, TextFont );
	UxPutHighlightOnEnter( iterations_text1, "true" );
	UxPutForeground( iterations_text1, TextForeground );
	UxPutBackground( iterations_text1, TextBackground );
	UxPutHeight( iterations_text1, 30 );
	UxPutWidth( iterations_text1, 40 );
	UxPutY( iterations_text1, 310 );
	UxPutX( iterations_text1, 374 );

	UxPutMarginWidth( iterations_text2, 5 );
	UxPutMarginHeight( iterations_text2, 3 );
	UxPutAncestorSensitive( iterations_text2, "true" );
	UxPutMaxLength( iterations_text2, 6 );
	UxPutHighlightColor( iterations_text2, "Black" );
	UxPutCursorPositionVisible( iterations_text2, "false" );
	UxPutFontList( iterations_text2, TextFont );
	UxPutForeground( iterations_text2, TextForeground );
	UxPutEditable( iterations_text2, "false" );
	UxPutBackground( iterations_text2, TextBackground );
	UxPutHeight( iterations_text2, 30 );
	UxPutWidth( iterations_text2, 40 );
	UxPutY( iterations_text2, 310 );
	UxPutX( iterations_text2, 502 );

	UxPutAncestorSensitive( toggleButton25, "true" );
	UxPutIndicatorSize( toggleButton25, 13 );
	UxPutHighlightColor( toggleButton25, "Black" );
	UxPutForeground( toggleButton25, "Black" );
	UxPutTranslations( toggleButton25, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton25, "true" );
	UxPutSelectColor( toggleButton25, SelectColor );
	UxPutBackground( toggleButton25, LabelBackground );
	UxPutHeight( toggleButton25, 30 );
	UxPutWidth( toggleButton25, 20 );
	UxPutY( toggleButton25, 171 );
	UxPutX( toggleButton25, 83 );

	UxPutAncestorSensitive( toggleButton26, "true" );
	UxPutIndicatorSize( toggleButton26, 13 );
	UxPutHighlightColor( toggleButton26, "Black" );
	UxPutForeground( toggleButton26, "Black" );
	UxPutTranslations( toggleButton26, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton26, "true" );
	UxPutSelectColor( toggleButton26, SelectColor );
	UxPutBackground( toggleButton26, LabelBackground );
	UxPutHeight( toggleButton26, 30 );
	UxPutWidth( toggleButton26, 20 );
	UxPutY( toggleButton26, 171 );
	UxPutX( toggleButton26, 103 );

	UxPutAncestorSensitive( toggleButton27, "true" );
	UxPutIndicatorSize( toggleButton27, 13 );
	UxPutHighlightColor( toggleButton27, "Black" );
	UxPutForeground( toggleButton27, "Black" );
	UxPutTranslations( toggleButton27, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton27, "true" );
	UxPutSelectColor( toggleButton27, SelectColor );
	UxPutBackground( toggleButton27, LabelBackground );
	UxPutHeight( toggleButton27, 30 );
	UxPutWidth( toggleButton27, 20 );
	UxPutY( toggleButton27, 171 );
	UxPutX( toggleButton27, 123 );

	UxPutAncestorSensitive( toggleButton34, "true" );
	UxPutIndicatorSize( toggleButton34, 13 );
	UxPutHighlightColor( toggleButton34, "Black" );
	UxPutForeground( toggleButton34, "Black" );
	UxPutTranslations( toggleButton34, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton34, "true" );
	UxPutSelectColor( toggleButton34, SelectColor );
	UxPutBackground( toggleButton34, LabelBackground );
	UxPutHeight( toggleButton34, 30 );
	UxPutWidth( toggleButton34, 20 );
	UxPutY( toggleButton34, 263 );
	UxPutX( toggleButton34, 83 );

	UxPutAncestorSensitive( toggleButton35, "true" );
	UxPutIndicatorSize( toggleButton35, 13 );
	UxPutHighlightColor( toggleButton35, "Black" );
	UxPutForeground( toggleButton35, "Black" );
	UxPutTranslations( toggleButton35, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton35, "true" );
	UxPutSelectColor( toggleButton35, SelectColor );
	UxPutBackground( toggleButton35, LabelBackground );
	UxPutHeight( toggleButton35, 30 );
	UxPutWidth( toggleButton35, 20 );
	UxPutY( toggleButton35, 263 );
	UxPutX( toggleButton35, 103 );

	UxPutAncestorSensitive( toggleButton36, "true" );
	UxPutIndicatorSize( toggleButton36, 13 );
	UxPutHighlightColor( toggleButton36, "Black" );
	UxPutForeground( toggleButton36, "Black" );
	UxPutTranslations( toggleButton36, HELPGausstab );
	UxPutHighlightOnEnter( toggleButton36, "true" );
	UxPutSelectColor( toggleButton36, SelectColor );
	UxPutBackground( toggleButton36, LabelBackground );
	UxPutHeight( toggleButton36, 30 );
	UxPutWidth( toggleButton36, 20 );
	UxPutY( toggleButton36, 263 );
	UxPutX( toggleButton36, 123 );

	UxPutForeground( help_text_gauss, TextForeground );
	UxPutMarginWidth( help_text_gauss, 3 );
	UxPutMarginHeight( help_text_gauss, 3 );
	UxPutAncestorSensitive( help_text_gauss, "true" );
	UxPutCursorPositionVisible( help_text_gauss, "false" );
	UxPutFontList( help_text_gauss, TextFont );
	UxPutEditable( help_text_gauss, "false" );
	UxPutBackground( help_text_gauss, SHelpBackground );
	UxPutHeight( help_text_gauss, 46 );
	UxPutWidth( help_text_gauss, 578 );
	UxPutY( help_text_gauss, 340 );
	UxPutX( help_text_gauss, 4 );

	UxPutAncestorSensitive( GaussForm2, "true" );
	UxPutBackground( GaussForm2, ButtonBackground );
	UxPutHeight( GaussForm2, 36 );
	UxPutWidth( GaussForm2, 586 );
	UxPutY( GaussForm2, 388 );
	UxPutX( GaussForm2, 0 );
	UxPutResizePolicy( GaussForm2, "resize_none" );

	UxPutSensitive( ExecuteGauss, "true" );
	UxPutAncestorSensitive( ExecuteGauss, "true" );
	UxPutHighlightColor( ExecuteGauss, "Black" );
	UxPutTranslations( ExecuteGauss, HelpGaussTable );
	UxPutHighlightOnEnter( ExecuteGauss, "true" );
	UxPutBackground( ExecuteGauss, ButtonBackground );
	UxPutForeground( ExecuteGauss, ButtonForeground );
	UxPutFontList( ExecuteGauss, BoldTextFont );
	UxPutLabelString( ExecuteGauss, "Apply" );
	UxPutHeight( ExecuteGauss, 30 );
	UxPutWidth( ExecuteGauss, 90 );
	UxPutY( ExecuteGauss, 2 );
	UxPutX( ExecuteGauss, 256 );

	UxPutSensitive( ClearGauss, "true" );
	UxPutAncestorSensitive( ClearGauss, "true" );
	UxPutHighlightColor( ClearGauss, "Black" );
	UxPutTranslations( ClearGauss, HelpGaussTable );
	UxPutHighlightOnEnter( ClearGauss, "true" );
	UxPutBackground( ClearGauss, ButtonBackground );
	UxPutForeground( ClearGauss, ApplyForeground );
	UxPutFontList( ClearGauss, BoldTextFont );
	UxPutLabelString( ClearGauss, "Clear" );
	UxPutHeight( ClearGauss, 30 );
	UxPutWidth( ClearGauss, 90 );
	UxPutY( ClearGauss, 2 );
	UxPutX( ClearGauss, 34 );

	UxPutSensitive( CopyGauss, "true" );
	UxPutAncestorSensitive( CopyGauss, "true" );
	UxPutHighlightColor( CopyGauss, "Black" );
	UxPutTranslations( CopyGauss, HelpGaussTable );
	UxPutHighlightOnEnter( CopyGauss, "true" );
	UxPutBackground( CopyGauss, ButtonBackground );
	UxPutForeground( CopyGauss, ButtonForeground );
	UxPutFontList( CopyGauss, BoldTextFont );
	UxPutLabelString( CopyGauss, "Copy" );
	UxPutHeight( CopyGauss, 30 );
	UxPutWidth( CopyGauss, 90 );
	UxPutY( CopyGauss, 2 );
	UxPutX( CopyGauss, 146 );

	UxPutBackground( separator7, ButtonBackground );
	UxPutHeight( separator7, 6 );
	UxPutWidth( separator7, 590 );
	UxPutY( separator7, 382 );
	UxPutX( separator7, -2 );

	UxPutAncestorSensitive( label7, "true" );
	UxPutHighlightColor( label7, "Black" );
	UxPutFontList( label7, TextFont );
	UxPutForeground( label7, TextForeground );
	UxPutLabelString( label7, "Min" );
	UxPutBackground( label7, LabelBackground );
	UxPutHeight( label7, 20 );
	UxPutWidth( label7, 46 );
	UxPutY( label7, 292 );
	UxPutX( label7, 374 );

	UxPutAncestorSensitive( label8, "true" );
	UxPutHighlightColor( label8, "Black" );
	UxPutFontList( label8, TextFont );
	UxPutForeground( label8, TextForeground );
	UxPutLabelString( label8, "Max" );
	UxPutBackground( label8, LabelBackground );
	UxPutHeight( label8, 20 );
	UxPutWidth( label8, 46 );
	UxPutY( label8, 292 );
	UxPutX( label8, 432 );

	UxPutAncestorSensitive( label9, "true" );
	UxPutHighlightColor( label9, "Black" );
	UxPutFontList( label9, TextFont );
	UxPutForeground( label9, TextForeground );
	UxPutLabelString( label9, "Current" );
	UxPutBackground( label9, LabelBackground );
	UxPutHeight( label9, 20 );
	UxPutWidth( label9, 54 );
	UxPutY( label9, 292 );
	UxPutX( label9, 496 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_GaussShell()
{
	/* Create the swidgets */

	GaussShell = UxCreateApplicationShell( "GaussShell", NO_PARENT );
	UxPutContext( GaussShell, UxGaussShellContext );

	GaussWindow = UxCreateMainWindow( "GaussWindow", GaussShell );
	GaussMenu = UxCreateRowColumn( "GaussMenu", GaussWindow );
	FramePane2 = UxCreateRowColumn( "FramePane2", GaussMenu );
	CutxGauss = UxCreatePushButton( "CutxGauss", FramePane2 );
	CutyGauss = UxCreatePushButton( "CutyGauss", FramePane2 );
	UnzoomGauss = UxCreatePushButton( "UnzoomGauss", FramePane2 );
	RedrawGauss = UxCreatePushButton( "RedrawGauss", FramePane2 );
	PrintGauss = UxCreatePushButton( "PrintGauss", FramePane2 );
	menu1_top_b1 = UxCreateCascadeButton( "menu1_top_b1", GaussMenu );
	GraphicsPane = UxCreateRowColumn( "GraphicsPane", GaussMenu );
	ResidualItem = UxCreatePushButton( "ResidualItem", GraphicsPane );
	InitialItem = UxCreatePushButton( "InitialItem", GraphicsPane );
	ComponentsItem = UxCreatePushButton( "ComponentsItem", GraphicsPane );
	SolutionItem = UxCreatePushButton( "SolutionItem", GraphicsPane );
	ContinItem = UxCreatePushButton( "ContinItem", GraphicsPane );
	menu1_top_b2 = UxCreateCascadeButton( "menu1_top_b2", GaussMenu );
	OtherPane = UxCreateRowColumn( "OtherPane", GaussMenu );
	StatItem = UxCreatePushButton( "StatItem", OtherPane );
	IntegrItem = UxCreatePushButton( "IntegrItem", OtherPane );
	GaussMenu_top_b1 = UxCreateCascadeButton( "GaussMenu_top_b1", GaussMenu );
	ReturnGauss2 = UxCreateRowColumn( "ReturnGauss2", GaussMenu );
	ReturnButton = UxCreatePushButton( "ReturnButton", ReturnGauss2 );
	menu1_top_b4 = UxCreateCascadeButton( "menu1_top_b4", GaussMenu );
	GaussTopform = UxCreateForm( "GaussTopform", GaussWindow );
	label1 = UxCreateLabel( "label1", GaussTopform );
	Components = UxCreateText( "Components", GaussTopform );
	label2 = UxCreateLabel( "label2", GaussTopform );
	label3 = UxCreateLabel( "label3", GaussTopform );
	label4 = UxCreateLabel( "label4", GaussTopform );
	label5 = UxCreateLabel( "label5", GaussTopform );
	toggleButton1 = UxCreateToggleButton( "toggleButton1", GaussTopform );
	toggleButton2 = UxCreateToggleButton( "toggleButton2", GaussTopform );
	toggleButton3 = UxCreateToggleButton( "toggleButton3", GaussTopform );
	toggleButton4 = UxCreateToggleButton( "toggleButton4", GaussTopform );
	toggleButton5 = UxCreateToggleButton( "toggleButton5", GaussTopform );
	toggleButton6 = UxCreateToggleButton( "toggleButton6", GaussTopform );
	toggleButton7 = UxCreateToggleButton( "toggleButton7", GaussTopform );
	toggleButton8 = UxCreateToggleButton( "toggleButton8", GaussTopform );
	toggleButton9 = UxCreateToggleButton( "toggleButton9", GaussTopform );
	textField11 = UxCreateText( "textField11", GaussTopform );
	textField12 = UxCreateText( "textField12", GaussTopform );
	textField13 = UxCreateText( "textField13", GaussTopform );
	textField14 = UxCreateText( "textField14", GaussTopform );
	textField15 = UxCreateText( "textField15", GaussTopform );
	textField16 = UxCreateText( "textField16", GaussTopform );
	textField17 = UxCreateText( "textField17", GaussTopform );
	textField18 = UxCreateText( "textField18", GaussTopform );
	textField19 = UxCreateText( "textField19", GaussTopform );
	textField21 = UxCreateText( "textField21", GaussTopform );
	textField22 = UxCreateText( "textField22", GaussTopform );
	textField23 = UxCreateText( "textField23", GaussTopform );
	textField24 = UxCreateText( "textField24", GaussTopform );
	textField25 = UxCreateText( "textField25", GaussTopform );
	textField26 = UxCreateText( "textField26", GaussTopform );
	textField27 = UxCreateText( "textField27", GaussTopform );
	textField28 = UxCreateText( "textField28", GaussTopform );
	textField20 = UxCreateText( "textField20", GaussTopform );
	toggleButton10 = UxCreateToggleButton( "toggleButton10", GaussTopform );
	toggleButton11 = UxCreateToggleButton( "toggleButton11", GaussTopform );
	toggleButton12 = UxCreateToggleButton( "toggleButton12", GaussTopform );
	toggleButton13 = UxCreateToggleButton( "toggleButton13", GaussTopform );
	toggleButton14 = UxCreateToggleButton( "toggleButton14", GaussTopform );
	toggleButton15 = UxCreateToggleButton( "toggleButton15", GaussTopform );
	toggleButton16 = UxCreateToggleButton( "toggleButton16", GaussTopform );
	toggleButton17 = UxCreateToggleButton( "toggleButton17", GaussTopform );
	toggleButton18 = UxCreateToggleButton( "toggleButton18", GaussTopform );
	toggleButton19 = UxCreateToggleButton( "toggleButton19", GaussTopform );
	toggleButton20 = UxCreateToggleButton( "toggleButton20", GaussTopform );
	toggleButton21 = UxCreateToggleButton( "toggleButton21", GaussTopform );
	toggleButton22 = UxCreateToggleButton( "toggleButton22", GaussTopform );
	toggleButton23 = UxCreateToggleButton( "toggleButton23", GaussTopform );
	toggleButton24 = UxCreateToggleButton( "toggleButton24", GaussTopform );
	toggleButton28 = UxCreateToggleButton( "toggleButton28", GaussTopform );
	toggleButton29 = UxCreateToggleButton( "toggleButton29", GaussTopform );
	toggleButton30 = UxCreateToggleButton( "toggleButton30", GaussTopform );
	toggleButton31 = UxCreateToggleButton( "toggleButton31", GaussTopform );
	toggleButton32 = UxCreateToggleButton( "toggleButton32", GaussTopform );
	toggleButton33 = UxCreateToggleButton( "toggleButton33", GaussTopform );
	GaussUpComponent = UxCreateArrowButton( "GaussUpComponent", GaussTopform );
	GaussDownComponent = UxCreateArrowButton( "GaussDownComponent", GaussTopform );
	label6 = UxCreateLabel( "label6", GaussTopform );
	iterations_text = UxCreateText( "iterations_text", GaussTopform );
	iterations_text1 = UxCreateText( "iterations_text1", GaussTopform );
	iterations_text2 = UxCreateText( "iterations_text2", GaussTopform );
	toggleButton25 = UxCreateToggleButton( "toggleButton25", GaussTopform );
	toggleButton26 = UxCreateToggleButton( "toggleButton26", GaussTopform );
	toggleButton27 = UxCreateToggleButton( "toggleButton27", GaussTopform );
	toggleButton34 = UxCreateToggleButton( "toggleButton34", GaussTopform );
	toggleButton35 = UxCreateToggleButton( "toggleButton35", GaussTopform );
	toggleButton36 = UxCreateToggleButton( "toggleButton36", GaussTopform );
	help_text_gauss = UxCreateText( "help_text_gauss", GaussTopform );
	GaussForm2 = UxCreateForm( "GaussForm2", GaussTopform );
	ExecuteGauss = UxCreatePushButton( "ExecuteGauss", GaussForm2 );
	ClearGauss = UxCreatePushButton( "ClearGauss", GaussForm2 );
	CopyGauss = UxCreatePushButton( "CopyGauss", GaussForm2 );
	separator7 = UxCreateSeparator( "separator7", GaussTopform );
	label7 = UxCreateLabel( "label7", GaussTopform );
	label8 = UxCreateLabel( "label8", GaussTopform );
	label9 = UxCreateLabel( "label9", GaussTopform );

	_Uxinit_GaussShell();

	/* Create the X widgets */

	UxCreateWidget( GaussShell );
	UxCreateWidget( GaussWindow );
	UxCreateWidget( GaussMenu );
	UxCreateWidget( FramePane2 );
	UxCreateWidget( CutxGauss );
	UxCreateWidget( CutyGauss );
	UxCreateWidget( UnzoomGauss );
	UxCreateWidget( RedrawGauss );
	UxCreateWidget( PrintGauss );
	UxPutSubMenuId( menu1_top_b1, "FramePane2" );
	UxCreateWidget( menu1_top_b1 );

	UxCreateWidget( GraphicsPane );
	UxCreateWidget( ResidualItem );
	UxCreateWidget( InitialItem );
	UxCreateWidget( ComponentsItem );
	UxCreateWidget( SolutionItem );
	UxCreateWidget( ContinItem );
	UxPutSubMenuId( menu1_top_b2, "GraphicsPane" );
	UxCreateWidget( menu1_top_b2 );

	UxCreateWidget( OtherPane );
	UxCreateWidget( StatItem );
	UxCreateWidget( IntegrItem );
	UxPutSubMenuId( GaussMenu_top_b1, "OtherPane" );
	UxCreateWidget( GaussMenu_top_b1 );

	UxCreateWidget( ReturnGauss2 );
	UxCreateWidget( ReturnButton );
	UxPutSubMenuId( menu1_top_b4, "ReturnGauss2" );
	UxCreateWidget( menu1_top_b4 );

	UxCreateWidget( GaussTopform );
	UxCreateWidget( label1 );
	UxCreateWidget( Components );
	UxCreateWidget( label2 );
	UxCreateWidget( label3 );
	UxCreateWidget( label4 );
	UxCreateWidget( label5 );
	UxCreateWidget( toggleButton1 );
	UxCreateWidget( toggleButton2 );
	UxCreateWidget( toggleButton3 );
	UxCreateWidget( toggleButton4 );
	UxCreateWidget( toggleButton5 );
	UxCreateWidget( toggleButton6 );
	UxCreateWidget( toggleButton7 );
	UxCreateWidget( toggleButton8 );
	UxCreateWidget( toggleButton9 );
	UxCreateWidget( textField11 );
	UxCreateWidget( textField12 );
	UxCreateWidget( textField13 );
	UxCreateWidget( textField14 );
	UxCreateWidget( textField15 );
	UxCreateWidget( textField16 );
	UxCreateWidget( textField17 );
	UxCreateWidget( textField18 );
	UxCreateWidget( textField19 );
	UxCreateWidget( textField21 );
	UxCreateWidget( textField22 );
	UxCreateWidget( textField23 );
	UxCreateWidget( textField24 );
	UxCreateWidget( textField25 );
	UxCreateWidget( textField26 );
	UxCreateWidget( textField27 );
	UxCreateWidget( textField28 );
	UxCreateWidget( textField20 );
	UxCreateWidget( toggleButton10 );
	UxCreateWidget( toggleButton11 );
	UxCreateWidget( toggleButton12 );
	UxCreateWidget( toggleButton13 );
	UxCreateWidget( toggleButton14 );
	UxCreateWidget( toggleButton15 );
	UxCreateWidget( toggleButton16 );
	UxCreateWidget( toggleButton17 );
	UxCreateWidget( toggleButton18 );
	UxCreateWidget( toggleButton19 );
	UxCreateWidget( toggleButton20 );
	UxCreateWidget( toggleButton21 );
	UxCreateWidget( toggleButton22 );
	UxCreateWidget( toggleButton23 );
	UxCreateWidget( toggleButton24 );
	UxCreateWidget( toggleButton28 );
	UxCreateWidget( toggleButton29 );
	UxCreateWidget( toggleButton30 );
	UxCreateWidget( toggleButton31 );
	UxCreateWidget( toggleButton32 );
	UxCreateWidget( toggleButton33 );
	UxCreateWidget( GaussUpComponent );
	UxCreateWidget( GaussDownComponent );
	UxCreateWidget( label6 );
	UxCreateWidget( iterations_text );
	UxCreateWidget( iterations_text1 );
	UxCreateWidget( iterations_text2 );
	UxCreateWidget( toggleButton25 );
	UxCreateWidget( toggleButton26 );
	UxCreateWidget( toggleButton27 );
	UxCreateWidget( toggleButton34 );
	UxCreateWidget( toggleButton35 );
	UxCreateWidget( toggleButton36 );
	UxCreateWidget( help_text_gauss );
	UxCreateWidget( GaussForm2 );
	UxCreateWidget( ExecuteGauss );
	UxCreateWidget( ClearGauss );
	UxCreateWidget( CopyGauss );
	UxCreateWidget( separator7 );
	UxCreateWidget( label7 );
	UxCreateWidget( label8 );
	UxCreateWidget( label9 );

	UxPutMenuHelpWidget( GaussMenu, "menu1_top_b4" );

	UxAddCallback( CutxGauss, XmNactivateCallback,
			activateCB_CutxGauss,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( CutyGauss, XmNactivateCallback,
			activateCB_CutyGauss,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( UnzoomGauss, XmNactivateCallback,
			activateCB_UnzoomGauss,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( RedrawGauss, XmNactivateCallback,
			activateCB_RedrawGauss,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( PrintGauss, XmNactivateCallback,
			activateCB_PrintGauss,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( ResidualItem, XmNactivateCallback,
			activateCB_ResidualItem,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( InitialItem, XmNactivateCallback,
			activateCB_InitialItem,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( ComponentsItem, XmNactivateCallback,
			activateCB_ComponentsItem,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( SolutionItem, XmNactivateCallback,
			activateCB_SolutionItem,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( ContinItem, XmNactivateCallback,
			activateCB_ContinItem,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( StatItem, XmNactivateCallback,
			activateCB_StatItem,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( IntegrItem, XmNactivateCallback,
			activateCB_IntegrItem,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( ReturnButton, XmNactivateCallback,
			activateCB_ReturnButton,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( Components, XmNfocusCallback,
			focusCB_Components,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( Components, XmNmodifyVerifyCallback,
			modifyVerifyCB_Components,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( Components, XmNlosingFocusCallback,
			losingFocusCB_Components,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField11, XmNfocusCallback,
			focusCB_textField11,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField11, XmNlosingFocusCallback,
			losingFocusCB_textField11,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField11, XmNmodifyVerifyCallback,
			modifyVerifyCB_textField11,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField12, XmNfocusCallback,
			focusCB_textField12,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField12, XmNlosingFocusCallback,
			losingFocusCB_textField12,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField12, XmNmodifyVerifyCallback,
			modifyVerifyCB_textField12,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField13, XmNfocusCallback,
			focusCB_textField13,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField13, XmNlosingFocusCallback,
			losingFocusCB_textField13,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField13, XmNmodifyVerifyCallback,
			modifyVerifyCB_textField13,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField14, XmNfocusCallback,
			focusCB_textField14,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField14, XmNlosingFocusCallback,
			losingFocusCB_textField14,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField14, XmNmodifyVerifyCallback,
			modifyVerifyCB_textField14,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField15, XmNfocusCallback,
			focusCB_textField15,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField15, XmNlosingFocusCallback,
			losingFocusCB_textField15,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField15, XmNmodifyVerifyCallback,
			modifyVerifyCB_textField15,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField16, XmNfocusCallback,
			focusCB_textField16,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField16, XmNlosingFocusCallback,
			losingFocusCB_textField16,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField16, XmNmodifyVerifyCallback,
			modifyVerifyCB_textField16,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField17, XmNfocusCallback,
			focusCB_textField17,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField17, XmNlosingFocusCallback,
			losingFocusCB_textField17,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField17, XmNmodifyVerifyCallback,
			modifyVerifyCB_textField17,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField18, XmNfocusCallback,
			focusCB_textField18,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField18, XmNlosingFocusCallback,
			losingFocusCB_textField18,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField18, XmNmodifyVerifyCallback,
			modifyVerifyCB_textField18,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField19, XmNfocusCallback,
			focusCB_textField19,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField19, XmNlosingFocusCallback,
			losingFocusCB_textField19,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField19, XmNmodifyVerifyCallback,
			modifyVerifyCB_textField19,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField21, XmNlosingFocusCallback,
			losingFocusCB_textField21,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField21, XmNfocusCallback,
			focusCB_textField21,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField22, XmNlosingFocusCallback,
			losingFocusCB_textField22,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField22, XmNfocusCallback,
			focusCB_textField22,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField23, XmNlosingFocusCallback,
			losingFocusCB_textField23,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField23, XmNfocusCallback,
			focusCB_textField23,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField24, XmNlosingFocusCallback,
			losingFocusCB_textField24,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField24, XmNfocusCallback,
			focusCB_textField24,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField25, XmNlosingFocusCallback,
			losingFocusCB_textField25,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField25, XmNfocusCallback,
			focusCB_textField25,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField26, XmNlosingFocusCallback,
			losingFocusCB_textField26,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField26, XmNfocusCallback,
			focusCB_textField26,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField27, XmNlosingFocusCallback,
			losingFocusCB_textField27,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField27, XmNfocusCallback,
			focusCB_textField27,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField28, XmNlosingFocusCallback,
			losingFocusCB_textField28,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField28, XmNfocusCallback,
			focusCB_textField28,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( textField20, XmNlosingFocusCallback,
			losingFocusCB_textField20,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( textField20, XmNfocusCallback,
			focusCB_textField20,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( GaussUpComponent, XmNactivateCallback,
			activateCB_GaussUpComponent,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( GaussDownComponent, XmNactivateCallback,
			activateCB_GaussDownComponent,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( iterations_text, XmNfocusCallback,
			focusCB_iterations_text,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( iterations_text, XmNmodifyVerifyCallback,
			modifyVerifyCB_iterations_text,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( iterations_text, XmNlosingFocusCallback,
			losingFocusCB_iterations_text,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( iterations_text1, XmNfocusCallback,
			focusCB_iterations_text1,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( iterations_text1, XmNmodifyVerifyCallback,
			modifyVerifyCB_iterations_text1,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( iterations_text1, XmNlosingFocusCallback,
			losingFocusCB_iterations_text1,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( iterations_text2, XmNlosingFocusCallback,
			losingFocusCB_iterations_text2,
			(XtPointer) UxGaussShellContext );
	UxAddCallback( iterations_text2, XmNfocusCallback,
			focusCB_iterations_text2,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( ExecuteGauss, XmNactivateCallback,
			activateCB_ExecuteGauss,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( ClearGauss, XmNactivateCallback,
			activateCB_ClearGauss,
			(XtPointer) UxGaussShellContext );

	UxAddCallback( CopyGauss, XmNactivateCallback,
			activateCB_CopyGauss,
			(XtPointer) UxGaussShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( GaussShell );

	UxMainWindowSetAreas( GaussWindow, GaussMenu, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, GaussTopform );
	return ( GaussShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_GaussShell()
{
	swidget                 rtrn;
	_UxCGaussShell          *UxContext;

	UxGaussShellContext = UxContext =
		(_UxCGaussShell *) UxMalloc( sizeof(_UxCGaussShell) );

	rtrn = _Uxbuild_GaussShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_GaussShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "HelpHelp", action_HelpHelp },
				{ "clearHelpGauss", action_clearHelpGauss },
				{ "HelpGauss", action_HelpGauss }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_GaussShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

