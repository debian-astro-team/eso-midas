/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090825		last modif

===========================================================================*/


/*******************************************************************************
	HelpTopLevel.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxRowCol.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxHelpTopLevel;
	swidget	Uxform20;
	swidget	UxscrolledWindow9;
	swidget	UxrowColumn4;
	swidget	UxhelpText;
	swidget	Uxhelp_print;
	swidget	Uxpb_help_return;
} _UxCHelpTopLevel;

#define HelpTopLevel            UxHelpTopLevelContext->UxHelpTopLevel
#define form20                  UxHelpTopLevelContext->Uxform20
#define scrolledWindow9         UxHelpTopLevelContext->UxscrolledWindow9
#define rowColumn4              UxHelpTopLevelContext->UxrowColumn4
#define helpText                UxHelpTopLevelContext->UxhelpText
#define help_print              UxHelpTopLevelContext->Uxhelp_print
#define pb_help_return          UxHelpTopLevelContext->Uxpb_help_return

static _UxCHelpTopLevel	*UxHelpTopLevelContext;

extern void out_error();



/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_HelpTopLevel();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_help_print( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCHelpTopLevel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxHelpTopLevelContext;
	UxHelpTopLevelContext = UxContext =
			(_UxCHelpTopLevel *) UxGetContext( UxThisWidget );
	{
	out_error("Not yet implemented");
	}
	UxHelpTopLevelContext = UxSaveCtx;
}

static void	activateCB_pb_help_return( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCHelpTopLevel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxHelpTopLevelContext;
	UxHelpTopLevelContext = UxContext =
			(_UxCHelpTopLevel *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("HelpTopLevel"));
	 
	
	}
	UxHelpTopLevelContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_HelpTopLevel()
{
	UxPutBackground( HelpTopLevel, ButtonBackground );
	UxPutIconName( HelpTopLevel, "Extended help" );
	UxPutKeyboardFocusPolicy( HelpTopLevel, "pointer" );
	UxPutTitle( HelpTopLevel, "Extended help" );
	UxPutHeight( HelpTopLevel, 415 );
	UxPutWidth( HelpTopLevel, 708 );
	UxPutY( HelpTopLevel, 28 );
	UxPutX( HelpTopLevel, 20 );

	UxPutBackground( form20, ButtonBackground );
	UxPutHeight( form20, 284 );
	UxPutWidth( form20, 664 );
	UxPutY( form20, 0 );
	UxPutX( form20, 0 );
	UxPutUnitType( form20, "pixels" );
	UxPutResizePolicy( form20, "resize_none" );

	UxPutBackground( scrolledWindow9, WindowBackground );
	UxPutScrollBarPlacement( scrolledWindow9, "bottom_left" );
	UxPutHeight( scrolledWindow9, 372 );
	UxPutWidth( scrolledWindow9, 710 );
	UxPutY( scrolledWindow9, 2 );
	UxPutX( scrolledWindow9, 0 );
	UxPutScrollingPolicy( scrolledWindow9, "automatic" );

	UxPutEntryAlignment( rowColumn4, "alignment_beginning" );
	UxPutBackground( rowColumn4, "white" );
	UxPutOrientation( rowColumn4, "horizontal" );
	UxPutHeight( rowColumn4, 427 );
	UxPutWidth( rowColumn4, 418 );
	UxPutY( rowColumn4, 4 );
	UxPutX( rowColumn4, 25 );

	UxPutHighlightColor( helpText, "Black" );
	UxPutForeground( helpText, TextForeground );
	UxPutCursorPositionVisible( helpText, "false" );
	UxPutEditMode( helpText, "multi_line_edit" );
	UxPutFontList( helpText, SmallFont );
	UxPutBackground( helpText, TextBackground );
	UxPutHeight( helpText, 1076 );
	UxPutWidth( helpText, 676 );
	UxPutY( helpText, 3 );
	UxPutX( helpText, 3 );

	UxPutForeground( help_print, ButtonForeground );
	UxPutHighlightOnEnter( help_print, "true" );
	UxPutHighlightColor( help_print, "Black" );
	UxPutFontList( help_print, BoldTextFont );
	UxPutLabelString( help_print, "Print" );
	UxPutBackground( help_print, ButtonBackground );
	UxPutHeight( help_print, 32 );
	UxPutWidth( help_print, 82 );
	UxPutY( help_print, 378 );
	UxPutX( help_print, 532 );

	UxPutForeground( pb_help_return, CancelForeground );
	UxPutHighlightOnEnter( pb_help_return, "true" );
	UxPutHighlightColor( pb_help_return, "red" );
	UxPutLabelString( pb_help_return, "Quit" );
	UxPutFontList( pb_help_return, BoldTextFont );
	UxPutBackground( pb_help_return, ButtonBackground );
	UxPutHeight( pb_help_return, 32 );
	UxPutWidth( pb_help_return, 82 );
	UxPutY( pb_help_return, 378 );
	UxPutX( pb_help_return, 616 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_HelpTopLevel()
{
	/* Create the swidgets */

	HelpTopLevel = UxCreateTopLevelShell( "HelpTopLevel", NO_PARENT );
	UxPutContext( HelpTopLevel, UxHelpTopLevelContext );

	form20 = UxCreateForm( "form20", HelpTopLevel );
	scrolledWindow9 = UxCreateScrolledWindow( "scrolledWindow9", form20 );
	rowColumn4 = UxCreateRowColumn( "rowColumn4", scrolledWindow9 );
	helpText = UxCreateText( "helpText", rowColumn4 );
	help_print = UxCreatePushButton( "help_print", form20 );
	pb_help_return = UxCreatePushButton( "pb_help_return", form20 );

	_Uxinit_HelpTopLevel();

	/* Create the X widgets */

	UxCreateWidget( HelpTopLevel );
	UxCreateWidget( form20 );
	UxCreateWidget( scrolledWindow9 );
	UxCreateWidget( rowColumn4 );
	UxCreateWidget( helpText );
	UxCreateWidget( help_print );
	UxCreateWidget( pb_help_return );

	UxAddCallback( help_print, XmNactivateCallback,
			activateCB_help_print,
			(XtPointer) UxHelpTopLevelContext );

	UxAddCallback( pb_help_return, XmNactivateCallback,
			activateCB_pb_help_return,
			(XtPointer) UxHelpTopLevelContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( HelpTopLevel );

	return ( HelpTopLevel );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_HelpTopLevel()
{
	swidget                 rtrn;
	_UxCHelpTopLevel        *UxContext;

	UxHelpTopLevelContext = UxContext =
		(_UxCHelpTopLevel *) UxMalloc( sizeof(_UxCHelpTopLevel) );

	rtrn = _Uxbuild_HelpTopLevel();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_HelpTopLevel()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_HelpTopLevel();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

