/* @(#)StatShell.c	19.1 (ES0-DMD) 02/25/03 13:43:41 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	StatShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	int	mumble;
} _UxCStatShell;


static _UxCStatShell	*UxStatShellContext;

swidget	StatShell;
swidget	form7;
swidget	text1;
swidget	return_stat;

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_StatShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_return_stat( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStatShellContext;
	UxStatShellContext = UxContext =
			(_UxCStatShell *) UxGetContext( UxThisWidget );
	{
	extern swidget StatShell;
	UxPopdownInterface(StatShell);
	}
	UxStatShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_StatShell()
{
	UxPutDefaultFontList( StatShell, TextFont );
	UxPutBackground( StatShell, ButtonBackground );
	UxPutTitle( StatShell, "Statistics" );
	UxPutHeight( StatShell, 340 );
	UxPutWidth( StatShell, 456 );
	UxPutY( StatShell, 483 );
	UxPutX( StatShell, 472 );

	UxPutTextFontList( form7, TextFont );
	UxPutLabelFontList( form7, TextFont );
	UxPutButtonFontList( form7, TextFont );
	UxPutBackground( form7, ButtonBackground );
	UxPutHeight( form7, 340 );
	UxPutWidth( form7, 438 );
	UxPutY( form7, 10 );
	UxPutX( form7, 10 );
	UxPutUnitType( form7, "pixels" );
	UxPutResizePolicy( form7, "resize_none" );

	UxPutTopShadowColor( text1, WindowBackground );
	UxPutHighlightColor( text1, TextForeground );
	UxPutCursorPositionVisible( text1, "false" );
	UxPutForeground( text1, TextForeground );
	UxPutRows( text1, 20 );
	UxPutFontList( text1, TextFont );
	UxPutEditable( text1, "false" );
	UxPutBackground( text1, TextBackground );
	UxPutHeight( text1, 290 );
	UxPutWidth( text1, 446 );
	UxPutY( text1, 8 );
	UxPutX( text1, 4 );

	UxPutHighlightColor( return_stat, "Black" );
	UxPutHighlightOnEnter( return_stat, "true" );
	UxPutForeground( return_stat, ButtonForeground );
	UxPutLabelString( return_stat, "Ok" );
	UxPutFontList( return_stat, BoldTextFont );
	UxPutBackground( return_stat, ButtonBackground );
	UxPutHeight( return_stat, 30 );
	UxPutWidth( return_stat, 90 );
	UxPutY( return_stat, 298 );
	UxPutX( return_stat, 28 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_StatShell()
{
	/* Create the swidgets */

	StatShell = UxCreateTopLevelShell( "StatShell", NO_PARENT );
	UxPutContext( StatShell, UxStatShellContext );

	form7 = UxCreateForm( "form7", StatShell );
	text1 = UxCreateText( "text1", form7 );
	return_stat = UxCreatePushButton( "return_stat", form7 );

	_Uxinit_StatShell();

	/* Create the X widgets */

	UxCreateWidget( StatShell );
	UxCreateWidget( form7 );
	UxCreateWidget( text1 );
	UxCreateWidget( return_stat );

	UxAddCallback( return_stat, XmNactivateCallback,
			activateCB_return_stat,
			(XtPointer) UxStatShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( StatShell );

	return ( StatShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_StatShell()
{
	swidget                 rtrn;
	_UxCStatShell           *UxContext;

	UxStatShellContext = UxContext =
		(_UxCStatShell *) UxMalloc( sizeof(_UxCStatShell) );

	rtrn = _Uxbuild_StatShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_StatShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_StatShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

