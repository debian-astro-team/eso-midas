/* @(#)MessageShell.c	19.1 (ES0-DMD) 02/25/03 13:43:40 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	MessageShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxLabel.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxMessageDeparator;
	swidget	UxMessageLabel;
} _UxCMessageShell;

#define MessageDeparator        UxMessageShellContext->UxMessageDeparator
#define MessageLabel            UxMessageShellContext->UxMessageLabel

static _UxCMessageShell	*UxMessageShellContext;

swidget	MessageShell;
swidget	MessageForm;
swidget	OkMessage;

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_MessageShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_OkMessage( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMessageShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMessageShellContext;
	UxMessageShellContext = UxContext =
			(_UxCMessageShell *) UxGetContext( UxThisWidget );
	{
	extern swidget MessageShell;
	UxPopdownInterface(MessageShell);
	}
	UxMessageShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_MessageShell()
{
	UxPutBackground( MessageShell, ButtonBackground );
	UxPutTitle( MessageShell, "Message Window" );
	UxPutHeight( MessageShell, 115 );
	UxPutWidth( MessageShell, 346 );
	UxPutY( MessageShell, 481 );
	UxPutX( MessageShell, 546 );

	UxPutBackground( MessageForm, ButtonBackground );
	UxPutHeight( MessageForm, 340 );
	UxPutWidth( MessageForm, 438 );
	UxPutY( MessageForm, 10 );
	UxPutX( MessageForm, 10 );
	UxPutUnitType( MessageForm, "pixels" );
	UxPutResizePolicy( MessageForm, "resize_none" );

	UxPutHighlightColor( OkMessage, "Black" );
	UxPutHighlightOnEnter( OkMessage, "true" );
	UxPutForeground( OkMessage, ButtonForeground );
	UxPutLabelString( OkMessage, "Ok" );
	UxPutFontList( OkMessage, BoldTextFont );
	UxPutBackground( OkMessage, ButtonBackground );
	UxPutHeight( OkMessage, 30 );
	UxPutWidth( OkMessage, 90 );
	UxPutY( OkMessage, 69 );
	UxPutX( OkMessage, 123 );

	UxPutBackground( MessageDeparator, ButtonBackground );
	UxPutHeight( MessageDeparator, 10 );
	UxPutWidth( MessageDeparator, 386 );
	UxPutY( MessageDeparator, 56 );
	UxPutX( MessageDeparator, 0 );

	UxPutRecomputeSize( MessageLabel, "false" );
	UxPutLabelString( MessageLabel, "message" );
	UxPutAlignment( MessageLabel, "alignment_center" );
	UxPutFontList( MessageLabel, BoldTextFont );
	UxPutBackground( MessageLabel, ButtonBackground );
	UxPutHeight( MessageLabel, 24 );
	UxPutWidth( MessageLabel, 231 );
	UxPutY( MessageLabel, 22 );
	UxPutX( MessageLabel, 66 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_MessageShell()
{
	/* Create the swidgets */

	MessageShell = UxCreateTopLevelShell( "MessageShell", NO_PARENT );
	UxPutContext( MessageShell, UxMessageShellContext );

	MessageForm = UxCreateForm( "MessageForm", MessageShell );
	OkMessage = UxCreatePushButton( "OkMessage", MessageForm );
	MessageDeparator = UxCreateSeparator( "MessageDeparator", MessageForm );
	MessageLabel = UxCreateLabel( "MessageLabel", MessageForm );

	_Uxinit_MessageShell();

	/* Create the X widgets */

	UxCreateWidget( MessageShell );
	UxCreateWidget( MessageForm );
	UxCreateWidget( OkMessage );
	UxCreateWidget( MessageDeparator );
	UxCreateWidget( MessageLabel );

	UxAddCallback( OkMessage, XmNactivateCallback,
			activateCB_OkMessage,
			(XtPointer) UxMessageShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( MessageShell );

	return ( MessageShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_MessageShell()
{
	swidget                 rtrn;
	_UxCMessageShell        *UxContext;

	UxMessageShellContext = UxContext =
		(_UxCMessageShell *) UxMalloc( sizeof(_UxCMessageShell) );

	rtrn = _Uxbuild_MessageShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_MessageShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_MessageShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

