/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	AliceShell.c


.VERSION
 090420         last modif

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "UxLib.h"
#include "UxDrArea.h"
#include "UxSep.h"
#include "UxArrB.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxTogB.h"
#include "UxCascB.h"
#include "UxPushB.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>
#include <alice_help.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxAliceWindow;
	swidget	UxTopmenu;
	swidget	UxFilePane;
	swidget	UxReadItem;
	swidget	UxSaveAsItem;
	swidget	UxSaveItem;
	swidget	UxFileMenu;
	swidget	UxFramePane;
	swidget	UxCutxItem;
	swidget	UxCutyItem;
	swidget	UxUnzoomItem;
	swidget	UxRedrawItem;
	swidget	UxPrintPane;
	swidget	UxFrameMenu;
	swidget	UxContinPane;
	swidget	UxBeginItem;
	swidget	UxAddItem;
	swidget	UxFitItem;
	swidget	UxReFitItem;
	swidget	UxContinunmMenu;
	swidget	UxFilterPane;
	swidget	UxSmoothItem;
	swidget	UxMedianItem;
	swidget	UxUndoItem;
	swidget	UxFilterMenu;
	swidget	UxLabelPane;
	swidget	UxAxisItem;
	swidget	UxCursorItem;
	swidget	UxLoadItem;
	swidget	UxClearItem;
	swidget	UxLabelMenu;
	swidget	UxUtilsPane;
	swidget	UxGetcursorItem;
	swidget	UxSavetmpItem;
	swidget	UxPrinterItem;
	swidget	UxUtilsMenu;
	swidget	UxOptionPane;
	swidget	UxPlotRadio;
	swidget	UxStraightToggle;
	swidget	UxHistoToggle;
	swidget	UxPlotItem;
	swidget	UxContinRadio;
	swidget	UxPolynomialItem;
	swidget	UxSplineItem;
	swidget	UxContinuumItem;
	swidget	UxPrintRadio;
	swidget	UxNormalToggle;
	swidget	UxPortraitToggle;
	swidget	UxPrintItem;
	swidget	UxOptionMenu;
	swidget	UxHelpPane;
	swidget	UxContext;
	swidget	UxOnHelp;
	swidget	UxHelpMenu;
	swidget	UxQuitPane;
	swidget	UxQuitItem;
	swidget	UxQuitMenu;
	swidget	UxAliceForm1;
	swidget	UxCurrLine;
	swidget	UxLinestep;
	swidget	UxarrowButton1;
	swidget	UxarrowButton2;
	swidget	Uxdegree_text;
	swidget	UxarrowButton3;
	swidget	UxarrowButton4;
	swidget	UxAliceForm2;
	swidget	UxGaussButton;
	swidget	UxIntegrateButton;
	swidget	UxRebinButton;
	swidget	UxRebinButton1;
	swidget	Uxseparator1;
	swidget	Uxwidth_text;
	swidget	UxarrowButton11;
	swidget	UxarrowButton12;
	swidget	Uxhelp_text_top;
	swidget	Uxseparator3;
	swidget	Uxseparator8;
	swidget	Uxseparator9;
	swidget	Uxseparator10;
	swidget	UxGaussDrawingArea;
	swidget	Uxseparator11;
	swidget	Uxseparator12;
	swidget	Uxseparator13;
	swidget	Uxseparator14;
	swidget	UxAutoFitTButton;
} _UxCAliceShell;

#define AliceWindow             UxAliceShellContext->UxAliceWindow
#define Topmenu                 UxAliceShellContext->UxTopmenu
#define FilePane                UxAliceShellContext->UxFilePane
#define ReadItem                UxAliceShellContext->UxReadItem
#define SaveAsItem              UxAliceShellContext->UxSaveAsItem
#define SaveItem                UxAliceShellContext->UxSaveItem
#define FileMenu                UxAliceShellContext->UxFileMenu
#define FramePane               UxAliceShellContext->UxFramePane
#define CutxItem                UxAliceShellContext->UxCutxItem
#define CutyItem                UxAliceShellContext->UxCutyItem
#define UnzoomItem              UxAliceShellContext->UxUnzoomItem
#define RedrawItem              UxAliceShellContext->UxRedrawItem
#define PrintPane               UxAliceShellContext->UxPrintPane
#define FrameMenu               UxAliceShellContext->UxFrameMenu
#define ContinPane              UxAliceShellContext->UxContinPane
#define BeginItem               UxAliceShellContext->UxBeginItem
#define AddItem                 UxAliceShellContext->UxAddItem
#define FitItem                 UxAliceShellContext->UxFitItem
#define ReFitItem               UxAliceShellContext->UxReFitItem
#define ContinunmMenu           UxAliceShellContext->UxContinunmMenu
#define FilterPane              UxAliceShellContext->UxFilterPane
#define SmoothItem              UxAliceShellContext->UxSmoothItem
#define MedianItem              UxAliceShellContext->UxMedianItem
#define UndoItem                UxAliceShellContext->UxUndoItem
#define FilterMenu              UxAliceShellContext->UxFilterMenu
#define LabelPane               UxAliceShellContext->UxLabelPane
#define AxisItem                UxAliceShellContext->UxAxisItem
#define CursorItem              UxAliceShellContext->UxCursorItem
#define LoadItem                UxAliceShellContext->UxLoadItem
#define ClearItem               UxAliceShellContext->UxClearItem
#define LabelMenu               UxAliceShellContext->UxLabelMenu
#define UtilsPane               UxAliceShellContext->UxUtilsPane
#define GetcursorItem           UxAliceShellContext->UxGetcursorItem
#define SavetmpItem             UxAliceShellContext->UxSavetmpItem
#define PrinterItem             UxAliceShellContext->UxPrinterItem
#define UtilsMenu               UxAliceShellContext->UxUtilsMenu
#define OptionPane              UxAliceShellContext->UxOptionPane
#define PlotRadio               UxAliceShellContext->UxPlotRadio
#define StraightToggle          UxAliceShellContext->UxStraightToggle
#define HistoToggle             UxAliceShellContext->UxHistoToggle
#define PlotItem                UxAliceShellContext->UxPlotItem
#define ContinRadio             UxAliceShellContext->UxContinRadio
#define PolynomialItem          UxAliceShellContext->UxPolynomialItem
#define SplineItem              UxAliceShellContext->UxSplineItem
#define ContinuumItem           UxAliceShellContext->UxContinuumItem
#define PrintRadio              UxAliceShellContext->UxPrintRadio
#define NormalToggle            UxAliceShellContext->UxNormalToggle
#define PortraitToggle          UxAliceShellContext->UxPortraitToggle
#define PrintItem               UxAliceShellContext->UxPrintItem
#define OptionMenu              UxAliceShellContext->UxOptionMenu
#define HelpPane                UxAliceShellContext->UxHelpPane
#define Context                 UxAliceShellContext->UxContext
#define OnHelp                  UxAliceShellContext->UxOnHelp
#define HelpMenu                UxAliceShellContext->UxHelpMenu
#define QuitPane                UxAliceShellContext->UxQuitPane
#define QuitItem                UxAliceShellContext->UxQuitItem
#define QuitMenu                UxAliceShellContext->UxQuitMenu
#define AliceForm1              UxAliceShellContext->UxAliceForm1
#define CurrLine                UxAliceShellContext->UxCurrLine
#define Linestep                UxAliceShellContext->UxLinestep
#define arrowButton1            UxAliceShellContext->UxarrowButton1
#define arrowButton2            UxAliceShellContext->UxarrowButton2
#define degree_text             UxAliceShellContext->Uxdegree_text
#define arrowButton3            UxAliceShellContext->UxarrowButton3
#define arrowButton4            UxAliceShellContext->UxarrowButton4
#define AliceForm2              UxAliceShellContext->UxAliceForm2
#define GaussButton             UxAliceShellContext->UxGaussButton
#define IntegrateButton         UxAliceShellContext->UxIntegrateButton
#define RebinButton             UxAliceShellContext->UxRebinButton
#define RebinButton1            UxAliceShellContext->UxRebinButton1
#define separator1              UxAliceShellContext->Uxseparator1
#define width_text              UxAliceShellContext->Uxwidth_text
#define arrowButton11           UxAliceShellContext->UxarrowButton11
#define arrowButton12           UxAliceShellContext->UxarrowButton12
#define help_text_top           UxAliceShellContext->Uxhelp_text_top
#define separator3              UxAliceShellContext->Uxseparator3
#define separator8              UxAliceShellContext->Uxseparator8
#define separator9              UxAliceShellContext->Uxseparator9
#define separator10             UxAliceShellContext->Uxseparator10
#define GaussDrawingArea        UxAliceShellContext->UxGaussDrawingArea
#define separator11             UxAliceShellContext->Uxseparator11
#define separator12             UxAliceShellContext->Uxseparator12
#define separator13             UxAliceShellContext->Uxseparator13
#define separator14             UxAliceShellContext->Uxseparator14
#define AutoFitTButton          UxAliceShellContext->UxAutoFitTButton


extern void SetFileList(), save_TMP(), noframe_error();
extern void DisplayExtendedHelp(), InitWindows(), clearLabels();
extern void vloc();
 
extern char *find_extended_help();

extern int AppendDialogText(); 
extern int integrate(), mean_filter(), median_filter();


static _UxCAliceShell	*UxAliceShellContext;

swidget	AliceShell;
swidget	Line;
swidget	Degree;
swidget	Line1;
swidget	Line2;
swidget	Line3;
swidget	Line4;
swidget	arrowButton8;
swidget	arrowButton6;
swidget	arrowButton5;
swidget	arrowButton7;
swidget	Step;
swidget	step_text;
swidget	Cut_x;
swidget	Cut_y;
swidget	Move;
swidget	Unzoom;
swidget	AutoFitLabel;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*HELPTopTab = "#override\n\
<EnterWindow>:HelpTop1()\n\
<LeaveWindow>:clearHelpTop()\n";

static char	*TextTab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
 <Key>osfDelete:delete-previous-character()\n";

static char	*Help = "#override\n\
<Btn3Down>:HelpHelp()\n\
<EnterWindow>:HelpTop1()\n\
<LeaveWindow>:clearHelpTop()\n";

/*
static char	*transTable1 = "#override\n\
<EnterWindow>:HelpTop2()\n\
<LeaveWindow>:clearHelpTop()\n";
*/

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_AliceShell();

void  spec(), vdef_wspec(), move_zoom(), box(), plot_over(), get_cursor();
void init_values(), load_image(), load_points(), draw_zoom();
void add_fit(), zoom(), print_plot(), read_image();
void out_error(), save_file(), plot_fit(), plot_spline();

extern int   auto_fit(), SCTPUT();


/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_HelpTop2( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	 if( UxWidget == UxGetWidget(arrowButton1))
	  {UxPutText(UxFindSwidget("help_text_top"),IMAGEUPAR);
	   UxPutText(UxFindSwidget("HelpTopLevel"),IMAGEUPAR);}
	 else if( UxWidget == UxGetWidget(arrowButton2))
	  UxPutText(UxFindSwidget("help_text_top"),IMAGEDNAR);
	 else if( UxWidget == UxGetWidget(arrowButton11))
	  UxPutText(UxFindSwidget("help_text_top"),FILTERUPAR);
	 else if( UxWidget == UxGetWidget(arrowButton12))
	  UxPutText(UxFindSwidget("help_text_top"),FILTERDNAR);
	 else if( UxWidget == UxGetWidget(arrowButton3))
	  UxPutText(UxFindSwidget("help_text_top"),FITUPAR);
	 else if( UxWidget == UxGetWidget(arrowButton4))
	  UxPutText(UxFindSwidget("help_text_top"),FITDNAR);
	 else if( UxWidget == UxGetWidget(AutoFitTButton))
	  UxPutText(UxFindSwidget("help_text_top"),AUTOFIT);
	 else if( UxWidget == UxGetWidget(GaussButton))
	  UxPutText(UxFindSwidget("help_text_top"),GAUSSINTER);
	 else if( UxWidget == UxGetWidget(IntegrateButton))
	  UxPutText(UxFindSwidget("help_text_top"),INTEGRATE);
	 else if( UxWidget == UxGetWidget(Cut_x))
	  UxPutText(UxFindSwidget("help_text_top"),CUTX);
	else if( UxWidget == UxGetWidget(Cut_y))
	  UxPutText(UxFindSwidget("help_text_top"),CUTY);
	else if( UxWidget == UxGetWidget(Move))
	  UxPutText(UxFindSwidget("help_text_top"),MOVE);
	else if( UxWidget == UxGetWidget(Unzoom))
	  UxPutText(UxFindSwidget("help_text_top"),UNZOOM);
	else if( UxWidget == UxGetWidget(arrowButton7))
	  UxPutText(UxFindSwidget("help_text_top"),ZOOMUPARROW);
	 else if( UxWidget == UxGetWidget(arrowButton8))
	  UxPutText(UxFindSwidget("help_text_top"),ZOOMLFARROW );
	 else if( UxWidget == UxGetWidget(arrowButton6))
	  UxPutText(UxFindSwidget("help_text_top"), ZOOMDNARROW);
	 else if( UxWidget == UxGetWidget(arrowButton5))
	  UxPutText(UxFindSwidget("help_text_top"), ZOOMRTARROW);
	else if( UxWidget == UxGetWidget(step_text))
	  UxPutText(UxFindSwidget("help_text_top"), ZOOMSTEP);
	 else if( UxWidget == UxGetWidget(RebinButton1))
	  UxPutText(UxFindSwidget("help_text_top"), REBIN);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	action_HelpHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxAliceShellContext = UxSaveCtx;
}

static void	action_clearHelpTop( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{UxPutText(UxFindSwidget("help_text_top"),"");
	 UxPutText(UxFindSwidget("HelpTopLevel"),"");}
	UxAliceShellContext = UxSaveCtx;
}

static void	action_HelpTop1( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	 if( UxWidget == UxGetWidget(arrowButton1))
	  {UxPutText(UxFindSwidget("help_text_top"),IMAGEUPAR);
	   UxPutText(UxFindSwidget("HelpTopLevel"),IMAGEUPAR);}
	 else if( UxWidget == UxGetWidget(arrowButton2))
	  UxPutText(UxFindSwidget("help_text_top"),IMAGEDNAR);
	 else if( UxWidget == UxGetWidget(arrowButton11))
	  UxPutText(UxFindSwidget("help_text_top"),FILTERUPAR);
	 else if( UxWidget == UxGetWidget(arrowButton12))
	  UxPutText(UxFindSwidget("help_text_top"),FILTERDNAR);
	 else if( UxWidget == UxGetWidget(arrowButton3))
	  UxPutText(UxFindSwidget("help_text_top"),FITUPAR);
	 else if( UxWidget == UxGetWidget(arrowButton4))
	  UxPutText(UxFindSwidget("help_text_top"),FITDNAR);
	 else if( UxWidget == UxGetWidget(AutoFitTButton))
	  UxPutText(UxFindSwidget("help_text_top"),AUTOFIT);
	 else if( UxWidget == UxGetWidget(GaussButton))
	  UxPutText(UxFindSwidget("help_text_top"),GAUSSINTER);
	 else if( UxWidget == UxGetWidget(IntegrateButton))
	  UxPutText(UxFindSwidget("help_text_top"),INTEGRATE);
	 else if( UxWidget == UxGetWidget(Cut_x))
	  UxPutText(UxFindSwidget("help_text_top"),CUTX);
	else if( UxWidget == UxGetWidget(Cut_y))
	  UxPutText(UxFindSwidget("help_text_top"),CUTY);
	else if( UxWidget == UxGetWidget(Move))
	  UxPutText(UxFindSwidget("help_text_top"),MOVE);
	else if( UxWidget == UxGetWidget(Unzoom))
	  UxPutText(UxFindSwidget("help_text_top"),UNZOOM);
	else if( UxWidget == UxGetWidget(arrowButton7))
	  UxPutText(UxFindSwidget("help_text_top"),ZOOMUPARROW);
	 else if( UxWidget == UxGetWidget(arrowButton8))
	  UxPutText(UxFindSwidget("help_text_top"),ZOOMLFARROW );
	 else if( UxWidget == UxGetWidget(arrowButton6))
	  UxPutText(UxFindSwidget("help_text_top"), ZOOMDNARROW);
	 else if( UxWidget == UxGetWidget(arrowButton5))
	  UxPutText(UxFindSwidget("help_text_top"), ZOOMRTARROW);
	else if( UxWidget == UxGetWidget(step_text))
	  UxPutText(UxFindSwidget("help_text_top"), ZOOMSTEP);
	 else if( UxWidget == UxGetWidget(RebinButton))
	  UxPutText(UxFindSwidget("help_text_top"), REBIN);
	}
	UxAliceShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_ReadItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	extern swidget ListPopup;
	extern swidget scrolledList1;
	UxPopupInterface(ListPopup,no_grab);
	SetFileList( UxGetWidget(scrolledList1),1, "*.bdf" );
	if(!specInputFrame)
	 {
	 AppendDialogText("cre/gra 0 1000,400,40,500");
	 AppendDialogText("SET/GCURSOR ? C_HAIR");
	 }
	specInputFrame = TRUE;
	caseList = CASEBDF;
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_SaveAsItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	UxPopupInterface(UxFindSwidget("SaveAsShell"),no_grab);
	 
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_SaveItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	save_file(specSaveName);
	save_TMP();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_CutxItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 zoom(MOVE_X2);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_CutyItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 zoom(MOVE_Y2);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_UnzoomItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	{
	 specXcen = specXcenw2;
	 specYcen = specYcenw2;
	 specDx = specDxw2;
	 specDy = specDyw2;
	 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	      specYcen-specDy,specYcen+specDy,PLOTMODE);
	 if(OverPlotMode)
	   plot_over();
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_RedrawItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 {
	  spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	        specYcen-specDy,specYcen+specDy,PLOTMODE);
	  if(OverPlotMode)
	     plot_over();
	  }
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_PrintPane( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 {
	 if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("NormalToggle"))))
	  print_plot(NORMALPRINT,PrinterName);
	 else
	  print_plot(PORTRAITPRINT,PrinterName);
	 }
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_BeginItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	{
	 gaussNumOfFitData=0;
	  fitPairNum = 0;
	 add_fit(6);
	 if(fitMode == POLYFITMODE)
	  plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);
	 else
	  plot_spline(gaussNumOfFitData,6);
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_AddItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	{
	add_fit(6);
	if(fitMode == POLYFITMODE) 
	 plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);
	else
	 plot_spline(gaussNumOfFitData,6);
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_FitItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	{
	if(fitAddFit)
	 {
	  if(fitMode == POLYFITMODE)
	   plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);
	  else
	   plot_spline(gaussNumOfFitData,6);
	 }
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_ReFitItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	{
	if(fitAddFit)
	 {
	 auto_fit(0);
	 plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);
	 }
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_SmoothItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	float y2[MAXVALUES];
	int i;
	if(specInputFrame)
	{
	mean_filter(specY,specNpix[0],filterWindWidth,y2);
	for(i=0;i<specNpix[0];i++)
	 specY[i]=y2[i]; 
	median_filter(specY,specNpix[0],filterWindWidth,y2);
	load_points();
	draw_zoom();
	 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	specYcen-specDy,specYcen+specDy,PLOTMODE);
	if(OverPlotMode)
	 plot_over();
	XtSetSensitive(UxGetWidget(UxFindSwidget("UndoItem")),1);
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_MedianItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	float y2[MAXVALUES];
	int i;
	if(specInputFrame)
	{
	median_filter(specY,specNpix[0],filterWindWidth,y2);
	for(i=0;i<specNpix[0];i++)
	 specY[i]=y2[i]; 
	median_filter(specY,specNpix[0],filterWindWidth,y2);
	load_points();
	draw_zoom();
	 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	      specYcen-specDy,specYcen+specDy,PLOTMODE);
	if(OverPlotMode)
	 plot_over();
	XtSetSensitive(UxGetWidget(UxFindSwidget("UndoItem")),1);
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_UndoItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	 if(specInputFrame)
	  {
	   read_image(specLastName);
	   XtSetSensitive(UxGetWidget(UxFindSwidget("UndoItem")),0);
	  }
	 else
	  noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_AxisItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	char out[20];
	if(specInputFrame)
	 {
	 sprintf(out,"%s",plotTitle);
	 UxPutText(UxFindSwidget("Titletext"),out);
	 sprintf(out,"%s",plotLabelX);
	 UxPutText(UxFindSwidget("labelxtext"),out);
	 sprintf(out,"%s",plotLabelY);
	 UxPutText(UxFindSwidget("labelytext"),out);
	 UxPopupInterface(UxFindSwidget("LabelOptions"),no_grab); 
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_CursorItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	  UxPopupInterface(UxFindSwidget("LabelCursor"),no_grab);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_LoadItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	extern swidget ListPopup;
	extern swidget scrolledList1;
	if(specInputFrame)
	 {
	  UxPopupInterface(ListPopup,no_grab);
	  SetFileList( UxGetWidget(scrolledList1), 1,"*.prg" );
	  caseList = CASEPRG;
	 }
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_ClearItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 {
	  clearLabels();
	  system("rm -f TMPalice.prg");
	  spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	        specYcen-specDy,specYcen+specDy,PLOTMODE);
	  if(OverPlotMode)
	   plot_over();
	 }
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_GetcursorItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	float x1,y1;
	int key;
	char output[80];
	key = 1;
	if(specInputFrame)
	{
	 SCTPUT("\n");
	 SCTPUT("\t X-axis\t\t Y-axis");
	 SCTPUT("\t---------------------------");
	 while(key == 1)
	 {
	    vloc(&x1,&y1,&key);
	    if(key == 1)
	    {
	    sprintf(output,"\t%f\t%f",x1,y1);
	    SCTPUT(output);
	    }
	 }
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_SavetmpItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	 UxPopupInterface(UxFindSwidget("Savefiles"),no_grab);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_PrinterItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	 UxPopupInterface(UxFindSwidget("Printer"),no_grab);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	valueChangedCB_StraightToggle( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	plotMode = LINEMODE;
	UxAliceShellContext = UxSaveCtx;
}

static void	valueChangedCB_HistoToggle( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	plotMode = HISTMODE;
	UxAliceShellContext = UxSaveCtx;
}

static void	valueChangedCB_PolynomialItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	fitMode = POLYFITMODE;
	XtSetSensitive(UxGetWidget(UxFindSwidget("arrowButton4")),1);
	XtSetSensitive(UxGetWidget(UxFindSwidget("arrowButton3")),1);
	XtSetSensitive(UxGetWidget(UxFindSwidget("degree_text")),1);
	XtSetSensitive(UxGetWidget(UxFindSwidget("ReFitItem")),1);
	UxAliceShellContext = UxSaveCtx;
}

static void	valueChangedCB_SplineItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	fitMode = SPLINEFITMODE;
	XtSetSensitive(UxGetWidget(UxFindSwidget("arrowButton3")),0);
	XtSetSensitive(UxGetWidget(UxFindSwidget("arrowButton4")),0);
	XtSetSensitive(UxGetWidget(UxFindSwidget("degree_text")),0);
	 XtSetSensitive(UxGetWidget(UxFindSwidget("ReFitItem")),0);
	UxAliceShellContext = UxSaveCtx;
}

static void	valueChangedCB_NormalToggle( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("NormalToggle"))))
	{
	 AppendDialogText("del/gra 0");
	 AppendDialogText("cre/gra 0 1000,400,40,500");
	 AppendDialogText("SET/GCURSOR ? C_HAIR");
	}
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	valueChangedCB_PortraitToggle( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("PortraitToggle"))))
	{
	 AppendDialogText("del/gra 0");
	 AppendDialogText("cre/gra 0 500,800,10,80");
	 AppendDialogText("SET/GCURSOR ? C_HAIR");
	}
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_Context( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{ UxPopupInterface(UxFindSwidget("HelpTopLevel"),no_grab);
	 UxPutText(UxFindSwidget("helpText"),find_extended_help("CONTEXT"));
	 }
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_OnHelp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	UxPopupInterface(UxFindSwidget("HelpTopLevel"),no_grab);
	UxPutText(UxFindSwidget("helpText"),find_extended_help("HELP"));
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_QuitItem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	system("rm -f alicel.plt alice.plt TMPalice.* TMPcont.bdf pscrplot.0");
	exit(0); 
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	focusCB_CurrLine( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_top"),IMAGELINE);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_CurrLine( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 specModLineNum = TRUE;
	 
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	losingFocusCB_CurrLine( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	int aux_specLineNum;
	char out[6];
	if(specInputFrame )
	{
	if( specModLineNum)
	{ specModLineNum = FALSE;
	  sscanf(UxGetText(CurrLine),"%d",&aux_specLineNum);
	   if(aux_specLineNum != specLineNum)
	   {
	   if ((aux_specLineNum > 0) & 
               ((aux_specLineNum + specLineStep -1) <= specDim))
	    {
	     specLineNum = aux_specLineNum;
	     read_image(specImageName);
	    }
	   else   
	   {sprintf(out,"%d",specLineNum);
	    XmTextSetString(UxGetWidget(UxFindSwidget("CurrLine")),out);
	    out_error("Invalid Line Number");
	   }
	  }
	 } 
	}
	else if(specModLineNum)
	 noframe_error();
	 UxPutText(UxFindSwidget("help_text_top")," ");
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	focusCB_Linestep( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_top"),IMAGEWIDE);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_Linestep( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	extern int specModLineStep;
	if(specInputFrame)
	 specModLineStep = TRUE;
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	losingFocusCB_Linestep( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	extern int specModLineStep;
	int aux;
	char out[20];
	if(specInputFrame & specModLineStep)
	{
	if( specModLineStep)
	{ specModLineStep = FALSE;
	  sscanf(UxGetText(Linestep),"%d",&aux);
	  if (( aux <= specDim) & (aux >0))
	  {
	   specLineStep = aux;
	   read_image(specImageName);
	  }
	  else
	  {
	   sprintf(out,"%d",specLineStep);
	   XmTextSetString(UxGetWidget(UxFindSwidget("Linestep")),out);
	   out_error("Invalid Line Step");
	  }
	 }
	}
	else if(specModLineStep)
	 noframe_error();
	UxPutText(UxFindSwidget("help_text_top"),"");
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	char line[20];
	if(specInputFrame)
	{
	if(specLineNum + specLineStep  <=  specDim)
	 specLineNum+=specLineStep;
	else 
	 specLineNum = 1;
	sprintf(line,"%d",specLineNum);
	UxPutText(CurrLine,line);
	read_image(specImageName);
	specModLineNum = FALSE;
	specModLineStep = FALSE;
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	char line[20];
	if(specInputFrame)
	{
	if(specLineNum - specLineStep > 0)
	 specLineNum-=specLineStep;
	else
	 specLineNum = specDim - specLineStep +1;
	sprintf(line,"%d",specLineNum);
	UxPutText(CurrLine,line);
	read_image(specImageName);
	specModLineNum = FALSE;
	specModLineStep = FALSE;
	}
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	focusCB_degree_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_top"),FITDEGREE);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_degree_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	fitModFitDeg = TRUE;
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	losingFocusCB_degree_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	int aux_fitDegree;
	char out[3];
	if( fitModFitDeg)
	{ fitModFitDeg = FALSE;
	  sscanf(UxGetText(degree_text),"%d",&aux_fitDegree);
	  if(aux_fitDegree >0 && aux_fitDegree < 16)
	   fitDegree = aux_fitDegree;
	  else
	  {
	 sprintf(out,"%d",fitDegree);
	 XmTextSetString(UxGetWidget(UxFindSwidget("degree_text")),out);
	  }
	}
	UxPutText(UxFindSwidget("help_text_top"),"");
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	char deg[20];
	if(fitDegree < 15)
	 fitDegree++;
	else 
	 fitDegree = 1;
	sprintf(deg,"%d",fitDegree);
	UxPutText(degree_text,deg);
	specModLineNum = FALSE;
	specModLineStep = FALSE;
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	char deg[20];
	if(fitDegree > 1)
	 fitDegree--;
	else 
	 fitDegree = 15;
	sprintf(deg,"%d",fitDegree);
	UxPutText(degree_text,deg);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	helpCB_GaussButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_top"),"Help Button");
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_GaussButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 UxPopupInterface(UxFindSwidget("GaussShell"),no_grab);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_IntegrateButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 integrate();
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	helpCB_RebinButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_top"),"Help Button");
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_RebinButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	UxPopupInterface(UxFindSwidget("RebinShell"),no_grab);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	helpCB_RebinButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_top"),"Help Button");
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_RebinButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	UxPopupInterface(UxFindSwidget("OverPlotShell"),no_grab);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	focusCB_width_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
#include <alice_help.h>
	UxPutText(UxFindSwidget("help_text_top"),FILTERWIDE);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_width_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	filterModWindWidth = TRUE;
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	losingFocusCB_width_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	int aux_width;
	char out[8];
	if( filterModWindWidth)
	{ filterModWindWidth = FALSE;
	  sscanf(UxGetText(width_text),"%d",&aux_width);
	  if(aux_width >1 && aux_width < specNpix[0]/2)
	   filterWindWidth = aux_width;
	  else
	  {
	 sprintf(out,"%d",filterWindWidth);
	 XmTextSetString(UxGetWidget(UxFindSwidget("width_text")),out);
	 out_error("Invalid Window Width");
	  }
	}
	UxPutText(UxFindSwidget("help_text_top"),"");
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	char out[20];
	if(filterWindWidth < specNpix[0]/2)
	 filterWindWidth++;
	else
	 out_error("Invalid Window Width");
	sprintf(out,"%d",filterWindWidth);
	UxPutText(width_text,out);
	specModLineNum = FALSE;
	specModLineStep = FALSE;
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	char out[20];
	if(filterWindWidth > 2)
	 filterWindWidth--;
	else
	 out_error("Invalid Window Width");
	sprintf(out,"%d",filterWindWidth);
	UxPutText(width_text,out);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	exposeCB_GaussDrawingArea( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	Window window;
	Display *display;
	display = XtDisplay(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
	window = XtWindow(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
	XClearWindow(display,window);
	draw_zoom();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	createCB_GaussDrawingArea( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
#define Cross_cursor 30
	Window window;
	Display *display;
	XSetWindowAttributes attributes;
	
	display = XtDisplay(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
	window = XtWindow(UxGetWidget(UxFindSwidget("GaussDrawingArea")));
	
	attributes.cursor = XCreateFontCursor(display,Cross_cursor);
	 XChangeWindowAttributes(display,window,CWCursor,&attributes);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	 if(specInputFrame)
	 zoom(STEPDN_X);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame) 
	 zoom(STEPDN_Y);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 zoom(STEPUP_X);
	else
	 noframe_error();
	
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_arrowButton7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 zoom(STEPUP_Y);
	else
	noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	focusCB_step_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("help_text_top"),ZOOMSTEP);
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_step_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	filterModStepWin = TRUE;
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	losingFocusCB_step_text( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if( filterModStepWin)
	{ filterModStepWin = FALSE;
	  sscanf(UxGetText(step_text),"%f",&specStepWin);
	}
	UxPutText(UxFindSwidget("help_text_top"),"");
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_Cut_x( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 zoom(MOVE_X1);  
	else
	 noframe_error();
	
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_Cut_y( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 zoom(MOVE_Y1);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_Move( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 zoom(MOVE_WIN);
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

static void	activateCB_Unzoom( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAliceShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAliceShellContext;
	UxAliceShellContext = UxContext =
			(_UxCAliceShell *) UxGetContext( UxThisWidget );
	{
	if(specInputFrame)
	 {
	  box(specXcenw2-specDxw2,specXcenw2+specDxw2,
	        specYcenw2-specDyw2,specYcenw2+specDyw2,GXequiv );
	  specXmin = specX[0]; specXmax = specX[specNpix[0]-1];
	  specYmin = specYmax = specY[0];
	 for(i = 0; i < specNpix[0] ; i++)
	 {
	  if(specY[i] > specYmax)
	     specYmax = specY[i];
	  if(specY[i] < specYmin)
	     specYmin = specY[i];
	 }
	 specXcen=specXmin+(specXmax-specXmin)/2;
	 specYcen=specYmin+(specYmax-specYmin)/2;
	 specDx = specXmax - specXcen;
	 specDy = specYmax - specYcen;
	 specXcenw2 =specXcen;
	 specYcenw2 =specYcen;
	 specDxw2 =specDx;
	 specDyw2 =specDy;
	 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	       specYcen-specDy,specYcen+specDy,PLOTMODE);
	 if(OverPlotMode)
	   plot_over();
	 draw_zoom();
	 }
	else
	 noframe_error();
	}
	UxAliceShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_AliceShell()
{
	UxPutDefaultFontList( AliceShell, BoldTextFont );
	UxPutTitle( AliceShell, "Alice - Spectrum Analysis" );
	UxPutKeyboardFocusPolicy( AliceShell, "pointer" );
	UxPutHeight( AliceShell, 469 );
	UxPutWidth( AliceShell, 561 );
	UxPutY( AliceShell, 425 );
	UxPutX( AliceShell, 478 );

	UxPutBackground( AliceWindow, WindowBackground );
	UxPutHeight( AliceWindow, 72 );
	UxPutWidth( AliceWindow, 106 );
	UxPutY( AliceWindow, 0 );
	UxPutX( AliceWindow, 13 );
	UxPutUnitType( AliceWindow, "pixels" );

	UxPutMenuAccelerator( Topmenu, "<KeyUp>F10" );
	UxPutBackground( Topmenu, MenuBackground );
	UxPutRowColumnType( Topmenu, "menu_bar" );

	UxPutForeground( FilePane, MenuForeground );
	UxPutHighlightColor( FilePane, "Black" );
	UxPutBackground( FilePane, MenuBackground );
	UxPutRowColumnType( FilePane, "menu_pulldown" );

	UxPutForeground( ReadItem, MenuForeground );
	UxPutFontList( ReadItem, BoldTextFont );
	UxPutBackground( ReadItem, MenuBackground );
	UxPutMnemonic( ReadItem, "O" );
	UxPutLabelString( ReadItem, "Open ..." );

	UxPutForeground( SaveAsItem, MenuForeground );
	UxPutFontList( SaveAsItem, BoldTextFont );
	UxPutMnemonic( SaveAsItem, "A" );
	UxPutBackground( SaveAsItem, MenuBackground );
	UxPutLabelString( SaveAsItem, "Save As ..." );

	UxPutForeground( SaveItem, MenuForeground );
	UxPutFontList( SaveItem, BoldTextFont );
	UxPutBackground( SaveItem, MenuBackground );
	UxPutMnemonic( SaveItem, "S" );
	UxPutLabelString( SaveItem, "Save" );

	UxPutHighlightColor( FileMenu, "Black" );
	UxPutForeground( FileMenu, MenuForeground );
	UxPutFontList( FileMenu, BoldTextFont );
	UxPutBackground( FileMenu, MenuBackground );
	UxPutMnemonic( FileMenu, "F" );
	UxPutLabelString( FileMenu, "File" );

	UxPutForeground( FramePane, MenuForeground );
	UxPutHighlightColor( FramePane, "Black" );
	UxPutBackground( FramePane, MenuBackground );
	UxPutRowColumnType( FramePane, "menu_pulldown" );

	UxPutForeground( CutxItem, MenuForeground );
	UxPutFontList( CutxItem, BoldTextFont );
	UxPutBackground( CutxItem, MenuBackground );
	UxPutMnemonic( CutxItem, "X" );
	UxPutLabelString( CutxItem, "Cut X" );

	UxPutForeground( CutyItem, MenuForeground );
	UxPutFontList( CutyItem, BoldTextFont );
	UxPutBackground( CutyItem, MenuBackground );
	UxPutMnemonic( CutyItem, "Y" );
	UxPutLabelString( CutyItem, "Cut Y" );

	UxPutForeground( UnzoomItem, MenuForeground );
	UxPutFontList( UnzoomItem, BoldTextFont );
	UxPutBackground( UnzoomItem, MenuBackground );
	UxPutMnemonic( UnzoomItem, "U" );
	UxPutLabelString( UnzoomItem, "Unzoom" );

	UxPutForeground( RedrawItem, MenuForeground );
	UxPutFontList( RedrawItem, BoldTextFont );
	UxPutBackground( RedrawItem, MenuBackground );
	UxPutMnemonic( RedrawItem, "R" );
	UxPutLabelString( RedrawItem, "Redraw" );

	UxPutForeground( PrintPane, MenuForeground );
	UxPutFontList( PrintPane, BoldTextFont );
	UxPutBackground( PrintPane, MenuBackground );
	UxPutMnemonic( PrintPane, "P" );
	UxPutLabelString( PrintPane, "Print" );

	UxPutHighlightColor( FrameMenu, "Black" );
	UxPutForeground( FrameMenu, MenuForeground );
	UxPutFontList( FrameMenu, BoldTextFont );
	UxPutBackground( FrameMenu, MenuBackground );
	UxPutMnemonic( FrameMenu, "m" );
	UxPutLabelString( FrameMenu, "Frame" );

	UxPutForeground( ContinPane, MenuForeground );
	UxPutHighlightColor( ContinPane, "Black" );
	UxPutBackground( ContinPane, MenuBackground );
	UxPutRowColumnType( ContinPane, "menu_pulldown" );

	UxPutForeground( BeginItem, MenuForeground );
	UxPutFontList( BeginItem, BoldTextFont );
	UxPutBackground( BeginItem, MenuBackground );
	UxPutMnemonic( BeginItem, "B" );
	UxPutLabelString( BeginItem, "Begin" );

	UxPutForeground( AddItem, MenuForeground );
	UxPutFontList( AddItem, BoldTextFont );
	UxPutBackground( AddItem, MenuBackground );
	UxPutMnemonic( AddItem, "A" );
	UxPutLabelString( AddItem, "Add points" );

	UxPutForeground( FitItem, MenuForeground );
	UxPutFontList( FitItem, BoldTextFont );
	UxPutBackground( FitItem, MenuBackground );
	UxPutMnemonic( FitItem, "P" );
	UxPutLabelString( FitItem, "Plot" );

	UxPutForeground( ReFitItem, MenuForeground );
	UxPutFontList( ReFitItem, BoldTextFont );
	UxPutBackground( ReFitItem, MenuBackground );
	UxPutMnemonic( ReFitItem, "F" );
	UxPutLabelString( ReFitItem, "Fit" );

	UxPutHighlightColor( ContinunmMenu, "Black" );
	UxPutForeground( ContinunmMenu, MenuForeground );
	UxPutFontList( ContinunmMenu, BoldTextFont );
	UxPutBackground( ContinunmMenu, MenuBackground );
	UxPutMnemonic( ContinunmMenu, "C" );
	UxPutLabelString( ContinunmMenu, "Continuum" );

	UxPutForeground( FilterPane, MenuForeground );
	UxPutHighlightColor( FilterPane, "Black" );
	UxPutBackground( FilterPane, MenuBackground );
	UxPutRowColumnType( FilterPane, "menu_pulldown" );

	UxPutForeground( SmoothItem, MenuForeground );
	UxPutFontList( SmoothItem, BoldTextFont );
	UxPutBackground( SmoothItem, MenuBackground );
	UxPutMnemonic( SmoothItem, "S" );
	UxPutLabelString( SmoothItem, "Smooth" );

	UxPutForeground( MedianItem, MenuForeground );
	UxPutFontList( MedianItem, BoldTextFont );
	UxPutBackground( MedianItem, MenuBackground );
	UxPutMnemonic( MedianItem, "M" );
	UxPutLabelString( MedianItem, "Median" );

	UxPutForeground( UndoItem, MenuForeground );
	UxPutFontList( UndoItem, BoldTextFont );
	UxPutBackground( UndoItem, MenuBackground );
	UxPutMnemonic( UndoItem, "U" );
	UxPutLabelString( UndoItem, "Undo" );

	UxPutHighlightColor( FilterMenu, "Black" );
	UxPutForeground( FilterMenu, MenuForeground );
	UxPutFontList( FilterMenu, BoldTextFont );
	UxPutBackground( FilterMenu, MenuBackground );
	UxPutMnemonic( FilterMenu, "t" );
	UxPutLabelString( FilterMenu, "Filter" );

	UxPutForeground( LabelPane, MenuForeground );
	UxPutHighlightColor( LabelPane, "Black" );
	UxPutBackground( LabelPane, MenuBackground );
	UxPutRowColumnType( LabelPane, "menu_pulldown" );

	UxPutForeground( AxisItem, MenuForeground );
	UxPutFontList( AxisItem, BoldTextFont );
	UxPutBackground( AxisItem, MenuBackground );
	UxPutMnemonic( AxisItem, "a" );
	UxPutLabelString( AxisItem, "Label axis" );

	UxPutForeground( CursorItem, MenuForeground );
	UxPutFontList( CursorItem, BoldTextFont );
	UxPutBackground( CursorItem, MenuBackground );
	UxPutMnemonic( CursorItem, "C" );
	UxPutLabelString( CursorItem, "Label / Cursor" );

	UxPutForeground( LoadItem, MenuForeground );
	UxPutFontList( LoadItem, BoldTextFont );
	UxPutBackground( LoadItem, MenuBackground );
	UxPutMnemonic( LoadItem, "L" );
	UxPutLabelString( LoadItem, "Load label file" );

	UxPutForeground( ClearItem, MenuForeground );
	UxPutFontList( ClearItem, BoldTextFont );
	UxPutBackground( ClearItem, MenuBackground );
	UxPutMnemonic( ClearItem, "C" );
	UxPutLabelString( ClearItem, "Clear labels" );

	UxPutHighlightColor( LabelMenu, "Black" );
	UxPutForeground( LabelMenu, MenuForeground );
	UxPutFontList( LabelMenu, BoldTextFont );
	UxPutBackground( LabelMenu, MenuBackground );
	UxPutMnemonic( LabelMenu, "L" );
	UxPutLabelString( LabelMenu, "Label" );

	UxPutForeground( UtilsPane, MenuForeground );
	UxPutHighlightColor( UtilsPane, "Black" );
	UxPutBackground( UtilsPane, MenuBackground );
	UxPutRowColumnType( UtilsPane, "menu_pulldown" );

	UxPutForeground( GetcursorItem, MenuForeground );
	UxPutFontList( GetcursorItem, BoldTextFont );
	UxPutBackground( GetcursorItem, MenuBackground );
	UxPutMnemonic( GetcursorItem, "G" );
	UxPutLabelString( GetcursorItem, "Get cursor" );

	UxPutForeground( SavetmpItem, MenuForeground );
	UxPutFontList( SavetmpItem, BoldTextFont );
	UxPutBackground( SavetmpItem, MenuBackground );
	UxPutMnemonic( SavetmpItem, "S" );
	UxPutLabelString( SavetmpItem, "Save TMP files ..." );

	UxPutForeground( PrinterItem, MenuForeground );
	UxPutFontList( PrinterItem, BoldTextFont );
	UxPutBackground( PrinterItem, MenuBackground );
	UxPutMnemonic( PrinterItem, "D" );
	UxPutLabelString( PrinterItem, "Default Printer ..." );

	UxPutHighlightColor( UtilsMenu, "Black" );
	UxPutForeground( UtilsMenu, MenuForeground );
	UxPutFontList( UtilsMenu, BoldTextFont );
	UxPutBackground( UtilsMenu, MenuBackground );
	UxPutMnemonic( UtilsMenu, "U" );
	UxPutLabelString( UtilsMenu, "Utils" );

	UxPutHighlightColor( OptionPane, "Black" );
	UxPutForeground( OptionPane, MenuForeground );
	UxPutBackground( OptionPane, MenuBackground );
	UxPutRowColumnType( OptionPane, "menu_pulldown" );

	UxPutForeground( PlotRadio, MenuForeground );
	UxPutBackground( PlotRadio, MenuBackground );
	UxPutRadioBehavior( PlotRadio, "true" );
	UxPutRowColumnType( PlotRadio, "menu_pulldown" );

	UxPutHeight( StraightToggle, 20 );
	UxPutForeground( StraightToggle, MenuForeground );
	UxPutFontList( StraightToggle, BoldTextFont );
	UxPutSet( StraightToggle, "true" );
	UxPutBackground( StraightToggle, MenuBackground );
	UxPutLabelString( StraightToggle, "Straight line" );

	UxPutHeight( HistoToggle, 20 );
	UxPutForeground( HistoToggle, MenuForeground );
	UxPutFontList( HistoToggle, BoldTextFont );
	UxPutBackground( HistoToggle, MenuBackground );
	UxPutLabelString( HistoToggle, "Histogram - like" );

	UxPutForeground( PlotItem, MenuForeground );
	UxPutFontList( PlotItem, BoldTextFont );
	UxPutBackground( PlotItem, MenuBackground );
	UxPutLabelString( PlotItem, "Plot mode" );

	UxPutBackground( ContinRadio, MenuBackground );
	UxPutForeground( ContinRadio, MenuForeground );
	UxPutRadioBehavior( ContinRadio, "true" );
	UxPutRowColumnType( ContinRadio, "menu_pulldown" );

	UxPutHeight( PolynomialItem, 20 );
	UxPutSet( PolynomialItem, "true" );
	UxPutBackground( PolynomialItem, MenuBackground );
	UxPutForeground( PolynomialItem, MenuForeground );
	UxPutFontList( PolynomialItem, BoldTextFont );
	UxPutLabelString( PolynomialItem, "Polynomial" );

	UxPutHeight( SplineItem, 20 );
	UxPutSet( SplineItem, "false" );
	UxPutBackground( SplineItem, MenuBackground );
	UxPutForeground( SplineItem, MenuForeground );
	UxPutFontList( SplineItem, BoldTextFont );
	UxPutLabelString( SplineItem, "Spline" );

	UxPutBackground( ContinuumItem, MenuBackground );
	UxPutForeground( ContinuumItem, MenuForeground );
	UxPutFontList( ContinuumItem, BoldTextFont );
	UxPutLabelString( ContinuumItem, "Continuum fit mode" );

	UxPutBackground( PrintRadio, MenuBackground );
	UxPutRadioBehavior( PrintRadio, "true" );
	UxPutRowColumnType( PrintRadio, "menu_pulldown" );

	UxPutSet( NormalToggle, "true" );
	UxPutForeground( NormalToggle, MenuForeground );
	UxPutFontList( NormalToggle, BoldTextFont );
	UxPutBackground( NormalToggle, MenuBackground );
	UxPutMnemonic( NormalToggle, "L" );
	UxPutLabelString( NormalToggle, "Landscape" );

	UxPutForeground( PortraitToggle, MenuForeground );
	UxPutFontList( PortraitToggle, BoldTextFont );
	UxPutBackground( PortraitToggle, MenuBackground );
	UxPutMnemonic( PortraitToggle, "P" );
	UxPutLabelString( PortraitToggle, "Portrait" );

	UxPutForeground( PrintItem, MenuForeground );
	UxPutFontList( PrintItem, BoldTextFont );
	UxPutBackground( PrintItem, MenuBackground );
	UxPutLabelString( PrintItem, "Print Mode" );

	UxPutHighlightColor( OptionMenu, "Black" );
	UxPutForeground( OptionMenu, MenuForeground );
	UxPutFontList( OptionMenu, BoldTextFont );
	UxPutBackground( OptionMenu, MenuBackground );
	UxPutMnemonic( OptionMenu, "O" );
	UxPutLabelString( OptionMenu, "Options" );

	UxPutForeground( HelpPane, MenuForeground );
	UxPutBackground( HelpPane, MenuBackground );
	UxPutRowColumnType( HelpPane, "menu_pulldown" );

	UxPutForeground( Context, MenuForeground );
	UxPutFontList( Context, BoldTextFont );
	UxPutBackground( Context, MenuBackground );
	UxPutMnemonic( Context, "C" );
	UxPutLabelString( Context, "On Context" );

	UxPutForeground( OnHelp, MenuForeground );
	UxPutFontList( OnHelp, BoldTextFont );
	UxPutBackground( OnHelp, MenuBackground );
	UxPutMnemonic( OnHelp, "H" );
	UxPutLabelString( OnHelp, "On Help" );

	UxPutForeground( HelpMenu, MenuForeground );
	UxPutFontList( HelpMenu, BoldTextFont );
	UxPutBackground( HelpMenu, MenuBackground );
	UxPutMnemonic( HelpMenu, "H" );
	UxPutLabelString( HelpMenu, "Help" );

	UxPutForeground( QuitPane, MenuForeground );
	UxPutHighlightColor( QuitPane, "Black" );
	UxPutBackground( QuitPane, MenuBackground );
	UxPutRowColumnType( QuitPane, "menu_pulldown" );

	UxPutForeground( QuitItem, MenuForeground );
	UxPutFontList( QuitItem, BoldTextFont );
	UxPutBackground( QuitItem, MenuBackground );
	UxPutMnemonic( QuitItem, "B" );
	UxPutLabelString( QuitItem, "Bye" );

	UxPutHighlightColor( QuitMenu, "Black" );
	UxPutForeground( QuitMenu, MenuForeground );
	UxPutFontList( QuitMenu, BoldTextFont );
	UxPutBackground( QuitMenu, MenuBackground );
	UxPutMnemonic( QuitMenu, "Q" );
	UxPutLabelString( QuitMenu, "Quit" );

	UxPutBackground( AliceForm1, WindowBackground );

	UxPutLabelString( Line, "Line :" );
	UxPutForeground( Line, TextForeground );
	UxPutFontList( Line, TextFont );
	UxPutBackground( Line, LabelBackground );
	UxPutHeight( Line, 30 );
	UxPutWidth( Line, 40 );
	UxPutY( Line, 35 );
	UxPutX( Line, 13 );

	UxPutTranslations( CurrLine, TextTab );
	UxPutFontList( CurrLine, TextFont );
	UxPutHighlightOnEnter( CurrLine, "true" );
	UxPutMaxLength( CurrLine, 5 );
	UxPutForeground( CurrLine, TextForeground );
	UxPutBorderColor( CurrLine, "Gray100" );
	UxPutBackground( CurrLine, TextBackground );
	UxPutHeight( CurrLine, 36 );
	UxPutWidth( CurrLine, 52 );
	UxPutY( CurrLine, 35 );
	UxPutX( CurrLine, 55 );

	UxPutTranslations( Linestep, TextTab );
	UxPutFontList( Linestep, TextFont );
	UxPutHighlightOnEnter( Linestep, "true" );
	UxPutMaxLength( Linestep, 3 );
	UxPutForeground( Linestep, TextForeground );
	UxPutBorderColor( Linestep, "Gray100" );
	UxPutBackground( Linestep, TextBackground );
	UxPutHeight( Linestep, 36 );
	UxPutWidth( Linestep, 34 );
	UxPutY( Linestep, 35 );
	UxPutX( Linestep, 109 );

	UxPutHighlightColor( arrowButton1, "Black" );
	UxPutTranslations( arrowButton1, HELPTopTab );
	UxPutHighlightOnEnter( arrowButton1, "true" );
	UxPutForeground( arrowButton1, MenuBackground );
	UxPutBackground( arrowButton1, TextBackground );
	UxPutHeight( arrowButton1, 20 );
	UxPutWidth( arrowButton1, 30 );
	UxPutY( arrowButton1, 33 );
	UxPutX( arrowButton1, 141 );

	UxPutHighlightColor( arrowButton2, "Black" );
	UxPutTranslations( arrowButton2, HELPTopTab );
	UxPutHighlightOnEnter( arrowButton2, "true" );
	UxPutArrowDirection( arrowButton2, "arrow_down" );
	UxPutForeground( arrowButton2, MenuBackground );
	UxPutBackground( arrowButton2, TextBackground );
	UxPutHeight( arrowButton2, 20 );
	UxPutWidth( arrowButton2, 30 );
	UxPutY( arrowButton2, 51 );
	UxPutX( arrowButton2, 141 );

	UxPutForeground( Degree, TextForeground );
	UxPutLabelString( Degree, "Fit Degree :" );
	UxPutBorderColor( Degree, "Gray100" );
	UxPutFontList( Degree, TextFont );
	UxPutBackground( Degree, LabelBackground );
	UxPutHeight( Degree, 30 );
	UxPutWidth( Degree, 87 );
	UxPutY( Degree, 35 );
	UxPutX( Degree, 343 );

	UxPutTranslations( degree_text, TextTab );
	UxPutFontList( degree_text, TextFont );
	UxPutHighlightOnEnter( degree_text, "true" );
	UxPutMaxLength( degree_text, 3 );
	UxPutForeground( degree_text, TextForeground );
	UxPutBorderColor( degree_text, "Gray100" );
	UxPutBackground( degree_text, TextBackground );
	UxPutHeight( degree_text, 36 );
	UxPutWidth( degree_text, 36 );
	UxPutY( degree_text, 33 );
	UxPutX( degree_text, 431 );

	UxPutHighlightColor( arrowButton3, "Black" );
	UxPutTranslations( arrowButton3, HELPTopTab );
	UxPutHighlightOnEnter( arrowButton3, "true" );
	UxPutForeground( arrowButton3, MenuBackground );
	UxPutBackground( arrowButton3, TextBackground );
	UxPutHeight( arrowButton3, 20 );
	UxPutWidth( arrowButton3, 30 );
	UxPutY( arrowButton3, 32 );
	UxPutX( arrowButton3, 464 );

	UxPutHighlightColor( arrowButton4, "Black" );
	UxPutTranslations( arrowButton4, HELPTopTab );
	UxPutHighlightOnEnter( arrowButton4, "true" );
	UxPutForeground( arrowButton4, MenuBackground );
	UxPutArrowDirection( arrowButton4, "arrow_down" );
	UxPutBackground( arrowButton4, TextBackground );
	UxPutHeight( arrowButton4, 20 );
	UxPutWidth( arrowButton4, 30 );
	UxPutY( arrowButton4, 50 );
	UxPutX( arrowButton4, 464 );

	UxPutBackground( AliceForm2, ButtonBackground );
	UxPutHeight( AliceForm2, 44 );
	UxPutWidth( AliceForm2, 565 );
	UxPutY( AliceForm2, 389 );
	UxPutX( AliceForm2, -1 );
	UxPutResizePolicy( AliceForm2, "resize_none" );

	UxPutHighlightColor( GaussButton, "Black" );
	UxPutTranslations( GaussButton, Help );
	UxPutHighlightOnEnter( GaussButton, "true" );
	UxPutBackground( GaussButton, ButtonBackground );
	UxPutLabelString( GaussButton, "Gauss ..." );
	UxPutForeground( GaussButton, ButtonForeground );
	UxPutFontList( GaussButton, BoldTextFont );
	UxPutHeight( GaussButton, 30 );
	UxPutWidth( GaussButton, 90 );
	UxPutY( GaussButton, 6 );
	UxPutX( GaussButton, 32 );

	UxPutTranslations( IntegrateButton, Help );
	UxPutHighlightOnEnter( IntegrateButton, "true" );
	UxPutBackground( IntegrateButton, ButtonBackground );
	UxPutLabelString( IntegrateButton, "Integrate" );
	UxPutForeground( IntegrateButton, ButtonForeground );
	UxPutFontList( IntegrateButton, BoldTextFont );
	UxPutHeight( IntegrateButton, 30 );
	UxPutWidth( IntegrateButton, 90 );
	UxPutY( IntegrateButton, 6 );
	UxPutX( IntegrateButton, 375 );

	UxPutHighlightColor( RebinButton, "Black" );
	UxPutTranslations( RebinButton, Help );
	UxPutHighlightOnEnter( RebinButton, "true" );
	UxPutBackground( RebinButton, ButtonBackground );
	UxPutLabelString( RebinButton, "Rebin ..." );
	UxPutForeground( RebinButton, ButtonForeground );
	UxPutFontList( RebinButton, BoldTextFont );
	UxPutHeight( RebinButton, 30 );
	UxPutWidth( RebinButton, 90 );
	UxPutY( RebinButton, 6 );
	UxPutX( RebinButton, 152 );

	UxPutHighlightColor( RebinButton1, "Black" );
	UxPutTranslations( RebinButton1, Help );
	UxPutHighlightOnEnter( RebinButton1, "true" );
	UxPutBackground( RebinButton1, ButtonBackground );
	UxPutLabelString( RebinButton1, "Overplot ..." );
	UxPutForeground( RebinButton1, ButtonForeground );
	UxPutFontList( RebinButton1, BoldTextFont );
	UxPutHeight( RebinButton1, 30 );
	UxPutWidth( RebinButton1, 90 );
	UxPutY( RebinButton1, 6 );
	UxPutX( RebinButton1, 263 );

	UxPutForeground( separator1, TextForeground );
	UxPutBackground( separator1, LabelBackground );
	UxPutHeight( separator1, 5 );
	UxPutWidth( separator1, 554 );
	UxPutY( separator1, 84 );
	UxPutX( separator1, 1 );

	UxPutTranslations( width_text, TextTab );
	UxPutFontList( width_text, TextFont );
	UxPutHighlightOnEnter( width_text, "true" );
	UxPutMaxLength( width_text, 4 );
	UxPutForeground( width_text, TextForeground );
	UxPutBorderColor( width_text, "Gray100" );
	UxPutBackground( width_text, TextBackground );
	UxPutHeight( width_text, 36 );
	UxPutWidth( width_text, 52 );
	UxPutY( width_text, 34 );
	UxPutX( width_text, 253 );

	UxPutHighlightColor( arrowButton11, "Black" );
	UxPutTranslations( arrowButton11, HELPTopTab );
	UxPutHighlightOnEnter( arrowButton11, "true" );
	UxPutForeground( arrowButton11, MenuBackground );
	UxPutBackground( arrowButton11, TextBackground );
	UxPutHeight( arrowButton11, 20 );
	UxPutWidth( arrowButton11, 30 );
	UxPutY( arrowButton11, 32 );
	UxPutX( arrowButton11, 303 );

	UxPutHighlightColor( arrowButton12, "Black" );
	UxPutTranslations( arrowButton12, HELPTopTab );
	UxPutHighlightOnEnter( arrowButton12, "true" );
	UxPutForeground( arrowButton12, MenuBackground );
	UxPutArrowDirection( arrowButton12, "arrow_down" );
	UxPutBackground( arrowButton12, TextBackground );
	UxPutHeight( arrowButton12, 20 );
	UxPutWidth( arrowButton12, 30 );
	UxPutY( arrowButton12, 50 );
	UxPutX( arrowButton12, 303 );

	UxPutForeground( Line1, TextForeground );
	UxPutLabelString( Line1, "Window :" );
	UxPutFontList( Line1, TextFont );
	UxPutBackground( Line1, LabelBackground );
	UxPutHeight( Line1, 30 );
	UxPutWidth( Line1, 72 );
	UxPutY( Line1, 34 );
	UxPutX( Line1, 177 );

	UxPutForeground( Line2, TextForeground );
	UxPutLabelString( Line2, "Image" );
	UxPutFontList( Line2, TextFont );
	UxPutBackground( Line2, LabelBackground );
	UxPutHeight( Line2, 18 );
	UxPutWidth( Line2, 64 );
	UxPutY( Line2, 5 );
	UxPutX( Line2, 71 );

	UxPutForeground( Line3, TextForeground );
	UxPutLabelString( Line3, "Filter" );
	UxPutFontList( Line3, TextFont );
	UxPutBackground( Line3, LabelBackground );
	UxPutHeight( Line3, 18 );
	UxPutWidth( Line3, 64 );
	UxPutY( Line3, 4 );
	UxPutX( Line3, 221 );

	UxPutForeground( Line4, TextForeground );
	UxPutLabelString( Line4, "Continuum" );
	UxPutFontList( Line4, TextFont );
	UxPutBackground( Line4, LabelBackground );
	UxPutHeight( Line4, 18 );
	UxPutWidth( Line4, 86 );
	UxPutY( Line4, 4 );
	UxPutX( Line4, 398 );

	UxPutMarginHeight( help_text_top, 3 );
	UxPutForeground( help_text_top, TextForeground );
	UxPutCursorPositionVisible( help_text_top, "false" );
	UxPutFontList( help_text_top, TextFont );
	UxPutEditable( help_text_top, "false" );
	UxPutBackground( help_text_top, SHelpBackground );
	UxPutHeight( help_text_top, 48 );
	UxPutWidth( help_text_top, 555 );
	UxPutY( help_text_top, 332 );
	UxPutX( help_text_top, 4 );

	UxPutForeground( separator3, TextForeground );
	UxPutBackground( separator3, ButtonBackground );
	UxPutHeight( separator3, 8 );
	UxPutWidth( separator3, 565 );
	UxPutY( separator3, 382 );
	UxPutX( separator3, -2 );

	UxPutForeground( separator8, TextForeground );
	UxPutBackground( separator8, LabelBackground );
	UxPutHeight( separator8, 10 );
	UxPutWidth( separator8, 158 );
	UxPutY( separator8, 25 );
	UxPutX( separator8, 13 );

	UxPutForeground( separator9, TextForeground );
	UxPutBackground( separator9, LabelBackground );
	UxPutHeight( separator9, 10 );
	UxPutWidth( separator9, 146 );
	UxPutY( separator9, 24 );
	UxPutX( separator9, 185 );

	UxPutForeground( separator10, TextForeground );
	UxPutBackground( separator10, LabelBackground );
	UxPutHeight( separator10, 10 );
	UxPutWidth( separator10, 202 );
	UxPutY( separator10, 24 );
	UxPutX( separator10, 348 );

	UxPutHighlightColor( GaussDrawingArea, "Black" );
	UxPutForeground( GaussDrawingArea, TextForeground );
	UxPutMarginWidth( GaussDrawingArea, 0 );
	UxPutMarginHeight( GaussDrawingArea, 0 );
	UxPutBackground( GaussDrawingArea, TextBackground );
	UxPutHeight( GaussDrawingArea, 200 );
	UxPutWidth( GaussDrawingArea, 400 );
	UxPutY( GaussDrawingArea, 112 );
	UxPutX( GaussDrawingArea, 16 );
	UxPutResizePolicy( GaussDrawingArea, "resize_none" );

	UxPutTranslations( arrowButton8, HELPTopTab );
	UxPutHighlightColor( arrowButton8, "Black" );
	UxPutHighlightOnEnter( arrowButton8, "true" );
	UxPutForeground( arrowButton8, TextForeground );
	UxPutBackground( arrowButton8, ButtonBackground );
	UxPutArrowDirection( arrowButton8, "arrow_left" );
	UxPutHeight( arrowButton8, 30 );
	UxPutWidth( arrowButton8, 30 );
	UxPutY( arrowButton8, 239 );
	UxPutX( arrowButton8, 442 );

	UxPutTranslations( arrowButton6, HELPTopTab );
	UxPutHighlightColor( arrowButton6, "Black" );
	UxPutHighlightOnEnter( arrowButton6, "true" );
	UxPutForeground( arrowButton6, TextForeground );
	UxPutBackground( arrowButton6, ButtonBackground );
	UxPutArrowDirection( arrowButton6, "arrow_down" );
	UxPutHeight( arrowButton6, 30 );
	UxPutWidth( arrowButton6, 30 );
	UxPutY( arrowButton6, 251 );
	UxPutX( arrowButton6, 476 );

	UxPutTranslations( arrowButton5, HELPTopTab );
	UxPutHighlightColor( arrowButton5, "Black" );
	UxPutHighlightOnEnter( arrowButton5, "true" );
	UxPutForeground( arrowButton5, TextForeground );
	UxPutBackground( arrowButton5, ButtonBackground );
	UxPutArrowDirection( arrowButton5, "arrow_right" );
	UxPutHeight( arrowButton5, 30 );
	UxPutWidth( arrowButton5, 30 );
	UxPutY( arrowButton5, 239 );
	UxPutX( arrowButton5, 508 );

	UxPutTranslations( arrowButton7, HELPTopTab );
	UxPutHighlightColor( arrowButton7, "Black" );
	UxPutHighlightOnEnter( arrowButton7, "true" );
	UxPutForeground( arrowButton7, TextForeground );
	UxPutBackground( arrowButton7, ButtonBackground );
	UxPutHeight( arrowButton7, 30 );
	UxPutWidth( arrowButton7, 30 );
	UxPutY( arrowButton7, 223 );
	UxPutX( arrowButton7, 475 );

	UxPutLabelString( Step, "Step :" );
	UxPutBackground( Step, LabelBackground );
	UxPutForeground( Step, TextForeground );
	UxPutFontList( Step, TextFont );
	UxPutHeight( Step, 30 );
	UxPutWidth( Step, 50 );
	UxPutY( Step, 293 );
	UxPutX( Step, 437 );

	UxPutTopShadowColor( step_text, WindowBackground );
	UxPutHighlightColor( step_text, "Black" );
	UxPutTranslations( step_text, TextTab );
	UxPutFontList( step_text, TextFont );
	UxPutSensitive( step_text, "true" );
	UxPutText( step_text, "0.2" );
	UxPutHighlightOnEnter( step_text, "true" );
	UxPutBackground( step_text, TextBackground );
	UxPutForeground( step_text, TextForeground );
	UxPutHeight( step_text, 34 );
	UxPutWidth( step_text, 51 );
	UxPutY( step_text, 289 );
	UxPutX( step_text, 487 );

	UxPutTranslations( Cut_x, Help );
	UxPutHighlightColor( Cut_x, "Black" );
	UxPutHighlightOnEnter( Cut_x, "true" );
	UxPutForeground( Cut_x, ButtonForeground );
	UxPutFontList( Cut_x, BoldTextFont );
	UxPutLabelString( Cut_x, "Cut X" );
	UxPutBackground( Cut_x, ButtonBackground );
	UxPutHeight( Cut_x, 30 );
	UxPutWidth( Cut_x, 90 );
	UxPutY( Cut_x, 94 );
	UxPutX( Cut_x, 443 );

	UxPutTranslations( Cut_y, Help );
	UxPutHighlightColor( Cut_y, "Black" );
	UxPutHighlightOnEnter( Cut_y, "true" );
	UxPutLabelString( Cut_y, "Cut Y" );
	UxPutFontList( Cut_y, BoldTextFont );
	UxPutForeground( Cut_y, ButtonForeground );
	UxPutBackground( Cut_y, ButtonBackground );
	UxPutHeight( Cut_y, 30 );
	UxPutWidth( Cut_y, 90 );
	UxPutY( Cut_y, 124 );
	UxPutX( Cut_y, 443 );

	UxPutTranslations( Move, Help );
	UxPutHighlightColor( Move, "Black" );
	UxPutHighlightOnEnter( Move, "true" );
	UxPutFontList( Move, BoldTextFont );
	UxPutForeground( Move, ButtonForeground );
	UxPutBackground( Move, ButtonBackground );
	UxPutHeight( Move, 30 );
	UxPutWidth( Move, 90 );
	UxPutY( Move, 155 );
	UxPutX( Move, 443 );

	UxPutForeground( separator11, TextForeground );
	UxPutBackground( separator11, LabelBackground );
	UxPutHeight( separator11, 9 );
	UxPutWidth( separator11, 416 );
	UxPutY( separator11, 316 );
	UxPutX( separator11, 8 );

	UxPutForeground( separator12, TextForeground );
	UxPutBackground( separator12, LabelBackground );
	UxPutOrientation( separator12, "vertical" );
	UxPutHeight( separator12, 216 );
	UxPutWidth( separator12, 10 );
	UxPutY( separator12, 102 );
	UxPutX( separator12, 421 );

	UxPutForeground( separator13, TextForeground );
	UxPutBackground( separator13, LabelBackground );
	UxPutOrientation( separator13, "vertical" );
	UxPutHeight( separator13, 215 );
	UxPutWidth( separator13, 10 );
	UxPutY( separator13, 104 );
	UxPutX( separator13, 2 );

	UxPutForeground( separator14, TextForeground );
	UxPutBackground( separator14, LabelBackground );
	UxPutHeight( separator14, 9 );
	UxPutWidth( separator14, 415 );
	UxPutY( separator14, 99 );
	UxPutX( separator14, 8 );

	UxPutTranslations( Unzoom, Help );
	UxPutHighlightColor( Unzoom, "Black" );
	UxPutHighlightOnEnter( Unzoom, "true" );
	UxPutFontList( Unzoom, BoldTextFont );
	UxPutForeground( Unzoom, ButtonForeground );
	UxPutBackground( Unzoom, ButtonBackground );
	UxPutHeight( Unzoom, 30 );
	UxPutWidth( Unzoom, 90 );
	UxPutY( Unzoom, 186 );
	UxPutX( Unzoom, 443 );

	UxPutLabelString( AutoFitTButton, "" );
	UxPutAncestorSensitive( AutoFitTButton, "true" );
	UxPutIndicatorSize( AutoFitTButton, 13 );
	UxPutForeground( AutoFitTButton, "Black" );
	UxPutTranslations( AutoFitTButton, HELPTopTab );
	UxPutHighlightOnEnter( AutoFitTButton, "true" );
	UxPutSelectColor( AutoFitTButton, SelectColor );
	UxPutBackground( AutoFitTButton, LabelBackground );
	UxPutHeight( AutoFitTButton, 23 );
	UxPutWidth( AutoFitTButton, 20 );
	UxPutY( AutoFitTButton, 47 );
	UxPutX( AutoFitTButton, 514 );

	UxPutForeground( AutoFitLabel, TextForeground );
	UxPutLabelString( AutoFitLabel, "Auto-fit" );
	UxPutFontList( AutoFitLabel, TextFont );
	UxPutBackground( AutoFitLabel, LabelBackground );
	UxPutHeight( AutoFitLabel, 18 );
	UxPutWidth( AutoFitLabel, 62 );
	UxPutY( AutoFitLabel, 33 );
	UxPutX( AutoFitLabel, 495 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_AliceShell()
{
	/* Create the swidgets */

	AliceShell = UxCreateApplicationShell( "AliceShell", NO_PARENT );
	UxPutContext( AliceShell, UxAliceShellContext );

	AliceWindow = UxCreateMainWindow( "AliceWindow", AliceShell );
	Topmenu = UxCreateRowColumn( "Topmenu", AliceWindow );
	FilePane = UxCreateRowColumn( "FilePane", Topmenu );
	ReadItem = UxCreatePushButton( "ReadItem", FilePane );
	SaveAsItem = UxCreatePushButton( "SaveAsItem", FilePane );
	SaveItem = UxCreatePushButton( "SaveItem", FilePane );
	FileMenu = UxCreateCascadeButton( "FileMenu", Topmenu );
	FramePane = UxCreateRowColumn( "FramePane", Topmenu );
	CutxItem = UxCreatePushButton( "CutxItem", FramePane );
	CutyItem = UxCreatePushButton( "CutyItem", FramePane );
	UnzoomItem = UxCreatePushButton( "UnzoomItem", FramePane );
	RedrawItem = UxCreatePushButton( "RedrawItem", FramePane );
	PrintPane = UxCreatePushButton( "PrintPane", FramePane );
	FrameMenu = UxCreateCascadeButton( "FrameMenu", Topmenu );
	ContinPane = UxCreateRowColumn( "ContinPane", Topmenu );
	BeginItem = UxCreatePushButton( "BeginItem", ContinPane );
	AddItem = UxCreatePushButton( "AddItem", ContinPane );
	FitItem = UxCreatePushButton( "FitItem", ContinPane );
	ReFitItem = UxCreatePushButton( "ReFitItem", ContinPane );
	ContinunmMenu = UxCreateCascadeButton( "ContinunmMenu", Topmenu );
	FilterPane = UxCreateRowColumn( "FilterPane", Topmenu );
	SmoothItem = UxCreatePushButton( "SmoothItem", FilterPane );
	MedianItem = UxCreatePushButton( "MedianItem", FilterPane );
	UndoItem = UxCreatePushButton( "UndoItem", FilterPane );
	FilterMenu = UxCreateCascadeButton( "FilterMenu", Topmenu );
	LabelPane = UxCreateRowColumn( "LabelPane", Topmenu );
	AxisItem = UxCreatePushButton( "AxisItem", LabelPane );
	CursorItem = UxCreatePushButton( "CursorItem", LabelPane );
	LoadItem = UxCreatePushButton( "LoadItem", LabelPane );
	ClearItem = UxCreatePushButton( "ClearItem", LabelPane );
	LabelMenu = UxCreateCascadeButton( "LabelMenu", Topmenu );
	UtilsPane = UxCreateRowColumn( "UtilsPane", Topmenu );
	GetcursorItem = UxCreatePushButton( "GetcursorItem", UtilsPane );
	SavetmpItem = UxCreatePushButton( "SavetmpItem", UtilsPane );
	PrinterItem = UxCreatePushButton( "PrinterItem", UtilsPane );
	UtilsMenu = UxCreateCascadeButton( "UtilsMenu", Topmenu );
	OptionPane = UxCreateRowColumn( "OptionPane", Topmenu );
	PlotRadio = UxCreateRowColumn( "PlotRadio", OptionPane );
	StraightToggle = UxCreateToggleButton( "StraightToggle", PlotRadio );
	HistoToggle = UxCreateToggleButton( "HistoToggle", PlotRadio );
	PlotItem = UxCreateCascadeButton( "PlotItem", OptionPane );
	ContinRadio = UxCreateRowColumn( "ContinRadio", OptionPane );
	PolynomialItem = UxCreateToggleButton( "PolynomialItem", ContinRadio );
	SplineItem = UxCreateToggleButton( "SplineItem", ContinRadio );
	ContinuumItem = UxCreateCascadeButton( "ContinuumItem", OptionPane );
	PrintRadio = UxCreateRowColumn( "PrintRadio", OptionPane );
	NormalToggle = UxCreateToggleButton( "NormalToggle", PrintRadio );
	PortraitToggle = UxCreateToggleButton( "PortraitToggle", PrintRadio );
	PrintItem = UxCreateCascadeButton( "PrintItem", OptionPane );
	OptionMenu = UxCreateCascadeButton( "OptionMenu", Topmenu );
	HelpPane = UxCreateRowColumn( "HelpPane", Topmenu );
	Context = UxCreatePushButton( "Context", HelpPane );
	OnHelp = UxCreatePushButton( "OnHelp", HelpPane );
	HelpMenu = UxCreateCascadeButton( "HelpMenu", Topmenu );
	QuitPane = UxCreateRowColumn( "QuitPane", Topmenu );
	QuitItem = UxCreatePushButton( "QuitItem", QuitPane );
	QuitMenu = UxCreateCascadeButton( "QuitMenu", Topmenu );
	AliceForm1 = UxCreateForm( "AliceForm1", AliceWindow );
	Line = UxCreateLabel( "Line", AliceForm1 );
	CurrLine = UxCreateText( "CurrLine", AliceForm1 );
	Linestep = UxCreateText( "Linestep", AliceForm1 );
	arrowButton1 = UxCreateArrowButton( "arrowButton1", AliceForm1 );
	arrowButton2 = UxCreateArrowButton( "arrowButton2", AliceForm1 );
	Degree = UxCreateLabel( "Degree", AliceForm1 );
	degree_text = UxCreateText( "degree_text", AliceForm1 );
	arrowButton3 = UxCreateArrowButton( "arrowButton3", AliceForm1 );
	arrowButton4 = UxCreateArrowButton( "arrowButton4", AliceForm1 );
	AliceForm2 = UxCreateForm( "AliceForm2", AliceForm1 );
	GaussButton = UxCreatePushButton( "GaussButton", AliceForm2 );
	IntegrateButton = UxCreatePushButton( "IntegrateButton", AliceForm2 );
	RebinButton = UxCreatePushButton( "RebinButton", AliceForm2 );
	RebinButton1 = UxCreatePushButton( "RebinButton1", AliceForm2 );
	separator1 = UxCreateSeparator( "separator1", AliceForm1 );
	width_text = UxCreateText( "width_text", AliceForm1 );
	arrowButton11 = UxCreateArrowButton( "arrowButton11", AliceForm1 );
	arrowButton12 = UxCreateArrowButton( "arrowButton12", AliceForm1 );
	Line1 = UxCreateLabel( "Line1", AliceForm1 );
	Line2 = UxCreateLabel( "Line2", AliceForm1 );
	Line3 = UxCreateLabel( "Line3", AliceForm1 );
	Line4 = UxCreateLabel( "Line4", AliceForm1 );
	help_text_top = UxCreateText( "help_text_top", AliceForm1 );
	separator3 = UxCreateSeparator( "separator3", AliceForm1 );
	separator8 = UxCreateSeparator( "separator8", AliceForm1 );
	separator9 = UxCreateSeparator( "separator9", AliceForm1 );
	separator10 = UxCreateSeparator( "separator10", AliceForm1 );
	GaussDrawingArea = UxCreateDrawingArea( "GaussDrawingArea", AliceForm1 );
	arrowButton8 = UxCreateArrowButton( "arrowButton8", AliceForm1 );
	arrowButton6 = UxCreateArrowButton( "arrowButton6", AliceForm1 );
	arrowButton5 = UxCreateArrowButton( "arrowButton5", AliceForm1 );
	arrowButton7 = UxCreateArrowButton( "arrowButton7", AliceForm1 );
	Step = UxCreateLabel( "Step", AliceForm1 );
	step_text = UxCreateText( "step_text", AliceForm1 );
	Cut_x = UxCreatePushButton( "Cut_x", AliceForm1 );
	Cut_y = UxCreatePushButton( "Cut_y", AliceForm1 );
	Move = UxCreatePushButton( "Move", AliceForm1 );
	separator11 = UxCreateSeparator( "separator11", AliceForm1 );
	separator12 = UxCreateSeparator( "separator12", AliceForm1 );
	separator13 = UxCreateSeparator( "separator13", AliceForm1 );
	separator14 = UxCreateSeparator( "separator14", AliceForm1 );
	Unzoom = UxCreatePushButton( "Unzoom", AliceForm1 );
	AutoFitTButton = UxCreateToggleButton( "AutoFitTButton", AliceForm1 );
	AutoFitLabel = UxCreateLabel( "AutoFitLabel", AliceForm1 );

	_Uxinit_AliceShell();

	/* Create the X widgets */

	UxCreateWidget( AliceShell );
	UxCreateWidget( AliceWindow );
	UxCreateWidget( Topmenu );
	UxCreateWidget( FilePane );
	UxCreateWidget( ReadItem );
	UxCreateWidget( SaveAsItem );
	UxCreateWidget( SaveItem );
	UxPutSubMenuId( FileMenu, "FilePane" );
	UxCreateWidget( FileMenu );

	UxCreateWidget( FramePane );
	UxCreateWidget( CutxItem );
	UxCreateWidget( CutyItem );
	UxCreateWidget( UnzoomItem );
	UxCreateWidget( RedrawItem );
	UxCreateWidget( PrintPane );
	UxPutSubMenuId( FrameMenu, "FramePane" );
	UxCreateWidget( FrameMenu );

	UxCreateWidget( ContinPane );
	UxCreateWidget( BeginItem );
	UxCreateWidget( AddItem );
	UxCreateWidget( FitItem );
	UxCreateWidget( ReFitItem );
	UxPutSubMenuId( ContinunmMenu, "ContinPane" );
	UxCreateWidget( ContinunmMenu );

	UxCreateWidget( FilterPane );
	UxCreateWidget( SmoothItem );
	UxCreateWidget( MedianItem );
	UxCreateWidget( UndoItem );
	UxPutSubMenuId( FilterMenu, "FilterPane" );
	UxCreateWidget( FilterMenu );

	UxCreateWidget( LabelPane );
	UxCreateWidget( AxisItem );
	UxCreateWidget( CursorItem );
	UxCreateWidget( LoadItem );
	UxCreateWidget( ClearItem );
	UxPutSubMenuId( LabelMenu, "LabelPane" );
	UxCreateWidget( LabelMenu );

	UxCreateWidget( UtilsPane );
	UxCreateWidget( GetcursorItem );
	UxCreateWidget( SavetmpItem );
	UxCreateWidget( PrinterItem );
	UxPutSubMenuId( UtilsMenu, "UtilsPane" );
	UxCreateWidget( UtilsMenu );

	UxCreateWidget( OptionPane );
	UxCreateWidget( PlotRadio );
	UxCreateWidget( StraightToggle );
	UxCreateWidget( HistoToggle );
	UxPutSubMenuId( PlotItem, "PlotRadio" );
	UxCreateWidget( PlotItem );

	UxCreateWidget( ContinRadio );
	UxCreateWidget( PolynomialItem );
	UxCreateWidget( SplineItem );
	UxPutSubMenuId( ContinuumItem, "ContinRadio" );
	UxCreateWidget( ContinuumItem );

	UxCreateWidget( PrintRadio );
	UxCreateWidget( NormalToggle );
	UxCreateWidget( PortraitToggle );
	UxPutSubMenuId( PrintItem, "PrintRadio" );
	UxCreateWidget( PrintItem );

	UxPutSubMenuId( OptionMenu, "OptionPane" );
	UxCreateWidget( OptionMenu );

	UxCreateWidget( HelpPane );
	UxCreateWidget( Context );
	UxCreateWidget( OnHelp );
	UxPutSubMenuId( HelpMenu, "HelpPane" );
	UxCreateWidget( HelpMenu );

	UxCreateWidget( QuitPane );
	UxCreateWidget( QuitItem );
	UxPutSubMenuId( QuitMenu, "QuitPane" );
	UxCreateWidget( QuitMenu );

	UxCreateWidget( AliceForm1 );
	UxCreateWidget( Line );
	UxCreateWidget( CurrLine );
	UxCreateWidget( Linestep );
	UxCreateWidget( arrowButton1 );
	UxCreateWidget( arrowButton2 );
	UxCreateWidget( Degree );
	UxCreateWidget( degree_text );
	UxCreateWidget( arrowButton3 );
	UxCreateWidget( arrowButton4 );
	UxCreateWidget( AliceForm2 );
	UxCreateWidget( GaussButton );
	UxCreateWidget( IntegrateButton );
	UxCreateWidget( RebinButton );
	UxCreateWidget( RebinButton1 );
	UxCreateWidget( separator1 );
	UxCreateWidget( width_text );
	UxCreateWidget( arrowButton11 );
	UxCreateWidget( arrowButton12 );
	UxCreateWidget( Line1 );
	UxCreateWidget( Line2 );
	UxCreateWidget( Line3 );
	UxCreateWidget( Line4 );
	UxCreateWidget( help_text_top );
	UxCreateWidget( separator3 );
	UxCreateWidget( separator8 );
	UxCreateWidget( separator9 );
	UxCreateWidget( separator10 );
	UxCreateWidget( GaussDrawingArea );
	createCB_GaussDrawingArea( UxGetWidget( GaussDrawingArea ),
			(XtPointer) UxAliceShellContext, (XtPointer) NULL );

	UxCreateWidget( arrowButton8 );
	UxCreateWidget( arrowButton6 );
	UxCreateWidget( arrowButton5 );
	UxCreateWidget( arrowButton7 );
	UxCreateWidget( Step );
	UxCreateWidget( step_text );
	UxCreateWidget( Cut_x );
	UxCreateWidget( Cut_y );
	UxCreateWidget( Move );
	UxCreateWidget( separator11 );
	UxCreateWidget( separator12 );
	UxCreateWidget( separator13 );
	UxCreateWidget( separator14 );
	UxCreateWidget( Unzoom );
	UxCreateWidget( AutoFitTButton );
	UxCreateWidget( AutoFitLabel );

	UxPutMenuHelpWidget( Topmenu, "QuitMenu" );

	UxAddCallback( ReadItem, XmNactivateCallback,
			activateCB_ReadItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( SaveAsItem, XmNactivateCallback,
			activateCB_SaveAsItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( SaveItem, XmNactivateCallback,
			activateCB_SaveItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( CutxItem, XmNactivateCallback,
			activateCB_CutxItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( CutyItem, XmNactivateCallback,
			activateCB_CutyItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( UnzoomItem, XmNactivateCallback,
			activateCB_UnzoomItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( RedrawItem, XmNactivateCallback,
			activateCB_RedrawItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( PrintPane, XmNactivateCallback,
			activateCB_PrintPane,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( BeginItem, XmNactivateCallback,
			activateCB_BeginItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( AddItem, XmNactivateCallback,
			activateCB_AddItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( FitItem, XmNactivateCallback,
			activateCB_FitItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( ReFitItem, XmNactivateCallback,
			activateCB_ReFitItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( SmoothItem, XmNactivateCallback,
			activateCB_SmoothItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( MedianItem, XmNactivateCallback,
			activateCB_MedianItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( UndoItem, XmNactivateCallback,
			activateCB_UndoItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( AxisItem, XmNactivateCallback,
			activateCB_AxisItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( CursorItem, XmNactivateCallback,
			activateCB_CursorItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( LoadItem, XmNactivateCallback,
			activateCB_LoadItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( ClearItem, XmNactivateCallback,
			activateCB_ClearItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( GetcursorItem, XmNactivateCallback,
			activateCB_GetcursorItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( SavetmpItem, XmNactivateCallback,
			activateCB_SavetmpItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( PrinterItem, XmNactivateCallback,
			activateCB_PrinterItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( StraightToggle, XmNvalueChangedCallback,
			valueChangedCB_StraightToggle,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( HistoToggle, XmNvalueChangedCallback,
			valueChangedCB_HistoToggle,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( PolynomialItem, XmNvalueChangedCallback,
			valueChangedCB_PolynomialItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( SplineItem, XmNvalueChangedCallback,
			valueChangedCB_SplineItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( NormalToggle, XmNvalueChangedCallback,
			valueChangedCB_NormalToggle,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( PortraitToggle, XmNvalueChangedCallback,
			valueChangedCB_PortraitToggle,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( Context, XmNactivateCallback,
			activateCB_Context,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( OnHelp, XmNactivateCallback,
			activateCB_OnHelp,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( QuitItem, XmNactivateCallback,
			activateCB_QuitItem,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( CurrLine, XmNfocusCallback,
			focusCB_CurrLine,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( CurrLine, XmNmodifyVerifyCallback,
			modifyVerifyCB_CurrLine,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( CurrLine, XmNlosingFocusCallback,
			losingFocusCB_CurrLine,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( Linestep, XmNfocusCallback,
			focusCB_Linestep,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( Linestep, XmNmodifyVerifyCallback,
			modifyVerifyCB_Linestep,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( Linestep, XmNlosingFocusCallback,
			losingFocusCB_Linestep,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton1, XmNactivateCallback,
			activateCB_arrowButton1,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton2, XmNactivateCallback,
			activateCB_arrowButton2,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( degree_text, XmNfocusCallback,
			focusCB_degree_text,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( degree_text, XmNmodifyVerifyCallback,
			modifyVerifyCB_degree_text,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( degree_text, XmNlosingFocusCallback,
			losingFocusCB_degree_text,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton3, XmNactivateCallback,
			activateCB_arrowButton3,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton4, XmNactivateCallback,
			activateCB_arrowButton4,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( GaussButton, XmNhelpCallback,
			helpCB_GaussButton,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( GaussButton, XmNactivateCallback,
			activateCB_GaussButton,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( IntegrateButton, XmNactivateCallback,
			activateCB_IntegrateButton,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( RebinButton, XmNhelpCallback,
			helpCB_RebinButton,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( RebinButton, XmNactivateCallback,
			activateCB_RebinButton,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( RebinButton1, XmNhelpCallback,
			helpCB_RebinButton1,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( RebinButton1, XmNactivateCallback,
			activateCB_RebinButton1,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( width_text, XmNfocusCallback,
			focusCB_width_text,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( width_text, XmNmodifyVerifyCallback,
			modifyVerifyCB_width_text,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( width_text, XmNlosingFocusCallback,
			losingFocusCB_width_text,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton11, XmNactivateCallback,
			activateCB_arrowButton11,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton12, XmNactivateCallback,
			activateCB_arrowButton12,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( GaussDrawingArea, XmNexposeCallback,
			exposeCB_GaussDrawingArea,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton8, XmNactivateCallback,
			activateCB_arrowButton8,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton6, XmNactivateCallback,
			activateCB_arrowButton6,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton5, XmNactivateCallback,
			activateCB_arrowButton5,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( arrowButton7, XmNactivateCallback,
			activateCB_arrowButton7,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( step_text, XmNfocusCallback,
			focusCB_step_text,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( step_text, XmNmodifyVerifyCallback,
			modifyVerifyCB_step_text,
			(XtPointer) UxAliceShellContext );
	UxAddCallback( step_text, XmNlosingFocusCallback,
			losingFocusCB_step_text,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( Cut_x, XmNactivateCallback,
			activateCB_Cut_x,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( Cut_y, XmNactivateCallback,
			activateCB_Cut_y,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( Move, XmNactivateCallback,
			activateCB_Move,
			(XtPointer) UxAliceShellContext );

	UxAddCallback( Unzoom, XmNactivateCallback,
			activateCB_Unzoom,
			(XtPointer) UxAliceShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( AliceShell );

	UxMainWindowSetAreas( AliceWindow, Topmenu, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, AliceForm1 );
	return ( AliceShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_AliceShell()
{
	swidget                 rtrn;
	_UxCAliceShell          *UxContext;

	UxAliceShellContext = UxContext =
		(_UxCAliceShell *) UxMalloc( sizeof(_UxCAliceShell) );

	{
		swidget create_GaussShell();
		swidget create_HelpTopLevel();
		swidget create_StatShell();
		swidget create_MessageShell();
		swidget create_sh_list1();
		swidget create_LabelOptions();
		swidget create_LabelCursor();
		swidget create_Savefiles();
		swidget create_Printer();
		swidget create_RebinShell();
		swidget create_SaveAsShell();
		swidget create_OverPlotShell();
		 
		extern swidget ListPopup;
		rtrn = _Uxbuild_AliceShell();

		create_GaussShell();
		create_HelpTopLevel();
		ListPopup = create_sh_list1();
		create_StatShell();
		create_MessageShell();
		create_LabelOptions();
		create_LabelCursor();
		create_Savefiles();
		create_Printer();
		create_RebinShell();
		create_SaveAsShell();
		create_OverPlotShell();
		init_values();
		InitWindows();
		return(rtrn);
	}
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_AliceShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "HelpTop2", action_HelpTop2 },
				{ "HelpHelp", action_HelpHelp },
				{ "clearHelpTop", action_clearHelpTop },
				{ "HelpTop1", action_HelpTop1 }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_AliceShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

