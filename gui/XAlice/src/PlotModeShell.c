/* @(#)PlotModeShell.c	19.1 (ES0-DMD) 02/25/03 13:43:40 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	PlotModeShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>
#include <alice_help.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxPlotModeShell;
	swidget	UxPlotModeForm;
	swidget	UxPlotModeRow;
	swidget	UxStraightToggle;
	swidget	UxHistoToggle;
	swidget	UxPlotModeform2;
	swidget	UxPlotModeSeparator;
} _UxCPlotModeShell;

#define PlotModeShell           UxPlotModeShellContext->UxPlotModeShell
#define PlotModeForm            UxPlotModeShellContext->UxPlotModeForm
#define PlotModeRow             UxPlotModeShellContext->UxPlotModeRow
#define StraightToggle          UxPlotModeShellContext->UxStraightToggle
#define HistoToggle             UxPlotModeShellContext->UxHistoToggle
#define PlotModeform2           UxPlotModeShellContext->UxPlotModeform2
#define PlotModeSeparator       UxPlotModeShellContext->UxPlotModeSeparator

static _UxCPlotModeShell	*UxPlotModeShellContext;

swidget	OkPloteMode;
swidget	CancelPlotMode;

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_PlotModeShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_OkPloteMode( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCPlotModeShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxPlotModeShellContext;
	UxPlotModeShellContext = UxContext =
			(_UxCPlotModeShell *) UxGetContext( UxThisWidget );
	{
	Read_plotmode();
	UxPopdownInterface(PlotModeShell);
	}
	UxPlotModeShellContext = UxSaveCtx;
}

static void	activateCB_CancelPlotMode( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCPlotModeShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxPlotModeShellContext;
	UxPlotModeShellContext = UxContext =
			(_UxCPlotModeShell *) UxGetContext( UxThisWidget );
	{
	Set_plotmode();
	UxPopdownInterface(PlotModeShell);
	}
	UxPlotModeShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_PlotModeShell()
{
	UxPutIconName( PlotModeShell, "Plot Mode" );
	UxPutHeight( PlotModeShell, 150 );
	UxPutWidth( PlotModeShell, 246 );
	UxPutY( PlotModeShell, 447 );
	UxPutX( PlotModeShell, 610 );

	UxPutBackground( PlotModeForm, WindowBackground );
	UxPutHeight( PlotModeForm, 144 );
	UxPutWidth( PlotModeForm, 246 );
	UxPutY( PlotModeForm, 50 );
	UxPutX( PlotModeForm, 76 );
	UxPutUnitType( PlotModeForm, "pixels" );
	UxPutResizePolicy( PlotModeForm, "resize_none" );

	UxPutBackground( PlotModeRow, LabelBackground );
	UxPutRadioBehavior( PlotModeRow, "true" );
	UxPutHeight( PlotModeRow, 64 );
	UxPutWidth( PlotModeRow, 140 );
	UxPutY( PlotModeRow, 20 );
	UxPutX( PlotModeRow, 50 );

	UxPutHighlightColor( StraightToggle, "#000000" );
	UxPutForeground( StraightToggle, TextForeground );
	UxPutSet( StraightToggle, "true" );
	UxPutLabelString( StraightToggle, "Straight line" );
	UxPutSelectColor( StraightToggle, SelectColor );
	UxPutFontList( StraightToggle, TextFont );
	UxPutBackground( StraightToggle, LabelBackground );
	UxPutHeight( StraightToggle, 32 );
	UxPutWidth( StraightToggle, 136 );
	UxPutY( StraightToggle, 14 );
	UxPutX( StraightToggle, 8 );

	UxPutHighlightColor( HistoToggle, "#000000" );
	UxPutForeground( HistoToggle, TextForeground );
	UxPutLabelString( HistoToggle, "Histogram - like" );
	UxPutSelectColor( HistoToggle, SelectColor );
	UxPutFontList( HistoToggle, TextFont );
	UxPutBackground( HistoToggle, LabelBackground );
	UxPutHeight( HistoToggle, 32 );
	UxPutWidth( HistoToggle, 102 );
	UxPutY( HistoToggle, 42 );
	UxPutX( HistoToggle, 10 );

	UxPutBackground( PlotModeform2, ButtonBackground );
	UxPutHeight( PlotModeform2, 56 );
	UxPutWidth( PlotModeform2, 250 );
	UxPutY( PlotModeform2, 98 );
	UxPutX( PlotModeform2, -4 );
	UxPutResizePolicy( PlotModeform2, "resize_none" );

	UxPutHighlightColor( OkPloteMode, "#000000" );
	UxPutHighlightOnEnter( OkPloteMode, "true" );
	UxPutLabelString( OkPloteMode, "Ok" );
	UxPutForeground( OkPloteMode, ButtonForeground );
	UxPutFontList( OkPloteMode, BoldTextFont );
	UxPutBackground( OkPloteMode, ButtonBackground );
	UxPutHeight( OkPloteMode, 30 );
	UxPutWidth( OkPloteMode, 90 );
	UxPutY( OkPloteMode, 9 );
	UxPutX( OkPloteMode, 27 );

	UxPutHighlightColor( CancelPlotMode, "#000000" );
	UxPutHighlightOnEnter( CancelPlotMode, "true" );
	UxPutLabelString( CancelPlotMode, "Cancel" );
	UxPutForeground( CancelPlotMode, CancelForeground );
	UxPutFontList( CancelPlotMode, BoldTextFont );
	UxPutBackground( CancelPlotMode, ButtonBackground );
	UxPutHeight( CancelPlotMode, 30 );
	UxPutWidth( CancelPlotMode, 90 );
	UxPutY( CancelPlotMode, 9 );
	UxPutX( CancelPlotMode, 137 );

	UxPutBackground( PlotModeSeparator, ButtonBackground );
	UxPutHeight( PlotModeSeparator, 8 );
	UxPutWidth( PlotModeSeparator, 246 );
	UxPutY( PlotModeSeparator, 92 );
	UxPutX( PlotModeSeparator, 2 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_PlotModeShell()
{
	/* Create the swidgets */

	PlotModeShell = UxCreateTopLevelShell( "PlotModeShell", NO_PARENT );
	UxPutContext( PlotModeShell, UxPlotModeShellContext );

	PlotModeForm = UxCreateForm( "PlotModeForm", PlotModeShell );
	PlotModeRow = UxCreateRowColumn( "PlotModeRow", PlotModeForm );
	StraightToggle = UxCreateToggleButton( "StraightToggle", PlotModeRow );
	HistoToggle = UxCreateToggleButton( "HistoToggle", PlotModeRow );
	PlotModeform2 = UxCreateForm( "PlotModeform2", PlotModeForm );
	OkPloteMode = UxCreatePushButton( "OkPloteMode", PlotModeform2 );
	CancelPlotMode = UxCreatePushButton( "CancelPlotMode", PlotModeform2 );
	PlotModeSeparator = UxCreateSeparator( "PlotModeSeparator", PlotModeForm );

	_Uxinit_PlotModeShell();

	/* Create the X widgets */

	UxCreateWidget( PlotModeShell );
	UxCreateWidget( PlotModeForm );
	UxCreateWidget( PlotModeRow );
	UxCreateWidget( StraightToggle );
	UxCreateWidget( HistoToggle );
	UxCreateWidget( PlotModeform2 );
	UxCreateWidget( OkPloteMode );
	UxCreateWidget( CancelPlotMode );
	UxCreateWidget( PlotModeSeparator );

	UxAddCallback( OkPloteMode, XmNactivateCallback,
			activateCB_OkPloteMode,
			(XtPointer) UxPlotModeShellContext );

	UxAddCallback( CancelPlotMode, XmNactivateCallback,
			activateCB_CancelPlotMode,
			(XtPointer) UxPlotModeShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( PlotModeShell );

	return ( PlotModeShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_PlotModeShell()
{
	swidget                 rtrn;
	_UxCPlotModeShell       *UxContext;

	UxPlotModeShellContext = UxContext =
		(_UxCPlotModeShell *) UxMalloc( sizeof(_UxCPlotModeShell) );

	rtrn = _Uxbuild_PlotModeShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_PlotModeShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_PlotModeShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

