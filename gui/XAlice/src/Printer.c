/* @(#)Printer.c	19.1 (ES0-DMD) 02/25/03 13:43:40 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	Printer.c

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxText.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>
#include <alice_help.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxPrinter;
	swidget	Uxform1;
	swidget	UxPrinterText;
	swidget	UxTextLabel1;
	swidget	UxSaveAsForm1;
	swidget	UxSaveAsSeparator1;
} _UxCPrinter;

#define Printer                 UxPrinterContext->UxPrinter
#define form1                   UxPrinterContext->Uxform1
#define PrinterText             UxPrinterContext->UxPrinterText
#define TextLabel1              UxPrinterContext->UxTextLabel1
#define SaveAsForm1             UxPrinterContext->UxSaveAsForm1
#define SaveAsSeparator1        UxPrinterContext->UxSaveAsSeparator1

static _UxCPrinter	*UxPrinterContext;

swidget	OkPrinter;
swidget	CancelPrinter;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable2 = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_Printer();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	createCB_PrinterText( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCPrinter             *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxPrinterContext;
	UxPrinterContext = UxContext =
			(_UxCPrinter *) UxGetContext( UxThisWidget );
	{
	{
	
	if ( getenv("PRINTER") != NULL ) {
	    strcpy(PrinterName,getenv("PRINTER"));
	    UxPutText(UxFindSwidget("PrinterText"),PrinterName );
	}
	}
	}
	UxPrinterContext = UxSaveCtx;
}

static void	activateCB_OkPrinter( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCPrinter             *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxPrinterContext;
	UxPrinterContext = UxContext =
			(_UxCPrinter *) UxGetContext( UxThisWidget );
	{
	strcpy(PrinterName,UxGetText(UxFindSwidget("PrinterText")));
	UxPopdownInterface(UxFindSwidget("Printer"));
	
	}
	UxPrinterContext = UxSaveCtx;
}

static void	activateCB_CancelPrinter( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCPrinter             *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxPrinterContext;
	UxPrinterContext = UxContext =
			(_UxCPrinter *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("Printer"));
	}
	UxPrinterContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_Printer()
{
	UxPutKeyboardFocusPolicy( Printer, "pointer" );
	UxPutHeight( Printer, 118 );
	UxPutWidth( Printer, 299 );
	UxPutY( Printer, 460 );
	UxPutX( Printer, 682 );

	UxPutBackground( form1, WindowBackground );
	UxPutHeight( form1, 121 );
	UxPutWidth( form1, 297 );
	UxPutY( form1, 68 );
	UxPutX( form1, 75 );
	UxPutUnitType( form1, "pixels" );
	UxPutResizePolicy( form1, "resize_none" );

	UxPutHighlightColor( PrinterText, "Black" );
	UxPutForeground( PrinterText, TextForeground );
	UxPutTranslations( PrinterText, transTable2 );
	UxPutFontList( PrinterText, TextFont );
	UxPutBackground( PrinterText, TextBackground );
	UxPutHeight( PrinterText, 38 );
	UxPutWidth( PrinterText, 152 );
	UxPutY( PrinterText, 14 );
	UxPutX( PrinterText, 138 );

	UxPutHighlightColor( TextLabel1, "Black" );
	UxPutForeground( TextLabel1, TextForeground );
	UxPutLabelString( TextLabel1, "Printer name :" );
	UxPutFontList( TextLabel1, TextFont );
	UxPutBackground( TextLabel1, LabelBackground );
	UxPutHeight( TextLabel1, 34 );
	UxPutWidth( TextLabel1, 110 );
	UxPutY( TextLabel1, 16 );
	UxPutX( TextLabel1, 12 );

	UxPutBackground( SaveAsForm1, ButtonBackground );
	UxPutHeight( SaveAsForm1, 52 );
	UxPutWidth( SaveAsForm1, 303 );
	UxPutY( SaveAsForm1, 66 );
	UxPutX( SaveAsForm1, 0 );
	UxPutResizePolicy( SaveAsForm1, "resize_none" );

	UxPutHighlightColor( OkPrinter, "#000000" );
	UxPutHighlightOnEnter( OkPrinter, "true" );
	UxPutLabelString( OkPrinter, "Ok" );
	UxPutForeground( OkPrinter, ButtonForeground );
	UxPutFontList( OkPrinter, BoldTextFont );
	UxPutBackground( OkPrinter, ButtonBackground );
	UxPutHeight( OkPrinter, 30 );
	UxPutWidth( OkPrinter, 90 );
	UxPutY( OkPrinter, 10 );
	UxPutX( OkPrinter, 20 );

	UxPutHighlightColor( CancelPrinter, "#000000" );
	UxPutHighlightOnEnter( CancelPrinter, "true" );
	UxPutLabelString( CancelPrinter, "Cancel" );
	UxPutForeground( CancelPrinter, CancelForeground );
	UxPutFontList( CancelPrinter, BoldTextFont );
	UxPutBackground( CancelPrinter, ButtonBackground );
	UxPutHeight( CancelPrinter, 30 );
	UxPutWidth( CancelPrinter, 90 );
	UxPutY( CancelPrinter, 10 );
	UxPutX( CancelPrinter, 126 );

	UxPutBackground( SaveAsSeparator1, ButtonBackground );
	UxPutHeight( SaveAsSeparator1, 10 );
	UxPutWidth( SaveAsSeparator1, 300 );
	UxPutY( SaveAsSeparator1, 60 );
	UxPutX( SaveAsSeparator1, -1 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_Printer()
{
	/* Create the swidgets */

	Printer = UxCreateTopLevelShell( "Printer", NO_PARENT );
	UxPutContext( Printer, UxPrinterContext );

	form1 = UxCreateForm( "form1", Printer );
	PrinterText = UxCreateText( "PrinterText", form1 );
	TextLabel1 = UxCreateLabel( "TextLabel1", form1 );
	SaveAsForm1 = UxCreateForm( "SaveAsForm1", form1 );
	OkPrinter = UxCreatePushButton( "OkPrinter", SaveAsForm1 );
	CancelPrinter = UxCreatePushButton( "CancelPrinter", SaveAsForm1 );
	SaveAsSeparator1 = UxCreateSeparator( "SaveAsSeparator1", form1 );

	_Uxinit_Printer();

	/* Create the X widgets */

	UxCreateWidget( Printer );
	UxCreateWidget( form1 );
	UxCreateWidget( PrinterText );
	createCB_PrinterText( UxGetWidget( PrinterText ),
			(XtPointer) UxPrinterContext, (XtPointer) NULL );

	UxCreateWidget( TextLabel1 );
	UxCreateWidget( SaveAsForm1 );
	UxCreateWidget( OkPrinter );
	UxCreateWidget( CancelPrinter );
	UxCreateWidget( SaveAsSeparator1 );

	UxAddCallback( OkPrinter, XmNactivateCallback,
			activateCB_OkPrinter,
			(XtPointer) UxPrinterContext );

	UxAddCallback( CancelPrinter, XmNactivateCallback,
			activateCB_CancelPrinter,
			(XtPointer) UxPrinterContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( Printer );

	return ( Printer );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_Printer()
{
	swidget                 rtrn;
	_UxCPrinter             *UxContext;

	UxPrinterContext = UxContext =
		(_UxCPrinter *) UxMalloc( sizeof(_UxCPrinter) );

	rtrn = _Uxbuild_Printer();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_Printer()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_Printer();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

