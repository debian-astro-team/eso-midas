/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090825		last modif

===========================================================================*/


/*******************************************************************************
	LabelOptions.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxText.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <alice_global.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

extern int spec();

extern void newlabels(), plot_over();



typedef	struct
{
	int	mumble;
} _UxCLabelOptions;


static _UxCLabelOptions	*UxLabelOptionsContext;

swidget	LabelOptions;
swidget	form9;
swidget	Titletext;
swidget	labelxtext;
swidget	labelytext;
swidget	label10;
swidget	label11;
swidget	label12;
swidget	OkLabelOptions;
swidget	CancelLabelOptions;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*LabelTab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_LabelOptions();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_OkLabelOptions( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelOptions        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelOptionsContext;
	UxLabelOptionsContext = UxContext =
			(_UxCLabelOptions *) UxGetContext( UxThisWidget );
	{
	newlabels();
	spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,
	       specYcen-specDy,specYcen+specDy,PLOTMODE);
	if(OverPlotMode)
	   plot_over();
	UxPopdownInterface(LabelOptions);
	}
	UxLabelOptionsContext = UxSaveCtx;
}

static void	activateCB_CancelLabelOptions( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLabelOptions        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLabelOptionsContext;
	UxLabelOptionsContext = UxContext =
			(_UxCLabelOptions *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(LabelOptions);
	}
	UxLabelOptionsContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_LabelOptions()
{
	UxPutKeyboardFocusPolicy( LabelOptions, "pointer" );
	UxPutIconName( LabelOptions, "Label Options" );
	UxPutHeight( LabelOptions, 183 );
	UxPutWidth( LabelOptions, 396 );
	UxPutY( LabelOptions, 470 );
	UxPutX( LabelOptions, 502 );

	UxPutBackground( form9, ButtonBackground );
	UxPutHeight( form9, 66 );
	UxPutWidth( form9, 208 );
	UxPutY( form9, 44 );
	UxPutX( form9, 68 );
	UxPutUnitType( form9, "pixels" );
	UxPutResizePolicy( form9, "resize_none" );

	UxPutMarginHeight( Titletext, 3 );
	UxPutHighlightColor( Titletext, "Black" );
	UxPutTranslations( Titletext, LabelTab );
	UxPutFontList( Titletext, TextFont );
	UxPutHighlightOnEnter( Titletext, "true" );
	UxPutForeground( Titletext, TextForeground );
	UxPutBackground( Titletext, TextBackground );
	UxPutHeight( Titletext, 34 );
	UxPutWidth( Titletext, 282 );
	UxPutY( Titletext, 20 );
	UxPutX( Titletext, 102 );

	UxPutMarginHeight( labelxtext, 3 );
	UxPutHighlightColor( labelxtext, "Black" );
	UxPutTranslations( labelxtext, LabelTab );
	UxPutFontList( labelxtext, TextFont );
	UxPutHighlightOnEnter( labelxtext, "true" );
	UxPutForeground( labelxtext, TextForeground );
	UxPutBackground( labelxtext, TextBackground );
	UxPutHeight( labelxtext, 34 );
	UxPutWidth( labelxtext, 282 );
	UxPutY( labelxtext, 56 );
	UxPutX( labelxtext, 102 );

	UxPutMarginHeight( labelytext, 3 );
	UxPutHighlightColor( labelytext, "Black" );
	UxPutTranslations( labelytext, LabelTab );
	UxPutFontList( labelytext, TextFont );
	UxPutHighlightOnEnter( labelytext, "true" );
	UxPutForeground( labelytext, TextForeground );
	UxPutBackground( labelytext, TextBackground );
	UxPutHeight( labelytext, 34 );
	UxPutWidth( labelytext, 282 );
	UxPutY( labelytext, 92 );
	UxPutX( labelytext, 102 );

	UxPutAlignment( label10, "alignment_beginning" );
	UxPutHighlightColor( label10, "Black" );
	UxPutFontList( label10, TextFont );
	UxPutLabelString( label10, "Title :" );
	UxPutForeground( label10, TextForeground );
	UxPutBackground( label10, ButtonBackground );
	UxPutHeight( label10, 28 );
	UxPutWidth( label10, 78 );
	UxPutY( label10, 22 );
	UxPutX( label10, 14 );

	UxPutAlignment( label11, "alignment_beginning" );
	UxPutHighlightColor( label11, "Black" );
	UxPutFontList( label11, TextFont );
	UxPutLabelString( label11, "Label X :" );
	UxPutForeground( label11, TextForeground );
	UxPutBackground( label11, ButtonBackground );
	UxPutHeight( label11, 28 );
	UxPutWidth( label11, 78 );
	UxPutY( label11, 58 );
	UxPutX( label11, 16 );

	UxPutAlignment( label12, "alignment_beginning" );
	UxPutHighlightColor( label12, "Black" );
	UxPutFontList( label12, TextFont );
	UxPutLabelString( label12, "Label Y :" );
	UxPutForeground( label12, TextForeground );
	UxPutBackground( label12, ButtonBackground );
	UxPutHeight( label12, 28 );
	UxPutWidth( label12, 78 );
	UxPutY( label12, 94 );
	UxPutX( label12, 16 );

	UxPutHighlightColor( OkLabelOptions, "Black" );
	UxPutHighlightOnEnter( OkLabelOptions, "true" );
	UxPutLabelString( OkLabelOptions, "Ok" );
	UxPutForeground( OkLabelOptions, ButtonForeground );
	UxPutFontList( OkLabelOptions, BoldTextFont );
	UxPutBackground( OkLabelOptions, ButtonBackground );
	UxPutHeight( OkLabelOptions, 30 );
	UxPutWidth( OkLabelOptions, 90 );
	UxPutY( OkLabelOptions, 138 );
	UxPutX( OkLabelOptions, 30 );

	UxPutHighlightColor( CancelLabelOptions, "Black" );
	UxPutHighlightOnEnter( CancelLabelOptions, "true" );
	UxPutLabelString( CancelLabelOptions, "Cancel" );
	UxPutForeground( CancelLabelOptions, CancelForeground );
	UxPutFontList( CancelLabelOptions, BoldTextFont );
	UxPutBackground( CancelLabelOptions, ButtonBackground );
	UxPutHeight( CancelLabelOptions, 30 );
	UxPutWidth( CancelLabelOptions, 90 );
	UxPutY( CancelLabelOptions, 138 );
	UxPutX( CancelLabelOptions, 142 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_LabelOptions()
{
	/* Create the swidgets */

	LabelOptions = UxCreateTopLevelShell( "LabelOptions", NO_PARENT );
	UxPutContext( LabelOptions, UxLabelOptionsContext );

	form9 = UxCreateForm( "form9", LabelOptions );
	Titletext = UxCreateText( "Titletext", form9 );
	labelxtext = UxCreateText( "labelxtext", form9 );
	labelytext = UxCreateText( "labelytext", form9 );
	label10 = UxCreateLabel( "label10", form9 );
	label11 = UxCreateLabel( "label11", form9 );
	label12 = UxCreateLabel( "label12", form9 );
	OkLabelOptions = UxCreatePushButton( "OkLabelOptions", form9 );
	CancelLabelOptions = UxCreatePushButton( "CancelLabelOptions", form9 );

	_Uxinit_LabelOptions();

	/* Create the X widgets */

	UxCreateWidget( LabelOptions );
	UxCreateWidget( form9 );
	UxCreateWidget( Titletext );
	UxCreateWidget( labelxtext );
	UxCreateWidget( labelytext );
	UxCreateWidget( label10 );
	UxCreateWidget( label11 );
	UxCreateWidget( label12 );
	UxCreateWidget( OkLabelOptions );
	UxCreateWidget( CancelLabelOptions );

	UxAddCallback( OkLabelOptions, XmNactivateCallback,
			activateCB_OkLabelOptions,
			(XtPointer) UxLabelOptionsContext );

	UxAddCallback( CancelLabelOptions, XmNactivateCallback,
			activateCB_CancelLabelOptions,
			(XtPointer) UxLabelOptionsContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( LabelOptions );

	return ( LabelOptions );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_LabelOptions()
{
	swidget                 rtrn;
	_UxCLabelOptions        *UxContext;

	UxLabelOptionsContext = UxContext =
		(_UxCLabelOptions *) UxMalloc( sizeof(_UxCLabelOptions) );

	rtrn = _Uxbuild_LabelOptions();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_LabelOptions()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_LabelOptions();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

