/* @(#)alice_help.h	19.1 (ESO-IPG) 02/25/03 13:43:07 */
#define IMAGELINE 	"Row of the 2-D spectrum to be used."
#define IMAGEWIDE 	"This parameter allows the average of a number of rows to be loaded instead of \nonly one."
#define IMAGEUPAR 	"The current row is changed to the next row in the spectrum."
#define IMAGEDNAR 	"The current row is changed to the previous row in the spectrum."
#define FILTERWIDE	"Size of the filter window."
#define FILTERUPAR	"The size of the filter window is increased."
#define FILTERDNAR	"The size of the filter window is decreased."
#define FITDEGREE	"Degree of the polynomial fit of the continuum."
#define FITUPAR		"The fit degree is increased."
#define FITDNAR		"The fit degree is decreased."
#define AUTOFIT		"Enable the automatic continuum fitting."
#define READLIST	"READLIST"
#define CONTINMENU	"CONTINMENU"
#define FILTERMENU	"FILTERMENU"
#define GAUSSINTER	"Open the interface for gaussian fitting."
#define INTEGRATE	"Integrate over a line defined interactively by the user in the graphic window. The \nresults go both to the Midas session and to the file TMPalice.tbl."
#define REBIN		"Open the rebinning interface."
#define CUTX		"Cut the spectrum displayed in the main window in the x direction, displaying the \nresult in the Midas graphic window. The middle button finishes the process."
#define CUTY		"Cut the spectrum displayed in the main window in the y direction, displaying the \nresult in the Midas graphic window. The middle button finishes the process."
#define MOVE		"Move the current box over the spectrum displayed in the main windo, displaying the \nresult in the Midas graphic window. The middle button finishes the process."
#define UNZOOM		"The current box covers the whole spectrum."
#define ZOOMUPARROW     "The current box grows in the y direction in one zoom step."
#define ZOOMDNARROW     "The current box shrinks in the y direction in one zoom step."
#define ZOOMRTARROW     "The current box grows in the x direction in one zoom step."
#define ZOOMLFARROW     "The current box shrinks in the x direction in one zoom step."
#define ZOOMSTEP	"Zoom step."
#define LABEL		"LABEL"
#define FRAME		"FRAME"
#define UTILSTOP	"UTILSTOP"
#define COMPONENT	"This component is used/not used as initial guess depending on the toggle button status."
#define COMPFIELD	"Number of components for the gaussian fitting process."
#define COMPUPAR	"Increase the number of components."
#define COMPDNAR	"Decrease the number of components."
#define FIX1		"Fix the amplitude."
#define FIX2		"Fix the central-x value."
#define FIX3		"Fix the sigma."
#define GAUSSINIT	"Amplitude, central-x value and sigma separated by blanks."
#define OUTFIELD	"Amplitude, central-x value and sigma separated by blanks."
#define ITERATIONMIN	"Minimum number of iterations for the gaussian fitting process."
#define ITERATIONMAX	"Maximum number of iterations for the gaussian fitting process."
#define ITERATIONCUR	"Actual number of iterations made in the gaussian fitting process."
#define CLEAR		"Clear the \"Initial Guess\" and \"Fit Values\" fields."
#define COPY		"Copy the selected \"Fit Values\" fields to  \"Initial Guess\" fields."
#define EXECUTE		"Execute the gaussian fitting using the number of components given. The components \nnot entered in the \"Initial Guess\" field are asked interactively."
#define FRAMEGAUSS	"FRAMEGAUSS"
#define UTILSGAUSS	"UTILSGAUSS"
#define RETURN		"RETURN"
#define QUIT		"QUIT"
