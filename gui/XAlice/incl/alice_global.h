/* 
.VERSION
 031017		last modif 
*/


#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#define LO 0.05
#define UP 0.95
#define MOVE_WIN 1
#define MOVE_X1   2
#define MOVE_X2   3
#define MOVE_Y1   4
#define MOVE_Y2   5
#define STEPUP_X    6
#define STEPUP_Y    7
#define STEPDN_X    8
#define STEPDN_Y    9
#define DRAW "color=1"
#define DRAWCOL "color=2"
#define NODRAW "color=0"
#define PLOTNAME "alice.plt"
#define PLOTAPP "alice.plt/a"
#define PLOTMODE 	0
#define OVERMODE	1
#define MAXVALUES 30000			 /* was:  MAXVALUES 6000 */
#define MAXWINDOWS 10
#define STANDARDFONT 0
#define QUALITYFONT 1
#define GREEKFONT   2
#define SCRIPTFONT  3
#define OLDENGFONT  4
#define TINYFONT    5
#define CASEPRG     1
#define CASEBDF     2
#define OVERBDF     3
#define ZOPT        1
#define LOGOPT      2
#define INVOPT      3
#define LINEMODE    0
#define HISTMODE    1
#define POLYFITMODE	0
#define SPLINEFITMODE	1
#define NORMALPRINT	0
#define PORTRAITPRINT	1

int 	filterModStepWin,
  	filterModWindWidth,
	filterWindWidth;

int 	specModLineNum,
	specModLineStep,
	specInputFrame,
	specAbortCursor,
    	specLineNum,
	specLineStep,
	specImno,
	specNilval,
	specNumData,
	specNaxis,
    	specNpix[16],
	specDim;

int 	specCurrCursor;
int 	flush;
int 	fitModFitDeg,
	fitAddFit,
	fitDegree,
	fitMode,
	fitPairNum;
int 	gaussModNumOfComp,
	gaussModInitGuess,
	gaussFixOpt[27],          /* boolean array */
    	gaussModMaxIterations,
	gaussModMinIterations,
	gaussNumOfComp,
    	gaussNumOfSol,
	gaussNumOfFitData,
	gaussMaxIterations,
	gaussMinIterations;

int 	plotModAngle,
	plotModSize,
	plotMode,
	plotDefaultTitle,
	plotFont,
	caseList;

int 	LOOP,
	OverPlotMode,   /* Redraw or not redraw mode            */
	ApplyOverPlot,  /* boolean TRUE => overplot was applied */
	OverPlotNum,    /* number of overplot spectrum          */
	overPos;

int 	rebinOption,
	rebinRebin;
int  	ivalues[MAXVALUES];

int  	i,
	key,
	pixel,
	MonitorPid;

float 	specX[MAXVALUES],    /* current spectrum X values */
	specY[MAXVALUES],    /* current spectrum Y values */
	specXaux[MAXVALUES], /* fit X values              */
      	specYaux[MAXVALUES], /* fit Y values              */
      	OverX[15][MAXVALUES],/* overplot spectrum X values */
	OverY[15][MAXVALUES],/* overplot spectrum Y values */
	specCuts[2],
	specClip[4],
	specDeltaCursor[100];

float 	overYmin, /* overplot Y limits */
	overYmax;

      /* Midas window */
float 	specXmax,  /* spectrum limits */
	specXmin,
	specYmax,
	specYmin,
	specStep,  /* image step      */
	specStart, /* image strat     */
       	specXcen,  /* frame central x value */
	specYcen,  /* frame central y value */
	specDx,    /* frame distance from central x to max. X */
	specDy,    /* frame distance from central x to max. Y */
	specStepWin, /* window step */

      /* AliceDrawingArea */
	specXcenw2,
	specYcenw2,
       	specDxw2,
	specDyw2,
	specXnorm,  /* normalization factor (Midas window to AliceDrawigArea)*/
	specYnorm;

float	specFluxReal; /* flux for integration command */

float 	specAxesXl, /* frame axes limits */
	specAxesXu,
	specAxesYl,
	specAxesYu;

double 	gaussErrors[27], /*  gaussian fitting errors  */
	gaussAMatrix[28][28];

double 	gaussFitValues[27],/* continuum fit values */
	fitPolyValues[20], /* continuum polynomial  fit values */
	fitXminPair[200],  /* continuum limits */
       	fitXmaxPair[200];

float 	plotSize,
	plotAngle;

double 	fitRms;

int 	unit;

char 	specImageName[80],
	specSaveName[80],
	specLastName[80],
	specFrameIdent[32];

char 	PrinterName[80];

char 	plotLabelX[80],
	plotLabelY[80],
	plotTitle[80];

Window 	wnds[MAXWINDOWS];
XPoint 	specPoints[MAXVALUES];
Pixmap 	p16[20];
Cursor 	p8[20];
