! UIMX ascii 2.0 key: 2170                                                      

*StatShell.class: topLevelShell
*StatShell.parent: NO_PARENT
*StatShell.static: false
*StatShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>
*StatShell.ispecdecl:
*StatShell.funcdecl: swidget create_StatShell()\

*StatShell.funcname: create_StatShell
*StatShell.funcdef: "swidget", "<create_StatShell>(%)"
*StatShell.icode:
*StatShell.fcode: return(rtrn);\

*StatShell.auxdecl:
*StatShell.name: StatShell
*StatShell.x: 472
*StatShell.y: 483
*StatShell.width: 456
*StatShell.height: 340
*StatShell.title: "Statistics"
*StatShell.background: ButtonBackground 
*StatShell.defaultFontList: TextFont 

*form7.class: form
*form7.parent: StatShell
*form7.static: false
*form7.name: form7
*form7.resizePolicy: "resize_none"
*form7.unitType: "pixels"
*form7.x: 10
*form7.y: 10
*form7.width: 438
*form7.height: 340
*form7.background: ButtonBackground 
*form7.buttonFontList: TextFont 
*form7.labelFontList: TextFont 
*form7.textFontList: TextFont 

*text1.class: text
*text1.parent: form7
*text1.static: false
*text1.name: text1
*text1.x: 4
*text1.y: 8
*text1.width: 446
*text1.height: 290
*text1.background: TextBackground 
*text1.editable: "false"
*text1.fontList: TextFont 
*text1.rows: 20
*text1.foreground: TextForeground 
*text1.cursorPositionVisible: "false"
*text1.highlightColor: TextForeground 
*text1.topShadowColor: WindowBackground 

*return_stat.class: pushButton
*return_stat.parent: form7
*return_stat.static: false
*return_stat.name: return_stat
*return_stat.x: 28
*return_stat.y: 298
*return_stat.width: 90
*return_stat.height: 30
*return_stat.background: ButtonBackground 
*return_stat.fontList: BoldTextFont 
*return_stat.labelString: "Ok"
*return_stat.activateCallback: {\
extern swidget StatShell;\
UxPopdownInterface(StatShell);\
}
*return_stat.foreground: ButtonForeground
*return_stat.highlightOnEnter: "true"
*return_stat.highlightColor: "Black"

