! UIMX ascii 2.0 key: 2702                                                      

*translation.table: TextListTab
*translation.parent: OverPlotShell
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation.<Key>osfDelete: delete-previous-character()

*OverPlotShell.class: topLevelShell
*OverPlotShell.parent: NO_PARENT
*OverPlotShell.static: true
*OverPlotShell.gbldecl: #include <stdio.h>\
#include <alice_global.h>\
#include <alice_help.h>\
#include <ExternResources.h>\

*OverPlotShell.ispecdecl:
*OverPlotShell.funcdecl: swidget create_OverPlotShell()\

*OverPlotShell.funcname: create_OverPlotShell
*OverPlotShell.funcdef: "swidget", "<create_OverPlotShell>(%)"
*OverPlotShell.icode:
*OverPlotShell.fcode: return(rtrn);\

*OverPlotShell.auxdecl:
*OverPlotShell.name: OverPlotShell
*OverPlotShell.x: 65
*OverPlotShell.y: 275
*OverPlotShell.width: 596
*OverPlotShell.height: 402
*OverPlotShell.iconName: "Over Plot"
*OverPlotShell.keyboardFocusPolicy: "pointer"

*OverPlotTopForm.class: form
*OverPlotTopForm.parent: OverPlotShell
*OverPlotTopForm.static: true
*OverPlotTopForm.name: OverPlotTopForm
*OverPlotTopForm.resizePolicy: "resize_none"
*OverPlotTopForm.unitType: "pixels"
*OverPlotTopForm.x: 76
*OverPlotTopForm.y: 73
*OverPlotTopForm.width: 129
*OverPlotTopForm.height: 121
*OverPlotTopForm.background: WindowBackground 

*OverPlotForm2.class: form
*OverPlotForm2.parent: OverPlotTopForm
*OverPlotForm2.static: true
*OverPlotForm2.name: OverPlotForm2
*OverPlotForm2.resizePolicy: "resize_none"
*OverPlotForm2.x: 0
*OverPlotForm2.y: 354
*OverPlotForm2.width: 612
*OverPlotForm2.height: 67
*OverPlotForm2.background: ButtonBackground 

*CancelOver.class: pushButton
*CancelOver.parent: OverPlotForm2
*CancelOver.static: false
*CancelOver.name: CancelOver
*CancelOver.x: 362
*CancelOver.y: 6
*CancelOver.width: 90
*CancelOver.height: 30
*CancelOver.background: ButtonBackground 
*CancelOver.fontList: BoldTextFont 
*CancelOver.foreground: CancelForeground 
*CancelOver.labelString: "Cancel"
*CancelOver.activateCallback: {\
UxPopdownInterface(OverPlotShell);\
}
*CancelOver.highlightOnEnter: "true"
*CancelOver.highlightColor: "#000000"

*ApplayOver.class: pushButton
*ApplayOver.parent: OverPlotForm2
*ApplayOver.static: false
*ApplayOver.name: ApplayOver
*ApplayOver.x: 31
*ApplayOver.y: 6
*ApplayOver.width: 90
*ApplayOver.height: 30
*ApplayOver.background: ButtonBackground 
*ApplayOver.fontList: BoldTextFont 
*ApplayOver.foreground: ButtonForeground 
*ApplayOver.labelString: "Apply"
*ApplayOver.highlightOnEnter: "true"
*ApplayOver.highlightColor: "#000000"
*ApplayOver.activateCallback: {\
extern Boolean XmToggleButtonGetState();\
ApplyOverPlot = TRUE;\
OverPlotMode = FALSE;\
plot_over();\
OverPlotMode = TRUE;\
ApplyOverPlot = FALSE;\
/*UxPopdownInterface(UxFindSwidget("OverPlotShell")); */\
}

*AddNewOver.class: pushButton
*AddNewOver.parent: OverPlotForm2
*AddNewOver.static: false
*AddNewOver.name: AddNewOver
*AddNewOver.x: 140
*AddNewOver.y: 6
*AddNewOver.width: 90
*AddNewOver.height: 30
*AddNewOver.background: ButtonBackground 
*AddNewOver.fontList: BoldTextFont 
*AddNewOver.foreground: ButtonForeground 
*AddNewOver.labelString: "Add new"
*AddNewOver.highlightOnEnter: "true"
*AddNewOver.highlightColor: "#000000"
*AddNewOver.activateCallback: {\
{\
extern swidget ListPopup;\
extern swidget scrolledList1;\
UxPopupInterface(ListPopup,no_grab);\
SetFileList( UxGetWidget(scrolledList1),1, "*.bdf" );\
caseList = OVERBDF;\
}\
}

*ClearOver.class: pushButton
*ClearOver.parent: OverPlotForm2
*ClearOver.static: false
*ClearOver.name: ClearOver
*ClearOver.x: 249
*ClearOver.y: 6
*ClearOver.width: 90
*ClearOver.height: 30
*ClearOver.background: ButtonBackground
*ClearOver.fontList: BoldTextFont 
*ClearOver.foreground: ApplyForeground
*ClearOver.labelString: "Clear"
*ClearOver.highlightOnEnter: "true"
*ClearOver.highlightColor: "#000000"
*ClearOver.activateCallback: {\
clear_over();\
}

*separator15.class: separator
*separator15.parent: OverPlotTopForm
*separator15.static: true
*separator15.name: separator15
*separator15.x: -3
*separator15.y: 346
*separator15.width: 617
*separator15.height: 8
*separator15.background: ButtonBackground 

*OverPlotRowInc.class: rowColumn
*OverPlotRowInc.parent: OverPlotTopForm
*OverPlotRowInc.static: true
*OverPlotRowInc.name: OverPlotRowInc
*OverPlotRowInc.x: 357
*OverPlotRowInc.y: 141
*OverPlotRowInc.width: 126
*OverPlotRowInc.height: 60
*OverPlotRowInc.radioBehavior: "true"
*OverPlotRowInc.background: WindowBackground

*Inctb1.class: toggleButton
*Inctb1.parent: OverPlotRowInc
*Inctb1.static: true
*Inctb1.name: Inctb1
*Inctb1.x: 3
*Inctb1.y: 3
*Inctb1.width: 117
*Inctb1.height: 28
*Inctb1.background: WindowBackground
*Inctb1.fontList: TextFont 
*Inctb1.foreground: TextForeground 
*Inctb1.selectColor: LabelBackground
*Inctb1.labelString: "Increase   dy :"
*Inctb1.set: "false"
*Inctb1.sensitive: "false"

*Inctb2.class: toggleButton
*Inctb2.parent: OverPlotRowInc
*Inctb2.static: true
*Inctb2.name: Inctb2
*Inctb2.x: 3
*Inctb2.y: 34
*Inctb2.width: 117
*Inctb2.height: 28
*Inctb2.background: WindowBackground
*Inctb2.fontList: TextFont 
*Inctb2.foreground: TextForeground 
*Inctb2.selectColor: LabelBackground
*Inctb2.labelString: "Automatic   n :"
*Inctb2.sensitive: "false"
*Inctb2.set: "true"

*OverPlotRowCont.class: rowColumn
*OverPlotRowCont.parent: OverPlotTopForm
*OverPlotRowCont.static: true
*OverPlotRowCont.name: OverPlotRowCont
*OverPlotRowCont.x: 357
*OverPlotRowCont.y: 72
*OverPlotRowCont.width: 96
*OverPlotRowCont.height: 57
*OverPlotRowCont.radioBehavior: "true"
*OverPlotRowCont.background: WindowBackground

*Conttb1.class: toggleButton
*Conttb1.parent: OverPlotRowCont
*Conttb1.static: true
*Conttb1.name: Conttb1
*Conttb1.x: 26
*Conttb1.y: 18
*Conttb1.width: 115
*Conttb1.height: 28
*Conttb1.background: WindowBackground
*Conttb1.fontList: TextFont 
*Conttb1.foreground: TextForeground 
*Conttb1.selectColor: LabelBackground
*Conttb1.labelString: "Substract"
*Conttb1.sensitive: "false"
*Conttb1.set: "true"

*Conttb2.class: toggleButton
*Conttb2.parent: OverPlotRowCont
*Conttb2.static: true
*Conttb2.name: Conttb2
*Conttb2.x: 26
*Conttb2.y: 49
*Conttb2.width: 115
*Conttb2.height: 28
*Conttb2.background: WindowBackground
*Conttb2.fontList: TextFont 
*Conttb2.foreground: TextForeground 
*Conttb2.selectColor: LabelBackground
*Conttb2.labelString: "Divide"
*Conttb2.sensitive: "false"

*label23.class: label
*label23.parent: OverPlotTopForm
*label23.static: true
*label23.name: label23
*label23.x: 426
*label23.y: 207
*label23.width: 85
*label23.height: 22
*label23.background: LabelBackground
*label23.labelString: "Y Scaling"
*label23.fontList: TextFont
*label23.foreground: TextForeground

*separator16.class: separator
*separator16.parent: OverPlotTopForm
*separator16.static: true
*separator16.name: separator16
*separator16.x: 345
*separator16.y: 132
*separator16.width: 130
*separator16.height: 13
*separator16.background: WindowBackground

*separator17.class: separator
*separator17.parent: OverPlotTopForm
*separator17.static: true
*separator17.name: separator17
*separator17.x: 514
*separator17.y: 213
*separator17.width: 63
*separator17.height: 13
*separator17.background: WindowBackground

*dyText.class: text
*dyText.parent: OverPlotTopForm
*dyText.static: true
*dyText.name: dyText
*dyText.x: 485
*dyText.y: 142
*dyText.width: 59
*dyText.height: 28
*dyText.background: TextBackground
*dyText.fontList: TextFont
*dyText.translations: TextListTab 
*dyText.marginHeight: 1
*dyText.marginWidth: 4
*dyText.foreground: TextForeground

*nText.class: text
*nText.parent: OverPlotTopForm
*nText.static: true
*nText.name: nText
*nText.x: 485
*nText.y: 174
*nText.width: 59
*nText.height: 28
*nText.background: TextBackground
*nText.fontList: TextFont
*nText.translations: TextListTab 
*nText.marginHeight: 1
*nText.marginWidth: 4
*nText.foreground: TextForeground

*label25.class: label
*label25.parent: OverPlotTopForm
*label25.static: true
*label25.name: label25
*label25.x: 102
*label25.y: 26
*label25.width: 202
*label25.height: 22
*label25.background: LabelBackground
*label25.labelString: "File                      Line     n"
*label25.fontList: TextFont
*label25.alignment: "alignment_end"
*label25.foreground: TextForeground

*RedrawTB.class: toggleButton
*RedrawTB.parent: OverPlotTopForm
*RedrawTB.static: true
*RedrawTB.name: RedrawTB
*RedrawTB.x: 388
*RedrawTB.y: 18
*RedrawTB.width: 88
*RedrawTB.height: 27
*RedrawTB.background: WindowBackground
*RedrawTB.fontList: TextFont
*RedrawTB.selectColor: SelectColor
*RedrawTB.labelString: "Redraw"
*RedrawTB.foreground: TextForeground
*RedrawTB.set: "true"

*ContAutoFitTb.class: toggleButton
*ContAutoFitTb.parent: OverPlotTopForm
*ContAutoFitTb.static: true
*ContAutoFitTb.name: ContAutoFitTb
*ContAutoFitTb.x: 386
*ContAutoFitTb.y: 49
*ContAutoFitTb.width: 152
*ContAutoFitTb.height: 22
*ContAutoFitTb.background: WindowBackground
*ContAutoFitTb.fontList: TextFont
*ContAutoFitTb.selectColor: SelectColor
*ContAutoFitTb.labelString: "Continuum auto-fit"
*ContAutoFitTb.foreground: TextForeground
*ContAutoFitTb.valueChangedCallback: {\
if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("ContAutoFitTb"))))\
{\
 XtSetSensitive(UxGetWidget(UxFindSwidget("Conttb1")),1);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("Conttb2")),1);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("Inctb1")),1);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("Inctb2")),1);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("dyText")),1);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("nText")),1);\
 UxPutSelectColor(UxFindSwidget("Conttb1"),SelectColor);\
 UxPutSelectColor(UxFindSwidget("Conttb2"),SelectColor);\
 UxPutSelectColor(UxFindSwidget("Inctb1"),SelectColor);\
 UxPutSelectColor(UxFindSwidget("Inctb2"),SelectColor);\
}\
else\
{\
 XtSetSensitive(UxGetWidget(UxFindSwidget("Conttb1")),0);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("Conttb2")),0);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("Inctb1")),0);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("Inctb2")),0);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("dyText")),0);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("nText")),0);\
 UxPutSelectColor(UxFindSwidget("Conttb1"),WindowBackground);\
 UxPutSelectColor(UxFindSwidget("Conttb2"),WindowBackground);\
 UxPutSelectColor(UxFindSwidget("Inctb1"),WindowBackground);\
 UxPutSelectColor(UxFindSwidget("Inctb2"),WindowBackground);\
}\
}
*ContAutoFitTb.set: "false"

*ScalingRow.class: rowColumn
*ScalingRow.parent: OverPlotTopForm
*ScalingRow.static: true
*ScalingRow.name: ScalingRow
*ScalingRow.x: 352
*ScalingRow.y: 230
*ScalingRow.width: 115
*ScalingRow.height: 112
*ScalingRow.radioBehavior: "true"
*ScalingRow.background: WindowBackground

*Scalingtb1.class: toggleButton
*Scalingtb1.parent: ScalingRow
*Scalingtb1.static: true
*Scalingtb1.name: Scalingtb1
*Scalingtb1.x: 26
*Scalingtb1.y: 18
*Scalingtb1.width: 115
*Scalingtb1.height: 28
*Scalingtb1.background: WindowBackground
*Scalingtb1.fontList: TextFont 
*Scalingtb1.foreground: TextForeground 
*Scalingtb1.selectColor: SelectColor 
*Scalingtb1.labelString: "Active"
*Scalingtb1.sensitive: "true"
*Scalingtb1.set: "false"
*Scalingtb1.armCallback: {\
 Yscaling_active();\
}

*Scalingtb2.class: toggleButton
*Scalingtb2.parent: ScalingRow
*Scalingtb2.static: true
*Scalingtb2.name: Scalingtb2
*Scalingtb2.x: 26
*Scalingtb2.y: 49
*Scalingtb2.width: 115
*Scalingtb2.height: 28
*Scalingtb2.background: WindowBackground
*Scalingtb2.fontList: TextFont 
*Scalingtb2.foreground: TextForeground 
*Scalingtb2.selectColor: SelectColor 
*Scalingtb2.labelString: "Automatic"
*Scalingtb2.sensitive: "true"
*Scalingtb2.armCallback: {\
 Yscaling_auto();\
}
*Scalingtb2.set: "true"

*Scalingtb3.class: toggleButton
*Scalingtb3.parent: ScalingRow
*Scalingtb3.static: true
*Scalingtb3.name: Scalingtb3
*Scalingtb3.x: 11
*Scalingtb3.y: 42
*Scalingtb3.width: 115
*Scalingtb3.height: 28
*Scalingtb3.background: WindowBackground
*Scalingtb3.fontList: TextFont 
*Scalingtb3.foreground: TextForeground 
*Scalingtb3.selectColor: SelectColor 
*Scalingtb3.labelString: "Manual"
*Scalingtb3.sensitive: "true"
*Scalingtb3.armCallback: {\
 Yscaling_manual();\
\
}

*yminText.class: text
*yminText.parent: OverPlotTopForm
*yminText.static: true
*yminText.name: yminText
*yminText.x: 512
*yminText.y: 243
*yminText.width: 59
*yminText.height: 28
*yminText.background: TextBackground
*yminText.fontList: TextFont
*yminText.translations: TextListTab
*yminText.marginHeight: 1
*yminText.marginWidth: 4
*yminText.foreground: TextForeground
*yminText.valueChangedCallback: {\
XFlush(UxDisplay);\
}
*yminText.losingFocusCallback: {\
UxGetText(UxFindSwidget("yminText"));\
}

*ymaxText.class: text
*ymaxText.parent: OverPlotTopForm
*ymaxText.static: true
*ymaxText.name: ymaxText
*ymaxText.x: 513
*ymaxText.y: 274
*ymaxText.width: 59
*ymaxText.height: 28
*ymaxText.background: TextBackground
*ymaxText.fontList: TextFont
*ymaxText.translations: TextListTab
*ymaxText.marginHeight: 1
*ymaxText.marginWidth: 4
*ymaxText.foreground: TextForeground
*ymaxText.losingFocusCallback: {\
UxGetText(UxFindSwidget("ymaxText"));\
}

*label24.class: label
*label24.parent: OverPlotTopForm
*label24.static: true
*label24.name: label24
*label24.x: 459
*label24.y: 245
*label24.width: 48
*label24.height: 22
*label24.background: LabelBackground
*label24.labelString: "y min :"
*label24.fontList: TextFont
*label24.alignment: "alignment_end"
*label24.foreground: TextForeground

*label26.class: label
*label26.parent: OverPlotTopForm
*label26.static: true
*label26.name: label26
*label26.x: 459
*label26.y: 277
*label26.width: 48
*label26.height: 22
*label26.background: LabelBackground
*label26.labelString: "y max :"
*label26.fontList: TextFont
*label26.alignment: "alignment_end"
*label26.foreground: TextForeground

*separator18.class: separator
*separator18.parent: OverPlotTopForm
*separator18.static: true
*separator18.name: separator18
*separator18.x: 450
*separator18.y: 233
*separator18.width: 10
*separator18.height: 90
*separator18.background: WindowBackground
*separator18.orientation: "vertical"

*OverPlotText.class: text
*OverPlotText.parent: OverPlotTopForm
*OverPlotText.static: true
*OverPlotText.name: OverPlotText
*OverPlotText.x: 30
*OverPlotText.y: 56
*OverPlotText.width: 299
*OverPlotText.height: 248
*OverPlotText.background: TextBackground
*OverPlotText.translations: ""
*OverPlotText.editMode: "multi_line_edit"
*OverPlotText.fontList: "9x15"
*OverPlotText.foreground: TextForeground 
*OverPlotText.rows: 15
*OverPlotText.columns: 30
*OverPlotText.maxLength: 1200

*separator19.class: separator
*separator19.parent: OverPlotTopForm
*separator19.static: true
*separator19.name: separator19
*separator19.x: 345
*separator19.y: 53
*separator19.width: 40
*separator19.height: 13
*separator19.background: WindowBackground

*separator20.class: separator
*separator20.parent: OverPlotTopForm
*separator20.static: true
*separator20.name: separator20
*separator20.x: 538
*separator20.y: 53
*separator20.width: 40
*separator20.height: 13
*separator20.background: WindowBackground

*SelOverTb1.class: toggleButton
*SelOverTb1.parent: OverPlotTopForm
*SelOverTb1.static: true
*SelOverTb1.name: SelOverTb1
*SelOverTb1.x: 8
*SelOverTb1.y: 67
*SelOverTb1.width: 20
*SelOverTb1.height: 14
*SelOverTb1.background: WindowBackground
*SelOverTb1.fontList: TextFont
*SelOverTb1.selectColor: SelectColor
*SelOverTb1.labelString: ""
*SelOverTb1.foreground: TextForeground
*SelOverTb1.indicatorSize: 12
*SelOverTb1.marginLeft: 2
*SelOverTb1.set: "true"
*SelOverTb1.mappedWhenManaged: "false"

*SelOverTb2.class: toggleButton
*SelOverTb2.parent: OverPlotTopForm
*SelOverTb2.static: true
*SelOverTb2.name: SelOverTb2
*SelOverTb2.x: 8
*SelOverTb2.y: 82
*SelOverTb2.width: 20
*SelOverTb2.height: 14
*SelOverTb2.background: WindowBackground
*SelOverTb2.fontList: TextFont
*SelOverTb2.selectColor: SelectColor
*SelOverTb2.labelString: ""
*SelOverTb2.foreground: TextForeground
*SelOverTb2.indicatorSize: 12
*SelOverTb2.marginLeft: 2
*SelOverTb2.set: "true"
*SelOverTb2.mappedWhenManaged: "false"

*SelOverTb3.class: toggleButton
*SelOverTb3.parent: OverPlotTopForm
*SelOverTb3.static: true
*SelOverTb3.name: SelOverTb3
*SelOverTb3.x: 8
*SelOverTb3.y: 97
*SelOverTb3.width: 20
*SelOverTb3.height: 14
*SelOverTb3.background: WindowBackground
*SelOverTb3.fontList: TextFont
*SelOverTb3.selectColor: SelectColor
*SelOverTb3.labelString: ""
*SelOverTb3.foreground: TextForeground
*SelOverTb3.indicatorSize: 12
*SelOverTb3.marginLeft: 2
*SelOverTb3.set: "true"
*SelOverTb3.mappedWhenManaged: "false"

*SelOverTb4.class: toggleButton
*SelOverTb4.parent: OverPlotTopForm
*SelOverTb4.static: true
*SelOverTb4.name: SelOverTb4
*SelOverTb4.x: 8
*SelOverTb4.y: 112
*SelOverTb4.width: 20
*SelOverTb4.height: 14
*SelOverTb4.background: WindowBackground
*SelOverTb4.fontList: TextFont
*SelOverTb4.selectColor: SelectColor
*SelOverTb4.labelString: ""
*SelOverTb4.foreground: TextForeground
*SelOverTb4.indicatorSize: 12
*SelOverTb4.marginLeft: 2
*SelOverTb4.set: "true"
*SelOverTb4.mappedWhenManaged: "false"

*SelOverTb5.class: toggleButton
*SelOverTb5.parent: OverPlotTopForm
*SelOverTb5.static: true
*SelOverTb5.name: SelOverTb5
*SelOverTb5.x: 8
*SelOverTb5.y: 127
*SelOverTb5.width: 20
*SelOverTb5.height: 14
*SelOverTb5.background: WindowBackground
*SelOverTb5.fontList: TextFont
*SelOverTb5.selectColor: SelectColor
*SelOverTb5.labelString: ""
*SelOverTb5.foreground: TextForeground
*SelOverTb5.indicatorSize: 12
*SelOverTb5.marginLeft: 2
*SelOverTb5.set: "true"
*SelOverTb5.mappedWhenManaged: "false"

*SelOverTb6.class: toggleButton
*SelOverTb6.parent: OverPlotTopForm
*SelOverTb6.static: true
*SelOverTb6.name: SelOverTb6
*SelOverTb6.x: 8
*SelOverTb6.y: 142
*SelOverTb6.width: 20
*SelOverTb6.height: 14
*SelOverTb6.background: WindowBackground
*SelOverTb6.fontList: TextFont
*SelOverTb6.selectColor: SelectColor
*SelOverTb6.labelString: ""
*SelOverTb6.foreground: TextForeground
*SelOverTb6.indicatorSize: 12
*SelOverTb6.marginLeft: 2
*SelOverTb6.set: "true"
*SelOverTb6.mappedWhenManaged: "false"

*SelOverTb7.class: toggleButton
*SelOverTb7.parent: OverPlotTopForm
*SelOverTb7.static: true
*SelOverTb7.name: SelOverTb7
*SelOverTb7.x: 8
*SelOverTb7.y: 157
*SelOverTb7.width: 20
*SelOverTb7.height: 14
*SelOverTb7.background: WindowBackground
*SelOverTb7.fontList: TextFont
*SelOverTb7.selectColor: SelectColor
*SelOverTb7.labelString: ""
*SelOverTb7.foreground: TextForeground
*SelOverTb7.indicatorSize: 12
*SelOverTb7.marginLeft: 2
*SelOverTb7.set: "true"
*SelOverTb7.mappedWhenManaged: "false"

*SelOverTb8.class: toggleButton
*SelOverTb8.parent: OverPlotTopForm
*SelOverTb8.static: true
*SelOverTb8.name: SelOverTb8
*SelOverTb8.x: 8
*SelOverTb8.y: 172
*SelOverTb8.width: 20
*SelOverTb8.height: 14
*SelOverTb8.background: WindowBackground
*SelOverTb8.fontList: TextFont
*SelOverTb8.selectColor: SelectColor
*SelOverTb8.labelString: ""
*SelOverTb8.foreground: TextForeground
*SelOverTb8.indicatorSize: 12
*SelOverTb8.marginLeft: 2
*SelOverTb8.set: "true"
*SelOverTb8.mappedWhenManaged: "false"

*SelOverTb9.class: toggleButton
*SelOverTb9.parent: OverPlotTopForm
*SelOverTb9.static: true
*SelOverTb9.name: SelOverTb9
*SelOverTb9.x: 8
*SelOverTb9.y: 187
*SelOverTb9.width: 20
*SelOverTb9.height: 14
*SelOverTb9.background: WindowBackground
*SelOverTb9.fontList: TextFont
*SelOverTb9.selectColor: SelectColor
*SelOverTb9.labelString: ""
*SelOverTb9.foreground: TextForeground
*SelOverTb9.indicatorSize: 12
*SelOverTb9.marginLeft: 2
*SelOverTb9.set: "true"
*SelOverTb9.mappedWhenManaged: "false"

*SelOverTb10.class: toggleButton
*SelOverTb10.parent: OverPlotTopForm
*SelOverTb10.static: true
*SelOverTb10.name: SelOverTb10
*SelOverTb10.x: 8
*SelOverTb10.y: 202
*SelOverTb10.width: 20
*SelOverTb10.height: 14
*SelOverTb10.background: WindowBackground
*SelOverTb10.fontList: TextFont
*SelOverTb10.selectColor: SelectColor
*SelOverTb10.labelString: ""
*SelOverTb10.foreground: TextForeground
*SelOverTb10.indicatorSize: 12
*SelOverTb10.marginLeft: 2
*SelOverTb10.set: "true"
*SelOverTb10.mappedWhenManaged: "false"

*SelOverTb11.class: toggleButton
*SelOverTb11.parent: OverPlotTopForm
*SelOverTb11.static: true
*SelOverTb11.name: SelOverTb11
*SelOverTb11.x: 8
*SelOverTb11.y: 217
*SelOverTb11.width: 20
*SelOverTb11.height: 14
*SelOverTb11.background: WindowBackground
*SelOverTb11.fontList: TextFont
*SelOverTb11.selectColor: SelectColor
*SelOverTb11.labelString: ""
*SelOverTb11.foreground: TextForeground
*SelOverTb11.indicatorSize: 12
*SelOverTb11.marginLeft: 2
*SelOverTb11.set: "true"
*SelOverTb11.mappedWhenManaged: "false"

*SelOverTb12.class: toggleButton
*SelOverTb12.parent: OverPlotTopForm
*SelOverTb12.static: true
*SelOverTb12.name: SelOverTb12
*SelOverTb12.x: 8
*SelOverTb12.y: 232
*SelOverTb12.width: 20
*SelOverTb12.height: 14
*SelOverTb12.background: WindowBackground
*SelOverTb12.fontList: TextFont
*SelOverTb12.selectColor: SelectColor
*SelOverTb12.labelString: ""
*SelOverTb12.foreground: TextForeground
*SelOverTb12.indicatorSize: 12
*SelOverTb12.marginLeft: 2
*SelOverTb12.set: "true"
*SelOverTb12.mappedWhenManaged: "false"

*SelOverTb13.class: toggleButton
*SelOverTb13.parent: OverPlotTopForm
*SelOverTb13.static: true
*SelOverTb13.name: SelOverTb13
*SelOverTb13.x: 8
*SelOverTb13.y: 247
*SelOverTb13.width: 20
*SelOverTb13.height: 14
*SelOverTb13.background: WindowBackground
*SelOverTb13.fontList: TextFont
*SelOverTb13.selectColor: SelectColor
*SelOverTb13.labelString: ""
*SelOverTb13.foreground: TextForeground
*SelOverTb13.indicatorSize: 12
*SelOverTb13.marginLeft: 2
*SelOverTb13.set: "true"
*SelOverTb13.mappedWhenManaged: "false"

*SelOverTb14.class: toggleButton
*SelOverTb14.parent: OverPlotTopForm
*SelOverTb14.static: true
*SelOverTb14.name: SelOverTb14
*SelOverTb14.x: 8
*SelOverTb14.y: 262
*SelOverTb14.width: 20
*SelOverTb14.height: 14
*SelOverTb14.background: WindowBackground
*SelOverTb14.fontList: TextFont
*SelOverTb14.selectColor: SelectColor
*SelOverTb14.labelString: ""
*SelOverTb14.foreground: TextForeground
*SelOverTb14.indicatorSize: 12
*SelOverTb14.marginLeft: 2
*SelOverTb14.set: "true"
*SelOverTb14.mappedWhenManaged: "false"

*SelOverTb15.class: toggleButton
*SelOverTb15.parent: OverPlotTopForm
*SelOverTb15.static: true
*SelOverTb15.name: SelOverTb15
*SelOverTb15.x: 8
*SelOverTb15.y: 277
*SelOverTb15.width: 20
*SelOverTb15.height: 14
*SelOverTb15.background: WindowBackground
*SelOverTb15.fontList: TextFont
*SelOverTb15.selectColor: SelectColor
*SelOverTb15.labelString: ""
*SelOverTb15.foreground: TextForeground
*SelOverTb15.indicatorSize: 12
*SelOverTb15.marginLeft: 2
*SelOverTb15.set: "true"
*SelOverTb15.mappedWhenManaged: "false"

*separator21.class: separator
*separator21.parent: OverPlotTopForm
*separator21.static: true
*separator21.name: separator21
*separator21.x: 345
*separator21.y: 213
*separator21.width: 82
*separator21.height: 13
*separator21.background: WindowBackground

*separator22.class: separator
*separator22.parent: OverPlotTopForm
*separator22.static: true
*separator22.name: separator22
*separator22.x: 338
*separator22.y: 12
*separator22.width: 10
*separator22.height: 322
*separator22.background: WindowBackground
*separator22.orientation: "vertical"

*separator23.class: separator
*separator23.parent: OverPlotTopForm
*separator23.static: true
*separator23.name: separator23
*separator23.x: 574
*separator23.y: 11
*separator23.width: 10
*separator23.height: 323
*separator23.background: WindowBackground
*separator23.orientation: "vertical"

*separator24.class: separator
*separator24.parent: OverPlotTopForm
*separator24.static: true
*separator24.name: separator24
*separator24.x: 345
*separator24.y: 328
*separator24.width: 231
*separator24.height: 13
*separator24.background: WindowBackground

*separator25.class: separator
*separator25.parent: OverPlotTopForm
*separator25.static: true
*separator25.name: separator25
*separator25.x: 345
*separator25.y: 7
*separator25.width: 231
*separator25.height: 13
*separator25.background: WindowBackground

