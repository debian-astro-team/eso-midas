! UIMX ascii 2.0 key: 3544                                                      

*translation.table: ZTab
*translation.parent: RebinShell
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation. <Key>osfDelete: delete-previous-character()

*RebinShell.class: topLevelShell
*RebinShell.parent: NO_PARENT
*RebinShell.static: true
*RebinShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>\

*RebinShell.ispecdecl:
*RebinShell.funcdecl: swidget create_RebinShell()\

*RebinShell.funcname: create_RebinShell
*RebinShell.funcdef: "swidget", "<create_RebinShell>(%)"
*RebinShell.icode:
*RebinShell.fcode: return(rtrn);\

*RebinShell.auxdecl:
*RebinShell.name: RebinShell
*RebinShell.x: 579
*RebinShell.y: 484
*RebinShell.width: 306
*RebinShell.height: 166
*RebinShell.background: WindowBackground
*RebinShell.keyboardFocusPolicy: "pointer"
*RebinShell.iconName: "Rebin"

*RebinTopForm.class: form
*RebinTopForm.parent: RebinShell
*RebinTopForm.static: true
*RebinTopForm.name: RebinTopForm
*RebinTopForm.resizePolicy: "resize_none"
*RebinTopForm.unitType: "pixels"
*RebinTopForm.x: -2
*RebinTopForm.y: -3
*RebinTopForm.width: 306
*RebinTopForm.height: 170
*RebinTopForm.background: WindowBackground 

*RebinRowColumn.class: rowColumn
*RebinRowColumn.parent: RebinTopForm
*RebinRowColumn.static: true
*RebinRowColumn.name: RebinRowColumn
*RebinRowColumn.x: 26
*RebinRowColumn.y: 14
*RebinRowColumn.width: 130
*RebinRowColumn.height: 92
*RebinRowColumn.radioBehavior: "true"
*RebinRowColumn.background: LabelBackground 

*Rebintb1.class: toggleButton
*Rebintb1.parent: RebinRowColumn
*Rebintb1.static: true
*Rebintb1.name: Rebintb1
*Rebintb1.x: 64
*Rebintb1.y: 32
*Rebintb1.width: 66
*Rebintb1.height: 22
*Rebintb1.background: LabelBackground 
*Rebintb1.selectColor: SelectColor 
*Rebintb1.indicatorOn: "true"
*Rebintb1.labelString: " x ` = x / (1+z)"
*Rebintb1.armCallback: {\
rebinOption= ZOPT;\
}
*Rebintb1.set: "true"
*Rebintb1.createCallback: {\
rebinOption = ZOPT;\
}
*Rebintb1.fontList: TextFont 
*Rebintb1.foreground: TextForeground 
*Rebintb1.highlightColor: "Black"

*Rebintb2.class: toggleButton
*Rebintb2.parent: RebinRowColumn
*Rebintb2.static: true
*Rebintb2.name: Rebintb2
*Rebintb2.x: 22
*Rebintb2.y: 32
*Rebintb2.width: 86
*Rebintb2.height: 22
*Rebintb2.background: LabelBackground 
*Rebintb2.selectColor: SelectColor 
*Rebintb2.indicatorOn: "true"
*Rebintb2.labelString: " x ` = log(x)"
*Rebintb2.armCallback: {\
rebinOption=LOGOPT;\
}
*Rebintb2.fontList: TextFont 
*Rebintb2.foreground: TextForeground 
*Rebintb2.highlightColor: "Black"

*Rebintb3.class: toggleButton
*Rebintb3.parent: RebinRowColumn
*Rebintb3.static: true
*Rebintb3.name: Rebintb3
*Rebintb3.x: 26
*Rebintb3.y: 60
*Rebintb3.width: 48
*Rebintb3.height: 20
*Rebintb3.background: LabelBackground 
*Rebintb3.selectColor: SelectColor 
*Rebintb3.indicatorOn: "true"
*Rebintb3.labelString: " x ` = 1/x"
*Rebintb3.armCallback: {\
rebinOption = INVOPT;\
}
*Rebintb3.fontList: TextFont 
*Rebintb3.foreground: TextForeground 
*Rebintb3.highlightColor: "Black"

*ZRebintext.class: text
*ZRebintext.parent: RebinTopForm
*ZRebintext.static: true
*ZRebintext.name: ZRebintext
*ZRebintext.x: 198
*ZRebintext.y: 12
*ZRebintext.width: 96
*ZRebintext.height: 32
*ZRebintext.background: TextBackground 
*ZRebintext.fontList: TextFont 
*ZRebintext.translations: ZTab
*ZRebintext.foreground: TextForeground 
*ZRebintext.highlightColor: "Black"
*ZRebintext.topShadowColor: LabelBackground
*ZRebintext.marginHeight: 3

*ZRebinLabel.class: label
*ZRebinLabel.parent: RebinTopForm
*ZRebinLabel.static: true
*ZRebinLabel.name: ZRebinLabel
*ZRebinLabel.x: 176
*ZRebinLabel.y: 16
*ZRebinLabel.width: 24
*ZRebinLabel.height: 26
*ZRebinLabel.background: LabelBackground 
*ZRebinLabel.labelString: "z:"
*ZRebinLabel.fontList: TextFont 
*ZRebinLabel.labelType: "string"
*ZRebinLabel.foreground: TextForeground 
*ZRebinLabel.highlightColor: "Black"

*RebinForm2.class: form
*RebinForm2.parent: RebinTopForm
*RebinForm2.static: true
*RebinForm2.name: RebinForm2
*RebinForm2.resizePolicy: "resize_none"
*RebinForm2.x: -2
*RebinForm2.y: 116
*RebinForm2.width: 310
*RebinForm2.height: 50
*RebinForm2.background: ButtonBackground 

*OkRebin.class: pushButton
*OkRebin.parent: RebinForm2
*OkRebin.static: false
*OkRebin.name: OkRebin
*OkRebin.x: 24
*OkRebin.y: 10
*OkRebin.width: 90
*OkRebin.height: 30
*OkRebin.background: ButtonBackground 
*OkRebin.fontList: BoldTextFont 
*OkRebin.foreground: ButtonForeground 
*OkRebin.labelString: "Ok"
*OkRebin.highlightOnEnter: "true"
*OkRebin.activateCallback: {\
float z,X[MAXVALUES],Y[MAXVALUES];\
int i;\
sscanf(UxGetText(UxFindSwidget("ZRebintext")),"%f",&z);\
for(i=0;i<specNpix[0] ; i++)\
 {\
  X[i] = specX[i];\
  Y[i] = specY[i]; \
 }\
rebin(rebinOption,X,specX,Y,specY,specNpix[0],z,&specXmin,\
      &specXmax,&specYmin,&specYmax);\
rebinRebin =TRUE;\
UxPopdownInterface(UxFindSwidget("RebinShell"));\
specYcen = (specYmin+specYmax)/2;\
specDy    = specYmax - specYcen;\
specYcenw2 = specYcen;\
specDyw2 = specDy;\
specXcen = (specXmin+specXmax)/2;\
specDx    = specXmax - specXcen;\
specDxw2  = specDx;\
specXcenw2 = specXcen;\
spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
     specYcen-specDy,specYcen+specDy,PLOTMODE);\
load_points();\
draw_zoom(); \
}
*OkRebin.highlightColor: "#000000"

*CancelRebin.class: pushButton
*CancelRebin.parent: RebinForm2
*CancelRebin.static: false
*CancelRebin.name: CancelRebin
*CancelRebin.x: 136
*CancelRebin.y: 10
*CancelRebin.width: 90
*CancelRebin.height: 30
*CancelRebin.background: ButtonBackground 
*CancelRebin.fontList: BoldTextFont 
*CancelRebin.foreground: CancelForeground 
*CancelRebin.labelString: "Cancel"
*CancelRebin.activateCallback: {\
UxPopdownInterface(RebinShell);\
}
*CancelRebin.highlightOnEnter: "true"
*CancelRebin.highlightColor: "#000000"

*separator4.class: separator
*separator4.parent: RebinTopForm
*separator4.static: true
*separator4.name: separator4
*separator4.x: 2
*separator4.y: 110
*separator4.width: 306
*separator4.height: 8
*separator4.background: ButtonBackground 

