! UIMX ascii 2.0 key: 5708                                                      
*action.HelpTop1: {\
 if( UxWidget == UxGetWidget(arrowButton1))\
  {UxPutText(UxFindSwidget("help_text_top"),IMAGEUPAR);\
   UxPutText(UxFindSwidget("HelpTopLevel"),IMAGEUPAR);}\
 else if( UxWidget == UxGetWidget(arrowButton2))\
  UxPutText(UxFindSwidget("help_text_top"),IMAGEDNAR);\
 else if( UxWidget == UxGetWidget(arrowButton11))\
  UxPutText(UxFindSwidget("help_text_top"),FILTERUPAR);\
 else if( UxWidget == UxGetWidget(arrowButton12))\
  UxPutText(UxFindSwidget("help_text_top"),FILTERDNAR);\
 else if( UxWidget == UxGetWidget(arrowButton3))\
  UxPutText(UxFindSwidget("help_text_top"),FITUPAR);\
 else if( UxWidget == UxGetWidget(arrowButton4))\
  UxPutText(UxFindSwidget("help_text_top"),FITDNAR);\
 else if( UxWidget == UxGetWidget(AutoFitTButton))\
  UxPutText(UxFindSwidget("help_text_top"),AUTOFIT);\
 else if( UxWidget == UxGetWidget(GaussButton))\
  UxPutText(UxFindSwidget("help_text_top"),GAUSSINTER);\
 else if( UxWidget == UxGetWidget(IntegrateButton))\
  UxPutText(UxFindSwidget("help_text_top"),INTEGRATE);\
 else if( UxWidget == UxGetWidget(Cut_x))\
  UxPutText(UxFindSwidget("help_text_top"),CUTX);\
else if( UxWidget == UxGetWidget(Cut_y))\
  UxPutText(UxFindSwidget("help_text_top"),CUTY);\
else if( UxWidget == UxGetWidget(Move))\
  UxPutText(UxFindSwidget("help_text_top"),MOVE);\
else if( UxWidget == UxGetWidget(Unzoom))\
  UxPutText(UxFindSwidget("help_text_top"),UNZOOM);\
else if( UxWidget == UxGetWidget(arrowButton7))\
  UxPutText(UxFindSwidget("help_text_top"),ZOOMUPARROW);\
 else if( UxWidget == UxGetWidget(arrowButton8))\
  UxPutText(UxFindSwidget("help_text_top"),ZOOMLFARROW );\
 else if( UxWidget == UxGetWidget(arrowButton6))\
  UxPutText(UxFindSwidget("help_text_top"), ZOOMDNARROW);\
 else if( UxWidget == UxGetWidget(arrowButton5))\
  UxPutText(UxFindSwidget("help_text_top"), ZOOMRTARROW);\
else if( UxWidget == UxGetWidget(step_text))\
  UxPutText(UxFindSwidget("help_text_top"), ZOOMSTEP);\
 else if( UxWidget == UxGetWidget(RebinButton))\
  UxPutText(UxFindSwidget("help_text_top"), REBIN);\
}  
*action.clearHelpTop: {UxPutText(UxFindSwidget("help_text_top"),"");\
 UxPutText(UxFindSwidget("HelpTopLevel"),"");}
*action.HelpHelp: DisplayExtendedHelp(UxWidget); 
*action.HelpTop2: {\
 if( UxWidget == UxGetWidget(arrowButton1))\
  {UxPutText(UxFindSwidget("help_text_top"),IMAGEUPAR);\
   UxPutText(UxFindSwidget("HelpTopLevel"),IMAGEUPAR);}\
 else if( UxWidget == UxGetWidget(arrowButton2))\
  UxPutText(UxFindSwidget("help_text_top"),IMAGEDNAR);\
 else if( UxWidget == UxGetWidget(arrowButton11))\
  UxPutText(UxFindSwidget("help_text_top"),FILTERUPAR);\
 else if( UxWidget == UxGetWidget(arrowButton12))\
  UxPutText(UxFindSwidget("help_text_top"),FILTERDNAR);\
 else if( UxWidget == UxGetWidget(arrowButton3))\
  UxPutText(UxFindSwidget("help_text_top"),FITUPAR);\
 else if( UxWidget == UxGetWidget(arrowButton4))\
  UxPutText(UxFindSwidget("help_text_top"),FITDNAR);\
 else if( UxWidget == UxGetWidget(AutoFitTButton))\
  UxPutText(UxFindSwidget("help_text_top"),AUTOFIT);\
 else if( UxWidget == UxGetWidget(GaussButton))\
  UxPutText(UxFindSwidget("help_text_top"),GAUSSINTER);\
 else if( UxWidget == UxGetWidget(IntegrateButton))\
  UxPutText(UxFindSwidget("help_text_top"),INTEGRATE);\
 else if( UxWidget == UxGetWidget(Cut_x))\
  UxPutText(UxFindSwidget("help_text_top"),CUTX);\
else if( UxWidget == UxGetWidget(Cut_y))\
  UxPutText(UxFindSwidget("help_text_top"),CUTY);\
else if( UxWidget == UxGetWidget(Move))\
  UxPutText(UxFindSwidget("help_text_top"),MOVE);\
else if( UxWidget == UxGetWidget(Unzoom))\
  UxPutText(UxFindSwidget("help_text_top"),UNZOOM);\
else if( UxWidget == UxGetWidget(arrowButton7))\
  UxPutText(UxFindSwidget("help_text_top"),ZOOMUPARROW);\
 else if( UxWidget == UxGetWidget(arrowButton8))\
  UxPutText(UxFindSwidget("help_text_top"),ZOOMLFARROW );\
 else if( UxWidget == UxGetWidget(arrowButton6))\
  UxPutText(UxFindSwidget("help_text_top"), ZOOMDNARROW);\
 else if( UxWidget == UxGetWidget(arrowButton5))\
  UxPutText(UxFindSwidget("help_text_top"), ZOOMRTARROW);\
else if( UxWidget == UxGetWidget(step_text))\
  UxPutText(UxFindSwidget("help_text_top"), ZOOMSTEP);\
 else if( UxWidget == UxGetWidget(RebinButton1))\
  UxPutText(UxFindSwidget("help_text_top"), REBIN);\
}  

*translation.table: HELPTopTab
*translation.parent: AliceShell
*translation.policy: override
*translation.<EnterWindow>: HelpTop1()
*translation.<LeaveWindow>: clearHelpTop()

*translation.table: TextTab
*translation.parent: AliceShell
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation. <Key>osfDelete: delete-previous-character()

*translation.table: Help
*translation.parent: AliceShell
*translation.policy: override
*translation.<Btn3Down>: HelpHelp()
*translation.<EnterWindow>: HelpTop1()
*translation.<LeaveWindow>: clearHelpTop()

*translation.table: transTable1
*translation.parent: AliceShell
*translation.policy: override
*translation.<EnterWindow>: HelpTop2()
*translation.<LeaveWindow>: clearHelpTop()

*AliceShell.class: applicationShell
*AliceShell.parent: NO_PARENT
*AliceShell.static: false
*AliceShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>\
#include <alice_help.h>\
\

*AliceShell.ispecdecl:
*AliceShell.funcdecl: swidget create_AliceShell()\

*AliceShell.funcname: create_AliceShell
*AliceShell.funcdef: "swidget", "<create_AliceShell>(%)"
*AliceShell.icode: swidget create_GaussShell();\
swidget create_HelpTopLevel();\
swidget create_StatShell();\
swidget create_MessageShell();\
swidget create_sh_list1();\
swidget create_LabelOptions();\
swidget create_LabelCursor();\
swidget create_Savefiles();\
swidget create_Printer();\
swidget create_RebinShell();\
swidget create_SaveAsShell();\
swidget create_OverPlotShell();\
 \
extern swidget ListPopup; 
*AliceShell.fcode: create_GaussShell();\
create_HelpTopLevel();\
ListPopup = create_sh_list1();\
create_StatShell();\
create_MessageShell();\
create_LabelOptions();\
create_LabelCursor();\
create_Savefiles();\
create_Printer();\
create_RebinShell();\
create_SaveAsShell();\
create_OverPlotShell();\
init_values();\
InitWindows();\
return(rtrn);\

*AliceShell.auxdecl:
*AliceShell.name: AliceShell
*AliceShell.x: 478
*AliceShell.y: 425
*AliceShell.width: 561
*AliceShell.height: 469
*AliceShell.keyboardFocusPolicy: "pointer"
*AliceShell.title: "Alice - Spectrum Analysis"
*AliceShell.defaultFontList: BoldTextFont  

*AliceWindow.class: mainWindow
*AliceWindow.parent: AliceShell
*AliceWindow.static: true
*AliceWindow.name: AliceWindow
*AliceWindow.unitType: "pixels"
*AliceWindow.x: 13
*AliceWindow.y: 0
*AliceWindow.width: 106
*AliceWindow.height: 72
*AliceWindow.background: WindowBackground 

*Topmenu.class: rowColumn
*Topmenu.parent: AliceWindow
*Topmenu.static: true
*Topmenu.name: Topmenu
*Topmenu.rowColumnType: "menu_bar"
*Topmenu.background: MenuBackground
*Topmenu.menuAccelerator: "<KeyUp>F10"
*Topmenu.menuHelpWidget: "QuitMenu"

*FilePane.class: rowColumn
*FilePane.parent: Topmenu
*FilePane.static: true
*FilePane.name: FilePane
*FilePane.rowColumnType: "menu_pulldown"
*FilePane.background: MenuBackground
*FilePane.highlightColor: "Black"
*FilePane.foreground: MenuForeground

*ReadItem.class: pushButton
*ReadItem.parent: FilePane
*ReadItem.static: true
*ReadItem.name: ReadItem
*ReadItem.labelString: "Open ..."
*ReadItem.mnemonic: "O"
*ReadItem.background: MenuBackground
*ReadItem.activateCallback: {\
extern swidget ListPopup;\
extern swidget scrolledList1;\
UxPopupInterface(ListPopup,no_grab);\
SetFileList( UxGetWidget(scrolledList1),1, "*.bdf" );\
if(!specInputFrame)\
 {\
 AppendDialogText("cre/gra 0 1000,400,40,500");\
 AppendDialogText("SET/GCURSOR ? C_HAIR");\
 }\
specInputFrame = TRUE;\
caseList = CASEBDF;\
}
*ReadItem.fontList: BoldTextFont 
*ReadItem.foreground: MenuForeground

*SaveAsItem.class: pushButton
*SaveAsItem.parent: FilePane
*SaveAsItem.static: true
*SaveAsItem.name: SaveAsItem
*SaveAsItem.labelString: "Save As ..."
*SaveAsItem.background: MenuBackground
*SaveAsItem.mnemonic: "A"
*SaveAsItem.fontList: BoldTextFont 
*SaveAsItem.foreground: MenuForeground
*SaveAsItem.activateCallback: {\
UxPopupInterface(UxFindSwidget("SaveAsShell"),no_grab);\
 \
}

*SaveItem.class: pushButton
*SaveItem.parent: FilePane
*SaveItem.static: true
*SaveItem.name: SaveItem
*SaveItem.labelString: "Save"
*SaveItem.mnemonic: "S"
*SaveItem.background: MenuBackground
*SaveItem.fontList: BoldTextFont 
*SaveItem.foreground: MenuForeground
*SaveItem.activateCallback: {\
save_file(specSaveName);\
save_TMP();\
}

*FramePane.class: rowColumn
*FramePane.parent: Topmenu
*FramePane.static: true
*FramePane.name: FramePane
*FramePane.rowColumnType: "menu_pulldown"
*FramePane.background: MenuBackground
*FramePane.highlightColor: "Black"
*FramePane.foreground: MenuForeground

*CutxItem.class: pushButton
*CutxItem.parent: FramePane
*CutxItem.static: true
*CutxItem.name: CutxItem
*CutxItem.labelString: "Cut X"
*CutxItem.mnemonic: "X"
*CutxItem.background: MenuBackground
*CutxItem.activateCallback: {\
if(specInputFrame)\
 zoom(MOVE_X2);\
else\
 noframe_error();\
}
*CutxItem.fontList: BoldTextFont 
*CutxItem.foreground: MenuForeground

*CutyItem.class: pushButton
*CutyItem.parent: FramePane
*CutyItem.static: true
*CutyItem.name: CutyItem
*CutyItem.labelString: "Cut Y"
*CutyItem.mnemonic: "Y"
*CutyItem.background: MenuBackground
*CutyItem.activateCallback: {\
if(specInputFrame)\
 zoom(MOVE_Y2);\
else\
 noframe_error();\
}
*CutyItem.fontList: BoldTextFont 
*CutyItem.foreground: MenuForeground

*UnzoomItem.class: pushButton
*UnzoomItem.parent: FramePane
*UnzoomItem.static: true
*UnzoomItem.name: UnzoomItem
*UnzoomItem.labelString: "Unzoom"
*UnzoomItem.mnemonic: "U"
*UnzoomItem.background: MenuBackground
*UnzoomItem.activateCallback: {\
if(specInputFrame)\
{\
 specXcen = specXcenw2;\
 specYcen = specYcenw2;\
 specDx = specDxw2;\
 specDy = specDyw2;\
 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
      specYcen-specDy,specYcen+specDy,PLOTMODE);\
 if(OverPlotMode)\
   plot_over();\
}\
else\
 noframe_error();\
}
*UnzoomItem.fontList: BoldTextFont 
*UnzoomItem.foreground: MenuForeground

*RedrawItem.class: pushButton
*RedrawItem.parent: FramePane
*RedrawItem.static: true
*RedrawItem.name: RedrawItem
*RedrawItem.labelString: "Redraw"
*RedrawItem.mnemonic: "R"
*RedrawItem.background: MenuBackground
*RedrawItem.activateCallback: {\
if(specInputFrame)\
 {\
  spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
        specYcen-specDy,specYcen+specDy,PLOTMODE);\
  if(OverPlotMode)\
     plot_over();\
  }\
else\
 noframe_error();\
}
*RedrawItem.fontList: BoldTextFont 
*RedrawItem.foreground: MenuForeground

*PrintPane.class: pushButton
*PrintPane.parent: FramePane
*PrintPane.static: true
*PrintPane.name: PrintPane
*PrintPane.labelString: "Print"
*PrintPane.mnemonic: "P"
*PrintPane.background: MenuBackground
*PrintPane.activateCallback: {\
if(specInputFrame)\
 {\
 if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("NormalToggle"))))\
  print_plot(NORMALPRINT,PrinterName);\
 else\
  print_plot(PORTRAITPRINT,PrinterName);\
 }\
else\
 noframe_error();\
}
*PrintPane.fontList: BoldTextFont 
*PrintPane.foreground: MenuForeground

*ContinPane.class: rowColumn
*ContinPane.parent: Topmenu
*ContinPane.static: true
*ContinPane.name: ContinPane
*ContinPane.rowColumnType: "menu_pulldown"
*ContinPane.background: MenuBackground
*ContinPane.highlightColor: "Black"
*ContinPane.foreground: MenuForeground

*BeginItem.class: pushButton
*BeginItem.parent: ContinPane
*BeginItem.static: true
*BeginItem.name: BeginItem
*BeginItem.labelString: "Begin"
*BeginItem.mnemonic: "B"
*BeginItem.background: MenuBackground
*BeginItem.activateCallback: {\
if(specInputFrame)\
{\
 gaussNumOfFitData=0;\
  fitPairNum = 0;\
 add_fit(6);\
 if(fitMode == POLYFITMODE)\
  plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);\
 else\
  plot_spline(gaussNumOfFitData,6);\
}\
else\
 noframe_error();\
}
*BeginItem.fontList: BoldTextFont 
*BeginItem.foreground: MenuForeground

*AddItem.class: pushButton
*AddItem.parent: ContinPane
*AddItem.static: true
*AddItem.name: AddItem
*AddItem.labelString: "Add points"
*AddItem.mnemonic: "A"
*AddItem.background: MenuBackground
*AddItem.activateCallback: {\
if(specInputFrame)\
{\
add_fit(6);\
if(fitMode == POLYFITMODE) \
 plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);\
else\
 plot_spline(gaussNumOfFitData,6);\
}\
else\
 noframe_error();\
}
*AddItem.fontList: BoldTextFont 
*AddItem.foreground: MenuForeground

*FitItem.class: pushButton
*FitItem.parent: ContinPane
*FitItem.static: true
*FitItem.name: FitItem
*FitItem.labelString: "Plot"
*FitItem.mnemonic: "P"
*FitItem.background: MenuBackground
*FitItem.activateCallback: {\
if(specInputFrame)\
{\
if(fitAddFit)\
 {\
  if(fitMode == POLYFITMODE)\
   plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);\
  else\
   plot_spline(gaussNumOfFitData,6);\
 }\
}\
else\
 noframe_error();\
}
*FitItem.fontList: BoldTextFont 
*FitItem.foreground: MenuForeground

*ReFitItem.class: pushButton
*ReFitItem.parent: ContinPane
*ReFitItem.static: true
*ReFitItem.name: ReFitItem
*ReFitItem.labelString: "Fit"
*ReFitItem.mnemonic: "F"
*ReFitItem.background: MenuBackground 
*ReFitItem.fontList: BoldTextFont  
*ReFitItem.foreground: MenuForeground 
*ReFitItem.activateCallback: {\
if(specInputFrame)\
{\
if(fitAddFit)\
 {\
 auto_fit(0);\
 plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);\
 }\
}\
else\
 noframe_error();\
}

*FilterPane.class: rowColumn
*FilterPane.parent: Topmenu
*FilterPane.static: true
*FilterPane.name: FilterPane
*FilterPane.rowColumnType: "menu_pulldown"
*FilterPane.background: MenuBackground
*FilterPane.highlightColor: "Black"
*FilterPane.foreground: MenuForeground

*SmoothItem.class: pushButton
*SmoothItem.parent: FilterPane
*SmoothItem.static: true
*SmoothItem.name: SmoothItem
*SmoothItem.labelString: "Smooth"
*SmoothItem.mnemonic: "S"
*SmoothItem.background: MenuBackground
*SmoothItem.activateCallback: {\
float y2[MAXVALUES];\
int i;\
if(specInputFrame)\
{\
mean_filter(specY,specNpix[0],filterWindWidth,y2);\
for(i=0;i<specNpix[0];i++)\
 specY[i]=y2[i]; \
median_filter(specY,specNpix[0],filterWindWidth,y2);\
load_points();\
draw_zoom();\
 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
specYcen-specDy,specYcen+specDy,PLOTMODE);\
if(OverPlotMode)\
 plot_over();\
XtSetSensitive(UxGetWidget(UxFindSwidget("UndoItem")),1);\
}\
else\
 noframe_error();\
}
*SmoothItem.fontList: BoldTextFont 
*SmoothItem.foreground: MenuForeground

*MedianItem.class: pushButton
*MedianItem.parent: FilterPane
*MedianItem.static: true
*MedianItem.name: MedianItem
*MedianItem.labelString: "Median"
*MedianItem.mnemonic: "M"
*MedianItem.background: MenuBackground
*MedianItem.activateCallback: {\
float y2[MAXVALUES];\
int i;\
if(specInputFrame)\
{\
median_filter(specY,specNpix[0],filterWindWidth,y2);\
for(i=0;i<specNpix[0];i++)\
 specY[i]=y2[i]; \
median_filter(specY,specNpix[0],filterWindWidth,y2);\
load_points();\
draw_zoom();\
 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
      specYcen-specDy,specYcen+specDy,PLOTMODE);\
if(OverPlotMode)\
 plot_over();\
XtSetSensitive(UxGetWidget(UxFindSwidget("UndoItem")),1);\
}\
else\
 noframe_error();\
}
*MedianItem.fontList: BoldTextFont 
*MedianItem.foreground: MenuForeground

*UndoItem.class: pushButton
*UndoItem.parent: FilterPane
*UndoItem.static: true
*UndoItem.name: UndoItem
*UndoItem.labelString: "Undo"
*UndoItem.mnemonic: "U"
*UndoItem.activateCallback: {\
 if(specInputFrame)\
  {\
   read_image(specLastName);\
   XtSetSensitive(UxGetWidget(UxFindSwidget("UndoItem")),0);\
  }\
 else\
  noframe_error();\
}
*UndoItem.background: MenuBackground 
*UndoItem.fontList: BoldTextFont 
*UndoItem.foreground: MenuForeground 

*LabelPane.class: rowColumn
*LabelPane.parent: Topmenu
*LabelPane.static: true
*LabelPane.name: LabelPane
*LabelPane.rowColumnType: "menu_pulldown"
*LabelPane.background: MenuBackground
*LabelPane.highlightColor: "Black"
*LabelPane.foreground: MenuForeground

*AxisItem.class: pushButton
*AxisItem.parent: LabelPane
*AxisItem.static: true
*AxisItem.name: AxisItem
*AxisItem.labelString: "Label axis"
*AxisItem.mnemonic: "a"
*AxisItem.background: MenuBackground
*AxisItem.activateCallback: {\
 extern swidget LabelOptions;\
char out[20];\
if(specInputFrame)\
 {\
 sprintf(out,"%s",plotTitle);\
 UxPutText(UxFindSwidget("Titletext"),out);\
 sprintf(out,"%s",plotLabelX);\
 UxPutText(UxFindSwidget("labelxtext"),out);\
 sprintf(out,"%s",plotLabelY);\
 UxPutText(UxFindSwidget("labelytext"),out);\
 UxPopupInterface(UxFindSwidget("LabelOptions"),no_grab); \
}\
else\
 noframe_error();\
}
*AxisItem.fontList: BoldTextFont 
*AxisItem.foreground: MenuForeground

*CursorItem.class: pushButton
*CursorItem.parent: LabelPane
*CursorItem.static: true
*CursorItem.name: CursorItem
*CursorItem.labelString: "Label / Cursor"
*CursorItem.mnemonic: "C"
*CursorItem.background: MenuBackground
*CursorItem.activateCallback: {\
if(specInputFrame)\
  UxPopupInterface(UxFindSwidget("LabelCursor"),no_grab);\
else\
 noframe_error();\
}
*CursorItem.fontList: BoldTextFont 
*CursorItem.foreground: MenuForeground

*LoadItem.class: pushButton
*LoadItem.parent: LabelPane
*LoadItem.static: true
*LoadItem.name: LoadItem
*LoadItem.labelString: "Load label file"
*LoadItem.mnemonic: "L"
*LoadItem.background: MenuBackground
*LoadItem.activateCallback: {\
extern swidget ListPopup;\
extern swidget scrolledList1;\
if(specInputFrame)\
 {\
  UxPopupInterface(ListPopup,no_grab);\
  SetFileList( UxGetWidget(scrolledList1), 1,"*.prg" );\
  caseList = CASEPRG;\
 }\
else\
 noframe_error();\
}
*LoadItem.fontList: BoldTextFont 
*LoadItem.foreground: MenuForeground

*ClearItem.class: pushButton
*ClearItem.parent: LabelPane
*ClearItem.static: true
*ClearItem.name: ClearItem
*ClearItem.labelString: "Clear labels"
*ClearItem.mnemonic: "C"
*ClearItem.background: MenuBackground
*ClearItem.activateCallback: {\
if(specInputFrame)\
 {\
  clearLabels();\
  system("rm -f TMPalice.prg");\
  spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
        specYcen-specDy,specYcen+specDy,PLOTMODE);\
  if(OverPlotMode)\
   plot_over();\
 }\
else\
 noframe_error();\
}
*ClearItem.fontList: BoldTextFont 
*ClearItem.foreground: MenuForeground

*UtilsPane.class: rowColumn
*UtilsPane.parent: Topmenu
*UtilsPane.static: true
*UtilsPane.name: UtilsPane
*UtilsPane.rowColumnType: "menu_pulldown"
*UtilsPane.background: MenuBackground
*UtilsPane.highlightColor: "Black"
*UtilsPane.foreground: MenuForeground

*GetcursorItem.class: pushButton
*GetcursorItem.parent: UtilsPane
*GetcursorItem.static: true
*GetcursorItem.name: GetcursorItem
*GetcursorItem.labelString: "Get cursor"
*GetcursorItem.mnemonic: "G"
*GetcursorItem.background: MenuBackground
*GetcursorItem.activateCallback: {\
float x1,y1;\
int key;\
char output[80];\
key = 1;\
if(specInputFrame)\
{\
 SCTPUT("\n");\
 SCTPUT("\t X-axis\t\t Y-axis");\
 SCTPUT("\t---------------------------");\
 while(key == 1)\
 {\
    vloc(&x1,&y1,&key);\
    if(key == 1)\
    {\
    sprintf(output,"\t%f\t%f",x1,y1);\
    SCTPUT(output);\
    }\
 }\
}\
else\
 noframe_error();\
}
*GetcursorItem.fontList: BoldTextFont 
*GetcursorItem.foreground: MenuForeground

*SavetmpItem.class: pushButton
*SavetmpItem.parent: UtilsPane
*SavetmpItem.static: true
*SavetmpItem.name: SavetmpItem
*SavetmpItem.labelString: "Save TMP files ..."
*SavetmpItem.mnemonic: "S"
*SavetmpItem.background: MenuBackground
*SavetmpItem.activateCallback: {\
 UxPopupInterface(UxFindSwidget("Savefiles"),no_grab);\
}
*SavetmpItem.fontList: BoldTextFont 
*SavetmpItem.foreground: MenuForeground

*PrinterItem.class: pushButton
*PrinterItem.parent: UtilsPane
*PrinterItem.static: true
*PrinterItem.name: PrinterItem
*PrinterItem.labelString: "Default Printer ..."
*PrinterItem.mnemonic: "D"
*PrinterItem.activateCallback: {\
 UxPopupInterface(UxFindSwidget("Printer"),no_grab);\
}
*PrinterItem.background: MenuBackground
*PrinterItem.fontList: BoldTextFont 
*PrinterItem.foreground: MenuForeground 

*OptionPane.class: rowColumn
*OptionPane.parent: Topmenu
*OptionPane.static: true
*OptionPane.name: OptionPane
*OptionPane.rowColumnType: "menu_pulldown"
*OptionPane.background: MenuBackground
*OptionPane.foreground: MenuForeground
*OptionPane.highlightColor: "Black"

*PlotItem.class: cascadeButton
*PlotItem.parent: OptionPane
*PlotItem.static: true
*PlotItem.name: PlotItem
*PlotItem.labelString: "Plot mode"
*PlotItem.subMenuId: "PlotRadio"
*PlotItem.background: MenuBackground
*PlotItem.fontList: BoldTextFont 
*PlotItem.foreground: MenuForeground

*ContinuumItem.class: cascadeButton
*ContinuumItem.parent: OptionPane
*ContinuumItem.static: true
*ContinuumItem.name: ContinuumItem
*ContinuumItem.labelString: "Continuum fit mode"
*ContinuumItem.subMenuId: "ContinRadio"
*ContinuumItem.fontList: BoldTextFont 
*ContinuumItem.foreground: MenuForeground 
*ContinuumItem.background: MenuBackground

*PrintItem.class: cascadeButton
*PrintItem.parent: OptionPane
*PrintItem.static: true
*PrintItem.name: PrintItem
*PrintItem.labelString: "Print Mode"
*PrintItem.subMenuId: "PrintRadio"
*PrintItem.background: MenuBackground
*PrintItem.fontList: BoldTextFont
*PrintItem.foreground: MenuForeground

*ContinRadio.class: rowColumn
*ContinRadio.parent: OptionPane
*ContinRadio.static: true
*ContinRadio.name: ContinRadio
*ContinRadio.rowColumnType: "menu_pulldown"
*ContinRadio.radioBehavior: "true"
*ContinRadio.foreground: MenuForeground 
*ContinRadio.background: MenuBackground 

*PolynomialItem.class: toggleButton
*PolynomialItem.parent: ContinRadio
*PolynomialItem.static: true
*PolynomialItem.name: PolynomialItem
*PolynomialItem.labelString: "Polynomial"
*PolynomialItem.fontList: BoldTextFont 
*PolynomialItem.foreground: MenuForeground 
*PolynomialItem.background: MenuBackground 
*PolynomialItem.set: "true"
*PolynomialItem.valueChangedCallback: fitMode = POLYFITMODE;\
XtSetSensitive(UxGetWidget(UxFindSwidget("arrowButton4")),1);\
XtSetSensitive(UxGetWidget(UxFindSwidget("arrowButton3")),1);\
XtSetSensitive(UxGetWidget(UxFindSwidget("degree_text")),1);\
XtSetSensitive(UxGetWidget(UxFindSwidget("ReFitItem")),1);\
 \

*PolynomialItem.height: 20

*SplineItem.class: toggleButton
*SplineItem.parent: ContinRadio
*SplineItem.static: true
*SplineItem.name: SplineItem
*SplineItem.labelString: "Spline"
*SplineItem.fontList: BoldTextFont 
*SplineItem.foreground: MenuForeground 
*SplineItem.background: MenuBackground 
*SplineItem.valueChangedCallback: fitMode = SPLINEFITMODE;\
XtSetSensitive(UxGetWidget(UxFindSwidget("arrowButton3")),0);\
XtSetSensitive(UxGetWidget(UxFindSwidget("arrowButton4")),0);\
XtSetSensitive(UxGetWidget(UxFindSwidget("degree_text")),0);\
 XtSetSensitive(UxGetWidget(UxFindSwidget("ReFitItem")),0);
*SplineItem.set: "false"
*SplineItem.height: 20

*PlotRadio.class: rowColumn
*PlotRadio.parent: OptionPane
*PlotRadio.static: true
*PlotRadio.name: PlotRadio
*PlotRadio.rowColumnType: "menu_pulldown"
*PlotRadio.radioBehavior: "true"
*PlotRadio.background: MenuBackground 
*PlotRadio.foreground: MenuForeground

*StraightToggle.class: toggleButton
*StraightToggle.parent: PlotRadio
*StraightToggle.static: true
*StraightToggle.name: StraightToggle
*StraightToggle.labelString: "Straight line"
*StraightToggle.background: MenuBackground
*StraightToggle.set: "true"
*StraightToggle.fontList: BoldTextFont 
*StraightToggle.foreground: MenuForeground 
*StraightToggle.valueChangedCallback: plotMode = LINEMODE;
*StraightToggle.height: 20

*HistoToggle.class: toggleButton
*HistoToggle.parent: PlotRadio
*HistoToggle.static: true
*HistoToggle.name: HistoToggle
*HistoToggle.labelString: "Histogram - like"
*HistoToggle.background: MenuBackground
*HistoToggle.fontList: BoldTextFont 
*HistoToggle.foreground: MenuForeground 
*HistoToggle.valueChangedCallback: plotMode = HISTMODE;
*HistoToggle.height: 20

*PrintRadio.class: rowColumn
*PrintRadio.parent: OptionPane
*PrintRadio.static: true
*PrintRadio.name: PrintRadio
*PrintRadio.rowColumnType: "menu_pulldown"
*PrintRadio.radioBehavior: "true"
*PrintRadio.background: MenuBackground

*NormalToggle.class: toggleButton
*NormalToggle.parent: PrintRadio
*NormalToggle.static: true
*NormalToggle.name: NormalToggle
*NormalToggle.labelString: "Landscape"
*NormalToggle.mnemonic: "L"
*NormalToggle.background: MenuBackground
*NormalToggle.fontList: BoldTextFont
*NormalToggle.foreground: MenuForeground
*NormalToggle.set: "true"
*NormalToggle.valueChangedCallback: {\
if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("NormalToggle"))))\
{\
 AppendDialogText("del/gra 0");\
 AppendDialogText("cre/gra 0 1000,400,40,500");\
 AppendDialogText("SET/GCURSOR ? C_HAIR");\
}\
}

*PortraitToggle.class: toggleButton
*PortraitToggle.parent: PrintRadio
*PortraitToggle.static: true
*PortraitToggle.name: PortraitToggle
*PortraitToggle.labelString: "Portrait"
*PortraitToggle.mnemonic: "P"
*PortraitToggle.background: MenuBackground
*PortraitToggle.fontList: BoldTextFont
*PortraitToggle.foreground: MenuForeground
*PortraitToggle.valueChangedCallback: {\
if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("PortraitToggle"))))\
{\
 AppendDialogText("del/gra 0");\
 AppendDialogText("cre/gra 0 500,800,10,80");\
 AppendDialogText("SET/GCURSOR ? C_HAIR");\
}\
}

*HelpPane.class: rowColumn
*HelpPane.parent: Topmenu
*HelpPane.static: true
*HelpPane.name: HelpPane
*HelpPane.rowColumnType: "menu_pulldown"
*HelpPane.background: MenuBackground 
*HelpPane.foreground: MenuForeground 

*Context.class: pushButton
*Context.parent: HelpPane
*Context.static: true
*Context.name: Context
*Context.labelString: "On Context"
*Context.mnemonic: "C"
*Context.activateCallback: { UxPopupInterface(UxFindSwidget("HelpTopLevel"),no_grab);\
 UxPutText(UxFindSwidget("helpText"),find_extended_help("CONTEXT"));\
 } 
*Context.background: MenuBackground 
*Context.fontList: BoldTextFont 
*Context.foreground: MenuForeground 

*OnHelp.class: pushButton
*OnHelp.parent: HelpPane
*OnHelp.static: true
*OnHelp.name: OnHelp
*OnHelp.labelString: "On Help"
*OnHelp.mnemonic: "H"
*OnHelp.background: MenuBackground 
*OnHelp.fontList: BoldTextFont 
*OnHelp.foreground: MenuForeground 
*OnHelp.activateCallback: {\
UxPopupInterface(UxFindSwidget("HelpTopLevel"),no_grab);\
UxPutText(UxFindSwidget("helpText"),find_extended_help("HELP"));\
}

*QuitPane.class: rowColumn
*QuitPane.parent: Topmenu
*QuitPane.static: true
*QuitPane.name: QuitPane
*QuitPane.rowColumnType: "menu_pulldown"
*QuitPane.background: MenuBackground
*QuitPane.highlightColor: "Black"
*QuitPane.foreground: MenuForeground

*QuitItem.class: pushButton
*QuitItem.parent: QuitPane
*QuitItem.static: true
*QuitItem.name: QuitItem
*QuitItem.labelString: "Bye"
*QuitItem.mnemonic: "B"
*QuitItem.background: MenuBackground
*QuitItem.activateCallback: {\
system("rm -f alicel.plt alice.plt TMPalice.* TMPcont.bdf pscrplot.0");\
exit(0); \
}
*QuitItem.fontList: BoldTextFont 
*QuitItem.foreground: MenuForeground

*FileMenu.class: cascadeButton
*FileMenu.parent: Topmenu
*FileMenu.static: true
*FileMenu.name: FileMenu
*FileMenu.labelString: "File"
*FileMenu.mnemonic: "F"
*FileMenu.subMenuId: "FilePane"
*FileMenu.background: MenuBackground
*FileMenu.fontList: BoldTextFont 
*FileMenu.foreground: MenuForeground 
*FileMenu.highlightColor: "Black"

*FrameMenu.class: cascadeButton
*FrameMenu.parent: Topmenu
*FrameMenu.static: true
*FrameMenu.name: FrameMenu
*FrameMenu.labelString: "Frame"
*FrameMenu.mnemonic: "m"
*FrameMenu.subMenuId: "FramePane"
*FrameMenu.background: MenuBackground
*FrameMenu.fontList: BoldTextFont 
*FrameMenu.foreground: MenuForeground 
*FrameMenu.highlightColor: "Black"

*ContinunmMenu.class: cascadeButton
*ContinunmMenu.parent: Topmenu
*ContinunmMenu.static: true
*ContinunmMenu.name: ContinunmMenu
*ContinunmMenu.labelString: "Continuum"
*ContinunmMenu.mnemonic: "C"
*ContinunmMenu.subMenuId: "ContinPane"
*ContinunmMenu.background: MenuBackground
*ContinunmMenu.fontList: BoldTextFont 
*ContinunmMenu.foreground: MenuForeground 
*ContinunmMenu.highlightColor: "Black"

*FilterMenu.class: cascadeButton
*FilterMenu.parent: Topmenu
*FilterMenu.static: true
*FilterMenu.name: FilterMenu
*FilterMenu.labelString: "Filter"
*FilterMenu.mnemonic: "t"
*FilterMenu.subMenuId: "FilterPane"
*FilterMenu.background: MenuBackground
*FilterMenu.fontList: BoldTextFont 
*FilterMenu.foreground: MenuForeground 
*FilterMenu.highlightColor: "Black"

*LabelMenu.class: cascadeButton
*LabelMenu.parent: Topmenu
*LabelMenu.static: true
*LabelMenu.name: LabelMenu
*LabelMenu.labelString: "Label"
*LabelMenu.mnemonic: "L"
*LabelMenu.subMenuId: "LabelPane"
*LabelMenu.background: MenuBackground
*LabelMenu.fontList: BoldTextFont 
*LabelMenu.foreground: MenuForeground 
*LabelMenu.highlightColor: "Black"

*UtilsMenu.class: cascadeButton
*UtilsMenu.parent: Topmenu
*UtilsMenu.static: true
*UtilsMenu.name: UtilsMenu
*UtilsMenu.labelString: "Utils"
*UtilsMenu.mnemonic: "U"
*UtilsMenu.subMenuId: "UtilsPane"
*UtilsMenu.background: MenuBackground
*UtilsMenu.fontList: BoldTextFont 
*UtilsMenu.foreground: MenuForeground 
*UtilsMenu.highlightColor: "Black"

*OptionMenu.class: cascadeButton
*OptionMenu.parent: Topmenu
*OptionMenu.static: true
*OptionMenu.name: OptionMenu
*OptionMenu.labelString: "Options"
*OptionMenu.mnemonic: "O"
*OptionMenu.subMenuId: "OptionPane"
*OptionMenu.background: MenuBackground
*OptionMenu.fontList: BoldTextFont 
*OptionMenu.foreground: MenuForeground 
*OptionMenu.highlightColor: "Black"

*HelpMenu.class: cascadeButton
*HelpMenu.parent: Topmenu
*HelpMenu.static: true
*HelpMenu.name: HelpMenu
*HelpMenu.labelString: "Help"
*HelpMenu.mnemonic: "H"
*HelpMenu.subMenuId: "HelpPane"
*HelpMenu.background: MenuBackground 
*HelpMenu.fontList: BoldTextFont 
*HelpMenu.foreground: MenuForeground 

*QuitMenu.class: cascadeButton
*QuitMenu.parent: Topmenu
*QuitMenu.static: true
*QuitMenu.name: QuitMenu
*QuitMenu.labelString: "Quit"
*QuitMenu.mnemonic: "Q"
*QuitMenu.subMenuId: "QuitPane"
*QuitMenu.background: MenuBackground
*QuitMenu.fontList: BoldTextFont 
*QuitMenu.foreground: MenuForeground 
*QuitMenu.highlightColor: "Black"

*AliceForm1.class: form
*AliceForm1.parent: AliceWindow
*AliceForm1.static: true
*AliceForm1.name: AliceForm1
*AliceForm1.background: WindowBackground 

*Line.class: label
*Line.parent: AliceForm1
*Line.static: false
*Line.name: Line
*Line.x: 13
*Line.y: 35
*Line.width: 40
*Line.height: 30
*Line.background: LabelBackground
*Line.fontList: TextFont 
*Line.foreground: TextForeground 
*Line.labelString: "Line :"

*CurrLine.class: text
*CurrLine.parent: AliceForm1
*CurrLine.static: true
*CurrLine.name: CurrLine
*CurrLine.x: 55
*CurrLine.y: 35
*CurrLine.width: 52
*CurrLine.height: 36
*CurrLine.background: TextBackground
*CurrLine.borderColor: "Gray100"
*CurrLine.losingFocusCallback: {\
int aux_specLineNum;\
char out[6];\
if(specInputFrame )\
{\
if( specModLineNum)\
{ specModLineNum = FALSE;\
  sscanf(UxGetText(CurrLine),"%d",&aux_specLineNum);\
   if(aux_specLineNum != specLineNum)\
   {\
   if(aux_specLineNum > 0 & aux_specLineNum + specLineStep -1 <= specDim)\
    {\
     specLineNum = aux_specLineNum;\
     read_image(specImageName);\
    }\
   else   \
   {sprintf(out,"%d",specLineNum);\
    XmTextSetString(UxGetWidget(UxFindSwidget("CurrLine")),out);\
    out_error("Invalid Line Number");\
   }\
  }\
 } \
}\
else if(specModLineNum)\
 noframe_error();\
 UxPutText(UxFindSwidget("help_text_top")," ");\
}
*CurrLine.modifyVerifyCallback: {\
if(specInputFrame)\
 specModLineNum = TRUE;\
 \
}
*CurrLine.foreground: TextForeground
*CurrLine.maxLength: 5
*CurrLine.highlightOnEnter: "true"
*CurrLine.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_top"),IMAGELINE);\
}
*CurrLine.fontList: TextFont 
*CurrLine.translations: TextTab

*Linestep.class: text
*Linestep.parent: AliceForm1
*Linestep.static: true
*Linestep.name: Linestep
*Linestep.x: 109
*Linestep.y: 35
*Linestep.width: 34
*Linestep.height: 36
*Linestep.background: TextBackground
*Linestep.borderColor: "Gray100"
*Linestep.losingFocusCallback: {\
extern int specModLineStep;\
int aux;\
char out[20];\
if(specInputFrame & specModLineStep)\
{\
if( specModLineStep)\
{ specModLineStep = FALSE;\
  sscanf(UxGetText(Linestep),"%d",&aux);\
  if( aux <= specDim & aux >0)\
  {\
   specLineStep = aux;\
   read_image(specImageName);\
  }\
  else\
  {\
   sprintf(out,"%d",specLineStep);\
   XmTextSetString(UxGetWidget(UxFindSwidget("Linestep")),out);\
   out_error("Invalid Line Step");\
  }\
 }\
}\
else if(specModLineStep)\
 noframe_error();\
UxPutText(UxFindSwidget("help_text_top"),"");\
}
*Linestep.modifyVerifyCallback: {\
extern int specModLineStep;\
if(specInputFrame)\
 specModLineStep = TRUE;\
}
*Linestep.foreground: TextForeground
*Linestep.maxLength: 3
*Linestep.highlightOnEnter: "true"
*Linestep.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_top"),IMAGEWIDE);\
}
*Linestep.fontList: TextFont 
*Linestep.translations: TextTab

*arrowButton1.class: arrowButton
*arrowButton1.parent: AliceForm1
*arrowButton1.static: true
*arrowButton1.name: arrowButton1
*arrowButton1.x: 141
*arrowButton1.y: 33
*arrowButton1.width: 30
*arrowButton1.height: 20
*arrowButton1.background: TextBackground 
*arrowButton1.foreground: MenuBackground 
*arrowButton1.activateCallback: {\
char line[20];\
if(specInputFrame)\
{\
if(specLineNum + specLineStep  <=  specDim)\
 specLineNum+=specLineStep;\
else \
 specLineNum = 1;\
sprintf(line,"%d",specLineNum);\
UxPutText(CurrLine,line);\
read_image(specImageName);\
specModLineNum = FALSE;\
specModLineStep = FALSE;\
}\
else\
 noframe_error();\
}
*arrowButton1.highlightOnEnter: "true"
*arrowButton1.translations: HELPTopTab
*arrowButton1.highlightColor: "Black"

*arrowButton2.class: arrowButton
*arrowButton2.parent: AliceForm1
*arrowButton2.static: true
*arrowButton2.name: arrowButton2
*arrowButton2.x: 141
*arrowButton2.y: 51
*arrowButton2.width: 30
*arrowButton2.height: 20
*arrowButton2.background: TextBackground 
*arrowButton2.foreground: MenuBackground 
*arrowButton2.arrowDirection: "arrow_down"
*arrowButton2.activateCallback: {\
char line[20];\
if(specInputFrame)\
{\
if(specLineNum - specLineStep > 0)\
 specLineNum-=specLineStep;\
else\
 specLineNum = specDim - specLineStep +1;\
sprintf(line,"%d",specLineNum);\
UxPutText(CurrLine,line);\
read_image(specImageName);\
specModLineNum = FALSE;\
specModLineStep = FALSE;\
}\
else\
 noframe_error();\
}
*arrowButton2.highlightOnEnter: "true"
*arrowButton2.translations: HELPTopTab
*arrowButton2.highlightColor: "Black"

*Degree.class: label
*Degree.parent: AliceForm1
*Degree.static: false
*Degree.name: Degree
*Degree.x: 343
*Degree.y: 35
*Degree.width: 87
*Degree.height: 30
*Degree.background: LabelBackground
*Degree.fontList: TextFont 
*Degree.borderColor: "Gray100"
*Degree.labelString: "Fit Degree :"
*Degree.foreground: TextForeground

*degree_text.class: text
*degree_text.parent: AliceForm1
*degree_text.static: true
*degree_text.name: degree_text
*degree_text.x: 431
*degree_text.y: 33
*degree_text.width: 36
*degree_text.height: 36
*degree_text.background: TextBackground
*degree_text.borderColor: "Gray100"
*degree_text.losingFocusCallback: {\
int aux_fitDegree;\
char out[3];\
if( fitModFitDeg)\
{ fitModFitDeg = FALSE;\
  sscanf(UxGetText(degree_text),"%d",&aux_fitDegree);\
  if(aux_fitDegree >0 && aux_fitDegree < 16)\
   fitDegree = aux_fitDegree;\
  else\
  {\
 sprintf(out,"%d",fitDegree);\
 XmTextSetString(UxGetWidget(UxFindSwidget("degree_text")),out);\
  }\
}\
UxPutText(UxFindSwidget("help_text_top"),"");\
}
*degree_text.modifyVerifyCallback: {\
fitModFitDeg = TRUE;\
}
*degree_text.foreground: TextForeground
*degree_text.maxLength: 3
*degree_text.highlightOnEnter: "true"
*degree_text.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_top"),FITDEGREE);\
}
*degree_text.fontList: TextFont 
*degree_text.translations: TextTab

*arrowButton3.class: arrowButton
*arrowButton3.parent: AliceForm1
*arrowButton3.static: true
*arrowButton3.name: arrowButton3
*arrowButton3.x: 464
*arrowButton3.y: 32
*arrowButton3.width: 30
*arrowButton3.height: 20
*arrowButton3.background: TextBackground 
*arrowButton3.foreground: MenuBackground 
*arrowButton3.activateCallback: {\
char deg[20];\
if(fitDegree < 15)\
 fitDegree++;\
else \
 fitDegree = 1;\
sprintf(deg,"%d",fitDegree);\
UxPutText(degree_text,deg);\
specModLineNum = FALSE;\
specModLineStep = FALSE;\
}
*arrowButton3.highlightOnEnter: "true"
*arrowButton3.translations: HELPTopTab
*arrowButton3.highlightColor: "Black"

*arrowButton4.class: arrowButton
*arrowButton4.parent: AliceForm1
*arrowButton4.static: true
*arrowButton4.name: arrowButton4
*arrowButton4.x: 464
*arrowButton4.y: 50
*arrowButton4.width: 30
*arrowButton4.height: 20
*arrowButton4.background: TextBackground 
*arrowButton4.arrowDirection: "arrow_down"
*arrowButton4.foreground: MenuBackground 
*arrowButton4.activateCallback: {\
char deg[20];\
if(fitDegree > 1)\
 fitDegree--;\
else \
 fitDegree = 15;\
sprintf(deg,"%d",fitDegree);\
UxPutText(degree_text,deg);\
}
*arrowButton4.highlightOnEnter: "true"
*arrowButton4.translations: HELPTopTab
*arrowButton4.highlightColor: "Black"

*AliceForm2.class: form
*AliceForm2.parent: AliceForm1
*AliceForm2.static: true
*AliceForm2.name: AliceForm2
*AliceForm2.resizePolicy: "resize_none"
*AliceForm2.x: -1
*AliceForm2.y: 389
*AliceForm2.width: 565
*AliceForm2.height: 44
*AliceForm2.background: ButtonBackground  

*GaussButton.class: pushButton
*GaussButton.parent: AliceForm2
*GaussButton.static: true
*GaussButton.name: GaussButton
*GaussButton.x: 32
*GaussButton.y: 6
*GaussButton.width: 90
*GaussButton.height: 30
*GaussButton.fontList: BoldTextFont 
*GaussButton.foreground: ButtonForeground
*GaussButton.labelString: "Gauss ..."
*GaussButton.activateCallback: {\
extern swidget Gauss_Shell;\
if(specInputFrame)\
 UxPopupInterface(UxFindSwidget("GaussShell"),no_grab);\
else\
 noframe_error();\
}
*GaussButton.background: ButtonBackground
*GaussButton.helpCallback: {\
UxPutText(UxFindSwidget("help_text_top"),"Help Button");\
}
*GaussButton.highlightOnEnter: "true"
*GaussButton.translations: Help 
*GaussButton.highlightColor: "Black"

*IntegrateButton.class: pushButton
*IntegrateButton.parent: AliceForm2
*IntegrateButton.static: true
*IntegrateButton.name: IntegrateButton
*IntegrateButton.x: 375
*IntegrateButton.y: 6
*IntegrateButton.width: 90
*IntegrateButton.height: 30
*IntegrateButton.fontList: BoldTextFont 
*IntegrateButton.foreground: ButtonForeground
*IntegrateButton.labelString: "Integrate"
*IntegrateButton.background: ButtonBackground
*IntegrateButton.activateCallback: {\
if(specInputFrame)\
 integrate();\
else\
 noframe_error();\
}
*IntegrateButton.highlightOnEnter: "true"
*IntegrateButton.translations: Help 

*RebinButton.class: pushButton
*RebinButton.parent: AliceForm2
*RebinButton.static: true
*RebinButton.name: RebinButton
*RebinButton.x: 152
*RebinButton.y: 6
*RebinButton.width: 90
*RebinButton.height: 30
*RebinButton.fontList: BoldTextFont 
*RebinButton.foreground: ButtonForeground
*RebinButton.labelString: "Rebin ..."
*RebinButton.activateCallback: {\
if(specInputFrame)\
UxPopupInterface(UxFindSwidget("RebinShell"),no_grab);\
else\
 noframe_error();\
}
*RebinButton.background: ButtonBackground
*RebinButton.helpCallback: {\
UxPutText(UxFindSwidget("help_text_top"),"Help Button");\
}
*RebinButton.highlightOnEnter: "true"
*RebinButton.translations: Help 
*RebinButton.highlightColor: "Black"

*RebinButton1.class: pushButton
*RebinButton1.parent: AliceForm2
*RebinButton1.static: true
*RebinButton1.name: RebinButton1
*RebinButton1.x: 263
*RebinButton1.y: 6
*RebinButton1.width: 90
*RebinButton1.height: 30
*RebinButton1.fontList: BoldTextFont 
*RebinButton1.foreground: ButtonForeground
*RebinButton1.labelString: "Overplot ..."
*RebinButton1.activateCallback: {\
if(specInputFrame)\
UxPopupInterface(UxFindSwidget("OverPlotShell"),no_grab);\
else\
 noframe_error();\
}
*RebinButton1.background: ButtonBackground
*RebinButton1.helpCallback: {\
UxPutText(UxFindSwidget("help_text_top"),"Help Button");\
}
*RebinButton1.highlightOnEnter: "true"
*RebinButton1.translations: Help 
*RebinButton1.highlightColor: "Black"

*separator1.class: separator
*separator1.parent: AliceForm1
*separator1.static: true
*separator1.name: separator1
*separator1.x: 1
*separator1.y: 84
*separator1.width: 554
*separator1.height: 5
*separator1.background: LabelBackground
*separator1.foreground: TextForeground 

*width_text.class: text
*width_text.parent: AliceForm1
*width_text.static: true
*width_text.name: width_text
*width_text.x: 253
*width_text.y: 34
*width_text.width: 52
*width_text.height: 36
*width_text.background: TextBackground
*width_text.borderColor: "Gray100"
*width_text.losingFocusCallback: {\
int aux_width;\
char out[8];\
if( filterModWindWidth)\
{ filterModWindWidth = FALSE;\
  sscanf(UxGetText(width_text),"%d",&aux_width);\
  if(aux_width >1 && aux_width < specNpix[0]/2)\
   filterWindWidth = aux_width;\
  else\
  {\
 sprintf(out,"%d",filterWindWidth);\
 XmTextSetString(UxGetWidget(UxFindSwidget("width_text")),out);\
 out_error("Invalid Window Width");\
  }\
}\
UxPutText(UxFindSwidget("help_text_top"),"");\
}
*width_text.modifyVerifyCallback: {\
filterModWindWidth = TRUE;\
}
*width_text.foreground: TextForeground
*width_text.maxLength: 4
*width_text.highlightOnEnter: "true"
*width_text.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_top"),FILTERWIDE);\
}
*width_text.fontList: TextFont 
*width_text.translations: TextTab

*arrowButton11.class: arrowButton
*arrowButton11.parent: AliceForm1
*arrowButton11.static: true
*arrowButton11.name: arrowButton11
*arrowButton11.x: 303
*arrowButton11.y: 32
*arrowButton11.width: 30
*arrowButton11.height: 20
*arrowButton11.background: TextBackground 
*arrowButton11.foreground: MenuBackground 
*arrowButton11.activateCallback: {\
char out[20];\
if(filterWindWidth < specNpix[0]/2)\
 filterWindWidth++;\
else\
 out_error("Invalid Window Width");\
sprintf(out,"%d",filterWindWidth);\
UxPutText(width_text,out);\
specModLineNum = FALSE;\
specModLineStep = FALSE;\
}
*arrowButton11.highlightOnEnter: "true"
*arrowButton11.translations: HELPTopTab
*arrowButton11.highlightColor: "Black"

*arrowButton12.class: arrowButton
*arrowButton12.parent: AliceForm1
*arrowButton12.static: true
*arrowButton12.name: arrowButton12
*arrowButton12.x: 303
*arrowButton12.y: 50
*arrowButton12.width: 30
*arrowButton12.height: 20
*arrowButton12.background: TextBackground 
*arrowButton12.arrowDirection: "arrow_down"
*arrowButton12.foreground: MenuBackground 
*arrowButton12.activateCallback: {\
char out[20];\
if(filterWindWidth > 2)\
 filterWindWidth--;\
else\
 out_error("Invalid Window Width");\
sprintf(out,"%d",filterWindWidth);\
UxPutText(width_text,out);\
}
*arrowButton12.highlightOnEnter: "true"
*arrowButton12.translations: HELPTopTab
*arrowButton12.highlightColor: "Black"

*Line1.class: label
*Line1.parent: AliceForm1
*Line1.static: false
*Line1.name: Line1
*Line1.x: 177
*Line1.y: 34
*Line1.width: 72
*Line1.height: 30
*Line1.background: LabelBackground
*Line1.fontList: TextFont 
*Line1.labelString: "Window :"
*Line1.foreground: TextForeground 

*Line2.class: label
*Line2.parent: AliceForm1
*Line2.static: false
*Line2.name: Line2
*Line2.x: 71
*Line2.y: 5
*Line2.width: 64
*Line2.height: 18
*Line2.background: LabelBackground
*Line2.fontList: TextFont 
*Line2.labelString: "Image"
*Line2.foreground: TextForeground 

*Line3.class: label
*Line3.parent: AliceForm1
*Line3.static: false
*Line3.name: Line3
*Line3.x: 221
*Line3.y: 4
*Line3.width: 64
*Line3.height: 18
*Line3.background: LabelBackground
*Line3.fontList: TextFont 
*Line3.labelString: "Filter"
*Line3.foreground: TextForeground 

*Line4.class: label
*Line4.parent: AliceForm1
*Line4.static: false
*Line4.name: Line4
*Line4.x: 398
*Line4.y: 4
*Line4.width: 86
*Line4.height: 18
*Line4.background: LabelBackground
*Line4.fontList: TextFont 
*Line4.labelString: "Continuum"
*Line4.foreground: TextForeground 

*help_text_top.class: text
*help_text_top.parent: AliceForm1
*help_text_top.static: true
*help_text_top.name: help_text_top
*help_text_top.x: 4
*help_text_top.y: 332
*help_text_top.width: 555
*help_text_top.height: 48
*help_text_top.background: SHelpBackground 
*help_text_top.editable: "false"
*help_text_top.fontList: TextFont 
*help_text_top.cursorPositionVisible: "false"
*help_text_top.foreground: TextForeground 
*help_text_top.marginHeight: 3

*separator3.class: separator
*separator3.parent: AliceForm1
*separator3.static: true
*separator3.name: separator3
*separator3.x: -2
*separator3.y: 382
*separator3.width: 565
*separator3.height: 8
*separator3.background: ButtonBackground 
*separator3.foreground: TextForeground 

*separator8.class: separator
*separator8.parent: AliceForm1
*separator8.static: true
*separator8.name: separator8
*separator8.x: 13
*separator8.y: 25
*separator8.width: 158
*separator8.height: 10
*separator8.background: LabelBackground
*separator8.foreground: TextForeground 

*separator9.class: separator
*separator9.parent: AliceForm1
*separator9.static: true
*separator9.name: separator9
*separator9.x: 185
*separator9.y: 24
*separator9.width: 146
*separator9.height: 10
*separator9.background: LabelBackground
*separator9.foreground: TextForeground 

*separator10.class: separator
*separator10.parent: AliceForm1
*separator10.static: true
*separator10.name: separator10
*separator10.x: 348
*separator10.y: 24
*separator10.width: 202
*separator10.height: 10
*separator10.background: LabelBackground
*separator10.foreground: TextForeground 

*GaussDrawingArea.class: drawingArea
*GaussDrawingArea.parent: AliceForm1
*GaussDrawingArea.static: true
*GaussDrawingArea.name: GaussDrawingArea
*GaussDrawingArea.resizePolicy: "resize_none"
*GaussDrawingArea.x: 16
*GaussDrawingArea.y: 112
*GaussDrawingArea.width: 400
*GaussDrawingArea.height: 200
*GaussDrawingArea.background: TextBackground 
*GaussDrawingArea.marginHeight: 0
*GaussDrawingArea.marginWidth: 0
*GaussDrawingArea.foreground: TextForeground 
*GaussDrawingArea.highlightColor: "Black"
*GaussDrawingArea.createCallback: {\
#define Cross_cursor 30\
Window window;\
Display *display;\
XSetWindowAttributes attributes;\
\
display = XtDisplay(UxGetWidget(UxFindSwidget("GaussDrawingArea")));\
window = XtWindow(UxGetWidget(UxFindSwidget("GaussDrawingArea")));\
\
attributes.cursor = XCreateFontCursor(display,Cross_cursor);\
 XChangeWindowAttributes(display,window,CWCursor,&attributes);\
}
*GaussDrawingArea.exposeCallback: {\
Window window;\
Display *display;\
display = XtDisplay(UxGetWidget(UxFindSwidget("GaussDrawingArea")));\
window = XtWindow(UxGetWidget(UxFindSwidget("GaussDrawingArea")));\
XClearWindow(display,window);\
draw_zoom();\
}

*arrowButton8.class: arrowButton
*arrowButton8.parent: AliceForm1
*arrowButton8.static: false
*arrowButton8.name: arrowButton8
*arrowButton8.x: 442
*arrowButton8.y: 239
*arrowButton8.width: 30
*arrowButton8.height: 30
*arrowButton8.arrowDirection: "arrow_left"
*arrowButton8.background: ButtonBackground
*arrowButton8.activateCallback: {\
 if(specInputFrame)\
 zoom(STEPDN_X);\
else\
 noframe_error();\
}
*arrowButton8.foreground: TextForeground 
*arrowButton8.highlightOnEnter: "true"
*arrowButton8.highlightColor: "Black"
*arrowButton8.translations: HELPTopTab

*arrowButton6.class: arrowButton
*arrowButton6.parent: AliceForm1
*arrowButton6.static: false
*arrowButton6.name: arrowButton6
*arrowButton6.x: 476
*arrowButton6.y: 251
*arrowButton6.width: 30
*arrowButton6.height: 30
*arrowButton6.arrowDirection: "arrow_down"
*arrowButton6.background: ButtonBackground
*arrowButton6.activateCallback: {\
if(specInputFrame) \
 zoom(STEPDN_Y);\
else\
 noframe_error();\
}
*arrowButton6.foreground: TextForeground 
*arrowButton6.highlightOnEnter: "true"
*arrowButton6.highlightColor: "Black"
*arrowButton6.translations: HELPTopTab

*arrowButton5.class: arrowButton
*arrowButton5.parent: AliceForm1
*arrowButton5.static: false
*arrowButton5.name: arrowButton5
*arrowButton5.x: 508
*arrowButton5.y: 239
*arrowButton5.width: 30
*arrowButton5.height: 30
*arrowButton5.arrowDirection: "arrow_right"
*arrowButton5.background: ButtonBackground
*arrowButton5.activateCallback: {\
if(specInputFrame)\
 zoom(STEPUP_X);\
else\
 noframe_error();\
\
}
*arrowButton5.foreground: TextForeground 
*arrowButton5.highlightOnEnter: "true"
*arrowButton5.highlightColor: "Black"
*arrowButton5.translations: HELPTopTab

*arrowButton7.class: arrowButton
*arrowButton7.parent: AliceForm1
*arrowButton7.static: false
*arrowButton7.name: arrowButton7
*arrowButton7.x: 475
*arrowButton7.y: 223
*arrowButton7.width: 30
*arrowButton7.height: 30
*arrowButton7.background: ButtonBackground
*arrowButton7.activateCallback: {\
if(specInputFrame)\
 zoom(STEPUP_Y);\
else\
noframe_error();\
}
*arrowButton7.foreground: TextForeground 
*arrowButton7.highlightOnEnter: "true"
*arrowButton7.highlightColor: "Black"
*arrowButton7.translations: HELPTopTab

*Step.class: label
*Step.parent: AliceForm1
*Step.static: false
*Step.name: Step
*Step.x: 437
*Step.y: 293
*Step.width: 50
*Step.height: 30
*Step.fontList: TextFont 
*Step.foreground: TextForeground 
*Step.background: LabelBackground
*Step.labelString: "Step :"

*step_text.class: text
*step_text.parent: AliceForm1
*step_text.static: false
*step_text.name: step_text
*step_text.x: 487
*step_text.y: 289
*step_text.width: 51
*step_text.height: 34
*step_text.foreground: TextForeground 
*step_text.background: TextBackground
*step_text.highlightOnEnter: "true"
*step_text.text: "0.2"
*step_text.sensitive: "true"
*step_text.losingFocusCallback: {\
if( filterModStepWin)\
{ filterModStepWin = FALSE;\
  sscanf(UxGetText(step_text),"%f",&specStepWin);\
}\
UxPutText(UxFindSwidget("help_text_top"),"");\
}
*step_text.modifyVerifyCallback: {\
filterModStepWin = TRUE;\
}
*step_text.fontList: TextFont 
*step_text.translations: TextTab
*step_text.highlightColor: "Black"
*step_text.topShadowColor: WindowBackground
*step_text.focusCallback: {\
UxPutText(UxFindSwidget("help_text_top"),ZOOMSTEP);\
}

*Cut_x.class: pushButton
*Cut_x.parent: AliceForm1
*Cut_x.static: false
*Cut_x.name: Cut_x
*Cut_x.x: 443
*Cut_x.y: 94
*Cut_x.width: 90
*Cut_x.height: 30
*Cut_x.background: ButtonBackground
*Cut_x.labelString: "Cut X"
*Cut_x.fontList: BoldTextFont 
*Cut_x.foreground: ButtonForeground
*Cut_x.activateCallback: {\
if(specInputFrame)\
 zoom(MOVE_X1);  \
else\
 noframe_error();\
\
}
*Cut_x.highlightOnEnter: "true"
*Cut_x.highlightColor: "Black"
*Cut_x.translations: Help 

*Cut_y.class: pushButton
*Cut_y.parent: AliceForm1
*Cut_y.static: false
*Cut_y.name: Cut_y
*Cut_y.x: 443
*Cut_y.y: 124
*Cut_y.width: 90
*Cut_y.height: 30
*Cut_y.background: ButtonBackground
*Cut_y.foreground: ButtonForeground
*Cut_y.fontList: BoldTextFont 
*Cut_y.labelString: "Cut Y"
*Cut_y.activateCallback: {\
if(specInputFrame)\
 zoom(MOVE_Y1);\
else\
 noframe_error();\
}
*Cut_y.highlightOnEnter: "true"
*Cut_y.highlightColor: "Black"
*Cut_y.translations: Help 

*Move.class: pushButton
*Move.parent: AliceForm1
*Move.static: false
*Move.name: Move
*Move.x: 443
*Move.y: 155
*Move.width: 90
*Move.height: 30
*Move.background: ButtonBackground
*Move.foreground: ButtonForeground
*Move.fontList: BoldTextFont 
*Move.activateCallback: {\
if(specInputFrame)\
 zoom(MOVE_WIN);\
else\
 noframe_error();\
}
*Move.highlightOnEnter: "true"
*Move.highlightColor: "Black"
*Move.translations: Help 

*separator11.class: separator
*separator11.parent: AliceForm1
*separator11.static: true
*separator11.name: separator11
*separator11.x: 8
*separator11.y: 316
*separator11.width: 416
*separator11.height: 9
*separator11.background: LabelBackground
*separator11.foreground: TextForeground 

*separator12.class: separator
*separator12.parent: AliceForm1
*separator12.static: true
*separator12.name: separator12
*separator12.x: 421
*separator12.y: 102
*separator12.width: 10
*separator12.height: 216
*separator12.orientation: "vertical"
*separator12.background: LabelBackground
*separator12.foreground: TextForeground 

*separator13.class: separator
*separator13.parent: AliceForm1
*separator13.static: true
*separator13.name: separator13
*separator13.x: 2
*separator13.y: 104
*separator13.width: 10
*separator13.height: 215
*separator13.orientation: "vertical"
*separator13.background: LabelBackground
*separator13.foreground: TextForeground 

*separator14.class: separator
*separator14.parent: AliceForm1
*separator14.static: true
*separator14.name: separator14
*separator14.x: 8
*separator14.y: 99
*separator14.width: 415
*separator14.height: 9
*separator14.background: LabelBackground
*separator14.foreground: TextForeground 

*Unzoom.class: pushButton
*Unzoom.parent: AliceForm1
*Unzoom.static: false
*Unzoom.name: Unzoom
*Unzoom.x: 443
*Unzoom.y: 186
*Unzoom.width: 90
*Unzoom.height: 30
*Unzoom.background: ButtonBackground
*Unzoom.foreground: ButtonForeground
*Unzoom.fontList: BoldTextFont 
*Unzoom.activateCallback: {\
if(specInputFrame)\
 {\
  box(specXcenw2-specDxw2,specXcenw2+specDxw2,\
        specYcenw2-specDyw2,specYcenw2+specDyw2,GXequiv );\
  specXmin = specX[0]; specXmax = specX[specNpix[0]-1];\
  specYmin = specYmax = specY[0];\
 for(i = 0; i < specNpix[0] ; i++)\
 {\
  if(specY[i] > specYmax)\
     specYmax = specY[i];\
  if(specY[i] < specYmin)\
     specYmin = specY[i];\
 }\
 specXcen=specXmin+(specXmax-specXmin)/2;\
 specYcen=specYmin+(specYmax-specYmin)/2;\
 specDx = specXmax - specXcen;\
 specDy = specYmax - specYcen;\
 specXcenw2 =specXcen;\
 specYcenw2 =specYcen;\
 specDxw2 =specDx;\
 specDyw2 =specDy;\
 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
       specYcen-specDy,specYcen+specDy,PLOTMODE);\
 if(OverPlotMode)\
   plot_over();\
 draw_zoom();\
 }\
else\
 noframe_error();\
}
*Unzoom.highlightOnEnter: "true"
*Unzoom.highlightColor: "Black"
*Unzoom.translations: Help 

*AutoFitTButton.class: toggleButton
*AutoFitTButton.parent: AliceForm1
*AutoFitTButton.static: true
*AutoFitTButton.name: AutoFitTButton
*AutoFitTButton.x: 514
*AutoFitTButton.y: 47
*AutoFitTButton.width: 20
*AutoFitTButton.height: 23
*AutoFitTButton.background: LabelBackground 
*AutoFitTButton.selectColor: SelectColor 
*AutoFitTButton.highlightOnEnter: "true"
*AutoFitTButton.translations: HELPTopTab 
*AutoFitTButton.foreground: "Black"
*AutoFitTButton.indicatorSize: 13
*AutoFitTButton.ancestorSensitive: "true"
*AutoFitTButton.labelString: ""

*AutoFitLabel.class: label
*AutoFitLabel.parent: AliceForm1
*AutoFitLabel.static: false
*AutoFitLabel.name: AutoFitLabel
*AutoFitLabel.x: 495
*AutoFitLabel.y: 33
*AutoFitLabel.width: 62
*AutoFitLabel.height: 18
*AutoFitLabel.background: LabelBackground
*AutoFitLabel.fontList: TextFont 
*AutoFitLabel.labelString: "Auto-fit"
*AutoFitLabel.foreground: TextForeground 

