! UIMX ascii 2.0 key: 1162                                                      

*sh_list1.class: topLevelShell
*sh_list1.parent: NO_PARENT
*sh_list1.static: false
*sh_list1.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>\

*sh_list1.ispecdecl:
*sh_list1.funcdecl: swidget create_sh_list1()\

*sh_list1.funcname: create_sh_list1
*sh_list1.funcdef: "swidget", "<create_sh_list1>(%)"
*sh_list1.icode:
*sh_list1.fcode: return(rtrn);\

*sh_list1.auxdecl:
*sh_list1.name: sh_list1
*sh_list1.x: 468
*sh_list1.y: 426
*sh_list1.width: 210
*sh_list1.height: 350
*sh_list1.iconName: "List selection"
*sh_list1.keyboardFocusPolicy: "pointer"
*sh_list1.background: SHelpBackground 
*sh_list1.borderColor: WindowBackground
*sh_list1.defaultFontList: TextFont 

*form4.class: form
*form4.parent: sh_list1
*form4.static: false
*form4.name: form4
*form4.resizePolicy: "resize_none"
*form4.unitType: "pixels"
*form4.x: 0
*form4.y: 0
*form4.width: 305
*form4.height: 240
*form4.background: WindowBackground 
*form4.foreground: TextForeground 

*pushButton2.class: pushButton
*pushButton2.parent: form4
*pushButton2.static: false
*pushButton2.name: pushButton2
*pushButton2.x: 0
*pushButton2.y: 314
*pushButton2.width: 210
*pushButton2.height: 35
*pushButton2.background: ButtonBackground
*pushButton2.fontList: BoldTextFont
*pushButton2.labelString: "Cancel"
*pushButton2.activateCallback: {\
extern swidget ListPopup;\
\
UxPopdownInterface(ListPopup);\
}
*pushButton2.foreground: CancelForeground
*pushButton2.highlightOnEnter: "true"

*scrolledWindow1.class: scrolledWindow
*scrolledWindow1.parent: form4
*scrolledWindow1.static: false
*scrolledWindow1.name: scrolledWindow1
*scrolledWindow1.scrollingPolicy: "application_defined"
*scrolledWindow1.x: 0
*scrolledWindow1.y: 0
*scrolledWindow1.visualPolicy: "variable"
*scrolledWindow1.scrollBarDisplayPolicy: "static"
*scrolledWindow1.shadowThickness: 0
*scrolledWindow1.scrollBarPlacement: "bottom_left"
*scrolledWindow1.background: WindowBackground
*scrolledWindow1.height: 314
*scrolledWindow1.foreground: TextForeground
*scrolledWindow1.bottomShadowColor: WindowBackground 
*scrolledWindow1.topShadowColor: "Black"

*scrolledList1.class: scrolledList
*scrolledList1.parent: scrolledWindow1
*scrolledList1.static: false
*scrolledList1.name: scrolledList1
*scrolledList1.width: 191
*scrolledList1.height: 184
*scrolledList1.scrollBarDisplayPolicy: "static"
*scrolledList1.listSizePolicy: "constant"
*scrolledList1.background: SHelpBackground
*scrolledList1.fontList: TextFont
*scrolledList1.visibleItemCount: 15
*scrolledList1.browseSelectionCallback: {\
char *choice,prgname[40];\
XmListCallbackStruct *cbs;\
extern swidget TextSwidget, ListPopup;\
/*UxPopdownInterface(ListPopup); */\
cbs = (XmListCallbackStruct *)UxCallbackArg;\
\
XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);\
switch(caseList)\
{\
  case CASEBDF:\
   strcpy(specImageName,choice);\
   read_image(specImageName);\
   UxPopdownInterface(ListPopup);\
   break;\
  case CASEPRG: \
   strcpy(prgname,choice);\
   read_prg(prgname);\
   UxPopdownInterface(ListPopup);\
   break;\
  case OVERBDF:\
   read_image_over(choice);\
   break;\
}\
XtFree(choice);   \
\
 \
}
*scrolledList1.createCallback: {\
extern Widget FileListWidget;\
\
/*\
FileListWidget = UxWidget;\
*/\
}
*scrolledList1.foreground: TextForeground
*scrolledList1.highlightColor: "Black"

