! UIMX ascii 2.0 key: 6724                                                      

*translation.table: transTable2
*translation.parent: Printer
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation.<Key>osfDelete: delete-previous-character()

*Printer.class: topLevelShell
*Printer.parent: NO_PARENT
*Printer.static: true
*Printer.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>\
#include <alice_help.h>
*Printer.ispecdecl:
*Printer.funcdecl: swidget create_Printer()\

*Printer.funcname: create_Printer
*Printer.funcdef: "swidget", "<create_Printer>(%)"
*Printer.icode:
*Printer.fcode: return(rtrn);\

*Printer.auxdecl:
*Printer.name: Printer
*Printer.x: 682
*Printer.y: 460
*Printer.width: 299
*Printer.height: 118
*Printer.keyboardFocusPolicy: "pointer"

*form1.class: form
*form1.parent: Printer
*form1.static: true
*form1.name: form1
*form1.resizePolicy: "resize_none"
*form1.unitType: "pixels"
*form1.x: 75
*form1.y: 68
*form1.width: 297
*form1.height: 121
*form1.background: WindowBackground 

*PrinterText.class: text
*PrinterText.parent: form1
*PrinterText.static: true
*PrinterText.name: PrinterText
*PrinterText.x: 138
*PrinterText.y: 14
*PrinterText.width: 152
*PrinterText.height: 38
*PrinterText.background: TextBackground 
*PrinterText.fontList: TextFont 
*PrinterText.createCallback: {\
{\
#include <stdlib.h>\
\
if ( getenv("PRINTER") != NULL ) {\
    strcpy(PrinterName,getenv("PRINTER"));\
    UxPutText(UxFindSwidget("PrinterText"),PrinterName );\
}\
}\
}
*PrinterText.translations: transTable2
*PrinterText.foreground: TextForeground 
*PrinterText.highlightColor: "Black"

*TextLabel1.class: label
*TextLabel1.parent: form1
*TextLabel1.static: true
*TextLabel1.name: TextLabel1
*TextLabel1.x: 12
*TextLabel1.y: 16
*TextLabel1.width: 110
*TextLabel1.height: 34
*TextLabel1.background: LabelBackground
*TextLabel1.fontList: TextFont 
*TextLabel1.labelString: "Printer name :"
*TextLabel1.foreground: TextForeground 
*TextLabel1.highlightColor: "Black"

*SaveAsForm1.class: form
*SaveAsForm1.parent: form1
*SaveAsForm1.static: true
*SaveAsForm1.name: SaveAsForm1
*SaveAsForm1.resizePolicy: "resize_none"
*SaveAsForm1.x: 0
*SaveAsForm1.y: 66
*SaveAsForm1.width: 303
*SaveAsForm1.height: 52
*SaveAsForm1.background: ButtonBackground 

*OkPrinter.class: pushButton
*OkPrinter.parent: SaveAsForm1
*OkPrinter.static: false
*OkPrinter.name: OkPrinter
*OkPrinter.x: 20
*OkPrinter.y: 10
*OkPrinter.width: 90
*OkPrinter.height: 30
*OkPrinter.background: ButtonBackground 
*OkPrinter.fontList: BoldTextFont 
*OkPrinter.foreground: ButtonForeground 
*OkPrinter.labelString: "Ok"
*OkPrinter.highlightOnEnter: "true"
*OkPrinter.activateCallback: {\
strcpy(PrinterName,UxGetText(UxFindSwidget("PrinterText")));\
UxPopdownInterface(UxFindSwidget("Printer"));\
\
}
*OkPrinter.highlightColor: "#000000"

*CancelPrinter.class: pushButton
*CancelPrinter.parent: SaveAsForm1
*CancelPrinter.static: false
*CancelPrinter.name: CancelPrinter
*CancelPrinter.x: 126
*CancelPrinter.y: 10
*CancelPrinter.width: 90
*CancelPrinter.height: 30
*CancelPrinter.background: ButtonBackground 
*CancelPrinter.fontList: BoldTextFont 
*CancelPrinter.foreground: CancelForeground 
*CancelPrinter.labelString: "Cancel"
*CancelPrinter.activateCallback: {\
UxPopdownInterface(UxFindSwidget("Printer"));\
}
*CancelPrinter.highlightOnEnter: "true"
*CancelPrinter.highlightColor: "#000000"

*SaveAsSeparator1.class: separator
*SaveAsSeparator1.parent: form1
*SaveAsSeparator1.static: true
*SaveAsSeparator1.name: SaveAsSeparator1
*SaveAsSeparator1.x: -1
*SaveAsSeparator1.y: 60
*SaveAsSeparator1.width: 300
*SaveAsSeparator1.height: 10
*SaveAsSeparator1.background: ButtonBackground 

