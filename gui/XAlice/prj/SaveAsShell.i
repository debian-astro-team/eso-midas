! UIMX ascii 2.0 key: 6992                                                      

*translation.table: SaveTextTab
*translation.parent: SaveAsShell
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation.<Key>osfDelete: delete-previous-character()

*SaveAsShell.class: topLevelShell
*SaveAsShell.parent: NO_PARENT
*SaveAsShell.static: true
*SaveAsShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>\
#include <alice_help.h>\

*SaveAsShell.ispecdecl:
*SaveAsShell.funcdecl: swidget create_SaveAsShell()\

*SaveAsShell.funcname: create_SaveAsShell
*SaveAsShell.funcdef: "swidget", "<create_SaveAsShell>(%)"
*SaveAsShell.icode:
*SaveAsShell.fcode: return(rtrn);\

*SaveAsShell.auxdecl:
*SaveAsShell.name: SaveAsShell
*SaveAsShell.x: 456
*SaveAsShell.y: 426
*SaveAsShell.width: 296
*SaveAsShell.height: 133
*SaveAsShell.iconName: "Save As"
*SaveAsShell.keyboardFocusPolicy: "pointer"

*SaveAsTopForm.class: form
*SaveAsTopForm.parent: SaveAsShell
*SaveAsTopForm.static: true
*SaveAsTopForm.name: SaveAsTopForm
*SaveAsTopForm.resizePolicy: "resize_none"
*SaveAsTopForm.unitType: "pixels"
*SaveAsTopForm.x: 76
*SaveAsTopForm.y: 62
*SaveAsTopForm.width: 142
*SaveAsTopForm.height: 110
*SaveAsTopForm.background: WindowBackground 

*TextLabel.class: label
*TextLabel.parent: SaveAsTopForm
*TextLabel.static: true
*TextLabel.name: TextLabel
*TextLabel.x: 10
*TextLabel.y: 18
*TextLabel.width: 110
*TextLabel.height: 34
*TextLabel.background: LabelBackground
*TextLabel.fontList: TextFont 
*TextLabel.labelString: "Output file:"
*TextLabel.foreground: TextForeground 
*TextLabel.highlightColor: "Black"

*SaveAsText.class: text
*SaveAsText.parent: SaveAsTopForm
*SaveAsText.static: true
*SaveAsText.name: SaveAsText
*SaveAsText.x: 126
*SaveAsText.y: 16
*SaveAsText.width: 152
*SaveAsText.height: 38
*SaveAsText.background: TextBackground 
*SaveAsText.fontList: TextFont 
*SaveAsText.createCallback: {\
UxPutText(UxFindSwidget("SaveAsText"),"noname.bdf" );\
}
*SaveAsText.translations: SaveTextTab
*SaveAsText.foreground: TextForeground 
*SaveAsText.highlightColor: "Black"

*SaveAsForm2.class: form
*SaveAsForm2.parent: SaveAsTopForm
*SaveAsForm2.static: true
*SaveAsForm2.name: SaveAsForm2
*SaveAsForm2.resizePolicy: "resize_none"
*SaveAsForm2.x: 0
*SaveAsForm2.y: 80
*SaveAsForm2.width: 296
*SaveAsForm2.height: 52
*SaveAsForm2.background: ButtonBackground 

*OkSaveAs.class: pushButton
*OkSaveAs.parent: SaveAsForm2
*OkSaveAs.static: false
*OkSaveAs.name: OkSaveAs
*OkSaveAs.x: 20
*OkSaveAs.y: 10
*OkSaveAs.width: 90
*OkSaveAs.height: 30
*OkSaveAs.background: ButtonBackground 
*OkSaveAs.fontList: BoldTextFont 
*OkSaveAs.foreground: ButtonForeground 
*OkSaveAs.labelString: "Ok"
*OkSaveAs.highlightOnEnter: "true"
*OkSaveAs.activateCallback: {\
strcpy(specSaveName,UxGetText(UxFindSwidget("SaveAsText")));\
save_file(specSaveName);\
UxPopdownInterface(UxFindSwidget("SaveAsShell"));\
\
}
*OkSaveAs.highlightColor: "#000000"

*CancelSaveAs.class: pushButton
*CancelSaveAs.parent: SaveAsForm2
*CancelSaveAs.static: false
*CancelSaveAs.name: CancelSaveAs
*CancelSaveAs.x: 126
*CancelSaveAs.y: 10
*CancelSaveAs.width: 90
*CancelSaveAs.height: 30
*CancelSaveAs.background: ButtonBackground 
*CancelSaveAs.fontList: BoldTextFont 
*CancelSaveAs.foreground: CancelForeground 
*CancelSaveAs.labelString: "Cancel"
*CancelSaveAs.activateCallback: {\
UxPopdownInterface(UxFindSwidget("SaveAsShell"));\
}
*CancelSaveAs.highlightOnEnter: "true"
*CancelSaveAs.highlightColor: "#000000"

*SaveAsSeparator.class: separator
*SaveAsSeparator.parent: SaveAsTopForm
*SaveAsSeparator.static: true
*SaveAsSeparator.name: SaveAsSeparator
*SaveAsSeparator.x: 0
*SaveAsSeparator.y: 72
*SaveAsSeparator.width: 300
*SaveAsSeparator.height: 10
*SaveAsSeparator.background: ButtonBackground 

