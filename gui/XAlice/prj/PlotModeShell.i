! UIMX ascii 2.0 key: 1773                                                      

*PlotModeShell.class: topLevelShell
*PlotModeShell.parent: NO_PARENT
*PlotModeShell.static: true
*PlotModeShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>\
#include <alice_help.h>
*PlotModeShell.ispecdecl:
*PlotModeShell.funcdecl: swidget create_PlotModeShell()\

*PlotModeShell.funcname: create_PlotModeShell
*PlotModeShell.funcdef: "swidget", "<create_PlotModeShell>(%)"
*PlotModeShell.icode:
*PlotModeShell.fcode: return(rtrn);\

*PlotModeShell.auxdecl:
*PlotModeShell.name: PlotModeShell
*PlotModeShell.x: 610
*PlotModeShell.y: 447
*PlotModeShell.width: 246
*PlotModeShell.height: 150
*PlotModeShell.iconName: "Plot Mode"

*PlotModeForm.class: form
*PlotModeForm.parent: PlotModeShell
*PlotModeForm.static: true
*PlotModeForm.name: PlotModeForm
*PlotModeForm.resizePolicy: "resize_none"
*PlotModeForm.unitType: "pixels"
*PlotModeForm.x: 76
*PlotModeForm.y: 50
*PlotModeForm.width: 246
*PlotModeForm.height: 144
*PlotModeForm.background: WindowBackground 

*PlotModeRow.class: rowColumn
*PlotModeRow.parent: PlotModeForm
*PlotModeRow.static: true
*PlotModeRow.name: PlotModeRow
*PlotModeRow.x: 50
*PlotModeRow.y: 20
*PlotModeRow.width: 140
*PlotModeRow.height: 64
*PlotModeRow.radioBehavior: "true"
*PlotModeRow.background: LabelBackground 

*StraightToggle.class: toggleButton
*StraightToggle.parent: PlotModeRow
*StraightToggle.static: true
*StraightToggle.name: StraightToggle
*StraightToggle.x: 8
*StraightToggle.y: 14
*StraightToggle.width: 136
*StraightToggle.height: 32
*StraightToggle.background: LabelBackground 
*StraightToggle.fontList: TextFont 
*StraightToggle.selectColor: SelectColor 
*StraightToggle.labelString: "Straight line"
*StraightToggle.set: "true"
*StraightToggle.foreground: TextForeground 
*StraightToggle.highlightColor: "#000000"

*HistoToggle.class: toggleButton
*HistoToggle.parent: PlotModeRow
*HistoToggle.static: true
*HistoToggle.name: HistoToggle
*HistoToggle.x: 10
*HistoToggle.y: 42
*HistoToggle.width: 102
*HistoToggle.height: 32
*HistoToggle.background: LabelBackground 
*HistoToggle.fontList: TextFont 
*HistoToggle.selectColor: SelectColor 
*HistoToggle.labelString: "Histogram - like"
*HistoToggle.foreground: TextForeground 
*HistoToggle.highlightColor: "#000000"

*PlotModeform2.class: form
*PlotModeform2.parent: PlotModeForm
*PlotModeform2.static: true
*PlotModeform2.name: PlotModeform2
*PlotModeform2.resizePolicy: "resize_none"
*PlotModeform2.x: -4
*PlotModeform2.y: 98
*PlotModeform2.width: 250
*PlotModeform2.height: 56
*PlotModeform2.background: ButtonBackground

*OkPloteMode.class: pushButton
*OkPloteMode.parent: PlotModeform2
*OkPloteMode.static: false
*OkPloteMode.name: OkPloteMode
*OkPloteMode.x: 27
*OkPloteMode.y: 9
*OkPloteMode.width: 90
*OkPloteMode.height: 30
*OkPloteMode.background: ButtonBackground
*OkPloteMode.fontList: BoldTextFont 
*OkPloteMode.foreground: ButtonForeground 
*OkPloteMode.labelString: "Ok"
*OkPloteMode.highlightOnEnter: "true"
*OkPloteMode.activateCallback: {\
Read_plotmode();\
UxPopdownInterface(PlotModeShell);\
}
*OkPloteMode.highlightColor: "#000000"

*CancelPlotMode.class: pushButton
*CancelPlotMode.parent: PlotModeform2
*CancelPlotMode.static: false
*CancelPlotMode.name: CancelPlotMode
*CancelPlotMode.x: 137
*CancelPlotMode.y: 9
*CancelPlotMode.width: 90
*CancelPlotMode.height: 30
*CancelPlotMode.background: ButtonBackground
*CancelPlotMode.fontList: BoldTextFont 
*CancelPlotMode.foreground: CancelForeground 
*CancelPlotMode.labelString: "Cancel"
*CancelPlotMode.activateCallback: {\
Set_plotmode();\
UxPopdownInterface(PlotModeShell);\
}
*CancelPlotMode.highlightOnEnter: "true"
*CancelPlotMode.highlightColor: "#000000"

*PlotModeSeparator.class: separator
*PlotModeSeparator.parent: PlotModeForm
*PlotModeSeparator.static: true
*PlotModeSeparator.name: PlotModeSeparator
*PlotModeSeparator.x: 2
*PlotModeSeparator.y: 92
*PlotModeSeparator.width: 246
*PlotModeSeparator.height: 8
*PlotModeSeparator.background: ButtonBackground

