! UIMX ascii 2.0 key: 8131                                                      

*translation.table: LabelTab
*translation.parent: LabelOptions
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation.<Key>osfDelete: delete-previous-character()

*LabelOptions.class: topLevelShell
*LabelOptions.parent: NO_PARENT
*LabelOptions.static: false
*LabelOptions.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>\

*LabelOptions.ispecdecl:
*LabelOptions.funcdecl: swidget create_LabelOptions()\

*LabelOptions.funcname: create_LabelOptions
*LabelOptions.funcdef: "swidget", "<create_LabelOptions>(%)"
*LabelOptions.icode:
*LabelOptions.fcode: return(rtrn);\

*LabelOptions.auxdecl:
*LabelOptions.name: LabelOptions
*LabelOptions.x: 502
*LabelOptions.y: 470
*LabelOptions.width: 396
*LabelOptions.height: 183
*LabelOptions.iconName: "Label Options"
*LabelOptions.keyboardFocusPolicy: "pointer"

*form9.class: form
*form9.parent: LabelOptions
*form9.static: false
*form9.name: form9
*form9.resizePolicy: "resize_none"
*form9.unitType: "pixels"
*form9.x: 68
*form9.y: 44
*form9.width: 208
*form9.height: 66
*form9.background: ButtonBackground 

*Titletext.class: text
*Titletext.parent: form9
*Titletext.static: false
*Titletext.name: Titletext
*Titletext.x: 102
*Titletext.y: 20
*Titletext.width: 282
*Titletext.height: 34
*Titletext.background: TextBackground 
*Titletext.foreground: TextForeground 
*Titletext.highlightOnEnter: "true"
*Titletext.fontList: TextFont  
*Titletext.translations: LabelTab
*Titletext.highlightColor: "Black"
*Titletext.marginHeight: 3

*labelxtext.class: text
*labelxtext.parent: form9
*labelxtext.static: false
*labelxtext.name: labelxtext
*labelxtext.x: 102
*labelxtext.y: 56
*labelxtext.width: 282
*labelxtext.height: 34
*labelxtext.background: TextBackground 
*labelxtext.foreground: TextForeground 
*labelxtext.highlightOnEnter: "true"
*labelxtext.fontList: TextFont  
*labelxtext.translations: LabelTab
*labelxtext.highlightColor: "Black"
*labelxtext.marginHeight: 3

*labelytext.class: text
*labelytext.parent: form9
*labelytext.static: false
*labelytext.name: labelytext
*labelytext.x: 102
*labelytext.y: 92
*labelytext.width: 282
*labelytext.height: 34
*labelytext.background: TextBackground 
*labelytext.foreground: TextForeground 
*labelytext.highlightOnEnter: "true"
*labelytext.fontList: TextFont  
*labelytext.translations: LabelTab
*labelytext.highlightColor: "Black"
*labelytext.marginHeight: 3

*label10.class: label
*label10.parent: form9
*label10.static: false
*label10.name: label10
*label10.x: 14
*label10.y: 22
*label10.width: 78
*label10.height: 28
*label10.background: ButtonBackground 
*label10.foreground: TextForeground 
*label10.labelString: "Title :"
*label10.fontList: TextFont 
*label10.highlightColor: "Black"
*label10.alignment: "alignment_beginning"

*label11.class: label
*label11.parent: form9
*label11.static: false
*label11.name: label11
*label11.x: 16
*label11.y: 58
*label11.width: 78
*label11.height: 28
*label11.background: ButtonBackground 
*label11.foreground: TextForeground 
*label11.labelString: "Label X :"
*label11.fontList: TextFont 
*label11.highlightColor: "Black"
*label11.alignment: "alignment_beginning"

*label12.class: label
*label12.parent: form9
*label12.static: false
*label12.name: label12
*label12.x: 16
*label12.y: 94
*label12.width: 78
*label12.height: 28
*label12.background: ButtonBackground 
*label12.foreground: TextForeground 
*label12.labelString: "Label Y :"
*label12.fontList: TextFont 
*label12.highlightColor: "Black"
*label12.alignment: "alignment_beginning"

*OkLabelOptions.class: pushButton
*OkLabelOptions.parent: form9
*OkLabelOptions.static: false
*OkLabelOptions.name: OkLabelOptions
*OkLabelOptions.x: 30
*OkLabelOptions.y: 138
*OkLabelOptions.width: 90
*OkLabelOptions.height: 30
*OkLabelOptions.background: ButtonBackground 
*OkLabelOptions.fontList: BoldTextFont 
*OkLabelOptions.foreground: ButtonForeground 
*OkLabelOptions.labelString: "Ok"
*OkLabelOptions.activateCallback: {\
newlabels();\
spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
       specYcen-specDy,specYcen+specDy,PLOTMODE);\
if(OverPlotMode)\
   plot_over();\
UxPopdownInterface(LabelOptions);\
}
*OkLabelOptions.highlightOnEnter: "true"
*OkLabelOptions.highlightColor: "Black"

*CancelLabelOptions.class: pushButton
*CancelLabelOptions.parent: form9
*CancelLabelOptions.static: false
*CancelLabelOptions.name: CancelLabelOptions
*CancelLabelOptions.x: 142
*CancelLabelOptions.y: 138
*CancelLabelOptions.width: 90
*CancelLabelOptions.height: 30
*CancelLabelOptions.background: ButtonBackground 
*CancelLabelOptions.fontList: BoldTextFont 
*CancelLabelOptions.foreground: CancelForeground 
*CancelLabelOptions.labelString: "Cancel"
*CancelLabelOptions.activateCallback: {\
UxPopdownInterface(LabelOptions);\
}
*CancelLabelOptions.highlightOnEnter: "true"
*CancelLabelOptions.highlightColor: "Black"

