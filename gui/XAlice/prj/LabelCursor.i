! UIMX ascii 2.0 key: 8365                                                      

*translation.table: CursorTab
*translation.parent: LabelCursor
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation.<Key>osfDelete: delete-previous-character()

*LabelCursor.class: topLevelShell
*LabelCursor.parent: NO_PARENT
*LabelCursor.static: true
*LabelCursor.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>
*LabelCursor.ispecdecl:
*LabelCursor.funcdecl: swidget create_LabelCursor()\

*LabelCursor.funcname: create_LabelCursor
*LabelCursor.funcdef: "swidget", "<create_LabelCursor>(%)"
*LabelCursor.icode:
*LabelCursor.fcode: return(rtrn);\

*LabelCursor.auxdecl:
*LabelCursor.name: LabelCursor
*LabelCursor.x: 533
*LabelCursor.y: 448
*LabelCursor.width: 384
*LabelCursor.height: 148
*LabelCursor.keyboardFocusPolicy: "pointer"
*LabelCursor.iconName: "Label Cursor"

*form10.class: form
*form10.parent: LabelCursor
*form10.static: true
*form10.name: form10
*form10.resizePolicy: "resize_none"
*form10.unitType: "pixels"
*form10.x: 52
*form10.y: 16
*form10.width: 182
*form10.height: 98
*form10.background: WindowBackground 

*form11.class: form
*form11.parent: form10
*form11.static: true
*form11.name: form11
*form11.resizePolicy: "resize_none"
*form11.x: 0
*form11.y: 96
*form11.width: 386
*form11.height: 52
*form11.background: ButtonBackground 

*OkLabelCursor.class: pushButton
*OkLabelCursor.parent: form11
*OkLabelCursor.static: true
*OkLabelCursor.name: OkLabelCursor
*OkLabelCursor.x: 132
*OkLabelCursor.y: 12
*OkLabelCursor.width: 90
*OkLabelCursor.height: 30
*OkLabelCursor.background: ButtonBackground 
*OkLabelCursor.fontList: BoldTextFont 
*OkLabelCursor.foreground: ButtonForeground 
*OkLabelCursor.labelString: "Ok"
*OkLabelCursor.activateCallback: {\
char label[240];\
strcpy(label, UxGetText(UxFindSwidget("LabelText")));\
labelCursor(label,plotAngle,plotSize,plotFont);\
 \
\
 \
 \
}
*OkLabelCursor.highlightOnEnter: "true"
*OkLabelCursor.highlightColor: "Black"

*CancelLabel.class: pushButton
*CancelLabel.parent: form11
*CancelLabel.static: true
*CancelLabel.name: CancelLabel
*CancelLabel.x: 240
*CancelLabel.y: 12
*CancelLabel.width: 90
*CancelLabel.height: 30
*CancelLabel.background: ButtonBackground 
*CancelLabel.fontList: BoldTextFont 
*CancelLabel.foreground: CancelForeground 
*CancelLabel.labelString: "Cancel"
*CancelLabel.activateCallback: {\
UxPopdownInterface(LabelCursor);\
}
*CancelLabel.highlightOnEnter: "true"
*CancelLabel.highlightColor: "Black"

*DefaultLabel.class: pushButton
*DefaultLabel.parent: form11
*DefaultLabel.static: true
*DefaultLabel.name: DefaultLabel
*DefaultLabel.x: 24
*DefaultLabel.y: 12
*DefaultLabel.width: 90
*DefaultLabel.height: 30
*DefaultLabel.background: ButtonBackground 
*DefaultLabel.fontList: BoldTextFont 
*DefaultLabel.foreground: ButtonForeground 
*DefaultLabel.labelString: "Default"
*DefaultLabel.activateCallback: {\
plotFont = 0;\
plotAngle = 0.0;\
plotSize = 1.5;\
UxPutText(UxFindSwidget("AngleText"),"0");\
UxPutText(UxFindSwidget("SizeText"),"1.5");\
UxPutText(UxFindSwidget("FontText"),"Standard");\
UxPutText(UxFindSwidget("LabelText"),"");\
}
*DefaultLabel.highlightOnEnter: "true"
*DefaultLabel.highlightColor: "Black"

*separator2.class: separator
*separator2.parent: form10
*separator2.static: true
*separator2.name: separator2
*separator2.x: -2
*separator2.y: 90
*separator2.width: 386
*separator2.height: 6
*separator2.background: ButtonBackground 

*LabelText.class: text
*LabelText.parent: form10
*LabelText.static: true
*LabelText.name: LabelText
*LabelText.x: 60
*LabelText.y: 6
*LabelText.width: 316
*LabelText.height: 32
*LabelText.background: TextBackground 
*LabelText.maxLength: 240
*LabelText.highlightOnEnter: "true"
*LabelText.fontList: TextFont 
*LabelText.translations: CursorTab
*LabelText.foreground: TextForeground 
*LabelText.highlightColor: "Black"
*LabelText.marginHeight: 3

*label13.class: label
*label13.parent: form10
*label13.static: true
*label13.name: label13
*label13.x: 6
*label13.y: 8
*label13.width: 52
*label13.height: 30
*label13.background: LabelBackground 
*label13.labelString: "Text :"
*label13.fontList: TextFont 
*label13.foreground: TextForeground 
*label13.highlightColor: "Black"

*AngleText.class: text
*AngleText.parent: form10
*AngleText.static: true
*AngleText.name: AngleText
*AngleText.x: 68
*AngleText.y: 50
*AngleText.width: 42
*AngleText.height: 32
*AngleText.background: TextBackground 
*AngleText.losingFocusCallback: {\
float aux_angle;\
char out[20];\
if( plotModAngle)\
{ plotModAngle = FALSE;\
  if(sscanf(UxGetText( UxFindSwidget("AngleText") ) ,"%f",&aux_angle)==1)\
   plotAngle = aux_angle;\
  else\
  {\
  sprintf(out,"%g",plotAngle);\
  UxPutText( UxFindSwidget("AngleText") ,out); \
  out_error("Invalid angle input");\
  }  \
}\
}
*AngleText.modifyVerifyCallback: {\
plotModAngle = TRUE;\
}
*AngleText.maxLength: 40
*AngleText.highlightOnEnter: "true"
*AngleText.fontList: TextFont 
*AngleText.translations: CursorTab
*AngleText.foreground: TextForeground 
*AngleText.highlightColor: "Black"
*AngleText.marginHeight: 3

*SizeText.class: text
*SizeText.parent: form10
*SizeText.static: true
*SizeText.name: SizeText
*SizeText.x: 170
*SizeText.y: 50
*SizeText.width: 42
*SizeText.height: 32
*SizeText.background: TextBackground 
*SizeText.losingFocusCallback: {\
float aux_size;\
char out[20];\
if( plotModSize)\
{ plotModSize = FALSE;\
  if(sscanf(UxGetText( UxFindSwidget("SizeText" )) ,"%f",&aux_size)==1)\
   plotSize = aux_size;\
  else\
  {\
  sprintf(out,"%g",plotSize);\
  UxPutText(UxFindSwidget("SizeText") ,out); \
  out_error("Invalid size input");\
  }  \
}\
}
*SizeText.modifyVerifyCallback: {\
plotModSize = TRUE;\
}
*SizeText.maxLength: 40
*SizeText.highlightOnEnter: "true"
*SizeText.fontList: TextFont 
*SizeText.translations: CursorTab
*SizeText.foreground: TextForeground 
*SizeText.highlightColor: "Black"
*SizeText.marginHeight: 3

*FontText.class: text
*FontText.parent: form10
*FontText.static: true
*FontText.name: FontText
*FontText.x: 254
*FontText.y: 50
*FontText.width: 94
*FontText.height: 32
*FontText.background: TextBackground 
*FontText.editable: "false"
*FontText.maxLength: 20
*FontText.columns: 12
*FontText.highlightOnEnter: "true"
*FontText.fontList: TextFont 
*FontText.cursorPositionVisible: "false"
*FontText.foreground: TextForeground 
*FontText.highlightColor: "Black"
*FontText.marginHeight: 3
*FontText.marginWidth: 3

*label14.class: label
*label14.parent: form10
*label14.static: true
*label14.name: label14
*label14.x: 10
*label14.y: 50
*label14.width: 58
*label14.height: 30
*label14.background: LabelBackground 
*label14.labelString: "Angle :"
*label14.fontList: TextFont 
*label14.foreground: TextForeground 
*label14.highlightColor: "Black"

*label15.class: label
*label15.parent: form10
*label15.static: true
*label15.name: label15
*label15.x: 110
*label15.y: 50
*label15.width: 58
*label15.height: 30
*label15.background: LabelBackground 
*label15.labelString: "Size :"
*label15.fontList: TextFont 
*label15.foreground: TextForeground 
*label15.highlightColor: "Black"

*label16.class: label
*label16.parent: form10
*label16.static: true
*label16.name: label16
*label16.x: 214
*label16.y: 50
*label16.width: 40
*label16.height: 30
*label16.background: LabelBackground 
*label16.labelString: "Font :"
*label16.fontList: TextFont 
*label16.foreground: TextForeground 
*label16.highlightColor: "Black"

*arrowButton13.class: arrowButton
*arrowButton13.parent: form10
*arrowButton13.static: true
*arrowButton13.name: arrowButton13
*arrowButton13.x: 346
*arrowButton13.y: 48
*arrowButton13.width: 30
*arrowButton13.height: 20
*arrowButton13.foreground: MenuBackground 
*arrowButton13.activateCallback: {\
if(plotFont < 5)\
 plotFont++;\
else \
 plotFont = 0;\
putFontText(plotFont);\
}
*arrowButton13.highlightOnEnter: "true"
*arrowButton13.background: TextBackground 

*arrowButton14.class: arrowButton
*arrowButton14.parent: form10
*arrowButton14.static: true
*arrowButton14.name: arrowButton14
*arrowButton14.x: 346
*arrowButton14.y: 66
*arrowButton14.width: 30
*arrowButton14.height: 20
*arrowButton14.arrowDirection: "arrow_down"
*arrowButton14.foreground: MenuBackground 
*arrowButton14.activateCallback: {\
if(plotFont > 1)\
 plotFont--;\
else \
 plotFont = 5;\
putFontText(plotFont);\
}
*arrowButton14.highlightOnEnter: "true"
*arrowButton14.background: TextBackground 

