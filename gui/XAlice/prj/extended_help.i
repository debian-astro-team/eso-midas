! UIMX ascii 2.0 key: 3087                                                      

*HelpTopLevel.class: topLevelShell
*HelpTopLevel.parent: NO_PARENT
*HelpTopLevel.static: true
*HelpTopLevel.gbldecl: #include <stdio.h>\

*HelpTopLevel.funcdecl: swidget create_HelpTopLevel()\

*HelpTopLevel.funcname: create_HelpTopLevel
*HelpTopLevel.funcdef: "swidget", "<create_HelpTopLevel>(%)"
*HelpTopLevel.fcode: return(rtrn);\

*HelpTopLevel.name: HelpTopLevel
*HelpTopLevel.x: 20
*HelpTopLevel.y: 28
*HelpTopLevel.width: 708
*HelpTopLevel.height: 415
*HelpTopLevel.title: "Extended help"
*HelpTopLevel.keyboardFocusPolicy: "pointer"
*HelpTopLevel.iconName: "Extended help"
*HelpTopLevel.background: ButtonBackground 

*form20.class: form
*form20.parent: HelpTopLevel
*form20.static: true
*form20.name: form20
*form20.resizePolicy: "resize_none"
*form20.unitType: "pixels"
*form20.x: 0
*form20.y: 0
*form20.width: 664
*form20.height: 284
*form20.background: ButtonBackground 

*scrolledWindow9.class: scrolledWindow
*scrolledWindow9.parent: form20
*scrolledWindow9.static: true
*scrolledWindow9.name: scrolledWindow9
*scrolledWindow9.scrollingPolicy: "automatic"
*scrolledWindow9.x: 0
*scrolledWindow9.y: 2
*scrolledWindow9.width: 710
*scrolledWindow9.height: 372
*scrolledWindow9.scrollBarPlacement: "bottom_left"
*scrolledWindow9.background: WindowBackground 

*rowColumn4.class: rowColumn
*rowColumn4.parent: scrolledWindow9
*rowColumn4.static: true
*rowColumn4.name: rowColumn4
*rowColumn4.x: 25
*rowColumn4.y: 4
*rowColumn4.width: 418
*rowColumn4.height: 427
*rowColumn4.orientation: "horizontal"
*rowColumn4.background: "white"
*rowColumn4.entryAlignment: "alignment_beginning"

*help_Text.class: text
*help_Text.parent: rowColumn4
*help_Text.static: true
*help_Text.name: help_Text
*help_Text.x: 3
*help_Text.y: 3
*help_Text.width: 674
*help_Text.height: 595
*help_Text.background: TextBackground 
*help_Text.fontList: TextFont 
*help_Text.editMode: "multi_line_edit"
*help_Text.cursorPositionVisible: "false"
*help_Text.foreground: TextForeground 
*help_Text.highlightColor: "Black"

*help_print.class: pushButton
*help_print.parent: form20
*help_print.static: true
*help_print.name: help_print
*help_print.x: 532
*help_print.y: 378
*help_print.width: 82
*help_print.height: 32
*help_print.background: ButtonBackground 
*help_print.labelString: "print"
*help_print.fontList: BoldTextFont 
*help_print.highlightColor: "Black"
*help_print.highlightOnEnter: "true"
*help_print.foreground: ApplyForeground 

*pb_help_return.class: pushButton
*pb_help_return.parent: form20
*pb_help_return.static: true
*pb_help_return.name: pb_help_return
*pb_help_return.x: 616
*pb_help_return.y: 378
*pb_help_return.width: 82
*pb_help_return.height: 32
*pb_help_return.background: ButtonBackground 
*pb_help_return.fontList: BoldTextFont 
*pb_help_return.labelString: "return"
*pb_help_return.activateCallback: {\
extern swidget ExtendedHelp;\
\
UxPopdownInterface(ExtendedHelp);\
\
}
*pb_help_return.highlightColor: "red"
*pb_help_return.highlightOnEnter: "true"
*pb_help_return.foreground: ButtonForeground 

