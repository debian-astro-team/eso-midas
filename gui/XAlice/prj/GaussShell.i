! UIMX ascii 2.0 key: 117                                                       
*action.HelpGauss: {\
 if( UxWidget == UxGetWidget(toggleButton1))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);\
 else if( UxWidget == UxGetWidget(toggleButton2))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);\
 else if( UxWidget == UxGetWidget(toggleButton3))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);\
 else if( UxWidget == UxGetWidget(toggleButton4))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);\
 else if( UxWidget == UxGetWidget(toggleButton5))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);\
 else if( UxWidget == UxGetWidget(toggleButton6))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);\
 else if( UxWidget == UxGetWidget(toggleButton7))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);\
 else if( UxWidget == UxGetWidget(toggleButton8))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);\
 else if( UxWidget == UxGetWidget(toggleButton9))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPONENT);\
 else if( UxWidget == UxGetWidget(GaussUpComponent))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPUPAR);\
 else if( UxWidget == UxGetWidget(GaussDownComponent))\
  UxPutText(UxFindSwidget("help_text_gauss"),COMPDNAR);\
 if( UxWidget == UxGetWidget(ClearGauss))\
  UxPutText(UxFindSwidget("help_text_gauss"),CLEAR);\
if( UxWidget == UxGetWidget(CopyGauss))\
  UxPutText(UxFindSwidget("help_text_gauss"),COPY);\
 else if( UxWidget == UxGetWidget(ExecuteGauss))\
  UxPutText(UxFindSwidget("help_text_gauss"),EXECUTE);\
 else if( UxWidget == UxGetWidget(toggleButton10))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);\
 else if( UxWidget == UxGetWidget(toggleButton13))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);\
 else if( UxWidget == UxGetWidget(toggleButton16))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);\
 else if( UxWidget == UxGetWidget(toggleButton19))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);\
 else if( UxWidget == UxGetWidget(toggleButton22))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);\
 else if( UxWidget == UxGetWidget(toggleButton25))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);\
 else if( UxWidget == UxGetWidget(toggleButton28))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);\
 else if( UxWidget == UxGetWidget(toggleButton31))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);\
 else if( UxWidget == UxGetWidget(toggleButton34))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX1);\
 else if( UxWidget == UxGetWidget(toggleButton11))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);\
 else if( UxWidget == UxGetWidget(toggleButton14))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);\
 else if( UxWidget == UxGetWidget(toggleButton17))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);\
 else if( UxWidget == UxGetWidget(toggleButton20))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);\
 else if( UxWidget == UxGetWidget(toggleButton23))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);\
 else if( UxWidget == UxGetWidget(toggleButton26))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);\
 else if( UxWidget == UxGetWidget(toggleButton29))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);\
 else if( UxWidget == UxGetWidget(toggleButton32))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);\
 else if( UxWidget == UxGetWidget(toggleButton35))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX2);\
 if ( UxWidget == UxGetWidget(toggleButton12))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);\
 else if( UxWidget == UxGetWidget(toggleButton15))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);\
 else if( UxWidget == UxGetWidget(toggleButton18))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);\
 else if( UxWidget == UxGetWidget(toggleButton21))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);\
 else if( UxWidget == UxGetWidget(toggleButton24))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);\
 else if( UxWidget == UxGetWidget(toggleButton27))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);\
 else if( UxWidget == UxGetWidget(toggleButton30))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);\
 else if( UxWidget == UxGetWidget(toggleButton33))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);\
 else if( UxWidget == UxGetWidget(toggleButton36))\
  UxPutText(UxFindSwidget("help_text_gauss"),FIX3);\
}
*action.clearHelpGauss: {UxPutText(UxFindSwidget("help_text_gauss"),"");}
*action.HelpHelp: DisplayExtendedHelp(UxWidget); 

*translation.table: HELPGausstab
*translation.parent: GaussShell
*translation.policy: override
*translation.<EnterWindow>: HelpGauss()
*translation.<LeaveWindow>: clearHelpGauss()

*translation.table: TextGaussTab
*translation.parent: GaussShell
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation.<Key>osfDelete: delete-previous-character()

*translation.table: HelpGaussTable
*translation.parent: GaussShell
*translation.policy: override
*translation.<Btn3Down>: HelpHelp()
*translation.<EnterWindow>: HelpGauss()
*translation.<LeaveWindow>: clearHelpGauss()

*GaussShell.class: applicationShell
*GaussShell.parent: NO_PARENT
*GaussShell.static: true
*GaussShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <alice_global.h>\
#include <alice_help.h>\

*GaussShell.ispecdecl:
*GaussShell.funcdecl: swidget create_GaussShell()\

*GaussShell.funcname: create_GaussShell
*GaussShell.funcdef: "swidget", "<create_GaussShell>(%)"
*GaussShell.icode:
*GaussShell.fcode: return(rtrn);\

*GaussShell.auxdecl:
*GaussShell.name: GaussShell
*GaussShell.x: 466
*GaussShell.y: 424
*GaussShell.width: 588
*GaussShell.height: 455
*GaussShell.keyboardFocusPolicy: "pointer"
*GaussShell.sensitive: "true"
*GaussShell.ancestorSensitive: "true"

*GaussWindow.class: mainWindow
*GaussWindow.parent: GaussShell
*GaussWindow.static: false
*GaussWindow.name: GaussWindow
*GaussWindow.unitType: "pixels"
*GaussWindow.x: 122
*GaussWindow.y: 86
*GaussWindow.width: 278
*GaussWindow.height: 220
*GaussWindow.ancestorSensitive: "true"
*GaussWindow.sensitive: "true"

*GaussMenu.class: rowColumn
*GaussMenu.parent: GaussWindow
*GaussMenu.static: true
*GaussMenu.name: GaussMenu
*GaussMenu.rowColumnType: "menu_bar"
*GaussMenu.background: MenuBackground 
*GaussMenu.menuHelpWidget: "menu1_top_b4"
*GaussMenu.menuAccelerator: "<KeyUp>F10"

*FramePane2.class: rowColumn
*FramePane2.parent: GaussMenu
*FramePane2.static: true
*FramePane2.name: FramePane2
*FramePane2.rowColumnType: "menu_pulldown"
*FramePane2.background: MenuBackground 
*FramePane2.foreground: MenuForeground 
*FramePane2.highlightColor: "#000000"

*CutxGauss.class: pushButton
*CutxGauss.parent: FramePane2
*CutxGauss.static: true
*CutxGauss.name: CutxGauss
*CutxGauss.labelString: "Cut X"
*CutxGauss.mnemonic: "X"
*CutxGauss.activateCallback: {\
 zoom(MOVE_X2);\
\
}
*CutxGauss.background: MenuBackground 
*CutxGauss.fontList: BoldTextFont 
*CutxGauss.foreground: MenuForeground 
*CutxGauss.highlightColor: "#000000"

*CutyGauss.class: pushButton
*CutyGauss.parent: FramePane2
*CutyGauss.static: true
*CutyGauss.name: CutyGauss
*CutyGauss.labelString: "Cut Y"
*CutyGauss.mnemonic: "Y"
*CutyGauss.activateCallback: {\
zoom(MOVE_Y2);\
\
}
*CutyGauss.background: MenuBackground 
*CutyGauss.fontList: BoldTextFont 
*CutyGauss.foreground: MenuForeground 
*CutyGauss.highlightColor: "#000000"

*UnzoomGauss.class: pushButton
*UnzoomGauss.parent: FramePane2
*UnzoomGauss.static: true
*UnzoomGauss.name: UnzoomGauss
*UnzoomGauss.labelString: "Unzoom"
*UnzoomGauss.mnemonic: "U"
*UnzoomGauss.activateCallback: {\
 specXcen = specXcenw2;\
 specYcen = specYcenw2;\
 specDx = specDxw2;\
 specDy = specDyw2;\
 spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
      specYcen-specDy,specYcen+specDy,PLOTMODE);\
if(OverPlotMode)\
   plot_over();\
}
*UnzoomGauss.background: MenuBackground 
*UnzoomGauss.fontList: BoldTextFont 
*UnzoomGauss.foreground: MenuForeground 
*UnzoomGauss.highlightColor: "#000000"

*RedrawGauss.class: pushButton
*RedrawGauss.parent: FramePane2
*RedrawGauss.static: true
*RedrawGauss.name: RedrawGauss
*RedrawGauss.labelString: "Redraw"
*RedrawGauss.mnemonic: "R"
*RedrawGauss.activateCallback: {\
  spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,\
       specYcen-specDy,specYcen+specDy,PLOTMODE);\
  if(OverPlotMode)\
   plot_over();\
}
*RedrawGauss.background: MenuBackground 
*RedrawGauss.fontList: BoldTextFont 
*RedrawGauss.foreground: MenuForeground 
*RedrawGauss.highlightColor: "#000000"

*PrintGauss.class: pushButton
*PrintGauss.parent: FramePane2
*PrintGauss.static: true
*PrintGauss.name: PrintGauss
*PrintGauss.labelString: "Print"
*PrintGauss.mnemonic: "P"
*PrintGauss.activateCallback: {\
if(XmToggleButtonGetState(UxGetWidget(UxFindSwidget("NormalToggle"))))\
   print_plot(NORMALPRINT,PrinterName);\
else\
   print_plot(PORTRAITPRINT,PrinterName);\
}\
 
*PrintGauss.background: MenuBackground 
*PrintGauss.fontList: BoldTextFont 
*PrintGauss.foreground: MenuForeground 
*PrintGauss.highlightColor: "#000000"

*GraphicsPane.class: rowColumn
*GraphicsPane.parent: GaussMenu
*GraphicsPane.static: true
*GraphicsPane.name: GraphicsPane
*GraphicsPane.rowColumnType: "menu_pulldown"
*GraphicsPane.background: MenuBackground 
*GraphicsPane.foreground: MenuForeground 
*GraphicsPane.highlightColor: "#000000"

*ResidualItem.class: pushButton
*ResidualItem.parent: GraphicsPane
*ResidualItem.static: true
*ResidualItem.name: ResidualItem
*ResidualItem.labelString: "Draw Residual"
*ResidualItem.mnemonic: "R"
*ResidualItem.activateCallback: {\
if(gaussNumOfSol >0)\
{\
read_fit_values();\
draw_error();\
}\
}
*ResidualItem.background: MenuBackground 
*ResidualItem.fontList: BoldTextFont 
*ResidualItem.foreground: MenuForeground 
*ResidualItem.highlightColor: "#000000"

*InitialItem.class: pushButton
*InitialItem.parent: GraphicsPane
*InitialItem.static: true
*InitialItem.name: InitialItem
*InitialItem.labelString: "Draw initial guess"
*InitialItem.mnemonic: "i"
*InitialItem.activateCallback: {\
read_init_guess();\
draw_init_guess();\
draw_number_component(3);\
}
*InitialItem.background: MenuBackground 
*InitialItem.fontList: BoldTextFont 
*InitialItem.foreground: MenuForeground 
*InitialItem.highlightColor: "#000000"

*ComponentsItem.class: pushButton
*ComponentsItem.parent: GraphicsPane
*ComponentsItem.static: true
*ComponentsItem.name: ComponentsItem
*ComponentsItem.labelString: "Draw fit components"
*ComponentsItem.mnemonic: "c"
*ComponentsItem.activateCallback: {\
int i;\
if(gaussNumOfSol>0)\
{\
 read_fit_values();\
 for(i=0;i<gaussNumOfSol;i++)\
  draw_sgauss(gaussFitValues[i*3],gaussFitValues[i*3+1],\
  gaussFitValues[i*3+2],4);\
}\
 draw_number_component(4);\
}
*ComponentsItem.background: MenuBackground 
*ComponentsItem.fontList: BoldTextFont 
*ComponentsItem.foreground: MenuForeground 
*ComponentsItem.highlightColor: "#000000"

*SolutionItem.class: pushButton
*SolutionItem.parent: GraphicsPane
*SolutionItem.static: true
*SolutionItem.name: SolutionItem
*SolutionItem.labelString: "Draw solution"
*SolutionItem.mnemonic: "s"
*SolutionItem.activateCallback: {\
/*spec(specX,specY,specNpix[0],specXcen-specDx,specXcen+specDx,specYcen-specDy,specYcen+specDy);*/\
if(gaussNumOfSol >0)\
 {\
  read_fit_values();\
  draw_gauss();\
 }\
}
*SolutionItem.background: MenuBackground 
*SolutionItem.fontList: BoldTextFont 
*SolutionItem.foreground: MenuForeground 
*SolutionItem.highlightColor: "#000000"

*ContinItem.class: pushButton
*ContinItem.parent: GraphicsPane
*ContinItem.static: true
*ContinItem.name: ContinItem
*ContinItem.labelString: "Draw continuum"
*ContinItem.mnemonic: "n"
*ContinItem.activateCallback: {\
if(fitAddFit)\
 {\
 if(fitMode == POLYFITMODE)\
  plot_fit(specXaux,specYaux,gaussNumOfFitData-1,fitDegree+1,6);\
 else\
  plot_spline(gaussNumOfFitData,6);\
 }\
}
*ContinItem.background: MenuBackground 
*ContinItem.fontList: BoldTextFont 
*ContinItem.foreground: MenuForeground 
*ContinItem.highlightColor: "#000000"

*OtherPane.class: rowColumn
*OtherPane.parent: GaussMenu
*OtherPane.static: true
*OtherPane.name: OtherPane
*OtherPane.rowColumnType: "menu_pulldown"
*OtherPane.background: MenuBackground 
*OtherPane.foreground: MenuForeground 
*OtherPane.highlightColor: "#000000"

*StatItem.class: pushButton
*StatItem.parent: OtherPane
*StatItem.static: true
*StatItem.name: StatItem
*StatItem.labelString: "Show statistics"
*StatItem.mnemonic: "s"
*StatItem.activateCallback: {\
extern swidget StatShell;\
UxPopupInterface(StatShell,no_grab);\
}
*StatItem.background: MenuBackground 
*StatItem.fontList: BoldTextFont 
*StatItem.foreground: MenuForeground 
*StatItem.highlightColor: "#000000"

*IntegrItem.class: pushButton
*IntegrItem.parent: OtherPane
*IntegrItem.static: true
*IntegrItem.name: IntegrItem
*IntegrItem.labelString: "Print Integration"
*IntegrItem.mnemonic: "I"
*IntegrItem.activateCallback: {\
out_integration();\
}
*IntegrItem.background: MenuBackground 
*IntegrItem.fontList: BoldTextFont 
*IntegrItem.foreground: MenuForeground 
*IntegrItem.highlightColor: "#000000"

*ReturnGauss2.class: rowColumn
*ReturnGauss2.parent: GaussMenu
*ReturnGauss2.static: true
*ReturnGauss2.name: ReturnGauss2
*ReturnGauss2.rowColumnType: "menu_pulldown"
*ReturnGauss2.background: MenuBackground 
*ReturnGauss2.foreground: MenuForeground 
*ReturnGauss2.highlightColor: "#000000"

*ReturnButton.class: pushButton
*ReturnButton.parent: ReturnGauss2
*ReturnButton.static: true
*ReturnButton.name: ReturnButton
*ReturnButton.labelString: "Return"
*ReturnButton.mnemonic: "R"
*ReturnButton.activateCallback: {\
UxPopdownInterface(UxFindSwidget("GaussShell"));\
}
*ReturnButton.background: MenuBackground 
*ReturnButton.fontList: BoldTextFont 
*ReturnButton.foreground: MenuForeground 
*ReturnButton.highlightColor: "#000000"

*menu1_top_b1.class: cascadeButton
*menu1_top_b1.parent: GaussMenu
*menu1_top_b1.static: true
*menu1_top_b1.name: menu1_top_b1
*menu1_top_b1.labelString: "Frame"
*menu1_top_b1.mnemonic: "F"
*menu1_top_b1.subMenuId: "FramePane2"
*menu1_top_b1.background: MenuBackground 
*menu1_top_b1.fontList: BoldTextFont 
*menu1_top_b1.foreground: MenuForeground 
*menu1_top_b1.highlightColor: "#000000"
*menu1_top_b1.ancestorSensitive: "true"

*menu1_top_b2.class: cascadeButton
*menu1_top_b2.parent: GaussMenu
*menu1_top_b2.static: true
*menu1_top_b2.name: menu1_top_b2
*menu1_top_b2.labelString: "Graphics"
*menu1_top_b2.mnemonic: "G"
*menu1_top_b2.subMenuId: "GraphicsPane"
*menu1_top_b2.background: MenuBackground 
*menu1_top_b2.fontList: BoldTextFont 
*menu1_top_b2.foreground: MenuForeground 
*menu1_top_b2.highlightColor: "#000000"
*menu1_top_b2.ancestorSensitive: "true"

*GaussMenu_top_b1.class: cascadeButton
*GaussMenu_top_b1.parent: GaussMenu
*GaussMenu_top_b1.static: true
*GaussMenu_top_b1.name: GaussMenu_top_b1
*GaussMenu_top_b1.labelString: "Other"
*GaussMenu_top_b1.mnemonic: "O"
*GaussMenu_top_b1.subMenuId: "OtherPane"
*GaussMenu_top_b1.background: MenuBackground 
*GaussMenu_top_b1.fontList: BoldTextFont 
*GaussMenu_top_b1.foreground: MenuForeground 
*GaussMenu_top_b1.highlightColor: "#000000"
*GaussMenu_top_b1.ancestorSensitive: "true"

*menu1_top_b4.class: cascadeButton
*menu1_top_b4.parent: GaussMenu
*menu1_top_b4.static: true
*menu1_top_b4.name: menu1_top_b4
*menu1_top_b4.labelString: "Quit"
*menu1_top_b4.mnemonic: "Q"
*menu1_top_b4.subMenuId: "ReturnGauss2"
*menu1_top_b4.background: MenuBackground 
*menu1_top_b4.fontList: BoldTextFont 
*menu1_top_b4.foreground: MenuForeground 
*menu1_top_b4.highlightColor: "#000000"
*menu1_top_b4.ancestorSensitive: "true"

*GaussTopform.class: form
*GaussTopform.parent: GaussWindow
*GaussTopform.static: false
*GaussTopform.name: GaussTopform
*GaussTopform.background: WindowBackground 
*GaussTopform.ancestorSensitive: "true"
*GaussTopform.sensitive: "true"

*label1.class: label
*label1.parent: GaussTopform
*label1.static: false
*label1.name: label1
*label1.x: 8
*label1.y: 308
*label1.width: 110
*label1.height: 30
*label1.background: LabelBackground 
*label1.fontList: TextFont 
*label1.labelString: "Components :"
*label1.foreground: TextForeground 
*label1.highlightColor: "Black"
*label1.ancestorSensitive: "true"

*Components.class: text
*Components.parent: GaussTopform
*Components.static: false
*Components.name: Components
*Components.x: 118
*Components.y: 304
*Components.width: 30
*Components.height: 34
*Components.background: TextBackground 
*Components.losingFocusCallback: {\
if( gaussModNumOfComp)\
{ gaussModNumOfComp = FALSE;\
  sscanf(UxGetText(Components),"%d",&gaussNumOfComp);\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*Components.modifyVerifyCallback: {\
gaussModNumOfComp = TRUE;\
}
*Components.editable: "false"
*Components.fontList: TextFont 
*Components.foreground: TextForeground 
*Components.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),COMPFIELD);\
}
*Components.cursorPositionVisible: "false"
*Components.highlightColor: "Black"
*Components.maxLength: 5
*Components.ancestorSensitive: "true"
*Components.marginHeight: 3
*Components.marginWidth: 5

*label2.class: label
*label2.parent: GaussTopform
*label2.static: false
*label2.name: label2
*label2.x: 4
*label2.y: 2
*label2.width: 83
*label2.height: 20
*label2.background: LabelBackground 
*label2.fontList: TextFont 
*label2.labelString: "Component"
*label2.foreground: TextForeground 
*label2.highlightColor: "Black"
*label2.ancestorSensitive: "true"

*label3.class: label
*label3.parent: GaussTopform
*label3.static: false
*label3.name: label3
*label3.x: 90
*label3.y: 2
*label3.width: 50
*label3.height: 20
*label3.background: LabelBackground 
*label3.fontList: TextFont 
*label3.labelString: "Fix"
*label3.foreground: TextForeground 
*label3.highlightColor: "Black"
*label3.ancestorSensitive: "true"

*label4.class: label
*label4.parent: GaussTopform
*label4.static: false
*label4.name: label4
*label4.x: 150
*label4.y: 2
*label4.width: 210
*label4.height: 20
*label4.background: LabelBackground 
*label4.fontList: TextFont 
*label4.labelString: "Initial Guess"
*label4.foreground: TextForeground 
*label4.highlightColor: "Black"
*label4.ancestorSensitive: "true"

*label5.class: label
*label5.parent: GaussTopform
*label5.static: false
*label5.name: label5
*label5.x: 360
*label5.y: 2
*label5.width: 210
*label5.height: 20
*label5.background: LabelBackground 
*label5.fontList: TextFont 
*label5.labelString: "Fit Values"
*label5.foreground: TextForeground 
*label5.highlightColor: "Black"
*label5.ancestorSensitive: "true"

*toggleButton1.class: toggleButton
*toggleButton1.parent: GaussTopform
*toggleButton1.static: false
*toggleButton1.name: toggleButton1
*toggleButton1.x: 13
*toggleButton1.y: 21
*toggleButton1.width: 60
*toggleButton1.height: 30
*toggleButton1.background: LabelBackground 
*toggleButton1.fontList: BoldTextFont 
*toggleButton1.selectColor: SelectColor 
*toggleButton1.labelString: "     "
*toggleButton1.set: "false"
*toggleButton1.foreground: TextForeground 
*toggleButton1.highlightOnEnter: "true"
*toggleButton1.translations: HELPGausstab
*toggleButton1.highlightColor: "Black"
*toggleButton1.indicatorSize: 16
*toggleButton1.ancestorSensitive: "true"

*toggleButton2.class: toggleButton
*toggleButton2.parent: GaussTopform
*toggleButton2.static: false
*toggleButton2.name: toggleButton2
*toggleButton2.x: 13
*toggleButton2.y: 51
*toggleButton2.width: 60
*toggleButton2.height: 30
*toggleButton2.background: LabelBackground 
*toggleButton2.fontList: BoldTextFont 
*toggleButton2.selectColor: SelectColor 
*toggleButton2.labelString: "     "
*toggleButton2.set: "false"
*toggleButton2.foreground: TextForeground 
*toggleButton2.highlightOnEnter: "true"
*toggleButton2.translations: HELPGausstab
*toggleButton2.highlightColor: "Black"
*toggleButton2.indicatorSize: 16
*toggleButton2.ancestorSensitive: "true"

*toggleButton3.class: toggleButton
*toggleButton3.parent: GaussTopform
*toggleButton3.static: true
*toggleButton3.name: toggleButton3
*toggleButton3.x: 13
*toggleButton3.y: 81
*toggleButton3.width: 60
*toggleButton3.height: 30
*toggleButton3.background: LabelBackground 
*toggleButton3.fontList: BoldTextFont 
*toggleButton3.selectColor: SelectColor 
*toggleButton3.labelString: "     "
*toggleButton3.set: "false"
*toggleButton3.foreground: TextForeground 
*toggleButton3.highlightOnEnter: "true"
*toggleButton3.translations: HELPGausstab
*toggleButton3.highlightColor: "Black"
*toggleButton3.indicatorSize: 16
*toggleButton3.ancestorSensitive: "true"

*toggleButton4.class: toggleButton
*toggleButton4.parent: GaussTopform
*toggleButton4.static: true
*toggleButton4.name: toggleButton4
*toggleButton4.x: 13
*toggleButton4.y: 111
*toggleButton4.width: 60
*toggleButton4.height: 30
*toggleButton4.background: LabelBackground 
*toggleButton4.fontList: BoldTextFont 
*toggleButton4.selectColor: SelectColor 
*toggleButton4.labelString: "     "
*toggleButton4.set: "false"
*toggleButton4.foreground: TextForeground 
*toggleButton4.highlightOnEnter: "true"
*toggleButton4.translations: HELPGausstab
*toggleButton4.highlightColor: "Black"
*toggleButton4.indicatorSize: 16
*toggleButton4.ancestorSensitive: "true"

*toggleButton5.class: toggleButton
*toggleButton5.parent: GaussTopform
*toggleButton5.static: true
*toggleButton5.name: toggleButton5
*toggleButton5.x: 13
*toggleButton5.y: 141
*toggleButton5.width: 60
*toggleButton5.height: 30
*toggleButton5.background: LabelBackground 
*toggleButton5.fontList: BoldTextFont 
*toggleButton5.selectColor: SelectColor 
*toggleButton5.labelString: "     "
*toggleButton5.set: "false"
*toggleButton5.foreground: TextForeground 
*toggleButton5.highlightOnEnter: "true"
*toggleButton5.translations: HELPGausstab
*toggleButton5.highlightColor: "Black"
*toggleButton5.indicatorSize: 16
*toggleButton5.ancestorSensitive: "true"

*toggleButton6.class: toggleButton
*toggleButton6.parent: GaussTopform
*toggleButton6.static: true
*toggleButton6.name: toggleButton6
*toggleButton6.x: 13
*toggleButton6.y: 171
*toggleButton6.width: 60
*toggleButton6.height: 30
*toggleButton6.background: LabelBackground 
*toggleButton6.fontList: BoldTextFont 
*toggleButton6.selectColor: SelectColor 
*toggleButton6.labelString: "     "
*toggleButton6.set: "false"
*toggleButton6.foreground: TextForeground 
*toggleButton6.highlightOnEnter: "true"
*toggleButton6.translations: HELPGausstab
*toggleButton6.highlightColor: "Black"
*toggleButton6.indicatorSize: 16
*toggleButton6.ancestorSensitive: "true"

*toggleButton7.class: toggleButton
*toggleButton7.parent: GaussTopform
*toggleButton7.static: true
*toggleButton7.name: toggleButton7
*toggleButton7.x: 13
*toggleButton7.y: 201
*toggleButton7.width: 60
*toggleButton7.height: 30
*toggleButton7.background: LabelBackground 
*toggleButton7.fontList: BoldTextFont 
*toggleButton7.selectColor: SelectColor 
*toggleButton7.labelString: "     "
*toggleButton7.set: "false"
*toggleButton7.foreground: TextForeground 
*toggleButton7.highlightOnEnter: "true"
*toggleButton7.translations: HELPGausstab
*toggleButton7.highlightColor: "Black"
*toggleButton7.indicatorSize: 16
*toggleButton7.ancestorSensitive: "true"

*toggleButton8.class: toggleButton
*toggleButton8.parent: GaussTopform
*toggleButton8.static: true
*toggleButton8.name: toggleButton8
*toggleButton8.x: 13
*toggleButton8.y: 231
*toggleButton8.width: 60
*toggleButton8.height: 30
*toggleButton8.background: LabelBackground 
*toggleButton8.fontList: BoldTextFont 
*toggleButton8.selectColor: SelectColor 
*toggleButton8.labelString: "     "
*toggleButton8.set: "false"
*toggleButton8.foreground: TextForeground 
*toggleButton8.highlightOnEnter: "true"
*toggleButton8.translations: HELPGausstab
*toggleButton8.highlightColor: "Black"
*toggleButton8.indicatorSize: 16
*toggleButton8.ancestorSensitive: "true"

*toggleButton9.class: toggleButton
*toggleButton9.parent: GaussTopform
*toggleButton9.static: true
*toggleButton9.name: toggleButton9
*toggleButton9.x: 13
*toggleButton9.y: 261
*toggleButton9.width: 60
*toggleButton9.height: 30
*toggleButton9.background: LabelBackground 
*toggleButton9.fontList: BoldTextFont 
*toggleButton9.selectColor: SelectColor 
*toggleButton9.labelString: "     "
*toggleButton9.set: "false"
*toggleButton9.foreground: TextForeground 
*toggleButton9.highlightOnEnter: "true"
*toggleButton9.translations: HELPGausstab
*toggleButton9.highlightColor: "Black"
*toggleButton9.indicatorSize: 16
*toggleButton9.ancestorSensitive: "true"

*textField11.class: text
*textField11.parent: GaussTopform
*textField11.static: false
*textField11.name: textField11
*textField11.x: 151
*textField11.y: 24
*textField11.width: 210
*textField11.height: 30
*textField11.background: TextBackground
*textField11.modifyVerifyCallback: {\
gaussModInitGuess= TRUE;\
}
*textField11.losingFocusCallback: {\
extern read_init_guess();\
if( gaussModInitGuess)\
{ gaussModInitGuess = FALSE;\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*textField11.foreground: TextForeground 
*textField11.maxLength: 320
*textField11.highlightOnEnter: "true"
*textField11.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);\
}
*textField11.fontList: SmallPropFont 
*textField11.translations: TextGaussTab
*textField11.highlightColor: "Black"
*textField11.ancestorSensitive: "true"

*textField12.class: text
*textField12.parent: GaussTopform
*textField12.static: false
*textField12.name: textField12
*textField12.x: 151
*textField12.y: 54
*textField12.width: 210
*textField12.height: 30
*textField12.background: TextBackground
*textField12.modifyVerifyCallback: {\
gaussModInitGuess = TRUE;\
}
*textField12.losingFocusCallback: {\
extern read_init_guess();\
if( gaussModInitGuess)\
{ gaussModInitGuess = FALSE;\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*textField12.foreground: TextForeground 
*textField12.maxLength: 320
*textField12.highlightOnEnter: "true"
*textField12.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);\
}
*textField12.fontList: SmallPropFont 
*textField12.translations: TextGaussTab
*textField12.highlightColor: "Black"
*textField12.ancestorSensitive: "true"

*textField13.class: text
*textField13.parent: GaussTopform
*textField13.static: false
*textField13.name: textField13
*textField13.x: 151
*textField13.y: 84
*textField13.width: 210
*textField13.height: 30
*textField13.background: TextBackground
*textField13.modifyVerifyCallback: {\
gaussModInitGuess = TRUE;\
}
*textField13.losingFocusCallback: {\
extern read_init_guess();\
if( gaussModInitGuess)\
{ gaussModInitGuess = FALSE;\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*textField13.foreground: TextForeground 
*textField13.maxLength: 320
*textField13.highlightOnEnter: "true"
*textField13.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);\
}
*textField13.fontList: SmallPropFont 
*textField13.translations: TextGaussTab
*textField13.highlightColor: "Black"
*textField13.ancestorSensitive: "true"

*textField14.class: text
*textField14.parent: GaussTopform
*textField14.static: false
*textField14.name: textField14
*textField14.x: 151
*textField14.y: 114
*textField14.width: 210
*textField14.height: 30
*textField14.background: TextBackground
*textField14.modifyVerifyCallback: {\
gaussModInitGuess= TRUE;\
}
*textField14.losingFocusCallback: {\
extern read_init_guess();\
if( gaussModInitGuess)\
{ gaussModInitGuess = FALSE;\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*textField14.foreground: TextForeground 
*textField14.maxLength: 320
*textField14.highlightOnEnter: "true"
*textField14.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);\
}
*textField14.fontList: SmallPropFont 
*textField14.translations: TextGaussTab
*textField14.highlightColor: "Black"
*textField14.ancestorSensitive: "true"

*textField15.class: text
*textField15.parent: GaussTopform
*textField15.static: false
*textField15.name: textField15
*textField15.x: 151
*textField15.y: 144
*textField15.width: 210
*textField15.height: 30
*textField15.background: TextBackground
*textField15.modifyVerifyCallback: {\
gaussModInitGuess = TRUE;\
}
*textField15.losingFocusCallback: {\
extern read_init_guess();\
if( gaussModInitGuess)\
{ gaussModInitGuess = FALSE;\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*textField15.foreground: TextForeground 
*textField15.maxLength: 320
*textField15.highlightOnEnter: "true"
*textField15.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);\
}
*textField15.fontList: SmallPropFont 
*textField15.translations: TextGaussTab
*textField15.highlightColor: "Black"
*textField15.ancestorSensitive: "true"

*textField16.class: text
*textField16.parent: GaussTopform
*textField16.static: false
*textField16.name: textField16
*textField16.x: 151
*textField16.y: 174
*textField16.width: 210
*textField16.height: 30
*textField16.background: TextBackground
*textField16.modifyVerifyCallback: {\
gaussModInitGuess = TRUE;\
}
*textField16.losingFocusCallback: {\
extern read_init_guess();\
if( gaussModInitGuess)\
{ gaussModInitGuess = FALSE;\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*textField16.foreground: TextForeground 
*textField16.maxLength: 320
*textField16.highlightOnEnter: "true"
*textField16.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);\
}
*textField16.fontList: SmallPropFont 
*textField16.translations: TextGaussTab
*textField16.highlightColor: "Black"
*textField16.ancestorSensitive: "true"

*textField17.class: text
*textField17.parent: GaussTopform
*textField17.static: false
*textField17.name: textField17
*textField17.x: 151
*textField17.y: 204
*textField17.width: 210
*textField17.height: 30
*textField17.background: TextBackground
*textField17.modifyVerifyCallback: {\
gaussModInitGuess = TRUE;\
}
*textField17.losingFocusCallback: {\
extern read_init_guess();\
if( gaussModInitGuess)\
{ gaussModInitGuess = FALSE;\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*textField17.foreground: TextForeground 
*textField17.maxLength: 320
*textField17.highlightOnEnter: "true"
*textField17.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);\
}
*textField17.fontList: SmallPropFont 
*textField17.translations: TextGaussTab
*textField17.highlightColor: "Black"
*textField17.ancestorSensitive: "true"

*textField18.class: text
*textField18.parent: GaussTopform
*textField18.static: false
*textField18.name: textField18
*textField18.x: 151
*textField18.y: 234
*textField18.width: 210
*textField18.height: 30
*textField18.background: TextBackground
*textField18.modifyVerifyCallback: {\
gaussModInitGuess = TRUE;\
}
*textField18.losingFocusCallback: {\
extern read_init_guess();\
if( gaussModInitGuess)\
{ gaussModInitGuess = FALSE;\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*textField18.foreground: TextForeground 
*textField18.maxLength: 320
*textField18.highlightOnEnter: "true"
*textField18.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);\
}
*textField18.fontList: SmallPropFont 
*textField18.translations: TextGaussTab
*textField18.highlightColor: "Black"
*textField18.ancestorSensitive: "true"

*textField19.class: text
*textField19.parent: GaussTopform
*textField19.static: false
*textField19.name: textField19
*textField19.x: 151
*textField19.y: 264
*textField19.width: 210
*textField19.height: 30
*textField19.background: TextBackground
*textField19.modifyVerifyCallback: {\
gaussModInitGuess = TRUE;\
}
*textField19.losingFocusCallback: {\
extern read_init_guess();\
if( gaussModInitGuess)\
{ gaussModInitGuess = FALSE;\
  read_init_guess();\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*textField19.foreground: TextForeground 
*textField19.maxLength: 320
*textField19.highlightOnEnter: "true"
*textField19.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),GAUSSINIT);\
}
*textField19.fontList: SmallPropFont 
*textField19.translations: TextGaussTab
*textField19.highlightColor: "Black"
*textField19.ancestorSensitive: "true"

*textField21.class: text
*textField21.parent: GaussTopform
*textField21.static: false
*textField21.name: textField21
*textField21.x: 361
*textField21.y: 54
*textField21.width: 210
*textField21.height: 30
*textField21.background: TextBackground
*textField21.editable: "false"
*textField21.foreground: TextForeground 
*textField21.maxLength: 160
*textField21.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*textField21.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),""); \
}
*textField21.fontList: SmallPropFont 
*textField21.cursorPositionVisible: "false"
*textField21.highlightColor: "Black"
*textField21.ancestorSensitive: "true"

*textField22.class: text
*textField22.parent: GaussTopform
*textField22.static: false
*textField22.name: textField22
*textField22.x: 361
*textField22.y: 84
*textField22.width: 210
*textField22.height: 30
*textField22.background: TextBackground
*textField22.editable: "false"
*textField22.foreground: TextForeground 
*textField22.maxLength: 160
*textField22.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*textField22.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),""); \
}
*textField22.fontList: SmallPropFont 
*textField22.cursorPositionVisible: "false"
*textField22.highlightColor: "Black"
*textField22.ancestorSensitive: "true"

*textField23.class: text
*textField23.parent: GaussTopform
*textField23.static: false
*textField23.name: textField23
*textField23.x: 361
*textField23.y: 114
*textField23.width: 210
*textField23.height: 30
*textField23.background: TextBackground
*textField23.editable: "false"
*textField23.foreground: TextForeground 
*textField23.maxLength: 160
*textField23.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*textField23.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),""); \
}
*textField23.fontList: SmallPropFont 
*textField23.cursorPositionVisible: "false"
*textField23.highlightColor: "Black"
*textField23.ancestorSensitive: "true"

*textField24.class: text
*textField24.parent: GaussTopform
*textField24.static: false
*textField24.name: textField24
*textField24.x: 361
*textField24.y: 144
*textField24.width: 210
*textField24.height: 30
*textField24.background: TextBackground
*textField24.editable: "false"
*textField24.foreground: TextForeground 
*textField24.maxLength: 160
*textField24.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*textField24.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),""); \
}
*textField24.fontList: SmallPropFont 
*textField24.cursorPositionVisible: "false"
*textField24.highlightColor: "Black"
*textField24.ancestorSensitive: "true"

*textField25.class: text
*textField25.parent: GaussTopform
*textField25.static: false
*textField25.name: textField25
*textField25.x: 361
*textField25.y: 174
*textField25.width: 210
*textField25.height: 30
*textField25.background: TextBackground
*textField25.editable: "false"
*textField25.foreground: TextForeground 
*textField25.maxLength: 160
*textField25.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*textField25.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),""); \
}
*textField25.fontList: SmallPropFont 
*textField25.cursorPositionVisible: "false"
*textField25.highlightColor: "Black"
*textField25.ancestorSensitive: "true"

*textField26.class: text
*textField26.parent: GaussTopform
*textField26.static: false
*textField26.name: textField26
*textField26.x: 361
*textField26.y: 204
*textField26.width: 210
*textField26.height: 30
*textField26.background: TextBackground
*textField26.editable: "false"
*textField26.foreground: TextForeground 
*textField26.maxLength: 160
*textField26.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*textField26.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),""); \
}
*textField26.fontList: SmallPropFont 
*textField26.cursorPositionVisible: "false"
*textField26.highlightColor: "Black"
*textField26.ancestorSensitive: "true"

*textField27.class: text
*textField27.parent: GaussTopform
*textField27.static: false
*textField27.name: textField27
*textField27.x: 361
*textField27.y: 234
*textField27.width: 210
*textField27.height: 30
*textField27.background: TextBackground
*textField27.editable: "false"
*textField27.foreground: TextForeground 
*textField27.maxLength: 160
*textField27.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*textField27.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),""); \
}
*textField27.fontList: SmallPropFont 
*textField27.cursorPositionVisible: "false"
*textField27.highlightColor: "Black"
*textField27.ancestorSensitive: "true"

*textField28.class: text
*textField28.parent: GaussTopform
*textField28.static: false
*textField28.name: textField28
*textField28.x: 361
*textField28.y: 264
*textField28.width: 210
*textField28.height: 30
*textField28.background: TextBackground
*textField28.editable: "false"
*textField28.foreground: TextForeground 
*textField28.maxLength: 160
*textField28.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*textField28.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),""); \
}
*textField28.fontList: SmallPropFont 
*textField28.cursorPositionVisible: "false"
*textField28.highlightColor: "Black"
*textField28.ancestorSensitive: "true"

*textField20.class: text
*textField20.parent: GaussTopform
*textField20.static: false
*textField20.name: textField20
*textField20.x: 361
*textField20.y: 25
*textField20.width: 210
*textField20.height: 30
*textField20.background: TextBackground
*textField20.editable: "false"
*textField20.foreground: TextForeground 
*textField20.maxLength: 160
*textField20.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*textField20.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),""); \
}
*textField20.fontList: SmallPropFont 
*textField20.cursorPositionVisible: "false"
*textField20.highlightColor: "Black"
*textField20.ancestorSensitive: "true"

*toggleButton10.class: toggleButton
*toggleButton10.parent: GaussTopform
*toggleButton10.static: true
*toggleButton10.name: toggleButton10
*toggleButton10.x: 83
*toggleButton10.y: 21
*toggleButton10.width: 20
*toggleButton10.height: 30
*toggleButton10.background: LabelBackground 
*toggleButton10.selectColor: SelectColor 
*toggleButton10.highlightOnEnter: "true"
*toggleButton10.translations: HELPGausstab
*toggleButton10.foreground: "Black"
*toggleButton10.highlightColor: "Black"
*toggleButton10.indicatorSize: 13
*toggleButton10.ancestorSensitive: "true"

*toggleButton11.class: toggleButton
*toggleButton11.parent: GaussTopform
*toggleButton11.static: true
*toggleButton11.name: toggleButton11
*toggleButton11.x: 103
*toggleButton11.y: 21
*toggleButton11.width: 20
*toggleButton11.height: 30
*toggleButton11.background: LabelBackground 
*toggleButton11.selectColor: SelectColor 
*toggleButton11.highlightOnEnter: "true"
*toggleButton11.translations: HELPGausstab
*toggleButton11.foreground: "Black"
*toggleButton11.highlightColor: "Black"
*toggleButton11.indicatorSize: 13
*toggleButton11.ancestorSensitive: "true"

*toggleButton12.class: toggleButton
*toggleButton12.parent: GaussTopform
*toggleButton12.static: true
*toggleButton12.name: toggleButton12
*toggleButton12.x: 123
*toggleButton12.y: 21
*toggleButton12.width: 20
*toggleButton12.height: 30
*toggleButton12.background: LabelBackground 
*toggleButton12.selectColor: SelectColor 
*toggleButton12.highlightOnEnter: "true"
*toggleButton12.translations: HELPGausstab
*toggleButton12.foreground: "Black"
*toggleButton12.highlightColor: "Black"
*toggleButton12.indicatorSize: 13
*toggleButton12.ancestorSensitive: "true"

*toggleButton13.class: toggleButton
*toggleButton13.parent: GaussTopform
*toggleButton13.static: true
*toggleButton13.name: toggleButton13
*toggleButton13.x: 83
*toggleButton13.y: 51
*toggleButton13.width: 20
*toggleButton13.height: 30
*toggleButton13.background: LabelBackground 
*toggleButton13.selectColor: SelectColor 
*toggleButton13.highlightOnEnter: "true"
*toggleButton13.translations: HELPGausstab
*toggleButton13.foreground: "Black"
*toggleButton13.highlightColor: "Black"
*toggleButton13.indicatorSize: 13
*toggleButton13.ancestorSensitive: "true"

*toggleButton14.class: toggleButton
*toggleButton14.parent: GaussTopform
*toggleButton14.static: true
*toggleButton14.name: toggleButton14
*toggleButton14.x: 103
*toggleButton14.y: 51
*toggleButton14.width: 20
*toggleButton14.height: 30
*toggleButton14.background: LabelBackground 
*toggleButton14.selectColor: SelectColor 
*toggleButton14.highlightOnEnter: "true"
*toggleButton14.translations: HELPGausstab
*toggleButton14.foreground: "Black"
*toggleButton14.highlightColor: "Black"
*toggleButton14.indicatorSize: 13
*toggleButton14.ancestorSensitive: "true"

*toggleButton15.class: toggleButton
*toggleButton15.parent: GaussTopform
*toggleButton15.static: true
*toggleButton15.name: toggleButton15
*toggleButton15.x: 123
*toggleButton15.y: 51
*toggleButton15.width: 20
*toggleButton15.height: 30
*toggleButton15.background: LabelBackground 
*toggleButton15.selectColor: SelectColor 
*toggleButton15.highlightOnEnter: "true"
*toggleButton15.translations: HELPGausstab
*toggleButton15.foreground: "Black"
*toggleButton15.highlightColor: "Black"
*toggleButton15.indicatorSize: 13
*toggleButton15.ancestorSensitive: "true"

*toggleButton16.class: toggleButton
*toggleButton16.parent: GaussTopform
*toggleButton16.static: true
*toggleButton16.name: toggleButton16
*toggleButton16.x: 83
*toggleButton16.y: 81
*toggleButton16.width: 20
*toggleButton16.height: 30
*toggleButton16.background: LabelBackground 
*toggleButton16.selectColor: SelectColor 
*toggleButton16.highlightOnEnter: "true"
*toggleButton16.translations: HELPGausstab
*toggleButton16.foreground: "Black"
*toggleButton16.highlightColor: "Black"
*toggleButton16.indicatorSize: 13
*toggleButton16.ancestorSensitive: "true"

*toggleButton17.class: toggleButton
*toggleButton17.parent: GaussTopform
*toggleButton17.static: true
*toggleButton17.name: toggleButton17
*toggleButton17.x: 103
*toggleButton17.y: 81
*toggleButton17.width: 20
*toggleButton17.height: 30
*toggleButton17.background: LabelBackground 
*toggleButton17.selectColor: SelectColor 
*toggleButton17.highlightOnEnter: "true"
*toggleButton17.translations: HELPGausstab
*toggleButton17.foreground: "Black"
*toggleButton17.highlightColor: "Black"
*toggleButton17.indicatorSize: 13
*toggleButton17.ancestorSensitive: "true"

*toggleButton18.class: toggleButton
*toggleButton18.parent: GaussTopform
*toggleButton18.static: true
*toggleButton18.name: toggleButton18
*toggleButton18.x: 123
*toggleButton18.y: 81
*toggleButton18.width: 20
*toggleButton18.height: 30
*toggleButton18.background: LabelBackground 
*toggleButton18.selectColor: SelectColor 
*toggleButton18.highlightOnEnter: "true"
*toggleButton18.translations: HELPGausstab
*toggleButton18.foreground: "Black"
*toggleButton18.highlightColor: "Black"
*toggleButton18.indicatorSize: 13
*toggleButton18.ancestorSensitive: "true"

*toggleButton19.class: toggleButton
*toggleButton19.parent: GaussTopform
*toggleButton19.static: true
*toggleButton19.name: toggleButton19
*toggleButton19.x: 83
*toggleButton19.y: 111
*toggleButton19.width: 20
*toggleButton19.height: 30
*toggleButton19.background: LabelBackground 
*toggleButton19.selectColor: SelectColor 
*toggleButton19.highlightOnEnter: "true"
*toggleButton19.translations: HELPGausstab
*toggleButton19.foreground: "Black"
*toggleButton19.highlightColor: "Black"
*toggleButton19.indicatorSize: 13
*toggleButton19.ancestorSensitive: "true"

*toggleButton20.class: toggleButton
*toggleButton20.parent: GaussTopform
*toggleButton20.static: true
*toggleButton20.name: toggleButton20
*toggleButton20.x: 103
*toggleButton20.y: 111
*toggleButton20.width: 20
*toggleButton20.height: 30
*toggleButton20.background: LabelBackground 
*toggleButton20.selectColor: SelectColor 
*toggleButton20.highlightOnEnter: "true"
*toggleButton20.translations: HELPGausstab
*toggleButton20.foreground: "Black"
*toggleButton20.highlightColor: "Black"
*toggleButton20.indicatorSize: 13
*toggleButton20.ancestorSensitive: "true"

*toggleButton21.class: toggleButton
*toggleButton21.parent: GaussTopform
*toggleButton21.static: true
*toggleButton21.name: toggleButton21
*toggleButton21.x: 123
*toggleButton21.y: 111
*toggleButton21.width: 20
*toggleButton21.height: 30
*toggleButton21.background: LabelBackground 
*toggleButton21.selectColor: SelectColor 
*toggleButton21.highlightOnEnter: "true"
*toggleButton21.translations: HELPGausstab
*toggleButton21.foreground: "Black"
*toggleButton21.highlightColor: "Black"
*toggleButton21.indicatorSize: 13
*toggleButton21.ancestorSensitive: "true"

*toggleButton22.class: toggleButton
*toggleButton22.parent: GaussTopform
*toggleButton22.static: true
*toggleButton22.name: toggleButton22
*toggleButton22.x: 83
*toggleButton22.y: 141
*toggleButton22.width: 20
*toggleButton22.height: 30
*toggleButton22.background: LabelBackground 
*toggleButton22.selectColor: SelectColor 
*toggleButton22.highlightOnEnter: "true"
*toggleButton22.translations: HELPGausstab
*toggleButton22.foreground: "Black"
*toggleButton22.highlightColor: "Black"
*toggleButton22.indicatorSize: 13
*toggleButton22.ancestorSensitive: "true"

*toggleButton23.class: toggleButton
*toggleButton23.parent: GaussTopform
*toggleButton23.static: true
*toggleButton23.name: toggleButton23
*toggleButton23.x: 103
*toggleButton23.y: 141
*toggleButton23.width: 20
*toggleButton23.height: 30
*toggleButton23.background: LabelBackground 
*toggleButton23.selectColor: SelectColor 
*toggleButton23.highlightOnEnter: "true"
*toggleButton23.translations: HELPGausstab
*toggleButton23.foreground: "Black"
*toggleButton23.highlightColor: "Black"
*toggleButton23.indicatorSize: 13
*toggleButton23.ancestorSensitive: "true"

*toggleButton24.class: toggleButton
*toggleButton24.parent: GaussTopform
*toggleButton24.static: true
*toggleButton24.name: toggleButton24
*toggleButton24.x: 123
*toggleButton24.y: 141
*toggleButton24.width: 20
*toggleButton24.height: 30
*toggleButton24.background: LabelBackground 
*toggleButton24.selectColor: SelectColor 
*toggleButton24.highlightOnEnter: "true"
*toggleButton24.translations: HELPGausstab
*toggleButton24.foreground: "Black"
*toggleButton24.highlightColor: "Black"
*toggleButton24.indicatorSize: 13
*toggleButton24.ancestorSensitive: "true"

*toggleButton28.class: toggleButton
*toggleButton28.parent: GaussTopform
*toggleButton28.static: true
*toggleButton28.name: toggleButton28
*toggleButton28.x: 83
*toggleButton28.y: 201
*toggleButton28.width: 20
*toggleButton28.height: 30
*toggleButton28.background: LabelBackground 
*toggleButton28.selectColor: SelectColor 
*toggleButton28.highlightOnEnter: "true"
*toggleButton28.translations: HELPGausstab
*toggleButton28.foreground: "Black"
*toggleButton28.highlightColor: "Black"
*toggleButton28.indicatorSize: 13
*toggleButton28.ancestorSensitive: "true"

*toggleButton29.class: toggleButton
*toggleButton29.parent: GaussTopform
*toggleButton29.static: true
*toggleButton29.name: toggleButton29
*toggleButton29.x: 103
*toggleButton29.y: 201
*toggleButton29.width: 20
*toggleButton29.height: 30
*toggleButton29.background: LabelBackground 
*toggleButton29.selectColor: SelectColor 
*toggleButton29.highlightOnEnter: "true"
*toggleButton29.translations: HELPGausstab
*toggleButton29.foreground: "Black"
*toggleButton29.highlightColor: "Black"
*toggleButton29.indicatorSize: 13
*toggleButton29.ancestorSensitive: "true"

*toggleButton30.class: toggleButton
*toggleButton30.parent: GaussTopform
*toggleButton30.static: true
*toggleButton30.name: toggleButton30
*toggleButton30.x: 123
*toggleButton30.y: 201
*toggleButton30.width: 20
*toggleButton30.height: 30
*toggleButton30.background: LabelBackground 
*toggleButton30.selectColor: SelectColor 
*toggleButton30.highlightOnEnter: "true"
*toggleButton30.translations: HELPGausstab
*toggleButton30.foreground: "Black"
*toggleButton30.highlightColor: "Black"
*toggleButton30.indicatorSize: 13
*toggleButton30.ancestorSensitive: "true"

*toggleButton31.class: toggleButton
*toggleButton31.parent: GaussTopform
*toggleButton31.static: true
*toggleButton31.name: toggleButton31
*toggleButton31.x: 83
*toggleButton31.y: 231
*toggleButton31.width: 20
*toggleButton31.height: 30
*toggleButton31.background: LabelBackground 
*toggleButton31.selectColor: SelectColor 
*toggleButton31.highlightOnEnter: "true"
*toggleButton31.translations: HELPGausstab
*toggleButton31.foreground: "Black"
*toggleButton31.highlightColor: "Black"
*toggleButton31.indicatorSize: 13
*toggleButton31.ancestorSensitive: "true"

*toggleButton32.class: toggleButton
*toggleButton32.parent: GaussTopform
*toggleButton32.static: true
*toggleButton32.name: toggleButton32
*toggleButton32.x: 103
*toggleButton32.y: 231
*toggleButton32.width: 20
*toggleButton32.height: 30
*toggleButton32.background: LabelBackground 
*toggleButton32.selectColor: SelectColor 
*toggleButton32.highlightOnEnter: "true"
*toggleButton32.translations: HELPGausstab
*toggleButton32.foreground: "Black"
*toggleButton32.highlightColor: "Black"
*toggleButton32.indicatorSize: 13
*toggleButton32.ancestorSensitive: "true"

*toggleButton33.class: toggleButton
*toggleButton33.parent: GaussTopform
*toggleButton33.static: true
*toggleButton33.name: toggleButton33
*toggleButton33.x: 123
*toggleButton33.y: 231
*toggleButton33.width: 20
*toggleButton33.height: 30
*toggleButton33.background: LabelBackground 
*toggleButton33.selectColor: SelectColor 
*toggleButton33.highlightOnEnter: "true"
*toggleButton33.translations: HELPGausstab
*toggleButton33.foreground: "Black"
*toggleButton33.highlightColor: "Black"
*toggleButton33.indicatorSize: 13
*toggleButton33.ancestorSensitive: "true"

*GaussUpComponent.class: arrowButton
*GaussUpComponent.parent: GaussTopform
*GaussUpComponent.static: true
*GaussUpComponent.name: GaussUpComponent
*GaussUpComponent.x: 145
*GaussUpComponent.y: 302
*GaussUpComponent.width: 30
*GaussUpComponent.height: 20
*GaussUpComponent.background: TextBackground 
*GaussUpComponent.foreground: MenuBackground 
*GaussUpComponent.borderColor: TextBackground 
*GaussUpComponent.activateCallback: {\
char line[20];\
if(gaussNumOfComp<9)\
 gaussNumOfComp++;\
sprintf(line,"%d",gaussNumOfComp);\
UxPutText(Components,line);\
}
*GaussUpComponent.highlightOnEnter: "true"
*GaussUpComponent.translations: HELPGausstab
*GaussUpComponent.highlightColor: "Black"
*GaussUpComponent.ancestorSensitive: "true"

*GaussDownComponent.class: arrowButton
*GaussDownComponent.parent: GaussTopform
*GaussDownComponent.static: true
*GaussDownComponent.name: GaussDownComponent
*GaussDownComponent.x: 145
*GaussDownComponent.y: 318
*GaussDownComponent.width: 30
*GaussDownComponent.height: 20
*GaussDownComponent.background: TextBackground 
*GaussDownComponent.foreground: MenuBackground 
*GaussDownComponent.arrowDirection: "arrow_down"
*GaussDownComponent.borderColor: TextBackground 
*GaussDownComponent.activateCallback: {\
char line[20];\
if(gaussNumOfComp>1)\
gaussNumOfComp--;\
sprintf(line,"%d",gaussNumOfComp);\
UxPutText(Components,line);\
}
*GaussDownComponent.highlightOnEnter: "true"
*GaussDownComponent.translations: HELPGausstab
*GaussDownComponent.highlightColor: "#000000"
*GaussDownComponent.ancestorSensitive: "true"

*label6.class: label
*label6.parent: GaussTopform
*label6.static: true
*label6.name: label6
*label6.x: 276
*label6.y: 312
*label6.width: 90
*label6.height: 22
*label6.background: LabelBackground 
*label6.labelString: "Iterations :"
*label6.fontList: TextFont 
*label6.foreground: TextForeground 
*label6.highlightColor: "Black"
*label6.ancestorSensitive: "true"

*iterations_text.class: text
*iterations_text.parent: GaussTopform
*iterations_text.static: false
*iterations_text.name: iterations_text
*iterations_text.x: 436
*iterations_text.y: 310
*iterations_text.width: 40
*iterations_text.height: 30
*iterations_text.background: TextBackground 
*iterations_text.losingFocusCallback: {\
if( gaussModMaxIterations)\
{ gaussModMaxIterations = FALSE;\
  sscanf(UxGetText(iterations_text),"%d",&gaussMaxIterations);\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*iterations_text.modifyVerifyCallback: {\
gaussModMaxIterations = TRUE;\
}
*iterations_text.foreground: TextForeground 
*iterations_text.highlightOnEnter: "true"
*iterations_text.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),ITERATIONMAX );\
}
*iterations_text.fontList: TextFont 
*iterations_text.translations: TextGaussTab
*iterations_text.highlightColor: "Black"
*iterations_text.maxLength: 5
*iterations_text.ancestorSensitive: "true"
*iterations_text.marginHeight: 3
*iterations_text.marginWidth: 5

*iterations_text1.class: text
*iterations_text1.parent: GaussTopform
*iterations_text1.static: false
*iterations_text1.name: iterations_text1
*iterations_text1.x: 374
*iterations_text1.y: 310
*iterations_text1.width: 40
*iterations_text1.height: 30
*iterations_text1.background: TextBackground 
*iterations_text1.losingFocusCallback: {\
if( gaussModMinIterations)\
{ gaussModMinIterations = FALSE;\
  sscanf(UxGetText(iterations_text1),"%d",&gaussMinIterations);\
}\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*iterations_text1.modifyVerifyCallback: {\
gaussModMinIterations = TRUE;\
}
*iterations_text1.foreground: TextForeground 
*iterations_text1.highlightOnEnter: "true"
*iterations_text1.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),ITERATIONMIN );\
}
*iterations_text1.fontList: TextFont 
*iterations_text1.translations: TextGaussTab
*iterations_text1.highlightColor: "Black"
*iterations_text1.maxLength: 4
*iterations_text1.ancestorSensitive: "true"
*iterations_text1.marginHeight: 3
*iterations_text1.marginWidth: 5

*iterations_text2.class: text
*iterations_text2.parent: GaussTopform
*iterations_text2.static: false
*iterations_text2.name: iterations_text2
*iterations_text2.x: 502
*iterations_text2.y: 310
*iterations_text2.width: 40
*iterations_text2.height: 30
*iterations_text2.background: TextBackground 
*iterations_text2.editable: "false"
*iterations_text2.foreground: TextForeground 
*iterations_text2.focusCallback: {\
#include <alice_help.h>\
UxPutText(UxFindSwidget("help_text_gauss"),OUTFIELD);\
}
*iterations_text2.losingFocusCallback: {\
UxPutText(UxFindSwidget("help_text_gauss"),"");\
}
*iterations_text2.fontList: TextFont 
*iterations_text2.cursorPositionVisible: "false"
*iterations_text2.highlightColor: "Black"
*iterations_text2.maxLength: 6
*iterations_text2.ancestorSensitive: "true"
*iterations_text2.marginHeight: 3
*iterations_text2.marginWidth: 5

*toggleButton25.class: toggleButton
*toggleButton25.parent: GaussTopform
*toggleButton25.static: true
*toggleButton25.name: toggleButton25
*toggleButton25.x: 83
*toggleButton25.y: 171
*toggleButton25.width: 20
*toggleButton25.height: 30
*toggleButton25.background: LabelBackground 
*toggleButton25.selectColor: SelectColor 
*toggleButton25.highlightOnEnter: "true"
*toggleButton25.translations: HELPGausstab
*toggleButton25.foreground: "Black"
*toggleButton25.highlightColor: "Black"
*toggleButton25.indicatorSize: 13
*toggleButton25.ancestorSensitive: "true"

*toggleButton26.class: toggleButton
*toggleButton26.parent: GaussTopform
*toggleButton26.static: true
*toggleButton26.name: toggleButton26
*toggleButton26.x: 103
*toggleButton26.y: 171
*toggleButton26.width: 20
*toggleButton26.height: 30
*toggleButton26.background: LabelBackground 
*toggleButton26.selectColor: SelectColor 
*toggleButton26.highlightOnEnter: "true"
*toggleButton26.translations: HELPGausstab
*toggleButton26.foreground: "Black"
*toggleButton26.highlightColor: "Black"
*toggleButton26.indicatorSize: 13
*toggleButton26.ancestorSensitive: "true"

*toggleButton27.class: toggleButton
*toggleButton27.parent: GaussTopform
*toggleButton27.static: true
*toggleButton27.name: toggleButton27
*toggleButton27.x: 123
*toggleButton27.y: 171
*toggleButton27.width: 20
*toggleButton27.height: 30
*toggleButton27.background: LabelBackground 
*toggleButton27.selectColor: SelectColor 
*toggleButton27.highlightOnEnter: "true"
*toggleButton27.translations: HELPGausstab
*toggleButton27.foreground: "Black"
*toggleButton27.highlightColor: "Black"
*toggleButton27.indicatorSize: 13
*toggleButton27.ancestorSensitive: "true"

*toggleButton34.class: toggleButton
*toggleButton34.parent: GaussTopform
*toggleButton34.static: true
*toggleButton34.name: toggleButton34
*toggleButton34.x: 83
*toggleButton34.y: 263
*toggleButton34.width: 20
*toggleButton34.height: 30
*toggleButton34.background: LabelBackground 
*toggleButton34.selectColor: SelectColor 
*toggleButton34.highlightOnEnter: "true"
*toggleButton34.translations: HELPGausstab
*toggleButton34.foreground: "Black"
*toggleButton34.highlightColor: "Black"
*toggleButton34.indicatorSize: 13
*toggleButton34.ancestorSensitive: "true"

*toggleButton35.class: toggleButton
*toggleButton35.parent: GaussTopform
*toggleButton35.static: true
*toggleButton35.name: toggleButton35
*toggleButton35.x: 103
*toggleButton35.y: 263
*toggleButton35.width: 20
*toggleButton35.height: 30
*toggleButton35.background: LabelBackground 
*toggleButton35.selectColor: SelectColor 
*toggleButton35.highlightOnEnter: "true"
*toggleButton35.translations: HELPGausstab
*toggleButton35.foreground: "Black"
*toggleButton35.highlightColor: "Black"
*toggleButton35.indicatorSize: 13
*toggleButton35.ancestorSensitive: "true"

*toggleButton36.class: toggleButton
*toggleButton36.parent: GaussTopform
*toggleButton36.static: true
*toggleButton36.name: toggleButton36
*toggleButton36.x: 123
*toggleButton36.y: 263
*toggleButton36.width: 20
*toggleButton36.height: 30
*toggleButton36.background: LabelBackground 
*toggleButton36.selectColor: SelectColor 
*toggleButton36.highlightOnEnter: "true"
*toggleButton36.translations: HELPGausstab
*toggleButton36.foreground: "Black"
*toggleButton36.highlightColor: "Black"
*toggleButton36.indicatorSize: 13
*toggleButton36.ancestorSensitive: "true"

*help_text_gauss.class: text
*help_text_gauss.parent: GaussTopform
*help_text_gauss.static: true
*help_text_gauss.name: help_text_gauss
*help_text_gauss.x: 4
*help_text_gauss.y: 340
*help_text_gauss.width: 578
*help_text_gauss.height: 46
*help_text_gauss.background: SHelpBackground 
*help_text_gauss.editable: "false"
*help_text_gauss.fontList: TextFont 
*help_text_gauss.cursorPositionVisible: "false"
*help_text_gauss.ancestorSensitive: "true"
*help_text_gauss.marginHeight: 3
*help_text_gauss.marginWidth: 3
*help_text_gauss.foreground: TextForeground  

*GaussForm2.class: form
*GaussForm2.parent: GaussTopform
*GaussForm2.static: false
*GaussForm2.name: GaussForm2
*GaussForm2.resizePolicy: "resize_none"
*GaussForm2.x: 0
*GaussForm2.y: 388
*GaussForm2.width: 586
*GaussForm2.height: 36
*GaussForm2.background: ButtonBackground 
*GaussForm2.ancestorSensitive: "true"

*ExecuteGauss.class: pushButton
*ExecuteGauss.parent: GaussForm2
*ExecuteGauss.static: true
*ExecuteGauss.name: ExecuteGauss
*ExecuteGauss.x: 256
*ExecuteGauss.y: 2
*ExecuteGauss.width: 90
*ExecuteGauss.height: 30
*ExecuteGauss.labelString: "Apply"
*ExecuteGauss.fontList: BoldTextFont  
*ExecuteGauss.foreground: ButtonForeground 
*ExecuteGauss.background: ButtonBackground 
*ExecuteGauss.activateCallback: {\
extern mgauss();\
get_fix_opt();\
cursor();\
read_init_guess();\
if(!specAbortCursor)\
{\
 gaussNumOfSol = gaussNumOfComp;\
/* SCTPUT(".... Computing Solution"); */\
 ChangeCurs();\
 mgauss();\
 out_fit_values();\
 draw_gauss();\
 print_statistics();\
}\
}
*ExecuteGauss.highlightOnEnter: "true"
*ExecuteGauss.translations: HelpGaussTable
*ExecuteGauss.highlightColor: "Black"
*ExecuteGauss.ancestorSensitive: "true"
*ExecuteGauss.sensitive: "true"

*ClearGauss.class: pushButton
*ClearGauss.parent: GaussForm2
*ClearGauss.static: true
*ClearGauss.name: ClearGauss
*ClearGauss.x: 34
*ClearGauss.y: 2
*ClearGauss.width: 90
*ClearGauss.height: 30
*ClearGauss.labelString: "Clear"
*ClearGauss.fontList: BoldTextFont 
*ClearGauss.foreground: ApplyForeground 
*ClearGauss.background: ButtonBackground 
*ClearGauss.activateCallback: {\
clear_values();\
\
}
*ClearGauss.highlightOnEnter: "true"
*ClearGauss.translations: HelpGaussTable
*ClearGauss.highlightColor: "Black"
*ClearGauss.ancestorSensitive: "true"
*ClearGauss.sensitive: "true"

*CopyGauss.class: pushButton
*CopyGauss.parent: GaussForm2
*CopyGauss.static: true
*CopyGauss.name: CopyGauss
*CopyGauss.x: 146
*CopyGauss.y: 2
*CopyGauss.width: 90
*CopyGauss.height: 30
*CopyGauss.labelString: "Copy"
*CopyGauss.fontList: BoldTextFont  
*CopyGauss.foreground: ButtonForeground 
*CopyGauss.background: ButtonBackground 
*CopyGauss.activateCallback: {\
copy_fit_values(); \
}
*CopyGauss.highlightOnEnter: "true"
*CopyGauss.translations: HelpGaussTable
*CopyGauss.highlightColor: "Black"
*CopyGauss.ancestorSensitive: "true"
*CopyGauss.sensitive: "true"

*separator7.class: separator
*separator7.parent: GaussTopform
*separator7.static: true
*separator7.name: separator7
*separator7.x: -2
*separator7.y: 382
*separator7.width: 590
*separator7.height: 6
*separator7.background: ButtonBackground 

*label7.class: label
*label7.parent: GaussTopform
*label7.static: true
*label7.name: label7
*label7.x: 374
*label7.y: 292
*label7.width: 46
*label7.height: 20
*label7.background: LabelBackground 
*label7.labelString: "Min"
*label7.foreground: TextForeground 
*label7.fontList: TextFont 
*label7.highlightColor: "Black"
*label7.ancestorSensitive: "true"

*label8.class: label
*label8.parent: GaussTopform
*label8.static: true
*label8.name: label8
*label8.x: 432
*label8.y: 292
*label8.width: 46
*label8.height: 20
*label8.background: LabelBackground 
*label8.labelString: "Max"
*label8.foreground: TextForeground 
*label8.fontList: TextFont 
*label8.highlightColor: "Black"
*label8.ancestorSensitive: "true"

*label9.class: label
*label9.parent: GaussTopform
*label9.static: true
*label9.name: label9
*label9.x: 496
*label9.y: 292
*label9.width: 54
*label9.height: 20
*label9.background: LabelBackground 
*label9.labelString: "Current"
*label9.foreground: TextForeground 
*label9.fontList: TextFont 
*label9.highlightColor: "Black"
*label9.ancestorSensitive: "true"

