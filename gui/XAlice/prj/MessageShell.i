! UIMX ascii 2.0 key: 8649                                                      

*MessageShell.class: topLevelShell
*MessageShell.parent: NO_PARENT
*MessageShell.static: false
*MessageShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\

*MessageShell.ispecdecl:
*MessageShell.funcdecl: swidget create_MessageShell()\

*MessageShell.funcname: create_MessageShell
*MessageShell.funcdef: "swidget", "<create_MessageShell>(%)"
*MessageShell.icode:
*MessageShell.fcode: return(rtrn);\

*MessageShell.auxdecl:
*MessageShell.name: MessageShell
*MessageShell.x: 546
*MessageShell.y: 481
*MessageShell.width: 346
*MessageShell.height: 115
*MessageShell.title: "Message Window"
*MessageShell.background: ButtonBackground 

*MessageForm.class: form
*MessageForm.parent: MessageShell
*MessageForm.static: false
*MessageForm.name: MessageForm
*MessageForm.resizePolicy: "resize_none"
*MessageForm.unitType: "pixels"
*MessageForm.x: 10
*MessageForm.y: 10
*MessageForm.width: 438
*MessageForm.height: 340
*MessageForm.background: ButtonBackground 

*OkMessage.class: pushButton
*OkMessage.parent: MessageForm
*OkMessage.static: false
*OkMessage.name: OkMessage
*OkMessage.x: 123
*OkMessage.y: 69
*OkMessage.width: 90
*OkMessage.height: 30
*OkMessage.background: ButtonBackground 
*OkMessage.fontList: BoldTextFont 
*OkMessage.labelString: "Ok"
*OkMessage.activateCallback: {\
extern swidget MessageShell;\
UxPopdownInterface(MessageShell);\
}
*OkMessage.foreground: ButtonForeground 
*OkMessage.highlightOnEnter: "true"
*OkMessage.highlightColor: "Black"

*MessageDeparator.class: separator
*MessageDeparator.parent: MessageForm
*MessageDeparator.static: true
*MessageDeparator.name: MessageDeparator
*MessageDeparator.x: 0
*MessageDeparator.y: 56
*MessageDeparator.width: 386
*MessageDeparator.height: 10
*MessageDeparator.background: ButtonBackground 

*MessageLabel.class: label
*MessageLabel.parent: MessageForm
*MessageLabel.static: true
*MessageLabel.name: MessageLabel
*MessageLabel.x: 66
*MessageLabel.y: 22
*MessageLabel.width: 231
*MessageLabel.height: 24
*MessageLabel.background: ButtonBackground 
*MessageLabel.fontList: BoldTextFont 
*MessageLabel.alignment: "alignment_center"
*MessageLabel.labelString: "message"
*MessageLabel.recomputeSize: "false"

