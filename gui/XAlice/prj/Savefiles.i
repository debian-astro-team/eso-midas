! UIMX ascii 2.0 key: 2196                                                      

*translation.table: SaveTab
*translation.parent: Savefiles
*translation.policy: override
*translation.<Key>Delete: delete-previous-character()
*translation.<Key>BackSpace: delete-previous-character()
*translation.<Key>osfBackSpace: delete-previous-character()
*translation.<Key>osfDelete: delete-previous-character()

*Savefiles.class: topLevelShell
*Savefiles.parent: NO_PARENT
*Savefiles.static: true
*Savefiles.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\

*Savefiles.ispecdecl:
*Savefiles.funcdecl: swidget create_Savefiles()\

*Savefiles.funcname: create_Savefiles
*Savefiles.funcdef: "swidget", "<create_Savefiles>(%)"
*Savefiles.icode:
*Savefiles.fcode: return(rtrn);\

*Savefiles.auxdecl:
*Savefiles.name: Savefiles
*Savefiles.x: 505
*Savefiles.y: 320
*Savefiles.width: 369
*Savefiles.height: 210
*Savefiles.title: "Save TMP files"
*Savefiles.keyboardFocusPolicy: "pointer"

*form12.class: form
*form12.parent: Savefiles
*form12.static: true
*form12.name: form12
*form12.resizePolicy: "resize_none"
*form12.unitType: "pixels"
*form12.x: 66
*form12.y: 22
*form12.width: 369
*form12.height: 222
*form12.background: ButtonBackground

*nameprg.class: text
*nameprg.parent: form12
*nameprg.static: true
*nameprg.name: nameprg
*nameprg.x: 172
*nameprg.y: 32
*nameprg.width: 170
*nameprg.height: 30
*nameprg.background: TextBackground 
*nameprg.highlightOnEnter: "true"
*nameprg.fontList: TextFont 
*nameprg.translations: SaveTab
*nameprg.foreground: TextForeground 
*nameprg.highlightColor: "Black"
*nameprg.marginHeight: 2

*nametbl.class: text
*nametbl.parent: form12
*nametbl.static: true
*nametbl.name: nametbl
*nametbl.x: 172
*nametbl.y: 64
*nametbl.width: 170
*nametbl.height: 30
*nametbl.background: TextBackground 
*nametbl.highlightOnEnter: "true"
*nametbl.fontList: TextFont 
*nametbl.translations: SaveTab
*nametbl.foreground: TextForeground 
*nametbl.highlightColor: "Black"
*nametbl.marginHeight: 2

*namestat.class: text
*namestat.parent: form12
*namestat.static: true
*namestat.name: namestat
*namestat.x: 172
*namestat.y: 96
*namestat.width: 170
*namestat.height: 30
*namestat.background: TextBackground 
*namestat.highlightOnEnter: "true"
*namestat.fontList: TextFont 
*namestat.translations: SaveTab
*namestat.foreground: TextForeground 
*namestat.highlightColor: "Black"
*namestat.marginHeight: 2

*label17.class: label
*label17.parent: form12
*label17.static: true
*label17.name: label17
*label17.x: 26
*label17.y: 32
*label17.width: 138
*label17.height: 30
*label17.background: ButtonBackground
*label17.labelString: "TMPalice.prg"
*label17.fontList: TextFont 
*label17.foreground: TextForeground 
*label17.highlightColor: "Black"

*label18.class: label
*label18.parent: form12
*label18.static: true
*label18.name: label18
*label18.x: 26
*label18.y: 64
*label18.width: 138
*label18.height: 30
*label18.background: ButtonBackground
*label18.labelString: "TMPalice.tbl"
*label18.fontList: TextFont 
*label18.foreground: TextForeground 
*label18.highlightColor: "Black"

*label19.class: label
*label19.parent: form12
*label19.static: true
*label19.name: label19
*label19.x: 25
*label19.y: 96
*label19.width: 138
*label19.height: 30
*label19.background: ButtonBackground
*label19.labelString: "TMPalice.stat"
*label19.fontList: TextFont 
*label19.foreground: TextForeground 
*label19.highlightColor: "Black"

*label20.class: label
*label20.parent: form12
*label20.static: true
*label20.name: label20
*label20.x: 26
*label20.y: 4
*label20.width: 142
*label20.height: 20
*label20.background: ButtonBackground
*label20.fontList: TextFont 
*label20.labelString: "TMP file"
*label20.foreground: TextForeground 
*label20.highlightColor: "Black"

*label21.class: label
*label21.parent: form12
*label21.static: true
*label21.name: label21
*label21.x: 180
*label21.y: 4
*label21.width: 134
*label21.height: 20
*label21.background: ButtonBackground
*label21.fontList: TextFont 
*label21.labelString: "New name"
*label21.foreground: TextForeground 
*label21.highlightColor: "Black"

*OkSaveFiles.class: pushButton
*OkSaveFiles.parent: form12
*OkSaveFiles.static: false
*OkSaveFiles.name: OkSaveFiles
*OkSaveFiles.x: 27
*OkSaveFiles.y: 168
*OkSaveFiles.width: 90
*OkSaveFiles.height: 30
*OkSaveFiles.background: ButtonBackground
*OkSaveFiles.fontList: BoldTextFont 
*OkSaveFiles.foreground: ButtonForeground 
*OkSaveFiles.labelString: "Ok"
*OkSaveFiles.activateCallback: {\
save_TMP();\
UxPopdownInterface(Savefiles);\
}
*OkSaveFiles.highlightOnEnter: "true"
*OkSaveFiles.highlightColor: "Black"

*CancelSaveFiles.class: pushButton
*CancelSaveFiles.parent: form12
*CancelSaveFiles.static: false
*CancelSaveFiles.name: CancelSaveFiles
*CancelSaveFiles.x: 135
*CancelSaveFiles.y: 168
*CancelSaveFiles.width: 90
*CancelSaveFiles.height: 30
*CancelSaveFiles.background: ButtonBackground
*CancelSaveFiles.fontList: BoldTextFont 
*CancelSaveFiles.foreground: CancelForeground 
*CancelSaveFiles.labelString: "Cancel"
*CancelSaveFiles.activateCallback: {\
UxPopdownInterface(Savefiles);\
}
*CancelSaveFiles.highlightOnEnter: "true"
*CancelSaveFiles.highlightColor: "Black"

*separator5.class: separator
*separator5.parent: form12
*separator5.static: true
*separator5.name: separator5
*separator5.x: 30
*separator5.y: 22
*separator5.width: 136
*separator5.height: 8
*separator5.background: ButtonBackground

*separator6.class: separator
*separator6.parent: form12
*separator6.static: true
*separator6.name: separator6
*separator6.x: 172
*separator6.y: 22
*separator6.width: 172
*separator6.height: 8
*separator6.background: ButtonBackground

*label22.class: label
*label22.parent: form12
*label22.static: true
*label22.name: label22
*label22.x: 25
*label22.y: 132
*label22.width: 138
*label22.height: 30
*label22.background: ButtonBackground
*label22.labelString: "TMPcont.bdf"
*label22.fontList: TextFont 
*label22.foreground: TextForeground 
*label22.highlightColor: "Black"

*namecont.class: text
*namecont.parent: form12
*namecont.static: true
*namecont.name: namecont
*namecont.x: 172
*namecont.y: 132
*namecont.width: 170
*namecont.height: 30
*namecont.background: TextBackground 
*namecont.highlightOnEnter: "true"
*namecont.fontList: TextFont 
*namecont.translations: SaveTab
*namecont.foreground: TextForeground 
*namecont.highlightColor: "Black"
*namecont.marginHeight: 2

