/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        alloc.c
.MODULE       subroutines
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Routines for memory [de]allocation
.KEYWORDS     Memory management.
.VERSION 1.0  1-Feb-1991   Implementation

 090819		last modif
------------------------------------------------------------*/

#include <midas_def.h>
#include <stdio.h>

int **imatrix( nrl, nrh, ncl, nch ) 
int nrl, nrh, ncl, nch;
/* Allocates a int matrix with range [nrl..nrh][ncl..nch] */
{
    int i;
    int **m;

    /* Allocate pointers to rows */
    m = (int **)osmmget( (unsigned) (nrh-nrl+1) * sizeof(int *) );
    m -= nrl;

    /* Allocate rows and set pointers to them */
    for ( i = nrl; i <= nrh; i++ ) {
	m[i] = (int *)osmmget( (unsigned) (nch-ncl+1) * sizeof(int) );
        m[i] -= ncl;
    }
return 0;
}

int *ivector( nl, nh ) 
int nl, nh;
/* Allocates a int vector with range [nl..nh] */
{
    int *v;

    v = (int *)osmmget( (unsigned) (nh-nl+1) * sizeof(int) );
    return( v - nl );
}

void free_imatrix( m, nrl, nrh, ncl, nch ) 
int **m;
int nrl, nrh, ncl, nch;
/* Frees a matrix allocated with imatrix() */
{
    int i;

    for ( i = nrh; i >= nrl; i-- )
	osmmfree( (char *) (m[i] + ncl) );

    osmmfree( (char *) (m + nrl) );
}

void free_ivector( v, nl, nh ) 
int *v;
int nl, nh;
/* Frees a vector allocated with ivector() */
{
    osmmfree( (char *) (v + nl) );
}

double **dmatrix( nrl, nrh, ncl, nch ) 
int nrl, nrh, ncl, nch;
/* Allocates a double matrix with range [nrl..nrh][ncl..nch] */
{
    int i;
    double **m;

    /* Allocate pointers to rows */
    m = (double **)osmmget( (unsigned) (nrh-nrl+1) * sizeof(double *) );
    m -= nrl;

    /* Allocate rows and set pointers to them */
    for ( i = nrl; i <= nrh; i++ ) {
	m[i] = (double *)osmmget( (unsigned) (nch-ncl+1) * sizeof(double) );
        m[i] -= ncl;
    }
    return( m );
}

double *dvector( nl, nh ) 
int nl, nh;
/* Allocates a double vector with range [nl..nh] */
{
    double *v;

    v = (double *)osmmget( (unsigned) (nh-nl+1) * sizeof(double) );
    return( v - nl );
}

void free_dmatrix( m, nrl, nrh, ncl, nch ) 
double **m;
int nrl, nrh, ncl, nch;
/* Frees a matrix allocated with dmatrix() */
{
    int i;

    for ( i = nrh; i >= nrl; i-- )
	osmmfree( (char *) (m[i] + ncl) );

    osmmfree( (char *) (m + nrl) );
}

void free_dvector( v, nl, nh ) 
double *v;
int nl, nh;
/* Frees a vector allocated with dvector() */
{
    osmmfree( (char *) (v + nl) );
}

float **fmatrix( nrl, nrh, ncl, nch ) 
int nrl, nrh, ncl, nch;
/* Allocates a float matrix with range [nrl..nrh][ncl..nch] */
{
    int i;
    float **m;

    /* Allocate pointers to rows */
    m = (float **)osmmget( (unsigned) (nrh-nrl+1) * sizeof(float *) );
    m -= nrl;

    /* Allocate rows and set pointers to them */
    for ( i = nrl; i <= nrh; i++ ) {
	m[i] = (float *)osmmget( (unsigned) (nch-ncl+1) * sizeof(float) );
        m[i] -= ncl;
    }
    return( m );
}

float *fvector( nl, nh ) 
int nl, nh;
/* Allocates a float vector with range [nl..nh] */
{
    float *v;

    v = (float *)osmmget( (unsigned) (nh-nl+1) * sizeof(float) );
    return( v - nl );
}

void free_fmatrix( m, nrl, nrh, ncl, nch ) 
float **m;
int nrl, nrh, ncl, nch;
/* Frees a matrix allocated with fmatrix() */
{
    int i;

    for ( i = nrh; i >= nrl; i-- )
	osmmfree( (char *) (m[i] + ncl) );

    osmmfree( (char *) (m + nrl) );
}

void free_fvector( v, nl, nh ) 
float *v;
int nl, nh;
/* Frees a vector allocated with fvector() */
{
    osmmfree( (char *) (v + nl) );
}

char **cmatrix( nrl, nrh, ncl, nch ) 
int nrl, nrh, ncl, nch;
/* Allocates a char matrix with range [nrl..nrh][ncl..nch] */
{
    int i;
    char **m;

    /* Allocate pointers to rows */
    m = (char **)osmmget( (unsigned) (nrh-nrl+1) * sizeof(char *) );
    m -= nrl;

    /* Allocate rows and set pointers to them */
    for ( i = nrl; i <= nrh; i++ ) {
	m[i] = (char *)osmmget( (unsigned) (nch-ncl+1) * sizeof(char) );
        m[i] -= ncl;
    }
    return( m );
}

char *cvector( nl, nh ) 
int nl, nh;
/* Allocates a char vector with range [nl..nh] */
{
    char *v;

    v = (char *)osmmget( (unsigned) (nh-nl+1) * sizeof(char) );
    return( v - nl );
}

void free_cmatrix( m, nrl, nrh, ncl, nch ) 
char **m;
int nrl, nrh, ncl, nch;
/* Frees a matrix allocated with cmatrix() */
{
    int i;

    for ( i = nrh; i >= nrl; i-- )
	osmmfree( (char *) (m[i] + ncl) );

    osmmfree( (char *) (m + nrl) );
}

void free_cvector( v, nl, nh ) 
char *v;
int nl, nh;
/* Frees a vector allocated with cvector() */
{
    osmmfree( (char *) (v + nl) );
}
