/* @(#)HelpShell.c	19.1 (ES0-DMD) 02/25/03 13:47:23 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	HelpShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxRowCol.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxHelpShell;
	swidget	Uxform21;
	swidget	UxscrolledWindow9;
	swidget	UxrowColumn8;
	swidget	Uxtx_extended_help;
	swidget	Uxhelp_print;
	swidget	Uxpb_help_return;
} _UxCHelpShell;

#define HelpShell               UxHelpShellContext->UxHelpShell
#define form21                  UxHelpShellContext->Uxform21
#define scrolledWindow9         UxHelpShellContext->UxscrolledWindow9
#define rowColumn8              UxHelpShellContext->UxrowColumn8
#define tx_extended_help        UxHelpShellContext->Uxtx_extended_help
#define help_print              UxHelpShellContext->Uxhelp_print
#define pb_help_return          UxHelpShellContext->Uxpb_help_return

static _UxCHelpShell	*UxHelpShellContext;


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_HelpShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_help_return( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCHelpShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxHelpShellContext;
	UxHelpShellContext = UxContext =
			(_UxCHelpShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("HelpShell"));
	 
	
	}
	UxHelpShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_HelpShell()
{
	UxPutGeometry( HelpShell, "-0+0" );
	UxPutBackground( HelpShell, ButtonBackground );
	UxPutIconName( HelpShell, "Extended help" );
	UxPutKeyboardFocusPolicy( HelpShell, "pointer" );
	UxPutTitle( HelpShell, "Extended help" );
	UxPutHeight( HelpShell, 415 );
	UxPutWidth( HelpShell, 700 );
	UxPutY( HelpShell, 70 );
	UxPutX( HelpShell, 20 );

	UxPutBackground( form21, ButtonBackground );
	UxPutHeight( form21, 284 );
	UxPutWidth( form21, 664 );
	UxPutY( form21, 0 );
	UxPutX( form21, 0 );
	UxPutUnitType( form21, "pixels" );
	UxPutResizePolicy( form21, "resize_none" );

	UxPutBackground( scrolledWindow9, WindowBackground );
	UxPutScrollBarPlacement( scrolledWindow9, "bottom_left" );
	UxPutHeight( scrolledWindow9, 372 );
	UxPutWidth( scrolledWindow9, 710 );
	UxPutY( scrolledWindow9, 2 );
	UxPutX( scrolledWindow9, 0 );
	UxPutScrollingPolicy( scrolledWindow9, "automatic" );

	UxPutEntryAlignment( rowColumn8, "alignment_beginning" );
	UxPutBackground( rowColumn8, "white" );
	UxPutOrientation( rowColumn8, "horizontal" );
	UxPutHeight( rowColumn8, 427 );
	UxPutWidth( rowColumn8, 418 );
	UxPutY( rowColumn8, 4 );
	UxPutX( rowColumn8, 25 );

	UxPutHighlightColor( tx_extended_help, "Black" );
	UxPutForeground( tx_extended_help, TextForeground );
	UxPutCursorPositionVisible( tx_extended_help, "false" );
	UxPutEditMode( tx_extended_help, "multi_line_edit" );
	UxPutFontList( tx_extended_help, SmallFont );
	UxPutBackground( tx_extended_help, TextBackground );
	UxPutHeight( tx_extended_help, 797 );
	UxPutWidth( tx_extended_help, 673 );
	UxPutY( tx_extended_help, 3 );
	UxPutX( tx_extended_help, 3 );

	UxPutForeground( help_print, ButtonForeground );
	UxPutHighlightOnEnter( help_print, "true" );
	UxPutHighlightColor( help_print, "Black" );
	UxPutFontList( help_print, BoldTextFont );
	UxPutLabelString( help_print, "Print" );
	UxPutBackground( help_print, ButtonBackground );
	UxPutHeight( help_print, 32 );
	UxPutWidth( help_print, 82 );
	UxPutY( help_print, 378 );
	UxPutX( help_print, 516 );

	UxPutForeground( pb_help_return, CancelForeground );
	UxPutHighlightOnEnter( pb_help_return, "true" );
	UxPutHighlightColor( pb_help_return, "red" );
	UxPutLabelString( pb_help_return, "Quit" );
	UxPutFontList( pb_help_return, BoldTextFont );
	UxPutBackground( pb_help_return, ButtonBackground );
	UxPutHeight( pb_help_return, 32 );
	UxPutWidth( pb_help_return, 82 );
	UxPutY( pb_help_return, 378 );
	UxPutX( pb_help_return, 604 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_HelpShell()
{
	/* Create the swidgets */

	HelpShell = UxCreateTopLevelShell( "HelpShell", NO_PARENT );
	UxPutContext( HelpShell, UxHelpShellContext );

	form21 = UxCreateForm( "form21", HelpShell );
	scrolledWindow9 = UxCreateScrolledWindow( "scrolledWindow9", form21 );
	rowColumn8 = UxCreateRowColumn( "rowColumn8", scrolledWindow9 );
	tx_extended_help = UxCreateText( "tx_extended_help", rowColumn8 );
	help_print = UxCreatePushButton( "help_print", form21 );
	pb_help_return = UxCreatePushButton( "pb_help_return", form21 );

	_Uxinit_HelpShell();

	/* Create the X widgets */

	UxCreateWidget( HelpShell );
	UxCreateWidget( form21 );
	UxCreateWidget( scrolledWindow9 );
	UxCreateWidget( rowColumn8 );
	UxCreateWidget( tx_extended_help );
	UxCreateWidget( help_print );
	UxCreateWidget( pb_help_return );

	UxAddCallback( pb_help_return, XmNactivateCallback,
			activateCB_pb_help_return,
			(XtPointer) UxHelpShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( HelpShell );

	return ( HelpShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_HelpShell()
{
	swidget                 rtrn;
	_UxCHelpShell           *UxContext;

	UxHelpShellContext = UxContext =
		(_UxCHelpShell *) UxMalloc( sizeof(_UxCHelpShell) );

	rtrn = _Uxbuild_HelpShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_HelpShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_HelpShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

