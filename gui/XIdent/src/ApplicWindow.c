/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* .VERSION

    090819	last modif
*/


/*******************************************************************************
	ApplicWindow.c

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "UxLib.h"
#include "UxLabel.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxSep.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

#include <spec_comm.h>


/* parameters for identification */
#define IDBEGIN         0
#define IDCONT          1

/* label or no label the spectrum representing the starting line */
#define LABEL_SPEC      0
#define NO_LABEL        1


void ident_line(), delete_idents();
void show_current_idents(), redraw_plot();

void read_params_long();
int  read_lincat_table(), read_line_x();
int  SCTPUT();
int plot_spec();
void display_lincat_table(), get_wlc_value();
void cutx_spectrum(), cuty_spectrum(), shift_spectrum();
void unzoom_spectrum(), getcur_spectrum(), end_gmidas();
void AppendDialogText(), DisplayShortHelp(), DisplayExtendedHelp();


/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxApplicWindow;
	swidget	UxMainWindow;
	swidget	Uxmenu3;
	swidget	Uxmenu3_p2;
	swidget	Uxmenu3_p2_b1;
	swidget	Uxmenu3_p2_b2;
	swidget	Uxmenu3_p2_b3;
	swidget	Uxmenu3_p2_b4;
	swidget	Uxmenu3_p2_b5;
	swidget	Uxmenu3_p2_b7;
	swidget	Uxmenu3_p2_b8;
	swidget	Uxmenu3_top_b2;
	swidget	Uxmenu3_p3;
	swidget	Uxmenu3_p3_b1;
	swidget	Uxmenu3_p3_b2;
	swidget	Uxmenu3_p3_b3;
	swidget	Uxmenu3_top_b3;
	swidget	Uxmenu3_p4;
	swidget	Uxmenu_help_window;
	swidget	Uxmenu3_top_b4;
	swidget	Uxform3;
	swidget	Uxform2;
	swidget	Uxpb_begin;
	swidget	Uxpb_continue;
	swidget	Uxpb_delete;
	swidget	Uxseparator1;
	swidget	UxSHelp;
	swidget	UxscrolledWindow1;
	swidget	Uxls_lincat;
	swidget	Uxlabel1;
} _UxCApplicWindow;

#define ApplicWindow            UxApplicWindowContext->UxApplicWindow
#define MainWindow              UxApplicWindowContext->UxMainWindow
#define menu3                   UxApplicWindowContext->Uxmenu3
#define menu3_p2                UxApplicWindowContext->Uxmenu3_p2
#define menu3_p2_b1             UxApplicWindowContext->Uxmenu3_p2_b1
#define menu3_p2_b2             UxApplicWindowContext->Uxmenu3_p2_b2
#define menu3_p2_b3             UxApplicWindowContext->Uxmenu3_p2_b3
#define menu3_p2_b4             UxApplicWindowContext->Uxmenu3_p2_b4
#define menu3_p2_b5             UxApplicWindowContext->Uxmenu3_p2_b5
#define menu3_p2_b7             UxApplicWindowContext->Uxmenu3_p2_b7
#define menu3_p2_b8             UxApplicWindowContext->Uxmenu3_p2_b8
#define menu3_top_b2            UxApplicWindowContext->Uxmenu3_top_b2
#define menu3_p3                UxApplicWindowContext->Uxmenu3_p3
#define menu3_p3_b1             UxApplicWindowContext->Uxmenu3_p3_b1
#define menu3_p3_b2             UxApplicWindowContext->Uxmenu3_p3_b2
#define menu3_p3_b3             UxApplicWindowContext->Uxmenu3_p3_b3
#define menu3_top_b3            UxApplicWindowContext->Uxmenu3_top_b3
#define menu3_p4                UxApplicWindowContext->Uxmenu3_p4
#define menu_help_window        UxApplicWindowContext->Uxmenu_help_window
#define menu3_top_b4            UxApplicWindowContext->Uxmenu3_top_b4
#define form3                   UxApplicWindowContext->Uxform3
#define form2                   UxApplicWindowContext->Uxform2
#define pb_begin                UxApplicWindowContext->Uxpb_begin
#define pb_continue             UxApplicWindowContext->Uxpb_continue
#define pb_delete               UxApplicWindowContext->Uxpb_delete
#define separator1              UxApplicWindowContext->Uxseparator1
#define SHelp                   UxApplicWindowContext->UxSHelp
#define scrolledWindow1         UxApplicWindowContext->UxscrolledWindow1
#define ls_lincat               UxApplicWindowContext->Uxls_lincat
#define label1                  UxApplicWindowContext->Uxlabel1

static _UxCApplicWindow	*UxApplicWindowContext;


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ApplicWindow();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	UxPutText(SHelp, "");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayShortHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_menu3_p2_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	cutx_spectrum();
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu3_p2_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	cuty_spectrum();
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu3_p2_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	shift_spectrum();
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu3_p2_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	unzoom_spectrum();
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu3_p2_b5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	getcur_spectrum();
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu3_p2_b8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	AppendDialogText("select/tab 'LINTAB' all");
	end_gmidas();
	exit(0);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu3_p3_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("graph/long");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu3_p3_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("copy/graphic laser");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu3_p3_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("reset/display");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_window( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_begin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{

	
	extern int NumLincatPos;   /* cardinality of LincatPos[] */
	extern int NumWlcPos;      /* cardinality of WlcPos[] */
	extern int IdentBegin;
	
	NumLincatPos = NumWlcPos = 0;
	
	AppendDialogText(C_SELINE);
	read_params_long();
	if ( ! read_lincat_table() )
	    return;
	
	if ( !read_line_x(IDBEGIN) ) {  /* :X of line table */
	    SCTPUT( "*** Lines have not been searched ***" );
	    return;
	}
	
	if ( !plot_spec(LABEL_SPEC) ) /* plot 'WLC(Ystart)' */
	     return;
	IdentBegin = TRUE;
	
	get_wlc_value();
	
	display_lincat_table(UxGetWidget(UxFindSwidget("ls_lincat")));
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_continue( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	extern int NumWlcPos, NumLincatPos;
	extern int IdentBegin;
	
	NumWlcPos = NumLincatPos = 0;
	
	AppendDialogText(C_SELINE);
	read_params_long();
	if ( ! read_lincat_table() )
	    return;
	
	if ( !read_line_x(IDCONT) ) {   /* :X of line table */
	    SCTPUT( "*** Lines have not been searched ***" );
	    return;
	}
	
	if ( !IdentBegin ) {
	    if ( !plot_spec(LABEL_SPEC) )   /* plot 'WLC(Ystart)' */
	        return;
	}
	else 
	    redraw_plot();
	
	show_current_idents();
	get_wlc_value();
	
	display_lincat_table(UxGetWidget(UxFindSwidget("ls_lincat")));
	
	IdentBegin = TRUE;
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_delete( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	extern int NumWlcPos, NumLincatPos;
	extern int IdentBegin;
	
	NumWlcPos = NumLincatPos = 0;
	
	AppendDialogText(C_SELINE);
	read_params_long();
	if ( ! read_lincat_table() )
	    return;
	
	if ( !read_line_x(IDCONT) ) {   /* :X of line table */
	    SCTPUT( "*** Lines have not been searched ***" );
	    return;
	}
	
	if ( !IdentBegin ) {
	    if ( !plot_spec(LABEL_SPEC) )   /* plot 'WLC(Ystart)' */
	        return;
	}
	else 
	    redraw_plot();
	
	show_current_idents();
	delete_idents();
	
	IdentBegin = TRUE;
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	browseSelectionCB_ls_lincat( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *choice;
	int i;
	float item_value;
	XmListCallbackStruct *cbs;
	
	extern int NumLincatPos, NumWlcPos;
	extern float LincatPos[];
	
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	
	if ( NumLincatPos == NumWlcPos ) /* Noclick in graphic window */
	    return;
	
	XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);
	
	sscanf( choice, "%f", &item_value );
	
	for ( i = NumLincatPos-1; i >= 0; i-- )
	    if ( item_value == LincatPos[i] ) /* value already selected */
	        return;
	
	LincatPos[NumLincatPos] = item_value;
	NumLincatPos++;
	
	
	ident_line();       /* Ident wavelength in line table */
	get_wlc_value();    /* let's go to graphic window again */
	
	XtFree(choice);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_ls_lincat( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	display_lincat_table(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ApplicWindow()
{
	UxPutBackground( ApplicWindow, ApplicBackground );
	UxPutTitle( ApplicWindow, "Lines Identification" );
	UxPutKeyboardFocusPolicy( ApplicWindow, "pointer" );
	UxPutIconName( ApplicWindow, "Lines Identification" );
	UxPutHeight( ApplicWindow, 476 );
	UxPutWidth( ApplicWindow, 367 );
	UxPutY( ApplicWindow, 6 );
	UxPutX( ApplicWindow, 6 );

	UxPutBackground( MainWindow, WindowBackground );
	UxPutHeight( MainWindow, 470 );
	UxPutWidth( MainWindow, 400 );
	UxPutY( MainWindow, 0 );
	UxPutX( MainWindow, 0 );
	UxPutUnitType( MainWindow, "pixels" );

	UxPutBackground( menu3, MenuBackground );
	UxPutMenuAccelerator( menu3, "<KeyUp>F10" );
	UxPutRowColumnType( menu3, "menu_bar" );

	UxPutForeground( menu3_p2, MenuForeground );
	UxPutBackground( menu3_p2, MenuBackground );
	UxPutRowColumnType( menu3_p2, "menu_pulldown" );

	UxPutFontList( menu3_p2_b1, BoldTextFont );
	UxPutMnemonic( menu3_p2_b1, "x" );
	UxPutLabelString( menu3_p2_b1, "Cut x" );

	UxPutFontList( menu3_p2_b2, BoldTextFont );
	UxPutMnemonic( menu3_p2_b2, "y" );
	UxPutLabelString( menu3_p2_b2, "Cut y" );

	UxPutFontList( menu3_p2_b3, BoldTextFont );
	UxPutMnemonic( menu3_p2_b3, "S" );
	UxPutLabelString( menu3_p2_b3, "Scroll" );

	UxPutFontList( menu3_p2_b4, BoldTextFont );
	UxPutMnemonic( menu3_p2_b4, "U" );
	UxPutLabelString( menu3_p2_b4, "Unzoom" );

	UxPutFontList( menu3_p2_b5, BoldTextFont );
	UxPutMnemonic( menu3_p2_b5, "G" );
	UxPutLabelString( menu3_p2_b5, "Get cursor" );

	UxPutFontList( menu3_p2_b8, BoldTextFont );
	UxPutAccelerator( menu3_p2_b8, "E" );
	UxPutLabelString( menu3_p2_b8, "Exit" );

	UxPutForeground( menu3_top_b2, MenuForeground );
	UxPutFontList( menu3_top_b2, BoldTextFont );
	UxPutBackground( menu3_top_b2, MenuBackground );
	UxPutMnemonic( menu3_top_b2, "F" );
	UxPutLabelString( menu3_top_b2, "Frame" );

	UxPutForeground( menu3_p3, MenuForeground );
	UxPutBackground( menu3_p3, MenuBackground );
	UxPutRowColumnType( menu3_p3, "menu_pulldown" );

	UxPutFontList( menu3_p3_b1, BoldTextFont );
	UxPutMnemonic( menu3_p3_b1, "C" );
	UxPutLabelString( menu3_p3_b1, "Create graphics" );

	UxPutFontList( menu3_p3_b2, BoldTextFont );
	UxPutMnemonic( menu3_p3_b2, "P" );
	UxPutLabelString( menu3_p3_b2, "Print graphics" );

	UxPutFontList( menu3_p3_b3, BoldTextFont );
	UxPutMnemonic( menu3_p3_b3, "R" );
	UxPutLabelString( menu3_p3_b3, "Reset graphics" );

	UxPutForeground( menu3_top_b3, MenuForeground );
	UxPutFontList( menu3_top_b3, BoldTextFont );
	UxPutBackground( menu3_top_b3, MenuBackground );
	UxPutMnemonic( menu3_top_b3, "U" );
	UxPutLabelString( menu3_top_b3, "Utils" );

	UxPutForeground( menu3_p4, MenuForeground );
	UxPutBackground( menu3_p4, MenuBackground );
	UxPutRowColumnType( menu3_p4, "menu_pulldown" );

	UxPutFontList( menu_help_window, BoldTextFont );
	UxPutMnemonic( menu_help_window, "W" );
	UxPutLabelString( menu_help_window, "On Window ..." );

	UxPutForeground( menu3_top_b4, MenuForeground );
	UxPutFontList( menu3_top_b4, BoldTextFont );
	UxPutBackground( menu3_top_b4, MenuBackground );
	UxPutMnemonic( menu3_top_b4, "H" );
	UxPutLabelString( menu3_top_b4, "Help" );

	UxPutY( form3, 34 );
	UxPutBackground( form3, WindowBackground );

	UxPutBackground( form2, ButtonBackground );
	UxPutHeight( form2, 42 );
	UxPutWidth( form2, 368 );
	UxPutY( form2, 400 );
	UxPutX( form2, -2 );
	UxPutResizePolicy( form2, "resize_none" );

	UxPutHighlightOnEnter( pb_begin, "true" );
	UxPutLabelString( pb_begin, "begin" );
	UxPutFontList( pb_begin, BoldTextFont );
	UxPutForeground( pb_begin, ButtonForeground );
	UxPutBackground( pb_begin, ButtonBackground );
	UxPutHeight( pb_begin, 30 );
	UxPutWidth( pb_begin, 85 );
	UxPutY( pb_begin, 5 );
	UxPutX( pb_begin, 5 );

	UxPutHighlightOnEnter( pb_continue, "true" );
	UxPutLabelString( pb_continue, "continue" );
	UxPutFontList( pb_continue, BoldTextFont );
	UxPutForeground( pb_continue, ButtonForeground );
	UxPutBackground( pb_continue, ButtonBackground );
	UxPutHeight( pb_continue, 30 );
	UxPutWidth( pb_continue, 85 );
	UxPutY( pb_continue, 5 );
	UxPutX( pb_continue, 94 );

	UxPutHighlightOnEnter( pb_delete, "true" );
	UxPutLabelString( pb_delete, "delete" );
	UxPutFontList( pb_delete, BoldTextFont );
	UxPutForeground( pb_delete, ButtonForeground );
	UxPutBackground( pb_delete, ButtonBackground );
	UxPutHeight( pb_delete, 30 );
	UxPutWidth( pb_delete, 85 );
	UxPutY( pb_delete, 5 );
	UxPutX( pb_delete, 184 );

	UxPutBackground( separator1, WindowBackground );
	UxPutHeight( separator1, 6 );
	UxPutWidth( separator1, 372 );
	UxPutY( separator1, 394 );
	UxPutX( separator1, -2 );

	UxPutFontList( SHelp, TextFont );
	UxPutEditable( SHelp, "false" );
	UxPutCursorPositionVisible( SHelp, "false" );
	UxPutBackground( SHelp, SHelpBackground );
	UxPutHeight( SHelp, 50 );
	UxPutWidth( SHelp, 368 );
	UxPutY( SHelp, 346 );
	UxPutX( SHelp, 0 );

	UxPutBackground( scrolledWindow1, ListBackground );
	UxPutScrollBarPlacement( scrolledWindow1, "bottom_left" );
	UxPutShadowThickness( scrolledWindow1, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow1, "static" );
	UxPutVisualPolicy( scrolledWindow1, "variable" );
	UxPutY( scrolledWindow1, 35 );
	UxPutX( scrolledWindow1, 0 );
	UxPutScrollingPolicy( scrolledWindow1, "application_defined" );

	UxPutForeground( ls_lincat, ListForeground );
	UxPutFontList( ls_lincat, SmallFont );
	UxPutBackground( ls_lincat, ListBackground );
	UxPutVisibleItemCount( ls_lincat, 18 );
	UxPutScrollBarDisplayPolicy( ls_lincat, "static" );
	UxPutListSizePolicy( ls_lincat, "constant" );
	UxPutHeight( ls_lincat, 295 );
	UxPutWidth( ls_lincat, 346 );

	UxPutForeground( label1, TextForeground );
	UxPutBackground( label1, WindowBackground );
	UxPutAlignment( label1, "alignment_beginning" );
	UxPutLabelString( label1, "        Wavelength           Intensity             ION" );
	UxPutFontList( label1, BoldTextFont );
	UxPutHeight( label1, 35 );
	UxPutWidth( label1, 365 );
	UxPutY( label1, 0 );
	UxPutX( label1, 0 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ApplicWindow()
{
	/* Create the swidgets */

	ApplicWindow = UxCreateApplicationShell( "ApplicWindow", NO_PARENT );
	UxPutContext( ApplicWindow, UxApplicWindowContext );

	MainWindow = UxCreateMainWindow( "MainWindow", ApplicWindow );
	menu3 = UxCreateRowColumn( "menu3", MainWindow );
	menu3_p2 = UxCreateRowColumn( "menu3_p2", menu3 );
	menu3_p2_b1 = UxCreatePushButtonGadget( "menu3_p2_b1", menu3_p2 );
	menu3_p2_b2 = UxCreatePushButtonGadget( "menu3_p2_b2", menu3_p2 );
	menu3_p2_b3 = UxCreatePushButtonGadget( "menu3_p2_b3", menu3_p2 );
	menu3_p2_b4 = UxCreatePushButtonGadget( "menu3_p2_b4", menu3_p2 );
	menu3_p2_b5 = UxCreatePushButtonGadget( "menu3_p2_b5", menu3_p2 );
	menu3_p2_b7 = UxCreateSeparator( "menu3_p2_b7", menu3_p2 );
	menu3_p2_b8 = UxCreatePushButtonGadget( "menu3_p2_b8", menu3_p2 );
	menu3_top_b2 = UxCreateCascadeButton( "menu3_top_b2", menu3 );
	menu3_p3 = UxCreateRowColumn( "menu3_p3", menu3 );
	menu3_p3_b1 = UxCreatePushButtonGadget( "menu3_p3_b1", menu3_p3 );
	menu3_p3_b2 = UxCreatePushButtonGadget( "menu3_p3_b2", menu3_p3 );
	menu3_p3_b3 = UxCreatePushButtonGadget( "menu3_p3_b3", menu3_p3 );
	menu3_top_b3 = UxCreateCascadeButton( "menu3_top_b3", menu3 );
	menu3_p4 = UxCreateRowColumn( "menu3_p4", menu3 );
	menu_help_window = UxCreatePushButtonGadget( "menu_help_window", menu3_p4 );
	menu3_top_b4 = UxCreateCascadeButton( "menu3_top_b4", menu3 );
	form3 = UxCreateForm( "form3", MainWindow );
	form2 = UxCreateForm( "form2", form3 );
	pb_begin = UxCreatePushButton( "pb_begin", form2 );
	pb_continue = UxCreatePushButton( "pb_continue", form2 );
	pb_delete = UxCreatePushButton( "pb_delete", form2 );
	separator1 = UxCreateSeparator( "separator1", form3 );
	SHelp = UxCreateText( "SHelp", form3 );
	scrolledWindow1 = UxCreateScrolledWindow( "scrolledWindow1", form3 );
	ls_lincat = UxCreateScrolledList( "ls_lincat", scrolledWindow1 );
	label1 = UxCreateLabel( "label1", form3 );

	_Uxinit_ApplicWindow();

	/* Create the X widgets */

	UxCreateWidget( ApplicWindow );
	UxCreateWidget( MainWindow );
	UxCreateWidget( menu3 );
	UxCreateWidget( menu3_p2 );
	UxCreateWidget( menu3_p2_b1 );
	UxCreateWidget( menu3_p2_b2 );
	UxCreateWidget( menu3_p2_b3 );
	UxCreateWidget( menu3_p2_b4 );
	UxCreateWidget( menu3_p2_b5 );
	UxCreateWidget( menu3_p2_b7 );
	UxCreateWidget( menu3_p2_b8 );
	UxPutSubMenuId( menu3_top_b2, "menu3_p2" );
	UxCreateWidget( menu3_top_b2 );

	UxCreateWidget( menu3_p3 );
	UxCreateWidget( menu3_p3_b1 );
	UxCreateWidget( menu3_p3_b2 );
	UxCreateWidget( menu3_p3_b3 );
	UxPutSubMenuId( menu3_top_b3, "menu3_p3" );
	UxCreateWidget( menu3_top_b3 );

	UxCreateWidget( menu3_p4 );
	UxCreateWidget( menu_help_window );
	UxPutSubMenuId( menu3_top_b4, "menu3_p4" );
	UxCreateWidget( menu3_top_b4 );

	UxCreateWidget( form3 );
	UxPutBottomAttachment( form2, "attach_form" );
	UxCreateWidget( form2 );

	UxCreateWidget( pb_begin );
	UxCreateWidget( pb_continue );
	UxCreateWidget( pb_delete );
	UxPutBottomWidget( separator1, "form2" );
	UxPutBottomAttachment( separator1, "attach_widget" );
	UxCreateWidget( separator1 );

	UxPutBottomWidget( SHelp, "separator1" );
	UxPutBottomAttachment( SHelp, "attach_widget" );
	UxCreateWidget( SHelp );

	UxPutBottomWidget( scrolledWindow1, "SHelp" );
	UxPutBottomAttachment( scrolledWindow1, "attach_widget" );
	UxCreateWidget( scrolledWindow1 );

	UxCreateWidget( ls_lincat );
	createCB_ls_lincat( UxGetWidget( ls_lincat ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutBottomWidget( label1, "scrolledWindow1" );
	UxPutBottomAttachment( label1, "attach_widget" );
	UxCreateWidget( label1 );


	UxPutMenuHelpWidget( menu3, "menu3_top_b4" );

	UxAddCallback( menu3_p2_b1, XmNactivateCallback,
			activateCB_menu3_p2_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu3_p2_b2, XmNactivateCallback,
			activateCB_menu3_p2_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu3_p2_b3, XmNactivateCallback,
			activateCB_menu3_p2_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu3_p2_b4, XmNactivateCallback,
			activateCB_menu3_p2_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu3_p2_b5, XmNactivateCallback,
			activateCB_menu3_p2_b5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu3_p2_b8, XmNactivateCallback,
			activateCB_menu3_p2_b8,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu3_p3_b1, XmNactivateCallback,
			activateCB_menu3_p3_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu3_p3_b2, XmNactivateCallback,
			activateCB_menu3_p3_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu3_p3_b3, XmNactivateCallback,
			activateCB_menu3_p3_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_window, XmNactivateCallback,
			activateCB_menu_help_window,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_begin, XmNactivateCallback,
			activateCB_pb_begin,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_continue, XmNactivateCallback,
			activateCB_pb_continue,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_delete, XmNactivateCallback,
			activateCB_pb_delete,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( ls_lincat, XmNbrowseSelectionCallback,
			browseSelectionCB_ls_lincat,
			(XtPointer) UxApplicWindowContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ApplicWindow );

	UxMainWindowSetAreas( MainWindow, menu3, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, form3 );
	return ( ApplicWindow );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ApplicWindow()
{
	swidget                 rtrn;
	_UxCApplicWindow        *UxContext;

	UxApplicWindowContext = UxContext =
		(_UxCApplicWindow *) UxMalloc( sizeof(_UxCApplicWindow) );

	{
		extern swidget create_HelpShell();
		rtrn = _Uxbuild_ApplicWindow();

		create_HelpShell();
		
		return(rtrn);
	}
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ApplicWindow()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearHelp", action_ClearHelp },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "WriteHelp", action_WriteHelp }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ApplicWindow();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

