! UIMX ascii 2.0 key: 555                                                       

*HelpShell.class: topLevelShell
*HelpShell.parent: NO_PARENT
*HelpShell.static: true
*HelpShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*HelpShell.ispecdecl:
*HelpShell.funcdecl: swidget create_HelpShell()\

*HelpShell.funcname: create_HelpShell
*HelpShell.funcdef: "swidget", "<create_HelpShell>(%)"
*HelpShell.icode:
*HelpShell.fcode: return(rtrn);\

*HelpShell.auxdecl:
*HelpShell.name: HelpShell
*HelpShell.x: 20
*HelpShell.y: 70
*HelpShell.width: 700
*HelpShell.height: 415
*HelpShell.title: "Extended help"
*HelpShell.keyboardFocusPolicy: "pointer"
*HelpShell.iconName: "Extended help"
*HelpShell.background: ButtonBackground 
*HelpShell.geometry: "-0+0"

*form21.class: form
*form21.parent: HelpShell
*form21.static: true
*form21.name: form21
*form21.resizePolicy: "resize_none"
*form21.unitType: "pixels"
*form21.x: 0
*form21.y: 0
*form21.width: 664
*form21.height: 284
*form21.background: ButtonBackground 

*scrolledWindow9.class: scrolledWindow
*scrolledWindow9.parent: form21
*scrolledWindow9.static: true
*scrolledWindow9.name: scrolledWindow9
*scrolledWindow9.scrollingPolicy: "automatic"
*scrolledWindow9.x: 0
*scrolledWindow9.y: 2
*scrolledWindow9.width: 710
*scrolledWindow9.height: 372
*scrolledWindow9.scrollBarPlacement: "bottom_left"
*scrolledWindow9.background: WindowBackground 

*rowColumn8.class: rowColumn
*rowColumn8.parent: scrolledWindow9
*rowColumn8.static: true
*rowColumn8.name: rowColumn8
*rowColumn8.x: 25
*rowColumn8.y: 4
*rowColumn8.width: 418
*rowColumn8.height: 427
*rowColumn8.orientation: "horizontal"
*rowColumn8.background: "white"
*rowColumn8.entryAlignment: "alignment_beginning"

*tx_extended_help.class: text
*tx_extended_help.parent: rowColumn8
*tx_extended_help.static: true
*tx_extended_help.name: tx_extended_help
*tx_extended_help.x: 3
*tx_extended_help.y: 3
*tx_extended_help.width: 673
*tx_extended_help.height: 797
*tx_extended_help.background: TextBackground 
*tx_extended_help.fontList: SmallFont 
*tx_extended_help.editMode: "multi_line_edit"
*tx_extended_help.cursorPositionVisible: "false"
*tx_extended_help.foreground: TextForeground 
*tx_extended_help.highlightColor: "Black"

*help_print.class: pushButton
*help_print.parent: form21
*help_print.static: true
*help_print.name: help_print
*help_print.x: 516
*help_print.y: 378
*help_print.width: 82
*help_print.height: 32
*help_print.background: ButtonBackground 
*help_print.labelString: "Print"
*help_print.fontList: BoldTextFont 
*help_print.highlightColor: "Black"
*help_print.highlightOnEnter: "true"
*help_print.foreground: ButtonForeground 

*pb_help_return.class: pushButton
*pb_help_return.parent: form21
*pb_help_return.static: true
*pb_help_return.name: pb_help_return
*pb_help_return.x: 604
*pb_help_return.y: 378
*pb_help_return.width: 82
*pb_help_return.height: 32
*pb_help_return.background: ButtonBackground 
*pb_help_return.fontList: BoldTextFont 
*pb_help_return.labelString: "Quit"
*pb_help_return.activateCallback: {\
UxPopdownInterface(UxFindSwidget("HelpShell"));\
 \
\
}
*pb_help_return.highlightColor: "red"
*pb_help_return.highlightOnEnter: "true"
*pb_help_return.foreground: CancelForeground  

