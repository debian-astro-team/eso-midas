! UIMX ascii 2.0 key: 5799                                                      
*action.WriteHelp: {\
DisplayShortHelp(UxWidget);\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.ClearHelp: {\
UxPutText(SHelp, "");\
}

*ApplicWindow.class: applicationShell
*ApplicWindow.parent: NO_PARENT
*ApplicWindow.static: true
*ApplicWindow.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\

*ApplicWindow.ispecdecl:
*ApplicWindow.funcdecl: swidget create_ApplicWindow()\

*ApplicWindow.funcname: create_ApplicWindow
*ApplicWindow.funcdef: "swidget", "<create_ApplicWindow>(%)"
*ApplicWindow.icode: extern swidget create_HelpShell();
*ApplicWindow.fcode: create_HelpShell();\
\
return(rtrn);\

*ApplicWindow.auxdecl:
*ApplicWindow.name: ApplicWindow
*ApplicWindow.x: 6
*ApplicWindow.y: 6
*ApplicWindow.width: 367
*ApplicWindow.height: 476
*ApplicWindow.iconName: "Lines Identification"
*ApplicWindow.keyboardFocusPolicy: "pointer"
*ApplicWindow.title: "Lines Identification"
*ApplicWindow.background: ApplicBackground

*MainWindow.class: mainWindow
*MainWindow.parent: ApplicWindow
*MainWindow.static: true
*MainWindow.name: MainWindow
*MainWindow.unitType: "pixels"
*MainWindow.x: 0
*MainWindow.y: 0
*MainWindow.width: 400
*MainWindow.height: 470
*MainWindow.background: WindowBackground

*menu3.class: rowColumn
*menu3.parent: MainWindow
*menu3.static: true
*menu3.name: menu3
*menu3.rowColumnType: "menu_bar"
*menu3.menuHelpWidget: "menu3_top_b4"
*menu3.menuAccelerator: "<KeyUp>F10"
*menu3.background: MenuBackground

*menu3_p2.class: rowColumn
*menu3_p2.parent: menu3
*menu3_p2.static: true
*menu3_p2.name: menu3_p2
*menu3_p2.rowColumnType: "menu_pulldown"
*menu3_p2.background: MenuBackground
*menu3_p2.foreground: MenuForeground

*menu3_p2_b1.class: pushButtonGadget
*menu3_p2_b1.parent: menu3_p2
*menu3_p2_b1.static: true
*menu3_p2_b1.name: menu3_p2_b1
*menu3_p2_b1.labelString: "Cut x"
*menu3_p2_b1.mnemonic: "x"
*menu3_p2_b1.fontList: BoldTextFont
*menu3_p2_b1.activateCallback: cutx_spectrum();

*menu3_p2_b2.class: pushButtonGadget
*menu3_p2_b2.parent: menu3_p2
*menu3_p2_b2.static: true
*menu3_p2_b2.name: menu3_p2_b2
*menu3_p2_b2.labelString: "Cut y"
*menu3_p2_b2.mnemonic: "y"
*menu3_p2_b2.fontList: BoldTextFont
*menu3_p2_b2.activateCallback: cuty_spectrum();

*menu3_p2_b3.class: pushButtonGadget
*menu3_p2_b3.parent: menu3_p2
*menu3_p2_b3.static: true
*menu3_p2_b3.name: menu3_p2_b3
*menu3_p2_b3.labelString: "Scroll"
*menu3_p2_b3.mnemonic: "S"
*menu3_p2_b3.fontList: BoldTextFont
*menu3_p2_b3.activateCallback: shift_spectrum();

*menu3_p2_b4.class: pushButtonGadget
*menu3_p2_b4.parent: menu3_p2
*menu3_p2_b4.static: true
*menu3_p2_b4.name: menu3_p2_b4
*menu3_p2_b4.labelString: "Unzoom"
*menu3_p2_b4.mnemonic: "U"
*menu3_p2_b4.fontList: BoldTextFont
*menu3_p2_b4.activateCallback: unzoom_spectrum();

*menu3_p2_b5.class: pushButtonGadget
*menu3_p2_b5.parent: menu3_p2
*menu3_p2_b5.static: true
*menu3_p2_b5.name: menu3_p2_b5
*menu3_p2_b5.labelString: "Get cursor"
*menu3_p2_b5.mnemonic: "G"
*menu3_p2_b5.fontList: BoldTextFont
*menu3_p2_b5.activateCallback: getcur_spectrum();

*menu3_p2_b7.class: separator
*menu3_p2_b7.parent: menu3_p2
*menu3_p2_b7.static: true
*menu3_p2_b7.name: menu3_p2_b7

*menu3_p2_b8.class: pushButtonGadget
*menu3_p2_b8.parent: menu3_p2
*menu3_p2_b8.static: true
*menu3_p2_b8.name: menu3_p2_b8
*menu3_p2_b8.labelString: "Exit"
*menu3_p2_b8.accelerator: "E"
*menu3_p2_b8.activateCallback: {\
AppendDialogText("select/tab 'LINTAB' all");\
end_gmidas();\
exit(0);\
}
*menu3_p2_b8.fontList: BoldTextFont

*menu3_p3.class: rowColumn
*menu3_p3.parent: menu3
*menu3_p3.static: true
*menu3_p3.name: menu3_p3
*menu3_p3.rowColumnType: "menu_pulldown"
*menu3_p3.background: MenuBackground
*menu3_p3.foreground: MenuForeground

*menu3_p3_b1.class: pushButtonGadget
*menu3_p3_b1.parent: menu3_p3
*menu3_p3_b1.static: true
*menu3_p3_b1.name: menu3_p3_b1
*menu3_p3_b1.labelString: "Create graphics"
*menu3_p3_b1.mnemonic: "C"
*menu3_p3_b1.fontList: BoldTextFont
*menu3_p3_b1.activateCallback: AppendDialogText("graph/long");

*menu3_p3_b2.class: pushButtonGadget
*menu3_p3_b2.parent: menu3_p3
*menu3_p3_b2.static: true
*menu3_p3_b2.name: menu3_p3_b2
*menu3_p3_b2.labelString: "Print graphics"
*menu3_p3_b2.mnemonic: "P"
*menu3_p3_b2.fontList: BoldTextFont
*menu3_p3_b2.activateCallback: AppendDialogText("copy/graphic laser");

*menu3_p3_b3.class: pushButtonGadget
*menu3_p3_b3.parent: menu3_p3
*menu3_p3_b3.static: true
*menu3_p3_b3.name: menu3_p3_b3
*menu3_p3_b3.labelString: "Reset graphics"
*menu3_p3_b3.mnemonic: "R"
*menu3_p3_b3.fontList: BoldTextFont
*menu3_p3_b3.activateCallback: AppendDialogText("reset/display");

*menu3_p4.class: rowColumn
*menu3_p4.parent: menu3
*menu3_p4.static: true
*menu3_p4.name: menu3_p4
*menu3_p4.rowColumnType: "menu_pulldown"
*menu3_p4.background: MenuBackground
*menu3_p4.foreground: MenuForeground

*menu_help_window.class: pushButtonGadget
*menu_help_window.parent: menu3_p4
*menu_help_window.static: true
*menu_help_window.name: menu_help_window
*menu_help_window.labelString: "On Window ..."
*menu_help_window.mnemonic: "W"
*menu_help_window.fontList: BoldTextFont
*menu_help_window.activateCallback: DisplayExtendedHelp(UxWidget);

*menu3_top_b2.class: cascadeButton
*menu3_top_b2.parent: menu3
*menu3_top_b2.static: true
*menu3_top_b2.name: menu3_top_b2
*menu3_top_b2.labelString: "Frame"
*menu3_top_b2.mnemonic: "F"
*menu3_top_b2.subMenuId: "menu3_p2"
*menu3_top_b2.background: MenuBackground
*menu3_top_b2.fontList: BoldTextFont
*menu3_top_b2.foreground: MenuForeground

*menu3_top_b3.class: cascadeButton
*menu3_top_b3.parent: menu3
*menu3_top_b3.static: true
*menu3_top_b3.name: menu3_top_b3
*menu3_top_b3.labelString: "Utils"
*menu3_top_b3.mnemonic: "U"
*menu3_top_b3.subMenuId: "menu3_p3"
*menu3_top_b3.background: MenuBackground
*menu3_top_b3.fontList: BoldTextFont
*menu3_top_b3.foreground: MenuForeground

*menu3_top_b4.class: cascadeButton
*menu3_top_b4.parent: menu3
*menu3_top_b4.static: true
*menu3_top_b4.name: menu3_top_b4
*menu3_top_b4.labelString: "Help"
*menu3_top_b4.mnemonic: "H"
*menu3_top_b4.subMenuId: "menu3_p4"
*menu3_top_b4.background: MenuBackground
*menu3_top_b4.fontList: BoldTextFont
*menu3_top_b4.foreground: MenuForeground

*form3.class: form
*form3.parent: MainWindow
*form3.static: true
*form3.name: form3
*form3.background: WindowBackground
*form3.y: 34

*form2.class: form
*form2.parent: form3
*form2.static: true
*form2.name: form2
*form2.resizePolicy: "resize_none"
*form2.x: -2
*form2.y: 400
*form2.width: 368
*form2.height: 42
*form2.background: ButtonBackground
*form2.bottomAttachment: "attach_form"

*pb_begin.class: pushButton
*pb_begin.parent: form2
*pb_begin.static: true
*pb_begin.name: pb_begin
*pb_begin.x: 5
*pb_begin.y: 5
*pb_begin.width: 85
*pb_begin.height: 30
*pb_begin.background: ButtonBackground
*pb_begin.foreground: ButtonForeground
*pb_begin.fontList: BoldTextFont
*pb_begin.labelString: "begin"
*pb_begin.highlightOnEnter: "true"
*pb_begin.activateCallback: {\
#include <main_defs.h>\
#include <spec_comm.h>\
\
extern int NumLincatPos;   /* cardinality of LincatPos[] */\
extern int NumWlcPos;      /* cardinality of WlcPos[] */\
extern int IdentBegin;\
\
NumLincatPos = NumWlcPos = 0;\
\
AppendDialogText(C_SELINE);\
read_params_long();\
if ( ! read_lincat_table() )\
    return;\
\
if ( !read_line_x(IDBEGIN) ) {  /* :X of line table */\
    SCTPUT( "*** Lines have not been searched ***" );\
    return;\
}\
\
if ( !plot_spec(LABEL_SPEC) ) /* plot 'WLC(Ystart)' */\
     return;\
IdentBegin = TRUE;\
\
get_wlc_value();\
\
display_lincat_table(UxGetWidget(UxFindSwidget("ls_lincat")));\
}

*pb_continue.class: pushButton
*pb_continue.parent: form2
*pb_continue.static: true
*pb_continue.name: pb_continue
*pb_continue.x: 94
*pb_continue.y: 5
*pb_continue.width: 85
*pb_continue.height: 30
*pb_continue.background: ButtonBackground
*pb_continue.foreground: ButtonForeground
*pb_continue.fontList: BoldTextFont
*pb_continue.labelString: "continue"
*pb_continue.highlightOnEnter: "true"
*pb_continue.activateCallback: {\
#include <main_defs.h>\
#include <spec_comm.h>\
\
extern int NumWlcPos, NumLincatPos;\
extern int IdentBegin;\
\
NumWlcPos = NumLincatPos = 0;\
\
AppendDialogText(C_SELINE);\
read_params_long();\
if ( ! read_lincat_table() )\
    return;\
\
if ( !read_line_x(IDCONT) ) {   /* :X of line table */\
    SCTPUT( "*** Lines have not been searched ***" );\
    return;\
}\
\
if ( !IdentBegin ) {\
    if ( !plot_spec(LABEL_SPEC) )   /* plot 'WLC(Ystart)' */\
        return;\
}\
else \
    redraw_plot();\
\
show_current_idents();\
get_wlc_value();\
\
display_lincat_table(UxGetWidget(UxFindSwidget("ls_lincat")));\
\
IdentBegin = TRUE;\
}

*pb_delete.class: pushButton
*pb_delete.parent: form2
*pb_delete.static: true
*pb_delete.name: pb_delete
*pb_delete.x: 184
*pb_delete.y: 5
*pb_delete.width: 85
*pb_delete.height: 30
*pb_delete.background: ButtonBackground
*pb_delete.foreground: ButtonForeground
*pb_delete.fontList: BoldTextFont
*pb_delete.labelString: "delete"
*pb_delete.highlightOnEnter: "true"
*pb_delete.activateCallback: {\
#include <main_defs.h>\
#include <spec_comm.h>\
\
extern int NumWlcPos, NumLincatPos;\
extern int IdentBegin;\
\
NumWlcPos = NumLincatPos = 0;\
\
AppendDialogText(C_SELINE);\
read_params_long();\
if ( ! read_lincat_table() )\
    return;\
\
if ( !read_line_x(IDCONT) ) {   /* :X of line table */\
    SCTPUT( "*** Lines have not been searched ***" );\
    return;\
}\
\
if ( !IdentBegin ) {\
    if ( !plot_spec(LABEL_SPEC) )   /* plot 'WLC(Ystart)' */\
        return;\
}\
else \
    redraw_plot();\
\
show_current_idents();\
delete_idents();\
\
IdentBegin = TRUE;\
}

*separator1.class: separator
*separator1.parent: form3
*separator1.static: true
*separator1.name: separator1
*separator1.x: -2
*separator1.y: 394
*separator1.width: 372
*separator1.height: 6
*separator1.background: WindowBackground
*separator1.bottomAttachment: "attach_widget"
*separator1.bottomWidget: "form2"

*SHelp.class: text
*SHelp.parent: form3
*SHelp.static: true
*SHelp.name: SHelp
*SHelp.x: 0
*SHelp.y: 346
*SHelp.width: 368
*SHelp.height: 50
*SHelp.background: SHelpBackground
*SHelp.cursorPositionVisible: "false"
*SHelp.editable: "false"
*SHelp.fontList: TextFont
*SHelp.bottomAttachment: "attach_widget"
*SHelp.bottomWidget: "separator1"

*scrolledWindow1.class: scrolledWindow
*scrolledWindow1.parent: form3
*scrolledWindow1.static: true
*scrolledWindow1.name: scrolledWindow1
*scrolledWindow1.scrollingPolicy: "application_defined"
*scrolledWindow1.x: 0
*scrolledWindow1.y: 35
*scrolledWindow1.visualPolicy: "variable"
*scrolledWindow1.scrollBarDisplayPolicy: "static"
*scrolledWindow1.shadowThickness: 0
*scrolledWindow1.scrollBarPlacement: "bottom_left"
*scrolledWindow1.background: ListBackground
*scrolledWindow1.bottomAttachment: "attach_widget"
*scrolledWindow1.bottomWidget: "SHelp"

*ls_lincat.class: scrolledList
*ls_lincat.parent: scrolledWindow1
*ls_lincat.static: true
*ls_lincat.name: ls_lincat
*ls_lincat.width: 346
*ls_lincat.height: 295
*ls_lincat.listSizePolicy: "constant"
*ls_lincat.scrollBarDisplayPolicy: "static"
*ls_lincat.visibleItemCount: 18
*ls_lincat.background: ListBackground
*ls_lincat.createCallback: {\
display_lincat_table(UxWidget);\
}
*ls_lincat.browseSelectionCallback: {\
char *choice;\
int i;\
float item_value;\
XmListCallbackStruct *cbs;\
\
extern int NumLincatPos, NumWlcPos;\
extern float LincatPos[];\
\
cbs = (XmListCallbackStruct *)UxCallbackArg;\
\
if ( NumLincatPos == NumWlcPos ) /* Noclick in graphic window */\
    return;\
\
XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);\
\
sscanf( choice, "%f", &item_value );\
\
for ( i = NumLincatPos-1; i >= 0; i-- )\
    if ( item_value == LincatPos[i] ) /* value already selected */\
        return;\
\
LincatPos[NumLincatPos] = item_value;\
NumLincatPos++;\
\
\
ident_line();       /* Ident wavelength in line table */\
get_wlc_value();    /* let's go to graphic window again */\
\
XtFree(choice);\
}
*ls_lincat.fontList: SmallFont
*ls_lincat.foreground: ListForeground

*label1.class: label
*label1.parent: form3
*label1.static: true
*label1.name: label1
*label1.x: 0
*label1.y: 0
*label1.width: 365
*label1.height: 35
*label1.fontList: BoldTextFont
*label1.labelString: "        Wavelength           Intensity             ION"
*label1.alignment: "alignment_beginning"
*label1.background: WindowBackground
*label1.foreground: TextForeground
*label1.bottomAttachment: "attach_widget"
*label1.bottomWidget: "scrolledWindow1"

