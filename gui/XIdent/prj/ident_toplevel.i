! UIMX ascii 2.0 key: 9442                                                      

*ident_toplevel.class: topLevelShell
*ident_toplevel.parent: NO_PARENT
*ident_toplevel.static: true
*ident_toplevel.gbldecl: #include <stdio.h>\

*ident_toplevel.funcdecl: swidget create_ident_toplevel()\

*ident_toplevel.funcname: create_ident_toplevel
*ident_toplevel.funcdef: "swidget", "<create_ident_toplevel>(%)"
*ident_toplevel.fcode: return(rtrn);\

*ident_toplevel.name: ident_toplevel
*ident_toplevel.x: 10
*ident_toplevel.y: 10
*ident_toplevel.width: 366
*ident_toplevel.height: 425
*ident_toplevel.defaultFontList: "-misc-fixed-medium-r-normal--13-120-75-75-c-80-iso8859-1"
*ident_toplevel.keyboardFocusPolicy: "pointer"
*ident_toplevel.title: "Lines identification"

*form1.class: form
*form1.parent: ident_toplevel
*form1.static: true
*form1.name: form1
*form1.resizePolicy: "resize_none"
*form1.unitType: "pixels"
*form1.x: 5
*form1.y: 0
*form1.width: 365
*form1.height: 425

*form2.class: form
*form2.parent: form1
*form2.static: true
*form2.name: form2
*form2.resizePolicy: "resize_none"
*form2.x: 0
*form2.y: 350
*form2.width: 365
*form2.height: 75
*form2.background: "grey70"

*pushButton1.class: pushButton
*pushButton1.parent: form2
*pushButton1.static: true
*pushButton1.name: pushButton1
*pushButton1.x: 5
*pushButton1.y: 5
*pushButton1.width: 85
*pushButton1.height: 30
*pushButton1.background: "pink"
*pushButton1.foreground: "black"
*pushButton1.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*pushButton1.labelString: "begin"
*pushButton1.highlightOnEnter: "true"
*pushButton1.activateCallback: {\
#include <main_defs.h>\
\
extern int NumLincatPos;   /* cardinality of LincatPos[] */\
extern int NumWlcPos;      /* cardinality of WlcPos[] */\
extern int IdentBegin;     /* toggle */\
\
if ( !exists_graphic() )\
    return;\
\
NumLincatPos = NumWlcPos = 0;\
\
if ( !read_line_x(IDBEGIN) ) {  /* :X of line table */\
    SCTPUT( "*** Lines have not been searched ***" );\
    return;\
}\
\
if ( !plot_spec(LABEL_SPEC) ) /* plot 'WLC(Ystart)' */\
     return;\
\
get_wlc_value();\
IdentBegin = TRUE;\
\
}

*pushButton2.class: pushButton
*pushButton2.parent: form2
*pushButton2.static: true
*pushButton2.name: pushButton2
*pushButton2.x: 95
*pushButton2.y: 5
*pushButton2.width: 85
*pushButton2.height: 30
*pushButton2.background: "pink"
*pushButton2.foreground: "black"
*pushButton2.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*pushButton2.labelString: "continue"
*pushButton2.highlightOnEnter: "true"
*pushButton2.activateCallback: {\
#include <main_defs.h>\
\
extern int IdentBegin;\
extern int NumWlcPos, NumLincatPos;\
\
if ( IdentBegin ) {\
    show_current_idents();\
    get_wlc_value();\
    return;\
}\
\
if ( !exists_graphic() )\
    return;\
\
NumWlcPos = NumLincatPos = 0;\
\
if ( !read_line_x(IDCONT) ) {   /* :X of line table */\
    SCTPUT( "*** Lines have not been searched ***" );\
    return;\
}\
\
if ( !plot_spec(LABEL_SPEC) )   /* plot 'WLC(Ystart)' */\
    return;\
\
show_current_idents();\
get_wlc_value();\
IdentBegin = TRUE;\
\
}

*pushButton3.class: pushButton
*pushButton3.parent: form2
*pushButton3.static: true
*pushButton3.name: pushButton3
*pushButton3.x: 185
*pushButton3.y: 5
*pushButton3.width: 85
*pushButton3.height: 30
*pushButton3.background: "pink"
*pushButton3.foreground: "black"
*pushButton3.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*pushButton3.labelString: "add line"
*pushButton3.highlightOnEnter: "true"

*pushButton4.class: pushButton
*pushButton4.parent: form2
*pushButton4.static: true
*pushButton4.name: pushButton4
*pushButton4.x: 275
*pushButton4.y: 5
*pushButton4.width: 85
*pushButton4.height: 30
*pushButton4.background: "pink"
*pushButton4.foreground: "black"
*pushButton4.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*pushButton4.labelString: "search"
*pushButton4.highlightOnEnter: "true"

*pushButton5.class: pushButton
*pushButton5.parent: form2
*pushButton5.static: true
*pushButton5.name: pushButton5
*pushButton5.x: 185
*pushButton5.y: 40
*pushButton5.width: 85
*pushButton5.height: 30
*pushButton5.background: "yellow"
*pushButton5.foreground: "black"
*pushButton5.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*pushButton5.labelString: "quit"
*pushButton5.highlightOnEnter: "true"
*pushButton5.activateCallback: {\
end_gmidas();\
exit(0);\
}

*menu1.class: rowColumn
*menu1.parent: form2
*menu1.static: true
*menu1.name: menu1
*menu1.rowColumnType: "menu_bar"
*menu1.y: 40
*menu1.menuAccelerator: "<KeyUp>F10"
*menu1.background: "green"
*menu1.x: 5
*menu1.height: 30
*menu1.width: 85

*menu1_p1.class: rowColumn
*menu1_p1.parent: menu1
*menu1_p1.static: true
*menu1_p1.name: menu1_p1
*menu1_p1.rowColumnType: "menu_pulldown"
*menu1_p1.background: "green"

*menu1_p1_b1.class: pushButton
*menu1_p1_b1.parent: menu1_p1
*menu1_p1_b1.static: true
*menu1_p1_b1.name: menu1_p1_b1
*menu1_p1_b1.labelString: "trim x"
*menu1_p1_b1.background: "green"
*menu1_p1_b1.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*menu1_p1_b1.foreground: "black"
*menu1_p1_b1.activateCallback: cutx_spectrum();

*menu1_p1_b2.class: pushButton
*menu1_p1_b2.parent: menu1_p1
*menu1_p1_b2.static: true
*menu1_p1_b2.name: menu1_p1_b2
*menu1_p1_b2.labelString: "trim y"
*menu1_p1_b2.background: "green"
*menu1_p1_b2.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*menu1_p1_b2.foreground: "black"
*menu1_p1_b2.activateCallback: cuty_spectrum();

*menu1_p1_b3.class: pushButton
*menu1_p1_b3.parent: menu1_p1
*menu1_p1_b3.static: true
*menu1_p1_b3.name: menu1_p1_b3
*menu1_p1_b3.labelString: "scroll"
*menu1_p1_b3.background: "green"
*menu1_p1_b3.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*menu1_p1_b3.foreground: "black"
*menu1_p1_b3.activateCallback: shift_spectrum();

*menu1_p1_b4.class: pushButton
*menu1_p1_b4.parent: menu1_p1
*menu1_p1_b4.static: true
*menu1_p1_b4.name: menu1_p1_b4
*menu1_p1_b4.labelString: "unzoom"
*menu1_p1_b4.background: "green"
*menu1_p1_b4.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*menu1_p1_b4.foreground: "black"
*menu1_p1_b4.activateCallback: unzoom_spectrum();

*menu1_p1_b5.class: pushButton
*menu1_p1_b5.parent: menu1_p1
*menu1_p1_b5.static: true
*menu1_p1_b5.name: menu1_p1_b5
*menu1_p1_b5.labelString: "get cursor"
*menu1_p1_b5.background: "green"
*menu1_p1_b5.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*menu1_p1_b5.foreground: "black"
*menu1_p1_b5.activateCallback: getcur_spectrum();

*menu1_top_b1.class: cascadeButton
*menu1_top_b1.parent: menu1
*menu1_top_b1.static: true
*menu1_top_b1.name: menu1_top_b1
*menu1_top_b1.labelString: "frame"
*menu1_top_b1.subMenuId: "menu1_p1"
*menu1_top_b1.background: "green"
*menu1_top_b1.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*menu1_top_b1.height: 21
*menu1_top_b1.x: 5
*menu1_top_b1.marginHeight: 1
*menu1_top_b1.marginWidth: 6
*menu1_top_b1.marginLeft: 8
*menu1_top_b1.marginRight: 8
*menu1_top_b1.highlightOnEnter: "true"
*menu1_top_b1.foreground: "black"

*menu2.class: rowColumn
*menu2.parent: form2
*menu2.static: true
*menu2.name: menu2
*menu2.rowColumnType: "menu_bar"
*menu2.y: 40
*menu2.menuAccelerator: "<KeyUp>F10"
*menu2.background: "lightblue"
*menu2.x: 95
*menu2.height: 30
*menu2.width: 85

*menu1_p2.class: rowColumn
*menu1_p2.parent: menu2
*menu1_p2.static: true
*menu1_p2.name: menu1_p2
*menu1_p2.rowColumnType: "menu_pulldown"

*menu1_p1_b6.class: pushButton
*menu1_p1_b6.parent: menu1_p2
*menu1_p1_b6.static: true
*menu1_p1_b6.name: menu1_p1_b6
*menu1_p1_b6.labelString: "create graphics"
*menu1_p1_b6.activateCallback: AppendDialogText("graph/spec");
*menu1_p1_b6.background: "lightblue"
*menu1_p1_b6.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"

*menu1_p1_b7.class: pushButton
*menu1_p1_b7.parent: menu1_p2
*menu1_p1_b7.static: true
*menu1_p1_b7.name: menu1_p1_b7
*menu1_p1_b7.labelString: "create display"
*menu1_p1_b7.activateCallback: AppendDialogText("crea/graphic");
*menu1_p1_b7.background: "lightblue"
*menu1_p1_b7.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"

*menu1_p1_b8.class: pushButton
*menu1_p1_b8.parent: menu1_p2
*menu1_p1_b8.static: true
*menu1_p1_b8.name: menu1_p1_b8
*menu1_p1_b8.labelString: "reset disp & graph"
*menu1_p1_b8.activateCallback: AppendDialogText("reset/display");
*menu1_p1_b8.background: "lightblue"
*menu1_p1_b8.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"

*menu1_p1_b9.class: pushButton
*menu1_p1_b9.parent: menu1_p2
*menu1_p1_b9.static: true
*menu1_p1_b9.name: menu1_p1_b9
*menu1_p1_b9.labelString: "get MIDAS table"
*menu1_p1_b9.background: "lightblue"
*menu1_p1_b9.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"

*menu1_p1_b10.class: pushButton
*menu1_p1_b10.parent: menu1_p2
*menu1_p1_b10.static: true
*menu1_p1_b10.name: menu1_p1_b10
*menu1_p1_b10.labelString: "print graphics"
*menu1_p1_b10.activateCallback: AppendDialogText("copy/graphic");
*menu1_p1_b10.background: "lightblue"
*menu1_p1_b10.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"

*menu1_top_b2.class: cascadeButton
*menu1_top_b2.parent: menu2
*menu1_top_b2.static: true
*menu1_top_b2.name: menu1_top_b2
*menu1_top_b2.labelString: "utils"
*menu1_top_b2.subMenuId: "menu1_p2"
*menu1_top_b2.background: "lightblue"
*menu1_top_b2.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*menu1_top_b2.height: 21
*menu1_top_b2.x: 5
*menu1_top_b2.marginHeight: 1
*menu1_top_b2.marginWidth: 6
*menu1_top_b2.marginLeft: 8
*menu1_top_b2.marginRight: 8
*menu1_top_b2.highlightOnEnter: "true"

*label1.class: label
*label1.parent: form1
*label1.static: true
*label1.name: label1
*label1.x: 0
*label1.y: 0
*label1.width: 365
*label1.height: 35
*label1.fontList: "-misc-fixed-bold-r-normal--13-120-75-75-c-80-iso8859-1"
*label1.labelString: "   Wavelength      Intensity       ION"
*label1.alignment: "alignment_beginning"
*label1.background: "grey90"
*label1.foreground: "black"

*scrolledWindow1.class: scrolledWindow
*scrolledWindow1.parent: form1
*scrolledWindow1.static: true
*scrolledWindow1.name: scrolledWindow1
*scrolledWindow1.scrollingPolicy: "application_defined"
*scrolledWindow1.x: 0
*scrolledWindow1.y: 35
*scrolledWindow1.visualPolicy: "variable"
*scrolledWindow1.scrollBarDisplayPolicy: "static"
*scrolledWindow1.shadowThickness: 0
*scrolledWindow1.scrollBarPlacement: "bottom_left"
*scrolledWindow1.background: "grey96"

*ls_lincat.class: scrolledList
*ls_lincat.parent: scrolledWindow1
*ls_lincat.static: true
*ls_lincat.name: ls_lincat
*ls_lincat.width: 346
*ls_lincat.height: 295
*ls_lincat.listSizePolicy: "constant"
*ls_lincat.scrollBarDisplayPolicy: "static"
*ls_lincat.visibleItemCount: 20
*ls_lincat.background: "grey96"
*ls_lincat.createCallback: {\
display_lincat_table(UxWidget);\
}
*ls_lincat.browseSelectionCallback: {\
char *choice;\
int i;\
float item_value;\
XmListCallbackStruct *cbs;\
\
extern int NumLincatPos, NumWlcPos;\
extern float LincatPos[];\
\
cbs = (XmListCallbackStruct *)UxCallbackArg;\
\
if ( NumLincatPos == NumWlcPos ) /* Noclick in graphic window */\
    return;\
\
XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);\
\
sscanf( choice, "%f", &item_value );\
\
for ( i = NumLincatPos-1; i >= 0; i-- )\
    if ( item_value == LincatPos[i] ) /* value already selected */\
        return;\
\
LincatPos[NumLincatPos] = item_value;\
NumLincatPos++;\
\
ident_line();       /* Ident wavelength in line table */\
get_wlc_value();    /* let's go to graphic window again */\
\
XtFree(choice);\
}

