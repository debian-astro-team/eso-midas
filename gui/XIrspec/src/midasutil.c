/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       midasutil.c                                */
/* .AUTHORS     Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    XIrspec                                    */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Creation  27-OCT-1993  
    090828	last modif			           */
/* ------------------------------------------------------- */

#include <gl_defs.h>
#include <xm_defs.h>
#include <midas_def.h>
#include <irspec_defs.h>
#include <irspec_comm.h>

#include <UxLib.h>


extern int AppendDialogText(), exist_descriptor(), file_exists();

extern void WriteKeyword();



void ReadDefaultsIrspec()
{
}

void DisplayParamsIrspec()
{
}

void InitIrspec()
{
    ReadDefaultsIrspec();
    DisplayParamsIrspec();
}

int GetRespCoords( frame, data )
char *frame;
float data[];
{
    int nulval, actval, id;
    int unit;

    if ( !exist_descriptor(frame, "IRS_YROWS") )
	return(FALSE);

    SCFOPN(frame, D_R4_FORMAT, 0, F_IMA_TYPE, &id);
    SCDRDR(id, "IRS_YROWS", 1, 4, &actval, data, &unit, &nulval);
    SCFCLO(id);

    return(TRUE);
}

int GetCoords( coorfil, data, ncoords )
char coorfil[];
int data[];
int ncoords;
{
    int nulval, sortcol, aw, ar, ncols, nrows;
    char str[MAXLINE];
    int aux, i, id, col_yw;

    if ( ! file_exists( coorfil, ".tbl" ) ) {  /* do nothing */
	sprintf(str, "*** Error: table %s could not be opened ***", coorfil);
	SCTPUT(str);
        return FALSE;
    }

    TCTOPN(coorfil, F_I_MODE, &id);

    TCIGET(id, &ncols, &nrows, &sortcol, &aw, &ar);
    if ( nrows < ncoords ) {
	sprintf(str, "*** Error: only %d points given ***", nrows);
	SCTPUT(str);
    	TCTCLO(id);
        return FALSE;
    }
    TCCSER(id, ":y_wcoord", &col_yw);
    if ( col_yw == -1 ) {
	SCTPUT("*** Error: missing column :y_wcoord in 'COORTAB' ***");
    	TCTCLO(id);
        return FALSE;
    }

    for ( i = 0; i < ncoords; i++ )
        TCERDI(id, i+1, col_yw, &data[i], &nulval);

    /* Now order from less to greater by pairs */
    for ( i = 0; i < ncoords - 1; i += 2 )
	if ( data[i] > data[i+1] ) {
	    aux = data[i+1];
	    data[i+1] = data[i];
	    data[i] = aux;
	}

    TCTCLO(id);

    return TRUE;
}

void WriteKeyword( text, keyword_cmd )
char *text;
char *keyword_cmd;
{
    char command[MAXLINE];

    sprintf(command, "%s%s", keyword_cmd, text);
    AppendDialogText(command);
}

/****************************************************************************
 exist_descriptor(): checks if the descriptor 'desc' exists in 'image'.
*/
int exist_descriptor( image, desc )
char *image;    /* name of the image */
char *desc;     /* descriptor name */
{
    int nulval, id;
    char type;

    if ( file_exists( image, ".bdf" ) ) {
        SCFOPN( image, D_R4_FORMAT, 0, F_IMA_TYPE, &id );
        SCDFND( id, desc, &type, &nulval, &nulval );
        if ( type != ' ' ) {
            SCFCLO(id);
            return(TRUE);
        }
        SCFCLO(id);
    }
    return(FALSE);
}
