/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	FluxShell.c

.VERSION
 090828         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTextF.h"
#include "UxPushBG.h"
#include "UxSepG.h"
#include "UxLabel.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxObjectShell;
	swidget	Uxform10;
	swidget	Uxform11;
	swidget	UxpushButton18;
	swidget	Uxpb_object_flux;
	swidget	Uxpb_object_reduce;
	swidget	Uxshelp_object;
	swidget	Uxseparator8;
	swidget	Uxseparator9;
	swidget	UxrowColumn5;
	swidget	Uxrb_obj_std;
	swidget	Uxrb_obj_high;
	swidget	Uxlabel34;
	swidget	UxseparatorGadget21;
	swidget	UxseparatorGadget22;
	swidget	UxseparatorGadget25;
	swidget	Uxmenu2_p4;
	swidget	Uxmn_obj_micron;
	swidget	Uxmn_obj_angstrom;
	swidget	Uxmn_obj_unit;
	swidget	Uxlabel21;
	swidget	Uxtf_obj_resp;
	swidget	Uxtf_obj_ref;
	swidget	Uxlb_obj_ref;
	swidget	Uxlabel28;
	swidget	Uxlabel29;
	swidget	Uxtf_obj_factor;
	swidget	Uxtf_obj_shift;
	swidget	Uxlabel30;
	swidget	Uxlabel31;
	swidget	Uxtf_obj_deltax;
	swidget	Uxlabel32;
	swidget	Uxtf_obj_deltay;
	swidget	Uxlabel33;
	swidget	Uxlabel35;
	swidget	Uxtf_obj_finput;
	swidget	Uxlabel36;
	swidget	Uxtf_obj_foutput;
	swidget	Uxlabel37;
	swidget	Uxtg_obj_sky_zero;
	swidget	Uxtf_obj_sky_table;
	swidget	Uxtg_obj_debug;
	swidget	Uxlabel38;
	swidget	Uxtf_obj_cuts1;
	swidget	Uxtf_obj_cuts2;
} _UxCObjectShell;

#define ObjectShell             UxObjectShellContext->UxObjectShell
#define form10                  UxObjectShellContext->Uxform10
#define form11                  UxObjectShellContext->Uxform11
#define pushButton18            UxObjectShellContext->UxpushButton18
#define pb_object_flux          UxObjectShellContext->Uxpb_object_flux
#define pb_object_reduce        UxObjectShellContext->Uxpb_object_reduce
#define shelp_object            UxObjectShellContext->Uxshelp_object
#define separator8              UxObjectShellContext->Uxseparator8
#define separator9              UxObjectShellContext->Uxseparator9
#define rowColumn5              UxObjectShellContext->UxrowColumn5
#define rb_obj_std              UxObjectShellContext->Uxrb_obj_std
#define rb_obj_high             UxObjectShellContext->Uxrb_obj_high
#define label34                 UxObjectShellContext->Uxlabel34
#define separatorGadget21       UxObjectShellContext->UxseparatorGadget21
#define separatorGadget22       UxObjectShellContext->UxseparatorGadget22
#define separatorGadget25       UxObjectShellContext->UxseparatorGadget25
#define menu2_p4                UxObjectShellContext->Uxmenu2_p4
#define mn_obj_micron           UxObjectShellContext->Uxmn_obj_micron
#define mn_obj_angstrom         UxObjectShellContext->Uxmn_obj_angstrom
#define mn_obj_unit             UxObjectShellContext->Uxmn_obj_unit
#define label21                 UxObjectShellContext->Uxlabel21
#define tf_obj_resp             UxObjectShellContext->Uxtf_obj_resp
#define tf_obj_ref              UxObjectShellContext->Uxtf_obj_ref
#define lb_obj_ref              UxObjectShellContext->Uxlb_obj_ref
#define label28                 UxObjectShellContext->Uxlabel28
#define label29                 UxObjectShellContext->Uxlabel29
#define tf_obj_factor           UxObjectShellContext->Uxtf_obj_factor
#define tf_obj_shift            UxObjectShellContext->Uxtf_obj_shift
#define label30                 UxObjectShellContext->Uxlabel30
#define label31                 UxObjectShellContext->Uxlabel31
#define tf_obj_deltax           UxObjectShellContext->Uxtf_obj_deltax
#define label32                 UxObjectShellContext->Uxlabel32
#define tf_obj_deltay           UxObjectShellContext->Uxtf_obj_deltay
#define label33                 UxObjectShellContext->Uxlabel33
#define label35                 UxObjectShellContext->Uxlabel35
#define tf_obj_finput           UxObjectShellContext->Uxtf_obj_finput
#define label36                 UxObjectShellContext->Uxlabel36
#define tf_obj_foutput          UxObjectShellContext->Uxtf_obj_foutput
#define label37                 UxObjectShellContext->Uxlabel37
#define tg_obj_sky_zero         UxObjectShellContext->Uxtg_obj_sky_zero
#define tf_obj_sky_table        UxObjectShellContext->Uxtf_obj_sky_table
#define tg_obj_debug            UxObjectShellContext->Uxtg_obj_debug
#define label38                 UxObjectShellContext->Uxlabel38
#define tf_obj_cuts1            UxObjectShellContext->Uxtf_obj_cuts1
#define tf_obj_cuts2            UxObjectShellContext->Uxtf_obj_cuts2

static _UxCObjectShell	*UxObjectShellContext;

extern void SetFileList(), ObjectFluxCallback(), ObjectReduceCallback();



/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*SelectFileObject = "#override\n\
<Btn3Down>:FileSelectACT()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ObjectShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCObjectShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxObjectShellContext;
	UxObjectShellContext = UxContext =
			(_UxCObjectShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <irspec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	strcpy(DirSpecs, "*.bdf");
		 
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_object")) ) {
	    SET_LIST_TITLE("Enter object frame");
	    ListType = LIST_OBJECT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_sky")) ) {
	    SET_LIST_TITLE("Enter sky frame");
	    ListType = LIST_SKY;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_ststar")) ) {
	    SET_LIST_TITLE("Enter standard star");
	    ListType = LIST_STSTAR;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_stsky")) )  {
	    SET_LIST_TITLE("Enter standard sky");
	    ListType = LIST_STSKY;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_dark")) ) {
	    SET_LIST_TITLE("Enter dark frame");
	    ListType = LIST_DARK;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_flat")) )  {
	    SET_LIST_TITLE("Enter flat-field frame");
	    ListType = LIST_FLAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_std_flux")) )  {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX;
	    strcpy(DirSpecs, "*.tbl");
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_std_ref")) )  {
	    SET_LIST_TITLE("Enter reference frame");
	    ListType = LIST_STD_REF;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_ref")) )  {
	    SET_LIST_TITLE("Enter reference frame");
	    ListType = LIST_OBJ_REF;
	} 
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_finput")) )  {
	    SET_LIST_TITLE("Enter flux input frame");
	    ListType = LIST_OBJ_FINPUT;
	} 
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_resp")) )  {
	    SET_LIST_TITLE("Enter response frame");
	    ListType = LIST_RESP;
	}
	  
	FileListWidget = UxGetWidget(UxFindSwidget("sl_file_list"));
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxObjectShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCObjectShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxObjectShellContext;
	UxObjectShellContext = UxContext =
			(_UxCObjectShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("ObjectShell"));
	}
	UxObjectShellContext = UxSaveCtx;
}

static void	activateCB_pb_object_flux( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCObjectShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxObjectShellContext;
	UxObjectShellContext = UxContext =
			(_UxCObjectShell *) UxGetContext( UxThisWidget );
	{
	ObjectFluxCallback();
	}
	UxObjectShellContext = UxSaveCtx;
}

static void	activateCB_pb_object_reduce( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCObjectShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxObjectShellContext;
	UxObjectShellContext = UxContext =
			(_UxCObjectShell *) UxGetContext( UxThisWidget );
	{
	ObjectReduceCallback();
	}
	UxObjectShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_obj_std( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCObjectShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxObjectShellContext;
	UxObjectShellContext = UxContext =
			(_UxCObjectShell *) UxGetContext( UxThisWidget );
	{
	if ( XmToggleButtonGetState(UxWidget) ) { /*selected */
	   XtSetSensitive(UxGetWidget(tf_obj_ref), FALSE);
	   XtSetSensitive(UxGetWidget(lb_obj_ref), FALSE);
	}
	else {
	   XtSetSensitive(UxGetWidget(tf_obj_ref), TRUE);
	   XtSetSensitive(UxGetWidget(lb_obj_ref), TRUE);
	}
	
	}
	UxObjectShellContext = UxSaveCtx;
}

static void	activateCB_mn_obj_micron( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCObjectShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxObjectShellContext;
	UxObjectShellContext = UxContext =
			(_UxCObjectShell *) UxGetContext( UxThisWidget );
	{
	}
	UxObjectShellContext = UxSaveCtx;
}

static void	activateCB_mn_obj_angstrom( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCObjectShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxObjectShellContext;
	UxObjectShellContext = UxContext =
			(_UxCObjectShell *) UxGetContext( UxThisWidget );
	{
	}
	UxObjectShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ObjectShell()
{
	UxPutIconName( ObjectShell, "ObjectShell" );
	UxPutBackground( ObjectShell, WindowBackground );
	UxPutGeometry( ObjectShell, "+10+60" );
	UxPutKeyboardFocusPolicy( ObjectShell, "pointer" );
	UxPutTitle( ObjectShell, "Object calibration" );
	UxPutHeight( ObjectShell, 602 );
	UxPutWidth( ObjectShell, 490 );
	UxPutY( ObjectShell, 79 );
	UxPutX( ObjectShell, 660 );

	UxPutLabelFontList( form10, TextFont );
	UxPutButtonFontList( form10, TextFont );
	UxPutBackground( form10, WindowBackground );
	UxPutHeight( form10, 288 );
	UxPutWidth( form10, 490 );
	UxPutY( form10, 0 );
	UxPutX( form10, 0 );
	UxPutUnitType( form10, "pixels" );
	UxPutResizePolicy( form10, "resize_none" );

	UxPutBackground( form11, ButtonBackground );
	UxPutHeight( form11, 48 );
	UxPutWidth( form11, 490 );
	UxPutY( form11, 552 );
	UxPutX( form11, -1 );
	UxPutResizePolicy( form11, "resize_none" );

	UxPutLabelString( pushButton18, "Return" );
	UxPutForeground( pushButton18, CancelForeground );
	UxPutFontList( pushButton18, BoldTextFont );
	UxPutBackground( pushButton18, ButtonBackground );
	UxPutHeight( pushButton18, 30 );
	UxPutWidth( pushButton18, 86 );
	UxPutY( pushButton18, 6 );
	UxPutX( pushButton18, 196 );

	UxPutLabelString( pb_object_flux, "Flux" );
	UxPutForeground( pb_object_flux, ButtonForeground );
	UxPutFontList( pb_object_flux, BoldTextFont );
	UxPutBackground( pb_object_flux, ButtonBackground );
	UxPutHeight( pb_object_flux, 30 );
	UxPutWidth( pb_object_flux, 86 );
	UxPutY( pb_object_flux, 6 );
	UxPutX( pb_object_flux, 102 );

	UxPutLabelString( pb_object_reduce, "Reduce" );
	UxPutForeground( pb_object_reduce, ButtonForeground );
	UxPutFontList( pb_object_reduce, BoldTextFont );
	UxPutBackground( pb_object_reduce, ButtonBackground );
	UxPutHeight( pb_object_reduce, 30 );
	UxPutWidth( pb_object_reduce, 86 );
	UxPutY( pb_object_reduce, 6 );
	UxPutX( pb_object_reduce, 8 );

	UxPutWordWrap( shelp_object, "false" );
	UxPutHighlightOnEnter( shelp_object, "false" );
	UxPutEditMode( shelp_object, "single_line_edit" );
	UxPutFontList( shelp_object, TextFont );
	UxPutEditable( shelp_object, "false" );
	UxPutCursorPositionVisible( shelp_object, "false" );
	UxPutBackground( shelp_object, SHelpBackground );
	UxPutHeight( shelp_object, 50 );
	UxPutWidth( shelp_object, 484 );
	UxPutY( shelp_object, 496 );
	UxPutX( shelp_object, 3 );

	UxPutBackground( separator8, WindowBackground );
	UxPutHeight( separator8, 6 );
	UxPutWidth( separator8, 492 );
	UxPutY( separator8, 492 );
	UxPutX( separator8, -1 );

	UxPutBackground( separator9, WindowBackground );
	UxPutHeight( separator9, 10 );
	UxPutWidth( separator9, 492 );
	UxPutY( separator9, 544 );
	UxPutX( separator9, -3 );

	UxPutIsAligned( rowColumn5, "true" );
	UxPutEntryAlignment( rowColumn5, "alignment_beginning" );
	UxPutBorderWidth( rowColumn5, 0 );
	UxPutShadowThickness( rowColumn5, 0 );
	UxPutLabelString( rowColumn5, "" );
	UxPutEntryBorder( rowColumn5, 0 );
	UxPutBackground( rowColumn5, "grey80" );
	UxPutRadioBehavior( rowColumn5, "true" );
	UxPutHeight( rowColumn5, 67 );
	UxPutWidth( rowColumn5, 82 );
	UxPutY( rowColumn5, 32 );
	UxPutX( rowColumn5, 58 );

	UxPutIndicatorSize( rb_obj_std, 16 );
	UxPutHighlightOnEnter( rb_obj_std, "true" );
	UxPutForeground( rb_obj_std, TextForeground );
	UxPutSelectColor( rb_obj_std, SelectColor );
	UxPutSet( rb_obj_std, "true" );
	UxPutLabelString( rb_obj_std, "Standard" );
	UxPutFontList( rb_obj_std, TextFont );
	UxPutBackground( rb_obj_std, WindowBackground );
	UxPutHeight( rb_obj_std, 20 );
	UxPutWidth( rb_obj_std, 76 );
	UxPutY( rb_obj_std, 0 );
	UxPutX( rb_obj_std, -12 );

	UxPutIndicatorSize( rb_obj_high, 16 );
	UxPutHighlightOnEnter( rb_obj_high, "true" );
	UxPutSet( rb_obj_high, "false" );
	UxPutForeground( rb_obj_high, TextForeground );
	UxPutSelectColor( rb_obj_high, SelectColor );
	UxPutLabelString( rb_obj_high, "High accuracy" );
	UxPutFontList( rb_obj_high, TextFont );
	UxPutBackground( rb_obj_high, WindowBackground );
	UxPutHeight( rb_obj_high, 66 );
	UxPutWidth( rb_obj_high, 76 );
	UxPutY( rb_obj_high, 34 );
	UxPutX( rb_obj_high, 3 );

	UxPutForeground( label34, TextForeground );
	UxPutAlignment( label34, "alignment_beginning" );
	UxPutLabelString( label34, "Wavelength calibration" );
	UxPutFontList( label34, TextFont );
	UxPutBackground( label34, WindowBackground );
	UxPutHeight( label34, 22 );
	UxPutWidth( label34, 160 );
	UxPutY( label34, 8 );
	UxPutX( label34, 42 );

	UxPutOrientation( separatorGadget21, "vertical" );
	UxPutHeight( separatorGadget21, 84 );
	UxPutWidth( separatorGadget21, 12 );
	UxPutY( separatorGadget21, 20 );
	UxPutX( separatorGadget21, 29 );

	UxPutOrientation( separatorGadget22, "vertical" );
	UxPutHeight( separatorGadget22, 84 );
	UxPutWidth( separatorGadget22, 12 );
	UxPutY( separatorGadget22, 20 );
	UxPutX( separatorGadget22, 204 );

	UxPutHeight( separatorGadget25, 8 );
	UxPutWidth( separatorGadget25, 178 );
	UxPutY( separatorGadget25, 100 );
	UxPutX( separatorGadget25, 34 );

	UxPutForeground( menu2_p4, TextForeground );
	UxPutBackground( menu2_p4, WindowBackground );
	UxPutRowColumnType( menu2_p4, "menu_pulldown" );

	UxPutFontList( mn_obj_micron, TextFont );
	UxPutLabelString( mn_obj_micron, "microns" );

	UxPutFontList( mn_obj_angstrom, TextFont );
	UxPutLabelString( mn_obj_angstrom, "angstroms" );

	UxPutLabelString( mn_obj_unit, "Units : " );
	UxPutSpacing( mn_obj_unit, 0 );
	UxPutMarginWidth( mn_obj_unit, 0 );
	UxPutForeground( mn_obj_unit, TextForeground );
	UxPutBackground( mn_obj_unit, WindowBackground );
	UxPutY( mn_obj_unit, 22 );
	UxPutX( mn_obj_unit, 231 );
	UxPutRowColumnType( mn_obj_unit, "menu_option" );

	UxPutForeground( label21, TextForeground );
	UxPutAlignment( label21, "alignment_beginning" );
	UxPutLabelString( label21, "Response frame :" );
	UxPutFontList( label21, TextFont );
	UxPutBackground( label21, LabelBackground );
	UxPutHeight( label21, 30 );
	UxPutWidth( label21, 122 );
	UxPutY( label21, 410 );
	UxPutX( label21, 14 );

	UxPutTranslations( tf_obj_resp, SelectFileObject );
	UxPutForeground( tf_obj_resp, TextForeground );
	UxPutHighlightOnEnter( tf_obj_resp, "true" );
	UxPutFontList( tf_obj_resp, TextFont );
	UxPutBackground( tf_obj_resp, TextBackground );
	UxPutHeight( tf_obj_resp, 34 );
	UxPutWidth( tf_obj_resp, 221 );
	UxPutY( tf_obj_resp, 406 );
	UxPutX( tf_obj_resp, 146 );

	UxPutSensitive( tf_obj_ref, "false" );
	UxPutTranslations( tf_obj_ref, SelectFileObject );
	UxPutForeground( tf_obj_ref, TextForeground );
	UxPutHighlightOnEnter( tf_obj_ref, "true" );
	UxPutFontList( tf_obj_ref, TextFont );
	UxPutBackground( tf_obj_ref, TextBackground );
	UxPutHeight( tf_obj_ref, 34 );
	UxPutWidth( tf_obj_ref, 221 );
	UxPutY( tf_obj_ref, 114 );
	UxPutX( tf_obj_ref, 148 );

	UxPutSensitive( lb_obj_ref, "false" );
	UxPutForeground( lb_obj_ref, TextForeground );
	UxPutAlignment( lb_obj_ref, "alignment_beginning" );
	UxPutLabelString( lb_obj_ref, "Reference frame :" );
	UxPutFontList( lb_obj_ref, TextFont );
	UxPutBackground( lb_obj_ref, LabelBackground );
	UxPutHeight( lb_obj_ref, 30 );
	UxPutWidth( lb_obj_ref, 122 );
	UxPutY( lb_obj_ref, 120 );
	UxPutX( lb_obj_ref, 18 );

	UxPutForeground( label28, TextForeground );
	UxPutAlignment( label28, "alignment_beginning" );
	UxPutLabelString( label28, "SKY SUBTRACTION PARAMETERS" );
	UxPutFontList( label28, TextFont );
	UxPutBackground( label28, LabelBackground );
	UxPutHeight( label28, 30 );
	UxPutWidth( label28, 246 );
	UxPutY( label28, 161 );
	UxPutX( label28, 17 );

	UxPutForeground( label29, TextForeground );
	UxPutAlignment( label29, "alignment_beginning" );
	UxPutLabelString( label29, "Factor :" );
	UxPutFontList( label29, TextFont );
	UxPutBackground( label29, LabelBackground );
	UxPutHeight( label29, 30 );
	UxPutWidth( label29, 54 );
	UxPutY( label29, 198 );
	UxPutX( label29, 16 );

	UxPutText( tf_obj_factor, "1" );
	UxPutMaxLength( tf_obj_factor, 5 );
	UxPutForeground( tf_obj_factor, TextForeground );
	UxPutHighlightOnEnter( tf_obj_factor, "true" );
	UxPutFontList( tf_obj_factor, TextFont );
	UxPutBackground( tf_obj_factor, TextBackground );
	UxPutHeight( tf_obj_factor, 34 );
	UxPutWidth( tf_obj_factor, 56 );
	UxPutY( tf_obj_factor, 196 );
	UxPutX( tf_obj_factor, 70 );

	UxPutMaxLength( tf_obj_shift, 5 );
	UxPutForeground( tf_obj_shift, TextForeground );
	UxPutHighlightOnEnter( tf_obj_shift, "true" );
	UxPutFontList( tf_obj_shift, TextFont );
	UxPutBackground( tf_obj_shift, TextBackground );
	UxPutHeight( tf_obj_shift, 34 );
	UxPutWidth( tf_obj_shift, 56 );
	UxPutY( tf_obj_shift, 196 );
	UxPutX( tf_obj_shift, 178 );

	UxPutForeground( label30, TextForeground );
	UxPutAlignment( label30, "alignment_beginning" );
	UxPutLabelString( label30, "Shift :" );
	UxPutFontList( label30, TextFont );
	UxPutBackground( label30, LabelBackground );
	UxPutHeight( label30, 30 );
	UxPutWidth( label30, 46 );
	UxPutY( label30, 198 );
	UxPutX( label30, 136 );

	UxPutForeground( label31, TextForeground );
	UxPutAlignment( label31, "alignment_beginning" );
	UxPutLabelString( label31, "Deltax :" );
	UxPutFontList( label31, TextFont );
	UxPutBackground( label31, LabelBackground );
	UxPutHeight( label31, 30 );
	UxPutWidth( label31, 58 );
	UxPutY( label31, 198 );
	UxPutX( label31, 242 );

	UxPutMaxLength( tf_obj_deltax, 5 );
	UxPutForeground( tf_obj_deltax, TextForeground );
	UxPutHighlightOnEnter( tf_obj_deltax, "true" );
	UxPutFontList( tf_obj_deltax, TextFont );
	UxPutBackground( tf_obj_deltax, TextBackground );
	UxPutHeight( tf_obj_deltax, 34 );
	UxPutWidth( tf_obj_deltax, 56 );
	UxPutY( tf_obj_deltax, 196 );
	UxPutX( tf_obj_deltax, 300 );

	UxPutForeground( label32, TextForeground );
	UxPutAlignment( label32, "alignment_beginning" );
	UxPutLabelString( label32, "Deltay :" );
	UxPutFontList( label32, TextFont );
	UxPutBackground( label32, LabelBackground );
	UxPutHeight( label32, 30 );
	UxPutWidth( label32, 58 );
	UxPutY( label32, 198 );
	UxPutX( label32, 362 );

	UxPutMaxLength( tf_obj_deltay, 5 );
	UxPutForeground( tf_obj_deltay, TextForeground );
	UxPutHighlightOnEnter( tf_obj_deltay, "true" );
	UxPutFontList( tf_obj_deltay, TextFont );
	UxPutBackground( tf_obj_deltay, TextBackground );
	UxPutHeight( tf_obj_deltay, 34 );
	UxPutWidth( tf_obj_deltay, 56 );
	UxPutY( tf_obj_deltay, 196 );
	UxPutX( tf_obj_deltay, 420 );

	UxPutForeground( label33, TextForeground );
	UxPutAlignment( label33, "alignment_beginning" );
	UxPutLabelString( label33, "FLUX CALIBRATION PARAMETERS" );
	UxPutFontList( label33, TextFont );
	UxPutBackground( label33, LabelBackground );
	UxPutHeight( label33, 30 );
	UxPutWidth( label33, 246 );
	UxPutY( label33, 334 );
	UxPutX( label33, 17 );

	UxPutForeground( label35, TextForeground );
	UxPutAlignment( label35, "alignment_beginning" );
	UxPutLabelString( label35, "Input frame :" );
	UxPutFontList( label35, TextFont );
	UxPutBackground( label35, LabelBackground );
	UxPutHeight( label35, 30 );
	UxPutWidth( label35, 122 );
	UxPutY( label35, 372 );
	UxPutX( label35, 14 );

	UxPutTranslations( tf_obj_finput, SelectFileObject );
	UxPutForeground( tf_obj_finput, TextForeground );
	UxPutHighlightOnEnter( tf_obj_finput, "true" );
	UxPutFontList( tf_obj_finput, TextFont );
	UxPutBackground( tf_obj_finput, TextBackground );
	UxPutHeight( tf_obj_finput, 34 );
	UxPutWidth( tf_obj_finput, 221 );
	UxPutY( tf_obj_finput, 368 );
	UxPutX( tf_obj_finput, 146 );

	UxPutForeground( label36, TextForeground );
	UxPutAlignment( label36, "alignment_beginning" );
	UxPutLabelString( label36, "Output frame :" );
	UxPutFontList( label36, TextFont );
	UxPutBackground( label36, LabelBackground );
	UxPutHeight( label36, 30 );
	UxPutWidth( label36, 122 );
	UxPutY( label36, 450 );
	UxPutX( label36, 14 );

	UxPutTranslations( tf_obj_foutput, SelectFileObject );
	UxPutForeground( tf_obj_foutput, TextForeground );
	UxPutHighlightOnEnter( tf_obj_foutput, "true" );
	UxPutFontList( tf_obj_foutput, TextFont );
	UxPutBackground( tf_obj_foutput, TextBackground );
	UxPutHeight( tf_obj_foutput, 34 );
	UxPutWidth( tf_obj_foutput, 221 );
	UxPutY( tf_obj_foutput, 446 );
	UxPutX( tf_obj_foutput, 146 );

	UxPutForeground( label37, TextForeground );
	UxPutAlignment( label37, "alignment_beginning" );
	UxPutLabelString( label37, "Sky table :" );
	UxPutFontList( label37, TextFont );
	UxPutBackground( label37, LabelBackground );
	UxPutHeight( label37, 30 );
	UxPutWidth( label37, 78 );
	UxPutY( label37, 238 );
	UxPutX( label37, 16 );

	UxPutIndicatorSize( tg_obj_sky_zero, 16 );
	UxPutSensitive( tg_obj_sky_zero, "true" );
	UxPutHighlightOnEnter( tg_obj_sky_zero, "true" );
	UxPutForeground( tg_obj_sky_zero, TextForeground );
	UxPutSelectColor( tg_obj_sky_zero, SelectColor );
	UxPutSet( tg_obj_sky_zero, "false" );
	UxPutLabelString( tg_obj_sky_zero, "Force sky to zero" );
	UxPutFontList( tg_obj_sky_zero, TextFont );
	UxPutBackground( tg_obj_sky_zero, WindowBackground );
	UxPutHeight( tg_obj_sky_zero, 31 );
	UxPutWidth( tg_obj_sky_zero, 143 );
	UxPutY( tg_obj_sky_zero, 276 );
	UxPutX( tg_obj_sky_zero, 14 );

	UxPutText( tf_obj_sky_table, "cursor" );
	UxPutTranslations( tf_obj_sky_table, SelectFileObject );
	UxPutForeground( tf_obj_sky_table, TextForeground );
	UxPutHighlightOnEnter( tf_obj_sky_table, "true" );
	UxPutFontList( tf_obj_sky_table, TextFont );
	UxPutBackground( tf_obj_sky_table, TextBackground );
	UxPutHeight( tf_obj_sky_table, 34 );
	UxPutWidth( tf_obj_sky_table, 171 );
	UxPutY( tf_obj_sky_table, 236 );
	UxPutX( tf_obj_sky_table, 93 );

	UxPutIndicatorSize( tg_obj_debug, 16 );
	UxPutSensitive( tg_obj_debug, "true" );
	UxPutHighlightOnEnter( tg_obj_debug, "true" );
	UxPutForeground( tg_obj_debug, TextForeground );
	UxPutSelectColor( tg_obj_debug, SelectColor );
	UxPutSet( tg_obj_debug, "false" );
	UxPutLabelString( tg_obj_debug, "Debug mode" );
	UxPutFontList( tg_obj_debug, TextFont );
	UxPutBackground( tg_obj_debug, WindowBackground );
	UxPutHeight( tg_obj_debug, 31 );
	UxPutWidth( tg_obj_debug, 112 );
	UxPutY( tg_obj_debug, 276 );
	UxPutX( tg_obj_debug, 174 );

	UxPutForeground( label38, TextForeground );
	UxPutAlignment( label38, "alignment_beginning" );
	UxPutLabelString( label38, "Cuts :" );
	UxPutFontList( label38, TextFont );
	UxPutBackground( label38, LabelBackground );
	UxPutHeight( label38, 30 );
	UxPutWidth( label38, 48 );
	UxPutY( label38, 238 );
	UxPutX( label38, 275 );

	UxPutText( tf_obj_cuts1, "0" );
	UxPutMaxLength( tf_obj_cuts1, 5 );
	UxPutForeground( tf_obj_cuts1, TextForeground );
	UxPutHighlightOnEnter( tf_obj_cuts1, "true" );
	UxPutFontList( tf_obj_cuts1, TextFont );
	UxPutBackground( tf_obj_cuts1, TextBackground );
	UxPutHeight( tf_obj_cuts1, 34 );
	UxPutWidth( tf_obj_cuts1, 56 );
	UxPutY( tf_obj_cuts1, 236 );
	UxPutX( tf_obj_cuts1, 323 );

	UxPutText( tf_obj_cuts2, "0" );
	UxPutMaxLength( tf_obj_cuts2, 5 );
	UxPutForeground( tf_obj_cuts2, TextForeground );
	UxPutHighlightOnEnter( tf_obj_cuts2, "true" );
	UxPutFontList( tf_obj_cuts2, TextFont );
	UxPutBackground( tf_obj_cuts2, TextBackground );
	UxPutHeight( tf_obj_cuts2, 34 );
	UxPutWidth( tf_obj_cuts2, 56 );
	UxPutY( tf_obj_cuts2, 236 );
	UxPutX( tf_obj_cuts2, 383 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ObjectShell()
{
	/* Create the swidgets */

	ObjectShell = UxCreateApplicationShell( "ObjectShell", NO_PARENT );
	UxPutContext( ObjectShell, UxObjectShellContext );

	form10 = UxCreateForm( "form10", ObjectShell );
	form11 = UxCreateForm( "form11", form10 );
	pushButton18 = UxCreatePushButton( "pushButton18", form11 );
	pb_object_flux = UxCreatePushButton( "pb_object_flux", form11 );
	pb_object_reduce = UxCreatePushButton( "pb_object_reduce", form11 );
	shelp_object = UxCreateText( "shelp_object", form10 );
	separator8 = UxCreateSeparator( "separator8", form10 );
	separator9 = UxCreateSeparator( "separator9", form10 );
	rowColumn5 = UxCreateRowColumn( "rowColumn5", form10 );
	rb_obj_std = UxCreateToggleButton( "rb_obj_std", rowColumn5 );
	rb_obj_high = UxCreateToggleButton( "rb_obj_high", rowColumn5 );
	label34 = UxCreateLabel( "label34", form10 );
	separatorGadget21 = UxCreateSeparatorGadget( "separatorGadget21", form10 );
	separatorGadget22 = UxCreateSeparatorGadget( "separatorGadget22", form10 );
	separatorGadget25 = UxCreateSeparatorGadget( "separatorGadget25", form10 );
	menu2_p4 = UxCreateRowColumn( "menu2_p4", form10 );
	mn_obj_micron = UxCreatePushButtonGadget( "mn_obj_micron", menu2_p4 );
	mn_obj_angstrom = UxCreatePushButtonGadget( "mn_obj_angstrom", menu2_p4 );
	mn_obj_unit = UxCreateRowColumn( "mn_obj_unit", form10 );
	label21 = UxCreateLabel( "label21", form10 );
	tf_obj_resp = UxCreateTextField( "tf_obj_resp", form10 );
	tf_obj_ref = UxCreateTextField( "tf_obj_ref", form10 );
	lb_obj_ref = UxCreateLabel( "lb_obj_ref", form10 );
	label28 = UxCreateLabel( "label28", form10 );
	label29 = UxCreateLabel( "label29", form10 );
	tf_obj_factor = UxCreateTextField( "tf_obj_factor", form10 );
	tf_obj_shift = UxCreateTextField( "tf_obj_shift", form10 );
	label30 = UxCreateLabel( "label30", form10 );
	label31 = UxCreateLabel( "label31", form10 );
	tf_obj_deltax = UxCreateTextField( "tf_obj_deltax", form10 );
	label32 = UxCreateLabel( "label32", form10 );
	tf_obj_deltay = UxCreateTextField( "tf_obj_deltay", form10 );
	label33 = UxCreateLabel( "label33", form10 );
	label35 = UxCreateLabel( "label35", form10 );
	tf_obj_finput = UxCreateTextField( "tf_obj_finput", form10 );
	label36 = UxCreateLabel( "label36", form10 );
	tf_obj_foutput = UxCreateTextField( "tf_obj_foutput", form10 );
	label37 = UxCreateLabel( "label37", form10 );
	tg_obj_sky_zero = UxCreateToggleButton( "tg_obj_sky_zero", form10 );
	tf_obj_sky_table = UxCreateTextField( "tf_obj_sky_table", form10 );
	tg_obj_debug = UxCreateToggleButton( "tg_obj_debug", form10 );
	label38 = UxCreateLabel( "label38", form10 );
	tf_obj_cuts1 = UxCreateTextField( "tf_obj_cuts1", form10 );
	tf_obj_cuts2 = UxCreateTextField( "tf_obj_cuts2", form10 );

	_Uxinit_ObjectShell();

	/* Create the X widgets */

	UxCreateWidget( ObjectShell );
	UxCreateWidget( form10 );
	UxCreateWidget( form11 );
	UxCreateWidget( pushButton18 );
	UxCreateWidget( pb_object_flux );
	UxCreateWidget( pb_object_reduce );
	UxCreateWidget( shelp_object );
	UxCreateWidget( separator8 );
	UxCreateWidget( separator9 );
	UxCreateWidget( rowColumn5 );
	UxCreateWidget( rb_obj_std );
	UxCreateWidget( rb_obj_high );
	UxCreateWidget( label34 );
	UxCreateWidget( separatorGadget21 );
	UxCreateWidget( separatorGadget22 );
	UxCreateWidget( separatorGadget25 );
	UxCreateWidget( menu2_p4 );
	UxCreateWidget( mn_obj_micron );
	UxCreateWidget( mn_obj_angstrom );
	UxPutSubMenuId( mn_obj_unit, "menu2_p4" );
	UxCreateWidget( mn_obj_unit );

	UxCreateWidget( label21 );
	UxCreateWidget( tf_obj_resp );
	UxCreateWidget( tf_obj_ref );
	UxCreateWidget( lb_obj_ref );
	UxCreateWidget( label28 );
	UxCreateWidget( label29 );
	UxCreateWidget( tf_obj_factor );
	UxCreateWidget( tf_obj_shift );
	UxCreateWidget( label30 );
	UxCreateWidget( label31 );
	UxCreateWidget( tf_obj_deltax );
	UxCreateWidget( label32 );
	UxCreateWidget( tf_obj_deltay );
	UxCreateWidget( label33 );
	UxCreateWidget( label35 );
	UxCreateWidget( tf_obj_finput );
	UxCreateWidget( label36 );
	UxCreateWidget( tf_obj_foutput );
	UxCreateWidget( label37 );
	UxCreateWidget( tg_obj_sky_zero );
	UxCreateWidget( tf_obj_sky_table );
	UxCreateWidget( tg_obj_debug );
	UxCreateWidget( label38 );
	UxCreateWidget( tf_obj_cuts1 );
	UxCreateWidget( tf_obj_cuts2 );

	UxAddCallback( pushButton18, XmNactivateCallback,
			activateCB_pushButton18,
			(XtPointer) UxObjectShellContext );

	UxAddCallback( pb_object_flux, XmNactivateCallback,
			activateCB_pb_object_flux,
			(XtPointer) UxObjectShellContext );

	UxAddCallback( pb_object_reduce, XmNactivateCallback,
			activateCB_pb_object_reduce,
			(XtPointer) UxObjectShellContext );

	UxAddCallback( rb_obj_std, XmNvalueChangedCallback,
			valueChangedCB_rb_obj_std,
			(XtPointer) UxObjectShellContext );

	UxAddCallback( mn_obj_micron, XmNactivateCallback,
			activateCB_mn_obj_micron,
			(XtPointer) UxObjectShellContext );

	UxAddCallback( mn_obj_angstrom, XmNactivateCallback,
			activateCB_mn_obj_angstrom,
			(XtPointer) UxObjectShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ObjectShell );

	return ( ObjectShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ObjectShell()
{
	swidget                 rtrn;
	_UxCObjectShell         *UxContext;

	UxObjectShellContext = UxContext =
		(_UxCObjectShell *) UxMalloc( sizeof(_UxCObjectShell) );

	rtrn = _Uxbuild_ObjectShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ObjectShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ObjectShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

