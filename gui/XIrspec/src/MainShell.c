/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	MainShell.c

.VERSION
 090828         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxLabel.h"
#include "UxTextF.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxTogBG.h"
#include "UxSepG.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxApplicWindow;
	swidget	UxMainWindow;
	swidget	Uxmenu1;
	swidget	Uxmenu1_p7;
	swidget	Uxmenu1_p7_b1;
	swidget	Uxmenu1_p7_b2;
	swidget	Uxmenu1_p7_b3;
	swidget	Uxmenu1_top_b1;
	swidget	Uxmenu1_p4;
	swidget	Uxmenu1_p4_b1;
	swidget	Uxmenu1_p4_b2;
	swidget	Uxmenu1_p4_b8;
	swidget	Uxmenu1_p4_b10;
	swidget	Uxmenu1_p4_b13;
	swidget	Uxmenu1_p4_b12;
	swidget	Uxmenu1_p4_b3;
	swidget	Uxmenu1_p4_b4;
	swidget	Uxmenu1_p4_b9;
	swidget	Uxmenu1_p4_b5;
	swidget	Uxmenu1_p4_b11;
	swidget	Uxmenu1_p4_b14;
	swidget	Uxmenu1_top_b4;
	swidget	Uxmenu1_p8;
	swidget	Uxtg_options_exit;
	swidget	Uxmenu1_top_b2;
	swidget	Uxmenu1_p5;
	swidget	Uxmenu_help_context;
	swidget	Uxmenu_help_help;
	swidget	Uxmenu1_p5_b5;
	swidget	Uxmenu_help_tutorial;
	swidget	Uxmenu_help_version;
	swidget	Uxmenu1_top_b5;
	swidget	Uxmenu1_p6;
	swidget	Uxmenu1_p6_b1;
	swidget	Uxmenu1_top_b6;
	swidget	Uxform1;
	swidget	Uxform5;
	swidget	Uxpb_main_badpix;
	swidget	Uxpb_main_flat;
	swidget	Uxpb_main_object;
	swidget	Uxpb_main_standard;
	swidget	Uxpb_main_extract;
	swidget	Uxpb_main_merge;
	swidget	Uxpb_main_flux_table;
	swidget	Uxshelp_main;
	swidget	Uxseparator1;
	swidget	Uxtf_main_dark;
	swidget	Uxlabel4;
	swidget	Uxlabel3;
	swidget	Uxtf_main_ststar;
	swidget	Uxlabel8;
	swidget	Uxlabel1;
	swidget	Uxtf_main_sky;
	swidget	Uxtf_main_object;
	swidget	Uxlabel2;
	swidget	Uxlabel7;
	swidget	Uxtf_main_stsky;
	swidget	Uxlabel5;
	swidget	Uxtf_main_flat;
	swidget	Uxseparator12;
} _UxCApplicWindow;

#define ApplicWindow            UxApplicWindowContext->UxApplicWindow
#define MainWindow              UxApplicWindowContext->UxMainWindow
#define menu1                   UxApplicWindowContext->Uxmenu1
#define menu1_p7                UxApplicWindowContext->Uxmenu1_p7
#define menu1_p7_b1             UxApplicWindowContext->Uxmenu1_p7_b1
#define menu1_p7_b2             UxApplicWindowContext->Uxmenu1_p7_b2
#define menu1_p7_b3             UxApplicWindowContext->Uxmenu1_p7_b3
#define menu1_top_b1            UxApplicWindowContext->Uxmenu1_top_b1
#define menu1_p4                UxApplicWindowContext->Uxmenu1_p4
#define menu1_p4_b1             UxApplicWindowContext->Uxmenu1_p4_b1
#define menu1_p4_b2             UxApplicWindowContext->Uxmenu1_p4_b2
#define menu1_p4_b8             UxApplicWindowContext->Uxmenu1_p4_b8
#define menu1_p4_b10            UxApplicWindowContext->Uxmenu1_p4_b10
#define menu1_p4_b13            UxApplicWindowContext->Uxmenu1_p4_b13
#define menu1_p4_b12            UxApplicWindowContext->Uxmenu1_p4_b12
#define menu1_p4_b3             UxApplicWindowContext->Uxmenu1_p4_b3
#define menu1_p4_b4             UxApplicWindowContext->Uxmenu1_p4_b4
#define menu1_p4_b9             UxApplicWindowContext->Uxmenu1_p4_b9
#define menu1_p4_b5             UxApplicWindowContext->Uxmenu1_p4_b5
#define menu1_p4_b11            UxApplicWindowContext->Uxmenu1_p4_b11
#define menu1_p4_b14            UxApplicWindowContext->Uxmenu1_p4_b14
#define menu1_top_b4            UxApplicWindowContext->Uxmenu1_top_b4
#define menu1_p8                UxApplicWindowContext->Uxmenu1_p8
#define tg_options_exit         UxApplicWindowContext->Uxtg_options_exit
#define menu1_top_b2            UxApplicWindowContext->Uxmenu1_top_b2
#define menu1_p5                UxApplicWindowContext->Uxmenu1_p5
#define menu_help_context       UxApplicWindowContext->Uxmenu_help_context
#define menu_help_help          UxApplicWindowContext->Uxmenu_help_help
#define menu1_p5_b5             UxApplicWindowContext->Uxmenu1_p5_b5
#define menu_help_tutorial      UxApplicWindowContext->Uxmenu_help_tutorial
#define menu_help_version       UxApplicWindowContext->Uxmenu_help_version
#define menu1_top_b5            UxApplicWindowContext->Uxmenu1_top_b5
#define menu1_p6                UxApplicWindowContext->Uxmenu1_p6
#define menu1_p6_b1             UxApplicWindowContext->Uxmenu1_p6_b1
#define menu1_top_b6            UxApplicWindowContext->Uxmenu1_top_b6
#define form1                   UxApplicWindowContext->Uxform1
#define form5                   UxApplicWindowContext->Uxform5
#define pb_main_badpix          UxApplicWindowContext->Uxpb_main_badpix
#define pb_main_flat            UxApplicWindowContext->Uxpb_main_flat
#define pb_main_object          UxApplicWindowContext->Uxpb_main_object
#define pb_main_standard        UxApplicWindowContext->Uxpb_main_standard
#define pb_main_extract         UxApplicWindowContext->Uxpb_main_extract
#define pb_main_merge           UxApplicWindowContext->Uxpb_main_merge
#define pb_main_flux_table      UxApplicWindowContext->Uxpb_main_flux_table
#define shelp_main              UxApplicWindowContext->Uxshelp_main
#define separator1              UxApplicWindowContext->Uxseparator1
#define tf_main_dark            UxApplicWindowContext->Uxtf_main_dark
#define label4                  UxApplicWindowContext->Uxlabel4
#define label3                  UxApplicWindowContext->Uxlabel3
#define tf_main_ststar          UxApplicWindowContext->Uxtf_main_ststar
#define label8                  UxApplicWindowContext->Uxlabel8
#define label1                  UxApplicWindowContext->Uxlabel1
#define tf_main_sky             UxApplicWindowContext->Uxtf_main_sky
#define tf_main_object          UxApplicWindowContext->Uxtf_main_object
#define label2                  UxApplicWindowContext->Uxlabel2
#define label7                  UxApplicWindowContext->Uxlabel7
#define tf_main_stsky           UxApplicWindowContext->Uxtf_main_stsky
#define label5                  UxApplicWindowContext->Uxlabel5
#define tf_main_flat            UxApplicWindowContext->Uxtf_main_flat
#define separator12             UxApplicWindowContext->Uxseparator12

static _UxCApplicWindow	*UxApplicWindowContext;

extern void DisplayExtendedHelp(), DisplayShortHelp();
extern void SetFileList(), FileSaveCallback(), FileSaveAsCallback();
extern void QuitCallback(), MainBadpixCallback(), MainFlatCallback();
extern void MainObjectCallback(), MainStandardCallback();
extern void WriteKeyword(), PopupIrspec();


extern int PopupList(), AppendDialogText();



/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*SelectFileMain = "#override\n\
<Btn3Down>:FileSelectACT()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ApplicWindow();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayShortHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("shelp_main"), "");
	UxPutText(UxFindSwidget("shelp_badpix"), "");
	UxPutText(UxFindSwidget("shelp_standard"), "");
	UxPutText(UxFindSwidget("shelp_flux_table"), "");
	UxPutText(UxFindSwidget("shelp_extract"), "");
	UxPutText(UxFindSwidget("shelp_object"), "");
	UxPutText(UxFindSwidget("shelp_merge"), "");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_FileSelectACT1( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <irspec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	strcpy(DirSpecs, "*.bdf");
		 
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_object1")) ) {
	    SET_LIST_TITLE("Enter object frame");
	    ListType = LIST_OBJECT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_sky")) ) {
	    SET_LIST_TITLE("Enter sky frame");
	    ListType = LIST_SKY;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_ststar")) ) {
	    SET_LIST_TITLE("Enter standard star");
	    ListType = LIST_STSTAR;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_stsky")) )  {
	    SET_LIST_TITLE("Enter standard sky");
	    ListType = LIST_STSKY;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_dark")) ) {
	    SET_LIST_TITLE("Enter dark frame");
	    ListType = LIST_DARK;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_flat")) )  {
	    SET_LIST_TITLE("Enter flat-field frame");
	    ListType = LIST_FLAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_in")) )  {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX;
	    strcpy(DirSpecs, "*.tbl");
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_resp")) )  {
	    SET_LIST_TITLE("Enter response frame");
	    ListType = LIST_RESP;
	}
	 
	
	  
	FileListWidget = UxGetWidget(UxFindSwidget("sl_file_list"));
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <irspec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	strcpy(DirSpecs, "*.bdf");
		 
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_object")) ) {
	    SET_LIST_TITLE("Enter object frame");
	    ListType = LIST_OBJECT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_sky")) ) {
	    SET_LIST_TITLE("Enter sky frame");
	    ListType = LIST_SKY;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_ststar")) ) {
	    SET_LIST_TITLE("Enter standard star");
	    ListType = LIST_STSTAR;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_stsky")) )  {
	    SET_LIST_TITLE("Enter standard sky");
	    ListType = LIST_STSKY;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_dark")) ) {
	    SET_LIST_TITLE("Enter dark frame");
	    ListType = LIST_DARK;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_flat")) )  {
	    SET_LIST_TITLE("Enter flat-field frame");
	    ListType = LIST_FLAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_std_flux")) )  {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX;
	    strcpy(DirSpecs, "*.tbl");
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_std_ref")) )  {
	    SET_LIST_TITLE("Enter reference frame");
	    ListType = LIST_STD_REF;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_ref")) )  {
	    SET_LIST_TITLE("Enter reference frame");
	    ListType = LIST_OBJ_REF;
	} 
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_finput")) )  {
	    SET_LIST_TITLE("Enter flux input frame");
	    ListType = LIST_OBJ_FINPUT;
	} 
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_resp")) )  {
	    SET_LIST_TITLE("Enter response frame");
	    ListType = LIST_RESP;
	}
	  
	FileListWidget = UxGetWidget(UxFindSwidget("sl_file_list"));
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_menu1_p7_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <irspec_comm.h>
	
	PopupList(LIST_OPEN);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p7_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	FileSaveCallback();
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p7_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	FileSaveAsCallback();
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("crea/display");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("crea/graph");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{        
#include <irspec_comm.h>
	
	PopupList(LIST_LOAD_IMA);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("clear/channel over");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("copy/display laser");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("copy/graph laser");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("get/gcursor");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("get/cursor");
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_tg_options_exit( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_context( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_help( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_tutorial( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_version( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p6_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	QuitCallback();
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_badpix( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupIrspec("BadpixShell");
	MainBadpixCallback();
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_flat( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	{
	MainFlatCallback();
	}
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_object( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	{
	MainObjectCallback();
	}
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_standard( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	MainStandardCallback();
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_extract( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupIrspec("ExtractShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_merge( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	{
	PopupIrspec("MergeShell");
	}
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_flux_table( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupIrspec("FluxTableShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_main_dark( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <irspec_comm.h>
	
	char *text;
	extern char Dark[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Dark) ) {
	    strcpy(Dark, text); 
	    WriteKeyword(Dark, K_DARK);
	}
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_main_ststar( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *text;
	extern char InStstar[];
	
	text = XmTextGetString(UxWidget);
	strcpy(InStstar, text);
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_main_sky( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *text;
	extern char InSky[];
	
	text = XmTextGetString(UxWidget);
	strcpy(InSky, text);
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_main_object( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *text;
	extern char InObj[];
	
	text = XmTextGetString(UxWidget);
	strcpy(InObj, text);
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_main_stsky( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	char *text;
	extern char InStsky[];
	
	text = XmTextGetString(UxWidget);
	strcpy(InStsky, text);
	
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_main_flat( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <irspec_comm.h>
	
	char *text;
	extern char Flat[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, Flat) ) {
	    strcpy(Flat, text); 
	    WriteKeyword(text, K_FLAT);
	}
	XtFree(text);
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ApplicWindow()
{
	UxPutGeometry( ApplicWindow, "+0+0" );
	UxPutTitle( ApplicWindow, "Irspec reduction" );
	UxPutKeyboardFocusPolicy( ApplicWindow, "pointer" );
	UxPutIconName( ApplicWindow, "Irspec" );
	UxPutHeight( ApplicWindow, 449 );
	UxPutWidth( ApplicWindow, 447 );
	UxPutY( ApplicWindow, 21 );
	UxPutX( ApplicWindow, 2 );

	UxPutBackground( MainWindow, WindowBackground );
	UxPutHeight( MainWindow, 460 );
	UxPutWidth( MainWindow, 440 );
	UxPutY( MainWindow, 0 );
	UxPutX( MainWindow, 10 );
	UxPutUnitType( MainWindow, "pixels" );

	UxPutBackground( menu1, MenuBackground );
	UxPutMenuAccelerator( menu1, "<KeyUp>F10" );
	UxPutRowColumnType( menu1, "menu_bar" );

	UxPutForeground( menu1_p7, MenuForeground );
	UxPutBackground( menu1_p7, MenuBackground );
	UxPutRowColumnType( menu1_p7, "menu_pulldown" );

	UxPutFontList( menu1_p7_b1, BoldTextFont );
	UxPutMnemonic( menu1_p7_b1, "O" );
	UxPutLabelString( menu1_p7_b1, "Open" );

	UxPutFontList( menu1_p7_b2, BoldTextFont );
	UxPutMnemonic( menu1_p7_b2, "S" );
	UxPutLabelString( menu1_p7_b2, "Save" );

	UxPutFontList( menu1_p7_b3, BoldTextFont );
	UxPutMnemonic( menu1_p7_b3, "A" );
	UxPutLabelString( menu1_p7_b3, "Save As ..." );

	UxPutMnemonic( menu1_top_b1, "F" );
	UxPutForeground( menu1_top_b1, MenuForeground );
	UxPutFontList( menu1_top_b1, BoldTextFont );
	UxPutBackground( menu1_top_b1, MenuBackground );
	UxPutLabelString( menu1_top_b1, "File" );

	UxPutForeground( menu1_p4, MenuForeground );
	UxPutBackground( menu1_p4, MenuBackground );
	UxPutRowColumnType( menu1_p4, "menu_pulldown" );

	UxPutFontList( menu1_p4_b1, BoldTextFont );
	UxPutMnemonic( menu1_p4_b1, "D" );
	UxPutLabelString( menu1_p4_b1, "Create Display" );

	UxPutFontList( menu1_p4_b2, BoldTextFont );
	UxPutMnemonic( menu1_p4_b2, "G" );
	UxPutLabelString( menu1_p4_b2, "Create Graphics" );

	UxPutMnemonic( menu1_p4_b10, "L" );
	UxPutFontList( menu1_p4_b10, BoldTextFont );
	UxPutLabelString( menu1_p4_b10, "Load image" );

	UxPutMnemonic( menu1_p4_b13, "o" );
	UxPutFontList( menu1_p4_b13, BoldTextFont );
	UxPutLabelString( menu1_p4_b13, "Clear overlay" );

	UxPutFontList( menu1_p4_b3, BoldTextFont );
	UxPutMnemonic( menu1_p4_b3, "P" );
	UxPutLabelString( menu1_p4_b3, "Print Display" );

	UxPutFontList( menu1_p4_b4, BoldTextFont );
	UxPutMnemonic( menu1_p4_b4, "r" );
	UxPutLabelString( menu1_p4_b4, "Print Graphics" );

	UxPutFontList( menu1_p4_b5, BoldTextFont );
	UxPutMnemonic( menu1_p4_b5, "C" );
	UxPutLabelString( menu1_p4_b5, "Graphics Cursor" );

	UxPutFontList( menu1_p4_b11, BoldTextFont );
	UxPutMnemonic( menu1_p4_b11, "u" );
	UxPutLabelString( menu1_p4_b11, "Display Cursor" );

	UxPutForeground( menu1_top_b4, MenuForeground );
	UxPutFontList( menu1_top_b4, BoldTextFont );
	UxPutBackground( menu1_top_b4, MenuBackground );
	UxPutMnemonic( menu1_top_b4, "U" );
	UxPutLabelString( menu1_top_b4, "Utils" );

	UxPutForeground( menu1_p8, MenuForeground );
	UxPutBackground( menu1_p8, MenuBackground );
	UxPutRowColumnType( menu1_p8, "menu_pulldown" );

	UxPutSet( tg_options_exit, "true" );
	UxPutSelectColor( tg_options_exit, SelectColor );
	UxPutFontList( tg_options_exit, BoldTextFont );
	UxPutLabelString( tg_options_exit, "Remove intermediate files on exit" );

	UxPutMnemonic( menu1_top_b2, "O" );
	UxPutForeground( menu1_top_b2, MenuForeground );
	UxPutFontList( menu1_top_b2, BoldTextFont );
	UxPutBackground( menu1_top_b2, MenuBackground );
	UxPutLabelString( menu1_top_b2, "Options" );

	UxPutForeground( menu1_p5, MenuForeground );
	UxPutBackground( menu1_p5, MenuBackground );
	UxPutRowColumnType( menu1_p5, "menu_pulldown" );

	UxPutFontList( menu_help_context, BoldTextFont );
	UxPutMnemonic( menu_help_context, "C" );
	UxPutLabelString( menu_help_context, "On Context ..." );

	UxPutFontList( menu_help_help, BoldTextFont );
	UxPutMnemonic( menu_help_help, "H" );
	UxPutLabelString( menu_help_help, "On Help ..." );

	UxPutFontList( menu_help_tutorial, BoldTextFont );
	UxPutMnemonic( menu_help_tutorial, "T" );
	UxPutLabelString( menu_help_tutorial, "Tutorial" );

	UxPutFontList( menu_help_version, BoldTextFont );
	UxPutMnemonic( menu_help_version, "V" );
	UxPutLabelString( menu_help_version, "On Version..." );

	UxPutForeground( menu1_top_b5, MenuForeground );
	UxPutFontList( menu1_top_b5, BoldTextFont );
	UxPutBackground( menu1_top_b5, MenuBackground );
	UxPutMnemonic( menu1_top_b5, "H" );
	UxPutLabelString( menu1_top_b5, "Help" );

	UxPutForeground( menu1_p6, MenuForeground );
	UxPutBackground( menu1_p6, MenuBackground );
	UxPutRowColumnType( menu1_p6, "menu_pulldown" );

	UxPutFontList( menu1_p6_b1, BoldTextFont );
	UxPutMnemonic( menu1_p6_b1, "B" );
	UxPutLabelString( menu1_p6_b1, "Bye" );

	UxPutForeground( menu1_top_b6, MenuForeground );
	UxPutFontList( menu1_top_b6, BoldTextFont );
	UxPutBackground( menu1_top_b6, MenuBackground );
	UxPutMnemonic( menu1_top_b6, "Q" );
	UxPutLabelString( menu1_top_b6, "Quit" );

	UxPutBackground( form1, WindowBackground );

	UxPutBackground( form5, ButtonBackground );
	UxPutHeight( form5, 72 );
	UxPutWidth( form5, 447 );
	UxPutY( form5, 343 );
	UxPutX( form5, -1 );
	UxPutResizePolicy( form5, "resize_none" );

	UxPutLabelString( pb_main_badpix, "Badpix ..." );
	UxPutForeground( pb_main_badpix, ButtonForeground );
	UxPutFontList( pb_main_badpix, BoldTextFont );
	UxPutBackground( pb_main_badpix, ButtonBackground );
	UxPutHeight( pb_main_badpix, 30 );
	UxPutWidth( pb_main_badpix, 86 );
	UxPutY( pb_main_badpix, 6 );
	UxPutX( pb_main_badpix, 4 );

	UxPutLabelString( pb_main_flat, "Flat ..." );
	UxPutForeground( pb_main_flat, ButtonForeground );
	UxPutFontList( pb_main_flat, BoldTextFont );
	UxPutBackground( pb_main_flat, ButtonBackground );
	UxPutHeight( pb_main_flat, 30 );
	UxPutWidth( pb_main_flat, 86 );
	UxPutY( pb_main_flat, 6 );
	UxPutX( pb_main_flat, 92 );

	UxPutLabelString( pb_main_object, "Object ..." );
	UxPutForeground( pb_main_object, ButtonForeground );
	UxPutFontList( pb_main_object, BoldTextFont );
	UxPutBackground( pb_main_object, ButtonBackground );
	UxPutHeight( pb_main_object, 30 );
	UxPutWidth( pb_main_object, 86 );
	UxPutY( pb_main_object, 6 );
	UxPutX( pb_main_object, 356 );

	UxPutLabelString( pb_main_standard, "Std star ..." );
	UxPutForeground( pb_main_standard, ButtonForeground );
	UxPutFontList( pb_main_standard, BoldTextFont );
	UxPutBackground( pb_main_standard, ButtonBackground );
	UxPutHeight( pb_main_standard, 30 );
	UxPutWidth( pb_main_standard, 86 );
	UxPutY( pb_main_standard, 6 );
	UxPutX( pb_main_standard, 268 );

	UxPutLabelString( pb_main_extract, "Extract ..." );
	UxPutForeground( pb_main_extract, ButtonForeground );
	UxPutFontList( pb_main_extract, BoldTextFont );
	UxPutBackground( pb_main_extract, ButtonBackground );
	UxPutHeight( pb_main_extract, 30 );
	UxPutWidth( pb_main_extract, 86 );
	UxPutY( pb_main_extract, 40 );
	UxPutX( pb_main_extract, 4 );

	UxPutLabelString( pb_main_merge, "Merge ..." );
	UxPutForeground( pb_main_merge, ButtonForeground );
	UxPutFontList( pb_main_merge, BoldTextFont );
	UxPutBackground( pb_main_merge, ButtonBackground );
	UxPutHeight( pb_main_merge, 30 );
	UxPutWidth( pb_main_merge, 86 );
	UxPutY( pb_main_merge, 40 );
	UxPutX( pb_main_merge, 92 );

	UxPutLabelString( pb_main_flux_table, "Flux tbl ..." );
	UxPutForeground( pb_main_flux_table, ButtonForeground );
	UxPutFontList( pb_main_flux_table, BoldTextFont );
	UxPutBackground( pb_main_flux_table, ButtonBackground );
	UxPutHeight( pb_main_flux_table, 30 );
	UxPutWidth( pb_main_flux_table, 86 );
	UxPutY( pb_main_flux_table, 6 );
	UxPutX( pb_main_flux_table, 180 );

	UxPutWordWrap( shelp_main, "false" );
	UxPutHighlightOnEnter( shelp_main, "false" );
	UxPutEditMode( shelp_main, "single_line_edit" );
	UxPutFontList( shelp_main, TextFont );
	UxPutEditable( shelp_main, "false" );
	UxPutCursorPositionVisible( shelp_main, "false" );
	UxPutBackground( shelp_main, SHelpBackground );
	UxPutHeight( shelp_main, 50 );
	UxPutWidth( shelp_main, 448 );
	UxPutY( shelp_main, 290 );
	UxPutX( shelp_main, 0 );

	UxPutBackground( separator1, LabelBackground );
	UxPutHeight( separator1, 6 );
	UxPutWidth( separator1, 450 );
	UxPutY( separator1, 284 );
	UxPutX( separator1, -2 );

	UxPutTranslations( tf_main_dark, SelectFileMain );
	UxPutForeground( tf_main_dark, TextForeground );
	UxPutHighlightOnEnter( tf_main_dark, "true" );
	UxPutFontList( tf_main_dark, TextFont );
	UxPutBackground( tf_main_dark, TextBackground );
	UxPutHeight( tf_main_dark, 34 );
	UxPutWidth( tf_main_dark, 236 );
	UxPutY( tf_main_dark, 194 );
	UxPutX( tf_main_dark, 172 );

	UxPutForeground( label4, TextForeground );
	UxPutAlignment( label4, "alignment_beginning" );
	UxPutLabelString( label4, "Dark frame :" );
	UxPutFontList( label4, TextFont );
	UxPutBackground( label4, LabelBackground );
	UxPutHeight( label4, 30 );
	UxPutWidth( label4, 128 );
	UxPutY( label4, 196 );
	UxPutX( label4, 30 );

	UxPutForeground( label3, TextForeground );
	UxPutAlignment( label3, "alignment_beginning" );
	UxPutLabelString( label3, "Standard sky :" );
	UxPutFontList( label3, TextFont );
	UxPutBackground( label3, LabelBackground );
	UxPutHeight( label3, 30 );
	UxPutWidth( label3, 128 );
	UxPutY( label3, 158 );
	UxPutX( label3, 30 );

	UxPutTranslations( tf_main_ststar, SelectFileMain );
	UxPutBorderWidth( tf_main_ststar, 0 );
	UxPutForeground( tf_main_ststar, TextForeground );
	UxPutHighlightOnEnter( tf_main_ststar, "true" );
	UxPutFontList( tf_main_ststar, TextFont );
	UxPutBackground( tf_main_ststar, TextBackground );
	UxPutHeight( tf_main_ststar, 34 );
	UxPutWidth( tf_main_ststar, 236 );
	UxPutY( tf_main_ststar, 120 );
	UxPutX( tf_main_ststar, 172 );

	UxPutForeground( label8, TextForeground );
	UxPutAlignment( label8, "alignment_beginning" );
	UxPutLabelString( label8, "Standard star :" );
	UxPutFontList( label8, TextFont );
	UxPutBackground( label8, LabelBackground );
	UxPutHeight( label8, 30 );
	UxPutWidth( label8, 128 );
	UxPutY( label8, 122 );
	UxPutX( label8, 30 );

	UxPutForeground( label1, TextForeground );
	UxPutAlignment( label1, "alignment_beginning" );
	UxPutLabelString( label1, "Sky :" );
	UxPutFontList( label1, TextFont );
	UxPutBackground( label1, LabelBackground );
	UxPutHeight( label1, 30 );
	UxPutWidth( label1, 128 );
	UxPutY( label1, 84 );
	UxPutX( label1, 30 );

	UxPutTranslations( tf_main_sky, SelectFileMain );
	UxPutForeground( tf_main_sky, TextForeground );
	UxPutFontList( tf_main_sky, TextFont );
	UxPutBackground( tf_main_sky, TextBackground );
	UxPutHeight( tf_main_sky, 34 );
	UxPutWidth( tf_main_sky, 235 );
	UxPutY( tf_main_sky, 82 );
	UxPutX( tf_main_sky, 172 );

	UxPutTranslations( tf_main_object, SelectFileMain );
	UxPutForeground( tf_main_object, TextForeground );
	UxPutFontList( tf_main_object, TextFont );
	UxPutBackground( tf_main_object, TextBackground );
	UxPutHeight( tf_main_object, 34 );
	UxPutWidth( tf_main_object, 235 );
	UxPutY( tf_main_object, 44 );
	UxPutX( tf_main_object, 172 );

	UxPutForeground( label2, TextForeground );
	UxPutAlignment( label2, "alignment_beginning" );
	UxPutLabelString( label2, "Object :" );
	UxPutFontList( label2, TextFont );
	UxPutBackground( label2, LabelBackground );
	UxPutHeight( label2, 30 );
	UxPutWidth( label2, 128 );
	UxPutY( label2, 44 );
	UxPutX( label2, 30 );

	UxPutForeground( label7, TextForeground );
	UxPutAlignment( label7, "alignment_beginning" );
	UxPutLabelString( label7, "GENERAL PARAMETERS" );
	UxPutFontList( label7, TextFont );
	UxPutBackground( label7, LabelBackground );
	UxPutHeight( label7, 30 );
	UxPutWidth( label7, 180 );
	UxPutY( label7, 8 );
	UxPutX( label7, 2 );

	UxPutTranslations( tf_main_stsky, SelectFileMain );
	UxPutBorderWidth( tf_main_stsky, 0 );
	UxPutForeground( tf_main_stsky, TextForeground );
	UxPutHighlightOnEnter( tf_main_stsky, "true" );
	UxPutFontList( tf_main_stsky, TextFont );
	UxPutBackground( tf_main_stsky, TextBackground );
	UxPutHeight( tf_main_stsky, 34 );
	UxPutWidth( tf_main_stsky, 236 );
	UxPutY( tf_main_stsky, 156 );
	UxPutX( tf_main_stsky, 172 );

	UxPutForeground( label5, TextForeground );
	UxPutAlignment( label5, "alignment_beginning" );
	UxPutLabelString( label5, "Flat frame :" );
	UxPutFontList( label5, TextFont );
	UxPutBackground( label5, LabelBackground );
	UxPutHeight( label5, 30 );
	UxPutWidth( label5, 128 );
	UxPutY( label5, 234 );
	UxPutX( label5, 30 );

	UxPutTranslations( tf_main_flat, SelectFileMain );
	UxPutForeground( tf_main_flat, TextForeground );
	UxPutHighlightOnEnter( tf_main_flat, "true" );
	UxPutFontList( tf_main_flat, TextFont );
	UxPutBackground( tf_main_flat, TextBackground );
	UxPutHeight( tf_main_flat, 34 );
	UxPutWidth( tf_main_flat, 236 );
	UxPutY( tf_main_flat, 232 );
	UxPutX( tf_main_flat, 172 );

	UxPutBackground( separator12, LabelBackground );
	UxPutHeight( separator12, 6 );
	UxPutWidth( separator12, 450 );
	UxPutY( separator12, 338 );
	UxPutX( separator12, -2 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ApplicWindow()
{
	/* Create the swidgets */

	ApplicWindow = UxCreateApplicationShell( "ApplicWindow", NO_PARENT );
	UxPutContext( ApplicWindow, UxApplicWindowContext );

	MainWindow = UxCreateMainWindow( "MainWindow", ApplicWindow );
	menu1 = UxCreateRowColumn( "menu1", MainWindow );
	menu1_p7 = UxCreateRowColumn( "menu1_p7", menu1 );
	menu1_p7_b1 = UxCreatePushButtonGadget( "menu1_p7_b1", menu1_p7 );
	menu1_p7_b2 = UxCreatePushButtonGadget( "menu1_p7_b2", menu1_p7 );
	menu1_p7_b3 = UxCreatePushButtonGadget( "menu1_p7_b3", menu1_p7 );
	menu1_top_b1 = UxCreateCascadeButton( "menu1_top_b1", menu1 );
	menu1_p4 = UxCreateRowColumn( "menu1_p4", menu1 );
	menu1_p4_b1 = UxCreatePushButtonGadget( "menu1_p4_b1", menu1_p4 );
	menu1_p4_b2 = UxCreatePushButtonGadget( "menu1_p4_b2", menu1_p4 );
	menu1_p4_b8 = UxCreateSeparatorGadget( "menu1_p4_b8", menu1_p4 );
	menu1_p4_b10 = UxCreatePushButtonGadget( "menu1_p4_b10", menu1_p4 );
	menu1_p4_b13 = UxCreatePushButtonGadget( "menu1_p4_b13", menu1_p4 );
	menu1_p4_b12 = UxCreateSeparatorGadget( "menu1_p4_b12", menu1_p4 );
	menu1_p4_b3 = UxCreatePushButtonGadget( "menu1_p4_b3", menu1_p4 );
	menu1_p4_b4 = UxCreatePushButtonGadget( "menu1_p4_b4", menu1_p4 );
	menu1_p4_b9 = UxCreateSeparatorGadget( "menu1_p4_b9", menu1_p4 );
	menu1_p4_b5 = UxCreatePushButtonGadget( "menu1_p4_b5", menu1_p4 );
	menu1_p4_b11 = UxCreatePushButtonGadget( "menu1_p4_b11", menu1_p4 );
	menu1_p4_b14 = UxCreateSeparatorGadget( "menu1_p4_b14", menu1_p4 );
	menu1_top_b4 = UxCreateCascadeButton( "menu1_top_b4", menu1 );
	menu1_p8 = UxCreateRowColumn( "menu1_p8", menu1 );
	tg_options_exit = UxCreateToggleButtonGadget( "tg_options_exit", menu1_p8 );
	menu1_top_b2 = UxCreateCascadeButton( "menu1_top_b2", menu1 );
	menu1_p5 = UxCreateRowColumn( "menu1_p5", menu1 );
	menu_help_context = UxCreatePushButtonGadget( "menu_help_context", menu1_p5 );
	menu_help_help = UxCreatePushButtonGadget( "menu_help_help", menu1_p5 );
	menu1_p5_b5 = UxCreateSeparatorGadget( "menu1_p5_b5", menu1_p5 );
	menu_help_tutorial = UxCreatePushButtonGadget( "menu_help_tutorial", menu1_p5 );
	menu_help_version = UxCreatePushButtonGadget( "menu_help_version", menu1_p5 );
	menu1_top_b5 = UxCreateCascadeButton( "menu1_top_b5", menu1 );
	menu1_p6 = UxCreateRowColumn( "menu1_p6", menu1 );
	menu1_p6_b1 = UxCreatePushButtonGadget( "menu1_p6_b1", menu1_p6 );
	menu1_top_b6 = UxCreateCascadeButton( "menu1_top_b6", menu1 );
	form1 = UxCreateForm( "form1", MainWindow );
	form5 = UxCreateForm( "form5", form1 );
	pb_main_badpix = UxCreatePushButton( "pb_main_badpix", form5 );
	pb_main_flat = UxCreatePushButton( "pb_main_flat", form5 );
	pb_main_object = UxCreatePushButton( "pb_main_object", form5 );
	pb_main_standard = UxCreatePushButton( "pb_main_standard", form5 );
	pb_main_extract = UxCreatePushButton( "pb_main_extract", form5 );
	pb_main_merge = UxCreatePushButton( "pb_main_merge", form5 );
	pb_main_flux_table = UxCreatePushButton( "pb_main_flux_table", form5 );
	shelp_main = UxCreateText( "shelp_main", form1 );
	separator1 = UxCreateSeparator( "separator1", form1 );
	tf_main_dark = UxCreateTextField( "tf_main_dark", form1 );
	label4 = UxCreateLabel( "label4", form1 );
	label3 = UxCreateLabel( "label3", form1 );
	tf_main_ststar = UxCreateTextField( "tf_main_ststar", form1 );
	label8 = UxCreateLabel( "label8", form1 );
	label1 = UxCreateLabel( "label1", form1 );
	tf_main_sky = UxCreateTextField( "tf_main_sky", form1 );
	tf_main_object = UxCreateTextField( "tf_main_object", form1 );
	label2 = UxCreateLabel( "label2", form1 );
	label7 = UxCreateLabel( "label7", form1 );
	tf_main_stsky = UxCreateTextField( "tf_main_stsky", form1 );
	label5 = UxCreateLabel( "label5", form1 );
	tf_main_flat = UxCreateTextField( "tf_main_flat", form1 );
	separator12 = UxCreateSeparator( "separator12", form1 );

	_Uxinit_ApplicWindow();

	/* Create the X widgets */

	UxCreateWidget( ApplicWindow );
	UxCreateWidget( MainWindow );
	UxCreateWidget( menu1 );
	UxCreateWidget( menu1_p7 );
	UxCreateWidget( menu1_p7_b1 );
	UxCreateWidget( menu1_p7_b2 );
	UxCreateWidget( menu1_p7_b3 );
	UxPutSubMenuId( menu1_top_b1, "menu1_p7" );
	UxCreateWidget( menu1_top_b1 );

	UxCreateWidget( menu1_p4 );
	UxCreateWidget( menu1_p4_b1 );
	UxCreateWidget( menu1_p4_b2 );
	UxCreateWidget( menu1_p4_b8 );
	UxCreateWidget( menu1_p4_b10 );
	UxCreateWidget( menu1_p4_b13 );
	UxCreateWidget( menu1_p4_b12 );
	UxCreateWidget( menu1_p4_b3 );
	UxCreateWidget( menu1_p4_b4 );
	UxCreateWidget( menu1_p4_b9 );
	UxCreateWidget( menu1_p4_b5 );
	UxCreateWidget( menu1_p4_b11 );
	UxCreateWidget( menu1_p4_b14 );
	UxPutSubMenuId( menu1_top_b4, "menu1_p4" );
	UxCreateWidget( menu1_top_b4 );

	UxCreateWidget( menu1_p8 );
	UxCreateWidget( tg_options_exit );
	UxPutSubMenuId( menu1_top_b2, "menu1_p8" );
	UxCreateWidget( menu1_top_b2 );

	UxCreateWidget( menu1_p5 );
	UxCreateWidget( menu_help_context );
	UxCreateWidget( menu_help_help );
	UxCreateWidget( menu1_p5_b5 );
	UxCreateWidget( menu_help_tutorial );
	UxCreateWidget( menu_help_version );
	UxPutSubMenuId( menu1_top_b5, "menu1_p5" );
	UxCreateWidget( menu1_top_b5 );

	UxCreateWidget( menu1_p6 );
	UxCreateWidget( menu1_p6_b1 );
	UxPutSubMenuId( menu1_top_b6, "menu1_p6" );
	UxCreateWidget( menu1_top_b6 );

	UxCreateWidget( form1 );
	UxCreateWidget( form5 );
	UxCreateWidget( pb_main_badpix );
	UxCreateWidget( pb_main_flat );
	UxCreateWidget( pb_main_object );
	UxCreateWidget( pb_main_standard );
	UxCreateWidget( pb_main_extract );
	UxCreateWidget( pb_main_merge );
	UxCreateWidget( pb_main_flux_table );
	UxCreateWidget( shelp_main );
	UxCreateWidget( separator1 );
	UxCreateWidget( tf_main_dark );
	UxCreateWidget( label4 );
	UxCreateWidget( label3 );
	UxCreateWidget( tf_main_ststar );
	UxCreateWidget( label8 );
	UxCreateWidget( label1 );
	UxCreateWidget( tf_main_sky );
	UxCreateWidget( tf_main_object );
	UxCreateWidget( label2 );
	UxCreateWidget( label7 );
	UxCreateWidget( tf_main_stsky );
	UxCreateWidget( label5 );
	UxCreateWidget( tf_main_flat );
	UxCreateWidget( separator12 );

	UxPutMenuHelpWidget( menu1, "menu1_top_b6" );

	UxAddCallback( menu1_p7_b1, XmNactivateCallback,
			activateCB_menu1_p7_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p7_b2, XmNactivateCallback,
			activateCB_menu1_p7_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p7_b3, XmNactivateCallback,
			activateCB_menu1_p7_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b1, XmNactivateCallback,
			activateCB_menu1_p4_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b2, XmNactivateCallback,
			activateCB_menu1_p4_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b10, XmNactivateCallback,
			activateCB_menu1_p4_b10,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b13, XmNactivateCallback,
			activateCB_menu1_p4_b13,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b3, XmNactivateCallback,
			activateCB_menu1_p4_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b4, XmNactivateCallback,
			activateCB_menu1_p4_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b5, XmNactivateCallback,
			activateCB_menu1_p4_b5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b11, XmNactivateCallback,
			activateCB_menu1_p4_b11,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tg_options_exit, XmNvalueChangedCallback,
			valueChangedCB_tg_options_exit,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_context, XmNactivateCallback,
			activateCB_menu_help_context,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_help, XmNactivateCallback,
			activateCB_menu_help_help,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_tutorial, XmNactivateCallback,
			activateCB_menu_help_tutorial,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_version, XmNactivateCallback,
			activateCB_menu_help_version,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p6_b1, XmNactivateCallback,
			activateCB_menu1_p6_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_badpix, XmNactivateCallback,
			activateCB_pb_main_badpix,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_flat, XmNactivateCallback,
			activateCB_pb_main_flat,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_object, XmNactivateCallback,
			activateCB_pb_main_object,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_standard, XmNactivateCallback,
			activateCB_pb_main_standard,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_extract, XmNactivateCallback,
			activateCB_pb_main_extract,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_merge, XmNactivateCallback,
			activateCB_pb_main_merge,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_flux_table, XmNactivateCallback,
			activateCB_pb_main_flux_table,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_main_dark, XmNlosingFocusCallback,
			losingFocusCB_tf_main_dark,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_main_ststar, XmNlosingFocusCallback,
			losingFocusCB_tf_main_ststar,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_main_sky, XmNlosingFocusCallback,
			losingFocusCB_tf_main_sky,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_main_object, XmNlosingFocusCallback,
			losingFocusCB_tf_main_object,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_main_stsky, XmNlosingFocusCallback,
			losingFocusCB_tf_main_stsky,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_main_flat, XmNlosingFocusCallback,
			losingFocusCB_tf_main_flat,
			(XtPointer) UxApplicWindowContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ApplicWindow );

	UxMainWindowSetAreas( MainWindow, menu1, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, form1 );
	return ( ApplicWindow );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ApplicWindow()
{
	swidget                 rtrn;
	_UxCApplicWindow        *UxContext;

	UxApplicWindowContext = UxContext =
		(_UxCApplicWindow *) UxMalloc( sizeof(_UxCApplicWindow) );

	{
		extern swidget create_StandardShell();
		extern swidget create_BadpixShell();
		extern swidget create_ObjectShell();
		extern swidget create_FluxTableShell();
		extern swidget create_MergeShell();
		extern swidget create_ExtractShell();
		extern swidget create_HelpShell();
		extern swidget create_file_dialog();
		rtrn = _Uxbuild_ApplicWindow();

		create_StandardShell();
		create_BadpixShell();
		create_ObjectShell();
		create_FluxTableShell();
		create_MergeShell();
		create_ExtractShell();
		create_HelpShell();
		create_file_dialog();
		
		return(rtrn);
	}
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ApplicWindow()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "WriteHelp", action_WriteHelp },
				{ "ClearHelp", action_ClearHelp },
				{ "FileSelectACT1", action_FileSelectACT1 },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ApplicWindow();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

