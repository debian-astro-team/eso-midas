/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	FluxTableShell.c

.VERSION
 090828         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSepG.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxSep.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxFluxTableShell;
	swidget	Uxform9;
	swidget	Uxseparator13;
	swidget	Uxshelp_flux_table;
	swidget	Uxform16;
	swidget	UxpushButton3;
	swidget	Uxpb_flux_table_apply;
	swidget	Uxseparator14;
	swidget	Uxlabel13;
	swidget	Uxtf_flux_in;
	swidget	Uxtf_flux_out;
	swidget	Uxlabel15;
	swidget	UxrowColumn3;
	swidget	Uxrb_interp_black;
	swidget	Uxrb_interp_poly;
	swidget	Uxrb_interp_spline;
	swidget	Uxlabel16;
	swidget	UxseparatorGadget9;
	swidget	UxseparatorGadget10;
	swidget	UxseparatorGadget13;
	swidget	Uxlabel17;
	swidget	Uxtf_step;
	swidget	Uxlabel20;
	swidget	Uxtf_fitdeg;
} _UxCFluxTableShell;

#define FluxTableShell          UxFluxTableShellContext->UxFluxTableShell
#define form9                   UxFluxTableShellContext->Uxform9
#define separator13             UxFluxTableShellContext->Uxseparator13
#define shelp_flux_table        UxFluxTableShellContext->Uxshelp_flux_table
#define form16                  UxFluxTableShellContext->Uxform16
#define pushButton3             UxFluxTableShellContext->UxpushButton3
#define pb_flux_table_apply     UxFluxTableShellContext->Uxpb_flux_table_apply
#define separator14             UxFluxTableShellContext->Uxseparator14
#define label13                 UxFluxTableShellContext->Uxlabel13
#define tf_flux_in              UxFluxTableShellContext->Uxtf_flux_in
#define tf_flux_out             UxFluxTableShellContext->Uxtf_flux_out
#define label15                 UxFluxTableShellContext->Uxlabel15
#define rowColumn3              UxFluxTableShellContext->UxrowColumn3
#define rb_interp_black         UxFluxTableShellContext->Uxrb_interp_black
#define rb_interp_poly          UxFluxTableShellContext->Uxrb_interp_poly
#define rb_interp_spline        UxFluxTableShellContext->Uxrb_interp_spline
#define label16                 UxFluxTableShellContext->Uxlabel16
#define separatorGadget9        UxFluxTableShellContext->UxseparatorGadget9
#define separatorGadget10       UxFluxTableShellContext->UxseparatorGadget10
#define separatorGadget13       UxFluxTableShellContext->UxseparatorGadget13
#define label17                 UxFluxTableShellContext->Uxlabel17
#define tf_step                 UxFluxTableShellContext->Uxtf_step
#define label20                 UxFluxTableShellContext->Uxlabel20
#define tf_fitdeg               UxFluxTableShellContext->Uxtf_fitdeg

static _UxCFluxTableShell	*UxFluxTableShellContext;

extern void FluxTableApplyCallback();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_FluxTableShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxTableShell      *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxTableShellContext;
	UxFluxTableShellContext = UxContext =
			(_UxCFluxTableShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("FluxTableShell"));
	}
	UxFluxTableShellContext = UxSaveCtx;
}

static void	activateCB_pb_flux_table_apply( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxTableShell      *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxTableShellContext;
	UxFluxTableShellContext = UxContext =
			(_UxCFluxTableShell *) UxGetContext( UxThisWidget );
	{
	FluxTableApplyCallback();
	}
	UxFluxTableShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_flux_in( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxTableShell      *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxTableShellContext;
	UxFluxTableShellContext = UxContext =
			(_UxCFluxTableShell *) UxGetContext( UxThisWidget );
	{
	char *text, output[80];
	int i;
	
	text = XmTextGetString(UxWidget);
	for ( i = 0; text[i] != '.' && text[i] != '\0'; i++ )
	     ;
	text[i] = '\0';
	
	sprintf(output, "%s_flux", text);
	XmTextSetString(UxGetWidget(UxFindSwidget("tf_flux_out")), output);
	
	XtFree(text);
	}
	UxFluxTableShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_FluxTableShell()
{
	UxPutTitle( FluxTableShell, "Flux table generation form" );
	UxPutGeometry( FluxTableShell, "+10+60" );
	UxPutDefaultFontList( FluxTableShell, TextFont );
	UxPutKeyboardFocusPolicy( FluxTableShell, "pointer" );
	UxPutIconName( FluxTableShell, "Flux table generation form" );
	UxPutHeight( FluxTableShell, 344 );
	UxPutWidth( FluxTableShell, 490 );
	UxPutY( FluxTableShell, 186 );
	UxPutX( FluxTableShell, 491 );

	UxPutBackground( form9, "grey80" );
	UxPutHeight( form9, 548 );
	UxPutWidth( form9, 490 );
	UxPutY( form9, 4 );
	UxPutX( form9, 4 );
	UxPutUnitType( form9, "pixels" );
	UxPutResizePolicy( form9, "resize_none" );

	UxPutBackground( separator13, WindowBackground );
	UxPutHeight( separator13, 6 );
	UxPutWidth( separator13, 492 );
	UxPutY( separator13, 286 );
	UxPutX( separator13, -2 );

	UxPutWordWrap( shelp_flux_table, "false" );
	UxPutHighlightOnEnter( shelp_flux_table, "false" );
	UxPutEditMode( shelp_flux_table, "single_line_edit" );
	UxPutFontList( shelp_flux_table, TextFont );
	UxPutEditable( shelp_flux_table, "false" );
	UxPutCursorPositionVisible( shelp_flux_table, "false" );
	UxPutBackground( shelp_flux_table, SHelpBackground );
	UxPutHeight( shelp_flux_table, 50 );
	UxPutWidth( shelp_flux_table, 490 );
	UxPutY( shelp_flux_table, 238 );
	UxPutX( shelp_flux_table, 0 );

	UxPutBackground( form16, ButtonBackground );
	UxPutHeight( form16, 50 );
	UxPutWidth( form16, 490 );
	UxPutY( form16, 294 );
	UxPutX( form16, 0 );
	UxPutResizePolicy( form16, "resize_none" );

	UxPutLabelString( pushButton3, "Return" );
	UxPutForeground( pushButton3, CancelForeground );
	UxPutFontList( pushButton3, BoldTextFont );
	UxPutBackground( pushButton3, ButtonBackground );
	UxPutHeight( pushButton3, 30 );
	UxPutWidth( pushButton3, 86 );
	UxPutY( pushButton3, 10 );
	UxPutX( pushButton3, 104 );

	UxPutLabelString( pb_flux_table_apply, "Apply" );
	UxPutForeground( pb_flux_table_apply, ButtonForeground );
	UxPutFontList( pb_flux_table_apply, BoldTextFont );
	UxPutBackground( pb_flux_table_apply, ButtonBackground );
	UxPutHeight( pb_flux_table_apply, 30 );
	UxPutWidth( pb_flux_table_apply, 86 );
	UxPutY( pb_flux_table_apply, 10 );
	UxPutX( pb_flux_table_apply, 10 );

	UxPutBackground( separator14, WindowBackground );
	UxPutHeight( separator14, 6 );
	UxPutWidth( separator14, 492 );
	UxPutY( separator14, 234 );
	UxPutX( separator14, 2 );

	UxPutForeground( label13, TextForeground );
	UxPutAlignment( label13, "alignment_beginning" );
	UxPutLabelString( label13, "Broad band flux table :" );
	UxPutFontList( label13, TextFont );
	UxPutBackground( label13, LabelBackground );
	UxPutHeight( label13, 30 );
	UxPutWidth( label13, 148 );
	UxPutY( label13, 9 );
	UxPutX( label13, 22 );

	UxPutForeground( tf_flux_in, TextForeground );
	UxPutHighlightOnEnter( tf_flux_in, "true" );
	UxPutFontList( tf_flux_in, TextFont );
	UxPutBackground( tf_flux_in, TextBackground );
	UxPutHeight( tf_flux_in, 34 );
	UxPutWidth( tf_flux_in, 221 );
	UxPutY( tf_flux_in, 7 );
	UxPutX( tf_flux_in, 218 );

	UxPutForeground( tf_flux_out, TextForeground );
	UxPutHighlightOnEnter( tf_flux_out, "true" );
	UxPutFontList( tf_flux_out, TextFont );
	UxPutBackground( tf_flux_out, TextBackground );
	UxPutHeight( tf_flux_out, 34 );
	UxPutWidth( tf_flux_out, 221 );
	UxPutY( tf_flux_out, 47 );
	UxPutX( tf_flux_out, 218 );

	UxPutForeground( label15, TextForeground );
	UxPutAlignment( label15, "alignment_beginning" );
	UxPutLabelString( label15, "Output flux table :" );
	UxPutFontList( label15, TextFont );
	UxPutBackground( label15, LabelBackground );
	UxPutHeight( label15, 30 );
	UxPutWidth( label15, 184 );
	UxPutY( label15, 49 );
	UxPutX( label15, 22 );

	UxPutIsAligned( rowColumn3, "true" );
	UxPutEntryAlignment( rowColumn3, "alignment_beginning" );
	UxPutBorderWidth( rowColumn3, 0 );
	UxPutShadowThickness( rowColumn3, 0 );
	UxPutLabelString( rowColumn3, "" );
	UxPutEntryBorder( rowColumn3, 0 );
	UxPutBackground( rowColumn3, "grey80" );
	UxPutRadioBehavior( rowColumn3, "true" );
	UxPutHeight( rowColumn3, 67 );
	UxPutWidth( rowColumn3, 82 );
	UxPutY( rowColumn3, 121 );
	UxPutX( rowColumn3, 49 );

	UxPutIndicatorSize( rb_interp_black, 16 );
	UxPutHighlightOnEnter( rb_interp_black, "true" );
	UxPutForeground( rb_interp_black, TextForeground );
	UxPutSelectColor( rb_interp_black, SelectColor );
	UxPutSet( rb_interp_black, "true" );
	UxPutLabelString( rb_interp_black, "Black body" );
	UxPutFontList( rb_interp_black, TextFont );
	UxPutBackground( rb_interp_black, WindowBackground );
	UxPutHeight( rb_interp_black, 20 );
	UxPutWidth( rb_interp_black, 76 );
	UxPutY( rb_interp_black, 0 );
	UxPutX( rb_interp_black, -12 );

	UxPutIndicatorSize( rb_interp_poly, 16 );
	UxPutHighlightOnEnter( rb_interp_poly, "true" );
	UxPutSet( rb_interp_poly, "false" );
	UxPutForeground( rb_interp_poly, TextForeground );
	UxPutSelectColor( rb_interp_poly, SelectColor );
	UxPutLabelString( rb_interp_poly, "Polynomial" );
	UxPutFontList( rb_interp_poly, TextFont );
	UxPutBackground( rb_interp_poly, WindowBackground );
	UxPutHeight( rb_interp_poly, 66 );
	UxPutWidth( rb_interp_poly, 76 );
	UxPutY( rb_interp_poly, 34 );
	UxPutX( rb_interp_poly, 3 );

	UxPutIndicatorSize( rb_interp_spline, 16 );
	UxPutHighlightOnEnter( rb_interp_spline, "true" );
	UxPutSet( rb_interp_spline, "false" );
	UxPutForeground( rb_interp_spline, TextForeground );
	UxPutSelectColor( rb_interp_spline, SelectColor );
	UxPutLabelString( rb_interp_spline, "Spline" );
	UxPutFontList( rb_interp_spline, TextFont );
	UxPutBackground( rb_interp_spline, WindowBackground );
	UxPutHeight( rb_interp_spline, 66 );
	UxPutWidth( rb_interp_spline, 76 );
	UxPutY( rb_interp_spline, 42 );
	UxPutX( rb_interp_spline, 11 );

	UxPutForeground( label16, TextForeground );
	UxPutAlignment( label16, "alignment_beginning" );
	UxPutLabelString( label16, "Interpolation method" );
	UxPutFontList( label16, TextFont );
	UxPutBackground( label16, WindowBackground );
	UxPutHeight( label16, 22 );
	UxPutWidth( label16, 138 );
	UxPutY( label16, 97 );
	UxPutX( label16, 33 );

	UxPutOrientation( separatorGadget9, "vertical" );
	UxPutHeight( separatorGadget9, 114 );
	UxPutWidth( separatorGadget9, 12 );
	UxPutY( separatorGadget9, 111 );
	UxPutX( separatorGadget9, 24 );

	UxPutOrientation( separatorGadget10, "vertical" );
	UxPutHeight( separatorGadget10, 114 );
	UxPutWidth( separatorGadget10, 12 );
	UxPutY( separatorGadget10, 111 );
	UxPutX( separatorGadget10, 168 );

	UxPutHeight( separatorGadget13, 8 );
	UxPutWidth( separatorGadget13, 148 );
	UxPutY( separatorGadget13, 221 );
	UxPutX( separatorGadget13, 28 );

	UxPutForeground( label17, TextForeground );
	UxPutAlignment( label17, "alignment_beginning" );
	UxPutLabelString( label17, "Wavelength step :" );
	UxPutFontList( label17, TextFont );
	UxPutBackground( label17, LabelBackground );
	UxPutHeight( label17, 30 );
	UxPutWidth( label17, 142 );
	UxPutY( label17, 110 );
	UxPutX( label17, 202 );

	UxPutText( tf_step, "0.01" );
	UxPutForeground( tf_step, TextForeground );
	UxPutHighlightOnEnter( tf_step, "true" );
	UxPutFontList( tf_step, TextFont );
	UxPutBackground( tf_step, TextBackground );
	UxPutHeight( tf_step, 34 );
	UxPutWidth( tf_step, 80 );
	UxPutY( tf_step, 108 );
	UxPutX( tf_step, 344 );

	UxPutForeground( label20, TextForeground );
	UxPutAlignment( label20, "alignment_beginning" );
	UxPutLabelString( label20, "Fit degree :" );
	UxPutFontList( label20, TextFont );
	UxPutBackground( label20, LabelBackground );
	UxPutHeight( label20, 30 );
	UxPutWidth( label20, 142 );
	UxPutY( label20, 148 );
	UxPutX( label20, 202 );

	UxPutText( tf_fitdeg, "2" );
	UxPutForeground( tf_fitdeg, TextForeground );
	UxPutHighlightOnEnter( tf_fitdeg, "true" );
	UxPutFontList( tf_fitdeg, TextFont );
	UxPutBackground( tf_fitdeg, TextBackground );
	UxPutHeight( tf_fitdeg, 34 );
	UxPutWidth( tf_fitdeg, 80 );
	UxPutY( tf_fitdeg, 146 );
	UxPutX( tf_fitdeg, 344 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_FluxTableShell()
{
	/* Create the swidgets */

	FluxTableShell = UxCreateApplicationShell( "FluxTableShell", NO_PARENT );
	UxPutContext( FluxTableShell, UxFluxTableShellContext );

	form9 = UxCreateForm( "form9", FluxTableShell );
	separator13 = UxCreateSeparator( "separator13", form9 );
	shelp_flux_table = UxCreateText( "shelp_flux_table", form9 );
	form16 = UxCreateForm( "form16", form9 );
	pushButton3 = UxCreatePushButton( "pushButton3", form16 );
	pb_flux_table_apply = UxCreatePushButton( "pb_flux_table_apply", form16 );
	separator14 = UxCreateSeparator( "separator14", form9 );
	label13 = UxCreateLabel( "label13", form9 );
	tf_flux_in = UxCreateTextField( "tf_flux_in", form9 );
	tf_flux_out = UxCreateTextField( "tf_flux_out", form9 );
	label15 = UxCreateLabel( "label15", form9 );
	rowColumn3 = UxCreateRowColumn( "rowColumn3", form9 );
	rb_interp_black = UxCreateToggleButton( "rb_interp_black", rowColumn3 );
	rb_interp_poly = UxCreateToggleButton( "rb_interp_poly", rowColumn3 );
	rb_interp_spline = UxCreateToggleButton( "rb_interp_spline", rowColumn3 );
	label16 = UxCreateLabel( "label16", form9 );
	separatorGadget9 = UxCreateSeparatorGadget( "separatorGadget9", form9 );
	separatorGadget10 = UxCreateSeparatorGadget( "separatorGadget10", form9 );
	separatorGadget13 = UxCreateSeparatorGadget( "separatorGadget13", form9 );
	label17 = UxCreateLabel( "label17", form9 );
	tf_step = UxCreateTextField( "tf_step", form9 );
	label20 = UxCreateLabel( "label20", form9 );
	tf_fitdeg = UxCreateTextField( "tf_fitdeg", form9 );

	_Uxinit_FluxTableShell();

	/* Create the X widgets */

	UxCreateWidget( FluxTableShell );
	UxCreateWidget( form9 );
	UxCreateWidget( separator13 );
	UxCreateWidget( shelp_flux_table );
	UxCreateWidget( form16 );
	UxCreateWidget( pushButton3 );
	UxCreateWidget( pb_flux_table_apply );
	UxCreateWidget( separator14 );
	UxCreateWidget( label13 );
	UxCreateWidget( tf_flux_in );
	UxCreateWidget( tf_flux_out );
	UxCreateWidget( label15 );
	UxCreateWidget( rowColumn3 );
	UxCreateWidget( rb_interp_black );
	UxCreateWidget( rb_interp_poly );
	UxCreateWidget( rb_interp_spline );
	UxCreateWidget( label16 );
	UxCreateWidget( separatorGadget9 );
	UxCreateWidget( separatorGadget10 );
	UxCreateWidget( separatorGadget13 );
	UxCreateWidget( label17 );
	UxCreateWidget( tf_step );
	UxCreateWidget( label20 );
	UxCreateWidget( tf_fitdeg );

	UxAddCallback( pushButton3, XmNactivateCallback,
			activateCB_pushButton3,
			(XtPointer) UxFluxTableShellContext );

	UxAddCallback( pb_flux_table_apply, XmNactivateCallback,
			activateCB_pb_flux_table_apply,
			(XtPointer) UxFluxTableShellContext );

	UxAddCallback( tf_flux_in, XmNlosingFocusCallback,
			losingFocusCB_tf_flux_in,
			(XtPointer) UxFluxTableShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( FluxTableShell );

	return ( FluxTableShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_FluxTableShell()
{
	swidget                 rtrn;
	_UxCFluxTableShell      *UxContext;

	UxFluxTableShellContext = UxContext =
		(_UxCFluxTableShell *) UxMalloc( sizeof(_UxCFluxTableShell) );

	rtrn = _Uxbuild_FluxTableShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_FluxTableShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_FluxTableShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

