/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	CalibShell.c

.VERSION
 090828         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxPushBG.h"
#include "UxText.h"
#include "UxSep.h"
#include "UxTextF.h"
#include "UxSepG.h"
#include "UxLabel.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxBadpixShell;
	swidget	Uxform3;
	swidget	UxrowColumn2;
	swidget	Uxrb_badpix_obj;
	swidget	Uxrb_badpix_sky;
	swidget	Uxrb_badpix_ststar;
	swidget	Uxrb_badpix_stsky;
	swidget	Uxlabel14;
	swidget	UxseparatorGadget6;
	swidget	UxseparatorGadget7;
	swidget	UxseparatorGadget8;
	swidget	Uxlabel18;
	swidget	Uxtf_badpix_frames;
	swidget	Uxlabel19;
	swidget	Uxbadpix_tab_label;
	swidget	Uxtf_badpix_table;
	swidget	Uxseparator4;
	swidget	Uxshelp_badpix;
	swidget	Uxtf_badpix_thres1;
	swidget	Uxtf_badpix_thres2;
	swidget	Uxmenu2_p1;
	swidget	Uxmn_clean_auto;
	swidget	Uxmn_clean_x;
	swidget	Uxmn_clean_y;
	swidget	Uxmn_clean_xy;
	swidget	Uxmn_clean;
	swidget	Uxtg_badpix_display;
	swidget	Uxform20;
	swidget	UxpushButton40;
	swidget	Uxpb_badpix_define;
	swidget	Uxpb_badpix_apply;
	swidget	Uxmenu2_p2;
	swidget	Uxmn_mode_rel;
	swidget	Uxmn_mode_abs;
	swidget	Uxmn_mode;
	swidget	Uxseparator5;
	swidget	Uxlabel6;
} _UxCBadpixShell;

#define BadpixShell             UxBadpixShellContext->UxBadpixShell
#define form3                   UxBadpixShellContext->Uxform3
#define rowColumn2              UxBadpixShellContext->UxrowColumn2
#define rb_badpix_obj           UxBadpixShellContext->Uxrb_badpix_obj
#define rb_badpix_sky           UxBadpixShellContext->Uxrb_badpix_sky
#define rb_badpix_ststar        UxBadpixShellContext->Uxrb_badpix_ststar
#define rb_badpix_stsky         UxBadpixShellContext->Uxrb_badpix_stsky
#define label14                 UxBadpixShellContext->Uxlabel14
#define separatorGadget6        UxBadpixShellContext->UxseparatorGadget6
#define separatorGadget7        UxBadpixShellContext->UxseparatorGadget7
#define separatorGadget8        UxBadpixShellContext->UxseparatorGadget8
#define label18                 UxBadpixShellContext->Uxlabel18
#define tf_badpix_frames        UxBadpixShellContext->Uxtf_badpix_frames
#define label19                 UxBadpixShellContext->Uxlabel19
#define badpix_tab_label        UxBadpixShellContext->Uxbadpix_tab_label
#define tf_badpix_table         UxBadpixShellContext->Uxtf_badpix_table
#define separator4              UxBadpixShellContext->Uxseparator4
#define shelp_badpix            UxBadpixShellContext->Uxshelp_badpix
#define tf_badpix_thres1        UxBadpixShellContext->Uxtf_badpix_thres1
#define tf_badpix_thres2        UxBadpixShellContext->Uxtf_badpix_thres2
#define menu2_p1                UxBadpixShellContext->Uxmenu2_p1
#define mn_clean_auto           UxBadpixShellContext->Uxmn_clean_auto
#define mn_clean_x              UxBadpixShellContext->Uxmn_clean_x
#define mn_clean_y              UxBadpixShellContext->Uxmn_clean_y
#define mn_clean_xy             UxBadpixShellContext->Uxmn_clean_xy
#define mn_clean                UxBadpixShellContext->Uxmn_clean
#define tg_badpix_display       UxBadpixShellContext->Uxtg_badpix_display
#define form20                  UxBadpixShellContext->Uxform20
#define pushButton40            UxBadpixShellContext->UxpushButton40
#define pb_badpix_define        UxBadpixShellContext->Uxpb_badpix_define
#define pb_badpix_apply         UxBadpixShellContext->Uxpb_badpix_apply
#define menu2_p2                UxBadpixShellContext->Uxmenu2_p2
#define mn_mode_rel             UxBadpixShellContext->Uxmn_mode_rel
#define mn_mode_abs             UxBadpixShellContext->Uxmn_mode_abs
#define mn_mode                 UxBadpixShellContext->Uxmn_mode
#define separator5              UxBadpixShellContext->Uxseparator5
#define label6                  UxBadpixShellContext->Uxlabel6

static _UxCBadpixShell	*UxBadpixShellContext;

extern void WriteKeyword(), BadpixInputCallback(), BadpixDefineCallback();
extern void BadpixApplyCallback();




/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_BadpixShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	valueChangedCB_rb_badpix_obj( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	BadpixInputCallback("tf_main_object");
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_badpix_sky( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	BadpixInputCallback("tf_main_sky");
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_badpix_ststar( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	BadpixInputCallback("tf_main_ststar");
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_badpix_stsky( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	BadpixInputCallback("tf_main_stsky");
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_badpix_table( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
#include <irspec_comm.h>
	
	char *text;
	extern char BadpixTable[];
	
	text = XmTextGetString(UxWidget);
	
	if ( strcmp(text, BadpixTable) ) {
	    strcpy(BadpixTable, text);
	    WriteKeyword(text, K_BADPIX);
	}
	
	XtFree(text);
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	activateCB_mn_clean_auto( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	activateCB_mn_clean_x( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	activateCB_mn_clean_y( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	activateCB_mn_clean_xy( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	activateCB_pushButton40( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("BadpixShell"));
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	activateCB_pb_badpix_define( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	BadpixDefineCallback();
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	activateCB_pb_badpix_apply( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	BadpixApplyCallback();
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	activateCB_mn_mode_rel( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	}
	UxBadpixShellContext = UxSaveCtx;
}

static void	activateCB_mn_mode_abs( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBadpixShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBadpixShellContext;
	UxBadpixShellContext = UxContext =
			(_UxCBadpixShell *) UxGetContext( UxThisWidget );
	{
	}
	UxBadpixShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_BadpixShell()
{
	UxPutGeometry( BadpixShell, "+10+60" );
	UxPutDefaultFontList( BadpixShell, TextFont );
	UxPutKeyboardFocusPolicy( BadpixShell, "pointer" );
	UxPutIconName( BadpixShell, "Bad pixels form" );
	UxPutHeight( BadpixShell, 410 );
	UxPutWidth( BadpixShell, 490 );
	UxPutY( BadpixShell, 332 );
	UxPutX( BadpixShell, 555 );

	UxPutBackground( form3, "grey80" );
	UxPutHeight( form3, 548 );
	UxPutWidth( form3, 490 );
	UxPutY( form3, 4 );
	UxPutX( form3, 4 );
	UxPutUnitType( form3, "pixels" );
	UxPutResizePolicy( form3, "resize_none" );

	UxPutIsAligned( rowColumn2, "true" );
	UxPutEntryAlignment( rowColumn2, "alignment_beginning" );
	UxPutBorderWidth( rowColumn2, 0 );
	UxPutShadowThickness( rowColumn2, 0 );
	UxPutLabelString( rowColumn2, "" );
	UxPutEntryBorder( rowColumn2, 0 );
	UxPutBackground( rowColumn2, "grey80" );
	UxPutRadioBehavior( rowColumn2, "true" );
	UxPutHeight( rowColumn2, 67 );
	UxPutWidth( rowColumn2, 82 );
	UxPutY( rowColumn2, 34 );
	UxPutX( rowColumn2, 21 );

	UxPutIndicatorSize( rb_badpix_obj, 16 );
	UxPutHighlightOnEnter( rb_badpix_obj, "true" );
	UxPutForeground( rb_badpix_obj, TextForeground );
	UxPutSelectColor( rb_badpix_obj, SelectColor );
	UxPutSet( rb_badpix_obj, "true" );
	UxPutLabelString( rb_badpix_obj, "Object" );
	UxPutFontList( rb_badpix_obj, TextFont );
	UxPutBackground( rb_badpix_obj, WindowBackground );
	UxPutHeight( rb_badpix_obj, 21 );
	UxPutWidth( rb_badpix_obj, 76 );
	UxPutY( rb_badpix_obj, 4 );
	UxPutX( rb_badpix_obj, -12 );

	UxPutIndicatorSize( rb_badpix_sky, 16 );
	UxPutHighlightOnEnter( rb_badpix_sky, "true" );
	UxPutSet( rb_badpix_sky, "false" );
	UxPutForeground( rb_badpix_sky, TextForeground );
	UxPutSelectColor( rb_badpix_sky, SelectColor );
	UxPutLabelString( rb_badpix_sky, "Sky" );
	UxPutFontList( rb_badpix_sky, TextFont );
	UxPutBackground( rb_badpix_sky, WindowBackground );
	UxPutHeight( rb_badpix_sky, 66 );
	UxPutWidth( rb_badpix_sky, 76 );
	UxPutY( rb_badpix_sky, 35 );
	UxPutX( rb_badpix_sky, -12 );

	UxPutIndicatorSize( rb_badpix_ststar, 16 );
	UxPutHighlightOnEnter( rb_badpix_ststar, "true" );
	UxPutSet( rb_badpix_ststar, "false" );
	UxPutForeground( rb_badpix_ststar, TextForeground );
	UxPutSelectColor( rb_badpix_ststar, SelectColor );
	UxPutLabelString( rb_badpix_ststar, "Standard star" );
	UxPutFontList( rb_badpix_ststar, TextFont );
	UxPutBackground( rb_badpix_ststar, WindowBackground );
	UxPutHeight( rb_badpix_ststar, 66 );
	UxPutWidth( rb_badpix_ststar, 76 );
	UxPutY( rb_badpix_ststar, 66 );
	UxPutX( rb_badpix_ststar, -12 );

	UxPutIndicatorSize( rb_badpix_stsky, 16 );
	UxPutHighlightOnEnter( rb_badpix_stsky, "true" );
	UxPutSet( rb_badpix_stsky, "false" );
	UxPutForeground( rb_badpix_stsky, TextForeground );
	UxPutSelectColor( rb_badpix_stsky, SelectColor );
	UxPutLabelString( rb_badpix_stsky, "Standard sky" );
	UxPutFontList( rb_badpix_stsky, TextFont );
	UxPutBackground( rb_badpix_stsky, WindowBackground );
	UxPutHeight( rb_badpix_stsky, 66 );
	UxPutWidth( rb_badpix_stsky, 76 );
	UxPutY( rb_badpix_stsky, 97 );
	UxPutX( rb_badpix_stsky, -12 );

	UxPutForeground( label14, TextForeground );
	UxPutAlignment( label14, "alignment_beginning" );
	UxPutLabelString( label14, "Input image" );
	UxPutFontList( label14, TextFont );
	UxPutBackground( label14, WindowBackground );
	UxPutHeight( label14, 18 );
	UxPutWidth( label14, 85 );
	UxPutY( label14, 14 );
	UxPutX( label14, 39 );

	UxPutOrientation( separatorGadget6, "vertical" );
	UxPutHeight( separatorGadget6, 144 );
	UxPutWidth( separatorGadget6, 7 );
	UxPutY( separatorGadget6, 24 );
	UxPutX( separatorGadget6, 12 );

	UxPutHeight( separatorGadget7, 8 );
	UxPutWidth( separatorGadget7, 136 );
	UxPutY( separatorGadget7, 162 );
	UxPutX( separatorGadget7, 13 );

	UxPutOrientation( separatorGadget8, "vertical" );
	UxPutHeight( separatorGadget8, 144 );
	UxPutWidth( separatorGadget8, 8 );
	UxPutY( separatorGadget8, 24 );
	UxPutX( separatorGadget8, 143 );

	UxPutForeground( label18, TextForeground );
	UxPutAlignment( label18, "alignment_beginning" );
	UxPutLabelString( label18, "Threshold :" );
	UxPutFontList( label18, TextFont );
	UxPutBackground( label18, LabelBackground );
	UxPutHeight( label18, 30 );
	UxPutWidth( label18, 80 );
	UxPutY( label18, 264 );
	UxPutX( label18, 10 );

	UxPutForeground( tf_badpix_frames, TextForeground );
	UxPutHighlightOnEnter( tf_badpix_frames, "true" );
	UxPutFontList( tf_badpix_frames, TextFont );
	UxPutBackground( tf_badpix_frames, TextBackground );
	UxPutHeight( tf_badpix_frames, 34 );
	UxPutWidth( tf_badpix_frames, 82 );
	UxPutY( tf_badpix_frames, 260 );
	UxPutX( tf_badpix_frames, 404 );

	UxPutForeground( label19, TextForeground );
	UxPutAlignment( label19, "alignment_beginning" );
	UxPutLabelString( label19, "Number of sub-frames :" );
	UxPutFontList( label19, TextFont );
	UxPutBackground( label19, LabelBackground );
	UxPutHeight( label19, 30 );
	UxPutWidth( label19, 154 );
	UxPutY( label19, 262 );
	UxPutX( label19, 246 );

	UxPutForeground( badpix_tab_label, TextForeground );
	UxPutAlignment( badpix_tab_label, "alignment_beginning" );
	UxPutLabelString( badpix_tab_label, "Bad pixels table :" );
	UxPutFontList( badpix_tab_label, TextFont );
	UxPutBackground( badpix_tab_label, LabelBackground );
	UxPutHeight( badpix_tab_label, 30 );
	UxPutWidth( badpix_tab_label, 124 );
	UxPutY( badpix_tab_label, 106 );
	UxPutX( badpix_tab_label, 166 );

	UxPutText( tf_badpix_table, "irsbadpix.tbl" );
	UxPutForeground( tf_badpix_table, TextForeground );
	UxPutHighlightOnEnter( tf_badpix_table, "true" );
	UxPutFontList( tf_badpix_table, TextFont );
	UxPutBackground( tf_badpix_table, TextBackground );
	UxPutHeight( tf_badpix_table, 34 );
	UxPutWidth( tf_badpix_table, 170 );
	UxPutY( tf_badpix_table, 102 );
	UxPutX( tf_badpix_table, 310 );

	UxPutBackground( separator4, WindowBackground );
	UxPutHeight( separator4, 6 );
	UxPutWidth( separator4, 492 );
	UxPutY( separator4, 354 );
	UxPutX( separator4, -2 );

	UxPutWordWrap( shelp_badpix, "false" );
	UxPutHighlightOnEnter( shelp_badpix, "false" );
	UxPutEditMode( shelp_badpix, "single_line_edit" );
	UxPutFontList( shelp_badpix, TextFont );
	UxPutEditable( shelp_badpix, "false" );
	UxPutCursorPositionVisible( shelp_badpix, "false" );
	UxPutBackground( shelp_badpix, SHelpBackground );
	UxPutHeight( shelp_badpix, 50 );
	UxPutWidth( shelp_badpix, 490 );
	UxPutY( shelp_badpix, 306 );
	UxPutX( shelp_badpix, 0 );

	UxPutForeground( tf_badpix_thres1, TextForeground );
	UxPutHighlightOnEnter( tf_badpix_thres1, "true" );
	UxPutFontList( tf_badpix_thres1, TextFont );
	UxPutBackground( tf_badpix_thres1, TextBackground );
	UxPutHeight( tf_badpix_thres1, 34 );
	UxPutWidth( tf_badpix_thres1, 68 );
	UxPutY( tf_badpix_thres1, 260 );
	UxPutX( tf_badpix_thres1, 92 );

	UxPutForeground( tf_badpix_thres2, TextForeground );
	UxPutHighlightOnEnter( tf_badpix_thres2, "true" );
	UxPutFontList( tf_badpix_thres2, TextFont );
	UxPutBackground( tf_badpix_thres2, TextBackground );
	UxPutHeight( tf_badpix_thres2, 34 );
	UxPutWidth( tf_badpix_thres2, 66 );
	UxPutY( tf_badpix_thres2, 260 );
	UxPutX( tf_badpix_thres2, 162 );

	UxPutForeground( menu2_p1, TextForeground );
	UxPutBackground( menu2_p1, WindowBackground );
	UxPutRowColumnType( menu2_p1, "menu_pulldown" );

	UxPutFontList( mn_clean_auto, TextFont );
	UxPutLabelString( mn_clean_auto, " Automatic " );

	UxPutFontList( mn_clean_x, TextFont );
	UxPutLabelString( mn_clean_x, " X-direction " );

	UxPutFontList( mn_clean_y, TextFont );
	UxPutLabelString( mn_clean_y, " Y-direction " );

	UxPutFontList( mn_clean_xy, TextFont );
	UxPutLabelString( mn_clean_xy, " X+Y-direction " );

	UxPutLabelString( mn_clean, "Clean method : " );
	UxPutSpacing( mn_clean, 0 );
	UxPutMarginWidth( mn_clean, 0 );
	UxPutForeground( mn_clean, TextForeground );
	UxPutBackground( mn_clean, WindowBackground );
	UxPutY( mn_clean, 18 );
	UxPutX( mn_clean, 166 );
	UxPutRowColumnType( mn_clean, "menu_option" );

	UxPutIndicatorSize( tg_badpix_display, 16 );
	UxPutSensitive( tg_badpix_display, "true" );
	UxPutHighlightOnEnter( tg_badpix_display, "true" );
	UxPutForeground( tg_badpix_display, TextForeground );
	UxPutSelectColor( tg_badpix_display, SelectColor );
	UxPutSet( tg_badpix_display, "false" );
	UxPutLabelString( tg_badpix_display, "Display image" );
	UxPutFontList( tg_badpix_display, TextFont );
	UxPutBackground( tg_badpix_display, WindowBackground );
	UxPutHeight( tg_badpix_display, 31 );
	UxPutWidth( tg_badpix_display, 122 );
	UxPutY( tg_badpix_display, 60 );
	UxPutX( tg_badpix_display, 164 );

	UxPutBackground( form20, ButtonBackground );
	UxPutHeight( form20, 46 );
	UxPutWidth( form20, 490 );
	UxPutY( form20, 362 );
	UxPutX( form20, 0 );
	UxPutResizePolicy( form20, "resize_none" );

	UxPutLabelString( pushButton40, "Return" );
	UxPutForeground( pushButton40, CancelForeground );
	UxPutFontList( pushButton40, BoldTextFont );
	UxPutBackground( pushButton40, ButtonBackground );
	UxPutHeight( pushButton40, 30 );
	UxPutWidth( pushButton40, 86 );
	UxPutY( pushButton40, 8 );
	UxPutX( pushButton40, 190 );

	UxPutLabelString( pb_badpix_define, "Define" );
	UxPutForeground( pb_badpix_define, ButtonForeground );
	UxPutFontList( pb_badpix_define, BoldTextFont );
	UxPutBackground( pb_badpix_define, ButtonBackground );
	UxPutHeight( pb_badpix_define, 30 );
	UxPutWidth( pb_badpix_define, 86 );
	UxPutY( pb_badpix_define, 8 );
	UxPutX( pb_badpix_define, 2 );

	UxPutLabelString( pb_badpix_apply, "Apply" );
	UxPutForeground( pb_badpix_apply, ButtonForeground );
	UxPutFontList( pb_badpix_apply, BoldTextFont );
	UxPutBackground( pb_badpix_apply, ButtonBackground );
	UxPutHeight( pb_badpix_apply, 30 );
	UxPutWidth( pb_badpix_apply, 86 );
	UxPutY( pb_badpix_apply, 8 );
	UxPutX( pb_badpix_apply, 96 );

	UxPutForeground( menu2_p2, TextForeground );
	UxPutBackground( menu2_p2, WindowBackground );
	UxPutRowColumnType( menu2_p2, "menu_pulldown" );

	UxPutFontList( mn_mode_rel, TextFont );
	UxPutLabelString( mn_mode_rel, " Relative " );

	UxPutFontList( mn_mode_abs, TextFont );
	UxPutLabelString( mn_mode_abs, " Absolute " );

	UxPutLabelString( mn_mode, "Selection mode : " );
	UxPutSpacing( mn_mode, 0 );
	UxPutMarginWidth( mn_mode, 0 );
	UxPutForeground( mn_mode, TextForeground );
	UxPutBackground( mn_mode, WindowBackground );
	UxPutY( mn_mode, 222 );
	UxPutX( mn_mode, 10 );
	UxPutRowColumnType( mn_mode, "menu_option" );

	UxPutBackground( separator5, WindowBackground );
	UxPutHeight( separator5, 6 );
	UxPutWidth( separator5, 492 );
	UxPutY( separator5, 302 );
	UxPutX( separator5, 2 );

	UxPutForeground( label6, TextForeground );
	UxPutAlignment( label6, "alignment_beginning" );
	UxPutLabelString( label6, "BAD PIXELS DEFINITION PARAMETERS" );
	UxPutFontList( label6, TextFont );
	UxPutBackground( label6, LabelBackground );
	UxPutHeight( label6, 30 );
	UxPutWidth( label6, 285 );
	UxPutY( label6, 191 );
	UxPutX( label6, 15 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_BadpixShell()
{
	/* Create the swidgets */

	BadpixShell = UxCreateApplicationShell( "BadpixShell", NO_PARENT );
	UxPutContext( BadpixShell, UxBadpixShellContext );

	form3 = UxCreateForm( "form3", BadpixShell );
	rowColumn2 = UxCreateRowColumn( "rowColumn2", form3 );
	rb_badpix_obj = UxCreateToggleButton( "rb_badpix_obj", rowColumn2 );
	rb_badpix_sky = UxCreateToggleButton( "rb_badpix_sky", rowColumn2 );
	rb_badpix_ststar = UxCreateToggleButton( "rb_badpix_ststar", rowColumn2 );
	rb_badpix_stsky = UxCreateToggleButton( "rb_badpix_stsky", rowColumn2 );
	label14 = UxCreateLabel( "label14", form3 );
	separatorGadget6 = UxCreateSeparatorGadget( "separatorGadget6", form3 );
	separatorGadget7 = UxCreateSeparatorGadget( "separatorGadget7", form3 );
	separatorGadget8 = UxCreateSeparatorGadget( "separatorGadget8", form3 );
	label18 = UxCreateLabel( "label18", form3 );
	tf_badpix_frames = UxCreateTextField( "tf_badpix_frames", form3 );
	label19 = UxCreateLabel( "label19", form3 );
	badpix_tab_label = UxCreateLabel( "badpix_tab_label", form3 );
	tf_badpix_table = UxCreateTextField( "tf_badpix_table", form3 );
	separator4 = UxCreateSeparator( "separator4", form3 );
	shelp_badpix = UxCreateText( "shelp_badpix", form3 );
	tf_badpix_thres1 = UxCreateTextField( "tf_badpix_thres1", form3 );
	tf_badpix_thres2 = UxCreateTextField( "tf_badpix_thres2", form3 );
	menu2_p1 = UxCreateRowColumn( "menu2_p1", form3 );
	mn_clean_auto = UxCreatePushButtonGadget( "mn_clean_auto", menu2_p1 );
	mn_clean_x = UxCreatePushButtonGadget( "mn_clean_x", menu2_p1 );
	mn_clean_y = UxCreatePushButtonGadget( "mn_clean_y", menu2_p1 );
	mn_clean_xy = UxCreatePushButtonGadget( "mn_clean_xy", menu2_p1 );
	mn_clean = UxCreateRowColumn( "mn_clean", form3 );
	tg_badpix_display = UxCreateToggleButton( "tg_badpix_display", form3 );
	form20 = UxCreateForm( "form20", form3 );
	pushButton40 = UxCreatePushButton( "pushButton40", form20 );
	pb_badpix_define = UxCreatePushButton( "pb_badpix_define", form20 );
	pb_badpix_apply = UxCreatePushButton( "pb_badpix_apply", form20 );
	menu2_p2 = UxCreateRowColumn( "menu2_p2", form3 );
	mn_mode_rel = UxCreatePushButtonGadget( "mn_mode_rel", menu2_p2 );
	mn_mode_abs = UxCreatePushButtonGadget( "mn_mode_abs", menu2_p2 );
	mn_mode = UxCreateRowColumn( "mn_mode", form3 );
	separator5 = UxCreateSeparator( "separator5", form3 );
	label6 = UxCreateLabel( "label6", form3 );

	_Uxinit_BadpixShell();

	/* Create the X widgets */

	UxCreateWidget( BadpixShell );
	UxCreateWidget( form3 );
	UxCreateWidget( rowColumn2 );
	UxCreateWidget( rb_badpix_obj );
	UxCreateWidget( rb_badpix_sky );
	UxCreateWidget( rb_badpix_ststar );
	UxCreateWidget( rb_badpix_stsky );
	UxCreateWidget( label14 );
	UxCreateWidget( separatorGadget6 );
	UxCreateWidget( separatorGadget7 );
	UxCreateWidget( separatorGadget8 );
	UxCreateWidget( label18 );
	UxCreateWidget( tf_badpix_frames );
	UxCreateWidget( label19 );
	UxCreateWidget( badpix_tab_label );
	UxCreateWidget( tf_badpix_table );
	UxCreateWidget( separator4 );
	UxCreateWidget( shelp_badpix );
	UxCreateWidget( tf_badpix_thres1 );
	UxCreateWidget( tf_badpix_thres2 );
	UxCreateWidget( menu2_p1 );
	UxCreateWidget( mn_clean_auto );
	UxCreateWidget( mn_clean_x );
	UxCreateWidget( mn_clean_y );
	UxCreateWidget( mn_clean_xy );
	UxPutSubMenuId( mn_clean, "menu2_p1" );
	UxCreateWidget( mn_clean );

	UxCreateWidget( tg_badpix_display );
	UxCreateWidget( form20 );
	UxCreateWidget( pushButton40 );
	UxCreateWidget( pb_badpix_define );
	UxCreateWidget( pb_badpix_apply );
	UxCreateWidget( menu2_p2 );
	UxCreateWidget( mn_mode_rel );
	UxCreateWidget( mn_mode_abs );
	UxPutSubMenuId( mn_mode, "menu2_p2" );
	UxCreateWidget( mn_mode );

	UxCreateWidget( separator5 );
	UxCreateWidget( label6 );

	UxAddCallback( rb_badpix_obj, XmNvalueChangedCallback,
			valueChangedCB_rb_badpix_obj,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( rb_badpix_sky, XmNvalueChangedCallback,
			valueChangedCB_rb_badpix_sky,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( rb_badpix_ststar, XmNvalueChangedCallback,
			valueChangedCB_rb_badpix_ststar,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( rb_badpix_stsky, XmNvalueChangedCallback,
			valueChangedCB_rb_badpix_stsky,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( tf_badpix_table, XmNlosingFocusCallback,
			losingFocusCB_tf_badpix_table,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( mn_clean_auto, XmNactivateCallback,
			activateCB_mn_clean_auto,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( mn_clean_x, XmNactivateCallback,
			activateCB_mn_clean_x,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( mn_clean_y, XmNactivateCallback,
			activateCB_mn_clean_y,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( mn_clean_xy, XmNactivateCallback,
			activateCB_mn_clean_xy,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( pushButton40, XmNactivateCallback,
			activateCB_pushButton40,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( pb_badpix_define, XmNactivateCallback,
			activateCB_pb_badpix_define,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( pb_badpix_apply, XmNactivateCallback,
			activateCB_pb_badpix_apply,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( mn_mode_rel, XmNactivateCallback,
			activateCB_mn_mode_rel,
			(XtPointer) UxBadpixShellContext );

	UxAddCallback( mn_mode_abs, XmNactivateCallback,
			activateCB_mn_mode_abs,
			(XtPointer) UxBadpixShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( BadpixShell );

	return ( BadpixShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_BadpixShell()
{
	swidget                 rtrn;
	_UxCBadpixShell         *UxContext;

	UxBadpixShellContext = UxContext =
		(_UxCBadpixShell *) UxMalloc( sizeof(_UxCBadpixShell) );

	rtrn = _Uxbuild_BadpixShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_BadpixShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_BadpixShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

