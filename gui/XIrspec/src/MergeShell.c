/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	MergeShell.c

.VERSION
 090828         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTogB.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxSep.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxMergeShell;
	swidget	Uxform7;
	swidget	Uxlabel24;
	swidget	Uxtf_merge_step;
	swidget	Uxlabel25;
	swidget	Uxbadpix_tab_label1;
	swidget	Uxtf_merge_out;
	swidget	Uxseparator6;
	swidget	Uxshelp_merge;
	swidget	Uxtf_merge_i1;
	swidget	Uxtf_merge_i2;
	swidget	Uxform8;
	swidget	UxpushButton2;
	swidget	Uxpb_merge_apply;
	swidget	Uxbadpix_ima_label2;
	swidget	Uxtf_merge_prefix;
	swidget	Uxseparator7;
	swidget	Uxtg_merge_correct;
	swidget	Uxbadpix_ima_label1;
	swidget	Uxtf_merge_pixels;
	swidget	Uxbadpix_ima_label3;
	swidget	Uxtf_merge_ref;
	swidget	Uxbadpix_ima_label4;
	swidget	Uxtf_merge_digits;
	swidget	Uxtg_merge_plot;
} _UxCMergeShell;

#define MergeShell              UxMergeShellContext->UxMergeShell
#define form7                   UxMergeShellContext->Uxform7
#define label24                 UxMergeShellContext->Uxlabel24
#define tf_merge_step           UxMergeShellContext->Uxtf_merge_step
#define label25                 UxMergeShellContext->Uxlabel25
#define badpix_tab_label1       UxMergeShellContext->Uxbadpix_tab_label1
#define tf_merge_out            UxMergeShellContext->Uxtf_merge_out
#define separator6              UxMergeShellContext->Uxseparator6
#define shelp_merge             UxMergeShellContext->Uxshelp_merge
#define tf_merge_i1             UxMergeShellContext->Uxtf_merge_i1
#define tf_merge_i2             UxMergeShellContext->Uxtf_merge_i2
#define form8                   UxMergeShellContext->Uxform8
#define pushButton2             UxMergeShellContext->UxpushButton2
#define pb_merge_apply          UxMergeShellContext->Uxpb_merge_apply
#define badpix_ima_label2       UxMergeShellContext->Uxbadpix_ima_label2
#define tf_merge_prefix         UxMergeShellContext->Uxtf_merge_prefix
#define separator7              UxMergeShellContext->Uxseparator7
#define tg_merge_correct        UxMergeShellContext->Uxtg_merge_correct
#define badpix_ima_label1       UxMergeShellContext->Uxbadpix_ima_label1
#define tf_merge_pixels         UxMergeShellContext->Uxtf_merge_pixels
#define badpix_ima_label3       UxMergeShellContext->Uxbadpix_ima_label3
#define tf_merge_ref            UxMergeShellContext->Uxtf_merge_ref
#define badpix_ima_label4       UxMergeShellContext->Uxbadpix_ima_label4
#define tf_merge_digits         UxMergeShellContext->Uxtf_merge_digits
#define tg_merge_plot           UxMergeShellContext->Uxtg_merge_plot

static _UxCMergeShell	*UxMergeShellContext;

extern void MergeApplyCallback();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_MergeShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("MergeShell"));
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	activateCB_pb_merge_apply( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	MergeApplyCallback();
	}
	UxMergeShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_MergeShell()
{
	UxPutTitle( MergeShell, "Merge form" );
	UxPutGeometry( MergeShell, "+10+60" );
	UxPutDefaultFontList( MergeShell, TextFont );
	UxPutKeyboardFocusPolicy( MergeShell, "pointer" );
	UxPutIconName( MergeShell, "Merge form" );
	UxPutHeight( MergeShell, 373 );
	UxPutWidth( MergeShell, 490 );
	UxPutY( MergeShell, 81 );
	UxPutX( MergeShell, 12 );

	UxPutBackground( form7, "grey80" );
	UxPutHeight( form7, 493 );
	UxPutWidth( form7, 492 );
	UxPutY( form7, 4 );
	UxPutX( form7, 4 );
	UxPutUnitType( form7, "pixels" );
	UxPutResizePolicy( form7, "resize_none" );

	UxPutForeground( label24, TextForeground );
	UxPutAlignment( label24, "alignment_beginning" );
	UxPutLabelString( label24, "Interval :" );
	UxPutFontList( label24, TextFont );
	UxPutBackground( label24, LabelBackground );
	UxPutHeight( label24, 30 );
	UxPutWidth( label24, 64 );
	UxPutY( label24, 16 );
	UxPutX( label24, 158 );

	UxPutForeground( tf_merge_step, TextForeground );
	UxPutHighlightOnEnter( tf_merge_step, "true" );
	UxPutFontList( tf_merge_step, TextFont );
	UxPutBackground( tf_merge_step, TextBackground );
	UxPutHeight( tf_merge_step, 34 );
	UxPutWidth( tf_merge_step, 50 );
	UxPutY( tf_merge_step, 14 );
	UxPutX( tf_merge_step, 410 );

	UxPutForeground( label25, TextForeground );
	UxPutAlignment( label25, "alignment_beginning" );
	UxPutLabelString( label25, "Step :" );
	UxPutFontList( label25, TextFont );
	UxPutBackground( label25, LabelBackground );
	UxPutHeight( label25, 30 );
	UxPutWidth( label25, 48 );
	UxPutY( label25, 16 );
	UxPutX( label25, 362 );

	UxPutForeground( badpix_tab_label1, TextForeground );
	UxPutAlignment( badpix_tab_label1, "alignment_beginning" );
	UxPutLabelString( badpix_tab_label1, "Output table :" );
	UxPutFontList( badpix_tab_label1, TextFont );
	UxPutBackground( badpix_tab_label1, LabelBackground );
	UxPutHeight( badpix_tab_label1, 30 );
	UxPutWidth( badpix_tab_label1, 96 );
	UxPutY( badpix_tab_label1, 56 );
	UxPutX( badpix_tab_label1, 10 );

	UxPutForeground( tf_merge_out, TextForeground );
	UxPutHighlightOnEnter( tf_merge_out, "true" );
	UxPutFontList( tf_merge_out, TextFont );
	UxPutBackground( tf_merge_out, TextBackground );
	UxPutHeight( tf_merge_out, 34 );
	UxPutWidth( tf_merge_out, 170 );
	UxPutY( tf_merge_out, 54 );
	UxPutX( tf_merge_out, 106 );

	UxPutBackground( separator6, WindowBackground );
	UxPutHeight( separator6, 6 );
	UxPutWidth( separator6, 492 );
	UxPutY( separator6, 316 );
	UxPutX( separator6, -2 );

	UxPutWordWrap( shelp_merge, "false" );
	UxPutHighlightOnEnter( shelp_merge, "false" );
	UxPutEditMode( shelp_merge, "single_line_edit" );
	UxPutFontList( shelp_merge, TextFont );
	UxPutEditable( shelp_merge, "false" );
	UxPutCursorPositionVisible( shelp_merge, "false" );
	UxPutBackground( shelp_merge, SHelpBackground );
	UxPutHeight( shelp_merge, 50 );
	UxPutWidth( shelp_merge, 490 );
	UxPutY( shelp_merge, 268 );
	UxPutX( shelp_merge, 0 );

	UxPutForeground( tf_merge_i1, TextForeground );
	UxPutHighlightOnEnter( tf_merge_i1, "true" );
	UxPutFontList( tf_merge_i1, TextFont );
	UxPutBackground( tf_merge_i1, TextBackground );
	UxPutHeight( tf_merge_i1, 34 );
	UxPutWidth( tf_merge_i1, 62 );
	UxPutY( tf_merge_i1, 14 );
	UxPutX( tf_merge_i1, 220 );

	UxPutForeground( tf_merge_i2, TextForeground );
	UxPutHighlightOnEnter( tf_merge_i2, "true" );
	UxPutFontList( tf_merge_i2, TextFont );
	UxPutBackground( tf_merge_i2, TextBackground );
	UxPutHeight( tf_merge_i2, 34 );
	UxPutWidth( tf_merge_i2, 62 );
	UxPutY( tf_merge_i2, 14 );
	UxPutX( tf_merge_i2, 282 );

	UxPutBackground( form8, ButtonBackground );
	UxPutHeight( form8, 50 );
	UxPutWidth( form8, 490 );
	UxPutY( form8, 324 );
	UxPutX( form8, 0 );
	UxPutResizePolicy( form8, "resize_none" );

	UxPutLabelString( pushButton2, "Return" );
	UxPutForeground( pushButton2, CancelForeground );
	UxPutFontList( pushButton2, BoldTextFont );
	UxPutBackground( pushButton2, ButtonBackground );
	UxPutHeight( pushButton2, 30 );
	UxPutWidth( pushButton2, 86 );
	UxPutY( pushButton2, 10 );
	UxPutX( pushButton2, 104 );

	UxPutLabelString( pb_merge_apply, "Apply" );
	UxPutForeground( pb_merge_apply, ButtonForeground );
	UxPutFontList( pb_merge_apply, BoldTextFont );
	UxPutBackground( pb_merge_apply, ButtonBackground );
	UxPutHeight( pb_merge_apply, 30 );
	UxPutWidth( pb_merge_apply, 86 );
	UxPutY( pb_merge_apply, 10 );
	UxPutX( pb_merge_apply, 10 );

	UxPutForeground( badpix_ima_label2, TextForeground );
	UxPutAlignment( badpix_ima_label2, "alignment_beginning" );
	UxPutLabelString( badpix_ima_label2, "Prefix :" );
	UxPutFontList( badpix_ima_label2, TextFont );
	UxPutBackground( badpix_ima_label2, LabelBackground );
	UxPutHeight( badpix_ima_label2, 30 );
	UxPutWidth( badpix_ima_label2, 50 );
	UxPutY( badpix_ima_label2, 16 );
	UxPutX( badpix_ima_label2, 10 );

	UxPutForeground( tf_merge_prefix, TextForeground );
	UxPutHighlightOnEnter( tf_merge_prefix, "true" );
	UxPutFontList( tf_merge_prefix, TextFont );
	UxPutBackground( tf_merge_prefix, TextBackground );
	UxPutHeight( tf_merge_prefix, 34 );
	UxPutWidth( tf_merge_prefix, 87 );
	UxPutY( tf_merge_prefix, 14 );
	UxPutX( tf_merge_prefix, 60 );

	UxPutBackground( separator7, WindowBackground );
	UxPutHeight( separator7, 6 );
	UxPutWidth( separator7, 492 );
	UxPutY( separator7, 264 );
	UxPutX( separator7, 2 );

	UxPutIndicatorSize( tg_merge_correct, 16 );
	UxPutSensitive( tg_merge_correct, "true" );
	UxPutHighlightOnEnter( tg_merge_correct, "true" );
	UxPutForeground( tg_merge_correct, TextForeground );
	UxPutSelectColor( tg_merge_correct, SelectColor );
	UxPutSet( tg_merge_correct, "true" );
	UxPutLabelString( tg_merge_correct, "Correct overlapping parts" );
	UxPutFontList( tg_merge_correct, TextFont );
	UxPutBackground( tg_merge_correct, WindowBackground );
	UxPutHeight( tg_merge_correct, 31 );
	UxPutWidth( tg_merge_correct, 190 );
	UxPutY( tg_merge_correct, 106 );
	UxPutX( tg_merge_correct, 146 );

	UxPutForeground( badpix_ima_label1, TextForeground );
	UxPutAlignment( badpix_ima_label1, "alignment_beginning" );
	UxPutLabelString( badpix_ima_label1, "Pixels excluded at edges :" );
	UxPutFontList( badpix_ima_label1, TextFont );
	UxPutBackground( badpix_ima_label1, LabelBackground );
	UxPutHeight( badpix_ima_label1, 30 );
	UxPutWidth( badpix_ima_label1, 174 );
	UxPutY( badpix_ima_label1, 144 );
	UxPutX( badpix_ima_label1, 10 );

	UxPutText( tf_merge_pixels, "0" );
	UxPutForeground( tf_merge_pixels, TextForeground );
	UxPutHighlightOnEnter( tf_merge_pixels, "true" );
	UxPutFontList( tf_merge_pixels, TextFont );
	UxPutBackground( tf_merge_pixels, TextBackground );
	UxPutHeight( tf_merge_pixels, 34 );
	UxPutWidth( tf_merge_pixels, 46 );
	UxPutY( tf_merge_pixels, 144 );
	UxPutX( tf_merge_pixels, 248 );

	UxPutForeground( badpix_ima_label3, TextForeground );
	UxPutAlignment( badpix_ima_label3, "alignment_beginning" );
	UxPutLabelString( badpix_ima_label3, "Reference frame (central frame=0) :" );
	UxPutFontList( badpix_ima_label3, TextFont );
	UxPutBackground( badpix_ima_label3, LabelBackground );
	UxPutHeight( badpix_ima_label3, 30 );
	UxPutWidth( badpix_ima_label3, 234 );
	UxPutY( badpix_ima_label3, 182 );
	UxPutX( badpix_ima_label3, 10 );

	UxPutText( tf_merge_ref, "0" );
	UxPutForeground( tf_merge_ref, TextForeground );
	UxPutHighlightOnEnter( tf_merge_ref, "true" );
	UxPutFontList( tf_merge_ref, TextFont );
	UxPutBackground( tf_merge_ref, TextBackground );
	UxPutHeight( tf_merge_ref, 34 );
	UxPutWidth( tf_merge_ref, 46 );
	UxPutY( tf_merge_ref, 180 );
	UxPutX( tf_merge_ref, 248 );

	UxPutForeground( badpix_ima_label4, TextForeground );
	UxPutAlignment( badpix_ima_label4, "alignment_beginning" );
	UxPutLabelString( badpix_ima_label4, "Digits on input frames :" );
	UxPutFontList( badpix_ima_label4, TextFont );
	UxPutBackground( badpix_ima_label4, LabelBackground );
	UxPutHeight( badpix_ima_label4, 30 );
	UxPutWidth( badpix_ima_label4, 234 );
	UxPutY( badpix_ima_label4, 218 );
	UxPutX( badpix_ima_label4, 10 );

	UxPutText( tf_merge_digits, "4" );
	UxPutForeground( tf_merge_digits, TextForeground );
	UxPutHighlightOnEnter( tf_merge_digits, "true" );
	UxPutFontList( tf_merge_digits, TextFont );
	UxPutBackground( tf_merge_digits, TextBackground );
	UxPutHeight( tf_merge_digits, 34 );
	UxPutWidth( tf_merge_digits, 46 );
	UxPutY( tf_merge_digits, 216 );
	UxPutX( tf_merge_digits, 248 );

	UxPutIndicatorSize( tg_merge_plot, 16 );
	UxPutSensitive( tg_merge_plot, "true" );
	UxPutHighlightOnEnter( tg_merge_plot, "true" );
	UxPutForeground( tg_merge_plot, TextForeground );
	UxPutSelectColor( tg_merge_plot, SelectColor );
	UxPutSet( tg_merge_plot, "true" );
	UxPutLabelString( tg_merge_plot, "Plot spectra" );
	UxPutFontList( tg_merge_plot, TextFont );
	UxPutBackground( tg_merge_plot, WindowBackground );
	UxPutHeight( tg_merge_plot, 31 );
	UxPutWidth( tg_merge_plot, 122 );
	UxPutY( tg_merge_plot, 106 );
	UxPutX( tg_merge_plot, 14 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_MergeShell()
{
	/* Create the swidgets */

	MergeShell = UxCreateApplicationShell( "MergeShell", NO_PARENT );
	UxPutContext( MergeShell, UxMergeShellContext );

	form7 = UxCreateForm( "form7", MergeShell );
	label24 = UxCreateLabel( "label24", form7 );
	tf_merge_step = UxCreateTextField( "tf_merge_step", form7 );
	label25 = UxCreateLabel( "label25", form7 );
	badpix_tab_label1 = UxCreateLabel( "badpix_tab_label1", form7 );
	tf_merge_out = UxCreateTextField( "tf_merge_out", form7 );
	separator6 = UxCreateSeparator( "separator6", form7 );
	shelp_merge = UxCreateText( "shelp_merge", form7 );
	tf_merge_i1 = UxCreateTextField( "tf_merge_i1", form7 );
	tf_merge_i2 = UxCreateTextField( "tf_merge_i2", form7 );
	form8 = UxCreateForm( "form8", form7 );
	pushButton2 = UxCreatePushButton( "pushButton2", form8 );
	pb_merge_apply = UxCreatePushButton( "pb_merge_apply", form8 );
	badpix_ima_label2 = UxCreateLabel( "badpix_ima_label2", form7 );
	tf_merge_prefix = UxCreateTextField( "tf_merge_prefix", form7 );
	separator7 = UxCreateSeparator( "separator7", form7 );
	tg_merge_correct = UxCreateToggleButton( "tg_merge_correct", form7 );
	badpix_ima_label1 = UxCreateLabel( "badpix_ima_label1", form7 );
	tf_merge_pixels = UxCreateTextField( "tf_merge_pixels", form7 );
	badpix_ima_label3 = UxCreateLabel( "badpix_ima_label3", form7 );
	tf_merge_ref = UxCreateTextField( "tf_merge_ref", form7 );
	badpix_ima_label4 = UxCreateLabel( "badpix_ima_label4", form7 );
	tf_merge_digits = UxCreateTextField( "tf_merge_digits", form7 );
	tg_merge_plot = UxCreateToggleButton( "tg_merge_plot", form7 );

	_Uxinit_MergeShell();

	/* Create the X widgets */

	UxCreateWidget( MergeShell );
	UxCreateWidget( form7 );
	UxCreateWidget( label24 );
	UxCreateWidget( tf_merge_step );
	UxCreateWidget( label25 );
	UxCreateWidget( badpix_tab_label1 );
	UxCreateWidget( tf_merge_out );
	UxCreateWidget( separator6 );
	UxCreateWidget( shelp_merge );
	UxCreateWidget( tf_merge_i1 );
	UxCreateWidget( tf_merge_i2 );
	UxCreateWidget( form8 );
	UxCreateWidget( pushButton2 );
	UxCreateWidget( pb_merge_apply );
	UxCreateWidget( badpix_ima_label2 );
	UxCreateWidget( tf_merge_prefix );
	UxCreateWidget( separator7 );
	UxCreateWidget( tg_merge_correct );
	UxCreateWidget( badpix_ima_label1 );
	UxCreateWidget( tf_merge_pixels );
	UxCreateWidget( badpix_ima_label3 );
	UxCreateWidget( tf_merge_ref );
	UxCreateWidget( badpix_ima_label4 );
	UxCreateWidget( tf_merge_digits );
	UxCreateWidget( tg_merge_plot );

	UxAddCallback( pushButton2, XmNactivateCallback,
			activateCB_pushButton2,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( pb_merge_apply, XmNactivateCallback,
			activateCB_pb_merge_apply,
			(XtPointer) UxMergeShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( MergeShell );

	return ( MergeShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_MergeShell()
{
	swidget                 rtrn;
	_UxCMergeShell          *UxContext;

	UxMergeShellContext = UxContext =
		(_UxCMergeShell *) UxMalloc( sizeof(_UxCMergeShell) );

	rtrn = _Uxbuild_MergeShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_MergeShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_MergeShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

