/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       cb_list.c                                  */
/* .AUTHORS     Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    XIrspec                                    */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Creation  27-OCT-1993         */
/*  090828	last modif				   */
/* ------------------------------------------------------- */

#include <gl_defs.h>
#include <xm_defs.h>
#include <irspec_defs.h>
#include <irspec_comm.h>
#include <UxLib.h>

#include <Xm/ToggleB.h>

#include <midas_def.h>

#define DIR_DELIMITER	'/'

extern int AppendDialogText();

extern void WriteKeyword(), SetFileList();
extern void IrspecSave(), IrspecOpen();





static char FileIn[MAXLINE];

int PopupList( list_type )
int list_type;
{
    int strip = 1;  /* strip off the directories */

    extern swidget FileListInterface;
    extern Widget FileListWidget;
    extern char  DirSpecs[];

    ListType = list_type;

    switch ( list_type ) {
	case LIST_OPEN:
	    SET_LIST_TITLE("Enter parameters file");
       	    strcpy(DirSpecs, "*.irs");
	    break;
	case LIST_LOAD_IMA:
	    SET_LIST_TITLE("Enter image to load");
       	    strcpy(DirSpecs, "*.bdf");
	    break;
	case LIST_EXT_AVER:
	case LIST_EXT_WEIGHT:
	case LIST_FIT_SKY:
	    SET_LIST_TITLE("Enter input image");
       	    strcpy(DirSpecs, "*.bdf");
	    break;
	default:
	    break;
    }

    FileListWidget = UxGetWidget(UxFindSwidget("sl_file_list"));
    SetFileList(FileListWidget, strip, DirSpecs);
    UxPopupInterface(FileListInterface, exclusive_grab);
return 0;
}

void CallbackList( choice )
char *choice;
{
    char command[2*MAXLINE], file_out[MAXLINE];
    char *sky, *sky1, *sky2, *sky3, *sky4, *order, *ron, *gain, *sigma,
         *radius;
    int i, mode;

    extern swidget FileListInterface, TextFieldSwidget;
    
    DialogType = DIALOG_EXT_WEIGHT;

    UxPopdownInterface(FileListInterface);
    switch ( ListType ) {
        case LIST_LOAD_IMA:
     	    sprintf(command, "%s%s", C_LOAD_IMA, choice);
    	    AppendDialogText(command);
    	    break;
        case LIST_OBJECT:
	    strcpy(InObj, choice);
            UxPutValue(TextFieldSwidget, choice);
    	    break;
        case LIST_SKY:
	    strcpy(InSky, choice);
            UxPutValue(TextFieldSwidget, choice);
    	    break;
        case LIST_STSTAR:
	    strcpy(InStstar, choice);
            UxPutValue(TextFieldSwidget, choice);
    	    break;
        case LIST_STSKY:
	    strcpy(InStsky, choice);
            UxPutValue(TextFieldSwidget, choice);
    	    break;
        case LIST_DARK:
	    strcpy(Dark, choice);
            UxPutValue(TextFieldSwidget, choice);
	    WriteKeyword(choice, K_DARK);
    	    break;
        case LIST_FLAT:
	    strcpy(Flat, choice);
            UxPutValue(TextFieldSwidget, choice);
	    WriteKeyword(choice, K_FLAT);
    	    break;
        case LIST_FLUX:
        case LIST_RESP:
        case LIST_STD_REF:
        case LIST_OBJ_REF:
        case LIST_OBJ_FINPUT:
            UxPutValue(TextFieldSwidget, choice);
    	    break;
        case LIST_OPEN:
	    IrspecOpen(choice);
            UxPutValue(UxFindSwidget("tf_main_object"), InObj);
            UxPutValue(UxFindSwidget("tf_main_sky"), InSky);
            UxPutValue(UxFindSwidget("tf_main_ststar"), InStstar);
            UxPutValue(UxFindSwidget("tf_main_stsky"), InStsky);
            UxPutValue(UxFindSwidget("tf_main_flat"), Flat);
            UxPutValue(UxFindSwidget("tf_main_dark"), Dark);
	    WriteKeyword(Flat, K_FLAT);
	    WriteKeyword(Dark, K_DARK);
	    strcpy(ParametersFile, choice);
    	    break;
        case LIST_FIT_SKY:
            sky = XmTextGetString(UxGetWidget(UxFindSwidget("tf_sky")));
    	    sky1 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_lowsky1")));
    	    sky2 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_lowsky2")));
    	    sky3 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_uppsky1")));
    	    sky4 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_uppsky2")));
    	    order = XmTextGetString(UxGetWidget(UxFindSwidget("tf_skyord")));
    	    gain = XmTextGetString(UxGetWidget(UxFindSwidget("tf_gain")));
    	    ron = XmTextGetString(UxGetWidget(UxFindSwidget("tf_ron")));
    	    sigma = XmTextGetString(UxGetWidget(UxFindSwidget("tf_sigma")));
    	    radius = XmTextGetString(UxGetWidget(UxFindSwidget("tf_radius")));
            mode = (XmToggleButtonGetState(UxGetWidget(UxFindSwidget("rb_skymod_0"))) ? 0 : 1);
            sprintf(command, "%s%s %s %s,%s,%s,%s %s %d %s,%s,%s %s", 
                    C_EXTR_FIT_SKY, choice, sky, sky1, sky2, sky3, sky4, order,
                    mode, gain, ron, sigma, radius);
            AppendDialogText(command);
            XtFree(sky);
            XtFree(sky1);
            XtFree(sky2);
            XtFree(sky3);
            XtFree(sky4);
            XtFree(order);
            XtFree(gain);
            XtFree(ron);
            XtFree(sigma);
            XtFree(radius);
            break;
        case LIST_EXT_AVER:
	    DialogType = DIALOG_EXT_AVER;
        case LIST_EXT_WEIGHT:
            /* generation of the output image name */
            for ( i = 0; choice[i] != '.' && choice[i] != '\0'; i++ )
             	;
            strncpy(file_out, choice, i);
            file_out[i] = '\0';
            sprintf(file_out, "%s_obj", file_out);
	    strcpy(FileIn, choice);
	    SET_DIALOG_PROMPT("Enter output image :");
            UxPutValue(UxFindSwidget("tf_file_dialog"), file_out);
    	    UxPopupInterface(UxFindSwidget("file_dialog"), exclusive_grab);
    	    break;
    }
    XtFree(choice);
}

void CallbackDialog()
{
    char command[2*MAXLINE], method[MAXLINE];
    char *out, *flat, *dark;
    char *obj, *sky, *ststar, *stsky;
    char *obj1, *obj2, *ron, *gain, *sigma, *order, *niter;
    char *sky1, *sky2, *sky3, *sky4;

    out = XmTextGetString(UxGetWidget(UxFindSwidget("tf_file_dialog")));
    switch ( DialogType ) {
	case DIALOG_SAVE_AS:
            obj = XmTextGetString(UxGetWidget(UxFindSwidget("tf_main_object")));
            sky = XmTextGetString(UxGetWidget(UxFindSwidget("tf_main_sky")));
            ststar = XmTextGetString(UxGetWidget(UxFindSwidget("tf_main_ststar")));
            stsky = XmTextGetString(UxGetWidget(UxFindSwidget("tf_main_stsky")));
            dark = XmTextGetString(UxGetWidget(UxFindSwidget("tf_main_dark")));
            flat = XmTextGetString(UxGetWidget(UxFindSwidget("tf_main_flat")));

	    strcpy(ParametersFile, out);
	    IrspecSave(out, obj, sky, ststar, stsky, dark, flat);

            XtFree(obj);
            XtFree(sky);
            XtFree(ststar);
            XtFree(stsky);
            XtFree(dark);
            XtFree(flat);
	    break;
	case DIALOG_FLAT:
            flat = XmTextGetString(UxGetWidget(UxFindSwidget("tf_main_flat")));
            dark = XmTextGetString(UxGetWidget(UxFindSwidget("tf_main_dark")));
	    if ( dark[0] == '\0' )
		strcpy(dark, "0");
	    sprintf(command, "%s%s %s %s", C_FLAT, flat, dark, out);
            AppendDialogText(command);
	    WriteKeyword(out, K_FLAT);
	    XtFree(flat);
	    XtFree(dark);
	    break;
	case DIALOG_EXT_WEIGHT:
    	    sky = XmTextGetString(UxGetWidget(UxFindSwidget("tf_sky")));
    	    obj1 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_object1")));
    	    obj2 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_object2")));
    	    order = XmTextGetString(UxGetWidget(UxFindSwidget("tf_order")));
    	    niter = XmTextGetString(UxGetWidget(UxFindSwidget("tf_niter")));
    	    ron = XmTextGetString(UxGetWidget(UxFindSwidget("tf_ron")));
    	    gain = XmTextGetString(UxGetWidget(UxFindSwidget("tf_gain")));
    	    sigma = XmTextGetString(UxGetWidget(UxFindSwidget("tf_sigma")));
            sprintf(command, "*** Sky frame: %s ***", sky);
	    SCTPUT(" "); SCTPUT(command); SCTPUT(" ");
     	    sprintf(command, "%s %s %s %s %s,%s %s,%s %s,%s,%s", 
		    C_EXTR_WEIGHTED, FileIn, out, sky, obj1, obj2, order, 
		    niter, ron, gain, sigma);
            AppendDialogText(command);
    	    XtFree(sky);
    	    XtFree(obj1);
    	    XtFree(obj2);
    	    XtFree(order);
    	    XtFree(niter);
    	    XtFree(ron);
    	    XtFree(gain);
    	    XtFree(sigma);
	    break;
	case DIALOG_EXT_AVER:
            if ( XmToggleButtonGetState(UxGetWidget(UxFindSwidget("rb_extmtd_line"))) )
		strcpy(method, "LINEAR");
	    else
		strcpy(method, "AVERAGE");
    	    obj1 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_object1")));
    	    obj2 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_object2")));
    	    sky1 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_lowsky1")));
    	    sky2 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_lowsky2")));
    	    sky3 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_uppsky1")));
    	    sky4 = XmTextGetString(UxGetWidget(UxFindSwidget("tf_uppsky2")));
     	    sprintf(command, "%s %s %s %s,%s %s,%s,%s,%s %s", C_EXTR_AVERAGE, 
                    FileIn, out, obj1, obj2, sky1, sky2, sky3, sky4, method);
            AppendDialogText(command);
    	    XtFree(sky1);
    	    XtFree(sky2);
    	    XtFree(sky3);
    	    XtFree(sky4);
    	    XtFree(obj1);
    	    XtFree(obj2);
	    break;
    	default:
	    break;
    }
    XtFree(out);
    UxPopdownInterface(UxFindSwidget("file_dialog"));
}
