/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       help.c                                     */
/* .AUTHOR      Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    XIrspec                                    */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Creation  17-MAR-1993         */
/*  090828	last modif				   */
/* ------------------------------------------------------- */

#include <gl_defs.h>
#include <xm_defs.h>
#include <UxLib.h>

#include <midas_def.h>


#define PRINT_COMMAND		"$ {syscoms(1:20)} lp"
#define PRINT_FILE		"help.out" /* temporary, it must read $SYSCOMS */
#define HELP_SHORT		"gui/XIrspec/help/irspec_short.hlp"
#define HELP_EXTENDED		"gui/XIrspec/help/irspec_extended.hlp"
#define HELP_DELIMITER		'~'

#define find_short_help(s)	find_help(HELP_SHORT, s)
#define find_extended_help(s)	find_help(HELP_EXTENDED, s)

#define MAX_HELP_LINES	80*200 	/* Max of 200 lines */

extern int AppendDialogText();



static char help_text[MAX_HELP_LINES]; 
static char print_text[MAX_HELP_LINES];

typedef struct st_help {
	char help_key[40];
	char widget_name[40];
	char widget_help[20];
} SHORT_HELP;

SHORT_HELP HelpList[] = {
	{ "MAIN_T_OBJECT", "tf_main_object", "shelp_main" },
	{ "MAIN_T_SKY", "tf_main_sky", "shelp_main" },
	{ "MAIN_T_STSTAR", "tf_main_ststar", "shelp_main" },
	{ "MAIN_T_STSKY", "tf_main_stsky", "shelp_main" },
	{ "MAIN_T_DARK", "tf_main_dark", "shelp_main" },
	{ "MAIN_T_FLAT", "tf_main_flat", "shelp_main" },
	{ "MAIN_B_BADPIX", "pb_main_badpix", "shelp_main" },
	{ "MAIN_B_FLAT", "pb_main_flat", "shelp_main" },
	{ "MAIN_B_FLUX_TABLE", "pb_main_flux_table", "shelp_main" },
	{ "MAIN_B_STANDARD", "pb_main_standard", "shelp_main" },
	{ "MAIN_B_OBJECT", "pb_main_object", "shelp_main" },
	{ "MAIN_B_EXTRACT", "pb_main_extract", "shelp_main" },
	{ "MAIN_B_MERGE", "pb_main_merge", "shelp_main" },
	{ "BADPIX_T_THRES1", "tf_badpix_thres1", "shelp_badpix" },
	{ "BADPIX_T_THRES2", "tf_badpix_thres2", "shelp_badpix" },
	{ "BADPIX_T_FRAMES", "tf_badpix_frames", "shelp_badpix" },
	{ "BADPIX_T_TABLE", "tf_badpix_table", "shelp_badpix" },
	{ "BADPIX_B_DEFINE", "pb_badpix_define", "shelp_badpix" },
	{ "BADPIX_B_APPLY", "pb_badpix_apply", "shelp_badpix" },
	{ "OBJECT_T_REF", "tf_obj_ref", "shelp_object" },
	{ "OBJECT_T_FINPUT", "tf_obj_finput", "shelp_object" },
	{ "OBJECT_T_FOUTPUT", "tf_obj_foutput", "shelp_object" },
	{ "OBJECT_T_RESP", "tf_obj_resp", "shelp_object" },
	{ "OBJECT_T_FACTOR", "tf_obj_factor", "shelp_object" },
	{ "OBJECT_T_SHIFT", "tf_obj_shift", "shelp_object" },
	{ "OBJECT_T_DELTAX", "tf_obj_deltax", "shelp_object" },
	{ "OBJECT_T_DELTAY", "tf_obj_deltay", "shelp_object" },
	{ "OBJECT_T_SKY_TABLE", "tf_obj_sky_table", "shelp_object" },
	{ "OBJECT_T_CUTS1", "tf_obj_cuts1", "shelp_object" },
	{ "OBJECT_T_CUTS2", "tf_obj_cuts2", "shelp_object" },
	{ "OBJECT_B_REDUCE", "pb_object_reduce", "shelp_object" },
	{ "OBJECT_B_FLUX", "pb_object_flux", "shelp_object" },
	{ "STANDARD_T_REF", "tf_std_ref", "shelp_standard" },
	{ "STANDARD_T_FLUX", "tf_std_flux", "shelp_standard" },
	{ "STANDARD_T_RESP", "tf_std_resp", "shelp_standard" },
	{ "STANDARD_T_YRESP1", "tf_std_yresp1", "shelp_standard" },
	{ "STANDARD_T_YRESP2", "tf_std_yresp2", "shelp_standard" },
	{ "STANDARD_T_YRESP3", "tf_std_yresp3", "shelp_standard" },
	{ "STANDARD_T_YRESP4", "tf_std_yresp4", "shelp_standard" },
	{ "STANDARD_T_FACTOR", "tf_std_factor", "shelp_standard" },
	{ "STANDARD_T_SHIFT", "tf_std_shift", "shelp_standard" },
	{ "STANDARD_T_DELTAX", "tf_std_deltax", "shelp_standard" },
	{ "STANDARD_T_DELTAY", "tf_std_deltay", "shelp_standard" },
	{ "STANDARD_T_SKY_TABLE", "tf_std_sky_table", "shelp_standard" },
	{ "STANDARD_T_CUTS1", "tf_std_cuts1", "shelp_standard" },
	{ "STANDARD_T_CUTS2", "tf_std_cuts2", "shelp_standard" },
	{ "STANDARD_B_REDUCE", "pb_standard_reduce", "shelp_standard" },
	{ "MERGE_T_PREFIX", "tf_merge_prefix", "shelp_merge" },
	{ "MERGE_T_I1", "tf_merge_i1", "shelp_merge" },
	{ "MERGE_T_I2", "tf_merge_i2", "shelp_merge" },
	{ "MERGE_T_STEP", "tf_merge_step", "shelp_merge" },
	{ "MERGE_T_OUT", "tf_merge_out", "shelp_merge" },
	{ "MERGE_T_PIXELS", "tf_merge_pixels", "shelp_merge" },
	{ "MERGE_T_REF", "tf_merge_ref", "shelp_merge" },
	{ "MERGE_T_DIGITS", "tf_merge_digits", "shelp_merge" },
	{ "MERGE_B_APPLY", "pb_merge_apply", "shelp_merge" },
	{ "FLUX_T_IN", "tf_flux_in", "shelp_flux_table" },
	{ "FLUX_T_OUT", "tf_flux_out", "shelp_flux_table" },
	{ "FLUX_T_STEP", "tf_step", "shelp_flux_table" },
	{ "FLUX_T_FITDEG", "tf_fitdeg", "shelp_flux_table" },
	{ "FLUX_B_APPLY", "pb_flux_table_apply", "shelp_flux_table" },
	{ "EXTRACT_T_SKY1", "tf_lowsky1", "shelp_extract" },
	{ "EXTRACT_T_SKY2", "tf_lowsky2", "shelp_extract" },
	{ "EXTRACT_T_SKY3", "tf_uppsky1", "shelp_extract" },
	{ "EXTRACT_T_SKY4", "tf_uppsky2", "shelp_extract" },
	{ "EXTRACT_T_OBJ1", "tf_object1", "shelp_extract" },
	{ "EXTRACT_T_OBJ2", "tf_object2", "shelp_extract" },
	{ "EXTRACT_T_SKYORD", "tf_skyord", "shelp_extract" },
	{ "EXTRACT_T_ORDER", "tf_order", "shelp_extract" },
	{ "EXTRACT_T_ITER", "tf_niter", "shelp_extract" },
	{ "EXTRACT_T_RON", "tf_ron", "shelp_extract" },
	{ "EXTRACT_T_GAIN", "tf_gain", "shelp_extract" },
	{ "EXTRACT_T_SIGMA", "tf_sigma", "shelp_extract" },
	{ "EXTRACT_T_RADIUS", "tf_radius", "shelp_extract" },
	{ "EXTRACT_T_SKY_NAME", "tf_sky", "shelp_extract" },
	{ "EXTRACT_B_SKY", "pb_ext_sky", "shelp_extract" },
	{ "EXTRACT_B_OBJECT", "pb_ext_object", "shelp_extract" },
	{ "EXTRACT_B_EXT_FIT", "pb_ext_fit", "shelp_extract" },
	{ "EXTRACT_B_AVERAGE", "pb_ext_average", "shelp_extract" },
	{ "EXTRACT_B_WEIGHT", "pb_ext_weight", "shelp_extract" },
	{ "", "" }
};

char *find_help();

void DisplayShortHelp( sw )
Widget sw; 	/* current widget */
{
    char s[256], s_help[32], w_name[40];
    int i;
 
    s[0] = '\0';

    for ( i = 0; HelpList[i].help_key[0] != '\0'; i++ ) {
	strcpy(w_name, HelpList[i].widget_name);
        if ( sw == UxGetWidget(UxFindSwidget(w_name)) ) {
      	    strcpy(s, find_short_help(HelpList[i].help_key));
	    strcpy(s_help, HelpList[i].widget_help);
	    break;
        }
    }

    if ( s[0] != '\0' )
    	UxPutText(UxFindSwidget(s_help), s);
}

void DisplayExtendedHelp( sw )
Widget sw; 	/* current widget */
{
    char s[4000];
 
    s[0] = '\0';

    if ( sw == UxGetWidget(UxFindSwidget("menu_help_context")) )
      	strcpy(s, find_extended_help("HELP_CONTEXT"));
    else if ( sw == UxGetWidget(UxFindSwidget("menu_help_help")) )
      	strcpy(s, find_extended_help("HELP_HELP"));
    else if ( sw == UxGetWidget(UxFindSwidget("menu_help_tutorial")) )
      	strcpy(s, find_extended_help("HELP_TUTORIAL"));
    else if ( sw == UxGetWidget(UxFindSwidget("menu_help_version")) )
      	strcpy(s, find_extended_help("HELP_VERSION"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_badpix")) )
      	strcpy(s, find_extended_help("MAIN_BADPIX"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_flat")) )
      	strcpy(s, find_extended_help("MAIN_FLAT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_standard")) )
      	strcpy(s, find_extended_help("MAIN_STANDARD"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_object")) )
      	strcpy(s, find_extended_help("MAIN_OBJECT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_extract")) )
      	strcpy(s, find_extended_help("MAIN_EXTRACT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_merge")) )
      	strcpy(s, find_extended_help("MAIN_MERGE"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_flux_table")) )
      	strcpy(s, find_extended_help("MAIN_FLUX"));

    else if ( sw == UxGetWidget(UxFindSwidget("pb_badpix_define")) )
      	strcpy(s, find_extended_help("BADPIX_DEFINE"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_badpix_apply")) )
      	strcpy(s, find_extended_help("BADPIX_APPLY"));

    else if ( sw == UxGetWidget(UxFindSwidget("pb_object_reduce")) )
      	strcpy(s, find_extended_help("OBJECT_REDUCE"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_object_flux")) )
      	strcpy(s, find_extended_help("OBJECT_FLUX"));

    else if ( sw == UxGetWidget(UxFindSwidget("pb_standard_reduce")) )
      	strcpy(s, find_extended_help("STANDARD_REDUCE"));

    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_table_apply")) )
      	strcpy(s, find_extended_help("FLUX_APPLY"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_merge_apply")) )
      	strcpy(s, find_extended_help("MERGE_APPLY"));

    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_sky")) )
      	strcpy(s, find_extended_help("EXTRACT_SKY"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_fit")) )
      	strcpy(s, find_extended_help("EXTRACT_FIT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_object")) )
      	strcpy(s, find_extended_help("EXTRACT_OBJECT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_average")) )
      	strcpy(s, find_extended_help("EXTRACT_AVERAGE"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_weight")) )
      	strcpy(s, find_extended_help("EXTRACT_WEIGHT"));

    if ( s[0] != '\0' ) {
    	UxPopupInterface(UxFindSwidget("HelpShell"), no_grab);
    	UxPutText(UxFindSwidget("tx_extended_help"), s);
    }

    /* let's save the text for optional printing */
    strcpy(print_text, s);
}

char *find_help( help_file, key )
char *help_file;
char *key;
{
    char midashome[512], midvers[80], help_pathname[512];
    char c, h, s[MAXLINE];
    FILE *fp;
    int idx = 0;

    osfphname("MIDVERS", midvers);
    osfphname("MIDASHOME", midashome);
    sprintf( help_pathname, "%s/%s/%s", midashome, midvers, help_file );
    if ( (fp = fopen(help_pathname, "r")) == NULL ) {
        sprintf(s, "XIrspec Help file [%s] not found.\r\n", help_pathname);
	SCTPUT(s);
        return("");
    }

    while ( (c = getc(fp)) != EOF )
	if ( c == HELP_DELIMITER && 
             !strncmp(key, fgets(s, MAXLINE, fp), strlen(key)) ) {
    	    while ( (h = getc(fp)) != EOF && h != HELP_DELIMITER )
		help_text[idx++] = h;
	    help_text[idx] = '\0';
            fclose(fp);
	    return(help_text);
	}
    fclose(fp);
    return("");

}

void PrintExtendedHelp()
{
    char s[MAXLINE];
    FILE *fp;

    fp = fopen(PRINT_FILE, "w");
    fprintf(fp, "%s", print_text);
    fclose(fp);

    sprintf(s, "%s %s", PRINT_COMMAND, PRINT_FILE);
    AppendDialogText(s);
}
