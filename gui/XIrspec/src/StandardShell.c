/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	StandardShell.c

.VERSION
 090828         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTextF.h"
#include "UxPushBG.h"
#include "UxSepG.h"
#include "UxLabel.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxStandardShell;
	swidget	Uxform2;
	swidget	Uxform4;
	swidget	UxpushButton1;
	swidget	Uxpb_standard_reduce;
	swidget	Uxshelp_standard;
	swidget	Uxseparator2;
	swidget	Uxseparator3;
	swidget	UxrowColumn1;
	swidget	Uxrb_calib_std;
	swidget	Uxrb_calib_high;
	swidget	Uxlabel12;
	swidget	UxseparatorGadget1;
	swidget	UxseparatorGadget2;
	swidget	UxseparatorGadget5;
	swidget	Uxmenu2_p3;
	swidget	Uxmn_unit_micron;
	swidget	Uxmn_unit_angstrom;
	swidget	Uxmn_unit;
	swidget	Uxtf_yresp2;
	swidget	Uxtf_yresp1;
	swidget	Uxtf_yresp3;
	swidget	Uxtf_yresp4;
	swidget	Uxmenu2_p5;
	swidget	Uxmn_ypos_inter;
	swidget	Uxmn_ypos_noint;
	swidget	Uxmn_ypos;
	swidget	Uxlabel9;
	swidget	Uxtf_std_resp;
	swidget	Uxlb_std_ref;
	swidget	Uxtf_std_ref;
	swidget	Uxtf_std_flux;
	swidget	Uxlabel10;
	swidget	Uxlabel11;
	swidget	Uxlabel22;
	swidget	Uxtf_std_factor;
	swidget	Uxtf_std_shift;
	swidget	Uxtf_std_deltax;
	swidget	Uxtf_std_deltay;
	swidget	Uxlabel23;
	swidget	Uxlabel26;
	swidget	Uxlabel27;
	swidget	Uxlabel39;
	swidget	Uxmenu2_p6;
	swidget	Uxmn_obs_mode_double;
	swidget	Uxmn_obs_mode_single;
	swidget	Uxmn_obs_mode;
	swidget	Uxtg_std_normalize;
	swidget	Uxlabel40;
	swidget	Uxtf_std_sky_table;
	swidget	Uxlabel41;
	swidget	Uxtf_std_cuts1;
	swidget	Uxtf_std_cuts2;
	swidget	Uxtg_std_sky_zero;
	swidget	Uxtg_std_debug;
} _UxCStandardShell;

#define StandardShell           UxStandardShellContext->UxStandardShell
#define form2                   UxStandardShellContext->Uxform2
#define form4                   UxStandardShellContext->Uxform4
#define pushButton1             UxStandardShellContext->UxpushButton1
#define pb_standard_reduce      UxStandardShellContext->Uxpb_standard_reduce
#define shelp_standard          UxStandardShellContext->Uxshelp_standard
#define separator2              UxStandardShellContext->Uxseparator2
#define separator3              UxStandardShellContext->Uxseparator3
#define rowColumn1              UxStandardShellContext->UxrowColumn1
#define rb_calib_std            UxStandardShellContext->Uxrb_calib_std
#define rb_calib_high           UxStandardShellContext->Uxrb_calib_high
#define label12                 UxStandardShellContext->Uxlabel12
#define separatorGadget1        UxStandardShellContext->UxseparatorGadget1
#define separatorGadget2        UxStandardShellContext->UxseparatorGadget2
#define separatorGadget5        UxStandardShellContext->UxseparatorGadget5
#define menu2_p3                UxStandardShellContext->Uxmenu2_p3
#define mn_unit_micron          UxStandardShellContext->Uxmn_unit_micron
#define mn_unit_angstrom        UxStandardShellContext->Uxmn_unit_angstrom
#define mn_unit                 UxStandardShellContext->Uxmn_unit
#define tf_yresp2               UxStandardShellContext->Uxtf_yresp2
#define tf_yresp1               UxStandardShellContext->Uxtf_yresp1
#define tf_yresp3               UxStandardShellContext->Uxtf_yresp3
#define tf_yresp4               UxStandardShellContext->Uxtf_yresp4
#define menu2_p5                UxStandardShellContext->Uxmenu2_p5
#define mn_ypos_inter           UxStandardShellContext->Uxmn_ypos_inter
#define mn_ypos_noint           UxStandardShellContext->Uxmn_ypos_noint
#define mn_ypos                 UxStandardShellContext->Uxmn_ypos
#define label9                  UxStandardShellContext->Uxlabel9
#define tf_std_resp             UxStandardShellContext->Uxtf_std_resp
#define lb_std_ref              UxStandardShellContext->Uxlb_std_ref
#define tf_std_ref              UxStandardShellContext->Uxtf_std_ref
#define tf_std_flux             UxStandardShellContext->Uxtf_std_flux
#define label10                 UxStandardShellContext->Uxlabel10
#define label11                 UxStandardShellContext->Uxlabel11
#define label22                 UxStandardShellContext->Uxlabel22
#define tf_std_factor           UxStandardShellContext->Uxtf_std_factor
#define tf_std_shift            UxStandardShellContext->Uxtf_std_shift
#define tf_std_deltax           UxStandardShellContext->Uxtf_std_deltax
#define tf_std_deltay           UxStandardShellContext->Uxtf_std_deltay
#define label23                 UxStandardShellContext->Uxlabel23
#define label26                 UxStandardShellContext->Uxlabel26
#define label27                 UxStandardShellContext->Uxlabel27
#define label39                 UxStandardShellContext->Uxlabel39
#define menu2_p6                UxStandardShellContext->Uxmenu2_p6
#define mn_obs_mode_double      UxStandardShellContext->Uxmn_obs_mode_double
#define mn_obs_mode_single      UxStandardShellContext->Uxmn_obs_mode_single
#define mn_obs_mode             UxStandardShellContext->Uxmn_obs_mode
#define tg_std_normalize        UxStandardShellContext->Uxtg_std_normalize
#define label40                 UxStandardShellContext->Uxlabel40
#define tf_std_sky_table        UxStandardShellContext->Uxtf_std_sky_table
#define label41                 UxStandardShellContext->Uxlabel41
#define tf_std_cuts1            UxStandardShellContext->Uxtf_std_cuts1
#define tf_std_cuts2            UxStandardShellContext->Uxtf_std_cuts2
#define tg_std_sky_zero         UxStandardShellContext->Uxtg_std_sky_zero
#define tg_std_debug            UxStandardShellContext->Uxtg_std_debug

static _UxCStandardShell	*UxStandardShellContext;


extern void SetFileList(),StandardReduceCallback();



/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*SelectFileStandard = "#override\n\
<Btn3Down>:FileSelectACT()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_StandardShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <irspec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	strcpy(DirSpecs, "*.bdf");
		 
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_object")) ) {
	    SET_LIST_TITLE("Enter object frame");
	    ListType = LIST_OBJECT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_sky")) ) {
	    SET_LIST_TITLE("Enter sky frame");
	    ListType = LIST_SKY;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_ststar")) ) {
	    SET_LIST_TITLE("Enter standard star");
	    ListType = LIST_STSTAR;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_stsky")) )  {
	    SET_LIST_TITLE("Enter standard sky");
	    ListType = LIST_STSKY;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_dark")) ) {
	    SET_LIST_TITLE("Enter dark frame");
	    ListType = LIST_DARK;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_main_flat")) )  {
	    SET_LIST_TITLE("Enter flat-field frame");
	    ListType = LIST_FLAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_std_flux")) )  {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX;
	    strcpy(DirSpecs, "*.tbl");
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_std_ref")) )  {
	    SET_LIST_TITLE("Enter reference frame");
	    ListType = LIST_STD_REF;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_ref")) )  {
	    SET_LIST_TITLE("Enter reference frame");
	    ListType = LIST_OBJ_REF;
	} 
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_finput")) )  {
	    SET_LIST_TITLE("Enter flux input frame");
	    ListType = LIST_OBJ_FINPUT;
	} 
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_obj_resp")) )  {
	    SET_LIST_TITLE("Enter response frame");
	    ListType = LIST_RESP;
	}
	  
	FileListWidget = UxGetWidget(UxFindSwidget("sl_file_list"));
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxStandardShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("StandardShell"));
	}
	UxStandardShellContext = UxSaveCtx;
}

static void	activateCB_pb_standard_reduce( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
	StandardReduceCallback();
	}
	UxStandardShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_calib_std( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
	if ( XmToggleButtonGetState(UxWidget) ) { /*selected */
	   XtSetSensitive(UxGetWidget(tf_std_ref), FALSE);
	   XtSetSensitive(UxGetWidget(lb_std_ref), FALSE);
	}
	else {
	   XtSetSensitive(UxGetWidget(tf_std_ref), TRUE);
	   XtSetSensitive(UxGetWidget(lb_std_ref), TRUE);
	}
	
	}
	UxStandardShellContext = UxSaveCtx;
}

static void	activateCB_mn_unit_micron( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
	}
	UxStandardShellContext = UxSaveCtx;
}

static void	activateCB_mn_unit_angstrom( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
	}
	UxStandardShellContext = UxSaveCtx;
}

static void	activateCB_mn_ypos_inter( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
	}
	UxStandardShellContext = UxSaveCtx;
}

static void	activateCB_mn_ypos_noint( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
	}
	UxStandardShellContext = UxSaveCtx;
}

static void	activateCB_mn_obs_mode_double( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
	}
	UxStandardShellContext = UxSaveCtx;
}

static void	activateCB_mn_obs_mode_single( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCStandardShell       *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxStandardShellContext;
	UxStandardShellContext = UxContext =
			(_UxCStandardShell *) UxGetContext( UxThisWidget );
	{
	}
	UxStandardShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_StandardShell()
{
	UxPutIconName( StandardShell, "StandardShell" );
	UxPutBackground( StandardShell, WindowBackground );
	UxPutGeometry( StandardShell, "+10+60" );
	UxPutKeyboardFocusPolicy( StandardShell, "pointer" );
	UxPutTitle( StandardShell, "Standard calibration" );
	UxPutHeight( StandardShell, 642 );
	UxPutWidth( StandardShell, 490 );
	UxPutY( StandardShell, 81 );
	UxPutX( StandardShell, 12 );

	UxPutLabelFontList( form2, TextFont );
	UxPutButtonFontList( form2, TextFont );
	UxPutBackground( form2, WindowBackground );
	UxPutHeight( form2, 211 );
	UxPutWidth( form2, 530 );
	UxPutY( form2, 0 );
	UxPutX( form2, 0 );
	UxPutUnitType( form2, "pixels" );
	UxPutResizePolicy( form2, "resize_none" );

	UxPutBackground( form4, ButtonBackground );
	UxPutHeight( form4, 40 );
	UxPutWidth( form4, 490 );
	UxPutY( form4, 600 );
	UxPutX( form4, 0 );
	UxPutResizePolicy( form4, "resize_none" );

	UxPutLabelString( pushButton1, "Return" );
	UxPutForeground( pushButton1, CancelForeground );
	UxPutFontList( pushButton1, BoldTextFont );
	UxPutBackground( pushButton1, ButtonBackground );
	UxPutHeight( pushButton1, 30 );
	UxPutWidth( pushButton1, 86 );
	UxPutY( pushButton1, 4 );
	UxPutX( pushButton1, 98 );

	UxPutLabelString( pb_standard_reduce, "Reduce" );
	UxPutForeground( pb_standard_reduce, ButtonForeground );
	UxPutFontList( pb_standard_reduce, BoldTextFont );
	UxPutBackground( pb_standard_reduce, ButtonBackground );
	UxPutHeight( pb_standard_reduce, 30 );
	UxPutWidth( pb_standard_reduce, 86 );
	UxPutY( pb_standard_reduce, 4 );
	UxPutX( pb_standard_reduce, 6 );

	UxPutWordWrap( shelp_standard, "false" );
	UxPutEditMode( shelp_standard, "single_line_edit" );
	UxPutFontList( shelp_standard, TextFont );
	UxPutEditable( shelp_standard, "false" );
	UxPutCursorPositionVisible( shelp_standard, "false" );
	UxPutBackground( shelp_standard, SHelpBackground );
	UxPutHeight( shelp_standard, 50 );
	UxPutWidth( shelp_standard, 490 );
	UxPutY( shelp_standard, 542 );
	UxPutX( shelp_standard, 0 );

	UxPutBackground( separator2, WindowBackground );
	UxPutHeight( separator2, 6 );
	UxPutWidth( separator2, 492 );
	UxPutY( separator2, 536 );
	UxPutX( separator2, 2 );

	UxPutBackground( separator3, WindowBackground );
	UxPutHeight( separator3, 10 );
	UxPutWidth( separator3, 492 );
	UxPutY( separator3, 590 );
	UxPutX( separator3, 0 );

	UxPutIsAligned( rowColumn1, "true" );
	UxPutEntryAlignment( rowColumn1, "alignment_beginning" );
	UxPutBorderWidth( rowColumn1, 0 );
	UxPutShadowThickness( rowColumn1, 0 );
	UxPutLabelString( rowColumn1, "" );
	UxPutEntryBorder( rowColumn1, 0 );
	UxPutBackground( rowColumn1, "grey80" );
	UxPutRadioBehavior( rowColumn1, "true" );
	UxPutHeight( rowColumn1, 67 );
	UxPutWidth( rowColumn1, 82 );
	UxPutY( rowColumn1, 38 );
	UxPutX( rowColumn1, 61 );

	UxPutIndicatorSize( rb_calib_std, 16 );
	UxPutHighlightOnEnter( rb_calib_std, "true" );
	UxPutForeground( rb_calib_std, TextForeground );
	UxPutSelectColor( rb_calib_std, SelectColor );
	UxPutSet( rb_calib_std, "true" );
	UxPutLabelString( rb_calib_std, "Standard" );
	UxPutFontList( rb_calib_std, TextFont );
	UxPutBackground( rb_calib_std, WindowBackground );
	UxPutHeight( rb_calib_std, 20 );
	UxPutWidth( rb_calib_std, 76 );
	UxPutY( rb_calib_std, 0 );
	UxPutX( rb_calib_std, -12 );

	UxPutIndicatorSize( rb_calib_high, 16 );
	UxPutHighlightOnEnter( rb_calib_high, "true" );
	UxPutSet( rb_calib_high, "false" );
	UxPutForeground( rb_calib_high, TextForeground );
	UxPutSelectColor( rb_calib_high, SelectColor );
	UxPutLabelString( rb_calib_high, "High accuracy" );
	UxPutFontList( rb_calib_high, TextFont );
	UxPutBackground( rb_calib_high, WindowBackground );
	UxPutHeight( rb_calib_high, 66 );
	UxPutWidth( rb_calib_high, 76 );
	UxPutY( rb_calib_high, 34 );
	UxPutX( rb_calib_high, 3 );

	UxPutForeground( label12, TextForeground );
	UxPutAlignment( label12, "alignment_beginning" );
	UxPutLabelString( label12, "Wavelength calibration" );
	UxPutFontList( label12, TextFont );
	UxPutBackground( label12, WindowBackground );
	UxPutHeight( label12, 22 );
	UxPutWidth( label12, 162 );
	UxPutY( label12, 14 );
	UxPutX( label12, 48 );

	UxPutOrientation( separatorGadget1, "vertical" );
	UxPutHeight( separatorGadget1, 84 );
	UxPutWidth( separatorGadget1, 12 );
	UxPutY( separatorGadget1, 26 );
	UxPutX( separatorGadget1, 32 );

	UxPutOrientation( separatorGadget2, "vertical" );
	UxPutHeight( separatorGadget2, 84 );
	UxPutWidth( separatorGadget2, 12 );
	UxPutY( separatorGadget2, 26 );
	UxPutX( separatorGadget2, 207 );

	UxPutHeight( separatorGadget5, 8 );
	UxPutWidth( separatorGadget5, 178 );
	UxPutY( separatorGadget5, 106 );
	UxPutX( separatorGadget5, 37 );

	UxPutForeground( menu2_p3, TextForeground );
	UxPutBackground( menu2_p3, WindowBackground );
	UxPutRowColumnType( menu2_p3, "menu_pulldown" );

	UxPutFontList( mn_unit_micron, TextFont );
	UxPutLabelString( mn_unit_micron, "microns" );

	UxPutFontList( mn_unit_angstrom, TextFont );
	UxPutLabelString( mn_unit_angstrom, "angstroms" );

	UxPutLabelString( mn_unit, "Units : " );
	UxPutSpacing( mn_unit, 0 );
	UxPutMarginWidth( mn_unit, 0 );
	UxPutForeground( mn_unit, TextForeground );
	UxPutBackground( mn_unit, WindowBackground );
	UxPutY( mn_unit, 22 );
	UxPutX( mn_unit, 242 );
	UxPutRowColumnType( mn_unit, "menu_option" );

	UxPutMaxLength( tf_yresp2, 5 );
	UxPutForeground( tf_yresp2, TextForeground );
	UxPutHighlightOnEnter( tf_yresp2, "true" );
	UxPutFontList( tf_yresp2, TextFont );
	UxPutBackground( tf_yresp2, TextBackground );
	UxPutHeight( tf_yresp2, 34 );
	UxPutWidth( tf_yresp2, 56 );
	UxPutY( tf_yresp2, 287 );
	UxPutX( tf_yresp2, 306 );

	UxPutMaxLength( tf_yresp1, 5 );
	UxPutForeground( tf_yresp1, TextForeground );
	UxPutHighlightOnEnter( tf_yresp1, "true" );
	UxPutFontList( tf_yresp1, TextFont );
	UxPutBackground( tf_yresp1, TextBackground );
	UxPutHeight( tf_yresp1, 34 );
	UxPutWidth( tf_yresp1, 56 );
	UxPutY( tf_yresp1, 287 );
	UxPutX( tf_yresp1, 253 );

	UxPutMaxLength( tf_yresp3, 5 );
	UxPutForeground( tf_yresp3, TextForeground );
	UxPutHighlightOnEnter( tf_yresp3, "true" );
	UxPutFontList( tf_yresp3, TextFont );
	UxPutBackground( tf_yresp3, TextBackground );
	UxPutHeight( tf_yresp3, 34 );
	UxPutWidth( tf_yresp3, 56 );
	UxPutY( tf_yresp3, 287 );
	UxPutX( tf_yresp3, 360 );

	UxPutMaxLength( tf_yresp4, 5 );
	UxPutForeground( tf_yresp4, TextForeground );
	UxPutHighlightOnEnter( tf_yresp4, "true" );
	UxPutFontList( tf_yresp4, TextFont );
	UxPutBackground( tf_yresp4, TextBackground );
	UxPutHeight( tf_yresp4, 34 );
	UxPutWidth( tf_yresp4, 56 );
	UxPutY( tf_yresp4, 287 );
	UxPutX( tf_yresp4, 414 );

	UxPutForeground( menu2_p5, TextForeground );
	UxPutBackground( menu2_p5, WindowBackground );
	UxPutRowColumnType( menu2_p5, "menu_pulldown" );

	UxPutFontList( mn_ypos_inter, TextFont );
	UxPutLabelString( mn_ypos_inter, "interactive" );

	UxPutFontList( mn_ypos_noint, TextFont );
	UxPutLabelString( mn_ypos_noint, "non-interactive" );

	UxPutLabelString( mn_ypos, "Y-positions : " );
	UxPutSpacing( mn_ypos, 0 );
	UxPutMarginWidth( mn_ypos, 0 );
	UxPutForeground( mn_ypos, TextForeground );
	UxPutBackground( mn_ypos, WindowBackground );
	UxPutY( mn_ypos, 287 );
	UxPutX( mn_ypos, 16 );
	UxPutRowColumnType( mn_ypos, "menu_option" );

	UxPutForeground( label9, TextForeground );
	UxPutAlignment( label9, "alignment_beginning" );
	UxPutLabelString( label9, "Response frame :" );
	UxPutFontList( label9, TextFont );
	UxPutBackground( label9, LabelBackground );
	UxPutHeight( label9, 30 );
	UxPutWidth( label9, 118 );
	UxPutY( label9, 250 );
	UxPutX( label9, 16 );

	UxPutForeground( tf_std_resp, TextForeground );
	UxPutHighlightOnEnter( tf_std_resp, "true" );
	UxPutFontList( tf_std_resp, TextFont );
	UxPutBackground( tf_std_resp, TextBackground );
	UxPutHeight( tf_std_resp, 34 );
	UxPutWidth( tf_std_resp, 191 );
	UxPutY( tf_std_resp, 246 );
	UxPutX( tf_std_resp, 140 );

	UxPutSensitive( lb_std_ref, "false" );
	UxPutForeground( lb_std_ref, TextForeground );
	UxPutAlignment( lb_std_ref, "alignment_beginning" );
	UxPutLabelString( lb_std_ref, "Reference frame :" );
	UxPutFontList( lb_std_ref, TextFont );
	UxPutBackground( lb_std_ref, LabelBackground );
	UxPutHeight( lb_std_ref, 30 );
	UxPutWidth( lb_std_ref, 122 );
	UxPutY( lb_std_ref, 126 );
	UxPutX( lb_std_ref, 16 );

	UxPutSensitive( tf_std_ref, "false" );
	UxPutTranslations( tf_std_ref, SelectFileStandard );
	UxPutForeground( tf_std_ref, TextForeground );
	UxPutHighlightOnEnter( tf_std_ref, "true" );
	UxPutFontList( tf_std_ref, TextFont );
	UxPutBackground( tf_std_ref, TextBackground );
	UxPutHeight( tf_std_ref, 34 );
	UxPutWidth( tf_std_ref, 189 );
	UxPutY( tf_std_ref, 122 );
	UxPutX( tf_std_ref, 140 );

	UxPutTranslations( tf_std_flux, SelectFileStandard );
	UxPutForeground( tf_std_flux, TextForeground );
	UxPutHighlightOnEnter( tf_std_flux, "true" );
	UxPutFontList( tf_std_flux, TextFont );
	UxPutBackground( tf_std_flux, TextBackground );
	UxPutHeight( tf_std_flux, 34 );
	UxPutWidth( tf_std_flux, 192 );
	UxPutY( tf_std_flux, 204 );
	UxPutX( tf_std_flux, 140 );

	UxPutForeground( label10, TextForeground );
	UxPutAlignment( label10, "alignment_beginning" );
	UxPutLabelString( label10, "Flux table :" );
	UxPutFontList( label10, TextFont );
	UxPutBackground( label10, LabelBackground );
	UxPutHeight( label10, 30 );
	UxPutWidth( label10, 76 );
	UxPutY( label10, 210 );
	UxPutX( label10, 16 );

	UxPutForeground( label11, TextForeground );
	UxPutAlignment( label11, "alignment_beginning" );
	UxPutLabelString( label11, "SKY SUBTRACTION PARAMETERS" );
	UxPutFontList( label11, TextFont );
	UxPutBackground( label11, LabelBackground );
	UxPutHeight( label11, 30 );
	UxPutWidth( label11, 246 );
	UxPutY( label11, 382 );
	UxPutX( label11, 5 );

	UxPutForeground( label22, TextForeground );
	UxPutAlignment( label22, "alignment_beginning" );
	UxPutLabelString( label22, "Factor :" );
	UxPutFontList( label22, TextFont );
	UxPutBackground( label22, LabelBackground );
	UxPutHeight( label22, 30 );
	UxPutWidth( label22, 54 );
	UxPutY( label22, 418 );
	UxPutX( label22, 16 );

	UxPutText( tf_std_factor, "1" );
	UxPutMaxLength( tf_std_factor, 5 );
	UxPutForeground( tf_std_factor, TextForeground );
	UxPutHighlightOnEnter( tf_std_factor, "true" );
	UxPutFontList( tf_std_factor, TextFont );
	UxPutBackground( tf_std_factor, TextBackground );
	UxPutHeight( tf_std_factor, 34 );
	UxPutWidth( tf_std_factor, 56 );
	UxPutY( tf_std_factor, 414 );
	UxPutX( tf_std_factor, 69 );

	UxPutMaxLength( tf_std_shift, 5 );
	UxPutForeground( tf_std_shift, TextForeground );
	UxPutHighlightOnEnter( tf_std_shift, "true" );
	UxPutFontList( tf_std_shift, TextFont );
	UxPutBackground( tf_std_shift, TextBackground );
	UxPutHeight( tf_std_shift, 34 );
	UxPutWidth( tf_std_shift, 56 );
	UxPutY( tf_std_shift, 414 );
	UxPutX( tf_std_shift, 175 );

	UxPutMaxLength( tf_std_deltax, 5 );
	UxPutForeground( tf_std_deltax, TextForeground );
	UxPutHighlightOnEnter( tf_std_deltax, "true" );
	UxPutFontList( tf_std_deltax, TextFont );
	UxPutBackground( tf_std_deltax, TextBackground );
	UxPutHeight( tf_std_deltax, 34 );
	UxPutWidth( tf_std_deltax, 56 );
	UxPutY( tf_std_deltax, 414 );
	UxPutX( tf_std_deltax, 297 );

	UxPutMaxLength( tf_std_deltay, 5 );
	UxPutForeground( tf_std_deltay, TextForeground );
	UxPutHighlightOnEnter( tf_std_deltay, "true" );
	UxPutFontList( tf_std_deltay, TextFont );
	UxPutBackground( tf_std_deltay, TextBackground );
	UxPutHeight( tf_std_deltay, 34 );
	UxPutWidth( tf_std_deltay, 56 );
	UxPutY( tf_std_deltay, 414 );
	UxPutX( tf_std_deltay, 417 );

	UxPutForeground( label23, TextForeground );
	UxPutAlignment( label23, "alignment_beginning" );
	UxPutLabelString( label23, "Shift :" );
	UxPutFontList( label23, TextFont );
	UxPutBackground( label23, LabelBackground );
	UxPutHeight( label23, 30 );
	UxPutWidth( label23, 46 );
	UxPutY( label23, 418 );
	UxPutX( label23, 133 );

	UxPutForeground( label26, TextForeground );
	UxPutAlignment( label26, "alignment_beginning" );
	UxPutLabelString( label26, "Deltax :" );
	UxPutFontList( label26, TextFont );
	UxPutBackground( label26, LabelBackground );
	UxPutHeight( label26, 30 );
	UxPutWidth( label26, 58 );
	UxPutY( label26, 418 );
	UxPutX( label26, 239 );

	UxPutForeground( label27, TextForeground );
	UxPutAlignment( label27, "alignment_beginning" );
	UxPutLabelString( label27, "Deltay :" );
	UxPutFontList( label27, TextFont );
	UxPutBackground( label27, LabelBackground );
	UxPutHeight( label27, 30 );
	UxPutWidth( label27, 58 );
	UxPutY( label27, 418 );
	UxPutX( label27, 359 );

	UxPutForeground( label39, TextForeground );
	UxPutAlignment( label39, "alignment_beginning" );
	UxPutLabelString( label39, "RESPONSE GENERATION PARAMETERS" );
	UxPutFontList( label39, TextFont );
	UxPutBackground( label39, LabelBackground );
	UxPutHeight( label39, 30 );
	UxPutWidth( label39, 284 );
	UxPutY( label39, 171 );
	UxPutX( label39, 7 );

	UxPutForeground( menu2_p6, TextForeground );
	UxPutBackground( menu2_p6, WindowBackground );
	UxPutRowColumnType( menu2_p6, "menu_pulldown" );

	UxPutFontList( mn_obs_mode_double, TextFont );
	UxPutLabelString( mn_obs_mode_double, "two frames" );

	UxPutFontList( mn_obs_mode_single, TextFont );
	UxPutLabelString( mn_obs_mode_single, "single frame" );

	UxPutLabelString( mn_obs_mode, "Observing mode : " );
	UxPutSpacing( mn_obs_mode, 0 );
	UxPutMarginWidth( mn_obs_mode, 0 );
	UxPutForeground( mn_obs_mode, TextForeground );
	UxPutBackground( mn_obs_mode, WindowBackground );
	UxPutY( mn_obs_mode, 327 );
	UxPutX( mn_obs_mode, 16 );
	UxPutRowColumnType( mn_obs_mode, "menu_option" );

	UxPutIndicatorSize( tg_std_normalize, 16 );
	UxPutSensitive( tg_std_normalize, "true" );
	UxPutHighlightOnEnter( tg_std_normalize, "true" );
	UxPutForeground( tg_std_normalize, TextForeground );
	UxPutSelectColor( tg_std_normalize, SelectColor );
	UxPutSet( tg_std_normalize, "true" );
	UxPutLabelString( tg_std_normalize, "Automatic normalization" );
	UxPutFontList( tg_std_normalize, TextFont );
	UxPutBackground( tg_std_normalize, WindowBackground );
	UxPutHeight( tg_std_normalize, 31 );
	UxPutWidth( tg_std_normalize, 186 );
	UxPutY( tg_std_normalize, 331 );
	UxPutX( tg_std_normalize, 277 );

	UxPutForeground( label40, TextForeground );
	UxPutAlignment( label40, "alignment_beginning" );
	UxPutLabelString( label40, "Sky table :" );
	UxPutFontList( label40, TextFont );
	UxPutBackground( label40, LabelBackground );
	UxPutHeight( label40, 30 );
	UxPutWidth( label40, 78 );
	UxPutY( label40, 456 );
	UxPutX( label40, 14 );

	UxPutText( tf_std_sky_table, "cursor" );
	UxPutTranslations( tf_std_sky_table, SelectFileStandard );
	UxPutForeground( tf_std_sky_table, TextForeground );
	UxPutHighlightOnEnter( tf_std_sky_table, "true" );
	UxPutFontList( tf_std_sky_table, TextFont );
	UxPutBackground( tf_std_sky_table, TextBackground );
	UxPutHeight( tf_std_sky_table, 34 );
	UxPutWidth( tf_std_sky_table, 171 );
	UxPutY( tf_std_sky_table, 454 );
	UxPutX( tf_std_sky_table, 91 );

	UxPutForeground( label41, TextForeground );
	UxPutAlignment( label41, "alignment_beginning" );
	UxPutLabelString( label41, "Cuts :" );
	UxPutFontList( label41, TextFont );
	UxPutBackground( label41, LabelBackground );
	UxPutHeight( label41, 30 );
	UxPutWidth( label41, 48 );
	UxPutY( label41, 456 );
	UxPutX( label41, 273 );

	UxPutText( tf_std_cuts1, "0" );
	UxPutMaxLength( tf_std_cuts1, 5 );
	UxPutForeground( tf_std_cuts1, TextForeground );
	UxPutHighlightOnEnter( tf_std_cuts1, "true" );
	UxPutFontList( tf_std_cuts1, TextFont );
	UxPutBackground( tf_std_cuts1, TextBackground );
	UxPutHeight( tf_std_cuts1, 34 );
	UxPutWidth( tf_std_cuts1, 56 );
	UxPutY( tf_std_cuts1, 454 );
	UxPutX( tf_std_cuts1, 321 );

	UxPutText( tf_std_cuts2, "0" );
	UxPutMaxLength( tf_std_cuts2, 5 );
	UxPutForeground( tf_std_cuts2, TextForeground );
	UxPutHighlightOnEnter( tf_std_cuts2, "true" );
	UxPutFontList( tf_std_cuts2, TextFont );
	UxPutBackground( tf_std_cuts2, TextBackground );
	UxPutHeight( tf_std_cuts2, 34 );
	UxPutWidth( tf_std_cuts2, 56 );
	UxPutY( tf_std_cuts2, 454 );
	UxPutX( tf_std_cuts2, 381 );

	UxPutIndicatorSize( tg_std_sky_zero, 16 );
	UxPutSensitive( tg_std_sky_zero, "true" );
	UxPutHighlightOnEnter( tg_std_sky_zero, "true" );
	UxPutForeground( tg_std_sky_zero, TextForeground );
	UxPutSelectColor( tg_std_sky_zero, SelectColor );
	UxPutSet( tg_std_sky_zero, "false" );
	UxPutLabelString( tg_std_sky_zero, "Force sky to zero" );
	UxPutFontList( tg_std_sky_zero, TextFont );
	UxPutBackground( tg_std_sky_zero, WindowBackground );
	UxPutHeight( tg_std_sky_zero, 31 );
	UxPutWidth( tg_std_sky_zero, 143 );
	UxPutY( tg_std_sky_zero, 494 );
	UxPutX( tg_std_sky_zero, 12 );

	UxPutIndicatorSize( tg_std_debug, 16 );
	UxPutSensitive( tg_std_debug, "true" );
	UxPutHighlightOnEnter( tg_std_debug, "true" );
	UxPutForeground( tg_std_debug, TextForeground );
	UxPutSelectColor( tg_std_debug, SelectColor );
	UxPutSet( tg_std_debug, "false" );
	UxPutLabelString( tg_std_debug, "Debug mode" );
	UxPutFontList( tg_std_debug, TextFont );
	UxPutBackground( tg_std_debug, WindowBackground );
	UxPutHeight( tg_std_debug, 31 );
	UxPutWidth( tg_std_debug, 111 );
	UxPutY( tg_std_debug, 494 );
	UxPutX( tg_std_debug, 172 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_StandardShell()
{
	/* Create the swidgets */

	StandardShell = UxCreateApplicationShell( "StandardShell", NO_PARENT );
	UxPutContext( StandardShell, UxStandardShellContext );

	form2 = UxCreateForm( "form2", StandardShell );
	form4 = UxCreateForm( "form4", form2 );
	pushButton1 = UxCreatePushButton( "pushButton1", form4 );
	pb_standard_reduce = UxCreatePushButton( "pb_standard_reduce", form4 );
	shelp_standard = UxCreateText( "shelp_standard", form2 );
	separator2 = UxCreateSeparator( "separator2", form2 );
	separator3 = UxCreateSeparator( "separator3", form2 );
	rowColumn1 = UxCreateRowColumn( "rowColumn1", form2 );
	rb_calib_std = UxCreateToggleButton( "rb_calib_std", rowColumn1 );
	rb_calib_high = UxCreateToggleButton( "rb_calib_high", rowColumn1 );
	label12 = UxCreateLabel( "label12", form2 );
	separatorGadget1 = UxCreateSeparatorGadget( "separatorGadget1", form2 );
	separatorGadget2 = UxCreateSeparatorGadget( "separatorGadget2", form2 );
	separatorGadget5 = UxCreateSeparatorGadget( "separatorGadget5", form2 );
	menu2_p3 = UxCreateRowColumn( "menu2_p3", form2 );
	mn_unit_micron = UxCreatePushButtonGadget( "mn_unit_micron", menu2_p3 );
	mn_unit_angstrom = UxCreatePushButtonGadget( "mn_unit_angstrom", menu2_p3 );
	mn_unit = UxCreateRowColumn( "mn_unit", form2 );
	tf_yresp2 = UxCreateTextField( "tf_yresp2", form2 );
	tf_yresp1 = UxCreateTextField( "tf_yresp1", form2 );
	tf_yresp3 = UxCreateTextField( "tf_yresp3", form2 );
	tf_yresp4 = UxCreateTextField( "tf_yresp4", form2 );
	menu2_p5 = UxCreateRowColumn( "menu2_p5", form2 );
	mn_ypos_inter = UxCreatePushButtonGadget( "mn_ypos_inter", menu2_p5 );
	mn_ypos_noint = UxCreatePushButtonGadget( "mn_ypos_noint", menu2_p5 );
	mn_ypos = UxCreateRowColumn( "mn_ypos", form2 );
	label9 = UxCreateLabel( "label9", form2 );
	tf_std_resp = UxCreateTextField( "tf_std_resp", form2 );
	lb_std_ref = UxCreateLabel( "lb_std_ref", form2 );
	tf_std_ref = UxCreateTextField( "tf_std_ref", form2 );
	tf_std_flux = UxCreateTextField( "tf_std_flux", form2 );
	label10 = UxCreateLabel( "label10", form2 );
	label11 = UxCreateLabel( "label11", form2 );
	label22 = UxCreateLabel( "label22", form2 );
	tf_std_factor = UxCreateTextField( "tf_std_factor", form2 );
	tf_std_shift = UxCreateTextField( "tf_std_shift", form2 );
	tf_std_deltax = UxCreateTextField( "tf_std_deltax", form2 );
	tf_std_deltay = UxCreateTextField( "tf_std_deltay", form2 );
	label23 = UxCreateLabel( "label23", form2 );
	label26 = UxCreateLabel( "label26", form2 );
	label27 = UxCreateLabel( "label27", form2 );
	label39 = UxCreateLabel( "label39", form2 );
	menu2_p6 = UxCreateRowColumn( "menu2_p6", form2 );
	mn_obs_mode_double = UxCreatePushButtonGadget( "mn_obs_mode_double", menu2_p6 );
	mn_obs_mode_single = UxCreatePushButtonGadget( "mn_obs_mode_single", menu2_p6 );
	mn_obs_mode = UxCreateRowColumn( "mn_obs_mode", form2 );
	tg_std_normalize = UxCreateToggleButton( "tg_std_normalize", form2 );
	label40 = UxCreateLabel( "label40", form2 );
	tf_std_sky_table = UxCreateTextField( "tf_std_sky_table", form2 );
	label41 = UxCreateLabel( "label41", form2 );
	tf_std_cuts1 = UxCreateTextField( "tf_std_cuts1", form2 );
	tf_std_cuts2 = UxCreateTextField( "tf_std_cuts2", form2 );
	tg_std_sky_zero = UxCreateToggleButton( "tg_std_sky_zero", form2 );
	tg_std_debug = UxCreateToggleButton( "tg_std_debug", form2 );

	_Uxinit_StandardShell();

	/* Create the X widgets */

	UxCreateWidget( StandardShell );
	UxCreateWidget( form2 );
	UxCreateWidget( form4 );
	UxCreateWidget( pushButton1 );
	UxCreateWidget( pb_standard_reduce );
	UxCreateWidget( shelp_standard );
	UxCreateWidget( separator2 );
	UxCreateWidget( separator3 );
	UxCreateWidget( rowColumn1 );
	UxCreateWidget( rb_calib_std );
	UxCreateWidget( rb_calib_high );
	UxCreateWidget( label12 );
	UxCreateWidget( separatorGadget1 );
	UxCreateWidget( separatorGadget2 );
	UxCreateWidget( separatorGadget5 );
	UxCreateWidget( menu2_p3 );
	UxCreateWidget( mn_unit_micron );
	UxCreateWidget( mn_unit_angstrom );
	UxPutSubMenuId( mn_unit, "menu2_p3" );
	UxCreateWidget( mn_unit );

	UxCreateWidget( tf_yresp2 );
	UxCreateWidget( tf_yresp1 );
	UxCreateWidget( tf_yresp3 );
	UxCreateWidget( tf_yresp4 );
	UxCreateWidget( menu2_p5 );
	UxCreateWidget( mn_ypos_inter );
	UxCreateWidget( mn_ypos_noint );
	UxPutSubMenuId( mn_ypos, "menu2_p5" );
	UxCreateWidget( mn_ypos );

	UxCreateWidget( label9 );
	UxCreateWidget( tf_std_resp );
	UxCreateWidget( lb_std_ref );
	UxCreateWidget( tf_std_ref );
	UxCreateWidget( tf_std_flux );
	UxCreateWidget( label10 );
	UxCreateWidget( label11 );
	UxCreateWidget( label22 );
	UxCreateWidget( tf_std_factor );
	UxCreateWidget( tf_std_shift );
	UxCreateWidget( tf_std_deltax );
	UxCreateWidget( tf_std_deltay );
	UxCreateWidget( label23 );
	UxCreateWidget( label26 );
	UxCreateWidget( label27 );
	UxCreateWidget( label39 );
	UxCreateWidget( menu2_p6 );
	UxCreateWidget( mn_obs_mode_double );
	UxCreateWidget( mn_obs_mode_single );
	UxPutSubMenuId( mn_obs_mode, "menu2_p6" );
	UxCreateWidget( mn_obs_mode );

	UxCreateWidget( tg_std_normalize );
	UxCreateWidget( label40 );
	UxCreateWidget( tf_std_sky_table );
	UxCreateWidget( label41 );
	UxCreateWidget( tf_std_cuts1 );
	UxCreateWidget( tf_std_cuts2 );
	UxCreateWidget( tg_std_sky_zero );
	UxCreateWidget( tg_std_debug );

	UxAddCallback( pushButton1, XmNactivateCallback,
			activateCB_pushButton1,
			(XtPointer) UxStandardShellContext );

	UxAddCallback( pb_standard_reduce, XmNactivateCallback,
			activateCB_pb_standard_reduce,
			(XtPointer) UxStandardShellContext );

	UxAddCallback( rb_calib_std, XmNvalueChangedCallback,
			valueChangedCB_rb_calib_std,
			(XtPointer) UxStandardShellContext );

	UxAddCallback( mn_unit_micron, XmNactivateCallback,
			activateCB_mn_unit_micron,
			(XtPointer) UxStandardShellContext );

	UxAddCallback( mn_unit_angstrom, XmNactivateCallback,
			activateCB_mn_unit_angstrom,
			(XtPointer) UxStandardShellContext );

	UxAddCallback( mn_ypos_inter, XmNactivateCallback,
			activateCB_mn_ypos_inter,
			(XtPointer) UxStandardShellContext );

	UxAddCallback( mn_ypos_noint, XmNactivateCallback,
			activateCB_mn_ypos_noint,
			(XtPointer) UxStandardShellContext );

	UxAddCallback( mn_obs_mode_double, XmNactivateCallback,
			activateCB_mn_obs_mode_double,
			(XtPointer) UxStandardShellContext );

	UxAddCallback( mn_obs_mode_single, XmNactivateCallback,
			activateCB_mn_obs_mode_single,
			(XtPointer) UxStandardShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( StandardShell );

	return ( StandardShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_StandardShell()
{
	swidget                 rtrn;
	_UxCStandardShell       *UxContext;

	UxStandardShellContext = UxContext =
		(_UxCStandardShell *) UxMalloc( sizeof(_UxCStandardShell) );

	rtrn = _Uxbuild_StandardShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_StandardShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_StandardShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

