! @(#)irsgcoord.prg	19.1 (ES0-DMD) 02/25/03 13:47:33
! @(#)irsgcoord.prg	19.1 (ESO-ASD) 02/25/03 13:47:33
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       irsgcoord.prg
!.AUTHORS     Cristian Levin   (ESO/La Silla)
!.KEYWORDS    XIrspec
!.PURPOSE     
!      Get coordinates from the display window.
!      Command:
!         @g irsgcoord  number_of_coords
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
define/par p1 1 N  "Number of coordinates : "
define/par p2 ? C  "Output table          : "
define/maxpar   2
!
if 'dazdevr(10)' .eq. -1 then
    write/out "*** Please create the display window and load an image ***"
    return/exit
endif
!
get/curs 'p2' ? ? 'p1',1
comp/tab 'p2' :y_wcoord = 1.0 + (:y_coord - '{idimemc},start(2)') / '{idimemc},step(2)'
