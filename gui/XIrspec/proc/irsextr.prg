! @(#)irsextr.prg	19.1 (ES0-DMD) 02/25/03 13:47:33
! @(#)irsextr.prg	19.1 (ESO-ASD) 02/25/03 13:47:33
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       irsextr.prg
!.AUTHOR      Cristian Levin   (ESO/La Silla)
!.KEYWORDS    XIrspec
!.PURPOSE     
!   Execute the command :
!      @g irsextr out in sky limits order,iter ron,g,thresh 
!.VERSION     1.0  Package Creation  17-MAR-1993  
!-------------------------------------------------------
!
CROSSREF  in  out  sky  obj  
!
DEFINE/PARA P1 ?   I  "input frame:"
DEFINE/PARA P2 ?   I  "output frame:"
DEFINE/PARA P3 0.  ?  "sky [frame or constant]:"
DEFINE/PARA P4 ?   N  "object limits c1,c2 in pixels:"
DEFINE/PARA P5 ?   N  "order, iter:"     
DEFINE/PARA P6 ?   N  "ron, g, thresh:"  
!
DEFINE/MAXPAR  6
!
WRITE/KEY   INPUTI/I/1/4  {P4},{P5}
WRITE/KEY   INPUTR/R/1/3  {P6}
!
DEFINE/LOCAL SKY/C/1/80 " " ALL
!
! check, if sky is a number or a frame
!
INPUTI(1) = M$TSTNO(P3)
IF INPUTI(1) .EQ. 1 THEN                 ! sky is a number
  CREA/IMA &s = 'P1' POLY 'P3'
  WRITE/KEY SKY middumms
ELSE                                     ! sky is a frame
  WRITE/KEY SKY 'P3'                    
ENDIF
!
WRITE/OUT "extract useful part"
!
WRITE/KEYW INPUTI/I/1/2  'P4'
!
EXTR/IMA   &a =  'P1'[<,@'INPUTI(1)':>,@'INPUTI(2)']
EXTR/IMA   &b =  'SKY'[<,@'INPUTI(1)':>,@'INPUTI(2)']
!
WRITE/OUT "For comparison: frame middummd.bdf contains"
WRITE/OUT "result with equal weights and no mask for cosmics"
!
COMP/IMA   &c = &a - &b
AVER/ROW   &d = &c <,> NO
!
WRITE/OUT "extract weighted mean"
! 
WRITE/KEY IN_A       middumma
WRITE/KEY IN_B       middummb
WRITE/KEY OUT_A        'P2'
WRITE/KEY INPUTI       'P5'
WRITE/KEY INPUTR       'P6'
!
RUN STD_EXE:SPOEXT

COPY/DD  {P1}  *,3  {P2}
!
! purge files
!
-PURGE cosmics.bdf,middumm*.bdf
!
