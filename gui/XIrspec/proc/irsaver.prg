! @(#)irsaver.prg	19.1 (ES0-DMD) 02/25/03 13:47:33
! @(#)irsaver.prg	19.1 (ESO-ASD) 02/25/03 13:47:33
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!.COPYRIGHT   (C) 1993 European Southern Observatory
!.IDENT       irsaver.prg
!.AUTHOR      Cristian Levin   (ESO/La Silla)
!.KEYWORDS    XIrspec
!.PURPOSE     
!      Extract spectrum from a frame using row averages
!      Command:
!         @g irsaver in out  obj  sky  mtd
!
!.VERSION     1.0  Package Creation            17-MAR-1993  
!----------------------------------------------------------------------
!
CROSSREF in  out  obj  sky  mtd 
!
define/par p1 ? I    "Enter input frame        : "
define/par p2 ? I    "Enter output frame       : "
define/par p3 ? NUMB "Enter object coordinates : "
define/par p4 ? NUMB "Sky: "
define/par p5 ? CHAR "Extraction method:"
!
DEFINE/MAXPAR  5
!
def/loc cobj/i/1/2 'p3'
def/loc csky/i/1/4 'p4'
def/loc error/I/1/1  0

SET/FORMAT   I1

IF p5(1:1) .NE. "L" .AND. p5(1:1) .NE. "A" THEN
   WRITE/OUT "Error: Wrong parameter p5 = {p5}"
   WRITE/OUT "       Possible values are LINEAR and AVERAGE"
   RETURN/EXIT
ENDIF

def/loc np/I/1/1    {{P1},NPIX(2)}

if cobj(1) .lt. 1 .or.  cobj(1) .gt. np  error = 1
if cobj(2) .lt. 1 .or.  cobj(2) .gt. np  error = 1
if csky(1) .lt. 1 .or.  csky(1) .gt. np  error = 1
if csky(2) .lt. 1 .or.  csky(2) .gt. np  error = 1
if csky(3) .lt. 1 .or.  csky(3) .gt. np  error = 1
if csky(4) .lt. 1 .or.  csky(4) .gt. np  error = 1

if error .ne. 0 then
   WRITE/OUT "Error :  Invalid object or sky limits "
   WRITE/OUT "  Valid range is     : 1,{np}"
   RETURN/EXIT
endif

aver/row &a = 'p1' @'csky(1)',@'csky(2)' 
aver/row &b = 'p1' @'csky(3)',@'csky(4)' 
comp/ima &s = (&a + &b) / 2.0

aver/row &o   = 'p1' @'cobj(1)',@'cobj(2)' 

comp/ima 'p2' = &o - &s

IF p5(1:1) .EQ. "L"   comp/ima {P2} = {P2}*('cobj(2)'-'cobj(1)'+1)

COPY/DD  {P1}  *,3  {P2}

return
