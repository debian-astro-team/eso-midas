/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================

.VERSION
090422		last modif

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#include <stdlib.h>
#include <stdio.h>
#include <midas_def.h>
#include <tbldef.h>
#include <math.h> 
#include <string.h>
#include <globaldef.h>
#include <modeldef.h>

extern FILTER FL[1000];
extern int	Nrows;

int  flire();

void sget();
void siget();
void sfget();




void read_filtchar()
{
int  i=0,leak;
FILE *db;
char dbname[80];
char line[200];

char    *dirdata;

dirdata=(char *) getenv("MID_FILTERS");
if (dirdata==NULL)
   fprintf(stderr,"ERROR: cannot find MID_FILTERS environment variable\r\n");

sprintf(dbname,"%s/filters/filter.db",dirdata); 

db=fopen(dbname,"r");
if (db==NULL)
   fprintf(stderr,"ERROR: cannot open the filter database %s\r\n",dbname);
else
{
while(flire(db,line)!=-1)
{

sget(line,FL[i].filttype);
sget(line,FL[i].instrument);
sget(line,FL[i].design);
sget(line,FL[i].cosmetic);
sget(line,FL[i].imaqual);
sget(line,FL[i].cwl_lswp);
sget(line,FL[i].fwhm_wt50);
sfget(line,&FL[i].pwl);
sfget(line,&FL[i].taupk);
sget(line,FL[i].shape);
sfget(line,&FL[i].size);
sfget(line,&FL[i].opthick);
sget(line,FL[i].date);
sget(line,FL[i].codeachat);
sfget(line,&FL[i].thickness);
siget(line,&FL[i].redleak);
siget(line,&FL[i].nleakpeak);
sget(line,FL[i].leaks);
sget(line,FL[i].info);

if (FL[i].redleak!=0)
	leak=1;
else
	leak=0;

sprintf(FL[i].alldata,
"%12s%10s%4s%8s%8s%7.1f%5.1f%2s%4.1f%5.1f%2d",
        FL[i].filttype,
        FL[i].instrument,
        FL[i].imaqual,
        FL[i].cwl_lswp,
        FL[i].fwhm_wt50,
        FL[i].pwl,
        FL[i].taupk,
        FL[i].shape,
        FL[i].size,
        FL[i].opthick,
        leak
        );

sscanf(FL[i].filttype,"%d %s",&FL[i].eson,line);
i++;
};
Nrows=i;
};

}
