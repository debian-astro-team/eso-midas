/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <stdio.h>
#include <string.h>

void	endname(mot,end)
char	mot[],end[];
{
int     i,j,k=0,l;

l=strlen(mot)+1;
for(i=0;(i<l)&&(mot[i]!=46);i++);

for(j=i+1;j<l;j++)
        end[k++]=mot[j];
end[k]='\0';
}

void	base_name(mot,base)
char	mot[],base[];
{
int     i,j,l;

l=strlen(mot)+1;
for(i=0;(i<l)&&(mot[i]!=46);i++);

for(j=0;j<i;j++)
        base[j]=mot[j];

base[j]='\0';
}

int	fmot(fichier,mot)

FILE *fichier;
char mot[];
{
int i=0,c;
while((c=getc(fichier))=='\n' || c==' ' || c=='\t');
mot[i++]=c;
if (c==-1)
	return(c);

while((c=getc(fichier))!='\n' && c!=' ' && c!='\t')
        mot[i++]=c;
mot[i]='\0';

return 0;
}

int	flire(fichier,phrase)

FILE *fichier;
char phrase[];
{
int i=0,c;
while((c=getc(fichier))==' ' || c=='\t');
phrase[i++]=c;
if (c==-1)
	return(c);
else 	if (c!='\n')
	{
	while((phrase[i++]=getc(fichier))!='\n');
	phrase[i-1]='\0';
	};
return 0;
}

void	sget(line,mot)

char line[];
char mot[];
{
char newline[300];
int i=0,j,k=0,l=0,c;

while((c=line[k++]) == ' ' );

if ( c != '|' ) {
    mot[i++]=c;
    while((c=line[k++])!='|' && c!='\0')
        mot[i++]=c;
    mot[i]='\0';
}
for(j=k;j<strlen(line);j++)
    newline[l++]= line[j];

newline[l]='\0';
strcpy(line,newline);
}

void	sfget(line,real)

char line[];
float *real;
{
char newline[300];
char mot[20];
int i=0,j,k=0,l=0,c;

while((c=line[k++])=='|' || c==' ' );
mot[i++]=c;

while((c=line[k++])!='|' && c!='\0')
        mot[i++]=c;
mot[i]='\0';

for(j=k;j<strlen(line);j++)
	newline[l++]= line[j];

newline[l]='\0';
strcpy(line,newline);

sscanf(mot,"%f",real);
}

void	siget(line,entier)

char line[];
int *entier;
{
char newline[300];
char mot[20];
int i=0,j,k=0,l=0,c;

while((c=line[k++])=='|' || c==' ' );
mot[i++]=c;

while((c=line[k++])!='|' && c!='\0')
        mot[i++]=c;
mot[i]='\0';
for(j=k;j<=strlen(line);j++)
	newline[l++]= line[j];

newline[l]='\0';
strcpy(line,newline);

sscanf(mot,"%d",entier);
}

