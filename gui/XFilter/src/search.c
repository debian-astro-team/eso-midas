/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFICATION   gfinter.c
.MODULE           Subroutine
.LANGUAGE         C
.AUTHOR           Jose Mendez - Eso La Silla
.PURPOSE          The functions of this module are application
                  subroutines to handler filters in X11-Midas Package.
.KEYWORD          Contains Graphics interface and applications.
.ENVIRONMENT      Unix
.VERSION          1.0 10-Mar-91.

 090826		last modif
----------------------------------------------------------*/

#include <midas_def.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <UxLib.h>
#include <globaldef.h>
#include <model.h>


extern void UpdateMessageWindow();
void            DisplayList();
void            DisplayChoose();


/************************
   External variables
 ************************/
extern char currsel[100];


/* Filter definitions */
extern FILTER FL[1000];

extern int Nrows, Ncols;

int ltrimx;
int ltrimy;

/* Flags to select */

extern float goffx1, goffx2, goffy1, goffy2;

/*-----------------------------------------------------------------------*/
/***************************
     Local functions
***************************/
void print_select();

/*  Execute the command specified in client_data */

void print_select(list,name,n)
char	*list[];
char	name[];
int	n;
{
FILE	*out; 
register int	i;

out=fopen(name,"w");

if (out != (FILE *)0)
	for(i=0;i<n;i++)
	   fprintf(out,"%s\r\n",list[i]);

fclose(out);
}

/*-----------------------------------------------------------------------*/

 /* display the filter Neso characteristics */

int search_nfilt(nfilt)
int nfilt;

{
int i,indx=-1,nfrows;
char  *List[2];
char *osmmget();

nfrows=1;
ltrimx=ltrimy=FALSE; 


for(i=0;i<Nrows;i++)
	if(nfilt==FL[i].eson) { indx=i; break; }

List[0] = osmmget( 100 );
 
i=indx;

if(indx!=-1)
	{
        UpdateMessageWindow("Nfilt  Type Instrument  IQ   Cwl     Fwhm    Pwl  T%  Size   Opt Leak");

	sprintf(List[0],"%s",FL[indx].alldata);
	List[1] = NULL;
	DisplayList( List, 1);
	}
 else
	{
	sprintf(List[0],"Filter  %5d  Characteristic Does not Exist",nfilt);
	UpdateMessageWindow(List[0]);
	}

strcpy(currsel,"neso.dat");
print_select(List,currsel,nfrows);

osmmfree(List[0]);

return(indx);
}

/*-----------------------------------------------------------------------*/

void search_cwav(cw1,cw2)
float cw1,cw2;
{
int	i,nid,nfdb=0;
float	cwl;
int	listn[1000];
char	*List[FMAX];
char	*osmmget();

ltrimx=ltrimy=FALSE; 
for(i=0;i<Nrows;i++)
	{
	nid=sscanf(FL[i].cwl_lswp,"%f",&cwl);
	if (nid==1)
         {
	 if(!strcmp(UxGetSet(UxFindSwidget("tg_imaqual")),"true"))
	   {
	   if( cwl >= cw1 && cwl <= cw2 && !strncmp(FL[i].design,"I",1) ) 
		listn[nfdb++]=i;
           }
	 else
	  {
	  if( cwl >= cw1 && cwl <= cw2 && strncmp(FL[i].design,"I",1))
                listn[nfdb++]=i;
	  }
         }
        else
         {
         if(cw2>=1200. && !strcmp(UxGetSet(UxFindSwidget("tg_imaqual")),"true"))
		listn[nfdb++]=i;
	 }
	}

if(nfdb==0)   /* ok */
	{
	List[0] = osmmget( 100 );
	sprintf(List[0]," Filters not Found in CW range [%6.1f, %6.1f] .",
		cw1,cw2);
	UpdateMessageWindow(List[0]);
	osmmfree(List[0]);   
	}
else
	{
        UpdateMessageWindow("Nfilt  Type Instrument  IQ   Cwl     Fwhm    Pwl  T%  Size   Opt Leak");


	/* Memory allocate and init it */
	for( i=0; i<nfdb; i++) 
		{
		List[i] = osmmget( 100 );  
		sprintf( List[i],"%s",FL[listn[i]].alldata);
		}
	List[nfdb] = NULL;

	DisplayList( List, nfdb); 

	strcpy(currsel,"cwav.dat");
	print_select(List,currsel,nfdb);

	/* free memory allocated */ 
	for(i=0 ; i<nfdb ;i++)
		osmmfree(List[i]);  
	}
}

/*-----------------------------------------------------------------------*/


void search_all_filters()
{
int	i,nfdb=0;
int	listn[1000];
char	*List[FMAX];
char  *osmmget();


for(i=0;i<Nrows;i++)
    listn[nfdb++]=i;

if(nfdb==0)   /* ok */
	UpdateMessageWindow( "Filters not Found" );
else
	{
        UpdateMessageWindow("Nfilt  Type Instrument  IQ   Cwl     Fwhm    Pwl  T%  Size   Opt Leak");


	/* Memory allocate and init it */
	for( i=0; i<nfdb; i++) 
		{
		List[i] = osmmget( 100 );  
		sprintf( List[i],"%s",FL[listn[i]].alldata);
		}
	List[nfdb] = NULL;

	DisplayList( List, nfdb); 

	strcpy(currsel,"cwav.dat");
	print_select(List,currsel,nfdb);

	/* free memory allocated */ 
	for(i=0 ; i<nfdb ;i++)
		osmmfree(List[i]);  
	}
}

/*-----------------------------------------------------------------------*/


void search_cwbw(cw1,cw2,bw1,bw2)
float cw1,cw2,bw1,bw2;
{
float cwl, bwl;
int	listn[1000];
int i,nid,nfdb=0;
char *List[FMAX];
char  *osmmget();

ltrimx=ltrimy=FALSE; 

for(i=0;i<Nrows;i++)
	{
	nid=sscanf(FL[i].cwl_lswp,"%f",&cwl);
	sscanf(FL[i].fwhm_wt50,"%f",&bwl);
	if (nid==1 && strncmp(FL[i].fwhm_wt50, "lwp", 3) &&
	              strncmp(FL[i].fwhm_wt50, "swp", 3) )
	    {
	    if(!strcmp(UxGetSet(UxFindSwidget("tg_imaqual1")),"true"))
	       {
	       if( cwl >= cw1 && cwl <= cw2 && !strncmp(FL[i].design,"I",1) 
			&& bwl >= bw1 && bwl <= bw2 )
		listn[nfdb++]=i;
               }
	    else
               {
	       if( cwl >= cw1 && cwl <= cw2 && strncmp(FL[i].design,"I",1) 
			&& bwl >= bw1 && bwl <= bw2 )
		listn[nfdb++]=i;
               }
	    };
	};

if(nfdb==0)
	{
	List[0] = osmmget( 100 );
	sprintf(List[0],
      		"Filters not Found in CW:[%6.1f,%6.1f]&BW:[%6.2f,%6.2f] ",
		cw1,cw2,bw1,bw2);
	UpdateMessageWindow(List[0]);
	osmmfree(List[0]);
	}
else
	{
        UpdateMessageWindow("Nfilt  Type Instrument  IQ   Cwl     Fwhm    Pwl  T%  Size   Opt Leak");

	/* Memory allocate */
    	for( i=0; i<nfdb; i++)
		{
     		List[i] = osmmget( 100 );
      		sprintf( List[i],"%s",FL[listn[i]].alldata);
		}
	List[nfdb] = NULL;

	DisplayList( List, nfdb); 

	strcpy(currsel,"cwbw.dat");
	print_select(List,currsel,nfdb);

	/* free memory allocated */
	for(i=0 ; i<nfdb ;i++)
      		osmmfree(List[i]);
	}
}

/*-----------------------------------------------------------------------*/


void search_all()
{
int i, nfdb = 0;
char *List[FMAX];
char  *osmmget();
int listn[1000];


for(i=0;i<Nrows;i++)
	listn[nfdb++]=i;

if(nfdb==0)
   {
   List[0] = osmmget( 100 );

   sprintf(List[0]," Filters not Found  ");
   List[1]=NULL;

   DisplayChoose( List, 1);

   osmmfree(List[0]);
   }
else
   {
   for( i=0; i<nfdb; i++)
      {
      List[i] = osmmget( 100 );
      sprintf( List[i],"%s",FL[listn[i]].alldata);
      };

    List[nfdb] = NULL;


   DisplayChoose( List, nfdb); 

   /* free memory allocated */
   for(i=0 ; i<nfdb;i++)
      osmmfree(List[i]);
   }
 }

/*-----------------------------------------------------------------------*/

void search_basic( instrument )
char *instrument;
{
int	i, nfdb = 0;
char	*List[FMAX];
char	inst_data[20];
char  *osmmget();
int	listn[1000];

if ( !strcmp(instrument, "emmib") )
    strcpy(inst_data, "EMMIB");
else if ( !strcmp(instrument, "emmir") )
    strcpy(inst_data, "EMMIR");
else if ( !strcmp(instrument, "efosc1") )
    strcpy(inst_data, "EFOSC I");
else if ( !strcmp(instrument, "efosc2") )
    strcpy(inst_data, "EFOSC II");
else if ( !strcmp(instrument, "susi") )
    strcpy(inst_data, "SUSI");
else if ( !strcmp(instrument, "bc") )
    strcpy(inst_data, "B&C");

for(i = 0; i < Nrows; i++ )
   if ( (!strcmp(FL[i].instrument, inst_data)) || 
	(!strcmp(FL[i].instrument, "EMMI") && !strncmp(inst_data, "EMMI", 4)))
	listn[nfdb++]=i;

if(nfdb==0)
   {
    List[0] = osmmget( 100 );

    sprintf(List[0],
      " Filters not Found  ");
   DisplayChoose( List, 1);

    osmmfree(List[0]);
   }
else
   {
    /* Memory allocate */

    for( i=0; i<nfdb; i++)
      {
      List[i] = osmmget( 100 );
      sprintf( List[i],"%s",FL[listn[i]].alldata);
      };

    List[nfdb] = NULL;


     DisplayChoose( List, nfdb); 

    /* free memory allocated */
    for(i=0 ; i<nfdb;i++)
      osmmfree(List[i]);
   }
}

/*-----------------------------------------------------------------------*/


void search_f_inst( instname, basicset )
char	*instname;
int	basicset;
{
if ( basicset )
    search_basic(instname);
else
    search_all();
}
