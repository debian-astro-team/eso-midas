/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <stdio.h>
#include <string.h>
#include <globaldef.h>
#include <model.h>


extern void endname();





void	save_mod(name)
char	name[];
{
extern	struct s_trans	T;
extern	struct s_gui	G;

FILE	*out;
char	term[10];

endname(name,term);

if(strcmp(term,"trans"))
	strcat(name,".trans");

strcpy(T.transfile,name);

out=fopen(name,"w");


fprintf(out,"# Configuration file for Transmission model : %s\n",name);
fprintf(out,"instrument      %s\n\n",G.sel_inst);

if (T.ispectra !=0)
	fprintf(out,"spectrum        %d %s %s\n",
				T.ispectra,T.spectrapath,T.spectrafile);
else
	fprintf(out,"spectrum        %d no_path no_spectrum\n",T.ispectra);
fprintf(out,"sky             %d %d %d\n",T.isky,T.darkness,T.emlines);
fprintf(out,"atmosphere      %d %f\n",T.iatmos,T.airmass);
fprintf(out,"mirror          %d\n",T.imirror);
fprintf(out,"optics          %d\n",T.ioptics);
fprintf(out,"dispersor       %d %d\n",T.igrism,T.ngrism);
fprintf(out,"filter          %d %d\n",T.ifilter,T.nfilter);
fprintf(out,"ccd             %d %d\n",T.iccd,T.nccd);
fprintf(out,"gain            %f\n",T.einadu);
fprintf(out,"ron             %f\n",T.ron);
fprintf(out,"etime_snr       %d %f %f\n",T.itimeSN,T.etime,T.SN);
fprintf(out,"extractsky      %d\n",1);

fclose(out);
}
