/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	BlackBody.c

.VERSION
 090826         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <model.h>
#include <global.h>

extern void do_bbody();



extern struct s_bbody N;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxform7;
	swidget	Uxform8;
	swidget	UxApplyBB;
	swidget	UxCancelBB;
	swidget	Uxseparator9;
	swidget	Uxlbl_T;
	swidget	Uxlbl_Dist;
	swidget	Uxtf_T;
	swidget	Uxtf_Dist;
	swidget	Uxlbl_radius;
	swidget	Uxlbl_wr;
	swidget	Uxtf_radius;
	swidget	Uxtf_wrmin;
	swidget	Uxtf_wrmax;
} _UxCBlackBody;

#define form7                   UxBlackBodyContext->Uxform7
#define form8                   UxBlackBodyContext->Uxform8
#define ApplyBB                 UxBlackBodyContext->UxApplyBB
#define CancelBB                UxBlackBodyContext->UxCancelBB
#define separator9              UxBlackBodyContext->Uxseparator9
#define lbl_T                   UxBlackBodyContext->Uxlbl_T
#define lbl_Dist                UxBlackBodyContext->Uxlbl_Dist
#define tf_T                    UxBlackBodyContext->Uxtf_T
#define tf_Dist                 UxBlackBodyContext->Uxtf_Dist
#define lbl_radius              UxBlackBodyContext->Uxlbl_radius
#define lbl_wr                  UxBlackBodyContext->Uxlbl_wr
#define tf_radius               UxBlackBodyContext->Uxtf_radius
#define tf_wrmin                UxBlackBodyContext->Uxtf_wrmin
#define tf_wrmax                UxBlackBodyContext->Uxtf_wrmax

static _UxCBlackBody	*UxBlackBodyContext;

swidget	BlackBody;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*BBtext_tab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_BlackBody();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_ApplyBB( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBlackBody           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBlackBodyContext;
	UxBlackBodyContext = UxContext =
			(_UxCBlackBody *) UxGetContext( UxThisWidget );
	{
	do_bbody();
	}
	UxBlackBodyContext = UxSaveCtx;
}

static void	activateCB_CancelBB( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBlackBody           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBlackBodyContext;
	UxBlackBodyContext = UxContext =
			(_UxCBlackBody *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(BlackBody);
	}
	UxBlackBodyContext = UxSaveCtx;
}

static void	losingFocusCB_tf_T( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBlackBody           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBlackBodyContext;
	UxBlackBodyContext = UxContext =
			(_UxCBlackBody *) UxGetContext( UxThisWidget );
	{
	
	int nid;
	char am[10];
	float temp;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_T")),"%f",&temp);
	if (nid==1)
		{
		if ((temp>=1000.)&&(temp<=100000.))
			N.T=temp;
		else if (temp<1000.)
			N.T=1000.;
		else if (temp>10000.)
			N.T=10000.;
		}
	
	sprintf(am,"%.1f",N.T);
	UxPutText(UxFindSwidget("tf_T"),am);
	
	}
	UxBlackBodyContext = UxSaveCtx;
}

static void	losingFocusCB_tf_Dist( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBlackBody           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBlackBodyContext;
	UxBlackBodyContext = UxContext =
			(_UxCBlackBody *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float real;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_Dist")),"%f",&real);
	if (nid==1)
		{
		if ((real>=.01)&&(real<=10000.))
			N.dist=real;
		else if (real<.01)
			N.dist=.01;
		else if (real>10000.)
			N.dist=10000.;
		}
	
	sprintf(am,"%.2f",N.dist);
	UxPutText(UxFindSwidget("tf_Dist"),am);
	
	}
	UxBlackBodyContext = UxSaveCtx;
}

static void	losingFocusCB_tf_radius( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBlackBody           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBlackBodyContext;
	UxBlackBodyContext = UxContext =
			(_UxCBlackBody *) UxGetContext( UxThisWidget );
	{
	
	int nid;
	char am[10];
	float real;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_radius")),"%f",&real);
	if (nid==1)
		{
		if ((real>=.01)&&(real<=10000.))
			N.radius=real;
		else if (real<.01)
			N.radius=.01;
		else if (real>10000.)
			N.radius=10000.;
		}
	
	sprintf(am,"%.2f",N.radius);
	UxPutText(UxFindSwidget("tf_radius"),am);
	
	}
	UxBlackBodyContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wrmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBlackBody           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBlackBodyContext;
	UxBlackBodyContext = UxContext =
			(_UxCBlackBody *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float minw;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_wrmin")),"%f",&minw);
	if (nid==1)
		{
		if ((minw>=300.)&&(minw<=1200.))
			N.wrmin=minw;
		else if (minw<300.)
			N.wrmin=300.;
		else if (minw>1200.)
			N.wrmin=1200.;
		}
	
	sprintf(am,"%.1f",N.wrmin);
	UxPutText(UxFindSwidget("tf_wrmin"),am);
	
	
	}
	UxBlackBodyContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wrmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBlackBody           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBlackBodyContext;
	UxBlackBodyContext = UxContext =
			(_UxCBlackBody *) UxGetContext( UxThisWidget );
	{
	
	int nid;
	char am[20];
	float maxw;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_wrmax")),"%f",&maxw);
	if (nid==1)
		{
		if ((maxw>=300.)&&(maxw<=1200.))
			N.wrmax=maxw;
		else if (maxw<300.)
			N.wrmax=300.;
		else if (maxw>1200.)
			N.wrmax=1200.;
		}
	
	sprintf(am,"%.1f",N.wrmax);
	UxPutText(UxFindSwidget("tf_wrmax"),am);
	
	
	}
	UxBlackBodyContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_BlackBody()
{
	UxPutBackground( BlackBody, WindowBackground );
	UxPutBorderColor( BlackBody, WindowBackground );
	UxPutKeyboardFocusPolicy( BlackBody, "pointer" );
	UxPutIconName( BlackBody, "Black Body" );
	UxPutHeight( BlackBody, 230 );
	UxPutWidth( BlackBody, 380 );
	UxPutY( BlackBody, 165 );
	UxPutX( BlackBody, 430 );

	UxPutBorderColor( form7, WindowBackground );
	UxPutBackground( form7, WindowBackground );
	UxPutHeight( form7, 548 );
	UxPutWidth( form7, 490 );
	UxPutY( form7, 4 );
	UxPutX( form7, 4 );
	UxPutUnitType( form7, "pixels" );
	UxPutResizePolicy( form7, "resize_none" );

	UxPutBorderColor( form8, ButtonBackground );
	UxPutBackground( form8, ButtonBackground );
	UxPutHeight( form8, 44 );
	UxPutWidth( form8, 490 );
	UxPutY( form8, 150 );
	UxPutX( form8, 0 );
	UxPutResizePolicy( form8, "resize_none" );

	UxPutLabelString( ApplyBB, "Apply" );
	UxPutForeground( ApplyBB, ApplyForeground );
	UxPutFontList( ApplyBB, BoldTextFont );
	UxPutBorderColor( ApplyBB, ButtonBackground );
	UxPutBackground( ApplyBB, ButtonBackground );
	UxPutHeight( ApplyBB, 30 );
	UxPutWidth( ApplyBB, 80 );
	UxPutY( ApplyBB, 6 );
	UxPutX( ApplyBB, 10 );

	UxPutLabelString( CancelBB, "Cancel" );
	UxPutForeground( CancelBB, CancelForeground );
	UxPutFontList( CancelBB, BoldTextFont );
	UxPutBorderColor( CancelBB, ButtonBackground );
	UxPutBackground( CancelBB, ButtonBackground );
	UxPutHeight( CancelBB, 30 );
	UxPutWidth( CancelBB, 86 );
	UxPutY( CancelBB, 6 );
	UxPutX( CancelBB, 110 );

	UxPutBorderColor( separator9, WindowBackground );
	UxPutBackground( separator9, WindowBackground );
	UxPutHeight( separator9, 10 );
	UxPutWidth( separator9, 492 );
	UxPutY( separator9, 140 );
	UxPutX( separator9, 0 );

	UxPutForeground( lbl_T, TextForeground );
	UxPutAlignment( lbl_T, "alignment_beginning" );
	UxPutLabelString( lbl_T, "Temperature (K) :" );
	UxPutFontList( lbl_T, TextFont );
	UxPutBorderColor( lbl_T, LabelBackground );
	UxPutBackground( lbl_T, LabelBackground );
	UxPutHeight( lbl_T, 30 );
	UxPutWidth( lbl_T, 126 );
	UxPutY( lbl_T, 10 );
	UxPutX( lbl_T, 10 );

	UxPutForeground( lbl_Dist, TextForeground );
	UxPutAlignment( lbl_Dist, "alignment_beginning" );
	UxPutLabelString( lbl_Dist, "Distance (pc) :" );
	UxPutFontList( lbl_Dist, TextFont );
	UxPutBorderColor( lbl_Dist, LabelBackground );
	UxPutBackground( lbl_Dist, LabelBackground );
	UxPutHeight( lbl_Dist, 30 );
	UxPutWidth( lbl_Dist, 104 );
	UxPutY( lbl_Dist, 50 );
	UxPutX( lbl_Dist, 10 );

	UxPutTranslations( tf_T, BBtext_tab );
	UxPutSelectionArrayCount( tf_T, 3 );
	UxPutMaxLength( tf_T, 200 );
	UxPutForeground( tf_T, TextForeground );
	UxPutFontList( tf_T, TextFont );
	UxPutBorderColor( tf_T, TextBackground );
	UxPutBackground( tf_T, TextBackground );
	UxPutHeight( tf_T, 35 );
	UxPutWidth( tf_T, 80 );
	UxPutY( tf_T, 6 );
	UxPutX( tf_T, 190 );

	UxPutTranslations( tf_Dist, BBtext_tab );
	UxPutSelectionArrayCount( tf_Dist, 3 );
	UxPutMaxLength( tf_Dist, 200 );
	UxPutForeground( tf_Dist, TextForeground );
	UxPutFontList( tf_Dist, TextFont );
	UxPutBorderColor( tf_Dist, TextBackground );
	UxPutBackground( tf_Dist, TextBackground );
	UxPutHeight( tf_Dist, 35 );
	UxPutWidth( tf_Dist, 80 );
	UxPutY( tf_Dist, 48 );
	UxPutX( tf_Dist, 190 );

	UxPutForeground( lbl_radius, TextForeground );
	UxPutAlignment( lbl_radius, "alignment_beginning" );
	UxPutLabelString( lbl_radius, "Radius (solar unit) :" );
	UxPutFontList( lbl_radius, TextFont );
	UxPutBorderColor( lbl_radius, LabelBackground );
	UxPutBackground( lbl_radius, LabelBackground );
	UxPutHeight( lbl_radius, 30 );
	UxPutWidth( lbl_radius, 132 );
	UxPutY( lbl_radius, 90 );
	UxPutX( lbl_radius, 10 );

	UxPutForeground( lbl_wr, TextForeground );
	UxPutAlignment( lbl_wr, "alignment_beginning" );
	UxPutLabelString( lbl_wr, "Wavelength Range (nm):" );
	UxPutFontList( lbl_wr, TextFont );
	UxPutBorderColor( lbl_wr, LabelBackground );
	UxPutBackground( lbl_wr, LabelBackground );
	UxPutHeight( lbl_wr, 30 );
	UxPutWidth( lbl_wr, 170 );
	UxPutY( lbl_wr, 130 );
	UxPutX( lbl_wr, 10 );

	UxPutTranslations( tf_radius, BBtext_tab );
	UxPutSelectionArrayCount( tf_radius, 3 );
	UxPutMaxLength( tf_radius, 200 );
	UxPutForeground( tf_radius, TextForeground );
	UxPutFontList( tf_radius, TextFont );
	UxPutBorderColor( tf_radius, TextBackground );
	UxPutBackground( tf_radius, TextBackground );
	UxPutHeight( tf_radius, 35 );
	UxPutWidth( tf_radius, 80 );
	UxPutY( tf_radius, 88 );
	UxPutX( tf_radius, 190 );

	UxPutTranslations( tf_wrmin, BBtext_tab );
	UxPutSelectionArrayCount( tf_wrmin, 3 );
	UxPutMaxLength( tf_wrmin, 200 );
	UxPutForeground( tf_wrmin, TextForeground );
	UxPutFontList( tf_wrmin, TextFont );
	UxPutBorderColor( tf_wrmin, TextBackground );
	UxPutBackground( tf_wrmin, TextBackground );
	UxPutHeight( tf_wrmin, 35 );
	UxPutWidth( tf_wrmin, 80 );
	UxPutY( tf_wrmin, 128 );
	UxPutX( tf_wrmin, 190 );

	UxPutTranslations( tf_wrmax, BBtext_tab );
	UxPutSelectionArrayCount( tf_wrmax, 3 );
	UxPutMaxLength( tf_wrmax, 200 );
	UxPutForeground( tf_wrmax, TextForeground );
	UxPutFontList( tf_wrmax, TextFont );
	UxPutBorderColor( tf_wrmax, TextBackground );
	UxPutBackground( tf_wrmax, TextBackground );
	UxPutHeight( tf_wrmax, 35 );
	UxPutWidth( tf_wrmax, 80 );
	UxPutY( tf_wrmax, 128 );
	UxPutX( tf_wrmax, 278 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_BlackBody()
{
	/* Create the swidgets */

	BlackBody = UxCreateApplicationShell( "BlackBody", NO_PARENT );
	UxPutContext( BlackBody, UxBlackBodyContext );

	form7 = UxCreateForm( "form7", BlackBody );
	form8 = UxCreateForm( "form8", form7 );
	ApplyBB = UxCreatePushButton( "ApplyBB", form8 );
	CancelBB = UxCreatePushButton( "CancelBB", form8 );
	separator9 = UxCreateSeparator( "separator9", form7 );
	lbl_T = UxCreateLabel( "lbl_T", form7 );
	lbl_Dist = UxCreateLabel( "lbl_Dist", form7 );
	tf_T = UxCreateText( "tf_T", form7 );
	tf_Dist = UxCreateText( "tf_Dist", form7 );
	lbl_radius = UxCreateLabel( "lbl_radius", form7 );
	lbl_wr = UxCreateLabel( "lbl_wr", form7 );
	tf_radius = UxCreateText( "tf_radius", form7 );
	tf_wrmin = UxCreateText( "tf_wrmin", form7 );
	tf_wrmax = UxCreateText( "tf_wrmax", form7 );

	_Uxinit_BlackBody();

	/* Create the X widgets */

	UxCreateWidget( BlackBody );
	UxCreateWidget( form7 );
	UxPutRightAttachment( form8, "attach_form" );
	UxPutLeftAttachment( form8, "attach_form" );
	UxPutBottomOffset( form8, 2 );
	UxPutBottomAttachment( form8, "attach_form" );
	UxCreateWidget( form8 );

	UxCreateWidget( ApplyBB );
	UxCreateWidget( CancelBB );
	UxPutRightAttachment( separator9, "attach_form" );
	UxPutLeftAttachment( separator9, "attach_form" );
	UxPutBottomWidget( separator9, "form8" );
	UxPutBottomOffset( separator9, 0 );
	UxPutBottomAttachment( separator9, "attach_widget" );
	UxCreateWidget( separator9 );

	UxCreateWidget( lbl_T );
	UxCreateWidget( lbl_Dist );
	UxCreateWidget( tf_T );
	UxCreateWidget( tf_Dist );
	UxCreateWidget( lbl_radius );
	UxCreateWidget( lbl_wr );
	UxCreateWidget( tf_radius );
	UxCreateWidget( tf_wrmin );
	UxCreateWidget( tf_wrmax );

	UxAddCallback( ApplyBB, XmNactivateCallback,
			activateCB_ApplyBB,
			(XtPointer) UxBlackBodyContext );

	UxAddCallback( CancelBB, XmNactivateCallback,
			activateCB_CancelBB,
			(XtPointer) UxBlackBodyContext );

	UxAddCallback( tf_T, XmNlosingFocusCallback,
			losingFocusCB_tf_T,
			(XtPointer) UxBlackBodyContext );

	UxAddCallback( tf_Dist, XmNlosingFocusCallback,
			losingFocusCB_tf_Dist,
			(XtPointer) UxBlackBodyContext );

	UxAddCallback( tf_radius, XmNlosingFocusCallback,
			losingFocusCB_tf_radius,
			(XtPointer) UxBlackBodyContext );

	UxAddCallback( tf_wrmin, XmNlosingFocusCallback,
			losingFocusCB_tf_wrmin,
			(XtPointer) UxBlackBodyContext );

	UxAddCallback( tf_wrmax, XmNlosingFocusCallback,
			losingFocusCB_tf_wrmax,
			(XtPointer) UxBlackBodyContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( BlackBody );

	return ( BlackBody );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_BlackBody()
{
	swidget                 rtrn;
	_UxCBlackBody           *UxContext;

	UxBlackBodyContext = UxContext =
		(_UxCBlackBody *) UxMalloc( sizeof(_UxCBlackBody) );

	rtrn = _Uxbuild_BlackBody();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_BlackBody()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_BlackBody();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

