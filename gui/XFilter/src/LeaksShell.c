/* @(#)LeaksShell.c	19.1 (ES0-DMD) 02/25/03 13:46:31 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	LeaksShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxRowCol.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxLeaksShell;
	swidget	Uxform22;
	swidget	UxscrolledWindow11;
	swidget	UxrowColumn9;
	swidget	Uxtx_red_leaks;
	swidget	Uxpb_leaks_return;
} _UxCLeaksShell;

#define LeaksShell              UxLeaksShellContext->UxLeaksShell
#define form22                  UxLeaksShellContext->Uxform22
#define scrolledWindow11        UxLeaksShellContext->UxscrolledWindow11
#define rowColumn9              UxLeaksShellContext->UxrowColumn9
#define tx_red_leaks            UxLeaksShellContext->Uxtx_red_leaks
#define pb_leaks_return         UxLeaksShellContext->Uxpb_leaks_return

static _UxCLeaksShell	*UxLeaksShellContext;


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_LeaksShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_leaks_return( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCLeaksShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLeaksShellContext;
	UxLeaksShellContext = UxContext =
			(_UxCLeaksShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("LeaksShell"));
	 
	
	}
	UxLeaksShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_LeaksShell()
{
	UxPutGeometry( LeaksShell, "-0+0" );
	UxPutBackground( LeaksShell, ButtonBackground );
	UxPutIconName( LeaksShell, "Red Leaks" );
	UxPutKeyboardFocusPolicy( LeaksShell, "pointer" );
	UxPutTitle( LeaksShell, "Red Leaks" );
	UxPutHeight( LeaksShell, 418 );
	UxPutWidth( LeaksShell, 356 );
	UxPutY( LeaksShell, 438 );
	UxPutX( LeaksShell, 716 );

	UxPutBackground( form22, ButtonBackground );
	UxPutHeight( form22, 414 );
	UxPutWidth( form22, 356 );
	UxPutY( form22, 0 );
	UxPutX( form22, 0 );
	UxPutUnitType( form22, "pixels" );
	UxPutResizePolicy( form22, "resize_none" );

	UxPutScrollBarDisplayPolicy( scrolledWindow11, "static" );
	UxPutBackground( scrolledWindow11, WindowBackground );
	UxPutScrollBarPlacement( scrolledWindow11, "bottom_left" );
	UxPutHeight( scrolledWindow11, 374 );
	UxPutWidth( scrolledWindow11, 358 );
	UxPutY( scrolledWindow11, 2 );
	UxPutX( scrolledWindow11, 0 );
	UxPutScrollingPolicy( scrolledWindow11, "automatic" );

	UxPutEntryAlignment( rowColumn9, "alignment_beginning" );
	UxPutBackground( rowColumn9, "white" );
	UxPutOrientation( rowColumn9, "horizontal" );
	UxPutHeight( rowColumn9, 427 );
	UxPutWidth( rowColumn9, 418 );
	UxPutY( rowColumn9, 4 );
	UxPutX( rowColumn9, 25 );

	UxPutHighlightColor( tx_red_leaks, "Black" );
	UxPutForeground( tx_red_leaks, TextForeground );
	UxPutCursorPositionVisible( tx_red_leaks, "false" );
	UxPutEditMode( tx_red_leaks, "multi_line_edit" );
	UxPutFontList( tx_red_leaks, SmallFont );
	UxPutBackground( tx_red_leaks, TextBackground );
	UxPutHeight( tx_red_leaks, 389 );
	UxPutWidth( tx_red_leaks, 322 );
	UxPutY( tx_red_leaks, 3 );
	UxPutX( tx_red_leaks, 3 );

	UxPutForeground( pb_leaks_return, CancelForeground );
	UxPutHighlightOnEnter( pb_leaks_return, "true" );
	UxPutHighlightColor( pb_leaks_return, "red" );
	UxPutLabelString( pb_leaks_return, "Return" );
	UxPutFontList( pb_leaks_return, BoldTextFont );
	UxPutBackground( pb_leaks_return, ButtonBackground );
	UxPutHeight( pb_leaks_return, 32 );
	UxPutWidth( pb_leaks_return, 82 );
	UxPutY( pb_leaks_return, 380 );
	UxPutX( pb_leaks_return, 262 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_LeaksShell()
{
	/* Create the swidgets */

	LeaksShell = UxCreateTopLevelShell( "LeaksShell", NO_PARENT );
	UxPutContext( LeaksShell, UxLeaksShellContext );

	form22 = UxCreateForm( "form22", LeaksShell );
	scrolledWindow11 = UxCreateScrolledWindow( "scrolledWindow11", form22 );
	rowColumn9 = UxCreateRowColumn( "rowColumn9", scrolledWindow11 );
	tx_red_leaks = UxCreateText( "tx_red_leaks", rowColumn9 );
	pb_leaks_return = UxCreatePushButton( "pb_leaks_return", form22 );

	_Uxinit_LeaksShell();

	/* Create the X widgets */

	UxCreateWidget( LeaksShell );
	UxCreateWidget( form22 );
	UxCreateWidget( scrolledWindow11 );
	UxCreateWidget( rowColumn9 );
	UxCreateWidget( tx_red_leaks );
	UxCreateWidget( pb_leaks_return );

	UxAddCallback( pb_leaks_return, XmNactivateCallback,
			activateCB_pb_leaks_return,
			(XtPointer) UxLeaksShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( LeaksShell );

	return ( LeaksShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_LeaksShell()
{
	swidget                 rtrn;
	_UxCLeaksShell          *UxContext;

	UxLeaksShellContext = UxContext =
		(_UxCLeaksShell *) UxMalloc( sizeof(_UxCLeaksShell) );

	rtrn = _Uxbuild_LeaksShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_LeaksShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_LeaksShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

