/* @(#)set_range.c	19.1 (ES0-DMD) 02/25/03 13:46:37 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


#include <math.h>
#include <stdio.h>
#include <UxLib.h>
#include <globaldef.h>
#include <model.h>

void	set_range()
{
extern	struct s_trans	T;
extern	float	XWmin,XWmax;
char	word[10];

if (XWmin==-1.)
	XWmin=T.atmosmin;
if (XWmax==-1.)
	XWmax=T.atmosmax;

if (T.iatmos !=0)
	{
	XWmin=Max(XWmin,T.atmosmin);
	XWmax=Min(XWmax,T.atmosmax);
	};

if (T.igrism !=0)
	{
	XWmin=Max(XWmin,T.grismmin);
	XWmax=Min(XWmax,T.grismmax);
	}

if (T.imirror !=0)
	{
	XWmin=Max(XWmin,T.mirmin);
	XWmax=Min(XWmax,T.mirmax);
	};

if (T.ioptics !=0)
	{
	XWmin=Max(XWmin,T.optmin);
	XWmax=Min(XWmax,T.optmax);
	};

if (T.ifilter !=0)
	{
	XWmin=Max(XWmin,T.filtmin);
	XWmax=Min(XWmax,T.filtmax);
	}

if (T.iccd !=0)
	{
        XWmin=Max(XWmin,T.ccdmin);
        XWmax=Min(XWmax,T.ccdmax);
	};

if (T.ispectra !=0)
        {
        XWmin=Max(XWmin,T.specmin);
        XWmax=Min(XWmax,T.specmax);
        };

if (T.isky !=0)
	{
        XWmin=Max(XWmin,T.skymin);
        XWmax=Min(XWmax,T.skymax);
	};


sprintf(word,"%.1f",XWmin);
UxPutText(UxFindSwidget("tf_TWmin"),word);
sprintf(word,"%.1f",XWmax);
UxPutText(UxFindSwidget("tf_TWmax"),word);
}

