/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826		last modif

===========================================================================*/


#include <stdio.h>
#include <midas_def.h>
#include <globaldef.h>



extern void init_model();
extern int read_filtchar();




void init_filter()
{
extern	int	pltgr;
extern	float	cw1,cw2,bw1,bw2;

cw1=300.;
cw2=800.;
bw1=10.;
bw2=20.;

SCSPRO("FILTER");

pltgr=TRUE;
read_filtchar();


/* set of model variables */

init_model();
/*set_model();*/

}

void end_filter()
{
 SCSEPI();
}

