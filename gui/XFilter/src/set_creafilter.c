/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <stdio.h>
#include <UxLib.h>
#include <model.h>

void	set_creafilter()

{
extern struct s_filter	F;
void	set_filter();

F.cw=600.;
F.cwl=850.;
F.bw=10.;
F.bwl=5.;
F.peak=80.;
F.peakl=10.;

set_filter();

}

void	set_filter()

{
extern struct s_filter	F;
char	word[10];

sprintf(word,"%.1f",F.cw);
UxPutText( UxFindSwidget("tf_cw"), word );
sprintf(word,"%.1f",F.cwl);
UxPutText( UxFindSwidget("tf_cwl"), word );
sprintf(word,"%.1f",F.bw);
UxPutText( UxFindSwidget("tf_bw"), word );
sprintf(word,"%.1f",F.bwl);
UxPutText( UxFindSwidget("tf_bwl"), word );
sprintf(word,"%.1f",F.peak);
UxPutText( UxFindSwidget("tf_pk"), word );
sprintf(word,"%.1f",F.peakl);
UxPutText( UxFindSwidget("tf_pkl"), word );
}
