/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	FilterCreate.c

.VERSION
 090826         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxCascB.h"
#include "UxRowCol.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <model.h>
#include <global.h>

extern  struct s_filter F;
extern  struct s_gui G;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxFilterForm;
	swidget	Uxlbl_cw;
	swidget	Uxlbl_bw;
	swidget	Uxlbl_peak;
	swidget	Uxform18;
	swidget	UxpB_FCapply;
	swidget	UxpB_FCcancel;
	swidget	UxhelptextFC;
	swidget	Uxseparator19;
	swidget	Uxtf_pk;
	swidget	Uxlbl_cwl;
	swidget	Uxlbl_bwl;
	swidget	Uxlbl_pkl;
	swidget	Uxtf_cwl;
	swidget	UxmeFC;
	swidget	Uxm_fc_file;
	swidget	Uxme_fc_open;
	swidget	Uxme_fc_save;
	swidget	Uxme_fc_saveas;
	swidget	UxmeFC_file;
	swidget	Uxm_fc_help;
	swidget	Uxme_fc_hm;
	swidget	UxmeFC_help;
	swidget	Uxm_fc_quit;
	swidget	Uxme_fc_bye;
	swidget	UxmeFC_quit;
	swidget	Uxseparator22;
	swidget	Uxtf_cw;
	swidget	Uxtf_bw;
	swidget	Uxseparator20;
	swidget	Uxtxt_pk;
	swidget	Uxtf_bwl;
	swidget	Uxtf_pkl;
} _UxCFilterCreate;

#define FilterForm              UxFilterCreateContext->UxFilterForm
#define lbl_cw                  UxFilterCreateContext->Uxlbl_cw
#define lbl_bw                  UxFilterCreateContext->Uxlbl_bw
#define lbl_peak                UxFilterCreateContext->Uxlbl_peak
#define form18                  UxFilterCreateContext->Uxform18
#define pB_FCapply              UxFilterCreateContext->UxpB_FCapply
#define pB_FCcancel             UxFilterCreateContext->UxpB_FCcancel
#define helptextFC              UxFilterCreateContext->UxhelptextFC
#define separator19             UxFilterCreateContext->Uxseparator19
#define tf_pk                   UxFilterCreateContext->Uxtf_pk
#define lbl_cwl                 UxFilterCreateContext->Uxlbl_cwl
#define lbl_bwl                 UxFilterCreateContext->Uxlbl_bwl
#define lbl_pkl                 UxFilterCreateContext->Uxlbl_pkl
#define tf_cwl                  UxFilterCreateContext->Uxtf_cwl
#define meFC                    UxFilterCreateContext->UxmeFC
#define m_fc_file               UxFilterCreateContext->Uxm_fc_file
#define me_fc_open              UxFilterCreateContext->Uxme_fc_open
#define me_fc_save              UxFilterCreateContext->Uxme_fc_save
#define me_fc_saveas            UxFilterCreateContext->Uxme_fc_saveas
#define meFC_file               UxFilterCreateContext->UxmeFC_file
#define m_fc_help               UxFilterCreateContext->Uxm_fc_help
#define me_fc_hm                UxFilterCreateContext->Uxme_fc_hm
#define meFC_help               UxFilterCreateContext->UxmeFC_help
#define m_fc_quit               UxFilterCreateContext->Uxm_fc_quit
#define me_fc_bye               UxFilterCreateContext->Uxme_fc_bye
#define meFC_quit               UxFilterCreateContext->UxmeFC_quit
#define separator22             UxFilterCreateContext->Uxseparator22
#define tf_cw                   UxFilterCreateContext->Uxtf_cw
#define tf_bw                   UxFilterCreateContext->Uxtf_bw
#define separator20             UxFilterCreateContext->Uxseparator20
#define txt_pk                  UxFilterCreateContext->Uxtxt_pk
#define tf_bwl                  UxFilterCreateContext->Uxtf_bwl
#define tf_pkl                  UxFilterCreateContext->Uxtf_pkl

static _UxCFilterCreate	*UxFilterCreateContext;


extern void SetFileList(), DisplayExtendedHelp();
extern void do_creafilter(), save_filt();


swidget	FilterCreate;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*text_tab1 = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_FilterCreate();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_HelpHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxFilterCreateContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pB_FCapply( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	do_creafilter();
	
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	activateCB_pB_FCcancel( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	
	UxPopdownInterface(UxFindSwidget("FilterCreate"));
	
	
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	focusCB_tf_pk( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextFC"),"Peak transmission of the filter in %\n");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	losingFocusCB_tf_pk( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float	real;
	
	nid=sscanf(UxGetText(tf_pk),"%f",&real);
	 
	if (nid==1)
		{
		if ((real>=1.)&&(real<=100.))
			F.peak=real;
		else if (real<1.)
			F.peak=1.;
		else if (real>100.)
			F.peak=100.;
		}
	else
		F.cw=80.;
	
	sprintf(am,"%1.1f",F.peak);
	UxPutText(UxFindSwidget("tf_pk"),am);
	
	
	UxPutText(UxFindSwidget("helptextFC"),"");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	focusCB_tf_cwl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextFC"),"Central Wavelength of the red leak (in nm)");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	losingFocusCB_tf_cwl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float	real;
	
	nid=sscanf(UxGetText(tf_cwl),"%f",&real);
	
	if (nid==1)
		{
		if ((real>=300.)&&(real<=1200.))
			F.cwl=real;
		else if (real<300.)
			F.cwl=300.;
		else if (real>1200.)
			F.cwl=1200.;
		}
	else
		F.cwl=850.;
	
	sprintf(am,"%1.1f",F.cwl);
	UxPutText(tf_cwl,am);
	
	
	UxPutText(UxFindSwidget("helptextFC"),"");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	activateCB_me_fc_open( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	extern swidget scrolledchoose;
	extern struct s_gui    G;
	
	strcpy(G.sel_type,"filter_load");
	
	SetFileList( UxGetWidget(scrolledchoose),1, "*.flt" );
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	activateCB_me_fc_save( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	save_filt(F.filterfile);
	UxFilterCreateContext = UxSaveCtx;
}

static void	activateCB_me_fc_saveas( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	strcpy(G.sel_type,"filter_save");
	UxPutTitle( UxFindSwidget("AskShell"),"Filter Save Name");
	UxPutLabelString( UxFindSwidget("labelAsk"), "Filename (no ext.):" );
	UxPutText( UxFindSwidget("textAsk"),"filt");
	
	UxPopupInterface(UxFindSwidget("AskShell"),no_grab);
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	activateCB_me_fc_hm( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	activateCB_me_fc_bye( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	UxPopdownInterface(UxFindSwidget("FilterCreate"));
	UxFilterCreateContext = UxSaveCtx;
}

static void	focusCB_tf_cw( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextFC"),"Central Wavelength of the main peak (in nm)\n");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	losingFocusCB_tf_cw( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float	real;
	
	nid=sscanf(UxGetText(tf_cw),"%f",&real);
	
	if (nid==1)
		{
		if ((real>=300.)&&(real<=1200.))
			F.cw=real;
		else if (real<300.)
			F.cw=300.;
		else if (real>1200.)
			F.cw=1200.;
		}
	else
		F.cw=600.;
	
	sprintf(am,"%1.1f",F.cw);
	UxPutText(UxFindSwidget("tf_cw"),am);
	
	
	UxPutText(UxFindSwidget("helptextFC"),"");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	focusCB_tf_bw( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextFC"),"Bandwidth of the main peak (in nm)\n");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	losingFocusCB_tf_bw( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float	real;
	
	nid=sscanf(UxGetText(tf_bw),"%f",&real);
	
	if (nid==1)
		{
		if ((real>=1.)&&(real<=500.))
			F.bw=real;
		else if (real<1.)
			F.bw=1.;
		else if (real>500.)
			F.bw=500.;
		}
	else
		F.bw=10.;
	
	sprintf(am,"%1.1f",F.bw);
	UxPutText(UxFindSwidget("tf_bw"),am);
	
	
	UxPutText(UxFindSwidget("helptextFC"),"");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	focusCB_tf_bwl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextFC"),"Bandwidth of the red leak (in nm)");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	losingFocusCB_tf_bwl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	
	int nid;
	char am[10];
	float	real;
	
	nid=sscanf(UxGetText(tf_bwl),"%f",&real);
	
	if (nid==1)
		{
		if ((real>=1.)&&(real<=500.))
			F.bwl=real;
		else if (real<1.)
			F.bwl=1.;
		else if (real>500.)
			F.bwl=500.;
		}
	else
		F.bwl=F.bw;
	
	sprintf(am,"%1.1f",F.bwl);
	UxPutText(UxFindSwidget("tf_bwl"),am);
	
	
	UxPutText(UxFindSwidget("helptextFC"),"");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	focusCB_tf_pkl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextFC"),"Peak transmission of the red leak in percentage\nof the peak transmission of the filter");
	}
	UxFilterCreateContext = UxSaveCtx;
}

static void	losingFocusCB_tf_pkl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFilterCreate        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFilterCreateContext;
	UxFilterCreateContext = UxContext =
			(_UxCFilterCreate *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float	real;
	
	nid=sscanf(UxGetText(tf_pkl),"%f",&real);
	
	if (nid==1)
		{
		if ((real>=.1)&&(real<=100.))
			F.peakl=real;
		else if (real<.1)
			F.peakl=.1;
		else if (real>100.)
			F.peakl=100.;
		}
	else
		F.peakl=10.;
	
	sprintf(am,"%1.1f",F.peakl);
	UxPutText(UxFindSwidget("tf_pkl"),am);
	
	
	UxPutText(UxFindSwidget("helptextFC"),"");
	}
	UxFilterCreateContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_FilterCreate()
{
	UxPutTitle( FilterCreate, "Create_Filter" );
	UxPutKeyboardFocusPolicy( FilterCreate, "pointer" );
	UxPutBorderColor( FilterCreate, WindowBackground );
	UxPutWinGravity( FilterCreate, "forget_gravity" );
	UxPutGeometry( FilterCreate, "+650+60" );
	UxPutDefaultFontList( FilterCreate, TextFont );
	UxPutIconName( FilterCreate, "Create_Filter" );
	UxPutHeight( FilterCreate, 405 );
	UxPutWidth( FilterCreate, 300 );
	UxPutY( FilterCreate, 81 );
	UxPutX( FilterCreate, 648 );

	UxPutBorderColor( FilterForm, WindowBackground );
	UxPutBackground( FilterForm, WindowBackground );
	UxPutHeight( FilterForm, 540 );
	UxPutWidth( FilterForm, 490 );
	UxPutY( FilterForm, 0 );
	UxPutX( FilterForm, 5 );
	UxPutUnitType( FilterForm, "pixels" );
	UxPutResizePolicy( FilterForm, "resize_none" );

	UxPutForeground( lbl_cw, TextForeground );
	UxPutAlignment( lbl_cw, "alignment_beginning" );
	UxPutLabelString( lbl_cw, "Central Wavelength (nm):" );
	UxPutFontList( lbl_cw, TextFont );
	UxPutBorderColor( lbl_cw, LabelBackground );
	UxPutBackground( lbl_cw, LabelBackground );
	UxPutHeight( lbl_cw, 25 );
	UxPutWidth( lbl_cw, 180 );
	UxPutY( lbl_cw, 50 );
	UxPutX( lbl_cw, 15 );

	UxPutForeground( lbl_bw, TextForeground );
	UxPutAlignment( lbl_bw, "alignment_beginning" );
	UxPutLabelString( lbl_bw, "Bandwidth  (nm):" );
	UxPutFontList( lbl_bw, TextFont );
	UxPutBorderColor( lbl_bw, LabelBackground );
	UxPutBackground( lbl_bw, LabelBackground );
	UxPutHeight( lbl_bw, 30 );
	UxPutWidth( lbl_bw, 170 );
	UxPutY( lbl_bw, 85 );
	UxPutX( lbl_bw, 15 );

	UxPutForeground( lbl_peak, TextForeground );
	UxPutAlignment( lbl_peak, "alignment_beginning" );
	UxPutLabelString( lbl_peak, "Peak (%) :" );
	UxPutFontList( lbl_peak, TextFont );
	UxPutBorderColor( lbl_peak, LabelBackground );
	UxPutBackground( lbl_peak, LabelBackground );
	UxPutHeight( lbl_peak, 30 );
	UxPutWidth( lbl_peak, 80 );
	UxPutY( lbl_peak, 125 );
	UxPutX( lbl_peak, 15 );

	UxPutButtonFontList( form18, BoldTextFont );
	UxPutBorderColor( form18, ButtonBackground );
	UxPutBackground( form18, ButtonBackground );
	UxPutHeight( form18, 40 );
	UxPutWidth( form18, 490 );
	UxPutY( form18, 350 );
	UxPutX( form18, -10 );
	UxPutResizePolicy( form18, "resize_none" );

	UxPutLabelString( pB_FCapply, "Apply" );
	UxPutForeground( pB_FCapply, ApplyForeground );
	UxPutFontList( pB_FCapply, BoldTextFont );
	UxPutBorderColor( pB_FCapply, ButtonBackground );
	UxPutBackground( pB_FCapply, ButtonBackground );
	UxPutHeight( pB_FCapply, 30 );
	UxPutWidth( pB_FCapply, 86 );
	UxPutY( pB_FCapply, 5 );
	UxPutX( pB_FCapply, 15 );

	UxPutLabelString( pB_FCcancel, "Cancel" );
	UxPutForeground( pB_FCcancel, CancelForeground );
	UxPutFontList( pB_FCcancel, BoldTextFont );
	UxPutBorderColor( pB_FCcancel, ButtonBackground );
	UxPutBackground( pB_FCcancel, ButtonBackground );
	UxPutHeight( pB_FCcancel, 30 );
	UxPutWidth( pB_FCcancel, 86 );
	UxPutY( pB_FCcancel, 5 );
	UxPutX( pB_FCcancel, 124 );

	UxPutFontList( helptextFC, TextFont );
	UxPutEditable( helptextFC, "false" );
	UxPutCursorPositionVisible( helptextFC, "false" );
	UxPutBorderColor( helptextFC, SHelpBackground );
	UxPutBackground( helptextFC, SHelpBackground );
	UxPutHeight( helptextFC, 50 );
	UxPutWidth( helptextFC, 490 );
	UxPutY( helptextFC, 300 );
	UxPutX( helptextFC, 0 );

	UxPutBorderColor( separator19, WindowBackground );
	UxPutBackground( separator19, WindowBackground );
	UxPutHeight( separator19, 5 );
	UxPutWidth( separator19, 492 );
	UxPutY( separator19, 340 );
	UxPutX( separator19, 0 );

	UxPutTranslations( tf_pk, text_tab1 );
	UxPutText( tf_pk, "1." );
	UxPutForeground( tf_pk, TextForeground );
	UxPutHighlightOnEnter( tf_pk, "true" );
	UxPutFontList( tf_pk, TextFont );
	UxPutBorderColor( tf_pk, TextBackground );
	UxPutBackground( tf_pk, TextBackground );
	UxPutHeight( tf_pk, 35 );
	UxPutWidth( tf_pk, 65 );
	UxPutY( tf_pk, 123 );
	UxPutX( tf_pk, 225 );

	UxPutForeground( lbl_cwl, TextForeground );
	UxPutAlignment( lbl_cwl, "alignment_beginning" );
	UxPutLabelString( lbl_cwl, "Central Wavelength (nm) :" );
	UxPutFontList( lbl_cwl, TextFont );
	UxPutBorderColor( lbl_cwl, LabelBackground );
	UxPutBackground( lbl_cwl, LabelBackground );
	UxPutHeight( lbl_cwl, 30 );
	UxPutWidth( lbl_cwl, 170 );
	UxPutY( lbl_cwl, 182 );
	UxPutX( lbl_cwl, 15 );

	UxPutForeground( lbl_bwl, TextForeground );
	UxPutAlignment( lbl_bwl, "alignment_beginning" );
	UxPutLabelString( lbl_bwl, "Bandwidth (nm) :" );
	UxPutFontList( lbl_bwl, TextFont );
	UxPutBorderColor( lbl_bwl, LabelBackground );
	UxPutBackground( lbl_bwl, LabelBackground );
	UxPutHeight( lbl_bwl, 30 );
	UxPutWidth( lbl_bwl, 105 );
	UxPutY( lbl_bwl, 222 );
	UxPutX( lbl_bwl, 15 );

	UxPutForeground( lbl_pkl, TextForeground );
	UxPutAlignment( lbl_pkl, "alignment_beginning" );
	UxPutLabelString( lbl_pkl, "% of the main peak :" );
	UxPutFontList( lbl_pkl, TextFont );
	UxPutBorderColor( lbl_pkl, LabelBackground );
	UxPutBackground( lbl_pkl, LabelBackground );
	UxPutHeight( lbl_pkl, 30 );
	UxPutWidth( lbl_pkl, 140 );
	UxPutY( lbl_pkl, 262 );
	UxPutX( lbl_pkl, 15 );

	UxPutTranslations( tf_cwl, text_tab1 );
	UxPutText( tf_cwl, "1." );
	UxPutForeground( tf_cwl, TextForeground );
	UxPutHighlightOnEnter( tf_cwl, "true" );
	UxPutFontList( tf_cwl, TextFont );
	UxPutBorderColor( tf_cwl, TextBackground );
	UxPutBackground( tf_cwl, TextBackground );
	UxPutHeight( tf_cwl, 35 );
	UxPutWidth( tf_cwl, 65 );
	UxPutY( tf_cwl, 180 );
	UxPutX( tf_cwl, 225 );

	UxPutX( meFC, 170 );
	UxPutY( meFC, 0 );
	UxPutMenuAccelerator( meFC, "<KeyUp>F10" );
	UxPutBorderColor( meFC, MenuBackground );
	UxPutBackground( meFC, MenuBackground );
	UxPutRowColumnType( meFC, "menu_bar" );

	UxPutBorderColor( m_fc_file, MenuBackground );
	UxPutBackground( m_fc_file, MenuBackground );
	UxPutRowColumnType( m_fc_file, "menu_pulldown" );

	UxPutForeground( me_fc_open, MenuForeground );
	UxPutFontList( me_fc_open, BoldTextFont );
	UxPutBorderColor( me_fc_open, MenuBackground );
	UxPutBackground( me_fc_open, MenuBackground );
	UxPutLabelString( me_fc_open, "Open ..." );

	UxPutForeground( me_fc_save, MenuForeground );
	UxPutFontList( me_fc_save, BoldTextFont );
	UxPutBorderColor( me_fc_save, MenuBackground );
	UxPutBackground( me_fc_save, MenuBackground );
	UxPutLabelString( me_fc_save, "Save" );

	UxPutForeground( me_fc_saveas, MenuForeground );
	UxPutFontList( me_fc_saveas, BoldTextFont );
	UxPutBorderColor( me_fc_saveas, MenuBackground );
	UxPutBackground( me_fc_saveas, MenuBackground );
	UxPutLabelString( me_fc_saveas, "Save as ..." );

	UxPutForeground( meFC_file, MenuForeground );
	UxPutFontList( meFC_file, BoldTextFont );
	UxPutBorderColor( meFC_file, MenuBackground );
	UxPutMnemonic( meFC_file, "F" );
	UxPutBackground( meFC_file, MenuBackground );
	UxPutLabelString( meFC_file, "File" );

	UxPutBackground( m_fc_help, MenuBackground );
	UxPutBorderColor( m_fc_help, MenuBackground );
	UxPutRowColumnType( m_fc_help, "menu_pulldown" );

	UxPutForeground( me_fc_hm, MenuForeground );
	UxPutFontList( me_fc_hm, BoldTextFont );
	UxPutBorderColor( me_fc_hm, MenuBackground );
	UxPutBackground( me_fc_hm, MenuBackground );
	UxPutMnemonic( me_fc_hm, "M" );
	UxPutLabelString( me_fc_hm, "On Create Filter ..." );

	UxPutBorderColor( meFC_help, MenuBackground );
	UxPutForeground( meFC_help, MenuForeground );
	UxPutFontList( meFC_help, BoldTextFont );
	UxPutBackground( meFC_help, MenuBackground );
	UxPutMnemonic( meFC_help, "H" );
	UxPutLabelString( meFC_help, "Help" );

	UxPutBackground( m_fc_quit, MenuBackground );
	UxPutBorderColor( m_fc_quit, MenuBackground );
	UxPutRowColumnType( m_fc_quit, "menu_pulldown" );

	UxPutForeground( me_fc_bye, MenuForeground );
	UxPutFontList( me_fc_bye, BoldTextFont );
	UxPutBorderColor( me_fc_bye, MenuBackground );
	UxPutBackground( me_fc_bye, MenuBackground );
	UxPutMnemonic( me_fc_bye, "B" );
	UxPutLabelString( me_fc_bye, "Bye" );

	UxPutBorderColor( meFC_quit, MenuBackground );
	UxPutForeground( meFC_quit, MenuForeground );
	UxPutFontList( meFC_quit, BoldTextFont );
	UxPutBackground( meFC_quit, MenuBackground );
	UxPutMnemonic( meFC_quit, "Q" );
	UxPutLabelString( meFC_quit, "Quit" );

	UxPutBorderColor( separator22, WindowBackground );
	UxPutBackground( separator22, WindowBackground );
	UxPutHeight( separator22, 5 );
	UxPutWidth( separator22, 115 );
	UxPutY( separator22, 165 );
	UxPutX( separator22, 0 );

	UxPutTranslations( tf_cw, text_tab1 );
	UxPutText( tf_cw, "1." );
	UxPutForeground( tf_cw, TextForeground );
	UxPutHighlightOnEnter( tf_cw, "true" );
	UxPutFontList( tf_cw, TextFont );
	UxPutBorderColor( tf_cw, TextBackground );
	UxPutBackground( tf_cw, TextBackground );
	UxPutHeight( tf_cw, 35 );
	UxPutWidth( tf_cw, 65 );
	UxPutY( tf_cw, 45 );
	UxPutX( tf_cw, 225 );

	UxPutTranslations( tf_bw, text_tab1 );
	UxPutText( tf_bw, "1." );
	UxPutForeground( tf_bw, TextForeground );
	UxPutHighlightOnEnter( tf_bw, "true" );
	UxPutFontList( tf_bw, TextFont );
	UxPutBorderColor( tf_bw, TextBackground );
	UxPutBackground( tf_bw, TextBackground );
	UxPutHeight( tf_bw, 35 );
	UxPutWidth( tf_bw, 65 );
	UxPutY( tf_bw, 83 );
	UxPutX( tf_bw, 225 );

	UxPutBorderColor( separator20, WindowBackground );
	UxPutBackground( separator20, WindowBackground );
	UxPutHeight( separator20, 5 );
	UxPutWidth( separator20, 100 );
	UxPutY( separator20, 165 );
	UxPutX( separator20, 195 );

	UxPutForeground( txt_pk, TextForeground );
	UxPutAlignment( txt_pk, "alignment_beginning" );
	UxPutLabelString( txt_pk, "Red Leak" );
	UxPutFontList( txt_pk, BoldTextFont );
	UxPutBorderColor( txt_pk, LabelBackground );
	UxPutBackground( txt_pk, LabelBackground );
	UxPutHeight( txt_pk, 30 );
	UxPutWidth( txt_pk, 72 );
	UxPutY( txt_pk, 150 );
	UxPutX( txt_pk, 125 );

	UxPutTranslations( tf_bwl, text_tab1 );
	UxPutText( tf_bwl, "1." );
	UxPutForeground( tf_bwl, TextForeground );
	UxPutHighlightOnEnter( tf_bwl, "true" );
	UxPutFontList( tf_bwl, TextFont );
	UxPutBorderColor( tf_bwl, TextBackground );
	UxPutBackground( tf_bwl, TextBackground );
	UxPutHeight( tf_bwl, 35 );
	UxPutWidth( tf_bwl, 65 );
	UxPutY( tf_bwl, 220 );
	UxPutX( tf_bwl, 225 );

	UxPutTranslations( tf_pkl, text_tab1 );
	UxPutText( tf_pkl, "1." );
	UxPutForeground( tf_pkl, TextForeground );
	UxPutHighlightOnEnter( tf_pkl, "true" );
	UxPutFontList( tf_pkl, TextFont );
	UxPutBorderColor( tf_pkl, TextBackground );
	UxPutBackground( tf_pkl, TextBackground );
	UxPutHeight( tf_pkl, 35 );
	UxPutWidth( tf_pkl, 65 );
	UxPutY( tf_pkl, 260 );
	UxPutX( tf_pkl, 225 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_FilterCreate()
{
	/* Create the swidgets */

	FilterCreate = UxCreateApplicationShell( "FilterCreate", NO_PARENT );
	UxPutContext( FilterCreate, UxFilterCreateContext );

	FilterForm = UxCreateForm( "FilterForm", FilterCreate );
	lbl_cw = UxCreateLabel( "lbl_cw", FilterForm );
	lbl_bw = UxCreateLabel( "lbl_bw", FilterForm );
	lbl_peak = UxCreateLabel( "lbl_peak", FilterForm );
	form18 = UxCreateForm( "form18", FilterForm );
	pB_FCapply = UxCreatePushButton( "pB_FCapply", form18 );
	pB_FCcancel = UxCreatePushButton( "pB_FCcancel", form18 );
	helptextFC = UxCreateText( "helptextFC", FilterForm );
	separator19 = UxCreateSeparator( "separator19", FilterForm );
	tf_pk = UxCreateText( "tf_pk", FilterForm );
	lbl_cwl = UxCreateLabel( "lbl_cwl", FilterForm );
	lbl_bwl = UxCreateLabel( "lbl_bwl", FilterForm );
	lbl_pkl = UxCreateLabel( "lbl_pkl", FilterForm );
	tf_cwl = UxCreateText( "tf_cwl", FilterForm );
	meFC = UxCreateRowColumn( "meFC", FilterForm );
	m_fc_file = UxCreateRowColumn( "m_fc_file", meFC );
	me_fc_open = UxCreatePushButton( "me_fc_open", m_fc_file );
	me_fc_save = UxCreatePushButton( "me_fc_save", m_fc_file );
	me_fc_saveas = UxCreatePushButton( "me_fc_saveas", m_fc_file );
	meFC_file = UxCreateCascadeButton( "meFC_file", meFC );
	m_fc_help = UxCreateRowColumn( "m_fc_help", meFC );
	me_fc_hm = UxCreatePushButton( "me_fc_hm", m_fc_help );
	meFC_help = UxCreateCascadeButton( "meFC_help", meFC );
	m_fc_quit = UxCreateRowColumn( "m_fc_quit", meFC );
	me_fc_bye = UxCreatePushButton( "me_fc_bye", m_fc_quit );
	meFC_quit = UxCreateCascadeButton( "meFC_quit", meFC );
	separator22 = UxCreateSeparator( "separator22", FilterForm );
	tf_cw = UxCreateText( "tf_cw", FilterForm );
	tf_bw = UxCreateText( "tf_bw", FilterForm );
	separator20 = UxCreateSeparator( "separator20", FilterForm );
	txt_pk = UxCreateLabel( "txt_pk", FilterForm );
	tf_bwl = UxCreateText( "tf_bwl", FilterForm );
	tf_pkl = UxCreateText( "tf_pkl", FilterForm );

	_Uxinit_FilterCreate();

	/* Create the X widgets */

	UxCreateWidget( FilterCreate );
	UxCreateWidget( FilterForm );
	UxCreateWidget( lbl_cw );
	UxCreateWidget( lbl_bw );
	UxCreateWidget( lbl_peak );
	UxPutBottomOffset( form18, 2 );
	UxPutBottomAttachment( form18, "attach_form" );
	UxPutTopOffset( form18, 5 );
	UxPutTopAttachment( form18, "attach_none" );
	UxPutRightOffset( form18, 0 );
	UxPutRightAttachment( form18, "attach_form" );
	UxPutLeftOffset( form18, 0 );
	UxPutLeftAttachment( form18, "attach_form" );
	UxCreateWidget( form18 );

	UxCreateWidget( pB_FCapply );
	UxCreateWidget( pB_FCcancel );
	UxPutRightOffset( helptextFC, 2 );
	UxPutRightAttachment( helptextFC, "attach_form" );
	UxPutLeftOffset( helptextFC, 2 );
	UxPutLeftAttachment( helptextFC, "attach_form" );
	UxPutBottomWidget( helptextFC, "form18" );
	UxPutBottomOffset( helptextFC, 2 );
	UxPutBottomAttachment( helptextFC, "attach_widget" );
	UxCreateWidget( helptextFC );

	UxPutRightOffset( separator19, 0 );
	UxPutRightAttachment( separator19, "attach_form" );
	UxPutLeftAttachment( separator19, "attach_form" );
	UxPutBottomWidget( separator19, "helptextFC" );
	UxPutBottomOffset( separator19, 2 );
	UxPutBottomAttachment( separator19, "attach_widget" );
	UxCreateWidget( separator19 );

	UxCreateWidget( tf_pk );
	UxCreateWidget( lbl_cwl );
	UxCreateWidget( lbl_bwl );
	UxCreateWidget( lbl_pkl );
	UxCreateWidget( tf_cwl );
	UxPutTopAttachment( meFC, "attach_form" );
	UxPutRightAttachment( meFC, "attach_form" );
	UxPutLeftAttachment( meFC, "attach_form" );
	UxCreateWidget( meFC );

	UxCreateWidget( m_fc_file );
	UxCreateWidget( me_fc_open );
	UxCreateWidget( me_fc_save );
	UxCreateWidget( me_fc_saveas );
	UxPutSubMenuId( meFC_file, "m_fc_file" );
	UxCreateWidget( meFC_file );

	UxCreateWidget( m_fc_help );
	UxCreateWidget( me_fc_hm );
	UxPutSubMenuId( meFC_help, "m_fc_help" );
	UxCreateWidget( meFC_help );

	UxCreateWidget( m_fc_quit );
	UxCreateWidget( me_fc_bye );
	UxPutSubMenuId( meFC_quit, "m_fc_quit" );
	UxCreateWidget( meFC_quit );

	UxPutRightAttachment( separator22, "attach_none" );
	UxPutLeftAttachment( separator22, "attach_form" );
	UxCreateWidget( separator22 );

	UxCreateWidget( tf_cw );
	UxCreateWidget( tf_bw );
	UxPutRightAttachment( separator20, "attach_form" );
	UxCreateWidget( separator20 );

	UxCreateWidget( txt_pk );
	UxCreateWidget( tf_bwl );
	UxCreateWidget( tf_pkl );

	UxAddCallback( pB_FCapply, XmNactivateCallback,
			activateCB_pB_FCapply,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( pB_FCcancel, XmNactivateCallback,
			activateCB_pB_FCcancel,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( tf_pk, XmNfocusCallback,
			focusCB_tf_pk,
			(XtPointer) UxFilterCreateContext );
	UxAddCallback( tf_pk, XmNlosingFocusCallback,
			losingFocusCB_tf_pk,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( tf_cwl, XmNfocusCallback,
			focusCB_tf_cwl,
			(XtPointer) UxFilterCreateContext );
	UxAddCallback( tf_cwl, XmNlosingFocusCallback,
			losingFocusCB_tf_cwl,
			(XtPointer) UxFilterCreateContext );

	UxPutMenuHelpWidget( meFC, "meFC_quit" );

	UxAddCallback( me_fc_open, XmNactivateCallback,
			activateCB_me_fc_open,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( me_fc_save, XmNactivateCallback,
			activateCB_me_fc_save,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( me_fc_saveas, XmNactivateCallback,
			activateCB_me_fc_saveas,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( me_fc_hm, XmNactivateCallback,
			activateCB_me_fc_hm,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( me_fc_bye, XmNactivateCallback,
			activateCB_me_fc_bye,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( tf_cw, XmNfocusCallback,
			focusCB_tf_cw,
			(XtPointer) UxFilterCreateContext );
	UxAddCallback( tf_cw, XmNlosingFocusCallback,
			losingFocusCB_tf_cw,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( tf_bw, XmNfocusCallback,
			focusCB_tf_bw,
			(XtPointer) UxFilterCreateContext );
	UxAddCallback( tf_bw, XmNlosingFocusCallback,
			losingFocusCB_tf_bw,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( tf_bwl, XmNfocusCallback,
			focusCB_tf_bwl,
			(XtPointer) UxFilterCreateContext );
	UxAddCallback( tf_bwl, XmNlosingFocusCallback,
			losingFocusCB_tf_bwl,
			(XtPointer) UxFilterCreateContext );

	UxAddCallback( tf_pkl, XmNfocusCallback,
			focusCB_tf_pkl,
			(XtPointer) UxFilterCreateContext );
	UxAddCallback( tf_pkl, XmNlosingFocusCallback,
			losingFocusCB_tf_pkl,
			(XtPointer) UxFilterCreateContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( FilterCreate );

	return ( FilterCreate );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_FilterCreate()
{
	swidget                 rtrn;
	_UxCFilterCreate        *UxContext;

	UxFilterCreateContext = UxContext =
		(_UxCFilterCreate *) UxMalloc( sizeof(_UxCFilterCreate) );

	rtrn = _Uxbuild_FilterCreate();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_FilterCreate()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "HelpHelp", action_HelpHelp }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_FilterCreate();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

