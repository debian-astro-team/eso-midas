/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <math.h>
#include <stdio.h>
#include <globaldef.h>
#include <model.h>

extern int read_ccd_table(), read_filt_table(), read_trans(), read_sky();



void	lec_data()
{
extern	struct s_trans	T;
extern  int	Natmos;
extern  float   Xatmos[],Yatmos[];
extern  int	Ngrism;
extern  float   Xgrism[],Ygrism[];
extern  int	Nmir;
extern  float   Xmir[],Ymir[];
extern  int	Noptics;
extern  float   Xoptics[],Yoptics[];

register int	i;

if (T.isky !=0)
	read_sky(T.skyfile);


if (T.iatmos !=0)
	{
	read_trans(T.atmosfile,Xatmos,Yatmos,&Natmos,&T.atmosmin,&T.atmosmax);
	if (T.airmass!=1.)
	    for(i=0;i<Natmos;i++)
		Yatmos[i]=pow(Yatmos[i],T.airmass);
	};

if (T.igrism !=0)
	read_trans(T.grismfile,Xgrism,Ygrism,&Ngrism,&T.grismmin,&T.grismmax);

if (T.imirror !=0)
	{
	if (T.imirror==1)
		read_trans(T.mirnewfile,Xmir,Ymir,&Nmir,&T.mirmin,&T.mirmax);
	else if (T.imirror==2)
		read_trans(T.miroldfile,Xmir,Ymir,&Nmir,&T.mirmin,&T.mirmax);
	};

if (T.ioptics !=0)
	read_trans(T.opticsfile,Xoptics,Yoptics,&Noptics,&T.optmin,&T.optmax);

if (T.ifilter !=0)
	read_filt_table(T.nfilter);

if (T.iccd !=0)
	read_ccd_table(T.nccd);
}
