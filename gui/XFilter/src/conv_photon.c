/* @(#)conv_photon.c	19.1 (ES0-DMD) 02/25/03 13:46:32 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


void	conv_photon(x,y,y2,n)

float	x[],y[],y2[];
int	n;
{
register int	i;

/* conversion du flux en nombre de photons par angstrom*m^2*s */
/* 5.03e-5=1e-19/h/c  (c exprime en Angstrom) */
/* 5.03e-4=1e-19/h/c  (c exprime en nm) */

for(i=0;i<n;i++)
	y2[i]=y[i]*5.03e-4*x[i];

}
