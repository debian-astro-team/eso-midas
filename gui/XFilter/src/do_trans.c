/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <math.h>
#include <stdio.h>
#include <globaldef.h>
#include <model.h>


extern void c_minmax(), plot_xy(), end_graphic1();
extern void set_range();



void	do_trans()
{
extern	struct s_trans	T;
extern	struct s_mod	M;
extern  float   XWmin,XWmax;
extern  int	Natmos;
extern  float   Xatmos[],Yatmos[];
extern  int	Ngrism;
extern  float   Xgrism[],Ygrism[];
extern  int	Nmir;
extern  float   Xmir[],Ymir[];
extern  int	Noptics;
extern  float   Xoptics[],Yoptics[];
extern  int	Nfilter;
extern  float   Xfilt[],Yfilt[];
extern  int	Nccd;
extern  float   Xccd[],Yccd[];
extern  float	Oxmi,Oxma,Oymi,Oyma;

register int	i;
float	xr[6000],yr[6000];
char	str[150], str1[40];
float	interpol(),poisson();
float	xmi,xma,ymi,yma;



set_range();

if (T.resolution!=0.)
	M.nmax=(int)((XWmax-XWmin)/T.resolution);
else
	M.nmax=200.;

if (M.nmax>4000)
	{
	M.nmax=4000;
	fprintf(stderr,"WARNING: array too small, resolution reduced\r\n");
	};
for(i=0;i<M.nmax;i++)
        {
        xr[i]=XWmin+((float)i)/((float) (M.nmax-1))*(XWmax-XWmin);
        yr[i]=1.;
        };

if (T.iatmos !=0)
	{
        for(i=0;i<M.nmax;i++)
              yr[i]*=interpol(xr[i],Xatmos,Yatmos,Natmos);
	};

if (T.imirror !=0)
    {
    for(i=0;i<M.nmax;i++)
        yr[i]*=yr[i]*=interpol(xr[i],Xmir,Ymir,Nmir);
    };

if (T.igrism !=0)
	{
	for(i=0;i<M.nmax;i++)
              yr[i]*=interpol(xr[i],Xgrism,Ygrism,Ngrism);
	};

if (T.ifilter !=0)
	{
	for(i=0;i<M.nmax;i++)
              yr[i]*=interpol(xr[i],Xfilt,Yfilt,Nfilter);
	};

if (T.ioptics !=0)
	{
	for(i=0;i<M.nmax;i++)
              yr[i]*=interpol(xr[i],Xoptics,Yoptics,Noptics);
	};

if (T.iccd !=0)
	{
	for(i=0;i<M.nmax;i++)
              yr[i]*=interpol(xr[i],Xccd,Yccd,Nccd);
	};

c_minmax(xr,yr,M.nmax,&xmi,&xma,&ymi,&yma);

sprintf(str1,"\1 Total Transmission");
sprintf(str,
  "TITLE=%20s;DTITL=%4.1f;LABX=%12s;LABY=%12s;DLABL=%4.1f;NGEOM;USER;BOLD;GRID;TIME;XSPACE=%1d;",
     str1, 2.0, "\1Wave [nm]", "\1 Transmission",1.0,0);

yma=Min(1.,yma+.1);
ymi=0.;

plot_xy(str,0,xr,yr,M.nmax,XWmin,XWmax,0.,yma);
end_graphic1();
Oxmi=xmi;
Oxma=xma;
Oymi=ymi;
Oyma=yma;

}
