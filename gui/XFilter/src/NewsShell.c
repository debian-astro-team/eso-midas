/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	NewsShell.c

.VERSION
 090826         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxRowCol.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxNewsShell;
	swidget	Uxform21;
	swidget	UxscrolledWindow10;
	swidget	UxrowColumn8;
	swidget	Uxtx_news;
	swidget	Uxpb_news_return;
	swidget	Uxhelp_print1;
} _UxCNewsShell;

#define NewsShell               UxNewsShellContext->UxNewsShell
#define form21                  UxNewsShellContext->Uxform21
#define scrolledWindow10        UxNewsShellContext->UxscrolledWindow10
#define rowColumn8              UxNewsShellContext->UxrowColumn8
#define tx_news                 UxNewsShellContext->Uxtx_news
#define pb_news_return          UxNewsShellContext->Uxpb_news_return
#define help_print1             UxNewsShellContext->Uxhelp_print1

static _UxCNewsShell	*UxNewsShellContext;

extern void PrintNews();



/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_NewsShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_news_return( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCNewsShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxNewsShellContext;
	UxNewsShellContext = UxContext =
			(_UxCNewsShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("NewsShell"));
	 
	
	}
	UxNewsShellContext = UxSaveCtx;
}

static void	activateCB_help_print1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCNewsShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxNewsShellContext;
	UxNewsShellContext = UxContext =
			(_UxCNewsShell *) UxGetContext( UxThisWidget );
	{
	PrintNews();
	}
	UxNewsShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_NewsShell()
{
	UxPutGeometry( NewsShell, "-0+0" );
	UxPutBackground( NewsShell, ButtonBackground );
	UxPutIconName( NewsShell, "News about filters" );
	UxPutKeyboardFocusPolicy( NewsShell, "pointer" );
	UxPutTitle( NewsShell, "News about filters" );
	UxPutHeight( NewsShell, 415 );
	UxPutWidth( NewsShell, 700 );
	UxPutY( NewsShell, 448 );
	UxPutX( NewsShell, 576 );

	UxPutBackground( form21, ButtonBackground );
	UxPutHeight( form21, 284 );
	UxPutWidth( form21, 664 );
	UxPutY( form21, 0 );
	UxPutX( form21, 0 );
	UxPutUnitType( form21, "pixels" );
	UxPutResizePolicy( form21, "resize_none" );

	UxPutBackground( scrolledWindow10, WindowBackground );
	UxPutScrollBarPlacement( scrolledWindow10, "bottom_left" );
	UxPutHeight( scrolledWindow10, 372 );
	UxPutWidth( scrolledWindow10, 710 );
	UxPutY( scrolledWindow10, 2 );
	UxPutX( scrolledWindow10, 0 );
	UxPutScrollingPolicy( scrolledWindow10, "automatic" );

	UxPutEntryAlignment( rowColumn8, "alignment_beginning" );
	UxPutBackground( rowColumn8, "white" );
	UxPutOrientation( rowColumn8, "horizontal" );
	UxPutHeight( rowColumn8, 427 );
	UxPutWidth( rowColumn8, 418 );
	UxPutY( rowColumn8, 4 );
	UxPutX( rowColumn8, 25 );

	UxPutHighlightColor( tx_news, "Black" );
	UxPutForeground( tx_news, TextForeground );
	UxPutCursorPositionVisible( tx_news, "false" );
	UxPutEditMode( tx_news, "multi_line_edit" );
	UxPutFontList( tx_news, SmallFont );
	UxPutBackground( tx_news, TextBackground );
	UxPutHeight( tx_news, 1730 );
	UxPutWidth( tx_news, 666 );
	UxPutY( tx_news, -136 );
	UxPutX( tx_news, 3 );

	UxPutForeground( pb_news_return, CancelForeground );
	UxPutHighlightOnEnter( pb_news_return, "true" );
	UxPutHighlightColor( pb_news_return, "red" );
	UxPutLabelString( pb_news_return, "Return" );
	UxPutFontList( pb_news_return, BoldTextFont );
	UxPutBackground( pb_news_return, ButtonBackground );
	UxPutHeight( pb_news_return, 32 );
	UxPutWidth( pb_news_return, 82 );
	UxPutY( pb_news_return, 378 );
	UxPutX( pb_news_return, 604 );

	UxPutBorderColor( help_print1, ButtonBackground );
	UxPutForeground( help_print1, ButtonForeground );
	UxPutHighlightOnEnter( help_print1, "true" );
	UxPutHighlightColor( help_print1, "Black" );
	UxPutFontList( help_print1, BoldTextFont );
	UxPutLabelString( help_print1, "Print" );
	UxPutBackground( help_print1, ButtonBackground );
	UxPutHeight( help_print1, 32 );
	UxPutWidth( help_print1, 82 );
	UxPutY( help_print1, 378 );
	UxPutX( help_print1, 518 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_NewsShell()
{
	/* Create the swidgets */

	NewsShell = UxCreateTopLevelShell( "NewsShell", NO_PARENT );
	UxPutContext( NewsShell, UxNewsShellContext );

	form21 = UxCreateForm( "form21", NewsShell );
	scrolledWindow10 = UxCreateScrolledWindow( "scrolledWindow10", form21 );
	rowColumn8 = UxCreateRowColumn( "rowColumn8", scrolledWindow10 );
	tx_news = UxCreateText( "tx_news", rowColumn8 );
	pb_news_return = UxCreatePushButton( "pb_news_return", form21 );
	help_print1 = UxCreatePushButton( "help_print1", form21 );

	_Uxinit_NewsShell();

	/* Create the X widgets */

	UxCreateWidget( NewsShell );
	UxCreateWidget( form21 );
	UxCreateWidget( scrolledWindow10 );
	UxCreateWidget( rowColumn8 );
	UxCreateWidget( tx_news );
	UxCreateWidget( pb_news_return );
	UxCreateWidget( help_print1 );

	UxAddCallback( pb_news_return, XmNactivateCallback,
			activateCB_pb_news_return,
			(XtPointer) UxNewsShellContext );

	UxAddCallback( help_print1, XmNactivateCallback,
			activateCB_help_print1,
			(XtPointer) UxNewsShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( NewsShell );

	return ( NewsShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_NewsShell()
{
	swidget                 rtrn;
	_UxCNewsShell           *UxContext;

	UxNewsShellContext = UxContext =
		(_UxCNewsShell *) UxMalloc( sizeof(_UxCNewsShell) );

	rtrn = _Uxbuild_NewsShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_NewsShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_NewsShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

