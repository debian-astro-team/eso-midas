/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	ModelShell.c

.VERSION
090826		last modif

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "UxLib.h"
#include "UxTogBG.h"
#include "UxCascB.h"
#include "UxSep.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <model.h>
#include <global.h>

extern  struct s_gui G;
extern  struct s_trans T;
extern	float	XWmin,XWmax;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxform4;
	swidget	Uxlbl_spec;
	swidget	Uxtf_spectrum;
	swidget	Uxlbl_atm;
	swidget	Uxlbl_airmass;
	swidget	Uxform6;
	swidget	UxpB_MReset;
	swidget	UxpB_Mtrans;
	swidget	UxpB_Mres;
	swidget	UxpB_MFCreate;
	swidget	UxpB_MPrint;
	swidget	Uxatm_mod;
	swidget	Uxtg_atmyes;
	swidget	Uxtg_atmno;
	swidget	UxhelptextModel;
	swidget	Uxseparator8;
	swidget	Uxmenu_spec;
	swidget	Uxmn_standard;
	swidget	Uxmn_hststd;
	swidget	Uxmn_ctiostd;
	swidget	Uxmn_blackbody;
	swidget	Uxmn_spec;
	swidget	Uxtf_airmass;
	swidget	Uxlbl_mirror;
	swidget	Uxlbl_grism;
	swidget	Uxlbl_filter;
	swidget	Uxme_10grism;
	swidget	Uxmn_nogrism;
	swidget	Uxmn_grism0;
	swidget	Uxmn_grism1;
	swidget	Uxmn_grism2;
	swidget	Uxmn_grism3;
	swidget	Uxmn_grism4;
	swidget	Uxmn_grism5;
	swidget	Uxmn_grism6;
	swidget	Uxmn_grism7;
	swidget	Uxmn_grism8;
	swidget	Uxmn_grism9;
	swidget	Uxmn_wgrism;
	swidget	Uxtf_filter;
	swidget	Uxlbl_ccd;
	swidget	Uxlist_filt;
	swidget	Uxtg_basic;
	swidget	Uxtg_all;
	swidget	Uxtg_ccd;
	swidget	Uxtg_ccdyes;
	swidget	Uxtg_ccdno;
	swidget	Uxtf_time;
	swidget	Uxlbl_eadu;
	swidget	Uxtf_eadu;
	swidget	UxmeModel;
	swidget	Uxme_m_file;
	swidget	Uxme_m_load;
	swidget	Uxme_m_save;
	swidget	Uxme_m_saveas;
	swidget	Uxme_m_sep;
	swidget	Uxme_m_loadcurve;
	swidget	Uxme_m_savecurve;
	swidget	Uxme_m_saveascurve;
	swidget	Uxmt_m_file;
	swidget	Uxme_m_frame;
	swidget	Uxme_m_trimx;
	swidget	Uxme_m_trimy;
	swidget	Uxme_m_unzoom;
	swidget	Uxme_m_redraw;
	swidget	Uxme_m_print;
	swidget	Uxmt_m_frame;
	swidget	Uxme_m_option;
	swidget	Uxme_m_plotoption;
	swidget	Uxme_m_plot;
	swidget	Uxme_m_oplot;
	swidget	Uxme_m_pmode;
	swidget	Uxme_m_traceoption;
	swidget	Uxme_m_line;
	swidget	Uxme_m_histo;
	swidget	Uxme_m_tmode;
	swidget	Uxmet_m_options;
	swidget	Uxme_m_help;
	swidget	Uxme_m_hm;
	swidget	Uxmt_m_help;
	swidget	Uxme_m_quit;
	swidget	Uxme_m_bye;
	swidget	Uxmt_m_quit;
	swidget	Uxseparator2;
	swidget	Uxseparator3;
	swidget	Uxseparator7;
	swidget	Uxlbl_title;
	swidget	Uxtf_SN;
	swidget	Uxtg_time_sn;
	swidget	Uxtg_etime;
	swidget	Uxtg_sn;
	swidget	Uxlbl_sky;
	swidget	Uxtg_brightness;
	swidget	Uxtg_bright;
	swidget	Uxtg_dark;
	swidget	Uxtg_lines;
	swidget	Uxtg_emli;
	swidget	Uxtg_noemli;
	swidget	Uxseparator18;
	swidget	Uxtg_sky;
	swidget	Uxtg_skyon;
	swidget	Uxtg_skyoff;
	swidget	Uxtg_extsky;
	swidget	Uxlbl_mirror1;
	swidget	Uxtg_opt;
	swidget	Uxtg_optyes;
	swidget	Uxtg_optno;
	swidget	Uxtg_mir;
	swidget	Uxtg_mirnew;
	swidget	Uxtg_mirold;
	swidget	Uxtg_mirno;
	swidget	Uxtf_disp_pix;
	swidget	Uxlbl_disp;
	swidget	Uxlabel7;
	swidget	Uxtf_TWmax;
	swidget	Uxtf_TWmin;
	swidget	Uxseparator10;
	swidget	Uxseparator4;
} _UxCModelShell;

#define form4                   UxModelShellContext->Uxform4
#define lbl_spec                UxModelShellContext->Uxlbl_spec
#define tf_spectrum             UxModelShellContext->Uxtf_spectrum
#define lbl_atm                 UxModelShellContext->Uxlbl_atm
#define lbl_airmass             UxModelShellContext->Uxlbl_airmass
#define form6                   UxModelShellContext->Uxform6
#define pB_MReset               UxModelShellContext->UxpB_MReset
#define pB_Mtrans               UxModelShellContext->UxpB_Mtrans
#define pB_Mres                 UxModelShellContext->UxpB_Mres
#define pB_MFCreate             UxModelShellContext->UxpB_MFCreate
#define pB_MPrint               UxModelShellContext->UxpB_MPrint
#define atm_mod                 UxModelShellContext->Uxatm_mod
#define tg_atmyes               UxModelShellContext->Uxtg_atmyes
#define tg_atmno                UxModelShellContext->Uxtg_atmno
#define helptextModel           UxModelShellContext->UxhelptextModel
#define separator8              UxModelShellContext->Uxseparator8
#define menu_spec               UxModelShellContext->Uxmenu_spec
#define mn_standard             UxModelShellContext->Uxmn_standard
#define mn_hststd               UxModelShellContext->Uxmn_hststd
#define mn_ctiostd              UxModelShellContext->Uxmn_ctiostd
#define mn_blackbody            UxModelShellContext->Uxmn_blackbody
#define mn_spec                 UxModelShellContext->Uxmn_spec
#define tf_airmass              UxModelShellContext->Uxtf_airmass
#define lbl_mirror              UxModelShellContext->Uxlbl_mirror
#define lbl_grism               UxModelShellContext->Uxlbl_grism
#define lbl_filter              UxModelShellContext->Uxlbl_filter
#define me_10grism              UxModelShellContext->Uxme_10grism
#define mn_nogrism              UxModelShellContext->Uxmn_nogrism
#define mn_grism0               UxModelShellContext->Uxmn_grism0
#define mn_grism1               UxModelShellContext->Uxmn_grism1
#define mn_grism2               UxModelShellContext->Uxmn_grism2
#define mn_grism3               UxModelShellContext->Uxmn_grism3
#define mn_grism4               UxModelShellContext->Uxmn_grism4
#define mn_grism5               UxModelShellContext->Uxmn_grism5
#define mn_grism6               UxModelShellContext->Uxmn_grism6
#define mn_grism7               UxModelShellContext->Uxmn_grism7
#define mn_grism8               UxModelShellContext->Uxmn_grism8
#define mn_grism9               UxModelShellContext->Uxmn_grism9
#define mn_wgrism               UxModelShellContext->Uxmn_wgrism
#define tf_filter               UxModelShellContext->Uxtf_filter
#define lbl_ccd                 UxModelShellContext->Uxlbl_ccd
#define list_filt               UxModelShellContext->Uxlist_filt
#define tg_basic                UxModelShellContext->Uxtg_basic
#define tg_all                  UxModelShellContext->Uxtg_all
#define tg_ccd                  UxModelShellContext->Uxtg_ccd
#define tg_ccdyes               UxModelShellContext->Uxtg_ccdyes
#define tg_ccdno                UxModelShellContext->Uxtg_ccdno
#define tf_time                 UxModelShellContext->Uxtf_time
#define lbl_eadu                UxModelShellContext->Uxlbl_eadu
#define tf_eadu                 UxModelShellContext->Uxtf_eadu
#define meModel                 UxModelShellContext->UxmeModel
#define me_m_file               UxModelShellContext->Uxme_m_file
#define me_m_load               UxModelShellContext->Uxme_m_load
#define me_m_save               UxModelShellContext->Uxme_m_save
#define me_m_saveas             UxModelShellContext->Uxme_m_saveas
#define me_m_sep                UxModelShellContext->Uxme_m_sep
#define me_m_loadcurve          UxModelShellContext->Uxme_m_loadcurve
#define me_m_savecurve          UxModelShellContext->Uxme_m_savecurve
#define me_m_saveascurve        UxModelShellContext->Uxme_m_saveascurve
#define mt_m_file               UxModelShellContext->Uxmt_m_file
#define me_m_frame              UxModelShellContext->Uxme_m_frame
#define me_m_trimx              UxModelShellContext->Uxme_m_trimx
#define me_m_trimy              UxModelShellContext->Uxme_m_trimy
#define me_m_unzoom             UxModelShellContext->Uxme_m_unzoom
#define me_m_redraw             UxModelShellContext->Uxme_m_redraw
#define me_m_print              UxModelShellContext->Uxme_m_print
#define mt_m_frame              UxModelShellContext->Uxmt_m_frame
#define me_m_option             UxModelShellContext->Uxme_m_option
#define me_m_plotoption         UxModelShellContext->Uxme_m_plotoption
#define me_m_plot               UxModelShellContext->Uxme_m_plot
#define me_m_oplot              UxModelShellContext->Uxme_m_oplot
#define me_m_pmode              UxModelShellContext->Uxme_m_pmode
#define me_m_traceoption        UxModelShellContext->Uxme_m_traceoption
#define me_m_line               UxModelShellContext->Uxme_m_line
#define me_m_histo              UxModelShellContext->Uxme_m_histo
#define me_m_tmode              UxModelShellContext->Uxme_m_tmode
#define met_m_options           UxModelShellContext->Uxmet_m_options
#define me_m_help               UxModelShellContext->Uxme_m_help
#define me_m_hm                 UxModelShellContext->Uxme_m_hm
#define mt_m_help               UxModelShellContext->Uxmt_m_help
#define me_m_quit               UxModelShellContext->Uxme_m_quit
#define me_m_bye                UxModelShellContext->Uxme_m_bye
#define mt_m_quit               UxModelShellContext->Uxmt_m_quit
#define separator2              UxModelShellContext->Uxseparator2
#define separator3              UxModelShellContext->Uxseparator3
#define separator7              UxModelShellContext->Uxseparator7
#define lbl_title               UxModelShellContext->Uxlbl_title
#define tf_SN                   UxModelShellContext->Uxtf_SN
#define tg_time_sn              UxModelShellContext->Uxtg_time_sn
#define tg_etime                UxModelShellContext->Uxtg_etime
#define tg_sn                   UxModelShellContext->Uxtg_sn
#define lbl_sky                 UxModelShellContext->Uxlbl_sky
#define tg_brightness           UxModelShellContext->Uxtg_brightness
#define tg_bright               UxModelShellContext->Uxtg_bright
#define tg_dark                 UxModelShellContext->Uxtg_dark
#define tg_lines                UxModelShellContext->Uxtg_lines
#define tg_emli                 UxModelShellContext->Uxtg_emli
#define tg_noemli               UxModelShellContext->Uxtg_noemli
#define separator18             UxModelShellContext->Uxseparator18
#define tg_sky                  UxModelShellContext->Uxtg_sky
#define tg_skyon                UxModelShellContext->Uxtg_skyon
#define tg_skyoff               UxModelShellContext->Uxtg_skyoff
#define tg_extsky               UxModelShellContext->Uxtg_extsky
#define lbl_mirror1             UxModelShellContext->Uxlbl_mirror1
#define tg_opt                  UxModelShellContext->Uxtg_opt
#define tg_optyes               UxModelShellContext->Uxtg_optyes
#define tg_optno                UxModelShellContext->Uxtg_optno
#define tg_mir                  UxModelShellContext->Uxtg_mir
#define tg_mirnew               UxModelShellContext->Uxtg_mirnew
#define tg_mirold               UxModelShellContext->Uxtg_mirold
#define tg_mirno                UxModelShellContext->Uxtg_mirno
#define tf_disp_pix             UxModelShellContext->Uxtf_disp_pix
#define lbl_disp                UxModelShellContext->Uxlbl_disp
#define label7                  UxModelShellContext->Uxlabel7
#define tf_TWmax                UxModelShellContext->Uxtf_TWmax
#define tf_TWmin                UxModelShellContext->Uxtf_TWmin
#define separator10             UxModelShellContext->Uxseparator10
#define separator4              UxModelShellContext->Uxseparator4

static _UxCModelShell	*UxModelShellContext;

extern void SetFileList(), DisplayShortTMHelp(), DisplayExtendedHelp();
extern void plot_spec(), plot_atmos();
extern void plot_filters(), plot_ccd();
extern void plot_sky(), plot_optics(), plot_mirror();
extern void read_ccdchar(), search_cwav(), end_graphic();
extern void end_viewp(), search_cwbw(), def_inst(), set_model();
extern void set_bbody(), set_grism(), save_mod(), save_curve();
extern void set_creafilter(), do_convo(), do_trans(), init_model();
extern void search_f_inst(), between();
extern void Trim(), Unzoom();

extern char *news_filters();

extern int AppendDialogText();



swidget	ModelShell;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*chooseTF = "#override\n\
<Btn3Down>:ChooseListUpF()\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n";

static char	*chooseTS = "#override\n\
<Btn3Down>:ChooseListUpS()\n\
<Key>Delete:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n";

static char	*text_tab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

static char	*helpTM = "#override\n\
<Btn3Down>:HelpHelp()\n\
<EnterWindow>:HelpShortTM()\n\
<LeaveWindow>:ClearShortTM()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ModelShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearShortTM( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextModel"),"");
	 UxPutText(UxFindSwidget("HelpFShell"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	action_HelpShortTM( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	DisplayShortTMHelp(UxWidget);
	UxModelShellContext = UxSaveCtx;
}

static void	action_HelpHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxModelShellContext = UxSaveCtx;
}

static void	action_ChooseListUpS( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern swidget scrolledchoose;
	char *transdir,filepath[50];
	char	*hist;
	
	hist=(char *) UxGetMenuHistory(UxFindSwidget("mn_spec"));
	if (!strcmp(hist,"mn_hststd"))
		strcpy(G.sel_type,"hststd");
	else
		strcpy(G.sel_type,"spectrum");
	
	if (!strcmp(G.sel_type,"hststd"))
	{
	transdir=(char *) getenv("MID_FILTERS");
	sprintf(filepath,"%s/hststd/*.tbl",transdir);
	SetFileList( UxGetWidget(scrolledchoose),1, filepath );
	}
	else
	{
	transdir=(char *) getenv("MID_FILTERS");
	sprintf(filepath,"%s/esostd/*.tbl",transdir);
	SetFileList( UxGetWidget(scrolledchoose),1, filepath );
	}
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	action_ChooseListUpF( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	
	strcpy(G.sel_type,"filter");
	search_f_inst(G.sel_inst,G.basic_set);
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxModelShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	motionVerifyCB_tf_spectrum( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	MOD_spectrum = 1;
	}
	UxModelShellContext = UxSaveCtx;
}

static void	focusCB_tf_spectrum( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	
	UxPutText(UxFindSwidget("helptextModel"),"spectrum");
	
	
	if (T.ispectra==0)
		UxPutText(UxFindSwidget("tf_spectrum"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_spectrum( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char spectrum[30];
	char *filepath;
	
	if (MOD_spectrum==1)
	{
	nid=sscanf(UxGetText(tf_spectrum),"%s",spectrum);
	
	if (nid<1)
		T.ispectra=0;
	
	else
		{
		T.ispectra=1;
		if (strcmp(spectrum,(char *)T.spectrafile))
			{
			strcpy((char *)T.spectrafile,spectrum);
			if(!strcmp(G.sel_type,"hststd"))
				{
				filepath=(char *)getenv("MID_FILTERS");
				sprintf((char *)T.spectrapath,"%s/hststd",filepath);
				}
			else
				{
				filepath=(char *)getenv("MID_FILTERS");
				sprintf((char *)T.spectrapath,"%s/esostd",filepath);
				}
	
			plot_spec(T.spectrapath,T.spectrafile);
			}
		};
	}
	
	MOD_spectrum=0;
	
	if (T.ispectra==0)
		UxPutText(tf_spectrum,"no spectrum");
	else
		UxPutText(tf_spectrum,T.spectrafile);
	
	
	UxPutText(UxFindSwidget("helptextModel"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_pB_MReset( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	init_model();
	def_inst(G.sel_inst);
	set_model();
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_pB_Mtrans( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	do_trans();
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_pB_Mres( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	do_convo();
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_pB_MFCreate( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	set_creafilter();
	UxPopupInterface(UxFindSwidget("FilterCreate"),no_grab);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_pB_MPrint( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	
	 extern int pltgr,putplt,putplt9,ovpltgr;
	extern char Printer[];
	
	char str[40];
	
	if(pltgr)
	    pltgr=FALSE;
	
	if(putplt)
	    putplt=FALSE;
	
	if(putplt9)
	    putplt9=FALSE;
	
	if(ovpltgr)
	         {
		  end_graphic();
	          ovpltgr=FALSE;
	         }
	sprintf( str, "copy/graph %s filter.plt", Printer );
	AppendDialogText(str);
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_atmyes( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	char	*dir;
	
	dir=(char *)getenv("MID_FILTERS");
	if (dir!=NULL)
		{
		T.iatmos=1;
		sprintf(T.atmosfile,"%s/atmos/extatmos.dat",dir);
		plot_atmos(T.atmosfile);
		}
	else
		{
		fprintf(stderr,"WARNING: do not know where to find data\n");
		fprintf(stderr,"\t please set the FILTERDIR env. variable\n");
		};
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_atmno( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.iatmos=0;
	
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	valueChangedCB_tg_atmno( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_standard( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	char *transdir,filepath[50];
	extern swidget scrolledchoose;
	
	strcpy(G.sel_type,"spectrum");
	transdir=(char *) getenv("MID_FILTERS");
	sprintf(filepath,"%s/esostd/*.tbl",transdir);
	SetFileList( UxGetWidget(scrolledchoose),1, filepath );
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_hststd( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	char *transdir,filepath[50];
	extern swidget scrolledchoose;
	
	strcpy(G.sel_type,"hststd");
	transdir=(char *) getenv("MID_FILTERS");
	sprintf(filepath,"%s/hststd/*.tbl",transdir);
	SetFileList( UxGetWidget(scrolledchoose),1, filepath );
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_ctiostd( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	char *transdir,filepath[50];
	extern swidget scrolledchoose;
	
	strcpy(G.sel_type,"ctiostd");
	transdir=(char *) getenv("MID_FILTERS");
	sprintf(filepath,"%s/ctiostd/*.tbl",transdir);
	SetFileList( UxGetWidget(scrolledchoose),1, filepath );
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_blackbody( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	set_bbody();
	UxModelShellContext = UxSaveCtx;
}

static void	focusCB_tf_airmass( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextModel"),"airmass value\n");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_airmass( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	
	nid=sscanf(UxGetText(tf_airmass),"%f",&T.airmass);
	if ((nid<1)||(T.airmass<1.))
		T.airmass=1.;
	if (T.airmass>10.)
		T.airmass=10.;
	
	sprintf(am,"%1.3f",T.airmass);
	UxPutText(UxFindSwidget("tf_airmass"),am);
	
	
	UxPutText(UxFindSwidget("helptextModel"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_nogrism( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=0;
	T.ngrism=0;
	T.resolution=0.;
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism0( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=1;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=2;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=3;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=4;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=5;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=6;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=7;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=8;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=9;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_mn_grism9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	T.igrism=1;
	T.ngrism=10;
	
	set_grism(T.ngrism);
	UxModelShellContext = UxSaveCtx;
}

static void	focusCB_tf_filter( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	char	number[10];
	
	UxPutText(UxFindSwidget("helptextModel"),"ESO Filter Number\n");
	
	if (T.ifilter ==0)
		UxPutText(UxFindSwidget("tf_filter"),"");
	else if (T.ifilter ==1)
		{
		sprintf(number,"%d",T.nfilter);
		UxPutText(UxFindSwidget("tf_filter"),number);
		}
	else if (T.ifilter ==2)
		UxPutText(UxFindSwidget("tf_filter"),"crea_filter");
	
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_tf_filter( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	MOD_FilterNUMBER = 1;
	}
	UxModelShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_filter( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	int nid=0,nfilt=0,search_nfilt();
	char	am[10];
	
	
	if(MOD_FilterNUMBER)
	{
	nid=sscanf(UxGetText(UxFindSwidget("tf_filter")),"%d",&nfilt);
	  if (nid>0)
	     {
	     if (nfilt!=T.nfilter)
		{
		T.nfilter=nfilt;
		if(search_nfilt(T.nfilter)!=-1)
			{
			plot_filters(T.nfilter);
			T.ifilter=1;
			}
		else
			T.ifilter=0;
		};
	     }
	  else if (!strcmp(UxGetText(UxFindSwidget("tf_filter")),"crea_filter"))
		T.ifilter=2;
	  else
		T.ifilter=0;
	
	}
	
	if (T.ifilter==0)
		UxPutText(UxFindSwidget("tf_filter"),"free");
	else if (T.ifilter==1)
		{
		sprintf(am,"%d",T.nfilter);
		UxPutText(UxFindSwidget("tf_filter"),am);
		}
	if (T.ifilter==2)
		UxPutText(UxFindSwidget("tf_filter"),"crea_filter");
	
	UxPutText(UxFindSwidget("helptextModel"),"");
	
	MOD_FilterNUMBER=0;
	
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_basic( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	G.basic_set=1;
	strcpy(G.sel_type,"filter");
	
	search_f_inst(G.sel_inst,G.basic_set);
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_all( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	G.basic_set=0;
	strcpy(G.sel_type,"filter");
	search_f_inst(G.sel_inst,G.basic_set);
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_ccdyes( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.iccd=1;
	plot_ccd(T.nccd);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_ccdno( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.iccd=0;
	}
	UxModelShellContext = UxSaveCtx;
}

static void	focusCB_tf_time( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextModel"),"Exposure Time in seconds\n");
	 
	}
	UxModelShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_time( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	 
	nid=sscanf(UxGetText(tf_time),"%f",&T.etime);
	if ((nid==0)||(T.etime<.01))
		T.etime=.01;
	sprintf(am,"%1.2f",T.etime);
	
	UxPutText(tf_time,am);
	
	UxPutText(UxFindSwidget("helptextModel"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	focusCB_tf_eadu( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextModel"),"number of electrons in ADU");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_eadu( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float eadu;
	
	nid=sscanf(UxGetText(tf_eadu),"%f",&eadu);
	if ((nid==1)||(eadu>=1.))
		T.einadu=eadu;
	
	
	sprintf(am,"%.2f",T.einadu);
	UxPutText(tf_eadu,am);
	
	
	UxPutText(UxFindSwidget("helptextModel"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_load( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern swidget scrolledchoose;
	extern struct s_gui    G;
	
	strcpy(G.sel_type,"trans_load");
	
	SetFileList( UxGetWidget(scrolledchoose),1, "*.trans" );
	
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_save( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	save_mod(T.transfile);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_saveas( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	strcpy(G.sel_type,"trans_save");
	UxPutTitle( UxFindSwidget("AskShell"),"Configuration Save Name");
	UxPutLabelString( UxFindSwidget("labelAsk"), "Filename (no ext.):" );
	
	UxPopupInterface(UxFindSwidget("AskShell"),no_grab);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_loadcurve( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern swidget scrolledchoose;
	extern struct s_gui    G;
	
	strcpy(G.sel_type,"curve_load");
	
	SetFileList( UxGetWidget(scrolledchoose),1, "*.dat" );
	
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_savecurve( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	save_curve(T.curvefile);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_saveascurve( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	strcpy(G.sel_type,"curve_save");
	UxPutText( UxFindSwidget("textAsk"),T.curvefile);
	UxPutTitle( UxFindSwidget("AskShell"),"Save Name");
	UxPutLabelString( UxFindSwidget("labelAsk"), "Filename :" );
	
	UxPopupInterface(UxFindSwidget("AskShell"),no_grab);
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_trimx( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern int ltrimx,ltrimy;
	
	ltrimx=TRUE;
	ltrimy=FALSE;
	
	Trim();
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_trimy( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern int ltrimx,ltrimy;
	
	ltrimx=FALSE;
	ltrimy=TRUE;
	
	Trim();
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_unzoom( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern int ltrimx,ltrimy;
	
	ltrimx=FALSE;
	ltrimy=FALSE;
	
	Unzoom();
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_redraw( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern int ovpltgr;
	
	if(ovpltgr)
	       {
		end_graphic();
	       }
	AppendDialogText( "copy/graph G filter.plt \n" );
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_print( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	 extern int pltgr,putplt,putplt9,ovpltgr;
	extern char Printer[];
	
	char str[40];
	
	if(pltgr)
	    pltgr=FALSE;
	
	if(putplt)
	    putplt=FALSE;
	
	if(putplt9)
	    putplt9=FALSE;
	
	if(ovpltgr)
	         {
		  end_graphic();
	          ovpltgr=FALSE;
	         }
	sprintf( str, "copy/graph %s filter.plt", Printer );
	AppendDialogText(str);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_m_plot( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern int plt4gr,pltgr,ovplt4gr,ovpltgr,putplt,putplt9;
	
	end_viewp();
	
	        plt4gr=FALSE;
	        pltgr=TRUE;
	        ovplt4gr=FALSE;
	        ovpltgr=FALSE;
		putplt=FALSE;
	        putplt9=FALSE;
	
	UxPutSet(UxFindSwidget("me_f_plot"),"true");
	UxPutSet(UxFindSwidget("me_f_oplot"),"false");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_m_oplot( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	
	extern int plt4gr,pltgr,ovplt4gr,ovpltgr,putplt,putplt9,lfirstp;
	extern int nc_ovplt;
	
	        if(ovpltgr && !lfirstp )
	         {
		end_graphic();
	         }
	
	        ovpltgr=TRUE;
	        ovplt4gr=FALSE;
	        pltgr=FALSE;
		putplt9=FALSE;
	        plt4gr=FALSE;
	        putplt=FALSE;
	        lfirstp=TRUE;
	        nc_ovplt=0;
	
	UxPutSet(UxFindSwidget("me_f_plot"),"false");
	UxPutSet(UxFindSwidget("me_f_oplot"),"true");
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_m_line( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern int tracemode;
	
	tracemode=FALSE;
	UxPutSet(UxFindSwidget("me_f_line"),"true");
	UxPutSet(UxFindSwidget("me_f_histo"),"false");
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_m_histo( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	extern int tracemode;
	
	tracemode=TRUE;
	
	UxPutSet(UxFindSwidget("me_f_histo"),"true");
	UxPutSet(UxFindSwidget("me_f_line"),"false");
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_hm( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	activateCB_me_m_bye( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	UxPopdownInterface(UxFindSwidget("ModelShell"));
	UxPopdownInterface(UxFindSwidget("BruzualModel"));
	UxModelShellContext = UxSaveCtx;
}

static void	focusCB_tf_SN( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextModel"),"Global Signal to Noise Ratio \n");
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_SN( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	
	nid=sscanf(UxGetText(tf_SN),"%f",&T.SN);
	if ((nid==0)||(T.SN<.01))
	        T.SN=.01;
	sprintf(am,"%1.2f",T.SN);
	
	UxPutText(tf_SN,am);
	
	UxPutText(UxFindSwidget("helptextModel"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_etime( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	
	T.itimeSN=0;
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_sn( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	 T.itimeSN=1;
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_bright( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.darkness=0;
	if (T.emlines==0)
		strcpy(T.skyfile,"sky_b.dat");
	else
		strcpy(T.skyfile,"sky_b_em.dat");
	
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_dark( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.darkness=1;
	if (T.emlines==0)
		strcpy(T.skyfile,"sky_d.dat");
	else
		strcpy(T.skyfile,"sky_d_em.dat");
	
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_emli( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.emlines=1;
	if (T.darkness==0)
		strcpy(T.skyfile,"sky_b_em.dat");
	else
		strcpy(T.skyfile,"sky_d_em.dat");
	
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_noemli( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.emlines=0;
	if (T.darkness==0)
		strcpy(T.skyfile,"sky_b.dat");
	else
		strcpy(T.skyfile,"sky_d.dat");
	
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_skyon( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.isky=1;
	plot_sky(T.skyfile);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_skyoff( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	
	T.isky=0;
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_optyes( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.ioptics=1;
	plot_optics(T.opticsfile);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_optno( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.ioptics=0;
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_mirnew( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.imirror=1;
	plot_optics(T.mirnewfile);
	
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_mirold( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.imirror=2;
	plot_mirror(T.miroldfile);
	}
	UxModelShellContext = UxSaveCtx;
}

static void	armCB_tg_mirno( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	T.imirror=0;
	}
	UxModelShellContext = UxSaveCtx;
}

static void	focusCB_tf_disp_pix( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextModel"),"Resolution in nm/pixel of the dispersor");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_disp_pix( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	
	UxPutText(UxFindSwidget("helptextModel"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	focusCB_tf_TWmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextModel"),"Maximum Wavelength (in nm)");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_TWmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float maxw;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_TWmax")),"%f",&maxw);
	if (nid==1)
		 between(&XWmax,maxw,.5,2000.);
	
	sprintf(am,"%.1f",XWmax);
	UxPutText(UxFindSwidget("tf_TWmax"),am);
	
	
	UxPutText(UxFindSwidget("helptextModel"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	focusCB_tf_TWmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextModel"),"Minimum Wavelength (in nm)");
	}
	UxModelShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_TWmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCModelShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxModelShellContext;
	UxModelShellContext = UxContext =
			(_UxCModelShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float minw;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_TWmin")),"%f",&minw);
	if (nid==1)
		 between(&XWmin,minw,.5,2000.);
	
	sprintf(am,"%.1f",XWmin);
	UxPutText(UxFindSwidget("tf_TWmin"),am);
	
	
	UxPutText(UxFindSwidget("helptextModel"),"");
	}
	UxModelShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ModelShell()
{
	UxPutY( ModelShell, 300 );
	UxPutX( ModelShell, 100 );
	UxPutTitle( ModelShell, "Transmission_Model" );
	UxPutKeyboardFocusPolicy( ModelShell, "pointer" );
	UxPutBorderColor( ModelShell, WindowBackground );
	UxPutWinGravity( ModelShell, "forget_gravity" );
	UxPutGeometry( ModelShell, "+650+60" );
	UxPutDefaultFontList( ModelShell, TextFont );
	UxPutIconName( ModelShell, "Transmission_Model" );
	UxPutHeight( ModelShell, 640 );
	UxPutWidth( ModelShell, 440 );

	UxPutBorderColor( form4, WindowBackground );
	UxPutBackground( form4, WindowBackground );
	UxPutHeight( form4, 540 );
	UxPutWidth( form4, 490 );
	UxPutY( form4, 40 );
	UxPutX( form4, 75 );
	UxPutUnitType( form4, "pixels" );
	UxPutResizePolicy( form4, "resize_none" );

	UxPutForeground( lbl_spec, TextForeground );
	UxPutAlignment( lbl_spec, "alignment_beginning" );
	UxPutLabelString( lbl_spec, "Spectrum :" );
	UxPutFontList( lbl_spec, TextFont );
	UxPutBorderColor( lbl_spec, LabelBackground );
	UxPutBackground( lbl_spec, LabelBackground );
	UxPutHeight( lbl_spec, 25 );
	UxPutWidth( lbl_spec, 74 );
	UxPutY( lbl_spec, 79 );
	UxPutX( lbl_spec, 16 );

	UxPutText( tf_spectrum, "no spectrum" );
	UxPutTranslations( tf_spectrum, chooseTS );
	UxPutMaxLength( tf_spectrum, 50 );
	UxPutForeground( tf_spectrum, TextForeground );
	UxPutHighlightOnEnter( tf_spectrum, "true" );
	UxPutFontList( tf_spectrum, TextFont );
	UxPutBorderColor( tf_spectrum, TextBackground );
	UxPutBackground( tf_spectrum, TextBackground );
	UxPutHeight( tf_spectrum, 35 );
	UxPutWidth( tf_spectrum, 190 );
	UxPutY( tf_spectrum, 74 );
	UxPutX( tf_spectrum, 234 );

	UxPutForeground( lbl_atm, TextForeground );
	UxPutAlignment( lbl_atm, "alignment_beginning" );
	UxPutLabelString( lbl_atm, "Atmospheric\nextinction :" );
	UxPutFontList( lbl_atm, TextFont );
	UxPutBorderColor( lbl_atm, LabelBackground );
	UxPutBackground( lbl_atm, LabelBackground );
	UxPutHeight( lbl_atm, 39 );
	UxPutWidth( lbl_atm, 88 );
	UxPutY( lbl_atm, 181 );
	UxPutX( lbl_atm, 12 );

	UxPutForeground( lbl_airmass, TextForeground );
	UxPutAlignment( lbl_airmass, "alignment_beginning" );
	UxPutLabelString( lbl_airmass, "Airmass :" );
	UxPutFontList( lbl_airmass, TextFont );
	UxPutBorderColor( lbl_airmass, LabelBackground );
	UxPutBackground( lbl_airmass, LabelBackground );
	UxPutHeight( lbl_airmass, 30 );
	UxPutWidth( lbl_airmass, 70 );
	UxPutY( lbl_airmass, 185 );
	UxPutX( lbl_airmass, 270 );

	UxPutHorizontalSpacing( form6, 10 );
	UxPutButtonFontList( form6, BoldTextFont );
	UxPutBorderColor( form6, ButtonBackground );
	UxPutBackground( form6, ButtonBackground );
	UxPutHeight( form6, 66 );
	UxPutWidth( form6, 440 );
	UxPutY( form6, 572 );
	UxPutX( form6, 0 );
	UxPutResizePolicy( form6, "resize_none" );

	UxPutWidth( pB_MReset, 120 );
	UxPutTranslations( pB_MReset, helpTM );
	UxPutLabelString( pB_MReset, "Reset" );
	UxPutForeground( pB_MReset, ButtonForeground );
	UxPutFontList( pB_MReset, BoldTextFont );
	UxPutBorderColor( pB_MReset, ButtonBackground );
	UxPutBackground( pB_MReset, ButtonBackground );
	UxPutHeight( pB_MReset, 30 );
	UxPutY( pB_MReset, 2 );
	UxPutX( pB_MReset, 14 );

	UxPutWidth( pB_Mtrans, 120 );
	UxPutTranslations( pB_Mtrans, helpTM );
	UxPutLabelString( pB_Mtrans, "Transmission" );
	UxPutForeground( pB_Mtrans, ApplyForeground );
	UxPutFontList( pB_Mtrans, BoldTextFont );
	UxPutBorderColor( pB_Mtrans, ButtonBackground );
	UxPutBackground( pB_Mtrans, ButtonBackground );
	UxPutHeight( pB_Mtrans, 30 );
	UxPutY( pB_Mtrans, 34 );
	UxPutX( pB_Mtrans, 15 );

	UxPutWidth( pB_Mres, 120 );
	UxPutTranslations( pB_Mres, helpTM );
	UxPutLabelString( pB_Mres, "Apply" );
	UxPutForeground( pB_Mres, ApplyForeground );
	UxPutFontList( pB_Mres, BoldTextFont );
	UxPutBorderColor( pB_Mres, ButtonBackground );
	UxPutBackground( pB_Mres, ButtonBackground );
	UxPutHeight( pB_Mres, 30 );
	UxPutY( pB_Mres, 34 );
	UxPutX( pB_Mres, 160 );

	UxPutWidth( pB_MFCreate, 120 );
	UxPutY( pB_MFCreate, 5 );
	UxPutX( pB_MFCreate, 308 );
	UxPutTranslations( pB_MFCreate, helpTM );
	UxPutLabelString( pB_MFCreate, "Create Filter ..." );
	UxPutForeground( pB_MFCreate, ButtonForeground );
	UxPutFontList( pB_MFCreate, BoldTextFont );
	UxPutBorderColor( pB_MFCreate, ButtonBackground );
	UxPutBackground( pB_MFCreate, ButtonBackground );
	UxPutHeight( pB_MFCreate, 30 );

	UxPutWidth( pB_MPrint, 120 );
	UxPutTranslations( pB_MPrint, helpTM );
	UxPutLabelString( pB_MPrint, "Print" );
	UxPutForeground( pB_MPrint, ButtonForeground );
	UxPutFontList( pB_MPrint, BoldTextFont );
	UxPutBorderColor( pB_MPrint, ButtonBackground );
	UxPutBackground( pB_MPrint, ButtonBackground );
	UxPutHeight( pB_MPrint, 30 );
	UxPutY( pB_MPrint, 5 );
	UxPutX( pB_MPrint, 160 );

	UxPutSpacing( atm_mod, 0 );
	UxPutMarginHeight( atm_mod, 0 );
	UxPutOrientation( atm_mod, "horizontal" );
	UxPutIsAligned( atm_mod, "true" );
	UxPutEntryAlignment( atm_mod, "alignment_beginning" );
	UxPutBorderWidth( atm_mod, 0 );
	UxPutShadowThickness( atm_mod, 0 );
	UxPutLabelString( atm_mod, "" );
	UxPutEntryBorder( atm_mod, 0 );
	UxPutBorderColor( atm_mod, WindowBackground );
	UxPutBackground( atm_mod, WindowBackground );
	UxPutRadioBehavior( atm_mod, "true" );
	UxPutHeight( atm_mod, 37 );
	UxPutWidth( atm_mod, 171 );
	UxPutY( atm_mod, 186 );
	UxPutX( atm_mod, 121 );

	UxPutTranslations( tg_atmyes, helpTM );
	UxPutHighlightOnEnter( tg_atmyes, "true" );
	UxPutForeground( tg_atmyes, TextForeground );
	UxPutSelectColor( tg_atmyes, SelectColor );
	UxPutSet( tg_atmyes, "true" );
	UxPutLabelString( tg_atmyes, "yes" );
	UxPutFontList( tg_atmyes, TextFont );
	UxPutBorderColor( tg_atmyes, WindowBackground );
	UxPutBackground( tg_atmyes, WindowBackground );
	UxPutHeight( tg_atmyes, 28 );
	UxPutWidth( tg_atmyes, 77 );
	UxPutY( tg_atmyes, 200 );
	UxPutX( tg_atmyes, 3 );

	UxPutTranslations( tg_atmno, helpTM );
	UxPutHighlightOnEnter( tg_atmno, "true" );
	UxPutSet( tg_atmno, "false" );
	UxPutForeground( tg_atmno, TextForeground );
	UxPutSelectColor( tg_atmno, SelectColor );
	UxPutLabelString( tg_atmno, "no " );
	UxPutFontList( tg_atmno, TextFont );
	UxPutBorderColor( tg_atmno, WindowBackground );
	UxPutBackground( tg_atmno, WindowBackground );
	UxPutHeight( tg_atmno, 28 );
	UxPutWidth( tg_atmno, 119 );
	UxPutY( tg_atmno, 200 );
	UxPutX( tg_atmno, 54 );

	UxPutFontList( helptextModel, TextFont );
	UxPutEditable( helptextModel, "false" );
	UxPutCursorPositionVisible( helptextModel, "false" );
	UxPutBorderColor( helptextModel, SHelpBackground );
	UxPutBackground( helptextModel, SHelpBackground );
	UxPutHeight( helptextModel, 50 );
	UxPutWidth( helptextModel, 490 );
	UxPutY( helptextModel, 300 );
	UxPutX( helptextModel, 0 );

	UxPutBorderColor( separator8, WindowBackground );
	UxPutBackground( separator8, WindowBackground );
	UxPutHeight( separator8, 5 );
	UxPutWidth( separator8, 492 );
	UxPutY( separator8, 340 );
	UxPutX( separator8, 0 );

	UxPutRowColumnType( menu_spec, "menu_pulldown" );

	UxPutForeground( mn_standard, TextForeground );
	UxPutFontList( mn_standard, TextFont );
	UxPutBorderColor( mn_standard, WindowBackground );
	UxPutBackground( mn_standard, WindowBackground );
	UxPutLabelString( mn_standard, "Standard" );

	UxPutForeground( mn_hststd, TextForeground );
	UxPutFontList( mn_hststd, TextFont );
	UxPutBorderColor( mn_hststd, WindowBackground );
	UxPutBackground( mn_hststd, WindowBackground );
	UxPutLabelString( mn_hststd, "HST Std" );

	UxPutForeground( mn_ctiostd, TextForeground );
	UxPutFontList( mn_ctiostd, TextFont );
	UxPutBorderColor( mn_ctiostd, WindowBackground );
	UxPutBackground( mn_ctiostd, WindowBackground );
	UxPutLabelString( mn_ctiostd, "CTIO Std" );

	UxPutMarginWidth( mn_blackbody, 0 );
	UxPutForeground( mn_blackbody, TextForeground );
	UxPutFontList( mn_blackbody, TextFont );
	UxPutBorderColor( mn_blackbody, WindowBackground );
	UxPutBackground( mn_blackbody, WindowBackground );
	UxPutLabelString( mn_blackbody, "Black Body ..." );

	UxPutTranslations( mn_spec, helpTM );
	UxPutHeight( mn_spec, 35 );
	UxPutSpacing( mn_spec, 0 );
	UxPutMarginWidth( mn_spec, 0 );
	UxPutForeground( mn_spec, TextForeground );
	UxPutBorderColor( mn_spec, WindowBackground );
	UxPutBackground( mn_spec, WindowBackground );
	UxPutY( mn_spec, 74 );
	UxPutX( mn_spec, 92 );
	UxPutRowColumnType( mn_spec, "menu_option" );

	UxPutTranslations( tf_airmass, text_tab );
	UxPutText( tf_airmass, "1." );
	UxPutForeground( tf_airmass, TextForeground );
	UxPutHighlightOnEnter( tf_airmass, "true" );
	UxPutFontList( tf_airmass, TextFont );
	UxPutBorderColor( tf_airmass, TextBackground );
	UxPutBackground( tf_airmass, TextBackground );
	UxPutHeight( tf_airmass, 35 );
	UxPutWidth( tf_airmass, 80 );
	UxPutY( tf_airmass, 183 );
	UxPutX( tf_airmass, 342 );

	UxPutForeground( lbl_mirror, TextForeground );
	UxPutAlignment( lbl_mirror, "alignment_beginning" );
	UxPutLabelString( lbl_mirror, "Mirror :" );
	UxPutFontList( lbl_mirror, TextFont );
	UxPutBorderColor( lbl_mirror, LabelBackground );
	UxPutBackground( lbl_mirror, LabelBackground );
	UxPutHeight( lbl_mirror, 30 );
	UxPutWidth( lbl_mirror, 62 );
	UxPutY( lbl_mirror, 230 );
	UxPutX( lbl_mirror, 14 );

	UxPutForeground( lbl_grism, TextForeground );
	UxPutAlignment( lbl_grism, "alignment_beginning" );
	UxPutLabelString( lbl_grism, "Grism :" );
	UxPutFontList( lbl_grism, TextFont );
	UxPutBorderColor( lbl_grism, LabelBackground );
	UxPutBackground( lbl_grism, LabelBackground );
	UxPutHeight( lbl_grism, 30 );
	UxPutWidth( lbl_grism, 54 );
	UxPutY( lbl_grism, 268 );
	UxPutX( lbl_grism, 14 );

	UxPutForeground( lbl_filter, TextForeground );
	UxPutAlignment( lbl_filter, "alignment_beginning" );
	UxPutLabelString( lbl_filter, "Filter :" );
	UxPutFontList( lbl_filter, TextFont );
	UxPutBorderColor( lbl_filter, LabelBackground );
	UxPutBackground( lbl_filter, LabelBackground );
	UxPutHeight( lbl_filter, 30 );
	UxPutWidth( lbl_filter, 50 );
	UxPutY( lbl_filter, 312 );
	UxPutX( lbl_filter, 14 );

	UxPutRowColumnType( me_10grism, "menu_pulldown" );

	UxPutForeground( mn_nogrism, TextForeground );
	UxPutFontList( mn_nogrism, TextFont );
	UxPutBorderColor( mn_nogrism, WindowBackground );
	UxPutBackground( mn_nogrism, WindowBackground );
	UxPutLabelString( mn_nogrism, " Free Position " );

	UxPutBorderColor( mn_grism0, WindowBackground );
	UxPutBackground( mn_grism0, WindowBackground );
	UxPutForeground( mn_grism0, TextForeground );
	UxPutFontList( mn_grism0, TextFont );
	UxPutLabelString( mn_grism0, " Grism #0 " );

	UxPutForeground( mn_grism1, TextForeground );
	UxPutFontList( mn_grism1, TextFont );
	UxPutBorderColor( mn_grism1, WindowBackground );
	UxPutBackground( mn_grism1, WindowBackground );
	UxPutLabelString( mn_grism1, " Grism #1 " );

	UxPutBorderColor( mn_grism2, WindowBackground );
	UxPutBackground( mn_grism2, WindowBackground );
	UxPutForeground( mn_grism2, TextForeground );
	UxPutFontList( mn_grism2, TextFont );
	UxPutLabelString( mn_grism2, " Grism #2 " );

	UxPutBorderColor( mn_grism3, WindowBackground );
	UxPutBackground( mn_grism3, WindowBackground );
	UxPutForeground( mn_grism3, TextForeground );
	UxPutFontList( mn_grism3, TextFont );
	UxPutLabelString( mn_grism3, " Grism #3 " );

	UxPutBorderColor( mn_grism4, WindowBackground );
	UxPutBackground( mn_grism4, WindowBackground );
	UxPutForeground( mn_grism4, TextForeground );
	UxPutFontList( mn_grism4, TextFont );
	UxPutLabelString( mn_grism4, " Grism #4 " );

	UxPutBorderColor( mn_grism5, WindowBackground );
	UxPutBackground( mn_grism5, WindowBackground );
	UxPutForeground( mn_grism5, TextForeground );
	UxPutFontList( mn_grism5, TextFont );
	UxPutLabelString( mn_grism5, " Grism #5 " );

	UxPutBorderColor( mn_grism6, WindowBackground );
	UxPutBackground( mn_grism6, WindowBackground );
	UxPutForeground( mn_grism6, TextForeground );
	UxPutFontList( mn_grism6, TextFont );
	UxPutLabelString( mn_grism6, " Grism #6 " );

	UxPutBorderColor( mn_grism7, WindowBackground );
	UxPutBackground( mn_grism7, WindowBackground );
	UxPutForeground( mn_grism7, TextForeground );
	UxPutFontList( mn_grism7, TextFont );
	UxPutLabelString( mn_grism7, " Grism #7 " );

	UxPutBorderColor( mn_grism8, WindowBackground );
	UxPutBackground( mn_grism8, WindowBackground );
	UxPutForeground( mn_grism8, TextForeground );
	UxPutFontList( mn_grism8, TextFont );
	UxPutLabelString( mn_grism8, " Grism #8 " );

	UxPutBorderColor( mn_grism9, WindowBackground );
	UxPutBackground( mn_grism9, WindowBackground );
	UxPutForeground( mn_grism9, TextForeground );
	UxPutFontList( mn_grism9, TextFont );
	UxPutLabelString( mn_grism9, " Grism #9 " );

	UxPutLabelString( mn_wgrism, "" );
	UxPutTranslations( mn_wgrism, helpTM );
	UxPutHeight( mn_wgrism, 35 );
	UxPutForeground( mn_wgrism, TextForeground );
	UxPutBorderColor( mn_wgrism, WindowBackground );
	UxPutBackground( mn_wgrism, WindowBackground );
	UxPutY( mn_wgrism, 266 );
	UxPutX( mn_wgrism, 68 );
	UxPutRowColumnType( mn_wgrism, "menu_option" );

	UxPutTranslations( tf_filter, chooseTF );
	UxPutForeground( tf_filter, TextForeground );
	UxPutHighlightOnEnter( tf_filter, "true" );
	UxPutFontList( tf_filter, TextFont );
	UxPutBorderColor( tf_filter, TextBackground );
	UxPutBackground( tf_filter, TextBackground );
	UxPutHeight( tf_filter, 35 );
	UxPutWidth( tf_filter, 190 );
	UxPutY( tf_filter, 310 );
	UxPutX( tf_filter, 232 );

	UxPutY( lbl_ccd, 354 );
	UxPutX( lbl_ccd, 14 );
	UxPutForeground( lbl_ccd, TextForeground );
	UxPutAlignment( lbl_ccd, "alignment_beginning" );
	UxPutLabelString( lbl_ccd, "CCD #00 :" );
	UxPutFontList( lbl_ccd, TextFont );
	UxPutBorderColor( lbl_ccd, LabelBackground );
	UxPutBackground( lbl_ccd, LabelBackground );
	UxPutHeight( lbl_ccd, 30 );
	UxPutWidth( lbl_ccd, 74 );

	UxPutOrientation( list_filt, "horizontal" );
	UxPutIsAligned( list_filt, "true" );
	UxPutEntryAlignment( list_filt, "alignment_beginning" );
	UxPutBorderWidth( list_filt, 0 );
	UxPutShadowThickness( list_filt, 0 );
	UxPutLabelString( list_filt, "" );
	UxPutEntryBorder( list_filt, 0 );
	UxPutBorderColor( list_filt, WindowBackground );
	UxPutBackground( list_filt, WindowBackground );
	UxPutRadioBehavior( list_filt, "true" );
	UxPutHeight( list_filt, 78 );
	UxPutWidth( list_filt, 171 );
	UxPutY( list_filt, 310 );
	UxPutX( list_filt, 57 );

	UxPutTranslations( tg_basic, helpTM );
	UxPutHighlightOnEnter( tg_basic, "true" );
	UxPutForeground( tg_basic, TextForeground );
	UxPutSelectColor( tg_basic, SelectColor );
	UxPutSet( tg_basic, "true" );
	UxPutLabelString( tg_basic, "Basic" );
	UxPutFontList( tg_basic, TextFont );
	UxPutBorderColor( tg_basic, WindowBackground );
	UxPutBackground( tg_basic, WindowBackground );
	UxPutHeight( tg_basic, 28 );
	UxPutWidth( tg_basic, 87 );
	UxPutY( tg_basic, 0 );
	UxPutX( tg_basic, 0 );

	UxPutTranslations( tg_all, helpTM );
	UxPutHighlightOnEnter( tg_all, "true" );
	UxPutSet( tg_all, "false" );
	UxPutForeground( tg_all, TextForeground );
	UxPutSelectColor( tg_all, SelectColor );
	UxPutLabelString( tg_all, "All" );
	UxPutFontList( tg_all, TextFont );
	UxPutBorderColor( tg_all, WindowBackground );
	UxPutBackground( tg_all, WindowBackground );
	UxPutHeight( tg_all, 28 );
	UxPutWidth( tg_all, 53 );
	UxPutY( tg_all, 10 );
	UxPutX( tg_all, 39 );

	UxPutMarginWidth( tg_ccd, 0 );
	UxPutSpacing( tg_ccd, 3 );
	UxPutMarginHeight( tg_ccd, 0 );
	UxPutOrientation( tg_ccd, "horizontal" );
	UxPutIsAligned( tg_ccd, "true" );
	UxPutEntryAlignment( tg_ccd, "alignment_beginning" );
	UxPutBorderWidth( tg_ccd, 0 );
	UxPutShadowThickness( tg_ccd, 0 );
	UxPutLabelString( tg_ccd, "" );
	UxPutEntryBorder( tg_ccd, 0 );
	UxPutBorderColor( tg_ccd, WindowBackground );
	UxPutBackground( tg_ccd, WindowBackground );
	UxPutRadioBehavior( tg_ccd, "true" );
	UxPutHeight( tg_ccd, 78 );
	UxPutWidth( tg_ccd, 171 );
	UxPutY( tg_ccd, 353 );
	UxPutX( tg_ccd, 86 );

	UxPutTranslations( tg_ccdyes, helpTM );
	UxPutHighlightOnEnter( tg_ccdyes, "true" );
	UxPutForeground( tg_ccdyes, TextForeground );
	UxPutSelectColor( tg_ccdyes, SelectColor );
	UxPutSet( tg_ccdyes, "true" );
	UxPutLabelString( tg_ccdyes, "yes" );
	UxPutFontList( tg_ccdyes, TextFont );
	UxPutBorderColor( tg_ccdyes, WindowBackground );
	UxPutBackground( tg_ccdyes, WindowBackground );
	UxPutHeight( tg_ccdyes, 28 );
	UxPutWidth( tg_ccdyes, 87 );
	UxPutY( tg_ccdyes, 0 );
	UxPutX( tg_ccdyes, 0 );

	UxPutTranslations( tg_ccdno, helpTM );
	UxPutHighlightOnEnter( tg_ccdno, "true" );
	UxPutSet( tg_ccdno, "false" );
	UxPutForeground( tg_ccdno, TextForeground );
	UxPutSelectColor( tg_ccdno, SelectColor );
	UxPutLabelString( tg_ccdno, "no" );
	UxPutFontList( tg_ccdno, TextFont );
	UxPutBorderColor( tg_ccdno, WindowBackground );
	UxPutBackground( tg_ccdno, WindowBackground );
	UxPutHeight( tg_ccdno, 28 );
	UxPutWidth( tg_ccdno, 53 );
	UxPutY( tg_ccdno, 3 );
	UxPutX( tg_ccdno, 70 );

	UxPutTranslations( tf_time, text_tab );
	UxPutText( tf_time, "1.00" );
	UxPutForeground( tf_time, TextForeground );
	UxPutHighlightOnEnter( tf_time, "true" );
	UxPutFontList( tf_time, TextFont );
	UxPutBorderColor( tf_time, TextBackground );
	UxPutBackground( tf_time, TextBackground );
	UxPutHeight( tf_time, 35 );
	UxPutWidth( tf_time, 80 );
	UxPutY( tf_time, 394 );
	UxPutX( tf_time, 188 );

	UxPutForeground( lbl_eadu, TextForeground );
	UxPutAlignment( lbl_eadu, "alignment_beginning" );
	UxPutLabelString( lbl_eadu, "e-/ADU :" );
	UxPutFontList( lbl_eadu, TextFont );
	UxPutBorderColor( lbl_eadu, LabelBackground );
	UxPutBackground( lbl_eadu, LabelBackground );
	UxPutHeight( lbl_eadu, 30 );
	UxPutWidth( lbl_eadu, 66 );
	UxPutY( lbl_eadu, 352 );
	UxPutX( lbl_eadu, 274 );

	UxPutTranslations( tf_eadu, text_tab );
	UxPutText( tf_eadu, "1." );
	UxPutForeground( tf_eadu, TextForeground );
	UxPutHighlightOnEnter( tf_eadu, "true" );
	UxPutFontList( tf_eadu, TextFont );
	UxPutBorderColor( tf_eadu, TextBackground );
	UxPutBackground( tf_eadu, TextBackground );
	UxPutHeight( tf_eadu, 35 );
	UxPutWidth( tf_eadu, 80 );
	UxPutY( tf_eadu, 350 );
	UxPutX( tf_eadu, 342 );

	UxPutSpacing( meModel, 10 );
	UxPutMenuAccelerator( meModel, "<KeyUp>F10" );
	UxPutBorderColor( meModel, MenuBackground );
	UxPutBackground( meModel, MenuBackground );
	UxPutRowColumnType( meModel, "menu_bar" );

	UxPutBorderColor( me_m_file, MenuBackground );
	UxPutBackground( me_m_file, MenuBackground );
	UxPutRowColumnType( me_m_file, "menu_pulldown" );

	UxPutMnemonic( me_m_load, "L" );
	UxPutForeground( me_m_load, MenuForeground );
	UxPutFontList( me_m_load, BoldTextFont );
	UxPutBorderColor( me_m_load, MenuBackground );
	UxPutBackground( me_m_load, MenuBackground );
	UxPutLabelString( me_m_load, "Load Config..." );

	UxPutMnemonic( me_m_save, "S" );
	UxPutForeground( me_m_save, MenuForeground );
	UxPutFontList( me_m_save, BoldTextFont );
	UxPutBorderColor( me_m_save, MenuBackground );
	UxPutBackground( me_m_save, MenuBackground );
	UxPutLabelString( me_m_save, "Save Config" );

	UxPutMnemonic( me_m_saveas, "a" );
	UxPutForeground( me_m_saveas, MenuForeground );
	UxPutFontList( me_m_saveas, BoldTextFont );
	UxPutBorderColor( me_m_saveas, MenuBackground );
	UxPutBackground( me_m_saveas, MenuBackground );
	UxPutLabelString( me_m_saveas, "Save Config as..." );

	UxPutForeground( me_m_sep, MenuForeground );
	UxPutBorderColor( me_m_sep, MenuBackground );
	UxPutBackground( me_m_sep, MenuBackground );

	UxPutForeground( me_m_loadcurve, MenuForeground );
	UxPutFontList( me_m_loadcurve, BoldTextFont );
	UxPutBorderColor( me_m_loadcurve, MenuBackground );
	UxPutBackground( me_m_loadcurve, MenuBackground );
	UxPutMnemonic( me_m_loadcurve, "o" );
	UxPutLabelString( me_m_loadcurve, "Load Curve..." );

	UxPutForeground( me_m_savecurve, MenuForeground );
	UxPutFontList( me_m_savecurve, BoldTextFont );
	UxPutBorderColor( me_m_savecurve, MenuBackground );
	UxPutBackground( me_m_savecurve, MenuBackground );
	UxPutMnemonic( me_m_savecurve, "v" );
	UxPutLabelString( me_m_savecurve, "Save Curve" );

	UxPutForeground( me_m_saveascurve, MenuForeground );
	UxPutFontList( me_m_saveascurve, BoldTextFont );
	UxPutBorderColor( me_m_saveascurve, MenuBackground );
	UxPutBackground( me_m_saveascurve, MenuBackground );
	UxPutMnemonic( me_m_saveascurve, "s" );
	UxPutLabelString( me_m_saveascurve, "Save Curve as ..." );

	UxPutForeground( mt_m_file, MenuForeground );
	UxPutFontList( mt_m_file, BoldTextFont );
	UxPutBorderColor( mt_m_file, MenuBackground );
	UxPutBackground( mt_m_file, MenuBackground );
	UxPutMnemonic( mt_m_file, "F" );
	UxPutLabelString( mt_m_file, "File" );

	UxPutBorderColor( me_m_frame, MenuBackground );
	UxPutBackground( me_m_frame, MenuBackground );
	UxPutRowColumnType( me_m_frame, "menu_pulldown" );

	UxPutBorderColor( me_m_trimx, MenuBackground );
	UxPutBackground( me_m_trimx, MenuBackground );
	UxPutForeground( me_m_trimx, MenuForeground );
	UxPutFontList( me_m_trimx, BoldTextFont );
	UxPutMnemonic( me_m_trimx, "X" );
	UxPutLabelString( me_m_trimx, "Cut X" );

	UxPutBorderColor( me_m_trimy, MenuBackground );
	UxPutBackground( me_m_trimy, MenuBackground );
	UxPutForeground( me_m_trimy, MenuForeground );
	UxPutFontList( me_m_trimy, BoldTextFont );
	UxPutMnemonic( me_m_trimy, "Y" );
	UxPutLabelString( me_m_trimy, "Cut Y" );

	UxPutBorderColor( me_m_unzoom, MenuBackground );
	UxPutBackground( me_m_unzoom, MenuBackground );
	UxPutForeground( me_m_unzoom, MenuForeground );
	UxPutFontList( me_m_unzoom, BoldTextFont );
	UxPutMnemonic( me_m_unzoom, "U" );
	UxPutLabelString( me_m_unzoom, "Unzoom" );

	UxPutBorderColor( me_m_redraw, MenuBackground );
	UxPutBackground( me_m_redraw, MenuBackground );
	UxPutForeground( me_m_redraw, MenuForeground );
	UxPutFontList( me_m_redraw, BoldTextFont );
	UxPutMnemonic( me_m_redraw, "R" );
	UxPutLabelString( me_m_redraw, "Redraw" );

	UxPutBorderColor( me_m_print, MenuBackground );
	UxPutBackground( me_m_print, MenuBackground );
	UxPutForeground( me_m_print, MenuForeground );
	UxPutFontList( me_m_print, BoldTextFont );
	UxPutMnemonic( me_m_print, "P" );
	UxPutLabelString( me_m_print, "Print" );

	UxPutFontList( mt_m_frame, BoldTextFont );
	UxPutForeground( mt_m_frame, MenuForeground );
	UxPutBorderColor( mt_m_frame, MenuBackground );
	UxPutBackground( mt_m_frame, MenuBackground );
	UxPutMnemonic( mt_m_frame, "a" );
	UxPutLabelString( mt_m_frame, "Frame" );

	UxPutRowColumnType( me_m_option, "menu_pulldown" );

	UxPutRadioBehavior( me_m_plotoption, "true" );
	UxPutForeground( me_m_plotoption, MenuForeground );
	UxPutBorderColor( me_m_plotoption, MenuBackground );
	UxPutBackground( me_m_plotoption, MenuBackground );
	UxPutRowColumnType( me_m_plotoption, "menu_pulldown" );

	UxPutSet( me_m_plot, "true" );
	UxPutSelectColor( me_m_plot, SelectColor );
	UxPutFontList( me_m_plot, BoldTextFont );
	UxPutLabelString( me_m_plot, "PLot one item" );

	UxPutSelectColor( me_m_oplot, SelectColor );
	UxPutFontList( me_m_oplot, BoldTextFont );
	UxPutLabelString( me_m_oplot, "Overplot items" );

	UxPutForeground( me_m_pmode, MenuForeground );
	UxPutFontList( me_m_pmode, BoldTextFont );
	UxPutBorderColor( me_m_pmode, MenuBackground );
	UxPutBackground( me_m_pmode, MenuBackground );
	UxPutMnemonic( me_m_pmode, "P" );
	UxPutLabelString( me_m_pmode, "Plot mode" );

	UxPutRadioBehavior( me_m_traceoption, "true" );
	UxPutForeground( me_m_traceoption, MenuForeground );
	UxPutBorderColor( me_m_traceoption, MenuBackground );
	UxPutBackground( me_m_traceoption, MenuBackground );
	UxPutRowColumnType( me_m_traceoption, "menu_pulldown" );

	UxPutSet( me_m_line, "true" );
	UxPutSelectColor( me_m_line, SelectColor );
	UxPutFontList( me_m_line, BoldTextFont );
	UxPutMnemonic( me_m_line, "S" );
	UxPutLabelString( me_m_line, "Straight Line" );

	UxPutSelectColor( me_m_histo, SelectColor );
	UxPutFontList( me_m_histo, BoldTextFont );
	UxPutMnemonic( me_m_histo, "H" );
	UxPutLabelString( me_m_histo, "Histogram line" );

	UxPutForeground( me_m_tmode, MenuForeground );
	UxPutFontList( me_m_tmode, BoldTextFont );
	UxPutBorderColor( me_m_tmode, MenuBackground );
	UxPutBackground( me_m_tmode, MenuBackground );
	UxPutMnemonic( me_m_tmode, "T" );
	UxPutLabelString( me_m_tmode, "Trace mode" );

	UxPutForeground( met_m_options, MenuForeground );
	UxPutFontList( met_m_options, BoldTextFont );
	UxPutBorderColor( met_m_options, MenuBackground );
	UxPutBackground( met_m_options, MenuBackground );
	UxPutMnemonic( met_m_options, "O" );
	UxPutLabelString( met_m_options, "Option" );

	UxPutBorderColor( me_m_help, MenuBackground );
	UxPutBackground( me_m_help, MenuBackground );
	UxPutRowColumnType( me_m_help, "menu_pulldown" );

	UxPutForeground( me_m_hm, MenuForeground );
	UxPutFontList( me_m_hm, BoldTextFont );
	UxPutBorderColor( me_m_hm, MenuBackground );
	UxPutBackground( me_m_hm, MenuBackground );
	UxPutMnemonic( me_m_hm, "M" );
	UxPutLabelString( me_m_hm, "On Transmission Model ..." );

	UxPutForeground( mt_m_help, MenuForeground );
	UxPutFontList( mt_m_help, BoldTextFont );
	UxPutBorderColor( mt_m_help, MenuBackground );
	UxPutBackground( mt_m_help, MenuBackground );
	UxPutMnemonic( mt_m_help, "H" );
	UxPutLabelString( mt_m_help, "Help" );

	UxPutBorderColor( me_m_quit, MenuBackground );
	UxPutBackground( me_m_quit, MenuBackground );
	UxPutRowColumnType( me_m_quit, "menu_pulldown" );

	UxPutForeground( me_m_bye, MenuForeground );
	UxPutFontList( me_m_bye, BoldTextFont );
	UxPutBorderColor( me_m_bye, MenuBackground );
	UxPutBackground( me_m_bye, MenuBackground );
	UxPutMnemonic( me_m_bye, "B" );
	UxPutLabelString( me_m_bye, "Bye" );

	UxPutForeground( mt_m_quit, MenuForeground );
	UxPutFontList( mt_m_quit, BoldTextFont );
	UxPutBorderColor( mt_m_quit, MenuBackground );
	UxPutBackground( mt_m_quit, MenuBackground );
	UxPutMnemonic( mt_m_quit, "Q" );
	UxPutLabelString( mt_m_quit, "Quit" );

	UxPutBorderColor( separator2, WindowBackground );
	UxPutBackground( separator2, WindowBackground );
	UxPutHeight( separator2, 5 );
	UxPutWidth( separator2, 492 );
	UxPutY( separator2, 174 );
	UxPutX( separator2, 0 );

	UxPutBorderColor( separator3, WindowBackground );
	UxPutBackground( separator3, WindowBackground );
	UxPutHeight( separator3, 5 );
	UxPutWidth( separator3, 492 );
	UxPutY( separator3, 388 );
	UxPutX( separator3, 0 );

	UxPutBorderColor( separator7, WindowBackground );
	UxPutBackground( separator7, WindowBackground );
	UxPutHeight( separator7, 5 );
	UxPutWidth( separator7, 492 );
	UxPutY( separator7, 224 );
	UxPutX( separator7, 0 );

	UxPutRecomputeSize( lbl_title, "false" );
	UxPutForeground( lbl_title, TextForeground );
	UxPutAlignment( lbl_title, "alignment_center" );
	UxPutLabelString( lbl_title, "Transmission Model" );
	UxPutFontList( lbl_title, BoldTextFont );
	UxPutBorderColor( lbl_title, LabelBackground );
	UxPutBackground( lbl_title, LabelBackground );
	UxPutHeight( lbl_title, 28 );
	UxPutWidth( lbl_title, 438 );
	UxPutY( lbl_title, 34 );
	UxPutX( lbl_title, 1 );

	UxPutTranslations( tf_SN, text_tab );
	UxPutText( tf_SN, "1.00" );
	UxPutForeground( tf_SN, TextForeground );
	UxPutHighlightOnEnter( tf_SN, "true" );
	UxPutFontList( tf_SN, TextFont );
	UxPutBorderColor( tf_SN, TextBackground );
	UxPutBackground( tf_SN, TextBackground );
	UxPutHeight( tf_SN, 35 );
	UxPutWidth( tf_SN, 80 );
	UxPutY( tf_SN, 432 );
	UxPutX( tf_SN, 188 );

	UxPutOrientation( tg_time_sn, "vertical" );
	UxPutIsAligned( tg_time_sn, "true" );
	UxPutEntryAlignment( tg_time_sn, "alignment_beginning" );
	UxPutBorderWidth( tg_time_sn, 0 );
	UxPutShadowThickness( tg_time_sn, 0 );
	UxPutLabelString( tg_time_sn, "" );
	UxPutEntryBorder( tg_time_sn, 0 );
	UxPutBorderColor( tg_time_sn, WindowBackground );
	UxPutBackground( tg_time_sn, WindowBackground );
	UxPutRadioBehavior( tg_time_sn, "true" );
	UxPutHeight( tg_time_sn, 78 );
	UxPutWidth( tg_time_sn, 171 );
	UxPutY( tg_time_sn, 400 );
	UxPutX( tg_time_sn, 9 );

	UxPutTranslations( tg_etime, helpTM );
	UxPutHighlightOnEnter( tg_etime, "true" );
	UxPutForeground( tg_etime, TextForeground );
	UxPutSelectColor( tg_etime, SelectColor );
	UxPutSet( tg_etime, "true" );
	UxPutLabelString( tg_etime, "Exposure time (sec)" );
	UxPutFontList( tg_etime, TextFont );
	UxPutBorderColor( tg_etime, WindowBackground );
	UxPutBackground( tg_etime, WindowBackground );
	UxPutHeight( tg_etime, 28 );
	UxPutWidth( tg_etime, 87 );
	UxPutY( tg_etime, 0 );
	UxPutX( tg_etime, 0 );

	UxPutTranslations( tg_sn, helpTM );
	UxPutHighlightOnEnter( tg_sn, "true" );
	UxPutSet( tg_sn, "false" );
	UxPutForeground( tg_sn, TextForeground );
	UxPutSelectColor( tg_sn, SelectColor );
	UxPutLabelString( tg_sn, "Signal to Noise Ratio" );
	UxPutFontList( tg_sn, TextFont );
	UxPutBorderColor( tg_sn, WindowBackground );
	UxPutBackground( tg_sn, WindowBackground );
	UxPutHeight( tg_sn, 28 );
	UxPutWidth( tg_sn, 53 );
	UxPutY( tg_sn, 3 );
	UxPutX( tg_sn, 70 );

	UxPutForeground( lbl_sky, TextForeground );
	UxPutAlignment( lbl_sky, "alignment_beginning" );
	UxPutLabelString( lbl_sky, "Sky :" );
	UxPutFontList( lbl_sky, TextFont );
	UxPutBorderColor( lbl_sky, LabelBackground );
	UxPutBackground( lbl_sky, LabelBackground );
	UxPutHeight( lbl_sky, 25 );
	UxPutWidth( lbl_sky, 44 );
	UxPutY( lbl_sky, 112 );
	UxPutX( lbl_sky, 13 );

	UxPutSpacing( tg_brightness, 0 );
	UxPutMarginWidth( tg_brightness, 2 );
	UxPutMarginHeight( tg_brightness, 0 );
	UxPutOrientation( tg_brightness, "vertical" );
	UxPutIsAligned( tg_brightness, "true" );
	UxPutEntryAlignment( tg_brightness, "alignment_beginning" );
	UxPutBorderWidth( tg_brightness, 0 );
	UxPutShadowThickness( tg_brightness, 0 );
	UxPutLabelString( tg_brightness, "" );
	UxPutEntryBorder( tg_brightness, 0 );
	UxPutBorderColor( tg_brightness, WindowBackground );
	UxPutBackground( tg_brightness, WindowBackground );
	UxPutRadioBehavior( tg_brightness, "true" );
	UxPutHeight( tg_brightness, 78 );
	UxPutWidth( tg_brightness, 171 );
	UxPutY( tg_brightness, 112 );
	UxPutX( tg_brightness, 152 );

	UxPutTranslations( tg_bright, helpTM );
	UxPutHighlightOnEnter( tg_bright, "true" );
	UxPutForeground( tg_bright, TextForeground );
	UxPutSelectColor( tg_bright, SelectColor );
	UxPutSet( tg_bright, "true" );
	UxPutLabelString( tg_bright, "Bright" );
	UxPutFontList( tg_bright, TextFont );
	UxPutBorderColor( tg_bright, WindowBackground );
	UxPutBackground( tg_bright, WindowBackground );
	UxPutHeight( tg_bright, 28 );
	UxPutWidth( tg_bright, 87 );
	UxPutY( tg_bright, 12 );
	UxPutX( tg_bright, 2 );

	UxPutTranslations( tg_dark, helpTM );
	UxPutHighlightOnEnter( tg_dark, "true" );
	UxPutSet( tg_dark, "false" );
	UxPutForeground( tg_dark, TextForeground );
	UxPutSelectColor( tg_dark, SelectColor );
	UxPutLabelString( tg_dark, "Dark" );
	UxPutFontList( tg_dark, TextFont );
	UxPutBorderColor( tg_dark, WindowBackground );
	UxPutBackground( tg_dark, WindowBackground );
	UxPutHeight( tg_dark, 28 );
	UxPutWidth( tg_dark, 53 );
	UxPutY( tg_dark, 28 );
	UxPutX( tg_dark, 0 );

	UxPutSpacing( tg_lines, 0 );
	UxPutMarginWidth( tg_lines, 2 );
	UxPutMarginHeight( tg_lines, 0 );
	UxPutOrientation( tg_lines, "vertical" );
	UxPutIsAligned( tg_lines, "true" );
	UxPutEntryAlignment( tg_lines, "alignment_beginning" );
	UxPutBorderWidth( tg_lines, 0 );
	UxPutShadowThickness( tg_lines, 0 );
	UxPutLabelString( tg_lines, "" );
	UxPutEntryBorder( tg_lines, 0 );
	UxPutBorderColor( tg_lines, WindowBackground );
	UxPutBackground( tg_lines, WindowBackground );
	UxPutRadioBehavior( tg_lines, "true" );
	UxPutHeight( tg_lines, 78 );
	UxPutWidth( tg_lines, 171 );
	UxPutY( tg_lines, 112 );
	UxPutX( tg_lines, 240 );

	UxPutTranslations( tg_emli, helpTM );
	UxPutHighlightOnEnter( tg_emli, "true" );
	UxPutForeground( tg_emli, TextForeground );
	UxPutSelectColor( tg_emli, SelectColor );
	UxPutSet( tg_emli, "true" );
	UxPutLabelString( tg_emli, "With Emission Lines" );
	UxPutFontList( tg_emli, TextFont );
	UxPutBorderColor( tg_emli, WindowBackground );
	UxPutBackground( tg_emli, WindowBackground );
	UxPutHeight( tg_emli, 28 );
	UxPutWidth( tg_emli, 87 );
	UxPutY( tg_emli, 12 );
	UxPutX( tg_emli, 2 );

	UxPutTranslations( tg_noemli, helpTM );
	UxPutHighlightOnEnter( tg_noemli, "true" );
	UxPutSet( tg_noemli, "false" );
	UxPutForeground( tg_noemli, TextForeground );
	UxPutSelectColor( tg_noemli, SelectColor );
	UxPutLabelString( tg_noemli, "Without Emision Lines" );
	UxPutFontList( tg_noemli, TextFont );
	UxPutBorderColor( tg_noemli, WindowBackground );
	UxPutBackground( tg_noemli, WindowBackground );
	UxPutHeight( tg_noemli, 28 );
	UxPutWidth( tg_noemli, 53 );
	UxPutY( tg_noemli, 3 );
	UxPutX( tg_noemli, 70 );

	UxPutBorderColor( separator18, WindowBackground );
	UxPutBackground( separator18, WindowBackground );
	UxPutHeight( separator18, 5 );
	UxPutWidth( separator18, 492 );
	UxPutY( separator18, 75 );
	UxPutX( separator18, 0 );

	UxPutSpacing( tg_sky, 0 );
	UxPutMarginWidth( tg_sky, 2 );
	UxPutMarginHeight( tg_sky, 0 );
	UxPutOrientation( tg_sky, "vertical" );
	UxPutIsAligned( tg_sky, "true" );
	UxPutEntryAlignment( tg_sky, "alignment_beginning" );
	UxPutBorderWidth( tg_sky, 0 );
	UxPutShadowThickness( tg_sky, 0 );
	UxPutLabelString( tg_sky, "" );
	UxPutEntryBorder( tg_sky, 0 );
	UxPutBorderColor( tg_sky, WindowBackground );
	UxPutBackground( tg_sky, WindowBackground );
	UxPutRadioBehavior( tg_sky, "true" );
	UxPutHeight( tg_sky, 78 );
	UxPutWidth( tg_sky, 171 );
	UxPutY( tg_sky, 112 );
	UxPutX( tg_sky, 76 );

	UxPutTranslations( tg_skyon, helpTM );
	UxPutHighlightOnEnter( tg_skyon, "true" );
	UxPutForeground( tg_skyon, TextForeground );
	UxPutSelectColor( tg_skyon, SelectColor );
	UxPutSet( tg_skyon, "true" );
	UxPutLabelString( tg_skyon, "yes" );
	UxPutFontList( tg_skyon, TextFont );
	UxPutBorderColor( tg_skyon, WindowBackground );
	UxPutBackground( tg_skyon, WindowBackground );
	UxPutHeight( tg_skyon, 28 );
	UxPutWidth( tg_skyon, 87 );
	UxPutY( tg_skyon, 126 );
	UxPutX( tg_skyon, 2 );

	UxPutTranslations( tg_skyoff, helpTM );
	UxPutHighlightOnEnter( tg_skyoff, "true" );
	UxPutSet( tg_skyoff, "false" );
	UxPutForeground( tg_skyoff, TextForeground );
	UxPutSelectColor( tg_skyoff, SelectColor );
	UxPutLabelString( tg_skyoff, "no" );
	UxPutFontList( tg_skyoff, TextFont );
	UxPutBorderColor( tg_skyoff, WindowBackground );
	UxPutBackground( tg_skyoff, WindowBackground );
	UxPutHeight( tg_skyoff, 28 );
	UxPutWidth( tg_skyoff, 53 );
	UxPutY( tg_skyoff, 126 );
	UxPutX( tg_skyoff, 2 );

	UxPutTranslations( tg_extsky, helpTM );
	UxPutHighlightOnEnter( tg_extsky, "true" );
	UxPutForeground( tg_extsky, TextForeground );
	UxPutSelectColor( tg_extsky, SelectColor );
	UxPutSet( tg_extsky, "true" );
	UxPutLabelString( tg_extsky, "Extract Sky" );
	UxPutFontList( tg_extsky, TextFont );
	UxPutBorderColor( tg_extsky, WindowBackground );
	UxPutBackground( tg_extsky, WindowBackground );
	UxPutHeight( tg_extsky, 26 );
	UxPutWidth( tg_extsky, 104 );
	UxPutY( tg_extsky, 418 );
	UxPutX( tg_extsky, 314 );

	UxPutForeground( lbl_mirror1, TextForeground );
	UxPutAlignment( lbl_mirror1, "alignment_beginning" );
	UxPutLabelString( lbl_mirror1, "Optics :" );
	UxPutFontList( lbl_mirror1, TextFont );
	UxPutBorderColor( lbl_mirror1, LabelBackground );
	UxPutBackground( lbl_mirror1, LabelBackground );
	UxPutHeight( lbl_mirror1, 30 );
	UxPutWidth( lbl_mirror1, 60 );
	UxPutY( lbl_mirror1, 230 );
	UxPutX( lbl_mirror1, 262 );

	UxPutMarginWidth( tg_opt, 0 );
	UxPutSpacing( tg_opt, 3 );
	UxPutMarginHeight( tg_opt, 0 );
	UxPutOrientation( tg_opt, "horizontal" );
	UxPutIsAligned( tg_opt, "true" );
	UxPutEntryAlignment( tg_opt, "alignment_beginning" );
	UxPutBorderWidth( tg_opt, 0 );
	UxPutShadowThickness( tg_opt, 0 );
	UxPutLabelString( tg_opt, "" );
	UxPutEntryBorder( tg_opt, 0 );
	UxPutBorderColor( tg_opt, WindowBackground );
	UxPutBackground( tg_opt, WindowBackground );
	UxPutRadioBehavior( tg_opt, "true" );
	UxPutHeight( tg_opt, 78 );
	UxPutWidth( tg_opt, 171 );
	UxPutY( tg_opt, 230 );
	UxPutX( tg_opt, 318 );

	UxPutTranslations( tg_optyes, helpTM );
	UxPutHighlightOnEnter( tg_optyes, "true" );
	UxPutForeground( tg_optyes, TextForeground );
	UxPutSelectColor( tg_optyes, SelectColor );
	UxPutSet( tg_optyes, "true" );
	UxPutLabelString( tg_optyes, "yes" );
	UxPutFontList( tg_optyes, TextFont );
	UxPutBorderColor( tg_optyes, WindowBackground );
	UxPutBackground( tg_optyes, WindowBackground );
	UxPutHeight( tg_optyes, 28 );
	UxPutWidth( tg_optyes, 87 );
	UxPutY( tg_optyes, 0 );
	UxPutX( tg_optyes, 0 );

	UxPutTranslations( tg_optno, helpTM );
	UxPutHighlightOnEnter( tg_optno, "true" );
	UxPutSet( tg_optno, "false" );
	UxPutForeground( tg_optno, TextForeground );
	UxPutSelectColor( tg_optno, SelectColor );
	UxPutLabelString( tg_optno, "no" );
	UxPutFontList( tg_optno, TextFont );
	UxPutBorderColor( tg_optno, WindowBackground );
	UxPutBackground( tg_optno, WindowBackground );
	UxPutHeight( tg_optno, 28 );
	UxPutWidth( tg_optno, 53 );
	UxPutY( tg_optno, 3 );
	UxPutX( tg_optno, 70 );

	UxPutMarginWidth( tg_mir, 0 );
	UxPutSpacing( tg_mir, 3 );
	UxPutMarginHeight( tg_mir, 0 );
	UxPutOrientation( tg_mir, "horizontal" );
	UxPutIsAligned( tg_mir, "true" );
	UxPutEntryAlignment( tg_mir, "alignment_beginning" );
	UxPutBorderWidth( tg_mir, 0 );
	UxPutShadowThickness( tg_mir, 0 );
	UxPutLabelString( tg_mir, "" );
	UxPutEntryBorder( tg_mir, 0 );
	UxPutBorderColor( tg_mir, WindowBackground );
	UxPutBackground( tg_mir, WindowBackground );
	UxPutRadioBehavior( tg_mir, "true" );
	UxPutHeight( tg_mir, 78 );
	UxPutWidth( tg_mir, 171 );
	UxPutY( tg_mir, 231 );
	UxPutX( tg_mir, 80 );

	UxPutTranslations( tg_mirnew, helpTM );
	UxPutHighlightOnEnter( tg_mirnew, "true" );
	UxPutForeground( tg_mirnew, TextForeground );
	UxPutSelectColor( tg_mirnew, SelectColor );
	UxPutSet( tg_mirnew, "true" );
	UxPutLabelString( tg_mirnew, "new" );
	UxPutFontList( tg_mirnew, TextFont );
	UxPutBorderColor( tg_mirnew, WindowBackground );
	UxPutBackground( tg_mirnew, WindowBackground );
	UxPutHeight( tg_mirnew, 28 );
	UxPutWidth( tg_mirnew, 87 );
	UxPutY( tg_mirnew, 0 );
	UxPutX( tg_mirnew, 0 );

	UxPutTranslations( tg_mirold, helpTM );
	UxPutHighlightOnEnter( tg_mirold, "true" );
	UxPutSet( tg_mirold, "false" );
	UxPutForeground( tg_mirold, TextForeground );
	UxPutSelectColor( tg_mirold, SelectColor );
	UxPutLabelString( tg_mirold, "old" );
	UxPutFontList( tg_mirold, TextFont );
	UxPutBorderColor( tg_mirold, WindowBackground );
	UxPutBackground( tg_mirold, WindowBackground );
	UxPutHeight( tg_mirold, 28 );
	UxPutWidth( tg_mirold, 53 );
	UxPutY( tg_mirold, 0 );
	UxPutX( tg_mirold, 46 );

	UxPutTranslations( tg_mirno, helpTM );
	UxPutHighlightOnEnter( tg_mirno, "true" );
	UxPutSet( tg_mirno, "false" );
	UxPutForeground( tg_mirno, TextForeground );
	UxPutSelectColor( tg_mirno, SelectColor );
	UxPutLabelString( tg_mirno, "no" );
	UxPutFontList( tg_mirno, TextFont );
	UxPutBorderColor( tg_mirno, WindowBackground );
	UxPutBackground( tg_mirno, WindowBackground );
	UxPutHeight( tg_mirno, 28 );
	UxPutWidth( tg_mirno, 325 );
	UxPutY( tg_mirno, 0 );
	UxPutX( tg_mirno, -160 );

	UxPutTranslations( tf_disp_pix, text_tab );
	UxPutText( tf_disp_pix, "1." );
	UxPutForeground( tf_disp_pix, TextForeground );
	UxPutHighlightOnEnter( tf_disp_pix, "true" );
	UxPutFontList( tf_disp_pix, TextFont );
	UxPutBorderColor( tf_disp_pix, TextBackground );
	UxPutBackground( tf_disp_pix, TextBackground );
	UxPutHeight( tf_disp_pix, 35 );
	UxPutWidth( tf_disp_pix, 80 );
	UxPutY( tf_disp_pix, 266 );
	UxPutX( tf_disp_pix, 342 );

	UxPutForeground( lbl_disp, TextForeground );
	UxPutAlignment( lbl_disp, "alignment_beginning" );
	UxPutLabelString( lbl_disp, "nm/pix :" );
	UxPutFontList( lbl_disp, TextFont );
	UxPutBorderColor( lbl_disp, LabelBackground );
	UxPutBackground( lbl_disp, LabelBackground );
	UxPutHeight( lbl_disp, 30 );
	UxPutWidth( lbl_disp, 60 );
	UxPutY( lbl_disp, 268 );
	UxPutX( lbl_disp, 280 );

	UxPutAlignment( label7, "alignment_beginning" );
	UxPutLabelString( label7, "Wavelength Range (nm) :" );
	UxPutForeground( label7, TextForeground );
	UxPutFontList( label7, TextFont );
	UxPutBorderColor( label7, WindowBackground );
	UxPutBackground( label7, WindowBackground );
	UxPutHeight( label7, 23 );
	UxPutWidth( label7, 165 );
	UxPutY( label7, 480 );
	UxPutX( label7, 16 );

	UxPutTranslations( tf_TWmax, text_tab );
	UxPutText( tf_TWmax, "900." );
	UxPutForeground( tf_TWmax, TextForeground );
	UxPutHighlightOnEnter( tf_TWmax, "true" );
	UxPutFontList( tf_TWmax, TextFont );
	UxPutBorderColor( tf_TWmax, TextBackground );
	UxPutBackground( tf_TWmax, TextBackground );
	UxPutHeight( tf_TWmax, 35 );
	UxPutWidth( tf_TWmax, 80 );
	UxPutY( tf_TWmax, 474 );
	UxPutX( tf_TWmax, 274 );

	UxPutTranslations( tf_TWmin, text_tab );
	UxPutText( tf_TWmin, "300." );
	UxPutForeground( tf_TWmin, TextForeground );
	UxPutHighlightOnEnter( tf_TWmin, "true" );
	UxPutFontList( tf_TWmin, TextFont );
	UxPutBorderColor( tf_TWmin, TextBackground );
	UxPutBackground( tf_TWmin, TextBackground );
	UxPutHeight( tf_TWmin, 35 );
	UxPutWidth( tf_TWmin, 80 );
	UxPutY( tf_TWmin, 474 );
	UxPutX( tf_TWmin, 188 );

	UxPutBorderColor( separator10, WindowBackground );
	UxPutBackground( separator10, WindowBackground );
	UxPutHeight( separator10, 5 );
	UxPutWidth( separator10, 492 );
	UxPutY( separator10, 468 );
	UxPutX( separator10, 0 );

	UxPutOrientation( separator4, "vertical" );
	UxPutBorderColor( separator4, WindowBackground );
	UxPutBackground( separator4, WindowBackground );
	UxPutHeight( separator4, 74 );
	UxPutWidth( separator4, 8 );
	UxPutY( separator4, 394 );
	UxPutX( separator4, 296 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ModelShell()
{
	/* Create the swidgets */

	ModelShell = UxCreateApplicationShell( "ModelShell", NO_PARENT );
	UxPutContext( ModelShell, UxModelShellContext );

	form4 = UxCreateForm( "form4", ModelShell );
	lbl_spec = UxCreateLabel( "lbl_spec", form4 );
	tf_spectrum = UxCreateText( "tf_spectrum", form4 );
	lbl_atm = UxCreateLabel( "lbl_atm", form4 );
	lbl_airmass = UxCreateLabel( "lbl_airmass", form4 );
	form6 = UxCreateForm( "form6", form4 );
	pB_MReset = UxCreatePushButton( "pB_MReset", form6 );
	pB_Mtrans = UxCreatePushButton( "pB_Mtrans", form6 );
	pB_Mres = UxCreatePushButton( "pB_Mres", form6 );
	pB_MFCreate = UxCreatePushButton( "pB_MFCreate", form6 );
	pB_MPrint = UxCreatePushButton( "pB_MPrint", form6 );
	atm_mod = UxCreateRowColumn( "atm_mod", form4 );
	tg_atmyes = UxCreateToggleButton( "tg_atmyes", atm_mod );
	tg_atmno = UxCreateToggleButton( "tg_atmno", atm_mod );
	helptextModel = UxCreateText( "helptextModel", form4 );
	separator8 = UxCreateSeparator( "separator8", form4 );
	menu_spec = UxCreateRowColumn( "menu_spec", form4 );
	mn_standard = UxCreatePushButton( "mn_standard", menu_spec );
	mn_hststd = UxCreatePushButton( "mn_hststd", menu_spec );
	mn_ctiostd = UxCreatePushButton( "mn_ctiostd", menu_spec );
	mn_blackbody = UxCreatePushButton( "mn_blackbody", menu_spec );
	mn_spec = UxCreateRowColumn( "mn_spec", form4 );
	tf_airmass = UxCreateText( "tf_airmass", form4 );
	lbl_mirror = UxCreateLabel( "lbl_mirror", form4 );
	lbl_grism = UxCreateLabel( "lbl_grism", form4 );
	lbl_filter = UxCreateLabel( "lbl_filter", form4 );
	me_10grism = UxCreateRowColumn( "me_10grism", form4 );
	mn_nogrism = UxCreatePushButton( "mn_nogrism", me_10grism );
	mn_grism0 = UxCreatePushButton( "mn_grism0", me_10grism );
	mn_grism1 = UxCreatePushButton( "mn_grism1", me_10grism );
	mn_grism2 = UxCreatePushButton( "mn_grism2", me_10grism );
	mn_grism3 = UxCreatePushButton( "mn_grism3", me_10grism );
	mn_grism4 = UxCreatePushButton( "mn_grism4", me_10grism );
	mn_grism5 = UxCreatePushButton( "mn_grism5", me_10grism );
	mn_grism6 = UxCreatePushButton( "mn_grism6", me_10grism );
	mn_grism7 = UxCreatePushButton( "mn_grism7", me_10grism );
	mn_grism8 = UxCreatePushButton( "mn_grism8", me_10grism );
	mn_grism9 = UxCreatePushButton( "mn_grism9", me_10grism );
	mn_wgrism = UxCreateRowColumn( "mn_wgrism", form4 );
	tf_filter = UxCreateText( "tf_filter", form4 );
	lbl_ccd = UxCreateLabel( "lbl_ccd", form4 );
	list_filt = UxCreateRowColumn( "list_filt", form4 );
	tg_basic = UxCreateToggleButton( "tg_basic", list_filt );
	tg_all = UxCreateToggleButton( "tg_all", list_filt );
	tg_ccd = UxCreateRowColumn( "tg_ccd", form4 );
	tg_ccdyes = UxCreateToggleButton( "tg_ccdyes", tg_ccd );
	tg_ccdno = UxCreateToggleButton( "tg_ccdno", tg_ccd );
	tf_time = UxCreateText( "tf_time", form4 );
	lbl_eadu = UxCreateLabel( "lbl_eadu", form4 );
	tf_eadu = UxCreateText( "tf_eadu", form4 );
	meModel = UxCreateRowColumn( "meModel", form4 );
	me_m_file = UxCreateRowColumn( "me_m_file", meModel );
	me_m_load = UxCreatePushButton( "me_m_load", me_m_file );
	me_m_save = UxCreatePushButton( "me_m_save", me_m_file );
	me_m_saveas = UxCreatePushButton( "me_m_saveas", me_m_file );
	me_m_sep = UxCreateSeparator( "me_m_sep", me_m_file );
	me_m_loadcurve = UxCreatePushButton( "me_m_loadcurve", me_m_file );
	me_m_savecurve = UxCreatePushButton( "me_m_savecurve", me_m_file );
	me_m_saveascurve = UxCreatePushButton( "me_m_saveascurve", me_m_file );
	mt_m_file = UxCreateCascadeButton( "mt_m_file", meModel );
	me_m_frame = UxCreateRowColumn( "me_m_frame", meModel );
	me_m_trimx = UxCreatePushButton( "me_m_trimx", me_m_frame );
	me_m_trimy = UxCreatePushButton( "me_m_trimy", me_m_frame );
	me_m_unzoom = UxCreatePushButton( "me_m_unzoom", me_m_frame );
	me_m_redraw = UxCreatePushButton( "me_m_redraw", me_m_frame );
	me_m_print = UxCreatePushButton( "me_m_print", me_m_frame );
	mt_m_frame = UxCreateCascadeButton( "mt_m_frame", meModel );
	me_m_option = UxCreateRowColumn( "me_m_option", meModel );
	me_m_plotoption = UxCreateRowColumn( "me_m_plotoption", me_m_option );
	me_m_plot = UxCreateToggleButtonGadget( "me_m_plot", me_m_plotoption );
	me_m_oplot = UxCreateToggleButtonGadget( "me_m_oplot", me_m_plotoption );
	me_m_pmode = UxCreateCascadeButton( "me_m_pmode", me_m_option );
	me_m_traceoption = UxCreateRowColumn( "me_m_traceoption", me_m_option );
	me_m_line = UxCreateToggleButtonGadget( "me_m_line", me_m_traceoption );
	me_m_histo = UxCreateToggleButtonGadget( "me_m_histo", me_m_traceoption );
	me_m_tmode = UxCreateCascadeButton( "me_m_tmode", me_m_option );
	met_m_options = UxCreateCascadeButton( "met_m_options", meModel );
	me_m_help = UxCreateRowColumn( "me_m_help", meModel );
	me_m_hm = UxCreatePushButton( "me_m_hm", me_m_help );
	mt_m_help = UxCreateCascadeButton( "mt_m_help", meModel );
	me_m_quit = UxCreateRowColumn( "me_m_quit", meModel );
	me_m_bye = UxCreatePushButton( "me_m_bye", me_m_quit );
	mt_m_quit = UxCreateCascadeButton( "mt_m_quit", meModel );
	separator2 = UxCreateSeparator( "separator2", form4 );
	separator3 = UxCreateSeparator( "separator3", form4 );
	separator7 = UxCreateSeparator( "separator7", form4 );
	lbl_title = UxCreateLabel( "lbl_title", form4 );
	tf_SN = UxCreateText( "tf_SN", form4 );
	tg_time_sn = UxCreateRowColumn( "tg_time_sn", form4 );
	tg_etime = UxCreateToggleButton( "tg_etime", tg_time_sn );
	tg_sn = UxCreateToggleButton( "tg_sn", tg_time_sn );
	lbl_sky = UxCreateLabel( "lbl_sky", form4 );
	tg_brightness = UxCreateRowColumn( "tg_brightness", form4 );
	tg_bright = UxCreateToggleButton( "tg_bright", tg_brightness );
	tg_dark = UxCreateToggleButton( "tg_dark", tg_brightness );
	tg_lines = UxCreateRowColumn( "tg_lines", form4 );
	tg_emli = UxCreateToggleButton( "tg_emli", tg_lines );
	tg_noemli = UxCreateToggleButton( "tg_noemli", tg_lines );
	separator18 = UxCreateSeparator( "separator18", form4 );
	tg_sky = UxCreateRowColumn( "tg_sky", form4 );
	tg_skyon = UxCreateToggleButton( "tg_skyon", tg_sky );
	tg_skyoff = UxCreateToggleButton( "tg_skyoff", tg_sky );
	tg_extsky = UxCreateToggleButton( "tg_extsky", form4 );
	lbl_mirror1 = UxCreateLabel( "lbl_mirror1", form4 );
	tg_opt = UxCreateRowColumn( "tg_opt", form4 );
	tg_optyes = UxCreateToggleButton( "tg_optyes", tg_opt );
	tg_optno = UxCreateToggleButton( "tg_optno", tg_opt );
	tg_mir = UxCreateRowColumn( "tg_mir", form4 );
	tg_mirnew = UxCreateToggleButton( "tg_mirnew", tg_mir );
	tg_mirold = UxCreateToggleButton( "tg_mirold", tg_mir );
	tg_mirno = UxCreateToggleButton( "tg_mirno", tg_mir );
	tf_disp_pix = UxCreateText( "tf_disp_pix", form4 );
	lbl_disp = UxCreateLabel( "lbl_disp", form4 );
	label7 = UxCreateLabel( "label7", form4 );
	tf_TWmax = UxCreateText( "tf_TWmax", form4 );
	tf_TWmin = UxCreateText( "tf_TWmin", form4 );
	separator10 = UxCreateSeparator( "separator10", form4 );
	separator4 = UxCreateSeparator( "separator4", form4 );

	_Uxinit_ModelShell();

	/* Create the X widgets */

	UxCreateWidget( ModelShell );
	UxCreateWidget( form4 );
	UxCreateWidget( lbl_spec );
	UxCreateWidget( tf_spectrum );
	UxCreateWidget( lbl_atm );
	UxCreateWidget( lbl_airmass );
	UxPutBottomOffset( form6, 2 );
	UxPutBottomAttachment( form6, "attach_form" );
	UxPutTopOffset( form6, 5 );
	UxPutTopAttachment( form6, "attach_none" );
	UxPutRightOffset( form6, 0 );
	UxPutRightAttachment( form6, "attach_form" );
	UxPutLeftOffset( form6, 0 );
	UxPutLeftAttachment( form6, "attach_form" );
	UxCreateWidget( form6 );

	UxPutTopOffset( pB_MReset, 5 );
	UxPutTopAttachment( pB_MReset, "attach_form" );
	UxPutLeftOffset( pB_MReset, 15 );
	UxPutLeftAttachment( pB_MReset, "attach_form" );
	UxCreateWidget( pB_MReset );

	UxCreateWidget( pB_Mtrans );
	UxCreateWidget( pB_Mres );
	UxCreateWidget( pB_MFCreate );
	UxCreateWidget( pB_MPrint );
	UxCreateWidget( atm_mod );
	UxCreateWidget( tg_atmyes );
	UxCreateWidget( tg_atmno );
	UxPutRightOffset( helptextModel, 2 );
	UxPutRightAttachment( helptextModel, "attach_form" );
	UxPutLeftOffset( helptextModel, 2 );
	UxPutLeftAttachment( helptextModel, "attach_form" );
	UxPutBottomWidget( helptextModel, "form6" );
	UxPutBottomOffset( helptextModel, 1 );
	UxPutBottomAttachment( helptextModel, "attach_widget" );
	UxCreateWidget( helptextModel );

	UxPutLeftOffset( separator8, 0 );
	UxPutRightOffset( separator8, 0 );
	UxPutRightAttachment( separator8, "attach_form" );
	UxPutLeftAttachment( separator8, "attach_form" );
	UxPutBottomWidget( separator8, "helptextModel" );
	UxPutBottomOffset( separator8, 0 );
	UxPutBottomAttachment( separator8, "attach_widget" );
	UxCreateWidget( separator8 );

	UxCreateWidget( menu_spec );
	UxCreateWidget( mn_standard );
	UxCreateWidget( mn_hststd );
	UxCreateWidget( mn_ctiostd );
	UxCreateWidget( mn_blackbody );
	UxPutSubMenuId( mn_spec, "menu_spec" );
	UxCreateWidget( mn_spec );

	UxCreateWidget( tf_airmass );
	UxCreateWidget( lbl_mirror );
	UxCreateWidget( lbl_grism );
	UxCreateWidget( lbl_filter );
	UxCreateWidget( me_10grism );
	UxCreateWidget( mn_nogrism );
	UxCreateWidget( mn_grism0 );
	UxCreateWidget( mn_grism1 );
	UxCreateWidget( mn_grism2 );
	UxCreateWidget( mn_grism3 );
	UxCreateWidget( mn_grism4 );
	UxCreateWidget( mn_grism5 );
	UxCreateWidget( mn_grism6 );
	UxCreateWidget( mn_grism7 );
	UxCreateWidget( mn_grism8 );
	UxCreateWidget( mn_grism9 );
	UxPutSubMenuId( mn_wgrism, "me_10grism" );
	UxCreateWidget( mn_wgrism );

	UxCreateWidget( tf_filter );
	UxCreateWidget( lbl_ccd );
	UxCreateWidget( list_filt );
	UxCreateWidget( tg_basic );
	UxCreateWidget( tg_all );
	UxCreateWidget( tg_ccd );
	UxCreateWidget( tg_ccdyes );
	UxCreateWidget( tg_ccdno );
	UxCreateWidget( tf_time );
	UxCreateWidget( lbl_eadu );
	UxCreateWidget( tf_eadu );
	UxPutTopAttachment( meModel, "attach_form" );
	UxPutRightAttachment( meModel, "attach_form" );
	UxPutLeftAttachment( meModel, "attach_form" );
	UxCreateWidget( meModel );

	UxCreateWidget( me_m_file );
	UxCreateWidget( me_m_load );
	UxCreateWidget( me_m_save );
	UxCreateWidget( me_m_saveas );
	UxCreateWidget( me_m_sep );
	UxCreateWidget( me_m_loadcurve );
	UxCreateWidget( me_m_savecurve );
	UxCreateWidget( me_m_saveascurve );
	UxPutSubMenuId( mt_m_file, "me_m_file" );
	UxCreateWidget( mt_m_file );

	UxCreateWidget( me_m_frame );
	UxCreateWidget( me_m_trimx );
	UxCreateWidget( me_m_trimy );
	UxCreateWidget( me_m_unzoom );
	UxCreateWidget( me_m_redraw );
	UxCreateWidget( me_m_print );
	UxPutSubMenuId( mt_m_frame, "me_m_frame" );
	UxCreateWidget( mt_m_frame );

	UxCreateWidget( me_m_option );
	UxCreateWidget( me_m_plotoption );
	UxCreateWidget( me_m_plot );
	UxCreateWidget( me_m_oplot );
	UxPutSubMenuId( me_m_pmode, "me_m_plotoption" );
	UxCreateWidget( me_m_pmode );

	UxCreateWidget( me_m_traceoption );
	UxCreateWidget( me_m_line );
	UxCreateWidget( me_m_histo );
	UxPutSubMenuId( me_m_tmode, "me_m_traceoption" );
	UxCreateWidget( me_m_tmode );

	UxPutSubMenuId( met_m_options, "me_m_option" );
	UxCreateWidget( met_m_options );

	UxCreateWidget( me_m_help );
	UxCreateWidget( me_m_hm );
	UxPutSubMenuId( mt_m_help, "me_m_help" );
	UxCreateWidget( mt_m_help );

	UxCreateWidget( me_m_quit );
	UxCreateWidget( me_m_bye );
	UxPutSubMenuId( mt_m_quit, "me_m_quit" );
	UxCreateWidget( mt_m_quit );

	UxPutRightOffset( separator2, 0 );
	UxPutLeftOffset( separator2, 0 );
	UxPutRightAttachment( separator2, "attach_form" );
	UxPutLeftAttachment( separator2, "attach_form" );
	UxCreateWidget( separator2 );

	UxPutRightAttachment( separator3, "attach_form" );
	UxPutLeftAttachment( separator3, "attach_form" );
	UxCreateWidget( separator3 );

	UxPutRightAttachment( separator7, "attach_form" );
	UxPutLeftAttachment( separator7, "attach_form" );
	UxCreateWidget( separator7 );

	UxPutTopWidget( lbl_title, "meModel" );
	UxPutTopAttachment( lbl_title, "attach_widget" );
	UxPutTopOffset( lbl_title, 0 );
	UxPutRightOffset( lbl_title, 1 );
	UxPutLeftOffset( lbl_title, 1 );
	UxPutRightAttachment( lbl_title, "attach_form" );
	UxPutLeftAttachment( lbl_title, "attach_form" );
	UxCreateWidget( lbl_title );

	UxCreateWidget( tf_SN );
	UxCreateWidget( tg_time_sn );
	UxCreateWidget( tg_etime );
	UxCreateWidget( tg_sn );
	UxCreateWidget( lbl_sky );
	UxCreateWidget( tg_brightness );
	UxCreateWidget( tg_bright );
	UxCreateWidget( tg_dark );
	UxCreateWidget( tg_lines );
	UxCreateWidget( tg_emli );
	UxCreateWidget( tg_noemli );
	UxPutTopWidget( separator18, "lbl_title" );
	UxPutTopOffset( separator18, 0 );
	UxPutTopAttachment( separator18, "attach_widget" );
	UxPutRightAttachment( separator18, "attach_form" );
	UxPutLeftAttachment( separator18, "attach_form" );
	UxCreateWidget( separator18 );

	UxCreateWidget( tg_sky );
	UxCreateWidget( tg_skyon );
	UxCreateWidget( tg_skyoff );
	UxCreateWidget( tg_extsky );
	UxCreateWidget( lbl_mirror1 );
	UxCreateWidget( tg_opt );
	UxCreateWidget( tg_optyes );
	UxCreateWidget( tg_optno );
	UxCreateWidget( tg_mir );
	UxCreateWidget( tg_mirnew );
	UxCreateWidget( tg_mirold );
	UxCreateWidget( tg_mirno );
	UxCreateWidget( tf_disp_pix );
	UxCreateWidget( lbl_disp );
	UxCreateWidget( label7 );
	UxCreateWidget( tf_TWmax );
	UxCreateWidget( tf_TWmin );
	UxPutRightAttachment( separator10, "attach_form" );
	UxPutLeftAttachment( separator10, "attach_form" );
	UxCreateWidget( separator10 );

	UxPutTopWidget( separator4, "separator3" );
	UxPutTopOffset( separator4, 1 );
	UxPutTopAttachment( separator4, "attach_widget" );
	UxPutBottomWidget( separator4, "separator10" );
	UxPutBottomOffset( separator4, 1 );
	UxPutBottomAttachment( separator4, "attach_widget" );
	UxCreateWidget( separator4 );


	UxAddCallback( tf_spectrum, XmNmotionVerifyCallback,
			motionVerifyCB_tf_spectrum,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_spectrum, XmNfocusCallback,
			focusCB_tf_spectrum,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_spectrum, XmNlosingFocusCallback,
			losingFocusCB_tf_spectrum,
			(XtPointer) UxModelShellContext );

	UxAddCallback( pB_MReset, XmNactivateCallback,
			activateCB_pB_MReset,
			(XtPointer) UxModelShellContext );

	UxAddCallback( pB_Mtrans, XmNactivateCallback,
			activateCB_pB_Mtrans,
			(XtPointer) UxModelShellContext );

	UxAddCallback( pB_Mres, XmNactivateCallback,
			activateCB_pB_Mres,
			(XtPointer) UxModelShellContext );

	UxAddCallback( pB_MFCreate, XmNactivateCallback,
			activateCB_pB_MFCreate,
			(XtPointer) UxModelShellContext );

	UxAddCallback( pB_MPrint, XmNactivateCallback,
			activateCB_pB_MPrint,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_atmyes, XmNarmCallback,
			armCB_tg_atmyes,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_atmno, XmNarmCallback,
			armCB_tg_atmno,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tg_atmno, XmNvalueChangedCallback,
			valueChangedCB_tg_atmno,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_standard, XmNactivateCallback,
			activateCB_mn_standard,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_hststd, XmNactivateCallback,
			activateCB_mn_hststd,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_ctiostd, XmNactivateCallback,
			activateCB_mn_ctiostd,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_blackbody, XmNactivateCallback,
			activateCB_mn_blackbody,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tf_airmass, XmNfocusCallback,
			focusCB_tf_airmass,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_airmass, XmNlosingFocusCallback,
			losingFocusCB_tf_airmass,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_nogrism, XmNactivateCallback,
			activateCB_mn_nogrism,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism0, XmNactivateCallback,
			activateCB_mn_grism0,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism1, XmNactivateCallback,
			activateCB_mn_grism1,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism2, XmNactivateCallback,
			activateCB_mn_grism2,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism3, XmNactivateCallback,
			activateCB_mn_grism3,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism4, XmNactivateCallback,
			activateCB_mn_grism4,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism5, XmNactivateCallback,
			activateCB_mn_grism5,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism6, XmNactivateCallback,
			activateCB_mn_grism6,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism7, XmNactivateCallback,
			activateCB_mn_grism7,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism8, XmNactivateCallback,
			activateCB_mn_grism8,
			(XtPointer) UxModelShellContext );

	UxAddCallback( mn_grism9, XmNactivateCallback,
			activateCB_mn_grism9,
			(XtPointer) UxModelShellContext );

	UxPutMenuHistory( mn_wgrism, "mn_nogrism" );

	UxAddCallback( tf_filter, XmNfocusCallback,
			focusCB_tf_filter,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_filter, XmNmodifyVerifyCallback,
			modifyVerifyCB_tf_filter,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_filter, XmNlosingFocusCallback,
			losingFocusCB_tf_filter,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_basic, XmNarmCallback,
			armCB_tg_basic,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_all, XmNarmCallback,
			armCB_tg_all,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_ccdyes, XmNarmCallback,
			armCB_tg_ccdyes,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_ccdno, XmNarmCallback,
			armCB_tg_ccdno,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tf_time, XmNfocusCallback,
			focusCB_tf_time,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_time, XmNlosingFocusCallback,
			losingFocusCB_tf_time,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tf_eadu, XmNfocusCallback,
			focusCB_tf_eadu,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_eadu, XmNlosingFocusCallback,
			losingFocusCB_tf_eadu,
			(XtPointer) UxModelShellContext );

	UxPutMenuHelpWidget( meModel, "mt_m_quit" );

	UxAddCallback( me_m_load, XmNactivateCallback,
			activateCB_me_m_load,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_save, XmNactivateCallback,
			activateCB_me_m_save,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_saveas, XmNactivateCallback,
			activateCB_me_m_saveas,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_loadcurve, XmNactivateCallback,
			activateCB_me_m_loadcurve,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_savecurve, XmNactivateCallback,
			activateCB_me_m_savecurve,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_saveascurve, XmNactivateCallback,
			activateCB_me_m_saveascurve,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_trimx, XmNactivateCallback,
			activateCB_me_m_trimx,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_trimy, XmNactivateCallback,
			activateCB_me_m_trimy,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_unzoom, XmNactivateCallback,
			activateCB_me_m_unzoom,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_redraw, XmNactivateCallback,
			activateCB_me_m_redraw,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_print, XmNactivateCallback,
			activateCB_me_m_print,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_plot, XmNvalueChangedCallback,
			valueChangedCB_me_m_plot,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_oplot, XmNvalueChangedCallback,
			valueChangedCB_me_m_oplot,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_line, XmNvalueChangedCallback,
			valueChangedCB_me_m_line,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_histo, XmNvalueChangedCallback,
			valueChangedCB_me_m_histo,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_hm, XmNactivateCallback,
			activateCB_me_m_hm,
			(XtPointer) UxModelShellContext );

	UxAddCallback( me_m_bye, XmNactivateCallback,
			activateCB_me_m_bye,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tf_SN, XmNfocusCallback,
			focusCB_tf_SN,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_SN, XmNlosingFocusCallback,
			losingFocusCB_tf_SN,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_etime, XmNarmCallback,
			armCB_tg_etime,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_sn, XmNarmCallback,
			armCB_tg_sn,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_bright, XmNarmCallback,
			armCB_tg_bright,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_dark, XmNarmCallback,
			armCB_tg_dark,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_emli, XmNarmCallback,
			armCB_tg_emli,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_noemli, XmNarmCallback,
			armCB_tg_noemli,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_skyon, XmNarmCallback,
			armCB_tg_skyon,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_skyoff, XmNarmCallback,
			armCB_tg_skyoff,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_optyes, XmNarmCallback,
			armCB_tg_optyes,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_optno, XmNarmCallback,
			armCB_tg_optno,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_mirnew, XmNarmCallback,
			armCB_tg_mirnew,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_mirold, XmNarmCallback,
			armCB_tg_mirold,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tg_mirno, XmNarmCallback,
			armCB_tg_mirno,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tf_disp_pix, XmNfocusCallback,
			focusCB_tf_disp_pix,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_disp_pix, XmNlosingFocusCallback,
			losingFocusCB_tf_disp_pix,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tf_TWmax, XmNfocusCallback,
			focusCB_tf_TWmax,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_TWmax, XmNlosingFocusCallback,
			losingFocusCB_tf_TWmax,
			(XtPointer) UxModelShellContext );

	UxAddCallback( tf_TWmin, XmNfocusCallback,
			focusCB_tf_TWmin,
			(XtPointer) UxModelShellContext );
	UxAddCallback( tf_TWmin, XmNlosingFocusCallback,
			losingFocusCB_tf_TWmin,
			(XtPointer) UxModelShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ModelShell );

	return ( ModelShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ModelShell()
{
	swidget                 rtrn;
	_UxCModelShell          *UxContext;

	UxModelShellContext = UxContext =
		(_UxCModelShell *) UxMalloc( sizeof(_UxCModelShell) );

	rtrn = _Uxbuild_ModelShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ModelShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearShortTM", action_ClearShortTM },
				{ "HelpShortTM", action_HelpShortTM },
				{ "HelpHelp", action_HelpHelp },
				{ "ChooseListUpS", action_ChooseListUpS },
				{ "ChooseListUpF", action_ChooseListUpF }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ModelShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

