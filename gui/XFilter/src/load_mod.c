/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826		last modif

===========================================================================*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <globaldef.h>
#include <model.h>


extern void endname(), set_model(), init_model(), def_inst();

extern int flire();



void	load_mod(name)
char	name[];

{
extern	struct s_trans	T;

int	es,end,fmot();
char	term[10],first[10],second[50];
FILE	*IN;

endname(name,term);

if (strcmp(term,"trans"))
	strcat(name,".trans");

IN=fopen(name,"r");

if (IN == (FILE *)0)
	fprintf(stderr,"WARNING: file does not exist\r\n");
else
{
init_model();

do{
	fmot(IN,first);
  }while(strcmp(first,"instrument"));

/* instrument definition */

fscanf(IN,"%s",first); flire(IN,second);
def_inst(first);

do{
	end=fmot(IN,first);

	if (!strcmp(first,"spectrum"))
		{
		fscanf(IN,"%d%s%s",&T.ispectra,T.spectrapath,T.spectrafile);
		flire(IN,second);
		}
	else if (!strcmp(first,"sky"))
		{
		fscanf(IN,"%d %d %d",&T.isky,&T.darkness,&T.emlines);
		flire(IN,second);
		}
	else if (!strcmp(first,"atmosphere"))
		{
		fscanf(IN,"%d%f",&T.iatmos,&T.airmass); flire(IN,second);
		}
	else if (!strcmp(first,"mirror"))
		{
		fscanf(IN,"%d",&T.imirror); flire(IN,second);
		}
	else if (!strcmp(first,"optics"))
		{
		fscanf(IN,"%d",&T.ioptics); flire(IN,second);
		}
	else if (!strcmp(first,"dispersor"))
		{
		fscanf(IN,"%d%d",&T.igrism,&T.ngrism);
		flire(IN,second);
		}
	else if (!strcmp(first,"filter"))
		{
		fscanf(IN,"%d%d",&T.ifilter,&T.nfilter); flire(IN,second);
		}
	else if (!strcmp(first,"ccd"))
		{
		fscanf(IN,"%d%d",&T.iccd,&T.nccd); flire(IN,second);
		}
	else if (!strcmp(first,"gain"))
		{
		fscanf(IN,"%f",&T.einadu); flire(IN,second);
		}
	else if (!strcmp(first,"ron"))
		{
		fscanf(IN,"%f",&T.ron); flire(IN,second);
		}
	else if (!strcmp(first,"etime_snr"))
		{
		fscanf(IN,"%d%f%f",&T.itimeSN,&T.etime,&T.SN);
		flire(IN,second);
		}
	else if (!strcmp(first,"extractsky"))
		{
		fscanf(IN,"%d",&es); flire(IN,second);
		};
	  }
	while(end!=-1);

fclose(IN);

set_model();
};

}
