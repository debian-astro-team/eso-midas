/*===========================================================================
  Copyright (C) 1995i-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826		last modif

===========================================================================*/

/*------------------------------------------------------------
 * This is the project main program file for Ux generated  
 * code. You may add application dependent source code
 * at the appropriate places. 
 * 			     
 * Do not modify the statements preceded by the dollar
 * sign ($), these statements will be replaced with
 * the appropriate source code when the main program is
 * automatically generated. 
 *			
 * $Date: 2009-08-26 13:03:19 $  		$Revision: 1.2 $ 
 *-----------------------------------------------------------*/

#ifdef XOPEN_CATALOG
#include <locale.h>
#endif

#include <UxLib.h>


/*----------------------------------------------
 * Insert application global declarations here
 *---------------------------------------------*/
#include <signal.h>
#include <stdlib.h>
#include <StdResources.h>
#include <model.h>
#include <globalvar.h>
#include <modelvar.h>

extern void init_filter();



int MonitorPid;

int main(argc,argv)
        int     argc;
        char    *argv[];
{
        /*-----------------------------------------------------------
         * Declarations.
         * The default identifier - mainIface will only be declared  
         * if the interface function is global and of type swidget. 
         * To change the identifier to a different name, modify the
         * string mainIface in the file "main.dat".  If "mainIface"
         * is declared, it will be used below where the return value 
         * of  PJ_INTERFACE_FUNCTION_CALL will be assigned to it.
         *-----------------------------------------------------------*/ 

        swidget mainIface;

        /*--------------------------------
         * Interface function declaration
         *-------------------------------*/     

        swidget create_ApplicShell();

        

        /*---------------------
         * Initialize Program
         *--------------------*/

#ifdef XOPEN_CATALOG
        setlocale(LC_ALL, "");
#endif

        UxTopLevel = UxInitialize("XFilter", &argc, argv);

        /*------------------------------------------------------
         * Insert initialization code for your application here
         *-----------------------------------------------------*/
	init_filter(); 
          MonitorPid = atoi(argv[1]); 

        /*----------------------------------------------------------------
         * Create and popup the first window of the interface.  The      
         * return value can be used in the popdown or destroy functions.
         * The swidget return value of  PJ_INTERFACE_FUNCTION_CALL will
         * be assigned to "mainIface" from  PJ_INTERFACE_RETVAL_TYPE. 
         *---------------------------------------------------------------*/

        mainIface = create_ApplicShell();

        UxPopupInterface(mainIface, no_grab);

        /*-------------------------
         * Enter the event loop 
         *------------------------*/
        signal(SIGINT,SIG_IGN);
        signal(SIGQUIT,SIG_IGN);

        UxMainLoop();

return 0;
}

int MAIN_()
{
/* dummy routine needed by the Fortran linker */
return 0;
}
