/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <stdio.h>
#include <UxLib.h>
#include <model.h>


extern void lec_data(), set_grism();





void	set_model()

{
extern struct s_trans	T;
extern float	XWmin,XWmax;

char	word[10];

/* spectrum and sky -------------------------------------------------*/

if(T.ispectra!=0)
	UxPutText( UxFindSwidget("tf_spectrum"), T.spectrafile );
else
	UxPutText( UxFindSwidget("tf_spectrum"), "no spectrum" );

if (T.isky==0)
	{
	UxPutSet(UxFindSwidget("tg_skyon"),"false");
	UxPutSet(UxFindSwidget("tg_skyoff"),"true");
	}
else
	{
	UxPutSet(UxFindSwidget("tg_skyon"),"true");
	UxPutSet(UxFindSwidget("tg_skyoff"),"false");
	};
strcpy(T.skyfile,"sky_");

if (T.darkness==0)
	{
	UxPutSet(UxFindSwidget("tg_dark"),"false");
	UxPutSet(UxFindSwidget("tg_bright"),"true");
	strcat(T.skyfile,"b");
	}
else
	{
	UxPutSet(UxFindSwidget("tg_dark"),"true");
	UxPutSet(UxFindSwidget("tg_bright"),"false");
	strcat(T.skyfile,"d");
	};

if (T.emlines==0)
	{
	UxPutSet(UxFindSwidget("tg_emli"),"false");
	UxPutSet(UxFindSwidget("tg_noemli"),"true");
	strcat(T.skyfile,".dat");
	}
else
	{
	UxPutSet(UxFindSwidget("tg_emli"),"true");
	UxPutSet(UxFindSwidget("tg_noemli"),"false");
	strcat(T.skyfile,"_em.dat");
	};

/* atmosphere -------------------------------------------------*/

if (T.iatmos==0)
	{
	UxPutSet(UxFindSwidget("tg_atmyes"),"false");
	UxPutSet(UxFindSwidget("tg_atmno"),"true");
	}
else
	{
	UxPutSet(UxFindSwidget("tg_atmyes"),"true");
	UxPutSet(UxFindSwidget("tg_atmno"),"false");
	};
sprintf(word,"%.3f",T.airmass);
UxPutText( UxFindSwidget("tf_airmass"), word );

/* instrument  -------------------------------------------------*/

if (T.imirror==0)
	{
	UxPutSet(UxFindSwidget("tg_mirnew"),"false");
	UxPutSet(UxFindSwidget("tg_mirold"),"false");
	UxPutSet(UxFindSwidget("tg_mirno"),"true");
	}
else if (T.imirror==1)
	{
	UxPutSet(UxFindSwidget("tg_mirnew"),"true");
	UxPutSet(UxFindSwidget("tg_mirold"),"false");
	UxPutSet(UxFindSwidget("tg_mirno"),"false");
	}
else
	{
	UxPutSet(UxFindSwidget("tg_mirnew"),"false");
	UxPutSet(UxFindSwidget("tg_mirold"),"true");
	UxPutSet(UxFindSwidget("tg_mirno"),"false");
	};

if (T.ioptics==0)
	{
	UxPutSet(UxFindSwidget("tg_optyes"),"false");
	UxPutSet(UxFindSwidget("tg_optno"),"true");
	}
else
	{
	UxPutSet(UxFindSwidget("tg_optyes"),"true");
	UxPutSet(UxFindSwidget("tg_optno"),"false");
	};
if( T.dsensitive==1)
{
UxPutSensitive(UxFindSwidget("mn_wgrism"),"true");
UxPutSensitive(UxFindSwidget("lbl_grism"),"true");
if(T.igrism!=0)
	{
	sprintf(word,"mn_grism%d",T.ngrism);
	UxPutMenuHistory(UxFindSwidget("mn_wgrism"),"mn_nogrism");
	UxPutMenuHistory(UxFindSwidget("mn_wgrism"),word);
	}
else
	{
	UxPutMenuHistory(UxFindSwidget("mn_wgrism"),"mn_grism1");
	UxPutMenuHistory(UxFindSwidget("mn_wgrism"),"mn_nogrism");
	};
}
else
{
UxPutSensitive(UxFindSwidget("mn_wgrism"),"false");
UxPutSensitive(UxFindSwidget("lbl_grism"),"false");
}
set_grism(T.ngrism);

if(T.ifilter!=0)
	{
	sprintf(word,"%d",T.nfilter);
	UxPutText( UxFindSwidget("tf_filter"), word );
	}
else
	UxPutText( UxFindSwidget("tf_filter"), "free" );

if (T.iccd==0)
	{
	UxPutSet(UxFindSwidget("tg_ccdyes"),"false");
	UxPutSet(UxFindSwidget("tg_ccdno"),"true");
	}
else
	{
	UxPutSet(UxFindSwidget("tg_ccdyes"),"true");
	UxPutSet(UxFindSwidget("tg_ccdno"),"false");
	};

sprintf(word,"%.2f",T.einadu);
UxPutText( UxFindSwidget("tf_eadu"), word );

/* integration time/SN  -------------------------------------------------*/

sprintf(word,"%.1f",T.etime);
UxPutText( UxFindSwidget("tf_time"), word );
sprintf(word,"%.1f",T.SN);
UxPutText( UxFindSwidget("tf_SN"), word );

if (T.itimeSN==0)
	{
	UxPutSet(UxFindSwidget("tg_sn"),"false");
	UxPutSet(UxFindSwidget("tg_etime"),"true");
	}
else
	{
	UxPutSet(UxFindSwidget("tg_sn"),"true");
	UxPutSet(UxFindSwidget("tg_etime"),"false");
	};


/* read data tables */

XWmin=XWmax=-1;
lec_data();

UxPopupInterface(UxFindSwidget("ModelShell"),no_grab);
}
