/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	WaveBShell.c

.VERSION
 090826		last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTogB.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h> 
#include <global.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxform3;
	swidget	Uxform5;
	swidget	UxApplyWaveB;
	swidget	UxCancelWaveB;
	swidget	Uxseparator5;
	swidget	Uxlabel3;
	swidget	Uxlabel4;
	swidget	Uxtextcwmin1;
	swidget	Uxtextcwmax1;
	swidget	Uxlabel5;
	swidget	Uxlabel6;
	swidget	Uxtextbwmin;
	swidget	Uxtextbwmax;
	swidget	Uxtg_imaqual1;
} _UxCWaveBShell;

#define form3                   UxWaveBShellContext->Uxform3
#define form5                   UxWaveBShellContext->Uxform5
#define ApplyWaveB              UxWaveBShellContext->UxApplyWaveB
#define CancelWaveB             UxWaveBShellContext->UxCancelWaveB
#define separator5              UxWaveBShellContext->Uxseparator5
#define label3                  UxWaveBShellContext->Uxlabel3
#define label4                  UxWaveBShellContext->Uxlabel4
#define textcwmin1              UxWaveBShellContext->Uxtextcwmin1
#define textcwmax1              UxWaveBShellContext->Uxtextcwmax1
#define label5                  UxWaveBShellContext->Uxlabel5
#define label6                  UxWaveBShellContext->Uxlabel6
#define textbwmin               UxWaveBShellContext->Uxtextbwmin
#define textbwmax               UxWaveBShellContext->Uxtextbwmax
#define tg_imaqual1             UxWaveBShellContext->Uxtg_imaqual1

static _UxCWaveBShell	*UxWaveBShellContext;

extern void search_cwbw();


swidget	WaveBShell;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*WBtext_tab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_WaveBShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_ApplyWaveB( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveBShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveBShellContext;
	UxWaveBShellContext = UxContext =
			(_UxCWaveBShell *) UxGetContext( UxThisWidget );
	{
	extern float cw1,cw2,bw1,bw2;
	
	search_cwbw(cw1,cw2,bw1,bw2);
	
	UxPopdownInterface(WaveBShell);
	}
	UxWaveBShellContext = UxSaveCtx;
}

static void	activateCB_CancelWaveB( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveBShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveBShellContext;
	UxWaveBShellContext = UxContext =
			(_UxCWaveBShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(WaveBShell);
	}
	UxWaveBShellContext = UxSaveCtx;
}

static void	losingFocusCB_textcwmin1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveBShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveBShellContext;
	UxWaveBShellContext = UxContext =
			(_UxCWaveBShell *) UxGetContext( UxThisWidget );
	{
	
	int nid;
	char am[10];
	float minw;
	extern float cw1;
	
	nid=sscanf(UxGetText(UxFindSwidget("textcwmin1")),"%f",&minw);
	if (nid==1)
		{
		if ((minw>=300.)&&(minw<=1100.))
			cw1=minw;
		else if (minw<300.)
			cw1=300.;
		else if (minw>1100.)
			cw1=1100.;
		}
	
	sprintf(am,"%.1f",cw1);
	UxPutText(UxFindSwidget("textcwmin1"),am);
	
	
	}
	UxWaveBShellContext = UxSaveCtx;
}

static void	losingFocusCB_textcwmax1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveBShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveBShellContext;
	UxWaveBShellContext = UxContext =
			(_UxCWaveBShell *) UxGetContext( UxThisWidget );
	{
	 int nid;
	char am[20];
	float maxw;
	extern float cw2;
	
	
	nid=sscanf(UxGetText(UxFindSwidget("textcwmax1")),"%f",&maxw);
	if (nid==1)
		{
		if ((maxw>=400.)&&(maxw<=1200.))
			cw2=maxw;
		else if (maxw<400.)
			cw2=400.;
		else if (maxw>1200.)
			cw2=1200.;
		}
	
	sprintf(am,"%.1f",cw2);
	UxPutText(UxFindSwidget("textcwmax1"),am);
	
	
	}
	UxWaveBShellContext = UxSaveCtx;
}

static void	losingFocusCB_textbwmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveBShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveBShellContext;
	UxWaveBShellContext = UxContext =
			(_UxCWaveBShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float minw;
	extern float bw1;
	
	nid=sscanf(UxGetText(UxFindSwidget("textbwmin")),"%f",&minw);
	if (nid==1)
		{
		if ((minw>=0.)&&(minw<=500.))
			bw1=minw;
		else if (minw<0.)
			bw1=0.;
		else if (minw>500.)
			bw1=500.;
		}
	
	sprintf(am,"%.1f",bw1);
	UxPutText(UxFindSwidget("textbwmin"),am);
	
	}
	UxWaveBShellContext = UxSaveCtx;
}

static void	losingFocusCB_textbwmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveBShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveBShellContext;
	UxWaveBShellContext = UxContext =
			(_UxCWaveBShell *) UxGetContext( UxThisWidget );
	{
	 
	int nid;
	char am[20];
	float maxw;
	extern float bw2;
	
	
	nid=sscanf(UxGetText(UxFindSwidget("textbwmax")),"%f",&maxw);
	if (nid==1)
		{
		if ((maxw>=5.)&&(maxw<=600.))
			bw2=maxw;
		else if (maxw<5.)
			bw2=5.;
		else if (maxw>600.)
			bw2=600.;
		}
	
	sprintf(am,"%.1f",bw2);
	UxPutText(UxFindSwidget("textbwmax"),am);
	
	}
	UxWaveBShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_WaveBShell()
{
	UxPutBackground( WaveBShell, WindowBackground );
	UxPutBorderColor( WaveBShell, WindowBackground );
	UxPutKeyboardFocusPolicy( WaveBShell, "pointer" );
	UxPutIconName( WaveBShell, "Wavelength & Bandwidth" );
	UxPutHeight( WaveBShell, 260 );
	UxPutWidth( WaveBShell, 325 );
	UxPutY( WaveBShell, 275 );
	UxPutX( WaveBShell, 619 );

	UxPutBorderColor( form3, WindowBackground );
	UxPutBackground( form3, WindowBackground );
	UxPutHeight( form3, 548 );
	UxPutWidth( form3, 490 );
	UxPutY( form3, 4 );
	UxPutX( form3, 4 );
	UxPutUnitType( form3, "pixels" );
	UxPutResizePolicy( form3, "resize_none" );

	UxPutBorderColor( form5, ButtonBackground );
	UxPutBackground( form5, ButtonBackground );
	UxPutHeight( form5, 44 );
	UxPutWidth( form5, 490 );
	UxPutY( form5, 150 );
	UxPutX( form5, 0 );
	UxPutResizePolicy( form5, "resize_none" );

	UxPutLabelString( ApplyWaveB, "Apply" );
	UxPutForeground( ApplyWaveB, ApplyForeground );
	UxPutFontList( ApplyWaveB, BoldTextFont );
	UxPutBorderColor( ApplyWaveB, ButtonBackground );
	UxPutBackground( ApplyWaveB, ButtonBackground );
	UxPutHeight( ApplyWaveB, 30 );
	UxPutWidth( ApplyWaveB, 80 );
	UxPutY( ApplyWaveB, 6 );
	UxPutX( ApplyWaveB, 10 );

	UxPutLabelString( CancelWaveB, "Cancel" );
	UxPutForeground( CancelWaveB, CancelForeground );
	UxPutFontList( CancelWaveB, BoldTextFont );
	UxPutBorderColor( CancelWaveB, ButtonBackground );
	UxPutBackground( CancelWaveB, ButtonBackground );
	UxPutHeight( CancelWaveB, 30 );
	UxPutWidth( CancelWaveB, 86 );
	UxPutY( CancelWaveB, 6 );
	UxPutX( CancelWaveB, 110 );

	UxPutBorderColor( separator5, WindowBackground );
	UxPutBackground( separator5, WindowBackground );
	UxPutHeight( separator5, 10 );
	UxPutWidth( separator5, 492 );
	UxPutY( separator5, 140 );
	UxPutX( separator5, 0 );

	UxPutForeground( label3, TextForeground );
	UxPutAlignment( label3, "alignment_beginning" );
	UxPutLabelString( label3, "Central Wavelenght min (nm) :" );
	UxPutFontList( label3, TextFont );
	UxPutBorderColor( label3, LabelBackground );
	UxPutBackground( label3, LabelBackground );
	UxPutHeight( label3, 30 );
	UxPutWidth( label3, 220 );
	UxPutY( label3, 10 );
	UxPutX( label3, 10 );

	UxPutForeground( label4, TextForeground );
	UxPutAlignment( label4, "alignment_beginning" );
	UxPutLabelString( label4, "Central Wavelenght max (nm) :" );
	UxPutFontList( label4, TextFont );
	UxPutBorderColor( label4, LabelBackground );
	UxPutBackground( label4, LabelBackground );
	UxPutHeight( label4, 28 );
	UxPutWidth( label4, 220 );
	UxPutY( label4, 52 );
	UxPutX( label4, 10 );

	UxPutTranslations( textcwmin1, WBtext_tab );
	UxPutSelectionArrayCount( textcwmin1, 3 );
	UxPutMaxLength( textcwmin1, 200 );
	UxPutForeground( textcwmin1, TextForeground );
	UxPutFontList( textcwmin1, TextFont );
	UxPutBorderColor( textcwmin1, TextBackground );
	UxPutBackground( textcwmin1, TextBackground );
	UxPutHeight( textcwmin1, 35 );
	UxPutWidth( textcwmin1, 80 );
	UxPutY( textcwmin1, 8 );
	UxPutX( textcwmin1, 236 );

	UxPutTranslations( textcwmax1, WBtext_tab );
	UxPutSelectionArrayCount( textcwmax1, 3 );
	UxPutMaxLength( textcwmax1, 200 );
	UxPutForeground( textcwmax1, TextForeground );
	UxPutFontList( textcwmax1, TextFont );
	UxPutBorderColor( textcwmax1, TextBackground );
	UxPutBackground( textcwmax1, TextBackground );
	UxPutHeight( textcwmax1, 35 );
	UxPutWidth( textcwmax1, 80 );
	UxPutY( textcwmax1, 48 );
	UxPutX( textcwmax1, 236 );

	UxPutForeground( label5, TextForeground );
	UxPutAlignment( label5, "alignment_beginning" );
	UxPutLabelString( label5, "Bandwidth min (nm) :" );
	UxPutFontList( label5, TextFont );
	UxPutBorderColor( label5, LabelBackground );
	UxPutBackground( label5, LabelBackground );
	UxPutHeight( label5, 30 );
	UxPutWidth( label5, 220 );
	UxPutY( label5, 90 );
	UxPutX( label5, 10 );

	UxPutForeground( label6, TextForeground );
	UxPutAlignment( label6, "alignment_beginning" );
	UxPutLabelString( label6, "Bandwidth max (nm) :" );
	UxPutFontList( label6, TextFont );
	UxPutBorderColor( label6, LabelBackground );
	UxPutBackground( label6, LabelBackground );
	UxPutHeight( label6, 30 );
	UxPutWidth( label6, 220 );
	UxPutY( label6, 130 );
	UxPutX( label6, 10 );

	UxPutTranslations( textbwmin, WBtext_tab );
	UxPutSelectionArrayCount( textbwmin, 3 );
	UxPutMaxLength( textbwmin, 200 );
	UxPutForeground( textbwmin, TextForeground );
	UxPutFontList( textbwmin, TextFont );
	UxPutBorderColor( textbwmin, TextBackground );
	UxPutBackground( textbwmin, TextBackground );
	UxPutHeight( textbwmin, 35 );
	UxPutWidth( textbwmin, 80 );
	UxPutY( textbwmin, 88 );
	UxPutX( textbwmin, 236 );

	UxPutTranslations( textbwmax, WBtext_tab );
	UxPutSelectionArrayCount( textbwmax, 3 );
	UxPutMaxLength( textbwmax, 200 );
	UxPutForeground( textbwmax, TextForeground );
	UxPutFontList( textbwmax, TextFont );
	UxPutBorderColor( textbwmax, TextBackground );
	UxPutBackground( textbwmax, TextBackground );
	UxPutHeight( textbwmax, 35 );
	UxPutWidth( textbwmax, 80 );
	UxPutY( textbwmax, 128 );
	UxPutX( textbwmax, 236 );

	UxPutHighlightOnEnter( tg_imaqual1, "true" );
	UxPutForeground( tg_imaqual1, TextForeground );
	UxPutSelectColor( tg_imaqual1, SelectColor );
	UxPutSet( tg_imaqual1, "true" );
	UxPutLabelString( tg_imaqual1, "Image Quality" );
	UxPutFontList( tg_imaqual1, TextFont );
	UxPutBorderColor( tg_imaqual1, WindowBackground );
	UxPutBackground( tg_imaqual1, WindowBackground );
	UxPutHeight( tg_imaqual1, 26 );
	UxPutWidth( tg_imaqual1, 134 );
	UxPutY( tg_imaqual1, 170 );
	UxPutX( tg_imaqual1, 10 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_WaveBShell()
{
	/* Create the swidgets */

	WaveBShell = UxCreateApplicationShell( "WaveBShell", NO_PARENT );
	UxPutContext( WaveBShell, UxWaveBShellContext );

	form3 = UxCreateForm( "form3", WaveBShell );
	form5 = UxCreateForm( "form5", form3 );
	ApplyWaveB = UxCreatePushButton( "ApplyWaveB", form5 );
	CancelWaveB = UxCreatePushButton( "CancelWaveB", form5 );
	separator5 = UxCreateSeparator( "separator5", form3 );
	label3 = UxCreateLabel( "label3", form3 );
	label4 = UxCreateLabel( "label4", form3 );
	textcwmin1 = UxCreateText( "textcwmin1", form3 );
	textcwmax1 = UxCreateText( "textcwmax1", form3 );
	label5 = UxCreateLabel( "label5", form3 );
	label6 = UxCreateLabel( "label6", form3 );
	textbwmin = UxCreateText( "textbwmin", form3 );
	textbwmax = UxCreateText( "textbwmax", form3 );
	tg_imaqual1 = UxCreateToggleButton( "tg_imaqual1", form3 );

	_Uxinit_WaveBShell();

	/* Create the X widgets */

	UxCreateWidget( WaveBShell );
	UxCreateWidget( form3 );
	UxPutRightAttachment( form5, "attach_form" );
	UxPutLeftAttachment( form5, "attach_form" );
	UxPutBottomOffset( form5, 2 );
	UxPutBottomAttachment( form5, "attach_form" );
	UxCreateWidget( form5 );

	UxCreateWidget( ApplyWaveB );
	UxCreateWidget( CancelWaveB );
	UxPutRightAttachment( separator5, "attach_form" );
	UxPutLeftAttachment( separator5, "attach_form" );
	UxPutBottomWidget( separator5, "form5" );
	UxPutBottomOffset( separator5, 0 );
	UxPutBottomAttachment( separator5, "attach_widget" );
	UxCreateWidget( separator5 );

	UxCreateWidget( label3 );
	UxCreateWidget( label4 );
	UxCreateWidget( textcwmin1 );
	UxCreateWidget( textcwmax1 );
	UxCreateWidget( label5 );
	UxCreateWidget( label6 );
	UxCreateWidget( textbwmin );
	UxCreateWidget( textbwmax );
	UxCreateWidget( tg_imaqual1 );

	UxAddCallback( ApplyWaveB, XmNactivateCallback,
			activateCB_ApplyWaveB,
			(XtPointer) UxWaveBShellContext );

	UxAddCallback( CancelWaveB, XmNactivateCallback,
			activateCB_CancelWaveB,
			(XtPointer) UxWaveBShellContext );

	UxAddCallback( textcwmin1, XmNlosingFocusCallback,
			losingFocusCB_textcwmin1,
			(XtPointer) UxWaveBShellContext );

	UxAddCallback( textcwmax1, XmNlosingFocusCallback,
			losingFocusCB_textcwmax1,
			(XtPointer) UxWaveBShellContext );

	UxAddCallback( textbwmin, XmNlosingFocusCallback,
			losingFocusCB_textbwmin,
			(XtPointer) UxWaveBShellContext );

	UxAddCallback( textbwmax, XmNlosingFocusCallback,
			losingFocusCB_textbwmax,
			(XtPointer) UxWaveBShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( WaveBShell );

	return ( WaveBShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_WaveBShell()
{
	swidget                 rtrn;
	_UxCWaveBShell          *UxContext;

	UxWaveBShellContext = UxContext =
		(_UxCWaveBShell *) UxMalloc( sizeof(_UxCWaveBShell) );

	rtrn = _Uxbuild_WaveBShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_WaveBShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_WaveBShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

