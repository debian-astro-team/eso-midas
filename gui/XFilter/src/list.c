/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <stdio.h>
#include <UxLib.h>
#include <Xm/List.h>


void DisplayList( list, nelem )
char *list[];
int nelem;
{
Widget w;

    int i;
    XmStringTable str_list;


w=UxGetWidget(UxFindSwidget("scrolledList"));

str_list = (XmStringTable)XtMalloc(nelem * sizeof(XmString *));

    for (i=0;i<nelem;i++)
        str_list[i] = XmStringCreateSimple(list[i]);

    XmListSetPos(w,1);
    XmListDeleteAllItems(w);
    XmListAddItems(w,str_list,nelem,1);

    for (i=0;i<nelem;i++)
        XmStringFree(str_list[i]);
    /* XtFree((XmStringTable) str_list); */
    XtFree((char *) str_list);

}

void ClearList()
{
Widget w;

w=UxGetWidget(UxFindSwidget("scrolledList"));

    XmListSetPos(w,1);
    XmListDeleteAllItems(w);
}

void DisplayChoose( list, nelem )
char *list[];
int nelem;
{
Widget w;

    int i;
    XmStringTable str_list;


w=UxGetWidget(UxFindSwidget("scrolledchoose"));

str_list = (XmStringTable)XtMalloc(nelem * sizeof(XmString *));

    for (i=0;i<nelem;i++)
        str_list[i] = XmStringCreateSimple(list[i]);

    XmListSetPos(w,1);
    XmListDeleteAllItems(w);
    XmListAddItems(w,str_list,nelem,1);

    for (i=0;i<nelem;i++)
        XmStringFree(str_list[i]);
    /* XtFree((XmStringTable) str_list);*/
    XtFree((char *) str_list);
}

