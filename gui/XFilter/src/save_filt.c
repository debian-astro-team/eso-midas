/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <stdio.h>
#include <string.h>
#include <globaldef.h>
#include <model.h>


extern void endname();




void	save_filt(name)
char	name[];
{
extern	struct s_filter	F;

FILE	*out;
char	term[10];

if(strlen(name)!=0)
{
endname(name,term);
if(strcmp(term,"flt"))
	strcat(name,".flt");
}
else
	strcpy(name,"tmp.flt");

strcpy(F.filterfile,name);

out=fopen(name,"w");

fprintf(out,"# filter characteristic: %s\n",name);
fprintf(out,"cw\t%f\n",F.cw);
fprintf(out,"bw\t%f\n",F.bw);
fprintf(out,"peak\t%f\n",F.peak);
fprintf(out,"# red leak\n");
fprintf(out,"cwl\t%f\n",F.cwl);
fprintf(out,"bwl\t%f\n",F.bwl);
fprintf(out,"peakl\t%f\n",F.peakl);

fclose(out);
}
