/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	HelpFShell.c

.VERSION
 090826         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxRowCol.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxHelpFShell;
	swidget	Uxform20;
	swidget	UxscrolledWindow9;
	swidget	UxrowColumn4;
	swidget	Uxhelp_Text;
	swidget	Uxhelp_print;
	swidget	Uxpb_help_return;
} _UxCHelpFShell;

#define HelpFShell              UxHelpFShellContext->UxHelpFShell
#define form20                  UxHelpFShellContext->Uxform20
#define scrolledWindow9         UxHelpFShellContext->UxscrolledWindow9
#define rowColumn4              UxHelpFShellContext->UxrowColumn4
#define help_Text               UxHelpFShellContext->Uxhelp_Text
#define help_print              UxHelpFShellContext->Uxhelp_print
#define pb_help_return          UxHelpFShellContext->Uxpb_help_return

static _UxCHelpFShell	*UxHelpFShellContext;

extern void PrintExtendedHelp();



/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_HelpFShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_help_print( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCHelpFShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxHelpFShellContext;
	UxHelpFShellContext = UxContext =
			(_UxCHelpFShell *) UxGetContext( UxThisWidget );
	{
	PrintExtendedHelp();
	}
	UxHelpFShellContext = UxSaveCtx;
}

static void	activateCB_pb_help_return( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCHelpFShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxHelpFShellContext;
	UxHelpFShellContext = UxContext =
			(_UxCHelpFShell *) UxGetContext( UxThisWidget );
	{
	 
	UxPopdownInterface(UxFindSwidget("HelpFShell"));
	
	}
	UxHelpFShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_HelpFShell()
{
	UxPutBackground( HelpFShell, ButtonBackground );
	UxPutIconName( HelpFShell, "Extended help" );
	UxPutKeyboardFocusPolicy( HelpFShell, "pointer" );
	UxPutTitle( HelpFShell, "Extended help" );
	UxPutHeight( HelpFShell, 415 );
	UxPutWidth( HelpFShell, 708 );
	UxPutY( HelpFShell, 28 );
	UxPutX( HelpFShell, 20 );

	UxPutBackground( form20, ButtonBackground );
	UxPutHeight( form20, 284 );
	UxPutWidth( form20, 664 );
	UxPutY( form20, 0 );
	UxPutX( form20, 0 );
	UxPutUnitType( form20, "pixels" );
	UxPutResizePolicy( form20, "resize_none" );

	UxPutBackground( scrolledWindow9, WindowBackground );
	UxPutScrollBarPlacement( scrolledWindow9, "bottom_left" );
	UxPutHeight( scrolledWindow9, 372 );
	UxPutWidth( scrolledWindow9, 710 );
	UxPutY( scrolledWindow9, 2 );
	UxPutX( scrolledWindow9, 0 );
	UxPutScrollingPolicy( scrolledWindow9, "automatic" );

	UxPutEntryAlignment( rowColumn4, "alignment_beginning" );
	UxPutBackground( rowColumn4, "white" );
	UxPutOrientation( rowColumn4, "horizontal" );
	UxPutHeight( rowColumn4, 427 );
	UxPutWidth( rowColumn4, 418 );
	UxPutY( rowColumn4, 4 );
	UxPutX( rowColumn4, 25 );

	UxPutHighlightColor( help_Text, "Black" );
	UxPutForeground( help_Text, TextForeground );
	UxPutCursorPositionVisible( help_Text, "false" );
	UxPutEditMode( help_Text, "multi_line_edit" );
	UxPutFontList( help_Text, SmallFont );
	UxPutBackground( help_Text, TextBackground );
	UxPutHeight( help_Text, 846 );
	UxPutWidth( help_Text, 674 );
	UxPutY( help_Text, -58 );
	UxPutX( help_Text, 3 );

	UxPutBorderColor( help_print, ButtonBackground );
	UxPutForeground( help_print, ButtonForeground );
	UxPutHighlightOnEnter( help_print, "true" );
	UxPutHighlightColor( help_print, "Black" );
	UxPutFontList( help_print, BoldTextFont );
	UxPutLabelString( help_print, "Print" );
	UxPutBackground( help_print, ButtonBackground );
	UxPutHeight( help_print, 32 );
	UxPutWidth( help_print, 82 );
	UxPutY( help_print, 378 );
	UxPutX( help_print, 532 );

	UxPutBorderColor( pb_help_return, ButtonBackground );
	UxPutForeground( pb_help_return, CancelForeground );
	UxPutHighlightOnEnter( pb_help_return, "true" );
	UxPutHighlightColor( pb_help_return, "red" );
	UxPutLabelString( pb_help_return, "Return" );
	UxPutFontList( pb_help_return, BoldTextFont );
	UxPutBackground( pb_help_return, ButtonBackground );
	UxPutHeight( pb_help_return, 32 );
	UxPutWidth( pb_help_return, 82 );
	UxPutY( pb_help_return, 378 );
	UxPutX( pb_help_return, 616 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_HelpFShell()
{
	/* Create the swidgets */

	HelpFShell = UxCreateTopLevelShell( "HelpFShell", NO_PARENT );
	UxPutContext( HelpFShell, UxHelpFShellContext );

	form20 = UxCreateForm( "form20", HelpFShell );
	scrolledWindow9 = UxCreateScrolledWindow( "scrolledWindow9", form20 );
	rowColumn4 = UxCreateRowColumn( "rowColumn4", scrolledWindow9 );
	help_Text = UxCreateText( "help_Text", rowColumn4 );
	help_print = UxCreatePushButton( "help_print", form20 );
	pb_help_return = UxCreatePushButton( "pb_help_return", form20 );

	_Uxinit_HelpFShell();

	/* Create the X widgets */

	UxCreateWidget( HelpFShell );
	UxCreateWidget( form20 );
	UxCreateWidget( scrolledWindow9 );
	UxCreateWidget( rowColumn4 );
	UxCreateWidget( help_Text );
	UxCreateWidget( help_print );
	UxCreateWidget( pb_help_return );

	UxAddCallback( help_print, XmNactivateCallback,
			activateCB_help_print,
			(XtPointer) UxHelpFShellContext );

	UxAddCallback( pb_help_return, XmNactivateCallback,
			activateCB_pb_help_return,
			(XtPointer) UxHelpFShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( HelpFShell );

	return ( HelpFShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_HelpFShell()
{
	swidget                 rtrn;
	_UxCHelpFShell          *UxContext;

	UxHelpFShellContext = UxContext =
		(_UxCHelpFShell *) UxMalloc( sizeof(_UxCHelpFShell) );

	rtrn = _Uxbuild_HelpFShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_HelpFShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_HelpFShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

