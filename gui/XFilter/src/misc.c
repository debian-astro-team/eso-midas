/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <gl_defs.h>
#include <xm_defs.h>
#include <main_defs.h>
#include <spec_comm.h>
#include <UxLib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <midas_def.h>
 

extern int AppendDialogText();



#define PRINT_COMMAND           "$ {syscoms(1:20)} {mid$prnt(3:52)} "
#define PRINT_FILE              "help.out" 

#define SHELP_FILE	"gui/XFilter/help/sfilter.hlp"
#define HELP_FILE	"gui/XFilter/help/filter.hlp"
#define NEWS_FILE	"filters/filter.news"
#define HELP_DELIMITER	'~'

#define MAX_HELP_LINES  80*200  /* Max of 200 lines */

static char help_text[MAX_HELP_LINES];
static char print_text[MAX_HELP_LINES];
static char shelp_text[80*2]; /* Max of 2 lines */

void PrintExtendedHelp();

/*------------ DisplayExtendedHelp ----------------------------------*/

void DisplayExtendedHelp( sw )
Widget sw; 	/* current widget */
{
    char s[4000];
    char *find_extended_help();
 
/* Main Window */
    if ( sw == UxGetWidget(UxFindSwidget("EmmiButtonB")) )
      	strcpy(s, find_extended_help("EMMIB"));
    else if ( sw == UxGetWidget(UxFindSwidget("EmmiButtonR")) )
      	strcpy(s, find_extended_help("EMMIR"));
    else if ( sw == UxGetWidget(UxFindSwidget("Efosc1Button")) )
      	strcpy(s, find_extended_help("EFOSC1"));
    else if ( sw == UxGetWidget(UxFindSwidget("Efosc2Button")) )
      	strcpy(s, find_extended_help("EFOSC2"));
    else if ( sw == UxGetWidget(UxFindSwidget("SusiButton")) )
      	strcpy(s, find_extended_help("SUSI"));
    else if ( sw == UxGetWidget(UxFindSwidget("me_f_hfilter")) )
      	strcpy(s, find_extended_help("CONTEXT"));
    else if ( sw == UxGetWidget(UxFindSwidget("me_f_hhelp")) )
      	strcpy(s, find_extended_help("HELP"));

/* Model Shell */
    else if ( sw == UxGetWidget(UxFindSwidget("me_m_hm")) )
      	strcpy(s, find_extended_help("TMODEL"));
    else if ( sw == UxGetWidget(UxFindSwidget("pB_MReset")) )
      	strcpy(s, find_extended_help("RESET"));
    else if ( sw == UxGetWidget(UxFindSwidget("pB_MPrint")) )
      	strcpy(s, find_extended_help("PRINT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pB_MFCreate")) )
      	strcpy(s, find_extended_help("CREAFILTER"));
    else if ( sw == UxGetWidget(UxFindSwidget("pB_Mtrans")) )
      	strcpy(s, find_extended_help("TRANS"));
    else if ( sw == UxGetWidget(UxFindSwidget("pB_Mres")) )
      	strcpy(s, find_extended_help("OBSFLUX"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_spec")) )
      	strcpy(s, find_extended_help("SELECTSPEC"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_skyon")) )
      	strcpy(s, find_extended_help("SKYON"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_skyoff")) )
      	strcpy(s, find_extended_help("SKYOFF"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_bright")) )
      	strcpy(s, find_extended_help("BRIGHT"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_dark")) )
      	strcpy(s, find_extended_help("DARK"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_emli")) )
      	strcpy(s, find_extended_help("EMLINES"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_noemli")) )
      	strcpy(s, find_extended_help("NOEMLINES"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_atmyes")) )
      	strcpy(s, find_extended_help("ATMOS"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_atmno")) )
      	strcpy(s, find_extended_help("NOATMOS"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_mirnew")) )
      	strcpy(s, find_extended_help("MIRRORNEW"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_mirold")) )
      	strcpy(s, find_extended_help("MIRROROLD"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_mirno")) )
      	strcpy(s, find_extended_help("MIRRORNO"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_optyes")) )
      	strcpy(s, find_extended_help("OPTICON"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_optno")) )
      	strcpy(s, find_extended_help("OPTICOFF"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_wgrism")) )
      	strcpy(s, find_extended_help("SELECTGRISM"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_basic")) )
      	strcpy(s, find_extended_help("FBASIC"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_all")) )
      	strcpy(s, find_extended_help("FALL"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_ccdyes")) )
      	strcpy(s, find_extended_help("CCDON"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_ccdno")) )
      	strcpy(s, find_extended_help("CCDOFF"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_etime")) )
      	strcpy(s, find_extended_help("ETIME"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_sn")) )
      	strcpy(s, find_extended_help("SNRATIO"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_extsky")) )
      	strcpy(s, find_extended_help("EXTSKY"));
    else if ( sw == UxGetWidget(UxFindSwidget("me_fc_hm")) )
      	strcpy(s, find_extended_help("CREAFILTER"));

/* Cloudy Shell */
    else if ( sw == UxGetWidget(UxFindSwidget("me_c_hcloudy")) )
      	strcpy(s, find_extended_help("CLOUDY"));

/* Bruzual Shell */
    else if ( sw == UxGetWidget(UxFindSwidget("BM_newised")) )
      	strcpy(s, find_extended_help("BMNEWISED"));
    else if ( sw == UxGetWidget(UxFindSwidget("BM_apply")) )
      	strcpy(s, find_extended_help("BMFLUX"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_IMF")) )
      	strcpy(s, find_extended_help("IMF"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_mass")) )
      	strcpy(s, find_extended_help("IMFMASS"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_ssp")) )
      	strcpy(s, find_extended_help("POPINST"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_csp")) )
      	strcpy(s, find_extended_help("POPCOMP"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_SFR")) )
      	strcpy(s, find_extended_help("SFR"));
    else if ( sw == UxGetWidget(UxFindSwidget("tf_sfr_c")) )
      	strcpy(s, find_extended_help("SFR_C"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_gaz")) )
      	strcpy(s, find_extended_help("RECYCLE"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_nogaz")) )
      	strcpy(s, find_extended_help("NORECYCLE"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_dist")) )
      	strcpy(s, find_extended_help("ZMPC"));
    else if ( sw == UxGetWidget(UxFindSwidget("me_b_hgissel")) )
      	strcpy(s, find_extended_help("GISSEL"));

    if ( s[0] != '\0' ) {
    	UxPopupInterface(UxFindSwidget("HelpFShell"), no_grab);
    	UxPutText(UxFindSwidget("help_Text"), s);
    }
    /* let's save the text for optional printing */
    strcpy(print_text, s);
}

/*--------- DisplayShortMHelp --------------------------------------*/

void DisplayShortMHelp( sw )
Widget sw; 	/* current widget */
{
    char s[160];
    char *find_short_help();
 
/* Main Window */
    if ( sw == UxGetWidget(UxFindSwidget("EmmiButtonB")) )
      	strcpy(s, find_short_help("EMMIB"));
    else if ( sw == UxGetWidget(UxFindSwidget("EmmiButtonR")) )
      	strcpy(s, find_short_help("EMMIR"));
    else if ( sw == UxGetWidget(UxFindSwidget("Efosc1Button")) )
      	strcpy(s, find_short_help("EFOSC1"));
    else if ( sw == UxGetWidget(UxFindSwidget("Efosc2Button")) )
      	strcpy(s, find_short_help("EFOSC2"));
    else if ( sw == UxGetWidget(UxFindSwidget("SusiButton")) )
      	strcpy(s, find_short_help("SUSI"));

    if ( s[0] != '\0' ) {
    	UxPutText(UxFindSwidget("SHelp"), s);
    }
}

/*--------- DisplayShortBMHelp --------------------------------------*/

void DisplayShortBMHelp( sw )
Widget sw; 	/* current widget */
{
    char s[160];
    char *find_short_help();
 

/* Bruzual Shell */
    if ( sw == UxGetWidget(UxFindSwidget("BM_newised")) )
      	strcpy(s, find_short_help("BMNEWISED"));
    else if ( sw == UxGetWidget(UxFindSwidget("BM_apply")) )
      	strcpy(s, find_short_help("BMFLUX"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_IMF")) )
      	strcpy(s, find_short_help("IMF"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_mass")) )
      	strcpy(s, find_short_help("IMFMASS"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_ssp")) )
      	strcpy(s, find_short_help("POPINST"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_csp")) )
      	strcpy(s, find_short_help("POPCOMP"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_SFR")) )
      	strcpy(s, find_short_help("SFR"));
    else if ( sw == UxGetWidget(UxFindSwidget("tf_sfr_c")) )
      	strcpy(s, find_short_help("SFR_C"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_gaz")) )
      	strcpy(s, find_short_help("RECYCLE"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_nogaz")) )
      	strcpy(s, find_short_help("NORECYCLE"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_dist")) )
      	strcpy(s, find_short_help("ZMPC"));

    if ( s[0] != '\0' ) {
    	UxPutText(UxFindSwidget("helptextBM"), s);
    }
}

/*--------- DisplayShortTMHelp -------------------------------------*/

void DisplayShortTMHelp( sw )
Widget sw; 	/* current widget */
{
    char s[160];
    char *find_short_help();
 
/* Model Shell */
    if ( sw == UxGetWidget(UxFindSwidget("pB_MReset")) )
      	strcpy(s, find_short_help("RESET"));
    else if ( sw == UxGetWidget(UxFindSwidget("pB_MPrint")) )
      	strcpy(s, find_short_help("PRINT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pB_MFCreate")) )
      	strcpy(s, find_short_help("CREAFILTER"));
    else if ( sw == UxGetWidget(UxFindSwidget("pB_Mtrans")) )
      	strcpy(s, find_short_help("TRANS"));
    else if ( sw == UxGetWidget(UxFindSwidget("pB_Mres")) )
      	strcpy(s, find_short_help("OBSFLUX"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_spec")) )
      	strcpy(s, find_short_help("SELECTSPEC"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_skyon")) )
      	strcpy(s, find_short_help("SKYON"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_skyoff")) )
      	strcpy(s, find_short_help("SKYOFF"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_bright")) )
      	strcpy(s, find_short_help("BRIGHT"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_dark")) )
      	strcpy(s, find_short_help("DARK"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_emli")) )
      	strcpy(s, find_short_help("EMLINES"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_noemli")) )
      	strcpy(s, find_short_help("NOEMLINES"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_atmyes")) )
      	strcpy(s, find_short_help("ATMOS"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_atmno")) )
      	strcpy(s, find_short_help("NOATMOS"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_mirnew")) )
      	strcpy(s, find_short_help("MIRRORNEW"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_mirold")) )
      	strcpy(s, find_short_help("MIRROROLD"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_mirno")) )
      	strcpy(s, find_short_help("MIRRORNO"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_optyes")) )
      	strcpy(s, find_short_help("OPTICON"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_optno")) )
      	strcpy(s, find_short_help("OPTICOFF"));
    else if ( sw == UxGetWidget(UxFindSwidget("mn_wgrism")) )
      	strcpy(s, find_short_help("SELECTGRISM"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_basic")) )
      	strcpy(s, find_short_help("FBASIC"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_all")) )
      	strcpy(s, find_short_help("FALL"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_ccdyes")) )
      	strcpy(s, find_short_help("CCDON"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_ccdno")) )
      	strcpy(s, find_short_help("CCDOFF"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_etime")) )
      	strcpy(s, find_short_help("ETIME"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_sn")) )
      	strcpy(s, find_short_help("SNRATIO"));
    else if ( sw == UxGetWidget(UxFindSwidget("tg_extsky")) )
      	strcpy(s, find_short_help("EXTSKY"));

    if ( s[0] != '\0' ) {
    	UxPutText(UxFindSwidget("helptextModel"), s);
    }
}

/*--------- DisplayShortCMHelp --------------------------------------*/

void DisplayShortCMHelp( sw )
Widget sw; 	/* current widget */
{
    char s[160];
    char *find_short_help();
 
/* Main Window */
    if ( sw == UxGetWidget(UxFindSwidget("CM_apply")) )
      	strcpy(s, find_short_help("CMAPPLY"));

    if ( s[0] != '\0' ) {
    	UxPutText(UxFindSwidget("helptextCM"), s);
    }
}

/*------------ find_extended_help ----------------------------------*/

char *find_extended_help( key )
char *key;
{
    char midashome[512], midvers[80], help_pathname[512];
    char c, h, s[MAXLINE];
    FILE *fp;
    int idx = 0;

    osfphname("MIDVERS", midvers);
    osfphname("MIDASHOME", midashome);
    sprintf( help_pathname, "%s/%s/%s", midashome, midvers, HELP_FILE );

    if ( (fp = fopen(help_pathname, "r")) == NULL ) {
        sprintf(s, "XFilter Help file [%s] not found.\r\n", help_pathname);
	SCTPUT(s);
        return("");
    }

    while ( (c = getc(fp)) != EOF )
	if ( c == HELP_DELIMITER && 
             !strncmp(key, fgets(s, MAXLINE, fp), strlen(key)) ) {
    	    while ( (h = getc(fp)) != EOF && h != HELP_DELIMITER )
		help_text[idx++] = h;
	    help_text[idx] = '\0';
            fclose(fp);
	    return(help_text);
	}
    fclose(fp);
    return("");

}
/*------------ news_filters ----------------------------------*/

char *news_filters()
{
    char filtersdir[512], news_pathname[512];
    char c, s[MAXLINE];
    FILE *fp;
    int idx = 0;

    osfphname("MID_FILTERS", filtersdir);
    sprintf( news_pathname, "%s/%s", filtersdir, NEWS_FILE );

    if ( (fp = fopen(news_pathname, "r")) == NULL ) {
        sprintf(s, "XFilter News file [%s] not found.\r\n", news_pathname);
	SCTPUT(s);
        return("");
    }

    while ( (c = getc(fp)) != EOF )
	help_text[idx++] = c;

    help_text[idx] = '\0';
    fclose(fp);
    return(help_text);
}

/*------------ find_short_help ----------------------------------*/

char *find_short_help( key )
char *key;
{
    char midashome[512], midvers[80], help_pathname[512];
    char c, h, s[160];
    FILE *fp;
    int idx = 0;

    osfphname("MIDVERS", midvers);
    osfphname("MIDASHOME", midashome);
    sprintf( help_pathname, "%s/%s/%s", midashome, midvers, SHELP_FILE );

    if ( (fp = fopen(help_pathname, "r")) == NULL ) {
        sprintf(s, "XFilter Help file [%s] not found.\r\n", help_pathname);
	SCTPUT(s);
        return("");
    }

    while ( (c = getc(fp)) != EOF )
	if ( c == HELP_DELIMITER && 
             !strncmp(key, fgets(s, MAXLINE, fp), strlen(key)) ) {
    	    while ( (h = getc(fp)) != EOF && h != HELP_DELIMITER )
		shelp_text[idx++] = h;
	    shelp_text[idx] = '\0';
            fclose(fp);
	    return(shelp_text);
	}
    fclose(fp);
    return("");

}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        misc.c
.MODULE       subroutines
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      Miscelaneous functions:
		- Checking of the existence of a Unix file.
		- Checking of the Midas graphics window.
.KEYWORDS     file checking, graphics window
.VERSION 1.0  1-Apr-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/


/* check for the existence of 'file' with the extension 'ext' (incl. dot) */

int file_exists( file, ext )
char *file, *ext;
{
    char fileext[MAXLINE];
    int i;
    struct stat statbuf;

    if ( file[0] == '\0' )
	return(FALSE);

    for ( i = 0; file[i] != '\0'; i++ )
	if ( file[i] == ' ' ) {
	    file[i] = '\0';
	    break;
	}

    if ( strstr(file, ext) == NULL )
    	sprintf( fileext, "%s%s", file, ext );
    else
	strcpy( fileext, file );

    if ( stat(fileext, &statbuf) == -1 )
	return(FALSE);

    return(TRUE);
}

int graphwin_exists()
{
    char unit[10];
    char file_old[MAXLINE], file[MAXLINE];
    char midwork[MAXLINE];

    osfphname("MID_WORK", midwork);
    osfphname("DAZUNIT", unit);
    unit[3] = '\0';

    sprintf(file_old, "%s%sXW", midwork, unit);
    sprintf(file, "%smidas_xw%s", midwork, unit);
    return(file_exists(file_old, ".soc") || file_exists(file_old, ".soc=") ||
           file_exists(file, "") || file_exists(file, "=") );
}

void PrintNews()
{
    strcpy(print_text, news_filters());
    PrintExtendedHelp();
}

void PrintExtendedHelp()
{
    char s[80*2];
    FILE *fp;

    fp = fopen(PRINT_FILE, "w");
    fprintf(fp, "%s", print_text);
    fclose(fp);

    sprintf(s, "%s %s", PRINT_COMMAND, PRINT_FILE);
    AppendDialogText(s);
}
