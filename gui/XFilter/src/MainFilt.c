/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

===========================================================================*/


/*******************************************************************************
	MainFilt.c


.VERSION
 090826         last modif

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "UxLib.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxSep.h"
#include "UxForm.h"
#include "UxTogBG.h"
#include "UxCascBG.h"
#include "UxSepG.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h> 
#include <global.h>
#include <model.h>

extern	struct s_gui	G;
extern  struct s_trans  T;
swidget ListPopup,ListMain;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxApplicWind;
	swidget	UxmeFilter;
	swidget	Uxme_f_select;
	swidget	Uxme_f_filt;
	swidget	Uxme_f_cw;
	swidget	Uxme_f_cwbw;
	swidget	Uxme_f_filterlist;
	swidget	Uxme_f_sepf;
	swidget	Uxme_f_ccd;
	swidget	Uxme_f_ccdlist;
	swidget	Uxmt_f_select;
	swidget	Uxme_f_frame;
	swidget	Uxme_f_trimx;
	swidget	Uxme_f_trimy;
	swidget	Uxme_f_unzoom;
	swidget	Uxme_f_redraw;
	swidget	Uxme_f_print;
	swidget	Uxmt_f_frame;
	swidget	Uxme_f_utils;
	swidget	Uxme_f_clg;
	swidget	Uxme_f_dg;
	swidget	Uxme_f_rd;
	swidget	Uxme_f_sep1;
	swidget	Uxme_f_pm;
	swidget	Uxme_f_sep2;
	swidget	Uxme_f_extract;
	swidget	Uxme_f_sep3;
	swidget	Uxme_f_list;
	swidget	Uxmt_f_utils;
	swidget	Uxme_f_label;
	swidget	Uxme_f_lblaxis;
	swidget	Uxme_f_lblcursor;
	swidget	Uxme_f_loadlbl;
	swidget	Uxme_f_clearlbl;
	swidget	Uxmt_f_label;
	swidget	Uxme_f_option;
	swidget	Uxme_f_plotoption;
	swidget	Uxme_f_plot;
	swidget	Uxme_f_oplot;
	swidget	Uxme_f_4plot;
	swidget	Uxme_f_9plot;
	swidget	Uxme_f_pmode;
	swidget	Uxme_f_traceoption;
	swidget	Uxme_f_line;
	swidget	Uxme_f_histo;
	swidget	Uxme_f_tmode;
	swidget	Uxmt_f_display;
	swidget	Uxme_f_help;
	swidget	Uxme_f_hfilter;
	swidget	Uxme_f_hhelp;
	swidget	Uxmt_f_help;
	swidget	Uxme_f_news;
	swidget	Uxme_f_txnews;
	swidget	UxmeFilter_top_b1;
	swidget	Uxme_f_quit;
	swidget	Uxme_f_bye;
	swidget	Uxmt_f_quit;
	swidget	Uxform1;
	swidget	Uxseparator6;
	swidget	UxMessage;
	swidget	Uxseparator1;
	swidget	Uxform2;
	swidget	UxEmmiButtonB;
	swidget	UxEfosc2Button;
	swidget	UxEfosc1Button;
	swidget	UxEmmiButtonR;
	swidget	UxSusiButton;
	swidget	UxSHelp;
	swidget	UxListWindow;
	swidget	UxscrolledList;
} _UxCApplicShell;

#define ApplicWind              UxApplicShellContext->UxApplicWind
#define meFilter                UxApplicShellContext->UxmeFilter
#define me_f_select             UxApplicShellContext->Uxme_f_select
#define me_f_filt               UxApplicShellContext->Uxme_f_filt
#define me_f_cw                 UxApplicShellContext->Uxme_f_cw
#define me_f_cwbw               UxApplicShellContext->Uxme_f_cwbw
#define me_f_filterlist         UxApplicShellContext->Uxme_f_filterlist
#define me_f_sepf               UxApplicShellContext->Uxme_f_sepf
#define me_f_ccd                UxApplicShellContext->Uxme_f_ccd
#define me_f_ccdlist            UxApplicShellContext->Uxme_f_ccdlist
#define mt_f_select             UxApplicShellContext->Uxmt_f_select
#define me_f_frame              UxApplicShellContext->Uxme_f_frame
#define me_f_trimx              UxApplicShellContext->Uxme_f_trimx
#define me_f_trimy              UxApplicShellContext->Uxme_f_trimy
#define me_f_unzoom             UxApplicShellContext->Uxme_f_unzoom
#define me_f_redraw             UxApplicShellContext->Uxme_f_redraw
#define me_f_print              UxApplicShellContext->Uxme_f_print
#define mt_f_frame              UxApplicShellContext->Uxmt_f_frame
#define me_f_utils              UxApplicShellContext->Uxme_f_utils
#define me_f_clg                UxApplicShellContext->Uxme_f_clg
#define me_f_dg                 UxApplicShellContext->Uxme_f_dg
#define me_f_rd                 UxApplicShellContext->Uxme_f_rd
#define me_f_sep1               UxApplicShellContext->Uxme_f_sep1
#define me_f_pm                 UxApplicShellContext->Uxme_f_pm
#define me_f_sep2               UxApplicShellContext->Uxme_f_sep2
#define me_f_extract            UxApplicShellContext->Uxme_f_extract
#define me_f_sep3               UxApplicShellContext->Uxme_f_sep3
#define me_f_list               UxApplicShellContext->Uxme_f_list
#define mt_f_utils              UxApplicShellContext->Uxmt_f_utils
#define me_f_label              UxApplicShellContext->Uxme_f_label
#define me_f_lblaxis            UxApplicShellContext->Uxme_f_lblaxis
#define me_f_lblcursor          UxApplicShellContext->Uxme_f_lblcursor
#define me_f_loadlbl            UxApplicShellContext->Uxme_f_loadlbl
#define me_f_clearlbl           UxApplicShellContext->Uxme_f_clearlbl
#define mt_f_label              UxApplicShellContext->Uxmt_f_label
#define me_f_option             UxApplicShellContext->Uxme_f_option
#define me_f_plotoption         UxApplicShellContext->Uxme_f_plotoption
#define me_f_plot               UxApplicShellContext->Uxme_f_plot
#define me_f_oplot              UxApplicShellContext->Uxme_f_oplot
#define me_f_4plot              UxApplicShellContext->Uxme_f_4plot
#define me_f_9plot              UxApplicShellContext->Uxme_f_9plot
#define me_f_pmode              UxApplicShellContext->Uxme_f_pmode
#define me_f_traceoption        UxApplicShellContext->Uxme_f_traceoption
#define me_f_line               UxApplicShellContext->Uxme_f_line
#define me_f_histo              UxApplicShellContext->Uxme_f_histo
#define me_f_tmode              UxApplicShellContext->Uxme_f_tmode
#define mt_f_display            UxApplicShellContext->Uxmt_f_display
#define me_f_help               UxApplicShellContext->Uxme_f_help
#define me_f_hfilter            UxApplicShellContext->Uxme_f_hfilter
#define me_f_hhelp              UxApplicShellContext->Uxme_f_hhelp
#define mt_f_help               UxApplicShellContext->Uxmt_f_help
#define me_f_news               UxApplicShellContext->Uxme_f_news
#define me_f_txnews             UxApplicShellContext->Uxme_f_txnews
#define meFilter_top_b1         UxApplicShellContext->UxmeFilter_top_b1
#define me_f_quit               UxApplicShellContext->Uxme_f_quit
#define me_f_bye                UxApplicShellContext->Uxme_f_bye
#define mt_f_quit               UxApplicShellContext->Uxmt_f_quit
#define form1                   UxApplicShellContext->Uxform1
#define separator6              UxApplicShellContext->Uxseparator6
#define Message                 UxApplicShellContext->UxMessage
#define separator1              UxApplicShellContext->Uxseparator1
#define form2                   UxApplicShellContext->Uxform2
#define EmmiButtonB             UxApplicShellContext->UxEmmiButtonB
#define Efosc2Button            UxApplicShellContext->UxEfosc2Button
#define Efosc1Button            UxApplicShellContext->UxEfosc1Button
#define EmmiButtonR             UxApplicShellContext->UxEmmiButtonR
#define SusiButton              UxApplicShellContext->UxSusiButton
#define SHelp                   UxApplicShellContext->UxSHelp
#define ListWindow              UxApplicShellContext->UxListWindow
#define scrolledList            UxApplicShellContext->UxscrolledList

static _UxCApplicShell	*UxApplicShellContext;

extern void ClearList(), DisplayShortMHelp(), DisplayExtendedHelp();
extern void plot_ccd(), plot4ccd(), plot4filter(), plot9filter();
extern void plot_filters(), end_filter();
extern void read_ccdchar(), search_cwav(), end_graphic();
extern void end_viewp(), search_cwbw(), def_inst(), set_model();
extern void Trim(), Unzoom();

extern char *news_filters();

extern int AppendDialogText();



swidget	ApplicShell;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*Help = "#override\n\
<Btn3Down>:HelpHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ApplicShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("SHelp"),"");
	 UxPutText(UxFindSwidget("HelpFShell"),"");
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	DisplayShortMHelp(UxWidget);
	UxApplicShellContext = UxSaveCtx;
}

static void	action_HelpHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_me_f_filt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern char currsel[];
	extern int filter_flag,lcwav,lcwbw,ccd_flag;
	
	ClearList();
	
	strcpy(currsel,"neso.dat");
	filter_flag=TRUE;
	lcwav=FALSE;
	lcwbw=FALSE;
	
	ccd_flag=FALSE;
	strcpy(G.sel_type,"esofilter");
	UxPutTitle( UxFindSwidget("AskShell"),"ESO Filter Selection");
	UxPutLabelString( UxFindSwidget("labelAsk"), "ESO filter number:" );
	UxPutText( UxFindSwidget("textAsk"),"");
	
	UxPopupInterface(UxFindSwidget("AskShell"),no_grab);
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_cw( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int filter_flag,lcwav,lcwbw,ccd_flag;
	extern char currsel[];
	extern float cw1,cw2;
	
	char	word[10];
	
	filter_flag=TRUE;
	lcwav=TRUE;
	lcwbw=FALSE;
	ccd_flag=FALSE;
	
	strcpy(currsel,"cwav.dat");
	sprintf(word,"%.1f",cw1);
	UxPutText(UxFindSwidget("textcwmin"),word);
	sprintf(word,"%.1f",cw2);
	UxPutText(UxFindSwidget("textcwmax"),word);
	
	UxPopupInterface(UxFindSwidget("WaveShell"),no_grab);
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_cwbw( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int filter_flag,lcwav,lcwbw,ccd_flag;
	extern char currsel[];
	extern float bw1,bw2,cw1,cw2;
	
	char	word[10];
	
	filter_flag=TRUE;
	lcwav=FALSE;
	lcwbw=TRUE;
	ccd_flag=FALSE;
	
	strcpy(currsel,"cwbw.dat");
	
	sprintf(word,"%.1f",cw1);
	UxPutText(UxFindSwidget("textcwmin1"),word);
	sprintf(word,"%.1f",cw2);
	UxPutText(UxFindSwidget("textcwmax1"),word);
	sprintf(word,"%.1f",bw1);
	UxPutText(UxFindSwidget("textbwmin"),word);
	sprintf(word,"%.1f",bw2);
	UxPutText(UxFindSwidget("textbwmax"),word);
	
	
	UxPopupInterface(UxFindSwidget("WaveBShell"),no_grab);
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_filterlist( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ccd_flag,filter_flag,lcwav,lcwbw;
	
	extern char currsel[];
	
	
	strcpy(currsel,"esofilter.dat");
	
	filter_flag=TRUE;
	
	ccd_flag=FALSE;
	lcwav=FALSE;
	lcwbw=FALSE;
	
	search_cwav(200.,1200.);
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_ccd( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ccd_flag,filter_flag,lcwav,lcwbw;
	extern char currsel[];
	
	ClearList();
	
	strcpy(currsel,"esoccd.dat");
	
	ccd_flag=TRUE;
	
	filter_flag=FALSE;
	lcwav=FALSE;
	lcwbw=FALSE;
	
	strcpy(G.sel_type,"esoccd");
	UxPutTitle( UxFindSwidget("AskShell"),"ESO CCD Selection");
	UxPutLabelString( UxFindSwidget("labelAsk"), "ESO CCD number:" );
	UxPutText( UxFindSwidget("textAsk"),"");
	
	UxPopupInterface(UxFindSwidget("AskShell"),no_grab);
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_ccdlist( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ccd_flag,filter_flag,lcwav,lcwbw;
	
	extern char currsel[];
	
	
	strcpy(currsel,"esoccd.dat");
	
	ccd_flag=TRUE;
	
	filter_flag=FALSE;
	lcwav=FALSE;
	lcwbw=FALSE;
	
	read_ccdchar();
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_trimx( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ltrimx,ltrimy;
	
	ltrimx=TRUE;
	ltrimy=FALSE;
	
	Trim();
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_trimy( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ltrimx,ltrimy;
	
	ltrimx=FALSE;
	ltrimy=TRUE;
	
	Trim();
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_unzoom( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ltrimx,ltrimy;
	
	ltrimx=FALSE;
	ltrimy=FALSE;
	
	Unzoom();
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_redraw( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ovpltgr;
	
	if(ovpltgr)
	       {
		end_graphic();
	       }
	AppendDialogText( "copy/graph G filter.plt \n" );
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_print( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	 extern int pltgr,putplt,putplt9,ovpltgr;
	extern char Printer[];
	
	char str[40];
	
	if(pltgr)
	    pltgr=FALSE;
	
	if(putplt)
	    putplt=FALSE;
	
	if(putplt9)
	    putplt9=FALSE;
	
	if(ovpltgr)
	         {
		  end_graphic();
	          ovpltgr=FALSE;
	         }
	sprintf( str, "copy/graph %s filter.plt", Printer );
	AppendDialogText(str);
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_clg( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ovpltgr,lfirstp;
	
	 if(ovpltgr)
	               {
	                lfirstp=TRUE;
			end_graphic();
	               }
	
	AppendDialogText( "clear/graph\n" );
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_dg( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ovpltgr,lfirstp;
	
	if(ovpltgr)
	       {
		lfirstp=TRUE;
		end_graphic();
	       }
	AppendDialogText( "delete/graph\n" );
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_rd( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int ovpltgr;
	 
	if(ovpltgr)
	       {
		end_graphic();
	       }
	AppendDialogText( "reset/display\n" );
	
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_pm( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern char Printer[];
	
	strcpy(G.sel_type,"printer");
	UxPutTitle( UxFindSwidget("AskShell"),"Printer Name");
	UxPutLabelString( UxFindSwidget("labelAsk"), "Printer Name:" );
	if ((int)strlen(Printer) < 1)
	UxPutText( UxFindSwidget("textAsk"), getenv("PRINTER") );
	else
	UxPutText( UxFindSwidget("textAsk"), Printer );
	
	UxPopupInterface(UxFindSwidget("AskShell"),no_grab);
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_extract( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	strcpy(G.sel_type,"extract_curve");
	UxPutText( UxFindSwidget("textAsk"),T.curvefile);
	UxPutTitle( UxFindSwidget("AskShell"),"Extract Curve Save Name");
	UxPutLabelString( UxFindSwidget("labelAsk"), "Filename (no ext.):" );
	
	UxPopupInterface(UxFindSwidget("AskShell"),no_grab);
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_list( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern char currsel[];
	char str[80];

	sprintf(str,"$ {syscoms(1:20)} {mid$prnt(3:52)} %s ",currsel);
	AppendDialogText(str);
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_lblaxis( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	 
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_lblcursor( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_loadlbl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	
	
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_clearlbl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_f_plot( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int plt4gr,pltgr,ovplt4gr,ovpltgr,putplt,putplt9,lfirstp;
	
	end_viewp();
	
	        plt4gr=FALSE;
	        pltgr=TRUE;
	        ovplt4gr=FALSE;
	        ovpltgr=FALSE;
		putplt=FALSE;
	        putplt9=FALSE;
	
	UxPutSet(UxFindSwidget("me_m_oplot"),"false");
	UxPutSet(UxFindSwidget("me_m_plot"),"true");
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_f_oplot( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int plt4gr,pltgr,ovplt4gr,ovpltgr,putplt,putplt9,lfirstp;
	extern int nc_ovplt;
	
	        if(ovpltgr && !lfirstp )
	         {
		end_graphic();
	         }
	
	        ovpltgr=TRUE;
	        ovplt4gr=FALSE;
	        pltgr=FALSE;
		putplt9=FALSE;
	        plt4gr=FALSE;
	        putplt=FALSE;
	        lfirstp=TRUE;
	        nc_ovplt=0;
	UxPutSet(UxFindSwidget("me_m_plot"),"false");
	UxPutSet(UxFindSwidget("me_m_oplot"),"true");
	
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_f_4plot( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int plt4gr,pltgr,ovplt4gr,ovpltgr,putplt,putplt9,lfirstp;
	extern int filter_flag,lcwav,lcwbw;
	extern int ccd_flag;
	extern int itab;
	
	extern float cw1,cw2,bw1,bw2;
	
	itab=0;
	if(filter_flag)
	        {
	        if(ovpltgr && !lfirstp )
	         {
		end_graphic();
	         }
	        putplt=TRUE;
	        pltgr=FALSE;
	        ovplt4gr=FALSE;
		putplt9=FALSE;
	        plt4gr=FALSE;
	        ovpltgr=FALSE;
	
	        if(lcwav)
	         search_cwav(cw1,cw2);
	        if(lcwbw)
	         search_cwbw(cw1,cw2,bw1,bw2);
	        }
	if(ccd_flag){
	
	        AppendDialogText( "write/out Plot 4 Selected CCD " );
	        putplt=TRUE;
	        pltgr=FALSE;
	        ovplt4gr=FALSE;
		putplt9=FALSE;
	        plt4gr=FALSE;
	        ovpltgr=FALSE;
	
	        read_ccdchar();
	        }
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_f_9plot( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int plt4gr,pltgr,ovplt4gr,ovpltgr,putplt,putplt9,lfirstp;
	extern int filter_flag,lcwav,lcwbw;
	extern int itab;
	extern float cw1,cw2,bw1,bw2;
	
	
	itab=0;
	
	if(filter_flag)
	        {
	        if(ovpltgr && !lfirstp )
	         {
		end_graphic();
	         }
	
	        putplt=FALSE;
	        pltgr=FALSE;
	        ovplt4gr=FALSE;
		putplt9=TRUE;
	        plt4gr=FALSE;
	        ovpltgr=FALSE;
	
	        if(lcwav)
	         search_cwav(cw1,cw2);
	        if(lcwbw)
	         search_cwbw(cw1,cw2,bw1,bw2);
	        }
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_f_line( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int tracemode;
	
	tracemode=FALSE;
	
	UxPutSet(UxFindSwidget("me_m_histo"),"false");
	UxPutSet(UxFindSwidget("me_m_line"),"true");
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	valueChangedCB_me_f_histo( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	extern int tracemode;
	
	tracemode=TRUE;
	
	UxPutSet(UxFindSwidget("me_m_histo"),"true");
	UxPutSet(UxFindSwidget("me_m_line"),"false");
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_hfilter( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	/*
	extern swidget HelpFShell;
	UxPopupInterface(UxFindSwidget("HelpFShell"),no_grab);
	UxPutText(UxFindSwidget("help_Text"),find_extended_help("CONTEXT"));
	*/
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_hhelp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_txnews( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	UxPopupInterface(UxFindSwidget("NewsShell"),no_grab);
	UxPutText(UxFindSwidget("tx_news"), news_filters());
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_me_f_bye( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	end_filter();
	exit(0);
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_EmmiButtonB( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	
	def_inst("emmib");
	
	set_model();
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_Efosc2Button( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	def_inst("efosc2");
	set_model();
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_Efosc1Button( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	def_inst("efosc1");
	set_model();
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_EmmiButtonR( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	def_inst("emmir");
	set_model();
	
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	activateCB_SusiButton( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	
	def_inst("susi");
	
	set_model();
	}
	UxApplicShellContext = UxSaveCtx;
}

static void	browseSelectionCB_scrolledList( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicShellContext;
	UxApplicShellContext = UxContext =
			(_UxCApplicShell *) UxGetContext( UxThisWidget );
	{
	char *choice,line[80];
	XmListCallbackStruct *cbs;
	int n_select;
	extern int pltgr,ovpltgr,putplt,putplt9;
	extern int ccd_flag,num_ccd;
	extern int filter_flag,gnfilt;
	extern int itab,tab4multi[4],tab9multi[9];
	
	
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	
	
	XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);
	
	strcpy(line,choice);
	
	if (sscanf(line,"%d",&n_select)!=0)
	{
	if (ccd_flag == TRUE) {
		num_ccd=n_select;
	
		if (putplt==FALSE)
			plot_ccd(num_ccd);
		else
			{
			tab4multi[itab++]=num_ccd;
			if(itab==4)
				{
				plot4ccd(tab4multi);
				itab=0;
				}
			}; 
		}
	else if (filter_flag == TRUE) { 
		gnfilt=n_select;
	
		if (putplt==TRUE)
			{
			tab4multi[itab++]=gnfilt;
			if(itab==4)
				{
				plot4filter(tab4multi);
				itab=0;
				}
			}
		else if (putplt9==TRUE)
			{
			tab9multi[itab++]=gnfilt;
			if(itab==9)
				{
				plot9filter(tab9multi);
				itab=0;
				}
			}
		else
			plot_filters(gnfilt);
		}
	
	}
	
	
	}
	UxApplicShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ApplicShell()
{
	UxPutSensitive( ApplicShell, "true" );
	UxPutY( ApplicShell, 4 );
	UxPutX( ApplicShell, 616 );
	UxPutBorderColor( ApplicShell, WindowBackground );
	UxPutBaseWidth( ApplicShell, 0 );
	UxPutBaseHeight( ApplicShell, 0 );
	UxPutTitle( ApplicShell, "Filter" );
	UxPutKeyboardFocusPolicy( ApplicShell, "pointer" );
	UxPutIconName( ApplicShell, "Filter" );
	UxPutHeight( ApplicShell, 550 );
	UxPutWidth( ApplicShell, 655 );

	UxPutBorderColor( ApplicWind, WindowBackground );
	UxPutBackground( ApplicWind, WindowBackground );
	UxPutHeight( ApplicWind, 370 );
	UxPutWidth( ApplicWind, 480 );
	UxPutY( ApplicWind, 0 );
	UxPutX( ApplicWind, 10 );
	UxPutUnitType( ApplicWind, "pixels" );

	UxPutForeground( meFilter, MenuForeground );
	UxPutBackground( meFilter, MenuBackground );
	UxPutBorderColor( meFilter, MenuBackground );
	UxPutMenuAccelerator( meFilter, "<KeyUp>F10" );
	UxPutRowColumnType( meFilter, "menu_bar" );

	UxPutForeground( me_f_select, MenuForeground );
	UxPutBorderColor( me_f_select, MenuBackground );
	UxPutBackground( me_f_select, MenuBackground );
	UxPutRowColumnType( me_f_select, "menu_pulldown" );

	UxPutFontList( me_f_filt, BoldTextFont );
	UxPutMnemonic( me_f_filt, "F" );
	UxPutLabelString( me_f_filt, "ESO Filter ..." );

	UxPutFontList( me_f_cw, BoldTextFont );
	UxPutMnemonic( me_f_cw, "W" );
	UxPutLabelString( me_f_cw, "Central Wavelength ..." );

	UxPutFontList( me_f_cwbw, BoldTextFont );
	UxPutMnemonic( me_f_cwbw, "B" );
	UxPutLabelString( me_f_cwbw, "Central Wave & Bandwidth ..." );

	UxPutFontList( me_f_filterlist, BoldTextFont );
	UxPutMnemonic( me_f_filterlist, "L" );
	UxPutLabelString( me_f_filterlist, "ESO Filter List" );

	UxPutFontList( me_f_ccd, BoldTextFont );
	UxPutMnemonic( me_f_ccd, "C" );
	UxPutLabelString( me_f_ccd, "ESO CCD ..." );

	UxPutFontList( me_f_ccdlist, BoldTextFont );
	UxPutMnemonic( me_f_ccdlist, "D" );
	UxPutLabelString( me_f_ccdlist, "ESO CCD List" );

	UxPutFontList( mt_f_select, BoldTextFont );
	UxPutMnemonic( mt_f_select, "S" );
	UxPutLabelString( mt_f_select, "Select" );

	UxPutForeground( me_f_frame, MenuForeground );
	UxPutBorderColor( me_f_frame, MenuBackground );
	UxPutBackground( me_f_frame, MenuBackground );
	UxPutRowColumnType( me_f_frame, "menu_pulldown" );

	UxPutFontList( me_f_trimx, BoldTextFont );
	UxPutMnemonic( me_f_trimx, "X" );
	UxPutLabelString( me_f_trimx, "Cut X" );

	UxPutFontList( me_f_trimy, BoldTextFont );
	UxPutMnemonic( me_f_trimy, "Y" );
	UxPutLabelString( me_f_trimy, "Cut Y" );

	UxPutFontList( me_f_unzoom, BoldTextFont );
	UxPutMnemonic( me_f_unzoom, "U" );
	UxPutLabelString( me_f_unzoom, "Unzoom" );

	UxPutFontList( me_f_redraw, BoldTextFont );
	UxPutMnemonic( me_f_redraw, "R" );
	UxPutLabelString( me_f_redraw, "Redraw" );

	UxPutFontList( me_f_print, BoldTextFont );
	UxPutMnemonic( me_f_print, "P" );
	UxPutLabelString( me_f_print, "Print" );

	UxPutFontList( mt_f_frame, BoldTextFont );
	UxPutMnemonic( mt_f_frame, "F" );
	UxPutLabelString( mt_f_frame, "Frame" );

	UxPutForeground( me_f_utils, MenuForeground );
	UxPutBorderColor( me_f_utils, MenuBackground );
	UxPutBackground( me_f_utils, MenuBackground );
	UxPutRowColumnType( me_f_utils, "menu_pulldown" );

	UxPutFontList( me_f_clg, BoldTextFont );
	UxPutMnemonic( me_f_clg, "C" );
	UxPutLabelString( me_f_clg, "Clear Graphics" );

	UxPutFontList( me_f_dg, BoldTextFont );
	UxPutMnemonic( me_f_dg, "D" );
	UxPutLabelString( me_f_dg, "Delete Graphics" );

	UxPutFontList( me_f_rd, BoldTextFont );
	UxPutMnemonic( me_f_rd, "R" );
	UxPutLabelString( me_f_rd, "Reset Display" );

	UxPutFontList( me_f_pm, BoldTextFont );
	UxPutMnemonic( me_f_pm, "P" );
	UxPutLabelString( me_f_pm, "Default Printer ..." );

	UxPutFontList( me_f_extract, BoldTextFont );
	UxPutMnemonic( me_f_extract, "E" );
	UxPutLabelString( me_f_extract, "Extract Curve ..." );

	UxPutFontList( me_f_list, BoldTextFont );
	UxPutMnemonic( me_f_list, "L" );
	UxPutLabelString( me_f_list, "Print List " );

	UxPutFontList( mt_f_utils, BoldTextFont );
	UxPutMnemonic( mt_f_utils, "U" );
	UxPutLabelString( mt_f_utils, "Utils" );

	UxPutBorderColor( me_f_label, MenuBackground );
	UxPutBackground( me_f_label, MenuBackground );
	UxPutForeground( me_f_label, MenuForeground );
	UxPutRowColumnType( me_f_label, "menu_pulldown" );

	UxPutFontList( me_f_lblaxis, BoldTextFont );
	UxPutMnemonic( me_f_lblaxis, "A" );
	UxPutLabelString( me_f_lblaxis, "Label Axis" );

	UxPutFontList( me_f_lblcursor, BoldTextFont );
	UxPutMnemonic( me_f_lblcursor, "C" );
	UxPutLabelString( me_f_lblcursor, "Label Cursor" );

	UxPutFontList( me_f_loadlbl, BoldTextFont );
	UxPutMnemonic( me_f_loadlbl, "F" );
	UxPutLabelString( me_f_loadlbl, "Load Label File" );

	UxPutFontList( me_f_clearlbl, BoldTextFont );
	UxPutMnemonic( me_f_clearlbl, "C" );
	UxPutLabelString( me_f_clearlbl, "Clear Labels" );

	UxPutSensitive( mt_f_label, "false" );
	UxPutFontList( mt_f_label, BoldTextFont );
	UxPutMnemonic( mt_f_label, "L" );
	UxPutLabelString( mt_f_label, "Label" );

	UxPutForeground( me_f_option, MenuForeground );
	UxPutBorderColor( me_f_option, MenuBackground );
	UxPutBackground( me_f_option, MenuBackground );
	UxPutRowColumnType( me_f_option, "menu_pulldown" );

	UxPutRadioBehavior( me_f_plotoption, "true" );
	UxPutForeground( me_f_plotoption, MenuForeground );
	UxPutBorderColor( me_f_plotoption, MenuBackground );
	UxPutBackground( me_f_plotoption, MenuBackground );
	UxPutRowColumnType( me_f_plotoption, "menu_pulldown" );

	UxPutMnemonic( me_f_plot, "P" );
	UxPutFontList( me_f_plot, BoldTextFont );
	UxPutSet( me_f_plot, "true" );
	UxPutSelectColor( me_f_plot, SelectColor );
	UxPutLabelString( me_f_plot, "Plot one filter" );

	UxPutMnemonic( me_f_oplot, "O" );
	UxPutSelectColor( me_f_oplot, SelectColor );
	UxPutFontList( me_f_oplot, BoldTextFont );
	UxPutSet( me_f_oplot, "false" );
	UxPutLabelString( me_f_oplot, "Overplot filters" );

	UxPutMnemonic( me_f_4plot, "4" );
	UxPutFontList( me_f_4plot, BoldTextFont );
	UxPutSet( me_f_4plot, "false" );
	UxPutSelectColor( me_f_4plot, SelectColor );
	UxPutLabelString( me_f_4plot, "Plot 4 filters" );

	UxPutMnemonic( me_f_9plot, "9" );
	UxPutFontList( me_f_9plot, BoldTextFont );
	UxPutSet( me_f_9plot, "false" );
	UxPutSelectColor( me_f_9plot, SelectColor );
	UxPutLabelString( me_f_9plot, "Plot 9 filters" );

	UxPutFontList( me_f_pmode, BoldTextFont );
	UxPutLabelString( me_f_pmode, "Plot mode " );

	UxPutRadioBehavior( me_f_traceoption, "true" );
	UxPutForeground( me_f_traceoption, MenuForeground );
	UxPutBorderColor( me_f_traceoption, MenuBackground );
	UxPutBackground( me_f_traceoption, MenuBackground );
	UxPutRowColumnType( me_f_traceoption, "menu_pulldown" );

	UxPutSet( me_f_line, "true" );
	UxPutSelectColor( me_f_line, SelectColor );
	UxPutFontList( me_f_line, BoldTextFont );
	UxPutMnemonic( me_f_line, "S" );
	UxPutLabelString( me_f_line, "Straight line" );

	UxPutSet( me_f_histo, "false" );
	UxPutSelectColor( me_f_histo, SelectColor );
	UxPutFontList( me_f_histo, BoldTextFont );
	UxPutMnemonic( me_f_histo, "H" );
	UxPutLabelString( me_f_histo, "Histogram line" );

	UxPutFontList( me_f_tmode, BoldTextFont );
	UxPutLabelString( me_f_tmode, "Trace mode " );

	UxPutFontList( mt_f_display, BoldTextFont );
	UxPutMnemonic( mt_f_display, "O" );
	UxPutLabelString( mt_f_display, "Options" );

	UxPutForeground( me_f_help, MenuForeground );
	UxPutBorderColor( me_f_help, MenuBackground );
	UxPutBackground( me_f_help, MenuBackground );
	UxPutRowColumnType( me_f_help, "menu_pulldown" );

	UxPutFontList( me_f_hfilter, BoldTextFont );
	UxPutMnemonic( me_f_hfilter, "X" );
	UxPutLabelString( me_f_hfilter, "On Xfilter ..." );

	UxPutFontList( me_f_hhelp, BoldTextFont );
	UxPutMnemonic( me_f_hhelp, "H" );
	UxPutLabelString( me_f_hhelp, "On Help ..." );

	UxPutFontList( mt_f_help, BoldTextFont );
	UxPutMnemonic( mt_f_help, "H" );
	UxPutLabelString( mt_f_help, "Help" );

	UxPutForeground( me_f_news, MenuForeground );
	UxPutBackground( me_f_news, MenuBackground );
	UxPutRowColumnType( me_f_news, "menu_pulldown" );

	UxPutFontList( me_f_txnews, BoldTextFont );
	UxPutMnemonic( me_f_txnews, "N" );
	UxPutLabelString( me_f_txnews, "News about filters ..." );

	UxPutFontList( meFilter_top_b1, BoldTextFont );
	UxPutMnemonic( meFilter_top_b1, "H" );
	UxPutLabelString( meFilter_top_b1, "News" );

	UxPutForeground( me_f_quit, MenuForeground );
	UxPutBorderColor( me_f_quit, MenuBackground );
	UxPutBackground( me_f_quit, MenuBackground );
	UxPutRowColumnType( me_f_quit, "menu_pulldown" );

	UxPutFontList( me_f_bye, BoldTextFont );
	UxPutMnemonic( me_f_bye, "B" );
	UxPutLabelString( me_f_bye, "Bye" );

	UxPutFontList( mt_f_quit, BoldTextFont );
	UxPutMnemonic( mt_f_quit, "Q" );
	UxPutLabelString( mt_f_quit, "Quit" );

	UxPutResizePolicy( form1, "resize_none" );
	UxPutButtonFontList( form1, BoldTextFont );
	UxPutBorderColor( form1, WindowBackground );
	UxPutBackground( form1, WindowBackground );

	UxPutSeparatorType( separator6, "shadow_etched_in" );
	UxPutMappedWhenManaged( separator6, "true" );
	UxPutSensitive( separator6, "false" );
	UxPutAncestorSensitive( separator6, "false" );
	UxPutBorderColor( separator6, LabelBackground );
	UxPutBackground( separator6, LabelBackground );
	UxPutWidth( separator6, 508 );
	UxPutY( separator6, 3 );
	UxPutX( separator6, 0 );

	UxPutAlignment( Message, "alignment_beginning" );
	UxPutRecomputeSize( Message, "false" );
	UxPutHeight( Message, 45 );
	UxPutFontList( Message, "9x15bold" );
	UxPutBorderColor( Message, LabelBackground );
	UxPutBackground( Message, LabelBackground );

	UxPutBorderColor( separator1, LabelBackground );
	UxPutBackground( separator1, LabelBackground );

	UxPutHeight( form2, 70 );
	UxPutTextFontList( form2, TextFont );
	UxPutLabelFontList( form2, TextFont );
	UxPutBorderColor( form2, ButtonBackground );
	UxPutBackground( form2, ButtonBackground );

	UxPutTranslations( EmmiButtonB, Help );
	UxPutLabelString( EmmiButtonB, "EMMI B ..." );
	UxPutForeground( EmmiButtonB, ButtonForeground );
	UxPutFontList( EmmiButtonB, BoldTextFont );
	UxPutBorderColor( EmmiButtonB, ButtonBackground );
	UxPutBackground( EmmiButtonB, ButtonBackground );
	UxPutHeight( EmmiButtonB, 30 );
	UxPutWidth( EmmiButtonB, 100 );
	UxPutY( EmmiButtonB, 10 );
	UxPutX( EmmiButtonB, 15 );

	UxPutTranslations( Efosc2Button, Help );
	UxPutLabelString( Efosc2Button, "EFOSC 2 ..." );
	UxPutForeground( Efosc2Button, ButtonForeground );
	UxPutFontList( Efosc2Button, BoldTextFont );
	UxPutBorderColor( Efosc2Button, ButtonBackground );
	UxPutBackground( Efosc2Button, ButtonBackground );
	UxPutHeight( Efosc2Button, 30 );
	UxPutWidth( Efosc2Button, 100 );
	UxPutY( Efosc2Button, 8 );
	UxPutX( Efosc2Button, 542 );

	UxPutTranslations( Efosc1Button, Help );
	UxPutLabelString( Efosc1Button, "EFOSC 1 ..." );
	UxPutForeground( Efosc1Button, ButtonForeground );
	UxPutFontList( Efosc1Button, BoldTextFont );
	UxPutBorderColor( Efosc1Button, ButtonBackground );
	UxPutBackground( Efosc1Button, ButtonBackground );
	UxPutHeight( Efosc1Button, 30 );
	UxPutWidth( Efosc1Button, 100 );
	UxPutY( Efosc1Button, 8 );
	UxPutX( Efosc1Button, 422 );

	UxPutTranslations( EmmiButtonR, Help );
	UxPutLabelString( EmmiButtonR, "EMMI R ..." );
	UxPutForeground( EmmiButtonR, ButtonForeground );
	UxPutFontList( EmmiButtonR, BoldTextFont );
	UxPutBorderColor( EmmiButtonR, ButtonBackground );
	UxPutBackground( EmmiButtonR, ButtonBackground );
	UxPutHeight( EmmiButtonR, 30 );
	UxPutWidth( EmmiButtonR, 100 );
	UxPutY( EmmiButtonR, 11 );
	UxPutX( EmmiButtonR, 79 );

	UxPutTranslations( SusiButton, Help );
	UxPutLabelString( SusiButton, "SUSI ..." );
	UxPutForeground( SusiButton, ButtonForeground );
	UxPutFontList( SusiButton, BoldTextFont );
	UxPutBorderColor( SusiButton, ButtonBackground );
	UxPutBackground( SusiButton, ButtonBackground );
	UxPutHeight( SusiButton, 30 );
	UxPutWidth( SusiButton, 100 );
	UxPutY( SusiButton, 42 );
	UxPutX( SusiButton, 10 );

	UxPutFontList( SHelp, TextFont );
	UxPutBorderColor( SHelp, SHelpBackground );
	UxPutBackground( SHelp, SHelpBackground );
	UxPutHeight( SHelp, 50 );

	UxPutScrollBarPlacement( ListWindow, "bottom_left" );
	UxPutForeground( ListWindow, TextForeground );
	UxPutBottomShadowColor( ListWindow, WindowBackground );
	UxPutBorderColor( ListWindow, WindowBackground );
	UxPutBackground( ListWindow, WindowBackground );
	UxPutShadowThickness( ListWindow, 0 );
	UxPutScrollBarDisplayPolicy( ListWindow, "static" );
	UxPutVisualPolicy( ListWindow, "variable" );
	UxPutScrollingPolicy( ListWindow, "application_defined" );

	UxPutVisibleItemCount( scrolledList, 16 );
	UxPutScrollBarDisplayPolicy( scrolledList, "static" );
	UxPutListSizePolicy( scrolledList, "constant" );
	UxPutForeground( scrolledList, TextForeground );
	UxPutFontList( scrolledList, "9x15" );
	UxPutBorderColor( scrolledList, SHelpBackground );
	UxPutBackground( scrolledList, SHelpBackground );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ApplicShell()
{
	/* Create the swidgets */

	ApplicShell = UxCreateApplicationShell( "ApplicShell", NO_PARENT );
	UxPutContext( ApplicShell, UxApplicShellContext );

	ApplicWind = UxCreateMainWindow( "ApplicWind", ApplicShell );
	meFilter = UxCreateRowColumn( "meFilter", ApplicWind );
	me_f_select = UxCreateRowColumn( "me_f_select", meFilter );
	me_f_filt = UxCreatePushButtonGadget( "me_f_filt", me_f_select );
	me_f_cw = UxCreatePushButtonGadget( "me_f_cw", me_f_select );
	me_f_cwbw = UxCreatePushButtonGadget( "me_f_cwbw", me_f_select );
	me_f_filterlist = UxCreatePushButtonGadget( "me_f_filterlist", me_f_select );
	me_f_sepf = UxCreateSeparatorGadget( "me_f_sepf", me_f_select );
	me_f_ccd = UxCreatePushButtonGadget( "me_f_ccd", me_f_select );
	me_f_ccdlist = UxCreatePushButtonGadget( "me_f_ccdlist", me_f_select );
	mt_f_select = UxCreateCascadeButtonGadget( "mt_f_select", meFilter );
	me_f_frame = UxCreateRowColumn( "me_f_frame", meFilter );
	me_f_trimx = UxCreatePushButtonGadget( "me_f_trimx", me_f_frame );
	me_f_trimy = UxCreatePushButtonGadget( "me_f_trimy", me_f_frame );
	me_f_unzoom = UxCreatePushButtonGadget( "me_f_unzoom", me_f_frame );
	me_f_redraw = UxCreatePushButtonGadget( "me_f_redraw", me_f_frame );
	me_f_print = UxCreatePushButtonGadget( "me_f_print", me_f_frame );
	mt_f_frame = UxCreateCascadeButtonGadget( "mt_f_frame", meFilter );
	me_f_utils = UxCreateRowColumn( "me_f_utils", meFilter );
	me_f_clg = UxCreatePushButtonGadget( "me_f_clg", me_f_utils );
	me_f_dg = UxCreatePushButtonGadget( "me_f_dg", me_f_utils );
	me_f_rd = UxCreatePushButtonGadget( "me_f_rd", me_f_utils );
	me_f_sep1 = UxCreateSeparatorGadget( "me_f_sep1", me_f_utils );
	me_f_pm = UxCreatePushButtonGadget( "me_f_pm", me_f_utils );
	me_f_sep2 = UxCreateSeparatorGadget( "me_f_sep2", me_f_utils );
	me_f_extract = UxCreatePushButtonGadget( "me_f_extract", me_f_utils );
	me_f_sep3 = UxCreateSeparatorGadget( "me_f_sep3", me_f_utils );
	me_f_list = UxCreatePushButtonGadget( "me_f_list", me_f_utils );
	mt_f_utils = UxCreateCascadeButtonGadget( "mt_f_utils", meFilter );
	me_f_label = UxCreateRowColumn( "me_f_label", meFilter );
	me_f_lblaxis = UxCreatePushButtonGadget( "me_f_lblaxis", me_f_label );
	me_f_lblcursor = UxCreatePushButtonGadget( "me_f_lblcursor", me_f_label );
	me_f_loadlbl = UxCreatePushButtonGadget( "me_f_loadlbl", me_f_label );
	me_f_clearlbl = UxCreatePushButtonGadget( "me_f_clearlbl", me_f_label );
	mt_f_label = UxCreateCascadeButtonGadget( "mt_f_label", meFilter );
	me_f_option = UxCreateRowColumn( "me_f_option", meFilter );
	me_f_plotoption = UxCreateRowColumn( "me_f_plotoption", me_f_option );
	me_f_plot = UxCreateToggleButtonGadget( "me_f_plot", me_f_plotoption );
	me_f_oplot = UxCreateToggleButtonGadget( "me_f_oplot", me_f_plotoption );
	me_f_4plot = UxCreateToggleButtonGadget( "me_f_4plot", me_f_plotoption );
	me_f_9plot = UxCreateToggleButtonGadget( "me_f_9plot", me_f_plotoption );
	me_f_pmode = UxCreateCascadeButtonGadget( "me_f_pmode", me_f_option );
	me_f_traceoption = UxCreateRowColumn( "me_f_traceoption", me_f_option );
	me_f_line = UxCreateToggleButtonGadget( "me_f_line", me_f_traceoption );
	me_f_histo = UxCreateToggleButtonGadget( "me_f_histo", me_f_traceoption );
	me_f_tmode = UxCreateCascadeButtonGadget( "me_f_tmode", me_f_option );
	mt_f_display = UxCreateCascadeButtonGadget( "mt_f_display", meFilter );
	me_f_help = UxCreateRowColumn( "me_f_help", meFilter );
	me_f_hfilter = UxCreatePushButtonGadget( "me_f_hfilter", me_f_help );
	me_f_hhelp = UxCreatePushButtonGadget( "me_f_hhelp", me_f_help );
	mt_f_help = UxCreateCascadeButtonGadget( "mt_f_help", meFilter );
	me_f_news = UxCreateRowColumn( "me_f_news", meFilter );
	me_f_txnews = UxCreatePushButtonGadget( "me_f_txnews", me_f_news );
	meFilter_top_b1 = UxCreateCascadeButtonGadget( "meFilter_top_b1", meFilter );
	me_f_quit = UxCreateRowColumn( "me_f_quit", meFilter );
	me_f_bye = UxCreatePushButtonGadget( "me_f_bye", me_f_quit );
	mt_f_quit = UxCreateCascadeButtonGadget( "mt_f_quit", meFilter );
	form1 = UxCreateForm( "form1", ApplicWind );
	separator6 = UxCreateSeparator( "separator6", form1 );
	Message = UxCreateLabel( "Message", form1 );
	separator1 = UxCreateSeparator( "separator1", form1 );
	form2 = UxCreateForm( "form2", form1 );
	EmmiButtonB = UxCreatePushButton( "EmmiButtonB", form2 );
	Efosc2Button = UxCreatePushButton( "Efosc2Button", form2 );
	Efosc1Button = UxCreatePushButton( "Efosc1Button", form2 );
	EmmiButtonR = UxCreatePushButton( "EmmiButtonR", form2 );
	SusiButton = UxCreatePushButton( "SusiButton", form2 );
	SHelp = UxCreateText( "SHelp", form1 );
	ListWindow = UxCreateScrolledWindow( "ListWindow", form1 );
	scrolledList = UxCreateScrolledList( "scrolledList", ListWindow );

	_Uxinit_ApplicShell();

	/* Create the X widgets */

	UxCreateWidget( ApplicShell );
	UxCreateWidget( ApplicWind );
	UxCreateWidget( meFilter );
	UxCreateWidget( me_f_select );
	UxCreateWidget( me_f_filt );
	UxCreateWidget( me_f_cw );
	UxCreateWidget( me_f_cwbw );
	UxCreateWidget( me_f_filterlist );
	UxCreateWidget( me_f_sepf );
	UxCreateWidget( me_f_ccd );
	UxCreateWidget( me_f_ccdlist );
	UxPutSubMenuId( mt_f_select, "me_f_select" );
	UxCreateWidget( mt_f_select );

	UxCreateWidget( me_f_frame );
	UxCreateWidget( me_f_trimx );
	UxCreateWidget( me_f_trimy );
	UxCreateWidget( me_f_unzoom );
	UxCreateWidget( me_f_redraw );
	UxCreateWidget( me_f_print );
	UxPutSubMenuId( mt_f_frame, "me_f_frame" );
	UxCreateWidget( mt_f_frame );

	UxCreateWidget( me_f_utils );
	UxCreateWidget( me_f_clg );
	UxCreateWidget( me_f_dg );
	UxCreateWidget( me_f_rd );
	UxCreateWidget( me_f_sep1 );
	UxCreateWidget( me_f_pm );
	UxCreateWidget( me_f_sep2 );
	UxCreateWidget( me_f_extract );
	UxCreateWidget( me_f_sep3 );
	UxCreateWidget( me_f_list );
	UxPutSubMenuId( mt_f_utils, "me_f_utils" );
	UxCreateWidget( mt_f_utils );

	UxCreateWidget( me_f_label );
	UxCreateWidget( me_f_lblaxis );
	UxCreateWidget( me_f_lblcursor );
	UxCreateWidget( me_f_loadlbl );
	UxCreateWidget( me_f_clearlbl );
	UxPutSubMenuId( mt_f_label, "me_f_label" );
	UxCreateWidget( mt_f_label );

	UxCreateWidget( me_f_option );
	UxCreateWidget( me_f_plotoption );
	UxCreateWidget( me_f_plot );
	UxCreateWidget( me_f_oplot );
	UxCreateWidget( me_f_4plot );
	UxCreateWidget( me_f_9plot );
	UxPutSubMenuId( me_f_pmode, "me_f_plotoption" );
	UxCreateWidget( me_f_pmode );

	UxCreateWidget( me_f_traceoption );
	UxCreateWidget( me_f_line );
	UxCreateWidget( me_f_histo );
	UxPutSubMenuId( me_f_tmode, "me_f_traceoption" );
	UxCreateWidget( me_f_tmode );

	UxPutSubMenuId( mt_f_display, "me_f_option" );
	UxCreateWidget( mt_f_display );

	UxCreateWidget( me_f_help );
	UxCreateWidget( me_f_hfilter );
	UxCreateWidget( me_f_hhelp );
	UxPutSubMenuId( mt_f_help, "me_f_help" );
	UxCreateWidget( mt_f_help );

	UxCreateWidget( me_f_news );
	UxCreateWidget( me_f_txnews );
	UxPutSubMenuId( meFilter_top_b1, "me_f_news" );
	UxCreateWidget( meFilter_top_b1 );

	UxCreateWidget( me_f_quit );
	UxCreateWidget( me_f_bye );
	UxPutSubMenuId( mt_f_quit, "me_f_quit" );
	UxCreateWidget( mt_f_quit );

	UxCreateWidget( form1 );
	UxPutLeftAttachment( separator6, "attach_form" );
	UxPutTopOffset( separator6, 3 );
	UxPutTopAttachment( separator6, "attach_form" );
	UxPutRightAttachment( separator6, "attach_form" );
	UxPutLeftOffset( separator6, 0 );
	UxPutBottomOffset( separator6, 3 );
	UxPutBottomAttachment( separator6, "attach_none" );
	UxCreateWidget( separator6 );

	UxPutTopWidget( Message, "separator6" );
	UxPutTopOffset( Message, 0 );
	UxPutLeftAttachment( Message, "attach_form" );
	UxPutResizable( Message, "false" );
	UxPutTopAttachment( Message, "attach_widget" );
	UxPutRightOffset( Message, 5 );
	UxPutRightAttachment( Message, "attach_form" );
	UxPutLeftOffset( Message, 25 );
	UxPutBottomOffset( Message, 0 );
	UxPutBottomAttachment( Message, "attach_none" );
	UxCreateWidget( Message );

	UxPutLeftAttachment( separator1, "attach_form" );
	UxPutTopWidget( separator1, "Message" );
	UxPutTopAttachment( separator1, "attach_widget" );
	UxPutRightAttachment( separator1, "attach_form" );
	UxCreateWidget( separator1 );

	UxPutBottomOffset( form2, 2 );
	UxPutRightAttachment( form2, "attach_form" );
	UxPutLeftAttachment( form2, "attach_form" );
	UxPutResizable( form2, "true" );
	UxPutBottomAttachment( form2, "attach_form" );
	UxCreateWidget( form2 );

	UxPutLeftOffset( EmmiButtonB, 10 );
	UxPutLeftAttachment( EmmiButtonB, "attach_form" );
	UxPutTopOffset( EmmiButtonB, 5 );
	UxPutTopAttachment( EmmiButtonB, "attach_form" );
	UxCreateWidget( EmmiButtonB );

	UxPutLeftOffset( Efosc2Button, 430 );
	UxPutLeftAttachment( Efosc2Button, "attach_form" );
	UxPutRightOffset( Efosc2Button, 210 );
	UxPutRightAttachment( Efosc2Button, "attach_none" );
	UxPutTopOffset( Efosc2Button, 5 );
	UxPutTopAttachment( Efosc2Button, "attach_form" );
	UxCreateWidget( Efosc2Button );

	UxPutLeftOffset( Efosc1Button, 325 );
	UxPutLeftAttachment( Efosc1Button, "attach_form" );
	UxPutRightOffset( Efosc1Button, 110 );
	UxPutRightAttachment( Efosc1Button, "attach_none" );
	UxPutTopOffset( Efosc1Button, 5 );
	UxPutTopAttachment( Efosc1Button, "attach_form" );
	UxCreateWidget( Efosc1Button );

	UxPutLeftAttachment( EmmiButtonR, "attach_form" );
	UxPutLeftOffset( EmmiButtonR, 115 );
	UxPutTopOffset( EmmiButtonR, 5 );
	UxPutTopAttachment( EmmiButtonR, "attach_form" );
	UxCreateWidget( EmmiButtonR );

	UxPutTopOffset( SusiButton, 5 );
	UxPutTopAttachment( SusiButton, "attach_form" );
	UxPutLeftOffset( SusiButton, 220 );
	UxPutLeftAttachment( SusiButton, "attach_form" );
	UxPutBottomOffset( SusiButton, 5 );
	UxPutBottomAttachment( SusiButton, "attach_none" );
	UxCreateWidget( SusiButton );

	UxPutBottomWidget( SHelp, "form2" );
	UxPutLeftOffset( SHelp, 2 );
	UxPutLeftAttachment( SHelp, "attach_form" );
	UxPutTopAttachment( SHelp, "attach_none" );
	UxPutRightOffset( SHelp, 2 );
	UxPutRightAttachment( SHelp, "attach_form" );
	UxPutBottomOffset( SHelp, 2 );
	UxPutBottomAttachment( SHelp, "attach_widget" );
	UxCreateWidget( SHelp );

	UxPutLeftOffset( ListWindow, 1 );
	UxPutRightOffset( ListWindow, 1 );
	UxPutBottomWidget( ListWindow, "SHelp" );
	UxPutBottomOffset( ListWindow, 1 );
	UxPutBottomAttachment( ListWindow, "attach_widget" );
	UxPutTopWidget( ListWindow, "separator1" );
	UxPutTopOffset( ListWindow, 1 );
	UxPutTopAttachment( ListWindow, "attach_widget" );
	UxPutRightAttachment( ListWindow, "attach_form" );
	UxPutLeftAttachment( ListWindow, "attach_form" );
	UxCreateWidget( ListWindow );

	UxCreateWidget( scrolledList );

	UxPutIconPixmap( ApplicShell, "/usr/include/X11/bitmaps/boxes" );

	UxPutMenuHelpWidget( meFilter, "mt_f_quit" );

	UxAddCallback( me_f_filt, XmNactivateCallback,
			activateCB_me_f_filt,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_cw, XmNactivateCallback,
			activateCB_me_f_cw,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_cwbw, XmNactivateCallback,
			activateCB_me_f_cwbw,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_filterlist, XmNactivateCallback,
			activateCB_me_f_filterlist,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_ccd, XmNactivateCallback,
			activateCB_me_f_ccd,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_ccdlist, XmNactivateCallback,
			activateCB_me_f_ccdlist,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_trimx, XmNactivateCallback,
			activateCB_me_f_trimx,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_trimy, XmNactivateCallback,
			activateCB_me_f_trimy,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_unzoom, XmNactivateCallback,
			activateCB_me_f_unzoom,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_redraw, XmNactivateCallback,
			activateCB_me_f_redraw,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_print, XmNactivateCallback,
			activateCB_me_f_print,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_clg, XmNactivateCallback,
			activateCB_me_f_clg,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_dg, XmNactivateCallback,
			activateCB_me_f_dg,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_rd, XmNactivateCallback,
			activateCB_me_f_rd,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_pm, XmNactivateCallback,
			activateCB_me_f_pm,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_extract, XmNactivateCallback,
			activateCB_me_f_extract,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_list, XmNactivateCallback,
			activateCB_me_f_list,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_lblaxis, XmNactivateCallback,
			activateCB_me_f_lblaxis,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_lblcursor, XmNactivateCallback,
			activateCB_me_f_lblcursor,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_loadlbl, XmNactivateCallback,
			activateCB_me_f_loadlbl,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_clearlbl, XmNactivateCallback,
			activateCB_me_f_clearlbl,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_plot, XmNvalueChangedCallback,
			valueChangedCB_me_f_plot,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_oplot, XmNvalueChangedCallback,
			valueChangedCB_me_f_oplot,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_4plot, XmNvalueChangedCallback,
			valueChangedCB_me_f_4plot,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_9plot, XmNvalueChangedCallback,
			valueChangedCB_me_f_9plot,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_line, XmNvalueChangedCallback,
			valueChangedCB_me_f_line,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_histo, XmNvalueChangedCallback,
			valueChangedCB_me_f_histo,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_hfilter, XmNactivateCallback,
			activateCB_me_f_hfilter,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_hhelp, XmNactivateCallback,
			activateCB_me_f_hhelp,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_txnews, XmNactivateCallback,
			activateCB_me_f_txnews,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( me_f_bye, XmNactivateCallback,
			activateCB_me_f_bye,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( EmmiButtonB, XmNactivateCallback,
			activateCB_EmmiButtonB,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( Efosc2Button, XmNactivateCallback,
			activateCB_Efosc2Button,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( Efosc1Button, XmNactivateCallback,
			activateCB_Efosc1Button,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( EmmiButtonR, XmNactivateCallback,
			activateCB_EmmiButtonR,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( SusiButton, XmNactivateCallback,
			activateCB_SusiButton,
			(XtPointer) UxApplicShellContext );

	UxAddCallback( scrolledList, XmNbrowseSelectionCallback,
			browseSelectionCB_scrolledList,
			(XtPointer) UxApplicShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ApplicShell );

	UxMainWindowSetAreas( ApplicWind, meFilter, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, form1 );
	return ( ApplicShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ApplicShell()
{
	swidget                 rtrn;
	_UxCApplicShell         *UxContext;

	UxApplicShellContext = UxContext =
		(_UxCApplicShell *) UxMalloc( sizeof(_UxCApplicShell) );

	{
		swidget create_HelpFShell();
		swidget create_WaveBShell();
		swidget create_WaveShell();
		swidget create_ModelShell();
		swidget create_ChooseList();
		swidget create_AskShell();
		swidget create_BruzualModel();
		swidget create_FilterCreate();
		swidget create_BlackBody();
		swidget create_CloudyModel();
		swidget create_NewsShell();
		swidget create_LeaksShell();
		rtrn = _Uxbuild_ApplicShell();

		create_HelpFShell(); 
		create_WaveBShell();
		create_WaveShell();
		create_ModelShell();
		create_AskShell();
		create_BruzualModel();
		create_FilterCreate();
		create_BlackBody();
		create_CloudyModel();
		create_NewsShell();
		create_LeaksShell();
		
		ListPopup = create_ChooseList();
		
		ListMain = rtrn;
		
		return(rtrn);
	}
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ApplicShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "HelpHelp", action_HelpHelp }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ApplicShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

