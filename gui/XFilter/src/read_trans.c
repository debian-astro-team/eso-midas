/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY


.VERSION
 090826		last modif


===========================================================================*/


#include <stdio.h>
#include <string.h>
#include <globaldef.h>
#include <model.h>


extern int flire();



int read_trans (file, x, y, n, xmin, xmax)
char file[];
float x[npmax], y[npmax];
int *n;
float *xmin, *xmax;

{
register int i, j;
int ifile, ncom, letter;

float min, max;
float ymin, ymax;

char line[100];

FILE *IN;



IN = fopen (file, "r");
ifile = 0;

if ((IN != (FILE *) 0) && (fscanf (IN, "%s", line) != -1))
   {
   ifile = 1;
   rewind (IN);
   for (i = 0, letter = getc (IN);
        ((letter != 48) && (letter != 49) && (letter != 50)
         && (letter != 51) && (letter != 52) && (letter != 53)
         && (letter != 54) && (letter != 55) && (letter != 56)
         && (letter != 57) && (letter != 32)); i++)
      {
      flire (IN, line);
      letter = getc (IN);
      };
   ncom = i;
   flire (IN, line);
   fscanf (IN, "%f%e", &min, &ymin);
   max = min;
   ymax = ymin;

   rewind (IN);
   for (i = 0; i < ncom; i++, flire (IN, line));

   j = 0;
   while (fscanf (IN, "%f%e", &x[j], &y[j]) != -1)
      {
      flire (IN, line);
      min = Min (min, x[j]);
      max = Max (max, x[j]);
      ymin = Min (ymin, y[j]);
      ymax = Max (ymax, y[j]);
      j++;
      };
   *n = j;

   if ((*n) > npmax)
      fprintf (stderr, "\n\nERROR: number of data too large !!!\n\n");

   if ((ymax > 1.) && (ymax < 100.))
      {
      for (j = 0; j < *n; j++) y[j] *= 0.01;
      ymin *= 0.01;
      ymax *= 0.01;
      };

   *xmin = min;
   *xmax = max;
   }

fclose (IN);

return (ifile);
}

/*-------------------------------------------------------------------*/


int read_curve (file, x, y, n, str)
char file[];
char str[];
float x[], y[];
int *n;

{
register int i, j;
int ifile, ncom, letter;

char line[200];

FILE *IN;



IN = fopen (file, "r");
ifile = 0;

if ((IN != (FILE *) 0) && (fscanf (IN, "%s", line) != -1))
   {
   ifile = 1;
   rewind (IN);
   letter = getc (IN);
   rewind (IN);
   for (i = 0; ((letter != 48) && (letter != 49) && (letter != 50) &&
                (letter != 51) && (letter != 52) && (letter != 53) &&
                (letter != 54) && (letter != 55) && (letter != 56) &&
                (letter != 57) && (letter != 32)); i++)
      {
      flire (IN, line);
      letter = line[0];
      if (!strncmp (line, "TITLE", 5)) (void)strcpy (str, line);
      };
   ncom = i;
   rewind (IN);
   for (j = 0; j < ncom; j++) flire (IN, line);

   j = 0;
   while (fscanf (IN, "%f%e", &x[j], &y[j]) != -1)
      {
      flire (IN, line);
      j++;
      };
   *n = j;

   if ((*n) > npmax)
      fprintf (stderr, "\n\nERROR: number of data too large !!!\n\n");
   }

fclose (IN);

return (ifile);
}
