/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826		last modif


===========================================================================*/


#include <stdlib.h>
#include <stdio.h>
#include <UxLib.h>
#include <model.h>


extern void plot_grism();





void	set_grism(ngrism)

int	ngrism;
{
extern	struct	s_trans	T;
extern  struct s_gui G;

char    am[20];
char    *dirdata;

dirdata=(char *) getenv("MID_FILTERS");
if (dirdata==NULL)
   fprintf(stderr,"ERROR: cannot find MID_FILTERS environment variable\r\n");



if (ngrism!=0)
	{
	strcpy(T.grismfile,T.gsmfl[ngrism-1]);
	T.resolution=T.pixel*T.res[ngrism-1];
	sprintf(am,"%.3f",T.resolution);
	UxPutText(UxFindSwidget("tf_disp_pix"),am);
	plot_grism(T.grismfile);
	}
else
	{
	T.resolution=0.;
	UxPutText(UxFindSwidget("tf_disp_pix"),"");
	};

switch(ngrism)
{
case 0:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/dimbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/dimr53.dat",dirdata);
	break;
case 1:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/dimbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/dimr53.dat",dirdata);
	break;
case 2:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/medbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/dimr53.dat",dirdata);
	break;
case 3:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/medbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/dimr53.dat",dirdata);
	break;
case 4:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/medbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/dimr53.dat",dirdata);
	break;
case 5:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/medbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/dimr53.dat",dirdata);
	break;
case 6:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/medbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/dimr53.dat",dirdata);
	break;
case 7:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/medbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/medr53.dat",dirdata);
	break;
case 8:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/medbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/medr53.dat",dirdata);
	break;
case 9:
	if (!strcmp(G.sel_inst,"emmib"))
		sprintf(T.opticsfile,"%s/emmi/medbl.dat",dirdata);
	else if (!strcmp(G.sel_inst,"emmir"))
		sprintf(T.opticsfile,"%s/emmi/medr53.dat",dirdata);
	break;

}


}
