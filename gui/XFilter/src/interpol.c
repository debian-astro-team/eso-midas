/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include<stdio.h>

float	interpol(x,xx,yy,np)

int	np;
float	x,xx[],yy[];

{
register int	i;
float	y;

for(i=0;i<np-1;i++)
	{
	if ((x>=xx[i])&&(x<=xx[i+1]))
		{
		y=yy[i]+(x-xx[i])/(xx[i+1]-xx[i])*(yy[i+1]-yy[i]);
		return(y);
		};
	};

fprintf(stderr,"WARNING: interpolation problem\r");
return -9999.99;
}
