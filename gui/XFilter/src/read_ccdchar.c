/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENTIFICATION   read_ccdchar.c
.MODULE           subroutine
.LANGUAGE         C
.AUTHOR           Jose Mendez - Eso La Silla
.PURPOSE          Read filter characteristics table, that contains
                  existing lasilla filters.
.KEYWORD          Read this table, and maintain its contains to fast
                  access to filters characteristics.
.COMMENTS         Read Filter Characteristics table.
.ENVIRONMENT      Unix
.VERSION          1.0 Mar 91.

 090826		last modif
---------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <midas_def.h>
#include <tbldef.h>
#include <math.h> 
#include <string.h>

#include <globaldef.h>


extern void DisplayList(), print_select();


void read_ccdchar()
{
extern char currsel[100];

extern void UpdateMessageWindow();

char *CList[CCDMAX];  

  int id;
  int i; 
  char table[80];
  char sccd[100];
  int sortcol, aw, ar, ncols, nrows, nul;
  int cccd;

char    *dirdata;

dirdata=(char *) getenv("MID_FILTERS");
if (dirdata==NULL)
   fprintf(stderr,"ERROR: cannot find MID_FILTERS environment variable\r\n");


sprintf(table,"%s/ccd/ccdchar.tbl",dirdata); 

if(TCTOPN(table,F_I_MODE,&id)!=0) 
	{
	SCTPUT("ERROR: Table ccdchar.tbl could not be opened.");
	return;
	}

TCIGET(id,&ncols,&nrows,&sortcol,&aw,&ar);

for(i=0;i<nrows;i++)
	CList[i] = (char *)osmmget( 100 );

TCCSER(id,"#1",&cccd);
for(i=0;i<nrows; i++)
	{
	TCERDC(id,i+1,cccd,sccd,&nul);
	strcpy(CList[i],sccd);    
	}

CList[nrows]=NULL;

DisplayList(CList,nrows);
print_select(CList,currsel,nrows);

for(i=0;i<nrows;i++)
	osmmfree(CList[i]);

TCTCLO(id);

UpdateMessageWindow("N   Type      PixSize   Format   Ron  Dark      Comments\n              [micron]          [e-] [e-/pix/h]");
}
