/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	CloudyModel.c

.VERSION
 090826         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxCascB.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include<string.h>
#include <ExternResources.h>
#include <model.h>
#include <global.h>

extern struct	s_cloudy C;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxCloudyModel;
	swidget	Uxform_CM;
	swidget	Uxlbl_Ccopyright;
	swidget	Uxform_CM1;
	swidget	UxCM_apply;
	swidget	Uxlbl_inc_cont;
	swidget	Uxlbl_chemic;
	swidget	Uxlbl_range;
	swidget	Uxtg_geometry;
	swidget	Uxtg_spheric;
	swidget	Uxtg_plan;
	swidget	Uxtf_Cwmin;
	swidget	Uxtf_Cwmax;
	swidget	UxhelptextCM;
	swidget	UxseparatorCM1;
	swidget	Uxmn_comp;
	swidget	Uxmn_solar;
	swidget	Uxmn_cameron;
	swidget	Uxmn_h2;
	swidget	Uxmn_nova;
	swidget	Uxmn_pnebula;
	swidget	Uxmn_aism;
	swidget	Uxmn_primordial;
	swidget	Uxm_composition;
	swidget	Uxlbl_geometry;
	swidget	Uxtf_metal;
	swidget	UxseparatorCM3;
	swidget	UxseparatorCM4;
	swidget	UxseparatorCM5;
	swidget	Uxmn_continuum;
	swidget	Uxmn_background;
	swidget	Uxmn_bbody;
	swidget	Uxmn_brem;
	swidget	Uxmn_fireball;
	swidget	Uxmn_powerlaw;
	swidget	Uxmn_agn;
	swidget	Uxmn_crab;
	swidget	Uxmn_cflow;
	swidget	Uxmn_kurucz;
	swidget	Uxmn_mihalas;
	swidget	Uxmn_atlas;
	swidget	Uxmn_werner;
	swidget	Uxmn_ism;
	swidget	Uxm_continuum;
	swidget	Uxlbl_lum;
	swidget	Uxmn_lum;
	swidget	Uxmn_luminosity;
	swidget	Uxmn_flux;
	swidget	Uxmn_bolo;
	swidget	Uxmn_visual;
	swidget	Uxmn_endensity;
	swidget	Uxmn_photon;
	swidget	Uxm_lum;
	swidget	UxseparatorCM2;
	swidget	Uxmenu_metal;
	swidget	Uxmn_metalonly;
	swidget	Uxmn_metalgrain;
	swidget	Uxmn_metal;
	swidget	UxmeCloudy;
	swidget	Uxme_c_file;
	swidget	Uxme_c_open;
	swidget	Uxmt_c_file;
	swidget	Uxme_c_help;
	swidget	Uxme_c_hcloudy;
	swidget	Uxmt_c_help;
	swidget	Uxme_c_quit;
	swidget	Uxme_c_bye;
	swidget	Uxmt_c_quit;
	swidget	Uxtf_contpar;
	swidget	Uxlbl_contpar;
	swidget	Uxlbl_density;
	swidget	Uxmn_density;
	swidget	Uxmn_cdens;
	swidget	Uxmn_cgazpres;
	swidget	Uxmn_cpres;
	swidget	Uxmn_globule;
	swidget	Uxm_density;
	swidget	Uxlbl_intpar;
	swidget	Uxtf_intpar;
	swidget	Uxlbl_extd;
	swidget	Uxtf_extd;
	swidget	Uxlbl_hden;
	swidget	Uxtf_hden;
	swidget	Uxlbl_cover;
	swidget	Uxtf_cover;
	swidget	Uxlbl_fill;
	swidget	Uxtf_fill;
	swidget	Uxmenu_rmin;
	swidget	Uxmn_rmincm;
	swidget	Uxmn_rminpc;
	swidget	Uxmn_rmin;
	swidget	Uxtf_rmin;
	swidget	Uxmenu_rmax;
	swidget	Uxmn_rmaxcm;
	swidget	Uxmn_rmaxpc;
	swidget	Uxmn_rmax;
	swidget	Uxtf_rmax;
	swidget	Uxtg_sphere;
	swidget	Uxtg_expanding;
	swidget	Uxtg_static;
	swidget	Uxtg_sphereon;
} _UxCCloudyModel;

#define CloudyModel             UxCloudyModelContext->UxCloudyModel
#define form_CM                 UxCloudyModelContext->Uxform_CM
#define lbl_Ccopyright          UxCloudyModelContext->Uxlbl_Ccopyright
#define form_CM1                UxCloudyModelContext->Uxform_CM1
#define CM_apply                UxCloudyModelContext->UxCM_apply
#define lbl_inc_cont            UxCloudyModelContext->Uxlbl_inc_cont
#define lbl_chemic              UxCloudyModelContext->Uxlbl_chemic
#define lbl_range               UxCloudyModelContext->Uxlbl_range
#define tg_geometry             UxCloudyModelContext->Uxtg_geometry
#define tg_spheric              UxCloudyModelContext->Uxtg_spheric
#define tg_plan                 UxCloudyModelContext->Uxtg_plan
#define tf_Cwmin                UxCloudyModelContext->Uxtf_Cwmin
#define tf_Cwmax                UxCloudyModelContext->Uxtf_Cwmax
#define helptextCM              UxCloudyModelContext->UxhelptextCM
#define separatorCM1            UxCloudyModelContext->UxseparatorCM1
#define mn_comp                 UxCloudyModelContext->Uxmn_comp
#define mn_solar                UxCloudyModelContext->Uxmn_solar
#define mn_cameron              UxCloudyModelContext->Uxmn_cameron
#define mn_h2                   UxCloudyModelContext->Uxmn_h2
#define mn_nova                 UxCloudyModelContext->Uxmn_nova
#define mn_pnebula              UxCloudyModelContext->Uxmn_pnebula
#define mn_aism                 UxCloudyModelContext->Uxmn_aism
#define mn_primordial           UxCloudyModelContext->Uxmn_primordial
#define m_composition           UxCloudyModelContext->Uxm_composition
#define lbl_geometry            UxCloudyModelContext->Uxlbl_geometry
#define tf_metal                UxCloudyModelContext->Uxtf_metal
#define separatorCM3            UxCloudyModelContext->UxseparatorCM3
#define separatorCM4            UxCloudyModelContext->UxseparatorCM4
#define separatorCM5            UxCloudyModelContext->UxseparatorCM5
#define mn_continuum            UxCloudyModelContext->Uxmn_continuum
#define mn_background           UxCloudyModelContext->Uxmn_background
#define mn_bbody                UxCloudyModelContext->Uxmn_bbody
#define mn_brem                 UxCloudyModelContext->Uxmn_brem
#define mn_fireball             UxCloudyModelContext->Uxmn_fireball
#define mn_powerlaw             UxCloudyModelContext->Uxmn_powerlaw
#define mn_agn                  UxCloudyModelContext->Uxmn_agn
#define mn_crab                 UxCloudyModelContext->Uxmn_crab
#define mn_cflow                UxCloudyModelContext->Uxmn_cflow
#define mn_kurucz               UxCloudyModelContext->Uxmn_kurucz
#define mn_mihalas              UxCloudyModelContext->Uxmn_mihalas
#define mn_atlas                UxCloudyModelContext->Uxmn_atlas
#define mn_werner               UxCloudyModelContext->Uxmn_werner
#define mn_ism                  UxCloudyModelContext->Uxmn_ism
#define m_continuum             UxCloudyModelContext->Uxm_continuum
#define lbl_lum                 UxCloudyModelContext->Uxlbl_lum
#define mn_lum                  UxCloudyModelContext->Uxmn_lum
#define mn_luminosity           UxCloudyModelContext->Uxmn_luminosity
#define mn_flux                 UxCloudyModelContext->Uxmn_flux
#define mn_bolo                 UxCloudyModelContext->Uxmn_bolo
#define mn_visual               UxCloudyModelContext->Uxmn_visual
#define mn_endensity            UxCloudyModelContext->Uxmn_endensity
#define mn_photon               UxCloudyModelContext->Uxmn_photon
#define m_lum                   UxCloudyModelContext->Uxm_lum
#define separatorCM2            UxCloudyModelContext->UxseparatorCM2
#define menu_metal              UxCloudyModelContext->Uxmenu_metal
#define mn_metalonly            UxCloudyModelContext->Uxmn_metalonly
#define mn_metalgrain           UxCloudyModelContext->Uxmn_metalgrain
#define mn_metal                UxCloudyModelContext->Uxmn_metal
#define meCloudy                UxCloudyModelContext->UxmeCloudy
#define me_c_file               UxCloudyModelContext->Uxme_c_file
#define me_c_open               UxCloudyModelContext->Uxme_c_open
#define mt_c_file               UxCloudyModelContext->Uxmt_c_file
#define me_c_help               UxCloudyModelContext->Uxme_c_help
#define me_c_hcloudy            UxCloudyModelContext->Uxme_c_hcloudy
#define mt_c_help               UxCloudyModelContext->Uxmt_c_help
#define me_c_quit               UxCloudyModelContext->Uxme_c_quit
#define me_c_bye                UxCloudyModelContext->Uxme_c_bye
#define mt_c_quit               UxCloudyModelContext->Uxmt_c_quit
#define tf_contpar              UxCloudyModelContext->Uxtf_contpar
#define lbl_contpar             UxCloudyModelContext->Uxlbl_contpar
#define lbl_density             UxCloudyModelContext->Uxlbl_density
#define mn_density              UxCloudyModelContext->Uxmn_density
#define mn_cdens                UxCloudyModelContext->Uxmn_cdens
#define mn_cgazpres             UxCloudyModelContext->Uxmn_cgazpres
#define mn_cpres                UxCloudyModelContext->Uxmn_cpres
#define mn_globule              UxCloudyModelContext->Uxmn_globule
#define m_density               UxCloudyModelContext->Uxm_density
#define lbl_intpar              UxCloudyModelContext->Uxlbl_intpar
#define tf_intpar               UxCloudyModelContext->Uxtf_intpar
#define lbl_extd                UxCloudyModelContext->Uxlbl_extd
#define tf_extd                 UxCloudyModelContext->Uxtf_extd
#define lbl_hden                UxCloudyModelContext->Uxlbl_hden
#define tf_hden                 UxCloudyModelContext->Uxtf_hden
#define lbl_cover               UxCloudyModelContext->Uxlbl_cover
#define tf_cover                UxCloudyModelContext->Uxtf_cover
#define lbl_fill                UxCloudyModelContext->Uxlbl_fill
#define tf_fill                 UxCloudyModelContext->Uxtf_fill
#define menu_rmin               UxCloudyModelContext->Uxmenu_rmin
#define mn_rmincm               UxCloudyModelContext->Uxmn_rmincm
#define mn_rminpc               UxCloudyModelContext->Uxmn_rminpc
#define mn_rmin                 UxCloudyModelContext->Uxmn_rmin
#define tf_rmin                 UxCloudyModelContext->Uxtf_rmin
#define menu_rmax               UxCloudyModelContext->Uxmenu_rmax
#define mn_rmaxcm               UxCloudyModelContext->Uxmn_rmaxcm
#define mn_rmaxpc               UxCloudyModelContext->Uxmn_rmaxpc
#define mn_rmax                 UxCloudyModelContext->Uxmn_rmax
#define tf_rmax                 UxCloudyModelContext->Uxtf_rmax
#define tg_sphere               UxCloudyModelContext->Uxtg_sphere
#define tg_expanding            UxCloudyModelContext->Uxtg_expanding
#define tg_static               UxCloudyModelContext->Uxtg_static
#define tg_sphereon             UxCloudyModelContext->Uxtg_sphereon

static _UxCCloudyModel	*UxCloudyModelContext;

extern void SetFileList(), DisplayShortCMHelp(), DisplayExtendedHelp();
extern void do_cloudy(), between();



/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable2 = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n";

static char	*transTable4 = "#override\n\
<Btn3Down>:HelpHelp()\n\
<EnterWindow>:HelpShortCM()\n\
<LeaveWindow>:ClearShortCM()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_CloudyModel();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearShortCM( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextCM"),"");
	 }
	UxCloudyModelContext = UxSaveCtx;
}

static void	action_HelpShortCM( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	DisplayShortCMHelp(UxWidget);
	UxCloudyModelContext = UxSaveCtx;
}

static void	action_HelpHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxCloudyModelContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_CM_apply( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	do_cloudy();
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	armCB_tg_spheric( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	armCB_tg_plan( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	valueChangedCB_tg_plan( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_Cwmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextCM"),"Minimum Wavelength (in nm)");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_Cwmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float minw;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_Cwmin")),"%f",&minw);
	
	
	sprintf(am,"%.1f",minw);
	UxPutText(UxFindSwidget("tf_Cwmin"),am);
	
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_Cwmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextCM"),"Maximum Wavelength (in nm)");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_Cwmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float maxw;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_Cwmax")),"%f",&maxw);
	
	
	sprintf(am,"%.1f",maxw);
	UxPutText(UxFindSwidget("tf_Cwmax"),am);
	
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_solar( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.chemic,"solar");
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_cameron( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.chemic,"Cameron");
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_h2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.chemic,"hii region");
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_nova( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.chemic,"nova");
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_pnebula( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.chemic,"planetary nebula");
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_aism( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.chemic,"_ISM");
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_primordial( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.chemic,"primordial");
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_metal( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	
	UxPutText(UxFindSwidget("helptextCM"),"Metals (and grains) scaling factor\n If negative log scale, if positive linear scale");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_metal( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_metal")),"%f",&reel);
	
	if (nid==1)
		{
		if (C.imetal==0)
			between(&C.metal,reel,-20.,1000.);
		else
			between(&C.metalgrain,reel,-20.,1000.);
		}
	
	sprintf(am,"%.2f",reel);
	UxPutText(UxFindSwidget("tf_metal"),am);
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_background( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Redshift:");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"false");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"false");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Scale Factor:");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	sprintf(word,"%.3f",C.zbg);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	strcpy(C.continuum,"bakground");
	sprintf(word,"%.2f",C.scalebg);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_bbody( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Temperature:");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"blackbody");
	sprintf(word,"%.3f",C.tempbb);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_brem( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Temp. (log):");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"bremsstrahlung");
	sprintf(word,"%.3f",C.tempbrem);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_fireball( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Redshift:");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"false");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"false");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"false");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"false");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"fireball");
	
	sprintf(word,"%.3f",C.zbg);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	UxPutText( UxFindSwidget("tf_intpar"), "" );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_powerlaw( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Spectral Index:");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"powerlaw");
	sprintf(word,"%.3f",C.spectral);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_agn( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Break (micron):");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	sprintf(word,"%.3f",C.agnbreak);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	strcpy(C.continuum,"agn");
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_crab( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"false");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"false");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"crab");
	UxPutText( UxFindSwidget("tf_contpar"), "" );
	
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_cflow( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"false");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"false");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"coolingflow");
	UxPutText( UxFindSwidget("tf_contpar"), "" );
	
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_kurucz( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Temperature:");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"kurucz");
	sprintf(word,"%.3f",C.tempstar);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_mihalas( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Temperature:");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"mihalas");
	sprintf(word,"%.3f",C.tempstar);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_atlas( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Temperature:");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"atlas");
	sprintf(word,"%.3f",C.tempstar);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_werner( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	 UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Temperature:");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"true");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"true");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"werner");
	sprintf(word,"%.3f",C.temppn);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_ism( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	UxPutSensitive(UxFindSwidget("lbl_contpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_contpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_contpar"),"Scale Factor:");
	
	UxPutSensitive(UxFindSwidget("m_lum"),"false");
	UxPutSensitive(UxFindSwidget("lbl_lum"),"false");
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"false");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"false");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"");
	
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_flux");
	UxPutMenuHistory(UxFindSwidget("m_lum"),"mn_luminosity");
	
	strcpy(C.continuum,"ism");
	sprintf(word,"%.3f",C.scaleism);
	UxPutText( UxFindSwidget("tf_contpar"), word );
	
	UxPutText( UxFindSwidget("tf_intpar"), "" );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_luminosity( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Luminosity (log):");
	
	strcpy(C.intensity,"luminosity");
	sprintf(word,"%.2f",C.luminosity);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_flux( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Flux at 500nm:");
	
	strcpy(C.intensity,"flux");
	sprintf(word,"%.2f",C.flux);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_bolo( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Magnitude:");
	
	strcpy(C.intensity,"bolometric");
	sprintf(word,"%.2f",C.mag);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_visual( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Magnitude:");
	
	strcpy(C.intensity,"visual");
	sprintf(word,"%.2f",C.mag);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_endensity( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"Temperature:");
	
	strcpy(C.intensity,"endensity");
	sprintf(word,"%.1f",C.tempeden);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_photon( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	char word[10];
	
	UxPutSensitive(UxFindSwidget("lbl_intpar"),"true");
	UxPutSensitive(UxFindSwidget("tf_intpar"),"true");
	UxPutLabelString(UxFindSwidget("lbl_intpar"),"number/s (log):");
	
	strcpy(C.intensity,"number");
	sprintf(word,"%.2f",C.number);
	UxPutText( UxFindSwidget("tf_intpar"), word );
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_metalonly( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	C.imetal=0;
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_metalgrain( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	C.imetal=1;
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_me_c_open( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	extern swidget scrolledchoose;
	
	SetFileList( UxGetWidget(scrolledchoose),1, "*.ised" );
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_me_c_hcloudy( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_me_c_bye( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	UxPopdownInterface(UxFindSwidget("CloudyModel"));
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_contpar( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	int isfr = 1;
	
	if (isfr==1)
	 UxPutText(UxFindSwidget("helptextCM"),"Exponential-folding time TAU (Gyr)");
	else if (isfr==-1)
	 UxPutText(UxFindSwidget("helptextCM"),"Bruzual mu parameter");
	else if (isfr==2)
	 UxPutText(UxFindSwidget("helptextCM"),"Duration of the Single Birst (Gyr)"); 
	else if (isfr==3)
	 UxPutText(UxFindSwidget("helptextCM"),"Star Formation Rate in Solar Mass per year");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_contpar( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_contpar")),"%f",&reel);
	if (nid==1)
		{
		if (!strcmp(C.continuum,"background"))
			{
			between(&C.zbg,reel,0.,2000.);
			reel=C.zbg;
			}
		else if (!strcmp(C.continuum,"blackbody"))
			{
			between(&C.tempbb,reel,2000.,1000000.);
			reel=C.tempbb;
			}
		else if (!strcmp(C.continuum,"bremsstrahlung"))
			{
			between(&C.tempbrem,reel,2.,20.);
			reel=C.tempbrem;
			}
		else if (!strcmp(C.continuum,"powerlaw"))
			{
			between(&C.spectral,reel,-10.,-0.1);
			reel=C.spectral;
			}
		else if (!strcmp(C.continuum,"agn"))
			{
			between(&C.agnbreak,reel,0.,1000.);
			reel=C.agnbreak;
			}
		else if (!strcmp(C.continuum,"kurucz"))
			{
			between(&C.tempstar,reel,10000.,60000.);
			reel=C.tempstar;
			}
		else if (!strcmp(C.continuum,"mihalas"))
			{
			between(&C.tempstar,reel,10000.,60000.);
			reel=C.tempstar;
			}
		else if (!strcmp(C.continuum,"atlas"))
			{
			between(&C.tempstar,reel,10000.,60000.);
			reel=C.tempstar;
			}
		else if (!strcmp(C.continuum,"werner"))
			{
			between(&C.temppn,reel,10000.,200000.);
			reel=C.temppn;
			}
		else if (!strcmp(C.continuum,"ism"))
			{
			between(&C.scaleism,reel,0.,10.);
			reel=C.scaleism;
			}
		}
	
	sprintf(am,"%.2f",reel);
	UxPutText(UxFindSwidget("tf_contpar"),am);
	
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_cdens( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.denslaw,"constant density");
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_cgazpres( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.denslaw,"constant gaz pressure");
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_cpres( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.denslaw,"constant pressure");
	UxCloudyModelContext = UxSaveCtx;
}

static void	activateCB_mn_globule( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	strcpy(C.denslaw,"globule");
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_intpar( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	
	UxPutText(UxFindSwidget("helptextCM"),"Intensity Parameter");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_intpar( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_intpar")),"%f",&reel);
	
	
	sprintf(am,"%.2f",reel);
	UxPutText(UxFindSwidget("tf_intpar"),am);
	
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_extd( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextCM"),"Extinction colum density (log)");
	
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_extd( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_extd")),"%f",&reel);
	
	if (nid==1)
		between(&C.extinction,reel,0.,50.);
	
	sprintf(am,"%.2f",reel);
	UxPutText(UxFindSwidget("tf_extd"),am);
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_hden( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	UxPutText(UxFindSwidget("helptextCM"),"Hydrogen density in log of cm-3");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_hden( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_hden")),"%f",&reel);
	if (nid==1)
		between(&C.hden,reel,1.,50.);
	
	sprintf(am,"%.2f",C.hden);
	UxPutText(UxFindSwidget("tf_hden"),am);
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_cover( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	UxPutText(UxFindSwidget("helptextCM"),"Covering factor for the emission line region.\n Must be between 0 and 1");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_cover( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_cover")),"%f",&reel);
	
	if (nid==1)
		between(&C.cover,reel,0.0001,1.);
	
	
	sprintf(am,"%.2f",C.cover);
	UxPutText(UxFindSwidget("tf_cover"),am);
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_fill( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	UxPutText(UxFindSwidget("helptextCM"),"Filling factor for a clumpy model.\n Must be between 0 and 1");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_fill( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_fill")),"%f",&reel);
	if (nid==1)
		between(&C.fill,reel,0.0001,1.);
	
	
	sprintf(am,"%.2f",reel);
	UxPutText(UxFindSwidget("tf_fill"),am);
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_rmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	
	UxPutText(UxFindSwidget("helptextCM"),"Inner Radius");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_rmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_rmin")),"%f",&reel);
	
	
	sprintf(am,"%.2f",reel);
	UxPutText(UxFindSwidget("tf_rmin"),am);
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	focusCB_tf_rmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	
	UxPutText(UxFindSwidget("helptextCM"),"Outer Radius");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_rmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_rmax")),"%f",&reel);
	
	
	sprintf(am,"%.2f",reel);
	UxPutText(UxFindSwidget("tf_rmax"),am);
	
	UxPutText(UxFindSwidget("helptextCM"),"");
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	armCB_tg_expanding( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	armCB_tg_static( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	valueChangedCB_tg_static( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

static void	armCB_tg_sphereon( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCloudyModel         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCloudyModelContext;
	UxCloudyModelContext = UxContext =
			(_UxCCloudyModel *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCloudyModelContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_CloudyModel()
{
	UxPutKeyboardFocusPolicy( CloudyModel, "pointer" );
	UxPutIconName( CloudyModel, "Cloudy v84.09 G.Ferland" );
	UxPutDefaultFontList( CloudyModel, TextFont );
	UxPutBorderColor( CloudyModel, WindowBackground );
	UxPutBackground( CloudyModel, WindowBackground );
	UxPutHeight( CloudyModel, 580 );
	UxPutWidth( CloudyModel, 550 );
	UxPutY( CloudyModel, 304 );
	UxPutX( CloudyModel, 606 );

	UxPutTextFontList( form_CM, TextFont );
	UxPutLabelFontList( form_CM, TextFont );
	UxPutForeground( form_CM, TextForeground );
	UxPutButtonFontList( form_CM, TextFont );
	UxPutBorderColor( form_CM, WindowBackground );
	UxPutBackground( form_CM, WindowBackground );
	UxPutHeight( form_CM, 125 );
	UxPutWidth( form_CM, 138 );
	UxPutY( form_CM, -1 );
	UxPutX( form_CM, -3 );
	UxPutUnitType( form_CM, "pixels" );
	UxPutResizePolicy( form_CM, "resize_none" );

	UxPutLabelString( lbl_Ccopyright, "Cloudy v84.09 by G.Ferland   (gary@asta.pa.uky.edu)" );
	UxPutForeground( lbl_Ccopyright, TextForeground );
	UxPutFontList( lbl_Ccopyright, BoldTextFont );
	UxPutBorderColor( lbl_Ccopyright, WindowBackground );
	UxPutBackground( lbl_Ccopyright, WindowBackground );
	UxPutHeight( lbl_Ccopyright, 25 );
	UxPutWidth( lbl_Ccopyright, 408 );
	UxPutY( lbl_Ccopyright, 40 );
	UxPutX( lbl_Ccopyright, 0 );

	UxPutForeground( form_CM1, TextForeground );
	UxPutButtonFontList( form_CM1, BoldTextFont );
	UxPutBorderColor( form_CM1, ButtonBackground );
	UxPutBackground( form_CM1, ButtonBackground );
	UxPutHeight( form_CM1, 43 );
	UxPutWidth( form_CM1, 385 );
	UxPutY( form_CM1, 329 );
	UxPutX( form_CM1, 2 );
	UxPutResizePolicy( form_CM1, "resize_none" );

	UxPutTranslations( CM_apply, transTable4 );
	UxPutLabelString( CM_apply, "Apply" );
	UxPutBackground( CM_apply, ButtonBackground );
	UxPutForeground( CM_apply, ApplyForeground );
	UxPutFontList( CM_apply, BoldTextFont );
	UxPutHeight( CM_apply, 30 );
	UxPutWidth( CM_apply, 90 );
	UxPutY( CM_apply, 6 );
	UxPutX( CM_apply, 16 );

	UxPutAlignment( lbl_inc_cont, "alignment_beginning" );
	UxPutLabelString( lbl_inc_cont, "Incident Continuum :" );
	UxPutForeground( lbl_inc_cont, TextForeground );
	UxPutFontList( lbl_inc_cont, TextFont );
	UxPutBorderColor( lbl_inc_cont, WindowBackground );
	UxPutBackground( lbl_inc_cont, WindowBackground );
	UxPutHeight( lbl_inc_cont, 23 );
	UxPutWidth( lbl_inc_cont, 140 );
	UxPutY( lbl_inc_cont, 86 );
	UxPutX( lbl_inc_cont, 10 );

	UxPutAlignment( lbl_chemic, "alignment_beginning" );
	UxPutLabelString( lbl_chemic, "Chemical Composition :" );
	UxPutForeground( lbl_chemic, TextForeground );
	UxPutFontList( lbl_chemic, TextFont );
	UxPutBorderColor( lbl_chemic, WindowBackground );
	UxPutBackground( lbl_chemic, WindowBackground );
	UxPutHeight( lbl_chemic, 23 );
	UxPutWidth( lbl_chemic, 154 );
	UxPutY( lbl_chemic, 216 );
	UxPutX( lbl_chemic, 10 );

	UxPutAlignment( lbl_range, "alignment_beginning" );
	UxPutLabelString( lbl_range, "Wavelengt Range (nm) :" );
	UxPutForeground( lbl_range, TextForeground );
	UxPutFontList( lbl_range, TextFont );
	UxPutBorderColor( lbl_range, WindowBackground );
	UxPutBackground( lbl_range, WindowBackground );
	UxPutHeight( lbl_range, 23 );
	UxPutWidth( lbl_range, 165 );
	UxPutY( lbl_range, 435 );
	UxPutX( lbl_range, 8 );

	UxPutForeground( tg_geometry, TextForeground );
	UxPutSpacing( tg_geometry, 0 );
	UxPutMarginWidth( tg_geometry, 0 );
	UxPutOrientation( tg_geometry, "horizontal" );
	UxPutIsAligned( tg_geometry, "true" );
	UxPutEntryAlignment( tg_geometry, "alignment_center" );
	UxPutBorderWidth( tg_geometry, 0 );
	UxPutShadowThickness( tg_geometry, 0 );
	UxPutLabelString( tg_geometry, "" );
	UxPutEntryBorder( tg_geometry, 0 );
	UxPutBorderColor( tg_geometry, WindowBackground );
	UxPutBackground( tg_geometry, WindowBackground );
	UxPutRadioBehavior( tg_geometry, "true" );
	UxPutWidth( tg_geometry, 208 );
	UxPutY( tg_geometry, 300 );
	UxPutX( tg_geometry, 90 );

	UxPutSpacing( tg_spheric, 0 );
	UxPutMarginWidth( tg_spheric, 0 );
	UxPutMarginLeft( tg_spheric, 0 );
	UxPutHighlightOnEnter( tg_spheric, "true" );
	UxPutForeground( tg_spheric, TextForeground );
	UxPutSelectColor( tg_spheric, SelectColor );
	UxPutSet( tg_spheric, "true" );
	UxPutLabelString( tg_spheric, "Spherical" );
	UxPutFontList( tg_spheric, TextFont );
	UxPutBorderColor( tg_spheric, WindowBackground );
	UxPutBackground( tg_spheric, WindowBackground );
	UxPutHeight( tg_spheric, 28 );
	UxPutWidth( tg_spheric, 91 );
	UxPutY( tg_spheric, 303 );
	UxPutX( tg_spheric, 0 );

	UxPutSpacing( tg_plan, 0 );
	UxPutMarginWidth( tg_plan, 0 );
	UxPutMarginLeft( tg_plan, 0 );
	UxPutHighlightOnEnter( tg_plan, "true" );
	UxPutSet( tg_plan, "false" );
	UxPutForeground( tg_plan, TextForeground );
	UxPutSelectColor( tg_plan, SelectColor );
	UxPutLabelString( tg_plan, "Plane Parallel" );
	UxPutFontList( tg_plan, TextFont );
	UxPutBorderColor( tg_plan, WindowBackground );
	UxPutBackground( tg_plan, WindowBackground );
	UxPutHeight( tg_plan, 28 );
	UxPutWidth( tg_plan, 78 );
	UxPutY( tg_plan, 303 );
	UxPutX( tg_plan, 107 );

	UxPutTranslations( tf_Cwmin, transTable2 );
	UxPutText( tf_Cwmin, "300." );
	UxPutForeground( tf_Cwmin, TextForeground );
	UxPutHighlightOnEnter( tf_Cwmin, "true" );
	UxPutFontList( tf_Cwmin, TextFont );
	UxPutBorderColor( tf_Cwmin, TextBackground );
	UxPutBackground( tf_Cwmin, TextBackground );
	UxPutHeight( tf_Cwmin, 35 );
	UxPutWidth( tf_Cwmin, 65 );
	UxPutY( tf_Cwmin, 428 );
	UxPutX( tf_Cwmin, 178 );

	UxPutTranslations( tf_Cwmax, transTable2 );
	UxPutText( tf_Cwmax, "900." );
	UxPutForeground( tf_Cwmax, TextForeground );
	UxPutHighlightOnEnter( tf_Cwmax, "true" );
	UxPutFontList( tf_Cwmax, TextFont );
	UxPutBorderColor( tf_Cwmax, TextBackground );
	UxPutBackground( tf_Cwmax, TextBackground );
	UxPutHeight( tf_Cwmax, 35 );
	UxPutWidth( tf_Cwmax, 65 );
	UxPutY( tf_Cwmax, 429 );
	UxPutX( tf_Cwmax, 251 );

	UxPutForeground( helptextCM, TextForeground );
	UxPutFontList( helptextCM, TextFont );
	UxPutEditable( helptextCM, "false" );
	UxPutCursorPositionVisible( helptextCM, "false" );
	UxPutBorderColor( helptextCM, SHelpBackground );
	UxPutBackground( helptextCM, SHelpBackground );
	UxPutHeight( helptextCM, 50 );
	UxPutWidth( helptextCM, 490 );
	UxPutY( helptextCM, 540 );
	UxPutX( helptextCM, 0 );

	UxPutBorderColor( separatorCM1, WindowBackground );
	UxPutBackground( separatorCM1, WindowBackground );
	UxPutHeight( separatorCM1, 5 );
	UxPutWidth( separatorCM1, 492 );
	UxPutY( separatorCM1, 373 );
	UxPutX( separatorCM1, -4 );

	UxPutBorderColor( mn_comp, WindowBackground );
	UxPutBackground( mn_comp, WindowBackground );
	UxPutForeground( mn_comp, TextForeground );
	UxPutY( mn_comp, -20 );
	UxPutX( mn_comp, 0 );
	UxPutRowColumnType( mn_comp, "menu_pulldown" );

	UxPutY( mn_solar, 102 );
	UxPutX( mn_solar, 2 );
	UxPutForeground( mn_solar, TextForeground );
	UxPutFontList( mn_solar, TextFont );
	UxPutBorderColor( mn_solar, WindowBackground );
	UxPutBackground( mn_solar, WindowBackground );
	UxPutLabelString( mn_solar, "Solar" );

	UxPutY( mn_cameron, 102 );
	UxPutX( mn_cameron, 2 );
	UxPutBorderColor( mn_cameron, WindowBackground );
	UxPutBackground( mn_cameron, WindowBackground );
	UxPutForeground( mn_cameron, TextForeground );
	UxPutFontList( mn_cameron, TextFont );
	UxPutLabelString( mn_cameron, "Cameron" );

	UxPutY( mn_h2, 102 );
	UxPutX( mn_h2, 2 );
	UxPutForeground( mn_h2, TextForeground );
	UxPutFontList( mn_h2, TextFont );
	UxPutBorderColor( mn_h2, WindowBackground );
	UxPutBackground( mn_h2, WindowBackground );
	UxPutLabelString( mn_h2, "H II" );

	UxPutY( mn_nova, 102 );
	UxPutX( mn_nova, 2 );
	UxPutBorderColor( mn_nova, WindowBackground );
	UxPutBackground( mn_nova, WindowBackground );
	UxPutForeground( mn_nova, TextForeground );
	UxPutFontList( mn_nova, TextFont );
	UxPutLabelString( mn_nova, "Nova" );

	UxPutForeground( mn_pnebula, TextForeground );
	UxPutFontList( mn_pnebula, TextFont );
	UxPutBorderColor( mn_pnebula, WindowBackground );
	UxPutBackground( mn_pnebula, WindowBackground );
	UxPutLabelString( mn_pnebula, "P. Nebula" );

	UxPutForeground( mn_aism, TextForeground );
	UxPutFontList( mn_aism, TextFont );
	UxPutBorderColor( mn_aism, WindowBackground );
	UxPutBackground( mn_aism, WindowBackground );
	UxPutLabelString( mn_aism, "I.S.M." );

	UxPutForeground( mn_primordial, TextForeground );
	UxPutFontList( mn_primordial, TextFont );
	UxPutBorderColor( mn_primordial, WindowBackground );
	UxPutBackground( mn_primordial, WindowBackground );
	UxPutLabelString( mn_primordial, "Primordial" );

	UxPutLabelString( m_composition, "" );
	UxPutEntryAlignment( m_composition, "alignment_center" );
	UxPutHeight( m_composition, 35 );
	UxPutForeground( m_composition, TextForeground );
	UxPutBorderColor( m_composition, WindowBackground );
	UxPutBackground( m_composition, WindowBackground );
	UxPutY( m_composition, 212 );
	UxPutX( m_composition, 168 );
	UxPutRowColumnType( m_composition, "menu_option" );

	UxPutAlignment( lbl_geometry, "alignment_beginning" );
	UxPutLabelString( lbl_geometry, "Geometry :" );
	UxPutForeground( lbl_geometry, TextForeground );
	UxPutFontList( lbl_geometry, TextFont );
	UxPutBorderColor( lbl_geometry, WindowBackground );
	UxPutBackground( lbl_geometry, WindowBackground );
	UxPutHeight( lbl_geometry, 23 );
	UxPutWidth( lbl_geometry, 82 );
	UxPutY( lbl_geometry, 306 );
	UxPutX( lbl_geometry, 10 );

	UxPutTranslations( tf_metal, transTable2 );
	UxPutText( tf_metal, "1." );
	UxPutForeground( tf_metal, TextForeground );
	UxPutHighlightOnEnter( tf_metal, "true" );
	UxPutFontList( tf_metal, TextFont );
	UxPutBorderColor( tf_metal, TextBackground );
	UxPutBackground( tf_metal, TextBackground );
	UxPutHeight( tf_metal, 35 );
	UxPutWidth( tf_metal, 80 );
	UxPutY( tf_metal, 210 );
	UxPutX( tf_metal, 456 );

	UxPutBorderColor( separatorCM3, WindowBackground );
	UxPutBackground( separatorCM3, WindowBackground );
	UxPutHeight( separatorCM3, 5 );
	UxPutWidth( separatorCM3, 490 );
	UxPutY( separatorCM3, 202 );
	UxPutX( separatorCM3, 2 );

	UxPutBorderColor( separatorCM4, WindowBackground );
	UxPutBackground( separatorCM4, WindowBackground );
	UxPutHeight( separatorCM4, 5 );
	UxPutWidth( separatorCM4, 492 );
	UxPutY( separatorCM4, 418 );
	UxPutX( separatorCM4, -2 );

	UxPutBorderColor( separatorCM5, WindowBackground );
	UxPutBackground( separatorCM5, WindowBackground );
	UxPutHeight( separatorCM5, 5 );
	UxPutWidth( separatorCM5, 492 );
	UxPutY( separatorCM5, 292 );
	UxPutX( separatorCM5, 0 );

	UxPutBorderColor( mn_continuum, WindowBackground );
	UxPutBackground( mn_continuum, WindowBackground );
	UxPutForeground( mn_continuum, TextForeground );
	UxPutY( mn_continuum, -20 );
	UxPutX( mn_continuum, 0 );
	UxPutRowColumnType( mn_continuum, "menu_pulldown" );

	UxPutY( mn_background, 102 );
	UxPutX( mn_background, 2 );
	UxPutForeground( mn_background, TextForeground );
	UxPutFontList( mn_background, TextFont );
	UxPutBorderColor( mn_background, WindowBackground );
	UxPutBackground( mn_background, WindowBackground );
	UxPutLabelString( mn_background, "Background" );

	UxPutY( mn_bbody, 102 );
	UxPutX( mn_bbody, 2 );
	UxPutBorderColor( mn_bbody, WindowBackground );
	UxPutBackground( mn_bbody, WindowBackground );
	UxPutForeground( mn_bbody, TextForeground );
	UxPutFontList( mn_bbody, TextFont );
	UxPutLabelString( mn_bbody, "BlackBody" );

	UxPutY( mn_brem, 102 );
	UxPutX( mn_brem, 2 );
	UxPutForeground( mn_brem, TextForeground );
	UxPutFontList( mn_brem, TextFont );
	UxPutBorderColor( mn_brem, WindowBackground );
	UxPutBackground( mn_brem, WindowBackground );
	UxPutLabelString( mn_brem, "Bremsstrahlung" );

	UxPutForeground( mn_fireball, TextForeground );
	UxPutFontList( mn_fireball, TextFont );
	UxPutBorderColor( mn_fireball, WindowBackground );
	UxPutBackground( mn_fireball, WindowBackground );
	UxPutLabelString( mn_fireball, "FireBall only" );

	UxPutForeground( mn_powerlaw, TextForeground );
	UxPutFontList( mn_powerlaw, TextFont );
	UxPutBorderColor( mn_powerlaw, WindowBackground );
	UxPutBackground( mn_powerlaw, WindowBackground );
	UxPutLabelString( mn_powerlaw, "Power Law" );

	UxPutForeground( mn_agn, TextForeground );
	UxPutFontList( mn_agn, TextFont );
	UxPutBorderColor( mn_agn, WindowBackground );
	UxPutBackground( mn_agn, WindowBackground );
	UxPutLabelString( mn_agn, "A.G.N." );

	UxPutForeground( mn_crab, TextForeground );
	UxPutFontList( mn_crab, TextFont );
	UxPutBorderColor( mn_crab, WindowBackground );
	UxPutBackground( mn_crab, WindowBackground );
	UxPutLabelString( mn_crab, "Crab Nebula" );

	UxPutForeground( mn_cflow, TextForeground );
	UxPutFontList( mn_cflow, TextFont );
	UxPutBorderColor( mn_cflow, WindowBackground );
	UxPutBackground( mn_cflow, WindowBackground );
	UxPutLabelString( mn_cflow, "Cooling Flow" );

	UxPutForeground( mn_kurucz, TextForeground );
	UxPutFontList( mn_kurucz, TextFont );
	UxPutBorderColor( mn_kurucz, WindowBackground );
	UxPutBackground( mn_kurucz, WindowBackground );
	UxPutLabelString( mn_kurucz, "Kurucz Star" );

	UxPutForeground( mn_mihalas, TextForeground );
	UxPutFontList( mn_mihalas, TextFont );
	UxPutBorderColor( mn_mihalas, WindowBackground );
	UxPutBackground( mn_mihalas, WindowBackground );
	UxPutLabelString( mn_mihalas, "Mihalas Star" );

	UxPutForeground( mn_atlas, TextForeground );
	UxPutFontList( mn_atlas, TextFont );
	UxPutBorderColor( mn_atlas, WindowBackground );
	UxPutBackground( mn_atlas, WindowBackground );
	UxPutLabelString( mn_atlas, "Star Atlas" );

	UxPutForeground( mn_werner, TextForeground );
	UxPutFontList( mn_werner, TextFont );
	UxPutBorderColor( mn_werner, WindowBackground );
	UxPutBackground( mn_werner, WindowBackground );
	UxPutLabelString( mn_werner, "Werner P.N." );

	UxPutForeground( mn_ism, TextForeground );
	UxPutFontList( mn_ism, TextFont );
	UxPutBorderColor( mn_ism, WindowBackground );
	UxPutBackground( mn_ism, WindowBackground );
	UxPutLabelString( mn_ism, "I.S.M." );

	UxPutLabelString( m_continuum, "" );
	UxPutEntryAlignment( m_continuum, "alignment_center" );
	UxPutHeight( m_continuum, 35 );
	UxPutForeground( m_continuum, TextForeground );
	UxPutBorderColor( m_continuum, WindowBackground );
	UxPutBackground( m_continuum, WindowBackground );
	UxPutY( m_continuum, 80 );
	UxPutX( m_continuum, 152 );
	UxPutRowColumnType( m_continuum, "menu_option" );

	UxPutAlignment( lbl_lum, "alignment_beginning" );
	UxPutLabelString( lbl_lum, "Intensity :" );
	UxPutForeground( lbl_lum, TextForeground );
	UxPutFontList( lbl_lum, TextFont );
	UxPutBorderColor( lbl_lum, WindowBackground );
	UxPutBackground( lbl_lum, WindowBackground );
	UxPutHeight( lbl_lum, 23 );
	UxPutWidth( lbl_lum, 88 );
	UxPutY( lbl_lum, 124 );
	UxPutX( lbl_lum, 10 );

	UxPutForeground( mn_lum, TextForeground );
	UxPutY( mn_lum, -20 );
	UxPutX( mn_lum, 0 );
	UxPutRowColumnType( mn_lum, "menu_pulldown" );

	UxPutY( mn_luminosity, 102 );
	UxPutX( mn_luminosity, 2 );
	UxPutForeground( mn_luminosity, TextForeground );
	UxPutFontList( mn_luminosity, TextFont );
	UxPutBorderColor( mn_luminosity, WindowBackground );
	UxPutBackground( mn_luminosity, WindowBackground );
	UxPutLabelString( mn_luminosity, "Luminosity" );

	UxPutY( mn_flux, 102 );
	UxPutX( mn_flux, 2 );
	UxPutBorderColor( mn_flux, WindowBackground );
	UxPutBackground( mn_flux, WindowBackground );
	UxPutForeground( mn_flux, TextForeground );
	UxPutFontList( mn_flux, TextFont );
	UxPutLabelString( mn_flux, "Flux Density" );

	UxPutForeground( mn_bolo, TextForeground );
	UxPutFontList( mn_bolo, TextFont );
	UxPutBorderColor( mn_bolo, WindowBackground );
	UxPutBackground( mn_bolo, WindowBackground );
	UxPutLabelString( mn_bolo, "Bolometric mag." );

	UxPutForeground( mn_visual, TextForeground );
	UxPutFontList( mn_visual, TextFont );
	UxPutBorderColor( mn_visual, WindowBackground );
	UxPutBackground( mn_visual, WindowBackground );
	UxPutLabelString( mn_visual, "Visual mag." );

	UxPutForeground( mn_endensity, TextForeground );
	UxPutFontList( mn_endensity, TextFont );
	UxPutBorderColor( mn_endensity, WindowBackground );
	UxPutBackground( mn_endensity, WindowBackground );
	UxPutLabelString( mn_endensity, "Energy Density" );

	UxPutForeground( mn_photon, TextForeground );
	UxPutFontList( mn_photon, TextFont );
	UxPutBorderColor( mn_photon, WindowBackground );
	UxPutBackground( mn_photon, WindowBackground );
	UxPutLabelString( mn_photon, "Ionizing Photons" );

	UxPutLabelString( m_lum, "" );
	UxPutEntryAlignment( m_lum, "alignment_center" );
	UxPutHeight( m_lum, 35 );
	UxPutForeground( m_lum, TextForeground );
	UxPutBorderColor( m_lum, WindowBackground );
	UxPutBackground( m_lum, WindowBackground );
	UxPutY( m_lum, 120 );
	UxPutX( m_lum, 154 );
	UxPutRowColumnType( m_lum, "menu_option" );

	UxPutBorderColor( separatorCM2, WindowBackground );
	UxPutBackground( separatorCM2, WindowBackground );
	UxPutHeight( separatorCM2, 5 );
	UxPutWidth( separatorCM2, 492 );
	UxPutY( separatorCM2, 71 );
	UxPutX( separatorCM2, 0 );

	UxPutForeground( menu_metal, TextForeground );
	UxPutY( menu_metal, -20 );
	UxPutX( menu_metal, 0 );
	UxPutRowColumnType( menu_metal, "menu_pulldown" );

	UxPutY( mn_metalonly, 102 );
	UxPutX( mn_metalonly, 2 );
	UxPutForeground( mn_metalonly, TextForeground );
	UxPutFontList( mn_metalonly, TextFont );
	UxPutBorderColor( mn_metalonly, WindowBackground );
	UxPutBackground( mn_metalonly, WindowBackground );
	UxPutLabelString( mn_metalonly, "Metal" );

	UxPutY( mn_metalgrain, 102 );
	UxPutX( mn_metalgrain, 2 );
	UxPutBorderColor( mn_metalgrain, WindowBackground );
	UxPutBackground( mn_metalgrain, WindowBackground );
	UxPutForeground( mn_metalgrain, TextForeground );
	UxPutFontList( mn_metalgrain, TextFont );
	UxPutLabelString( mn_metalgrain, "Metal & Grain" );

	UxPutLabelString( mn_metal, "" );
	UxPutEntryAlignment( mn_metal, "alignment_center" );
	UxPutSpacing( mn_metal, 0 );
	UxPutMarginWidth( mn_metal, 0 );
	UxPutHeight( mn_metal, 35 );
	UxPutForeground( mn_metal, TextForeground );
	UxPutBorderColor( mn_metal, WindowBackground );
	UxPutBackground( mn_metal, WindowBackground );
	UxPutY( mn_metal, 212 );
	UxPutX( mn_metal, 316 );
	UxPutRowColumnType( mn_metal, "menu_option" );

	UxPutSpacing( meCloudy, 10 );
	UxPutPacking( meCloudy, "pack_tight" );
	UxPutBorderColor( meCloudy, MenuBackground );
	UxPutMenuAccelerator( meCloudy, "<KeyUp>F10" );
	UxPutForeground( meCloudy, MenuForeground );
	UxPutBackground( meCloudy, MenuBackground );
	UxPutY( meCloudy, 0 );
	UxPutX( meCloudy, 40 );
	UxPutRowColumnType( meCloudy, "menu_bar" );

	UxPutForeground( me_c_file, MenuForeground );
	UxPutBackground( me_c_file, MenuBackground );
	UxPutRowColumnType( me_c_file, "menu_pulldown" );

	UxPutMnemonic( me_c_open, "O" );
	UxPutBorderColor( me_c_open, MenuBackground );
	UxPutForeground( me_c_open, MenuForeground );
	UxPutFontList( me_c_open, BoldTextFont );
	UxPutBackground( me_c_open, MenuBackground );
	UxPutLabelString( me_c_open, "Open ..." );

	UxPutBorderColor( mt_c_file, MenuBackground );
	UxPutForeground( mt_c_file, MenuForeground );
	UxPutFontList( mt_c_file, BoldTextFont );
	UxPutBackground( mt_c_file, MenuBackground );
	UxPutMnemonic( mt_c_file, "F" );
	UxPutLabelString( mt_c_file, "File" );

	UxPutRowColumnType( me_c_help, "menu_pulldown" );

	UxPutBorderColor( me_c_hcloudy, MenuBackground );
	UxPutBackground( me_c_hcloudy, MenuBackground );
	UxPutForeground( me_c_hcloudy, MenuForeground );
	UxPutFontList( me_c_hcloudy, BoldTextFont );
	UxPutMnemonic( me_c_hcloudy, "O" );
	UxPutLabelString( me_c_hcloudy, "On CLOUDY Model ..." );

	UxPutBorderColor( mt_c_help, MenuBackground );
	UxPutBackground( mt_c_help, MenuBackground );
	UxPutForeground( mt_c_help, MenuForeground );
	UxPutFontList( mt_c_help, BoldTextFont );
	UxPutMnemonic( mt_c_help, "H" );
	UxPutLabelString( mt_c_help, "Help" );

	UxPutX( me_c_quit, 400 );
	UxPutRowColumnType( me_c_quit, "menu_pulldown" );

	UxPutBorderColor( me_c_bye, MenuBackground );
	UxPutBackground( me_c_bye, MenuBackground );
	UxPutForeground( me_c_bye, MenuForeground );
	UxPutFontList( me_c_bye, BoldTextFont );
	UxPutMnemonic( me_c_bye, "B" );
	UxPutLabelString( me_c_bye, "Bye" );

	UxPutAlignment( mt_c_quit, "alignment_end" );
	UxPutX( mt_c_quit, 400 );
	UxPutBorderColor( mt_c_quit, MenuBackground );
	UxPutBackground( mt_c_quit, MenuBackground );
	UxPutForeground( mt_c_quit, MenuForeground );
	UxPutFontList( mt_c_quit, BoldTextFont );
	UxPutMnemonic( mt_c_quit, "Q" );
	UxPutLabelString( mt_c_quit, "Quit" );

	UxPutTranslations( tf_contpar, transTable2 );
	UxPutText( tf_contpar, "1." );
	UxPutForeground( tf_contpar, TextForeground );
	UxPutHighlightOnEnter( tf_contpar, "true" );
	UxPutFontList( tf_contpar, TextFont );
	UxPutBorderColor( tf_contpar, TextBackground );
	UxPutBackground( tf_contpar, TextBackground );
	UxPutHeight( tf_contpar, 35 );
	UxPutWidth( tf_contpar, 80 );
	UxPutY( tf_contpar, 80 );
	UxPutX( tf_contpar, 456 );

	UxPutAlignment( lbl_contpar, "alignment_end" );
	UxPutLabelString( lbl_contpar, "Parameter :" );
	UxPutForeground( lbl_contpar, TextForeground );
	UxPutFontList( lbl_contpar, TextFont );
	UxPutBorderColor( lbl_contpar, WindowBackground );
	UxPutBackground( lbl_contpar, WindowBackground );
	UxPutHeight( lbl_contpar, 20 );
	UxPutWidth( lbl_contpar, 116 );
	UxPutY( lbl_contpar, 86 );
	UxPutX( lbl_contpar, 338 );

	UxPutAlignment( lbl_density, "alignment_beginning" );
	UxPutLabelString( lbl_density, "H Density Law :" );
	UxPutForeground( lbl_density, TextForeground );
	UxPutFontList( lbl_density, TextFont );
	UxPutBorderColor( lbl_density, WindowBackground );
	UxPutBackground( lbl_density, WindowBackground );
	UxPutHeight( lbl_density, 23 );
	UxPutWidth( lbl_density, 100 );
	UxPutY( lbl_density, 256 );
	UxPutX( lbl_density, 10 );

	UxPutForeground( mn_density, TextForeground );
	UxPutY( mn_density, -20 );
	UxPutX( mn_density, 0 );
	UxPutRowColumnType( mn_density, "menu_pulldown" );

	UxPutY( mn_cdens, 102 );
	UxPutX( mn_cdens, 2 );
	UxPutForeground( mn_cdens, TextForeground );
	UxPutFontList( mn_cdens, TextFont );
	UxPutBorderColor( mn_cdens, WindowBackground );
	UxPutBackground( mn_cdens, WindowBackground );
	UxPutLabelString( mn_cdens, "Constant Density" );

	UxPutY( mn_cgazpres, 102 );
	UxPutX( mn_cgazpres, 2 );
	UxPutBorderColor( mn_cgazpres, WindowBackground );
	UxPutBackground( mn_cgazpres, WindowBackground );
	UxPutForeground( mn_cgazpres, TextForeground );
	UxPutFontList( mn_cgazpres, TextFont );
	UxPutLabelString( mn_cgazpres, "Cst. Gas Pressure" );

	UxPutForeground( mn_cpres, TextForeground );
	UxPutFontList( mn_cpres, TextFont );
	UxPutBorderColor( mn_cpres, WindowBackground );
	UxPutBackground( mn_cpres, WindowBackground );
	UxPutLabelString( mn_cpres, "Cst. Pressure" );

	UxPutForeground( mn_globule, TextForeground );
	UxPutFontList( mn_globule, TextFont );
	UxPutBorderColor( mn_globule, WindowBackground );
	UxPutBackground( mn_globule, WindowBackground );
	UxPutLabelString( mn_globule, "Globule" );

	UxPutLabelString( m_density, "" );
	UxPutEntryAlignment( m_density, "alignment_center" );
	UxPutRadioBehavior( m_density, "true" );
	UxPutHeight( m_density, 35 );
	UxPutForeground( m_density, TextForeground );
	UxPutBorderColor( m_density, WindowBackground );
	UxPutBackground( m_density, WindowBackground );
	UxPutY( m_density, 252 );
	UxPutX( m_density, 124 );
	UxPutRowColumnType( m_density, "menu_option" );

	UxPutAlignment( lbl_intpar, "alignment_end" );
	UxPutLabelString( lbl_intpar, "Parameter :" );
	UxPutForeground( lbl_intpar, TextForeground );
	UxPutFontList( lbl_intpar, TextFont );
	UxPutBorderColor( lbl_intpar, WindowBackground );
	UxPutBackground( lbl_intpar, WindowBackground );
	UxPutHeight( lbl_intpar, 23 );
	UxPutWidth( lbl_intpar, 116 );
	UxPutY( lbl_intpar, 124 );
	UxPutX( lbl_intpar, 338 );

	UxPutTranslations( tf_intpar, transTable2 );
	UxPutText( tf_intpar, "1." );
	UxPutForeground( tf_intpar, TextForeground );
	UxPutHighlightOnEnter( tf_intpar, "true" );
	UxPutFontList( tf_intpar, TextFont );
	UxPutBorderColor( tf_intpar, TextBackground );
	UxPutBackground( tf_intpar, TextBackground );
	UxPutHeight( tf_intpar, 35 );
	UxPutWidth( tf_intpar, 80 );
	UxPutY( tf_intpar, 118 );
	UxPutX( tf_intpar, 456 );

	UxPutAlignment( lbl_extd, "alignment_beginning" );
	UxPutLabelString( lbl_extd, "Extinction (column density):" );
	UxPutForeground( lbl_extd, TextForeground );
	UxPutFontList( lbl_extd, TextFont );
	UxPutBorderColor( lbl_extd, WindowBackground );
	UxPutBackground( lbl_extd, WindowBackground );
	UxPutHeight( lbl_extd, 23 );
	UxPutWidth( lbl_extd, 188 );
	UxPutY( lbl_extd, 166 );
	UxPutX( lbl_extd, 10 );

	UxPutTranslations( tf_extd, transTable2 );
	UxPutText( tf_extd, "1." );
	UxPutForeground( tf_extd, TextForeground );
	UxPutHighlightOnEnter( tf_extd, "true" );
	UxPutFontList( tf_extd, TextFont );
	UxPutBorderColor( tf_extd, TextBackground );
	UxPutBackground( tf_extd, TextBackground );
	UxPutHeight( tf_extd, 35 );
	UxPutWidth( tf_extd, 80 );
	UxPutY( tf_extd, 160 );
	UxPutX( tf_extd, 216 );

	UxPutAlignment( lbl_hden, "alignment_end" );
	UxPutLabelString( lbl_hden, "H Density :" );
	UxPutForeground( lbl_hden, TextForeground );
	UxPutFontList( lbl_hden, TextFont );
	UxPutBorderColor( lbl_hden, WindowBackground );
	UxPutBackground( lbl_hden, WindowBackground );
	UxPutHeight( lbl_hden, 20 );
	UxPutWidth( lbl_hden, 116 );
	UxPutY( lbl_hden, 258 );
	UxPutX( lbl_hden, 338 );

	UxPutTranslations( tf_hden, transTable2 );
	UxPutText( tf_hden, "1." );
	UxPutForeground( tf_hden, TextForeground );
	UxPutHighlightOnEnter( tf_hden, "true" );
	UxPutFontList( tf_hden, TextFont );
	UxPutBorderColor( tf_hden, TextBackground );
	UxPutBackground( tf_hden, TextBackground );
	UxPutHeight( tf_hden, 35 );
	UxPutWidth( tf_hden, 80 );
	UxPutY( tf_hden, 252 );
	UxPutX( tf_hden, 456 );

	UxPutAlignment( lbl_cover, "alignment_end" );
	UxPutLabelString( lbl_cover, "Covering Factor:" );
	UxPutForeground( lbl_cover, TextForeground );
	UxPutFontList( lbl_cover, TextFont );
	UxPutBorderColor( lbl_cover, WindowBackground );
	UxPutBackground( lbl_cover, WindowBackground );
	UxPutHeight( lbl_cover, 20 );
	UxPutWidth( lbl_cover, 114 );
	UxPutY( lbl_cover, 307 );
	UxPutX( lbl_cover, 340 );

	UxPutTranslations( tf_cover, transTable2 );
	UxPutText( tf_cover, "1." );
	UxPutForeground( tf_cover, TextForeground );
	UxPutHighlightOnEnter( tf_cover, "true" );
	UxPutFontList( tf_cover, TextFont );
	UxPutBorderColor( tf_cover, TextBackground );
	UxPutBackground( tf_cover, TextBackground );
	UxPutHeight( tf_cover, 35 );
	UxPutWidth( tf_cover, 80 );
	UxPutY( tf_cover, 300 );
	UxPutX( tf_cover, 456 );

	UxPutAlignment( lbl_fill, "alignment_end" );
	UxPutLabelString( lbl_fill, "Filling Factor:" );
	UxPutForeground( lbl_fill, TextForeground );
	UxPutFontList( lbl_fill, TextFont );
	UxPutBorderColor( lbl_fill, WindowBackground );
	UxPutBackground( lbl_fill, WindowBackground );
	UxPutHeight( lbl_fill, 20 );
	UxPutWidth( lbl_fill, 114 );
	UxPutY( lbl_fill, 342 );
	UxPutX( lbl_fill, 340 );

	UxPutTranslations( tf_fill, transTable2 );
	UxPutText( tf_fill, "1." );
	UxPutForeground( tf_fill, TextForeground );
	UxPutHighlightOnEnter( tf_fill, "true" );
	UxPutFontList( tf_fill, TextFont );
	UxPutBorderColor( tf_fill, TextBackground );
	UxPutBackground( tf_fill, TextBackground );
	UxPutHeight( tf_fill, 35 );
	UxPutWidth( tf_fill, 80 );
	UxPutY( tf_fill, 335 );
	UxPutX( tf_fill, 456 );

	UxPutForeground( menu_rmin, TextForeground );
	UxPutY( menu_rmin, -20 );
	UxPutX( menu_rmin, 0 );
	UxPutRowColumnType( menu_rmin, "menu_pulldown" );

	UxPutY( mn_rmincm, 102 );
	UxPutX( mn_rmincm, 2 );
	UxPutForeground( mn_rmincm, TextForeground );
	UxPutFontList( mn_rmincm, TextFont );
	UxPutBorderColor( mn_rmincm, WindowBackground );
	UxPutBackground( mn_rmincm, WindowBackground );
	UxPutLabelString( mn_rmincm, "Inner R. (log cm)" );

	UxPutY( mn_rminpc, 102 );
	UxPutX( mn_rminpc, 2 );
	UxPutBorderColor( mn_rminpc, WindowBackground );
	UxPutBackground( mn_rminpc, WindowBackground );
	UxPutForeground( mn_rminpc, TextForeground );
	UxPutFontList( mn_rminpc, TextFont );
	UxPutLabelString( mn_rminpc, "Inner R. (pc)" );

	UxPutLabelString( mn_rmin, "" );
	UxPutEntryAlignment( mn_rmin, "alignment_center" );
	UxPutSpacing( mn_rmin, 0 );
	UxPutMarginWidth( mn_rmin, 0 );
	UxPutHeight( mn_rmin, 35 );
	UxPutForeground( mn_rmin, TextForeground );
	UxPutBorderColor( mn_rmin, WindowBackground );
	UxPutBackground( mn_rmin, WindowBackground );
	UxPutY( mn_rmin, 337 );
	UxPutX( mn_rmin, 10 );
	UxPutRowColumnType( mn_rmin, "menu_option" );

	UxPutTranslations( tf_rmin, transTable2 );
	UxPutText( tf_rmin, "1." );
	UxPutForeground( tf_rmin, TextForeground );
	UxPutHighlightOnEnter( tf_rmin, "true" );
	UxPutFontList( tf_rmin, TextFont );
	UxPutBorderColor( tf_rmin, TextBackground );
	UxPutBackground( tf_rmin, TextBackground );
	UxPutHeight( tf_rmin, 35 );
	UxPutWidth( tf_rmin, 80 );
	UxPutY( tf_rmin, 335 );
	UxPutX( tf_rmin, 180 );

	UxPutForeground( menu_rmax, TextForeground );
	UxPutY( menu_rmax, -20 );
	UxPutX( menu_rmax, 0 );
	UxPutRowColumnType( menu_rmax, "menu_pulldown" );

	UxPutY( mn_rmaxcm, 102 );
	UxPutX( mn_rmaxcm, 2 );
	UxPutForeground( mn_rmaxcm, TextForeground );
	UxPutFontList( mn_rmaxcm, TextFont );
	UxPutBorderColor( mn_rmaxcm, WindowBackground );
	UxPutBackground( mn_rmaxcm, WindowBackground );
	UxPutLabelString( mn_rmaxcm, "Outer R. (log cm)" );

	UxPutY( mn_rmaxpc, 102 );
	UxPutX( mn_rmaxpc, 2 );
	UxPutBorderColor( mn_rmaxpc, WindowBackground );
	UxPutBackground( mn_rmaxpc, WindowBackground );
	UxPutForeground( mn_rmaxpc, TextForeground );
	UxPutFontList( mn_rmaxpc, TextFont );
	UxPutLabelString( mn_rmaxpc, "Outer R. (pc)" );

	UxPutLabelString( mn_rmax, "" );
	UxPutEntryAlignment( mn_rmax, "alignment_center" );
	UxPutSpacing( mn_rmax, 0 );
	UxPutMarginWidth( mn_rmax, 0 );
	UxPutHeight( mn_rmax, 35 );
	UxPutForeground( mn_rmax, TextForeground );
	UxPutBorderColor( mn_rmax, WindowBackground );
	UxPutBackground( mn_rmax, WindowBackground );
	UxPutY( mn_rmax, 374 );
	UxPutX( mn_rmax, 12 );
	UxPutRowColumnType( mn_rmax, "menu_option" );

	UxPutTranslations( tf_rmax, transTable2 );
	UxPutText( tf_rmax, "1." );
	UxPutForeground( tf_rmax, TextForeground );
	UxPutHighlightOnEnter( tf_rmax, "true" );
	UxPutFontList( tf_rmax, TextFont );
	UxPutBorderColor( tf_rmax, TextBackground );
	UxPutBackground( tf_rmax, TextBackground );
	UxPutHeight( tf_rmax, 35 );
	UxPutWidth( tf_rmax, 80 );
	UxPutY( tf_rmax, 372 );
	UxPutX( tf_rmax, 180 );

	UxPutForeground( tg_sphere, TextForeground );
	UxPutSpacing( tg_sphere, 0 );
	UxPutMarginWidth( tg_sphere, 0 );
	UxPutOrientation( tg_sphere, "horizontal" );
	UxPutIsAligned( tg_sphere, "true" );
	UxPutEntryAlignment( tg_sphere, "alignment_center" );
	UxPutBorderWidth( tg_sphere, 0 );
	UxPutShadowThickness( tg_sphere, 0 );
	UxPutLabelString( tg_sphere, "" );
	UxPutEntryBorder( tg_sphere, 0 );
	UxPutBorderColor( tg_sphere, WindowBackground );
	UxPutBackground( tg_sphere, WindowBackground );
	UxPutRadioBehavior( tg_sphere, "true" );
	UxPutWidth( tg_sphere, 188 );
	UxPutY( tg_sphere, 372 );
	UxPutX( tg_sphere, 364 );

	UxPutSpacing( tg_expanding, 0 );
	UxPutMarginWidth( tg_expanding, 0 );
	UxPutMarginLeft( tg_expanding, 0 );
	UxPutHighlightOnEnter( tg_expanding, "true" );
	UxPutForeground( tg_expanding, TextForeground );
	UxPutSelectColor( tg_expanding, SelectColor );
	UxPutSet( tg_expanding, "true" );
	UxPutLabelString( tg_expanding, "Expanding" );
	UxPutFontList( tg_expanding, TextFont );
	UxPutBorderColor( tg_expanding, WindowBackground );
	UxPutBackground( tg_expanding, WindowBackground );
	UxPutHeight( tg_expanding, 28 );
	UxPutWidth( tg_expanding, 94 );
	UxPutY( tg_expanding, 375 );
	UxPutX( tg_expanding, 0 );

	UxPutSpacing( tg_static, 0 );
	UxPutMarginWidth( tg_static, 0 );
	UxPutMarginLeft( tg_static, 0 );
	UxPutHighlightOnEnter( tg_static, "true" );
	UxPutSet( tg_static, "false" );
	UxPutForeground( tg_static, TextForeground );
	UxPutSelectColor( tg_static, SelectColor );
	UxPutLabelString( tg_static, "Static" );
	UxPutFontList( tg_static, TextFont );
	UxPutBorderColor( tg_static, WindowBackground );
	UxPutBackground( tg_static, WindowBackground );
	UxPutHeight( tg_static, 28 );
	UxPutWidth( tg_static, 94 );
	UxPutY( tg_static, 375 );
	UxPutX( tg_static, 87 );

	UxPutSpacing( tg_sphereon, 0 );
	UxPutMarginWidth( tg_sphereon, 0 );
	UxPutMarginLeft( tg_sphereon, 0 );
	UxPutHighlightOnEnter( tg_sphereon, "true" );
	UxPutForeground( tg_sphereon, TextForeground );
	UxPutSelectColor( tg_sphereon, SelectColor );
	UxPutSet( tg_sphereon, "true" );
	UxPutLabelString( tg_sphereon, "Sphere:" );
	UxPutFontList( tg_sphereon, TextFont );
	UxPutBorderColor( tg_sphereon, WindowBackground );
	UxPutBackground( tg_sphereon, WindowBackground );
	UxPutHeight( tg_sphereon, 28 );
	UxPutWidth( tg_sphereon, 76 );
	UxPutY( tg_sphereon, 375 );
	UxPutX( tg_sphereon, 288 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_CloudyModel()
{
	/* Create the swidgets */

	CloudyModel = UxCreateApplicationShell( "CloudyModel", NO_PARENT );
	UxPutContext( CloudyModel, UxCloudyModelContext );

	form_CM = UxCreateForm( "form_CM", CloudyModel );
	lbl_Ccopyright = UxCreateLabel( "lbl_Ccopyright", form_CM );
	form_CM1 = UxCreateForm( "form_CM1", form_CM );
	CM_apply = UxCreatePushButton( "CM_apply", form_CM1 );
	lbl_inc_cont = UxCreateLabel( "lbl_inc_cont", form_CM );
	lbl_chemic = UxCreateLabel( "lbl_chemic", form_CM );
	lbl_range = UxCreateLabel( "lbl_range", form_CM );
	tg_geometry = UxCreateRowColumn( "tg_geometry", form_CM );
	tg_spheric = UxCreateToggleButton( "tg_spheric", tg_geometry );
	tg_plan = UxCreateToggleButton( "tg_plan", tg_geometry );
	tf_Cwmin = UxCreateText( "tf_Cwmin", form_CM );
	tf_Cwmax = UxCreateText( "tf_Cwmax", form_CM );
	helptextCM = UxCreateText( "helptextCM", form_CM );
	separatorCM1 = UxCreateSeparator( "separatorCM1", form_CM );
	mn_comp = UxCreateRowColumn( "mn_comp", form_CM );
	mn_solar = UxCreatePushButton( "mn_solar", mn_comp );
	mn_cameron = UxCreatePushButton( "mn_cameron", mn_comp );
	mn_h2 = UxCreatePushButton( "mn_h2", mn_comp );
	mn_nova = UxCreatePushButton( "mn_nova", mn_comp );
	mn_pnebula = UxCreatePushButton( "mn_pnebula", mn_comp );
	mn_aism = UxCreatePushButton( "mn_aism", mn_comp );
	mn_primordial = UxCreatePushButton( "mn_primordial", mn_comp );
	m_composition = UxCreateRowColumn( "m_composition", form_CM );
	lbl_geometry = UxCreateLabel( "lbl_geometry", form_CM );
	tf_metal = UxCreateText( "tf_metal", form_CM );
	separatorCM3 = UxCreateSeparator( "separatorCM3", form_CM );
	separatorCM4 = UxCreateSeparator( "separatorCM4", form_CM );
	separatorCM5 = UxCreateSeparator( "separatorCM5", form_CM );
	mn_continuum = UxCreateRowColumn( "mn_continuum", form_CM );
	mn_background = UxCreatePushButton( "mn_background", mn_continuum );
	mn_bbody = UxCreatePushButton( "mn_bbody", mn_continuum );
	mn_brem = UxCreatePushButton( "mn_brem", mn_continuum );
	mn_fireball = UxCreatePushButton( "mn_fireball", mn_continuum );
	mn_powerlaw = UxCreatePushButton( "mn_powerlaw", mn_continuum );
	mn_agn = UxCreatePushButton( "mn_agn", mn_continuum );
	mn_crab = UxCreatePushButton( "mn_crab", mn_continuum );
	mn_cflow = UxCreatePushButton( "mn_cflow", mn_continuum );
	mn_kurucz = UxCreatePushButton( "mn_kurucz", mn_continuum );
	mn_mihalas = UxCreatePushButton( "mn_mihalas", mn_continuum );
	mn_atlas = UxCreatePushButton( "mn_atlas", mn_continuum );
	mn_werner = UxCreatePushButton( "mn_werner", mn_continuum );
	mn_ism = UxCreatePushButton( "mn_ism", mn_continuum );
	m_continuum = UxCreateRowColumn( "m_continuum", form_CM );
	lbl_lum = UxCreateLabel( "lbl_lum", form_CM );
	mn_lum = UxCreateRowColumn( "mn_lum", form_CM );
	mn_luminosity = UxCreatePushButton( "mn_luminosity", mn_lum );
	mn_flux = UxCreatePushButton( "mn_flux", mn_lum );
	mn_bolo = UxCreatePushButton( "mn_bolo", mn_lum );
	mn_visual = UxCreatePushButton( "mn_visual", mn_lum );
	mn_endensity = UxCreatePushButton( "mn_endensity", mn_lum );
	mn_photon = UxCreatePushButton( "mn_photon", mn_lum );
	m_lum = UxCreateRowColumn( "m_lum", form_CM );
	separatorCM2 = UxCreateSeparator( "separatorCM2", form_CM );
	menu_metal = UxCreateRowColumn( "menu_metal", form_CM );
	mn_metalonly = UxCreatePushButton( "mn_metalonly", menu_metal );
	mn_metalgrain = UxCreatePushButton( "mn_metalgrain", menu_metal );
	mn_metal = UxCreateRowColumn( "mn_metal", form_CM );
	meCloudy = UxCreateRowColumn( "meCloudy", form_CM );
	me_c_file = UxCreateRowColumn( "me_c_file", meCloudy );
	me_c_open = UxCreatePushButton( "me_c_open", me_c_file );
	mt_c_file = UxCreateCascadeButton( "mt_c_file", meCloudy );
	me_c_help = UxCreateRowColumn( "me_c_help", meCloudy );
	me_c_hcloudy = UxCreatePushButton( "me_c_hcloudy", me_c_help );
	mt_c_help = UxCreateCascadeButton( "mt_c_help", meCloudy );
	me_c_quit = UxCreateRowColumn( "me_c_quit", meCloudy );
	me_c_bye = UxCreatePushButton( "me_c_bye", me_c_quit );
	mt_c_quit = UxCreateCascadeButton( "mt_c_quit", meCloudy );
	tf_contpar = UxCreateText( "tf_contpar", form_CM );
	lbl_contpar = UxCreateLabel( "lbl_contpar", form_CM );
	lbl_density = UxCreateLabel( "lbl_density", form_CM );
	mn_density = UxCreateRowColumn( "mn_density", form_CM );
	mn_cdens = UxCreatePushButton( "mn_cdens", mn_density );
	mn_cgazpres = UxCreatePushButton( "mn_cgazpres", mn_density );
	mn_cpres = UxCreatePushButton( "mn_cpres", mn_density );
	mn_globule = UxCreatePushButton( "mn_globule", mn_density );
	m_density = UxCreateRowColumn( "m_density", form_CM );
	lbl_intpar = UxCreateLabel( "lbl_intpar", form_CM );
	tf_intpar = UxCreateText( "tf_intpar", form_CM );
	lbl_extd = UxCreateLabel( "lbl_extd", form_CM );
	tf_extd = UxCreateText( "tf_extd", form_CM );
	lbl_hden = UxCreateLabel( "lbl_hden", form_CM );
	tf_hden = UxCreateText( "tf_hden", form_CM );
	lbl_cover = UxCreateLabel( "lbl_cover", form_CM );
	tf_cover = UxCreateText( "tf_cover", form_CM );
	lbl_fill = UxCreateLabel( "lbl_fill", form_CM );
	tf_fill = UxCreateText( "tf_fill", form_CM );
	menu_rmin = UxCreateRowColumn( "menu_rmin", form_CM );
	mn_rmincm = UxCreatePushButton( "mn_rmincm", menu_rmin );
	mn_rminpc = UxCreatePushButton( "mn_rminpc", menu_rmin );
	mn_rmin = UxCreateRowColumn( "mn_rmin", form_CM );
	tf_rmin = UxCreateText( "tf_rmin", form_CM );
	menu_rmax = UxCreateRowColumn( "menu_rmax", form_CM );
	mn_rmaxcm = UxCreatePushButton( "mn_rmaxcm", menu_rmax );
	mn_rmaxpc = UxCreatePushButton( "mn_rmaxpc", menu_rmax );
	mn_rmax = UxCreateRowColumn( "mn_rmax", form_CM );
	tf_rmax = UxCreateText( "tf_rmax", form_CM );
	tg_sphere = UxCreateRowColumn( "tg_sphere", form_CM );
	tg_expanding = UxCreateToggleButton( "tg_expanding", tg_sphere );
	tg_static = UxCreateToggleButton( "tg_static", tg_sphere );
	tg_sphereon = UxCreateToggleButton( "tg_sphereon", form_CM );

	_Uxinit_CloudyModel();

	/* Create the X widgets */

	UxCreateWidget( CloudyModel );
	UxCreateWidget( form_CM );
	UxPutTopAttachment( lbl_Ccopyright, "attach_none" );
	UxPutRightAttachment( lbl_Ccopyright, "attach_form" );
	UxPutLeftAttachment( lbl_Ccopyright, "attach_form" );
	UxCreateWidget( lbl_Ccopyright );

	UxPutRightAttachment( form_CM1, "attach_form" );
	UxPutLeftAttachment( form_CM1, "attach_form" );
	UxPutBottomOffset( form_CM1, 2 );
	UxPutBottomAttachment( form_CM1, "attach_form" );
	UxCreateWidget( form_CM1 );

	UxCreateWidget( CM_apply );
	UxCreateWidget( lbl_inc_cont );
	UxCreateWidget( lbl_chemic );
	UxCreateWidget( lbl_range );
	UxCreateWidget( tg_geometry );
	UxCreateWidget( tg_spheric );
	UxCreateWidget( tg_plan );
	UxCreateWidget( tf_Cwmin );
	UxCreateWidget( tf_Cwmax );
	UxPutRightAttachment( helptextCM, "attach_form" );
	UxPutLeftAttachment( helptextCM, "attach_form" );
	UxPutBottomWidget( helptextCM, "form_CM1" );
	UxPutBottomOffset( helptextCM, 2 );
	UxPutBottomAttachment( helptextCM, "attach_widget" );
	UxCreateWidget( helptextCM );

	UxPutRightAttachment( separatorCM1, "attach_form" );
	UxPutLeftAttachment( separatorCM1, "attach_form" );
	UxPutBottomWidget( separatorCM1, "helptextCM" );
	UxPutBottomAttachment( separatorCM1, "attach_widget" );
	UxCreateWidget( separatorCM1 );

	UxCreateWidget( mn_comp );
	UxCreateWidget( mn_solar );
	UxCreateWidget( mn_cameron );
	UxCreateWidget( mn_h2 );
	UxCreateWidget( mn_nova );
	UxCreateWidget( mn_pnebula );
	UxCreateWidget( mn_aism );
	UxCreateWidget( mn_primordial );
	UxPutSubMenuId( m_composition, "mn_comp" );
	UxCreateWidget( m_composition );

	UxCreateWidget( lbl_geometry );
	UxCreateWidget( tf_metal );
	UxPutRightAttachment( separatorCM3, "attach_form" );
	UxPutLeftAttachment( separatorCM3, "attach_form" );
	UxCreateWidget( separatorCM3 );

	UxPutRightAttachment( separatorCM4, "attach_form" );
	UxPutLeftAttachment( separatorCM4, "attach_form" );
	UxCreateWidget( separatorCM4 );

	UxPutRightAttachment( separatorCM5, "attach_form" );
	UxPutLeftAttachment( separatorCM5, "attach_form" );
	UxCreateWidget( separatorCM5 );

	UxCreateWidget( mn_continuum );
	UxCreateWidget( mn_background );
	UxCreateWidget( mn_bbody );
	UxCreateWidget( mn_brem );
	UxCreateWidget( mn_fireball );
	UxCreateWidget( mn_powerlaw );
	UxCreateWidget( mn_agn );
	UxCreateWidget( mn_crab );
	UxCreateWidget( mn_cflow );
	UxCreateWidget( mn_kurucz );
	UxCreateWidget( mn_mihalas );
	UxCreateWidget( mn_atlas );
	UxCreateWidget( mn_werner );
	UxCreateWidget( mn_ism );
	UxPutSubMenuId( m_continuum, "mn_continuum" );
	UxCreateWidget( m_continuum );

	UxCreateWidget( lbl_lum );
	UxCreateWidget( mn_lum );
	UxCreateWidget( mn_luminosity );
	UxCreateWidget( mn_flux );
	UxCreateWidget( mn_bolo );
	UxCreateWidget( mn_visual );
	UxCreateWidget( mn_endensity );
	UxCreateWidget( mn_photon );
	UxPutSubMenuId( m_lum, "mn_lum" );
	UxCreateWidget( m_lum );

	UxPutRightAttachment( separatorCM2, "attach_form" );
	UxPutLeftAttachment( separatorCM2, "attach_form" );
	UxCreateWidget( separatorCM2 );

	UxCreateWidget( menu_metal );
	UxCreateWidget( mn_metalonly );
	UxCreateWidget( mn_metalgrain );
	UxPutSubMenuId( mn_metal, "menu_metal" );
	UxCreateWidget( mn_metal );

	UxPutTopAttachment( meCloudy, "attach_form" );
	UxPutRightAttachment( meCloudy, "attach_form" );
	UxPutLeftAttachment( meCloudy, "attach_form" );
	UxCreateWidget( meCloudy );

	UxCreateWidget( me_c_file );
	UxCreateWidget( me_c_open );
	UxPutSubMenuId( mt_c_file, "me_c_file" );
	UxCreateWidget( mt_c_file );

	UxCreateWidget( me_c_help );
	UxCreateWidget( me_c_hcloudy );
	UxPutSubMenuId( mt_c_help, "me_c_help" );
	UxCreateWidget( mt_c_help );

	UxCreateWidget( me_c_quit );
	UxCreateWidget( me_c_bye );
	UxPutSubMenuId( mt_c_quit, "me_c_quit" );
	UxCreateWidget( mt_c_quit );

	UxCreateWidget( tf_contpar );
	UxCreateWidget( lbl_contpar );
	UxCreateWidget( lbl_density );
	UxCreateWidget( mn_density );
	UxCreateWidget( mn_cdens );
	UxCreateWidget( mn_cgazpres );
	UxCreateWidget( mn_cpres );
	UxCreateWidget( mn_globule );
	UxPutSubMenuId( m_density, "mn_density" );
	UxCreateWidget( m_density );

	UxCreateWidget( lbl_intpar );
	UxCreateWidget( tf_intpar );
	UxCreateWidget( lbl_extd );
	UxCreateWidget( tf_extd );
	UxCreateWidget( lbl_hden );
	UxCreateWidget( tf_hden );
	UxCreateWidget( lbl_cover );
	UxCreateWidget( tf_cover );
	UxCreateWidget( lbl_fill );
	UxCreateWidget( tf_fill );
	UxCreateWidget( menu_rmin );
	UxCreateWidget( mn_rmincm );
	UxCreateWidget( mn_rminpc );
	UxPutSubMenuId( mn_rmin, "menu_rmin" );
	UxCreateWidget( mn_rmin );

	UxCreateWidget( tf_rmin );
	UxCreateWidget( menu_rmax );
	UxCreateWidget( mn_rmaxcm );
	UxCreateWidget( mn_rmaxpc );
	UxPutSubMenuId( mn_rmax, "menu_rmax" );
	UxCreateWidget( mn_rmax );

	UxCreateWidget( tf_rmax );
	UxCreateWidget( tg_sphere );
	UxCreateWidget( tg_expanding );
	UxCreateWidget( tg_static );
	UxCreateWidget( tg_sphereon );

	UxAddCallback( CM_apply, XmNactivateCallback,
			activateCB_CM_apply,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tg_spheric, XmNarmCallback,
			armCB_tg_spheric,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tg_plan, XmNarmCallback,
			armCB_tg_plan,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tg_plan, XmNvalueChangedCallback,
			valueChangedCB_tg_plan,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tf_Cwmin, XmNfocusCallback,
			focusCB_tf_Cwmin,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_Cwmin, XmNlosingFocusCallback,
			losingFocusCB_tf_Cwmin,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tf_Cwmax, XmNfocusCallback,
			focusCB_tf_Cwmax,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_Cwmax, XmNlosingFocusCallback,
			losingFocusCB_tf_Cwmax,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_solar, XmNactivateCallback,
			activateCB_mn_solar,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_cameron, XmNactivateCallback,
			activateCB_mn_cameron,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_h2, XmNactivateCallback,
			activateCB_mn_h2,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_nova, XmNactivateCallback,
			activateCB_mn_nova,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_pnebula, XmNactivateCallback,
			activateCB_mn_pnebula,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_aism, XmNactivateCallback,
			activateCB_mn_aism,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_primordial, XmNactivateCallback,
			activateCB_mn_primordial,
			(XtPointer) UxCloudyModelContext );

	UxPutMenuHistory( m_composition, "mn_solar" );

	UxAddCallback( tf_metal, XmNfocusCallback,
			focusCB_tf_metal,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_metal, XmNlosingFocusCallback,
			losingFocusCB_tf_metal,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_background, XmNactivateCallback,
			activateCB_mn_background,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_bbody, XmNactivateCallback,
			activateCB_mn_bbody,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_brem, XmNactivateCallback,
			activateCB_mn_brem,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_fireball, XmNactivateCallback,
			activateCB_mn_fireball,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_powerlaw, XmNactivateCallback,
			activateCB_mn_powerlaw,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_agn, XmNactivateCallback,
			activateCB_mn_agn,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_crab, XmNactivateCallback,
			activateCB_mn_crab,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_cflow, XmNactivateCallback,
			activateCB_mn_cflow,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_kurucz, XmNactivateCallback,
			activateCB_mn_kurucz,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_mihalas, XmNactivateCallback,
			activateCB_mn_mihalas,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_atlas, XmNactivateCallback,
			activateCB_mn_atlas,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_werner, XmNactivateCallback,
			activateCB_mn_werner,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_ism, XmNactivateCallback,
			activateCB_mn_ism,
			(XtPointer) UxCloudyModelContext );

	UxPutMenuHistory( m_continuum, "mn_background" );

	UxAddCallback( mn_luminosity, XmNactivateCallback,
			activateCB_mn_luminosity,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_flux, XmNactivateCallback,
			activateCB_mn_flux,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_bolo, XmNactivateCallback,
			activateCB_mn_bolo,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_visual, XmNactivateCallback,
			activateCB_mn_visual,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_endensity, XmNactivateCallback,
			activateCB_mn_endensity,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_photon, XmNactivateCallback,
			activateCB_mn_photon,
			(XtPointer) UxCloudyModelContext );

	UxPutMenuHistory( m_lum, "mn_luminosity" );

	UxAddCallback( mn_metalonly, XmNactivateCallback,
			activateCB_mn_metalonly,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_metalgrain, XmNactivateCallback,
			activateCB_mn_metalgrain,
			(XtPointer) UxCloudyModelContext );

	UxPutMenuHistory( mn_metal, "mn_metalonly" );

	UxPutMenuHelpWidget( meCloudy, "mt_c_quit" );

	UxAddCallback( me_c_open, XmNactivateCallback,
			activateCB_me_c_open,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( me_c_hcloudy, XmNactivateCallback,
			activateCB_me_c_hcloudy,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( me_c_bye, XmNactivateCallback,
			activateCB_me_c_bye,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tf_contpar, XmNfocusCallback,
			focusCB_tf_contpar,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_contpar, XmNlosingFocusCallback,
			losingFocusCB_tf_contpar,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_cdens, XmNactivateCallback,
			activateCB_mn_cdens,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_cgazpres, XmNactivateCallback,
			activateCB_mn_cgazpres,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_cpres, XmNactivateCallback,
			activateCB_mn_cpres,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( mn_globule, XmNactivateCallback,
			activateCB_mn_globule,
			(XtPointer) UxCloudyModelContext );

	UxPutMenuHistory( m_density, "mn_cdens" );

	UxAddCallback( tf_intpar, XmNfocusCallback,
			focusCB_tf_intpar,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_intpar, XmNlosingFocusCallback,
			losingFocusCB_tf_intpar,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tf_extd, XmNfocusCallback,
			focusCB_tf_extd,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_extd, XmNlosingFocusCallback,
			losingFocusCB_tf_extd,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tf_hden, XmNfocusCallback,
			focusCB_tf_hden,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_hden, XmNlosingFocusCallback,
			losingFocusCB_tf_hden,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tf_cover, XmNfocusCallback,
			focusCB_tf_cover,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_cover, XmNlosingFocusCallback,
			losingFocusCB_tf_cover,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tf_fill, XmNfocusCallback,
			focusCB_tf_fill,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_fill, XmNlosingFocusCallback,
			losingFocusCB_tf_fill,
			(XtPointer) UxCloudyModelContext );

	UxPutMenuHistory( mn_rmin, "mn_rmincm" );

	UxAddCallback( tf_rmin, XmNfocusCallback,
			focusCB_tf_rmin,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_rmin, XmNlosingFocusCallback,
			losingFocusCB_tf_rmin,
			(XtPointer) UxCloudyModelContext );

	UxPutMenuHistory( mn_rmax, "mn_rmaxcm" );

	UxAddCallback( tf_rmax, XmNfocusCallback,
			focusCB_tf_rmax,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tf_rmax, XmNlosingFocusCallback,
			losingFocusCB_tf_rmax,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tg_expanding, XmNarmCallback,
			armCB_tg_expanding,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tg_static, XmNarmCallback,
			armCB_tg_static,
			(XtPointer) UxCloudyModelContext );
	UxAddCallback( tg_static, XmNvalueChangedCallback,
			valueChangedCB_tg_static,
			(XtPointer) UxCloudyModelContext );

	UxAddCallback( tg_sphereon, XmNarmCallback,
			armCB_tg_sphereon,
			(XtPointer) UxCloudyModelContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( CloudyModel );

	return ( CloudyModel );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_CloudyModel()
{
	swidget                 rtrn;
	_UxCCloudyModel         *UxContext;

	UxCloudyModelContext = UxContext =
		(_UxCCloudyModel *) UxMalloc( sizeof(_UxCCloudyModel) );

	rtrn = _Uxbuild_CloudyModel();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_CloudyModel()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearShortCM", action_ClearShortCM },
				{ "HelpShortCM", action_HelpShortCM },
				{ "HelpHelp", action_HelpHelp }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_CloudyModel();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

