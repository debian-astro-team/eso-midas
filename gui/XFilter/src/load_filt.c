/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826		last modif

===========================================================================*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <globaldef.h>
#include <model.h>


extern void endname(), set_filter();

extern int flire();



void	load_filt(name)
char	name[];

{
extern	struct s_filter	F;

int	end,fmot();
char	term[10],first[10],second[50];
FILE	*IN;

endname(name,term);

if (strcmp(term,"flt"))
	strcat(name,".flt");

IN=fopen(name,"r");

if (IN == (FILE *)0)
	fprintf(stderr,"WARNING: file does not exist\r\n");
else
{

fscanf(IN,"%s",first); flire(IN,second);

do{
	end=fmot(IN,first);

	if (!strcmp(first,"cw"))
		{
		fscanf(IN,"%f",&F.cw); flire(IN,second);
		}
	else if (!strcmp(first,"bw"))
		{
		fscanf(IN,"%f",&F.bw); flire(IN,second);
		}
	else if (!strcmp(first,"peak"))
		{
		fscanf(IN,"%f",&F.peak); flire(IN,second);
		}
	else if (!strcmp(first,"cwl"))
		{
		fscanf(IN,"%f",&F.cwl); flire(IN,second);
		}
	else if (!strcmp(first,"bwl"))
		{
		fscanf(IN,"%f",&F.bwl); flire(IN,second);
		}
	else if (!strcmp(first,"peakl"))
		{
		fscanf(IN,"%f",&F.peakl); flire(IN,second);
		};
	  }
	while(end!=-1);

fclose(IN);

set_filter();
};

}
