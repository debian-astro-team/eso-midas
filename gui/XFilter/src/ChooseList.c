/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	ChooseList.c

.VERSION
051025		last modif

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "UxLib.h"
#include "UxPushB.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <modeldef.h>
#include <model.h>
#include <global.h>



extern void plot_filters();
extern void plot_spec(), plot_spec2();
extern void load_mod(), load_curve(), load_filt(), base_name();

extern int search_nfilt();



extern struct s_gui G;
extern struct s_trans T;
extern struct s_bruzual B;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	int	mumble;
} _UxCChooseList;


static _UxCChooseList	*UxChooseListContext;

swidget	ChooseList;
swidget	form19;
swidget	choosewindow;
swidget	scrolledchoose;
swidget	pushButton6;

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ChooseList();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	browseSelectionCB_scrolledchoose( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCChooseList          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxChooseListContext;
	UxChooseListContext = UxContext =
			(_UxCChooseList *) UxGetContext( UxThisWidget );
	{
	extern	char Ask[];
	char *path,*choice;
	XmListCallbackStruct *cbs;
	char n_select[5];
	int nid;
	
	
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	
	XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);
	
	sscanf(choice,"%s",n_select);
	
	if (!strcmp(G.sel_type,"filter"))
		{
		UxPutText(UxFindSwidget("tf_filter"),n_select); 
		nid=sscanf(n_select,"%d",&T.nfilter);
		if (nid!=0)
			{
	                if (search_nfilt(T.nfilter)!=-1)
				{
				T.ifilter=1;
		                plot_filters(T.nfilter);
				MOD_FilterNUMBER=FALSE;
				}
			else
				T.ifilter=0;
	                }
		else
			T.ifilter=0;
		}
	else if (!strcmp(G.sel_type,"hststd"))
		{
		nid=sscanf(n_select,"%s.tbl",T.spectrafile);
		if (nid==1)
	                {
			T.ispectra=1;
	                path=(char *)getenv("MID_FILTERS");
	                sprintf((char *)T.spectrapath,"%s/hststd",path);
			plot_spec(T.spectrapath,T.spectrafile);
			UxPutText(UxFindSwidget("tf_spectrum"),T.spectrafile);
	
			MOD_spectrum=FALSE;
	                }
		else
			T.ispectra=0;
		}
	else if (!strcmp(G.sel_type,"ctiostd"))
		{
		nid=sscanf(n_select,"%s.tbl",T.spectrafile);
		if (nid==1)
	                {
			T.ispectra=1;
	                path=(char *)getenv("MID_FILTERS");
	                sprintf((char *)T.spectrapath,"%s/ctiostd",path);
			plot_spec2(T.spectrapath,T.spectrafile);
			UxPutText(UxFindSwidget("tf_spectrum"),T.spectrafile);
	
			MOD_spectrum=FALSE;
	                }
		else
			T.ispectra=0;
		}
	else if (!strcmp(G.sel_type,"spectrum"))
		{
		nid=sscanf(n_select,"%s.tbl",T.spectrafile);
		if (nid==1)
	                {
			T.ispectra=1;
	                path=(char *)getenv("MID_FILTERS");
	                sprintf((char *)T.spectrapath,"%s/esostd",path);
			plot_spec(T.spectrapath,T.spectrafile);
			UxPutText(UxFindSwidget("tf_spectrum"),T.spectrafile);
	
			MOD_spectrum=FALSE;
	                }
		else
			T.ispectra=0;
		}
	else if (!strcmp(G.sel_type,"trans_load"))
		{
		nid=sscanf(n_select,"%s.trans",Ask);
		strcpy(T.transfile,Ask);
		load_mod(Ask);
		}
	else if (!strcmp(G.sel_type,"curve_load"))
		{
		nid=sscanf(n_select,"%s",Ask);
		strcpy(T.curvefile,Ask);
		load_curve(Ask);
		}
	else if (!strcmp(G.sel_type,"filter_load"))
		{
		nid=sscanf(n_select,"%s.flt",Ask);
		UxPutText(UxFindSwidget("textAsk"),Ask);
		load_filt(Ask);
		}
	else if (!strcmp(G.sel_type,"ised"))
		{
		B.ised=1;
		nid=sscanf(n_select,"%s.ised",B.sedfile);
		base_name(B.sedfile,B.cspfile);
		UxPutText(UxFindSwidget("tf_ised"),B.sedfile);
		}
	else if (!strcmp(G.sel_type,"flux"))
		{
		B.ioutput=1;
		nid=sscanf(n_select,"%s.flx",B.outfile);
		UxPutText(UxFindSwidget("tf_output"),B.outfile);
		};
	}
	UxChooseListContext = UxSaveCtx;
}

static void	activateCB_pushButton6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCChooseList          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxChooseListContext;
	UxChooseListContext = UxContext =
			(_UxCChooseList *) UxGetContext( UxThisWidget );
	{
	extern swidget ListPopup;
	
	UxPopdownInterface(ListPopup);
	}
	UxChooseListContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ChooseList()
{
	UxPutDefaultFontList( ChooseList, TextFont );
	UxPutBorderColor( ChooseList, WindowBackground );
	UxPutBackground( ChooseList, SHelpBackground );
	UxPutKeyboardFocusPolicy( ChooseList, "pointer" );
	UxPutIconName( ChooseList, "List selection" );
	UxPutHeight( ChooseList, 350 );
	UxPutWidth( ChooseList, 378 );
	UxPutY( ChooseList, 571 );
	UxPutX( ChooseList, 402 );

	UxPutForeground( form19, TextForeground );
	UxPutBorderColor( form19, WindowBackground );
	UxPutBackground( form19, WindowBackground );
	UxPutHeight( form19, 240 );
	UxPutWidth( form19, 350 );
	UxPutY( form19, 0 );
	UxPutX( form19, 0 );
	UxPutUnitType( form19, "pixels" );
	UxPutResizePolicy( form19, "resize_none" );

	UxPutWidth( choosewindow, 376 );
	UxPutTopShadowColor( choosewindow, "Black" );
	UxPutBottomShadowColor( choosewindow, WindowBackground );
	UxPutForeground( choosewindow, TextForeground );
	UxPutHeight( choosewindow, 314 );
	UxPutBorderColor( choosewindow, WindowBackground );
	UxPutBackground( choosewindow, WindowBackground );
	UxPutScrollBarPlacement( choosewindow, "bottom_left" );
	UxPutShadowThickness( choosewindow, 0 );
	UxPutScrollBarDisplayPolicy( choosewindow, "static" );
	UxPutVisualPolicy( choosewindow, "variable" );
	UxPutY( choosewindow, 2 );
	UxPutX( choosewindow, 0 );
	UxPutScrollingPolicy( choosewindow, "application_defined" );

	UxPutHighlightColor( scrolledchoose, "Black" );
	UxPutForeground( scrolledchoose, TextForeground );
	UxPutVisibleItemCount( scrolledchoose, 18 );
	UxPutFontList( scrolledchoose, SmallFont );
	UxPutBackground( scrolledchoose, SHelpBackground );
	UxPutListSizePolicy( scrolledchoose, "constant" );
	UxPutScrollBarDisplayPolicy( scrolledchoose, "static" );
	UxPutHeight( scrolledchoose, 295 );
	UxPutWidth( scrolledchoose, 357 );

	UxPutRecomputeSize( pushButton6, "false" );
	UxPutHighlightOnEnter( pushButton6, "true" );
	UxPutForeground( pushButton6, CancelForeground );
	UxPutLabelString( pushButton6, "Cancel" );
	UxPutFontList( pushButton6, BoldTextFont );
	UxPutBorderColor( pushButton6, ButtonBackground );
	UxPutBackground( pushButton6, ButtonBackground );
	UxPutHeight( pushButton6, 35 );
	UxPutWidth( pushButton6, 348 );
	UxPutY( pushButton6, 314 );
	UxPutX( pushButton6, 0 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ChooseList()
{
	/* Create the swidgets */

	ChooseList = UxCreateTopLevelShell( "ChooseList", NO_PARENT );
	UxPutContext( ChooseList, UxChooseListContext );

	form19 = UxCreateForm( "form19", ChooseList );
	choosewindow = UxCreateScrolledWindow( "choosewindow", form19 );
	scrolledchoose = UxCreateScrolledList( "scrolledchoose", choosewindow );
	pushButton6 = UxCreatePushButton( "pushButton6", form19 );

	_Uxinit_ChooseList();

	/* Create the X widgets */

	UxCreateWidget( ChooseList );
	UxCreateWidget( form19 );
	UxCreateWidget( choosewindow );
	UxCreateWidget( scrolledchoose );
	UxPutTopWidget( pushButton6, "choosewindow" );
	UxPutTopOffset( pushButton6, 1 );
	UxPutTopAttachment( pushButton6, "attach_widget" );
	UxPutRightOffset( pushButton6, 1 );
	UxPutRightAttachment( pushButton6, "attach_form" );
	UxPutLeftOffset( pushButton6, 1 );
	UxPutLeftAttachment( pushButton6, "attach_form" );
	UxPutBottomOffset( pushButton6, 1 );
	UxPutBottomAttachment( pushButton6, "attach_form" );
	UxCreateWidget( pushButton6 );


	UxAddCallback( scrolledchoose, XmNbrowseSelectionCallback,
			browseSelectionCB_scrolledchoose,
			(XtPointer) UxChooseListContext );

	UxAddCallback( pushButton6, XmNactivateCallback,
			activateCB_pushButton6,
			(XtPointer) UxChooseListContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ChooseList );

	return ( ChooseList );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ChooseList()
{
	swidget                 rtrn;
	_UxCChooseList          *UxContext;

	UxChooseListContext = UxContext =
		(_UxCChooseList *) UxMalloc( sizeof(_UxCChooseList) );

	rtrn = _Uxbuild_ChooseList();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ChooseList()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_ChooseList();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

