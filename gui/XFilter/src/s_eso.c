/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include<stdio.h>

extern int search_nfilt();

extern void plot_ccd(), plot_filters(), end_graphic();





void	s_esofilter(word)
char	word[];

{
int num_filt;

sscanf(word,"%d",&num_filt);
search_nfilt(num_filt);
plot_filters(num_filt);
end_graphic();
}

void	s_esoccd(word)
char	word[];

{
extern int num_ccd;

sscanf(word,"%d",&num_ccd);
plot_ccd(num_ccd);
end_graphic();
}
