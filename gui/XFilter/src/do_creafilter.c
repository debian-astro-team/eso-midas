/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <stdio.h>
#include <math.h>
#include <string.h>
#include <UxLib.h>
#include <model.h>
#include <globaldef.h>

extern void plot_xy(), end_graphic1();



void	do_creafilter()

{
extern	struct	s_filter F;
extern	struct	s_trans T;
extern  float   Xfilt[],Yfilt[];
extern	int	Nfilter;
extern  float        Oxmi,Oxma,Oymi,Oyma;

register int	i;
char	str1[30],str[150];
float	I,pas,wmin,wmax,bias;

UxPutText(UxFindSwidget("tf_filter"),"crea_filter");

Nfilter=1001;
wmin=Min(F.cw-5.*F.bw,F.cwl-5.*F.bwl);
wmax=Max(F.cw+5.*F.bw,F.cwl+5.*F.bwl);
pas=(wmax-wmin)/(Nfilter-1);

for(i=0;i<Nfilter;i++)
	{
	I=i;
	Xfilt[i]=wmin+pas*I;
	Yfilt[i]=F.peak/100.*
	  ( exp(-2.*(Xfilt[i]-F.cw)*(Xfilt[i]-F.cw)/F.bw/F.bw) +
	  F.peakl/100.*exp(-2.*(Xfilt[i]-F.cwl)*(Xfilt[i]-F.cwl)/F.bwl/F.bwl) );
	};


T.ifilter=2;

    sprintf(str1,"\1FILTER  Transmission Curve");
    sprintf(str,
     "TITLE=%18s;DTITL=%4.1f;LABX=%12s;LABY=%12s;DLABL=%4.1f;NGEOM;USER;GRID;XSPACE=%1d",
    str1, 2.0, "\1Lambda [nm]", "\1Flux[%1]",1.0,0);

     bias=(wmax-wmin)/16.;
     wmin-=bias;
     wmax+=bias;

     plot_xy(str,0,Xfilt, Yfilt, Nfilter,wmin,wmax,-.05,1.0);
   end_graphic1();
   Oxmi=wmin;
   Oxma=wmax;
   Oymi=-.05;
   Oyma=1.0;

}
