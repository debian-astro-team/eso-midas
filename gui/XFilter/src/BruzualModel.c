/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	BruzualModel.c

.VERSION
 090826         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxCascB.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxPushB.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <model.h>
#include <global.h>

extern  struct s_gui G;
extern  struct s_trans T;
extern  struct s_bruzual B;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxBruzualModel;
	swidget	Uxform_BM;
	swidget	Uxlbl_copyright;
	swidget	Uxform_BM2;
	swidget	UxBM_newised;
	swidget	UxBM_apply;
	swidget	Uxlbl_sfr_c;
	swidget	Uxlbl_ssp;
	swidget	Uxlbl_sfr;
	swidget	Uxlabel1;
	swidget	Uxlbl_output;
	swidget	Uxlbl_H0;
	swidget	Uxlbl_age;
	swidget	Uxgaz_rec;
	swidget	Uxtg_gaz;
	swidget	Uxtg_nogaz;
	swidget	Uxtf_wvmin;
	swidget	Uxtf_wvmax;
	swidget	Uxtf_age;
	swidget	Uxtf_omega;
	swidget	Uxtf_z;
	swidget	Uxtf_H0;
	swidget	UxhelptextBM;
	swidget	UxseparatorBM;
	swidget	Uxmenu_SFR;
	swidget	Uxmn_exptau;
	swidget	Uxmn_expmu;
	swidget	Uxmn_single;
	swidget	Uxmn_constant;
	swidget	Uxmn_SFR;
	swidget	Uxlbl_gaz;
	swidget	Uxtf_sfr_c;
	swidget	UxseparatorBM1;
	swidget	Uxlbl_Omega1;
	swidget	Uxtf_output;
	swidget	UxseparatorBM2;
	swidget	UxseparatorBM3;
	swidget	Uxmenu_IMF;
	swidget	Uxmn_sp;
	swidget	Uxmn_sc;
	swidget	Uxmn_ms;
	swidget	Uxmn_IMF;
	swidget	Uxlbl_mass;
	swidget	Uxmenu_mass;
	swidget	Uxmn_m1;
	swidget	Uxmn_m2;
	swidget	Uxmn_m3;
	swidget	Uxmn_m4;
	swidget	Uxmn_m5;
	swidget	Uxmn_mass;
	swidget	Uxlbl_stpop;
	swidget	Uxstellar_pop;
	swidget	Uxtg_ssp;
	swidget	Uxtg_csp;
	swidget	UxseparatorBM4;
	swidget	Uxlbl_massgal;
	swidget	Uxtf_massgal;
	swidget	Uxmenu_dist;
	swidget	Uxmn_z;
	swidget	Uxmn_Mpc;
	swidget	Uxmn_dist;
	swidget	UxseparatorBM5;
	swidget	Uxlbl_ised;
	swidget	Uxtf_ised;
	swidget	UxmeBruzual;
	swidget	Uxme_b_file;
	swidget	Uxme_b_open;
	swidget	Uxmt_b_file;
	swidget	Uxme_b_help;
	swidget	Uxme_b_hgissel;
	swidget	Uxmt_b_help;
	swidget	Uxme_b_quit;
	swidget	Uxme_b_bye;
	swidget	Uxmt_b_quit;
} _UxCBruzualModel;

#define BruzualModel            UxBruzualModelContext->UxBruzualModel
#define form_BM                 UxBruzualModelContext->Uxform_BM
#define lbl_copyright           UxBruzualModelContext->Uxlbl_copyright
#define form_BM2                UxBruzualModelContext->Uxform_BM2
#define BM_newised              UxBruzualModelContext->UxBM_newised
#define BM_apply                UxBruzualModelContext->UxBM_apply
#define lbl_sfr_c               UxBruzualModelContext->Uxlbl_sfr_c
#define lbl_ssp                 UxBruzualModelContext->Uxlbl_ssp
#define lbl_sfr                 UxBruzualModelContext->Uxlbl_sfr
#define label1                  UxBruzualModelContext->Uxlabel1
#define lbl_output              UxBruzualModelContext->Uxlbl_output
#define lbl_H0                  UxBruzualModelContext->Uxlbl_H0
#define lbl_age                 UxBruzualModelContext->Uxlbl_age
#define gaz_rec                 UxBruzualModelContext->Uxgaz_rec
#define tg_gaz                  UxBruzualModelContext->Uxtg_gaz
#define tg_nogaz                UxBruzualModelContext->Uxtg_nogaz
#define tf_wvmin                UxBruzualModelContext->Uxtf_wvmin
#define tf_wvmax                UxBruzualModelContext->Uxtf_wvmax
#define tf_age                  UxBruzualModelContext->Uxtf_age
#define tf_omega                UxBruzualModelContext->Uxtf_omega
#define tf_z                    UxBruzualModelContext->Uxtf_z
#define tf_H0                   UxBruzualModelContext->Uxtf_H0
#define helptextBM              UxBruzualModelContext->UxhelptextBM
#define separatorBM             UxBruzualModelContext->UxseparatorBM
#define menu_SFR                UxBruzualModelContext->Uxmenu_SFR
#define mn_exptau               UxBruzualModelContext->Uxmn_exptau
#define mn_expmu                UxBruzualModelContext->Uxmn_expmu
#define mn_single               UxBruzualModelContext->Uxmn_single
#define mn_constant             UxBruzualModelContext->Uxmn_constant
#define mn_SFR                  UxBruzualModelContext->Uxmn_SFR
#define lbl_gaz                 UxBruzualModelContext->Uxlbl_gaz
#define tf_sfr_c                UxBruzualModelContext->Uxtf_sfr_c
#define separatorBM1            UxBruzualModelContext->UxseparatorBM1
#define lbl_Omega1              UxBruzualModelContext->Uxlbl_Omega1
#define tf_output               UxBruzualModelContext->Uxtf_output
#define separatorBM2            UxBruzualModelContext->UxseparatorBM2
#define separatorBM3            UxBruzualModelContext->UxseparatorBM3
#define menu_IMF                UxBruzualModelContext->Uxmenu_IMF
#define mn_sp                   UxBruzualModelContext->Uxmn_sp
#define mn_sc                   UxBruzualModelContext->Uxmn_sc
#define mn_ms                   UxBruzualModelContext->Uxmn_ms
#define mn_IMF                  UxBruzualModelContext->Uxmn_IMF
#define lbl_mass                UxBruzualModelContext->Uxlbl_mass
#define menu_mass               UxBruzualModelContext->Uxmenu_mass
#define mn_m1                   UxBruzualModelContext->Uxmn_m1
#define mn_m2                   UxBruzualModelContext->Uxmn_m2
#define mn_m3                   UxBruzualModelContext->Uxmn_m3
#define mn_m4                   UxBruzualModelContext->Uxmn_m4
#define mn_m5                   UxBruzualModelContext->Uxmn_m5
#define mn_mass                 UxBruzualModelContext->Uxmn_mass
#define lbl_stpop               UxBruzualModelContext->Uxlbl_stpop
#define stellar_pop             UxBruzualModelContext->Uxstellar_pop
#define tg_ssp                  UxBruzualModelContext->Uxtg_ssp
#define tg_csp                  UxBruzualModelContext->Uxtg_csp
#define separatorBM4            UxBruzualModelContext->UxseparatorBM4
#define lbl_massgal             UxBruzualModelContext->Uxlbl_massgal
#define tf_massgal              UxBruzualModelContext->Uxtf_massgal
#define menu_dist               UxBruzualModelContext->Uxmenu_dist
#define mn_z                    UxBruzualModelContext->Uxmn_z
#define mn_Mpc                  UxBruzualModelContext->Uxmn_Mpc
#define mn_dist                 UxBruzualModelContext->Uxmn_dist
#define separatorBM5            UxBruzualModelContext->UxseparatorBM5
#define lbl_ised                UxBruzualModelContext->Uxlbl_ised
#define tf_ised                 UxBruzualModelContext->Uxtf_ised
#define meBruzual               UxBruzualModelContext->UxmeBruzual
#define me_b_file               UxBruzualModelContext->Uxme_b_file
#define me_b_open               UxBruzualModelContext->Uxme_b_open
#define mt_b_file               UxBruzualModelContext->Uxmt_b_file
#define me_b_help               UxBruzualModelContext->Uxme_b_help
#define me_b_hgissel            UxBruzualModelContext->Uxme_b_hgissel
#define mt_b_help               UxBruzualModelContext->Uxmt_b_help
#define me_b_quit               UxBruzualModelContext->Uxme_b_quit
#define me_b_bye                UxBruzualModelContext->Uxme_b_bye
#define mt_b_quit               UxBruzualModelContext->Uxmt_b_quit

static _UxCBruzualModel	*UxBruzualModelContext;

extern void SetFileList(), DisplayShortBMHelp(), DisplayExtendedHelp();
extern void set_ssp(), set_csp(), do_bruzual(), create_sed();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*table_ised = "#override\n\
<Btn3Down>:ChooseListUpISED()\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n";

static char	*BMtext_tab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n";

static char	*table_out = "#override\n\
<Btn3Down>:ChooseListUpOUT()\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n";

static char	*HelpBM = "#override\n\
<Btn3Down>:HelpHelp()\n\
<EnterWindow>:HelpShortBM()\n\
<LeaveWindow>:ClearShortBM()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_BruzualModel();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearShortBM( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextBM"),"");
	 UxPutText(UxFindSwidget("HelpFShell"),"");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	action_HelpShortBM( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	DisplayShortBMHelp(UxWidget);
	UxBruzualModelContext = UxSaveCtx;
}

static void	action_HelpHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxBruzualModelContext = UxSaveCtx;
}

static void	action_ChooseListUpOUT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	
	strcpy(G.sel_type,"flux");
	
	UxPutTitle( UxFindSwidget("AskShell"),"Output Flux Name");
	UxPutLabelString( UxFindSwidget("labelAsk"), "Filename (no extension) :" );
	
	UxPopupInterface(UxFindSwidget("AskShell"),no_grab);
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	action_ChooseListUpISED( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	extern swidget scrolledchoose;
	
	strcpy(G.sel_type,"ised");
	
	SetFileList( UxGetWidget(scrolledchoose),1, "*.ised" );
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_BM_newised( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	create_sed();
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_BM_apply( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	do_bruzual();
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	armCB_tg_gaz( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.grecycle=1;
	set_csp();
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	armCB_tg_nogaz( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.grecycle=0;
	set_csp();
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	valueChangedCB_tg_nogaz( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_wvmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextBM"),"Minimum Wavelength (in nm)");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wvmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float minw;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_wvmin")),"%f",&minw);
	if (nid==1)
		{
		if ((minw>=.5)&&(minw<=2000.))
			B.minwave=minw;
		else if (minw<.5)
			B.minwave=.5;
		else if (minw>2000.)
			B.minwave=2000.;
		}
	
	
	sprintf(am,"%.1f",B.minwave);
	UxPutText(UxFindSwidget("tf_wvmin"),am);
	
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_wvmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextBM"),"Maximum Wavelength (in nm)");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wvmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float maxw;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_wvmax")),"%f",&maxw);
	
	if (nid==1)
		{
		if ((maxw>=.5)&&(maxw<=2000.))
			B.maxwave=maxw;
		else if (maxw<.5)
			B.maxwave=.5;
		else if (maxw>2000.)
			B.maxwave=2000.;
		}
	
	
	
	sprintf(am,"%.1f",B.maxwave);
	UxPutText(UxFindSwidget("tf_wvmax"),am);
	
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_age( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextBM"),"Age of the Galaxy in Gyear");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_age( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float age;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_age")),"%f",&age);
	
	if (nid==1)
		{
		if ((age>=.1)&&(age<=30.))
			B.agegal=age;
		else if (age<.1)
			B.agegal=.1;
		else if (age>30.)
			B.agegal=30.;
		}
	
	sprintf(am,"%.1f",B.agegal);
	UxPutText(UxFindSwidget("tf_age"),am);
	
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_omega( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextBM"),"Cosmological Density Parameter");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_omega( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float om;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_omega")),"%f",&om);
	
	if (nid==1)
		{
		if ((om>=0.)&&(om<=5.))
			B.omega=om;
		else if (om<0.)
			B.omega=0.001;
		else if (om>5.)
			B.omega=5.;
		}
	
	if (B.omega==0.)
		B.omega=0.001;
	
	
	sprintf(am,"%.2f",B.omega);
	UxPutText(UxFindSwidget("tf_omega"),am);
	
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_z( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	if (B.idist==0)
	UxPutText(UxFindSwidget("helptextBM"),"Redshift of the Galaxy");
	else
	UxPutText(UxFindSwidget("helptextBM"),"Distance of the Galaxy in Mpc");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_z( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_z")),"%f",&reel);
	if (nid>0)
		{
		if (B.idist==0)
		{
		if ((reel>=0.)&&(reel<=20.))
			B.redshift=reel;
		else if	(reel<0.)
			B.redshift=0.;
		else if	(reel>20.)
			B.redshift=20.;
		}
		else if (B.idist==1)
		{
		if ((reel>=0.)&&(reel<=500.))
			B.dist=reel;
		else if	(reel<0.)
			B.dist=0.;
		else if	(reel>500.)
			B.dist=500.;
		}
	
		};
	
	if (B.idist==0)
		{
		if (B.redshift>=0.001)
			{
			sprintf(am,"%.4f",B.redshift);
			UxPutText(UxFindSwidget("tf_z"),am);
			}
		else
			{
			B.idist=1;
			B.dist=300000./B.H0*B.redshift;
			UxPutMenuHistory(UxFindSwidget("mn_dist"),"mn_Mpc");
			UxPutMenuHistory(UxFindSwidget("mn_dist"),"mn_z");
			UxPutMenuHistory(UxFindSwidget("mn_dist"),"mn_Mpc");
			sprintf(am,"%.2f",B.dist);
			UxPutText(UxFindSwidget("tf_z"),am);
			}
		}
	else if (B.idist==1)
		{
		if (B.dist<100.)
			{
			sprintf(am,"%.2f",B.dist);
			UxPutText(UxFindSwidget("tf_z"),am);
			}
		else
			{
			B.idist=0;
			UxPutMenuHistory(UxFindSwidget("mn_dist"),"mn_z");
			UxPutMenuHistory(UxFindSwidget("mn_dist"),"mn_Mpc");
			UxPutMenuHistory(UxFindSwidget("mn_dist"),"mn_z");
			B.redshift=B.H0/300000.*B.dist;
			sprintf(am,"%.4f",B.redshift);
			UxPutText(UxFindSwidget("tf_z"),am);
			}
		}
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_H0( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextBM"),"Hubble Constant in km/s/Mpc");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_H0( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float h;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_H0")),"%f",&h);
	
	if (nid==1)
		{
		if ((h>=5.)&&(h<=200.))
			B.H0=h;
		else if (h<5.)
			B.H0=5.;
		else if (h>200.)
			B.H0=200.;
		}
	
	
	sprintf(am,"%.1f",B.H0);
	UxPutText(UxFindSwidget("tf_H0"),am);
	
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_exptau( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	char am[10];
	
	B.isfr=1;
	sprintf(am,"%.3f",B.etau);
	UxPutText(UxFindSwidget("tf_sfr_c"),am);
	set_csp();
	
	UxPutSensitive(UxFindSwidget("gaz_rec"),"true");
	UxPutSensitive(UxFindSwidget("tg_gaz"),"true");
	UxPutSensitive(UxFindSwidget("tg_nogaz"),"true");
	UxPutSensitive(UxFindSwidget("lbl_gaz"),"true");
	UxPutLabelString(UxFindSwidget("lbl_sfr_c"),"Exponential time (Gyr) :");
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_expmu( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	char am[10];
	
	B.isfr=-1;
	sprintf(am,"%.3f",B.emu);
	UxPutText(UxFindSwidget("tf_sfr_c"),am);
	set_csp();
	
	
	UxPutSensitive(UxFindSwidget("gaz_rec"),"true");
	UxPutSensitive(UxFindSwidget("tg_gaz"),"true");
	UxPutSensitive(UxFindSwidget("tg_nogaz"),"true");
	UxPutSensitive(UxFindSwidget("lbl_gaz"),"true");
	UxPutLabelString(UxFindSwidget("lbl_sfr_c"),"Bruzual Mu Parameter :");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_single( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	char am[10];
	
	B.isfr=2;
	sprintf(am,"%.3f",B.dburst);
	UxPutText(UxFindSwidget("tf_sfr_c"),am);
	set_csp();
	
	UxPutSensitive(UxFindSwidget("gaz_rec"),"false");
	UxPutSensitive(UxFindSwidget("tg_gaz"),"false");
	UxPutSensitive(UxFindSwidget("tg_nogaz"),"false");
	UxPutSensitive(UxFindSwidget("lbl_gaz"),"false");
	UxPutLabelString(UxFindSwidget("lbl_sfr_c"),"Duration of burst (Gyr) :");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_constant( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	char am[10];
	
	B.isfr=3;
	sprintf(am,"%.3f",B.crate);
	UxPutText(UxFindSwidget("tf_sfr_c"),am);
	set_csp();
	
	UxPutSensitive(UxFindSwidget("gaz_rec"),"false");
	UxPutSensitive(UxFindSwidget("tg_gaz"),"false");
	UxPutSensitive(UxFindSwidget("tg_nogaz"),"false");
	UxPutSensitive(UxFindSwidget("lbl_gaz"),"false");
	UxPutLabelString(UxFindSwidget("lbl_sfr_c"),"SFR in Mo/yr :");
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_sfr_c( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	if (B.isfr==1)
	UxPutText(UxFindSwidget("helptextBM"),"Exponential-folding time TAU (Gyr)");
	else if (B.isfr==-1)
	UxPutText(UxFindSwidget("helptextBM"),"Bruzual mu parameter");
	else if (B.isfr==2)
	UxPutText(UxFindSwidget("helptextBM"),"Duration of the Single Birst (Gyr)"); 
	else if (B.isfr==3)
	UxPutText(UxFindSwidget("helptextBM"),"Star Formation Rate in Solar Mass per year");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_sfr_c( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float reel=1.;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_sfr_c")),"%f",&reel);
	if (nid==1)
		{
		if ((B.isfr==1)&&(reel>0.)&&(reel<20.))
			B.etau=reel;
		else if ((B.isfr==-1)&&(reel>0.)&&(reel<200.))
			B.emu=reel;
		else if ((B.isfr==2)&&(reel>0.)&&(reel<20.))
			B.dburst=reel;
		else if ((B.isfr==3)&&(reel>0.)&&(reel<10000.))
			B.crate=reel;
		};
	
	if (B.isfr==1)
		reel=B.etau;
	else if (B.isfr==-1)
		reel=B.emu;
	else if (B.isfr==2)
		reel=B.dburst;
	else if (B.isfr==3)
		reel=B.crate;
	
	
	sprintf(am,"%.2f",reel);
	UxPutText(UxFindSwidget("tf_sfr_c"),am);
	set_csp();
	
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	motionVerifyCB_tf_output( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	MOD_outfile = 1;
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_output( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	
	UxPutText(UxFindSwidget("helptextBM"),"Output File Basename (no extension)");
	
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_output( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char	fileout[30];
	
	
	if (MOD_outfile==1)
	{
	nid=sscanf(UxGetText(tf_output),"%s",fileout);
	
	if (nid<1)
	        strcpy(B.outfile,"default_bm");
	
	else
		if (strcmp(fileout,B.outfile))
	                strcpy(B.outfile,fileout);
	
	
	}
	
	MOD_outfile=0;
	
	UxPutText(UxFindSwidget("tf_output"),B.outfile);
	
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_sp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.imf=1;
	set_ssp();
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_sc( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.imf=2;
	set_ssp();
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_ms( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.imf=3;
	set_ssp();
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_m1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.imfmass=1;
	set_ssp();
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_m2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.imfmass=2;
	set_ssp();
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_m3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.imfmass=3;
	set_ssp();
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_m4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.imfmass=4;
	set_ssp();
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_m5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.imfmass=5;
	set_ssp();
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	armCB_tg_ssp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.istpop=0;
	UxPutSensitive(UxFindSwidget("lbl_sfr"),"false");
	UxPutSensitive(UxFindSwidget("lbl_sfr_c"),"false");
	UxPutSensitive(UxFindSwidget("lbl_gaz"),"false");
	UxPutSensitive(UxFindSwidget("mn_SFR"),"false");
	UxPutSensitive(UxFindSwidget("tf_sfr_c"),"false");
	UxPutSensitive(UxFindSwidget("gaz_rec"),"false");
	UxPutSensitive(UxFindSwidget("tg_gaz"),"false");
	UxPutSensitive(UxFindSwidget("tg_nogaz"),"false");
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	armCB_tg_csp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	B.istpop=1;
	UxPutSensitive(UxFindSwidget("lbl_sfr"),"true");
	UxPutSensitive(UxFindSwidget("lbl_sfr_c"),"true");
	UxPutSensitive(UxFindSwidget("lbl_gaz"),"true");
	UxPutSensitive(UxFindSwidget("mn_SFR"),"true");
	UxPutSensitive(UxFindSwidget("tf_sfr_c"),"true");
	UxPutSensitive(UxFindSwidget("gaz_rec"),"true");
	UxPutSensitive(UxFindSwidget("tg_gaz"),"true");
	UxPutSensitive(UxFindSwidget("tg_nogaz"),"true");
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	valueChangedCB_tg_csp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_massgal( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	UxPutText(UxFindSwidget("helptextBM"),"Age of the Galaxy in Gyear");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_massgal( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float mass;
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_massgal")),"%f",&mass);
	
	if (nid==1)
		{
		if ((mass>= .00001)&&(mass<= 100000.))
			B.massgal=mass;
		else if (mass<.00001)
			B.massgal=.00001;
		else if (mass>100000.)
			B.massgal=100000.;
		}
	
	
	sprintf(am,"%.3f",B.massgal);
	UxPutText(UxFindSwidget("tf_massgal"),am);
	
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_z( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	char am[10];
	
	B.idist=0;
	sprintf(am,"%.4f",B.redshift);
	UxPutText(tf_z,am);
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_mn_Mpc( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	char am[10];
	
	B.idist=1;
	sprintf(am,"%.2f",B.dist);
	UxPutText(tf_z,am);
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	focusCB_tf_ised( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	
	UxPutText(UxFindSwidget("helptextBM"),"ISED File\n you can enter it manually or select it");
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	losingFocusCB_tf_ised( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	int nid;
	char	fileised[80];
	
	
	nid=sscanf(UxGetText(UxFindSwidget("tf_ised")),"%s",fileised);
	
	if (nid>0)
		if (strcmp(fileised,B.sedfile))
	                strcpy(B.sedfile,fileised);
	
	UxPutText(UxFindSwidget("tf_ised"),B.sedfile);
	
	UxPutText(UxFindSwidget("helptextBM"),"");
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_me_b_open( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	extern swidget scrolledchoose;
	
	strcpy(G.sel_type,"ised");
	
	SetFileList( UxGetWidget(scrolledchoose),1, "*.ised" );
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_me_b_hgissel( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxBruzualModelContext = UxSaveCtx;
}

static void	activateCB_me_b_bye( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBruzualModel        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBruzualModelContext;
	UxBruzualModelContext = UxContext =
			(_UxCBruzualModel *) UxGetContext( UxThisWidget );
	UxPopdownInterface(UxFindSwidget("BruzualModel"));
	UxBruzualModelContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_BruzualModel()
{
	UxPutKeyboardFocusPolicy( BruzualModel, "pointer" );
	UxPutIconName( BruzualModel, "Bruzual_and_Charlot_1993_Model" );
	UxPutDefaultFontList( BruzualModel, TextFont );
	UxPutBorderColor( BruzualModel, WindowBackground );
	UxPutBackground( BruzualModel, WindowBackground );
	UxPutHeight( BruzualModel, 655 );
	UxPutWidth( BruzualModel, 490 );
	UxPutY( BruzualModel, 200 );
	UxPutX( BruzualModel, 20 );

	UxPutTextFontList( form_BM, TextFont );
	UxPutLabelFontList( form_BM, TextFont );
	UxPutForeground( form_BM, TextForeground );
	UxPutButtonFontList( form_BM, TextFont );
	UxPutBorderColor( form_BM, WindowBackground );
	UxPutBackground( form_BM, WindowBackground );
	UxPutHeight( form_BM, 125 );
	UxPutWidth( form_BM, 138 );
	UxPutY( form_BM, -1 );
	UxPutX( form_BM, -3 );
	UxPutUnitType( form_BM, "pixels" );
	UxPutResizePolicy( form_BM, "resize_none" );

	UxPutLabelString( lbl_copyright, "Galaxy Isochrone Synthesis Spectral Evolution Library" );
	UxPutForeground( lbl_copyright, TextForeground );
	UxPutFontList( lbl_copyright, BoldTextFont );
	UxPutBorderColor( lbl_copyright, WindowBackground );
	UxPutBackground( lbl_copyright, WindowBackground );
	UxPutHeight( lbl_copyright, 25 );
	UxPutWidth( lbl_copyright, 408 );
	UxPutY( lbl_copyright, 40 );
	UxPutX( lbl_copyright, 0 );

	UxPutForeground( form_BM2, TextForeground );
	UxPutButtonFontList( form_BM2, BoldTextFont );
	UxPutBorderColor( form_BM2, ButtonBackground );
	UxPutBackground( form_BM2, ButtonBackground );
	UxPutHeight( form_BM2, 43 );
	UxPutWidth( form_BM2, 385 );
	UxPutY( form_BM2, 329 );
	UxPutX( form_BM2, 2 );
	UxPutResizePolicy( form_BM2, "resize_none" );

	UxPutTranslations( BM_newised, HelpBM );
	UxPutLabelString( BM_newised, "New ISED" );
	UxPutBackground( BM_newised, ButtonBackground );
	UxPutForeground( BM_newised, ApplyForeground );
	UxPutFontList( BM_newised, BoldTextFont );
	UxPutHeight( BM_newised, 30 );
	UxPutWidth( BM_newised, 90 );
	UxPutY( BM_newised, 7 );
	UxPutX( BM_newised, 10 );

	UxPutTranslations( BM_apply, HelpBM );
	UxPutLabelString( BM_apply, "Apply" );
	UxPutBackground( BM_apply, ButtonBackground );
	UxPutForeground( BM_apply, ApplyForeground );
	UxPutFontList( BM_apply, BoldTextFont );
	UxPutHeight( BM_apply, 30 );
	UxPutWidth( BM_apply, 90 );
	UxPutY( BM_apply, 7 );
	UxPutX( BM_apply, 135 );

	UxPutAlignment( lbl_sfr_c, "alignment_beginning" );
	UxPutLabelString( lbl_sfr_c, "Exponential time (Gyr) :" );
	UxPutForeground( lbl_sfr_c, TextForeground );
	UxPutFontList( lbl_sfr_c, TextFont );
	UxPutBorderColor( lbl_sfr_c, WindowBackground );
	UxPutBackground( lbl_sfr_c, WindowBackground );
	UxPutHeight( lbl_sfr_c, 23 );
	UxPutWidth( lbl_sfr_c, 150 );
	UxPutY( lbl_sfr_c, 271 );
	UxPutX( lbl_sfr_c, 10 );

	UxPutAlignment( lbl_ssp, "alignment_beginning" );
	UxPutLabelString( lbl_ssp, "Initial Mass Function :" );
	UxPutForeground( lbl_ssp, TextForeground );
	UxPutFontList( lbl_ssp, TextFont );
	UxPutBorderColor( lbl_ssp, WindowBackground );
	UxPutBackground( lbl_ssp, WindowBackground );
	UxPutHeight( lbl_ssp, 23 );
	UxPutWidth( lbl_ssp, 155 );
	UxPutY( lbl_ssp, 125 );
	UxPutX( lbl_ssp, 10 );

	UxPutAlignment( lbl_sfr, "alignment_beginning" );
	UxPutLabelString( lbl_sfr, "Star Formation Rate :" );
	UxPutForeground( lbl_sfr, TextForeground );
	UxPutFontList( lbl_sfr, TextFont );
	UxPutBorderColor( lbl_sfr, WindowBackground );
	UxPutBackground( lbl_sfr, WindowBackground );
	UxPutHeight( lbl_sfr, 23 );
	UxPutWidth( lbl_sfr, 145 );
	UxPutY( lbl_sfr, 234 );
	UxPutX( lbl_sfr, 10 );

	UxPutAlignment( label1, "alignment_beginning" );
	UxPutLabelString( label1, "Wavelength Range (nm) :" );
	UxPutForeground( label1, TextForeground );
	UxPutFontList( label1, TextFont );
	UxPutBorderColor( label1, WindowBackground );
	UxPutBackground( label1, WindowBackground );
	UxPutHeight( label1, 23 );
	UxPutWidth( label1, 170 );
	UxPutY( label1, 351 );
	UxPutX( label1, 10 );

	UxPutAlignment( lbl_output, "alignment_beginning" );
	UxPutLabelString( lbl_output, "Output Flux File :" );
	UxPutForeground( lbl_output, TextForeground );
	UxPutFontList( lbl_output, TextFont );
	UxPutBorderColor( lbl_output, WindowBackground );
	UxPutBackground( lbl_output, WindowBackground );
	UxPutHeight( lbl_output, 23 );
	UxPutWidth( lbl_output, 130 );
	UxPutY( lbl_output, 516 );
	UxPutX( lbl_output, 10 );

	UxPutAlignment( lbl_H0, "alignment_beginning" );
	UxPutLabelString( lbl_H0, "H0 (km/s/Mpc) :" );
	UxPutForeground( lbl_H0, TextForeground );
	UxPutFontList( lbl_H0, TextFont );
	UxPutBorderColor( lbl_H0, WindowBackground );
	UxPutBackground( lbl_H0, WindowBackground );
	UxPutHeight( lbl_H0, 23 );
	UxPutWidth( lbl_H0, 110 );
	UxPutY( lbl_H0, 471 );
	UxPutX( lbl_H0, 10 );

	UxPutAlignment( lbl_age, "alignment_beginning" );
	UxPutLabelString( lbl_age, "Galaxy Age (Gyr) :" );
	UxPutForeground( lbl_age, TextForeground );
	UxPutFontList( lbl_age, TextFont );
	UxPutBorderColor( lbl_age, WindowBackground );
	UxPutBackground( lbl_age, WindowBackground );
	UxPutHeight( lbl_age, 23 );
	UxPutWidth( lbl_age, 130 );
	UxPutY( lbl_age, 431 );
	UxPutX( lbl_age, 10 );

	UxPutOrientation( gaz_rec, "horizontal" );
	UxPutIsAligned( gaz_rec, "true" );
	UxPutEntryAlignment( gaz_rec, "alignment_beginning" );
	UxPutBorderWidth( gaz_rec, 0 );
	UxPutShadowThickness( gaz_rec, 0 );
	UxPutLabelString( gaz_rec, "" );
	UxPutEntryBorder( gaz_rec, 0 );
	UxPutBorderColor( gaz_rec, WindowBackground );
	UxPutBackground( gaz_rec, WindowBackground );
	UxPutRadioBehavior( gaz_rec, "true" );
	UxPutWidth( gaz_rec, 208 );
	UxPutY( gaz_rec, 300 );
	UxPutX( gaz_rec, 180 );

	UxPutTranslations( tg_gaz, HelpBM );
	UxPutHighlightOnEnter( tg_gaz, "true" );
	UxPutForeground( tg_gaz, TextForeground );
	UxPutSelectColor( tg_gaz, SelectColor );
	UxPutSet( tg_gaz, "true" );
	UxPutLabelString( tg_gaz, "Recycle" );
	UxPutFontList( tg_gaz, TextFont );
	UxPutBorderColor( tg_gaz, WindowBackground );
	UxPutBackground( tg_gaz, WindowBackground );
	UxPutHeight( tg_gaz, 28 );
	UxPutWidth( tg_gaz, 19 );
	UxPutY( tg_gaz, 5 );
	UxPutX( tg_gaz, 5 );

	UxPutTranslations( tg_nogaz, HelpBM );
	UxPutHighlightOnEnter( tg_nogaz, "true" );
	UxPutSet( tg_nogaz, "false" );
	UxPutForeground( tg_nogaz, TextForeground );
	UxPutSelectColor( tg_nogaz, SelectColor );
	UxPutLabelString( tg_nogaz, "no Recycle" );
	UxPutFontList( tg_nogaz, TextFont );
	UxPutBorderColor( tg_nogaz, WindowBackground );
	UxPutBackground( tg_nogaz, WindowBackground );
	UxPutHeight( tg_nogaz, 28 );
	UxPutWidth( tg_nogaz, 78 );
	UxPutY( tg_nogaz, 117 );
	UxPutX( tg_nogaz, 115 );

	UxPutTranslations( tf_wvmin, BMtext_tab );
	UxPutText( tf_wvmin, "300." );
	UxPutForeground( tf_wvmin, TextForeground );
	UxPutHighlightOnEnter( tf_wvmin, "true" );
	UxPutFontList( tf_wvmin, TextFont );
	UxPutBorderColor( tf_wvmin, TextBackground );
	UxPutBackground( tf_wvmin, TextBackground );
	UxPutHeight( tf_wvmin, 35 );
	UxPutWidth( tf_wvmin, 65 );
	UxPutY( tf_wvmin, 344 );
	UxPutX( tf_wvmin, 180 );

	UxPutTranslations( tf_wvmax, BMtext_tab );
	UxPutText( tf_wvmax, "900." );
	UxPutForeground( tf_wvmax, TextForeground );
	UxPutHighlightOnEnter( tf_wvmax, "true" );
	UxPutFontList( tf_wvmax, TextFont );
	UxPutBorderColor( tf_wvmax, TextBackground );
	UxPutBackground( tf_wvmax, TextBackground );
	UxPutHeight( tf_wvmax, 35 );
	UxPutWidth( tf_wvmax, 65 );
	UxPutY( tf_wvmax, 345 );
	UxPutX( tf_wvmax, 253 );

	UxPutTranslations( tf_age, BMtext_tab );
	UxPutText( tf_age, "15." );
	UxPutForeground( tf_age, TextForeground );
	UxPutHighlightOnEnter( tf_age, "true" );
	UxPutFontList( tf_age, TextFont );
	UxPutBorderColor( tf_age, TextBackground );
	UxPutBackground( tf_age, TextBackground );
	UxPutHeight( tf_age, 35 );
	UxPutWidth( tf_age, 65 );
	UxPutY( tf_age, 425 );
	UxPutX( tf_age, 180 );

	UxPutTranslations( tf_omega, BMtext_tab );
	UxPutText( tf_omega, "0." );
	UxPutForeground( tf_omega, TextForeground );
	UxPutHighlightOnEnter( tf_omega, "true" );
	UxPutFontList( tf_omega, TextFont );
	UxPutBorderColor( tf_omega, TextBackground );
	UxPutBackground( tf_omega, TextBackground );
	UxPutHeight( tf_omega, 35 );
	UxPutWidth( tf_omega, 65 );
	UxPutY( tf_omega, 465 );
	UxPutX( tf_omega, 415 );

	UxPutTranslations( tf_z, BMtext_tab );
	UxPutText( tf_z, "0." );
	UxPutForeground( tf_z, TextForeground );
	UxPutHighlightOnEnter( tf_z, "true" );
	UxPutFontList( tf_z, TextFont );
	UxPutBorderColor( tf_z, TextBackground );
	UxPutBackground( tf_z, TextBackground );
	UxPutHeight( tf_z, 35 );
	UxPutWidth( tf_z, 65 );
	UxPutY( tf_z, 425 );
	UxPutX( tf_z, 415 );

	UxPutTranslations( tf_H0, BMtext_tab );
	UxPutText( tf_H0, "50." );
	UxPutForeground( tf_H0, TextForeground );
	UxPutHighlightOnEnter( tf_H0, "true" );
	UxPutFontList( tf_H0, TextFont );
	UxPutBorderColor( tf_H0, TextBackground );
	UxPutBackground( tf_H0, TextBackground );
	UxPutHeight( tf_H0, 35 );
	UxPutWidth( tf_H0, 65 );
	UxPutY( tf_H0, 465 );
	UxPutX( tf_H0, 180 );

	UxPutFontList( helptextBM, TextFont );
	UxPutEditable( helptextBM, "false" );
	UxPutCursorPositionVisible( helptextBM, "false" );
	UxPutBorderColor( helptextBM, SHelpBackground );
	UxPutBackground( helptextBM, SHelpBackground );
	UxPutHeight( helptextBM, 50 );
	UxPutWidth( helptextBM, 490 );
	UxPutY( helptextBM, 540 );
	UxPutX( helptextBM, 0 );

	UxPutBorderColor( separatorBM, WindowBackground );
	UxPutBackground( separatorBM, WindowBackground );
	UxPutHeight( separatorBM, 5 );
	UxPutWidth( separatorBM, 492 );
	UxPutY( separatorBM, 373 );
	UxPutX( separatorBM, -4 );

	UxPutForeground( menu_SFR, TextForeground );
	UxPutY( menu_SFR, -20 );
	UxPutX( menu_SFR, 0 );
	UxPutRowColumnType( menu_SFR, "menu_pulldown" );

	UxPutY( mn_exptau, 102 );
	UxPutX( mn_exptau, 2 );
	UxPutForeground( mn_exptau, TextForeground );
	UxPutFontList( mn_exptau, TextFont );
	UxPutBorderColor( mn_exptau, WindowBackground );
	UxPutBackground( mn_exptau, WindowBackground );
	UxPutLabelString( mn_exptau, " Exponential (Tau) " );

	UxPutY( mn_expmu, 102 );
	UxPutX( mn_expmu, 2 );
	UxPutBorderColor( mn_expmu, WindowBackground );
	UxPutBackground( mn_expmu, WindowBackground );
	UxPutForeground( mn_expmu, TextForeground );
	UxPutFontList( mn_expmu, TextFont );
	UxPutLabelString( mn_expmu, " Exponential (Mu) " );

	UxPutY( mn_single, 102 );
	UxPutX( mn_single, 2 );
	UxPutForeground( mn_single, TextForeground );
	UxPutFontList( mn_single, TextFont );
	UxPutBorderColor( mn_single, WindowBackground );
	UxPutBackground( mn_single, WindowBackground );
	UxPutLabelString( mn_single, " Single Burst " );

	UxPutY( mn_constant, 102 );
	UxPutX( mn_constant, 2 );
	UxPutBorderColor( mn_constant, WindowBackground );
	UxPutBackground( mn_constant, WindowBackground );
	UxPutForeground( mn_constant, TextForeground );
	UxPutFontList( mn_constant, TextFont );
	UxPutLabelString( mn_constant, " Constant Rate " );

	UxPutLabelString( mn_SFR, "" );
	UxPutTranslations( mn_SFR, HelpBM );
	UxPutHeight( mn_SFR, 35 );
	UxPutForeground( mn_SFR, TextForeground );
	UxPutBorderColor( mn_SFR, WindowBackground );
	UxPutBackground( mn_SFR, WindowBackground );
	UxPutY( mn_SFR, 230 );
	UxPutX( mn_SFR, 180 );
	UxPutRowColumnType( mn_SFR, "menu_option" );

	UxPutAlignment( lbl_gaz, "alignment_beginning" );
	UxPutLabelString( lbl_gaz, "Gaz Ejected by Stars :" );
	UxPutForeground( lbl_gaz, TextForeground );
	UxPutFontList( lbl_gaz, TextFont );
	UxPutBorderColor( lbl_gaz, WindowBackground );
	UxPutBackground( lbl_gaz, WindowBackground );
	UxPutHeight( lbl_gaz, 23 );
	UxPutWidth( lbl_gaz, 150 );
	UxPutY( lbl_gaz, 306 );
	UxPutX( lbl_gaz, 10 );

	UxPutTranslations( tf_sfr_c, HelpBM );
	UxPutText( tf_sfr_c, "1." );
	UxPutForeground( tf_sfr_c, TextForeground );
	UxPutHighlightOnEnter( tf_sfr_c, "true" );
	UxPutFontList( tf_sfr_c, TextFont );
	UxPutBorderColor( tf_sfr_c, TextBackground );
	UxPutBackground( tf_sfr_c, TextBackground );
	UxPutHeight( tf_sfr_c, 35 );
	UxPutWidth( tf_sfr_c, 65 );
	UxPutY( tf_sfr_c, 265 );
	UxPutX( tf_sfr_c, 180 );

	UxPutBorderColor( separatorBM1, WindowBackground );
	UxPutBackground( separatorBM1, WindowBackground );
	UxPutHeight( separatorBM1, 5 );
	UxPutWidth( separatorBM1, 490 );
	UxPutY( separatorBM1, 225 );
	UxPutX( separatorBM1, 0 );

	UxPutAlignment( lbl_Omega1, "alignment_beginning" );
	UxPutLabelString( lbl_Omega1, "Omega :" );
	UxPutForeground( lbl_Omega1, TextForeground );
	UxPutFontList( lbl_Omega1, TextFont );
	UxPutBorderColor( lbl_Omega1, WindowBackground );
	UxPutBackground( lbl_Omega1, WindowBackground );
	UxPutHeight( lbl_Omega1, 23 );
	UxPutWidth( lbl_Omega1, 94 );
	UxPutY( lbl_Omega1, 470 );
	UxPutX( lbl_Omega1, 275 );

	UxPutTranslations( tf_output, table_out );
	UxPutMaxLength( tf_output, 50 );
	UxPutForeground( tf_output, TextForeground );
	UxPutHighlightOnEnter( tf_output, "true" );
	UxPutFontList( tf_output, TextFont );
	UxPutBorderColor( tf_output, TextBackground );
	UxPutBackground( tf_output, TextBackground );
	UxPutHeight( tf_output, 35 );
	UxPutWidth( tf_output, 209 );
	UxPutY( tf_output, 510 );
	UxPutX( tf_output, 180 );

	UxPutBorderColor( separatorBM2, WindowBackground );
	UxPutBackground( separatorBM2, WindowBackground );
	UxPutHeight( separatorBM2, 5 );
	UxPutWidth( separatorBM2, 492 );
	UxPutY( separatorBM2, 505 );
	UxPutX( separatorBM2, 0 );

	UxPutBorderColor( separatorBM3, WindowBackground );
	UxPutBackground( separatorBM3, WindowBackground );
	UxPutHeight( separatorBM3, 5 );
	UxPutWidth( separatorBM3, 492 );
	UxPutY( separatorBM3, 335 );
	UxPutX( separatorBM3, 0 );

	UxPutForeground( menu_IMF, TextForeground );
	UxPutY( menu_IMF, -20 );
	UxPutX( menu_IMF, 0 );
	UxPutRowColumnType( menu_IMF, "menu_pulldown" );

	UxPutY( mn_sp, 102 );
	UxPutX( mn_sp, 2 );
	UxPutForeground( mn_sp, TextForeground );
	UxPutFontList( mn_sp, TextFont );
	UxPutBorderColor( mn_sp, WindowBackground );
	UxPutBackground( mn_sp, WindowBackground );
	UxPutLabelString( mn_sp, " Salpeter (1955) " );

	UxPutY( mn_sc, 102 );
	UxPutX( mn_sc, 2 );
	UxPutBorderColor( mn_sc, WindowBackground );
	UxPutBackground( mn_sc, WindowBackground );
	UxPutForeground( mn_sc, TextForeground );
	UxPutFontList( mn_sc, TextFont );
	UxPutLabelString( mn_sc, " Scalo (1986) " );

	UxPutY( mn_ms, 102 );
	UxPutX( mn_ms, 2 );
	UxPutForeground( mn_ms, TextForeground );
	UxPutFontList( mn_ms, TextFont );
	UxPutBorderColor( mn_ms, WindowBackground );
	UxPutBackground( mn_ms, WindowBackground );
	UxPutLabelString( mn_ms, " Miller & Scalo (1979) " );

	UxPutLabelString( mn_IMF, "" );
	UxPutTranslations( mn_IMF, HelpBM );
	UxPutHeight( mn_IMF, 35 );
	UxPutForeground( mn_IMF, TextForeground );
	UxPutBorderColor( mn_IMF, WindowBackground );
	UxPutBackground( mn_IMF, WindowBackground );
	UxPutY( mn_IMF, 120 );
	UxPutX( mn_IMF, 175 );
	UxPutRowColumnType( mn_IMF, "menu_option" );

	UxPutAlignment( lbl_mass, "alignment_beginning" );
	UxPutLabelString( lbl_mass, "IMF Mass Interval :" );
	UxPutForeground( lbl_mass, TextForeground );
	UxPutFontList( lbl_mass, TextFont );
	UxPutBorderColor( lbl_mass, WindowBackground );
	UxPutBackground( lbl_mass, WindowBackground );
	UxPutHeight( lbl_mass, 23 );
	UxPutWidth( lbl_mass, 134 );
	UxPutY( lbl_mass, 160 );
	UxPutX( lbl_mass, 10 );

	UxPutForeground( menu_mass, TextForeground );
	UxPutY( menu_mass, -20 );
	UxPutX( menu_mass, 0 );
	UxPutRowColumnType( menu_mass, "menu_pulldown" );

	UxPutY( mn_m1, 102 );
	UxPutX( mn_m1, 2 );
	UxPutForeground( mn_m1, TextForeground );
	UxPutFontList( mn_m1, TextFont );
	UxPutBorderColor( mn_m1, WindowBackground );
	UxPutBackground( mn_m1, WindowBackground );
	UxPutLabelString( mn_m1, " from 0.1 to 125 Solar Masses " );

	UxPutY( mn_m2, 102 );
	UxPutX( mn_m2, 2 );
	UxPutBorderColor( mn_m2, WindowBackground );
	UxPutBackground( mn_m2, WindowBackground );
	UxPutForeground( mn_m2, TextForeground );
	UxPutFontList( mn_m2, TextFont );
	UxPutLabelString( mn_m2, " from 0.1 to 65 Solar Masses " );

	UxPutY( mn_m3, 102 );
	UxPutX( mn_m3, 2 );
	UxPutForeground( mn_m3, TextForeground );
	UxPutFontList( mn_m3, TextFont );
	UxPutBorderColor( mn_m3, WindowBackground );
	UxPutBackground( mn_m3, WindowBackground );
	UxPutLabelString( mn_m3, " from 0.1 to 30 Solar Masses " );

	UxPutFontList( mn_m4, TextFont );
	UxPutBorderColor( mn_m4, WindowBackground );
	UxPutBackground( mn_m4, WindowBackground );
	UxPutLabelString( mn_m4, " from 0.1 to 2.5 Solar Masses " );

	UxPutFontList( mn_m5, TextFont );
	UxPutBorderColor( mn_m5, WindowBackground );
	UxPutBackground( mn_m5, WindowBackground );
	UxPutLabelString( mn_m5, " from 2.5 to 125 Solar Masses " );

	UxPutLabelString( mn_mass, "" );
	UxPutTranslations( mn_mass, HelpBM );
	UxPutHeight( mn_mass, 35 );
	UxPutForeground( mn_mass, TextForeground );
	UxPutBorderColor( mn_mass, WindowBackground );
	UxPutBackground( mn_mass, WindowBackground );
	UxPutY( mn_mass, 155 );
	UxPutX( mn_mass, 175 );
	UxPutRowColumnType( mn_mass, "menu_option" );

	UxPutAlignment( lbl_stpop, "alignment_beginning" );
	UxPutLabelString( lbl_stpop, "Stellar Population :" );
	UxPutForeground( lbl_stpop, TextForeground );
	UxPutFontList( lbl_stpop, TextFont );
	UxPutBorderColor( lbl_stpop, WindowBackground );
	UxPutBackground( lbl_stpop, WindowBackground );
	UxPutHeight( lbl_stpop, 23 );
	UxPutWidth( lbl_stpop, 134 );
	UxPutY( lbl_stpop, 195 );
	UxPutX( lbl_stpop, 10 );

	UxPutOrientation( stellar_pop, "horizontal" );
	UxPutIsAligned( stellar_pop, "true" );
	UxPutEntryAlignment( stellar_pop, "alignment_beginning" );
	UxPutBorderWidth( stellar_pop, 0 );
	UxPutShadowThickness( stellar_pop, 0 );
	UxPutLabelString( stellar_pop, "" );
	UxPutEntryBorder( stellar_pop, 0 );
	UxPutBorderColor( stellar_pop, WindowBackground );
	UxPutBackground( stellar_pop, WindowBackground );
	UxPutRadioBehavior( stellar_pop, "true" );
	UxPutWidth( stellar_pop, 208 );
	UxPutY( stellar_pop, 190 );
	UxPutX( stellar_pop, 175 );

	UxPutTranslations( tg_ssp, HelpBM );
	UxPutHighlightOnEnter( tg_ssp, "true" );
	UxPutForeground( tg_ssp, TextForeground );
	UxPutSelectColor( tg_ssp, SelectColor );
	UxPutSet( tg_ssp, "true" );
	UxPutLabelString( tg_ssp, "Instantaneous" );
	UxPutFontList( tg_ssp, TextFont );
	UxPutBorderColor( tg_ssp, WindowBackground );
	UxPutBackground( tg_ssp, WindowBackground );
	UxPutHeight( tg_ssp, 28 );
	UxPutWidth( tg_ssp, 19 );
	UxPutY( tg_ssp, 3 );
	UxPutX( tg_ssp, 83 );

	UxPutTranslations( tg_csp, HelpBM );
	UxPutHighlightOnEnter( tg_csp, "true" );
	UxPutSet( tg_csp, "false" );
	UxPutForeground( tg_csp, TextForeground );
	UxPutSelectColor( tg_csp, SelectColor );
	UxPutLabelString( tg_csp, "Composite" );
	UxPutFontList( tg_csp, TextFont );
	UxPutBorderColor( tg_csp, WindowBackground );
	UxPutBackground( tg_csp, WindowBackground );
	UxPutHeight( tg_csp, 28 );
	UxPutWidth( tg_csp, 128 );
	UxPutY( tg_csp, 3 );
	UxPutX( tg_csp, 120 );

	UxPutBorderColor( separatorBM4, WindowBackground );
	UxPutBackground( separatorBM4, WindowBackground );
	UxPutHeight( separatorBM4, 5 );
	UxPutWidth( separatorBM4, 492 );
	UxPutY( separatorBM4, 71 );
	UxPutX( separatorBM4, 0 );

	UxPutAlignment( lbl_massgal, "alignment_beginning" );
	UxPutLabelString( lbl_massgal, "Galaxy Mass (1e10 Mo):" );
	UxPutForeground( lbl_massgal, TextForeground );
	UxPutFontList( lbl_massgal, TextFont );
	UxPutBorderColor( lbl_massgal, WindowBackground );
	UxPutBackground( lbl_massgal, WindowBackground );
	UxPutHeight( lbl_massgal, 23 );
	UxPutWidth( lbl_massgal, 166 );
	UxPutY( lbl_massgal, 390 );
	UxPutX( lbl_massgal, 10 );

	UxPutTranslations( tf_massgal, BMtext_tab );
	UxPutText( tf_massgal, "15." );
	UxPutForeground( tf_massgal, TextForeground );
	UxPutHighlightOnEnter( tf_massgal, "true" );
	UxPutFontList( tf_massgal, TextFont );
	UxPutBorderColor( tf_massgal, TextBackground );
	UxPutBackground( tf_massgal, TextBackground );
	UxPutHeight( tf_massgal, 35 );
	UxPutWidth( tf_massgal, 65 );
	UxPutY( tf_massgal, 385 );
	UxPutX( tf_massgal, 180 );

	UxPutForeground( menu_dist, TextForeground );
	UxPutY( menu_dist, -20 );
	UxPutX( menu_dist, 0 );
	UxPutRowColumnType( menu_dist, "menu_pulldown" );

	UxPutY( mn_z, 102 );
	UxPutX( mn_z, 2 );
	UxPutForeground( mn_z, TextForeground );
	UxPutFontList( mn_z, TextFont );
	UxPutBorderColor( mn_z, WindowBackground );
	UxPutBackground( mn_z, WindowBackground );
	UxPutLabelString( mn_z, "Redshift (z)" );

	UxPutY( mn_Mpc, 102 );
	UxPutX( mn_Mpc, 2 );
	UxPutBorderColor( mn_Mpc, WindowBackground );
	UxPutBackground( mn_Mpc, WindowBackground );
	UxPutForeground( mn_Mpc, TextForeground );
	UxPutFontList( mn_Mpc, TextFont );
	UxPutLabelString( mn_Mpc, "Distance (Mpc)" );

	UxPutLabelString( mn_dist, "" );
	UxPutTranslations( mn_dist, HelpBM );
	UxPutHeight( mn_dist, 35 );
	UxPutForeground( mn_dist, TextForeground );
	UxPutBorderColor( mn_dist, WindowBackground );
	UxPutBackground( mn_dist, WindowBackground );
	UxPutY( mn_dist, 425 );
	UxPutX( mn_dist, 260 );
	UxPutRowColumnType( mn_dist, "menu_option" );

	UxPutBorderColor( separatorBM5, WindowBackground );
	UxPutBackground( separatorBM5, WindowBackground );
	UxPutHeight( separatorBM5, 5 );
	UxPutWidth( separatorBM5, 492 );
	UxPutY( separatorBM5, 111 );
	UxPutX( separatorBM5, 0 );

	UxPutAlignment( lbl_ised, "alignment_beginning" );
	UxPutLabelString( lbl_ised, "ISED File :" );
	UxPutForeground( lbl_ised, TextForeground );
	UxPutFontList( lbl_ised, TextFont );
	UxPutBorderColor( lbl_ised, WindowBackground );
	UxPutBackground( lbl_ised, WindowBackground );
	UxPutHeight( lbl_ised, 23 );
	UxPutWidth( lbl_ised, 94 );
	UxPutY( lbl_ised, 81 );
	UxPutX( lbl_ised, 10 );

	UxPutTranslations( tf_ised, table_ised );
	UxPutMaxLength( tf_ised, 50 );
	UxPutForeground( tf_ised, TextForeground );
	UxPutHighlightOnEnter( tf_ised, "true" );
	UxPutFontList( tf_ised, TextFont );
	UxPutBorderColor( tf_ised, TextBackground );
	UxPutBackground( tf_ised, TextBackground );
	UxPutHeight( tf_ised, 35 );
	UxPutWidth( tf_ised, 210 );
	UxPutY( tf_ised, 75 );
	UxPutX( tf_ised, 180 );

	UxPutSpacing( meBruzual, 10 );
	UxPutPacking( meBruzual, "pack_tight" );
	UxPutBorderColor( meBruzual, MenuBackground );
	UxPutMenuAccelerator( meBruzual, "<KeyUp>F10" );
	UxPutForeground( meBruzual, MenuForeground );
	UxPutBackground( meBruzual, MenuBackground );
	UxPutY( meBruzual, 0 );
	UxPutX( meBruzual, 40 );
	UxPutRowColumnType( meBruzual, "menu_bar" );

	UxPutForeground( me_b_file, MenuForeground );
	UxPutBackground( me_b_file, MenuBackground );
	UxPutRowColumnType( me_b_file, "menu_pulldown" );

	UxPutMnemonic( me_b_open, "O" );
	UxPutBorderColor( me_b_open, MenuBackground );
	UxPutForeground( me_b_open, MenuForeground );
	UxPutFontList( me_b_open, BoldTextFont );
	UxPutBackground( me_b_open, MenuBackground );
	UxPutLabelString( me_b_open, "Open ..." );

	UxPutBorderColor( mt_b_file, MenuBackground );
	UxPutForeground( mt_b_file, MenuForeground );
	UxPutFontList( mt_b_file, BoldTextFont );
	UxPutBackground( mt_b_file, MenuBackground );
	UxPutMnemonic( mt_b_file, "F" );
	UxPutLabelString( mt_b_file, "File" );

	UxPutRowColumnType( me_b_help, "menu_pulldown" );

	UxPutBorderColor( me_b_hgissel, MenuBackground );
	UxPutBackground( me_b_hgissel, MenuBackground );
	UxPutForeground( me_b_hgissel, MenuForeground );
	UxPutFontList( me_b_hgissel, BoldTextFont );
	UxPutMnemonic( me_b_hgissel, "O" );
	UxPutLabelString( me_b_hgissel, "On GISSEL Model ..." );

	UxPutBorderColor( mt_b_help, MenuBackground );
	UxPutBackground( mt_b_help, MenuBackground );
	UxPutForeground( mt_b_help, MenuForeground );
	UxPutFontList( mt_b_help, BoldTextFont );
	UxPutMnemonic( mt_b_help, "H" );
	UxPutLabelString( mt_b_help, "Help" );

	UxPutX( me_b_quit, 400 );
	UxPutRowColumnType( me_b_quit, "menu_pulldown" );

	UxPutBorderColor( me_b_bye, MenuBackground );
	UxPutBackground( me_b_bye, MenuBackground );
	UxPutForeground( me_b_bye, MenuForeground );
	UxPutFontList( me_b_bye, BoldTextFont );
	UxPutMnemonic( me_b_bye, "B" );
	UxPutLabelString( me_b_bye, "Bye" );

	UxPutAlignment( mt_b_quit, "alignment_end" );
	UxPutX( mt_b_quit, 400 );
	UxPutBorderColor( mt_b_quit, MenuBackground );
	UxPutBackground( mt_b_quit, MenuBackground );
	UxPutForeground( mt_b_quit, MenuForeground );
	UxPutFontList( mt_b_quit, BoldTextFont );
	UxPutMnemonic( mt_b_quit, "Q" );
	UxPutLabelString( mt_b_quit, "Quit" );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_BruzualModel()
{
	/* Create the swidgets */

	BruzualModel = UxCreateApplicationShell( "BruzualModel", NO_PARENT );
	UxPutContext( BruzualModel, UxBruzualModelContext );

	form_BM = UxCreateForm( "form_BM", BruzualModel );
	lbl_copyright = UxCreateLabel( "lbl_copyright", form_BM );
	form_BM2 = UxCreateForm( "form_BM2", form_BM );
	BM_newised = UxCreatePushButton( "BM_newised", form_BM2 );
	BM_apply = UxCreatePushButton( "BM_apply", form_BM2 );
	lbl_sfr_c = UxCreateLabel( "lbl_sfr_c", form_BM );
	lbl_ssp = UxCreateLabel( "lbl_ssp", form_BM );
	lbl_sfr = UxCreateLabel( "lbl_sfr", form_BM );
	label1 = UxCreateLabel( "label1", form_BM );
	lbl_output = UxCreateLabel( "lbl_output", form_BM );
	lbl_H0 = UxCreateLabel( "lbl_H0", form_BM );
	lbl_age = UxCreateLabel( "lbl_age", form_BM );
	gaz_rec = UxCreateRowColumn( "gaz_rec", form_BM );
	tg_gaz = UxCreateToggleButton( "tg_gaz", gaz_rec );
	tg_nogaz = UxCreateToggleButton( "tg_nogaz", gaz_rec );
	tf_wvmin = UxCreateText( "tf_wvmin", form_BM );
	tf_wvmax = UxCreateText( "tf_wvmax", form_BM );
	tf_age = UxCreateText( "tf_age", form_BM );
	tf_omega = UxCreateText( "tf_omega", form_BM );
	tf_z = UxCreateText( "tf_z", form_BM );
	tf_H0 = UxCreateText( "tf_H0", form_BM );
	helptextBM = UxCreateText( "helptextBM", form_BM );
	separatorBM = UxCreateSeparator( "separatorBM", form_BM );
	menu_SFR = UxCreateRowColumn( "menu_SFR", form_BM );
	mn_exptau = UxCreatePushButton( "mn_exptau", menu_SFR );
	mn_expmu = UxCreatePushButton( "mn_expmu", menu_SFR );
	mn_single = UxCreatePushButton( "mn_single", menu_SFR );
	mn_constant = UxCreatePushButton( "mn_constant", menu_SFR );
	mn_SFR = UxCreateRowColumn( "mn_SFR", form_BM );
	lbl_gaz = UxCreateLabel( "lbl_gaz", form_BM );
	tf_sfr_c = UxCreateText( "tf_sfr_c", form_BM );
	separatorBM1 = UxCreateSeparator( "separatorBM1", form_BM );
	lbl_Omega1 = UxCreateLabel( "lbl_Omega1", form_BM );
	tf_output = UxCreateText( "tf_output", form_BM );
	separatorBM2 = UxCreateSeparator( "separatorBM2", form_BM );
	separatorBM3 = UxCreateSeparator( "separatorBM3", form_BM );
	menu_IMF = UxCreateRowColumn( "menu_IMF", form_BM );
	mn_sp = UxCreatePushButton( "mn_sp", menu_IMF );
	mn_sc = UxCreatePushButton( "mn_sc", menu_IMF );
	mn_ms = UxCreatePushButton( "mn_ms", menu_IMF );
	mn_IMF = UxCreateRowColumn( "mn_IMF", form_BM );
	lbl_mass = UxCreateLabel( "lbl_mass", form_BM );
	menu_mass = UxCreateRowColumn( "menu_mass", form_BM );
	mn_m1 = UxCreatePushButton( "mn_m1", menu_mass );
	mn_m2 = UxCreatePushButton( "mn_m2", menu_mass );
	mn_m3 = UxCreatePushButton( "mn_m3", menu_mass );
	mn_m4 = UxCreatePushButton( "mn_m4", menu_mass );
	mn_m5 = UxCreatePushButton( "mn_m5", menu_mass );
	mn_mass = UxCreateRowColumn( "mn_mass", form_BM );
	lbl_stpop = UxCreateLabel( "lbl_stpop", form_BM );
	stellar_pop = UxCreateRowColumn( "stellar_pop", form_BM );
	tg_ssp = UxCreateToggleButton( "tg_ssp", stellar_pop );
	tg_csp = UxCreateToggleButton( "tg_csp", stellar_pop );
	separatorBM4 = UxCreateSeparator( "separatorBM4", form_BM );
	lbl_massgal = UxCreateLabel( "lbl_massgal", form_BM );
	tf_massgal = UxCreateText( "tf_massgal", form_BM );
	menu_dist = UxCreateRowColumn( "menu_dist", form_BM );
	mn_z = UxCreatePushButton( "mn_z", menu_dist );
	mn_Mpc = UxCreatePushButton( "mn_Mpc", menu_dist );
	mn_dist = UxCreateRowColumn( "mn_dist", form_BM );
	separatorBM5 = UxCreateSeparator( "separatorBM5", form_BM );
	lbl_ised = UxCreateLabel( "lbl_ised", form_BM );
	tf_ised = UxCreateText( "tf_ised", form_BM );
	meBruzual = UxCreateRowColumn( "meBruzual", form_BM );
	me_b_file = UxCreateRowColumn( "me_b_file", meBruzual );
	me_b_open = UxCreatePushButton( "me_b_open", me_b_file );
	mt_b_file = UxCreateCascadeButton( "mt_b_file", meBruzual );
	me_b_help = UxCreateRowColumn( "me_b_help", meBruzual );
	me_b_hgissel = UxCreatePushButton( "me_b_hgissel", me_b_help );
	mt_b_help = UxCreateCascadeButton( "mt_b_help", meBruzual );
	me_b_quit = UxCreateRowColumn( "me_b_quit", meBruzual );
	me_b_bye = UxCreatePushButton( "me_b_bye", me_b_quit );
	mt_b_quit = UxCreateCascadeButton( "mt_b_quit", meBruzual );

	_Uxinit_BruzualModel();

	/* Create the X widgets */

	UxCreateWidget( BruzualModel );
	UxCreateWidget( form_BM );
	UxPutTopAttachment( lbl_copyright, "attach_none" );
	UxPutRightAttachment( lbl_copyright, "attach_form" );
	UxPutLeftAttachment( lbl_copyright, "attach_form" );
	UxCreateWidget( lbl_copyright );

	UxPutRightAttachment( form_BM2, "attach_form" );
	UxPutLeftAttachment( form_BM2, "attach_form" );
	UxPutBottomOffset( form_BM2, 2 );
	UxPutBottomAttachment( form_BM2, "attach_form" );
	UxCreateWidget( form_BM2 );

	UxCreateWidget( BM_newised );
	UxCreateWidget( BM_apply );
	UxCreateWidget( lbl_sfr_c );
	UxCreateWidget( lbl_ssp );
	UxCreateWidget( lbl_sfr );
	UxCreateWidget( label1 );
	UxCreateWidget( lbl_output );
	UxCreateWidget( lbl_H0 );
	UxCreateWidget( lbl_age );
	UxCreateWidget( gaz_rec );
	UxCreateWidget( tg_gaz );
	UxCreateWidget( tg_nogaz );
	UxCreateWidget( tf_wvmin );
	UxCreateWidget( tf_wvmax );
	UxCreateWidget( tf_age );
	UxCreateWidget( tf_omega );
	UxCreateWidget( tf_z );
	UxCreateWidget( tf_H0 );
	UxPutRightAttachment( helptextBM, "attach_form" );
	UxPutLeftAttachment( helptextBM, "attach_form" );
	UxPutBottomWidget( helptextBM, "form_BM2" );
	UxPutBottomOffset( helptextBM, 2 );
	UxPutBottomAttachment( helptextBM, "attach_widget" );
	UxCreateWidget( helptextBM );

	UxPutRightAttachment( separatorBM, "attach_form" );
	UxPutLeftAttachment( separatorBM, "attach_form" );
	UxPutBottomWidget( separatorBM, "helptextBM" );
	UxPutBottomAttachment( separatorBM, "attach_widget" );
	UxCreateWidget( separatorBM );

	UxCreateWidget( menu_SFR );
	UxCreateWidget( mn_exptau );
	UxCreateWidget( mn_expmu );
	UxCreateWidget( mn_single );
	UxCreateWidget( mn_constant );
	UxPutSubMenuId( mn_SFR, "menu_SFR" );
	UxCreateWidget( mn_SFR );

	UxCreateWidget( lbl_gaz );
	UxCreateWidget( tf_sfr_c );
	UxPutRightAttachment( separatorBM1, "attach_form" );
	UxPutLeftAttachment( separatorBM1, "attach_form" );
	UxCreateWidget( separatorBM1 );

	UxCreateWidget( lbl_Omega1 );
	UxCreateWidget( tf_output );
	UxPutRightAttachment( separatorBM2, "attach_form" );
	UxPutLeftAttachment( separatorBM2, "attach_form" );
	UxCreateWidget( separatorBM2 );

	UxPutRightAttachment( separatorBM3, "attach_form" );
	UxPutLeftAttachment( separatorBM3, "attach_form" );
	UxCreateWidget( separatorBM3 );

	UxCreateWidget( menu_IMF );
	UxCreateWidget( mn_sp );
	UxCreateWidget( mn_sc );
	UxCreateWidget( mn_ms );
	UxPutSubMenuId( mn_IMF, "menu_IMF" );
	UxCreateWidget( mn_IMF );

	UxCreateWidget( lbl_mass );
	UxCreateWidget( menu_mass );
	UxCreateWidget( mn_m1 );
	UxCreateWidget( mn_m2 );
	UxCreateWidget( mn_m3 );
	UxCreateWidget( mn_m4 );
	UxCreateWidget( mn_m5 );
	UxPutSubMenuId( mn_mass, "menu_mass" );
	UxCreateWidget( mn_mass );

	UxCreateWidget( lbl_stpop );
	UxCreateWidget( stellar_pop );
	UxCreateWidget( tg_ssp );
	UxCreateWidget( tg_csp );
	UxPutRightAttachment( separatorBM4, "attach_form" );
	UxPutLeftAttachment( separatorBM4, "attach_form" );
	UxCreateWidget( separatorBM4 );

	UxCreateWidget( lbl_massgal );
	UxCreateWidget( tf_massgal );
	UxCreateWidget( menu_dist );
	UxCreateWidget( mn_z );
	UxCreateWidget( mn_Mpc );
	UxPutSubMenuId( mn_dist, "menu_dist" );
	UxCreateWidget( mn_dist );

	UxPutRightAttachment( separatorBM5, "attach_form" );
	UxPutLeftAttachment( separatorBM5, "attach_form" );
	UxCreateWidget( separatorBM5 );

	UxCreateWidget( lbl_ised );
	UxCreateWidget( tf_ised );
	UxPutTopAttachment( meBruzual, "attach_form" );
	UxPutRightAttachment( meBruzual, "attach_form" );
	UxPutLeftAttachment( meBruzual, "attach_form" );
	UxCreateWidget( meBruzual );

	UxCreateWidget( me_b_file );
	UxCreateWidget( me_b_open );
	UxPutSubMenuId( mt_b_file, "me_b_file" );
	UxCreateWidget( mt_b_file );

	UxCreateWidget( me_b_help );
	UxCreateWidget( me_b_hgissel );
	UxPutSubMenuId( mt_b_help, "me_b_help" );
	UxCreateWidget( mt_b_help );

	UxCreateWidget( me_b_quit );
	UxCreateWidget( me_b_bye );
	UxPutSubMenuId( mt_b_quit, "me_b_quit" );
	UxCreateWidget( mt_b_quit );


	UxAddCallback( BM_newised, XmNactivateCallback,
			activateCB_BM_newised,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( BM_apply, XmNactivateCallback,
			activateCB_BM_apply,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tg_gaz, XmNarmCallback,
			armCB_tg_gaz,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tg_nogaz, XmNarmCallback,
			armCB_tg_nogaz,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tg_nogaz, XmNvalueChangedCallback,
			valueChangedCB_tg_nogaz,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tf_wvmin, XmNfocusCallback,
			focusCB_tf_wvmin,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_wvmin, XmNlosingFocusCallback,
			losingFocusCB_tf_wvmin,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tf_wvmax, XmNfocusCallback,
			focusCB_tf_wvmax,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_wvmax, XmNlosingFocusCallback,
			losingFocusCB_tf_wvmax,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tf_age, XmNfocusCallback,
			focusCB_tf_age,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_age, XmNlosingFocusCallback,
			losingFocusCB_tf_age,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tf_omega, XmNfocusCallback,
			focusCB_tf_omega,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_omega, XmNlosingFocusCallback,
			losingFocusCB_tf_omega,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tf_z, XmNfocusCallback,
			focusCB_tf_z,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_z, XmNlosingFocusCallback,
			losingFocusCB_tf_z,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tf_H0, XmNfocusCallback,
			focusCB_tf_H0,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_H0, XmNlosingFocusCallback,
			losingFocusCB_tf_H0,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_exptau, XmNactivateCallback,
			activateCB_mn_exptau,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_expmu, XmNactivateCallback,
			activateCB_mn_expmu,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_single, XmNactivateCallback,
			activateCB_mn_single,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_constant, XmNactivateCallback,
			activateCB_mn_constant,
			(XtPointer) UxBruzualModelContext );

	UxPutMenuHistory( mn_SFR, "mn_exptau" );

	UxAddCallback( tf_sfr_c, XmNfocusCallback,
			focusCB_tf_sfr_c,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_sfr_c, XmNlosingFocusCallback,
			losingFocusCB_tf_sfr_c,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tf_output, XmNmotionVerifyCallback,
			motionVerifyCB_tf_output,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_output, XmNfocusCallback,
			focusCB_tf_output,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_output, XmNlosingFocusCallback,
			losingFocusCB_tf_output,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_sp, XmNactivateCallback,
			activateCB_mn_sp,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_sc, XmNactivateCallback,
			activateCB_mn_sc,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_ms, XmNactivateCallback,
			activateCB_mn_ms,
			(XtPointer) UxBruzualModelContext );

	UxPutMenuHistory( mn_IMF, "mn_sc" );

	UxAddCallback( mn_m1, XmNactivateCallback,
			activateCB_mn_m1,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_m2, XmNactivateCallback,
			activateCB_mn_m2,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_m3, XmNactivateCallback,
			activateCB_mn_m3,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_m4, XmNactivateCallback,
			activateCB_mn_m4,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_m5, XmNactivateCallback,
			activateCB_mn_m5,
			(XtPointer) UxBruzualModelContext );

	UxPutMenuHistory( mn_mass, "mn_m1" );

	UxAddCallback( tg_ssp, XmNarmCallback,
			armCB_tg_ssp,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tg_csp, XmNarmCallback,
			armCB_tg_csp,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tg_csp, XmNvalueChangedCallback,
			valueChangedCB_tg_csp,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( tf_massgal, XmNfocusCallback,
			focusCB_tf_massgal,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_massgal, XmNlosingFocusCallback,
			losingFocusCB_tf_massgal,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_z, XmNactivateCallback,
			activateCB_mn_z,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( mn_Mpc, XmNactivateCallback,
			activateCB_mn_Mpc,
			(XtPointer) UxBruzualModelContext );

	UxPutMenuHistory( mn_dist, "mn_z" );

	UxAddCallback( tf_ised, XmNfocusCallback,
			focusCB_tf_ised,
			(XtPointer) UxBruzualModelContext );
	UxAddCallback( tf_ised, XmNlosingFocusCallback,
			losingFocusCB_tf_ised,
			(XtPointer) UxBruzualModelContext );

	UxPutMenuHelpWidget( meBruzual, "mt_b_quit" );

	UxAddCallback( me_b_open, XmNactivateCallback,
			activateCB_me_b_open,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( me_b_hgissel, XmNactivateCallback,
			activateCB_me_b_hgissel,
			(XtPointer) UxBruzualModelContext );

	UxAddCallback( me_b_bye, XmNactivateCallback,
			activateCB_me_b_bye,
			(XtPointer) UxBruzualModelContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( BruzualModel );

	return ( BruzualModel );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_BruzualModel()
{
	swidget                 rtrn;
	_UxCBruzualModel        *UxContext;

	UxBruzualModelContext = UxContext =
		(_UxCBruzualModel *) UxMalloc( sizeof(_UxCBruzualModel) );

	rtrn = _Uxbuild_BruzualModel();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_BruzualModel()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearShortBM", action_ClearShortBM },
				{ "HelpShortBM", action_HelpShortBM },
				{ "HelpHelp", action_HelpHelp },
				{ "ChooseListUpOUT", action_ChooseListUpOUT },
				{ "ChooseListUpISED", action_ChooseListUpISED }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_BruzualModel();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

