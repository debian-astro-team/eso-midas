/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	WaveShell.c

.VERSION
 090826         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTogB.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h> 
#include <global.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxform10;
	swidget	Uxform11;
	swidget	UxApplyWave;
	swidget	UxCancelWave;
	swidget	Uxseparator12;
	swidget	Uxlabel16;
	swidget	Uxtextcwmin;
	swidget	Uxlabel2;
	swidget	Uxtextcwmax;
	swidget	Uxtg_imaqual;
} _UxCWaveShell;

#define form10                  UxWaveShellContext->Uxform10
#define form11                  UxWaveShellContext->Uxform11
#define ApplyWave               UxWaveShellContext->UxApplyWave
#define CancelWave              UxWaveShellContext->UxCancelWave
#define separator12             UxWaveShellContext->Uxseparator12
#define label16                 UxWaveShellContext->Uxlabel16
#define textcwmin               UxWaveShellContext->Uxtextcwmin
#define label2                  UxWaveShellContext->Uxlabel2
#define textcwmax               UxWaveShellContext->Uxtextcwmax
#define tg_imaqual              UxWaveShellContext->Uxtg_imaqual

static _UxCWaveShell	*UxWaveShellContext;

extern void search_cwav();


swidget	WaveShell;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*Wtext_tab = "#override\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_WaveShell();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_ApplyWave( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveShellContext;
	UxWaveShellContext = UxContext =
			(_UxCWaveShell *) UxGetContext( UxThisWidget );
	{
	extern float cw1,cw2;
	
	search_cwav(cw1,cw2);
	UxPopdownInterface(WaveShell);
	}
	UxWaveShellContext = UxSaveCtx;
}

static void	activateCB_CancelWave( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveShellContext;
	UxWaveShellContext = UxContext =
			(_UxCWaveShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(WaveShell);
	}
	UxWaveShellContext = UxSaveCtx;
}

static void	losingFocusCB_textcwmin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveShellContext;
	UxWaveShellContext = UxContext =
			(_UxCWaveShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[10];
	float minw;
	extern float cw1;
	
	nid=sscanf(UxGetText(UxFindSwidget("textcwmin")),"%f",&minw);
	if (nid==1)
		{
		if ((minw>=300.)&&(minw<=1100.))
			cw1=minw;
		else if (minw<300.)
			cw1=300.;
		else if (minw>1100.)
			cw1=1100.;
		}
	
	sprintf(am,"%.1f",cw1);
	UxPutText(UxFindSwidget("textcwmin"),am);
	
	}
	UxWaveShellContext = UxSaveCtx;
}

static void	losingFocusCB_textcwmax( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCWaveShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxWaveShellContext;
	UxWaveShellContext = UxContext =
			(_UxCWaveShell *) UxGetContext( UxThisWidget );
	{
	int nid;
	char am[20];
	float maxw;
	extern float cw2;
	
	
	nid=sscanf(UxGetText(UxFindSwidget("textcwmax")),"%f",&maxw);
	if (nid==1)
		{
		if ((maxw>=400.)&&(maxw<=1200.))
			cw2=maxw;
		else if (maxw<400.)
			cw2=400.;
		else if (maxw>1200.)
			cw2=1200.;
		}
	
	sprintf(am,"%.1f",cw2);
	UxPutText(UxFindSwidget("textcwmax"),am);
	
	}
	UxWaveShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_WaveShell()
{
	UxPutBorderColor( WaveShell, WindowBackground );
	UxPutIconName( WaveShell, "WaveShell" );
	UxPutKeyboardFocusPolicy( WaveShell, "pointer" );
	UxPutTitle( WaveShell, "Central Wavelength" );
	UxPutHeight( WaveShell, 170 );
	UxPutWidth( WaveShell, 316 );
	UxPutY( WaveShell, 521 );
	UxPutX( WaveShell, 602 );

	UxPutBorderColor( form10, WindowBackground );
	UxPutBackground( form10, WindowBackground );
	UxPutHeight( form10, 100 );
	UxPutWidth( form10, 490 );
	UxPutY( form10, 0 );
	UxPutX( form10, 0 );
	UxPutUnitType( form10, "pixels" );
	UxPutResizePolicy( form10, "resize_none" );

	UxPutBorderColor( form11, ButtonBackground );
	UxPutBackground( form11, ButtonBackground );
	UxPutHeight( form11, 40 );
	UxPutWidth( form11, 490 );
	UxPutY( form11, 120 );
	UxPutX( form11, 0 );
	UxPutResizePolicy( form11, "resize_none" );

	UxPutLabelString( ApplyWave, "Apply" );
	UxPutForeground( ApplyWave, ApplyForeground );
	UxPutFontList( ApplyWave, BoldTextFont );
	UxPutBorderColor( ApplyWave, ButtonBackground );
	UxPutBackground( ApplyWave, ButtonBackground );
	UxPutHeight( ApplyWave, 30 );
	UxPutWidth( ApplyWave, 86 );
	UxPutY( ApplyWave, 4 );
	UxPutX( ApplyWave, 8 );

	UxPutLabelString( CancelWave, "Cancel" );
	UxPutForeground( CancelWave, CancelForeground );
	UxPutFontList( CancelWave, BoldTextFont );
	UxPutBorderColor( CancelWave, ButtonBackground );
	UxPutBackground( CancelWave, ButtonBackground );
	UxPutHeight( CancelWave, 30 );
	UxPutWidth( CancelWave, 86 );
	UxPutY( CancelWave, 4 );
	UxPutX( CancelWave, 102 );

	UxPutBorderColor( separator12, WindowBackground );
	UxPutBackground( separator12, WindowBackground );
	UxPutHeight( separator12, 10 );
	UxPutWidth( separator12, 492 );
	UxPutY( separator12, 110 );
	UxPutX( separator12, 0 );

	UxPutForeground( label16, TextForeground );
	UxPutAlignment( label16, "alignment_beginning" );
	UxPutLabelString( label16, "Central Wavelenght min (nm) :" );
	UxPutFontList( label16, TextFont );
	UxPutBorderColor( label16, LabelBackground );
	UxPutBackground( label16, LabelBackground );
	UxPutHeight( label16, 30 );
	UxPutWidth( label16, 206 );
	UxPutY( label16, 8 );
	UxPutX( label16, 10 );

	UxPutTranslations( textcwmin, Wtext_tab );
	UxPutSelectionArrayCount( textcwmin, 3 );
	UxPutMaxLength( textcwmin, 200 );
	UxPutForeground( textcwmin, TextForeground );
	UxPutFontList( textcwmin, TextFont );
	UxPutBorderColor( textcwmin, TextBackground );
	UxPutBackground( textcwmin, TextBackground );
	UxPutHeight( textcwmin, 35 );
	UxPutWidth( textcwmin, 80 );
	UxPutY( textcwmin, 8 );
	UxPutX( textcwmin, 224 );

	UxPutForeground( label2, TextForeground );
	UxPutAlignment( label2, "alignment_beginning" );
	UxPutLabelString( label2, "Central Wavelenght max (nm) :" );
	UxPutFontList( label2, TextFont );
	UxPutBorderColor( label2, LabelBackground );
	UxPutBackground( label2, LabelBackground );
	UxPutHeight( label2, 30 );
	UxPutWidth( label2, 206 );
	UxPutY( label2, 45 );
	UxPutX( label2, 10 );

	UxPutTranslations( textcwmax, Wtext_tab );
	UxPutSelectionArrayCount( textcwmax, 3 );
	UxPutMaxLength( textcwmax, 200 );
	UxPutForeground( textcwmax, TextForeground );
	UxPutFontList( textcwmax, TextFont );
	UxPutBorderColor( textcwmax, TextBackground );
	UxPutBackground( textcwmax, TextBackground );
	UxPutHeight( textcwmax, 35 );
	UxPutWidth( textcwmax, 80 );
	UxPutY( textcwmax, 43 );
	UxPutX( textcwmax, 224 );

	UxPutHighlightOnEnter( tg_imaqual, "true" );
	UxPutForeground( tg_imaqual, TextForeground );
	UxPutSelectColor( tg_imaqual, SelectColor );
	UxPutSet( tg_imaqual, "true" );
	UxPutLabelString( tg_imaqual, "Image Quality" );
	UxPutFontList( tg_imaqual, TextFont );
	UxPutBorderColor( tg_imaqual, WindowBackground );
	UxPutBackground( tg_imaqual, WindowBackground );
	UxPutHeight( tg_imaqual, 26 );
	UxPutWidth( tg_imaqual, 134 );
	UxPutY( tg_imaqual, 84 );
	UxPutX( tg_imaqual, 10 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_WaveShell()
{
	/* Create the swidgets */

	WaveShell = UxCreateApplicationShell( "WaveShell", NO_PARENT );
	UxPutContext( WaveShell, UxWaveShellContext );

	form10 = UxCreateForm( "form10", WaveShell );
	form11 = UxCreateForm( "form11", form10 );
	ApplyWave = UxCreatePushButton( "ApplyWave", form11 );
	CancelWave = UxCreatePushButton( "CancelWave", form11 );
	separator12 = UxCreateSeparator( "separator12", form10 );
	label16 = UxCreateLabel( "label16", form10 );
	textcwmin = UxCreateText( "textcwmin", form10 );
	label2 = UxCreateLabel( "label2", form10 );
	textcwmax = UxCreateText( "textcwmax", form10 );
	tg_imaqual = UxCreateToggleButton( "tg_imaqual", form10 );

	_Uxinit_WaveShell();

	/* Create the X widgets */

	UxCreateWidget( WaveShell );
	UxCreateWidget( form10 );
	UxPutRightAttachment( form11, "attach_form" );
	UxPutLeftAttachment( form11, "attach_form" );
	UxPutBottomOffset( form11, 2 );
	UxPutBottomAttachment( form11, "attach_form" );
	UxCreateWidget( form11 );

	UxCreateWidget( ApplyWave );
	UxCreateWidget( CancelWave );
	UxPutRightAttachment( separator12, "attach_form" );
	UxPutLeftAttachment( separator12, "attach_form" );
	UxPutBottomWidget( separator12, "form11" );
	UxPutBottomOffset( separator12, 0 );
	UxPutBottomAttachment( separator12, "attach_widget" );
	UxCreateWidget( separator12 );

	UxCreateWidget( label16 );
	UxPutLeftOffset( textcwmin, 260 );
	UxCreateWidget( textcwmin );

	UxCreateWidget( label2 );
	UxCreateWidget( textcwmax );
	UxCreateWidget( tg_imaqual );

	UxAddCallback( ApplyWave, XmNactivateCallback,
			activateCB_ApplyWave,
			(XtPointer) UxWaveShellContext );

	UxAddCallback( CancelWave, XmNactivateCallback,
			activateCB_CancelWave,
			(XtPointer) UxWaveShellContext );

	UxAddCallback( textcwmin, XmNlosingFocusCallback,
			losingFocusCB_textcwmin,
			(XtPointer) UxWaveShellContext );

	UxAddCallback( textcwmax, XmNlosingFocusCallback,
			losingFocusCB_textcwmax,
			(XtPointer) UxWaveShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( WaveShell );

	return ( WaveShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_WaveShell()
{
	swidget                 rtrn;
	_UxCWaveShell           *UxContext;

	UxWaveShellContext = UxContext =
		(_UxCWaveShell *) UxMalloc( sizeof(_UxCWaveShell) );

	rtrn = _Uxbuild_WaveShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_WaveShell()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_WaveShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

