/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826		last modif


===========================================================================*/


#include <stdlib.h>
#include <stdio.h>
#include <UxLib.h>
#include <model.h>


extern int flire(), fmot();








void	def_inst(instname)

char *instname;

{
extern struct s_trans	T;
extern struct s_gui	G;

FILE	*ins,*bf;
char	*dirdata;
char	name[80],bfilter[80],first[80],second[50],third[50];
int	i;

dirdata=(char *) getenv("MID_FILTERS");
if (dirdata==NULL)
   fprintf(stderr,"ERROR: cannot find MID_FILTERS environment variable\r\n");

/* open and read instrument definition file */

sprintf(name,"%s/inst/%s",dirdata,instname);

ins=fopen(name,"r");
if (ins == (FILE *)0)
        fprintf(stderr,
		"ERROR: instrument definition file:%s does not exist\r\n",name);
else
{
while(flire(ins,first)!=-1)
{
T.dsensitive=1;

  if (strncmp(first,"#",1))
	{
        if (!strncmp(first,"title",5))
                {
                flire(ins,second);
		UxPutLabelString(UxFindSwidget("lbl_title"),second); 
                }
        else if (!strncmp(first,"optics",6))
                {
		do{
		 fmot(ins,second);
		 if (!strcmp(second,"mirnew"))
			{
                	flire(ins,third);
			sprintf(T.mirnewfile,"%s/%s/%s",dirdata,instname,third);
			}
		 else if (!strcmp(second,"mirold"))
			{
                	flire(ins,third);
			sprintf(T.miroldfile,"%s/%s/%s",dirdata,instname,third);
			}
		 else if (!strcmp(second,"surface"))
			{
                	fscanf(ins,"%f",&T.surface); flire(ins,third);
			}
		 else if (!strcmp(second,"ofile"))
			{
                	flire(ins,third);
			sprintf(T.opticsfile,"%s/%s/%s",dirdata,instname,third);
			}
		 } while(strcmp(second,"end"));
                }
        else if (!strncmp(first,"disp",4))
                {
		do{
		 fmot(ins,second);
		 if (!strcmp(second,"sensitive"))
			{
                	flire(ins,third);
			if (!strcmp(third,"no"))
			   {
			   /* fprintf(stderr,"not sensitive\r\n"); */
			   T.dsensitive=0;
			   }
			else
			   UxPutSensitive(UxFindSwidget("mn_wgrism"),"true");
			}
		 else if (!strcmp(second,"free"))
			{
                	flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_nogrism"),third);
			}
		 else if (!strcmp(second,"disp0"))
			{
                	fscanf(ins,"%f%s",&T.res[0],third);
			sprintf(T.gsmfl[0],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism0"),third);
			}
		 else if (!strcmp(second,"disp1"))
			{
                        fscanf(ins,"%f%s",&T.res[1],third);
                        sprintf(T.gsmfl[1],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism1"),third);
			}
		 else if (!strcmp(second,"disp2"))
			{
                        fscanf(ins,"%f%s",&T.res[2],third);
                        sprintf(T.gsmfl[2],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism2"),third);
			}
		 else if (!strcmp(second,"disp3"))
			{
                        fscanf(ins,"%f%s",&T.res[3],third);
                        sprintf(T.gsmfl[3],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism3"),third);
			}
		 else if (!strcmp(second,"disp4"))
			{
                        fscanf(ins,"%f%s",&T.res[4],third);
                        sprintf(T.gsmfl[4],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism4"),third);
			}
		 else if (!strcmp(second,"disp5"))
			{
                        fscanf(ins,"%f%s",&T.res[5],third);
                        sprintf(T.gsmfl[5],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism5"),third);
			}
		 else if (!strcmp(second,"disp6"))
			{
                        fscanf(ins,"%f%s",&T.res[6],third);
                        sprintf(T.gsmfl[6],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism6"),third);
			}
		 else if (!strcmp(second,"disp7"))
			{
                        fscanf(ins,"%f%s",&T.res[7],third);
                        sprintf(T.gsmfl[7],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism7"),third);
			}
		 else if (!strcmp(second,"disp8"))
			{
                        fscanf(ins,"%f%s",&T.res[8],third);
                        sprintf(T.gsmfl[8],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism8"),third);
			}
		 else if (!strcmp(second,"disp9"))
			{
                        fscanf(ins,"%f%s",&T.res[9],third);
                        sprintf(T.gsmfl[9],"%s/%s/%s",dirdata,instname,third);
			flire(ins,third);
			UxPutLabelString(UxFindSwidget("mn_grism9"),third);
			}
		 } while(strcmp(second,"end"));
                }
        else if (!strcmp(first,"ccd"))
                {
		do{
		 fmot(ins,second);
		 if (!strcmp(second,"title"))
			{
                	flire(ins,third);
			strcat(third," :");
			UxPutLabelString(UxFindSwidget("lbl_ccd"),third);
			}
		 else if (!strcmp(second,"nccd"))
			{
                	fscanf(ins,"%d",&T.nccd); flire(ins,third);
			}
		 else if (!strcmp(second,"pixel"))
			{
                	fscanf(ins,"%f",&T.pixel); flire(ins,third);
			}
		 else if (!strcmp(second,"ron"))
			{
                	fscanf(ins,"%f",&T.ron); flire(ins,third);
			}
		 else if (!strcmp(second,"gain"))
			{
                	fscanf(ins,"%f",&T.einadu); flire(ins,third);
			}
		 else if (!strcmp(second,"bias"))
			{
                	fscanf(ins,"%f",&T.bias); flire(ins,third);
			}
		 } while(strcmp(second,"end"));
                }
        else if (!strcmp(first,"filter"))
                {
		do{
		 fmot(ins,second);
		 if (!strcmp(second,"basic"))
			{
                	flire(ins,third);
			sprintf(bfilter,"%s/%s/%s",dirdata,instname,third);
			bf=fopen(bfilter,"r");
			i=0;
			while(flire(bf,third)!=-1)
				strcpy(T.basicfilt[i++],third);
			T.nbasic=i;
			fclose(bf);
			}
		 } while(strcmp(second,"end"));
                }
	};
};

fclose(ins);

/******************* others instrument constants *******************/

/* instrument selection */
strcpy(G.sel_inst,instname);

/* miroirs */
T.imirror=1;

/* optics */
T.ioptics=1;

/* grism */
T.igrism=0;
T.ngrism=0;
T.grismfile[0]='\0';

/* filter */
T.nfilter=0;
T.ifilter=0;
T.filterfile[0]='\0';

/* ccd */
T.iccd=1;

}


}
