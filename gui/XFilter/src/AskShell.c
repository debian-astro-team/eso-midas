/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	AskShell.c

.VERSION
 090826         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxText.h"
#include "UxLabel.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h> 
#include <global.h>
#include <model.h>

extern struct s_gui G;

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxformAsk;
	swidget	UxformAsk2;
	swidget	UxOkAsk;
	swidget	UxCancelAsk;
	swidget	UxseparatorAsk;
	swidget	UxlabelAsk;
	swidget	UxtextAsk;
} _UxCAskShell;

#define formAsk                 UxAskShellContext->UxformAsk
#define formAsk2                UxAskShellContext->UxformAsk2
#define OkAsk                   UxAskShellContext->UxOkAsk
#define CancelAsk               UxAskShellContext->UxCancelAsk
#define separatorAsk            UxAskShellContext->UxseparatorAsk
#define labelAsk                UxAskShellContext->UxlabelAsk
#define textAsk                 UxAskShellContext->UxtextAsk

static _UxCAskShell	*UxAskShellContext;

extern void s_esoccd(), s_esofilter(), create_bbody();
extern void create_flux(), create_csp(), save_filt();
extern void save_curve(), save_mod(), SetFileList();




swidget	AskShell;

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*chooseTL = "#override\n\
<Btn3Down>:ChooseListUpL()\n\
<Key>Delete:delete-previous-character()\n\
<Key>BackSpace:delete-previous-character()\n\
<Key>osfDelete:delete-previous-character()\n\
<Key>osfBackSpace:delete-previous-character()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_AskShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ChooseListUpL( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCAskShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAskShellContext;
	UxAskShellContext = UxContext =
			(_UxCAskShell *) UxGetContext( UxThisWidget );
	{
	extern swidget scrolledchoose;
	extern struct s_gui    G;
	
	strcpy(G.sel_type,"trans_load");
	
	SetFileList( UxGetWidget(scrolledchoose),1, "*.trans" );
	
	UxPopupInterface(UxFindSwidget("ChooseList"),no_grab);
	
	}
	UxAskShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_OkAsk( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAskShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAskShellContext;
	UxAskShellContext = UxContext =
			(_UxCAskShell *) UxGetContext( UxThisWidget );
	{
	extern	char	Ask[];
	extern	char	Printer[];
	char	again[50];
	
	UxPopdownInterface(UxFindSwidget("ChooseList"));
	UxPopdownInterface(UxFindSwidget("AskShell"));
	
	if (sscanf(UxGetText(textAsk),"%s",again)==1)
		strcpy(Ask,again);
	
	if(!strcmp(G.sel_type,"trans_save"))
		save_mod(Ask);
	else if(!strcmp(G.sel_type,"curve_save"))
		save_curve(Ask);
	else if(!strcmp(G.sel_type,"extract_curve"))
		save_curve(Ask);
	else if(!strcmp(G.sel_type,"filter_save"))
		save_filt(Ask);
	else if (!strcmp(G.sel_type,"ised"))
		 create_csp(Ask);
	else if (!strcmp(G.sel_type,"flux"))
		 create_flux(Ask);
	else if (!strcmp(G.sel_type,"bbody"))
		 create_bbody(Ask);
	else if (!strcmp(G.sel_type,"printer"))
		 strcpy(Printer,Ask);
	else if (!strcmp(G.sel_type,"esofilter"))
		 s_esofilter(Ask);
	else if (!strcmp(G.sel_type,"esoccd"))
		 s_esoccd(Ask);
	}
	UxAskShellContext = UxSaveCtx;
}

static void	activateCB_CancelAsk( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAskShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAskShellContext;
	UxAskShellContext = UxContext =
			(_UxCAskShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(AskShell);
	}
	UxAskShellContext = UxSaveCtx;
}

static void	modifyVerifyCB_textAsk( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAskShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAskShellContext;
	UxAskShellContext = UxContext =
			(_UxCAskShell *) UxGetContext( UxThisWidget );
	{
	MOD_Ask = TRUE;
	}
	UxAskShellContext = UxSaveCtx;
}

static void	losingFocusCB_textAsk( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCAskShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxAskShellContext;
	UxAskShellContext = UxContext =
			(_UxCAskShell *) UxGetContext( UxThisWidget );
	{
	extern char Ask[];
	
	if(MOD_Ask)
	{ 
	  sscanf(UxGetText(textAsk),"%s",Ask);
	}
	MOD_Ask=FALSE;
	}
	UxAskShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_AskShell()
{
	UxPutBackground( AskShell, "grey80" );
	UxPutBorderColor( AskShell, WindowBackground );
	UxPutIconName( AskShell, "AskShell" );
	UxPutKeyboardFocusPolicy( AskShell, "pointer" );
	UxPutTitle( AskShell, "Ask" );
	UxPutHeight( AskShell, 105 );
	UxPutWidth( AskShell, 400 );
	UxPutY( AskShell, 500 );
	UxPutX( AskShell, 600 );

	UxPutBorderColor( formAsk, WindowBackground );
	UxPutBackground( formAsk, WindowBackground );
	UxPutHeight( formAsk, 100 );
	UxPutWidth( formAsk, 490 );
	UxPutY( formAsk, 0 );
	UxPutX( formAsk, 0 );
	UxPutUnitType( formAsk, "pixels" );
	UxPutResizePolicy( formAsk, "resize_none" );

	UxPutBorderColor( formAsk2, ButtonBackground );
	UxPutBackground( formAsk2, ButtonBackground );
	UxPutHeight( formAsk2, 40 );
	UxPutWidth( formAsk2, 490 );
	UxPutY( formAsk2, 120 );
	UxPutX( formAsk2, 0 );
	UxPutResizePolicy( formAsk2, "resize_none" );

	UxPutLabelString( OkAsk, "Ok" );
	UxPutForeground( OkAsk, ApplyForeground );
	UxPutFontList( OkAsk, BoldTextFont );
	UxPutBorderColor( OkAsk, ButtonBackground );
	UxPutBackground( OkAsk, ButtonBackground );
	UxPutHeight( OkAsk, 30 );
	UxPutWidth( OkAsk, 80 );
	UxPutY( OkAsk, 4 );
	UxPutX( OkAsk, 98 );

	UxPutLabelString( CancelAsk, "Cancel" );
	UxPutForeground( CancelAsk, CancelForeground );
	UxPutFontList( CancelAsk, BoldTextFont );
	UxPutBorderColor( CancelAsk, ButtonBackground );
	UxPutBackground( CancelAsk, ButtonBackground );
	UxPutHeight( CancelAsk, 30 );
	UxPutWidth( CancelAsk, 80 );
	UxPutY( CancelAsk, 4 );
	UxPutX( CancelAsk, 120 );

	UxPutBorderColor( separatorAsk, WindowBackground );
	UxPutBackground( separatorAsk, WindowBackground );
	UxPutHeight( separatorAsk, 10 );
	UxPutWidth( separatorAsk, 492 );
	UxPutY( separatorAsk, 110 );
	UxPutX( separatorAsk, 0 );

	UxPutForeground( labelAsk, TextForeground );
	UxPutAlignment( labelAsk, "alignment_beginning" );
	UxPutLabelString( labelAsk, "Ask :" );
	UxPutFontList( labelAsk, TextFont );
	UxPutBorderColor( labelAsk, LabelBackground );
	UxPutBackground( labelAsk, LabelBackground );
	UxPutHeight( labelAsk, 30 );
	UxPutWidth( labelAsk, 125 );
	UxPutY( labelAsk, 12 );
	UxPutX( labelAsk, 10 );

	UxPutTranslations( textAsk, chooseTL );
	UxPutSelectionArrayCount( textAsk, 3 );
	UxPutMaxLength( textAsk, 200 );
	UxPutForeground( textAsk, TextForeground );
	UxPutFontList( textAsk, TextFont );
	UxPutBorderColor( textAsk, TextBackground );
	UxPutBackground( textAsk, TextBackground );
	UxPutHeight( textAsk, 35 );
	UxPutWidth( textAsk, 220 );
	UxPutY( textAsk, 10 );
	UxPutX( textAsk, 260 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_AskShell()
{
	/* Create the swidgets */

	AskShell = UxCreateApplicationShell( "AskShell", NO_PARENT );
	UxPutContext( AskShell, UxAskShellContext );

	formAsk = UxCreateForm( "formAsk", AskShell );
	formAsk2 = UxCreateForm( "formAsk2", formAsk );
	OkAsk = UxCreatePushButton( "OkAsk", formAsk2 );
	CancelAsk = UxCreatePushButton( "CancelAsk", formAsk2 );
	separatorAsk = UxCreateSeparator( "separatorAsk", formAsk );
	labelAsk = UxCreateLabel( "labelAsk", formAsk );
	textAsk = UxCreateText( "textAsk", formAsk );

	_Uxinit_AskShell();

	/* Create the X widgets */

	UxCreateWidget( AskShell );
	UxCreateWidget( formAsk );
	UxPutRightAttachment( formAsk2, "attach_form" );
	UxPutLeftAttachment( formAsk2, "attach_form" );
	UxPutTopOffset( formAsk2, 60 );
	UxPutTopAttachment( formAsk2, "attach_form" );
	UxCreateWidget( formAsk2 );

	UxPutLeftOffset( OkAsk, 20 );
	UxPutLeftAttachment( OkAsk, "attach_form" );
	UxCreateWidget( OkAsk );

	UxPutLeftOffset( CancelAsk, 120 );
	UxPutLeftAttachment( CancelAsk, "attach_form" );
	UxPutRightOffset( CancelAsk, 55 );
	UxPutRightAttachment( CancelAsk, "attach_none" );
	UxCreateWidget( CancelAsk );

	UxPutTopOffset( separatorAsk, 50 );
	UxPutTopAttachment( separatorAsk, "attach_form" );
	UxPutRightAttachment( separatorAsk, "attach_form" );
	UxPutLeftAttachment( separatorAsk, "attach_form" );
	UxCreateWidget( separatorAsk );

	UxPutTopOffset( labelAsk, 12 );
	UxPutTopAttachment( labelAsk, "attach_form" );
	UxPutLeftOffset( labelAsk, 10 );
	UxPutLeftAttachment( labelAsk, "attach_form" );
	UxCreateWidget( labelAsk );

	UxPutTopOffset( textAsk, 10 );
	UxPutTopAttachment( textAsk, "attach_form" );
	UxPutRightOffset( textAsk, 10 );
	UxPutRightAttachment( textAsk, "attach_form" );
	UxPutLeftOffset( textAsk, 260 );
	UxCreateWidget( textAsk );


	UxAddCallback( OkAsk, XmNactivateCallback,
			activateCB_OkAsk,
			(XtPointer) UxAskShellContext );

	UxAddCallback( CancelAsk, XmNactivateCallback,
			activateCB_CancelAsk,
			(XtPointer) UxAskShellContext );

	UxAddCallback( textAsk, XmNmodifyVerifyCallback,
			modifyVerifyCB_textAsk,
			(XtPointer) UxAskShellContext );
	UxAddCallback( textAsk, XmNlosingFocusCallback,
			losingFocusCB_textAsk,
			(XtPointer) UxAskShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( AskShell );

	return ( AskShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_AskShell()
{
	swidget                 rtrn;
	_UxCAskShell            *UxContext;

	UxAskShellContext = UxContext =
		(_UxCAskShell *) UxMalloc( sizeof(_UxCAskShell) );

	rtrn = _Uxbuild_AskShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_AskShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ChooseListUpL", action_ChooseListUpL }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_AskShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

