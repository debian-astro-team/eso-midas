/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <stdio.h>
#include <UxLib.h>
#include <model.h>

void	set_bbody()

{
extern struct s_bbody	N;
char	word[10];

N.T=9000.;
sprintf(word,"%.0f",N.T);
UxPutText( UxFindSwidget("tf_T"), word );

N.dist=50.;
sprintf(word,"%.0f",N.dist);
UxPutText( UxFindSwidget("tf_Dist"), word );

N.radius=1.;
sprintf(word,"%.2f",N.radius);
UxPutText( UxFindSwidget("tf_radius"), word );

N.wrmin=300.;
sprintf(word,"%.1f",N.wrmin);
UxPutText( UxFindSwidget("tf_wrmin"), word );
N.wrmax=1000.;
sprintf(word,"%.1f",N.wrmax);
UxPutText( UxFindSwidget("tf_wrmax"), word );

sprintf(N.outfile,"bb%.0f",N.T);

UxPopupInterface(UxFindSwidget("BlackBody"),no_grab);
}
