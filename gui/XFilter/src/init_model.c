/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826		last modif

===========================================================================*/



#include <stdlib.h>
#include <stdio.h>
#include <UxLib.h>
#include <model.h>

/****************************************************************/
/*                 Program  : convo				*/
/*                 Auteur   : jean-paul				*/
/****************************************************************/

void init_model()

{
extern struct s_gui	G;
extern struct s_mod	M;
extern struct s_trans	T;
extern char Printer[];

char    *dirdata;
char    *defprinter;

dirdata=(char *) getenv("MID_FILTERS");
if (dirdata==NULL)
   fprintf(stderr,"ERROR: cannot find MID_FILTERS environment variable\r\n");

/* set printer */

defprinter=(char *) getenv("PRINTER");
if (defprinter==NULL) {
   fprintf(stderr,"WARNING: PRINTER variable undefined. It will be defaulted to \"lp\".\r\n");
	strcpy(Printer,"lp");
}
else
	strcpy(Printer,defprinter);

/* set global variables */

	M.outfile[0]='\0';
        M.nmax=200;
	M.imagnitude=0;
	M.magnitude=0.;

/* exposure time - SN */
	T.itimeSN=1;
	T.etime=1.;
	T.SN=10.;

/* atmosphere */
	T.iatmos=1;
	sprintf(T.atmosfile,"%s/atmos/extatmos.dat",dirdata);
	T.airmass=1.;
	T.isky=1;
	T.darkness=0;
	T.emlines=1;
        strcpy(T.skyfile,"sky_b_em.dat");

/* source */
	T.ispectra=0;
        T.spectrafile[0]='\0';
        T.spectrapath[0]='\0';


/* set uimx window variables */

G.basic_set=1;
 UxPutSet(UxFindSwidget("tg_basic"),"true");
 UxPutSet(UxFindSwidget("tg_all"),"false");


}
