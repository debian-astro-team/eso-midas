/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826		last modif
===========================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <globaldef.h>
#include <model.h>


extern void endname(), end_graphic1(), plot_xy(), c_minmax(); 

extern int read_curve ();








void	load_curve(name)
char	name[];

{
extern	float	Oxmi,Oxma,Oymi,Oyma;

float	x[6000],y[6000];
int	n;
float	xmin,xmax;
float	ymin,ymax;
char	term[10],str[200];

endname(name,term);

if (strcmp(term,"dat"))
	strcat(name,".dat");

read_curve(name,x,y,&n,str);
c_minmax(x,y,n,&xmin,&xmax,&ymin,&ymax);
plot_xy(str,0,x,y,n,xmin,xmax,ymin,ymax);
end_graphic1();

   Oxmi=xmin;
   Oxma=xmax;
   Oymi=ymin;
   Oyma=ymax;
}
