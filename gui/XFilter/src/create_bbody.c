/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090826         last modif

===========================================================================*/


#include <math.h>
#include <stdio.h>
#include <string.h>
#include <UxLib.h>
#include <globaldef.h>
#include <model.h>

extern void  c_minmax(), plot_xy(), end_graphic1();



/* (R_solaire/ pc )^2 = (6.9598e5 / 3.0856e13)^2=5.087615e-16 (unitless)*/
#define	rs2_pc2	5.087615e-16
/* hplanck * c^2 = 6.625e-27 (erg.sec) * 2.997925e10 (cm/s)^2
	= 5.954254e-06 erg.cm2/sec */
/* hc2 /(nm)^5 = 5.954254e37 1e-16erg/cm2/A/s */
#define	hc2	5.954254e37
/* hc2 /(nm)^5 * (R_solaire/ pc )^2 = 3.029295e+22 1e-16erg/cm2/A/s */
/* hc2 /(nm)^5 * (R_solaire/ pc )^2/ 100^5 = 3.029295e+12 1e-16erg/cm2/A/s */
#define hc2rs2_pc2	3.029295e+12
/* hplanck * c / kboltzman = 6.625e-27 (erg.sec) * 2.997925e10 (cm/s) / 
	1.380622e-16 erg/K = 1.4385728 cm/K =  1.4385728e7 nm.K */
#define	hc_k	1.4385728e7	

void	create_bbody(fluxfile)
char	fluxfile[];

{
extern	struct s_bbody	N;
extern	struct s_trans	T;
extern	float	Xspec[],Yspec[];
extern	int	Nspec;
extern  float        Oxmi,Oxma,Oymi,Oyma;

FILE	*fp;
register int	i;
float	pas,x,y;
char    str[150], str1[40];
float	xmin,xmax,ymin,ymax;

strcat(fluxfile,".flx");
UxPutText(UxFindSwidget("tf_spectrum"),fluxfile);

T.ispectra=2;
strcpy((char *)T.spectrafile,fluxfile);

sprintf(str1,"\1 Flux of the Black Body");
sprintf(str,
  "TITLE=%20s;DTITL=%4.1f;LABX=%12s;LABY=%12s;DLABL=%4.1f;NGEOM;USER;BOLD;GRID;TIME;XSPACE=%1d;",
     str1, 2.0, "\1Wave [nm]", "\1 Flux [1e-16 erg/cm2/A/s]",1.0,0);

Nspec=500;
pas=(N.wrmax-N.wrmin)/(Nspec-1);

fp=fopen(fluxfile,"w");
for(i=0;i<Nspec;i++)
	{
	Xspec[i]=x=N.wrmin+i*pas;
	y=x/100.;
	Yspec[i]=hc2rs2_pc2/pow(y,5.)/(exp(hc_k/x/N.T)-1.)*
		(N.radius*N.radius/N.dist/N.dist);
	fprintf(fp,"%f  %e\n",Xspec[i],Yspec[i]);
	};
fclose(fp);

c_minmax(Xspec,Yspec,Nspec,&xmin,&xmax,&ymin,&ymax);

T.specmin=xmin;
T.specmax=xmax;
plot_xy(str,0,Xspec,Yspec,Nspec,xmin,xmax,ymin,ymax);
end_graphic1();
Oxmi=xmin;
Oxma=xmax;
Oymi=ymin;
Oyma=ymax;
}
