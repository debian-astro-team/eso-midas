#include "globaldef.h"

char	Ask[80];

char	Printer[80];	/* Laser printer to send plots */
char	currsel[100];  		/* output data file */

int	Nrows, Ncols;


/* Filter Characteristic definitions */

int	Nfilt[FMAX];
char	*Charac[FMAX];
char	*Comment[FMAX];
float	Rcwavl[FMAX];
float	Rbwidth[FMAX];
float	Rtran[FMAX];
float	Opthi[FMAX];
float	Sizc[FMAX];
float	Thic[FMAX];

/* limit variables */

float   cw1,cw2,cw1p,cw2p;        /* cwavwlength to plot */
float	bw1,bw2;

/* multiplot */

int itab,tab4multi[4],tab9multi[9];

/* overplot variables */

int	nc_ovplt;

/* logical variables */

int      pltgr=FALSE,plt4gr=FALSE,ovplt4gr=FALSE,ovpltgr=FALSE;
int      lcwav=FALSE,lcwbw=FALSE,putplt=FALSE, putplt9=FALSE;
int      lfirstp=FALSE;
int      ccd_flag=FALSE,filter_flag=FALSE;

int	tracemode=FALSE; /* FALSE = Straight line */

/* transmision variables */
int     Nccd;
float   Xccd[MAXCCDAT];
float   Yccd[MAXCCDAT];

int     Nfilter;
float   Xfilt[FILTMAX];
float   Yfilt[FILTMAX];

int     Nsky;
float   Xsky[SPECMAX];
float   Ysky[SPECMAX];

int     Nspec;
float   Xspec[SPECMAX];
float   Yspec[SPECMAX];

int     Ngrism;
float   Xgrism[1000];
float   Ygrism[1000];

int     Natmos;
float   Xatmos[100];
float   Yatmos[100];

int     Noptics;
float   Xoptics[1000];
float   Yoptics[1000];

int     Nmir;
float   Xmir[100];
float   Ymir[100];
