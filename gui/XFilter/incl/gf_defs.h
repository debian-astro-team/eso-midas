/*  gf_defs.h:
 *
 *    Contain extern declarations of variables and functions,
 *    #includes, #defines and typedefs related to graphical interface.
 *
 */


#include <x_defs.h>

#define VERSION	"2.0"
#define Offset(field) (XtOffset(XxfilterResources *, field))
/* filter applic */
#define FMAX 800

/* popup actions */
#define POPUP_PLTTBL	0
#define POPUP_CWAV      1
#define POPUP_NESO      2
#define POPUP_CWBW      3
#define POPUP_PRINT     4
#define POPUP_EXTR      5



typedef struct {
   Boolean   bell;		/* if True, bell on */
   Boolean   bigicon;          /* option -bigicon */
   Boolean   debug;            /* option -debug */

} XxfilterResources;

/* SetHelp */
XtActionProc SetHelp();

/* ginter.c */
extern void 		CreateSubWindows();	/* all subwindows */

/* extern variables */

extern Display		*display;
extern XtAppContext 	app_context;
extern Widget		toplevel, readme, messageWindow, commandWindow;
			/* formWindow; */

extern Cursor		watch;			/* XC_watch cursor */
extern XxfilterResources  app_resources;		/* application resources */
extern Boolean		debug;
