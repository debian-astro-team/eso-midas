/*
.VERSION
 090826		last modif
*/

#ifndef MODELDEF_H
#define MODELDEF_H

struct s_gui {
		char	sel_type[20];
		char	sel_inst[20];
                int     basic_set;
		};

struct	s_mod {
		char	outfile[80];
                int	nmax;
                char    filtre[1];
		float   mzero;
                int     imagnitude;
                float   magnitude;
		};

struct	s_trans{
		char	transfile[80];
		char	curvefile[80];
/* integration */
		int	itimeSN;
		float	etime;
		float	SN;
/* spectrum */
		int     ispectra;
                char	spectrafile[80];
                char	spectrapath[80];
		float	specmin;
		float	specmax;
/* sky*/
		int	isky;
		int	darkness;
		int	emlines;
		char	skyfile[80];
		float	skymin;
		float	skymax;
/* atmosphere */
		int	iatmos;
                char	atmosfile[80];
                float	airmass;
		float	atmosmin;
		float	atmosmax;
/* instrument */
	/* mirror */
		int	imirror;
                char	mirrorfile[80];
                char	mirnewfile[80];
                char	miroldfile[80];
		float	mirmin;
		float	mirmax;
                float	surface;
	/* optics */
		int	ioptics;
                char	opticsfile[80];
		float	optmin;
		float	optmax;
	/* dispersor */
		int	dsensitive;
		int	igrism;
		int	ngrism;
                char	grismfile[80];
                char    gsmfl[15][80];
                float   resolution;
                float   res[15];
		float	grismmin;
		float	grismmax;
	/* filter */
		int	ifilter;
		int	nfilter;
                int	filterfile[80];
                char	basicfilt[20][80];
                int     nbasic;
		float	filtmin;
		float	filtmax;
	/* ccd */
		int	iccd;
		int	nccd;
                char	ccdfile[80];
		float	ccdmin;
		float	ccdmax;
                float   pixel;
                float   einadu;
                float   bias;
                float   ron;
		};

struct	s_bruzual{
		long	imf;
		long	imfmass;
                long    istpop;
                char    sspfile[80];
                char    cspfile[80];
                long    ised;
                char    sedfile[80];
                long	isfr;
                float   etau;
                float   emu;
		float	dburst;
		float	crate;
                long	grecycle;
                float	minwave;
		float   maxwave;
		float   massgal;
		float   agegal;
		long	idist;
                float	redshift;
                float	dist;
                float	zf;
                float	H0;
                float	omega;
                long    ioutput;
                char    outfile[80];
		};

struct	s_cloudy{
		char	continuum[25];
		float   zbg;
		float	scalebg;
		float	tempbb;
		float	tempbrem;
		float	spectral;
		float	tempstar;
		float	temppn;
		float	agnbreak;
		float	scaleism;
		char	intensity[25];
		float	luminosity;
		float	flux;
		float	mag;
		float	tempeden;
		float	number;
		float	extinction;
		char	chemic[25];
		int	imetal;
		float	metal;
		float	metalgrain;
		char	denslaw[25];
		float	hden;
		int	geometry;
		float	cover;
		float	fill;
		int	unitsmin;
		float	rmin;
		int	unitsmax;
		float	rmax;
		int	sphere;
		int	sphereop;
		float	wmin;
		float	wmax;
		};

struct	s_bbody{
                float   T;
                float   dist;
		float	radius;
		float	wrmin;
                float	wrmax;
                char    outfile[80];
		};

struct  s_filter{
                float   cw;
                float   bw;
                float   peak;
                float   cwl;
                float   bwl;
                float   peakl;
                long    ifile;
                char    filterfile[80];
                };

typedef	struct	{
		char	alldata[250];
		int	eson;
		char	name[15];
		char	design[3];
		char	date[15];
		char	cosmetic[7];
		char	imaqual[5];
		char	cwl_lswp[10];
		char	fwhm_wt50[10];
		float	pwl;
		float	taupk;
		char	shape[2];
		float	size;
		float	thickness;
		float	opthick;
		char	filttype[20];
		char	instrument[20];
		char	codeachat[20];
		char	info[20];
		int	redleak;
		int	nleakpeak;
		char	leakpeak[50];
		char	leakred[50];
		char	leaks[50];
		float	cwleak[10];
		float	pkleak[10];
		}
		FILTER;

#endif
