/* ccd */
#define CCDMAX 37
#define MAXCCDAT      70

/* filter */
#define FMAX 800
#define FILTMAX 6000

/* spectra */
#define SPECMAX 6000

#define MAXLIST 2000

#define MAXCW 20
#define MAXNF 500
#define MAXOVPLT 30

/* logical */

#ifndef TRUE
#define FALSE 0
#define TRUE (!FALSE)
#endif

#define Min(A,B)        ((A)<(B)?(A):(B))
#define Max(A,B)        ((A)>(B)?(A):(B))
#define Swap(A,B)       T=A;A=B;B=T; 
