/* @(#)gl_defs.h	19.1 (ESO-IPG) 02/25/03 13:45:25 */
/* @(#)gl_defs.h	8.3 (ESO-IPG) 10/25/94 17:25:23 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        gl_defs.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      global definitions for Midas related applications.
.KEYWORDS     global definitions
.COMMENTS     
.VERSION 1.0  1-Mar-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

#define MAXLINE		128
#define END_FILE	-1
#define OSAERR		-1	/* error status returned by osa routines */
#define SUCCESS		0	/* exit status returned by osa routines */

#define MAXDATA		16384	/* max number of elements in a column */
#define MAXLABEL	32	/* max length of a column label */
#define MAXCOLS		512 	/* max. no of cols in a table */
#define MAXNAME		128	/* max. length of a filename */
#define DEFAULT_CHAR	'?'	/* character used for default parameter */

#define MAX_IDENT	72  	/* as defined in Appendix C of env. Manual */

#define OUTSIDE_MIDAS	"-1"
#define MAXVALS		60      /* max. length of default parameters */
#define MAXUNIT		16	/* max. length of a unit string */

#define ALL_DESC	1       /* used in SCDCOP() */

#ifndef TRUE
#define FALSE		0
#define TRUE		(! FALSE)
#endif

/* Macros */

#define SWAP(a, b)      {int swap_tmp; \
                         swap_tmp = a; a = b; b = swap_tmp;}

#define NINT(x)              ((x) + ( ((x) < 0.0) ? -0.5: 0.5))

