#  @(#)help.h	19.1 (ESO-IPG) 02/25/03 13:45:25 
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++
#.COPYRIGHT   (C) 1993 European Southern Observatory
#.IDENT       long.hlp
#.AUTHORS     Pascal Ballester (ESO/Garching)
#             Cristian Levin   (ESO/La Silla)
#.KEYWORDS    Spectroscopy, Long-Slit
#.PURPOSE
#             This is the help file for the extended help
#             feature of XLong.
#
#.VERSION     1.0  Package Creation  17-MAR-1993
#-------------------------------------------------------
#

~HELP_CONTEXT
This is the GUI XEchelle for reduction of Echelle spectra. The 
context requires auxiliary calibration tables. Auxiliary data include 
catalogues with comparison lines and tables with the fluxes of some 
standard stars. 

The area  MID_ARC contains the following calibration catalogues,
thar.tbl, Thorium-Argon catalogue (for  CASPEC, ECHELLEC, EMMI).
hear.tbl, Helium-Argon  catalogue (for  EFOSC).
as well as Th-Ar tables selected for blends at spectral resolutions 
25000, 33000, 50000, 100000, respectively named thar25.tbl, thar50.tbl,
thar50.tbl, thar100.tbl (these tables have been prepared by H.Hensberge,
M.David and W.Verschueren from Antwerp University). 

Standard stars for flux calibration are available in the area 
 MID_STANDARD. Additional tables of southern spectrophotometric
standards are available in /midas/calib/data/spec/ctio (Hamuy & al., 
1992, PASP, 104 533-552 and Hamuy & al., 1994, PASP 106 
566-589) and midas/calib/data/datatrans/hststd (HST Standards).

These tables are distributed
on request in complement to the Midas releases. These tables are
also available on anonymous ftp at the host ftphost.hq.eso.org
(IP number 134.171.40.2). The files to be retrieved are located
in the directory /midaspub/calib and are named README.calib and 
calib.tar.Z. Command  SHOW/TABLE can be used to visualize the
column name and physical units of the tables. Demonstration 
data required to execute the tutorial
procedure  TUTORIAL/ECHELLE are also located on this ftp
server in the directory /midaspub/demo as echelle.tar.Z. FTP access
is also provided on the World Wide Web URL:

 http://http.hq.eso.org/midas-info/midas.html 

The calibration directory contains other information 
such as characteristic curves for ESO filters and CCD detectors, which 
can be visualized with the Graphical User Interface XFilter 
(command  CREATE/GUI FILTER).
in the directory /midaspub/demo as echelle.tar.Z. 
~HELP_HELP
You can get an extended help in each panel of the interface by pressing 
the HELP button. 

~HELP_TUTORIAL
A tutorial demonstrating a standard echelle reduction session
is provided by the command TUTORIAL/ECHELLE. The tutorial requires
demonstration tables, provided on the ftp server:

ftphost.hq.eso.org (IP number 134.171.40.2). 

in the directory /midaspub/demo as echelle.tar.Z. 

~HELP_VERSION
This is the 95NOV version of the context Echelle. This context
supports world coordinate calibrations. The 94NOV version of the 
can be accessed with SET/CONTEXT echellec command (however this GUI
supports only 95NOV version).

~MAIN_SEARCH
Open the search lines interface.

~MAIN_IDENT
Open the interface that allows identification of comparison lines.  
The line catalogue is listed in the main window. More information is 
given with the individual buttons in the interface.

PARAMETERS: the following parameters are required by interface:

a) Line catalogue   -- name of the file containing the wavelength
                               of the comparison lines.

b) Wavelength range -- select the range of wavelengths from the
                               line table to be used in the calibration.


c) Strength limit   -- select the minimum intensity of the comparison
                               lines to be used in the calibration.

d) Starting line    -- row number where the lines will be identified.

~MAIN_CALIB
Open the wavelength calibration interface.

~MAIN_REBIN
Open the rebinning interface.

~MAIN_EXTRACT
Open the spectrum extraction interface. It contains commands for fitting 
the sky and extracting the spectra of unresolved sources using an 
optimization algorithm.

~MAIN_FLUX
Open the flux calibration interface.

~MAIN_BATCH
Open the batch reduction form.

~SEARCH_SEARCH
Automatic search of 2-D spectra for the positions of the spectral lines.
The interface offers three algorithms for determining the centroids:

gravity   -- Find the center of gravity of the lines.

gaussian  -- Fit a gaussian to the line profiles.

maximum   -- Use the pixel with the maximum intensity.

If you click the left buitton of the mouse, the list of .bdf files appears.
Click the file you want to search. The identifier of the file is displayed
in the main window.  

PARAMETERS: the following parameters are required by search:

a) Threshold              -- to define the level above which lines are
                             identified
b) Width of search window -- to search and center the lines.

c) Smooth Factor          -- search averaging N rows above and N rows below 
                             every line (except at borders). Thus a smoothing 
                             of 2xN+1 rows is applied.
d) Binning step           -- 

~SEARCH_PLOT
Plot the found lines

~CALIB_CALIBRATE

Determine the dispersion relation for the starting row using the
identified lines (mode IDENT) or a previous session (mode GUESS) as 
initial guesses.  

The program iterates until all lines within the given tolerance are 
identified.  The number of lines found and the final rms of the fit are 
given.

The iterative identification of lines is controlled by the matching 
parameter (alpha). For each element of the table line a computed wavelength
is estimated. The closest wavelength in the line catalog will be the
identification candidate. The absolute difference between the computed
wavelength and the candidate wavelength is the residual. The residual
divided by alpha is the acceptation distance. The distance
of four neighbours is taken into account (left and right in the line table
and in the line catalog). The candidate is retained if the distance of 
the closest of the four neighbours is larger than the the acceptation 
distance. The optimal range for the matching parameter alpha is between
0. and 0.5, which secures that the candidates will be retained only if the
neighbours are at least twice further (alpha=0.5) than the residual.

The maximal deviation is expressed in pixels. The iterative process
aborts if the fit rms in pixels after line identification is larger than 
the maximal deviation. A typical value for this parameter is 10 pixels.

~CALIB_EDIT
Add or subtract lines from the fit.  Use the "Dispersion" or "Spectrum" 
button to display the fit.  Click "edit".  Position the graphics cursor 
on top of the line you want to edit (eliminate or reset. In colour displays, 
the line symbol will turn red when eliminated and green or blue again when 
reset).  The button "Calibrate" should be used to update the fit after 
editing points.

~CALIB_GETCUR
Read the wavelength of any feature with the graphics cursor.  The values
of the pixel position and the corresponding wavelength are displayed in
the MIDAS window.  This command is designed to be used with the
"Spectrum" command.

~CALIB_ALL
It propagates the polynomial fits from the initial line to the whole 
spectrum row-by-row. 

~CALIB_TWICE
The row-by-row calibration is done in two passes in such a way that the 
same set of lines is used in all rows. Thus, this option uses only the 
lines that have been found in all the rows of the spectrum.

~CALIB_DISPER
Plot delta lambda vs. lambda for all lines found.

~CALIB_RESID
Plot the fit residuals vs. lambda.

~CALIB_SPEC
Plot the spectrum labeling the lines with their table wavelengths.

~CALIB_SHAPE
Plot the distortion of the spectrum from a specific wavelength value.

~REBIN_RBR
Rebin the spectrum to wavelength. Calibration must be done before to
determine the polynomial coefficients.   This pulldown menu offers
three options:

LINEAR    -- Rebins using linear interpolation.  Flux is preserved.

QUADRATIC -- Rebins using quadratic interpolation.  Flux is preserved.

SPLINE    -- Rebins using spline interpolation.  Flux is not preserved.

After clicking the desired method  LONG prompts for the file to
be rebinned and the name of the output file.

~REBIN_2D

~REBIN_TABLE

~REBIN_PLOT

~EXTRACT_SKY
 Asks the user for the sky windows using the cursor over the display
 window. The coordinates are then displayed in the input form.

~EXTRACT_OBJECT
 Asks the user for the object limits using the cursor over the display
 window. The coordinates are then displayed in the input form.

~EXTRACT_FIT
 Fit a polynomial to the data in two windows along the Y-axis.
 Two modes can be selected:

Same spatial profile -- Here it is assumed that the normalized spatial
                        profile is the same for all columns and only one
                        polynomial is fitted to the mean spatial profile.

Independent profile  -- In this case an independent polynomial is fitted
                        for every row of the spectrum, filtering also the
                        cosmic rays.

PARAMETERS: The following parameters must be set in the input form:

a) Sky limits (pixels)   -- starting and ending Y coordinates of the sky
                            windows

b) Polynomial fit degree -- degree of the fit to be used.

c) Radius                -- used for cosmics rays rejection.

~EXTRACT_AVERAGE
The standard method. The sky is taken from the sky limits values of 
the input window. The result is summed or averaged depending on the
extraction method parameter.

~EXTRACT_WEIGHT
 Extract spectrum from CCD frame. The rows of the spectrum are
 added with weights which are chosen for optimal S/N-ratio of
 the resulting spectrum. Cosmic ray hits are removed by
 analyzing the profile perpendicular to the dispersion (assumed
 to be along the X-axis). The method is only suited for
 spectra of (spatially) unresolved sources. The method is very
 similar to the one described by Horne (1986, PASP 98, 609).

PARAMETERS:  The extraction algorithm uses the following parameters:

a) Object limits (pixels)    -- Approximate Y coordinates of the object
                                (starting and ending).

b) Extraction iterations     -- Number of iterations in the extraction
                                process.

c) Read-out-noise (e-)       -- Read out noise of the CCD in electrons.

d) Inverse gain factor       -- Conversion factor from ADU to electrons.
                                (e-/ADU)

e) Threshold for cosmic rays -- Threshold used for the rejection of
                                cosmic ray hits.

~FLUX_EXTIN
Determine the extinction curve by interpolating the standard extinction
tables for La Silla.  This table must be in the working directory and
can be copied from the MIDAS database using a button in the "utils"
pulldown menu. The airmass must be given.  If present, the airmass in
the file header is used as default.  In the present version, the airmass
correction can only be applied to 1-D spectra.

~FLUX_INTEGR
Bin the standard star spectrum to the bands of the calibration tables
and produce the calibration curve.

~FLUX_FIT
This command allows to fit the calibration curve depending on the 
fitting selected:

polynomial  -- the fit degree of the input form is used.

spline      -- Standard spline fits.  The parameters of the spline must
               be given in the input form (smooth factor, degree).

~EDIT_FLUX
This command allows to edit the points generated by the integration process.

~FLUX_CORRECT
Calibrate the input image in flux, using the response curve generated.

~FLUX_FILTER

~FLUX_PFLUX
Plot the flux table.

~FLUX_PRESP
Plot the response curve.

