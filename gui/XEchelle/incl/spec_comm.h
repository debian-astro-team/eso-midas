/* @(#)spec_comm.h	19.1 (ESO-IPG) 02/25/03 13:45:25 */
/* @(#)spec_comm.h	19.1 (ESO-IPG) 02/25/03 13:45:25 */
/*
 * ECHEL context commands
 */
#define C_INIT 		"init/Echel "
#define C_SAVE 		"save/Echel "
#define C_CLEAN 	"clean/Echel "
#define C_SEARCH 	"search/Echel "
#define C_PLOT_SEARCH 	"plot/search "
#define C_IDENT 	"xident/Echel "

#define C_SELINE 	"@s lnident,seline "
#define C_SELLIN_ALL 	"sel/tab 'lintab' all"
#define C_SELCAT_ALL 	"sel/tab 'lincat' all"
#define C_NODEL 	"@s lnident,nodel "
#define C_CALIB_RBR 	"calib/Echel "
#define C_CALIB_RBR2 	"calib/twice "
#define C_CALIB_EDIT_D 	"plot/delta edit "
#define C_CALIB_EDIT_C 	"plot/calib edit "
#define C_CALIB_ERAS 	"erase/Echel "
#define C_CALIB_CURS 	"plot/calib cursor "
#define C_PLOT_DELTA 	"plot/delta "
#define C_PLOT_CALIB 	"plot/calib "
#define C_PLOT_IDENT 	"plot/ident "
#define C_PLOT_DISTOR 	"plot/distort "
#define C_PLOT_RESID 	"plot/residual "

#define C_REBIN 	"rebin/Echel "
#define C_APP_DISP 	"apply/disp "
#define C_RECTIFY 	"rectify/Echel "
#define C_PLOT_SPEC 	"plot/spec "

#define C_EXTR_GCOOR 	"gcoord/Echel "
#define C_EXTR_FIT_SKY	"skyfit/Echel "
#define C_EXTR_AVERAGE 	"extract/average "
#define C_EXTR_WEIGHTED "extract/Echel "

#define C_EXTIN 	"extin/Echel "
#define C_INTEGR 	"integr/Echel "
#define C_PLOT_RESP 	"plot/response "
#define C_FLUX_FIT 	"response/Echel "
#define C_CORRECT 	"calib/flux "
#define C_FLUX_FILT 	"response/filter "
#define C_PLOT_FLUX 	"plot/flux "
#define C_EDIT_FLUX 	"edit/flux "

#define C_GRAPHICS	"graph/Echel "
#define C_DISPLAY	"make/display "
#define C_LOAD_IMA	"load/Echel "

#define C_BATCHRED	"batch/Echel "
#define C_REDUCE	"reduce/Echel "
#define C_REDINIT	"reduce/init "
#define C_REDSAVE	"reduce/save "

/*
 * Commands for updating the ECHEL keywords
 */
#define K_INSTRUME	"set/Echel INSTRUME = "
#define K_WLC		"set/Echel WLC = "
#define K_WIDTH		"set/Echel WIDTH = "
#define K_THRES		"set/Echel THRES = "
#define K_SEAMTD	"set/Echel SEAMTD = "
#define K_YSTART	"set/Echel YSTART = "
#define K_YWIDTH	"set/Echel YWIDTH = "
#define K_YSTEP		"set/Echel YSTEP = "
#define K_WLCMTD	"set/Echel WLCMTD = "
#define K_TOL		"set/Echel TOL = "
#define K_FITD		"set/Echel FITD = "
#define K_DCX1		"set/Echel DCX = "
#define K_DCX2		"set/Echel DCX = ,"
#define K_LINTAB	"set/Echel LINTAB = "
#define K_COERBR	"set/Echel COERBR = "
#define K_SESSION	"set/Echel SESSION = "
#define K_LINCAT	"set/Echel LINCAT = "
#define K_WRANG1	"set/Echel WRANG = "
#define K_WRANG2	"set/Echel WRANG = ,"
#define K_IMIN		"set/Echel IMIN = "
#define K_ALPHA		"set/Echel ALPHA = "
#define K_WLCNITER1	"set/Echel WLCNITER = "
#define K_WLCNITER2	"set/Echel WLCNITER = ,"
#define K_MAXDEV	"set/Echel MAXDEV = "
#define K_GUESS		"set/Echel GUESS = "
#define K_COROPT	"set/Echel COROPT = "
#define K_CORVISU	"set/Echel CORVISU = "
#define K_SHIFT		"set/Echel SHIFT = "
#define K_WCENTER	"set/Echel WCENTER = "
#define K_AVDISP	"set/Echel AVDISP = "
#define K_TWODOPT	"set/Echel TWODOPT = "

#define K_REBMTD	"set/Echel REBMTD = "
#define K_REBSTRT	"set/Echel REBSTRT = "
#define K_REBEND	"set/Echel REBEND = "
#define K_REBSTP	"set/Echel REBSTP = "

#define K_EXTMTD	"set/Echel EXTMTD = "
#define K_OBJECT1	"set/Echel OBJECT = "
#define K_OBJECT2	"set/Echel OBJECT = ,"
#define K_LOWSKY1	"set/Echel LOWSKY = "
#define K_LOWSKY2	"set/Echel LOWSKY = ,"
#define K_UPPSKY1	"set/Echel UPPSKY = "
#define K_UPPSKY2	"set/Echel UPPSKY = ,"

#define K_RON		"set/Echel RON = "
#define K_GAIN		"set/Echel GAIN = "
#define K_SIGMA		"set/Echel SIGMA = "
#define K_RADIUS	"set/Echel RADIUS = "
#define K_SKYORD	"set/Echel SKYORD = "
#define K_SKYMOD	"set/Echel SKYMOD = "
#define K_ORDER		"set/Echel ORDER = "
#define K_NITER		"set/Echel NITER = "

#define K_COMET		"set/Echel COMET = "
#define K_FFIT		"set/Echel FFIT = "
#define K_FDEG		"set/Echel FDEG = "
#define K_FVISU		"set/Echel FVISU = "

#define K_RESPONSE	"set/Echel RESPONSE = "
#define K_RESPTAB	"set/Echel RESPTAB = "
#define K_EXTAB		"set/Echel EXTAB = "
#define K_STD		"set/Echel STD = "
#define K_FLUXTAB	"set/Echel FLUXTAB = "
#define K_FILTMED	"set/Echel FILTMED = "
#define K_FILTSMO	"set/Echel FILTSMO = "
#define K_FITD		"set/Echel FITD = "
#define K_PLOTYP	"set/Echel PLOTYP = "
#define K_FITYP		"set/Echel FITYP = "
#define K_RESPLOT	"set/Echel RESPLOT = "
#define K_SMOOTH	"set/Echel SMOOTH = "

#define K_NPIX		"set/Echel NPIX = "
#define K_START		"set/Echel START = "
#define K_STEP		"set/Echel STEP = "

#define LIST_WLC		0
#define LIST_REBIN_RBR		1
#define LIST_REBIN_2D		2
#define LIST_REBIN_TBL		3
#define LIST_EXTIN		4
#define LIST_INTEGR		5
#define LIST_FLUX_FILT		6
#define LIST_FIT_SKY		7
#define LIST_EXT_AVER		8
#define LIST_EXT_WEIGHT		9
#define LIST_LINCAT		10
#define LIST_GUESS		11
#define LIST_FLUX_TBL		12
#define LIST_EXTIN_TBL		13
#define LIST_SESSION		14
#define LIST_LOAD_IMA		15
#define LIST_BROWSER		16
#define LIST_CORRECT		17

#define DIALOG_REBIN_RBR	0
#define DIALOG_REBIN_2D		1
#define DIALOG_REBIN_TBL	2
#define DIALOG_SESSION		3
#define DIALOG_EXTIN		4
#define DIALOG_EXT_AVER		5
#define DIALOG_EXT_WEIGHT	6
#define DIALOG_CORRECT		7

/* Reduction form definitions */
#define K_INPUTF	"set/Echel INPUTF = "
#define K_INPNUMB	"set/Echel INPNUMB = "
#define K_OUTPUTF	"set/Echel OUTPUTF = "
#define K_OUTNUMB	"set/Echel OUTNUMB = "
#define K_ROTOPT	"set/Echel ROTOPT = "
#define K_ROTSTART	"set/Echel ROTSTART = "
#define K_ROTSTEP	"set/Echel ROTSTEP = "
#define K_TRIMOPT	"set/Echel TRIMOPT = "
#define K_TRIM1		"set/Echel TRIM = "
#define K_TRIM2		"set/Echel TRIM = ,"
#define K_TRIM3		"set/Echel TRIM = ,,"
#define K_TRIM4		"set/Echel TRIM = ,,,"
#define K_REBOPT	"set/Echel REBOPT = "
#define K_ECHELSESS	"set/Echel SESSION = "
#define K_REDRBMTD	"set/Echel REBMTD = "
#define K_EXTOPT	"set/Echel EXTOPT = "
#define K_REDEXTAB	"set/Echel EXTAB = "
#define K_RESPOPT	"set/Echel RESPOPT = "
#define K_REDRESP	"set/Echel RESPONSE = "
#define K_BIASOPT	"set/Echel BIASOPT = "
#define K_DARKOPT	"set/Echel DARKOPT = "
#define K_FLATOPT	"set/Echel FLATOPT = "
#define K_DARK		"set/Echel DARK = "
#define K_FLAT		"set/Echel FLAT = "
#define K_BIAS		"set/Echel BIAS = "

#define LIST_REDSESS	0
#define LIST_INPUTF	1
#define LIST_BIAS	2
#define LIST_DARK	3
#define LIST_FLAT	4
#define LIST_LONGSESS	5
#define LIST_REDEXTAB	6
#define LIST_REDRESP	7

#define DIALOG_REDSESS	0
