/* @(#)long_help.h	19.1 (ESO-IPG) 02/25/03 13:45:25 */
/* @(#)long_help.h	8.1 (ESO-IPG) 8/31/94 16:23:30 */
/* @(#)sp_help.h	4.1.1.1 (ESO-La Silla) 7/30/92 19:23:20 */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.IDENT        sp_help.h
.MODULE       header
.LANGUAGE     C
.AUTHOR       Cristian Levin - ESO La Silla
.PURPOSE      definitions for the help elements of XLong.
.KEYWORDS     interactive help
.COMMENTS     
.VERSION 1.0  1-Mar-1991   Implementation
.ENVIRONMENT  UNIX
------------------------------------------------------------*/

#define HELP_UTILS		"utils"
#define HELP_RETURN		"return"
#define HELP_FRAME		"frame"

#define HELP_MAIN_SEARCH 	"main-search"
#define HELP_MAIN_IDENT 	"main-ident"
#define HELP_MAIN_CALIBR 	"main-calibr"
#define HELP_MAIN_REBIN 	"main-rebin"
#define HELP_MAIN_EXTRACT 	"main-extract"
#define HELP_MAIN_FLUX 		"main-flux"
#define HELP_MAIN_INIT 		"main-init"
#define HELP_MAIN_SAVE 		"main-save"
#define HELP_MAIN_QUIT 		"main-quit"

#define HELP_IDENT_BEGIN 	"ident-begin"
#define HELP_IDENT_CONTINUE 	"ident-continue"
#define HELP_IDENT_ADD 		"ident-add"
#define HELP_IDENT_SEARCH 	"ident-search"

#define HELP_CALIB_BEGIN 	"calib-begin"
#define HELP_CALIB_PLOT 	"calib-plot"
#define HELP_CALIB_GETCUR 	"calib-getcur"
#define HELP_CALIB_EDIT 	"calib-edit"
#define HELP_CALIB_FIT 		"calib-fit"
#define HELP_CALIB_UPDATE 	"calib-update"
#define HELP_CALIB_CAL_ALL 	"calib-cal_all"

#define HELP_EXTRACT_GETSKY 	"extract-get_sky"
#define HELP_EXTRACT_GETOBJ 	"extract-get_obj"
#define HELP_EXTRACT_EXTSPEC 	"extract-ext_spec"
#define HELP_EXTRACT_FITSKY 	"extract-fit_sky"

#define HELP_FLUX_EXTINCT 	"flux-extinct"
#define HELP_FLUX_INTEGR 	"flux-integr"
#define HELP_FLUX_PLOT 		"flux-plot"
#define HELP_FLUX_RESPONSE 	"flux-response"
#define HELP_FLUX_FIT 		"flux-fit"
