! UIMX ascii 2.0 key: 3595                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  

*translation.table: FileSelectFlux
*translation.parent: FluxShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable34
*translation.parent: FluxShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*FluxShell.class: applicationShell
*FluxShell.parent: NO_PARENT
*FluxShell.static: true
*FluxShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*FluxShell.ispecdecl:
*FluxShell.funcdecl: swidget create_FluxShell()\

*FluxShell.funcname: create_FluxShell
*FluxShell.funcdef: "swidget", "<create_FluxShell>(%)"
*FluxShell.icode:
*FluxShell.fcode: return(rtrn);\

*FluxShell.auxdecl:
*FluxShell.name: FluxShell
*FluxShell.x: 294
*FluxShell.y: 83
*FluxShell.width: 490
*FluxShell.height: 427
*FluxShell.title: "Flux calibration"
*FluxShell.keyboardFocusPolicy: "pointer"
*FluxShell.geometry: "+10+60"
*FluxShell.background: WindowBackground

*form10.class: form
*form10.parent: FluxShell
*form10.static: true
*form10.name: form10
*form10.resizePolicy: "resize_none"
*form10.unitType: "pixels"
*form10.x: 0
*form10.y: 0
*form10.width: 408
*form10.height: 348
*form10.background: WindowBackground

*form11.class: form
*form11.parent: form10
*form11.static: true
*form11.name: form11
*form11.resizePolicy: "resize_none"
*form11.x: -2
*form11.y: 342
*form11.width: 490
*form11.height: 84
*form11.background: ButtonBackground

*pb_flux_extin.class: pushButton
*pb_flux_extin.parent: form11
*pb_flux_extin.static: true
*pb_flux_extin.name: pb_flux_extin
*pb_flux_extin.x: 8
*pb_flux_extin.y: 4
*pb_flux_extin.width: 86
*pb_flux_extin.height: 30
*pb_flux_extin.background: ButtonBackground
*pb_flux_extin.fontList: BoldTextFont
*pb_flux_extin.foreground: ApplyForeground
*pb_flux_extin.labelString: "Extinct"
*pb_flux_extin.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_EXTIN);\
}

*pushButton18.class: pushButton
*pushButton18.parent: form11
*pushButton18.static: true
*pushButton18.name: pushButton18
*pushButton18.x: 290
*pushButton18.y: 42
*pushButton18.width: 86
*pushButton18.height: 30
*pushButton18.background: ButtonBackground
*pushButton18.fontList: BoldTextFont
*pushButton18.foreground: CancelForeground
*pushButton18.labelString: "Cancel"
*pushButton18.activateCallback: {\
UxPopdownInterface(UxFindSwidget("FluxShell"));\
}

*pb_flux_presp.class: pushButton
*pb_flux_presp.parent: form11
*pb_flux_presp.static: true
*pb_flux_presp.name: pb_flux_presp
*pb_flux_presp.x: 196
*pb_flux_presp.y: 42
*pb_flux_presp.width: 86
*pb_flux_presp.height: 30
*pb_flux_presp.background: ButtonBackground
*pb_flux_presp.fontList: BoldTextFont
*pb_flux_presp.foreground: ButtonForeground
*pb_flux_presp.labelString: "Plot resp"
*pb_flux_presp.activateCallback: {\
#include <spec_comm.h>\
\
AppendDialogText(C_PLOT_RESP);\
\
}

*pb_flux_integr.class: pushButton
*pb_flux_integr.parent: form11
*pb_flux_integr.static: true
*pb_flux_integr.name: pb_flux_integr
*pb_flux_integr.x: 102
*pb_flux_integr.y: 4
*pb_flux_integr.width: 86
*pb_flux_integr.height: 30
*pb_flux_integr.background: ButtonBackground
*pb_flux_integr.fontList: BoldTextFont
*pb_flux_integr.foreground: ApplyForeground
*pb_flux_integr.labelString: "Integr"
*pb_flux_integr.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_INTEGR);\
}

*pb_flux_fit.class: pushButton
*pb_flux_fit.parent: form11
*pb_flux_fit.static: true
*pb_flux_fit.name: pb_flux_fit
*pb_flux_fit.x: 196
*pb_flux_fit.y: 4
*pb_flux_fit.width: 86
*pb_flux_fit.height: 30
*pb_flux_fit.background: ButtonBackground
*pb_flux_fit.fontList: BoldTextFont
*pb_flux_fit.foreground: ApplyForeground
*pb_flux_fit.labelString: "Fit"
*pb_flux_fit.activateCallback: {\
#include <spec_comm.h>\
\
AppendDialogText(C_FLUX_FIT);\
\
}

*pb_flux_pflux.class: pushButton
*pb_flux_pflux.parent: form11
*pb_flux_pflux.static: true
*pb_flux_pflux.name: pb_flux_pflux
*pb_flux_pflux.x: 102
*pb_flux_pflux.y: 42
*pb_flux_pflux.width: 86
*pb_flux_pflux.height: 30
*pb_flux_pflux.background: ButtonBackground
*pb_flux_pflux.fontList: BoldTextFont
*pb_flux_pflux.foreground: ButtonForeground
*pb_flux_pflux.labelString: "Plot flux"
*pb_flux_pflux.activateCallback: {\
#include <spec_comm.h>\
\
AppendDialogText(C_PLOT_FLUX);\
\
}

*pb_flux_filter.class: pushButton
*pb_flux_filter.parent: form11
*pb_flux_filter.static: true
*pb_flux_filter.name: pb_flux_filter
*pb_flux_filter.x: 384
*pb_flux_filter.y: 4
*pb_flux_filter.width: 86
*pb_flux_filter.height: 30
*pb_flux_filter.background: ButtonBackground
*pb_flux_filter.fontList: BoldTextFont
*pb_flux_filter.foreground: ApplyForeground
*pb_flux_filter.labelString: "Filter"
*pb_flux_filter.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_FLUX_FILT);\
}

*pb_correct.class: pushButton
*pb_correct.parent: form11
*pb_correct.static: true
*pb_correct.name: pb_correct
*pb_correct.x: 8
*pb_correct.y: 42
*pb_correct.width: 86
*pb_correct.height: 30
*pb_correct.background: ButtonBackground
*pb_correct.fontList: BoldTextFont
*pb_correct.foreground: ApplyForeground
*pb_correct.labelString: "Correct"
*pb_correct.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_CORRECT);\
\
}

*pb_edit_flux.class: pushButton
*pb_edit_flux.parent: form11
*pb_edit_flux.static: true
*pb_edit_flux.name: pb_edit_flux
*pb_edit_flux.x: 290
*pb_edit_flux.y: 4
*pb_edit_flux.width: 86
*pb_edit_flux.height: 30
*pb_edit_flux.background: ButtonBackground
*pb_edit_flux.fontList: BoldTextFont
*pb_edit_flux.foreground: ApplyForeground
*pb_edit_flux.labelString: "Edit"
*pb_edit_flux.activateCallback: {\
#include <spec_comm.h>\
\
AppendDialogText(C_EDIT_FLUX);\
\
}

*shelp_flux.class: text
*shelp_flux.parent: form10
*shelp_flux.static: true
*shelp_flux.name: shelp_flux
*shelp_flux.x: 0
*shelp_flux.y: 282
*shelp_flux.width: 484
*shelp_flux.height: 50
*shelp_flux.background: SHelpBackground
*shelp_flux.cursorPositionVisible: "false"
*shelp_flux.editable: "false"
*shelp_flux.fontList: TextFont

*separator8.class: separator
*separator8.parent: form10
*separator8.static: true
*separator8.name: separator8
*separator8.x: -2
*separator8.y: 272
*separator8.width: 492
*separator8.height: 10
*separator8.background: WindowBackground

*separator9.class: separator
*separator9.parent: form10
*separator9.static: true
*separator9.name: separator9
*separator9.x: -4
*separator9.y: 330
*separator9.width: 492
*separator9.height: 10
*separator9.background: WindowBackground

*label27.class: label
*label27.parent: form10
*label27.static: true
*label27.name: label27
*label27.x: 10
*label27.y: 12
*label27.width: 126
*label27.height: 30
*label27.background: LabelBackground
*label27.fontList: TextFont
*label27.labelString: "Extinction table :"
*label27.alignment: "alignment_beginning"
*label27.foreground: TextForeground

*tf_extin_tbl.class: textField
*tf_extin_tbl.parent: form10
*tf_extin_tbl.static: true
*tf_extin_tbl.name: tf_extin_tbl
*tf_extin_tbl.x: 148
*tf_extin_tbl.y: 10
*tf_extin_tbl.width: 222
*tf_extin_tbl.height: 34
*tf_extin_tbl.background: TextBackground
*tf_extin_tbl.fontList: TextFont
*tf_extin_tbl.highlightOnEnter: "true"
*tf_extin_tbl.foreground: TextForeground
*tf_extin_tbl.translations: FileSelectFlux
*tf_extin_tbl.losingFocusCallback: {\
\
}

*label29.class: label
*label29.parent: form10
*label29.static: true
*label29.name: label29
*label29.x: 10
*label29.y: 48
*label29.width: 122
*label29.height: 30
*label29.background: LabelBackground
*label29.fontList: TextFont
*label29.labelString: "Flux table :"
*label29.alignment: "alignment_beginning"
*label29.foreground: TextForeground

*tf_flux_tbl.class: textField
*tf_flux_tbl.parent: form10
*tf_flux_tbl.static: true
*tf_flux_tbl.name: tf_flux_tbl
*tf_flux_tbl.x: 148
*tf_flux_tbl.y: 48
*tf_flux_tbl.width: 221
*tf_flux_tbl.height: 34
*tf_flux_tbl.background: TextBackground
*tf_flux_tbl.fontList: TextFont
*tf_flux_tbl.highlightOnEnter: "true"
*tf_flux_tbl.foreground: TextForeground
*tf_flux_tbl.translations: FileSelectFlux
*tf_flux_tbl.losingFocusCallback: {\
\
}

*label30.class: label
*label30.parent: form10
*label30.static: true
*label30.name: label30
*label30.x: 10
*label30.y: 88
*label30.width: 126
*label30.height: 30
*label30.background: LabelBackground
*label30.fontList: TextFont
*label30.labelString: "Response curve :"
*label30.alignment: "alignment_beginning"
*label30.foreground: TextForeground

*tf_output_resp.class: textField
*tf_output_resp.parent: form10
*tf_output_resp.static: true
*tf_output_resp.name: tf_output_resp
*tf_output_resp.x: 148
*tf_output_resp.y: 88
*tf_output_resp.width: 221
*tf_output_resp.height: 34
*tf_output_resp.background: TextBackground
*tf_output_resp.fontList: TextFont
*tf_output_resp.highlightOnEnter: "true"
*tf_output_resp.foreground: TextForeground
*tf_output_resp.losingFocusCallback: {\
\
}

*rowColumn4.class: rowColumn
*rowColumn4.parent: form10
*rowColumn4.static: true
*rowColumn4.name: rowColumn4
*rowColumn4.x: 233
*rowColumn4.y: 191
*rowColumn4.width: 82
*rowColumn4.height: 67
*rowColumn4.radioBehavior: "true"
*rowColumn4.background: "grey80"
*rowColumn4.entryBorder: 0
*rowColumn4.labelString: ""
*rowColumn4.shadowThickness: 0
*rowColumn4.borderWidth: 0
*rowColumn4.entryAlignment: "alignment_beginning"
*rowColumn4.isAligned: "true"

*rb_fityp_poly.class: toggleButton
*rb_fityp_poly.parent: rowColumn4
*rb_fityp_poly.static: true
*rb_fityp_poly.name: rb_fityp_poly
*rb_fityp_poly.x: 3
*rb_fityp_poly.y: 2
*rb_fityp_poly.width: 76
*rb_fityp_poly.height: 20
*rb_fityp_poly.background: WindowBackground
*rb_fityp_poly.fontList: TextFont
*rb_fityp_poly.labelString: "POLY"
*rb_fityp_poly.set: "true"
*rb_fityp_poly.selectColor: SelectColor
*rb_fityp_poly.foreground: TextForeground
*rb_fityp_poly.highlightOnEnter: "true"
*rb_fityp_poly.indicatorSize: 16
*rb_fityp_poly.armCallback: RadioSet(UxWidget);
*rb_fityp_poly.translations: transTable34

*rb_fityp_spli.class: toggleButton
*rb_fityp_spli.parent: rowColumn4
*rb_fityp_spli.static: true
*rb_fityp_spli.name: rb_fityp_spli
*rb_fityp_spli.x: 3
*rb_fityp_spli.y: 34
*rb_fityp_spli.width: 76
*rb_fityp_spli.height: 66
*rb_fityp_spli.background: WindowBackground
*rb_fityp_spli.fontList: TextFont
*rb_fityp_spli.labelString: "SPLINE"
*rb_fityp_spli.selectColor: SelectColor
*rb_fityp_spli.foreground: TextForeground
*rb_fityp_spli.valueChangedCallback: {\
\
}
*rb_fityp_spli.set: "false"
*rb_fityp_spli.highlightOnEnter: "true"
*rb_fityp_spli.indicatorSize: 16
*rb_fityp_spli.armCallback: RadioSet(UxWidget);
*rb_fityp_spli.translations: transTable34

*label33.class: label
*label33.parent: form10
*label33.static: true
*label33.name: label33
*label33.x: 236
*label33.y: 164
*label33.width: 80
*label33.height: 22
*label33.background: WindowBackground
*label33.fontList: TextFont
*label33.labelString: "Fitting type"
*label33.alignment: "alignment_beginning"
*label33.foreground: TextForeground

*separatorGadget16.class: separatorGadget
*separatorGadget16.parent: form10
*separatorGadget16.static: true
*separatorGadget16.name: separatorGadget16
*separatorGadget16.x: 218
*separatorGadget16.y: 180
*separatorGadget16.width: 12
*separatorGadget16.height: 84
*separatorGadget16.orientation: "vertical"

*separatorGadget17.class: separatorGadget
*separatorGadget17.parent: form10
*separatorGadget17.static: true
*separatorGadget17.name: separatorGadget17
*separatorGadget17.x: 224
*separatorGadget17.y: 260
*separatorGadget17.width: 104
*separatorGadget17.height: 8

*separatorGadget18.class: separatorGadget
*separatorGadget18.parent: form10
*separatorGadget18.static: true
*separatorGadget18.name: separatorGadget18
*separatorGadget18.x: 322
*separatorGadget18.y: 180
*separatorGadget18.width: 12
*separatorGadget18.height: 84
*separatorGadget18.orientation: "vertical"

*rowColumn5.class: rowColumn
*rowColumn5.parent: form10
*rowColumn5.static: true
*rowColumn5.name: rowColumn5
*rowColumn5.x: 348
*rowColumn5.y: 190
*rowColumn5.width: 82
*rowColumn5.height: 67
*rowColumn5.radioBehavior: "true"
*rowColumn5.background: "grey80"
*rowColumn5.entryBorder: 0
*rowColumn5.labelString: ""
*rowColumn5.shadowThickness: 0
*rowColumn5.borderWidth: 0
*rowColumn5.entryAlignment: "alignment_beginning"
*rowColumn5.isAligned: "true"

*rb_plotyp_rati.class: toggleButton
*rb_plotyp_rati.parent: rowColumn5
*rb_plotyp_rati.static: true
*rb_plotyp_rati.name: rb_plotyp_rati
*rb_plotyp_rati.x: -12
*rb_plotyp_rati.y: 0
*rb_plotyp_rati.width: 76
*rb_plotyp_rati.height: 20
*rb_plotyp_rati.background: WindowBackground
*rb_plotyp_rati.fontList: TextFont
*rb_plotyp_rati.labelString: "RATIO"
*rb_plotyp_rati.set: "true"
*rb_plotyp_rati.selectColor: SelectColor
*rb_plotyp_rati.foreground: TextForeground
*rb_plotyp_rati.highlightOnEnter: "true"
*rb_plotyp_rati.indicatorSize: 16
*rb_plotyp_rati.armCallback: RadioSet(UxWidget);
*rb_plotyp_rati.translations: transTable34

*rb_plotyp_colo.class: toggleButton
*rb_plotyp_colo.parent: rowColumn5
*rb_plotyp_colo.static: true
*rb_plotyp_colo.name: rb_plotyp_colo
*rb_plotyp_colo.x: 3
*rb_plotyp_colo.y: 34
*rb_plotyp_colo.width: 76
*rb_plotyp_colo.height: 66
*rb_plotyp_colo.background: WindowBackground
*rb_plotyp_colo.fontList: TextFont
*rb_plotyp_colo.labelString: "MAGNITUDE"
*rb_plotyp_colo.selectColor: SelectColor
*rb_plotyp_colo.foreground: TextForeground
*rb_plotyp_colo.valueChangedCallback: {\
\
}
*rb_plotyp_colo.set: "false"
*rb_plotyp_colo.highlightOnEnter: "true"
*rb_plotyp_colo.indicatorSize: 16
*rb_plotyp_colo.armCallback: RadioSet(UxWidget);
*rb_plotyp_colo.translations: transTable34

*label34.class: label
*label34.parent: form10
*label34.static: true
*label34.name: label34
*label34.x: 365
*label34.y: 164
*label34.width: 91
*label34.height: 22
*label34.background: WindowBackground
*label34.fontList: TextFont
*label34.labelString: "Fitting space"
*label34.alignment: "alignment_beginning"
*label34.foreground: TextForeground

*separatorGadget21.class: separatorGadget
*separatorGadget21.parent: form10
*separatorGadget21.static: true
*separatorGadget21.name: separatorGadget21
*separatorGadget21.x: 334
*separatorGadget21.y: 180
*separatorGadget21.width: 12
*separatorGadget21.height: 84
*separatorGadget21.orientation: "vertical"

*separatorGadget22.class: separatorGadget
*separatorGadget22.parent: form10
*separatorGadget22.static: true
*separatorGadget22.name: separatorGadget22
*separatorGadget22.x: 471
*separatorGadget22.y: 180
*separatorGadget22.width: 12
*separatorGadget22.height: 84
*separatorGadget22.orientation: "vertical"

*separatorGadget23.class: separatorGadget
*separatorGadget23.parent: form10
*separatorGadget23.static: true
*separatorGadget23.name: separatorGadget23
*separatorGadget23.x: 338
*separatorGadget23.y: 176
*separatorGadget23.width: 24
*separatorGadget23.height: 10

*separatorGadget24.class: separatorGadget
*separatorGadget24.parent: form10
*separatorGadget24.static: true
*separatorGadget24.name: separatorGadget24
*separatorGadget24.x: 460
*separatorGadget24.y: 176
*separatorGadget24.width: 17
*separatorGadget24.height: 10

*separatorGadget25.class: separatorGadget
*separatorGadget25.parent: form10
*separatorGadget25.static: true
*separatorGadget25.name: separatorGadget25
*separatorGadget25.x: 339
*separatorGadget25.y: 260
*separatorGadget25.width: 138
*separatorGadget25.height: 8

*separatorGadget19.class: separatorGadget
*separatorGadget19.parent: form10
*separatorGadget19.static: true
*separatorGadget19.name: separatorGadget19
*separatorGadget19.x: 318
*separatorGadget19.y: 176
*separatorGadget19.width: 10
*separatorGadget19.height: 10

*separatorGadget20.class: separatorGadget
*separatorGadget20.parent: form10
*separatorGadget20.static: true
*separatorGadget20.name: separatorGadget20
*separatorGadget20.x: 222
*separatorGadget20.y: 176
*separatorGadget20.width: 10
*separatorGadget20.height: 10

*label35.class: label
*label35.parent: form10
*label35.static: true
*label35.name: label35
*label35.x: 12
*label35.y: 226
*label35.width: 120
*label35.height: 30
*label35.background: LabelBackground
*label35.fontList: TextFont
*label35.labelString: "Smoothing factor :"
*label35.alignment: "alignment_beginning"
*label35.foreground: TextForeground

*tf_smooth.class: textField
*tf_smooth.parent: form10
*tf_smooth.static: true
*tf_smooth.name: tf_smooth
*tf_smooth.x: 136
*tf_smooth.y: 222
*tf_smooth.width: 80
*tf_smooth.height: 34
*tf_smooth.background: TextBackground
*tf_smooth.fontList: TextFont
*tf_smooth.highlightOnEnter: "true"
*tf_smooth.foreground: TextForeground
*tf_smooth.losingFocusCallback: {\
\
}

*label36.class: label
*label36.parent: form10
*label36.static: true
*label36.name: label36
*label36.x: 12
*label36.y: 186
*label36.width: 120
*label36.height: 30
*label36.background: LabelBackground
*label36.fontList: TextFont
*label36.labelString: "Fit degree :"
*label36.alignment: "alignment_beginning"
*label36.foreground: TextForeground

*tf_fitd.class: textField
*tf_fitd.parent: form10
*tf_fitd.static: true
*tf_fitd.name: tf_fitd
*tf_fitd.x: 136
*tf_fitd.y: 182
*tf_fitd.width: 80
*tf_fitd.height: 34
*tf_fitd.background: TextBackground
*tf_fitd.fontList: TextFont
*tf_fitd.highlightOnEnter: "true"
*tf_fitd.foreground: TextForeground
*tf_fitd.losingFocusCallback: {\
\
}

*label37.class: label
*label37.parent: form10
*label37.static: true
*label37.name: label37
*label37.x: 4
*label37.y: 140
*label37.width: 180
*label37.height: 30
*label37.background: LabelBackground
*label37.fontList: TextFont
*label37.labelString: "FITTING PARAMETERS"
*label37.alignment: "alignment_beginning"
*label37.foreground: TextForeground

