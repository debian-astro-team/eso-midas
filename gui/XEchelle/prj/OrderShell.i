! UIMX ascii 2.0 key: 3518                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  

*translation.table: transTable10
*translation.parent: OrderShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable11
*translation.parent: OrderShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*OrderShell.class: transientShell
*OrderShell.parent: NO_PARENT
*OrderShell.static: true
*OrderShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*OrderShell.ispecdecl:
*OrderShell.funcdecl: swidget create_OrderShell()\

*OrderShell.funcname: create_OrderShell
*OrderShell.funcdef: "swidget", "<create_OrderShell>(%)"
*OrderShell.icode:
*OrderShell.fcode: return(rtrn);\

*OrderShell.auxdecl:
*OrderShell.name: OrderShell
*OrderShell.x: 845
*OrderShell.y: 107
*OrderShell.width: 500
*OrderShell.height: 344
*OrderShell.title: "XEchelle: Order Definition"
*OrderShell.geometry: "+10+60"

*form27.class: form
*form27.parent: OrderShell
*form27.static: true
*form27.name: form27
*form27.resizePolicy: "resize_none"
*form27.unitType: "pixels"
*form27.x: 4
*form27.y: 2
*form27.width: 500
*form27.height: 344
*form27.background: WindowBackground

*label61.class: label
*label61.parent: form27
*label61.static: true
*label61.name: label61
*label61.x: 18
*label61.y: 134
*label61.width: 170
*label61.height: 30
*label61.background: LabelBackground
*label61.fontList: TextFont
*label61.labelString: "Threshold:"
*label61.alignment: "alignment_beginning"
*label61.foreground: TextForeground

*tf_ywidth2.class: textField
*tf_ywidth2.parent: form27
*tf_ywidth2.static: true
*tf_ywidth2.name: tf_ywidth2
*tf_ywidth2.x: 208
*tf_ywidth2.y: 132
*tf_ywidth2.width: 82
*tf_ywidth2.height: 34
*tf_ywidth2.background: TextBackground
*tf_ywidth2.fontList: TextFont
*tf_ywidth2.highlightOnEnter: "true"
*tf_ywidth2.foreground: TextForeground
*tf_ywidth2.losingFocusCallback: {\
\
}

*label62.class: label
*label62.parent: form27
*label62.static: true
*label62.name: label62
*label62.x: 18
*label62.y: 170
*label62.width: 170
*label62.height: 30
*label62.background: LabelBackground
*label62.fontList: TextFont
*label62.labelString: "Slope:"
*label62.alignment: "alignment_beginning"
*label62.foreground: TextForeground

*label63.class: label
*label63.parent: form27
*label63.static: true
*label63.name: label63
*label63.x: 18
*label63.y: 58
*label63.width: 170
*label63.height: 30
*label63.background: LabelBackground
*label63.fontList: TextFont
*label63.labelString: "Number of orders:"
*label63.alignment: "alignment_beginning"
*label63.foreground: TextForeground

*tf_ystep2.class: textField
*tf_ystep2.parent: form27
*tf_ystep2.static: true
*tf_ystep2.name: tf_ystep2
*tf_ystep2.x: 208
*tf_ystep2.y: 170
*tf_ystep2.width: 82
*tf_ystep2.height: 34
*tf_ystep2.background: TextBackground
*tf_ystep2.fontList: TextFont
*tf_ystep2.highlightOnEnter: "true"
*tf_ystep2.foreground: TextForeground
*tf_ystep2.losingFocusCallback: {\
\
}

*tf_width2.class: textField
*tf_width2.parent: form27
*tf_width2.static: true
*tf_width2.name: tf_width2
*tf_width2.x: 208
*tf_width2.y: 94
*tf_width2.width: 82
*tf_width2.height: 34
*tf_width2.background: TextBackground
*tf_width2.fontList: TextFont
*tf_width2.highlightOnEnter: "true"
*tf_width2.foreground: TextForeground
*tf_width2.losingFocusCallback: {\
\
}

*rowColumn26.class: rowColumn
*rowColumn26.parent: form27
*rowColumn26.static: true
*rowColumn26.name: rowColumn26
*rowColumn26.x: 332
*rowColumn26.y: 82
*rowColumn26.width: 106
*rowColumn26.height: 96
*rowColumn26.radioBehavior: "true"
*rowColumn26.background: WindowBackground
*rowColumn26.entryBorder: 0
*rowColumn26.labelString: ""
*rowColumn26.shadowThickness: 0
*rowColumn26.borderWidth: 0
*rowColumn26.entryAlignment: "alignment_beginning"
*rowColumn26.adjustLast: "false"
*rowColumn26.adjustMargin: "true"
*rowColumn26.isAligned: "true"

*rb_seamtd_gaus2.class: toggleButton
*rb_seamtd_gaus2.parent: rowColumn26
*rb_seamtd_gaus2.static: true
*rb_seamtd_gaus2.name: rb_seamtd_gaus2
*rb_seamtd_gaus2.x: 12
*rb_seamtd_gaus2.y: 12
*rb_seamtd_gaus2.width: 148
*rb_seamtd_gaus2.height: 30
*rb_seamtd_gaus2.background: WindowBackground
*rb_seamtd_gaus2.fontList: TextFont
*rb_seamtd_gaus2.labelString: "Hough"
*rb_seamtd_gaus2.set: "true"
*rb_seamtd_gaus2.selectColor: SelectColor
*rb_seamtd_gaus2.highlightOnEnter: "true"
*rb_seamtd_gaus2.valueChangedCallback: {\
\
}
*rb_seamtd_gaus2.indicatorSize: 16
*rb_seamtd_gaus2.foreground: TextForeground
*rb_seamtd_gaus2.armCallback: RadioSet ( UxWidget ) ;
*rb_seamtd_gaus2.translations: transTable11

*rb_seamtd_grav2.class: toggleButton
*rb_seamtd_grav2.parent: rowColumn26
*rb_seamtd_grav2.static: true
*rb_seamtd_grav2.name: rb_seamtd_grav2
*rb_seamtd_grav2.x: 5
*rb_seamtd_grav2.y: 85
*rb_seamtd_grav2.width: 148
*rb_seamtd_grav2.height: 30
*rb_seamtd_grav2.background: WindowBackground
*rb_seamtd_grav2.fontList: TextFont
*rb_seamtd_grav2.labelString: "Standard"
*rb_seamtd_grav2.selectColor: SelectColor
*rb_seamtd_grav2.highlightOnEnter: "true"
*rb_seamtd_grav2.valueChangedCallback: {\
\
}
*rb_seamtd_grav2.indicatorSize: 16
*rb_seamtd_grav2.foreground: TextForeground
*rb_seamtd_grav2.armCallback: RadioSet ( UxWidget ) ;
*rb_seamtd_grav2.translations: transTable11

*rb_seamtd_maxi2.class: toggleButton
*rb_seamtd_maxi2.parent: rowColumn26
*rb_seamtd_maxi2.static: true
*rb_seamtd_maxi2.name: rb_seamtd_maxi2
*rb_seamtd_maxi2.x: 4
*rb_seamtd_maxi2.y: 66
*rb_seamtd_maxi2.width: 96
*rb_seamtd_maxi2.height: 24
*rb_seamtd_maxi2.background: WindowBackground
*rb_seamtd_maxi2.fontList: TextFont
*rb_seamtd_maxi2.labelString: "Complement"
*rb_seamtd_maxi2.selectColor: SelectColor
*rb_seamtd_maxi2.highlightOnEnter: "true"
*rb_seamtd_maxi2.valueChangedCallback: {\
\
}
*rb_seamtd_maxi2.indicatorSize: 16
*rb_seamtd_maxi2.foreground: TextForeground
*rb_seamtd_maxi2.armCallback: RadioSet ( UxWidget ) ;
*rb_seamtd_maxi2.translations: transTable11

*label64.class: label
*label64.parent: form27
*label64.static: true
*label64.name: label64
*label64.x: 336
*label64.y: 52
*label64.width: 106
*label64.height: 30
*label64.background: LabelBackground
*label64.fontList: TextFont
*label64.labelString: "Method"
*label64.alignment: "alignment_center"
*label64.foreground: TextForeground

*separatorGadget6.class: separatorGadget
*separatorGadget6.parent: form27
*separatorGadget6.static: true
*separatorGadget6.name: separatorGadget6
*separatorGadget6.x: 450
*separatorGadget6.y: 66
*separatorGadget6.width: 12
*separatorGadget6.height: 120
*separatorGadget6.orientation: "vertical"

*separatorGadget7.class: separatorGadget
*separatorGadget7.parent: form27
*separatorGadget7.static: true
*separatorGadget7.name: separatorGadget7
*separatorGadget7.x: 314
*separatorGadget7.y: 66
*separatorGadget7.width: 12
*separatorGadget7.height: 120
*separatorGadget7.orientation: "vertical"

*separatorGadget8.class: separatorGadget
*separatorGadget8.parent: form27
*separatorGadget8.static: true
*separatorGadget8.name: separatorGadget8
*separatorGadget8.x: 318
*separatorGadget8.y: 64
*separatorGadget8.width: 16
*separatorGadget8.height: 10

*separatorGadget9.class: separatorGadget
*separatorGadget9.parent: form27
*separatorGadget9.static: true
*separatorGadget9.name: separatorGadget9
*separatorGadget9.x: 442
*separatorGadget9.y: 64
*separatorGadget9.width: 14
*separatorGadget9.height: 10

*tf_thres2.class: textField
*tf_thres2.parent: form27
*tf_thres2.static: true
*tf_thres2.name: tf_thres2
*tf_thres2.x: 208
*tf_thres2.y: 56
*tf_thres2.width: 82
*tf_thres2.height: 34
*tf_thres2.background: TextBackground
*tf_thres2.fontList: TextFont
*tf_thres2.highlightOnEnter: "true"
*tf_thres2.foreground: TextForeground
*tf_thres2.losingFocusCallback: {\
\
}

*form28.class: form
*form28.parent: form27
*form28.static: true
*form28.name: form28
*form28.resizePolicy: "resize_none"
*form28.x: 2
*form28.y: 282
*form28.width: 490
*form28.height: 40
*form28.background: ButtonBackground

*pb_search_search3.class: pushButton
*pb_search_search3.parent: form28
*pb_search_search3.static: true
*pb_search_search3.name: pb_search_search3
*pb_search_search3.x: 8
*pb_search_search3.y: 4
*pb_search_search3.width: 86
*pb_search_search3.height: 30
*pb_search_search3.background: ButtonBackground
*pb_search_search3.fontList: BoldTextFont
*pb_search_search3.foreground: ApplyForeground
*pb_search_search3.labelString: "Define"
*pb_search_search3.activateCallback: {\
 MidasCommand(UxThisWidget);\
\
}

*pushButton3.class: pushButton
*pushButton3.parent: form28
*pushButton3.static: true
*pushButton3.name: pushButton3
*pushButton3.x: 392
*pushButton3.y: 6
*pushButton3.width: 86
*pushButton3.height: 30
*pushButton3.background: ButtonBackground
*pushButton3.fontList: BoldTextFont
*pushButton3.foreground: CancelForeground
*pushButton3.labelString: "Cancel"
*pushButton3.activateCallback: {\
UxPopdownInterface(UxFindSwidget("OrderShell"));\
}

*pb_search_plot3.class: pushButton
*pb_search_plot3.parent: form28
*pb_search_plot3.static: true
*pb_search_plot3.name: pb_search_plot3
*pb_search_plot3.x: 102
*pb_search_plot3.y: 4
*pb_search_plot3.width: 100
*pb_search_plot3.height: 30
*pb_search_plot3.background: ButtonBackground
*pb_search_plot3.fontList: BoldTextFont
*pb_search_plot3.foreground: ButtonForeground
*pb_search_plot3.labelString: "Load Orders"
*pb_search_plot3.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pb_search_plot4.class: pushButton
*pb_search_plot4.parent: form28
*pb_search_plot4.static: true
*pb_search_plot4.name: pb_search_plot4
*pb_search_plot4.x: 214
*pb_search_plot4.y: 4
*pb_search_plot4.width: 86
*pb_search_plot4.height: 30
*pb_search_plot4.background: ButtonBackground
*pb_search_plot4.fontList: BoldTextFont
*pb_search_plot4.foreground: ButtonForeground
*pb_search_plot4.labelString: "Save..."
*pb_search_plot4.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pb_search_plot5.class: pushButton
*pb_search_plot5.parent: form28
*pb_search_plot5.static: true
*pb_search_plot5.name: pb_search_plot5
*pb_search_plot5.x: 304
*pb_search_plot5.y: 4
*pb_search_plot5.width: 86
*pb_search_plot5.height: 30
*pb_search_plot5.background: ButtonBackground
*pb_search_plot5.fontList: BoldTextFont
*pb_search_plot5.foreground: ButtonForeground
*pb_search_plot5.labelString: "Help..."
*pb_search_plot5.activateCallback: {\
GetExtendedHelp(UxWidget);\
}

*shelp_search3.class: text
*shelp_search3.parent: form27
*shelp_search3.static: true
*shelp_search3.name: shelp_search3
*shelp_search3.x: 4
*shelp_search3.y: 222
*shelp_search3.width: 484
*shelp_search3.height: 50
*shelp_search3.background: SHelpBackground
*shelp_search3.cursorPositionVisible: "false"
*shelp_search3.editable: "false"
*shelp_search3.fontList: TextFont

*separator22.class: separator
*separator22.parent: form27
*separator22.static: true
*separator22.name: separator22
*separator22.x: 2
*separator22.y: 212
*separator22.width: 492
*separator22.height: 10
*separator22.background: WindowBackground

*separator23.class: separator
*separator23.parent: form27
*separator23.static: true
*separator23.name: separator23
*separator23.x: 0
*separator23.y: 270
*separator23.width: 492
*separator23.height: 10
*separator23.background: WindowBackground

*separatorGadget10.class: separatorGadget
*separatorGadget10.parent: form27
*separatorGadget10.static: true
*separatorGadget10.name: separatorGadget10
*separatorGadget10.x: 318
*separatorGadget10.y: 180
*separatorGadget10.width: 138
*separatorGadget10.height: 10

*label65.class: label
*label65.parent: form27
*label65.static: true
*label65.name: label65
*label65.x: 18
*label65.y: 94
*label65.width: 170
*label65.height: 30
*label65.background: LabelBackground
*label65.fontList: TextFont
*label65.labelString: "Order width:"
*label65.alignment: "alignment_beginning"
*label65.foreground: TextForeground

*tf_thres3.class: textField
*tf_thres3.parent: form27
*tf_thres3.static: true
*tf_thres3.name: tf_thres3
*tf_thres3.x: 208
*tf_thres3.y: 12
*tf_thres3.width: 212
*tf_thres3.height: 34
*tf_thres3.background: TextBackground
*tf_thres3.fontList: TextFont
*tf_thres3.highlightOnEnter: "true"
*tf_thres3.foreground: TextForeground
*tf_thres3.losingFocusCallback: {\
\
}

*label66.class: label
*label66.parent: form27
*label66.static: true
*label66.name: label66
*label66.x: 20
*label66.y: 14
*label66.width: 170
*label66.height: 30
*label66.background: LabelBackground
*label66.fontList: TextFont
*label66.labelString: "Order Definition Frame:"
*label66.alignment: "alignment_beginning"
*label66.foreground: TextForeground

*pb_main_search15.class: pushButton
*pb_main_search15.parent: form27
*pb_main_search15.static: true
*pb_main_search15.name: pb_main_search15
*pb_main_search15.x: 426
*pb_main_search15.y: 14
*pb_main_search15.height: 28
*pb_main_search15.background: ButtonBackground
*pb_main_search15.fontList: BoldTextFont
*pb_main_search15.foreground: ButtonForeground
*pb_main_search15.labelString: "..."
*pb_main_search15.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search15.recomputeSize: "true"
*pb_main_search15.width: 46

