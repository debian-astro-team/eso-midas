! UIMX ascii 2.0 key: 1471                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  
*action.UpdateDirectory: {\
\
 char node[80];\
\
 /* if (XmTextGetSelection(UxWidget) == NULL) return; */\
 strcpy(node,\
     XmTextGetSelection(UxWidget));\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
node[strlen(node)-1] = '\0';\
WGet_all_dirs(node);\
WChange_Midas_dir();\
WGet_all_files();\
\
}

*translation.table: transTable30
*translation.parent: MergeShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable31
*translation.parent: MergeShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*translation.table: transTable32
*translation.parent: MergeShell
*translation.policy: override
*translation.<Btn1Up>: UpdateDirectory()

*MergeShell.class: transientShell
*MergeShell.parent: NO_PARENT
*MergeShell.static: true
*MergeShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*MergeShell.ispecdecl:
*MergeShell.funcdecl: swidget create_MergeShell()\

*MergeShell.funcname: create_MergeShell
*MergeShell.funcdef: "swidget", "<create_MergeShell>(%)"
*MergeShell.icode:
*MergeShell.fcode: return(rtrn);\

*MergeShell.auxdecl:
*MergeShell.name: MergeShell
*MergeShell.x: 454
*MergeShell.y: 514
*MergeShell.width: 500
*MergeShell.height: 330
*MergeShell.geometry: "+10+60"
*MergeShell.title: "XEchelle: Orders Merging"

*form14.class: form
*form14.parent: MergeShell
*form14.static: true
*form14.name: form14
*form14.resizePolicy: "resize_none"
*form14.unitType: "pixels"
*form14.x: 4
*form14.y: 2
*form14.width: 500
*form14.height: 344
*form14.background: WindowBackground

*form15.class: form
*form15.parent: form14
*form15.static: true
*form15.name: form15
*form15.resizePolicy: "resize_none"
*form15.x: 2
*form15.y: 282
*form15.width: 490
*form15.height: 40
*form15.background: ButtonBackground

*pb_search_search13.class: pushButton
*pb_search_search13.parent: form15
*pb_search_search13.static: true
*pb_search_search13.name: pb_search_search13
*pb_search_search13.x: 2
*pb_search_search13.y: 4
*pb_search_search13.width: 86
*pb_search_search13.height: 30
*pb_search_search13.background: ButtonBackground
*pb_search_search13.fontList: BoldTextFont
*pb_search_search13.foreground: ApplyForeground
*pb_search_search13.labelString: "Merge"
*pb_search_search13.activateCallback: {\
 MidasCommand(UxThisWidget);\
\
}

*pushButton10.class: pushButton
*pushButton10.parent: form15
*pushButton10.static: true
*pushButton10.name: pushButton10
*pushButton10.x: 398
*pushButton10.y: 4
*pushButton10.width: 86
*pushButton10.height: 30
*pushButton10.background: ButtonBackground
*pushButton10.fontList: BoldTextFont
*pushButton10.foreground: CancelForeground
*pushButton10.labelString: "Cancel"
*pushButton10.activateCallback: {\
UxPopdownInterface(UxFindSwidget("MergeShell"));\
}

*pb_search_plot25.class: pushButton
*pb_search_plot25.parent: form15
*pb_search_plot25.static: true
*pb_search_plot25.name: pb_search_plot25
*pb_search_plot25.x: 90
*pb_search_plot25.y: 4
*pb_search_plot25.width: 112
*pb_search_plot25.height: 30
*pb_search_plot25.background: ButtonBackground
*pb_search_plot25.fontList: BoldTextFont
*pb_search_plot25.foreground: ButtonForeground
*pb_search_plot25.labelString: "Plot Overlaps"
*pb_search_plot25.activateCallback: {\
MidasCommand(UxThisWidget);\
}

*pb_search_plot28.class: pushButton
*pb_search_plot28.parent: form15
*pb_search_plot28.static: true
*pb_search_plot28.name: pb_search_plot28
*pb_search_plot28.x: 202
*pb_search_plot28.y: 4
*pb_search_plot28.width: 118
*pb_search_plot28.height: 30
*pb_search_plot28.background: ButtonBackground
*pb_search_plot28.fontList: BoldTextFont
*pb_search_plot28.foreground: ButtonForeground
*pb_search_plot28.labelString: "Plot Spectrum"
*pb_search_plot28.activateCallback: {\
 MidasCommand(UxThisWidget);\
\
}

*pb_search_plot29.class: pushButton
*pb_search_plot29.parent: form15
*pb_search_plot29.static: true
*pb_search_plot29.name: pb_search_plot29
*pb_search_plot29.x: 324
*pb_search_plot29.y: 4
*pb_search_plot29.width: 74
*pb_search_plot29.height: 30
*pb_search_plot29.background: ButtonBackground
*pb_search_plot29.fontList: BoldTextFont
*pb_search_plot29.foreground: ButtonForeground
*pb_search_plot29.labelString: "Help"
*pb_search_plot29.activateCallback: {\
GetExtendedHelp(UxWidget);\
}

*shelp_search9.class: text
*shelp_search9.parent: form14
*shelp_search9.static: true
*shelp_search9.name: shelp_search9
*shelp_search9.x: 4
*shelp_search9.y: 222
*shelp_search9.width: 484
*shelp_search9.height: 50
*shelp_search9.background: SHelpBackground
*shelp_search9.cursorPositionVisible: "false"
*shelp_search9.editable: "false"
*shelp_search9.fontList: TextFont

*separator10.class: separator
*separator10.parent: form14
*separator10.static: true
*separator10.name: separator10
*separator10.x: 2
*separator10.y: 212
*separator10.width: 492
*separator10.height: 10
*separator10.background: WindowBackground

*separator11.class: separator
*separator11.parent: form14
*separator11.static: true
*separator11.name: separator11
*separator11.x: 0
*separator11.y: 270
*separator11.width: 492
*separator11.height: 10
*separator11.background: WindowBackground

*label38.class: label
*label38.parent: form14
*label38.static: true
*label38.name: label38
*label38.x: 20
*label38.y: 14
*label38.width: 130
*label38.height: 30
*label38.background: LabelBackground
*label38.fontList: TextFont
*label38.labelString: "Input Frame:"
*label38.alignment: "alignment_beginning"
*label38.foreground: TextForeground

*tf_thres8.class: textField
*tf_thres8.parent: form14
*tf_thres8.static: true
*tf_thres8.name: tf_thres8
*tf_thres8.x: 170
*tf_thres8.y: 14
*tf_thres8.width: 210
*tf_thres8.height: 34
*tf_thres8.background: TextBackground
*tf_thres8.fontList: TextFont
*tf_thres8.highlightOnEnter: "true"
*tf_thres8.foreground: TextForeground
*tf_thres8.losingFocusCallback: {\
\
}

*pb_main_search30.class: pushButton
*pb_main_search30.parent: form14
*pb_main_search30.static: true
*pb_main_search30.name: pb_main_search30
*pb_main_search30.x: 428
*pb_main_search30.y: 14
*pb_main_search30.height: 28
*pb_main_search30.background: ButtonBackground
*pb_main_search30.fontList: BoldTextFont
*pb_main_search30.foreground: ButtonForeground
*pb_main_search30.labelString: "..."
*pb_main_search30.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search30.recomputeSize: "true"
*pb_main_search30.width: 46

*pb_main_search31.class: pushButton
*pb_main_search31.parent: form14
*pb_main_search31.static: true
*pb_main_search31.name: pb_main_search31
*pb_main_search31.x: 428
*pb_main_search31.y: 54
*pb_main_search31.height: 28
*pb_main_search31.background: ButtonBackground
*pb_main_search31.fontList: BoldTextFont
*pb_main_search31.foreground: ButtonForeground
*pb_main_search31.labelString: "..."
*pb_main_search31.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search31.recomputeSize: "true"
*pb_main_search31.width: 46

*tf_thres11.class: textField
*tf_thres11.parent: form14
*tf_thres11.static: true
*tf_thres11.name: tf_thres11
*tf_thres11.x: 168
*tf_thres11.y: 52
*tf_thres11.width: 212
*tf_thres11.height: 34
*tf_thres11.background: TextBackground
*tf_thres11.fontList: TextFont
*tf_thres11.highlightOnEnter: "true"
*tf_thres11.foreground: TextForeground
*tf_thres11.losingFocusCallback: {\
\
}

*label42.class: label
*label42.parent: form14
*label42.static: true
*label42.name: label42
*label42.x: 20
*label42.y: 56
*label42.width: 120
*label42.height: 30
*label42.background: LabelBackground
*label42.fontList: TextFont
*label42.labelString: "Output Frame:"
*label42.alignment: "alignment_beginning"
*label42.foreground: TextForeground

*label43.class: label
*label43.parent: form14
*label43.static: true
*label43.name: label43
*label43.x: 22
*label43.y: 94
*label43.width: 68
*label43.height: 30
*label43.background: LabelBackground
*label43.fontList: TextFont
*label43.labelString: "Method:"
*label43.alignment: "alignment_beginning"
*label43.foreground: TextForeground

*rowColumn6.class: rowColumn
*rowColumn6.parent: form14
*rowColumn6.static: true
*rowColumn6.name: rowColumn6
*rowColumn6.x: 134
*rowColumn6.y: 97
*rowColumn6.width: 261
*rowColumn6.height: 34
*rowColumn6.radioBehavior: "true"
*rowColumn6.background: WindowBackground
*rowColumn6.entryBorder: 0
*rowColumn6.labelString: ""
*rowColumn6.shadowThickness: 0
*rowColumn6.borderWidth: 0
*rowColumn6.entryAlignment: "alignment_beginning"
*rowColumn6.adjustLast: "false"
*rowColumn6.adjustMargin: "true"
*rowColumn6.isAligned: "true"
*rowColumn6.orientation: "horizontal"
*rowColumn6.resizeWidth: "true"

*rb_seamtd_gaus6.class: toggleButton
*rb_seamtd_gaus6.parent: rowColumn6
*rb_seamtd_gaus6.static: true
*rb_seamtd_gaus6.name: rb_seamtd_gaus6
*rb_seamtd_gaus6.x: 2
*rb_seamtd_gaus6.y: 3
*rb_seamtd_gaus6.width: 84
*rb_seamtd_gaus6.height: 28
*rb_seamtd_gaus6.background: WindowBackground
*rb_seamtd_gaus6.fontList: TextFont
*rb_seamtd_gaus6.labelString: "Average Overlaps"
*rb_seamtd_gaus6.set: "true"
*rb_seamtd_gaus6.selectColor: SelectColor
*rb_seamtd_gaus6.highlightOnEnter: "true"
*rb_seamtd_gaus6.valueChangedCallback: {\
\
}
*rb_seamtd_gaus6.indicatorSize: 16
*rb_seamtd_gaus6.foreground: TextForeground
*rb_seamtd_gaus6.armCallback: RadioSet(UxWidget);
*rb_seamtd_gaus6.translations: transTable31

*rb_seamtd_grav6.class: toggleButton
*rb_seamtd_grav6.parent: rowColumn6
*rb_seamtd_grav6.static: true
*rb_seamtd_grav6.name: rb_seamtd_grav6
*rb_seamtd_grav6.x: 114
*rb_seamtd_grav6.y: 4
*rb_seamtd_grav6.width: 84
*rb_seamtd_grav6.height: 28
*rb_seamtd_grav6.background: WindowBackground
*rb_seamtd_grav6.fontList: TextFont
*rb_seamtd_grav6.labelString: "No Append"
*rb_seamtd_grav6.selectColor: SelectColor
*rb_seamtd_grav6.highlightOnEnter: "true"
*rb_seamtd_grav6.valueChangedCallback: {\
\
}
*rb_seamtd_grav6.indicatorSize: 16
*rb_seamtd_grav6.foreground: TextForeground
*rb_seamtd_grav6.armCallback: RadioSet(UxWidget);
*rb_seamtd_grav6.translations: transTable31

*label45.class: label
*label45.parent: form14
*label45.static: true
*label45.name: label45
*label45.x: 24
*label45.y: 170
*label45.width: 94
*label45.height: 30
*label45.background: LabelBackground
*label45.fontList: TextFont
*label45.labelString: "Order Range"
*label45.alignment: "alignment_beginning"
*label45.foreground: TextForeground

*label46.class: label
*label46.parent: form14
*label46.static: true
*label46.name: label46
*label46.x: 140
*label46.y: 170
*label46.width: 40
*label46.height: 30
*label46.background: LabelBackground
*label46.fontList: TextFont
*label46.labelString: "Min="
*label46.alignment: "alignment_beginning"
*label46.foreground: TextForeground

*tf_alpha31.class: textField
*tf_alpha31.parent: form14
*tf_alpha31.static: true
*tf_alpha31.name: tf_alpha31
*tf_alpha31.x: 188
*tf_alpha31.y: 168
*tf_alpha31.width: 96
*tf_alpha31.height: 32
*tf_alpha31.background: TextBackground
*tf_alpha31.fontList: TextFont
*tf_alpha31.highlightOnEnter: "true"
*tf_alpha31.foreground: TextForeground
*tf_alpha31.losingFocusCallback: {\
\
}

*label47.class: label
*label47.parent: form14
*label47.static: true
*label47.name: label47
*label47.x: 302
*label47.y: 168
*label47.width: 50
*label47.height: 30
*label47.background: LabelBackground
*label47.fontList: TextFont
*label47.labelString: "Max="
*label47.alignment: "alignment_beginning"
*label47.foreground: TextForeground

*tf_alpha40.class: textField
*tf_alpha40.parent: form14
*tf_alpha40.static: true
*tf_alpha40.name: tf_alpha40
*tf_alpha40.x: 360
*tf_alpha40.y: 166
*tf_alpha40.width: 96
*tf_alpha40.height: 32
*tf_alpha40.background: TextBackground
*tf_alpha40.fontList: TextFont
*tf_alpha40.highlightOnEnter: "true"
*tf_alpha40.foreground: TextForeground
*tf_alpha40.losingFocusCallback: {\
\
}

*label48.class: label
*label48.parent: form14
*label48.static: true
*label48.name: label48
*label48.x: 16
*label48.y: 134
*label48.width: 136
*label48.height: 30
*label48.background: LabelBackground
*label48.fontList: TextFont
*label48.labelString: "Ignored Interval:"
*label48.alignment: "alignment_beginning"
*label48.foreground: TextForeground

*tf_alpha42.class: textField
*tf_alpha42.parent: form14
*tf_alpha42.static: true
*tf_alpha42.name: tf_alpha42
*tf_alpha42.x: 184
*tf_alpha42.y: 134
*tf_alpha42.width: 140
*tf_alpha42.height: 32
*tf_alpha42.background: TextBackground
*tf_alpha42.fontList: TextFont
*tf_alpha42.highlightOnEnter: "true"
*tf_alpha42.foreground: TextForeground
*tf_alpha42.losingFocusCallback: {\
\
}

