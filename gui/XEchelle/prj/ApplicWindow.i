! UIMX ascii 2.0 key: 3501                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  
*action.UpdateDirectory: {\
\
 char node[80];\
\
 /* if (XmTextGetSelection(UxWidget) == NULL) return; */\
 strcpy(node,\
     XmTextGetSelection(UxWidget));\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
node[strlen(node)-1] = '\0';\
WGet_all_dirs(node);\
WChange_Midas_dir();\
WGet_all_files();\
\
}
*action.ClearHelp: {\
/*\
UxPutText(UxFindSwidget("shelp_main"), "");\
UxPutText(UxFindSwidget("shelp_search"), "");\
UxPutText(UxFindSwidget("shelp_calib"), "");\
UxPutText(UxFindSwidget("shelp_rebin"), "");\
UxPutText(UxFindSwidget("shelp_extract"), "");\
UxPutText(UxFindSwidget("shelp_flux"), "");\
*/\
}
*action.WriteHelp: {\
DisplayShortHelp(UxWidget);\
}

*translation.table: SelectFileMain
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable1
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable2
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*translation.table: DirList
*translation.parent: ApplicWindow
*translation.policy: override
*translation.<Btn1Up>: UpdateDirectory()

*ApplicWindow.class: applicationShell
*ApplicWindow.parent: NO_PARENT
*ApplicWindow.static: true
*ApplicWindow.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <proto_xech.h>\
\
int FilterFiles();\
int GetSelection();\

*ApplicWindow.ispecdecl:
*ApplicWindow.funcdecl: swidget create_ApplicWindow()\

*ApplicWindow.funcname: create_ApplicWindow
*ApplicWindow.funcdef: "swidget", "<create_ApplicWindow>(%)"
*ApplicWindow.icode: extern swidget create_SearchShell();\
extern swidget create_CalibShell();\
extern swidget create_RebinShell();\
extern swidget create_file_dialog();\
extern swidget create_extin_dialog();\
extern swidget create_resid_dialog();\
extern swidget create_HelpShell();\
extern swidget create_OrderShell();\
extern swidget create_RotateShell();\
extern swidget create_BackgroundShell();\
extern swidget create_ExtractionShell();\
extern swidget create_SkyShell();\
extern swidget create_FlatShell();\
extern swidget create_RespShell();\
extern swidget create_MergeShell();\
\

*ApplicWindow.fcode: create_SearchShell();\
create_CalibShell();\
create_RebinShell();\
create_file_dialog();\
create_extin_dialog();\
create_resid_dialog();\
create_HelpShell();\
create_OrderShell();\
create_RotateShell();\
create_BackgroundShell();\
create_ExtractionShell();\
create_SkyShell();\
create_FlatShell();\
create_RespShell();\
create_MergeShell();\
\
return(rtrn);\
\

*ApplicWindow.auxdecl: FilterFiles(node)\
\
char *node;\
\
{\
 WGet_all_dirs(node);\
 WGet_all_files();\
}\
\
CheckStatus(sw)\
\
swidget sw;\
{\
     char status[20]; \
     strcpy(status,UxGetSet(sw));\
     if (status[0] == 't') return(TRUE);\
     else return(FALSE);\
}
*ApplicWindow.name: ApplicWindow
*ApplicWindow.x: 51
*ApplicWindow.y: 126
*ApplicWindow.width: 810
*ApplicWindow.height: 588
*ApplicWindow.iconName: "XEchelle"
*ApplicWindow.keyboardFocusPolicy: "pointer"
*ApplicWindow.title: "Echelle Reduction"
*ApplicWindow.geometry: "+0+0"

*MainWindow.class: mainWindow
*MainWindow.parent: ApplicWindow
*MainWindow.static: true
*MainWindow.name: MainWindow
*MainWindow.unitType: "pixels"
*MainWindow.x: -2
*MainWindow.y: 0
*MainWindow.width: 810
*MainWindow.height: 588
*MainWindow.background: WindowBackground

*menu1.class: rowColumn
*menu1.parent: MainWindow
*menu1.static: true
*menu1.name: menu1
*menu1.rowColumnType: "menu_bar"
*menu1.menuAccelerator: "<KeyUp>F10"
*menu1.menuHelpWidget: "menu1_top_b5"
*menu1.background: MenuBackground

*menu1_p1.class: rowColumn
*menu1_p1.parent: menu1
*menu1_p1.static: true
*menu1_p1.name: menu1_p1
*menu1_p1.rowColumnType: "menu_pulldown"
*menu1_p1.background: MenuBackground
*menu1_p1.foreground: MenuForeground

*menu1_p1_b1.class: pushButtonGadget
*menu1_p1_b1.parent: menu1_p1
*menu1_p1_b1.static: true
*menu1_p1_b1.name: menu1_p1_b1
*menu1_p1_b1.labelString: "Open..."
*menu1_p1_b1.mnemonic: "O"
*menu1_p1_b1.fontList: BoldTextFont
*menu1_p1_b1.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_SESSION);\
}

*menu1_p1_b2.class: pushButtonGadget
*menu1_p1_b2.parent: menu1_p1
*menu1_p1_b2.static: true
*menu1_p1_b2.name: menu1_p1_b2
*menu1_p1_b2.labelString: "Save"
*menu1_p1_b2.mnemonic: "S"
*menu1_p1_b2.fontList: BoldTextFont
*menu1_p1_b2.activateCallback: {\
#include <spec_comm.h>\
\
extern char Session[];\
char command[256];\
\
sprintf(command, "%s%s", C_SAVE, Session);\
AppendDialogText(command);\
}

*menu1_p1_b3.class: pushButtonGadget
*menu1_p1_b3.parent: menu1_p1
*menu1_p1_b3.static: true
*menu1_p1_b3.name: menu1_p1_b3
*menu1_p1_b3.labelString: "Save As ..."
*menu1_p1_b3.mnemonic: "S"
*menu1_p1_b3.fontList: BoldTextFont
*menu1_p1_b3.activateCallback: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern int DialogType;\
extern char Session[];\
\
SET_DIALOG_PROMPT("Output parameters table :");\
\
XmTextSetString(UxGetWidget(UxFindSwidget("tf_file_dialog")), Session);\
DialogType = DIALOG_SESSION;\
UxPopupInterface(UxFindSwidget("file_dialog"), exclusive_grab);\
}

*menu1_p1_b5.class: separator
*menu1_p1_b5.parent: menu1_p1
*menu1_p1_b5.static: true
*menu1_p1_b5.name: menu1_p1_b5

*menu1_p1_b6.class: pushButtonGadget
*menu1_p1_b6.parent: menu1_p1
*menu1_p1_b6.static: true
*menu1_p1_b6.name: menu1_p1_b6
*menu1_p1_b6.labelString: "Set to defaults"
*menu1_p1_b6.activateCallback: {AppendDialogText("INIT/ECHELLE");\
 AppendDialogText("SYNCHRO/ECHELLE");\
 InitAllFields();\
}
*menu1_p1_b6.fontList: BoldTextFont

*menu1_p1_b7.class: pushButtonGadget
*menu1_p1_b7.parent: menu1_p1
*menu1_p1_b7.static: true
*menu1_p1_b7.name: menu1_p1_b7
*menu1_p1_b7.labelString: "Synchronize"
*menu1_p1_b7.activateCallback: {AppendDialogText("Synchron/ECHELLE");\
 InitAllFields();\
}
*menu1_p1_b7.fontList: BoldTextFont

*menu1_p1_b8.class: separator
*menu1_p1_b8.parent: menu1_p1
*menu1_p1_b8.static: true
*menu1_p1_b8.name: menu1_p1_b8

*menu1_p1_b4.class: pushButtonGadget
*menu1_p1_b4.parent: menu1_p1
*menu1_p1_b4.static: true
*menu1_p1_b4.name: menu1_p1_b4
*menu1_p1_b4.labelString: "Exit"
*menu1_p1_b4.mnemonic: "E"
*menu1_p1_b4.accelerator: "E"
*menu1_p1_b4.activateCallback: {exit(0);}
*menu1_p1_b4.fontList: BoldTextFont

*menu1_p4.class: rowColumn
*menu1_p4.parent: menu1
*menu1_p4.static: true
*menu1_p4.name: menu1_p4
*menu1_p4.rowColumnType: "menu_pulldown"
*menu1_p4.background: MenuBackground
*menu1_p4.foreground: MenuForeground

*menu1_p4_b1.class: pushButtonGadget
*menu1_p4_b1.parent: menu1_p4
*menu1_p4_b1.static: true
*menu1_p4_b1.name: menu1_p4_b1
*menu1_p4_b1.labelString: "Create Display"
*menu1_p4_b1.mnemonic: "D"
*menu1_p4_b1.fontList: BoldTextFont
*menu1_p4_b1.activateCallback: AppendDialogText("create/display");

*menu1_p4_b2.class: pushButtonGadget
*menu1_p4_b2.parent: menu1_p4
*menu1_p4_b2.static: true
*menu1_p4_b2.name: menu1_p4_b2
*menu1_p4_b2.labelString: "Create Graphics"
*menu1_p4_b2.mnemonic: "G"
*menu1_p4_b2.fontList: BoldTextFont
*menu1_p4_b2.activateCallback: AppendDialogText("graph/spec");

*menu1_p4_b8.class: separatorGadget
*menu1_p4_b8.parent: menu1_p4
*menu1_p4_b8.static: true
*menu1_p4_b8.name: menu1_p4_b8

*menu1_p4_b10.class: pushButtonGadget
*menu1_p4_b10.parent: menu1_p4
*menu1_p4_b10.static: true
*menu1_p4_b10.name: menu1_p4_b10
*menu1_p4_b10.labelString: "Load image"
*menu1_p4_b10.activateCallback: {        \
#include <spec_comm.h>\
\
PopupList(LIST_LOAD_IMA);\
}
*menu1_p4_b10.fontList: BoldTextFont
*menu1_p4_b10.mnemonic: "L"

*menu1_p4_b13.class: pushButtonGadget
*menu1_p4_b13.parent: menu1_p4
*menu1_p4_b13.static: true
*menu1_p4_b13.name: menu1_p4_b13
*menu1_p4_b13.labelString: "Clear overlay"
*menu1_p4_b13.activateCallback: AppendDialogText("clear/channel over");
*menu1_p4_b13.fontList: BoldTextFont
*menu1_p4_b13.mnemonic: "o"

*menu1_p4_b12.class: separatorGadget
*menu1_p4_b12.parent: menu1_p4
*menu1_p4_b12.static: true
*menu1_p4_b12.name: menu1_p4_b12

*menu1_p4_b3.class: pushButtonGadget
*menu1_p4_b3.parent: menu1_p4
*menu1_p4_b3.static: true
*menu1_p4_b3.name: menu1_p4_b3
*menu1_p4_b3.labelString: "Print Display"
*menu1_p4_b3.mnemonic: "P"
*menu1_p4_b3.fontList: BoldTextFont
*menu1_p4_b3.activateCallback: AppendDialogText("copy/display laser");

*menu1_p4_b4.class: pushButtonGadget
*menu1_p4_b4.parent: menu1_p4
*menu1_p4_b4.static: true
*menu1_p4_b4.name: menu1_p4_b4
*menu1_p4_b4.labelString: "Print Graphics"
*menu1_p4_b4.mnemonic: "r"
*menu1_p4_b4.fontList: BoldTextFont
*menu1_p4_b4.activateCallback: AppendDialogText("copy/graph laser");

*menu1_p4_b9.class: separatorGadget
*menu1_p4_b9.parent: menu1_p4
*menu1_p4_b9.static: true
*menu1_p4_b9.name: menu1_p4_b9

*menu1_p4_b5.class: pushButtonGadget
*menu1_p4_b5.parent: menu1_p4
*menu1_p4_b5.static: true
*menu1_p4_b5.name: menu1_p4_b5
*menu1_p4_b5.labelString: "Graphics Cursor"
*menu1_p4_b5.mnemonic: "C"
*menu1_p4_b5.fontList: BoldTextFont
*menu1_p4_b5.activateCallback: AppendDialogText("get/gcursor");

*menu1_p4_b11.class: pushButtonGadget
*menu1_p4_b11.parent: menu1_p4
*menu1_p4_b11.static: true
*menu1_p4_b11.name: menu1_p4_b11
*menu1_p4_b11.labelString: "Display Cursor"
*menu1_p4_b11.mnemonic: "u"
*menu1_p4_b11.activateCallback: AppendDialogText("get/cursor");
*menu1_p4_b11.fontList: BoldTextFont

*menu1_p4_b14.class: separatorGadget
*menu1_p4_b14.parent: menu1_p4
*menu1_p4_b14.static: true
*menu1_p4_b14.name: menu1_p4_b14

*menu1_p4_b15.class: pushButtonGadget
*menu1_p4_b15.parent: menu1_p4
*menu1_p4_b15.static: true
*menu1_p4_b15.name: menu1_p4_b15
*menu1_p4_b15.labelString: "get calib. table"
*menu1_p4_b15.activateCallback: {\
#include <spec_comm.h>\
\
char midashome[256];\
\
osfphname("MIDASHOME", midashome);\
sprintf(DirSpecs, "%s/calib/data/", midashome);\
\
PopupList(LIST_BROWSER);\
}
*menu1_p4_b15.fontList: BoldTextFont

*menu1_p5.class: rowColumn
*menu1_p5.parent: menu1
*menu1_p5.static: true
*menu1_p5.name: menu1_p5
*menu1_p5.rowColumnType: "menu_pulldown"
*menu1_p5.background: MenuBackground
*menu1_p5.foreground: MenuForeground

*menu_help_context.class: pushButtonGadget
*menu_help_context.parent: menu1_p5
*menu_help_context.static: true
*menu_help_context.name: menu_help_context
*menu_help_context.labelString: "On Context ..."
*menu_help_context.mnemonic: "C"
*menu_help_context.fontList: BoldTextFont
*menu_help_context.activateCallback: DisplayExtendedHelp(UxWidget);

*menu_help_help.class: pushButtonGadget
*menu_help_help.parent: menu1_p5
*menu_help_help.static: true
*menu_help_help.name: menu_help_help
*menu_help_help.labelString: "On Help ..."
*menu_help_help.mnemonic: "H"
*menu_help_help.fontList: BoldTextFont
*menu_help_help.activateCallback: DisplayExtendedHelp(UxWidget);

*menu1_p5_b5.class: separatorGadget
*menu1_p5_b5.parent: menu1_p5
*menu1_p5_b5.static: true
*menu1_p5_b5.name: menu1_p5_b5

*menu_help_tutorial.class: pushButtonGadget
*menu_help_tutorial.parent: menu1_p5
*menu_help_tutorial.static: true
*menu_help_tutorial.name: menu_help_tutorial
*menu_help_tutorial.labelString: "Tutorial"
*menu_help_tutorial.mnemonic: "T"
*menu_help_tutorial.fontList: BoldTextFont
*menu_help_tutorial.activateCallback: DisplayExtendedHelp(UxWidget);

*menu_help_version.class: pushButtonGadget
*menu_help_version.parent: menu1_p5
*menu_help_version.static: true
*menu_help_version.name: menu_help_version
*menu_help_version.labelString: "On Version..."
*menu_help_version.mnemonic: "V"
*menu_help_version.fontList: BoldTextFont
*menu_help_version.activateCallback: DisplayExtendedHelp(UxWidget);

*menu1_top_b1.class: cascadeButton
*menu1_top_b1.parent: menu1
*menu1_top_b1.static: true
*menu1_top_b1.name: menu1_top_b1
*menu1_top_b1.labelString: "File"
*menu1_top_b1.mnemonic: "F"
*menu1_top_b1.subMenuId: "menu1_p1"
*menu1_top_b1.background: MenuBackground
*menu1_top_b1.fontList: BoldTextFont
*menu1_top_b1.foreground: MenuForeground

*menu1_top_b4.class: cascadeButton
*menu1_top_b4.parent: menu1
*menu1_top_b4.static: true
*menu1_top_b4.name: menu1_top_b4
*menu1_top_b4.labelString: "Utils"
*menu1_top_b4.mnemonic: "U"
*menu1_top_b4.subMenuId: "menu1_p4"
*menu1_top_b4.background: MenuBackground
*menu1_top_b4.fontList: BoldTextFont
*menu1_top_b4.foreground: MenuForeground

*menu1_top_b5.class: cascadeButton
*menu1_top_b5.parent: menu1
*menu1_top_b5.static: true
*menu1_top_b5.name: menu1_top_b5
*menu1_top_b5.labelString: "Help"
*menu1_top_b5.mnemonic: "H"
*menu1_top_b5.subMenuId: "menu1_p5"
*menu1_top_b5.background: MenuBackground
*menu1_top_b5.fontList: BoldTextFont
*menu1_top_b5.foreground: MenuForeground

*form1.class: form
*form1.parent: MainWindow
*form1.static: true
*form1.name: form1
*form1.background: WindowBackground

*form5.class: form
*form5.parent: form1
*form5.static: true
*form5.name: form5
*form5.resizePolicy: "resize_none"
*form5.x: -1
*form5.y: 368
*form5.width: 451
*form5.height: 40
*form5.background: ButtonBackground
*form5.bottomAttachment: "attach_form"
*form5.leftAttachment: "attach_form"
*form5.leftOffset: 0
*form5.rightAttachment: "attach_form"
*form5.rightOffset: 0
*form5.bottomOffset: 0
*form5.topAttachment: "attach_none"
*form5.topOffset: 60

*pb_main_batch.class: pushButton
*pb_main_batch.parent: form5
*pb_main_batch.static: true
*pb_main_batch.name: pb_main_batch
*pb_main_batch.x: 96
*pb_main_batch.y: 2
*pb_main_batch.width: 86
*pb_main_batch.height: 30
*pb_main_batch.background: ButtonBackground
*pb_main_batch.fontList: BoldTextFont
*pb_main_batch.foreground: ButtonForeground
*pb_main_batch.labelString: "Reduce"
*pb_main_batch.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}
*pb_main_batch.leftAttachment: "attach_form"
*pb_main_batch.leftOffset: 5
*pb_main_batch.leftWidget: ""
*pb_main_batch.bottomAttachment: "attach_form"
*pb_main_batch.bottomOffset: 5

*pb_main_ident.class: pushButton
*pb_main_ident.parent: form5
*pb_main_ident.static: true
*pb_main_ident.name: pb_main_ident
*pb_main_ident.x: 30
*pb_main_ident.y: 4
*pb_main_ident.width: 86
*pb_main_ident.height: 30
*pb_main_ident.background: ButtonBackground
*pb_main_ident.fontList: BoldTextFont
*pb_main_ident.foreground: ButtonForeground
*pb_main_ident.labelString: "Rotate"
*pb_main_ident.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}
*pb_main_ident.bottomAttachment: "attach_form"
*pb_main_ident.bottomOffset: 5
*pb_main_ident.leftAttachment: "attach_widget"
*pb_main_ident.leftOffset: 5
*pb_main_ident.leftWidget: "pb_main_batch"

*pb_main_batch1.class: pushButton
*pb_main_batch1.parent: form5
*pb_main_batch1.static: true
*pb_main_batch1.name: pb_main_batch1
*pb_main_batch1.x: 184
*pb_main_batch1.y: 2
*pb_main_batch1.width: 120
*pb_main_batch1.height: 30
*pb_main_batch1.background: ButtonBackground
*pb_main_batch1.fontList: BoldTextFont
*pb_main_batch1.foreground: ButtonForeground
*pb_main_batch1.labelString: "Load Orders"
*pb_main_batch1.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}
*pb_main_batch1.leftAttachment: "attach_widget"
*pb_main_batch1.leftOffset: 5
*pb_main_batch1.leftWidget: "pb_main_ident"
*pb_main_batch1.bottomAttachment: "attach_form"
*pb_main_batch1.bottomOffset: 5

*pb_main_batch2.class: pushButton
*pb_main_batch2.parent: form5
*pb_main_batch2.static: true
*pb_main_batch2.name: pb_main_batch2
*pb_main_batch2.x: 306
*pb_main_batch2.y: 2
*pb_main_batch2.width: 120
*pb_main_batch2.height: 30
*pb_main_batch2.background: ButtonBackground
*pb_main_batch2.fontList: BoldTextFont
*pb_main_batch2.foreground: ButtonForeground
*pb_main_batch2.labelString: "Save"
*pb_main_batch2.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}
*pb_main_batch2.leftAttachment: "attach_widget"
*pb_main_batch2.leftOffset: 5
*pb_main_batch2.leftWidget: "pb_main_batch1"
*pb_main_batch2.bottomAttachment: "attach_form"
*pb_main_batch2.bottomOffset: 5

*pb_main_batch3.class: pushButton
*pb_main_batch3.parent: form5
*pb_main_batch3.static: true
*pb_main_batch3.name: pb_main_batch3
*pb_main_batch3.x: 686
*pb_main_batch3.y: 2
*pb_main_batch3.width: 119
*pb_main_batch3.height: 30
*pb_main_batch3.background: ButtonBackground
*pb_main_batch3.fontList: BoldTextFont
*pb_main_batch3.foreground: ButtonForeground
*pb_main_batch3.labelString: "Close Window"
*pb_main_batch3.activateCallback: {\
{exit(0);}\
}
*pb_main_batch3.leftAttachment: "attach_none"
*pb_main_batch3.leftOffset: 5
*pb_main_batch3.leftWidget: "pb_main_batch2"
*pb_main_batch3.rightAttachment: "attach_form"
*pb_main_batch3.rightOffset: 5
*pb_main_batch3.bottomAttachment: "attach_form"
*pb_main_batch3.bottomOffset: 5

*separator12.class: separator
*separator12.parent: form1
*separator12.static: true
*separator12.name: separator12
*separator12.x: 0
*separator12.y: 360
*separator12.width: 450
*separator12.height: 6
*separator12.background: LabelBackground
*separator12.bottomAttachment: "attach_widget"
*separator12.bottomWidget: "form5"
*separator12.leftAttachment: "attach_form"
*separator12.rightAttachment: "attach_form"
*separator12.rightOffset: 0
*separator12.leftOffset: 0

*shelp_main.class: text
*shelp_main.parent: form1
*shelp_main.static: true
*shelp_main.name: shelp_main
*shelp_main.x: 0
*shelp_main.y: 310
*shelp_main.width: 448
*shelp_main.height: 50
*shelp_main.background: SHelpBackground
*shelp_main.cursorPositionVisible: "false"
*shelp_main.editable: "false"
*shelp_main.fontList: TextFont
*shelp_main.bottomAttachment: "attach_widget"
*shelp_main.bottomWidget: "separator12"
*shelp_main.leftAttachment: "attach_form"
*shelp_main.leftOffset: 5
*shelp_main.rightAttachment: "attach_form"
*shelp_main.rightOffset: 5

*separator1.class: separator
*separator1.parent: form1
*separator1.static: true
*separator1.name: separator1
*separator1.x: -2
*separator1.y: 304
*separator1.width: 450
*separator1.height: 6
*separator1.background: LabelBackground
*separator1.bottomAttachment: "attach_widget"
*separator1.bottomWidget: "shelp_main"
*separator1.leftAttachment: "attach_form"
*separator1.leftOffset: 0
*separator1.rightAttachment: "attach_form"
*separator1.rightOffset: 0

*form22.class: form
*form22.parent: form1
*form22.static: true
*form22.name: form22
*form22.resizePolicy: "resize_none"
*form22.x: 250
*form22.y: 10
*form22.width: 526
*form22.height: 534
*form22.background: WindowBackground
*form22.leftAttachment: "attach_form"
*form22.leftOffset: 250
*form22.rightAttachment: "attach_form"
*form22.rightOffset: 5
*form22.topAttachment: "attach_form"
*form22.topOffset: 5
*form22.bottomAttachment: "attach_widget"
*form22.bottomOffset: 5
*form22.bottomWidget: "separator1"
*form22.shadowThickness: 3
*form22.shadowType: "shadow_in"

*ListWindow1.class: scrolledWindow
*ListWindow1.parent: form22
*ListWindow1.static: true
*ListWindow1.name: ListWindow1
*ListWindow1.shadowThickness: 0
*ListWindow1.background: WindowBackground
*ListWindow1.borderColor: WindowBackground
*ListWindow1.bottomShadowColor: WindowBackground
*ListWindow1.foreground: TextForeground
*ListWindow1.scrollBarPlacement: "bottom_right"
*ListWindow1.x: 130
*ListWindow1.y: 368
*ListWindow1.leftAttachment: "attach_form"
*ListWindow1.leftOffset: 10
*ListWindow1.rightAttachment: "attach_form"
*ListWindow1.rightOffset: 350
*ListWindow1.bottomAttachment: "attach_form"
*ListWindow1.bottomOffset: 60
*ListWindow1.topAttachment: "attach_form"
*ListWindow1.topOffset: 160
*ListWindow1.highlightColor: "black"
*ListWindow1.topShadowColor: WindowBackground

*scrolledList1.class: scrolledText
*scrolledList1.parent: ListWindow1
*scrolledList1.static: true
*scrolledList1.name: scrolledList1
*scrolledList1.background: SHelpBackground
*scrolledList1.borderColor: SHelpBackground
*scrolledList1.fontList: "9x15"
*scrolledList1.foreground: TextForeground
*scrolledList1.topShadowColor: "Black"
*scrolledList1.selectionArray: "select_line select_line select_line select_line"
*scrolledList1.translations: DirList

*tf_wrang1.class: textField
*tf_wrang1.parent: form22
*tf_wrang1.static: true
*tf_wrang1.name: tf_wrang1
*tf_wrang1.x: 100
*tf_wrang1.y: 230
*tf_wrang1.width: 250
*tf_wrang1.height: 34
*tf_wrang1.background: TextBackground
*tf_wrang1.fontList: TextFont
*tf_wrang1.highlightOnEnter: "true"
*tf_wrang1.foreground: TextForeground
*tf_wrang1.losingFocusCallback: {\
\
}
*tf_wrang1.topAttachment: "attach_none"
*tf_wrang1.topOffset: 40
*tf_wrang1.topWidget: "ListWindow1"
*tf_wrang1.leftAttachment: "attach_form"
*tf_wrang1.leftOffset: 110
*tf_wrang1.leftPosition: 100
*tf_wrang1.rightAttachment: "attach_form"
*tf_wrang1.rightOffset: 220
*tf_wrang1.bottomAttachment: "attach_form"
*tf_wrang1.bottomOffset: 5
*tf_wrang1.text: "esp"

*label3.class: label
*label3.parent: form22
*label3.static: true
*label3.name: label3
*label3.x: 10
*label3.y: 492
*label3.width: 132
*label3.height: 36
*label3.background: WindowBackground
*label3.fontList: BoldTextFont
*label3.foreground: TextForeground
*label3.labelString: "Output Name:"
*label3.topAttachment: "attach_none"
*label3.topOffset: 40
*label3.topWidget: "ListWindow1"
*label3.leftAttachment: "attach_form"
*label3.leftOffset: 5
*label3.rightOffset: 5
*label3.alignment: "alignment_beginning"
*label3.bottomAttachment: "attach_form"
*label3.bottomOffset: 5

*label4.class: label
*label4.parent: form22
*label4.static: true
*label4.name: label4
*label4.x: 336
*label4.y: 492
*label4.width: 105
*label4.height: 36
*label4.background: WindowBackground
*label4.fontList: BoldTextFont
*label4.foreground: TextForeground
*label4.labelString: "Start Number:"
*label4.topAttachment: "attach_none"
*label4.topOffset: 40
*label4.topWidget: "ListWindow1"
*label4.alignment: "alignment_beginning"
*label4.leftAttachment: "attach_widget"
*label4.leftOffset: 5
*label4.rightAttachment: "attach_form"
*label4.rightOffset: 110
*label4.leftWidget: "tf_wrang1"
*label4.bottomAttachment: "attach_form"
*label4.bottomOffset: 5

*tf_wrang2.class: textField
*tf_wrang2.parent: form22
*tf_wrang2.static: true
*tf_wrang2.name: tf_wrang2
*tf_wrang2.x: 450
*tf_wrang2.y: 230
*tf_wrang2.width: 100
*tf_wrang2.height: 34
*tf_wrang2.background: TextBackground
*tf_wrang2.fontList: TextFont
*tf_wrang2.highlightOnEnter: "true"
*tf_wrang2.foreground: TextForeground
*tf_wrang2.losingFocusCallback: {\
\
}
*tf_wrang2.topAttachment: "attach_none"
*tf_wrang2.topOffset: 40
*tf_wrang2.topWidget: "ListWindow1"
*tf_wrang2.leftAttachment: "attach_widget"
*tf_wrang2.leftOffset: 5
*tf_wrang2.leftWidget: "label4"
*tf_wrang2.rightAttachment: "attach_form"
*tf_wrang2.rightOffset: 5
*tf_wrang2.bottomAttachment: "attach_form"
*tf_wrang2.bottomOffset: 5
*tf_wrang2.text: "1"

*form23.class: form
*form23.parent: form22
*form23.static: true
*form23.name: form23
*form23.resizePolicy: "resize_none"
*form23.x: 8
*form23.y: 54
*form23.width: 544
*form23.height: 40
*form23.leftAttachment: "attach_form"
*form23.leftOffset: 4
*form23.rightAttachment: "attach_form"
*form23.rightOffset: 4
*form23.background: WindowBackground
*form23.topOffset: 60
*form23.topAttachment: "attach_form"

*label2.class: label
*label2.parent: form23
*label2.static: true
*label2.name: label2
*label2.x: 20
*label2.y: 4
*label2.width: 80
*label2.height: 36
*label2.background: WindowBackground
*label2.fontList: BoldTextFont
*label2.foreground: TextForeground
*label2.labelString: "Files:"
*label2.leftAttachment: "attach_form"
*label2.leftOffset: 5

*tf_session.class: textField
*tf_session.parent: form23
*tf_session.static: true
*tf_session.name: tf_session
*tf_session.x: 112
*tf_session.y: 2
*tf_session.width: 235
*tf_session.height: 34
*tf_session.background: TextBackground
*tf_session.fontList: TextFont
*tf_session.highlightOnEnter: "true"
*tf_session.foreground: TextForeground
*tf_session.cursorPositionVisible: "true"
*tf_session.editable: "true"
*tf_session.borderWidth: 1
*tf_session.leftAttachment: "attach_form"
*tf_session.leftOffset: 90
*tf_session.rightAttachment: "attach_form"
*tf_session.rightOffset: 85
*tf_session.text: "*.bdf"

*pb_main_batch4.class: pushButton
*pb_main_batch4.parent: form23
*pb_main_batch4.static: true
*pb_main_batch4.name: pb_main_batch4
*pb_main_batch4.x: 394
*pb_main_batch4.y: 6
*pb_main_batch4.width: 75
*pb_main_batch4.height: 30
*pb_main_batch4.background: ButtonBackground
*pb_main_batch4.fontList: BoldTextFont
*pb_main_batch4.foreground: ButtonForeground
*pb_main_batch4.labelString: "Filter"
*pb_main_batch4.activateCallback: {\
{\
\
char node[200];\
\
strcpy(node,UxGetText(tf_session1));\
WGet_all_dirs(node);\
WChange_Midas_dir();\
WGet_all_files();\
\
}\
}
*pb_main_batch4.rightAttachment: "attach_form"
*pb_main_batch4.rightOffset: 5

*form24.class: form
*form24.parent: form22
*form24.static: true
*form24.name: form24
*form24.resizePolicy: "resize_none"
*form24.x: 6
*form24.y: 8
*form24.width: 544
*form24.height: 40
*form24.background: WindowBackground
*form24.topAttachment: "attach_form"
*form24.topOffset: 10

*label5.class: label
*label5.parent: form24
*label5.static: true
*label5.name: label5
*label5.x: 20
*label5.y: 4
*label5.width: 80
*label5.height: 36
*label5.background: WindowBackground
*label5.fontList: BoldTextFont
*label5.foreground: TextForeground
*label5.labelString: "Directory:"
*label5.leftAttachment: "attach_form"
*label5.leftOffset: 6

*tf_session1.class: textField
*tf_session1.parent: form24
*tf_session1.static: true
*tf_session1.name: tf_session1
*tf_session1.x: 100
*tf_session1.y: 2
*tf_session1.width: 235
*tf_session1.height: 34
*tf_session1.background: TextBackground
*tf_session1.fontList: TextFont
*tf_session1.highlightOnEnter: "true"
*tf_session1.foreground: TextForeground
*tf_session1.cursorPositionVisible: "true"
*tf_session1.editable: "true"
*tf_session1.borderWidth: 1
*tf_session1.leftAttachment: "attach_form"
*tf_session1.leftOffset: 90
*tf_session1.rightAttachment: "attach_form"
*tf_session1.rightOffset: 80
*tf_session1.text: "."

*label32.class: label
*label32.parent: form22
*label32.static: true
*label32.name: label32
*label32.x: 32
*label32.y: 114
*label32.width: 132
*label32.height: 36
*label32.background: WindowBackground
*label32.fontList: BoldTextFont
*label32.foreground: TextForeground
*label32.labelString: "Directories:"
*label32.alignment: "alignment_beginning"
*label32.topAttachment: "attach_form"
*label32.topOffset: 110
*label32.leftAttachment: "attach_form"
*label32.leftOffset: 10

*label39.class: label
*label39.parent: form22
*label39.static: true
*label39.name: label39
*label39.x: 232
*label39.y: 114
*label39.width: 132
*label39.height: 36
*label39.background: WindowBackground
*label39.fontList: BoldTextFont
*label39.foreground: TextForeground
*label39.labelString: "Files:"
*label39.alignment: "alignment_beginning"
*label39.leftAttachment: "attach_form"
*label39.leftOffset: 230
*label39.topAttachment: "attach_form"
*label39.topOffset: 110

*scrolledWindow4.class: scrolledWindow
*scrolledWindow4.parent: form22
*scrolledWindow4.static: true
*scrolledWindow4.name: scrolledWindow4
*scrolledWindow4.x: 215
*scrolledWindow4.y: -92
*scrolledWindow4.shadowThickness: 0
*scrolledWindow4.background: WindowBackground
*scrolledWindow4.bottomAttachment: "attach_form"
*scrolledWindow4.bottomOffset: 60
*scrolledWindow4.leftAttachment: "attach_widget"
*scrolledWindow4.leftOffset: 10
*scrolledWindow4.leftWidget: "ListWindow1"
*scrolledWindow4.topAttachment: "attach_form"
*scrolledWindow4.topOffset: 160
*scrolledWindow4.rightAttachment: "attach_form"
*scrolledWindow4.rightOffset: 10
*scrolledWindow4.scrollBarPlacement: "bottom_right"

*scrolledList4.class: scrolledList
*scrolledList4.parent: scrolledWindow4
*scrolledList4.static: true
*scrolledList4.name: scrolledList4
*scrolledList4.width: 311
*scrolledList4.height: 597
*scrolledList4.background: SHelpBackground
*scrolledList4.fontList: "9x15"
*scrolledList4.listSizePolicy: "constant"
*scrolledList4.scrollBarDisplayPolicy: "static"
*scrolledList4.selectionPolicy: "multiple_select"
*scrolledList4.borderColor: SHelpBackground
*scrolledList4.createCallback: {\
\
}

*rowColumn11.class: rowColumn
*rowColumn11.parent: form1
*rowColumn11.static: true
*rowColumn11.name: rowColumn11
*rowColumn11.x: 270
*rowColumn11.y: 20
*rowColumn11.width: 292
*rowColumn11.height: 422
*rowColumn11.background: WindowBackground
*rowColumn11.packing: "pack_tight"
*rowColumn11.resizeHeight: "true"
*rowColumn11.marginHeight: 0
*rowColumn11.leftAttachment: "attach_form"
*rowColumn11.leftOffset: 5
*rowColumn11.topAttachment: "attach_form"
*rowColumn11.topOffset: 10
*rowColumn11.bottomAttachment: "attach_widget"
*rowColumn11.bottomOffset: 5
*rowColumn11.bottomWidget: "separator1"

*label7.class: label
*label7.parent: rowColumn11
*label7.static: true
*label7.name: label7
*label7.x: 6
*label7.y: 68
*label7.width: 190
*label7.height: 30
*label7.background: LabelBackground
*label7.fontList: BoldTextFont
*label7.labelString: "Calibration"
*label7.alignment: "alignment_beginning"
*label7.foreground: TextForeground
*label7.marginLeft: 60

*rowColumn13.class: rowColumn
*rowColumn13.parent: rowColumn11
*rowColumn13.static: true
*rowColumn13.name: rowColumn13
*rowColumn13.x: -7
*rowColumn13.y: 225
*rowColumn13.width: 250
*rowColumn13.height: 20
*rowColumn13.background: WindowBackground
*rowColumn13.orientation: "horizontal"
*rowColumn13.packing: "pack_tight"
*rowColumn13.resizeWidth: "false"
*rowColumn13.adjustLast: "true"

*toggleButton5.class: label
*toggleButton5.parent: rowColumn13
*toggleButton5.static: true
*toggleButton5.name: toggleButton5
*toggleButton5.x: 3
*toggleButton5.y: -4
*toggleButton5.width: 195
*toggleButton5.height: 20
*toggleButton5.recomputeSize: "false"
*toggleButton5.background: WindowBackground
*toggleButton5.fontList: TextFont
*toggleButton5.foreground: TextForeground
*toggleButton5.labelString: "Geometry and Rotation"

*pb_main_search4.class: pushButton
*pb_main_search4.parent: rowColumn13
*pb_main_search4.static: true
*pb_main_search4.name: pb_main_search4
*pb_main_search4.x: 3
*pb_main_search4.y: 3
*pb_main_search4.height: 25
*pb_main_search4.background: ButtonBackground
*pb_main_search4.fontList: BoldTextFont
*pb_main_search4.foreground: ButtonForeground
*pb_main_search4.labelString: "..."
*pb_main_search4.activateCallback: {\
PopupLong("RotateShell");\
}
*pb_main_search4.recomputeSize: "false"

*rowColumn14.class: rowColumn
*rowColumn14.parent: rowColumn11
*rowColumn14.static: true
*rowColumn14.name: rowColumn14
*rowColumn14.x: -3
*rowColumn14.y: 189
*rowColumn14.width: 250
*rowColumn14.height: 20
*rowColumn14.background: WindowBackground
*rowColumn14.orientation: "horizontal"
*rowColumn14.packing: "pack_tight"
*rowColumn14.resizeWidth: "false"
*rowColumn14.adjustLast: "true"

*toggleButton6.class: label
*toggleButton6.parent: rowColumn14
*toggleButton6.static: true
*toggleButton6.name: toggleButton6
*toggleButton6.x: 3
*toggleButton6.y: -4
*toggleButton6.width: 195
*toggleButton6.height: 20
*toggleButton6.recomputeSize: "false"
*toggleButton6.background: WindowBackground
*toggleButton6.fontList: TextFont
*toggleButton6.foreground: TextForeground
*toggleButton6.labelString: "Order Definition"

*pb_main_search5.class: pushButton
*pb_main_search5.parent: rowColumn14
*pb_main_search5.static: true
*pb_main_search5.name: pb_main_search5
*pb_main_search5.x: 3
*pb_main_search5.y: 3
*pb_main_search5.height: 25
*pb_main_search5.background: ButtonBackground
*pb_main_search5.fontList: BoldTextFont
*pb_main_search5.foreground: ButtonForeground
*pb_main_search5.labelString: "..."
*pb_main_search5.activateCallback: {\
PopupLong("OrderShell");\
}
*pb_main_search5.recomputeSize: "false"

*rowColumn15.class: rowColumn
*rowColumn15.parent: rowColumn11
*rowColumn15.static: true
*rowColumn15.name: rowColumn15
*rowColumn15.x: 2
*rowColumn15.y: 2
*rowColumn15.width: 250
*rowColumn15.height: 20
*rowColumn15.background: WindowBackground
*rowColumn15.orientation: "horizontal"
*rowColumn15.packing: "pack_tight"
*rowColumn15.resizeWidth: "false"
*rowColumn15.adjustLast: "true"

*toggleButton7.class: label
*toggleButton7.parent: rowColumn15
*toggleButton7.static: true
*toggleButton7.name: toggleButton7
*toggleButton7.x: 3
*toggleButton7.y: -4
*toggleButton7.width: 195
*toggleButton7.height: 20
*toggleButton7.recomputeSize: "false"
*toggleButton7.background: WindowBackground
*toggleButton7.fontList: TextFont
*toggleButton7.foreground: TextForeground
*toggleButton7.labelString: "Arc Line Search"

*pb_main_search6.class: pushButton
*pb_main_search6.parent: rowColumn15
*pb_main_search6.static: true
*pb_main_search6.name: pb_main_search6
*pb_main_search6.x: 3
*pb_main_search6.y: 3
*pb_main_search6.height: 25
*pb_main_search6.background: ButtonBackground
*pb_main_search6.fontList: BoldTextFont
*pb_main_search6.foreground: ButtonForeground
*pb_main_search6.labelString: "..."
*pb_main_search6.activateCallback: {\
PopupLong("SearchShell");\
}
*pb_main_search6.recomputeSize: "false"

*rowColumn16.class: rowColumn
*rowColumn16.parent: rowColumn11
*rowColumn16.static: true
*rowColumn16.name: rowColumn16
*rowColumn16.x: 3
*rowColumn16.y: 137
*rowColumn16.width: 250
*rowColumn16.height: 20
*rowColumn16.background: WindowBackground
*rowColumn16.orientation: "horizontal"
*rowColumn16.packing: "pack_tight"
*rowColumn16.resizeWidth: "false"
*rowColumn16.adjustLast: "true"

*toggleButton8.class: label
*toggleButton8.parent: rowColumn16
*toggleButton8.static: true
*toggleButton8.name: toggleButton8
*toggleButton8.x: 3
*toggleButton8.y: -4
*toggleButton8.width: 195
*toggleButton8.height: 20
*toggleButton8.recomputeSize: "false"
*toggleButton8.background: WindowBackground
*toggleButton8.fontList: TextFont
*toggleButton8.foreground: TextForeground
*toggleButton8.labelString: "Wavelength Calibration"

*pb_main_search7.class: pushButton
*pb_main_search7.parent: rowColumn16
*pb_main_search7.static: true
*pb_main_search7.name: pb_main_search7
*pb_main_search7.x: 3
*pb_main_search7.y: 3
*pb_main_search7.height: 25
*pb_main_search7.background: ButtonBackground
*pb_main_search7.fontList: BoldTextFont
*pb_main_search7.foreground: ButtonForeground
*pb_main_search7.labelString: "..."
*pb_main_search7.activateCallback: {\
PopupLong("CalibShell");\
}
*pb_main_search7.recomputeSize: "false"

*rowColumn17.class: rowColumn
*rowColumn17.parent: rowColumn11
*rowColumn17.static: true
*rowColumn17.name: rowColumn17
*rowColumn17.x: 2
*rowColumn17.y: 24
*rowColumn17.width: 250
*rowColumn17.height: 20
*rowColumn17.background: WindowBackground
*rowColumn17.orientation: "horizontal"
*rowColumn17.packing: "pack_tight"
*rowColumn17.resizeWidth: "false"
*rowColumn17.adjustLast: "true"

*toggleButton9.class: label
*toggleButton9.parent: rowColumn17
*toggleButton9.static: true
*toggleButton9.name: toggleButton9
*toggleButton9.x: 3
*toggleButton9.y: -4
*toggleButton9.width: 120
*toggleButton9.height: 20
*toggleButton9.recomputeSize: "false"
*toggleButton9.background: WindowBackground
*toggleButton9.fontList: TextFont
*toggleButton9.foreground: TextForeground
*toggleButton9.labelString: "EMMI Mode:"

*mn_tol7.class: rowColumn
*mn_tol7.parent: rowColumn17
*mn_tol7.static: true
*mn_tol7.name: mn_tol7
*mn_tol7.rowColumnType: "menu_option"
*mn_tol7.subMenuId: "menu2_p7"
*mn_tol7.x: 474
*mn_tol7.y: 76
*mn_tol7.background: WindowBackground
*mn_tol7.foreground: TextForeground
*mn_tol7.marginWidth: 0
*mn_tol7.spacing: 0
*mn_tol7.labelString: " "
*mn_tol7.width: 120
*mn_tol7.translations: transTable2

*menu2_p7.class: rowColumn
*menu2_p7.parent: mn_tol7
*menu2_p7.static: true
*menu2_p7.name: menu2_p7
*menu2_p7.rowColumnType: "menu_pulldown"
*menu2_p7.background: WindowBackground
*menu2_p7.foreground: TextForeground
*menu2_p7.width: 120
*menu2_p7.x: 520

*mn_tol_angstroms7.class: pushButtonGadget
*mn_tol_angstroms7.parent: menu2_p7
*mn_tol_angstroms7.static: true
*mn_tol_angstroms7.name: mn_tol_angstroms7
*mn_tol_angstroms7.labelString: "#9CD3"
*mn_tol_angstroms7.activateCallback: {RadioSet(UxWidget);}
*mn_tol_angstroms7.fontList: TextFont
*mn_tol_angstroms7.width: 120
*mn_tol_angstroms7.x: 520

*mn_tol_pixels7.class: pushButtonGadget
*mn_tol_pixels7.parent: menu2_p7
*mn_tol_pixels7.static: true
*mn_tol_pixels7.name: mn_tol_pixels7
*mn_tol_pixels7.labelString: "#9CD4"
*mn_tol_pixels7.activateCallback: {RadioSet(UxWidget);}
*mn_tol_pixels7.fontList: TextFont
*mn_tol_pixels7.width: 120
*mn_tol_pixels7.x: 520

*menu2_p7_b3.class: pushButtonGadget
*menu2_p7_b3.parent: menu2_p7
*menu2_p7_b3.static: true
*menu2_p7_b3.name: menu2_p7_b3
*menu2_p7_b3.labelString: "#10CD3"
*menu2_p7_b3.fontList: TextFont
*menu2_p7_b3.activateCallback: {RadioSet(UxWidget);}

*menu2_p7_b4.class: pushButtonGadget
*menu2_p7_b4.parent: menu2_p7
*menu2_p7_b4.static: true
*menu2_p7_b4.name: menu2_p7_b4
*menu2_p7_b4.labelString: "#10CD4"
*menu2_p7_b4.fontList: TextFont
*menu2_p7_b4.activateCallback: {RadioSet(UxWidget);}

*menu2_p7_b5.class: pushButtonGadget
*menu2_p7_b5.parent: menu2_p7
*menu2_p7_b5.static: true
*menu2_p7_b5.name: menu2_p7_b5
*menu2_p7_b5.labelString: "#10CD5"
*menu2_p7_b5.fontList: TextFont
*menu2_p7_b5.activateCallback: {RadioSet(UxWidget);}

*menu2_p7_b6.class: pushButtonGadget
*menu2_p7_b6.parent: menu2_p7
*menu2_p7_b6.static: true
*menu2_p7_b6.name: menu2_p7_b6
*menu2_p7_b6.labelString: "#10CD6"
*menu2_p7_b6.fontList: TextFont
*menu2_p7_b6.activateCallback: {RadioSet(UxWidget);}

*label1.class: label
*label1.parent: rowColumn11
*label1.static: true
*label1.name: label1
*label1.x: 6
*label1.y: 68
*label1.width: 190
*label1.height: 30
*label1.background: LabelBackground
*label1.fontList: BoldTextFont
*label1.labelString: "Reduction"
*label1.alignment: "alignment_beginning"
*label1.foreground: TextForeground

*rowColumn18.class: rowColumn
*rowColumn18.parent: rowColumn11
*rowColumn18.static: true
*rowColumn18.name: rowColumn18
*rowColumn18.x: 2
*rowColumn18.y: 24
*rowColumn18.width: 250
*rowColumn18.height: 20
*rowColumn18.background: WindowBackground
*rowColumn18.orientation: "horizontal"
*rowColumn18.packing: "pack_tight"
*rowColumn18.resizeWidth: "false"
*rowColumn18.adjustLast: "true"

*toggleButton3.class: toggleButton
*toggleButton3.parent: rowColumn18
*toggleButton3.static: true
*toggleButton3.name: toggleButton3
*toggleButton3.x: 3
*toggleButton3.y: -4
*toggleButton3.width: 195
*toggleButton3.height: 20
*toggleButton3.recomputeSize: "false"
*toggleButton3.background: WindowBackground
*toggleButton3.fontList: TextFont
*toggleButton3.foreground: TextForeground
*toggleButton3.labelString: "Rotation"
*toggleButton3.selectColor: "Yellow"
*toggleButton3.valueChangedCallback: {\
if (CheckStatus(UxThisWidget) == TRUE)\
    AppendDialogText("SET/ECHEL ROTOPT=YES");\
else\
    AppendDialogText("SET/ECHEL ROTOPT=NO");\
\
}
*toggleButton3.translations: transTable2
*toggleButton3.armCallback: RadioSet(UxWidget);

*pb_main_search2.class: pushButton
*pb_main_search2.parent: rowColumn18
*pb_main_search2.static: true
*pb_main_search2.name: pb_main_search2
*pb_main_search2.x: 3
*pb_main_search2.y: 3
*pb_main_search2.height: 25
*pb_main_search2.background: ButtonBackground
*pb_main_search2.fontList: BoldTextFont
*pb_main_search2.foreground: ButtonForeground
*pb_main_search2.labelString: "..."
*pb_main_search2.activateCallback: {\
PopupLong("RotateShell");\
}
*pb_main_search2.recomputeSize: "false"

*rowColumn19.class: rowColumn
*rowColumn19.parent: rowColumn11
*rowColumn19.static: true
*rowColumn19.name: rowColumn19
*rowColumn19.x: 2
*rowColumn19.y: 24
*rowColumn19.width: 250
*rowColumn19.height: 20
*rowColumn19.background: WindowBackground
*rowColumn19.orientation: "horizontal"
*rowColumn19.packing: "pack_tight"
*rowColumn19.resizeWidth: "false"
*rowColumn19.adjustLast: "true"

*toggleButton10.class: toggleButton
*toggleButton10.parent: rowColumn19
*toggleButton10.static: true
*toggleButton10.name: toggleButton10
*toggleButton10.x: 3
*toggleButton10.y: -4
*toggleButton10.width: 195
*toggleButton10.height: 20
*toggleButton10.recomputeSize: "false"
*toggleButton10.background: WindowBackground
*toggleButton10.fontList: TextFont
*toggleButton10.foreground: TextForeground
*toggleButton10.labelString: "Inter-Order Background"
*toggleButton10.selectColor: "Yellow"
*toggleButton10.valueChangedCallback: {\
if (CheckStatus(UxThisWidget) == TRUE)\
    AppendDialogText("SET/ECHEL BKGOPT=YES");\
else\
    AppendDialogText("SET/ECHEL BKGOPT=NO");\
\
}
*toggleButton10.translations: transTable2
*toggleButton10.armCallback: RadioSet(UxWidget);

*pb_main_search9.class: pushButton
*pb_main_search9.parent: rowColumn19
*pb_main_search9.static: true
*pb_main_search9.name: pb_main_search9
*pb_main_search9.x: 3
*pb_main_search9.y: 3
*pb_main_search9.height: 25
*pb_main_search9.background: ButtonBackground
*pb_main_search9.fontList: BoldTextFont
*pb_main_search9.foreground: ButtonForeground
*pb_main_search9.labelString: "..."
*pb_main_search9.activateCallback: {\
PopupLong("BackgroundShell");\
}
*pb_main_search9.recomputeSize: "false"

*rowColumn20.class: rowColumn
*rowColumn20.parent: rowColumn11
*rowColumn20.static: true
*rowColumn20.name: rowColumn20
*rowColumn20.x: 2
*rowColumn20.y: 24
*rowColumn20.width: 250
*rowColumn20.height: 20
*rowColumn20.background: WindowBackground
*rowColumn20.orientation: "horizontal"
*rowColumn20.packing: "pack_tight"
*rowColumn20.resizeWidth: "false"
*rowColumn20.adjustLast: "true"

*toggleButton11.class: toggleButton
*toggleButton11.parent: rowColumn20
*toggleButton11.static: true
*toggleButton11.name: toggleButton11
*toggleButton11.x: 3
*toggleButton11.y: -4
*toggleButton11.width: 195
*toggleButton11.height: 20
*toggleButton11.recomputeSize: "false"
*toggleButton11.background: WindowBackground
*toggleButton11.fontList: TextFont
*toggleButton11.foreground: TextForeground
*toggleButton11.labelString: "Extraction"
*toggleButton11.selectColor: "Yellow"
*toggleButton11.valueChangedCallback: {\
if (CheckStatus(UxThisWidget) == TRUE)\
    AppendDialogText("SET/ECHEL EXTOPT=YES");\
else\
    AppendDialogText("SET/ECHEL EXTOPT=NO");\
\
}
*toggleButton11.translations: transTable2
*toggleButton11.armCallback: RadioSet(UxWidget);

*pb_main_search10.class: pushButton
*pb_main_search10.parent: rowColumn20
*pb_main_search10.static: true
*pb_main_search10.name: pb_main_search10
*pb_main_search10.x: 3
*pb_main_search10.y: 3
*pb_main_search10.height: 25
*pb_main_search10.background: ButtonBackground
*pb_main_search10.fontList: BoldTextFont
*pb_main_search10.foreground: ButtonForeground
*pb_main_search10.labelString: "..."
*pb_main_search10.activateCallback: {\
PopupLong("ExtractionShell");\
}
*pb_main_search10.recomputeSize: "false"

*rowColumn21.class: rowColumn
*rowColumn21.parent: rowColumn11
*rowColumn21.static: true
*rowColumn21.name: rowColumn21
*rowColumn21.x: 2
*rowColumn21.y: 24
*rowColumn21.width: 250
*rowColumn21.height: 20
*rowColumn21.background: WindowBackground
*rowColumn21.orientation: "horizontal"
*rowColumn21.packing: "pack_tight"
*rowColumn21.resizeWidth: "false"
*rowColumn21.adjustLast: "true"

*toggleButton12.class: toggleButton
*toggleButton12.parent: rowColumn21
*toggleButton12.static: true
*toggleButton12.name: toggleButton12
*toggleButton12.x: 3
*toggleButton12.y: -4
*toggleButton12.width: 195
*toggleButton12.height: 20
*toggleButton12.recomputeSize: "false"
*toggleButton12.background: WindowBackground
*toggleButton12.fontList: TextFont
*toggleButton12.foreground: TextForeground
*toggleButton12.labelString: "Sky Background"
*toggleButton12.selectColor: "Yellow"
*toggleButton12.valueChangedCallback: {\
if (CheckStatus(UxThisWidget) == TRUE)\
    AppendDialogText("SET/ECHEL SKYOPT=YES");\
else\
    AppendDialogText("SET/ECHEL SKYOPT=NO");\
\
}
*toggleButton12.translations: transTable2
*toggleButton12.armCallback: RadioSet(UxWidget);

*pb_main_search11.class: pushButton
*pb_main_search11.parent: rowColumn21
*pb_main_search11.static: true
*pb_main_search11.name: pb_main_search11
*pb_main_search11.x: 3
*pb_main_search11.y: 3
*pb_main_search11.height: 25
*pb_main_search11.background: ButtonBackground
*pb_main_search11.fontList: BoldTextFont
*pb_main_search11.foreground: ButtonForeground
*pb_main_search11.labelString: "..."
*pb_main_search11.activateCallback: {\
PopupLong("SkyShell");\
}
*pb_main_search11.recomputeSize: "false"

*rowColumn9.class: rowColumn
*rowColumn9.parent: rowColumn11
*rowColumn9.static: true
*rowColumn9.name: rowColumn9
*rowColumn9.x: 2
*rowColumn9.y: 24
*rowColumn9.width: 250
*rowColumn9.height: 20
*rowColumn9.background: WindowBackground
*rowColumn9.orientation: "horizontal"
*rowColumn9.packing: "pack_tight"
*rowColumn9.resizeWidth: "false"
*rowColumn9.adjustLast: "true"
*rowColumn9.sensitive: "true"

*toggleButton1.class: toggleButton
*toggleButton1.parent: rowColumn9
*toggleButton1.static: true
*toggleButton1.name: toggleButton1
*toggleButton1.x: 3
*toggleButton1.y: 3
*toggleButton1.width: 195
*toggleButton1.height: 29
*toggleButton1.recomputeSize: "false"
*toggleButton1.background: WindowBackground
*toggleButton1.fontList: TextFont
*toggleButton1.foreground: TextForeground
*toggleButton1.labelString: "Resampling"
*toggleButton1.selectColor: "Yellow"
*toggleButton1.valueChangedCallback: {\
if (CheckStatus(UxThisWidget) == TRUE)\
    AppendDialogText("SET/ECHEL REBOPT=YES");\
else\
    AppendDialogText("SET/ECHEL REBOPT=NO");\
\
}
*toggleButton1.translations: transTable2
*toggleButton1.armCallback: RadioSet(UxWidget);

*pb_main_search1.class: pushButton
*pb_main_search1.parent: rowColumn9
*pb_main_search1.static: true
*pb_main_search1.name: pb_main_search1
*pb_main_search1.x: 201
*pb_main_search1.y: 3
*pb_main_search1.height: 25
*pb_main_search1.background: ButtonBackground
*pb_main_search1.fontList: BoldTextFont
*pb_main_search1.foreground: ButtonForeground
*pb_main_search1.labelString: "..."
*pb_main_search1.activateCallback: {\
PopupLong("RebinShell");\
}
*pb_main_search1.recomputeSize: "false"

*rowColumn10.class: rowColumn
*rowColumn10.parent: rowColumn11
*rowColumn10.static: true
*rowColumn10.name: rowColumn10
*rowColumn10.x: 2
*rowColumn10.y: 24
*rowColumn10.width: 250
*rowColumn10.height: 20
*rowColumn10.background: WindowBackground
*rowColumn10.orientation: "horizontal"
*rowColumn10.packing: "pack_tight"
*rowColumn10.resizeWidth: "false"
*rowColumn10.adjustLast: "true"

*toggleButton2.class: toggleButton
*toggleButton2.parent: rowColumn10
*toggleButton2.static: true
*toggleButton2.name: toggleButton2
*toggleButton2.x: 3
*toggleButton2.y: -4
*toggleButton2.width: 195
*toggleButton2.height: 20
*toggleButton2.recomputeSize: "false"
*toggleButton2.background: WindowBackground
*toggleButton2.fontList: TextFont
*toggleButton2.foreground: TextForeground
*toggleButton2.labelString: "Flat-Field"
*toggleButton2.selectColor: "Yellow"
*toggleButton2.valueChangedCallback: {\
if (CheckStatus(UxThisWidget) == TRUE)\
    AppendDialogText("SET/ECHEL FFOPT=YES");\
else\
    AppendDialogText("SET/ECHEL FFOPT=NO");\
\
}
*toggleButton2.translations: transTable2
*toggleButton2.armCallback: RadioSet(UxWidget);

*pb_main_search3.class: pushButton
*pb_main_search3.parent: rowColumn10
*pb_main_search3.static: true
*pb_main_search3.name: pb_main_search3
*pb_main_search3.x: 3
*pb_main_search3.y: 3
*pb_main_search3.height: 25
*pb_main_search3.background: ButtonBackground
*pb_main_search3.fontList: BoldTextFont
*pb_main_search3.foreground: ButtonForeground
*pb_main_search3.labelString: "..."
*pb_main_search3.activateCallback: {\
PopupLong("FlatShell");\
}
*pb_main_search3.recomputeSize: "false"

*rowColumn12.class: rowColumn
*rowColumn12.parent: rowColumn11
*rowColumn12.static: true
*rowColumn12.name: rowColumn12
*rowColumn12.x: 2
*rowColumn12.y: 24
*rowColumn12.width: 250
*rowColumn12.height: 20
*rowColumn12.background: WindowBackground
*rowColumn12.orientation: "horizontal"
*rowColumn12.packing: "pack_tight"
*rowColumn12.resizeWidth: "false"
*rowColumn12.adjustLast: "true"

*toggleButton4.class: toggleButton
*toggleButton4.parent: rowColumn12
*toggleButton4.static: true
*toggleButton4.name: toggleButton4
*toggleButton4.x: 3
*toggleButton4.y: -4
*toggleButton4.width: 195
*toggleButton4.height: 20
*toggleButton4.recomputeSize: "false"
*toggleButton4.background: WindowBackground
*toggleButton4.fontList: TextFont
*toggleButton4.foreground: TextForeground
*toggleButton4.labelString: "Chromatic Response"
*toggleButton4.selectColor: "Yellow"
*toggleButton4.valueChangedCallback: {\
if (CheckStatus(UxThisWidget) == TRUE)\
    AppendDialogText("SET/ECHEL RESPOPT=YES");\
else\
    AppendDialogText("SET/ECHEL RESPOPT=NO");\
\
}
*toggleButton4.translations: transTable2
*toggleButton4.armCallback: RadioSet(UxWidget);

*pb_main_search12.class: pushButton
*pb_main_search12.parent: rowColumn12
*pb_main_search12.static: true
*pb_main_search12.name: pb_main_search12
*pb_main_search12.x: 3
*pb_main_search12.y: 3
*pb_main_search12.height: 25
*pb_main_search12.background: ButtonBackground
*pb_main_search12.fontList: BoldTextFont
*pb_main_search12.foreground: ButtonForeground
*pb_main_search12.labelString: "..."
*pb_main_search12.activateCallback: {\
PopupLong("RespShell");\
}
*pb_main_search12.recomputeSize: "false"

*rowColumn22.class: rowColumn
*rowColumn22.parent: rowColumn11
*rowColumn22.static: true
*rowColumn22.name: rowColumn22
*rowColumn22.x: 2
*rowColumn22.y: 24
*rowColumn22.width: 250
*rowColumn22.height: 20
*rowColumn22.background: WindowBackground
*rowColumn22.orientation: "horizontal"
*rowColumn22.packing: "pack_tight"
*rowColumn22.resizeWidth: "false"
*rowColumn22.adjustLast: "true"

*toggleButton13.class: toggleButton
*toggleButton13.parent: rowColumn22
*toggleButton13.static: true
*toggleButton13.name: toggleButton13
*toggleButton13.x: 3
*toggleButton13.y: -4
*toggleButton13.width: 195
*toggleButton13.height: 20
*toggleButton13.recomputeSize: "false"
*toggleButton13.background: WindowBackground
*toggleButton13.fontList: TextFont
*toggleButton13.foreground: TextForeground
*toggleButton13.labelString: "Order Merging"
*toggleButton13.selectColor: "Yellow"
*toggleButton13.valueChangedCallback: {\
if (CheckStatus(UxThisWidget) == TRUE)\
    AppendDialogText("SET/ECHEL MGOPT=YES");\
else\
    AppendDialogText("SET/ECHEL MGOPT=NO");\
\
}
*toggleButton13.translations: transTable2
*toggleButton13.armCallback: RadioSet(UxWidget);

*pb_main_search13.class: pushButton
*pb_main_search13.parent: rowColumn22
*pb_main_search13.static: true
*pb_main_search13.name: pb_main_search13
*pb_main_search13.x: 3
*pb_main_search13.y: 3
*pb_main_search13.height: 25
*pb_main_search13.background: ButtonBackground
*pb_main_search13.fontList: BoldTextFont
*pb_main_search13.foreground: ButtonForeground
*pb_main_search13.labelString: "..."
*pb_main_search13.activateCallback: {\
PopupLong("MergeShell");\
}
*pb_main_search13.recomputeSize: "false"

