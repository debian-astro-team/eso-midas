! UIMX ascii 2.0 key: 7204                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  
*action.UpdateDirectory: {\
\
 char node[80];\
\
 /* if (XmTextGetSelection(UxWidget) == NULL) return; */\
 strcpy(node,\
     XmTextGetSelection(UxWidget));\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
node[strlen(node)-1] = '\0';\
WGet_all_dirs(node);\
WChange_Midas_dir();\
WGet_all_files();\
\
}

*translation.table: transTable24
*translation.parent: FlatShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable25
*translation.parent: FlatShell
*translation.policy: override
*translation.<Btn3Down>: HelpHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*translation.table: transTable26
*translation.parent: FlatShell
*translation.policy: override
*translation.<Btn1Up>: UpdateDirectory()

*FlatShell.class: transientShell
*FlatShell.parent: NO_PARENT
*FlatShell.static: true
*FlatShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*FlatShell.ispecdecl:
*FlatShell.funcdecl: swidget create_FlatShell()\

*FlatShell.funcname: create_FlatShell
*FlatShell.funcdef: "swidget", "<create_FlatShell>(%)"
*FlatShell.icode:
*FlatShell.fcode: return(rtrn);\

*FlatShell.auxdecl:
*FlatShell.name: FlatShell
*FlatShell.x: 484
*FlatShell.y: 574
*FlatShell.width: 530
*FlatShell.height: 300
*FlatShell.geometry: "+10+60"

*form37.class: form
*form37.parent: FlatShell
*form37.static: true
*form37.name: form37
*form37.resizePolicy: "resize_none"
*form37.unitType: "pixels"
*form37.x: 0
*form37.y: -2
*form37.width: 408
*form37.height: 348
*form37.background: WindowBackground

*form38.class: form
*form38.parent: form37
*form38.static: true
*form38.name: form38
*form38.resizePolicy: "resize_none"
*form38.x: 10
*form38.y: 240
*form38.width: 508
*form38.height: 50
*form38.background: ButtonBackground

*pb_search_search10.class: pushButton
*pb_search_search10.parent: form38
*pb_search_search10.static: true
*pb_search_search10.name: pb_search_search10
*pb_search_search10.x: 16
*pb_search_search10.y: 10
*pb_search_search10.width: 150
*pb_search_search10.height: 30
*pb_search_search10.background: ButtonBackground
*pb_search_search10.fontList: BoldTextFont
*pb_search_search10.foreground: ApplyForeground
*pb_search_search10.labelString: "Fit and Subtract"
*pb_search_search10.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pushButton7.class: pushButton
*pushButton7.parent: form38
*pushButton7.static: true
*pushButton7.name: pushButton7
*pushButton7.x: 412
*pushButton7.y: 8
*pushButton7.width: 86
*pushButton7.height: 30
*pushButton7.background: ButtonBackground
*pushButton7.fontList: BoldTextFont
*pushButton7.foreground: CancelForeground
*pushButton7.labelString: "Cancel"
*pushButton7.activateCallback: {\
UxPopdownInterface(UxFindSwidget("FlatShell"));\
}

*pb_search_plot21.class: pushButton
*pb_search_plot21.parent: form38
*pb_search_plot21.static: true
*pb_search_plot21.name: pb_search_plot21
*pb_search_plot21.x: 320
*pb_search_plot21.y: 10
*pb_search_plot21.width: 86
*pb_search_plot21.height: 30
*pb_search_plot21.background: ButtonBackground
*pb_search_plot21.fontList: BoldTextFont
*pb_search_plot21.foreground: ButtonForeground
*pb_search_plot21.labelString: "Help..."
*pb_search_plot21.activateCallback: {\
GetExtendedHelp(UxWidget);\
}

*pb_search_plot24.class: pushButton
*pb_search_plot24.parent: form38
*pb_search_plot24.static: true
*pb_search_plot24.name: pb_search_plot24
*pb_search_plot24.x: 202
*pb_search_plot24.y: 10
*pb_search_plot24.width: 106
*pb_search_plot24.height: 30
*pb_search_plot24.background: ButtonBackground
*pb_search_plot24.fontList: BoldTextFont
*pb_search_plot24.foreground: ButtonForeground
*pb_search_plot24.labelString: "Plot Blaze"
*pb_search_plot24.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*shelp_search7.class: text
*shelp_search7.parent: form37
*shelp_search7.static: true
*shelp_search7.name: shelp_search7
*shelp_search7.x: 8
*shelp_search7.y: 182
*shelp_search7.width: 508
*shelp_search7.height: 50
*shelp_search7.background: SHelpBackground
*shelp_search7.cursorPositionVisible: "false"
*shelp_search7.editable: "false"
*shelp_search7.fontList: TextFont

*separator30.class: separator
*separator30.parent: form37
*separator30.static: true
*separator30.name: separator30
*separator30.x: 8
*separator30.y: 232
*separator30.width: 514
*separator30.height: 10
*separator30.background: WindowBackground

*separator31.class: separator
*separator31.parent: form37
*separator31.static: true
*separator31.name: separator31
*separator31.x: 8
*separator31.y: 168
*separator31.width: 518
*separator31.height: 10
*separator31.background: WindowBackground

*tf_alpha35.class: textField
*tf_alpha35.parent: form37
*tf_alpha35.static: true
*tf_alpha35.name: tf_alpha35
*tf_alpha35.x: 130
*tf_alpha35.y: 8
*tf_alpha35.width: 230
*tf_alpha35.height: 32
*tf_alpha35.background: TextBackground
*tf_alpha35.fontList: TextFont
*tf_alpha35.highlightOnEnter: "true"
*tf_alpha35.foreground: TextForeground
*tf_alpha35.losingFocusCallback: {\
\
}

*tf_alpha36.class: textField
*tf_alpha36.parent: form37
*tf_alpha36.static: true
*tf_alpha36.name: tf_alpha36
*tf_alpha36.x: 130
*tf_alpha36.y: 46
*tf_alpha36.width: 230
*tf_alpha36.height: 32
*tf_alpha36.background: TextBackground
*tf_alpha36.fontList: TextFont
*tf_alpha36.highlightOnEnter: "true"
*tf_alpha36.foreground: TextForeground
*tf_alpha36.losingFocusCallback: {\
\
}
*tf_alpha36.text: "middummff.bdf"

*tf_alpha37.class: textField
*tf_alpha37.parent: form37
*tf_alpha37.static: true
*tf_alpha37.name: tf_alpha37
*tf_alpha37.x: 130
*tf_alpha37.y: 90
*tf_alpha37.width: 230
*tf_alpha37.height: 32
*tf_alpha37.background: TextBackground
*tf_alpha37.fontList: TextFont
*tf_alpha37.highlightOnEnter: "true"
*tf_alpha37.foreground: TextForeground
*tf_alpha37.losingFocusCallback: {\
\
}

*label114.class: label
*label114.parent: form37
*label114.static: true
*label114.name: label114
*label114.x: 18
*label114.y: 10
*label114.width: 94
*label114.height: 30
*label114.background: LabelBackground
*label114.fontList: TextFont
*label114.labelString: "Input Frame"
*label114.alignment: "alignment_beginning"
*label114.foreground: TextForeground

*pb_main_search25.class: pushButton
*pb_main_search25.parent: form37
*pb_main_search25.static: true
*pb_main_search25.name: pb_main_search25
*pb_main_search25.x: 365
*pb_main_search25.y: 10
*pb_main_search25.height: 28
*pb_main_search25.background: ButtonBackground
*pb_main_search25.fontList: BoldTextFont
*pb_main_search25.foreground: ButtonForeground
*pb_main_search25.labelString: "..."
*pb_main_search25.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search25.recomputeSize: "true"
*pb_main_search25.width: 46

*pb_main_search26.class: pushButton
*pb_main_search26.parent: form37
*pb_main_search26.static: true
*pb_main_search26.name: pb_main_search26
*pb_main_search26.x: 365
*pb_main_search26.y: 48
*pb_main_search26.height: 28
*pb_main_search26.background: ButtonBackground
*pb_main_search26.fontList: BoldTextFont
*pb_main_search26.foreground: ButtonForeground
*pb_main_search26.labelString: "..."
*pb_main_search26.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search26.recomputeSize: "true"
*pb_main_search26.width: 46

*pb_main_search27.class: pushButton
*pb_main_search27.parent: form37
*pb_main_search27.static: true
*pb_main_search27.name: pb_main_search27
*pb_main_search27.x: 365
*pb_main_search27.y: 88
*pb_main_search27.height: 28
*pb_main_search27.background: ButtonBackground
*pb_main_search27.fontList: BoldTextFont
*pb_main_search27.foreground: ButtonForeground
*pb_main_search27.labelString: "..."
*pb_main_search27.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search27.recomputeSize: "true"
*pb_main_search27.width: 46

*label115.class: label
*label115.parent: form37
*label115.static: true
*label115.name: label115
*label115.x: 4
*label115.y: 42
*label115.width: 94
*label115.height: 30
*label115.background: LabelBackground
*label115.fontList: TextFont
*label115.labelString: "- Background"
*label115.alignment: "alignment_beginning"
*label115.foreground: TextForeground

*label116.class: label
*label116.parent: form37
*label116.static: true
*label116.name: label116
*label116.x: 6
*label116.y: 86
*label116.width: 116
*label116.height: 30
*label116.background: LabelBackground
*label116.fontList: TextFont
*label116.labelString: "= Output Frame"
*label116.alignment: "alignment_beginning"
*label116.foreground: TextForeground

*pb_search_plot26.class: pushButton
*pb_search_plot26.parent: form37
*pb_search_plot26.static: true
*pb_search_plot26.name: pb_search_plot26
*pb_search_plot26.x: 420
*pb_search_plot26.y: 10
*pb_search_plot26.width: 86
*pb_search_plot26.height: 30
*pb_search_plot26.background: ButtonBackground
*pb_search_plot26.fontList: BoldTextFont
*pb_search_plot26.foreground: ButtonForeground
*pb_search_plot26.labelString: "Display"
*pb_search_plot26.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pb_search_plot27.class: pushButton
*pb_search_plot27.parent: form37
*pb_search_plot27.static: true
*pb_search_plot27.name: pb_search_plot27
*pb_search_plot27.x: 420
*pb_search_plot27.y: 48
*pb_search_plot27.width: 86
*pb_search_plot27.height: 30
*pb_search_plot27.background: ButtonBackground
*pb_search_plot27.fontList: BoldTextFont
*pb_search_plot27.foreground: ButtonForeground
*pb_search_plot27.labelString: "Display"
*pb_search_plot27.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*tf_alpha11.class: textField
*tf_alpha11.parent: form37
*tf_alpha11.static: true
*tf_alpha11.name: tf_alpha11
*tf_alpha11.x: 130
*tf_alpha11.y: 130
*tf_alpha11.width: 230
*tf_alpha11.height: 32
*tf_alpha11.background: TextBackground
*tf_alpha11.fontList: TextFont
*tf_alpha11.highlightOnEnter: "true"
*tf_alpha11.foreground: TextForeground
*tf_alpha11.losingFocusCallback: {\
\
}

*label6.class: label
*label6.parent: form37
*label6.static: true
*label6.name: label6
*label6.x: 8
*label6.y: 128
*label6.width: 116
*label6.height: 30
*label6.background: LabelBackground
*label6.fontList: TextFont
*label6.labelString: "Blaze Function:"
*label6.alignment: "alignment_beginning"
*label6.foreground: TextForeground

