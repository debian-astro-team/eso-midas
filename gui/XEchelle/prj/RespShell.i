! UIMX ascii 2.0 key: 1450                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  
*action.UpdateDirectory: {\
\
 char node[80];\
\
 /* if (XmTextGetSelection(UxWidget) == NULL) return; */\
 strcpy(node,\
     XmTextGetSelection(UxWidget));\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
node[strlen(node)-1] = '\0';\
WGet_all_dirs(node);\
WChange_Midas_dir();\
WGet_all_files();\
\
}

*translation.table: transTable27
*translation.parent: RespShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable28
*translation.parent: RespShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*translation.table: transTable29
*translation.parent: RespShell
*translation.policy: override
*translation.<Btn1Up>: UpdateDirectory()

*translation.table: transTable34
*translation.parent: RespShell
*translation.policy: override
*translation.<Btn3Down>: HelpHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*RespShell.class: transientShell
*RespShell.parent: NO_PARENT
*RespShell.static: true
*RespShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*RespShell.ispecdecl:
*RespShell.funcdecl: swidget create_RespShell()\

*RespShell.funcname: create_RespShell
*RespShell.funcdef: "swidget", "<create_RespShell>(%)"
*RespShell.icode:
*RespShell.fcode: return(rtrn);\

*RespShell.auxdecl:
*RespShell.name: RespShell
*RespShell.x: 899
*RespShell.y: 287
*RespShell.width: 530
*RespShell.height: 640
*RespShell.geometry: "+10+60"
*RespShell.title: "XEchelle: Instrumental Response"

*form39.class: form
*form39.parent: RespShell
*form39.static: true
*form39.name: form39
*form39.resizePolicy: "resize_none"
*form39.unitType: "pixels"
*form39.x: 0
*form39.y: -2
*form39.width: 530
*form39.height: 640
*form39.background: WindowBackground

*form40.class: form
*form40.parent: form39
*form40.static: true
*form40.name: form40
*form40.resizePolicy: "resize_none"
*form40.x: 12
*form40.y: 552
*form40.width: 508
*form40.height: 46
*form40.background: ButtonBackground

*pb_search_search12.class: pushButton
*pb_search_search12.parent: form40
*pb_search_search12.static: true
*pb_search_search12.name: pb_search_search12
*pb_search_search12.x: 16
*pb_search_search12.y: 10
*pb_search_search12.width: 86
*pb_search_search12.height: 30
*pb_search_search12.background: ButtonBackground
*pb_search_search12.fontList: BoldTextFont
*pb_search_search12.foreground: ApplyForeground
*pb_search_search12.labelString: "Fit"
*pb_search_search12.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pushButton8.class: pushButton
*pushButton8.parent: form40
*pushButton8.static: true
*pushButton8.name: pushButton8
*pushButton8.x: 402
*pushButton8.y: 10
*pushButton8.width: 86
*pushButton8.height: 30
*pushButton8.background: ButtonBackground
*pushButton8.fontList: BoldTextFont
*pushButton8.foreground: CancelForeground
*pushButton8.labelString: "Cancel"
*pushButton8.activateCallback: {\
UxPopdownInterface(UxFindSwidget("RespShell"));\
}

*pb_search_plot22.class: pushButton
*pb_search_plot22.parent: form40
*pb_search_plot22.static: true
*pb_search_plot22.name: pb_search_plot22
*pb_search_plot22.x: 308
*pb_search_plot22.y: 8
*pb_search_plot22.width: 86
*pb_search_plot22.height: 30
*pb_search_plot22.background: ButtonBackground
*pb_search_plot22.fontList: BoldTextFont
*pb_search_plot22.foreground: ButtonForeground
*pb_search_plot22.labelString: "Help..."
*pb_search_plot22.activateCallback: {\
GetExtendedHelp(UxWidget);\
}

*pb_search_plot23.class: pushButton
*pb_search_plot23.parent: form40
*pb_search_plot23.static: true
*pb_search_plot23.name: pb_search_plot23
*pb_search_plot23.x: 110
*pb_search_plot23.y: 12
*pb_search_plot23.width: 138
*pb_search_plot23.height: 30
*pb_search_plot23.background: ButtonBackground
*pb_search_plot23.fontList: BoldTextFont
*pb_search_plot23.foreground: ButtonForeground
*pb_search_plot23.labelString: "Plot Response"
*pb_search_plot23.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*separator32.class: separator
*separator32.parent: form39
*separator32.static: true
*separator32.name: separator32
*separator32.x: 10
*separator32.y: 544
*separator32.width: 514
*separator32.height: 10
*separator32.background: WindowBackground

*separator33.class: separator
*separator33.parent: form39
*separator33.static: true
*separator33.name: separator33
*separator33.x: 8
*separator33.y: 480
*separator33.width: 518
*separator33.height: 10
*separator33.background: WindowBackground

*rowColumn3.class: rowColumn
*rowColumn3.parent: form39
*rowColumn3.static: true
*rowColumn3.name: rowColumn3
*rowColumn3.x: 92
*rowColumn3.y: 45
*rowColumn3.width: 106
*rowColumn3.height: 96
*rowColumn3.radioBehavior: "true"
*rowColumn3.background: WindowBackground
*rowColumn3.entryBorder: 0
*rowColumn3.labelString: ""
*rowColumn3.shadowThickness: 0
*rowColumn3.borderWidth: 0
*rowColumn3.entryAlignment: "alignment_beginning"
*rowColumn3.adjustLast: "false"
*rowColumn3.adjustMargin: "true"
*rowColumn3.isAligned: "true"
*rowColumn3.orientation: "horizontal"
*rowColumn3.resizeWidth: "true"

*rb_seamtd_gaus4.class: toggleButton
*rb_seamtd_gaus4.parent: rowColumn3
*rb_seamtd_gaus4.static: true
*rb_seamtd_gaus4.name: rb_seamtd_gaus4
*rb_seamtd_gaus4.x: 3
*rb_seamtd_gaus4.y: 0
*rb_seamtd_gaus4.width: 139
*rb_seamtd_gaus4.height: 31
*rb_seamtd_gaus4.background: WindowBackground
*rb_seamtd_gaus4.fontList: TextFont
*rb_seamtd_gaus4.labelString: "Standard Star"
*rb_seamtd_gaus4.set: "true"
*rb_seamtd_gaus4.selectColor: SelectColor
*rb_seamtd_gaus4.highlightOnEnter: "true"
*rb_seamtd_gaus4.valueChangedCallback: {\
\
}
*rb_seamtd_gaus4.indicatorSize: 16
*rb_seamtd_gaus4.foreground: TextForeground
*rb_seamtd_gaus4.armCallback: RadioSet(UxWidget);
*rb_seamtd_gaus4.translations: transTable28

*rb_seamtd_grav4.class: toggleButton
*rb_seamtd_grav4.parent: rowColumn3
*rb_seamtd_grav4.static: true
*rb_seamtd_grav4.name: rb_seamtd_grav4
*rb_seamtd_grav4.x: 136
*rb_seamtd_grav4.y: 3
*rb_seamtd_grav4.width: 108
*rb_seamtd_grav4.height: 28
*rb_seamtd_grav4.background: WindowBackground
*rb_seamtd_grav4.fontList: TextFont
*rb_seamtd_grav4.labelString: "Blaze Function Fit"
*rb_seamtd_grav4.selectColor: SelectColor
*rb_seamtd_grav4.highlightOnEnter: "true"
*rb_seamtd_grav4.valueChangedCallback: {\
\
}
*rb_seamtd_grav4.indicatorSize: 16
*rb_seamtd_grav4.foreground: TextForeground
*rb_seamtd_grav4.armCallback: RadioSet(UxWidget);
*rb_seamtd_grav4.translations: transTable28

*label113.class: label
*label113.parent: form39
*label113.static: true
*label113.name: label113
*label113.x: 18
*label113.y: 47
*label113.width: 68
*label113.height: 30
*label113.background: LabelBackground
*label113.fontList: TextFont
*label113.labelString: "Method:"
*label113.alignment: "alignment_beginning"
*label113.foreground: TextForeground

*tf_alpha38.class: textField
*tf_alpha38.parent: form39
*tf_alpha38.static: true
*tf_alpha38.name: tf_alpha38
*tf_alpha38.x: 126
*tf_alpha38.y: 8
*tf_alpha38.width: 236
*tf_alpha38.height: 32
*tf_alpha38.background: TextBackground
*tf_alpha38.fontList: TextFont
*tf_alpha38.highlightOnEnter: "true"
*tf_alpha38.foreground: TextForeground
*tf_alpha38.losingFocusCallback: {\
\
}

*label117.class: label
*label117.parent: form39
*label117.static: true
*label117.name: label117
*label117.x: 18
*label117.y: 10
*label117.width: 94
*label117.height: 30
*label117.background: LabelBackground
*label117.fontList: TextFont
*label117.labelString: "Input Frame:"
*label117.alignment: "alignment_beginning"
*label117.foreground: TextForeground

*pb_main_search28.class: pushButton
*pb_main_search28.parent: form39
*pb_main_search28.static: true
*pb_main_search28.name: pb_main_search28
*pb_main_search28.x: 366
*pb_main_search28.y: 10
*pb_main_search28.height: 28
*pb_main_search28.background: ButtonBackground
*pb_main_search28.fontList: BoldTextFont
*pb_main_search28.foreground: ButtonForeground
*pb_main_search28.labelString: "..."
*pb_main_search28.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search28.recomputeSize: "true"
*pb_main_search28.width: 46

*pb_search_plot30.class: pushButton
*pb_search_plot30.parent: form39
*pb_search_plot30.static: true
*pb_search_plot30.name: pb_search_plot30
*pb_search_plot30.x: 420
*pb_search_plot30.y: 10
*pb_search_plot30.width: 86
*pb_search_plot30.height: 30
*pb_search_plot30.background: ButtonBackground
*pb_search_plot30.fontList: BoldTextFont
*pb_search_plot30.foreground: ButtonForeground
*pb_search_plot30.labelString: "Display"
*pb_search_plot30.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*form41.class: form
*form41.parent: form39
*form41.static: true
*form41.name: form41
*form41.resizePolicy: "resize_none"
*form41.x: 14
*form41.y: 80
*form41.width: 508
*form41.height: 194
*form41.background: ButtonBackground

*label99.class: label
*label99.parent: form41
*label99.static: true
*label99.name: label99
*label99.x: 36
*label99.y: 120
*label99.width: 100
*label99.height: 30
*label99.background: LabelBackground
*label99.fontList: TextFont
*label99.labelString: "Smooth Filter:"
*label99.alignment: "alignment_beginning"
*label99.foreground: TextForeground

*tf_alpha26.class: textField
*tf_alpha26.parent: form41
*tf_alpha26.static: true
*tf_alpha26.name: tf_alpha26
*tf_alpha26.x: 390
*tf_alpha26.y: 74
*tf_alpha26.width: 96
*tf_alpha26.height: 32
*tf_alpha26.background: TextBackground
*tf_alpha26.fontList: TextFont
*tf_alpha26.highlightOnEnter: "true"
*tf_alpha26.foreground: TextForeground
*tf_alpha26.losingFocusCallback: {\
\
}

*tf_alpha27.class: textField
*tf_alpha27.parent: form41
*tf_alpha27.static: true
*tf_alpha27.name: tf_alpha27
*tf_alpha27.x: 232
*tf_alpha27.y: 156
*tf_alpha27.width: 96
*tf_alpha27.height: 32
*tf_alpha27.background: TextBackground
*tf_alpha27.fontList: TextFont
*tf_alpha27.highlightOnEnter: "true"
*tf_alpha27.foreground: TextForeground
*tf_alpha27.losingFocusCallback: {\
\
}

*label102.class: label
*label102.parent: form41
*label102.static: true
*label102.name: label102
*label102.x: 36
*label102.y: 158
*label102.width: 114
*label102.height: 30
*label102.background: LabelBackground
*label102.fontList: TextFont
*label102.labelString: "Ignored Pixels:"
*label102.alignment: "alignment_beginning"
*label102.foreground: TextForeground

*tf_alpha28.class: textField
*tf_alpha28.parent: form41
*tf_alpha28.static: true
*tf_alpha28.name: tf_alpha28
*tf_alpha28.x: 234
*tf_alpha28.y: 116
*tf_alpha28.width: 96
*tf_alpha28.height: 32
*tf_alpha28.background: TextBackground
*tf_alpha28.fontList: TextFont
*tf_alpha28.highlightOnEnter: "true"
*tf_alpha28.foreground: TextForeground
*tf_alpha28.losingFocusCallback: {\
\
}

*tf_alpha29.class: textField
*tf_alpha29.parent: form41
*tf_alpha29.static: true
*tf_alpha29.name: tf_alpha29
*tf_alpha29.x: 388
*tf_alpha29.y: 154
*tf_alpha29.width: 96
*tf_alpha29.height: 32
*tf_alpha29.background: TextBackground
*tf_alpha29.fontList: TextFont
*tf_alpha29.highlightOnEnter: "true"
*tf_alpha29.foreground: TextForeground
*tf_alpha29.losingFocusCallback: {\
\
}

*tf_alpha30.class: textField
*tf_alpha30.parent: form41
*tf_alpha30.static: true
*tf_alpha30.name: tf_alpha30
*tf_alpha30.x: 388
*tf_alpha30.y: 116
*tf_alpha30.width: 96
*tf_alpha30.height: 32
*tf_alpha30.background: TextBackground
*tf_alpha30.fontList: TextFont
*tf_alpha30.highlightOnEnter: "true"
*tf_alpha30.foreground: TextForeground
*tf_alpha30.losingFocusCallback: {\
\
}

*label104.class: label
*label104.parent: form41
*label104.static: true
*label104.name: label104
*label104.x: 36
*label104.y: 80
*label104.width: 136
*label104.height: 30
*label104.background: LabelBackground
*label104.fontList: TextFont
*label104.labelString: "Median Filter:"
*label104.alignment: "alignment_beginning"
*label104.foreground: TextForeground

*tf_alpha32.class: textField
*tf_alpha32.parent: form41
*tf_alpha32.static: true
*tf_alpha32.name: tf_alpha32
*tf_alpha32.x: 236
*tf_alpha32.y: 76
*tf_alpha32.width: 96
*tf_alpha32.height: 32
*tf_alpha32.background: TextBackground
*tf_alpha32.fontList: TextFont
*tf_alpha32.highlightOnEnter: "true"
*tf_alpha32.foreground: TextForeground
*tf_alpha32.losingFocusCallback: {\
\
}

*label105.class: label
*label105.parent: form41
*label105.static: true
*label105.name: label105
*label105.x: 188
*label105.y: 158
*label105.width: 40
*label105.height: 30
*label105.background: LabelBackground
*label105.fontList: TextFont
*label105.labelString: "Min="
*label105.alignment: "alignment_beginning"
*label105.foreground: TextForeground

*label106.class: label
*label106.parent: form41
*label106.static: true
*label106.name: label106
*label106.x: 186
*label106.y: 118
*label106.width: 36
*label106.height: 30
*label106.background: LabelBackground
*label106.fontList: TextFont
*label106.labelString: "X="
*label106.alignment: "alignment_beginning"
*label106.foreground: TextForeground

*label107.class: label
*label107.parent: form41
*label107.static: true
*label107.name: label107
*label107.x: 362
*label107.y: 74
*label107.width: 20
*label107.height: 30
*label107.background: LabelBackground
*label107.fontList: TextFont
*label107.labelString: "Y="
*label107.alignment: "alignment_beginning"
*label107.foreground: TextForeground

*label108.class: label
*label108.parent: form41
*label108.static: true
*label108.name: label108
*label108.x: 338
*label108.y: 156
*label108.width: 50
*label108.height: 30
*label108.background: LabelBackground
*label108.fontList: TextFont
*label108.labelString: "Max="
*label108.alignment: "alignment_beginning"
*label108.foreground: TextForeground

*label109.class: label
*label109.parent: form41
*label109.static: true
*label109.name: label109
*label109.x: 356
*label109.y: 116
*label109.width: 20
*label109.height: 30
*label109.background: LabelBackground
*label109.fontList: TextFont
*label109.labelString: "Y="
*label109.alignment: "alignment_beginning"
*label109.foreground: TextForeground

*label110.class: label
*label110.parent: form41
*label110.static: true
*label110.name: label110
*label110.x: 186
*label110.y: 80
*label110.width: 34
*label110.height: 30
*label110.background: LabelBackground
*label110.fontList: TextFont
*label110.labelString: "X="
*label110.alignment: "alignment_beginning"
*label110.foreground: TextForeground

*tf_alpha39.class: textField
*tf_alpha39.parent: form41
*tf_alpha39.static: true
*tf_alpha39.name: tf_alpha39
*tf_alpha39.x: 132
*tf_alpha39.y: 36
*tf_alpha39.width: 228
*tf_alpha39.height: 32
*tf_alpha39.background: TextBackground
*tf_alpha39.fontList: TextFont
*tf_alpha39.highlightOnEnter: "true"
*tf_alpha39.foreground: TextForeground
*tf_alpha39.losingFocusCallback: {\
\
}

*pb_main_search29.class: pushButton
*pb_main_search29.parent: form41
*pb_main_search29.static: true
*pb_main_search29.name: pb_main_search29
*pb_main_search29.x: 364
*pb_main_search29.y: 38
*pb_main_search29.height: 28
*pb_main_search29.background: ButtonBackground
*pb_main_search29.fontList: BoldTextFont
*pb_main_search29.foreground: ButtonForeground
*pb_main_search29.labelString: "..."
*pb_main_search29.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search29.recomputeSize: "true"
*pb_main_search29.width: 46

*label118.class: label
*label118.parent: form41
*label118.static: true
*label118.name: label118
*label118.x: 22
*label118.y: 38
*label118.width: 94
*label118.height: 30
*label118.background: LabelBackground
*label118.fontList: TextFont
*label118.labelString: "Flux Table"
*label118.alignment: "alignment_beginning"
*label118.foreground: TextForeground

*pb_search_plot31.class: pushButton
*pb_search_plot31.parent: form41
*pb_search_plot31.static: true
*pb_search_plot31.name: pb_search_plot31
*pb_search_plot31.x: 422
*pb_search_plot31.y: 38
*pb_search_plot31.width: 86
*pb_search_plot31.height: 30
*pb_search_plot31.background: ButtonBackground
*pb_search_plot31.fontList: BoldTextFont
*pb_search_plot31.foreground: ButtonForeground
*pb_search_plot31.labelString: "Plot"
*pb_search_plot31.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*label127.class: label
*label127.parent: form41
*label127.static: true
*label127.name: label127
*label127.x: 168
*label127.y: 2
*label127.width: 166
*label127.height: 30
*label127.background: LabelBackground
*label127.fontList: BoldTextFont
*label127.labelString: "Standard Star"
*label127.alignment: "alignment_center"
*label127.foreground: TextForeground

*form42.class: form
*form42.parent: form39
*form42.static: true
*form42.name: form42
*form42.resizePolicy: "resize_none"
*form42.x: 14
*form42.y: 284
*form42.width: 508
*form42.height: 194
*form42.background: ButtonBackground

*label103.class: label
*label103.parent: form42
*label103.static: true
*label103.name: label103
*label103.x: 34
*label103.y: 118
*label103.width: 100
*label103.height: 30
*label103.background: LabelBackground
*label103.fontList: TextFont
*label103.labelString: "Alpha:"
*label103.alignment: "alignment_beginning"
*label103.foreground: TextForeground

*tf_alpha33.class: textField
*tf_alpha33.parent: form42
*tf_alpha33.static: true
*tf_alpha33.name: tf_alpha33
*tf_alpha33.x: 230
*tf_alpha33.y: 154
*tf_alpha33.width: 96
*tf_alpha33.height: 32
*tf_alpha33.background: TextBackground
*tf_alpha33.fontList: TextFont
*tf_alpha33.highlightOnEnter: "true"
*tf_alpha33.foreground: TextForeground
*tf_alpha33.losingFocusCallback: {\
\
}

*label111.class: label
*label111.parent: form42
*label111.static: true
*label111.name: label111
*label111.x: 34
*label111.y: 156
*label111.width: 94
*label111.height: 30
*label111.background: LabelBackground
*label111.fontList: TextFont
*label111.labelString: "Lambda"
*label111.alignment: "alignment_beginning"
*label111.foreground: TextForeground

*tf_alpha34.class: textField
*tf_alpha34.parent: form42
*tf_alpha34.static: true
*tf_alpha34.name: tf_alpha34
*tf_alpha34.x: 192
*tf_alpha34.y: 114
*tf_alpha34.width: 136
*tf_alpha34.height: 32
*tf_alpha34.background: TextBackground
*tf_alpha34.fontList: TextFont
*tf_alpha34.highlightOnEnter: "true"
*tf_alpha34.foreground: TextForeground
*tf_alpha34.losingFocusCallback: {\
\
}

*tf_alpha41.class: textField
*tf_alpha41.parent: form42
*tf_alpha41.static: true
*tf_alpha41.name: tf_alpha41
*tf_alpha41.x: 386
*tf_alpha41.y: 152
*tf_alpha41.width: 96
*tf_alpha41.height: 32
*tf_alpha41.background: TextBackground
*tf_alpha41.fontList: TextFont
*tf_alpha41.highlightOnEnter: "true"
*tf_alpha41.foreground: TextForeground
*tf_alpha41.losingFocusCallback: {\
\
}

*label112.class: label
*label112.parent: form42
*label112.static: true
*label112.name: label112
*label112.x: 34
*label112.y: 78
*label112.width: 136
*label112.height: 30
*label112.background: LabelBackground
*label112.fontList: TextFont
*label112.labelString: "Grating Constant:"
*label112.alignment: "alignment_beginning"
*label112.foreground: TextForeground

*tf_alpha43.class: textField
*tf_alpha43.parent: form42
*tf_alpha43.static: true
*tf_alpha43.name: tf_alpha43
*tf_alpha43.x: 190
*tf_alpha43.y: 74
*tf_alpha43.width: 140
*tf_alpha43.height: 32
*tf_alpha43.background: TextBackground
*tf_alpha43.fontList: TextFont
*tf_alpha43.highlightOnEnter: "true"
*tf_alpha43.foreground: TextForeground
*tf_alpha43.losingFocusCallback: {\
\
}

*label120.class: label
*label120.parent: form42
*label120.static: true
*label120.name: label120
*label120.x: 186
*label120.y: 156
*label120.width: 40
*label120.height: 30
*label120.background: LabelBackground
*label120.fontList: TextFont
*label120.labelString: "Min="
*label120.alignment: "alignment_beginning"
*label120.foreground: TextForeground

*label123.class: label
*label123.parent: form42
*label123.static: true
*label123.name: label123
*label123.x: 336
*label123.y: 154
*label123.width: 50
*label123.height: 30
*label123.background: LabelBackground
*label123.fontList: TextFont
*label123.labelString: "Max="
*label123.alignment: "alignment_beginning"
*label123.foreground: TextForeground

*rowColumn28.class: rowColumn
*rowColumn28.parent: form42
*rowColumn28.static: true
*rowColumn28.name: rowColumn28
*rowColumn28.x: 146
*rowColumn28.y: 36
*rowColumn28.width: 261
*rowColumn28.height: 34
*rowColumn28.radioBehavior: "true"
*rowColumn28.background: WindowBackground
*rowColumn28.entryBorder: 0
*rowColumn28.labelString: ""
*rowColumn28.shadowThickness: 0
*rowColumn28.borderWidth: 0
*rowColumn28.entryAlignment: "alignment_beginning"
*rowColumn28.adjustLast: "false"
*rowColumn28.adjustMargin: "true"
*rowColumn28.isAligned: "true"
*rowColumn28.orientation: "horizontal"
*rowColumn28.resizeWidth: "true"

*rb_seamtd_gaus5.class: toggleButton
*rb_seamtd_gaus5.parent: rowColumn28
*rb_seamtd_gaus5.static: true
*rb_seamtd_gaus5.name: rb_seamtd_gaus5
*rb_seamtd_gaus5.x: 2
*rb_seamtd_gaus5.y: 3
*rb_seamtd_gaus5.width: 84
*rb_seamtd_gaus5.height: 28
*rb_seamtd_gaus5.background: WindowBackground
*rb_seamtd_gaus5.fontList: TextFont
*rb_seamtd_gaus5.labelString: "Fit"
*rb_seamtd_gaus5.set: "true"
*rb_seamtd_gaus5.selectColor: SelectColor
*rb_seamtd_gaus5.highlightOnEnter: "true"
*rb_seamtd_gaus5.valueChangedCallback: {\
\
}
*rb_seamtd_gaus5.indicatorSize: 16
*rb_seamtd_gaus5.foreground: TextForeground
*rb_seamtd_gaus5.armCallback: RadioSet(UxWidget);
*rb_seamtd_gaus5.translations: transTable28

*rb_seamtd_grav5.class: toggleButton
*rb_seamtd_grav5.parent: rowColumn28
*rb_seamtd_grav5.static: true
*rb_seamtd_grav5.name: rb_seamtd_grav5
*rb_seamtd_grav5.x: 114
*rb_seamtd_grav5.y: 4
*rb_seamtd_grav5.width: 84
*rb_seamtd_grav5.height: 28
*rb_seamtd_grav5.background: WindowBackground
*rb_seamtd_grav5.fontList: TextFont
*rb_seamtd_grav5.labelString: "Overlap"
*rb_seamtd_grav5.selectColor: SelectColor
*rb_seamtd_grav5.highlightOnEnter: "true"
*rb_seamtd_grav5.valueChangedCallback: {\
\
}
*rb_seamtd_grav5.indicatorSize: 16
*rb_seamtd_grav5.foreground: TextForeground
*rb_seamtd_grav5.armCallback: RadioSet(UxWidget);
*rb_seamtd_grav5.translations: transTable28

*rb_seamtd_maxi5.class: toggleButton
*rb_seamtd_maxi5.parent: rowColumn28
*rb_seamtd_maxi5.static: true
*rb_seamtd_maxi5.name: rb_seamtd_maxi5
*rb_seamtd_maxi5.x: 175
*rb_seamtd_maxi5.y: 3
*rb_seamtd_maxi5.width: 84
*rb_seamtd_maxi5.height: 28
*rb_seamtd_maxi5.background: WindowBackground
*rb_seamtd_maxi5.fontList: TextFont
*rb_seamtd_maxi5.labelString: "Sinc"
*rb_seamtd_maxi5.selectColor: SelectColor
*rb_seamtd_maxi5.highlightOnEnter: "true"
*rb_seamtd_maxi5.valueChangedCallback: {\
\
}
*rb_seamtd_maxi5.indicatorSize: 16
*rb_seamtd_maxi5.foreground: TextForeground
*rb_seamtd_maxi5.armCallback: RadioSet(UxWidget);
*rb_seamtd_maxi5.translations: transTable28

*label119.class: label
*label119.parent: form42
*label119.static: true
*label119.name: label119
*label119.x: 56
*label119.y: 36
*label119.width: 68
*label119.height: 30
*label119.background: LabelBackground
*label119.fontList: TextFont
*label119.labelString: "Method:"
*label119.alignment: "alignment_beginning"
*label119.foreground: TextForeground

*label126.class: label
*label126.parent: form42
*label126.static: true
*label126.name: label126
*label126.x: 168
*label126.y: 2
*label126.width: 166
*label126.height: 30
*label126.background: LabelBackground
*label126.fontList: BoldTextFont
*label126.labelString: "Blaze Function Fitting"
*label126.alignment: "alignment_center"
*label126.foreground: TextForeground

*shelp_search8.class: text
*shelp_search8.parent: form39
*shelp_search8.static: true
*shelp_search8.name: shelp_search8
*shelp_search8.x: 12
*shelp_search8.y: 492
*shelp_search8.width: 508
*shelp_search8.height: 50
*shelp_search8.background: SHelpBackground
*shelp_search8.cursorPositionVisible: "false"
*shelp_search8.editable: "false"
*shelp_search8.fontList: TextFont

