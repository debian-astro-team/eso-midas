! UIMX ascii 2.0 key: 7715                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  

*translation.table: transTable7
*translation.parent: RotateShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable8
*translation.parent: RotateShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*translation.table: transTable9
*translation.parent: RotateShell
*translation.policy: augment
*translation.<LeaveWindow>: ClearShort()

*RotateShell.class: transientShell
*RotateShell.parent: NO_PARENT
*RotateShell.static: true
*RotateShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*RotateShell.ispecdecl:
*RotateShell.funcdecl: swidget create_RotateShell()\

*RotateShell.funcname: create_RotateShell
*RotateShell.funcdef: "swidget", "<create_RotateShell>(%)"
*RotateShell.icode:
*RotateShell.fcode: return(rtrn);\

*RotateShell.auxdecl:
*RotateShell.name: RotateShell
*RotateShell.x: 722
*RotateShell.y: 372
*RotateShell.width: 500
*RotateShell.height: 540
*RotateShell.title: "XEchelle: Rotation and Geometry"
*RotateShell.geometry: "+10+60"

*form25.class: form
*form25.parent: RotateShell
*form25.static: true
*form25.name: form25
*form25.resizePolicy: "resize_none"
*form25.unitType: "pixels"
*form25.x: 0
*form25.y: -2
*form25.width: 408
*form25.height: 348
*form25.background: WindowBackground

*form26.class: form
*form26.parent: form25
*form26.static: true
*form26.name: form26
*form26.resizePolicy: "resize_none"
*form26.x: 6
*form26.y: 490
*form26.width: 480
*form26.height: 40
*form26.background: ButtonBackground

*pb_search_search2.class: pushButton
*pb_search_search2.parent: form26
*pb_search_search2.static: true
*pb_search_search2.name: pb_search_search2
*pb_search_search2.x: 8
*pb_search_search2.y: 4
*pb_search_search2.width: 86
*pb_search_search2.height: 30
*pb_search_search2.background: ButtonBackground
*pb_search_search2.fontList: BoldTextFont
*pb_search_search2.foreground: ApplyForeground
*pb_search_search2.labelString: "Scan"
*pb_search_search2.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}
*pb_search_search2.translations: ""

*pushButton2.class: pushButton
*pushButton2.parent: form26
*pushButton2.static: true
*pushButton2.name: pushButton2
*pushButton2.x: 196
*pushButton2.y: 4
*pushButton2.width: 86
*pushButton2.height: 30
*pushButton2.background: ButtonBackground
*pushButton2.fontList: BoldTextFont
*pushButton2.foreground: CancelForeground
*pushButton2.labelString: "Cancel"
*pushButton2.activateCallback: {\
UxPopdownInterface(UxFindSwidget("RotateShell"));\
}
*pushButton2.translations: ""

*pb_search_plot2.class: pushButton
*pb_search_plot2.parent: form26
*pb_search_plot2.static: true
*pb_search_plot2.name: pb_search_plot2
*pb_search_plot2.x: 102
*pb_search_plot2.y: 4
*pb_search_plot2.width: 86
*pb_search_plot2.height: 30
*pb_search_plot2.background: ButtonBackground
*pb_search_plot2.fontList: BoldTextFont
*pb_search_plot2.foreground: ButtonForeground
*pb_search_plot2.labelString: "Help..."
*pb_search_plot2.activateCallback: {\
GetExtendedHelp(UxWidget);\
}
*pb_search_plot2.translations: ""

*shelp_search2.class: text
*shelp_search2.parent: form25
*shelp_search2.static: true
*shelp_search2.name: shelp_search2
*shelp_search2.x: 8
*shelp_search2.y: 428
*shelp_search2.width: 484
*shelp_search2.height: 50
*shelp_search2.background: SHelpBackground
*shelp_search2.cursorPositionVisible: "false"
*shelp_search2.editable: "false"
*shelp_search2.fontList: TextFont

*separator20.class: separator
*separator20.parent: form25
*separator20.static: true
*separator20.name: separator20
*separator20.x: 4
*separator20.y: 280
*separator20.width: 492
*separator20.height: 10
*separator20.background: WindowBackground

*separator21.class: separator
*separator21.parent: form25
*separator21.static: true
*separator21.name: separator21
*separator21.x: 2
*separator21.y: 478
*separator21.width: 492
*separator21.height: 10
*separator21.background: WindowBackground

*mn_tol2.class: rowColumn
*mn_tol2.parent: form25
*mn_tol2.static: true
*mn_tol2.name: mn_tol2
*mn_tol2.rowColumnType: "menu_option"
*mn_tol2.subMenuId: "menu2_p2"
*mn_tol2.x: 206
*mn_tol2.y: 38
*mn_tol2.background: WindowBackground
*mn_tol2.foreground: TextForeground
*mn_tol2.marginWidth: 0
*mn_tol2.spacing: 0
*mn_tol2.labelString: " "
*mn_tol2.translations: transTable8

*menu2_p2.class: rowColumn
*menu2_p2.parent: mn_tol2
*menu2_p2.static: true
*menu2_p2.name: menu2_p2
*menu2_p2.rowColumnType: "menu_pulldown"
*menu2_p2.background: WindowBackground
*menu2_p2.foreground: TextForeground
*menu2_p2.translations: ""

*mn_tol_angstroms2.class: pushButtonGadget
*mn_tol_angstroms2.parent: menu2_p2
*mn_tol_angstroms2.static: true
*mn_tol_angstroms2.name: mn_tol_angstroms2
*mn_tol_angstroms2.labelString: " Emmi "
*mn_tol_angstroms2.fontList: BoldTextFont
*mn_tol_angstroms2.activateCallback: {RadioSet(UxWidget);}\
\


*mn_tol_pixels2.class: pushButtonGadget
*mn_tol_pixels2.parent: menu2_p2
*mn_tol_pixels2.static: true
*mn_tol_pixels2.name: mn_tol_pixels2
*mn_tol_pixels2.labelString: " Caspec "
*mn_tol_pixels2.fontList: BoldTextFont
*mn_tol_pixels2.activateCallback: {RadioSet(UxWidget);}

*menu2_p2_b3.class: pushButtonGadget
*menu2_p2_b3.parent: menu2_p2
*menu2_p2_b3.static: true
*menu2_p2_b3.name: menu2_p2_b3
*menu2_p2_b3.labelString: " Efosc "
*menu2_p2_b3.fontList: BoldTextFont
*menu2_p2_b3.activateCallback: {RadioSet(UxWidget);}\


*menu2_p2_b4.class: pushButtonGadget
*menu2_p2_b4.parent: menu2_p2
*menu2_p2_b4.static: true
*menu2_p2_b4.name: menu2_p2_b4
*menu2_p2_b4.labelString: " Echelec "
*menu2_p2_b4.fontList: BoldTextFont
*menu2_p2_b4.activateCallback: {RadioSet(UxWidget);}

*mn_tol3.class: rowColumn
*mn_tol3.parent: form25
*mn_tol3.static: true
*mn_tol3.name: mn_tol3
*mn_tol3.rowColumnType: "menu_option"
*mn_tol3.subMenuId: "menu2_p3"
*mn_tol3.x: 206
*mn_tol3.y: 72
*mn_tol3.background: WindowBackground
*mn_tol3.foreground: TextForeground
*mn_tol3.marginWidth: 0
*mn_tol3.spacing: 0
*mn_tol3.labelString: " "
*mn_tol3.translations: transTable8

*menu2_p3.class: rowColumn
*menu2_p3.parent: mn_tol3
*menu2_p3.static: true
*menu2_p3.name: menu2_p3
*menu2_p3.rowColumnType: "menu_pulldown"
*menu2_p3.background: WindowBackground
*menu2_p3.foreground: TextForeground

*mn_tol_angstroms3.class: pushButtonGadget
*mn_tol_angstroms3.parent: menu2_p3
*mn_tol_angstroms3.static: true
*mn_tol_angstroms3.name: mn_tol_angstroms3
*mn_tol_angstroms3.labelString: " No Flip "
*mn_tol_angstroms3.activateCallback: {RadioSet ( UxWidget ) ; }
*mn_tol_angstroms3.fontList: BoldTextFont

*mn_tol_pixels3.class: pushButtonGadget
*mn_tol_pixels3.parent: menu2_p3
*mn_tol_pixels3.static: true
*mn_tol_pixels3.name: mn_tol_pixels3
*mn_tol_pixels3.labelString: " Flip X "
*mn_tol_pixels3.activateCallback: {RadioSet ( UxWidget ) ; }\

*mn_tol_pixels3.fontList: BoldTextFont

*menu2_p3_b4.class: pushButtonGadget
*menu2_p3_b4.parent: menu2_p3
*menu2_p3_b4.static: true
*menu2_p3_b4.name: menu2_p3_b4
*menu2_p3_b4.labelString: " Flip Y "
*menu2_p3_b4.fontList: BoldTextFont
*menu2_p3_b4.activateCallback: {RadioSet ( UxWidget ) ; }

*menu2_p3_b3.class: pushButtonGadget
*menu2_p3_b3.parent: menu2_p3
*menu2_p3_b3.static: true
*menu2_p3_b3.name: menu2_p3_b3
*menu2_p3_b3.labelString: " Flip XY "
*menu2_p3_b3.fontList: BoldTextFont
*menu2_p3_b3.activateCallback: {RadioSet ( UxWidget ) ; }

*mn_tol5.class: rowColumn
*mn_tol5.parent: form25
*mn_tol5.static: true
*mn_tol5.name: mn_tol5
*mn_tol5.rowColumnType: "menu_option"
*mn_tol5.subMenuId: "menu2_p5"
*mn_tol5.x: 324
*mn_tol5.y: 72
*mn_tol5.background: WindowBackground
*mn_tol5.foreground: TextForeground
*mn_tol5.marginWidth: 0
*mn_tol5.spacing: 0
*mn_tol5.labelString: " "
*mn_tol5.translations: transTable8

*menu2_p5.class: rowColumn
*menu2_p5.parent: mn_tol5
*menu2_p5.static: true
*menu2_p5.name: menu2_p5
*menu2_p5.rowColumnType: "menu_pulldown"
*menu2_p5.background: WindowBackground
*menu2_p5.foreground: TextForeground

*mn_tol_angstroms5.class: pushButtonGadget
*mn_tol_angstroms5.parent: menu2_p5
*mn_tol_angstroms5.static: true
*mn_tol_angstroms5.name: mn_tol_angstroms5
*mn_tol_angstroms5.labelString: " No Rotation "
*mn_tol_angstroms5.activateCallback: {RadioSet ( UxWidget ) ; }
*mn_tol_angstroms5.fontList: BoldTextFont

*mn_tol_pixels5.class: pushButtonGadget
*mn_tol_pixels5.parent: menu2_p5
*mn_tol_pixels5.static: true
*mn_tol_pixels5.name: mn_tol_pixels5
*mn_tol_pixels5.labelString: " Rotate 90 "
*mn_tol_pixels5.activateCallback: {RadioSet ( UxWidget ) ; }
*mn_tol_pixels5.fontList: BoldTextFont

*menu2_p5_b3.class: pushButtonGadget
*menu2_p5_b3.parent: menu2_p5
*menu2_p5_b3.static: true
*menu2_p5_b3.name: menu2_p5_b3
*menu2_p5_b3.labelString: " Rotate 180 "
*menu2_p5_b3.fontList: BoldTextFont
*menu2_p5_b3.activateCallback: {RadioSet ( UxWidget ) ; }

*menu2_p5_b4.class: pushButtonGadget
*menu2_p5_b4.parent: menu2_p5
*menu2_p5_b4.static: true
*menu2_p5_b4.name: menu2_p5_b4
*menu2_p5_b4.labelString: " Rotate 270 "
*menu2_p5_b4.fontList: BoldTextFont
*menu2_p5_b4.activateCallback: {RadioSet ( UxWidget ) ; }

*label40.class: label
*label40.parent: form25
*label40.static: true
*label40.name: label40
*label40.x: 20
*label40.y: 236
*label40.width: 150
*label40.height: 30
*label40.background: LabelBackground
*label40.fontList: TextFont
*label40.labelString: "Binning Factor :"
*label40.alignment: "alignment_beginning"
*label40.foreground: TextForeground

*tf_alpha2.class: textField
*tf_alpha2.parent: form25
*tf_alpha2.static: true
*tf_alpha2.name: tf_alpha2
*tf_alpha2.x: 218
*tf_alpha2.y: 376
*tf_alpha2.width: 96
*tf_alpha2.height: 32
*tf_alpha2.background: TextBackground
*tf_alpha2.fontList: TextFont
*tf_alpha2.highlightOnEnter: "true"
*tf_alpha2.foreground: TextForeground
*tf_alpha2.losingFocusCallback: {\
\
}
*tf_alpha2.translations: ""

*label41.class: label
*label41.parent: form25
*label41.static: true
*label41.name: label41
*label41.x: 14
*label41.y: 378
*label41.width: 150
*label41.height: 30
*label41.background: LabelBackground
*label41.fontList: TextFont
*label41.labelString: "Scan Limits (pixels) :"
*label41.alignment: "alignment_beginning"
*label41.foreground: TextForeground

*tf_alpha4.class: textField
*tf_alpha4.parent: form25
*tf_alpha4.static: true
*tf_alpha4.name: tf_alpha4
*tf_alpha4.x: 374
*tf_alpha4.y: 376
*tf_alpha4.width: 96
*tf_alpha4.height: 32
*tf_alpha4.background: TextBackground
*tf_alpha4.fontList: TextFont
*tf_alpha4.highlightOnEnter: "true"
*tf_alpha4.foreground: TextForeground
*tf_alpha4.losingFocusCallback: {\
\
}
*tf_alpha4.translations: ""

*tf_alpha5.class: textField
*tf_alpha5.parent: form25
*tf_alpha5.static: true
*tf_alpha5.name: tf_alpha5
*tf_alpha5.x: 372
*tf_alpha5.y: 232
*tf_alpha5.width: 96
*tf_alpha5.height: 32
*tf_alpha5.background: TextBackground
*tf_alpha5.fontList: TextFont
*tf_alpha5.highlightOnEnter: "true"
*tf_alpha5.foreground: TextForeground
*tf_alpha5.losingFocusCallback: {\
\
}
*tf_alpha5.translations: ""

*tf_alpha6.class: textField
*tf_alpha6.parent: form25
*tf_alpha6.static: true
*tf_alpha6.name: tf_alpha6
*tf_alpha6.x: 216
*tf_alpha6.y: 108
*tf_alpha6.width: 97
*tf_alpha6.height: 32
*tf_alpha6.background: TextBackground
*tf_alpha6.fontList: TextFont
*tf_alpha6.highlightOnEnter: "true"
*tf_alpha6.foreground: TextForeground
*tf_alpha6.losingFocusCallback: {\
\
}
*tf_alpha6.text: "1."
*tf_alpha6.translations: ""

*label54.class: label
*label54.parent: form25
*label54.static: true
*label54.name: label54
*label54.x: 12
*label54.y: 110
*label54.width: 204
*label54.height: 30
*label54.background: LabelBackground
*label54.fontList: TextFont
*label54.labelString: "Default Exposure Time (s):"
*label54.alignment: "alignment_beginning"
*label54.foreground: TextForeground

*rowColumn23.class: rowColumn
*rowColumn23.parent: form25
*rowColumn23.static: true
*rowColumn23.name: rowColumn23
*rowColumn23.x: 8
*rowColumn23.y: 42
*rowColumn23.width: 200
*rowColumn23.height: 67
*rowColumn23.background: WindowBackground
*rowColumn23.radioBehavior: "true"
*rowColumn23.resizeWidth: "false"

*toggleButton14.class: toggleButton
*toggleButton14.parent: rowColumn23
*toggleButton14.static: true
*toggleButton14.name: toggleButton14
*toggleButton14.x: 3
*toggleButton14.y: 3
*toggleButton14.width: 150
*toggleButton14.height: 29
*toggleButton14.recomputeSize: "false"
*toggleButton14.background: WindowBackground
*toggleButton14.fontList: TextFont
*toggleButton14.foreground: TextForeground
*toggleButton14.labelString: "Standard Orientation for:"
*toggleButton14.selectColor: "Yellow"
*toggleButton14.valueChangedCallback: {\
\
}
*toggleButton14.armCallback: {\
RadioSet(UxWidget);\
}

*toggleButton15.class: toggleButton
*toggleButton15.parent: rowColumn23
*toggleButton15.static: true
*toggleButton15.name: toggleButton15
*toggleButton15.x: 12
*toggleButton15.y: 14
*toggleButton15.width: 234
*toggleButton15.height: 29
*toggleButton15.recomputeSize: "false"
*toggleButton15.background: WindowBackground
*toggleButton15.fontList: TextFont
*toggleButton15.foreground: TextForeground
*toggleButton15.labelString: "Rotation Parameters:"
*toggleButton15.selectColor: "Yellow"
*toggleButton15.valueChangedCallback: {\
\
}
*toggleButton15.set: "true"
*toggleButton15.armCallback: {\
RadioSet(UxWidget);\
}

*label44.class: label
*label44.parent: form25
*label44.static: true
*label44.name: label44
*label44.x: 20
*label44.y: 192
*label44.width: 150
*label44.height: 30
*label44.background: LabelBackground
*label44.fontList: TextFont
*label44.labelString: "Image size (pixels):"
*label44.alignment: "alignment_beginning"
*label44.foreground: TextForeground

*label55.class: label
*label55.parent: form25
*label55.static: true
*label55.name: label55
*label55.x: 172
*label55.y: 378
*label55.width: 40
*label55.height: 30
*label55.background: LabelBackground
*label55.fontList: TextFont
*label55.labelString: "Ymin="
*label55.alignment: "alignment_beginning"
*label55.foreground: TextForeground

*label56.class: label
*label56.parent: form25
*label56.static: true
*label56.name: label56
*label56.x: 198
*label56.y: 232
*label56.width: 40
*label56.height: 30
*label56.background: LabelBackground
*label56.fontList: TextFont
*label56.labelString: "X="
*label56.alignment: "alignment_beginning"
*label56.foreground: TextForeground

*label57.class: label
*label57.parent: form25
*label57.static: true
*label57.name: label57
*label57.x: 352
*label57.y: 186
*label57.width: 20
*label57.height: 30
*label57.background: LabelBackground
*label57.fontList: TextFont
*label57.labelString: "Y="
*label57.alignment: "alignment_beginning"
*label57.foreground: TextForeground

*label58.class: label
*label58.parent: form25
*label58.static: true
*label58.name: label58
*label58.x: 210
*label58.y: 294
*label58.width: 90
*label58.height: 30
*label58.background: LabelBackground
*label58.fontList: BoldTextFont
*label58.labelString: "Scan"
*label58.alignment: "alignment_center"
*label58.foreground: TextForeground

*label59.class: label
*label59.parent: form25
*label59.static: true
*label59.name: label59
*label59.x: 354
*label59.y: 222
*label59.width: 20
*label59.height: 30
*label59.background: LabelBackground
*label59.fontList: TextFont
*label59.labelString: "Y="
*label59.alignment: "alignment_beginning"
*label59.foreground: TextForeground

*label60.class: label
*label60.parent: form25
*label60.static: true
*label60.name: label60
*label60.x: 202
*label60.y: 188
*label60.width: 40
*label60.height: 30
*label60.background: LabelBackground
*label60.fontList: TextFont
*label60.labelString: "X="
*label60.alignment: "alignment_beginning"
*label60.foreground: TextForeground

*separator34.class: separator
*separator34.parent: form25
*separator34.static: true
*separator34.name: separator34
*separator34.x: 4
*separator34.y: 420
*separator34.width: 492
*separator34.height: 10
*separator34.background: WindowBackground

*label49.class: label
*label49.parent: form25
*label49.static: true
*label49.name: label49
*label49.x: 322
*label49.y: 376
*label49.width: 50
*label49.height: 30
*label49.background: LabelBackground
*label49.fontList: TextFont
*label49.labelString: "Ymax="
*label49.alignment: "alignment_beginning"
*label49.foreground: TextForeground

*label50.class: label
*label50.parent: form25
*label50.static: true
*label50.name: label50
*label50.x: 206
*label50.y: 2
*label50.width: 90
*label50.height: 30
*label50.background: LabelBackground
*label50.fontList: BoldTextFont
*label50.labelString: "Rotation"
*label50.alignment: "alignment_center"
*label50.foreground: TextForeground

*separator35.class: separator
*separator35.parent: form25
*separator35.static: true
*separator35.name: separator35
*separator35.x: 2
*separator35.y: 142
*separator35.width: 492
*separator35.height: 10
*separator35.background: WindowBackground

*label51.class: label
*label51.parent: form25
*label51.static: true
*label51.name: label51
*label51.x: 206
*label51.y: 154
*label51.width: 90
*label51.height: 30
*label51.background: LabelBackground
*label51.fontList: BoldTextFont
*label51.labelString: "Geometry"
*label51.alignment: "alignment_center"
*label51.foreground: TextForeground

*pb_main_search32.class: pushButton
*pb_main_search32.parent: form25
*pb_main_search32.static: true
*pb_main_search32.name: pb_main_search32
*pb_main_search32.x: 424
*pb_main_search32.y: 332
*pb_main_search32.height: 28
*pb_main_search32.background: ButtonBackground
*pb_main_search32.fontList: BoldTextFont
*pb_main_search32.foreground: ButtonForeground
*pb_main_search32.labelString: "..."
*pb_main_search32.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search32.recomputeSize: "true"
*pb_main_search32.width: 46
*pb_main_search32.translations: ""

*label52.class: label
*label52.parent: form25
*label52.static: true
*label52.name: label52
*label52.x: 14
*label52.y: 330
*label52.width: 116
*label52.height: 30
*label52.background: LabelBackground
*label52.fontList: TextFont
*label52.labelString: "Reference Image:"
*label52.alignment: "alignment_beginning"
*label52.foreground: TextForeground

*tf_alpha1.class: textField
*tf_alpha1.parent: form25
*tf_alpha1.static: true
*tf_alpha1.name: tf_alpha1
*tf_alpha1.x: 248
*tf_alpha1.y: 184
*tf_alpha1.width: 96
*tf_alpha1.height: 32
*tf_alpha1.background: TextBackground
*tf_alpha1.fontList: TextFont
*tf_alpha1.highlightOnEnter: "true"
*tf_alpha1.foreground: TextForeground
*tf_alpha1.losingFocusCallback: {\
\
}
*tf_alpha1.translations: ""

*tf_alpha3.class: textField
*tf_alpha3.parent: form25
*tf_alpha3.static: true
*tf_alpha3.name: tf_alpha3
*tf_alpha3.x: 248
*tf_alpha3.y: 230
*tf_alpha3.width: 96
*tf_alpha3.height: 32
*tf_alpha3.background: TextBackground
*tf_alpha3.fontList: TextFont
*tf_alpha3.highlightOnEnter: "true"
*tf_alpha3.foreground: TextForeground
*tf_alpha3.losingFocusCallback: {\
\
}
*tf_alpha3.translations: ""

*tf_alpha7.class: textField
*tf_alpha7.parent: form25
*tf_alpha7.static: true
*tf_alpha7.name: tf_alpha7
*tf_alpha7.x: 164
*tf_alpha7.y: 328
*tf_alpha7.width: 242
*tf_alpha7.height: 32
*tf_alpha7.background: TextBackground
*tf_alpha7.fontList: TextFont
*tf_alpha7.highlightOnEnter: "true"
*tf_alpha7.foreground: TextForeground
*tf_alpha7.losingFocusCallback: {\
\
}
*tf_alpha7.translations: ""

*tf_alpha44.class: textField
*tf_alpha44.parent: form25
*tf_alpha44.static: true
*tf_alpha44.name: tf_alpha44
*tf_alpha44.x: 374
*tf_alpha44.y: 186
*tf_alpha44.width: 96
*tf_alpha44.height: 32
*tf_alpha44.background: TextBackground
*tf_alpha44.fontList: TextFont
*tf_alpha44.highlightOnEnter: "true"
*tf_alpha44.foreground: TextForeground
*tf_alpha44.losingFocusCallback: {\
\
}
*tf_alpha44.translations: ""

