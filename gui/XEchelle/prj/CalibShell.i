! UIMX ascii 2.0 key: 369                                                       
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  

*translation.table: SelectFileCalib
*translation.parent: CalibShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable3
*translation.parent: CalibShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable4
*translation.parent: CalibShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable5
*translation.parent: CalibShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable6
*translation.parent: CalibShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*CalibShell.class: applicationShell
*CalibShell.parent: NO_PARENT
*CalibShell.static: true
*CalibShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*CalibShell.ispecdecl:
*CalibShell.funcdecl: swidget create_CalibShell()\

*CalibShell.funcname: create_CalibShell
*CalibShell.funcdef: "swidget", "<create_CalibShell>(%)"
*CalibShell.icode:
*CalibShell.fcode: return(rtrn);\

*CalibShell.auxdecl:
*CalibShell.name: CalibShell
*CalibShell.x: 29
*CalibShell.y: 327
*CalibShell.width: 520
*CalibShell.height: 520
*CalibShell.iconName: "Calibration"
*CalibShell.keyboardFocusPolicy: "pointer"
*CalibShell.defaultFontList: TextFont
*CalibShell.geometry: "+10+60"

*form3.class: form
*form3.parent: CalibShell
*form3.static: true
*form3.name: form3
*form3.resizePolicy: "resize_none"
*form3.unitType: "pixels"
*form3.x: 2
*form3.y: 0
*form3.width: 600
*form3.height: 616
*form3.background: "grey80"

*rowColumn2.class: rowColumn
*rowColumn2.parent: form3
*rowColumn2.static: true
*rowColumn2.name: rowColumn2
*rowColumn2.x: 368
*rowColumn2.y: 304
*rowColumn2.width: 402
*rowColumn2.height: 31
*rowColumn2.radioBehavior: "true"
*rowColumn2.background: "grey80"
*rowColumn2.entryBorder: 0
*rowColumn2.labelString: ""
*rowColumn2.shadowThickness: 0
*rowColumn2.borderWidth: 0
*rowColumn2.entryAlignment: "alignment_beginning"
*rowColumn2.isAligned: "true"
*rowColumn2.orientation: "vertical"
*rowColumn2.packing: "pack_tight"
*rowColumn2.translations: transTable6

*rb_wlcmtd_iden.class: toggleButton
*rb_wlcmtd_iden.parent: rowColumn2
*rb_wlcmtd_iden.static: true
*rb_wlcmtd_iden.name: rb_wlcmtd_iden
*rb_wlcmtd_iden.x: 3
*rb_wlcmtd_iden.y: 3
*rb_wlcmtd_iden.width: 77
*rb_wlcmtd_iden.height: 113
*rb_wlcmtd_iden.background: WindowBackground
*rb_wlcmtd_iden.fontList: TextFont
*rb_wlcmtd_iden.labelString: "Yes"
*rb_wlcmtd_iden.set: "true"
*rb_wlcmtd_iden.selectColor: SelectColor
*rb_wlcmtd_iden.foreground: TextForeground
*rb_wlcmtd_iden.highlightOnEnter: "true"
*rb_wlcmtd_iden.indicatorSize: 16
*rb_wlcmtd_iden.valueChangedCallback: {\
\
\
}
*rb_wlcmtd_iden.translations: transTable6
*rb_wlcmtd_iden.armCallback: RadioSet ( UxWidget ) ;

*rb_wlcmtd_gues.class: toggleButton
*rb_wlcmtd_gues.parent: rowColumn2
*rb_wlcmtd_gues.static: true
*rb_wlcmtd_gues.name: rb_wlcmtd_gues
*rb_wlcmtd_gues.x: 3
*rb_wlcmtd_gues.y: 34
*rb_wlcmtd_gues.width: 93
*rb_wlcmtd_gues.height: 32
*rb_wlcmtd_gues.background: WindowBackground
*rb_wlcmtd_gues.fontList: TextFont
*rb_wlcmtd_gues.labelString: "No"
*rb_wlcmtd_gues.selectColor: SelectColor
*rb_wlcmtd_gues.foreground: TextForeground
*rb_wlcmtd_gues.valueChangedCallback: {\
\
\
}
*rb_wlcmtd_gues.set: "false"
*rb_wlcmtd_gues.highlightOnEnter: "true"
*rb_wlcmtd_gues.indicatorSize: 16
*rb_wlcmtd_gues.translations: transTable6
*rb_wlcmtd_gues.armCallback: RadioSet ( UxWidget ) ;

*label14.class: label
*label14.parent: form3
*label14.static: true
*label14.name: label14
*label14.x: 14
*label14.y: 56
*label14.width: 60
*label14.height: 18
*label14.background: WindowBackground
*label14.fontList: TextFont
*label14.labelString: "Method"
*label14.alignment: "alignment_beginning"
*label14.foreground: TextForeground
*label14.leftAttachment: "attach_form"
*label14.leftOffset: 10

*label16.class: label
*label16.parent: form3
*label16.static: true
*label16.name: label16
*label16.x: 14
*label16.y: 124
*label16.width: 130
*label16.height: 30
*label16.background: LabelBackground
*label16.fontList: TextFont
*label16.labelString: "Tolerance :"
*label16.alignment: "alignment_beginning"
*label16.foreground: TextForeground
*label16.leftAttachment: "attach_form"
*label16.leftOffset: 10

*tf_tol.class: textField
*tf_tol.parent: form3
*tf_tol.static: true
*tf_tol.name: tf_tol
*tf_tol.x: 238
*tf_tol.y: 120
*tf_tol.width: 100
*tf_tol.height: 34
*tf_tol.background: TextBackground
*tf_tol.fontList: TextFont
*tf_tol.highlightOnEnter: "true"
*tf_tol.foreground: TextForeground
*tf_tol.losingFocusCallback: {\
\
}

*label17.class: label
*label17.parent: form3
*label17.static: true
*label17.name: label17
*label17.x: 14
*label17.y: 164
*label17.width: 200
*label17.height: 30
*label17.background: LabelBackground
*label17.fontList: TextFont
*label17.labelString: "Degree of polynomial :"
*label17.alignment: "alignment_beginning"
*label17.foreground: TextForeground
*label17.leftAttachment: "attach_form"
*label17.leftOffset: 10

*tf_dcx1.class: textField
*tf_dcx1.parent: form3
*tf_dcx1.static: true
*tf_dcx1.name: tf_dcx1
*tf_dcx1.x: 238
*tf_dcx1.y: 158
*tf_dcx1.width: 100
*tf_dcx1.height: 34
*tf_dcx1.background: TextBackground
*tf_dcx1.fontList: TextFont
*tf_dcx1.highlightOnEnter: "true"
*tf_dcx1.foreground: TextForeground
*tf_dcx1.losingFocusCallback: {\
\
}

*tf_alpha.class: textField
*tf_alpha.parent: form3
*tf_alpha.static: true
*tf_alpha.name: tf_alpha
*tf_alpha.x: 238
*tf_alpha.y: 238
*tf_alpha.width: 96
*tf_alpha.height: 32
*tf_alpha.background: TextBackground
*tf_alpha.fontList: TextFont
*tf_alpha.highlightOnEnter: "true"
*tf_alpha.foreground: TextForeground
*tf_alpha.losingFocusCallback: {\
\
}

*label19.class: label
*label19.parent: form3
*label19.static: true
*label19.name: label19
*label19.x: 14
*label19.y: 274
*label19.width: 200
*label19.height: 30
*label19.background: LabelBackground
*label19.fontList: TextFont
*label19.labelString: "Matching parameter :"
*label19.alignment: "alignment_beginning"
*label19.foreground: TextForeground
*label19.leftAttachment: "attach_form"
*label19.leftOffset: 10

*label20.class: label
*label20.parent: form3
*label20.static: true
*label20.name: label20
*label20.x: 14
*label20.y: 354
*label20.width: 200
*label20.height: 30
*label20.background: LabelBackground
*label20.fontList: TextFont
*label20.labelString: "Maximum deviation (pixels) :"
*label20.alignment: "alignment_beginning"
*label20.foreground: TextForeground
*label20.leftAttachment: "attach_form"
*label20.leftOffset: 10

*tf_maxdev.class: textField
*tf_maxdev.parent: form3
*tf_maxdev.static: true
*tf_maxdev.name: tf_maxdev
*tf_maxdev.x: 238
*tf_maxdev.y: 312
*tf_maxdev.width: 100
*tf_maxdev.height: 34
*tf_maxdev.background: TextBackground
*tf_maxdev.fontList: TextFont
*tf_maxdev.highlightOnEnter: "true"
*tf_maxdev.foreground: TextForeground
*tf_maxdev.losingFocusCallback: {\
\
}

*guess_session_label.class: label
*guess_session_label.parent: form3
*guess_session_label.static: true
*guess_session_label.name: guess_session_label
*guess_session_label.x: 14
*guess_session_label.y: 88
*guess_session_label.width: 100
*guess_session_label.height: 30
*guess_session_label.background: LabelBackground
*guess_session_label.fontList: TextFont
*guess_session_label.labelString: "Initial guess :"
*guess_session_label.alignment: "alignment_beginning"
*guess_session_label.foreground: TextForeground
*guess_session_label.sensitive: "false"
*guess_session_label.leftAttachment: "attach_form"
*guess_session_label.leftOffset: 10

*separator4.class: separator
*separator4.parent: form3
*separator4.static: true
*separator4.name: separator4
*separator4.x: 356
*separator4.y: 252
*separator4.width: 110
*separator4.height: 11
*separator4.background: WindowBackground

*shelp_calib.class: text
*shelp_calib.parent: form3
*shelp_calib.static: true
*shelp_calib.name: shelp_calib
*shelp_calib.x: 8
*shelp_calib.y: 404
*shelp_calib.width: 510
*shelp_calib.height: 50
*shelp_calib.background: SHelpBackground
*shelp_calib.cursorPositionVisible: "false"
*shelp_calib.editable: "false"
*shelp_calib.fontList: TextFont

*separator5.class: separator
*separator5.parent: form3
*separator5.static: true
*separator5.name: separator5
*separator5.x: 4
*separator5.y: 456
*separator5.width: 516
*separator5.height: 6
*separator5.background: WindowBackground

*tf_wlcniter1.class: textField
*tf_wlcniter1.parent: form3
*tf_wlcniter1.static: true
*tf_wlcniter1.name: tf_wlcniter1
*tf_wlcniter1.x: 238
*tf_wlcniter1.y: 196
*tf_wlcniter1.width: 48
*tf_wlcniter1.height: 34
*tf_wlcniter1.background: TextBackground
*tf_wlcniter1.fontList: TextFont
*tf_wlcniter1.highlightOnEnter: "true"
*tf_wlcniter1.foreground: TextForeground
*tf_wlcniter1.losingFocusCallback: {\
\
}

*tf_wlcniter2.class: textField
*tf_wlcniter2.parent: form3
*tf_wlcniter2.static: true
*tf_wlcniter2.name: tf_wlcniter2
*tf_wlcniter2.x: 288
*tf_wlcniter2.y: 196
*tf_wlcniter2.width: 48
*tf_wlcniter2.height: 34
*tf_wlcniter2.background: TextBackground
*tf_wlcniter2.fontList: TextFont
*tf_wlcniter2.highlightOnEnter: "true"
*tf_wlcniter2.foreground: TextForeground
*tf_wlcniter2.losingFocusCallback: {\
\
}

*tg_coropt.class: toggleButton
*tg_coropt.parent: form3
*tg_coropt.static: true
*tg_coropt.name: tg_coropt
*tg_coropt.x: 302
*tg_coropt.y: 82
*tg_coropt.width: 206
*tg_coropt.height: 26
*tg_coropt.background: WindowBackground
*tg_coropt.fontList: TextFont
*tg_coropt.labelString: "Compute Cross-correlation"
*tg_coropt.set: "false"
*tg_coropt.selectColor: SelectColor
*tg_coropt.foreground: TextForeground
*tg_coropt.highlightOnEnter: "true"
*tg_coropt.sensitive: "false"
*tg_coropt.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( !UpdateToggle )\
    return;\
\
if ( XmToggleButtonGetState(UxWidget) )\
    AppendDialogText("set/long CORVISU=YES COROPT=YES");\
else\
    AppendDialogText("set/long CORVISU=NO COROPT=NO");\
}
*tg_coropt.indicatorSize: 16

*form20.class: form
*form20.parent: form3
*form20.static: true
*form20.name: form20
*form20.resizePolicy: "resize_none"
*form20.x: 4
*form20.y: 466
*form20.width: 520
*form20.height: 48
*form20.background: ButtonBackground

*pb_calib_calibrate.class: pushButton
*pb_calib_calibrate.parent: form20
*pb_calib_calibrate.static: true
*pb_calib_calibrate.name: pb_calib_calibrate
*pb_calib_calibrate.x: 4
*pb_calib_calibrate.y: 6
*pb_calib_calibrate.width: 86
*pb_calib_calibrate.height: 30
*pb_calib_calibrate.background: ButtonBackground
*pb_calib_calibrate.fontList: BoldTextFont
*pb_calib_calibrate.foreground: ApplyForeground
*pb_calib_calibrate.labelString: "Calibrate"
*pb_calib_calibrate.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pushButton40.class: pushButton
*pushButton40.parent: form20
*pushButton40.static: true
*pushButton40.name: pushButton40
*pushButton40.x: 430
*pushButton40.y: 6
*pushButton40.width: 86
*pushButton40.height: 30
*pushButton40.background: ButtonBackground
*pushButton40.fontList: BoldTextFont
*pushButton40.foreground: CancelForeground
*pushButton40.labelString: "Cancel"
*pushButton40.activateCallback: {\
UxPopdownInterface(UxFindSwidget("CalibShell"));\
}

*pb_calib_disper.class: pushButton
*pb_calib_disper.parent: form20
*pb_calib_disper.static: true
*pb_calib_disper.name: pb_calib_disper
*pb_calib_disper.x: 344
*pb_calib_disper.y: 6
*pb_calib_disper.width: 86
*pb_calib_disper.height: 30
*pb_calib_disper.background: ButtonBackground
*pb_calib_disper.fontList: BoldTextFont
*pb_calib_disper.foreground: ButtonForeground
*pb_calib_disper.labelString: "Help"
*pb_calib_disper.activateCallback: {\
GetExtendedHelp(UxWidget);\
\
}

*pb_calib_resid.class: pushButton
*pb_calib_resid.parent: form20
*pb_calib_resid.static: true
*pb_calib_resid.name: pb_calib_resid
*pb_calib_resid.x: 92
*pb_calib_resid.y: 6
*pb_calib_resid.width: 86
*pb_calib_resid.height: 30
*pb_calib_resid.background: ButtonBackground
*pb_calib_resid.fontList: BoldTextFont
*pb_calib_resid.foreground: ButtonForeground
*pb_calib_resid.labelString: "Residuals"
*pb_calib_resid.activateCallback: {\
 MidasCommand(UxThisWidget);\
\
}

*pb_calib_spec.class: pushButton
*pb_calib_spec.parent: form20
*pb_calib_spec.static: true
*pb_calib_spec.name: pb_calib_spec
*pb_calib_spec.x: 180
*pb_calib_spec.y: 6
*pb_calib_spec.width: 86
*pb_calib_spec.height: 30
*pb_calib_spec.background: ButtonBackground
*pb_calib_spec.fontList: BoldTextFont
*pb_calib_spec.foreground: ButtonForeground
*pb_calib_spec.labelString: "Spectrum"
*pb_calib_spec.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*label8.class: label
*label8.parent: form3
*label8.static: true
*label8.name: label8
*label8.x: 14
*label8.y: 314
*label8.width: 200
*label8.height: 30
*label8.background: LabelBackground
*label8.fontList: TextFont
*label8.labelString: "Initial Tolerance (pixels) :"
*label8.alignment: "alignment_beginning"
*label8.foreground: TextForeground
*label8.leftAttachment: "attach_form"
*label8.leftOffset: 10

*tf_maxdev1.class: textField
*tf_maxdev1.parent: form3
*tf_maxdev1.static: true
*tf_maxdev1.name: tf_maxdev1
*tf_maxdev1.x: 238
*tf_maxdev1.y: 352
*tf_maxdev1.width: 100
*tf_maxdev1.height: 34
*tf_maxdev1.background: TextBackground
*tf_maxdev1.fontList: TextFont
*tf_maxdev1.highlightOnEnter: "true"
*tf_maxdev1.foreground: TextForeground
*tf_maxdev1.losingFocusCallback: {\
\
}

*tf_maxdev2.class: textField
*tf_maxdev2.parent: form3
*tf_maxdev2.static: true
*tf_maxdev2.name: tf_maxdev2
*tf_maxdev2.x: 238
*tf_maxdev2.y: 272
*tf_maxdev2.width: 100
*tf_maxdev2.height: 34
*tf_maxdev2.background: TextBackground
*tf_maxdev2.fontList: TextFont
*tf_maxdev2.highlightOnEnter: "true"
*tf_maxdev2.foreground: TextForeground
*tf_maxdev2.losingFocusCallback: {\
\
}

*tf_guess1.class: textField
*tf_guess1.parent: form3
*tf_guess1.static: true
*tf_guess1.name: tf_guess1
*tf_guess1.x: 178
*tf_guess1.y: 6
*tf_guess1.width: 278
*tf_guess1.height: 36
*tf_guess1.background: TextBackground
*tf_guess1.fontList: TextFont
*tf_guess1.highlightOnEnter: "true"
*tf_guess1.foreground: TextForeground
*tf_guess1.sensitive: "true"
*tf_guess1.losingFocusCallback: {\
\
}
*tf_guess1.translations: ""

*tf_guess2.class: textField
*tf_guess2.parent: form3
*tf_guess2.static: true
*tf_guess2.name: tf_guess2
*tf_guess2.x: 126
*tf_guess2.y: 80
*tf_guess2.width: 164
*tf_guess2.height: 36
*tf_guess2.background: TextBackground
*tf_guess2.fontList: TextFont
*tf_guess2.highlightOnEnter: "true"
*tf_guess2.foreground: TextForeground
*tf_guess2.sensitive: "false"
*tf_guess2.losingFocusCallback: {\
\
}
*tf_guess2.translations: ""

*label15.class: label
*label15.parent: form3
*label15.static: true
*label15.name: label15
*label15.x: 14
*label15.y: 6
*label15.width: 210
*label15.height: 30
*label15.background: LabelBackground
*label15.fontList: TextFont
*label15.labelString: "Line Catalog:"
*label15.alignment: "alignment_beginning"
*label15.foreground: TextForeground
*label15.leftAttachment: "attach_form"
*label15.leftOffset: 10

*label26.class: label
*label26.parent: form3
*label26.static: true
*label26.name: label26
*label26.x: 14
*label26.y: 238
*label26.width: 200
*label26.height: 30
*label26.background: LabelBackground
*label26.fontList: TextFont
*label26.labelString: "Maximum deviation (pixels) :"
*label26.alignment: "alignment_beginning"
*label26.foreground: TextForeground
*label26.leftAttachment: "attach_form"
*label26.leftOffset: 10

*rowColumn24.class: rowColumn
*rowColumn24.parent: form3
*rowColumn24.static: true
*rowColumn24.name: rowColumn24
*rowColumn24.x: 104
*rowColumn24.y: 43
*rowColumn24.width: 402
*rowColumn24.height: 31
*rowColumn24.radioBehavior: "true"
*rowColumn24.background: "grey80"
*rowColumn24.entryBorder: 0
*rowColumn24.labelString: ""
*rowColumn24.shadowThickness: 0
*rowColumn24.borderWidth: 0
*rowColumn24.entryAlignment: "alignment_beginning"
*rowColumn24.isAligned: "true"
*rowColumn24.orientation: "horizontal"
*rowColumn24.packing: "pack_tight"
*rowColumn24.translations: ""

*rb_wlcmtd_iden1.class: toggleButton
*rb_wlcmtd_iden1.parent: rowColumn24
*rb_wlcmtd_iden1.static: true
*rb_wlcmtd_iden1.name: rb_wlcmtd_iden1
*rb_wlcmtd_iden1.x: 3
*rb_wlcmtd_iden1.y: 2
*rb_wlcmtd_iden1.width: 76
*rb_wlcmtd_iden1.height: 20
*rb_wlcmtd_iden1.background: WindowBackground
*rb_wlcmtd_iden1.fontList: TextFont
*rb_wlcmtd_iden1.labelString: "PAIR"
*rb_wlcmtd_iden1.set: "true"
*rb_wlcmtd_iden1.selectColor: SelectColor
*rb_wlcmtd_iden1.foreground: TextForeground
*rb_wlcmtd_iden1.highlightOnEnter: "true"
*rb_wlcmtd_iden1.indicatorSize: 16
*rb_wlcmtd_iden1.valueChangedCallback: {\
\
}
*rb_wlcmtd_iden1.translations: transTable6
*rb_wlcmtd_iden1.armCallback: {\
RadioSet(UxWidget);\
}

*rb_wlcmtd_gues4.class: toggleButton
*rb_wlcmtd_gues4.parent: rowColumn24
*rb_wlcmtd_gues4.static: true
*rb_wlcmtd_gues4.name: rb_wlcmtd_gues4
*rb_wlcmtd_gues4.x: 3
*rb_wlcmtd_gues4.y: 34
*rb_wlcmtd_gues4.width: 93
*rb_wlcmtd_gues4.height: 32
*rb_wlcmtd_gues4.background: WindowBackground
*rb_wlcmtd_gues4.fontList: TextFont
*rb_wlcmtd_gues4.labelString: "ANGLE"
*rb_wlcmtd_gues4.selectColor: SelectColor
*rb_wlcmtd_gues4.foreground: TextForeground
*rb_wlcmtd_gues4.valueChangedCallback: {\
RadioSet ( UxWidget ) ;\
}
*rb_wlcmtd_gues4.set: "false"
*rb_wlcmtd_gues4.highlightOnEnter: "true"
*rb_wlcmtd_gues4.indicatorSize: 16
*rb_wlcmtd_gues4.translations: transTable6
*rb_wlcmtd_gues4.armCallback: {\
RadioSet(UxWidget);\
}

*rb_wlcmtd_gues5.class: toggleButton
*rb_wlcmtd_gues5.parent: rowColumn24
*rb_wlcmtd_gues5.static: true
*rb_wlcmtd_gues5.name: rb_wlcmtd_gues5
*rb_wlcmtd_gues5.x: 146
*rb_wlcmtd_gues5.y: 2
*rb_wlcmtd_gues5.width: 90
*rb_wlcmtd_gues5.height: 23
*rb_wlcmtd_gues5.background: WindowBackground
*rb_wlcmtd_gues5.fontList: TextFont
*rb_wlcmtd_gues5.labelString: "RESTART"
*rb_wlcmtd_gues5.selectColor: SelectColor
*rb_wlcmtd_gues5.foreground: TextForeground
*rb_wlcmtd_gues5.valueChangedCallback: {\
\
\
}
*rb_wlcmtd_gues5.set: "false"
*rb_wlcmtd_gues5.highlightOnEnter: "true"
*rb_wlcmtd_gues5.indicatorSize: 16
*rb_wlcmtd_gues5.translations: transTable6
*rb_wlcmtd_gues5.armCallback: {\
RadioSet(UxWidget);\
}

*rb_wlcmtd_gues6.class: toggleButton
*rb_wlcmtd_gues6.parent: rowColumn24
*rb_wlcmtd_gues6.static: true
*rb_wlcmtd_gues6.name: rb_wlcmtd_gues6
*rb_wlcmtd_gues6.x: 3
*rb_wlcmtd_gues6.y: 34
*rb_wlcmtd_gues6.width: 76
*rb_wlcmtd_gues6.height: 66
*rb_wlcmtd_gues6.background: WindowBackground
*rb_wlcmtd_gues6.fontList: TextFont
*rb_wlcmtd_gues6.labelString: "ORDER"
*rb_wlcmtd_gues6.selectColor: SelectColor
*rb_wlcmtd_gues6.foreground: TextForeground
*rb_wlcmtd_gues6.valueChangedCallback: {\
\
\
}
*rb_wlcmtd_gues6.set: "false"
*rb_wlcmtd_gues6.highlightOnEnter: "true"
*rb_wlcmtd_gues6.indicatorSize: 16
*rb_wlcmtd_gues6.translations: transTable6
*rb_wlcmtd_gues6.armCallback: {\
RadioSet(UxWidget);\
}

*rb_wlcmtd_gues7.class: toggleButton
*rb_wlcmtd_gues7.parent: rowColumn24
*rb_wlcmtd_gues7.static: true
*rb_wlcmtd_gues7.name: rb_wlcmtd_gues7
*rb_wlcmtd_gues7.x: 114
*rb_wlcmtd_gues7.y: 6
*rb_wlcmtd_gues7.width: 76
*rb_wlcmtd_gues7.height: 17
*rb_wlcmtd_gues7.background: WindowBackground
*rb_wlcmtd_gues7.fontList: TextFont
*rb_wlcmtd_gues7.labelString: "GUESS"
*rb_wlcmtd_gues7.selectColor: SelectColor
*rb_wlcmtd_gues7.foreground: TextForeground
*rb_wlcmtd_gues7.valueChangedCallback: {\
\
\
}
*rb_wlcmtd_gues7.set: "false"
*rb_wlcmtd_gues7.highlightOnEnter: "true"
*rb_wlcmtd_gues7.indicatorSize: 16
*rb_wlcmtd_gues7.translations: transTable6
*rb_wlcmtd_gues7.armCallback: {\
RadioSet(UxWidget);\
}

*rowColumn25.class: rowColumn
*rowColumn25.parent: form3
*rowColumn25.static: true
*rowColumn25.name: rowColumn25
*rowColumn25.x: 368
*rowColumn25.y: 183
*rowColumn25.width: 83
*rowColumn25.height: 74
*rowColumn25.radioBehavior: "true"
*rowColumn25.background: "grey80"
*rowColumn25.entryBorder: 0
*rowColumn25.labelString: ""
*rowColumn25.shadowThickness: 0
*rowColumn25.borderWidth: 0
*rowColumn25.entryAlignment: "alignment_beginning"
*rowColumn25.isAligned: "true"
*rowColumn25.orientation: "vertical"
*rowColumn25.packing: "pack_tight"
*rowColumn25.translations: transTable6

*rb_wlcmtd_iden2.class: toggleButton
*rb_wlcmtd_iden2.parent: rowColumn25
*rb_wlcmtd_iden2.static: true
*rb_wlcmtd_iden2.name: rb_wlcmtd_iden2
*rb_wlcmtd_iden2.x: 3
*rb_wlcmtd_iden2.y: 3
*rb_wlcmtd_iden2.width: 77
*rb_wlcmtd_iden2.height: 32
*rb_wlcmtd_iden2.background: WindowBackground
*rb_wlcmtd_iden2.fontList: TextFont
*rb_wlcmtd_iden2.labelString: "1D"
*rb_wlcmtd_iden2.set: "true"
*rb_wlcmtd_iden2.selectColor: SelectColor
*rb_wlcmtd_iden2.foreground: TextForeground
*rb_wlcmtd_iden2.highlightOnEnter: "true"
*rb_wlcmtd_iden2.indicatorSize: 16
*rb_wlcmtd_iden2.valueChangedCallback: {\
\
}
*rb_wlcmtd_iden2.translations: transTable6
*rb_wlcmtd_iden2.armCallback: RadioSet ( UxWidget ) ;

*rb_wlcmtd_gues1.class: toggleButton
*rb_wlcmtd_gues1.parent: rowColumn25
*rb_wlcmtd_gues1.static: true
*rb_wlcmtd_gues1.name: rb_wlcmtd_gues1
*rb_wlcmtd_gues1.x: 3
*rb_wlcmtd_gues1.y: 39
*rb_wlcmtd_gues1.width: 77
*rb_wlcmtd_gues1.height: 32
*rb_wlcmtd_gues1.background: WindowBackground
*rb_wlcmtd_gues1.fontList: TextFont
*rb_wlcmtd_gues1.labelString: "2D"
*rb_wlcmtd_gues1.selectColor: SelectColor
*rb_wlcmtd_gues1.foreground: TextForeground
*rb_wlcmtd_gues1.valueChangedCallback: {\
\
}
*rb_wlcmtd_gues1.set: "false"
*rb_wlcmtd_gues1.highlightOnEnter: "true"
*rb_wlcmtd_gues1.indicatorSize: 16
*rb_wlcmtd_gues1.translations: transTable6
*rb_wlcmtd_gues1.armCallback: RadioSet ( UxWidget ) ;

*separator13.class: separator
*separator13.parent: form3
*separator13.static: true
*separator13.name: separator13
*separator13.x: 8
*separator13.y: 392
*separator13.width: 512
*separator13.height: 10
*separator13.background: WindowBackground

*separator14.class: separator
*separator14.parent: form3
*separator14.static: true
*separator14.name: separator14
*separator14.x: 350
*separator14.y: 290
*separator14.width: 12
*separator14.height: 88
*separator14.background: WindowBackground
*separator14.orientation: "vertical"

*separator15.class: separator
*separator15.parent: form3
*separator15.static: true
*separator15.name: separator15
*separator15.x: 352
*separator15.y: 372
*separator15.width: 110
*separator15.height: 10
*separator15.background: WindowBackground

*separator16.class: separator
*separator16.parent: form3
*separator16.static: true
*separator16.name: separator16
*separator16.x: 458
*separator16.y: 288
*separator16.width: 12
*separator16.height: 88
*separator16.background: WindowBackground
*separator16.orientation: "vertical"

*separator17.class: separator
*separator17.parent: form3
*separator17.static: true
*separator17.name: separator17
*separator17.x: 358
*separator17.y: 171
*separator17.width: 12
*separator17.height: 87
*separator17.background: WindowBackground
*separator17.orientation: "vertical"

*separator18.class: separator
*separator18.parent: form3
*separator18.static: true
*separator18.name: separator18
*separator18.x: 456
*separator18.y: 169
*separator18.width: 14
*separator18.height: 93
*separator18.background: WindowBackground
*separator18.orientation: "vertical"

*separator19.class: separator
*separator19.parent: form3
*separator19.static: true
*separator19.name: separator19
*separator19.x: 456
*separator19.y: 169
*separator19.width: 12
*separator19.height: 91
*separator19.background: WindowBackground
*separator19.orientation: "vertical"

*label28.class: label
*label28.parent: form3
*label28.static: true
*label28.name: label28
*label28.x: 366
*label28.y: 276
*label28.width: 88
*label28.height: 30
*label28.background: LabelBackground
*label28.fontList: TextFont
*label28.labelString: "Visualize"
*label28.alignment: "alignment_beginning"
*label28.foreground: TextForeground

*label31.class: label
*label31.parent: form3
*label31.static: true
*label31.name: label31
*label31.x: 362
*label31.y: 154
*label31.width: 100
*label31.height: 30
*label31.background: LabelBackground
*label31.fontList: TextFont
*label31.labelString: "Disp. Relation"
*label31.alignment: "alignment_beginning"
*label31.foreground: TextForeground

*mn_tol1.class: rowColumn
*mn_tol1.parent: form3
*mn_tol1.static: true
*mn_tol1.name: mn_tol1
*mn_tol1.rowColumnType: "menu_option"
*mn_tol1.subMenuId: "menu2_p1"
*mn_tol1.x: 350
*mn_tol1.y: 124
*mn_tol1.background: WindowBackground
*mn_tol1.foreground: TextForeground
*mn_tol1.marginWidth: 0
*mn_tol1.spacing: 0
*mn_tol1.labelString: " "
*mn_tol1.translations: transTable6

*menu2_p1.class: rowColumn
*menu2_p1.parent: mn_tol1
*menu2_p1.static: true
*menu2_p1.name: menu2_p1
*menu2_p1.rowColumnType: "menu_pulldown"
*menu2_p1.background: WindowBackground
*menu2_p1.foreground: TextForeground

*mn_tol_angstroms1.class: pushButtonGadget
*mn_tol_angstroms1.parent: menu2_p1
*mn_tol_angstroms1.static: true
*mn_tol_angstroms1.name: mn_tol_angstroms1
*mn_tol_angstroms1.labelString: " Angstroms "
*mn_tol_angstroms1.activateCallback: {\
#include <spec_comm.h>\
\
char text[32];\
extern float Tol;\
extern int TolPixels;\
\
TolPixels = FALSE;\
\
sprintf(text, "%f", -Tol);\
WriteKeyword(text, K_TOL);\
\
XtFree(text);\
}
*mn_tol_angstroms1.fontList: TextFont

*mn_tol_pixels1.class: pushButtonGadget
*mn_tol_pixels1.parent: menu2_p1
*mn_tol_pixels1.static: true
*mn_tol_pixels1.name: mn_tol_pixels1
*mn_tol_pixels1.labelString: " Pixels "
*mn_tol_pixels1.activateCallback: {\
#include <spec_comm.h>\
\
char text[32];\
extern float Tol;\
extern int TolPixels;\
\
TolPixels = TRUE;\
\
sprintf(text, "%f", Tol);\
WriteKeyword(text, K_TOL);\
\
XtFree(text);\
}
*mn_tol_pixels1.fontList: TextFont

*label18.class: label
*label18.parent: form3
*label18.static: true
*label18.name: label18
*label18.x: 13
*label18.y: 198
*label18.width: 200
*label18.height: 30
*label18.background: LabelBackground
*label18.fontList: TextFont
*label18.labelString: "Min. and Max. number of iter. :"
*label18.alignment: "alignment_beginning"
*label18.foreground: TextForeground

*pb_main_search33.class: pushButton
*pb_main_search33.parent: form3
*pb_main_search33.static: true
*pb_main_search33.name: pb_main_search33
*pb_main_search33.x: 464
*pb_main_search33.y: 8
*pb_main_search33.height: 28
*pb_main_search33.background: ButtonBackground
*pb_main_search33.fontList: BoldTextFont
*pb_main_search33.foreground: ButtonForeground
*pb_main_search33.labelString: "..."
*pb_main_search33.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search33.recomputeSize: "true"
*pb_main_search33.width: 46

