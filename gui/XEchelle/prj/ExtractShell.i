! UIMX ascii 2.0 key: 660                                                       
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}

*translation.table: SelectFileExtract
*translation.parent: ExtractShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*ExtractShell.class: applicationShell
*ExtractShell.parent: NO_PARENT
*ExtractShell.static: true
*ExtractShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*ExtractShell.ispecdecl:
*ExtractShell.funcdecl: swidget create_ExtractShell()\

*ExtractShell.funcname: create_ExtractShell
*ExtractShell.funcdef: "swidget", "<create_ExtractShell>(%)"
*ExtractShell.icode:
*ExtractShell.fcode: return(rtrn);\

*ExtractShell.auxdecl:
*ExtractShell.name: ExtractShell
*ExtractShell.x: 2
*ExtractShell.y: 21
*ExtractShell.width: 490
*ExtractShell.height: 661
*ExtractShell.title: "Spectrum Extraction"
*ExtractShell.keyboardFocusPolicy: "pointer"
*ExtractShell.geometry: "+10+60"
*ExtractShell.background: WindowBackground

*form14.class: form
*form14.parent: ExtractShell
*form14.static: true
*form14.name: form14
*form14.resizePolicy: "resize_none"
*form14.unitType: "pixels"
*form14.x: 0
*form14.y: 0
*form14.width: 490
*form14.height: 662
*form14.background: WindowBackground

*form15.class: form
*form15.parent: form14
*form15.static: true
*form15.name: form15
*form15.resizePolicy: "resize_none"
*form15.x: 0
*form15.y: 576
*form15.width: 490
*form15.height: 84
*form15.background: ButtonBackground

*pb_ext_sky.class: pushButton
*pb_ext_sky.parent: form15
*pb_ext_sky.static: true
*pb_ext_sky.name: pb_ext_sky
*pb_ext_sky.x: 8
*pb_ext_sky.y: 4
*pb_ext_sky.width: 86
*pb_ext_sky.height: 30
*pb_ext_sky.background: ButtonBackground
*pb_ext_sky.fontList: BoldTextFont
*pb_ext_sky.foreground: ApplyForeground
*pb_ext_sky.labelString: "Get sky"
*pb_ext_sky.activateCallback: {\
#include <spec_comm.h>\
\
char command[128], s[10];\
int sky[4];\
extern int Lowsky[], Uppsky[];\
\
sprintf(command, "%s 4", C_EXTR_GCOOR);\
AppendDialogText(command);\
\
if ( !GetCoords(sky, 4) )\
    return;\
\
sprintf(command, "set/long LOWSKY=%d,%d UPPSKY=%d,%d", sky[0], sky[1], sky[2], sky[3]);\
AppendDialogText(command);\
sprintf(s, "%d", sky[0]);\
UxPutValue(UxFindSwidget("tf_lowsky1"), s);\
sprintf(s, "%d", sky[1]);\
UxPutValue(UxFindSwidget("tf_lowsky2"), s);\
sprintf(s, "%d", sky[2]);\
UxPutValue(UxFindSwidget("tf_uppsky1"), s);\
sprintf(s, "%d", sky[3]);\
UxPutValue(UxFindSwidget("tf_uppsky2"), s);\
\
Lowsky[0] = sky[0]; Lowsky[1] = sky[1];\
Uppsky[0] = sky[2]; Uppsky[1] = sky[3];\
\
\
\
\
\
\
}

*pushButton28.class: pushButton
*pushButton28.parent: form15
*pushButton28.static: true
*pushButton28.name: pushButton28
*pushButton28.x: 8
*pushButton28.y: 40
*pushButton28.width: 86
*pushButton28.height: 30
*pushButton28.background: ButtonBackground
*pushButton28.fontList: BoldTextFont
*pushButton28.foreground: CancelForeground
*pushButton28.labelString: "Cancel"
*pushButton28.activateCallback: {\
UxPopdownInterface(UxFindSwidget("ExtractShell"));\
}

*pb_ext_object.class: pushButton
*pb_ext_object.parent: form15
*pb_ext_object.static: true
*pb_ext_object.name: pb_ext_object
*pb_ext_object.x: 102
*pb_ext_object.y: 4
*pb_ext_object.width: 86
*pb_ext_object.height: 30
*pb_ext_object.background: ButtonBackground
*pb_ext_object.fontList: BoldTextFont
*pb_ext_object.foreground: ApplyForeground
*pb_ext_object.labelString: "Get object"
*pb_ext_object.activateCallback: {\
#include <spec_comm.h>\
\
char command[128], s[10];\
extern int Objlim[];\
\
sprintf(command, "%s 2", C_EXTR_GCOOR);\
AppendDialogText(command);\
\
if ( !GetCoords(Objlim, 2) )\
    return;\
\
sprintf(command, "set/long OBJECT=%d,%d", Objlim[0], Objlim[1]);\
AppendDialogText(command);\
sprintf(s, "%d", Objlim[0]);\
UxPutValue(UxFindSwidget("tf_object1"), s);\
sprintf(s, "%d", Objlim[1]);\
UxPutValue(UxFindSwidget("tf_object2"), s);\
\
}

*pb_ext_fit.class: pushButton
*pb_ext_fit.parent: form15
*pb_ext_fit.static: true
*pb_ext_fit.name: pb_ext_fit
*pb_ext_fit.x: 196
*pb_ext_fit.y: 4
*pb_ext_fit.width: 86
*pb_ext_fit.height: 30
*pb_ext_fit.background: ButtonBackground
*pb_ext_fit.fontList: BoldTextFont
*pb_ext_fit.foreground: ApplyForeground
*pb_ext_fit.labelString: "Fit sky"
*pb_ext_fit.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_FIT_SKY);\
}

*pb_ext_weight.class: pushButton
*pb_ext_weight.parent: form15
*pb_ext_weight.static: true
*pb_ext_weight.name: pb_ext_weight
*pb_ext_weight.x: 386
*pb_ext_weight.y: 4
*pb_ext_weight.width: 86
*pb_ext_weight.height: 30
*pb_ext_weight.background: ButtonBackground
*pb_ext_weight.fontList: BoldTextFont
*pb_ext_weight.foreground: ApplyForeground
*pb_ext_weight.labelString: "Ext weight"
*pb_ext_weight.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_EXT_WEIGHT);\
}

*pb_ext_average.class: pushButton
*pb_ext_average.parent: form15
*pb_ext_average.static: true
*pb_ext_average.name: pb_ext_average
*pb_ext_average.x: 290
*pb_ext_average.y: 4
*pb_ext_average.width: 86
*pb_ext_average.height: 30
*pb_ext_average.background: ButtonBackground
*pb_ext_average.fontList: BoldTextFont
*pb_ext_average.foreground: ApplyForeground
*pb_ext_average.labelString: "Ext average"
*pb_ext_average.activateCallback: {\
#include <spec_comm.h>\
\
PopupList(LIST_EXT_AVER);\
}

*shelp_extract.class: text
*shelp_extract.parent: form14
*shelp_extract.static: true
*shelp_extract.name: shelp_extract
*shelp_extract.x: 2
*shelp_extract.y: 516
*shelp_extract.width: 484
*shelp_extract.height: 50
*shelp_extract.background: SHelpBackground
*shelp_extract.cursorPositionVisible: "false"
*shelp_extract.editable: "false"
*shelp_extract.fontList: TextFont

*separator10.class: separator
*separator10.parent: form14
*separator10.static: true
*separator10.name: separator10
*separator10.x: 0
*separator10.y: 506
*separator10.width: 492
*separator10.height: 10
*separator10.background: WindowBackground

*separator11.class: separator
*separator11.parent: form14
*separator11.static: true
*separator11.name: separator11
*separator11.x: -2
*separator11.y: 564
*separator11.width: 492
*separator11.height: 10
*separator11.background: WindowBackground

*rowColumn6.class: rowColumn
*rowColumn6.parent: form14
*rowColumn6.static: true
*rowColumn6.name: rowColumn6
*rowColumn6.x: 264
*rowColumn6.y: 422
*rowColumn6.width: 82
*rowColumn6.height: 67
*rowColumn6.radioBehavior: "true"
*rowColumn6.background: "grey80"
*rowColumn6.entryBorder: 0
*rowColumn6.labelString: ""
*rowColumn6.shadowThickness: 0
*rowColumn6.borderWidth: 0
*rowColumn6.entryAlignment: "alignment_beginning"
*rowColumn6.isAligned: "true"

*rb_extmtd_line.class: toggleButton
*rb_extmtd_line.parent: rowColumn6
*rb_extmtd_line.static: true
*rb_extmtd_line.name: rb_extmtd_line
*rb_extmtd_line.x: 3
*rb_extmtd_line.y: 2
*rb_extmtd_line.width: 76
*rb_extmtd_line.height: 20
*rb_extmtd_line.background: WindowBackground
*rb_extmtd_line.fontList: TextFont
*rb_extmtd_line.labelString: "SUM"
*rb_extmtd_line.set: "true"
*rb_extmtd_line.selectColor: SelectColor
*rb_extmtd_line.foreground: TextForeground
*rb_extmtd_line.highlightOnEnter: "true"
*rb_extmtd_line.indicatorSize: 16

*rb_extmtd_aver.class: toggleButton
*rb_extmtd_aver.parent: rowColumn6
*rb_extmtd_aver.static: true
*rb_extmtd_aver.name: rb_extmtd_aver
*rb_extmtd_aver.x: 3
*rb_extmtd_aver.y: 34
*rb_extmtd_aver.width: 76
*rb_extmtd_aver.height: 66
*rb_extmtd_aver.background: WindowBackground
*rb_extmtd_aver.fontList: TextFont
*rb_extmtd_aver.labelString: "AVERAGE"
*rb_extmtd_aver.selectColor: SelectColor
*rb_extmtd_aver.foreground: TextForeground
*rb_extmtd_aver.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( !UpdateToggle )\
    return;\
\
if ( XmToggleButtonGetState(UxWidget) )  /*selected */\
    WriteKeyword("AVERAGE", K_EXTMTD);\
else\
    WriteKeyword("LINEAR", K_EXTMTD);\
\
}
*rb_extmtd_aver.set: "false"
*rb_extmtd_aver.highlightOnEnter: "true"
*rb_extmtd_aver.indicatorSize: 16

*label47.class: label
*label47.parent: form14
*label47.static: true
*label47.name: label47
*label47.x: 256
*label47.y: 396
*label47.width: 122
*label47.height: 26
*label47.background: WindowBackground
*label47.fontList: TextFont
*label47.labelString: "Extraction method"
*label47.alignment: "alignment_beginning"
*label47.foreground: TextForeground

*separatorGadget26.class: separatorGadget
*separatorGadget26.parent: form14
*separatorGadget26.static: true
*separatorGadget26.name: separatorGadget26
*separatorGadget26.x: 241
*separatorGadget26.y: 412
*separatorGadget26.width: 12
*separatorGadget26.height: 84
*separatorGadget26.orientation: "vertical"

*separatorGadget27.class: separatorGadget
*separatorGadget27.parent: form14
*separatorGadget27.static: true
*separatorGadget27.name: separatorGadget27
*separatorGadget27.x: 246
*separatorGadget27.y: 492
*separatorGadget27.width: 144
*separatorGadget27.height: 8

*separatorGadget28.class: separatorGadget
*separatorGadget28.parent: form14
*separatorGadget28.static: true
*separatorGadget28.name: separatorGadget28
*separatorGadget28.x: 384
*separatorGadget28.y: 412
*separatorGadget28.width: 12
*separatorGadget28.height: 84
*separatorGadget28.orientation: "vertical"

*rowColumn7.class: rowColumn
*rowColumn7.parent: form14
*rowColumn7.static: true
*rowColumn7.name: rowColumn7
*rowColumn7.x: 45
*rowColumn7.y: 420
*rowColumn7.width: 82
*rowColumn7.height: 67
*rowColumn7.radioBehavior: "true"
*rowColumn7.background: "grey80"
*rowColumn7.entryBorder: 0
*rowColumn7.labelString: ""
*rowColumn7.shadowThickness: 0
*rowColumn7.borderWidth: 0
*rowColumn7.entryAlignment: "alignment_beginning"
*rowColumn7.isAligned: "true"

*rb_skymod_0.class: toggleButton
*rb_skymod_0.parent: rowColumn7
*rb_skymod_0.static: true
*rb_skymod_0.name: rb_skymod_0
*rb_skymod_0.x: -12
*rb_skymod_0.y: 0
*rb_skymod_0.width: 76
*rb_skymod_0.height: 20
*rb_skymod_0.background: WindowBackground
*rb_skymod_0.fontList: TextFont
*rb_skymod_0.labelString: "Same spatial profile"
*rb_skymod_0.set: "true"
*rb_skymod_0.selectColor: SelectColor
*rb_skymod_0.foreground: TextForeground
*rb_skymod_0.highlightOnEnter: "true"
*rb_skymod_0.indicatorSize: 16

*rb_skymod_1.class: toggleButton
*rb_skymod_1.parent: rowColumn7
*rb_skymod_1.static: true
*rb_skymod_1.name: rb_skymod_1
*rb_skymod_1.x: 3
*rb_skymod_1.y: 34
*rb_skymod_1.width: 76
*rb_skymod_1.height: 66
*rb_skymod_1.background: WindowBackground
*rb_skymod_1.fontList: TextFont
*rb_skymod_1.labelString: "Independent profile"
*rb_skymod_1.selectColor: SelectColor
*rb_skymod_1.foreground: TextForeground
*rb_skymod_1.valueChangedCallback: {\
#include <spec_comm.h>\
\
extern int UpdateToggle;\
\
if ( !UpdateToggle )\
    return;\
\
if ( XmToggleButtonGetState(UxWidget) )  /*selected */\
    WriteKeyword("1", K_SKYMOD);\
else\
    WriteKeyword("0", K_SKYMOD);\
\
}
*rb_skymod_1.set: "false"
*rb_skymod_1.highlightOnEnter: "true"
*rb_skymod_1.indicatorSize: 16

*label48.class: label
*label48.parent: form14
*label48.static: true
*label48.name: label48
*label48.x: 68
*label48.y: 394
*label48.width: 105
*label48.height: 26
*label48.background: WindowBackground
*label48.fontList: TextFont
*label48.labelString: "Sky fitting mode"
*label48.alignment: "alignment_beginning"
*label48.foreground: TextForeground

*separatorGadget29.class: separatorGadget
*separatorGadget29.parent: form14
*separatorGadget29.static: true
*separatorGadget29.name: separatorGadget29
*separatorGadget29.x: 31
*separatorGadget29.y: 410
*separatorGadget29.width: 12
*separatorGadget29.height: 84
*separatorGadget29.orientation: "vertical"

*separatorGadget30.class: separatorGadget
*separatorGadget30.parent: form14
*separatorGadget30.static: true
*separatorGadget30.name: separatorGadget30
*separatorGadget30.x: 202
*separatorGadget30.y: 410
*separatorGadget30.width: 12
*separatorGadget30.height: 84
*separatorGadget30.orientation: "vertical"

*separatorGadget31.class: separatorGadget
*separatorGadget31.parent: form14
*separatorGadget31.static: true
*separatorGadget31.name: separatorGadget31
*separatorGadget31.x: 35
*separatorGadget31.y: 406
*separatorGadget31.width: 33
*separatorGadget31.height: 10

*separatorGadget32.class: separatorGadget
*separatorGadget32.parent: form14
*separatorGadget32.static: true
*separatorGadget32.name: separatorGadget32
*separatorGadget32.x: 172
*separatorGadget32.y: 406
*separatorGadget32.width: 36
*separatorGadget32.height: 10

*separatorGadget33.class: separatorGadget
*separatorGadget33.parent: form14
*separatorGadget33.static: true
*separatorGadget33.name: separatorGadget33
*separatorGadget33.x: 36
*separatorGadget33.y: 490
*separatorGadget33.width: 172
*separatorGadget33.height: 8

*separatorGadget34.class: separatorGadget
*separatorGadget34.parent: form14
*separatorGadget34.static: true
*separatorGadget34.name: separatorGadget34
*separatorGadget34.x: 374
*separatorGadget34.y: 408
*separatorGadget34.width: 16
*separatorGadget34.height: 10

*separatorGadget35.class: separatorGadget
*separatorGadget35.parent: form14
*separatorGadget35.static: true
*separatorGadget35.name: separatorGadget35
*separatorGadget35.x: 246
*separatorGadget35.y: 408
*separatorGadget35.width: 13
*separatorGadget35.height: 10

*label49.class: label
*label49.parent: form14
*label49.static: true
*label49.name: label49
*label49.x: 16
*label49.y: 62
*label49.width: 150
*label49.height: 30
*label49.background: LabelBackground
*label49.fontList: TextFont
*label49.labelString: "Object limits (pixels) :"
*label49.alignment: "alignment_beginning"
*label49.foreground: TextForeground

*label50.class: label
*label50.parent: form14
*label50.static: true
*label50.name: label50
*label50.x: 16
*label50.y: 26
*label50.width: 120
*label50.height: 30
*label50.background: LabelBackground
*label50.fontList: TextFont
*label50.labelString: "Sky limits (pixels) :"
*label50.alignment: "alignment_beginning"
*label50.foreground: TextForeground

*tf_lowsky2.class: textField
*tf_lowsky2.parent: form14
*tf_lowsky2.static: true
*tf_lowsky2.name: tf_lowsky2
*tf_lowsky2.x: 292
*tf_lowsky2.y: 24
*tf_lowsky2.width: 56
*tf_lowsky2.height: 34
*tf_lowsky2.background: TextBackground
*tf_lowsky2.fontList: TextFont
*tf_lowsky2.highlightOnEnter: "true"
*tf_lowsky2.foreground: TextForeground
*tf_lowsky2.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Lowsky[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Lowsky[1] ) {\
    Lowsky[1] = val;\
    WriteKeyword(text, K_LOWSKY2);\
}\
\
XtFree(text);\
}
*tf_lowsky2.maxLength: 5

*tf_sky.class: textField
*tf_sky.parent: form14
*tf_sky.static: true
*tf_sky.name: tf_sky
*tf_sky.x: 238
*tf_sky.y: 356
*tf_sky.width: 210
*tf_sky.height: 34
*tf_sky.background: TextBackground
*tf_sky.fontList: TextFont
*tf_sky.highlightOnEnter: "true"
*tf_sky.foreground: TextForeground

*label38.class: label
*label38.parent: form14
*label38.static: true
*label38.name: label38
*label38.x: 16
*label38.y: 354
*label38.width: 186
*label38.height: 30
*label38.background: LabelBackground
*label38.fontList: TextFont
*label38.labelString: "Sky (image or constant) :"
*label38.alignment: "alignment_beginning"
*label38.foreground: TextForeground

*label42.class: label
*label42.parent: form14
*label42.static: true
*label42.name: label42
*label42.x: 16
*label42.y: 136
*label42.width: 194
*label42.height: 30
*label42.background: LabelBackground
*label42.fontList: TextFont
*label42.labelString: "Order for optimal extraction :"
*label42.alignment: "alignment_beginning"
*label42.foreground: TextForeground

*tf_order.class: textField
*tf_order.parent: form14
*tf_order.static: true
*tf_order.name: tf_order
*tf_order.x: 238
*tf_order.y: 136
*tf_order.width: 80
*tf_order.height: 34
*tf_order.background: TextBackground
*tf_order.fontList: TextFont
*tf_order.highlightOnEnter: "true"
*tf_order.foreground: TextForeground
*tf_order.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Order;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Order ) {\
    Order = val;\
    WriteKeyword(text, K_ORDER);\
}\
\
XtFree(text);\
}

*tf_skyord.class: textField
*tf_skyord.parent: form14
*tf_skyord.static: true
*tf_skyord.name: tf_skyord
*tf_skyord.x: 238
*tf_skyord.y: 98
*tf_skyord.width: 80
*tf_skyord.height: 34
*tf_skyord.background: TextBackground
*tf_skyord.fontList: TextFont
*tf_skyord.highlightOnEnter: "true"
*tf_skyord.foreground: TextForeground
*tf_skyord.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Skyord;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Skyord ) {\
    Skyord = val;\
    WriteKeyword(text, K_SKYORD);\
}\
\
XtFree(text);\
}

*label43.class: label
*label43.parent: form14
*label43.static: true
*label43.name: label43
*label43.x: 16
*label43.y: 100
*label43.width: 120
*label43.height: 30
*label43.background: LabelBackground
*label43.fontList: TextFont
*label43.labelString: "Order for sky fit :"
*label43.alignment: "alignment_beginning"
*label43.foreground: TextForeground

*label45.class: label
*label45.parent: form14
*label45.static: true
*label45.name: label45
*label45.x: 16
*label45.y: 210
*label45.width: 154
*label45.height: 30
*label45.background: LabelBackground
*label45.fontList: TextFont
*label45.labelString: "Read-out-noise (e-) :"
*label45.alignment: "alignment_beginning"
*label45.foreground: TextForeground

*tf_ron.class: textField
*tf_ron.parent: form14
*tf_ron.static: true
*tf_ron.name: tf_ron
*tf_ron.x: 238
*tf_ron.y: 210
*tf_ron.width: 80
*tf_ron.height: 34
*tf_ron.background: TextBackground
*tf_ron.fontList: TextFont
*tf_ron.highlightOnEnter: "true"
*tf_ron.foreground: TextForeground
*tf_ron.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
float val;\
extern float Ron;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%f", &val);\
\
if ( val != Ron ) {\
    Ron = val;\
    WriteKeyword(text, K_RON);\
}\
\
XtFree(text);\
\
}

*tf_niter.class: textField
*tf_niter.parent: form14
*tf_niter.static: true
*tf_niter.name: tf_niter
*tf_niter.x: 238
*tf_niter.y: 172
*tf_niter.width: 80
*tf_niter.height: 34
*tf_niter.background: TextBackground
*tf_niter.fontList: TextFont
*tf_niter.highlightOnEnter: "true"
*tf_niter.foreground: TextForeground
*tf_niter.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Niter;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Niter ) {\
    Niter = val;\
    WriteKeyword(text, K_NITER);\
}\
\
XtFree(text);\
}

*label46.class: label
*label46.parent: form14
*label46.static: true
*label46.name: label46
*label46.x: 16
*label46.y: 174
*label46.width: 146
*label46.height: 30
*label46.background: LabelBackground
*label46.fontList: TextFont
*label46.labelString: "Extraction iterations :"
*label46.alignment: "alignment_beginning"
*label46.foreground: TextForeground

*label51.class: label
*label51.parent: form14
*label51.static: true
*label51.name: label51
*label51.x: 16
*label51.y: 246
*label51.width: 206
*label51.height: 30
*label51.background: LabelBackground
*label51.fontList: TextFont
*label51.labelString: "Inverse gain factor (e-/ADU) :"
*label51.alignment: "alignment_beginning"
*label51.foreground: TextForeground

*tf_gain.class: textField
*tf_gain.parent: form14
*tf_gain.static: true
*tf_gain.name: tf_gain
*tf_gain.x: 238
*tf_gain.y: 246
*tf_gain.width: 80
*tf_gain.height: 34
*tf_gain.background: TextBackground
*tf_gain.fontList: TextFont
*tf_gain.highlightOnEnter: "true"
*tf_gain.foreground: TextForeground
*tf_gain.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
float val;\
extern float Gain;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%f", &val);\
\
if ( val != Gain ) {\
    Gain = val;\
    WriteKeyword(text, K_GAIN);\
}\
\
XtFree(text);\
\
}

*label52.class: label
*label52.parent: form14
*label52.static: true
*label52.name: label52
*label52.x: 16
*label52.y: 320
*label52.width: 220
*label52.height: 30
*label52.background: LabelBackground
*label52.fontList: TextFont
*label52.labelString: "Radius for cosmic rays removal :"
*label52.alignment: "alignment_beginning"
*label52.foreground: TextForeground

*tf_radius.class: textField
*tf_radius.parent: form14
*tf_radius.static: true
*tf_radius.name: tf_radius
*tf_radius.x: 238
*tf_radius.y: 320
*tf_radius.width: 80
*tf_radius.height: 34
*tf_radius.background: TextBackground
*tf_radius.fontList: TextFont
*tf_radius.highlightOnEnter: "true"
*tf_radius.foreground: TextForeground
*tf_radius.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Radius;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Radius ) {\
    Radius = val;\
    WriteKeyword(text, K_RADIUS);\
}\
\
XtFree(text);\
}

*tf_sigma.class: textField
*tf_sigma.parent: form14
*tf_sigma.static: true
*tf_sigma.name: tf_sigma
*tf_sigma.x: 238
*tf_sigma.y: 282
*tf_sigma.width: 80
*tf_sigma.height: 34
*tf_sigma.background: TextBackground
*tf_sigma.fontList: TextFont
*tf_sigma.highlightOnEnter: "true"
*tf_sigma.foreground: TextForeground
*tf_sigma.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
float val;\
extern float Sigma;\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%f", &val);\
\
if ( val != Sigma ) {\
    Sigma = val;\
    WriteKeyword(text, K_SIGMA);\
}\
\
XtFree(text);\
\
}

*label53.class: label
*label53.parent: form14
*label53.static: true
*label53.name: label53
*label53.x: 16
*label53.y: 284
*label53.width: 182
*label53.height: 30
*label53.background: LabelBackground
*label53.fontList: TextFont
*label53.labelString: "Threshold for cosmic rays :"
*label53.alignment: "alignment_beginning"
*label53.foreground: TextForeground

*tf_lowsky1.class: textField
*tf_lowsky1.parent: form14
*tf_lowsky1.static: true
*tf_lowsky1.name: tf_lowsky1
*tf_lowsky1.x: 238
*tf_lowsky1.y: 24
*tf_lowsky1.width: 56
*tf_lowsky1.height: 34
*tf_lowsky1.background: TextBackground
*tf_lowsky1.fontList: TextFont
*tf_lowsky1.highlightOnEnter: "true"
*tf_lowsky1.foreground: TextForeground
*tf_lowsky1.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Lowsky[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Lowsky[0] ) {\
    Lowsky[0] = val;\
    WriteKeyword(text, K_LOWSKY1);\
}\
\
XtFree(text);\
}
*tf_lowsky1.maxLength: 5

*tf_uppsky1.class: textField
*tf_uppsky1.parent: form14
*tf_uppsky1.static: true
*tf_uppsky1.name: tf_uppsky1
*tf_uppsky1.x: 346
*tf_uppsky1.y: 24
*tf_uppsky1.width: 56
*tf_uppsky1.height: 34
*tf_uppsky1.background: TextBackground
*tf_uppsky1.fontList: TextFont
*tf_uppsky1.highlightOnEnter: "true"
*tf_uppsky1.foreground: TextForeground
*tf_uppsky1.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Uppsky[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Uppsky[0] ) {\
    Uppsky[0] = val;\
    WriteKeyword(text, K_UPPSKY1);\
}\
\
XtFree(text);\
}
*tf_uppsky1.maxLength: 5

*tf_uppsky2.class: textField
*tf_uppsky2.parent: form14
*tf_uppsky2.static: true
*tf_uppsky2.name: tf_uppsky2
*tf_uppsky2.x: 400
*tf_uppsky2.y: 24
*tf_uppsky2.width: 56
*tf_uppsky2.height: 34
*tf_uppsky2.background: TextBackground
*tf_uppsky2.fontList: TextFont
*tf_uppsky2.highlightOnEnter: "true"
*tf_uppsky2.foreground: TextForeground
*tf_uppsky2.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Uppsky[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Uppsky[1] ) {\
    Uppsky[1] = val;\
    WriteKeyword(text, K_UPPSKY2);\
}\
\
XtFree(text);\
}
*tf_uppsky2.maxLength: 5

*tf_object1.class: textField
*tf_object1.parent: form14
*tf_object1.static: true
*tf_object1.name: tf_object1
*tf_object1.x: 238
*tf_object1.y: 60
*tf_object1.width: 56
*tf_object1.height: 34
*tf_object1.background: TextBackground
*tf_object1.fontList: TextFont
*tf_object1.highlightOnEnter: "true"
*tf_object1.foreground: TextForeground
*tf_object1.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Objlim[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Objlim[0] ) {\
    Objlim[0] = val;\
    WriteKeyword(text, K_OBJECT1);\
}\
\
XtFree(text);\
}
*tf_object1.maxLength: 5

*tf_object2.class: textField
*tf_object2.parent: form14
*tf_object2.static: true
*tf_object2.name: tf_object2
*tf_object2.x: 292
*tf_object2.y: 60
*tf_object2.width: 56
*tf_object2.height: 34
*tf_object2.background: TextBackground
*tf_object2.fontList: TextFont
*tf_object2.highlightOnEnter: "true"
*tf_object2.foreground: TextForeground
*tf_object2.losingFocusCallback: {\
#include <spec_comm.h>\
\
char *text;\
int val;\
extern int Objlim[];\
\
text = XmTextGetString(UxWidget);\
sscanf(text, "%d", &val);\
\
if ( val != Objlim[1] ) {\
    Objlim[1] = val;\
    WriteKeyword(text, K_OBJECT2);\
}\
\
XtFree(text);\
}
*tf_object2.maxLength: 5

