! UIMX ascii 2.0 key: 8610                                                      

*lincat_list.class: topLevelShell
*lincat_list.parent: NO_PARENT
*lincat_list.static: true
*lincat_list.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*lincat_list.ispecdecl:
*lincat_list.funcdecl: swidget create_lincat_list()\

*lincat_list.funcname: create_lincat_list
*lincat_list.funcdef: "swidget", "<create_lincat_list>(%)"
*lincat_list.icode:
*lincat_list.fcode: return(rtrn);\

*lincat_list.auxdecl:
*lincat_list.name: lincat_list
*lincat_list.x: 571
*lincat_list.y: 236
*lincat_list.width: 199
*lincat_list.height: 355
*lincat_list.iconName: "Wavelength selection"
*lincat_list.keyboardFocusPolicy: "pointer"
*lincat_list.background: "AntiqueWhite"
*lincat_list.geometry: "+100+100"

*form9.class: form
*form9.parent: lincat_list
*form9.static: true
*form9.name: form9
*form9.resizePolicy: "resize_none"
*form9.unitType: "pixels"
*form9.x: 0
*form9.y: 0
*form9.width: 214
*form9.height: 352
*form9.background: ApplicBackground

*scrolledWindow2.class: scrolledWindow
*scrolledWindow2.parent: form9
*scrolledWindow2.static: true
*scrolledWindow2.name: scrolledWindow2
*scrolledWindow2.scrollingPolicy: "application_defined"
*scrolledWindow2.x: 2
*scrolledWindow2.y: 2
*scrolledWindow2.visualPolicy: "variable"
*scrolledWindow2.scrollBarDisplayPolicy: "static"
*scrolledWindow2.shadowThickness: 0
*scrolledWindow2.scrollBarPlacement: "bottom_left"
*scrolledWindow2.background: ListBackground
*scrolledWindow2.height: 350
*scrolledWindow2.rightAttachment: "attach_form"
*scrolledWindow2.rightOffset: 2
*scrolledWindow2.leftAttachment: "attach_form"
*scrolledWindow2.leftOffset: 2
*scrolledWindow2.width: 160

*sl_lincat_list.class: scrolledList
*sl_lincat_list.parent: scrolledWindow2
*sl_lincat_list.static: true
*sl_lincat_list.name: sl_lincat_list
*sl_lincat_list.width: 238
*sl_lincat_list.height: 352
*sl_lincat_list.scrollBarDisplayPolicy: "static"
*sl_lincat_list.listSizePolicy: "variable"
*sl_lincat_list.background: TextBackground
*sl_lincat_list.fontList: SmallFont
*sl_lincat_list.visibleItemCount: 21
*sl_lincat_list.browseSelectionCallback: {\
#include <spec_comm.h>\
\
char command[256];\
char *choice;\
XmListCallbackStruct *cbs;\
extern char Plotmode[];\
\
cbs = (XmListCallbackStruct *)UxCallbackArg;\
\
XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);\
\
if ( XmToggleButtonGetState(UxGetWidget(UxFindSwidget("tg_twodopt"))) )\
    sprintf(command, "%s%s ? 2D", C_PLOT_DISTOR, choice); /* 2-D */\
else\
    sprintf(command, "%s%s", C_PLOT_DISTOR, choice); /* RBR */\
\
AppendDialogText(command);\
strcpy(Plotmode, C_PLOT_DISTOR);\
\
XtFree(choice);\
UxPopdownInterface(UxFindSwidget("lincat_list"));\
\
}
*sl_lincat_list.foreground: ListForeground

