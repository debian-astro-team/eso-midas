! UIMX ascii 2.0 key: 6707                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  

*translation.table: transTable17
*translation.parent: ExtractionShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable18
*translation.parent: ExtractionShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*ExtractionShell.class: transientShell
*ExtractionShell.parent: NO_PARENT
*ExtractionShell.static: true
*ExtractionShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*ExtractionShell.ispecdecl:
*ExtractionShell.funcdecl: swidget create_ExtractionShell()\

*ExtractionShell.funcname: create_ExtractionShell
*ExtractionShell.funcdef: "swidget", "<create_ExtractionShell>(%)"
*ExtractionShell.icode:
*ExtractionShell.fcode: return(rtrn);\

*ExtractionShell.auxdecl:
*ExtractionShell.name: ExtractionShell
*ExtractionShell.x: 520
*ExtractionShell.y: 506
*ExtractionShell.width: 510
*ExtractionShell.height: 410
*ExtractionShell.geometry: "+10+60"
*ExtractionShell.title: "XEchelle: Extraction"

*form31.class: form
*form31.parent: ExtractionShell
*form31.static: true
*form31.name: form31
*form31.resizePolicy: "resize_none"
*form31.unitType: "pixels"
*form31.x: 4
*form31.y: 2
*form31.width: 500
*form31.height: 344
*form31.background: WindowBackground

*label88.class: label
*label88.parent: form31
*label88.static: true
*label88.name: label88
*label88.x: 22
*label88.y: 168
*label88.width: 170
*label88.height: 30
*label88.background: LabelBackground
*label88.fontList: TextFont
*label88.labelString: "Read-Out Noise:"
*label88.alignment: "alignment_beginning"
*label88.foreground: TextForeground

*tf_ywidth3.class: textField
*tf_ywidth3.parent: form31
*tf_ywidth3.static: true
*tf_ywidth3.name: tf_ywidth3
*tf_ywidth3.x: 212
*tf_ywidth3.y: 166
*tf_ywidth3.width: 82
*tf_ywidth3.height: 34
*tf_ywidth3.background: TextBackground
*tf_ywidth3.fontList: TextFont
*tf_ywidth3.highlightOnEnter: "true"
*tf_ywidth3.foreground: TextForeground
*tf_ywidth3.losingFocusCallback: {\
\
}

*label89.class: label
*label89.parent: form31
*label89.static: true
*label89.name: label89
*label89.x: 22
*label89.y: 204
*label89.width: 170
*label89.height: 30
*label89.background: LabelBackground
*label89.fontList: TextFont
*label89.labelString: "Gain:"
*label89.alignment: "alignment_beginning"
*label89.foreground: TextForeground

*label90.class: label
*label90.parent: form31
*label90.static: true
*label90.name: label90
*label90.x: 22
*label90.y: 92
*label90.width: 170
*label90.height: 30
*label90.background: LabelBackground
*label90.fontList: TextFont
*label90.labelString: "Slit:"
*label90.alignment: "alignment_beginning"
*label90.foreground: TextForeground

*tf_ystep3.class: textField
*tf_ystep3.parent: form31
*tf_ystep3.static: true
*tf_ystep3.name: tf_ystep3
*tf_ystep3.x: 212
*tf_ystep3.y: 200
*tf_ystep3.width: 82
*tf_ystep3.height: 34
*tf_ystep3.background: TextBackground
*tf_ystep3.fontList: TextFont
*tf_ystep3.highlightOnEnter: "true"
*tf_ystep3.foreground: TextForeground
*tf_ystep3.losingFocusCallback: {\
\
}

*tf_width3.class: textField
*tf_width3.parent: form31
*tf_width3.static: true
*tf_width3.name: tf_width3
*tf_width3.x: 212
*tf_width3.y: 128
*tf_width3.width: 82
*tf_width3.height: 34
*tf_width3.background: TextBackground
*tf_width3.fontList: TextFont
*tf_width3.highlightOnEnter: "true"
*tf_width3.foreground: TextForeground
*tf_width3.losingFocusCallback: {\
\
}

*rowColumn27.class: rowColumn
*rowColumn27.parent: form31
*rowColumn27.static: true
*rowColumn27.name: rowColumn27
*rowColumn27.x: 336
*rowColumn27.y: 116
*rowColumn27.width: 106
*rowColumn27.height: 96
*rowColumn27.radioBehavior: "true"
*rowColumn27.background: WindowBackground
*rowColumn27.entryBorder: 0
*rowColumn27.labelString: ""
*rowColumn27.shadowThickness: 0
*rowColumn27.borderWidth: 0
*rowColumn27.entryAlignment: "alignment_beginning"
*rowColumn27.adjustLast: "false"
*rowColumn27.adjustMargin: "true"
*rowColumn27.isAligned: "true"

*rb_seamtd_gaus3.class: toggleButton
*rb_seamtd_gaus3.parent: rowColumn27
*rb_seamtd_gaus3.static: true
*rb_seamtd_gaus3.name: rb_seamtd_gaus3
*rb_seamtd_gaus3.x: 12
*rb_seamtd_gaus3.y: 12
*rb_seamtd_gaus3.width: 148
*rb_seamtd_gaus3.height: 30
*rb_seamtd_gaus3.background: WindowBackground
*rb_seamtd_gaus3.fontList: TextFont
*rb_seamtd_gaus3.labelString: "Average"
*rb_seamtd_gaus3.set: "true"
*rb_seamtd_gaus3.selectColor: SelectColor
*rb_seamtd_gaus3.highlightOnEnter: "true"
*rb_seamtd_gaus3.valueChangedCallback: {\
\
}
*rb_seamtd_gaus3.indicatorSize: 16
*rb_seamtd_gaus3.foreground: TextForeground
*rb_seamtd_gaus3.armCallback: RadioSet(UxWidget);
*rb_seamtd_gaus3.translations: transTable18

*rb_seamtd_grav3.class: toggleButton
*rb_seamtd_grav3.parent: rowColumn27
*rb_seamtd_grav3.static: true
*rb_seamtd_grav3.name: rb_seamtd_grav3
*rb_seamtd_grav3.x: 5
*rb_seamtd_grav3.y: 85
*rb_seamtd_grav3.width: 148
*rb_seamtd_grav3.height: 30
*rb_seamtd_grav3.background: WindowBackground
*rb_seamtd_grav3.fontList: TextFont
*rb_seamtd_grav3.labelString: "Linear"
*rb_seamtd_grav3.selectColor: SelectColor
*rb_seamtd_grav3.highlightOnEnter: "true"
*rb_seamtd_grav3.valueChangedCallback: {\
\
}
*rb_seamtd_grav3.indicatorSize: 16
*rb_seamtd_grav3.foreground: TextForeground
*rb_seamtd_grav3.armCallback: RadioSet(UxWidget);
*rb_seamtd_grav3.translations: transTable18

*rb_seamtd_maxi3.class: toggleButton
*rb_seamtd_maxi3.parent: rowColumn27
*rb_seamtd_maxi3.static: true
*rb_seamtd_maxi3.name: rb_seamtd_maxi3
*rb_seamtd_maxi3.x: 4
*rb_seamtd_maxi3.y: 66
*rb_seamtd_maxi3.width: 96
*rb_seamtd_maxi3.height: 24
*rb_seamtd_maxi3.background: WindowBackground
*rb_seamtd_maxi3.fontList: TextFont
*rb_seamtd_maxi3.labelString: "Optimal"
*rb_seamtd_maxi3.selectColor: SelectColor
*rb_seamtd_maxi3.highlightOnEnter: "true"
*rb_seamtd_maxi3.valueChangedCallback: {\
\
}
*rb_seamtd_maxi3.indicatorSize: 16
*rb_seamtd_maxi3.foreground: TextForeground
*rb_seamtd_maxi3.armCallback: RadioSet(UxWidget);
*rb_seamtd_maxi3.translations: transTable18

*label91.class: label
*label91.parent: form31
*label91.static: true
*label91.name: label91
*label91.x: 340
*label91.y: 86
*label91.width: 106
*label91.height: 30
*label91.background: LabelBackground
*label91.fontList: TextFont
*label91.labelString: "Method"
*label91.alignment: "alignment_center"
*label91.foreground: TextForeground

*separatorGadget1.class: separatorGadget
*separatorGadget1.parent: form31
*separatorGadget1.static: true
*separatorGadget1.name: separatorGadget1
*separatorGadget1.x: 454
*separatorGadget1.y: 100
*separatorGadget1.width: 12
*separatorGadget1.height: 120
*separatorGadget1.orientation: "vertical"

*separatorGadget2.class: separatorGadget
*separatorGadget2.parent: form31
*separatorGadget2.static: true
*separatorGadget2.name: separatorGadget2
*separatorGadget2.x: 318
*separatorGadget2.y: 100
*separatorGadget2.width: 12
*separatorGadget2.height: 120
*separatorGadget2.orientation: "vertical"

*separatorGadget3.class: separatorGadget
*separatorGadget3.parent: form31
*separatorGadget3.static: true
*separatorGadget3.name: separatorGadget3
*separatorGadget3.x: 322
*separatorGadget3.y: 98
*separatorGadget3.width: 16
*separatorGadget3.height: 10

*separatorGadget4.class: separatorGadget
*separatorGadget4.parent: form31
*separatorGadget4.static: true
*separatorGadget4.name: separatorGadget4
*separatorGadget4.x: 446
*separatorGadget4.y: 98
*separatorGadget4.width: 14
*separatorGadget4.height: 10

*tf_thres5.class: textField
*tf_thres5.parent: form31
*tf_thres5.static: true
*tf_thres5.name: tf_thres5
*tf_thres5.x: 212
*tf_thres5.y: 90
*tf_thres5.width: 82
*tf_thres5.height: 34
*tf_thres5.background: TextBackground
*tf_thres5.fontList: TextFont
*tf_thres5.highlightOnEnter: "true"
*tf_thres5.foreground: TextForeground
*tf_thres5.losingFocusCallback: {\
\
}

*form32.class: form
*form32.parent: form31
*form32.static: true
*form32.name: form32
*form32.resizePolicy: "resize_none"
*form32.x: 14
*form32.y: 360
*form32.width: 490
*form32.height: 40
*form32.background: ButtonBackground

*pb_search_search7.class: pushButton
*pb_search_search7.parent: form32
*pb_search_search7.static: true
*pb_search_search7.name: pb_search_search7
*pb_search_search7.x: 8
*pb_search_search7.y: 4
*pb_search_search7.width: 86
*pb_search_search7.height: 30
*pb_search_search7.background: ButtonBackground
*pb_search_search7.fontList: BoldTextFont
*pb_search_search7.foreground: ApplyForeground
*pb_search_search7.labelString: "Extract"
*pb_search_search7.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pushButton5.class: pushButton
*pushButton5.parent: form32
*pushButton5.static: true
*pushButton5.name: pushButton5
*pushButton5.x: 392
*pushButton5.y: 6
*pushButton5.width: 86
*pushButton5.height: 30
*pushButton5.background: ButtonBackground
*pushButton5.fontList: BoldTextFont
*pushButton5.foreground: CancelForeground
*pushButton5.labelString: "Cancel"
*pushButton5.activateCallback: {\
UxPopdownInterface(UxFindSwidget("ExtractionShell"));\
}

*pb_search_plot13.class: pushButton
*pb_search_plot13.parent: form32
*pb_search_plot13.static: true
*pb_search_plot13.name: pb_search_plot13
*pb_search_plot13.x: 102
*pb_search_plot13.y: 4
*pb_search_plot13.width: 100
*pb_search_plot13.height: 30
*pb_search_plot13.background: ButtonBackground
*pb_search_plot13.fontList: BoldTextFont
*pb_search_plot13.foreground: ButtonForeground
*pb_search_plot13.labelString: "Plot Orders"
*pb_search_plot13.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pb_search_plot18.class: pushButton
*pb_search_plot18.parent: form32
*pb_search_plot18.static: true
*pb_search_plot18.name: pb_search_plot18
*pb_search_plot18.x: 304
*pb_search_plot18.y: 4
*pb_search_plot18.width: 86
*pb_search_plot18.height: 30
*pb_search_plot18.background: ButtonBackground
*pb_search_plot18.fontList: BoldTextFont
*pb_search_plot18.foreground: ButtonForeground
*pb_search_plot18.labelString: "Help..."
*pb_search_plot18.activateCallback: {\
GetExtendedHelp(UxWidget);\
}

*shelp_search5.class: text
*shelp_search5.parent: form31
*shelp_search5.static: true
*shelp_search5.name: shelp_search5
*shelp_search5.x: 16
*shelp_search5.y: 300
*shelp_search5.width: 484
*shelp_search5.height: 50
*shelp_search5.background: SHelpBackground
*shelp_search5.cursorPositionVisible: "false"
*shelp_search5.editable: "false"
*shelp_search5.fontList: TextFont

*separator26.class: separator
*separator26.parent: form31
*separator26.static: true
*separator26.name: separator26
*separator26.x: 14
*separator26.y: 290
*separator26.width: 492
*separator26.height: 10
*separator26.background: WindowBackground

*separator27.class: separator
*separator27.parent: form31
*separator27.static: true
*separator27.name: separator27
*separator27.x: 10
*separator27.y: 348
*separator27.width: 492
*separator27.height: 10
*separator27.background: WindowBackground

*separatorGadget5.class: separatorGadget
*separatorGadget5.parent: form31
*separatorGadget5.static: true
*separatorGadget5.name: separatorGadget5
*separatorGadget5.x: 322
*separatorGadget5.y: 214
*separatorGadget5.width: 138
*separatorGadget5.height: 10

*label92.class: label
*label92.parent: form31
*label92.static: true
*label92.name: label92
*label92.x: 22
*label92.y: 128
*label92.width: 170
*label92.height: 30
*label92.background: LabelBackground
*label92.fontList: TextFont
*label92.labelString: "Offset:"
*label92.alignment: "alignment_beginning"
*label92.foreground: TextForeground

*tf_thres6.class: textField
*tf_thres6.parent: form31
*tf_thres6.static: true
*tf_thres6.name: tf_thres6
*tf_thres6.x: 208
*tf_thres6.y: 12
*tf_thres6.width: 212
*tf_thres6.height: 34
*tf_thres6.background: TextBackground
*tf_thres6.fontList: TextFont
*tf_thres6.highlightOnEnter: "true"
*tf_thres6.foreground: TextForeground
*tf_thres6.losingFocusCallback: {\
\
}

*label93.class: label
*label93.parent: form31
*label93.static: true
*label93.name: label93
*label93.x: 20
*label93.y: 14
*label93.width: 170
*label93.height: 30
*label93.background: LabelBackground
*label93.fontList: TextFont
*label93.labelString: "Input Frame:"
*label93.alignment: "alignment_beginning"
*label93.foreground: TextForeground

*pb_main_search19.class: pushButton
*pb_main_search19.parent: form31
*pb_main_search19.static: true
*pb_main_search19.name: pb_main_search19
*pb_main_search19.x: 428
*pb_main_search19.y: 14
*pb_main_search19.height: 28
*pb_main_search19.background: ButtonBackground
*pb_main_search19.fontList: BoldTextFont
*pb_main_search19.foreground: ButtonForeground
*pb_main_search19.labelString: "..."
*pb_main_search19.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search19.recomputeSize: "true"
*pb_main_search19.width: 46

*label94.class: label
*label94.parent: form31
*label94.static: true
*label94.name: label94
*label94.x: 20
*label94.y: 56
*label94.width: 170
*label94.height: 30
*label94.background: LabelBackground
*label94.fontList: TextFont
*label94.labelString: "Output Frame:"
*label94.alignment: "alignment_beginning"
*label94.foreground: TextForeground

*tf_thres7.class: textField
*tf_thres7.parent: form31
*tf_thres7.static: true
*tf_thres7.name: tf_thres7
*tf_thres7.x: 210
*tf_thres7.y: 50
*tf_thres7.width: 212
*tf_thres7.height: 34
*tf_thres7.background: TextBackground
*tf_thres7.fontList: TextFont
*tf_thres7.highlightOnEnter: "true"
*tf_thres7.foreground: TextForeground
*tf_thres7.losingFocusCallback: {\
\
}

*label95.class: label
*label95.parent: form31
*label95.static: true
*label95.name: label95
*label95.x: 22
*label95.y: 242
*label95.width: 170
*label95.height: 30
*label95.background: LabelBackground
*label95.fontList: TextFont
*label95.labelString: "Threshold:"
*label95.alignment: "alignment_beginning"
*label95.foreground: TextForeground

*tf_ystep4.class: textField
*tf_ystep4.parent: form31
*tf_ystep4.static: true
*tf_ystep4.name: tf_ystep4
*tf_ystep4.x: 212
*tf_ystep4.y: 240
*tf_ystep4.width: 82
*tf_ystep4.height: 34
*tf_ystep4.background: TextBackground
*tf_ystep4.fontList: TextFont
*tf_ystep4.highlightOnEnter: "true"
*tf_ystep4.foreground: TextForeground
*tf_ystep4.losingFocusCallback: {\
\
}

*pb_main_search20.class: pushButton
*pb_main_search20.parent: form31
*pb_main_search20.static: true
*pb_main_search20.name: pb_main_search20
*pb_main_search20.x: 426
*pb_main_search20.y: 52
*pb_main_search20.height: 28
*pb_main_search20.background: ButtonBackground
*pb_main_search20.fontList: BoldTextFont
*pb_main_search20.foreground: ButtonForeground
*pb_main_search20.labelString: "..."
*pb_main_search20.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search20.recomputeSize: "true"
*pb_main_search20.width: 46

