! UIMX ascii 2.0 key: 112                                                       
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  
*action.UpdateDirectory: {\
\
 char node[80];\
\
 /* if (XmTextGetSelection(UxWidget) == NULL) return; */\
 strcpy(node,\
     XmTextGetSelection(UxWidget));\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
node[strlen(node)-1] = '\0';\
WGet_all_dirs(node);\
WChange_Midas_dir();\
WGet_all_files();\
\
}

*translation.table: SelectFileRebin
*translation.parent: RebinShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable19
*translation.parent: RebinShell
*translation.policy: override
*translation.<Btn3Down>: HelpHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*translation.table: transTable20
*translation.parent: RebinShell
*translation.policy: override
*translation.<Btn1Up>: UpdateDirectory()

*RebinShell.class: applicationShell
*RebinShell.parent: NO_PARENT
*RebinShell.static: true
*RebinShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*RebinShell.ispecdecl:
*RebinShell.funcdecl: swidget create_RebinShell()\

*RebinShell.funcname: create_RebinShell
*RebinShell.funcdef: "swidget", "<create_RebinShell>(%)"
*RebinShell.icode:
*RebinShell.fcode: return(rtrn);\

*RebinShell.auxdecl:
*RebinShell.name: RebinShell
*RebinShell.x: 383
*RebinShell.y: 105
*RebinShell.width: 490
*RebinShell.height: 263
*RebinShell.title: "XEchelle: Rebinning"
*RebinShell.keyboardFocusPolicy: "pointer"
*RebinShell.geometry: "+10+60"
*RebinShell.background: WindowBackground

*form7.class: form
*form7.parent: RebinShell
*form7.static: true
*form7.name: form7
*form7.resizePolicy: "resize_none"
*form7.unitType: "pixels"
*form7.x: 0
*form7.y: 0
*form7.width: 408
*form7.height: 348
*form7.background: WindowBackground

*label21.class: label
*label21.parent: form7
*label21.static: true
*label21.name: label21
*label21.x: 24
*label21.y: 30
*label21.width: 168
*label21.height: 30
*label21.background: LabelBackground
*label21.fontList: TextFont
*label21.labelString: "Input Frame:"
*label21.alignment: "alignment_beginning"
*label21.foreground: TextForeground

*tf_rebstrt.class: textField
*tf_rebstrt.parent: form7
*tf_rebstrt.static: true
*tf_rebstrt.name: tf_rebstrt
*tf_rebstrt.x: 201
*tf_rebstrt.y: 24
*tf_rebstrt.width: 200
*tf_rebstrt.height: 34
*tf_rebstrt.background: TextBackground
*tf_rebstrt.fontList: TextFont
*tf_rebstrt.highlightOnEnter: "true"
*tf_rebstrt.foreground: TextForeground
*tf_rebstrt.losingFocusCallback: {\
\
}

*label22.class: label
*label22.parent: form7
*label22.static: true
*label22.name: label22
*label22.x: 24
*label22.y: 66
*label22.width: 156
*label22.height: 30
*label22.background: LabelBackground
*label22.fontList: TextFont
*label22.labelString: "Output Frame:"
*label22.alignment: "alignment_beginning"
*label22.foreground: TextForeground

*label23.class: label
*label23.parent: form7
*label23.static: true
*label23.name: label23
*label23.x: 24
*label23.y: 102
*label23.width: 158
*label23.height: 30
*label23.background: LabelBackground
*label23.fontList: TextFont
*label23.labelString: "Wavelength step (A) :"
*label23.alignment: "alignment_beginning"
*label23.foreground: TextForeground

*tf_rebend.class: textField
*tf_rebend.parent: form7
*tf_rebend.static: true
*tf_rebend.name: tf_rebend
*tf_rebend.x: 201
*tf_rebend.y: 62
*tf_rebend.width: 200
*tf_rebend.height: 34
*tf_rebend.background: TextBackground
*tf_rebend.fontList: TextFont
*tf_rebend.highlightOnEnter: "true"
*tf_rebend.foreground: TextForeground
*tf_rebend.losingFocusCallback: {\
\
}

*tf_rebstp.class: textField
*tf_rebstp.parent: form7
*tf_rebstp.static: true
*tf_rebstp.name: tf_rebstp
*tf_rebstp.x: 201
*tf_rebstp.y: 100
*tf_rebstp.width: 200
*tf_rebstp.height: 34
*tf_rebstp.background: TextBackground
*tf_rebstp.fontList: TextFont
*tf_rebstp.highlightOnEnter: "true"
*tf_rebstp.foreground: TextForeground
*tf_rebstp.losingFocusCallback: {\
\
}

*form8.class: form
*form8.parent: form7
*form8.static: true
*form8.name: form8
*form8.resizePolicy: "resize_none"
*form8.x: -2
*form8.y: 222
*form8.width: 490
*form8.height: 40
*form8.background: ButtonBackground

*pb_rebin_rbr.class: pushButton
*pb_rebin_rbr.parent: form8
*pb_rebin_rbr.static: true
*pb_rebin_rbr.name: pb_rebin_rbr
*pb_rebin_rbr.x: 8
*pb_rebin_rbr.y: 4
*pb_rebin_rbr.width: 86
*pb_rebin_rbr.height: 30
*pb_rebin_rbr.background: ButtonBackground
*pb_rebin_rbr.fontList: BoldTextFont
*pb_rebin_rbr.foreground: ApplyForeground
*pb_rebin_rbr.labelString: "Rebin"
*pb_rebin_rbr.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pushButton13.class: pushButton
*pushButton13.parent: form8
*pushButton13.static: true
*pushButton13.name: pushButton13
*pushButton13.x: 394
*pushButton13.y: 2
*pushButton13.width: 86
*pushButton13.height: 30
*pushButton13.background: ButtonBackground
*pushButton13.fontList: BoldTextFont
*pushButton13.foreground: CancelForeground
*pushButton13.labelString: "Cancel"
*pushButton13.activateCallback: {\
UxPopdownInterface(UxFindSwidget("RebinShell"));\
}

*pb_rebin_plot.class: pushButton
*pb_rebin_plot.parent: form8
*pb_rebin_plot.static: true
*pb_rebin_plot.name: pb_rebin_plot
*pb_rebin_plot.x: 114
*pb_rebin_plot.y: 2
*pb_rebin_plot.width: 86
*pb_rebin_plot.height: 30
*pb_rebin_plot.background: ButtonBackground
*pb_rebin_plot.fontList: BoldTextFont
*pb_rebin_plot.foreground: ButtonForeground
*pb_rebin_plot.labelString: "Plot "
*pb_rebin_plot.activateCallback: {\
 MidasCommand(UxThisWidget);\
\
}

*pb_search_plot17.class: pushButton
*pb_search_plot17.parent: form8
*pb_search_plot17.static: true
*pb_search_plot17.name: pb_search_plot17
*pb_search_plot17.x: 296
*pb_search_plot17.y: 4
*pb_search_plot17.width: 86
*pb_search_plot17.height: 30
*pb_search_plot17.background: ButtonBackground
*pb_search_plot17.fontList: BoldTextFont
*pb_search_plot17.foreground: ButtonForeground
*pb_search_plot17.labelString: "Help..."
*pb_search_plot17.activateCallback: {\
GetExtendedHelp(UxWidget);\
}

*shelp_rebin.class: text
*shelp_rebin.parent: form7
*shelp_rebin.static: true
*shelp_rebin.name: shelp_rebin
*shelp_rebin.x: 0
*shelp_rebin.y: 162
*shelp_rebin.width: 484
*shelp_rebin.height: 50
*shelp_rebin.background: SHelpBackground
*shelp_rebin.cursorPositionVisible: "false"
*shelp_rebin.editable: "false"
*shelp_rebin.fontList: TextFont

*separator6.class: separator
*separator6.parent: form7
*separator6.static: true
*separator6.name: separator6
*separator6.x: -2
*separator6.y: 152
*separator6.width: 492
*separator6.height: 10
*separator6.background: WindowBackground

*separator7.class: separator
*separator7.parent: form7
*separator7.static: true
*separator7.name: separator7
*separator7.x: -4
*separator7.y: 210
*separator7.width: 492
*separator7.height: 10
*separator7.background: WindowBackground

*pb_main_search21.class: pushButton
*pb_main_search21.parent: form7
*pb_main_search21.static: true
*pb_main_search21.name: pb_main_search21
*pb_main_search21.x: 412
*pb_main_search21.y: 26
*pb_main_search21.height: 28
*pb_main_search21.background: ButtonBackground
*pb_main_search21.fontList: BoldTextFont
*pb_main_search21.foreground: ButtonForeground
*pb_main_search21.labelString: "..."
*pb_main_search21.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search21.recomputeSize: "true"
*pb_main_search21.width: 46

*pb_main_search22.class: pushButton
*pb_main_search22.parent: form7
*pb_main_search22.static: true
*pb_main_search22.name: pb_main_search22
*pb_main_search22.x: 414
*pb_main_search22.y: 64
*pb_main_search22.height: 28
*pb_main_search22.background: ButtonBackground
*pb_main_search22.fontList: BoldTextFont
*pb_main_search22.foreground: ButtonForeground
*pb_main_search22.labelString: "..."
*pb_main_search22.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search22.recomputeSize: "true"
*pb_main_search22.width: 46

