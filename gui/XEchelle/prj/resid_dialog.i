! UIMX ascii 2.0 key: 4559                                                      

*resid_dialog.class: applicationShell
*resid_dialog.parent: NO_PARENT
*resid_dialog.static: true
*resid_dialog.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*resid_dialog.ispecdecl:
*resid_dialog.funcdecl: swidget create_resid_dialog()\

*resid_dialog.funcname: create_resid_dialog
*resid_dialog.funcdef: "swidget", "<create_resid_dialog>(%)"
*resid_dialog.icode:
*resid_dialog.fcode: return(rtrn);\

*resid_dialog.auxdecl:
*resid_dialog.name: resid_dialog
*resid_dialog.x: 102
*resid_dialog.y: 121
*resid_dialog.width: 246
*resid_dialog.height: 105
*resid_dialog.title: "Residuals plot"
*resid_dialog.keyboardFocusPolicy: "pointer"
*resid_dialog.geometry: "+180+360"
*resid_dialog.background: WindowBackground

*form18.class: form
*form18.parent: resid_dialog
*form18.static: true
*form18.name: form18
*form18.resizePolicy: "resize_none"
*form18.unitType: "pixels"
*form18.x: 0
*form18.y: 0
*form18.width: 408
*form18.height: 348
*form18.background: WindowBackground

*form19.class: form
*form19.parent: form18
*form19.static: true
*form19.name: form19
*form19.resizePolicy: "resize_none"
*form19.x: 0
*form19.y: 62
*form19.width: 452
*form19.height: 40
*form19.background: ButtonBackground

*pushButton29.class: pushButton
*pushButton29.parent: form19
*pushButton29.static: true
*pushButton29.name: pushButton29
*pushButton29.x: 8
*pushButton29.y: 4
*pushButton29.width: 80
*pushButton29.height: 30
*pushButton29.background: ButtonBackground
*pushButton29.fontList: BoldTextFont
*pushButton29.foreground: ApplyForeground
*pushButton29.labelString: "Ok"
*pushButton29.activateCallback: {\
#include <spec_comm.h>\
 \
char command[256];\
char *text;\
extern char Plotmode[];\
\
strcpy(Plotmode, C_PLOT_RESID);\
if ( XmToggleButtonGetState(UxGetWidget(UxFindSwidget("tg_residual")))) \
    sprintf(command, "%sALL", C_PLOT_RESID);\
else {\
    text = XmTextGetString(UxGetWidget(UxFindSwidget("tf_residual")));\
    sprintf(command, "%s%s", C_PLOT_RESID, text);\
    XtFree(text);\
}\
AppendDialogText(command);\
UxPopdownInterface(UxFindSwidget("resid_dialog"));\
}

*pushButton36.class: pushButton
*pushButton36.parent: form19
*pushButton36.static: true
*pushButton36.name: pushButton36
*pushButton36.x: 100
*pushButton36.y: 4
*pushButton36.width: 80
*pushButton36.height: 30
*pushButton36.background: ButtonBackground
*pushButton36.fontList: BoldTextFont
*pushButton36.foreground: CancelForeground
*pushButton36.labelString: "Cancel"
*pushButton36.activateCallback: {\
UxPopdownInterface(UxFindSwidget("resid_dialog"));\
}

*label25.class: label
*label25.parent: form18
*label25.static: true
*label25.name: label25
*label25.x: 22
*label25.y: 10
*label25.width: 94
*label25.height: 36
*label25.background: LabelBackground
*label25.fontList: TextFont
*label25.labelString: "Row number :"
*label25.alignment: "alignment_beginning"
*label25.foreground: TextForeground

*tf_residual.class: textField
*tf_residual.parent: form18
*tf_residual.static: true
*tf_residual.name: tf_residual
*tf_residual.x: 120
*tf_residual.y: 8
*tf_residual.width: 54
*tf_residual.height: 40
*tf_residual.background: TextBackground
*tf_residual.fontList: TextFont
*tf_residual.highlightOnEnter: "true"
*tf_residual.foreground: TextForeground

*tg_residual.class: toggleButton
*tg_residual.parent: form18
*tg_residual.static: true
*tg_residual.name: tg_residual
*tg_residual.x: 176
*tg_residual.y: 10
*tg_residual.width: 60
*tg_residual.height: 36
*tg_residual.background: WindowBackground
*tg_residual.fontList: TextFont
*tg_residual.labelString: "ALL"
*tg_residual.set: "false"
*tg_residual.selectColor: SelectColor
*tg_residual.foreground: TextForeground
*tg_residual.highlightOnEnter: "true"
*tg_residual.valueChangedCallback: {\
extern int AllResidual;\
\
AllResidual = XmToggleButtonGetState(UxWidget);\
XtSetSensitive(UxGetWidget(UxFindSwidget("tf_residual")), !AllResidual);\
}
*tg_residual.indicatorSize: 16

