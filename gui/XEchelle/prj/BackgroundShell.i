! UIMX ascii 2.0 key: 2769                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  
*action.UpdateDirectory: {\
\
 char node[80];\
\
 /* if (XmTextGetSelection(UxWidget) == NULL) return; */\
 strcpy(node,\
     XmTextGetSelection(UxWidget));\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
node[strlen(node)-1] = '\0';\
WGet_all_dirs(node);\
WChange_Midas_dir();\
WGet_all_files();\
\
}

*translation.table: transTable14
*translation.parent: BackgroundShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable15
*translation.parent: BackgroundShell
*translation.policy: override
*translation.<Btn3Down>: HelpHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*translation.table: transTable16
*translation.parent: BackgroundShell
*translation.policy: override
*translation.<Btn1Up>: UpdateDirectory()

*BackgroundShell.class: transientShell
*BackgroundShell.parent: NO_PARENT
*BackgroundShell.static: true
*BackgroundShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*BackgroundShell.ispecdecl:
*BackgroundShell.funcdecl: swidget create_BackgroundShell()\

*BackgroundShell.funcname: create_BackgroundShell
*BackgroundShell.funcdef: "swidget", "<create_BackgroundShell>(%)"
*BackgroundShell.icode:
*BackgroundShell.fcode: return(rtrn);\

*BackgroundShell.auxdecl:
*BackgroundShell.name: BackgroundShell
*BackgroundShell.x: 604
*BackgroundShell.y: 466
*BackgroundShell.width: 520
*BackgroundShell.height: 505
*BackgroundShell.geometry: "+10+60"
*BackgroundShell.title: "XEchelle: Interorder Background"

*form29.class: form
*form29.parent: BackgroundShell
*form29.static: true
*form29.name: form29
*form29.resizePolicy: "resize_none"
*form29.unitType: "pixels"
*form29.x: 0
*form29.y: -2
*form29.width: 408
*form29.height: 348
*form29.background: WindowBackground

*form30.class: form
*form30.parent: form29
*form30.static: true
*form30.name: form30
*form30.resizePolicy: "resize_none"
*form30.x: 6
*form30.y: 460
*form30.width: 508
*form30.height: 40
*form30.background: ButtonBackground

*pushButton4.class: pushButton
*pushButton4.parent: form30
*pushButton4.static: true
*pushButton4.name: pushButton4
*pushButton4.x: 422
*pushButton4.y: 4
*pushButton4.width: 80
*pushButton4.height: 30
*pushButton4.background: ButtonBackground
*pushButton4.fontList: BoldTextFont
*pushButton4.foreground: CancelForeground
*pushButton4.labelString: "Cancel"
*pushButton4.activateCallback: {\
UxPopdownInterface(UxFindSwidget("BackgroundShell"));\
}

*pb_search_plot1.class: pushButton
*pb_search_plot1.parent: form30
*pb_search_plot1.static: true
*pb_search_plot1.name: pb_search_plot1
*pb_search_plot1.x: 340
*pb_search_plot1.y: 4
*pb_search_plot1.width: 80
*pb_search_plot1.height: 30
*pb_search_plot1.background: ButtonBackground
*pb_search_plot1.fontList: BoldTextFont
*pb_search_plot1.foreground: ButtonForeground
*pb_search_plot1.labelString: "Help..."
*pb_search_plot1.activateCallback: {\
GetExtendedHelp(UxWidget);\
}

*pb_search_search6.class: pushButton
*pb_search_search6.parent: form30
*pb_search_search6.static: true
*pb_search_search6.name: pb_search_search6
*pb_search_search6.x: 8
*pb_search_search6.y: 4
*pb_search_search6.width: 80
*pb_search_search6.height: 30
*pb_search_search6.background: ButtonBackground
*pb_search_search6.fontList: BoldTextFont
*pb_search_search6.foreground: ApplyForeground
*pb_search_search6.labelString: "Subtract"
*pb_search_search6.activateCallback: {\
 MidasCommand(UxThisWidget);\
\
}

*pb_search_plot9.class: pushButton
*pb_search_plot9.parent: form30
*pb_search_plot9.static: true
*pb_search_plot9.name: pb_search_plot9
*pb_search_plot9.x: 92
*pb_search_plot9.y: 4
*pb_search_plot9.width: 80
*pb_search_plot9.height: 30
*pb_search_plot9.background: ButtonBackground
*pb_search_plot9.fontList: BoldTextFont
*pb_search_plot9.foreground: ButtonForeground
*pb_search_plot9.labelString: "Positions"
*pb_search_plot9.activateCallback: {\
 MidasCommand(UxThisWidget);\
\
}

*pb_search_plot10.class: pushButton
*pb_search_plot10.parent: form30
*pb_search_plot10.static: true
*pb_search_plot10.name: pb_search_plot10
*pb_search_plot10.x: 176
*pb_search_plot10.y: 4
*pb_search_plot10.width: 80
*pb_search_plot10.height: 30
*pb_search_plot10.background: ButtonBackground
*pb_search_plot10.fontList: BoldTextFont
*pb_search_plot10.foreground: ButtonForeground
*pb_search_plot10.labelString: "Select"
*pb_search_plot10.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pb_search_plot12.class: pushButton
*pb_search_plot12.parent: form30
*pb_search_plot12.static: true
*pb_search_plot12.name: pb_search_plot12
*pb_search_plot12.x: 258
*pb_search_plot12.y: 4
*pb_search_plot12.width: 80
*pb_search_plot12.height: 30
*pb_search_plot12.background: ButtonBackground
*pb_search_plot12.fontList: BoldTextFont
*pb_search_plot12.foreground: ButtonForeground
*pb_search_plot12.labelString: "Plot"
*pb_search_plot12.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*shelp_search4.class: text
*shelp_search4.parent: form29
*shelp_search4.static: true
*shelp_search4.name: shelp_search4
*shelp_search4.x: 4
*shelp_search4.y: 402
*shelp_search4.width: 508
*shelp_search4.height: 50
*shelp_search4.background: SHelpBackground
*shelp_search4.cursorPositionVisible: "false"
*shelp_search4.editable: "false"
*shelp_search4.fontList: TextFont

*separator24.class: separator
*separator24.parent: form29
*separator24.static: true
*separator24.name: separator24
*separator24.x: 4
*separator24.y: 452
*separator24.width: 514
*separator24.height: 10
*separator24.background: WindowBackground

*separator25.class: separator
*separator25.parent: form29
*separator25.static: true
*separator25.name: separator25
*separator25.x: 2
*separator25.y: 388
*separator25.width: 518
*separator25.height: 10
*separator25.background: WindowBackground

*label69.class: label
*label69.parent: form29
*label69.static: true
*label69.name: label69
*label69.x: 16
*label69.y: 342
*label69.width: 100
*label69.height: 30
*label69.background: LabelBackground
*label69.fontList: TextFont
*label69.labelString: "Radius (pixel):"
*label69.alignment: "alignment_beginning"
*label69.foreground: TextForeground

*tf_alpha8.class: textField
*tf_alpha8.parent: form29
*tf_alpha8.static: true
*tf_alpha8.name: tf_alpha8
*tf_alpha8.x: 370
*tf_alpha8.y: 296
*tf_alpha8.width: 96
*tf_alpha8.height: 32
*tf_alpha8.background: TextBackground
*tf_alpha8.fontList: TextFont
*tf_alpha8.highlightOnEnter: "true"
*tf_alpha8.foreground: TextForeground
*tf_alpha8.losingFocusCallback: {\
\
}

*tf_alpha9.class: textField
*tf_alpha9.parent: form29
*tf_alpha9.static: true
*tf_alpha9.name: tf_alpha9
*tf_alpha9.x: 370
*tf_alpha9.y: 222
*tf_alpha9.width: 96
*tf_alpha9.height: 32
*tf_alpha9.background: TextBackground
*tf_alpha9.fontList: TextFont
*tf_alpha9.highlightOnEnter: "true"
*tf_alpha9.foreground: TextForeground
*tf_alpha9.losingFocusCallback: {\
\
}

*label70.class: label
*label70.parent: form29
*label70.static: true
*label70.name: label70
*label70.x: 222
*label70.y: 224
*label70.width: 140
*label70.height: 30
*label70.background: LabelBackground
*label70.fontList: TextFont
*label70.labelString: "Number of iterations:"
*label70.alignment: "alignment_beginning"
*label70.foreground: TextForeground

*tf_alpha10.class: textField
*tf_alpha10.parent: form29
*tf_alpha10.static: true
*tf_alpha10.name: tf_alpha10
*tf_alpha10.x: 214
*tf_alpha10.y: 338
*tf_alpha10.width: 96
*tf_alpha10.height: 32
*tf_alpha10.background: TextBackground
*tf_alpha10.fontList: TextFont
*tf_alpha10.highlightOnEnter: "true"
*tf_alpha10.foreground: TextForeground
*tf_alpha10.losingFocusCallback: {\
\
}

*tf_alpha12.class: textField
*tf_alpha12.parent: form29
*tf_alpha12.static: true
*tf_alpha12.name: tf_alpha12
*tf_alpha12.x: 368
*tf_alpha12.y: 338
*tf_alpha12.width: 96
*tf_alpha12.height: 32
*tf_alpha12.background: TextBackground
*tf_alpha12.fontList: TextFont
*tf_alpha12.highlightOnEnter: "true"
*tf_alpha12.foreground: TextForeground
*tf_alpha12.losingFocusCallback: {\
\
}

*tf_alpha13.class: textField
*tf_alpha13.parent: form29
*tf_alpha13.static: true
*tf_alpha13.name: tf_alpha13
*tf_alpha13.x: 120
*tf_alpha13.y: 222
*tf_alpha13.width: 96
*tf_alpha13.height: 32
*tf_alpha13.background: TextBackground
*tf_alpha13.fontList: TextFont
*tf_alpha13.highlightOnEnter: "true"
*tf_alpha13.foreground: TextForeground
*tf_alpha13.losingFocusCallback: {\
\
}

*label71.class: label
*label71.parent: form29
*label71.static: true
*label71.name: label71
*label71.x: 14
*label71.y: 222
*label71.width: 94
*label71.height: 30
*label71.background: LabelBackground
*label71.fontList: TextFont
*label71.labelString: "Step (pixel):"
*label71.alignment: "alignment_beginning"
*label71.foreground: TextForeground

*label72.class: label
*label72.parent: form29
*label72.static: true
*label72.name: label72
*label72.x: 16
*label72.y: 302
*label72.width: 136
*label72.height: 30
*label72.background: LabelBackground
*label72.fontList: TextFont
*label72.labelString: "Polynomial Degree:"
*label72.alignment: "alignment_beginning"
*label72.foreground: TextForeground

*tf_alpha14.class: textField
*tf_alpha14.parent: form29
*tf_alpha14.static: true
*tf_alpha14.name: tf_alpha14
*tf_alpha14.x: 216
*tf_alpha14.y: 298
*tf_alpha14.width: 96
*tf_alpha14.height: 32
*tf_alpha14.background: TextBackground
*tf_alpha14.fontList: TextFont
*tf_alpha14.highlightOnEnter: "true"
*tf_alpha14.foreground: TextForeground
*tf_alpha14.losingFocusCallback: {\
\
}

*label74.class: label
*label74.parent: form29
*label74.static: true
*label74.name: label74
*label74.x: 166
*label74.y: 340
*label74.width: 36
*label74.height: 30
*label74.background: LabelBackground
*label74.fontList: TextFont
*label74.labelString: "X="
*label74.alignment: "alignment_beginning"
*label74.foreground: TextForeground

*label75.class: label
*label75.parent: form29
*label75.static: true
*label75.name: label75
*label75.x: 342
*label75.y: 296
*label75.width: 20
*label75.height: 30
*label75.background: LabelBackground
*label75.fontList: TextFont
*label75.labelString: "Y="
*label75.alignment: "alignment_beginning"
*label75.foreground: TextForeground

*label77.class: label
*label77.parent: form29
*label77.static: true
*label77.name: label77
*label77.x: 336
*label77.y: 338
*label77.width: 20
*label77.height: 30
*label77.background: LabelBackground
*label77.fontList: TextFont
*label77.labelString: "Y="
*label77.alignment: "alignment_beginning"
*label77.foreground: TextForeground

*label78.class: label
*label78.parent: form29
*label78.static: true
*label78.name: label78
*label78.x: 166
*label78.y: 302
*label78.width: 34
*label78.height: 30
*label78.background: LabelBackground
*label78.fontList: TextFont
*label78.labelString: "X="
*label78.alignment: "alignment_beginning"
*label78.foreground: TextForeground

*toggleButton17.class: toggleButton
*toggleButton17.parent: form29
*toggleButton17.static: true
*toggleButton17.name: toggleButton17
*toggleButton17.x: 12
*toggleButton17.y: 172
*toggleButton17.width: 136
*toggleButton17.height: 29
*toggleButton17.recomputeSize: "false"
*toggleButton17.background: WindowBackground
*toggleButton17.fontList: TextFont
*toggleButton17.foreground: TextForeground
*toggleButton17.labelString: "Plot Background"
*toggleButton17.selectColor: "Yellow"
*toggleButton17.valueChangedCallback: {\
\
}
*toggleButton17.armCallback: RadioSet(UxWidget);

*rowColumn1.class: rowColumn
*rowColumn1.parent: form29
*rowColumn1.static: true
*rowColumn1.name: rowColumn1
*rowColumn1.x: 88
*rowColumn1.y: 129
*rowColumn1.width: 106
*rowColumn1.height: 96
*rowColumn1.radioBehavior: "true"
*rowColumn1.background: WindowBackground
*rowColumn1.entryBorder: 0
*rowColumn1.labelString: ""
*rowColumn1.shadowThickness: 0
*rowColumn1.borderWidth: 0
*rowColumn1.entryAlignment: "alignment_beginning"
*rowColumn1.adjustLast: "false"
*rowColumn1.adjustMargin: "true"
*rowColumn1.isAligned: "true"
*rowColumn1.orientation: "horizontal"
*rowColumn1.resizeWidth: "true"

*rb_seamtd_gaus1.class: toggleButton
*rb_seamtd_gaus1.parent: rowColumn1
*rb_seamtd_gaus1.static: true
*rb_seamtd_gaus1.name: rb_seamtd_gaus1
*rb_seamtd_gaus1.x: 12
*rb_seamtd_gaus1.y: 12
*rb_seamtd_gaus1.width: 148
*rb_seamtd_gaus1.height: 30
*rb_seamtd_gaus1.background: WindowBackground
*rb_seamtd_gaus1.fontList: TextFont
*rb_seamtd_gaus1.labelString: "Spline"
*rb_seamtd_gaus1.set: "true"
*rb_seamtd_gaus1.selectColor: SelectColor
*rb_seamtd_gaus1.highlightOnEnter: "true"
*rb_seamtd_gaus1.valueChangedCallback: {\
\
}
*rb_seamtd_gaus1.indicatorSize: 16
*rb_seamtd_gaus1.foreground: TextForeground
*rb_seamtd_gaus1.armCallback: RadioSet(UxWidget);

*rb_seamtd_grav1.class: toggleButton
*rb_seamtd_grav1.parent: rowColumn1
*rb_seamtd_grav1.static: true
*rb_seamtd_grav1.name: rb_seamtd_grav1
*rb_seamtd_grav1.x: 136
*rb_seamtd_grav1.y: 3
*rb_seamtd_grav1.width: 108
*rb_seamtd_grav1.height: 28
*rb_seamtd_grav1.background: WindowBackground
*rb_seamtd_grav1.fontList: TextFont
*rb_seamtd_grav1.labelString: "Polynomial  "
*rb_seamtd_grav1.selectColor: SelectColor
*rb_seamtd_grav1.highlightOnEnter: "true"
*rb_seamtd_grav1.valueChangedCallback: {\
\
}
*rb_seamtd_grav1.indicatorSize: 16
*rb_seamtd_grav1.foreground: TextForeground
*rb_seamtd_grav1.armCallback: RadioSet(UxWidget);

*rb_seamtd_maxi1.class: toggleButton
*rb_seamtd_maxi1.parent: rowColumn1
*rb_seamtd_maxi1.static: true
*rb_seamtd_maxi1.name: rb_seamtd_maxi1
*rb_seamtd_maxi1.x: 4
*rb_seamtd_maxi1.y: 66
*rb_seamtd_maxi1.width: 96
*rb_seamtd_maxi1.height: 24
*rb_seamtd_maxi1.background: WindowBackground
*rb_seamtd_maxi1.fontList: TextFont
*rb_seamtd_maxi1.labelString: "Smooth"
*rb_seamtd_maxi1.selectColor: SelectColor
*rb_seamtd_maxi1.highlightOnEnter: "true"
*rb_seamtd_maxi1.valueChangedCallback: {\
\
}
*rb_seamtd_maxi1.indicatorSize: 16
*rb_seamtd_maxi1.foreground: TextForeground
*rb_seamtd_maxi1.armCallback: RadioSet(UxWidget);

*label79.class: label
*label79.parent: form29
*label79.static: true
*label79.name: label79
*label79.x: 14
*label79.y: 262
*label79.width: 100
*label79.height: 30
*label79.background: LabelBackground
*label79.fontList: TextFont
*label79.labelString: "Spline degree:"
*label79.alignment: "alignment_beginning"
*label79.foreground: TextForeground

*label80.class: label
*label80.parent: form29
*label80.static: true
*label80.name: label80
*label80.x: 224
*label80.y: 262
*label80.width: 126
*label80.height: 30
*label80.background: LabelBackground
*label80.fontList: TextFont
*label80.labelString: "Smoothing Factor:"
*label80.alignment: "alignment_beginning"
*label80.foreground: TextForeground

*tf_alpha15.class: textField
*tf_alpha15.parent: form29
*tf_alpha15.static: true
*tf_alpha15.name: tf_alpha15
*tf_alpha15.x: 368
*tf_alpha15.y: 258
*tf_alpha15.width: 96
*tf_alpha15.height: 32
*tf_alpha15.background: TextBackground
*tf_alpha15.fontList: TextFont
*tf_alpha15.highlightOnEnter: "true"
*tf_alpha15.foreground: TextForeground
*tf_alpha15.losingFocusCallback: {\
\
}

*tf_alpha16.class: textField
*tf_alpha16.parent: form29
*tf_alpha16.static: true
*tf_alpha16.name: tf_alpha16
*tf_alpha16.x: 120
*tf_alpha16.y: 264
*tf_alpha16.width: 96
*tf_alpha16.height: 32
*tf_alpha16.background: TextBackground
*tf_alpha16.fontList: TextFont
*tf_alpha16.highlightOnEnter: "true"
*tf_alpha16.foreground: TextForeground
*tf_alpha16.losingFocusCallback: {\
\
}

*label81.class: label
*label81.parent: form29
*label81.static: true
*label81.name: label81
*label81.x: 14
*label81.y: 131
*label81.width: 68
*label81.height: 30
*label81.background: LabelBackground
*label81.fontList: TextFont
*label81.labelString: "Method:"
*label81.alignment: "alignment_beginning"
*label81.foreground: TextForeground

*tf_alpha17.class: textField
*tf_alpha17.parent: form29
*tf_alpha17.static: true
*tf_alpha17.name: tf_alpha17
*tf_alpha17.x: 126
*tf_alpha17.y: 8
*tf_alpha17.width: 236
*tf_alpha17.height: 32
*tf_alpha17.background: TextBackground
*tf_alpha17.fontList: TextFont
*tf_alpha17.highlightOnEnter: "true"
*tf_alpha17.foreground: TextForeground
*tf_alpha17.losingFocusCallback: {\
\
}

*tf_alpha18.class: textField
*tf_alpha18.parent: form29
*tf_alpha18.static: true
*tf_alpha18.name: tf_alpha18
*tf_alpha18.x: 132
*tf_alpha18.y: 46
*tf_alpha18.width: 228
*tf_alpha18.height: 32
*tf_alpha18.background: TextBackground
*tf_alpha18.fontList: TextFont
*tf_alpha18.highlightOnEnter: "true"
*tf_alpha18.foreground: TextForeground
*tf_alpha18.losingFocusCallback: {\
\
}

*tf_alpha19.class: textField
*tf_alpha19.parent: form29
*tf_alpha19.static: true
*tf_alpha19.name: tf_alpha19
*tf_alpha19.x: 128
*tf_alpha19.y: 90
*tf_alpha19.width: 230
*tf_alpha19.height: 32
*tf_alpha19.background: TextBackground
*tf_alpha19.fontList: TextFont
*tf_alpha19.highlightOnEnter: "true"
*tf_alpha19.foreground: TextForeground
*tf_alpha19.losingFocusCallback: {\
\
}

*label82.class: label
*label82.parent: form29
*label82.static: true
*label82.name: label82
*label82.x: 18
*label82.y: 10
*label82.width: 94
*label82.height: 30
*label82.background: LabelBackground
*label82.fontList: TextFont
*label82.labelString: "Input Frame"
*label82.alignment: "alignment_beginning"
*label82.foreground: TextForeground

*pb_main_search17.class: pushButton
*pb_main_search17.parent: form29
*pb_main_search17.static: true
*pb_main_search17.name: pb_main_search17
*pb_main_search17.x: 366
*pb_main_search17.y: 50
*pb_main_search17.height: 28
*pb_main_search17.background: ButtonBackground
*pb_main_search17.fontList: BoldTextFont
*pb_main_search17.foreground: ButtonForeground
*pb_main_search17.labelString: "..."
*pb_main_search17.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search17.recomputeSize: "true"
*pb_main_search17.width: 46

*pb_main_search18.class: pushButton
*pb_main_search18.parent: form29
*pb_main_search18.static: true
*pb_main_search18.name: pb_main_search18
*pb_main_search18.x: 364
*pb_main_search18.y: 88
*pb_main_search18.height: 28
*pb_main_search18.background: ButtonBackground
*pb_main_search18.fontList: BoldTextFont
*pb_main_search18.foreground: ButtonForeground
*pb_main_search18.labelString: "..."
*pb_main_search18.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search18.recomputeSize: "true"
*pb_main_search18.width: 46

*label83.class: label
*label83.parent: form29
*label83.static: true
*label83.name: label83
*label83.x: 4
*label83.y: 42
*label83.width: 94
*label83.height: 30
*label83.background: LabelBackground
*label83.fontList: TextFont
*label83.labelString: "- Background"
*label83.alignment: "alignment_beginning"
*label83.foreground: TextForeground

*label84.class: label
*label84.parent: form29
*label84.static: true
*label84.name: label84
*label84.x: 6
*label84.y: 86
*label84.width: 116
*label84.height: 30
*label84.background: LabelBackground
*label84.fontList: TextFont
*label84.labelString: "= Output Frame"
*label84.alignment: "alignment_beginning"
*label84.foreground: TextForeground

*tf_alpha20.class: textField
*tf_alpha20.parent: form29
*tf_alpha20.static: true
*tf_alpha20.name: tf_alpha20
*tf_alpha20.x: 394
*tf_alpha20.y: 174
*tf_alpha20.width: 78
*tf_alpha20.height: 32
*tf_alpha20.background: TextBackground
*tf_alpha20.fontList: TextFont
*tf_alpha20.highlightOnEnter: "true"
*tf_alpha20.foreground: TextForeground
*tf_alpha20.losingFocusCallback: {\
\
}
*tf_alpha20.text: "5000"

*tf_alpha21.class: textField
*tf_alpha21.parent: form29
*tf_alpha21.static: true
*tf_alpha21.name: tf_alpha21
*tf_alpha21.x: 266
*tf_alpha21.y: 172
*tf_alpha21.width: 70
*tf_alpha21.height: 32
*tf_alpha21.background: TextBackground
*tf_alpha21.fontList: TextFont
*tf_alpha21.highlightOnEnter: "true"
*tf_alpha21.foreground: TextForeground
*tf_alpha21.losingFocusCallback: {\
\
}
*tf_alpha21.text: "0"

*label85.class: label
*label85.parent: form29
*label85.static: true
*label85.name: label85
*label85.x: 222
*label85.y: 174
*label85.width: 40
*label85.height: 30
*label85.background: LabelBackground
*label85.fontList: TextFont
*label85.labelString: "Min="
*label85.alignment: "alignment_beginning"
*label85.foreground: TextForeground

*label86.class: label
*label86.parent: form29
*label86.static: true
*label86.name: label86
*label86.x: 342
*label86.y: 174
*label86.width: 50
*label86.height: 30
*label86.background: LabelBackground
*label86.fontList: TextFont
*label86.labelString: "Max="
*label86.alignment: "alignment_beginning"
*label86.foreground: TextForeground

*label87.class: label
*label87.parent: form29
*label87.static: true
*label87.name: label87
*label87.x: 162
*label87.y: 172
*label87.width: 50
*label87.height: 30
*label87.background: LabelBackground
*label87.fontList: TextFont
*label87.labelString: "Cuts:"
*label87.alignment: "alignment_beginning"
*label87.foreground: TextForeground

*pb_search_plot11.class: pushButton
*pb_search_plot11.parent: form29
*pb_search_plot11.static: true
*pb_search_plot11.name: pb_search_plot11
*pb_search_plot11.x: 420
*pb_search_plot11.y: 10
*pb_search_plot11.width: 86
*pb_search_plot11.height: 30
*pb_search_plot11.background: ButtonBackground
*pb_search_plot11.fontList: BoldTextFont
*pb_search_plot11.foreground: ButtonForeground
*pb_search_plot11.labelString: "Display"
*pb_search_plot11.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pb_search_plot15.class: pushButton
*pb_search_plot15.parent: form29
*pb_search_plot15.static: true
*pb_search_plot15.name: pb_search_plot15
*pb_search_plot15.x: 422
*pb_search_plot15.y: 48
*pb_search_plot15.width: 86
*pb_search_plot15.height: 30
*pb_search_plot15.background: ButtonBackground
*pb_search_plot15.fontList: BoldTextFont
*pb_search_plot15.foreground: ButtonForeground
*pb_search_plot15.labelString: "Display"
*pb_search_plot15.activateCallback: {\
 MidasCommand(UxThisWidget);\
\
}

*pb_search_plot16.class: pushButton
*pb_search_plot16.parent: form29
*pb_search_plot16.static: true
*pb_search_plot16.name: pb_search_plot16
*pb_search_plot16.x: 422
*pb_search_plot16.y: 88
*pb_search_plot16.width: 86
*pb_search_plot16.height: 30
*pb_search_plot16.background: ButtonBackground
*pb_search_plot16.fontList: BoldTextFont
*pb_search_plot16.foreground: ButtonForeground
*pb_search_plot16.labelString: "Display"
*pb_search_plot16.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pb_main_search8.class: pushButton
*pb_main_search8.parent: form29
*pb_main_search8.static: true
*pb_main_search8.name: pb_main_search8
*pb_main_search8.x: 366
*pb_main_search8.y: 12
*pb_main_search8.height: 28
*pb_main_search8.background: ButtonBackground
*pb_main_search8.fontList: BoldTextFont
*pb_main_search8.foreground: ButtonForeground
*pb_main_search8.labelString: "..."
*pb_main_search8.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search8.recomputeSize: "true"
*pb_main_search8.width: 46

