! UIMX ascii 2.0 key: 4538                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  
*action.UpdateDirectory: {\
\
 char node[80];\
\
 /* if (XmTextGetSelection(UxWidget) == NULL) return; */\
 strcpy(node,\
     XmTextGetSelection(UxWidget));\
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
\
node[strlen(node)-1] = '\0';\
WGet_all_dirs(node);\
WChange_Midas_dir();\
WGet_all_files();\
\
}

*translation.table: transTable21
*translation.parent: SkyShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable22
*translation.parent: SkyShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*translation.table: transTable23
*translation.parent: SkyShell
*translation.policy: override
*translation.<Btn1Up>: UpdateDirectory()

*translation.table: transTable33
*translation.parent: SkyShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*SkyShell.class: transientShell
*SkyShell.parent: NO_PARENT
*SkyShell.static: true
*SkyShell.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*SkyShell.ispecdecl:
*SkyShell.funcdecl: swidget create_SkyShell()\

*SkyShell.funcname: create_SkyShell
*SkyShell.funcdef: "swidget", "<create_SkyShell>(%)"
*SkyShell.icode:
*SkyShell.fcode: return(rtrn);\

*SkyShell.auxdecl:
*SkyShell.name: SkyShell
*SkyShell.x: 274
*SkyShell.y: 118
*SkyShell.width: 520
*SkyShell.height: 340
*SkyShell.title: "XEchelle: Sky Background"
*SkyShell.geometry: "+10+60"

*form33.class: form
*form33.parent: SkyShell
*form33.static: true
*form33.name: form33
*form33.resizePolicy: "resize_none"
*form33.unitType: "pixels"
*form33.x: -10
*form33.y: 0
*form33.width: 500
*form33.height: 344
*form33.background: WindowBackground

*form34.class: form
*form34.parent: form33
*form34.static: true
*form34.name: form34
*form34.resizePolicy: "resize_none"
*form34.x: 12
*form34.y: 296
*form34.width: 490
*form34.height: 40
*form34.background: ButtonBackground

*pb_search_search8.class: pushButton
*pb_search_search8.parent: form34
*pb_search_search8.static: true
*pb_search_search8.name: pb_search_search8
*pb_search_search8.x: 8
*pb_search_search8.y: 4
*pb_search_search8.width: 86
*pb_search_search8.height: 30
*pb_search_search8.background: ButtonBackground
*pb_search_search8.fontList: BoldTextFont
*pb_search_search8.foreground: ApplyForeground
*pb_search_search8.labelString: "Windows"
*pb_search_search8.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pushButton6.class: pushButton
*pushButton6.parent: form34
*pushButton6.static: true
*pushButton6.name: pushButton6
*pushButton6.x: 392
*pushButton6.y: 6
*pushButton6.width: 86
*pushButton6.height: 30
*pushButton6.background: ButtonBackground
*pushButton6.fontList: BoldTextFont
*pushButton6.foreground: CancelForeground
*pushButton6.labelString: "Cancel"
*pushButton6.activateCallback: {\
UxPopdownInterface(UxFindSwidget("SkyShell"));\
}

*pb_search_plot19.class: pushButton
*pb_search_plot19.parent: form34
*pb_search_plot19.static: true
*pb_search_plot19.name: pb_search_plot19
*pb_search_plot19.x: 196
*pb_search_plot19.y: 4
*pb_search_plot19.width: 100
*pb_search_plot19.height: 30
*pb_search_plot19.background: ButtonBackground
*pb_search_plot19.fontList: BoldTextFont
*pb_search_plot19.foreground: ButtonForeground
*pb_search_plot19.labelString: "Plot Orders"
*pb_search_plot19.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pb_search_plot20.class: pushButton
*pb_search_plot20.parent: form34
*pb_search_plot20.static: true
*pb_search_plot20.name: pb_search_plot20
*pb_search_plot20.x: 304
*pb_search_plot20.y: 4
*pb_search_plot20.width: 86
*pb_search_plot20.height: 30
*pb_search_plot20.background: ButtonBackground
*pb_search_plot20.fontList: BoldTextFont
*pb_search_plot20.foreground: ButtonForeground
*pb_search_plot20.labelString: "Help..."
*pb_search_plot20.activateCallback: {\
GetExtendedHelp(UxWidget);\
}

*pb_search_search9.class: pushButton
*pb_search_search9.parent: form34
*pb_search_search9.static: true
*pb_search_search9.name: pb_search_search9
*pb_search_search9.x: 104
*pb_search_search9.y: 4
*pb_search_search9.width: 86
*pb_search_search9.height: 30
*pb_search_search9.background: ButtonBackground
*pb_search_search9.fontList: BoldTextFont
*pb_search_search9.foreground: ApplyForeground
*pb_search_search9.labelString: "Extract"
*pb_search_search9.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*shelp_search6.class: text
*shelp_search6.parent: form33
*shelp_search6.static: true
*shelp_search6.name: shelp_search6
*shelp_search6.x: 14
*shelp_search6.y: 236
*shelp_search6.width: 484
*shelp_search6.height: 50
*shelp_search6.background: SHelpBackground
*shelp_search6.cursorPositionVisible: "false"
*shelp_search6.editable: "false"
*shelp_search6.fontList: TextFont

*separator28.class: separator
*separator28.parent: form33
*separator28.static: true
*separator28.name: separator28
*separator28.x: 12
*separator28.y: 226
*separator28.width: 492
*separator28.height: 10
*separator28.background: WindowBackground

*separator29.class: separator
*separator29.parent: form33
*separator29.static: true
*separator29.name: separator29
*separator29.x: 8
*separator29.y: 284
*separator29.width: 492
*separator29.height: 10
*separator29.background: WindowBackground

*tf_thres9.class: textField
*tf_thres9.parent: form33
*tf_thres9.static: true
*tf_thres9.name: tf_thres9
*tf_thres9.x: 208
*tf_thres9.y: 12
*tf_thres9.width: 212
*tf_thres9.height: 34
*tf_thres9.background: TextBackground
*tf_thres9.fontList: TextFont
*tf_thres9.highlightOnEnter: "true"
*tf_thres9.foreground: TextForeground
*tf_thres9.losingFocusCallback: {\
\
}

*label100.class: label
*label100.parent: form33
*label100.static: true
*label100.name: label100
*label100.x: 20
*label100.y: 14
*label100.width: 170
*label100.height: 30
*label100.background: LabelBackground
*label100.fontList: TextFont
*label100.labelString: "Input Frame:"
*label100.alignment: "alignment_beginning"
*label100.foreground: TextForeground

*pb_main_search23.class: pushButton
*pb_main_search23.parent: form33
*pb_main_search23.static: true
*pb_main_search23.name: pb_main_search23
*pb_main_search23.x: 428
*pb_main_search23.y: 14
*pb_main_search23.height: 28
*pb_main_search23.background: ButtonBackground
*pb_main_search23.fontList: BoldTextFont
*pb_main_search23.foreground: ButtonForeground
*pb_main_search23.labelString: "..."
*pb_main_search23.activateCallback: {\
SelectList(UxWidget);\
\
\
}
*pb_main_search23.recomputeSize: "true"
*pb_main_search23.width: 46

*label101.class: label
*label101.parent: form33
*label101.static: true
*label101.name: label101
*label101.x: 20
*label101.y: 56
*label101.width: 170
*label101.height: 30
*label101.background: LabelBackground
*label101.fontList: TextFont
*label101.labelString: "Output Frame:"
*label101.alignment: "alignment_beginning"
*label101.foreground: TextForeground

*tf_thres10.class: textField
*tf_thres10.parent: form33
*tf_thres10.static: true
*tf_thres10.name: tf_thres10
*tf_thres10.x: 210
*tf_thres10.y: 50
*tf_thres10.width: 212
*tf_thres10.height: 34
*tf_thres10.background: TextBackground
*tf_thres10.fontList: TextFont
*tf_thres10.highlightOnEnter: "true"
*tf_thres10.foreground: TextForeground
*tf_thres10.losingFocusCallback: {\
\
}

*pb_main_search24.class: pushButton
*pb_main_search24.parent: form33
*pb_main_search24.static: true
*pb_main_search24.name: pb_main_search24
*pb_main_search24.x: 426
*pb_main_search24.y: 52
*pb_main_search24.height: 28
*pb_main_search24.background: ButtonBackground
*pb_main_search24.fontList: BoldTextFont
*pb_main_search24.foreground: ButtonForeground
*pb_main_search24.labelString: "..."
*pb_main_search24.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search24.recomputeSize: "true"
*pb_main_search24.width: 46

*form35.class: form
*form35.parent: form33
*form35.static: true
*form35.name: form35
*form35.resizePolicy: "resize_none"
*form35.x: 154
*form35.y: 98
*form35.width: 354
*form35.height: 40
*form35.background: WindowBackground

*label24.class: label
*label24.parent: form35
*label24.static: true
*label24.name: label24
*label24.x: 32
*label24.y: 2
*label24.width: 52
*label24.height: 35
*label24.background: LabelBackground
*label24.fontList: TextFont
*label24.labelString: "Min="
*label24.alignment: "alignment_beginning"
*label24.foreground: TextForeground
*label24.recomputeSize: "false"
*label24.bottomAttachment: "attach_form"
*label24.bottomOffset: 2

*tf_alpha22.class: textField
*tf_alpha22.parent: form35
*tf_alpha22.static: true
*tf_alpha22.name: tf_alpha22
*tf_alpha22.x: 82
*tf_alpha22.y: 2
*tf_alpha22.width: 110
*tf_alpha22.height: 35
*tf_alpha22.background: TextBackground
*tf_alpha22.fontList: TextFont
*tf_alpha22.highlightOnEnter: "true"
*tf_alpha22.foreground: TextForeground
*tf_alpha22.losingFocusCallback: {\
\
}
*tf_alpha22.bottomAttachment: "attach_form"
*tf_alpha22.bottomOffset: 2

*tf_alpha23.class: textField
*tf_alpha23.parent: form35
*tf_alpha23.static: true
*tf_alpha23.name: tf_alpha23
*tf_alpha23.x: 240
*tf_alpha23.y: 2
*tf_alpha23.width: 110
*tf_alpha23.height: 35
*tf_alpha23.background: TextBackground
*tf_alpha23.fontList: TextFont
*tf_alpha23.highlightOnEnter: "true"
*tf_alpha23.foreground: TextForeground
*tf_alpha23.losingFocusCallback: {\
\
}
*tf_alpha23.bottomAttachment: "attach_form"
*tf_alpha23.bottomOffset: 2

*label96.class: label
*label96.parent: form35
*label96.static: true
*label96.name: label96
*label96.x: 198
*label96.y: 2
*label96.width: 42
*label96.height: 35
*label96.background: LabelBackground
*label96.fontList: TextFont
*label96.labelString: "Max="
*label96.alignment: "alignment_beginning"
*label96.foreground: TextForeground
*label96.bottomAttachment: "attach_form"
*label96.bottomOffset: 2

*form36.class: form
*form36.parent: form33
*form36.static: true
*form36.name: form36
*form36.resizePolicy: "resize_none"
*form36.x: 186
*form36.y: 136
*form36.width: 322
*form36.height: 40
*form36.background: WindowBackground

*label97.class: label
*label97.parent: form36
*label97.static: true
*label97.name: label97
*label97.x: 0
*label97.y: 2
*label97.width: 52
*label97.height: 35
*label97.background: LabelBackground
*label97.fontList: TextFont
*label97.labelString: "Min="
*label97.alignment: "alignment_beginning"
*label97.foreground: TextForeground
*label97.recomputeSize: "false"
*label97.bottomAttachment: "attach_form"
*label97.bottomOffset: 2

*tf_alpha24.class: textField
*tf_alpha24.parent: form36
*tf_alpha24.static: true
*tf_alpha24.name: tf_alpha24
*tf_alpha24.x: 50
*tf_alpha24.y: 2
*tf_alpha24.width: 110
*tf_alpha24.height: 35
*tf_alpha24.background: TextBackground
*tf_alpha24.fontList: TextFont
*tf_alpha24.highlightOnEnter: "true"
*tf_alpha24.foreground: TextForeground
*tf_alpha24.losingFocusCallback: {\
\
}
*tf_alpha24.bottomAttachment: "attach_form"
*tf_alpha24.bottomOffset: 2

*tf_alpha25.class: textField
*tf_alpha25.parent: form36
*tf_alpha25.static: true
*tf_alpha25.name: tf_alpha25
*tf_alpha25.x: 206
*tf_alpha25.y: 2
*tf_alpha25.width: 110
*tf_alpha25.height: 35
*tf_alpha25.background: TextBackground
*tf_alpha25.fontList: TextFont
*tf_alpha25.highlightOnEnter: "true"
*tf_alpha25.foreground: TextForeground
*tf_alpha25.losingFocusCallback: {\
\
}
*tf_alpha25.bottomAttachment: "attach_form"
*tf_alpha25.bottomOffset: 2

*label98.class: label
*label98.parent: form36
*label98.static: true
*label98.name: label98
*label98.x: 162
*label98.y: 3
*label98.width: 46
*label98.height: 35
*label98.background: LabelBackground
*label98.fontList: TextFont
*label98.labelString: "Max="
*label98.alignment: "alignment_beginning"
*label98.foreground: TextForeground
*label98.bottomAttachment: "attach_form"
*label98.bottomOffset: 2

*toggleButton19.class: toggleButton
*toggleButton19.parent: form33
*toggleButton19.static: true
*toggleButton19.name: toggleButton19
*toggleButton19.x: 30
*toggleButton19.y: 186
*toggleButton19.width: 178
*toggleButton19.height: 35
*toggleButton19.recomputeSize: "false"
*toggleButton19.background: WindowBackground
*toggleButton19.fontList: TextFont
*toggleButton19.foreground: TextForeground
*toggleButton19.labelString: "Cosmic Rays Filtering"
*toggleButton19.selectColor: "Yellow"
*toggleButton19.valueChangedCallback: {\
if (CheckStatus(UxThisWidget) == TRUE)\
    AppendDialogText("SET/ECHEL ROTOPT=YES");\
else\
    AppendDialogText("SET/ECHEL ROTOPT=NO");\
\
}
*toggleButton19.leftAttachment: "attach_form"
*toggleButton19.leftOffset: 20

*rowColumn7.class: rowColumn
*rowColumn7.parent: form33
*rowColumn7.static: true
*rowColumn7.name: rowColumn7
*rowColumn7.x: 16
*rowColumn7.y: 98
*rowColumn7.width: 106
*rowColumn7.height: 96
*rowColumn7.radioBehavior: "true"
*rowColumn7.background: WindowBackground
*rowColumn7.entryBorder: 0
*rowColumn7.labelString: ""
*rowColumn7.shadowThickness: 0
*rowColumn7.borderWidth: 0
*rowColumn7.entryAlignment: "alignment_beginning"
*rowColumn7.adjustLast: "false"
*rowColumn7.adjustMargin: "true"
*rowColumn7.isAligned: "true"

*toggleButton16.class: toggleButton
*toggleButton16.parent: rowColumn7
*toggleButton16.static: true
*toggleButton16.name: toggleButton16
*toggleButton16.x: -16
*toggleButton16.y: -2
*toggleButton16.width: 145
*toggleButton16.height: 35
*toggleButton16.recomputeSize: "false"
*toggleButton16.background: WindowBackground
*toggleButton16.fontList: TextFont
*toggleButton16.foreground: TextForeground
*toggleButton16.labelString: "Offsets Window 1:"
*toggleButton16.selectColor: "Yellow"
*toggleButton16.valueChangedCallback: {\
\
}
*toggleButton16.armCallback: RadioSet(UxWidget);
*toggleButton16.translations: transTable22

*toggleButton18.class: toggleButton
*toggleButton18.parent: rowColumn7
*toggleButton18.static: true
*toggleButton18.name: toggleButton18
*toggleButton18.x: -16
*toggleButton18.y: 26
*toggleButton18.width: 141
*toggleButton18.height: 35
*toggleButton18.recomputeSize: "false"
*toggleButton18.background: WindowBackground
*toggleButton18.fontList: TextFont
*toggleButton18.foreground: TextForeground
*toggleButton18.labelString: "Offsets Window 2:"
*toggleButton18.selectColor: "Yellow"
*toggleButton18.valueChangedCallback: {\
\
}
*toggleButton18.armCallback: RadioSet(UxWidget);
*toggleButton18.translations: transTable22

