! UIMX ascii 2.0 key: 2250                                                      
*action.FileSelectACT: {\
#include <xm_defs.h>\
#include <spec_comm.h>\
\
extern swidget FileListInterface, TextFieldSwidget;\
extern Widget FileListWidget;\
extern char  DirSpecs[];\
extern int ListType;\
\
int  strip = 1; /* strip off */\
\
TextFieldSwidget = UxThisWidget;\
\
if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {\
    SET_LIST_TITLE("Enter line catalog");\
    ListType = LIST_LINCAT;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {\
    SET_LIST_TITLE("Enter guess table");\
    ListType = LIST_GUESS;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {\
    SET_LIST_TITLE("Enter flux table");\
    ListType = LIST_FLUX_TBL;\
}\
else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {\
    SET_LIST_TITLE("Enter extinction table");\
    ListType = LIST_EXTIN_TBL;\
}\
\
strcpy(DirSpecs, "*.tbl");	\
SetFileList(FileListWidget, strip, DirSpecs);\
UxPopupInterface(FileListInterface, exclusive_grab);\
\
}
*action.ExtendedHelp: {\
DisplayExtendedHelp(UxWidget);\
}
*action.HelpShort: {WidgetEnter(UxWidget);}
*action.ClearShort: {WidgetLeave(UxWidget);}  

*translation.table: transTable12
*translation.parent: SearchShell
*translation.policy: override
*translation.<Btn3Down>: FileSelectACT()

*translation.table: transTable13
*translation.parent: SearchShell
*translation.policy: override
*translation.<Btn3Down>: ExtendedHelp()
*translation.<EnterWindow>: HelpShort()
*translation.<LeaveWindow>: ClearShort()

*SearchShell.class: applicationShell
*SearchShell.parent: NO_PARENT
*SearchShell.static: true
*SearchShell.gbldecl: #include <stdio.h> \
#include <ExternResources.h>  
*SearchShell.ispecdecl:
*SearchShell.funcdecl: swidget create_SearchShell()\

*SearchShell.funcname: create_SearchShell
*SearchShell.funcdef: "swidget", "<create_SearchShell>(%)"
*SearchShell.icode:
*SearchShell.fcode: return(rtrn);\

*SearchShell.auxdecl:
*SearchShell.name: SearchShell
*SearchShell.x: 370
*SearchShell.y: 53
*SearchShell.width: 620
*SearchShell.height: 350
*SearchShell.title: "XEchelle: Search Arc Lines"
*SearchShell.keyboardFocusPolicy: "pointer"
*SearchShell.geometry: "+10+60"
*SearchShell.background: WindowBackground

*form2.class: form
*form2.parent: SearchShell
*form2.static: true
*form2.name: form2
*form2.resizePolicy: "resize_none"
*form2.unitType: "pixels"
*form2.x: -16
*form2.y: 0
*form2.width: 666
*form2.height: 350
*form2.background: WindowBackground
*form2.labelFontList: TextFont
*form2.textFontList: TextFont

*label9.class: label
*label9.parent: form2
*label9.static: true
*label9.name: label9
*label9.x: 354
*label9.y: 172
*label9.width: 124
*label9.height: 30
*label9.background: LabelBackground
*label9.fontList: TextFont
*label9.labelString: "Width:"
*label9.alignment: "alignment_beginning"
*label9.foreground: TextForeground
*label9.topAttachment: "attach_form"
*label9.topOffset: 160
*label9.leftAttachment: "attach_form"
*label9.leftOffset: 350

*tf_ywidth1.class: textField
*tf_ywidth1.parent: form2
*tf_ywidth1.static: true
*tf_ywidth1.name: tf_ywidth1
*tf_ywidth1.x: 480
*tf_ywidth1.y: 162
*tf_ywidth1.width: 120
*tf_ywidth1.height: 35
*tf_ywidth1.background: TextBackground
*tf_ywidth1.fontList: TextFont
*tf_ywidth1.highlightOnEnter: "true"
*tf_ywidth1.foreground: TextForeground
*tf_ywidth1.losingFocusCallback: {\
\
}
*tf_ywidth1.topAttachment: "attach_form"
*tf_ywidth1.topOffset: 160
*tf_ywidth1.leftAttachment: "attach_form"
*tf_ywidth1.leftOffset: 480

*label10.class: label
*label10.parent: form2
*label10.static: true
*label10.name: label10
*label10.x: 354
*label10.y: 120
*label10.width: 120
*label10.height: 30
*label10.background: LabelBackground
*label10.fontList: TextFont
*label10.labelString: "Threshold:"
*label10.alignment: "alignment_beginning"
*label10.foreground: TextForeground
*label10.topAttachment: "attach_form"
*label10.topOffset: 120
*label10.leftAttachment: "attach_form"
*label10.leftOffset: 350

*label11.class: label
*label11.parent: form2
*label11.static: true
*label11.name: label11
*label11.x: 28
*label11.y: 124
*label11.width: 120
*label11.height: 30
*label11.background: LabelBackground
*label11.fontList: TextFont
*label11.labelString: "Slit:"
*label11.alignment: "alignment_beginning"
*label11.foreground: TextForeground
*label11.topAttachment: "attach_form"
*label11.topOffset: 120
*label11.leftAttachment: "attach_form"
*label11.leftOffset: 20

*tf_ystep1.class: textField
*tf_ystep1.parent: form2
*tf_ystep1.static: true
*tf_ystep1.name: tf_ystep1
*tf_ystep1.x: 480
*tf_ystep1.y: 116
*tf_ystep1.width: 120
*tf_ystep1.height: 35
*tf_ystep1.background: TextBackground
*tf_ystep1.fontList: TextFont
*tf_ystep1.highlightOnEnter: "true"
*tf_ystep1.foreground: TextForeground
*tf_ystep1.losingFocusCallback: {\
\
}
*tf_ystep1.topAttachment: "attach_form"
*tf_ystep1.topOffset: 120
*tf_ystep1.leftAttachment: "attach_form"
*tf_ystep1.leftOffset: 480

*tf_width1.class: textField
*tf_width1.parent: form2
*tf_width1.static: true
*tf_width1.name: tf_width1
*tf_width1.x: 170
*tf_width1.y: 170
*tf_width1.width: 120
*tf_width1.height: 35
*tf_width1.background: TextBackground
*tf_width1.fontList: TextFont
*tf_width1.highlightOnEnter: "true"
*tf_width1.foreground: TextForeground
*tf_width1.losingFocusCallback: {\
\
}
*tf_width1.topAttachment: "attach_form"
*tf_width1.topOffset: 160
*tf_width1.leftAttachment: "attach_form"
*tf_width1.leftOffset: 170

*label12.class: label
*label12.parent: form2
*label12.static: true
*label12.name: label12
*label12.x: 28
*label12.y: 80
*label12.width: 120
*label12.height: 30
*label12.background: LabelBackground
*label12.fontList: TextFont
*label12.labelString: "Extraction method:"
*label12.alignment: "alignment_beginning"
*label12.foreground: TextForeground
*label12.topAttachment: "attach_form"
*label12.topOffset: 80
*label12.leftAttachment: "attach_form"
*label12.leftOffset: 20

*tf_thres1.class: textField
*tf_thres1.parent: form2
*tf_thres1.static: true
*tf_thres1.name: tf_thres1
*tf_thres1.x: 238
*tf_thres1.y: 182
*tf_thres1.width: 120
*tf_thres1.height: 35
*tf_thres1.background: TextBackground
*tf_thres1.fontList: TextFont
*tf_thres1.highlightOnEnter: "true"
*tf_thres1.foreground: TextForeground
*tf_thres1.losingFocusCallback: {\
\
}
*tf_thres1.topAttachment: "attach_form"
*tf_thres1.topOffset: 120
*tf_thres1.leftAttachment: "attach_form"
*tf_thres1.leftOffset: 170

*form4.class: form
*form4.parent: form2
*form4.static: true
*form4.name: form4
*form4.resizePolicy: "resize_none"
*form4.x: 0
*form4.y: 304
*form4.width: 666
*form4.height: 40
*form4.background: ButtonBackground

*pb_search_search1.class: pushButton
*pb_search_search1.parent: form4
*pb_search_search1.static: true
*pb_search_search1.name: pb_search_search1
*pb_search_search1.x: 8
*pb_search_search1.y: 4
*pb_search_search1.width: 184
*pb_search_search1.height: 30
*pb_search_search1.background: ButtonBackground
*pb_search_search1.fontList: BoldTextFont
*pb_search_search1.foreground: ApplyForeground
*pb_search_search1.labelString: "Extract and Search"
*pb_search_search1.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pushButton1.class: pushButton
*pushButton1.parent: form4
*pushButton1.static: true
*pushButton1.name: pushButton1
*pushButton1.x: 528
*pushButton1.y: 4
*pushButton1.width: 86
*pushButton1.height: 30
*pushButton1.background: ButtonBackground
*pushButton1.fontList: BoldTextFont
*pushButton1.foreground: CancelForeground
*pushButton1.labelString: "Cancel"
*pushButton1.activateCallback: {\
UxPopdownInterface(UxFindSwidget("SearchShell"));\
}

*pb_search_plot6.class: pushButton
*pb_search_plot6.parent: form4
*pb_search_plot6.static: true
*pb_search_plot6.name: pb_search_plot6
*pb_search_plot6.x: 432
*pb_search_plot6.y: 4
*pb_search_plot6.width: 86
*pb_search_plot6.height: 30
*pb_search_plot6.background: ButtonBackground
*pb_search_plot6.fontList: BoldTextFont
*pb_search_plot6.foreground: ButtonForeground
*pb_search_plot6.labelString: "Help..."
*pb_search_plot6.activateCallback: {\
GetExtendedHelp(UxWidget);\
}

*pb_search_plot7.class: pushButton
*pb_search_plot7.parent: form4
*pb_search_plot7.static: true
*pb_search_plot7.name: pb_search_plot7
*pb_search_plot7.x: 198
*pb_search_plot7.y: 4
*pb_search_plot7.width: 118
*pb_search_plot7.height: 30
*pb_search_plot7.background: ButtonBackground
*pb_search_plot7.fontList: BoldTextFont
*pb_search_plot7.foreground: ButtonForeground
*pb_search_plot7.labelString: "Load Search"
*pb_search_plot7.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*pb_search_plot8.class: pushButton
*pb_search_plot8.parent: form4
*pb_search_plot8.static: true
*pb_search_plot8.name: pb_search_plot8
*pb_search_plot8.x: 332
*pb_search_plot8.y: 4
*pb_search_plot8.width: 86
*pb_search_plot8.height: 30
*pb_search_plot8.background: ButtonBackground
*pb_search_plot8.fontList: BoldTextFont
*pb_search_plot8.foreground: ButtonForeground
*pb_search_plot8.labelString: "Save..."
*pb_search_plot8.activateCallback: {\
MidasCommand(UxThisWidget);\
\
}

*shelp_search1.class: text
*shelp_search1.parent: form2
*shelp_search1.static: true
*shelp_search1.name: shelp_search1
*shelp_search1.x: 2
*shelp_search1.y: 244
*shelp_search1.width: 664
*shelp_search1.height: 50
*shelp_search1.background: SHelpBackground
*shelp_search1.cursorPositionVisible: "false"
*shelp_search1.editable: "false"
*shelp_search1.fontList: TextFont
*shelp_search1.leftAttachment: "attach_form"
*shelp_search1.leftOffset: 5
*shelp_search1.rightAttachment: "attach_form"
*shelp_search1.rightOffset: 5

*separator2.class: separator
*separator2.parent: form2
*separator2.static: true
*separator2.name: separator2
*separator2.x: 0
*separator2.y: 234
*separator2.width: 664
*separator2.height: 10
*separator2.background: WindowBackground
*separator2.leftAttachment: "attach_form"
*separator2.leftOffset: 5
*separator2.rightAttachment: "attach_form"
*separator2.rightOffset: 5

*separator3.class: separator
*separator3.parent: form2
*separator3.static: true
*separator3.name: separator3
*separator3.x: -2
*separator3.y: 292
*separator3.width: 672
*separator3.height: 10
*separator3.background: WindowBackground
*separator3.leftAttachment: "attach_form"
*separator3.leftOffset: 5
*separator3.rightAttachment: "attach_form"
*separator3.rightOffset: 5

*label13.class: label
*label13.parent: form2
*label13.static: true
*label13.name: label13
*label13.x: 28
*label13.y: 174
*label13.width: 120
*label13.height: 30
*label13.background: LabelBackground
*label13.fontList: TextFont
*label13.labelString: "Offset:"
*label13.alignment: "alignment_beginning"
*label13.foreground: TextForeground
*label13.topAttachment: "attach_form"
*label13.topOffset: 160
*label13.leftAttachment: "attach_form"
*label13.leftOffset: 20

*label67.class: label
*label67.parent: form2
*label67.static: true
*label67.name: label67
*label67.x: 20
*label67.y: 14
*label67.width: 216
*label67.height: 30
*label67.background: LabelBackground
*label67.fontList: TextFont
*label67.labelString: "Wavelength Calibration Frame:"
*label67.alignment: "alignment_beginning"
*label67.foreground: TextForeground
*label67.bottomAttachment: "attach_none"
*label67.bottomOffset: 10
*label67.topAttachment: "attach_form"
*label67.topOffset: 20
*label67.leftAttachment: "attach_form"
*label67.leftOffset: 20

*tf_thres4.class: textField
*tf_thres4.parent: form2
*tf_thres4.static: true
*tf_thres4.name: tf_thres4
*tf_thres4.x: 242
*tf_thres4.y: 12
*tf_thres4.width: 272
*tf_thres4.height: 35
*tf_thres4.background: TextBackground
*tf_thres4.fontList: TextFont
*tf_thres4.highlightOnEnter: "true"
*tf_thres4.foreground: TextForeground
*tf_thres4.losingFocusCallback: {\
\
}
*tf_thres4.bottomAttachment: "attach_none"
*tf_thres4.bottomOffset: 10
*tf_thres4.topAttachment: "attach_form"
*tf_thres4.topOffset: 20

*pb_main_search16.class: pushButton
*pb_main_search16.parent: form2
*pb_main_search16.static: true
*pb_main_search16.name: pb_main_search16
*pb_main_search16.x: 548
*pb_main_search16.y: 14
*pb_main_search16.height: 28
*pb_main_search16.background: ButtonBackground
*pb_main_search16.fontList: BoldTextFont
*pb_main_search16.foreground: ButtonForeground
*pb_main_search16.labelString: "..."
*pb_main_search16.activateCallback: {\
SelectList(UxWidget);\
\
}
*pb_main_search16.recomputeSize: "true"
*pb_main_search16.width: 46
*pb_main_search16.bottomAttachment: "attach_none"
*pb_main_search16.bottomOffset: 10
*pb_main_search16.topAttachment: "attach_form"
*pb_main_search16.topOffset: 20

*label68.class: label
*label68.parent: form2
*label68.static: true
*label68.name: label68
*label68.x: 354
*label68.y: 82
*label68.width: 120
*label68.height: 30
*label68.background: LabelBackground
*label68.fontList: TextFont
*label68.labelString: "Search method"
*label68.alignment: "alignment_beginning"
*label68.foreground: TextForeground
*label68.topAttachment: "attach_form"
*label68.topOffset: 80
*label68.leftAttachment: "attach_form"
*label68.leftOffset: 350

*mn_tol6.class: rowColumn
*mn_tol6.parent: form2
*mn_tol6.static: true
*mn_tol6.name: mn_tol6
*mn_tol6.rowColumnType: "menu_option"
*mn_tol6.subMenuId: "menu2_p6"
*mn_tol6.x: 474
*mn_tol6.y: 76
*mn_tol6.background: WindowBackground
*mn_tol6.foreground: TextForeground
*mn_tol6.marginWidth: 0
*mn_tol6.spacing: 0
*mn_tol6.labelString: " "
*mn_tol6.width: 120
*mn_tol6.topOffset: 80
*mn_tol6.leftAttachment: "attach_form"
*mn_tol6.leftOffset: 480
*mn_tol6.translations: transTable13

*menu2_p6.class: rowColumn
*menu2_p6.parent: mn_tol6
*menu2_p6.static: true
*menu2_p6.name: menu2_p6
*menu2_p6.rowColumnType: "menu_pulldown"
*menu2_p6.background: WindowBackground
*menu2_p6.foreground: TextForeground
*menu2_p6.width: 120
*menu2_p6.x: 520

*mn_tol_angstroms6.class: pushButtonGadget
*mn_tol_angstroms6.parent: menu2_p6
*mn_tol_angstroms6.static: true
*mn_tol_angstroms6.name: mn_tol_angstroms6
*mn_tol_angstroms6.labelString: " Gaussian "
*mn_tol_angstroms6.activateCallback: {RadioSet ( UxWidget ) ; }
*mn_tol_angstroms6.fontList: TextFont
*mn_tol_angstroms6.width: 120
*mn_tol_angstroms6.x: 520

*mn_tol_pixels6.class: pushButtonGadget
*mn_tol_pixels6.parent: menu2_p6
*mn_tol_pixels6.static: true
*mn_tol_pixels6.name: mn_tol_pixels6
*mn_tol_pixels6.labelString: " Gravity "
*mn_tol_pixels6.activateCallback: {RadioSet ( UxWidget ) ; }
*mn_tol_pixels6.fontList: TextFont
*mn_tol_pixels6.width: 120
*mn_tol_pixels6.x: 520

*tf_thres12.class: textField
*tf_thres12.parent: form2
*tf_thres12.static: true
*tf_thres12.name: tf_thres12
*tf_thres12.x: 170
*tf_thres12.y: 80
*tf_thres12.width: 120
*tf_thres12.height: 35
*tf_thres12.background: WindowBackground
*tf_thres12.fontList: TextFont
*tf_thres12.highlightOnEnter: "true"
*tf_thres12.foreground: TextForeground
*tf_thres12.losingFocusCallback: {\
\
}
*tf_thres12.editable: "false"
*tf_thres12.highlightColor: WindowBackground

