! UIMX ascii 2.0 key: 5497                                                      

*extin_dialog.class: applicationShell
*extin_dialog.parent: NO_PARENT
*extin_dialog.static: true
*extin_dialog.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*extin_dialog.ispecdecl:
*extin_dialog.funcdecl: swidget create_extin_dialog()\

*extin_dialog.funcname: create_extin_dialog
*extin_dialog.funcdef: "swidget", "<create_extin_dialog>(%)"
*extin_dialog.icode:
*extin_dialog.fcode: return(rtrn);\

*extin_dialog.auxdecl:
*extin_dialog.name: extin_dialog
*extin_dialog.x: 102
*extin_dialog.y: 121
*extin_dialog.width: 342
*extin_dialog.height: 135
*extin_dialog.title: ""
*extin_dialog.keyboardFocusPolicy: "pointer"
*extin_dialog.geometry: "+100+100"
*extin_dialog.background: WindowBackground

*form16.class: form
*form16.parent: extin_dialog
*form16.static: true
*form16.name: form16
*form16.resizePolicy: "resize_none"
*form16.unitType: "pixels"
*form16.x: 0
*form16.y: 0
*form16.width: 408
*form16.height: 138
*form16.background: WindowBackground

*form17.class: form
*form17.parent: form16
*form17.static: true
*form17.name: form17
*form17.resizePolicy: "resize_none"
*form17.x: 0
*form17.y: 96
*form17.width: 340
*form17.height: 40
*form17.background: ButtonBackground

*pushButton34.class: pushButton
*pushButton34.parent: form17
*pushButton34.static: true
*pushButton34.name: pushButton34
*pushButton34.x: 8
*pushButton34.y: 4
*pushButton34.width: 80
*pushButton34.height: 30
*pushButton34.background: ButtonBackground
*pushButton34.fontList: BoldTextFont
*pushButton34.foreground: ApplyForeground
*pushButton34.labelString: "Ok"
*pushButton34.activateCallback: {\
CallbackDialog();\
}

*pushButton35.class: pushButton
*pushButton35.parent: form17
*pushButton35.static: true
*pushButton35.name: pushButton35
*pushButton35.x: 100
*pushButton35.y: 4
*pushButton35.width: 80
*pushButton35.height: 30
*pushButton35.background: ButtonBackground
*pushButton35.fontList: BoldTextFont
*pushButton35.foreground: CancelForeground
*pushButton35.labelString: "Cancel"
*pushButton35.activateCallback: {\
UxPopdownInterface(UxFindSwidget("extin_dialog"));\
}

*lb_output_extin.class: label
*lb_output_extin.parent: form16
*lb_output_extin.static: true
*lb_output_extin.name: lb_output_extin
*lb_output_extin.x: 10
*lb_output_extin.y: 56
*lb_output_extin.width: 104
*lb_output_extin.height: 30
*lb_output_extin.background: LabelBackground
*lb_output_extin.fontList: TextFont
*lb_output_extin.labelString: "Output image :"
*lb_output_extin.alignment: "alignment_beginning"
*lb_output_extin.foreground: TextForeground

*tf_output_extin.class: textField
*tf_output_extin.parent: form16
*tf_output_extin.static: true
*tf_output_extin.name: tf_output_extin
*tf_output_extin.x: 118
*tf_output_extin.y: 54
*tf_output_extin.width: 210
*tf_output_extin.height: 34
*tf_output_extin.background: TextBackground
*tf_output_extin.fontList: TextFont
*tf_output_extin.highlightOnEnter: "true"
*tf_output_extin.foreground: TextForeground
*tf_output_extin.losingFocusCallback: {\
char *text;\
extern char Session[];\
\
text = XmTextGetString(UxWidget);\
strcpy(Session, text);\
\
XtFree(text);\
}
*tf_output_extin.activateCallback: {\
CallbackDialog();\
}

*lb_airmass.class: label
*lb_airmass.parent: form16
*lb_airmass.static: true
*lb_airmass.name: lb_airmass
*lb_airmass.x: 10
*lb_airmass.y: 20
*lb_airmass.width: 106
*lb_airmass.height: 30
*lb_airmass.background: LabelBackground
*lb_airmass.fontList: TextFont
*lb_airmass.labelString: "Airmass value :"
*lb_airmass.alignment: "alignment_beginning"
*lb_airmass.foreground: TextForeground

*tf_airmass.class: textField
*tf_airmass.parent: form16
*tf_airmass.static: true
*tf_airmass.name: tf_airmass
*tf_airmass.x: 118
*tf_airmass.y: 18
*tf_airmass.width: 108
*tf_airmass.height: 34
*tf_airmass.background: TextBackground
*tf_airmass.fontList: TextFont
*tf_airmass.highlightOnEnter: "true"
*tf_airmass.foreground: TextForeground
*tf_airmass.losingFocusCallback: {\
char *text;\
extern char Session[];\
\
text = XmTextGetString(UxWidget);\
strcpy(Session, text);\
\
XtFree(text);\
}
*tf_airmass.activateCallback: {\
CallbackDialog();\
}

