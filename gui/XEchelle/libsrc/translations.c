/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090825		last modif

===========================================================================*/

#include <Xm/Xm.h>
#include <Xm/Text.h>
#include <Xm/ToggleB.h>
#include <UxLib.h>
#include <midas_def.h>
#include <ctype.h>

extern int  AppendDialogText(), SetFileList(), CallbackList();
extern int  AppendDialogTextNoWait();

int  FindLastIndex();

void InitField(), ImageOpen(), ImageClose();


int    WIndex=-1, imno;
int    IndexMemory = 0;
char   CValue[100], NValue[100];
#define NB_KEY 205      /* Number of entries in the initialisation list */
int    dbg=0; /* If dbg>=1, a number of internal check messages are displayed (Values 0,1,2,3) */

struct kiwi {

    char   keyword[9];  /* Midas Keyword */
    char   type;        /* Keyword type */
    int    position;    /* Position of the first element */
    int    length;      /* Number of elements of the Keyword */
    char   shtext[128]; /* Short Help Text */
    char   shelpw[30];  /* Short Help swidget */
    char   widget[30];  /* Swidget */
    char   wtype;       /* Text, Option, Radio, None: Not attached to a Midas keyword */
    char   midcom[30];  /* Associated Midas Command */
    char   assw[30];    /* Associated swidget */
                        /* The associated swidget gives the possibility to attach the same short help to a second swidget */
                        /* (label of a Text widget, parent of an OptionMenu of a RadioButton) */
} kiwitab[] = {

/* Panel: Rotation and Geometry (15 elements) */

{"CCDBIN",     'R', 1, 1 , "Binning Factor in X (usually 1.)                   ", "shelp_search2", "tf_alpha3",  'T', "CCDBIN=",    ""},
{"CCDBIN",     'R', 2, 1 , "Binning Factor in Y (usually 1.)                   ", "shelp_search2", "tf_alpha5",  'T', "CCDBIN=,",   ""},
{"IMSIZE",     'I', 1, 1 , "Number of columns, in pixel.                       ", "shelp_search2", "tf_alpha1",  'T', "IMSIZE=",    ""},
{"IMSIZE",     'I', 2, 1 , "Number of rows, in pixel.                          ", "shelp_search2", "tf_alpha44", 'T', "IMSIZE=,",   ""},
{"SCAN",       'I', 1, 1 , "Scan lower limit in pixels                         ", "shelp_search2", "tf_alpha2",  'T', "SCAN=",      ""},
{"SCAN",       'I', 2, 1 , "Scan upper limit in pixels                         ", "shelp_search2", "tf_alpha4",  'T', "SCAN=,",     ""},
{"DEFTIME",    'R', 1, 1,  "Default exposure time (sec)                        ", "shelp_search2", "tf_alpha6",  'T', "DEFTIME = ", ""},
{"ORDREF",     'C', 1, 60, "Reference image for order definition               ", "shelp_search2", "tf_alpha7",  'T', "ORDREF = ",  ""},

/* Option Menu widgets */
{"FLIPAXIS",   'C', 1, 4,  "Flip Axis for rotate/echelle (None, X, Y, XY)      ", "shelp_search2", "mn_tol_angstroms3",    'O', "NONE",    "mn_tol3"},
{"FLIPAXIS",   'C', 1, 4,  "Flip Axis for rotate/echelle (None, X, Y, XY)      ", "shelp_search2", "mn_tol_pixels3",       'O', "X",       "mn_tol3"},
{"FLIPAXIS",   'C', 1, 4,  "Flip Axis for rotate/echelle (None, X, Y, XY)      ", "shelp_search2", "menu2_p3_b4",          'O', "Y",       "mn_tol3"},
{"FLIPAXIS",   'C', 1, 4,  "Flip Axis for rotate/echelle (None, X, Y, XY)      ", "shelp_search2", "menu2_p3_b3",          'O', "XY",      "mn_tol3"},
{"ROTANGLE",   'R', 1, 1,  "Rotation angle for rotate/eche (0, 90, 180, 270)   ", "shelp_search2", "mn_tol_angstroms5",    'O', "0",       "mn_tol5"},
{"ROTANGLE",   'R', 1, 1,  "Rotation angle for rotate/eche (0, 90, 180, 270)   ", "shelp_search2", "mn_tol_pixels5",       'O', "90",      "mn_tol5"},
{"ROTANGLE",   'R', 1, 1,  "Rotation angle for rotate/eche (0, 90, 180, 270)   ", "shelp_search2", "menu2_p5_b3",          'O', "180",     "mn_tol5"},
{"ROTANGLE",   'R', 1, 1,  "Rotation angle for rotate/eche (0, 90, 180, 270)   ", "shelp_search2", "menu2_p5_b4",          'O', "270",     "mn_tol5"},
{"ROTMTD",     'C', 1, 12, "Rotation method                                    ", "shelp_search2", "mn_tol_angstroms2",    'O', "Emmi",    "mn_tol2"},
{"ROTMTD",     'C', 1, 12, "Rotation method                                    ", "shelp_search2", "mn_tol_pixels2",       'O', "Caspec",  "mn_tol2"},
{"ROTMTD",     'C', 1, 12, "Rotation method                                    ", "shelp_search2", "menu2_p2_b3",          'O', "Efosc",   "mn_tol2"},
{"ROTMTD",     'C', 1, 12, "Rotation method                                    ", "shelp_search2", "menu2_p2_b4",          'O', "Echelec", "mn_tol2"},

{"ROTMTD",     'C', 1, 12, "Rotation method                                    ", "shelp_search2", "toggleButton15",       'R', "DEF", "rowColumn23"},
{"ROTMTD",     'C', 1, 12, "Rotation method                                    ", "shelp_search2", "toggleButton14",       'U', "DEF", "rowColumn23"},

/* Widgets not linked to Midas keywords (Type N). Only the Short Help is used. */
{"NOMIDKEY",   'C', 1, 60, "Reference image for interactive scan determination ", "shelp_search2", "tf_alpha7",          'N', "INSTRUM = ",      ""},
{"00000000",   'C', 1, 60, "File list of images in the current directory       ", "shelp_search2", "pb_main_search32",   'L', "*.bdf",      "tf_alpha7"},
{"NOMIDKEY",   'C', 1, 60, "Interactive determination of the scan limits       ", "shelp_search2", "pb_search_search2",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_search2", "pb_search_plot2",    'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_search2", "pushButton2",        'N', "INSTRUM = ",      ""},

/*****************************************/
/* Panel: Order Definition (14 elements) */
/* Unused keywords 
"ORDTAB",     'C',1,12, "Table of order positions                             ", "shelp_main",   "tf_range1",   'T',  "ORDTAB = ",      "",
"BAKTAB",     'C',1,12, "Table of background positions                        ", "shelp_main",   "tf_range1",   'T',  "BAKTAB = ",      "",
"ECHORD",     'I',1,6,  "Nb of orders (1),                                    ", "shelp_main",   "tf_range1",   'T',  "ECHORD = ",      "",
"DEFPOL",     'I',1,2,  "Degrees of bivariate plynomial                       ", "shelp_main",   "tf_range1",   'T',  "DEFPOL = ",      "",
"WIDTH1",     'I',1,1,  "Order width                                          ", "shelp_main",   "tf_range1",   'T',  "WIDTH1 = ",      "",
"THRES1",     'R',1,1,  "Threshold for order following                        ", "shelp_main",   "tf_range1",   'T',  "THRES1 = ",      "",
*/

{"ORDREF",     'C',1,60, "Reference image for order  definition                 ", "shelp_search3",   "tf_thres3",   'T',  "ORDREF = ",      ""},
{"NBORDI",     'I',1,1,  "Number of orders  (automatic if null)                 ", "shelp_search3",   "tf_thres2",   'T',  "NBORDI = ",      ""},
{"WIDTHI",     'I',1,1,  "Order width       (automatic if null)                 ", "shelp_search3",   "tf_width2",   'T',  "WIDTHI = ",      ""},
{"THRESI",     'R',1,1,  "Thresh. for order following (automatic if null)       ", "shelp_search3",   "tf_ywidth2",  'T',  "THRESI = ",      ""},
{"SLOPE",      'R',1,1,  "Mean slope of orders                                  ", "shelp_search3",   "tf_ystep2",   'T',  "SLOPE = ",       ""},

/* Radio Buttons Short Help and link to Midas keywords */
{"DEFMTD",     'C',1,12, "Order definition methods: Hough transform detection                   ", "shelp_search3", "rb_seamtd_gaus2", 'R', "HOUGH",   ""},
{"DEFMTD",     'C',1,12, "Order definition methods: Standard threshold based detection          ", "shelp_search3", "rb_seamtd_grav2", 'R', "STD",     ""},
{"DEFMTD",     'C',1,12, "Order definition methods: Complementary threshold based detection     ", "shelp_search3", "rb_seamtd_maxi2", 'R', "COM",     ""},

/* Widgets not linked to Midas keywords (Type N). Only the Short Help is used. */
{"00000000",   'C', 1, 60, "File list of images in the current directory       ", "shelp_search3", "pb_main_search15",   'L', "*.bdf", "tf_thres3"},
{"NOMIDKEY",   'C', 1, 60, "Order Definition                                   ", "shelp_search3", "pb_search_search3",  'N', "INSTRUM = ",    ""},
{"NOMIDKEY",   'C', 1, 60, "Displays the defined orders                        ", "shelp_search3", "pb_search_plot3",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Save the order definition solution                 ", "shelp_search3", "pb_search_plot4",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_search3", "pb_search_plot5",    'N', "INSTRUM = ",    ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_search3", "pushButton3",        'N', "INSTRUM = ",    ""},

/* Panel: Lines Searching (14 elements) */

{"WLC",        'C',1,80, "Wavelength calibration spectrum                       ", "shelp_search1",   "tf_thres4",   'T',  "WLC = ",      ""},
{"SLIT",       'R',1,1,  "Slit height (pixel)                                   ", "shelp_search1",   "tf_thres1",   'T',  "SLIT = ",     ""},
{"OFFSET",     'R',1,1,  "Offset to the order reference (pixel)                 ", "shelp_search1",   "tf_width1",   'T',  "OFFSET = ",   ""},
{"WIDTH2",     'R',1,1,  "Width of lines along X axis                           ", "shelp_search1",   "tf_ywidth1",   'T',  "WIDTH2 = ",   ""},
{"THRES2",     'R',1,1,  "Relative threshold above background.                  ", "shelp_search1",   "tf_ystep1",    'T',  "THRES2 = ",   ""},
{"EXTMTD",     'C',1,12, "Extraction method. Can be set in Extraction Panel. Recommended: Average", "shelp_search1",   "tf_thres12", 'T',  "EXTMTD = ", ""},

/* Option Menus */

{"SEAMTD",     'C',1,8,  "Line search method: Gaussian Centering (recommended)  ", "shelp_search1",   "mn_tol_angstroms6", 'O',  "Gaussian",    "mn_tol6"},
{"SEAMTD",     'C',1,8,  "Line search method: Gravity Centering                 ", "shelp_search1",   "mn_tol_pixels6",    'O',  "Gravity",     "mn_tol6"},

/* Widgets not linked to Midas keywords (Type N). Only the Short Help is used. */
{"00000000",   'C', 1, 60, "File list of images in the current directory       ", "shelp_search1", "pb_main_search16",   'L', "*.bdf",  "tf_thres4"},
{"NOMIDKEY",   'C', 1, 60, "Extracts arc spectrum and searches lines           ", "shelp_search1", "pb_search_search1",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Displays lines on the arc frames                   ", "shelp_search1", "pb_search_plot7",    'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Saves the position of the arc lines                ", "shelp_search1", "pb_search_plot8",    'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_search1", "pb_search_plot6",    'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_search1", "pushButton1",        'N', "INSTRUM = ",      ""},

/* Panel Wavelength Calibration (27 elements) */

/* Unused
"LINTAB",     'C',1,60, "Table of line identifications                                         ", "shelp_calib",   "tf_range1",   'T',  "LINTAB = ",      "",
"WLCREG",     'C',1,12, "Regression mode (Standard, Robust)                                    ", "shelp_calib",   "tf_range1",   'T',  "WLCREG = ",      "",
"RELORD",     'I',1,1,  "Relative order number used to center disp. relation                   ", "shelp_calib",   "tf_range1",   'T',  "RELORD = ",      "",
"ABSORD",     'I',1,1,  "Absolute order number corresponding to RELORD                         ", "shelp_calib",   "tf_range1",   'T',  "ABSORD = ",      "",
"AVDISP",  'R', 1, 1,   "Average dispersion (wavel. units per pixel)                           ", "shelp_main",   "tf_range1",   'T',  "AVDISP=",      "",
"WCENTER", 'D', 1, 1,   "Central wavelength                                                    ", "shelp_main",   "tf_range1",   'T',  "WCENTER=",      "",
"BETA",    'R', 1, 1,   "Non-Linearity to the dispersion relation                              ", "shelp_main",   "tf_range1",   'T',  "BETA = ",      "",
*/

{"LINCAT",     'C',1,80, "Line catalog for WLC                                                  ", "shelp_calib",   "tf_guess1",   'T',  "LINCAT = ",      ""},
{"GUESS",      'C',1,60, "Name of guess session (if WLCMTD=GUESS)                               ", "shelp_calib",   "tf_guess2",   'T',  "GUESS = ",       ""},
{"TOL",        'R',1,1,  "Tolerance for single order calibration                                ", "shelp_calib",   "tf_tol",      'T',  "TOL = ",         ""},
{"DC",         'I',1,1,  "Degree for global identification [1,5]                                ", "shelp_calib",   "tf_dcx1",     'T',  "DC = ",          ""},
{"WLCNITER",   'I',1,1,  "Minimum number of iterations                                          ", "shelp_calib",   "tf_wlcniter1",   'T',  "WLCNITER = ", ""},
{"WLCNITER",   'I',2,1,  "Maximum number of iterations                                          ", "shelp_calib",   "tf_wlcniter2",   'T',  "WLCNITER =, ", ""},
{"WLCITER",    'R',3,1,  "Maximal error in pixels                                               ", "shelp_calib",   "tf_alpha",   'T',  "WLCITER =,, ",      ""},
{"WLCITER",    'R',2,1,  "Matching parameter (alpha)                                            ", "shelp_calib",   "tf_maxdev2",   'T',  "WLCITER =, ",    ""},
{"WLCITER",    'R',1,1,  "Initial tolerance (pixels)                                            ", "shelp_calib",   "tf_maxdev",   'T',  "WLCITER = ",     ""},
{"WLCITER",    'R',3,1,  "Maximal error in pixels                                               ", "shelp_calib",   "tf_maxdev1",   'T',  "WLCITER =,, ",    ""},

/* Radio Buttons Short Help and link to Midas keywords */
{"WLCMTD",     'C',1,10, "Method PAIR: Identify two lines in two overlapped orders              ", "shelp_calib",   "rb_wlcmtd_iden1", 'R',  "PAIR",  "rowColumn24"},
{"WLCMTD",     'C',1,10, "Method ANGLE: Identify a minimum of 4 lines anywhere                  ", "shelp_calib",   "rb_wlcmtd_gues4", 'R',  "ANGLE", "rowColumn24"},
{"WLCMTD",     'C',1,10, "Method RESTART: After Pair or Angle, use previous identifications     ", "shelp_calib",   "rb_wlcmtd_gues5", 'R',  "RESTART","rowColumn24"},
{"WLCMTD",     'C',1,10, "Method ORDER: Skip identification loop, recompute only dispersion rel.", "shelp_calib",   "rb_wlcmtd_gues6", 'R',  "ORDER",  "rowColumn24"},
{"WLCMTD",     'C',1,10, "Method GUESS: Use a previously saved session to calibrate             ", "shelp_calib",   "rb_wlcmtd_gues7", 'R',  "GUESS",  "rowColumn24"},

{"WLCOPT",     'C',1,60, "Dispersion relation type 1D: order by order independent polynomials   ", "shelp_calib",   "rb_wlcmtd_iden2", 'R',  "1D",     ""},
{"WLCOPT",     'C',1,60, "Dispersion relation type 2D: global bivariate polynomial              ", "shelp_calib",   "rb_wlcmtd_gues1", 'R',  "2D",     ""},

{"WLCVISU",    'C',1,3,  "Visualization of identifications (YES/NO)                             ", "shelp_calib",   "rb_wlcmtd_iden", 'R',  "YES",     ""},
{"WLCVISU",    'C',1,3,  "Visualization of identifications (YES/NO)                             ", "shelp_calib",   "rb_wlcmtd_gues", 'R',  "NO",      ""},

/* Widgets Not linked to Midas Keywords */
{"NOMIDKEY",   'C', 1, 10, "Unit for the tolerance (pixel, wavel. units)       ", "shelp_calib", "mn_tol_1",            'N',  "WLCMTD = ",      ""},
{"00000000",   'C', 1, 60, "File list of images in the current directory       ", "shelp_calib", "pb_main_search33",    'L', "*.tbl",      "tf_guess1"},
{"NOMIDKEY",   'C', 1, 60, "Performs wavelength calibration                    ", "shelp_calib", "pb_calib_calibrate",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Plots residuals after calibration                  ", "shelp_calib", "pb_calib_resid",      'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Displays identified lines after calibration        ", "shelp_calib", "pb_calib_spec",       'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_calib", "pb_calib_disper",     'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_calib", "pushButton40",        'N', "INSTRUM = ",      ""},

/* Interorder Background Panel */
{"BKGMTD",     'C',1,12, "Background method: POLY,SPLINE,SMOOTH                ", "shelp_search4",   "rb_seamtd_gaus1",   'R',  "Spline",    ""},
{"BKGMTD",     'C',1,12, "Background method: POLY,SPLINE,SMOOTH                ", "shelp_search4",   "rb_seamtd_grav1",   'R',  "Poly",      ""},
{"BKGMTD",     'C',1,12, "Background method: POLY,SPLINE,SMOOTH                ", "shelp_search4",   "rb_seamtd_maxi1",   'R',  "Smooth",    ""},
{"BKGVISU",    'C',1,3,  "Backgr. visu (YES/NO) for LOAD/ECH,SUB/BACK          ", "shelp_search4",   "toggleButton17",    'G',  "BKGVISU=",  ""},
{"BKGSTEP",    'I',1,1,  "Step along X.     (Meth. SPLINE and POLY)            ", "shelp_search4",   "tf_alpha13",        'T',  "BKGSTEP = ",      ""},
{"BKGDEG",     'I',1,1,  "Degree of spline polynomials (Meth. SPLINE)          ", "shelp_search4",   "tf_alpha16",        'T',  "BKGDEG = ",      ""},
{"BKGSMO",     'R',1,1,  "Smoothing factor             (Meth. SPLINE)          ", "shelp_search4",   "tf_alpha15",        'T',  "BKGSMO = ",      ""},
{"BKGPOL",     'I',1,1,  "Degree in X and Y of polynomial (Meth. POLY)         ", "shelp_search4",   "tf_alpha14",        'T',  "BKGPOL = ",      ""},
{"BKGPOL",     'I',2,1,  "Degree in X and Y of polynomial (Meth. POLY)         ", "shelp_search4",   "tf_alpha8",         'T',  "BKGPOL =, ",      ""},
{"BKGRAD",     'I',1,1,  "Radius in X and Y (Meth. SPLINE and SMOOTH)          ", "shelp_search4",   "tf_alpha10",        'T',  "BKGRAD = ",      ""},
{"BKGRAD",     'I',2,1,  "Radius in X and Y (Meth. SPLINE and SMOOTH)          ", "shelp_search4",   "tf_alpha12",        'T',  "BKGRAD =, ",      ""},
{"BKGNIT",     'I',1,1,  "Number of iterations        (Meth. SMOOTH)           ", "shelp_search4",   "tf_alpha9",         'T',  "BKGNIT = ",      ""},

/* Widgets not linked to Midas keywords */
{"NOMIDKEY",   'C', 1, 10, "Minimum cut value for the plot.                    ", "shelp_search4", "tf_alpha21",     'N',  "WLCMTD = ",      "label85"},
{"NOMIDKEY",   'C', 1, 10, "Maximum cut value for the plot.                    ", "shelp_search4", "tf_alpha20",     'N',  "WLCMTD = ",      "label86"},

{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search4", "pb_main_search8",     'L',  "*.bdf",  "tf_alpha17"},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search4", "pb_main_search17",    'L',  "*.bdf",  "tf_alpha18"},
{"00000000",   'C', 1, 10, "Pops up the file selection list                    ", "shelp_search4", "pb_main_search18",    'L',  "*.bdf",  "tf_alpha19"},
{"NOMIDKEY",   'C', 1, 60, "Displays file                                      ", "shelp_search4", "pb_search_plot11",    'N', "INSTRUM = ",  "pb_search_plot15"},
{"NOMIDKEY",   'C', 1, 60, "Displays file                                      ", "shelp_search4", "pb_search_plot16",    'N', "INSTRUM = ",  ""},

{"NOMIDKEY",   'C', 1, 10, "Displays the background reference positions        ", "shelp_search4", "pb_search_plot9",     'N',  "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Plots a cross-order trace of the background        ", "shelp_search4", "pb_search_plot12",    'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Subtracts background from input frame              ", "shelp_search4", "pb_search_search6",   'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Interactively unselect background ref. positions   ", "shelp_search4", "pb_search_plot10",    'N', "INSTRUM = ",      ""},

{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_search4", "pb_search_plot1",     'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_search4", "pushButton4",         'N', "INSTRUM = ",      ""},

/* Extraction Panel */

/* Widgets not linked to Midas keywords */
{"NOMIDKEY",   'C', 1, 10, "Minimum cut value for the plot.                    ", "shelp_search5", "pb_search_search7", 'N',  "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 10, "Maximum cut value for the plot.                    ", "shelp_search5", "pb_search_plot_13", 'N',  "WLCMTD = ",      ""},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search5", "pb_main_search19",  'L',  "*.bdf",  "tf_thres6"},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search5", "pb_main_search20",  'L',  "*.bdf",  "tf_thres7"},
{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_search5", "pb_search_plot18",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_search5", "pushButton5",       'N', "INSTRUM = ",      ""},

{"EXTMTD",     'C',1,12, "Background method: POLY,SPLINE,SMOOTH                ", "shelp_search5",   "rb_seamtd_gaus3",   'R',  "Average",   "rowColumn27"},
{"EXTMTD",     'C',1,12, "Background method: POLY,SPLINE,SMOOTH                ", "shelp_search5",   "rb_seamtd_grav3",   'R',  "Linear",    "rowColumn27"},
{"EXTMTD",     'C',1,12, "Background method: POLY,SPLINE,SMOOTH                ", "shelp_search5",   "rb_seamtd_maxi3",   'R',  "Optimal",   "rowColumn27"},

{"SLIT",       'R',1,1,  "Slit height (pixel)                                   ", "shelp_search5",   "tf_thres5",   'T',  "SLIT = ",      ""},
{"OFFSET",     'R',1,1,  "Offset to the order reference (pixel)                 ", "shelp_search5",   "tf_width3",   'T',  "OFFSET = ",    ""},
{"RON",        'R',1,1,  "Read-out noise of the CCD (e-)                        ", "shelp_search5",   "tf_ywidth3",   'T', "RON = ",       ""},
{"GAIN",       'R',1,1,  "Inverse gain factor of the CCD (e-/ADU                ", "shelp_search5",   "tf_ystep3",   'T',  "GAIN = ",      ""},
{"EXTSIGMA",   'R',1,1,  "Threshold  (in units of the ron)                      ", "shelp_search5",   "tf_ystep4",   'T',  "EXTSIGMA = ",  ""},

/* Sky Extraction Panel */

/* Unused
"POSSKY",     'C',1,12, "Sky feature positioning mode.                                         ", "shelp_search6",   "tf_range1",   'T',  "POSSKY = ",      "",
*/

{"NSKY",       'I',1,1,  "Number of sky windows  (1 or 2)                      ", "shelp_search6",   "toggleButton16",   'R',  "1",           "rowColumn7"},
{"NSKY",       'I',1,1,  "Number of sky windows  (1 or 2)                      ", "shelp_search6",   "toggleButton18",   'R',  "2",           "rowColumn7"},
{"SKYWIND",    'R',1,1,  "Offset limits of sky windows                         ", "shelp_search6",   "tf_alpha22",   'T',  "SKYWIND = ",      ""},
{"SKYWIND",    'R',2,1,  "Offset limits of sky windows                         ", "shelp_search6",   "tf_alpha23",   'T',  "SKYWIND =, ",     ""},
{"SKYWIND",    'R',3,1,  "Offset limits of sky windows                         ", "shelp_search6",   "tf_alpha24",   'T',  "SKYWIND =,, ",    ""},
{"SKYWIND",    'R',4,1,  "Offset limits of sky windows                         ", "shelp_search6",   "tf_alpha25",   'T',  "SKYWIND =,,, ",   ""},

{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search6", "pb_main_search23",  'L',  "*.bdf",      "tf_thres9"},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search6", "pb_main_search24",  'L',  "*.bdf",      "tf_thres10"},
{"NOMIDKEY",   'C', 1, 10, "Interactive determination of the window limits     ", "shelp_search6", "pb_search_search8", 'N',  "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 10, "Sky background extraction                          ", "shelp_search6", "pb_search_search9", 'N',  "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 10, "Plots extracted sky background                     ", "shelp_search6", "pb_search_plot_19", 'N',  "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_search6", "pb_search_plot20",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_search6", "pushButton6",       'N', "INSTRUM = ",      ""},

/* Rebin Shell */

{"SAMPLE",     'C', 1, 60, "Step in wavelength                                 ", "shelp_rebin", "tf_rebstp",         'T', "SAMPLE = ",      ""},
{"NOMIDKEY",   'C', 1, 10, "Resamples the spectrum                             ", "shelp_rebin", "pb_rebin_rbr",      'N', "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 10, "Plots the resampled orders                         ", "shelp_rebin", "pb_rebin_plot",     'N', "WLCMTD = ",      ""},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_rebin", "pb_main_search21",  'L', "*.bdf", "tf_rebstrt"},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_rebin", "pb_main_search22",  'L', "*.bdf", "tf_rebend"},
{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_rebin", "pb_search_plot17",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_rebin", "pushButton13",      'N', "INSTRUM = ",      ""},

/* Unused
"REBMTD",     'C',1,2,  "Rebin method: NONLINEAR (for REBIN/ECHELLE).                          ", "shelp_main",   "tf_range1",   'T',  "REBMTD = ",      "",
*/

/* Flat Shell */
{"FLAT",       'C',1,60, "Flat field (Order Definition: see ORDREF)            ", "shelp_search7", "tf_alpha35",   'T',  "FLAT = ",      ""},
{"CORRECT",    'C',1,60, "Flat field corrected for background                  ", "shelp_search7", "tf_alpha37",   'T',  "CORRECT = ",      ""},
{"BLAZE",      'C',1,60, "Blaze function estimated from flat-field             ", "shelp_search7", "tf_alpha11",   'T',  "BLAZE = ",      ""},

{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search7", "pb_main_search25",  'L', "*.bdf",      "tf_alpha35"},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search7", "pb_main_search26",  'L', "*.bdf",      "tf_alpha36"},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search7", "pb_main_search27",  'L', "*.bdf",      "tf_alpha37"},
{"NOMIDKEY",   'C', 1, 10, "Displays the frame                                 ", "shelp_search7", "pb_search_plot26",  'N', "WLCMTD = ",  "pb_search_plot27"},

{"NOMIDKEY",   'C', 1, 10, "Fits the blaze function                            ", "shelp_search7", "pb_search_search10",'N', "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 10, "Plots the blaze function.                          ", "shelp_search7", "pb_search_plot24",  'N', "WLCMTD = ",      ""},

{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_search7", "pb_search_plot21",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_search7", "pushButton7",       'N', "INSTRUM = ",      ""},

/* Resp Shell */

{"STD",        'C',1,60, "Name of standard star spectrum                       ", "shelp_search8",   "tf_alpha38",   'T',  "STD = ",      ""},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search8", "pb_main_search28",  'L', "*.bdf",      "tf_alpha38"},

{"RESPMTD",    'C',1,12, "Instrum. response corr. methods: STD, IUE            ", "shelp_search8",   "rb_seamtd_gaus4",   'R',  "STD",      "rowColumn3"},
{"RESPMTD",    'C',1,12, "Instrum. response corr. methods: STD, IUE            ", "shelp_search8",   "rb_seamtd_grav4",   'R',  "IUE",      "rowColumn3"},

{"FLUXTAB",    'C',1,60, "Flux table associated to STD                         ", "shelp_search8",   "tf_alpha39",   'T',  "FLUXTAB = ",      ""},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search8", "pb_main_search29",  'L', "*.tbl",      "tf_alpha39"},

{"FILTMED",    'I',1,1,  "Response median filtering (see FILTER/MED)           ", "shelp_search8",   "tf_alpha32",  'T',  "FILTMED = ",      ""},
{"FILTMED",    'I',2,1,  "Response median filtering (see FILTER/MED)           ", "shelp_search8",   "tf_alpha26",  'T',  "FILTMED =,",      ""},

{"FILTSMO",    'I',1,1,  "Response Smoothing filtering (see FILTER/SMO)        ", "shelp_search8",   "tf_alpha28",   'T',  "FILTSMO = ",      ""},
{"FILTSMO",    'I',2,1,  "Response Smoothing filtering (see FILTER/SMO)        ", "shelp_search8",   "tf_alpha30",  'T',   "FILTSMO =, ",      ""},

{"PIXNUL",     'I',1,1,  "No. of pixels zeroed on left and right edges         ", "shelp_search8",   "tf_alpha27",  'T',  "PIXNUL = ",      ""},
{"PIXNUL",     'I',2,1,  "No. of pixels zeroed on left and right edges         ", "shelp_search8",   "tf_alpha29",  'T',  "PIXNUL =,",      ""},

{"RIPMTD",     'C',1,12, "Ripple method (if RESPMTD=IUE): SINC, OVER, FIT      ", "shelp_search8",   "rb_seamtd_gaus5",   'R',  "Fit",      "rowColumn28"},
{"RIPMTD",     'C',1,12, "Ripple method (if RESPMTD=IUE): SINC, OVER, FIT      ", "shelp_search8",   "rb_seamtd_grav5",   'R',  "Over",      "rowColumn28"},
{"RIPMTD",     'C',1,12, "Ripple method (if RESPMTD=IUE): SINC, OVER, FIT      ", "shelp_search8",   "rb_seamtd_maxi5",   'R',  "Sinc",      "rowColumn28"},

{"RIPK",       'R',1,1,  "Grating constant                                     ", "shelp_search8",   "tf_alpha43",  'T',  "RIPK = ",      ""},
{"ALPHA",      'R',1,1,  "Broadening factor [0,1]                              ", "shelp_search8",   "tf_alpha34",  'T',  "ALPHA = ",      ""},
{"LAMBDA1",    'R',1,1,  "Delta   in  wavelength unit                          ", "shelp_search8",   "tf_alpha33",  'T',  "LAMBDA1 = ",      ""},
{"LAMBDA2",    'R',1,1,  "Overlap in  wavelength unit                          ", "shelp_search8",   "tf_alpha41",  'T',  "LAMBDA2 = ",      ""},

{"NOMIDKEY",   'C', 1, 10, "Fits the response function                         ", "shelp_search8", "pb_search_search12",'N', "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 10, "Plots the response function                        ", "shelp_search8", "pb_search_plot23",  'N', "WLCMTD = ",      ""},

{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_search8", "pb_search_plot22",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_search8", "pushButton8",       'N', "INSTRUM = ",      ""},

/* Unused
"FILTMED",    'I',3,1,  "Response median filtering (see FILTER/MED)           ", "shelp_search8",   "tf_range1",   'T',  "FILTMED =,, ",      "",
"FILTSMO",    'I',3,1,  "Response Smoothing filtering (see FILTER/SMO)        ", "shelp_search8",   "tf_range1",   'T',  "FILTSMO =,, ",      "",
"RESPONSE",   'C',1,60, "Name of output response function                                      ", "shelp_main",   "tf_range1",   'T',  "RESPONSE = ",      "",
"RIPFRZ",     'C',1,3,  "Freeze values K and Alpha [YES/NO]                                    ", "shelp_main",   "tf_range1",   'T',  "RIPFRZ = ",      "",
"RIPRAD",     'I',1,2,  "X radius for median and smoothing.                                    ", "shelp_main",   "tf_range1",   'T',  "RIPRAD = ",      "",
*/

/* Merge Shell */

{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search9", "pb_main_search30",  'L', "*.bdf", "tf_thres8"},
{"00000000",   'C', 1, 10, "Pops up file selection list                        ", "shelp_search9", "pb_main_search31",  'L', "*.bdf", "tf_thres11"},

{"MRGMTD",     'C',1,12, "Merging methods: NOAPPEND,AVERAGE                    ", "shelp_search9","rb_seamtd_gaus6",  'R',  "Average",   "rowColumn6"},
{"MRGMTD",     'C',1,12, "Merging methods: NOAPPEND,AVERAGE                    ", "shelp_search9","rb_seamtd_grav6",   'R',  "NoAppend",  "rowColumn6"},

{"DELTA",      'R',1,1,  "Wav. interval to be skipped (MRGMTD=AVERAGE)        ", "shelp_search9","tf_alpha42",  'T',  "DELTA = ",      ""},
{"MRGORD",     'I',1,1,  "Ord1,Ord2 (MRGMTD=NOAPP). 0,0:all orders            ", "shelp_search9","tf_alpha31",  'T',  "MRGORD = ",      ""},
{"MRGORD",     'I',2,1,  "Ord1,Ord2 (MRGMTD=NOAPP). 0,0:all orders            ", "shelp_search9","tf_alpha40",  'T',  "MRGORD =, ",      ""},

{"NOMIDKEY",   'C', 1, 10, "Merge the orders                                   ", "shelp_search9", "pb_search_search13",'N', "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 10, "Plot Overlaps                                      ", "shelp_search9", "pb_search_plot25",'N', "WLCMTD = ",      ""},
{"NOMIDKEY",   'C', 1, 10, "Plots the merged spectrum                          ", "shelp_search9", "pb_search_plot28",  'N', "WLCMTD = ",      ""},

{"NOMIDKEY",   'C', 1, 60, "Extended Help message on this panel                ", "shelp_search9", "pb_search_plot29",  'N', "INSTRUM = ",      ""},
{"NOMIDKEY",   'C', 1, 60, "Closes this window.                                ", "shelp_search9", "pushButton10",      'N', "INSTRUM = ",      ""},

/* Main Panel */

{"ROTOPT",     'C',1,3,  "Rotation/Flip option (Yes/No)                                         ", "shelp_main",   "toggleButton3",  'G', "YES", ""},
{"BKGOPT",     'C',1,3,  "Interorder Background subtraction option                              ", "shelp_main",   "toggleButton10", 'G', "YES", ""}, 
{"EXTOPT",     'C',1,3,  "Spectrum extraction option (Yes/No)                                   ", "shelp_main",   "toggleButton11", 'G', "YES", ""},
{"SKYOPT",     'C',1,3,  "Sky extraction option (Yes/No)                                        ", "shelp_main",   "toggleButton12", 'G', "YES", ""},
{"REBOPT",     'C',1,3,  "Rebin option (Yes/No)                                                 ", "shelp_main",   "toggleButton1",  'G', "YES", ""},
{"FFOPT",      'C',1,3,  "Optional flat-fielding                                                ", "shelp_main",   "toggleButton2",  'G', "YES", ""},
{"RESPOPT",    'C',1,3,  "Optional Response Correction                                          ", "shelp_main",   "toggleButton4",  'G', "YES", ""},
{"MGOPT",      'C',1,3,  "Optional merge                                                        ", "shelp_main",   "toggleButton13", 'G', "YES", ""},

{"0000000",    'C',1,3,  "Applies reduction scheme to selected images",                            "shelp_main",  "pb_main_batch", 'N',  "STD = ", ""},
{"0000000",    'C',1,3,  "Applies rotation to selected images        ",                            "shelp_main",  "pb_main_ident", 'N',  "STD = ", ""},
{"0000000",    'C',1,3,  "Loads and traces orders on the first selected image",                  "shelp_main",  "pb_main_batch1", 'N', "STD = ", ""},
{"0000000",    'C',1,3,  "Saves the echelle session (under the name sess)",                        "shelp_main",  "pb_main_batch2", 'N', "STD = ", ""},

/* Option Menu */
{"NOMIDKEY",   'C', 1, 10, "Selection of an EMMI pre-calibrated solution        ", "shelp_main",   "toggleButton9",  'N', "EMMI",      "mn_tol7"},

{"0000000",     'C',1,60, "Selection of an EMMI pre-calibrated solution        ", "shelp_main",   "mn_tol_angstroms7", 'O',  "\"EMMI Grat. 9 CD 3\"",  "mn_tol7"},
{"0000000",     'C',1,60, "Selection of an EMMI pre-calibrated solution        ", "shelp_main",   "mn_tol_pixels7",    'O',  "\"EMMI Grat. 9 CD 4\"",  "mn_tol7"},
{"0000000",     'C',1,60, "Selection of an EMMI pre-calibrated solution        ", "shelp_main",   "menu2_p7_b3",       'O',  "\"EMMI Grat. 10 CD 3\"", "mn_tol7"},
{"0000000",     'C',1,60, "Selection of an EMMI pre-calibrated solution        ", "shelp_main",   "menu2_p7_b4",       'O',  "\"EMMI Grat. 10 CD 4\"", "mn_tol7"},
{"0000000",     'C',1,60, "Selection of an EMMI pre-calibrated solution        ", "shelp_main",   "menu2_p7_b5",       'O',  "\"EMMI Grat. 10 CD 5\"", "mn_tol7"},
{"0000000",     'C',1,60, "Selection of an EMMI pre-calibrated solution        ", "shelp_main",   "menu2_p7_b6",       'O',  "\"EMMI Grat. 10 CD 6\"", "mn_tol7"},

/* Unused  (command FILTER/ECHELLE)
"CROPT",      'C',1,3,  "Cosmic rays filtering option (Yes/No)                ", "shelp_main",   "toggleButton10",   'T',  "CROPT = ",      "",
*/

{"ZLASTKEY",   'C', 1, 60, "*The* last keyword of the list                     ", "shelp_calib", "pushButton40",        'N', "INSTRUM = ",      ""},

/*
"OBJ",        'C',1,60, "Object spectrum                                                       ", "shelp_main",   "tf_range1",   'T',  "OBJ = ",      "",
"GRATING",    'C',1,60, "Disperser name                                                        ", "shelp_main",   "tf_range1",   'T',  "GRATING = ",      "",
"CROSS",      'C',1,60, "Cross disperser name                                                  ", "shelp_main",   "tf_range1",   'T',  "CROSS = ",      "",
"CCD",        'C',1,60, "CCD Name                                                              ", "shelp_main",   "tf_range1",   'T',  "CCD = ",      "",
"OBSTAB",     'C',1,60, "List of observations                                                  ", "shelp_main",   "tf_range1",   'T',  "OBSTAB = ",      "",
"WLRANGE",    'R',1,2,  "Wavelength range                                                      ", "shelp_main",   "tf_range1",   'T',  "WLRANGE = ",      "",
"CRFILT",     'I',1,3,  "Cosmic rays filtering: widx, widy, no_iter                            ", "shelp_main",   "tf_range1",   'T',  "CRFILT = ",      "",
"MEDFILT",    'R',1,3,  "Median filtering     : radx, rady, mthresh                            ", "shelp_main",   "tf_range1",   'T',  "MEDFILT = ",      "",
"CCDFILT",    'R',1,3,  "CCD parameters       : ron, g, ethresh                                ", "shelp_main",   "tf_range1",   'T',  "CCDFILT = ",      "",

"RIPORD",     'I',1,1,  "Order currently processed                                             ", "shelp_main",   "tf_range1",   'T',  "RIPORD = ",      "",
"RIPLIM",     'I',1,2,  "Order limits in pixels (0,0: all)                                     ", "shelp_main",   "tf_range1",   'T',  "RIPLIM = ",      "",
"RIPBIN",     'R',1,1,  "Bin of histogram for normalization                                   ", "shelp_main",   "tf_range1",   'T',  "RIPBIN = ",      "",
"ROTAXIS",    'C',1,3,  "Mode of rotation or flip                                              ", "shelp_main",   "tf_range1",   'T',  "ROTAXIS = ",      "",
*/

};

void InitSession(s)
char *s;

{
  int i;
  char name[100];

  strcpy(name,s);
  strcat(name,"ORDE.tbl");

  if (dbg == 1) printf("Last Index: %d. NB_KEY= %d\n",FindLastIndex(), NB_KEY);
  SCFOPN(name, D_R4_FORMAT, 1, F_TBL_TYPE, &imno);
  for (i=0; i<NB_KEY; i++) InitField(i);
  SCFCLO(imno);
}

void InitAllFields()
{
  int i;
  if (dbg == 1) printf("Last Index: %d. NB_KEY= %d\n",FindLastIndex(), NB_KEY);
  ImageOpen();
  for (i=0; i<NB_KEY; i++) InitField(i);
  ImageClose();
}


void ImageOpen()
{
  /* Open Midas Image: middummgui.bdf */
  char name[400];
  strcpy(name,UxGetText(UxFindSwidget("tf_session1")));
  strcat(name,"/middummgui.bdf");
  SCFOPN(name, D_R4_FORMAT, 1, F_IMA_TYPE, &imno);
}

void ImageClose()
{
 SCFCLO(imno);
}

int  FindIndex(sw)
Widget sw;
{
int index=0;
int found=0;

while (index < NB_KEY) {
if (sw == (Widget) UxGetWidget(UxFindSwidget(kiwitab[index].widget)))  found = 1;
if ((int) strlen(kiwitab[index].assw) > 0)
   if (sw == (Widget) UxGetWidget(UxFindSwidget(kiwitab[index].assw)))  found = 1;
if (found == 1) break;
else index++;
}

return(index);

}

int  FindLastIndex()
{
int index;
for(index=0; (kiwitab[index].keyword[0] != 'Z'); index++);
return(index);
}

void WidgetEnter(sw)
Widget sw;

{
int index;

if (dbg >= 3) printf("Entered widget\n");

if ( (index=FindIndex(sw)) >= NB_KEY) return;
UxPutText(UxFindSwidget(kiwitab[index].shelpw), kiwitab[index].shtext);
if (kiwitab[index].wtype == 'T') 
    strcpy(CValue, XmTextGetString(UxGetWidget(UxFindSwidget(kiwitab[index].widget))));
}

void WidgetLeave(w)
Widget w;

{
char command[50];
int index;

if (dbg >= 3) printf("Left widget\n");

/* Determines Widget Index */
if ( (index=FindIndex(w)) >= NB_KEY) return;

if (dbg >= 2) printf("Index %d, Type %c, Name %s\n",index,kiwitab[index].wtype,kiwitab[index].widget);

/* Empty Short Help Field */
UxPutText(UxFindSwidget(kiwitab[index].shelpw), " ");

strcpy(command,"Set/Echel ");

if (kiwitab[index].wtype == 'T') {
   strcpy(NValue, XmTextGetString(UxGetWidget(UxFindSwidget(kiwitab[index].widget))));
   if (dbg >=2) printf("Old value: %s, New Value: %s, index %d\n",CValue,NValue,index);
   if (strcmp(CValue, NValue) != 0) {
     strcat(command,kiwitab[index].midcom);
     strcat(command,XmTextGetString(w));
     AppendDialogTextNoWait(command);
   }}

/* Additional operations for a few widgets (Slit/Offset in both Search and Extraction Panels) */
if (strcmp(kiwitab[index].widget,"tf_thres1") == 0) 
    UxPutText(UxFindSwidget("tf_thres5"),NValue);
if (strcmp(kiwitab[index].widget,"tf_thres5") == 0) 
    UxPutText(UxFindSwidget("tf_thres1"),NValue);
if (strcmp(kiwitab[index].widget,"tf_width1") == 0) 
    UxPutText(UxFindSwidget("tf_width3"),NValue);
if (strcmp(kiwitab[index].widget,"tf_width3") == 0) 
    UxPutText(UxFindSwidget("tf_width1"),NValue);

}


void InitField(index)
int index;

{
int    unit, null, ival, actvals;
float  rval;
double dval;
char   field[100];

  /* Widgets not linked to Midas keywords are not processed */
  if (kiwitab[index].wtype == 'N' || kiwitab[index].keyword[0] == '0') return;

  /* Reads Midas keyword value */

  if (dbg > 1) printf("Read key %s, type %c, index %d\n",kiwitab[index].keyword,kiwitab[index].type,index);

      if (kiwitab[index].type == 'I') {
          SCDRDI(imno, kiwitab[index].keyword, kiwitab[index].position, 1, &actvals, &ival, &unit, &null);
          sprintf(field, "%d", ival);
	}
      if (kiwitab[index].type == 'R') {
          SCDRDR(imno, kiwitab[index].keyword, kiwitab[index].position, 1, &actvals, &rval, &unit, &null);
          sprintf(field, "%f", rval);
	}
      if (kiwitab[index].type == 'D') {
          SCDRDD(imno, kiwitab[index].keyword, kiwitab[index].position, 1, &actvals, &dval, &unit, &null);
          sprintf(field, "%f", dval);
	}
      if (kiwitab[index].type == 'C') {
          SCDGETC(imno, kiwitab[index].keyword, kiwitab[index].position, kiwitab[index].length, &actvals, field);
	}

  /* Text widgets */
  if (kiwitab[index].wtype == 'T')
      UxPutText(UxFindSwidget(kiwitab[index].widget), field);

  /* Toggle Buttons linked to a Yes/No Midas keyword */
  /* So far only one case in the Interorder Background Panel */
   if (kiwitab[index].wtype == 'G') { 
          if (toupper(field[0]) == 'Y') XmToggleButtonSetState(UxGetWidget(UxFindSwidget(kiwitab[index].widget)), TRUE, FALSE);
          else                          XmToggleButtonSetState(UxGetWidget(UxFindSwidget(kiwitab[index].widget)), FALSE, FALSE);
	}

  /* Radio-Buttons and Option Menus */
 
   if (toupper(field[0]) == toupper(kiwitab[index].midcom[0]) && toupper(field[1]) == toupper(kiwitab[index].midcom[1]) ) {    
   /* Case Insensitive match on the two first letters of the value */

      if (kiwitab[index].wtype == 'R')
         XmToggleButtonSetState(UxGetWidget(UxFindSwidget(kiwitab[index].widget)), TRUE, FALSE);

      if (kiwitab[index].wtype == 'O') {
         if (toupper(field[0]) == toupper(kiwitab[index].midcom[0])) {
                 if (dbg > 2) printf("Now matched with %s\n",kiwitab[index].midcom);
                 UxPutMenuHistory(
                   UxFindSwidget(kiwitab[index].assw),
                   kiwitab[index].widget);
                 /* Special Case: In Rotation panel, also set the Radio Button */
                 if (strcmp(kiwitab[index].assw, "mn_tol2") == 0) {
                     XmToggleButtonSetState(UxGetWidget(UxFindSwidget("toggleButton14")), TRUE, FALSE);
                     XmToggleButtonSetState(UxGetWidget(UxFindSwidget("toggleButton15")), FALSE,FALSE);
		 }
       }}
  }

  else { /* No match */
      if (kiwitab[index].wtype == 'R')
         XmToggleButtonSetState(UxGetWidget(UxFindSwidget(kiwitab[index].widget)), FALSE, FALSE);
    }
}


int RadioSet(sw)

Widget sw;

{
char command[50];
int index, grat, gris;

if ( (index=FindIndex(sw)) >= NB_KEY) return 1;

/* Special Case: EMMI Pre-Calibrated solutions */
if (strcmp(kiwitab[index].assw, "mn_tol7") == 0) {
              strcpy(command, 
               UxGetLabelString(UxFindSwidget(UxGetMenuHistory(UxFindSwidget("mn_tol7")))));
              sscanf(command,"#%dCD%d",&grat,&gris);
              sprintf(command,"INITIAL/EMMI %d %d", grat, gris);
              AppendDialogText(command);
              AppendDialogText("SYNCHRO/ECHELLE");
              InitAllFields();
              return(0);
   }

/* General Code */

strcpy(command,"Set/Echel ");
strcat(command,kiwitab[index].keyword);
strcat(command," = ");

switch(kiwitab[index].wtype) {

 case ('G'): /* Toggle Buttons */
              if (XmToggleButtonGetState(UxGetWidget(UxFindSwidget(kiwitab[index].widget))) == TRUE)
                 strcat(command,"NO");
             else
                 strcat(command,"YES");
            AppendDialogTextNoWait(command);
            break;

 case ('U'): /* Case Undefined (no general processing, special cases are treated below) */
            break;

 default: /* Radio-Buttons and Option Menus (types R and O) */
          strcat(command,kiwitab[index].midcom);
          AppendDialogTextNoWait(command);
          break;

	}


/* Special Case: Radio Widget for WLCMTD (Handling of GUESS mode) */
if (strcmp(kiwitab[index].assw, "rowColumn24") == 0) {
 if (strcmp(kiwitab[index].widget, "rb_wlcmtd_gues7") == 0) {
   UxPutSensitive(UxFindSwidget("tf_guess2"), "true");
   UxPutSensitive(UxFindSwidget("guess_session_label"), "true");
   UxPutSensitive(UxFindSwidget("tg_coropt"), "true");
   }
 else {
   UxPutSensitive(UxFindSwidget("tf_guess2"), "false");
   UxPutSensitive(UxFindSwidget("guess_session_label"), "false");
   UxPutSensitive(UxFindSwidget("tg_coropt"), "false");
   }
}

/* Special Case: Extraction Method Radio-Button updates Search panel*/
if (strcmp(kiwitab[index].assw, "rowColumn27") == 0)
    UxPutText(UxFindSwidget("tf_thres12"),kiwitab[index].midcom);

/* Special Case: Rotation Method Radio-Button for pre-defined Orientations */
if (strcmp(kiwitab[index].widget, "toggleButton14") == 0) {
    strcat(command, UxGetLabelString(UxFindSwidget(UxGetMenuHistory(UxFindSwidget("mn_tol2")))));
    AppendDialogTextNoWait(command);
    }
if (strcmp(kiwitab[index].assw, "mn_tol2") == 0) {
    XmToggleButtonSetState(UxGetWidget(UxFindSwidget("toggleButton14")), TRUE, FALSE);
    XmToggleButtonSetState(UxGetWidget(UxFindSwidget("toggleButton15")), FALSE, FALSE);
    strcat(command, UxGetLabelString(UxFindSwidget(UxGetMenuHistory(UxFindSwidget("mn_tol2")))));
    AppendDialogTextNoWait(command);
}

return 0;
}

int StringToIndex(s)

char *s;

{
int index=0;
int found=0;

while (index < NB_KEY) {
if (strcmp(kiwitab[index].widget,s) == 0)  found = 1;
if ((int) strlen(kiwitab[index].assw) > 0)
   if (strcmp(kiwitab[index].assw,s) == 0)  found = 1;
if (found == 1) break;
else index++;
}

if (dbg > 1) printf("Found swidget %s at index %d\n",s,index);

return(index);

}

void SelectList(sw)
swidget sw;

{
   extern Widget   FileListWidget;
   extern char     DirSpecs[];
   extern swidget  FileListInterface;

   int strip = 1; /* Strips directories */
   IndexMemory = FindIndex(sw);

   strcpy(DirSpecs, kiwitab[IndexMemory].midcom);
   SetFileList(FileListWidget, strip, DirSpecs);
   UxPopupInterface(FileListInterface, exclusive_grab);
   
}

void ChoiceList(choice)
char *choice;

{
   extern swidget FileListInterface;

   if (IndexMemory != 0) {
       strcpy(CValue,"");
       UxPutText(UxFindSwidget(kiwitab[IndexMemory].assw), choice);
       UxPopdownInterface(FileListInterface);
       if (dbg >= 2) printf("Now call Widget Leave: %s\n",kiwitab[IndexMemory].assw);
       WidgetLeave(UxGetWidget(UxFindSwidget(kiwitab[IndexMemory].assw)));
       IndexMemory = 0;
  }
  else {
       if (dbg >= 2) printf("Ignored: Attempted to write %s at index %d\n",choice,IndexMemory);
       CallbackList(choice);
  }
}

