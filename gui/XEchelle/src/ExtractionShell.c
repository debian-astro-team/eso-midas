/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	ExtractionShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxSepG.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxExtractionShell;
	swidget	Uxform31;
	swidget	Uxlabel88;
	swidget	Uxtf_ywidth3;
	swidget	Uxlabel89;
	swidget	Uxlabel90;
	swidget	Uxtf_ystep3;
	swidget	Uxtf_width3;
	swidget	UxrowColumn27;
	swidget	Uxrb_seamtd_gaus3;
	swidget	Uxrb_seamtd_grav3;
	swidget	Uxrb_seamtd_maxi3;
	swidget	Uxlabel91;
	swidget	UxseparatorGadget1;
	swidget	UxseparatorGadget2;
	swidget	UxseparatorGadget3;
	swidget	UxseparatorGadget4;
	swidget	Uxtf_thres5;
	swidget	Uxform32;
	swidget	Uxpb_search_search7;
	swidget	UxpushButton5;
	swidget	Uxpb_search_plot13;
	swidget	Uxpb_search_plot18;
	swidget	Uxshelp_search5;
	swidget	Uxseparator26;
	swidget	Uxseparator27;
	swidget	UxseparatorGadget5;
	swidget	Uxlabel92;
	swidget	Uxtf_thres6;
	swidget	Uxlabel93;
	swidget	Uxpb_main_search19;
	swidget	Uxlabel94;
	swidget	Uxtf_thres7;
	swidget	Uxlabel95;
	swidget	Uxtf_ystep4;
	swidget	Uxpb_main_search20;
} _UxCExtractionShell;

#define ExtractionShell         UxExtractionShellContext->UxExtractionShell
#define form31                  UxExtractionShellContext->Uxform31
#define label88                 UxExtractionShellContext->Uxlabel88
#define tf_ywidth3              UxExtractionShellContext->Uxtf_ywidth3
#define label89                 UxExtractionShellContext->Uxlabel89
#define label90                 UxExtractionShellContext->Uxlabel90
#define tf_ystep3               UxExtractionShellContext->Uxtf_ystep3
#define tf_width3               UxExtractionShellContext->Uxtf_width3
#define rowColumn27             UxExtractionShellContext->UxrowColumn27
#define rb_seamtd_gaus3         UxExtractionShellContext->Uxrb_seamtd_gaus3
#define rb_seamtd_grav3         UxExtractionShellContext->Uxrb_seamtd_grav3
#define rb_seamtd_maxi3         UxExtractionShellContext->Uxrb_seamtd_maxi3
#define label91                 UxExtractionShellContext->Uxlabel91
#define separatorGadget1        UxExtractionShellContext->UxseparatorGadget1
#define separatorGadget2        UxExtractionShellContext->UxseparatorGadget2
#define separatorGadget3        UxExtractionShellContext->UxseparatorGadget3
#define separatorGadget4        UxExtractionShellContext->UxseparatorGadget4
#define tf_thres5               UxExtractionShellContext->Uxtf_thres5
#define form32                  UxExtractionShellContext->Uxform32
#define pb_search_search7       UxExtractionShellContext->Uxpb_search_search7
#define pushButton5             UxExtractionShellContext->UxpushButton5
#define pb_search_plot13        UxExtractionShellContext->Uxpb_search_plot13
#define pb_search_plot18        UxExtractionShellContext->Uxpb_search_plot18
#define shelp_search5           UxExtractionShellContext->Uxshelp_search5
#define separator26             UxExtractionShellContext->Uxseparator26
#define separator27             UxExtractionShellContext->Uxseparator27
#define separatorGadget5        UxExtractionShellContext->UxseparatorGadget5
#define label92                 UxExtractionShellContext->Uxlabel92
#define tf_thres6               UxExtractionShellContext->Uxtf_thres6
#define label93                 UxExtractionShellContext->Uxlabel93
#define pb_main_search19        UxExtractionShellContext->Uxpb_main_search19
#define label94                 UxExtractionShellContext->Uxlabel94
#define tf_thres7               UxExtractionShellContext->Uxtf_thres7
#define label95                 UxExtractionShellContext->Uxlabel95
#define tf_ystep4               UxExtractionShellContext->Uxtf_ystep4
#define pb_main_search20        UxExtractionShellContext->Uxpb_main_search20

static _UxCExtractionShell	*UxExtractionShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();
extern void  WriteKeyword(), GetExtendedHelp(), SelectList();


extern int RadioSet(), PopupList(), AppendDialogText();

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable18 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ExtractionShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxExtractionShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxExtractionShellContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	losingFocusCB_tf_ywidth3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_ystep3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_width3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_gaus3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxExtractionShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_gaus3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_grav3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxExtractionShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_grav3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_maxi3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxExtractionShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_maxi3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_search7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	activateCB_pushButton5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("ExtractionShell"));
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search19( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_ystep4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search20( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractionShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractionShellContext;
	UxExtractionShellContext = UxContext =
			(_UxCExtractionShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxExtractionShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ExtractionShell()
{
	UxPutTitle( ExtractionShell, "XEchelle: Extraction" );
	UxPutGeometry( ExtractionShell, "+10+60" );
	UxPutHeight( ExtractionShell, 410 );
	UxPutWidth( ExtractionShell, 510 );
	UxPutY( ExtractionShell, 506 );
	UxPutX( ExtractionShell, 520 );

	UxPutBackground( form31, WindowBackground );
	UxPutHeight( form31, 344 );
	UxPutWidth( form31, 500 );
	UxPutY( form31, 2 );
	UxPutX( form31, 4 );
	UxPutUnitType( form31, "pixels" );
	UxPutResizePolicy( form31, "resize_none" );

	UxPutForeground( label88, TextForeground );
	UxPutAlignment( label88, "alignment_beginning" );
	UxPutLabelString( label88, "Read-Out Noise:" );
	UxPutFontList( label88, TextFont );
	UxPutBackground( label88, LabelBackground );
	UxPutHeight( label88, 30 );
	UxPutWidth( label88, 170 );
	UxPutY( label88, 168 );
	UxPutX( label88, 22 );

	UxPutForeground( tf_ywidth3, TextForeground );
	UxPutHighlightOnEnter( tf_ywidth3, "true" );
	UxPutFontList( tf_ywidth3, TextFont );
	UxPutBackground( tf_ywidth3, TextBackground );
	UxPutHeight( tf_ywidth3, 34 );
	UxPutWidth( tf_ywidth3, 82 );
	UxPutY( tf_ywidth3, 166 );
	UxPutX( tf_ywidth3, 212 );

	UxPutForeground( label89, TextForeground );
	UxPutAlignment( label89, "alignment_beginning" );
	UxPutLabelString( label89, "Gain:" );
	UxPutFontList( label89, TextFont );
	UxPutBackground( label89, LabelBackground );
	UxPutHeight( label89, 30 );
	UxPutWidth( label89, 170 );
	UxPutY( label89, 204 );
	UxPutX( label89, 22 );

	UxPutForeground( label90, TextForeground );
	UxPutAlignment( label90, "alignment_beginning" );
	UxPutLabelString( label90, "Slit:" );
	UxPutFontList( label90, TextFont );
	UxPutBackground( label90, LabelBackground );
	UxPutHeight( label90, 30 );
	UxPutWidth( label90, 170 );
	UxPutY( label90, 92 );
	UxPutX( label90, 22 );

	UxPutForeground( tf_ystep3, TextForeground );
	UxPutHighlightOnEnter( tf_ystep3, "true" );
	UxPutFontList( tf_ystep3, TextFont );
	UxPutBackground( tf_ystep3, TextBackground );
	UxPutHeight( tf_ystep3, 34 );
	UxPutWidth( tf_ystep3, 82 );
	UxPutY( tf_ystep3, 200 );
	UxPutX( tf_ystep3, 212 );

	UxPutForeground( tf_width3, TextForeground );
	UxPutHighlightOnEnter( tf_width3, "true" );
	UxPutFontList( tf_width3, TextFont );
	UxPutBackground( tf_width3, TextBackground );
	UxPutHeight( tf_width3, 34 );
	UxPutWidth( tf_width3, 82 );
	UxPutY( tf_width3, 128 );
	UxPutX( tf_width3, 212 );

	UxPutIsAligned( rowColumn27, "true" );
	UxPutAdjustMargin( rowColumn27, "true" );
	UxPutAdjustLast( rowColumn27, "false" );
	UxPutEntryAlignment( rowColumn27, "alignment_beginning" );
	UxPutBorderWidth( rowColumn27, 0 );
	UxPutShadowThickness( rowColumn27, 0 );
	UxPutLabelString( rowColumn27, "" );
	UxPutEntryBorder( rowColumn27, 0 );
	UxPutBackground( rowColumn27, WindowBackground );
	UxPutRadioBehavior( rowColumn27, "true" );
	UxPutHeight( rowColumn27, 96 );
	UxPutWidth( rowColumn27, 106 );
	UxPutY( rowColumn27, 116 );
	UxPutX( rowColumn27, 336 );

	UxPutTranslations( rb_seamtd_gaus3, transTable18 );
	UxPutForeground( rb_seamtd_gaus3, TextForeground );
	UxPutIndicatorSize( rb_seamtd_gaus3, 16 );
	UxPutHighlightOnEnter( rb_seamtd_gaus3, "true" );
	UxPutSelectColor( rb_seamtd_gaus3, SelectColor );
	UxPutSet( rb_seamtd_gaus3, "true" );
	UxPutLabelString( rb_seamtd_gaus3, "Average" );
	UxPutFontList( rb_seamtd_gaus3, TextFont );
	UxPutBackground( rb_seamtd_gaus3, WindowBackground );
	UxPutHeight( rb_seamtd_gaus3, 30 );
	UxPutWidth( rb_seamtd_gaus3, 148 );
	UxPutY( rb_seamtd_gaus3, 12 );
	UxPutX( rb_seamtd_gaus3, 12 );

	UxPutTranslations( rb_seamtd_grav3, transTable18 );
	UxPutForeground( rb_seamtd_grav3, TextForeground );
	UxPutIndicatorSize( rb_seamtd_grav3, 16 );
	UxPutHighlightOnEnter( rb_seamtd_grav3, "true" );
	UxPutSelectColor( rb_seamtd_grav3, SelectColor );
	UxPutLabelString( rb_seamtd_grav3, "Linear" );
	UxPutFontList( rb_seamtd_grav3, TextFont );
	UxPutBackground( rb_seamtd_grav3, WindowBackground );
	UxPutHeight( rb_seamtd_grav3, 30 );
	UxPutWidth( rb_seamtd_grav3, 148 );
	UxPutY( rb_seamtd_grav3, 85 );
	UxPutX( rb_seamtd_grav3, 5 );

	UxPutTranslations( rb_seamtd_maxi3, transTable18 );
	UxPutForeground( rb_seamtd_maxi3, TextForeground );
	UxPutIndicatorSize( rb_seamtd_maxi3, 16 );
	UxPutHighlightOnEnter( rb_seamtd_maxi3, "true" );
	UxPutSelectColor( rb_seamtd_maxi3, SelectColor );
	UxPutLabelString( rb_seamtd_maxi3, "Optimal" );
	UxPutFontList( rb_seamtd_maxi3, TextFont );
	UxPutBackground( rb_seamtd_maxi3, WindowBackground );
	UxPutHeight( rb_seamtd_maxi3, 24 );
	UxPutWidth( rb_seamtd_maxi3, 96 );
	UxPutY( rb_seamtd_maxi3, 66 );
	UxPutX( rb_seamtd_maxi3, 4 );

	UxPutForeground( label91, TextForeground );
	UxPutAlignment( label91, "alignment_center" );
	UxPutLabelString( label91, "Method" );
	UxPutFontList( label91, TextFont );
	UxPutBackground( label91, LabelBackground );
	UxPutHeight( label91, 30 );
	UxPutWidth( label91, 106 );
	UxPutY( label91, 86 );
	UxPutX( label91, 340 );

	UxPutOrientation( separatorGadget1, "vertical" );
	UxPutHeight( separatorGadget1, 120 );
	UxPutWidth( separatorGadget1, 12 );
	UxPutY( separatorGadget1, 100 );
	UxPutX( separatorGadget1, 454 );

	UxPutOrientation( separatorGadget2, "vertical" );
	UxPutHeight( separatorGadget2, 120 );
	UxPutWidth( separatorGadget2, 12 );
	UxPutY( separatorGadget2, 100 );
	UxPutX( separatorGadget2, 318 );

	UxPutHeight( separatorGadget3, 10 );
	UxPutWidth( separatorGadget3, 16 );
	UxPutY( separatorGadget3, 98 );
	UxPutX( separatorGadget3, 322 );

	UxPutHeight( separatorGadget4, 10 );
	UxPutWidth( separatorGadget4, 14 );
	UxPutY( separatorGadget4, 98 );
	UxPutX( separatorGadget4, 446 );

	UxPutForeground( tf_thres5, TextForeground );
	UxPutHighlightOnEnter( tf_thres5, "true" );
	UxPutFontList( tf_thres5, TextFont );
	UxPutBackground( tf_thres5, TextBackground );
	UxPutHeight( tf_thres5, 34 );
	UxPutWidth( tf_thres5, 82 );
	UxPutY( tf_thres5, 90 );
	UxPutX( tf_thres5, 212 );

	UxPutBackground( form32, ButtonBackground );
	UxPutHeight( form32, 40 );
	UxPutWidth( form32, 490 );
	UxPutY( form32, 360 );
	UxPutX( form32, 14 );
	UxPutResizePolicy( form32, "resize_none" );

	UxPutLabelString( pb_search_search7, "Extract" );
	UxPutForeground( pb_search_search7, ApplyForeground );
	UxPutFontList( pb_search_search7, BoldTextFont );
	UxPutBackground( pb_search_search7, ButtonBackground );
	UxPutHeight( pb_search_search7, 30 );
	UxPutWidth( pb_search_search7, 86 );
	UxPutY( pb_search_search7, 4 );
	UxPutX( pb_search_search7, 8 );

	UxPutLabelString( pushButton5, "Cancel" );
	UxPutForeground( pushButton5, CancelForeground );
	UxPutFontList( pushButton5, BoldTextFont );
	UxPutBackground( pushButton5, ButtonBackground );
	UxPutHeight( pushButton5, 30 );
	UxPutWidth( pushButton5, 86 );
	UxPutY( pushButton5, 6 );
	UxPutX( pushButton5, 392 );

	UxPutLabelString( pb_search_plot13, "Plot Orders" );
	UxPutForeground( pb_search_plot13, ButtonForeground );
	UxPutFontList( pb_search_plot13, BoldTextFont );
	UxPutBackground( pb_search_plot13, ButtonBackground );
	UxPutHeight( pb_search_plot13, 30 );
	UxPutWidth( pb_search_plot13, 100 );
	UxPutY( pb_search_plot13, 4 );
	UxPutX( pb_search_plot13, 102 );

	UxPutLabelString( pb_search_plot18, "Help..." );
	UxPutForeground( pb_search_plot18, ButtonForeground );
	UxPutFontList( pb_search_plot18, BoldTextFont );
	UxPutBackground( pb_search_plot18, ButtonBackground );
	UxPutHeight( pb_search_plot18, 30 );
	UxPutWidth( pb_search_plot18, 86 );
	UxPutY( pb_search_plot18, 4 );
	UxPutX( pb_search_plot18, 304 );

	UxPutFontList( shelp_search5, TextFont );
	UxPutEditable( shelp_search5, "false" );
	UxPutCursorPositionVisible( shelp_search5, "false" );
	UxPutBackground( shelp_search5, SHelpBackground );
	UxPutHeight( shelp_search5, 50 );
	UxPutWidth( shelp_search5, 484 );
	UxPutY( shelp_search5, 300 );
	UxPutX( shelp_search5, 16 );

	UxPutBackground( separator26, WindowBackground );
	UxPutHeight( separator26, 10 );
	UxPutWidth( separator26, 492 );
	UxPutY( separator26, 290 );
	UxPutX( separator26, 14 );

	UxPutBackground( separator27, WindowBackground );
	UxPutHeight( separator27, 10 );
	UxPutWidth( separator27, 492 );
	UxPutY( separator27, 348 );
	UxPutX( separator27, 10 );

	UxPutHeight( separatorGadget5, 10 );
	UxPutWidth( separatorGadget5, 138 );
	UxPutY( separatorGadget5, 214 );
	UxPutX( separatorGadget5, 322 );

	UxPutForeground( label92, TextForeground );
	UxPutAlignment( label92, "alignment_beginning" );
	UxPutLabelString( label92, "Offset:" );
	UxPutFontList( label92, TextFont );
	UxPutBackground( label92, LabelBackground );
	UxPutHeight( label92, 30 );
	UxPutWidth( label92, 170 );
	UxPutY( label92, 128 );
	UxPutX( label92, 22 );

	UxPutForeground( tf_thres6, TextForeground );
	UxPutHighlightOnEnter( tf_thres6, "true" );
	UxPutFontList( tf_thres6, TextFont );
	UxPutBackground( tf_thres6, TextBackground );
	UxPutHeight( tf_thres6, 34 );
	UxPutWidth( tf_thres6, 212 );
	UxPutY( tf_thres6, 12 );
	UxPutX( tf_thres6, 208 );

	UxPutForeground( label93, TextForeground );
	UxPutAlignment( label93, "alignment_beginning" );
	UxPutLabelString( label93, "Input Frame:" );
	UxPutFontList( label93, TextFont );
	UxPutBackground( label93, LabelBackground );
	UxPutHeight( label93, 30 );
	UxPutWidth( label93, 170 );
	UxPutY( label93, 14 );
	UxPutX( label93, 20 );

	UxPutWidth( pb_main_search19, 46 );
	UxPutRecomputeSize( pb_main_search19, "true" );
	UxPutLabelString( pb_main_search19, "..." );
	UxPutForeground( pb_main_search19, ButtonForeground );
	UxPutFontList( pb_main_search19, BoldTextFont );
	UxPutBackground( pb_main_search19, ButtonBackground );
	UxPutHeight( pb_main_search19, 28 );
	UxPutY( pb_main_search19, 14 );
	UxPutX( pb_main_search19, 428 );

	UxPutForeground( label94, TextForeground );
	UxPutAlignment( label94, "alignment_beginning" );
	UxPutLabelString( label94, "Output Frame:" );
	UxPutFontList( label94, TextFont );
	UxPutBackground( label94, LabelBackground );
	UxPutHeight( label94, 30 );
	UxPutWidth( label94, 170 );
	UxPutY( label94, 56 );
	UxPutX( label94, 20 );

	UxPutForeground( tf_thres7, TextForeground );
	UxPutHighlightOnEnter( tf_thres7, "true" );
	UxPutFontList( tf_thres7, TextFont );
	UxPutBackground( tf_thres7, TextBackground );
	UxPutHeight( tf_thres7, 34 );
	UxPutWidth( tf_thres7, 212 );
	UxPutY( tf_thres7, 50 );
	UxPutX( tf_thres7, 210 );

	UxPutForeground( label95, TextForeground );
	UxPutAlignment( label95, "alignment_beginning" );
	UxPutLabelString( label95, "Threshold:" );
	UxPutFontList( label95, TextFont );
	UxPutBackground( label95, LabelBackground );
	UxPutHeight( label95, 30 );
	UxPutWidth( label95, 170 );
	UxPutY( label95, 242 );
	UxPutX( label95, 22 );

	UxPutForeground( tf_ystep4, TextForeground );
	UxPutHighlightOnEnter( tf_ystep4, "true" );
	UxPutFontList( tf_ystep4, TextFont );
	UxPutBackground( tf_ystep4, TextBackground );
	UxPutHeight( tf_ystep4, 34 );
	UxPutWidth( tf_ystep4, 82 );
	UxPutY( tf_ystep4, 240 );
	UxPutX( tf_ystep4, 212 );

	UxPutWidth( pb_main_search20, 46 );
	UxPutRecomputeSize( pb_main_search20, "true" );
	UxPutLabelString( pb_main_search20, "..." );
	UxPutForeground( pb_main_search20, ButtonForeground );
	UxPutFontList( pb_main_search20, BoldTextFont );
	UxPutBackground( pb_main_search20, ButtonBackground );
	UxPutHeight( pb_main_search20, 28 );
	UxPutY( pb_main_search20, 52 );
	UxPutX( pb_main_search20, 426 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ExtractionShell()
{
	/* Create the swidgets */

	ExtractionShell = UxCreateTransientShell( "ExtractionShell", NO_PARENT );
	UxPutContext( ExtractionShell, UxExtractionShellContext );

	form31 = UxCreateForm( "form31", ExtractionShell );
	label88 = UxCreateLabel( "label88", form31 );
	tf_ywidth3 = UxCreateTextField( "tf_ywidth3", form31 );
	label89 = UxCreateLabel( "label89", form31 );
	label90 = UxCreateLabel( "label90", form31 );
	tf_ystep3 = UxCreateTextField( "tf_ystep3", form31 );
	tf_width3 = UxCreateTextField( "tf_width3", form31 );
	rowColumn27 = UxCreateRowColumn( "rowColumn27", form31 );
	rb_seamtd_gaus3 = UxCreateToggleButton( "rb_seamtd_gaus3", rowColumn27 );
	rb_seamtd_grav3 = UxCreateToggleButton( "rb_seamtd_grav3", rowColumn27 );
	rb_seamtd_maxi3 = UxCreateToggleButton( "rb_seamtd_maxi3", rowColumn27 );
	label91 = UxCreateLabel( "label91", form31 );
	separatorGadget1 = UxCreateSeparatorGadget( "separatorGadget1", form31 );
	separatorGadget2 = UxCreateSeparatorGadget( "separatorGadget2", form31 );
	separatorGadget3 = UxCreateSeparatorGadget( "separatorGadget3", form31 );
	separatorGadget4 = UxCreateSeparatorGadget( "separatorGadget4", form31 );
	tf_thres5 = UxCreateTextField( "tf_thres5", form31 );
	form32 = UxCreateForm( "form32", form31 );
	pb_search_search7 = UxCreatePushButton( "pb_search_search7", form32 );
	pushButton5 = UxCreatePushButton( "pushButton5", form32 );
	pb_search_plot13 = UxCreatePushButton( "pb_search_plot13", form32 );
	pb_search_plot18 = UxCreatePushButton( "pb_search_plot18", form32 );
	shelp_search5 = UxCreateText( "shelp_search5", form31 );
	separator26 = UxCreateSeparator( "separator26", form31 );
	separator27 = UxCreateSeparator( "separator27", form31 );
	separatorGadget5 = UxCreateSeparatorGadget( "separatorGadget5", form31 );
	label92 = UxCreateLabel( "label92", form31 );
	tf_thres6 = UxCreateTextField( "tf_thres6", form31 );
	label93 = UxCreateLabel( "label93", form31 );
	pb_main_search19 = UxCreatePushButton( "pb_main_search19", form31 );
	label94 = UxCreateLabel( "label94", form31 );
	tf_thres7 = UxCreateTextField( "tf_thres7", form31 );
	label95 = UxCreateLabel( "label95", form31 );
	tf_ystep4 = UxCreateTextField( "tf_ystep4", form31 );
	pb_main_search20 = UxCreatePushButton( "pb_main_search20", form31 );

	_Uxinit_ExtractionShell();

	/* Create the X widgets */

	UxCreateWidget( ExtractionShell );
	UxCreateWidget( form31 );
	UxCreateWidget( label88 );
	UxCreateWidget( tf_ywidth3 );
	UxCreateWidget( label89 );
	UxCreateWidget( label90 );
	UxCreateWidget( tf_ystep3 );
	UxCreateWidget( tf_width3 );
	UxCreateWidget( rowColumn27 );
	UxCreateWidget( rb_seamtd_gaus3 );
	UxCreateWidget( rb_seamtd_grav3 );
	UxCreateWidget( rb_seamtd_maxi3 );
	UxCreateWidget( label91 );
	UxCreateWidget( separatorGadget1 );
	UxCreateWidget( separatorGadget2 );
	UxCreateWidget( separatorGadget3 );
	UxCreateWidget( separatorGadget4 );
	UxCreateWidget( tf_thres5 );
	UxCreateWidget( form32 );
	UxCreateWidget( pb_search_search7 );
	UxCreateWidget( pushButton5 );
	UxCreateWidget( pb_search_plot13 );
	UxCreateWidget( pb_search_plot18 );
	UxCreateWidget( shelp_search5 );
	UxCreateWidget( separator26 );
	UxCreateWidget( separator27 );
	UxCreateWidget( separatorGadget5 );
	UxCreateWidget( label92 );
	UxCreateWidget( tf_thres6 );
	UxCreateWidget( label93 );
	UxCreateWidget( pb_main_search19 );
	UxCreateWidget( label94 );
	UxCreateWidget( tf_thres7 );
	UxCreateWidget( label95 );
	UxCreateWidget( tf_ystep4 );
	UxCreateWidget( pb_main_search20 );

	UxAddCallback( tf_ywidth3, XmNlosingFocusCallback,
			losingFocusCB_tf_ywidth3,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( tf_ystep3, XmNlosingFocusCallback,
			losingFocusCB_tf_ystep3,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( tf_width3, XmNlosingFocusCallback,
			losingFocusCB_tf_width3,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( rb_seamtd_gaus3, XmNarmCallback,
			armCB_rb_seamtd_gaus3,
			(XtPointer) UxExtractionShellContext );
	UxAddCallback( rb_seamtd_gaus3, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_gaus3,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( rb_seamtd_grav3, XmNarmCallback,
			armCB_rb_seamtd_grav3,
			(XtPointer) UxExtractionShellContext );
	UxAddCallback( rb_seamtd_grav3, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_grav3,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( rb_seamtd_maxi3, XmNarmCallback,
			armCB_rb_seamtd_maxi3,
			(XtPointer) UxExtractionShellContext );
	UxAddCallback( rb_seamtd_maxi3, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_maxi3,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( tf_thres5, XmNlosingFocusCallback,
			losingFocusCB_tf_thres5,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( pb_search_search7, XmNactivateCallback,
			activateCB_pb_search_search7,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( pushButton5, XmNactivateCallback,
			activateCB_pushButton5,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( pb_search_plot13, XmNactivateCallback,
			activateCB_pb_search_plot13,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( pb_search_plot18, XmNactivateCallback,
			activateCB_pb_search_plot18,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( tf_thres6, XmNlosingFocusCallback,
			losingFocusCB_tf_thres6,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( pb_main_search19, XmNactivateCallback,
			activateCB_pb_main_search19,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( tf_thres7, XmNlosingFocusCallback,
			losingFocusCB_tf_thres7,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( tf_ystep4, XmNlosingFocusCallback,
			losingFocusCB_tf_ystep4,
			(XtPointer) UxExtractionShellContext );

	UxAddCallback( pb_main_search20, XmNactivateCallback,
			activateCB_pb_main_search20,
			(XtPointer) UxExtractionShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ExtractionShell );

	return ( ExtractionShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ExtractionShell()
{
	swidget                 rtrn;
	_UxCExtractionShell     *UxContext;

	UxExtractionShellContext = UxContext =
		(_UxCExtractionShell *) UxMalloc( sizeof(_UxCExtractionShell) );

	rtrn = _Uxbuild_ExtractionShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ExtractionShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ExtractionShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

