/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	MergeShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTranSh.h"

#include "proto_xech.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxMergeShell;
	swidget	Uxform14;
	swidget	Uxform15;
	swidget	Uxpb_search_search13;
	swidget	UxpushButton10;
	swidget	Uxpb_search_plot25;
	swidget	Uxpb_search_plot28;
	swidget	Uxpb_search_plot29;
	swidget	Uxshelp_search9;
	swidget	Uxseparator10;
	swidget	Uxseparator11;
	swidget	Uxlabel38;
	swidget	Uxtf_thres8;
	swidget	Uxpb_main_search30;
	swidget	Uxpb_main_search31;
	swidget	Uxtf_thres11;
	swidget	Uxlabel42;
	swidget	Uxlabel43;
	swidget	UxrowColumn6;
	swidget	Uxrb_seamtd_gaus6;
	swidget	Uxrb_seamtd_grav6;
	swidget	Uxlabel45;
	swidget	Uxlabel46;
	swidget	Uxtf_alpha31;
	swidget	Uxlabel47;
	swidget	Uxtf_alpha40;
	swidget	Uxlabel48;
	swidget	Uxtf_alpha42;
} _UxCMergeShell;

#define MergeShell              UxMergeShellContext->UxMergeShell
#define form14                  UxMergeShellContext->Uxform14
#define form15                  UxMergeShellContext->Uxform15
#define pb_search_search13      UxMergeShellContext->Uxpb_search_search13
#define pushButton10            UxMergeShellContext->UxpushButton10
#define pb_search_plot25        UxMergeShellContext->Uxpb_search_plot25
#define pb_search_plot28        UxMergeShellContext->Uxpb_search_plot28
#define pb_search_plot29        UxMergeShellContext->Uxpb_search_plot29
#define shelp_search9           UxMergeShellContext->Uxshelp_search9
#define separator10             UxMergeShellContext->Uxseparator10
#define separator11             UxMergeShellContext->Uxseparator11
#define label38                 UxMergeShellContext->Uxlabel38
#define tf_thres8               UxMergeShellContext->Uxtf_thres8
#define pb_main_search30        UxMergeShellContext->Uxpb_main_search30
#define pb_main_search31        UxMergeShellContext->Uxpb_main_search31
#define tf_thres11              UxMergeShellContext->Uxtf_thres11
#define label42                 UxMergeShellContext->Uxlabel42
#define label43                 UxMergeShellContext->Uxlabel43
#define rowColumn6              UxMergeShellContext->UxrowColumn6
#define rb_seamtd_gaus6         UxMergeShellContext->Uxrb_seamtd_gaus6
#define rb_seamtd_grav6         UxMergeShellContext->Uxrb_seamtd_grav6
#define label45                 UxMergeShellContext->Uxlabel45
#define label46                 UxMergeShellContext->Uxlabel46
#define tf_alpha31              UxMergeShellContext->Uxtf_alpha31
#define label47                 UxMergeShellContext->Uxlabel47
#define tf_alpha40              UxMergeShellContext->Uxtf_alpha40
#define label48                 UxMergeShellContext->Uxlabel48
#define tf_alpha42              UxMergeShellContext->Uxtf_alpha42

static _UxCMergeShell	*UxMergeShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();
extern void  WriteKeyword(), GetExtendedHelp(), SelectList();


extern int RadioSet(), PopupList(), AppendDialogText();

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable31 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_MergeShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	DisplayShortHelp(UxWidget);
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	/*
	UxPutText(UxFindSwidget("shelp_main"), "");
	UxPutText(UxFindSwidget("shelp_search"), "");
	UxPutText(UxFindSwidget("shelp_calib"), "");
	UxPutText(UxFindSwidget("shelp_rebin"), "");
	UxPutText(UxFindSwidget("shelp_extract"), "");
	UxPutText(UxFindSwidget("shelp_flux"), "");
	*/
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	action_UpdateDirectory( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	
	 char node[80];
	
	 /* if (XmTextGetSelection(UxWidget) == NULL) return; */
	 strcpy(node,
	     XmTextGetSelection(UxWidget));
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	
	node[strlen(node)-1] = '\0';
	WGet_all_dirs(node);
	WChange_Midas_dir();
	WGet_all_files();
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxMergeShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxMergeShellContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxMergeShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_search_search13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	 MidasCommand(UxWidget);
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	activateCB_pushButton10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("MergeShell"));
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot25( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot28( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	 MidasCommand(UxWidget);
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot29( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search30( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search31( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_gaus6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxMergeShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_gaus6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_grav6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxMergeShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_grav6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha31( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha40( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxMergeShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha42( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCMergeShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxMergeShellContext;
	UxMergeShellContext = UxContext =
			(_UxCMergeShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxMergeShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_MergeShell()
{
	UxPutTitle( MergeShell, "XEchelle: Orders Merging" );
	UxPutGeometry( MergeShell, "+10+60" );
	UxPutHeight( MergeShell, 330 );
	UxPutWidth( MergeShell, 500 );
	UxPutY( MergeShell, 514 );
	UxPutX( MergeShell, 454 );

	UxPutBackground( form14, WindowBackground );
	UxPutHeight( form14, 344 );
	UxPutWidth( form14, 500 );
	UxPutY( form14, 2 );
	UxPutX( form14, 4 );
	UxPutUnitType( form14, "pixels" );
	UxPutResizePolicy( form14, "resize_none" );

	UxPutBackground( form15, ButtonBackground );
	UxPutHeight( form15, 40 );
	UxPutWidth( form15, 490 );
	UxPutY( form15, 282 );
	UxPutX( form15, 2 );
	UxPutResizePolicy( form15, "resize_none" );

	UxPutLabelString( pb_search_search13, "Merge" );
	UxPutForeground( pb_search_search13, ApplyForeground );
	UxPutFontList( pb_search_search13, BoldTextFont );
	UxPutBackground( pb_search_search13, ButtonBackground );
	UxPutHeight( pb_search_search13, 30 );
	UxPutWidth( pb_search_search13, 86 );
	UxPutY( pb_search_search13, 4 );
	UxPutX( pb_search_search13, 2 );

	UxPutLabelString( pushButton10, "Cancel" );
	UxPutForeground( pushButton10, CancelForeground );
	UxPutFontList( pushButton10, BoldTextFont );
	UxPutBackground( pushButton10, ButtonBackground );
	UxPutHeight( pushButton10, 30 );
	UxPutWidth( pushButton10, 86 );
	UxPutY( pushButton10, 4 );
	UxPutX( pushButton10, 398 );

	UxPutLabelString( pb_search_plot25, "Plot Overlaps" );
	UxPutForeground( pb_search_plot25, ButtonForeground );
	UxPutFontList( pb_search_plot25, BoldTextFont );
	UxPutBackground( pb_search_plot25, ButtonBackground );
	UxPutHeight( pb_search_plot25, 30 );
	UxPutWidth( pb_search_plot25, 112 );
	UxPutY( pb_search_plot25, 4 );
	UxPutX( pb_search_plot25, 90 );

	UxPutLabelString( pb_search_plot28, "Plot Spectrum" );
	UxPutForeground( pb_search_plot28, ButtonForeground );
	UxPutFontList( pb_search_plot28, BoldTextFont );
	UxPutBackground( pb_search_plot28, ButtonBackground );
	UxPutHeight( pb_search_plot28, 30 );
	UxPutWidth( pb_search_plot28, 118 );
	UxPutY( pb_search_plot28, 4 );
	UxPutX( pb_search_plot28, 202 );

	UxPutLabelString( pb_search_plot29, "Help" );
	UxPutForeground( pb_search_plot29, ButtonForeground );
	UxPutFontList( pb_search_plot29, BoldTextFont );
	UxPutBackground( pb_search_plot29, ButtonBackground );
	UxPutHeight( pb_search_plot29, 30 );
	UxPutWidth( pb_search_plot29, 74 );
	UxPutY( pb_search_plot29, 4 );
	UxPutX( pb_search_plot29, 324 );

	UxPutFontList( shelp_search9, TextFont );
	UxPutEditable( shelp_search9, "false" );
	UxPutCursorPositionVisible( shelp_search9, "false" );
	UxPutBackground( shelp_search9, SHelpBackground );
	UxPutHeight( shelp_search9, 50 );
	UxPutWidth( shelp_search9, 484 );
	UxPutY( shelp_search9, 222 );
	UxPutX( shelp_search9, 4 );

	UxPutBackground( separator10, WindowBackground );
	UxPutHeight( separator10, 10 );
	UxPutWidth( separator10, 492 );
	UxPutY( separator10, 212 );
	UxPutX( separator10, 2 );

	UxPutBackground( separator11, WindowBackground );
	UxPutHeight( separator11, 10 );
	UxPutWidth( separator11, 492 );
	UxPutY( separator11, 270 );
	UxPutX( separator11, 0 );

	UxPutForeground( label38, TextForeground );
	UxPutAlignment( label38, "alignment_beginning" );
	UxPutLabelString( label38, "Input Frame:" );
	UxPutFontList( label38, TextFont );
	UxPutBackground( label38, LabelBackground );
	UxPutHeight( label38, 30 );
	UxPutWidth( label38, 130 );
	UxPutY( label38, 14 );
	UxPutX( label38, 20 );

	UxPutForeground( tf_thres8, TextForeground );
	UxPutHighlightOnEnter( tf_thres8, "true" );
	UxPutFontList( tf_thres8, TextFont );
	UxPutBackground( tf_thres8, TextBackground );
	UxPutHeight( tf_thres8, 34 );
	UxPutWidth( tf_thres8, 210 );
	UxPutY( tf_thres8, 14 );
	UxPutX( tf_thres8, 170 );

	UxPutWidth( pb_main_search30, 46 );
	UxPutRecomputeSize( pb_main_search30, "true" );
	UxPutLabelString( pb_main_search30, "..." );
	UxPutForeground( pb_main_search30, ButtonForeground );
	UxPutFontList( pb_main_search30, BoldTextFont );
	UxPutBackground( pb_main_search30, ButtonBackground );
	UxPutHeight( pb_main_search30, 28 );
	UxPutY( pb_main_search30, 14 );
	UxPutX( pb_main_search30, 428 );

	UxPutWidth( pb_main_search31, 46 );
	UxPutRecomputeSize( pb_main_search31, "true" );
	UxPutLabelString( pb_main_search31, "..." );
	UxPutForeground( pb_main_search31, ButtonForeground );
	UxPutFontList( pb_main_search31, BoldTextFont );
	UxPutBackground( pb_main_search31, ButtonBackground );
	UxPutHeight( pb_main_search31, 28 );
	UxPutY( pb_main_search31, 54 );
	UxPutX( pb_main_search31, 428 );

	UxPutForeground( tf_thres11, TextForeground );
	UxPutHighlightOnEnter( tf_thres11, "true" );
	UxPutFontList( tf_thres11, TextFont );
	UxPutBackground( tf_thres11, TextBackground );
	UxPutHeight( tf_thres11, 34 );
	UxPutWidth( tf_thres11, 212 );
	UxPutY( tf_thres11, 52 );
	UxPutX( tf_thres11, 168 );

	UxPutForeground( label42, TextForeground );
	UxPutAlignment( label42, "alignment_beginning" );
	UxPutLabelString( label42, "Output Frame:" );
	UxPutFontList( label42, TextFont );
	UxPutBackground( label42, LabelBackground );
	UxPutHeight( label42, 30 );
	UxPutWidth( label42, 120 );
	UxPutY( label42, 56 );
	UxPutX( label42, 20 );

	UxPutForeground( label43, TextForeground );
	UxPutAlignment( label43, "alignment_beginning" );
	UxPutLabelString( label43, "Method:" );
	UxPutFontList( label43, TextFont );
	UxPutBackground( label43, LabelBackground );
	UxPutHeight( label43, 30 );
	UxPutWidth( label43, 68 );
	UxPutY( label43, 94 );
	UxPutX( label43, 22 );

	UxPutResizeWidth( rowColumn6, "true" );
	UxPutOrientation( rowColumn6, "horizontal" );
	UxPutIsAligned( rowColumn6, "true" );
	UxPutAdjustMargin( rowColumn6, "true" );
	UxPutAdjustLast( rowColumn6, "false" );
	UxPutEntryAlignment( rowColumn6, "alignment_beginning" );
	UxPutBorderWidth( rowColumn6, 0 );
	UxPutShadowThickness( rowColumn6, 0 );
	UxPutLabelString( rowColumn6, "" );
	UxPutEntryBorder( rowColumn6, 0 );
	UxPutBackground( rowColumn6, WindowBackground );
	UxPutRadioBehavior( rowColumn6, "true" );
	UxPutHeight( rowColumn6, 34 );
	UxPutWidth( rowColumn6, 261 );
	UxPutY( rowColumn6, 97 );
	UxPutX( rowColumn6, 134 );

	UxPutTranslations( rb_seamtd_gaus6, transTable31 );
	UxPutForeground( rb_seamtd_gaus6, TextForeground );
	UxPutIndicatorSize( rb_seamtd_gaus6, 16 );
	UxPutHighlightOnEnter( rb_seamtd_gaus6, "true" );
	UxPutSelectColor( rb_seamtd_gaus6, SelectColor );
	UxPutSet( rb_seamtd_gaus6, "true" );
	UxPutLabelString( rb_seamtd_gaus6, "Average Overlaps" );
	UxPutFontList( rb_seamtd_gaus6, TextFont );
	UxPutBackground( rb_seamtd_gaus6, WindowBackground );
	UxPutHeight( rb_seamtd_gaus6, 28 );
	UxPutWidth( rb_seamtd_gaus6, 84 );
	UxPutY( rb_seamtd_gaus6, 3 );
	UxPutX( rb_seamtd_gaus6, 2 );

	UxPutTranslations( rb_seamtd_grav6, transTable31 );
	UxPutForeground( rb_seamtd_grav6, TextForeground );
	UxPutIndicatorSize( rb_seamtd_grav6, 16 );
	UxPutHighlightOnEnter( rb_seamtd_grav6, "true" );
	UxPutSelectColor( rb_seamtd_grav6, SelectColor );
	UxPutLabelString( rb_seamtd_grav6, "No Append" );
	UxPutFontList( rb_seamtd_grav6, TextFont );
	UxPutBackground( rb_seamtd_grav6, WindowBackground );
	UxPutHeight( rb_seamtd_grav6, 28 );
	UxPutWidth( rb_seamtd_grav6, 84 );
	UxPutY( rb_seamtd_grav6, 4 );
	UxPutX( rb_seamtd_grav6, 114 );

	UxPutForeground( label45, TextForeground );
	UxPutAlignment( label45, "alignment_beginning" );
	UxPutLabelString( label45, "Order Range" );
	UxPutFontList( label45, TextFont );
	UxPutBackground( label45, LabelBackground );
	UxPutHeight( label45, 30 );
	UxPutWidth( label45, 94 );
	UxPutY( label45, 170 );
	UxPutX( label45, 24 );

	UxPutForeground( label46, TextForeground );
	UxPutAlignment( label46, "alignment_beginning" );
	UxPutLabelString( label46, "Min=" );
	UxPutFontList( label46, TextFont );
	UxPutBackground( label46, LabelBackground );
	UxPutHeight( label46, 30 );
	UxPutWidth( label46, 40 );
	UxPutY( label46, 170 );
	UxPutX( label46, 140 );

	UxPutForeground( tf_alpha31, TextForeground );
	UxPutHighlightOnEnter( tf_alpha31, "true" );
	UxPutFontList( tf_alpha31, TextFont );
	UxPutBackground( tf_alpha31, TextBackground );
	UxPutHeight( tf_alpha31, 32 );
	UxPutWidth( tf_alpha31, 96 );
	UxPutY( tf_alpha31, 168 );
	UxPutX( tf_alpha31, 188 );

	UxPutForeground( label47, TextForeground );
	UxPutAlignment( label47, "alignment_beginning" );
	UxPutLabelString( label47, "Max=" );
	UxPutFontList( label47, TextFont );
	UxPutBackground( label47, LabelBackground );
	UxPutHeight( label47, 30 );
	UxPutWidth( label47, 50 );
	UxPutY( label47, 168 );
	UxPutX( label47, 302 );

	UxPutForeground( tf_alpha40, TextForeground );
	UxPutHighlightOnEnter( tf_alpha40, "true" );
	UxPutFontList( tf_alpha40, TextFont );
	UxPutBackground( tf_alpha40, TextBackground );
	UxPutHeight( tf_alpha40, 32 );
	UxPutWidth( tf_alpha40, 96 );
	UxPutY( tf_alpha40, 166 );
	UxPutX( tf_alpha40, 360 );

	UxPutForeground( label48, TextForeground );
	UxPutAlignment( label48, "alignment_beginning" );
	UxPutLabelString( label48, "Ignored Interval:" );
	UxPutFontList( label48, TextFont );
	UxPutBackground( label48, LabelBackground );
	UxPutHeight( label48, 30 );
	UxPutWidth( label48, 136 );
	UxPutY( label48, 134 );
	UxPutX( label48, 16 );

	UxPutForeground( tf_alpha42, TextForeground );
	UxPutHighlightOnEnter( tf_alpha42, "true" );
	UxPutFontList( tf_alpha42, TextFont );
	UxPutBackground( tf_alpha42, TextBackground );
	UxPutHeight( tf_alpha42, 32 );
	UxPutWidth( tf_alpha42, 140 );
	UxPutY( tf_alpha42, 134 );
	UxPutX( tf_alpha42, 184 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_MergeShell()
{
	/* Create the swidgets */

	MergeShell = UxCreateTransientShell( "MergeShell", NO_PARENT );
	UxPutContext( MergeShell, UxMergeShellContext );

	form14 = UxCreateForm( "form14", MergeShell );
	form15 = UxCreateForm( "form15", form14 );
	pb_search_search13 = UxCreatePushButton( "pb_search_search13", form15 );
	pushButton10 = UxCreatePushButton( "pushButton10", form15 );
	pb_search_plot25 = UxCreatePushButton( "pb_search_plot25", form15 );
	pb_search_plot28 = UxCreatePushButton( "pb_search_plot28", form15 );
	pb_search_plot29 = UxCreatePushButton( "pb_search_plot29", form15 );
	shelp_search9 = UxCreateText( "shelp_search9", form14 );
	separator10 = UxCreateSeparator( "separator10", form14 );
	separator11 = UxCreateSeparator( "separator11", form14 );
	label38 = UxCreateLabel( "label38", form14 );
	tf_thres8 = UxCreateTextField( "tf_thres8", form14 );
	pb_main_search30 = UxCreatePushButton( "pb_main_search30", form14 );
	pb_main_search31 = UxCreatePushButton( "pb_main_search31", form14 );
	tf_thres11 = UxCreateTextField( "tf_thres11", form14 );
	label42 = UxCreateLabel( "label42", form14 );
	label43 = UxCreateLabel( "label43", form14 );
	rowColumn6 = UxCreateRowColumn( "rowColumn6", form14 );
	rb_seamtd_gaus6 = UxCreateToggleButton( "rb_seamtd_gaus6", rowColumn6 );
	rb_seamtd_grav6 = UxCreateToggleButton( "rb_seamtd_grav6", rowColumn6 );
	label45 = UxCreateLabel( "label45", form14 );
	label46 = UxCreateLabel( "label46", form14 );
	tf_alpha31 = UxCreateTextField( "tf_alpha31", form14 );
	label47 = UxCreateLabel( "label47", form14 );
	tf_alpha40 = UxCreateTextField( "tf_alpha40", form14 );
	label48 = UxCreateLabel( "label48", form14 );
	tf_alpha42 = UxCreateTextField( "tf_alpha42", form14 );

	_Uxinit_MergeShell();

	/* Create the X widgets */

	UxCreateWidget( MergeShell );
	UxCreateWidget( form14 );
	UxCreateWidget( form15 );
	UxCreateWidget( pb_search_search13 );
	UxCreateWidget( pushButton10 );
	UxCreateWidget( pb_search_plot25 );
	UxCreateWidget( pb_search_plot28 );
	UxCreateWidget( pb_search_plot29 );
	UxCreateWidget( shelp_search9 );
	UxCreateWidget( separator10 );
	UxCreateWidget( separator11 );
	UxCreateWidget( label38 );
	UxCreateWidget( tf_thres8 );
	UxCreateWidget( pb_main_search30 );
	UxCreateWidget( pb_main_search31 );
	UxCreateWidget( tf_thres11 );
	UxCreateWidget( label42 );
	UxCreateWidget( label43 );
	UxCreateWidget( rowColumn6 );
	UxCreateWidget( rb_seamtd_gaus6 );
	UxCreateWidget( rb_seamtd_grav6 );
	UxCreateWidget( label45 );
	UxCreateWidget( label46 );
	UxCreateWidget( tf_alpha31 );
	UxCreateWidget( label47 );
	UxCreateWidget( tf_alpha40 );
	UxCreateWidget( label48 );
	UxCreateWidget( tf_alpha42 );

	UxAddCallback( pb_search_search13, XmNactivateCallback,
			activateCB_pb_search_search13,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( pushButton10, XmNactivateCallback,
			activateCB_pushButton10,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( pb_search_plot25, XmNactivateCallback,
			activateCB_pb_search_plot25,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( pb_search_plot28, XmNactivateCallback,
			activateCB_pb_search_plot28,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( pb_search_plot29, XmNactivateCallback,
			activateCB_pb_search_plot29,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( tf_thres8, XmNlosingFocusCallback,
			losingFocusCB_tf_thres8,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( pb_main_search30, XmNactivateCallback,
			activateCB_pb_main_search30,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( pb_main_search31, XmNactivateCallback,
			activateCB_pb_main_search31,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( tf_thres11, XmNlosingFocusCallback,
			losingFocusCB_tf_thres11,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( rb_seamtd_gaus6, XmNarmCallback,
			armCB_rb_seamtd_gaus6,
			(XtPointer) UxMergeShellContext );
	UxAddCallback( rb_seamtd_gaus6, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_gaus6,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( rb_seamtd_grav6, XmNarmCallback,
			armCB_rb_seamtd_grav6,
			(XtPointer) UxMergeShellContext );
	UxAddCallback( rb_seamtd_grav6, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_grav6,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( tf_alpha31, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha31,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( tf_alpha40, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha40,
			(XtPointer) UxMergeShellContext );

	UxAddCallback( tf_alpha42, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha42,
			(XtPointer) UxMergeShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( MergeShell );

	return ( MergeShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_MergeShell()
{
	swidget                 rtrn;
	_UxCMergeShell          *UxContext;

	UxMergeShellContext = UxContext =
		(_UxCMergeShell *) UxMalloc( sizeof(_UxCMergeShell) );

	rtrn = _Uxbuild_MergeShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_MergeShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "WriteHelp", action_WriteHelp },
				{ "ClearHelp", action_ClearHelp },
				{ "UpdateDirectory", action_UpdateDirectory },
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_MergeShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

