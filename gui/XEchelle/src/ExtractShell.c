/* @(#)ExtractShell.c	19.1 (ES0-DMD) 02/25/03 13:45:55 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	ExtractShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTextF.h"
#include "UxSepG.h"
#include "UxLabel.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxExtractShell;
	swidget	Uxform14;
	swidget	Uxform15;
	swidget	Uxpb_ext_sky;
	swidget	UxpushButton28;
	swidget	Uxpb_ext_object;
	swidget	Uxpb_ext_fit;
	swidget	Uxpb_ext_weight;
	swidget	Uxpb_ext_average;
	swidget	Uxshelp_extract;
	swidget	Uxseparator10;
	swidget	Uxseparator11;
	swidget	UxrowColumn6;
	swidget	Uxrb_extmtd_line;
	swidget	Uxrb_extmtd_aver;
	swidget	Uxlabel47;
	swidget	UxseparatorGadget26;
	swidget	UxseparatorGadget27;
	swidget	UxseparatorGadget28;
	swidget	UxrowColumn7;
	swidget	Uxrb_skymod_0;
	swidget	Uxrb_skymod_1;
	swidget	Uxlabel48;
	swidget	UxseparatorGadget29;
	swidget	UxseparatorGadget30;
	swidget	UxseparatorGadget31;
	swidget	UxseparatorGadget32;
	swidget	UxseparatorGadget33;
	swidget	UxseparatorGadget34;
	swidget	UxseparatorGadget35;
	swidget	Uxlabel49;
	swidget	Uxlabel50;
	swidget	Uxtf_lowsky2;
	swidget	Uxtf_sky;
	swidget	Uxlabel38;
	swidget	Uxlabel42;
	swidget	Uxtf_order;
	swidget	Uxtf_skyord;
	swidget	Uxlabel43;
	swidget	Uxlabel45;
	swidget	Uxtf_ron;
	swidget	Uxtf_niter;
	swidget	Uxlabel46;
	swidget	Uxlabel51;
	swidget	Uxtf_gain;
	swidget	Uxlabel52;
	swidget	Uxtf_radius;
	swidget	Uxtf_sigma;
	swidget	Uxlabel53;
	swidget	Uxtf_lowsky1;
	swidget	Uxtf_uppsky1;
	swidget	Uxtf_uppsky2;
	swidget	Uxtf_object1;
	swidget	Uxtf_object2;
} _UxCExtractShell;

#define ExtractShell            UxExtractShellContext->UxExtractShell
#define form14                  UxExtractShellContext->Uxform14
#define form15                  UxExtractShellContext->Uxform15
#define pb_ext_sky              UxExtractShellContext->Uxpb_ext_sky
#define pushButton28            UxExtractShellContext->UxpushButton28
#define pb_ext_object           UxExtractShellContext->Uxpb_ext_object
#define pb_ext_fit              UxExtractShellContext->Uxpb_ext_fit
#define pb_ext_weight           UxExtractShellContext->Uxpb_ext_weight
#define pb_ext_average          UxExtractShellContext->Uxpb_ext_average
#define shelp_extract           UxExtractShellContext->Uxshelp_extract
#define separator10             UxExtractShellContext->Uxseparator10
#define separator11             UxExtractShellContext->Uxseparator11
#define rowColumn6              UxExtractShellContext->UxrowColumn6
#define rb_extmtd_line          UxExtractShellContext->Uxrb_extmtd_line
#define rb_extmtd_aver          UxExtractShellContext->Uxrb_extmtd_aver
#define label47                 UxExtractShellContext->Uxlabel47
#define separatorGadget26       UxExtractShellContext->UxseparatorGadget26
#define separatorGadget27       UxExtractShellContext->UxseparatorGadget27
#define separatorGadget28       UxExtractShellContext->UxseparatorGadget28
#define rowColumn7              UxExtractShellContext->UxrowColumn7
#define rb_skymod_0             UxExtractShellContext->Uxrb_skymod_0
#define rb_skymod_1             UxExtractShellContext->Uxrb_skymod_1
#define label48                 UxExtractShellContext->Uxlabel48
#define separatorGadget29       UxExtractShellContext->UxseparatorGadget29
#define separatorGadget30       UxExtractShellContext->UxseparatorGadget30
#define separatorGadget31       UxExtractShellContext->UxseparatorGadget31
#define separatorGadget32       UxExtractShellContext->UxseparatorGadget32
#define separatorGadget33       UxExtractShellContext->UxseparatorGadget33
#define separatorGadget34       UxExtractShellContext->UxseparatorGadget34
#define separatorGadget35       UxExtractShellContext->UxseparatorGadget35
#define label49                 UxExtractShellContext->Uxlabel49
#define label50                 UxExtractShellContext->Uxlabel50
#define tf_lowsky2              UxExtractShellContext->Uxtf_lowsky2
#define tf_sky                  UxExtractShellContext->Uxtf_sky
#define label38                 UxExtractShellContext->Uxlabel38
#define label42                 UxExtractShellContext->Uxlabel42
#define tf_order                UxExtractShellContext->Uxtf_order
#define tf_skyord               UxExtractShellContext->Uxtf_skyord
#define label43                 UxExtractShellContext->Uxlabel43
#define label45                 UxExtractShellContext->Uxlabel45
#define tf_ron                  UxExtractShellContext->Uxtf_ron
#define tf_niter                UxExtractShellContext->Uxtf_niter
#define label46                 UxExtractShellContext->Uxlabel46
#define label51                 UxExtractShellContext->Uxlabel51
#define tf_gain                 UxExtractShellContext->Uxtf_gain
#define label52                 UxExtractShellContext->Uxlabel52
#define tf_radius               UxExtractShellContext->Uxtf_radius
#define tf_sigma                UxExtractShellContext->Uxtf_sigma
#define label53                 UxExtractShellContext->Uxlabel53
#define tf_lowsky1              UxExtractShellContext->Uxtf_lowsky1
#define tf_uppsky1              UxExtractShellContext->Uxtf_uppsky1
#define tf_uppsky2              UxExtractShellContext->Uxtf_uppsky2
#define tf_object1              UxExtractShellContext->Uxtf_object1
#define tf_object2              UxExtractShellContext->Uxtf_object2

static _UxCExtractShell	*UxExtractShellContext;


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*SelectFileExtract = "#override\n\
<Btn3Down>:FileSelectACT()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ExtractShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxExtractShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_ext_sky( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char command[128], s[10];
	int sky[4];
	extern int Lowsky[], Uppsky[];
	
	sprintf(command, "%s 4", C_EXTR_GCOOR);
	AppendDialogText(command);
	
	if ( !GetCoords(sky, 4) )
	    return;
	
	sprintf(command, "set/long LOWSKY=%d,%d UPPSKY=%d,%d", sky[0], sky[1], sky[2], sky[3]);
	AppendDialogText(command);
	sprintf(s, "%d", sky[0]);
	UxPutValue(UxFindSwidget("tf_lowsky1"), s);
	sprintf(s, "%d", sky[1]);
	UxPutValue(UxFindSwidget("tf_lowsky2"), s);
	sprintf(s, "%d", sky[2]);
	UxPutValue(UxFindSwidget("tf_uppsky1"), s);
	sprintf(s, "%d", sky[3]);
	UxPutValue(UxFindSwidget("tf_uppsky2"), s);
	
	Lowsky[0] = sky[0]; Lowsky[1] = sky[1];
	Uppsky[0] = sky[2]; Uppsky[1] = sky[3];
	
	
	
	
	
	
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	activateCB_pushButton28( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("ExtractShell"));
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	activateCB_pb_ext_object( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char command[128], s[10];
	extern int Objlim[];
	
	sprintf(command, "%s 2", C_EXTR_GCOOR);
	AppendDialogText(command);
	
	if ( !GetCoords(Objlim, 2) )
	    return;
	
	sprintf(command, "set/long OBJECT=%d,%d", Objlim[0], Objlim[1]);
	AppendDialogText(command);
	sprintf(s, "%d", Objlim[0]);
	UxPutValue(UxFindSwidget("tf_object1"), s);
	sprintf(s, "%d", Objlim[1]);
	UxPutValue(UxFindSwidget("tf_object2"), s);
	
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	activateCB_pb_ext_fit( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_FIT_SKY);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	activateCB_pb_ext_weight( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_EXT_WEIGHT);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	activateCB_pb_ext_average( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_EXT_AVER);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_extmtd_aver( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( !UpdateToggle )
	    return;
	
	if ( XmToggleButtonGetState(UxWidget) )  /*selected */
	    WriteKeyword("AVERAGE", K_EXTMTD);
	else
	    WriteKeyword("LINEAR", K_EXTMTD);
	
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_skymod_1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( !UpdateToggle )
	    return;
	
	if ( XmToggleButtonGetState(UxWidget) )  /*selected */
	    WriteKeyword("1", K_SKYMOD);
	else
	    WriteKeyword("0", K_SKYMOD);
	
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_lowsky2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Lowsky[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Lowsky[1] ) {
	    Lowsky[1] = val;
	    WriteKeyword(text, K_LOWSKY2);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_order( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Order;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Order ) {
	    Order = val;
	    WriteKeyword(text, K_ORDER);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_skyord( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Skyord;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Skyord ) {
	    Skyord = val;
	    WriteKeyword(text, K_SKYORD);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_ron( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	float val;
	extern float Ron;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%f", &val);
	
	if ( val != Ron ) {
	    Ron = val;
	    WriteKeyword(text, K_RON);
	}
	
	XtFree(text);
	
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_niter( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Niter;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Niter ) {
	    Niter = val;
	    WriteKeyword(text, K_NITER);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_gain( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	float val;
	extern float Gain;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%f", &val);
	
	if ( val != Gain ) {
	    Gain = val;
	    WriteKeyword(text, K_GAIN);
	}
	
	XtFree(text);
	
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_radius( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Radius;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Radius ) {
	    Radius = val;
	    WriteKeyword(text, K_RADIUS);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_sigma( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	float val;
	extern float Sigma;
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%f", &val);
	
	if ( val != Sigma ) {
	    Sigma = val;
	    WriteKeyword(text, K_SIGMA);
	}
	
	XtFree(text);
	
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_lowsky1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Lowsky[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Lowsky[0] ) {
	    Lowsky[0] = val;
	    WriteKeyword(text, K_LOWSKY1);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_uppsky1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Uppsky[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Uppsky[0] ) {
	    Uppsky[0] = val;
	    WriteKeyword(text, K_UPPSKY1);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_uppsky2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Uppsky[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Uppsky[1] ) {
	    Uppsky[1] = val;
	    WriteKeyword(text, K_UPPSKY2);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_object1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Objlim[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Objlim[0] ) {
	    Objlim[0] = val;
	    WriteKeyword(text, K_OBJECT1);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_object2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCExtractShell        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtractShellContext;
	UxExtractShellContext = UxContext =
			(_UxCExtractShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char *text;
	int val;
	extern int Objlim[];
	
	text = XmTextGetString(UxWidget);
	sscanf(text, "%d", &val);
	
	if ( val != Objlim[1] ) {
	    Objlim[1] = val;
	    WriteKeyword(text, K_OBJECT2);
	}
	
	XtFree(text);
	}
	UxExtractShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ExtractShell()
{
	UxPutBackground( ExtractShell, WindowBackground );
	UxPutGeometry( ExtractShell, "+10+60" );
	UxPutKeyboardFocusPolicy( ExtractShell, "pointer" );
	UxPutTitle( ExtractShell, "Spectrum Extraction" );
	UxPutHeight( ExtractShell, 661 );
	UxPutWidth( ExtractShell, 490 );
	UxPutY( ExtractShell, 21 );
	UxPutX( ExtractShell, 2 );

	UxPutBackground( form14, WindowBackground );
	UxPutHeight( form14, 662 );
	UxPutWidth( form14, 490 );
	UxPutY( form14, 0 );
	UxPutX( form14, 0 );
	UxPutUnitType( form14, "pixels" );
	UxPutResizePolicy( form14, "resize_none" );

	UxPutBackground( form15, ButtonBackground );
	UxPutHeight( form15, 84 );
	UxPutWidth( form15, 490 );
	UxPutY( form15, 576 );
	UxPutX( form15, 0 );
	UxPutResizePolicy( form15, "resize_none" );

	UxPutLabelString( pb_ext_sky, "Get sky" );
	UxPutForeground( pb_ext_sky, ApplyForeground );
	UxPutFontList( pb_ext_sky, BoldTextFont );
	UxPutBackground( pb_ext_sky, ButtonBackground );
	UxPutHeight( pb_ext_sky, 30 );
	UxPutWidth( pb_ext_sky, 86 );
	UxPutY( pb_ext_sky, 4 );
	UxPutX( pb_ext_sky, 8 );

	UxPutLabelString( pushButton28, "Cancel" );
	UxPutForeground( pushButton28, CancelForeground );
	UxPutFontList( pushButton28, BoldTextFont );
	UxPutBackground( pushButton28, ButtonBackground );
	UxPutHeight( pushButton28, 30 );
	UxPutWidth( pushButton28, 86 );
	UxPutY( pushButton28, 40 );
	UxPutX( pushButton28, 8 );

	UxPutLabelString( pb_ext_object, "Get object" );
	UxPutForeground( pb_ext_object, ApplyForeground );
	UxPutFontList( pb_ext_object, BoldTextFont );
	UxPutBackground( pb_ext_object, ButtonBackground );
	UxPutHeight( pb_ext_object, 30 );
	UxPutWidth( pb_ext_object, 86 );
	UxPutY( pb_ext_object, 4 );
	UxPutX( pb_ext_object, 102 );

	UxPutLabelString( pb_ext_fit, "Fit sky" );
	UxPutForeground( pb_ext_fit, ApplyForeground );
	UxPutFontList( pb_ext_fit, BoldTextFont );
	UxPutBackground( pb_ext_fit, ButtonBackground );
	UxPutHeight( pb_ext_fit, 30 );
	UxPutWidth( pb_ext_fit, 86 );
	UxPutY( pb_ext_fit, 4 );
	UxPutX( pb_ext_fit, 196 );

	UxPutLabelString( pb_ext_weight, "Ext weight" );
	UxPutForeground( pb_ext_weight, ApplyForeground );
	UxPutFontList( pb_ext_weight, BoldTextFont );
	UxPutBackground( pb_ext_weight, ButtonBackground );
	UxPutHeight( pb_ext_weight, 30 );
	UxPutWidth( pb_ext_weight, 86 );
	UxPutY( pb_ext_weight, 4 );
	UxPutX( pb_ext_weight, 386 );

	UxPutLabelString( pb_ext_average, "Ext average" );
	UxPutForeground( pb_ext_average, ApplyForeground );
	UxPutFontList( pb_ext_average, BoldTextFont );
	UxPutBackground( pb_ext_average, ButtonBackground );
	UxPutHeight( pb_ext_average, 30 );
	UxPutWidth( pb_ext_average, 86 );
	UxPutY( pb_ext_average, 4 );
	UxPutX( pb_ext_average, 290 );

	UxPutFontList( shelp_extract, TextFont );
	UxPutEditable( shelp_extract, "false" );
	UxPutCursorPositionVisible( shelp_extract, "false" );
	UxPutBackground( shelp_extract, SHelpBackground );
	UxPutHeight( shelp_extract, 50 );
	UxPutWidth( shelp_extract, 484 );
	UxPutY( shelp_extract, 516 );
	UxPutX( shelp_extract, 2 );

	UxPutBackground( separator10, WindowBackground );
	UxPutHeight( separator10, 10 );
	UxPutWidth( separator10, 492 );
	UxPutY( separator10, 506 );
	UxPutX( separator10, 0 );

	UxPutBackground( separator11, WindowBackground );
	UxPutHeight( separator11, 10 );
	UxPutWidth( separator11, 492 );
	UxPutY( separator11, 564 );
	UxPutX( separator11, -2 );

	UxPutIsAligned( rowColumn6, "true" );
	UxPutEntryAlignment( rowColumn6, "alignment_beginning" );
	UxPutBorderWidth( rowColumn6, 0 );
	UxPutShadowThickness( rowColumn6, 0 );
	UxPutLabelString( rowColumn6, "" );
	UxPutEntryBorder( rowColumn6, 0 );
	UxPutBackground( rowColumn6, "grey80" );
	UxPutRadioBehavior( rowColumn6, "true" );
	UxPutHeight( rowColumn6, 67 );
	UxPutWidth( rowColumn6, 82 );
	UxPutY( rowColumn6, 422 );
	UxPutX( rowColumn6, 264 );

	UxPutIndicatorSize( rb_extmtd_line, 16 );
	UxPutHighlightOnEnter( rb_extmtd_line, "true" );
	UxPutForeground( rb_extmtd_line, TextForeground );
	UxPutSelectColor( rb_extmtd_line, SelectColor );
	UxPutSet( rb_extmtd_line, "true" );
	UxPutLabelString( rb_extmtd_line, "SUM" );
	UxPutFontList( rb_extmtd_line, TextFont );
	UxPutBackground( rb_extmtd_line, WindowBackground );
	UxPutHeight( rb_extmtd_line, 20 );
	UxPutWidth( rb_extmtd_line, 76 );
	UxPutY( rb_extmtd_line, 2 );
	UxPutX( rb_extmtd_line, 3 );

	UxPutIndicatorSize( rb_extmtd_aver, 16 );
	UxPutHighlightOnEnter( rb_extmtd_aver, "true" );
	UxPutSet( rb_extmtd_aver, "false" );
	UxPutForeground( rb_extmtd_aver, TextForeground );
	UxPutSelectColor( rb_extmtd_aver, SelectColor );
	UxPutLabelString( rb_extmtd_aver, "AVERAGE" );
	UxPutFontList( rb_extmtd_aver, TextFont );
	UxPutBackground( rb_extmtd_aver, WindowBackground );
	UxPutHeight( rb_extmtd_aver, 66 );
	UxPutWidth( rb_extmtd_aver, 76 );
	UxPutY( rb_extmtd_aver, 34 );
	UxPutX( rb_extmtd_aver, 3 );

	UxPutForeground( label47, TextForeground );
	UxPutAlignment( label47, "alignment_beginning" );
	UxPutLabelString( label47, "Extraction method" );
	UxPutFontList( label47, TextFont );
	UxPutBackground( label47, WindowBackground );
	UxPutHeight( label47, 26 );
	UxPutWidth( label47, 122 );
	UxPutY( label47, 396 );
	UxPutX( label47, 256 );

	UxPutOrientation( separatorGadget26, "vertical" );
	UxPutHeight( separatorGadget26, 84 );
	UxPutWidth( separatorGadget26, 12 );
	UxPutY( separatorGadget26, 412 );
	UxPutX( separatorGadget26, 241 );

	UxPutHeight( separatorGadget27, 8 );
	UxPutWidth( separatorGadget27, 144 );
	UxPutY( separatorGadget27, 492 );
	UxPutX( separatorGadget27, 246 );

	UxPutOrientation( separatorGadget28, "vertical" );
	UxPutHeight( separatorGadget28, 84 );
	UxPutWidth( separatorGadget28, 12 );
	UxPutY( separatorGadget28, 412 );
	UxPutX( separatorGadget28, 384 );

	UxPutIsAligned( rowColumn7, "true" );
	UxPutEntryAlignment( rowColumn7, "alignment_beginning" );
	UxPutBorderWidth( rowColumn7, 0 );
	UxPutShadowThickness( rowColumn7, 0 );
	UxPutLabelString( rowColumn7, "" );
	UxPutEntryBorder( rowColumn7, 0 );
	UxPutBackground( rowColumn7, "grey80" );
	UxPutRadioBehavior( rowColumn7, "true" );
	UxPutHeight( rowColumn7, 67 );
	UxPutWidth( rowColumn7, 82 );
	UxPutY( rowColumn7, 420 );
	UxPutX( rowColumn7, 45 );

	UxPutIndicatorSize( rb_skymod_0, 16 );
	UxPutHighlightOnEnter( rb_skymod_0, "true" );
	UxPutForeground( rb_skymod_0, TextForeground );
	UxPutSelectColor( rb_skymod_0, SelectColor );
	UxPutSet( rb_skymod_0, "true" );
	UxPutLabelString( rb_skymod_0, "Same spatial profile" );
	UxPutFontList( rb_skymod_0, TextFont );
	UxPutBackground( rb_skymod_0, WindowBackground );
	UxPutHeight( rb_skymod_0, 20 );
	UxPutWidth( rb_skymod_0, 76 );
	UxPutY( rb_skymod_0, 0 );
	UxPutX( rb_skymod_0, -12 );

	UxPutIndicatorSize( rb_skymod_1, 16 );
	UxPutHighlightOnEnter( rb_skymod_1, "true" );
	UxPutSet( rb_skymod_1, "false" );
	UxPutForeground( rb_skymod_1, TextForeground );
	UxPutSelectColor( rb_skymod_1, SelectColor );
	UxPutLabelString( rb_skymod_1, "Independent profile" );
	UxPutFontList( rb_skymod_1, TextFont );
	UxPutBackground( rb_skymod_1, WindowBackground );
	UxPutHeight( rb_skymod_1, 66 );
	UxPutWidth( rb_skymod_1, 76 );
	UxPutY( rb_skymod_1, 34 );
	UxPutX( rb_skymod_1, 3 );

	UxPutForeground( label48, TextForeground );
	UxPutAlignment( label48, "alignment_beginning" );
	UxPutLabelString( label48, "Sky fitting mode" );
	UxPutFontList( label48, TextFont );
	UxPutBackground( label48, WindowBackground );
	UxPutHeight( label48, 26 );
	UxPutWidth( label48, 105 );
	UxPutY( label48, 394 );
	UxPutX( label48, 68 );

	UxPutOrientation( separatorGadget29, "vertical" );
	UxPutHeight( separatorGadget29, 84 );
	UxPutWidth( separatorGadget29, 12 );
	UxPutY( separatorGadget29, 410 );
	UxPutX( separatorGadget29, 31 );

	UxPutOrientation( separatorGadget30, "vertical" );
	UxPutHeight( separatorGadget30, 84 );
	UxPutWidth( separatorGadget30, 12 );
	UxPutY( separatorGadget30, 410 );
	UxPutX( separatorGadget30, 202 );

	UxPutHeight( separatorGadget31, 10 );
	UxPutWidth( separatorGadget31, 33 );
	UxPutY( separatorGadget31, 406 );
	UxPutX( separatorGadget31, 35 );

	UxPutHeight( separatorGadget32, 10 );
	UxPutWidth( separatorGadget32, 36 );
	UxPutY( separatorGadget32, 406 );
	UxPutX( separatorGadget32, 172 );

	UxPutHeight( separatorGadget33, 8 );
	UxPutWidth( separatorGadget33, 172 );
	UxPutY( separatorGadget33, 490 );
	UxPutX( separatorGadget33, 36 );

	UxPutHeight( separatorGadget34, 10 );
	UxPutWidth( separatorGadget34, 16 );
	UxPutY( separatorGadget34, 408 );
	UxPutX( separatorGadget34, 374 );

	UxPutHeight( separatorGadget35, 10 );
	UxPutWidth( separatorGadget35, 13 );
	UxPutY( separatorGadget35, 408 );
	UxPutX( separatorGadget35, 246 );

	UxPutForeground( label49, TextForeground );
	UxPutAlignment( label49, "alignment_beginning" );
	UxPutLabelString( label49, "Object limits (pixels) :" );
	UxPutFontList( label49, TextFont );
	UxPutBackground( label49, LabelBackground );
	UxPutHeight( label49, 30 );
	UxPutWidth( label49, 150 );
	UxPutY( label49, 62 );
	UxPutX( label49, 16 );

	UxPutForeground( label50, TextForeground );
	UxPutAlignment( label50, "alignment_beginning" );
	UxPutLabelString( label50, "Sky limits (pixels) :" );
	UxPutFontList( label50, TextFont );
	UxPutBackground( label50, LabelBackground );
	UxPutHeight( label50, 30 );
	UxPutWidth( label50, 120 );
	UxPutY( label50, 26 );
	UxPutX( label50, 16 );

	UxPutMaxLength( tf_lowsky2, 5 );
	UxPutForeground( tf_lowsky2, TextForeground );
	UxPutHighlightOnEnter( tf_lowsky2, "true" );
	UxPutFontList( tf_lowsky2, TextFont );
	UxPutBackground( tf_lowsky2, TextBackground );
	UxPutHeight( tf_lowsky2, 34 );
	UxPutWidth( tf_lowsky2, 56 );
	UxPutY( tf_lowsky2, 24 );
	UxPutX( tf_lowsky2, 292 );

	UxPutForeground( tf_sky, TextForeground );
	UxPutHighlightOnEnter( tf_sky, "true" );
	UxPutFontList( tf_sky, TextFont );
	UxPutBackground( tf_sky, TextBackground );
	UxPutHeight( tf_sky, 34 );
	UxPutWidth( tf_sky, 210 );
	UxPutY( tf_sky, 356 );
	UxPutX( tf_sky, 238 );

	UxPutForeground( label38, TextForeground );
	UxPutAlignment( label38, "alignment_beginning" );
	UxPutLabelString( label38, "Sky (image or constant) :" );
	UxPutFontList( label38, TextFont );
	UxPutBackground( label38, LabelBackground );
	UxPutHeight( label38, 30 );
	UxPutWidth( label38, 186 );
	UxPutY( label38, 354 );
	UxPutX( label38, 16 );

	UxPutForeground( label42, TextForeground );
	UxPutAlignment( label42, "alignment_beginning" );
	UxPutLabelString( label42, "Order for optimal extraction :" );
	UxPutFontList( label42, TextFont );
	UxPutBackground( label42, LabelBackground );
	UxPutHeight( label42, 30 );
	UxPutWidth( label42, 194 );
	UxPutY( label42, 136 );
	UxPutX( label42, 16 );

	UxPutForeground( tf_order, TextForeground );
	UxPutHighlightOnEnter( tf_order, "true" );
	UxPutFontList( tf_order, TextFont );
	UxPutBackground( tf_order, TextBackground );
	UxPutHeight( tf_order, 34 );
	UxPutWidth( tf_order, 80 );
	UxPutY( tf_order, 136 );
	UxPutX( tf_order, 238 );

	UxPutForeground( tf_skyord, TextForeground );
	UxPutHighlightOnEnter( tf_skyord, "true" );
	UxPutFontList( tf_skyord, TextFont );
	UxPutBackground( tf_skyord, TextBackground );
	UxPutHeight( tf_skyord, 34 );
	UxPutWidth( tf_skyord, 80 );
	UxPutY( tf_skyord, 98 );
	UxPutX( tf_skyord, 238 );

	UxPutForeground( label43, TextForeground );
	UxPutAlignment( label43, "alignment_beginning" );
	UxPutLabelString( label43, "Order for sky fit :" );
	UxPutFontList( label43, TextFont );
	UxPutBackground( label43, LabelBackground );
	UxPutHeight( label43, 30 );
	UxPutWidth( label43, 120 );
	UxPutY( label43, 100 );
	UxPutX( label43, 16 );

	UxPutForeground( label45, TextForeground );
	UxPutAlignment( label45, "alignment_beginning" );
	UxPutLabelString( label45, "Read-out-noise (e-) :" );
	UxPutFontList( label45, TextFont );
	UxPutBackground( label45, LabelBackground );
	UxPutHeight( label45, 30 );
	UxPutWidth( label45, 154 );
	UxPutY( label45, 210 );
	UxPutX( label45, 16 );

	UxPutForeground( tf_ron, TextForeground );
	UxPutHighlightOnEnter( tf_ron, "true" );
	UxPutFontList( tf_ron, TextFont );
	UxPutBackground( tf_ron, TextBackground );
	UxPutHeight( tf_ron, 34 );
	UxPutWidth( tf_ron, 80 );
	UxPutY( tf_ron, 210 );
	UxPutX( tf_ron, 238 );

	UxPutForeground( tf_niter, TextForeground );
	UxPutHighlightOnEnter( tf_niter, "true" );
	UxPutFontList( tf_niter, TextFont );
	UxPutBackground( tf_niter, TextBackground );
	UxPutHeight( tf_niter, 34 );
	UxPutWidth( tf_niter, 80 );
	UxPutY( tf_niter, 172 );
	UxPutX( tf_niter, 238 );

	UxPutForeground( label46, TextForeground );
	UxPutAlignment( label46, "alignment_beginning" );
	UxPutLabelString( label46, "Extraction iterations :" );
	UxPutFontList( label46, TextFont );
	UxPutBackground( label46, LabelBackground );
	UxPutHeight( label46, 30 );
	UxPutWidth( label46, 146 );
	UxPutY( label46, 174 );
	UxPutX( label46, 16 );

	UxPutForeground( label51, TextForeground );
	UxPutAlignment( label51, "alignment_beginning" );
	UxPutLabelString( label51, "Inverse gain factor (e-/ADU) :" );
	UxPutFontList( label51, TextFont );
	UxPutBackground( label51, LabelBackground );
	UxPutHeight( label51, 30 );
	UxPutWidth( label51, 206 );
	UxPutY( label51, 246 );
	UxPutX( label51, 16 );

	UxPutForeground( tf_gain, TextForeground );
	UxPutHighlightOnEnter( tf_gain, "true" );
	UxPutFontList( tf_gain, TextFont );
	UxPutBackground( tf_gain, TextBackground );
	UxPutHeight( tf_gain, 34 );
	UxPutWidth( tf_gain, 80 );
	UxPutY( tf_gain, 246 );
	UxPutX( tf_gain, 238 );

	UxPutForeground( label52, TextForeground );
	UxPutAlignment( label52, "alignment_beginning" );
	UxPutLabelString( label52, "Radius for cosmic rays removal :" );
	UxPutFontList( label52, TextFont );
	UxPutBackground( label52, LabelBackground );
	UxPutHeight( label52, 30 );
	UxPutWidth( label52, 220 );
	UxPutY( label52, 320 );
	UxPutX( label52, 16 );

	UxPutForeground( tf_radius, TextForeground );
	UxPutHighlightOnEnter( tf_radius, "true" );
	UxPutFontList( tf_radius, TextFont );
	UxPutBackground( tf_radius, TextBackground );
	UxPutHeight( tf_radius, 34 );
	UxPutWidth( tf_radius, 80 );
	UxPutY( tf_radius, 320 );
	UxPutX( tf_radius, 238 );

	UxPutForeground( tf_sigma, TextForeground );
	UxPutHighlightOnEnter( tf_sigma, "true" );
	UxPutFontList( tf_sigma, TextFont );
	UxPutBackground( tf_sigma, TextBackground );
	UxPutHeight( tf_sigma, 34 );
	UxPutWidth( tf_sigma, 80 );
	UxPutY( tf_sigma, 282 );
	UxPutX( tf_sigma, 238 );

	UxPutForeground( label53, TextForeground );
	UxPutAlignment( label53, "alignment_beginning" );
	UxPutLabelString( label53, "Threshold for cosmic rays :" );
	UxPutFontList( label53, TextFont );
	UxPutBackground( label53, LabelBackground );
	UxPutHeight( label53, 30 );
	UxPutWidth( label53, 182 );
	UxPutY( label53, 284 );
	UxPutX( label53, 16 );

	UxPutMaxLength( tf_lowsky1, 5 );
	UxPutForeground( tf_lowsky1, TextForeground );
	UxPutHighlightOnEnter( tf_lowsky1, "true" );
	UxPutFontList( tf_lowsky1, TextFont );
	UxPutBackground( tf_lowsky1, TextBackground );
	UxPutHeight( tf_lowsky1, 34 );
	UxPutWidth( tf_lowsky1, 56 );
	UxPutY( tf_lowsky1, 24 );
	UxPutX( tf_lowsky1, 238 );

	UxPutMaxLength( tf_uppsky1, 5 );
	UxPutForeground( tf_uppsky1, TextForeground );
	UxPutHighlightOnEnter( tf_uppsky1, "true" );
	UxPutFontList( tf_uppsky1, TextFont );
	UxPutBackground( tf_uppsky1, TextBackground );
	UxPutHeight( tf_uppsky1, 34 );
	UxPutWidth( tf_uppsky1, 56 );
	UxPutY( tf_uppsky1, 24 );
	UxPutX( tf_uppsky1, 346 );

	UxPutMaxLength( tf_uppsky2, 5 );
	UxPutForeground( tf_uppsky2, TextForeground );
	UxPutHighlightOnEnter( tf_uppsky2, "true" );
	UxPutFontList( tf_uppsky2, TextFont );
	UxPutBackground( tf_uppsky2, TextBackground );
	UxPutHeight( tf_uppsky2, 34 );
	UxPutWidth( tf_uppsky2, 56 );
	UxPutY( tf_uppsky2, 24 );
	UxPutX( tf_uppsky2, 400 );

	UxPutMaxLength( tf_object1, 5 );
	UxPutForeground( tf_object1, TextForeground );
	UxPutHighlightOnEnter( tf_object1, "true" );
	UxPutFontList( tf_object1, TextFont );
	UxPutBackground( tf_object1, TextBackground );
	UxPutHeight( tf_object1, 34 );
	UxPutWidth( tf_object1, 56 );
	UxPutY( tf_object1, 60 );
	UxPutX( tf_object1, 238 );

	UxPutMaxLength( tf_object2, 5 );
	UxPutForeground( tf_object2, TextForeground );
	UxPutHighlightOnEnter( tf_object2, "true" );
	UxPutFontList( tf_object2, TextFont );
	UxPutBackground( tf_object2, TextBackground );
	UxPutHeight( tf_object2, 34 );
	UxPutWidth( tf_object2, 56 );
	UxPutY( tf_object2, 60 );
	UxPutX( tf_object2, 292 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ExtractShell()
{
	/* Create the swidgets */

	ExtractShell = UxCreateApplicationShell( "ExtractShell", NO_PARENT );
	UxPutContext( ExtractShell, UxExtractShellContext );

	form14 = UxCreateForm( "form14", ExtractShell );
	form15 = UxCreateForm( "form15", form14 );
	pb_ext_sky = UxCreatePushButton( "pb_ext_sky", form15 );
	pushButton28 = UxCreatePushButton( "pushButton28", form15 );
	pb_ext_object = UxCreatePushButton( "pb_ext_object", form15 );
	pb_ext_fit = UxCreatePushButton( "pb_ext_fit", form15 );
	pb_ext_weight = UxCreatePushButton( "pb_ext_weight", form15 );
	pb_ext_average = UxCreatePushButton( "pb_ext_average", form15 );
	shelp_extract = UxCreateText( "shelp_extract", form14 );
	separator10 = UxCreateSeparator( "separator10", form14 );
	separator11 = UxCreateSeparator( "separator11", form14 );
	rowColumn6 = UxCreateRowColumn( "rowColumn6", form14 );
	rb_extmtd_line = UxCreateToggleButton( "rb_extmtd_line", rowColumn6 );
	rb_extmtd_aver = UxCreateToggleButton( "rb_extmtd_aver", rowColumn6 );
	label47 = UxCreateLabel( "label47", form14 );
	separatorGadget26 = UxCreateSeparatorGadget( "separatorGadget26", form14 );
	separatorGadget27 = UxCreateSeparatorGadget( "separatorGadget27", form14 );
	separatorGadget28 = UxCreateSeparatorGadget( "separatorGadget28", form14 );
	rowColumn7 = UxCreateRowColumn( "rowColumn7", form14 );
	rb_skymod_0 = UxCreateToggleButton( "rb_skymod_0", rowColumn7 );
	rb_skymod_1 = UxCreateToggleButton( "rb_skymod_1", rowColumn7 );
	label48 = UxCreateLabel( "label48", form14 );
	separatorGadget29 = UxCreateSeparatorGadget( "separatorGadget29", form14 );
	separatorGadget30 = UxCreateSeparatorGadget( "separatorGadget30", form14 );
	separatorGadget31 = UxCreateSeparatorGadget( "separatorGadget31", form14 );
	separatorGadget32 = UxCreateSeparatorGadget( "separatorGadget32", form14 );
	separatorGadget33 = UxCreateSeparatorGadget( "separatorGadget33", form14 );
	separatorGadget34 = UxCreateSeparatorGadget( "separatorGadget34", form14 );
	separatorGadget35 = UxCreateSeparatorGadget( "separatorGadget35", form14 );
	label49 = UxCreateLabel( "label49", form14 );
	label50 = UxCreateLabel( "label50", form14 );
	tf_lowsky2 = UxCreateTextField( "tf_lowsky2", form14 );
	tf_sky = UxCreateTextField( "tf_sky", form14 );
	label38 = UxCreateLabel( "label38", form14 );
	label42 = UxCreateLabel( "label42", form14 );
	tf_order = UxCreateTextField( "tf_order", form14 );
	tf_skyord = UxCreateTextField( "tf_skyord", form14 );
	label43 = UxCreateLabel( "label43", form14 );
	label45 = UxCreateLabel( "label45", form14 );
	tf_ron = UxCreateTextField( "tf_ron", form14 );
	tf_niter = UxCreateTextField( "tf_niter", form14 );
	label46 = UxCreateLabel( "label46", form14 );
	label51 = UxCreateLabel( "label51", form14 );
	tf_gain = UxCreateTextField( "tf_gain", form14 );
	label52 = UxCreateLabel( "label52", form14 );
	tf_radius = UxCreateTextField( "tf_radius", form14 );
	tf_sigma = UxCreateTextField( "tf_sigma", form14 );
	label53 = UxCreateLabel( "label53", form14 );
	tf_lowsky1 = UxCreateTextField( "tf_lowsky1", form14 );
	tf_uppsky1 = UxCreateTextField( "tf_uppsky1", form14 );
	tf_uppsky2 = UxCreateTextField( "tf_uppsky2", form14 );
	tf_object1 = UxCreateTextField( "tf_object1", form14 );
	tf_object2 = UxCreateTextField( "tf_object2", form14 );

	_Uxinit_ExtractShell();

	/* Create the X widgets */

	UxCreateWidget( ExtractShell );
	UxCreateWidget( form14 );
	UxCreateWidget( form15 );
	UxCreateWidget( pb_ext_sky );
	UxCreateWidget( pushButton28 );
	UxCreateWidget( pb_ext_object );
	UxCreateWidget( pb_ext_fit );
	UxCreateWidget( pb_ext_weight );
	UxCreateWidget( pb_ext_average );
	UxCreateWidget( shelp_extract );
	UxCreateWidget( separator10 );
	UxCreateWidget( separator11 );
	UxCreateWidget( rowColumn6 );
	UxCreateWidget( rb_extmtd_line );
	UxCreateWidget( rb_extmtd_aver );
	UxCreateWidget( label47 );
	UxCreateWidget( separatorGadget26 );
	UxCreateWidget( separatorGadget27 );
	UxCreateWidget( separatorGadget28 );
	UxCreateWidget( rowColumn7 );
	UxCreateWidget( rb_skymod_0 );
	UxCreateWidget( rb_skymod_1 );
	UxCreateWidget( label48 );
	UxCreateWidget( separatorGadget29 );
	UxCreateWidget( separatorGadget30 );
	UxCreateWidget( separatorGadget31 );
	UxCreateWidget( separatorGadget32 );
	UxCreateWidget( separatorGadget33 );
	UxCreateWidget( separatorGadget34 );
	UxCreateWidget( separatorGadget35 );
	UxCreateWidget( label49 );
	UxCreateWidget( label50 );
	UxCreateWidget( tf_lowsky2 );
	UxCreateWidget( tf_sky );
	UxCreateWidget( label38 );
	UxCreateWidget( label42 );
	UxCreateWidget( tf_order );
	UxCreateWidget( tf_skyord );
	UxCreateWidget( label43 );
	UxCreateWidget( label45 );
	UxCreateWidget( tf_ron );
	UxCreateWidget( tf_niter );
	UxCreateWidget( label46 );
	UxCreateWidget( label51 );
	UxCreateWidget( tf_gain );
	UxCreateWidget( label52 );
	UxCreateWidget( tf_radius );
	UxCreateWidget( tf_sigma );
	UxCreateWidget( label53 );
	UxCreateWidget( tf_lowsky1 );
	UxCreateWidget( tf_uppsky1 );
	UxCreateWidget( tf_uppsky2 );
	UxCreateWidget( tf_object1 );
	UxCreateWidget( tf_object2 );

	UxAddCallback( pb_ext_sky, XmNactivateCallback,
			activateCB_pb_ext_sky,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( pushButton28, XmNactivateCallback,
			activateCB_pushButton28,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( pb_ext_object, XmNactivateCallback,
			activateCB_pb_ext_object,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( pb_ext_fit, XmNactivateCallback,
			activateCB_pb_ext_fit,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( pb_ext_weight, XmNactivateCallback,
			activateCB_pb_ext_weight,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( pb_ext_average, XmNactivateCallback,
			activateCB_pb_ext_average,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( rb_extmtd_aver, XmNvalueChangedCallback,
			valueChangedCB_rb_extmtd_aver,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( rb_skymod_1, XmNvalueChangedCallback,
			valueChangedCB_rb_skymod_1,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_lowsky2, XmNlosingFocusCallback,
			losingFocusCB_tf_lowsky2,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_order, XmNlosingFocusCallback,
			losingFocusCB_tf_order,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_skyord, XmNlosingFocusCallback,
			losingFocusCB_tf_skyord,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_ron, XmNlosingFocusCallback,
			losingFocusCB_tf_ron,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_niter, XmNlosingFocusCallback,
			losingFocusCB_tf_niter,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_gain, XmNlosingFocusCallback,
			losingFocusCB_tf_gain,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_radius, XmNlosingFocusCallback,
			losingFocusCB_tf_radius,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_sigma, XmNlosingFocusCallback,
			losingFocusCB_tf_sigma,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_lowsky1, XmNlosingFocusCallback,
			losingFocusCB_tf_lowsky1,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_uppsky1, XmNlosingFocusCallback,
			losingFocusCB_tf_uppsky1,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_uppsky2, XmNlosingFocusCallback,
			losingFocusCB_tf_uppsky2,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_object1, XmNlosingFocusCallback,
			losingFocusCB_tf_object1,
			(XtPointer) UxExtractShellContext );

	UxAddCallback( tf_object2, XmNlosingFocusCallback,
			losingFocusCB_tf_object2,
			(XtPointer) UxExtractShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ExtractShell );

	return ( ExtractShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ExtractShell()
{
	swidget                 rtrn;
	_UxCExtractShell        *UxContext;

	UxExtractShellContext = UxContext =
		(_UxCExtractShell *) UxMalloc( sizeof(_UxCExtractShell) );

	rtrn = _Uxbuild_ExtractShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ExtractShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ExtractShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

