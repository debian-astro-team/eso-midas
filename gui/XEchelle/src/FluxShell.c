/* @(#)FluxShell.c	19.1 (ES0-DMD) 02/25/03 13:45:56 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	FluxShell.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSepG.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxFluxShell;
	swidget	Uxform10;
	swidget	Uxform11;
	swidget	Uxpb_flux_extin;
	swidget	UxpushButton18;
	swidget	Uxpb_flux_presp;
	swidget	Uxpb_flux_integr;
	swidget	Uxpb_flux_fit;
	swidget	Uxpb_flux_pflux;
	swidget	Uxpb_flux_filter;
	swidget	Uxpb_correct;
	swidget	Uxpb_edit_flux;
	swidget	Uxshelp_flux;
	swidget	Uxseparator8;
	swidget	Uxseparator9;
	swidget	Uxlabel27;
	swidget	Uxtf_extin_tbl;
	swidget	Uxlabel29;
	swidget	Uxtf_flux_tbl;
	swidget	Uxlabel30;
	swidget	Uxtf_output_resp;
	swidget	UxrowColumn4;
	swidget	Uxrb_fityp_poly;
	swidget	Uxrb_fityp_spli;
	swidget	Uxlabel33;
	swidget	UxseparatorGadget16;
	swidget	UxseparatorGadget17;
	swidget	UxseparatorGadget18;
	swidget	UxrowColumn5;
	swidget	Uxrb_plotyp_rati;
	swidget	Uxrb_plotyp_colo;
	swidget	Uxlabel34;
	swidget	UxseparatorGadget21;
	swidget	UxseparatorGadget22;
	swidget	UxseparatorGadget23;
	swidget	UxseparatorGadget24;
	swidget	UxseparatorGadget25;
	swidget	UxseparatorGadget19;
	swidget	UxseparatorGadget20;
	swidget	Uxlabel35;
	swidget	Uxtf_smooth;
	swidget	Uxlabel36;
	swidget	Uxtf_fitd;
	swidget	Uxlabel37;
} _UxCFluxShell;

#define FluxShell               UxFluxShellContext->UxFluxShell
#define form10                  UxFluxShellContext->Uxform10
#define form11                  UxFluxShellContext->Uxform11
#define pb_flux_extin           UxFluxShellContext->Uxpb_flux_extin
#define pushButton18            UxFluxShellContext->UxpushButton18
#define pb_flux_presp           UxFluxShellContext->Uxpb_flux_presp
#define pb_flux_integr          UxFluxShellContext->Uxpb_flux_integr
#define pb_flux_fit             UxFluxShellContext->Uxpb_flux_fit
#define pb_flux_pflux           UxFluxShellContext->Uxpb_flux_pflux
#define pb_flux_filter          UxFluxShellContext->Uxpb_flux_filter
#define pb_correct              UxFluxShellContext->Uxpb_correct
#define pb_edit_flux            UxFluxShellContext->Uxpb_edit_flux
#define shelp_flux              UxFluxShellContext->Uxshelp_flux
#define separator8              UxFluxShellContext->Uxseparator8
#define separator9              UxFluxShellContext->Uxseparator9
#define label27                 UxFluxShellContext->Uxlabel27
#define tf_extin_tbl            UxFluxShellContext->Uxtf_extin_tbl
#define label29                 UxFluxShellContext->Uxlabel29
#define tf_flux_tbl             UxFluxShellContext->Uxtf_flux_tbl
#define label30                 UxFluxShellContext->Uxlabel30
#define tf_output_resp          UxFluxShellContext->Uxtf_output_resp
#define rowColumn4              UxFluxShellContext->UxrowColumn4
#define rb_fityp_poly           UxFluxShellContext->Uxrb_fityp_poly
#define rb_fityp_spli           UxFluxShellContext->Uxrb_fityp_spli
#define label33                 UxFluxShellContext->Uxlabel33
#define separatorGadget16       UxFluxShellContext->UxseparatorGadget16
#define separatorGadget17       UxFluxShellContext->UxseparatorGadget17
#define separatorGadget18       UxFluxShellContext->UxseparatorGadget18
#define rowColumn5              UxFluxShellContext->UxrowColumn5
#define rb_plotyp_rati          UxFluxShellContext->Uxrb_plotyp_rati
#define rb_plotyp_colo          UxFluxShellContext->Uxrb_plotyp_colo
#define label34                 UxFluxShellContext->Uxlabel34
#define separatorGadget21       UxFluxShellContext->UxseparatorGadget21
#define separatorGadget22       UxFluxShellContext->UxseparatorGadget22
#define separatorGadget23       UxFluxShellContext->UxseparatorGadget23
#define separatorGadget24       UxFluxShellContext->UxseparatorGadget24
#define separatorGadget25       UxFluxShellContext->UxseparatorGadget25
#define separatorGadget19       UxFluxShellContext->UxseparatorGadget19
#define separatorGadget20       UxFluxShellContext->UxseparatorGadget20
#define label35                 UxFluxShellContext->Uxlabel35
#define tf_smooth               UxFluxShellContext->Uxtf_smooth
#define label36                 UxFluxShellContext->Uxlabel36
#define tf_fitd                 UxFluxShellContext->Uxtf_fitd
#define label37                 UxFluxShellContext->Uxlabel37

static _UxCFluxShell	*UxFluxShellContext;


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*FileSelectFlux = "#override\n\
<Btn3Down>:FileSelectACT()\n";

static char	*transTable34 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_FluxShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxFluxShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxFluxShellContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	Cardinal		UxNumParams = *p_UxNumParams;
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxFluxShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_flux_extin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_EXTIN);
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	activateCB_pushButton18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("FluxShell"));
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	activateCB_pb_flux_presp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	AppendDialogText(C_PLOT_RESP);
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	activateCB_pb_flux_integr( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_INTEGR);
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	activateCB_pb_flux_fit( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	AppendDialogText(C_FLUX_FIT);
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	activateCB_pb_flux_pflux( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	AppendDialogText(C_PLOT_FLUX);
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	activateCB_pb_flux_filter( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_FLUX_FILT);
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	activateCB_pb_correct( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_CORRECT);
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	activateCB_pb_edit_flux( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	AppendDialogText(C_EDIT_FLUX);
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_extin_tbl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_flux_tbl( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_output_resp( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	armCB_rb_fityp_poly( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxFluxShellContext = UxSaveCtx;
}

static void	armCB_rb_fityp_spli( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxFluxShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_fityp_spli( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	armCB_rb_plotyp_rati( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxFluxShellContext = UxSaveCtx;
}

static void	armCB_rb_plotyp_colo( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxFluxShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_plotyp_colo( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_smooth( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFluxShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_fitd( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFluxShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFluxShellContext;
	UxFluxShellContext = UxContext =
			(_UxCFluxShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFluxShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_FluxShell()
{
	UxPutBackground( FluxShell, WindowBackground );
	UxPutGeometry( FluxShell, "+10+60" );
	UxPutKeyboardFocusPolicy( FluxShell, "pointer" );
	UxPutTitle( FluxShell, "Flux calibration" );
	UxPutHeight( FluxShell, 427 );
	UxPutWidth( FluxShell, 490 );
	UxPutY( FluxShell, 83 );
	UxPutX( FluxShell, 294 );

	UxPutBackground( form10, WindowBackground );
	UxPutHeight( form10, 348 );
	UxPutWidth( form10, 408 );
	UxPutY( form10, 0 );
	UxPutX( form10, 0 );
	UxPutUnitType( form10, "pixels" );
	UxPutResizePolicy( form10, "resize_none" );

	UxPutBackground( form11, ButtonBackground );
	UxPutHeight( form11, 84 );
	UxPutWidth( form11, 490 );
	UxPutY( form11, 342 );
	UxPutX( form11, -2 );
	UxPutResizePolicy( form11, "resize_none" );

	UxPutLabelString( pb_flux_extin, "Extinct" );
	UxPutForeground( pb_flux_extin, ApplyForeground );
	UxPutFontList( pb_flux_extin, BoldTextFont );
	UxPutBackground( pb_flux_extin, ButtonBackground );
	UxPutHeight( pb_flux_extin, 30 );
	UxPutWidth( pb_flux_extin, 86 );
	UxPutY( pb_flux_extin, 4 );
	UxPutX( pb_flux_extin, 8 );

	UxPutLabelString( pushButton18, "Cancel" );
	UxPutForeground( pushButton18, CancelForeground );
	UxPutFontList( pushButton18, BoldTextFont );
	UxPutBackground( pushButton18, ButtonBackground );
	UxPutHeight( pushButton18, 30 );
	UxPutWidth( pushButton18, 86 );
	UxPutY( pushButton18, 42 );
	UxPutX( pushButton18, 290 );

	UxPutLabelString( pb_flux_presp, "Plot resp" );
	UxPutForeground( pb_flux_presp, ButtonForeground );
	UxPutFontList( pb_flux_presp, BoldTextFont );
	UxPutBackground( pb_flux_presp, ButtonBackground );
	UxPutHeight( pb_flux_presp, 30 );
	UxPutWidth( pb_flux_presp, 86 );
	UxPutY( pb_flux_presp, 42 );
	UxPutX( pb_flux_presp, 196 );

	UxPutLabelString( pb_flux_integr, "Integr" );
	UxPutForeground( pb_flux_integr, ApplyForeground );
	UxPutFontList( pb_flux_integr, BoldTextFont );
	UxPutBackground( pb_flux_integr, ButtonBackground );
	UxPutHeight( pb_flux_integr, 30 );
	UxPutWidth( pb_flux_integr, 86 );
	UxPutY( pb_flux_integr, 4 );
	UxPutX( pb_flux_integr, 102 );

	UxPutLabelString( pb_flux_fit, "Fit" );
	UxPutForeground( pb_flux_fit, ApplyForeground );
	UxPutFontList( pb_flux_fit, BoldTextFont );
	UxPutBackground( pb_flux_fit, ButtonBackground );
	UxPutHeight( pb_flux_fit, 30 );
	UxPutWidth( pb_flux_fit, 86 );
	UxPutY( pb_flux_fit, 4 );
	UxPutX( pb_flux_fit, 196 );

	UxPutLabelString( pb_flux_pflux, "Plot flux" );
	UxPutForeground( pb_flux_pflux, ButtonForeground );
	UxPutFontList( pb_flux_pflux, BoldTextFont );
	UxPutBackground( pb_flux_pflux, ButtonBackground );
	UxPutHeight( pb_flux_pflux, 30 );
	UxPutWidth( pb_flux_pflux, 86 );
	UxPutY( pb_flux_pflux, 42 );
	UxPutX( pb_flux_pflux, 102 );

	UxPutLabelString( pb_flux_filter, "Filter" );
	UxPutForeground( pb_flux_filter, ApplyForeground );
	UxPutFontList( pb_flux_filter, BoldTextFont );
	UxPutBackground( pb_flux_filter, ButtonBackground );
	UxPutHeight( pb_flux_filter, 30 );
	UxPutWidth( pb_flux_filter, 86 );
	UxPutY( pb_flux_filter, 4 );
	UxPutX( pb_flux_filter, 384 );

	UxPutLabelString( pb_correct, "Correct" );
	UxPutForeground( pb_correct, ApplyForeground );
	UxPutFontList( pb_correct, BoldTextFont );
	UxPutBackground( pb_correct, ButtonBackground );
	UxPutHeight( pb_correct, 30 );
	UxPutWidth( pb_correct, 86 );
	UxPutY( pb_correct, 42 );
	UxPutX( pb_correct, 8 );

	UxPutLabelString( pb_edit_flux, "Edit" );
	UxPutForeground( pb_edit_flux, ApplyForeground );
	UxPutFontList( pb_edit_flux, BoldTextFont );
	UxPutBackground( pb_edit_flux, ButtonBackground );
	UxPutHeight( pb_edit_flux, 30 );
	UxPutWidth( pb_edit_flux, 86 );
	UxPutY( pb_edit_flux, 4 );
	UxPutX( pb_edit_flux, 290 );

	UxPutFontList( shelp_flux, TextFont );
	UxPutEditable( shelp_flux, "false" );
	UxPutCursorPositionVisible( shelp_flux, "false" );
	UxPutBackground( shelp_flux, SHelpBackground );
	UxPutHeight( shelp_flux, 50 );
	UxPutWidth( shelp_flux, 484 );
	UxPutY( shelp_flux, 282 );
	UxPutX( shelp_flux, 0 );

	UxPutBackground( separator8, WindowBackground );
	UxPutHeight( separator8, 10 );
	UxPutWidth( separator8, 492 );
	UxPutY( separator8, 272 );
	UxPutX( separator8, -2 );

	UxPutBackground( separator9, WindowBackground );
	UxPutHeight( separator9, 10 );
	UxPutWidth( separator9, 492 );
	UxPutY( separator9, 330 );
	UxPutX( separator9, -4 );

	UxPutForeground( label27, TextForeground );
	UxPutAlignment( label27, "alignment_beginning" );
	UxPutLabelString( label27, "Extinction table :" );
	UxPutFontList( label27, TextFont );
	UxPutBackground( label27, LabelBackground );
	UxPutHeight( label27, 30 );
	UxPutWidth( label27, 126 );
	UxPutY( label27, 12 );
	UxPutX( label27, 10 );

	UxPutTranslations( tf_extin_tbl, FileSelectFlux );
	UxPutForeground( tf_extin_tbl, TextForeground );
	UxPutHighlightOnEnter( tf_extin_tbl, "true" );
	UxPutFontList( tf_extin_tbl, TextFont );
	UxPutBackground( tf_extin_tbl, TextBackground );
	UxPutHeight( tf_extin_tbl, 34 );
	UxPutWidth( tf_extin_tbl, 222 );
	UxPutY( tf_extin_tbl, 10 );
	UxPutX( tf_extin_tbl, 148 );

	UxPutForeground( label29, TextForeground );
	UxPutAlignment( label29, "alignment_beginning" );
	UxPutLabelString( label29, "Flux table :" );
	UxPutFontList( label29, TextFont );
	UxPutBackground( label29, LabelBackground );
	UxPutHeight( label29, 30 );
	UxPutWidth( label29, 122 );
	UxPutY( label29, 48 );
	UxPutX( label29, 10 );

	UxPutTranslations( tf_flux_tbl, FileSelectFlux );
	UxPutForeground( tf_flux_tbl, TextForeground );
	UxPutHighlightOnEnter( tf_flux_tbl, "true" );
	UxPutFontList( tf_flux_tbl, TextFont );
	UxPutBackground( tf_flux_tbl, TextBackground );
	UxPutHeight( tf_flux_tbl, 34 );
	UxPutWidth( tf_flux_tbl, 221 );
	UxPutY( tf_flux_tbl, 48 );
	UxPutX( tf_flux_tbl, 148 );

	UxPutForeground( label30, TextForeground );
	UxPutAlignment( label30, "alignment_beginning" );
	UxPutLabelString( label30, "Response curve :" );
	UxPutFontList( label30, TextFont );
	UxPutBackground( label30, LabelBackground );
	UxPutHeight( label30, 30 );
	UxPutWidth( label30, 126 );
	UxPutY( label30, 88 );
	UxPutX( label30, 10 );

	UxPutForeground( tf_output_resp, TextForeground );
	UxPutHighlightOnEnter( tf_output_resp, "true" );
	UxPutFontList( tf_output_resp, TextFont );
	UxPutBackground( tf_output_resp, TextBackground );
	UxPutHeight( tf_output_resp, 34 );
	UxPutWidth( tf_output_resp, 221 );
	UxPutY( tf_output_resp, 88 );
	UxPutX( tf_output_resp, 148 );

	UxPutIsAligned( rowColumn4, "true" );
	UxPutEntryAlignment( rowColumn4, "alignment_beginning" );
	UxPutBorderWidth( rowColumn4, 0 );
	UxPutShadowThickness( rowColumn4, 0 );
	UxPutLabelString( rowColumn4, "" );
	UxPutEntryBorder( rowColumn4, 0 );
	UxPutBackground( rowColumn4, "grey80" );
	UxPutRadioBehavior( rowColumn4, "true" );
	UxPutHeight( rowColumn4, 67 );
	UxPutWidth( rowColumn4, 82 );
	UxPutY( rowColumn4, 191 );
	UxPutX( rowColumn4, 233 );

	UxPutTranslations( rb_fityp_poly, transTable34 );
	UxPutIndicatorSize( rb_fityp_poly, 16 );
	UxPutHighlightOnEnter( rb_fityp_poly, "true" );
	UxPutForeground( rb_fityp_poly, TextForeground );
	UxPutSelectColor( rb_fityp_poly, SelectColor );
	UxPutSet( rb_fityp_poly, "true" );
	UxPutLabelString( rb_fityp_poly, "POLY" );
	UxPutFontList( rb_fityp_poly, TextFont );
	UxPutBackground( rb_fityp_poly, WindowBackground );
	UxPutHeight( rb_fityp_poly, 20 );
	UxPutWidth( rb_fityp_poly, 76 );
	UxPutY( rb_fityp_poly, 2 );
	UxPutX( rb_fityp_poly, 3 );

	UxPutTranslations( rb_fityp_spli, transTable34 );
	UxPutIndicatorSize( rb_fityp_spli, 16 );
	UxPutHighlightOnEnter( rb_fityp_spli, "true" );
	UxPutSet( rb_fityp_spli, "false" );
	UxPutForeground( rb_fityp_spli, TextForeground );
	UxPutSelectColor( rb_fityp_spli, SelectColor );
	UxPutLabelString( rb_fityp_spli, "SPLINE" );
	UxPutFontList( rb_fityp_spli, TextFont );
	UxPutBackground( rb_fityp_spli, WindowBackground );
	UxPutHeight( rb_fityp_spli, 66 );
	UxPutWidth( rb_fityp_spli, 76 );
	UxPutY( rb_fityp_spli, 34 );
	UxPutX( rb_fityp_spli, 3 );

	UxPutForeground( label33, TextForeground );
	UxPutAlignment( label33, "alignment_beginning" );
	UxPutLabelString( label33, "Fitting type" );
	UxPutFontList( label33, TextFont );
	UxPutBackground( label33, WindowBackground );
	UxPutHeight( label33, 22 );
	UxPutWidth( label33, 80 );
	UxPutY( label33, 164 );
	UxPutX( label33, 236 );

	UxPutOrientation( separatorGadget16, "vertical" );
	UxPutHeight( separatorGadget16, 84 );
	UxPutWidth( separatorGadget16, 12 );
	UxPutY( separatorGadget16, 180 );
	UxPutX( separatorGadget16, 218 );

	UxPutHeight( separatorGadget17, 8 );
	UxPutWidth( separatorGadget17, 104 );
	UxPutY( separatorGadget17, 260 );
	UxPutX( separatorGadget17, 224 );

	UxPutOrientation( separatorGadget18, "vertical" );
	UxPutHeight( separatorGadget18, 84 );
	UxPutWidth( separatorGadget18, 12 );
	UxPutY( separatorGadget18, 180 );
	UxPutX( separatorGadget18, 322 );

	UxPutIsAligned( rowColumn5, "true" );
	UxPutEntryAlignment( rowColumn5, "alignment_beginning" );
	UxPutBorderWidth( rowColumn5, 0 );
	UxPutShadowThickness( rowColumn5, 0 );
	UxPutLabelString( rowColumn5, "" );
	UxPutEntryBorder( rowColumn5, 0 );
	UxPutBackground( rowColumn5, "grey80" );
	UxPutRadioBehavior( rowColumn5, "true" );
	UxPutHeight( rowColumn5, 67 );
	UxPutWidth( rowColumn5, 82 );
	UxPutY( rowColumn5, 190 );
	UxPutX( rowColumn5, 348 );

	UxPutTranslations( rb_plotyp_rati, transTable34 );
	UxPutIndicatorSize( rb_plotyp_rati, 16 );
	UxPutHighlightOnEnter( rb_plotyp_rati, "true" );
	UxPutForeground( rb_plotyp_rati, TextForeground );
	UxPutSelectColor( rb_plotyp_rati, SelectColor );
	UxPutSet( rb_plotyp_rati, "true" );
	UxPutLabelString( rb_plotyp_rati, "RATIO" );
	UxPutFontList( rb_plotyp_rati, TextFont );
	UxPutBackground( rb_plotyp_rati, WindowBackground );
	UxPutHeight( rb_plotyp_rati, 20 );
	UxPutWidth( rb_plotyp_rati, 76 );
	UxPutY( rb_plotyp_rati, 0 );
	UxPutX( rb_plotyp_rati, -12 );

	UxPutTranslations( rb_plotyp_colo, transTable34 );
	UxPutIndicatorSize( rb_plotyp_colo, 16 );
	UxPutHighlightOnEnter( rb_plotyp_colo, "true" );
	UxPutSet( rb_plotyp_colo, "false" );
	UxPutForeground( rb_plotyp_colo, TextForeground );
	UxPutSelectColor( rb_plotyp_colo, SelectColor );
	UxPutLabelString( rb_plotyp_colo, "MAGNITUDE" );
	UxPutFontList( rb_plotyp_colo, TextFont );
	UxPutBackground( rb_plotyp_colo, WindowBackground );
	UxPutHeight( rb_plotyp_colo, 66 );
	UxPutWidth( rb_plotyp_colo, 76 );
	UxPutY( rb_plotyp_colo, 34 );
	UxPutX( rb_plotyp_colo, 3 );

	UxPutForeground( label34, TextForeground );
	UxPutAlignment( label34, "alignment_beginning" );
	UxPutLabelString( label34, "Fitting space" );
	UxPutFontList( label34, TextFont );
	UxPutBackground( label34, WindowBackground );
	UxPutHeight( label34, 22 );
	UxPutWidth( label34, 91 );
	UxPutY( label34, 164 );
	UxPutX( label34, 365 );

	UxPutOrientation( separatorGadget21, "vertical" );
	UxPutHeight( separatorGadget21, 84 );
	UxPutWidth( separatorGadget21, 12 );
	UxPutY( separatorGadget21, 180 );
	UxPutX( separatorGadget21, 334 );

	UxPutOrientation( separatorGadget22, "vertical" );
	UxPutHeight( separatorGadget22, 84 );
	UxPutWidth( separatorGadget22, 12 );
	UxPutY( separatorGadget22, 180 );
	UxPutX( separatorGadget22, 471 );

	UxPutHeight( separatorGadget23, 10 );
	UxPutWidth( separatorGadget23, 24 );
	UxPutY( separatorGadget23, 176 );
	UxPutX( separatorGadget23, 338 );

	UxPutHeight( separatorGadget24, 10 );
	UxPutWidth( separatorGadget24, 17 );
	UxPutY( separatorGadget24, 176 );
	UxPutX( separatorGadget24, 460 );

	UxPutHeight( separatorGadget25, 8 );
	UxPutWidth( separatorGadget25, 138 );
	UxPutY( separatorGadget25, 260 );
	UxPutX( separatorGadget25, 339 );

	UxPutHeight( separatorGadget19, 10 );
	UxPutWidth( separatorGadget19, 10 );
	UxPutY( separatorGadget19, 176 );
	UxPutX( separatorGadget19, 318 );

	UxPutHeight( separatorGadget20, 10 );
	UxPutWidth( separatorGadget20, 10 );
	UxPutY( separatorGadget20, 176 );
	UxPutX( separatorGadget20, 222 );

	UxPutForeground( label35, TextForeground );
	UxPutAlignment( label35, "alignment_beginning" );
	UxPutLabelString( label35, "Smoothing factor :" );
	UxPutFontList( label35, TextFont );
	UxPutBackground( label35, LabelBackground );
	UxPutHeight( label35, 30 );
	UxPutWidth( label35, 120 );
	UxPutY( label35, 226 );
	UxPutX( label35, 12 );

	UxPutForeground( tf_smooth, TextForeground );
	UxPutHighlightOnEnter( tf_smooth, "true" );
	UxPutFontList( tf_smooth, TextFont );
	UxPutBackground( tf_smooth, TextBackground );
	UxPutHeight( tf_smooth, 34 );
	UxPutWidth( tf_smooth, 80 );
	UxPutY( tf_smooth, 222 );
	UxPutX( tf_smooth, 136 );

	UxPutForeground( label36, TextForeground );
	UxPutAlignment( label36, "alignment_beginning" );
	UxPutLabelString( label36, "Fit degree :" );
	UxPutFontList( label36, TextFont );
	UxPutBackground( label36, LabelBackground );
	UxPutHeight( label36, 30 );
	UxPutWidth( label36, 120 );
	UxPutY( label36, 186 );
	UxPutX( label36, 12 );

	UxPutForeground( tf_fitd, TextForeground );
	UxPutHighlightOnEnter( tf_fitd, "true" );
	UxPutFontList( tf_fitd, TextFont );
	UxPutBackground( tf_fitd, TextBackground );
	UxPutHeight( tf_fitd, 34 );
	UxPutWidth( tf_fitd, 80 );
	UxPutY( tf_fitd, 182 );
	UxPutX( tf_fitd, 136 );

	UxPutForeground( label37, TextForeground );
	UxPutAlignment( label37, "alignment_beginning" );
	UxPutLabelString( label37, "FITTING PARAMETERS" );
	UxPutFontList( label37, TextFont );
	UxPutBackground( label37, LabelBackground );
	UxPutHeight( label37, 30 );
	UxPutWidth( label37, 180 );
	UxPutY( label37, 140 );
	UxPutX( label37, 4 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_FluxShell()
{
	/* Create the swidgets */

	FluxShell = UxCreateApplicationShell( "FluxShell", NO_PARENT );
	UxPutContext( FluxShell, UxFluxShellContext );

	form10 = UxCreateForm( "form10", FluxShell );
	form11 = UxCreateForm( "form11", form10 );
	pb_flux_extin = UxCreatePushButton( "pb_flux_extin", form11 );
	pushButton18 = UxCreatePushButton( "pushButton18", form11 );
	pb_flux_presp = UxCreatePushButton( "pb_flux_presp", form11 );
	pb_flux_integr = UxCreatePushButton( "pb_flux_integr", form11 );
	pb_flux_fit = UxCreatePushButton( "pb_flux_fit", form11 );
	pb_flux_pflux = UxCreatePushButton( "pb_flux_pflux", form11 );
	pb_flux_filter = UxCreatePushButton( "pb_flux_filter", form11 );
	pb_correct = UxCreatePushButton( "pb_correct", form11 );
	pb_edit_flux = UxCreatePushButton( "pb_edit_flux", form11 );
	shelp_flux = UxCreateText( "shelp_flux", form10 );
	separator8 = UxCreateSeparator( "separator8", form10 );
	separator9 = UxCreateSeparator( "separator9", form10 );
	label27 = UxCreateLabel( "label27", form10 );
	tf_extin_tbl = UxCreateTextField( "tf_extin_tbl", form10 );
	label29 = UxCreateLabel( "label29", form10 );
	tf_flux_tbl = UxCreateTextField( "tf_flux_tbl", form10 );
	label30 = UxCreateLabel( "label30", form10 );
	tf_output_resp = UxCreateTextField( "tf_output_resp", form10 );
	rowColumn4 = UxCreateRowColumn( "rowColumn4", form10 );
	rb_fityp_poly = UxCreateToggleButton( "rb_fityp_poly", rowColumn4 );
	rb_fityp_spli = UxCreateToggleButton( "rb_fityp_spli", rowColumn4 );
	label33 = UxCreateLabel( "label33", form10 );
	separatorGadget16 = UxCreateSeparatorGadget( "separatorGadget16", form10 );
	separatorGadget17 = UxCreateSeparatorGadget( "separatorGadget17", form10 );
	separatorGadget18 = UxCreateSeparatorGadget( "separatorGadget18", form10 );
	rowColumn5 = UxCreateRowColumn( "rowColumn5", form10 );
	rb_plotyp_rati = UxCreateToggleButton( "rb_plotyp_rati", rowColumn5 );
	rb_plotyp_colo = UxCreateToggleButton( "rb_plotyp_colo", rowColumn5 );
	label34 = UxCreateLabel( "label34", form10 );
	separatorGadget21 = UxCreateSeparatorGadget( "separatorGadget21", form10 );
	separatorGadget22 = UxCreateSeparatorGadget( "separatorGadget22", form10 );
	separatorGadget23 = UxCreateSeparatorGadget( "separatorGadget23", form10 );
	separatorGadget24 = UxCreateSeparatorGadget( "separatorGadget24", form10 );
	separatorGadget25 = UxCreateSeparatorGadget( "separatorGadget25", form10 );
	separatorGadget19 = UxCreateSeparatorGadget( "separatorGadget19", form10 );
	separatorGadget20 = UxCreateSeparatorGadget( "separatorGadget20", form10 );
	label35 = UxCreateLabel( "label35", form10 );
	tf_smooth = UxCreateTextField( "tf_smooth", form10 );
	label36 = UxCreateLabel( "label36", form10 );
	tf_fitd = UxCreateTextField( "tf_fitd", form10 );
	label37 = UxCreateLabel( "label37", form10 );

	_Uxinit_FluxShell();

	/* Create the X widgets */

	UxCreateWidget( FluxShell );
	UxCreateWidget( form10 );
	UxCreateWidget( form11 );
	UxCreateWidget( pb_flux_extin );
	UxCreateWidget( pushButton18 );
	UxCreateWidget( pb_flux_presp );
	UxCreateWidget( pb_flux_integr );
	UxCreateWidget( pb_flux_fit );
	UxCreateWidget( pb_flux_pflux );
	UxCreateWidget( pb_flux_filter );
	UxCreateWidget( pb_correct );
	UxCreateWidget( pb_edit_flux );
	UxCreateWidget( shelp_flux );
	UxCreateWidget( separator8 );
	UxCreateWidget( separator9 );
	UxCreateWidget( label27 );
	UxCreateWidget( tf_extin_tbl );
	UxCreateWidget( label29 );
	UxCreateWidget( tf_flux_tbl );
	UxCreateWidget( label30 );
	UxCreateWidget( tf_output_resp );
	UxCreateWidget( rowColumn4 );
	UxCreateWidget( rb_fityp_poly );
	UxCreateWidget( rb_fityp_spli );
	UxCreateWidget( label33 );
	UxCreateWidget( separatorGadget16 );
	UxCreateWidget( separatorGadget17 );
	UxCreateWidget( separatorGadget18 );
	UxCreateWidget( rowColumn5 );
	UxCreateWidget( rb_plotyp_rati );
	UxCreateWidget( rb_plotyp_colo );
	UxCreateWidget( label34 );
	UxCreateWidget( separatorGadget21 );
	UxCreateWidget( separatorGadget22 );
	UxCreateWidget( separatorGadget23 );
	UxCreateWidget( separatorGadget24 );
	UxCreateWidget( separatorGadget25 );
	UxCreateWidget( separatorGadget19 );
	UxCreateWidget( separatorGadget20 );
	UxCreateWidget( label35 );
	UxCreateWidget( tf_smooth );
	UxCreateWidget( label36 );
	UxCreateWidget( tf_fitd );
	UxCreateWidget( label37 );

	UxAddCallback( pb_flux_extin, XmNactivateCallback,
			activateCB_pb_flux_extin,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( pushButton18, XmNactivateCallback,
			activateCB_pushButton18,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( pb_flux_presp, XmNactivateCallback,
			activateCB_pb_flux_presp,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( pb_flux_integr, XmNactivateCallback,
			activateCB_pb_flux_integr,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( pb_flux_fit, XmNactivateCallback,
			activateCB_pb_flux_fit,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( pb_flux_pflux, XmNactivateCallback,
			activateCB_pb_flux_pflux,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( pb_flux_filter, XmNactivateCallback,
			activateCB_pb_flux_filter,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( pb_correct, XmNactivateCallback,
			activateCB_pb_correct,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( pb_edit_flux, XmNactivateCallback,
			activateCB_pb_edit_flux,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( tf_extin_tbl, XmNlosingFocusCallback,
			losingFocusCB_tf_extin_tbl,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( tf_flux_tbl, XmNlosingFocusCallback,
			losingFocusCB_tf_flux_tbl,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( tf_output_resp, XmNlosingFocusCallback,
			losingFocusCB_tf_output_resp,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( rb_fityp_poly, XmNarmCallback,
			armCB_rb_fityp_poly,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( rb_fityp_spli, XmNarmCallback,
			armCB_rb_fityp_spli,
			(XtPointer) UxFluxShellContext );
	UxAddCallback( rb_fityp_spli, XmNvalueChangedCallback,
			valueChangedCB_rb_fityp_spli,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( rb_plotyp_rati, XmNarmCallback,
			armCB_rb_plotyp_rati,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( rb_plotyp_colo, XmNarmCallback,
			armCB_rb_plotyp_colo,
			(XtPointer) UxFluxShellContext );
	UxAddCallback( rb_plotyp_colo, XmNvalueChangedCallback,
			valueChangedCB_rb_plotyp_colo,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( tf_smooth, XmNlosingFocusCallback,
			losingFocusCB_tf_smooth,
			(XtPointer) UxFluxShellContext );

	UxAddCallback( tf_fitd, XmNlosingFocusCallback,
			losingFocusCB_tf_fitd,
			(XtPointer) UxFluxShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( FluxShell );

	return ( FluxShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_FluxShell()
{
	swidget                 rtrn;
	_UxCFluxShell           *UxContext;

	UxFluxShellContext = UxContext =
		(_UxCFluxShell *) UxMalloc( sizeof(_UxCFluxShell) );

	rtrn = _Uxbuild_FluxShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_FluxShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_FluxShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

