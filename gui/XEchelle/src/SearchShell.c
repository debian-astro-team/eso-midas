/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	SearchShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxSearchShell;
	swidget	Uxform2;
	swidget	Uxlabel9;
	swidget	Uxtf_ywidth1;
	swidget	Uxlabel10;
	swidget	Uxlabel11;
	swidget	Uxtf_ystep1;
	swidget	Uxtf_width1;
	swidget	Uxlabel12;
	swidget	Uxtf_thres1;
	swidget	Uxform4;
	swidget	Uxpb_search_search1;
	swidget	UxpushButton1;
	swidget	Uxpb_search_plot6;
	swidget	Uxpb_search_plot7;
	swidget	Uxpb_search_plot8;
	swidget	Uxshelp_search1;
	swidget	Uxseparator2;
	swidget	Uxseparator3;
	swidget	Uxlabel13;
	swidget	Uxlabel67;
	swidget	Uxtf_thres4;
	swidget	Uxpb_main_search16;
	swidget	Uxlabel68;
	swidget	Uxmenu2_p6;
	swidget	Uxmn_tol_angstroms6;
	swidget	Uxmn_tol_pixels6;
	swidget	Uxmn_tol6;
	swidget	Uxtf_thres12;
} _UxCSearchShell;

#define SearchShell             UxSearchShellContext->UxSearchShell
#define form2                   UxSearchShellContext->Uxform2
#define label9                  UxSearchShellContext->Uxlabel9
#define tf_ywidth1              UxSearchShellContext->Uxtf_ywidth1
#define label10                 UxSearchShellContext->Uxlabel10
#define label11                 UxSearchShellContext->Uxlabel11
#define tf_ystep1               UxSearchShellContext->Uxtf_ystep1
#define tf_width1               UxSearchShellContext->Uxtf_width1
#define label12                 UxSearchShellContext->Uxlabel12
#define tf_thres1               UxSearchShellContext->Uxtf_thres1
#define form4                   UxSearchShellContext->Uxform4
#define pb_search_search1       UxSearchShellContext->Uxpb_search_search1
#define pushButton1             UxSearchShellContext->UxpushButton1
#define pb_search_plot6         UxSearchShellContext->Uxpb_search_plot6
#define pb_search_plot7         UxSearchShellContext->Uxpb_search_plot7
#define pb_search_plot8         UxSearchShellContext->Uxpb_search_plot8
#define shelp_search1           UxSearchShellContext->Uxshelp_search1
#define separator2              UxSearchShellContext->Uxseparator2
#define separator3              UxSearchShellContext->Uxseparator3
#define label13                 UxSearchShellContext->Uxlabel13
#define label67                 UxSearchShellContext->Uxlabel67
#define tf_thres4               UxSearchShellContext->Uxtf_thres4
#define pb_main_search16        UxSearchShellContext->Uxpb_main_search16
#define label68                 UxSearchShellContext->Uxlabel68
#define menu2_p6                UxSearchShellContext->Uxmenu2_p6
#define mn_tol_angstroms6       UxSearchShellContext->Uxmn_tol_angstroms6
#define mn_tol_pixels6          UxSearchShellContext->Uxmn_tol_pixels6
#define mn_tol6                 UxSearchShellContext->Uxmn_tol6
#define tf_thres12              UxSearchShellContext->Uxtf_thres12

static _UxCSearchShell	*UxSearchShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter(), GetExtendedHelp(), SelectList();

extern int RadioSet(), PopupList(), AppendDialogText();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable13 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_SearchShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	DisplayShortHelp(UxWidget);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	/*
	UxPutText(UxFindSwidget("shelp_main"), "");
	UxPutText(UxFindSwidget("shelp_search"), "");
	UxPutText(UxFindSwidget("shelp_calib"), "");
	UxPutText(UxFindSwidget("shelp_rebin"), "");
	UxPutText(UxFindSwidget("shelp_extract"), "");
	UxPutText(UxFindSwidget("shelp_flux"), "");
	*/
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxSearchShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxSearchShellContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxSearchShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	losingFocusCB_tf_ywidth1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_ystep1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_width1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_search1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_pushButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("SearchShell"));
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_angstroms6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxSearchShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_pixels6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxSearchShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSearchShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSearchShellContext;
	UxSearchShellContext = UxContext =
			(_UxCSearchShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSearchShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_SearchShell()
{
	UxPutBackground( SearchShell, WindowBackground );
	UxPutGeometry( SearchShell, "+10+60" );
	UxPutKeyboardFocusPolicy( SearchShell, "pointer" );
	UxPutTitle( SearchShell, "XEchelle: Search Arc Lines" );
	UxPutHeight( SearchShell, 350 );
	UxPutWidth( SearchShell, 620 );
	UxPutY( SearchShell, 53 );
	UxPutX( SearchShell, 370 );

	UxPutTextFontList( form2, TextFont );
	UxPutLabelFontList( form2, TextFont );
	UxPutBackground( form2, WindowBackground );
	UxPutHeight( form2, 350 );
	UxPutWidth( form2, 666 );
	UxPutY( form2, 0 );
	UxPutX( form2, -16 );
	UxPutUnitType( form2, "pixels" );
	UxPutResizePolicy( form2, "resize_none" );

	UxPutForeground( label9, TextForeground );
	UxPutAlignment( label9, "alignment_beginning" );
	UxPutLabelString( label9, "Width:" );
	UxPutFontList( label9, TextFont );
	UxPutBackground( label9, LabelBackground );
	UxPutHeight( label9, 30 );
	UxPutWidth( label9, 124 );
	UxPutY( label9, 172 );
	UxPutX( label9, 354 );

	UxPutForeground( tf_ywidth1, TextForeground );
	UxPutHighlightOnEnter( tf_ywidth1, "true" );
	UxPutFontList( tf_ywidth1, TextFont );
	UxPutBackground( tf_ywidth1, TextBackground );
	UxPutHeight( tf_ywidth1, 35 );
	UxPutWidth( tf_ywidth1, 120 );
	UxPutY( tf_ywidth1, 162 );
	UxPutX( tf_ywidth1, 480 );

	UxPutForeground( label10, TextForeground );
	UxPutAlignment( label10, "alignment_beginning" );
	UxPutLabelString( label10, "Threshold:" );
	UxPutFontList( label10, TextFont );
	UxPutBackground( label10, LabelBackground );
	UxPutHeight( label10, 30 );
	UxPutWidth( label10, 120 );
	UxPutY( label10, 120 );
	UxPutX( label10, 354 );

	UxPutForeground( label11, TextForeground );
	UxPutAlignment( label11, "alignment_beginning" );
	UxPutLabelString( label11, "Slit:" );
	UxPutFontList( label11, TextFont );
	UxPutBackground( label11, LabelBackground );
	UxPutHeight( label11, 30 );
	UxPutWidth( label11, 120 );
	UxPutY( label11, 124 );
	UxPutX( label11, 28 );

	UxPutForeground( tf_ystep1, TextForeground );
	UxPutHighlightOnEnter( tf_ystep1, "true" );
	UxPutFontList( tf_ystep1, TextFont );
	UxPutBackground( tf_ystep1, TextBackground );
	UxPutHeight( tf_ystep1, 35 );
	UxPutWidth( tf_ystep1, 120 );
	UxPutY( tf_ystep1, 116 );
	UxPutX( tf_ystep1, 480 );

	UxPutForeground( tf_width1, TextForeground );
	UxPutHighlightOnEnter( tf_width1, "true" );
	UxPutFontList( tf_width1, TextFont );
	UxPutBackground( tf_width1, TextBackground );
	UxPutHeight( tf_width1, 35 );
	UxPutWidth( tf_width1, 120 );
	UxPutY( tf_width1, 170 );
	UxPutX( tf_width1, 170 );

	UxPutForeground( label12, TextForeground );
	UxPutAlignment( label12, "alignment_beginning" );
	UxPutLabelString( label12, "Extraction method:" );
	UxPutFontList( label12, TextFont );
	UxPutBackground( label12, LabelBackground );
	UxPutHeight( label12, 30 );
	UxPutWidth( label12, 120 );
	UxPutY( label12, 80 );
	UxPutX( label12, 28 );

	UxPutForeground( tf_thres1, TextForeground );
	UxPutHighlightOnEnter( tf_thres1, "true" );
	UxPutFontList( tf_thres1, TextFont );
	UxPutBackground( tf_thres1, TextBackground );
	UxPutHeight( tf_thres1, 35 );
	UxPutWidth( tf_thres1, 120 );
	UxPutY( tf_thres1, 182 );
	UxPutX( tf_thres1, 238 );

	UxPutBackground( form4, ButtonBackground );
	UxPutHeight( form4, 40 );
	UxPutWidth( form4, 666 );
	UxPutY( form4, 304 );
	UxPutX( form4, 0 );
	UxPutResizePolicy( form4, "resize_none" );

	UxPutLabelString( pb_search_search1, "Extract and Search" );
	UxPutForeground( pb_search_search1, ApplyForeground );
	UxPutFontList( pb_search_search1, BoldTextFont );
	UxPutBackground( pb_search_search1, ButtonBackground );
	UxPutHeight( pb_search_search1, 30 );
	UxPutWidth( pb_search_search1, 184 );
	UxPutY( pb_search_search1, 4 );
	UxPutX( pb_search_search1, 8 );

	UxPutLabelString( pushButton1, "Cancel" );
	UxPutForeground( pushButton1, CancelForeground );
	UxPutFontList( pushButton1, BoldTextFont );
	UxPutBackground( pushButton1, ButtonBackground );
	UxPutHeight( pushButton1, 30 );
	UxPutWidth( pushButton1, 86 );
	UxPutY( pushButton1, 4 );
	UxPutX( pushButton1, 528 );

	UxPutLabelString( pb_search_plot6, "Help..." );
	UxPutForeground( pb_search_plot6, ButtonForeground );
	UxPutFontList( pb_search_plot6, BoldTextFont );
	UxPutBackground( pb_search_plot6, ButtonBackground );
	UxPutHeight( pb_search_plot6, 30 );
	UxPutWidth( pb_search_plot6, 86 );
	UxPutY( pb_search_plot6, 4 );
	UxPutX( pb_search_plot6, 432 );

	UxPutLabelString( pb_search_plot7, "Load Search" );
	UxPutForeground( pb_search_plot7, ButtonForeground );
	UxPutFontList( pb_search_plot7, BoldTextFont );
	UxPutBackground( pb_search_plot7, ButtonBackground );
	UxPutHeight( pb_search_plot7, 30 );
	UxPutWidth( pb_search_plot7, 118 );
	UxPutY( pb_search_plot7, 4 );
	UxPutX( pb_search_plot7, 198 );

	UxPutLabelString( pb_search_plot8, "Save..." );
	UxPutForeground( pb_search_plot8, ButtonForeground );
	UxPutFontList( pb_search_plot8, BoldTextFont );
	UxPutBackground( pb_search_plot8, ButtonBackground );
	UxPutHeight( pb_search_plot8, 30 );
	UxPutWidth( pb_search_plot8, 86 );
	UxPutY( pb_search_plot8, 4 );
	UxPutX( pb_search_plot8, 332 );

	UxPutFontList( shelp_search1, TextFont );
	UxPutEditable( shelp_search1, "false" );
	UxPutCursorPositionVisible( shelp_search1, "false" );
	UxPutBackground( shelp_search1, SHelpBackground );
	UxPutHeight( shelp_search1, 50 );
	UxPutWidth( shelp_search1, 664 );
	UxPutY( shelp_search1, 244 );
	UxPutX( shelp_search1, 2 );

	UxPutBackground( separator2, WindowBackground );
	UxPutHeight( separator2, 10 );
	UxPutWidth( separator2, 664 );
	UxPutY( separator2, 234 );
	UxPutX( separator2, 0 );

	UxPutBackground( separator3, WindowBackground );
	UxPutHeight( separator3, 10 );
	UxPutWidth( separator3, 672 );
	UxPutY( separator3, 292 );
	UxPutX( separator3, -2 );

	UxPutForeground( label13, TextForeground );
	UxPutAlignment( label13, "alignment_beginning" );
	UxPutLabelString( label13, "Offset:" );
	UxPutFontList( label13, TextFont );
	UxPutBackground( label13, LabelBackground );
	UxPutHeight( label13, 30 );
	UxPutWidth( label13, 120 );
	UxPutY( label13, 174 );
	UxPutX( label13, 28 );

	UxPutForeground( label67, TextForeground );
	UxPutAlignment( label67, "alignment_beginning" );
	UxPutLabelString( label67, "Wavelength Calibration Frame:" );
	UxPutFontList( label67, TextFont );
	UxPutBackground( label67, LabelBackground );
	UxPutHeight( label67, 30 );
	UxPutWidth( label67, 216 );
	UxPutY( label67, 14 );
	UxPutX( label67, 20 );

	UxPutForeground( tf_thres4, TextForeground );
	UxPutHighlightOnEnter( tf_thres4, "true" );
	UxPutFontList( tf_thres4, TextFont );
	UxPutBackground( tf_thres4, TextBackground );
	UxPutHeight( tf_thres4, 35 );
	UxPutWidth( tf_thres4, 272 );
	UxPutY( tf_thres4, 12 );
	UxPutX( tf_thres4, 242 );

	UxPutWidth( pb_main_search16, 46 );
	UxPutRecomputeSize( pb_main_search16, "true" );
	UxPutLabelString( pb_main_search16, "..." );
	UxPutForeground( pb_main_search16, ButtonForeground );
	UxPutFontList( pb_main_search16, BoldTextFont );
	UxPutBackground( pb_main_search16, ButtonBackground );
	UxPutHeight( pb_main_search16, 28 );
	UxPutY( pb_main_search16, 14 );
	UxPutX( pb_main_search16, 548 );

	UxPutForeground( label68, TextForeground );
	UxPutAlignment( label68, "alignment_beginning" );
	UxPutLabelString( label68, "Search method" );
	UxPutFontList( label68, TextFont );
	UxPutBackground( label68, LabelBackground );
	UxPutHeight( label68, 30 );
	UxPutWidth( label68, 120 );
	UxPutY( label68, 82 );
	UxPutX( label68, 354 );

	UxPutX( menu2_p6, 520 );
	UxPutWidth( menu2_p6, 120 );
	UxPutForeground( menu2_p6, TextForeground );
	UxPutBackground( menu2_p6, WindowBackground );
	UxPutRowColumnType( menu2_p6, "menu_pulldown" );

	UxPutX( mn_tol_angstroms6, 520 );
	UxPutWidth( mn_tol_angstroms6, 120 );
	UxPutFontList( mn_tol_angstroms6, TextFont );
	UxPutLabelString( mn_tol_angstroms6, " Gaussian " );

	UxPutX( mn_tol_pixels6, 520 );
	UxPutWidth( mn_tol_pixels6, 120 );
	UxPutFontList( mn_tol_pixels6, TextFont );
	UxPutLabelString( mn_tol_pixels6, " Gravity " );

	UxPutTranslations( mn_tol6, transTable13 );
	UxPutWidth( mn_tol6, 120 );
	UxPutLabelString( mn_tol6, " " );
	UxPutSpacing( mn_tol6, 0 );
	UxPutMarginWidth( mn_tol6, 0 );
	UxPutForeground( mn_tol6, TextForeground );
	UxPutBackground( mn_tol6, WindowBackground );
	UxPutY( mn_tol6, 76 );
	UxPutX( mn_tol6, 474 );
	UxPutRowColumnType( mn_tol6, "menu_option" );

	UxPutHighlightColor( tf_thres12, WindowBackground );
	UxPutEditable( tf_thres12, "false" );
	UxPutForeground( tf_thres12, TextForeground );
	UxPutHighlightOnEnter( tf_thres12, "true" );
	UxPutFontList( tf_thres12, TextFont );
	UxPutBackground( tf_thres12, WindowBackground );
	UxPutHeight( tf_thres12, 35 );
	UxPutWidth( tf_thres12, 120 );
	UxPutY( tf_thres12, 80 );
	UxPutX( tf_thres12, 170 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_SearchShell()
{
	/* Create the swidgets */

	SearchShell = UxCreateApplicationShell( "SearchShell", NO_PARENT );
	UxPutContext( SearchShell, UxSearchShellContext );

	form2 = UxCreateForm( "form2", SearchShell );
	label9 = UxCreateLabel( "label9", form2 );
	tf_ywidth1 = UxCreateTextField( "tf_ywidth1", form2 );
	label10 = UxCreateLabel( "label10", form2 );
	label11 = UxCreateLabel( "label11", form2 );
	tf_ystep1 = UxCreateTextField( "tf_ystep1", form2 );
	tf_width1 = UxCreateTextField( "tf_width1", form2 );
	label12 = UxCreateLabel( "label12", form2 );
	tf_thres1 = UxCreateTextField( "tf_thres1", form2 );
	form4 = UxCreateForm( "form4", form2 );
	pb_search_search1 = UxCreatePushButton( "pb_search_search1", form4 );
	pushButton1 = UxCreatePushButton( "pushButton1", form4 );
	pb_search_plot6 = UxCreatePushButton( "pb_search_plot6", form4 );
	pb_search_plot7 = UxCreatePushButton( "pb_search_plot7", form4 );
	pb_search_plot8 = UxCreatePushButton( "pb_search_plot8", form4 );
	shelp_search1 = UxCreateText( "shelp_search1", form2 );
	separator2 = UxCreateSeparator( "separator2", form2 );
	separator3 = UxCreateSeparator( "separator3", form2 );
	label13 = UxCreateLabel( "label13", form2 );
	label67 = UxCreateLabel( "label67", form2 );
	tf_thres4 = UxCreateTextField( "tf_thres4", form2 );
	pb_main_search16 = UxCreatePushButton( "pb_main_search16", form2 );
	label68 = UxCreateLabel( "label68", form2 );
	menu2_p6 = UxCreateRowColumn( "menu2_p6", form2 );
	mn_tol_angstroms6 = UxCreatePushButtonGadget( "mn_tol_angstroms6", menu2_p6 );
	mn_tol_pixels6 = UxCreatePushButtonGadget( "mn_tol_pixels6", menu2_p6 );
	mn_tol6 = UxCreateRowColumn( "mn_tol6", form2 );
	tf_thres12 = UxCreateTextField( "tf_thres12", form2 );

	_Uxinit_SearchShell();

	/* Create the X widgets */

	UxCreateWidget( SearchShell );
	UxCreateWidget( form2 );
	UxPutLeftOffset( label9, 350 );
	UxPutLeftAttachment( label9, "attach_form" );
	UxPutTopOffset( label9, 160 );
	UxPutTopAttachment( label9, "attach_form" );
	UxCreateWidget( label9 );

	UxPutLeftOffset( tf_ywidth1, 480 );
	UxPutLeftAttachment( tf_ywidth1, "attach_form" );
	UxPutTopOffset( tf_ywidth1, 160 );
	UxPutTopAttachment( tf_ywidth1, "attach_form" );
	UxCreateWidget( tf_ywidth1 );

	UxPutLeftOffset( label10, 350 );
	UxPutLeftAttachment( label10, "attach_form" );
	UxPutTopOffset( label10, 120 );
	UxPutTopAttachment( label10, "attach_form" );
	UxCreateWidget( label10 );

	UxPutLeftOffset( label11, 20 );
	UxPutLeftAttachment( label11, "attach_form" );
	UxPutTopOffset( label11, 120 );
	UxPutTopAttachment( label11, "attach_form" );
	UxCreateWidget( label11 );

	UxPutLeftOffset( tf_ystep1, 480 );
	UxPutLeftAttachment( tf_ystep1, "attach_form" );
	UxPutTopOffset( tf_ystep1, 120 );
	UxPutTopAttachment( tf_ystep1, "attach_form" );
	UxCreateWidget( tf_ystep1 );

	UxPutLeftOffset( tf_width1, 170 );
	UxPutLeftAttachment( tf_width1, "attach_form" );
	UxPutTopOffset( tf_width1, 160 );
	UxPutTopAttachment( tf_width1, "attach_form" );
	UxCreateWidget( tf_width1 );

	UxPutLeftOffset( label12, 20 );
	UxPutLeftAttachment( label12, "attach_form" );
	UxPutTopOffset( label12, 80 );
	UxPutTopAttachment( label12, "attach_form" );
	UxCreateWidget( label12 );

	UxPutLeftOffset( tf_thres1, 170 );
	UxPutLeftAttachment( tf_thres1, "attach_form" );
	UxPutTopOffset( tf_thres1, 120 );
	UxPutTopAttachment( tf_thres1, "attach_form" );
	UxCreateWidget( tf_thres1 );

	UxCreateWidget( form4 );
	UxCreateWidget( pb_search_search1 );
	UxCreateWidget( pushButton1 );
	UxCreateWidget( pb_search_plot6 );
	UxCreateWidget( pb_search_plot7 );
	UxCreateWidget( pb_search_plot8 );
	UxPutRightOffset( shelp_search1, 5 );
	UxPutRightAttachment( shelp_search1, "attach_form" );
	UxPutLeftOffset( shelp_search1, 5 );
	UxPutLeftAttachment( shelp_search1, "attach_form" );
	UxCreateWidget( shelp_search1 );

	UxPutRightOffset( separator2, 5 );
	UxPutRightAttachment( separator2, "attach_form" );
	UxPutLeftOffset( separator2, 5 );
	UxPutLeftAttachment( separator2, "attach_form" );
	UxCreateWidget( separator2 );

	UxPutRightOffset( separator3, 5 );
	UxPutRightAttachment( separator3, "attach_form" );
	UxPutLeftOffset( separator3, 5 );
	UxPutLeftAttachment( separator3, "attach_form" );
	UxCreateWidget( separator3 );

	UxPutLeftOffset( label13, 20 );
	UxPutLeftAttachment( label13, "attach_form" );
	UxPutTopOffset( label13, 160 );
	UxPutTopAttachment( label13, "attach_form" );
	UxCreateWidget( label13 );

	UxPutLeftOffset( label67, 20 );
	UxPutLeftAttachment( label67, "attach_form" );
	UxPutTopOffset( label67, 20 );
	UxPutTopAttachment( label67, "attach_form" );
	UxPutBottomOffset( label67, 10 );
	UxPutBottomAttachment( label67, "attach_none" );
	UxCreateWidget( label67 );

	UxPutTopOffset( tf_thres4, 20 );
	UxPutTopAttachment( tf_thres4, "attach_form" );
	UxPutBottomOffset( tf_thres4, 10 );
	UxPutBottomAttachment( tf_thres4, "attach_none" );
	UxCreateWidget( tf_thres4 );

	UxPutTopOffset( pb_main_search16, 20 );
	UxPutTopAttachment( pb_main_search16, "attach_form" );
	UxPutBottomOffset( pb_main_search16, 10 );
	UxPutBottomAttachment( pb_main_search16, "attach_none" );
	UxCreateWidget( pb_main_search16 );

	UxPutLeftOffset( label68, 350 );
	UxPutLeftAttachment( label68, "attach_form" );
	UxPutTopOffset( label68, 80 );
	UxPutTopAttachment( label68, "attach_form" );
	UxCreateWidget( label68 );

	UxCreateWidget( menu2_p6 );
	UxCreateWidget( mn_tol_angstroms6 );
	UxCreateWidget( mn_tol_pixels6 );
	UxPutLeftOffset( mn_tol6, 480 );
	UxPutLeftAttachment( mn_tol6, "attach_form" );
	UxPutTopOffset( mn_tol6, 80 );
	UxPutSubMenuId( mn_tol6, "menu2_p6" );
	UxCreateWidget( mn_tol6 );

	UxCreateWidget( tf_thres12 );

	UxAddCallback( tf_ywidth1, XmNlosingFocusCallback,
			losingFocusCB_tf_ywidth1,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( tf_ystep1, XmNlosingFocusCallback,
			losingFocusCB_tf_ystep1,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( tf_width1, XmNlosingFocusCallback,
			losingFocusCB_tf_width1,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( tf_thres1, XmNlosingFocusCallback,
			losingFocusCB_tf_thres1,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( pb_search_search1, XmNactivateCallback,
			activateCB_pb_search_search1,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( pushButton1, XmNactivateCallback,
			activateCB_pushButton1,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( pb_search_plot6, XmNactivateCallback,
			activateCB_pb_search_plot6,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( pb_search_plot7, XmNactivateCallback,
			activateCB_pb_search_plot7,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( pb_search_plot8, XmNactivateCallback,
			activateCB_pb_search_plot8,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( tf_thres4, XmNlosingFocusCallback,
			losingFocusCB_tf_thres4,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( pb_main_search16, XmNactivateCallback,
			activateCB_pb_main_search16,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( mn_tol_angstroms6, XmNactivateCallback,
			activateCB_mn_tol_angstroms6,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( mn_tol_pixels6, XmNactivateCallback,
			activateCB_mn_tol_pixels6,
			(XtPointer) UxSearchShellContext );

	UxAddCallback( tf_thres12, XmNlosingFocusCallback,
			losingFocusCB_tf_thres12,
			(XtPointer) UxSearchShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( SearchShell );

	return ( SearchShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_SearchShell()
{
	swidget                 rtrn;
	_UxCSearchShell         *UxContext;

	UxSearchShellContext = UxContext =
		(_UxCSearchShell *) UxMalloc( sizeof(_UxCSearchShell) );

	rtrn = _Uxbuild_SearchShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_SearchShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "WriteHelp", action_WriteHelp },
				{ "ClearHelp", action_ClearHelp },
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_SearchShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

