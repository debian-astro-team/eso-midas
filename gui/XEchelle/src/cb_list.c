/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       cb_list.c                                  */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    XLong, Spectroscopy, Long-Slit             */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Creation  17-MAR-1993   
 
    090825	laast modif                                */
/* ------------------------------------------------------- */

#include <gl_defs.h>
#include <xm_defs.h>
#include <main_defs.h>
#include <spec_comm.h>
#include <UxLib.h>

#define DIR_DELIMITER	'/'

extern int AppendDialogText(), exist_airmass();

extern void  InitSession(), SetFileList(), WriteKeyword(), UpdateDescriptors();



static char FileIn[MAXLINE], FileOut[MAXLINE];
static int PopupDialogWindow, PopupExtinWindow;
static float Airmass;

int PopupList( list_type )
int list_type;
{
    int strip = 1;  /* strip off the directories */

    extern swidget FileListInterface;
    extern Widget FileListWidget;
    extern char  DirSpecs[];

    ListType = list_type;

    switch ( list_type ) {
	case LIST_WLC:
	    SET_LIST_TITLE("Enter calibration frame");
       	    strcpy(DirSpecs, "*.bdf"); 
	    break;
	case LIST_REBIN_RBR:
	case LIST_REBIN_2D:
	case LIST_REBIN_TBL:
	case LIST_EXTIN:
	case LIST_INTEGR:
	case LIST_FLUX_FILT:
	case LIST_FIT_SKY:
	case LIST_EXT_AVER:
	case LIST_EXT_WEIGHT:
	case LIST_CORRECT:
	    SET_LIST_TITLE("Enter input image");
       	    strcpy(DirSpecs, "*.bdf");
	    break;
	case LIST_LOAD_IMA:
	    SET_LIST_TITLE("Enter image to load");
       	    strcpy(DirSpecs, "*.bdf");
	    break;
	case LIST_SESSION:
	    SET_LIST_TITLE("Enter parameters table");
       	    strcpy(DirSpecs, "*ORDE.tbl");
	    break;
	case LIST_BROWSER:
	    SET_LIST_TITLE("MIDAS browser");
	    /* DirSpecs[] was setted in the Browse menu callback first time */
	    strip = 0; /* keep the directories */
	    break;
	default:
	    break;
    }

    SetFileList(FileListWidget, strip, DirSpecs);
    UxPopupInterface(FileListInterface, exclusive_grab);
return 0;
}

void CallbackList( choice )
char *choice;
{
    char command[512],  str[256];
    char *sky;
    float fval;
    int i;
    int popdown_filelist = TRUE;
    
    extern swidget FileListInterface;
    extern swidget TextFieldSwidget;
    extern char  DirSpecs[];
    
    PopupDialogWindow = PopupExtinWindow = FALSE;
    Airmass = 1.0;
    command[0] = '\0';

    switch ( ListType ) {
        case LIST_LOAD_IMA:
     	    sprintf(command, "%s %s", "LOAD/IMAGE", choice);
    	    break;
        case LIST_SESSION:
            choice[strlen(choice)-8] = '\0';
            InitSession(choice);
            sprintf(command, "%s%s", C_INIT, choice);
    	    break;
        case LIST_WLC:
            UpdateDescriptors(choice);
            strcpy(command, C_SEARCH);
    	    break;
        case LIST_LINCAT:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Lincat, choice);
            WriteKeyword(Lincat, K_LINCAT);
    	    break;
        case LIST_GUESS:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Guess, choice);
            WriteKeyword(Guess, K_GUESS);
    	    break;
        case LIST_REBIN_RBR:
        case LIST_REBIN_2D:
        case LIST_REBIN_TBL:
            /*generation of the output image name */
            for ( i = 0; choice[i] != '.' && choice[i] != '\0'; i++ )
            	;
            strncpy(FileOut, choice, i);
            FileOut[i] = '\0';
            sprintf(FileOut, "%s_reb", FileOut);
	    strcpy(FileIn, choice);
	    PopupDialogWindow = TRUE;
	    break;
        case LIST_EXTIN_TBL:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Extab, choice);
            WriteKeyword(Extab, K_EXTAB);
    	    break;
        case LIST_FLUX_TBL:
            UxPutValue(TextFieldSwidget, choice);
            strcpy(Fluxtab, choice);
            WriteKeyword(Fluxtab, K_FLUXTAB);
    	    break;
        case LIST_INTEGR:
     	    sprintf(command, "%s%s", C_INTEGR, choice);
    	    break;
        case LIST_FLUX_FILT:
     	    sprintf(command, "%s%s", C_FLUX_FILT, choice);
    	    break;
        case LIST_EXTIN:
            /* generation of the output image name */
            for ( i = 0; choice[i] != '.' && choice[i] != '\0'; i++ )
             	;
            strncpy(FileOut, choice, i);
            FileOut[i] = '\0';
            sprintf(FileOut, "%s_ext", FileOut);
	    strcpy(FileIn, choice);
	    PopupExtinWindow = TRUE;
    	    break;
        case LIST_FIT_SKY:
    	    sky = XmTextGetString(UxGetWidget(UxFindSwidget("tf_sky")));
     	    sprintf(command, "%s%s %s", C_EXTR_FIT_SKY, choice, sky);
    	    XtFree(sky);
    	    break;
        case LIST_EXT_AVER:
        case LIST_EXT_WEIGHT:
            /* generation of the output image name */
            for ( i = 0; choice[i] != '.' && choice[i] != '\0'; i++ )
             	;
            strncpy(FileOut, choice, i);
            FileOut[i] = '\0';
            sprintf(FileOut, "%s_obj", FileOut);
	    strcpy(FileIn, choice);
	    PopupDialogWindow = TRUE;
    	    break;
        case LIST_CORRECT:
            /* generation of the output image name */
            for ( i = 0; choice[i] != '.' && choice[i] != '\0'; i++ )
             	;
            strncpy(FileOut, choice, i);
            FileOut[i] = '\0';
            sprintf(FileOut, "%s_cor", FileOut);
	    strcpy(FileIn, choice);
	    PopupDialogWindow = TRUE;
    	    break;
	case LIST_BROWSER:
	    for ( i = 0; choice[i] != DIR_DELIMITER && choice[i] != '\0'; i++ )
		;
	    if ( choice[i] == DIR_DELIMITER ) {
	        strcat(DirSpecs, choice);
	        PopupList(LIST_BROWSER);
    		popdown_filelist = FALSE;
	    }
	    else
		sprintf(command, "$cp %s%s %s", DirSpecs, choice, choice);
	    break;
    }
    XtFree(choice);

    if ( popdown_filelist )
    	UxPopdownInterface(FileListInterface);
    
    if ( PopupExtinWindow ) {
        DialogType = DIALOG_EXTIN;
        UxPutValue(UxFindSwidget("tf_output_extin"), FileOut);
        if ( exist_airmass(FileIn, &fval) )
	    Airmass = fval;
    	sprintf(str, "%.4f", Airmass);
        UxPutValue(UxFindSwidget("tf_airmass"), str);
    	UxPopupInterface(UxFindSwidget("extin_dialog"), exclusive_grab);
    }
    else if ( PopupDialogWindow ) {
	switch ( ListType ) {
	    case LIST_REBIN_RBR:
	        SET_DIALOG_PROMPT("Enter output image :");
	        DialogType = DIALOG_REBIN_RBR;
		break;
	    case LIST_REBIN_2D:
	        SET_DIALOG_PROMPT("Enter output image :");
	        DialogType = DIALOG_REBIN_2D;
		break;
	    case LIST_REBIN_TBL:
	        SET_DIALOG_PROMPT("Enter output table :");
	        DialogType = DIALOG_REBIN_TBL;
		break;
	    case LIST_EXT_AVER:
	        SET_DIALOG_PROMPT("Enter output image :");
	        DialogType = DIALOG_EXT_AVER;
		break;
	    case LIST_EXT_WEIGHT:
	        SET_DIALOG_PROMPT("Enter output image :");
	        DialogType = DIALOG_EXT_WEIGHT;
		break;
	    case LIST_CORRECT:
	        SET_DIALOG_PROMPT("Enter output image :");
	        DialogType = DIALOG_CORRECT;
		break;
	}
        UxPutValue(UxFindSwidget("tf_file_dialog"), FileOut);
    	UxPopupInterface(UxFindSwidget("file_dialog"), exclusive_grab);
    }
    else if ( command[0] != '\0' )
    	AppendDialogText(command);

}

void CallbackDialog()
{
    char command[128];
    char *out, *fout, *sky;

    out = XmTextGetString(UxGetWidget(UxFindSwidget("tf_file_dialog")));
    switch ( DialogType ) {
	case DIALOG_SESSION:
    	    strcpy(Session, out);
     	    sprintf(command, "%s%s", C_SAVE, Session);
	    break;
	case DIALOG_REBIN_RBR:
     	    sprintf(command, "%s%s %s", C_REBIN, FileIn, out);
	    break;
	case DIALOG_REBIN_2D:
     	    sprintf(command, "%s%s %s", C_RECTIFY, FileIn, out);
	    break;
	case DIALOG_REBIN_TBL:
     	    sprintf(command, "%s%s %s", C_APP_DISP, FileIn, out);
	    break;
	case DIALOG_EXT_AVER:
     	    sprintf(command, "%s%s %s", C_EXTR_AVERAGE, FileIn, out);
	    break;
	case DIALOG_EXT_WEIGHT:
    	    sky = XmTextGetString(UxGetWidget(UxFindSwidget("tf_sky")));
     	    sprintf(command, "%s%s %s %s", C_EXTR_WEIGHTED, FileIn, out, sky);
    	    XtFree(sky);
	    break;
	case DIALOG_CORRECT:
     	    sprintf(command, "%s%s %s", C_CORRECT, FileIn, out);
	    break;
	case DIALOG_EXTIN:
            fout = XmTextGetString(UxGetWidget(UxFindSwidget("tf_output_extin")));
     	    sprintf(command, "%s%s %s %f", C_EXTIN, FileIn, fout, Airmass);
    	    XtFree(fout);
    	    UxPopdownInterface(UxFindSwidget("extin_dialog"));
	    break;
    }
    AppendDialogText(command);
    XtFree(out);
    UxPopdownInterface(UxFindSwidget("file_dialog"));
}
