/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	RespShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxText.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxSep.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTranSh.h"

#include "proto_xech.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxRespShell;
	swidget	Uxform39;
	swidget	Uxform40;
	swidget	Uxpb_search_search12;
	swidget	UxpushButton8;
	swidget	Uxpb_search_plot22;
	swidget	Uxpb_search_plot23;
	swidget	Uxseparator32;
	swidget	Uxseparator33;
	swidget	UxrowColumn3;
	swidget	Uxrb_seamtd_gaus4;
	swidget	Uxrb_seamtd_grav4;
	swidget	Uxlabel113;
	swidget	Uxtf_alpha38;
	swidget	Uxlabel117;
	swidget	Uxpb_main_search28;
	swidget	Uxpb_search_plot30;
	swidget	Uxform41;
	swidget	Uxlabel99;
	swidget	Uxtf_alpha26;
	swidget	Uxtf_alpha27;
	swidget	Uxlabel102;
	swidget	Uxtf_alpha28;
	swidget	Uxtf_alpha29;
	swidget	Uxtf_alpha30;
	swidget	Uxlabel104;
	swidget	Uxtf_alpha32;
	swidget	Uxlabel105;
	swidget	Uxlabel106;
	swidget	Uxlabel107;
	swidget	Uxlabel108;
	swidget	Uxlabel109;
	swidget	Uxlabel110;
	swidget	Uxtf_alpha39;
	swidget	Uxpb_main_search29;
	swidget	Uxlabel118;
	swidget	Uxpb_search_plot31;
	swidget	Uxlabel127;
	swidget	Uxform42;
	swidget	Uxlabel103;
	swidget	Uxtf_alpha33;
	swidget	Uxlabel111;
	swidget	Uxtf_alpha34;
	swidget	Uxtf_alpha41;
	swidget	Uxlabel112;
	swidget	Uxtf_alpha43;
	swidget	Uxlabel120;
	swidget	Uxlabel123;
	swidget	UxrowColumn28;
	swidget	Uxrb_seamtd_gaus5;
	swidget	Uxrb_seamtd_grav5;
	swidget	Uxrb_seamtd_maxi5;
	swidget	Uxlabel119;
	swidget	Uxlabel126;
	swidget	Uxshelp_search8;
} _UxCRespShell;

#define RespShell               UxRespShellContext->UxRespShell
#define form39                  UxRespShellContext->Uxform39
#define form40                  UxRespShellContext->Uxform40
#define pb_search_search12      UxRespShellContext->Uxpb_search_search12
#define pushButton8             UxRespShellContext->UxpushButton8
#define pb_search_plot22        UxRespShellContext->Uxpb_search_plot22
#define pb_search_plot23        UxRespShellContext->Uxpb_search_plot23
#define separator32             UxRespShellContext->Uxseparator32
#define separator33             UxRespShellContext->Uxseparator33
#define rowColumn3              UxRespShellContext->UxrowColumn3
#define rb_seamtd_gaus4         UxRespShellContext->Uxrb_seamtd_gaus4
#define rb_seamtd_grav4         UxRespShellContext->Uxrb_seamtd_grav4
#define label113                UxRespShellContext->Uxlabel113
#define tf_alpha38              UxRespShellContext->Uxtf_alpha38
#define label117                UxRespShellContext->Uxlabel117
#define pb_main_search28        UxRespShellContext->Uxpb_main_search28
#define pb_search_plot30        UxRespShellContext->Uxpb_search_plot30
#define form41                  UxRespShellContext->Uxform41
#define label99                 UxRespShellContext->Uxlabel99
#define tf_alpha26              UxRespShellContext->Uxtf_alpha26
#define tf_alpha27              UxRespShellContext->Uxtf_alpha27
#define label102                UxRespShellContext->Uxlabel102
#define tf_alpha28              UxRespShellContext->Uxtf_alpha28
#define tf_alpha29              UxRespShellContext->Uxtf_alpha29
#define tf_alpha30              UxRespShellContext->Uxtf_alpha30
#define label104                UxRespShellContext->Uxlabel104
#define tf_alpha32              UxRespShellContext->Uxtf_alpha32
#define label105                UxRespShellContext->Uxlabel105
#define label106                UxRespShellContext->Uxlabel106
#define label107                UxRespShellContext->Uxlabel107
#define label108                UxRespShellContext->Uxlabel108
#define label109                UxRespShellContext->Uxlabel109
#define label110                UxRespShellContext->Uxlabel110
#define tf_alpha39              UxRespShellContext->Uxtf_alpha39
#define pb_main_search29        UxRespShellContext->Uxpb_main_search29
#define label118                UxRespShellContext->Uxlabel118
#define pb_search_plot31        UxRespShellContext->Uxpb_search_plot31
#define label127                UxRespShellContext->Uxlabel127
#define form42                  UxRespShellContext->Uxform42
#define label103                UxRespShellContext->Uxlabel103
#define tf_alpha33              UxRespShellContext->Uxtf_alpha33
#define label111                UxRespShellContext->Uxlabel111
#define tf_alpha34              UxRespShellContext->Uxtf_alpha34
#define tf_alpha41              UxRespShellContext->Uxtf_alpha41
#define label112                UxRespShellContext->Uxlabel112
#define tf_alpha43              UxRespShellContext->Uxtf_alpha43
#define label120                UxRespShellContext->Uxlabel120
#define label123                UxRespShellContext->Uxlabel123
#define rowColumn28             UxRespShellContext->UxrowColumn28
#define rb_seamtd_gaus5         UxRespShellContext->Uxrb_seamtd_gaus5
#define rb_seamtd_grav5         UxRespShellContext->Uxrb_seamtd_grav5
#define rb_seamtd_maxi5         UxRespShellContext->Uxrb_seamtd_maxi5
#define label119                UxRespShellContext->Uxlabel119
#define label126                UxRespShellContext->Uxlabel126
#define shelp_search8           UxRespShellContext->Uxshelp_search8

static _UxCRespShell	*UxRespShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();
extern void  WriteKeyword(), GetExtendedHelp(), SelectList();


extern int RadioSet(), PopupList(), AppendDialogText();

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable28 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_RespShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_UpdateDirectory( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	 char node[80];
	
	 /* if (XmTextGetSelection(UxWidget) == NULL) return; */
	 strcpy(node,
	     XmTextGetSelection(UxWidget));
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	
	node[strlen(node)-1] = '\0';
	WGet_all_dirs(node);
	WChange_Midas_dir();
	WGet_all_files();
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxRespShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxRespShellContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxRespShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxRespShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_search_search12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	activateCB_pushButton8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("RespShell"));
	}
	UxRespShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot22( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	}
	UxRespShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot23( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_gaus4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxRespShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_gaus4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_grav4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxRespShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_grav4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha38( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search28( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot30( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha26( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha27( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha28( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha29( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha30( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha32( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha39( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search29( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot31( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha33( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha34( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha41( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha43( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_gaus5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxRespShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_gaus5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_grav5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxRespShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_grav5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_maxi5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxRespShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_maxi5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRespShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRespShellContext;
	UxRespShellContext = UxContext =
			(_UxCRespShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRespShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_RespShell()
{
	UxPutTitle( RespShell, "XEchelle: Instrumental Response" );
	UxPutGeometry( RespShell, "+10+60" );
	UxPutHeight( RespShell, 640 );
	UxPutWidth( RespShell, 530 );
	UxPutY( RespShell, 287 );
	UxPutX( RespShell, 899 );

	UxPutBackground( form39, WindowBackground );
	UxPutHeight( form39, 640 );
	UxPutWidth( form39, 530 );
	UxPutY( form39, -2 );
	UxPutX( form39, 0 );
	UxPutUnitType( form39, "pixels" );
	UxPutResizePolicy( form39, "resize_none" );

	UxPutBackground( form40, ButtonBackground );
	UxPutHeight( form40, 46 );
	UxPutWidth( form40, 508 );
	UxPutY( form40, 552 );
	UxPutX( form40, 12 );
	UxPutResizePolicy( form40, "resize_none" );

	UxPutLabelString( pb_search_search12, "Fit" );
	UxPutForeground( pb_search_search12, ApplyForeground );
	UxPutFontList( pb_search_search12, BoldTextFont );
	UxPutBackground( pb_search_search12, ButtonBackground );
	UxPutHeight( pb_search_search12, 30 );
	UxPutWidth( pb_search_search12, 86 );
	UxPutY( pb_search_search12, 10 );
	UxPutX( pb_search_search12, 16 );

	UxPutLabelString( pushButton8, "Cancel" );
	UxPutForeground( pushButton8, CancelForeground );
	UxPutFontList( pushButton8, BoldTextFont );
	UxPutBackground( pushButton8, ButtonBackground );
	UxPutHeight( pushButton8, 30 );
	UxPutWidth( pushButton8, 86 );
	UxPutY( pushButton8, 10 );
	UxPutX( pushButton8, 402 );

	UxPutLabelString( pb_search_plot22, "Help..." );
	UxPutForeground( pb_search_plot22, ButtonForeground );
	UxPutFontList( pb_search_plot22, BoldTextFont );
	UxPutBackground( pb_search_plot22, ButtonBackground );
	UxPutHeight( pb_search_plot22, 30 );
	UxPutWidth( pb_search_plot22, 86 );
	UxPutY( pb_search_plot22, 8 );
	UxPutX( pb_search_plot22, 308 );

	UxPutLabelString( pb_search_plot23, "Plot Response" );
	UxPutForeground( pb_search_plot23, ButtonForeground );
	UxPutFontList( pb_search_plot23, BoldTextFont );
	UxPutBackground( pb_search_plot23, ButtonBackground );
	UxPutHeight( pb_search_plot23, 30 );
	UxPutWidth( pb_search_plot23, 138 );
	UxPutY( pb_search_plot23, 12 );
	UxPutX( pb_search_plot23, 110 );

	UxPutBackground( separator32, WindowBackground );
	UxPutHeight( separator32, 10 );
	UxPutWidth( separator32, 514 );
	UxPutY( separator32, 544 );
	UxPutX( separator32, 10 );

	UxPutBackground( separator33, WindowBackground );
	UxPutHeight( separator33, 10 );
	UxPutWidth( separator33, 518 );
	UxPutY( separator33, 480 );
	UxPutX( separator33, 8 );

	UxPutResizeWidth( rowColumn3, "true" );
	UxPutOrientation( rowColumn3, "horizontal" );
	UxPutIsAligned( rowColumn3, "true" );
	UxPutAdjustMargin( rowColumn3, "true" );
	UxPutAdjustLast( rowColumn3, "false" );
	UxPutEntryAlignment( rowColumn3, "alignment_beginning" );
	UxPutBorderWidth( rowColumn3, 0 );
	UxPutShadowThickness( rowColumn3, 0 );
	UxPutLabelString( rowColumn3, "" );
	UxPutEntryBorder( rowColumn3, 0 );
	UxPutBackground( rowColumn3, WindowBackground );
	UxPutRadioBehavior( rowColumn3, "true" );
	UxPutHeight( rowColumn3, 96 );
	UxPutWidth( rowColumn3, 106 );
	UxPutY( rowColumn3, 45 );
	UxPutX( rowColumn3, 92 );

	UxPutTranslations( rb_seamtd_gaus4, transTable28 );
	UxPutForeground( rb_seamtd_gaus4, TextForeground );
	UxPutIndicatorSize( rb_seamtd_gaus4, 16 );
	UxPutHighlightOnEnter( rb_seamtd_gaus4, "true" );
	UxPutSelectColor( rb_seamtd_gaus4, SelectColor );
	UxPutSet( rb_seamtd_gaus4, "true" );
	UxPutLabelString( rb_seamtd_gaus4, "Standard Star" );
	UxPutFontList( rb_seamtd_gaus4, TextFont );
	UxPutBackground( rb_seamtd_gaus4, WindowBackground );
	UxPutHeight( rb_seamtd_gaus4, 31 );
	UxPutWidth( rb_seamtd_gaus4, 139 );
	UxPutY( rb_seamtd_gaus4, 0 );
	UxPutX( rb_seamtd_gaus4, 3 );

	UxPutTranslations( rb_seamtd_grav4, transTable28 );
	UxPutForeground( rb_seamtd_grav4, TextForeground );
	UxPutIndicatorSize( rb_seamtd_grav4, 16 );
	UxPutHighlightOnEnter( rb_seamtd_grav4, "true" );
	UxPutSelectColor( rb_seamtd_grav4, SelectColor );
	UxPutLabelString( rb_seamtd_grav4, "Blaze Function Fit" );
	UxPutFontList( rb_seamtd_grav4, TextFont );
	UxPutBackground( rb_seamtd_grav4, WindowBackground );
	UxPutHeight( rb_seamtd_grav4, 28 );
	UxPutWidth( rb_seamtd_grav4, 108 );
	UxPutY( rb_seamtd_grav4, 3 );
	UxPutX( rb_seamtd_grav4, 136 );

	UxPutForeground( label113, TextForeground );
	UxPutAlignment( label113, "alignment_beginning" );
	UxPutLabelString( label113, "Method:" );
	UxPutFontList( label113, TextFont );
	UxPutBackground( label113, LabelBackground );
	UxPutHeight( label113, 30 );
	UxPutWidth( label113, 68 );
	UxPutY( label113, 47 );
	UxPutX( label113, 18 );

	UxPutForeground( tf_alpha38, TextForeground );
	UxPutHighlightOnEnter( tf_alpha38, "true" );
	UxPutFontList( tf_alpha38, TextFont );
	UxPutBackground( tf_alpha38, TextBackground );
	UxPutHeight( tf_alpha38, 32 );
	UxPutWidth( tf_alpha38, 236 );
	UxPutY( tf_alpha38, 8 );
	UxPutX( tf_alpha38, 126 );

	UxPutForeground( label117, TextForeground );
	UxPutAlignment( label117, "alignment_beginning" );
	UxPutLabelString( label117, "Input Frame:" );
	UxPutFontList( label117, TextFont );
	UxPutBackground( label117, LabelBackground );
	UxPutHeight( label117, 30 );
	UxPutWidth( label117, 94 );
	UxPutY( label117, 10 );
	UxPutX( label117, 18 );

	UxPutWidth( pb_main_search28, 46 );
	UxPutRecomputeSize( pb_main_search28, "true" );
	UxPutLabelString( pb_main_search28, "..." );
	UxPutForeground( pb_main_search28, ButtonForeground );
	UxPutFontList( pb_main_search28, BoldTextFont );
	UxPutBackground( pb_main_search28, ButtonBackground );
	UxPutHeight( pb_main_search28, 28 );
	UxPutY( pb_main_search28, 10 );
	UxPutX( pb_main_search28, 366 );

	UxPutLabelString( pb_search_plot30, "Display" );
	UxPutForeground( pb_search_plot30, ButtonForeground );
	UxPutFontList( pb_search_plot30, BoldTextFont );
	UxPutBackground( pb_search_plot30, ButtonBackground );
	UxPutHeight( pb_search_plot30, 30 );
	UxPutWidth( pb_search_plot30, 86 );
	UxPutY( pb_search_plot30, 10 );
	UxPutX( pb_search_plot30, 420 );

	UxPutBackground( form41, ButtonBackground );
	UxPutHeight( form41, 194 );
	UxPutWidth( form41, 508 );
	UxPutY( form41, 80 );
	UxPutX( form41, 14 );
	UxPutResizePolicy( form41, "resize_none" );

	UxPutForeground( label99, TextForeground );
	UxPutAlignment( label99, "alignment_beginning" );
	UxPutLabelString( label99, "Smooth Filter:" );
	UxPutFontList( label99, TextFont );
	UxPutBackground( label99, LabelBackground );
	UxPutHeight( label99, 30 );
	UxPutWidth( label99, 100 );
	UxPutY( label99, 120 );
	UxPutX( label99, 36 );

	UxPutForeground( tf_alpha26, TextForeground );
	UxPutHighlightOnEnter( tf_alpha26, "true" );
	UxPutFontList( tf_alpha26, TextFont );
	UxPutBackground( tf_alpha26, TextBackground );
	UxPutHeight( tf_alpha26, 32 );
	UxPutWidth( tf_alpha26, 96 );
	UxPutY( tf_alpha26, 74 );
	UxPutX( tf_alpha26, 390 );

	UxPutForeground( tf_alpha27, TextForeground );
	UxPutHighlightOnEnter( tf_alpha27, "true" );
	UxPutFontList( tf_alpha27, TextFont );
	UxPutBackground( tf_alpha27, TextBackground );
	UxPutHeight( tf_alpha27, 32 );
	UxPutWidth( tf_alpha27, 96 );
	UxPutY( tf_alpha27, 156 );
	UxPutX( tf_alpha27, 232 );

	UxPutForeground( label102, TextForeground );
	UxPutAlignment( label102, "alignment_beginning" );
	UxPutLabelString( label102, "Ignored Pixels:" );
	UxPutFontList( label102, TextFont );
	UxPutBackground( label102, LabelBackground );
	UxPutHeight( label102, 30 );
	UxPutWidth( label102, 114 );
	UxPutY( label102, 158 );
	UxPutX( label102, 36 );

	UxPutForeground( tf_alpha28, TextForeground );
	UxPutHighlightOnEnter( tf_alpha28, "true" );
	UxPutFontList( tf_alpha28, TextFont );
	UxPutBackground( tf_alpha28, TextBackground );
	UxPutHeight( tf_alpha28, 32 );
	UxPutWidth( tf_alpha28, 96 );
	UxPutY( tf_alpha28, 116 );
	UxPutX( tf_alpha28, 234 );

	UxPutForeground( tf_alpha29, TextForeground );
	UxPutHighlightOnEnter( tf_alpha29, "true" );
	UxPutFontList( tf_alpha29, TextFont );
	UxPutBackground( tf_alpha29, TextBackground );
	UxPutHeight( tf_alpha29, 32 );
	UxPutWidth( tf_alpha29, 96 );
	UxPutY( tf_alpha29, 154 );
	UxPutX( tf_alpha29, 388 );

	UxPutForeground( tf_alpha30, TextForeground );
	UxPutHighlightOnEnter( tf_alpha30, "true" );
	UxPutFontList( tf_alpha30, TextFont );
	UxPutBackground( tf_alpha30, TextBackground );
	UxPutHeight( tf_alpha30, 32 );
	UxPutWidth( tf_alpha30, 96 );
	UxPutY( tf_alpha30, 116 );
	UxPutX( tf_alpha30, 388 );

	UxPutForeground( label104, TextForeground );
	UxPutAlignment( label104, "alignment_beginning" );
	UxPutLabelString( label104, "Median Filter:" );
	UxPutFontList( label104, TextFont );
	UxPutBackground( label104, LabelBackground );
	UxPutHeight( label104, 30 );
	UxPutWidth( label104, 136 );
	UxPutY( label104, 80 );
	UxPutX( label104, 36 );

	UxPutForeground( tf_alpha32, TextForeground );
	UxPutHighlightOnEnter( tf_alpha32, "true" );
	UxPutFontList( tf_alpha32, TextFont );
	UxPutBackground( tf_alpha32, TextBackground );
	UxPutHeight( tf_alpha32, 32 );
	UxPutWidth( tf_alpha32, 96 );
	UxPutY( tf_alpha32, 76 );
	UxPutX( tf_alpha32, 236 );

	UxPutForeground( label105, TextForeground );
	UxPutAlignment( label105, "alignment_beginning" );
	UxPutLabelString( label105, "Min=" );
	UxPutFontList( label105, TextFont );
	UxPutBackground( label105, LabelBackground );
	UxPutHeight( label105, 30 );
	UxPutWidth( label105, 40 );
	UxPutY( label105, 158 );
	UxPutX( label105, 188 );

	UxPutForeground( label106, TextForeground );
	UxPutAlignment( label106, "alignment_beginning" );
	UxPutLabelString( label106, "X=" );
	UxPutFontList( label106, TextFont );
	UxPutBackground( label106, LabelBackground );
	UxPutHeight( label106, 30 );
	UxPutWidth( label106, 36 );
	UxPutY( label106, 118 );
	UxPutX( label106, 186 );

	UxPutForeground( label107, TextForeground );
	UxPutAlignment( label107, "alignment_beginning" );
	UxPutLabelString( label107, "Y=" );
	UxPutFontList( label107, TextFont );
	UxPutBackground( label107, LabelBackground );
	UxPutHeight( label107, 30 );
	UxPutWidth( label107, 20 );
	UxPutY( label107, 74 );
	UxPutX( label107, 362 );

	UxPutForeground( label108, TextForeground );
	UxPutAlignment( label108, "alignment_beginning" );
	UxPutLabelString( label108, "Max=" );
	UxPutFontList( label108, TextFont );
	UxPutBackground( label108, LabelBackground );
	UxPutHeight( label108, 30 );
	UxPutWidth( label108, 50 );
	UxPutY( label108, 156 );
	UxPutX( label108, 338 );

	UxPutForeground( label109, TextForeground );
	UxPutAlignment( label109, "alignment_beginning" );
	UxPutLabelString( label109, "Y=" );
	UxPutFontList( label109, TextFont );
	UxPutBackground( label109, LabelBackground );
	UxPutHeight( label109, 30 );
	UxPutWidth( label109, 20 );
	UxPutY( label109, 116 );
	UxPutX( label109, 356 );

	UxPutForeground( label110, TextForeground );
	UxPutAlignment( label110, "alignment_beginning" );
	UxPutLabelString( label110, "X=" );
	UxPutFontList( label110, TextFont );
	UxPutBackground( label110, LabelBackground );
	UxPutHeight( label110, 30 );
	UxPutWidth( label110, 34 );
	UxPutY( label110, 80 );
	UxPutX( label110, 186 );

	UxPutForeground( tf_alpha39, TextForeground );
	UxPutHighlightOnEnter( tf_alpha39, "true" );
	UxPutFontList( tf_alpha39, TextFont );
	UxPutBackground( tf_alpha39, TextBackground );
	UxPutHeight( tf_alpha39, 32 );
	UxPutWidth( tf_alpha39, 228 );
	UxPutY( tf_alpha39, 36 );
	UxPutX( tf_alpha39, 132 );

	UxPutWidth( pb_main_search29, 46 );
	UxPutRecomputeSize( pb_main_search29, "true" );
	UxPutLabelString( pb_main_search29, "..." );
	UxPutForeground( pb_main_search29, ButtonForeground );
	UxPutFontList( pb_main_search29, BoldTextFont );
	UxPutBackground( pb_main_search29, ButtonBackground );
	UxPutHeight( pb_main_search29, 28 );
	UxPutY( pb_main_search29, 38 );
	UxPutX( pb_main_search29, 364 );

	UxPutForeground( label118, TextForeground );
	UxPutAlignment( label118, "alignment_beginning" );
	UxPutLabelString( label118, "Flux Table" );
	UxPutFontList( label118, TextFont );
	UxPutBackground( label118, LabelBackground );
	UxPutHeight( label118, 30 );
	UxPutWidth( label118, 94 );
	UxPutY( label118, 38 );
	UxPutX( label118, 22 );

	UxPutLabelString( pb_search_plot31, "Plot" );
	UxPutForeground( pb_search_plot31, ButtonForeground );
	UxPutFontList( pb_search_plot31, BoldTextFont );
	UxPutBackground( pb_search_plot31, ButtonBackground );
	UxPutHeight( pb_search_plot31, 30 );
	UxPutWidth( pb_search_plot31, 86 );
	UxPutY( pb_search_plot31, 38 );
	UxPutX( pb_search_plot31, 422 );

	UxPutForeground( label127, TextForeground );
	UxPutAlignment( label127, "alignment_center" );
	UxPutLabelString( label127, "Standard Star" );
	UxPutFontList( label127, BoldTextFont );
	UxPutBackground( label127, LabelBackground );
	UxPutHeight( label127, 30 );
	UxPutWidth( label127, 166 );
	UxPutY( label127, 2 );
	UxPutX( label127, 168 );

	UxPutBackground( form42, ButtonBackground );
	UxPutHeight( form42, 194 );
	UxPutWidth( form42, 508 );
	UxPutY( form42, 284 );
	UxPutX( form42, 14 );
	UxPutResizePolicy( form42, "resize_none" );

	UxPutForeground( label103, TextForeground );
	UxPutAlignment( label103, "alignment_beginning" );
	UxPutLabelString( label103, "Alpha:" );
	UxPutFontList( label103, TextFont );
	UxPutBackground( label103, LabelBackground );
	UxPutHeight( label103, 30 );
	UxPutWidth( label103, 100 );
	UxPutY( label103, 118 );
	UxPutX( label103, 34 );

	UxPutForeground( tf_alpha33, TextForeground );
	UxPutHighlightOnEnter( tf_alpha33, "true" );
	UxPutFontList( tf_alpha33, TextFont );
	UxPutBackground( tf_alpha33, TextBackground );
	UxPutHeight( tf_alpha33, 32 );
	UxPutWidth( tf_alpha33, 96 );
	UxPutY( tf_alpha33, 154 );
	UxPutX( tf_alpha33, 230 );

	UxPutForeground( label111, TextForeground );
	UxPutAlignment( label111, "alignment_beginning" );
	UxPutLabelString( label111, "Lambda" );
	UxPutFontList( label111, TextFont );
	UxPutBackground( label111, LabelBackground );
	UxPutHeight( label111, 30 );
	UxPutWidth( label111, 94 );
	UxPutY( label111, 156 );
	UxPutX( label111, 34 );

	UxPutForeground( tf_alpha34, TextForeground );
	UxPutHighlightOnEnter( tf_alpha34, "true" );
	UxPutFontList( tf_alpha34, TextFont );
	UxPutBackground( tf_alpha34, TextBackground );
	UxPutHeight( tf_alpha34, 32 );
	UxPutWidth( tf_alpha34, 136 );
	UxPutY( tf_alpha34, 114 );
	UxPutX( tf_alpha34, 192 );

	UxPutForeground( tf_alpha41, TextForeground );
	UxPutHighlightOnEnter( tf_alpha41, "true" );
	UxPutFontList( tf_alpha41, TextFont );
	UxPutBackground( tf_alpha41, TextBackground );
	UxPutHeight( tf_alpha41, 32 );
	UxPutWidth( tf_alpha41, 96 );
	UxPutY( tf_alpha41, 152 );
	UxPutX( tf_alpha41, 386 );

	UxPutForeground( label112, TextForeground );
	UxPutAlignment( label112, "alignment_beginning" );
	UxPutLabelString( label112, "Grating Constant:" );
	UxPutFontList( label112, TextFont );
	UxPutBackground( label112, LabelBackground );
	UxPutHeight( label112, 30 );
	UxPutWidth( label112, 136 );
	UxPutY( label112, 78 );
	UxPutX( label112, 34 );

	UxPutForeground( tf_alpha43, TextForeground );
	UxPutHighlightOnEnter( tf_alpha43, "true" );
	UxPutFontList( tf_alpha43, TextFont );
	UxPutBackground( tf_alpha43, TextBackground );
	UxPutHeight( tf_alpha43, 32 );
	UxPutWidth( tf_alpha43, 140 );
	UxPutY( tf_alpha43, 74 );
	UxPutX( tf_alpha43, 190 );

	UxPutForeground( label120, TextForeground );
	UxPutAlignment( label120, "alignment_beginning" );
	UxPutLabelString( label120, "Min=" );
	UxPutFontList( label120, TextFont );
	UxPutBackground( label120, LabelBackground );
	UxPutHeight( label120, 30 );
	UxPutWidth( label120, 40 );
	UxPutY( label120, 156 );
	UxPutX( label120, 186 );

	UxPutForeground( label123, TextForeground );
	UxPutAlignment( label123, "alignment_beginning" );
	UxPutLabelString( label123, "Max=" );
	UxPutFontList( label123, TextFont );
	UxPutBackground( label123, LabelBackground );
	UxPutHeight( label123, 30 );
	UxPutWidth( label123, 50 );
	UxPutY( label123, 154 );
	UxPutX( label123, 336 );

	UxPutResizeWidth( rowColumn28, "true" );
	UxPutOrientation( rowColumn28, "horizontal" );
	UxPutIsAligned( rowColumn28, "true" );
	UxPutAdjustMargin( rowColumn28, "true" );
	UxPutAdjustLast( rowColumn28, "false" );
	UxPutEntryAlignment( rowColumn28, "alignment_beginning" );
	UxPutBorderWidth( rowColumn28, 0 );
	UxPutShadowThickness( rowColumn28, 0 );
	UxPutLabelString( rowColumn28, "" );
	UxPutEntryBorder( rowColumn28, 0 );
	UxPutBackground( rowColumn28, WindowBackground );
	UxPutRadioBehavior( rowColumn28, "true" );
	UxPutHeight( rowColumn28, 34 );
	UxPutWidth( rowColumn28, 261 );
	UxPutY( rowColumn28, 36 );
	UxPutX( rowColumn28, 146 );

	UxPutTranslations( rb_seamtd_gaus5, transTable28 );
	UxPutForeground( rb_seamtd_gaus5, TextForeground );
	UxPutIndicatorSize( rb_seamtd_gaus5, 16 );
	UxPutHighlightOnEnter( rb_seamtd_gaus5, "true" );
	UxPutSelectColor( rb_seamtd_gaus5, SelectColor );
	UxPutSet( rb_seamtd_gaus5, "true" );
	UxPutLabelString( rb_seamtd_gaus5, "Fit" );
	UxPutFontList( rb_seamtd_gaus5, TextFont );
	UxPutBackground( rb_seamtd_gaus5, WindowBackground );
	UxPutHeight( rb_seamtd_gaus5, 28 );
	UxPutWidth( rb_seamtd_gaus5, 84 );
	UxPutY( rb_seamtd_gaus5, 3 );
	UxPutX( rb_seamtd_gaus5, 2 );

	UxPutTranslations( rb_seamtd_grav5, transTable28 );
	UxPutForeground( rb_seamtd_grav5, TextForeground );
	UxPutIndicatorSize( rb_seamtd_grav5, 16 );
	UxPutHighlightOnEnter( rb_seamtd_grav5, "true" );
	UxPutSelectColor( rb_seamtd_grav5, SelectColor );
	UxPutLabelString( rb_seamtd_grav5, "Overlap" );
	UxPutFontList( rb_seamtd_grav5, TextFont );
	UxPutBackground( rb_seamtd_grav5, WindowBackground );
	UxPutHeight( rb_seamtd_grav5, 28 );
	UxPutWidth( rb_seamtd_grav5, 84 );
	UxPutY( rb_seamtd_grav5, 4 );
	UxPutX( rb_seamtd_grav5, 114 );

	UxPutTranslations( rb_seamtd_maxi5, transTable28 );
	UxPutForeground( rb_seamtd_maxi5, TextForeground );
	UxPutIndicatorSize( rb_seamtd_maxi5, 16 );
	UxPutHighlightOnEnter( rb_seamtd_maxi5, "true" );
	UxPutSelectColor( rb_seamtd_maxi5, SelectColor );
	UxPutLabelString( rb_seamtd_maxi5, "Sinc" );
	UxPutFontList( rb_seamtd_maxi5, TextFont );
	UxPutBackground( rb_seamtd_maxi5, WindowBackground );
	UxPutHeight( rb_seamtd_maxi5, 28 );
	UxPutWidth( rb_seamtd_maxi5, 84 );
	UxPutY( rb_seamtd_maxi5, 3 );
	UxPutX( rb_seamtd_maxi5, 175 );

	UxPutForeground( label119, TextForeground );
	UxPutAlignment( label119, "alignment_beginning" );
	UxPutLabelString( label119, "Method:" );
	UxPutFontList( label119, TextFont );
	UxPutBackground( label119, LabelBackground );
	UxPutHeight( label119, 30 );
	UxPutWidth( label119, 68 );
	UxPutY( label119, 36 );
	UxPutX( label119, 56 );

	UxPutForeground( label126, TextForeground );
	UxPutAlignment( label126, "alignment_center" );
	UxPutLabelString( label126, "Blaze Function Fitting" );
	UxPutFontList( label126, BoldTextFont );
	UxPutBackground( label126, LabelBackground );
	UxPutHeight( label126, 30 );
	UxPutWidth( label126, 166 );
	UxPutY( label126, 2 );
	UxPutX( label126, 168 );

	UxPutFontList( shelp_search8, TextFont );
	UxPutEditable( shelp_search8, "false" );
	UxPutCursorPositionVisible( shelp_search8, "false" );
	UxPutBackground( shelp_search8, SHelpBackground );
	UxPutHeight( shelp_search8, 50 );
	UxPutWidth( shelp_search8, 508 );
	UxPutY( shelp_search8, 492 );
	UxPutX( shelp_search8, 12 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_RespShell()
{
	/* Create the swidgets */

	RespShell = UxCreateTransientShell( "RespShell", NO_PARENT );
	UxPutContext( RespShell, UxRespShellContext );

	form39 = UxCreateForm( "form39", RespShell );
	form40 = UxCreateForm( "form40", form39 );
	pb_search_search12 = UxCreatePushButton( "pb_search_search12", form40 );
	pushButton8 = UxCreatePushButton( "pushButton8", form40 );
	pb_search_plot22 = UxCreatePushButton( "pb_search_plot22", form40 );
	pb_search_plot23 = UxCreatePushButton( "pb_search_plot23", form40 );
	separator32 = UxCreateSeparator( "separator32", form39 );
	separator33 = UxCreateSeparator( "separator33", form39 );
	rowColumn3 = UxCreateRowColumn( "rowColumn3", form39 );
	rb_seamtd_gaus4 = UxCreateToggleButton( "rb_seamtd_gaus4", rowColumn3 );
	rb_seamtd_grav4 = UxCreateToggleButton( "rb_seamtd_grav4", rowColumn3 );
	label113 = UxCreateLabel( "label113", form39 );
	tf_alpha38 = UxCreateTextField( "tf_alpha38", form39 );
	label117 = UxCreateLabel( "label117", form39 );
	pb_main_search28 = UxCreatePushButton( "pb_main_search28", form39 );
	pb_search_plot30 = UxCreatePushButton( "pb_search_plot30", form39 );
	form41 = UxCreateForm( "form41", form39 );
	label99 = UxCreateLabel( "label99", form41 );
	tf_alpha26 = UxCreateTextField( "tf_alpha26", form41 );
	tf_alpha27 = UxCreateTextField( "tf_alpha27", form41 );
	label102 = UxCreateLabel( "label102", form41 );
	tf_alpha28 = UxCreateTextField( "tf_alpha28", form41 );
	tf_alpha29 = UxCreateTextField( "tf_alpha29", form41 );
	tf_alpha30 = UxCreateTextField( "tf_alpha30", form41 );
	label104 = UxCreateLabel( "label104", form41 );
	tf_alpha32 = UxCreateTextField( "tf_alpha32", form41 );
	label105 = UxCreateLabel( "label105", form41 );
	label106 = UxCreateLabel( "label106", form41 );
	label107 = UxCreateLabel( "label107", form41 );
	label108 = UxCreateLabel( "label108", form41 );
	label109 = UxCreateLabel( "label109", form41 );
	label110 = UxCreateLabel( "label110", form41 );
	tf_alpha39 = UxCreateTextField( "tf_alpha39", form41 );
	pb_main_search29 = UxCreatePushButton( "pb_main_search29", form41 );
	label118 = UxCreateLabel( "label118", form41 );
	pb_search_plot31 = UxCreatePushButton( "pb_search_plot31", form41 );
	label127 = UxCreateLabel( "label127", form41 );
	form42 = UxCreateForm( "form42", form39 );
	label103 = UxCreateLabel( "label103", form42 );
	tf_alpha33 = UxCreateTextField( "tf_alpha33", form42 );
	label111 = UxCreateLabel( "label111", form42 );
	tf_alpha34 = UxCreateTextField( "tf_alpha34", form42 );
	tf_alpha41 = UxCreateTextField( "tf_alpha41", form42 );
	label112 = UxCreateLabel( "label112", form42 );
	tf_alpha43 = UxCreateTextField( "tf_alpha43", form42 );
	label120 = UxCreateLabel( "label120", form42 );
	label123 = UxCreateLabel( "label123", form42 );
	rowColumn28 = UxCreateRowColumn( "rowColumn28", form42 );
	rb_seamtd_gaus5 = UxCreateToggleButton( "rb_seamtd_gaus5", rowColumn28 );
	rb_seamtd_grav5 = UxCreateToggleButton( "rb_seamtd_grav5", rowColumn28 );
	rb_seamtd_maxi5 = UxCreateToggleButton( "rb_seamtd_maxi5", rowColumn28 );
	label119 = UxCreateLabel( "label119", form42 );
	label126 = UxCreateLabel( "label126", form42 );
	shelp_search8 = UxCreateText( "shelp_search8", form39 );

	_Uxinit_RespShell();

	/* Create the X widgets */

	UxCreateWidget( RespShell );
	UxCreateWidget( form39 );
	UxCreateWidget( form40 );
	UxCreateWidget( pb_search_search12 );
	UxCreateWidget( pushButton8 );
	UxCreateWidget( pb_search_plot22 );
	UxCreateWidget( pb_search_plot23 );
	UxCreateWidget( separator32 );
	UxCreateWidget( separator33 );
	UxCreateWidget( rowColumn3 );
	UxCreateWidget( rb_seamtd_gaus4 );
	UxCreateWidget( rb_seamtd_grav4 );
	UxCreateWidget( label113 );
	UxCreateWidget( tf_alpha38 );
	UxCreateWidget( label117 );
	UxCreateWidget( pb_main_search28 );
	UxCreateWidget( pb_search_plot30 );
	UxCreateWidget( form41 );
	UxCreateWidget( label99 );
	UxCreateWidget( tf_alpha26 );
	UxCreateWidget( tf_alpha27 );
	UxCreateWidget( label102 );
	UxCreateWidget( tf_alpha28 );
	UxCreateWidget( tf_alpha29 );
	UxCreateWidget( tf_alpha30 );
	UxCreateWidget( label104 );
	UxCreateWidget( tf_alpha32 );
	UxCreateWidget( label105 );
	UxCreateWidget( label106 );
	UxCreateWidget( label107 );
	UxCreateWidget( label108 );
	UxCreateWidget( label109 );
	UxCreateWidget( label110 );
	UxCreateWidget( tf_alpha39 );
	UxCreateWidget( pb_main_search29 );
	UxCreateWidget( label118 );
	UxCreateWidget( pb_search_plot31 );
	UxCreateWidget( label127 );
	UxCreateWidget( form42 );
	UxCreateWidget( label103 );
	UxCreateWidget( tf_alpha33 );
	UxCreateWidget( label111 );
	UxCreateWidget( tf_alpha34 );
	UxCreateWidget( tf_alpha41 );
	UxCreateWidget( label112 );
	UxCreateWidget( tf_alpha43 );
	UxCreateWidget( label120 );
	UxCreateWidget( label123 );
	UxCreateWidget( rowColumn28 );
	UxCreateWidget( rb_seamtd_gaus5 );
	UxCreateWidget( rb_seamtd_grav5 );
	UxCreateWidget( rb_seamtd_maxi5 );
	UxCreateWidget( label119 );
	UxCreateWidget( label126 );
	UxCreateWidget( shelp_search8 );

	UxAddCallback( pb_search_search12, XmNactivateCallback,
			activateCB_pb_search_search12,
			(XtPointer) UxRespShellContext );

	UxAddCallback( pushButton8, XmNactivateCallback,
			activateCB_pushButton8,
			(XtPointer) UxRespShellContext );

	UxAddCallback( pb_search_plot22, XmNactivateCallback,
			activateCB_pb_search_plot22,
			(XtPointer) UxRespShellContext );

	UxAddCallback( pb_search_plot23, XmNactivateCallback,
			activateCB_pb_search_plot23,
			(XtPointer) UxRespShellContext );

	UxAddCallback( rb_seamtd_gaus4, XmNarmCallback,
			armCB_rb_seamtd_gaus4,
			(XtPointer) UxRespShellContext );
	UxAddCallback( rb_seamtd_gaus4, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_gaus4,
			(XtPointer) UxRespShellContext );

	UxAddCallback( rb_seamtd_grav4, XmNarmCallback,
			armCB_rb_seamtd_grav4,
			(XtPointer) UxRespShellContext );
	UxAddCallback( rb_seamtd_grav4, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_grav4,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha38, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha38,
			(XtPointer) UxRespShellContext );

	UxAddCallback( pb_main_search28, XmNactivateCallback,
			activateCB_pb_main_search28,
			(XtPointer) UxRespShellContext );

	UxAddCallback( pb_search_plot30, XmNactivateCallback,
			activateCB_pb_search_plot30,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha26, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha26,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha27, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha27,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha28, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha28,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha29, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha29,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha30, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha30,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha32, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha32,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha39, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha39,
			(XtPointer) UxRespShellContext );

	UxAddCallback( pb_main_search29, XmNactivateCallback,
			activateCB_pb_main_search29,
			(XtPointer) UxRespShellContext );

	UxAddCallback( pb_search_plot31, XmNactivateCallback,
			activateCB_pb_search_plot31,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha33, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha33,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha34, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha34,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha41, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha41,
			(XtPointer) UxRespShellContext );

	UxAddCallback( tf_alpha43, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha43,
			(XtPointer) UxRespShellContext );

	UxAddCallback( rb_seamtd_gaus5, XmNarmCallback,
			armCB_rb_seamtd_gaus5,
			(XtPointer) UxRespShellContext );
	UxAddCallback( rb_seamtd_gaus5, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_gaus5,
			(XtPointer) UxRespShellContext );

	UxAddCallback( rb_seamtd_grav5, XmNarmCallback,
			armCB_rb_seamtd_grav5,
			(XtPointer) UxRespShellContext );
	UxAddCallback( rb_seamtd_grav5, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_grav5,
			(XtPointer) UxRespShellContext );

	UxAddCallback( rb_seamtd_maxi5, XmNarmCallback,
			armCB_rb_seamtd_maxi5,
			(XtPointer) UxRespShellContext );
	UxAddCallback( rb_seamtd_maxi5, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_maxi5,
			(XtPointer) UxRespShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( RespShell );

	return ( RespShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_RespShell()
{
	swidget                 rtrn;
	_UxCRespShell           *UxContext;

	UxRespShellContext = UxContext =
		(_UxCRespShell *) UxMalloc( sizeof(_UxCRespShell) );

	rtrn = _Uxbuild_RespShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_RespShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "UpdateDirectory", action_UpdateDirectory },
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_RespShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

