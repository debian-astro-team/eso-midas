/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	SkyShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxRowCol.h"
#include "UxTogB.h"
#include "UxLabel.h"
#include "UxTextF.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTranSh.h"

#include "proto_xech.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxSkyShell;
	swidget	Uxform33;
	swidget	Uxform34;
	swidget	Uxpb_search_search8;
	swidget	UxpushButton6;
	swidget	Uxpb_search_plot19;
	swidget	Uxpb_search_plot20;
	swidget	Uxpb_search_search9;
	swidget	Uxshelp_search6;
	swidget	Uxseparator28;
	swidget	Uxseparator29;
	swidget	Uxtf_thres9;
	swidget	Uxlabel100;
	swidget	Uxpb_main_search23;
	swidget	Uxlabel101;
	swidget	Uxtf_thres10;
	swidget	Uxpb_main_search24;
	swidget	Uxform35;
	swidget	Uxlabel24;
	swidget	Uxtf_alpha22;
	swidget	Uxtf_alpha23;
	swidget	Uxlabel96;
	swidget	Uxform36;
	swidget	Uxlabel97;
	swidget	Uxtf_alpha24;
	swidget	Uxtf_alpha25;
	swidget	Uxlabel98;
	swidget	UxtoggleButton19;
	swidget	UxrowColumn7;
	swidget	UxtoggleButton16;
	swidget	UxtoggleButton18;
} _UxCSkyShell;

#define SkyShell                UxSkyShellContext->UxSkyShell
#define form33                  UxSkyShellContext->Uxform33
#define form34                  UxSkyShellContext->Uxform34
#define pb_search_search8       UxSkyShellContext->Uxpb_search_search8
#define pushButton6             UxSkyShellContext->UxpushButton6
#define pb_search_plot19        UxSkyShellContext->Uxpb_search_plot19
#define pb_search_plot20        UxSkyShellContext->Uxpb_search_plot20
#define pb_search_search9       UxSkyShellContext->Uxpb_search_search9
#define shelp_search6           UxSkyShellContext->Uxshelp_search6
#define separator28             UxSkyShellContext->Uxseparator28
#define separator29             UxSkyShellContext->Uxseparator29
#define tf_thres9               UxSkyShellContext->Uxtf_thres9
#define label100                UxSkyShellContext->Uxlabel100
#define pb_main_search23        UxSkyShellContext->Uxpb_main_search23
#define label101                UxSkyShellContext->Uxlabel101
#define tf_thres10              UxSkyShellContext->Uxtf_thres10
#define pb_main_search24        UxSkyShellContext->Uxpb_main_search24
#define form35                  UxSkyShellContext->Uxform35
#define label24                 UxSkyShellContext->Uxlabel24
#define tf_alpha22              UxSkyShellContext->Uxtf_alpha22
#define tf_alpha23              UxSkyShellContext->Uxtf_alpha23
#define label96                 UxSkyShellContext->Uxlabel96
#define form36                  UxSkyShellContext->Uxform36
#define label97                 UxSkyShellContext->Uxlabel97
#define tf_alpha24              UxSkyShellContext->Uxtf_alpha24
#define tf_alpha25              UxSkyShellContext->Uxtf_alpha25
#define label98                 UxSkyShellContext->Uxlabel98
#define toggleButton19          UxSkyShellContext->UxtoggleButton19
#define rowColumn7              UxSkyShellContext->UxrowColumn7
#define toggleButton16          UxSkyShellContext->UxtoggleButton16
#define toggleButton18          UxSkyShellContext->UxtoggleButton18

static _UxCSkyShell	*UxSkyShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();
extern void  WriteKeyword(), GetExtendedHelp(), SelectList();


extern int CheckStatus(), RadioSet(), PopupList(), AppendDialogText();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/


static char	*transTable22 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_SkyShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_UpdateDirectory( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	
	 char node[80];
	
	 /* if (XmTextGetSelection(UxWidget) == NULL) return; */
	 strcpy(node,
	     XmTextGetSelection(UxWidget));
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	
	node[strlen(node)-1] = '\0';
	WGet_all_dirs(node);
	WChange_Midas_dir();
	WGet_all_files();
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxSkyShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxSkyShellContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxSkyShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_search_search8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	activateCB_pushButton6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("SkyShell"));
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot19( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot20( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_search9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search23( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search24( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha22( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha23( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha24( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha25( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton19( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	if (CheckStatus(UxThisWidget) == TRUE)
	    AppendDialogText("SET/ECHEL ROTOPT=YES");
	else
	    AppendDialogText("SET/ECHEL ROTOPT=NO");
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	armCB_toggleButton16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxSkyShellContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSkyShellContext = UxSaveCtx;
}

static void	armCB_toggleButton18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxSkyShellContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCSkyShell            *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxSkyShellContext;
	UxSkyShellContext = UxContext =
			(_UxCSkyShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxSkyShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_SkyShell()
{
	UxPutGeometry( SkyShell, "+10+60" );
	UxPutTitle( SkyShell, "XEchelle: Sky Background" );
	UxPutHeight( SkyShell, 340 );
	UxPutWidth( SkyShell, 520 );
	UxPutY( SkyShell, 118 );
	UxPutX( SkyShell, 274 );

	UxPutBackground( form33, WindowBackground );
	UxPutHeight( form33, 344 );
	UxPutWidth( form33, 500 );
	UxPutY( form33, 0 );
	UxPutX( form33, -10 );
	UxPutUnitType( form33, "pixels" );
	UxPutResizePolicy( form33, "resize_none" );

	UxPutBackground( form34, ButtonBackground );
	UxPutHeight( form34, 40 );
	UxPutWidth( form34, 490 );
	UxPutY( form34, 296 );
	UxPutX( form34, 12 );
	UxPutResizePolicy( form34, "resize_none" );

	UxPutLabelString( pb_search_search8, "Windows" );
	UxPutForeground( pb_search_search8, ApplyForeground );
	UxPutFontList( pb_search_search8, BoldTextFont );
	UxPutBackground( pb_search_search8, ButtonBackground );
	UxPutHeight( pb_search_search8, 30 );
	UxPutWidth( pb_search_search8, 86 );
	UxPutY( pb_search_search8, 4 );
	UxPutX( pb_search_search8, 8 );

	UxPutLabelString( pushButton6, "Cancel" );
	UxPutForeground( pushButton6, CancelForeground );
	UxPutFontList( pushButton6, BoldTextFont );
	UxPutBackground( pushButton6, ButtonBackground );
	UxPutHeight( pushButton6, 30 );
	UxPutWidth( pushButton6, 86 );
	UxPutY( pushButton6, 6 );
	UxPutX( pushButton6, 392 );

	UxPutLabelString( pb_search_plot19, "Plot Orders" );
	UxPutForeground( pb_search_plot19, ButtonForeground );
	UxPutFontList( pb_search_plot19, BoldTextFont );
	UxPutBackground( pb_search_plot19, ButtonBackground );
	UxPutHeight( pb_search_plot19, 30 );
	UxPutWidth( pb_search_plot19, 100 );
	UxPutY( pb_search_plot19, 4 );
	UxPutX( pb_search_plot19, 196 );

	UxPutLabelString( pb_search_plot20, "Help..." );
	UxPutForeground( pb_search_plot20, ButtonForeground );
	UxPutFontList( pb_search_plot20, BoldTextFont );
	UxPutBackground( pb_search_plot20, ButtonBackground );
	UxPutHeight( pb_search_plot20, 30 );
	UxPutWidth( pb_search_plot20, 86 );
	UxPutY( pb_search_plot20, 4 );
	UxPutX( pb_search_plot20, 304 );

	UxPutLabelString( pb_search_search9, "Extract" );
	UxPutForeground( pb_search_search9, ApplyForeground );
	UxPutFontList( pb_search_search9, BoldTextFont );
	UxPutBackground( pb_search_search9, ButtonBackground );
	UxPutHeight( pb_search_search9, 30 );
	UxPutWidth( pb_search_search9, 86 );
	UxPutY( pb_search_search9, 4 );
	UxPutX( pb_search_search9, 104 );

	UxPutFontList( shelp_search6, TextFont );
	UxPutEditable( shelp_search6, "false" );
	UxPutCursorPositionVisible( shelp_search6, "false" );
	UxPutBackground( shelp_search6, SHelpBackground );
	UxPutHeight( shelp_search6, 50 );
	UxPutWidth( shelp_search6, 484 );
	UxPutY( shelp_search6, 236 );
	UxPutX( shelp_search6, 14 );

	UxPutBackground( separator28, WindowBackground );
	UxPutHeight( separator28, 10 );
	UxPutWidth( separator28, 492 );
	UxPutY( separator28, 226 );
	UxPutX( separator28, 12 );

	UxPutBackground( separator29, WindowBackground );
	UxPutHeight( separator29, 10 );
	UxPutWidth( separator29, 492 );
	UxPutY( separator29, 284 );
	UxPutX( separator29, 8 );

	UxPutForeground( tf_thres9, TextForeground );
	UxPutHighlightOnEnter( tf_thres9, "true" );
	UxPutFontList( tf_thres9, TextFont );
	UxPutBackground( tf_thres9, TextBackground );
	UxPutHeight( tf_thres9, 34 );
	UxPutWidth( tf_thres9, 212 );
	UxPutY( tf_thres9, 12 );
	UxPutX( tf_thres9, 208 );

	UxPutForeground( label100, TextForeground );
	UxPutAlignment( label100, "alignment_beginning" );
	UxPutLabelString( label100, "Input Frame:" );
	UxPutFontList( label100, TextFont );
	UxPutBackground( label100, LabelBackground );
	UxPutHeight( label100, 30 );
	UxPutWidth( label100, 170 );
	UxPutY( label100, 14 );
	UxPutX( label100, 20 );

	UxPutWidth( pb_main_search23, 46 );
	UxPutRecomputeSize( pb_main_search23, "true" );
	UxPutLabelString( pb_main_search23, "..." );
	UxPutForeground( pb_main_search23, ButtonForeground );
	UxPutFontList( pb_main_search23, BoldTextFont );
	UxPutBackground( pb_main_search23, ButtonBackground );
	UxPutHeight( pb_main_search23, 28 );
	UxPutY( pb_main_search23, 14 );
	UxPutX( pb_main_search23, 428 );

	UxPutForeground( label101, TextForeground );
	UxPutAlignment( label101, "alignment_beginning" );
	UxPutLabelString( label101, "Output Frame:" );
	UxPutFontList( label101, TextFont );
	UxPutBackground( label101, LabelBackground );
	UxPutHeight( label101, 30 );
	UxPutWidth( label101, 170 );
	UxPutY( label101, 56 );
	UxPutX( label101, 20 );

	UxPutForeground( tf_thres10, TextForeground );
	UxPutHighlightOnEnter( tf_thres10, "true" );
	UxPutFontList( tf_thres10, TextFont );
	UxPutBackground( tf_thres10, TextBackground );
	UxPutHeight( tf_thres10, 34 );
	UxPutWidth( tf_thres10, 212 );
	UxPutY( tf_thres10, 50 );
	UxPutX( tf_thres10, 210 );

	UxPutWidth( pb_main_search24, 46 );
	UxPutRecomputeSize( pb_main_search24, "true" );
	UxPutLabelString( pb_main_search24, "..." );
	UxPutForeground( pb_main_search24, ButtonForeground );
	UxPutFontList( pb_main_search24, BoldTextFont );
	UxPutBackground( pb_main_search24, ButtonBackground );
	UxPutHeight( pb_main_search24, 28 );
	UxPutY( pb_main_search24, 52 );
	UxPutX( pb_main_search24, 426 );

	UxPutBackground( form35, WindowBackground );
	UxPutHeight( form35, 40 );
	UxPutWidth( form35, 354 );
	UxPutY( form35, 98 );
	UxPutX( form35, 154 );
	UxPutResizePolicy( form35, "resize_none" );

	UxPutRecomputeSize( label24, "false" );
	UxPutForeground( label24, TextForeground );
	UxPutAlignment( label24, "alignment_beginning" );
	UxPutLabelString( label24, "Min=" );
	UxPutFontList( label24, TextFont );
	UxPutBackground( label24, LabelBackground );
	UxPutHeight( label24, 35 );
	UxPutWidth( label24, 52 );
	UxPutY( label24, 2 );
	UxPutX( label24, 32 );

	UxPutForeground( tf_alpha22, TextForeground );
	UxPutHighlightOnEnter( tf_alpha22, "true" );
	UxPutFontList( tf_alpha22, TextFont );
	UxPutBackground( tf_alpha22, TextBackground );
	UxPutHeight( tf_alpha22, 35 );
	UxPutWidth( tf_alpha22, 110 );
	UxPutY( tf_alpha22, 2 );
	UxPutX( tf_alpha22, 82 );

	UxPutForeground( tf_alpha23, TextForeground );
	UxPutHighlightOnEnter( tf_alpha23, "true" );
	UxPutFontList( tf_alpha23, TextFont );
	UxPutBackground( tf_alpha23, TextBackground );
	UxPutHeight( tf_alpha23, 35 );
	UxPutWidth( tf_alpha23, 110 );
	UxPutY( tf_alpha23, 2 );
	UxPutX( tf_alpha23, 240 );

	UxPutForeground( label96, TextForeground );
	UxPutAlignment( label96, "alignment_beginning" );
	UxPutLabelString( label96, "Max=" );
	UxPutFontList( label96, TextFont );
	UxPutBackground( label96, LabelBackground );
	UxPutHeight( label96, 35 );
	UxPutWidth( label96, 42 );
	UxPutY( label96, 2 );
	UxPutX( label96, 198 );

	UxPutBackground( form36, WindowBackground );
	UxPutHeight( form36, 40 );
	UxPutWidth( form36, 322 );
	UxPutY( form36, 136 );
	UxPutX( form36, 186 );
	UxPutResizePolicy( form36, "resize_none" );

	UxPutRecomputeSize( label97, "false" );
	UxPutForeground( label97, TextForeground );
	UxPutAlignment( label97, "alignment_beginning" );
	UxPutLabelString( label97, "Min=" );
	UxPutFontList( label97, TextFont );
	UxPutBackground( label97, LabelBackground );
	UxPutHeight( label97, 35 );
	UxPutWidth( label97, 52 );
	UxPutY( label97, 2 );
	UxPutX( label97, 0 );

	UxPutForeground( tf_alpha24, TextForeground );
	UxPutHighlightOnEnter( tf_alpha24, "true" );
	UxPutFontList( tf_alpha24, TextFont );
	UxPutBackground( tf_alpha24, TextBackground );
	UxPutHeight( tf_alpha24, 35 );
	UxPutWidth( tf_alpha24, 110 );
	UxPutY( tf_alpha24, 2 );
	UxPutX( tf_alpha24, 50 );

	UxPutForeground( tf_alpha25, TextForeground );
	UxPutHighlightOnEnter( tf_alpha25, "true" );
	UxPutFontList( tf_alpha25, TextFont );
	UxPutBackground( tf_alpha25, TextBackground );
	UxPutHeight( tf_alpha25, 35 );
	UxPutWidth( tf_alpha25, 110 );
	UxPutY( tf_alpha25, 2 );
	UxPutX( tf_alpha25, 206 );

	UxPutForeground( label98, TextForeground );
	UxPutAlignment( label98, "alignment_beginning" );
	UxPutLabelString( label98, "Max=" );
	UxPutFontList( label98, TextFont );
	UxPutBackground( label98, LabelBackground );
	UxPutHeight( label98, 35 );
	UxPutWidth( label98, 46 );
	UxPutY( label98, 3 );
	UxPutX( label98, 162 );

	UxPutSelectColor( toggleButton19, "Yellow" );
	UxPutLabelString( toggleButton19, "Cosmic Rays Filtering" );
	UxPutForeground( toggleButton19, TextForeground );
	UxPutFontList( toggleButton19, TextFont );
	UxPutBackground( toggleButton19, WindowBackground );
	UxPutRecomputeSize( toggleButton19, "false" );
	UxPutHeight( toggleButton19, 35 );
	UxPutWidth( toggleButton19, 178 );
	UxPutY( toggleButton19, 186 );
	UxPutX( toggleButton19, 30 );

	UxPutIsAligned( rowColumn7, "true" );
	UxPutAdjustMargin( rowColumn7, "true" );
	UxPutAdjustLast( rowColumn7, "false" );
	UxPutEntryAlignment( rowColumn7, "alignment_beginning" );
	UxPutBorderWidth( rowColumn7, 0 );
	UxPutShadowThickness( rowColumn7, 0 );
	UxPutLabelString( rowColumn7, "" );
	UxPutEntryBorder( rowColumn7, 0 );
	UxPutBackground( rowColumn7, WindowBackground );
	UxPutRadioBehavior( rowColumn7, "true" );
	UxPutHeight( rowColumn7, 96 );
	UxPutWidth( rowColumn7, 106 );
	UxPutY( rowColumn7, 98 );
	UxPutX( rowColumn7, 16 );

	UxPutTranslations( toggleButton16, transTable22 );
	UxPutSelectColor( toggleButton16, "Yellow" );
	UxPutLabelString( toggleButton16, "Offsets Window 1:" );
	UxPutForeground( toggleButton16, TextForeground );
	UxPutFontList( toggleButton16, TextFont );
	UxPutBackground( toggleButton16, WindowBackground );
	UxPutRecomputeSize( toggleButton16, "false" );
	UxPutHeight( toggleButton16, 35 );
	UxPutWidth( toggleButton16, 145 );
	UxPutY( toggleButton16, -2 );
	UxPutX( toggleButton16, -16 );

	UxPutTranslations( toggleButton18, transTable22 );
	UxPutSelectColor( toggleButton18, "Yellow" );
	UxPutLabelString( toggleButton18, "Offsets Window 2:" );
	UxPutForeground( toggleButton18, TextForeground );
	UxPutFontList( toggleButton18, TextFont );
	UxPutBackground( toggleButton18, WindowBackground );
	UxPutRecomputeSize( toggleButton18, "false" );
	UxPutHeight( toggleButton18, 35 );
	UxPutWidth( toggleButton18, 141 );
	UxPutY( toggleButton18, 26 );
	UxPutX( toggleButton18, -16 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_SkyShell()
{
	/* Create the swidgets */

	SkyShell = UxCreateTransientShell( "SkyShell", NO_PARENT );
	UxPutContext( SkyShell, UxSkyShellContext );

	form33 = UxCreateForm( "form33", SkyShell );
	form34 = UxCreateForm( "form34", form33 );
	pb_search_search8 = UxCreatePushButton( "pb_search_search8", form34 );
	pushButton6 = UxCreatePushButton( "pushButton6", form34 );
	pb_search_plot19 = UxCreatePushButton( "pb_search_plot19", form34 );
	pb_search_plot20 = UxCreatePushButton( "pb_search_plot20", form34 );
	pb_search_search9 = UxCreatePushButton( "pb_search_search9", form34 );
	shelp_search6 = UxCreateText( "shelp_search6", form33 );
	separator28 = UxCreateSeparator( "separator28", form33 );
	separator29 = UxCreateSeparator( "separator29", form33 );
	tf_thres9 = UxCreateTextField( "tf_thres9", form33 );
	label100 = UxCreateLabel( "label100", form33 );
	pb_main_search23 = UxCreatePushButton( "pb_main_search23", form33 );
	label101 = UxCreateLabel( "label101", form33 );
	tf_thres10 = UxCreateTextField( "tf_thres10", form33 );
	pb_main_search24 = UxCreatePushButton( "pb_main_search24", form33 );
	form35 = UxCreateForm( "form35", form33 );
	label24 = UxCreateLabel( "label24", form35 );
	tf_alpha22 = UxCreateTextField( "tf_alpha22", form35 );
	tf_alpha23 = UxCreateTextField( "tf_alpha23", form35 );
	label96 = UxCreateLabel( "label96", form35 );
	form36 = UxCreateForm( "form36", form33 );
	label97 = UxCreateLabel( "label97", form36 );
	tf_alpha24 = UxCreateTextField( "tf_alpha24", form36 );
	tf_alpha25 = UxCreateTextField( "tf_alpha25", form36 );
	label98 = UxCreateLabel( "label98", form36 );
	toggleButton19 = UxCreateToggleButton( "toggleButton19", form33 );
	rowColumn7 = UxCreateRowColumn( "rowColumn7", form33 );
	toggleButton16 = UxCreateToggleButton( "toggleButton16", rowColumn7 );
	toggleButton18 = UxCreateToggleButton( "toggleButton18", rowColumn7 );

	_Uxinit_SkyShell();

	/* Create the X widgets */

	UxCreateWidget( SkyShell );
	UxCreateWidget( form33 );
	UxCreateWidget( form34 );
	UxCreateWidget( pb_search_search8 );
	UxCreateWidget( pushButton6 );
	UxCreateWidget( pb_search_plot19 );
	UxCreateWidget( pb_search_plot20 );
	UxCreateWidget( pb_search_search9 );
	UxCreateWidget( shelp_search6 );
	UxCreateWidget( separator28 );
	UxCreateWidget( separator29 );
	UxCreateWidget( tf_thres9 );
	UxCreateWidget( label100 );
	UxCreateWidget( pb_main_search23 );
	UxCreateWidget( label101 );
	UxCreateWidget( tf_thres10 );
	UxCreateWidget( pb_main_search24 );
	UxCreateWidget( form35 );
	UxPutBottomOffset( label24, 2 );
	UxPutBottomAttachment( label24, "attach_form" );
	UxCreateWidget( label24 );

	UxPutBottomOffset( tf_alpha22, 2 );
	UxPutBottomAttachment( tf_alpha22, "attach_form" );
	UxCreateWidget( tf_alpha22 );

	UxPutBottomOffset( tf_alpha23, 2 );
	UxPutBottomAttachment( tf_alpha23, "attach_form" );
	UxCreateWidget( tf_alpha23 );

	UxPutBottomOffset( label96, 2 );
	UxPutBottomAttachment( label96, "attach_form" );
	UxCreateWidget( label96 );

	UxCreateWidget( form36 );
	UxPutBottomOffset( label97, 2 );
	UxPutBottomAttachment( label97, "attach_form" );
	UxCreateWidget( label97 );

	UxPutBottomOffset( tf_alpha24, 2 );
	UxPutBottomAttachment( tf_alpha24, "attach_form" );
	UxCreateWidget( tf_alpha24 );

	UxPutBottomOffset( tf_alpha25, 2 );
	UxPutBottomAttachment( tf_alpha25, "attach_form" );
	UxCreateWidget( tf_alpha25 );

	UxPutBottomOffset( label98, 2 );
	UxPutBottomAttachment( label98, "attach_form" );
	UxCreateWidget( label98 );

	UxPutLeftOffset( toggleButton19, 20 );
	UxPutLeftAttachment( toggleButton19, "attach_form" );
	UxCreateWidget( toggleButton19 );

	UxCreateWidget( rowColumn7 );
	UxCreateWidget( toggleButton16 );
	UxCreateWidget( toggleButton18 );

	UxAddCallback( pb_search_search8, XmNactivateCallback,
			activateCB_pb_search_search8,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( pushButton6, XmNactivateCallback,
			activateCB_pushButton6,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( pb_search_plot19, XmNactivateCallback,
			activateCB_pb_search_plot19,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( pb_search_plot20, XmNactivateCallback,
			activateCB_pb_search_plot20,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( pb_search_search9, XmNactivateCallback,
			activateCB_pb_search_search9,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( tf_thres9, XmNlosingFocusCallback,
			losingFocusCB_tf_thres9,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( pb_main_search23, XmNactivateCallback,
			activateCB_pb_main_search23,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( tf_thres10, XmNlosingFocusCallback,
			losingFocusCB_tf_thres10,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( pb_main_search24, XmNactivateCallback,
			activateCB_pb_main_search24,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( tf_alpha22, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha22,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( tf_alpha23, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha23,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( tf_alpha24, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha24,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( tf_alpha25, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha25,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( toggleButton19, XmNvalueChangedCallback,
			valueChangedCB_toggleButton19,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( toggleButton16, XmNarmCallback,
			armCB_toggleButton16,
			(XtPointer) UxSkyShellContext );
	UxAddCallback( toggleButton16, XmNvalueChangedCallback,
			valueChangedCB_toggleButton16,
			(XtPointer) UxSkyShellContext );

	UxAddCallback( toggleButton18, XmNarmCallback,
			armCB_toggleButton18,
			(XtPointer) UxSkyShellContext );
	UxAddCallback( toggleButton18, XmNvalueChangedCallback,
			valueChangedCB_toggleButton18,
			(XtPointer) UxSkyShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( SkyShell );

	return ( SkyShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_SkyShell()
{
	swidget                 rtrn;
	_UxCSkyShell            *UxContext;

	UxSkyShellContext = UxContext =
		(_UxCSkyShell *) UxMalloc( sizeof(_UxCSkyShell) );

	rtrn = _Uxbuild_SkyShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_SkyShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "UpdateDirectory", action_UpdateDirectory },
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_SkyShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

