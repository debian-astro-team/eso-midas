/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	extin_dialog.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxextin_dialog;
	swidget	Uxform16;
	swidget	Uxform17;
	swidget	UxpushButton34;
	swidget	UxpushButton35;
	swidget	Uxlb_output_extin;
	swidget	Uxtf_output_extin;
	swidget	Uxlb_airmass;
	swidget	Uxtf_airmass;
} _UxCextin_dialog;

#define extin_dialog            UxExtin_dialogContext->Uxextin_dialog
#define form16                  UxExtin_dialogContext->Uxform16
#define form17                  UxExtin_dialogContext->Uxform17
#define pushButton34            UxExtin_dialogContext->UxpushButton34
#define pushButton35            UxExtin_dialogContext->UxpushButton35
#define lb_output_extin         UxExtin_dialogContext->Uxlb_output_extin
#define tf_output_extin         UxExtin_dialogContext->Uxtf_output_extin
#define lb_airmass              UxExtin_dialogContext->Uxlb_airmass
#define tf_airmass              UxExtin_dialogContext->Uxtf_airmass

static _UxCextin_dialog	*UxExtin_dialogContext;

extern void CallbackDialog();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_extin_dialog();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton34( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCextin_dialog        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtin_dialogContext;
	UxExtin_dialogContext = UxContext =
			(_UxCextin_dialog *) UxGetContext( UxThisWidget );
	{
	CallbackDialog();
	}
	UxExtin_dialogContext = UxSaveCtx;
}

static void	activateCB_pushButton35( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCextin_dialog        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtin_dialogContext;
	UxExtin_dialogContext = UxContext =
			(_UxCextin_dialog *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("extin_dialog"));
	}
	UxExtin_dialogContext = UxSaveCtx;
}

static void	activateCB_tf_output_extin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCextin_dialog        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtin_dialogContext;
	UxExtin_dialogContext = UxContext =
			(_UxCextin_dialog *) UxGetContext( UxThisWidget );
	{
	CallbackDialog();
	}
	UxExtin_dialogContext = UxSaveCtx;
}

static void	losingFocusCB_tf_output_extin( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCextin_dialog        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtin_dialogContext;
	UxExtin_dialogContext = UxContext =
			(_UxCextin_dialog *) UxGetContext( UxThisWidget );
	{
	char *text;
	extern char Session[];
	
	text = XmTextGetString(UxWidget);
	strcpy(Session, text);
	
	XtFree(text);
	}
	UxExtin_dialogContext = UxSaveCtx;
}

static void	activateCB_tf_airmass( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCextin_dialog        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtin_dialogContext;
	UxExtin_dialogContext = UxContext =
			(_UxCextin_dialog *) UxGetContext( UxThisWidget );
	{
	CallbackDialog();
	}
	UxExtin_dialogContext = UxSaveCtx;
}

static void	losingFocusCB_tf_airmass( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCextin_dialog        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxExtin_dialogContext;
	UxExtin_dialogContext = UxContext =
			(_UxCextin_dialog *) UxGetContext( UxThisWidget );
	{
	char *text;
	extern char Session[];
	
	text = XmTextGetString(UxWidget);
	strcpy(Session, text);
	
	XtFree(text);
	}
	UxExtin_dialogContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_extin_dialog()
{
	UxPutBackground( extin_dialog, WindowBackground );
	UxPutGeometry( extin_dialog, "+100+100" );
	UxPutKeyboardFocusPolicy( extin_dialog, "pointer" );
	UxPutTitle( extin_dialog, "" );
	UxPutHeight( extin_dialog, 135 );
	UxPutWidth( extin_dialog, 342 );
	UxPutY( extin_dialog, 121 );
	UxPutX( extin_dialog, 102 );

	UxPutBackground( form16, WindowBackground );
	UxPutHeight( form16, 138 );
	UxPutWidth( form16, 408 );
	UxPutY( form16, 0 );
	UxPutX( form16, 0 );
	UxPutUnitType( form16, "pixels" );
	UxPutResizePolicy( form16, "resize_none" );

	UxPutBackground( form17, ButtonBackground );
	UxPutHeight( form17, 40 );
	UxPutWidth( form17, 340 );
	UxPutY( form17, 96 );
	UxPutX( form17, 0 );
	UxPutResizePolicy( form17, "resize_none" );

	UxPutLabelString( pushButton34, "Ok" );
	UxPutForeground( pushButton34, ApplyForeground );
	UxPutFontList( pushButton34, BoldTextFont );
	UxPutBackground( pushButton34, ButtonBackground );
	UxPutHeight( pushButton34, 30 );
	UxPutWidth( pushButton34, 80 );
	UxPutY( pushButton34, 4 );
	UxPutX( pushButton34, 8 );

	UxPutLabelString( pushButton35, "Cancel" );
	UxPutForeground( pushButton35, CancelForeground );
	UxPutFontList( pushButton35, BoldTextFont );
	UxPutBackground( pushButton35, ButtonBackground );
	UxPutHeight( pushButton35, 30 );
	UxPutWidth( pushButton35, 80 );
	UxPutY( pushButton35, 4 );
	UxPutX( pushButton35, 100 );

	UxPutForeground( lb_output_extin, TextForeground );
	UxPutAlignment( lb_output_extin, "alignment_beginning" );
	UxPutLabelString( lb_output_extin, "Output image :" );
	UxPutFontList( lb_output_extin, TextFont );
	UxPutBackground( lb_output_extin, LabelBackground );
	UxPutHeight( lb_output_extin, 30 );
	UxPutWidth( lb_output_extin, 104 );
	UxPutY( lb_output_extin, 56 );
	UxPutX( lb_output_extin, 10 );

	UxPutForeground( tf_output_extin, TextForeground );
	UxPutHighlightOnEnter( tf_output_extin, "true" );
	UxPutFontList( tf_output_extin, TextFont );
	UxPutBackground( tf_output_extin, TextBackground );
	UxPutHeight( tf_output_extin, 34 );
	UxPutWidth( tf_output_extin, 210 );
	UxPutY( tf_output_extin, 54 );
	UxPutX( tf_output_extin, 118 );

	UxPutForeground( lb_airmass, TextForeground );
	UxPutAlignment( lb_airmass, "alignment_beginning" );
	UxPutLabelString( lb_airmass, "Airmass value :" );
	UxPutFontList( lb_airmass, TextFont );
	UxPutBackground( lb_airmass, LabelBackground );
	UxPutHeight( lb_airmass, 30 );
	UxPutWidth( lb_airmass, 106 );
	UxPutY( lb_airmass, 20 );
	UxPutX( lb_airmass, 10 );

	UxPutForeground( tf_airmass, TextForeground );
	UxPutHighlightOnEnter( tf_airmass, "true" );
	UxPutFontList( tf_airmass, TextFont );
	UxPutBackground( tf_airmass, TextBackground );
	UxPutHeight( tf_airmass, 34 );
	UxPutWidth( tf_airmass, 108 );
	UxPutY( tf_airmass, 18 );
	UxPutX( tf_airmass, 118 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_extin_dialog()
{
	/* Create the swidgets */

	extin_dialog = UxCreateApplicationShell( "extin_dialog", NO_PARENT );
	UxPutContext( extin_dialog, UxExtin_dialogContext );

	form16 = UxCreateForm( "form16", extin_dialog );
	form17 = UxCreateForm( "form17", form16 );
	pushButton34 = UxCreatePushButton( "pushButton34", form17 );
	pushButton35 = UxCreatePushButton( "pushButton35", form17 );
	lb_output_extin = UxCreateLabel( "lb_output_extin", form16 );
	tf_output_extin = UxCreateTextField( "tf_output_extin", form16 );
	lb_airmass = UxCreateLabel( "lb_airmass", form16 );
	tf_airmass = UxCreateTextField( "tf_airmass", form16 );

	_Uxinit_extin_dialog();

	/* Create the X widgets */

	UxCreateWidget( extin_dialog );
	UxCreateWidget( form16 );
	UxCreateWidget( form17 );
	UxCreateWidget( pushButton34 );
	UxCreateWidget( pushButton35 );
	UxCreateWidget( lb_output_extin );
	UxCreateWidget( tf_output_extin );
	UxCreateWidget( lb_airmass );
	UxCreateWidget( tf_airmass );

	UxAddCallback( pushButton34, XmNactivateCallback,
			activateCB_pushButton34,
			(XtPointer) UxExtin_dialogContext );

	UxAddCallback( pushButton35, XmNactivateCallback,
			activateCB_pushButton35,
			(XtPointer) UxExtin_dialogContext );

	UxAddCallback( tf_output_extin, XmNactivateCallback,
			activateCB_tf_output_extin,
			(XtPointer) UxExtin_dialogContext );
	UxAddCallback( tf_output_extin, XmNlosingFocusCallback,
			losingFocusCB_tf_output_extin,
			(XtPointer) UxExtin_dialogContext );

	UxAddCallback( tf_airmass, XmNactivateCallback,
			activateCB_tf_airmass,
			(XtPointer) UxExtin_dialogContext );
	UxAddCallback( tf_airmass, XmNlosingFocusCallback,
			losingFocusCB_tf_airmass,
			(XtPointer) UxExtin_dialogContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( extin_dialog );

	return ( extin_dialog );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_extin_dialog()
{
	swidget                 rtrn;
	_UxCextin_dialog        *UxContext;

	UxExtin_dialogContext = UxContext =
		(_UxCextin_dialog *) UxMalloc( sizeof(_UxCextin_dialog) );

	rtrn = _Uxbuild_extin_dialog();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_extin_dialog()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_extin_dialog();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

