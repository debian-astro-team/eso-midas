/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY

.VERSION
 090825         last modif


===========================================================================*/

/*------------------------------------------------------------
 * This is the project main program file for Ux generated  
 * code. You may add application dependent source code
 * at the appropriate places. 
 * 			     
 * Do not modify the statements preceded by the dollar
 * sign ($), these statements will be replaced with
 * the appropriate source code when the main program is
 * automatically generated. 
 *			
 * $Date: 2009-08-25 15:17:06 $  		$Revision: 1.2 $ 
 *-----------------------------------------------------------*/

#ifdef XOPEN_CATALOG
#include <locale.h>
#endif

#include <UxLib.h>


/*----------------------------------------------
 * Insert application global declarations here
 *---------------------------------------------*/
#include <gl_defs.h>
#include <spec_defs.h>
#include <main_defs.h>
#include <main_decl.h>

#include <stdlib.h>
#include <signal.h>
#include <StdResources.h>

#include <midas_def.h>
#include    <osyparms.h>

/* variables for file list management */
swidget FileListInterface, TextFieldSwidget;
Widget FileListWidget;
int FillTextField;
char DirSpecs[MAXLINE];

extern void SetResources(), WInit_widgets(), InitAllFields();

extern int FilterFiles();

extern swidget create_file_selection();
extern swidget create_file_list();
extern swidget create_lincat_list();


int main(argc,argv)
	int 	argc;
	char 	*argv[];
{
	/*-----------------------------------------------------------
	 * Declarations.
	 * The default identifier - mainIface will only be declared  
	 * if the interface function is global and of type swidget. 
	 * To change the identifier to a different name, modify the
	 * string mainIface in the file "main.dat".  If "mainIface"
	 * is declared, it will be used below where the return value 
	 * of  PJ_INTERFACE_FUNCTION_CALL will be assigned to it.
         *-----------------------------------------------------------*/ 

        int narg, fsmode = 0;
	int cntflag = 1, logflag = 2, dispflag = 1;

  	swidget mainIface;

	/*--------------------------------
	 * Interface function declaration
	 *-------------------------------*/	

 	swidget create_ApplicWindow();

	

	/*---------------------
	 * Initialize Program
	 *--------------------*/

#ifdef XOPEN_CATALOG
	setlocale(LC_ALL, "");
#endif

  	UxTopLevel = UxInitialize("XEchelle", &argc, argv);

	/*------------------------------------------------------
	 * Insert initialization code for your application here
	 *-----------------------------------------------------*/

        SetResources(argc, argv);
	SCSPRO("XEchelle");
        osscatch(SIGINT,SIG_IGN);       /* Ignore INT signals (Ctrl-C) */
        SCECNT("PUT", &cntflag, &logflag, &dispflag);  
        MonitorPid = atoi(argv[1]);

        for (narg = 1; narg < argc; narg++)
           if ( (argv[narg][0] == '-') && strcmp(argv[narg], "-fs") == 0 ) 
               fsmode = 1; /* MOTIF File Sel. */

        if (fsmode == 0)
           FileListInterface = create_file_list();
        else
           FileListInterface = create_file_selection();

        create_lincat_list();

        /*----------------------------------------------------------------
	 * Create and popup the first window of the interface.  The 	 
	 * return value can be used in the popdown or destroy functions.
	 * The swidget return value of  PJ_INTERFACE_FUNCTION_CALL will
	 * be assigned to "mainIface" from  PJ_INTERFACE_RETVAL_TYPE. 
	 *---------------------------------------------------------------*/

	mainIface = create_ApplicWindow();

	UxPopupInterface(mainIface, no_grab);

        /* Initializes content of file list */
        WInit_widgets(
         UxGetWidget(UxFindSwidget("tf_session1")),
         UxGetWidget(UxFindSwidget("tf_session")),
         UxGetWidget(UxFindSwidget("scrolledList1")),
         UxGetWidget(UxFindSwidget("scrolledList4"))
        );

        FilterFiles(".");
        InitAllFields();

	/*-------------------------
	 * Enter the event loop 
	 *------------------------*/

  	UxMainLoop();

return 0;
}
