/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	CalibShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxPushBG.h"
#include "UxPushB.h"
#include "UxText.h"
#include "UxSep.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxForm.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxCalibShell;
	swidget	Uxform3;
	swidget	UxrowColumn2;
	swidget	Uxrb_wlcmtd_iden;
	swidget	Uxrb_wlcmtd_gues;
	swidget	Uxlabel14;
	swidget	Uxlabel16;
	swidget	Uxtf_tol;
	swidget	Uxlabel17;
	swidget	Uxtf_dcx1;
	swidget	Uxtf_alpha;
	swidget	Uxlabel19;
	swidget	Uxlabel20;
	swidget	Uxtf_maxdev;
	swidget	Uxguess_session_label;
	swidget	Uxseparator4;
	swidget	Uxshelp_calib;
	swidget	Uxseparator5;
	swidget	Uxtf_wlcniter1;
	swidget	Uxtf_wlcniter2;
	swidget	Uxtg_coropt;
	swidget	Uxform20;
	swidget	Uxpb_calib_calibrate;
	swidget	UxpushButton40;
	swidget	Uxpb_calib_disper;
	swidget	Uxpb_calib_resid;
	swidget	Uxpb_calib_spec;
	swidget	Uxlabel8;
	swidget	Uxtf_maxdev1;
	swidget	Uxtf_maxdev2;
	swidget	Uxtf_guess1;
	swidget	Uxtf_guess2;
	swidget	Uxlabel15;
	swidget	Uxlabel26;
	swidget	UxrowColumn24;
	swidget	Uxrb_wlcmtd_iden1;
	swidget	Uxrb_wlcmtd_gues4;
	swidget	Uxrb_wlcmtd_gues5;
	swidget	Uxrb_wlcmtd_gues6;
	swidget	Uxrb_wlcmtd_gues7;
	swidget	UxrowColumn25;
	swidget	Uxrb_wlcmtd_iden2;
	swidget	Uxrb_wlcmtd_gues1;
	swidget	Uxseparator13;
	swidget	Uxseparator14;
	swidget	Uxseparator15;
	swidget	Uxseparator16;
	swidget	Uxseparator17;
	swidget	Uxseparator18;
	swidget	Uxseparator19;
	swidget	Uxlabel28;
	swidget	Uxlabel31;
	swidget	Uxmenu2_p1;
	swidget	Uxmn_tol_angstroms1;
	swidget	Uxmn_tol_pixels1;
	swidget	Uxmn_tol1;
	swidget	Uxlabel18;
	swidget	Uxpb_main_search33;
} _UxCCalibShell;

#define CalibShell              UxCalibShellContext->UxCalibShell
#define form3                   UxCalibShellContext->Uxform3
#define rowColumn2              UxCalibShellContext->UxrowColumn2
#define rb_wlcmtd_iden          UxCalibShellContext->Uxrb_wlcmtd_iden
#define rb_wlcmtd_gues          UxCalibShellContext->Uxrb_wlcmtd_gues
#define label14                 UxCalibShellContext->Uxlabel14
#define label16                 UxCalibShellContext->Uxlabel16
#define tf_tol                  UxCalibShellContext->Uxtf_tol
#define label17                 UxCalibShellContext->Uxlabel17
#define tf_dcx1                 UxCalibShellContext->Uxtf_dcx1
#define tf_alpha                UxCalibShellContext->Uxtf_alpha
#define label19                 UxCalibShellContext->Uxlabel19
#define label20                 UxCalibShellContext->Uxlabel20
#define tf_maxdev               UxCalibShellContext->Uxtf_maxdev
#define guess_session_label     UxCalibShellContext->Uxguess_session_label
#define separator4              UxCalibShellContext->Uxseparator4
#define shelp_calib             UxCalibShellContext->Uxshelp_calib
#define separator5              UxCalibShellContext->Uxseparator5
#define tf_wlcniter1            UxCalibShellContext->Uxtf_wlcniter1
#define tf_wlcniter2            UxCalibShellContext->Uxtf_wlcniter2
#define tg_coropt               UxCalibShellContext->Uxtg_coropt
#define form20                  UxCalibShellContext->Uxform20
#define pb_calib_calibrate      UxCalibShellContext->Uxpb_calib_calibrate
#define pushButton40            UxCalibShellContext->UxpushButton40
#define pb_calib_disper         UxCalibShellContext->Uxpb_calib_disper
#define pb_calib_resid          UxCalibShellContext->Uxpb_calib_resid
#define pb_calib_spec           UxCalibShellContext->Uxpb_calib_spec
#define label8                  UxCalibShellContext->Uxlabel8
#define tf_maxdev1              UxCalibShellContext->Uxtf_maxdev1
#define tf_maxdev2              UxCalibShellContext->Uxtf_maxdev2
#define tf_guess1               UxCalibShellContext->Uxtf_guess1
#define tf_guess2               UxCalibShellContext->Uxtf_guess2
#define label15                 UxCalibShellContext->Uxlabel15
#define label26                 UxCalibShellContext->Uxlabel26
#define rowColumn24             UxCalibShellContext->UxrowColumn24
#define rb_wlcmtd_iden1         UxCalibShellContext->Uxrb_wlcmtd_iden1
#define rb_wlcmtd_gues4         UxCalibShellContext->Uxrb_wlcmtd_gues4
#define rb_wlcmtd_gues5         UxCalibShellContext->Uxrb_wlcmtd_gues5
#define rb_wlcmtd_gues6         UxCalibShellContext->Uxrb_wlcmtd_gues6
#define rb_wlcmtd_gues7         UxCalibShellContext->Uxrb_wlcmtd_gues7
#define rowColumn25             UxCalibShellContext->UxrowColumn25
#define rb_wlcmtd_iden2         UxCalibShellContext->Uxrb_wlcmtd_iden2
#define rb_wlcmtd_gues1         UxCalibShellContext->Uxrb_wlcmtd_gues1
#define separator13             UxCalibShellContext->Uxseparator13
#define separator14             UxCalibShellContext->Uxseparator14
#define separator15             UxCalibShellContext->Uxseparator15
#define separator16             UxCalibShellContext->Uxseparator16
#define separator17             UxCalibShellContext->Uxseparator17
#define separator18             UxCalibShellContext->Uxseparator18
#define separator19             UxCalibShellContext->Uxseparator19
#define label28                 UxCalibShellContext->Uxlabel28
#define label31                 UxCalibShellContext->Uxlabel31
#define menu2_p1                UxCalibShellContext->Uxmenu2_p1
#define mn_tol_angstroms1       UxCalibShellContext->Uxmn_tol_angstroms1
#define mn_tol_pixels1          UxCalibShellContext->Uxmn_tol_pixels1
#define mn_tol1                 UxCalibShellContext->Uxmn_tol1
#define label18                 UxCalibShellContext->Uxlabel18
#define pb_main_search33        UxCalibShellContext->Uxpb_main_search33

static _UxCCalibShell	*UxCalibShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();
extern void  WriteKeyword(), GetExtendedHelp(), SelectList();


extern int RadioSet(), PopupList(), AppendDialogText();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable6 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_CalibShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	DisplayShortHelp(UxWidget);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	/*
	UxPutText(UxFindSwidget("shelp_main"), "");
	UxPutText(UxFindSwidget("shelp_search"), "");
	UxPutText(UxFindSwidget("shelp_calib"), "");
	UxPutText(UxFindSwidget("shelp_rebin"), "");
	UxPutText(UxFindSwidget("shelp_extract"), "");
	UxPutText(UxFindSwidget("shelp_flux"), "");
	*/
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxCalibShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxCalibShellContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	armCB_rb_wlcmtd_iden( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	RadioSet ( UxWidget ) ;
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_wlcmtd_iden( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	armCB_rb_wlcmtd_gues( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	RadioSet ( UxWidget ) ;
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_wlcmtd_gues( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_tol( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_dcx1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_maxdev( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wlcniter1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wlcniter2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_tg_coropt( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern int UpdateToggle;
	
	if ( !UpdateToggle )
	    return;
	
	if ( XmToggleButtonGetState(UxWidget) )
	    AppendDialogText("set/long CORVISU=YES COROPT=YES");
	else
	    AppendDialogText("set/long CORVISU=NO COROPT=NO");
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_calibrate( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pushButton40( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("CalibShell"));
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_disper( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_resid( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	 MidasCommand(UxWidget);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_calib_spec( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_maxdev1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_maxdev2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_guess1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_guess2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	armCB_rb_wlcmtd_iden1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	RadioSet(UxWidget);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_wlcmtd_iden1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	armCB_rb_wlcmtd_gues4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	RadioSet(UxWidget);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_wlcmtd_gues4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	RadioSet ( UxWidget ) ;
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	armCB_rb_wlcmtd_gues5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	RadioSet(UxWidget);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_wlcmtd_gues5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	armCB_rb_wlcmtd_gues6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	RadioSet(UxWidget);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_wlcmtd_gues6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	armCB_rb_wlcmtd_gues7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	RadioSet(UxWidget);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_wlcmtd_gues7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	armCB_rb_wlcmtd_iden2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	RadioSet ( UxWidget ) ;
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_wlcmtd_iden2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	armCB_rb_wlcmtd_gues1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	RadioSet ( UxWidget ) ;
	UxCalibShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_wlcmtd_gues1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_angstroms1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char text[32];
	extern float Tol;
	extern int TolPixels;
	
	TolPixels = FALSE;
	
	sprintf(text, "%f", -Tol);
	WriteKeyword(text, K_TOL);
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_pixels1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char text[32];
	extern float Tol;
	extern int TolPixels;
	
	TolPixels = TRUE;
	
	sprintf(text, "%f", Tol);
	WriteKeyword(text, K_TOL);
	
	XtFree(text);
	}
	UxCalibShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search33( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCCalibShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxCalibShellContext;
	UxCalibShellContext = UxContext =
			(_UxCCalibShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxCalibShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_CalibShell()
{
	UxPutGeometry( CalibShell, "+10+60" );
	UxPutDefaultFontList( CalibShell, TextFont );
	UxPutKeyboardFocusPolicy( CalibShell, "pointer" );
	UxPutIconName( CalibShell, "Calibration" );
	UxPutHeight( CalibShell, 520 );
	UxPutWidth( CalibShell, 520 );
	UxPutY( CalibShell, 327 );
	UxPutX( CalibShell, 29 );

	UxPutBackground( form3, "grey80" );
	UxPutHeight( form3, 616 );
	UxPutWidth( form3, 600 );
	UxPutY( form3, 0 );
	UxPutX( form3, 2 );
	UxPutUnitType( form3, "pixels" );
	UxPutResizePolicy( form3, "resize_none" );

	UxPutTranslations( rowColumn2, transTable6 );
	UxPutPacking( rowColumn2, "pack_tight" );
	UxPutOrientation( rowColumn2, "vertical" );
	UxPutIsAligned( rowColumn2, "true" );
	UxPutEntryAlignment( rowColumn2, "alignment_beginning" );
	UxPutBorderWidth( rowColumn2, 0 );
	UxPutShadowThickness( rowColumn2, 0 );
	UxPutLabelString( rowColumn2, "" );
	UxPutEntryBorder( rowColumn2, 0 );
	UxPutBackground( rowColumn2, "grey80" );
	UxPutRadioBehavior( rowColumn2, "true" );
	UxPutHeight( rowColumn2, 31 );
	UxPutWidth( rowColumn2, 402 );
	UxPutY( rowColumn2, 304 );
	UxPutX( rowColumn2, 368 );

	UxPutTranslations( rb_wlcmtd_iden, transTable6 );
	UxPutIndicatorSize( rb_wlcmtd_iden, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_iden, "true" );
	UxPutForeground( rb_wlcmtd_iden, TextForeground );
	UxPutSelectColor( rb_wlcmtd_iden, SelectColor );
	UxPutSet( rb_wlcmtd_iden, "true" );
	UxPutLabelString( rb_wlcmtd_iden, "Yes" );
	UxPutFontList( rb_wlcmtd_iden, TextFont );
	UxPutBackground( rb_wlcmtd_iden, WindowBackground );
	UxPutHeight( rb_wlcmtd_iden, 113 );
	UxPutWidth( rb_wlcmtd_iden, 77 );
	UxPutY( rb_wlcmtd_iden, 3 );
	UxPutX( rb_wlcmtd_iden, 3 );

	UxPutTranslations( rb_wlcmtd_gues, transTable6 );
	UxPutIndicatorSize( rb_wlcmtd_gues, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_gues, "true" );
	UxPutSet( rb_wlcmtd_gues, "false" );
	UxPutForeground( rb_wlcmtd_gues, TextForeground );
	UxPutSelectColor( rb_wlcmtd_gues, SelectColor );
	UxPutLabelString( rb_wlcmtd_gues, "No" );
	UxPutFontList( rb_wlcmtd_gues, TextFont );
	UxPutBackground( rb_wlcmtd_gues, WindowBackground );
	UxPutHeight( rb_wlcmtd_gues, 32 );
	UxPutWidth( rb_wlcmtd_gues, 93 );
	UxPutY( rb_wlcmtd_gues, 34 );
	UxPutX( rb_wlcmtd_gues, 3 );

	UxPutForeground( label14, TextForeground );
	UxPutAlignment( label14, "alignment_beginning" );
	UxPutLabelString( label14, "Method" );
	UxPutFontList( label14, TextFont );
	UxPutBackground( label14, WindowBackground );
	UxPutHeight( label14, 18 );
	UxPutWidth( label14, 60 );
	UxPutY( label14, 56 );
	UxPutX( label14, 14 );

	UxPutForeground( label16, TextForeground );
	UxPutAlignment( label16, "alignment_beginning" );
	UxPutLabelString( label16, "Tolerance :" );
	UxPutFontList( label16, TextFont );
	UxPutBackground( label16, LabelBackground );
	UxPutHeight( label16, 30 );
	UxPutWidth( label16, 130 );
	UxPutY( label16, 124 );
	UxPutX( label16, 14 );

	UxPutForeground( tf_tol, TextForeground );
	UxPutHighlightOnEnter( tf_tol, "true" );
	UxPutFontList( tf_tol, TextFont );
	UxPutBackground( tf_tol, TextBackground );
	UxPutHeight( tf_tol, 34 );
	UxPutWidth( tf_tol, 100 );
	UxPutY( tf_tol, 120 );
	UxPutX( tf_tol, 238 );

	UxPutForeground( label17, TextForeground );
	UxPutAlignment( label17, "alignment_beginning" );
	UxPutLabelString( label17, "Degree of polynomial :" );
	UxPutFontList( label17, TextFont );
	UxPutBackground( label17, LabelBackground );
	UxPutHeight( label17, 30 );
	UxPutWidth( label17, 200 );
	UxPutY( label17, 164 );
	UxPutX( label17, 14 );

	UxPutForeground( tf_dcx1, TextForeground );
	UxPutHighlightOnEnter( tf_dcx1, "true" );
	UxPutFontList( tf_dcx1, TextFont );
	UxPutBackground( tf_dcx1, TextBackground );
	UxPutHeight( tf_dcx1, 34 );
	UxPutWidth( tf_dcx1, 100 );
	UxPutY( tf_dcx1, 158 );
	UxPutX( tf_dcx1, 238 );

	UxPutForeground( tf_alpha, TextForeground );
	UxPutHighlightOnEnter( tf_alpha, "true" );
	UxPutFontList( tf_alpha, TextFont );
	UxPutBackground( tf_alpha, TextBackground );
	UxPutHeight( tf_alpha, 32 );
	UxPutWidth( tf_alpha, 96 );
	UxPutY( tf_alpha, 238 );
	UxPutX( tf_alpha, 238 );

	UxPutForeground( label19, TextForeground );
	UxPutAlignment( label19, "alignment_beginning" );
	UxPutLabelString( label19, "Matching parameter :" );
	UxPutFontList( label19, TextFont );
	UxPutBackground( label19, LabelBackground );
	UxPutHeight( label19, 30 );
	UxPutWidth( label19, 200 );
	UxPutY( label19, 274 );
	UxPutX( label19, 14 );

	UxPutForeground( label20, TextForeground );
	UxPutAlignment( label20, "alignment_beginning" );
	UxPutLabelString( label20, "Maximum deviation (pixels) :" );
	UxPutFontList( label20, TextFont );
	UxPutBackground( label20, LabelBackground );
	UxPutHeight( label20, 30 );
	UxPutWidth( label20, 200 );
	UxPutY( label20, 354 );
	UxPutX( label20, 14 );

	UxPutForeground( tf_maxdev, TextForeground );
	UxPutHighlightOnEnter( tf_maxdev, "true" );
	UxPutFontList( tf_maxdev, TextFont );
	UxPutBackground( tf_maxdev, TextBackground );
	UxPutHeight( tf_maxdev, 34 );
	UxPutWidth( tf_maxdev, 100 );
	UxPutY( tf_maxdev, 312 );
	UxPutX( tf_maxdev, 238 );

	UxPutSensitive( guess_session_label, "false" );
	UxPutForeground( guess_session_label, TextForeground );
	UxPutAlignment( guess_session_label, "alignment_beginning" );
	UxPutLabelString( guess_session_label, "Initial guess :" );
	UxPutFontList( guess_session_label, TextFont );
	UxPutBackground( guess_session_label, LabelBackground );
	UxPutHeight( guess_session_label, 30 );
	UxPutWidth( guess_session_label, 100 );
	UxPutY( guess_session_label, 88 );
	UxPutX( guess_session_label, 14 );

	UxPutBackground( separator4, WindowBackground );
	UxPutHeight( separator4, 11 );
	UxPutWidth( separator4, 110 );
	UxPutY( separator4, 252 );
	UxPutX( separator4, 356 );

	UxPutFontList( shelp_calib, TextFont );
	UxPutEditable( shelp_calib, "false" );
	UxPutCursorPositionVisible( shelp_calib, "false" );
	UxPutBackground( shelp_calib, SHelpBackground );
	UxPutHeight( shelp_calib, 50 );
	UxPutWidth( shelp_calib, 510 );
	UxPutY( shelp_calib, 404 );
	UxPutX( shelp_calib, 8 );

	UxPutBackground( separator5, WindowBackground );
	UxPutHeight( separator5, 6 );
	UxPutWidth( separator5, 516 );
	UxPutY( separator5, 456 );
	UxPutX( separator5, 4 );

	UxPutForeground( tf_wlcniter1, TextForeground );
	UxPutHighlightOnEnter( tf_wlcniter1, "true" );
	UxPutFontList( tf_wlcniter1, TextFont );
	UxPutBackground( tf_wlcniter1, TextBackground );
	UxPutHeight( tf_wlcniter1, 34 );
	UxPutWidth( tf_wlcniter1, 48 );
	UxPutY( tf_wlcniter1, 196 );
	UxPutX( tf_wlcniter1, 238 );

	UxPutForeground( tf_wlcniter2, TextForeground );
	UxPutHighlightOnEnter( tf_wlcniter2, "true" );
	UxPutFontList( tf_wlcniter2, TextFont );
	UxPutBackground( tf_wlcniter2, TextBackground );
	UxPutHeight( tf_wlcniter2, 34 );
	UxPutWidth( tf_wlcniter2, 48 );
	UxPutY( tf_wlcniter2, 196 );
	UxPutX( tf_wlcniter2, 288 );

	UxPutIndicatorSize( tg_coropt, 16 );
	UxPutSensitive( tg_coropt, "false" );
	UxPutHighlightOnEnter( tg_coropt, "true" );
	UxPutForeground( tg_coropt, TextForeground );
	UxPutSelectColor( tg_coropt, SelectColor );
	UxPutSet( tg_coropt, "false" );
	UxPutLabelString( tg_coropt, "Compute Cross-correlation" );
	UxPutFontList( tg_coropt, TextFont );
	UxPutBackground( tg_coropt, WindowBackground );
	UxPutHeight( tg_coropt, 26 );
	UxPutWidth( tg_coropt, 206 );
	UxPutY( tg_coropt, 82 );
	UxPutX( tg_coropt, 302 );

	UxPutBackground( form20, ButtonBackground );
	UxPutHeight( form20, 48 );
	UxPutWidth( form20, 520 );
	UxPutY( form20, 466 );
	UxPutX( form20, 4 );
	UxPutResizePolicy( form20, "resize_none" );

	UxPutLabelString( pb_calib_calibrate, "Calibrate" );
	UxPutForeground( pb_calib_calibrate, ApplyForeground );
	UxPutFontList( pb_calib_calibrate, BoldTextFont );
	UxPutBackground( pb_calib_calibrate, ButtonBackground );
	UxPutHeight( pb_calib_calibrate, 30 );
	UxPutWidth( pb_calib_calibrate, 86 );
	UxPutY( pb_calib_calibrate, 6 );
	UxPutX( pb_calib_calibrate, 4 );

	UxPutLabelString( pushButton40, "Cancel" );
	UxPutForeground( pushButton40, CancelForeground );
	UxPutFontList( pushButton40, BoldTextFont );
	UxPutBackground( pushButton40, ButtonBackground );
	UxPutHeight( pushButton40, 30 );
	UxPutWidth( pushButton40, 86 );
	UxPutY( pushButton40, 6 );
	UxPutX( pushButton40, 430 );

	UxPutLabelString( pb_calib_disper, "Help" );
	UxPutForeground( pb_calib_disper, ButtonForeground );
	UxPutFontList( pb_calib_disper, BoldTextFont );
	UxPutBackground( pb_calib_disper, ButtonBackground );
	UxPutHeight( pb_calib_disper, 30 );
	UxPutWidth( pb_calib_disper, 86 );
	UxPutY( pb_calib_disper, 6 );
	UxPutX( pb_calib_disper, 344 );

	UxPutLabelString( pb_calib_resid, "Residuals" );
	UxPutForeground( pb_calib_resid, ButtonForeground );
	UxPutFontList( pb_calib_resid, BoldTextFont );
	UxPutBackground( pb_calib_resid, ButtonBackground );
	UxPutHeight( pb_calib_resid, 30 );
	UxPutWidth( pb_calib_resid, 86 );
	UxPutY( pb_calib_resid, 6 );
	UxPutX( pb_calib_resid, 92 );

	UxPutLabelString( pb_calib_spec, "Spectrum" );
	UxPutForeground( pb_calib_spec, ButtonForeground );
	UxPutFontList( pb_calib_spec, BoldTextFont );
	UxPutBackground( pb_calib_spec, ButtonBackground );
	UxPutHeight( pb_calib_spec, 30 );
	UxPutWidth( pb_calib_spec, 86 );
	UxPutY( pb_calib_spec, 6 );
	UxPutX( pb_calib_spec, 180 );

	UxPutForeground( label8, TextForeground );
	UxPutAlignment( label8, "alignment_beginning" );
	UxPutLabelString( label8, "Initial Tolerance (pixels) :" );
	UxPutFontList( label8, TextFont );
	UxPutBackground( label8, LabelBackground );
	UxPutHeight( label8, 30 );
	UxPutWidth( label8, 200 );
	UxPutY( label8, 314 );
	UxPutX( label8, 14 );

	UxPutForeground( tf_maxdev1, TextForeground );
	UxPutHighlightOnEnter( tf_maxdev1, "true" );
	UxPutFontList( tf_maxdev1, TextFont );
	UxPutBackground( tf_maxdev1, TextBackground );
	UxPutHeight( tf_maxdev1, 34 );
	UxPutWidth( tf_maxdev1, 100 );
	UxPutY( tf_maxdev1, 352 );
	UxPutX( tf_maxdev1, 238 );

	UxPutForeground( tf_maxdev2, TextForeground );
	UxPutHighlightOnEnter( tf_maxdev2, "true" );
	UxPutFontList( tf_maxdev2, TextFont );
	UxPutBackground( tf_maxdev2, TextBackground );
	UxPutHeight( tf_maxdev2, 34 );
	UxPutWidth( tf_maxdev2, 100 );
	UxPutY( tf_maxdev2, 272 );
	UxPutX( tf_maxdev2, 238 );

	UxPutTranslations( tf_guess1, "" );
	UxPutSensitive( tf_guess1, "true" );
	UxPutForeground( tf_guess1, TextForeground );
	UxPutHighlightOnEnter( tf_guess1, "true" );
	UxPutFontList( tf_guess1, TextFont );
	UxPutBackground( tf_guess1, TextBackground );
	UxPutHeight( tf_guess1, 36 );
	UxPutWidth( tf_guess1, 278 );
	UxPutY( tf_guess1, 6 );
	UxPutX( tf_guess1, 178 );

	UxPutTranslations( tf_guess2, "" );
	UxPutSensitive( tf_guess2, "false" );
	UxPutForeground( tf_guess2, TextForeground );
	UxPutHighlightOnEnter( tf_guess2, "true" );
	UxPutFontList( tf_guess2, TextFont );
	UxPutBackground( tf_guess2, TextBackground );
	UxPutHeight( tf_guess2, 36 );
	UxPutWidth( tf_guess2, 164 );
	UxPutY( tf_guess2, 80 );
	UxPutX( tf_guess2, 126 );

	UxPutForeground( label15, TextForeground );
	UxPutAlignment( label15, "alignment_beginning" );
	UxPutLabelString( label15, "Line Catalog:" );
	UxPutFontList( label15, TextFont );
	UxPutBackground( label15, LabelBackground );
	UxPutHeight( label15, 30 );
	UxPutWidth( label15, 210 );
	UxPutY( label15, 6 );
	UxPutX( label15, 14 );

	UxPutForeground( label26, TextForeground );
	UxPutAlignment( label26, "alignment_beginning" );
	UxPutLabelString( label26, "Maximum deviation (pixels) :" );
	UxPutFontList( label26, TextFont );
	UxPutBackground( label26, LabelBackground );
	UxPutHeight( label26, 30 );
	UxPutWidth( label26, 200 );
	UxPutY( label26, 238 );
	UxPutX( label26, 14 );

	UxPutTranslations( rowColumn24, "" );
	UxPutPacking( rowColumn24, "pack_tight" );
	UxPutOrientation( rowColumn24, "horizontal" );
	UxPutIsAligned( rowColumn24, "true" );
	UxPutEntryAlignment( rowColumn24, "alignment_beginning" );
	UxPutBorderWidth( rowColumn24, 0 );
	UxPutShadowThickness( rowColumn24, 0 );
	UxPutLabelString( rowColumn24, "" );
	UxPutEntryBorder( rowColumn24, 0 );
	UxPutBackground( rowColumn24, "grey80" );
	UxPutRadioBehavior( rowColumn24, "true" );
	UxPutHeight( rowColumn24, 31 );
	UxPutWidth( rowColumn24, 402 );
	UxPutY( rowColumn24, 43 );
	UxPutX( rowColumn24, 104 );

	UxPutTranslations( rb_wlcmtd_iden1, transTable6 );
	UxPutIndicatorSize( rb_wlcmtd_iden1, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_iden1, "true" );
	UxPutForeground( rb_wlcmtd_iden1, TextForeground );
	UxPutSelectColor( rb_wlcmtd_iden1, SelectColor );
	UxPutSet( rb_wlcmtd_iden1, "true" );
	UxPutLabelString( rb_wlcmtd_iden1, "PAIR" );
	UxPutFontList( rb_wlcmtd_iden1, TextFont );
	UxPutBackground( rb_wlcmtd_iden1, WindowBackground );
	UxPutHeight( rb_wlcmtd_iden1, 20 );
	UxPutWidth( rb_wlcmtd_iden1, 76 );
	UxPutY( rb_wlcmtd_iden1, 2 );
	UxPutX( rb_wlcmtd_iden1, 3 );

	UxPutTranslations( rb_wlcmtd_gues4, transTable6 );
	UxPutIndicatorSize( rb_wlcmtd_gues4, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_gues4, "true" );
	UxPutSet( rb_wlcmtd_gues4, "false" );
	UxPutForeground( rb_wlcmtd_gues4, TextForeground );
	UxPutSelectColor( rb_wlcmtd_gues4, SelectColor );
	UxPutLabelString( rb_wlcmtd_gues4, "ANGLE" );
	UxPutFontList( rb_wlcmtd_gues4, TextFont );
	UxPutBackground( rb_wlcmtd_gues4, WindowBackground );
	UxPutHeight( rb_wlcmtd_gues4, 32 );
	UxPutWidth( rb_wlcmtd_gues4, 93 );
	UxPutY( rb_wlcmtd_gues4, 34 );
	UxPutX( rb_wlcmtd_gues4, 3 );

	UxPutTranslations( rb_wlcmtd_gues5, transTable6 );
	UxPutIndicatorSize( rb_wlcmtd_gues5, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_gues5, "true" );
	UxPutSet( rb_wlcmtd_gues5, "false" );
	UxPutForeground( rb_wlcmtd_gues5, TextForeground );
	UxPutSelectColor( rb_wlcmtd_gues5, SelectColor );
	UxPutLabelString( rb_wlcmtd_gues5, "RESTART" );
	UxPutFontList( rb_wlcmtd_gues5, TextFont );
	UxPutBackground( rb_wlcmtd_gues5, WindowBackground );
	UxPutHeight( rb_wlcmtd_gues5, 23 );
	UxPutWidth( rb_wlcmtd_gues5, 90 );
	UxPutY( rb_wlcmtd_gues5, 2 );
	UxPutX( rb_wlcmtd_gues5, 146 );

	UxPutTranslations( rb_wlcmtd_gues6, transTable6 );
	UxPutIndicatorSize( rb_wlcmtd_gues6, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_gues6, "true" );
	UxPutSet( rb_wlcmtd_gues6, "false" );
	UxPutForeground( rb_wlcmtd_gues6, TextForeground );
	UxPutSelectColor( rb_wlcmtd_gues6, SelectColor );
	UxPutLabelString( rb_wlcmtd_gues6, "ORDER" );
	UxPutFontList( rb_wlcmtd_gues6, TextFont );
	UxPutBackground( rb_wlcmtd_gues6, WindowBackground );
	UxPutHeight( rb_wlcmtd_gues6, 66 );
	UxPutWidth( rb_wlcmtd_gues6, 76 );
	UxPutY( rb_wlcmtd_gues6, 34 );
	UxPutX( rb_wlcmtd_gues6, 3 );

	UxPutTranslations( rb_wlcmtd_gues7, transTable6 );
	UxPutIndicatorSize( rb_wlcmtd_gues7, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_gues7, "true" );
	UxPutSet( rb_wlcmtd_gues7, "false" );
	UxPutForeground( rb_wlcmtd_gues7, TextForeground );
	UxPutSelectColor( rb_wlcmtd_gues7, SelectColor );
	UxPutLabelString( rb_wlcmtd_gues7, "GUESS" );
	UxPutFontList( rb_wlcmtd_gues7, TextFont );
	UxPutBackground( rb_wlcmtd_gues7, WindowBackground );
	UxPutHeight( rb_wlcmtd_gues7, 17 );
	UxPutWidth( rb_wlcmtd_gues7, 76 );
	UxPutY( rb_wlcmtd_gues7, 6 );
	UxPutX( rb_wlcmtd_gues7, 114 );

	UxPutTranslations( rowColumn25, transTable6 );
	UxPutPacking( rowColumn25, "pack_tight" );
	UxPutOrientation( rowColumn25, "vertical" );
	UxPutIsAligned( rowColumn25, "true" );
	UxPutEntryAlignment( rowColumn25, "alignment_beginning" );
	UxPutBorderWidth( rowColumn25, 0 );
	UxPutShadowThickness( rowColumn25, 0 );
	UxPutLabelString( rowColumn25, "" );
	UxPutEntryBorder( rowColumn25, 0 );
	UxPutBackground( rowColumn25, "grey80" );
	UxPutRadioBehavior( rowColumn25, "true" );
	UxPutHeight( rowColumn25, 74 );
	UxPutWidth( rowColumn25, 83 );
	UxPutY( rowColumn25, 183 );
	UxPutX( rowColumn25, 368 );

	UxPutTranslations( rb_wlcmtd_iden2, transTable6 );
	UxPutIndicatorSize( rb_wlcmtd_iden2, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_iden2, "true" );
	UxPutForeground( rb_wlcmtd_iden2, TextForeground );
	UxPutSelectColor( rb_wlcmtd_iden2, SelectColor );
	UxPutSet( rb_wlcmtd_iden2, "true" );
	UxPutLabelString( rb_wlcmtd_iden2, "1D" );
	UxPutFontList( rb_wlcmtd_iden2, TextFont );
	UxPutBackground( rb_wlcmtd_iden2, WindowBackground );
	UxPutHeight( rb_wlcmtd_iden2, 32 );
	UxPutWidth( rb_wlcmtd_iden2, 77 );
	UxPutY( rb_wlcmtd_iden2, 3 );
	UxPutX( rb_wlcmtd_iden2, 3 );

	UxPutTranslations( rb_wlcmtd_gues1, transTable6 );
	UxPutIndicatorSize( rb_wlcmtd_gues1, 16 );
	UxPutHighlightOnEnter( rb_wlcmtd_gues1, "true" );
	UxPutSet( rb_wlcmtd_gues1, "false" );
	UxPutForeground( rb_wlcmtd_gues1, TextForeground );
	UxPutSelectColor( rb_wlcmtd_gues1, SelectColor );
	UxPutLabelString( rb_wlcmtd_gues1, "2D" );
	UxPutFontList( rb_wlcmtd_gues1, TextFont );
	UxPutBackground( rb_wlcmtd_gues1, WindowBackground );
	UxPutHeight( rb_wlcmtd_gues1, 32 );
	UxPutWidth( rb_wlcmtd_gues1, 77 );
	UxPutY( rb_wlcmtd_gues1, 39 );
	UxPutX( rb_wlcmtd_gues1, 3 );

	UxPutBackground( separator13, WindowBackground );
	UxPutHeight( separator13, 10 );
	UxPutWidth( separator13, 512 );
	UxPutY( separator13, 392 );
	UxPutX( separator13, 8 );

	UxPutOrientation( separator14, "vertical" );
	UxPutBackground( separator14, WindowBackground );
	UxPutHeight( separator14, 88 );
	UxPutWidth( separator14, 12 );
	UxPutY( separator14, 290 );
	UxPutX( separator14, 350 );

	UxPutBackground( separator15, WindowBackground );
	UxPutHeight( separator15, 10 );
	UxPutWidth( separator15, 110 );
	UxPutY( separator15, 372 );
	UxPutX( separator15, 352 );

	UxPutOrientation( separator16, "vertical" );
	UxPutBackground( separator16, WindowBackground );
	UxPutHeight( separator16, 88 );
	UxPutWidth( separator16, 12 );
	UxPutY( separator16, 288 );
	UxPutX( separator16, 458 );

	UxPutOrientation( separator17, "vertical" );
	UxPutBackground( separator17, WindowBackground );
	UxPutHeight( separator17, 87 );
	UxPutWidth( separator17, 12 );
	UxPutY( separator17, 171 );
	UxPutX( separator17, 358 );

	UxPutOrientation( separator18, "vertical" );
	UxPutBackground( separator18, WindowBackground );
	UxPutHeight( separator18, 93 );
	UxPutWidth( separator18, 14 );
	UxPutY( separator18, 169 );
	UxPutX( separator18, 456 );

	UxPutOrientation( separator19, "vertical" );
	UxPutBackground( separator19, WindowBackground );
	UxPutHeight( separator19, 91 );
	UxPutWidth( separator19, 12 );
	UxPutY( separator19, 169 );
	UxPutX( separator19, 456 );

	UxPutForeground( label28, TextForeground );
	UxPutAlignment( label28, "alignment_beginning" );
	UxPutLabelString( label28, "Visualize" );
	UxPutFontList( label28, TextFont );
	UxPutBackground( label28, LabelBackground );
	UxPutHeight( label28, 30 );
	UxPutWidth( label28, 88 );
	UxPutY( label28, 276 );
	UxPutX( label28, 366 );

	UxPutForeground( label31, TextForeground );
	UxPutAlignment( label31, "alignment_beginning" );
	UxPutLabelString( label31, "Disp. Relation" );
	UxPutFontList( label31, TextFont );
	UxPutBackground( label31, LabelBackground );
	UxPutHeight( label31, 30 );
	UxPutWidth( label31, 100 );
	UxPutY( label31, 154 );
	UxPutX( label31, 362 );

	UxPutForeground( menu2_p1, TextForeground );
	UxPutBackground( menu2_p1, WindowBackground );
	UxPutRowColumnType( menu2_p1, "menu_pulldown" );

	UxPutFontList( mn_tol_angstroms1, TextFont );
	UxPutLabelString( mn_tol_angstroms1, " Angstroms " );

	UxPutFontList( mn_tol_pixels1, TextFont );
	UxPutLabelString( mn_tol_pixels1, " Pixels " );

	UxPutTranslations( mn_tol1, transTable6 );
	UxPutLabelString( mn_tol1, " " );
	UxPutSpacing( mn_tol1, 0 );
	UxPutMarginWidth( mn_tol1, 0 );
	UxPutForeground( mn_tol1, TextForeground );
	UxPutBackground( mn_tol1, WindowBackground );
	UxPutY( mn_tol1, 124 );
	UxPutX( mn_tol1, 350 );
	UxPutRowColumnType( mn_tol1, "menu_option" );

	UxPutForeground( label18, TextForeground );
	UxPutAlignment( label18, "alignment_beginning" );
	UxPutLabelString( label18, "Min. and Max. number of iter. :" );
	UxPutFontList( label18, TextFont );
	UxPutBackground( label18, LabelBackground );
	UxPutHeight( label18, 30 );
	UxPutWidth( label18, 200 );
	UxPutY( label18, 198 );
	UxPutX( label18, 13 );

	UxPutWidth( pb_main_search33, 46 );
	UxPutRecomputeSize( pb_main_search33, "true" );
	UxPutLabelString( pb_main_search33, "..." );
	UxPutForeground( pb_main_search33, ButtonForeground );
	UxPutFontList( pb_main_search33, BoldTextFont );
	UxPutBackground( pb_main_search33, ButtonBackground );
	UxPutHeight( pb_main_search33, 28 );
	UxPutY( pb_main_search33, 8 );
	UxPutX( pb_main_search33, 464 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_CalibShell()
{
	/* Create the swidgets */

	CalibShell = UxCreateApplicationShell( "CalibShell", NO_PARENT );
	UxPutContext( CalibShell, UxCalibShellContext );

	form3 = UxCreateForm( "form3", CalibShell );
	rowColumn2 = UxCreateRowColumn( "rowColumn2", form3 );
	rb_wlcmtd_iden = UxCreateToggleButton( "rb_wlcmtd_iden", rowColumn2 );
	rb_wlcmtd_gues = UxCreateToggleButton( "rb_wlcmtd_gues", rowColumn2 );
	label14 = UxCreateLabel( "label14", form3 );
	label16 = UxCreateLabel( "label16", form3 );
	tf_tol = UxCreateTextField( "tf_tol", form3 );
	label17 = UxCreateLabel( "label17", form3 );
	tf_dcx1 = UxCreateTextField( "tf_dcx1", form3 );
	tf_alpha = UxCreateTextField( "tf_alpha", form3 );
	label19 = UxCreateLabel( "label19", form3 );
	label20 = UxCreateLabel( "label20", form3 );
	tf_maxdev = UxCreateTextField( "tf_maxdev", form3 );
	guess_session_label = UxCreateLabel( "guess_session_label", form3 );
	separator4 = UxCreateSeparator( "separator4", form3 );
	shelp_calib = UxCreateText( "shelp_calib", form3 );
	separator5 = UxCreateSeparator( "separator5", form3 );
	tf_wlcniter1 = UxCreateTextField( "tf_wlcniter1", form3 );
	tf_wlcniter2 = UxCreateTextField( "tf_wlcniter2", form3 );
	tg_coropt = UxCreateToggleButton( "tg_coropt", form3 );
	form20 = UxCreateForm( "form20", form3 );
	pb_calib_calibrate = UxCreatePushButton( "pb_calib_calibrate", form20 );
	pushButton40 = UxCreatePushButton( "pushButton40", form20 );
	pb_calib_disper = UxCreatePushButton( "pb_calib_disper", form20 );
	pb_calib_resid = UxCreatePushButton( "pb_calib_resid", form20 );
	pb_calib_spec = UxCreatePushButton( "pb_calib_spec", form20 );
	label8 = UxCreateLabel( "label8", form3 );
	tf_maxdev1 = UxCreateTextField( "tf_maxdev1", form3 );
	tf_maxdev2 = UxCreateTextField( "tf_maxdev2", form3 );
	tf_guess1 = UxCreateTextField( "tf_guess1", form3 );
	tf_guess2 = UxCreateTextField( "tf_guess2", form3 );
	label15 = UxCreateLabel( "label15", form3 );
	label26 = UxCreateLabel( "label26", form3 );
	rowColumn24 = UxCreateRowColumn( "rowColumn24", form3 );
	rb_wlcmtd_iden1 = UxCreateToggleButton( "rb_wlcmtd_iden1", rowColumn24 );
	rb_wlcmtd_gues4 = UxCreateToggleButton( "rb_wlcmtd_gues4", rowColumn24 );
	rb_wlcmtd_gues5 = UxCreateToggleButton( "rb_wlcmtd_gues5", rowColumn24 );
	rb_wlcmtd_gues6 = UxCreateToggleButton( "rb_wlcmtd_gues6", rowColumn24 );
	rb_wlcmtd_gues7 = UxCreateToggleButton( "rb_wlcmtd_gues7", rowColumn24 );
	rowColumn25 = UxCreateRowColumn( "rowColumn25", form3 );
	rb_wlcmtd_iden2 = UxCreateToggleButton( "rb_wlcmtd_iden2", rowColumn25 );
	rb_wlcmtd_gues1 = UxCreateToggleButton( "rb_wlcmtd_gues1", rowColumn25 );
	separator13 = UxCreateSeparator( "separator13", form3 );
	separator14 = UxCreateSeparator( "separator14", form3 );
	separator15 = UxCreateSeparator( "separator15", form3 );
	separator16 = UxCreateSeparator( "separator16", form3 );
	separator17 = UxCreateSeparator( "separator17", form3 );
	separator18 = UxCreateSeparator( "separator18", form3 );
	separator19 = UxCreateSeparator( "separator19", form3 );
	label28 = UxCreateLabel( "label28", form3 );
	label31 = UxCreateLabel( "label31", form3 );
	menu2_p1 = UxCreateRowColumn( "menu2_p1", form3 );
	mn_tol_angstroms1 = UxCreatePushButtonGadget( "mn_tol_angstroms1", menu2_p1 );
	mn_tol_pixels1 = UxCreatePushButtonGadget( "mn_tol_pixels1", menu2_p1 );
	mn_tol1 = UxCreateRowColumn( "mn_tol1", form3 );
	label18 = UxCreateLabel( "label18", form3 );
	pb_main_search33 = UxCreatePushButton( "pb_main_search33", form3 );

	_Uxinit_CalibShell();

	/* Create the X widgets */

	UxCreateWidget( CalibShell );
	UxCreateWidget( form3 );
	UxCreateWidget( rowColumn2 );
	UxCreateWidget( rb_wlcmtd_iden );
	UxCreateWidget( rb_wlcmtd_gues );
	UxPutLeftOffset( label14, 10 );
	UxPutLeftAttachment( label14, "attach_form" );
	UxCreateWidget( label14 );

	UxPutLeftOffset( label16, 10 );
	UxPutLeftAttachment( label16, "attach_form" );
	UxCreateWidget( label16 );

	UxCreateWidget( tf_tol );
	UxPutLeftOffset( label17, 10 );
	UxPutLeftAttachment( label17, "attach_form" );
	UxCreateWidget( label17 );

	UxCreateWidget( tf_dcx1 );
	UxCreateWidget( tf_alpha );
	UxPutLeftOffset( label19, 10 );
	UxPutLeftAttachment( label19, "attach_form" );
	UxCreateWidget( label19 );

	UxPutLeftOffset( label20, 10 );
	UxPutLeftAttachment( label20, "attach_form" );
	UxCreateWidget( label20 );

	UxCreateWidget( tf_maxdev );
	UxPutLeftOffset( guess_session_label, 10 );
	UxPutLeftAttachment( guess_session_label, "attach_form" );
	UxCreateWidget( guess_session_label );

	UxCreateWidget( separator4 );
	UxCreateWidget( shelp_calib );
	UxCreateWidget( separator5 );
	UxCreateWidget( tf_wlcniter1 );
	UxCreateWidget( tf_wlcniter2 );
	UxCreateWidget( tg_coropt );
	UxCreateWidget( form20 );
	UxCreateWidget( pb_calib_calibrate );
	UxCreateWidget( pushButton40 );
	UxCreateWidget( pb_calib_disper );
	UxCreateWidget( pb_calib_resid );
	UxCreateWidget( pb_calib_spec );
	UxPutLeftOffset( label8, 10 );
	UxPutLeftAttachment( label8, "attach_form" );
	UxCreateWidget( label8 );

	UxCreateWidget( tf_maxdev1 );
	UxCreateWidget( tf_maxdev2 );
	UxCreateWidget( tf_guess1 );
	UxCreateWidget( tf_guess2 );
	UxPutLeftOffset( label15, 10 );
	UxPutLeftAttachment( label15, "attach_form" );
	UxCreateWidget( label15 );

	UxPutLeftOffset( label26, 10 );
	UxPutLeftAttachment( label26, "attach_form" );
	UxCreateWidget( label26 );

	UxCreateWidget( rowColumn24 );
	UxCreateWidget( rb_wlcmtd_iden1 );
	UxCreateWidget( rb_wlcmtd_gues4 );
	UxCreateWidget( rb_wlcmtd_gues5 );
	UxCreateWidget( rb_wlcmtd_gues6 );
	UxCreateWidget( rb_wlcmtd_gues7 );
	UxCreateWidget( rowColumn25 );
	UxCreateWidget( rb_wlcmtd_iden2 );
	UxCreateWidget( rb_wlcmtd_gues1 );
	UxCreateWidget( separator13 );
	UxCreateWidget( separator14 );
	UxCreateWidget( separator15 );
	UxCreateWidget( separator16 );
	UxCreateWidget( separator17 );
	UxCreateWidget( separator18 );
	UxCreateWidget( separator19 );
	UxCreateWidget( label28 );
	UxCreateWidget( label31 );
	UxCreateWidget( menu2_p1 );
	UxCreateWidget( mn_tol_angstroms1 );
	UxCreateWidget( mn_tol_pixels1 );
	UxPutSubMenuId( mn_tol1, "menu2_p1" );
	UxCreateWidget( mn_tol1 );

	UxCreateWidget( label18 );
	UxCreateWidget( pb_main_search33 );

	UxAddCallback( rb_wlcmtd_iden, XmNarmCallback,
			armCB_rb_wlcmtd_iden,
			(XtPointer) UxCalibShellContext );
	UxAddCallback( rb_wlcmtd_iden, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_iden,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( rb_wlcmtd_gues, XmNarmCallback,
			armCB_rb_wlcmtd_gues,
			(XtPointer) UxCalibShellContext );
	UxAddCallback( rb_wlcmtd_gues, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_gues,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_tol, XmNlosingFocusCallback,
			losingFocusCB_tf_tol,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_dcx1, XmNlosingFocusCallback,
			losingFocusCB_tf_dcx1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_alpha, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_maxdev, XmNlosingFocusCallback,
			losingFocusCB_tf_maxdev,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_wlcniter1, XmNlosingFocusCallback,
			losingFocusCB_tf_wlcniter1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_wlcniter2, XmNlosingFocusCallback,
			losingFocusCB_tf_wlcniter2,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tg_coropt, XmNvalueChangedCallback,
			valueChangedCB_tg_coropt,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_calibrate, XmNactivateCallback,
			activateCB_pb_calib_calibrate,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pushButton40, XmNactivateCallback,
			activateCB_pushButton40,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_disper, XmNactivateCallback,
			activateCB_pb_calib_disper,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_resid, XmNactivateCallback,
			activateCB_pb_calib_resid,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_calib_spec, XmNactivateCallback,
			activateCB_pb_calib_spec,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_maxdev1, XmNlosingFocusCallback,
			losingFocusCB_tf_maxdev1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_maxdev2, XmNlosingFocusCallback,
			losingFocusCB_tf_maxdev2,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_guess1, XmNlosingFocusCallback,
			losingFocusCB_tf_guess1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( tf_guess2, XmNlosingFocusCallback,
			losingFocusCB_tf_guess2,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( rb_wlcmtd_iden1, XmNarmCallback,
			armCB_rb_wlcmtd_iden1,
			(XtPointer) UxCalibShellContext );
	UxAddCallback( rb_wlcmtd_iden1, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_iden1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( rb_wlcmtd_gues4, XmNarmCallback,
			armCB_rb_wlcmtd_gues4,
			(XtPointer) UxCalibShellContext );
	UxAddCallback( rb_wlcmtd_gues4, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_gues4,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( rb_wlcmtd_gues5, XmNarmCallback,
			armCB_rb_wlcmtd_gues5,
			(XtPointer) UxCalibShellContext );
	UxAddCallback( rb_wlcmtd_gues5, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_gues5,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( rb_wlcmtd_gues6, XmNarmCallback,
			armCB_rb_wlcmtd_gues6,
			(XtPointer) UxCalibShellContext );
	UxAddCallback( rb_wlcmtd_gues6, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_gues6,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( rb_wlcmtd_gues7, XmNarmCallback,
			armCB_rb_wlcmtd_gues7,
			(XtPointer) UxCalibShellContext );
	UxAddCallback( rb_wlcmtd_gues7, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_gues7,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( rb_wlcmtd_iden2, XmNarmCallback,
			armCB_rb_wlcmtd_iden2,
			(XtPointer) UxCalibShellContext );
	UxAddCallback( rb_wlcmtd_iden2, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_iden2,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( rb_wlcmtd_gues1, XmNarmCallback,
			armCB_rb_wlcmtd_gues1,
			(XtPointer) UxCalibShellContext );
	UxAddCallback( rb_wlcmtd_gues1, XmNvalueChangedCallback,
			valueChangedCB_rb_wlcmtd_gues1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( mn_tol_angstroms1, XmNactivateCallback,
			activateCB_mn_tol_angstroms1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( mn_tol_pixels1, XmNactivateCallback,
			activateCB_mn_tol_pixels1,
			(XtPointer) UxCalibShellContext );

	UxAddCallback( pb_main_search33, XmNactivateCallback,
			activateCB_pb_main_search33,
			(XtPointer) UxCalibShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( CalibShell );

	return ( CalibShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_CalibShell()
{
	swidget                 rtrn;
	_UxCCalibShell          *UxContext;

	UxCalibShellContext = UxContext =
		(_UxCCalibShell *) UxMalloc( sizeof(_UxCCalibShell) );

	rtrn = _Uxbuild_CalibShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_CalibShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "WriteHelp", action_WriteHelp },
				{ "ClearHelp", action_ClearHelp },
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_CalibShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

