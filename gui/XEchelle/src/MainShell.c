/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	MainShell.c


.VERSION
 090825         last modif

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "UxLib.h"
#include "UxTogB.h"
#include "UxScList.h"
#include "UxLabel.h"
#include "UxTextF.h"
#include "UxScText.h"
#include "UxScrW.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxCascB.h"
#include "UxSepG.h"
#include "UxSep.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxMainW.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>
#include <proto_xech.h>

#include <midas_def.h>

void FilterFiles();
int GetSelection();

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxApplicWindow;
	swidget	UxMainWindow;
	swidget	Uxmenu1;
	swidget	Uxmenu1_p1;
	swidget	Uxmenu1_p1_b1;
	swidget	Uxmenu1_p1_b2;
	swidget	Uxmenu1_p1_b3;
	swidget	Uxmenu1_p1_b5;
	swidget	Uxmenu1_p1_b6;
	swidget	Uxmenu1_p1_b7;
	swidget	Uxmenu1_p1_b8;
	swidget	Uxmenu1_p1_b4;
	swidget	Uxmenu1_top_b1;
	swidget	Uxmenu1_p4;
	swidget	Uxmenu1_p4_b1;
	swidget	Uxmenu1_p4_b2;
	swidget	Uxmenu1_p4_b8;
	swidget	Uxmenu1_p4_b10;
	swidget	Uxmenu1_p4_b13;
	swidget	Uxmenu1_p4_b12;
	swidget	Uxmenu1_p4_b3;
	swidget	Uxmenu1_p4_b4;
	swidget	Uxmenu1_p4_b9;
	swidget	Uxmenu1_p4_b5;
	swidget	Uxmenu1_p4_b11;
	swidget	Uxmenu1_p4_b14;
	swidget	Uxmenu1_p4_b15;
	swidget	Uxmenu1_top_b4;
	swidget	Uxmenu1_p5;
	swidget	Uxmenu_help_context;
	swidget	Uxmenu_help_help;
	swidget	Uxmenu1_p5_b5;
	swidget	Uxmenu_help_tutorial;
	swidget	Uxmenu_help_version;
	swidget	Uxmenu1_top_b5;
	swidget	Uxform1;
	swidget	Uxform5;
	swidget	Uxpb_main_batch;
	swidget	Uxpb_main_ident;
	swidget	Uxpb_main_batch1;
	swidget	Uxpb_main_batch2;
	swidget	Uxpb_main_batch3;
	swidget	Uxseparator12;
	swidget	Uxshelp_main;
	swidget	Uxseparator1;
	swidget	Uxform22;
	swidget	UxListWindow1;
	swidget	UxscrolledList1;
	swidget	Uxtf_wrang1;
	swidget	Uxlabel3;
	swidget	Uxlabel4;
	swidget	Uxtf_wrang2;
	swidget	Uxform23;
	swidget	Uxlabel2;
	swidget	Uxtf_session;
	swidget	Uxpb_main_batch4;
	swidget	Uxform24;
	swidget	Uxlabel5;
	swidget	Uxtf_session1;
	swidget	Uxlabel32;
	swidget	Uxlabel39;
	swidget	UxscrolledWindow4;
	swidget	UxscrolledList4;
	swidget	UxrowColumn11;
	swidget	Uxlabel7;
	swidget	UxrowColumn13;
	swidget	UxtoggleButton5;
	swidget	Uxpb_main_search4;
	swidget	UxrowColumn14;
	swidget	UxtoggleButton6;
	swidget	Uxpb_main_search5;
	swidget	UxrowColumn15;
	swidget	UxtoggleButton7;
	swidget	Uxpb_main_search6;
	swidget	UxrowColumn16;
	swidget	UxtoggleButton8;
	swidget	Uxpb_main_search7;
	swidget	UxrowColumn17;
	swidget	UxtoggleButton9;
	swidget	Uxmenu2_p7;
	swidget	Uxmn_tol_angstroms7;
	swidget	Uxmn_tol_pixels7;
	swidget	Uxmenu2_p7_b3;
	swidget	Uxmenu2_p7_b4;
	swidget	Uxmenu2_p7_b5;
	swidget	Uxmenu2_p7_b6;
	swidget	Uxmn_tol7;
	swidget	Uxlabel1;
	swidget	UxrowColumn18;
	swidget	UxtoggleButton3;
	swidget	Uxpb_main_search2;
	swidget	UxrowColumn19;
	swidget	UxtoggleButton10;
	swidget	Uxpb_main_search9;
	swidget	UxrowColumn20;
	swidget	UxtoggleButton11;
	swidget	Uxpb_main_search10;
	swidget	UxrowColumn21;
	swidget	UxtoggleButton12;
	swidget	Uxpb_main_search11;
	swidget	UxrowColumn9;
	swidget	UxtoggleButton1;
	swidget	Uxpb_main_search1;
	swidget	UxrowColumn10;
	swidget	UxtoggleButton2;
	swidget	Uxpb_main_search3;
	swidget	UxrowColumn12;
	swidget	UxtoggleButton4;
	swidget	Uxpb_main_search12;
	swidget	UxrowColumn22;
	swidget	UxtoggleButton13;
	swidget	Uxpb_main_search13;
} _UxCApplicWindow;

#define ApplicWindow            UxApplicWindowContext->UxApplicWindow
#define MainWindow              UxApplicWindowContext->UxMainWindow
#define menu1                   UxApplicWindowContext->Uxmenu1
#define menu1_p1                UxApplicWindowContext->Uxmenu1_p1
#define menu1_p1_b1             UxApplicWindowContext->Uxmenu1_p1_b1
#define menu1_p1_b2             UxApplicWindowContext->Uxmenu1_p1_b2
#define menu1_p1_b3             UxApplicWindowContext->Uxmenu1_p1_b3
#define menu1_p1_b5             UxApplicWindowContext->Uxmenu1_p1_b5
#define menu1_p1_b6             UxApplicWindowContext->Uxmenu1_p1_b6
#define menu1_p1_b7             UxApplicWindowContext->Uxmenu1_p1_b7
#define menu1_p1_b8             UxApplicWindowContext->Uxmenu1_p1_b8
#define menu1_p1_b4             UxApplicWindowContext->Uxmenu1_p1_b4
#define menu1_top_b1            UxApplicWindowContext->Uxmenu1_top_b1
#define menu1_p4                UxApplicWindowContext->Uxmenu1_p4
#define menu1_p4_b1             UxApplicWindowContext->Uxmenu1_p4_b1
#define menu1_p4_b2             UxApplicWindowContext->Uxmenu1_p4_b2
#define menu1_p4_b8             UxApplicWindowContext->Uxmenu1_p4_b8
#define menu1_p4_b10            UxApplicWindowContext->Uxmenu1_p4_b10
#define menu1_p4_b13            UxApplicWindowContext->Uxmenu1_p4_b13
#define menu1_p4_b12            UxApplicWindowContext->Uxmenu1_p4_b12
#define menu1_p4_b3             UxApplicWindowContext->Uxmenu1_p4_b3
#define menu1_p4_b4             UxApplicWindowContext->Uxmenu1_p4_b4
#define menu1_p4_b9             UxApplicWindowContext->Uxmenu1_p4_b9
#define menu1_p4_b5             UxApplicWindowContext->Uxmenu1_p4_b5
#define menu1_p4_b11            UxApplicWindowContext->Uxmenu1_p4_b11
#define menu1_p4_b14            UxApplicWindowContext->Uxmenu1_p4_b14
#define menu1_p4_b15            UxApplicWindowContext->Uxmenu1_p4_b15
#define menu1_top_b4            UxApplicWindowContext->Uxmenu1_top_b4
#define menu1_p5                UxApplicWindowContext->Uxmenu1_p5
#define menu_help_context       UxApplicWindowContext->Uxmenu_help_context
#define menu_help_help          UxApplicWindowContext->Uxmenu_help_help
#define menu1_p5_b5             UxApplicWindowContext->Uxmenu1_p5_b5
#define menu_help_tutorial      UxApplicWindowContext->Uxmenu_help_tutorial
#define menu_help_version       UxApplicWindowContext->Uxmenu_help_version
#define menu1_top_b5            UxApplicWindowContext->Uxmenu1_top_b5
#define form1                   UxApplicWindowContext->Uxform1
#define form5                   UxApplicWindowContext->Uxform5
#define pb_main_batch           UxApplicWindowContext->Uxpb_main_batch
#define pb_main_ident           UxApplicWindowContext->Uxpb_main_ident
#define pb_main_batch1          UxApplicWindowContext->Uxpb_main_batch1
#define pb_main_batch2          UxApplicWindowContext->Uxpb_main_batch2
#define pb_main_batch3          UxApplicWindowContext->Uxpb_main_batch3
#define separator12             UxApplicWindowContext->Uxseparator12
#define shelp_main              UxApplicWindowContext->Uxshelp_main
#define separator1              UxApplicWindowContext->Uxseparator1
#define form22                  UxApplicWindowContext->Uxform22
#define ListWindow1             UxApplicWindowContext->UxListWindow1
#define scrolledList1           UxApplicWindowContext->UxscrolledList1
#define tf_wrang1               UxApplicWindowContext->Uxtf_wrang1
#define label3                  UxApplicWindowContext->Uxlabel3
#define label4                  UxApplicWindowContext->Uxlabel4
#define tf_wrang2               UxApplicWindowContext->Uxtf_wrang2
#define form23                  UxApplicWindowContext->Uxform23
#define label2                  UxApplicWindowContext->Uxlabel2
#define tf_session              UxApplicWindowContext->Uxtf_session
#define pb_main_batch4          UxApplicWindowContext->Uxpb_main_batch4
#define form24                  UxApplicWindowContext->Uxform24
#define label5                  UxApplicWindowContext->Uxlabel5
#define tf_session1             UxApplicWindowContext->Uxtf_session1
#define label32                 UxApplicWindowContext->Uxlabel32
#define label39                 UxApplicWindowContext->Uxlabel39
#define scrolledWindow4         UxApplicWindowContext->UxscrolledWindow4
#define scrolledList4           UxApplicWindowContext->UxscrolledList4
#define rowColumn11             UxApplicWindowContext->UxrowColumn11
#define label7                  UxApplicWindowContext->Uxlabel7
#define rowColumn13             UxApplicWindowContext->UxrowColumn13
#define toggleButton5           UxApplicWindowContext->UxtoggleButton5
#define pb_main_search4         UxApplicWindowContext->Uxpb_main_search4
#define rowColumn14             UxApplicWindowContext->UxrowColumn14
#define toggleButton6           UxApplicWindowContext->UxtoggleButton6
#define pb_main_search5         UxApplicWindowContext->Uxpb_main_search5
#define rowColumn15             UxApplicWindowContext->UxrowColumn15
#define toggleButton7           UxApplicWindowContext->UxtoggleButton7
#define pb_main_search6         UxApplicWindowContext->Uxpb_main_search6
#define rowColumn16             UxApplicWindowContext->UxrowColumn16
#define toggleButton8           UxApplicWindowContext->UxtoggleButton8
#define pb_main_search7         UxApplicWindowContext->Uxpb_main_search7
#define rowColumn17             UxApplicWindowContext->UxrowColumn17
#define toggleButton9           UxApplicWindowContext->UxtoggleButton9
#define menu2_p7                UxApplicWindowContext->Uxmenu2_p7
#define mn_tol_angstroms7       UxApplicWindowContext->Uxmn_tol_angstroms7
#define mn_tol_pixels7          UxApplicWindowContext->Uxmn_tol_pixels7
#define menu2_p7_b3             UxApplicWindowContext->Uxmenu2_p7_b3
#define menu2_p7_b4             UxApplicWindowContext->Uxmenu2_p7_b4
#define menu2_p7_b5             UxApplicWindowContext->Uxmenu2_p7_b5
#define menu2_p7_b6             UxApplicWindowContext->Uxmenu2_p7_b6
#define mn_tol7                 UxApplicWindowContext->Uxmn_tol7
#define label1                  UxApplicWindowContext->Uxlabel1
#define rowColumn18             UxApplicWindowContext->UxrowColumn18
#define toggleButton3           UxApplicWindowContext->UxtoggleButton3
#define pb_main_search2         UxApplicWindowContext->Uxpb_main_search2
#define rowColumn19             UxApplicWindowContext->UxrowColumn19
#define toggleButton10          UxApplicWindowContext->UxtoggleButton10
#define pb_main_search9         UxApplicWindowContext->Uxpb_main_search9
#define rowColumn20             UxApplicWindowContext->UxrowColumn20
#define toggleButton11          UxApplicWindowContext->UxtoggleButton11
#define pb_main_search10        UxApplicWindowContext->Uxpb_main_search10
#define rowColumn21             UxApplicWindowContext->UxrowColumn21
#define toggleButton12          UxApplicWindowContext->UxtoggleButton12
#define pb_main_search11        UxApplicWindowContext->Uxpb_main_search11
#define rowColumn9              UxApplicWindowContext->UxrowColumn9
#define toggleButton1           UxApplicWindowContext->UxtoggleButton1
#define pb_main_search1         UxApplicWindowContext->Uxpb_main_search1
#define rowColumn10             UxApplicWindowContext->UxrowColumn10
#define toggleButton2           UxApplicWindowContext->UxtoggleButton2
#define pb_main_search3         UxApplicWindowContext->Uxpb_main_search3
#define rowColumn12             UxApplicWindowContext->UxrowColumn12
#define toggleButton4           UxApplicWindowContext->UxtoggleButton4
#define pb_main_search12        UxApplicWindowContext->Uxpb_main_search12
#define rowColumn22             UxApplicWindowContext->UxrowColumn22
#define toggleButton13          UxApplicWindowContext->UxtoggleButton13
#define pb_main_search13        UxApplicWindowContext->Uxpb_main_search13

static _UxCApplicWindow	*UxApplicWindowContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();

extern int RadioSet(), PopupList(), AppendDialogText();



/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable2 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

static char	*DirList = "#override\n\
<Btn1Up>:UpdateDirectory()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_ApplicWindow();

/*******************************************************************************
	Auxiliary code from the Declarations Editor:
*******************************************************************************/

void FilterFiles(node)

char *node;

{
 WGet_all_dirs(node);
 WGet_all_files();
}

int CheckStatus(sw)

swidget sw;
{
     char status[20]; 
     strcpy(status,UxGetSet(sw));
     if (status[0] == 't') return(TRUE);
     else return(FALSE);
}

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_WriteHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayShortHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ClearHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	/*
	UxPutText(UxFindSwidget("shelp_main"), "");
	UxPutText(UxFindSwidget("shelp_search"), "");
	UxPutText(UxFindSwidget("shelp_calib"), "");
	UxPutText(UxFindSwidget("shelp_rebin"), "");
	UxPutText(UxFindSwidget("shelp_extract"), "");
	UxPutText(UxFindSwidget("shelp_flux"), "");
	*/
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_UpdateDirectory( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	 char node[80];
	
	 /* if (XmTextGetSelection(UxWidget) == NULL) return; */
	 strcpy(node,
	     XmTextGetSelection(UxWidget));
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	
	node[strlen(node)-1] = '\0';
	WGet_all_dirs(node);
	WChange_Midas_dir();
	WGet_all_files();
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_menu1_p1_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	PopupList(LIST_SESSION);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	extern char Session[];
	char command[256];
	
	sprintf(command, "%s%s", C_SAVE, Session);
	AppendDialogText(command);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern int DialogType;
	extern char Session[];
	
	SET_DIALOG_PROMPT("Output parameters table :");
	
	XmTextSetString(UxGetWidget(UxFindSwidget("tf_file_dialog")), Session);
	DialogType = DIALOG_SESSION;
	UxPopupInterface(UxFindSwidget("file_dialog"), exclusive_grab);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{AppendDialogText("INIT/ECHELLE");
	 AppendDialogText("SYNCHRO/ECHELLE");
	 InitAllFields();
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{AppendDialogText("Synchron/ECHELLE");
	 InitAllFields();
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p1_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{exit(0);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("create/display");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("graph/spec");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{        
#include <spec_comm.h>
	
	PopupList(LIST_LOAD_IMA);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("clear/channel over");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("copy/display laser");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("copy/graph laser");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("get/gcursor");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	AppendDialogText("get/cursor");
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu1_p4_b15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char midashome[256];
	extern char DirSpecs[];

	osfphname("MIDASHOME", midashome);
	sprintf(DirSpecs, "%s/calib/data/", midashome);
	
	PopupList(LIST_BROWSER);
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_context( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_help( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_tutorial( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu_help_version( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	DisplayExtendedHelp(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_batch( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_ident( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_batch1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_batch2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_batch3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	{exit(0);}
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wrang1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	losingFocusCB_tf_wrang2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_batch4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	{
	
	char node[200];
	
	strcpy(node,UxGetText(tf_session1));
	WGet_all_dirs(node);
	WChange_Midas_dir();
	WGet_all_files();
	
	}
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	createCB_scrolledList4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("RotateShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("OrderShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("SearchShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("CalibShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_mn_tol_angstroms7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_mn_tol_pixels7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu2_p7_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu2_p7_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu2_p7_b5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_menu2_p7_b6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxApplicWindowContext = UxSaveCtx;
}

static void	armCB_toggleButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	if (CheckStatus(UxThisWidget) == TRUE)
	    AppendDialogText("SET/ECHEL ROTOPT=YES");
	else
	    AppendDialogText("SET/ECHEL ROTOPT=NO");
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("RotateShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	armCB_toggleButton10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	if (CheckStatus(UxThisWidget) == TRUE)
	    AppendDialogText("SET/ECHEL BKGOPT=YES");
	else
	    AppendDialogText("SET/ECHEL BKGOPT=NO");
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("BackgroundShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	armCB_toggleButton11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	if (CheckStatus(UxThisWidget) == TRUE)
	    AppendDialogText("SET/ECHEL EXTOPT=YES");
	else
	    AppendDialogText("SET/ECHEL EXTOPT=NO");
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("ExtractionShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	armCB_toggleButton12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	if (CheckStatus(UxThisWidget) == TRUE)
	    AppendDialogText("SET/ECHEL SKYOPT=YES");
	else
	    AppendDialogText("SET/ECHEL SKYOPT=NO");
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("SkyShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	armCB_toggleButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	if (CheckStatus(UxThisWidget) == TRUE)
	    AppendDialogText("SET/ECHEL REBOPT=YES");
	else
	    AppendDialogText("SET/ECHEL REBOPT=NO");
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("RebinShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	armCB_toggleButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	if (CheckStatus(UxThisWidget) == TRUE)
	    AppendDialogText("SET/ECHEL FFOPT=YES");
	else
	    AppendDialogText("SET/ECHEL FFOPT=NO");
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("FlatShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	armCB_toggleButton4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	if (CheckStatus(UxThisWidget) == TRUE)
	    AppendDialogText("SET/ECHEL RESPOPT=YES");
	else
	    AppendDialogText("SET/ECHEL RESPOPT=NO");
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("RespShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	armCB_toggleButton13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxApplicWindowContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	if (CheckStatus(UxThisWidget) == TRUE)
	    AppendDialogText("SET/ECHEL MGOPT=YES");
	else
	    AppendDialogText("SET/ECHEL MGOPT=NO");
	
	}
	UxApplicWindowContext = UxSaveCtx;
}

static void	activateCB_pb_main_search13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCApplicWindow        *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxApplicWindowContext;
	UxApplicWindowContext = UxContext =
			(_UxCApplicWindow *) UxGetContext( UxThisWidget );
	{
	PopupLong("MergeShell");
	}
	UxApplicWindowContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_ApplicWindow()
{
	UxPutGeometry( ApplicWindow, "+0+0" );
	UxPutTitle( ApplicWindow, "Echelle Reduction" );
	UxPutKeyboardFocusPolicy( ApplicWindow, "pointer" );
	UxPutIconName( ApplicWindow, "XEchelle" );
	UxPutHeight( ApplicWindow, 700 );
	UxPutWidth( ApplicWindow, 810 );
	UxPutY( ApplicWindow, 410 );
	UxPutX( ApplicWindow, 0 );

	UxPutBackground( MainWindow, WindowBackground );
	UxPutHeight( MainWindow, 320 );
	UxPutWidth( MainWindow, 810 );
	UxPutY( MainWindow, 0 );
	UxPutX( MainWindow, -2 );
	UxPutUnitType( MainWindow, "pixels" );

	UxPutBackground( menu1, MenuBackground );
	UxPutMenuAccelerator( menu1, "<KeyUp>F10" );
	UxPutRowColumnType( menu1, "menu_bar" );

	UxPutForeground( menu1_p1, MenuForeground );
	UxPutBackground( menu1_p1, MenuBackground );
	UxPutRowColumnType( menu1_p1, "menu_pulldown" );

	UxPutFontList( menu1_p1_b1, BoldTextFont );
	UxPutMnemonic( menu1_p1_b1, "O" );
	UxPutLabelString( menu1_p1_b1, "Open..." );

	UxPutFontList( menu1_p1_b2, BoldTextFont );
	UxPutMnemonic( menu1_p1_b2, "S" );
	UxPutLabelString( menu1_p1_b2, "Save" );

	UxPutFontList( menu1_p1_b3, BoldTextFont );
	UxPutMnemonic( menu1_p1_b3, "S" );
	UxPutLabelString( menu1_p1_b3, "Save As ..." );

	UxPutFontList( menu1_p1_b6, BoldTextFont );
	UxPutLabelString( menu1_p1_b6, "Set to defaults" );

	UxPutFontList( menu1_p1_b7, BoldTextFont );
	UxPutLabelString( menu1_p1_b7, "Synchronize" );

	UxPutFontList( menu1_p1_b4, BoldTextFont );
	UxPutAccelerator( menu1_p1_b4, "E" );
	UxPutMnemonic( menu1_p1_b4, "E" );
	UxPutLabelString( menu1_p1_b4, "Exit" );

	UxPutForeground( menu1_top_b1, MenuForeground );
	UxPutFontList( menu1_top_b1, BoldTextFont );
	UxPutBackground( menu1_top_b1, MenuBackground );
	UxPutMnemonic( menu1_top_b1, "F" );
	UxPutLabelString( menu1_top_b1, "File" );

	UxPutForeground( menu1_p4, MenuForeground );
	UxPutBackground( menu1_p4, MenuBackground );
	UxPutRowColumnType( menu1_p4, "menu_pulldown" );

	UxPutFontList( menu1_p4_b1, BoldTextFont );
	UxPutMnemonic( menu1_p4_b1, "D" );
	UxPutLabelString( menu1_p4_b1, "Create Display" );

	UxPutFontList( menu1_p4_b2, BoldTextFont );
	UxPutMnemonic( menu1_p4_b2, "G" );
	UxPutLabelString( menu1_p4_b2, "Create Graphics" );

	UxPutMnemonic( menu1_p4_b10, "L" );
	UxPutFontList( menu1_p4_b10, BoldTextFont );
	UxPutLabelString( menu1_p4_b10, "Load image" );

	UxPutMnemonic( menu1_p4_b13, "o" );
	UxPutFontList( menu1_p4_b13, BoldTextFont );
	UxPutLabelString( menu1_p4_b13, "Clear overlay" );

	UxPutFontList( menu1_p4_b3, BoldTextFont );
	UxPutMnemonic( menu1_p4_b3, "P" );
	UxPutLabelString( menu1_p4_b3, "Print Display" );

	UxPutFontList( menu1_p4_b4, BoldTextFont );
	UxPutMnemonic( menu1_p4_b4, "r" );
	UxPutLabelString( menu1_p4_b4, "Print Graphics" );

	UxPutFontList( menu1_p4_b5, BoldTextFont );
	UxPutMnemonic( menu1_p4_b5, "C" );
	UxPutLabelString( menu1_p4_b5, "Graphics Cursor" );

	UxPutFontList( menu1_p4_b11, BoldTextFont );
	UxPutMnemonic( menu1_p4_b11, "u" );
	UxPutLabelString( menu1_p4_b11, "Display Cursor" );

	UxPutFontList( menu1_p4_b15, BoldTextFont );
	UxPutLabelString( menu1_p4_b15, "get calib. table" );

	UxPutForeground( menu1_top_b4, MenuForeground );
	UxPutFontList( menu1_top_b4, BoldTextFont );
	UxPutBackground( menu1_top_b4, MenuBackground );
	UxPutMnemonic( menu1_top_b4, "U" );
	UxPutLabelString( menu1_top_b4, "Utils" );

	UxPutForeground( menu1_p5, MenuForeground );
	UxPutBackground( menu1_p5, MenuBackground );
	UxPutRowColumnType( menu1_p5, "menu_pulldown" );

	UxPutFontList( menu_help_context, BoldTextFont );
	UxPutMnemonic( menu_help_context, "C" );
	UxPutLabelString( menu_help_context, "On Context ..." );

	UxPutFontList( menu_help_help, BoldTextFont );
	UxPutMnemonic( menu_help_help, "H" );
	UxPutLabelString( menu_help_help, "On Help ..." );

	UxPutFontList( menu_help_tutorial, BoldTextFont );
	UxPutMnemonic( menu_help_tutorial, "T" );
	UxPutLabelString( menu_help_tutorial, "Tutorial" );

	UxPutFontList( menu_help_version, BoldTextFont );
	UxPutMnemonic( menu_help_version, "V" );
	UxPutLabelString( menu_help_version, "On Version..." );

	UxPutForeground( menu1_top_b5, MenuForeground );
	UxPutFontList( menu1_top_b5, BoldTextFont );
	UxPutBackground( menu1_top_b5, MenuBackground );
	UxPutMnemonic( menu1_top_b5, "H" );
	UxPutLabelString( menu1_top_b5, "Help" );

	UxPutBackground( form1, WindowBackground );

	UxPutBackground( form5, ButtonBackground );
	UxPutHeight( form5, 40 );
	UxPutWidth( form5, 451 );
	UxPutY( form5, 368 );
	UxPutX( form5, -1 );
	UxPutResizePolicy( form5, "resize_none" );

	UxPutLabelString( pb_main_batch, "Reduce" );
	UxPutForeground( pb_main_batch, ButtonForeground );
	UxPutFontList( pb_main_batch, BoldTextFont );
	UxPutBackground( pb_main_batch, ButtonBackground );
	UxPutHeight( pb_main_batch, 30 );
	UxPutWidth( pb_main_batch, 86 );
	UxPutY( pb_main_batch, 2 );
	UxPutX( pb_main_batch, 96 );

	UxPutLabelString( pb_main_ident, "Rotate" );
	UxPutForeground( pb_main_ident, ButtonForeground );
	UxPutFontList( pb_main_ident, BoldTextFont );
	UxPutBackground( pb_main_ident, ButtonBackground );
	UxPutHeight( pb_main_ident, 30 );
	UxPutWidth( pb_main_ident, 86 );
	UxPutY( pb_main_ident, 4 );
	UxPutX( pb_main_ident, 30 );

	UxPutLabelString( pb_main_batch1, "Load Orders" );
	UxPutForeground( pb_main_batch1, ButtonForeground );
	UxPutFontList( pb_main_batch1, BoldTextFont );
	UxPutBackground( pb_main_batch1, ButtonBackground );
	UxPutHeight( pb_main_batch1, 30 );
	UxPutWidth( pb_main_batch1, 120 );
	UxPutY( pb_main_batch1, 2 );
	UxPutX( pb_main_batch1, 184 );

	UxPutLabelString( pb_main_batch2, "Save" );
	UxPutForeground( pb_main_batch2, ButtonForeground );
	UxPutFontList( pb_main_batch2, BoldTextFont );
	UxPutBackground( pb_main_batch2, ButtonBackground );
	UxPutHeight( pb_main_batch2, 30 );
	UxPutWidth( pb_main_batch2, 120 );
	UxPutY( pb_main_batch2, 2 );
	UxPutX( pb_main_batch2, 306 );

	UxPutLabelString( pb_main_batch3, "Close Window" );
	UxPutForeground( pb_main_batch3, ButtonForeground );
	UxPutFontList( pb_main_batch3, BoldTextFont );
	UxPutBackground( pb_main_batch3, ButtonBackground );
	UxPutHeight( pb_main_batch3, 30 );
	UxPutWidth( pb_main_batch3, 119 );
	UxPutY( pb_main_batch3, 2 );
	UxPutX( pb_main_batch3, 686 );

	UxPutBackground( separator12, LabelBackground );
	UxPutHeight( separator12, 6 );
	UxPutWidth( separator12, 450 );
	UxPutY( separator12, 360 );
	UxPutX( separator12, 0 );

	UxPutFontList( shelp_main, TextFont );
	UxPutEditable( shelp_main, "false" );
	UxPutCursorPositionVisible( shelp_main, "false" );
	UxPutBackground( shelp_main, SHelpBackground );
	UxPutHeight( shelp_main, 50 );
	UxPutWidth( shelp_main, 448 );
	UxPutY( shelp_main, 310 );
	UxPutX( shelp_main, 0 );

	UxPutBackground( separator1, LabelBackground );
	UxPutHeight( separator1, 6 );
	UxPutWidth( separator1, 450 );
	UxPutY( separator1, 304 );
	UxPutX( separator1, -2 );

	UxPutShadowType( form22, "shadow_in" );
	UxPutShadowThickness( form22, 3 );
	UxPutBackground( form22, WindowBackground );
	UxPutHeight( form22, 534 );
	UxPutWidth( form22, 526 );
	UxPutY( form22, 10 );
	UxPutX( form22, 250 );
	UxPutResizePolicy( form22, "resize_none" );

	UxPutTopShadowColor( ListWindow1, WindowBackground );
	UxPutHighlightColor( ListWindow1, "black" );
	UxPutY( ListWindow1, 368 );
	UxPutX( ListWindow1, 130 );
	UxPutScrollBarPlacement( ListWindow1, "bottom_right" );
	UxPutForeground( ListWindow1, TextForeground );
	UxPutBottomShadowColor( ListWindow1, WindowBackground );
	UxPutBorderColor( ListWindow1, WindowBackground );
	UxPutBackground( ListWindow1, WindowBackground );
	UxPutShadowThickness( ListWindow1, 0 );

	UxPutTranslations( scrolledList1, DirList );
	UxPutSelectionArray( scrolledList1, "select_line select_line select_line select_line" );
	UxPutTopShadowColor( scrolledList1, "Black" );
	UxPutForeground( scrolledList1, TextForeground );
	UxPutFontList( scrolledList1, "9x15" );
	UxPutBorderColor( scrolledList1, SHelpBackground );
	UxPutBackground( scrolledList1, SHelpBackground );

	UxPutText( tf_wrang1, "esp" );
	UxPutForeground( tf_wrang1, TextForeground );
	UxPutHighlightOnEnter( tf_wrang1, "true" );
	UxPutFontList( tf_wrang1, TextFont );
	UxPutBackground( tf_wrang1, TextBackground );
	UxPutHeight( tf_wrang1, 34 );
	UxPutWidth( tf_wrang1, 250 );
	UxPutY( tf_wrang1, 230 );
	UxPutX( tf_wrang1, 100 );

	UxPutAlignment( label3, "alignment_beginning" );
	UxPutLabelString( label3, "Output Name:" );
	UxPutForeground( label3, TextForeground );
	UxPutFontList( label3, BoldTextFont );
	UxPutBackground( label3, WindowBackground );
	UxPutHeight( label3, 36 );
	UxPutWidth( label3, 132 );
	UxPutY( label3, 492 );
	UxPutX( label3, 10 );

	UxPutAlignment( label4, "alignment_beginning" );
	UxPutLabelString( label4, "Start Number:" );
	UxPutForeground( label4, TextForeground );
	UxPutFontList( label4, BoldTextFont );
	UxPutBackground( label4, WindowBackground );
	UxPutHeight( label4, 36 );
	UxPutWidth( label4, 105 );
	UxPutY( label4, 492 );
	UxPutX( label4, 336 );

	UxPutText( tf_wrang2, "1" );
	UxPutForeground( tf_wrang2, TextForeground );
	UxPutHighlightOnEnter( tf_wrang2, "true" );
	UxPutFontList( tf_wrang2, TextFont );
	UxPutBackground( tf_wrang2, TextBackground );
	UxPutHeight( tf_wrang2, 34 );
	UxPutWidth( tf_wrang2, 100 );
	UxPutY( tf_wrang2, 230 );
	UxPutX( tf_wrang2, 450 );

	UxPutBackground( form23, WindowBackground );
	UxPutHeight( form23, 40 );
	UxPutWidth( form23, 544 );
	UxPutY( form23, 54 );
	UxPutX( form23, 8 );
	UxPutResizePolicy( form23, "resize_none" );

	UxPutLabelString( label2, "Files:" );
	UxPutForeground( label2, TextForeground );
	UxPutFontList( label2, BoldTextFont );
	UxPutBackground( label2, WindowBackground );
	UxPutHeight( label2, 36 );
	UxPutWidth( label2, 80 );
	UxPutY( label2, 4 );
	UxPutX( label2, 20 );

	UxPutText( tf_session, "*.bdf" );
	UxPutBorderWidth( tf_session, 1 );
	UxPutEditable( tf_session, "true" );
	UxPutCursorPositionVisible( tf_session, "true" );
	UxPutForeground( tf_session, TextForeground );
	UxPutHighlightOnEnter( tf_session, "true" );
	UxPutFontList( tf_session, TextFont );
	UxPutBackground( tf_session, TextBackground );
	UxPutHeight( tf_session, 34 );
	UxPutWidth( tf_session, 235 );
	UxPutY( tf_session, 2 );
	UxPutX( tf_session, 112 );

	UxPutLabelString( pb_main_batch4, "Filter" );
	UxPutForeground( pb_main_batch4, ButtonForeground );
	UxPutFontList( pb_main_batch4, BoldTextFont );
	UxPutBackground( pb_main_batch4, ButtonBackground );
	UxPutHeight( pb_main_batch4, 30 );
	UxPutWidth( pb_main_batch4, 75 );
	UxPutY( pb_main_batch4, 6 );
	UxPutX( pb_main_batch4, 394 );

	UxPutBackground( form24, WindowBackground );
	UxPutHeight( form24, 40 );
	UxPutWidth( form24, 544 );
	UxPutY( form24, 8 );
	UxPutX( form24, 6 );
	UxPutResizePolicy( form24, "resize_none" );

	UxPutLabelString( label5, "Directory:" );
	UxPutForeground( label5, TextForeground );
	UxPutFontList( label5, BoldTextFont );
	UxPutBackground( label5, WindowBackground );
	UxPutHeight( label5, 36 );
	UxPutWidth( label5, 80 );
	UxPutY( label5, 4 );
	UxPutX( label5, 20 );

	UxPutText( tf_session1, "." );
	UxPutBorderWidth( tf_session1, 1 );
	UxPutEditable( tf_session1, "true" );
	UxPutCursorPositionVisible( tf_session1, "true" );
	UxPutForeground( tf_session1, TextForeground );
	UxPutHighlightOnEnter( tf_session1, "true" );
	UxPutFontList( tf_session1, TextFont );
	UxPutBackground( tf_session1, TextBackground );
	UxPutHeight( tf_session1, 34 );
	UxPutWidth( tf_session1, 235 );
	UxPutY( tf_session1, 2 );
	UxPutX( tf_session1, 100 );

	UxPutAlignment( label32, "alignment_beginning" );
	UxPutLabelString( label32, "Directories:" );
	UxPutForeground( label32, TextForeground );
	UxPutFontList( label32, BoldTextFont );
	UxPutBackground( label32, WindowBackground );
	UxPutHeight( label32, 36 );
	UxPutWidth( label32, 132 );
	UxPutY( label32, 114 );
	UxPutX( label32, 32 );

	UxPutAlignment( label39, "alignment_beginning" );
	UxPutLabelString( label39, "Files:" );
	UxPutForeground( label39, TextForeground );
	UxPutFontList( label39, BoldTextFont );
	UxPutBackground( label39, WindowBackground );
	UxPutHeight( label39, 36 );
	UxPutWidth( label39, 132 );
	UxPutY( label39, 114 );
	UxPutX( label39, 232 );

	UxPutScrollBarPlacement( scrolledWindow4, "bottom_right" );
	UxPutBackground( scrolledWindow4, WindowBackground );
	UxPutShadowThickness( scrolledWindow4, 0 );
	UxPutY( scrolledWindow4, -92 );
	UxPutX( scrolledWindow4, 215 );

	UxPutBorderColor( scrolledList4, SHelpBackground );
	UxPutSelectionPolicy( scrolledList4, "multiple_select" );
	UxPutScrollBarDisplayPolicy( scrolledList4, "static" );
	UxPutListSizePolicy( scrolledList4, "constant" );
	UxPutFontList( scrolledList4, "9x15" );
	UxPutBackground( scrolledList4, SHelpBackground );
	UxPutHeight( scrolledList4, 597 );
	UxPutWidth( scrolledList4, 311 );

	UxPutMarginHeight( rowColumn11, 0 );
	UxPutResizeHeight( rowColumn11, "true" );
	UxPutPacking( rowColumn11, "pack_tight" );
	UxPutBackground( rowColumn11, WindowBackground );
	UxPutHeight( rowColumn11, 422 );
	UxPutWidth( rowColumn11, 292 );
	UxPutY( rowColumn11, 20 );
	UxPutX( rowColumn11, 270 );

	UxPutMarginLeft( label7, 60 );
	UxPutForeground( label7, TextForeground );
	UxPutAlignment( label7, "alignment_beginning" );
	UxPutLabelString( label7, "Calibration" );
	UxPutFontList( label7, BoldTextFont );
	UxPutBackground( label7, LabelBackground );
	UxPutHeight( label7, 30 );
	UxPutWidth( label7, 190 );
	UxPutY( label7, 68 );
	UxPutX( label7, 6 );

	UxPutAdjustLast( rowColumn13, "true" );
	UxPutResizeWidth( rowColumn13, "false" );
	UxPutPacking( rowColumn13, "pack_tight" );
	UxPutOrientation( rowColumn13, "horizontal" );
	UxPutBackground( rowColumn13, WindowBackground );
	UxPutHeight( rowColumn13, 20 );
	UxPutWidth( rowColumn13, 250 );
	UxPutY( rowColumn13, 225 );
	UxPutX( rowColumn13, -7 );

	UxPutLabelString( toggleButton5, "Geometry and Rotation" );
	UxPutForeground( toggleButton5, TextForeground );
	UxPutFontList( toggleButton5, TextFont );
	UxPutBackground( toggleButton5, WindowBackground );
	UxPutRecomputeSize( toggleButton5, "false" );
	UxPutHeight( toggleButton5, 20 );
	UxPutWidth( toggleButton5, 195 );
	UxPutY( toggleButton5, -4 );
	UxPutX( toggleButton5, 3 );

	UxPutRecomputeSize( pb_main_search4, "false" );
	UxPutLabelString( pb_main_search4, "..." );
	UxPutForeground( pb_main_search4, ButtonForeground );
	UxPutFontList( pb_main_search4, BoldTextFont );
	UxPutBackground( pb_main_search4, ButtonBackground );
	UxPutHeight( pb_main_search4, 25 );
	UxPutY( pb_main_search4, 3 );
	UxPutX( pb_main_search4, 3 );

	UxPutAdjustLast( rowColumn14, "true" );
	UxPutResizeWidth( rowColumn14, "false" );
	UxPutPacking( rowColumn14, "pack_tight" );
	UxPutOrientation( rowColumn14, "horizontal" );
	UxPutBackground( rowColumn14, WindowBackground );
	UxPutHeight( rowColumn14, 20 );
	UxPutWidth( rowColumn14, 250 );
	UxPutY( rowColumn14, 189 );
	UxPutX( rowColumn14, -3 );

	UxPutLabelString( toggleButton6, "Order Definition" );
	UxPutForeground( toggleButton6, TextForeground );
	UxPutFontList( toggleButton6, TextFont );
	UxPutBackground( toggleButton6, WindowBackground );
	UxPutRecomputeSize( toggleButton6, "false" );
	UxPutHeight( toggleButton6, 20 );
	UxPutWidth( toggleButton6, 195 );
	UxPutY( toggleButton6, -4 );
	UxPutX( toggleButton6, 3 );

	UxPutRecomputeSize( pb_main_search5, "false" );
	UxPutLabelString( pb_main_search5, "..." );
	UxPutForeground( pb_main_search5, ButtonForeground );
	UxPutFontList( pb_main_search5, BoldTextFont );
	UxPutBackground( pb_main_search5, ButtonBackground );
	UxPutHeight( pb_main_search5, 25 );
	UxPutY( pb_main_search5, 3 );
	UxPutX( pb_main_search5, 3 );

	UxPutAdjustLast( rowColumn15, "true" );
	UxPutResizeWidth( rowColumn15, "false" );
	UxPutPacking( rowColumn15, "pack_tight" );
	UxPutOrientation( rowColumn15, "horizontal" );
	UxPutBackground( rowColumn15, WindowBackground );
	UxPutHeight( rowColumn15, 20 );
	UxPutWidth( rowColumn15, 250 );
	UxPutY( rowColumn15, 2 );
	UxPutX( rowColumn15, 2 );

	UxPutLabelString( toggleButton7, "Arc Line Search" );
	UxPutForeground( toggleButton7, TextForeground );
	UxPutFontList( toggleButton7, TextFont );
	UxPutBackground( toggleButton7, WindowBackground );
	UxPutRecomputeSize( toggleButton7, "false" );
	UxPutHeight( toggleButton7, 20 );
	UxPutWidth( toggleButton7, 195 );
	UxPutY( toggleButton7, -4 );
	UxPutX( toggleButton7, 3 );

	UxPutRecomputeSize( pb_main_search6, "false" );
	UxPutLabelString( pb_main_search6, "..." );
	UxPutForeground( pb_main_search6, ButtonForeground );
	UxPutFontList( pb_main_search6, BoldTextFont );
	UxPutBackground( pb_main_search6, ButtonBackground );
	UxPutHeight( pb_main_search6, 25 );
	UxPutY( pb_main_search6, 3 );
	UxPutX( pb_main_search6, 3 );

	UxPutAdjustLast( rowColumn16, "true" );
	UxPutResizeWidth( rowColumn16, "false" );
	UxPutPacking( rowColumn16, "pack_tight" );
	UxPutOrientation( rowColumn16, "horizontal" );
	UxPutBackground( rowColumn16, WindowBackground );
	UxPutHeight( rowColumn16, 20 );
	UxPutWidth( rowColumn16, 250 );
	UxPutY( rowColumn16, 137 );
	UxPutX( rowColumn16, 3 );

	UxPutLabelString( toggleButton8, "Wavelength Calibration" );
	UxPutForeground( toggleButton8, TextForeground );
	UxPutFontList( toggleButton8, TextFont );
	UxPutBackground( toggleButton8, WindowBackground );
	UxPutRecomputeSize( toggleButton8, "false" );
	UxPutHeight( toggleButton8, 20 );
	UxPutWidth( toggleButton8, 195 );
	UxPutY( toggleButton8, -4 );
	UxPutX( toggleButton8, 3 );

	UxPutRecomputeSize( pb_main_search7, "false" );
	UxPutLabelString( pb_main_search7, "..." );
	UxPutForeground( pb_main_search7, ButtonForeground );
	UxPutFontList( pb_main_search7, BoldTextFont );
	UxPutBackground( pb_main_search7, ButtonBackground );
	UxPutHeight( pb_main_search7, 25 );
	UxPutY( pb_main_search7, 3 );
	UxPutX( pb_main_search7, 3 );

	UxPutAdjustLast( rowColumn17, "true" );
	UxPutResizeWidth( rowColumn17, "false" );
	UxPutPacking( rowColumn17, "pack_tight" );
	UxPutOrientation( rowColumn17, "horizontal" );
	UxPutBackground( rowColumn17, WindowBackground );
	UxPutHeight( rowColumn17, 20 );
	UxPutWidth( rowColumn17, 250 );
	UxPutY( rowColumn17, 24 );
	UxPutX( rowColumn17, 2 );

	UxPutLabelString( toggleButton9, "EMMI Mode:" );
	UxPutForeground( toggleButton9, TextForeground );
	UxPutFontList( toggleButton9, TextFont );
	UxPutBackground( toggleButton9, WindowBackground );
	UxPutRecomputeSize( toggleButton9, "false" );
	UxPutHeight( toggleButton9, 20 );
	UxPutWidth( toggleButton9, 120 );
	UxPutY( toggleButton9, -4 );
	UxPutX( toggleButton9, 3 );

	UxPutX( menu2_p7, 520 );
	UxPutWidth( menu2_p7, 120 );
	UxPutForeground( menu2_p7, TextForeground );
	UxPutBackground( menu2_p7, WindowBackground );
	UxPutRowColumnType( menu2_p7, "menu_pulldown" );

	UxPutX( mn_tol_angstroms7, 520 );
	UxPutWidth( mn_tol_angstroms7, 120 );
	UxPutFontList( mn_tol_angstroms7, TextFont );
	UxPutLabelString( mn_tol_angstroms7, "#9CD3" );

	UxPutX( mn_tol_pixels7, 520 );
	UxPutWidth( mn_tol_pixels7, 120 );
	UxPutFontList( mn_tol_pixels7, TextFont );
	UxPutLabelString( mn_tol_pixels7, "#9CD4" );

	UxPutFontList( menu2_p7_b3, TextFont );
	UxPutLabelString( menu2_p7_b3, "#10CD3" );

	UxPutFontList( menu2_p7_b4, TextFont );
	UxPutLabelString( menu2_p7_b4, "#10CD4" );

	UxPutFontList( menu2_p7_b5, TextFont );
	UxPutLabelString( menu2_p7_b5, "#10CD5" );

	UxPutFontList( menu2_p7_b6, TextFont );
	UxPutLabelString( menu2_p7_b6, "#10CD6" );

	UxPutTranslations( mn_tol7, transTable2 );
	UxPutWidth( mn_tol7, 120 );
	UxPutLabelString( mn_tol7, " " );
	UxPutSpacing( mn_tol7, 0 );
	UxPutMarginWidth( mn_tol7, 0 );
	UxPutForeground( mn_tol7, TextForeground );
	UxPutBackground( mn_tol7, WindowBackground );
	UxPutY( mn_tol7, 76 );
	UxPutX( mn_tol7, 474 );
	UxPutRowColumnType( mn_tol7, "menu_option" );

	UxPutForeground( label1, TextForeground );
	UxPutAlignment( label1, "alignment_beginning" );
	UxPutLabelString( label1, "Reduction" );
	UxPutFontList( label1, BoldTextFont );
	UxPutBackground( label1, LabelBackground );
	UxPutHeight( label1, 30 );
	UxPutWidth( label1, 190 );
	UxPutY( label1, 68 );
	UxPutX( label1, 6 );

	UxPutAdjustLast( rowColumn18, "true" );
	UxPutResizeWidth( rowColumn18, "false" );
	UxPutPacking( rowColumn18, "pack_tight" );
	UxPutOrientation( rowColumn18, "horizontal" );
	UxPutBackground( rowColumn18, WindowBackground );
	UxPutHeight( rowColumn18, 20 );
	UxPutWidth( rowColumn18, 250 );
	UxPutY( rowColumn18, 24 );
	UxPutX( rowColumn18, 2 );

	UxPutTranslations( toggleButton3, transTable2 );
	UxPutSelectColor( toggleButton3, "Yellow" );
	UxPutLabelString( toggleButton3, "Rotation" );
	UxPutForeground( toggleButton3, TextForeground );
	UxPutFontList( toggleButton3, TextFont );
	UxPutBackground( toggleButton3, WindowBackground );
	UxPutRecomputeSize( toggleButton3, "false" );
	UxPutHeight( toggleButton3, 20 );
	UxPutWidth( toggleButton3, 195 );
	UxPutY( toggleButton3, -4 );
	UxPutX( toggleButton3, 3 );

	UxPutRecomputeSize( pb_main_search2, "false" );
	UxPutLabelString( pb_main_search2, "..." );
	UxPutForeground( pb_main_search2, ButtonForeground );
	UxPutFontList( pb_main_search2, BoldTextFont );
	UxPutBackground( pb_main_search2, ButtonBackground );
	UxPutHeight( pb_main_search2, 25 );
	UxPutY( pb_main_search2, 3 );
	UxPutX( pb_main_search2, 3 );

	UxPutAdjustLast( rowColumn19, "true" );
	UxPutResizeWidth( rowColumn19, "false" );
	UxPutPacking( rowColumn19, "pack_tight" );
	UxPutOrientation( rowColumn19, "horizontal" );
	UxPutBackground( rowColumn19, WindowBackground );
	UxPutHeight( rowColumn19, 20 );
	UxPutWidth( rowColumn19, 250 );
	UxPutY( rowColumn19, 24 );
	UxPutX( rowColumn19, 2 );

	UxPutTranslations( toggleButton10, transTable2 );
	UxPutSelectColor( toggleButton10, "Yellow" );
	UxPutLabelString( toggleButton10, "Inter-Order Background" );
	UxPutForeground( toggleButton10, TextForeground );
	UxPutFontList( toggleButton10, TextFont );
	UxPutBackground( toggleButton10, WindowBackground );
	UxPutRecomputeSize( toggleButton10, "false" );
	UxPutHeight( toggleButton10, 20 );
	UxPutWidth( toggleButton10, 195 );
	UxPutY( toggleButton10, -4 );
	UxPutX( toggleButton10, 3 );

	UxPutRecomputeSize( pb_main_search9, "false" );
	UxPutLabelString( pb_main_search9, "..." );
	UxPutForeground( pb_main_search9, ButtonForeground );
	UxPutFontList( pb_main_search9, BoldTextFont );
	UxPutBackground( pb_main_search9, ButtonBackground );
	UxPutHeight( pb_main_search9, 25 );
	UxPutY( pb_main_search9, 3 );
	UxPutX( pb_main_search9, 3 );

	UxPutAdjustLast( rowColumn20, "true" );
	UxPutResizeWidth( rowColumn20, "false" );
	UxPutPacking( rowColumn20, "pack_tight" );
	UxPutOrientation( rowColumn20, "horizontal" );
	UxPutBackground( rowColumn20, WindowBackground );
	UxPutHeight( rowColumn20, 20 );
	UxPutWidth( rowColumn20, 250 );
	UxPutY( rowColumn20, 24 );
	UxPutX( rowColumn20, 2 );

	UxPutTranslations( toggleButton11, transTable2 );
	UxPutSelectColor( toggleButton11, "Yellow" );
	UxPutLabelString( toggleButton11, "Extraction" );
	UxPutForeground( toggleButton11, TextForeground );
	UxPutFontList( toggleButton11, TextFont );
	UxPutBackground( toggleButton11, WindowBackground );
	UxPutRecomputeSize( toggleButton11, "false" );
	UxPutHeight( toggleButton11, 20 );
	UxPutWidth( toggleButton11, 195 );
	UxPutY( toggleButton11, -4 );
	UxPutX( toggleButton11, 3 );

	UxPutRecomputeSize( pb_main_search10, "false" );
	UxPutLabelString( pb_main_search10, "..." );
	UxPutForeground( pb_main_search10, ButtonForeground );
	UxPutFontList( pb_main_search10, BoldTextFont );
	UxPutBackground( pb_main_search10, ButtonBackground );
	UxPutHeight( pb_main_search10, 25 );
	UxPutY( pb_main_search10, 3 );
	UxPutX( pb_main_search10, 3 );

	UxPutAdjustLast( rowColumn21, "true" );
	UxPutResizeWidth( rowColumn21, "false" );
	UxPutPacking( rowColumn21, "pack_tight" );
	UxPutOrientation( rowColumn21, "horizontal" );
	UxPutBackground( rowColumn21, WindowBackground );
	UxPutHeight( rowColumn21, 20 );
	UxPutWidth( rowColumn21, 250 );
	UxPutY( rowColumn21, 24 );
	UxPutX( rowColumn21, 2 );

	UxPutTranslations( toggleButton12, transTable2 );
	UxPutSelectColor( toggleButton12, "Yellow" );
	UxPutLabelString( toggleButton12, "Sky Background" );
	UxPutForeground( toggleButton12, TextForeground );
	UxPutFontList( toggleButton12, TextFont );
	UxPutBackground( toggleButton12, WindowBackground );
	UxPutRecomputeSize( toggleButton12, "false" );
	UxPutHeight( toggleButton12, 20 );
	UxPutWidth( toggleButton12, 195 );
	UxPutY( toggleButton12, -4 );
	UxPutX( toggleButton12, 3 );

	UxPutRecomputeSize( pb_main_search11, "false" );
	UxPutLabelString( pb_main_search11, "..." );
	UxPutForeground( pb_main_search11, ButtonForeground );
	UxPutFontList( pb_main_search11, BoldTextFont );
	UxPutBackground( pb_main_search11, ButtonBackground );
	UxPutHeight( pb_main_search11, 25 );
	UxPutY( pb_main_search11, 3 );
	UxPutX( pb_main_search11, 3 );

	UxPutSensitive( rowColumn9, "true" );
	UxPutAdjustLast( rowColumn9, "true" );
	UxPutResizeWidth( rowColumn9, "false" );
	UxPutPacking( rowColumn9, "pack_tight" );
	UxPutOrientation( rowColumn9, "horizontal" );
	UxPutBackground( rowColumn9, WindowBackground );
	UxPutHeight( rowColumn9, 20 );
	UxPutWidth( rowColumn9, 250 );
	UxPutY( rowColumn9, 24 );
	UxPutX( rowColumn9, 2 );

	UxPutTranslations( toggleButton1, transTable2 );
	UxPutSelectColor( toggleButton1, "Yellow" );
	UxPutLabelString( toggleButton1, "Resampling" );
	UxPutForeground( toggleButton1, TextForeground );
	UxPutFontList( toggleButton1, TextFont );
	UxPutBackground( toggleButton1, WindowBackground );
	UxPutRecomputeSize( toggleButton1, "false" );
	UxPutHeight( toggleButton1, 29 );
	UxPutWidth( toggleButton1, 195 );
	UxPutY( toggleButton1, 3 );
	UxPutX( toggleButton1, 3 );

	UxPutRecomputeSize( pb_main_search1, "false" );
	UxPutLabelString( pb_main_search1, "..." );
	UxPutForeground( pb_main_search1, ButtonForeground );
	UxPutFontList( pb_main_search1, BoldTextFont );
	UxPutBackground( pb_main_search1, ButtonBackground );
	UxPutHeight( pb_main_search1, 25 );
	UxPutY( pb_main_search1, 3 );
	UxPutX( pb_main_search1, 201 );

	UxPutAdjustLast( rowColumn10, "true" );
	UxPutResizeWidth( rowColumn10, "false" );
	UxPutPacking( rowColumn10, "pack_tight" );
	UxPutOrientation( rowColumn10, "horizontal" );
	UxPutBackground( rowColumn10, WindowBackground );
	UxPutHeight( rowColumn10, 20 );
	UxPutWidth( rowColumn10, 250 );
	UxPutY( rowColumn10, 24 );
	UxPutX( rowColumn10, 2 );

	UxPutTranslations( toggleButton2, transTable2 );
	UxPutSelectColor( toggleButton2, "Yellow" );
	UxPutLabelString( toggleButton2, "Flat-Field" );
	UxPutForeground( toggleButton2, TextForeground );
	UxPutFontList( toggleButton2, TextFont );
	UxPutBackground( toggleButton2, WindowBackground );
	UxPutRecomputeSize( toggleButton2, "false" );
	UxPutHeight( toggleButton2, 20 );
	UxPutWidth( toggleButton2, 195 );
	UxPutY( toggleButton2, -4 );
	UxPutX( toggleButton2, 3 );

	UxPutRecomputeSize( pb_main_search3, "false" );
	UxPutLabelString( pb_main_search3, "..." );
	UxPutForeground( pb_main_search3, ButtonForeground );
	UxPutFontList( pb_main_search3, BoldTextFont );
	UxPutBackground( pb_main_search3, ButtonBackground );
	UxPutHeight( pb_main_search3, 25 );
	UxPutY( pb_main_search3, 3 );
	UxPutX( pb_main_search3, 3 );

	UxPutAdjustLast( rowColumn12, "true" );
	UxPutResizeWidth( rowColumn12, "false" );
	UxPutPacking( rowColumn12, "pack_tight" );
	UxPutOrientation( rowColumn12, "horizontal" );
	UxPutBackground( rowColumn12, WindowBackground );
	UxPutHeight( rowColumn12, 20 );
	UxPutWidth( rowColumn12, 250 );
	UxPutY( rowColumn12, 24 );
	UxPutX( rowColumn12, 2 );

	UxPutTranslations( toggleButton4, transTable2 );
	UxPutSelectColor( toggleButton4, "Yellow" );
	UxPutLabelString( toggleButton4, "Chromatic Response" );
	UxPutForeground( toggleButton4, TextForeground );
	UxPutFontList( toggleButton4, TextFont );
	UxPutBackground( toggleButton4, WindowBackground );
	UxPutRecomputeSize( toggleButton4, "false" );
	UxPutHeight( toggleButton4, 20 );
	UxPutWidth( toggleButton4, 195 );
	UxPutY( toggleButton4, -4 );
	UxPutX( toggleButton4, 3 );

	UxPutRecomputeSize( pb_main_search12, "false" );
	UxPutLabelString( pb_main_search12, "..." );
	UxPutForeground( pb_main_search12, ButtonForeground );
	UxPutFontList( pb_main_search12, BoldTextFont );
	UxPutBackground( pb_main_search12, ButtonBackground );
	UxPutHeight( pb_main_search12, 25 );
	UxPutY( pb_main_search12, 3 );
	UxPutX( pb_main_search12, 3 );

	UxPutAdjustLast( rowColumn22, "true" );
	UxPutResizeWidth( rowColumn22, "false" );
	UxPutPacking( rowColumn22, "pack_tight" );
	UxPutOrientation( rowColumn22, "horizontal" );
	UxPutBackground( rowColumn22, WindowBackground );
	UxPutHeight( rowColumn22, 20 );
	UxPutWidth( rowColumn22, 250 );
	UxPutY( rowColumn22, 24 );
	UxPutX( rowColumn22, 2 );

	UxPutTranslations( toggleButton13, transTable2 );
	UxPutSelectColor( toggleButton13, "Yellow" );
	UxPutLabelString( toggleButton13, "Order Merging" );
	UxPutForeground( toggleButton13, TextForeground );
	UxPutFontList( toggleButton13, TextFont );
	UxPutBackground( toggleButton13, WindowBackground );
	UxPutRecomputeSize( toggleButton13, "false" );
	UxPutHeight( toggleButton13, 20 );
	UxPutWidth( toggleButton13, 195 );
	UxPutY( toggleButton13, -4 );
	UxPutX( toggleButton13, 3 );

	UxPutRecomputeSize( pb_main_search13, "false" );
	UxPutLabelString( pb_main_search13, "..." );
	UxPutForeground( pb_main_search13, ButtonForeground );
	UxPutFontList( pb_main_search13, BoldTextFont );
	UxPutBackground( pb_main_search13, ButtonBackground );
	UxPutHeight( pb_main_search13, 25 );
	UxPutY( pb_main_search13, 3 );
	UxPutX( pb_main_search13, 3 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_ApplicWindow()
{
	/* Create the swidgets */

	ApplicWindow = UxCreateApplicationShell( "ApplicWindow", NO_PARENT );
	UxPutContext( ApplicWindow, UxApplicWindowContext );

	MainWindow = UxCreateMainWindow( "MainWindow", ApplicWindow );
	menu1 = UxCreateRowColumn( "menu1", MainWindow );
	menu1_p1 = UxCreateRowColumn( "menu1_p1", menu1 );
	menu1_p1_b1 = UxCreatePushButtonGadget( "menu1_p1_b1", menu1_p1 );
	menu1_p1_b2 = UxCreatePushButtonGadget( "menu1_p1_b2", menu1_p1 );
	menu1_p1_b3 = UxCreatePushButtonGadget( "menu1_p1_b3", menu1_p1 );
	menu1_p1_b5 = UxCreateSeparator( "menu1_p1_b5", menu1_p1 );
	menu1_p1_b6 = UxCreatePushButtonGadget( "menu1_p1_b6", menu1_p1 );
	menu1_p1_b7 = UxCreatePushButtonGadget( "menu1_p1_b7", menu1_p1 );
	menu1_p1_b8 = UxCreateSeparator( "menu1_p1_b8", menu1_p1 );
	menu1_p1_b4 = UxCreatePushButtonGadget( "menu1_p1_b4", menu1_p1 );
	menu1_top_b1 = UxCreateCascadeButton( "menu1_top_b1", menu1 );
	menu1_p4 = UxCreateRowColumn( "menu1_p4", menu1 );
	menu1_p4_b1 = UxCreatePushButtonGadget( "menu1_p4_b1", menu1_p4 );
	menu1_p4_b2 = UxCreatePushButtonGadget( "menu1_p4_b2", menu1_p4 );
	menu1_p4_b8 = UxCreateSeparatorGadget( "menu1_p4_b8", menu1_p4 );
	menu1_p4_b10 = UxCreatePushButtonGadget( "menu1_p4_b10", menu1_p4 );
	menu1_p4_b13 = UxCreatePushButtonGadget( "menu1_p4_b13", menu1_p4 );
	menu1_p4_b12 = UxCreateSeparatorGadget( "menu1_p4_b12", menu1_p4 );
	menu1_p4_b3 = UxCreatePushButtonGadget( "menu1_p4_b3", menu1_p4 );
	menu1_p4_b4 = UxCreatePushButtonGadget( "menu1_p4_b4", menu1_p4 );
	menu1_p4_b9 = UxCreateSeparatorGadget( "menu1_p4_b9", menu1_p4 );
	menu1_p4_b5 = UxCreatePushButtonGadget( "menu1_p4_b5", menu1_p4 );
	menu1_p4_b11 = UxCreatePushButtonGadget( "menu1_p4_b11", menu1_p4 );
	menu1_p4_b14 = UxCreateSeparatorGadget( "menu1_p4_b14", menu1_p4 );
	menu1_p4_b15 = UxCreatePushButtonGadget( "menu1_p4_b15", menu1_p4 );
	menu1_top_b4 = UxCreateCascadeButton( "menu1_top_b4", menu1 );
	menu1_p5 = UxCreateRowColumn( "menu1_p5", menu1 );
	menu_help_context = UxCreatePushButtonGadget( "menu_help_context", menu1_p5 );
	menu_help_help = UxCreatePushButtonGadget( "menu_help_help", menu1_p5 );
	menu1_p5_b5 = UxCreateSeparatorGadget( "menu1_p5_b5", menu1_p5 );
	menu_help_tutorial = UxCreatePushButtonGadget( "menu_help_tutorial", menu1_p5 );
	menu_help_version = UxCreatePushButtonGadget( "menu_help_version", menu1_p5 );
	menu1_top_b5 = UxCreateCascadeButton( "menu1_top_b5", menu1 );
	form1 = UxCreateForm( "form1", MainWindow );
	form5 = UxCreateForm( "form5", form1 );
	pb_main_batch = UxCreatePushButton( "pb_main_batch", form5 );
	pb_main_ident = UxCreatePushButton( "pb_main_ident", form5 );
	pb_main_batch1 = UxCreatePushButton( "pb_main_batch1", form5 );
	pb_main_batch2 = UxCreatePushButton( "pb_main_batch2", form5 );
	pb_main_batch3 = UxCreatePushButton( "pb_main_batch3", form5 );
	separator12 = UxCreateSeparator( "separator12", form1 );
	shelp_main = UxCreateText( "shelp_main", form1 );
	separator1 = UxCreateSeparator( "separator1", form1 );
	form22 = UxCreateForm( "form22", form1 );
	ListWindow1 = UxCreateScrolledWindow( "ListWindow1", form22 );
	scrolledList1 = UxCreateScrolledText( "scrolledList1", ListWindow1 );
	tf_wrang1 = UxCreateTextField( "tf_wrang1", form22 );
	label3 = UxCreateLabel( "label3", form22 );
	label4 = UxCreateLabel( "label4", form22 );
	tf_wrang2 = UxCreateTextField( "tf_wrang2", form22 );
	form23 = UxCreateForm( "form23", form22 );
	label2 = UxCreateLabel( "label2", form23 );
	tf_session = UxCreateTextField( "tf_session", form23 );
	pb_main_batch4 = UxCreatePushButton( "pb_main_batch4", form23 );
	form24 = UxCreateForm( "form24", form22 );
	label5 = UxCreateLabel( "label5", form24 );
	tf_session1 = UxCreateTextField( "tf_session1", form24 );
	label32 = UxCreateLabel( "label32", form22 );
	label39 = UxCreateLabel( "label39", form22 );
	scrolledWindow4 = UxCreateScrolledWindow( "scrolledWindow4", form22 );
	scrolledList4 = UxCreateScrolledList( "scrolledList4", scrolledWindow4 );
	rowColumn11 = UxCreateRowColumn( "rowColumn11", form1 );
	label7 = UxCreateLabel( "label7", rowColumn11 );
	rowColumn13 = UxCreateRowColumn( "rowColumn13", rowColumn11 );
	toggleButton5 = UxCreateLabel( "toggleButton5", rowColumn13 );
	pb_main_search4 = UxCreatePushButton( "pb_main_search4", rowColumn13 );
	rowColumn14 = UxCreateRowColumn( "rowColumn14", rowColumn11 );
	toggleButton6 = UxCreateLabel( "toggleButton6", rowColumn14 );
	pb_main_search5 = UxCreatePushButton( "pb_main_search5", rowColumn14 );
	rowColumn15 = UxCreateRowColumn( "rowColumn15", rowColumn11 );
	toggleButton7 = UxCreateLabel( "toggleButton7", rowColumn15 );
	pb_main_search6 = UxCreatePushButton( "pb_main_search6", rowColumn15 );
	rowColumn16 = UxCreateRowColumn( "rowColumn16", rowColumn11 );
	toggleButton8 = UxCreateLabel( "toggleButton8", rowColumn16 );
	pb_main_search7 = UxCreatePushButton( "pb_main_search7", rowColumn16 );
	rowColumn17 = UxCreateRowColumn( "rowColumn17", rowColumn11 );
	toggleButton9 = UxCreateLabel( "toggleButton9", rowColumn17 );
	menu2_p7 = UxCreateRowColumn( "menu2_p7", rowColumn17 );
	mn_tol_angstroms7 = UxCreatePushButtonGadget( "mn_tol_angstroms7", menu2_p7 );
	mn_tol_pixels7 = UxCreatePushButtonGadget( "mn_tol_pixels7", menu2_p7 );
	menu2_p7_b3 = UxCreatePushButtonGadget( "menu2_p7_b3", menu2_p7 );
	menu2_p7_b4 = UxCreatePushButtonGadget( "menu2_p7_b4", menu2_p7 );
	menu2_p7_b5 = UxCreatePushButtonGadget( "menu2_p7_b5", menu2_p7 );
	menu2_p7_b6 = UxCreatePushButtonGadget( "menu2_p7_b6", menu2_p7 );
	mn_tol7 = UxCreateRowColumn( "mn_tol7", rowColumn17 );
	label1 = UxCreateLabel( "label1", rowColumn11 );
	rowColumn18 = UxCreateRowColumn( "rowColumn18", rowColumn11 );
	toggleButton3 = UxCreateToggleButton( "toggleButton3", rowColumn18 );
	pb_main_search2 = UxCreatePushButton( "pb_main_search2", rowColumn18 );
	rowColumn19 = UxCreateRowColumn( "rowColumn19", rowColumn11 );
	toggleButton10 = UxCreateToggleButton( "toggleButton10", rowColumn19 );
	pb_main_search9 = UxCreatePushButton( "pb_main_search9", rowColumn19 );
	rowColumn20 = UxCreateRowColumn( "rowColumn20", rowColumn11 );
	toggleButton11 = UxCreateToggleButton( "toggleButton11", rowColumn20 );
	pb_main_search10 = UxCreatePushButton( "pb_main_search10", rowColumn20 );
	rowColumn21 = UxCreateRowColumn( "rowColumn21", rowColumn11 );
	toggleButton12 = UxCreateToggleButton( "toggleButton12", rowColumn21 );
	pb_main_search11 = UxCreatePushButton( "pb_main_search11", rowColumn21 );
	rowColumn9 = UxCreateRowColumn( "rowColumn9", rowColumn11 );
	toggleButton1 = UxCreateToggleButton( "toggleButton1", rowColumn9 );
	pb_main_search1 = UxCreatePushButton( "pb_main_search1", rowColumn9 );
	rowColumn10 = UxCreateRowColumn( "rowColumn10", rowColumn11 );
	toggleButton2 = UxCreateToggleButton( "toggleButton2", rowColumn10 );
	pb_main_search3 = UxCreatePushButton( "pb_main_search3", rowColumn10 );
	rowColumn12 = UxCreateRowColumn( "rowColumn12", rowColumn11 );
	toggleButton4 = UxCreateToggleButton( "toggleButton4", rowColumn12 );
	pb_main_search12 = UxCreatePushButton( "pb_main_search12", rowColumn12 );
	rowColumn22 = UxCreateRowColumn( "rowColumn22", rowColumn11 );
	toggleButton13 = UxCreateToggleButton( "toggleButton13", rowColumn22 );
	pb_main_search13 = UxCreatePushButton( "pb_main_search13", rowColumn22 );

	_Uxinit_ApplicWindow();

	/* Create the X widgets */

	UxCreateWidget( ApplicWindow );
	UxCreateWidget( MainWindow );
	UxCreateWidget( menu1 );
	UxCreateWidget( menu1_p1 );
	UxCreateWidget( menu1_p1_b1 );
	UxCreateWidget( menu1_p1_b2 );
	UxCreateWidget( menu1_p1_b3 );
	UxCreateWidget( menu1_p1_b5 );
	UxCreateWidget( menu1_p1_b6 );
	UxCreateWidget( menu1_p1_b7 );
	UxCreateWidget( menu1_p1_b8 );
	UxCreateWidget( menu1_p1_b4 );
	UxPutSubMenuId( menu1_top_b1, "menu1_p1" );
	UxCreateWidget( menu1_top_b1 );

	UxCreateWidget( menu1_p4 );
	UxCreateWidget( menu1_p4_b1 );
	UxCreateWidget( menu1_p4_b2 );
	UxCreateWidget( menu1_p4_b8 );
	UxCreateWidget( menu1_p4_b10 );
	UxCreateWidget( menu1_p4_b13 );
	UxCreateWidget( menu1_p4_b12 );
	UxCreateWidget( menu1_p4_b3 );
	UxCreateWidget( menu1_p4_b4 );
	UxCreateWidget( menu1_p4_b9 );
	UxCreateWidget( menu1_p4_b5 );
	UxCreateWidget( menu1_p4_b11 );
	UxCreateWidget( menu1_p4_b14 );
	UxCreateWidget( menu1_p4_b15 );
	UxPutSubMenuId( menu1_top_b4, "menu1_p4" );
	UxCreateWidget( menu1_top_b4 );

	UxCreateWidget( menu1_p5 );
	UxCreateWidget( menu_help_context );
	UxCreateWidget( menu_help_help );
	UxCreateWidget( menu1_p5_b5 );
	UxCreateWidget( menu_help_tutorial );
	UxCreateWidget( menu_help_version );
	UxPutSubMenuId( menu1_top_b5, "menu1_p5" );
	UxCreateWidget( menu1_top_b5 );

	UxCreateWidget( form1 );
	UxPutTopOffset( form5, 60 );
	UxPutTopAttachment( form5, "attach_none" );
	UxPutBottomOffset( form5, 0 );
	UxPutRightOffset( form5, 0 );
	UxPutRightAttachment( form5, "attach_form" );
	UxPutLeftOffset( form5, 0 );
	UxPutLeftAttachment( form5, "attach_form" );
	UxPutBottomAttachment( form5, "attach_form" );
	UxCreateWidget( form5 );

	UxPutBottomOffset( pb_main_batch, 5 );
	UxPutBottomAttachment( pb_main_batch, "attach_form" );
	UxPutLeftWidget( pb_main_batch, "" );
	UxPutLeftOffset( pb_main_batch, 5 );
	UxPutLeftAttachment( pb_main_batch, "attach_form" );
	UxCreateWidget( pb_main_batch );

	UxPutLeftWidget( pb_main_ident, "pb_main_batch" );
	UxPutLeftOffset( pb_main_ident, 5 );
	UxPutLeftAttachment( pb_main_ident, "attach_widget" );
	UxPutBottomOffset( pb_main_ident, 5 );
	UxPutBottomAttachment( pb_main_ident, "attach_form" );
	UxCreateWidget( pb_main_ident );

	UxPutBottomOffset( pb_main_batch1, 5 );
	UxPutBottomAttachment( pb_main_batch1, "attach_form" );
	UxPutLeftWidget( pb_main_batch1, "pb_main_ident" );
	UxPutLeftOffset( pb_main_batch1, 5 );
	UxPutLeftAttachment( pb_main_batch1, "attach_widget" );
	UxCreateWidget( pb_main_batch1 );

	UxPutBottomOffset( pb_main_batch2, 5 );
	UxPutBottomAttachment( pb_main_batch2, "attach_form" );
	UxPutLeftWidget( pb_main_batch2, "pb_main_batch1" );
	UxPutLeftOffset( pb_main_batch2, 5 );
	UxPutLeftAttachment( pb_main_batch2, "attach_widget" );
	UxCreateWidget( pb_main_batch2 );

	UxPutBottomOffset( pb_main_batch3, 5 );
	UxPutBottomAttachment( pb_main_batch3, "attach_form" );
	UxPutRightOffset( pb_main_batch3, 5 );
	UxPutRightAttachment( pb_main_batch3, "attach_form" );
	UxPutLeftWidget( pb_main_batch3, "pb_main_batch2" );
	UxPutLeftOffset( pb_main_batch3, 5 );
	UxPutLeftAttachment( pb_main_batch3, "attach_none" );
	UxCreateWidget( pb_main_batch3 );

	UxPutLeftOffset( separator12, 0 );
	UxPutRightOffset( separator12, 0 );
	UxPutRightAttachment( separator12, "attach_form" );
	UxPutLeftAttachment( separator12, "attach_form" );
	UxPutBottomWidget( separator12, "form5" );
	UxPutBottomAttachment( separator12, "attach_widget" );
	UxCreateWidget( separator12 );

	UxPutRightOffset( shelp_main, 5 );
	UxPutRightAttachment( shelp_main, "attach_form" );
	UxPutLeftOffset( shelp_main, 5 );
	UxPutLeftAttachment( shelp_main, "attach_form" );
	UxPutBottomWidget( shelp_main, "separator12" );
	UxPutBottomAttachment( shelp_main, "attach_widget" );
	UxCreateWidget( shelp_main );

	UxPutRightOffset( separator1, 0 );
	UxPutRightAttachment( separator1, "attach_form" );
	UxPutLeftOffset( separator1, 0 );
	UxPutLeftAttachment( separator1, "attach_form" );
	UxPutBottomWidget( separator1, "shelp_main" );
	UxPutBottomAttachment( separator1, "attach_widget" );
	UxCreateWidget( separator1 );

	UxPutBottomWidget( form22, "separator1" );
	UxPutBottomOffset( form22, 5 );
	UxPutBottomAttachment( form22, "attach_widget" );
	UxPutTopOffset( form22, 5 );
	UxPutTopAttachment( form22, "attach_form" );
	UxPutRightOffset( form22, 5 );
	UxPutRightAttachment( form22, "attach_form" );
	UxPutLeftOffset( form22, 250 );
	UxPutLeftAttachment( form22, "attach_form" );
	UxCreateWidget( form22 );

	UxPutTopOffset( ListWindow1, 160 );
	UxPutTopAttachment( ListWindow1, "attach_form" );
	UxPutBottomOffset( ListWindow1, 60 );
	UxPutBottomAttachment( ListWindow1, "attach_form" );
	UxPutRightOffset( ListWindow1, 350 );
	UxPutRightAttachment( ListWindow1, "attach_form" );
	UxPutLeftOffset( ListWindow1, 10 );
	UxPutLeftAttachment( ListWindow1, "attach_form" );
	UxCreateWidget( ListWindow1 );

	UxCreateWidget( scrolledList1 );
	UxPutBottomOffset( tf_wrang1, 5 );
	UxPutBottomAttachment( tf_wrang1, "attach_form" );
	UxPutRightOffset( tf_wrang1, 220 );
	UxPutRightAttachment( tf_wrang1, "attach_form" );
	UxPutLeftPosition( tf_wrang1, 100 );
	UxPutLeftOffset( tf_wrang1, 110 );
	UxPutLeftAttachment( tf_wrang1, "attach_form" );
	UxPutTopWidget( tf_wrang1, "ListWindow1" );
	UxPutTopOffset( tf_wrang1, 40 );
	UxPutTopAttachment( tf_wrang1, "attach_none" );
	UxCreateWidget( tf_wrang1 );

	UxPutBottomOffset( label3, 5 );
	UxPutBottomAttachment( label3, "attach_form" );
	UxPutRightOffset( label3, 5 );
	UxPutLeftOffset( label3, 5 );
	UxPutLeftAttachment( label3, "attach_form" );
	UxPutTopWidget( label3, "ListWindow1" );
	UxPutTopOffset( label3, 40 );
	UxPutTopAttachment( label3, "attach_none" );
	UxCreateWidget( label3 );

	UxPutBottomOffset( label4, 5 );
	UxPutBottomAttachment( label4, "attach_form" );
	UxPutLeftWidget( label4, "tf_wrang1" );
	UxPutRightOffset( label4, 110 );
	UxPutRightAttachment( label4, "attach_form" );
	UxPutLeftOffset( label4, 5 );
	UxPutLeftAttachment( label4, "attach_widget" );
	UxPutTopWidget( label4, "ListWindow1" );
	UxPutTopOffset( label4, 40 );
	UxPutTopAttachment( label4, "attach_none" );
	UxCreateWidget( label4 );

	UxPutBottomOffset( tf_wrang2, 5 );
	UxPutBottomAttachment( tf_wrang2, "attach_form" );
	UxPutRightOffset( tf_wrang2, 5 );
	UxPutRightAttachment( tf_wrang2, "attach_form" );
	UxPutLeftWidget( tf_wrang2, "label4" );
	UxPutLeftOffset( tf_wrang2, 5 );
	UxPutLeftAttachment( tf_wrang2, "attach_widget" );
	UxPutTopWidget( tf_wrang2, "ListWindow1" );
	UxPutTopOffset( tf_wrang2, 40 );
	UxPutTopAttachment( tf_wrang2, "attach_none" );
	UxCreateWidget( tf_wrang2 );

	UxPutTopAttachment( form23, "attach_form" );
	UxPutTopOffset( form23, 60 );
	UxPutRightOffset( form23, 4 );
	UxPutRightAttachment( form23, "attach_form" );
	UxPutLeftOffset( form23, 4 );
	UxPutLeftAttachment( form23, "attach_form" );
	UxCreateWidget( form23 );

	UxPutLeftOffset( label2, 5 );
	UxPutLeftAttachment( label2, "attach_form" );
	UxCreateWidget( label2 );

	UxPutRightOffset( tf_session, 85 );
	UxPutRightAttachment( tf_session, "attach_form" );
	UxPutLeftOffset( tf_session, 90 );
	UxPutLeftAttachment( tf_session, "attach_form" );
	UxCreateWidget( tf_session );

	UxPutRightOffset( pb_main_batch4, 5 );
	UxPutRightAttachment( pb_main_batch4, "attach_form" );
	UxCreateWidget( pb_main_batch4 );

	UxPutTopOffset( form24, 10 );
	UxPutTopAttachment( form24, "attach_form" );
	UxCreateWidget( form24 );

	UxPutLeftOffset( label5, 6 );
	UxPutLeftAttachment( label5, "attach_form" );
	UxCreateWidget( label5 );

	UxPutRightOffset( tf_session1, 80 );
	UxPutRightAttachment( tf_session1, "attach_form" );
	UxPutLeftOffset( tf_session1, 90 );
	UxPutLeftAttachment( tf_session1, "attach_form" );
	UxCreateWidget( tf_session1 );

	UxPutLeftOffset( label32, 10 );
	UxPutLeftAttachment( label32, "attach_form" );
	UxPutTopOffset( label32, 110 );
	UxPutTopAttachment( label32, "attach_form" );
	UxCreateWidget( label32 );

	UxPutTopOffset( label39, 110 );
	UxPutTopAttachment( label39, "attach_form" );
	UxPutLeftOffset( label39, 230 );
	UxPutLeftAttachment( label39, "attach_form" );
	UxCreateWidget( label39 );

	UxPutRightOffset( scrolledWindow4, 10 );
	UxPutRightAttachment( scrolledWindow4, "attach_form" );
	UxPutTopOffset( scrolledWindow4, 160 );
	UxPutTopAttachment( scrolledWindow4, "attach_form" );
	UxPutLeftWidget( scrolledWindow4, "ListWindow1" );
	UxPutLeftOffset( scrolledWindow4, 10 );
	UxPutLeftAttachment( scrolledWindow4, "attach_widget" );
	UxPutBottomOffset( scrolledWindow4, 60 );
	UxPutBottomAttachment( scrolledWindow4, "attach_form" );
	UxCreateWidget( scrolledWindow4 );

	UxCreateWidget( scrolledList4 );
	createCB_scrolledList4( UxGetWidget( scrolledList4 ),
			(XtPointer) UxApplicWindowContext, (XtPointer) NULL );

	UxPutBottomWidget( rowColumn11, "separator1" );
	UxPutBottomOffset( rowColumn11, 5 );
	UxPutBottomAttachment( rowColumn11, "attach_widget" );
	UxPutTopOffset( rowColumn11, 10 );
	UxPutTopAttachment( rowColumn11, "attach_form" );
	UxPutLeftOffset( rowColumn11, 5 );
	UxPutLeftAttachment( rowColumn11, "attach_form" );
	UxCreateWidget( rowColumn11 );

	UxCreateWidget( label7 );
	UxCreateWidget( rowColumn13 );
	UxCreateWidget( toggleButton5 );
	UxCreateWidget( pb_main_search4 );
	UxCreateWidget( rowColumn14 );
	UxCreateWidget( toggleButton6 );
	UxCreateWidget( pb_main_search5 );
	UxCreateWidget( rowColumn15 );
	UxCreateWidget( toggleButton7 );
	UxCreateWidget( pb_main_search6 );
	UxCreateWidget( rowColumn16 );
	UxCreateWidget( toggleButton8 );
	UxCreateWidget( pb_main_search7 );
	UxCreateWidget( rowColumn17 );
	UxCreateWidget( toggleButton9 );
	UxCreateWidget( menu2_p7 );
	UxCreateWidget( mn_tol_angstroms7 );
	UxCreateWidget( mn_tol_pixels7 );
	UxCreateWidget( menu2_p7_b3 );
	UxCreateWidget( menu2_p7_b4 );
	UxCreateWidget( menu2_p7_b5 );
	UxCreateWidget( menu2_p7_b6 );
	UxPutSubMenuId( mn_tol7, "menu2_p7" );
	UxCreateWidget( mn_tol7 );

	UxCreateWidget( label1 );
	UxCreateWidget( rowColumn18 );
	UxCreateWidget( toggleButton3 );
	UxCreateWidget( pb_main_search2 );
	UxCreateWidget( rowColumn19 );
	UxCreateWidget( toggleButton10 );
	UxCreateWidget( pb_main_search9 );
	UxCreateWidget( rowColumn20 );
	UxCreateWidget( toggleButton11 );
	UxCreateWidget( pb_main_search10 );
	UxCreateWidget( rowColumn21 );
	UxCreateWidget( toggleButton12 );
	UxCreateWidget( pb_main_search11 );
	UxCreateWidget( rowColumn9 );
	UxCreateWidget( toggleButton1 );
	UxCreateWidget( pb_main_search1 );
	UxCreateWidget( rowColumn10 );
	UxCreateWidget( toggleButton2 );
	UxCreateWidget( pb_main_search3 );
	UxCreateWidget( rowColumn12 );
	UxCreateWidget( toggleButton4 );
	UxCreateWidget( pb_main_search12 );
	UxCreateWidget( rowColumn22 );
	UxCreateWidget( toggleButton13 );
	UxCreateWidget( pb_main_search13 );

	UxPutMenuHelpWidget( menu1, "menu1_top_b5" );

	UxAddCallback( menu1_p1_b1, XmNactivateCallback,
			activateCB_menu1_p1_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b2, XmNactivateCallback,
			activateCB_menu1_p1_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b3, XmNactivateCallback,
			activateCB_menu1_p1_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b6, XmNactivateCallback,
			activateCB_menu1_p1_b6,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b7, XmNactivateCallback,
			activateCB_menu1_p1_b7,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p1_b4, XmNactivateCallback,
			activateCB_menu1_p1_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b1, XmNactivateCallback,
			activateCB_menu1_p4_b1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b2, XmNactivateCallback,
			activateCB_menu1_p4_b2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b10, XmNactivateCallback,
			activateCB_menu1_p4_b10,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b13, XmNactivateCallback,
			activateCB_menu1_p4_b13,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b3, XmNactivateCallback,
			activateCB_menu1_p4_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b4, XmNactivateCallback,
			activateCB_menu1_p4_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b5, XmNactivateCallback,
			activateCB_menu1_p4_b5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b11, XmNactivateCallback,
			activateCB_menu1_p4_b11,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu1_p4_b15, XmNactivateCallback,
			activateCB_menu1_p4_b15,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_context, XmNactivateCallback,
			activateCB_menu_help_context,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_help, XmNactivateCallback,
			activateCB_menu_help_help,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_tutorial, XmNactivateCallback,
			activateCB_menu_help_tutorial,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu_help_version, XmNactivateCallback,
			activateCB_menu_help_version,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_batch, XmNactivateCallback,
			activateCB_pb_main_batch,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_ident, XmNactivateCallback,
			activateCB_pb_main_ident,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_batch1, XmNactivateCallback,
			activateCB_pb_main_batch1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_batch2, XmNactivateCallback,
			activateCB_pb_main_batch2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_batch3, XmNactivateCallback,
			activateCB_pb_main_batch3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_wrang1, XmNlosingFocusCallback,
			losingFocusCB_tf_wrang1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( tf_wrang2, XmNlosingFocusCallback,
			losingFocusCB_tf_wrang2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_batch4, XmNactivateCallback,
			activateCB_pb_main_batch4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search4, XmNactivateCallback,
			activateCB_pb_main_search4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search5, XmNactivateCallback,
			activateCB_pb_main_search5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search6, XmNactivateCallback,
			activateCB_pb_main_search6,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search7, XmNactivateCallback,
			activateCB_pb_main_search7,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( mn_tol_angstroms7, XmNactivateCallback,
			activateCB_mn_tol_angstroms7,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( mn_tol_pixels7, XmNactivateCallback,
			activateCB_mn_tol_pixels7,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu2_p7_b3, XmNactivateCallback,
			activateCB_menu2_p7_b3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu2_p7_b4, XmNactivateCallback,
			activateCB_menu2_p7_b4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu2_p7_b5, XmNactivateCallback,
			activateCB_menu2_p7_b5,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( menu2_p7_b6, XmNactivateCallback,
			activateCB_menu2_p7_b6,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton3, XmNarmCallback,
			armCB_toggleButton3,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( toggleButton3, XmNvalueChangedCallback,
			valueChangedCB_toggleButton3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search2, XmNactivateCallback,
			activateCB_pb_main_search2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton10, XmNarmCallback,
			armCB_toggleButton10,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( toggleButton10, XmNvalueChangedCallback,
			valueChangedCB_toggleButton10,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search9, XmNactivateCallback,
			activateCB_pb_main_search9,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton11, XmNarmCallback,
			armCB_toggleButton11,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( toggleButton11, XmNvalueChangedCallback,
			valueChangedCB_toggleButton11,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search10, XmNactivateCallback,
			activateCB_pb_main_search10,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton12, XmNarmCallback,
			armCB_toggleButton12,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( toggleButton12, XmNvalueChangedCallback,
			valueChangedCB_toggleButton12,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search11, XmNactivateCallback,
			activateCB_pb_main_search11,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton1, XmNarmCallback,
			armCB_toggleButton1,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( toggleButton1, XmNvalueChangedCallback,
			valueChangedCB_toggleButton1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search1, XmNactivateCallback,
			activateCB_pb_main_search1,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton2, XmNarmCallback,
			armCB_toggleButton2,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( toggleButton2, XmNvalueChangedCallback,
			valueChangedCB_toggleButton2,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search3, XmNactivateCallback,
			activateCB_pb_main_search3,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton4, XmNarmCallback,
			armCB_toggleButton4,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( toggleButton4, XmNvalueChangedCallback,
			valueChangedCB_toggleButton4,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search12, XmNactivateCallback,
			activateCB_pb_main_search12,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( toggleButton13, XmNarmCallback,
			armCB_toggleButton13,
			(XtPointer) UxApplicWindowContext );
	UxAddCallback( toggleButton13, XmNvalueChangedCallback,
			valueChangedCB_toggleButton13,
			(XtPointer) UxApplicWindowContext );

	UxAddCallback( pb_main_search13, XmNactivateCallback,
			activateCB_pb_main_search13,
			(XtPointer) UxApplicWindowContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( ApplicWindow );

	UxMainWindowSetAreas( MainWindow, menu1, NULL_SWIDGET,
			NULL_SWIDGET, NULL_SWIDGET, form1 );
	return ( ApplicWindow );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_ApplicWindow()
{
	swidget                 rtrn;
	_UxCApplicWindow        *UxContext;

	UxApplicWindowContext = UxContext =
		(_UxCApplicWindow *) UxMalloc( sizeof(_UxCApplicWindow) );

	{
		extern swidget create_SearchShell();
		extern swidget create_CalibShell();
		extern swidget create_RebinShell();
		extern swidget create_file_dialog();
		extern swidget create_extin_dialog();
		extern swidget create_resid_dialog();
		extern swidget create_HelpShell();
		extern swidget create_OrderShell();
		extern swidget create_RotateShell();
		extern swidget create_BackgroundShell();
		extern swidget create_ExtractionShell();
		extern swidget create_SkyShell();
		extern swidget create_FlatShell();
		extern swidget create_RespShell();
		extern swidget create_MergeShell();
		rtrn = _Uxbuild_ApplicWindow();

		create_SearchShell();
		create_CalibShell();
		create_RebinShell();
		create_file_dialog();
		create_extin_dialog();
		create_resid_dialog();
		create_HelpShell();
		create_OrderShell();
		create_RotateShell();
		create_BackgroundShell();
		create_ExtractionShell();
		create_SkyShell();
		create_FlatShell();
		create_RespShell();
		create_MergeShell();
		
		return(rtrn);
	}
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_ApplicWindow()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "WriteHelp", action_WriteHelp },
				{ "ClearHelp", action_ClearHelp },
				{ "UpdateDirectory", action_UpdateDirectory },
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_ApplicWindow();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

