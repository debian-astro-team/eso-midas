/*===========================================================================
  Copyright (C) 1993-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* .IDENT       midasutil.c                                */
/* .AUTHORS     Pascal Ballester (ESO/Garching)            */
/*              Cristian Levin   (ESO/La Silla)            */
/* .KEYWORDS    XLong, Spectroscopy, Long-Slit             */
/* .PURPOSE                                                */
/* .VERSION     1.0  Package Creation  17-MAR-1993   

    090825	last modif				   */
/* ------------------------------------------------------- */

#include <gl_defs.h>
#include <xm_defs.h>
#include <main_defs.h>
#include <spec_comm.h>
#include <UxLib.h>

#include <midas_def.h>


#define HELP_FILE	"gui/XEchelle/incl/help.h"
#define HELP_DELIMITER	'~'

static char help_text[4000]; /* Max of 50 lines */

void PopupLong( interface )
char *interface;
{
    /*
    UxPopdownInterface(UxFindSwidget("SearchShell"));
    UxPopdownInterface(UxFindSwidget("CalibShell"));
    UxPopdownInterface(UxFindSwidget("RebinShell"));
    UxPopdownInterface(UxFindSwidget("ExtractShell"));
    UxPopdownInterface(UxFindSwidget("FluxShell"));
    */

    UxPopupInterface(UxFindSwidget(interface), no_grab);
}

void DisplayShortHelp( sw )
Widget sw; 	/* current widget */
{
    char s[256], s_help[32];
 
    s[0] = '\0';

    if ( sw == UxGetWidget(UxFindSwidget("tf_session")) ) {
      	strcpy(s,"Session name. The session can be saved using the \"File\" pulldown\nmenu");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_wlc")) ) {
      	strcpy(s,"Wavelength calibration frame, assigned in the search line process");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_instrume")) ) {
      	strcpy(s,"Instrument associated to the calibration frame");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_ystart")) ) {
      	strcpy(s,"Starting row for the calibration (pixel value)");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
      	strcpy(s,"Line catalogue table");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_wrang1")) ) {
      	strcpy(s,"Lower wavelength limit to be considered in the line catalogue");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_wrang2")) ) {
      	strcpy(s,"Upper wavelength limit to be considered in the line catalogue");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_imin")) ) {
      	strcpy(s,"Minimum strength (intensity) limit of the line catalogue");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_search")) ) {
      	strcpy(s,"Open the search line interface");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_ident")) ) {
      	strcpy(s,"Open the line identification interface");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_calib")) ) {
      	strcpy(s,"Open the wavelength calibration interface");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_rebin")) ) {
      	strcpy(s,"Open the rebinning interface");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_extract")) ) {
      	strcpy(s,"Open the spectrum extraction interface");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_flux")) ) {
      	strcpy(s,"Open the flux calibration interface");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_batch")) ) {
      	strcpy(s,"Open the batch reduction form");
	strcpy(s_help, "shelp_main");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_thres")) ) {
      	strcpy(s,"Threshold for for line detection (above local median)");
	strcpy(s_help, "shelp_search");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_width")) ) {
      	strcpy(s,"Window size in X for line detection (pixels)");
	strcpy(s_help, "shelp_search");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_ywidth")) ) {
      	strcpy(s,"Window size in Y for line detection (pixels)");
	strcpy(s_help, "shelp_search");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_ystep")) ) {
      	strcpy(s,"Step in Y for line searching (pixels)");
	strcpy(s_help, "shelp_search");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_search_search")) ) {
      	strcpy(s,"Do the search");
	strcpy(s_help, "shelp_search");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_search_plot")) ) {
      	strcpy(s,"Plot the lines found");
	strcpy(s_help, "shelp_search");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_tol")) ) {
      	strcpy(s,"tolerance in Angstroms for wavelength ident.");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_dcx1")) ) {
      	strcpy(s,"fit degree of the dispersion coeff. in X");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_dcx2")) ) {
      	strcpy(s,"fit degree of the dispersion coeff. in Y");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_wlcniter1")) ) {
      	strcpy(s,"Minimum number of iterations");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_wlcniter2")) ) {
      	strcpy(s,"Maximum number of iterations");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_alpha")) ) {
      	strcpy(s,"Rejection parameter for lines matching");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_maxdev")) ) {
      	strcpy(s,"Maximum deviation (pixels)");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_guess")) ) {
      	strcpy(s,"Guess session name");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_calibrate")) ) {
      	strcpy(s,"Do the calibration in the starting line");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_edit")) ) {
      	strcpy(s,"Edit the results in the dispersion or spectrum plot");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_getcur")) ) {
      	strcpy(s,"Get the cursor value (wavelength units) in the spectrum plot ");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_all")) ) {
      	strcpy(s,"Do the calibration in the whole calibration frame");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_twice")) ) {
      	strcpy(s,"Do the calibration in the whole calibration frame. It forces to find the same\nnumber of lines on each row");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_disper")) ) {
      	strcpy(s,"Plot the dispersion (delta(wave) / wave)");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_resid")) ) {
      	strcpy(s,"Plot the residuals (specific row or the whole frame)");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_spec")) ) {
      	strcpy(s,"Plot the starting line with the lines found");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_shape")) ) {
      	strcpy(s,"Plot the line shape or distortion associated to a wavelength value");
	strcpy(s_help, "shelp_calib");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_rebstrt")) ) {
      	strcpy(s,"Starting wavelength for rebinning");
	strcpy(s_help, "shelp_rebin");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_rebend")) ) {
      	strcpy(s,"Final wavelength for rebinning");
	strcpy(s_help, "shelp_rebin");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_rebstp")) ) {
      	strcpy(s,"Wavelength step for rebinning");
	strcpy(s_help, "shelp_rebin");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_rebin_rbr")) ) {
      	strcpy(s,"Do the rebinning row by row");
	strcpy(s_help, "shelp_rebin");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_rebin_2d")) ) {
      	strcpy(s,"Do the rebinning using a 2-D solution. The output is a frame");
	strcpy(s_help, "shelp_rebin");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_rebin_table")) ) {
      	strcpy(s,"Do the rebinning using a 2-D solution. The output is a table");
	strcpy(s_help, "shelp_rebin");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_rebin_plot")) ) {
      	strcpy(s,"Plot the result of the \"rebin table\" command");
	strcpy(s_help, "shelp_rebin");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_extin_tbl")) ) {
      	strcpy(s,"Extinction Table");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
      	strcpy(s,"Flux Table of the standard star");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_output_resp")) ) {
      	strcpy(s,"Response Image");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_fitd")) ) {
      	strcpy(s,"Degree of fit");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_smooth")) ) {
      	strcpy(s,"Smoothing factor for spline fitting");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_extin")) ) {
      	strcpy(s,"Do the extinction correction");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_integr")) ) {
      	strcpy(s,"Do the integration");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_fit")) ) {
      	strcpy(s,"Do the fitting, generating the response curve");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_edit_flux")) ) {
      	strcpy(s,"Edit the table generated by the integration process");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_correct")) ) {
      	strcpy(s,"Calibrate the input image in flux, using the response curve generated");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_filter")) ) {
      	strcpy(s,"Generates the response curve by filtering");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_pflux")) ) {
      	strcpy(s,"Plot the Flux Table");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_presp")) ) {
      	strcpy(s,"Plot the response curve");
	strcpy(s_help, "shelp_flux");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_lowsky1")) ) {
      	strcpy(s,"Lower row number of lower sky");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_lowsky2")) ) {
      	strcpy(s,"Upper row number of lower sky");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_uppsky1")) ) {
      	strcpy(s,"Lower row number of upper sky");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_uppsky2")) ) {
      	strcpy(s,"Upper row number of upper sky");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_object1")) ) {
      	strcpy(s," Lower row number of object spectrum");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_object2")) ) {
      	strcpy(s," Upper row number of object spectrum");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_skyord")) ) {
      	strcpy(s,"Order for sky fit");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_order")) ) {
      	strcpy(s,"Order for optimal extraction");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_niter")) ) {
      	strcpy(s,"Number of iterations");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_ron")) ) {
      	strcpy(s,"Read-Out-Noise (ADU)");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_gain")) ) {
      	strcpy(s,"Gain  (e-/ADU)");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_sigma")) ) {
      	strcpy(s,"Threshold for rejection of cosmics (std dev.)");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_radius")) ) {
      	strcpy(s,"Radius for cosmics rejection");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("tf_sky")) ) {
      	strcpy(s,"Intermediate sky frame name or constant value");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_sky")) ) {
      	strcpy(s,"Extract the sky coordinates from the image displayed (you have to enter\n4 points)");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_object")) ) {
      	strcpy(s,"Extract the object coordinates from the image displayed (you have to\nenter 2 points)");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_fit")) ) {
      	strcpy(s,"Generate the sky frame");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_average")) ) {
      	strcpy(s,"Extract the spectrum by averaging");
	strcpy(s_help, "shelp_extract");
    }
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_weight")) ) {
      	strcpy(s,"Extract the spectrum by weighting");
	strcpy(s_help, "shelp_extract");
    }

    if ( s[0] != '\0' )
    	UxPutText(UxFindSwidget(s_help), s);
}

void DisplayExtendedHelp( sw )
Widget sw; 	/* current widget */
{
    char s[4000];
    char *find_extended_help();
 
    s[0] = '\0';

    if ( sw == UxGetWidget(UxFindSwidget("menu_help_context")) )
      	strcpy(s, find_extended_help("HELP_CONTEXT"));
    else if ( sw == UxGetWidget(UxFindSwidget("menu_help_help")) )
      	strcpy(s, find_extended_help("HELP_HELP"));
    else if ( sw == UxGetWidget(UxFindSwidget("menu_help_tutorial")) )
      	strcpy(s, find_extended_help("HELP_TUTORIAL"));
    else if ( sw == UxGetWidget(UxFindSwidget("menu_help_version")) )
      	strcpy(s, find_extended_help("HELP_VERSION"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_search")) )
      	strcpy(s, find_extended_help("MAIN_SEARCH"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_ident")) )
      	strcpy(s, find_extended_help("MAIN_IDENT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_calib")) )
      	strcpy(s, find_extended_help("MAIN_CALIB"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_rebin")) )
      	strcpy(s, find_extended_help("MAIN_REBIN"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_extract")) )
      	strcpy(s, find_extended_help("MAIN_EXTRACT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_flux")) )
      	strcpy(s, find_extended_help("MAIN_FLUX"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_main_batch")) )
      	strcpy(s, find_extended_help("MAIN_BATCH"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_search_search")) )
      	strcpy(s, find_extended_help("SEARCH_SEARCH"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_search_plot")) )
      	strcpy(s, find_extended_help("SEARCH_PLOT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_calibrate")) )
      	strcpy(s, find_extended_help("CALIB_CALIBRATE"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_edit")) )
      	strcpy(s, find_extended_help("CALIB_EDIT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_getcur")) )
      	strcpy(s, find_extended_help("CALIB_GETCUR"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_all")) )
      	strcpy(s, find_extended_help("CALIB_ALL"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_twice")) )
      	strcpy(s, find_extended_help("CALIB_TWICE"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_disper")) )
      	strcpy(s, find_extended_help("CALIB_DISPER"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_resid")) )
      	strcpy(s, find_extended_help("CALIB_RESID"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_spec")) )
      	strcpy(s, find_extended_help("CALIB_SPEC"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_calib_shape")) )
      	strcpy(s, find_extended_help("CALIB_SHAPE"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_rebin_rbr")) )
      	strcpy(s, find_extended_help("REBIN_RBR"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_rebin_2d")) )
      	strcpy(s, find_extended_help("REBIN_2D"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_rebin_table")) )
      	strcpy(s, find_extended_help("REBIN_TABLE"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_rebin_plot")) )
      	strcpy(s, find_extended_help("REBIN_PLOT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_sky")) )
      	strcpy(s, find_extended_help("EXTRACT_SKY"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_object")) )
      	strcpy(s, find_extended_help("EXTRACT_OBJECT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_fit")) )
      	strcpy(s, find_extended_help("EXTRACT_FIT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_average")) )
      	strcpy(s, find_extended_help("EXTRACT_AVERAGE"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_ext_weight")) )
      	strcpy(s, find_extended_help("EXTRACT_WEIGHT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_extin")) )
      	strcpy(s, find_extended_help("FLUX_EXTIN"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_integr")) )
      	strcpy(s, find_extended_help("FLUX_INTEGR"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_fit")) )
      	strcpy(s, find_extended_help("FLUX_FIT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_edit_flux")) )
      	strcpy(s, find_extended_help("EDIT_FLUX"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_filter")) )
      	strcpy(s, find_extended_help("FLUX_FILTER"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_correct")) )
      	strcpy(s, find_extended_help("FLUX_CORRECT"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_pflux")) )
      	strcpy(s, find_extended_help("FLUX_PFLUX"));
    else if ( sw == UxGetWidget(UxFindSwidget("pb_flux_presp")) )
      	strcpy(s, find_extended_help("FLUX_PRESP"));

    if ( s[0] != '\0' ) {
    	UxPopupInterface(UxFindSwidget("HelpShell"), no_grab);
    	UxPutText(UxFindSwidget("tx_extended_help"), s);
    }
}

char *find_extended_help( key )
char *key;
{
    char midashome[512], midvers[80], help_pathname[512];
    char c, h, s[MAXLINE];
    FILE *fp;
    int idx = 0;

    osfphname("MIDVERS", midvers);
    osfphname("MIDASHOME", midashome);
    sprintf( help_pathname, "%s/%s/%s", midashome, midvers, HELP_FILE );
    if ( (fp = fopen(help_pathname, "r")) == NULL ) {
        sprintf(s, "XLong Help file [%s] not found.\r\n", help_pathname);
	SCTPUT(s);
        return("");
    }

    while ( (c = getc(fp)) != EOF )
	if ( c == HELP_DELIMITER && 
             !strncmp(key, fgets(s, MAXLINE, fp), strlen(key)) ) {
    	    while ( (h = getc(fp)) != EOF && h != HELP_DELIMITER )
		help_text[idx++] = h;
	    help_text[idx] = '\0';
            fclose(fp);
	    return(help_text);
	}
    fclose(fp);
    return("");

}
