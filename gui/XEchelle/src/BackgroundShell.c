/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	BackgroundShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxRowCol.h"
#include "UxTogB.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTranSh.h"

#include "proto_xech.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxBackgroundShell;
	swidget	Uxform29;
	swidget	Uxform30;
	swidget	UxpushButton4;
	swidget	Uxpb_search_plot1;
	swidget	Uxpb_search_search6;
	swidget	Uxpb_search_plot9;
	swidget	Uxpb_search_plot10;
	swidget	Uxpb_search_plot12;
	swidget	Uxshelp_search4;
	swidget	Uxseparator24;
	swidget	Uxseparator25;
	swidget	Uxlabel69;
	swidget	Uxtf_alpha8;
	swidget	Uxtf_alpha9;
	swidget	Uxlabel70;
	swidget	Uxtf_alpha10;
	swidget	Uxtf_alpha12;
	swidget	Uxtf_alpha13;
	swidget	Uxlabel71;
	swidget	Uxlabel72;
	swidget	Uxtf_alpha14;
	swidget	Uxlabel74;
	swidget	Uxlabel75;
	swidget	Uxlabel77;
	swidget	Uxlabel78;
	swidget	UxtoggleButton17;
	swidget	UxrowColumn1;
	swidget	Uxrb_seamtd_gaus1;
	swidget	Uxrb_seamtd_grav1;
	swidget	Uxrb_seamtd_maxi1;
	swidget	Uxlabel79;
	swidget	Uxlabel80;
	swidget	Uxtf_alpha15;
	swidget	Uxtf_alpha16;
	swidget	Uxlabel81;
	swidget	Uxtf_alpha17;
	swidget	Uxtf_alpha18;
	swidget	Uxtf_alpha19;
	swidget	Uxlabel82;
	swidget	Uxpb_main_search17;
	swidget	Uxpb_main_search18;
	swidget	Uxlabel83;
	swidget	Uxlabel84;
	swidget	Uxtf_alpha20;
	swidget	Uxtf_alpha21;
	swidget	Uxlabel85;
	swidget	Uxlabel86;
	swidget	Uxlabel87;
	swidget	Uxpb_search_plot11;
	swidget	Uxpb_search_plot15;
	swidget	Uxpb_search_plot16;
	swidget	Uxpb_main_search8;
} _UxCBackgroundShell;

#define BackgroundShell         UxBackgroundShellContext->UxBackgroundShell
#define form29                  UxBackgroundShellContext->Uxform29
#define form30                  UxBackgroundShellContext->Uxform30
#define pushButton4             UxBackgroundShellContext->UxpushButton4
#define pb_search_plot1         UxBackgroundShellContext->Uxpb_search_plot1
#define pb_search_search6       UxBackgroundShellContext->Uxpb_search_search6
#define pb_search_plot9         UxBackgroundShellContext->Uxpb_search_plot9
#define pb_search_plot10        UxBackgroundShellContext->Uxpb_search_plot10
#define pb_search_plot12        UxBackgroundShellContext->Uxpb_search_plot12
#define shelp_search4           UxBackgroundShellContext->Uxshelp_search4
#define separator24             UxBackgroundShellContext->Uxseparator24
#define separator25             UxBackgroundShellContext->Uxseparator25
#define label69                 UxBackgroundShellContext->Uxlabel69
#define tf_alpha8               UxBackgroundShellContext->Uxtf_alpha8
#define tf_alpha9               UxBackgroundShellContext->Uxtf_alpha9
#define label70                 UxBackgroundShellContext->Uxlabel70
#define tf_alpha10              UxBackgroundShellContext->Uxtf_alpha10
#define tf_alpha12              UxBackgroundShellContext->Uxtf_alpha12
#define tf_alpha13              UxBackgroundShellContext->Uxtf_alpha13
#define label71                 UxBackgroundShellContext->Uxlabel71
#define label72                 UxBackgroundShellContext->Uxlabel72
#define tf_alpha14              UxBackgroundShellContext->Uxtf_alpha14
#define label74                 UxBackgroundShellContext->Uxlabel74
#define label75                 UxBackgroundShellContext->Uxlabel75
#define label77                 UxBackgroundShellContext->Uxlabel77
#define label78                 UxBackgroundShellContext->Uxlabel78
#define toggleButton17          UxBackgroundShellContext->UxtoggleButton17
#define rowColumn1              UxBackgroundShellContext->UxrowColumn1
#define rb_seamtd_gaus1         UxBackgroundShellContext->Uxrb_seamtd_gaus1
#define rb_seamtd_grav1         UxBackgroundShellContext->Uxrb_seamtd_grav1
#define rb_seamtd_maxi1         UxBackgroundShellContext->Uxrb_seamtd_maxi1
#define label79                 UxBackgroundShellContext->Uxlabel79
#define label80                 UxBackgroundShellContext->Uxlabel80
#define tf_alpha15              UxBackgroundShellContext->Uxtf_alpha15
#define tf_alpha16              UxBackgroundShellContext->Uxtf_alpha16
#define label81                 UxBackgroundShellContext->Uxlabel81
#define tf_alpha17              UxBackgroundShellContext->Uxtf_alpha17
#define tf_alpha18              UxBackgroundShellContext->Uxtf_alpha18
#define tf_alpha19              UxBackgroundShellContext->Uxtf_alpha19
#define label82                 UxBackgroundShellContext->Uxlabel82
#define pb_main_search17        UxBackgroundShellContext->Uxpb_main_search17
#define pb_main_search18        UxBackgroundShellContext->Uxpb_main_search18
#define label83                 UxBackgroundShellContext->Uxlabel83
#define label84                 UxBackgroundShellContext->Uxlabel84
#define tf_alpha20              UxBackgroundShellContext->Uxtf_alpha20
#define tf_alpha21              UxBackgroundShellContext->Uxtf_alpha21
#define label85                 UxBackgroundShellContext->Uxlabel85
#define label86                 UxBackgroundShellContext->Uxlabel86
#define label87                 UxBackgroundShellContext->Uxlabel87
#define pb_search_plot11        UxBackgroundShellContext->Uxpb_search_plot11
#define pb_search_plot15        UxBackgroundShellContext->Uxpb_search_plot15
#define pb_search_plot16        UxBackgroundShellContext->Uxpb_search_plot16
#define pb_main_search8         UxBackgroundShellContext->Uxpb_main_search8

static _UxCBackgroundShell	*UxBackgroundShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();
extern void  WriteKeyword(), GetExtendedHelp(), SelectList();


extern int RadioSet(), PopupList(), AppendDialogText();

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_BackgroundShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_UpdateDirectory( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	 char node[80];
	
	 /* if (XmTextGetSelection(UxWidget) == NULL) return; */
	 strcpy(node,
	     XmTextGetSelection(UxWidget));
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	
	node[strlen(node)-1] = '\0';
	WGet_all_dirs(node);
	WChange_Midas_dir();
	WGet_all_files();
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pushButton4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("BackgroundShell"));
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_search6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	 MidasCommand(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	 MidasCommand(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha9( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha12( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha13( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	armCB_toggleButton17( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxBackgroundShellContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton17( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_gaus1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxBackgroundShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_gaus1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_grav1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxBackgroundShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_grav1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_maxi1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	RadioSet(UxWidget);
	UxBackgroundShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_maxi1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha17( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha19( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search17( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search18( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha20( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha21( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	 MidasCommand(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot16( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search8( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCBackgroundShell     *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxBackgroundShellContext;
	UxBackgroundShellContext = UxContext =
			(_UxCBackgroundShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxBackgroundShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_BackgroundShell()
{
	UxPutTitle( BackgroundShell, "XEchelle: Interorder Background" );
	UxPutGeometry( BackgroundShell, "+10+60" );
	UxPutHeight( BackgroundShell, 505 );
	UxPutWidth( BackgroundShell, 520 );
	UxPutY( BackgroundShell, 466 );
	UxPutX( BackgroundShell, 604 );

	UxPutBackground( form29, WindowBackground );
	UxPutHeight( form29, 348 );
	UxPutWidth( form29, 408 );
	UxPutY( form29, -2 );
	UxPutX( form29, 0 );
	UxPutUnitType( form29, "pixels" );
	UxPutResizePolicy( form29, "resize_none" );

	UxPutBackground( form30, ButtonBackground );
	UxPutHeight( form30, 40 );
	UxPutWidth( form30, 508 );
	UxPutY( form30, 460 );
	UxPutX( form30, 6 );
	UxPutResizePolicy( form30, "resize_none" );

	UxPutLabelString( pushButton4, "Cancel" );
	UxPutForeground( pushButton4, CancelForeground );
	UxPutFontList( pushButton4, BoldTextFont );
	UxPutBackground( pushButton4, ButtonBackground );
	UxPutHeight( pushButton4, 30 );
	UxPutWidth( pushButton4, 80 );
	UxPutY( pushButton4, 4 );
	UxPutX( pushButton4, 422 );

	UxPutLabelString( pb_search_plot1, "Help..." );
	UxPutForeground( pb_search_plot1, ButtonForeground );
	UxPutFontList( pb_search_plot1, BoldTextFont );
	UxPutBackground( pb_search_plot1, ButtonBackground );
	UxPutHeight( pb_search_plot1, 30 );
	UxPutWidth( pb_search_plot1, 80 );
	UxPutY( pb_search_plot1, 4 );
	UxPutX( pb_search_plot1, 340 );

	UxPutLabelString( pb_search_search6, "Subtract" );
	UxPutForeground( pb_search_search6, ApplyForeground );
	UxPutFontList( pb_search_search6, BoldTextFont );
	UxPutBackground( pb_search_search6, ButtonBackground );
	UxPutHeight( pb_search_search6, 30 );
	UxPutWidth( pb_search_search6, 80 );
	UxPutY( pb_search_search6, 4 );
	UxPutX( pb_search_search6, 8 );

	UxPutLabelString( pb_search_plot9, "Positions" );
	UxPutForeground( pb_search_plot9, ButtonForeground );
	UxPutFontList( pb_search_plot9, BoldTextFont );
	UxPutBackground( pb_search_plot9, ButtonBackground );
	UxPutHeight( pb_search_plot9, 30 );
	UxPutWidth( pb_search_plot9, 80 );
	UxPutY( pb_search_plot9, 4 );
	UxPutX( pb_search_plot9, 92 );

	UxPutLabelString( pb_search_plot10, "Select" );
	UxPutForeground( pb_search_plot10, ButtonForeground );
	UxPutFontList( pb_search_plot10, BoldTextFont );
	UxPutBackground( pb_search_plot10, ButtonBackground );
	UxPutHeight( pb_search_plot10, 30 );
	UxPutWidth( pb_search_plot10, 80 );
	UxPutY( pb_search_plot10, 4 );
	UxPutX( pb_search_plot10, 176 );

	UxPutLabelString( pb_search_plot12, "Plot" );
	UxPutForeground( pb_search_plot12, ButtonForeground );
	UxPutFontList( pb_search_plot12, BoldTextFont );
	UxPutBackground( pb_search_plot12, ButtonBackground );
	UxPutHeight( pb_search_plot12, 30 );
	UxPutWidth( pb_search_plot12, 80 );
	UxPutY( pb_search_plot12, 4 );
	UxPutX( pb_search_plot12, 258 );

	UxPutFontList( shelp_search4, TextFont );
	UxPutEditable( shelp_search4, "false" );
	UxPutCursorPositionVisible( shelp_search4, "false" );
	UxPutBackground( shelp_search4, SHelpBackground );
	UxPutHeight( shelp_search4, 50 );
	UxPutWidth( shelp_search4, 508 );
	UxPutY( shelp_search4, 402 );
	UxPutX( shelp_search4, 4 );

	UxPutBackground( separator24, WindowBackground );
	UxPutHeight( separator24, 10 );
	UxPutWidth( separator24, 514 );
	UxPutY( separator24, 452 );
	UxPutX( separator24, 4 );

	UxPutBackground( separator25, WindowBackground );
	UxPutHeight( separator25, 10 );
	UxPutWidth( separator25, 518 );
	UxPutY( separator25, 388 );
	UxPutX( separator25, 2 );

	UxPutForeground( label69, TextForeground );
	UxPutAlignment( label69, "alignment_beginning" );
	UxPutLabelString( label69, "Radius (pixel):" );
	UxPutFontList( label69, TextFont );
	UxPutBackground( label69, LabelBackground );
	UxPutHeight( label69, 30 );
	UxPutWidth( label69, 100 );
	UxPutY( label69, 342 );
	UxPutX( label69, 16 );

	UxPutForeground( tf_alpha8, TextForeground );
	UxPutHighlightOnEnter( tf_alpha8, "true" );
	UxPutFontList( tf_alpha8, TextFont );
	UxPutBackground( tf_alpha8, TextBackground );
	UxPutHeight( tf_alpha8, 32 );
	UxPutWidth( tf_alpha8, 96 );
	UxPutY( tf_alpha8, 296 );
	UxPutX( tf_alpha8, 370 );

	UxPutForeground( tf_alpha9, TextForeground );
	UxPutHighlightOnEnter( tf_alpha9, "true" );
	UxPutFontList( tf_alpha9, TextFont );
	UxPutBackground( tf_alpha9, TextBackground );
	UxPutHeight( tf_alpha9, 32 );
	UxPutWidth( tf_alpha9, 96 );
	UxPutY( tf_alpha9, 222 );
	UxPutX( tf_alpha9, 370 );

	UxPutForeground( label70, TextForeground );
	UxPutAlignment( label70, "alignment_beginning" );
	UxPutLabelString( label70, "Number of iterations:" );
	UxPutFontList( label70, TextFont );
	UxPutBackground( label70, LabelBackground );
	UxPutHeight( label70, 30 );
	UxPutWidth( label70, 140 );
	UxPutY( label70, 224 );
	UxPutX( label70, 222 );

	UxPutForeground( tf_alpha10, TextForeground );
	UxPutHighlightOnEnter( tf_alpha10, "true" );
	UxPutFontList( tf_alpha10, TextFont );
	UxPutBackground( tf_alpha10, TextBackground );
	UxPutHeight( tf_alpha10, 32 );
	UxPutWidth( tf_alpha10, 96 );
	UxPutY( tf_alpha10, 338 );
	UxPutX( tf_alpha10, 214 );

	UxPutForeground( tf_alpha12, TextForeground );
	UxPutHighlightOnEnter( tf_alpha12, "true" );
	UxPutFontList( tf_alpha12, TextFont );
	UxPutBackground( tf_alpha12, TextBackground );
	UxPutHeight( tf_alpha12, 32 );
	UxPutWidth( tf_alpha12, 96 );
	UxPutY( tf_alpha12, 338 );
	UxPutX( tf_alpha12, 368 );

	UxPutForeground( tf_alpha13, TextForeground );
	UxPutHighlightOnEnter( tf_alpha13, "true" );
	UxPutFontList( tf_alpha13, TextFont );
	UxPutBackground( tf_alpha13, TextBackground );
	UxPutHeight( tf_alpha13, 32 );
	UxPutWidth( tf_alpha13, 96 );
	UxPutY( tf_alpha13, 222 );
	UxPutX( tf_alpha13, 120 );

	UxPutForeground( label71, TextForeground );
	UxPutAlignment( label71, "alignment_beginning" );
	UxPutLabelString( label71, "Step (pixel):" );
	UxPutFontList( label71, TextFont );
	UxPutBackground( label71, LabelBackground );
	UxPutHeight( label71, 30 );
	UxPutWidth( label71, 94 );
	UxPutY( label71, 222 );
	UxPutX( label71, 14 );

	UxPutForeground( label72, TextForeground );
	UxPutAlignment( label72, "alignment_beginning" );
	UxPutLabelString( label72, "Polynomial Degree:" );
	UxPutFontList( label72, TextFont );
	UxPutBackground( label72, LabelBackground );
	UxPutHeight( label72, 30 );
	UxPutWidth( label72, 136 );
	UxPutY( label72, 302 );
	UxPutX( label72, 16 );

	UxPutForeground( tf_alpha14, TextForeground );
	UxPutHighlightOnEnter( tf_alpha14, "true" );
	UxPutFontList( tf_alpha14, TextFont );
	UxPutBackground( tf_alpha14, TextBackground );
	UxPutHeight( tf_alpha14, 32 );
	UxPutWidth( tf_alpha14, 96 );
	UxPutY( tf_alpha14, 298 );
	UxPutX( tf_alpha14, 216 );

	UxPutForeground( label74, TextForeground );
	UxPutAlignment( label74, "alignment_beginning" );
	UxPutLabelString( label74, "X=" );
	UxPutFontList( label74, TextFont );
	UxPutBackground( label74, LabelBackground );
	UxPutHeight( label74, 30 );
	UxPutWidth( label74, 36 );
	UxPutY( label74, 340 );
	UxPutX( label74, 166 );

	UxPutForeground( label75, TextForeground );
	UxPutAlignment( label75, "alignment_beginning" );
	UxPutLabelString( label75, "Y=" );
	UxPutFontList( label75, TextFont );
	UxPutBackground( label75, LabelBackground );
	UxPutHeight( label75, 30 );
	UxPutWidth( label75, 20 );
	UxPutY( label75, 296 );
	UxPutX( label75, 342 );

	UxPutForeground( label77, TextForeground );
	UxPutAlignment( label77, "alignment_beginning" );
	UxPutLabelString( label77, "Y=" );
	UxPutFontList( label77, TextFont );
	UxPutBackground( label77, LabelBackground );
	UxPutHeight( label77, 30 );
	UxPutWidth( label77, 20 );
	UxPutY( label77, 338 );
	UxPutX( label77, 336 );

	UxPutForeground( label78, TextForeground );
	UxPutAlignment( label78, "alignment_beginning" );
	UxPutLabelString( label78, "X=" );
	UxPutFontList( label78, TextFont );
	UxPutBackground( label78, LabelBackground );
	UxPutHeight( label78, 30 );
	UxPutWidth( label78, 34 );
	UxPutY( label78, 302 );
	UxPutX( label78, 166 );

	UxPutSelectColor( toggleButton17, "Yellow" );
	UxPutLabelString( toggleButton17, "Plot Background" );
	UxPutForeground( toggleButton17, TextForeground );
	UxPutFontList( toggleButton17, TextFont );
	UxPutBackground( toggleButton17, WindowBackground );
	UxPutRecomputeSize( toggleButton17, "false" );
	UxPutHeight( toggleButton17, 29 );
	UxPutWidth( toggleButton17, 136 );
	UxPutY( toggleButton17, 172 );
	UxPutX( toggleButton17, 12 );

	UxPutResizeWidth( rowColumn1, "true" );
	UxPutOrientation( rowColumn1, "horizontal" );
	UxPutIsAligned( rowColumn1, "true" );
	UxPutAdjustMargin( rowColumn1, "true" );
	UxPutAdjustLast( rowColumn1, "false" );
	UxPutEntryAlignment( rowColumn1, "alignment_beginning" );
	UxPutBorderWidth( rowColumn1, 0 );
	UxPutShadowThickness( rowColumn1, 0 );
	UxPutLabelString( rowColumn1, "" );
	UxPutEntryBorder( rowColumn1, 0 );
	UxPutBackground( rowColumn1, WindowBackground );
	UxPutRadioBehavior( rowColumn1, "true" );
	UxPutHeight( rowColumn1, 96 );
	UxPutWidth( rowColumn1, 106 );
	UxPutY( rowColumn1, 129 );
	UxPutX( rowColumn1, 88 );

	UxPutForeground( rb_seamtd_gaus1, TextForeground );
	UxPutIndicatorSize( rb_seamtd_gaus1, 16 );
	UxPutHighlightOnEnter( rb_seamtd_gaus1, "true" );
	UxPutSelectColor( rb_seamtd_gaus1, SelectColor );
	UxPutSet( rb_seamtd_gaus1, "true" );
	UxPutLabelString( rb_seamtd_gaus1, "Spline" );
	UxPutFontList( rb_seamtd_gaus1, TextFont );
	UxPutBackground( rb_seamtd_gaus1, WindowBackground );
	UxPutHeight( rb_seamtd_gaus1, 30 );
	UxPutWidth( rb_seamtd_gaus1, 148 );
	UxPutY( rb_seamtd_gaus1, 12 );
	UxPutX( rb_seamtd_gaus1, 12 );

	UxPutForeground( rb_seamtd_grav1, TextForeground );
	UxPutIndicatorSize( rb_seamtd_grav1, 16 );
	UxPutHighlightOnEnter( rb_seamtd_grav1, "true" );
	UxPutSelectColor( rb_seamtd_grav1, SelectColor );
	UxPutLabelString( rb_seamtd_grav1, "Polynomial  " );
	UxPutFontList( rb_seamtd_grav1, TextFont );
	UxPutBackground( rb_seamtd_grav1, WindowBackground );
	UxPutHeight( rb_seamtd_grav1, 28 );
	UxPutWidth( rb_seamtd_grav1, 108 );
	UxPutY( rb_seamtd_grav1, 3 );
	UxPutX( rb_seamtd_grav1, 136 );

	UxPutForeground( rb_seamtd_maxi1, TextForeground );
	UxPutIndicatorSize( rb_seamtd_maxi1, 16 );
	UxPutHighlightOnEnter( rb_seamtd_maxi1, "true" );
	UxPutSelectColor( rb_seamtd_maxi1, SelectColor );
	UxPutLabelString( rb_seamtd_maxi1, "Smooth" );
	UxPutFontList( rb_seamtd_maxi1, TextFont );
	UxPutBackground( rb_seamtd_maxi1, WindowBackground );
	UxPutHeight( rb_seamtd_maxi1, 24 );
	UxPutWidth( rb_seamtd_maxi1, 96 );
	UxPutY( rb_seamtd_maxi1, 66 );
	UxPutX( rb_seamtd_maxi1, 4 );

	UxPutForeground( label79, TextForeground );
	UxPutAlignment( label79, "alignment_beginning" );
	UxPutLabelString( label79, "Spline degree:" );
	UxPutFontList( label79, TextFont );
	UxPutBackground( label79, LabelBackground );
	UxPutHeight( label79, 30 );
	UxPutWidth( label79, 100 );
	UxPutY( label79, 262 );
	UxPutX( label79, 14 );

	UxPutForeground( label80, TextForeground );
	UxPutAlignment( label80, "alignment_beginning" );
	UxPutLabelString( label80, "Smoothing Factor:" );
	UxPutFontList( label80, TextFont );
	UxPutBackground( label80, LabelBackground );
	UxPutHeight( label80, 30 );
	UxPutWidth( label80, 126 );
	UxPutY( label80, 262 );
	UxPutX( label80, 224 );

	UxPutForeground( tf_alpha15, TextForeground );
	UxPutHighlightOnEnter( tf_alpha15, "true" );
	UxPutFontList( tf_alpha15, TextFont );
	UxPutBackground( tf_alpha15, TextBackground );
	UxPutHeight( tf_alpha15, 32 );
	UxPutWidth( tf_alpha15, 96 );
	UxPutY( tf_alpha15, 258 );
	UxPutX( tf_alpha15, 368 );

	UxPutForeground( tf_alpha16, TextForeground );
	UxPutHighlightOnEnter( tf_alpha16, "true" );
	UxPutFontList( tf_alpha16, TextFont );
	UxPutBackground( tf_alpha16, TextBackground );
	UxPutHeight( tf_alpha16, 32 );
	UxPutWidth( tf_alpha16, 96 );
	UxPutY( tf_alpha16, 264 );
	UxPutX( tf_alpha16, 120 );

	UxPutForeground( label81, TextForeground );
	UxPutAlignment( label81, "alignment_beginning" );
	UxPutLabelString( label81, "Method:" );
	UxPutFontList( label81, TextFont );
	UxPutBackground( label81, LabelBackground );
	UxPutHeight( label81, 30 );
	UxPutWidth( label81, 68 );
	UxPutY( label81, 131 );
	UxPutX( label81, 14 );

	UxPutForeground( tf_alpha17, TextForeground );
	UxPutHighlightOnEnter( tf_alpha17, "true" );
	UxPutFontList( tf_alpha17, TextFont );
	UxPutBackground( tf_alpha17, TextBackground );
	UxPutHeight( tf_alpha17, 32 );
	UxPutWidth( tf_alpha17, 236 );
	UxPutY( tf_alpha17, 8 );
	UxPutX( tf_alpha17, 126 );

	UxPutForeground( tf_alpha18, TextForeground );
	UxPutHighlightOnEnter( tf_alpha18, "true" );
	UxPutFontList( tf_alpha18, TextFont );
	UxPutBackground( tf_alpha18, TextBackground );
	UxPutHeight( tf_alpha18, 32 );
	UxPutWidth( tf_alpha18, 228 );
	UxPutY( tf_alpha18, 46 );
	UxPutX( tf_alpha18, 132 );

	UxPutForeground( tf_alpha19, TextForeground );
	UxPutHighlightOnEnter( tf_alpha19, "true" );
	UxPutFontList( tf_alpha19, TextFont );
	UxPutBackground( tf_alpha19, TextBackground );
	UxPutHeight( tf_alpha19, 32 );
	UxPutWidth( tf_alpha19, 230 );
	UxPutY( tf_alpha19, 90 );
	UxPutX( tf_alpha19, 128 );

	UxPutForeground( label82, TextForeground );
	UxPutAlignment( label82, "alignment_beginning" );
	UxPutLabelString( label82, "Input Frame" );
	UxPutFontList( label82, TextFont );
	UxPutBackground( label82, LabelBackground );
	UxPutHeight( label82, 30 );
	UxPutWidth( label82, 94 );
	UxPutY( label82, 10 );
	UxPutX( label82, 18 );

	UxPutWidth( pb_main_search17, 46 );
	UxPutRecomputeSize( pb_main_search17, "true" );
	UxPutLabelString( pb_main_search17, "..." );
	UxPutForeground( pb_main_search17, ButtonForeground );
	UxPutFontList( pb_main_search17, BoldTextFont );
	UxPutBackground( pb_main_search17, ButtonBackground );
	UxPutHeight( pb_main_search17, 28 );
	UxPutY( pb_main_search17, 50 );
	UxPutX( pb_main_search17, 366 );

	UxPutWidth( pb_main_search18, 46 );
	UxPutRecomputeSize( pb_main_search18, "true" );
	UxPutLabelString( pb_main_search18, "..." );
	UxPutForeground( pb_main_search18, ButtonForeground );
	UxPutFontList( pb_main_search18, BoldTextFont );
	UxPutBackground( pb_main_search18, ButtonBackground );
	UxPutHeight( pb_main_search18, 28 );
	UxPutY( pb_main_search18, 88 );
	UxPutX( pb_main_search18, 364 );

	UxPutForeground( label83, TextForeground );
	UxPutAlignment( label83, "alignment_beginning" );
	UxPutLabelString( label83, "- Background" );
	UxPutFontList( label83, TextFont );
	UxPutBackground( label83, LabelBackground );
	UxPutHeight( label83, 30 );
	UxPutWidth( label83, 94 );
	UxPutY( label83, 42 );
	UxPutX( label83, 4 );

	UxPutForeground( label84, TextForeground );
	UxPutAlignment( label84, "alignment_beginning" );
	UxPutLabelString( label84, "= Output Frame" );
	UxPutFontList( label84, TextFont );
	UxPutBackground( label84, LabelBackground );
	UxPutHeight( label84, 30 );
	UxPutWidth( label84, 116 );
	UxPutY( label84, 86 );
	UxPutX( label84, 6 );

	UxPutText( tf_alpha20, "5000" );
	UxPutForeground( tf_alpha20, TextForeground );
	UxPutHighlightOnEnter( tf_alpha20, "true" );
	UxPutFontList( tf_alpha20, TextFont );
	UxPutBackground( tf_alpha20, TextBackground );
	UxPutHeight( tf_alpha20, 32 );
	UxPutWidth( tf_alpha20, 78 );
	UxPutY( tf_alpha20, 174 );
	UxPutX( tf_alpha20, 394 );

	UxPutText( tf_alpha21, "0" );
	UxPutForeground( tf_alpha21, TextForeground );
	UxPutHighlightOnEnter( tf_alpha21, "true" );
	UxPutFontList( tf_alpha21, TextFont );
	UxPutBackground( tf_alpha21, TextBackground );
	UxPutHeight( tf_alpha21, 32 );
	UxPutWidth( tf_alpha21, 70 );
	UxPutY( tf_alpha21, 172 );
	UxPutX( tf_alpha21, 266 );

	UxPutForeground( label85, TextForeground );
	UxPutAlignment( label85, "alignment_beginning" );
	UxPutLabelString( label85, "Min=" );
	UxPutFontList( label85, TextFont );
	UxPutBackground( label85, LabelBackground );
	UxPutHeight( label85, 30 );
	UxPutWidth( label85, 40 );
	UxPutY( label85, 174 );
	UxPutX( label85, 222 );

	UxPutForeground( label86, TextForeground );
	UxPutAlignment( label86, "alignment_beginning" );
	UxPutLabelString( label86, "Max=" );
	UxPutFontList( label86, TextFont );
	UxPutBackground( label86, LabelBackground );
	UxPutHeight( label86, 30 );
	UxPutWidth( label86, 50 );
	UxPutY( label86, 174 );
	UxPutX( label86, 342 );

	UxPutForeground( label87, TextForeground );
	UxPutAlignment( label87, "alignment_beginning" );
	UxPutLabelString( label87, "Cuts:" );
	UxPutFontList( label87, TextFont );
	UxPutBackground( label87, LabelBackground );
	UxPutHeight( label87, 30 );
	UxPutWidth( label87, 50 );
	UxPutY( label87, 172 );
	UxPutX( label87, 162 );

	UxPutLabelString( pb_search_plot11, "Display" );
	UxPutForeground( pb_search_plot11, ButtonForeground );
	UxPutFontList( pb_search_plot11, BoldTextFont );
	UxPutBackground( pb_search_plot11, ButtonBackground );
	UxPutHeight( pb_search_plot11, 30 );
	UxPutWidth( pb_search_plot11, 86 );
	UxPutY( pb_search_plot11, 10 );
	UxPutX( pb_search_plot11, 420 );

	UxPutLabelString( pb_search_plot15, "Display" );
	UxPutForeground( pb_search_plot15, ButtonForeground );
	UxPutFontList( pb_search_plot15, BoldTextFont );
	UxPutBackground( pb_search_plot15, ButtonBackground );
	UxPutHeight( pb_search_plot15, 30 );
	UxPutWidth( pb_search_plot15, 86 );
	UxPutY( pb_search_plot15, 48 );
	UxPutX( pb_search_plot15, 422 );

	UxPutLabelString( pb_search_plot16, "Display" );
	UxPutForeground( pb_search_plot16, ButtonForeground );
	UxPutFontList( pb_search_plot16, BoldTextFont );
	UxPutBackground( pb_search_plot16, ButtonBackground );
	UxPutHeight( pb_search_plot16, 30 );
	UxPutWidth( pb_search_plot16, 86 );
	UxPutY( pb_search_plot16, 88 );
	UxPutX( pb_search_plot16, 422 );

	UxPutWidth( pb_main_search8, 46 );
	UxPutRecomputeSize( pb_main_search8, "true" );
	UxPutLabelString( pb_main_search8, "..." );
	UxPutForeground( pb_main_search8, ButtonForeground );
	UxPutFontList( pb_main_search8, BoldTextFont );
	UxPutBackground( pb_main_search8, ButtonBackground );
	UxPutHeight( pb_main_search8, 28 );
	UxPutY( pb_main_search8, 12 );
	UxPutX( pb_main_search8, 366 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_BackgroundShell()
{
	/* Create the swidgets */

	BackgroundShell = UxCreateTransientShell( "BackgroundShell", NO_PARENT );
	UxPutContext( BackgroundShell, UxBackgroundShellContext );

	form29 = UxCreateForm( "form29", BackgroundShell );
	form30 = UxCreateForm( "form30", form29 );
	pushButton4 = UxCreatePushButton( "pushButton4", form30 );
	pb_search_plot1 = UxCreatePushButton( "pb_search_plot1", form30 );
	pb_search_search6 = UxCreatePushButton( "pb_search_search6", form30 );
	pb_search_plot9 = UxCreatePushButton( "pb_search_plot9", form30 );
	pb_search_plot10 = UxCreatePushButton( "pb_search_plot10", form30 );
	pb_search_plot12 = UxCreatePushButton( "pb_search_plot12", form30 );
	shelp_search4 = UxCreateText( "shelp_search4", form29 );
	separator24 = UxCreateSeparator( "separator24", form29 );
	separator25 = UxCreateSeparator( "separator25", form29 );
	label69 = UxCreateLabel( "label69", form29 );
	tf_alpha8 = UxCreateTextField( "tf_alpha8", form29 );
	tf_alpha9 = UxCreateTextField( "tf_alpha9", form29 );
	label70 = UxCreateLabel( "label70", form29 );
	tf_alpha10 = UxCreateTextField( "tf_alpha10", form29 );
	tf_alpha12 = UxCreateTextField( "tf_alpha12", form29 );
	tf_alpha13 = UxCreateTextField( "tf_alpha13", form29 );
	label71 = UxCreateLabel( "label71", form29 );
	label72 = UxCreateLabel( "label72", form29 );
	tf_alpha14 = UxCreateTextField( "tf_alpha14", form29 );
	label74 = UxCreateLabel( "label74", form29 );
	label75 = UxCreateLabel( "label75", form29 );
	label77 = UxCreateLabel( "label77", form29 );
	label78 = UxCreateLabel( "label78", form29 );
	toggleButton17 = UxCreateToggleButton( "toggleButton17", form29 );
	rowColumn1 = UxCreateRowColumn( "rowColumn1", form29 );
	rb_seamtd_gaus1 = UxCreateToggleButton( "rb_seamtd_gaus1", rowColumn1 );
	rb_seamtd_grav1 = UxCreateToggleButton( "rb_seamtd_grav1", rowColumn1 );
	rb_seamtd_maxi1 = UxCreateToggleButton( "rb_seamtd_maxi1", rowColumn1 );
	label79 = UxCreateLabel( "label79", form29 );
	label80 = UxCreateLabel( "label80", form29 );
	tf_alpha15 = UxCreateTextField( "tf_alpha15", form29 );
	tf_alpha16 = UxCreateTextField( "tf_alpha16", form29 );
	label81 = UxCreateLabel( "label81", form29 );
	tf_alpha17 = UxCreateTextField( "tf_alpha17", form29 );
	tf_alpha18 = UxCreateTextField( "tf_alpha18", form29 );
	tf_alpha19 = UxCreateTextField( "tf_alpha19", form29 );
	label82 = UxCreateLabel( "label82", form29 );
	pb_main_search17 = UxCreatePushButton( "pb_main_search17", form29 );
	pb_main_search18 = UxCreatePushButton( "pb_main_search18", form29 );
	label83 = UxCreateLabel( "label83", form29 );
	label84 = UxCreateLabel( "label84", form29 );
	tf_alpha20 = UxCreateTextField( "tf_alpha20", form29 );
	tf_alpha21 = UxCreateTextField( "tf_alpha21", form29 );
	label85 = UxCreateLabel( "label85", form29 );
	label86 = UxCreateLabel( "label86", form29 );
	label87 = UxCreateLabel( "label87", form29 );
	pb_search_plot11 = UxCreatePushButton( "pb_search_plot11", form29 );
	pb_search_plot15 = UxCreatePushButton( "pb_search_plot15", form29 );
	pb_search_plot16 = UxCreatePushButton( "pb_search_plot16", form29 );
	pb_main_search8 = UxCreatePushButton( "pb_main_search8", form29 );

	_Uxinit_BackgroundShell();

	/* Create the X widgets */

	UxCreateWidget( BackgroundShell );
	UxCreateWidget( form29 );
	UxCreateWidget( form30 );
	UxCreateWidget( pushButton4 );
	UxCreateWidget( pb_search_plot1 );
	UxCreateWidget( pb_search_search6 );
	UxCreateWidget( pb_search_plot9 );
	UxCreateWidget( pb_search_plot10 );
	UxCreateWidget( pb_search_plot12 );
	UxCreateWidget( shelp_search4 );
	UxCreateWidget( separator24 );
	UxCreateWidget( separator25 );
	UxCreateWidget( label69 );
	UxCreateWidget( tf_alpha8 );
	UxCreateWidget( tf_alpha9 );
	UxCreateWidget( label70 );
	UxCreateWidget( tf_alpha10 );
	UxCreateWidget( tf_alpha12 );
	UxCreateWidget( tf_alpha13 );
	UxCreateWidget( label71 );
	UxCreateWidget( label72 );
	UxCreateWidget( tf_alpha14 );
	UxCreateWidget( label74 );
	UxCreateWidget( label75 );
	UxCreateWidget( label77 );
	UxCreateWidget( label78 );
	UxCreateWidget( toggleButton17 );
	UxCreateWidget( rowColumn1 );
	UxCreateWidget( rb_seamtd_gaus1 );
	UxCreateWidget( rb_seamtd_grav1 );
	UxCreateWidget( rb_seamtd_maxi1 );
	UxCreateWidget( label79 );
	UxCreateWidget( label80 );
	UxCreateWidget( tf_alpha15 );
	UxCreateWidget( tf_alpha16 );
	UxCreateWidget( label81 );
	UxCreateWidget( tf_alpha17 );
	UxCreateWidget( tf_alpha18 );
	UxCreateWidget( tf_alpha19 );
	UxCreateWidget( label82 );
	UxCreateWidget( pb_main_search17 );
	UxCreateWidget( pb_main_search18 );
	UxCreateWidget( label83 );
	UxCreateWidget( label84 );
	UxCreateWidget( tf_alpha20 );
	UxCreateWidget( tf_alpha21 );
	UxCreateWidget( label85 );
	UxCreateWidget( label86 );
	UxCreateWidget( label87 );
	UxCreateWidget( pb_search_plot11 );
	UxCreateWidget( pb_search_plot15 );
	UxCreateWidget( pb_search_plot16 );
	UxCreateWidget( pb_main_search8 );

	UxAddCallback( pushButton4, XmNactivateCallback,
			activateCB_pushButton4,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_search_plot1, XmNactivateCallback,
			activateCB_pb_search_plot1,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_search_search6, XmNactivateCallback,
			activateCB_pb_search_search6,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_search_plot9, XmNactivateCallback,
			activateCB_pb_search_plot9,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_search_plot10, XmNactivateCallback,
			activateCB_pb_search_plot10,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_search_plot12, XmNactivateCallback,
			activateCB_pb_search_plot12,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha8, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha8,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha9, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha9,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha10, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha10,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha12, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha12,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha13, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha13,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha14, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha14,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( toggleButton17, XmNarmCallback,
			armCB_toggleButton17,
			(XtPointer) UxBackgroundShellContext );
	UxAddCallback( toggleButton17, XmNvalueChangedCallback,
			valueChangedCB_toggleButton17,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( rb_seamtd_gaus1, XmNarmCallback,
			armCB_rb_seamtd_gaus1,
			(XtPointer) UxBackgroundShellContext );
	UxAddCallback( rb_seamtd_gaus1, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_gaus1,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( rb_seamtd_grav1, XmNarmCallback,
			armCB_rb_seamtd_grav1,
			(XtPointer) UxBackgroundShellContext );
	UxAddCallback( rb_seamtd_grav1, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_grav1,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( rb_seamtd_maxi1, XmNarmCallback,
			armCB_rb_seamtd_maxi1,
			(XtPointer) UxBackgroundShellContext );
	UxAddCallback( rb_seamtd_maxi1, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_maxi1,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha15, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha15,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha16, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha16,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha17, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha17,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha18, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha18,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha19, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha19,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_main_search17, XmNactivateCallback,
			activateCB_pb_main_search17,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_main_search18, XmNactivateCallback,
			activateCB_pb_main_search18,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha20, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha20,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( tf_alpha21, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha21,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_search_plot11, XmNactivateCallback,
			activateCB_pb_search_plot11,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_search_plot15, XmNactivateCallback,
			activateCB_pb_search_plot15,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_search_plot16, XmNactivateCallback,
			activateCB_pb_search_plot16,
			(XtPointer) UxBackgroundShellContext );

	UxAddCallback( pb_main_search8, XmNactivateCallback,
			activateCB_pb_main_search8,
			(XtPointer) UxBackgroundShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( BackgroundShell );

	return ( BackgroundShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_BackgroundShell()
{
	swidget                 rtrn;
	_UxCBackgroundShell     *UxContext;

	UxBackgroundShellContext = UxContext =
		(_UxCBackgroundShell *) UxMalloc( sizeof(_UxCBackgroundShell) );

	rtrn = _Uxbuild_BackgroundShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_BackgroundShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "UpdateDirectory", action_UpdateDirectory },
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_BackgroundShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

