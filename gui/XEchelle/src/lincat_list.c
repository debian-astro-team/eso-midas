/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	lincat_list.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxScList.h"
#include "UxScrW.h"
#include "UxForm.h"
#include "UxTopSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxlincat_list;
	swidget	Uxform9;
	swidget	UxscrolledWindow2;
	swidget	Uxsl_lincat_list;
} _UxClincat_list;

#define lincat_list             UxLincat_listContext->Uxlincat_list
#define form9                   UxLincat_listContext->Uxform9
#define scrolledWindow2         UxLincat_listContext->UxscrolledWindow2
#define sl_lincat_list          UxLincat_listContext->Uxsl_lincat_list

static _UxClincat_list	*UxLincat_listContext;

extern int XmToggleButtonGetState(), AppendDialogText();


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_lincat_list();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	browseSelectionCB_sl_lincat_list( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxClincat_list         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxLincat_listContext;
	UxLincat_listContext = UxContext =
			(_UxClincat_list *) UxGetContext( UxThisWidget );
	{
#include <spec_comm.h>
	
	char command[256];
	char *choice;
	XmListCallbackStruct *cbs;
	extern char Plotmode[];
	
	cbs = (XmListCallbackStruct *)UxCallbackArg;
	
	XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);
	
	if ( XmToggleButtonGetState(UxGetWidget(UxFindSwidget("tg_twodopt"))) )
	    sprintf(command, "%s%s ? 2D", C_PLOT_DISTOR, choice); /* 2-D */
	else
	    sprintf(command, "%s%s", C_PLOT_DISTOR, choice); /* RBR */
	
	AppendDialogText(command);
	strcpy(Plotmode, C_PLOT_DISTOR);
	
	XtFree(choice);
	UxPopdownInterface(UxFindSwidget("lincat_list"));
	
	}
	UxLincat_listContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_lincat_list()
{
	UxPutGeometry( lincat_list, "+100+100" );
	UxPutBackground( lincat_list, "AntiqueWhite" );
	UxPutKeyboardFocusPolicy( lincat_list, "pointer" );
	UxPutIconName( lincat_list, "Wavelength selection" );
	UxPutHeight( lincat_list, 355 );
	UxPutWidth( lincat_list, 199 );
	UxPutY( lincat_list, 236 );
	UxPutX( lincat_list, 571 );

	UxPutBackground( form9, ApplicBackground );
	UxPutHeight( form9, 352 );
	UxPutWidth( form9, 214 );
	UxPutY( form9, 0 );
	UxPutX( form9, 0 );
	UxPutUnitType( form9, "pixels" );
	UxPutResizePolicy( form9, "resize_none" );

	UxPutWidth( scrolledWindow2, 160 );
	UxPutHeight( scrolledWindow2, 350 );
	UxPutBackground( scrolledWindow2, ListBackground );
	UxPutScrollBarPlacement( scrolledWindow2, "bottom_left" );
	UxPutShadowThickness( scrolledWindow2, 0 );
	UxPutScrollBarDisplayPolicy( scrolledWindow2, "static" );
	UxPutVisualPolicy( scrolledWindow2, "variable" );
	UxPutY( scrolledWindow2, 2 );
	UxPutX( scrolledWindow2, 2 );
	UxPutScrollingPolicy( scrolledWindow2, "application_defined" );

	UxPutForeground( sl_lincat_list, ListForeground );
	UxPutVisibleItemCount( sl_lincat_list, 21 );
	UxPutFontList( sl_lincat_list, SmallFont );
	UxPutBackground( sl_lincat_list, TextBackground );
	UxPutListSizePolicy( sl_lincat_list, "variable" );
	UxPutScrollBarDisplayPolicy( sl_lincat_list, "static" );
	UxPutHeight( sl_lincat_list, 352 );
	UxPutWidth( sl_lincat_list, 238 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_lincat_list()
{
	/* Create the swidgets */

	lincat_list = UxCreateTopLevelShell( "lincat_list", NO_PARENT );
	UxPutContext( lincat_list, UxLincat_listContext );

	form9 = UxCreateForm( "form9", lincat_list );
	scrolledWindow2 = UxCreateScrolledWindow( "scrolledWindow2", form9 );
	sl_lincat_list = UxCreateScrolledList( "sl_lincat_list", scrolledWindow2 );

	_Uxinit_lincat_list();

	/* Create the X widgets */

	UxCreateWidget( lincat_list );
	UxCreateWidget( form9 );
	UxPutLeftOffset( scrolledWindow2, 2 );
	UxPutLeftAttachment( scrolledWindow2, "attach_form" );
	UxPutRightOffset( scrolledWindow2, 2 );
	UxPutRightAttachment( scrolledWindow2, "attach_form" );
	UxCreateWidget( scrolledWindow2 );

	UxCreateWidget( sl_lincat_list );

	UxAddCallback( sl_lincat_list, XmNbrowseSelectionCallback,
			browseSelectionCB_sl_lincat_list,
			(XtPointer) UxLincat_listContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( lincat_list );

	return ( lincat_list );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_lincat_list()
{
	swidget                 rtrn;
	_UxClincat_list         *UxContext;

	UxLincat_listContext = UxContext =
		(_UxClincat_list *) UxMalloc( sizeof(_UxClincat_list) );

	rtrn = _Uxbuild_lincat_list();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_lincat_list()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_lincat_list();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

