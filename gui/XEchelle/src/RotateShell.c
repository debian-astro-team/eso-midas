/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	RotateShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxTogB.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxPushBG.h"
#include "UxRowCol.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxRotateShell;
	swidget	Uxform25;
	swidget	Uxform26;
	swidget	Uxpb_search_search2;
	swidget	UxpushButton2;
	swidget	Uxpb_search_plot2;
	swidget	Uxshelp_search2;
	swidget	Uxseparator20;
	swidget	Uxseparator21;
	swidget	Uxmenu2_p2;
	swidget	Uxmn_tol_angstroms2;
	swidget	Uxmn_tol_pixels2;
	swidget	Uxmenu2_p2_b3;
	swidget	Uxmenu2_p2_b4;
	swidget	Uxmn_tol2;
	swidget	Uxmenu2_p3;
	swidget	Uxmn_tol_angstroms3;
	swidget	Uxmn_tol_pixels3;
	swidget	Uxmenu2_p3_b4;
	swidget	Uxmenu2_p3_b3;
	swidget	Uxmn_tol3;
	swidget	Uxmenu2_p5;
	swidget	Uxmn_tol_angstroms5;
	swidget	Uxmn_tol_pixels5;
	swidget	Uxmenu2_p5_b3;
	swidget	Uxmenu2_p5_b4;
	swidget	Uxmn_tol5;
	swidget	Uxlabel40;
	swidget	Uxtf_alpha2;
	swidget	Uxlabel41;
	swidget	Uxtf_alpha4;
	swidget	Uxtf_alpha5;
	swidget	Uxtf_alpha6;
	swidget	Uxlabel54;
	swidget	UxrowColumn23;
	swidget	UxtoggleButton14;
	swidget	UxtoggleButton15;
	swidget	Uxlabel44;
	swidget	Uxlabel55;
	swidget	Uxlabel56;
	swidget	Uxlabel57;
	swidget	Uxlabel58;
	swidget	Uxlabel59;
	swidget	Uxlabel60;
	swidget	Uxseparator34;
	swidget	Uxlabel49;
	swidget	Uxlabel50;
	swidget	Uxseparator35;
	swidget	Uxlabel51;
	swidget	Uxpb_main_search32;
	swidget	Uxlabel52;
	swidget	Uxtf_alpha1;
	swidget	Uxtf_alpha3;
	swidget	Uxtf_alpha7;
	swidget	Uxtf_alpha44;
} _UxCRotateShell;

#define RotateShell             UxRotateShellContext->UxRotateShell
#define form25                  UxRotateShellContext->Uxform25
#define form26                  UxRotateShellContext->Uxform26
#define pb_search_search2       UxRotateShellContext->Uxpb_search_search2
#define pushButton2             UxRotateShellContext->UxpushButton2
#define pb_search_plot2         UxRotateShellContext->Uxpb_search_plot2
#define shelp_search2           UxRotateShellContext->Uxshelp_search2
#define separator20             UxRotateShellContext->Uxseparator20
#define separator21             UxRotateShellContext->Uxseparator21
#define menu2_p2                UxRotateShellContext->Uxmenu2_p2
#define mn_tol_angstroms2       UxRotateShellContext->Uxmn_tol_angstroms2
#define mn_tol_pixels2          UxRotateShellContext->Uxmn_tol_pixels2
#define menu2_p2_b3             UxRotateShellContext->Uxmenu2_p2_b3
#define menu2_p2_b4             UxRotateShellContext->Uxmenu2_p2_b4
#define mn_tol2                 UxRotateShellContext->Uxmn_tol2
#define menu2_p3                UxRotateShellContext->Uxmenu2_p3
#define mn_tol_angstroms3       UxRotateShellContext->Uxmn_tol_angstroms3
#define mn_tol_pixels3          UxRotateShellContext->Uxmn_tol_pixels3
#define menu2_p3_b4             UxRotateShellContext->Uxmenu2_p3_b4
#define menu2_p3_b3             UxRotateShellContext->Uxmenu2_p3_b3
#define mn_tol3                 UxRotateShellContext->Uxmn_tol3
#define menu2_p5                UxRotateShellContext->Uxmenu2_p5
#define mn_tol_angstroms5       UxRotateShellContext->Uxmn_tol_angstroms5
#define mn_tol_pixels5          UxRotateShellContext->Uxmn_tol_pixels5
#define menu2_p5_b3             UxRotateShellContext->Uxmenu2_p5_b3
#define menu2_p5_b4             UxRotateShellContext->Uxmenu2_p5_b4
#define mn_tol5                 UxRotateShellContext->Uxmn_tol5
#define label40                 UxRotateShellContext->Uxlabel40
#define tf_alpha2               UxRotateShellContext->Uxtf_alpha2
#define label41                 UxRotateShellContext->Uxlabel41
#define tf_alpha4               UxRotateShellContext->Uxtf_alpha4
#define tf_alpha5               UxRotateShellContext->Uxtf_alpha5
#define tf_alpha6               UxRotateShellContext->Uxtf_alpha6
#define label54                 UxRotateShellContext->Uxlabel54
#define rowColumn23             UxRotateShellContext->UxrowColumn23
#define toggleButton14          UxRotateShellContext->UxtoggleButton14
#define toggleButton15          UxRotateShellContext->UxtoggleButton15
#define label44                 UxRotateShellContext->Uxlabel44
#define label55                 UxRotateShellContext->Uxlabel55
#define label56                 UxRotateShellContext->Uxlabel56
#define label57                 UxRotateShellContext->Uxlabel57
#define label58                 UxRotateShellContext->Uxlabel58
#define label59                 UxRotateShellContext->Uxlabel59
#define label60                 UxRotateShellContext->Uxlabel60
#define separator34             UxRotateShellContext->Uxseparator34
#define label49                 UxRotateShellContext->Uxlabel49
#define label50                 UxRotateShellContext->Uxlabel50
#define separator35             UxRotateShellContext->Uxseparator35
#define label51                 UxRotateShellContext->Uxlabel51
#define pb_main_search32        UxRotateShellContext->Uxpb_main_search32
#define label52                 UxRotateShellContext->Uxlabel52
#define tf_alpha1               UxRotateShellContext->Uxtf_alpha1
#define tf_alpha3               UxRotateShellContext->Uxtf_alpha3
#define tf_alpha7               UxRotateShellContext->Uxtf_alpha7
#define tf_alpha44              UxRotateShellContext->Uxtf_alpha44

static _UxCRotateShell	*UxRotateShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();
extern void  WriteKeyword(), GetExtendedHelp(), SelectList();


extern int RadioSet(), PopupList(), AppendDialogText();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable8 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_RotateShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxRotateShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxRotateShellContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxRotateShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_search_search2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_pushButton2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("RotateShell"));
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_angstroms2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_pixels2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_menu2_p2_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_menu2_p2_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet(UxWidget);}
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_angstroms3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_pixels3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_menu2_p3_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_menu2_p3_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_angstroms5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_mn_tol_pixels5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_menu2_p5_b3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_menu2_p5_b4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{RadioSet ( UxWidget ) ; }
	UxRotateShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha6( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	armCB_toggleButton14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	RadioSet(UxWidget);
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton14( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	armCB_toggleButton15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	RadioSet(UxWidget);
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	valueChangedCB_toggleButton15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search32( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha1( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha44( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCRotateShell         *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxRotateShellContext;
	UxRotateShellContext = UxContext =
			(_UxCRotateShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxRotateShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_RotateShell()
{
	UxPutGeometry( RotateShell, "+10+60" );
	UxPutTitle( RotateShell, "XEchelle: Rotation and Geometry" );
	UxPutHeight( RotateShell, 540 );
	UxPutWidth( RotateShell, 500 );
	UxPutY( RotateShell, 372 );
	UxPutX( RotateShell, 722 );

	UxPutBackground( form25, WindowBackground );
	UxPutHeight( form25, 348 );
	UxPutWidth( form25, 408 );
	UxPutY( form25, -2 );
	UxPutX( form25, 0 );
	UxPutUnitType( form25, "pixels" );
	UxPutResizePolicy( form25, "resize_none" );

	UxPutBackground( form26, ButtonBackground );
	UxPutHeight( form26, 40 );
	UxPutWidth( form26, 480 );
	UxPutY( form26, 490 );
	UxPutX( form26, 6 );
	UxPutResizePolicy( form26, "resize_none" );

	UxPutTranslations( pb_search_search2, "" );
	UxPutLabelString( pb_search_search2, "Scan" );
	UxPutForeground( pb_search_search2, ApplyForeground );
	UxPutFontList( pb_search_search2, BoldTextFont );
	UxPutBackground( pb_search_search2, ButtonBackground );
	UxPutHeight( pb_search_search2, 30 );
	UxPutWidth( pb_search_search2, 86 );
	UxPutY( pb_search_search2, 4 );
	UxPutX( pb_search_search2, 8 );

	UxPutTranslations( pushButton2, "" );
	UxPutLabelString( pushButton2, "Cancel" );
	UxPutForeground( pushButton2, CancelForeground );
	UxPutFontList( pushButton2, BoldTextFont );
	UxPutBackground( pushButton2, ButtonBackground );
	UxPutHeight( pushButton2, 30 );
	UxPutWidth( pushButton2, 86 );
	UxPutY( pushButton2, 4 );
	UxPutX( pushButton2, 196 );

	UxPutTranslations( pb_search_plot2, "" );
	UxPutLabelString( pb_search_plot2, "Help..." );
	UxPutForeground( pb_search_plot2, ButtonForeground );
	UxPutFontList( pb_search_plot2, BoldTextFont );
	UxPutBackground( pb_search_plot2, ButtonBackground );
	UxPutHeight( pb_search_plot2, 30 );
	UxPutWidth( pb_search_plot2, 86 );
	UxPutY( pb_search_plot2, 4 );
	UxPutX( pb_search_plot2, 102 );

	UxPutFontList( shelp_search2, TextFont );
	UxPutEditable( shelp_search2, "false" );
	UxPutCursorPositionVisible( shelp_search2, "false" );
	UxPutBackground( shelp_search2, SHelpBackground );
	UxPutHeight( shelp_search2, 50 );
	UxPutWidth( shelp_search2, 484 );
	UxPutY( shelp_search2, 428 );
	UxPutX( shelp_search2, 8 );

	UxPutBackground( separator20, WindowBackground );
	UxPutHeight( separator20, 10 );
	UxPutWidth( separator20, 492 );
	UxPutY( separator20, 280 );
	UxPutX( separator20, 4 );

	UxPutBackground( separator21, WindowBackground );
	UxPutHeight( separator21, 10 );
	UxPutWidth( separator21, 492 );
	UxPutY( separator21, 478 );
	UxPutX( separator21, 2 );

	UxPutTranslations( menu2_p2, "" );
	UxPutForeground( menu2_p2, TextForeground );
	UxPutBackground( menu2_p2, WindowBackground );
	UxPutRowColumnType( menu2_p2, "menu_pulldown" );

	UxPutFontList( mn_tol_angstroms2, BoldTextFont );
	UxPutLabelString( mn_tol_angstroms2, " Emmi " );

	UxPutFontList( mn_tol_pixels2, BoldTextFont );
	UxPutLabelString( mn_tol_pixels2, " Caspec " );

	UxPutFontList( menu2_p2_b3, BoldTextFont );
	UxPutLabelString( menu2_p2_b3, " Efosc " );

	UxPutFontList( menu2_p2_b4, BoldTextFont );
	UxPutLabelString( menu2_p2_b4, " Echelec " );

	UxPutTranslations( mn_tol2, transTable8 );
	UxPutLabelString( mn_tol2, " " );
	UxPutSpacing( mn_tol2, 0 );
	UxPutMarginWidth( mn_tol2, 0 );
	UxPutForeground( mn_tol2, TextForeground );
	UxPutBackground( mn_tol2, WindowBackground );
	UxPutY( mn_tol2, 38 );
	UxPutX( mn_tol2, 206 );
	UxPutRowColumnType( mn_tol2, "menu_option" );

	UxPutForeground( menu2_p3, TextForeground );
	UxPutBackground( menu2_p3, WindowBackground );
	UxPutRowColumnType( menu2_p3, "menu_pulldown" );

	UxPutFontList( mn_tol_angstroms3, BoldTextFont );
	UxPutLabelString( mn_tol_angstroms3, " No Flip " );

	UxPutFontList( mn_tol_pixels3, BoldTextFont );
	UxPutLabelString( mn_tol_pixels3, " Flip X " );

	UxPutFontList( menu2_p3_b4, BoldTextFont );
	UxPutLabelString( menu2_p3_b4, " Flip Y " );

	UxPutFontList( menu2_p3_b3, BoldTextFont );
	UxPutLabelString( menu2_p3_b3, " Flip XY " );

	UxPutTranslations( mn_tol3, transTable8 );
	UxPutLabelString( mn_tol3, " " );
	UxPutSpacing( mn_tol3, 0 );
	UxPutMarginWidth( mn_tol3, 0 );
	UxPutForeground( mn_tol3, TextForeground );
	UxPutBackground( mn_tol3, WindowBackground );
	UxPutY( mn_tol3, 72 );
	UxPutX( mn_tol3, 206 );
	UxPutRowColumnType( mn_tol3, "menu_option" );

	UxPutForeground( menu2_p5, TextForeground );
	UxPutBackground( menu2_p5, WindowBackground );
	UxPutRowColumnType( menu2_p5, "menu_pulldown" );

	UxPutFontList( mn_tol_angstroms5, BoldTextFont );
	UxPutLabelString( mn_tol_angstroms5, " No Rotation " );

	UxPutFontList( mn_tol_pixels5, BoldTextFont );
	UxPutLabelString( mn_tol_pixels5, " Rotate 90 " );

	UxPutFontList( menu2_p5_b3, BoldTextFont );
	UxPutLabelString( menu2_p5_b3, " Rotate 180 " );

	UxPutFontList( menu2_p5_b4, BoldTextFont );
	UxPutLabelString( menu2_p5_b4, " Rotate 270 " );

	UxPutTranslations( mn_tol5, transTable8 );
	UxPutLabelString( mn_tol5, " " );
	UxPutSpacing( mn_tol5, 0 );
	UxPutMarginWidth( mn_tol5, 0 );
	UxPutForeground( mn_tol5, TextForeground );
	UxPutBackground( mn_tol5, WindowBackground );
	UxPutY( mn_tol5, 72 );
	UxPutX( mn_tol5, 324 );
	UxPutRowColumnType( mn_tol5, "menu_option" );

	UxPutForeground( label40, TextForeground );
	UxPutAlignment( label40, "alignment_beginning" );
	UxPutLabelString( label40, "Binning Factor :" );
	UxPutFontList( label40, TextFont );
	UxPutBackground( label40, LabelBackground );
	UxPutHeight( label40, 30 );
	UxPutWidth( label40, 150 );
	UxPutY( label40, 236 );
	UxPutX( label40, 20 );

	UxPutTranslations( tf_alpha2, "" );
	UxPutForeground( tf_alpha2, TextForeground );
	UxPutHighlightOnEnter( tf_alpha2, "true" );
	UxPutFontList( tf_alpha2, TextFont );
	UxPutBackground( tf_alpha2, TextBackground );
	UxPutHeight( tf_alpha2, 32 );
	UxPutWidth( tf_alpha2, 96 );
	UxPutY( tf_alpha2, 376 );
	UxPutX( tf_alpha2, 218 );

	UxPutForeground( label41, TextForeground );
	UxPutAlignment( label41, "alignment_beginning" );
	UxPutLabelString( label41, "Scan Limits (pixels) :" );
	UxPutFontList( label41, TextFont );
	UxPutBackground( label41, LabelBackground );
	UxPutHeight( label41, 30 );
	UxPutWidth( label41, 150 );
	UxPutY( label41, 378 );
	UxPutX( label41, 14 );

	UxPutTranslations( tf_alpha4, "" );
	UxPutForeground( tf_alpha4, TextForeground );
	UxPutHighlightOnEnter( tf_alpha4, "true" );
	UxPutFontList( tf_alpha4, TextFont );
	UxPutBackground( tf_alpha4, TextBackground );
	UxPutHeight( tf_alpha4, 32 );
	UxPutWidth( tf_alpha4, 96 );
	UxPutY( tf_alpha4, 376 );
	UxPutX( tf_alpha4, 374 );

	UxPutTranslations( tf_alpha5, "" );
	UxPutForeground( tf_alpha5, TextForeground );
	UxPutHighlightOnEnter( tf_alpha5, "true" );
	UxPutFontList( tf_alpha5, TextFont );
	UxPutBackground( tf_alpha5, TextBackground );
	UxPutHeight( tf_alpha5, 32 );
	UxPutWidth( tf_alpha5, 96 );
	UxPutY( tf_alpha5, 232 );
	UxPutX( tf_alpha5, 372 );

	UxPutTranslations( tf_alpha6, "" );
	UxPutText( tf_alpha6, "1." );
	UxPutForeground( tf_alpha6, TextForeground );
	UxPutHighlightOnEnter( tf_alpha6, "true" );
	UxPutFontList( tf_alpha6, TextFont );
	UxPutBackground( tf_alpha6, TextBackground );
	UxPutHeight( tf_alpha6, 32 );
	UxPutWidth( tf_alpha6, 97 );
	UxPutY( tf_alpha6, 108 );
	UxPutX( tf_alpha6, 216 );

	UxPutForeground( label54, TextForeground );
	UxPutAlignment( label54, "alignment_beginning" );
	UxPutLabelString( label54, "Default Exposure Time (s):" );
	UxPutFontList( label54, TextFont );
	UxPutBackground( label54, LabelBackground );
	UxPutHeight( label54, 30 );
	UxPutWidth( label54, 204 );
	UxPutY( label54, 110 );
	UxPutX( label54, 12 );

	UxPutResizeWidth( rowColumn23, "false" );
	UxPutRadioBehavior( rowColumn23, "true" );
	UxPutBackground( rowColumn23, WindowBackground );
	UxPutHeight( rowColumn23, 67 );
	UxPutWidth( rowColumn23, 200 );
	UxPutY( rowColumn23, 42 );
	UxPutX( rowColumn23, 8 );

	UxPutSelectColor( toggleButton14, "Yellow" );
	UxPutLabelString( toggleButton14, "Standard Orientation for:" );
	UxPutForeground( toggleButton14, TextForeground );
	UxPutFontList( toggleButton14, TextFont );
	UxPutBackground( toggleButton14, WindowBackground );
	UxPutRecomputeSize( toggleButton14, "false" );
	UxPutHeight( toggleButton14, 29 );
	UxPutWidth( toggleButton14, 150 );
	UxPutY( toggleButton14, 3 );
	UxPutX( toggleButton14, 3 );

	UxPutSet( toggleButton15, "true" );
	UxPutSelectColor( toggleButton15, "Yellow" );
	UxPutLabelString( toggleButton15, "Rotation Parameters:" );
	UxPutForeground( toggleButton15, TextForeground );
	UxPutFontList( toggleButton15, TextFont );
	UxPutBackground( toggleButton15, WindowBackground );
	UxPutRecomputeSize( toggleButton15, "false" );
	UxPutHeight( toggleButton15, 29 );
	UxPutWidth( toggleButton15, 234 );
	UxPutY( toggleButton15, 14 );
	UxPutX( toggleButton15, 12 );

	UxPutForeground( label44, TextForeground );
	UxPutAlignment( label44, "alignment_beginning" );
	UxPutLabelString( label44, "Image size (pixels):" );
	UxPutFontList( label44, TextFont );
	UxPutBackground( label44, LabelBackground );
	UxPutHeight( label44, 30 );
	UxPutWidth( label44, 150 );
	UxPutY( label44, 192 );
	UxPutX( label44, 20 );

	UxPutForeground( label55, TextForeground );
	UxPutAlignment( label55, "alignment_beginning" );
	UxPutLabelString( label55, "Ymin=" );
	UxPutFontList( label55, TextFont );
	UxPutBackground( label55, LabelBackground );
	UxPutHeight( label55, 30 );
	UxPutWidth( label55, 40 );
	UxPutY( label55, 378 );
	UxPutX( label55, 172 );

	UxPutForeground( label56, TextForeground );
	UxPutAlignment( label56, "alignment_beginning" );
	UxPutLabelString( label56, "X=" );
	UxPutFontList( label56, TextFont );
	UxPutBackground( label56, LabelBackground );
	UxPutHeight( label56, 30 );
	UxPutWidth( label56, 40 );
	UxPutY( label56, 232 );
	UxPutX( label56, 198 );

	UxPutForeground( label57, TextForeground );
	UxPutAlignment( label57, "alignment_beginning" );
	UxPutLabelString( label57, "Y=" );
	UxPutFontList( label57, TextFont );
	UxPutBackground( label57, LabelBackground );
	UxPutHeight( label57, 30 );
	UxPutWidth( label57, 20 );
	UxPutY( label57, 186 );
	UxPutX( label57, 352 );

	UxPutForeground( label58, TextForeground );
	UxPutAlignment( label58, "alignment_center" );
	UxPutLabelString( label58, "Scan" );
	UxPutFontList( label58, BoldTextFont );
	UxPutBackground( label58, LabelBackground );
	UxPutHeight( label58, 30 );
	UxPutWidth( label58, 90 );
	UxPutY( label58, 294 );
	UxPutX( label58, 210 );

	UxPutForeground( label59, TextForeground );
	UxPutAlignment( label59, "alignment_beginning" );
	UxPutLabelString( label59, "Y=" );
	UxPutFontList( label59, TextFont );
	UxPutBackground( label59, LabelBackground );
	UxPutHeight( label59, 30 );
	UxPutWidth( label59, 20 );
	UxPutY( label59, 222 );
	UxPutX( label59, 354 );

	UxPutForeground( label60, TextForeground );
	UxPutAlignment( label60, "alignment_beginning" );
	UxPutLabelString( label60, "X=" );
	UxPutFontList( label60, TextFont );
	UxPutBackground( label60, LabelBackground );
	UxPutHeight( label60, 30 );
	UxPutWidth( label60, 40 );
	UxPutY( label60, 188 );
	UxPutX( label60, 202 );

	UxPutBackground( separator34, WindowBackground );
	UxPutHeight( separator34, 10 );
	UxPutWidth( separator34, 492 );
	UxPutY( separator34, 420 );
	UxPutX( separator34, 4 );

	UxPutForeground( label49, TextForeground );
	UxPutAlignment( label49, "alignment_beginning" );
	UxPutLabelString( label49, "Ymax=" );
	UxPutFontList( label49, TextFont );
	UxPutBackground( label49, LabelBackground );
	UxPutHeight( label49, 30 );
	UxPutWidth( label49, 50 );
	UxPutY( label49, 376 );
	UxPutX( label49, 322 );

	UxPutForeground( label50, TextForeground );
	UxPutAlignment( label50, "alignment_center" );
	UxPutLabelString( label50, "Rotation" );
	UxPutFontList( label50, BoldTextFont );
	UxPutBackground( label50, LabelBackground );
	UxPutHeight( label50, 30 );
	UxPutWidth( label50, 90 );
	UxPutY( label50, 2 );
	UxPutX( label50, 206 );

	UxPutBackground( separator35, WindowBackground );
	UxPutHeight( separator35, 10 );
	UxPutWidth( separator35, 492 );
	UxPutY( separator35, 142 );
	UxPutX( separator35, 2 );

	UxPutForeground( label51, TextForeground );
	UxPutAlignment( label51, "alignment_center" );
	UxPutLabelString( label51, "Geometry" );
	UxPutFontList( label51, BoldTextFont );
	UxPutBackground( label51, LabelBackground );
	UxPutHeight( label51, 30 );
	UxPutWidth( label51, 90 );
	UxPutY( label51, 154 );
	UxPutX( label51, 206 );

	UxPutTranslations( pb_main_search32, "" );
	UxPutWidth( pb_main_search32, 46 );
	UxPutRecomputeSize( pb_main_search32, "true" );
	UxPutLabelString( pb_main_search32, "..." );
	UxPutForeground( pb_main_search32, ButtonForeground );
	UxPutFontList( pb_main_search32, BoldTextFont );
	UxPutBackground( pb_main_search32, ButtonBackground );
	UxPutHeight( pb_main_search32, 28 );
	UxPutY( pb_main_search32, 332 );
	UxPutX( pb_main_search32, 424 );

	UxPutForeground( label52, TextForeground );
	UxPutAlignment( label52, "alignment_beginning" );
	UxPutLabelString( label52, "Reference Image:" );
	UxPutFontList( label52, TextFont );
	UxPutBackground( label52, LabelBackground );
	UxPutHeight( label52, 30 );
	UxPutWidth( label52, 116 );
	UxPutY( label52, 330 );
	UxPutX( label52, 14 );

	UxPutTranslations( tf_alpha1, "" );
	UxPutForeground( tf_alpha1, TextForeground );
	UxPutHighlightOnEnter( tf_alpha1, "true" );
	UxPutFontList( tf_alpha1, TextFont );
	UxPutBackground( tf_alpha1, TextBackground );
	UxPutHeight( tf_alpha1, 32 );
	UxPutWidth( tf_alpha1, 96 );
	UxPutY( tf_alpha1, 184 );
	UxPutX( tf_alpha1, 248 );

	UxPutTranslations( tf_alpha3, "" );
	UxPutForeground( tf_alpha3, TextForeground );
	UxPutHighlightOnEnter( tf_alpha3, "true" );
	UxPutFontList( tf_alpha3, TextFont );
	UxPutBackground( tf_alpha3, TextBackground );
	UxPutHeight( tf_alpha3, 32 );
	UxPutWidth( tf_alpha3, 96 );
	UxPutY( tf_alpha3, 230 );
	UxPutX( tf_alpha3, 248 );

	UxPutTranslations( tf_alpha7, "" );
	UxPutForeground( tf_alpha7, TextForeground );
	UxPutHighlightOnEnter( tf_alpha7, "true" );
	UxPutFontList( tf_alpha7, TextFont );
	UxPutBackground( tf_alpha7, TextBackground );
	UxPutHeight( tf_alpha7, 32 );
	UxPutWidth( tf_alpha7, 242 );
	UxPutY( tf_alpha7, 328 );
	UxPutX( tf_alpha7, 164 );

	UxPutTranslations( tf_alpha44, "" );
	UxPutForeground( tf_alpha44, TextForeground );
	UxPutHighlightOnEnter( tf_alpha44, "true" );
	UxPutFontList( tf_alpha44, TextFont );
	UxPutBackground( tf_alpha44, TextBackground );
	UxPutHeight( tf_alpha44, 32 );
	UxPutWidth( tf_alpha44, 96 );
	UxPutY( tf_alpha44, 186 );
	UxPutX( tf_alpha44, 374 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_RotateShell()
{
	/* Create the swidgets */

	RotateShell = UxCreateTransientShell( "RotateShell", NO_PARENT );
	UxPutContext( RotateShell, UxRotateShellContext );

	form25 = UxCreateForm( "form25", RotateShell );
	form26 = UxCreateForm( "form26", form25 );
	pb_search_search2 = UxCreatePushButton( "pb_search_search2", form26 );
	pushButton2 = UxCreatePushButton( "pushButton2", form26 );
	pb_search_plot2 = UxCreatePushButton( "pb_search_plot2", form26 );
	shelp_search2 = UxCreateText( "shelp_search2", form25 );
	separator20 = UxCreateSeparator( "separator20", form25 );
	separator21 = UxCreateSeparator( "separator21", form25 );
	menu2_p2 = UxCreateRowColumn( "menu2_p2", form25 );
	mn_tol_angstroms2 = UxCreatePushButtonGadget( "mn_tol_angstroms2", menu2_p2 );
	mn_tol_pixels2 = UxCreatePushButtonGadget( "mn_tol_pixels2", menu2_p2 );
	menu2_p2_b3 = UxCreatePushButtonGadget( "menu2_p2_b3", menu2_p2 );
	menu2_p2_b4 = UxCreatePushButtonGadget( "menu2_p2_b4", menu2_p2 );
	mn_tol2 = UxCreateRowColumn( "mn_tol2", form25 );
	menu2_p3 = UxCreateRowColumn( "menu2_p3", form25 );
	mn_tol_angstroms3 = UxCreatePushButtonGadget( "mn_tol_angstroms3", menu2_p3 );
	mn_tol_pixels3 = UxCreatePushButtonGadget( "mn_tol_pixels3", menu2_p3 );
	menu2_p3_b4 = UxCreatePushButtonGadget( "menu2_p3_b4", menu2_p3 );
	menu2_p3_b3 = UxCreatePushButtonGadget( "menu2_p3_b3", menu2_p3 );
	mn_tol3 = UxCreateRowColumn( "mn_tol3", form25 );
	menu2_p5 = UxCreateRowColumn( "menu2_p5", form25 );
	mn_tol_angstroms5 = UxCreatePushButtonGadget( "mn_tol_angstroms5", menu2_p5 );
	mn_tol_pixels5 = UxCreatePushButtonGadget( "mn_tol_pixels5", menu2_p5 );
	menu2_p5_b3 = UxCreatePushButtonGadget( "menu2_p5_b3", menu2_p5 );
	menu2_p5_b4 = UxCreatePushButtonGadget( "menu2_p5_b4", menu2_p5 );
	mn_tol5 = UxCreateRowColumn( "mn_tol5", form25 );
	label40 = UxCreateLabel( "label40", form25 );
	tf_alpha2 = UxCreateTextField( "tf_alpha2", form25 );
	label41 = UxCreateLabel( "label41", form25 );
	tf_alpha4 = UxCreateTextField( "tf_alpha4", form25 );
	tf_alpha5 = UxCreateTextField( "tf_alpha5", form25 );
	tf_alpha6 = UxCreateTextField( "tf_alpha6", form25 );
	label54 = UxCreateLabel( "label54", form25 );
	rowColumn23 = UxCreateRowColumn( "rowColumn23", form25 );
	toggleButton14 = UxCreateToggleButton( "toggleButton14", rowColumn23 );
	toggleButton15 = UxCreateToggleButton( "toggleButton15", rowColumn23 );
	label44 = UxCreateLabel( "label44", form25 );
	label55 = UxCreateLabel( "label55", form25 );
	label56 = UxCreateLabel( "label56", form25 );
	label57 = UxCreateLabel( "label57", form25 );
	label58 = UxCreateLabel( "label58", form25 );
	label59 = UxCreateLabel( "label59", form25 );
	label60 = UxCreateLabel( "label60", form25 );
	separator34 = UxCreateSeparator( "separator34", form25 );
	label49 = UxCreateLabel( "label49", form25 );
	label50 = UxCreateLabel( "label50", form25 );
	separator35 = UxCreateSeparator( "separator35", form25 );
	label51 = UxCreateLabel( "label51", form25 );
	pb_main_search32 = UxCreatePushButton( "pb_main_search32", form25 );
	label52 = UxCreateLabel( "label52", form25 );
	tf_alpha1 = UxCreateTextField( "tf_alpha1", form25 );
	tf_alpha3 = UxCreateTextField( "tf_alpha3", form25 );
	tf_alpha7 = UxCreateTextField( "tf_alpha7", form25 );
	tf_alpha44 = UxCreateTextField( "tf_alpha44", form25 );

	_Uxinit_RotateShell();

	/* Create the X widgets */

	UxCreateWidget( RotateShell );
	UxCreateWidget( form25 );
	UxCreateWidget( form26 );
	UxCreateWidget( pb_search_search2 );
	UxCreateWidget( pushButton2 );
	UxCreateWidget( pb_search_plot2 );
	UxCreateWidget( shelp_search2 );
	UxCreateWidget( separator20 );
	UxCreateWidget( separator21 );
	UxCreateWidget( menu2_p2 );
	UxCreateWidget( mn_tol_angstroms2 );
	UxCreateWidget( mn_tol_pixels2 );
	UxCreateWidget( menu2_p2_b3 );
	UxCreateWidget( menu2_p2_b4 );
	UxPutSubMenuId( mn_tol2, "menu2_p2" );
	UxCreateWidget( mn_tol2 );

	UxCreateWidget( menu2_p3 );
	UxCreateWidget( mn_tol_angstroms3 );
	UxCreateWidget( mn_tol_pixels3 );
	UxCreateWidget( menu2_p3_b4 );
	UxCreateWidget( menu2_p3_b3 );
	UxPutSubMenuId( mn_tol3, "menu2_p3" );
	UxCreateWidget( mn_tol3 );

	UxCreateWidget( menu2_p5 );
	UxCreateWidget( mn_tol_angstroms5 );
	UxCreateWidget( mn_tol_pixels5 );
	UxCreateWidget( menu2_p5_b3 );
	UxCreateWidget( menu2_p5_b4 );
	UxPutSubMenuId( mn_tol5, "menu2_p5" );
	UxCreateWidget( mn_tol5 );

	UxCreateWidget( label40 );
	UxCreateWidget( tf_alpha2 );
	UxCreateWidget( label41 );
	UxCreateWidget( tf_alpha4 );
	UxCreateWidget( tf_alpha5 );
	UxCreateWidget( tf_alpha6 );
	UxCreateWidget( label54 );
	UxCreateWidget( rowColumn23 );
	UxCreateWidget( toggleButton14 );
	UxCreateWidget( toggleButton15 );
	UxCreateWidget( label44 );
	UxCreateWidget( label55 );
	UxCreateWidget( label56 );
	UxCreateWidget( label57 );
	UxCreateWidget( label58 );
	UxCreateWidget( label59 );
	UxCreateWidget( label60 );
	UxCreateWidget( separator34 );
	UxCreateWidget( label49 );
	UxCreateWidget( label50 );
	UxCreateWidget( separator35 );
	UxCreateWidget( label51 );
	UxCreateWidget( pb_main_search32 );
	UxCreateWidget( label52 );
	UxCreateWidget( tf_alpha1 );
	UxCreateWidget( tf_alpha3 );
	UxCreateWidget( tf_alpha7 );
	UxCreateWidget( tf_alpha44 );

	UxAddCallback( pb_search_search2, XmNactivateCallback,
			activateCB_pb_search_search2,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( pushButton2, XmNactivateCallback,
			activateCB_pushButton2,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( pb_search_plot2, XmNactivateCallback,
			activateCB_pb_search_plot2,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( mn_tol_angstroms2, XmNactivateCallback,
			activateCB_mn_tol_angstroms2,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( mn_tol_pixels2, XmNactivateCallback,
			activateCB_mn_tol_pixels2,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( menu2_p2_b3, XmNactivateCallback,
			activateCB_menu2_p2_b3,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( menu2_p2_b4, XmNactivateCallback,
			activateCB_menu2_p2_b4,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( mn_tol_angstroms3, XmNactivateCallback,
			activateCB_mn_tol_angstroms3,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( mn_tol_pixels3, XmNactivateCallback,
			activateCB_mn_tol_pixels3,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( menu2_p3_b4, XmNactivateCallback,
			activateCB_menu2_p3_b4,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( menu2_p3_b3, XmNactivateCallback,
			activateCB_menu2_p3_b3,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( mn_tol_angstroms5, XmNactivateCallback,
			activateCB_mn_tol_angstroms5,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( mn_tol_pixels5, XmNactivateCallback,
			activateCB_mn_tol_pixels5,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( menu2_p5_b3, XmNactivateCallback,
			activateCB_menu2_p5_b3,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( menu2_p5_b4, XmNactivateCallback,
			activateCB_menu2_p5_b4,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( tf_alpha2, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha2,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( tf_alpha4, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha4,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( tf_alpha5, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha5,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( tf_alpha6, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha6,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( toggleButton14, XmNarmCallback,
			armCB_toggleButton14,
			(XtPointer) UxRotateShellContext );
	UxAddCallback( toggleButton14, XmNvalueChangedCallback,
			valueChangedCB_toggleButton14,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( toggleButton15, XmNarmCallback,
			armCB_toggleButton15,
			(XtPointer) UxRotateShellContext );
	UxAddCallback( toggleButton15, XmNvalueChangedCallback,
			valueChangedCB_toggleButton15,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( pb_main_search32, XmNactivateCallback,
			activateCB_pb_main_search32,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( tf_alpha1, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha1,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( tf_alpha3, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha3,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( tf_alpha7, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha7,
			(XtPointer) UxRotateShellContext );

	UxAddCallback( tf_alpha44, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha44,
			(XtPointer) UxRotateShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( RotateShell );

	return ( RotateShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_RotateShell()
{
	swidget                 rtrn;
	_UxCRotateShell         *UxContext;

	UxRotateShellContext = UxContext =
		(_UxCRotateShell *) UxMalloc( sizeof(_UxCRotateShell) );

	rtrn = _Uxbuild_RotateShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_RotateShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_RotateShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

