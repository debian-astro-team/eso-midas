/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	OrderShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxSepG.h"
#include "UxTogB.h"
#include "UxRowCol.h"
#include "UxTextF.h"
#include "UxLabel.h"
#include "UxForm.h"
#include "UxTranSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxOrderShell;
	swidget	Uxform27;
	swidget	Uxlabel61;
	swidget	Uxtf_ywidth2;
	swidget	Uxlabel62;
	swidget	Uxlabel63;
	swidget	Uxtf_ystep2;
	swidget	Uxtf_width2;
	swidget	UxrowColumn26;
	swidget	Uxrb_seamtd_gaus2;
	swidget	Uxrb_seamtd_grav2;
	swidget	Uxrb_seamtd_maxi2;
	swidget	Uxlabel64;
	swidget	UxseparatorGadget6;
	swidget	UxseparatorGadget7;
	swidget	UxseparatorGadget8;
	swidget	UxseparatorGadget9;
	swidget	Uxtf_thres2;
	swidget	Uxform28;
	swidget	Uxpb_search_search3;
	swidget	UxpushButton3;
	swidget	Uxpb_search_plot3;
	swidget	Uxpb_search_plot4;
	swidget	Uxpb_search_plot5;
	swidget	Uxshelp_search3;
	swidget	Uxseparator22;
	swidget	Uxseparator23;
	swidget	UxseparatorGadget10;
	swidget	Uxlabel65;
	swidget	Uxtf_thres3;
	swidget	Uxlabel66;
	swidget	Uxpb_main_search15;
} _UxCOrderShell;

#define OrderShell              UxOrderShellContext->UxOrderShell
#define form27                  UxOrderShellContext->Uxform27
#define label61                 UxOrderShellContext->Uxlabel61
#define tf_ywidth2              UxOrderShellContext->Uxtf_ywidth2
#define label62                 UxOrderShellContext->Uxlabel62
#define label63                 UxOrderShellContext->Uxlabel63
#define tf_ystep2               UxOrderShellContext->Uxtf_ystep2
#define tf_width2               UxOrderShellContext->Uxtf_width2
#define rowColumn26             UxOrderShellContext->UxrowColumn26
#define rb_seamtd_gaus2         UxOrderShellContext->Uxrb_seamtd_gaus2
#define rb_seamtd_grav2         UxOrderShellContext->Uxrb_seamtd_grav2
#define rb_seamtd_maxi2         UxOrderShellContext->Uxrb_seamtd_maxi2
#define label64                 UxOrderShellContext->Uxlabel64
#define separatorGadget6        UxOrderShellContext->UxseparatorGadget6
#define separatorGadget7        UxOrderShellContext->UxseparatorGadget7
#define separatorGadget8        UxOrderShellContext->UxseparatorGadget8
#define separatorGadget9        UxOrderShellContext->UxseparatorGadget9
#define tf_thres2               UxOrderShellContext->Uxtf_thres2
#define form28                  UxOrderShellContext->Uxform28
#define pb_search_search3       UxOrderShellContext->Uxpb_search_search3
#define pushButton3             UxOrderShellContext->UxpushButton3
#define pb_search_plot3         UxOrderShellContext->Uxpb_search_plot3
#define pb_search_plot4         UxOrderShellContext->Uxpb_search_plot4
#define pb_search_plot5         UxOrderShellContext->Uxpb_search_plot5
#define shelp_search3           UxOrderShellContext->Uxshelp_search3
#define separator22             UxOrderShellContext->Uxseparator22
#define separator23             UxOrderShellContext->Uxseparator23
#define separatorGadget10       UxOrderShellContext->UxseparatorGadget10
#define label65                 UxOrderShellContext->Uxlabel65
#define tf_thres3               UxOrderShellContext->Uxtf_thres3
#define label66                 UxOrderShellContext->Uxlabel66
#define pb_main_search15        UxOrderShellContext->Uxpb_main_search15

static _UxCOrderShell	*UxOrderShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();
extern void  WriteKeyword(), GetExtendedHelp(), SelectList();


extern int RadioSet(), PopupList(), AppendDialogText();

/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

static char	*transTable11 = "#override\n\
<Btn3Down>:ExtendedHelp()\n\
<EnterWindow>:HelpShort()\n\
<LeaveWindow>:ClearShort()\n";

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_OrderShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxOrderShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxOrderShellContext = UxSaveCtx;
}

static void	action_ExtendedHelp( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	DisplayExtendedHelp(UxWidget);
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxOrderShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	losingFocusCB_tf_ywidth2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_ystep2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_width2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_gaus2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	RadioSet ( UxWidget ) ;
	UxOrderShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_gaus2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_grav2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	RadioSet ( UxWidget ) ;
	UxOrderShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_grav2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	armCB_rb_seamtd_maxi2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	RadioSet ( UxWidget ) ;
	UxOrderShellContext = UxSaveCtx;
}

static void	valueChangedCB_rb_seamtd_maxi2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres2( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_search3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	 MidasCommand(UxWidget);
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	activateCB_pushButton3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("OrderShell"));
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot4( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot5( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_thres3( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxOrderShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search15( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCOrderShell          *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxOrderShellContext;
	UxOrderShellContext = UxContext =
			(_UxCOrderShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxOrderShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_OrderShell()
{
	UxPutGeometry( OrderShell, "+10+60" );
	UxPutTitle( OrderShell, "XEchelle: Order Definition" );
	UxPutHeight( OrderShell, 344 );
	UxPutWidth( OrderShell, 500 );
	UxPutY( OrderShell, 107 );
	UxPutX( OrderShell, 845 );

	UxPutBackground( form27, WindowBackground );
	UxPutHeight( form27, 344 );
	UxPutWidth( form27, 500 );
	UxPutY( form27, 2 );
	UxPutX( form27, 4 );
	UxPutUnitType( form27, "pixels" );
	UxPutResizePolicy( form27, "resize_none" );

	UxPutForeground( label61, TextForeground );
	UxPutAlignment( label61, "alignment_beginning" );
	UxPutLabelString( label61, "Threshold:" );
	UxPutFontList( label61, TextFont );
	UxPutBackground( label61, LabelBackground );
	UxPutHeight( label61, 30 );
	UxPutWidth( label61, 170 );
	UxPutY( label61, 134 );
	UxPutX( label61, 18 );

	UxPutForeground( tf_ywidth2, TextForeground );
	UxPutHighlightOnEnter( tf_ywidth2, "true" );
	UxPutFontList( tf_ywidth2, TextFont );
	UxPutBackground( tf_ywidth2, TextBackground );
	UxPutHeight( tf_ywidth2, 34 );
	UxPutWidth( tf_ywidth2, 82 );
	UxPutY( tf_ywidth2, 132 );
	UxPutX( tf_ywidth2, 208 );

	UxPutForeground( label62, TextForeground );
	UxPutAlignment( label62, "alignment_beginning" );
	UxPutLabelString( label62, "Slope:" );
	UxPutFontList( label62, TextFont );
	UxPutBackground( label62, LabelBackground );
	UxPutHeight( label62, 30 );
	UxPutWidth( label62, 170 );
	UxPutY( label62, 170 );
	UxPutX( label62, 18 );

	UxPutForeground( label63, TextForeground );
	UxPutAlignment( label63, "alignment_beginning" );
	UxPutLabelString( label63, "Number of orders:" );
	UxPutFontList( label63, TextFont );
	UxPutBackground( label63, LabelBackground );
	UxPutHeight( label63, 30 );
	UxPutWidth( label63, 170 );
	UxPutY( label63, 58 );
	UxPutX( label63, 18 );

	UxPutForeground( tf_ystep2, TextForeground );
	UxPutHighlightOnEnter( tf_ystep2, "true" );
	UxPutFontList( tf_ystep2, TextFont );
	UxPutBackground( tf_ystep2, TextBackground );
	UxPutHeight( tf_ystep2, 34 );
	UxPutWidth( tf_ystep2, 82 );
	UxPutY( tf_ystep2, 170 );
	UxPutX( tf_ystep2, 208 );

	UxPutForeground( tf_width2, TextForeground );
	UxPutHighlightOnEnter( tf_width2, "true" );
	UxPutFontList( tf_width2, TextFont );
	UxPutBackground( tf_width2, TextBackground );
	UxPutHeight( tf_width2, 34 );
	UxPutWidth( tf_width2, 82 );
	UxPutY( tf_width2, 94 );
	UxPutX( tf_width2, 208 );

	UxPutIsAligned( rowColumn26, "true" );
	UxPutAdjustMargin( rowColumn26, "true" );
	UxPutAdjustLast( rowColumn26, "false" );
	UxPutEntryAlignment( rowColumn26, "alignment_beginning" );
	UxPutBorderWidth( rowColumn26, 0 );
	UxPutShadowThickness( rowColumn26, 0 );
	UxPutLabelString( rowColumn26, "" );
	UxPutEntryBorder( rowColumn26, 0 );
	UxPutBackground( rowColumn26, WindowBackground );
	UxPutRadioBehavior( rowColumn26, "true" );
	UxPutHeight( rowColumn26, 96 );
	UxPutWidth( rowColumn26, 106 );
	UxPutY( rowColumn26, 82 );
	UxPutX( rowColumn26, 332 );

	UxPutTranslations( rb_seamtd_gaus2, transTable11 );
	UxPutForeground( rb_seamtd_gaus2, TextForeground );
	UxPutIndicatorSize( rb_seamtd_gaus2, 16 );
	UxPutHighlightOnEnter( rb_seamtd_gaus2, "true" );
	UxPutSelectColor( rb_seamtd_gaus2, SelectColor );
	UxPutSet( rb_seamtd_gaus2, "true" );
	UxPutLabelString( rb_seamtd_gaus2, "Hough" );
	UxPutFontList( rb_seamtd_gaus2, TextFont );
	UxPutBackground( rb_seamtd_gaus2, WindowBackground );
	UxPutHeight( rb_seamtd_gaus2, 30 );
	UxPutWidth( rb_seamtd_gaus2, 148 );
	UxPutY( rb_seamtd_gaus2, 12 );
	UxPutX( rb_seamtd_gaus2, 12 );

	UxPutTranslations( rb_seamtd_grav2, transTable11 );
	UxPutForeground( rb_seamtd_grav2, TextForeground );
	UxPutIndicatorSize( rb_seamtd_grav2, 16 );
	UxPutHighlightOnEnter( rb_seamtd_grav2, "true" );
	UxPutSelectColor( rb_seamtd_grav2, SelectColor );
	UxPutLabelString( rb_seamtd_grav2, "Standard" );
	UxPutFontList( rb_seamtd_grav2, TextFont );
	UxPutBackground( rb_seamtd_grav2, WindowBackground );
	UxPutHeight( rb_seamtd_grav2, 30 );
	UxPutWidth( rb_seamtd_grav2, 148 );
	UxPutY( rb_seamtd_grav2, 85 );
	UxPutX( rb_seamtd_grav2, 5 );

	UxPutTranslations( rb_seamtd_maxi2, transTable11 );
	UxPutForeground( rb_seamtd_maxi2, TextForeground );
	UxPutIndicatorSize( rb_seamtd_maxi2, 16 );
	UxPutHighlightOnEnter( rb_seamtd_maxi2, "true" );
	UxPutSelectColor( rb_seamtd_maxi2, SelectColor );
	UxPutLabelString( rb_seamtd_maxi2, "Complement" );
	UxPutFontList( rb_seamtd_maxi2, TextFont );
	UxPutBackground( rb_seamtd_maxi2, WindowBackground );
	UxPutHeight( rb_seamtd_maxi2, 24 );
	UxPutWidth( rb_seamtd_maxi2, 96 );
	UxPutY( rb_seamtd_maxi2, 66 );
	UxPutX( rb_seamtd_maxi2, 4 );

	UxPutForeground( label64, TextForeground );
	UxPutAlignment( label64, "alignment_center" );
	UxPutLabelString( label64, "Method" );
	UxPutFontList( label64, TextFont );
	UxPutBackground( label64, LabelBackground );
	UxPutHeight( label64, 30 );
	UxPutWidth( label64, 106 );
	UxPutY( label64, 52 );
	UxPutX( label64, 336 );

	UxPutOrientation( separatorGadget6, "vertical" );
	UxPutHeight( separatorGadget6, 120 );
	UxPutWidth( separatorGadget6, 12 );
	UxPutY( separatorGadget6, 66 );
	UxPutX( separatorGadget6, 450 );

	UxPutOrientation( separatorGadget7, "vertical" );
	UxPutHeight( separatorGadget7, 120 );
	UxPutWidth( separatorGadget7, 12 );
	UxPutY( separatorGadget7, 66 );
	UxPutX( separatorGadget7, 314 );

	UxPutHeight( separatorGadget8, 10 );
	UxPutWidth( separatorGadget8, 16 );
	UxPutY( separatorGadget8, 64 );
	UxPutX( separatorGadget8, 318 );

	UxPutHeight( separatorGadget9, 10 );
	UxPutWidth( separatorGadget9, 14 );
	UxPutY( separatorGadget9, 64 );
	UxPutX( separatorGadget9, 442 );

	UxPutForeground( tf_thres2, TextForeground );
	UxPutHighlightOnEnter( tf_thres2, "true" );
	UxPutFontList( tf_thres2, TextFont );
	UxPutBackground( tf_thres2, TextBackground );
	UxPutHeight( tf_thres2, 34 );
	UxPutWidth( tf_thres2, 82 );
	UxPutY( tf_thres2, 56 );
	UxPutX( tf_thres2, 208 );

	UxPutBackground( form28, ButtonBackground );
	UxPutHeight( form28, 40 );
	UxPutWidth( form28, 490 );
	UxPutY( form28, 282 );
	UxPutX( form28, 2 );
	UxPutResizePolicy( form28, "resize_none" );

	UxPutLabelString( pb_search_search3, "Define" );
	UxPutForeground( pb_search_search3, ApplyForeground );
	UxPutFontList( pb_search_search3, BoldTextFont );
	UxPutBackground( pb_search_search3, ButtonBackground );
	UxPutHeight( pb_search_search3, 30 );
	UxPutWidth( pb_search_search3, 86 );
	UxPutY( pb_search_search3, 4 );
	UxPutX( pb_search_search3, 8 );

	UxPutLabelString( pushButton3, "Cancel" );
	UxPutForeground( pushButton3, CancelForeground );
	UxPutFontList( pushButton3, BoldTextFont );
	UxPutBackground( pushButton3, ButtonBackground );
	UxPutHeight( pushButton3, 30 );
	UxPutWidth( pushButton3, 86 );
	UxPutY( pushButton3, 6 );
	UxPutX( pushButton3, 392 );

	UxPutLabelString( pb_search_plot3, "Load Orders" );
	UxPutForeground( pb_search_plot3, ButtonForeground );
	UxPutFontList( pb_search_plot3, BoldTextFont );
	UxPutBackground( pb_search_plot3, ButtonBackground );
	UxPutHeight( pb_search_plot3, 30 );
	UxPutWidth( pb_search_plot3, 100 );
	UxPutY( pb_search_plot3, 4 );
	UxPutX( pb_search_plot3, 102 );

	UxPutLabelString( pb_search_plot4, "Save..." );
	UxPutForeground( pb_search_plot4, ButtonForeground );
	UxPutFontList( pb_search_plot4, BoldTextFont );
	UxPutBackground( pb_search_plot4, ButtonBackground );
	UxPutHeight( pb_search_plot4, 30 );
	UxPutWidth( pb_search_plot4, 86 );
	UxPutY( pb_search_plot4, 4 );
	UxPutX( pb_search_plot4, 214 );

	UxPutLabelString( pb_search_plot5, "Help..." );
	UxPutForeground( pb_search_plot5, ButtonForeground );
	UxPutFontList( pb_search_plot5, BoldTextFont );
	UxPutBackground( pb_search_plot5, ButtonBackground );
	UxPutHeight( pb_search_plot5, 30 );
	UxPutWidth( pb_search_plot5, 86 );
	UxPutY( pb_search_plot5, 4 );
	UxPutX( pb_search_plot5, 304 );

	UxPutFontList( shelp_search3, TextFont );
	UxPutEditable( shelp_search3, "false" );
	UxPutCursorPositionVisible( shelp_search3, "false" );
	UxPutBackground( shelp_search3, SHelpBackground );
	UxPutHeight( shelp_search3, 50 );
	UxPutWidth( shelp_search3, 484 );
	UxPutY( shelp_search3, 222 );
	UxPutX( shelp_search3, 4 );

	UxPutBackground( separator22, WindowBackground );
	UxPutHeight( separator22, 10 );
	UxPutWidth( separator22, 492 );
	UxPutY( separator22, 212 );
	UxPutX( separator22, 2 );

	UxPutBackground( separator23, WindowBackground );
	UxPutHeight( separator23, 10 );
	UxPutWidth( separator23, 492 );
	UxPutY( separator23, 270 );
	UxPutX( separator23, 0 );

	UxPutHeight( separatorGadget10, 10 );
	UxPutWidth( separatorGadget10, 138 );
	UxPutY( separatorGadget10, 180 );
	UxPutX( separatorGadget10, 318 );

	UxPutForeground( label65, TextForeground );
	UxPutAlignment( label65, "alignment_beginning" );
	UxPutLabelString( label65, "Order width:" );
	UxPutFontList( label65, TextFont );
	UxPutBackground( label65, LabelBackground );
	UxPutHeight( label65, 30 );
	UxPutWidth( label65, 170 );
	UxPutY( label65, 94 );
	UxPutX( label65, 18 );

	UxPutForeground( tf_thres3, TextForeground );
	UxPutHighlightOnEnter( tf_thres3, "true" );
	UxPutFontList( tf_thres3, TextFont );
	UxPutBackground( tf_thres3, TextBackground );
	UxPutHeight( tf_thres3, 34 );
	UxPutWidth( tf_thres3, 212 );
	UxPutY( tf_thres3, 12 );
	UxPutX( tf_thres3, 208 );

	UxPutForeground( label66, TextForeground );
	UxPutAlignment( label66, "alignment_beginning" );
	UxPutLabelString( label66, "Order Definition Frame:" );
	UxPutFontList( label66, TextFont );
	UxPutBackground( label66, LabelBackground );
	UxPutHeight( label66, 30 );
	UxPutWidth( label66, 170 );
	UxPutY( label66, 14 );
	UxPutX( label66, 20 );

	UxPutWidth( pb_main_search15, 46 );
	UxPutRecomputeSize( pb_main_search15, "true" );
	UxPutLabelString( pb_main_search15, "..." );
	UxPutForeground( pb_main_search15, ButtonForeground );
	UxPutFontList( pb_main_search15, BoldTextFont );
	UxPutBackground( pb_main_search15, ButtonBackground );
	UxPutHeight( pb_main_search15, 28 );
	UxPutY( pb_main_search15, 14 );
	UxPutX( pb_main_search15, 426 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_OrderShell()
{
	/* Create the swidgets */

	OrderShell = UxCreateTransientShell( "OrderShell", NO_PARENT );
	UxPutContext( OrderShell, UxOrderShellContext );

	form27 = UxCreateForm( "form27", OrderShell );
	label61 = UxCreateLabel( "label61", form27 );
	tf_ywidth2 = UxCreateTextField( "tf_ywidth2", form27 );
	label62 = UxCreateLabel( "label62", form27 );
	label63 = UxCreateLabel( "label63", form27 );
	tf_ystep2 = UxCreateTextField( "tf_ystep2", form27 );
	tf_width2 = UxCreateTextField( "tf_width2", form27 );
	rowColumn26 = UxCreateRowColumn( "rowColumn26", form27 );
	rb_seamtd_gaus2 = UxCreateToggleButton( "rb_seamtd_gaus2", rowColumn26 );
	rb_seamtd_grav2 = UxCreateToggleButton( "rb_seamtd_grav2", rowColumn26 );
	rb_seamtd_maxi2 = UxCreateToggleButton( "rb_seamtd_maxi2", rowColumn26 );
	label64 = UxCreateLabel( "label64", form27 );
	separatorGadget6 = UxCreateSeparatorGadget( "separatorGadget6", form27 );
	separatorGadget7 = UxCreateSeparatorGadget( "separatorGadget7", form27 );
	separatorGadget8 = UxCreateSeparatorGadget( "separatorGadget8", form27 );
	separatorGadget9 = UxCreateSeparatorGadget( "separatorGadget9", form27 );
	tf_thres2 = UxCreateTextField( "tf_thres2", form27 );
	form28 = UxCreateForm( "form28", form27 );
	pb_search_search3 = UxCreatePushButton( "pb_search_search3", form28 );
	pushButton3 = UxCreatePushButton( "pushButton3", form28 );
	pb_search_plot3 = UxCreatePushButton( "pb_search_plot3", form28 );
	pb_search_plot4 = UxCreatePushButton( "pb_search_plot4", form28 );
	pb_search_plot5 = UxCreatePushButton( "pb_search_plot5", form28 );
	shelp_search3 = UxCreateText( "shelp_search3", form27 );
	separator22 = UxCreateSeparator( "separator22", form27 );
	separator23 = UxCreateSeparator( "separator23", form27 );
	separatorGadget10 = UxCreateSeparatorGadget( "separatorGadget10", form27 );
	label65 = UxCreateLabel( "label65", form27 );
	tf_thres3 = UxCreateTextField( "tf_thres3", form27 );
	label66 = UxCreateLabel( "label66", form27 );
	pb_main_search15 = UxCreatePushButton( "pb_main_search15", form27 );

	_Uxinit_OrderShell();

	/* Create the X widgets */

	UxCreateWidget( OrderShell );
	UxCreateWidget( form27 );
	UxCreateWidget( label61 );
	UxCreateWidget( tf_ywidth2 );
	UxCreateWidget( label62 );
	UxCreateWidget( label63 );
	UxCreateWidget( tf_ystep2 );
	UxCreateWidget( tf_width2 );
	UxCreateWidget( rowColumn26 );
	UxCreateWidget( rb_seamtd_gaus2 );
	UxCreateWidget( rb_seamtd_grav2 );
	UxCreateWidget( rb_seamtd_maxi2 );
	UxCreateWidget( label64 );
	UxCreateWidget( separatorGadget6 );
	UxCreateWidget( separatorGadget7 );
	UxCreateWidget( separatorGadget8 );
	UxCreateWidget( separatorGadget9 );
	UxCreateWidget( tf_thres2 );
	UxCreateWidget( form28 );
	UxCreateWidget( pb_search_search3 );
	UxCreateWidget( pushButton3 );
	UxCreateWidget( pb_search_plot3 );
	UxCreateWidget( pb_search_plot4 );
	UxCreateWidget( pb_search_plot5 );
	UxCreateWidget( shelp_search3 );
	UxCreateWidget( separator22 );
	UxCreateWidget( separator23 );
	UxCreateWidget( separatorGadget10 );
	UxCreateWidget( label65 );
	UxCreateWidget( tf_thres3 );
	UxCreateWidget( label66 );
	UxCreateWidget( pb_main_search15 );

	UxAddCallback( tf_ywidth2, XmNlosingFocusCallback,
			losingFocusCB_tf_ywidth2,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( tf_ystep2, XmNlosingFocusCallback,
			losingFocusCB_tf_ystep2,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( tf_width2, XmNlosingFocusCallback,
			losingFocusCB_tf_width2,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( rb_seamtd_gaus2, XmNarmCallback,
			armCB_rb_seamtd_gaus2,
			(XtPointer) UxOrderShellContext );
	UxAddCallback( rb_seamtd_gaus2, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_gaus2,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( rb_seamtd_grav2, XmNarmCallback,
			armCB_rb_seamtd_grav2,
			(XtPointer) UxOrderShellContext );
	UxAddCallback( rb_seamtd_grav2, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_grav2,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( rb_seamtd_maxi2, XmNarmCallback,
			armCB_rb_seamtd_maxi2,
			(XtPointer) UxOrderShellContext );
	UxAddCallback( rb_seamtd_maxi2, XmNvalueChangedCallback,
			valueChangedCB_rb_seamtd_maxi2,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( tf_thres2, XmNlosingFocusCallback,
			losingFocusCB_tf_thres2,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( pb_search_search3, XmNactivateCallback,
			activateCB_pb_search_search3,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( pushButton3, XmNactivateCallback,
			activateCB_pushButton3,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( pb_search_plot3, XmNactivateCallback,
			activateCB_pb_search_plot3,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( pb_search_plot4, XmNactivateCallback,
			activateCB_pb_search_plot4,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( pb_search_plot5, XmNactivateCallback,
			activateCB_pb_search_plot5,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( tf_thres3, XmNlosingFocusCallback,
			losingFocusCB_tf_thres3,
			(XtPointer) UxOrderShellContext );

	UxAddCallback( pb_main_search15, XmNactivateCallback,
			activateCB_pb_main_search15,
			(XtPointer) UxOrderShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( OrderShell );

	return ( OrderShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_OrderShell()
{
	swidget                 rtrn;
	_UxCOrderShell          *UxContext;

	UxOrderShellContext = UxContext =
		(_UxCOrderShell *) UxMalloc( sizeof(_UxCOrderShell) );

	rtrn = _Uxbuild_OrderShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_OrderShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "ExtendedHelp", action_ExtendedHelp },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_OrderShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

