/* @(#)file_selection.c	19.1 (ES0-DMD) 02/25/03 13:45:59 */
/*===========================================================================
  Copyright (C) 1995 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetss Ave, Cambridge, 
  MA 02139, USA.
 
  Corresponding concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	file_selection.c

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxFsBox.h"
#include "UxApplSh.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	Uxfile_selection;
	swidget	Uxfb_file_selection;
} _UxCfile_selection;

#define file_selection          UxFile_selectionContext->Uxfile_selection
#define fb_file_selection       UxFile_selectionContext->Uxfb_file_selection

static _UxCfile_selection	*UxFile_selectionContext;


/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_file_selection();

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	cancelCB_fb_file_selection( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_selection      *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_selectionContext;
	UxFile_selectionContext = UxContext =
			(_UxCfile_selection *) UxGetContext( UxThisWidget );
	{
	extern swidget FileListInterface;
	UxPopdownInterface (FileListInterface);
	}
	UxFile_selectionContext = UxSaveCtx;
}

static void	okCallback_fb_file_selection( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCfile_selection      *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFile_selectionContext;
	UxFile_selectionContext = UxContext =
			(_UxCfile_selection *) UxGetContext( UxThisWidget );
	{
	char s[128];
	extern swidget TextFieldSwidget;
	extern int FillTextField;
	
	strcpy(s, UxGetTextString(UxThisWidget)); 
	if ( FillTextField )
	    UxPutValue(TextFieldSwidget, s);
	UxPopdownInterface(UxThisWidget);
	}
	UxFile_selectionContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_file_selection()
{
	UxPutBackground( file_selection, ApplicBackground );
	UxPutKeyboardFocusPolicy( file_selection, "pointer" );
	UxPutDefaultFontList( file_selection, SmallFont );
	UxPutTitle( file_selection, "List selection" );
	UxPutIconName( file_selection, "List selection" );
	UxPutGeometry( file_selection, "+100+100" );
	UxPutHeight( file_selection, 320 );
	UxPutWidth( file_selection, 346 );
	UxPutY( file_selection, 388 );
	UxPutX( file_selection, 612 );

	UxPutListVisibleItemCount( fb_file_selection, 3 );
	UxPutMinimizeButtons( fb_file_selection, "false" );
	UxPutLabelFontList( fb_file_selection, BoldTextFont );
	UxPutTextFontList( fb_file_selection, TextFont );
	UxPutButtonFontList( fb_file_selection, BoldTextFont );
	UxPutForeground( fb_file_selection, TextForeground );
	UxPutBackground( fb_file_selection, WindowBackground );
	UxPutHeight( fb_file_selection, 300 );
	UxPutWidth( fb_file_selection, 345 );
	UxPutUnitType( fb_file_selection, "pixels" );
	UxPutResizePolicy( fb_file_selection, "resize_none" );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_file_selection()
{
	/* Create the swidgets */

	file_selection = UxCreateApplicationShell( "file_selection", NO_PARENT );
	UxPutContext( file_selection, UxFile_selectionContext );

	fb_file_selection = UxCreateFileSelectionBox( "fb_file_selection", file_selection );

	_Uxinit_file_selection();

	/* Create the X widgets */

	UxCreateWidget( file_selection );
	UxCreateWidget( fb_file_selection );

	UxAddCallback( fb_file_selection, XmNcancelCallback,
			cancelCB_fb_file_selection,
			(XtPointer) UxFile_selectionContext );
	UxAddCallback( fb_file_selection, XmNokCallback,
			okCallback_fb_file_selection,
			(XtPointer) UxFile_selectionContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( file_selection );

	return ( file_selection );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_file_selection()
{
	swidget                 rtrn;
	_UxCfile_selection      *UxContext;

	UxFile_selectionContext = UxContext =
		(_UxCfile_selection *) UxMalloc( sizeof(_UxCfile_selection) );

	rtrn = _Uxbuild_file_selection();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_file_selection()
{
	swidget			_Uxrtrn;

	_Uxrtrn = _Ux_create_file_selection();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

