/*===========================================================================
  Copyright (C) 1995-2009 European Southern Observatory (ESO)
 
  This program is free software; you can redistribute it and/or 
  modify it under the terms of the GNU General Public License as 
  published by the Free Software Foundation; either version 2 of 
  the License, or (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public 
  License along with this program; if not, write to the Free 
  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge, 
  MA 02139, USA.
 
  Correspondence concerning ESO-MIDAS should be addressed as follows:
	Internet e-mail: midas@eso.org
	Postal address: European Southern Observatory
			Data Management Division 
			Karl-Schwarzschild-Strasse 2
			D 85748 Garching bei Muenchen 
			GERMANY
===========================================================================*/


/*******************************************************************************
	FlatShell.c

.VERSION
 090825         last modif

*******************************************************************************/

#include <stdio.h>
#include "UxLib.h"
#include "UxLabel.h"
#include "UxTextF.h"
#include "UxSep.h"
#include "UxText.h"
#include "UxPushB.h"
#include "UxForm.h"
#include "UxTranSh.h"

#include "proto_xech.h"

/*******************************************************************************
	Includes, Defines, and Global variables from the Declarations Editor:
*******************************************************************************/

#include <ExternResources.h>

/*******************************************************************************
	The definition of the context structure:
	If you create multiple instances of your interface, the context
	structure ensures that your callbacks use the variables for the
	correct instance.

	For each swidget in the interface, each argument to the Interface
	function, and each variable in the Instance Specific section of the
	Declarations Editor, there is an entry in the context structure.
	and a #define.  The #define makes the variable name refer to the
	corresponding entry in the context structure.
*******************************************************************************/

typedef	struct
{
	swidget	UxFlatShell;
	swidget	Uxform37;
	swidget	Uxform38;
	swidget	Uxpb_search_search10;
	swidget	UxpushButton7;
	swidget	Uxpb_search_plot21;
	swidget	Uxpb_search_plot24;
	swidget	Uxshelp_search7;
	swidget	Uxseparator30;
	swidget	Uxseparator31;
	swidget	Uxtf_alpha35;
	swidget	Uxtf_alpha36;
	swidget	Uxtf_alpha37;
	swidget	Uxlabel114;
	swidget	Uxpb_main_search25;
	swidget	Uxpb_main_search26;
	swidget	Uxpb_main_search27;
	swidget	Uxlabel115;
	swidget	Uxlabel116;
	swidget	Uxpb_search_plot26;
	swidget	Uxpb_search_plot27;
	swidget	Uxtf_alpha11;
	swidget	Uxlabel6;
} _UxCFlatShell;

#define FlatShell               UxFlatShellContext->UxFlatShell
#define form37                  UxFlatShellContext->Uxform37
#define form38                  UxFlatShellContext->Uxform38
#define pb_search_search10      UxFlatShellContext->Uxpb_search_search10
#define pushButton7             UxFlatShellContext->UxpushButton7
#define pb_search_plot21        UxFlatShellContext->Uxpb_search_plot21
#define pb_search_plot24        UxFlatShellContext->Uxpb_search_plot24
#define shelp_search7           UxFlatShellContext->Uxshelp_search7
#define separator30             UxFlatShellContext->Uxseparator30
#define separator31             UxFlatShellContext->Uxseparator31
#define tf_alpha35              UxFlatShellContext->Uxtf_alpha35
#define tf_alpha36              UxFlatShellContext->Uxtf_alpha36
#define tf_alpha37              UxFlatShellContext->Uxtf_alpha37
#define label114                UxFlatShellContext->Uxlabel114
#define pb_main_search25        UxFlatShellContext->Uxpb_main_search25
#define pb_main_search26        UxFlatShellContext->Uxpb_main_search26
#define pb_main_search27        UxFlatShellContext->Uxpb_main_search27
#define label115                UxFlatShellContext->Uxlabel115
#define label116                UxFlatShellContext->Uxlabel116
#define pb_search_plot26        UxFlatShellContext->Uxpb_search_plot26
#define pb_search_plot27        UxFlatShellContext->Uxpb_search_plot27
#define tf_alpha11              UxFlatShellContext->Uxtf_alpha11
#define label6                  UxFlatShellContext->Uxlabel6

static _UxCFlatShell	*UxFlatShellContext;

extern void DisplayShortHelp(), DisplayExtendedHelp(), SetFileList();
extern void MidasCommand(), PopupLong(),  InitAllFields();
extern void  WidgetLeave(), WidgetEnter();
extern void  WriteKeyword(), GetExtendedHelp(), SelectList();


/*******************************************************************************
	The following are translation tables.
*******************************************************************************/

/*******************************************************************************
	Forward declarations of functions that are defined later in this file.
*******************************************************************************/

swidget	create_FlatShell();

/*******************************************************************************
	The following are Action functions.
*******************************************************************************/

static void	action_UpdateDirectory( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	
	 char node[80];
	
	 /* if (XmTextGetSelection(UxWidget) == NULL) return; */
	 strcpy(node,
	     XmTextGetSelection(UxWidget));
	 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));
	
	node[strlen(node)-1] = '\0';
	WGet_all_dirs(node);
	WChange_Midas_dir();
	WGet_all_files();
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	action_ClearShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{WidgetLeave(UxWidget);}
	UxFlatShellContext = UxSaveCtx;
}

static void	action_HelpShort( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{WidgetEnter(UxWidget);}
	UxFlatShellContext = UxSaveCtx;
}

static void	action_FileSelectACT( UxWidget, UxEvent, UxParams, p_UxNumParams )
	Widget		UxWidget;
	XEvent		*UxEvent;
	String		*UxParams;
	Cardinal	*p_UxNumParams;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
#include <xm_defs.h>
#include <spec_comm.h>
	
	extern swidget FileListInterface, TextFieldSwidget;
	extern Widget FileListWidget;
	extern char  DirSpecs[];
	extern int ListType;
	
	int  strip = 1; /* strip off */
	
	TextFieldSwidget = UxThisWidget;
	
	if ( UxWidget == UxGetWidget(UxFindSwidget("tf_lincat")) ) {
	    SET_LIST_TITLE("Enter line catalog");
	    ListType = LIST_LINCAT;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_guess")) ) {
	    SET_LIST_TITLE("Enter guess table");
	    ListType = LIST_GUESS;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_flux_tbl")) ) {
	    SET_LIST_TITLE("Enter flux table");
	    ListType = LIST_FLUX_TBL;
	}
	else if ( UxWidget == UxGetWidget(UxFindSwidget("tf_extin_tbl")) )  {
	    SET_LIST_TITLE("Enter extinction table");
	    ListType = LIST_EXTIN_TBL;
	}
	
	strcpy(DirSpecs, "*.tbl");	
	SetFileList(FileListWidget, strip, DirSpecs);
	UxPopupInterface(FileListInterface, exclusive_grab);
	
	}
	UxFlatShellContext = UxSaveCtx;
}

/*******************************************************************************
	The following are callback functions.
*******************************************************************************/

static void	activateCB_pb_search_search10( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	activateCB_pushButton7( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	UxPopdownInterface(UxFindSwidget("FlatShell"));
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot21( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	GetExtendedHelp(UxWidget);
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot24( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha35( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha36( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha37( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search25( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search26( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	activateCB_pb_main_search27( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	SelectList(UxWidget);
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot26( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	activateCB_pb_search_plot27( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	MidasCommand(UxWidget);
	
	}
	UxFlatShellContext = UxSaveCtx;
}

static void	losingFocusCB_tf_alpha11( UxWidget, UxClientData, UxCallbackArg )
	Widget		UxWidget;
	XtPointer	UxClientData, UxCallbackArg;
{
	_UxCFlatShell           *UxSaveCtx, *UxContext;
	swidget			UxThisWidget;

	UxThisWidget = UxWidgetToSwidget( UxWidget );
	UxSaveCtx = UxFlatShellContext;
	UxFlatShellContext = UxContext =
			(_UxCFlatShell *) UxGetContext( UxThisWidget );
	{
	
	}
	UxFlatShellContext = UxSaveCtx;
}

/*******************************************************************************
	The 'init_' function sets the private properties for all the
	swidgets to the values specified in the Property Table.
	Some properties need to be set after the X widgets have been
	created and the setting of these properties is done in the
	'build_' function after the UxCreateWidget call.
*******************************************************************************/

static void	_Uxinit_FlatShell()
{
	UxPutGeometry( FlatShell, "+10+60" );
	UxPutHeight( FlatShell, 300 );
	UxPutWidth( FlatShell, 530 );
	UxPutY( FlatShell, 574 );
	UxPutX( FlatShell, 484 );

	UxPutBackground( form37, WindowBackground );
	UxPutHeight( form37, 348 );
	UxPutWidth( form37, 408 );
	UxPutY( form37, -2 );
	UxPutX( form37, 0 );
	UxPutUnitType( form37, "pixels" );
	UxPutResizePolicy( form37, "resize_none" );

	UxPutBackground( form38, ButtonBackground );
	UxPutHeight( form38, 50 );
	UxPutWidth( form38, 508 );
	UxPutY( form38, 240 );
	UxPutX( form38, 10 );
	UxPutResizePolicy( form38, "resize_none" );

	UxPutLabelString( pb_search_search10, "Fit and Subtract" );
	UxPutForeground( pb_search_search10, ApplyForeground );
	UxPutFontList( pb_search_search10, BoldTextFont );
	UxPutBackground( pb_search_search10, ButtonBackground );
	UxPutHeight( pb_search_search10, 30 );
	UxPutWidth( pb_search_search10, 150 );
	UxPutY( pb_search_search10, 10 );
	UxPutX( pb_search_search10, 16 );

	UxPutLabelString( pushButton7, "Cancel" );
	UxPutForeground( pushButton7, CancelForeground );
	UxPutFontList( pushButton7, BoldTextFont );
	UxPutBackground( pushButton7, ButtonBackground );
	UxPutHeight( pushButton7, 30 );
	UxPutWidth( pushButton7, 86 );
	UxPutY( pushButton7, 8 );
	UxPutX( pushButton7, 412 );

	UxPutLabelString( pb_search_plot21, "Help..." );
	UxPutForeground( pb_search_plot21, ButtonForeground );
	UxPutFontList( pb_search_plot21, BoldTextFont );
	UxPutBackground( pb_search_plot21, ButtonBackground );
	UxPutHeight( pb_search_plot21, 30 );
	UxPutWidth( pb_search_plot21, 86 );
	UxPutY( pb_search_plot21, 10 );
	UxPutX( pb_search_plot21, 320 );

	UxPutLabelString( pb_search_plot24, "Plot Blaze" );
	UxPutForeground( pb_search_plot24, ButtonForeground );
	UxPutFontList( pb_search_plot24, BoldTextFont );
	UxPutBackground( pb_search_plot24, ButtonBackground );
	UxPutHeight( pb_search_plot24, 30 );
	UxPutWidth( pb_search_plot24, 106 );
	UxPutY( pb_search_plot24, 10 );
	UxPutX( pb_search_plot24, 202 );

	UxPutFontList( shelp_search7, TextFont );
	UxPutEditable( shelp_search7, "false" );
	UxPutCursorPositionVisible( shelp_search7, "false" );
	UxPutBackground( shelp_search7, SHelpBackground );
	UxPutHeight( shelp_search7, 50 );
	UxPutWidth( shelp_search7, 508 );
	UxPutY( shelp_search7, 182 );
	UxPutX( shelp_search7, 8 );

	UxPutBackground( separator30, WindowBackground );
	UxPutHeight( separator30, 10 );
	UxPutWidth( separator30, 514 );
	UxPutY( separator30, 232 );
	UxPutX( separator30, 8 );

	UxPutBackground( separator31, WindowBackground );
	UxPutHeight( separator31, 10 );
	UxPutWidth( separator31, 518 );
	UxPutY( separator31, 168 );
	UxPutX( separator31, 8 );

	UxPutForeground( tf_alpha35, TextForeground );
	UxPutHighlightOnEnter( tf_alpha35, "true" );
	UxPutFontList( tf_alpha35, TextFont );
	UxPutBackground( tf_alpha35, TextBackground );
	UxPutHeight( tf_alpha35, 32 );
	UxPutWidth( tf_alpha35, 230 );
	UxPutY( tf_alpha35, 8 );
	UxPutX( tf_alpha35, 130 );

	UxPutText( tf_alpha36, "middummff.bdf" );
	UxPutForeground( tf_alpha36, TextForeground );
	UxPutHighlightOnEnter( tf_alpha36, "true" );
	UxPutFontList( tf_alpha36, TextFont );
	UxPutBackground( tf_alpha36, TextBackground );
	UxPutHeight( tf_alpha36, 32 );
	UxPutWidth( tf_alpha36, 230 );
	UxPutY( tf_alpha36, 46 );
	UxPutX( tf_alpha36, 130 );

	UxPutForeground( tf_alpha37, TextForeground );
	UxPutHighlightOnEnter( tf_alpha37, "true" );
	UxPutFontList( tf_alpha37, TextFont );
	UxPutBackground( tf_alpha37, TextBackground );
	UxPutHeight( tf_alpha37, 32 );
	UxPutWidth( tf_alpha37, 230 );
	UxPutY( tf_alpha37, 90 );
	UxPutX( tf_alpha37, 130 );

	UxPutForeground( label114, TextForeground );
	UxPutAlignment( label114, "alignment_beginning" );
	UxPutLabelString( label114, "Input Frame" );
	UxPutFontList( label114, TextFont );
	UxPutBackground( label114, LabelBackground );
	UxPutHeight( label114, 30 );
	UxPutWidth( label114, 94 );
	UxPutY( label114, 10 );
	UxPutX( label114, 18 );

	UxPutWidth( pb_main_search25, 46 );
	UxPutRecomputeSize( pb_main_search25, "true" );
	UxPutLabelString( pb_main_search25, "..." );
	UxPutForeground( pb_main_search25, ButtonForeground );
	UxPutFontList( pb_main_search25, BoldTextFont );
	UxPutBackground( pb_main_search25, ButtonBackground );
	UxPutHeight( pb_main_search25, 28 );
	UxPutY( pb_main_search25, 10 );
	UxPutX( pb_main_search25, 365 );

	UxPutWidth( pb_main_search26, 46 );
	UxPutRecomputeSize( pb_main_search26, "true" );
	UxPutLabelString( pb_main_search26, "..." );
	UxPutForeground( pb_main_search26, ButtonForeground );
	UxPutFontList( pb_main_search26, BoldTextFont );
	UxPutBackground( pb_main_search26, ButtonBackground );
	UxPutHeight( pb_main_search26, 28 );
	UxPutY( pb_main_search26, 48 );
	UxPutX( pb_main_search26, 365 );

	UxPutWidth( pb_main_search27, 46 );
	UxPutRecomputeSize( pb_main_search27, "true" );
	UxPutLabelString( pb_main_search27, "..." );
	UxPutForeground( pb_main_search27, ButtonForeground );
	UxPutFontList( pb_main_search27, BoldTextFont );
	UxPutBackground( pb_main_search27, ButtonBackground );
	UxPutHeight( pb_main_search27, 28 );
	UxPutY( pb_main_search27, 88 );
	UxPutX( pb_main_search27, 365 );

	UxPutForeground( label115, TextForeground );
	UxPutAlignment( label115, "alignment_beginning" );
	UxPutLabelString( label115, "- Background" );
	UxPutFontList( label115, TextFont );
	UxPutBackground( label115, LabelBackground );
	UxPutHeight( label115, 30 );
	UxPutWidth( label115, 94 );
	UxPutY( label115, 42 );
	UxPutX( label115, 4 );

	UxPutForeground( label116, TextForeground );
	UxPutAlignment( label116, "alignment_beginning" );
	UxPutLabelString( label116, "= Output Frame" );
	UxPutFontList( label116, TextFont );
	UxPutBackground( label116, LabelBackground );
	UxPutHeight( label116, 30 );
	UxPutWidth( label116, 116 );
	UxPutY( label116, 86 );
	UxPutX( label116, 6 );

	UxPutLabelString( pb_search_plot26, "Display" );
	UxPutForeground( pb_search_plot26, ButtonForeground );
	UxPutFontList( pb_search_plot26, BoldTextFont );
	UxPutBackground( pb_search_plot26, ButtonBackground );
	UxPutHeight( pb_search_plot26, 30 );
	UxPutWidth( pb_search_plot26, 86 );
	UxPutY( pb_search_plot26, 10 );
	UxPutX( pb_search_plot26, 420 );

	UxPutLabelString( pb_search_plot27, "Display" );
	UxPutForeground( pb_search_plot27, ButtonForeground );
	UxPutFontList( pb_search_plot27, BoldTextFont );
	UxPutBackground( pb_search_plot27, ButtonBackground );
	UxPutHeight( pb_search_plot27, 30 );
	UxPutWidth( pb_search_plot27, 86 );
	UxPutY( pb_search_plot27, 48 );
	UxPutX( pb_search_plot27, 420 );

	UxPutForeground( tf_alpha11, TextForeground );
	UxPutHighlightOnEnter( tf_alpha11, "true" );
	UxPutFontList( tf_alpha11, TextFont );
	UxPutBackground( tf_alpha11, TextBackground );
	UxPutHeight( tf_alpha11, 32 );
	UxPutWidth( tf_alpha11, 230 );
	UxPutY( tf_alpha11, 130 );
	UxPutX( tf_alpha11, 130 );

	UxPutForeground( label6, TextForeground );
	UxPutAlignment( label6, "alignment_beginning" );
	UxPutLabelString( label6, "Blaze Function:" );
	UxPutFontList( label6, TextFont );
	UxPutBackground( label6, LabelBackground );
	UxPutHeight( label6, 30 );
	UxPutWidth( label6, 116 );
	UxPutY( label6, 128 );
	UxPutX( label6, 8 );

}

/*******************************************************************************
	The 'build_' function creates all the swidgets and X widgets,
	and sets their properties to the values specified in the
	Property Editor.
*******************************************************************************/

static swidget	_Uxbuild_FlatShell()
{
	/* Create the swidgets */

	FlatShell = UxCreateTransientShell( "FlatShell", NO_PARENT );
	UxPutContext( FlatShell, UxFlatShellContext );

	form37 = UxCreateForm( "form37", FlatShell );
	form38 = UxCreateForm( "form38", form37 );
	pb_search_search10 = UxCreatePushButton( "pb_search_search10", form38 );
	pushButton7 = UxCreatePushButton( "pushButton7", form38 );
	pb_search_plot21 = UxCreatePushButton( "pb_search_plot21", form38 );
	pb_search_plot24 = UxCreatePushButton( "pb_search_plot24", form38 );
	shelp_search7 = UxCreateText( "shelp_search7", form37 );
	separator30 = UxCreateSeparator( "separator30", form37 );
	separator31 = UxCreateSeparator( "separator31", form37 );
	tf_alpha35 = UxCreateTextField( "tf_alpha35", form37 );
	tf_alpha36 = UxCreateTextField( "tf_alpha36", form37 );
	tf_alpha37 = UxCreateTextField( "tf_alpha37", form37 );
	label114 = UxCreateLabel( "label114", form37 );
	pb_main_search25 = UxCreatePushButton( "pb_main_search25", form37 );
	pb_main_search26 = UxCreatePushButton( "pb_main_search26", form37 );
	pb_main_search27 = UxCreatePushButton( "pb_main_search27", form37 );
	label115 = UxCreateLabel( "label115", form37 );
	label116 = UxCreateLabel( "label116", form37 );
	pb_search_plot26 = UxCreatePushButton( "pb_search_plot26", form37 );
	pb_search_plot27 = UxCreatePushButton( "pb_search_plot27", form37 );
	tf_alpha11 = UxCreateTextField( "tf_alpha11", form37 );
	label6 = UxCreateLabel( "label6", form37 );

	_Uxinit_FlatShell();

	/* Create the X widgets */

	UxCreateWidget( FlatShell );
	UxCreateWidget( form37 );
	UxCreateWidget( form38 );
	UxCreateWidget( pb_search_search10 );
	UxCreateWidget( pushButton7 );
	UxCreateWidget( pb_search_plot21 );
	UxCreateWidget( pb_search_plot24 );
	UxCreateWidget( shelp_search7 );
	UxCreateWidget( separator30 );
	UxCreateWidget( separator31 );
	UxCreateWidget( tf_alpha35 );
	UxCreateWidget( tf_alpha36 );
	UxCreateWidget( tf_alpha37 );
	UxCreateWidget( label114 );
	UxCreateWidget( pb_main_search25 );
	UxCreateWidget( pb_main_search26 );
	UxCreateWidget( pb_main_search27 );
	UxCreateWidget( label115 );
	UxCreateWidget( label116 );
	UxCreateWidget( pb_search_plot26 );
	UxCreateWidget( pb_search_plot27 );
	UxCreateWidget( tf_alpha11 );
	UxCreateWidget( label6 );

	UxAddCallback( pb_search_search10, XmNactivateCallback,
			activateCB_pb_search_search10,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( pushButton7, XmNactivateCallback,
			activateCB_pushButton7,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( pb_search_plot21, XmNactivateCallback,
			activateCB_pb_search_plot21,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( pb_search_plot24, XmNactivateCallback,
			activateCB_pb_search_plot24,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( tf_alpha35, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha35,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( tf_alpha36, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha36,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( tf_alpha37, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha37,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( pb_main_search25, XmNactivateCallback,
			activateCB_pb_main_search25,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( pb_main_search26, XmNactivateCallback,
			activateCB_pb_main_search26,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( pb_main_search27, XmNactivateCallback,
			activateCB_pb_main_search27,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( pb_search_plot26, XmNactivateCallback,
			activateCB_pb_search_plot26,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( pb_search_plot27, XmNactivateCallback,
			activateCB_pb_search_plot27,
			(XtPointer) UxFlatShellContext );

	UxAddCallback( tf_alpha11, XmNlosingFocusCallback,
			losingFocusCB_tf_alpha11,
			(XtPointer) UxFlatShellContext );


	/* Finally, call UxRealizeInterface to create the X windows
	   for the widgets created above. */

	UxRealizeInterface( FlatShell );

	return ( FlatShell );
}

/*******************************************************************************
	The following function includes the code that was entered
	in the 'Initial Code' and 'Final Code' sections of the
	Declarations Editor. This function is called from the
	'Interface function' below.
*******************************************************************************/

static swidget	_Ux_create_FlatShell()
{
	swidget                 rtrn;
	_UxCFlatShell           *UxContext;

	UxFlatShellContext = UxContext =
		(_UxCFlatShell *) UxMalloc( sizeof(_UxCFlatShell) );

	rtrn = _Uxbuild_FlatShell();

	return(rtrn);
}

/*******************************************************************************
	The following is the 'Interface function' which is the
	external entry point for creating this interface.
	This function should be called from your application or from
	a callback function.
*******************************************************************************/

swidget	create_FlatShell()
{
	swidget			_Uxrtrn;
	static int		_Uxinit = 0;

	if ( ! _Uxinit )
	{
		static XtActionsRec	_Uxactions[] = {
				{ "UpdateDirectory", action_UpdateDirectory },
				{ "ClearShort", action_ClearShort },
				{ "HelpShort", action_HelpShort },
				{ "FileSelectACT", action_FileSelectACT }
				};

		XtAppAddActions( UxAppContext,
				_Uxactions,
				XtNumber(_Uxactions) );

		_Uxinit = 1;
	}

	_Uxrtrn = _Ux_create_FlatShell();

	return ( _Uxrtrn );
}

/*******************************************************************************
	END OF FILE
*******************************************************************************/

