! UIMX ascii 2.0 key: 577                                                       
*action.HelpACT: {\
/* char *s;\
 s = XmTextGetSelection(UxWidget); \
 \
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
*/}
*action.WriteHelp: {\
char s[100],comm[160]  ;\
extern char print_com[];\
Widget sw = UxWidget;\
\
strcpy(s,"");\
/*sprintf(comm,"WRITE/OUT here");\
AppendDialogText(comm);*/\
 \
if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {\
   strcpy(s,"Select the OST columns to be displayed into the scrolled window");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   } \
else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {\
   strcpy(s,"Activate interface for defining classification rules ");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }  \
else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {\
   strcpy(s,"Activate interface for classifying images");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {\
   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }\
else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {\
   strcpy(s,"Create the OST table");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {\
   strcpy(s,"Popdown this interface");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ \
   strcpy(s,"Name of the Observation Summary Table to be created ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ \
   strcpy(s,"List of frames to be processed ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }  \
  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  \
   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  \
   strcpy(s,"Push button to popup the File Selection Box");     \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  \
        strcpy(s,"Push button to popup the File Selection Box");  \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   } \
}
*action.ClearHelp: {\
UxPutText(UxFindSwidget("SHelp"),"");\
}
*action.SelectCommand: {\
\
 \
\
}
*action.nothing: {\
printf("toto:\n");\
}

*translation.table: transTable9
*translation.parent: Associate
*translation.policy: replace
*translation.<Btn1Up>: HelpACT()
*translation.<Btn1Down>: grab-focus()
*translation.<EnterWindow>: WriteHelp()
*translation.<LeaveWindow>: ClearHelp()

*translation.table: transTable10
*translation.parent: Associate
*translation.policy: override
*translation.<Btn1Up> : SelectCommand()

*translation.table: transTable11
*translation.parent: Associate
*translation.policy: override
*translation.<PtrMoved>: nothing()

*Associate.class: applicationShell
*Associate.parent: NO_PARENT
*Associate.static: true
*Associate.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*Associate.ispecdecl:
*Associate.funcdecl: swidget create_Associate()\

*Associate.funcname: create_Associate
*Associate.funcdef: "swidget", "<create_Associate>(%)"
*Associate.icode:
*Associate.fcode: return(rtrn);\

*Associate.auxdecl:
*Associate.name: Associate
*Associate.x: 666
*Associate.y: 397
*Associate.width: 920
*Associate.height: 363
*Associate.iconName: "Association Rules"

*mainWindow3.class: mainWindow
*mainWindow3.parent: Associate
*mainWindow3.static: true
*mainWindow3.name: mainWindow3
*mainWindow3.x: 200
*mainWindow3.y: 180
*mainWindow3.width: 700
*mainWindow3.height: 470
*mainWindow3.background: WindowBackground
*mainWindow3.unitType: "pixels"

*pullDownMenu3.class: rowColumn
*pullDownMenu3.parent: mainWindow3
*pullDownMenu3.static: true
*pullDownMenu3.name: pullDownMenu3
*pullDownMenu3.borderWidth: 0
*pullDownMenu3.menuHelpWidget: "pullDownMenu_top_b4"
*pullDownMenu3.rowColumnType: "menu_bar"
*pullDownMenu3.menuAccelerator: "<KeyUp>F10"
*pullDownMenu3.background: MenuBackground
*pullDownMenu3.foreground: MenuForeground

*quitPane3.class: rowColumn
*quitPane3.parent: pullDownMenu3
*quitPane3.static: true
*quitPane3.name: quitPane3
*quitPane3.rowColumnType: "menu_pulldown"
*quitPane3.background: MenuBackground
*quitPane3.foreground: MenuForeground

*quitPane_b4.class: pushButtonGadget
*quitPane_b4.parent: quitPane3
*quitPane_b4.static: true
*quitPane_b4.name: quitPane_b4
*quitPane_b4.labelString: "Bye"
*quitPane_b4.activateCallback: {\
extern swidget asso;\
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
extern int rowno;\
int rows,i;\
for (i=0; i<5; i++) {\
    XmTextSetString(arow1[i],"");\
    XmTextSetString(arow2[i],"");\
    XmTextSetString(arow3[i],"");\
    XmTextSetString(arow4[i],"");\
    }\
for (i=5; i<rowno; i++) {\
    XtDestroyWidget(arow1[i]);\
    arow1[i] = (Widget )0;\
    XtDestroyWidget(arow2[i]); \
    arow2[i] = (Widget )0;\
    XtDestroyWidget(arow3[i]);\
    arow3[i] = (Widget )0;\
     XtDestroyWidget(arow4[i]);\
    arow4[i] = (Widget )0;\
    } \
rowno =5;\
UxPopdownInterface(asso);\
}
*quitPane_b4.fontList: BoldTextFont

*UtilsPane3.class: rowColumn
*UtilsPane3.parent: pullDownMenu3
*UtilsPane3.static: true
*UtilsPane3.name: UtilsPane3
*UtilsPane3.rowColumnType: "menu_pulldown"
*UtilsPane3.background: MenuBackground
*UtilsPane3.foreground: MenuForeground

*UtilsPane_b11.class: pushButtonGadget
*UtilsPane_b11.parent: UtilsPane3
*UtilsPane_b11.static: true
*UtilsPane_b11.name: UtilsPane_b11
*UtilsPane_b11.labelString: "Open ..."
*UtilsPane_b11.fontList: BoldTextFont
*UtilsPane_b11.activateCallback: {\
char s[1000];\
extern swidget klist,tablelist;\
\
extern int do8;\
int strip =1;\
strcpy(s,"*.tbl");\
SetFileList(UxGetWidget(tablelist),strip,s);\
UxPutTitle(UxFindSwidget("transientShell8"),"Select Classification Table");\
do8 = 2;\
UxPopupInterface(klist,no_grab);\
}

*UtilsPane_b12.class: pushButtonGadget
*UtilsPane_b12.parent: UtilsPane3
*UtilsPane_b12.static: true
*UtilsPane_b12.name: UtilsPane_b12
*UtilsPane_b12.labelString: "Save"
*UtilsPane_b12.fontList: BoldTextFont
*UtilsPane_b12.activateCallback: {\
extern int tidasso;\
char mytext[60];\
int tid;\
strcpy(mytext,UxGetText(UxFindSwidget("arule_name")));\
\
tid = TCTID(mytext); \
if (tid != -1) TCTCLO(tid);\
save_asso_table(mytext);\
\
}

*pullDownMenu1_p2.class: rowColumn
*pullDownMenu1_p2.parent: pullDownMenu3
*pullDownMenu1_p2.static: true
*pullDownMenu1_p2.name: pullDownMenu1_p2
*pullDownMenu1_p2.rowColumnType: "menu_pulldown"
*pullDownMenu1_p2.background: MenuBackground
*pullDownMenu1_p2.foreground: MenuForeground

*pullDownMenu1_p3_b4.class: pushButtonGadget
*pullDownMenu1_p3_b4.parent: pullDownMenu1_p2
*pullDownMenu1_p3_b4.static: true
*pullDownMenu1_p3_b4.name: pullDownMenu1_p3_b4
*pullDownMenu1_p3_b4.labelString: "Add"
*pullDownMenu1_p3_b4.fontList: BoldTextFont
*pullDownMenu1_p3_b4.activateCallback: {\
extern int arowno;\
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
char mytext[8];\
arowno++;\
XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,arowno,NULL);\
sprintf(mytext,"a1_%d",arowno-1);\
arow1[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);\
sprintf(mytext,"a2_%d",arowno-1);\
arow2[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);\
sprintf(mytext,"a3_%d",arowno-1);\
arow3[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);\
sprintf(mytext,"a4_%d",arowno-1);\
arow4[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);\
}

*pullDownMenu1_p1_b1.class: pushButtonGadget
*pullDownMenu1_p1_b1.parent: pullDownMenu1_p2
*pullDownMenu1_p1_b1.static: true
*pullDownMenu1_p1_b1.name: pullDownMenu1_p1_b1
*pullDownMenu1_p1_b1.labelString: "Reset"
*pullDownMenu1_p1_b1.fontList: BoldTextFont
*pullDownMenu1_p1_b1.activateCallback: {\
extern int arowno;\
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
int i;\
for (i=0; i<arowno; i++) {\
   XmTextSetString(arow1[i],"");\
   XmTextSetString(arow2[i],"");\
   XmTextSetString(arow3[i],"");\
   XmTextSetString(arow4[i],"");\
    }\
}

*pullDownMenu3_p4.class: rowColumn
*pullDownMenu3_p4.parent: pullDownMenu3
*pullDownMenu3_p4.static: true
*pullDownMenu3_p4.name: pullDownMenu3_p4
*pullDownMenu3_p4.rowColumnType: "menu_pulldown"
*pullDownMenu3_p4.background: MenuBackground
*pullDownMenu3_p4.foreground: MenuForeground

*pullDownMenu3_p4_b1.class: pushButtonGadget
*pullDownMenu3_p4_b1.parent: pullDownMenu3_p4
*pullDownMenu3_p4_b1.static: true
*pullDownMenu3_p4_b1.name: pullDownMenu3_p4_b1
*pullDownMenu3_p4_b1.labelString: "On this interface..."
*pullDownMenu3_p4_b1.activateCallback: {\
extern int arowno;\
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
char mytext[8];\
arowno++;\
XtVaSetValues(UxGetWidget(UxFindSwidget("rowasso")),XmNnumColumns,arowno,NULL);\
sprintf(mytext,"a1_%d",arowno-1);\
arow1[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);\
sprintf(mytext,"a2_%d",arowno-1);\
arow2[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);\
sprintf(mytext,"a3_%d",arowno-1);\
arow3[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);\
sprintf(mytext,"a4_%d",arowno-1);\
arow4[arowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowasso")),NULL);\
}
*pullDownMenu3_p4_b1.fontList: BoldTextFont

*pullDownMenu3_p4_b2.class: pushButtonGadget
*pullDownMenu3_p4_b2.parent: pullDownMenu3_p4
*pullDownMenu3_p4_b2.static: true
*pullDownMenu3_p4_b2.name: pullDownMenu3_p4_b2
*pullDownMenu3_p4_b2.labelString: "On importing data..."
*pullDownMenu3_p4_b2.activateCallback: {\
extern int arowno;\
extern Widget arow1[20],arow2[20],arow3[20],arow4[20];\
int i;\
for (i=0; i<arowno; i++) {\
   XmTextSetString(arow1[i],"");\
   XmTextSetString(arow2[i],"");\
   XmTextSetString(arow3[i],"");\
   XmTextSetString(arow4[i],"");\
    }\
}
*pullDownMenu3_p4_b2.fontList: BoldTextFont

*pullDownMenu_top_b4.class: cascadeButton
*pullDownMenu_top_b4.parent: pullDownMenu3
*pullDownMenu_top_b4.static: true
*pullDownMenu_top_b4.name: pullDownMenu_top_b4
*pullDownMenu_top_b4.labelString: "Quit"
*pullDownMenu_top_b4.mnemonic: "Q"
*pullDownMenu_top_b4.subMenuId: "quitPane3"
*pullDownMenu_top_b4.background: MenuBackground
*pullDownMenu_top_b4.fontList: BoldTextFont
*pullDownMenu_top_b4.foreground: MenuForeground

*commandsCascade3.class: cascadeButton
*commandsCascade3.parent: pullDownMenu3
*commandsCascade3.static: true
*commandsCascade3.name: commandsCascade3
*commandsCascade3.labelString: "File"
*commandsCascade3.subMenuId: "UtilsPane3"
*commandsCascade3.background: MenuBackground
*commandsCascade3.fontList: BoldTextFont
*commandsCascade3.foreground: MenuForeground
*commandsCascade3.mnemonic: "F"
*commandsCascade3.x: 101

*pullDownMenu1_top_b3.class: cascadeButton
*pullDownMenu1_top_b3.parent: pullDownMenu3
*pullDownMenu1_top_b3.static: true
*pullDownMenu1_top_b3.name: pullDownMenu1_top_b3
*pullDownMenu1_top_b3.labelString: "Edit"
*pullDownMenu1_top_b3.mnemonic: "E"
*pullDownMenu1_top_b3.subMenuId: "pullDownMenu1_p2"
*pullDownMenu1_top_b3.background: MenuBackground
*pullDownMenu1_top_b3.fontList: BoldTextFont
*pullDownMenu1_top_b3.foreground: MenuForeground

*pullDownMenu3_top_b1.class: cascadeButton
*pullDownMenu3_top_b1.parent: pullDownMenu3
*pullDownMenu3_top_b1.static: true
*pullDownMenu3_top_b1.name: pullDownMenu3_top_b1
*pullDownMenu3_top_b1.labelString: "Help"
*pullDownMenu3_top_b1.mnemonic: "H"
*pullDownMenu3_top_b1.subMenuId: "pullDownMenu3_p4"
*pullDownMenu3_top_b1.background: MenuBackground
*pullDownMenu3_top_b1.fontList: BoldTextFont
*pullDownMenu3_top_b1.foreground: MenuForeground

*workAreaForm3.class: form
*workAreaForm3.parent: mainWindow3
*workAreaForm3.static: true
*workAreaForm3.name: workAreaForm3
*workAreaForm3.width: 800
*workAreaForm3.height: 400
*workAreaForm3.borderWidth: 0
*workAreaForm3.background: WindowBackground

*form4.class: form
*form4.parent: workAreaForm3
*form4.static: true
*form4.name: form4
*form4.resizePolicy: "resize_none"
*form4.unitType: "pixels"
*form4.x: 10
*form4.y: 10
*form4.width: 900
*form4.height: 300
*form4.background: WindowBackground

*scrolledWindow4.class: scrolledWindow
*scrolledWindow4.parent: form4
*scrolledWindow4.static: true
*scrolledWindow4.name: scrolledWindow4
*scrolledWindow4.scrollingPolicy: "automatic"
*scrolledWindow4.x: 10
*scrolledWindow4.y: 30
*scrolledWindow4.width: 900
*scrolledWindow4.height: 300
*scrolledWindow4.background: WindowBackground
*scrolledWindow4.scrollBarPlacement: "bottom_left"
*scrolledWindow4.scrollBarDisplayPolicy: "static"
*scrolledWindow4.leftAttachment: "attach_form"
*scrolledWindow4.rightAttachment: "attach_form"
*scrolledWindow4.rightOffset: 10
*scrolledWindow4.leftOffset: 10
*scrolledWindow4.bottomAttachment: "attach_form"
*scrolledWindow4.bottomOffset: 10
*scrolledWindow4.topOffset: 120
*scrolledWindow4.topAttachment: "attach_form"
*scrolledWindow4.visualPolicy: "variable"

*rowasso.class: rowColumn
*rowasso.parent: scrolledWindow4
*rowasso.static: true
*rowasso.name: rowasso
*rowasso.x: 8
*rowasso.y: 8
*rowasso.width: 860
*rowasso.height: 370
*rowasso.background: WindowBackground
*rowasso.entryAlignment: "alignment_center"
*rowasso.orientation: "horizontal"
*rowasso.packing: "pack_column"
*rowasso.createCallback: {\
\
}
*rowasso.numColumns: 5
*rowasso.entryCallback: {\
/*XmRowColumnCallbackStruct *cbs;\
Widget pb = cbs->widget;\
cbs = (XmRowColumnCallbackStruct *)UxCallbackArg;\
printf("%s: %s\n",XtName(pb),cbs->data);*/\
}
*rowasso.resizeWidth: "true"
*rowasso.isAligned: "true"

*label14.class: label
*label14.parent: form4
*label14.static: true
*label14.name: label14
*label14.x: 10
*label14.y: 420
*label14.width: 100
*label14.height: 40
*label14.background: WindowBackground
*label14.fontList: BoldTextFont
*label14.labelString: "FUNCTION"
*label14.alignment: "alignment_beginning"
*label14.topOffset: 70
*label14.topAttachment: "attach_form"
*label14.leftAttachment: "attach_form"
*label14.leftOffset: 80

*label15.class: label
*label15.parent: form4
*label15.static: true
*label15.name: label15
*label15.x: 90
*label15.y: 10
*label15.width: 100
*label15.height: 40
*label15.background: WindowBackground
*label15.fontList: BoldTextFont
*label15.labelString: "RANGE_1"
*label15.alignment: "alignment_beginning"
*label15.topAttachment: "attach_form"
*label15.leftAttachment: "attach_form"
*label15.leftOffset: 310
*label15.topOffset: 70

*label16.class: label
*label16.parent: form4
*label16.static: true
*label16.name: label16
*label16.x: 310
*label16.y: 10
*label16.width: 100
*label16.height: 40
*label16.background: WindowBackground
*label16.fontList: BoldTextFont
*label16.labelString: "RANGE_2"
*label16.alignment: "alignment_beginning"
*label16.leftAttachment: "attach_form"
*label16.leftOffset: 510
*label16.topAttachment: "attach_form"
*label16.topOffset: 70

*label17.class: label
*label17.parent: form4
*label17.static: true
*label17.name: label17
*label17.x: 510
*label17.y: 10
*label17.width: 100
*label17.height: 40
*label17.background: WindowBackground
*label17.fontList: BoldTextFont
*label17.labelString: "WEIGHT"
*label17.alignment: "alignment_beginning"
*label17.leftAttachment: "attach_form"
*label17.leftOffset: 720
*label17.topAttachment: "attach_form"
*label17.topOffset: 70

*label19.class: label
*label19.parent: form4
*label19.static: true
*label19.name: label19
*label19.x: 210
*label19.y: 10
*label19.width: 260
*label19.height: 50
*label19.background: WindowBackground
*label19.fontList: BoldTextFont
*label19.labelString: "Table Name"
*label19.leftAttachment: "attach_form"
*label19.leftOffset: 100
*label19.topAttachment: "attach_form"

*arule_name.class: text
*arule_name.parent: form4
*arule_name.static: true
*arule_name.name: arule_name
*arule_name.x: 480
*arule_name.y: 10
*arule_name.width: 360
*arule_name.height: 50
*arule_name.background: TextBackground
*arule_name.fontList: TextFont
*arule_name.topAttachment: "attach_form"
*arule_name.leftAttachment: "attach_form"
*arule_name.leftOffset: 400

