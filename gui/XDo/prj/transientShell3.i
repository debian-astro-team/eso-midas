! UIMX ascii 2.0 key: 303                                                       
*action.WriteHelp: {\
char s[100],comm[160]  ;\
extern char print_com[];\
Widget sw = UxWidget;\
\
strcpy(s,"");\
/*sprintf(comm,"WRITE/OUT here");\
AppendDialogText(comm);*/\
 \
if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {\
   strcpy(s,"Select the OST columns to be displayed into the scrolled window");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   } \
else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {\
   strcpy(s,"Activate interface for defining classification rules ");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }  \
else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {\
   strcpy(s,"Activate interface for classifying images");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {\
   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }\
else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {\
   strcpy(s,"Create the OST table");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {\
   strcpy(s,"Popdown this interface");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ \
   strcpy(s,"Name of the Observation Summary Table to be created ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ \
   strcpy(s,"List of frames to be processed ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }  \
  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  \
   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  \
   strcpy(s,"Push button to popup the File Selection Box");     \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  \
        strcpy(s,"Push button to popup the File Selection Box");  \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   } \
}
*action.ClearHelp: {\
UxPutText(UxFindSwidget("SHelp"),"");\
}

*translation.table: transTable2
*translation.parent: transientShell3
*translation.policy: override
*translation.<EnterWindow>  : WriteHelp ()
*translation.<LeaveWindow> : ClearHelp ()

*transientShell3.class: transientShell
*transientShell3.parent: NO_PARENT
*transientShell3.static: true
*transientShell3.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <midas_def.h>\
char *instr[] = {"EMMI","SUSI"};\
\
extern char print_com[], mid_mail[];\
extern swidget scrolledList1;
*transientShell3.ispecdecl:
*transientShell3.funcdecl: swidget create_transientShell3()\

*transientShell3.funcname: create_transientShell3
*transientShell3.funcdef: "swidget", "<create_transientShell3>(%)"
*transientShell3.icode:
*transientShell3.fcode: return(rtrn);\

*transientShell3.auxdecl:
*transientShell3.name: transientShell3
*transientShell3.x: 264
*transientShell3.y: 431
*transientShell3.width: 480
*transientShell3.height: 400
*transientShell3.popupCallback: {\
\
}
*transientShell3.title: "Creation of The OST"
*transientShell3.translations: ""

*workArea3.class: form
*workArea3.parent: transientShell3
*workArea3.static: true
*workArea3.name: workArea3
*workArea3.x: 0
*workArea3.y: 0
*workArea3.width: 480
*workArea3.height: 400
*workArea3.borderWidth: 0
*workArea3.createCallback: {\
\
}
*workArea3.mapCallback: {\
\
}
*workArea3.background: WindowBackground
*workArea3.unitType: "pixels"
*workArea3.noResize: "true"
*workArea3.resizePolicy: "resize_none"

*SHelp3.class: text
*SHelp3.parent: workArea3
*SHelp3.static: true
*SHelp3.name: SHelp3
*SHelp3.x: 30
*SHelp3.y: 380
*SHelp3.width: 325
*SHelp3.height: 40
*SHelp3.background: SHelpBackground
*SHelp3.foreground: TextForeground
*SHelp3.highlightColor: ApplicBackground
*SHelp3.fontList: TextFont
*SHelp3.cursorPositionVisible: "true"
*SHelp3.translations: ""
*SHelp3.activateCallback: {\
\
}
*SHelp3.pendingDelete: "true"
*SHelp3.blinkRate: 500
*SHelp3.editMode: "single_line_edit"
*SHelp3.highlightOnEnter: "true"
*SHelp3.wordWrap: "false"
*SHelp3.accelerators: ""
*SHelp3.leftAttachment: "attach_form"
*SHelp3.leftOffset: 5
*SHelp3.rightAttachment: "attach_form"
*SHelp3.rightOffset: 5
*SHelp3.bottomAttachment: "attach_form"
*SHelp3.bottomOffset: 50
*SHelp3.editable: "true"
*SHelp3.text: " "

*NewsButton3.class: pushButton
*NewsButton3.parent: workArea3
*NewsButton3.static: true
*NewsButton3.name: NewsButton3
*NewsButton3.x: 20
*NewsButton3.y: 630
*NewsButton3.width: 95
*NewsButton3.height: 30
*NewsButton3.background: ButtonBackground
*NewsButton3.labelString: "Apply"
*NewsButton3.foreground: ButtonForeground
*NewsButton3.fontList: BoldTextFont
*NewsButton3.activateCallback: {\
char command[160],iframe[60],dtable[60],fflag[8],sflag[8],flag[4],pfix[5];\
extern char ostchoice[60];\
extern int tidost,tidcomm;\
int unit;\
strcpy(ostchoice ,osfsupply(UxGetText(UxFindSwidget("ost_t1")),".tbl"));\
strncpy(iframe,UxGetText(UxFindSwidget("ost_t2")),60);\
/*printf("ostchoice: %s\n",iframe);*/\
if (iframe[0] == '\0') iframe[0] = '?';\
strncpy(dtable,UxGetText(UxFindSwidget("ost_t3")),60);\
strcpy(fflag ,UxGetSet(UxFindSwidget("ost_to1")));\
strcpy(sflag,UxGetSet(UxFindSwidget("ost_to2")));\
\
if (fflag[0] == 't') flag[0] = 'F';\
else flag[0] = 'M';\
if (sflag[0] == 't') flag[2] = 'F';\
else flag[2] = 'N';\
flag[1] = 'C';\
flag[3] = '\0';\
if (tidost != -1) {\
        TCTCLO(tidost);\
        tidost = -1;\
        }\
TCTOPN( "TAB_COMM",F_IO_MODE,&tidcomm);\
TCEWRC(tidcomm,1,2,ostchoice);\
TCEWRC(tidcomm,1,3,dtable);\
/*SCDWRC(tidost ,"table_descr",1,dtable,1,60,&unit);*/\
TCTCLO(tidcomm);\
tidcomm = -1; \
strcpy(pfix,"+");\
ostcrea(iframe,pfix,dtable,ostchoice,flag);\
\
sprintf(command,"CREATE/OST %s ? %s %s %s",iframe,dtable,ostchoice,flag);\
/*AppendDialogText(command);*/\
\
if (read_ost_table()) {\
\
display_ident_table(UxGetWidget(UxFindSwidget("identlist")));\
}\
/*SCDWRC(tidost ,"table_descr",1,dtable,1,60,&unit);*/\
}
*NewsButton3.recomputeSize: "false"
*NewsButton3.topAttachment: "attach_widget"
*NewsButton3.topOffset: 10
*NewsButton3.topWidget: "SHelp3"
*NewsButton3.highlightColor: "Black"
*NewsButton3.leftAttachment: "attach_form"
*NewsButton3.leftOffset: 50

*PrintButton3.class: pushButton
*PrintButton3.parent: workArea3
*PrintButton3.static: true
*PrintButton3.name: PrintButton3
*PrintButton3.x: 280
*PrintButton3.y: 330
*PrintButton3.width: 95
*PrintButton3.height: 30
*PrintButton3.background: ButtonBackground
*PrintButton3.labelString: "Cancel"
*PrintButton3.foreground: ButtonForeground
*PrintButton3.fontList: BoldTextFont
*PrintButton3.activateCallback: {\
extern swidget crea;\
\
UxPopdownInterface(crea);\
\
}
*PrintButton3.recomputeSize: "false"
*PrintButton3.topAttachment: "attach_widget"
*PrintButton3.topOffset: 10
*PrintButton3.topWidget: "SHelp3"
*PrintButton3.leftAttachment: "attach_none"
*PrintButton3.leftOffset: 40
*PrintButton3.leftWidget: "NewsButton3"
*PrintButton3.highlightColor: "Black"
*PrintButton3.rightAttachment: "attach_form"
*PrintButton3.rightOffset: 50

*label4.class: label
*label4.parent: workArea3
*label4.static: true
*label4.name: label4
*label4.x: 10
*label4.y: 50
*label4.width: 130
*label4.height: 20
*label4.background: LabelBackground
*label4.fontList: BoldTextFont
*label4.foreground: TextForeground
*label4.labelString: "OST"
*label4.leftAttachment: "attach_form"
*label4.leftOffset: 35
*label4.topOffset: 20
*label4.topAttachment: "attach_form"

*label3.class: label
*label3.parent: workArea3
*label3.static: true
*label3.name: label3
*label3.x: 5
*label3.y: 60
*label3.width: 130
*label3.height: 20
*label3.background: LabelBackground
*label3.fontList: BoldTextFont
*label3.foreground: TextForeground
*label3.labelString: "Input Frames"
*label3.topAttachment: "attach_form"
*label3.topOffset: 65
*label3.leftAttachment: "attach_form"
*label3.leftOffset: 35

*label1.class: label
*label1.parent: workArea3
*label1.static: true
*label1.name: label1
*label1.x: 0
*label1.y: 110
*label1.width: 130
*label1.height: 20
*label1.background: LabelBackground
*label1.fontList: BoldTextFont
*label1.foreground: TextForeground
*label1.labelString: "Descriptor Table"
*label1.leftAttachment: "attach_form"
*label1.leftOffset: 35
*label1.topAttachment: "attach_form"
*label1.topOffset: 110

*label2.class: label
*label2.parent: workArea3
*label2.static: true
*label2.name: label2
*label2.x: 10
*label2.y: 160
*label2.width: 70
*label2.height: 20
*label2.background: LabelBackground
*label2.fontList: BoldTextFont
*label2.foreground: TextForeground
*label2.labelString: "Type"
*label2.leftAttachment: "attach_form"
*label2.topAttachment: "attach_form"
*label2.leftOffset: 30
*label2.topOffset: 250

*ost_t1.class: text
*ost_t1.parent: workArea3
*ost_t1.static: true
*ost_t1.name: ost_t1
*ost_t1.x: 200
*ost_t1.y: 30
*ost_t1.width: 160
*ost_t1.height: 40
*ost_t1.topAttachment: "attach_form"
*ost_t1.topOffset: 10
*ost_t1.rightAttachment: "attach_none"
*ost_t1.rightOffset: 20
*ost_t1.fontList: TextFont
*ost_t1.foreground: TextForeground
*ost_t1.background: TextBackground
*ost_t1.leftAttachment: "attach_form"
*ost_t1.leftOffset: 220
*ost_t1.topShadowColor: "Gray80" 
*ost_t1.cursorPosition: 0 

*ost_t2.class: text
*ost_t2.parent: workArea3
*ost_t2.static: true
*ost_t2.name: ost_t2
*ost_t2.x: 200
*ost_t2.y: 50
*ost_t2.width: 160
*ost_t2.height: 40
*ost_t2.fontList: TextFont
*ost_t2.foreground: TextForeground
*ost_t2.background: TextBackground
*ost_t2.topAttachment: "attach_form"
*ost_t2.topOffset: 58
*ost_t2.rightAttachment: "attach_none"
*ost_t2.rightOffset: 20
*ost_t2.leftAttachment: "attach_form"
*ost_t2.leftOffset: 220
*ost_t2.text: ""

*pushButton4.class: pushButton
*pushButton4.parent: workArea3
*pushButton4.static: true
*pushButton4.name: pushButton4
*pushButton4.x: 400
*pushButton4.y: 60
*pushButton4.width: 40
*pushButton4.height: 40
*pushButton4.fontList: BoldTextFont
*pushButton4.foreground: ButtonForeground
*pushButton4.background: ButtonBackground
*pushButton4.labelString: "..."
*pushButton4.topAttachment: "attach_form"
*pushButton4.topOffset: 58
*pushButton4.rightAttachment: "attach_form"
*pushButton4.rightOffset: 40
*pushButton4.activateCallback: {\
char s[1000];\
extern swidget flist;\
\
UxPopupInterface(flist,no_grab);\
\
}

*pushButton5.class: pushButton
*pushButton5.parent: workArea3
*pushButton5.static: true
*pushButton5.name: pushButton5
*pushButton5.x: 400
*pushButton5.y: 100
*pushButton5.width: 40
*pushButton5.height: 40
*pushButton5.fontList: BoldTextFont
*pushButton5.foreground: ButtonForeground
*pushButton5.background: ButtonBackground
*pushButton5.labelString: "..."
*pushButton5.rightAttachment: "attach_form"
*pushButton5.rightOffset: 40
*pushButton5.topAttachment: "attach_form"
*pushButton5.topOffset: 158
*pushButton5.activateCallback: {\
extern swidget ilist,dlist;\
extern int doI;\
char cbuf1[8],cbuf2[8];\
XmString *str;\
int item_no,i;\
\
strcpy(cbuf1,UxGetSet(UxFindSwidget("toggleButtonGadget1")));\
strcpy(cbuf2,UxGetSet(UxFindSwidget("toggleButtonGadget2")));\
if (cbuf1[0] == 't') {\
     doI = 0;\
     item_no = 2;\
     str = (XmString *)XtMalloc(item_no * sizeof(XmString));\
     for (i=0; i<item_no; i++)\
        str[i] = XmStringCreateSimple(instr[i]);\
     XtVaSetValues(UxGetWidget(UxFindSwidget("SelectInstrument")),\
               XmNlistItems,str,\
               XmNlistItemCount,item_no,\
               NULL);\
     \
     UxPutListLabelString( UxFindSwidget("SelectInstrument"),"Instrument");\
     UxPutDialogTitle(UxFindSwidget("SelectInstrument"),"Select Instrument");\
     UxPopupInterface(ilist,no_grab);\
     }\
else if (cbuf2[0] == 't') {\
     /*  XtVaSetValues(UxGetWidget(dlist),XmNtitle,\
                     "Select Descriptor Table",NULL);*/\
       UxPutDialogTitle(UxFindSwidget("SelectDescrTable"),"Select Descriptor Table");\
       UxPopupInterface(dlist,no_grab);\
       }\
else {\
       UxPutDialogTitle(UxFindSwidget("SelectDescrTable"),"Select FITS File");\
       UxPopupInterface(dlist,no_grab);\
       }\
}

*ost_t3.class: text
*ost_t3.parent: workArea3
*ost_t3.static: true
*ost_t3.name: ost_t3
*ost_t3.x: 170
*ost_t3.y: 100
*ost_t3.width: 160
*ost_t3.height: 40
*ost_t3.fontList: TextFont
*ost_t3.foreground: TextForeground
*ost_t3.background: TextBackground
*ost_t3.topAttachment: "attach_form"
*ost_t3.topOffset: 158
*ost_t3.leftAttachment: "attach_form"
*ost_t3.leftOffset: 220

*ost_to1.class: toggleButton
*ost_to1.parent: workArea3
*ost_to1.static: true
*ost_to1.name: ost_to1
*ost_to1.x: 140
*ost_to1.y: 160
*ost_to1.width: 80
*ost_to1.height: 30
*ost_to1.topAttachment: "attach_form"
*ost_to1.topOffset: 245
*ost_to1.background: TextBackground
*ost_to1.fontList: BoldTextFont
*ost_to1.foreground: TextForeground
*ost_to1.leftAttachment: "attach_form"
*ost_to1.leftOffset: 120
*ost_to1.selectColor: "PaleGreen"
*ost_to1.labelString: "Fits"
*ost_to1.indicatorType: "one_of_many"
*ost_to1.valueChangedCallback: {\
char cbuf[8];\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't') \
    UxPutLabelString(UxThisWidget,"Fits");\
else\
    UxPutLabelString(UxThisWidget,"Midas");\
}
*ost_to1.set: "true"

*label6.class: label
*label6.parent: workArea3
*label6.static: true
*label6.name: label6
*label6.x: 40
*label6.y: 182
*label6.width: 70
*label6.height: 20
*label6.background: LabelBackground
*label6.fontList: BoldTextFont
*label6.foreground: TextForeground
*label6.labelString: "Statistics"
*label6.leftAttachment: "attach_form"
*label6.leftOffset: 250
*label6.topAttachment: "attach_form"
*label6.topOffset: 250

*ost_to2.class: toggleButton
*ost_to2.parent: workArea3
*ost_to2.static: true
*ost_to2.name: ost_to2
*ost_to2.x: 130
*ost_to2.y: 178
*ost_to2.width: 80
*ost_to2.height: 30
*ost_to2.background: TextBackground
*ost_to2.fontList: BoldTextFont
*ost_to2.foreground: TextForeground
*ost_to2.selectColor: "PaleGreen"
*ost_to2.labelString: "Yes"
*ost_to2.indicatorType: "one_of_many"
*ost_to2.leftAttachment: "attach_none"
*ost_to2.leftOffset: 30
*ost_to2.topAttachment: "attach_form"
*ost_to2.topOffset: 245
*ost_to2.valueChangedCallback: {\
char cbuf[8];\
strcpy(cbuf,UxGetSet(UxThisWidget));\
if (cbuf[0] == 't')\
    UxPutLabelString(UxThisWidget,"Yes");\
else\
    UxPutLabelString(UxThisWidget,"No");\
    \
}
*ost_to2.rightAttachment: "attach_form"
*ost_to2.rightOffset: 30
*ost_to2.set: "true"

*rowColumn1.class: rowColumn
*rowColumn1.parent: workArea3
*rowColumn1.static: true
*rowColumn1.name: rowColumn1
*rowColumn1.x: 10
*rowColumn1.y: 130
*rowColumn1.width: 140
*rowColumn1.height: 110
*rowColumn1.background: WindowBackground
*rowColumn1.radioBehavior: "true"
*rowColumn1.leftAttachment: "attach_form"
*rowColumn1.leftOffset: 20

*toggleButtonGadget1.class: toggleButtonGadget
*toggleButtonGadget1.parent: rowColumn1
*toggleButtonGadget1.static: true
*toggleButtonGadget1.name: toggleButtonGadget1
*toggleButtonGadget1.x: 10
*toggleButtonGadget1.y: 10
*toggleButtonGadget1.width: 120
*toggleButtonGadget1.height: 20
*toggleButtonGadget1.indicatorType: "one_of_many"
*toggleButtonGadget1.labelString: "Get Standard Table"
*toggleButtonGadget1.fontList: TextFont
*toggleButtonGadget1.selectColor: "PaleGreen"

*toggleButtonGadget2.class: toggleButtonGadget
*toggleButtonGadget2.parent: rowColumn1
*toggleButtonGadget2.static: true
*toggleButtonGadget2.name: toggleButtonGadget2
*toggleButtonGadget2.x: 13
*toggleButtonGadget2.y: 13
*toggleButtonGadget2.width: 120
*toggleButtonGadget2.height: 20
*toggleButtonGadget2.indicatorType: "one_of_many"
*toggleButtonGadget2.labelString: "Select Own Table"
*toggleButtonGadget2.fontList: TextFont
*toggleButtonGadget2.selectColor: "PaleGreen"
*toggleButtonGadget2.set: "true"

*toggleButtonGadget3.class: toggleButtonGadget
*toggleButtonGadget3.parent: rowColumn1
*toggleButtonGadget3.static: true
*toggleButtonGadget3.name: toggleButtonGadget3
*toggleButtonGadget3.x: 13
*toggleButtonGadget3.y: 44
*toggleButtonGadget3.width: 120
*toggleButtonGadget3.height: 20
*toggleButtonGadget3.indicatorType: "one_of_many"
*toggleButtonGadget3.labelString: "Create from Fits File"
*toggleButtonGadget3.fontList: TextFont
*toggleButtonGadget3.selectColor: "PaleGreen"

*separator1.class: separator
*separator1.parent: workArea3
*separator1.static: true
*separator1.name: separator1
*separator1.x: 180
*separator1.y: 140
*separator1.width: 10
*separator1.height: 106
*separator1.orientation: "vertical"
*separator1.background: WindowBackground
*separator1.leftAttachment: "attach_form"
*separator1.leftOffset: 180
*separator1.topAttachment: "attach_form"
*separator1.topOffset: 120

*separator2.class: separator
*separator2.parent: workArea3
*separator2.static: true
*separator2.name: separator2
*separator2.x: 190
*separator2.y: 145
*separator2.width: 10
*separator2.height: 106
*separator2.orientation: "vertical"
*separator2.background: WindowBackground
*separator2.topAttachment: "attach_form"
*separator2.topOffset: 120
*separator2.leftAttachment: "attach_form"
*separator2.leftOffset: 10

*separator3.class: separator
*separator3.parent: workArea3
*separator3.static: true
*separator3.name: separator3
*separator3.x: 10
*separator3.y: 230
*separator3.width: 170
*separator3.height: 10
*separator3.background: WindowBackground
*separator3.leftAttachment: "attach_form"
*separator3.leftOffset: 15
*separator3.topAttachment: "attach_form"
*separator3.topOffset: 222

*separator4.class: separator
*separator4.parent: workArea3
*separator4.static: true
*separator4.name: separator4
*separator4.x: 10
*separator4.y: 130
*separator4.width: 170
*separator4.height: 10
*separator4.background: WindowBackground
*separator4.topAttachment: "attach_form"
*separator4.topOffset: 115
*separator4.leftAttachment: "attach_form"
*separator4.leftOffset: 15

