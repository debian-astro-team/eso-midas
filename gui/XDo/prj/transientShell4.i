! UIMX ascii 2.0 key: 8867                                                      

*transientShell4.class: transientShell
*transientShell4.parent: NO_PARENT
*transientShell4.static: true
*transientShell4.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <Xm/LabelG.h>\
#include <midas_def.h>\
\
char *rowlab[256];
*transientShell4.ispecdecl:
*transientShell4.funcdecl: swidget create_transientShell4()\

*transientShell4.funcname: create_transientShell4
*transientShell4.funcdef: "swidget", "<create_transientShell4>(%)"
*transientShell4.icode:
*transientShell4.fcode: return(rtrn);\

*transientShell4.auxdecl:
*transientShell4.name: transientShell4
*transientShell4.x: 410
*transientShell4.y: 230
*transientShell4.width: 350
*transientShell4.height: 600

*form5.class: form
*form5.parent: transientShell4
*form5.static: true
*form5.name: form5
*form5.resizePolicy: "resize_none"
*form5.unitType: "pixels"
*form5.x: 10
*form5.y: 10
*form5.width: 350
*form5.height: 430
*form5.background: WindowBackground

*scrolledWindow5.class: scrolledWindow
*scrolledWindow5.parent: form5
*scrolledWindow5.static: true
*scrolledWindow5.name: scrolledWindow5
*scrolledWindow5.scrollingPolicy: "automatic"
*scrolledWindow5.x: 10
*scrolledWindow5.y: 30
*scrolledWindow5.width: 350
*scrolledWindow5.height: 380
*scrolledWindow5.background: WindowBackground
*scrolledWindow5.scrollBarPlacement: "bottom_left"
*scrolledWindow5.scrollBarDisplayPolicy: "static"
*scrolledWindow5.leftAttachment: "attach_form"
*scrolledWindow5.rightAttachment: "attach_form"
*scrolledWindow5.rightOffset: 10
*scrolledWindow5.leftOffset: 10

*rowColumn4.class: rowColumn
*rowColumn4.parent: scrolledWindow5
*rowColumn4.static: true
*rowColumn4.name: rowColumn4
*rowColumn4.x: 8
*rowColumn4.y: 8
*rowColumn4.width: 350
*rowColumn4.height: 370
*rowColumn4.background: WindowBackground
*rowColumn4.entryAlignment: "alignment_center"
*rowColumn4.orientation: "vertical"
*rowColumn4.packing: "pack_column"
*rowColumn4.createCallback: {\
\
}
*rowColumn4.numColumns: 2
*rowColumn4.entryCallback: {\
/*XmRowColumnCallbackStruct *cbs;\
Widget pb = cbs->widget;\
cbs = (XmRowColumnCallbackStruct *)UxCallbackArg;\
printf("%s: %s\n",XtName(pb),cbs->data);*/\
}
*rowColumn4.resizeWidth: "true"

*label5.class: label
*label5.parent: form5
*label5.static: true
*label5.name: label5
*label5.x: 10
*label5.y: 420
*label5.width: 240
*label5.height: 40
*label5.background: WindowBackground
*label5.fontList: BoldTextFont
*label5.labelString: "Rule Name"
*label5.alignment: "alignment_beginning"

*text4.class: text
*text4.parent: form5
*text4.static: true
*text4.name: text4
*text4.x: 10
*text4.y: 460
*text4.width: 330
*text4.height: 40
*text4.background: TextBackground
*text4.leftAttachment: "attach_form"
*text4.leftOffset: 10
*text4.rightAttachment: "attach_form"
*text4.rightOffset: 10

*pushButton9.class: pushButton
*pushButton9.parent: form5
*pushButton9.static: true
*pushButton9.name: pushButton9
*pushButton9.x: 20
*pushButton9.y: 530
*pushButton9.width: 80
*pushButton9.height: 40
*pushButton9.background: ButtonBackground
*pushButton9.fontList: BoldTextFont
*pushButton9.foreground: ButtonForeground
*pushButton9.labelString: "OK"
*pushButton9.leftAttachment: "attach_form"
*pushButton9.leftOffset: 20
*pushButton9.bottomAttachment: "attach_form"
*pushButton9.bottomOffset: 30

*pushButton10.class: pushButton
*pushButton10.parent: form5
*pushButton10.static: true
*pushButton10.name: pushButton10
*pushButton10.x: 140
*pushButton10.y: 530
*pushButton10.width: 80
*pushButton10.height: 40
*pushButton10.background: ButtonBackground
*pushButton10.fontList: BoldTextFont
*pushButton10.foreground: ButtonForeground
*pushButton10.labelString: "Apply"
*pushButton10.bottomAttachment: "attach_form"
*pushButton10.bottomOffset: 30
*pushButton10.leftAttachment: "attach_widget"
*pushButton10.leftOffset: 35
*pushButton10.leftWidget: "pushButton9"
*pushButton10.activateCallback: {\
extern int tidost;\
extern char *rowtext[256];\
int ncol,dummy,i,unit,lennew;\
char mylabel[TBL_LABLEN+2],*temp,selecrit[256];\
TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy);\
temp = osmmget(256);\
oscfill(selecrit,256,'\0');\
printf("ncol %d \n",ncol);\
for (i=1; i<=ncol; i++) \
      if (rowtext[i-1]) {\
        printf("rowtext: %s\n",rowtext[i-1]);\
     if (*rowtext[i-1]) {\
         mylabel[0] = ':';   \
         TCLGET(tidost,i,mylabel+1);\
         if (selecrit[0] != '\0') strcat(selecrit,".AND.");\
         decrypt1(mylabel,rowtext[i-1],selecrit,temp);\
         lennew = strlen(selecrit);\
          }\
}\
printf("selecrit: %s\n",selecrit);\
/*SCDWRC(tidost,"test",1,selecrit,1,lennew,&unit);*/\
             \
}

*pushButton11.class: pushButton
*pushButton11.parent: form5
*pushButton11.static: true
*pushButton11.name: pushButton11
*pushButton11.x: 250
*pushButton11.y: 530
*pushButton11.width: 80
*pushButton11.height: 40
*pushButton11.background: ButtonBackground
*pushButton11.fontList: BoldTextFont
*pushButton11.foreground: ButtonForeground
*pushButton11.labelString: "Cancel"
*pushButton11.leftAttachment: "attach_none"
*pushButton11.leftOffset: 40
*pushButton11.leftWidget: "pushButton10"
*pushButton11.rightAttachment: "attach_form"
*pushButton11.rightOffset: 20
*pushButton11.bottomAttachment: "attach_form"
*pushButton11.bottomOffset: 30
*pushButton11.activateCallback: {\
extern swidget classi;\
UxPopdownInterface(classi);\
}

