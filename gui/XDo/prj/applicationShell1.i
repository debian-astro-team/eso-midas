! UIMX ascii 2.0 key: 5545                                                      
*action.HelpACT: {\
/* char *s;\
 s = XmTextGetSelection(UxWidget); \
 \
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
*/}
*action.WriteHelp: {\
char s[100],comm[160]  ;\
extern char print_com[];\
Widget sw = UxWidget;\
\
strcpy(s,"");\
/*sprintf(comm,"WRITE/OUT here");\
AppendDialogText(comm);*/\
 \
if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {\
   strcpy(s,"Select the OST columns to be displayed into the scrolled window");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   } \
else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {\
   strcpy(s,"Activate interface for defining classification rules ");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }  \
else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {\
   strcpy(s,"Activate interface for classifying images");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {\
   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }\
else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {\
   strcpy(s,"Create the OST table");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {\
   strcpy(s,"Popdown this interface");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ \
   strcpy(s,"Name of the Observation Summary Table to be created ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ \
   strcpy(s,"List of frames to be processed ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }  \
  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  \
   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  \
   strcpy(s,"Push button to popup the File Selection Box");     \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  \
        strcpy(s,"Push button to popup the File Selection Box");  \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   } \
}
*action.ClearHelp: {\
UxPutText(UxFindSwidget("SHelp"),"");\
}
*action.SelectCommand: {\
\
 \
\
}
*action.nothing: {\
printf("toto:\n");\
}

*translation.table: transTable3
*translation.parent: applicationClas
*translation.policy: replace
*translation.<Btn1Up>: HelpACT()
*translation.<Btn1Down>: grab-focus()
*translation.<EnterWindow>: WriteHelp()
*translation.<LeaveWindow>: ClearHelp()

*translation.table: transTable4
*translation.parent: applicationClas
*translation.policy: override
*translation.<Btn1Up> : SelectCommand()

*translation.table: transTable5
*translation.parent: applicationClas
*translation.policy: override
*translation.<PtrMoved>: nothing()

*applicationClas.class: applicationShell
*applicationClas.parent: NO_PARENT
*applicationClas.static: true
*applicationClas.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <Xm/LabelG.h>\
#include <midas_def.h>\
#include <tbldef.h>\
char *osmmget();\
char *rowlab[256];
*applicationClas.ispecdecl:
*applicationClas.funcdecl: swidget create_applicationClas()\

*applicationClas.funcname: create_applicationClas
*applicationClas.funcdef: "swidget", "<create_applicationClas>(%)"
*applicationClas.icode:
*applicationClas.fcode: return(rtrn);\

*applicationClas.auxdecl:
*applicationClas.name: applicationClas
*applicationClas.x: 690
*applicationClas.y: 260
*applicationClas.width: 370
*applicationClas.height: 670
*applicationClas.title: "Classification Rules"

*mainWindow1.class: mainWindow
*mainWindow1.parent: applicationClas
*mainWindow1.static: true
*mainWindow1.name: mainWindow1
*mainWindow1.x: 200
*mainWindow1.y: 180
*mainWindow1.width: 700
*mainWindow1.height: 470
*mainWindow1.background: WindowBackground
*mainWindow1.unitType: "pixels"

*pullDownMenu1.class: rowColumn
*pullDownMenu1.parent: mainWindow1
*pullDownMenu1.static: true
*pullDownMenu1.name: pullDownMenu1
*pullDownMenu1.borderWidth: 0
*pullDownMenu1.menuHelpWidget: "pullDownMenu_top_b2"
*pullDownMenu1.rowColumnType: "menu_bar"
*pullDownMenu1.menuAccelerator: "<KeyUp>F10"
*pullDownMenu1.background: MenuBackground
*pullDownMenu1.foreground: MenuForeground

*quitPane1.class: rowColumn
*quitPane1.parent: pullDownMenu1
*quitPane1.static: true
*quitPane1.name: quitPane1
*quitPane1.rowColumnType: "menu_pulldown"
*quitPane1.background: MenuBackground
*quitPane1.foreground: MenuForeground

*quitPane_b1.class: pushButtonGadget
*quitPane_b1.parent: quitPane1
*quitPane_b1.static: true
*quitPane_b1.name: quitPane_b1
*quitPane_b1.labelString: "Bye"
*quitPane_b1.activateCallback: {\
extern swidget classi,ilist;\
UxPopdownInterface(ilist);  \
UxPopdownInterface(classi);\
}
*quitPane_b1.fontList: BoldTextFont

*UtilsPane1.class: rowColumn
*UtilsPane1.parent: pullDownMenu1
*UtilsPane1.static: true
*UtilsPane1.name: UtilsPane1
*UtilsPane1.rowColumnType: "menu_pulldown"
*UtilsPane1.background: MenuBackground
*UtilsPane1.foreground: MenuForeground

*UtilsPane_b1.class: pushButtonGadget
*UtilsPane_b1.parent: UtilsPane1
*UtilsPane_b1.static: true
*UtilsPane_b1.name: UtilsPane_b1
*UtilsPane_b1.labelString: "Import "
*UtilsPane_b1.fontList: BoldTextFont
*UtilsPane_b1.activateCallback: {\
extern int tidost;\
extern char ostchoice[60]; \
char command[160];\
sprintf(command,"COPY/dd template.bdf  *,3 %s",ostchoice);\
AppendDialogText(command);\
}

*UtilsPane_b2.class: pushButtonGadget
*UtilsPane_b2.parent: UtilsPane1
*UtilsPane_b2.static: true
*UtilsPane_b2.name: UtilsPane_b2
*UtilsPane_b2.labelString: "Save"
*UtilsPane_b2.fontList: BoldTextFont
*UtilsPane_b2.activateCallback: {\
extern int tidost,msgvalue;\
extern Widget rowrule[256][2];\
extern swidget mymsg,myerror;\
extern char ostchoice[60],commerr[180];\
char descr[16],command[180],type[2],selecrit[256];\
char temp[256],mytext[256],mylabel[TBL_LABLEN+2];\
int bytelem,noelem,ncol,dummy,i,tidtemp,coltemp;\
strcpy(descr,UxGetText(UxFindSwidget("clas_t1")));\
if (descr[0] == '\0') {\
   UxPutMessageString(UxFindSwidget("errorDialog1"),"No rule name specified");\
   UxPopupInterface(myerror,no_grab);\
   return;\
    }\
SCDFND(tidost,descr,type,&noelem,&bytelem);\
strcpy(selecrit,UxGetText(UxFindSwidget("criteria"))); \
if (selecrit[0] == '\0') {\
\
   TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy);\
   oscfill(selecrit,256,'\0');\
   for (i=1; i<=ncol; i++) {\
          strcpy(mytext, XmTextGetString(rowrule[i-1][1])); \
          if (*mytext) {\
             mylabel[0] = ':';   \
             TCLGET(tidost,i,mylabel+1);\
            if (selecrit[0] != '\0') strcat(selecrit,".AND.");\
            decrypt1(mylabel,mytext,selecrit,temp);\
            }\
   }\
}\
  sprintf( command," @s saverule %s %s TMP_RULE ",ostchoice  ,descr );\
 \
 TCTINI("TMP_RULE" ,F_IO_MODE,F_TRANS,1,2,&tidtemp);\
 TCCINI(tidtemp,D_C_FORMAT,strlen(selecrit),"A20"," ","VAL",&coltemp);\
 TCEWRC(tidtemp, 1,coltemp,selecrit);\
 TCTCLO(tidtemp);\
if (type[0] == ' ')  AppendDialogText(command);\
 \
else \
{\
  msgvalue = 1; \
  sprintf(commerr," @s  saverule %s %s TMP_RULE ",ostchoice  ,descr );\
  UxPutMessageString( UxFindSwidget("Warning"),"The rule already exists, do you want to overwrite it?");\
  UxPopupInterface(mymsg,exclusive_grab);\
}\
}\
\
 \


*pullDownMenu1_p3.class: rowColumn
*pullDownMenu1_p3.parent: pullDownMenu1
*pullDownMenu1_p3.static: true
*pullDownMenu1_p3.name: pullDownMenu1_p3
*pullDownMenu1_p3.rowColumnType: "menu_pulldown"
*pullDownMenu1_p3.background: MenuBackground
*pullDownMenu1_p3.foreground: MenuForeground

*pullDownMenu1_p3_b1.class: pushButtonGadget
*pullDownMenu1_p3_b1.parent: pullDownMenu1_p3
*pullDownMenu1_p3_b1.static: true
*pullDownMenu1_p3_b1.name: pullDownMenu1_p3_b1
*pullDownMenu1_p3_b1.labelString: "Editor..."
*pullDownMenu1_p3_b1.fontList: BoldTextFont
*pullDownMenu1_p3_b1.activateCallback: {\
extern swidget ilist;\
extern int doI;\
UxPutListLabelString(UxFindSwidget("SelectInstrument"),"Rule");\
UxPutDialogTitle(UxFindSwidget("SelectInstrument"),"Select Rule");\
doI = 1;\
read_descr(UxGetWidget(UxFindSwidget("SelectInstrument")));\
\
\
UxPopupInterface(ilist,no_grab);}

*pullDownMenu1_p3_b2.class: pushButtonGadget
*pullDownMenu1_p3_b2.parent: pullDownMenu1_p3
*pullDownMenu1_p3_b2.static: true
*pullDownMenu1_p3_b2.name: pullDownMenu1_p3_b2
*pullDownMenu1_p3_b2.labelString: "Delete"
*pullDownMenu1_p3_b2.fontList: BoldTextFont
*pullDownMenu1_p3_b2.sensitive: "false"
*pullDownMenu1_p3_b2.activateCallback.source: public
*pullDownMenu1_p3_b2.activateCallback: 

*pullDownMenu1_p4.class: rowColumn
*pullDownMenu1_p4.parent: pullDownMenu1
*pullDownMenu1_p4.static: true
*pullDownMenu1_p4.name: pullDownMenu1_p4
*pullDownMenu1_p4.rowColumnType: "menu_pulldown"
*pullDownMenu1_p4.background: MenuBackground
*pullDownMenu1_p4.foreground: MenuForeground

*pullDownMenu1_p4_b1.class: pushButtonGadget
*pullDownMenu1_p4_b1.parent: pullDownMenu1_p4
*pullDownMenu1_p4_b1.static: true
*pullDownMenu1_p4_b1.name: pullDownMenu1_p4_b1
*pullDownMenu1_p4_b1.labelString: "On this interface..."
*pullDownMenu1_p4_b1.activateCallback: {\
extern swidget help;\
display_help(help,"Rule.help");\
}
*pullDownMenu1_p4_b1.fontList: BoldTextFont

*pullDownMenu1_p4_b2.class: pushButtonGadget
*pullDownMenu1_p4_b2.parent: pullDownMenu1_p4
*pullDownMenu1_p4_b2.static: true
*pullDownMenu1_p4_b2.name: pullDownMenu1_p4_b2
*pullDownMenu1_p4_b2.labelString: "On NTT defaults..."
*pullDownMenu1_p4_b2.fontList: BoldTextFont 
*pullDownMenu1_p4_b2.activateCallback: {\
extern swidget help;\
display_help(help,"Rule_default.help");\
} 

*pullDownMenu_top_b2.class: cascadeButton
*pullDownMenu_top_b2.parent: pullDownMenu1
*pullDownMenu_top_b2.static: true
*pullDownMenu_top_b2.name: pullDownMenu_top_b2
*pullDownMenu_top_b2.labelString: "Quit"
*pullDownMenu_top_b2.mnemonic: "Q"
*pullDownMenu_top_b2.subMenuId: "quitPane1"
*pullDownMenu_top_b2.background: MenuBackground
*pullDownMenu_top_b2.fontList: BoldTextFont
*pullDownMenu_top_b2.foreground: MenuForeground

*commandsCascade1.class: cascadeButton
*commandsCascade1.parent: pullDownMenu1
*commandsCascade1.static: true
*commandsCascade1.name: commandsCascade1
*commandsCascade1.labelString: "Rule"
*commandsCascade1.subMenuId: "UtilsPane1"
*commandsCascade1.background: MenuBackground
*commandsCascade1.fontList: BoldTextFont
*commandsCascade1.foreground: MenuForeground
*commandsCascade1.mnemonic: "R"
*commandsCascade1.x: 101

*pullDownMenu1_top_b1.class: cascadeButton
*pullDownMenu1_top_b1.parent: pullDownMenu1
*pullDownMenu1_top_b1.static: true
*pullDownMenu1_top_b1.name: pullDownMenu1_top_b1
*pullDownMenu1_top_b1.labelString: "Edit"
*pullDownMenu1_top_b1.mnemonic: "E"
*pullDownMenu1_top_b1.subMenuId: "pullDownMenu1_p3"
*pullDownMenu1_top_b1.background: MenuBackground
*pullDownMenu1_top_b1.fontList: BoldTextFont
*pullDownMenu1_top_b1.foreground: MenuForeground

*pullDownMenu1_top_b4.class: cascadeButton
*pullDownMenu1_top_b4.parent: pullDownMenu1
*pullDownMenu1_top_b4.static: true
*pullDownMenu1_top_b4.name: pullDownMenu1_top_b4
*pullDownMenu1_top_b4.labelString: "Help"
*pullDownMenu1_top_b4.mnemonic: "H"
*pullDownMenu1_top_b4.subMenuId: "pullDownMenu1_p4"
*pullDownMenu1_top_b4.background: MenuBackground
*pullDownMenu1_top_b4.fontList: BoldTextFont
*pullDownMenu1_top_b4.foreground: MenuForeground

*workAreaForm1.class: form
*workAreaForm1.parent: mainWindow1
*workAreaForm1.static: true
*workAreaForm1.name: workAreaForm1
*workAreaForm1.width: 490
*workAreaForm1.height: 440
*workAreaForm1.borderWidth: 0
*workAreaForm1.background: WindowBackground

*form6.class: form
*form6.parent: workAreaForm1
*form6.static: true
*form6.name: form6
*form6.resizePolicy: "resize_none"
*form6.unitType: "pixels"
*form6.x: 10
*form6.y: 10
*form6.width: 350
*form6.height: 610
*form6.background: WindowBackground

*scrolledWindow6.class: scrolledWindow
*scrolledWindow6.parent: form6
*scrolledWindow6.static: true
*scrolledWindow6.name: scrolledWindow6
*scrolledWindow6.scrollingPolicy: "automatic"
*scrolledWindow6.x: 10
*scrolledWindow6.y: 30
*scrolledWindow6.width: 350
*scrolledWindow6.height: 380
*scrolledWindow6.background: WindowBackground
*scrolledWindow6.scrollBarPlacement: "bottom_left"
*scrolledWindow6.scrollBarDisplayPolicy: "static"
*scrolledWindow6.leftAttachment: "attach_form"
*scrolledWindow6.rightAttachment: "attach_form"
*scrolledWindow6.rightOffset: 10
*scrolledWindow6.leftOffset: 10
*scrolledWindow6.bottomAttachment: "attach_none"
*scrolledWindow6.bottomOffset: 70
*scrolledWindow6.topOffset: 50
*scrolledWindow6.topAttachment: "attach_form"

*rowdescr.class: rowColumn
*rowdescr.parent: scrolledWindow6
*rowdescr.static: true
*rowdescr.name: rowdescr
*rowdescr.x: 8
*rowdescr.y: 8
*rowdescr.width: 350
*rowdescr.height: 370
*rowdescr.background: WindowBackground
*rowdescr.entryAlignment: "alignment_center"
*rowdescr.orientation: "horizontal"
*rowdescr.packing: "pack_column"
*rowdescr.createCallback: {\
\
}
*rowdescr.numColumns: 2
*rowdescr.entryCallback: {\
/*XmRowColumnCallbackStruct *cbs;\
Widget pb = cbs->widget;\
cbs = (XmRowColumnCallbackStruct *)UxCallbackArg;\
printf("%s: %s\n",XtName(pb),cbs->data);*/\
}
*rowdescr.resizeWidth: "true"

*label7.class: label
*label7.parent: form6
*label7.static: true
*label7.name: label7
*label7.x: 10
*label7.y: 420
*label7.width: 240
*label7.height: 40
*label7.background: WindowBackground
*label7.fontList: BoldTextFont
*label7.labelString: "Selection Criteria"
*label7.alignment: "alignment_beginning"
*label7.topOffset: 440
*label7.topAttachment: "attach_form"

*pushButton13.class: pushButton
*pushButton13.parent: form6
*pushButton13.static: true
*pushButton13.name: pushButton13
*pushButton13.x: 140
*pushButton13.y: 530
*pushButton13.width: 80
*pushButton13.height: 40
*pushButton13.background: ButtonBackground
*pushButton13.fontList: BoldTextFont
*pushButton13.foreground: ButtonForeground
*pushButton13.labelString: "Apply"
*pushButton13.bottomAttachment: "attach_form"
*pushButton13.bottomOffset: 10
*pushButton13.activateCallback: {\
extern int tidost;\
extern Widget rowrule[256][2];\
int ncol,dummy,i,unit,lennew;\
char mylabel[TBL_LABLEN+2],*temp,selecrit[256],mytext[256];\
TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy);\
temp = osmmget(256);\
oscfill(selecrit,256,'\0');\
for (i=1; i<=ncol; i++) {\
          strcpy(mytext, XmTextGetString(rowrule[i-1][1])); \
     if (*mytext) {\
         mylabel[0] = ':';   \
         TCLGET(tidost,i,mylabel+1);\
         if (selecrit[0] != '\0') strcat(selecrit,".AND.");\
         decrypt1(mylabel,mytext,selecrit,temp);\
         lennew = strlen(selecrit);\
          }\
}\
UxPutText(UxFindSwidget("criteria"),selecrit);\
/*SCDWRC(tidost,"test",1,selecrit,1,lennew,&unit);*/\
             \
}
*pushButton13.leftAttachment: "attach_form"
*pushButton13.leftOffset: 40

*pushButton14.class: pushButton
*pushButton14.parent: form6
*pushButton14.static: true
*pushButton14.name: pushButton14
*pushButton14.x: 250
*pushButton14.y: 530
*pushButton14.width: 80
*pushButton14.height: 40
*pushButton14.background: ButtonBackground
*pushButton14.fontList: BoldTextFont
*pushButton14.foreground: ButtonForeground
*pushButton14.labelString: "Clear" 
*pushButton14.leftAttachment: "attach_none"
*pushButton14.leftOffset: 40
*pushButton14.leftWidget: "pushButton13"
*pushButton14.rightAttachment: "attach_form"
*pushButton14.rightOffset: 40
*pushButton14.bottomAttachment: "attach_form"
*pushButton14.bottomOffset: 10
*pushButton14.activateCallback: {\
extern Widget rowrule[256][2];\
extern int tidost;\
int dummy,i,ncol;\
TCIGET(tidost,&ncol,&dummy,&dummy,&dummy,&dummy);\
for (i=0; i<ncol; i++) {\
   XmTextSetString(rowrule[i][1],"");\
   }\
UxPutText(UxFindSwidget("criteria"),"");\
}

*label8.class: label
*label8.parent: form6
*label8.static: true
*label8.name: label8
*label8.x: 20
*label8.y: 410
*label8.width: 100
*label8.height: 40
*label8.background: WindowBackground
*label8.fontList: BoldTextFont
*label8.labelString: "Rule Name"
*label8.alignment: "alignment_beginning"
*label8.topAttachment: "attach_form"
*label8.topOffset: 0
*label8.leftOffset: 10
*label8.leftAttachment: "attach_form"

*clas_t1.class: text
*clas_t1.parent: form6
*clas_t1.static: true
*clas_t1.name: clas_t1
*clas_t1.x: 20
*clas_t1.y: 450
*clas_t1.width: 200
*clas_t1.height: 40
*clas_t1.background: TextBackground
*clas_t1.topAttachment: "attach_form"
*clas_t1.topOffset: 0
*clas_t1.leftAttachment: "attach_form"
*clas_t1.leftOffset: 130
*clas_t1.fontList: BoldSmallFont
*clas_t1.foreground: TextForeground

*scrolledWindow7.class: scrolledWindow
*scrolledWindow7.parent: form6
*scrolledWindow7.static: true
*scrolledWindow7.name: scrolledWindow7
*scrolledWindow7.scrollingPolicy: "application_defined"
*scrolledWindow7.x: 0
*scrolledWindow7.y: 490
*scrolledWindow7.visualPolicy: "variable"
*scrolledWindow7.scrollBarDisplayPolicy: "static"
*scrolledWindow7.shadowThickness: 0
*scrolledWindow7.background: WindowBackground
*scrolledWindow7.height: 60
*scrolledWindow7.leftAttachment: "attach_form"
*scrolledWindow7.leftOffset: 10
*scrolledWindow7.rightAttachment: "attach_form"
*scrolledWindow7.rightOffset: 10
*scrolledWindow7.topAttachment: "attach_form"
*scrolledWindow7.topOffset: 480
*scrolledWindow7.scrollBarPlacement: "bottom_left"

*criteria.class: scrolledText
*criteria.parent: scrolledWindow7
*criteria.static: true
*criteria.name: criteria
*criteria.width: 350
*criteria.height: 60
*criteria.background: TextBackground
*criteria.fontList: BoldTextFont
*criteria.editable: "true"
*criteria.valueChangedCallback: {\
\
}
*criteria.modifyVerifyCallback: {\
XmTextVerifyCallbackStruct *cbs;\
int len;\
/*cbs = (XmTextVerifyCallbackStruct *)UxCallbackArg;\
if (cbs->text->ptr == NULL)\
      return;\
for (len=0; len<cbs->text->length; len++) {\
  if (isdigit(cbs->text->ptr[len])) {\
        int  i;\
         for (i=len; (i+1) <cbs->text->length; i++)\
            cbs->text->ptr[i] = cbs->text->ptr[i+1];\
         cbs->text->length--;\
         len--;\
        }\
  } */\
}
*criteria.foreground: TextForeground

