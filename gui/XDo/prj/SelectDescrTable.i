! UIMX ascii 2.0 key: 7106                                                      

*SelectDescrTable.class: fileSelectionBox
*SelectDescrTable.parent: NO_PARENT
*SelectDescrTable.defaultShell: topLevelShell
*SelectDescrTable.static: true
*SelectDescrTable.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*SelectDescrTable.ispecdecl:
*SelectDescrTable.funcdecl: swidget create_SelectDescrTable()\

*SelectDescrTable.funcname: create_SelectDescrTable
*SelectDescrTable.funcdef: "swidget", "<create_SelectDescrTable>(%)"
*SelectDescrTable.icode:
*SelectDescrTable.fcode: return(rtrn);\

*SelectDescrTable.auxdecl:
*SelectDescrTable.name: SelectDescrTable
*SelectDescrTable.resizePolicy: "resize_none"
*SelectDescrTable.unitType: "pixels"
*SelectDescrTable.x: 770
*SelectDescrTable.y: 330
*SelectDescrTable.width: 350
*SelectDescrTable.height: 400
*SelectDescrTable.background: WindowBackground
*SelectDescrTable.buttonFontList: BoldTextFont
*SelectDescrTable.labelFontList: BoldTextFont
*SelectDescrTable.textFontList: BoldTextFont
*SelectDescrTable.cancelCallback: {\
extern swidget dlist;\
UxPopdownInterface(dlist);\
}
*SelectDescrTable.okCallback: {\
XmFileSelectionBoxCallbackStruct *cbs;\
char *value;\
\
cbs = (XmFileSelectionBoxCallbackStruct *)UxCallbackArg;\
 XmStringGetLtoR(cbs->value,XmSTRING_DEFAULT_CHARSET,&value);\
UxPutText(UxFindSwidget("ost_t3"),value);\
XmTextShowPosition(UxGetWidget(UxFindSwidget("ost_t3")),strlen(value));\
}
*SelectDescrTable.dialogTitle: ""

