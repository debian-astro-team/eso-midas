! UIMX ascii 2.0 key: 6617                                                      

*errorDialog1.class: errorDialog
*errorDialog1.parent: NO_PARENT
*errorDialog1.defaultShell: topLevelShell
*errorDialog1.static: true
*errorDialog1.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*errorDialog1.ispecdecl:
*errorDialog1.funcdecl: swidget create_errorDialog1()\

*errorDialog1.funcname: create_errorDialog1
*errorDialog1.funcdef: "swidget", "<create_errorDialog1>(%)"
*errorDialog1.icode:
*errorDialog1.fcode: return(rtrn);\

*errorDialog1.auxdecl:
*errorDialog1.name: errorDialog1
*errorDialog1.dialogType: "dialog_file_selection"
*errorDialog1.unitType: "pixels"
*errorDialog1.x: 620
*errorDialog1.y: 150
*errorDialog1.width: 300
*errorDialog1.height: 140
*errorDialog1.background: WindowBackground
*errorDialog1.textFontList: BoldTextFont
*errorDialog1.buttonFontList: BoldTextFont
*errorDialog1.dialogTitle: "Error"
*errorDialog1.messageString: ""
*errorDialog1.labelFontList: TextFont
*errorDialog1.minimizeButtons: "false"
*errorDialog1.allowShellResize: "true"
*errorDialog1.createCallback: {\
XtVaSetValues(UxWidget,XmNallowShellResize,True,NULL); \
XtVaSetValues(UxWidget,XmNdialogType,XmDIALOG_ERROR,NULL);\
}

