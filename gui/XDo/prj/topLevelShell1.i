! UIMX ascii 2.0 key: 1872                                                      

*HelpDo.class: topLevelShell
*HelpDo.parent: NO_PARENT
*HelpDo.static: true
*HelpDo.gbldecl: #include <stdio.h>\
#include <ExternResources.h>
*HelpDo.ispecdecl:
*HelpDo.funcdecl: swidget create_HelpDo()\

*HelpDo.funcname: create_HelpDo
*HelpDo.funcdef: "swidget", "<create_HelpDo>(%)"
*HelpDo.icode:
*HelpDo.fcode: return(rtrn);\

*HelpDo.auxdecl:
*HelpDo.name: HelpDo
*HelpDo.x: 630
*HelpDo.y: 10
*HelpDo.width: 540
*HelpDo.height: 260
*HelpDo.background: WindowBackground 
*HelpDo.iconName: "Helpwindow"

*form7.class: form
*form7.parent: HelpDo
*form7.static: true
*form7.name: form7
*form7.resizePolicy: "resize_none"
*form7.unitType: "pixels"
*form7.x: 0
*form7.y: 10
*form7.width: 520
*form7.height: 240
*form7.background: WindowBackground

*scrolledWindow9.class: scrolledWindow
*scrolledWindow9.parent: form7
*scrolledWindow9.static: true
*scrolledWindow9.name: scrolledWindow9
*scrolledWindow9.scrollingPolicy: "application_defined"
*scrolledWindow9.x: 0
*scrolledWindow9.y: 10
*scrolledWindow9.visualPolicy: "variable"
*scrolledWindow9.scrollBarDisplayPolicy: "static"
*scrolledWindow9.shadowThickness: 0
*scrolledWindow9.topAttachment: "attach_form"
*scrolledWindow9.topOffset: 0
*scrolledWindow9.rightAttachment: "attach_form"
*scrolledWindow9.rightOffset: 0
*scrolledWindow9.leftAttachment: "attach_form"
*scrolledWindow9.leftOffset: 0
*scrolledWindow9.background: ApplicBackground
*scrolledWindow9.foreground: TextForeground

*scrolledText1.class: scrolledText
*scrolledText1.parent: scrolledWindow9
*scrolledText1.static: true
*scrolledText1.name: scrolledText1
*scrolledText1.width: 520
*scrolledText1.height: 200
*scrolledText1.background: TextBackground
*scrolledText1.editMode: "multi_line_edit"
*scrolledText1.editable: "false"
*scrolledText1.fontList: TextFont
*scrolledText1.foreground: TextForeground
*scrolledText1.scrollHorizontal: "false"

*CloseHelp.class: pushButton
*CloseHelp.parent: form7
*CloseHelp.static: true
*CloseHelp.name: CloseHelp
*CloseHelp.x: 190
*CloseHelp.y: 230
*CloseHelp.width: 100
*CloseHelp.height: 30
*CloseHelp.background: ButtonBackground
*CloseHelp.fontList: BoldTextFont
*CloseHelp.foreground: CancelForeground
*CloseHelp.labelString: "Close"
*CloseHelp.bottomAttachment: "attach_form"
*CloseHelp.bottomOffset: 5
*CloseHelp.leftAttachment: "attach_form"
*CloseHelp.leftOffset: 215
*CloseHelp.rightAttachment: "attach_form"
*CloseHelp.rightOffset: 215
*CloseHelp.activateCallback: {\
extern swidget help;\
UxPopdownInterface(help);\
}

