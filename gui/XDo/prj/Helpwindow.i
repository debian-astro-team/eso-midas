! UIMX ascii 2.0 key: 5572                                                      

*translation.table: transTable2
*translation.parent: Helpwindow
*translation.policy: augment
*translation.<Btn3Down>: ListBeginSelect()
*translation.<Btn3Up>: ListEndSelect()

*Helpwindow.class: topLevelShell
*Helpwindow.parent: NO_PARENT
*Helpwindow.static: true
*Helpwindow.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
\

*Helpwindow.ispecdecl:
*Helpwindow.funcdecl: swidget create_Helpwindow()\

*Helpwindow.funcname: create_Helpwindow
*Helpwindow.funcdef: "swidget", "<create_Helpwindow>(%)"
*Helpwindow.icode:
*Helpwindow.fcode: return(rtrn);\

*Helpwindow.auxdecl:
*Helpwindow.name: Helpwindow
*Helpwindow.x: 610
*Helpwindow.y: 130
*Helpwindow.width: 500
*Helpwindow.height: 220
*Helpwindow.background: TextBackground
*Helpwindow.keyboardFocusPolicy: "pointer"
*Helpwindow.iconName: "Helpwindow"

*form11.class: form
*form11.parent: Helpwindow
*form11.static: true
*form11.name: form11
*form11.resizePolicy: "resize_none"
*form11.unitType: "pixels"
*form11.x: 0
*form11.y: 0
*form11.width: 460
*form11.height: 220
*form11.background: ApplicBackground

*scrolledWindow3.class: scrolledWindow
*scrolledWindow3.parent: form11
*scrolledWindow3.static: true
*scrolledWindow3.name: scrolledWindow3
*scrolledWindow3.scrollingPolicy: "application_defined"
*scrolledWindow3.x: 20
*scrolledWindow3.y: 20
*scrolledWindow3.visualPolicy: "variable"
*scrolledWindow3.scrollBarDisplayPolicy: "static"
*scrolledWindow3.shadowThickness: 0
*scrolledWindow3.background: ApplicBackground
*scrolledWindow3.bottomAttachment: "attach_form"
*scrolledWindow3.bottomOffset: 33
*scrolledWindow3.foreground: TextForeground
*scrolledWindow3.leftAttachment: "attach_form"
*scrolledWindow3.leftOffset: 2
*scrolledWindow3.rightAttachment: "attach_form"
*scrolledWindow3.scrollBarPlacement: "bottom_left"
*scrolledWindow3.rightOffset: 2
*scrolledWindow3.scrolledWindowMarginHeight: 1
*scrolledWindow3.scrolledWindowMarginWidth: 1
*scrolledWindow3.spacing: 2
*scrolledWindow3.topAttachment: "attach_form"
*scrolledWindow3.topOffset: 2

*scrolledText1.class: scrolledText
*scrolledText1.parent: scrolledWindow3
*scrolledText1.static: true
*scrolledText1.name: scrolledText1
*scrolledText1.width: 380
*scrolledText1.height: 150
*scrolledText1.background: TextBackground
*scrolledText1.fontList: TextFont
*scrolledText1.foreground: TextForeground
*scrolledText1.marginHeight: 1
*scrolledText1.marginWidth: 2
*scrolledText1.scrollLeftSide: "false"
*scrolledText1.editMode: "multi_line_edit"
*scrolledText1.editable: "false"
*scrolledText1.createCallback: {\
extern swidget shtext;\
\
shtext = UxWidgetToSwidget(UxWidget);\
\
}

*pushButton10.class: pushButton
*pushButton10.parent: form11
*pushButton10.static: true
*pushButton10.name: pushButton10
*pushButton10.x: 125
*pushButton10.y: 92
*pushButton10.width: 100
*pushButton10.height: 30
*pushButton10.background: ButtonBackground
*pushButton10.fontList: BoldTextFont
*pushButton10.labelString: "Close"
*pushButton10.activateCallback: {\
extern int  kick[5];\
\
extern swidget swhelp;\
\
kick[4] = 0;			/* no more help window */\
\
UxPopdownInterface(swhelp);\
}
*pushButton10.foreground: CancelForeground
*pushButton10.bottomAttachment: "attach_form"
*pushButton10.bottomOffset: 2
*pushButton10.rightAttachment: "attach_none"
*pushButton10.rightOffset: 100
*pushButton10.leftAttachment: "attach_form"
*pushButton10.leftOffset: 180

