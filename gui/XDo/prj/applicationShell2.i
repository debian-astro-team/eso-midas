! UIMX ascii 2.0 key: 7005                                                      
*action.HelpACT: {\
/* char *s;\
 s = XmTextGetSelection(UxWidget); \
 \
 XmTextClearSelection(UxWidget,XtLastTimestampProcessed(UxDisplay));\
*/}
*action.WriteHelp: {\
char s[100],comm[160]  ;\
extern char print_com[];\
Widget sw = UxWidget;\
\
strcpy(s,"");\
/*sprintf(comm,"WRITE/OUT here");\
AppendDialogText(comm);*/\
 \
if (sw == UxGetWidget(UxFindSwidget("ColButton")))  {\
   strcpy(s,"Select the OST columns to be displayed into the scrolled window");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   } \
else if (sw == UxGetWidget(UxFindSwidget("ClassiButton"))) {\
   strcpy(s,"Activate interface for defining classification rules ");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }  \
else if (sw == UxGetWidget(UxFindSwidget("AssoButton"))) {\
   strcpy(s,"Activate interface for classifying images");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("AssoButton1"))) {\
   strcpy(s,"Activate interface for associating calibration exposures to scientific frames");\
   UxPutText(UxFindSwidget("SHelp") ,s); \
    }\
else if (sw == UxGetWidget(UxFindSwidget("NewsButton3"))) {\
   strcpy(s,"Create the OST table");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
else if (sw == UxGetWidget(UxFindSwidget("PrintButton3"))) {\
   strcpy(s,"Popdown this interface");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t1"))){ \
   strcpy(s,"Name of the Observation Summary Table to be created ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
 else if (sw == UxGetWidget(UxFindSwidget("ost_t2"))){ \
   strcpy(s,"List of frames to be processed ");\
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }  \
  else if (sw == UxGetWidget(UxFindSwidget("ost_t3"))){  \
   strcpy(s,"Name of the table containing the list of relevant Midas descriptors");      \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton4"))){  \
   strcpy(s,"Push button to popup the File Selection Box");     \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   }\
  else if (sw == UxGetWidget(UxFindSwidget("pushButton5"))){  \
        strcpy(s,"Push button to popup the File Selection Box");  \
   UxPutText(UxFindSwidget("SHelp3") ,s); \
   } \
}
*action.ClearHelp: {\
UxPutText(UxFindSwidget("SHelp"),"");\
}
*action.SelectCommand: {\
\
 \
\
}
*action.nothing: {\
printf("toto:\n");\
}

*translation.table: transTable6
*translation.parent: Classify
*translation.policy: replace
*translation.<Btn1Up>: HelpACT()
*translation.<Btn1Down>: grab-focus()
*translation.<EnterWindow>: WriteHelp()
*translation.<LeaveWindow>: ClearHelp()

*translation.table: transTable7
*translation.parent: Classify
*translation.policy: override
*translation.<Btn1Up> : SelectCommand()

*translation.table: transTable8
*translation.parent: Classify
*translation.policy: override
*translation.<PtrMoved>: nothing()

*Classify.class: applicationShell
*Classify.parent: NO_PARENT
*Classify.static: true
*Classify.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <midas_def.h>\

*Classify.ispecdecl:
*Classify.funcdecl: swidget create_Classify()\

*Classify.funcname: create_Classify
*Classify.funcdef: "swidget", "<create_Classify>(%)"
*Classify.icode:
*Classify.fcode: return(rtrn);\

*Classify.auxdecl:
*Classify.name: Classify
*Classify.x: 29
*Classify.y: 387
*Classify.width: 500
*Classify.height: 450

*mainWindow2.class: mainWindow
*mainWindow2.parent: Classify
*mainWindow2.static: true
*mainWindow2.name: mainWindow2
*mainWindow2.x: 200
*mainWindow2.y: 180
*mainWindow2.width: 700
*mainWindow2.height: 470
*mainWindow2.background: WindowBackground
*mainWindow2.unitType: "pixels"

*pullDownMenu2.class: rowColumn
*pullDownMenu2.parent: mainWindow2
*pullDownMenu2.static: true
*pullDownMenu2.name: pullDownMenu2
*pullDownMenu2.borderWidth: 0
*pullDownMenu2.menuHelpWidget: "pullDownMenu_top_b3"
*pullDownMenu2.rowColumnType: "menu_bar"
*pullDownMenu2.menuAccelerator: "<KeyUp>F10"
*pullDownMenu2.background: MenuBackground
*pullDownMenu2.foreground: MenuForeground

*quitPane2.class: rowColumn
*quitPane2.parent: pullDownMenu2
*quitPane2.static: true
*quitPane2.name: quitPane2
*quitPane2.rowColumnType: "menu_pulldown"
*quitPane2.background: MenuBackground
*quitPane2.foreground: MenuForeground

*quitPane_b3.class: pushButtonGadget
*quitPane_b3.parent: quitPane2
*quitPane_b3.static: true
*quitPane_b3.name: quitPane_b3
*quitPane_b3.labelString: "Bye"
*quitPane_b3.activateCallback: {\
extern swidget clashaupt;\
extern Widget row1[40],row2[40],row3[40];\
extern int rowno;\
int rows,i;\
for (i=0; i<5; i++) {\
    XmTextSetString(row1[i],"");\
    XmTextSetString(row2[i],"");\
    XmTextSetString(row3[i],"");\
    }\
for (i=5; i<rowno; i++) {\
    XtDestroyWidget(row1[i]);\
    row1[i] = (Widget )0;\
    XtDestroyWidget(row2[i]); \
    row2[i] = (Widget )0;\
    XtDestroyWidget(row3[i]);\
    row2[i] = (Widget )0;\
    } \
rowno = 5;\
UxPopdownInterface(clashaupt);\
}
*quitPane_b3.fontList: BoldTextFont

*UtilsPane2.class: rowColumn
*UtilsPane2.parent: pullDownMenu2
*UtilsPane2.static: true
*UtilsPane2.name: UtilsPane2
*UtilsPane2.rowColumnType: "menu_pulldown"
*UtilsPane2.background: MenuBackground
*UtilsPane2.foreground: MenuForeground

*UtilsPane_b3.class: pushButtonGadget
*UtilsPane_b3.parent: UtilsPane2
*UtilsPane_b3.static: true
*UtilsPane_b3.name: UtilsPane_b3
*UtilsPane_b3.labelString: "Open ..."
*UtilsPane_b3.fontList: BoldTextFont
*UtilsPane_b3.activateCallback: {\
char s[1000];\
extern swidget klist,tablelist;\
\
extern int do8;\
int strip =1;\
strcpy(s,"*.tbl");\
SetFileList(UxGetWidget(tablelist),strip,s);\
UxPutTitle(UxFindSwidget("transientShell8"),"Select Classification Table");\
do8 = 1;\
UxPopupInterface(klist,no_grab);\
}

*UtilsPane_b8.class: pushButtonGadget
*UtilsPane_b8.parent: UtilsPane2
*UtilsPane_b8.static: true
*UtilsPane_b8.name: UtilsPane_b8
*UtilsPane_b8.labelString: "Save"
*UtilsPane_b8.fontList: BoldTextFont
*UtilsPane_b8.activateCallback: {\
extern int tidclas;\
char mytext[60];\
int tid;\
strcpy(mytext,UxGetText(UxFindSwidget("sclas_t1")));\
\
tid = TCTID(mytext); \
if (tid != -1) TCTCLO(tid);\
save_clas_table(mytext);\
\
}

*pullDownMenu1_p1.class: rowColumn
*pullDownMenu1_p1.parent: pullDownMenu2
*pullDownMenu1_p1.static: true
*pullDownMenu1_p1.name: pullDownMenu1_p1
*pullDownMenu1_p1.rowColumnType: "menu_pulldown"
*pullDownMenu1_p1.background: MenuBackground
*pullDownMenu1_p1.foreground: MenuForeground

*pullDownMenu1_p3_b3.class: pushButtonGadget
*pullDownMenu1_p3_b3.parent: pullDownMenu1_p1
*pullDownMenu1_p3_b3.static: true
*pullDownMenu1_p3_b3.name: pullDownMenu1_p3_b3
*pullDownMenu1_p3_b3.labelString: "Add"
*pullDownMenu1_p3_b3.fontList: BoldTextFont
*pullDownMenu1_p3_b3.activateCallback: {\
extern int rowno;\
extern Widget row1[40],row2[40],row3[40];\
char mytext[8];\
rowno++; \
XtVaSetValues(UxGetWidget(UxFindSwidget("rowproc")),XmNnumColumns,rowno,NULL);\
sprintf(mytext,"text1_%d",rowno-1);\
row1[rowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowproc")),NULL);\
sprintf(mytext,"text2_%d",rowno-1);\
row2[rowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowproc")),NULL);\
sprintf(mytext,"text3_%d",rowno-1);\
row3[rowno-1] = XtVaCreateManagedWidget(mytext, xmTextWidgetClass,UxGetWidget( UxFindSwidget("rowproc")),NULL);\
}

*pullDownMenu1_p1_b3.class: pushButtonGadget
*pullDownMenu1_p1_b3.parent: pullDownMenu1_p1
*pullDownMenu1_p1_b3.static: true
*pullDownMenu1_p1_b3.name: pullDownMenu1_p1_b3
*pullDownMenu1_p1_b3.labelString: "Clear" 
*pullDownMenu1_p1_b3.fontList: BoldTextFont
*pullDownMenu1_p1_b3.activateCallback: {\
extern int rowno;\
extern Widget row1[40],row2[40],row3[40];\
int i;\
for (i=0; i<rowno; i++) {\
   XmTextSetString(row1[i],"");\
   XmTextSetString(row2[i],"");\
   XmTextSetString(row3[i],""); \
    }\
}

*pullDownMenu2_p4.class: rowColumn
*pullDownMenu2_p4.parent: pullDownMenu2
*pullDownMenu2_p4.static: true
*pullDownMenu2_p4.name: pullDownMenu2_p4
*pullDownMenu2_p4.rowColumnType: "menu_pulldown"
*pullDownMenu2_p4.background: MenuBackground
*pullDownMenu2_p4.foreground: MenuForeground

*pullDownMenu2_p4_b1.class: pushButtonGadget
*pullDownMenu2_p4_b1.parent: pullDownMenu2_p4
*pullDownMenu2_p4_b1.static: true
*pullDownMenu2_p4_b1.name: pullDownMenu2_p4_b1
*pullDownMenu2_p4_b1.labelString: "On this interface..."
*pullDownMenu2_p4_b1.activateCallback: {\
extern swidget help;\
display_help(help,"Classi.help"); \
} 
*pullDownMenu2_p4_b1.fontList: BoldTextFont

*pullDownMenu2_p4_b2.class: pushButtonGadget
*pullDownMenu2_p4_b2.parent: pullDownMenu2_p4
*pullDownMenu2_p4_b2.static: true
*pullDownMenu2_p4_b2.name: pullDownMenu2_p4_b2
*pullDownMenu2_p4_b2.labelString: "On importing data..."
*pullDownMenu2_p4_b2.activateCallback: {\
 \
}
*pullDownMenu2_p4_b2.fontList: BoldTextFont

*pullDownMenu_top_b3.class: cascadeButton
*pullDownMenu_top_b3.parent: pullDownMenu2
*pullDownMenu_top_b3.static: true
*pullDownMenu_top_b3.name: pullDownMenu_top_b3
*pullDownMenu_top_b3.labelString: "Quit"
*pullDownMenu_top_b3.mnemonic: "Q"
*pullDownMenu_top_b3.subMenuId: "quitPane2"
*pullDownMenu_top_b3.background: MenuBackground
*pullDownMenu_top_b3.fontList: BoldTextFont
*pullDownMenu_top_b3.foreground: MenuForeground

*commandsCascade2.class: cascadeButton
*commandsCascade2.parent: pullDownMenu2
*commandsCascade2.static: true
*commandsCascade2.name: commandsCascade2
*commandsCascade2.labelString: "File"
*commandsCascade2.subMenuId: "UtilsPane2"
*commandsCascade2.background: MenuBackground
*commandsCascade2.fontList: BoldTextFont
*commandsCascade2.foreground: MenuForeground
*commandsCascade2.mnemonic: "F"
*commandsCascade2.x: 101

*pullDownMenu1_top_b2.class: cascadeButton
*pullDownMenu1_top_b2.parent: pullDownMenu2
*pullDownMenu1_top_b2.static: true
*pullDownMenu1_top_b2.name: pullDownMenu1_top_b2
*pullDownMenu1_top_b2.labelString: "Edit"
*pullDownMenu1_top_b2.mnemonic: "E"
*pullDownMenu1_top_b2.subMenuId: "pullDownMenu1_p1"
*pullDownMenu1_top_b2.background: MenuBackground
*pullDownMenu1_top_b2.fontList: BoldTextFont
*pullDownMenu1_top_b2.foreground: MenuForeground

*pullDownMenu2_top_b1.class: cascadeButton
*pullDownMenu2_top_b1.parent: pullDownMenu2
*pullDownMenu2_top_b1.static: true
*pullDownMenu2_top_b1.name: pullDownMenu2_top_b1
*pullDownMenu2_top_b1.labelString: "Help"
*pullDownMenu2_top_b1.mnemonic: "H"
*pullDownMenu2_top_b1.subMenuId: "pullDownMenu2_p4"
*pullDownMenu2_top_b1.background: MenuBackground
*pullDownMenu2_top_b1.fontList: BoldTextFont
*pullDownMenu2_top_b1.foreground: MenuForeground

*workAreaForm2.class: form
*workAreaForm2.parent: mainWindow2
*workAreaForm2.static: true
*workAreaForm2.name: workAreaForm2
*workAreaForm2.width: 700
*workAreaForm2.height: 400
*workAreaForm2.borderWidth: 0
*workAreaForm2.background: WindowBackground

*form3.class: form
*form3.parent: workAreaForm2
*form3.static: true
*form3.name: form3
*form3.resizePolicy: "resize_none"
*form3.unitType: "pixels"
*form3.x: 10
*form3.y: 10
*form3.width: 480
*form3.height: 400
*form3.background: WindowBackground

*scrolledWindow1.class: scrolledWindow
*scrolledWindow1.parent: form3
*scrolledWindow1.static: true
*scrolledWindow1.name: scrolledWindow1
*scrolledWindow1.scrollingPolicy: "automatic"
*scrolledWindow1.x: 10
*scrolledWindow1.y: 30
*scrolledWindow1.width: 350
*scrolledWindow1.height: 300
*scrolledWindow1.background: WindowBackground
*scrolledWindow1.scrollBarPlacement: "bottom_left"
*scrolledWindow1.scrollBarDisplayPolicy: "static"
*scrolledWindow1.leftAttachment: "attach_form"
*scrolledWindow1.rightAttachment: "attach_form"
*scrolledWindow1.rightOffset: 10
*scrolledWindow1.leftOffset: 10
*scrolledWindow1.bottomAttachment: "attach_form"
*scrolledWindow1.bottomOffset: 60
*scrolledWindow1.topOffset: 150
*scrolledWindow1.topAttachment: "attach_form"
*scrolledWindow1.visualPolicy: "variable"

*rowproc.class: rowColumn
*rowproc.parent: scrolledWindow1
*rowproc.static: true
*rowproc.name: rowproc
*rowproc.x: 8
*rowproc.y: 8
*rowproc.width: 500
*rowproc.height: 370
*rowproc.background: WindowBackground
*rowproc.entryAlignment: "alignment_center"
*rowproc.orientation: "horizontal"
*rowproc.packing: "pack_column"
*rowproc.createCallback: {\
\
}
*rowproc.numColumns: 3
*rowproc.entryCallback: {\
/*XmRowColumnCallbackStruct *cbs;\
Widget pb = cbs->widget;\
cbs = (XmRowColumnCallbackStruct *)UxCallbackArg;\
printf("%s: %s\n",XtName(pb),cbs->data);*/\
}
*rowproc.resizeWidth: "true"

*label5.class: label
*label5.parent: form3
*label5.static: true
*label5.name: label5
*label5.x: 10
*label5.y: 420
*label5.width: 240
*label5.height: 40
*label5.background: WindowBackground
*label5.fontList: BoldTextFont
*label5.labelString: "Selection Criteria"
*label5.alignment: "alignment_beginning"
*label5.topOffset: 440
*label5.topAttachment: "attach_form"

*pushButton6.class: pushButton
*pushButton6.parent: form3
*pushButton6.static: true
*pushButton6.name: pushButton6
*pushButton6.x: 140
*pushButton6.y: 530
*pushButton6.width: 80
*pushButton6.height: 40
*pushButton6.background: ButtonBackground
*pushButton6.fontList: BoldTextFont
*pushButton6.foreground: ButtonForeground
*pushButton6.labelString: "Apply"
*pushButton6.bottomAttachment: "attach_form"
*pushButton6.bottomOffset: 10
*pushButton6.activateCallback: {\
extern char ostchoice[60];\
extern int tidost;\
extern swidget myerror;\
char command[160],mycval[81],column[18];\
if (tidost == -1) {\
  UxPutMessageString(UxFindSwidget("errorDialog1"),"No OST opened");\
  UxPopupInterface(myerror,no_grab);\
  return;\
  }\
save_clas_table("TEMP_CLAS");\
sprintf(command,"CLASSIFY/IMAGE %s TEMP_CLAS.tbl",ostchoice);\
/*AppendDialogText(command);*/\
mycval[0] = '\0';\
column[0] = '\0';\
classify(ostchoice,"TEMP_CLAS.tbl",column,mycval);\
TCTCLO(tidost);\
TCTOPN(ostchoice,F_I_MODE,&tidost);\
/*display_col_table(UxGetWidget(UxFindSwidget("readtable")));*/\
\
}
*pushButton6.leftAttachment: "attach_form"
*pushButton6.leftOffset: 200

*label11.class: label
*label11.parent: form3
*label11.static: true
*label11.name: label11
*label11.x: 40
*label11.y: 10
*label11.width: 130
*label11.height: 30
*label11.background: WindowBackground
*label11.fontList: BoldTextFont
*label11.labelString: "DESCR"
*label11.topAttachment: "attach_form"
*label11.topOffset: 100
*label11.leftAttachment: "attach_form"
*label11.leftOffset: 40

*label12.class: label
*label12.parent: form3
*label12.static: true
*label12.name: label12
*label12.x: 50
*label12.y: 20
*label12.width: 130
*label12.height: 30
*label12.background: WindowBackground
*label12.fontList: BoldTextFont
*label12.labelString: "OUTCOL"
*label12.leftAttachment: "attach_form"
*label12.leftOffset: 180
*label12.topAttachment: "attach_form"
*label12.topOffset: 100

*label13.class: label
*label13.parent: form3
*label13.static: true
*label13.name: label13
*label13.x: 190
*label13.y: 20
*label13.width: 130
*label13.height: 30
*label13.background: WindowBackground
*label13.fontList: BoldTextFont
*label13.labelString: "OUTCHAR"
*label13.leftAttachment: "attach_form"
*label13.leftOffset: 320
*label13.topAttachment: "attach_form"
*label13.topOffset: 100

*label21.class: label
*label21.parent: form3
*label21.static: true
*label21.name: label21
*label21.x: 50
*label21.y: 110
*label21.width: 200
*label21.height: 30
*label21.background: WindowBackground
*label21.fontList: BoldTextFont
*label21.labelString: "Classification Table Name"
*label21.topAttachment: "attach_form"
*label21.topOffset: 30
*label21.leftAttachment: "attach_form"
*label21.leftOffset: 20

*sclas_t1.class: text
*sclas_t1.parent: form3
*sclas_t1.static: true
*sclas_t1.name: sclas_t1
*sclas_t1.x: 230
*sclas_t1.y: 20
*sclas_t1.width: 240
*sclas_t1.height: 40
*sclas_t1.background: TextBackground
*sclas_t1.topAttachment: "attach_form"
*sclas_t1.topOffset: 25
*sclas_t1.fontList: BoldTextFont
*sclas_t1.foreground: TextForeground

