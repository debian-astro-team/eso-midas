! UIMX ascii 2.0 key: 1766                                                      

*transientShell1.class: transientShell
*transientShell1.parent: NO_PARENT
*transientShell1.static: true
*transientShell1.gbldecl: #include <stdio.h>\
#include <ExternResources.h>\
#include <midas_def.h>\
#include <tbldef.h>
*transientShell1.ispecdecl:
*transientShell1.funcdecl: swidget create_transientShell1()\

*transientShell1.funcname: create_transientShell1
*transientShell1.funcdef: "swidget", "<create_transientShell1>(%)"
*transientShell1.icode:
*transientShell1.fcode: return(rtrn);\

*transientShell1.auxdecl:
*transientShell1.name: transientShell1
*transientShell1.x: 790
*transientShell1.y: 100
*transientShell1.width: 290
*transientShell1.height: 580
*transientShell1.title: "Select Columns"

*form2.class: form
*form2.parent: transientShell1
*form2.static: true
*form2.name: form2
*form2.resizePolicy: "resize_none"
*form2.unitType: "pixels"
*form2.x: 0
*form2.y: 0
*form2.width: 290
*form2.height: 240
*form2.background: ApplicBackground

*scrolledWindow3.class: scrolledWindow
*scrolledWindow3.parent: form2
*scrolledWindow3.static: true
*scrolledWindow3.name: scrolledWindow3
*scrolledWindow3.scrollingPolicy: "application_defined"
*scrolledWindow3.x: 10
*scrolledWindow3.y: 10
*scrolledWindow3.visualPolicy: "variable"
*scrolledWindow3.scrollBarDisplayPolicy: "static"
*scrolledWindow3.shadowThickness: 0
*scrolledWindow3.bottomAttachment: "attach_form"
*scrolledWindow3.bottomOffset: 45
*scrolledWindow3.height: 316
*scrolledWindow3.leftAttachment: "attach_form"
*scrolledWindow3.leftOffset: 2
*scrolledWindow3.rightAttachment: "attach_form"
*scrolledWindow3.rightOffset: 2
*scrolledWindow3.topAttachment: "attach_form"
*scrolledWindow3.topOffset: 225
*scrolledWindow3.width: 270
*scrolledWindow3.background: ApplicBackground
*scrolledWindow3.scrollBarPlacement: "bottom_left"

*columnlist.class: scrolledList
*columnlist.parent: scrolledWindow3
*columnlist.static: false
*columnlist.name: columnlist
*columnlist.width: 190
*columnlist.height: 320
*columnlist.background: TextBackground
*columnlist.fontList: TextFont
*columnlist.foreground: TextForeground
*columnlist.scrollBarDisplayPolicy: "static"
*columnlist.browseSelectionCallback: {\
\
}
*columnlist.visibleItemCount: 16
*columnlist.selectionPolicy: "extended_select"
*columnlist.extendedSelectionCallback: {\
XmListCallbackStruct *cbs;\
char *choice,mymess[200];\
extern char *colchoice;\
extern int colno;\
extern swidget clist;\
int i;\
/*cbs = (XmListCallbackStruct *)UxCallbackArg;\
if (cbs->reason == XmCR_EXTENDED_SELECT ){\
colno = 0;\
for (i=0; i<cbs->selected_item_count; i++) {\
   XmStringGetLtoR(cbs->selected_items[i],XmSTRING_DEFAULT_CHARSET,&choice);\
    \
   strcpy(colchoice +(TBL_LABLEN+1)*colno, choice);\
   colno++;\
   \
   XtFree(choice);\
   }\
sprintf(mymess,"WRITE/OUT %d",colno);\
AppendDialogText(mymess);\
}else {\
   XmStringGetLtoR(cbs->item,XmSTRING_DEFAULT_CHARSET,&choice);\
    \
   XtFree(choice);\
   }*/\
 /*   UxPopdownInterface(clist);*/\
 \
   \
/*   display_col_table(UxGetWidget(UxFindSwidget("readtable"))); */\
}
*columnlist.automaticSelection: "false"

*pushButton3.class: pushButton
*pushButton3.parent: form2
*pushButton3.static: true
*pushButton3.name: pushButton3
*pushButton3.x: 0
*pushButton3.y: 318
*pushButton3.width: 200
*pushButton3.height: 30
*pushButton3.background: ButtonBackground
*pushButton3.bottomAttachment: "attach_form"
*pushButton3.bottomOffset: 5
*pushButton3.leftAttachment: "attach_form"
*pushButton3.leftOffset: 170
*pushButton3.rightAttachment: "attach_form"
*pushButton3.rightOffset: 10
*pushButton3.labelString: "cancel"
*pushButton3.fontList: BoldTextFont
*pushButton3.foreground: ButtonForeground
*pushButton3.activateCallback: {\
extern swidget clist;\
UxPopdownInterface(clist);\
}

*apply.class: pushButton
*apply.parent: form2
*apply.static: true
*apply.name: apply
*apply.x: 160
*apply.y: 330
*apply.width: 200
*apply.height: 30
*apply.background: ButtonBackground
*apply.labelString: "apply"
*apply.fontList: BoldTextFont
*apply.foreground: ButtonForeground
*apply.leftOffset: 10
*apply.bottomAttachment: "attach_form"
*apply.bottomOffset: 5
*apply.leftAttachment: "attach_form"
*apply.rightAttachment: "attach_form"
*apply.rightOffset: 160
*apply.activateCallback: {\
extern char *colchoice;\
extern swidget clist;\
int i;\
getselectlist(UxGetWidget(UxFindSwidget("columnlist")));\
display_col_table(UxGetWidget(UxFindSwidget("readtable")));\
\
UxPopdownInterface(clist);\
}

*scrolledWindow8.class: scrolledWindow
*scrolledWindow8.parent: form2
*scrolledWindow8.static: true
*scrolledWindow8.name: scrolledWindow8
*scrolledWindow8.scrollingPolicy: "application_defined"
*scrolledWindow8.x: 10
*scrolledWindow8.y: 10
*scrolledWindow8.visualPolicy: "variable"
*scrolledWindow8.scrollBarDisplayPolicy: "static"
*scrolledWindow8.shadowThickness: 0
*scrolledWindow8.height: 316
*scrolledWindow8.width: 270
*scrolledWindow8.background: ApplicBackground
*scrolledWindow8.scrollBarPlacement: "bottom_left"
*scrolledWindow8.rightAttachment: "attach_form"
*scrolledWindow8.rightOffset: 0
*scrolledWindow8.topAttachment: "attach_form"
*scrolledWindow8.topOffset: 30
*scrolledWindow8.leftAttachment: "attach_form"
*scrolledWindow8.leftOffset: 0
*scrolledWindow8.bottomAttachment: "attach_form"
*scrolledWindow8.bottomOffset: 380

*columnlist1.class: scrolledList
*columnlist1.parent: scrolledWindow8
*columnlist1.static: false
*columnlist1.name: columnlist1
*columnlist1.width: 190
*columnlist1.height: 320
*columnlist1.background: TextBackground
*columnlist1.fontList: TextFont
*columnlist1.foreground: TextForeground
*columnlist1.scrollBarDisplayPolicy: "static"
*columnlist1.browseSelectionCallback: {\
XmListCallbackStruct *cbs;\
char *choice,mymode[50];\
cbs = (XmListCallbackStruct *)UxCallbackArg;\
XmStringGetLtoR(cbs->item, XmSTRING_DEFAULT_CHARSET, &choice);\
strcpy(mymode,choice);\
selectlist( mymode,UxGetWidget( UxFindSwidget("columnlist")));\
 XtFree(choice);\
}
*columnlist1.visibleItemCount: 9
*columnlist1.selectionPolicy: "browse_select"
*columnlist1.extendedSelectionCallback: {\
 \
}

*label22.class: label
*label22.parent: form2
*label22.static: true
*label22.name: label22
*label22.x: 20
*label22.y: 0
*label22.width: 270
*label22.height: 30
*label22.background: WindowBackground 
*label22.leftAttachment: "attach_form"
*label22.leftOffset: 0
*label22.rightAttachment: "attach_form"
*label22.rightOffset: 0
*label22.topAttachment: "attach_form"
*label22.topOffset: 0
*label22.fontList: BoldTextFont
*label22.labelString: "Instrument Mode"

*label23.class: label
*label23.parent: form2
*label23.static: true
*label23.name: label23
*label23.x: 10
*label23.y: 10
*label23.width: 270
*label23.height: 30
*label23.background: WindowBackground 
*label23.fontList: BoldTextFont
*label23.labelString: "Columns "
*label23.leftAttachment: "attach_form"
*label23.leftOffset: 0
*label23.rightAttachment: "attach_form"
*label23.rightOffset: 0
*label23.topAttachment: "attach_form"
*label23.topOffset: 195

